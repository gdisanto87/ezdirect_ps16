<?php

    // FILE DI PROVA: NELLA 1.4 ERA NELLA CARTELLA PRINCIPALE DI PRESTASHOP, SPOSTARLO LI' O MODIFICARE GLI URL CHE LO RICHIAMANO E SDOPPIARLO NEL FRONT END

    header('Access-Control-Allow-Origin: https://46.101.235.11'); // server di prova
    //header('Access-Control-Allow-Origin: https://185.81.4.51'); // decommentare: giusto, server ezdirect

    if (!defined('_PS_ADMIN_DIR_')) {
        define('_PS_ADMIN_DIR_', getcwd());
    }
    include(_PS_ADMIN_DIR_.'/../config/config.inc.php');

    if(Tools::getIsset('suggerisciIndirizzo'))
    {
        Address::suggestAddress(Tools::getValue('indirizzo'), Tools::getValue('cap'), Tools::getValue('citta'), Tools::getValue('campo'));
    }

    if(Tools::getIsset('cercacitta'))
    {
        if($_POST['cap'] == '')
            die();
        
        $cittas = Db::getInstance()->executeS('SELECT * FROM cap WHERE cap = "'.$_POST['cap'].'"');

        if(count($cittas) == 0)
        {
            $cittas = Db::getInstance()->executeS('SELECT * FROM cap WHERE cap LIKE "'.substr($_POST['cap'],0,4).'x%"');
        }

        if(count($cittas) == 0)
        {
            $cittas = Db::getInstance()->executeS('SELECT * FROM cap WHERE cap LIKE "'.substr($_POST['cap'],0,3).'xx%"');
        }

        $options_citta = '';

        foreach($cittas as $citta)
            $options_citta .= '<option rel="'.$citta['cod_istat'].'" name="'.$citta['comune'].'">'.$citta['comune'].'</option>';
            
        die($options_citta);
    }

    if(Tools::getIsset('cercaprovincia'))
    {
        if($_POST['citta'] == '')
            die();
        
        $provincia_cod = Db::getInstance()->getValue('SELECT cod_provincia FROM comuni WHERE cod_istat = "'.$_POST['citta'].'"');
        $provincia = Db::getInstance()->getRow('SELECT * FROM province WHERE cod_provincia = "'.$provincia_cod.'"');
        
        $id_state = Db::getInstance()->getValue('SELECT id_state FROM '._DB_PREFIX_.'state WHERE id_country = 10 AND iso_code = "'.$provincia['sigla'].'"');
        
        die($provincia['provincia'].':::'.$id_state);
    }

?>