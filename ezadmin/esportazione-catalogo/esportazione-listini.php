<?php

	/* CORREGGERE: file vuoto, vengono settati solo i titoli delle colonne! */

	if (!defined('_PS_ADMIN_DIR_')) {
		define('_PS_ADMIN_DIR_', getcwd());
	}
	require_once(_PS_ADMIN_DIR_.'/../config/config.inc.php');

	ini_set("memory_limit","99992M");
	set_time_limit(172000);
	//ini_set('display_errors', 'on');
	//error_reporting(E_ALL);
	date_default_timezone_set('Europe/Rome');

	$foglio = 0;

	/** PHPExcel */
	require_once 'Classes/PHPExcel.php';

	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();

	$context = Context::getContext();

	// $carrelli = array('127359', '124200', '124157'); // cosa sono?

	foreach($_POST['carrello'] as $marchio)
	{
		$query = "
			SELECT
				p.id_product AS id_prestashop,
				p.reference AS codice_ez,
				p.reference AS codice_prodotto,
				p.supplier_reference AS codice_costruttore,
				pl.name AS nome_prodotto,
				p.ean13 AS codice_ean,
				p.listino AS prezzo_listino,
				p.price AS prezzo_ezdirect,
				pl.description AS descrizione,
				p.id_category_default AS id_categoria_prestashop,
				p.id_manufacturer AS id_costruttore_prestashop,
				m.name AS costruttore,
				pl.description_short AS descrizione_breve
			FROM "._DB_PREFIX_."cart_product cp 
				JOIN "._DB_PREFIX_."product p ON cp.id_product = p.id_product
				LEFT JOIN "._DB_PREFIX_."product_download pd ON pd.id_product = p.id_product
				JOIN "._DB_PREFIX_."product_lang pl ON (p.id_product = pl.id_product)
				LEFT JOIN product_esolver pe ON pe.id_product = p.id_product
				LEFT JOIN "._DB_PREFIX_."manufacturer m ON (p.id_manufacturer = m.id_manufacturer)
				LEFT JOIN "._DB_PREFIX_."supplier s ON (p.id_supplier = s.id_supplier)
			WHERE pl.id_lang = ".$context->language->id."
				AND cp.id_cart = ".$marchio."
			GROUP BY id_prestashop
			ORDER BY cp.sort_order ASC
		";

		$result = Db::getInstance()->executeS($query);

		if($foglio > 0);
			$objPHPExcel->createSheet(NULL, $foglio);

		// Set properties

		$i = 2;

		if(Tools::getIsset('rivenditori') || Tools::getIsset('rivenditori2'))
		{
			$objPHPExcel->setActiveSheetIndex($foglio)
						->setCellValue('A1', 'Codice')
						->setCellValue('B1', 'Descrizione')
						->setCellValue('C1', 'Listino')
						->setCellValue('D1', 'Sconto')
						->setCellValue('E1', 'Netto')
			;
		}
		else
		{
			$objPHPExcel->setActiveSheetIndex($foglio)
						->setCellValue('A1', 'Codice')
						->setCellValue('B1', 'Descrizione')
						->setCellValue('C1', 'Listino')
			;
		}

		$objPHPExcel->getActiveSheet()->getStyle("A")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
		$objPHPExcel->getActiveSheet()->getStyle("B")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
		$objPHPExcel->getActiveSheet()->getStyle("C")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

		if(Tools::getIsset('rivenditori') || Tools::getIsset('rivenditori2'))
		{
			$objPHPExcel->getActiveSheet()->getStyle("D")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
			$objPHPExcel->getActiveSheet()->getStyle("E")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
		}

		foreach($result as $row) 
		{
			$objPHPExcel->getActiveSheet()->getCell("A$i")->setValueExplicit($row['codice_prodotto'], PHPExcel_Cell_DataType::TYPE_STRING);

			$descrizione_breve = $row['descrizione_breve'];

			$sc_riv_1 = Db::getInstance()->getValue("SELECT reduction FROM "._DB_PREFIX_."specific_price WHERE id_product = $row[id_prestashop] AND (specific_price_name = 'sc_riv_1') ORDER BY specific_price_name ASC");
			$sc_riv_2 = Db::getInstance()->getValue("SELECT reduction FROM "._DB_PREFIX_."specific_price WHERE id_product = $row[id_prestashop] AND (specific_price_name = 'sc_riv_2') ORDER BY specific_price_name ASC");

			if(Tools::getIsset('rivenditori'))
			{
				if($sc_riv_1 > 0)
					$sconto = $sc_riv_1;
				else
					$sconto = 0;
			}

			if(Tools::getIsset('rivenditori2'))
			{
				if($sc_riv_2 > 0)
					$sconto = $sc_riv_2;
				else
					$sconto = 0;
			}

			if(Tools::getIsset('rivenditori') || Tools::getIsset('rivenditori2'))
			{
				$prezzo_finito = $row['prezzo_listino'] - ($row['prezzo_listino'] * $sconto);
				
				$objPHPExcel->setActiveSheetIndex($foglio)
							->setCellValue("B$i", $row['nome_prodotto'])
							->setCellValue("C$i", number_format($row['prezzo_listino'], 2, ',', ''))
							->setCellValue("D$i", number_format(($sconto*100), 2, ',', ''))
							->setCellValue("E$i", number_format($prezzo_finito, 2, ',', ''))
				;
			}
			else
			{
				$objPHPExcel->setActiveSheetIndex($foglio)
							->setCellValue("B$i", $row['nome_prodotto'])
							->setCellValue("C$i", number_format($row['prezzo_listino'], 2, ',', ''))
				;
			}

			$i++; // INCREMENTO ----------------------------------------------------------------------------------------------------------------------------------------

		}
		
		$highestRow = $objPHPExcel->getActiveSHeet()->getHighestRow();

		if(Tools::getIsset('rivenditori') || Tools::getIsset('rivenditori2'))
		{
			$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);			
			$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->getStartColor()->setRGB('FFFF00');		
			$objPHPExcel->getActiveSheet()->getStyle("A1:E$i")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

			$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('10');
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('10');
			$objPHPExcel->getActiveSheet()->getStyle("A1:D1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle("D2:D$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()->getStyle("E2:E$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		}
		else
		{	
			$objPHPExcel->getActiveSheet()->getStyle("A1:C1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);			
			$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setRGB('FFFF00');		
			$objPHPExcel->getActiveSheet()->getStyle("A1:C$i")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

			$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->setBold(true);
		}

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('25');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('50');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('10');

		$objPHPExcel->getActiveSheet()->setTitle(substr(Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'cart WHERE id_cart = '.$marchio),0,30));

		$objPHPExcel->getActiveSheet()->getStyle("C2:C$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet

		$objPHPExcel->getActiveSheet()
			->getPageSetup()
			->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE)
		;
			
		$foglio = $foglio + 1;
	}

	$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
	$cacheSettings = array(' memoryCacheSize ' => '8000MB');
	PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

	$objPHPExcel->setActiveSheetIndex(0);

	$data = date("Ymd");

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->setPreCalculateFormulas(false);
	$objWriter->save(str_replace(".php", ".xls", "esportazione-catalogo/catalogo_xls/listini-ez.php"));
		
?>