<?php

$connection = mysql_connect(_DB_SERVER_,_DB_USER_,_DB_PASSWD_);
mysql_select_db(_DB_NAME_,$connection);
$link_global = $connection;

//$connection = mysql_connect("localhost","root",""); 
//mysql_select_db("ezdirect",$connection);

ini_set("memory_limit","892M");
set_time_limit(3600);

$query = "SELECT
product.id_product AS id_prestashop,
product.reference AS codice_spring,
product_other.aggiornato AS aggiornato,
product.serial AS seriale,
(CASE WHEN product.provvigione > 0 THEN product.provvigione ELSE '' END) AS provvigione,
product.wholesale_price AS prezzo_acquisto,
supplier.name AS fornitore,
product.date_available AS data_disponibilita,
product.stock_quantity AS mag_spring,
product.sconto_acquisto_1 AS sconto_acquisto_1,
product.sconto_acquisto_2 AS sconto_acquisto_2,
product.sconto_acquisto_3 AS sconto_acquisto_3,
product.scontolistinovendita AS scontolistinovendita,
product_other.sconto_rivenditore_1 AS sc_riv_1,
product_other.sconto_rivenditore_2 AS sc_riv_2,
product_other.sconto_rivenditore_1_min AS sc_riv_1_min,
product_other.sconto_rivenditore_1_max AS sc_riv_1_max,
product_other.sconto_rivenditore_2_min AS sc_riv_2_min,
product_other.sconto_rivenditore_2_max AS sc_riv_2_max,
product_other.directel AS directel,
product_other.redirect AS redirect,
product_other.wordpress_1 AS wordpress_1,
product_other.wordpress_2 AS wordpress_2,
product_other.wordpress_3 AS wordpress_3,
product.canonical AS canonical,
product.reference AS codice_prodotto,
product.supplier_reference AS codice_costruttore,
product_lang.name AS nome_prodotto,
product.ean13 AS codice_ean,
product.listino AS prezzo_listino,
product.price AS prezzo_ezdirect,
product.quantity AS quantita,
product.stock_quantity AS mag_ez,
product.supplier_quantity AS mag_allnet,
product.esprinet_quantity AS mag_esprinet,
product.itancia_quantity AS mag_itancia,
product_lang.description AS descrizione,
product_lang.available_now AS msg_disponibile,
product_lang.available_later AS msg_non_disponibile,
product.weight AS peso,
product.id_category_default AS id_categoria_prestashop,
product.id_manufacturer AS id_costruttore_prestashop,
manufacturer.name AS costruttore,
product_other.prova_gratuita AS prova_gratuita,
product_other.trasporto_gratuito AS trasporto_gratuito,
product_other.mostra_spese_spedizione AS mostra_spese_spedizione,
product.condition AS nuovo_eco,
product.date_upd AS data_ultima_modifica,
product.date_add AS data_inserimento,
product_other.garanzia_3_anni AS garanzia_3_anni,
product_other.assistenza_3_anni AS assistenza_3_anni,
product_lang.description_amazon AS descrizione_amazon,
product_lang.meta_description AS metatag_description,
product_lang.meta_title AS title_tag,
product_lang.meta_keywords AS metatag_keywords,
product_lang.punti_di_forza AS punti_di_forza,
product_lang.description_short AS descrizione_breve,
product_lang.cat_homepage AS cat_homepage,
product_lang.desc_homepage AS desc_homepage,
product.trovaprezzi AS trovaprezzi,
product.google_shopping AS google_shopping,
product.eprice AS eprice,
product.amazon AS amazon,
product.asin AS asin,
product.active AS stato,
product.noindex AS noindex,
pe.rebate_1 AS rebate_1,
pe.rebate_2 AS rebate_2,
pe.rebate_3 AS rebate_3
FROM
  product
  JOIN product_lang ON (product.id_product = product_lang.id_product)
  LEFT JOIN product_other ON (product.id_product = product_other.id_product)
  LEFT JOIN product_esolver pe ON pe.id_product = product.id_product
  LEFT JOIN manufacturer ON (product.id_manufacturer = manufacturer.id_manufacturer)
  LEFT JOIN supplier ON (product.id_supplier = supplier.id_supplier)
WHERE
 product_lang.id_lang = 5 AND (product.id_category_default = '27'
OR product.id_category_default = '217'
OR product.id_category_default = '179'
OR product.id_category_default = '178'
OR product.id_category_default = '225'
OR product.id_category_default = '242'
OR product.id_category_default = '180')

".(Tools::getIsset('senza_fp') ? ' AND product.fuori_produzione = 0' : '')."
GROUP BY id_prestashop
  ORDER BY id_costruttore_prestashop
 
  ";

$result = mysql_query($query, $link_global);

error_reporting(E_ALL);

date_default_timezone_set('Europe/Rome');

/** PHPExcel */
require_once 'Classes/PHPExcel.php';


// Create new PHPExcel object
echo date('H:i:s') . " Creato nuovo file di Excel... !";
$objPHPExcel = new PHPExcel();

// Set properties

$i = 2;

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Id prestashop')
            ->setCellValue('B1', 'Codice costruttore')
            ->setCellValue('C1', 'Codice EZ Spring')
            ->setCellValue('D1', 'Codice EAN')
            ->setCellValue('E1', 'ASIN Amazon')
            ->setCellValue('F1', 'Aggiornato')
            ->setCellValue('G1', 'Data ultima modifica')
            ->setCellValue('H1', 'Costruttore')
            ->setCellValue('I1', 'Nome prodotto')
            ->setCellValue('J1', 'Listino')
            ->setCellValue('K1', 'Sc acq 1')
            ->setCellValue('L1', 'Sc acq 2')
            ->setCellValue('M1', 'Sc acq 3')
			->setCellValue('N1', 'Netto eSolver')
            ->setCellValue('O1', 'Rebate 1')
			->setCellValue('P1', 'Rebate 2')
            ->setCellValue('Q1', 'Rebate 3')
            ->setCellValue('R1', 'Netto SALES')
            ->setCellValue('S1', 'Sc clienti')
            ->setCellValue('T1', 'Web')
            ->setCellValue('U1', (iconv("ISO-8859-1", "UTF-8", 'Marg')))
            ->setCellValue('V1', (iconv("ISO-8859-1", "UTF-8", 'Sc qt 1')))
            ->setCellValue('W1', (iconv("ISO-8859-1", "UTF-8", 'Sc qt 2')))
            ->setCellValue('X1', (iconv("ISO-8859-1", "UTF-8", 'Sc qt 3')))
            ->setCellValue('Y1', 'Sc riv 1')
            ->setCellValue('Z1', 'Premio Ag. Personalizzato')
            ->setCellValue('AA1', 'Fornitore')
            ->setCellValue('AB1', 'Qt. tot.')
            ->setCellValue('AC1', 'Categoria prestashop')
            ->setCellValue('AD1', 'Peso confezione')
            ->setCellValue('AE1', 'Data inserimento')
            ->setCellValue('AF1', 'Descrizione HTML')
            ->setCellValue('AG1', 'Descrizione breve (listing)')
            ->setCellValue('AH1', 'Descrizione Amazon')
            ->setCellValue('AI1', 'Title tag')
            ->setCellValue('AJ1', 'Metatag description')
            ->setCellValue('AK1', 'Keywords')
            ->setCellValue('AL1', 'Categoria home page')
            ->setCellValue('AM1', 'Descrizione home page')
            ->setCellValue('AN1', 'Noindex')
            ->setCellValue('AO1', 'Trovaprezzi')
            ->setCellValue('AP1', 'ePrice')
            ->setCellValue('AQ1', '')
            ->setCellValue('AR1', 'Google Shopping')
            ->setCellValue('AS1', 'Messaggio disponibile')
            ->setCellValue('AT1', 'Messaggio non disponibile')
            ->setCellValue('AU1', '')
            ->setCellValue('AV1', '')
            ->setCellValue('AW1', (iconv("ISO-8859-1", "UTF-8", 'Data disponibilit�')))
            ->setCellValue('AX1', 'Amazon IT')
            ->setCellValue('AY1', 'Amazon FR')
			->setCellValue('AZ1', 'Amazon ES')
			->setCellValue('BA1', 'Amazon DE')
			->setCellValue('BB1', 'Amazon UK')
			->setCellValue('BC1', 'Amazon NL')
			->setCellValue('BD1', '')
			->setCellValue('BE1', '')
			->setCellValue('BF1', '')
			->setCellValue('BG1', '')
			->setCellValue('BH1', '')
			->setCellValue('BI1', '')
			->setCellValue('BJ1', 'Nuovo/Eco')
			->setCellValue('BK1', '')
			->setCellValue('BL1', '')
			->setCellValue('BM1', '')
			->setCellValue('BN1', '')
			->setCellValue('BO1', 'Attivo (1)')
			->setCellValue('BP1', 'Prova gratuita')
			->setCellValue('BQ1', 'Trasporto gratis')
			->setCellValue('BR1', 'Immagine listing prodotti')
			->setCellValue('BS1', '')
			->setCellValue('BT1', '')
			->setCellValue('BU1', '')
			->setCellValue('BV1', '')
			->setCellValue('BW1', '')
			->setCellValue('BX1', '')
			->setCellValue('BY1', '')
			->setCellValue('BZ1', '')
			->setCellValue('CA1', '')
			->setCellValue('CB1', '')
			->setCellValue('CC1', '')
			->setCellValue('CD1', '')
			->setCellValue('CE1', '')
			->setCellValue('CF1', '')
			->setCellValue('CG1', 'Redirect')
			->setCellValue('CH1', 'Punto di forza')
			->setCellValue('CI1', 'Garanzia')
			->setCellValue('CJ1', 'Confezione')
			->setCellValue('CK1', 'Tipo')
			->setCellValue('CL1', 'Utilizzo')
			->setCellValue('CM1', '')
			->setCellValue('CN1', 'Display')
			->setCellValue('CO1', 'Identificazione chiamante')
			->setCellValue('CP1', 'Touchscreen')
			->setCellValue("CQ1", '')
			->setCellValue('CR1', 'Rubrica')
			->setCellValue('CS1', 'Bluetooth integrato')
			->setCellValue('CT1', 'Connessione')
			->setCellValue("CU1", 'Tecnologia collegamento')
			->setCellValue('CV1', 'Collegamento a centralino')
			->setCellValue('CW1', 'Protocolli gestiti')
			->setCellValue('CX1', 'Porte fxs')
			->setCellValue('CY1', 'Porte fxo')
			->setCellValue('CZ1', 'Porta RS 232 dati')
			->setCellValue('DA1', 'Porta USB per collegamento PC')
			->setCellValue('DB1', 'USB')
			->setCellValue('DC1', 'Connettori')
			->setCellValue('DD1', (iconv("ISO-8859-1", "UTF-8", 'Quantit� Sim gestite')))
			->setCellValue('DE1', 'Account VoIP gestiti')
			->setCellValue('DF1', 'Codec')
			->setCellValue('DG1', 'PoE')
			->setCellValue('DH1', (iconv("ISO-8859-1", "UTF-8", 'Quantit� porte ethernet')))
			->setCellValue('DI1', 'Wi-fi incluso')
			->setCellValue('DJ1', 'SAR')
			->setCellValue('DK1', 'Frequenze')
			->setCellValue('DL1', 'Copertura (mq)')
			->setCellValue('DM1', 'Guadagno dBm')
			->setCellValue('DN1', 'Potenza in uscita dB')
			->setCellValue('DO1', 'Led segnalazione')
			->setCellValue('DP1', 'Antenna')
			->setCellValue('DQ1', 'Gestione fax diretto')
			->setCellValue('DR1', 'Servizio DISA')
			->setCellValue('DS1', 'Gestione fax su PC')
			->setCellValue('DT1', 'SMS')
			->setCellValue('DU1', 'Sensori')
			->setCellValue('DV1', (iconv("ISO-8859-1", "UTF-8", 'Rel�')))
			->setCellValue('DW1', 'Call back')
			->setCellValue('DX1', 'Avviso fine credito')
			->setCellValue('DY1', 'Compatibile Skype')
			->setCellValue('DZ1', 'Android')
			->setCellValue('EA1', 'Vibrazione')
			->setCellValue('EB1', 'GPS')
			->setCellValue('EC1', 'Radio FM')
			->setCellValue('ED1', 'Player multimediale')
			->setCellValue('EE1', 'Fotocamera frontale')
			->setCellValue('EF1', 'Fotocamera posteriore')
			->setCellValue('EG1', 'Atex')
			->setCellValue('EH1', '')
			->setCellValue('EI1', 'Man down')
			->setCellValue('EJ1', 'NFC')
			->setCellValue('EK1', 'AGC')
			->setCellValue('EL1', '')
			->setCellValue('EM1', 'Attacco cuffia')
			->setCellValue('EN1', '')
			->setCellValue('EO1', 'Autonomia in conversazione')
			->setCellValue('EP1', 'Autonomia in standby')
			->setCellValue('EQ1', 'Consumo (watt)')
			->setCellValue('ER1', '')
			->setCellValue('ES1', '')
			->setCellValue('ET1', 'Peso')
			->setCellValue('EU1', 'Dimensioni')		
			->setCellValue('EV1', 'Colore')
			
			
			;



$objPHPExcel->getActiveSheet()->getStyle("B")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("C")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("D")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("E")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("G")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("J")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("K")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("L")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("M")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("N")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("O")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("P")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("Q")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("R")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("S")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("T")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("U")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("Y")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("Z")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("W")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
///////////////// campi specifici categoria
$objPHPExcel->getActiveSheet()->getStyle("DE")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("DH")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("DX")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
///////////////// fine campi specifici categoria


//manuali e immagini

$objPHPExcel->getActiveSheet()->getColumnDimension('BR')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BS')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BT')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BU')->setVisible(false);
//fine manuali e immagini


$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setVisible(false);

$objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setVisible(false);

$objPHPExcel->getActiveSheet()->getColumnDimension('BD')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BE')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BF')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BG')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BH')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BI')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BK')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BL')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BM')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BN')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BU')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BW')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BX')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BY')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BZ')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CA')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CB')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CC')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CD')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CE')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CF')->setVisible(false);

$objPHPExcel->getActiveSheet()->getColumnDimension('CM')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CQ')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('EH')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('ER')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('EN')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('ES')->setVisible(false);

if(Tools::getIsset('senza_seo'))
{
	$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setVisible(false);
	
}


// QUERY PER TUTTE LE CATEGORIE

$query1 = "SELECT category_lang.id_category, category_lang.name FROM category_lang JOIN product ON category_lang.id_category = product.id_category_default WHERE id_lang = 5";
$result1 = mysql_query($query1, $link_global);
$string_cats = '';
while ($row1 = mysql_fetch_array($result1, MYSQL_ASSOC)) {
$string_cats .= $row1['name'].",";
} 


while($row = mysql_fetch_assoc($result)) {

$objPHPExcel->getActiveSheet()->getCell("B$i")->setValueExplicit($row['codice_costruttore'], PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("C$i")->setValueExplicit($row['codice_prodotto'], PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("D$i")->setValueExplicit($row['codice_ean'], PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("E$i")->setValueExplicit($row['asin'], PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("AB$i")->setValueExplicit($row['quantita'], PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("AD$i")->setValueExplicit($row['peso'], PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("DE$i")->setValueExplicit('', PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("DH$i")->setValueExplicit('', PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("DX$i")->setValueExplicit('', PHPExcel_Cell_DataType::TYPE_STRING);





if(Tools::getIsset('senza_seo'))
{
	$descrizione = ''; $descrizione_breve = ''; $row['descrizione_amazon'] = ''; $title = ''; $metatag_description = ''; $metatag_keywords = ''; $row['cat_homepage'] = ''; $row['desc_homepage'] = '';
	
}

else
{

	$descrizione = $row['descrizione'];
	$descrizione_breve = $row['descrizione_breve'];
	$title = $row['title_tag'];
	$metatag_description = $row['metatag_description'];
	$metatag_keywords = $row['metatag_keywords'];

	$descrizione = preg_replace("/<br>/","<br />",$row['descrizione']);
	$descrizione = preg_replace("/<b>/","<strong>",$descrizione);
	$descrizione = preg_replace("/<\/b>/","</strong>",$descrizione);
	$descrizione = preg_replace("/<i>/","<em>",$descrizione);
	$descrizione = preg_replace("/<\/i>/","</em>",$descrizione);

	// correzioni ortografiche
	$descrizione = preg_replace("/epr/","per",$descrizione);
	$descrizione_breve = preg_replace("/epr/","per",$descrizione_breve);
	$title = preg_replace("/epr/","per",$title);
	$metatag_description = preg_replace("/epr/","per",$metatag_description);
	$metatag_keywords = preg_replace("/epr/","per",$metatag_keywords);

	$descrizione = preg_replace("/teelfon/","telefon",$descrizione);
	$descrizione_breve = preg_replace("/teelfon/","telefon",$descrizione_breve);
	$title = preg_replace("/teelfon/","telefon",$title);
	$metatag_description = preg_replace("/teelfon/","telefon",$metatag_description);
	$metatag_keywords = preg_replace("/teelfon/","telefon",$metatag_keywords);

	$descrizione = preg_replace("/ms link/","MS Lync",$descrizione);
	$descrizione_breve = preg_replace("/ms link/","MS Lync",$descrizione_breve);
	$title = preg_replace("/ms link/","MS Lync",$title);
	$metatag_description = preg_replace("/ms link/","MS Lync",$metatag_description);
	$metatag_keywords = preg_replace("/ms link/","MS Lync",$metatag_keywords);

	$descrizione = preg_replace("/ms linc/","MS Lync",$descrizione);
	$descrizione_breve = preg_replace("/ms linc/","MS Lync",$descrizione_breve);
	$title = preg_replace("/ms linc/","MS Lync",$title);
	$metatag_description = preg_replace("/ms linc/","MS Lync",$metatag_description);
	$metatag_keywords = preg_replace("/ms linc/","MS Lync",$metatag_keywords);

	$descrizione = preg_replace("/ms lynk/","MS Lync",$descrizione);
	$descrizione_breve = preg_replace("/ms lynk/","MS Lync",$descrizione_breve);
	$title = preg_replace("/ms lynk/","MS Lync",$title);
	$metatag_description = preg_replace("/ms lynk/","MS Lync",$metatag_description);
	$metatag_keywords = preg_replace("/ms lynk/","MS Lync",$metatag_keywords);

	$descrizione = preg_replace("/plantroncis/","Plantronics",$descrizione);
	$descrizione_breve = preg_replace("/plantroncis/","Plantronics",$descrizione_breve);
	$title = preg_replace("/plantroncis/","Plantronics",$title);
	$metatag_description = preg_replace("/plantroncis/","Plantronics",$metatag_description);
	$metatag_keywords = preg_replace("/plantroncis/","Plantronics",$metatag_keywords);

	$descrizione = preg_replace("/Plantroncis/","Plantronics",$descrizione);
	$descrizione_breve = preg_replace("/Plantroncis/","Plantronics",$descrizione_breve);
	$title = preg_replace("/Plantroncis/","Plantronics",$title);
	$metatag_description = preg_replace("/Plantroncis/","Plantronics",$metatag_description);
	$metatag_keywords = preg_replace("/Plantroncis/","Plantronics",$metatag_keywords);

	//correzioni ortografiche

	$title = ucfirst($title);
	$metatag_description = ucfirst($metatag_description);
	$metatag_keywords = ucfirst($metatag_keywords);

	if(($metatag_description[strlen($metatag_description)-1]) != ".") { $metatag_description = $metatag_description."."; } else { }



	if(!preg_match("/<h1*./",$descrizione)) {
	$descrizione = "<h1>".$row['nome_prodotto']."</h1> ".$descrizione;
	}
}

if($row['prezzo_acquisto'] == 0 || $row['prezzo_acquisto'] == '' || !isset($row['prezzo_acquisto'])) {
$netto = 0;
}
else {
$netto = $row['prezzo_acquisto'];
}

if($row['scontolistinovendita'] == 0 && $row['prezzo_listino'] != $row['prezzo_ezdirect'])
{
	$row['scontolistinovendita'] = ((($row['prezzo_listino'] - $row['prezzo_ezdirect']) * 100)/$row['prezzo_listino']);
}	

if($row['rebate_1'] == '')
	$row['rebate_1'] = 0;

if($row['rebate_2'] == '')
	$row['rebate_2'] = 0;

if($row['rebate_3'] == '')
	$row['rebate_3'] = 0;

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A$i", $row['id_prestashop'])
			->setCellValue("F$i", '')
			->setCellValue("G$i", $row['data_ultima_modifica'])
			->setCellValue("H$i", (iconv("ISO-8859-1", "UTF-8",$row['costruttore'])))
			->setCellValue("I$i", (iconv("ISO-8859-1", "UTF-8",$row['nome_prodotto'])))
			->setCellValue("J$i", number_format($row['prezzo_listino'], 2, ',', ''))
			->setCellValue("K$i", $row['sconto_acquisto_1'])
			->setCellValue("L$i", $row['sconto_acquisto_2'])
			->setCellValue("M$i", $row['sconto_acquisto_3'])
			->setCellValue("N$i", "=ROUND((IF(K$i>=0;(J$i*((100-K$i)/100)*((100-L$i)/100)*((100-M$i)/100));$netto)),2)")
			->setCellValue("O$i", $row['rebate_1'])
			->setCellValue("P$i", $row['rebate_2'])
			->setCellValue("Q$i", $row['rebate_3'])
			->setCellValue("R$i", "=ROUND((IF(K$i>=0;(J$i*((100-K$i)/100)*((100-L$i)/100)*((100-M$i)/100)*((100-O$i)/100)*((100-P$i)/100)*((100-Q$i)/100));$netto)),2)")
            ->setCellValue("S$i", number_format($row['scontolistinovendita'], 2, ',' ,''))
            ->setCellValue("T$i", "=ROUND((IF(S$i>=0;J$i-(J$i*S$i/100);".number_format($row['prezzo_ezdirect'], 2, '.', '').")),2)")
            ->setCellValue("U$i",  "=ROUND((((T$i-R$i)*100)/T$i),2)")
            ->setCellValue("V$i", '')
            ->setCellValue("W$i", '')
			->setCellValue("X$i", '')
->setCellValue("Z$i", $row['provvigione'])
            ->setCellValue("AA$i", $row['fornitore'])
            ->setCellValue("AE$i", $row['data_inserimento'])
            
			->setCellValue("AF$i", (iconv("ISO-8859-1", "UTF-8",$descrizione)))
             ->setCellValue("AG$i", (iconv("ISO-8859-1", "UTF-8",$descrizione_breve)))
            ->setCellValue("AH$i", (iconv("ISO-8859-1", "UTF-8",$row['descrizione_amazon'])))
            ->setCellValue("AI$i", (iconv("ISO-8859-1", "UTF-8",$title)))
           ->setCellValue("AJ$i", (iconv("ISO-8859-1", "UTF-8",$metatag_description)))
            ->setCellValue("AK$i", (iconv("ISO-8859-1", "UTF-8",$metatag_keywords)))
            ->setCellValue("AL$i", (iconv("ISO-8859-1", "UTF-8",$row['cat_homepage'])))
             ->setCellValue("AM$i", (iconv("ISO-8859-1", "UTF-8",$row['desc_homepage'])))
            ->setCellValue("AN$i", '')
            ->setCellValue("AO$i", '')
            ->setCellValue("AP$i", '')
            ->setCellValue("AQ$i", '')
            ->setCellValue("AR$i", '')
            ->setCellValue("AS$i", $row['msg_disponibile'])
            ->setCellValue("AT$i", $row['msg_non_disponibile'])
            ->setCellValue("AU$i", '')
            ->setCellValue("AV$i", '')
            ->setCellValue("AW$i", $row['data_disponibilita'])
            ->setCellValue("AX$i", '')
            ->setCellValue("AY$i", '')
			->setCellValue("AZ$i", '')
			->setCellValue("BA$i", '')
			->setCellValue("BB$i", '')
			->setCellValue("BC$i", '')
			->setCellValue("BD$i", '')
			->setCellValue("BE$i", '')
			->setCellValue("BF$i", '')
			->setCellValue("BG$i", '')
			->setCellValue("BH$i", '')
			->setCellValue("BI$i", '')
			->setCellValue("BJ$i", $row['nuovo_eco'])
			->setCellValue("BK$i", '')
			->setCellValue("BL$i", '')
			->setCellValue("BM$i", '')
			->setCellValue("BN$i", '')
			->setCellValue("BO$i", $row['stato'])
			->setCellValue("BP$i", $row['prova_gratuita'])
			->setCellValue("BQ$i", $row['trasporto_gratuito'])
			->setCellValue("BR$i", '')
			->setCellValue("BS$i", '')
			->setCellValue("BT$i", '')
			->setCellValue("BU$i", '')
			->setCellValue("BV$i", '')
			->setCellValue("BW$i", '')
			->setCellValue("BX$i", $row['canonical'])
			->setCellValue("BY$i", '')
			->setCellValue("BZ$i", '')
			->setCellValue("CA$i", '')
			->setCellValue("CB$i", '')
			->setCellValue("CC$i", '')
			->setCellValue("CD$i", '')
			->setCellValue("CE$i", '')
			->setCellValue("CF$i", '')
			->setCellValue("CG$i", $row['redirect'])
			->setCellValue("CH$i", str_replace(":::::",";",$row['punti_di_forza']))
			->setCellValue("CI$i", '')
			->setCellValue("CJ$i", '')
			->setCellValue("CK$i", '')
			->setCellValue("CL$i", '')
			->setCellValue("CM$i", '')
			->setCellValue("CN$i", '')
			->setCellValue("CO$i", '')
			->setCellValue("CP$i", '')
			->setCellValue("CQ$i", '')
			->setCellValue("CR$i", '')
			->setCellValue("CS$i", '')
			->setCellValue("CT$i", '')
			->setCellValue("CU$i", '')
			->setCellValue("CV$i", '')
			->setCellValue("CW$i", '')
			->setCellValue("CX$i", '')
			->setCellValue("CY$i", '')
			->setCellValue("CZ$i", '')
			->setCellValue("DA$i", '')
			->setCellValue("DB$i", '')
			->setCellValue("DC$i", '')
			->setCellValue("DD$i", '')
			->setCellValue("DF$i", '')
			->setCellValue("DG$i", '')
			->setCellValue("DI$i", "")
			->setCellValue("DJ$i", "")
			->setCellValue("DK$i", "")
			->setCellValue("DL$i", "")
			->setCellValue("DM$i", "")
			->setCellValue("DN$i", "")
			->setCellValue("DO$i", "")
			->setCellValue("DP$i", "")
			->setCellValue("DQ$i", "")
			->setCellValue("DR$i", "")
			->setCellValue("DS$i", "")
			->setCellValue("DT$i", "")
			->setCellValue("DU$i", "")
			->setCellValue("DV$i", "")
			->setCellValue("DW$i", "")
			->setCellValue("DX$i", "")
			->setCellValue("DY$i", "")
			->setCellValue("DZ$i", "")
			->setCellValue("EA$i", "")
			->setCellValue("EB$i", "")
			->setCellValue("EC$i", "")
			->setCellValue("ED$i", "")
			->setCellValue("EE$i", "")
			->setCellValue("EF$i", "")
			->setCellValue("EG$i", "")
			->setCellValue("EH$i", "")
			->setCellValue("EI$i", "")
			->setCellValue("EJ$i", "")
			->setCellValue("EK$i", "")
			->setCellValue("EL$i", "")
			->setCellValue("EM$i", "")
			->setCellValue("EN$i", "")
			->setCellValue("EO$i", "")
			->setCellValue("EP$i", "")
			->setCellValue("EQ$i", "")
			->setCellValue("ER$i", "")
			->setCellValue("ES$i", "")
			->setCellValue("ET$i", "")
			->setCellValue("EU$i", "")
			->setCellValue("EV$i", "")
			;

// CATEGORIE

$query1 = "SELECT category_lang.id_category, category_lang.name FROM category_lang JOIN product ON category_lang.id_category = product.id_category_default WHERE category_lang.id_lang = 5 AND product.id_product = $row[id_prestashop]";
$result1 = mysql_query($query1, $link_global);
while ($row1 = mysql_fetch_array($result1, MYSQL_ASSOC)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("AC$i", $row1['name']);
} 
// NOINDEX

if($row['noindex'] == 1) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("AN$i", (iconv("ISO-8859-1", "UTF-8", 'si')));
} else { }

//COMPARAPREZZI

if($row['trovaprezzi'] == 1) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("AO$i", (iconv("ISO-8859-1", "UTF-8", 's�')));
} else { }

if($row['eprice'] == 1) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("AP$i", (iconv("ISO-8859-1", "UTF-8", 's�')));
} else { }

$amazon_check = explode(";",$row['amazon']);

if(in_array("5",$amazon_check))
{
	$objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("AX$i", (iconv("ISO-8859-1", "UTF-8", 'si')));	
}

if(in_array("2",$amazon_check))
{
	$objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("AY$i", (iconv("ISO-8859-1", "UTF-8", 'si')));	
}

if(in_array("3",$amazon_check))
{
	$objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("AZ$i", (iconv("ISO-8859-1", "UTF-8", 'si')));	
}

if(in_array("4",$amazon_check))
{
	$objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("BA$i", (iconv("ISO-8859-1", "UTF-8", 'si')));	
}

if(in_array("1",$amazon_check))
{
	$objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("BB$i", (iconv("ISO-8859-1", "UTF-8", 'si')));	
}
if(in_array("6",$amazon_check))
{
	$objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("BC$i", (iconv("ISO-8859-1", "UTF-8", 'si')));	
}
if($row['google_shopping'] == 1) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("AR$i", (iconv("ISO-8859-1", "UTF-8", 's�')));
} else { }


// SCONTI QUANTITA

$query1 = "SELECT * FROM specific_price WHERE id_product = $row[id_prestashop] AND (specific_price_name = 'sc_qta_1' OR specific_price_name = 'sc_qta_2' OR specific_price_name = 'sc_qta_3') AND id_group = 1 ORDER BY specific_price_name ASC";
$result1 = mysql_query($query1, $link_global);
$cella[0] = "V";
$cella[1] = "W";
$cella[2] = "X";
$k = 0;
while ($row1 = mysql_fetch_array($result1, MYSQL_ASSOC)) {
$objPHPExcel->setActiveSheetIndex(0)
             ->setCellValue($cella[$k]."$i", $row1['from_quantity']."_".$row1['reduction']*100);
			$k++;
}

// SCONTI RIVENDITORI

$query1 = "SELECT * FROM specific_price WHERE id_product = $row[id_prestashop] AND (specific_price_name = 'sc_riv_1' OR specific_price_name = 'sc_riv_2' OR specific_price_name = 'sc_riv_3') ORDER BY specific_price_name ASC";
$result1 = mysql_query($query1, $link_global);
$cella[0] = "Y";

$k = 0;
while ($row1 = mysql_fetch_array($result1, MYSQL_ASSOC)) {
$objPHPExcel->setActiveSheetIndex(0)
             ->setCellValue($cella[$k]."$i", $row1['reduction']*100);
			$k++;
}


			
										// TIPO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CK$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 799", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CK$i")->setDataValidation($objValidation);
	
							// UTILIZZO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CL$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 800", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CL$i")->setDataValidation($objValidation);
	
	
			
						// AGGIORNATO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("F$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8", "s�")).',no, "');
	$objPHPExcel->getActiveSheet()->getCell("F$i")->setDataValidation($objValidation);
	

	
		// Garanzia
$objValidation = $objPHPExcel->getActiveSheet()->getCell("AV$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"12 mesi, 24 mesi, 36 mesi"');
	$objPHPExcel->getActiveSheet()->getCell("CI$i")->setDataValidation($objValidation);

	
		// NUOVO ECO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("BJ$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"new,used,refurbished"');
	$objPHPExcel->getActiveSheet()->getCell("BJ$i")->setDataValidation($objValidation);
	
	// HOME PAGE
$objValidation = $objPHPExcel->getActiveSheet()->getCell("BE$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8", "s�")).',no"');
	$objPHPExcel->getActiveSheet()->getCell("BE$i")->setDataValidation($objValidation);
	
	
		// PROVA GRATUITA
$objValidation = $objPHPExcel->getActiveSheet()->getCell("BP$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8", "s�")).',no"');
	$objPHPExcel->getActiveSheet()->getCell("BP$i")->setDataValidation($objValidation);
	
	
		// TRASP GRATUITO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("BQ$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8", "s�")).',no"');
	$objPHPExcel->getActiveSheet()->getCell("BQ$i")->setDataValidation($objValidation);
	
	
		// MOSTRA TRASP GRATUITO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CA$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8", "s�")).',no"');
	$objPHPExcel->getActiveSheet()->getCell("CA$i")->setDataValidation($objValidation);
	

	// AGGIORNATO
	$query1 = "SELECT aggiornato FROM product_other WHERE id_product = '$row[id_prestashop]'";
	$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("F$i", (iconv("ISO-8859-1", "UTF-8", $row1['aggiornato'])));
}
	
	
	// NUOVO ECO
	$query1 = "SELECT condition FROM product WHERE id_product = '$row[id_prestashop]'";
	$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("BJ$i", (iconv("ISO-8859-1", "UTF-8", $row1['nuovo_eco'])));
}

	// PROVA GRATUITA
	$query1 = "SELECT prova_gratuita FROM product_other WHERE id_product = '$row[id_prestashop]'";
	$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("BP$i", (iconv("ISO-8859-1", "UTF-8", $row1['prova_gratuita'])));
}
	
	

$rowtrasportogratuito = Db::getInstance()->getValue("SELECT value FROM configuration WHERE id_configuration = 240");

	$artrasportogratuito = unserialize($rowtrasportogratuito);
	if(in_array($row['id_prestashop'], $artrasportogratuito))
		$trasp_gratis = 's�';
	else
		$trasp_gratis = '';
		
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue("BQ$i", (iconv("ISO-8859-1", "UTF-8", $trasp_gratis)));
	
	
		// MOSTRA TRASP GRATUITO
	$query1 = "SELECT mostra_spese_spedizione FROM product_other WHERE id_product = '$row[id_prestashop]'";
	$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CA$i", (iconv("ISO-8859-1", "UTF-8", $row1['mostra_spese_spedizione'])));
}

		// Tipo
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '799' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CK$i", $row1['valore']);
}

	// Utilizzo
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '800' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CL$i", $row1['valore']);
}
	// Confezione
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '450' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CJ$i", $row1['valore']);
}

// HOME PAGE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '452' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("BE$i", $row1['valore']);
}
	
	
// GARANZIA
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '449' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CI$i", $row1['valore']);
}



	
	
		
				// display
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CN$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 460", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CN$i")->setDataValidation($objValidation);
			
	
		
				// identificativo chiamante
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CO$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 523", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CO$i")->setDataValidation($objValidation);
			
							// touchscreen
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CP$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 468", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CP$i")->setDataValidation($objValidation);
	
		// rubrica memorie
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CR$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 513", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CR$i")->setDataValidation($objValidation);
	
	
				// bluetooth integrato
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CS$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 491", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CS$i")->setDataValidation($objValidation);
	
	
		
				// CONNESSIONE
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CT$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 797", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CT$i")->setDataValidation($objValidation);
			
		
				// TECNOLOGIA COLLEGAMENTO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CU$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 798", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CU$i")->setDataValidation($objValidation);
			
	
		
				// COLLEGAMENTO A CENTRALINO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CV$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 680", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CV$i")->setDataValidation($objValidation);
			
	
	
		
				// PORTE FXS
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CX$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 682", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CX$i")->setDataValidation($objValidation);
			
	
	
		
				// PORTE FXO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CY$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 683", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CY$i")->setDataValidation($objValidation);
			
		
				// RS232
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CZ$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 615", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CZ$i")->setDataValidation($objValidation);
			
	
					// PORTA USB PER COLLEGAMENTO PC
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DA$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 533", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DA$i")->setDataValidation($objValidation);
	
					// USB
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DB$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 495", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DB$i")->setDataValidation($objValidation);
	
	// CONNETTORI
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DC$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 922", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DC$i")->setDataValidation($objValidation);
		
				// QUANTITA SIM GESTITE
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DD$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 681", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DD$i")->setDataValidation($objValidation);
			
				// ACCOUNT VOIP GESTITI
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DE$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 482", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DE$i")->setDataValidation($objValidation);
	
	
					// POE
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DG$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 486", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DG$i")->setDataValidation($objValidation);
			
							// WIFI
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DI$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 493", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DI$i")->setDataValidation($objValidation);
	
	// FREQUENZE
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DK$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 921", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DK$i")->setDataValidation($objValidation);
	
	// COPERTURA
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DL$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 918", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DL$i")->setDataValidation($objValidation);
	
	// GUADAGNO
	$objValidation = $objPHPExcel->getActiveSheet()->getCell("DM$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 919", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DM$i")->setDataValidation($objValidation);
			
			// POTENZA USCITA
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DN$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 920", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DN$i")->setDataValidation($objValidation);
	
	
				// LED SEGNALAZIONE
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DO$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 684", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DO$i")->setDataValidation($objValidation);
			
				// ANTENNA
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DP$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 685", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DP$i")->setDataValidation($objValidation);
			
	
				// GESTIONE FAX DIRETTO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DQ$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 686", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DQ$i")->setDataValidation($objValidation);
			
				// SERVIZIO DISA
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DR$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 687", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DR$i")->setDataValidation($objValidation);
			
	
				// GESTIONE FAX SU PC
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DS$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 717", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DS$i")->setDataValidation($objValidation);
			
				// SMS
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DT$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 524", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DT$i")->setDataValidation($objValidation);
			
	
				// SENSORI
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DU$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 688", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DU$i")->setDataValidation($objValidation);
			
	
		
				// RELE
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DV$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 689", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DV$i")->setDataValidation($objValidation);
			
	
		
				// CALLBACK
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DW$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 690", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DW$i")->setDataValidation($objValidation);
			
		
				// AVVISO FINE CREDITO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DX$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 691", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DX$i")->setDataValidation($objValidation);
			
		// COMPATIBILE SKYPE
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DY$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 771", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DY$i")->setDataValidation($objValidation);
	
	
					// ANDROID
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DZ$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 564", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DZ$i")->setDataValidation($objValidation);
	
	
	// Vibrazione
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EA$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 535", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EA$i")->setDataValidation($objValidation);
	
	// GPS
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EB$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 871", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EB$i")->setDataValidation($objValidation);
	
	
	// Radio FM
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EC$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 823", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EC$i")->setDataValidation($objValidation);
	
	// PLAYER
$objValidation = $objPHPExcel->getActiveSheet()->getCell("ED$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 565", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("ED$i")->setDataValidation($objValidation);
	
	// FOTOCAMERA FRONTALE
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EE$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 872", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EE$i")->setDataValidation($objValidation);
	
	// FOTOCAMERA POSTERIORE
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EF$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 873", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EF$i")->setDataValidation($objValidation);
	
	// ATEX
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EG$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 830", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EG$i")->setDataValidation($objValidation);
	

	// MAN DOWN
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EI$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 876", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EI$i")->setDataValidation($objValidation);	
	
		// NFC
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EJ$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 889", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EJ$i")->setDataValidation($objValidation);	
	
	// AGC
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EK$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 923", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EK$i")->setDataValidation($objValidation);
	
					// ATTACCO CUFFIA
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EM$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 504", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EM$i")->setDataValidation($objValidation);
	
	
	
	
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////// SPECIFICHE//////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	
// DISPLAY
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '460' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CN$i", $row1['valore']);
}


// ID CHIAMANTE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '523' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CO$i", $row1['valore']);
}

// TOUCHSCREEN
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '468' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CP$i", $row1['valore']);
}

// RUBRICA
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '513' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CR$i", $row1['valore']);
}

//BLUETOOTH INTEGRATO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '491' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CS$i", $row1['valore']);
}

//CONNESSIONE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '797' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CT$i", $row1['valore']);
}

// TECNOLOGIA COLLEGAMENTO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '798' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CU$i", $row1['valore']);
}

// COLLEGAMENTO A CENTRALINO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '680' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CV$i", $row1['valore']);
}

// PROTOCOLLI GESTITI
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '480' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CW$i", $row1['valore']);
}

// PORTE FXS
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '682' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CX$i", $row1['valore']);
}

// PORTE FXO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '683' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CY$i", $row1['valore']);
}

// PORTA RS232
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '615' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CZ$i", $row1['valore']);
}

// PORTA USB PER COLLEGAMENTO PC
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '533' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DA$i", $row1['valore']);
}

//USB
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '495' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DB$i", $row1['valore']);
}

//CONNETTORI
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '922' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DC$i", $row1['valore']);
}


// QUANTITA SIM GESTITE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '681' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DD$i", $row1['valore']);
}

// ACCOUNT  VOIP GESTITI
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '482' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DE$i", $row1['valore']);
}



// CODEC
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '481' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DF$i", $row1['valore']);
}



// POE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '486' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DG$i", $row1['valore']);
}

// QUANTITA PORTE ETHERNET 
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '484' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DH$i", $row1['valore']);
}


// WIFI INCLUSO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '493' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DI$i", $row1['valore']);
}

// SAR
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '875' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DJ$i", $row1['valore']);
}

//FREQUENZE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '921' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DK$i", $row1['valore']);
}

//COPERTURA
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '918' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DL$i", $row1['valore']);
}

//GUADAGNO DBM
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '919' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DM$i", $row1['valore']);
}

//POTENZA USCITA DB
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '920' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DN$i", $row1['valore']);
}


// LED SEGNALAZIONE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '684' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DO$i", $row1['valore']);
}

// ANTENNA
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '685' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DP$i", $row1['valore']);
}

// GESTIONE FAX DIRETTO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '686' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DQ$i", $row1['valore']);
}

// SERVIZIO DISA
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '687' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DR$i", $row1['valore']);
}


// GESTIONE FAX SU PC
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '717' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DS$i", $row1['valore']);
}

// SMS
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '524' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DT$i", $row1['valore']);
}

// SENSORI
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '688' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DU$i", $row1['valore']);
}

// RELE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '689' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DV$i", $row1['valore']);
}

// CALL BACK
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '690' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DW$i", $row1['valore']);
}

//AVVISO FINE CREDITO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '691' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DX$i", $row1['valore']);
}

// COMPATIBILE SKYPE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '771' AND feature_product.id_product = '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DY$i", $row1['valore']);
}

//ANDROID
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '564' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DZ$i", $row1['valore']);
}

// VIBRAZIONE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '535' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EA$i", $row1['valore']);
}

// GPS
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '871' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EB$i", $row1['valore']);
}

//RADIO FM
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '823' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EC$i", $row1['valore']);
}



// PLAYER MULTIMEDIALE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '565' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("ED$i", $row1['valore']);
}

// FOTOCAMERA FRONTALE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '872' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EE$i", $row1['valore']);
}

// FOTOCAMERA POSTERIORE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '873' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EF$i", $row1['valore']);
}

// ATEX
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '830' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EG$i", $row1['valore']);
}


// MAN DOWN
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '876' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EI$i", $row1['valore']);
}

// NFC
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '889' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EJ$i", $row1['valore']);
}

//AGC
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '923' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EK$i", $row1['valore']);
}

//ATTACCO CUFFIA
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '504' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EM$i", $row1['valore']);
}


//AUTONOMIA IN CONVERSAZIONE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '455' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EO$i", $row1['valore']);
}

//AUTONOMIA IN CONVERSAZIONE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '456' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EP$i", $row1['valore']);
}

//CONSUMO WATT
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '562' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EQ$i", $row1['valore']);
}

// PESO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '559' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("ET$i", $row1['valore']);
}

// DIMENSIONI
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '560' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EU$i", $row1['valore']);
}

// COOLORE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '453' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EV$i", $row1['valore']);
}


	////////////////////////////////////////////////////////////////////////////////////////////////// SPECIFICHE//////////////////////////////////////////////









$i++; // INCREMENTO ----------------------------------------------------------------------------------------------------------------------------------------



}





			
$highestRow = $objPHPExcel->getActiveSHeet()->getHighestRow();
$objPHPExcel->getActiveSheet()->getStyle('A1:ER1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);			
$objPHPExcel->getActiveSheet()->getStyle('A1:CM1')->getFill()->getStartColor()->setRGB('FFFF00');
$objPHPExcel->getActiveSheet()->getStyle('O1:Q1')->getFill()->getStartColor()->setRGB('FFFFFF'); //BIANCO
$objPHPExcel->getActiveSheet()->getStyle('R1:Y1')->getFill()->getStartColor()->setRGB('99FFFF'); //azzurro
$objPHPExcel->getActiveSheet()->getStyle('CN1:CP1')->getFill()->getStartColor()->setRGB('99CC99');	 // verde
$objPHPExcel->getActiveSheet()->getStyle('CR1:CR1')->getFill()->getStartColor()->setRGB('99FFFF'); // azzurro
$objPHPExcel->getActiveSheet()->getStyle('CS1:DC1')->getFill()->getStartColor()->setRGB('FF99FF'); //lilla 5 
$objPHPExcel->getActiveSheet()->getStyle('DD1:DN1')->getFill()->getStartColor()->setRGB('CCCCCC'); //grigio 6
$objPHPExcel->getActiveSheet()->getStyle('DO1:EL1')->getFill()->getStartColor()->setRGB('FFCC66'); //ocra 7
$objPHPExcel->getActiveSheet()->getStyle('EM1:EN1')->getFill()->getStartColor()->setRGB('6666FF'); //BLU 1
$objPHPExcel->getActiveSheet()->getStyle('EO1:EQ1')->getFill()->getStartColor()->setRGB('FF3333'); //ROSSO 10
$objPHPExcel->getActiveSheet()->getStyle('ER1:EV1')->getFill()->getStartColor()->setRGB('FF66CC'); //FUCSIA 11


	
$objPHPExcel->getActiveSheet()->getStyle("A1:EV$i")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A1:EV1')->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth('70');
$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth('70');
$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth('70');
$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth('70');
$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth('70');
$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth('70');
$objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setWidth('80');
$objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('AW')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('BA')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BE')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BF')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BG')->setWidth('50');
$objPHPExcel->getActiveSheet()->getColumnDimension('BI')->setWidth('80');
$objPHPExcel->getActiveSheet()->getColumnDimension('BJ')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BO')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BP')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BQ')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BR')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BS')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BT')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BU')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BV')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BW')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BX')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BY')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BZ')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('CA')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CB')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('CD')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CE')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CF')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CG')->setWidth('50');
$objPHPExcel->getActiveSheet()->getColumnDimension('CH')->setWidth('50');
$objPHPExcel->getActiveSheet()->getColumnDimension('CI')->setWidth('50');
$objPHPExcel->getActiveSheet()->getColumnDimension('CJ')->setWidth('50');
$objPHPExcel->getActiveSheet()->getColumnDimension('CN')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CO')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CP')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CQ')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CR')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CS')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CT')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CU')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CV')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CW')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CX')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CY')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CZ')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DA')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DB')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DC')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DD')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DE')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DF')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DG')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DH')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DI')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DJ')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DK')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DL')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DM')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DN')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DO')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DP')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DQ')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DR')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DS')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DT')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DU')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DV')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DW')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DX')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DY')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DZ')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EA')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EB')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EC')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('ED')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EE')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EF')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EG')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EH')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EI')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EJ')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EK')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EL')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EM')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EN')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EO')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EP')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EQ')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('ER')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('ES')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('ET')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EU')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EV')->setWidth('30');




$objPHPExcel->getActiveSheet()->getStyle("J2:J$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("K2:K$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("L2:L$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("M2:M$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("N2:N$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("O2:O$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("P2:P$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("Q2:Q$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("R2:R$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("S2:S$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("T2:T$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("U2:U$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("V2:V$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("X2:X$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("Y2:Y$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("W2:W$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("AA2:AA$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("AB2:AB$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

$objPHPExcel->getActiveSheet()->getStyle("J2:J$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:ER$i")->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
/* $objPHPExcel->getActiveSheet()->getStyle("P2:P$i")->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED); */


$objPHPExcel->getActiveSheet()->setTitle('GSM');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

$data = date("Ymd");

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->setPreCalculateFormulas(false);
$objWriter->save(str_replace(".php", ".xls", "esportazione-catalogo/catalogo_xls/gsm-$data.php"));

echo "<br /><br />Il file &egrave; pronto per essere scaricato: <a onclick='window.onbeforeunload = null' href='http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/gsm-$data.xls'>clicca qui per eseguire il download</a>!";

?>




