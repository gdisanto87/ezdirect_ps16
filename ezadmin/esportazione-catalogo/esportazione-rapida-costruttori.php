<?php

$connection = mysql_connect(_DB_SERVER_,_DB_USER_,_DB_PASSWD_);
mysql_select_db(_DB_NAME_,$connection);
$link_global = $connection;

//$connection = mysql_connect("localhost","root",""); 
//mysql_select_db("ezdirect",$connection);

ini_set("memory_limit","9992M");
set_time_limit(7200);

$foglio = 0;
/** PHPExcel */
require_once 'Classes/PHPExcel.php';


// Create new PHPExcel object
echo date('H:i:s') . " Creato nuovo file di Excel... !";
$objPHPExcel = new PHPExcel();


foreach($_POST['per-costruttore'] as $marchio)
{
	
$query = "SELECT
product.id_product AS id_prestashop,
product.reference AS codice_spring,
product_other.aggiornato AS aggiornato,
product.serial AS seriale,
(CASE WHEN product.provvigione > 0 THEN product.provvigione ELSE '' END) AS provvigione,
product.wholesale_price AS prezzo_acquisto,
supplier.name AS fornitore,
product.date_available AS data_disponibilita,
product.stock_quantity AS mag_spring,
product.sconto_acquisto_1 AS sconto_acquisto_1,
product.sconto_acquisto_2 AS sconto_acquisto_2,
product.sconto_acquisto_3 AS sconto_acquisto_3,
product.scontolistinovendita AS scontolistinovendita,
product_other.sconto_rivenditore_1 AS sc_riv_1,
product_other.sconto_rivenditore_2 AS sc_riv_2,
product_other.sconto_rivenditore_1_min AS sc_riv_1_min,
product_other.sconto_rivenditore_1_max AS sc_riv_1_max,
product_other.sconto_rivenditore_2_min AS sc_riv_2_min,
product_other.sconto_rivenditore_2_max AS sc_riv_2_max,
product_other.directel AS directel,
product_other.redirect AS redirect,
product_other.wordpress_1 AS wordpress_1,
product_other.wordpress_2 AS wordpress_2,
product_other.wordpress_3 AS wordpress_3,
product.canonical AS canonical,
product.reference AS codice_prodotto,
product.supplier_reference AS codice_costruttore,
product_lang.name AS nome_prodotto,
product.ean13 AS codice_ean,
product.listino AS prezzo_listino,
product.price AS prezzo_ezdirect,
product.quantity AS quantita,
product.stock_quantity AS mag_ez,
product.supplier_quantity AS mag_allnet,
product.esprinet_quantity AS mag_esprinet,
product.itancia_quantity AS mag_itancia,
product_lang.description AS descrizione,
product_lang.available_now AS msg_disponibile,
product_lang.available_later AS msg_non_disponibile,
product.weight AS peso,
product.id_category_default AS id_categoria_prestashop,
product.id_manufacturer AS id_costruttore_prestashop,
manufacturer.name AS costruttore,
product_other.prova_gratuita AS prova_gratuita,
product_other.trasporto_gratuito AS trasporto_gratuito,
product_other.mostra_spese_spedizione AS mostra_spese_spedizione,
product.condition AS nuovo_eco,
product.date_upd AS data_ultima_modifica,
product.date_add AS data_inserimento,
product_other.garanzia_3_anni AS garanzia_3_anni,
product_other.assistenza_3_anni AS assistenza_3_anni,
product_lang.description_amazon AS descrizione_amazon,
product_lang.meta_description AS metatag_description,
product_lang.meta_title AS title_tag,
product_lang.meta_keywords AS metatag_keywords,
product_lang.punti_di_forza AS punti_di_forza,
product_lang.description_short AS descrizione_breve,
product_lang.cat_homepage AS cat_homepage,
product_lang.desc_homepage AS desc_homepage,
product.trovaprezzi AS trovaprezzi,
product.google_shopping AS google_shopping,
product.eprice AS eprice,
product.amazon AS amazon,
product.asin AS asin,
product.active AS stato
FROM
  product
  LEFT JOIN product_download pd ON pd.id_product = product.id_product
  JOIN product_lang ON (product.id_product = product_lang.id_product)
  LEFT JOIN product_other ON (product.id_product = product_other.id_product)
  LEFT JOIN manufacturer ON (product.id_manufacturer = manufacturer.id_manufacturer)
  LEFT JOIN supplier ON (product.id_supplier = supplier.id_supplier)
WHERE product_lang.id_lang = 5
".($marchio != 'tutti' ? "AND product.id_manufacturer = ".$marchio."" : "")."
 ".(isset($_POST['includi_inattivi']) ? '' : 'AND product.active = 1')."
 ".(isset($_POST['includi_interno']) ? '' : 'AND product.id_category_default != 246')."
 ".(isset($_POST['includi_scaricabili']) ? '' : 'AND pd.id_product_download IS NULL')."
 ".(Tools::getIsset('senza_fp') ? ' AND product.fuori_produzione = 0' : '')."
GROUP BY id_prestashop
ORDER BY costruttore ASC, nome_prodotto ASC
  ";

$result = mysql_query($query, $link_global);
if($foglio > 0);
	$objPHPExcel->createSheet(NULL, $foglio);

// Set properties

$i = 2;

$objPHPExcel->setActiveSheetIndex($foglio)
			->setCellValue('A1', 'Costruttore')
            ->setCellValue('B1', 'Codice Ezdirect')
			
			->setCellValue('C1', 'EAN')
            ->setCellValue('D1', 'Nome prodotto')
            ->setCellValue('E1', 'Listino')
			->setCellValue('F1', 'Prezzo web')
            ->setCellValue('G1', 'Sc. riv 1 %')		
			->setCellValue('H1', 'Sc. riv 2 %')		
			;



$objPHPExcel->getActiveSheet()->getStyle("A")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("C")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("D")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
///////////////// fine campi specifici categoria


// QUERY PER TUTTE LE CATEGORIE

$query1 = "SELECT category_lang.id_category, category_lang.name FROM category_lang JOIN product ON category_lang.id_category = product.id_category_default WHERE id_lang = 5";
$result1 = mysql_query($query1, $link_global);
$string_cats = '';
while ($row1 = mysql_fetch_array($result1, MYSQL_ASSOC)) {
$string_cats .= $row1['name'].",";
} 


while($row = mysql_fetch_assoc($result)) {

$objPHPExcel->getActiveSheet()->getCell("B$i")->setValueExplicit($row['codice_prodotto'], PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("C$i")->setValueExplicit($row['codice_ean'], PHPExcel_Cell_DataType::TYPE_STRING);



if($row['prezzo_acquisto'] == 0 || $row['prezzo_acquisto'] == '' || !isset($row['prezzo_acquisto'])) {
$netto = 0;
}
else {
$netto = $row['prezzo_acquisto'];
}

if($row['scontolistinovendita'] == 0 && $row['prezzo_listino'] != $row['prezzo_ezdirect'])
{
	$row['scontolistinovendita'] = ((($row['prezzo_listino'] - $row['prezzo_ezdirect']) * 100)/$row['prezzo_listino']);
}	

$objPHPExcel->setActiveSheetIndex($foglio)
            
			->setCellValue("D$i", $row['nome_prodotto'])
			->setCellValue("A$i", (iconv("ISO-8859-1", "UTF-8",$row['costruttore'])))
			->setCellValue("E$i", number_format($row['prezzo_listino'],2,",","."))
			->setCellValue("F$i", number_format($row['prezzo_ezdirect'],2,",","."))
			;


// SCONTI RIVENDITORI

$query1 = "SELECT * FROM specific_price WHERE id_product = $row[id_prestashop] AND (specific_price_name = 'sc_riv_1') ORDER BY specific_price_name ASC";
$result1 = mysql_query($query1, $link_global);
$cella[0] = "G";
$k = 0;
while ($row1 = mysql_fetch_array($result1, MYSQL_ASSOC)) {
$objPHPExcel->setActiveSheetIndex($foglio)
             ->setCellValue($cella[$k]."$i", $row1['reduction']*100);
			$k++;
}

// SCONTI RIVENDITORI

$query2 = "SELECT * FROM specific_price WHERE id_product = $row[id_prestashop] AND (specific_price_name = 'sc_riv_2') ORDER BY specific_price_name ASC";
$result2 = mysql_query($query2, $link_global);
$cella[0] = "H";
$v = 0;
while ($row2 = mysql_fetch_array($result2, MYSQL_ASSOC)) {
$objPHPExcel->setActiveSheetIndex($foglio)
             ->setCellValue($cella[$v]."$i", $row1['reduction']*100);
			$v++;
}


$i++; // INCREMENTO ----------------------------------------------------------------------------------------------------------------------------------------



}
	
$highestRow = $objPHPExcel->getActiveSHeet()->getHighestRow();
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);			
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFill()->getStartColor()->setRGB('FFFF00');		
$objPHPExcel->getActiveSheet()->getStyle("A1:H$i")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('50');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth('10');

$objPHPExcel->getActiveSheet()->getStyle("E2:E$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("f2:F$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

$objPHPExcel->getActiveSheet()->getStyle("G2:G$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("H2:H$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);


if($marchio != 'tutti')
{
	$costruttore = Db::getInstance()->getValue('SELECT name FROM manufacturer WHERE id_manufacturer = '.$marchio);
	$objPHPExcel->getActiveSheet()->setTitle($costruttore);

	$titolo = $costruttore;
}
else
{
	$titolo .= 'tutti-';
}	

$objPHPExcel->getActiveSheet()->setTitle($titolo);
$foglio++;
}

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

$data = date("Ymd");

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->setPreCalculateFormulas(false);
$objWriter->save(str_replace(".php", ".xls", "esportazione-catalogo/catalogo_xls/".strtolower($titolo)."-$data-rapido.php"));

echo "<br /><br />Il file &egrave; pronto per essere scaricato: <a onclick='window.onbeforeunload = null' href='http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/".strtolower($titolo)."-$data-rapido.xls'>clicca qui per eseguire il download</a>!";

?>




