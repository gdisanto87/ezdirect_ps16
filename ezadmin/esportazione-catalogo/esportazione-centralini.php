<?php

$connection = mysql_connect(_DB_SERVER_,_DB_USER_,_DB_PASSWD_);
mysql_select_db(_DB_NAME_,$connection);
$link_global = $connection;

//$connection = mysql_connect("localhost","root",""); 
//mysql_select_db("ezdirect",$connection);

ini_set("memory_limit","892M");
set_time_limit(3600);

$query = "SELECT
product.id_product AS id_prestashop,
product.reference AS codice_spring,
product_other.aggiornato AS aggiornato,
product.serial AS seriale,
(CASE WHEN product.provvigione > 0 THEN product.provvigione ELSE '' END) AS provvigione,
product.wholesale_price AS prezzo_acquisto,
supplier.name AS fornitore,
product.date_available AS data_disponibilita,
product.stock_quantity AS mag_spring,
product.sconto_acquisto_1 AS sconto_acquisto_1,
product.sconto_acquisto_2 AS sconto_acquisto_2,
product.sconto_acquisto_3 AS sconto_acquisto_3,
product.scontolistinovendita AS scontolistinovendita,
product_other.sconto_rivenditore_1 AS sc_riv_1,
product_other.sconto_rivenditore_2 AS sc_riv_2,
product_other.sconto_rivenditore_1_min AS sc_riv_1_min,
product_other.sconto_rivenditore_1_max AS sc_riv_1_max,
product_other.sconto_rivenditore_2_min AS sc_riv_2_min,
product_other.sconto_rivenditore_2_max AS sc_riv_2_max,
product_other.directel AS directel,
product_other.redirect AS redirect,
product_other.wordpress_1 AS wordpress_1,
product_other.wordpress_2 AS wordpress_2,
product_other.wordpress_3 AS wordpress_3,
product.canonical AS canonical,
product.reference AS codice_prodotto,
product.supplier_reference AS codice_costruttore,
product_lang.name AS nome_prodotto,
product.ean13 AS codice_ean,
product.listino AS prezzo_listino,
product.price AS prezzo_ezdirect,
product.quantity AS quantita,
product.stock_quantity AS mag_ez,
product.supplier_quantity AS mag_allnet,
product.esprinet_quantity AS mag_esprinet,
product.itancia_quantity AS mag_itancia,
product_lang.description AS descrizione,
product_lang.available_now AS msg_disponibile,
product_lang.available_later AS msg_non_disponibile,
product.weight AS peso,
product.id_category_default AS id_categoria_prestashop,
product.id_manufacturer AS id_costruttore_prestashop,
manufacturer.name AS costruttore,
product_other.prova_gratuita AS prova_gratuita,
product_other.trasporto_gratuito AS trasporto_gratuito,
product_other.mostra_spese_spedizione AS mostra_spese_spedizione,
product.condition AS nuovo_eco,
product.date_upd AS data_ultima_modifica,
product.date_add AS data_inserimento,
product_other.garanzia_3_anni AS garanzia_3_anni,
product_other.assistenza_3_anni AS assistenza_3_anni,
product_lang.description_amazon AS descrizione_amazon,
product_lang.meta_description AS metatag_description,
product_lang.meta_title AS title_tag,
product_lang.meta_keywords AS metatag_keywords,
product_lang.punti_di_forza AS punti_di_forza,
product_lang.description_short AS descrizione_breve,
product_lang.cat_homepage AS cat_homepage,
product_lang.desc_homepage AS desc_homepage,
product.trovaprezzi AS trovaprezzi,
product.google_shopping AS google_shopping,
product.eprice AS eprice,
product.amazon AS amazon,
product.asin AS asin,
product.active AS stato,
product.noindex AS noindex,
pe.rebate_1 AS rebate_1,
pe.rebate_2 AS rebate_2,
pe.rebate_3 AS rebate_3
FROM
  product
  JOIN product_lang ON (product.id_product = product_lang.id_product)
  LEFT JOIN product_other ON (product.id_product = product_other.id_product)
  LEFT JOIN product_esolver pe ON pe.id_product = product.id_product
  LEFT JOIN manufacturer ON (product.id_manufacturer = manufacturer.id_manufacturer)
  LEFT JOIN supplier ON (product.id_supplier = supplier.id_supplier)
WHERE
 product_lang.id_lang = 5 AND (product.id_category_default = '23'
 OR product.id_category_default = '154'
 OR product.id_category_default = '63'
OR product.id_category_default = '235'
OR product.id_category_default = '181'
OR product.id_category_default = '205'
OR product.id_category_default = '97'
OR product.id_category_default = '94'
OR product.id_category_default = '207'
OR product.id_category_default = '206'
OR product.id_category_default = '213'
OR product.id_category_default = '153'
OR product.id_category_default = '137'
OR product.id_category_default = '227'
OR product.id_category_default = '232'
OR product.id_category_default = '138'
OR product.id_category_default = '209'
OR product.id_category_default = '158'
OR product.id_category_default = '81'
OR product.id_category_default = '231'
OR product.id_category_default = '67'
OR product.id_category_default = '151'
OR product.id_category_default = '122'
OR product.id_category_default = '199'
OR product.id_category_default = '150'
OR product.id_category_default = '187'
OR product.id_category_default = '123'
OR product.id_category_default = '152')
".(Tools::getIsset('senza_fp') ? ' AND product.fuori_produzione = 0' : '')."
GROUP BY id_prestashop
  ORDER BY id_costruttore_prestashop
 
  ";

$result = mysql_query($query, $link_global);

error_reporting(E_ALL);

date_default_timezone_set('Europe/Rome');

/** PHPExcel */
require_once 'Classes/PHPExcel.php';


// Create new PHPExcel object
echo date('H:i:s') . " Creato nuovo file di Excel... !";
$objPHPExcel = new PHPExcel();

// Set properties

$i = 2;

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Id prestashop')
            ->setCellValue('B1', 'Codice costruttore')
            ->setCellValue('C1', 'Codice EZ Spring')
            ->setCellValue('D1', 'Codice EAN')
            ->setCellValue('E1', 'ASIN Amazon')
            ->setCellValue('F1', 'Aggiornato')
            ->setCellValue('G1', 'Data ultima modifica')
            ->setCellValue('H1', 'Costruttore')
            ->setCellValue('I1', 'Nome prodotto')
            ->setCellValue('J1', 'Listino')
            ->setCellValue('K1', 'Sc acq 1')
            ->setCellValue('L1', 'Sc acq 2')
            ->setCellValue('M1', 'Sc acq 3')
			->setCellValue('N1', 'Netto eSolver')
            ->setCellValue('O1', 'Rebate 1')
			->setCellValue('P1', 'Rebate 2')
            ->setCellValue('Q1', 'Rebate 3')
            ->setCellValue('R1', 'Netto SALES')
            ->setCellValue('S1', 'Sc clienti')
            ->setCellValue('T1', 'Web')
            ->setCellValue('U1', (iconv("ISO-8859-1", "UTF-8", 'Marg')))
            ->setCellValue('V1', (iconv("ISO-8859-1", "UTF-8", 'Sc qt 1')))
            ->setCellValue('W1', (iconv("ISO-8859-1", "UTF-8", 'Sc qt 2')))
            ->setCellValue('X1', (iconv("ISO-8859-1", "UTF-8", 'Sc qt 3')))
            ->setCellValue('Y1', 'Sc riv 1')
            ->setCellValue('Z1', 'Premio Ag. Personalizzato')
            ->setCellValue('AA1', 'Fornitore')
            ->setCellValue('AB1', 'Qt. tot.')
            ->setCellValue('AC1', 'Categoria prestashop')
            ->setCellValue('AD1', 'Peso confezione')
            ->setCellValue('AE1', 'Data inserimento')
            ->setCellValue('AF1', 'Descrizione HTML')
            ->setCellValue('AG1', 'Descrizione breve (listing)')
            ->setCellValue('AH1', 'Descrizione Amazon')
            ->setCellValue('AI1', 'Title tag')
            ->setCellValue('AJ1', 'Metatag description')
            ->setCellValue('AK1', 'Keywords')
            ->setCellValue('AL1', 'Categoria home page')
            ->setCellValue('AM1', 'Descrizione home page')
            ->setCellValue('AN1', 'Noindex')
            ->setCellValue('AO1', 'Trovaprezzi')
            ->setCellValue('AP1', 'ePrice')
            ->setCellValue('AQ1', '')
            ->setCellValue('AR1', 'Google Shopping')
            ->setCellValue('AS1', 'Messaggio disponibile')
            ->setCellValue('AT1', 'Messaggio non disponibile')
            ->setCellValue('AU1', '')
            ->setCellValue('AV1', '')
            ->setCellValue('AW1', (iconv("ISO-8859-1", "UTF-8", 'Data disponibilit�')))
            ->setCellValue('AX1', 'Amazon IT')
            ->setCellValue('AY1', 'Amazon FR')
			->setCellValue('AZ1', 'Amazon ES')
			->setCellValue('BA1', 'Amazon DE')
			->setCellValue('BB1', 'Amazon UK')
			->setCellValue('BC1', 'Amazon NL')
			->setCellValue('BD1', '')
			->setCellValue('BE1', '')
			->setCellValue('BF1', '')
			->setCellValue('BG1', '')
			->setCellValue('BH1', '')
			->setCellValue('BI1', '')
			->setCellValue('BJ1', 'Nuovo/Eco')
			->setCellValue('BK1', '')
			->setCellValue('BL1', '')
			->setCellValue('BM1', '')
			->setCellValue('BN1', '')
			->setCellValue('BO1', 'Attivo (1)')
			->setCellValue('BP1', 'Prova gratuita')
			->setCellValue('BQ1', 'Trasporto gratis')
			->setCellValue('BR1', 'Immagine listing prodotti')
			->setCellValue('BS1', '')
			->setCellValue('BT1', '')
			->setCellValue('BU1', '')
			->setCellValue('BV1', '')
			->setCellValue('BW1', '')
			->setCellValue('BX1', '')
			->setCellValue('BY1', '')
			->setCellValue('BZ1', '')
			->setCellValue('CA1', '')
			->setCellValue('CB1', '')
			->setCellValue('CC1', '')
			->setCellValue('CD1', '')
			->setCellValue('CE1', '')
			->setCellValue('CF1', '')
			->setCellValue('CG1', 'Redirect')
			->setCellValue('CH1', 'Punto di forza')
			->setCellValue('CI1', 'Garanzia')
			->setCellValue('CJ1', 'Confezione')
			->setCellValue('CK1', 'Tipo')
			->setCellValue('CL1', 'Utilizzo')
			->setCellValue('CM1', '')
			->setCellValue('CN1', 'Messaggi vocali su linee (OPA)')
			->setCellValue('CO1', 'Livelli IVR-OPA')
			->setCellValue('CP1', (iconv("ISO-8859-1", "UTF-8", 'Quantit� messaggi (fasce orarie di lavoro)')))
			->setCellValue("CQ1", 'Cambio messaggi a orari impostati (giorno/notte)')
			->setCellValue('CR1', (iconv("ISO-8859-1", "UTF-8", 'Modalit� cambio messaggi (giorno/notte)')))
			->setCellValue('CS1', 'Voice mail - segreteria')
			->setCellValue('CT1', 'Registrazione conversazioni')
			->setCellValue("CU1", 'Audio wideband')
			->setCellValue('CV1', 'Messaggi / musica con file')
			->setCellValue('CW1', 'Memoria registrazione (ore')
			->setCellValue('CX1', 'Notifica voice mail to email')
			->setCellValue('CY1', '')
			->setCellValue('CZ1', '')
			->setCellValue('DA1', '')
			->setCellValue('DB1', '')
			->setCellValue('DC1', 'Rubrica (memorie)')
			->setCellValue('DD1', '')
			->setCellValue('DE1', '')
			->setCellValue('DF1', '')
			->setCellValue('DG1', '')
			->setCellValue('DH1', 'Tipo linee')
			->setCellValue('DI1', 'Linee esterne a bordo')
			->setCellValue('DJ1', 'Tipo di derivati interni collegabili')
			->setCellValue('DK1', 'Derivati a bordo')
			->setCellValue('DL1', (iconv("ISO-8859-1", "UTF-8", 'Capacit� massima porte (linee - interni)')))
			->setCellValue('DM1', 'Slot per schede di equipaggiamento')
			->setCellValue('DN1', 'Terminali cordless GAP collegabili')
			->setCellValue('DO1', '')
			->setCellValue('DP1', '')
			->setCellValue('DQ1', '')
			->setCellValue('DR1', '')
			->setCellValue('DS1', '')
			->setCellValue('DT1', 'Porte LAN')
			->setCellValue('DU1', 'Porte RS32')
			->setCellValue('DV1', 'Porte USB')
			->setCellValue('DW1', 'Porte WAN')
			->setCellValue('DX1', 'Connessione cavi (analog. digit.)')
			->setCellValue('DY1', 'Codec audio (VoIP)')
			->setCellValue('DZ1', 'Codec video (VoIP)')
			->setCellValue('EA1', 'Tapi (integrazione LAN/PC)')
			->setCellValue('EB1', 'Collegamento fonte musica esterna')
			->setCellValue('EC1', 'Collegamento cercapersone/P.A. esterno')
			->setCellValue('ED1', '')
			->setCellValue('EE1', '')
			->setCellValue('EF1', '')
			->setCellValue('EG1', '')
			->setCellValue('EH1', 'Selezione linee in uscita')
			->setCellValue('EI1', 'Conversazioni simultanee esterne')
			->setCellValue('EJ1', 'Riconoscimento fax automatico')
			->setCellValue('EK1', 'Least Cost Routing')
			->setCellValue('EL1', 'Trasferta manuale verso esterno')
			->setCellValue('EM1', 'Deviazione a numero esterno')
			->setCellValue('EN1', 'Conferenza')
			->setCellValue('EO1', 'Sveglia')
			->setCellValue('EP1', 'Funzioni hotel')
			->setCellValue('EQ1', 'Videochiamate VoIP')
			->setCellValue('ER1', 'Restrizione chiamate in uscita')
			->setCellValue('ES1', 'Fax to email')
			->setCellValue('ET1', 'Fax server')
			->setCellValue('EU1', 'Telegestione')
			->setCellValue('EV1', 'Click and call (da PC)')
			->setCellValue('EW1', 'Supporta citofoni')
			->setCellValue('EX1', 'Telefoni fissi collegabili')
			->setCellValue('EY1', 'Terminali cordless collegabili')
			->setCellValue('EZ1', 'Supporta softphones (PC-Palmare)')
			->setCellValue('FA1', 'Supporta linee VoIP SIP')
			->setCellValue('FB1', 'Integrazione telefoni cellulari')
			->setCellValue('FC1', 'Report del traffico telefonico (CDR)')
			->setCellValue('FD1', 'Documentazione in italiano')
			->setCellValue('FE1', 'Amministrazione')
			->setCellValue('FF1', 'T.38 Fax')
			->setCellValue('FG1', 'Password personale')
			->setCellValue('FH1', 'Blacklist')
			->setCellValue('FI1', 'DECT multicella integrato')
			->setCellValue('FJ1', 'Paging su telefoni (ditigali-IP)')
			->setCellValue('FK1', 'Paging su cercapersone/P.A.')
			->setCellValue('FL1', 'Compatibile Skype')
			->setCellValue('FM1', 'Ridondanza')
			->setCellValue('FN1', '')
			->setCellValue('FO1', '')
			->setCellValue('FP1', '')
			->setCellValue('FQ1', 'Consumo energetico')
			->setCellValue('FR1', 'Alimentazione')
			->setCellValue('FS1', 'Coll.to batterie esterne di soccorso')
			->setCellValue('FT1', '')
			->setCellValue('FU1', '')
			->setCellValue('FV1', '')
			->setCellValue('FW1', 'Dimensioni')
			->setCellValue('FX1', 'Peso')
			->setCellValue('FY1', 'Colore')
			->setCellValue('FZ1', '')
			->setCellValue('GA1', '')
			->setCellValue('GB1', '')
			->setCellValue('GC1', 'Installazione e certificazione')
			->setCellValue('GD1', 'Posa')
			->setCellValue('GE1', 'Supporto da remoto a installazione')
			->setCellValue('GF1', 'Contratto assistenza postvendita')
			->setCellValue('GG1', '')
			->setCellValue('GH1', '')
			->setCellValue('GI1', '')
			->setCellValue('GJ1', '')
			->setCellValue('GK1', 'Noleggio')

			
			
			
			
			
			
			
			
			
			
			;



$objPHPExcel->getActiveSheet()->getStyle("B")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("C")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("D")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("E")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("G")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("J")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("K")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("L")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("M")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("N")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("O")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("P")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("Q")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("R")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("S")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("T")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("U")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("Y")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("Z")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("W")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
///////////////// campi specifici categoria
$objPHPExcel->getActiveSheet()->getStyle("DE")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("DH")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("DT")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
///////////////// fine campi specifici categoria


//manuali e immagini

$objPHPExcel->getActiveSheet()->getColumnDimension('BR')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BS')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BT')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BU')->setVisible(false);
//fine manuali e immagini


$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setVisible(false);

$objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setVisible(false);

$objPHPExcel->getActiveSheet()->getColumnDimension('BD')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BE')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BF')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BG')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BH')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BI')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BK')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BL')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BM')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BN')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BU')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BW')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BX')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BY')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BZ')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CA')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CB')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CC')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CD')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CE')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CF')->setVisible(false);


$objPHPExcel->getActiveSheet()->getColumnDimension('CW')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CY')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CZ')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('DA')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('DB')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('DD')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('DE')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('DF')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('DG')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('DO')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('DP')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('DQ')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('DR')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('DS')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('ED')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('EE')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('EF')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('EG')->setVisible(false);

$objPHPExcel->getActiveSheet()->getColumnDimension('FN')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('FO')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('FP')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('FT')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('FU')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('FV')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('GA')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('GB')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('GG')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('GH')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('GI')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('GJ')->setVisible(false);

if(Tools::getIsset('senza_seo'))
{
	$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setVisible(false);
	
}



// QUERY PER TUTTE LE CATEGORIE

$query1 = "SELECT category_lang.id_category, category_lang.name FROM category_lang JOIN product ON category_lang.id_category = product.id_category_default WHERE id_lang = 5";
$result1 = mysql_query($query1, $link_global);
$string_cats = '';
while ($row1 = mysql_fetch_array($result1, MYSQL_ASSOC)) {
$string_cats .= $row1['name'].",";
} 


while($row = mysql_fetch_assoc($result)) {

$objPHPExcel->getActiveSheet()->getCell("B$i")->setValueExplicit($row['codice_costruttore'], PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("C$i")->setValueExplicit($row['codice_prodotto'], PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("D$i")->setValueExplicit($row['codice_ean'], PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("E$i")->setValueExplicit($row['asin'], PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("AB$i")->setValueExplicit($row['quantita'], PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("AD$i")->setValueExplicit($row['peso'], PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("DE$i")->setValueExplicit('', PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("DH$i")->setValueExplicit('', PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("DT$i")->setValueExplicit('', PHPExcel_Cell_DataType::TYPE_STRING);






if(Tools::getIsset('senza_seo'))
{
	$descrizione = ''; $descrizione_breve = ''; $row['descrizione_amazon'] = ''; $title = ''; $metatag_description = ''; $metatag_keywords = ''; $row['cat_homepage'] = ''; $row['desc_homepage'] = '';
	
}

else
{

	$descrizione = $row['descrizione'];
	$descrizione_breve = $row['descrizione_breve'];
	$title = $row['title_tag'];
	$metatag_description = $row['metatag_description'];
	$metatag_keywords = $row['metatag_keywords'];

	$descrizione = preg_replace("/<br>/","<br />",$row['descrizione']);
	$descrizione = preg_replace("/<b>/","<strong>",$descrizione);
	$descrizione = preg_replace("/<\/b>/","</strong>",$descrizione);
	$descrizione = preg_replace("/<i>/","<em>",$descrizione);
	$descrizione = preg_replace("/<\/i>/","</em>",$descrizione);

	// correzioni ortografiche
	$descrizione = preg_replace("/epr/","per",$descrizione);
	$descrizione_breve = preg_replace("/epr/","per",$descrizione_breve);
	$title = preg_replace("/epr/","per",$title);
	$metatag_description = preg_replace("/epr/","per",$metatag_description);
	$metatag_keywords = preg_replace("/epr/","per",$metatag_keywords);

	$descrizione = preg_replace("/teelfon/","telefon",$descrizione);
	$descrizione_breve = preg_replace("/teelfon/","telefon",$descrizione_breve);
	$title = preg_replace("/teelfon/","telefon",$title);
	$metatag_description = preg_replace("/teelfon/","telefon",$metatag_description);
	$metatag_keywords = preg_replace("/teelfon/","telefon",$metatag_keywords);

	$descrizione = preg_replace("/ms link/","MS Lync",$descrizione);
	$descrizione_breve = preg_replace("/ms link/","MS Lync",$descrizione_breve);
	$title = preg_replace("/ms link/","MS Lync",$title);
	$metatag_description = preg_replace("/ms link/","MS Lync",$metatag_description);
	$metatag_keywords = preg_replace("/ms link/","MS Lync",$metatag_keywords);

	$descrizione = preg_replace("/ms linc/","MS Lync",$descrizione);
	$descrizione_breve = preg_replace("/ms linc/","MS Lync",$descrizione_breve);
	$title = preg_replace("/ms linc/","MS Lync",$title);
	$metatag_description = preg_replace("/ms linc/","MS Lync",$metatag_description);
	$metatag_keywords = preg_replace("/ms linc/","MS Lync",$metatag_keywords);

	$descrizione = preg_replace("/ms lynk/","MS Lync",$descrizione);
	$descrizione_breve = preg_replace("/ms lynk/","MS Lync",$descrizione_breve);
	$title = preg_replace("/ms lynk/","MS Lync",$title);
	$metatag_description = preg_replace("/ms lynk/","MS Lync",$metatag_description);
	$metatag_keywords = preg_replace("/ms lynk/","MS Lync",$metatag_keywords);

	$descrizione = preg_replace("/plantroncis/","Plantronics",$descrizione);
	$descrizione_breve = preg_replace("/plantroncis/","Plantronics",$descrizione_breve);
	$title = preg_replace("/plantroncis/","Plantronics",$title);
	$metatag_description = preg_replace("/plantroncis/","Plantronics",$metatag_description);
	$metatag_keywords = preg_replace("/plantroncis/","Plantronics",$metatag_keywords);

	$descrizione = preg_replace("/Plantroncis/","Plantronics",$descrizione);
	$descrizione_breve = preg_replace("/Plantroncis/","Plantronics",$descrizione_breve);
	$title = preg_replace("/Plantroncis/","Plantronics",$title);
	$metatag_description = preg_replace("/Plantroncis/","Plantronics",$metatag_description);
	$metatag_keywords = preg_replace("/Plantroncis/","Plantronics",$metatag_keywords);

	//correzioni ortografiche

	$title = ucfirst($title);
	$metatag_description = ucfirst($metatag_description);
	$metatag_keywords = ucfirst($metatag_keywords);

	if(($metatag_description[strlen($metatag_description)-1]) != ".") { $metatag_description = $metatag_description."."; } else { }



	if(!preg_match("/<h1*./",$descrizione)) {
	$descrizione = "<h1>".$row['nome_prodotto']."</h1> ".$descrizione;
	}
}
if($row['prezzo_acquisto'] == 0 || $row['prezzo_acquisto'] == '' || !isset($row['prezzo_acquisto'])) {
$netto = 0;
}
else {
$netto = $row['prezzo_acquisto'];
}

if($row['scontolistinovendita'] == 0 && $row['prezzo_listino'] != $row['prezzo_ezdirect'])
{
	$row['scontolistinovendita'] = ((($row['prezzo_listino'] - $row['prezzo_ezdirect']) * 100)/$row['prezzo_listino']);
}	

if($row['rebate_1'] == '')
	$row['rebate_1'] = 0;

if($row['rebate_2'] == '')
	$row['rebate_2'] = 0;

if($row['rebate_3'] == '')
	$row['rebate_3'] = 0;


$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A$i", $row['id_prestashop'])
			->setCellValue("F$i", '')
			->setCellValue("G$i", $row['data_ultima_modifica'])
			->setCellValue("H$i", (iconv("ISO-8859-1", "UTF-8",$row['costruttore'])))
			->setCellValue("I$i", (iconv("ISO-8859-1", "UTF-8",$row['nome_prodotto'])))
			->setCellValue("J$i", number_format($row['prezzo_listino'], 2, ',', ''))
			->setCellValue("K$i", $row['sconto_acquisto_1'])
			->setCellValue("L$i", $row['sconto_acquisto_2'])
			->setCellValue("M$i", $row['sconto_acquisto_3'])
			->setCellValue("N$i", "=ROUND((IF(K$i>=0;(J$i*((100-K$i)/100)*((100-L$i)/100)*((100-M$i)/100));$netto)),2)")
			->setCellValue("O$i", $row['rebate_1'])
			->setCellValue("P$i", $row['rebate_2'])
			->setCellValue("Q$i", $row['rebate_3'])
			->setCellValue("R$i", "=ROUND((IF(K$i>=0;(J$i*((100-K$i)/100)*((100-L$i)/100)*((100-M$i)/100)*((100-O$i)/100)*((100-P$i)/100)*((100-Q$i)/100));$netto)),2)")
            ->setCellValue("S$i", number_format($row['scontolistinovendita'], 2, ',' ,''))
            ->setCellValue("T$i", "=ROUND((IF(S$i>=0;J$i-(J$i*S$i/100);".number_format($row['prezzo_ezdirect'], 2, '.', '').")),2)")
            ->setCellValue("U$i",  "=ROUND((((T$i-R$i)*100)/T$i),2)")
            ->setCellValue("V$i", '')
            ->setCellValue("W$i", '')
			->setCellValue("X$i", '')
->setCellValue("Z$i", $row['provvigione'])
            ->setCellValue("AA$i", $row['fornitore'])
            ->setCellValue("AE$i", $row['data_inserimento'])
            
			->setCellValue("AF$i", (iconv("ISO-8859-1", "UTF-8",$descrizione)))
             ->setCellValue("AG$i", (iconv("ISO-8859-1", "UTF-8",$descrizione_breve)))
            ->setCellValue("AH$i", (iconv("ISO-8859-1", "UTF-8",$row['descrizione_amazon'])))
            ->setCellValue("AI$i", (iconv("ISO-8859-1", "UTF-8",$title)))
           ->setCellValue("AJ$i", (iconv("ISO-8859-1", "UTF-8",$metatag_description)))
            ->setCellValue("AK$i", (iconv("ISO-8859-1", "UTF-8",$metatag_keywords)))
            ->setCellValue("AL$i", (iconv("ISO-8859-1", "UTF-8",$row['cat_homepage'])))
             ->setCellValue("AM$i", (iconv("ISO-8859-1", "UTF-8",$row['desc_homepage'])))
            ->setCellValue("AN$i", '')
            ->setCellValue("AO$i", '')
            ->setCellValue("AP$i", '')
            ->setCellValue("AQ$i", '')
            ->setCellValue("AR$i", '')
            ->setCellValue("AS$i", $row['msg_disponibile'])
            ->setCellValue("AT$i", $row['msg_non_disponibile'])
            ->setCellValue("AU$i", '')
            ->setCellValue("AV$i", '')
            ->setCellValue("AW$i", $row['data_disponibilita'])
            ->setCellValue("AX$i", '')
            ->setCellValue("AY$i", '')
			->setCellValue("AZ$i", '')
			->setCellValue("BA$i", '')
			->setCellValue("BB$i", '')
			->setCellValue("BC$i", '')
			->setCellValue("BD$i", '')
			->setCellValue("BE$i", '')
			->setCellValue("BF$i", '')
			->setCellValue("BG$i", '')
			->setCellValue("BH$i", '')
			->setCellValue("BI$i", '')
			->setCellValue("BJ$i", $row['nuovo_eco'])
			->setCellValue("BK$i", '')
			->setCellValue("BL$i", '')
			->setCellValue("BM$i", '')
			->setCellValue("BN$i", '')
			->setCellValue("BO$i", $row['stato'])
			->setCellValue("BP$i", $row['prova_gratuita'])
			->setCellValue("BQ$i", $row['trasporto_gratuito'])
			->setCellValue("BR$i", '')
			->setCellValue("BS$i", '')
			->setCellValue("BT$i", '')
			->setCellValue("BU$i", '')
			->setCellValue("BV$i", '')
			->setCellValue("BW$i", '')
			->setCellValue("BX$i", $row['canonical'])
			->setCellValue("BY$i", '')
			->setCellValue("BZ$i", '')
			->setCellValue("CA$i", '')
			->setCellValue("CB$i", '')
			->setCellValue("CC$i", '')
			->setCellValue("CD$i", '')
			->setCellValue("CE$i", '')
			->setCellValue("CF$i", '')
			->setCellValue("CG$i", $row['redirect'])
			->setCellValue("CH$i", str_replace(":::::",";",$row['punti_di_forza']))
			->setCellValue("CI$i", '')
			->setCellValue("CJ$i", '')
			->setCellValue("CK$i", '')
			->setCellValue("CL$i", '')
			->setCellValue("CM$i", '')
			->setCellValue("CN$i", '')
			->setCellValue("CO$i", '')
			->setCellValue("CP$i", '')
			->setCellValue("CQ$i", '')
			->setCellValue("CR$i", '')
			->setCellValue("CS$i", '')
			->setCellValue("CT$i", '')
			->setCellValue("CU$i", '')
			->setCellValue("CV$i", '')
			->setCellValue("CW$i", '')
			->setCellValue("CX$i", '')
			->setCellValue("CY$i", '')
			->setCellValue("CZ$i", '')
			->setCellValue("DA$i", '')
			->setCellValue("DB$i", '')
			->setCellValue("DC$i", '')
			->setCellValue("DD$i", '')
			->setCellValue("DF$i", '')
			->setCellValue("DG$i", '')
			->setCellValue("DI$i", "")
			->setCellValue("DJ$i", "")
			->setCellValue("DK$i", "")
			->setCellValue("DL$i", "")
			->setCellValue("DM$i", "")
			->setCellValue("DN$i", "")
			->setCellValue("DO$i", "")
			->setCellValue("DP$i", "")
			->setCellValue("DQ$i", "")
			->setCellValue("DR$i", "")
			->setCellValue("DS$i", "")
			->setCellValue("DT$i", "")
			->setCellValue("DU$i", "")
			->setCellValue("DV$i", "")
			->setCellValue("DW$i", "")
			->setCellValue("DX$i", "")
			->setCellValue("DY$i", "")
			->setCellValue("DZ$i", "")
			->setCellValue("EA$i", "")
			->setCellValue("EB$i", "")
			->setCellValue("EC$i", "")

			;

// CATEGORIE

$query1 = "SELECT category_lang.id_category, category_lang.name FROM category_lang JOIN product ON category_lang.id_category = product.id_category_default WHERE category_lang.id_lang = 5 AND product.id_product = $row[id_prestashop]";
$result1 = mysql_query($query1, $link_global);
while ($row1 = mysql_fetch_array($result1, MYSQL_ASSOC)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("AC$i", $row1['name']);
} 
// NOINDEX

if($row['noindex'] == 1) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("AN$i", (iconv("ISO-8859-1", "UTF-8", 'si')));
} else { }

//COMPARAPREZZI

if($row['trovaprezzi'] == 1) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("AO$i", (iconv("ISO-8859-1", "UTF-8", 's�')));
} else { }

if($row['eprice'] == 1) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("AP$i", (iconv("ISO-8859-1", "UTF-8", 's�')));
} else { }

$amazon_check = explode(";",$row['amazon']);

if(in_array("5",$amazon_check))
{
	$objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("AX$i", (iconv("ISO-8859-1", "UTF-8", 'si')));	
}

if(in_array("2",$amazon_check))
{
	$objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("AY$i", (iconv("ISO-8859-1", "UTF-8", 'si')));	
}

if(in_array("3",$amazon_check))
{
	$objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("AZ$i", (iconv("ISO-8859-1", "UTF-8", 'si')));	
}

if(in_array("4",$amazon_check))
{
	$objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("BA$i", (iconv("ISO-8859-1", "UTF-8", 'si')));	
}

if(in_array("1",$amazon_check))
{
	$objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("BB$i", (iconv("ISO-8859-1", "UTF-8", 'si')));	
}
if(in_array("6",$amazon_check))
{
	$objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("BC$i", (iconv("ISO-8859-1", "UTF-8", 'si')));	
}
if($row['google_shopping'] == 1) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("AR$i", (iconv("ISO-8859-1", "UTF-8", 's�')));
} else { }

// SCONTI QUANTITA

$query1 = "SELECT * FROM specific_price WHERE id_product = $row[id_prestashop] AND (specific_price_name = 'sc_qta_1' OR specific_price_name = 'sc_qta_2' OR specific_price_name = 'sc_qta_3') AND id_group = 1 ORDER BY specific_price_name ASC";
$result1 = mysql_query($query1, $link_global);
$cella[0] = "V";
$cella[1] = "W";
$cella[2] = "X";
$k = 0;
while ($row1 = mysql_fetch_array($result1, MYSQL_ASSOC)) {
$objPHPExcel->setActiveSheetIndex(0)
             ->setCellValue($cella[$k]."$i", $row1['from_quantity']."_".$row1['reduction']*100);
			$k++;
}


// SCONTI RIVENDITORI

$query1 = "SELECT * FROM specific_price WHERE id_product = $row[id_prestashop] AND (specific_price_name = 'sc_riv_1' OR specific_price_name = 'sc_riv_2' OR specific_price_name = 'sc_riv_3') ORDER BY specific_price_name ASC";
$result1 = mysql_query($query1, $link_global);
$cella[0] = "Y";

$k = 0;
while ($row1 = mysql_fetch_array($result1, MYSQL_ASSOC)) {
$objPHPExcel->setActiveSheetIndex(0)
             ->setCellValue($cella[$k]."$i", $row1['reduction']*100);
			$k++;
}



												// TIPO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CK$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 783", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CK$i")->setDataValidation($objValidation);
	
							// UTILIZZO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CL$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 784", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CL$i")->setDataValidation($objValidation);
	
			
					// AGGIORNATO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("F$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8", "s�")).',no, "');
	$objPHPExcel->getActiveSheet()->getCell("F$i")->setDataValidation($objValidation);
	

	
		// Garanzia
$objValidation = $objPHPExcel->getActiveSheet()->getCell("AV$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"12 mesi, 24 mesi, 36 mesi"');
	$objPHPExcel->getActiveSheet()->getCell("CI$i")->setDataValidation($objValidation);

	
		// NUOVO ECO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("BJ$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"new,used,refurbished"');
	$objPHPExcel->getActiveSheet()->getCell("BJ$i")->setDataValidation($objValidation);
	
	// HOME PAGE
$objValidation = $objPHPExcel->getActiveSheet()->getCell("BE$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8", "s�")).',no"');
	$objPHPExcel->getActiveSheet()->getCell("BE$i")->setDataValidation($objValidation);
	
	
		// PROVA GRATUITA
$objValidation = $objPHPExcel->getActiveSheet()->getCell("BP$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8", "s�")).',no"');
	$objPHPExcel->getActiveSheet()->getCell("BP$i")->setDataValidation($objValidation);
	
	
		// TRASP GRATUITO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("BQ$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8", "s�")).',no"');
	$objPHPExcel->getActiveSheet()->getCell("BQ$i")->setDataValidation($objValidation);
	
	
		// MOSTRA TRASP GRATUITO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CA$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8", "s�")).',no"');
	$objPHPExcel->getActiveSheet()->getCell("CA$i")->setDataValidation($objValidation);
	

	// AGGIORNATO
	$query1 = "SELECT aggiornato FROM product_other WHERE id_product = '$row[id_prestashop]'";
	$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("F$i", (iconv("ISO-8859-1", "UTF-8", $row1['aggiornato'])));
}
	
	
	// NUOVO ECO
	$query1 = "SELECT condition FROM product WHERE id_product = '$row[id_prestashop]'";
	$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("BJ$i", (iconv("ISO-8859-1", "UTF-8", $row1['nuovo_eco'])));
}

	// PROVA GRATUITA
	$query1 = "SELECT prova_gratuita FROM product_other WHERE id_product = '$row[id_prestashop]'";
	$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("BP$i", (iconv("ISO-8859-1", "UTF-8", $row1['prova_gratuita'])));
}
	
	

$rowtrasportogratuito = Db::getInstance()->getValue("SELECT value FROM configuration WHERE id_configuration = 240");

	$artrasportogratuito = unserialize($rowtrasportogratuito);
	if(in_array($row['id_prestashop'], $artrasportogratuito))
		$trasp_gratis = 's�';
	else
		$trasp_gratis = '';
		
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue("BQ$i", (iconv("ISO-8859-1", "UTF-8", $trasp_gratis)));
	
		// MOSTRA TRASP GRATUITO
	$query1 = "SELECT mostra_spese_spedizione FROM product_other WHERE id_product = '$row[id_prestashop]'";
	$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CA$i", (iconv("ISO-8859-1", "UTF-8", $row1['mostra_spese_spedizione'])));
}
	// Tipo
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '783' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CK$i", $row1['valore']);
}

	// Utilizzo
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '784' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CL$i", $row1['valore']);
}
	
	// Confezione
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '450' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CJ$i", $row1['valore']);
}

// HOME PAGE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '452' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("BE$i", $row1['valore']);
}
	
	
// GARANZIA
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '449' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CI$i", $row1['valore']);
}



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////// SPECIFICHE//////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
						// MESSAGGI VOCALI SU LINEE
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CN$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 597", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CN$i")->setDataValidation($objValidation);
	
	
						// LIVELLI IVR OPA
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CO$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 598", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CO$i")->setDataValidation($objValidation);
	
						// QUANTITA MSG
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CP$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 599", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CP$i")->setDataValidation($objValidation);
	
					//CAMBIO MSG A ORARI IMPOSTATI
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CQ$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 600", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CQ$i")->setDataValidation($objValidation);
	
						// MOD CAMBIO MSG
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CR$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 601", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CR$i")->setDataValidation($objValidation);
	
						// VOICE MAIL SEGRETERIA
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CS$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 602", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CS$i")->setDataValidation($objValidation);
	
					// REGISTRAZIONE CONVERSAZIONI
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CT$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 603", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CT$i")->setDataValidation($objValidation);
	

						// WIDEBAND
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CU$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 496", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CU$i")->setDataValidation($objValidation);
	

						// messaggi musica con file	
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CV$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 604", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CV$i")->setDataValidation($objValidation);
	

						// NOTIFICA VOICE MAIL TO EMAIL
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CX$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 606", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("CX$i")->setDataValidation($objValidation);
	
	
							// tipo linee
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DH$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 607", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DH$i")->setDataValidation($objValidation);
	
	
							// tipo derivati int collegabili
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DJ$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 609", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DJ$i")->setDataValidation($objValidation);
	
	
							// slot per schede di equipaggiamento
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DM$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 612", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DM$i")->setDataValidation($objValidation);
	
							// terminali cordless gap collegabili
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DN$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 613", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DN$i")->setDataValidation($objValidation);
	
							// connessione cavi analog digit
$objValidation = $objPHPExcel->getActiveSheet()->getCell("DX$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 618", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("DX$i")->setDataValidation($objValidation);
	

						// COLLEGAMENTO FONTE MUSICA ESTERNA
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EB$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 622", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EB$i")->setDataValidation($objValidation);
	

	
						// COLL. TO CERCAPERSONE
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EC$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 623", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EC$i")->setDataValidation($objValidation);
	

						// SELEZIONE LINEE USCITA
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EH$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 624", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EH$i")->setDataValidation($objValidation);
	
					// RICONOSCIMENTO FAX AUTOMATICO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EJ$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 626", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EJ$i")->setDataValidation($objValidation);
	

	
						// LCR
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EK$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 548", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EK$i")->setDataValidation($objValidation);
	
					// TRASFERTA MANUALE VERSO ESTERNO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EL$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 627", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EL$i")->setDataValidation($objValidation);
	

	
						// DEVIAZIONE  NUMERO EST
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EM$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 628", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EM$i")->setDataValidation($objValidation);
	

						// CONFERENZA
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EN$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 485", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EN$i")->setDataValidation($objValidation);
	

						// SVEGLIA
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EO$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 579", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EO$i")->setDataValidation($objValidation);
	

						// FUNZIONI HOTEL
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EP$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 630", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EP$i")->setDataValidation($objValidation);
	

	
						// VIDEOCHIAMATE VOIP
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EQ$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 631", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EQ$i")->setDataValidation($objValidation);
	

	
						// RESTRIZIONE CHIAMATE IN USCITA
$objValidation = $objPHPExcel->getActiveSheet()->getCell("ER$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 632", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("ER$i")->setDataValidation($objValidation);
	

	
						// FAX TO EMAIL
$objValidation = $objPHPExcel->getActiveSheet()->getCell("ES$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 633", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("ES$i")->setDataValidation($objValidation);
	

	
	
						// FAX SERVER
$objValidation = $objPHPExcel->getActiveSheet()->getCell("ET$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 634", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("ET$i")->setDataValidation($objValidation);
	

						// TELEGESTIONE
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EU$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 635", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EU$i")->setDataValidation($objValidation);
	

						// CLCIK AND CALL DA PC
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EV$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 636", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EV$i")->setDataValidation($objValidation);
	
	
							// supporta citofoni
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EW$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 637", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EW$i")->setDataValidation($objValidation);
	
							// fissi collegabili
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EX$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 638", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EX$i")->setDataValidation($objValidation);
	
							// cordless collegabili
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EY$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 640", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EY$i")->setDataValidation($objValidation);
	
	
	

						// SUPPORTA SOFTPHONES
$objValidation = $objPHPExcel->getActiveSheet()->getCell("EZ$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 639", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("EZ$i")->setDataValidation($objValidation);
	

						// SUPPORTA LINEE VOIP SIP
$objValidation = $objPHPExcel->getActiveSheet()->getCell("FA$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 641", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("FA$i")->setDataValidation($objValidation);
	

						// INTEGRAZIONE CELL
$objValidation = $objPHPExcel->getActiveSheet()->getCell("FB$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 642", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("FB$i")->setDataValidation($objValidation);
	

						// CDR
$objValidation = $objPHPExcel->getActiveSheet()->getCell("FC$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 643", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("FC$i")->setDataValidation($objValidation);
	
					// DOCUMENTAZIONE IN ITALIANO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("FD$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 644", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("FD$i")->setDataValidation($objValidation);
	
							// amministrazione
$objValidation = $objPHPExcel->getActiveSheet()->getCell("FE$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 645", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("FE$i")->setDataValidation($objValidation);
	
					// T38 FAX
$objValidation = $objPHPExcel->getActiveSheet()->getCell("FF$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 646", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("FF$i")->setDataValidation($objValidation);
	
					// PASSWORD PERSONALE
$objValidation = $objPHPExcel->getActiveSheet()->getCell("FG$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 647", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("FG$i")->setDataValidation($objValidation);
	
					// BLACKLIST
$objValidation = $objPHPExcel->getActiveSheet()->getCell("FH$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 648", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("FH$i")->setDataValidation($objValidation);
	
		// DECT
$objValidation = $objPHPExcel->getActiveSheet()->getCell("FI$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 649", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("FI$i")->setDataValidation($objValidation);
	
					// PAGING SU TELEFONI
$objValidation = $objPHPExcel->getActiveSheet()->getCell("FJ$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 650", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("FJ$i")->setDataValidation($objValidation);
	

						// PAGING SU CERCAPERS
$objValidation = $objPHPExcel->getActiveSheet()->getCell("FK$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 651", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("FK$i")->setDataValidation($objValidation);
	
		// COMPATIBILE SKYPE
$objValidation = $objPHPExcel->getActiveSheet()->getCell("FL$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 771", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("FL$i")->setDataValidation($objValidation);
	
	// COMPATIBILE SKYPE
$objValidation = $objPHPExcel->getActiveSheet()->getCell("FM$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 916", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("FM$i")->setDataValidation($objValidation);
	

						// BATTERIE ESTERNE SOCCORSO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("FS$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 652", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("FS$i")->setDataValidation($objValidation);
	
						// installazione e certificazione
$objValidation = $objPHPExcel->getActiveSheet()->getCell("GC$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 653", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("GC$i")->setDataValidation($objValidation);
	
							// posa
$objValidation = $objPHPExcel->getActiveSheet()->getCell("GD$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 654", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("GD$i")->setDataValidation($objValidation);
	
	
	
							// supporto da remoto a installazione
$objValidation = $objPHPExcel->getActiveSheet()->getCell("GE$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 655", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("GE$i")->setDataValidation($objValidation);
	
	
							// contratto assistenza postvendita
$objValidation = $objPHPExcel->getActiveSheet()->getCell("GF$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 656", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("GF$i")->setDataValidation($objValidation);
	
							// noleggio
$objValidation = $objPHPExcel->getActiveSheet()->getCell("GK$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
	$qspec = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value_lang.id_feature_value = feature_value.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.custom = 0 AND feature_value.id_feature = 657", $link_global);
	$strspec = "";
	while ($rspec = mysql_fetch_array($qspec, MYSQL_ASSOC)) {
	$strspec .= (utf8_decode($rspec['value'])).",";
	}
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8",$strspec)).'"');
	$objPHPExcel->getActiveSheet()->getCell("GK$i")->setDataValidation($objValidation);
	
	
	
// MESSAGGI VOCALI SU LINEE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '597' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CN$i", $row1['valore']);
}


// LIVELLI IVR OPA
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '598' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CO$i", $row1['valore']);
}

// QUANTITA MSG FASCE ORARIE DI LAVORO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '599' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CP$i", $row1['valore']);
}


// CAMBIO MSG A ORARI IMPOSTATI
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '600' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CQ$i", $row1['valore']);
}


// MODALITA CAMBIO MSG
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '601' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CR$i", $row1['valore']);
}



// VOICE MAIL
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '602' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CS$i", $row1['valore']);
}

// REGISTRA CONVERSDAZIONI
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '603' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CT$i", $row1['valore']);
}


// WIDEBAND
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '496' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CU$i", $row1['valore']);
}

// MESSAGGI MUSICA CON FILE 
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '604' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CV$i", $row1['valore']);
}

// MEMORIA REGISTRAZIONE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '605' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CW$i", $row1['valore']);
}

// NOTIFICAVOICEMAIL
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '606' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CX$i", $row1['valore']);
}

// RUBRICA MEMORIE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '513' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DC$i", $row1['valore']);
}


// TIPO LINEE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '607' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DH$i", $row1['valore']);
}

// LINEE ESTERNE A BORDO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '608' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DI$i", $row1['valore']);
}

// DERIVATI INT COLLEGABILI TIPO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '609' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DJ$i", $row1['valore']);
}

// DERIVFATI A BORDO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '610' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DK$i", $row1['valore']);
}

// CAPACITA MASSIMA PORTE LINEE INTERNI
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '611' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DL$i", $row1['valore']);
}

// SLOT SCHEDE EQUIPAGGIAMENTO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '612' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DM$i", $row1['valore']);
}



// TERMIONALI CORDLESS GAP COLL
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '613' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DN$i", $row1['valore']);
}


// PORTE LAN
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '576' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DT$i", $row1['valore']);
}


// PORTE RS32
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '615' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DU$i", $row1['valore']);
}



// PORTE USB
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '616' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DV$i", $row1['valore']);
}

// PORTE WAN
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '617' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DW$i", $row1['valore']);
}

// CONNESSIONE CAVI
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '618' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DX$i", $row1['valore']);
}

// CODEC AUDIO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '619' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DY$i", $row1['valore']);
}


// CODEC VIDEO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '620' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("DZ$i", $row1['valore']);
}

// TAPI
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '621' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EA$i", $row1['valore']);
}

// COLL FONTE MUSICALE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '622' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EB$i", $row1['valore']);
}

// COLL CERCAPERSONE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '623' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EC$i", $row1['valore']);
}

// LINEE USCITA
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '624' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EH$i", $row1['valore']);
}

// CONVERSAZIONI SIMULTANEE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '625' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EI$i", $row1['valore']);
}

// RICONOSCIMENTO FAX
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '626' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EJ$i", $row1['valore']);
}

// LCR
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '548' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EK$i", $row1['valore']);
}

// TRASFERTA MANUALE ESTERNO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '627' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EL$i", $row1['valore']);
}

// DEVIAZIONE NUMERO ESTERNO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '628' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EM$i", $row1['valore']);
}

// CONFERENZA
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '485' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EN$i", $row1['valore']);
}


// SVEGLIA
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '579' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EO$i", $row1['valore']);
}

// FUNZIONI HOTEL
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '630' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EP$i", $row1['valore']);
}

// VIDEOCHIAMATE VOIP
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '631' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EQ$i", $row1['valore']);
}

// RESTRIZIONE CHIAMATA IN USCITA
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '632' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("ER$i", $row1['valore']);
}

// FAX TO EMAIL
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '633' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("ES$i", $row1['valore']);
}

// FAX SERVER
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '634' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("ET$i", $row1['valore']);
}

// TELEGESTIONE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '635' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EU$i", $row1['valore']);
}

// CLCIK AND CALL
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '636' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EV$i", $row1['valore']);
}


// SUPPORTA CITOFONI
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '637' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EW$i", $row1['valore']);
}


// FISSI COLLEGABILI
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '638' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EX$i", $row1['valore']);
}

// CORDLESS COLLEGABILI
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '640' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EY$i", $row1['valore']);
}

// SUPPORTA SOFTPHONES
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '639' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("EZ$i", $row1['valore']);
}

// SUPPORTA LINEE VOIP SIP
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '641' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("FA$i", $row1['valore']);
}


// INTEGRAZIONE TEL CELLULARI
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '642' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("FB$i", $row1['valore']);
}

// CDR
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '643' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("FC$i", $row1['valore']);
}

// DOCUMENTAZIONE IN ITALIANO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '644' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("FD$i", $row1['valore']);
}

// AMMINISTRAZIONE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '645' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("FE$i", $row1['valore']);
}

// T38 FAX
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '646' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("FF$i", $row1['valore']);
}

// PASSWORD PERSONALE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '647' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("FG$i", $row1['valore']);
}

// BLACKLIST
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '648' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("FH$i", $row1['valore']);
}


// DECT MULTICELLA
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '649' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("FI$i", $row1['valore']);
}

// PAGING TEL
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '650' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("FJ$i", $row1['valore']);
}

// PAGING PA
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '651' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("FK$i", $row1['valore']);
}

// COMPATIBILE SKYPE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '771' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("FL$i", $row1['valore']);
}

// RIDONDANZA
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '916' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("FM$i", $row1['valore']);
}

// CONSUMO ENERGETICO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '562' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("FQ$i", $row1['valore']);
}

// ALIMENTAZIONE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '459' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("FR$i", $row1['valore']);
}

// BATTERIE ESTERNE SOCCORSO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '652' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("FS$i", $row1['valore']);
}

// DIMENSIONI
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '560' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("FW$i", $row1['valore']);
}

// PESO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '559' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("FX$i", $row1['valore']);
}

// COLORE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '453' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("FY$i", $row1['valore']);
}

// INSTALLAZIONE E CERTIFICAZIONE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '653' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("GC$i", $row1['valore']);
}


// POSA
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '654' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("GD$i", $row1['valore']);
}

// SUPPORTO REMOTO INSTALL
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '655' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("GE$i", $row1['valore']);
}

// ASS POSTVENDITA
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '656' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("GF$i", $row1['valore']);
}

// NOLEGGIO
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '657' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("GK$i", $row1['valore']);
}




	////////////////////////////////////////////////////////////////////////////////////////////////// SPECIFICHE//////////////////////////////////////////////









$i++; // INCREMENTO ----------------------------------------------------------------------------------------------------------------------------------------



}





			
$highestRow = $objPHPExcel->getActiveSHeet()->getHighestRow();
$objPHPExcel->getActiveSheet()->getStyle('A1:GK1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);			
$objPHPExcel->getActiveSheet()->getStyle('A1:CM1')->getFill()->getStartColor()->setRGB('FFFF00');
$objPHPExcel->getActiveSheet()->getStyle('O1:Q1')->getFill()->getStartColor()->setRGB('FFFFFF'); //BIANCO
$objPHPExcel->getActiveSheet()->getStyle('R1:Y1')->getFill()->getStartColor()->setRGB('99FFFF'); //azzurro
$objPHPExcel->getActiveSheet()->getStyle('CN1:DB1')->getFill()->getStartColor()->setRGB('6666FF'); // blu	

$objPHPExcel->getActiveSheet()->getStyle('DC1:DG1')->getFill()->getStartColor()->setRGB('99FFFF'); // azzurro

$objPHPExcel->getActiveSheet()->getStyle('DH1:DS1')->getFill()->getStartColor()->setRGB('FF99FF'); //lilla
$objPHPExcel->getActiveSheet()->getStyle('DT1:EG1')->getFill()->getStartColor()->setRGB('CCCCCC'); //grigio
$objPHPExcel->getActiveSheet()->getStyle('EH1:FP1')->getFill()->getStartColor()->setRGB('FFCC66'); //ocra

$objPHPExcel->getActiveSheet()->getStyle('FQ1:FV1')->getFill()->getStartColor()->setRGB('FF3333'); //ROSSO
$objPHPExcel->getActiveSheet()->getStyle('FW1:GB1')->getFill()->getStartColor()->setRGB('FF66CC'); //FUCSIA

$objPHPExcel->getActiveSheet()->getStyle('GC1:GJ1')->getFill()->getStartColor()->setRGB('999999'); //GRIGIO SCURO

$objPHPExcel->getActiveSheet()->getStyle('GK1')->getFill()->getStartColor()->setRGB('CC6633'); //MARRONE

	
$objPHPExcel->getActiveSheet()->getStyle("A1:GK$i")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A1:GK1')->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth('70');
$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth('70');
$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth('70');
$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth('70');
$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth('70');
$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth('70');
$objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setWidth('80');
$objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('AW')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('BA')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BE')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BF')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BG')->setWidth('50');
$objPHPExcel->getActiveSheet()->getColumnDimension('BI')->setWidth('80');
$objPHPExcel->getActiveSheet()->getColumnDimension('BJ')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BO')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BP')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BQ')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BR')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BS')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BT')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BU')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BV')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BW')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BX')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BY')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BZ')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('CA')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CB')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('CD')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CE')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CF')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CG')->setWidth('50');
$objPHPExcel->getActiveSheet()->getColumnDimension('CH')->setWidth('50');
$objPHPExcel->getActiveSheet()->getColumnDimension('CI')->setWidth('50');
$objPHPExcel->getActiveSheet()->getColumnDimension('CJ')->setWidth('50');
$objPHPExcel->getActiveSheet()->getColumnDimension('CN')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CO')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CP')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CQ')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CR')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CS')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CT')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CU')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CV')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CW')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CX')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CY')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CZ')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DA')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DB')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DC')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DD')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DE')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DF')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DG')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DH')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DI')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DJ')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DK')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DL')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DM')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DN')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DO')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DP')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DQ')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DR')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DS')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DT')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DU')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DV')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DW')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DX')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DY')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('DZ')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EA')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EB')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EC')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('ED')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EE')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EF')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EG')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EH')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EI')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EJ')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EK')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EL')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EM')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EN')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EO')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EP')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EQ')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('ER')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('ES')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('ET')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EU')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EV')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EW')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EX')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EY')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('EZ')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FA')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FB')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FC')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FD')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FE')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FF')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FG')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FH')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FI')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FJ')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FK')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FL')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FM')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FN')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FO')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FP')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FQ')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FR')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FS')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FT')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FU')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FV')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FW')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FX')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FY')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('FZ')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('GA')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('GB')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('GC')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('GD')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('GE')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('GF')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('GG')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('GH')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('GI')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('GJ')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('GK')->setWidth('30');

$objPHPExcel->getActiveSheet()->getStyle("J2:J$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("K2:K$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("L2:L$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("M2:M$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("N2:N$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("O2:O$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("P2:P$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("Q2:Q$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("R2:R$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("S2:S$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("T2:T$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("U2:U$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("V2:V$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("X2:X$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("Y2:Y$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("W2:W$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("AA2:AA$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("AB2:AB$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

$objPHPExcel->getActiveSheet()->setTitle('Cordless');

$objPHPExcel->getActiveSheet()->getStyle("J2:J$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:GK$i")->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
/* $objPHPExcel->getActiveSheet()->getStyle("P2:P$i")->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED); */

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

$data = date("Ymd");

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->setPreCalculateFormulas(false);
$objWriter->save(str_replace(".php", ".xls", "esportazione-catalogo/catalogo_xls/centralini-$data.php"));

echo "<br /><br />Il file &egrave; pronto per essere scaricato: <a onclick='window.onbeforeunload = null' href='http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/centralini-$data.xls'>clicca qui per eseguire il download</a>!";

?>




