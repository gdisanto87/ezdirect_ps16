<?php

$connection = mysql_connect(_DB_SERVER_,_DB_USER_,_DB_PASSWD_);
mysql_select_db(_DB_NAME_,$connection);
$link_global = $connection;

//$connection = mysql_connect("localhost","root",""); 
//mysql_select_db("ezdirect",$connection);

ini_set("memory_limit","892M");
set_time_limit(3600);

$query = "SELECT
product.id_product AS id_prestashop,
product.reference AS codice_spring,
product_other.aggiornato AS aggiornato,
product.serial AS seriale,
(CASE WHEN product.provvigione > 0 THEN product.provvigione ELSE '' END) AS provvigione,
product.wholesale_price AS prezzo_acquisto,
supplier.name AS fornitore,
product.date_available AS data_disponibilita,
product.stock_quantity AS mag_spring,
product.sconto_acquisto_1 AS sconto_acquisto_1,
product.sconto_acquisto_2 AS sconto_acquisto_2,
product.sconto_acquisto_3 AS sconto_acquisto_3,
product.scontolistinovendita AS scontolistinovendita,
product_other.sconto_rivenditore_1 AS sc_riv_1,
product_other.sconto_rivenditore_2 AS sc_riv_2,
product_other.sconto_rivenditore_1_min AS sc_riv_1_min,
product_other.sconto_rivenditore_1_max AS sc_riv_1_max,
product_other.sconto_rivenditore_2_min AS sc_riv_2_min,
product_other.sconto_rivenditore_2_max AS sc_riv_2_max,
product_other.directel AS directel,
product_other.redirect AS redirect,
product_other.wordpress_1 AS wordpress_1,
product_other.wordpress_2 AS wordpress_2,
product_other.wordpress_3 AS wordpress_3,
product.canonical AS canonical,
product.reference AS codice_prodotto,
product.supplier_reference AS codice_costruttore,
product_lang.name AS nome_prodotto,
product.ean13 AS codice_ean,
product.listino AS prezzo_listino,
product.price AS prezzo_ezdirect,
product.quantity AS quantita,
product.stock_quantity AS mag_ez,
product.supplier_quantity AS mag_allnet,
product.esprinet_quantity AS mag_esprinet,
product.itancia_quantity AS mag_itancia,
product_lang.description AS descrizione,
product_lang.available_now AS msg_disponibile,
product_lang.available_later AS msg_non_disponibile,
product.weight AS peso,
product.id_category_default AS id_categoria_prestashop,
product.id_manufacturer AS id_costruttore_prestashop,
manufacturer.name AS costruttore,
product_other.prova_gratuita AS prova_gratuita,
product_other.trasporto_gratuito AS trasporto_gratuito,
product_other.mostra_spese_spedizione AS mostra_spese_spedizione,
product.condition AS nuovo_eco,
product.date_upd AS data_ultima_modifica,
product.date_add AS data_inserimento,
product_other.garanzia_3_anni AS garanzia_3_anni,
product_other.assistenza_3_anni AS assistenza_3_anni,
product_lang.description_amazon AS descrizione_amazon,
product_lang.meta_description AS metatag_description,
product_lang.meta_title AS title_tag,
product_lang.meta_keywords AS metatag_keywords,
product_lang.punti_di_forza AS punti_di_forza,
product_lang.description_short AS descrizione_breve,
product_lang.cat_homepage AS cat_homepage,
product_lang.desc_homepage AS desc_homepage,
product.trovaprezzi AS trovaprezzi,
product.google_shopping AS google_shopping,
product.eprice AS eprice,
product.amazon AS amazon,
product.asin AS asin,
product.active AS stato,
product.noindex AS noindex,
pe.rebate_1 AS rebate_1,
pe.rebate_2 AS rebate_2,
pe.rebate_3 AS rebate_3
FROM
  product
  JOIN product_lang ON (product.id_product = product_lang.id_product)
  LEFT JOIN product_other ON (product.id_product = product_other.id_product)
  LEFT JOIN product_esolver pe ON pe.id_product = product.id_product
  LEFT JOIN manufacturer ON (product.id_manufacturer = manufacturer.id_manufacturer)
  LEFT JOIN supplier ON (product.id_supplier = supplier.id_supplier)
WHERE
 product_lang.id_lang = 5 AND (product.id_category_default = '246')
".(Tools::getIsset('senza_fp') ? ' AND product.fuori_produzione = 0' : '')."
















GROUP BY id_prestashop
ORDER BY id_costruttore_prestashop
  
  ";

$result = mysql_query($query, $link_global);

error_reporting(E_ALL);

date_default_timezone_set('Europe/Rome');

/** PHPExcel */
require_once 'Classes/PHPExcel.php';


// Create new PHPExcel object
echo date('H:i:s') . " Creato nuovo file di Excel... !";
$objPHPExcel = new PHPExcel();

// Set properties

$i = 2;

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Id prestashop')
            ->setCellValue('B1', 'Codice costruttore')
            ->setCellValue('C1', 'Codice EZ Spring')
            ->setCellValue('D1', 'Codice EAN')
            ->setCellValue('E1', 'ASIN Amazon')
            ->setCellValue('F1', 'Aggiornato')
            ->setCellValue('G1', 'Data ultima modifica')
            ->setCellValue('H1', 'Costruttore')
            ->setCellValue('I1', 'Nome prodotto')
            ->setCellValue('J1', 'Listino')
            ->setCellValue('K1', 'Sc acq 1')
            ->setCellValue('L1', 'Sc acq 2')
            ->setCellValue('M1', 'Sc acq 3')
			->setCellValue('N1', 'Netto eSolver')
            ->setCellValue('O1', 'Rebate 1')
			->setCellValue('P1', 'Rebate 2')
            ->setCellValue('Q1', 'Rebate 3')
            ->setCellValue('R1', 'Netto SALES')
            ->setCellValue('S1', 'Sc clienti')
            ->setCellValue('T1', 'Web')
            ->setCellValue('U1', (iconv("ISO-8859-1", "UTF-8", 'Marg')))
            ->setCellValue('V1', (iconv("ISO-8859-1", "UTF-8", 'Sc qt 1')))
            ->setCellValue('W1', (iconv("ISO-8859-1", "UTF-8", 'Sc qt 2')))
            ->setCellValue('X1', (iconv("ISO-8859-1", "UTF-8", 'Sc qt 3')))
            ->setCellValue('Y1', 'Sc riv 1')
            ->setCellValue('Z1', 'Premio Ag. Personalizzato')
            ->setCellValue('AA1', 'Fornitore')
            ->setCellValue('AB1', 'Qt. tot.')
            ->setCellValue('AC1', 'Categoria prestashop')
            ->setCellValue('AD1', 'Peso confezione')
            ->setCellValue('AE1', 'Data inserimento')
            ->setCellValue('AF1', 'Descrizione HTML')
            ->setCellValue('AG1', 'Descrizione breve (listing)')
            ->setCellValue('AH1', 'Descrizione Amazon')
            ->setCellValue('AI1', 'Title tag')
            ->setCellValue('AJ1', 'Metatag description')
            ->setCellValue('AK1', 'Keywords')
            ->setCellValue('AL1', 'Categoria home page')
            ->setCellValue('AM1', 'Descrizione home page')
            ->setCellValue('AN1', 'Noindex')
            ->setCellValue('AO1', 'Trovaprezzi')
            ->setCellValue('AP1', 'ePrice')
            ->setCellValue('AQ1', '')
            ->setCellValue('AR1', 'Google Shopping')
            ->setCellValue('AS1', 'Messaggio disponibile')
            ->setCellValue('AT1', 'Messaggio non disponibile')
            ->setCellValue('AU1', '')
            ->setCellValue('AV1', '')
            ->setCellValue('AW1', (iconv("ISO-8859-1", "UTF-8", 'Data disponibilit�')))
            ->setCellValue('AX1', 'Amazon IT')
            ->setCellValue('AY1', 'Amazon FR')
			->setCellValue('AZ1', 'Amazon ES')
			->setCellValue('BA1', 'Amazon DE')
			->setCellValue('BB1', 'Amazon UK')
			->setCellValue('BC1', 'Amazon NL')
			->setCellValue('BD1', '')
			->setCellValue('BE1', '')
			->setCellValue('BF1', '')
			->setCellValue('BG1', '')
			->setCellValue('BH1', '')
			->setCellValue('BI1', '')
			->setCellValue('BJ1', 'Nuovo/Eco')
			->setCellValue('BK1', '')
			->setCellValue('BL1', '')
			->setCellValue('BM1', '')
			->setCellValue('BN1', '')
			->setCellValue('BO1', 'Attivo (1)')
			->setCellValue('BP1', 'Prova gratuita')
			->setCellValue('BQ1', 'Trasporto gratis')
			->setCellValue('BR1', 'Immagine listing prodotti')
			->setCellValue('BS1', '')
			->setCellValue('BT1', '')
			->setCellValue('BU1', '')
			->setCellValue('BV1', '')
			->setCellValue('BW1', '')
			->setCellValue('BX1', '')
			->setCellValue('BY1', '')
			->setCellValue('BZ1', '')
			->setCellValue('CA1', '')
			->setCellValue('CB1', '')
			->setCellValue('CC1', '')
			->setCellValue('CD1', '')
			->setCellValue('CE1', '')
			->setCellValue('CF1', '')
			->setCellValue('CG1', 'Redirect')
			->setCellValue('CH1', 'Punto di forza')
			->setCellValue('CI1', 'Garanzia')
			->setCellValue('CJ1', 'Confezione')
			->setCellValue('CK1', 'Accessori')
			->setCellValue('CL1', 'Tipo')
			->setCellValue('CM1', '')
		
			;



$objPHPExcel->getActiveSheet()->getStyle("B")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("C")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("D")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("E")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("G")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("J")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("K")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("L")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("M")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("N")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("O")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("P")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("Q")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("R")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("S")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("T")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("U")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("Y")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("Z")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
$objPHPExcel->getActiveSheet()->getStyle("W")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
///////////////// campi specifici categoria
$objPHPExcel->getActiveSheet()->getStyle("DE")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("DH")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->getStyle("DT")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
///////////////// fine campi specifici categoria


//manuali e immagini

$objPHPExcel->getActiveSheet()->getColumnDimension('BR')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BS')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BT')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BU')->setVisible(false);
//fine manuali e immagini


$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setVisible(false);

$objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setVisible(false);

$objPHPExcel->getActiveSheet()->getColumnDimension('BD')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BE')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BF')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BG')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BH')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BI')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BK')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BL')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BM')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BN')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BU')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BW')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BX')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BY')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('BZ')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CA')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CB')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CC')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CD')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CE')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CF')->setVisible(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('CM')->setVisible(false);


if(Tools::getIsset('senza_seo'))
{
	$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setVisible(false);
	$objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setVisible(false);
	
}



// QUERY PER TUTTE LE CATEGORIE

$query1 = "SELECT category_lang.id_category, category_lang.name FROM category_lang JOIN product ON category_lang.id_category = product.id_category_default WHERE id_lang = 5";
$result1 = mysql_query($query1, $link_global);
$string_cats = '';
while ($row1 = mysql_fetch_array($result1, MYSQL_ASSOC)) {
$string_cats .= $row1['name'].",";
} 


while($row = mysql_fetch_assoc($result)) {

$objPHPExcel->getActiveSheet()->getCell("B$i")->setValueExplicit($row['codice_costruttore'], PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("C$i")->setValueExplicit($row['codice_prodotto'], PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("D$i")->setValueExplicit($row['codice_ean'], PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("E$i")->setValueExplicit($row['asin'], PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("AB$i")->setValueExplicit($row['quantita'], PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("AD$i")->setValueExplicit($row['peso'], PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("DE$i")->setValueExplicit('', PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("DH$i")->setValueExplicit('', PHPExcel_Cell_DataType::TYPE_STRING);
$objPHPExcel->getActiveSheet()->getCell("DT$i")->setValueExplicit('', PHPExcel_Cell_DataType::TYPE_STRING);






if(Tools::getIsset('senza_seo'))
{
	$descrizione = ''; $descrizione_breve = ''; $row['descrizione_amazon'] = ''; $title = ''; $metatag_description = ''; $metatag_keywords = ''; $row['cat_homepage'] = ''; $row['desc_homepage'] = '';
	
}

else
{

	$descrizione = $row['descrizione'];
	$descrizione_breve = $row['descrizione_breve'];
	$title = $row['title_tag'];
	$metatag_description = $row['metatag_description'];
	$metatag_keywords = $row['metatag_keywords'];

	$descrizione = preg_replace("/<br>/","<br />",$row['descrizione']);
	$descrizione = preg_replace("/<b>/","<strong>",$descrizione);
	$descrizione = preg_replace("/<\/b>/","</strong>",$descrizione);
	$descrizione = preg_replace("/<i>/","<em>",$descrizione);
	$descrizione = preg_replace("/<\/i>/","</em>",$descrizione);

	// correzioni ortografiche
	$descrizione = preg_replace("/epr/","per",$descrizione);
	$descrizione_breve = preg_replace("/epr/","per",$descrizione_breve);
	$title = preg_replace("/epr/","per",$title);
	$metatag_description = preg_replace("/epr/","per",$metatag_description);
	$metatag_keywords = preg_replace("/epr/","per",$metatag_keywords);

	$descrizione = preg_replace("/teelfon/","telefon",$descrizione);
	$descrizione_breve = preg_replace("/teelfon/","telefon",$descrizione_breve);
	$title = preg_replace("/teelfon/","telefon",$title);
	$metatag_description = preg_replace("/teelfon/","telefon",$metatag_description);
	$metatag_keywords = preg_replace("/teelfon/","telefon",$metatag_keywords);

	$descrizione = preg_replace("/ms link/","MS Lync",$descrizione);
	$descrizione_breve = preg_replace("/ms link/","MS Lync",$descrizione_breve);
	$title = preg_replace("/ms link/","MS Lync",$title);
	$metatag_description = preg_replace("/ms link/","MS Lync",$metatag_description);
	$metatag_keywords = preg_replace("/ms link/","MS Lync",$metatag_keywords);

	$descrizione = preg_replace("/ms linc/","MS Lync",$descrizione);
	$descrizione_breve = preg_replace("/ms linc/","MS Lync",$descrizione_breve);
	$title = preg_replace("/ms linc/","MS Lync",$title);
	$metatag_description = preg_replace("/ms linc/","MS Lync",$metatag_description);
	$metatag_keywords = preg_replace("/ms linc/","MS Lync",$metatag_keywords);

	$descrizione = preg_replace("/ms lynk/","MS Lync",$descrizione);
	$descrizione_breve = preg_replace("/ms lynk/","MS Lync",$descrizione_breve);
	$title = preg_replace("/ms lynk/","MS Lync",$title);
	$metatag_description = preg_replace("/ms lynk/","MS Lync",$metatag_description);
	$metatag_keywords = preg_replace("/ms lynk/","MS Lync",$metatag_keywords);

	$descrizione = preg_replace("/plantroncis/","Plantronics",$descrizione);
	$descrizione_breve = preg_replace("/plantroncis/","Plantronics",$descrizione_breve);
	$title = preg_replace("/plantroncis/","Plantronics",$title);
	$metatag_description = preg_replace("/plantroncis/","Plantronics",$metatag_description);
	$metatag_keywords = preg_replace("/plantroncis/","Plantronics",$metatag_keywords);

	$descrizione = preg_replace("/Plantroncis/","Plantronics",$descrizione);
	$descrizione_breve = preg_replace("/Plantroncis/","Plantronics",$descrizione_breve);
	$title = preg_replace("/Plantroncis/","Plantronics",$title);
	$metatag_description = preg_replace("/Plantroncis/","Plantronics",$metatag_description);
	$metatag_keywords = preg_replace("/Plantroncis/","Plantronics",$metatag_keywords);

	//correzioni ortografiche

	$title = ucfirst($title);
	$metatag_description = ucfirst($metatag_description);
	$metatag_keywords = ucfirst($metatag_keywords);

	if(($metatag_description[strlen($metatag_description)-1]) != ".") { $metatag_description = $metatag_description."."; } else { }



	if(!preg_match("/<h1*./",$descrizione)) {
	$descrizione = "<h1>".$row['nome_prodotto']."</h1> ".$descrizione;
	}
}
if($row['prezzo_acquisto'] == 0 || $row['prezzo_acquisto'] == '' || !isset($row['prezzo_acquisto'])) {
$netto = 0;
}
else {
$netto = $row['prezzo_acquisto'];
}

if($row['scontolistinovendita'] == 0 && $row['prezzo_listino'] != $row['prezzo_ezdirect'])
{
	$row['scontolistinovendita'] = ((($row['prezzo_listino'] - $row['prezzo_ezdirect']) * 100)/$row['prezzo_listino']);
}	

if($row['rebate_1'] == '')
	$row['rebate_1'] = 0;

if($row['rebate_2'] == '')
	$row['rebate_2'] = 0;

if($row['rebate_3'] == '')
	$row['rebate_3'] = 0;

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A$i", $row['id_prestashop'])
			->setCellValue("F$i", '')
			->setCellValue("G$i", $row['data_ultima_modifica'])
			->setCellValue("H$i", (iconv("ISO-8859-1", "UTF-8",$row['costruttore'])))
			->setCellValue("I$i", (iconv("ISO-8859-1", "UTF-8",$row['nome_prodotto'])))
			->setCellValue("J$i", number_format($row['prezzo_listino'], 2, ',', ''))
			->setCellValue("K$i", $row['sconto_acquisto_1'])
			->setCellValue("L$i", $row['sconto_acquisto_2'])
			->setCellValue("M$i", $row['sconto_acquisto_3'])
			->setCellValue("N$i", "=ROUND((IF(K$i>=0;(J$i*((100-K$i)/100)*((100-L$i)/100)*((100-M$i)/100));$netto)),2)")
			->setCellValue("O$i", $row['rebate_1'])
			->setCellValue("P$i", $row['rebate_2'])
			->setCellValue("Q$i", $row['rebate_3'])
			->setCellValue("R$i", "=ROUND((IF(K$i>=0;(J$i*((100-K$i)/100)*((100-L$i)/100)*((100-M$i)/100)*((100-O$i)/100)*((100-P$i)/100)*((100-Q$i)/100));$netto)),2)")
            ->setCellValue("S$i", number_format($row['scontolistinovendita'], 2, ',' ,''))
            ->setCellValue("T$i", "=ROUND((IF(S$i>=0;J$i-(J$i*S$i/100);".number_format($row['prezzo_ezdirect'], 2, '.', '').")),2)")
            ->setCellValue("U$i",  "=ROUND((((T$i-R$i)*100)/T$i),2)")
            ->setCellValue("V$i", '')
            ->setCellValue("W$i", '')
			->setCellValue("X$i", '')
->setCellValue("Z$i", $row['provvigione'])
            ->setCellValue("AA$i", $row['fornitore'])
            ->setCellValue("AE$i", $row['data_inserimento'])
            
			->setCellValue("AF$i", (iconv("ISO-8859-1", "UTF-8",$descrizione)))
             ->setCellValue("AG$i", (iconv("ISO-8859-1", "UTF-8",$descrizione_breve)))
            ->setCellValue("AH$i", (iconv("ISO-8859-1", "UTF-8",$row['descrizione_amazon'])))
            ->setCellValue("AI$i", (iconv("ISO-8859-1", "UTF-8",$title)))
           ->setCellValue("AJ$i", (iconv("ISO-8859-1", "UTF-8",$metatag_description)))
            ->setCellValue("AK$i", (iconv("ISO-8859-1", "UTF-8",$metatag_keywords)))
            ->setCellValue("AL$i", (iconv("ISO-8859-1", "UTF-8",$row['cat_homepage'])))
             ->setCellValue("AM$i", (iconv("ISO-8859-1", "UTF-8",$row['desc_homepage'])))
            ->setCellValue("AN$i", '')
            ->setCellValue("AO$i", '')
            ->setCellValue("AP$i", '')
            ->setCellValue("AQ$i", '')
            ->setCellValue("AR$i", '')
            ->setCellValue("AS$i", $row['msg_disponibile'])
            ->setCellValue("AT$i", $row['msg_non_disponibile'])
            ->setCellValue("AU$i", '')
            ->setCellValue("AV$i", '')
            ->setCellValue("AW$i", $row['data_disponibilita'])
            ->setCellValue("AX$i", '')
            ->setCellValue("AY$i", '')
			->setCellValue("AZ$i", '')
			->setCellValue("BA$i", '')
			->setCellValue("BB$i", '')
			->setCellValue("BC$i", '')
			->setCellValue("BD$i", '')
			->setCellValue("BE$i", '')
			->setCellValue("BF$i", '')
			->setCellValue("BG$i", '')
			->setCellValue("BH$i", '')
			->setCellValue("BI$i", '')
			->setCellValue("BJ$i", $row['nuovo_eco'])
			->setCellValue("BK$i", '')
			->setCellValue("BL$i", '')
			->setCellValue("BM$i", '')
			->setCellValue("BN$i", '')
			->setCellValue("BO$i", $row['stato'])
			->setCellValue("BP$i", $row['prova_gratuita'])
			->setCellValue("BQ$i", $row['trasporto_gratuito'])
			->setCellValue("BR$i", '')
			->setCellValue("BS$i", '')
			->setCellValue("BT$i", '')
			->setCellValue("BU$i", '')
			->setCellValue("BV$i", '')
			->setCellValue("BW$i", '')
			->setCellValue("BX$i", $row['canonical'])
			->setCellValue("BY$i", '')
			->setCellValue("BZ$i", '')
			->setCellValue("CA$i", '')
			->setCellValue("CB$i", '')
			->setCellValue("CC$i", '')
			->setCellValue("CD$i", '')
			->setCellValue("CE$i", '')
			->setCellValue("CF$i", '')
			->setCellValue("CG$i", $row['redirect'])
			->setCellValue("CH$i", str_replace(":::::",";",$row['punti_di_forza']))
			->setCellValue("CI$i", '')
			->setCellValue("CJ$i", '')			->setCellValue("CK$i", '')
			->setCellValue("CL$i", '')
			->setCellValue("CM$i", '')
		
			;

// CATEGORIE

$query1 = "SELECT category_lang.id_category, category_lang.name FROM category_lang JOIN product ON category_lang.id_category = product.id_category_default WHERE category_lang.id_lang = 5 AND product.id_product = $row[id_prestashop]";
$result1 = mysql_query($query1, $link_global);
while ($row1 = mysql_fetch_array($result1, MYSQL_ASSOC)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("AC$i", $row1['name']);
} 
// NOINDEX

if($row['noindex'] == 1) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("AN$i", (iconv("ISO-8859-1", "UTF-8", 'si')));
} else { }

//COMPARAPREZZI

if($row['trovaprezzi'] == 1) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("AO$i", (iconv("ISO-8859-1", "UTF-8", 's�')));
} else { }

if($row['eprice'] == 1) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("AP$i", (iconv("ISO-8859-1", "UTF-8", 's�')));
} else { }

$amazon_check = explode(";",$row['amazon']);

if(in_array("5",$amazon_check))
{
	$objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("AX$i", (iconv("ISO-8859-1", "UTF-8", 'si')));	
}

if(in_array("2",$amazon_check))
{
	$objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("AY$i", (iconv("ISO-8859-1", "UTF-8", 'si')));	
}

if(in_array("3",$amazon_check))
{
	$objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("AZ$i", (iconv("ISO-8859-1", "UTF-8", 'si')));	
}

if(in_array("4",$amazon_check))
{
	$objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("BA$i", (iconv("ISO-8859-1", "UTF-8", 'si')));	
}

if(in_array("1",$amazon_check))
{
	$objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("BB$i", (iconv("ISO-8859-1", "UTF-8", 'si')));	
}
if(in_array("6",$amazon_check))
{
	$objPHPExcel->setActiveSheetIndex(0)
           ->setCellValue("BC$i", (iconv("ISO-8859-1", "UTF-8", 'si')));	
}
if($row['google_shopping'] == 1) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("AR$i", (iconv("ISO-8859-1", "UTF-8", 's�')));
} else { }


// SCONTI QUANTITA

$query1 = "SELECT * FROM specific_price WHERE id_product = $row[id_prestashop] AND (specific_price_name = 'sc_qta_1' OR specific_price_name = 'sc_qta_2' OR specific_price_name = 'sc_qta_3') AND id_group = 1 ORDER BY specific_price_name ASC";
$result1 = mysql_query($query1, $link_global);
$cella[0] = "V";
$cella[1] = "W";
$cella[2] = "X";
$k = 0;
while ($row1 = mysql_fetch_array($result1, MYSQL_ASSOC)) {
$objPHPExcel->setActiveSheetIndex(0)
             ->setCellValue($cella[$k]."$i", $row1['from_quantity']."_".$row1['reduction']*100);
			$k++;
}

// SCONTI RIVENDITORI

$query1 = "SELECT * FROM specific_price WHERE id_product = $row[id_prestashop] AND (specific_price_name = 'sc_riv_1' OR specific_price_name = 'sc_riv_2' OR specific_price_name = 'sc_riv_3') ORDER BY specific_price_name ASC";
$result1 = mysql_query($query1, $link_global);
$cella[0] = "Y";

$k = 0;
while ($row1 = mysql_fetch_array($result1, MYSQL_ASSOC)) {
$objPHPExcel->setActiveSheetIndex(0)
             ->setCellValue($cella[$k]."$i", $row1['reduction']*100);
			$k++;
}

			
			
					// AGGIORNATO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("F$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8", "s�")).',no, "');
	$objPHPExcel->getActiveSheet()->getCell("F$i")->setDataValidation($objValidation);
	

	
		// Garanzia
$objValidation = $objPHPExcel->getActiveSheet()->getCell("AV$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"12 mesi, 24 mesi, 36 mesi"');
	$objPHPExcel->getActiveSheet()->getCell("CI$i")->setDataValidation($objValidation);

	
		// NUOVO ECO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("BJ$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"new,used,refurbished"');
	$objPHPExcel->getActiveSheet()->getCell("BJ$i")->setDataValidation($objValidation);
	
	// HOME PAGE
$objValidation = $objPHPExcel->getActiveSheet()->getCell("BE$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8", "s�")).',no"');
	$objPHPExcel->getActiveSheet()->getCell("BE$i")->setDataValidation($objValidation);
	
	
		// PROVA GRATUITA
$objValidation = $objPHPExcel->getActiveSheet()->getCell("BP$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8", "s�")).',no"');
	$objPHPExcel->getActiveSheet()->getCell("BP$i")->setDataValidation($objValidation);
	
	
		// TRASP GRATUITO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("BQ$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8", "s�")).',no"');
	$objPHPExcel->getActiveSheet()->getCell("BQ$i")->setDataValidation($objValidation);
	
	
		// MOSTRA TRASP GRATUITO
$objValidation = $objPHPExcel->getActiveSheet()->getCell("CA$i")->getDataValidation();
    $objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
    $objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
    $objValidation->setAllowBlank(true);
    $objValidation->setShowInputMessage(false);
    $objValidation->setShowErrorMessage(false);
    $objValidation->setShowDropDown(true);
    $objValidation->setErrorTitle('Errore!');
    $objValidation->setError('Valore non in lista.');
    $objValidation->setPromptTitle('Seleziona dalla lista');
    $objValidation->setPrompt('Seleziona un valore dalla lista');
    $objValidation->setFormula1('"'.(iconv("ISO-8859-1", "UTF-8", "s�")).',no"');
	$objPHPExcel->getActiveSheet()->getCell("CA$i")->setDataValidation($objValidation);
	

	// AGGIORNATO
	$query1 = "SELECT aggiornato FROM product_other WHERE id_product = '$row[id_prestashop]'";
	$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("F$i", (iconv("ISO-8859-1", "UTF-8", $row1['aggiornato'])));
}
	
	
	// NUOVO ECO
	$query1 = "SELECT condition FROM product WHERE id_product = '$row[id_prestashop]'";
	$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("BJ$i", (iconv("ISO-8859-1", "UTF-8", $row1['nuovo_eco'])));
}

	// PROVA GRATUITA
	$query1 = "SELECT prova_gratuita FROM product_other WHERE id_product = '$row[id_prestashop]'";
	$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("BP$i", (iconv("ISO-8859-1", "UTF-8", $row1['prova_gratuita'])));
}
	
	

$rowtrasportogratuito = Db::getInstance()->getValue("SELECT value FROM configuration WHERE id_configuration = 240");

	$artrasportogratuito = unserialize($rowtrasportogratuito);
	if(in_array($row['id_prestashop'], $artrasportogratuito))
		$trasp_gratis = 's�';
	else
		$trasp_gratis = '';
		
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue("BQ$i", (iconv("ISO-8859-1", "UTF-8", $trasp_gratis)));
	
	
		// MOSTRA TRASP GRATUITO
	$query1 = "SELECT mostra_spese_spedizione FROM product_other WHERE id_product = '$row[id_prestashop]'";
	$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CA$i", (iconv("ISO-8859-1", "UTF-8", $row1['mostra_spese_spedizione'])));
}
	
	// Confezione
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '450' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CJ$i", $row1['valore']);
}

// HOME PAGE
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '452' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("BE$i", $row1['valore']);
}
	
	
// GARANZIA
$query1 = "SELECT feature_value_lang.value AS valore FROM feature_product JOIN feature_value ON feature_product.id_feature_value = feature_value.id_feature_value JOIN feature_value_lang ON feature_product.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_product.id_feature = '449' AND feature_product.id_product =  '$row[id_prestashop]'";
$result1 = mysql_query($query1, $link_global);
while($row1 = mysql_fetch_assoc($result1)) {
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("CI$i", $row1['valore']);
}



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	




$i++; // INCREMENTO ----------------------------------------------------------------------------------------------------------------------------------------



}





			
$highestRow = $objPHPExcel->getActiveSHeet()->getHighestRow();
$objPHPExcel->getActiveSheet()->getStyle('A1:CN1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);			
$objPHPExcel->getActiveSheet()->getStyle('A1:CN1')->getFill()->getStartColor()->setRGB('FFFF00');		
$objPHPExcel->getActiveSheet()->getStyle('O1:Q1')->getFill()->getStartColor()->setRGB('FFFFFF'); //BIANCO
$objPHPExcel->getActiveSheet()->getStyle('R1:Y1')->getFill()->getStartColor()->setRGB('99FFFF'); //azzurro
$objPHPExcel->getActiveSheet()->getStyle("A1:CN$i")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$objPHPExcel->getActiveSheet()->getStyle('A1:CN1')->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth('20');
$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth('70');
$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth('70');
$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth('70');
$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth('70');
$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth('70');
$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth('70');
$objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setWidth('80');
$objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('AW')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('BA')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BE')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BF')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BG')->setWidth('50');
$objPHPExcel->getActiveSheet()->getColumnDimension('BI')->setWidth('80');
$objPHPExcel->getActiveSheet()->getColumnDimension('BJ')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BO')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BP')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BQ')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BR')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BS')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BT')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BU')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BV')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BW')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BX')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('BY')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('BZ')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('CA')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CB')->setWidth('10');
$objPHPExcel->getActiveSheet()->getColumnDimension('CD')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CE')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CF')->setWidth('30');
$objPHPExcel->getActiveSheet()->getColumnDimension('CG')->setWidth('50');
$objPHPExcel->getActiveSheet()->getColumnDimension('CH')->setWidth('50');
$objPHPExcel->getActiveSheet()->getColumnDimension('CI')->setWidth('50');
$objPHPExcel->getActiveSheet()->getColumnDimension('CJ')->setWidth('50');
$objPHPExcel->getActiveSheet()->getColumnDimension('CN')->setWidth('30');
$objPHPExcel->getActiveSheet()->getStyle("J2:J$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("K2:K$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("L2:L$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("M2:M$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("N2:N$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("O2:O$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("P2:P$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("Q2:Q$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("R2:R$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("S2:S$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("T2:T$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("U2:U$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("V2:V$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("X2:X$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("Y2:Y$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("W2:W$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("AA2:AA$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("AB2:AB$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

$objPHPExcel->getActiveSheet()->getStyle("J2:J$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:CN$i")->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
/* $objPHPExcel->getActiveSheet()->getStyle("P2:P$i")->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED); */


$objPHPExcel->getActiveSheet()->setTitle('Listino interno');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

$data = date("Ymd");

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->setPreCalculateFormulas(false);
$objWriter->save(str_replace(".php", ".xls", "esportazione-catalogo/catalogo_xls/listinointerno-$data.php"));

echo "<br /><br />Il file &egrave; pronto per essere scaricato: <a onclick='window.onbeforeunload = null' href='http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/listinointerno-$data.xls'>clicca qui per eseguire il download</a>!";

?>




