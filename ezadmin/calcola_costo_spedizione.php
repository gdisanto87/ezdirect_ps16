<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/


define('_PS_ADMIN_DIR_', getcwd());
define('PS_ADMIN_DIR', _PS_ADMIN_DIR_); // Retro-compatibility
include(PS_ADMIN_DIR.'/../config/config.inc.php');

		$cart_ctrl = new Cart($_POST['id_cart']);
		$carrier_cart = $_POST['carrier_cart'];
		
		$costo_trasporto_modificato = $_POST['trasporto_modificato'];
		if($carrier_cart != 0) {
			if($costo_trasporto_modificato != "" && $costo_trasporto_modificato > 0) {
				$costo_spedizione = $costo_trasporto_modificato;
				
			}
			else {				
				Db::getInstance()->executeS("UPDATE "._DB_PREFIX_."cart SET transport = '' WHERE id_cart = ".$_POST['id_cart']."");
				$id_customer = Db::getInstance()->getValue("SELECT id_customer FROM "._DB_PREFIX_."cart WHERE id_cart = ".$_POST['id_cart']."");
				$id_customer_group = Db::getInstance()->getValue("SELECT id_default_group FROM "._DB_PREFIX_."customer WHERE id_customer = ".$id_customer."");
				
				if($_POST['totale'] == 0)
					$_POST['totale'] = 1;
				
				if($_POST['sconti_extra'] == 1)
					$id_customer_group = 999999;
				
				$costo_spedizione = $cart_ctrl->getOrderShippingCostByTotalAndDelivery($carrier_cart, $_POST['totale'], $_POST['address_delivery'], $id_customer_group, false);
			}
			//controllo per verificare prodotti virtuali
			
			$current_products = str_replace("impImporto[", "", $_POST['products']);
			$current_products = str_replace("]",";",$current_products);
			$current_products = rtrim($current_products, ";");
			$products = Db::getInstance()->executeS("SELECT id_product FROM "._DB_PREFIX_."cart_product WHERE id_cart = ".$_POST['id_cart']." AND bundle != 88888877");
			
			$products = explode(";",$current_products);
			$no_vrt = 1;
			foreach ($products as $product) 
			{
				if (ProductDownload::getIdFromIdProduct($product) == 0)
				{
					$no_vrt = 1;
					break;
				}
				else 
				{
					$no_vrt = 0;
					
				}
			}
			
			if($no_vrt == 0) 
			{
				$id_carrier = Db::getInstance()->getValue("SELECT id_carrier FROM "._DB_PREFIX_."carrier WHERE active = 0 AND deleted = 0 AND name LIKE '%grat%'");
				Db::getInstance()->executeS("UPDATE "._DB_PREFIX_."cart SET transport = '".$id_carrier.":0' WHERE id_cart = ".$_POST['id_cart']."");
				
				$costo_spedizione = 0;
			}
		}
		else {
			$costo_spedizione = 0;
		}
		echo $carrier_cart."|".$costo_spedizione;
?>