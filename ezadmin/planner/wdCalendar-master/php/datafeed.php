<?php
include_once("dbconfig.php");
include_once("functions.php");
include_once(dirname(__FILE__) . '/../../config/config.inc.php'); // include('../../../config/config.inc.php');
include_once(dirname(__FILE__) . '/../../init.php'); // include('../../init.php'); 

ini_set("memory_limit","892M");
ini_set("display_errors", "off");
set_time_limit(3600);

function truncateWord($str, $maxLength) {
    if (strlen($str) > $maxLength) {
        $str = substr($str, 0, $maxLength);
        $str = preg_replace('/\s+.*?$/', '', $str);
        $str = trim($str);
    }

    return $str;
}

function addCalendar($st, $et, $sub, $ade){
  $ret = array();
  try{
    $db = new DBConnection();
    $db->getConnection();
    $sql = "insert into `jqcalendar` (`subject`, `starttime`, `endtime`, `isalldayevent`) values ('"
      .mysqli_real_escape_string($sub)."', '"
      .php2MySqlTime(js2PhpTime($st))."', '"
      .php2MySqlTime(js2PhpTime($et))."', '"
      .mysqli_real_escape_string($ade)."' )";
    //echo($sql);
		if(mysql_query($sql)==false){
      $ret['IsSuccess'] = false;
      $ret['Msg'] = mysql_error();
    }else{
      $ret['IsSuccess'] = true;
      $ret['Msg'] = 'add success';
      $ret['Data'] = mysql_insert_id();
    }
	}catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  return $ret;
}


function addDetailedCalendar($st, $et, $sub, $ade, $dscr, $loc, $color, $tz){
  $ret = array();
  try{
    $db = new DBConnection();
    $db->getConnection();
    $sql = "insert into `jqcalendar` (`subject`, `starttime`, `endtime`, `isalldayevent`, `description`, `location`, `color`) values ('"
      .mysqli_real_escape_string($sub)."', '"
      .php2MySqlTime(js2PhpTime($st))."', '"
      .php2MySqlTime(js2PhpTime($et))."', '"
      .mysqli_real_escape_string($ade)."', '"
      .mysqli_real_escape_string($dscr)."', '"
      .mysqli_real_escape_string($loc)."', '"
      .mysqli_real_escape_string($color)."' )";
    //echo($sql);
		if(mysql_query($sql)==false){
      $ret['IsSuccess'] = false;
      $ret['Msg'] = 'error'; //$ret['Msg'] = mysql_error();
    }else{
      $ret['IsSuccess'] = true;
      $ret['Msg'] = 'add success';
      $ret['Data'] = mysql_insert_id();
    }
	}catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  return $ret;
}

function chooseColor($tipo_richiesta) {
	$color = 1;
	switch($tipo_richiesta) 
	{
		case 'AE*Attivita': case 'AE*Telefonata': case 'AE*Visita': case 'AE*Caso' : case 'AE*Intervento' : $color = 17; break;
		case 'Attivita': $color = 2; break;
		case 'Telefonata': $color = 3; break;
		case 'Visita': $color = 4; break;
		case 'Caso': $color = 5; break;
		case 'Intervento': $color = 6; break;
		case 'Richiesta_RMA': $color = 7; break;
		case 'preventivo': $color = 8; break;
		case 'tirichiamiamonoi': $color = 9; break;
		case 'TODO Amazon': $color = 17; break;
		case 2: $color = 10; break; // contabilità
		case 3: $color = 11; break; // rivenditori
		case 4: $color = 12; break; // assistenza tecnica
		case 7: $color = 13; break; // messaggi
		case 8: $color = 14; break; // ordini
		case 9: $color = 15; break; // rma
		case 'FERIE': $color = 16; break;
		default: $color = 1; break;
	}
	return $color;
}

function chooseTipo($tipo_richiesta) {
	$tipo = 1;
	switch($tipo_richiesta) 
	{
		case 'AE*Attivita': $tipo = 'Attivit&agrave; STAFF'; break;
		case 'AE*Telefonata': $tipo = 'Telefonata STAFF'; break;
		case 'AE*Visita': $tipo = 'Visita STAFF'; break;
		case 'AE*Caso': $tipo = 'Caso STAFF'; break;
		case 'AE*Intervento': $tipo = 'Intervento STAFF'; break;
		
		case 'Attivita': $tipo = 'Attivit&agrave;'; break;
		case 'Telefonata': $tipo = 'Telefonata'; break;
		case 'Visita': $tipo = 'Visita'; break;
		case 'Caso': $tipo = 'Caso'; break;
		case 'Intervento': $tipo = 'Intervento'; break;
		case 'Richiesta_RMA': $tipo = 'Richiesta RMA'; break;
		case 'preventivo': $tipo = 'Preventivo'; break;
		case 'tirichiamiamonoi': $tipo = 'Ti richiamiamo noi'; break;
		case 2: $tipo = 'Contabilit&agrave;'; break; // contabilità
		case 3: $tipo = 'Rivenditori'; break; // rivenditori
		case 4: $tipo = 'Assistenza tecnica'; break; // assistenza tecnica
		case 7: $tipo = 'Messaggi'; break; // messaggi
		case 8: $tipo = 'Ordini'; break; // ordini
		case 9: $tipo = 'RMA'; break; // rma
		case 'FERIE': $tipo = 'Ferie'; break;
		default: $tipo = '--'; break;
	}
	return $tipo;
}

function buildLink($tipo_richiesta, $id_customer, $id_cosa) {
	$context = Context::getContext();
	$tipo = 1;

  // Correggere i token e gli href
	$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($context->employee->id));
	$tokenCustomerThreads = Tools::getAdminToken('AdminCustomerThreads'.(int)(Tab::getIdFromClassName('AdminCustomerThreads')).(int)($context->employee->id));
	
	$hrefticket = 'index.php?tab=AdminCustomers&id_customer='.$id_customer.'&viewticket&id_customer_thread='.$id_cosa.'&viewcustomer&token='.$tokenCustomers.'&tab-container-1=6';
	
	$hrefmsg = 'index.php?tab=AdminCustomers&id_customer='.$id_customer.'&viewmessage&id_mex='.$id_cosa.'&viewcustomer&token='.$tokenCustomers.'&tab-container-1=7';
	
	$hrefcommerciale = 'index.php?tab=AdminCustomers&id_customer='.$id_customer.'&viewpreventivo&id_thread='.$id_cosa.'&viewcustomer&token='.$tokenCustomers.'&tab-container-1=7';
	
	$hrefazione = 'index.php?tab=AdminCustomers&id_customer='.$id_customer.'&viewaction&id_action='.$id_cosa.'&viewcustomer&token='.$tokenCustomers.'&tab-container-1=10';
	
	$hrefazionestaff = 'index.php?tab=AdminCustomerThreads&updateemployee&id_employee='.$id_customer.'&viewaction&id_action='.$id_cosa.'&token='.$tokenCustomerThreads.'&tab-container-1=3';
	
	switch($tipo_richiesta) 
	{
		case 'AE*Attivita': case 'AE*Telefonata':  case 'AE*Visita':  case 'AE*Intervento':  case 'AE*Caso': $link = $hrefazionestaff; break;
		case 'Attivita': $link = $hrefazione; break;
		case 'Telefonata': $link = $hrefazione; break;
		case 'Visita': $link = $hrefazione; break;
		case 'Caso': $link = $hrefazione; break;
		case 'Intervento': $link = $hrefazione; break;
		case 'Richiesta_RMA': $link = $hrefazione; break;
		case 'preventivo': $link = $hrefcommerciale; break;
		case 'tirichiamiamonoi': $link = $hrefcommerciale; break;
		case 2: $link = $hrefticket; break; // contabilità
		case 3: $link = $hrefticket; break; // rivenditori
		case 4: $link = $hrefticket; break; // assistenza tecnica
		case 7: $link = $hrefmsg; break; // messaggi
		case 8: $link = $hrefticket; break; // ordini
		case 9: $link = $hrefticket; break; // rma
		case 'FERIE': $link = '#'; break;
		default: $link = '#'; break;
	}
	return $link;
}

function chooseImage($tipo_richiesta) {
	$image = '';
	switch($tipo_richiesta) 
	{
		case 'Attivita': case 'AE*Attivita' : $image = '<img style="display:block; float:left" src="http://www.ezdirect.it/img/admin/icons/attivita.gif" alt="Attivita" title="Attivita" />'; break;
		case 'Telefonata': case 'AE*Telefonata': $image = '<img style="display:block; float:left" src="http://www.ezdirect.it/img/admin/icons/telefonata.gif" alt="Telefonata" title="Telefonata" />'; break;
		case 'Visita': case 'AE*Visita': $image = '<img style="display:block; float:left" src="http://www.ezdirect.it/img/admin/icons/attivita.gif" alt="Visita" title="Visita" />'; break;
		case 'Caso': case 'AE*Caso': $image = '<img style="display:block; float:left" src="http://www.ezdirect.it/img/admin/icons/caso.gif" alt="Caso" title="Caso" />'; break;
		case 'Intervento': case 'AE*Intervento':  $image = '<img style="display:block; float:left" src="http://www.ezdirect.it/img/admin/icons/intervento.gif" alt="Intervento" title="Intervento" />'; break;
		case 'Richiesta_RMA': $image = '<img style="display:block; float:left" src="http://www.ezdirect.it/img/admin/icons/richiesta_rma.gif" alt="Richiesta RMA" title="Richiesta RMA" />'; break;
		case 'preventivo': $image = '<img style="display:block; float:left" src="http://www.ezdirect.it/img/admin/icons/commerciale.gif" alt="Preventivo" title="Preventivo" />'; break;
		case 'tirichiamiamonoi': $image = '<img style="display:block; float:left" src="http://www.ezdirect.it/img/admin/icons/tirichiamiamonoi.gif" alt="Ti richiamiamo noi" title="Ti richiamiamo noi" />'; break;
		case 2: $image = '<img style="display:block; float:left" src="http://www.ezdirect.it/img/admin/icons/contabilita.gif" alt="Contabilita" title="Contabilita" />'; break; // contabilità
		case 3: $image = '<img style="display:block; float:left" src="http://www.ezdirect.it/img/admin/icons/rivenditori.gif" alt="Rivenditori" title="Rivenditori" />'; break; // rivenditori
		case 4: $image = '<img style="display:block; float:left" src="http://www.ezdirect.it/img/admin/icons/assistenza.gif" alt="Assistenza tecnica" title="Assistenza tecnica" />'; break; // assistenza tecnica
		case 7: $image = '<img style="display:block; float:left" src="http://www.ezdirect.it/img/admin/icons/messaggio.gif" alt="Messaggi" title="Messaggi" />'; break; // messaggi
		case 8: $image = '<img style="display:block; float:left" src="http://www.ezdirect.it/img/admin/icons/ordini.gif" alt="Ordini" title="Ordini" />'; break; // ordini
		case 9: $image = '<img style="display:block; float:left" src="http://www.ezdirect.it/img/admin/icons/rma.gif" alt="RMA" title="RMA" />'; break; // rma
		case 'FERIE': $image = '<img style="display:block; float:left" src="http://www.ezdirect.it/img/admin/icons/ferie.gif" alt="Ferie" title="Ferie" />'; break; // ferie
		default: $image = 1; break;
	}
	return $image;
}

function customerMessagePreview($id, $tipo)
{
  if($tipo == 'Attivita' || $tipo == 'Telefonata' || $tipo == 'Visita' || $tipo == 'Caso' || $tipo == 'Intervento' || $tipo == 'Richiesta_RMA')
  {
    $messaggio = Db::getInstance()->getValue("SELECT action_message FROM action_message WHERE action_message != '' AND id_action = ".$id." ORDER BY id_action_message DESC");
    $messaggio = strip_tags(truncateWord(html_entity_decode($messaggio),60)."...");
  }
  else if($tipo == 'AE*Attivita' || $tipo == 'AE*Telefonata' || $tipo == 'AE*Visita' || $tipo == 'AE*Caso' || $tipo == 'AE*Intervento')
  {
    $messaggio = Db::getInstance()->getValue("SELECT action_message_employee FROM action_message_employee WHERE action_message_employee != '' AND id_action_employee = ".$id." ORDER BY id_action_message DESC");
    $messaggio = strip_tags(truncateWord(html_entity_decode($messaggio),60)."...");
  }
  else if($tipo == 'preventivo' || $tipo == 'tirichiamiamonoi')
  {
    $messaggio = Db::getInstance()->getValue("SELECT message FROM form_prevendita_message WHERE message != '' AND id_thread = ".$id." ORDER BY id_message DESC");
    $messaggio = strip_tags(truncateWord(html_entity_decode($messaggio),60)."...");
  }
  else if($tipo == 2 || $tipo == 3 || $tipo == 4 || $tipo == 7 || $tipo == 8 || $tipo == 9)
  {
    $messaggio = Db::getInstance()->getValue("SELECT message FROM "._DB_PREFIX_."customer_message WHERE message != '' AND id_customer_thread = ".$id." ORDER BY id_customer_message DESC");
    $messaggio = strip_tags(truncateWord(html_entity_decode($messaggio),61)."...");
  }
  else
  {
    $messaggio = 'Ferie';
  }

  return $messaggio;
}

function listCalendarByRange($sd, $ed){
  $ret = array();
  $ret['events'] = array();
  $ret["issort"] =true;
  $ret["start"] = php2JsTime($sd);
  $ret["end"] = php2JsTime($ed);
  $ret['error'] = null;
  try{
    /*$db = new DBConnection();
    $db->getConnection();*/

    //gestione ferie
		$sql_array = array();
		$sd_array = explode("-", php2MySqlTime($sd));
		$ed_array = explode("-", php2MySqlTime($ed));
		
		$sd_day_ar = explode(" ", ($sd_array[2]));
		$ed_day_ar = explode(" ", ($ed_array[2]));
		
		$sd_day = $sd_day_ar[0];
		$ed_day = $ed_day_ar[0];
		$sd_month = $sd_array[1];
		$ed_month = $ed_array[1];
		$sd_year = $sd_array[0];
		$ed_year = $ed_array[0];
		
    // Correggere $cookie2!
		$id_employee = $cookie2->id_employee;
		
		if($sd_month == $ed_month)
		{
			$array = Db::getInstance()->getValue('SELECT valori_ore FROM ore WHERE id_employee = '.$id_employee.' AND year = '.$sd_year.' AND month = '.$sd_month.'');
			
			$array = explode('*', $array);
			$i = 1;
			foreach($array as $ar)
			{
				if($i >= $sd_day && $i <= $ed_day)
				{
					if($ar == 'F')
					{
						$date_array = array();
						$date_array['id'] = '0';
						$date_array['tipo'] = 'FERIE';
						$date_array['cliente'] = '0';
						$date_array['status'] = '';
						$date_array['in_carico'] = '';
						$date_array['priority'] = '';
						$date_array['date_add'] = $sd_year.'-'.sprintf("%02d", $sd_month).'-'.sprintf("%02d", $i).' 09:00:00';
						$date_array['date_upd'] = $sd_year.'-'.sprintf("%02d", $sd_month).'-'.sprintf("%02d", $i).' 17:00:00';
						$sql_array[] = $date_array;
					}
				}
				$i++;
			}
		}
		else
		{
			$array1 = Db::getInstance()->getValue('SELECT valori_ore FROM ore WHERE id_employee = '.$id_employee.' AND year = '.$sd_year.' AND month = '.$sd_month.'');
			$array2 = Db::getInstance()->getValue('SELECT valori_ore FROM ore WHERE id_employee = '.$id_employee.' AND year = '.$ed_year.' AND month = '.$ed_month.'');
			$array1 = explode('*', $array1);
			$array2 = explode('*', $array2);
			
			$i = 1;
			foreach($array1 as $ar)
			{
				if($i >= $sd_day)
				{
					if($ar == 'F')
					{
						$date_array = array();
						$date_array['id'] = '0';
						$date_array['tipo'] = 'FERIE';
						$date_array['cliente'] = '0';
						$date_array['status'] = '';
						$date_array['in_carico'] = '';
						$date_array['priority'] = '';
						$date_array['date_add'] = $sd_year.'-'.sprintf("%02d", $sd_month).'-'.sprintf("%02d", $i).' 09:00:00';
						$date_array['date_upd'] = $sd_year.'-'.sprintf("%02d", $sd_month).'-'.sprintf("%02d", $i).' 17:00:00';
						$sql_array[] = $date_array;
					}
				}
				$i++;
			}
			$i = 1;
			foreach($array2 as $ar)
			{
				if($i <= $ed_day)
				{
					if($ar == 'F')
					{
						$date_array = array();
						$date_array['id'] = '0';
						$date_array['tipo'] = 'FERIE';
						$date_array['cliente'] = '0';
						$date_array['status'] = '--';
						$date_array['in_carico'] = $id_employee;
						$date_array['priority'] = '';
						$date_array['date_add'] = $ed_year.'-'.sprintf("%02d", $ed_month).'-'.sprintf("%02d", $i).' 09:00:00';
						$date_array['date_upd'] = $ed_year.'-'.sprintf("%02d", $ed_month).'-'.sprintf("%02d", $i).' 17:00:00';
						$sql_array[] = $date_array;
					}
				}
				$i++;
			}
		}
		foreach($sql_array as $sqlf)
		{
			$ret['events'][] = array(
			0, //0
			chooseImage($sqlf['tipo']).' '.'<span style="font-size:10px; display:block; float:left; '.($sqlf['priority'] == 'low' ? 'border:3px solid green' : ($sqlf['priority'] == 'medium' ? 'border:3px solid yellow' : ($sqlf['priority'] == 'high' ? 'border:3px solid red' : ''))).'">'.chooseTipo($sqlf['tipo']).' <br /><font style="color:black">'.strip_tags(substr("Ferie",0,15)."").'</font></span>', //1
			php2JsTime(mySql2PhpTime($sqlf['date_add'])), //2
			php2JsTime(mySql2PhpTime($sqlf['date_upd'])), //3
			0, //4
			0, //more than one day event 5
			0, //Recurring event, 6
			chooseColor($sqlf['tipo']), // 7
			1, //editable 8
			buildLink($sqlf['tipo'], 0, 0), //9
			"<strong>Ferie</strong>", //10
			$sqlf['date_add'], //11
			'--', //12
			'Ferie', //13
			Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$sqlf['in_carico']), //14
			Tools::displayDate($sqlf['date_add'], $cookie2->id_lang, false), //15
			Tools::displayDate($sqlf['date_upd'], $cookie2->id_lang, false), //16
			chooseTipo($sqlf['tipo']), // 17
			'--', //18
			customerMessagePreview(0, $sqlf['tipo']), //19
		  );
		}
		// Fine gestione ferie
	
		$sql = ("SELECT fp.id_thread as id, tipo_richiesta as tipo, id_customer as cliente, id_employee as in_carico, '' as priority, status, date_add, date_upd FROM form_prevendita_thread fp LEFT JOIN (SELECT id_thread, id_message FROM form_prevendita_message) fm1 ON fp.id_thread = fm1.id_thread LEFT JOIN (SELECT * FROM ticket_cc WHERE type = 'p***') tc ON fm1.id_message = tc.msg WHERE (id_employee = ".$cookie2->id_employee.") AND (status = 'open' OR status = 'pending1') AND (date_add BETWEEN '".php2MySqlTime($sd)."' and '".php2MySqlTime($ed)."')
		
		  UNION

        SELECT ct.id_customer_thread as id, id_contact as tipo, id_customer as cliente, id_employee as in_carico, priorita as priority, status, date_add, date_upd FROM "._DB_PREFIX_."customer_thread ct LEFT JOIN (SELECT id_customer_thread, id_customer_message FROM "._DB_PREFIX_."customer_message) cm1 ON ct.id_customer_thread = cm1.id_customer_thread LEFT JOIN (SELECT * FROM ticket_cc WHERE type = 't***') tc ON cm1.id_customer_message = tc.msg WHERE id_contact != 7 AND (id_employee = ".$cookie2->id_employee.") AND (status = 'open' OR status = 'pending1') AND (date_add BETWEEN '".php2MySqlTime($sd)."' and '".php2MySqlTime($ed)."') UNION 	
        
        SELECT ct.id_customer_thread as id, id_contact as tipo, id_customer as cliente, id_employee as in_carico, priorita as priority, status, date_add, date_upd FROM "._DB_PREFIX_."customer_thread ct JOIN (SELECT id_customer_thread, id_customer_message FROM "._DB_PREFIX_."customer_message WHERE id_employee = 0) cm1 ON ct.id_customer_thread = cm1.id_customer_thread LEFT JOIN (SELECT * FROM ticket_cc WHERE type = 't***') tc ON cm1.id_customer_message = tc.msg WHERE id_contact = 7 AND (id_employee = ".$cookie2->id_employee.") AND (status = 'open' OR status = 'pending1') AND (date_add BETWEEN '".php2MySqlTime($sd)."' and '".php2MySqlTime($ed)."') UNION 					
        
        SELECT ac.id_action as id, action_type as tipo, id_customer as cliente, action_to as in_carico, '' as priority, status, action_date, date_upd FROM action_thread ac LEFT JOIN (SELECT id_action, id_action_message FROM action_message) am1 ON ac.id_action = am1.id_action LEFT JOIN (SELECT * FROM ticket_cc WHERE type = 'a***') tc ON am1.id_action_message = tc.msg WHERE (action_to =  ".$cookie2->id_employee.") AND (status = 'open' OR status = 'pending1') AND (action_date BETWEEN '".php2MySqlTime($sd)."' and '".php2MySqlTime($ed)."') UNION
        
        SELECT ac.id_action as id, CONCAT('AE*',action_type) as tipo, action_to as cliente, action_to as in_carico, '' as priority, status, action_date, date_upd FROM action_thread_employee ac LEFT JOIN (SELECT id_action, id_action_message_employee FROM action_message_employee) am1 ON ac.id_action = am1.id_action LEFT JOIN (SELECT * FROM ticket_cc WHERE type = 'a***') tc ON am1.id_action_message_employee = tc.msg WHERE (action_to =  ".$cookie2->id_employee.") AND (status = 'open' OR status = 'pending1') AND (action_date BETWEEN '".php2MySqlTime($sd)."' and '".php2MySqlTime($ed)."')
		");
			
			/*  UNION 
			SELECT id_cart as id, 'Carrello' as tipo, id_customer as cliente, in_carico_a as in_carico, '' AS status, date_add, date_upd FROM cart WHERE in_carico_a =  ".$cookie->id_employee." AND (date_add BETWEEN '".php2MySqlTime($sd)."' and '".php2MySqlTime($ed)."') ORDER BY date_add DESC
			
			con COPIA CONOSCENZA
			$sql = ("SELECT fp.id_thread as id, tipo_richiesta as tipo, id_customer as cliente, id_employee as in_carico, status, date_add, date_upd FROM form_prevendita_thread fp LEFT JOIN (SELECT id_thread, id_message FROM form_prevendita_message) fm1 ON fp.id_thread = fm1.id_thread LEFT JOIN (SELECT * FROM ticket_cc WHERE type = 'p***') tc ON fm1.id_message = tc.msg WHERE (id_employee = ".$cookie2->id_employee." OR tc.cc LIKE '".$cookie2->id_employee.";%' OR tc.cc LIKE '%;".$cookie2->id_employee.";%') AND (status = 'open' OR status = 'pending1') AND (date_add BETWEEN '".php2MySqlTime($sd)."' and '".php2MySqlTime($ed)."') 
		
		  UNION 
	
			SELECT ct.id_customer_thread as id, id_contact as tipo, id_customer as cliente, id_employee as in_carico, status, date_add, date_upd FROM "._DB_PREFIX_."customer_thread ct LEFT JOIN (SELECT id_customer_thread, id_customer_message FROM "._DB_PREFIX_."customer_message) cm1 ON ct.id_customer_thread = cm1.id_customer_thread LEFT JOIN (SELECT * FROM ticket_cc WHERE type = 't***') tc ON cm1.id_customer_message = tc.msg WHERE id_contact != 7 AND (id_employee = ".$cookie2->id_employee." OR tc.cc LIKE '".$cookie2->id_employee.";%' OR tc.cc LIKE '%;".$cookie2->id_employee.";%') AND (status = 'open' OR status = 'pending1') AND (date_add BETWEEN '".php2MySqlTime($sd)."' and '".php2MySqlTime($ed)."') UNION 	

			SELECT ct.id_customer_thread as id, id_contact as tipo, id_customer as cliente, id_employee as in_carico, status, date_add, date_upd FROM "._DB_PREFIX_."customer_thread ct JOIN (SELECT id_customer_thread, id_customer_message FROM "._DB_PREFIX_."customer_message WHERE id_employee = 0) cm1 ON ct.id_customer_thread = cm1.id_customer_thread LEFT JOIN (SELECT * FROM ticket_cc WHERE type = 't***') tc ON cm1.id_customer_message = tc.msg WHERE id_contact = 7 AND (id_employee = ".$cookie2->id_employee." OR tc.cc LIKE '".$cookie2->id_employee.";%' OR tc.cc LIKE '%;".$cookie2->id_employee.";%') AND (status = 'open' OR status = 'pending1') AND (date_add BETWEEN '".php2MySqlTime($sd)."' and '".php2MySqlTime($ed)."') UNION 					
				
			SELECT ac.id_action as id, action_type as tipo, id_customer as cliente, action_to as in_carico, status, action_date, date_upd FROM action_thread ac LEFT JOIN (SELECT id_action, id_action_message FROM action_message) am1 ON ac.id_action = am1.id_action LEFT JOIN (SELECT * FROM ticket_cc WHERE type = 'a***') tc ON am1.id_action_message = tc.msg WHERE (action_to =  ".$cookie2->id_employee." OR tc.cc LIKE '".$cookie2->id_employee.";%' OR tc.cc LIKE '%;".$cookie2->id_employee.";%') AND (status = 'open' OR status = 'pending1') AND (action_date BETWEEN '".php2MySqlTime($sd)."' and '".php2MySqlTime($ed)."')");
			*/
    
    /*$sql = "select * from `jqcalendar` where `starttime` between '"
      .php2MySqlTime($sd)."' and '". php2MySqlTime($ed)."' ORDER BY starttime ASC";
    $handle = mysql_query($sql);*/
    //echo $sql;
    $rows = Db::getInstance()->executeS($sql);
    foreach($rows as $row){ // while ($row = mysql_fetch_object($handle)) {

      $data_planner = Db::getInstance()->getValue('SELECT data FROM date_planner WHERE id = '.$row['id'].' AND tipo = "'.$row['tipo'].'"');
		
      if(!$data_planner)
        $data = $row['date_add'];
      else
        $data = $data_planner;
    
      $data_array = explode(" ",$data);
      $ore_array = explode(":",$data_array[1]);
      $ore_array[0] = (int)$ore_array[0];
      
      if($ore_array[0] <= 7 || $ore_array[0] > 20)
        $ore_array[0] = "07";
      
      $ore = $ore_array[0].":".$ore_array[1].":".$ore_array[2];
      $data = $data_array[0]." ".$ore;
      
      if(!in_array($data, $array_date) && !in_array(date("Y-m-d H:i:s", strtotime($data)+(60*1)), $array_date) && !in_array(date("Y-m-d H:i:s", strtotime($data)-(60*1)), $array_date) && !in_array(date("Y-m-d H:i:s", strtotime($data)+(60*2)), $array_date) && !in_array(date("Y-m-d H:i:s", strtotime($data)-(60*2)), $array_date) && !in_array(date("Y-m-d H:i:s", strtotime($data)+(60*3)), $array_date) && !in_array(date("Y-m-d H:i:s", strtotime($data)-(60*3)), $array_date) && !in_array(date("Y-m-d H:i:s", strtotime($data)+(60*4)), $array_date) && !in_array(date("Y-m-d H:i:s", strtotime($data)-(60*4)), $array_date) && !in_array(date("Y-m-d H:i:s", strtotime($data)+(60*5)), $array_date) && !in_array(date("Y-m-d H:i:s", strtotime($data)-(60*5)), $array_date)) {
        $array_date[] = $data;
      }
      else {
        $dataV = strtotime($data);
        $dataN = $dataV+(60*15);
        $data = date("Y-m-d H:i:s", $dataN);
        $array_date[] = $data;
      }
      
      $cliente = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'customer WHERE id_customer = '.$row['cliente']);

      if($cliente['is_company'] == 1)
        $nome_cliente = $cliente['company'];
      else
        $nome_cliente = $cliente['firstname']." ".$cliente['lastname'];
        
      if($cliente['id_default_group'] == 1)
        $profilo_cliente = 'Cliente web';
      else if($cliente['id_default_group'] == 3)
        $profilo_cliente = 'Rivenditore';
      else
        $profilo_cliente = 'Altro';
        
      //$ret['events'][] = $row;
      //$attends = $row->AttendeeNames;
      //if($row->OtherAttendee){
      //  $attends .= $row->OtherAttendee;
      //}
      //echo $row->StartTime;
      /*$ret['events'][] = array(
        $row->Id,
        $row->Subject,
        php2JsTime(mySql2PhpTime($row->StartTime)),
        php2JsTime(mySql2PhpTime($row->EndTime)),
        $row->IsAllDayEvent,
        ($row->IsAllDayEvent!=1 && date("Y-m-d",mySql2PhpTime($row->EndTime))>date("Y-m-d",mySql2PhpTime($row->StartTime))?1:0), //more than one day event
        //$row->InstanceType,
        0,//Recurring event,
        $row->Color,
        1,//editable
        $row->Location, 
        ''//$attends
      );*/
      $ret['events'][] = array(
        $row['id'], //0
        chooseImage($row['tipo']).' '.'<span style="font-size:10px; display:block; float:left; '.($row['priority'] == 'low' ? 'border:3px solid green' : ($row['priority'] == 'medium' ? 'border:3px solid yellow' : ($row['priority'] == 'high' ? 'border:3px solid red' : ''))).'">'.chooseTipo($row['tipo']).' ('.($row['status'] == 'open' ? 'A' : 'L').')
        '.($row['in_carico'] != $cookie2->id_employee ? ' <br /><strong><font style="color:red">COPIA CONOSC.</font></strong>' : '').'
        <br /><font style="color:black">'.strip_tags((substr($row['tipo'],0,3) == 'AE*' ? 'Da '.Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = (SELECT action_from FROM action_thread_employee WHERE id_action = '.$row['id'].')').'' : truncateWord($nome_cliente,20).'...')).'</font></span>', //1
        php2JsTime(mySql2PhpTime($data)), //2
        php2JsTime(mySql2PhpTime($data)), //3
        0, //4
        0, //more than one day event 5
        0,//Recurring event, 6
        chooseColor($row['tipo']), // 7
        1,//editable 8
        buildLink($row['tipo'], $row['cliente'], $row['id']), //9
        "<strong>".$nome_cliente."</strong> - ".$profilo_cliente, //10
        $data, //11
        ($row['status'] == 'open' ? 'A' : 'L'), //12
        (substr($row['tipo'],0,3) == 'AE*' ? 'Da '.Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = (SELECT action_from FROM action_thread_employee WHERE id_action = '.$row['id'].')').'' : $nome_cliente), //13
        Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$row['in_carico']), //14
        Tools::displayDate($row['date_add'], $cookie2->id_lang, false), //15
        Tools::displayDate($row['date_upd'], $cookie2->id_lang, false), //16
        chooseTipo($row['tipo']), // 17
        ($row['status'] == 'open' ? 'Aperto' : 'In lavorazione'), //18
        customerMessagePreview($row['id'], $row['tipo']), //19
      );
    }
	}catch(Exception $e){
     $ret['error'] = $e->getMessage();
  }
  return $ret;
}

function listCalendar($day, $type){
  $phpTime = js2PhpTime($day);
  //echo $phpTime . "+" . $type;
  switch($type){
    case "month":
      $st = mktime(0, 0, 0, date("m", $phpTime), 1, date("Y", $phpTime));
      $et = mktime(0, 0, -1, date("m", $phpTime)+1, 1, date("Y", $phpTime));
      break;
    case "week":
      //suppose first day of a week is monday 
      $monday  =  date("d", $phpTime) - date('N', $phpTime) + 1;
      //echo date('N', $phpTime);
      $st = mktime(0,0,0,date("m", $phpTime), $monday, date("Y", $phpTime));
      $et = mktime(0,0,-1,date("m", $phpTime), $monday+7, date("Y", $phpTime));
      break;
    case "day":
      $st = mktime(0, 0, 0, date("m", $phpTime), date("d", $phpTime), date("Y", $phpTime));
      $et = mktime(0, 0, -1, date("m", $phpTime), date("d", $phpTime)+1, date("Y", $phpTime));
      break;
  }
  //echo $st . "--" . $et;
  return listCalendarByRange($st, $et);
}

function updateCalendar($id, $tp, $st, $et){ // function updateCalendar($id, $st, $et){
  $ret = array();
  try{
    $db = new DBConnection();
    $db->getConnection();

    $exist = Db::getInstance()->getValue('SELECT id FROM date_planner WHERE id = '.$id);
    if(!$exist)
      $sql = "INSERT INTO date_planner (id, tipo, data) VALUES (".$id.", '".$tp."', '".php2MySqlTime(js2PhpTime($st))."')";
    else
      $sql = "UPDATE date_planner SET tipo = '".$tp."', data = '".php2MySqlTime(js2PhpTime($st))."' WHERE id = '".$id."'";
    
    /*$sql = "update `jqcalendar` set"
      . " `starttime`='" . php2MySqlTime(js2PhpTime($st)) . "', "
      . " `endtime`='" . php2MySqlTime(js2PhpTime($et)) . "' "
      . "where `id`=" . $id;*/
    //echo $sql;
		if(mysql_query($sql)==false){
      $ret['IsSuccess'] = false;
      $ret['Msg'] = 'error'; //$ret['Msg'] = mysql_error();
    }else{
      $ret['IsSuccess'] = true;
      $ret['Msg'] = 'Succefully';
    }
	}catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  return $ret;
}

function updateDetailedCalendar($id, $st, $et, $sub, $ade, $dscr, $loc, $color, $tz){
  $ret = array();
  try{
    $db = new DBConnection();
    $db->getConnection();
    $sql = "update `jqcalendar` set"
      . " `starttime`='" . php2MySqlTime(js2PhpTime($st)) . "', "
      . " `endtime`='" . php2MySqlTime(js2PhpTime($et)) . "', "
      . " `subject`='" . mysqli_real_escape_string($sub) . "', "
      . " `isalldayevent`='" . mysqli_real_escape_string($ade) . "', "
      . " `description`='" . mysqli_real_escape_string($dscr) . "', "
      . " `location`='" . mysqli_real_escape_string($loc) . "', "
      . " `color`='" . mysqli_real_escape_string($color) . "' "
      . "where `id`=" . $id;
    //echo $sql;
		if(mysql_query($sql)==false){
      $ret['IsSuccess'] = false;
      $ret['Msg'] = 'error'; //$ret['Msg'] = mysql_error();
    }else{
      $ret['IsSuccess'] = true;
      $ret['Msg'] = 'Succefully';
    }
	}catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  return $ret;
}

function removeCalendar($id){
  $ret = array();
  try{
    $db = new DBConnection();
    $db->getConnection();
    $sql = "delete from `jqcalendar` where `id`=" . $id;
		if(mysql_query($sql)==false){
      $ret['IsSuccess'] = false;
      $ret['Msg'] = 'error'; //$ret['Msg'] = mysql_error();
    }else{
      $ret['IsSuccess'] = true;
      $ret['Msg'] = 'Succefully';
    }
	}catch(Exception $e){
     $ret['IsSuccess'] = false;
     $ret['Msg'] = $e->getMessage();
  }
  return $ret;
}




header('Content-type:text/javascript;charset=UTF-8');
$method = $_GET["method"];
switch ($method) {
    case "add":
        $ret = addCalendar($_POST["CalendarStartTime"], $_POST["CalendarEndTime"], $_POST["CalendarTitle"], $_POST["IsAllDayEvent"]);
        break;
    case "list":
        $ret = listCalendar($_POST["showdate"], $_POST["viewtype"]);
        break;
    case "update": // $_POST["CalendarStartTime"] diventa $_POST["richiesta"]
        $ret = updateCalendar($_POST["calendarId"], $_POST["richiesta"], $_POST["CalendarEndTime"]);
        break; 
    case "remove":
        $ret = removeCalendar( $_POST["calendarId"]);
        break;
    case "adddetails":
        $st = $_POST["stpartdate"] . " " . $_POST["stparttime"];
        $et = $_POST["etpartdate"] . " " . $_POST["etparttime"];
        if(isset($_GET["id"])){
            $ret = updateDetailedCalendar($_GET["id"], $st, $et, 
                $_POST["Subject"], isset($_POST["IsAllDayEvent"])?1:0, $_POST["Description"], 
                $_POST["Location"], $_POST["colorvalue"], $_POST["timezone"]);
        }else{
            $ret = addDetailedCalendar($st, $et,                    
                $_POST["Subject"], isset($_POST["IsAllDayEvent"])?1:0, $_POST["Description"], 
                $_POST["Location"], $_POST["colorvalue"], $_POST["timezone"]);
        }        
        break; 


}
echo json_encode($ret); 



?>