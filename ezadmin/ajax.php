<?php
/*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_ADMIN_DIR_')) {
    define('_PS_ADMIN_DIR_', getcwd());
}
include(_PS_ADMIN_DIR_.'/../config/config.inc.php');

/* Getting cookie or logout */
require_once(_PS_ADMIN_DIR_.'/init.php');

$context = Context::getContext();

if (Tools::isSubmit('ajaxReferrers')) {
    require(_PS_CONTROLLER_DIR_.'admin/AdminReferrersController.php');
}

if (Tools::getValue('page') == 'prestastore' and @fsockopen('addons.prestashop.com', 80, $errno, $errst, 3)) {
    readfile('http://addons.prestashop.com/adminmodules.php?lang='.$context->language->iso_code);
}

if (Tools::isSubmit('getAvailableFields') and Tools::isSubmit('entity')) {
    $jsonArray = array();
    $import = new AdminImportController();

    $fields = $import->getAvailableFields(true);
    foreach ($fields as $field) {
        $jsonArray[] = '{"field":"'.addslashes($field).'"}';
    }
    die('['.implode(',', $jsonArray).']');
}

if (Tools::isSubmit('ajaxProductPackItems')) {
    $jsonArray = array();
    $products = Db::getInstance()->executeS('
	SELECT p.`id_product`, pl.`name`
	FROM `'._DB_PREFIX_.'product` p
	NATURAL LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
	WHERE pl.`id_lang` = '.(int)(Tools::getValue('id_lang')).'
	'.Shop::addSqlRestrictionOnLang('pl').'
	AND NOT EXISTS (SELECT 1 FROM `'._DB_PREFIX_.'pack` WHERE `id_product_pack` = p.`id_product`)
	AND p.`id_product` != '.(int)(Tools::getValue('id_product')));

    foreach ($products as $packItem) {
        $jsonArray[] = '{"value": "'.(int)($packItem['id_product']).'-'.addslashes($packItem['name']).'", "text":"'.(int)($packItem['id_product']).' - '.addslashes($packItem['name']).'"}';
    }
    die('['.implode(',', $jsonArray).']');
}

if (Tools::isSubmit('getChildrenCategories') && Tools::isSubmit('id_category_parent')) {
    $children_categories = Category::getChildrenWithNbSelectedSubCat(Tools::getValue('id_category_parent'), Tools::getValue('selectedCat'), Context::getContext()->language->id, null, Tools::getValue('use_shop_context'));
    die(Tools::jsonEncode($children_categories));
}

if (Tools::isSubmit('getNotifications')) {
    $notification = new Notification;
    die(Tools::jsonEncode($notification->getLastElements()));
}

if (Tools::isSubmit('updateElementEmployee') && Tools::getValue('updateElementEmployeeType')) {
    $notification = new Notification;
    die($notification->updateEmployeeLastElement(Tools::getValue('updateElementEmployeeType')));
}

if (Tools::isSubmit('searchCategory')) {
    $q = Tools::getValue('q');
    $limit = Tools::getValue('limit');
    $results = Db::getInstance()->executeS('SELECT c.`id_category`, cl.`name`
		FROM `'._DB_PREFIX_.'category` c
		LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (c.`id_category` = cl.`id_category`'.Shop::addSqlRestrictionOnLang('cl').')
		WHERE cl.`id_lang` = '.(int)$context->language->id.' AND c.`level_depth` <> 0
		AND cl.`name` LIKE \'%'.pSQL($q).'%\'
		GROUP BY c.id_category
		ORDER BY c.`position`
		LIMIT '.(int)$limit);
    if ($results) {
        foreach ($results as $result) {
            echo trim($result['name']).'|'.(int)$result['id_category']."\n";
        }
    }
}

if (Tools::isSubmit('getParentCategoriesId') && $id_category = Tools::getValue('id_category')) {
    $category = new Category((int)$id_category);
    $results = Db::getInstance()->executeS('SELECT `id_category` FROM `'._DB_PREFIX_.'category` c WHERE c.`nleft` < '.(int)$category->nleft.' AND c.`nright` > '.(int)$category->nright.'');
    $output = array();
    foreach ($results as $result) {
        $output[] = $result;
    }

    die(Tools::jsonEncode($output));
}

if (Tools::isSubmit('getZones')) {
    $html = '<select id="zone_to_affect" name="zone_to_affect">';
    foreach (Zone::getZones() as $z) {
        $html .= '<option value="'.$z['id_zone'].'">'.$z['name'].'</option>';
    }
    $html .= '</select>';
    $array = array('hasError' => false, 'errors' => '', 'data' => $html);
    die(Tools::jsonEncode($array));
}

if (Tools::isSubmit('getEmailHTML') && $email = Tools::getValue('email')) {
    $email_html = AdminTranslationsController::getEmailHTML($email);
    die($email_html);
}

/** \/ OVERRIDE TESTATI \/ **/

if (Tools::isSubmit('searchCustomerById')) {
    $id_customer = Tools::getValue('id_customer');
    if( str_replace(' ', '', $id_customer) != "" ){
        $customer = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'customer WHERE id_customer = '.Tools::getValue('id_customer'));
        if(!$customer)
            die('Nessun cliente trovato');
        else
        {
            if($customer['is_company'] == 1)
                die($customer['company']);
            else
                die($customer['firstname'].' '.$customer['lastname']);
        }
    } else {
        die('');
    }
    
}

if (Tools::isSubmit('aggiornaPeriodoForecast'))
{
    // Non decommentare, chiedere a Federico
	/*Db::getInstance()->executeS('DELETE FROM forecast WHERE id_product = '.Tools::getValue('id_product'));
	Db::getInstance()->executeS('INSERT INTO forecast (id_product, periodo) VALUES ("'.Tools::getValue('id_product').'", "'.Tools::getValue('periodo').'")');*/
	
	Db::getInstance()->executeS('DELETE FROM forecast WHERE id_product = 11111');
	Db::getInstance()->executeS('INSERT INTO forecast (id_product, periodo) VALUES ("11111", "'.Tools::getValue('periodo').'")');
	
	die('ok');
}

if (Tools::isSubmit('togli_allegati'))
{
	$ids = $_POST['id_allegati'];
	$risposta = '';
	
	foreach($ids as $allegato)
	{
		$name = Db::getInstance()->getValue('SELECT file FROM '._DB_PREFIX_.'attachment WHERE id_attachment = '.$allegato);
		unlink('../download/'.$name);
		Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'attachment WHERE id_attachment = '.$allegato);
		Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'attachment_lang WHERE id_attachment = '.$allegato);
		Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'product_attachment WHERE id_attachment = '.$allegato);
	}

	die($risposta);
}

// AdminProductsController - form SEO (solo per italiano)
if (Tools::isSubmit('SEOby'))
{
	$text = $_POST['text'];
	$keyword = strtolower($_POST['keyword']);
	$title = strtolower($_POST['title']);
	$meta_description = strtolower($_POST['meta_description']);
	$friendly_url = $_POST['url'];
	$urlified_keyword = $_POST['urlified_keyword'];
	$tipo = $_POST['tipo'];
	$text_plain = strip_tags($text);
	$htmlDoc = new DomDocument();
    $htmlDoc->loadhtml($text);
	$numero_link = 0;
	$numero_link_interni = 0;
	$numero_link_esterni = 0;
	$numero_link_assoluti = 0;
	$findH1 = false;
	$numero_h1 = 0;
	$numero_h2 = 0;
	$numero_h2_keyword = 0;
	$numero_immagini = 0;
	$host = 'ezdirect.it';
	$numero_parole = str_word_count($text_plain);
	$conto_keyword = substr_count(strtolower($text_plain),$keyword);
	$rapporto_keyword = (($conto_keyword * 100) / $numero_parole);
	$p = $htmlDoc->getElementsByTagName('p');
	$primo_paragrafo_elemento = $p[0];
	$primo_paragrafo = $primo_paragrafo_elemento->nodeValue;
	$keyword_length = strlen($keyword);
	$titoli_immagini = array();
	$alt_immagini = array();
	$result = '';
	
	// CERCO I LINK
	foreach($htmlDoc->getElementsByTagName('a') as $link) {
		$numero_link++; 
		$url = trim($link->getAttribute('href'));
		$pos = strpos($url,$host);
		if( $pos === false ){ // NON TROVO URL SITO
			if( (substr($url, 0, 1) == '/') ||  (substr($url, 0, 1) == '#') )
            {
				// ASSOLUTO
				$numero_link_assoluti++;
			}
			else
			{
				// ESTERNO
				$numero_link_esterni++;
			}
		}
		else {
			if( $pos < 20 ){
				// INTERNO
				$numero_link_interni++;
			}
			else
			{
				// ESTERNO
				$numero_link_esterni++;
				
			}
		}
	}
	
	// CERCO GLI H1
	foreach($htmlDoc->getElementsByTagName('h1') as $h1) {
		$numero_h1++;
		
		if (strpos(strtolower($h1->nodeValue), $keyword) !== false)
			$findH1 = true;
	}
	
	// CERCO GLI H2
	foreach($htmlDoc->getElementsByTagName('h2') as $h2) {
		$numero_h2++;
		
		if (strpos(strtolower($h2->nodeValue), $keyword) !== false)
			$numero_h2_keyword++;
	}
	
	// CERCO ATTRIBUTI ALT E TITLE
	foreach($htmlDoc->getElementsByTagName('img') as $img) {
		$numero_immagini++;
		$titoli_immagini[] = strtolower($img->getAttribute('alt'));
		$alt_immagini[] = strtolower($img->getAttribute('title'));
	}

	// REGOLA 1: PAROLE
	if($numero_parole < 300)
		$result .= '<i class="icon-circle" style="color:red" alt="" title=""></i>&nbsp;&nbsp;<span style="color:red">Le parole devono essere almeno 300. Attualmente ce ne sono '.$numero_parole.'</span><br /><br />';
	else
		$result .= '<i class="icon-circle" style="color:green" alt="" title=""></i>&nbsp;&nbsp;<span style="color:green">Ottimo, ci sono almeno 300 parole nel testo. Attualmente ce ne sono '.$numero_parole.'</span><br /><br />';
	
	// REGOLA 2: NUMERO LINK
	/*if($numero_link == 0)
		$result .= '<i class="icon-circle" style="color:red" alt="" title=""></i>&nbsp;&nbsp;<span style="color:red">Dovresti inserire almeno un link nel testo</span><br /><br />';
	else
		$result .= '<i class="icon-circle" style="color:green" alt="" title=""></i>&nbsp;&nbsp;<span style="color:green">Bene, nel testo ho trovato almeno un link: ce ne sono in tutto '.$numero_link.'</span><br /><br />';
	*/
	// REGOLA 3: NUMERO LINK INTERNI
	/*if($numero_link_interni == 0)
		$result .= '<i class="icon-circle" style="color:red" alt="" title=""></i>&nbsp;&nbsp;<span style="color:red">Dovresti inserire almeno un link interno nel testo</span><br /><br />';
	else
		$result .= '<i class="icon-circle" style="color:green" alt="" title=""></i>&nbsp;&nbsp;<span style="color:green">Bene, nel testo ho trovato almeno un link interno : ce ne sono in tutto '.$numero_link_interni.'</span><br /><br />';
	*/
	// REGOLA 4: NUMERO LINK ESTERNI
	/*if($numero_link_esterni == 0)
		$result .= '<i class="icon-circle" style="color:orange" alt="" title=""></i>&nbsp;&nbsp;<span style="color:orange">Potresti inserire almeno un link esterno nel testo</span><br /><br />';
	else
		$result .= '<i class="icon-circle" style="color:green" alt="" title=""></i>&nbsp;&nbsp;<span style="color:green">Bene, nel testo ho trovato almeno un link esterno : ce ne sono in tutto '.$numero_link_esterni.'</span><br /><br />';
	*/
	// REGOLA 5: NUMERO LINK RELATIVI
	if($numero_link_assoluti > 0)
	{
		$result .= '<i class="icon-circle" style="color:orange" alt="" title=""></i>&nbsp;&nbsp;<span style="color:orange">Ci sono '.$numero_link_assoluti.' link relativi nel sito. Cerca di trasformarli in link assoluti.</span><br /><br />';
	}
	
	// REGOLA 6: NUMERO H1

	if($tipo == 'product')
	{
		if($numero_h1 == 0)
			$result .= '<i class="icon-circle" style="color:red" alt="" title=""></i>&nbsp;&nbsp;<span style="color:red">Manca l\'h1 nel testo: inseriscilo!</span><br /><br />';
		else if($numero_h1 == 1)
			$result .= '<i class="icon-circle" style="color:green" alt="" title=""></i>&nbsp;&nbsp;<span style="color:green">Ottimo, ho trovato l\'h1 nel testo</span><br /><br />';
		else
			$result .= '<i class="icon-circle" style="color:red" alt="" title=""></i>&nbsp;&nbsp;<span style="color:red">Ci sono troppi h1 nel testo: ne ho trovati '.$numero_h1.'. Devi metterne solo uno</span><br /><br />';
	}
	
	// REGOLA 7: NUMERO H2
	if($numero_h2 == 0)
		$result .= '<i class="icon-circle" style="color:red" alt="" title=""></i>&nbsp;&nbsp;<span style="color:red">Dovresti inserire almeno un h2 nel testo</span><br /><br />';
	else
		$result .= '<i class="icon-circle" style="color:green" alt="" title=""></i>&nbsp;&nbsp;<span style="color:green">Bene, nel testo ho trovato almeno un h2: ce ne sono in tutto '.$numero_h2.'</span><br /><br />';
	
	// REGOLA 14: NUMERO IMMAGINI
		
	if($numero_immagini == 0)
		$result .= '<i class="icon-circle" style="color:red" alt="" title=""></i>&nbsp;&nbsp;<span style="color:red">Dovresti inserire almeno un\'immagine nel testo</span><br /><br />';
	else
		$result .= '<i class="icon-circle" style="color:green" alt="" title=""></i>&nbsp;&nbsp;<span style="color:green">Bene, nel testo ho trovato almeno un\'immagine: ce ne sono in tutto '.$numero_immagini.'</span><br /><br />';
	
	if($keyword != '')
	{
		// REGOLA 8: PRESENZA KEYWORD IN H1
		if($tipo == 'product')
		{
			if($findH1)
				$result .= '<i class="icon-circle" style="color:green" alt="" title=""></i>&nbsp;&nbsp;<span style="color:green">Bene, ho trovato la parola chiave nell\'h1</span><br /><br />';
			else
				$result .= '<i class="icon-circle" style="color:red" alt="" title=""></i>&nbsp;&nbsp;<span style="color:red">La parola chiave non &egrave; presente nell\'h1</span><br /><br />';
		}

		// REGOLA 9: PRESENZA KEYWORD IN H2
		if($numero_h2_keyword > 0)
			$result .= '<i class="icon-circle" style="color:green" alt="" title=""></i>&nbsp;&nbsp;<span style="color:green">Bene, ho trovato almeno una volta la parola chiave nell\'h2. In tutto compare '.$numero_h2_keyword.' volte</span><br /><br />';
		else
			$result .= '<i class="icon-circle" style="color:red" alt="" title=""></i>&nbsp;&nbsp;<span style="color:red">La parola chiave non &egrave; presente in nessuno degli h2</span><br /><br />';
		
		// REGOLA 10: KEYWORD DENSITY
		if($rapporto_keyword > 0 && $rapporto_keyword < 0.5)
			$result .= '<i class="icon-circle" style="color:orange" alt="" title=""></i>&nbsp;&nbsp;<span style="color:orange">La parola chiave compare troppe poche volte nel testo: la ricorrenza &egrave; dello '.number_format($rapporto_keyword,2,",","").'%. Prova a inserire la parola chiave altre volte.</span><br /><br />';
		else if ($rapporto_keyword > 0.5 && $rapporto_keyword < 2.5)
			$result .= '<i class="icon-circle" style="color:green" alt="" title=""></i>&nbsp;&nbsp;<span style="color:green">Bene, la ricorrenza della parola chiave &egrave; dello '.number_format($rapporto_keyword,2,",","").'%. In tutto compare '.$conto_keyword.' volte nel testo.</span><br /><br />';
		else if($rapporto_keyword > 2.5)
			$result .= '<i class="icon-circle" style="color:red" alt="" title=""></i>&nbsp;&nbsp;<span style="color:red">La parola chiave compare troppe volte nel testo e questo potrebbe essere penalizzante: la ricorrenza &egrave; dello '.number_format($rapporto_keyword,2,",","").'%. Prova a toglierla da alcune parti.</span><br /><br />';
		else if($rapporto_keyword <= 0)
			$result .= '<i class="icon-circle" style="color:red" alt="" title=""></i>&nbsp;&nbsp;<span style="color:red">La parola chiave non compare nel testo. Devi inserirla.</span><br /><br />';
		
		/*
		if($conto_keyword >= 1)
			$result .= '<i class="icon-circle" style="color:green" alt="" title=""></i>&nbsp;&nbsp;<span style="color:green">Bene, la keyword compare almeno una volta nel testo. In tutto compare '.$conto_keyword.' volte</span><br /><br />';
		else
			$result .= '<i class="icon-circle" style="color:red" alt="" title=""></i>&nbsp;&nbsp;<span style="color:red">La parola chiave non compare almeno tre volte nel testo. In tutto compare '.$conto_keyword.' volte</span><br /><br />';
		*/
		
		// REGOLA 11: KEYWORD IN META DESCRIPTION
		if (strpos(strtolower($meta_description), $keyword) !== false)
			$result .= '<i class="icon-circle" style="color:green" alt="" title=""></i>&nbsp;&nbsp;<span style="color:green">Bene, la parola chiave compare nella meta description.</span><br /><br />';
		else
			$result .= '<i class="icon-circle" style="color:red" alt="" title=""></i>&nbsp;&nbsp;<span style="color:red">Nella meta description manca la parola chiave</span><br /><br />';
		
		// REGOLA 12: KEYWORD IN PRIMO PARAGRAFO
		if (strpos(strtolower($primo_paragrafo), $keyword) !== false)
			$result .= '<i class="icon-circle" style="color:green" alt="" title=""></i>&nbsp;&nbsp;<span style="color:green">Bene, la parola chiave compare nel primo paragrafo.</span><br /><br />';
		else
			$result .= '<i class="icon-circle" style="color:red" alt="" title=""></i>&nbsp;&nbsp;<span style="color:red">La parola chiave non &egrave; presente nel primo paragrafo</span><br /><br />';
		
		if($tipo != 'produttore')
		{
			if (strpos(strtolower($friendly_url), $urlified_keyword) !== false)
				$result .= '<i class="icon-circle" style="color:green" alt="" title=""></i>&nbsp;&nbsp;<span style="color:green">Bene, la parola chiave compare nella friendly url.</span><br /><br />';
			else
				$result .= '<i class="icon-circle" style="color:red" alt="" title=""></i>&nbsp;&nbsp;<span style="color:red">La parola chiave non compare nella friendly url</span><br /><br />';
		}
		// REGOLA 13: KEYWORD IN TITLE E SUO POSIZIONAMENTO
		if(substr($title,0,$keyword_length) == $keyword)
			$result .= '<i class="icon-circle" style="color:green" alt="" title=""></i>&nbsp;&nbsp;<span style="color:green">Bene, la parola chiave compare subito all\'inizio del title tag.</span><br /><br />';
		else
		{
			if (strpos(strtolower($title), $keyword) !== false)
			$result .= '<i class="icon-circle" style="color:orange" alt="" title=""></i>&nbsp;&nbsp;<span style="color:orange">Il title tag contiene la parola chiave, ma non all\'inizio. Potresti pensare di spostarla.</span><br /><br />';
		else
			$result .= '<i class="icon-circle" style="color:red" alt="" title=""></i>&nbsp;&nbsp;<span style="color:red">La parola chiave non &egrave; presente nel title tag</span><br /><br />';
		}

		if($numero_immagini > 0)
		{
			$titoli_immagini = implode('*GLUE*',$titoli_immagini);
			$alt_immagini = implode('*GLUE*',$alt_immagini);
			
			if (strpos(strtolower($titoli_immagini), $keyword) !== false)
				$result .= '<i class="icon-circle" style="color:green" alt="" title=""></i>&nbsp;&nbsp;<span style="color:green">Bene, la parola chiave compare almeno una volta nei titoli delle immagini.</span><br /><br />';
			else
				$result .= '<i class="icon-circle" style="color:orange" alt="" title=""></i>&nbsp;&nbsp;<span style="color:orange">La parola chiave non &egrave; presente nei titoli delle immagini. Potresti inserirla</span><br /><br />';
			
			if (strpos(strtolower($alt_immagini), $keyword) !== false)
				$result .= '<i class="icon-circle" style="color:green" alt="" title=""></i>&nbsp;&nbsp;<span style="color:green">Bene, la parola chiave compare almeno una volta nelle descrizioni delle immagini.</span><br /><br />';
			else
				$result .= '<i class="icon-circle" style="color:orange" alt="" title=""></i>&nbsp;&nbsp;<span style="color:orange">La parola chiave non &egrave; presente nelle descrizioni delle immagini. Potresti inserirla</span><br /><br />';
		}
		
	}
	else
	{
		$result .= '<i class="icon-circle" style="color:orange" alt="" title=""></i>&nbsp;&nbsp;<span style="color:orange">Non hai impostato una parola chiave. Impostala per avere ulteriori indicazioni.</span><br /><br />';
	}
	die ($result);
}

if (Tools::isSubmit('aggiorna_dati_cliente'))
{
	if(Tools::getValue('tipo') == 'agente') {
		$total = Db::getInstance()->getValue('SELECT count(id_customer) FROM customer_amministrazione WHERE id_customer = '.Tools::getValue('cliente'));
		
		if($total == 0) 
			Db::getInstance()->execute('INSERT INTO customer_amministrazione (id_customer, agente) VALUES ('.Tools::getValue('cliente').', '.Tools::getValue('valore').')');
		else
			Db::getInstance()->execute('UPDATE customer_amministrazione SET agente = '.Tools::getValue('valore').' WHERE id_customer = '.Tools::getValue('cliente'));
		
		die('Agente');
	}
	
	if(Tools::getValue('tipo') == 'tecnico') {
		$total = Db::getInstance()->getValue('SELECT count(id_customer) FROM customer_amministrazione WHERE id_customer = '.Tools::getValue('cliente'));
		
		if($total == 0) 
			Db::getInstance()->execute('INSERT INTO customer_amministrazione (id_customer, tecnico) VALUES ('.Tools::getValue('cliente').', '.Tools::getValue('valore').')');
		else
			Db::getInstance()->execute('UPDATE customer_amministrazione SET tecnico = '.Tools::getValue('valore').' WHERE id_customer = '.Tools::getValue('cliente'));
			
		die('Tecnico');
	}
	
	if(Tools::getValue('tipo') == 'referente') {
		Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'customer SET referente = '.Tools::getValue('valore').' WHERE id_customer = '.Tools::getValue('cliente'));
		
		die('Referente');
	}
}

/* Dashboard */

// Correggere: errore durante l'operazione!
if (Tools::getIsset('getCartDisplay'))
{
	$cart = new Cart(Tools::getValue('id_cart'));
	$currency = new Currency($cart->id_currency);
	$customer = new Customer($cart->id_customer);
	
	$tokenCarts = Tools::getAdminToken('AdminCarts'.(int)(Tab::getIdFromClassName('AdminCarts')).(int)($context->employee->id));
	
	$dati_carrello = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'cart WHERE id_cart = '.$cart->id);
	$id_order = Db::getInstance()->getValue('SELECT id_order FROM '._DB_PREFIX_.'orders WHERE id_cart = '.$cart->id);
	$total_price_wt = $cart->getOrderTotal(false);
	
	$cartDisplay .= '<script type="text/javascript">
		$(function() {
			$(".span-reference").tooltipster({ interactive: true, contentAsHTML: true  });
			$(".img_control").tooltipster({ interactive: true, contentAsHTML: true  });
		});
		</script>	
		<table class="table3" style="margin-top:20px">
		<th class="right" style="text-align:right;" colspan="2">
		<strong>N. Carrello </strong></th>
		<th class="right" style="text-align:right;">Data</th>
		<th class="right" style="">Prev.?</th>
		<th class="right" style="">Letto?</th>
		<th class="right" style="">Conv.?</th>
		<th class="right" style="text-align:right;">N.Ord.</th>
		<th class="right" style="text-align:right;">Totale</th>
		<th class="right" style="">Oggetto</th>
		<th class="right" style="">Creato da</th>
		<th class="right" style="">In carico</th>
		<th style="text-align:right">Azioni</th>
	</tr>
	<tr><td colspan="2" style="text-align:right"><strong><a style="color:#cc0000"href="index.php?tab=AdminCustomers&id_customer='.$cart->id_customer.'&viewcart&id_cart='.$cart->id.'&viewcustomer&token='. Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$context->employee->id).'&tab-container-1=4">'.$cart->id.'</a></span></td>
	
	<td style="text-align:right">'.Tools::displayDate($cart->date_add, (int)($context->language->id), false).'</td>
	<td style="text-align:right">'.($dati_carrello['preventivo'] == 1 ?  "<img src='https://www.ezdirect.it/img/admin/enabled.gif' alt='Si' title='Si' />" : "<img src='https://www.ezdirect.it/img/admin/red_no.gif' alt='No' title='No' />").'</td>
	<td style="text-align:right" >'.($dati_carrello['visualizzato'] == 1 ?  "<img src='https://www.ezdirect.it/img/admin/enabled.gif' alt='Si' title='Si' />" : "<img src='https://www.ezdirect.it/img/admin/red_no.gif' alt='No' title='No' />").'</td>
	<td style="text-align:right" >'.($id_ordine == 1 ?  "<img src='https://www.ezdirect.it/img/admin/enabled.gif' alt='Si' title='Si' />" : "<img src='https://www.ezdirect.it/img/admin/red_no.gif' alt='No' title='No' />").'</td>
	<td style="text-align:right">'.($id_ordine > 0 ? $id_ordine : '--').'</td>
	
	
	<td style="text-align:right">'.Tools::displayPrice($total_price_wt).'</td>
	<td style="text-align:right"><span title="'.$dati_carrello['name'].'">'.substr($dati_carrello['name'],0,15).' '.(strlen($dati_carrello['name']) > 15 ? '...' : '').'</td>
	
	<td style="text-align:right">'.($dati_carrello['created_by'] == 0 ? 'Cliente' : Db::getInstance()->getValue("SELECT CONCAT(firstname,' ',LEFT(lastname, 1),'.')  FROM "._DB_PREFIX_."employee WHERE id_employee = ".$dati_carrello['created_by']."")).'</td>
	<td style="text-align:right">'.Db::getInstance()->getValue("SELECT CONCAT(firstname,' ',LEFT(lastname, 1),'.')  FROM "._DB_PREFIX_."employee WHERE id_employee = ".$dati_carrello['in_carico_a']."").'</td>
	<td align="right">	<a href="index.php?tab=AdminCarts&id_customer='.$cart->id_customer.'&id_cart='.$cart->id.'&deleteccart&token='.$tokenCarts.'" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }"><img src="../img/admin/delete.gif" /></a></td>
	</tr>
	';
	
	$cart_products = Db::getInstance()->executeS("select cp.*,t.rate as tax_rate,p.quantity as stock,p.id_tax_rules_group,p.price as vendita,p.price as product_price,p.listino as listino,p.wholesale_price as acquisto,p.sconto_acquisto_1 as sc_acq_1, p.sconto_acquisto_2 as sc_acq_2, p.sconto_acquisto_3 as sc_acq_3, p.quantity as qt_tot, p.stock_quantity as magazzino, p.supplier_quantity as allnet, p.esprinet_quantity as esprinet, p.itancia_quantity as itancia, p.ordinato_quantity as qt_ordinato, p.impegnato_quantity as qt_impegnato, p.arrivo_quantity as qt_arrivo, p.arrivo_esprinet_quantity as qt_arrivo_esprinet, p.reference as product_reference,pl.name as product_name,i.id_image from "._DB_PREFIX_."cart_product cp left join ". _DB_PREFIX_."product p on  cp.id_product=p.id_product left join (select * from image where cover = 1) i on i.id_product = p.id_product left join ". _DB_PREFIX_."product_lang pl  on  cp.id_product=pl.id_product 
	LEFT JOIN `"._DB_PREFIX_."tax_rule` tr ON (p.`id_tax_rules_group` = tr.`id_tax_rules_group`
	AND tr.`id_country` = ".(int)Country::getDefaultCountryId()."
	AND tr.`id_state` = 0)
	LEFT JOIN `"._DB_PREFIX_."tax` t ON (t.`id_tax` = tr.`id_tax`)
	WHERE pl.id_lang = ".$context->language->id."
	AND id_cart=".$cart->id." GROUP BY cp.id_product ORDER BY cp.sort_order ASC");
						
	$cartDisplay .= '<tr class="invisible-table-row subs sub_cart_'.$cart->id.'">
	<th style="width:20px">Riga</th>
	<th style="width: 100px; text-align:right">Codice</th>
	<th colspan="4" style="width: 290px">Nome prodotto</th>
	<th style="width: 35px; text-align: right">Qta</th>
	<th style="width: 95px; text-align: right">Unitario</th>
	<th style="width: 115px; text-align: right">'.($context->employee->id_profile == 7 ? '' : 'Acquisto').'</th>
	<th style="width: 65px; text-align: right">'.($context->employee->id_profile == 7 ? '' : 'Margine').'</th>
	<th style="width: 60px; text-align: right">Magaz.</th>
	<th style="width: 65px; text-align: right">Tot. riga</th>
	</tr>';
	$i = 1;
	
	foreach($cart_products as $product)
	{
		$product['product_reference'] = Db::getInstance()->getValue('SELECT reference FROM '._DB_PREFIX_.'product WHERE id_product = '.$product['id_product']);
		$product['product_name'] = Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'product_lang WHERE id_lang = '.$context->language->id.' AND id_product = '.$product['id_product']);	
							
		$prezzo_partenza = ($product['price'] == 0 && $product['free'] == 0 && $product['sc_qta'] == 1 ? Product::trovaMigliorPrezzo($product['id_product'],$customer->id_default_group,$product['quantity']) - (Product::trovaMigliorPrezzo($product['id_product'],$customer->id_default_group,$product['quantity']) * ($product['sconto_extra']/100)) : ($product['price'] == 0 && $product['free'] == 0 && $product['sc_qta'] == 0 ? Product::trovaMigliorPrezzo($product['id_product'],1,1) - ( Product::trovaMigliorPrezzo($product['id_product'],1,1) * ($product['sconto_extra'] / 100))  : $product['price']));
					  
		if($id_order && !Tools::getIsset('vedirevisione'))
		{
			if($prezzo_partenza == 0)
				$prezzo_partenza = Db::getInstance()->getValue('SELECT (price - ((price*sconto_extra)/100)) FROM '._DB_PREFIX_.'cart_product WHERE id_product = '.$product['id_product'].' AND id_cart = '.$cart->id);
										
			if($prezzo_partenza == 0) 
			{	
				$is_product = Db::getInstance()->getValue('SELECT product_id FROM '._DB_PREFIX_.'order_detail WHERE product_id = '.$product['id_product']);
									
				if($is_product > 0)
				{
					$prezzo_partenza = number_format(Db::getInstance()->getValue('SELECT (product_price - ((product_price*reduction_percent)/100)) price FROM '._DB_PREFIX_.'order_detail WHERE product_id = '.$product['id_product'].' AND id_order = '.$id_order),2,',','');
					$productprice = $prezzo_partenza;
				}	
			}
		}

		$quantity = $product['quantity'];
		
		if($product['prezzo_acquisto'] > 0)
			$wholesale_price = $product['prezzo_acquisto'];
		else
			$wholesale_price = Db::getInstance()->getValue('SELECT wholesale_price FROM '._DB_PREFIX_.'product WHERE id_product = '.$product['id_product']);
		
		$nome_prodotto = $product['name'];
		
		if($nome_prodotto == '')
			$nome_prodotto = $product['product_name'];
									
		$cartDisplay .= '<tr class="invisible-table-row subs sub_cart_'.$cart->id.'" style="'.(((($prezzo_partenza - $wholesale_price)*100)/$prezzo_partenza) < 10 && $prezzo_partenza > 0 ? 'background-color:#ffcccc"' : "").'">
		<td style="text-align:right">'.$i.'</td>
		<td align="right">
		<span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($product['id_product']).'"><a href="index.php?tab=AdminCatalog&id_product='.$product['id_product'].'&updateproduct&token='.$tokenCatalog.'">'.$product['product_reference'].'</a></span></td>
		<td colspan="4"><span class="productName" title="'.$nome_prodotto.'">'.substr($nome_prodotto,0,35).' '.(strlen($nome_prodotto) > 35 ? '...' : '').'</span></td>
		<td align="right" class="productQuantity" '.($quantity > 1 && $product['customizationQuantityTotal'] > 0 ? 'style="font-weight:700;font-size:1.1em;color:red"' : '').'>'.(int)$quantity.'</td>
		<td align="right">'.Tools::displayPrice($prezzo_partenza, $currency, false).'</td>
		
		<td align="right">'.($context->employee->id_profile == 7 ? '' : Tools::displayPrice($wholesale_price, $currency, false)).'</td>
		<td align="right">'.($context->employee->id_profile == 7 ? '' : number_format(((($prezzo_partenza - $wholesale_price)*100)/$prezzo_partenza),2,",","").'%').'</td>
		<td align="right" class="productQuantity">'.Db::getInstance()->getValue('SELECT stock_quantity FROM product WHERE id_product = '.$product['id_product']).'</td>
		<td align="right">'.Tools::displayPrice(Tools::ps_round($prezzo_partenza, 2) * ((int)($product['quantity'])), $currency, false).'</td>
		</tr>';
		$i++;
	}	
	
	if($id_order > 0)
		$costo_spedizione = Db::getInstance()->getValue('SELECT (total_shipping / ((carrier_tax_rate/100)+1)) spedizione FROM orders WHERE id_order = '.$id_order);
	else
	{
		$provincia_cliente = Db::getInstance()->getValue("SELECT id_state FROM "._DB_PREFIX_."address WHERE id_customer = ".$customer->id." AND active = 1 AND deleted = 0 AND fatturazione = 1");
			
		$nazione_cliente = Db::getInstance()->getValue("SELECT id_country FROM "._DB_PREFIX_."address WHERE id_customer = ".$customer->id." AND active = 1 AND deleted = 0 AND fatturazione = 1");
			
		if($provincia_cliente == 0) {
			$zona_cliente = Db::getInstance()->getValue("SELECT id_zone FROM "._DB_PREFIX_."country WHERE id_country = ".$nazione_cliente."");
		}
		else {
			$zona_cliente = Db::getInstance()->getValue("SELECT id_zone FROM "._DB_PREFIX_."state WHERE id_state = ".$nazione_cliente."");
		}
		
		$gruppi_per_spedizione = array();
		$gruppi_per_spedizione[] = $customer->id_default_group;
		
		$spedizione_default = Db::getInstance()->getValue('SELECT d.`price` FROM `'._DB_PREFIX_.'delivery` d LEFT JOIN `'._DB_PREFIX_.'range_price` r ON d.`id_range_price` = r.`id_range_price` WHERE d.`id_zone` = '.$zona_cliente.' AND 1 >= r.`delimiter1` AND 156 < r.`delimiter2` AND d.`id_carrier` = '.$default_carrier.' ORDER BY r.`delimiter1` ASC');
			
		$costo_trasporto_modificato_r = Db::getInstance()->getValue("SELECT transport FROM "._DB_PREFIX_."cart WHERE id_cart = ".$cart->id."");
		$costo_trasporto_modificato_r = split(":",$costo_trasporto_modificato_r);
		$costo_trasporto_modificato = $costo_trasporto_modificato_r[1];
		if($carrier_cart != 0) {
			if($costo_trasporto_modificato != "" && $costo_trasporto_modificato > 0) {
				$costo_spedizione = $costo_trasporto_modificato;
			}
			else {				
				$costo_spedizione = $cart->getOrderShippingCost($carrier_cart, false);
				$costo_spedizione = $cart->getOrderShippingCostByTotalAndDelivery($carrier_cart, $total_price_wte, $cart->id_address_delivery, $customer->id_default_group, false);
			}
		}
		else {
			$costo_spedizione = $cart->getOrderShippingCostByTotalAndDelivery($default_carrier, $total_price_wt, $cart->id_address_delivery, $customer->id_default_group, false);
		}
		
	}	
	
	$cartDisplay .= '<tr class="invisible-table-row subs sub_'.$cart->id.'">
	<td style="text-align:right">'.$i.'</td>
	<td align="right">TRASP</td><td colspan="4">Trasporto</td><td align="right">1</td>
	<td align="right">'.Tools::displayPrice(($costo_trasporto_modificato == '' || $costo_trasporto_modificato == 0 ? $costo_spedizione : $costo_trasporto_modificato), $currency, false).'</td>
	<td align="right"></td><td align="right" class="productQuantity"></td><td align="right"></td>
	<td align="right">'.Tools::displayPrice(($costo_trasporto_modificato == '' || $costo_trasporto_modificato == 0 ? $costo_spedizione : $costo_trasporto_modificato), $currency, false).'</td></tr>';
	
	$cartDisplay .= '
		<tr><th colspan="11" style="text-align:right; ">Importo</th>
		<th style="width:80px; text-align:right; ">'.Tools::displayPrice($total_price_wt).'</th>
	</tr>';
	
	$cartDisplay .= '</table><br /><br />';

	die($cartDisplay);
}

// Correggere: errore durante l'operazione!
if (Tools::getIsset('getOrderDisplay'))
{
	$ord = new Order(Tools::getValue('id_order'));
	$currency = new Currency($ord->id_currency);
	
	$orderDisplay .= '<table class="table3" style="margin-top:20px">
		<th class="right" style="text-align:right;" colspan="2"><strong>N. Ordine </strong></th>
		<th class="right" style="text-align:right;">Data</th>
		<th class="right" style="text-align:right;">Carrello</th>
		<th style="text-align:right;">Pagamento</th>
		
		<th class="right" style="text-align:right;" colspan="2">Stato pagamento</th>
		<th class="right" style="text-align:right;" colspan="2">Stato tecnico</th>
		<th style="text-align:right;" colspan="2">Tracking</th>
		
		<th style="text-align:right;">Importo</th>
	</tr>
	<tr><td colspan="2" style="text-align:right"><strong><a style="color:#cc0000"href="index.php?tab=AdminCustomers&id_customer='.$ord->id_customer.'&vieworder&id_order='.$ord->id.'&viewcustomer&token='. Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$context->employee->id).'&tab-container-1=4">'.$ord->id.'</a></span></td>
	
	<td style="text-align:right">'.Tools::displayDate($ord->date_add, (int)($context->language->id), false).'</td>
	<td style="text-align:right">'.$ord->id_cart.'</td>
	<td style="text-align:right" ><span title="'.$ord->payment.'">'.substr($ord->payment,0,11).' '.(strlen($ord->payment) > 11 ? '...' : '').'</span></td>
	<td style="text-align:right" colspan="2"><span title="'.strip_tags(Order::getTechState($ord->id, date('Y-m-d H:i:s'), 'pay')).'">'.substr(Order::getTechState($ord->id, date('Y-m-d H:i:s'), 'pay'),0,46).' '.(strlen(Order::getTechState($ord->id, date('Y-m-d H:i:s'), 'pay')) > 46 ? '...' : '').'</span></td>
	<td style="text-align:right" colspan="2"><span title="'.strip_tags(Order::getTechState($ord->id, date('Y-m-d H:i:s'), 'pay')).'">'.substr(Order::getTechState($ord->id, date('Y-m-d H:i:s'), 'tech'),0,46).' '.(strlen(Order::getTechState($ord->id, date('Y-m-d H:i:s'), 'tech')) > 46 ? '...' : '').'</span></td>
	
	<td style="text-align:right" colspan="2">'.$ord->shipping_number.'</td>
	<td align="right">'.Tools::displayPrice(($ord->total_products+($ord->total_shipping/(($ord->carrier_tax_rate/100)+1))), $currency).'</td>
	';
	
	
	$orderDisplay .= '
	<script type="text/javascript">
	$(function() {
		$(".span-reference").tooltipster({ interactive: true, contentAsHTML: true  });
		$(".img_control").tooltipster({ interactive: true, contentAsHTML: true  });
	});
	</script>	
	<tr class="invisible-table-row subs sub_'.$ord->id.'">
	<th style="width:20px">Riga</th>
	<th style="width: 80px; text-align:right">Codice</th>
	<th style="width: 280px" colspan="3">Prodotto - Servizio</th>
	<th style="width: 35px; text-align: right">Qta</th>
	<th style="width: 105px; text-align: right">Unitario</th>
	<th style="width: 105px; text-align: right">'.($context->employee->id_profile == 7 ? '' : 'Acquisto').'</th>
	<th style="width: 55px; text-align: right">'.($context->employee->id_profile == 7 ? '' : 'Margine').'</th>
	<th style="width: 50px; text-align: right">Magaz.</th>
	<th style="width: 50px; text-align: right">Kit?</th>
	<th style="width: 85px; text-align: right">Tot. riga</th>
	
	</tr>';
	$products = $ord->getProducts();
	$i = 1;
	foreach ($products as $k => $product)
	{
		
		$product_price = $product['product_price'];
		
		$quantity = $product['product_quantity'] - $product['customizationQuantityTotal'];
		
		if($product['wholesale_price'] > 0)
			$wholesale_price = $product['wholesale_price'];
		else
		{
			if($product['no_acq'] == 1)
				$wholesale_price = 0;
			else
				$wholesale_price = Db::getInstance()->getValue('SELECT wholesale_price FROM '._DB_PREFIX_.'product WHERE id_product = '.$product['product_id']);
		}		
		$orderDisplay .= '<tr class="invisible-table-row subs sub_'.$ord->id.'" style="'.(((($product_price - $wholesale_price)*100)/$product_price) < 10 && $product_price > 0 ? ' background-color:#ffcccc"' : "").'">
			<td style="text-align:right">'.$i.'</td>
			<td align="right">
			<span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($product['product_id']).'"><a href="index.php?tab=AdminCatalog&id_product='.$product['product_id'].'&updateproduct&token='.$tokenCatalog.'">'.$product['product_reference'].'</a></span></td>
			<td colspan="3"><span class="productName" title="'.$product['product_name'].'">'.substr($product['product_name'],0,30).' '.(strlen($product['product_name']) > 30 ? '...' : '').'</span></td>
			<td align="right" class="productQuantity" '.($quantity > 1 && $product['customizationQuantityTotal'] > 0 ? 'style="font-weight:700;font-size:1.1em;color:red"' : '').'>'.(int)$quantity.'</td>
			<td align="right">'.Tools::displayPrice($product_price, $currency, false).'</td>
			
			<td align="right">'.($context->employee->id_profile == 7 ? '' : Tools::displayPrice($wholesale_price, $currency, false)).'</td>
			<td align="right">'.($context->employee->id_profile == 7 ? '' : number_format(((($product_price - $wholesale_price)*100)/$product_price),2,",","").'%').'</td>
			<td align="right" class="productQuantity">'.Db::getInstance()->getValue('SELECT stock_quantity FROM '._DB_PREFIX_.'product WHERE id_product = '.$product['product_id']).'</td>
			<td align="right">'.Product::controllaSeFaParteDiUnBundle($ord->id, $product['product_id']).'</td>
			<td align="right">'.Tools::displayPrice(Tools::ps_round($product_price, 2) * ((int)($product['product_quantity']) - $product['customizationQuantityTotal']), $currency, false).'</td>
			</tr>';
			$i++;
	}
	$orderDisplay .=  "";
	
	$ordershipping = ((($ord->total_shipping*100)/(100+$ord->carrier_tax_rate)));

	$orderDisplay .= '<tr class="invisible-table-row subs sub_'.$ord->id.'">
	<td style="text-align:right">'.$i.'</td>
	<td align="right">TRASP</td><td colspan="3">Trasporto</td><td align="right">1</td>
	<td align="right">'.Tools::displayPrice($ordershipping, $currency, false).'</td><td align="right"></td>
	<td align="right"></td><td align="right" class="productQuantity"></td><td align="right"></td>
	<td align="right">'.Tools::displayPrice($ordershipping, $currency, false).'</td></tr>';
	
	$orderDisplay .= '
		<tr><th colspan="11" style="text-align:right; ">Importo</th>
		<th style="width:80px; text-align:right; ">'.Tools::displayPrice(($ord->total_products+($ord->total_shipping/(($ord->carrier_tax_rate/100)+1))), $currency).'</th>
	</tr>';
	
	$orderDisplay .= '
	</table><br /><br />';
	
	if(Tools::getIsset('ultime') == 'y')
	{
		$carrello = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'cart WHERE id_cart = '.$ord->id_cart);
		$in_carico = $carrello['in_carico'];
		
		$ahrefcosa = '<a href="index.php?tab=AdminCustomers&id_customer='.$ord->id_customer.'&viewcart&id_cart='.$carrello['id_cart'].'&viewcustomer&token='.Tools::getAdminToken('AdminCarts'.(int)(Tab::getIdFromClassName('AdminCarts')).(int)($context->employee->id)).'&tab-container-1=4">'; $creato_da = $carrello['created_by']; if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
		
		if(is_numeric($in_carico)) {
			$newinc = new Employee($in_carico);
			$in_carico = $newinc->firstname." ".substr($newinc->lastname, 0,1).".";
		}
		else
		{
			$in_carico = '--';
		}
		
		$numero_notifiche =  Db::getInstance()->getValue("SELECT numero_notifiche FROM "._DB_PREFIX_."cart WHERE id_cart = ".$ultima_cosa['id']);
		$status_s = ($carrello['preventivo'] == 1 ? '<img src="../img/admin/enabled.gif" alt="Letto" title="Letto" />Letto' : '<img src="../img/admin/red_no.gif" alt="Non letto" title="Non letto" />Non letto');
		
		if($carrello['numero_notifiche'] > 0)
			$status_s .= '&nbsp;&nbsp;&nbsp;<img src="../img/admin/email-sent.gif" alt="Carrello notificato al cliente" title="Carrello notificato al cliente" />';
		
		$status = $status_s;
		
		if($carrello['preventivo'] == 0)
			$status = '--';
		
		$cartID = new Cart((int)($carrello['id_cart']));
		$importo_uc = Tools::displayPrice($cartID->getOrderTotal(false),1);
		
		$orderDisplay .= "<table class='table'>
		<tr><th style='width:100px; max-width:100px'>Tipo 
			
			</th><th style='width:70px; max-width:70px'>ID</th><th style='width:70px; max-width:70px'>Creato da</th><th  style='width:70px; max-width:70px'>In carico a</th><th style='width:30px; max-width:30px'>Conv.</th><th style='max-width:150px'>Status</th><th style='max-width:70px'>Scaduto</th><th style='max-width:100px'>Oggetto</th><th style='width:70px; max-width:70px'>Totale</th><th>Data apertura</th><th>Ultima modifica</th></tr>
		
		<tr><td style='width:100px; max-width:100px'>".($carrello['preventivo'] == 1 ? 'C.Preventivo' : 'Carrello')." 
			</td>
			<td style='width:70px; max-width:70px'>".$carrello['id_cart'].'-'.$carrello['revisioni']."</td>
			<td style='width:70px; max-width:70px'>".$creato_da."</td><td style='width:70px; max-width:70px'>".$in_carico."</td>
			
			<td style='width:30px; max-width:30px'><img src='https://www.ezdirect.it/img/admin/enabled.gif' alt='Si' title='Si' /></td>
			
			<td style='max-width:150px'>".$status."</td>
			<td style='max-width:70px'></td>
			
			<td style='max-width:100px'>".$carrello['name']."</td>
			
			<td style='width:70px; max-width:70px'>".Tools::displayPrice($importo_uc, 1)."</td>
			
			<td>".Tools::displayDate($carrello['date_add'],5)."</td>
			<td>".Tools::displayDate($carrello['date_upd'],5)."</td></tr></table>";
	}
	
	die($orderDisplay);
}

/* Fine Dashboard */

if (Tools::getIsset('readMail'))
{
	$connection = mysqli_connect($_POST['database_server'], $_POST['database_user'], $_POST['database_pass']);
	mysqli_select_db($connection,$_POST['database']);
	
	$result = mysqli_query($connection,'SELECT * FROM mail WHERE id = '.$_POST['id']);
	$email = mysqli_fetch_array($result);
	//$email = Db::getInstance()->getRow('SELECT * FROM mail WHERE id = '.$_POST['id']); // Tabella per test

	$allegati_string = '';
	
	if($email['numero_allegati'] > 0)
	{
		$allegati = unserialize($email['allegati']);
		foreach ($allegati as $allegato)
		{
			$allegati_html .= '<img src="../img/admin/import.gif" alt="Allegato" title="Allegato" /> <a target="_blank" href="http://uploads:asdf6Hsrbxh!64@www.ezdirectftp.it/um/'.$email['id'].'/'.$allegato.'">'.$allegato.'</a> - ';
			
		}

		$allegati_string = '<tr><th>Allegati</th><td>'.$allegati_html.'</td></tr>';
	}

	$email_table  = '<table class="table" style="width:100%">';
	$email_table .= '<tr><th style="width:100px">Da</th><td>'.htmlentities($email['da_intestazione']).' <a href="mailto:'.$email['da'].'"><img src="../img/admin/outlook.gif" alt="Invia una mail" title="Invia una mail" /></a> </td></tr>';
	$email_table .= '<tr><th>A</th><td>'.htmlentities($email['a_intestazione']).'</td></tr>';
	$email_table .= '<tr><th>Oggetto</th><td>'.$email['oggetto'].'</td></tr>';
	$email_table .= '<tr><th>Data</th><td>'.date('d/m/Y H:i:s', strtotime($email['data'])).'</td></tr>';
	$email_table .= $allegati_string;
	$email_table .= '<tr><td colspan="2"><div style="max-width:600px">'.str_replace('<style', '<style scoped ',$email['messaggio']).'</div></td></tr>';
	$email_table .= '</table><script type="text/javascript" src="https://www.ezdirect.it/js/jquery/scoper.min.js"></script>';
		
	$mailHTML = '';
	$mailHTML .= utf8_encode('<br /><br />'.$email_table);

	die($mailHTML);
}

if (Tools::getIsset('deletePromemoria'))
{
	Db::getInstance()->Execute('DELETE FROM ticket_promemoria WHERE msg = "'.Tools::getValue('msg').'" AND tipo_att = "'.Tools::getValue('tipo_att').'" AND ora = "'.Tools::getValue('ora').'"');
	die('ok');
}

/* Note private */

// Tabella note_attivita

// Modificata: aggiunto in $_POST 'tipo' per farla funzionare con tutti i tipi di azione
if(Tools::getIsset('creanota_attivita') && Tools::getValue('creanota_attivita') == 'y') 
{
	Db::getInstance()->execute("
		INSERT INTO note_attivita (id_note, id_attivita, tipo_attivita, id_employee, note, date_add, date_upd)
		VALUES (
			NULL,
			'".Tools::getValue('id_action')."',
			'".Tools::getValue('tipo')."',
			'".$context->employee->id."',
			'".addslashes(Tools::getValue('nota'))."',
			'".date("Y-m-d H:i:s")."',
			'".date("Y-m-d H:i:s")."'
		)");

	print Db::getInstance()->Insert_ID();
}

if(Tools::getIsset('modificanota_attivita') && Tools::getValue('modificanota_attivita') == 'y') 
{
	Db::getInstance()->execute("
		UPDATE note_attivita 
		SET note = '".addslashes(Tools::getValue('nota'))."', date_upd = '".date("Y-m-d H:i:s")."', id_employee = '".$context->employee->id."' 
		WHERE id_note = ".Tools::getValue('id_nota')
	);
}

if(Tools::getIsset('cancellanota_attivita') && Tools::getValue('cancellanota_attivita') == 'y') 
{
	Db::getInstance()->execute("DELETE FROM note_attivita WHERE id_note = ".Tools::getValue('id_nota'));
}

// Tabella customer_note

if(Tools::getIsset('creanota_customer') && Tools::getValue('creanota_customer') == 'y') 
{
	Db::getInstance()->execute("
		INSERT INTO customer_note (id_note, id_customer, id_employee, note, date_add, date_upd)
		VALUES (
			NULL,
			'".Tools::getValue('id_customer')."',
			'".$context->employee->id."',
			'".addslashes(Tools::getValue('nota'))."',
			'".date("Y-m-d H:i:s")."',
			'".date("Y-m-d H:i:s")."'
		)
	");

	print Db::getInstance()->Insert_ID();
}

if(Tools::getIsset('modificanota_customer') && Tools::getValue('modificanota_customer') == 'y') 
{
	Db::getInstance()->execute("
		UPDATE customer_note 
		SET note = '".addslashes(Tools::getValue('nota'))."', date_upd = '".date("Y-m-d H:i:s")."', id_employee = '".$context->employee->id."' 
		WHERE id_note = ".Tools::getValue('id_nota')
	); 
}

if(Tools::getIsset('cancellanota_customer') && Tools::getValue('cancellanota_customer') == 'y') 
{
	Db::getInstance()->executeS("DELETE FROM customer_note WHERE id_note = ".Tools::getValue('id_nota'));
}

// Tabella cart_note

if(Tools::getIsset('creanota_cart') && Tools::getValue('creanota_cart') == 'y') 
{
	Db::getInstance()->execute("
		INSERT INTO cart_note (id_note, id_cart, id_employee, note, date_add, date_upd)
		VALUES (
			NULL,
			'".Tools::getValue('id_cart')."',
			'".$context->employee->id."',
			'".addslashes(Tools::getValue('nota'))."',
			'".date("Y-m-d H:i:s")."',
			'".date("Y-m-d H:i:s")."'
		)
	");

	print Db::getInstance()->Insert_ID();
}

if(Tools::getIsset('modificanota_cart') && Tools::getValue('modificanota_cart') == 'y') 
{
	Db::getInstance()->execute("
		UPDATE cart_note 
		SET note = '".addslashes(Tools::getValue('nota'))."', date_upd = '".date("Y-m-d H:i:s")."', id_employee = '".$context->employee->id."' 
		WHERE id_note = ".Tools::getValue('id_nota')
	); 
}

if(Tools::getIsset('cancellanota_cart') && Tools::getValue('cancellanota_cart') == 'y') 
{
	Db::getInstance()->executeS("DELETE FROM cart_note WHERE id_note = ".Tools::getValue('id_nota'));
}

/* Fine note private */

/* Prezzi speciali */
/* Correggere: si può usare una funzione unica con un parametro tipo per la modifica dei prezzi speciali di acq e vnd */

// Modifica prezzo speciale di acquisto nella list
if(Tools::getIsset('modifica_speciale_acq'))
{
	// correggere: modifica l'orario inserito, è ok?
	$date_from_array = explode("/", $_POST['partenza']);
	$date_from = $date_from_array[2]."-".$date_from_array[1]."-".$date_from_array[0]." 00:00:00";
	
	$date_to_array = explode("/", $_POST['fine']);
	$date_to = $date_to_array[2]."-".$date_to_array[1]."-".$date_to_array[0]." 23:59:59";
	
	Db::getInstance()->execute("UPDATE "._DB_PREFIX_."specific_price_wholesale sp SET sp.reduction_1 = '".str_replace(",",".", Tools::getValue('reduction_1'))."', sp.reduction_2 = '".str_replace(",",".", Tools::getValue('reduction_2'))."', sp.reduction_3 = '".str_replace(",",".", Tools::getValue('reduction_3'))."', sp.wholesale_price = '".str_replace(",",".", Tools::getValue('prezzo_speciale'))."', sp.from = '".$date_from."', sp.to = '".$date_to."' WHERE sp.id_specific_price = '".Tools::getValue('id_speciale')."'");

	die;
}

// Modifica prezzo speciale di vendita nella list
if(Tools::getIsset('modifica_speciale'))
{
	// correggere: modifica l'orario inserito, è ok?
	$date_from_array = explode("/", $_POST['partenza']);
	$date_from = $date_from_array[2]."-".$date_from_array[1]."-".$date_from_array[0]." 00:00:00";
	
	$date_to_array = explode("/", $_POST['fine']);
	$date_to = $date_to_array[2]."-".$date_to_array[1]."-".$date_to_array[0]." 23:59:59";
	
	Db::getInstance()->execute("UPDATE "._DB_PREFIX_."specific_price sp SET sp.reduction_type = 'percentage', sp.reduction = '0', sp.price = '".str_replace(",",".", Tools::getValue('prezzo_speciale'))."', sp.from = '".$date_from."', sp.to = '".$date_to."' WHERE sp.id_specific_price = '".Tools::getValue('id_speciale')."'");

	die;
}

if (Tools::getIsset('changeDDT')) 
{
	$ddt = Tools::getValue('changeDDT');
	
	Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'order_detail SET ddt = "'.$ddt.'" WHERE product_id = '.$_POST['id_product'].' AND id_order = '.$_POST['id_order']);
	die('Ok');
}

// "Inoltra CC a" di azioni cliente
if(Tools::isSubmit('inoltra'))
{
	$utenti = $_POST['utenti'];
	$utenti = explode(',',$utenti);

	$tipo_richiesta_p = $_POST['tipo'];
	$tipo_richiesta_string = ($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo');

	$id_messaggio = $_POST['id_messaggio'];
	$cc2 = '';

	foreach($utenti as $conoscenza)
	{
		$mailconoscenza = Db::getInstance()->getValue("SELECT email FROM "._DB_PREFIX_."employee WHERE id_employee = ".$conoscenza);
							
		$tokenimp = Tools::getAdminToken("AdminCustomers".(int)Tab::getIdFromClassName("AdminCustomers").$conoscenza);
		$employeemess = new Employee($_POST['azione_da']);
		
		$cc2 .= $conoscenza.";";

		if($_POST['tipo'] == 'preventivo' || $_POST['tipo'] == 'tirichiamiamonoi')
		{
			$anno = date('Y');
			$anno = substr($anno, 2,2);
			$sigla = "P";
			$id_thread = $_POST['id_thread'];
			$id_richiesta = $sigla.$anno.$id_thread;
					
			$linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".$_POST['id_customer']."&viewcustomer&tab_name=actions&azione=actions&viewpreventivo&id_thread=".$id_thread."&token=".$tokenimp;
				
			$params4 = array(
				'{reply}' => "<strong>".$employeemess->firstname."</strong> ha inviato un ".$tipo_richiesta_string." a un cliente e ti ha messo in copia conoscenza.
							<br /><br />
							L'ID del ".$tipo_richiesta_string." &egrave;: <strong>".$id_richiesta."</strong><br /><br />
							<a href='".$linkimp."'>Clicca qui per aprire il ".$tipo_richiesta_string."</a>. ",
				'{firma}' => $firma,
				'{id_richiesta}' => "Id richiesta: <strong>".$id_richiesta."</strong>"
			);
			
			$cc1 = Db::getInstance()->getValue('SELECT cc FROM ticket_cc WHERE msg = '.$id_messaggio);
			
			$cc = $cc1.';'.$cc2;
			Db::getInstance()->execute("REPLACE INTO ticket_cc (msg, type, cc) VALUES (".$id_messaggio.", 'p', '".$cc."')");
			
			// correggere 'msg_base' con 'senzagrafica' e il secondo NULL con 'nonrispondere@ezdirect.it' finite le prove
			Mail::Send((int)$context->language->id, 'msg_base', Mail::l(ucfirst($tipo_richiesta_string).' utente in copia conoscenza', (int)$context->language->id), 
			$params4, $mailconoscenza, NULL, NULL, 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true);
		}
	}

	die('ok');
}

// Vai e chiudi azione corrente
if (Tools::isSubmit('closethis'))
{
	switch($_POST['tipo'])
	{
		case 'T': 
			$table = _DB_PREFIX_.'customer_thread';
			$id = 'id_customer_thread';
			break;
		case 'P':
			$table = 'form_prevendita_thread';
			$id = 'id_thread';
			break;
		case 'A':
			$table = 'action_thread';
			$id = 'id_action';
			break;
		case 'L':
			$table = _DB_PREFIX_.'bdl';
			$id = 'id_bdl';
			break;
		default:
			$table = '';
			$id = '';
			break;
	}
	
	if(Db::getInstance()->execute('UPDATE '.$table.' SET status = "closed" WHERE '.$id.' = '.$_POST['id']))
		die('ok');
	else
		die('error');
}

/* Ordini */

if(isset($_GET['getOrderPDF'])) 
{
	ob_clean();

	require_once('../classes/html2pdf/html2pdf.class.php');
	$order = new Order(Tools::getValue('id_order'));
	
	$content = Order::getOrderPDF($order->id, $order->id_customer);

	$html2pdf = new HTML2PDF('P','A4','it');
	$html2pdf->WriteHTML($content);

	$html2pdf->Output('ordine-n-'.$order->id.'.pdf', 'D'); 
}

if(isset($_GET['getDDT'])) 
{
	ob_clean();

	$order = new Order(Tools::getValue('id_order'));
	Fattura::getFattura($order->id, $order->id_customer, 'DDT');
}

if(isset($_GET['getFF'])) 
{
	ob_clean();

	$order = new Order(Tools::getValue('id_order'));
	Fattura::getFattura($order->id, $order->id_customer, 'ff');
}

// correggere: si può togliere controllo $id_customer?
if (Tools::isSubmit('submitRifOrdine') && $id_customer = (int)Tools::getValue('id_customer'))
{
	$rif_ordine = html_entity_decode(Tools::getValue('rif_ordine'));
	$id_order = Tools::getValue('id_order');
	$id_cart = Tools::getValue('id_cart');
	
	Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'orders SET `rif_ordine` = "'.pSQL($rif_ordine, true).'" WHERE id_customer = '.(int)$id_customer.' AND id_order = '.(int)$id_order.'');
	Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'cart SET `rif_ordine` = "'.pSQL($rif_ordine, true).'" WHERE id_customer = '.(int)$id_customer.' AND id_cart = '.(int)$id_cart.'');
	Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'carrelli_creati SET `rif_ordine` = "'.pSQL($rif_ordine, true).'" WHERE id_customer = '.(int)$id_customer.' AND id_cart = '.(int)$id_cart.'');
		
	die('ok');
}

if (Tools::isSubmit('submitDatiPA'))
{
	$cig = html_entity_decode(Tools::getValue('cig'));
	$cup = html_entity_decode(Tools::getValue('cup'));
	$ipa = html_entity_decode(Tools::getValue('ipa'));
	$data_ordine_mepa = Tools::getValue('data_ordine_mepa');
	$id_order = Tools::getValue('id_order');
	
	Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'orders SET `cig` = "'.pSQL($cig, true).'", `cup` = "'.pSQL($cup, true).'", `ipa` = "'.pSQL($ipa, true).'", `data_ordine_mepa` = "'.$data_ordine_mepa.'" WHERE id_order = '.(int)$id_order.'');
	
	$dati_pa = Db::getInstance()->getRow('SELECT cig, cup, ipa, data_ordine_mepa FROM '._DB_PREFIX_.'orders WHERE id_order = '.$id_order);

	die(json_encode($dati_pa));
}

// correggere: si può togliere controllo $id_customer?
if (Tools::isSubmit('submitNoFeedback') AND $id_customer = (int)Tools::getValue('id_customer') AND $id_order = (int)Tools::getValue('id_order'))
{
	$no_feedback = Tools::getValue('no_feedback');
	$no_feedback_cause = html_entity_decode(Tools::getValue('no_feedback_cause'));
	
	if (!Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'orders SET `no_feedback` = "'.pSQL($no_feedback, true).'", `no_feedback_cause` = "'.pSQL($no_feedback_cause, true).'" WHERE id_customer = '.(int)$id_customer.' AND id_order = '.(int)$id_order.'')) {
		die('error:update');
	}
	else {
		Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'orders SET `no_feedback` = "'.pSQL($no_feedback, true).'", `no_feedback_cause` = "'.pSQL($no_feedback_cause, true).'" WHERE id_customer = '.(int)$id_customer.' AND id_order = '.(int)$id_order.'');
		die('ok');
	}
}

// correggere: si può togliere controllo $id_customer?
if (Tools::isSubmit('submitOrdineAreaCarrello') AND $id_customer = (int)Tools::getValue('id_customer') AND $id_cart = (int)Tools::getValue('id_cart'))
{
	$note = html_entity_decode(Tools::getValue('note'));
	$notePrivate = html_entity_decode(Tools::getValue('notePrivate'));
	$esigenze = html_entity_decode(Tools::getValue('esigenze'));
	$risorse = html_entity_decode(Tools::getValue('risorse'));
	$premessa = html_entity_decode(Tools::getValue('premessa'));
	
	if (!empty($note) AND !Validate::isCleanHtml($note))
		die ('error:validation');
	if (!Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'cart SET `note` = "'.pSQL($note, true).'", `note_private` = "'.pSQL($notePrivate, true).'",`esigenze` = "'.pSQL($esigenze, true).'",`premessa` = "'.pSQL($premessa, true).'",`risorse` = "'.pSQL($risorse, true).'" WHERE id_customer = '.(int)$id_customer.' AND id_cart = '.(int)$id_cart.'')) {
		die ('error:update');
	}
	else {
		Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'carrelli_creati SET `note` = "'.pSQL($note, true).'", `note_private` = "'.pSQL($notePrivate, true).'",`premessa` = "'.pSQL($premessa, true).'",`esigenze` = "'.pSQL($esigenze, true).'",`risorse` = "'.pSQL($risorse, true).'" WHERE id_customer = '.(int)$id_customer.' AND id_cart = '.(int)$id_cart.'');
		die('ok');
	}
}

// CORREGGERE E TESTARE!
if (Tools::isSubmit('submitShippingNumber_ajax'))
{	
	$order = new Order(Tools::getValue('id_order'));
	$id_order = $order->id;
	
	$shipping_number = pSQL(Tools::getValue('shipping_number'));
	
	if($order->shipping_number == $shipping_number)
		die('NO');
	
	$order->shipping_number = $shipping_number;
	
	$order->update();
	
	if($shipping_number)
	{
		global $_LANGMAIL;
		$customer = new Customer((int)($order->id_customer));
		$carrier = new Carrier((int)($order->id_carrier));
		if (!Validate::isLoadedObject($customer) OR !Validate::isLoadedObject($carrier))
			die(Tools::displayError());
		$templateVars = array(
			'{followup}' => str_replace('@', $order->shipping_number, $carrier->url),
			'{firstname}' => $customer->firstname,
			'{id_collo}' =>$order->shipping_number,
			'{lastname}' => $customer->lastname,
			'{id_order}' => (int)($order->id)
		);
		
		$id_country = Db::getInstance()->getValue("SELECT id_country FROM address WHERE id_address = ".$order->id_address_delivery."");
		
		Db::getInstance()->executeS('UPDATE action_thread SET status = "closed" WHERE subject LIKE "%*** PROMEMORIA ORDINE FATTURATO N. '.$order->id.'%"');
		
		if($order->id_lang == 0)
			$order->id_lang = $context->language->id;
		
		if($customer->order_notifications == 1) {
		}
		else {
	
			if($id_country == 10) {
				@Mail::Send((int)$order->id_lang, 'in_transit', Mail::l('Ordine spedito', (int)$order->id_lang), $templateVars,
				$customer->email, $customer->firstname.' '.$customer->lastname, NULL, NULL, NULL, NULL,
				_PS_MAIL_DIR_, true);
			}
			else
			{
				@Mail::Send((int)$order->id_lang, 'in_transit', Mail::l('Order sent', (int)$order->id_lang), $templateVars,
				$customer->email, $customer->firstname.' '.$customer->lastname, NULL, NULL, NULL, NULL,
				_PS_MAIL_DIR_, true);
				
			}
		}
		
	}
	
	$newOrderStatusId = Tools::getValue('id_order_state');
				
	$id_order_state_attuale = Db::getInstance()->getValue('SELECT id_order_state FROM order_history WHERE id_order = '.$id_order.' ORDER BY id_order_history DESC');
					
	$stato_attuale = Order::eSolverStatus($id_order, $id_order_state_attuale);
	$stato_nuovo = Order::eSolverStatus($id_order, $newOrderStatusId);
	
	if($newOrderStatusId == 4 || $newOrderStatusId == 5 || $newOrderStatusId == 14 || $newOrderStatusId == 15 || $newOrderStatusId == 16 || $newOrderStatusId == 20)
	{
		$history_prec = Db::getInstance()->getValue('SELECT id_order_history FROM order_history WHERE (id_order_state = 4 OR id_order_state = 5  OR id_order_state = 14  OR id_order_state = 15  OR id_order_state = 16  OR id_order_state = 20) AND id_order = '.$order->id);
		
		
		if($modalita_esolver == 0 && (!$history_prec || $history_prec < 0 || $history_prec == ''))
		{
			// SCARICO MAGAZZINO
			
			$prime = Db::getInstance()->getValue('SELECT id_cart FROM order WHERE id_order = '.$order->id.' AND id_cart IN (SELECT id_cart FROM cart WHERE name LIKE "%AMAZON PRIME%")');
			
			if($prime > 0)
			{
				
			}
			else
			{
				$products = $order->getProducts();
				foreach($products as $product)
				{
					$stock = Db::getInstance()->getRow('SELECT quantity, stock_quantity, itancia_quantity, esprinet_quantity, techdata_quantity, intracom_quantity, supplier_quantity FROM product WHERE id_product = '.$product['product_id']);
					
					if($newOrderStatusId != 15)
						$stock_quantity = $stock['stock_quantity'] -= $product['product_quantity'];
					else
					{
						$cons = Db::getInstance()->getValue('SELECT cons FROM order_detail WHERE id_order = '.$order->id.' AND product_id = '.$product['product_id']);
						$cons = str_replace('0_','',$cons);
						$qt_da_sottrarre = $cons;
						$stock_quantity = $stock['stock_quantity'] -= $qt_da_sottrarre;
					}
					
					$qtsito = ($stock['supplier_quantity'] + $stock_quantity + $stock['esprinet_quantity'] + $stock['itancia_quantity'] + $stock['techdata_quantity'] + $stock['intracom_quantity']);
					
					Db::getInstance()->executeS("UPDATE product SET stock_quantity = $stock_quantity, quantity = $qtsito WHERE id_product = ".$product['product_id']."");

				}
			}	
		}
	}	
					
	$history = new OrderHistory();
	$history->id_order = (int)$id_order;
	$history->id_employee = (int)($context->employee->id);
	$history->changeIdOrderState((int)($newOrderStatusId), (int)($id_order));
	$order = new Order((int)$order->id);
	
	$products = $order->getProducts();

	if($newOrderStatusId == 31)
	{
		$trasp_id = Db::getInstance()->getValue('SELECT product_id FROM rimborsi WHERE product_id = "TRASP" AND id_order = '.$order->id);
		if($trasp_id == 0 || $trasp_id == '' || !$trasp_id)
		{
			Db::getInstance()->executeS('INSERT INTO rimborsi (id_order, product_id, product_price, product_quantity) VALUES ("'.$order->id.'", "TRASP", "'.$order->total_shipping.'", "1")');
		}
	}
					
	foreach($products as $product)
	{
		if($newOrderStatusId == 31)
		{
			$product_id = Db::getInstance()->getValue('SELECT product_id FROM rimborsi WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
			if(!$product_id || $product_id <= 0 || $product_id == '')
			{
				
				Db::getInstance()->executeS('INSERT INTO rimborsi (id_order, product_id, product_quantity, product_price) VALUES ("'.$order->id.'", "'.$product['product_id'].'", "'.$product['product_quantity'].'", "'.($product['product_price']-($product['product_price']*$product['reduction_percent']/100)).'")');
			}
		}
	
		if ($newOrderStatusId != 15 && ($newOrderStatusId != 24 && $newOrderStatusId != 25 && $newOrderStatusId != 27 && $newOrderStatusId != 28))
			Db::getInstance()->executeS('UPDATE order_detail SET cons = "" WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
		else
		{
			$cons = Db::getInstance()->getValue('SELECT cons FROM order_detail WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
			if($cons == '' || empty($cons))
			{
				//$rif_fatt = Db::getInstance()->getValue('SELECT rif_vs_ordine FROM fattura WHERE rif_vs_ordine = '.$order->id);
				if($rif_fatt == $order->id && in_array(trim($product['product_reference']), $array_parziali))
					Db::getInstance()->executeS('UPDATE order_detail SET cons = "0_'.Db::getInstance()->getValue('SELECT SUM(qt_ord) FROM fattura WHERE cod_articolo = "'.trim($product['product_reference']).'" AND rif_ordine = '.$order->id.' GROUP BY cod_articolo').'" WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
				else if($rif_fatt == $order->id && !(in_array(trim($product['product_reference']), $array_parziali)))
					Db::getInstance()->executeS('UPDATE order_detail SET cons = "0_0" WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
				else if($rif_fatt != $order->id)
					Db::getInstance()->executeS('UPDATE order_detail SET cons = "0_0" WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
				else
					Db::getInstance()->executeS('UPDATE order_detail SET cons = "0_0" WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
			}	
		}
	}
					
	if($newOrderStatusId == 4)
	{
		
		Db::getInstance()->executeS('UPDATE action_thread SET status = "closed" WHERE subject LIKE "%PROMEMORIA ORDINE CON BONIFICO N. '.$order->id.'%" AND action_to = 7');
		
		$scadenza = Db::getInstance()->getValue('SELECT scadenza FROM cart WHERE id_cart = '.$order->id_cart);
		$cadenza = Db::getInstance()->getValue('SELECT cadenza FROM cart WHERE id_cart = '.$order->id_cart);
		$competenza_dal = Db::getInstance()->getValue('SELECT competenza_dal FROM cart WHERE id_cart = '.$order->id_cart);
		$competenza_al = Db::getInstance()->getValue('SELECT competenza_al FROM cart WHERE id_cart = '.$order->id_cart);
		$id_contratto_t = Db::getInstance()->getValue("SELECT id_contratto FROM contratto_assistenza WHERE rif_ordine = ".$order->id);
		if($cadenza != '' && $id_contratto_t <= 0)
		{
			if($scadenza != '' && strtotime($scadenza) > 0  && $scadenza != '0000-00-00' && $scadenza != '1970-01-01')
			{
				
			}
			else
			{
				$scadenza = date('Y-m-d',strtotime(date("Y-m-d H:i:s", mktime()) . " + 365 day"));
			}
			$tot_teleassistenza_row = Db::getInstance()->getRow("SELECT count(*) AS tot_assistenza, SUM((`product_price` - ((product_price * reduction_percent) / 100))) AS totale FROM order_detail WHERE id_order = ".$order->id." AND (product_reference LIKE '%teleg%') AND (product_reference NOT LIKE '%r1y%')");
			
			$tot_assistenza_row = Db::getInstance()->getRow("SELECT count(*) AS tot_assistenza, SUM((`product_price` - ((product_price * reduction_percent) / 100))) AS totale FROM order_detail WHERE id_order = ".$order->id." AND (product_reference LIKE '%astectop%')");
			
			if($tot_teleassistenza_row['tot_assistenza'] > 0)
				$tipo_contratto = 4;
			else 
				$tipo_contratto = 2;
			
			$dom = explode('-',$scadenza);
			
			$importo = $tot_assistenza_row['totale'] + $tot_teleassistenza_row['totale'];
			
			$data_scadenza = $dom[0].'-'.$dom[1].'-'.$dom[2].' 23:59:59';
			
			$decorrenza = Db::getInstance()->getValue('SELECT decorrenza FROM cart WHERE id_cart = '.$order->id_cart);
			
			if($decorrenza != '' && $decorrenza != '0000-00-00')
			{
				$dom2 = explode('-',$decorrenza);
				$data_decorrenza = $dom2[0].'-'.$dom2[1].'-'.$dom2[2].' 23:59:59';
			}
			else
			{
				$data_decorrenza = date('Y-m-d H:i:s');
			}
			
			$dom3 = explode('-',$competenza_dal);
			$dom4 = explode('-',$competenza_al);
			
			$data_competenza_dal = $dom3[0].'-'.$dom3[1].'-'.$dom3[2].'';
			$data_competenza_al = $dom4[0].'-'.$dom4[1].'-'.$dom4[2].'';
			
			$id_contratto = Db::getInstance()->getValue("SELECT id_contratto FROM contratto_assistenza ORDER BY id_contratto DESC");
			$id_contratto++;
	
			Db::getInstance()->executeS("INSERT INTO contratto_assistenza (id_contratto, id_customer, codice_spring, status, tipo, descrizione, data_registrazione, data_inizio, data_fine, cadenza, competenza_dal, competenza_al, cig, cup, univoco, indirizzo, prezzo, periodicita, pagamento, blocco_amministrativo, rif_fattura, rif_noleggio, rif_ordine, note_private, date_add, date_upd) VALUES (".$id_contratto.", '".$order->id_customer."', '".Db::getInstance()->getValue('SELECT codice_spring FROM customer WHERE id_customer = '.$order->id_customer)."', '0', '".$tipo_contratto."', '', '".$data_decorrenza."', '".$data_decorrenza."', '".$data_scadenza."', '".$cadenza."', '".$data_competenza_dal."', '".$data_competenza_al."', '', '', '', '".$order->id_address_invoice."', '".$importo."', '12', '".Db::getInstance()->getValue('SELECT payment FROM orders WHERE id_order = '.$order->id.'')."', '0', '', '', '".$order->id."', '', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
			
			$products_ctr = Db::getInstance()->executeS('SELECT * FROM order_detail WHERE id_order = '.$order->id);
			
			foreach($products_ctr as $product_ctr)
				Db::getInstance()->executeS("INSERT INTO contratto_assistenza_prodotti (id_contratto, id_product, codice_prodotto, descrizione_prodotto, quantita, prezzo, seriale, note, indirizzo, date_add, date_upd) VALUES ('".$id_contratto."', ".$product_ctr['product_id'].", '".$product_ctr['product_reference']."', '".$product_ctr['product_name']."', '".$product_ctr['product_quantity']."', '".$product_ctr['product_price']."', '', '', '".$order->id_address_invoice."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
			
			$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
			$action_thread++;
			$action_subject = "*** PROMEMORIA - VERIFICARE CONTRATTO N. ".$id_contratto." ";
			
			$id_employee = 14;
			$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').'1');

			Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd, promemoria) VALUES ('$action_thread', 'Attivita', '".$order->id_customer."', '".addslashes($action_subject)."', 0, ".$id_employee.", 'open', '', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."', 1)");   

			Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', '".$order->id_customer."', 99, '".$id_employee."', '".$id_employee."', '', '<strong>*** VERIFICARE CONTRATTO APPENA INSERITO ***</strong><br /><br />Appena inserito <a href=\"https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$order->id_customer."&viewcustomer&id_contratto=".$id_contratto."&token=".$tokenCustomers."&tab-container-1=13\" target=\"_blank\">contratto n. ".$id_contratto."</a>. Verificare', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  

		}
		
	}
	
	if($newOrderStatusId == 16 || $newOrderStatusId == 4)
	{
		$cart_name = Db::getInstance()->getValue('SELECT name FROM cart WHERE id_cart = '.$cart->id);
		if(substr($cart_name,0,18 == 'Rinnovo assistenza'))
		{
			
			$names = explode(' ',$cart_name);
			$id_contratto = $names[2];
			$anno = $names[4];
			$scadenza = date('Y-m-d',strtotime(date("Y-m-d H:i:s", strtotime(Db::getInstance()->getValue('SELECT data_fine FROM contratto_assistenza WHERE id_contratto = '.$id_contratto)) . " + 365 day")));
			Db::getInstance()->executeS('UPDATE contratto_assistenza SET data_fine = "'.$scadenza.'", status = 0 WHERE id_contratto = '.$id_contratto);
			
			$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
			$action_thread++;
			$action_subject = "*** PROMEMORIA - VERIFICARE CONTRATTO N. ".$id_contratto." ";
			
			$id_employee = 14;
			$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').'1');

			Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd, promemoria) VALUES ('$action_thread', 'Attivita', '".$order->id_customer."', '".addslashes($action_subject)."', 0, ".$id_employee.", 'open', '', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."', 1)");   

			Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', '".$order->id_customer."', 99, '".$id_employee."', '".$id_employee."', '', '<strong>*** VERIFICARE CONTRATTO APPENA RINNOVATO ***</strong><br /><br />Appena rinnovato <a href=\"https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$order->id_customer."&viewcustomer&id_contratto=".$id_contratto."&token=".$tokenCustomers."&tab-container-1=13\" target=\"_blank\">contratto n. ".$id_contratto."</a>. Verificare', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
		}
	}

	if($newOrderStatusId == 18)
	{
		Db::getInstance()->executeS('UPDATE action_thread SET status = "closed" WHERE subject LIKE "*** PROMEMORIA ORDINE CON BONIFICO N. '.$order->id.'%"');
		
		$cart_subject = Db::getInstance()->getValue('SELECT name FROM cart WHERE id_cart = '.$order->id_cart);
		
		if(strpos($cart_subject, 'Rinnovo') !== false)
		{
			$params_contratto = explode('anno',$cart_subject);
			
			$anno_contratto = $params_contratto[1];
			$anno_contratto = (int)$anno_contratto;
			$numero_contratto = preg_replace('/[^0-9]/', '', $params_contratto[0]);
			
			$data_scadenza_contratto = Db::getInstance()->getValue('SELECT data_fine FROM contratto_assistenza WHERE id_contratto = '.$numero_contratto);
			
			$data_scadenza_att = explode(' ',$data_scadenza_contratto);
			$data_scadenza_attuale = explode('-',$data_scadenza_att[0]);
			
			$nuova_data_scadenza = ($anno_contratto+1).'-'.$data_scadenza_attuale[1].'-'.$data_scadenza_attuale[2].' 23:59:59';
			
			Db::getInstance()->executeS('UPDATE contratto_assistenza SET status = 0, data_fine = "'.$nuova_data_scadenza.'" WHERE id_contratto = '.$numero_contratto);
		}
		
		$tot_assistenza = Db::getInstance()->getValue("SELECT count( * ) AS tot_assistenza FROM order_detail WHERE (product_reference LIKE '%astec%' OR product_reference LIKE '%inst%') AND id_order = ".$order->id);
		if($tot_assistenza > 0) 
		{
			$first_product = Db::getInstance()->getValue('SELECT product_name FROM order_detail WHERE id_order = '.$order->id);
			$action_subject = "*** ACCETTATO BONIFICO SU ORDINE N. ".$order->id." (".$first_product.")";
			
			$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
			$action_thread++;
		
			Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd) VALUES ('$action_thread', 'Attivita', '".$order->id_customer."', '".addslashes($action_subject)."', ".$context->employee->id.", 5, 'open', 'O".$order->id."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");   

			Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', ".$order->id_customer.", ".$context->employee->id.", 5, 5, '', '".addslashes($action_subject)."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
		}	
	}

	$cart_name = Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'cart WHERE id_cart = '.$order->id_cart);

	if($newOrderStatusId == 4 && (strpos($cart_name,'AMAZON ID') !== false))
	{
		$action_this = Db::getInstance()->getValue("SELECT id_action FROM action_thread WHERE subject LIKE '%*** CHISURA AMAZON SU ORDINE N. ".$order->id." %'");
		if($action_this > 0) {
			// niente
		}
		else
		{
			$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
			$action_thread++;
			
			$action_subject = "*** CHISURA AMAZON SU ORDINE N. ".$order->id." ";
			$incaricato = 14;
			$messaggio_verifica = "Chiudere su Amazon ordine N. ".$order->id." ";
			
			Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd) VALUES ('$action_thread', 'TODO Amazon', '".$order->id_customer."', '".addslashes($action_subject)."', ".$context->employee->id.", ".$incaricato.", 'open', 'O".$order->id."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");   

			Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', ".$order->id_customer.", ".$context->employee->id.", ".$incaricato.", ".$incaricato.", '', '".addslashes($messaggio_verifica)."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
			
			//mando mail
			$tokenbarbara = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."2");
			$linkbarbara = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenbarbara.'';
			$tokenmatteo = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."7");
			$linkmatteo = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenmatteo.'';
			
			$tokenincarico = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders')).$incaricato);
			$linkincarico = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenincarico.'';
			$mail_incarico = Db::getInstance()->getValue('SELECT email FROM employee WHERE id_employee = '.$incaricato);
			
			$nome_incarico =  Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$incaricato);
			
			$testomail = "Verifica tecnica su ordine n. ".$order->id.". <br /><br />
			Messaggio: ".$messaggio_verifica."<br />
			Clicca sul link per entrare nell'ordine:<br />
			Barbara - <a href='".$linkbarbara."'>clicca qui</a><br />
			Matteo - <a href='".$linkmatteo."'>clicca qui</a><br />
			".($incaricato == 2 || $incaricato == 7 ? '' : $nome_incarico." - <a href='".$linkincarico."'>clicca qui</a><br />")."";
		}
	}
	
	if($newOrderStatusId == 24)
	{
		$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
		$action_thread++;
		
		$action_subject = "*** VERIFICA TECNICA SU ORDINE N. ".$order->id." ";
		
		$incaricato = Tools::getValue('impiegati_verifica');
		$messaggio_verifica = Tools::getValue('verifica_msg');
		
		$existing_action = Db::getInstance()->getValue('SELECT id_action FROM action_thread WHERE subject LIKE "%'.$action_subject.'%"');
		$existing_employee = Db::getInstance()->getValue('SELECT action_to FROM action_thread WHERE subject LIKE "%'.$action_subject.'%"');
		
		if($existing_action  > 0)
		{
			$action_thread = $existing_action;
			
			Db::getInstance()->executeS('UPDATE action_thread SET action_to = '.$incaricato.' WHERE id_action = '.$action_thread.'');
			
			Customer::Storico($action_thread, 'A', $context->employee->id, $incaricato);
			
			$mailimp = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".$incaricato."'");
						
			$tokenimp = Tools::getAdminToken("AdminCustomers"."2".$incaricato);
							
			$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewaction&id_action=".Tools::getValue('id_action')."&token=".$tokenimp.'&tab-container-1=10';
			
			$employeemess = new Employee($context->employee->id);

			$msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato l'azione numero ".$action_thread." su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirla</a>.";
							
			if($existing_employee != 0) {
								
				$mailimprev = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".$existing_employee."'");
				$nomeimprev = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = '".$existing_employee."'");
								
				$msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione dell'azione numero ".Tools::getValue('id_action')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect e l'ha passata a ".Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.Tools::getValue('assegnaimpiegatoa').'');
								
				$params = array(
				'{link}' => $linkimp,
				'{firma}' => '',
				'{msg}' => $msgimprev);
				
				if($existing_employee != $incaricato)
				{			
					Mail::Send(5, 'msg_base', Mail::l('Gestione azione revocata', 5), 
						$params, $mailimprev, NULL, NULL, NULL, NULL, NULL, 
					_PS_MAIL_DIR_, true);
				}		
			}
							
			$params = array(
			'{link}' => $linkimp,
			'{firma}' => '',
			'{msg}' => $msgimp);
							
			if($incaricato != $context->employee->id) {
				Mail::Send(5, 'msg_base', Mail::l('Azione assegnata a te su Ezdirect', 5), 
					$params, $mailimp, NULL, NULL, NULL, NULL, NULL, 
					_PS_MAIL_DIR_, true);
			}
		}
		else {
			Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd) VALUES ('$action_thread', 'Attivita', '".$order->id_customer."', '".addslashes($action_subject)."', ".$context->employee->id.", ".$incaricato.", 'open', 'O".$order->id."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");   
		}
		Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', ".$order->id_customer.", ".$context->employee->id.", ".$incaricato.", ".$incaricato.", '', '".addslashes($messaggio_verifica)."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
		
		//mando mail
		$tokenbarbara = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."2");
		$linkbarbara = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenbarbara.'';
		$tokenmatteo = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."7");
		$linkmatteo = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenmatteo.'';
		
		$tokenincarico = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders')).$incaricato);
		$linkincarico = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenincarico.'';
		$mail_incarico = Db::getInstance()->getValue('SELECT email FROM employee WHERE id_employee = '.$incaricato);
		
		$nome_incarico =  Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$incaricato);
		
		$testomail = "Verifica tecnica su ordine n. ".$order->id.". <br /><br />
		Messaggio: ".$messaggio_verifica."<br />
		Clicca sul link per entrare nell'ordine:<br />
		Barbara - <a href='".$linkbarbara."'>clicca qui</a><br />
		Matteo - <a href='".$linkmatteo."'>clicca qui</a><br />
		".($incaricato == 2 || $incaricato == 7 ? '' : $nome_incarico." - <a href='".$linkincarico."'>clicca qui</a><br />")."";
		
		$params = array('{reply}' => $testomail);
		Mail::Send(5, 'action', Mail::l('Ordine con verifica tecnica', 5), $params, "barbara.giorgini@ezdirect.it", NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true);
		Mail::Send(5, 'action', Mail::l('Ordine con verifica tecnica', 5), $params, "matteo.delgiudice@ezdirect.it", NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true);
		if($incaricato != 2 && $incaricato != 7)
			Mail::Send(5, 'action', Mail::l('Ordine con verifica tecnica', 5), $params, $mail_incarico, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true);
	}
	
	if($newOrderStatusId == 25)
	{
		//mando mail
		$tokenbarbara = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."2");
		$linkbarbara = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenbarbara.'';
		$tokenmatteo = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."7");
		$linkmatteo = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenmatteo.'';
		$actionverifica = Db::getInstance()->getValue('SELECT id_action FROM action_thread WHERE subject = "*** VERIFICA TECNICA SU ORDINE N. '.$order->id.' "');
		Db::getInstance()->getValue('UPDATE action_thread SET status = "closed" WHERE id_action = '.$actionverifica); 
		
		$messaggiotodo = Db::getInstance()->getValue('SELECT action_message FROM action_message WHERE id_action = '.$actionverifica.' ORDER BY id_action_message DESC');
		
		$testomail = "Verifica conclusa su ordine n. ".$order->id.". <br /><br />
		Ultimo messaggio del TODO associato alla verifica: ".$messaggiotodo."<br /><br />
		Clicca sul link per entrare nell'ordine:<br />
		Barbara - <a href='".$linkbarbara."'>clicca qui</a><br />
		Matteo - <a href='".$linkmatteo."'>clicca qui</a><br />";
		
		$params = array('{reply}' => $testomail);
		Mail::Send(5, 'action', Mail::l('Verifica conclusa su ordine '.$order->id, 5), $params, "barbara.giorgini@ezdirect.it", NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true);
		Mail::Send(5, 'action', Mail::l('Verifica conclusa su ordine '.$order->id, 5), $params, "matteo.delgiudice@ezdirect.it", NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true);
	}
	
	if($newOrderStatusId == 6)
	{
		$headers  = 'MIME-Version: 1.0' . "\n";
		$headers .= 'Content-Type: text/html' ."\n";
		$headers .= 'From: "Ezdirect" <info@ezdirect.it>' . "\n";

		$dati_cliente_od = Db::getInstance()->getRow('SELECT * FROM customer WHERE id_customer = '.$order->id_customer);

		if($dati_cliente_od['is_company'] == 1)
			$cli_od = $dati_cliente_od['company']. '(PI: '.$dati_cliente_od['vat_number'].')';
		else
			$cli_od = $dati_cliente_od['firstname'].' '.$dati_cliente_od['lastname']. '(CF: '.$dati_cliente_od['tax_code'].')';
		
		$messaggio_annullato = '<strong>Annullato ordine '.$id_order.'.<br /><br />Cliente: '.$cli_od.'<br /> Prodotti contenuti:</strong><br />';
		
		$products = $order->getProducts();
		foreach($products as $product)
			$messaggio_annullato .= $product['product_name'].' ('.$product['product_reference'].') - Qt: '.$product['product_quantity'].'<br />';
			
		$tokenMatteo = Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).'7');
		$tokenBarbara = Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).'2');
		
		$link_matteo = '<br /><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order='.$id_order.'&vieworder&token='.$tokenMatteo.'">Clicca qui per entrare nell\'ordine</a>';
		$link_barbara = '<br /><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order='.$id_order.'&vieworder&token='.$tokenBarbara.'">Clicca qui per entrare nell\'ordine</a>';
		
		//mail('posta@federicogiannini.com','ANNULLATO ORDINE '.$id_order,$messaggio_annullato.$link_barbara,$headers );
		mail('barbara.giorgini@ezdirect.it','ANNULLATO ORDINE '.$id_order,$messaggio_annullato.$link_barbara,$headers );
		mail('matteo.delgiudice@ezdirect.it','ANNULLATO ORDINE '.$id_order,$messaggio_annullato.$link_matteo,$headers );
	}
	
	$carrier = new Carrier((int)($order->id_carrier), (int)($order->id_lang));
	$templateVars = array();
	if ($history->id_order_state == Configuration::get('PS_OS_SHIPPING') AND $order->shipping_number)
		$templateVars = array('{followup}' => str_replace('@', $order->shipping_number, $carrier->url));
	elseif ($history->id_order_state == Configuration::get('PS_OS_CHEQUE'))
		$templateVars = array(
			'{cheque_name}' => (Configuration::get('CHEQUE_NAME') ? Configuration::get('CHEQUE_NAME') : ''),
			'{cheque_address_html}' => (Configuration::get('CHEQUE_ADDRESS') ? nl2br(Configuration::get('CHEQUE_ADDRESS')) : ''));
	elseif ($history->id_order_state == Configuration::get('PS_OS_BANKWIRE'))
		$templateVars = array(
			'{bankwire_owner}' => (Configuration::get('BANK_WIRE_OWNER') ? Configuration::get('BANK_WIRE_OWNER') : ''),
			'{bankwire_details}' => (Configuration::get('BANK_WIRE_DETAILS') ? nl2br(Configuration::get('BANK_WIRE_DETAILS')) : ''),
			'{bankwire_address}' => (Configuration::get('BANK_WIRE_ADDRESS') ? nl2br(Configuration::get('BANK_WIRE_ADDRESS')) : ''));
	
	if($history->addWithemail(true, $templateVars))
	{
		if($stato_attuale != $stato_nuovo)
		{
			if( $newOrderStatusId == 4 || $newOrderStatusId == 5 || $newOrderStatusId == 6 || $newOrderStatusId == 7 || $newOrderStatusId == 8 || $newOrderStatusId == 14 || $newOrderStatusId == 15 || $newOrderStatusId == 16 || $newOrderStatusId == 20 || $newOrderStatusId == 29)
			{
				// niente
			}
			else
			{
				Product::RecuperaCSV($id_order);
			}
		}
	}

	if($newOrderStatusId == 4 || $newOrderStatusId == 5 || $newOrderStatusId == 14 || $newOrderStatusId == 15 || $newOrderStatusId == 16 || $newOrderStatusId == 20)
	{
		$history_prec = Db::getInstance()->getValue('SELECT id_order_history FROM order_history WHERE (id_order_state = 4 OR id_order_state = 5  OR id_order_state = 14  OR id_order_state = 15  OR id_order_state = 16  OR id_order_state = 20) AND id_order = '.$order->id);
		
		// SCARICO MAGAZZINO
		if($modalita_esolver == 0 && (!$history_prec || $history_prec < 0 || $history_prec == ''))
		{
			$prime = Db::getInstance()->getValue('SELECT id_cart FROM orders WHERE id_order = '.$order->id.' AND id_cart IN (SELECT id_cart FROM cart WHERE name LIKE "%AMAZON PRIME%")');
			
			if($prime > 0){
				// niente
			}
			else
			{
				$products = $order->getProducts();
				foreach($products as $product)
				{
					$stock = Db::getInstance()->getRow('SELECT quantity, stock_quantity, itancia_quantity, esprinet_quantity, techdata_quantity, intracom_quantity, supplier_quantity FROM product WHERE id_product = '.$product['product_id']);
					
					if($newOrderStatusId != 15)
						$stock_quantity = $stock['stock_quantity'] -= $product['product_quantity'];
					else
					{
						$cons = Db::getInstance()->getValue('SELECT cons FROM order_detail WHERE id_order = '.$order->id.' AND product_id = '.$product['product_id']);
						$cons = str_replace('0_','',$cons);
						$qt_da_sottrarre = $cons;
						$stock_quantity = $stock['stock_quantity'] -= $qt_da_sottrarre;
					}
					
					$qtsito = ($stock['supplier_quantity'] + $stock_quantity + $stock['esprinet_quantity'] + $stock['itancia_quantity'] + $stock['techdata_quantity'] + $stock['intracom_quantity']);
					
					Db::getInstance()->executeS("UPDATE product SET stock_quantity = $stock_quantity, quantity = $qtsito WHERE id_product = ".$product['product_id']."");
				}
			}	
		}
	}

	die('ok');	
}

/* Checkbox */

// Verificato
if(Tools::isSubmit('updSpringOrder'))
{
	$updSpringOrder = Tools::getValue('updSpringOrder');
	$spring = Db::getInstance()->getValue('SELECT spring FROM '._DB_PREFIX_.'orders WHERE id_order = '.$_POST['id_order']);

	if($updSpringOrder == 'yes')
	{
		$spring = 1;
		$verificato_da = $context->employee->id;
		Db::getInstance()->execute('UPDATE action_thread SET status = "closed" WHERE subject LIKE "%*** ORDINE N. '.$_POST['id_order'].' NON VERIFICATO ***%"');
	}
	else
	{
		$spring = 0;
		$verificato_da = '';
		Db::getInstance()->execute('UPDATE action_thread SET status = "open" WHERE subject LIKE "%*** ORDINE N. '.$_POST['id_order'].' NON VERIFICATO ***%"');
	}

	Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'orders SET spring = "'.$spring.'", verificato_da = "'.$verificato_da.'" WHERE id_order = '.$_POST['id_order']);
	
	die('ok');
}

// P.I.
if (Tools::isSubmit('updCheckPI'))
{
	$updCheckPI = Tools::getValue('updCheckPI');
	$spring = Db::getInstance()->getValue('SELECT check_pi FROM '._DB_PREFIX_.'orders WHERE id_order = '.$_POST['id_order']);

	if($updCheckPI == 'yes')
		$spring = 1;
	else
		$spring = 0;
	
	Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'orders SET check_pi = "'.$spring.'" WHERE id_order = '.$_POST['id_order']);
	
	die('ok');
}

// Pagamento amazon ricevuto
if (Tools::isSubmit('updPagAmzRic'))
{
	$updPagAmzRic = Tools::getValue('updPagAmzRic');
	$spring = Db::getInstance()->getValue('SELECT pag_amz_ric FROM '._DB_PREFIX_.'orders WHERE id_order = '.$_POST['id_order']);

	if($updPagAmzRic == 'yes')
		$spring = 1;
	else
		$spring = 0;
	
	Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'orders SET pag_amz_ric = "'.$spring.'" WHERE id_order = '.$_POST['id_order']);
	
	die('ok');
}

// eSolver
if (Tools::isSubmit('updCheckEsolv'))
{
	$updCheckEsolv = Tools::getValue('updCheckEsolv');
	$spring = Db::getInstance()->getValue('SELECT check_esolver FROM '._DB_PREFIX_.'orders WHERE id_order = '.$_POST['id_order']);

	if($updCheckEsolv == 'yes')
		$spring = 1;
	else
		$spring = 0;

	Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'orders SET check_esolver = "'.$spring.'" WHERE id_order = '.$_POST['id_order']);

	die('ok');
}

// Ricezione contratto ezcloud
if (Tools::isSubmit('salvaRicezioneContratto'))
{
	$id_cart = Tools::getValue('id_cart');
	$id_employee = Tools::getValue('id_employee');
	$check_contratti = Tools::getValue('contratti_check');
	
	if($check_contratti == '1')
	{
		$array_check_contratti = array('check' => '1', 'id_employee' => $id_employee, 'data' => date('d/m/Y H:i:s'));
		$array_check_contratti = serialize($array_check_contratti);
		$resp = 'Ricezione contratto confermata da '.Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$id_employee).' in data '.date('d/m/Y H:i:s');
		Db::getInstance()->execute('UPDATE cart_ezcloud SET check_contratti = "'.addslashes($array_check_contratti).'" WHERE id_cart = '.$id_cart);
	}
	else
	{
		$resp = "<strong>Confermare la ricezione dell'allegato firmato dal cliente</strong>";
		Db::getInstance()->execute('UPDATE cart_ezcloud SET check_contratti = "" WHERE id_cart = '.$id_cart);
	}
	
	die($resp);
}

/* Fine ordini */


/** \/ OVERRIDE NON TESTATI \/ **/

if(isset($_GET['openAllegato']))
{
	$filename = $_GET['filename'];
	
	if(strpos($filename, ":::")) {
		$parti = explode(":::", $filename);
		$nomecodificato = $parti[0];
		$nomevero = $parti[1];
	}
	else {
		$nomecodificato = $filename;
		$nomevero = $filename;
	}
	
	$extensions = array('.txt' => 'text/plain', '.rtf' => 'application/rtf', '.doc' => 'application/msword', '.docx'=> 'application/msword',
	'.pdf' => 'application/pdf', '.zip' => 'multipart/x-zip', '.png' => 'image/png', '.jpeg' => 'image/jpeg', '.gif' => 'image/gif', '.jpg' => 'image/jpeg');

	$extension = '';
	foreach ($extensions AS $key => $val)
		if (substr($nomevero, -4) == $key OR substr($nomevero, -5) == $key) {
			$extension = $val;
			break;
		}
	
	ob_end_clean();
	header('Content-Type: '.$extension);
	header('Content-Disposition:attachment;filename="'.$nomevero.'"');
	readfile(_PS_UPLOAD_DIR_.$nomecodificato);
	die;
}

// Dashboard - Statistiche in breve - Target anno
if (Tools::isSubmit('getManufacturerQuarter'))
{
	$manufacturer = $_POST['id_manufacturer'];
	$costruttori = explode(";",$_POST['costruttori']);
	$home_manufacturers = array();
	
	foreach($costruttori as $cm) {
		if($cm != '')
			$home_manufacturers[] = $cm;
	}	

	$home_manufacturers = serialize($home_manufacturers);
	
	$home_manufacturers_id = Db::getInstance()->getValue('SELECT id_ck FROM fixed_ck WHERE id_employee = '.$context->employee->id.' AND name = "home_manufacturers"');
	if($home_manufacturers_id > 0)
		Db::getInstance()->execute('UPDATE fixed_ck SET value = "'.addslashes($home_manufacturers).'" WHERE id_employee = '.$context->employee->id.' AND name = "home_manufacturers"');
	else
		Db::getInstance()->execute('INSERT INTO fixed_ck VALUES (NULL, '.$context->employee->id.', "home_manufacturers", "'.addslashes($home_manufacturers).'")');
	
	$vendite_anno = Db::getInstance()->getValue('
		SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) as total
		FROM '._DB_PREFIX_.'orders o 
		JOIN '._DB_PREFIX_.'customer c ON o.id_customer = c.id_customer 
		LEFT JOIN '._DB_PREFIX_.'order_history oh ON oh.id_order_state = 6 AND oh.id_order = o.id_order
		LEFT JOIN '._DB_PREFIX_.'order_detail od ON o.id_order = od.id_order 
		WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire"  OR o.module = "other_payment"))) 
			AND oh.id_order_state IS NULL 
			AND od.manufacturer_id = '.$manufacturer.' 
			AND o.`date_add` BETWEEN "'.date('Y-01-01').' 00:00:00" AND "'.date('Y-12-31').' 23:59:59"
	');
	
	if($manufacturer == 105) {
		$tot_bdl_anno = Db::getInstance()->getValue('
			SELECT SUM(importo) FROM '._DB_PREFIX_.'bdl 
			WHERE importo > 0 
				AND invio_contabilita > 0 
				AND rif_ordine = 0 
				AND date_add BETWEEN "'.date('Y-01-01').' 00:00:00" AND "'.date('Y-12-31').' 23:59:59"
		');

		$vendite_anno += $tot_bdl_anno;
	}

	$target_anno = Db::getInstance()->getValue('SELECT configurazione FROM trgt WHERE descrizione = "m-'.$manufacturer.'-'.date('Y').'" AND anno = "'.date('Y').'"');
	$target_anno = unserialize($target_anno);
	
	$target_percent = ($vendite_anno*100) / $target_anno['totale'];

	die(number_format($target_percent, 2, ",", ""));
}

// Dashboard - Statistiche in breve - Target anno
if (Tools::isSubmit('removeManufacturerFromCookie'))
{
	$costruttori = explode(";",$_POST['costruttori']);
	$home_manufacturers = array();
	
	foreach($costruttori as $cm) {
		if($cm != '')
			$home_manufacturers[] = $cm;
	}	

	$home_manufacturers = serialize($home_manufacturers);
	$home_manufacturers_id = Db::getInstance()->getValue('SELECT id_ck FROM fixed_ck WHERE id_employee = '.$context->employee->id.' AND name = "home_manufacturers"');
	
	if($home_manufacturers_id > 0)
		Db::getInstance()->execute('UPDATE fixed_ck SET value = "'.addslashes($home_manufacturers).'" WHERE id_employee = '.$context->employee->id.' AND name = "home_manufacturers"');
	else
		Db::getInstance()->execute('INSERT INTO fixed_ck VALUES (NULL, '.$context->employee->id.', "home_manufacturers", "'.addslashes($home_manufacturers).'")');
}

/** \/ OVERRIDE DI FEDERICO \/ */

if (Tools::isSubmit('submitOre'))
{
	$dati_stringa = Tools::getValue('dati_da_salvare');
	$dati_stringa = preg_replace('/undefined/', '0', $dati_stringa);
	$dati = explode(':::',$dati_stringa);

	foreach($dati as $dato_stringa)
	{
		$dato = explode('|',$dato_stringa);
		
		$id_employee = $dato[0];
		$month = $dato[1];
		$year = $dato[2];
		$exists = Db::getInstance()->getValue('SELECT count(valori_ore) FROM ore WHERE id_employee = '.$id_employee.' AND month = '.$month.' AND year = '.$year.'');
		if($exists > 0)
			Db::getInstance()->execute('UPDATE ore SET valori_ore = "'.$dato[3].'" WHERE id_employee = '.$id_employee.' AND month = '.$month.' AND year = '.$year.'');
		else
			Db::getInstance()->execute('INSERT INTO ore (id_employee, month, year, valori_ore) VALUES ('.$id_employee.', '.$month.', '.$year.', "'.$dato[3].'")');
	}
	
	die('ok');
}

if(Tools::getIsset('esporta_ore_excel')) 
{
	ini_set("memory_limit","892M");
	set_time_limit(3600);
	require_once 'esportazione-catalogo/Classes/PHPExcel.php';
	$objPHPExcel = new PHPExcel();
				
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', Tools::getValue('month_name_ore_xls').' '.Tools::getValue('year_ore_xls'));
	$employees = Db::getInstance()->ExecuteS('
		SELECT `id_employee`, CONCAT(`firstname`, \' \', `lastname`) AS "name", lastname, firstname
		FROM `'._DB_PREFIX_.'employee`
		WHERE id_employee != 6 AND id_employee != 11 AND id_employee != 15  AND id_employee != 16 AND   id_employee != 20 AND id_employee != 3 AND id_employee != 18  AND id_employee != 21 AND id_employee != 23 AND id_employee != 25
		ORDER BY (CASE id_employee WHEN 1 THEN 0 WHEN 2 THEN 1 WHEN 7 THEN 2 WHEN 4 THEN 3 WHEN 3 THEN 4 WHEN 5 THEN 6 WHEN 13 THEN 7 WHEN 12 THEN 8 WHEN 14 THEN 9 WHEN 17 THEN 10 WHEN 18 THEN 11 WHEN 19 THEN 12 WHEN 22 THEN 13 WHEN 24 THEN 14 WHEN 26 THEN 15 END)');
	
	$num = 2;			
	
	$days_number = cal_days_in_month(CAL_GREGORIAN, Tools::getValue('month_ore_xls'), Tools::getValue('year_ore_xls'));
	$last_letter = '';
	
	if($days_number == 28)
	{
		$last_letter = 'AC';
		$columns = createColumnsArray($last_letter);
	}	
	else if($days_number == 29)
	{
		$last_letter = 'AD';
		$columns = createColumnsArray($last_letter);
	}
	else if($days_number == 30)
	{
		$last_letter = 'AE';
		$columns = createColumnsArray($last_letter);	
	}
	else 
	{
		$last_letter = 'AF';
		$columns = createColumnsArray($last_letter);	
	}
	foreach($employees as $employee)
	{
		if($employee['id_employee'] == 6 || (Tools::getValue('month_ore_xls') > 9 && Tools::getValue('year_ore_xls') >= 2018 && ($employee['id_employee'] == 3 || $employee['id_employee'] == 18)))
		{
		}
		else
		{
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue("A$num", $employee['lastname'].' '.$employee['firstname']);
			
			$valori_ore = Db::getInstance()->getValue('SELECT valori_ore FROM ore WHERE id_employee = '.$employee['id_employee'].' AND month = '.Tools::getValue('month_ore_xls').' AND year = '.Tools::getValue('year_ore_xls').'');
							
			$valori = explode("*",$valori_ore);
			$valori_i = 0;
			foreach(array_slice($columns, 1) as $column)
			{
				if($valori[$valori_i] == 0 && is_numeric($valori[$valori_i]))
					$valori[$valori_i] = '';
				
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue($column.$num, $valori[$valori_i]);
				$objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth(5);
				
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue($column."1", $valori_i+1);
				
				$objPHPExcel->getActiveSheet()->getStyle($column.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
				$valori_i++;
			}
		
			$num++;
		}
	}
				
	$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(30);
	
	$objPHPExcel->getActiveSheet()->getStyle("A")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				
	$objPHPExcel->getActiveSheet()->setTitle('Ore Ezdirect '.Tools::getValue('month_name_ore_xls').' '.Tools::getValue('year_ore_xls'));

	$objPHPExcel->getActiveSheet()->getStyle('A1:'.$last_letter.'1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	

	$objPHPExcel->getActiveSheet()->getStyle('A1:'.$last_letter.'1')->getFill()->getStartColor()->setRGB('FFFF00');	

	$objPHPExcel->getActiveSheet()->getStyle("A1:".$last_letter."$num")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				
	$objPHPExcel->getActiveSheet()->getStyle('A1:'.$last_letter.'1')->getFont()->setBold(true);
				
	$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
	$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
	PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

	$data = date("Ymd");

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->setPreCalculateFormulas(false);
			
	$filename = 'ore-ezdirect-'.Tools::getValue('month_name_ore_xls').'-'.Tools::getValue('year_ore_xls').'.xls';
	ob_end_clean();
	header('Content-type: application/ms-excel');
	header('Content-Disposition: attachment; filename='.$filename);
	$objWriter->save('php://output');
	return true;	
}