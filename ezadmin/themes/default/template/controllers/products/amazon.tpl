{if isset($product->id)}
<div id="product-amazon" class="panel product-tab">
	<input type="hidden" name="submitted_tabs[]" value="Amazon" />
	<h3>{l s='PROVA'}</h3>
</div>
<div class="panel">
	<h3>{l s='Supplier reference(s)'}</h3>
	<div class="alert alert-info">
		{if $associated_suppliers|@count == 0}
			{l s='You must specify the suppliers associated with this product. You must also select the default product supplier before setting references.'}
		{else}
			{l s='You can specify product reference(s) for each associated supplier.'}
		{/if}
		{l s='Click "Save and Stay" after changing selected suppliers to display the associated product references.'}
	</div>
	<div class="panel-footer">
		<a href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}{if isset($smarty.request.page) && $smarty.request.page > 1}&amp;submitFilterproduct={$smarty.request.page|intval}{/if}" class="btn btn-default"><i class="process-icon-cancel"></i> {l s='Cancel'}</a>
		<button type="submit" name="submitAddproduct" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> {l s='Save'}</button>
		<button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> {l s='Save and stay'}</button>
	</div>
</div>
{/if}
