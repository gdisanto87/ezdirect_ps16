{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $check_product_association_ajax}
	{assign var=class_input_ajax value='check_product_name '}
{else}
	{assign var=class_input_ajax value=''}
{/if}

<div id="product-informations" class="panel product-tab">
	<input type="hidden" name="submitted_tabs[]" value="Informations" />
	<h3 class="tab"> <i class="icon-info"></i> {l s='Information'}</h3>
	<script type="text/javascript">

		var msg_select_one = "{l s='Please select at least one product.' js=1}";
		var msg_set_quantity = "{l s='Please set a quantity to add a product.' js=1}";

		{if isset($ps_force_friendly_product) && $ps_force_friendly_product}
			var ps_force_friendly_product = 1;
		{else}
			var ps_force_friendly_product = 0;
		{/if}
		{if isset($PS_ALLOW_ACCENTED_CHARS_URL) && $PS_ALLOW_ACCENTED_CHARS_URL}
			var PS_ALLOW_ACCENTED_CHARS_URL = 1;
		{else}
			var PS_ALLOW_ACCENTED_CHARS_URL = 0;
		{/if}
		{$combinationImagesJs}
		{if $check_product_association_ajax}
				var search_term = '';
				$('document').ready( function() {
					$(".check_product_name")
						.autocomplete(
							'{$link->getAdminLink('AdminProducts', true)|addslashes}', {
								minChars: 3,
								max: 10,
								width: $(".check_product_name").width(),
								selectFirst: false,
								scroll: false,
								dataType: "json",
								formatItem: function(data, i, max, value, term) {
									search_term = term;
									// adding the little
									if ($('.ac_results').find('.separation').length == 0)
										$('.ac_results').css('background-color', '#EFEFEF')
											.prepend('<div style="color:#585A69; padding:2px 5px">{l s='Use a product from the list'}<div class="separation"></div></div>');
									return value;
								},
								parse: function(data) {
									var mytab = new Array();
									for (var i = 0; i < data.length; i++)
										mytab[mytab.length] = { data: data[i], value: data[i].name };
									return mytab;
								},
								extraParams: {
									ajax: 1,
									action: 'checkProductName',
									id_lang: {$id_lang}
								}
							}
						)
						.result(function(event, data, formatted) {
							// keep the searched term in the input
							$('#name_{$id_lang}').val(search_term);
							jConfirm('{l s='Do you want to use this product?'}&nbsp;<strong>'+data.name+'</strong>', '{l s='Confirmation'}', function(confirm){
								if (confirm == true)
									document.location.href = '{$link->getAdminLink('AdminProducts', true)}&updateproduct&id_product='+data.id_product;
								else
									return false;
							});
						});
				});
		{/if}
	</script>

	{if isset($display_common_field) && $display_common_field}
	<div class="alert alert-warning" style="display: block">{l s='Warning, if you change the value of fields with an orange bullet %s, the value will be changed for all other shops for this product' sprintf=$bullet_common_field}</div>
	{/if}

	{include file="controllers/products/multishop/check_fields.tpl" product_tab="Informations"}

	<div class="form-group">
		<label class="control-label col-lg-1" for="simple_product">
			{$bullet_common_field} {l s='Type'}
		</label>
		<div class="col-lg-3">
			<div class="radio">
				<label for="simple_product">
					<input type="radio" name="type_product" id="simple_product" value="{Product::PTYPE_SIMPLE}" {if $product_type == Product::PTYPE_SIMPLE}checked="checked"{/if} >
					{l s='Standard product'}</label>
			</div>
			<div class="radio">
				<label for="pack_product">
					<input type="radio" name="type_product" {if $is_in_pack}disabled="disabled"{/if} id="pack_product" value="{Product::PTYPE_PACK}" {if $product_type == Product::PTYPE_PACK}checked="checked"{/if} > {l s='Pack of existing products'}</label>
			</div>
			<div class="radio">
				<label for="virtual_product">
					<input type="radio" name="type_product" id="virtual_product" {if $is_in_pack}disabled="disabled"{/if} value="{Product::PTYPE_VIRTUAL}" {if $product_type == Product::PTYPE_VIRTUAL}checked="checked"{/if} >
					{l s='Virtual product (services, booking, downloadable products, etc.)'}</label> {* Prodotto scaricabile *}
			</div>
			<div class="row row-padding-top">
				<div id="warn_virtual_combinations" class="alert alert-warning" style="display:none">{l s='You cannot use combinations with a virtual product.'}</div>
				<div id="warn_pack_combinations" class="alert alert-warning" style="display:none">{l s='You cannot use combinations with a pack.'}</div>
			</div>
		</div>

		<span>{include file="controllers/products/multishop/checkbox.tpl" field="visibility" type="default"}</span>
		<label class="control-label col-lg-2" for="visibility">
			{l s='Visibility'}
		</label>
		<div class="col-lg-2">
			<select name="visibility" id="visibility">
				<option value="both" {if $product->visibility == 'both'}selected="selected"{/if} >{l s='Everywhere'}</option>
				<option value="catalog" {if $product->visibility == 'catalog'}selected="selected"{/if} >{l s='Catalog only'}</option>
				<option value="search" {if $product->visibility == 'search'}selected="selected"{/if} >{l s='Search only'}</option>
				<option value="none" {if $product->visibility == 'none'}selected="selected"{/if}>{l s='Nowhere'}</option>
			</select>
		</div>

	</div>

	<div id="product-pack-container" {if $product_type != Product::PTYPE_PACK}style="display:none"{/if}></div>

	<hr />

	{if isset($avviso_doppio)}
		<div class="col-lg-12">
			<div class="row">
				<div class="alert alert-danger">{$avviso_doppio}</div> {* Avviso campi univoci duplicati in un altro prodotto *}
			</div>
		</div>
	{/if}

	{* OVERRIDE *}
	<div class="row">
		{* PANEL A SINISTRA *}
		<div class="col-lg-6">
			<div id="product-informations-left" class="panel product-tab">
				<input type="hidden" name="submitted_tabs[]" value="informations_left" />

				<div class="form-group">
					<label class="control-label col-lg-3 required" for="name_{$id_lang}">
						<span class="label-tooltip" data-toggle="tooltip" title="{l s='The public name for this product.'} {l s='Invalid characters:'} &lt;&gt;;=#{}{l s='Keep within 55 characters'}">
							{l s='Name'}
						</span>
					</label>
					<div class="col-lg-9">
						{include file="controllers/products/input_text_lang.tpl"
							languages=$languages
							input_class="{$class_input_ajax}{if !$product->id || Configuration::get('PS_FORCE_FRIENDLY_PRODUCT')}copy2friendlyUrl{/if} updateCurrentText"
							input_value=$product->name
							input_name="name"
							required=true
						}
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-3 " for="reference">
						<span>
							Codice eSolver
						</span>
					</label>
					<div class="col-lg-5">
						<input required class="textbox-fix" type="text" id="reference" name="reference" value="{$product->reference|htmlentitiesUTF8}">
					</div>
					<div class="col-md-2">
						<input type="button" value="Verifica" name="verifica_esolver" onclick='$("#checkid").html("Attendi..."); $.get("checkreference.php",{ cmd: "checkref", id_product: "{$product->id}", reference: $("#reference").val() } ,function(data){  $("#checkid").html(data); });'>
						<span style="font: bold 12px;" id="checkid"></span> 
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-3 " for="supplier_reference">
						<span>
							Codice SKU
						</span>
					</label>
					<div class="col-lg-5">
						<input class="textbox-fix" type="text" id="supplier_reference" name="supplier_reference" value="{$product->supplier_reference}">
					</div>
					<div class="col-md-2">
						<input type="button" value="Verifica" name="verifica_sku" onclick='$("#checkidsup").html("Attendi..."); $.get("checkreference.php",{ cmd: "checksupref", id_product: "{$product->id}", supplier_reference: $("#supplier_reference").val() } ,function(data){  $("#checkidsup").html(data); });'>
						<span style="font: bold 12px;" id="checkidsup"></span> 
					</div>
					<div class="col-md-2">
						( <input type="checkbox" {if $product->reference == '' } checked="checked" {/if} name="sku_provvisorio"> Prov. )
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-3 " for="ean13">
						<span class="label-tooltip" data-toggle="tooltip"
							title="{l s='This type of product code is specific to Europe and Japan, but is widely used internationally. It is a superset of the UPC code: all products marked with an EAN will be accepted in North America.'}">
							{$bullet_common_field} {l s='EAN-13 or JAN barcode'}
						</span>
					</label>
					<div class="col-lg-5">
						<input class="textbox-fix" maxlength="13" type="text" id="ean13" name="ean13" value="{$product->ean13|htmlentitiesUTF8}">
					</div>
					<div class="col-md-4">
						Fuori produzione? <input type="checkbox" id="fuori_produzione" name="fuori_produzione" value="1" {if $product->fuori_produzione} checked="checked" {/if}>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3" for="upc">
						<span>
							Rif. fornitore 1
						</span>
					</label>
					<div class="col-md-3">
						<input class="textbox-fix" type="text" id="upc" name="upc" value="{$product->upc|escape:'html':'UTF-8'}">
					</div>
					<label class="control-label col-md-3" for="rif_fornitore_2">
						<span>
							Rif. fornitore 2
						</span>
					</label>
					<div class="col-md-3">
						<input class="textbox-fix" type="text" id="rif_fornitore_2" name="rif_fornitore_2" value="{$product->rif_fornitore_2|escape:'html':'UTF-8'}">
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-lg-3" for="asin">
						<span>
							ASIN
						</span>
					</label>
					<div class="col-lg-5">
						<input class="textbox-fix" type="text" id="asin" name="asin" value="{$product->asin}">
					</div>
					<div class="col-md-4">
						Trasp. gratis Amazon.it? 
						<input type="checkbox" id="amazon_free_shipping" name="amazon_free_shipping" value="1" {if $product->amazon_free_shipping} checked="checked" {/if}>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-3">
						<span>
							Dimensioni (cm)
						</span>
					</label>
					<div class="col-sm-2">
						<input class="textbox-fix" type="text" id="width" name="width" value="{$product->width}" onkeyup="if (isArrowKey(event)) return; this.value = this.value.replace(/,/g, '.');">
					</div>
					<div class="col-sm-1" style="padding-top: 5px;">
						Larg.
					</div>
					<div class="col-sm-2">
						<input class="textbox-fix" type="text" id="height" name="height" value="{$product->height}" onkeyup="if (isArrowKey(event)) return; this.value = this.value.replace(/,/g, '.');">
					</div>
					<div class="col-sm-1" style="padding-top: 5px;">
						Lung.
					</div>
					<div class="col-sm-2">
						<input class="textbox-fix" type="text" id="depth" name="depth" value="{$product->depth}" onkeyup="if (isArrowKey(event)) return; this.value = this.value.replace(/,/g, '.');">
					</div>
					<div class="col-sm-1" style="padding-top: 5px;">
						Alt.
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-3 " for="weight">
						<span>
							Peso
						</span>
					</label>
					<div class="col-sm-2">
						<input class="textbox-fix" type="text" id="weight" name="weight" value="{$product->weight}" onkeyup="if (isArrowKey(event)) return; this.value = this.value.replace(/,/g, '.');">
					</div>
					<div class="col-sm-1" style="padding-top: 5px;">
						g
					</div>
					<div class="col-sm-4" style="padding-top: 5px;">
						Scorta min. EZ / Amz
					</div>
					<div class="col-sm-2">
						<div class="row">
							<div class="col-sm-5">
								<input class="textbox-fix" type="text" id="scorta_minima" name="scorta_minima" value="{$product->scorta_minima}">
							</div>
							<div class="col-sm-1" style="padding-top: 5px; text-align: center;"> 
								/ 
							</div>
							<div class="col-sm-5">
								<input class="textbox-fix" type="text" id="scorta_minima_amazon" name="scorta_minima_amazon" value="{$product->scorta_minima_amazon}">
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">

					<div class="col-sm-6">
						Servizio con canone ricorrente? <input type="checkbox" id="prodotto_con_canone" name="prodotto_con_canone" value="1" {if $product->prodotto_con_canone} checked="checked" {/if} >
					</div>
					{*<div class="col-sm-4" style="display:none">
						Prodotto scaricabile <input type="checkbox" id="is_virtual_good" name="is_virtual_good">
					</div>*}
				</div>

				{if $canonical}
				<div class="row">
					<div class="col-sm-12">
						<p><strong class="rosso">ATTENZIONE</strong>: questo prodotto è il canonical di altri prodotti. Questi sono i codici dei prodotti:</p>
						{foreach $padrecanonical as $figlio} {* CORREGGERE: a cosa serve id_category in href? Perchè nella 1.4 usava addproduct al posto di update? *}
							<a href="{$link->getAdminLink('AdminProducts')}&updateproduct&id_product={$figlio['id_product']}&id_category={$figlio['id_category_default']}" target="_blank" style="text-decoration:underline">{$figlio['reference']}</a> 
						{/foreach}
						<p><strong>FARE ATTENZIONE se si vuole cancellare questo prodotto, il canonical non esisterebbe più.</p>
					</div>
				</div>
				{/if}

				<div class="row">
					<div class="col-sm-6">
						Data inserimento <span class="thick">{$product->date_add}</span>
					</div>

					<div class="col-sm-6">
						Ultimo agg. <span class="thick">{$product->date_upd}</span>
					</div>
				</div>

			</div>
		</div>
		{* PANEL A DESTRA *}
		<div class="col-lg-6">
			<div id="product-informations-right" class="panel product-tab">
				<input type="hidden" name="submitted_tabs[]" value="informations_right" />

				<div class="form-group">
					<label class="control-label col-sm-3 " for="active">
						<span>
							Status
						</span>
					</label>
					<div class="col-sm-2">
						<input type="radio" name="active" id="active_on" onclick="toggleDraftWarning(false);showOptions(true);" value="1"{if $product->active == 1} checked="checked"{/if}> {l s="Enabled"}
					</div>
					<div class="col-sm-2">
						<input type="radio" name="active" id="active_off" onclick="toggleDraftWarning(true);showOptions(false);" value="0"{if $product->active == 0} checked="checked"{/if}> {l s="Disabled"}
					</div>
					<div class="col-sm-2">
						<input type="radio" name="active" id="active_old" value="2"{if $product->active == 2} checked="checked"{/if}> {l s="Old"}
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-3 " for="condition">
						<span>
							Condizione
						</span>
					</label>
					<div class="col-sm-9">
						<select name="condition" id="condition">
							<option value="new" {if $product->condition == 'new'}selected="selected"{/if} >{l s='New'}</option>
							<option value="used" {if $product->condition == 'used'}selected="selected"{/if} >{l s='Used'}</option>
							<option value="refurbished" {if $product->condition == 'refurbished'}selected="selected"{/if}>{l s='Refurbished'}</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-3 " for="dispo_type">
						<span>
							Tipo disponibilità
						</span>
					</label>
					<div class="col-sm-9">
						<select name="dispo_type" id="dispo_type">							
							<option value="0" {if $product->dispo_type == 0}selected="selected"{/if}>Tutto</option>
							<option value="1" {if $product->dispo_type == 1}selected="selected"{/if}>Solo magazzino EZ</option>
							<option value="2" {if $product->dispo_type == 2}selected="selected"{/if}>Fornitore principale + Mag. EZ</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-3 " for="date_available">
						<span>
							Disponibile da
						</span>
					</label>
					<div class="col-sm-9">
						<input type="date" name="date_available" id="date_available" value="{$product->date_available}">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-3 " for="data_arrivo">
						<span>
							Data arrivo Allnet
						</span>
					</label>
					<div class="col-sm-9">
						<input type="date" name="data_arrivo" id="data_arrivo" value="{$product->data_arrivo}">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-3" for="id_manufacturer">{l s='Manufacturer'}</label>
					<div class="col-sm-6">
						<select name="id_manufacturer" id="id_manufacturer" onchange="/*getSupplierByManufacturer();*/ getMargins()">
							<option value="0">- {l s='Choose (optional)'} -</option>
							{if $product->id_manufacturer}
							<option value="{$product->id_manufacturer}" selected="selected" data-margin-cli="{$data_margin_cli}" data-margin-riv="{$data_margin_riv}" data-supplier="{$data_supplier}" data-othersuppliers="{$data_othersuppliers}">{$product->manufacturer_name}</option>
							{/if}
							<option disabled="disabled">-</option>
						</select>
					</div>
					<div class="col-sm-1">
					</div>
					<div class="col-sm-2">
						<a class="btn btn-link bt-icon confirm_leave" style="margin-bottom:0px" href="{$link->getAdminLink('AdminManufacturers')|escape:'html':'UTF-8'}&amp;addmanufacturer">
							<i class="icon-plus-sign"></i> {l s='Create'} <i class="icon-external-link-sign"></i>
						</a>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-3" for="id_supplier">{l s='Supplier'}</label>
					<div class="col-sm-6">
						<select name="id_supplier" id="id_supplier">
							<option value="0">- {l s='Choose (optional)'} -</option>
							{if $product->id_supplier}
							<option value="{$product->id_supplier}" selected="selected">{$product->supplier_name}</option>
							{/if}
							<option disabled="disabled">-</option>
						</select>
					</div>
					<div class="col-sm-1">
					</div>
					<div class="col-sm-2">
						<a class="btn btn-link bt-icon confirm_leave" style="margin-bottom:0px" href="{$link->getAdminLink('AdminSuppliers')|escape:'html':'UTF-8'}&amp;addsupplier">
							<i class="icon-plus-sign"></i> {l s='Create'} <i class="icon-external-link-sign"></i>
						</a>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-3 " for="altri-fornitori">
						<span>
							Altri fornitori
						</span>
					</label>
					{*<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-6">
								<a onclick="add_riga()" class="puntatore"><i class="icon-plus-sign verde"></i> Aggiungi forn.</a>
							</div>
							<div class="col-sm-6">
								<a onclick="rem_riga()" class="puntatore"><i class="icon-remove-sign rosso"></i> Rimuovi forn.</a>
							</div>
						</div>
					</div>*}
					<div id="altri-fornitori">
						<div class="row">
							<label class="control-label col-sm-3">
							</label>
							<div class="form-group col-sm-9">
								<select id="other_suppliers" name="other_suppliers[]" class="form-control textbox-fix select2mul" multiple="multiple">
									<option value="0">-- Seleziona --</option>
								{foreach $fornitori AS $key => $row}
									<option value="{$row["id_supplier"]}"{if $row["id_supplier"]|in_array:$altri_fornitori} selected{/if}>{$row["name"]}</option>
								{/foreach}
								</select>
							</div>
						</div>
					</div>
				</div>

				{* Tutta la parte commentata relativa a other_suppliers è il vecchio metodo usato nella 1.4 *}
				{*<div id="altri-fornitori">
					{if count($altri_fornitori)}
						{foreach $altri_fornitori AS $row2}
							<div class="row">
								<label class="control-label col-sm-3">
								</label>
								<div class="form-group col-sm-9">
									<select id="other_suppliers" name="other_suppliers[]" class="form-control textbox-fix">
										<option value="0">-- Seleziona --</option>
									{foreach $fornitori AS $key => $row}
										<option value="{$row["id_supplier"]}"{if $row["id_supplier"] == $row2} selected{/if}>{$row["name"]}</option>
									{/foreach}
									</select>
								</div>
							</div>
						{/foreach}
					{/if}
				</div>

				<div style="display:none">
					<select id="other_suppliers_cpy" name="other_suppliers[]" class="form-control textbox-fix">
						<option value="0">-- Seleziona --</option>
					{foreach $fornitori AS $key => $row}
						<option value="{$row["id_supplier"]}">{$row["name"]}</option>
					{/foreach}
					</select>
				</div>*}

				<div class="form-group">
					<label class="control-label col-sm-3 " for="note_fornitore">
						<span>
							Note forn.
						</span>
					</label>
					<div class="col-sm-9">
						<textarea name="note_fornitore" id="note_fornitore">{$product->note_fornitore}</textarea>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	{* FINE OVERRIDE *}

	{*<div class="form-group" style="display:none">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="name" type="default" multilang="true"}</span></div>
		<label class="control-label col-lg-2 required" for="name_{$id_lang}">
			<span class="label-tooltip" data-toggle="tooltip" title="{l s='The public name for this product.'} {l s='Invalid characters:'} &lt;&gt;;=#{}">
				{l s='Name'}
			</span>
		</label>
		<div class="col-lg-5">
			{include file="controllers/products/input_text_lang.tpl"
				languages=$languages
				input_class="{$class_input_ajax}{if !$product->id || Configuration::get('PS_FORCE_FRIENDLY_PRODUCT')}copy2friendlyUrl{/if} updateCurrentText"
				input_value=$product->name
				input_name="name"
				required=true
			}
		</div>
	</div>*}

	{*<div class="form-group" style="display:none">
		<label class="control-label col-lg-3" for="reference">
			<span class="label-tooltip" data-toggle="tooltip"
			title="{l s='Your internal reference code for this product.'} {l s='Allowed special characters:'} .-_#\">
				{$bullet_common_field} {l s='Reference code'}
			</span>
		</label>
		<div class="col-lg-5">
			<input type="text" id="reference" name="reference" value="{$product->reference|htmlentitiesUTF8}" />
		</div>
	</div>*}

	{*<div class="form-group" style="display:none">
		<label class="control-label col-lg-3" for="ean13">
			<span class="label-tooltip" data-toggle="tooltip"
				title="{l s='This type of product code is specific to Europe and Japan, but is widely used internationally. It is a superset of the UPC code: all products marked with an EAN will be accepted in North America.'}">
				{$bullet_common_field} {l s='EAN-13 or JAN barcode'}
			</span>
		</label>
		<div class="col-lg-3">
			<input maxlength="13" type="text" id="ean13" name="ean13" value="{$product->ean13|htmlentitiesUTF8}" />
		</div>
	</div>*}

	{*<div class="form-group" style="display:none">
		<label class="control-label col-lg-3" for="upc">
			<span class="label-tooltip" data-toggle="tooltip"
				title="{l s='This type of product code is widely used in the United States, Canada, the United Kingdom, Australia, New Zealand and in other countries.'}">
				{$bullet_common_field} {l s='UPC barcode'}
			</span>
		</label>
		<div class="col-lg-3">
			<input maxlength="12" type="text" id="upc" name="upc" value="{$product->upc|escape:'html':'UTF-8'}" />
		</div>
	</div>*}

	<hr/>

	<div class="form-group" style="display:none">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="active" type="radio" onclick=""}</span></div>
		<label class="control-label col-lg-2">
			{l s='Enabled'}
		</label>
		<div class="col-lg-9">
			<span class="switch prestashop-switch fixed-width-lg">
				<input onclick="toggleDraftWarning(false);showOptions(true);showRedirectProductOptions(false);" type="radio" name="active_originale" id="active_on_originale" value="1" {if $product->active || !$product->isAssociatedToShop()}checked="checked" {/if} />
				<label for="active_on_originale" class="radioCheck">
					{l s='Yes'}
				</label>
				<input onclick="toggleDraftWarning(true);showOptions(false);showRedirectProductOptions(true);"  type="radio" name="active_originale" id="active_off_originale" value="0" {if !$product->active && $product->isAssociatedToShop()}checked="checked"{/if} />
				<label for="active_off_originale" class="radioCheck">
					{l s='No'}
				</label>
				<a class="slide-button btn"></a>
			</span>
		</div>
	</div>

	<div class="form-group redirect_product_options" style="display:none">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="redirect_type" type="radio" onclick=""}</span></div>
		<label class="control-label col-lg-2" for="redirect_type">
			{l s='Redirect when disabled'}
		</label>
		<div class="col-lg-5">
			<select name="redirect_type" id="redirect_type">
				<option value="404" {if $product->redirect_type == '404'} selected="selected" {/if}>{l s='No redirect (404)'}</option>
				<option value="301" {if $product->redirect_type == '301'} selected="selected" {/if}>{l s='Redirected permanently (301)'}</option>
				<option value="302" {if $product->redirect_type == '302'} selected="selected" {/if}>{l s='Redirected temporarily (302)'}</option>
			</select>
		</div>
	</div>
	<div class="form-group redirect_product_options" style="display:none">
		<div class="col-lg-9 col-lg-offset-3">
			<div class="alert alert-info">
				{l s='404 Not Found = Do not redirect and display a 404 page.'}<br/>
				{l s='301 Moved Permanently = Permanently display another product instead.'}<br/>
				{l s='302 Moved Temporarily = Temporarily display another product instead.'}
			</div>
		</div>
	</div>

	<div class="form-group redirect_product_options redirect_product_options_product_choise" style="display:none">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="id_product_redirected" type="radio" onclick=""}</span></div>
		<label class="control-label col-lg-2" for="related_product_autocomplete_input">
			{l s='Related product:'}
		</label>
		<div class="col-lg-7">
			<input type="hidden" value="" name="id_product_redirected" />

			<div class="input-group">
				<input type="text" id="related_product_autocomplete_input" name="related_product_autocomplete_input" autocomplete="off" class="ac_input" />
				<span class="input-group-addon"><i class="icon-search"></i></span>
			</div>

			<div class="form-control-static">
				<span id="related_product_name"><i class="icon-warning-sign"></i>&nbsp;{l s='No related product.'}</span>
				<span id="related_product_remove" style="display:none">
					<a class="btn btn-default" href="#" onclick="removeRelatedProduct(); return false" id="related_product_remove_link">
						<i class="icon-remove text-danger"></i>
					</a>
				</span>
			</div>

		</div>
		<script type="text/javascript">
			var no_related_product = "{l s='No related product'}";
			var id_product_redirected = {$product->id_product_redirected|intval};
			var product_name_redirected = "{$product_name_redirected|escape:'html':'UTF-8'}";
		</script>
	</div> 

	{* <div class="form-group">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="visibility" type="default"}</span></div>
		<label class="control-label col-lg-2" for="visibility">
			{l s='Visibility'}
		</label>
		<div class="col-lg-3">
			<select name="visibility" id="visibility">
				<option value="both" {if $product->visibility == 'both'}selected="selected"{/if} >{l s='Everywhere'}</option>
				<option value="catalog" {if $product->visibility == 'catalog'}selected="selected"{/if} >{l s='Catalog only'}</option>
				<option value="search" {if $product->visibility == 'search'}selected="selected"{/if} >{l s='Search only'}</option>
				<option value="none" {if $product->visibility == 'none'}selected="selected"{/if}>{l s='Nowhere'}</option>
			</select>
		</div>
	</div> *}

	<div id="product_options" class="form-group" style="display:none">
		<div class="col-lg-12">
			<div class="form-group">
				<div class="col-lg-1">
					<span class="pull-right">
						{if isset($display_multishop_checkboxes) && $display_multishop_checkboxes}
							{include file="controllers/products/multishop/checkbox.tpl" only_checkbox="true" field="available_for_order" type="default"}
							{include file="controllers/products/multishop/checkbox.tpl" only_checkbox="true" field="show_price" type="show_price"}
							{include file="controllers/products/multishop/checkbox.tpl" only_checkbox="true" field="online_only" type="default"}
						{/if}
					</span>
				</div>
				<label class="control-label col-lg-2" for="available_for_order">
					{l s='Options'}
				</label>
				<div class="col-lg-9">
					<div class="checkbox">
						<label for="available_for_order">
							<input type="checkbox" name="available_for_order" id="available_for_order" value="1" {if $product->available_for_order}checked="checked"{/if} >
							{l s='Available for order'}</label>
					</div>
					<div class="checkbox">
						<label for="show_price">
							<input type="checkbox" name="show_price" id="show_price" value="1" {if $product->show_price}checked="checked"{/if} {if $product->available_for_order}disabled="disabled"{/if} >
							{l s='Show price'}</label>
					</div>
					<div class="checkbox">
						<label for="online_only">
							<input type="checkbox" name="online_only" id="online_only" value="1" {if $product->online_only}checked="checked"{/if} >
							{l s='Online only (not sold in your retail store)'}</label>
					</div>
				</div>
			</div>
			{* <div class="form-group">
				<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="condition" type="default"}</span></div>
				<label class="control-label col-lg-2" for="condition">
					{l s='Condition'}
				</label>
				<div class="col-lg-3">
					<select name="condition" id="condition">
						<option value="new" {if $product->condition == 'new'}selected="selected"{/if} >{l s='New'}</option>
						<option value="used" {if $product->condition == 'used'}selected="selected"{/if} >{l s='Used'}</option>
						<option value="refurbished" {if $product->condition == 'refurbished'}selected="selected"{/if}>{l s='Refurbished'}</option>
					</select>
				</div>
			</div> *}
		</div>
	</div>

	{* <hr/> *}
	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="description_short" type="tinymce" multilang="true"}</span></div>
		<label class="control-label col-lg-2" for="description_short_{$id_lang}">
			<span class="label-tooltip" data-toggle="tooltip" title="{l s='Appears in the product list(s), and at the top of the product page.'}">
				{l s='Short description'}
			</span>
		</label>
		<div class="col-lg-9">
			{include
				file="controllers/products/textarea_lang.tpl"
				languages=$languages
				input_name='description_short'
				class="autoload_rte"
				input_value=$product->description_short
				max=$PS_PRODUCT_SHORT_DESC_LIMIT}
		</div>
	</div>

	{* OVERRIDE *}
	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="cat_homepage" type="default" multilang="true"}</span></div>
		<label class="control-label col-lg-2" for="cat_homepage_{$id_lang}">
			<span class="label-tooltip" data-toggle="tooltip"
				title="{l s='Forbidden characters: <>;=#{}'}">
				{l s='Home page category'}
			</span>
		</label>
		<div class="col-lg-9">
			{include file="controllers/products/input_text_lang.tpl"
				languages=$languages 
				input_name='cat_homepage'
				input_value=$product->cat_homepage
				maxchar=100
			}
		</div>
	</div>
	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="desc_homepage" type="default" multilang="true"}</span></div>
		<label class="control-label col-lg-2" for="desc_homepage_{$id_lang}">
			<span class="label-tooltip" data-toggle="tooltip"
				title="{l s='Forbidden characters: <>;=#{}'}">
				{l s='Home page description'}
			</span>
		</label>
		<div class="col-lg-9">
			{include file="controllers/products/textarea_lang.tpl"
				languages=$languages 
				input_name='desc_homepage'
				input_value=$product->desc_homepage
				max=110
			}
		</div>
	</div>
	{* FINE OVERRIDE *}

	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="description" type="tinymce" multilang="true"}</span></div>
		<label class="control-label col-lg-2" for="description_{$id_lang}">
			<span class="label-tooltip" data-toggle="tooltip"
				title="{l s='Appears in the body of the product page.'}">
				{l s='Description'}
			</span>
		</label>
		<div class="col-lg-9">
			{include
				file="controllers/products/textarea_lang.tpl"
				languages=$languages input_name='description'
				class="autoload_rte"
				input_value=$product->description}
		</div>
	</div>
	{if $images}
	<div class="form-group">
		<div class="col-lg-9 col-lg-offset-3">
			<div class="alert alert-info">
				{capture}<a class="addImageDescription" href="javascript:void(0);">{l s='Click here'}</a>{/capture}
				{l s='Would you like to add an image in your description? %s and paste the given tag in the description.' sprintf=$smarty.capture.default}
			</div>
		</div>
	</div>
	<div id="createImageDescription" class="panel" style="display:none">
		<div class="form-group">
			<label class="control-label col-lg-3" for="smallImage_0">{l s='Select your image'}</label>
			<div class="col-lg-9">
				<ul class="list-inline">
					{foreach from=$images item=image key=key}
					<li>
						<input type="radio" name="smallImage" id="smallImage_{$key}" value="{$image.id_image}" {if $key == 0}checked="checked"{/if} >
						<label for="smallImage_{$key}" >
							<img src="{$image.src}" alt="{$image.legend}" />
						</label>
					</li>
					{/foreach}
				</ul>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3" for="leftRight_1">{l s='Position'}</label>
			<div class="col-lg-5">
				<p class="checkbox">
					<input type="radio" name="leftRight" id="leftRight_1" value="left" checked>
					<label for="leftRight_1" >{l s='left'}</label>
				</p>
				<p class="checkbox">
					<input type="radio" name="leftRight" id="leftRight_2" value="right">
					<label for="leftRight_2" >{l s='right'}</label>
				</p>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3" for="imageTypes_0">{l s='Select the type of picture'}</label>
			<div class="col-lg-5">
				{foreach from=$imagesTypes key=key item=type}
				<p class="checkbox">
					<input type="radio" name="imageTypes" id="imageTypes_{$key}" value="{$type.name}" {if $key == 0}checked="checked"{/if}>
					<label for="imageTypes_{$key}" >
						{$type.name} <span>{l s='%dpx by %dpx' sprintf=[$type.width, $type.height]}</span>
					</label>
				</p>
				{/foreach}
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3" for="resultImage">
				<span class="label-tooltip" data-toggle="tooltip"
				title="{l s='The tag to copy/paste into the description.'}">
					{l s='Image tag to insert'}
				</span>
			</label>
			<div class="col-lg-4">
				<input type="text" id="resultImage" name="resultImage" />
			</div>
			<p class="help-block"></p>
		</div>
	</div>
	{/if}

	{* OVERRIDE *}
	<div class="form-group">
		<div class="col-lg-3"><span class="pull-right"></span></div>
		<div class="col-lg-7">
			<button type="button" class="btn btn-default" style="background-color:black; color:white" data-toggle="collapse" data-target="#collapse_amz" aria-expanded="false" aria-controls="collapse_amz">{l s='Show/Hide Amazon'}</button>
		</div>
	</div>

	<div class="collapse" id="collapse_amz">

		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="description_amazon" type="tinymce" multilang="true"}</span></div>
			<label class="control-label col-lg-2" for="description_amazon_{$id_lang}">
				<span>
					{l s='Amazon Description'}
				</span>
			</label>
			<div class="col-lg-9">
				{include
					file="controllers/products/textarea_lang.tpl"
					languages=$languages input_name='description_amazon'
					class="autoload_rte"
					input_value=$product->description_amazon}
			</div>
		</div>
		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right"></span></div>
			<label class="control-label col-lg-2" for="no_amazon">
				<span>
					{l s='Motivo mancata pubblicazione su Amazon'}
				</span>
			</label>
			<div class="col-lg-7">
				<input type="text" id="no_amazon" name="no_amazon" value="{$product->no_amazon|htmlentitiesUTF8}" />
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right"></span></div>
			<label class="control-label col-lg-2" for="sku_amazon">
				<span>
					{l s='Amazon Logistics SKU'} {* SKU Logistica Amazon *}
				</span>
			</label>
			<div class="col-lg-3">
				<input type="text" id="sku_amazon" name="sku_amazon" value="{$product->sku_amazon}" readonly="readonly" />
			</div>

			<label class="control-label col-lg-1" for="fnsku">
				<span>
					{l s='FNSKU'}
				</span>
			</label>
			<div class="col-lg-3">
				<input type="text" id="fnsku" name="fnsku" value="{$product->fnsku}" readonly="readonly" />
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right"></span></div>
			<label class="control-label col-lg-2" for="punti_di_forza_amazon">
				<span>
					Punti di forza Amazon
				</span>
			</label>
		</div>

		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right"></span></div>
			<label class="control-label col-lg-2" for="punti_di_forza_0">
				<span>
					1
				</span>
			</label>
			<div class="col-lg-7">
				{include file="controllers/products/input_text_lang.tpl"
					languages=$languages
					input_value=$punti_di_forza[0]
					input_name="punti_di_forza_0"
				}
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right"></span></div>
			<label class="control-label col-lg-2" for="punti_di_forza_1">
				<span>
					2
				</span>
			</label>
			<div class="col-lg-7">
				{include file="controllers/products/input_text_lang.tpl"
					languages=$languages
					input_value=$punti_di_forza[1]
					input_name="punti_di_forza_1"
				}
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right"></span></div>
			<label class="control-label col-lg-2" for="punti_di_forza_2">
				<span>
					3
				</span>
			</label>
			<div class="col-lg-7">
				{include file="controllers/products/input_text_lang.tpl"
					languages=$languages
					input_value=$punti_di_forza[2]
					input_name="punti_di_forza_2"
				}
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right"></span></div>
			<label class="control-label col-lg-2" for="punti_di_forza_3">
				<span>
					4
				</span>
			</label>
			<div class="col-lg-7">
				{include file="controllers/products/input_text_lang.tpl"
					languages=$languages
					input_value=$punti_di_forza[3]
					input_name="punti_di_forza_3"
				}
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right"></span></div>
			<label class="control-label col-lg-2" for="punti_di_forza_4">
				<span>
					5
				</span>
			</label>
			<div class="col-lg-7">
				{include file="controllers/products/input_text_lang.tpl"
					languages=$languages
					input_value=$punti_di_forza[4]
					input_name="punti_di_forza_4"
				}
			</div>
		</div>

	</div>

	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right"></span></div>
		<label class="control-label col-lg-2" for="note">
			<span>
				{l s="Note"}
			</span>
		</label>
		<div class="col-lg-7">
			<textarea rows="6" cols="100" name="note" id="note">{$product->note}</textarea>
		</div>
	</div>

	{foreach $rowsvideos as $rowvideos}
		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right"></span></div>
			<label class="control-label col-lg-6" for="cancellavideo_{$rowvideos["id"]}">
				<span> {* Spunta per cancellare *}
					{l s="Check to delete video"} - <a href='{$rowvideos["videoc"]}' target='_blank'>{$rowvideos["videoc"]}</a>
				</span>
			</label>
			<div class="col-lg-3">
				<input type='checkbox' id='cancellavideo_{$rowvideos["id"]}' name='cancellavideo[]' value='{$rowvideos["id"]}' />
			</div>
		</div>
	{/foreach}

	{* Inutilizzati *}
	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right"></span></div>
		<label class="control-label col-lg-2" for="video1">
			<span>
				{l s="Video 1"}
			</span>
		</label>
		<div class="col-lg-7">
			<input type="text" id="video1" name="video1" value="" disabled />
		</div>
	</div>

	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right"></span></div>
		<label class="control-label col-lg-2" for="video2">
			<span>
				{l s="Video 2"}
			</span>
		</label>
		<div class="col-lg-7">
			<input type="text" id="video2" name="video2" value="" disabled />
		</div>
	</div>


	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right"></span></div>
		<label class="control-label col-lg-2" for="comparaprezzi">
			<span>
				Comparaprezzi
			</span>
		</label>
		<div class="col-lg-7" id='comparaprezzifields'>
			<input type="hidden" name="tutti_i_comparaprezzi" />
			<input type="checkbox" name="trovaprezzi_add" {if $product->trovaprezzi }checked="checked"{/if}/> Trovaprezzi &nbsp
			<input type="checkbox" name="google_shopping_add" {if $product->google_shopping }checked="checked"{/if}/> Google Shopping &nbsp<br />
			<input type="checkbox" name="amazon_it_checkbox" {if $amazon_check_it }checked="checked"{/if}{if isset($amazon_it_style) AND $amazon_it_style != ""} style="{$amazon_it_style}"{/if}/> Amazon.it &nbsp
			<input type="checkbox" name="amazon_fr_checkbox" {if $amazon_check_fr }checked="checked"{/if}{if isset($amazon_fr_style) AND $amazon_fr_style != ""} style="{$amazon_fr_style}"{/if}/> Amazon.fr &nbsp
			<input type="checkbox" name="amazon_es_checkbox" {if $amazon_check_es }checked="checked"{/if}{if isset($amazon_es_style) AND $amazon_es_style != ""} style="{$amazon_es_style}"{/if}/> Amazon.es &nbsp
			<input type="checkbox" name="amazon_uk_checkbox" {if $amazon_check_uk }checked="checked"{/if}{if isset($amazon_uk_style) AND $amazon_uk_style != ""} style="{$amazon_uk_style}"{/if}/> Amazon.co.uk &nbsp
			<input type="checkbox" name="amazon_de_checkbox" {if $amazon_check_de }checked="checked"{/if}{if isset($amazon_de_style) AND $amazon_de_style != ""} style="{$amazon_de_style}"{/if}/> Amazon.de &nbsp
			<input type="checkbox" name="amazon_nl_checkbox" {if $amazon_check_nl }checked="checked"{/if}{if isset($amazon_nl_style) AND $amazon_nl_style != ""} style="{$amazon_nl_style}"{/if}/> Amazon.nl &nbsp
			<input type="checkbox" name="amazon_se_checkbox" {if $amazon_check_se }checked="checked"{/if}{if isset($amazon_se_style) AND $amazon_se_style != ""} style="{$amazon_se_style}"{/if}/> Amazon.se &nbsp
			<input type="checkbox" name="amazon_pl_checkbox" {if $amazon_check_pl }checked="checked"{/if}{if isset($amazon_pl_style) AND $amazon_pl_style != ""} style="{$amazon_pl_style}"{/if}/> Amazon.pl &nbsp<br />
			<input type="checkbox" name="eprice_add" {if $product->eprice }checked="checked"{/if}/> ePrice &nbsp<br /><br />
			<input type="checkbox" onclick="checkUncheckSome('tutti_add','comparaprezzifields')" id="tutti_add" name="tutti_add" /> Tutti &nbsp<br />
		</div>
	</div>


	{* FINE OVERRIDE *}

	<div class="form-group">
		<label class="control-label col-lg-3" for="tags_{$id_lang}">
			<span class="label-tooltip" data-toggle="tooltip"
				title="{l s='Will be displayed in the tags block when enabled. Tags help customers easily find your products.'}">
				{l s='Tags:'}
			</span>
		</label>
		<div class="col-lg-9">
			{if $languages|count > 1}
			<div class="row">
			{/if}
				{foreach from=$languages item=language}
					{literal}
					<script type="text/javascript">
						$().ready(function () {
							var input_id = '{/literal}tags_{$language.id_lang}{literal}';
							$('#'+input_id).tagify({delimiters: [13,44], addTagPrompt: '{/literal}{l s='Add tag' js=1}{literal}'});
							$({/literal}'#{$table}{literal}_form').submit( function() {
								$(this).find('#'+input_id).val($('#'+input_id).tagify('serialize'));
							});
						});
					</script>
					{/literal}
				{if $languages|count > 1}
				<div class="translatable-field lang-{$language.id_lang}">
					<div class="col-lg-9">
				{/if}
						<input type="text" id="tags_{$language.id_lang}" class="tagify updateCurrentText" name="tags_{$language.id_lang}" value="{$product->getTags($language.id_lang, true)|htmlentitiesUTF8}" />
				{if $languages|count > 1}
					</div>
					<div class="col-lg-2">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
							{$language.iso_code}
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							{foreach from=$languages item=language}
							<li>
								<a href="javascript:tabs_manager.allow_hide_other_languages = false;hideOtherLanguage({$language.id_lang});">{$language.name}</a>
							</li>
							{/foreach}
						</ul>
					</div>
				</div>
				{/if}
				{/foreach}
			{if $languages|count > 1}
			</div>
			{/if}
		</div>
		<div class="col-lg-9 col-lg-offset-3">
			<div class="help-block">{l s='Each tag has to be followed by a comma. The following characters are forbidden: %s' sprintf='!&lt;;&gt;;?=+#&quot;&deg;{}_$%.'}
			</div>
		</div>
	</div>

	{* OVERRIDE *}

	{* FINE OVERRIDE *}

	<div class="panel-footer">
		<a href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}{if isset($smarty.request.page) && $smarty.request.page > 1}&amp;submitFilterproduct={$smarty.request.page|intval}{/if}" class="btn btn-default"><i class="process-icon-cancel"></i> {l s='Cancel'}</a>
		<button type="submit" name="submitAddproduct" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> {l s='Save'}</button>
		<button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> {l s='Save and stay'}</button>
	</div>
</div>

<br /><br />
{*Variabile $punti_di_forza:
<br /><br />
$punti_di_forza|debug_print_var*}

<script type="text/javascript">
	hideOtherLanguage({$default_form_language});
	var missing_product_name = "{l s='Please fill product name input field' js=1}";
</script>


<script type='text/javascript'>
{literal}
function checkUncheckSome(controller,theElements) {
	var formElements = theElements.split(',');
	var theController = document.getElementById(controller);
	for(var z=0; z<formElements.length;z++){
		theItem = document.getElementById(formElements[z]);
		if(theItem.type && theItem.type=='checkbox'){
				theItem.checked=theController.checked;
		} 
		else {
			theInputs = theItem.getElementsByTagName('input');
			for(var y=0; y<theInputs.length; y++){
				if(theInputs[y].type == 'checkbox' && theInputs[y].id != theController.id){
					theInputs[y].checked = theController.checked;
				}
			}
		}
	}
}

function uncheckTutti(){
	document.getElementById("tutti_checkbox_comparaprezzi").checked = false;
	//wip
}
{/literal}
</script>

{* finire funzione js che seleziona o deseleziona il tutti_add in base al fieldset *}




{* OVVERRIDE: correggere! *}
<script type="text/javascript">
	{literal}
	/*function getSupplierByManufacturer() // inutilizzata (cambiato metodo per aggiungere e rimuovere fornitori)
	{
		var supplier = $("option:selected", "#id_manufacturer").attr("data-supplier");
		document.getElementById("id_supplier").value = supplier;
		var other_suppliers = $("option:selected", "#id_manufacturer").attr("data-othersuppliers");
		var other_suppliers_array = other_suppliers.split(";");
		for(var z=0; z<other_suppliers_array.length;z++){
			item = other_suppliers_array[z];
			if(item.length != 0)
				add_riga_selected(item);
		}
	}*/
	
	function getMargins()
	{
		var margin_min_cli = $("option:selected", "#id_manufacturer").attr("data-margin-cli");
		var margin_min_riv = $("option:selected", "#id_manufacturer").attr("data-margin-riv");
		
		// console.log("cli: "+margin_min_cli+"   riv: "+margin_min_riv);
		
		document.getElementById("margin_min_cli").value = margin_min_cli;
		document.getElementById("margin_min_riv").value = margin_min_riv;
	}
	{/literal}
</script>
{* FINE OVVERRIDE *}
