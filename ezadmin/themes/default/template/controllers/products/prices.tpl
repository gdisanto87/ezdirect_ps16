{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<script type="text/javascript">
var Customer = new Object();
var product_url = '{$link->getAdminLink('AdminProducts', true)|addslashes}';
var ecotax_tax_excl = parseFloat({$ecotax_tax_excl});
var priceDisplayPrecision = {$smarty.const._PS_PRICE_DISPLAY_PRECISION_|intval};

$(document).ready(function () {
	Customer = {
		"hiddenField": jQuery('#id_customer'),
		"field": jQuery('#customer'),
		"container": jQuery('#customers'),
		"loader": jQuery('#customerLoader'),
		"init": function() {
			jQuery(Customer.field).typeWatch({
				"captureLength": 1,
				"highlight": true,
				"wait": 50,
				"callback": Customer.search
			}).focus(Customer.placeholderIn).blur(Customer.placeholderOut);
		},
		"placeholderIn": function() {
			if (this.value == '{l s='All customers'}') {
				this.value = '';
			}
		},
		"placeholderOut": function() {
			if (this.value == '') {
				this.value = '{l s='All customers'}';
			}
		},
		"search": function()
		{
			Customer.showLoader();
			jQuery.ajax({
				"type": "POST",
				"url": "{$link->getAdminLink('AdminCustomers')|addslashes}",
				"async": true,
				"dataType": "json",
				"data": {
					"ajax": "1",
					"token": "{getAdminToken tab='AdminCustomers'}",
					"tab": "AdminCustomers",
					"action": "searchCustomers",
					"customer_search": Customer.field.val()
				},
				"success": Customer.success
			});
		},
		"success": function(result)
		{
			if(result.found) {
				var html = '<ul class="list-unstyled">';
				jQuery.each(result.customers, function() {
					html += '<li><a class="fancybox" href="{$link->getAdminLink('AdminCustomers')}&id_customer='+this.id_customer+'&viewcustomer&liteDisplaying=1">'+this.firstname+' '+this.lastname+'</a>'+(this.birthday ? ' - '+this.birthday:'');
					html += ' - '+this.email;
					html += '<a onclick="Customer.select('+this.id_customer+', \''+this.firstname+' '+this.lastname+'\'); return false;" href="#" class="btn btn-default">{l s='Choose'}</a></li>';
				});
				html += '</ul>';
			}
			else
				html = '<div class="alert alert-warning">{l s='No customers found'}</div>';
			Customer.hideLoader();
			Customer.container.html(html);
			jQuery('.fancybox', Customer.container).fancybox();
		},
		"select": function(id_customer, fullname)
		{
			Customer.hiddenField.val(id_customer);
			Customer.field.val(fullname);
			Customer.container.empty();
			return false;
		},
		"showLoader": function() {
			Customer.loader.fadeIn();
		},
		"hideLoader": function() {
			Customer.loader.fadeOut();
		}
	};
	Customer.init();
});
</script>

<div id="product-flags" class="panel product-tab">
	<input type="hidden" name="submitted_tabs[]" value="flags" />
	<h3>
		Opzioni
	</h3>
	<div class="form-row">
		<div class="row" id="flags_row">
			<div class="form-group col-md-3" style="margin-bottom: 0px;">
				<label class="control-label" for="acquisto_in_dollari">
					<span>
						{l s='Acquisto in dollari?'}
					</span>
				</label>
				<input type="checkbox" name="acquisto_in_dollari" id="acquisto_in_dollari" value="1" {if $product->acquisto_in_dollari == 1} checked="checked" {/if} {* onchange="cambiaValuta();" *} />
			</div>

			<div class="form-group col-md-3" style="margin-bottom: 0px;">
				<label class="control-label" for="blocco_prezzi">
					<span class="label-tooltip" data-toggle="tooltip"
						title="{l s='Con questa opzione saranno disattivati gli aggiornamenti automatici dei prezzi per questo prodotto'}">
						{l s='Blocco prezzi?'}
					</span>
				</label>
				<input type="checkbox" name="blocco_prezzi" id="blocco_prezzi" value="1" {if $product->blocco_prezzi == 1} checked="checked" {/if} />
			</div>
			<div class="form-group col-md-3" style="margin-bottom: 0px;">
				<label class="control-label" for="login_for_offer">
					<span class="label-tooltip" data-toggle="tooltip"
						title="{l s='Con questa opzione il cliente vede il prezzo login for offer'}">
						{l s='Login for offer?'}
					</span>
				</label>
				<input type="checkbox" name="login_for_offer" id="login_for_offer" value="1" {if $product->login_for_offer == 1} checked="checked" {/if} />
			</div>
			<div class="form-group col-md-3" style="margin-bottom: 0px;">
				<label class="control-label" for="ottieni_miglior_prezzo_flag">
					<span>
						{l s='Togli "Ottieni miglior prezzo"'}
					</span>
				</label>
				<input type="checkbox" name="ottieni_miglior_prezzo_flag" id="ottieni_miglior_prezzo_flag" value="1" {if $product->ottieni_miglior_prezzo_flag == 1} checked="checked" {/if} />
			</div>
		</div>
	</div>
</div>

<div id="product-esolver" class="panel product-tab">
	<input type="hidden" name="submitted_tabs[]" value="esolver" />
	<h3 class="blu">
		Acquisto
		<i class="icon-minus-sign" style="cursor:pointer; color:red;" id="chiudi_acq" title="Chiudi" onclick="chiudiAcq()"></i>
		<i class="icon-plus-sign" style="cursor:pointer; color:green;" id="apri_acq" title="Apri" onclick="apriAcq()"></i>
	</h3>
	<div class="form-row">
		{if $product->acquisto_in_dollari == 0}
		<div class="row" id="acq_euro_row">
			
			<div class="form-group col-md-2">
				<label>Listino Vendita</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
						<input class="textbox-fix" type="text" id="listino_esolver" name="listino_esolver" value="{$esolver_listino}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="document.getElementById('listino').value = this.value.replace(/,/g, '.'); calcEsolverListino();">
						<input type="hidden" name="vecchio_listino" value="{$product->listino}" />
					</div>
				</p>
			</div>

			<div class="form-group col-md-2">
				<label>Sc.Acq. 1</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">%</span>
						<input class="textbox-fix" type="text" id="sconto_acquisto_1" name="sconto_acquisto_1" value="{$esolver_sconto_acq_1}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcEsolverScAcq();">
					</div>
				</p>
			</div>
			
			<div class="form-group col-md-2">
				<label>Sc.Acq. 2</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">%</span>
						<input class="textbox-fix" type="text" id="sconto_acquisto_2" name="sconto_acquisto_2" value="{$esolver_sconto_acq_2}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcEsolverScAcq();">
					</div>
				</p>
			</div>

			<div class="form-group col-md-2">
				<label>Sc.Acq. 3</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">%</span>
						<input class="textbox-fix" type="text" id="sconto_acquisto_3" name="sconto_acquisto_3" value="{$esolver_sconto_acq_3}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcEsolverScAcq();">
					</div>
				</p>
			</div>

			<div class="form-group col-md-2">
				<label>Netto eSolver</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
						<input class="textbox-fix" type="text" id="wholesale_price_esolver" name="wholesale_price_esolver" value="{$esolver_acq_netto}" onchange="this.value = this.value.replace(/,/g, '.'); onChangeEsolverAcquisto();">
					</div>
				</p>
			</div>

			{if $check_employee} {* Togliere? *}
			<div class="form-group col-md-2">
				<label>In $</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">$</span>
						<input class="textbox-fix" type="text" name="costo_dollari" value="{$esolver_dollari}" onchange="this.value = this.value.replace(/,/g, '.');">
					</div>
				</p>
			</div>
			{/if}

		</div>
		{else}
		<div class="row" id="acq_dollari_row">
			
			<div class="form-group col-md-2">
				<label>Listino Vendita</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">$</span>
						<input class="textbox-fix" type="text" id="listino_esolver_dollari" name="listino_esolver_dollari" value="" disabled onchange="this.value = this.value.replace(/,/g, '.'); document.getElementById('listino').value = this.value.replace(/,/g, '.');">
					</div>
				</p>
			</div>

			<div class="form-group col-md-2">
				<label>Sc.Acq. 1</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">%</span>
						<input class="textbox-fix" type="text" id="sconto_acquisto_1_dollari" name="sconto_acquisto_1_dollari" value="" disabled onchange="this.value = this.value.replace(/,/g, '.');">
					</div>
				</p>
			</div>
			
			<div class="form-group col-md-2">
				<label>Sc.Acq. 2</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">%</span>
						<input class="textbox-fix" type="text" id="sconto_acquisto_2_dollari" name="sconto_acquisto_2_dollari" value="" disabled onchange="this.value = this.value.replace(/,/g, '.');">
					</div>
				</p>
			</div>

			<div class="form-group col-md-2">
				<label>Sc.Acq. 3</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">%</span>
						<input class="textbox-fix" type="text" id="sconto_acquisto_3_dollari" name="sconto_acquisto_3_dollari" value="" disabled onchange="this.value = this.value.replace(/,/g, '.');">
					</div>
				</p>
			</div>

			<div class="form-group col-md-1">
				<label>Costi</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">$</span>
						<input class="textbox-fix" type="text" id="costi_agg_dollari" name="costi_agg_dollari" value="" disabled onchange="this.value = this.value.replace(/,/g, '.');">
					</div>
				</p>
			</div>

			<div class="form-group col-md-2">
				<label>Netto eSolver</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">$</span>
						<input class="textbox-fix" type="text" id="wholesale_price_esolver_dollari" name="wholesale_price_esolver_dollari" value="" disabled onchange="this.value = this.value.replace(/,/g, '.');">
					</div>
				</p>
			</div>

			<div class="form-group col-md-1">
				<label>In {$currency->prefix}{$currency->suffix}</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
						<input class="textbox-fix" type="text" name="costo_euro" value="" disabled onchange="this.value = this.value.replace(/,/g, '.');">
					</div>
				</p>
			</div>
			
		</div>
		{/if}

		{* REBATE *}
		<div class="row" id="acq_rebate_row">
			
			<div class="form-group col-md-2" id="rebate_1_div" style="margin-bottom:0px;">
				<label>Rebate 1</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">%</span>
						<input class="textbox-fix" type="text" id="rebate_1" name="rebate_1" value="{$sales_rebate_1}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcSalesRebate('1');" required>
					</div>
				</p>
			</div>

			<div class="form-group col-md-2" id="rebate_2_div" style="margin-bottom:0px;">
				<label>Rebate 2</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">%</span>
						<input class="textbox-fix" type="text" id="rebate_2" name="rebate_2" value="{$sales_rebate_2}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcSalesRebate('2');" required>
					</div>
				</p>
			</div>

			<div class="form-group col-md-2" id="rebate_3_div" style="margin-bottom:0px;">
				<label>Rebate 3</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">%</span>
						<input class="textbox-fix" type="text" id="rebate_3" name="rebate_3" value="{$sales_rebate_3}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcSalesRebate('3');" required>
					</div>
				</p>
			</div>

			<div class="form-group col-md-2" id="empty_div" style="margin-bottom:0px;">
			</div>

			<div class="form-group col-md-2" id="acquisto_netto" style="margin-bottom:0px;">
				<label>Acquisto netto</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
						<input class="textbox-fix" type="text" id="wholesale_price" name="wholesale_price" value="{$sales_acquisto}" onchange="this.value = this.value.replace(/,/g, '.'); onChangeAcquisto();" readonly required>
					</div>
				</p>
			</div>

		</div>


	</div>
	{*
	<div class="panel-footer">
		<a href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}{if isset($smarty.request.page) && $smarty.request.page > 1}&amp;submitFilterproduct={$smarty.request.page|intval}{/if}" class="btn btn-default"><i class="process-icon-cancel"></i> {l s='Cancel'}</a>
		<button type="submit" name="submitAddproduct" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> {l s='Save'}</button>
		<button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> {l s='Save and stay'}</button>
	</div>
	*}
</div>

<div class="row">
	{if $has_sp_v}
	<div class="col-md-6">
		<div id="product-spec-vendita" class="panel product-tab">
			<input type="hidden" name="submitted_tabs[]" value="spec_vendita" />
			<h3>Prezzo speciale vendita</h3>
			<div class="form-row">
				<div class="row">
					
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<label style="color:red">Prezzo</label>
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
								<input class="textbox-fix" type="text" id="prezzo_spec_vendita" name="prezzo_spec_vendita" value="{$prezzo_sp_v}" disabled>
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<label style="color:red">
							Margine
							<i class="icon-question-sign label-tooltip" data-toggle="tooltip" title="Calcolato sul miglior prezzo di acquisto disponibile"></i>
						</label>
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="margine_vendita" name="margine_vendita" value="{$margine_sp_v}" disabled>
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<label style="color:red">Qta. promo</label>
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">Pz.</span>
								<input class="textbox-fix" type="text" id="qta_promo_vendita" name="qta_promo_vendita" value="{$sp_pieces_v}" disabled>
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<label style="color:red">Scadenza</label>
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">Il</span>
								<input class="textbox-fix" type="text" id="scad_data_vendita" name="scad_data_vendita" value="{$data_sp_v}" disabled>
							</div>
						</p>
					</div>

				</div>

				{if $has_sp_alert}
				<div class="row">
					<span style="text-align:center; display:block; background-color:#FFCCCC; margin-bottom:0px;">
						<strong style="font-size:16px">!!! ATTENZIONE !!! Prezzo web MINORE del prezzo speciale.</strong>
					</span>
				</div>
				{/if}

			</div>
		</div>
	</div>
	{/if}
	{if $has_sp_a}
	<div class="col-md-6">

		<div id="product-spec-acq" class="panel product-tab">
			<input type="hidden" name="submitted_tabs[]" value="spec_acq" />
			<h3>Prezzo speciale acquisto</h3>
			<div class="form-row">
				<div class="row">
					
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<label style="color:red">Prezzo</label>
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
								<input class="textbox-fix" type="text" id="prezzo_spec_acquisto" name="prezzo_spec_acquisto" value="{$prezzo_sp_a}" disabled>
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<label style="color:red">
							Margine
							<i class="icon-question-sign label-tooltip" data-toggle="tooltip" title="Calcolato sul miglior prezzo di vendita disponibile"></i>
						</label>
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="margine_acquisto" name="margine_acquisto" value="{$margine_sp_a}" disabled>
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<label style="color:red">Qta. promo</label>
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">Pz.</span>
								<input class="textbox-fix" type="text" id="qta_promo_acquisto" name="qta_promo_acquisto" value="{$sp_pieces_a}" disabled>
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<label style="color:red">Scadenza</label>
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">Il</span>
								<input class="textbox-fix" type="text" id="scad_data_acquisto" name="scad_data_acquisto" value="{$data_sp_a}" disabled>
							</div>
						</p>
					</div>

				</div>

			</div>
		</div>
	</div>
	{/if}
</div>

<div id="product-sales" class="panel product-tab">
	<input type="hidden" name="submitted_tabs[]" value="sales" />
	<h3 class="blu">Sales</h3>
	<div class="form-row">

		{if $has_sales_alert}
		<div class="row">
			<span style="text-align:center; display:block; background-color:#FFCCCC; margin-bottom:0px;">
				<strong style="font-size:16px">!!! ATTENZIONE !!! Prodotto in vendita con margine sotto il 10%.</strong>
			</span>
			<br />
		</div>
		{/if}

		<div class="row">
			
			<div class="form-group col-md-3">
				<label>Listino</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
						<input class="textbox-fix" type="text" id="listino" name="listino" value="{$sales_listino}" onchange="this.value = this.value.replace(/,/g, '.'); document.getElementById('listino_esolver').value = this.value.replace(/,/g, '.');" onkeyup="calcSalesListino();" readonly>
					</div>
				</p>
			</div>

			<div class="form-group col-md-3">
				<label>Sconto clienti</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">%</span>
						<input class="textbox-fix" type="text" id="scontolistinovendita" name="scontolistinovendita" value="{$sales_sconto}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcSalesScontoPrezzoV('sconto');">
					</div>
				</p>
			</div>

			<div class="form-group col-md-3">
				<label>Prezzo di vendita</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
						<input class="textbox-fix" type="text" id="priceTE" name="price" value="{$sales_prezzo}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcSalesScontoPrezzoV('prezzo');" required>
					</div>
				</p>
			</div>

			<div class="form-group col-md-3">
				<label>Marginalità</label>
				<p class="form-control-static">
					<div class="input-group">
						<span class="input-group-addon">%</span>
						<input class="textbox-fix" type="text" id="marginalita" name="marginalita" value="{$sales_marginalita}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="if (isArrowKey(event)) return; calcPriceTI();">
					</div>
				</p>
			</div>

		</div>

		<div class="row">
			
			<div class="form-group col-md-3" style="margin-bottom:0px;">
				<label>Margine login for offer</label>
			</div>
			<div class="form-group col-md-2" style="margin-bottom:0px;">
				<div class="input-group">
					<span class="input-group-addon">%</span>
					<input class="textbox-fix" type="text" id="margin_login_for_offer" name="margin_login_for_offer" value="{$margine_login_for_offer}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="if(isArrowKey(event)) return; calcPrezzoFromMargine('wholesale_price', 'margin_login_for_offer', 'prezzo_login_for_offer');" required>
				</div>
			</div>

			<div class="form-group col-md-1" style="margin-bottom:0px;">
			</div>

			<div class="form-group col-md-3" style="margin-bottom:0px;">
				<label>Prezzo login for offer</label>
			</div>
			<div class="form-group col-md-2" style="margin-bottom:0px;">
				<div class="input-group">
					<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
					<input class="textbox-fix" type="text" id="prezzo_login_for_offer" name="prezzo_login_for_offer" value="{$prezzo_login_for_offer}" onchange="this.value = this.value.replace(/,/g, '.');" disabled required>
				</div>
		</div>

		</div>

	</div>
</div>

<div id="product-sconti-quantita" class="panel product-tab" style="background-color:#99cc99">
	<input type="hidden" name="submitted_tabs[]" value="sconti" />
	<h3>
		Sconti quantità (<strong> {$currency->sign} </strong>)
		<i class="icon-question-sign label-tooltip" data-toggle="tooltip" title="I margini sono calcolati sul prezzo di vendita"></i>
	</h3>
	<div class="form-row">
		<div class="row" id="qta1_row">

			<div class="form-group col-md-2" style="margin-bottom:0px;">
				<label>
					<i class="icon-minus-sign" style="cursor:pointer; color:red;" id="chiudi_qta" title="Chiudi" onclick="chiudiQta()"></i>
					<i class="icon-plus-sign" style="cursor:pointer; color:green;" id="apri_qta" title="Apri" onclick="apriQta()"></i>
				</label>
				<div class="input-group" style="font-weight:bold;">
					Sconto quantità 1
				</div>
			</div>
	
			<div class="form-group col-md-2" style="margin-bottom:0px;">
				<label>Quantità</label>
					<div class="input-group">
						<span class="input-group-addon">Pz.</span>
						<input class="textbox-fix" type="text" id="quantita_1" name="quantita_1" value="{$qta_1}" onchange="this.value = this.value.replace(/,/g, '.');">
					</div>
			</div>

			<div class="form-group col-md-2" style="margin-bottom:0px;">
				{*<label>Sc. qta 1</label>*}
				<label>Sconto</label>
					<div class="input-group">
						<span class="input-group-addon">%</span>
						<input class="textbox-fix" type="text" id="sconto_quantita_1" name="sconto_quantita_1" value="{$sc_qta_1p}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcScQta('1', 'sconto');" required>
					</div>
			</div>

			<div class="form-group col-md-2" style="margin-bottom:0px;">
				<label>Prezzo</label>
					<div class="input-group">
						<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
						<input class="textbox-fix" type="text" id="prezzo_qta_1" name="prezzo_qta_1" value="{$prezzo_qta_1}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcScQta('1', 'prezzo');" required>
					</div>
			</div>

			<div class="form-group col-md-2" style="margin-bottom:0px;">
				<label>Marginalità</label>
					<div class="input-group">
						<span class="input-group-addon">%</span>
						<input class="textbox-fix" type="text" id="margin_qta_1" name="margin_qta_1" value="{$margine_qta_1}" onchange="this.value = this.value.replace(/,/g, '.');">
					</div>
			</div>

		</div>

		<div class="row" id="qta2_row">

			<div class="form-group col-md-2" style="margin-bottom:0px;">
					<div class="input-group" style="font-weight:bold;">
						Sconto quantità 2
					</div>
			</div>
			
			<div class="form-group col-md-2" style="margin-bottom:0px;">
				{*<label>Quantità</label>*}
					<div class="input-group">
						<span class="input-group-addon">Pz.</span>
						<input class="textbox-fix" type="text" id="quantita_2" name="quantita_2" value="{$qta_2}" onchange="this.value = this.value.replace(/,/g, '.');">
					</div>
			</div>

			<div class="form-group col-md-2" style="margin-bottom:0px;">
				{*<label>Sc. qta 2</label>*}
					<div class="input-group">
						<span class="input-group-addon">%</span>
						<input class="textbox-fix" type="text" id="sconto_quantita_2" name="sconto_quantita_2" value="{$sc_qta_2p}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcScQta('2', 'sconto');" required>
					</div>
			</div>

			<div class="form-group col-md-2" style="margin-bottom:0px;">
				{*<label>Prezzo</label>*}
					<div class="input-group">
						<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
						<input class="textbox-fix" type="text" id="prezzo_qta_2" name="prezzo_qta_2" value="{$prezzo_qta_2}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcScQta('2', 'prezzo');" required>
					</div>
			</div>

			<div class="form-group col-md-2" style="margin-bottom:0px;">
				{*<label>Marginalità</label>*}
					<div class="input-group">
						<span class="input-group-addon">%</span>
						<input class="textbox-fix" type="text" id="margin_qta_2" name="margin_qta_2" value="{$margine_qta_2}" onchange="this.value = this.value.replace(/,/g, '.');">
					</div>
			</div>

		</div>

		<div class="row" id="qta3_row">

			<div class="form-group col-md-2" style="margin-bottom:0px;">
					<div class="input-group" style="font-weight:bold;">
						Sconto quantità 3
					</div>
			</div>
			
			<div class="form-group col-md-2" style="margin-bottom:0px;">
				{*<label>Quantità</label>*}
					<div class="input-group">
						<span class="input-group-addon">Pz.</span>
						<input class="textbox-fix" type="text" id="quantita_3" name="quantita_3" value="{$qta_3}" onchange="this.value = this.value.replace(/,/g, '.');">
					</div>
			</div>

			<div class="form-group col-md-2" style="margin-bottom:0px;">
				{*<label>Sc. qta 3</label>*}
					<div class="input-group">
						<span class="input-group-addon">%</span>
						<input class="textbox-fix" type="text" id="sconto_quantita_3" name="sconto_quantita_3" value="{$sc_qta_3p}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcScQta('3', 'sconto');" required>
					</div>
			</div>

			<div class="form-group col-md-2" style="margin-bottom:0px;">
				{*<label>Prezzo</label>*}
					<div class="input-group">
						<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
						<input class="textbox-fix" type="text" id="prezzo_qta_3" name="prezzo_qta_3" value="{$prezzo_qta_3}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcScQta('3', 'prezzo');" required>
					</div>
			</div>

			<div class="form-group col-md-2" style="margin-bottom:0px;">
				{*<label>Marginalità</label>*}
					<div class="input-group">
						<span class="input-group-addon">%</span>
						<input class="textbox-fix" type="text" id="margin_qta_3" name="margin_qta_3" value="{$margine_qta_3}" onchange="this.value = this.value.replace(/,/g, '.');">
					</div>
			</div>

		</div>

	</div>
</div>


<div class="row">
	<div class="col-md-8">
		<div id="product-spec-vendita" class="panel product-tab" style="background-color:#ff99ff">
			<input type="hidden" name="submitted_tabs[]" value="spec_vendita" />
			<h3>
				Sconti rivenditori (<strong> {$currency->sign} </strong>)
				<i class="icon-question-sign label-tooltip" data-toggle="tooltip" title="I margini sono calcolati sul prezzo di listino"></i>
			</h3>
			<div class="form-row">
				<div class="row" id="riv1_row">
					
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<label>
							<i class="icon-minus-sign" style="cursor:pointer; color:red;" id="chiudi_riv" title="Chiudi" onclick="chiudiRiv()"></i>
							<i class="icon-plus-sign" style="cursor:pointer; color:green;" id="apri_riv" title="Apri" onclick="apriRiv()"></i>
						</label>
						<div class="input-group" style="font-weight:bold;">
							Rivenditore 1
						</div>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Sc. rivend. 1</label>*}
						<label>Sconto</label>
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="sconto_rivenditore_1" name="sconto_rivenditore_1" value="{$sc_riv_1p}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcScRiv('1', 'sconto');" required>
							</div>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<label>Prezzo</label>
							<div class="input-group">
								<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
								<input class="textbox-fix" type="text" id="prezzo_riv_1" name="prezzo_riv_1" value="{$prezzo_riv_1}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcScRiv('1', 'prezzo');" required>
							</div>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<label>Marginalità</label>
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="margin_riv_1" name="margin_riv_1" value="{$margine_riv_1}" onchange="this.value = this.value.replace(/,/g, '.');">
							</div>
					</div>

				</div>

				<div class="row" id="riv2_row">

					<div class="form-group col-md-3" style="margin-bottom:0px;">
							<div class="input-group" style="font-weight:bold;">
								Rivenditore 2
								<i class="icon-question-sign label-tooltip" data-toggle="tooltip" title="Lo sconto rivenditore 2 è calcolato elevando del 3% il prezzo finale del rivenditore 1"></i>
							</div>
					</div>
					
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Sc. rivend. 2</label>
						<i class="icon-question-sign label-tooltip" data-toggle="tooltip" title="Lo sconto rivenditore 2 è calcolato elevando del 3% il prezzo finale del rivenditore 1"></i>*}
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="sconto_rivenditore_2" name="sconto_rivenditore_2" value="{$sc_riv_2p}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcScRiv('2', 'sconto');" readonly required>
							</div>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Prezzo</label>*}
							<div class="input-group">
								<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
								<input class="textbox-fix" type="text" id="prezzo_riv_2" name="prezzo_riv_2" value="{$prezzo_riv_2}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcScRiv('2', 'prezzo');" readonly required>
							</div>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Marginalità</label>*}
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="margin_riv_2" name="margin_riv_2" value="{$margine_riv_2}" onchange="this.value = this.value.replace(/,/g, '.');" readonly>
							</div>
					</div>

				</div>

				<div class="row" id="riv3_row">

					<div class="form-group col-md-3" style="margin-bottom:0px;">
							<div class="input-group" style="font-weight:bold;">
								Distributore
							</div>
					</div>
					
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Sc. distrib.</label>*}
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="sconto_rivenditore_3" name="sconto_rivenditore_3" value="{$sc_riv_3p}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcScRiv('3', 'sconto');" required>
							</div>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Prezzo</label>*}
							<div class="input-group">
								<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
								<input class="textbox-fix" type="text" id="prezzo_riv_3" name="prezzo_riv_3" value="{$prezzo_riv_3}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcScRiv('3', 'prezzo');" required>
							</div>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Marginalità</label>*}
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="margin_riv_3" name="margin_riv_3" value="{$margine_riv_3}" onchange="this.value = this.value.replace(/,/g, '.');">
							</div>
					</div>

				</div>

			</div>
		</div>
	</div>

	<div class="col-md-4">

		<div id="product-spec-acq" class="panel product-tab" style="background-color:#8cd9f070">
			<input type="hidden" name="submitted_tabs[]" value="spec_acq" />
			<h3>Premi agenti</h3>
			<div class="form-row">
				<div class="row">
					
					<div class="form-group col-md-6">
						<label>Standard</label>
						<i class="icon-question-sign label-tooltip" data-toggle="tooltip" title="Il premio agente corrisponde al 30% del miglior margine se questo è superiore al 20%, altrimenti il premio è del 25%"></i>
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="provvigione_standard" name="provvigione_standard" value="{$provvigione}" disabled>
							</div>
						</p>
					</div>

					<div class="form-group col-md-6">
						<label>Personalizzato</label>
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="provvigione" name="provvigione" value="{$provvigione_personalizzata}" onchange="this.value = this.value.replace(/,/g, '.');">
							</div>
						</p>
					</div>

				</div>
			</div>
		</div>
	</div>

</div>

<div class="row">
	<div class="col-md-8">

		<div id="product-spec-acq" class="panel product-tab" style="background-color: black; color: white;">
			<input type="hidden" name="submitted_tabs[]" value="spec_acq" />
			<h3>Scontistica Amazon (<strong> {$currency->sign} </strong>) 
				<i class="icon-question-sign label-tooltip" data-toggle="tooltip" title="Se ci sono prezzi speciali, i margini sono calcolati sui prezzi speciali, altrimenti i margini sono calcolati sui prezzi di listino"></i>
				<i class="icon-minus-sign" style="cursor:pointer; color:red;" id="chiudi_amazon" title="Chiudi Amazon" onclick="chiudiAmazon()"></i>
				<i class="icon-plus-sign" style="cursor:pointer; color:green;" id="apri_amazon" title="Apri Amazon" onclick="apriAmazon()"></i>
			</h3>
			
			<div class="form-row"> 
				<div class="row" id="amazon_fr_row">
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<label>Marketplace</label>
						<p class="form-control-static">
							<div class="input-group" style="font-weight:bold;">
								FR
							</div>
						</p>
					</div>
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<label>Sconto {*FR*}</label>
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="sconto_amazon_fr" name="sconto_amazon_fr" value="{$fr_sconto}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcAmazon('fr', 'sconto');">
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<label>Prezzo</label>
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
								<input class="textbox-fix" type="text" id="prezzo_amazon_fr" name="prezzo_amazon_fr" value="{$fr_prezzo}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcAmazon('fr', 'prezzo');">
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<label>Marginalità</label>
						
						<div id="margine_amazon_fr_alert" style="float:right; margin-right:10%">
							{if $fr_has_alert}
							<i class="icon-exclamation-sign" style="color:red" title="Margine FR inferiore al 12,50%"></i>
							{/if}
						</div>
						
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="margin_amazon_fr" name="margin_amazon_fr" value="{$fr_margine}" onchange="this.value = this.value.replace(/,/g, '.');" disabled>
							</div>
						</p>
					</div>

				</div>

				<div class="row" id="amazon_de_row">
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<p class="form-control-static">
							<div class="input-group" style="font-weight:bold;">
								DE
							</div>
						</p>
					</div>
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Sconto DE</label>*}
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="sconto_amazon_de" name="sconto_amazon_de" value="{$de_sconto}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcAmazon('de', 'sconto');">
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Prezzo</label>*}
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
								<input class="textbox-fix" type="text" id="prezzo_amazon_de" name="prezzo_amazon_de" value="{$de_prezzo}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcAmazon('de', 'prezzo');">
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Marginalità</label>*}

						<div id="margine_amazon_de_alert" style="float:right; margin-right:10%">
							{if $de_has_alert}
							<i class="icon-exclamation-sign" style="color:red" title="Margine DE inferiore al 12,50%"></i>
							{/if}
						</div>

						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="margin_amazon_de" name="margin_amazon_de" value="{$de_margine}" onchange="this.value = this.value.replace(/,/g, '.');" disabled>
							</div>
						</p>
					</div>

				</div>

				<div class="row" id="amazon_nl_row">
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<p class="form-control-static">
							<div class="input-group" style="font-weight:bold;">
								NL
							</div>
						</p>
					</div>
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Sconto NL</label>*}
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="sconto_amazon_nl" name="sconto_amazon_nl" value="{$nl_sconto}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcAmazon('nl', 'sconto');">
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Prezzo</label>*}
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
								<input class="textbox-fix" type="text" id="prezzo_amazon_nl" name="prezzo_amazon_nl" value="{$nl_prezzo}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcAmazon('nl', 'prezzo');">
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Marginalità</label>*}
						<div id="margine_amazon_nl_alert" style="float:right; margin-right:10%">
							{if $nl_has_alert}
							<i class="icon-exclamation-sign" style="color:red" title="Margine NL inferiore al 12,50%"></i>
							{/if}
						</div>

						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="margin_amazon_nl" name="margin_amazon_nl" value="{$nl_margine}" onchange="this.value = this.value.replace(/,/g, '.');" disabled>
							</div>
						</p>
					</div>

				</div>

				<div class="row" id="amazon_se_row">
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<p class="form-control-static">
							<div class="input-group" style="font-weight:bold;">
								SE
							</div>
						</p>
					</div>
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Sconto SE</label>*}
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="sconto_amazon_se" name="sconto_amazon_se" value="{$se_sconto}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcAmazon('se', 'sconto');">
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Prezzo</label>*}
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
								<input class="textbox-fix" type="text" id="prezzo_amazon_se" name="prezzo_amazon_se" value="{$se_prezzo}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcAmazon('se', 'prezzo');">
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Marginalità</label>*}
						
						<div id="margine_amazon_se_alert" style="float:right; margin-right:10%">
							{if $se_has_alert}
							<i class="icon-exclamation-sign" style="color:red" title="Margine SE inferiore al 12,50%"></i>
							{/if}
						</div>

						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="margin_amazon_se" name="margin_amazon_se" value="{$se_margine}" onchange="this.value = this.value.replace(/,/g, '.');" disabled>
							</div>
						</p>
					</div>

				</div>

				<div class="row" id="amazon_es_row">
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<p class="form-control-static">
							<div class="input-group" style="font-weight:bold;">
								ES
							</div>
						</p>
					</div>
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Sconto ES</label>*}
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="sconto_amazon_es" name="sconto_amazon_es" value="{$es_sconto}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcAmazon('es', 'sconto');">
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Prezzo</label>*}
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
								<input class="textbox-fix" type="text" id="prezzo_amazon_es" name="prezzo_amazon_es" value="{$es_prezzo}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcAmazon('es', 'prezzo');">
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Marginalità</label>*}
						
						<div id="margine_amazon_es_alert" style="float:right; margin-right:10%">
							{if $es_has_alert}
							<i class="icon-exclamation-sign" style="color:red" title="Margine ES inferiore al 12,50%"></i>
							{/if}
						</div>

						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="margin_amazon_es" name="margin_amazon_es" value="{$es_margine}" onchange="this.value = this.value.replace(/,/g, '.');" disabled>
							</div>
						</p>
					</div>

				</div>

				<div class="row" id="amazon_it_row">
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<p class="form-control-static">
							<div class="input-group" style="font-weight:bold;">
								IT
							</div>
						</p>
					</div>
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Sconto IT</label>*}
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="sconto_amazon_it" name="sconto_amazon_it" value="{$it_sconto}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcAmazon('it', 'sconto');">
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Prezzo</label>*}
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
								<input class="textbox-fix" type="text" id="prezzo_amazon_it" name="prezzo_amazon_it" value="{$it_prezzo}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcAmazon('it', 'prezzo');">
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Marginalità</label>*}
						
						<div id="margine_amazon_it_alert" style="float:right; margin-right:10%">
							{if $it_has_alert}
							<i class="icon-exclamation-sign" style="color:red" title="Margine IT inferiore al 12,50%"></i>
							{/if}
						</div>

						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="margin_amazon_it" name="margin_amazon_it" value="{$it_margine}" onchange="this.value = this.value.replace(/,/g, '.');" disabled>
							</div>
						</p>
					</div>

				</div>

				<div class="row" id="amazon_uk_row">
					<div class="form-group col-md-3">
						<p class="form-control-static">
							<div class="input-group" style="font-weight:bold;">
								UK
							</div>
						</p>
					</div>
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Sconto UK</label>*}
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="sconto_amazon_uk" name="sconto_amazon_uk" value="{$uk_sconto}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcAmazon('uk', 'sconto');">
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Prezzo</label>*}
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
								<input class="textbox-fix" type="text" id="prezzo_amazon_uk" name="prezzo_amazon_uk" value="{$uk_prezzo}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcAmazon('uk', 'prezzo');">
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Marginalità</label>*}
						
						<div id="margine_amazon_uk_alert" style="float:right; margin-right:10%">
							{if $uk_has_alert}
							<i class="icon-exclamation-sign" style="color:red" title="Margine UK inferiore al 12,50%"></i>
							{/if}
						</div>

						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="margin_amazon_uk" name="margin_amazon_uk" value="{$uk_margine}" onchange="this.value = this.value.replace(/,/g, '.');" disabled>
							</div>
						</p>
					</div>

				</div>

				<div class="row" id="amazon_pl_row">
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						<p class="form-control-static">
							<div class="input-group" style="font-weight:bold;">
								PL
							</div>
						</p>
					</div>
					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Sconto PL</label>*}
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="sconto_amazon_pl" name="sconto_amazon_pl" value="{$pl_sconto}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcAmazon('pl', 'sconto');">
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Prezzo</label>*}
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
								<input class="textbox-fix" type="text" id="prezzo_amazon_pl" name="prezzo_amazon_pl" value="{$pl_prezzo}" onchange="this.value = this.value.replace(/,/g, '.');" onkeyup="calcAmazon('pl', 'prezzo');">
							</div>
						</p>
					</div>

					<div class="form-group col-md-3" style="margin-bottom:0px;">
						{*<label>Marginalità</label>*}
						
						<div id="margine_amazon_pl_alert" style="float:right; margin-right:10%">
							{if $pl_has_alert}
							<i class="icon-exclamation-sign" style="color:red" title="Margine PL inferiore al 12,50%"></i>
							{/if}
						</div>
						
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="margin_amazon_pl" name="margin_amazon_pl" value="{$pl_margine}" onchange="this.value = this.value.replace(/,/g, '.');" disabled>
							</div>
						</p>
					</div>

				</div>
				
			</div>
		</div>
	</div>

	<div class="col-md-4">

		<div id="product-spec-acq" class="panel product-tab">
			<input type="hidden" name="submitted_tabs[]" value="spec_acq" />
			<h3>Margini minimi</h3>
			<div class="form-row">
				<div class="row">
					
					<div class="form-group col-md-6" style="margin-bottom:0px;">
						<label>Clienti</label>
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="margin_min_cli" name="margin_min_cli" value="{$margin_min_cli}">
							</div>
						</p>
					</div>

					<div class="form-group col-md-6" style="margin-bottom:0px;">
						<label>Rivenditori</label>
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">%</span>
								<input class="textbox-fix" type="text" id="margin_min_riv" name="margin_min_riv" value="{$margin_min_riv}">
							</div>
						</p>
					</div>

					{if $map_price}
					<div class="form-group col-md-12" style="margin-bottom:0px;">
						<label>Prezzo pubblicabile MAP</label>
						<p class="form-control-static">
							<div class="input-group">
								<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
								<input class="textbox-fix" type="text" id="map_price" name="map_price" value="{$map_price}" disabled>
							</div>
						</p>
					</div>
					{/if}

				</div>
			</div>
		</div>
	</div>

</div>

<div class="row">
	<div class="col-md-8">
		<div id="product-spec-acq" class="panel product-tab">
			<input type="hidden" name="submitted_tabs[]" value="spec_acq" />
			<h3>Migliori prezzi</h3>

			<div class="row">
				<div class="form-group col-md-6">
					<label>Miglior prezzo attuale (clienti web, per 1 pz.)</label>
					<h2>{$currency->sign} {$miglior_prezzo}</h2>
				</div>

				<div class="form-group col-md-6">
					<label>Miglior prezzo acquisto</label>
					<h2>{$currency->sign} {$miglior_prezzo_acquisto} - Marginalità: {$marginalita_migliore_percentuale} %</h2>
				</div>

				{if $occhio_vendita}
					<strong style="color:red">ATTENZIONE<br /> c'è un prezzo vendita speciale ma siamo sotto scorta. I margini sono calcolati sui prezzi di listino.</strong>
				{/if}
				<input type="hidden" id="miglior_prezzo" name="miglior_prezzo" value="{$miglior_prezzo}" />
				<input type="hidden" id="miglior_prezzo_appoggio" name="miglior_prezzo_appoggio" value="{$miglior_prezzo}" />

				{if $occhio_acquisto}
					<strong style="color:red">ATTENZIONE<br /> c'è un prezzo acquisto speciale ma siamo sotto scorta. I margini sono calcolati sui prezzi di listino.</strong>
				{/if}
				<input type="hidden" id="miglior_prezzo_acquisto" name="miglior_prezzo_acquisto" value="{$miglior_prezzo_acquisto}" />
			</div>

		</div>
	</div>
</div>

{* Pulsanti per mostrare/nascondere rows sconti *}
<script type="text/javascript">
	{literal}
	$(document).ready(function() {
		chiudiAmazon();
		chiudiAcq();
		chiudiQta();
		chiudiRiv();
	});

    function apriAmazon()
    {
        $('#amazon_fr_row').show();
        $('#amazon_de_row').show();
        $('#amazon_nl_row').show();
        $('#amazon_se_row').show();
        $('#amazon_es_row').show();
        $('#amazon_it_row').show();
        $('#amazon_uk_row').show();
        $('#amazon_pl_row').show();
        $('#chiudi_amazon').show();
        $('#apri_amazon').hide();
    }
    
    function chiudiAmazon()
    {
        $('#amazon_fr_row').show();
        $('#amazon_de_row').hide();
        $('#amazon_nl_row').hide();
        $('#amazon_se_row').hide();
        $('#amazon_es_row').hide();
        $('#amazon_it_row').show();
        $('#amazon_uk_row').hide();
        $('#amazon_pl_row').hide();
        $('#chiudi_amazon').hide();
        $('#apri_amazon').show();
    }

    function apriAcq()
    {
        $('#acq_euro_row').show();
        $('#acq_dollari_row').show();
        $('#acq_rebate_row').show();

        $('#rebate_1_div').show();
        $('#rebate_2_div').show();
        $('#rebate_3_div').show();
        $('#empty_div').show();

        $('#chiudi_acq').show();
        $('#apri_acq').hide();
    }
    
    function chiudiAcq()
    {
        $('#acq_euro_row').hide();
        $('#acq_dollari_row').hide();
        $('#acq_rebate_row').show();

        $('#rebate_1_div').hide();
        $('#rebate_2_div').hide();
        $('#rebate_3_div').hide();
        $('#empty_div').hide();
        $('#acquisto_netto').show();

        $('#chiudi_acq').hide();
        $('#apri_acq').show();
    }

    // CORREGGERE: per usarla devo creare a prescindere entrambe le righe e nasconderne una al caricamento
    /*function cambiaValuta(){
    	var checkBox = document.getElementById("acquisto_in_dollari");
    	if (checkBox.checked){
    		$('#acq_euro_row').hide();
        	$('#acq_dollari_row').show();
    	}
    	else{
    		$('#acq_euro_row').show();
        	$('#acq_dollari_row').hide();
    	}
    }*/

    function apriQta()
    {
        $('#qta1_row').show();
        $('#qta2_row').show();
        $('#qta3_row').show();
        $('#chiudi_qta').show();
        $('#apri_qta').hide();
    }
    
    function chiudiQta()
    {
        $('#qta1_row').show();
        $('#qta2_row').hide();
        $('#qta3_row').hide();
        $('#chiudi_qta').hide();
        $('#apri_qta').show();
    }

    function apriRiv()
    {
        $('#riv1_row').show();
        $('#riv2_row').show();
        $('#riv3_row').show();
        $('#chiudi_riv').show();
        $('#apri_riv').hide();
    }
    
    function chiudiRiv()
    {
        $('#riv1_row').show();
        $('#riv2_row').hide();
        $('#riv3_row').hide();
        $('#chiudi_riv').hide();
        $('#apri_riv').show();
    }
	{/literal}
</script>

{* FINE OVERRIDE *}

{capture assign=priceDisplayPrecisionFormat}{'%.'|cat:$smarty.const._PS_PRICE_DISPLAY_PRECISION_|cat:'f'}{/capture}
<div id="product-prices" class="panel product-tab">
	<input type="hidden" name="submitted_tabs[]" value="Prices" />
	<h3>{* l s='Product price' *}{l s='Tax rules'}</h3>
	{* <div class="alert alert-info" {if !$country_display_tax_label || $tax_exclude_taxe_option}style="display:none;"{/if}>
		{l s='You must enter either the pre-tax retail price, or the retail price with tax. The input field will be automatically calculated.'}
	</div> *}
	{include file="controllers/products/multishop/check_fields.tpl" product_tab="Prices"}
	{* <div class="form-group" style="display:none;">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="wholesale_price" type="default"}</span></div>
		<label class="control-label col-lg-2" for="wholesale_price">
			<span class="label-tooltip" data-toggle="tooltip" title="{l s='The wholesale price is the price you paid for the product. Do not include the tax.'}">{if !$country_display_tax_label || $tax_exclude_taxe_option}{l s='Wholesale price'}{else}{l s='Pre-tax wholesale price'}{/if}</span>
		</label>
		<div class="col-lg-2">
			<div class="input-group">
				<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
				<input maxlength="27" name="wholesale_price" id="wholesale_price" type="text" value="{{toolsConvertPrice price=$product->wholesale_price}|string_format:$priceDisplayPrecisionFormat}" onchange="this.value = this.value.replace(/,/g, '.');" />
			</div>
			{if isset($pack) && $pack->isPack($product->id)}<p class="help-block">{l s='The sum of wholesale prices of the products in the pack is %s%s%s' sprintf=[$currency->prefix,{toolsConvertPrice price=$pack->noPackWholesalePrice($product->id)|string_format:$priceDisplayPrecisionFormat},$currency->suffix]}</p>{/if}
		</div>
	</div>
	 <div class="form-group">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="price" type="price"}</span></div>
		<label class="control-label col-lg-2" for="priceTE">
			<span class="label-tooltip" data-toggle="tooltip" title="{l s='The pre-tax retail price is the price for which you intend sell this product to your customers. It should be higher than the pre-tax wholesale price: the difference between the two will be your margin.'}">{if !$country_display_tax_label || $tax_exclude_taxe_option}{l s='Retail price'}{else}{l s='Pre-tax retail price'}{/if}</span>
		</label>
		<div class="col-lg-2">
			<div class="input-group">
				<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
				<input type="hidden" id="priceTEReal" name="price" value="{toolsConvertPrice price=$product->price}"/>
				<input size="11" maxlength="27" id="priceTE" name="price_displayed" type="text" value="{{toolsConvertPrice price=$product->price}|string_format:'%.6f'}" onchange="noComma('priceTE'); $('#priceTEReal').val(this.value);" onkeyup="$('#priceType').val('TE'); $('#priceTEReal').val(this.value.replace(/,/g, '.')); if (isArrowKey(event)) return; calcPriceTI();" />
			</div>
		</div>
	</div> *}
	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="id_tax_rules_group" type="default"}</span></div>
		<label class="control-label col-lg-2" for="id_tax_rules_group">
			{l s='Tax rule:'}
		</label>
		<div class="col-lg-8">
			<script type="text/javascript">
				noTax = {if $tax_exclude_taxe_option}true{else}false{/if};
				taxesArray = new Array();
				{foreach $taxesRatesByGroup as $tax_by_group}
					taxesArray[{$tax_by_group.id_tax_rules_group}] = {$tax_by_group|json_encode};
				{/foreach}
				ecotaxTaxRate = {$ecotaxTaxRate / 100};
			</script>
			<div class="row">
				<div class="col-lg-6">
					<select onchange="javascript:calcPrice(); unitPriceWithTax('unit');" name="id_tax_rules_group" id="id_tax_rules_group" {if $tax_exclude_taxe_option}disabled="disabled"{/if} >
						<option value="0">{l s='No Tax'}</option>
					{foreach from=$tax_rules_groups item=tax_rules_group}
						<option value="{$tax_rules_group.id_tax_rules_group}" {if $product->getIdTaxRulesGroup() == $tax_rules_group.id_tax_rules_group}selected="selected"{/if} >
					{$tax_rules_group['name']|htmlentitiesUTF8}
						</option>
					{/foreach}
					</select>
				</div>
				<div class="col-lg-2">
					<a class="btn btn-link confirm_leave" href="{$link->getAdminLink('AdminTaxRulesGroup')|escape:'html':'UTF-8'}&amp;addtax_rules_group&amp;id_product={$product->id}"{if $tax_exclude_taxe_option} disabled="disabled"{/if}>
						<i class="icon-plus-sign"></i> {l s='Create new tax'} <i class="icon-external-link-sign"></i>
					</a>
				</div>
			</div>
		</div>
	</div>
	{if $tax_exclude_taxe_option}
	<div class="form-group">
		<div class="col-lg-9 col-lg-offset-3">
			<div class="alert">
				{l s='Taxes are currently disabled:'}
				<a href="{$link->getAdminLink('AdminTaxes')|escape:'html':'UTF-8'}">{l s='Click here to open the Taxes configuration page.'}</a>
				<input type="hidden" value="{$product->getIdTaxRulesGroup()}" name="id_tax_rules_group" />
			</div>
		</div>
	</div>
	{/if}
	<div class="form-group" {if !$ps_use_ecotax} style="display:none;"{/if}>
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="ecotax" type="default"}</span></div>
		<label class="control-label col-lg-2" for="ecotax">
			<span class="label-tooltip" data-toggle="tooltip" title="{l s='The ecotax is a local set of taxes intended to "promote ecologically sustainable activities via economic incentives". It is already included in retail price: the higher this ecotax is, the lower your margin will be.'}">{l s='Ecotax (tax incl.)'}</span>
		</label>
		<div class="input-group col-lg-2">
			<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
			<input maxlength="27" id="ecotax" name="ecotax" type="text" value="{$product->ecotax|string_format:$priceDisplayPrecisionFormat}" onkeyup="$('#priceType').val('TI');if (isArrowKey(event))return; calcPriceTE(); this.value = this.value.replace(/,/g, '.'); if (parseInt(this.value) > getE('priceTE').value) this.value = getE('priceTE').value; if (isNaN(this.value)) this.value = 0;" />
		</div>
	</div>
	<div class="form-group" style="display:none;" {*if !$country_display_tax_label || $tax_exclude_taxe_option}style="display:none;"{/if*} >
		<label class="control-label col-lg-3" for="priceTI">{l s='Retail price with tax'}</label>
		<div class="input-group col-lg-2">
			<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
			<input id="priceType" name="priceType" type="hidden" value="TE" />
			<input id="priceTI" name="priceTI" type="text" value="" onchange="noComma('priceTI');" maxlength="27" onkeyup="$('#priceType').val('TI');if (isArrowKey(event)) return;  calcPriceTE();" />
		</div>
		{if isset($pack) && $pack->isPack($product->id)}<p class="col-lg-9 col-lg-offset-3 help-block">{l s='The sum of prices of the products in the pack is %s%s%s' sprintf=[$currency->prefix,{toolsConvertPrice price=$pack->noPackPrice($product->id)|string_format:$priceDisplayPrecisionFormat},$currency->suffix]}</p>{/if}
	</div>

	<div class="form-group" style="display: none;">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="unit_price" type="unit_price"}</span></div>
		<label class="control-label col-lg-2" for="unit_price">
			<span class="label-tooltip" data-toggle="tooltip" title="{l s='When selling a pack of items, you can indicate the unit price for each item of the pack. For instance, "per bottle" or "per pound".'}">{l s='Unit price (tax excl.)'}</span>
		</label>
		<div class="col-lg-4">
			<div class="input-group">
				<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
				<input id="unit_price" name="unit_price" type="text" value="{$unit_price|string_format:'%.6f'}" maxlength="27" onkeyup="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, '.'); unitPriceWithTax('unit');"/>
				<span class="input-group-addon">{l s='per'}</span>
				<input id="unity" name="unity" type="text" value="{$product->unity|htmlentitiesUTF8}"  maxlength="255" onkeyup="if (isArrowKey(event)) return ;unitySecond();" onchange="unitySecond();"/>
			</div>
		</div>
	</div>
	{if isset($product->unity) && $product->unity}
	<div class="form-group">
		<div class="col-lg-9 col-lg-offset-3">
			<div class="alert alert-warning">
				<span>{l s='or'}
					{$currency->prefix}<span id="unit_price_with_tax">0.00</span>{$currency->suffix}
					{l s='per'} <span id="unity_second">{$product->unity}</span>{if $ps_tax && $country_display_tax_label} {l s='(tax incl.)'}{/if}
				</span>
			</div>
		</div>
	</div>
	{/if}
	<div class="form-group" style="display:none;">
		<div class="col-lg-1"><span class="pull-right">{include file="controllers/products/multishop/checkbox.tpl" field="on_sale" type="default"}</span></div>
		<label class="control-label col-lg-2" for="on_sale">&nbsp;</label>
		<div class="col-lg-9">
			<div class="checkbox">
				<label class="control-label" for="on_sale" >
					<input type="checkbox" name="on_sale" id="on_sale" {if $product->on_sale}checked="checked"{/if} value="1" />
					{l s='Display the "on sale" icon on the product page, and in the text found within the product listing.'}
				</label>
			</div>
		</div>
	</div>
	<div class="form-group" style="display:none;">
		<div class="col-lg-9 col-lg-offset-3">
			<div class="alert alert-warning">
				<strong>{l s='Final retail price:'}</strong>
				<span>
					{$currency->prefix}
					<span id="finalPrice" >0.00</span>
					{$currency->suffix}
					<span{if !$ps_tax} style="display:none;"{/if}> ({l s='tax incl.'})</span>
				</span>
				<span{if !$ps_tax} style="display:none;"{/if} >
				{if $country_display_tax_label}
					/
				{/if}
					{$currency->prefix}
				<span id="finalPriceWithoutTax"></span>
					{$currency->suffix}
					{if $country_display_tax_label}({l s='tax excl.'}){/if}
				</span>
			</div>
		</div>
	</div>
	{* <div class="panel-footer">
		<a href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}{if isset($smarty.request.page) && $smarty.request.page > 1}&amp;submitFilterproduct={$smarty.request.page|intval}{/if}" class="btn btn-default"><i class="process-icon-cancel"></i> {l s='Cancel'}</a>
		<button type="submit" name="submitAddproduct" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> {l s='Save'}</button>
		<button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> {l s='Save and stay'}</button>
	</div> *}
</div>
{if isset($specificPriceModificationForm)}
<div class="panel">
	<h3>{l s='Specific prices'}</h3>
	<div class="alert alert-info">
		{l s='You can set specific prices for clients belonging to different groups, different countries, etc.'}
	</div>
	<div class="form-group">
		<div class="col-lg-12">
			<a class="btn btn-default" href="#" id="show_specific_price">
				<i class="icon-plus-sign"></i> {l s='Add a new specific price'}
			</a>
			<a class="btn btn-default" href="#" id="hide_specific_price" style="display:none">
				<i class="icon-remove text-danger"></i> {l s='Cancel new specific price'}
			</a>
		</div>
	</div>
	<script type="text/javascript">
		var product_prices = new Array();
		{foreach from=$combinations item='combination'}
			product_prices['{$combination.id_product_attribute}'] = '{$combination.price|@addcslashes:'\''}';
		{/foreach}
	</script>
	<div id="add_specific_price" class="well clearfix" style="display: none;">
		<div class="col-lg-12">
			<div class="form-group">
				<label class="control-label col-lg-2" for="{if !$multi_shop}spm_currency_0{else}sp_id_shop{/if}">{l s='For'}</label>
				<div class="col-lg-9">
					<div class="row">
					{if !$multi_shop}
						<input type="hidden" name="sp_id_shop" value="0" />
					{else}
						<div class="col-lg-3">
							<select name="sp_id_shop" id="sp_id_shop">
								{if !$admin_one_shop}<option value="0">{l s='All shops'}</option>{/if}
								{foreach from=$shops item=shop}
								<option value="{$shop.id_shop}">{$shop.name|htmlentitiesUTF8}</option>
								{/foreach}
							</select>
						</div>
					{/if}
						<div class="col-lg-3">
							<select name="sp_id_currency" id="spm_currency_0" onchange="changeCurrencySpecificPrice(0);">
								<option value="0">{l s='All currencies'}</option>
								{foreach from=$currencies item=curr}
								<option value="{$curr.id_currency}">{$curr.name|htmlentitiesUTF8}</option>
								{/foreach}
							</select>
						</div>
						<div class="col-lg-3">
							<select name="sp_id_country" id="sp_id_country">
								<option value="0">{l s='All countries'}</option>
								{foreach from=$countries item=country}
								<option value="{$country.id_country}">{$country.name|htmlentitiesUTF8}</option>
								{/foreach}
							</select>
						</div>
						<div class="col-lg-3">
							<select name="sp_id_group" id="sp_id_group">
								<option value="0">{l s='All groups'}</option>
								{foreach from=$groups item=group}
								<option value="{$group.id_group}">{$group.name}</option>
								{/foreach}
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-2" for="customer">{l s='Customer'}</label>
				<div class="col-lg-4">
					<input type="hidden" name="sp_id_customer" id="id_customer" value="0" />
					<div class="input-group">
						<input type="text" name="customer" value="{l s='All customers'}" id="customer" autocomplete="off" />
						<span class="input-group-addon"><i id="customerLoader" class="icon-refresh icon-spin" style="display: none;"></i> <i class="icon-search"></i></span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-lg-10 col-lg-offset-2">
					<div id="customers"></div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-2" for="sp_from">{l s='Tipo'}</label>
				<div class="col-lg-9">
					<div class="row">
						<div class="col-lg-4">
							<div class="input-group">
								<input type="radio" name="sp_type" value="vendita" checked="checked" id="sp_type_vendita" /> Vendita
							</div>
						</div>
						<div class="col-lg-4">
							<div class="input-group">
								<input type="radio" name="sp_type" value="acquisto" id="sp_type_acquisto" /> Acquisto
							</div>
						</div>
					</div>
				</div>
			</div>
			{if $combinations|@count != 0}
			<div class="form-group">
				<label class="control-label col-lg-2" for="sp_id_product_attribute">{l s='Combination:'}</label>
				<div class="col-lg-4">
					<select id="sp_id_product_attribute" name="sp_id_product_attribute">
						<option value="0">{l s='Apply to all combinations'}</option>
					{foreach from=$combinations item='combination'}
						<option value="{$combination.id_product_attribute}">{$combination.attributes}</option>
					{/foreach}
					</select>
				</div>
			</div>
			{/if}
			<div class="form-group">
				<label class="control-label col-lg-2" for="sp_from">{l s='Available'}</label>
				<div class="col-lg-9">
					<div class="row">
						<div class="col-lg-4">
							<div class="input-group">
								<span class="input-group-addon">{l s='from'}</span>
								<input type="text" name="sp_from" class="datepicker" value="" style="text-align: center" id="sp_from" />
								<span class="input-group-addon"><i class="icon-calendar-empty"></i></span>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="input-group">
								<span class="input-group-addon">{l s='to'}</span>
								<input type="text" name="sp_to" class="datepicker" value="" style="text-align: center" id="sp_to" />
								<span class="input-group-addon"><i class="icon-calendar-empty"></i></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-2" for="sp_from_quantity">{l s='Starting at'}</label>
				<div class="col-lg-4">
					<div class="input-group">
						<span class="input-group-addon">{l s='unit'}</span>
						<input type="text" name="sp_from_quantity" id="sp_from_quantity" value="1" />
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-2" for="sp_price">{l s='Product price'}
					{if $country_display_tax_label}
						{l s='(tax excl.)'}
					{/if}
				</label>
				<div class="col-lg-9">
					<div class="row">
						<div class="col-lg-4">
							<div class="input-group">
								<span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
								<input type="text" disabled="disabled" name="sp_price" id="sp_price" value="{$product->price|string_format:$priceDisplayPrecisionFormat}" onchange="noComma('sp_price');" />
							</div>
							<p class="checkbox">
								<label for="leave_bprice">{l s='Leave base price:'}</label>
								<input type="checkbox" id="leave_bprice" name="leave_bprice"  value="1" checked="checked"  />
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-lg-2" for="sp_reduction">{l s='Apply a discount of'}</label>
				<div class="col-lg-4">
					<div class="row">
						<div class="col-lg-3">
							<input type="text" name="sp_reduction" id="sp_reduction" value="0.00" onchange="noComma('sp_reduction');"/>
						</div>
						<div class="col-lg-6">
							<select name="sp_reduction_type" id="sp_reduction_type">
								<option value="amount" selected="selected">{$currency->name|escape:'html':'UTF-8'}</option>
								<option value="percentage">{l s='%'}</option>
							</select>
						</div>
						<div class="col-lg-3">
							<select name="sp_reduction_tax" id="sp_reduction_tax">
								<option value="0">{l s='Tax excluded'}</option>
								<option value="1" selected="selected">{l s='Tax included'}</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var currencyName = '{$currency->name|escape:'html':'UTF-8'|@addcslashes:'\''}';
		$(document).ready(function(){
			product_prices['0'] = $('#sp_current_ht_price').html();
			$('#id_product_attribute').change(function() {
				$('#sp_current_ht_price').html(product_prices[$('#id_product_attribute option:selected').val()]);
			});
			$('#leave_bprice').click(function() {
				if (this.checked)
					$('#sp_price').attr('disabled', 'disabled');
				else
					$('#sp_price').removeAttr('disabled');
			});
			$('.datepicker').datetimepicker({
				prevText: '',
				nextText: '',
				dateFormat: 'yy-mm-dd',
				// Define a custom regional settings in order to use PrestaShop translation tools
				currentText: '{l s='Now' js=1}',
				closeText: '{l s='Done' js=1}',
				ampm: false,
				amNames: ['AM', 'A'],
				pmNames: ['PM', 'P'],
				timeFormat: 'hh:mm:ss tt',
				timeSuffix: '',
				timeOnlyTitle: '{l s='Choose Time' js=1}',
				timeText: '{l s='Time' js=1}',
				hourText: '{l s='Hour' js=1}',
				minuteText: '{l s='Minute' js=1}'
			});
			$('#sp_reduction_type').on('change', function() {
				if (this.value == 'percentage')
					$('#sp_reduction_tax').hide();
				else
					$('#sp_reduction_tax').show();
			});
		});
	</script>
	<div class="table-responsive">
	<table id="specific_prices_list" class="table table-bordered">
		<thead>
			<tr>
				<th>{l s='Rule'}</th>
				<th>{l s='Tipo'}</th>
				<th>{l s='Combination'}</th>
				{if $multi_shop}<th>{l s='Shop'}</th>{/if}
				<th>{l s='Currency'}</th>
				<th>{l s='Country'}</th>
				<th>{l s='Group'}</th>
				<th>{l s='Customer'}</th>
				{if $country_display_tax_label}
					<th>{l s='Fixed price (tax excl.)'}</th>
				{else}
					<th>{l s='Fixed price'}</th>
				{/if}
				<th>{l s='Impact'}</th>
				<th>{l s='Prezzo finito'}</th>
				<th>{l s='Period'}</th>
				<th>{l s='From (quantity)'}</th>
				<th>{l s='Action'}</th>
			</tr>
		</thead>
		<tbody>
			{$specificPriceModificationForm}
				<script type="text/javascript">
					$(document).ready(function() {
						delete_price_rule = '{l s="Do you really want to remove this price rule?"}';
						calcPriceTI();
						unitPriceWithTax('unit');
						});
				</script>
			{/if}
