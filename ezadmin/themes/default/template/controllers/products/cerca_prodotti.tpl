


<link rel="stylesheet" type="text/css" href="{$__PS_BASE_URI__}css/jquery.autocomplete.css" />

<script type="text/javascript" src="{$__PS_BASE_URI__}js/jquery/jquery.autocomplete2.js"></script>

<div id="ajax_choose_product" style="padding:6px; padding-top:2px; width:800px;">
    <p class="clear">{l s='Begin typing the first letters of the product name, then select the product from the drop-down list:'}</p>
    {literal}
    <input type="checkbox" checked="checked" id="prodotti_online" onclick="if(document.getElementById('product_autocomplete_input').value != '') { clearAutoComplete(); }" />Online &nbsp;&nbsp;
    <input type="checkbox" id="prodotti_offline" onclick="if(document.getElementById('product_autocomplete_input').value != '') { clearAutoComplete(); }" />Offline &nbsp;
    <input type="checkbox" id="prodotti_old" onclick="if(document.getElementById('product_autocomplete_input').value != '') { clearAutoComplete(); }" />Old &nbsp;&nbsp;
    <input type="checkbox" id="prodotti_disponibili" onclick="if(document.getElementById('product_autocomplete_input').value != '') { clearAutoComplete(); }" />Solo disp. &nbsp;&nbsp;&nbsp;
    {/literal}
			
    <script type="text/javascript" src="../js/select2.js"></script>
    <script type="text/javascript">
        $(document).ready(function() { $("#auto_marca").select2(); $("#auto_serie").select2(); $("#auto_fornitore").select2();  $("#auto_categoria").select2(); });
    </script>
					
    <select id="auto_marca" name="auto_marca" onchange="repopulateSeries(this.value); clearAutoComplete();" style="width:100px">
        <option value="0">Marca...</option>
        {foreach $marche as $key => $marca}
		<option value="{$marca['id_manufacturer']}">{$marca['name']}</option>
        {/foreach}
    </select>
			
					
    <select id="auto_serie" name="auto_serie" onchange="clearAutoComplete();" style="width:100px">
        <option value="0">Serie...</option>
        <option value="0">Scegli prima un costruttore</option>
    </select>
			
    <select id="auto_categoria" name="auto_categoria" onchange="clearAutoComplete();" style="width:100px">
        <option value="0">Categoria...</option>
		{foreach $categorie as $key => $categoria}
		<option value="{$categoria['id_category']}">{$categoria['name']}</option>
        {/foreach}
    </select>
			
    <select id="auto_fornitore" name="auto_fornitore" onchange="clearAutoComplete(); " style="width:100px">
        <option value="0">Fornitore...</option>
        {foreach $fornitori as $key => $fornitore}
		<option value="{$fornitore['id_supplier']}">{$fornitore['name']}</option>
        {/foreach}
    </select>
			
    <input id="reset" type="button" value="Reset" class="button" onclick="document.getElementById('prodotti_online').checked = true; document.getElementById('prodotti_offline').checked = true; document.getElementById('prodotti_old').checked = true; document.getElementById('prodotti_disponibili').checked = false; document.getElementById('auto_categoria').options[0].selected = true; $('#auto_categoria').select2();  document.getElementById('auto_marca').options[0].selected = true; $('#auto_marca').select2();  
			document.getElementById('auto_serie').options[0].selected = true; $('#auto_serie').select2();  
			document.getElementById('auto_fornitore').options[0].selected = true; $('#auto_fornitore').select2(); $('#prodlist').hide(); $('div.autocomplete_list').hide();" />
			<br />
			<input type="checkbox" name="copia_da" id="copia_da" onclick="if(document.getElementById('product_autocomplete_input').value != '') { clearAutoComplete(); }"  /> Spunta prima di cercare se vuoi copiare accessori dal prodotto selezionato, lascia bianco se vuoi caricarlo come accessorio
			<br />
			<input id="veditutti" type="button" value="Cerca" class="button" style="display:none" /> <input size="123" type="text" value="" id="product_autocomplete_input" style="margin-top:3px" /> 
								<img onclick="$(this).prev().search();" style="cursor: pointer;" src="../img/admin/add.gif" alt="'.$this->l('Add an accessory').'" title="'.$this->l('Add an accessory').'" />
							</div>
							<script type="text/javascript">
							
							function getAccessorieIds()
								{
									var ids = '. $obj->id.'+',';
									ids += $('#inputAccessories').val().replace(/\\-/g,',').replace(/\\,$/,'');
									ids = ids.replace(/\,$/,'');

									return ids;
								}
								
								urlToCall = null;
								/* function autocomplete */
								
								function getOnline()
					{
						if(document.getElementById("prodotti_online").checked == true)
							return 1;
						else
							return 0;
					}
					
					function getOffline()
					{
						if(document.getElementById("prodotti_offline").checked == true)
							return 1;
						else
							return 0;
					}

					function getOld()
					{
						if(document.getElementById("prodotti_old").checked == true)
							return 1;
						else
							return 0;
					}
					
					function getDisponibili()
					{
						if(document.getElementById("prodotti_disponibili").checked == true)
							return 1;
						else
							return 0;
					}
					
					function getSerie()
					{
						if(document.getElementById("auto_serie").value > 0)
							return document.getElementById("auto_serie").value;
						else
							return 0;
					}
					
					function getMarca()
					{
						if(document.getElementById("auto_marca").value > 0)
							return document.getElementById("auto_marca").value;
						else
							return 0;
					}
					
					function getFornitore()
					{
						if(document.getElementById("auto_fornitore").value > 0)
							return document.getElementById("auto_fornitore").value;
						else
							return 0;
					}
					
					function getCategoria()
					{
						if(document.getElementById("auto_categoria").value > 0)
							return document.getElementById("auto_categoria").value;
						else
							return 0;
					}
					
					function getCopiaDa()
					{
						console.log("COPIA ACCESSORI");
						if(document.getElementById("copia_da").checked == true)
							return 1;
						else
							return 0;
					}
					
					function repopulateSeries(id_manufacturer)
					{
						$("#auto_serie").empty();
						$.ajax({
						  url:"ajax.php?repopulate_series=y",
						  type: "POST",
						  data: { id_manufacturer: id_manufacturer
						  },
						  success:function(resp){  
							var newOptions = $.parseJSON(resp);
							
							 $("#auto_serie").append($("<option></option>")
								 .attr("value", "0").text("Serie..."));
							
							$.each(newOptions, function(key,value) {
							  $("#auto_serie").append($("<option></option>")
								 .attr("value", value).text(key));
							});
							
							$("#auto_serie").select2({
								placeholder: "Serie..."
							});
						  },
						  error: function(xhr,stato,errori){
						  }
						});
							
						
					}	
							
					$("body").on("click", function (event) {
						 if(event.target.tagName.toUpperCase() == "BODY"  || event.target.id == "auto_marca"   || event.target.id == "auto_serie" || event.target.id == "auto_categoria" || event.target.id == "auto_fornitore" || event.target.id == "veditutti" || event.target.id == "prodotti_online"  || event.target.id == "prodotti_offline" || event.target.id == "prodotti_old"  || event.target.id == "prodotti_disponibili" || event.target.id == "product_autocomplete_input")
						 {
							event.stopPropagation();
							
						 }
						 else
						 {
							$('#prodlist').hide();
							$('div.autocomplete_list').hide();
						 }
					});	
					
								$(function() {
						$('#product_autocomplete_input')
							.autocomplete('ajax_products_list2.php?id_customer=0', {
								minChars: 0,
								autoFill: false,
								max:5000,
								matchContains: true,
								matchSubset: 0,
								mustMatch:true,
								scroll:true,
								useCache:false,
								cacheLength:1,
								formatItem: function(item) {
							
											var color = "";
											if(item[24] == 1) {
												var color = "; color:red";
											}
											
											if(item[39] == 1)
											{
												var cornice = "; border:1px solid green";
											}
											else
											{
												var cornice = "; border:1px solid red";
											}
											if(item[28] == 0) {
												return '</table><br /><div onclick="return false;" style="position:absolute; z-index:99997; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:180px;float:left;margin-left:10px;">Codice</div></th><th style="width:630px;"><div style="width:200px; float:left;">Desc. prodotto<br /><br /></div></th><th style="width:60px; text-align:right"><div style="width:45px;float:left;">Prezzo</div></th><th style="width:35px; text-align:right"><div style="width:30px;float:left; text-align:right">EZ</div></th><th style="width:35px; text-align:right"><div style="width:40px;float:left; text-align:right">ESP</div></th><th style="width:35px; text-align:right"><div style="width:30px;float:left; text-align:right">INT</div></th><th style="width:35px; text-align:right"><div style="width:30px;float:left; text-align:right">ALN</div></th><th style="width:30px; text-align:right"><div style="width:30px;float:left; text-align:right">ITA</div></th><th style="width:30px; text-align:right"><div style="width:30px;float:left; text-align:right">ASI</div></th><th style="width:30px; text-align:right"><div style="width:30px;float:left; text-align:right">Imp</div></th><th style="width:30px; text-align:right"><div style="width:30px;float:left; text-align:center">Tot</div></th></tr></div><table>'
											
											;
											}
											else {
												return '<tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="'+item[35]+'" />&nbsp;&nbsp;'+item[4]+'</td><td style="width:350px'+color+'">'+item[5]+'</td><td style="width:60px; text-align:right">'+item[23]+'</td><td style="width:40px; text-align:right">'+item[25]+'</td><td style="width:40px; text-align:right">'+item[33]+'</td><td style="width:40px; text-align:right">'+item[49]+'</td><td style="width:40px; text-align:right">'+item[34]+'</td><td style="width:40px; text-align:right">'+item[38]+'</td><td style="width:40px; text-align:right">'+item[50]+'</td><td style="width:40px; text-align:right">'+item[37]+'</td><td style="width:40px; text-align:right">'+item[22]+'</td></tr>'
												
												;
												}
											}
								
							}).result(addAccessory);
							
							/*$("#product_autocomplete_input").setOptions({
								extraParams: {excludeIds : getAccessorieIds()}
							});*/		
						
					});
					
					
				
					$("#product_autocomplete_input").css('width','705px');
					$("#product_autocomplete_input").keypress(function(event){
								
								  var keycode = (event.keyCode ? event.keyCode : event.which);
								  if (keycode == "13") {
									  $("#veditutti").trigger("click");
									event.preventDefault();
									event.stopPropagation();    
								  }
								});
					
					
							</script><script type="text/javascript">
				
				$("#veditutti").click ( function (zEvent) {
					
					/*	$("body").trigger("click");
						$("body").trigger("click");
						
						
						
						$("#product_autocomplete_input").focus();
					//	$("#product_autocomplete_input").val("");
					//	$("#product_autocomplete_input").val(" ");
					*/
						var press = jQuery.Event("keypress");
						press.ctrlKey = true;
						if(document.getElementById('product_autocomplete_input').value == "")
						{
							$("#product_autocomplete_input").val(" ");
						}
						$("#product_autocomplete_input").trigger(press);
					
						$("#product_autocomplete_input").trigger("click");
						$("#product_autocomplete_input").trigger("click");
					//	$("#product_autocomplete_input").val("");
						
				} );


				</script>
						</td>
					</tr>';
					
					echo '<tr><td></td><td><br /><br /><br /></td></tr>';