<div class="row">
    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-list-alt"></i> 
                Storico
            </div>
            <div>
            {if !count($storico_prezzi)}
			    Nessuna informazione da mostrare
		    {else}
                <table class="table">
                    <tr>
                        <th>Data</th>
                        <th style="text-align:right">Listino</th>
                        <th style="text-align:right">Acquisto</th>
                        <th style="text-align:right">Vendita</th>
                    <tr>
                    <tr>
                        <td>Attuale</td>
                        <td style="text-align:right">{$listino_attuale}</td>
                        <td style="text-align:right">{$acquisto_attuale}</td>
                        <td style="text-align:right">{$vendita_attuale}</td>
                    </tr>

                {foreach $storico_prezzi as $storico}
                    <tr>
                        <td>{$storico['date']}</td>
                        {$storico['storico']} {* Stringa già formattata con <td> *}
                    </tr>
                {/foreach}
                
                </table>
            {/if}
            
            </div>
        </div>
    </div>
</div>