{*
<div id="product-esolver" class="panel product-tab">
	<input type="hidden" name="submitted_tabs[]" value="eSolver" />
	<h3>eSolver</h3>
</div>
*}
<div class="row">
    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-truck"></i> 
                Sezione articolo
            </div>

            <table cellpadding="5" cellspacing="5">
                <tbody>
                    <tr><td style="width:150px">ID CRM</td><td colspan="7"><input type="text" size="130" name="id_crm" readonly="readonly" value="{$obj->id}" /></td></tr>
                    <tr><td style="width:150px">Tipo anagrafica</td><td colspan="7"><input type="text" size="130" name="tipo_anagrafica" readonly="readonly" value="{$product['tipo_anagrafica']}" /></td></tr>
                    <tr><td>Modello</td><td colspan="7"><input type="text" size="130" name="modello" readonly="readonly" value="{$product['modello']}" /></td></tr>
                    <tr><td>Produttore</td><td colspan="7"><input type="text" size="130" value="{$product['costruttore']}" readonly="readonly" /></td></tr>
                    <tr><td>Codice articolo  EZ</td><td colspan="7"><input type="text" size="130" value="{$product['reference']}" readonly="readonly" /></td></tr>
                    <tr><td>Codice ID Database</td><td colspan="7"><input type="text" size="130" value="{$product['id_product']}" readonly="readonly" /></td></tr>
                    <tr><td>Cod. Produttore (SKU)</td><td colspan="7"><input type="text" size="130"  value="{$product['supplier_reference']}" readonly="readonly" /></td></tr>
                    <tr><td>Codifica fornitore 1</td><td colspan="7"><input type="text" size="130" value="{$product['cod_fornitore_1']}" readonly="readonly" /></td></tr>
                    <tr>
                        <td>Cod. EAN</td><td><input type="text" size="11" value="{$product['ean13']}" readonly="readonly" /></td>
                        <td>EAN descrizione</td><td><input type="text" size="11" name="ean_descrizione" value="{$product['ean_descrizione']}" /></td>
                        <td>EAN quantità</td><td><input type="text" size="6" name="ean_quantita" value="{$product['ean_quantita']}" /></td>
                        <td>EAN tipo</td><td><input type="text" size="6" name="ean_tipo" value="{$product['ean_tipo']}" /></td>
                    </tr>
                    <tr><td>Codice Instrastat</td><td colspan="7"><input type="text" size="130" name="intrastat" value="{$product['intrastat']}" /></td></tr>
                    <tr><td>Descr. Ez (50 car.)</td><td colspan="7"><input type="text" size="130" name="name_short" value="{substr($product['name'],0,50)}" readonly="readonly" /></td></tr>
                    <tr><td>Descr. estesa Ez</td><td colspan="7"><input type="text" size="130" value="{$product['name']}" readonly="readonly" /></td></tr>
                    <tr>
                        <td>In produzione?</td>
                        <td><input type="text" size="12" value="{$product['fuori_produzione_string']}" readonly="readonly" /></td>
                        <td>Usato (sì-no)</td>
                        <td><input type="text" size="12" value="{$product['condition_string']}" readonly="readonly" /></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Data inserimento</td><td><input type="text" size="12" value="{$product['date_add']}" readonly="readonly"/></td>
                        <td>Data ultima modifica</td><td><input type="text" size="12" value="{$product['date_upd']}" readonly="readonly"/></td>
                        <td>Data validit&agrave;</td><td><input type="text" size="12" value=""  readonly="readonly" /></td>
                        <td></td>
                    </tr>
                    <!-- <tr><td>Codice del produttore</td><td><input type="text" size="12"  value="{$product['manufacturer_code']}" readonly="readonly"/> -->
                    <tr>
                        <td>Codice della famiglia</td><td><input type="text" size="12" value="{$product['category_code']}" readonly="readonly"/></td>
                        <td>Descrizione famiglia</td><td><input type="text" size="12" value="{$product['categoria']}" readonly="readonly" /></td>
                        <td></td>
                        <td></td>
                    </tr>
                        <tr><td>Prodotto scaricabile</td><td><input type="text" size="12" value="{$product['downloadable']}" readonly="readonly" /></td>
                        <td>Codice prod scaricabile</td><td><input type="text" size="12" value="{$product['codice_prodotto_scaricabile']}" readonly="readonly" /></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Peso package</td><td><input type="text" size="12" value="{$product['weight']}"  readonly="readonly" /></td>
                        <td>Peso netto</td><td><input type="text" size="12" value=""  readonly="readonly" /></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Nome fornitore primario</td><td><input type="text" size="12" value="{$product['fornitore']}" readonly="readonly" /></td>
                        <td>Note fornitore</td><td><input type="text" size="12" value="{$product['note_fornitore']}" readonly="readonly" /></td>
                        <td>Codice del fornitore esterno</td><td><input type="text" size="12" value="{$product['codice_fornitore']}" readonly="readonly" /></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr><td>Tipo</td><td colspan="7"><input type="text" size="130"  value="{$product['tipo']}" readonly="readonly" /></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                    <tr><td>Serie</td><td colspan="7"><input type="text" size="130"  value="{$product['serie']}" readonly="readonly" /></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                    <tr><td>Altro</td><td colspan="7"><input type="text" size="130" name="altro_esolver" value="{$product['altro_esolver']}" /></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                    <tr><td>Note</td><td colspan="7"><input type="text" size="130" value="{$product['note']}" readonly="readonly" /></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                    <tr><td>Tempo di approvvigionamento</td><td colspan="7"><input type="text" size="130" name="tempo_approvvigionamento" value="{$product['tempo_approvvigionamento']}" /></td><td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                    <tr><td>Codice a barre c/o fornitore</td><td colspan="7"><input type="text" size="130" name="codice_barre_fornitore" value="{$product['codice_barre_fornitore']}" /></td><td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                    <tr><td>Note 1</td><td colspan="7"><input type="text" size="130" value="{$product['note']}" readonly="readonly" /></td><td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                    <tr><td>Note 2</td><td  colspan="7"><input type="text" size="130" name="note_2" value="{$product['note_2']}" /></td><td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                    <tr><td>Pz in confezione</td><td colspan="7"><input type="text" size="130" name="pezzi_in_confezione" value="{$product['pezzi_in_confezione']}" /></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                    <!-- <tr><td>Nr</td><td><input type="text" size="130" name="nr" value="{$product['nr']}" /> -->
                    <!-- <tr><td>Pz</td><td><input type="text" size="130" name="pz" value="{$product['pz']}" /> -->
                    <tr><td>Contenuto confezione</td><td colspan="7"><input type="text" size="130" value="{$product['confezione']}" readonly="readonly" /></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                    <tr><td>Garanzia</td><td colspan="7"><input type="text" size="130" value="{$product['garanzia']}" readonly="readonly" /></td><td></td><td></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                </tbody>
            </table>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-truck"></i> 
                Sezione magazzino Ezdirect e fornitori
            </div>

            <table cellpadding="5" cellspacing="5">
                <tr><td style="width:150px">Magazzino EZ</td><td><input type="text" size="10" value="{$product['stock_quantity']}" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td style="width:150px">Magazzino Amazon</td><td><input type="text" size="10" value="{$product['amazon_quantity']}" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td>Scorta minima</td><td><input type="text" size="10" value="{$product['scorta_minima']}" readonly="readonly" style="text-align:right" /></td>
                <tr><td>Scorta minima Amazon</td><td><input type="text" size="10" value="{$product['scorta_minima_amazon']}" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td>Ord da clienti</td><td><input type="text" size="10" value="{$product['ordinato']}" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td>Imp Ord. Su gestionale</td><td><input type="text" size="10" value="{$product['impegnato_quantity']}" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td>Ordinato a fornitore 1</td><td><input type="text" size="10" value="{$product['ordinato_quantity']}" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td>Magazzino Allnet</td><td><input type="text" size="10"  value="{$product['supplier_quantity']}" readonly="readonly" style="text-align:right" /></td>
                <td>In arrivo Allnet</td><td><input type="text" size="10" value="{$product['arrivo_quantity']}" readonly="readonly" style="text-align:right"/></td>
                <td>Data arrivo allnet</td><td><input type="text" size="10" value="{$product['data_arrivo']}" readonly="readonly" style="text-align:right"/></td></tr>
                <tr><td>Magazzino Esprinet</td><td><input type="text" size="10"  value="{$product['esprinet_quantity']}" readonly="readonly" style="text-align:right" /></td>
                <td>In arrivo Esprinet</td><td><input type="text" size="10" value="{$product['arrivo_esprinet_quantity']}" readonly="readonly" style="text-align:right"/></td>
                <td>Data arrivo Esprinet</td><td><input type="text" size="10" value="{$product['data_arrivo_esprinet']}" readonly="readonly" style="text-align:right"/></td></tr>
                <tr><td>Magazzino Attiva</td><td><input type="text" size="10"  value="{$product['attiva_quantity']}" readonly="readonly" style="text-align:right" /></td>
                <td>In arrivo Attiva</td><td><input type="text" size="10" value="{$product['arrivo_attiva_quantity']}" readonly="readonly" style="text-align:right"/></td>
                <td>Data arrivo Attiva</td><td><input type="text" size="10" value="{$product['data_arrivo_attiva']}" readonly="readonly" style="text-align:right"/></td></tr>
                <tr><td>Magazzino Ingram</td><td><input type="text" size="10"  value="{$product['ingram_quantity']}" readonly="readonly" style="text-align:right" /></td>
                <td>In arrivo Ingram</td><td><input type="text" size="10" value="{$product['arrivo_ingram_quantity']}" readonly="readonly" style="text-align:right"/></td>
                <td>Data arrivo Ingram</td><td><input type="text" size="10" value="{$product['data_arrivo_ingram']}" readonly="readonly" style="text-align:right"/></td></tr>
                <tr><td>Magazzino Itancia</td><td><input type="text" size="10"  value="{$product['itancia_quantity']}" readonly="readonly" style="text-align:right"/>
                </td><td>In arrivo Itancia</td><td><input type="text" size="10"  value="{$product['arrivo_itancia_quantity']}" readonly="readonly" style="text-align:right"/></td>
                <td>Data arrivo Itancia</td><td><input type="text" size="10"  value="{$product['data_arrivo_itancia']}" readonly="readonly" style="text-align:right" /></td></tr>
                <tr><td>Magazzino Intracom</td><td><input type="text" size="10"  value="{$product['intracom_quantity']}" readonly="readonly" style="text-align:right"/>
                </td><td>In arrivo Intracom</td><td><input type="text" size="10"  value="{$product['arrivo_intracom_quantity']}" readonly="readonly" style="text-align:right"/></td>
                <td>Data arrivo Intracom</td><td><input type="text" size="10"  value="{$product['data_arrivo_intracom']}" readonly="readonly" style="text-align:right" /></td></tr>
                <tr><td>Magazzino ASIT</td><td><input type="text" size="10"  value="{$product['asit_quantity']}" readonly="readonly" style="text-align:right"/>
                </td><td></td><td></td><td></td><td></td></tr>
                <tr><td>Disponibile da</td><td><input type="text" size="10" value="{$product['date_available']}" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
            </table>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-truck"></i> 
                Sezione listini
            </div>

            <table cellpadding="5" cellspacing="5">
                <tbody>
                    <tr>
                    <td style="width:150px">Listino costruttore</td><td><input type="text" size="8" value="{$product['listino']}" readonly="readonly" style="text-align:right" /></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td>
                    </tr>
                    <tr>
                        <td>Sconto 1 {$product['fornitore']}</td><td><input type="text" size="8"  value="{$product['sconto_acquisto_1']} %"  readonly="readonly" style="text-align:right" /></td>
                        <td>Sconto 2 {$product['fornitore']}</td><td><input type="text" size="8" value="{$product['sconto_acquisto_2']} %" readonly="readonly" style="text-align:right" /></td>
                        <td>Sconto 3 {$product['fornitore']}</td><td><input type="text" size="8" value="{$product['sconto_acquisto_3']} %" readonly="readonly" style="text-align:right" /></td>
                        <td>Netto acq. {$product['fornitore']}</td><td><input type="text" size="8" value="{$product['wholesale_price']}"  readonly="readonly" style="text-align:right" /></td>
                    </tr>
                    {foreach $listini_fornitori as $pli}
                    <tr>
                        <td>Sconto 1 {$pli['fornitore']}</td><td><input type="text" size="8"  value="{$pli['sconto_acquisto_1']} %"  readonly="readonly" style="text-align:right" /></td>
                        <td>Sconto 2 {$pli['fornitore']}</td><td><input type="text" size="8" value="{$pli['sconto_acquisto_2']} %" readonly="readonly" style="text-align:right" /></td>
                        <td>Sconto 3 {$pli['fornitore']}</td><td><input type="text" size="8" value="{$pli['sconto_acquisto_3']} %" readonly="readonly" style="text-align:right" /></td>
                        <td>Netto acq. {$pli['fornitore']}</td><td><input type="text" size="8" value="{$pli['wholesale_price']}"  readonly="readonly" style="text-align:right" /></td>
                    </tr>
                    {/foreach}
                    <tr>
                        <td>Acquisto in dollari</td><td><input type="text" size="8"  value="{$product['acquisto_in_dollari_string']}"  readonly="readonly" style="text-align:right" /></td>
                        <td></td><td></td><td></td><td></td><td></td><td></td>
                    </tr>
                    <tr><td>Blocco prezzi</td><td><input type="text" size="8" value="{$product['blocco_prezzi_string']}" readonly="readonly" style="text-align:right" /></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                    <tr><td>Login for offer</td><td><input type="text" size="8" value="{$product['login_for_offer_string']}" readonly="readonly" style="text-align:right" /></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                    <tr><td>Sconto cliente</td><td><input type="text" size="8"  value="{$product['scontolistinovendita']} %" readonly="readonly" style="text-align:right" /></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                    <tr><td>Prezzo vendita</td><td><input type="text" size="8" value="{$product['price']}" readonly="readonly" style="text-align:right" /></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                    <tr><td>Trasporto gratuito</td><td><input type="text" size="8" value="{$product['trasporto_gratuito_string']}" readonly="readonly" style="text-align:right" /></td>
                    <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                    <tr>
                        <td>Sc riv 1</td><td><input type="text" size="8" value="{$product['sconto_rivenditore_1']} %" readonly="readonly" style="text-align:right" /></td>
                        <td>Sc riv 2</td><td><input type="text" size="8" value="{$product['sconto_rivenditore_2']} %" readonly="readonly" style="text-align:right" /></td>
                        <td></td><td></td>
                    </tr>
                    <tr>
                        <td>Rebate 1</td><td><input type="text" size="8" value="{$product['rebate_1']} %" readonly="readonly" style="text-align:right" /></td>
                        <td>Descrizione rebate 1</td><td><input type="text" size="8" name="descrizione_rebate_1" value="{$product['descrizione_rebate_1']}" readonly="readonly" style="text-align:right" /></td>
                        <td></td><td></td>
                    </tr>
                    <tr>
                        <td>Rebate 2</td><td><input type="text" size="8" value="{$product['rebate_2']} %" readonly="readonly" style="text-align:right" /></td>
                        <td>Descrizione rebate 2</td><td><input type="text" size="8" name="descrizione_rebate_2" value="{$product['descrizione_rebate_2']}" readonly="readonly" style="text-align:right" /></td>
                        <td></td><td></td>
                    </tr>
                    <tr>
                        <td>Rebate 3</td><td><input type="text" size="8" value="{$product['rebate_3']} %" readonly="readonly" style="text-align:right" /></td>
                        <td>Descrizione rebate 3</td><td><input type="text" size="8" name="descrizione_rebate_3" value="{$product['descrizione_rebate_3']}" readonly="readonly" style="text-align:right" /></td>
                        <td></td><td></td>
                    </tr>
                    <tr><td>Premio target</td><td><input type="text" size="8" name="premio_target" value="{$product['premio_target']}" style="text-align:right" /></td>
                    <tr><td>Descrizione premio</td><td><input type="text" size="8" name="descrizione_premio" value="{$product['descrizione_premio']}" readonly="readonly" style="text-align:right" /></td><td></td><td></td></tr>
                </tbody>
            </table>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-truck"></i> 
                Sezione marketplace
            </div>

            <table>
                <tbody><tr><td style="width:150px">ASIN</td><td><input type="text" size="110" value="{$product['asin']}" readonly="readonly">
                </td></tr><tr><td>Commissione amazon</td><td><input type="text" size="110" name="commissione_amazon" value="{$product['commissione_amazon']}">
                </td></tr><tr><td>Commissione Eprice</td><td><input type="text" size="110" name="commissione_eprice" value="{$product['commissione_eprice']}">
                </td></tr>
                </tbody>
            </table>

            <br />

            <p style="text-align:center"><input type="submit" class="button" name="accoda_esolver" value="Accoda a CSV eSolver e scrivi file"></p></div>

        </div>
    </div>
</div>