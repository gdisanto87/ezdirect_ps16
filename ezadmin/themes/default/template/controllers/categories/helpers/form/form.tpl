{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{extends file="helpers/form/form.tpl"}

{block name="script"}
	var ps_force_friendly_product = false;
{/block}

{* OVERRIDE - correggere: spostare, non trova il js *}
<script type="text/javascript">
	{literal}

	function attivaSEO(testo) 
	{
		var keyword = document.getElementById("parola_chiave").value;
		{/literal} var title = document.getElementById("meta_title_{$id_lang}").value; {literal}
		{/literal} var meta_description = document.getElementById("meta_description_{$id_lang}").value; {literal}
		{/literal} var url = document.getElementById("link_rewrite_{$id_lang}").value; {literal}
		var urlified_keyword = str2url(document.getElementById("parola_chiave").value, "UTF-8");
		
		$.ajax({
			url:"ajax.php?SEOby=y",
			type: "POST",
			data: { text: testo,
			keyword: keyword,
			title: title,
			meta_description: meta_description,
			url: url,
			urlified_keyword: urlified_keyword,
			tipo: "product"
			},
			success:function(r){
				$("#seoby").html(r);
			},
			error: function(xhr,stato,errori){
				console.log("Errore nella cancellazione:"+xhr.status);
			}
		});
	}

	{/literal} var temp = document.getElementById("description_{$id_lang}").value; {literal}
	attivaSEO(temp);

	// PROVA! Correggere
	{/literal} var element1 = document.getElementById("description_{$id_lang}"); {literal}
	{/literal} var element2 = document.getElementById("meta_title_{$id_lang}"); {literal}
	{/literal} var element3 = document.getElementById("meta_description_{$id_lang}"); {literal}
	{/literal} element1.onkeypress = attivaSEO(document.getElementById("description_{$id_lang}").value); {literal}
	{/literal} element2.onkeyup = attivaSEO(document.getElementById("description_{$id_lang}").value); {literal}
	{/literal} element3.onkeyup = attivaSEO(document.getElementById("description_{$id_lang}").value); {literal}
	
	{/literal}
</script>
{* FINE OVERRIDE *}

{block name="input"}
	{if $input.name == "link_rewrite"}
		<script type="text/javascript">
		{if isset($PS_ALLOW_ACCENTED_CHARS_URL) && $PS_ALLOW_ACCENTED_CHARS_URL}
			var PS_ALLOW_ACCENTED_CHARS_URL = 1;
		{else}
			var PS_ALLOW_ACCENTED_CHARS_URL = 0;
		{/if}
		</script>
		{$smarty.block.parent}
	{else}
		{$smarty.block.parent}
	{/if}
	{if in_array($input.name, ['image', 'thumb'])}
		<div class="col-lg-6">
			<div class="help-block">{l s='Recommended dimensions (for the default theme): %1spx x %2spx' sprintf=[$input.format.width, $input.format.height]}
			</div>
		</div>
	{/if}

	{* OVERRIDE *}
	{if $input.name == "meta_title"}
		<div class="col-lg-6">
			<div class="help-block">{l s='Ottimale: tra i 50 e i 70 caratteri'}
			</div>
		</div>
	{/if}
	{if $input.name == "meta_description"}
		<div class="col-lg-6">
			<div class="help-block">{l s='Ottimale: tra i 110 e i 160 caratteri'}
			</div>
		</div>
	{/if}
	{if $input.name == "meta_keywords"}
		<div class="col-lg-6">
			<div class="help-block">{l s='Ottimale: 5-10 parole, 200 caratteri'}
			</div>
		</div>
	{/if}
	{if $input.name == "link_rewrite"}
		<div class="col-lg-6">
			<div class="help-block">{l s='SE SI VUOLE CAMBIARE QUESTO CAMPO, COMUNICARLO A FEDERICO'}
			</div>
		</div>

		{* Correggere e aggiungere tasto collapse *}
		<div class="col-lg-1"><span class="pull-right"></span></div>
		<div class="panel col-lg-9" id="category-seo-ita">
			<h3>{l s='SEO (italian only)'}</h3> {* SEO (solo per italiano) *}
			<div class="form-group">
				<div class="col-lg-1"><span class="pull-right"></span></div>
				<label class="control-label col-lg-2" for="parola_chiave">
					<span>
						{l s='Keyword'} {* Parola chiave *}
					</span>
				</label>
				<div class="col-lg-3">
					<input type="text" name="parola_chiave" id="parola_chiave" value="{$parola_chiave}" onkeyup="attivaSEO(document.getElementById('description_{$id_lang}').value);" />
				</div>
			</div>
			<div id="seoby" style="{if $hidden_seo} display:none; {else} display:block; {/if}"></div>
		</div>
		
	{/if}
	{* FINE OVERRIDE *}
	
{/block}
{block name="description"}
	{$smarty.block.parent}
	{*if ($input.name == 'groupBox')}
		<div class="alert alert-info">
			<h4>{$input.info_introduction}</h4>
			<p>{$input.unidentified}<br />
			{$input.guest}<br />
			{$input.customer}</p>
		</div>
	{/if*}
{/block}
{block name="input_row"}
	{$smarty.block.parent}
	{if ($input.name == 'thumb')}
	{$displayBackOfficeCategory}
	{/if}
{/block}