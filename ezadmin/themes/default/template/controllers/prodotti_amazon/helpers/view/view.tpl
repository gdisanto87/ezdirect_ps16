{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

<div id="container-prodotti-amazon">
	<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate>
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-shopping-cart"></i> Prodotti Amazon
			</div>

			<p>In questa tabella si possono vedere i prodotti attivi su Amazon. Sono presenti tutti i prodotti che hanno il flag sul nostro portale per l'uscita su Amazon (almeno uno).</p>

			<p><span class="thick">LEGENDA</span></p>
			<p>Flag verde (<i class="icon-check" style="color:green"></i>) = il prodotto è attivo sul marketplace relativo alla colonna (IT, FR ecc.) in cui si trova.</p>
			<p>Flag grigio (<i class="icon-check"></i>) = il prodotto ha il flag per essere pubblicato su Amazon ma non è attivo nel marketplace relativo alla colonna.</p>
			<p>Numero vicino al flag = quantità pubblicata sul marketplace.</p>
			<p>La colonna "Tipo QT" indica che tipo di disponibilità pubblichiamo su Amazon. Se "EZ", su Amazon pubblichiamo solo disponibilità del magazzino interno. Se "EZ+F", pubblichiamo disponibilità magazzino interno e magazzino fornitore principale.</p>
			<p>Un prodotto può avere i flag ma non essere attivo sul marketplace per vari motivi (non disponibile in magazzino, descrizione troppo lunga per Amazon, EAN assente, rifiuto di Amazon per altri motivi).</p>
			<br />
			<p><span class="thick">Scorrendo il mouse sul logo prime, si può vedere il prezzo che il prodotto ha su Prime.</span></p>
			<br />
			<p><span class="rosso">ATTENZIONE: i prodotti si aggiornano due volte al giorno (alle 07:05 e alle 23:05). Se si vede solo una colonna con i flag verdi (o due, o comunque non tutte) significa che la procedura è in corso di aggiornamento, quindi attendere qualche minuto e poi aggiornare con F5. La procedura può richiedere anche 20-30 minuti per completarsi causa tempi di Amazon.</span></p>
			<br />

		{if $smarty.post.cerca === null}

			<p><span class="thick">Seleziona i prodotti con i filtri sotto</span> (lasciali vuoti se vuoi la lista completa di tutti i prodotti):</p>
			<form>
				<div class="form-group">
					<label>Per codice prodotto</label>
					<select id="per_prodotto" name="per_prodotto[]" class="form-control textbox-fix select2mul" multiple="multiple">
						{if count($prodotti)}
							{foreach $prodotti AS $key => $row}
							<option value="{$row['id']}">{$row['name']} ({$row['reference']}) - ID {$row['id']}</option>
							{/foreach}
						{/if}
					</select>
				</div>

				<div class="form-group">
					<label>Per macrocategoria</label>
					<select id="per_macrocategoria" name="per_macrocategoria[]" class="form-control textbox-fix select2mul" multiple="multiple">
						{if count($macrocat)}
							{foreach $macrocat AS $key => $row}
							<option value="{$row['id']}">{$row['name']}</option>
							{/foreach}
						{/if}
					</select>
				</div>

				<div class="form-group">
					<label>Per sottocategoria</label>
					<select id="per_sottocategoria" name="per_sottocategoria[]" class="form-control textbox-fix select2mul" multiple="multiple">
						{if count($sottocat)}
							{foreach $sottocat AS $key => $row}
							<option value="{$row['id']}">{$row['name']}</option>
							{/foreach}
						{/if}
					</select>
				</div>

				<div class="form-group">
					<label>Per marchio</label>
					<select id="per_marchio" name="per_marchio[]" class="form-control textbox-fix select2mul" multiple="multiple">
						{if count($marchi)}
							{foreach $marchi AS $key => $row}
							<option value="{$row['id']}">{$row['name']}</option>
							{/foreach}
						{/if}
					</select>
				</div>

				<button type="submit" class="btn btn-default" name="cerca">Cerca</button>
			</form>

		{else}

			{if $striscia_prodotti or $striscia_macrocat or $striscia_cat or $striscia_marchi}
				<p><span class="thick">Hai cercato...</span>
				<br />
				{if $striscia_prodotti}
					<p>{$striscia_prodotti}</p>
				{/if}
				{if $striscia_macrocat}
					<p>{$striscia_macrocat}</p>
				{/if}
				{if $striscia_cat}
					<p>{$striscia_cat}</p>
				{/if}
				{if $striscia_marchi}
					<p>{$striscia_marchi}</p>
				{/if}
				<br />
			{/if}

			<div class="row">
				<div class="form-group col-md-12">
					<table class="table tabella_datatable" id="tabella_prodotti_amazon">
						<thead>
							<tr>
								<th><span class="title_box ">Codice</span></th>
								<th><span class="title_box ">Codice SKU</span></th>
								<th><span class="title_box ">EAN</span></th>
								<th><span class="title_box ">ASIN</span></th>
								<th><span class="title_box ">Prodotto</span></th>
								<th><span class="title_box ">Costruttore</span></th>
								<th><span class="title_box ">Mag. EZ</span></th>
								<th><span class="title_box ">Mag. Forn.</span></th>
								<th><span class="title_box ">Tipo QT</span></th>
								<th><span class="title_box ">Prezzo IT</span></th>
								<th><span class="title_box ">Mag. Amz.</span></th>
								<th><span class="title_box ">Scorta min. Amz.</span></th>
								<th><span class="title_box ">IT</span></th>
								<th><span class="title_box ">FR</span></th>
								<th><span class="title_box ">UK</span></th>
								<th><span class="title_box ">DE</span></th>
								<th><span class="title_box ">ES</span></th>
								<th><span class="title_box ">NL</span></th>
								<th><span class="title_box ">SE</span></th>
								<th><span class="title_box ">PL</span></th>
							</tr>
						</thead>
						<tbody>
							{foreach $lista_prodotti AS $key => $row}
							<tr style="{$row['row_style']}">
								<td><a class="tooltip_prodotto" title="{$row['tooltip']}" href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}&amp;id_product={$row['id']|intval}&amp;updateproduct" target="_blank">{$row['reference']}</a></td>
								<td>{$row["supplier_reference"]}</td>
								<td>{$row["ean13"]}</td>
								<td>{$row["asin"]}</td>
								<td>{$row["name"]}</td>
								<td>{$row["manufacturer"]} {$row["manufacturer_prime"]}</td>
								<td>{$row["stock_quantity"]}</td>
								<td>{$row["quantita_fornitore"]}</td>
								<td>{$row["tipo_qt"]}</td>
								<td style="{$row['prezzo_it_style']}">{$row["prezzo_it"]}</td>
								<td>{$row["amazon_quantity"]}</td>
								<td>{$row["scorta_minima_amazon"]}</td>
								<td>{$row["it"]} {*$row["it_icon"]*}</td>
								<td>{$row["fr"]} {*$row["fr_icon"]*}</td>
								<td>{$row["uk"]} {*$row["uk_icon"]*}</td>
								<td>{$row["de"]} {*$row["de_icon"]*}</td>
								<td>{$row["es"]} {*$row["es_icon"]*}</td>
								<td>{$row["nl"]} {*$row["nl_icon"]*}</td>
								<td>{$row["se"]} {*$row["se_icon"]*}</td>
								<td>{$row["pl"]} {*$row["pl_icon"]*}</td>
							</tr>
							{/foreach}
						</tbody>
					</table>

					<hr />

					<a class="btn btn-default" href="">
						Esporta i dati in formato Excel
					</a>
					
					<a class="btn btn-default" href="">
						Nuova ricerca
					</a>
				</div>
			</div>
		{/if}

		</div>
	</form>
</div>

{/block}
