{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

{assign var="link_gen_base" value="index.php?controller=AdminDuplicati&amp;"}
{assign var="link_gen_home" value="index.php?controller=AdminDuplicati"}
{assign var="token_base" value="&amp;token={getAdminToken tab='AdminDuplicati'}"}

<div id="container-duplicati">
	<form class="form-horizontal col-lg-12" method="post">
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-file-text"></i> Guide all'uso del portale
			</div>
				
			<h2>CLIENTI</h2>
			<a href='guide/connessionecomecliente.pdf' target='_blank'><strong>Come connettersi come cliente (e fare azioni sul profilo del cliente)</strong></a><br />
			<a href='guide/registrareanagrafica.pdf' target='_blank'><strong>Come registrare una nuova anagrafica</strong></a><br />
			<a href='guide/modificareanagrafica.pdf' target='_blank'><strong>Come modificare l'anagrafica di un cliente</strong></a><br />
			<a href='guide/aggiungerenotaprivata.pdf' target='_blank'><strong>Come aggiungere una nota privata nell'anagrafica di un cliente</strong></a><br />
			<a href='guide/aggiungereindirizzocliente.pdf' target='_blank'><strong>Come aggiungere o modificare un indirizzo a un cliente</strong></a><br /><br />
					
			<h2>ORDINI, CARRELLI, OFFERTE</h2>
			<a href='guide/crearecarrello.pdf' target='_blank'><strong>Come creare un nuovo carrello per un cliente registrato</strong></a><br />
			<a href='guide/mostrarecarrelloalcliente.pdf' target='_blank'><strong>Come mostrare un carrello a un cliente registrato</strong></a><br />
			<a href='guide/modificarecarrello.pdf' target='_blank'><strong>Come modificare un carrello di un cliente registrato</strong></a><br />
			<a href='guide/cancellarecarrello.pdf' target='_blank'><strong>Come cancellare un carrello</strong></a><br />
			<a href='guide/trasformazionecarrello.pdf' target='_blank'><strong>Come trasformare un carrello in un ordine</strong></a><br />
			<a href='guide/recuperocsv.pdf' target='_blank'><strong>Come recuperare il CSV di un ordine</strong></a><br />
			<a href='guide/modificareordine.pdf' target='_blank'><strong>Come modificare un ordine</strong></a><br />
			<a href='guide/crearecoupon.pdf' target='_blank'><strong>Come creare un codice coupon</strong></a><br />
			<a href='guide/impostare-trasporto-gratuito.pdf' target='_blank'><strong>Come impostare trasporto gratis per ordini a partire da un certo totale</strong></a><br />
			<a href='guide/inserirecodicetracking.pdf' target='_blank'><strong>Come inserire il codice di tracking per la spedizione di un ordine</strong></a><br /><br />

			<h2>CATALOGO</h2>
			<a href='guide/come-aggiungere-un-filtro.pdf' target='_blank'><strong>Come aggiungere un filtro</strong></a><br />
			<a href='guide/aggiungereallegatosupiuprodotti.pdf' target='_blank'><strong>Come aggiungere un allegato su più prodotti</strong></a><br /><br />

			<h2>ALTRO</h2>
			<a href='guide/bannerhome.pdf' target='_blank'><strong>Come creare, modificare ed eliminare i banner della home page</strong></a><br />
			<a href='guide/manuale-chat.pdf' target='_blank'><strong>Come utilizzare la chat di supporto per gli utenti</strong></a><br />
			<a href='guide/backupdatabase.pdf' target='_blank'><strong>Come fare il backup del database del sito</strong></a><br />
			<a href='guide/guidacms.jpg' target='_blank'><strong>Come scrivere e pubblicare un articolo CMS</strong></a><br /><br />
			<a href='guide/guidaamazon.pdf' target='_blank'><strong>Guida ad Amazon</strong></a><br /><br />

		</div>

	</form>

</div>


{/block}
