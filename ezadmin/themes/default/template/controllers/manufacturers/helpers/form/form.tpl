{extends file="helpers/form/form.tpl"}

{* Correggere: spostare, non trova il js *}
<script type="text/javascript">
	{literal}

	function attivaSEO(testo) 
	{
		var keyword = document.getElementById("parola_chiave").value;
		{/literal} var title = document.getElementById("meta_title_{$id_lang}").value; {literal}
		{/literal} var meta_description = document.getElementById("meta_description_{$id_lang}").value; {literal}
		{/literal} var url = document.getElementById("link_rewrite_{$id_lang}").value; {literal}
		var urlified_keyword = str2url(document.getElementById("parola_chiave").value, "UTF-8");
		
		$.ajax({
			url:"ajax.php?SEOby=y",
			type: "POST",
			data: { text: testo,
			keyword: keyword,
			title: title,
			meta_description: meta_description,
			url: url,
			urlified_keyword: urlified_keyword,
			tipo: "product"
			},
			success:function(r){
				$("#seoby").html(r);
			},
			error: function(xhr,stato,errori){
				console.log("Errore nella cancellazione:"+xhr.status);
			}
		});
	}

	{/literal} var temp = document.getElementById("description_{$id_lang}").value; {literal}
	attivaSEO(temp);

	// PROVA! Correggere
	{/literal} var element1 = document.getElementById("description_{$id_lang}"); {literal}
	{/literal} var element2 = document.getElementById("meta_title_{$id_lang}"); {literal}
	{/literal} var element3 = document.getElementById("meta_description_{$id_lang}"); {literal}
	{/literal} element1.onkeypress = attivaSEO(document.getElementById("description_{$id_lang}").value); {literal}
	{/literal} element2.onkeyup = attivaSEO(document.getElementById("description_{$id_lang}").value); {literal}
	{/literal} element3.onkeyup = attivaSEO(document.getElementById("description_{$id_lang}").value); {literal}
	
	{/literal}
</script>

{block name="input"}
	{$smarty.block.parent}
	{if $input.name == "meta_keywords"}
		{* Correggere e aggiungere tasto collapse *}
		<div class="panel col-lg-9" id="category-seo-ita">
			<h3>{l s='SEO (italian only)'}</h3> {* SEO (solo per italiano) *}
			<div class="form-group">
				<div class="col-lg-1"><span class="pull-right"></span></div>
				<label class="control-label col-lg-2" for="parola_chiave">
					<span>
						{l s='Keyword'} {* Parola chiave *}
					</span>
				</label>
				<div class="col-lg-3">
					<input type="text" name="parola_chiave" id="parola_chiave" value="{$parola_chiave}" onkeyup="attivaSEO(document.getElementById('description_{$id_lang}').value);" />
				</div>
			</div>
			<div id="seoby" style="{if $hidden_seo} display:none; {else} display:block; {/if}"></div>
		</div>
	{/if}
{/block}