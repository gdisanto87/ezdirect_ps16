{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

{if $forecast != 1}

<div id="container-forecast-stock">
	<form class="form-horizontal col-lg-12" method="post">
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-truck"></i> Forecast Stock
			</div>
			<div class="form-group">
				<label>Cerca in base al marchio:</label>
				<p class="form-control-static">
					<select id="per_marchio" name="per_marchio[]" class="form-control textbox-fix select2mul" multiple="multiple">
						<option value="">-- Seleziona --</option>
						{if count($marchi)}
							{foreach $marchi AS $key => $row}
							<option value="{$row['id']}">{$row['name']}</option>
							{/foreach}
						{/if}
					</select>
				</p>
			</div>

			<div class="form-group">
				<label>Seleziona i fornitori</label>
				<p class="form-control-static">
					<select id="per_fornitore" name="per_fornitore[]" class="form-control textbox-fix select2mul" multiple="multiple">
						<option value="">-- Seleziona --</option>
						{if count($fornitori)}
							{foreach $fornitori AS $key => $row}
							<option value="{$row['id']}">{$row['name']}</option>
							{/foreach}
						{/if}
					</select>
				</p>
			</div>

			<input type="hidden" name="azione" value="vai-forecast" />
			<button type="submit" class="btn btn-default">Cerca</button>
		</div>
	</form>
</div>

{else}

<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" name="product" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-user"></i>
            Ricerca forecast stock
            
            <div class="bottoni-tab">
                <a class="btn btn-default" href="">
                    Esporta i dati in formato Excel
				</a>
				
				<a class="btn btn-default" href="">
                    Torna a ricerca
                </a>
            </div>
		</div>

		<div class="row">
            <div class="form-group col-md-12">
				<label for="periodo_tutti">Periodo forecast:</label>
				<input type="text" name="periodo_tutti" id="periodo_tutti" onkeyup="calcolaPeriodoTutti()" value="{$forecast_registrato_2|round:2}" style="max-width: 200px;">
                <table class="table tabella_datatable" id="tabella_forecast_stock">
                    <thead>
                        <tr>
                            <th><span class="title_box ">Codice</span></th>
                            <th><span class="title_box ">Descrizione</span></th>
                            <th><span class="title_box ">Costruttore</span></th>
                            <th><span class="title_box ">Fornitore</span></th>
                            <th><span class="title_box ">Mese</span></th>
                            <th class="corner-cut corner-red" title="2 mesi"><span class="title_box ">2 m.</span></th>
							<th class="corner-cut corner-red" title="3 mesi"><span class="title_box ">3 m.</span></th>
							<th class="corner-cut corner-red" title="6 mesi"><span class="title_box ">6 m.</span></th>
							<th class="corner-cut corner-red" title="12 mesi"><span class="title_box ">12 m.</span></th>
							<th><span class="title_box ">Ordine 1 mese</span></th>
							<th><span class="title_box ">Forecast periodo</span></th>
							<th class="corner-cut corner-red" title="Disponibilita netta = Magazzino EZ - Impegnato"><span class="title_box ">Netto disp.</span></th>
							<th class="corner-cut corner-red" title="Ordinato a fornitore"><span class="title_box ">Ord.</span></th>
							<th class="corner-cut corner-red" title="Ordinato al fornitore + magazzino EZ netto - Impegnato"><span class="title_box ">Ord. + netto</span></th>
							<th><span class="title_box ">Ord. per periodo</span></th>
							<th style="display:none"><span class="title_box ">Previsione</span></th>
							<th style="display:none"><span class="title_box ">Calcola Periodo</span></th>
							<th style="display:none"><span class="title_box ">ID Prodotto</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $prodotti AS $key => $row}
                        <tr>
							<td><a class="tooltip_prodotto" title="{$row['tooltip']}" href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}&amp;id_product={$row['id_product']|intval}&amp;updateproduct" target="_blank">{$row['reference']}</a></td>
                            <td>{$row["nome_prodotto"]}</td>
                            <td>{$row["costruttore"]}</td>
                            <td>{$row["fornitore"]}</td>
                            <td>{$row["venduto30"]}</td>
                            <td>{$row["venduto60"]}</td>
							<td>{$row["venduto90"]}</td>
							<td>{$row["venduto180"]}</td>
							<td>{$row["venduto365"]}</td>
							<td>{$row["ord_mese"]}</td>
							<td id="calcolo_{$row['id_product']}">{$row["forecast"]}</td>
							<td>{$row["netto"]} <input type="hidden" value="{$row['netto']}" id="netto_{$row['id_product']}" /> </td>
							<td>{$row["ord"]} <input type="hidden" value="{$row['ord']}" id="ordinato_{$row['id_product']}" /> </td>
							<td>{$row["ord_netto"]}</td>
							<td id="ord_tot_{$row['id_product']}">{$row["ord_periodo"]}</td>
							<td style="display:none"><input type="hidden" value="{$row['previsione_raw']}" id="ordine_per_periodo_{$row['id_product']}" /><strong>{$row['previsione']}</strong></td>
							<td style="display:none"><input type="text" size="3" id="periodo_{$row['id_product']}" onkeyup="calcolaPeriodo({$row['id_product']})" name="periodo[{$row['id_product']}]" style="text-align:right" value="{$forecast_registrato}" /></td>
							<td style="display:none"><input type="hidden" value="{$row['id_product']}" class="id_productClass" id="id_{$row['id_product']}" name="id_{$row['id_product']}" /><strong>{$row['id_product']}</strong></td>
                        </tr>
                        {/foreach}
                    </tbody>
                </table>

				<hr />

				<a class="btn btn-default" href="">
                    Esporta i dati in formato Excel
				</a>
				
				<a class="btn btn-default" href="">
                    Torna a ricerca
                </a>
            </div>
        </div>
		
	</div>
</form>

{/if}

{/block}

