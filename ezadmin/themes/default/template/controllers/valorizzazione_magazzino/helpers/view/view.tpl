{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

{if ! isset($smarty.post.vai_valorizzazione)}

<div id="container-magazzino">
	<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate>
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> Valorizzazione magazzino
			</div>

			{* Correggere: usare bootstrap *}
			<form method="post" name="valorizzazione" id="valorizzazione">

				Cerca in base al marchio: <br />
				<select class="select2" id="per_marchio" name="per_marchio[]" style="width:600px">
					<option name="0" value="0">--- Scegli un marchio ---</option>
					<option name="tutto" value="tutto">MAGAZZINO EZ</option>
					<option name="logistica_amazon" value="logistica_amazon">LOGISTICA AMAZON</option>
					<option name="logistica_amazon_tutto" value="logistica_amazon_tutto">EZ + LOGISTICA AMAZON</option>
					{foreach $marchi as $m}
						<option name="{$m['id_manufacturer']}" value="{$m['id_manufacturer']}"">{$m['id_manufacturer']} - {$m['name']}</option>
					{/foreach}
				</select>

				<br /><br />
				
				<input type="checkbox" name="al_netto" checked="checked" /> Spunta per vedere anche quantit&agrave; e valore al netto dell'impegnato<br />
				<input type="checkbox" name="ordinato" checked="checked" /> Spunta per vedere anche quantit&agrave; e valore dell'ordinato a fornitore<br />
				
				<input class="button" type="submit" value="Cerca" name="vai_valorizzazione" />
			</form>
		</div>
	</form>
</div>

{else}

<div id="container-magazzino">
	<div class="form-horizontal col-lg-12">
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> Valorizzazione magazzino
			</div>

			{$riepilogo_html} {*  Variabile che sostituisce gli echo finchè non viene portato tutto in bootstrap *}

			<form method="post" name="form_indietro" id="form_indietro">
				<button type="button" class="btn btn-secondary" name="nuova_analisi" onclick="window.location = window.location.href;">Nuova analisi</button>
			</form>
		</div>
	</div>
</div>

{/if}

{/block}
