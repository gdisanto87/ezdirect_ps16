{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

{if !isset($smarty.post.cercaiclienti) && !isset($smarty.post.esportamailtxt)}

<div id="container-marketing-telefonate">
	<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate>
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-phone"></i> Analisi telefonate
			</div>

			<p>Questo strumento ti consente di estrapolare liste di clienti che hanno fatto telefonate a Ezdirect.</p>
			<br />
			{* Correggere: usare bootstrap *}
			<form method="post" name="cercaclienti" id="cercaclienti">
				<strong>Filtri di selezione</strong>
				<br /><br />

				<table cellpadding="9">
					<tr><td>Privati?</td><td>
					<input type='radio' value='0' name='escludi_privati' checked='checked' /> Includi privati &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
					<input type='radio' value='1' name='escludi_privati' /> Escludi privati</td><td></td></tr>
					
					<tr><td>Profilo cliente</td><td>
									
					{foreach $gruppi_clienti as $g}
						<input type='checkbox' value='0' class='gruppi' name='group[{$g['id_group']}]' /> {$g['name']} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					{/foreach}

					<br />
					- <a href='javascript:void(0)' onclick='$(".gruppi").attr("checked", true);'>Clicca per selezionarli tutti</a></td><td></td></tr>
					
					<tr><td>Numero telefonate</td><td>
					<select name='numero_telefonate' style='width:300px'>
					<option value='0'> Qualsiasi </option>
					<option value='1_10'>Da 1 a 10</option>
					<option value='11_30'>Da 11 a 30</option>
					<option value='31_50'>Da 31 a 50</option>
					<option value='50p'>Pi&ugrave; di 50</option>
					</select>
					</td><td><span class='hint_tooltip' style="cursor:pointer;" title="Per selezionare il numero complessivo di telefonate fatte dal cliente nel periodo selezionato." ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
					
					<tr><td>Durata complessiva</td><td>
					<select name='durata' style='width:300px'>
					<option value='0'> Qualsiasi </option>
					<option value='1_15'>Da 1 a 15 minuti</option>
					<option value='16_30'>Da 16 a 30 minuti</option>
					<option value='31_60'>Da 31 a 60 minuti</option>
					<option value='60p'>Pi&ugrave; di un'ora</option>
					</select>
					</td><td><span class='hint_tooltip' style="cursor:pointer;" title="Per selezionare la durata complessiva delle telefonate fatte dal cliente nel periodo selezionato." ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
					
					<tr><td>Per reparto raggiunto</td><td>
					<select name='reparto' style='width:300px'>
					<option value='0'> Tutti </option>
					<option value='6700'>Ordini</option>
					<option value='6701'>Commerciale</option>
					<option value='6702'>Assistenza tecnica</option>
					<option value='6703'>Amministrazione</option>
					<option value='6704'>Chiamata in entrata</option>
					</select>
					</td><td><span class='hint_tooltip' style="cursor:pointer;" title="Per selezionare tutte le chiamate da o verso un preciso reparto." ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
					
					<tr><td>Per interno raggiunto</td><td>
					<select name='interno' style='width:300px'>
					<option value='0'> Tutti </option>
					<option value='201'>Ezio</option>
					<option value='202'>Barbara</option>
					<option value='203'>Paolo</option>
					<option value='205'>Massimo</option>
					<option value='206'>Magazzino</option>
					<option value='207'>Matteo</option>
					<option value='208'>Valentina</option>
					<option value='209'>Federico</option>
					<option value='210'>Sala Riunioni</option>
					<option value='211'>Ingresso</option>
					<option value='212'>Laboratorio</option>
					<option value='214'>Laboratorio 1</option>
					<option value='215'>Laboratorio 2</option>
					<option value='216'>Laboratorio 3</option>
					<option value='217'>Laboratorio 4</option>
					<option value='218'>Amministrazione</option>
					<option value='219'>Lorenzo</option>
					</select>
					</td><td><span class='hint_tooltip' style="cursor:pointer;" title="Per selezionare tutte le chiamate da o verso un preciso interno." ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>

					<tr><td>Data</td><td>
					
					Dal <input type="date" id="arco_dal" name="arco_dal" /> al <input type="date" id="arco_al" name="arco_al" /></td><td><span class="hint_tooltip" style="cursor:pointer;" title="Modalità acquisti: si considera la data di acquisto. Modalità no acquisto o modalità tutti: si considera la data di registrazione dell\'account." ><img src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Aiuto" /></span></td></tr>
					
					<tr><td>Modalit&agrave;</td><td>
					<input type='radio' value='0' name='modalita' checked='checked' /> In ingresso &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
					
					<input type='radio' value='1' name='modalita' /> In uscita &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
					<input type='radio' value='2' name='modalita' /> Entrambe
					
					<tr><td></td><td><input type='submit' name='cercaiclienti' value='Cerca clienti' class='button' /></td><td></td></tr>
				</table>
			</form>

		</div>
	</form>
</div>

{elseif isset($smarty.post.esportamailtxt)}

<div id="container-marketing-telefonate">
	<div class="form-horizontal col-lg-12">
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-phone"></i> Analisi telefonate
			</div>

			<strong>Mail in formato TXT:</strong>

			{$esportamailtxt_html} {*  Variabile che sostituisce gli echo finchè non viene portato tutto in bootstrap *}

			<form method="post" name="form_indietro" id="form_indietro">
				<button type="button" class="btn btn-secondary" name="nuova_analisi" onclick="window.location = window.location.href;">Nuova analisi</button>
			</form>
		</div>
	</div>
</div>

{else}

<div id="container-marketing-telefonate">
	<div class="form-horizontal col-lg-12">
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-phone"></i> Analisi telefonate
			</div>

			{$riepilogo_html} {*  Variabile che sostituisce gli echo finchè non viene portato tutto in bootstrap *}

			<form method="post" name="form_indietro" id="form_indietro">
				<button type="button" class="btn btn-secondary" name="nuova_analisi" onclick="window.location = window.location.href;">Nuova analisi</button>
			</form>
		</div>
	</div>
</div>

{/if}

{/block}
