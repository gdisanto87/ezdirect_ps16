{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

<script type='text/javascript'>
	function refreshPlanner(newsrc) {
		var ifr = document.getElementById('planner');
		ifr.src = newsrc;
	}
</script>

<div id="container-planner">
	{*<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate>*}

		{* TEST Ultime attività *}
		<div class="panel">
			<div class="panel-heading">
				Ultime attività
			</div>
			<div class="row">
			{$ultime_attivita}
			</div>
		</div>

		{* Pulsanti *}
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> Opzioni
			</div>
			<div class="row">

				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><span class="nav-label"><i class="icon-plus-sign verde" title="Aggiungi"></i> Aggiungi nuova azione staff</span></a>
						<ul class="dropdown-menu">
							<li class="dropdown-submenu"> {* correggere: alt="ticket" ??? *}
								<li><a href="{$link_azione}"><img src="../img/admin/return.gif" alt="To-Do" title="To-Do" />&nbsp;&nbsp;&nbsp;To-do</a></li>
								{foreach $todo_li as $li}
								<li><a href="{$link_azione}&tipo-todo={$li['tipo']}"><img src="../img/admin/icons/{$li['icona']}.gif" alt="{$li['title']}" title="{$li['title']}" /> {$li['nome']}</a></li>
								{/foreach}
							</li>
						</ul>
					</li>
				</ul>

				<a class="btn btn-primary" id="togglePlanner">NASCONDI/MOSTRA IL PLANNER</a>

				{if $check_employee}
					| Vedi planner di: 
					<button class='btn' onclick='refreshPlanner("http://46.101.235.11/ezadmin/planner/wdCalendar-master-clean/sample.php?id_employee=4")'>Massimo</button>  - 
					<button class='btn' onclick='refreshPlanner("http://46.101.235.11/ezadmin/planner/wdCalendar-master-clean/sample.php?id_employee=5")'>Paolo</button> - 
					<button class='btn' onclick='refreshPlanner("http://46.101.235.11/ezadmin/planner/wdCalendar-master-clean/sample.php?id_employee=12")'>Lorenzo</button> -
					<button class='btn' onclick='refreshPlanner("http://46.101.235.11/ezadmin/planner/wdCalendar-master-clean/sample.php?id_employee=17")'>Leonardo</button> -
					<button class='btn' onclick='refreshPlanner("http://46.101.235.11/ezadmin/planner/wdCalendar-master-clean/sample.php?id_employee=26")'>Dorinel</button>
				{/if}
			</div>
		</div>

		{* Planner *}
		<div class="panel">
			<div class="panel-heading"> {* Correggere con employee di cui sto guardando il planner *}
				<i class="icon-calendar"></i> Planner di {$employee}
			</div>

			{* Correggere: uso il server di prova per testare; sostituire con https://www.ezdirect.it/ezadmin/... *}
			{* Installazione pulita e funzionante *}
			<iframe id='planner' src='http://46.101.235.11/ezadmin/planner/wdCalendar-master-clean/sample.php?id_employee={$id_employee}' style='border:0px; width:100%; height:620px; overflow:hide; {if $hidden_planner}display:none{else}display:block{/if}'>
				<base target='_parent' />
			</iframe>
			{* File modificati e non funzionanti *}
			{*<iframe id='planner' src='http://46.101.235.11/ezadmin/planner/wdCalendar-master/sample.php?id_employee={$id_employee}' style='border:0px; width:100%; height:620px; overflow:hide; {if $hidden_planner}display:none{else}display:block{/if}'>
				<base target='_parent' />
			</iframe>*}

		</div>

		{* Le mie attività *}
		<div class="panel">
			<div class="panel-heading">
				Le mie attività
			</div>
			{* Errore: il form di ricerca va in conflitto con i form all'interno della tabella - spostato sotto *}
			{*<div class="row">
				<form method="post">
					<div class="form-group col-md-8">
						<div class="form-control-static col-md-8">
							<label for="cercaticket">Ricerca testo nel ticket</label>
							<input class="textbox-fix" type="text" id="cercaticket" name="cercaticket">
						</div>
						<div class="form-control-static col-md-4">
							<button type="submit" name="vaicercaticket" class="btn btn-secondary">Cerca</button>
							<button type="submit" name="vairesetcerca" class="btn btn-secondary rosso">Resetta</button>
						</div>
					</div>
				</form>
			</div>*}
			<div class="row">
				<table class="table" id="tabella_mie_attivita">
					<thead>
						<tr>
							<th {*class="filtro"*}><span class="title_box ">{l s='ID Action'}</span></th>
							<th><span class="title_box ">{l s='Status'}</span></th>
							<th><span class="title_box ">{l s='Edit status'}</span></th>
							<th><span class="title_box ">{l s='In charge'}</span></th>
							<th><span class="title_box ">{l s='Opened by'}</span></th>
							<th><span class="title_box ">{l s='Type'}</span></th>
							<th {*class="filtro"*}><span class="title_box ">{l s='Customer'}</span></th>
							<th><span class="title_box ">{l s='Group'}</span></th>
							<th><span class="title_box ">{l s='Priority'}</span></th>
							<th><span class="title_box ">{l s='Date'}</span></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						{foreach $mie_att AS $att}
						<tr class="pointer">
							<td>{$att['ticket']}</td>
							<td data-sort="{$att['stato_sort']}">{$att['stato_icona']}</td>
							<td data-sort="{$att['stato_sort']}">{$att['status_edit']}</td>
							<td data-sort="{$att['employee']}">{$att['in_carico_edit']}</td> {*sort employee o id?*}
							<td>{$att['aperto_da']}</td>
							<td>{$att['contact_type']}</td>
							<td><a href="{$link->getAdminLink('AdminCustomers')}&id_customer={$att['id_customer']}&viewcustomer">{$att['customer']}</a></td>
							<td>{$att['gruppo_clienti']}</td>
							<td>{$att['priority']}</td>
							<td>{$att['data_act']}</td>
							<td>
								<a class="btn btn-default" style="border-radius:0px;" href="{$att['href']}" title="Vedi">
                            		<i class='icon-search'></i> {l s='View'}
								</a>
								{if !$check_admin2}
								<a class="btn btn-danger" style="border-radius:0px;" href="{$att['href_cancella']}" onclick="return confirm('Sei sicuro/a?')" title="Cancella">
									<i class='icon-trash'></i> {*l s='Delete'*}
								</a>
								{/if}
							</td>
						</tr>
						{/foreach}
					</tbody>
					<tfoot>
						<tr style="display:none;">
							<th {*class="filtro"*}><span class="title_box ">{l s='ID Action'}</span></th>
							<th><span class="title_box ">{l s='Status'}</span></th>
							<th><span class="title_box ">{l s='Edit status'}</span></th>
							<th><span class="title_box ">{l s='In charge'}</span></th>
							<th><span class="title_box ">{l s='Opened by'}</span></th>
							<th><span class="title_box ">{l s='Type'}</span></th>
							<th {*class="filtro"*}><span class="title_box ">{l s='Customer'}</span></th>
							<th><span class="title_box ">{l s='Group'}</span></th>
							<th><span class="title_box ">{l s='Priority'}</span></th>
							<th><span class="title_box ">{l s='Date'}</span></th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>

		{* Ricerca *}
		<div class="panel">
			<div class="panel-heading">
				Ricerca
			</div>
			<div class="row">
				<form name="ricerca" method="post">
					<div class="form-group col-md-8">
						<div class="form-control-static col-md-8">
							<label for="cercaticket">Ricerca testo nel ticket</label>
							<input class="textbox-fix" type="text" id="cercaticket" name="cercaticket" value="{$cercaticket}">
						</div>
						<div class="form-control-static col-md-4">
							<button type="submit" name="vaicercaticket" class="btn btn-secondary">Cerca</button>
							<button type="submit" name="vairesetcerca" class="btn btn-secondary rosso">Resetta</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		
	{*</form>*}
</div>


{/block}
