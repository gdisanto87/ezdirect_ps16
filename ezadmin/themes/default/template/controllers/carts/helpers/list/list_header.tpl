{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/list/list_header.tpl"}

{block name='override_header'}

	<script>
	$(document).ready(function () {
		$('.select2mul').select2();
	});
	</script>
	
	<div id="container-header">
		<form class="form-horizontal col-lg-12" method="post">

			<div class="panel">
				<div class="panel-heading">
					<i class="icon-truck"></i> Ricerca per prodotto
				</div>
				<div class="row">
					<div class="form-group col-md-8">
						<label>Cerca carrello in base al codice prodotto</label>
						<p class="form-control-static">
							<select id="cercacarrelloprodotto" name="cercacarrelloprodotto" class="form-control textbox-fix select2">
								<option value="">-- Seleziona --</option>
								{if count($prodotti)}
									{foreach $prodotti AS $row}
									<option value="{$row['id']}" {if $cercacarrelloprodotto == $row['id']}selected{/if}>{$row['name']} ({$row['reference']}) - ID {$row['id']}</option>
									{/foreach}
								{/if}
							</select>
						</p>
					</div>

					<div class="form-group col-md-4">
						<label>Ricerca testuale</label>
						<p class="form-control-static">
							<input class="textbox-fix h-std" type="text" id="cercatestoprodottocarrello" name="cercatestoprodottocarrello" value="{$cercatestoprodottocarrello}">
						</p>
					</div>
				</div>
				
				<p>Per cercare più prodotti, usare la parola "or" nella cella di testo libero. Es: per cercare carrelli che contengono astec o S412 o ez308 scrivere "astec or s412 or ez308".</p>

				<button type="submit" name="cerca" class="btn btn-secondary">Cerca</button>
				<button type="submit" name="reset_cerca" class="btn btn-secondary rosso">Resetta</button>
			</div>

		</form>
	</div>

{/block}
