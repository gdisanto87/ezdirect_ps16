{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

{assign var="link_gen_base" value="index.php?controller=AdminLogisticaAmazon&amp;"}
{assign var="link_gen_home" value="index.php?controller=AdminLogisticaAmazon"}
{assign var="token_base" value="&amp;token={getAdminToken tab='AdminLogisticaAmazon'}"}

<div id="container-logistica-amazon">
	<form class="form-horizontal col-lg-12"method="post" enctype="multipart/form-data" novalidate>
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-truck"></i> Logistica Amazon
			</div>

			<a class="btn btn-primary" onclick="window.onbeforeunload=null" href="{$path_file_confronto}">SCARICA FILE DI CONFRONTO LOGISTICA AMAZON-MAGAZZINO ESOLVER</a>
			<a class="btn btn-primary" onclick="window.onbeforeunload=null" href="{$path_file_quantita}">SCARICA FILE CON QUANTITÀ DA INVIARE A LOGISTICA AMAZON</a>
		</div>
	</form>
</div>


{/block}
