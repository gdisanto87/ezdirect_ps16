{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

{assign var="link_gen_base" value="index.php?controller="}
{assign var="token_base" value="&amp;token={getAdminToken tab='AdminOreFerieImpiegati'}"}
{*$is_agente = 1*}
{*$not_mio_agente = 1*}

<div id="container-customer">

	<nav class="navbar navbar-default">
		<div class="container-fluid">
			{* Questo è necessario per far funzionare la modalità mobile *}
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>

			<div class="collapse navbar-collapse" id="navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="{$link_gen_base}AdminEmployees&amp;token={getAdminToken tab='AdminEmployees'}">Impiegati</a></li>
                    <li><a href="{$link_gen_base}AdminOreFerieImpiegati&amp;tab_name=ore{$token_base}">Ore</a></li>
                    <li><a href="{$link_gen_base}AdminOreFerieImpiegati&amp;tab_name=ferie{$token_base}">Ferie</a></li>
                    <li><a href="{$link_gen_base}AdminAgenti&amp;token={getAdminToken tab='AdminAgenti'}">Agenti</a></li>
                </ul>
            </div>
			
		</div>
	</nav>

	
	<div id="container-customer">

		<div class="row">
			
			<div class="col-lg-12">
				
				{if ! isset($smarty.get.tab_name)}
				
				{else if ($smarty.get.tab_name == 'ferie')}
					<strong>Totale anno corrente</strong>
					
					<table class="table">
					
					<tr><th>Dipendente</th>
					
					<th  style="text-align:right">Ferie godute<br />(giorni)</th><th  style="text-align:right">Ferie pianificate<br />(giorni)</th><th style="text-align:right">Ferie residue<br />(giorni) *</th><th style="text-align:right">Permessi goduti<br />(ore)</th><th style="text-align:right">Permessi pianificati<br />(ore)</th><th style="text-align:right">Permessi residui<br />(ore)</th><th style="text-align:right">Certificati</th></tr>
					
				
					{foreach $impiegati AS $key => $impiegato}

						<tr>
						<td>{$impiegato['nome']}</td>
						<td style="text-align:right">{$impiegato['ferie_godute']}</td>
						<td style="text-align:right">{$impiegato['ferie_pianificate']}</td>
						<td style="text-align:right">{$impiegato['ferie_residue']}</td>
						<td style="text-align:right">{$impiegato['permessi_goduti']}</td>
						<td style="text-align:right">{$impiegato['permessi_pianificati']}</td>
						<td style="text-align:right">{$impiegato['permessi_residui']}</td>
						<td style="text-align:right">{$impiegato['certificati']}</td>
						</tr>
					{/foreach}
					</table>
				{else if ($smarty.get.tab_name == 'ore')}
					
					<script src="/js/handsontable.full.js"></script>
					<link rel="stylesheet" media="screen" href="/css/handsontable.full.css">
					
					<form action="" method="post">
					<select name="month-ore" style="display:block; float:left; width:500px; height:35px">
					<option value="1" {if ($smarty.post['month-ore']) && $smarty.post['month-ore'] == '1'}selected="selected"{else if !($smarty.post['month-ore']) && ($smarty.now|date_format:"%n") == 1}selected="selected"{/if}>Gennaio</option>
					<option value="2" {if ($smarty.post['month-ore']) && $smarty.post['month-ore'] == '2'}selected="selected"{else if !($smarty.post['month-ore']) && ($smarty.now|date_format:"%n") == 2}selected="selected"{/if}>Febbraio</option>
					<option value="3" {if ($smarty.post['month-ore']) && $smarty.post['month-ore'] == '3'}selected="selected"{else if !($smarty.post['month-ore']) && ($smarty.now|date_format:"%n") == 3}selected="selected"{/if}>Marzo</option>
					<option value="4" {if ($smarty.post['month-ore']) && $smarty.post['month-ore'] == '4'}selected="selected"{else if !($smarty.post['month-ore']) && ($smarty.now|date_format:"%n") == 4}selected="selected"{/if}>Aprile</option>
					<option value="5" {if ($smarty.post['month-ore']) && $smarty.post['month-ore'] == '5'}selected="selected"{else if !($smarty.post['month-ore']) && ($smarty.now|date_format:"%n") == 5}selected="selected"{/if}>Maggio</option>
					<option value="6" {if ($smarty.post['month-ore']) && $smarty.post['month-ore'] == '6'}selected="selected"{else if !($smarty.post['month-ore']) && ($smarty.now|date_format:"%n") == 6}selected="selected"{/if}>Giugno</option>
					<option value="7" {if ($smarty.post['month-ore']) && $smarty.post['month-ore'] == '7'}selected="selected"{else if !($smarty.post['month-ore']) && ($smarty.now|date_format:"%n") == 7}selected="selected"{/if}>Luglio</option>
					<option value="8" {if ($smarty.post['month-ore']) && $smarty.post['month-ore'] == '8'}selected="selected"{else if !($smarty.post['month-ore']) && ($smarty.now|date_format:"%n") == 8}selected="selected"{/if}>Agosto</option>
					<option value="9" {if ($smarty.post['month-ore']) && $smarty.post['month-ore'] == '9'}selected="selected"{else if !($smarty.post['month-ore']) && ($smarty.now|date_format:"%n") == 9}selected="selected"{/if}>Settembre</option>
					<option value="10" {if ($smarty.post['month-ore']) && $smarty.post['month-ore'] == '10'}selected="selected"{else if !($smarty.post['month-ore']) && ($smarty.now|date_format:"%n") == 10}selected="selected"{/if}>Ottobre</option>
					<option value="11" {if ($smarty.post['month-ore']) && $smarty.post['month-ore'] == '11'}selected="selected"{else if !($smarty.post['month-ore']) && ($smarty.now|date_format:"%n") == 11}selected="selected"{/if}>Novembre</option>
					<option value="12" {if ($smarty.post['month-ore']) && $smarty.post['month-ore'] == '12'}selected="selected"{else if !($smarty.post['month-ore']) && ($smarty.now|date_format:"%n") == 12}selected="selected"{/if}>Dicembre</option>
					
					
					</select>
					&nbsp;&nbsp;&nbsp;
					<select name="year-ore" style="display:block; float:left; width: 250px; height:35px">
					<option value="2014" {if ($smarty.post['year-ore']) && $smarty.post['year-ore'] == '2014'}selected="selected"{/if}>2014</option>
					<option value="2015"  {if ($smarty.post['year-ore']) && $smarty.post['year-ore'] == '2015'}selected="selected"{/if} >2015</option>
					<option value="2016"  {if ($smarty.post['year-ore']) && $smarty.post['year-ore'] == '2016'}selected="selected"{/if} >2016</option>
					<option value="2017"  {if ($smarty.post['year-ore']) && $smarty.post['year-ore'] == '2017'}selected="selected"{/if}>2017</option>
					
					<option value="2018"  {if ($smarty.post['year-ore']) && $smarty.post['year-ore'] == '2018'}selected="selected"{/if} >2018</option>
					
					<option value="2019"  {if ($smarty.post['year-ore']) && $smarty.post['year-ore'] == '2019'}selected="selected"{/if}>2019</option>
					<option value="2020"  {if ($smarty.post['year-ore']) && $smarty.post['year-ore'] == '2020'}selected="selected"{/if}>2020</option>
					<option value="2021"  {if (($smarty.post['year-ore']) && $smarty.post['year-ore'] == '2021') || !($smarty.post['year-ore'])}selected="selected"{/if}>2021</option>
					</select>
					&nbsp;&nbsp;&nbsp;
					<input type="submit" value="Seleziona" name="select-month-year" class="button" style="display:block; float:left; margin-top:0px; cursor:pointer; height:35px" />
					<div style="clear:both"></div>
					<br /><br />
					
					<script type="text/javascript">
					$(document).ready(function () {

					  var
						data = [{$rowImpiegati}],
						container = document.getElementById("example"),
						hot;
						
						  
						 var
						$$ = function(id) {
						  return document.getElementById(id);
						},

						save = $$("save");
						{literal}
			
						  hot = new Handsontable(container, {
						colWidths: [150, 27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27],
						data: data,
						minSpareRows: 0,
			
						{/literal}
						
						colHeaders: ["{$month} {$year}", {$days_sequence}],
						//rowHeaders: [{$rowHeaders}],
						className: "htLeft",
						manualColumnResize: true,
						manualRowResize: true,
						{literal}
						afterLoadData: function(){
								var rowHeaders = container.querySelectorAll(".rowHeader");
									
									for (var j = 0; j < rowHeaders.length; j++) {
										rowHeaders[j].style.width = "200px";
									}
								
							}
						  });
						
							 Handsontable.Dom.addEvent(save, "click", function (e) {
						
						{/literal}
						{$data_to_save}
						{$data_to_save_all}
					{literal}	
					
						$("#save_dati").html("<img src=\"../img/loader.gif\" />").show();
						$.post("ajax.php", {submitOre:1,dati_da_salvare:data_to_save_all}, function (r) {
								
								if (r == "ok")
									$("#save_dati").html("<b style=\"color:green\">Dati salvati correttamente</b>").fadeIn(400);
								else
									$("#save_dati").html("<b style=\"color:red\">Errore: dati non validi</b>").fadeIn(400);
								
							});
							
							 e.preventDefault(); 
						});
						
					  
					  function bindDumpButton() {
					  
						  Handsontable.Dom.addEvent(document.body, "click", function (e) {
							
							var element = e.target || e.srcElement;
							if (element.nodeName == "BUTTON" && element.name == "dump") {
							  var name = element.getAttribute("data-dump");
							  var instance = element.getAttribute("data-instance");
							  var hot = window[instance];
							  console.log("data of " + name, hot.getData());
							}
						  });
						}
					  bindDumpButton();

					});
								
					{/literal}
					</script>
					<script type="text/javascript">
					{literal}
							function exportExcel()
							{	
								var month_ore_xls = $("#month_ore_xls").val();
								var year_ore_xls = $("#year_ore_xls").val();
								var month_name_ore_xls = $("#month_name_ore_xls").val();
								$.post({"ajax.php", {esporta_ore_excel:1,month_ore_xls:month_ore_xls,year_ore_xls:year_ore_xls,month_name_ore_xls:month_name_ore_xls}, success:function (r) {
									
								},
								  error: function(xhr,stato,errori){
									 alert("Errore durante l\'operazione:"+xhr.status);
								  }
								  });
								
								
							}
						{/literal}
					</script>	
					<div class="handsontable" id="example"></div>
					
					 <p><br />
					  <form method="post">
					  <input type="hidden" id="month-ore-xls" name="month-ore-xls" value="{$month}" />
					  <input type="hidden" id="year-ore-xls" name="year-ore-xls" value="{$year}" />
					  <input type="hidden" id="month-name-ore-xls" name="month-name-ore-xls" value="{$month_name}" />
							<button name="save" id="save" class="button" style="cursor:pointer">Salva dati</button>
						</form>
						
						<form method="post" action="ajax.php" onSubmit="exportExcel(); return false;">
						<input type="hidden" id="month_ore_xls" name="month_ore_xls" value="{$month}" />
					  <input type="hidden" id="year_ore_xls" name="year_ore_xls" value="{$year}" />
					  <input type="hidden" id="month_name_ore_xls" name="month_name_ore_xls" value="{$month_name}" />
						<input type="submit" name="esporta_ore_excel" id="esporta_ore_excel" class="button" value="Esporta dati in formato Excel" style="cursor:pointer; height:27px; margin-top:-4px" />
						<!-- <label><input name="autosave" id="autosave" checked="checked" autocomplete="off" type="checkbox"> Autosave</label> -->
					  </p>
					 </form>
					 <div id="save_dati">
					 </div>
				{/if}
			</div>

		</div>
	</div>


	
{/block}

