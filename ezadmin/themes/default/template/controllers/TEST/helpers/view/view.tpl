{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

{assign var="link_gen_base" value="index.php?controller=AdminCustomers&amp;id_customer={$customer->id}&amp;viewcustomer&amp;"}
{assign var="token_base" value="&amp;token={getAdminToken tab='AdminCustomers'}"}
{*$is_agente = 1*}
{*$not_mio_agente = 1*}

{if $not_mio_agente == 1}
<div id="container-customer">
	<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-warning-sign"></i> {l s='Access denied'}
			</div>

			<h2 class="text-muted text-center">
				{l s='You do not have the necessary permissions to view this customer.'}
			</h2>
		</div>
	</form>
</div>
{else}
<div id="container-customer">

	<nav class="navbar navbar-default">
		<div class="container-fluid">
			{* Questo è necessario per far funzionare la modalità mobile *}
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li {if $tab_name == "tickets"}class="active"{/if}><a href="{$link_gen_base}tab_name=tickets{$token_base}">Tickets (0 / 0)</a></li>
					<li {if $tab_name == "actions"}class="active"{/if}><a href="{$link_gen_base}tab_name=actions{$token_base}">Azioni cliente ({if $count_azioni_cliente_aperte > 0} <span class="rosso">{$count_azioni_cliente_aperte}</span>{else}{$count_azioni_cliente_aperte}{/if} / {$count_azioni_cliente})</a></li>
					<li {if $tab_name == "todo"}class="active"{/if}><a href="{$link_gen_base}tab_name=todo{$token_base}">TO DO (0 / 0)</a></li>
					{if $is_agente == 0}<li {if $tab_name == "stats"}class="active"{/if}><a href="{$link_gen_base}tab_name=stats{$token_base}">Stat</a></li>{/if}
					{if $is_agente == 0}<li {if $tab_name == "contracts"}class="active"{/if}><a href="{$link_gen_base}tab_name=contracts{$token_base}">Contratti (0 / 0)</a></li>{/if}
					<li {if $tab_name == "bdl"}class="active"{/if}><a href="{$link_gen_base}tab_name=bdl{$token_base}">BDL ({$count_bdl_aperti} / {$count_bdl_totali})</a></li>
					{if $is_agente == 0}<li {if $tab_name == "mail"}class="active"{/if}><a href="{$link_gen_base}tab_name=mail{$token_base}">Mail</a></li>{/if}
					{if $is_agente == 0}<li {if $tab_name == "calls"}class="active"{/if}><a href="{$link_gen_base}tab_name=calls{$token_base}">Telefonate ({$count_telefonate_ricevute}-{$count_telefonate_inviate})</a></li>{/if}
					<li {if ! isset($tab_name) OR $tab_name == "customerdetails"}class="active"{/if}><a href="{$link_gen_base}{$token_base}">Anagrafica</a></li>
					<li {if $tab_name == "addresses"}class="active"{/if}><a href="{$link_gen_base}tab_name=addresses{$token_base}">Indirizzi</a></li>
					{if $customer->id_default_group != 2}<li {if $tab_name == "people"}class="active"{/if}><a href="{$link_gen_base}tab_name=people{$token_base}">Persone ({$count_persone})</a></li>{/if}
					<li {if $tab_name == "administration"}class="active"{/if}><a href="{$link_gen_base}tab_name=administration{$token_base}">Amministrazione</a></li>
					<li {if $tab_name == "orders"}class="active"{/if}><a href="{$link_gen_base}tab_name=orders{$token_base}">Ordini ({$count_ordini})</a></li>
					<li {if $tab_name == "carts"}class="active"{/if}><a href="{$link_gen_base}tab_name=carts{$token_base}">Carrelli ({$count_carrelli})</a></li>
					<li {if $tab_name == "invoices"}class="active"{/if}><a href="{$link_gen_base}tab_name=invoices{$token_base}">Fatture ({$count_fatture})</a></li>
					{if $is_agente == 0}<li {if $tab_name == "ezcloud"}class="active"{/if}><a href="{$link_gen_base}tab_name=ezcloud{$token_base}">Ezcloud</a></li>{/if}
					<li {if $tab_name == "documents"}class="active"{/if}><a href="{$link_gen_base}tab_name=documents{$token_base}">Documenti ({$count_documenti_dir}-{$count_documenti_files})</a></li>
				</ul>
			</div>
			
		</div>
	</nav>

	<div class="row">
		{if $riassunto_on}
		<div class="col-lg-12">

			<div class="panel">
				<div class="panel-heading">
					<i class="icon-home"></i>
					{l s='Summary'}
					
					<div class="panel-heading-action">
						<a class="btn btn-default squadrato" href="">
							<i class="icon-save"></i>
							{l s='Save'}
						</a>
					</div>
				</div>
				<div class="form-row">
					<div class="row">
						
						<div class="form-group col-md-2">
							<label for="customer" class="summary">{l s='Customer'}</label>
							<p class="form-control-static">
								{if isset($riassunto['cliente'])}
									{if $customer->active == 0 OR $customer->active == 2}
										<span class="rosso">{$riassunto['cliente']}</span>
									{else}
										{$riassunto['cliente']}
									{/if}
								{else}
									{l s='Unknown'}
								{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="codes" class="summary">{l s='Spring / CRM / eSolver'}</label>
							<p class="form-control-static">
								{if isset($riassunto['codici'])}{$riassunto['codici']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="profile" class="summary">{l s='Profile'}</label>
							<p class="form-control-static">
								{if isset($riassunto['profilo'])}{$riassunto['profilo']}{else}{l s='Unknown'}{/if}
								<span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="{$riassunto['tipo_profilo_title']}">
									{$riassunto['tipo_profilo']}
								</span>
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="customer_type" class="summary">{l s='Customer type'}</label>
							<p class="form-control-static">
								{if isset($riassunto['tipo_cliente'])}{$riassunto['tipo_cliente']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="status" class="summary">{l s='Status'}</label>
							<p class="form-control-static">
								{if isset($riassunto['status'])}
									{if $customer->active == 1}
									<span class="verde">{$riassunto['status']}</span>
									{else}
									<span class="rosso">{$riassunto['status']}</span>
									{/if}
								{else}
								{l s='Unknown'}
								{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="registration" class="summary">{l s='Registration'}</label>
							<p class="form-control-static">
								{if isset($riassunto['registrazione'])}{$riassunto['registrazione']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

					</div>
					<div class="row">
						
						<div class="form-group col-md-2">
							<label for="referent" class="summary">{l s='Referent'}</label>
							<p class="form-control-static">
								{*if isset($riassunto['referente'])}{$riassunto['referente']}{else}{l s='Unknown'}{/if*}
								<select class="form-control" id="select_referent">
									<option value="">{l s='Select Referent'}</option>
									{if count($riassunto['referenti'])}
										{foreach $riassunto['referenti'] AS $key => $referenti}
										<option value="{$referenti['id']}"{if $referenti['id'] == $riassunto['referente']['id']} selected="selected"{/if}>{$referenti['id']} - {$referenti['name']}</option>
										{/foreach}
									{/if}
								</select>
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="zone" class="summary">{l s='Zone'}</label>
							<p class="form-control-static">
								{if isset($riassunto['zona'])}{$riassunto['zona']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="orders_6" class="summary">{l s='Orders (6 months)'}</label>
							<p class="form-control-static">
								{if isset($riassunto['ord_6'])}{$riassunto['ord_6']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="orders_12" class="summary">{l s='Orders (1 year)'}</label>
							<p class="form-control-static">
								{if isset($riassunto['ord_12'])}{$riassunto['ord_12']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="tot_ord" class="summary">{l s='Total orders'}</label>
							<p class="form-control-static">
								{if isset($riassunto['ord_tot'])}{$riassunto['ord_tot']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="last_pur" class="summary">{l s='Last purchase'}</label>
							<p class="form-control-static">
								{if isset($riassunto['ultimo_acq'])}{$riassunto['ultimo_acq']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

					</div>
					<div class="row">
						
						<div class="form-group col-md-2">
							<label for="agent" class="summary">{l s='Agent'}</label>
							<p class="form-control-static">
								<select class="form-control" id="select_agent">
									<option value="">{l s='Select Agent'}</option>
									{if count($riassunto['impiegati'])}
										{foreach $riassunto['impiegati'] AS $key => $impiegato}
										<option value="{$impiegato['id']}"{if $impiegato['id'] == $riassunto['agente']['id']} selected="selected"{/if}>{$impiegato['id']} - {$impiegato['name']}</option>
										{/foreach}
									{/if}
								</select>
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="technician" class="summary">{l s='Technician'}</label>
							<p class="form-control-static">
								{*if isset($riassunto['tecnico'])}{$riassunto['tecnico']}{else}{l s='Unknown'}{/if*}
								<select class="form-control" id="select_tech">
									<option value="">{l s='Select Technician'}</option>
									{if count($riassunto['tecnici'])}
										{foreach $riassunto['tecnici'] AS $key => $tecnico}
										<option value="{$tecnico['id']}"{if $tecnico['id'] == $riassunto['tecnico']['id']} selected="selected"{/if}>{$tecnico['id']} - {$tecnico['name']}</option>
										{/foreach}
									{/if}
								</select>
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="visits" class="summary">{l s='Visits'}</label>
							<p class="form-control-static">
								{if isset($riassunto['visite'])}{$riassunto['visite']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="pages_visited" class="summary">{l s='Pages visited'}</label>
							<p class="form-control-static">
								{if isset($riassunto['pagine_viste'])}{$riassunto['pagine_viste']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="last_product" class="summary">{l s='Last product'}</label>
							<p class="form-control-static">
								
								{if isset($riassunto['ultimo_prod'])}
									{if isset($riassunto['ultimo_prod_title'])}
									<span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="{$riassunto['ultimo_prod_title']}">
										{$riassunto['ultimo_prod']}
									</span>
									{else}
									<p class="form-control-static">{$riassunto['ultimo_prod']}</p>
									{/if}
								{else}
								<p class="form-control-static">{l s='Unknown'}</p>
								{/if}
								
							</p>
						</div>

					</div>
					<div class="row">

						<div class="form-group col-md-2">
							<label for="last_ch_ric" class="summary">{l s='Last ch. ric.'}</label>
							<p class="form-control-static">
								{if isset($riassunto['ultima_ch_ric'])}{$riassunto['ultima_ch_ric']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="last_ch_inv" class="summary">{l s='Last ch. inv.'}</label>
							<p class="form-control-static">
								{if isset($riassunto['ultima_ch_inv'])}{$riassunto['ultima_ch_inv']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="last_action" class="summary">{l s='Last action'}</label>
							<p class="form-control-static">
								{if isset($riassunto['ultima_azione'])}{$riassunto['ultima_azione']}{else}{l s='Unknown'}{/if}
							</p>
						</div>


					</div>
				</div>
			</div>
		</div>
		{/if}

		{if isset($alert)}
		<div class="col-lg-12">
			<div class="row">
				<div class="alert alert-danger">{$alert}</div>
			</div>
		</div>
		{/if}
		
		<div class="col-lg-12">

						

			<div class="row">
				{*}
				<div class="col-lg-2 col-md-3">
					<div class="list-group list-group-horizontal-lg">
						

						<a class="list-group-item{if ! isset($tab_name) OR $tab_name == "customerdetails"} active{/if}" href="{$link_gen_base}{$token_base}">{l s='Customer details'}</a>
						<a class="list-group-item{if $tab_name == "administration"} active{/if}" href="{$link_gen_base}tab_name=administration{$token_base}">{l s='Administration'}</a>
						<a class="list-group-item{if $tab_name == "addresses"} active{/if}" href="{$link_gen_base}tab_name=addresses{$token_base}">{l s='Addresses'}</a>
						<a class="list-group-item{if $tab_name == "people"} active{/if}" href="{$link_gen_base}tab_name=people{$token_base}">{l s='People'}</a>
						<a class="list-group-item{if $tab_name == "invoices"} active{/if}" href="{$link_gen_base}tab_name=invoices{$token_base}">{l s='Invoices'}</a>
						<a class="list-group-item{if $tab_name == "carts"} active{/if}" href="{$link_gen_base}tab_name=carts{$token_base}">{l s='Carts'}</a>
						<a class="list-group-item{if $tab_name == "orders"} active{/if}" href="{$link_gen_base}tab_name=orders{$token_base}">{l s='Orders'}</a>
						<a class="list-group-item{if $tab_name == "viewedproduct"} active{/if}" href="{$link_gen_base}tab_name=viewedproduct{$token_base}">{l s='Viewed product'}</a>
						<a class="list-group-item{if $tab_name == "privatenote"} active{/if}" href="{$link_gen_base}tab_name=privatenote{$token_base}">{l s='Private note'}</a>
						<a class="list-group-item{if $tab_name == "messages"} active{/if}" href="{$link_gen_base}tab_name=messages{$token_base}">{l s='Messages'}</a>
						<a class="list-group-item{if $tab_name == "vouchers"} active{/if}" href="{$link_gen_base}tab_name=vouchers{$token_base}">{l s='Vouchers'}</a>
						<a class="list-group-item{if $tab_name == "lastconnections"} active{/if}" href="{$link_gen_base}tab_name=lastconnections{$token_base}">{l s='Last connections'}</a>
						<a class="list-group-item{if $tab_name == "groups"} active{/if}" href="{$link_gen_base}tab_name=groups{$token_base}">{l s='Groups'}</a>
						{if $customer->isGuest()}
						<a class="list-group-item{if $tab_name == "guest"} active{/if}" href="{$link_gen_base}tab_name=guest{$token_base}">{l s='Guest'}</a>
						{/if}
						<a class="list-group-item{if $tab_name == "others"} active{/if}" href="{$link_gen_base}tab_name=others{$token_base}">{l s='Others'}</a>
					</div>
				</div>
				{*}

{*CUSTOMER DETAILS*}
				{if ! isset($tab_name)}
				<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-user"></i>
							{* Verifica se il cliente ha impostato un nome azienda e regola l'interfaccia di conseguenza *}
							{l s='Customer details: '}
							{if isset($customer->company)}
								{$customer->firstname}
								{$customer->lastname}
							{else}
								{$customer->company}
								({$customer->firstname}
								{$customer->lastname})
							{/if}
							[{$customer->id|string_format:"%06d"}]
							-
							<a href="mailto:{$customer->email}"><i class="icon-envelope"></i>
								{$customer->email}
							</a>
							<div class="panel-heading-action">
								<a class="btn btn-default" href="{$current|escape:'html':'UTF-8'}&amp;updatecustomer&amp;id_customer={$customer->id|intval}&amp;token={$token|escape:'html':'UTF-8'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">

									<i class="icon-edit"></i>
									{l s='Edit'}
								</a>
							</div>
						</div>
						<div class="form-row">
							<div class="row">
								
								<div class="form-group col-md-4">
									<label for="company">{l s='Company'}</label>
									<p class="form-control-static">
										{if isset($customer->company)}{$customer->company}{else}{l s='Unknown'}{/if}
									</p>
								</div>
								
								<div class="form-group col-md-4">
									<label for="firstname">{l s='Firstname'}</label>
									<p class="form-control-static">
										{if isset($customer->firstname)}{$customer->firstname}{else}{l s='Unknown'}{/if}
									</p>
								</div>
								
								<div class="form-group col-md-4">
									<label for="lastname">{l s='Lastname'}</label>
									<p class="form-control-static">
										{if isset($customer->lastname)}{$customer->lastname}{else}{l s='Unknown'}{/if}
									</p>
								</div>

							</div>

							<div class="row">

								<div class="form-group col-md-4">
									<label for="esolver_code">{l s='eSolver Code'}</label>
									<p class="form-control-static">
										{if isset($customer->codice_esolver)}{$customer->codice_esolver}{else}{l s='Unknown'}{/if}
									</p>
								</div>
								
								<div class="form-group col-md-4">
									<label for="vat_code">{l s='VAT Code'}</label>
									<p class="form-control-static">
										{if isset($customer->vat_number)}{$customer->vat_number}{else}{l s='Unknown'}{/if}
									</p>
								</div>
								
								<div class="form-group col-md-4">
									<label for="tax_code">{l s='Tax Code'}</label>
									<p class="form-control-static">
										{if isset($customer->tax_code)}{$customer->tax_code}{else}{l s='Unknown'}{/if}
									</p>
								</div>

							</div>

							<div class="row">
								
								<div class="form-group col-md-4">
									<label for="email">{l s='Email'} <a href="mailto:{$customer->email}"><i class="icon-envelope"></i> {l s='Send'}</a></label>
									<p class="form-control-static">
										{if isset($customer->email)}{$customer->email}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								<div class="form-group col-md-4">
									<label for="pec">{l s='PEC'}</label>
									<p class="form-control-static">
										{if isset($customer->pec)}{$customer->pec}{else}{l s='Unknown'}{/if}
									</p>
								</div>
								
								<div class="form-group col-md-4">
									<label for="telephone">{l s='Telephone'}</label>
									<p class="form-control-static">
										{if isset($customer->telefono_principale)}{$customer->telefono_principale}{else}{l s='Unknown'}{/if}
									</p>
								</div>

							</div>

							<div class="row">
								
								<div class="form-group col-md-4">
									<label for="mobile">{l s='Mobile Phone'}</label>
									<p class="form-control-static">
										{if isset($customer->cellulare_principale)}{$customer->cellulare_principale}{else}{l s='Unknown'}{/if}
									</p>
								</div>
								
								<div class="form-group col-md-4">
									<label for="fax">{l s='Fax'}</label>
									<p class="form-control-static">
									{* Controllare quale address prendere per il fax *}
										{foreach $addresses AS $key => $address}
											{if isset($address['fax'])}
												{$address['fax']}
											{else}
												{l s='Unknown'}
											{/if}
										{/foreach}
									</p>
								</div>

								<div class="form-group col-md-4">
									<label for="new_forced_psw">{l s='New forced password'}</label>
									<p class="form-control-static">
										{* Potrebbe essere messo solo in edit (forse corrisponde a "password" di edit) *}
										<input type="text" size="33" name="passwd" value="" disabled>
									</p>
								</div>

							</div>

							<div class="row">
								
								<div class="form-group col-md-4">
								{* Codice destinatario per fatturazione elettronica (SDI) *}
									<label for="recipient_code">{l s='Recipient Code (SDI)'}</label>
									<p class="form-control-static">
										{if isset($customer->codice_univoco)}{$customer->codice_univoco}{else}{l s='Unknown'}{/if}
									</p>
								</div>
								
								<div class="form-group col-md-4">
									<label for="ipa">{l s='IPA (Electronic Invoicing Code for PA)'}</label>
									<p class="form-control-static">
										{if isset($customer->ipa)}{$customer->ipa}{else}{l s='Unknown'}{/if}
									</p>
								</div>
								
								<div class="form-group col-md-4">
									<label for="tax_regime">{l s='Tax Regime'}</label>
									<p class="form-control-static">
										{if isset($customer->tax_regime)}
											{if $customer->tax_regime == 0}
											    {l s='Base (Italy: VAT 22%)'}
											{elseif $customer->tax_regime == 2}
												{l s='VAT 10%'}
											{elseif $customer->tax_regime == 3}
												{l s='VAT 4%'}
											{elseif $customer->tax_regime == 1}
												{l s='Exempt'}
											{elseif $customer->tax_regime == 4}
												{l s='Split Payment with VAT 22%'}
											{/if}
										{else}
											{l s='Unknown'}
										{/if}
									</p>
									{* Questo va nel form di modifica:
									<select class="form-control" id="tax_regime" readonly>
										<option value="0">{l s='Base (Italy: VAT 22%)'}</option>
										<option value="2">{l s='VAT 10%'}</option>
										<option value="3">{l s='VAT 4%'}</option>
										<option value="1">{l s='Exempt'}</option>
										<option value="4">{l s='Split Payment with VAT 22%'}</option>
									</select>
									*}
								</div>

							</div>

							<div class="row">

								<div class="form-group col-md-4">
									<label for="employees">{l s='Employees'}</label>
									<p class="form-control-static">
										{if isset($customer->employees_number)}{$customer->employees_number}{else}{l s='Unknown'}{/if}
									</p>
								</div>
								

								<div class="form-group col-md-4">
									<label for="cust_prod_sector">{l s='Customer\'s product sector'}</label>
									<p class="form-control-static">
										{if isset($customer->settore)}{$customer->settore}{else}{l s='Unknown'}{/if}
									</p>

								</div>
								
								<div class="form-group col-md-4">
									<label for="most_purch_cat">{l s='Most purchased category'}</label>
									<p class="form-control-static">
										{if isset($customer->categoria)}{$customer->categoria}{else}{l s='Unknown'}{/if}
									</p>
								</div>

							</div>

							<div class="row">

								<div class="form-group col-md-4">
									<label for="channel">{l s='Channel'}</label>
									<p class="form-control-static">
										{if isset($customer->canale)}{$customer->canale}{else}{l s='Unknown'}{/if}
									</p>
								</div>
								
								<div class="form-group col-md-4">
									<label for="last_contact">{l s='Last contact'}</label>
									<p class="form-control-static">
										{if isset($customer->ultimo_contatto)}{$customer->ultimo_contatto}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								<div class="form-group col-md-4">
									<label for="profile">{l s='Profile'}</label>
									<p class="form-control-static">
										{if isset($customer->id_default_group)}{$groups[0]['name']}{else}{l s='Unknown'}{/if}
									</p>
								</div>
								
							</div>

							<!--<hr />-->

							<div class="row">

								<div class="form-group col-md-2">
									<label for="customer_type">{l s='Customer type'}</label>
									<p class="form-control-static">
										{if ! isset($customer->is_company)}
											<span class="label label-default" style="border-radius:0px;">
												<i class="icon-question"></i>
												{l s='Unknown'}
											</span>
										{elseif $customer->is_company == 1}
											<span class="label label-primary" style="border-radius:0px;">
												<i class="icon-building"></i>
												{l s='Company'}
											</span>
										{elseif $customer->is_company == 0}
											<span class="label label-primary" style="border-radius:0px;">
												<i class="icon-user"></i>
												{l s='Individual'}
											</span>
										{/if}
									</p>
									
								</div>

								<div class="form-group col-md-2">
									<label for="supplier">{l s='Supplier'}</label>
									<p class="form-control-static">
										{if ! isset($customer->supplier)}
											<span class="label label-default" style="border-radius:0px;">
												<i class="icon-question"></i>
												{*l s='Unknown'*}
											</span>
										{elseif $customer->supplier == 1 }
											<span class="label label-primary" style="border-radius:0px;">
												<i class="icon-check"></i>
												{*l s='Yes'*}
											</span>
										{elseif $customer->supplier == 0}
											<span class="label label-primary" style="border-radius:0px;">
												<i class="icon-remove"></i>
												{*l s='No'*}
											</span>
										{/if}
									</p>
									
								</div>

								<div class="form-group col-md-2">
									<label for="risk_customer">{l s='Risk customer'}</label>
									<p class="form-control-static">
										{if ! isset($customer->id_risk)}
											<span class="label label-default" style="border-radius:0px;">
												<i class="icon-question"></i>
											</span>
										{elseif $customer->id_risk == 1}
											<span class="label label-danger" style="border-radius:0px;">
												<i class="icon-check"></i>
											</span>
										{elseif $customer->id_risk == 0}
											<span class="label label-success" style="border-radius:0px;">
												<i class="icon-remove"></i>
											</span>
										{/if}
									</p>
									
								</div>

								<div class="form-group col-md-2">
									<label for="order_notifications">{l s='Email activity notification'}</label>
									<p class="form-control-static">
										{if ! isset($customer->order_notifications)}
											<span class="label label-default" style="border-radius:0px;">
												<i class="icon-question"></i>
												{*l s='Unknown'*}
											</span>
										{elseif $customer->order_notifications == 0}
											<span class="label label-primary" style="border-radius:0px;">
												<i class="icon-check"></i>
												{*l s='Yes'*}
											</span>
										{elseif $customer->order_notifications == 1}
											<span class="label label-primary" style="border-radius:0px;">
												<i class="icon-remove"></i>
												{*l s='No'*}
											</span>
										{/if}
									</p>
									
								</div>

								<div class="form-group col-md-2">
									<label for="gender">{l s='Gender'}</label>
									<p class="form-control-static">
										{if ! isset($customer->id_gender) || $customer->id_gender == 9}
											<span class="label label-default" style="border-radius:0px;">
												<i class="icon-question"></i>
												{*l s='Unknown'*}
											</span>
										{elseif $customer->id_gender == 1}
											<span class="label label-primary" style="border-radius:0px;">
												<i class="icon-male"></i>
												{*l s='Male'*}
											</span>
										{elseif $customer->id_gender == 2}
											<span class="label label-primary" style="border-radius:0px;">
												<i class="icon-female"></i>
												{*l s='Female'*}
											</span>
										{/if}
									</p>
									
								</div>
								
								<div class="form-group col-md-2">
									<label for="status">{l s='Status'}</label>
									<p class="form-control-static">
										{if ! isset($customer->active) }
											<span class="label label-default squadrato">
												<i class="icon-question"></i>
												{*l s='Unknown'*}
											</span>
										{elseif $customer->active == 1}
											<span class="label label-tooltip label-primary squadrato" data-toggle="tooltip" data-html="true" title="{l s='Active'}">
											{*}<span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="{l s='Activity discontinued'}">{*}
												<i class="icon-check"></i>
												{*l s='Active'*}
											</span>
										{elseif $customer->active == 0}
											<span class="label label-tooltip label-danger squadrato" data-toggle="tooltip" data-html="true" title="{l s='Inactive'}">
												<i class="icon-remove"></i>
												{*l s='Inactive'*}
											</span>
										{elseif $customer->active == 2}
											<span class="label label-tooltip label-warning squadrato" data-toggle="tooltip" data-html="true" title="{l s='Activity discontinued'}">
												<i class="icon-remove-sign"></i>
												{*l s='Activity discontinued'*}
											</span>
										{/if}
									</p>
									
								</div>

								
								{*
								<div class="form-group col-md-1">
									<label for="risk_customer"></label>
									<p class="form-control-static">
										{if ! isset($customer->risk_customer)}
											<span class="label label-default">
												<i class="icon-question"></i>
												{l s='Unknown'}
											</span>
										{elseif $customer->risk_customer == 0 }
											<span class="label label-success">
												<i class="icon-check"></i>
												{l s='Yes'}
											</span>
										{elseif $customer->risk_customer == 1}
											<span class="label label-danger">
												<i class="icon-remove"></i>
												{l s='No'}
											</span>
										{/if}
									</p>
									
								</div>
								*}

								{*
								<div class="form-group col-md-3">
									<label for="recipient_code">{l s='Recipient Code (SDI)'}</label>
									<input type="text" class="form-control"  value="{if isset($customer->recipient_code)}{$customer->recipient_code}{else}{l s='Unknown'}{/if}" readonly>
								</div>
								*}


								
								
							</div>

							<div class="row">
								<div class="form-group col-md-2">
									<label for="consents">{l s='Consents'}</label>
									<p class="form-control-static">
										{if ! isset($customer->consenso_1)}
											<span class="label label-default" style="border-radius:0px;">
												<i class="icon-question"></i>
												{l s='Base Consent'}
											</span>
										{elseif $customer->consenso_1 == 1}
											<span class="label label-primary" style="border-radius:0px;">
												<i class="icon-check"></i>
												{l s='Base Consent'}
											</span>
										{elseif $customer->consenso_1 == 0}
											<span class="label label-primary" style="border-radius:0px;">
												<i class="icon-remove"></i>
												{l s='Base Consent'}
											</span>
										{/if}
										<br />
										{if ! isset($customer->consenso_2)}
											<span class="label label-default" style="border-radius:0px;">
												<i class="icon-question"></i>
												{l s='Marketing'}
											</span>
										{elseif $customer->consenso_2 == 1}
											<span class="label label-primary" style="border-radius:0px;">
												<i class="icon-check"></i>
												{l s='Marketing'}
											</span>
										{elseif $customer->consenso_2 == 0}
											<span class="label label-primary" style="border-radius:0px;">
												<i class="icon-remove"></i>
												{l s='Marketing'}
											</span>
										{/if}
										<br />
										{if ! isset($customer->consenso_3)}
											<span class="label label-default" style="border-radius:0px;">
												<i class="icon-question"></i>
												{l s='3rd Marketing'}
											</span>
										{elseif $customer->consenso_3 == 1}
											<span class="label label-primary" style="border-radius:0px;">
												<i class="icon-check"></i>
												{l s='3rd Marketing'}
											</span>
										{elseif $customer->consenso_3 == 0}
											<span class="label label-primary" style="border-radius:0px;">
												<i class="icon-remove"></i>
												{l s='3rd Marketing'}
											</span>
										{/if}
									</p>
									
								</div>

								<div class="form-group col-md-2">
									<label for="birthday">{l s='Birthday (Age)'}</label>
									<p class="form-control-static">
										{if isset($customer->birthday) && $customer->birthday != '0000-00-00'}
											{l s='%1$s (%2$d years old)' sprintf=[$customer_birthday, $customer_stats['age']]}
										{else}
											{l s='Unknown'}
										{/if}
									</p>
								</div>

								<div class="form-group col-md-2">
									<label for="registration_date">{l s='Registration Date'}</label>
									<p class="form-control-static">
										{if ! isset($registration_date) }
											{l s='Unknown'}
										{else}
											{$registration_date}
										{/if}
									</p>
								</div>

								<div class="form-group col-md-2">
									<label for="last_visit">{l s='Last Visit'}</label>
									<p class="form-control-static">
										{if $customer_stats['last_visit']}
											{$last_visit}
										{else}
											{l s='Never'}
										{/if}
									</p>
								</div>

								<div class="form-group col-md-2">
									<label for="last_update">{l s='Last Update'}</label>
									<p class="form-control-static">
										{if isset($last_update)}
											{$last_update}
										{else}
											{l s='Unknown'}
										{/if}
									</p>
								</div>

								<div class="form-group col-md-2">
									<label for="language">{l s='Language'}</label>
									<p class="form-control-static">
										{if isset($customerLanguage->name)}
											{$customerLanguage->name}
										{else}
											{l s='Unknown'}
										{/if}
									</p>
								</div>

							</div>

						</div>
						{if $customer->isGuest()}
							{l s='This customer is registered as a Guest.'}
							{if !$customer_exists}
							<form method="post" action="index.php?tab=AdminCustomers&amp;id_customer={$customer->id|intval}&amp;token={getAdminToken tab='AdminCustomers'}">
								<input type="hidden" name="id_lang" value="{$id_lang}" />
								<p class="text-center">
									<input class="button" type="submit" name="submitGuestToCustomer" value="{l s='Transform to a customer account'}" />
								</p>
								{l s='This feature generates a random password before sending an email to your customer.'}
							</form>
							{else}
							<p class="text-muted text-center">
								{l s='A registered customer account using the defined email address already exists. '}
							</p>
							{/if}
						{/if}
					</div>
				</form>


				<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate name="note">
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-eye-close"></i> {l s='Private notes'}

							<div class="panel-heading-action">
								<a class="btn btn-default" href="{$current|escape:'html':'UTF-8'}&amp;updatecustomer&amp;id_customer={$customer->id|intval}&amp;token={$token|escape:'html':'UTF-8'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">

									<i class="icon-plus-sign"></i>
									{l s='Add private note'}
								</a>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-lg-7">
									<p class="thick">{l s='Note'}</p>
								</div>
								<div class="col-lg-2">
									<p class="thick">{l s='Created by'}</p>
								</div>
								<div class="col-lg-2">
									<p class="thick">{l s='Last edit'}</p>
								</div>
								<div class="col-lg-1">
								</div>
							</div>
							{foreach $customer_note as $key => $nota_privata}
							<div class="row">
								<input id="id_nota" name="id_nota" type="hidden" value="{$nota_privata['id_note']}">
								<div class="col-lg-7">
									<textarea readonly>{$nota_privata['note']}</textarea>
								</div>
								<div class="col-lg-2">
									{$nota_privata['creato_da']}
								</div>
								<div class="col-lg-2">
									{$nota_privata['date_upd']}
								</div>
								<div class="col-lg-1">
									<button type="submit" id="submitCustomerNote" class="btn btn-default pull-right">
										<i class="icon-trash"></i>
									</button>
								</div>
							</div>
							{/foreach}

							<hr />

							<div class="row">
								<div class="col-lg-7">
									<textarea></textarea>
								</div>
								<div class="col-lg-2">
									<p>username</p>
								</div>
								<div class="col-lg-2">
									<p>data attuale</p>
								</div>
								<div class="col-lg-1">
									
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<button type="submit" id="submitCustomerNote" class="btn btn-default pull-right">
									<i class="icon-save"></i>
									{l s='Save'}
								</button>
							</div>
						</div>
						
					</div>
				</form>


				{* Note private originali ( vale su un solo record? ) *}
				{*}
				<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-eye-close"></i> {l s='Add a private note'}
						</div>
						<div class="alert alert-info">{l s='This note will be displayed to all employees but not to customers.'}</div>
						<form id="customer_note" class="form-horizontal" action="ajax.php" method="post" onsubmit="saveCustomerNote({$customer->id|intval});return false;" >
							<div class="form-group">
								<div class="col-lg-12">
									<textarea name="note" id="noteContent" onkeyup="$('#submitCustomerNote').removeAttr('disabled');">{$customer_note}</textarea>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<button type="submit" id="submitCustomerNote" class="btn btn-default pull-right" disabled="disabled">
										<i class="icon-save"></i>
										{l s='Save'}
									</button>
								</div>
							</div>
							<span id="note_feedback"></span>
						</form>
					</div>
				</form>
				{*}
				

{*ADMINISTRATION*}
				{elseif $tab_name == "administration"}
				<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" name="product" novalidate name="{$current|escape:'html':'UTF-8'}">
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-legal"></i>
							{l s='Administration'}
							
							<div class="panel-heading-action">
								{*<a class="btn btn-default" href="{$link_gen_base}tab_name=administration&updatecustomer{$token_base}&amp;back={$smarty.server.REQUEST_URI|urlencode}">*}
								{assign var="back" value="&amp;token={getAdminToken tab='AdminCustomers'}"}
								<a class="btn btn-default" href="{$current|escape:'html':'UTF-8'}&amp;updatecustomer&amp;tab_name=administration&amp;id_customer={$customer->id|intval}&amp;token={$token|escape:'html':'UTF-8'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
								
									<i class="icon-edit"></i>
									{l s='Edit'}
								</a>
							</div>
						</div>
						<div class="form-row">

							<div class="row">
								
								<div class="form-group col-md-2">
									<label for="annual_revenue">{l s='Annual revenue'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['fatturato_annuo'])}{$amministrazione['fatturato_annuo']}{else}{l s='Unknown'}{/if}
									</p>
								</div>
								
								<div class="form-group col-md-2">
									<label for="total_revenue">{l s='Total revenue'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['fatturato_totale'])}{$amministrazione['fatturato_totale']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								<div class="form-group col-md-4">
									<div class="panel">
										<div class="panel-heading">
											{l s='Credit request requirements:'}
											{if ! isset($amministrazione['fido_ok'])}
												<span class="label label-default squadrato"><i class="icon-question"></i> {l s='Unknown'}</span>
											{elseif $amministrazione['fido_ok'] == 1}
												<span class="label label-success squadrato"><i class="icon-check"></i> {l s='Yes'}</span>
											{elseif $amministrazione['fido_ok'] == 0}
												<span class="label label-danger squadrato"><i class="icon-remove"></i> {l s='No'}</span>
											{/if}
											
											<span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="{l s='at least € 1500 of orders or 1 order of € 1000 net in the last 3 months'}">
												<i class="icon-question-sign"></i>
											</span>
										</div>
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="net_three_months">{l s='Net last 3 months'}</label>
												{if isset($amministrazione['acquisti_tre_mesi'])}
												{$amministrazione['acquisti_tre_mesi']}
												{else}
												<p class="form-control-static">{l s='Unknown'}</p>
												{/if}
											</div>
											
			
											<div class="form-group col-md-6">
												<label for="total_revenue">{l s='Order > 1000 Eur ?'}</label>
												{if isset($amministrazione['ordine_mille'])}
													{if $amministrazione['ordine_mille']}
														<span class="label label-primary squadrato"><i class="icon-check"></i> {*l s='Yes'*}</span>
													{else}
														<span class="label label-danger squadrato"><i class="icon-remove"></i> {*l s='No'*}</span>
													{/if}
												{else}
													<span class="label label-default squadrato"><i class="icon-question"></i> {*l s='Unknown'*}</span>
												{/if}
											</div>
										</div>
										<br />
									</div>
								</div>
								
							</div>

							<div class="row">

								<div class="form-group col-md-1">
									<label for="web_code">{l s='Web code'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['id_customer'])}{$amministrazione['id_customer']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Tipo soggetto *}
								<div class="form-group col-md-2">
									<label for="subject_type">{l s='Subject type'}</label>
									<p class="form-control-static">

										{if isset($amministrazione['tipo_soggetto'])}
											{* 1 = società di capitali*}
											{if $amministrazione['tipo_soggetto'] == 1}
											{l s='Capital company'}
											{* 2 = società di persone*}
											{elseif $amministrazione['tipo_soggetto'] == 2}
											{l s='Company of people'}
											{* 3 = persona fisica*}
											{elseif $amministrazione['tipo_soggetto'] == 3}
											{l s='Physical person'}
											{* 4 = altro*}
											{elseif $amministrazione['tipo_soggetto'] == 4}
											{l s='Other'}
											{else}
											{l s='ERR: Subject not found'}
											{/if}
										{else}
										{l s='Unknown'}
										{/if}
									</p>
								</div>

								{* Codice fornitore *}
								{if isset($amministrazione['codice_fornitore'])}
								<div class="form-group col-md-2">
									<label for="supp_code">{l s='Supplier code'}</label>
									<p class="form-control-static">
										{$amministrazione['codice_fornitore']}
									</p>
								</div>
								{/if}

								{* Payment *}
								<div class="form-group col-md-2">
									<label for="payment">{l s='Payment'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['pagamento'])}{$amministrazione['pagamento']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Fido *}
								<div class="form-group col-md-1">
									<label for="trust">{l s='Trust'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['fido'])}{$amministrazione['fido']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Fido residuo *}
								<div class="form-group col-md-2">
									<label for="residual_trust">{l s='Residual trust'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['fido_residuo'])}{$amministrazione['fido_residuo']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Assicurazione credito *}
								{*
								Si = tutto il fido è assicurato con Coface
								No o vuoto = il fido non è assicurato
								(N) = il fido è assicurato per N euro
								Non affidabile = richiesta non accettata da Coface
								*}
								<div class="form-group col-md-2">
									<label for="credit_insurance">{l s='Credit insurance'}
										<span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="Si = tutto il fido è assicurato con Coface<br />No o vuoto = il fido non è assicurato<br />(N) = il fido è assicurato per N euro<br />Non affidabile = richiesta non accettata da Coface">
											<i class="icon-question-sign"></i>
										</span>
									</label>
									<p class="form-control-static">
										{if isset($amministrazione['assicurato'])}{$amministrazione['assicurato']}{else}{l s='Unknown'}{/if}
										
									</p>
								</div>


							</div>

							<div class="row">
								{* Data affidamento *}
								<div class="form-group col-md-2">
									<label for="renewal_date">{l s='Assignment date'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['data_fido'])}{$amministrazione['data_fido']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Data rinnovo *}
								<div class="form-group col-md-2">
									<label for="renewal_date">{l s='Renewal date'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['data_rinnovo'])}{$amministrazione['data_rinnovo']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Data revoca *}
								<div class="form-group col-md-2">
									<label for="revocation_date">{l s='Revocation date'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['data_revoca'])}{$amministrazione['data_revoca']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Motivo revoca / Non affidato *}
								<div class="form-group col-md-3">
									<label for="reason_revocation">{l s='Reason for revocation / Not entrusted'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['motivo_revoca'])}{$amministrazione['motivo_revoca']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Non addebitare commissioni su pagamento differito *}
								<div class="form-group col-md-3">
									<label for="fees_def_pay">{l s='Do not charge fees on deferred payment'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['no_addebito_commissioni'])}{$amministrazione['no_addebito_commissioni']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

							</div>

							<div class="row">
								{* Trasporto a mezzo *}
								<div class="form-group col-md-2">
									<label for="renewal_date">{l s='Transport by'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['trasporto_a_mezzo'])}{$amministrazione['trasporto_a_mezzo']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Consegna (porto) *}
								<div class="form-group col-md-2">
									<label for="delivery_p">{l s='Delivery (port)'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['consegna'])}{$amministrazione['consegna']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Vettore *}
								<div class="form-group col-md-4">
									<label for="vector">{l s='Vector'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['vettore'])}{$amministrazione['vettore']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* ID corriere cliente *}
								<div class="form-group col-md-4">
									<label for="customer_courier_id">{l s='Customer courier ID'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['id_corriere'])}{$amministrazione['id_corriere']}{else}{l s='Unknown'}{/if}
									</p>
								</div>
							</div>

							<div class="row">
								{* Trasporto assicurato *}
								<div class="form-group col-md-2">
									<label for="insured_transp">{l s='Insured transport'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['trasporto_assicurato'])}
											{* net_three_months dovrebbe essere una variabile valuta *}
											{if $amministrazione['trasporto_assicurato']}
												<span class="label label-primary squadrato"><i class="icon-check"></i> {*l s='Yes'*}</span>
											{else}
												<span class="label label-warning squadrato"><i class="icon-remove"></i> {*l s='No'*}</span>
											{/if}
										{else}
											<span class="label label-default squadrato"><i class="icon-question"></i> {*l s='Unknown'*}</span>
										{/if}
									</p>
								</div>

								{* Fermo deposito *}
								<div class="form-group col-md-2">
									<label for="stat_storage">{l s='Stationary storage'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['fermo_deposito'])}{$amministrazione['fermo_deposito']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Trasporto gratis da *}
								<div class="form-group col-md-4">
									<label for="free_transp_from">{l s='Free transport from'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['trasporto_gratis_da'])}{$amministrazione['trasporto_gratis_da']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Note consegna *}
								<div class="form-group col-md-4">
									<label for="delivery_notes">{l s='Delivery notes'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['note_consegna'])}{$amministrazione['note_consegna']}{else}{l s='Unknown'}{/if}
									</p>
								</div>
							</div>

							<hr />

							<div class="row">
								{* IBAN *}
								<div class="form-group col-md-4">
									<label for="iban">{l s='IBAN'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['iban'])}{$amministrazione['iban']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Swift *}
								<div class="form-group col-md-4">
									<label for="swift">{l s='Swift'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['swift'])}{$amministrazione['swift']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* PayPal *}
								<div class="form-group col-md-4">
									<label for="paypal_email">{l s='PayPal'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['paypal'])}{$amministrazione['paypal']}{else}{l s='Unknown'}{/if}
									</p>
								</div>
							</div>

							<div class="row">
								{* IVA agevolata *}
								<div class="form-group col-md-4">
									<label for="reduced_vat">{l s='Reduced VAT'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['iva_agevolata'])}{$amministrazione['iva_agevolata']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Esportatore abituale *}
								<div class="form-group col-md-4">
									<label for="reg_exporter">{l s='Regular exporter'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['esportatore_abituale'])}
											{if $amministrazione['esportatore_abituale']}
												<span class="label label-primary squadrato"><i class="icon-check"></i> {*l s='Yes'*}</span>
											{else}
												<span class="label label-primary squadrato"><i class="icon-remove"></i> {*l s='No'*}</span>
											{/if}
										{else}
											<span class="label label-default squadrato"><i class="icon-question"></i> {*l s='Unknown'*}</span>
										{/if}
									</p>
								</div>

							</div>


							<div class="row">
								{* Sconto extra *}
								<div class="form-group col-md-4">
									<label for="extra_discount">{l s='Extra discount'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['sconto_extra'])}{$amministrazione['sconto_extra']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Per ordini da *}
								<div class="form-group col-md-4">
									<label for="for_order_from">{l s='For orders from'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['per_ordini_da'])}{$amministrazione['per_ordini_da']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Fino a (data) *}
								<div class="form-group col-md-4">
									<label for="until_date">{l s='Until (date)'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['fino_a'])}{$amministrazione['fino_a']}{else}{l s='Unknown'}{/if}
									</p>
								</div>
							</div>

							<div class="row">
								{* Rebate *}
								<div class="form-group col-md-4">
									<label for="rebate">{l s='Rebate'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['rebate'])}{$amministrazione['rebate']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Fatturato annuo *}
								<div class="form-group col-md-4">
									<label for="annual_revenue">{l s='Annual revenue'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['fatturato_annuo'])}{$amministrazione['fatturato_annuo']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Blacklist *}
								<div class="form-group col-md-4">
									<label for="blacklist">{l s='Blacklist'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['blacklist'])}
											{if $amministrazione['blacklist']}
												<span class="label label-danger squadrato"><i class="icon-check"></i> {*l s='Yes'*}</span>
											{else}
												<span class="label label-success squadrato"><i class="icon-remove"></i> {*l s='No'*}</span>
											{/if}
										{else}
											<span class="label label-default squadrato"><i class="icon-question"></i> {*l s='Unknown'*}</span>
										{/if}
									</p>
								</div>
							</div>

							<div class="row">
								{* Agente *}
								<div class="form-group col-md-4">
									<label for="agent">{l s='Agent'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['agente']['name'])}{$amministrazione['agente']['name']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Tecnico *}
								<div class="form-group col-md-4">
									<label for="technician">{l s='Technician'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['tecnico']['name'])}{$amministrazione['tecnico']['name']}{else}{l s='Unknown'}{/if}
									</p>
								</div>

								{* Installatore *}
								<div class="form-group col-md-4">
									<label for="installer">{l s='Installer'}</label>
									<p class="form-control-static">
										{if isset($amministrazione['installatore'])}{$amministrazione['installatore']}{else}{l s='Unknown'}{/if}
									</p>
								</div>
							</div>



							{*if $count_better_customers != '-'}
								<div class="row">
									<label class="control-label col-lg-3">{l s='Best Customer Rank'}</label>
									<div class="col-lg-9">
										<p class="form-control-static">{$count_better_customers}</p>
									</div>
								</div>
							{/if}
							{if $shop_is_feature_active}
								<div class="row">
									<label class="control-label col-lg-3">{l s='Shop'}</label>
									<div class="col-lg-9">
										<p class="form-control-static">{$name_shop}</p>
									</div>
								</div>
							{/if*}
							{* Da sistemare, i consent sono tre e di diverso tipo *}
							{*
							<div class="row">
								<label class="control-label col-lg-3">{l s='Registrations'}</label>
								<div class="col-lg-9">
									<p class="form-control-static">
										{if $customer->newsletter}
											<span class="label label-success">
												<i class="icon-check"></i>
												{l s='Newsletter'}
											</span>
										{else}
											<span class="label label-danger">
												<i class="icon-remove"></i>
												{l s='Newsletter'}
											</span>
										{/if}
										&nbsp;
										{if $customer->optin}
											<span class="label label-success">
												<i class="icon-check"></i>
												{l s='Opt in'}
											</span>
											{else}
											<span class="label label-danger">
												<i class="icon-remove"></i>
												{l s='Opt in'}
											</span>
										{/if}
									</p>
								</div>
							</div>
							*}
						</div>

						{if $customer->isGuest()}
							{l s='This customer is registered as a Guest.'}
							{if !$customer_exists}
							<form method="post" action="index.php?tab=AdminCustomers&amp;id_customer={$customer->id|intval}&amp;token={getAdminToken tab='AdminCustomers'}">
								<input type="hidden" name="id_lang" value="{$id_lang}" />
								<p class="text-center">
									<input class="button" type="submit" name="submitGuestToCustomer" value="{l s='Transform to a customer account'}" />
								</p>
								{l s='This feature generates a random password before sending an email to your customer.'}
							</form>
							{else}
							<p class="text-muted text-center">
								{l s='A registered customer account using the defined email address already exists. '}
							</p>
							{/if}
						{/if}

					</div>
				</form>

{*ADDRESSES*}
				{elseif $tab_name == "addresses"}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					{* display hook specified to this page : AdminCustomers *}
					{hook h="displayAdminCustomers" id_customer=$customer->id|intval}
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-map-marker"></i> {l s='Addresses'} <span class="badge">{count($addresses)}</span>

							<div class="panel-heading-action">
							{* Premendo il tasto "add" si viene reindirizzati alla creazione di un nuovo "customer" con id_customer_associate che sarebbe l'id del customer a cui associare la nuova anagrafica *}
							{* index.php?controller=AdminCustomers *}
								<a class="btn btn-default" href="{$current|escape:'html':'UTF-8'}&amp;addcustomer&amp;id_customer_associate={$customer->id|intval}&amp;token={getAdminToken tab='AdminCustomers'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
									<i class="icon-plus-sign"></i>
									{l s='Add'}
								</a>
							</div>
						</div>
						
						{if count($addresses)}
							<table class="table">
								<thead>
									<tr>
										<th><span class="title_box ">{l s='Type'}</span></th>
										<th><span class="title_box ">{l s='Company'}</span></th>
										<th><span class="title_box ">{l s='Name'}</span></th>
										<th><span class="title_box ">{l s='Address'}</span></th>
										<th><span class="title_box ">{l s='Zip/Postal Code'}</span></th>
										<th><span class="title_box ">{l s='City'}</span></th>
										<th><span class="title_box ">{l s='State'}</span></th>
										<th><span class="title_box ">{l s='Country'}</span></th>
										<th><span class="title_box ">{l s='Phone number(s)'}</span></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									{foreach $addresses AS $key => $address}
									<tr>
										<td>{if $address['fatturazione']}{l s='Fatturazione'}{else}{l s='Consegna'}{/if}</td>
										<td>{if $address['company']}{$address['company']}{else}--{/if}</td>
										<td>{$address['firstname']} {$address['lastname']}</td>
										<td>{$address['address1']} {if $address['address2']}{$address['address2']}{/if}</td>
										<td>{$address['postcode']}</td>
										<td>{$address['city']}</td>
										<td>{$address['state']}</td>
										<td>{$address['country']}</td>
										<td>
											{if $address['phone']}
												{$address['phone']}
												{if $address['phone_mobile']}<br />{$address['phone_mobile']}{/if}
											{else}
												{if $address['phone_mobile']}<br />{$address['phone_mobile']}{else}--{/if}
											{/if}
										</td>
										<td class="text-right">
											<div class="btn-group">
												<a class="btn btn-default" style="border-radius:0px;" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;addaddress=1&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
													<i class="icon-edit"></i> {* l s='Edit' *}
												</a>
												{* <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> *}
												{*	<span class="caret"></span> *}
												{* </button> *}
												{* <ul class="dropdown-menu"> *}
													{* <li> *}
												
												<a class="btn btn-danger" style="border-radius:0px;" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;deleteaddress&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
													<i class="icon-trash"></i>
													{* l s='Delete' *}
												</a>
													{* </li> *}
												{* </ul> *}
											</div>
										</td>
									</tr>
									{/foreach}
								</tbody>
							</table>
						{else}
							<p class="text-muted text-center">
								{l s='%1$s %2$s has not registered any addresses yet' sprintf=[$customer->firstname, $customer->lastname]}
							</p>
						{/if}
					</div>
				</form>

{*PEOPLE*}
				{elseif $tab_name == "people"}
				{if $customer->id_default_group != 2}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-file"></i> {l s='People'}
							{if count($persone) != 0 OR TRUE}
							<span class="badge" style="border-radius:0px;">
								{count($persone)}
							</span>
							{/if}

							<div class="panel-heading-action">
							{* Premendo il tasto "add" si viene reindirizzati alla creazione di un nuovo "customer" con id_customer_associate che sarebbe l'id del customer a cui associare la nuova anagrafica *}
							{* index.php?controller=AdminCustomers *}
								<a class="btn btn-default" href="{$current|escape:'html':'UTF-8'}&amp;addcustomer&amp;id_customer_associate={$customer->id|intval}&amp;token={getAdminToken tab='AdminCustomers'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
									<i class="icon-plus-sign"></i>
									{l s='Add'}
								</a>
							</div>
						</div>

						
						{* OR TRUE è per debug, togliere in produzione*}
						{if count($persone)}
							{*assign var=persone_n value=count($persone)*}
							<table class="table">
								<thead>
									<tr>
										<th class="center"><span class="title_box ">{l s='ID'}</span></th>
										<th><span class="title_box">{l s='Name'}</span></th>
										<th><span class="title_box">{l s='Surname'}</span></th>
										<th><span class="title_box">{l s='Role'}</span></th>
										<th><span class="title_box">{l s='Telephone 1'}</span></th>
										<th><span class="title_box">{l s='Telephone 2'}</span></th>
										<th><span class="title_box">{l s='Mobile Phone'}</span></th>
										<th><span class="title_box">{l s='Email'}</span></th>
										<th></th>
									</tr>
								</thead>
								<tbody>

								{foreach $persone AS $key => $persona}
									<tr onclick="document.location = '?tab=AdminOrders&amp;id_order={$order['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}'">
										<td>{$persona['id_persona']}</td>
										<td>{$persona['firstname']}</td>
										<td>{$persona['lastname']}</td>
										<td>{$persona['role']}</td>
										<td>{$persona['phone']}</td>
										<td>{$persona['phone_2']}</td>
										<td>{$persona['phone_mobile']}</td>
										<td>
											<a class="btn btn-default" style="border-radius:0px;" href="?tab=AdminOrders&amp;id_order={$order['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}">
												<i class='icon-edit'></i>
											</a>
											<a class="btn btn-default" style="border-radius:0px;" href="?tab=AdminOrders&amp;id_order={$order['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}">
												<i class='icon-envelope'></i>
											</a>
											<a class="btn btn-default" style="border-radius:0px;" href="?tab=AdminOrders&amp;id_order={$order['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}">
												<i class='icon-windows'></i>
											</a>
											<a class="btn btn-danger" style="border-radius:0px;" href="?tab=AdminOrders&amp;id_order={$order['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}">
												<i class='icon-trash'></i>
											</a>
										</td>
									</tr>
								{/foreach}
								</tbody>
							</table>
							
						{else}
						<p class="text-muted text-center">
							{*l s='%1$s %2$s has not placed any orders yet' sprintf=[$customer->firstname, $customer->lastname]*}
							{l s='There are no associated people'}
						</p>
						{/if}
					</div>
				</form>
				{/if}

{*INVOICES*}
				{elseif $tab_name == "invoices"}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-file"></i> {l s='Invoices'}
							{if count($invoices) != 0}
							<span class="badge">
								{count($invoices)}
							</span>
							{/if}
							
							<div class="panel-heading-action">
								<a class="btn btn-default" href="{$current|escape:'html':'UTF-8'}&amp;updatecustomer&amp;id_customer={$customer->id|intval}&amp;token={$token|escape:'html':'UTF-8'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
									<i class="icon-plus-sign"></i>
									{l s='Add'}
								</a>
							</div>
						</div>
						{if $invoices AND count($invoices) OR TRUE}
						{*
							{assign var=count_ok value=count($orders_ok)}
							{assign var=count_ko value=count($orders_ko)}
							<div class="panel">
								<div class="row">
									<div class="col-lg-6">
										<i class="icon-ok-circle icon-big"></i>
										{l s='Valid orders:'}
										<span class="label label-success">{$count_ok}</span>
										{l s='for a total amount of %s' sprintf=$total_ok}
									</div>
									<div class="col-lg-6">
										<i class="icon-exclamation-sign icon-big"></i>
										{l s='Invalid orders:'}
										<span class="label label-danger">{$count_ko}</span>
									</div>
								</div>
							</div>
						*}
							<table class="table">
								<thead>
									<tr>
										<th class="center"><span class="title_box ">{l s='Invoice ID'}</span></th>
										<th><span class="title_box">{l s='Historical ID'}</span></th>
										<th><span class="title_box">{l s='Site order ref.'}</span></th>
										<th><span class="title_box">{l s='eSolver order ref.'}</span></th>
										<th><span class="title_box">{l s='Invoice date'}</span></th>
										<th><span class="title_box">{l s='Payment'}</span></th>
										<th><span class="title_box">{l s='Taxable'}</span></th>
										<th><span class="title_box">{l s='Total invoice'}</span></th>
										<th></th>
									</tr>
								</thead>
								<tbody>

								<tr onclick="document.location = '?tab=AdminOrders&amp;id_order={$order['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}'">
										<td>3-16</td>
										<td>123TEST</td>
										<td>6563ABC</td>
										<td>56249</td>
										<td>01/01/2020</td>
										<td>PAYPAL</td>
										<td>€ 1.270,10</td>
										<td>€ 1.524,12</td>
										<td>
											<a class="btn btn-default" href="?tab=AdminOrders&amp;id_order={$order['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}">
												<i class='icon-search'></i> {l s='View'}
											</a>
										</td>
									</tr>

								{foreach $orders_ok AS $key => $order}
									<tr onclick="document.location = '?tab=AdminOrders&amp;id_order={$order['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}'">
										<td>{$order['id_order']}</td>
										<td>{dateFormat date=$order['date_add'] full=0}</td>
										<td>{$order['payment']}</td>
										<td>{$order['order_state']}</td>
										<td>{$order['nb_products']}</td>
										<td>{$order['total_paid_real']}</td>
										<td>
											<a class="btn btn-default" href="?tab=AdminOrders&amp;id_order={$order['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}">
												<i class='icon-search'></i> {l s='View'}
											</a>
										</td>
									</tr>
								{/foreach}
								</tbody>
							</table>
						{else}
						<p class="text-muted text-center">
							{l s='%1$s %2$s has not placed any orders yet' sprintf=[$customer->firstname, $customer->lastname]}
						</p>
						{/if}
					</div>
				</form>

{*CARTS*}
				{elseif $tab_name == "carts"}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-shopping-cart"></i> {l s='Carts'} <span class="badge" style="border-radius:0px;">{count($carts)}</span>
						</div>
						{if $carts AND count($carts)}
							<table class="table">
								<thead>
									<tr>
										<th><span class="title_box ">{l s='ID'}</span></th>
										<th><span class="title_box ">{l s='Date'}</span></th>
										<th><span class="title_box ">{l s='Quote'}</span></th>
										<th><span class="title_box ">{l s='Read'}</span></th>
										<th><span class="title_box ">{l s='Conv.'}</span></th>
										<th><span class="title_box ">{l s='ID Order'}</span></th>
										<th><span class="title_box ">{l s='Total'}</span></th>
										<th><span class="title_box ">{l s='Object'}</span></th>
										<th><span class="title_box ">{l s='Created by'}</span></th>
										<th><span class="title_box ">{l s='In charge to'}</span></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
								{foreach $carts AS $key => $cart}
									<tr onclick="document.location = '?tab=AdminCarts&amp;id_cart={$cart['id_cart']|intval}&amp;viewcart&amp;token={getAdminToken tab='AdminCarts'}'">
										<td>{$cart['id_cart']}</td>
										<td>
											<a href="index.php?tab=AdminCarts&amp;id_cart={$cart['id_cart']}&amp;viewcart&amp;token={getAdminToken tab='AdminCarts'}">
												{dateFormat date=$cart['date_upd'] full=0}
											</a>
										</td>
										<td>
											{if ! isset($cart['quote'])}
												<i class="icon-question"></i>
											{elseif $cart['quote'] == 0}
												<i class="icon-shopping-cart"></i>
											{elseif $cart['quote'] == 1}
												<i class="icon-calculator"></i>
											{/if}
										</td>
										<td>
											{if ! isset($cart['read'])}
												<i class="icon-question"></i>
											{*  Per Luca: Se = 0 non stampiamo icone; se vuoi puoi lasciare l'elseif vuoto }
											{elseif $cart['read'] == 0}
												<i class="icon-remove"></i>
											{ *}
											{elseif $cart['read']} {* Per Luca: visualizzato non è un booleano, quindi == 1 non va bene *}
												<i class="icon-check" style="color:green"></i>
											{/if}
										</td>
										<td>
											{if ! isset($cart['conv'])}
												<i class="icon-question"></i>
											{*  Per Luca: Se = 0 non stampiamo icone; se vuoi puoi lasciare l'elseif vuoto }
											{elseif $cart['conv'] == 0}
												<i class="icon-remove"></i>
											{ *}
											{elseif $cart['conv'] == 1}
												<i class="icon-check" style="color:green"></i>
											{/if}
										</td>
										<td>{if isset($cart['id_order'])} {$cart['id_order']} {else} - {/if}</td>
										<td>{if isset($cart['total_price'])} {$cart['total_price']} {else} - {/if}</td>
										<td>{if isset($cart['object'])} {$cart['object']} {else} - {/if}</td>
										<td>{if isset($cart['created_by'])} {$cart['created_by']} {else} - {/if}</td>
										<td>{if isset($cart['in_charge_to'])} {$cart['in_charge_to']} {else} - {/if}</td>
										<td>
											{*<a class="btn btn-default" href="?tab=AdminOrders&amp;id_order={$order['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}">*}
											<a class="btn btn-danger"  style="border-radius:0px;" href="">
												<i class='icon-trash'></i> {*l s='Delete'*}
											</a>
										</td>
									</tr>
								{/foreach}
								</tbody>
							</table>
						{else}
						<p class="text-muted text-center">
							{l s='No cart is available'}
						</p>
						{/if}
					</div>
					{if $products AND count($products)}
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-archive"></i> {l s='Purchased products'} <span class="badge">{count($products)}</span>
						</div>
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box">{l s='Date'}</span></th>
									<th><span class="title_box">{l s='Name'}</span></th>
									<th><span class="title_box">{l s='Quantity'}</span></th>
								</tr>
							</thead>
							<tbody>
								{foreach $products AS $key => $product}
								<tr onclick="document.location = '?tab=AdminOrders&amp;id_order={$product['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}'">
									<td>{dateFormat date=$product['date_add'] full=0}</td>
									<td>
										<a href="?tab=AdminOrders&amp;id_order={$product['id_order']}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}">
											{$product['product_name']}
										</a>
									</td>
									<td>{$product['product_quantity']}</td>
								</tr>
								{/foreach}
							</tbody>
						</table>
					</div>
					{/if}
				</form>

{*ORDERS*}
				{elseif $tab_name == "orders"}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-file"></i> {l s='Orders'} <span class="badge">{count($orders)}</span>
						</div>
						{if $orders AND count($orders)}
							{assign var=count_ok value=count($orders_ok)}
							{assign var=count_ko value=count($orders_ko)}
							<div class="panel">
								<div class="row">
									<div class="col-lg-6">
										<i class="icon-ok-circle icon-big"></i>
										{l s='Valid orders:'}
										<span class="label label-success">{$count_ok}</span>
										{l s='for a total amount of %s' sprintf=$total_ok}
									</div>
									<div class="col-lg-6">
										<i class="icon-exclamation-sign icon-big"></i>
										{l s='Invalid orders:'}
										<span class="label label-danger">{$count_ko}</span>
									</div>
								</div>
							</div>

							{* Ordini andati a buon fine *}
							{if $count_ok}
								<table class="table">
									<thead>
										<tr>
											<th class="center"><span class="title_box ">{l s='ID'}</span></th>
											<th><span class="title_box">{l s='Date'}</span></th>
											<th><span class="title_box">{l s='Status'}</span></th>
											<th><span class="title_box">{l s='Technical status'}</span></th>
											<th><span class="title_box">{l s='Cart'}</span></th>
											<th><span class="title_box">{l s='Payment'}</span></th>
											<th><span class="title_box">{l s='Tracking'}</span></th>
											<th><span class="title_box">{l s='Products'}</span></th>
											<th><span class="title_box">{l s='Total spent'}</span></th>
											<th></th>
										</tr>
									</thead>
									<tbody>
									{foreach $orders_ok AS $key => $order}
										<tr onclick="document.location = '?tab=AdminOrders&amp;id_order={$order['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}'">
											<td>{$order['id_order']}</td>
											<td>{dateFormat date=$order['date_add'] full=0}</td>
											<td>{$order['payment']}</td>
											<td>{$order['order_state']}</td>
											<td>{$order['nb_products']}</td>
											<td>{$order['total_paid_real']}</td>
											<td>
												<a class="btn btn-default" style="border-radius:0px;" href="?tab=AdminOrders&amp;id_order={$order['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}">
													<i class='icon-search'></i> {l s='View'}
												</a>
											</td>
										</tr>
									{/foreach}
									</tbody>
								</table>
							{/if}

							{* Ordini NON andati a buon fine *}
							{if $count_ko}
								<table class="table">
									<thead>
										<tr>
											<th class="center"><span class="title_box ">{l s='ID'}</span></th>
											<th><span class="title_box">{l s='Date'}</span></th>
											<th><span class="title_box">{l s='Status'}</span></th>
											<th><span class="title_box">{l s='Technical status'}</span></th>
											<th><span class="title_box">{l s='Cart'}</span></th>
											<th><span class="title_box">{l s='Payment'}</span></th>
											<th><span class="title_box">{l s='Tracking'}</span></th>
											<th><span class="title_box">{l s='Products'}</span></th>
											<th><span class="title_box">{l s='Total spent'}</span></th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										{foreach $orders_ko AS $key => $order}
										<tr onclick="document.location = '?tab=AdminOrders&amp;id_order={$order['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}'">
											<td>{$order['id_order']}</td>
											<td><a href="?tab=AdminOrders&amp;id_order={$order['id_order']}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}">{dateFormat date=$order['date_add'] full=0}</a></td>
											<td>{$order['order_state']}</td>
											<td>{$order['technical_status']}</td>
											<td>{$order['cart_id']}</td>
											<td>{$order['payment']}</td>
											<td>{$order['tracking']}</td>
											<td>{$order['nb_products']}</td>
											<td>{$order['total_paid_real']}</td>
											<td>
												<a class="btn btn-default" style="border-radius:0px;" href="?tab=AdminOrders&amp;id_order={$order['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}">
													<i class='icon-search'></i> {l s='View'}
												</a>
											</td>
										</tr>
										{/foreach}
									</tbody>
								</table>
							{/if}
						{else}
						<p class="text-muted text-center">
							{l s='%1$s %2$s has not placed any orders yet' sprintf=[$customer->firstname, $customer->lastname]}
						</p>
						{/if}
					</div>
				</form>

{*VIEWED PRODUCT*}
				{elseif $tab_name == "viewedproduct"}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
				{* Da modificare, far stampare "nessun prodotto visualizzato" anzi che non stampare il panel*}
					{if count($interested)}
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-eye"></i> {l s='Viewed products'} <span class="badge" style="border-radius:0px;">{count($interested)}</span>
						</div>
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='ID'}</span></th>
									<th><span class="title_box ">{l s='Name'}</span></th>
								</tr>
							</thead>
							<tbody>
							{foreach $interested as $key => $p}
								<tr onclick="document.location = '{$p['url']|escape:'html':'UTF-8'}'">
									<td>{$p['id']}</td>
									<td><a href="{$p['url']|escape:'html':'UTF-8'}">{$p['name']}</a></td>
								</tr>
							{/foreach}
							</tbody>
						</table>
					</div>
					{/if}
				</form>


{*MESSAGES*}
				{elseif $tab_name == "messages"}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-envelope"></i> {l s='Messages'} <span class="badge">{count($messages)}</span>
						</div>
						{if count($messages)}
							<table class="table">
								<thead>
									<th><span class="title_box">{l s='Status'}</span></th>
									<th><span class="title_box">{l s='Message'}</span></th>
									<th><span class="title_box">{l s='Sent on'}</span></th>
								</thead>
								{foreach $messages AS $message}
									<tr>
										<td>{$message['status']}</td>
										<td>
											<a href="index.php?tab=AdminCustomerThreads&amp;id_customer_thread={$message.id_customer_thread}&amp;viewcustomer_thread&amp;token={getAdminToken tab='AdminCustomerThreads'}">
												{$message['message']}...
											</a>
										</td>
										<td>{$message['date_add']}</td>
									</tr>
								{/foreach}
							</table>
						{else}
						<p class="text-muted text-center">
							{l s='%1$s %2$s has never contacted you' sprintf=[$customer->firstname, $customer->lastname]}
						</p>
						{/if}
					</div>
				</form>

{*LAST CONNECTIONS*}
				{elseif $tab_name == "lastconnections"}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					{if count($connections)}
					{* ROW relativa alle ultime connessioni *}
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-time"></i> {l s='Last connections'}
						</div>
						<table class="table">
							<thead>
							<tr>
								<th><span class="title_box">{l s='ID'}</span></th>
								<th><span class="title_box">{l s='Date'}</span></th>
								<th><span class="title_box">{l s='Pages viewed'}</span></th>
								<th><span class="title_box">{l s='Total time'}</span></th>
								<th><span class="title_box">{l s='Origin'}</span></th>
								<th><span class="title_box">{l s='IP Address'}</span></th>
							</tr>
							</thead>
							<tbody>
							{foreach $connections as $connection}
								<tr>
									<td>{$connection['id_connections']}</td>
									<td>{dateFormat date=$connection['date_add'] full=0}</td>
									<td>{$connection['pages']}</td>
									<td>{$connection['time']}</td>
									<td>{$connection['http_referer']}</td>
									<td>{$connection['ipaddress']}</td>
								</tr>
							{/foreach}
							</tbody>
						</table>
					</div>
					{/if}
				</form>

{*GROUPS*}
				{elseif $tab_name == "groups"}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-group"></i>
							{l s='Groups'}
							<span class="badge" style="border-radius:0px;">{count($groups)}</span>
							<a class="btn btn-default pull-right" href="{$current|escape:'html':'UTF-8'}&amp;updatecustomer&amp;id_customer={$customer->id|intval}&amp;token={$token|escape:'html':'UTF-8'}">
								<i class="icon-edit"></i> {l s='Edit'}
							</a>
						</div>
						{if $groups AND count($groups)}
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='ID'}</span></th>
									<th><span class="title_box ">{l s='Name'}</span></th>
								</tr>
							</thead>
							<tbody>
								{foreach $groups AS $key => $group}
								<tr onclick="document.location = '?tab=AdminGroups&amp;id_group={$group['id_group']|intval}&amp;viewgroup&amp;token={getAdminToken tab='AdminGroups'}'">
									<td>{$group['id_group']}</td>
									<td>
										<a href="?tab=AdminGroups&amp;id_group={$group['id_group']}&amp;viewgroup&amp;token={getAdminToken tab='AdminGroups'}">
											{$group['name']}
										</a>
									</td>
								</tr>
								{/foreach}
							</tbody>
						</table>
						{/if}
					</div>
				</form>

{*VOUCHERS*}
				{elseif $tab_name == "vouchers"}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-ticket"></i> {l s='Vouchers'} <span class="badge">{count($discounts)}</span>
						</div>
						{if count($discounts)}
							<table class="table">
								<thead>
									<tr>
										<th><span class="title_box">{l s='ID'}</span></th>
										<th><span class="title_box">{l s='Code'}</span></th>
										<th><span class="title_box">{l s='Name'}</span></th>
										<th><span class="title_box">{l s='Status'}</span></th>
										<th><span class="title_box">{l s='Qty available'}</span></th>
										<th><span class="title_box">{l s='Actions'}</span></th>
									</tr>
								</thead>
								<tbody>
							{foreach $discounts AS $key => $discount}
									<tr>
										<td>{$discount['id_cart_rule']}</td>
										<td>{$discount['code']}</td>
										<td>{$discount['name']}</td>
										<td>
											{if $discount['active']}
												<i class="icon-check"></i>
											{else}
												<i class="icon-remove"></i>
											{/if}
										</td>
										<td>{if $discount['quantity'] > 0}{$discount['quantity_for_user']|intval}{else}0{/if}</td>
										<td>
											<a href="?tab=AdminCartRules&amp;id_cart_rule={$discount['id_cart_rule']|intval}&amp;addcart_rule&amp;token={getAdminToken tab='AdminCartRules'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
												<i class="icon-pencil"></i>
											</a>
											<a href="?tab=AdminCartRules&amp;id_cart_rule={$discount['id_cart_rule']|intval}&amp;deletecart_rule&amp;token={getAdminToken tab='AdminCartRules'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
												<i class="icon-remove"></i>
											</a>
										</td>
									</tr>
								</tbody>
							{/foreach}
							</table>
						{else}
						<p class="text-muted text-center">
							{l s='%1$s %2$s has no discount vouchers' sprintf=[$customer->firstname, $customer->lastname]}
						</p>
						{/if}
					</div>
				</form>

{* CALLS *}
				{elseif $tab_name == "calls"}
				{if $is_agente == 0}
				<form class="form-horizontal col-lg-6" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-mail-forward"></i> {l s='Inbound Calls'} <span class="badge squadrato">{count($telefonate['ricevute'])}</span>

							<div class="panel-heading-action">
							
							</div>
						</div>

						{if count($telefonate['ricevute'])}
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='From'}</span></th>
									<th><span class="title_box ">{l s='To'}</span></th>
									<th><span class="title_box ">{l s='Date'}</span></th>
									<th><span class="title_box ">{l s='Time'}</span></th>
									<th><span class="title_box ">{l s='Status'}</span></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								{foreach $telefonate['ricevute'] AS $key => $call}
								<tr>
									<td>{$call['da']}</td>
									<td>{$call['a']}</td>
									<td>{$call['data']}</td>
									<td>{$call['durata']}</td>
									<td>{$call['stato']}</td>
									
									<td class="text-right">
										<div class="btn-group">
											<a class="btn btn-default" style="border-radius:0px;" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;addaddress=1&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
												<i class="icon-edit"></i> {* l s='Edit' *}
											</a>
											{* <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> *}
											{*	<span class="caret"></span> *}
											{* </button> *}
											{* <ul class="dropdown-menu"> *}
												{* <li> *}
											
											<a class="btn btn-danger" style="border-radius:0px;" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;deleteaddress&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
												<i class="icon-trash"></i>
												{* l s='Delete' *}
											</a>
												{* </li> *}
											{* </ul> *}
										</div>
									</td>
								</tr>
								{/foreach}
							</tbody>
						</table>
						{else}
						<p class="text-muted text-center">
							{l s='No records found'}
						</p>
						{/if}
					</div>
				</form>

				<form class="form-horizontal col-lg-6" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-mail-reply"></i> {l s='Outbound Calls'} <span class="badge squadrato">{count($telefonate['inviate'])}</span>

							<div class="panel-heading-action">
							
							</div>
						</div>

						{if count($telefonate['inviate'])}
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='From'}</span></th>
									<th><span class="title_box ">{l s='To'}</span></th>
									<th><span class="title_box ">{l s='Date'}</span></th>
									<th><span class="title_box ">{l s='Time'}</span></th>
									<th><span class="title_box ">{l s='Status'}</span></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								{foreach $telefonate['inviate'] AS $key => $call}
								<tr>
									<td>{$call['da']}</td>
									<td>{$call['a']}</td>
									<td>{$call['data']}</td>
									<td>{$call['durata']}</td>
									<td>{$call['stato']}</td>
									
									<td class="text-right">
										<div class="btn-group">
											<a class="btn btn-default" style="border-radius:0px;" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;addaddress=1&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
												<i class="icon-edit"></i> {* l s='Edit' *}
											</a>
											{* <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> *}
											{*	<span class="caret"></span> *}
											{* </button> *}
											{* <ul class="dropdown-menu"> *}
												{* <li> *}
											
											<a class="btn btn-danger" style="border-radius:0px;" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;deleteaddress&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
												<i class="icon-trash"></i>
												{* l s='Delete' *}
											</a>
												{* </li> *}
											{* </ul> *}
										</div>
									</td>
								</tr>
								{/foreach}
							</tbody>
						</table>
						{else}
						<p class="text-muted text-center">
							{l s='No records found'}
						</p>
						{/if}						
						
					</div>
				</form>

			</div>
			<div class="row">
				{* STATISTICHE *}
				<form class="form-horizontal col-lg-6" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-bar-chart"></i> {l s='Total per month'}

							<div class="panel-heading-action">

							</div>
						</div>

						{if count($telefonate['stats_mese'])}
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='Month'}</span></th>
									<th><span class="title_box ">{l s='Total inbound'}</span></th>
									<th><span class="title_box ">{l s='Duration inbound'}</span></th>
									<th><span class="title_box ">{l s='Total outbound'}</span></th>
									<th><span class="title_box ">{l s='Duration outbound'}</span></th>
								</tr>
							</thead>
							<tbody>
								{foreach $telefonate['stats_mese'] AS $key => $stats_mese}
								<tr>
									<td>{$stats_mese['mese_anno']}</td>
									<td>{if $stats_mese['totale_ricevute'] == NULL}-{else}{$stats_mese['totale_ricevute']}{/if}</td>
									<td>{if $stats_mese['durata_ricevute'] == NULL}-{else}{$stats_mese['durata_ricevute']}{/if}</td>
									<td>{if $stats_mese['totale_inviate'] == NULL}-{else}{$stats_mese['totale_inviate']}{/if}</td>
									<td>{if $stats_mese['durata_inviate'] == NULL}-{else}{$stats_mese['durata_inviate']}{/if}</td>
								</tr>
								{/foreach}
							</tbody>
							<tfoot>
								<tr>
									<td>Tot.</td>
									<td>{$telefonate['stats_totali']['totale_ricevute']}</td>
									<td>{$telefonate['stats_totali']['durata_ricevute']}</td>
									<td>{$telefonate['stats_totali']['totale_inviate']}</td>
									<td>{$telefonate['stats_totali']['durata_inviate']}</td>
								</tr>
							</tfoot>
						</table>
						{else}
						<p class="text-muted text-center">
							{l s='No records found'}
						</p>
						{/if}
					</div>
				</form>

				<form class="form-horizontal col-lg-6" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-bar-chart"></i> {l s='Total by department (always)'}</span>

							<div class="panel-heading-action">
							
							</div>
						</div>

						{if count($telefonate['stats_mese_reparto'])}
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='Department'}</span></th>
									<th><span class="title_box ">{l s='Total inbound'}</span></th>
									<th><span class="title_box ">{l s='Duration inbound'}</span></th>
									<th><span class="title_box ">{l s='Total outbound'}</span></th>
									<th><span class="title_box ">{l s='Duration outbound'}</span></th>
								</tr>
							</thead>
							<tbody>
								{foreach $telefonate['stats_mese_reparto'] AS $key => $stats_mese_reparto}
								<tr>
									<td>{if $stats_mese_reparto['reparto'] == NULL}-{else}{$stats_mese_reparto['reparto']}{/if}</td>
									<td>{if $stats_mese_reparto['totale_ricevute'] == NULL}-{else}{$stats_mese_reparto['totale_ricevute']}{/if}</td>
									<td>{if $stats_mese_reparto['durata_ricevute'] == NULL}-{else}{$stats_mese_reparto['durata_ricevute']}{/if}</td>
									<td>{if $stats_mese_reparto['totale_inviate'] == NULL}-{else}{$stats_mese_reparto['totale_inviate']}{/if}</td>
									<td>{if $stats_mese_reparto['durata_inviate'] == NULL}-{else}{$stats_mese_reparto['durata_inviate']}{/if}</td>
									
								</tr>
								{/foreach}
							</tbody>
							<tfoot>
								<tr>
									<td>Tot.</td>
									<td>{$telefonate['stats_totali_reparto']['totale_ricevute']}</td>
									<td>{$telefonate['stats_totali_reparto']['durata_ricevute']}</td>
									<td>{$telefonate['stats_totali_reparto']['totale_inviate']}</td>
									<td>{$telefonate['stats_totali_reparto']['durata_inviate']}</td>
								</tr>
							</tfoot>
						</table>
						{else}
						<p class="text-muted text-center">
							{l s='No records found'}
						</p>
						{/if}						
						
					</div>
				</form>
			
				{else}
				{$is_agente_activated = 1}
				{/if}

				{elseif $tab_name == "calls"}
				{if $is_agente == 0}
				<form class="form-horizontal col-lg-6" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-mail-forward"></i> {l s='Inbound Calls'} <span class="badge">{count($telefonate['ricevute'])}</span>

							<div class="panel-heading-action">
							
							</div>
						</div>

						{if count($telefonate['ricevute'])}
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='From'}</span></th>
									<th><span class="title_box ">{l s='To'}</span></th>
									<th><span class="title_box ">{l s='Date'}</span></th>
									<th><span class="title_box ">{l s='Time'}</span></th>
									<th><span class="title_box ">{l s='Status'}</span></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								{foreach $telefonate['ricevute'] AS $key => $call}
								<tr>
									<td>{$call['da']}</td>
									<td>{$call['a']}</td>
									<td>{$call['data']}</td>
									<td>{$call['durata']}</td>
									<td>{$call['stato']}</td>
									
									<td class="text-right">
										<div class="btn-group">
											<a class="btn btn-default" style="border-radius:0px;" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;addaddress=1&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
												<i class="icon-edit"></i> {* l s='Edit' *}
											</a>
											{* <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> *}
											{*	<span class="caret"></span> *}
											{* </button> *}
											{* <ul class="dropdown-menu"> *}
												{* <li> *}
											
											<a class="btn btn-danger" style="border-radius:0px;" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;deleteaddress&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
												<i class="icon-trash"></i>
												{* l s='Delete' *}
											</a>
												{* </li> *}
											{* </ul> *}
										</div>
									</td>
								</tr>
								{/foreach}
							</tbody>
						</table>
						{else}
						<p class="text-muted text-center">
							{l s='No records found'}
						</p>
						{/if}
					</div>
				</form>
				{else}
				{$is_agente_activated = 1}
				{/if}

{* MAIL *}
				{elseif $tab_name == "mail"}
				{if $is_agente == 0}
					<form class="form-horizontal col-lg-6" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-envelope"></i> {l s='Received Mail'} <span class="badge squadrato">{count($mail['ricevute'])}</span>

							<div class="panel-heading-action">
							
							</div>
						</div>

						{if count($mail['ricevute']) OR true}
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='From'}</span></th>
									<th><span class="title_box ">{l s='To'}</span></th>
									<th><span class="title_box ">{l s='Subject'}</span></th>
									<th><span class="title_box ">{l s='Date'}</span></th>
									<th><span class="title_box ">{l s='Attachments'}</span></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								{foreach $mail['ricevute'] AS $key => $call}
								<tr>
									<td>{$mail['da_intestazione']}</td>
									<td>{$mail['a_intestazione']}</td>
									<td>{$mail['oggetto']}</td>
									<td>{$mail['data']}</td>
									<td>{if $mail['numero_allegati'] > 0}{$mail['numero_allegati']|intval}{/if}</td>
									
									<td class="text-right">
										<div class="btn-group">
											<a class="btn btn-default" style="border-radius:0px;" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;addaddress=1&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
												<i class="icon-edit"></i> {* l s='Edit' *}
											</a>
											{* <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> *}
											{*	<span class="caret"></span> *}
											{* </button> *}
											{* <ul class="dropdown-menu"> *}
												{* <li> *}
											
											<a class="btn btn-danger" style="border-radius:0px;" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;deleteaddress&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
												<i class="icon-trash"></i>
												{* l s='Delete' *}
											</a>
												{* </li> *}
											{* </ul> *}
										</div>
									</td>
								</tr>
								{/foreach}

								<td>TESTgino@gmail.com</td>
								<td>info@asd.it</td>
								<td>Guasto pc</td>
								<td>12:34 01 Gennaio 2020</td>
								<td>NO</td>
								
								<td class="text-right">
									<div class="btn-group">
										<a class="btn btn-default" style="border-radius:0px;" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;addaddress=1&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
											<i class="icon-edit"></i> {* l s='Edit' *}
										</a>
										{* <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> *}
										{*	<span class="caret"></span> *}
										{* </button> *}
										{* <ul class="dropdown-menu"> *}
											{* <li> *}
										
										<a class="btn btn-danger" style="border-radius:0px;" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;deleteaddress&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
											<i class="icon-trash"></i>
											{* l s='Delete' *}
										</a>
											{* </li> *}
										{* </ul> *}
									</div>
								</td>
							</tbody>
						</table>
						{else}
						<p class="text-muted text-center">
							{l s='No mail was found'}
						</p>
						{/if}
					</div>
				</form>

				<form class="form-horizontal col-lg-6" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-envelope"></i> {l s='Sended Mail'} <span class="badge squadrato">{count($mail['inviate'])}</span>

							<div class="panel-heading-action">
							
							</div>
						</div>

						{if count($mail['inviate']) OR true}
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='From'}</span></th>
									<th><span class="title_box ">{l s='To'}</span></th>
									<th><span class="title_box ">{l s='Subject'}</span></th>
									<th><span class="title_box ">{l s='Date'}</span></th>
									<th><span class="title_box ">{l s='Attachments'}</span></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								{foreach $mail['inviate'] AS $key => $call}
								<tr>
									<td>{$mail['da_intestazione']}</td>
									<td>{$mail['a_intestazione']}</td>
									<td>{$mail['oggetto']}</td>
									<td>{$mail['data']}</td>
									<td>{if $mail['numero_allegati'] > 0}{$mail['numero_allegati']|intval}{/if}</td>
									
									<td class="text-right">
										<div class="btn-group">
											<a class="btn btn-default" style="border-radius:0px;" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;addaddress=1&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
												<i class="icon-edit"></i> {* l s='Edit' *}
											</a>
											{* <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> *}
											{*	<span class="caret"></span> *}
											{* </button> *}
											{* <ul class="dropdown-menu"> *}
												{* <li> *}
											
											<a class="btn btn-danger" style="border-radius:0px;" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;deleteaddress&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
												<i class="icon-trash"></i>
												{* l s='Delete' *}
											</a>
												{* </li> *}
											{* </ul> *}
										</div>
									</td>
								</tr>
								{/foreach}

								<td>TESTgino@gmail.com</td>
								<td>info@asd.it</td>
								<td>Guasto pc</td>
								<td>12:34 01 Gennaio 2020</td>
								<td>NO</td>
								
								<td class="text-right">
									<div class="btn-group">
										<a class="btn btn-default" style="border-radius:0px;" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;addaddress=1&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
											<i class="icon-edit"></i> {* l s='Edit' *}
										</a>
										{* <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> *}
										{*	<span class="caret"></span> *}
										{* </button> *}
										{* <ul class="dropdown-menu"> *}
											{* <li> *}
										
										<a class="btn btn-danger" style="border-radius:0px;" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;deleteaddress&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
											<i class="icon-trash"></i>
											{* l s='Delete' *}
										</a>
											{* </li> *}
										{* </ul> *}
									</div>
								</td>
							</tbody>
						</table>
						{else}
						<p class="text-muted text-center">
							{l s='No mail was found'}
						</p>
						{/if}
					</div>
				</form>
				{else}
				{$is_agente_activated = 1}
				{/if}

{* STATISTICHE *}
				{elseif $tab_name == "stats"}
				{if $is_agente == 0}
				
				{* DATI STATISTICI CLIENTE *}
				<form class="form-horizontal col-lg-6" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-envelope"></i> {l s='Customer statistical data'} <span class="badge squadrato">{count($mail['inviate'])}</span>

							<div class="panel-heading-action">
							
							</div>
						</div>

						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='Data'}</span></th>
									<th><span class="title_box ">{l s='Value'}</span></th>
								</tr>
							</thead>
							<tbody>
								{if $customer->is_company == 1}
								<tr>
									<td class="thick">{l s='Number of employees'}</td>
									<td>{if isset($customer->employees_number) AND $customer->employees_number != NULL}{$customer->employees_number}{else}-{/if}</td>
								</tr>
								{/if}
								<tr>
									<td class="thick">{l s='Interest'}</td>
									{foreach $stat['interesse'] as $key => $interesse}
										<td>{$interesse['category']}</td>
									{/foreach}								
								</tr>
								<tr>
									<td class="thick">{l s='First purchase'}</td>
									{foreach $stat['primo_acquisto'] as $key => $primo}
										<td>{$primo['product_name']}, {$primo['product_quantity']} pz.</td>
									{/foreach}
								</tr>
								<tr>
									<td class="thick">{l s='Last purchase'}</td>
									{if count($stat['ultimo_acquisto'])}
									{foreach $stat['ultimo_acquisto'] as $key => $ultimo}
										<td>{if isset($ultimo['product_name']) AND $ultimo['product_name'] != NULL}{$ultimo['product_name']}{/if}, {if isset($ultimo['product_quantity']) AND $ultimo['product_quantity'] != NULL}{$ultimo['product_quantity']}{/if} pz.</td>
									{/foreach}
									{else}
									<td><td>
									{/if}
								</tr>
							</tbody>
						</table>
					</div>
				</form>

				{* PRODOTTI NEL CARRELLO *}
				<form class="form-horizontal col-lg-6" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-shopping-cart"></i> {l s='Products in the cart'} <span class="badge squadrato">{count($interested)}</span>
						</div>
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='Added'}</span></th>
									<th><span class="title_box ">{l s='Product ID'}</span></th>
									<th><span class="title_box ">{l s='Product name'}</span></th>
								</tr>
							</thead>
							<tbody>
							{foreach $interested as $key => $p}
								{if $p['id'] != 0}
								<tr onclick="document.location = '{$p['url']|escape:'html':'UTF-8'}'">
									<td>{$p['date_add']}</td>
									<td>{$p['id']}</td>
									<td><a href="{$p['url']|escape:'html':'UTF-8'}">{$p['name']}</a></td>
								</tr>
								{else}
								<tr>
									<td>{$p['date_add']}</td>
									<td>{$p['id']}</td>
									<td>{$p['name']} - {l s='Product no longer existing'}</td>
								</tr>
								{/if}
							{/foreach}
							</tbody>
						</table>
					</div>
				</form>

				</div>
				<div class="row">

				{* PRODOTTI ORDINATI *}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-archive"></i> {l s='Purchased products'} <span class="badge squadrato">{count($products)}</span>
						</div>
						{if $products AND count($products)}
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box">{l s='Date'}</span></th>
									<th><span class="title_box">{l s='Name'}</span></th>
									<th><span class="title_box">{l s='Manufacturer'}</span></th>
									<th><span class="title_box">{l s='Category'}</span></th>
									<th><span class="title_box">{l s='Quantity'}</span></th>
									<th><span class="title_box">{l s='Total (always)'}</span></th>
								</tr>
							</thead>
							<tbody>
								{foreach $stat['prodotti_ordinati'] AS $key => $product}
								<tr onclick="document.location = '?tab=AdminOrders&amp;id_order={$product['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}'">
									<td>{dateFormat date=$product['date_add'] full=0}</td>
									<td>
										<a href="?tab=AdminOrders&amp;id_order={$product['id_order']}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}">
											{$product['product_name']}
										</a>
									</td>
									<td>{$product['manufacturer']}</td>
									<td>{$product['category']}</td>
									<td>{$product['product_quantity']}</td>
									<td>{$product['total']}</td>
								</tr>
								{/foreach}
							</tbody>
						</table>
						{else}
							{l s='This customer has never purchased on Ezdirect.'}
						{/if}
					</div>
					
				</form>

				</div>
				<div class="row">

				{* TOTALE ACQUISTI *}
				<form class="form-horizontal col-lg-4" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-envelope"></i> {l s='Total purchases'} <span class="badge squadrato">{count($mail['inviate'])}</span>

							<div class="panel-heading-action">
							
							</div>
						</div>
						{if count($stat['acquisti_per_anno'])}
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='Year'}</span></th>
									<th><span class="title_box ">{l s='Total paid'}</span></th>
									<th><span class="title_box ">{l s='Total VAT excluded'}</span></th>
								</tr>
							</thead>
							<tbody>
								{foreach $stat['acquisti_per_anno'] AS $key => $acq_anno}
								<tr>
									<td class="thick">{$acq_anno['anno']}</td>
									<td>{$acq_anno['totale']}</td>
									<td>{$acq_anno['totale_senza_iva']}</td> 
								</tr>
								{/foreach}
							</tbody>
							<tfoot>
								<tr>
									<td class="thick">{l s='Total'}</td>
									<td>{$stat['totale_ok_con_iva']}</td>
									<td>{$stat['totale_ok_senza_iva']}</td>
								</tr>
							</tfoot>
						</table>
						{else}
						<p>{l s='This customer has never purchased on Ezdirect.'}</p>
						{/if}
					</div>
				</form>
				
				{* ULTIME CONNESSIONI *}
				<form class="form-horizontal col-lg-8" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" novalidate>
				
					{* ROW relativa alle ultime connessioni *}
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-time"></i> {l s='Last connections'}
						</div>
						{if count($connections)}
						<table class="table">
							<thead>
							<tr>
								<th><span class="title_box">{l s='ID'}</span></th>
								<th><span class="title_box">{l s='Date'}</span></th>
								<th><span class="title_box">{l s='Pages viewed'}</span></th>
								<th><span class="title_box">{l s='Total time'}</span></th>
								<th><span class="title_box">{l s='Origin'}</span></th>
								<th><span class="title_box">{l s='IP Address'}</span></th>
							</tr>
							</thead>
							<tbody>
							{foreach $connections as $connection}
								<tr>
									<td>{$connection['id_connections']}</td>
									<td>{dateFormat date=$connection['date_add'] full=0}</td>
									<td>{$connection['pages']}</td>
									<td>{$connection['time']}</td>
									<td>{$connection['http_referer']}</td>
									<td>{$connection['ipaddress']}</td>
								</tr>
							{/foreach}
							</tbody>
						</table>
						{else}
						<p>{l s='This customer has never logged in.'}</p>
						{/if}
					</div>
					
				</form>
				{*}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-share-alt"></i> {l s='Referrers'}
						</div>
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='Date'}</span></th>
									<th><span class="title_box ">{l s='Name'}</span></th>
									{if $shop_is_feature_active}<th>{l s='Shop'}</th>{/if}
								</tr>
							</thead>
							<tbody>
								{foreach $referrers as $referrer}
								<tr>
									<td>{dateFormat date=$order['date_add'] full=0}</td>
									<td>{$referrer['name']}</td>
									{if $shop_is_feature_active}<td>{$referrer['shop_name']}</td>{/if}
								</tr>
								{/foreach}
							</tbody>
						</table>
					</div>

				</form>
				{*}
				{else}
				{$is_agente_activated = 1}
				{/if}

{* EZCLOUD *}
				{elseif $tab_name == "ezcloud"}
				{if $is_agente == 0}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-envelope"></i> {l s='Active Ezcloud virtual PBX'} <span class="badge">{count($ezcloud['centralini'])}</span>

							<div class="panel-heading-action">
								{if ! count($ezcloud['centralini'])}
								<a class="btn btn-default" href="{$current|escape:'html':'UTF-8'}&amp;updatecustomer&amp;id_customer={$customer->id|intval}&amp;token={$token|escape:'html':'UTF-8'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
									<i class="icon-plus-sign"></i>
									{l s='Activate Ezdirect PBX for this customer'}
								</a>
								{/if}
							</div>
						</div>
						{if count($ezcloud['centralini'])}
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='ID'}</span></th>
									<th><span class="title_box ">{l s='Status'}</span></th>
									<th><span class="title_box ">{l s='Activated on'}</span></th>
									<th><span class="title_box ">{l s='Qt. lines'}</span></th>
									<th><span class="title_box ">{l s='Qt. internal'}</span></th>
									<th><span class="title_box ">{l s='Internal single cost'}</span></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								{foreach $ezcloud['centralini'] AS $key => $centralino}
								<tr>
									<td>{$centralino['id_ezcloud']}</td>
									<td>{$centralino['active']}</td> {* da modificare *}
									<td>{$centralino['date_add']}</td> 
									<td>{$centralino['linee']}</td> 
									<td>{$centralino['interni']}</td> 
									<td>{$centralino['prezzo']}</td>
									<td><button type="button" class="btn btn-default" data-toggle="collapse" data-target="#dettaglio_{$centralino['id_ezcloud']}" aria-expanded="false" aria-controls="collapse_ra">{l s='Vedi'}</button></td>
									
								</tr>
								<tr>
									<td>
										<div class="collapse" id="dettaglio_{$centralino['id_ezcloud']}">
										<h2>Ezcloud n.{$centralino['id_ezcloud']}</h2>
										<h4>{l s='Customer evaluation'}</h4>
										<br />
										{l s='Quality of service: '}{$centralino['servizio']}
										<br />
										{l s='PBX Functionality: '}{$centralino['funzionalita']}
										<br />
										{l s='Support quality: '}{$centralino['supporto']}
										<br />
										<br />
										</div>
									</td>
								</tr>
								
								{/foreach}
							</tbody>
						</table>
						{else}
						<p>{l s='This customer does not yet have any active Ezcloud PBX.'}</p>
						{/if}
						
					</div>
				</form>
				{else}
				{$is_agente_activated = 1}
				{/if}

{* BDL ?*}
				{elseif $tab_name == "bdl"}
				{if $is_agente == 0}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" novalidate>
				
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-time"></i> {l s='BDL'} <span class="badge squadrato">{count($bdl['buoni'])}</span>

							<div class="panel-heading-action">
								<a class="btn btn-default" href="{$current|escape:'html':'UTF-8'}&amp;updatecustomer&amp;id_customer={$customer->id|intval}&amp;token={$token|escape:'html':'UTF-8'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
									<i class="icon-plus-sign"></i>
									{l s='Generate new BDL'}
								</a>
							</div>
						</div>

						{if count($bdl['buoni'])}
						{* Sono presenti {$count_bdl} buoni di lavoro per questa anagrafica *}
						{*<p>{l s='%1$s Job Vouchers (BDL) found' sprintf=count($bdl['buoni'])}</p> *}
						<table class="table">
							<thead>
							<tr>
								<th><span class="title_box">{l s='N. BDL'}</span></th>
								<th><span class="title_box">{l s='Date BDL'}</span></th>
								<th><span class="title_box">{l s='Reason'}</span></th>
								<th><span class="title_box">{l s='Amount'}</span></th>
								<th><span class="title_box">{l s='Status'}</span></th>
								<th><span class="title_box">{l s='Made by'}</span></th>
								<th><span class="title_box">{l s='Actions'}</span></th>
							</tr>
							</thead>
							<tbody>
							{foreach $bdl['buoni'] AS $key => $righe_buoni}
								<tr>
									<td>{$righe_buoni['id_bdl']}</td>
									<td>{$righe_buoni['data_bdl']}</td>
									<td>{$righe_buoni['motivo_chiamata']}</td>
									<td>{$righe_buoni['importo']}</td>
									{* qui mi passi l'intera riga class? *}
									<td><span {$righe_buoni['status_style']}>{$righe_buoni['status']}</span></td>
									<td>{$righe_buoni['fatto_da']}</td>
									<td>
										<a class="btn btn-default" href="#">
											<i class="icon-pencil"></i>
										</a>
										<a class="btn btn-default" href="#">
											<i class="icon-trash"></i>
										</a>
									</td>
								</tr>
							{/foreach}
							</tbody>
						</table>
						{else}
						{* Non sono presenti buoni di lavoro per questa anagrafica *}
						<p>{l s='No Job Vouchers (BDL) found'}</p>
						{/if}
					</div>
					
				</form>
				{else}
				{$is_agente_activated = 1}
				{/if}


{* AZIONI CLIENTE *}
				{elseif $tab_name == "actions"}
				{if $not_mio_agente == 0}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-check"></i> {l s='Customer actions'}

							<div class="panel-heading-action">
								
							</div>
						</div>
						{if count($azioni_cliente['azioni'])}
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='ID Action'}</span></th>
									<th><span class="title_box ">{l s='Status'}</span></th>
									<th><span class="title_box ">{l s='On charge to'}</span></th>
									<th><span class="title_box ">{l s='Type'}</span></th>
									<th><span class="title_box ">{l s='Q.ta com'}</span></th>
									<th><span class="title_box ">{l s='Last com.'}</span></th>
									<th><span class="title_box ">{l s='Opened on'}</span></th>
									<th><span class="title_box ">{l s='Changed on'}</span></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								{foreach $azioni_cliente['azioni'] AS $key => $azione_cliente}
								<tr>
									<td>{$azione_cliente['id_azione']}</td>
									<td>{$azione_cliente['status']}</td> {* da modificare con if *}
									<td>{$azione_cliente['in_carico']}</td> 
									<td>{$azione_cliente['tipo']}</td> {* varie icone if *}
									<td>{$azione_cliente['qta_com']}</td> 
									<td>{$azione_cliente['ultima_com']}</td>
									<td>{$azione_cliente['aperto_il']}</td>
									<td>{$azione_cliente['modificato_il']}</td>
									<td><button type="button" class="btn btn-default" data-toggle="collapse" data-target="#dettaglio_{$centralino['id_ezcloud']}" aria-expanded="false" aria-controls="collapse_ra">{l s='Vedi'}</button></td>
									
								</tr>
								{*}
								<tr>
									<td>
										<div class="collapse" id="dettaglio_{$centralino['id_ezcloud']}">
										<h2>Ezcloud n.{$centralino['id_ezcloud']}</h2>
										<h4>{l s='Customer evaluation'}</h4>
										<br />
										{l s='Quality of service: '}{$centralino['servizio']}
										<br />
										{l s='PBX Functionality: '}{$centralino['funzionalita']}
										<br />
										{l s='Support quality: '}{$centralino['supporto']}
										<br />
										<br />
										</div>
									</td>
								</tr>
								{*}
								{/foreach}
							</tbody>
						</table>
						{else}
						<p>{l s='This customer has no actions taken yet.'}</p>
						{/if}
						
					</div>
				</form>
				{else}
				{$is_agente_activated = 1}
				{/if}


{* TO DO *}
				{elseif $tab_name == "todo"}
				{if $not_mio_agente == 0}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-check"></i> {l s='TO DO'}

							<div class="panel-heading-action">
								
							</div>
						</div>
						{if count($todo['todo'])}
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='ID Action'}</span></th>
									<th><span class="title_box ">{l s='Status'}</span></th>
									<th><span class="title_box ">{l s='On charge to'}</span></th>
									<th><span class="title_box ">{l s='Type'}</span></th>
									<th><span class="title_box ">{l s='Q.ta com'}</span></th>
									<th><span class="title_box ">{l s='Last com.'}</span></th>
									<th><span class="title_box ">{l s='Opened on'}</span></th>
									<th><span class="title_box ">{l s='Changed on'}</span></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								{foreach $todo['todo'] AS $key => $todo_riga}
								<tr>
									<td>{$todo_riga['id_azione']}</td>
									<td>{$todo_riga['status']}</td> {* da modificare con if *}
									<td>{$todo_riga['in_carico']}</td> 
									<td>{$todo_riga['tipo']}</td> {* varie icone if *}
									<td>{$todo_riga['qta_com']}</td> 
									<td>{$todo_riga['ultima_com']}</td>
									<td>{$todo_riga['aperto_il']}</td>
									<td>{$todo_riga['modificato_il']}</td>
									<td><button type="button" class="btn btn-default" data-toggle="collapse" data-target="#dettaglio_{$centralino['id_ezcloud']}" aria-expanded="false" aria-controls="collapse_ra">{l s='Vedi'}</button></td>
									
								</tr>
								{*}
								<tr>
									<td>
										<div class="collapse" id="dettaglio_{$centralino['id_ezcloud']}">
										<h2>Ezcloud n.{$centralino['id_ezcloud']}</h2>
										<h4>{l s='Customer evaluation'}</h4>
										<br />
										{l s='Quality of service: '}{$centralino['servizio']}
										<br />
										{l s='PBX Functionality: '}{$centralino['funzionalita']}
										<br />
										{l s='Support quality: '}{$centralino['supporto']}
										<br />
										<br />
										</div>
									</td>
								</tr>
								{*}
								{/foreach}
							</tbody>
						</table>
						{else}
						<p>{l s='This customer has no TO DO yet.'}</p>
						{/if}
						
					</div>
				</form>
				{else}
				{$is_agente_activated = 1}
				{/if}


{* TICKETS *}
				{elseif $tab_name == "tickets"}
				{if $not_mio_agente == 0}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-ticket"></i> {l s='Tickets'}

							<div class="panel-heading-action">
								
							</div>
						</div>
						{if count($tickets['ticket'])}
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='ID Action'}</span></th>
									<th><span class="title_box ">{l s='Status'}</span></th>
									<th><span class="title_box ">{l s='On charge to'}</span></th>
									<th><span class="title_box ">{l s='Type'}</span></th>
									<th><span class="title_box ">{l s='Q.ta com'}</span></th>
									<th><span class="title_box ">{l s='Last com.'}</span></th>
									<th><span class="title_box ">{l s='Opened on'}</span></th>
									<th><span class="title_box ">{l s='Changed on'}</span></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								{foreach $tickets['ticket'] AS $key => $ticket}
								<tr>
									<td>{$ticket['id_ticket']}</td>
									<td>{$ticket['status']}</td> {* da modificare con if Verde (C) - Rosso (A) *}
									<td>{$ticket['in_carico']}</td> 
									<td>{$ticket['tipo']}</td> {* varie icone if cono stradale (?) e carrello*}
									<td>{$ticket['qta_com']}</td> 
									<td>{$ticket['ultima_com']}</td>
									<td>{$ticket['aperto_il']}</td>
									<td>{$ticket['modificato_il']}</td>
									<td><button type="button" class="btn btn-default" data-toggle="collapse" data-target="#dettaglio_{$centralino['id_ezcloud']}" aria-expanded="false" aria-controls="collapse_ra">{l s='Vedi'}</button></td>
									
								</tr>
								{*}
								<tr>
									<td>
										<div class="collapse" id="dettaglio_{$centralino['id_ezcloud']}">
										<h2>Ezcloud n.{$centralino['id_ezcloud']}</h2>
										<h4>{l s='Customer evaluation'}</h4>
										<br />
										{l s='Quality of service: '}{$centralino['servizio']}
										<br />
										{l s='PBX Functionality: '}{$centralino['funzionalita']}
										<br />
										{l s='Support quality: '}{$centralino['supporto']}
										<br />
										<br />
										</div>
									</td>
								</tr>
								{*}
								{/foreach}
							</tbody>
						</table>
						{else}
						<p>{l s='This customer has no ticket yet.'}</p>
						{/if}
						
					</div>
				</form>
				{else}
				{$is_agente_activated = 1}
				{/if}


{* CONTRATTI *}
				{elseif $tab_name == "contracts"}
				{if $is_agente == 0}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-comments"></i> {l s='Assistance contracts'}

							<div class="panel-heading-action">
								
							</div>
						</div>
						{if count($contratti_assistenza['contratti'])}
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='Contract Number'}</span></th>
									<th><span class="title_box ">{l s='Type'}</span></th>
									<th><span class="title_box ">{l s='Status'}</span></th>
									<th><span class="title_box ">{l s='Start on'}</span></th>
									<th><span class="title_box ">{l s='Expires'}</span></th>
									<th><span class="title_box ">{l s='Amount'}</span></th>
									<th><span class="title_box ">{l s='Headquarters'}</span></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								{foreach $contratti_assistenza['contratti'] AS $key => $contratto}
								<tr>
									<td>{$contratto['id_contratto']}</td>
									<td>{$contratto['tipo']}</td>
									<td>{$contratto['stato']}</td> 
									<td>{$contratto['inizia_il']}</td>
									<td>{$contratto['scade_il']}</td> 
									<td>{$contratto['importo']}</td>
									<td>{$contratto['sede']}</td>
									<td><button type="button" class="btn btn-default" data-toggle="collapse" data-target="#dettaglio_{$centralino['id_ezcloud']}" aria-expanded="false" aria-controls="collapse_ra">{l s='Vedi'}</button></td>
									
								</tr>
								{*}
								<tr>
									<td>
										<div class="collapse" id="dettaglio_{$centralino['id_ezcloud']}">
										<h2>Ezcloud n.{$centralino['id_ezcloud']}</h2>
										<h4>{l s='Customer evaluation'}</h4>
										<br />
										{l s='Quality of service: '}{$centralino['servizio']}
										<br />
										{l s='PBX Functionality: '}{$centralino['funzionalita']}
										<br />
										{l s='Support quality: '}{$centralino['supporto']}
										<br />
										<br />
										</div>
									</td>
								</tr>
								{*}
								{/foreach}
							</tbody>
						</table>
						{else}
						<p>{l s='This customer has no service contracts.'}</p>
						{/if}
						
					</div>
				</form>
				{else}
				{$is_agente_activated = 1}
				{/if}


{* DOCUMENTI *}
				{elseif $tab_name == "documents"}
				{if $is_agente == 0}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-paper-clip"></i> {l s='Documents'}

							<div class="panel-heading-action">
								
							</div>
						</div>

						{if $count_documenti != 0}
							{if $documenti['count_docs'] != 0}
							<p>{l s='Sono presenti %1$d file e %2$d sottocartelle in questa cartella.' sprintf=[$documenti['count_sub_files'], $documenti['count_sub_cartelle']]}</p>
							{else}
							<p>{l s='Non sono presenti file né sottocartelle in questa cartella.'}</p>
							{/if}
							<table class="table">
								<thead>
									<tr>
										<th><span class="title_box ">{l s='File'}</span></th>
										<th><span class="title_box ">{l s='Size'}</span></th>
										<th><span class="title_box ">{l s='Format'}</span></th>
										<th><span class="title_box ">{l s='Last edit'}</span></th>
										<th style='text-align:center;'><span class="title_box ">{l s='Select'}</span></th>
									</tr>
								</thead>
								<tbody>
									{foreach $documenti['cartelle'] AS $key => $documento}
									<tr>
										<td><i class="icon-folder"></i> {$documento['nome']}</td> {* Aggiungere in cima icona *}
										<td>{if $documento['dimensioni'] != 0}{$documento['dimensioni']}{/if}</td>
										<td>{$documento['formato']}</td> 
										<td>{$documento['ultima_modifica']}</td>
										<td style='text-align:center;'><input type='checkbox' name='selectdocfile[]' value='{$documento['nome']}' /></td>
									</tr>
									{/foreach}
									{foreach $documenti['files'] AS $key => $documento}
									<tr>
										<td><i class="{$documento['icona']}"></i> {$documento['nome']}</td> {* Aggiungere in cima icona *}
										<td>{if $documento['dimensioni'] != 0}{$documento['dimensioni']}{/if}</td>
										<td>{$documento['formato']}</td> 
										<td>{$documento['ultima_modifica']}</td>
										<td style='text-align:center;'><input type='checkbox' name='selectdocfile[]' value='{$documento['nome']}' /></td>
									</tr>
									{/foreach}
								</tbody>
							</table>
						{else}
						<p>{l s='The customer has no saved documents.'}</p>
						{* <p>{l s='Non esiste una cartella documenti associata a questa anagrafica.'}</p> *}
						{* INSERIRE PULSANTE CREAZIONE CARTELLA *}
						{/if}
						
					</div>
				</form>
				{else}
				{$is_agente_activated = 1}
				{/if}


{*OTHERS?*}
				{elseif $tab_name == "others"}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
				</form>

{*ERROR TAB NOT FOUND*}
				{elseif ! isset($is_agente_activated)}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-warning-sign"></i> {l s='Error'}
						</div>

						<h2 class="text-muted text-center">
							{l s='Tab'} <a class="text-danger">{$tab_name}</a> {l s='not found'}
						</h2>
					</div>
				</form>
				{/if}

				{if isset($is_agente_activated)}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-warning-sign"></i> {l s='Access denied'}
						</div>

						<h2 class="text-muted text-center">
							{l s='You do not have the necessary rights to access '} <a class="text-danger">{$tab_name}</a> {l s='tab'}
						</h2>
					</div>
				</form>
				{/if}

			</div>
		</div>

	






{********************************************************************************************************************************************************************************************************************************}
{********************************************************************************************************************************************************************************************************************************}
{********************************************************************************************************************************************************************************************************************************}
{********************************************************************************************************************************************************************************************************************************}
{********************************************************************************************************************************************************************************************************************************}
{********************************************************************************************************************************************************************************************************************************}



		{if count($emails)}
		{* ROW relativa alle email *}
		<div class="col-lg-6">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-envelope"></i> {l s='Last emails'}
				</div>
				<table class="table">
					<thead>
					<tr>
						<th><span class="title_box">{l s='Date'}</span></th>
						<th><span class="title_box">{l s='Language'}</span></th>
						<th><span class="title_box">{l s='Subject'}</span></th>
						<th><span class="title_box">{l s='Template'}</span></th>
					</tr>
					</thead>
					<tbody>
					{foreach $emails as $email}
						<tr>
							<td>{dateFormat date=$email['date_add'] full=1}</td>
							<td>{$email['language']}</td>
							<td>{$email['subject']}</td>
							<td>{$email['template']}</td>
						</tr>
					{/foreach}
					</tbody>
				</table>
			</div>
		</div>
		{/if}

	</div>
</div>
{/if}
<hr />
{/block}
