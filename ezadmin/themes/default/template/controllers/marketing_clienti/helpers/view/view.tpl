{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

<script type="text/javascript">
	$(document).ready(function() {
		$(".hint_tooltip").tooltip({ showURL: false  });
	});
</script>

{if !isset($smarty.post.cercaiclienti)}

<div id="container-stats-prodotti">
	<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate>
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> Marketing su fatturato
			</div>

            <p>Questo strumento ti consente di estrapolare liste di clienti (per invii mail, per statistiche... ) sulla base del fatturato. Puoi profilare i clienti secondo diversi filtri creando delle liste ad hoc. I dati sono esportabili in Excel ed &egrave; possibile anche esportare liste gi&agrave; formattate per l'inserimento su Mailup.</p>
			
            <strong>Filtri di selezione</strong>
            <br /><br />
			{* Correggere: usare bootstrap *}
			<form method="post" name="cercaclienti" id="cercaclienti">
                <table cellpadding="9">
				<tr>
                    <td>Prodotto/i acquistato/i</td>
                    <td>
                        <select multiple class="select2mul" id="per_prodotto" name="per_prodotto[]" style="width:600px">
                            <option name="0" value="0">--- Scegli un prodotto ---</option>
                            {foreach $prodotti as $row}
                                <option name='{$row["cod_articolo"]}' value='{$row["cod_articolo"]}'>{$row["cod_articolo"]} - {$row["desc_articolo"]}</option>
                            {/foreach}
                        </select>
                    </td>
                    <td><span class='hint_tooltip' style="cursor:pointer;" title="Per selezionare clienti che hanno acquistato uno o più prodotti precisi. Esempio: tutti i clienti che hanno acquistato la cuffia Ez Mono e la cuffia Ez Duo." ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td>
                </tr>
				<tr><td>Quantit&agrave;</td><td>
				<input type='text' value='' name='quantita' /> </td><td></td></tr>
				
				<tr><td>Importo</td><td>
				<input type='text' value='' name='importo' /> </td><td></td></tr>
				
				<tr><td>Nel periodo</td><td>
				Dal <input type="date" id="arco_dal" name="arco_dal"  /> al <input type="date" id="arco_al" name="arco_al"  /></td><td><span class="hint_tooltip" style="cursor:pointer;" title="Modalità acquisti: si considera la data di acquisto. Modalità no acquisto o modalità tutti: si considera la data di registrazione dell'account." ><img src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Aiuto" /></span></td></tr>
				<tr><td>Macrocategoria/e acquistata/e</td><td>
				<select multiple class="select2mul" id="per_macrocategoria" name="per_macrocategoria[]" style="width:600px">
				<option name="0" value="0">--- Scegli una macrocategoria ---</option>
				

                {foreach $macrocat as $rowcat}
					<option name='{$rowcat["id_category"]}' value='{$rowcat["id_category"]}'>{$rowcat["id_category"]} - {$rowcat["name"]}</option>
				{/foreach}
					
				</select></td><td><span class='hint_tooltip' style="cursor:pointer;" title="Per selezionare clienti che hanno acquistato in una o più macrocategorie. Esempio: clienti che hanno acquistato cuffie e centralini." ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				<tr><td>Sottocategoria/e acquistata/e</td><td>
				<select multiple class="select2mul" id="per_categoria" name="per_categoria[]" style="width:600px">
				<option name="0" value="0">--- Scegli una categoria ---</option>
				

                {foreach $sottocat as $rowcat}
					<option name='{$rowcat["id_category"]}' value='{$rowcat["id_category"]}'>{$rowcat["id_category"]} - {$rowcat["name"]}</option>
				{/foreach}
				
				</select></td><td><span class='hint_tooltip' style="cursor:pointer;" title="Per selezionare clienti che hanno acquistato in una o più sottocategorie. Esempio: clienti che hanno acquistato cuffie per ufficio e cuffie per call center." ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				<tr><td>Marca/marche acquistata/e</td><td>
				<select multiple class="select2mul" id="per_marchio" name="per_marchio[]" style="width:600px">
				<option name="0" value="0">--- Scegli un marchio ---</option>
				

                {foreach $marchi as $rowman}
					<option name='{$rowman["id_manufacturer"]}' value='{$rowman["id_manufacturer"]}'>{$rowman["id_manufacturer"]} - {$rowman["name"]}</option>
				{/foreach}
					
				</select></td><td><span class='hint_tooltip' style="cursor:pointer;" title="Per cercare clienti che hanno acquistato marche precise. Esempio: clienti che hanno acquistato Ezdirect, Jabra e Plantronics" ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				<tr><td>Privati?</td><td>
				<input type='radio' value='0' name='escludi_privati' checked='checked' /> Includi privati &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				<input type='radio' value='1' name='escludi_privati' /> Escludi privati</td><td></td></tr>
				
				<tr><td>Profilo cliente</td><td>

                {foreach $gruppi_clienti as $g}
					<input type='checkbox' value='0' class='gruppi' name='group[{$g['id_group']}]' /> {$g['name']} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				{/foreach}

				 - <a href='javascript:void(0)' onclick='$(".gruppi").attr("checked", true);'>Clicca per selezionarli tutti</a></td><td></td></tr>
				
				<tr><td>Inattivit&agrave;</td><td>
				<select name='inattivita'>
				<option value='0'> -- </option>
				<option value='6'> 6 mesi </option>
				<option value='12'> 12 mesi </option>
				<option value='18'> 18 mesi </option>
				<option value='24'> 24 mesi </option>
				<option value='48'> 48 mesi </option>
				<option value='60'> 60 mesi </option>
				<option value='60p'> Più di 60 mesi </option>
				</select>
				</td><td><span class='hint_tooltip' style="cursor:pointer;" title="Per selezionare clienti che non acquistano dal periodo scelto col menù a tendina. Lasciare vuoto (--) per ignorare questo parametro. " ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				
				<tr><td>Tipo cliente</td><td>
				<select name='tipo_cliente'>
				<option value=''> -- </option>
				<option value='attivo'> Attivo </option>
				<option value='fedele'> Fedele </option>
				<option value='lead'> Lead </option>
				<option value='prospect'> Prospect </option>
				<option value='inattivo'> Inattivo </option>
				</select>
				</td><td><span class='hint_tooltip' style="cursor:pointer;" title="ATTIVO: Almeno un acquisto nell'anno<br />FEDELE: almeno tre acquisti nell'anno<br />PROSPECT: non ha ancora acquistato quest'anno, ma ha un preventivo o un carrello<br />LEAD: non ha acquistato né chiesto preventivi quest'anno, ma ci ha contattato o ci sono attivit&agrave; in corso<br />INATTIVO: non acquista da almeno un anno" ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				
				<tr><td>Zona</td><td>
				<select name='zona_cliente'>
				<option value=''> -- </option>
				<option value='nord'> Nord </option>
				<option value='centro'> Centro </option>
				<option value='sud'> Sud </option>
				<option value='nd'> Estero / Non disponibile </option>
				</select>
				</td><td><span class='hint_tooltip' style="cursor:pointer;" title="ATTIVO: Almeno un acquisto nell'anno<br />FEDELE: almeno tre acquisti nell'anno<br />PROSPECT: non ha ancora acquistato quest'anno, ma ha un preventivo o un carrello<br />LEAD: non ha acquistato né chiesto preventivi quest'anno, ma ci ha contattato o ci sono attivit&agrave; in corso<br />INATTIVO: non acquista da almeno un anno" ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				
				<tr><td>Preventivo?</td><td>
				<input type='checkbox' value='0' name='prev_richiesto' /> Richiesto &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				<input type='checkbox' value='0' name='prev_ricevuto' /> Ricevuto</td><td><span class='hint_tooltip' style="cursor:pointer;" title="Per selezionare clienti che hanno richiesto o ricevuto un preventivo." ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				
				<tr><td>Ti richiamiamo noi</td><td>
				<input type='checkbox' value='0' name='ric_richiesto' /> Richiesto</td><td><span class='hint_tooltip' style="cursor:pointer;" title="Per selezionare clienti che hanno chiesto di essere richiamati." ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				
				<tr><td>Clienti che hanno fatto 1+ acquisti</td><td>
				<input type='checkbox' value='0' name='1piuacquisti' /> </td><td></td></tr>
				
				<tr><td>Keyword</td><td>
				<input type='text' value='' name='keyword' /> </td><td></td></tr>
				
				<tr><td>Modalit&agrave;</td><td>
				<input type='radio' value='0' name='modalita' checked='checked' onclick="$('.select2-container').show();"/> Solo clienti che hanno fatto acquisti &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				<input type='radio' value='2' name='modalita' onclick=" $('select').each(function () { $(this).select2('val', ''); }); $('.select2-container').hide(); $('#per_prodotto').hide(); $('#per_').hide(); $('#per_categoria').hide(); $('#per_marchio').hide();" /> Solo clienti che NON hanno fatto acquisti  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				<input type='radio' value='1' name='modalita' onclick=" $('select').each(function () { $(this).select2('val', ''); }); $('.select2-container').hide(); $('#per_prodotto').hide(); $('#per_').hide(); $('#per_categoria').hide(); $('#per_marchio').hide();" /> Tutti</td><td><span class='hint_tooltip' style='cursor:pointer;' title='In modalità no acquisti e modalità tutti, verranno eliminati i filtri per prodotto, categoria e marchio perché non compatibili' ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				
				<tr><td></td><td><input type='submit' name='cercaiclienti' value='Cerca clienti' class='button' /></td><td></td></tr>
				</table>
				
			</form>
		</div>
	</form>
</div>

{else}

<div id="container-marketing-preventivi">
	<div class="form-horizontal col-lg-12">
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> Marketing su fatturato
			</div>

			<strong>Riepilogo</strong>
			<br /><br />

			{$riepilogo_html} {*  Variabile che sostituisce gli echo finchè non viene portato tutto in bootstrap *}

			<form method="post" name="form_indietro" id="form_indietro">
				<button type="button" class="btn btn-secondary" name="nuova_analisi" onclick="window.location = window.location.href;">Nuova analisi</button>
			</form>
		</div>
	</div>
</div>

{/if}

{/block}
