{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

<script type="text/javascript">
{if $updated}
	tata.success('Update avvenuto con successo!', '');
{/if}
</script>

<div>
	<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate>
		<div class="panel">
			
		{* Prima fase di ricerca e selezione del costruttore *}
		{if isset($costruttori)}
			<div class="panel-heading">
				<i class="icon-gear"></i> Seleziona il costruttore
			</div>

			<p>Questo strumento ti permette di aggiornare i fornitori di tutti i prodotti del costruttore selezionato.</p>
			
			<div class="row">
				<div class="form-group col-md-2">
					<select id="vai-costruttore" name="vai-costruttore" class="form-control textbox-fix lettura">
					{foreach $costruttori AS $key => $row}
						{if $row["name"] != ""}<option value="{$row["id_manufacturer"]}">{$row["name"]}</option>{/if}
					{/foreach}
					</select>
				</div>
				<div class="form-group col-md-1">
					<button type="submit" class="btn btn-secondary">Vai</button>
				</div>
			</div>
		{* Selezione dei fornitori *}
		{elseif isset($fornitori)}
			<div class="panel-heading">
				<i class="icon-gear"></i> Seleziona i fornitori di: <strong>{$costruttore_selezionato}</strong>
			</div>

			<div class="row">
				<div class="form-group col-md-2">
					<label>Fornitore principale</label>
					<select id="fornitore-principale" name="fornitore-principale" class="form-control textbox-fix">
					{foreach $fornitori AS $key => $row}
						{if $row["name"] != ""}<option value="{$row["id_supplier"]}"{if $id_fornitore == $row["id_supplier"]} selected{/if}>{$row["name"]}</option>{/if}
					{/foreach}
					</select>
				</div>
				<div class="col-sm-2">
					<a class="btn btn-link bt-icon" href="{$link->getAdminLink('AdminSuppliers')|escape:'html':'UTF-8'}&amp;addsupplier" target="_blank">
						<i class="icon-plus-sign"></i> {l s='Create new supplier'} <i class="icon-external-link-sign"></i>
					</a>
				</div>
			</div>
			
			{* Tutta la parte commentata relativa a other_suppliers è il vecchio metodo usato nella 1.4 *}
			<div id="altri-fornitori"> 
				{*if count($altri_fornitori)*} 
					{*foreach $altri_fornitori AS $row2*}
						<div class="row">
							<div class="form-group col-md-6">
								<label>Altri fornitori</label>
								<select id="other_suppliers" name="other_suppliers[]" class="form-control textbox-fix select2mul" multiple="multiple">
									<option value="0">-- Seleziona --</option>
								{foreach $fornitori AS $key => $row}
								<option value="{$row["id_supplier"]}"{if $row["id_supplier"]|in_array:$altri_fornitori} selected{/if}>{$row["name"]}</option>
								{/foreach}
								</select>
							</div>
						</div>
					{*/foreach*}
				{*/if*}
			</div>
			
			{*<div class="row">
				<a onclick="add_riga()" class="puntatore"><i class="icon-plus-sign verde"></i> Aggiungi forn.</a>
			</div>
			
			<div class="row">
				<a onclick="rem_riga()" class="puntatore"><i class="icon-remove-sign rosso"></i> Rimuovi forn.</a>
			</div>*}

			<br />
			
			<div class="row">
				<button type="submit" class="btn btn-secondary" name="vai-fornitori" value="{$id_costruttore_selezionato}">VAI</button>
				<button type="submit" class="btn btn-secondary">TORNA ALLA SELEZIONE DEL COSTRUTTORE</button>
			</div>

		{/if}
		</div>
	</form>
	
	{*<div style="display: none">
		<select id="other_suppliers_cpy" name="other_suppliers[]" class="form-control textbox-fix">
			<option value="0">-- Seleziona --</option>
		{foreach $fornitori AS $key => $row}
		<option value="{$row["id_supplier"]}">{$row["name"]}</option>
		{/foreach}
		</select>
	</div>*}
</div>

{/block}
