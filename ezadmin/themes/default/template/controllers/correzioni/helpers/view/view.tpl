{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

{assign var="link_gen_base" value="index.php?controller=AdminCorrezioni&amp;"}
{assign var="link_gen_home" value="index.php?controller=AdminCorrezioni"}
{assign var="token_base" value="&amp;token={getAdminToken tab='AdminCorrezioni'}"}

{if ! isset($smarty.post.da_correggere) AND ! isset($smarty.post.correzione)}

<div id="container-correzioni">
	<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate onSubmit="return confirm('Sei sicuro?')">
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> Correzioni ortografiche
			</div>

			<p>Con questo form è possibile fare correzioni ortografiche su tutto il sito (prodotti, categorie, guide). Per ragioni di sicurezza è possibile sostituire <span class="thick">solo parole intere</span> e non parti di parola. Inserisci la parola da correggere nel primo campo e la correzione nel secondo campo.</p>

			<form method="post" name="form_correzione" id="form_correzione">
				<label>Parola da correggere</label>
				<input type="text" name="da_correggere" id="da_correggere">
				<br />
				<label>Correzione</label>
				<input type="text" name="correzione" id="correzione">
				<br />
				<button type="submit" class="btn btn-secondary" name="correggi">Clicca per correggere</button>
			</form>

		</div>
	</form>
</div>

{else}

<div id="container-correzioni">
	<div class="form-horizontal col-lg-12">
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> Correzioni ortografiche
			</div>

			<p>Correzioni eseguite! E' stato creato un file log con i dettagli.</p>

			<form method="post" name="form_log_file" id="form_log_file">
				<input type="hidden" name="log_file" id="log_file" value="{$log_file}">
				<button type="submit" class="btn btn-secondary" name="confermalog">SCARICA IL FILE</button>
				<button type="button" class="btn btn-secondary" name="nuova_correzione" onclick="window.location = window.location.href;">Nuova correzione</button>
			</form>
		</div>
	</div>
</div>

{/if}

{/block}
