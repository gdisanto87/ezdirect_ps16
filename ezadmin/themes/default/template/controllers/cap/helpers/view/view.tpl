{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

{assign var="link_gen_base" value="index.php?controller=AdminCap&amp;"}
{assign var="link_gen_home" value="index.php?controller=AdminCap"}
{assign var="token_base" value="&amp;token={getAdminToken tab='AdminCap'}"}

{if ! isset($smarty.post.cap_ricerca) AND ! isset($smarty.post.ricerca_precedente)}

<div id="container-cap">
	<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate>
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> Aggiornamento CAP
			</div>

			<p>Cerca il comune a cui vuoi modificare il CAP. Puoi inserire il nome del comune, una parte del nome, oppure il cap.</p>

			<form method="post" name="form_cap_ricerca" id="form_cap_ricerca">
				<input type="text" name="cap_ricerca" id="cap_ricerca">
				<br />
				<button type="submit" class="btn btn-secondary" name="cercacap">Cerca CAP</button>
			</form>

		</div>
	</form>
</div>

{else}

<div id="container-cap">
	<div class="form-horizontal col-lg-12">
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> Aggiornamento CAP{if $smarty.post.cap != ""}:{/if} {$smarty.post.cap}
			</div>

			<p>Ecco la lista dei comuni che puoi modificare. Clicca sull'icona in ogni singola riga per salvare.</p>

			<br />

			<button type="button" class="btn btn-secondary" name="nuova_ricerca" onclick="window.location = window.location.href;">Nuova ricerca</button>

			<br />
			<br />

			<div class="row">
				<div class="col-md-4">
					<span class="thick">Comune</span>
				</div>
				<div class="col-md-4">
					<span class="thick">CAP</span>
				</div>
			</div>

			<hr />

			{foreach $caps AS $key => $row}
			<form method="post" name="form_cap" id="form_cap">
				<div class="row">
					{*}
					<div class="col-md-1 parent-center">
						Comune: 
					</div>
					{*}
					<div class="col-md-4 col-sm-5">
						<input type="text" name="comune" id="comune_{$row["id"]}" value="{$row["comune"]}">
					</div>
					{*}
					<div class="col-md-1">
						CAP: 
					</div>
					{*}
					<div class="col-md-4 col-sm-5">
						<input type="text" name="cap" id="cap_{$row["id"]}" value="{$row["cap"]}">
					</div>
					<div class="col-md-2 col-sm-2">
						<input type="hidden" name="cap_id" value="{$row["id"]}">
						<input type="hidden" name="ricerca_precedente" value="{if isset($smarty.post.cap_ricerca)}{$smarty.post.cap_ricerca}{/if}{if isset($smarty.post.ricerca_precedente)}{$smarty.post.ricerca_precedente}{/if}">
						<button type="submit" class="btn {if isset($cap_modificato) AND $cap_modificato == $row["id"]}btn-success{else}btn-secondary{/if}" name="salva_cap"><i class="icon-save"></i></button>
					</div>
				</div>
			</form>

			<br />
			{/foreach}

		</div>
	</div>
</div>

{/if}

{/block}
