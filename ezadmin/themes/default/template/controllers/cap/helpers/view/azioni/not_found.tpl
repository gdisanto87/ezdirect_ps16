<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" name="product" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-warning-sign"></i> {l s='Error'}
        </div>

        <h2 class="text-muted text-center">
            {l s='Action'} '<a class="text-danger">{$smarty.get.azione}</a>' {l s='not found'}
        </h2>
    </div>
</form>