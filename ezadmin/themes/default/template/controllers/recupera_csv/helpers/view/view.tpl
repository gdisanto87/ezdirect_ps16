{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

{assign var="link_gen_base" value="index.php?controller=AdminRecuperaCsv&amp;"}
{assign var="link_gen_home" value="index.php?controller=AdminRecuperaCsv"}
{assign var="token_base" value="&amp;token={getAdminToken tab='AdminRecuperaCsv'}"}

{if (! isset($smarty.post.submitcsv) AND ! isset($smarty.post.submitbdl)) OR $errore}

<div id="container-recuperacsv">
	<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate>
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> Recupero file CSV
			</div>

			<p>Inserisci nel box qua sotto il numero dell'ordine per il quale vuoi recuperare il CSV e quindi clicca sul pulsante:</p>

			<form method="post" name="form_csv_ordine">
				<label>Recupero Ordine</label>
				<input type="text" name="recupera_ordine" id="recupera_ordine">
				<br />
				<button type="submit" name="submitcsv" class="btn btn-secondary">Recupera CSV</button>
			</form>

			<br />
			<br />

			{if $check_employee}
			<form method="post" name="form_csv_bdl">
				<label>Recupero BDL</label>
				<input type="text" name="recupera_bdl" id="recupera_bdl">
				<br />
				<button type="submit" name="submitbdl" class="btn btn-secondary">Recupera CSV</button>
			</form>
			{/if}

		</div>
	</form>
</div>

{else}

<div id="container-recuperacsv">
	<div class="form-horizontal col-lg-12">
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> Recupero file CSV
			</div>

			<p>Il file CSV è stato recuperato con successo. Nel caso in cui il CSV non sia presente sul server significa che l'ordine è stato cancellato o ha avuto dei problemi.</p>

			<button type="button" class="btn btn-secondary" name="nuovo_recupero" onclick="window.location = window.location.href;">Nuovo recupero</button>
		</div>
	</div>
</div>

{/if}

{/block}
