<form name="uploadfile" class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate>
    <div class="panel">
        <div class="panel-heading">
            Importa file XLS e carica/aggiorna prodotti
        </div>

        {if !isset($smarty.post.azione)}

            <p>Attraverso questo form puoi importare un foglio di Excel per aggiornare i prodotti esistenti nel database o per inserirne di nuovi. Seleziona il file da importare, quindi clicca su 'Carica': il file sarà automaticamente importato, i prodotti nuovi saranno inseriti automaticamente e quelli già esistenti saranno aggiornati. A seconda delle dimensioni del file, la procedura potrebbe impiegare qualche minuto prima di essere terminata.</p>

            <input type="file" id="filexls" name="filexls">

            <br />

            <input type="checkbox" name="aggiorna" id="aggiorna" value="solo_prezzi">
            <label for="aggiorna">NON IMPORTARE, AGGIORNA SOLO I PREZZI *</label>
            <p><strong>*</strong> Aggiorna: tutti i prezzi, categoria, costruttore, fornitore, codici SKU e eSolver, ASIN, EAN, condizione, peso, sconti, online/offline/old, data disponibilità, trasporto gratuito, punti di forza, noindex, esportazione Amazon e Google Shopping, garanzia, confezione, messaggio prodotto disponibile, messaggio prodotto non disponibile.</p>
            
            <br />
            <br />

            <button type="submit" class="btn btn-secondary" name="azione" value="importa_file">Importa il file</button>
        
        {else}

            {if !$imp_catalogo['check_file']}
                <p>Errore: file non riconosciuto!</p>
            {else}
                <p>File importato con successo.</p>
            {/if}
            <button type="button" class="btn btn-secondary" name="nuova_importazione" onclick="window.location = window.location.href;">Nuova importazione</button>
        
        {/if}
    </div>
</form>