<form class="form-horizontal col-lg-12"method="post" enctype="multipart/form-data" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-download-alt"></i> Catalogo
        </div>

        <div class="row">
            <form name='esportazione' method='post'>
                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-heading">
                            Esporta per categoria
                        </div>

                        {if !isset($smarty.post.azione) || (isset($smarty.post.azione) && ($smarty.post.azione == 'esporta_xls_costruttore' || $smarty.post.azione == 'esporta_listino_costruttore'))}

                            <p>Seleziona una categoria da esportare:</p>
                            
                            {foreach $catalogo['categorie'] AS $key => $categoria}
                                <input type="radio" name="export" id="categoria_{$categoria['id']}" value="{$categoria['id']}">
                                <label for="export"> {$categoria['name']}</label> {$categoria['tempo']}<br>
                            {/foreach}

                            <hr />

                            <input type="checkbox" id="senza_seo" name="senza_seo">
                            <label for=""> Spunta per esportare file SENZA campi SEO e descrizioni (file più leggero)</label><br>

                            <input type="checkbox" id="senza_fp" name="senza_fp">
                            <label for=""> Spunta per esportare file SENZA prodotti fuori produzione (file più leggero)</label><br>

                            <br />

                            <button type="submit" class="btn btn-secondary" name="azione" value="esporta_xls_categoria">Esporta catalogo XLS</button>
                            <button type="submit" class="btn btn-secondary" name="azione" value="esporta_listino_categoria">Esporta listino breve clienti</button>

                        {else}

                            {if !$catalogo['check_cat']}
                                <p>Questa categoria non è ancora pronta.</p>
                            {else} {* Correggere: qui bisogna stampare gli echo del file incluso nel controller; farei il download diretto piuttosto che il link per scaricare *}
                                <p>File esportato con successo.</p>
                            {/if}
                            <button type="button" class="btn btn-secondary" name="nuovo_export" onclick="window.location = window.location.href;">Nuova esportazione</button>
                        
                        {/if}
                    </div>
                </div>
            </form>

            <form name='esportazione-costruttore' method='post'>
                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-heading">
                            Esporta per costruttore
                        </div>

                        {if !isset($smarty.post.azione) || (isset($smarty.post.azione) && ($smarty.post.azione == 'esporta_xls_categoria' || $smarty.post.azione == 'esporta_listino_categoria'))}

                            <p>Seleziona il costruttore</p>

                            <input type="checkbox" id="costruttore_tutti" name="per-costruttore[]" value="tutti">
                            <label for="costruttore_tutti"> Tutti i costruttori</label><br>

                            <div style='height:325px; overflow:auto'>
                            {foreach $catalogo['costruttori'] AS $key => $costruttore}
                                <input type="checkbox" name="per-costruttore[]" id="costruttore_{$costruttore['id']}" value="{$costruttore['id']}">
                                <label for="per-costruttore[]"> {$costruttore['name']}</label><br>
                            {/foreach}
                            </div>

                            <hr />

                            <input type="checkbox" id="senza_seo" name="senza_seo">
                            <label for="senza_seo"> Spunta per esportare file SENZA campi SEO e descrizioni</label><br>

                            <input type="checkbox" id="senza_fp" name="senza_fp">
                            <label for="senza_fp"> Spunta per esportare file SENZA prodotti fuori produzione</label><br>

                            <input type="checkbox" id="includi_inattivi" name="includi_inattivi">
                            <label for="includi_inattivi"> Includi prodotti inattivi</label><br>

                            <input type="checkbox" id="includi_interno" name="includi_interno">
                            <label for="includi_interno"> Includi prodotti del listino interno</label><br>

                            <input type="checkbox" id="includi_scaricabili" name="includi_scaricabili">
                            <label for="includi_scaricabili"> Includi prodotti scaricabili</label><br>

                            <br />

                            <button type="submit" class="btn btn-secondary" name="azione" value="esporta_xls_costruttore">Esporta catalogo XLS</button>
                            <button type="submit" class="btn btn-secondary" name="azione" value="esporta_listino_costruttore">Esporta listino breve clienti</button>
                        
                        {else}

                            {* Correggere: qui bisogna stampare gli echo del file incluso nel controller; farei il download diretto piuttosto che il link per scaricare *}
                            <p>File esportato con successo.</p>
                            <button type="button" class="btn btn-secondary" name="nuovo_export" onclick="window.location = window.location.href;">Nuova esportazione</button>
                        
                        {/if}
                    </div>
                </div>
            </form>
        </div>
    </div>
</form>