{if count($partite_iva['duplicati'])}
    {foreach $partite_iva['duplicati'] AS $key => $piva_doppie}
    <form class="form-horizontal col-lg-12"  method="post" enctype="multipart/form-data" novalidate>
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-tasks"></i> {l s='VAT number'}: {$piva_doppie['vat_number']} <span class="badge squadrato">{count($piva_doppie['clienti_doppi'])}</span>

                <div class="panel-heading-action">
                
                </div>
            </div>

            <table class="table">
                <thead>
                    <tr>
                        <th><span class="title_box thick">{l s='ID CRM'}</span></th>
                        <th><span class="title_box thick">{l s='P.IVA'}</span></th>
                        <th><span class="title_box thick">{l s='Email'}</span></th>
                        <th><span class="title_box thick">{l s='Azienda'}</span></th>
                        <th><span class="title_box thick">{l s='Nome'}</span></th>
                        <th><span class="title_box thick">{l s='Cognome'}</span></th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $piva_doppie['clienti_doppi'] AS $key => $clienti_doppi}
                    <tr class="menu_links" onclick="document.location='{$clienti_doppi['href']}';" onmouseover="">
                        <td>{if $clienti_doppi['id_customer'] == NULL}-{else}<a >{$clienti_doppi['id_customer']}</a>{/if}</td>
                        <td>{if $clienti_doppi['vat_number'] == NULL}-{else}{$clienti_doppi['vat_number']}{/if}</td>
                        <td>{if $clienti_doppi['email'] == NULL}-{else}{$clienti_doppi['email']}{/if}</td>
                        <td>{if $clienti_doppi['company'] == NULL}-{else}{$clienti_doppi['company']}{/if}</td>
                        <td>{if $clienti_doppi['firstname'] == NULL}-{else}{$clienti_doppi['firstname']}{/if}</td>
                        <td>{if $clienti_doppi['lastname'] == NULL}-{else}{$clienti_doppi['lastname']}{/if}</td>
                    </tr>
                    {/foreach}
                </tbody>
            </table>					
            
        </div>
    </form>
    {/foreach}
{else}
<form class="form-horizontal col-lg-12"method="post" enctype="multipart/form-data" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-warning-sign"></i> {l s='There are no duplicates'}
        </div>

        <h2 class="text-muted text-center">
            {l s='No duplicate VAT numbers were found.'} {if isset($easteregg)}(Meglio così){/if}
        </h2>
    </div>
</form>
{/if}