<form class="form-horizontal col-lg-12"method="post" enctype="multipart/form-data" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-download-alt"></i> Esportatore universale per comparaprezzi
        </div>

        <p>Seleziona il comparatore su cui vuoi lavorare</p>

        <p>
            <button type="button" class="btn btn-secondary">Trovaprezzi</button>
            <button type="button" class="btn btn-secondary">Google Shopping</button>
            <button type="button" class="btn btn-secondary">Amazon</button>
            <button type="button" class="btn btn-secondary">Tutti (no Amazon)</button>
        </p>

    </div>
</form>