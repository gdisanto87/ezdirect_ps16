<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" name="esportazione-listini" novalidate>
    <div class="panel">
        <div class="panel-heading">
            Esporta listini
        </div>
        {if isset($listini['carrelli'])}
            <p>Scegli i carrelli</p>

            <hr />
            {foreach $listini['carrelli'] AS $key => $carrello}
                <input type="checkbox" id="carrello_{$carrello['id']}" name="carrello[]" value="carrello_{$carrello['id']}">
                <label for="carrello_{$carrello['id']}"> {$carrello['name']}</label><br>
            {/foreach}
            <hr />

            <input type="checkbox" id="riv_1" name="rivenditori" value="riv_1">
            <label for="riv_1"> Spunta per esportare il listino per rivenditori 1</label><br>

            <input type="checkbox" id="riv_2" name="rivenditori2" value="riv_2">
            <label for="riv_2"> Spunta per esportare il listino per rivenditori 2</label><br>

            <br />

            <button type="submit" class="btn btn-secondary" name="azione" value="esporta_xls">Esporta catalogo XLS</button>
        {else}
            <p>Creato nuovo file di Excel... !</p>
            <br />
            {* CORREGGERE: Prova sul server 46.101.235.11; sostituire con http://www.ezdirect.it/ *}
            <p>Il file &egrave; pronto per essere scaricato: <a onclick='window.onbeforeunload = null' href='http://46.101.235.11/ezadmin/esportazione-catalogo/catalogo_xls/listini-ez.xls'>clicca qui per eseguire il download</a>!</p>
            
            <button type="button" class="btn btn-secondary" name="nuovo_export_listini" onclick="window.location = window.location.href;">Nuova esportazione</button>
        {/if}
    </div>
</form>