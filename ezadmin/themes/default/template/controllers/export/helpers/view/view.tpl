{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

{assign var="link_gen_base" value="index.php?controller=AdminExport&amp;"}
{assign var="link_gen_home" value="index.php?controller=AdminExport"}
{assign var="token_base" value="&amp;token={getAdminToken tab='AdminExport'}"}

{if $is_agente == 1}
<div id="container-export">
	<form class="form-horizontal col-lg-12"method="post" enctype="multipart/form-data" novalidate>
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-warning-sign"></i> {l s='Access denied'}
			</div>

			<h2 class="text-muted text-center">
				{l s='You do not have the necessary permissions to view this tab.'}
			</h2>
		</div>
	</form>
</div>
{else}
<div id="container-export">

	<nav class="navbar navbar-default">
		<div class="container-fluid">
			{* Questo è necessario per far funzionare la modalità mobile *}
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					{if $is_agente == 0}<li {if ! isset($tab_name)}class="active"{/if}><a href="{$link_gen_home}{$token_base}">Catalogo</a></li>{/if}
					{if $is_agente == 0}<li {if $tab_name == "import_catalogo"}class="active"{/if}><a href="{$link_gen_base}tab_name=import_catalogo{$token_base}">Importa Catalogo</a></li>{/if}
					{if $is_agente == 0}<li {if $tab_name == "comparaprezzi"}class="active"{/if}><a href="{$link_gen_base}tab_name=comparaprezzi{$token_base}">Comparaprezzi</a></li>{/if}
					{if $is_agente == 0}<li {if $tab_name == "mailup"}class="active"{/if}><a href="{$link_gen_base}tab_name=mailup{$token_base}">MailUp</a></li>{/if}
					{if $is_agente == 0}<li {if $tab_name == "listini"}class="active"{/if}><a href="{$link_gen_base}tab_name=listini{$token_base}">Listini</a></li>{/if}

				</ul>
			</div>
			
		</div>
	</nav>
	
	<div class="col-lg-12">
		<div class="row">
		
		{* CATALOGO *}
		{if ! isset($tab_name)}
			{include file="./tabs/catalogo.tpl"}
		
		{* IMPORTA CATALOGO *}
		{elseif $tab_name == "import_catalogo"}
			{include file="./tabs/import_catalogo.tpl"}
		
		{* COMPARAPREZZI tutto attaccato *}
		{elseif $tab_name == "comparaprezzi"}
			{include file="./tabs/comparaprezzi.tpl"}
		
		{* MAILUP *}
		{elseif $tab_name == "mailup"}
			{include file="./tabs/mailup.tpl"}

		{* LISTINI *}
		{elseif $tab_name == "listini"}
			{include file="./tabs/listini.tpl"}

		{/if}

		</div>
	</div>
</div>
{/if}

{/block}
