<form class="form-horizontal col-lg-12"method="post" enctype="multipart/form-data" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-download-alt"></i> Esportazione MailUp
        </div>

        <p>Clicca sul file da prelevare. Puoi selezionare un periodo con gli appositi campi data. Se non selezioni il periodo, viene esportato il file degli ultimi 30 giorni. Se un indirizzo su Mailup è doppio, non viene esportato.</p>

        <p> {* Correggere date: type text con datepicker *}
        Dal <input type="text" id="arco_dal" name="arco_dal"> al <input type="text" id="arco_al" name="arco_al">
        </p>

        <br />

        <p>
            <button type="submit" class="btn btn-secondary" name="azione" value="aziende_tutti">Aziende TUTTI</button>
            <button type="submit" class="btn btn-secondary" name="azione" value="privati_tutti">Privati TUTTI</button>
            <button type="submit" class="btn btn-secondary" name="azione" value="rivenditori_1_tutti">Rivenditori 1 TUTTI</button>
            <button type="submit" class="btn btn-secondary" name="azione" value="richiesta_rivenditori_tutti">Rich. Riv. TUTTI</button>
            <button type="submit" class="btn btn-secondary" name="azione" value="ex_rivenditori_tutti">Ex Rivenditori TUTTI</button>
        </p>
        <p>
            <button type="submit" class="btn btn-secondary" name="azione" value="aziende_gdpr">Aziende GDPR</button>
            <button type="submit" class="btn btn-secondary" name="azione" value="privati_gdpr">Privati GDPR</button>
            <button type="submit" class="btn btn-secondary" name="azione" value="rivenditori_1_gdpr">Rivenditori 1 GDPR</button>
            <button type="submit" class="btn btn-secondary" name="azione" value="richiesta_rivenditori_gdpr">Rich. Riv. GDPR</button>
            <button type="submit" class="btn btn-secondary" name="azione" value="ex_rivenditori_gdpr">Ex Rivenditori GDPR</button>
        </p>
    </div>
</form>