{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

<div id="container-pin">
	<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate>
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> PIN Centralino
			</div>

			<p>Inserisci il file nel form per ottenere un file di testo coi campi dei PIN compilati:</p>

			<form>
				<input type="file" id="file_pin" name="file_pin">
				<br />
				<button type="button" class="btn btn-secondary">Carica</button>
			</form>

		</div>
	</form>
</div>


{/block}
