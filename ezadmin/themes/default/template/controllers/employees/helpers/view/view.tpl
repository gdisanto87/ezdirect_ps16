{* Creato da Carolina, non presente in PrestaShop 1.6 *}

{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

<div id="container-employee">

	{* PROVA Menu con panel collapsable *}
	{*<div class="row">
		<div class="col-lg-12" id="tabs">

			<div class="panel">
				<div class="panel-heading">
					<i class="icon-list"></i> {l s='Menu'}
				</div>

				<div class="btn-toolbar" role="toolbar">
					<div class="btn-group mr-2" role="group">
						<button type="button" class="btn btn-default" data-toggle="collapse" data-target="#collapse_ra" aria-expanded="false" aria-controls="collapse_ra">{l s='Recent Activities'}</button>
						<button type="button" class="btn btn-default" data-toggle="collapse" data-target="#collapse_d" aria-expanded="false" aria-controls="collapse_d">{l s='Employee Details'}</button>
					</div>
					{if (isset($employee->id_profile) && $employee->id_profile == 7)}
					<div class="btn-group mr-2" role="group">
						<button class="btn btn-default">{l s='Area Agente'}</button>
					</div>
					{/if}
				</div>
			</div>

		</div>
	</div>*}

{* <div class="collapse" id="collapse_ra"> *}
	<div class="row">

		{* Attività recenti *}
		<div class="col-lg-12" id="activities">

			<div class="panel">
				<div class="panel-heading">
					<i class="icon-calendar"></i> {l s='Recent activities'} <span class="badge">{count($activities)}</span>
				</div>
				{if ($activities && count($activities))}
					<table class="table" id="tabella_employees_ra">
						<thead>
							<tr>
								<th><span class="title_box ">{l s='Type'}</span></th>
								<th><span class="title_box ">{l s='ID'}</span></th>
								<th><span class="title_box ">{l s='Customer'}</span></th>
								<th><span class="title_box ">{l s='Customer Type'}</span></th>
								<th><span class="title_box ">{l s='Created by'}</span></th>
								<th><span class="title_box ">{l s='Conv.'}</span></th>
								<th><span class="title_box ">{l s='Status'}</span></th>
								<th><span class="title_box ">{l s='Expired'}</span></th>
								<th><span class="title_box ">{l s='Subject'}</span></th>
								<th><span class="title_box ">{l s='Total'}</span></th>
								<th><span class="title_box ">{l s='Date add'}</span></th>
								<th><span class="title_box ">{l s='Last modified'}</span></th>
							</tr>
						</thead>
						<tbody>
						{foreach $activities AS $activity}
							<tr class="pointer" onclick="document.location = '{$activity['href']}'">
								<td>{$activity['type']}</td>
								<td>{$activity['id']}</td>
								<td>{$activity['customer']}</td>
								<td>{$activity['customer_type']}</td>
								<td>{$activity['created_by']}</td>
								<td>{$activity['conv']}</td>
								<td><span {$activity['status_style']}>{$activity['status']}</span></td>
								<td class="thick">{$activity['expired']}</td>
								<td>{$activity['subject']}</td>
								<td>{$activity['total']}</td>
								<td>{$activity['date_add']}</td>
								<td>{$activity['last_modified']}</td>
							</tr>
						{/foreach}
						</tbody>
					</table>
				{else}
				<p class="text-muted text-center">
					{l s='No recent activities'}
				</p>
				{/if}
			</div>

		</div>

	</div>
{* </div> *}

	<div class="row">

		{* Dettagli *}
		<div class="col-lg-12" id="details">

			<div class="panel">		
				<div class="panel-heading">
					<i class="icon-user"></i>{l s='Employee details'}

					<div class="panel-heading-action">
						<a class="btn btn-default" href="{$current|escape:'html':'UTF-8'}&amp;id_employee={$employee->id|intval}&amp;updateemployee&amp;token={$token|escape:'html':'UTF-8'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
							<i class="icon-edit"></i>
							{l s='Edit'}
						</a>
						{* Icona per espandere sezione; da trasformare in meno per chiusura *}
						{* PROVA! Perchè si posiziona sopra a Edit? *}
						{* <a role="button" class="btn btn-default" data-toggle="collapse" href="#collapse_d" aria-expanded="false" aria-controls="collapse_d">
							<i class="icon-plus-sign"></i>
						</a> *}
					</div>
					
				</div>
				
				{* <div class="collapse" id="collapse_d"> *}
					<div class="form-row">
						<div class="row">
							
							<div class="form-group col-md-3">
								<label for="firstname">{l s='Firstname'}</label>
								<p class="form-control-static">
									{if isset($employee->firstname)}{$employee->firstname}{else}{l s='Unknown'}{/if}
								</p>
							</div>
							
							<div class="form-group col-md-3">
								<label for="lastname">{l s='Lastname'}</label>
								<p class="form-control-static">
									{if isset($employee->lastname)}{$employee->lastname}{else}{l s='Unknown'}{/if}
								</p>
							</div>

							<div class="form-group col-md-3">
								<label for="email">{l s='Email'} <a href="mailto:{$employee->email}"><i class="icon-envelope"></i> {l s='Send'}</a></label>
								<p class="form-control-static">
									{if isset($employee->email)}{$employee->email}{else}{l s='Unknown'}{/if}
								</p>
							</div>

							<div class="form-group col-md-3">
								<label for="profile">{l s='Profile'}</label>
								<p class="form-control-static">
									{if isset($employeeProfile)}{$employeeProfile}{else}{l s='Unknown'}{/if}
								</p>
							</div>

						</div>
					</div>

					<hr />

					<div class="form-row">
						<div class="row">

							<div class="form-group col-md-3">
								<label for="optin">{l s='Prestashop newsletter'}</label>
								<p class="form-control-static">
									{if (isset($employee->optin) && $employee->optin)}{l s='Yes'}{else}{l s='No'}{/if}
								</p>
							</div>

							<div class="form-group col-md-3">
								<label for="default_page">{l s='Menu orientation'}</label>
								<p class="form-control-static">
									{if (isset($employee->bo_menu) && $employee->bo_menu)}{l s='Left'}{else}{l s='Top'}{/if}
								</p>
							</div>

							<div class="form-group col-md-3">
								<label for="language">{l s='Language'}</label>
								<p class="form-control-static">
									{if isset($employeeLanguage->name)}{$employeeLanguage->name}{else}{l s='Unknown'}{/if}
								</p>
							</div>

							<div class="form-group col-md-3">
								<label for="active">{l s='Active'}</label>
								<p class="form-control-static">
									{if (isset($employee->active) && $employee->active)}{l s='Yes'}{else}{l s='No'}{/if}
								</p>
							</div>

						</div>
					</div>

					<hr />

					<div class="alert alert-info">{l s='Press "Edit" to see more options.'}</div>
				{* </div> *}
			</div>
		</div>
	</div>

</div>
{/block}
