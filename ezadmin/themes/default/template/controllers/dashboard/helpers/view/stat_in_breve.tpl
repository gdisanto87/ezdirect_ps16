{* Statistiche in breve *}

{assign var=periodo value=$stat_in_breve['periodo']}
{assign var=anno value=$stat_in_breve['anno']}
{assign var=clienti value=$stat_in_breve['clienti']}
{assign var=efficienza value=$stat_in_breve['efficienza']}
{assign var=target value=$stat_in_breve['target']}

<section class="panel" id="stat_in_breve">
    <h3><i class="icon-signal"></i> Statistiche in breve</h3>

    <section id="base_periodo">
        <header>Su base periodo - <a style="color:white; font-size:90%" href="{$link->getAdminLink('AdminStats')}">vai a stats</a></header>
        <table class="table">
            <tr><td>Ricavi</td><td style="text-align:right">{$periodo['ricavi']}</td></tr>
            <tr><td>di cui Amazon</td><td style="text-align:right">{$periodo['amazon']}</td></tr>
            <tr><td>di cui Servizi</td><td style="text-align:right">{$periodo['servizi']}</td></tr>
            <tr><td>Ordini</td><td style="text-align:right">{$periodo['ordini']}</td></tr>
            <tr><td>Target quarter % ({$periodo['target']})</td><td style="text-align:right">{$periodo['target_p']} %</td></tr>
        </table>
    </section>

    <section id="base_anno">
        <header>Su base anno - <a style="color:white; font-size:90%" href="{$link->getAdminLink('AdminStats')}">vai a stats</a></header>
        <table class="table">
            <tr><td>Ricavi</td><td style="text-align:right">{$anno['ricavi']}</td></tr>
            <tr><td>di cui Amazon</td><td style="text-align:right">{$anno['amazon']}</td></tr>
            <tr><td>di cui Servizi</td><td style="text-align:right">{$anno['servizi']}</td></tr>
            <tr><td>Ordini</td><td style="text-align:right">{$anno['ordini']}</td></tr>
            <tr><td>Target anno % ({$anno['target']})</td><td style="text-align:right">{$anno['target_p']} %</td></tr>
        </table>
    </section>

    <section id="clienti_totale">
        <header>Clienti (totale)</header>
        <table class="table">
            {foreach $clienti['gruppi_aziende'] as $g}
            <tr><td><a href="{$link->getAdminLink('AdminCustomers')}&submitFiltercustomer=1&customerFilter_a!id_default_group={$g['id']}">{$g['nome']}</a></td><td style="text-align:right">{$g['tot']}</td></tr>
            {/foreach}
			<tr><td><strong>Totale aziende</strong></td><td style="text-align:right"><strong>{$clienti['totale_aziende']}</strong></td></tr>
            {foreach $clienti['gruppi_privati'] as $g}
            <tr><td><a href="{$link->getAdminLink('AdminCustomers')}&submitFiltercustomer=1&customerFilter_a!id_default_group={$g['id']}">{$g['nome']}</a></td><td style="text-align:right">{$g['tot']}</td></tr>
            {/foreach}
			<tr><td><strong>Totale anagrafiche clienti</strong></td><td style="text-align:right"><strong>{$clienti['totale_privati']}</strong></td></tr>
            {foreach $clienti['gruppi_altri'] as $g}
            <tr><td><a href="{$link->getAdminLink('AdminCustomers')}&submitFiltercustomer=1&customerFilter_a!id_default_group={$g['id']}">{$g['nome']}</a></td><td style="text-align:right">{$g['tot']}</td></tr>
            {/foreach}
			<tr><td><strong>Totale anagrafiche</strong></td><td style="text-align:right"><strong>{$clienti['totale_clienti']}</strong></td></tr>
        </table>
    </section>

    <section id="efficienza_periodo">
        <header>Efficienza (periodo: {$mese} {$homeStats['year']})</header>
        <table class="table">
			<tr><td>Chiusura ticket</td><td style="text-align:right">{$efficienza['hourstkt']} ore lav.</td></tr>
			<tr><td>Chiusura preventivo</td><td style="text-align:right">{$efficienza['hoursprv']} ore lav.</td></tr>
			<tr><td>Chiusura todo</td><td style="text-align:right">{$efficienza['hourstodo']} ore lav.</td></tr>
		</table>
    </section>

    <section id="target_anno">
        <header>Target anno - <a style="color:white; font-size:90%" href="javascript:void(0)" onclick="javascript:accoda()";>Clicca per aggiungere costr.</a></header>
        <table class="table">
            {foreach $target['hms'] as $hm}
            <tr id="tr_manufacturer_{$hm['id']}">
                <td>
                    <select class="select-costruttori" id="choose-manufacturer-{$hm['id']}" onchange="getManufacturerTarget(this.value, 'm-choose-manufacturer-{$hm['id']}')">
                        <option value="0">-- Seleziona costruttore --</option>
                        {foreach $target['costruttori'] as $c}
                        <option value="{$c['id_manufacturer']}" {if $hm['id'] == $c['id_manufacturer']}selected="selected"{/if}>{$c['name']}</option>
                        {/foreach}
                    </select>
                    <br />Target: {$hm['target']}
                </td>
                <td><a href="javascript:void(0)" onclick="javascript:rimuovi({$hm['id']})";><img src="../img/admin/delete.gif" alt="Togli" title="Togli" /></a></td>
                <td style="text-align:right" id="m-choose-manufacturer-{$hm['id']}">{$hm['target_percent']} %</td>
            </tr>
            {/foreach}
        </table>
    </section>
</section>

{* Correggere: spostare in un file js a parte se possibile; testare funzioni e ajax *}
<script type="text/javascript">
    {literal}
    function getManufacturerTarget(id_manufacturer, id_element) {
        var costruttori = "";
        var arrayCostruttori = document.getElementsByClassName("select-costruttori");
        for (var i = 0; i < arrayCostruttori.length; ++i) {
            var item = (arrayCostruttori[i].value);  
            costruttori += item+";";
        }

        $.ajax({
            url:"ajax.php?getManufacturerQuarter=y",
            type: "POST",
            data: {
                costruttori: costruttori,
                getManufacturerQuarter: "y",
                id_manufacturer: id_manufacturer
            },
            success:function(resp){
                document.getElementById(id_element).innerHTML = resp + "%";
            },
            error: function(xhr,stato,errori){
                return "Errore!";
            }
        });
    }
    
    function accoda() {
        var possible = "0123456789";
        var randomid = "";
        for(var i=0; i < 11; i++) {
            randomid += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        
        // Correggere - testare:
        var string = '<tr id="tr_manufacturer_'+randomid+'"><td><select class="select-costruttori" id="choose-manufacturer-'+randomid+'" onchange="getManufacturerTarget(this.value, \'m-choose-manufacturer-'+randomid+'\')"><option value="0">-- Seleziona costruttore --</option>';
        
        {/literal}
        {foreach $costruttori as $c}
            string = string + '<option value="{$c['id_manufacturer']}">{$c['name']}</option>'; 
        {/foreach}
        {literal}

        string = string + '</select><a href="javascript:void(0)" onclick="javascript:rimuovi('+randomid+')";><img src="../img/admin/delete.gif" alt="Togli" title="Togli" /> </a></td><td style="text-align:right" id="m-choose-manufacturer-'+randomid+'"></td></tr>';
        $(string)).appendTo("#table_target");
    }

    function rimuovi(id) {
        document.getElementById('tr_manufacturer_'+id).innerHTML = "";
        
        var costruttori = "";
        var arrayCostruttori = document.getElementsByClassName("select-costruttori");
        for (var i = 0; i < arrayCostruttori.length; ++i) {
            var item = (arrayCostruttori[i].value);  
            costruttori += item+";";
        }
        
        $.ajax({
            url:"ajax.php?removeManufacturerFromCookie=y",
            type: "POST",
            data: { 
                costruttori: costruttori
            },
            success:function(resp){
                alert("Costruttore rimosso con successo");
            },
            error: function(xhr,stato,errori){
                return "Errore!";
            }
        });
    }
    {/literal}
</script>