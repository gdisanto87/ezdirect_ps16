{* Selzione periodo e grafici a torta del periodo selezionato *}

{assign var=categorie value=$grafici_periodo['categorie']}
{assign var=costruttori value=$grafici_periodo['costruttori']}

<section class="panel" id="seleziona_periodo">
    <form method="post"> 
        <div class="form-row">
            <div class="row">
                <div class="form-group col-md-12" style="margin-bottom:0px;">
                    <label class="col-md-3">Seleziona il periodo:</label>
                    <select class="col-md-3" style="margin-right:10px;" name="select-month" id="select-month">
                        <option value="Jan" {if $homeStats['period'] == 'Jan'} selected="selected"{/if}>Gennaio</option>
                        <option value="Feb" {if $homeStats['period'] == 'Feb'} selected="selected"{/if}>Febbraio</option>
                        <option value="Mar" {if $homeStats['period'] == 'Mar'} selected="selected"{/if}>Marzo</option>
                        <option value="Apr" {if $homeStats['period'] == 'Apr'} selected="selected"{/if}>Aprile</option>
                        <option value="May" {if $homeStats['period'] == 'May'} selected="selected"{/if}>Maggio</option>
                        <option value="Jun" {if $homeStats['period'] == 'Jun'} selected="selected"{/if}>Giugno</option>
                        <option value="Jul" {if $homeStats['period'] == 'Jul'} selected="selected"{/if}>Luglio</option>
                        <option value="Aug" {if $homeStats['period'] == 'Aug'} selected="selected"{/if}>Agosto</option>
                        <option value="Sep" {if $homeStats['period'] == 'Sep'} selected="selected"{/if}>Settembre</option>
                        <option value="Oct" {if $homeStats['period'] == 'Oct'} selected="selected"{/if}>Ottobre</option>
                        <option value="Nov" {if $homeStats['period'] == 'Nov'} selected="selected"{/if}>Novembre</option>
                        <option value="Dec" {if $homeStats['period'] == 'Dec'} selected="selected"{/if}>Dicembre</option>
                        <option value="q1" {if $homeStats['period'] == 'q1'} selected="selected"{/if}>Quarter 1</option>
                        <option value="q2" {if $homeStats['period'] == 'q2'} selected="selected"{/if}>Quarter 2</option>
                        <option value="q3" {if $homeStats['period'] == 'q3'} selected="selected"{/if}>Quarter 3</option>
                        <option value="q4" {if $homeStats['period'] == 'q4'} selected="selected"{/if}>Quarter 4</option>
                        <option value="anno" {if $homeStats['period'] == 'anno'} selected="selected"{/if}>Intero anno</option>
                    </select>
                    <select class="col-md-1" style="margin-right:10px;" name="select-year" id="select-year" style="height:28px;">
                        {for $i=2010 to $anno_attuale}
                        <option value="{$i}" {if $homeStats['year'] == $i} selected="selected"{/if}>{$i}</option>
                        {/for}
                    </select>
                    <input type="submit" name="vai-home" value="Vai" class="btn btm-secondary col-md-2" style="margin-right:10px;" />
                    <input type="submit" name="reset-home" value="Resetta" class="btn btm-secondary col-md-2" style="margin-right:10px;" />
                </div>
            </div>
        </div>
	</form>
</section>

<section class="panel" id="grafici_periodo">
    <h3><i class="icon-signal"></i> Statistiche del periodo</h3>

    <section id="grafico_categorie">
        <header>{$mese} - Categorie</header>

        <div id="chart_categorie_div"></div>

        <script type="text/javascript">
            {literal}
            // Load the Visualization API and the corechart package.
            google.charts.load("current", {"packages":["corechart"]});

            // Set a callback to run when the Google Visualization API is loaded.
            google.charts.setOnLoadCallback(drawChart);

            // Callback that creates and populates a data table, instantiates the pie chart, passes in the data and draws it.
            function drawChart() {
                // Create the data table.
                var data = google.visualization.arrayToDataTable([
                    ['Categoria', 'Vendite'],
                    {/literal}{$categorie['category_data_chart']}{literal}
                    ['Altre', {/literal}{$categorie['other_category_tot']}{literal}]
                ]);

                // Set chart options
                var options = {
                    "title":"Grafico categorie",
                    is3D: true,
                    legend: "labeled",
                    pieSliceText: "none",
                    chartArea:{left:20,top:0,width:"97%",height:"97%"},
                    "width":600,
                    "height":300
                };

                var chart = new google.visualization.PieChart(document.getElementById("chart_categorie_div"));
                chart.draw(data, options);
            }
            {/literal}
        </script>

    </section>

    <section id="grafico_costruttori">
        <header>{$mese} - Costruttori</header>

        <div id="chart_costruttori_div"></div>

        <script type="text/javascript">
            {literal}
            // Load the Visualization API and the corechart package.
            // => già fatto nello script precedente

            // Set a callback to run when the Google Visualization API is loaded.
            google.charts.setOnLoadCallback(drawChart2);

            // Callback that creates and populates a data table, instantiates the pie chart, passes in the data and draws it.
            function drawChart2() {
                // Create the data table.
                var data = google.visualization.arrayToDataTable([
                    ['Costruttore', 'Vendite'],
                    {/literal}{$costruttori['manufacturer_data_chart']}{literal}
                    ['Altri', {/literal}{$costruttori['other_manufacturer_tot']}{literal}]
                ]);

                // Set chart options
                var options = {
                    "title":"Grafico costruttori",
                    is3D: true,
                    legend: "labeled",
                    pieSliceText: "none",
                    chartArea:{left:20,top:0,width:"97%",height:"97%"},
                    "width":600,
                    "height":300
                };

                // Instantiate and draw our chart, passing in some options.
                var chart = new google.visualization.PieChart(document.getElementById("chart_costruttori_div"));
                chart.draw(data, options);
            }
            {/literal}
        </script>

    </section>
</section>