{* Serivizio clienti - Ultime attività *}

<section class="panel" id="servizio_clienti">
    <h3><i class="icon-user"></i> Servizio clienti</h3>
    <div class="row"> {* Correggere: mancano i parametri in AdminPlanner? *}
        <a href='{$link->getAdminLink("AdminPlanner")}&type=2'><img src='../img/ticket-a-120.jpg' alt='Ticket amministrativi' title='Ticket amministrativi' /></a>
        <a href='{$link->getAdminLink("AdminPlanner")}&type=8'><img src='../img/ticket-d-120.jpg' alt='Ticket ordini' title='Ticket ordini' /></a>
        <a href='{$link->getAdminLink("AdminPlanner")}&type=4'><img src='../img/ticket-t-120.jpg' alt='Ticket assistenza' title='Ticket assistenza' /></a>
        <a href='{$link->getAdminLink("AdminPlanner")}&type=preventivo'><img src='../img/ticket-p-120.jpg' alt='Ticket commerciale' title='Ticket commerciale' /></a>
        <a href='{$link->getAdminLink("AdminPlanner")}&type=3'><img src='../img/ticket-v-120.jpg' alt='Ticket rivenditori' title='Ticket rivenditori' /></a>
        <a href='{$link->getAdminLink("AdminPlanner")}'><img src='../img/ticket-tutti-120.jpg' alt='Tutti i ticket' title='Tutti i ticket' /></a>
        <a href='{$link->getAdminLink("AdminPlanner")}&data=oggi'><img src='../img/attivita-oggi-120.gif' alt='Attivita di oggi' title='Attivita di oggi' /></a>
        <br /><br />
    </div>
    <div class="row">
        {$ultime_attivita}
    </div>
</section>