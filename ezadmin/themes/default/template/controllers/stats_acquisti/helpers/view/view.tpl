{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

{if !$check_employee}
<div id="container-stats-acquisti">
	<div class="form-horizontal col-lg-12">
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-warning-sign"></i> {l s='Access denied'}
			</div>

			<h2 class="text-muted text-center">
				{l s='You do not have the necessary permissions to view this tab.'}
			</h2>			
		</div>
	</div>
</div>
{else}
<div id="container-stats-acquisti">
	<div class="form-horizontal col-lg-12">
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> Acquisti
			</div>
			{* Correggere: bootstrapizzare *}
			<strong class="rosso">ATTENZIONE: gli acquisti vengono aggiornati tutte le domeniche alle ore 02:00.</strong>
			<br /><br />

			{$riepilogo_html} {*  Variabile che sostituisce gli echo finchè non viene portato tutto in bootstrap *}
		
		</div>
	</div>
</div>
{/if}
{/block}
