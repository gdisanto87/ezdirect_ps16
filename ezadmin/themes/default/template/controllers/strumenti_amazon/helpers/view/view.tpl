{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

<div id="container-strumenti-amazon">
	<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate>
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cogs"></i> Strumenti Amazon
			</div>

			<p>In questa pagina hai a disposizione diversi strumenti per gestire Amazon. <span class="thick">ATTENZIONE</span>: Amazon consente di fare solo un numero limitato di operazioni in un'ora. Se viene raggiunto il limite, le procedure mostreranno messaggi d'errore.</p>

			<p>
				<a class="btn btn-primary" id="aggiornamento" href="{$link->getAdminLink('AdminStrumentiAmazon')|escape:'html':'UTF-8'}&amp;submitfeed=y" onclick="return confirm('Sei sicuro? La pagina si ricaricherà e la procedura durerà alcuni minuti.')">Invia/Aggiorna prodotti, prezzi e quantità (procedura unica)</a>
				<a class="btn btn-primary" id="ordini" href="{$link->getAdminLink('AdminStrumentiAmazon')|escape:'html':'UTF-8'}&amp;listorders=y" onclick="return confirm('Sei sicuro? La pagina si ricaricherà e la procedura durerà alcuni minuti.')">Importazione ordini</a>
				<a class="btn btn-primary" id="carrelli_amazon" onclick="window.onbeforeunload = null" href="carrelli-amazon.php">Scarica file carrelli preordinati</a>
				<a class="btn btn-primary" id="aggiornamento" href="{$link->getAdminLink('AdminStrumentiAmazon')|escape:'html':'UTF-8'}&amp;submitfattura=y" onclick="return confirm('Sei sicuro? La pagina si ricaricherà e la procedura durerà alcuni minuti.')">Carica fattura</a>
				{* <a class="btn btn-primary" id="prodotti" href="{$link->getAdminLink('AdminStrumentiAmazon')|escape:'html':'UTF-8'}&amp;amazonfeed=y" onclick="return confirm('Sei sicuro? La pagina si ricaricherà e la procedura durerà alcuni minuti.')">Aggiorna strumento disponibilit&agrave; dei marketplace</a> *}
			</p>

		</div>

		{if $smarty.get.listorders == 'y' && ($smarty.post.importa === null || !isset($smarty.post.importa))}
			<div class="panel"> {* Correggere: trasformare in date *}
				<div class="row">
					<form method="post" action="{$link->getAdminLink('AdminStrumentiAmazon')|escape:'html':'UTF-8'}&amp;listorders=y">
						<p>Seleziona l'intervallo temporale d'importazione degli ordini:</p>
						<div class="col-md-3">
							Dal <input type="text" id="arco_dal" name="arco_dal" /> 
						</div>
						<div class="col-md-3">
							al <input type="text" id="arco_al" name="arco_al" />
						</div>
						<div class="col-md-3">
							<input type="submit" name="importa" value="Importa ordini" />
						</div>
					</form>
				</div>
			</div>
		{else if $smarty.get.listorders == 'y' && $smarty.post.importa !== null}
			<div class="panel">
				<p>{$avviso}</p>
				<div>{$iframe}</div>
			</div>
		{/if}
		
		{if $smarty.get.submitfeed == 'y' || $smarty.get.submitfattura == 'y' || $smarty.get.amazonfeed == 'y'}
			<div class="panel">
				<p>{$avviso}</p>
				<div>{$iframe}</div>
			</div>
		{/if}
	</form>
</div>

{/block}
