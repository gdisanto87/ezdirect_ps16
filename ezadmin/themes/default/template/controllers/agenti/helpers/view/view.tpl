{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

{assign var="link_gen_base" value="index.php?controller=AdminCustomers&amp;id_customer={$customer->id}&amp;viewcustomer&amp;"}
{assign var="token_base" value="&amp;token={getAdminToken tab='AdminCustomers'}"}
{*$is_agente = 1*}
{*$not_mio_agente = 1*}

TEST TEST TEST

<div id="container-customer">

	<nav class="navbar navbar-default">
		<div class="container-fluid">
			{* Questo è necessario per far funzionare la modalità mobile *}
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li {if $tab_name == "tickets"}class="active"{/if}><a href="{$link_gen_base}tab_name=tickets{$token_base}">Tickets (0 / 0)</a></li>
					<li {if ! isset($tab_name) OR $tab_name == "customerdetails"}class="active"{/if}><a href="{$link_gen_base}{$token_base}">Anagrafica</a></li>

				</ul>
			</div>
			
		</div>
	</nav>

	<div class="row">
		{if $riassunto_on}
		<div class="col-lg-12">

			<div class="panel">
				<div class="panel-heading">
					<i class="icon-home"></i>
					{l s='Summary'}
					
					<div class="panel-heading-action">
						<a class="btn btn-default squadrato" href="">
							<i class="icon-save"></i>
							{l s='Save'}
						</a>
					</div>
				</div>
				<div class="form-row">
					<div class="row">
						
						<div class="form-group col-md-2">
							<label for="customer" class="summary">{l s='Customer'}</label>
							<p class="form-control-static">
								{if isset($riassunto['cliente'])}
									{if $customer->active == 0 OR $customer->active == 2}
										<span class="rosso">{$riassunto['cliente']}</span>
									{else}
										{$riassunto['cliente']}
									{/if}
								{else}
									{l s='Unknown'}
								{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="codes" class="summary">{l s='Spring / CRM / eSolver'}</label>
							<p class="form-control-static">
								{if isset($riassunto['codici'])}{$riassunto['codici']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="profile" class="summary">{l s='Profile'}</label>
							<p class="form-control-static">
								{if isset($riassunto['profilo'])}{$riassunto['profilo']}{else}{l s='Unknown'}{/if}
								<span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="{$riassunto['tipo_profilo_title']}">
									{$riassunto['tipo_profilo']}
								</span>
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="customer_type" class="summary">{l s='Customer type'}</label>
							<p class="form-control-static">
								{if isset($riassunto['tipo_cliente'])}{$riassunto['tipo_cliente']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="status" class="summary">{l s='Status'}</label>
							<p class="form-control-static">
								{if isset($riassunto['status'])}
									{if $customer->active == 1}
									<span class="verde">{$riassunto['status']}</span>
									{else}
									<span class="rosso">{$riassunto['status']}</span>
									{/if}
								{else}
								{l s='Unknown'}
								{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="registration" class="summary">{l s='Registration'}</label>
							<p class="form-control-static">
								{if isset($riassunto['registrazione'])}{$riassunto['registrazione']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

					</div>
					<div class="row">
						
						<div class="form-group col-md-2">
							<label for="referent" class="summary">{l s='Referent'}</label>
							<p class="form-control-static">
								{*if isset($riassunto['referente'])}{$riassunto['referente']}{else}{l s='Unknown'}{/if*}
								<select class="form-control" id="select_referent">
									<option value="">{l s='Select Referent'}</option>
									{if count($riassunto['referenti'])}
										{foreach $riassunto['referenti'] AS $key => $referenti}
										<option value="{$referenti['id']}"{if $referenti['id'] == $riassunto['referente']['id']} selected="selected"{/if}>{$referenti['id']} - {$referenti['name']}</option>
										{/foreach}
									{/if}
								</select>
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="zone" class="summary">{l s='Zone'}</label>
							<p class="form-control-static">
								{if isset($riassunto['zona'])}{$riassunto['zona']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="orders_6" class="summary">{l s='Orders (6 months)'}</label>
							<p class="form-control-static">
								{if isset($riassunto['ord_6'])}{$riassunto['ord_6']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="orders_12" class="summary">{l s='Orders (1 year)'}</label>
							<p class="form-control-static">
								{if isset($riassunto['ord_12'])}{$riassunto['ord_12']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="tot_ord" class="summary">{l s='Total orders'}</label>
							<p class="form-control-static">
								{if isset($riassunto['ord_tot'])}{$riassunto['ord_tot']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="last_pur" class="summary">{l s='Last purchase'}</label>
							<p class="form-control-static">
								{if isset($riassunto['ultimo_acq'])}{$riassunto['ultimo_acq']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

					</div>
					<div class="row">
						
						<div class="form-group col-md-2">
							<label for="agent" class="summary">{l s='Agent'}</label>
							<p class="form-control-static">
								<select class="form-control" id="select_agent">
									<option value="">{l s='Select Agent'}</option>
									{if count($riassunto['impiegati'])}
										{foreach $riassunto['impiegati'] AS $key => $impiegato}
										<option value="{$impiegato['id']}"{if $impiegato['id'] == $riassunto['agente']['id']} selected="selected"{/if}>{$impiegato['id']} - {$impiegato['name']}</option>
										{/foreach}
									{/if}
								</select>
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="technician" class="summary">{l s='Technician'}</label>
							<p class="form-control-static">
								{*if isset($riassunto['tecnico'])}{$riassunto['tecnico']}{else}{l s='Unknown'}{/if*}
								<select class="form-control" id="select_tech">
									<option value="">{l s='Select Technician'}</option>
									{if count($riassunto['tecnici'])}
										{foreach $riassunto['tecnici'] AS $key => $tecnico}
										<option value="{$tecnico['id']}"{if $tecnico['id'] == $riassunto['tecnico']['id']} selected="selected"{/if}>{$tecnico['id']} - {$tecnico['name']}</option>
										{/foreach}
									{/if}
								</select>
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="visits" class="summary">{l s='Visits'}</label>
							<p class="form-control-static">
								{if isset($riassunto['visite'])}{$riassunto['visite']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="pages_visited" class="summary">{l s='Pages visited'}</label>
							<p class="form-control-static">
								{if isset($riassunto['pagine_viste'])}{$riassunto['pagine_viste']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="last_product" class="summary">{l s='Last product'}</label>
							<p class="form-control-static">
								
								{if isset($riassunto['ultimo_prod'])}
									{if isset($riassunto['ultimo_prod_title'])}
									<span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="{$riassunto['ultimo_prod_title']}">
										{$riassunto['ultimo_prod']}
									</span>
									{else}
									<p class="form-control-static">{$riassunto['ultimo_prod']}</p>
									{/if}
								{else}
								<p class="form-control-static">{l s='Unknown'}</p>
								{/if}
								
							</p>
						</div>

					</div>
					<div class="row">

						<div class="form-group col-md-2">
							<label for="last_ch_ric" class="summary">{l s='Last ch. ric.'}</label>
							<p class="form-control-static">
								{if isset($riassunto['ultima_ch_ric'])}{$riassunto['ultima_ch_ric']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="last_ch_inv" class="summary">{l s='Last ch. inv.'}</label>
							<p class="form-control-static">
								{if isset($riassunto['ultima_ch_inv'])}{$riassunto['ultima_ch_inv']}{else}{l s='Unknown'}{/if}
							</p>
						</div>

						<div class="form-group col-md-2">
							<label for="last_action" class="summary">{l s='Last action'}</label>
							<p class="form-control-static">
								{if isset($riassunto['ultima_azione'])}{$riassunto['ultima_azione']}{else}{l s='Unknown'}{/if}
							</p>
						</div>


					</div>
				</div>
			</div>
		</div>
		{/if}

		{if isset($alert)}
		<div class="col-lg-12">
			<div class="row">
				<div class="alert alert-danger">{$alert}</div>
			</div>
		</div>
		{/if}
		
		<div class="col-lg-12">

			<div class="row">

{*ADDRESSES*}
				{if ! isset($tab_name)}

				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					{* display hook specified to this page : AdminCustomers *}
					{hook h="displayAdminCustomers" id_customer=$customer->id|intval}
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-map-marker"></i> {l s='Addresses'} <span class="badge">{count($addresses)}</span>

							<div class="panel-heading-action">
							{* Premendo il tasto "add" si viene reindirizzati alla creazione di un nuovo "customer" con id_customer_associate che sarebbe l'id del customer a cui associare la nuova anagrafica *}
							{* index.php?controller=AdminCustomers *}
								<a class="btn btn-default" href="{$current|escape:'html':'UTF-8'}&amp;addcustomer&amp;id_customer_associate={$customer->id|intval}&amp;token={getAdminToken tab='AdminCustomers'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
									<i class="icon-plus-sign"></i>
									{l s='Add'}
								</a>
							</div>
						</div>
						
						{if count($addresses)}
							<table class="table">
								<thead>
									<tr>
										<th><span class="title_box ">{l s='Type'}</span></th>
										<th><span class="title_box ">{l s='Company'}</span></th>
										<th><span class="title_box ">{l s='Name'}</span></th>
										<th><span class="title_box ">{l s='Address'}</span></th>
										<th><span class="title_box ">{l s='Zip/Postal Code'}</span></th>
										<th><span class="title_box ">{l s='City'}</span></th>
										<th><span class="title_box ">{l s='State'}</span></th>
										<th><span class="title_box ">{l s='Country'}</span></th>
										<th><span class="title_box ">{l s='Phone number(s)'}</span></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									{foreach $addresses AS $key => $address}
									<tr>
										<td>{if $address['fatturazione']}{l s='Fatturazione'}{else}{l s='Consegna'}{/if}</td>
										<td>{if $address['company']}{$address['company']}{else}--{/if}</td>
										<td>{$address['firstname']} {$address['lastname']}</td>
										<td>{$address['address1']} {if $address['address2']}{$address['address2']}{/if}</td>
										<td>{$address['postcode']}</td>
										<td>{$address['city']}</td>
										<td>{$address['state']}</td>
										<td>{$address['country']}</td>
										<td>
											{if $address['phone']}
												{$address['phone']}
												{if $address['phone_mobile']}<br />{$address['phone_mobile']}{/if}
											{else}
												{if $address['phone_mobile']}<br />{$address['phone_mobile']}{else}--{/if}
											{/if}
										</td>
										<td class="text-right">
											<div class="btn-group">
												<a class="btn btn-default" style="border-radius:0px;" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;addaddress=1&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
													<i class="icon-edit"></i> {* l s='Edit' *}
												</a>
												{* <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> *}
												{*	<span class="caret"></span> *}
												{* </button> *}
												{* <ul class="dropdown-menu"> *}
													{* <li> *}
												
												<a class="btn btn-danger" style="border-radius:0px;" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;deleteaddress&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
													<i class="icon-trash"></i>
													{* l s='Delete' *}
												</a>
													{* </li> *}
												{* </ul> *}
											</div>
										</td>
									</tr>
									{/foreach}
								</tbody>
							</table>
						{else}
							<p class="text-muted text-center">
								{l s='%1$s %2$s has not registered any addresses yet' sprintf=[$customer->firstname, $customer->lastname]}
							</p>
						{/if}
					</div>
				</form>


{*OTHERS?*}
				{elseif $tab_name == "others"}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
				</form>

{*ERROR TAB NOT FOUND*}
				{elseif ! isset($is_agente_activated)}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-warning-sign"></i> {l s='Error'}
						</div>

						<h2 class="text-muted text-center">
							{l s='Tab'} <a class="text-danger">{$tab_name}</a> {l s='not found'}
						</h2>
					</div>
				</form>
				{/if}

				{if isset($is_agente_activated)}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-warning-sign"></i> {l s='Access denied'}
						</div>

						<h2 class="text-muted text-center">
							{l s='You do not have the necessary rights to access '} <a class="text-danger">{$tab_name}</a> {l s='tab'}
						</h2>
					</div>
				</form>
				{/if}

			</div>
		</div>



	</div>
</div>

<hr />
{/block}
