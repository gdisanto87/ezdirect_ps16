{extends file="helpers/list/list_header.tpl"}

{block name='override_header'}
<div class="panel">
	
	<div class="form-row">
		<div class="row">
			<div class="form-group col-md-12">
				<p>Clicca sulla matita per apportare modifiche. Spunta il flag per cancellare. Tutte le modifiche e le cancellazioni saranno effettuate esclusivamente dopo la conferma cliccando sull'apposito pulsante.</p>
				<p><strong>NB:</strong> se la <strong>quantità</strong> è colorata di <strong class="rosso">rosso</strong>, significa che è terminata la quantità disponibile a prezzo speciale oppure che la quantità non è stata inserita; se la <strong>data di fine</strong> è colorata di <strong class="rosso">rosso</strong>, l'offerta è scaduta.</p>
				<p>Clicca su <strong>Bulk actions</strong> per applicare l'azione desiderata a tutti gli elementi selezionati.</p>
				{* <form method="post"> *}
					{* <input onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" type="submit" class="button" id="conferma_tutti" name="conferma_tutti" value="Conferma le modifiche" /> *}
					{* <input type="submit" class="btn btn-secondary" id="allunga" name="allunga" value="Allunga fino a fine mese (da oggi) i prezzi speciali flaggati" /> *}
					{* <input type="submit" class="btn btn-secondary" id="allunga_successivo" name="allunga_successivo" value="Allunga fino a fine mese SUCCESSIVO (da oggi) i prezzi speciali flaggati" /> *}
				{* </form> *}
			</div>
		</div>
	</div>

</div>

{* Stile delle azioni della pagina *}
{include file="./actions_stile.tpl"}

{/block}

{block name=leadin}
	
{/block}