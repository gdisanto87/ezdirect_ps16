{extends file="helpers/list/list_header.tpl"}

{block name='override_header'}

	<script>
	{literal}
		$(document).ready(function () { 
			$('.tooltip_prodotto').tooltip({html: true});
		});
	{/literal}
	</script>
	
	<div id="container-header">
		<form class="form-horizontal col-lg-12" method="post">

			<div class="panel">
				<div class="panel-heading">
					<i class="icon-truck"></i> Filtra ordini
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<span style="font-size:20px;">Stai visualizzando: </span>
						{if $smarty.get.pronti}<span style="font-size:20px; font-weight:bold;">ordini PRONTI</span>
						{else if $smarty.get.parziali}<span style="font-size:20px; font-weight:bold;">ordini PARZIALI</span>
						{else if $smarty.get.prime}<span style="font-size:20px; font-weight:bold;">ordini PRIME</span>
						{else}<span style="font-size:20px; font-weight:bold;">TUTTI gli ordini</span>{/if}
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<a class="btn btn-primary squadrato" href="{$link->getAdminLink('AdminAllocato')|escape:'html':'UTF-8'}&amp;parziali=s">Solo parziali</a>
						<a class="btn btn-primary squadrato" href="{$link->getAdminLink('AdminAllocato')|escape:'html':'UTF-8'}&amp;prime=s">Solo prime</a>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<a href="{$link->getAdminLink('AdminAllocato')|escape:'html':'UTF-8'}">Resetta filtri</a>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<p>Scrivere "1" nella barra di ricerca della colonna "Pronto?" per vedere solo gli ordini pronti per la spedizione. Scrivere "0" per vedere solo quelli NON pronti.</p>
						<p>NB: gli ordini marcati in <strong>grassetto</strong> sono quelli che aspettano la spedizione da più di due giorni.</p>
					</div>
				</div>
			</div>

		</form>
	</div>

{/block}
