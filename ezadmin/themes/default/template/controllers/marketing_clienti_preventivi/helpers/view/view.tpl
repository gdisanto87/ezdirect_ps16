{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

{if !isset($smarty.post.cercaiclienti)}

<div id="container-marketing-preventivi">
	<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate>
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> Marketing preventivi
			</div>

			<p>Questo strumento consente di cercare tutti i preventivi non convertiti in ordine, entro un certo periodo di tempo. Consente inoltre di condurre la ricerca in base al gruppo del cliente (clienti web/rivenditori). I risultati sono esportabili in un file Excel.</p>
			<br />
			{* Correggere: usare bootstrap *}
			<form method="post" name="cercaclienti" id="cercaclienti">

				Seleziona arco temporale: 
				<br /><br />
				
				Dal <input type="date" id="arco_dal" name="arco_dal"  /> al <input type="date" id="arco_al" name="arco_al"  />
				<br /><br />
				
				<input type="checkbox" name="clienti" checked="checked" /> Clienti web<br />
				<input type="checkbox" name="rivenditori" checked="checked" /> Rivenditori<br />
				<input type="checkbox" name="distributori" checked="checked" /> Distributori<br />
				--
				<br />
				
				<input type="checkbox" name="aziende" checked="checked" /> Aziende<br />
				<input type="checkbox" name="privati" checked="checked" /> Privati<br />
				
				<input class="button" type="submit" value="Cerca" name="cercaiclienti" />
			</form>
		</div>
	</form>
</div>

{else}

<div id="container-marketing-preventivi">
	<div class="form-horizontal col-lg-12">
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> Marketing preventivi
			</div>

			{$riepilogo_html} {*  Variabile che sostituisce gli echo finchè non viene portato tutto in bootstrap *}

			<form method="post" name="form_indietro" id="form_indietro">
				<button type="button" class="btn btn-secondary" name="nuova_analisi" onclick="window.location = window.location.href;">Nuova analisi</button>
			</form>
		</div>
	</div>
</div>

{/if}

{/block}
