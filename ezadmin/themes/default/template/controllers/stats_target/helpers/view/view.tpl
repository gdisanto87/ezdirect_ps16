{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

<div id="container-target">
	<div class="form-horizontal col-lg-12">
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> Target
			</div>

			{$riepilogo_html} {*  Variabile che sostituisce gli echo finchè non viene portato tutto in bootstrap *}

		</div>
	</div>
</div>

{/block}
