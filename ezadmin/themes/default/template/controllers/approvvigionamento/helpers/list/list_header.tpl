{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/list/list_header.tpl"}

{block name='override_header'}

	<script type="text/javascript">        
		{literal} 
		$(document).ready(function() { 
			$('.tooltip_prodotto').tooltip({html: true});
		});
		{/literal}
	</script>
	
	<div id="container-header">
		<form class="form-horizontal col-lg-12" method="post">

			<div class="panel">
				<div class="panel-heading">
					<i class="icon-truck"></i> Filtra prodotti
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<span style="font-size:20px;">Stai visualizzando: </span>
						{if $smarty.get.sm}<span style="font-size:20px; font-weight:bold;">prodotti CON SCORTA MINIMA</span>
						{else if $smarty.get.do}<span style="font-size:20px; font-weight:bold;">prodotti DA ORDINARE</span>
						{else}<span style="font-size:20px; font-weight:bold;">TUTTI i prodotti</span>{/if}
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<a class="btn btn-primary squadrato" href="{$link->getAdminLink('AdminApprovvigionamento')|escape:'html':'UTF-8'}&amp;sm=s">Visualizza solo prodotti che hanno scorta minima</a>
						<a class="btn btn-primary squadrato" href="{$link->getAdminLink('AdminApprovvigionamento')|escape:'html':'UTF-8'}&amp;do=s">Visualizza solo prodotti da ordinare al fornitore</a>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<a href="{$link->getAdminLink('AdminApprovvigionamento')|escape:'html':'UTF-8'}">Resetta filtri</a>
					</div>
				</div>
			</div>

		</form>
	</div>

{/block}
