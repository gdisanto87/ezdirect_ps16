{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

{if !isset($smarty.post.cercaiprodotti)}

<div id="container-stats-prodotti">
	<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate>
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> Prodotti venduti
			</div>

			<br />
			{* Correggere: usare bootstrap *}
			<form method="post" name="cercaprodotti" id="cercaprodotti">
				
				Cerca dati su preciso prodotto:<br />
				<select multiple class="select2mul" id="per_prodotto" name="per_prodotto[]" style="width:600px">
					<option name="0" value="0">--- Scegli un prodotto ---</option>
					{foreach $prodotti as $row}
						<option name='{$row["id_product"]}' value='{$row["id_product"]}'>{$row["id_product"]} - {$row["name"]} ({$row["reference"]}) - {$row["price"]}</option>
					{/foreach}
				</select>
				
				<br /><br />
				
				Cerca dati su prodotti di una macrocategoria:<br />
				<select multiple class="select2mul" id="per_macrocategoria" name="per_macrocategoria[]" style="width:600px">
					<option name="0" value="0">--- Scegli una macrocategoria ---</option>
					{foreach $macrocat as $rowcat}
						<option name='{$rowcat["id_category"]}' value='{$rowcat["id_category"]}'>{$rowcat["id_category"]} - {$rowcat["name"]}</option>
					{/foreach}
				</select>

				<br /><br />
				
				Cerca dati su prodotti di una sottocategoria:<br />
				<select multiple class="select2mul" id="per_categoria" name="per_categoria[]" style="width:600px">
					<option name="0" value="0">--- Scegli una categoria ---</option>
					{foreach $sottocat as $rowcat}
						<option name='{$rowcat["id_category"]}' value='{$rowcat["id_category"]}'>{$rowcat["id_category"]} - {$rowcat["name"]}</option>
					{/foreach}
				</select>
				
				<br /><br />

				Cerca dati su prodotti di un marchio:<br />
				<select multiple class="select2mul" id="per_marchio" name="per_marchio[]" style="width:600px">
					<option name="0" value="0">--- Scegli un marchio ---</option>
					{foreach $marchi as $rowman}
						<option name='{$rowman["id_manufacturer"]}' value='{$rowman["id_manufacturer"]}'>{$rowman["id_manufacturer"]} - {$rowman["name"]}</option>
					{/foreach}
				</select>
				
				<br /><br />
				
				<input type='checkbox' value='0' name='escludi-privati' /> Escludi privati<br /><br />
				
				Seleziona un arco temporale:<br /><br />
				Dal <input type="date" id="arco_dal" name="arco_dal"  /> al <input type="date" id="arco_al" name="arco_al"  />
				
				<br /><br />
				
				<input type="radio" name="su_tipo" value="fatturato" checked="checked"> Sul fatturato<br />
				<input type="radio" name="su_tipo" value="ordinato"> Sull'ordinato<br />
				<input type="radio" name="su_tipo" value="preventivi"> Sui preventivi<br /><br />
				
				<h2>Amazon</h2> 
				Flaggare le caselle sottostanti per limitare la ricerca agli ordini che provengono da Amazon. 
				
				<br /><br />
				
				<strong>Canale</strong><br />
				<input type="checkbox" name="AmazonEZ" /> EZ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="AmazonPrime" /> Prime 
				
				<br /><br />
				
				<strong>Marketplace</strong><br />
				<input type="checkbox" name="AmazonItalia" /> Italia&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="AmazonFrancia" /> Francia&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="AmazonGermania" /> Germania&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="AmazonSpagna" /> Spagna&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="AmazonRegnoUnito" /> Regno Unito&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
				<br /><br />
				
				<input type='submit' name='cercaiprodotti' value='Avvia' class='button' />
			</form>
		</div>
	</form>
</div>

{else}

<div id="container-stats-prodotti">
	<div class="form-horizontal col-lg-12">
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> Prodotti venduti
			</div>

			{$riepilogo_html} {*  Variabile che sostituisce gli echo finchè non viene portato tutto in bootstrap *}

			<form method="post" name="form_indietro" id="form_indietro">
				<button type="button" class="btn btn-secondary" name="nuova_analisi" onclick="window.location = window.location.href;">Nuova analisi</button>
			</form>
		</div>
	</div>
</div>

{/if}

{/block}
