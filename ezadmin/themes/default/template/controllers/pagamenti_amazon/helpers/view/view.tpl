{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

<div id="container-pagamenti-amazon">
	<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate>
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-money"></i> Pagamenti Amazon
			</div>

			<p>
				Seleziona pagamenti creati dal: <input type="date" id="data_creazione_da" name="data_creazione_da" /> al: <input type="date" id="data_creazione_a" name="data_creazione_a" />
			</p>
			<label for="marketplace" id="per_questi_mktp">Per questi marketplace</label>
			<select id="marketplace" name="marketplace">
				<option value="0">Tutti</option>
				{foreach from=$marketplace item=mkt}
					<option value="{$mkt['value']}">{$mkt['name']}</option>
				{/foreach}
			</select>

			<br />

			<input type="radio" id="riepiloghi" name="tipo" value="riepiloghi" onclick="$('#per_questi_mktp').hide(); $('#marketplace').hide()" />
			<label for="riepiloghi"> Riepiloghi complessivi periodi</label>

			<input type="radio" id="singoli" name="tipo" value="singoli" onclick="$('#per_questi_mktp').show(); $('#marketplace').show()" />
			<label for="singoli"> Singoli ordini</label>

			<br />
			<p style="font-style:italic;"> NB: in modalità singoli ordini, Amazon impone dei limiti al numero di ordini visualizzabili. Potrebbe essere necessario fare più ricerche. </p>
			<br />

			<p>
				<input type="submit" name="invia_form" value="Cerca" class="button" />
			</p>
			<p>
				<input type="submit" name="invia_form2" value="Attenzione: CERCA QUI per pagamenti prima del 10 aprile 2016 (vecchio account)" class="button" />
			</p>

		</div>
	</form>
</div>

{/block}
