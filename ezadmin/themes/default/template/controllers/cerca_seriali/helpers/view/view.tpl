{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

{assign var="link_gen_base" value="index.php?controller=AdminOrders&amp;"}
{assign var="link_gen_home" value="index.php?controller=AdminOrders"}
{assign var="token_base" value="&amp;token={getAdminToken tab='AdminOrders'}"}

{if ! isset($smarty.post.seriale)}

<div id="container-cap">
	<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate>
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> Cerca un seriale prodotto negli ordini
			</div>

			<form method="post" name="form_cap_ricerca" id="form_cap_ricerca">
				<input type="text" name="seriale" id="seriale">
				<br />
				<button type="submit" class="btn btn-secondary" name="cercaseriale">Cerca Seriale</button>
			</form>

		</div>
	</form>
</div>

{else}

<div id="container-cap">
	<div class="form-horizontal col-lg-12">
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-cog"></i> Cerca un seriale prodotto negli ordini
			</div>

			{if count($seriali) > 0}

				<p>Il seriale <strong>{$smarty.post.seriale}</strong> si trova in questi ordini:</p>

				<br />
				
				{foreach $seriali AS $seriale}
					<div class="row">
						<div class="col-md-3 col-sm-5">
							<p>Ordine <strong>{$seriale["id_order"]}</strong>: {$seriale["seriale"]}</p>
						</div>
						<div class="col-md-3 col-sm-5">
							<a href="index.php?controller=AdminOrders&id_order={$seriale["id_order"]}&vieworder{$token_base}" target="_blank"><i class="icon-search-plus"></i> Vedi</a>
						</div>
					</div>

					<br />
				{/foreach}

			{else}

				<p>Il seriale <strong>{$smarty.post.seriale}</strong> non si trova in nessun ordine.</p>

				<br />

			{/if}

			<button type="button" class="btn btn-secondary" name="nuova_ricerca" onclick="window.location = window.location.href;">Nuova ricerca</button>

		</div>
	</div>
</div>

{/if}

{/block}
