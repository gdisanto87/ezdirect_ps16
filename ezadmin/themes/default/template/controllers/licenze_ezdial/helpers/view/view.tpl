{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

<div id="container-ezdial">
	<form class="form-horizontal col-lg-12"method="post" enctype="multipart/form-data" novalidate>
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-code"></i> Funzioni per licenze EzDial
			</div>

			<p>
				<a class="btn btn-primary squadrato" href="{$link->getAdminLink('AdminLicenzeEzdial')|escape:'html':'UTF-8'}&amp;function=crea">Crea nuove licenze ezDial</a>
				<a class="btn btn-primary squadrato" href="{$link->getAdminLink('AdminLicenzeEzdial')|escape:'html':'UTF-8'}&amp;function=dissocia">Svincola licenze già assegnate</a>
				<a class="btn btn-primary squadrato" href="{$link->getAdminLink('AdminLicenzeEzdial')|escape:'html':'UTF-8'}&amp;function=associa">Associa licenze non assegnate</a>
				<a class="btn btn-primary squadrato" href="{$link->getAdminLink('AdminLicenzeEzdial')|escape:'html':'UTF-8'}&amp;function=cerca">Cerca una licenza</a>
			</p>

		</div>

		{if $smarty.get.function == 'crea' && ($smarty.post.submit_crea === null || !isset($smarty.post.submit_crea))}
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-plus"></i> Crea licenze Ezdial
				</div>
				<div class="row">
					<form method="post" action="{$link->getAdminLink('AdminLicenzeEzdial')|escape:'html':'UTF-8'}&amp;function=crea">
						<p>Da questo form puoi creare nuove licenze ezDial. Inserisci il numero di licenze che vuoi creare e verranno prodotte automaticamente licenze di 16 caratteri.</p>
						<div class="col-md-6">
							<input name="crea_licenza" type="text" id="crea_licenza" name="search_query" placeholder="Inserisci il numero di licenze che vuoi creare..." autocomplete="off" />
						</div>
						<div class="col-md-3">
							<input class="button" name="submit_crea" value="Crea" type="submit" onclick="javascript: var surec=window.confirm('Sei sicuro?'); if (surec) { return true; } else { return false; }" />
						</div>
					</form>
				</form>
				</div>
			</div>
		{else if $smarty.get.function == 'crea' && $smarty.post.submit_crea !== null}
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-plus"></i> Crea licenze Ezdial
				</div>
				{if $lii_string !== ''}
					<p><strong>Ecco i codici delle licenze appena create:</strong></p>
					<div>{$lii_string}</div>
				{else}
					<p><strong>Inserisci un numero di licenze valido.</strong></p>
				{/if}
			</div>
		{/if}

		{if $smarty.get.function == 'dissocia' && ($smarty.post.submit_dissocia === null || !isset($smarty.post.submit_dissocia)) && ($smarty.post.submit_dissocia_mac === null || !isset($smarty.post.submit_dissocia_mac))}
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-unlink"></i> Svincola licenze Ezdial
				</div>
				<div class="row">
					<form method="post" action="{$link->getAdminLink('AdminLicenzeEzdial')|escape:'html':'UTF-8'}&amp;function=dissocia">
						<p>Cerca per codice licenza o per Mac Address nel campo sotto:</p>
						<div class="col-md-6">
							<input name="cerca_licenza" type="text" id="cerca_licenza" name="search_query" placeholder="Inserisci licenza o Mac Address..." autocomplete="off" />
						</div>
						<div class="col-md-3">
							<input class="button" name="submit_dissocia" value="Cerca" type="submit" />
						</div>
					</form>
				</form>
				</div>
			</div>
		{else if $smarty.get.function == 'dissocia' && $smarty.post.submit_dissocia !== null}
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-unlink"></i> Svincola licenze Ezdial
				</div>
				<p><strong>Risultati della ricerca:</strong></p>
				<div>{$table_ml}</div>
			</div>
		{else if $smarty.get.function == 'dissocia' && $smarty.post.submit_dissocia_mac !== null}
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-unlink"></i> Svincola licenze Ezdial
				</div>
				<p><strong>Licenze svincolate con successo.</strong></p>
			</div>
		{/if}

		{if $smarty.get.function == 'associa' && ($smarty.post.submit_associa === null || !isset($smarty.post.submit_associa)) && ($smarty.post.submit_associa_mac === null || !isset($smarty.post.submit_associa_mac))}
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-link"></i> Associa licenze Ezdial
				</div>
				<div class="row">
					<form method="post" action="{$link->getAdminLink('AdminLicenzeEzdial')|escape:'html':'UTF-8'}&amp;function=associa">
						<p>Cerca per codice licenza nel campo sotto:</p>
						<div class="col-md-6">
							<input name="cerca_licenza" type="text" id="cerca_licenza" name="search_query" placeholder="Inserisci licenza..." autocomplete="off" />
						</div>
						<div class="col-md-3">
							<input class="button" name="submit_associa" value="Cerca" type="submit" />
						</div>
					</form>
				</form>
				</div>
			</div>
		{else if $smarty.get.function == 'associa' && $smarty.post.submit_associa !== null}
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-unlink"></i> Associa licenze Ezdial
				</div>
				<p><strong>Risultati della ricerca:</strong></p>
				<div>{$table_ml}</div>
			</div>
		{else if $smarty.get.function == 'associa' && $smarty.post.submit_associa_mac !== null}
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-unlink"></i> Associa licenze Ezdial
				</div>
				<p><strong>Licenze associate con successo.</strong></p>
			</div>
		{/if}

		{if $smarty.get.function == 'cerca' && ($smarty.post.submit_cerca === null || !isset($smarty.post.submit_cerca))}
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-search"></i> Cerca licenze Ezdial
				</div>
				<div class="row">
					<form method="post" action="{$link->getAdminLink('AdminLicenzeEzdial')|escape:'html':'UTF-8'}&amp;function=cerca">
						<p>Cerca per codice licenza o per Mac Address nel campo sotto:</p>
						<div class="col-md-6">
							<input name="cerca_licenza" type="text" id="cerca_licenza" name="search_query" placeholder="Inserisci licenza o Mac Address..." autocomplete="off" />
						</div>
						<div class="col-md-3">
							<input class="button" name="submit_cerca" value="Cerca" type="submit" />
						</div>
					</form>
				</form>
				</div>
			</div>
		{else if $smarty.get.function == 'cerca' && $smarty.post.submit_cerca !== null}
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-search"></i> Cerca licenze Ezdial
				</div>
				<p><strong>Risultati della ricerca:</strong></p>
				<div>{$table_ml}</div>
			</div>
		{/if}
	</form>
</div>

{/block}
