{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/list/list_header.tpl"}

{block name=leadin}
{if isset($updateOrderStatus_mode) && $updateOrderStatus_mode}
	<div class="panel">
		<div class="panel-heading">
			{l s='Choose an order status'}
		</div>
		<form action="{$REQUEST_URI}" method="post">
			<div class="radio">
				<label for="id_order_state">
					<select id="id_order_state" name="id_order_state">
{foreach from=$order_statuses item=order_status_name key=id_order_state}
						<option value="{$id_order_state|intval}">{$order_status_name|escape}</option>
{/foreach}
					</select>
				</label>
			</div>
{foreach $POST as $key => $value}
	{if is_array($value)}
		{foreach $value as $val}
			<input type="hidden" name="{$key|escape:'html':'UTF-8'}[]" value="{$val|escape:'html':'UTF-8'}" />
		{/foreach}
	{elseif strtolower($key) != 'id_order_state'}
			<input type="hidden" name="{$key|escape:'html':'UTF-8'}" value="{$value|escape:'html':'UTF-8'}" />

	{/if}
{/foreach}
			<div class="panel-footer">
				<button type="submit" name="cancel" class="btn btn-default">
					<i class="icon-remove"></i>
					{l s='Cancel'}
				</button>
				<button type="submit" class="btn btn-default" name="submitUpdateOrderStatus">
					<i class="icon-check"></i>
					{l s='Update Order Status'}
				</button>
			</div>
		</form>
	</div>
{/if}
{/block}

{block name='override_header'}

	<script>
	$(document).ready(function () {
		$('.select2mul').select2();
	});
	</script>
	
	<div id="container-header">
		<form class="form-horizontal col-lg-12" method="post">

			<div class="panel">
				<div class="panel-heading">
					<i class="icon-truck"></i> Ricerca per prodotto
				</div>
				<div class="row">
					<div class="form-group col-md-8">
						<label>Cerca ordine in base al codice prodotto</label>
						<p class="form-control-static">
							<select id="cercaordineprodotto" name="cercaordineprodotto" class="form-control textbox-fix select2">
								<option value="">-- Seleziona --</option>
								{if count($prodotti)}
									{foreach $prodotti AS $row}
									<option value="{$row['id']}" {if $cercaordineprodotto == $row['id']}selected{/if}>{$row['name']} ({$row['reference']}) - ID {$row['id']}</option>
									{/foreach}
								{/if}
							</select>
						</p>
					</div>

					<div class="form-group col-md-4">
						<label>Ricerca testuale</label>
						<p class="form-control-static">
							<input class="textbox-fix h-std" type="text" id="cercatestoprodottoordine" name="cercatestoprodottoordine" value="{$cercatestoprodottoordine}">
						</p>
					</div>
				</div>

				<p>Per cercare più prodotti, usare la parola "or" nella cella di testo libero. Es: per cercare ordini che contengono astec o S412 o ez308 scrivere "astec or s412 or ez308".</p>

				<button type="submit" name="cerca" class="btn btn-secondary">Cerca</button>
				<button type="submit" name="reset_cerca" class="btn btn-secondary rosso">Resetta</button>
			</div>

			<div class="panel">
				<div class="panel-heading">
					<i class="icon-truck"></i> Filtra ordini
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<span style="font-size:20px;">Stai visualizzando: </span>
						{if $smarty.get.aperti}<span style="font-size:20px; font-weight:bold;">Ordini aperti</span>
						{else if $smarty.get.astec}<span style="font-size:20px; font-weight:bold;">Ordini con assistenza</span>
						{else if $smarty.get.astec_aperti}<span style="font-size:20px; font-weight:bold;">Ordini con assistenza CON TODO APERTO</span>
						{else if $smarty.get.valid}<span style="font-size:20px; font-weight:bold;">Ordini validi</span>
						{else}<span style="font-size:20px; font-weight:bold;">TUTTI gli ordini</span>{/if}
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<a class="btn btn-primary squadrato" href="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;aperti=s">Ordini APERTI</a>
						<a class="btn btn-primary squadrato" href="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;astec=s">Ordini con assistenza</a>
						<a class="btn btn-primary squadrato" href="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;astec_aperti=s">Ordini con assistenza CON TODO APERTO</a>
						<a class="btn btn-primary squadrato" href="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;valid=s">Ordini validi</a>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						<a href="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}">Resetta filtri</a>
					</div>
				</div>
			</div>

		</form>
	</div>

{/block}
