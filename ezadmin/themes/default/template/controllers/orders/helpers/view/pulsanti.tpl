<div class="col-md-12">
    <div class="panel">
        <div class="row"> {*correggere: rif_ord viene considerato nei messaggi ecc? controllare*}
			<a href="{$link->getAdminLink('AdminCustomers')}&id_customer={$customer->id}&viewcustomer&tab_name=actions&azione=actions&viewmessage&aprinuovomessaggio&rif_ord={$order->id}"><img src="../img/admin/email.gif" alt="Invia un messaggio al cliente" /> Invia un messaggio al cliente</a> 
            - {if ($currentState->invoice or $order->invoice_number) and ($products|@count)}<a href="{$link->getAdminLink('AdminOrders')|replace:'index':'ajax'}&id_order={$order->id}&vieworder&getOrderPDF" target="_blank" onclick="window.onbeforeunload = null"><img src="../img/admin/charged_ok.gif" alt="Apri PDF" /> Apri PDF</a>{else}<img src="../img/admin/charged_ko.gif" alt="No riepilogo PDF" /> No riepilogo PDF{/if} 
            - <a href="javascript:window.print()"><img src="../img/admin/printer.gif" alt="Stampa" /> Stampa</a>
            {if $cancella_ordine} - <a href="{$link->getAdminLink('AdminOrders')}&id_order={$order->id}&deleteorder" onclick="return confirm('Sei sicuro/a?')"><img src="../img/admin/delete.gif" alt="Cancella" /> Cancella ordine</a> {/if} 
            {if $permessi_fattura} - <a href="{$link->getAdminLink('AdminOrders')|replace:'index':'ajax'}&id_order={$order->id}&vieworder&getFF" target="_blank" onclick="window.onbeforeunload = null"><img src="../img/admin/details.gif" alt="FF" />{if $nmft > 0} Apri{else} Crea{/if} fattura pro forma (PDF){if $nmft > 0} - {$nmft|str_pad:10:'0':$smarty.const.STR_PAD_LEFT} - {$an}{/if}</a>{/if} 
        </div> 

        <br />

        <div class="row">

            <div class="col-md-2">
                {* Apri DDT in formato PDF *}
                <a href="{$link->getAdminLink('AdminOrders')|replace:'index':'ajax'}&id_order={$order->id}&vieworder&getDDT" class="btn btn-default" id="apri_ddt_pdf" onclick="window.onbeforeunload = null"><img src="../img/admin/pdf.gif" alt="DDT" title="DDT" /> Apri DDT in formato PDF</a>
            </div>

            <div class="col-md-2">
                {* Aggiungi nuova azione padre - correggere *}
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><span class="nav-label"><i class="icon-plus-sign verde" title="Aggiungi"></i> Aggiungi nuova azione padre</span></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-submenu">
                                {foreach $riga_pulsanti['azioni_padre_li'] as $li}
                                <li class="dropright">
                                    <a href="{$li['link']}"><img src="../img/admin/{$li['icona']}.gif" alt="{$li['title']}" title="{$li['title']}" />&nbsp;&nbsp;&nbsp;{$li['nome']}</a>
                                    {* CORREGGERE! TEST sub menu *}
                                    {assign var=tipo_li value=$li['tipo']}
                                    {if $tipo_li != ''}
                                    <div style="position:absolute; left:160px">
                                        <ul class="">
                                        {foreach $riga_pulsanti[$tipo_li] as $elem}
                                            <li class="dropdown-item"><a href="{$li['link']}{$elem['link']}"><img src="../img/admin/icons/{$elem['icona']}.gif" alt="{$elem['title']}" title="{$elem['title']}" /> {$elem['nome']}</a></li>
                                        {/foreach}
                                        </ul>
                                    </div>
                                    {/if}
                                </li>
                                {/foreach}
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

            {if $smarty.get.id_customer_thread !== null || $smarty.get.id_thread !== null || $smarty.get.id_mex !== null || $smarty.get.id_action !== null || $smarty.get.id_bdl !== null || $smarty.get.id_order !== null || $smarty.get.id_cart !== null}
            <div class="col-md-2">
                {* Aggiungi nuova azione figlia - visibile solo quando mi trovo in una possibile azione padre - correggere *}
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><span class="nav-label"><i class="icon-users verde" title="Aggiungi"></i> Aggiungi nuova azione figlia</span></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-submenu">
                                {foreach $riga_pulsanti['azioni_figlie_li'] as $li}
                                <li class="dropright">
                                    <a href="{$li['link']}"><img src="../img/admin/{$li['icona']}.gif" alt="{$li['title']}" title="{$li['title']}" />&nbsp;&nbsp;&nbsp;{$li['nome']}</a>
                                    {* CORREGGERE! TEST sub menu *}
                                    {assign var=tipo_li value=$li['tipo']}
                                    {if $tipo_li != ''}
                                    <div style="position:absolute; left:160px">
                                        <ul class="">
                                        {foreach $riga_pulsanti[$tipo_li] as $elem}
                                            <li class="dropdown-item"><a href="{$li['link']}{$elem['link']}"><img src="../img/admin/icons/{$elem['icona']}.gif" alt="{$elem['title']}" title="{$elem['title']}" /> {$elem['nome']}</a></li>
                                        {/foreach}
                                        </ul>
                                    </div>
                                    {/if}
                                </li>
                                {/foreach}
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            {/if}

            {* // mettere nel controller:
            // Correggere: azioni figlie di customers - controllare se va bene anche per orders!

                        $orders_telegest = Db::getInstance()->getValue('SELECT count(id_order) FROM orders WHERE id_customer = '.$customer->id);
                        //if($orders_telegest > 0)
                        //{
                            $orders_telegest_a = Db::getInstance()->executeS('SELECT DISTINCT id_order FROM orders WHERE id_customer = '.$customer->id);
                            foreach($orders_telegest_a as $ot)
                            {
                                $orders_t_stringb .= ($otzb % 9 == 0 ? '</tr><tr>' : '').'<td><input type="checkbox" class="orders_telegest_c" name="orders_telegest[]" value="'.$ot['id_order'].'" /> '.$ot['id_order'].'&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                                $otzb++;
                            }	
                        
                                
                            echo '
                                <div class="hider" id="hider_telegest_form2" style="display:none"></div>
                                <div class="popup_box" id="popup_telegest_form2" style="display:none; width:50%; text-align:center; z-index:999999999"><form method="post" action="index.php?tab=AdminCustomers&viewcart&editcart&provvisorio=1&id_customer='.$customer->id.'&amp;copy_to='.$customer->id.'&copia_carrello_conferma=y&id_cart=84366&viewcustomer&token='.$this->token.'&tab-container-1=4&preventivo=1&rif_prev='.$rif.'#modifica-carrello"> Seleziona gli ordini su cui applicare la telegestione: <br /><table><tr>'.$orders_t_stringb.'</tr></table><br /><br />
                                
                                <input type="submit" class="button" value="Conferma" />
                                
                                <button type="button" class="button"  onclick="$(\'#hider_telegest_form2\').hide(); $(\'#popup_telegest_form2\').hide();">Chiudi</button>
                                
                                </form>
                                </div>
                                <div class="menu-apertura-in" style="position:absolute; left:100px">
                                    <ul class="dropdown">
                                        <li><a style="color:#000; cursor:pointer" onclick="$(\'#hider_telegest_form2\').show(); $(\'#popup_telegest_form2\').show(); "><img src="../img/admin/icons/attivita.gif" alt="Telegestione annuale" title="Telegestione annuale" />&nbsp;&nbsp;&nbsp;Telegestione annuale</a></li>
                                    </ul>
                                </div>
                            ';
                        //}
                        echo '
                        </li> 
                        
                        <li><a href="index.php?tab=AdminCustomers&viewcart&editcart&createnew&provvisorio=1&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4&preventivo=2&rif_prev='.$rif.'#modifica-carrello" onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }" id="link-ordine-manuale"><img src="../img/admin/cart.gif" alt="Nuovo ordine manuale" title="Nuovo ordine manuale" /> Ordine manuale</a></li> 
                        
                        <li><a href="index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&nuovobdl&token='.$this->token.'&riferimento='.$rif.'&tab-container-1=14"><img src="../img/admin/prefs.gif" alt="Buono di lavoro" title="Buono di lavoro" />&nbsp;&nbsp;&nbsp;Buono di lavoro (BDL)</a></li>
                        
                    </ul>
                </div>
            </div>
            *}

            <div class="col-md-2">
                {* Invia mail con outlook a: + selezione mail *}
                <a href="#" class="button" id="invio_mail_outlook_t" onclick="window.location = 'mailto:'+document.getElementById('selezione_outlook_t').value"><img src="../img/admin/outlook.gif" alt="Outlook" title="Outlook" />&nbsp;&nbsp;&nbsp;Invia mail con Outlook a: </a>
                <select name="selezione_outlook_t" id="selezione_outlook_t" onchange="window.location = 'mailto:'+this.value">
                    <option value="">-- Seleziona mail --</option>
                    {foreach $riga_pulsanti['mail_persone'] as $mail}
                    <option value="{$mail['email']}">{$mail['email']}</option>
                    {/foreach}
                </select>
            </div>

        </div>
    </div>
</div>