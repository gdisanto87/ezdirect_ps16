<script type="text/javascript">

window.onload = function() {
    // SOLO SVILUPPO, DA COMMENTARE POI
    document.getElementById("prodotti-edit").style.display = "block";

    //alert("test");
    
    //document.getElementById("prodotti-view").style.display = "block";
    // END
}

function primo_avvio()
{
    
}

function modifica_prodotti()
{
    document.getElementById("prodotti-edit").style.display = "block";
    document.getElementById("prodotti-view").style.display = "none";
}

function modifica_pa()
{
    document.getElementById("pa-edit").style.display = "block";
    document.getElementById("pa-view").style.display = "none";
    document.getElementById("inserisci_pa").style.display = "none";
}

function salva_pa()
{
    document.getElementById("pa-edit").style.display = "none";
    document.getElementById("pa-view").style.display = "block";
}

function qta_modificata(id_prodotto)
{
    //var quantity = parseFloat(document.getElementById('product_quantity['+id_prodotto+']').value);
    //var unitario = parseFloat(document.getElementById('prezzo_unitario['+id_prodotto+']').value);
    
    //alert(quantity);

    calcolaRigaProdotto(id_prodotto);
}

function calcolaRigaProdotto(id_product)
{
    var numprice = document.getElementById('product_price['+id_product+']').value;
    
    var numunitario = document.getElementById('prezzo_unitario['+id_product+']').value;
    var numacquisto = document.getElementById('wholesale_price['+id_product+']').value;
    
    var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
                
    var quantity = parseFloat(document.getElementById('product_quantity['+id_product+']').value);

    var unitario = parseFloat(numunitario.replace(/\s/g, "").replace(",", "."));
    var acquisto = parseFloat(numacquisto.replace(/\s/g, "").replace(",", "."));
    
    var spedizione = parseFloat(document.getElementById('transport').value.replace(/\s/g, "").replace(",", "."));
    
    var sconti = parseFloat(document.getElementById('total_discounts').value.replace(/\s/g, "").replace(",", "."));
    
    var commissioni = parseFloat(document.getElementById('total_commissions').value.replace(/\s/g, "").replace(",", "."));
    
    var totaleacquisto = acquisto * quantity;
    
    document.getElementById('totaleacquisto['+id_product+']').value = totaleacquisto;

    var totale = parseFloat(document.getElementById('totaleprodotti').value);
    var sconti = parseFloat(document.getElementById('total_discounts').value.replace(/\s/g, "").replace(",", "."));
    var commissioni = parseFloat(document.getElementById('total_commissions').value.replace(/\s/g, "").replace(",", "."));
            
    var sc_qta_1 = parseFloat(document.getElementById('sc_qta_1['+id_product+']').value.replace(/\s/g, "").replace(",", "."));
    var sc_qta_2 = parseFloat(document.getElementById('sc_qta_2['+id_product+']').value.replace(/\s/g, "").replace(",", "."));
    var sc_qta_3 = parseFloat(document.getElementById('sc_qta_3['+id_product+']').value.replace(/\s/g, "").replace(",", "."));
    var sc_riv_1 = parseFloat(document.getElementById('sc_riv_1['+id_product+']').value.replace(/\s/g, "").replace(",", "."));
    var sc_riv_2 = parseFloat(document.getElementById('sc_riv_2['+id_product+']').value.replace(/\s/g, "").replace(",", "."));
    var sc_riv_3 = parseFloat(document.getElementById('sc_riv_3['+id_product+']').value.replace(/\s/g, "").replace(",", "."));

    var sc_qta_1_q = parseFloat(document.getElementById('sc_qta_1_q['+id_product+']').value);
    var sc_qta_2_q = parseFloat(document.getElementById('sc_qta_2_q['+id_product+']').value);
    var sc_qta_3_q = parseFloat(document.getElementById('sc_qta_3_q['+id_product+']').value);
    var sc_riv_1_q = parseFloat(document.getElementById('sc_riv_1_q['+id_product+']').value);
    var sc_riv_2_q = parseFloat(document.getElementById('sc_riv_2_q['+id_product+']').value);
    var sc_riv_3_q = parseFloat(document.getElementById('sc_riv_3_q['+id_product+']').value);

    var importo = parseFloat(price*quantity);
    
    if (document.getElementById('usa_sconti_quantita['+id_product+']').checked == true) {

        {if $customer->id_default_group == 3 OR $customer->id_default_group == 12}
        if(document.getElementById('sconto_extra['+id_product+']').value.replace(/\s/g, "").replace(",", ".") > 0) {
            
        } 
        else {
            var prezzounitarionew = unitario;
            // name="id_default_group"
            var prezziconfronto = new Array(sc_riv_1, {if $customer->id_default_group == 12}sc_riv_3,{/if} sc_qta_1, sc_qta_2, sc_qta_3);
            for (var i = 0; i < prezziconfronto.length; ++i) {
                if(prezziconfronto[i] !=0 && prezziconfronto[i] < prezzounitarionew) { 
                    prezzounitarionew = prezziconfronto[i];
                } 
                else { 

                }
            }	
            var importo = parseFloat(prezzounitarionew*quantity);
            document.getElementById('product_price['+id_product+']').value=prezzounitarionew.toFixed(2).replace(".",",");					
        }
        
        {else if $customer->id_default_group == 22}

        if(document.getElementById('sconto_extra['+id_product+']').value.replace(/\s/g, "").replace(",", ".") > 0) {

        } 
        else {
            var prezzounitarionew = unitario;
                
            if(sc_riv_2 == 0)
            {
                sc_riv_1 = sc_riv_1 + (sc_riv_1 / 100 *3);
                var prezziconfronto = new Array(sc_riv_1, sc_qta_1, sc_qta_2, sc_qta_3);
                for (var i = 0; i < prezziconfronto.length; ++i) {
                    if(prezziconfronto[i] !=0 && prezziconfronto[i] < prezzounitarionew) { 
                        prezzounitarionew = prezziconfronto[i];
                        
                    } else { }
                }	
            }
            else
            {
                var prezziconfronto = new Array(sc_riv_2, sc_qta_1, sc_qta_2, sc_qta_3);
                for (var i = 0; i < prezziconfronto.length; ++i) {
                    if(prezziconfronto[i] !=0 && prezziconfronto[i] < prezzounitarionew) { 
                        prezzounitarionew = prezziconfronto[i];
                        
                    } else { }
                }	
            }
            if(prezzounitarionew > unitario)
                prezzounitarionew = unitario;
            
            var importo = parseFloat(prezzounitarionew*quantity);
            document.getElementById('product_price['+id_product+']').value=prezzounitarionew.toFixed(2).replace(".",",");					
        } 

        {else}

        if(quantity < sc_qta_1_q && sc_qta_1_q != 0) {
                
            if(document.getElementById('sconto_extra['+id_product+']').value.replace(/\s/g, "").replace(",", ".") > 0) {
            } else {
                document.getElementById('product_price['+id_product+']').value=unitario.toFixed(2).replace(".",",");	
                var importo = parseFloat(price*quantity);
            }		
        }
        
        else if((quantity >= sc_qta_1_q && quantity < sc_qta_2_q) || (quantity >= sc_qta_1_q && isNaN(parseFloat(sc_qta_2_q)))) {
            if(document.getElementById('sconto_extra['+id_product+']').value.replace(/\s/g, "").replace(",", ".") > 0) {
            } else {
                var importo = parseFloat(sc_qta_1*quantity);
                document.getElementById('product_price['+id_product+']').value=sc_qta_1.toFixed(2).replace(".",",");	
            }
        }
        
        else if((quantity >= sc_qta_2_q && quantity < sc_qta_3_q) || (quantity >= sc_qta_2_q && isNaN(parseFloat(sc_qta_3_q == 0)))) {
            if(document.getElementById('sconto_extra['+id_product+']').value.replace(/\s/g, "").replace(",", ".") > 0) {
            } else {
                var importo = parseFloat(sc_qta_2*quantity);	
                document.getElementById('product_price['+id_product+']').value=sc_qta_2.toFixed(2).replace(".",",");	
            }
        }
        
        else if(quantity >= sc_qta_3_q && sc_qta_2_q != 0 && sc_qta_3_q != 0 && sc_qta_1_q != 0) {
            if(document.getElementById('sconto_extra['+id_product+']').value.replace(/\s/g, "").replace(",", ".") > 0) {
            } else {
                var importo = parseFloat(sc_qta_3*quantity);	
                document.getElementById('product_price['+id_product+']').value=sc_qta_3.toFixed(2).replace(".",",");
            }
        }
        {/if}


    } else if (document.getElementById('usa_sconti_quantita['+id_product+']').checked == false) {
        var importo = parseFloat(price*quantity);
    }

    document.getElementById('product_price['+id_product+']').value.replace(".",",");
    
    totale = 0;

    document.getElementById('impImporto['+id_product+']').value = importo;
    
    var arrayImporti = document.getElementsByClassName("importo");
    for (var i = 0; i < arrayImporti.length; ++i) {
        var item = parseFloat(arrayImporti[i].value);  
        totale += item;
    }
    
    var totaleacquisti = 0;
    
    var arrayAcquisti = document.getElementsByClassName("prezzoacquisto");
    for (var i = 0; i < arrayAcquisti.length; ++i) {
        arrayAcquisti[i].value = arrayAcquisti[i].value.replace(",",".");
        var item = parseFloat(arrayAcquisti[i].value); 					
        totaleacquisti += item;
    }
    
    totale = totale + commissioni;
    totale = totale - sconti;

    /*
    
    var controllo_vendita = document.getElementById('controllo_vendita_'+id_product+'');


    if(numunitario < price)
        controllo_vendita.innerHTML = "<img class='img_control' src='../img/admin/error.png' style='padding:0px; width:15px; float:right' title='ATTENZIONE stai vendendo il prodotto a un prezzo maggiore rispetto al prezzo standard di "+document.getElementById('prezzo_unitario['+id_product+']').value + "' alt='ATTENZIONE stai vendendo il prodotto a un prezzo maggiore rispetto al prezzo standard di "+document.getElementById('prezzo_unitario['+id_product+']').value + "' />";
    else
        controllo_vendita.innerHTML = "";

    */
    
    var marginalitatotale = (((totale - totaleacquisti)*100) / totale);

    marginalitatotale = marginalitatotale.toFixed(2);

    document.getElementById('marginalitatotale').innerHTML=marginalitatotale.replace(".",",") + "%";
    
    var guadagnototale = totale - totaleacquisti;
    
    guadagnototale = guadagnototale.toFixed(2);
    
    document.getElementById('guadagnototale').innerHTML=guadagnototale.replace(".",",");
    
    importo = importo.toFixed(2);
    importo = importo.replace(".",",");
    
    document.getElementById('totale['+id_product+']').value=importo;
    
    totale = totale+spedizione;			
    var totalep = totale;
    totale = totale.toFixed(2);

    document.getElementById('totaleprodotti').value=totale;
    
    document.getElementById('spantotaleprodotti').innerHTML=totale.replace(".",",");
    
    numprice = document.getElementById('product_price['+id_product+']').value;
    
    price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
    
    if(marginalita < 10 && price > 0)
        document.getElementById('tr_'+id_product).style.backgroundColor='#ffcccc';
    else
        document.getElementById('tr_'+id_product).style.backgroundColor='#ffffff';
    
    var marginalita = (((price - acquisto)*100) / price);
    
    marginalita = marginalita.toFixed(2);
    
    document.getElementById('margin['+id_product+']').value=marginalita.replace(".",",")+"%";

    /*
    $.ajax({
        url:"ajax_products_list2.php?calcola_totale_con_iva=y",
        type: "POST",
        data: { address: "{$address_delivery}",
        customer : "{$id_customer}",
        totalep : totalep
            },
        success:function(resp){
            totale_con_iva = resp;
            totale_con_iva = parseFloat(totale_con_iva);
            totale_con_iva = totale_con_iva.toFixed(2);
            document.getElementById('spantotaleprodottiiva').innerHTML=totale_con_iva.replace(".",",");		
        },
        error: function(xhr,stato,errori){
            alert("Errore nella cancellazione:"+xhr.status);
        }
    });
    */

}

function calcolaImporto(id_product) 
{
    
}

// Salvataggio Rif. Ordine
function saveRifOrdine()
{
    tinyMCE.triggerSave(); // correggere: a cosa serve? si può eliminare?
    $("#rif_ordine_feedback").html("<img src=\"../img/loader.gif\" />").show();

    var rif_ordine = $("#rif_ordine").val();
    $.post("ajax.php", {
        submitRifOrdine: 1,
        id_customer: {$customer->id},
        id_order: {$order->id},
        id_cart: {$order->id_cart},
        rif_ordine: rif_ordine
        }, 
        function (r) {
            $("#note_feedback").html("").hide(); // correggere: manca note_feedback nel tpl
            if (r == "ok"){
                $("#rif_ordine_feedback").html("<img src=\"../img/admin/ok.gif\" alt=\"OK\" title=\"OK\" />").fadeIn(400);
                tata.success('Riferimento ordine modificato', '');
            }
            else if (r == "error:validation"){
                $("#rif_ordine_feedback").html("<b style=\"color:red\">Errore</b>").fadeIn(400);
                tata.error('Errore nella modifica', '');
            }
            else if (r == "error:update"){
                $("#rif_ordine_feedback").html("<b style=\"color:red\">Errore</b>").fadeIn(400);
                tata.error('Errore nella modifica', '');
            }
            $("#rif_ordine_feedback").fadeOut(5000);
        }
    );
}

// Salvataggio Dati P.A.
function saveDatiPA()
{
    tinyMCE.triggerSave(); // correggere: a cosa serve? si può eliminare?

    $.post("ajax.php", {
        submitDatiPA: 1,
        id_order: {$order->id},
        cig: $("#cig").val(),
        cup: $("#cup").val(),
        ipa: $("#ipa").val(),
        data_ordine_mepa: $("#data_ordine_mepa").val()
        }, 
        function (r) {
            if(r){
                vals = JSON.parse(r);
                $("#cig_string").html(vals['cig']);
                $("#cup_string").html(vals['cup']);
                $("#ipa_string").html(vals['ipa']);
                $("#data_ordine_mepa_string").html(vals['data_ordine_mepa']);
                tata.success('Dati P.A. modificati', '');
            }
            else
                tata.error('Errore nella modifica', '');
        }
    );
}

// Salvataggio dati feedback
function saveNoFeedback()
{	
    $("#nofeedback_feedback").html("<img src=\"../img/loader.gif\" />").show();

    if($("#no_feedback").is(":checked"))
        var no_feedback = 1;
    else
        var no_feedback = 0;
    
    var no_feedback_cause = $("#no_feedback_cause").val();
    
    $.post("ajax.php", {
        submitNoFeedback: 1,
        id_customer: {$customer->id},
        id_order: {$order->id},
        no_feedback: no_feedback,
        no_feedback_cause: no_feedback_cause
        }, 
        function (r) {
            $("#nofeedback_feedback").html("").hide();
            if (r == "ok"){
                $("#nofeedback_feedback").html("<b style=\"color:green\">Dati salvati</b>").fadeIn(400);
                tata.success('Dati feedback modificati', '');
            }
            else if (r == "error:validation"){
                $("#nofeedback_feedback").html("<b style=\"color:red\">Errore</b>").fadeIn(400);
                tata.error('Errore nella modifica', '');
            }
            else if (r == "error:update"){
                $("#nofeedback_feedback").html("<b style=\"color:red\">Errore</b>").fadeIn(400);
                tata.error('Errore nella modifica', '');
            }
            $("#nofeedback_feedback").fadeOut(3000);
        }
    );
}

// Salvataggio Area carrello
function saveOrdineAreaCarrello()
{	
    tinyMCE.triggerSave();
    
    $("#note_feedback").html("<img src=\"../img/loader.gif\" />").show();

    var premessa = $("#premessa").val();
    var esigenze = $("#esigenze").val();
    var risorse = $("#risorse").val();
    var noteContent = $("#note").val();
    var notePrivateContent = $("#note_private").val();

    $.post("ajax.php", {
        submitOrdineAreaCarrello: 1,
        id_customer: {$customer->id},
        id_cart: {$cart->id},
        premessa: premessa,
        esigenze: esigenze,
        risorse: risorse,
        note: noteContent,
        notePrivate: notePrivateContent,
        }, 
        function (r) {
            $("#note_feedback").html("").hide();
            if (r == "ok"){
                $("#note_feedback").html("<b style=\"color:green\">Note salvate</b>").fadeIn(400);
                tata.success('Dati carrello modificati', '');
            }
            else if (r == "error:validation"){
                $("#note_feedback").html("<b style=\"color:red\">Errore: note non valide</b>").fadeIn(400);
                tata.error('Errore nella modifica', '');
            }
            else if (r == "error:update"){
                $("#note_feedback").html("<b style=\"color:red\">Errore: impossibile salvare note</b>").fadeIn(400);
                tata.error('Errore nella modifica', '');
            }
            $("#note_feedback").fadeOut(3000);
        }
    );
}

/* Ajax checkbox */

// Checkbox panel Informazioni

function updSpringOrder(id)
{
    var updSpringOrder="no";

    if(document.getElementById("spring_"+id+"").checked == true)
        updSpringOrder="yes";
    else
        updSpringOrder="no";
    
    $.ajax({
        url:"ajax.php?updSpringOrder="+updSpringOrder,
        type: "POST",
        data: { 
            id_order: id
        },
        success:function(r){
            tata.success('Dati modificati', '');
        },
        error: function(xhr,stato,errori){
            alert("Errore durante l\'operazione:"+xhr.status);
        }
    });
}

function updCheckPI(id)
{
    var updCheckPI="no";

    if(document.getElementById("check_pi_"+id+"").checked == true)
        updCheckPI="yes";
    else
        updCheckPI="no";
    
    $.ajax({
        url:"ajax.php?updCheckPI="+updCheckPI,
        type: "POST",
        data: { 
            id_order: id
        },
        success:function(r){
            tata.success('Dati modificati', '');
        },
        error: function(xhr,stato,errori){
            alert("Errore durante l\'operazione:"+xhr.status);
        }
    });    
}

function updPagAmzRic(id)
{
    var updPagAmzRic="no";

    if(document.getElementById("pag_amz_ric_"+id+"").checked == true)
        updPagAmzRic="yes";
    else
        updPagAmzRic="no";
    
    $.ajax({
        url:"ajax.php?updPagAmzRic="+updPagAmzRic,
        type: "POST",
        data: { 
            id_order: id
        },
        success:function(r){
            tata.success('Dati modificati', '');
        },
        error: function(xhr,stato,errori){
            alert("Errore durante l\'operazione:"+xhr.status);
        }
    });
}

function updCheckEsolv(id)
{
    var updCheckEsolv="no";

    if(document.getElementById("check_esolver_"+id+"").checked == true)
        updCheckEsolv="yes";
    else
        updCheckEsolv="no";
    
    $.ajax({
        url:"ajax.php?updCheckEsolv="+updCheckEsolv,
        type: "POST",
        data: { 
            id_order: id
        },
        success:function(r){
            tata.success('Dati modificati', '');
        },
        error: function(xhr,stato,errori){
            alert("Errore durante l\'operazione:"+xhr.status);
        }
    });
}

// Salvataggio ricezione contratto ezcloud
function salvaRicezioneContratto()
{
    if(document.getElementById("check_contratti").checked == true)
        var contratti_check = 1;
    else
        var contratti_check = 0;
    
    $.ajax({
        url: "ajax.php?salvaRicezioneContratto=y&id_cart={$order->id_cart}&id_employee={$mio_id}&contratti_check="+contratti_check,
        type: "POST",
        data: { 
        },
        success:function(r){
            $("#check_contratti_span").html(r);
        },
        error: function(xhr,stato,errori){
            alert("Errore durante l\'operazione:"+xhr.status);
        }
    });
}

/* Fine ajax checkbox */

// onchange stato ordine - testare!
function onchangeStato()
{
    if(this.value == 4 || this.value == 15) {
        document.getElementById('imposta_tracking').style.display = 'inline'; 
        document.getElementById('submitState').style.display = 'none';			
    }
    else if(this.value == 24) { 
        document.getElementById('impiegati_verifica').style.display = 'inline';
        document.getElementById('submitState').style.display = 'inline'; 
        document.getElementById('imposta_tracking').style.display = 'none'; 				
        document.getElementById('verifica_msg').style.display = 'inline'; 
    } 
    else if(this.value == 33) { 
        document.getElementById('acconto1').style.display = 'inline';
    }
    else
    {
        document.getElementById('submitState').style.display = 'inline'; 
        document.getElementById('imposta_tracking').style.display = 'none'; 
        document.getElementById('impiegati_verifica').style.display = 'none'; 			
        document.getElementById('verifica_msg').style.display = 'none'; 
        document.getElementById('acconto1').style.display = 'none';
    }
}

// Correggere funzione in ajax!
function submitShippingNumber_ajax()
{
    // saveRifOrdine();

    var id_order_state = document.getElementById('id_order_state').value;
    var shipping_number = document.getElementById('shipping_number').value;

    $.ajax({
        url:"ajax.php?submitShippingNumber_ajax=y",
        type: "POST",
        data: {
            id_order: {$order->id},
            id_order_state: id_order_state,
            shipping_number: shipping_number
        }, 
        success:function (r) {
            alert("Numero di tracking salvato. Una mail è stata inviata al cliente");
            $("#shipping_number_div").html("<b style=\"color:green\">Numero di tracking salvato. Una mail è stata inviata al cliente</b>").fadeIn(400);
        },
        error: function(xhr,stato,errori){
        }
    });
}

/* Note private -> bug: non funziona la modifica di una nota appena creata */

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
	var actions = $("td.azioni_note").html();

    $(".add-new").click(function(){
		$(this).attr("disabled", "disabled");
		var index = $("table.tabella_note > tbody > tr:last-child").index();
        var row = '<tr>' +
            //'<td><input type="text" class="form-control" name="nuova_nota" id="nuova_nota"></td>' +
            '<td><textarea class="form-control" name="nuova_nota" id="nuova_nota"></textarea></td>' +
            '<td>{$mio_nome}</td>' +
            '<td>{$today}</td>' +
			'<td>' + actions + '</td>' +
        '</tr>';
    	$("table.tabella_note").append(row);
		$("table.tabella_note > tbody > tr").eq(index + 1).find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip();
    });

	$(document).on("click", ".add", function(){
		var empty = false;
        var dbid = $(this).parents("tr").attr('dbid');
		//var input = $(this).parents("tr").find('input[type="text"]');
		var input = $(this).parents("tr").find('textarea');
        var questo = this;
        input.each(function(){
			if(!$(this).val()){
				$(this).addClass("error");
				empty = true;
			} else{
                $(this).removeClass("error");
            }
		});
		$(this).parents("tr").find(".error").first().focus();
		if(!empty){
			input.each(function(){
				$(this).parent("td").html($(this).val());
                if(dbid === undefined){
                    $.ajax({
                        url:"ajax.php?creanota_cart=y",
                        type: "POST",
                        data: { 
                            id_cart: {$cart->id},
                            nota: $(this).val()
                        },
                        success:function(data){
                            // inserimento in tab dell'id nota creata
                            $(questo).parents("tr").attr('dbid', data);
                            tata.success('Nota creata con successo', 'ID '+data);
                        },
                        error: function(xhr,stato,errori){
                            tata.error('Errore nella creazione', xhr.status);
                        }
                    });
                } else {
                    $.ajax({
                        url:"ajax.php?modificanota_cart=y",
                        type: "POST",
                        data: { 
                            id_nota: dbid,
                            nota: $(this).val()
                        },
                        success:function(){
                            tata.success('Nota modificata con successo', '');
                            succ = true;
                        },
                        error: function(xhr,stato,errori){
                            tata.error('Errore nella modifica', xhr.status);
                        }
                    });
                }
			});			
			$(this).parents("tr").find(".add, .edit").toggle();
			$(".add-new").removeAttr("disabled");
		}		
    });

    $(function() {
        $("input").keypress(function(event) {
            var $self = $(this);
            var comm = $self.val();

            var keycode = (event.keycode ? event.keycode : event.which);
            if (keycode == 13) {
                var comm = $self.val();
                alert(comm);
                event.stopPropagation();
            }
        });
    });

	$(document).on("click", ".edit", function(){		
        $(this).parents("tr").find("td:not(:last-child)").each(function(){
            if($(this).hasClass('mod')){ 
                //$(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">'); 
                $(this).html('<textarea class="form-control" style="height:80px;" >' + $(this).text() + '</textarea>'); 
            }
			
		});		
		$(this).parents("tr").find(".add, .edit").toggle();
		$(".add-new").attr("disabled", "disabled");
    });

	$(document).on("click", ".delete", function(){
        var dbid = $(this).parents("tr").attr('dbid');
        var succ = false;
        var questo = this;
        if(dbid === undefined){
            $(this).parents("tr").remove();
            $(".add-new").removeAttr("disabled");
        } else {
            if(confirm("Vuoi cancellare la nota con id "+dbid+" ?")){
                $.ajax({
                    url:"ajax.php?cancellanota_cart=y",
                    type: "POST",
                    data: { 
                        cancella_nota: 'y',
                        id_nota: dbid
                    },
                    success:function(){
                        tata.success('Nota cancellata con successo', '');
                        succ = true;
                        $(questo).parents("tr").remove();
                        $(".add-new").removeAttr("disabled");
                    },
                    error: function(xhr,stato,errori){
                        tata.error('Errore nella cancellazione', xhr.status);
                    }
                });
            }
        }
    });
});


</script>