<script>

window.onload = function() {
    primo_avvio();
};

function primo_avvio(){
    f_contratto_assistenza();
    f_manutenzione_ordinaria();
    f_manutenzione_straordinaria();

}

function verifica_contabilita(cb){
    var val = document.getElementById(cb).checked;

    //alert(val);

    switch(cb){
        case "pagato":
            if(val){
                document.getElementById("da_pagare").checked = false;
            }else{
                document.getElementById("da_pagare").checked = true;
            }
        break;

        case "da_pagare":
            if(val){
                document.getElementById("pagato").checked = false;
            }else{
                document.getElementById("pagato").checked = true;
            }
        break;

        case "fatturato":
            if(val){
                document.getElementById("da_fatturare").checked = false;
            }else{
                document.getElementById("da_fatturare").checked = true;
            }
        break;

        case "da_fatturare":
            if(val){
                if(document.getElementById("rif_ordine").value > 0){
                    document.getElementById("da_fatturare").checked = false;
                    alert("Attenzione: esiste un riferimento ordine, dunque il BDL è già stato fatturato");
                } else {
                    document.getElementById("fatturato").checked = false;
                }
            }else{
                document.getElementById("fatturato").checked = true;
            }
        break;

        case "nessun_addebito":
            if(val){
                document.getElementById("div_contab1").style.display = "none";
                document.getElementById("div_contab2").style.display = "none";
            }else{
                if(document.getElementById("manutenzione_ordinaria").checked == false){
                    document.getElementById("div_contab1").style.display = "block";
                    document.getElementById("div_contab2").style.display = "block";
                }
            }
        break;
    }
}

function f_contratto_assistenza(){
    if(!document.getElementById("contratto_assistenza").checked){
        document.getElementById("manutenzione_ordinaria").checked = false;
        document.getElementById("manutenzione_straordinaria").checked = true;
        mostra_dettagli();
    }

    if(document.getElementById("contratto_assistenza").checked && document.getElementById("manutenzione_ordinaria").checked){
        // nascondi invia controllo
        document.getElementById("invio_controllo").style.display = "none";
    }
}

function f_manutenzione_ordinaria(){
    if(!document.getElementById("contratto_assistenza").checked){
        document.getElementById("manutenzione_ordinaria").checked = 0;
        alert("Non si può selezionare la manutenzione ordinaria senza un contratto di assistenza");
    }
    if(document.getElementById("manutenzione_ordinaria").checked && document.getElementById("contratto_assistenza").checked){
        document.getElementById("manutenzione_straordinaria").checked = false;
        document.getElementById("pagato").checked = true;
        document.getElementById("da_pagare").checked = false;
        document.getElementById("fatturato").checked = true;
        document.getElementById("da_fatturare").checked = false;
        document.getElementById("invia_controllo").checked = true;
        document.getElementById("invio_contabilita").checked = true;

        //nascondere tutto
        nascondi_dettagli();
    }

    if(document.getElementById("manutenzione_ordinaria").checked){
        document.getElementById("manutenzione_straordinaria").checked = false;
    }

    if(!document.getElementById("manutenzione_ordinaria").checked){
        document.getElementById("manutenzione_straordinaria").checked = true;
        //mostro tutto
        mostra_dettagli();
    }

    if(document.getElementById("contratto_assistenza").checked && document.getElementById("manutenzione_ordinaria").checked){
        // nascondi invia controllo
        document.getElementById("invio_controllo").style.display = "none";
    }
}

function f_manutenzione_straordinaria(){
    if(document.getElementById("manutenzione_straordinaria").checked){
        document.getElementById("manutenzione_ordinaria").checked = false;
        document.getElementById("invia_controllo").checked = false;
        document.getElementById("invio_contabilita").checked = false;

        //mostro tutto
        mostra_dettagli();
    }

    if(!document.getElementById("manutenzione_straordinaria").checked && !document.getElementById("contratto_assistenza").checked){
        document.getElementById("manutenzione_straordinaria").checked = true;
        alert("Non si può selezionare la manutenzione ordinaria senza un contratto di assistenza");
    }

    if(!document.getElementById("manutenzione_straordinaria").checked && document.getElementById("contratto_assistenza").checked){
        document.getElementById("manutenzione_ordinaria").checked = true;
    }
}

function mostra_dettagli(){
    document.getElementById("seleziona_prodotto").style.display = "block";
    document.getElementById("aggiungi_prodotti").style.display = "block";
    document.getElementById("tabella_costi").style.display = "block";
    document.getElementById("note_uno").style.display = "block";
    document.getElementById("nota_privata").style.display = "block";
    document.getElementById("note_due").style.display = "block";
    document.getElementById("div_contab1").style.display = "block";
    document.getElementById("div_contab2").style.display = "block";
    document.getElementById("ultima_row").style.display = "block";
}

function nascondi_dettagli(){
    document.getElementById("seleziona_prodotto").style.display = "none";
    document.getElementById("aggiungi_prodotti").style.display = "none";
    document.getElementById("tabella_costi").style.display = "none";
    document.getElementById("note_uno").style.display = "none";
    document.getElementById("nota_privata").style.display = "none";
    document.getElementById("note_due").style.display = "none";
    document.getElementById("div_contab1").style.display = "none";
    document.getElementById("div_contab2").style.display = "none";
    document.getElementById("ultima_row").style.display = "none";
}



function aggiorna_costo_unitario(nome){
    var costo = 0;

    switch(document.getElementById(nome).value) {
        case "":  
            costo = 0;
        break;

        case "EZDIRFIX":
            costo = "60.00";
        break;

        case "EZDIRFIXF":
            costo = "120.00";
        break;

        case "EZKMR":
            costo = "0.50";
        break;

        case "EZZONAFIX":
            costo = "80.00";
        break;

        case "EZTECORD":
            costo = "60.00";
        break;

        case "EZTEC30":
            costo = "30.00";
        break;

        case "EZTECXTRA":
            costo = "80.00";
        break;

        case "EZTECFEST":
            costo = "90.00";
        break;

        default:
            costo = 0; 
        break;
    }
        
    //return costo;

    document.getElementById(nome+"_unitario").value = costo;
    document.getElementById(nome+"_code").innerHTML = document.getElementById(nome).value;

    aggiorna_costo_totale_riga(nome);
}


function aggiorna_costo_totale_riga(nome){

    document.getElementById(nome+"_code").innerHTML = document.getElementById(nome).value;

    if(document.getElementById(nome+"_code").innerHTML != ""){
        //nome è nome del dettaglio ( es: diritto_chiamata )
        var codice = nome + "_code" // ( es: diritto_chiamata_code )
        
        if(document.getElementById(nome+"_qta").value == ""){
            document.getElementById(nome+"_qta").value = 1;
        }


        if(document.getElementById(nome+"_sconto").value == ""){
            document.getElementById(nome+"_sconto").value = 0;
        }

        var importo = 0;

                
        var numqta = document.getElementById(nome+"_qta").value;
        var qta = parseFloat(numqta.replace(/\s/g, "").replace(",", "."));
        var numunitario = document.getElementById(nome+"_unitario").value;
        var numsconto = document.getElementById(nome+"_sconto").value;
        var unitario = parseFloat(numunitario.replace(/\s/g, "").replace(",", "."));
        var sconto = parseFloat(numsconto.replace(/\s/g, "").replace(",", "."));
        document.getElementById(nome+"_importo").value = (qta*(unitario - ((sconto*unitario)/100))).toFixed(2).replace(",", ".");

        

    } else {
        document.getElementById(nome+"_qta").value = "";
        document.getElementById(nome+"_unitario").value = "";
        document.getElementById(nome+"_sconto").value = "";
        document.getElementById(nome+"_importo").value = "";
    }

    calcola_totale();

}


function calcola_totale() {
    
    var num_diritto_chiamata = document.getElementById("diritto_chiamata_importo").value;
    var diritto_chiamata = parseFloat(num_diritto_chiamata.replace(/\s/g, "").replace(",", "."));
    if(isNaN(diritto_chiamata)) { diritto_chiamata = 0; }
    
    var num_chilometri_percorsi = document.getElementById("chilometri_percorsi_importo").value;
    var chilometri_percorsi = parseFloat(num_chilometri_percorsi.replace(/\s/g, "").replace(",", "."));
    if(isNaN(chilometri_percorsi)) { chilometri_percorsi = 0; }
    
    var num_costo_manodopera = document.getElementById("costo_manodopera_importo").value;
    var costo_manodopera = parseFloat(num_costo_manodopera.replace(/\s/g, "").replace(",", "."));
    if(isNaN(costo_manodopera)) { costo_manodopera = 0; }
    
    var num_varie = document.getElementById("num_varie").value;
    
    var tot_varie = 0;
    for(var i=0; i<=num_varie; i++) {
        if(document.getElementById("ricambi_varie_"+i)) {
            var num_ricambi_e_varie = document.getElementById("ricambi_varie_"+i).value;
            var costo_ricambi_e_varie = parseFloat(num_ricambi_e_varie.replace(/\s/g, "").replace(",", "."));
            if(isNaN(costo_ricambi_e_varie)) { costo_ricambi_e_varie = 0; }
            tot_varie = tot_varie+costo_ricambi_e_varie;
        }
    }
    
    var totale_BDL = costo_manodopera + diritto_chiamata + chilometri_percorsi + tot_varie;
    document.getElementById("totale_importo").value = totale_BDL.toFixed(2).replace(",", ".");

}
</script>