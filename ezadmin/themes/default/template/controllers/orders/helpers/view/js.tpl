<script type="text/javascript">
{literal}

/*
$(document).ready(function(){
    $(".tree-menu").treeview();
    
    $(".tasti-apertura").mouseover(function() {
        $(this).children(".menu-apertura").show();
    }).mouseout(function() {
        $(this).children(".menu-apertura").hide();
    });
    
    $(".tasti-apertura-in").mouseover(function() {
        $(this).children(".menu-apertura-in").show();
    }).mouseout(function() {
        $(this).children(".menu-apertura-in").hide();
    });
});
*/

// ---------------------------------------------------------

function modificaIndirizzoSpedizione(id, tipo) {
    if(document.getElementById('ind_csv').checked == false)
        var ind_csv = "n";
    else
        var ind_csv = "y";
    $.ajax({
        url:"ajax.php?ordModificaIndirizzoSpedizione=y",
        type: "POST",
        data: { id_address: id, ind_csv: ind_csv, id_order: "{/literal}{$order->id}{literal}", tipo: tipo
        },
        success:function(r){
        var parsed = JSON.parse(r);
        
        if(tipo == "consegna")
        {	
            document.getElementById("cons_mod").href = parsed[0];
            document.getElementById("cons_map").href = parsed[1];
            document.getElementById("cons_destinatario").innerHTML = "<strong>"+parsed[2]+"</strong>";
            document.getElementById("cons_referente").innerHTML = parsed[3];
            document.getElementById("cons_tel").innerHTML = parsed[4];
            document.getElementById("cons_cell").innerHTML = parsed[5];
            document.getElementById("cons_indirizzo").innerHTML = parsed[6];
            document.getElementById("cons_suburb").innerHTML = parsed[7];
            document.getElementById("cons_c_o").innerHTML = parsed[8];
            document.getElementById("cons_cap").innerHTML = parsed[9];
            document.getElementById("cons_citta").innerHTML = parsed[10];
            document.getElementById("cons_provincia").innerHTML = parsed[11];
            document.getElementById("cons_nazione").innerHTML = parsed[12];
        }
        else
        {
            document.getElementById("inv_mod").href = parsed[0];
            document.getElementById("inv_tel").innerHTML = parsed[4];
            document.getElementById("inv_cell").innerHTML = parsed[5];
            document.getElementById("inv_indirizzo").innerHTML = parsed[6];
            document.getElementById("inv_suburb").innerHTML = parsed[7];
            document.getElementById("inv_c_o").innerHTML = parsed[8];
            document.getElementById("inv_cap").innerHTML = parsed[9];
            document.getElementById("inv_citta").innerHTML = parsed[10];
            document.getElementById("inv_provincia").innerHTML = parsed[11];
            document.getElementById("inv_nazione").innerHTML = parsed[12];
        }
        },
        error: function(xhr,stato,errori){
            alert("Errore durante l'operazione:"+xhr.status);
        }
    });
}

// ---------------------------------------------------------

function submitShippingNumber_ajax() {	
    saveRifOrdine();
    var id_order_state = document.getElementById('id_order_state').value;
    var shipping_number = document.getElementById('shipping_number').value;
    $.ajax({url:"ajax.php?submitShippingNumber_ajax=y",
        type: "POST",
        data: {id_order:{/literal}{$order->id}{literal},id_order_state:id_order_state,shipping_number:shipping_number}, success:function (r) {
        
            console.log("OK");
            alert("Numero di tracking salvato. Una mail è stata inviata al cliente");
            $("#shipping_number_div").html("<b style=\"color:green\">Numero di tracking salvato. Una mail è stata inviata al cliente</b>").fadeIn(400);
        
    },
        error: function(xhr,stato,errori){
    }
    });
}

// ---------------------------------------------------------

function saveNoFeedback() {	
    $("#nofeedback_feedback").html("<img src=\"../img/loader.gif\" />").show();
    if($("#no_feedback").is(":checked"))
        var no_feedback = 1;
    else
        var no_feedback = 0;
    
    console.log(no_feedback);
    var no_feedback_cause = $("#no_feedback_cause").val();
    console.log(no_feedback_cause);

    // ROBA COMMENTATA PER COMPATIBILIT PHP
    
    $.post("ajax.php", {submitNoFeedback:1,id_customer:{/literal}{$customer->id}{literal},id_order:"{/literal}{$order->id}{literal}",no_feedback:no_feedback,no_feedback_cause:no_feedback_cause}, function (r) {
        $("#nofeedback_feedback").html("").hide();
        if (r == "ok")
        {
            $("#nofeedback_feedback").html("<b style=\"color:green\">{/literal}{*addslashes(AdminOrders::lx('Dati salvati'))*}{literal}</b>").fadeIn(400);
        }
        else if (r == "error:validation")
            $("#nofeedback_feedback").html("<b style=\"color:red\">{/literal}{*addslashes(AdminOrders::lx('Errore: impossibile salvare dati'))*}{literal}</b>").fadeIn(400);
        else if (r == "error:update")
            $("#nofeedback_feedback").html("<b style=\"color:red\">{/literal}{*addslashes(AdminOrders::lx('Errore: impossibile salvare dati'))*}{literal}</b>").fadeIn(400);
        $("#nofeedback_feedback").fadeOut(3000);
    });
}

// ---------------------------------------------------------

function salvaRicezioneContratto() {
    if(document.getElementById("check_contratti").checked == true)
    {
        var contratti_check = 1;
    }
    else
    {
        var contratti_check = 0;
    }
    
    $.ajax({
    url:"ajax.php?salvaRicezioneContratto=y&id_cart={/literal}{$order->id_cart}{literal}&id_employee={/literal}{$cookie->id_employee}{literal}&contratti_check="+contratti_check,
    type: "POST",
    data: { 
    },
    success:function(r){
        // alert(r);
        $("#check_contratti_span").html(r);
    },
    error: function(xhr,stato,errori){
        alert("Errore durante l'operazione:"+xhr.status);
    }
    });
}

// ---------------------------------------------------------

function delCons(id) {

    var delcons="no";
    if(document.getElementById("product_cons["+id+"]").checked == true)
        delcons="no";
    else
        delcons="yes";
    
    $.ajax({
        url:"ajax.php?delcons="+delcons+"&qtforcons="+document.getElementById("qtforcons["+id+"]").value,
        type: "POST",
        data: { id_product: id,
        id_order: {/literal}{$order->id}{literal}
        },
        success:function(r){
        // alert(r);
        $("#cons_td_"+id).html("<img src=\"..\/img\/admin\/ok.gif\" alt=\"OK\" title=\"OK\"  \/>").fadeIn(100);
        $("#cons_td_"+id).html("<img src=\"..\/img\/admin\/ok.gif\" alt=\"OK\" title=\"OK\"  \/>").fadeOut(100);
        },
        error: function(xhr,stato,errori){
            alert("Errore durante l'operazione:"+xhr.status);
        }
    });
}

function changeDDT(id) {

    if(document.getElementById("ddt["+id+"]").checked == true)
        ddt="0";
    else
        ddt="1";
    
    $.ajax({
        url:"ajax.php?changeDDT="+ddt+"",
        type: "POST",
        data: { id_product: id,
        id_order: {/literal}{$order->id}{literal}
        },
        success:function(r){
        alert("Modifica avvenuta con successo");
        },
        error: function(xhr,stato,errori){
            alert("Errore durante l'operazione:"+xhr.status);
        }
    });
}

// ---------------------------------------------------------

// ESPANSIONE RIGHE TABELLA PRODOTTI
$(document).ready(function(){
        $("#espandi_dati_tecnici_{/literal}{$product['id_order_detail']}{literal}").click(function(){
        $("tr.dati_tecnici_{/literal}{$product['id_order_detail']}{literal}").toggle();
        if($("tr.dati_tecnici_{/literal}{$product['id_order_detail']}{literal}:visible").length) {
            $(this).attr("src", "../img/admin/forbbiden.gif");	
        }
        else {
            $(this).attr("src", "../img/admin/add.gif");
            $("tr.dati_tecnici_{/literal}{$product['id_order_detail']}{literal}").hide();
        }
        return false;
    });
});

function saveDatiTecnici(id_order_detail, id_product,riga_max) {

    for(riga=1; riga<=riga_max; riga++)
    {	
        $("#dati_tecnici_feedback_"+id_order_detail).html("<img src=\"../img/loader.gif\" />").show();
        var seriale = $('input[name="dt_seriale_'+id_order_detail+'['+riga+']"]').val();
        var mac = $('input[name="dt_mac_'+id_order_detail+'['+riga+']"]').val();
        var imei = $('input[name="dt_imei_'+id_order_detail+'['+riga+']"]').val();
        var note = $('input[name="dt_note_'+id_order_detail+'['+riga+']"]').val();
    
        // ROBA COMMENTATA PERCHE FUNZIONI PHP E NON TPL
            $.post("ajax.php", {submitOrdineDatiTecnici:1,riga:riga,id_customer:{/literal}{$customer->id}{literal},id_order:{/literal}{$order->id}{literal},seriale:seriale,mac:mac,imei:imei,note:note,id_order_detail:id_order_detail,id_product:id_product}, function (r) {
            $("#dati_tecnici_feedback_"+id_order_detail).html("").hide();
            if (r == "ok")
            {
                $("#dati_tecnici_feedback_"+id_order_detail).html("<b style=\"color:green\">{/literal}{*addslashes(AdminOrders::lx('Dati tecnici salvati'))*}{literal}</b>").fadeIn(400);
            }
            else if (r == "error:validation")
                $("#dati_tecnici_feedback_"+id_order_detail).html("<b style=\"color:red\">{/literal}{*addslashes(AdminOrders::lx('Errore: dati tecnici non validi'))*}{literal}</b>").fadeIn(400);
            else if (r == "error:update")
                $("#dati_tecnici_feedback_"+id_order_detail).html("<b style=\"color:red\">{/literal}{*addslashes(AdminOrders::lx('Errore: dati tecnici non salvati'))*}{literal}</b>").fadeIn(400);
            else
                $("#dati_tecnici_feedback_"+id_order_detail).html(r).fadeIn(400);
        });
    }
}
// ---------------------------------------------------------

function calcPrezzoScontoHTML(sconto, prezzo, quantita, visualizzazione1, visualizzazione2) {
    var sconto = parseFloat(document.getElementById(sconto).value.replace(/,/g, "."));
    var prezzo = parseFloat(document.getElementById(prezzo).value.replace(/,/g, "."));
    var quantita = parseFloat(document.getElementById(quantita).value.replace(/,/g, "."));
    var nuovoPrezzo = prezzo - ((prezzo * sconto) / 100);
    
    var nuovoPrezzoQ = nuovoPrezzo*quantita;
    nuovoPrezzo = nuovoPrezzo.toFixed(2);
    nuovoPrezzo = nuovoPrezzo.toString();				
    nuovoPrezzo = nuovoPrezzo.replace(".",",");
    
    nuovoPrezzoQ = nuovoPrezzoQ.toFixed(2);
    nuovoPrezzoQ = nuovoPrezzoQ.toString();				
    nuovoPrezzoQ = nuovoPrezzoQ.replace(".",",");
    document.getElementById(visualizzazione1).innerHTML = nuovoPrezzo;
    document.getElementById(visualizzazione2).innerHTML = nuovoPrezzoQ;
}

// if($rif_fatt > 0) alert:
// (source: sweetalert2.css)
/*
swal(
    "ATTENZIONE!",
    "Stai modificando un ordine gi&agrave; fatturato. Avvisare l'ufficio amministrazione!",
    "warning"
)
*/

// Non so cosa sia nè se serve ancora
// src="../js/jquery/jquery.tablednd_0_5.js"
$(document).ready(function() {
    $("#tableProducts").tableDnD();
});


function ristabilisciPrezzo(id_product) {
    var numprice = document.getElementById('product_price['+id_product+']').value;
    document.getElementById('sconto_extra['+id_product+']').value = 0;
    var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
    var unitario = parseFloat(document.getElementById('unitario['+id_product+']').value);
    unitario = unitario.toFixed(2);
    unitario = unitario.toString();				
    unitario = unitario.replace(".",",");
    document.getElementById('product_price['+id_product+']').value=unitario;
    price = unitario;
}

function calcolaPrezzoScontoExtra(id_product, tipo) {
    var numunitario = document.getElementById('unitario['+id_product+']').value;
    var unitario = parseFloat(numunitario.replace(/\s/g, "").replace(",", "."));
    
    if (unitario == 0) {
        unitario = 0.001;
    }
    else {
        
    }
    
    var quantity = parseFloat(document.getElementById('product_quantity['+id_product+']').value);
    var numsconto_extra = document.getElementById('sconto_extra['+id_product+']').value.replace(/\s/g, "").replace(",", ".");
    
    var numprice = document.getElementById('product_price['+id_product+']').value;	
    var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
    var sconto_extra = parseFloat(numsconto_extra);
    
    if(sconto_extra == null)
        sconto_extra = 0;
        
    var sc_qta_1 = parseFloat(document.getElementById('sc_qta_1['+id_product+']').value.replace(/\s/g, "").replace(",", "."));
    var sc_qta_2 = parseFloat(document.getElementById('sc_qta_2['+id_product+']').value.replace(/\s/g, "").replace(",", "."));
    var sc_qta_3 = parseFloat(document.getElementById('sc_qta_3['+id_product+']').value.replace(/\s/g, "").replace(",", "."));
    var sc_riv_1 = parseFloat(document.getElementById('sc_riv_1['+id_product+']').value.replace(/\s/g, "").replace(",", "."));
    var sc_riv_2 = parseFloat(document.getElementById('sc_riv_2['+id_product+']').value.replace(/\s/g, "").replace(",", "."));
    var sc_riv_3 = parseFloat(document.getElementById('sc_riv_3['+id_product+']').value.replace(/\s/g, "").replace(",", "."));
    var sc_qta_1_q = parseFloat(document.getElementById('sc_qta_1_q['+id_product+']').value);
    var sc_qta_2_q = parseFloat(document.getElementById('sc_qta_2_q['+id_product+']').value);
    var sc_qta_3_q = parseFloat(document.getElementById('sc_qta_3_q['+id_product+']').value);
    var sc_riv_1_q = parseFloat(document.getElementById('sc_riv_1_q['+id_product+']').value);
    var sc_riv_2_q = parseFloat(document.getElementById('sc_riv_2_q['+id_product+']').value);	
    var sc_riv_3_q = parseFloat(document.getElementById('sc_riv_3_q['+id_product+']').value);	
    var finito = unitario - ((sconto_extra*unitario)/100);
    
    if (document.getElementById('usa_sconti_quantita['+id_product+']').checked == true) {
        // Tutte le righe commentate da qui in poi sono PHP da inserire:

        // if ($customer->id_default_group == 3) {
            if(sc_riv_1 > unitario)
                sc_riv_1 = unitario;
                
            if(tipo == "sconto") {
                var finito = sc_riv_1 - ((sconto_extra*sc_riv_1)/100);
            }
            else if (tipo == "inverso") {
                var finito = ((sc_riv_1 - price)/sc_riv_1)*100;
            }    
        // }
        // else if ($customer->id_default_group == 12) {
            if(tipo == "sconto") {
                var finito = sc_riv_3 - ((sconto_extra*sc_riv_3)/100);
            }
            else if (tipo == "inverso") {
                var finito = ((sc_riv_3 - price)/sc_riv_3)*100;
            }
        // }
        
        // if ($customer->id_default_group == 22) {
            if(sc_riv_2 > 0)
            {
                if(sc_riv_2 > unitario)
                    sc_riv_2 = unitario;
                
                if(tipo == "sconto") {
                    var finito = sc_riv_2 - ((sconto_extra*sc_riv_2)/100);
                }
                else if (tipo == "inverso") {
                    var finito = ((sc_riv_2 - price)/sc_riv_2)*100;
                }
            }
            else
            {
                sc_riv_1 = sc_riv_1 + (sc_riv_1 / 100 * 3);
                if(sc_riv_1 > unitario)
                sc_riv_1 = unitario;
            
                if(tipo == "sconto") {
                    var finito = sc_riv_1 - ((sconto_extra*sc_riv_1)/100);
                }
                else if (tipo == "inverso") {
                    var finito = ((sc_riv_1 - price)/sc_riv_1)*100;
                }
            }
        // }  
        // else {
            if(quantity < sc_qta_1_q && sc_qta_1_q != 0) {
                if(tipo == "sconto") {
                    var finito = unitario - ((sconto_extra*unitario)/100);
                }
                else if (tipo == "inverso") {
                    var finito = ((unitario - price)/unitario)*100;
                }
            }
                
            else if((quantity >= sc_qta_1_q && quantity < sc_qta_2_q) || (quantity >= sc_qta_1_q && isNaN(parseFloat(sc_qta_2_q)))) {
                if(tipo == "sconto") {
                    var finito = sc_qta_1 - ((sconto_extra*sc_qta_1)/100);	
                }
                else if (tipo == "inverso") {
                    var finito = ((sc_qta_1 - price)/sc_qta_1)*100;
                }
            }
                
            else if((quantity >= sc_qta_2_q && quantity < sc_qta_3_q) || (quantity >= sc_qta_2_q && isNaN(parseFloat(sc_qta_3_q == 0)))) {
                if(tipo == "sconto") {
                    var finito = sc_qta_2 - ((sconto_extra*sc_qta_2)/100);
                }
                else if (tipo == "inverso") {
                    var finito = ((sc_qta_2 - price)/sc_qta_2)*100;
                }
            }

            else if(quantity >= sc_qta_3_q && sc_qta_2_q != 0 && sc_qta_3_q != 0 && sc_qta_1_q != 0) {
                if(tipo == "sconto") {
                    var finito = sc_qta_3 - ((sconto_extra*sc_qta_3)/100);					
                }
                else if (tipo == "inverso") {
                    var finito = ((sc_qta_3 - price)/sc_qta_3)*100;
                } 
            }
            
        // }			
    }
    else {
        if (tipo == "sconto") {
            var finito = unitario - ((sconto_extra*unitario)/100);
        }
        else if (tipo == "inverso") {
            var finito = ((unitario - price)/unitario)*100;
        }
    }

    finito = finito.toFixed(2);
    finito = finito.toString();				
    finito = finito.replace(".",",");
    
    if (tipo == "sconto") {
        document.getElementById('product_price['+id_product+']').value=finito;
    }
    else if (tipo == "inverso") {
        document.getElementById('sconto_extra['+id_product+']').value=finito;
    }
}

function calcolaImportoConSpedizione(id_metodo) {

    if(document.getElementById('trasporto_modificato').value == "y") {
        var metodo_price = parseFloat(document.getElementById('transport').value.replace(/\s/g, "").replace(",", "."));
    }
    else {
        var metodo_price = parseFloat(document.getElementById('metodo['+id_metodo+']').getAttribute("rel"));
    }

    var totale = 0;

    var arrayImporti = document.getElementsByClassName("importo");

    for (var i = 0; i < arrayImporti.length; ++i) {
        var item = parseFloat(arrayImporti[i].value);  
        totale += item;
    }

    var sconti = parseFloat(document.getElementById('total_discounts').value.replace(/\s/g, "").replace(",", "."));	
    var commissioni = parseFloat(document.getElementById('total_commissions').value.replace(/\s/g, "").replace(",", "."));	
    
    totale = totale + metodo_price;
    totale = totale + commissioni;
    totale = totale - sconti;
    totale = totale.toFixed(2);
    
    var totalep = totale;
    
    document.getElementById('totaleprodotti').value=totale;
            
    document.getElementById('spantotaleprodotti').innerHTML=totale.replace(".",",");
    
    $.ajax({
        url:"ajax_products_list2.php?calcola_totale_con_iva=y",
        type: "POST",
        data: { address: "{/literal}{$address_delivery}{literal}",
        customer : "{/literal}{$id_customer}{literal}",
        totalep : totalep
         },
        success:function(resp){
            totale_con_iva = resp;
            totale_con_iva = parseFloat(totale_con_iva);
            totale_con_iva = totale_con_iva.toFixed(2);
            document.getElementById('spantotaleprodottiiva').innerHTML=totale_con_iva.replace(".",",");		
        },
        error: function(xhr,stato,errori){
            alert("Errore nella cancellazione:"+xhr.status);
        }
    });
}

function calcolaImporto(id_product) {
    var numprice = document.getElementById('product_price['+id_product+']').value;
    
    var numunitario = document.getElementById('unitario['+id_product+']').value;
    var numacquisto = document.getElementById('wholesale_price['+id_product+']').value;
    
    var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
                
    var quantity = parseFloat(document.getElementById('product_quantity['+id_product+']').value);

    var unitario = parseFloat(numunitario.replace(/\s/g, "").replace(",", "."));
    var acquisto = parseFloat(numacquisto.replace(/\s/g, "").replace(",", "."));
    
    var spedizione = parseFloat(document.getElementById('transport').value.replace(/\s/g, "").replace(",", "."));
    
    var sconti = parseFloat(document.getElementById('total_discounts').value.replace(/\s/g, "").replace(",", "."));
    
    var commissioni = parseFloat(document.getElementById('total_commissions').value.replace(/\s/g, "").replace(",", "."));
    
    var totaleacquisto = acquisto * quantity;
    
    document.getElementById('totaleacquisto['+id_product+']').value = totaleacquisto;

    var totale = parseFloat(document.getElementById('totaleprodotti').value);
    var sconti = parseFloat(document.getElementById('total_discounts').value.replace(/\s/g, "").replace(",", "."));
    var commissioni = parseFloat(document.getElementById('total_commissions').value.replace(/\s/g, "").replace(",", "."));
            
    var sc_qta_1 = parseFloat(document.getElementById('sc_qta_1['+id_product+']').value.replace(/\s/g, "").replace(",", "."));
    var sc_qta_2 = parseFloat(document.getElementById('sc_qta_2['+id_product+']').value.replace(/\s/g, "").replace(",", "."));
    var sc_qta_3 = parseFloat(document.getElementById('sc_qta_3['+id_product+']').value.replace(/\s/g, "").replace(",", "."));
    var sc_riv_1 = parseFloat(document.getElementById('sc_riv_1['+id_product+']').value.replace(/\s/g, "").replace(",", "."));
    var sc_riv_2 = parseFloat(document.getElementById('sc_riv_2['+id_product+']').value.replace(/\s/g, "").replace(",", "."));
    var sc_riv_3 = parseFloat(document.getElementById('sc_riv_3['+id_product+']').value.replace(/\s/g, "").replace(",", "."));

    var sc_qta_1_q = parseFloat(document.getElementById('sc_qta_1_q['+id_product+']').value);
    var sc_qta_2_q = parseFloat(document.getElementById('sc_qta_2_q['+id_product+']').value);
    var sc_qta_3_q = parseFloat(document.getElementById('sc_qta_3_q['+id_product+']').value);
    var sc_riv_1_q = parseFloat(document.getElementById('sc_riv_1_q['+id_product+']').value);
    var sc_riv_2_q = parseFloat(document.getElementById('sc_riv_2_q['+id_product+']').value);
    var sc_riv_3_q = parseFloat(document.getElementById('sc_riv_3_q['+id_product+']').value);
        
    var importo = parseFloat(price*quantity);
    
    if (document.getElementById('usa_sconti_quantita['+id_product+']').checked == true) {

        // if ($customer->id_default_group == 3 || $customer->id_default_group == 12) {
            if(document.getElementById('sconto_extra['+id_product+']').value.replace(/\s/g, "").replace(",", ".") > 0) {
            
            } 
            else {
                var prezzounitarionew = unitario;
                // name="id_default_group"
                var prezziconfronto = new Array(sc_riv_1, {/literal}{if $customer->id_default_group == 12}sc_riv_3,{/if}{literal} sc_qta_1, sc_qta_2, sc_qta_3);
                for (var i = 0; i < prezziconfronto.length; ++i) {
                    if(prezziconfronto[i] !=0 && prezziconfronto[i] < prezzounitarionew) { 
                        prezzounitarionew = prezziconfronto[i];
                    } 
                    else { 

                    }
                }	
                var importo = parseFloat(prezzounitarionew*quantity);
                document.getElementById('product_price['+id_product+']').value=prezzounitarionew.toFixed(2).replace(".",",");					
            }
        
        // }
        
        // else if ($customer->id_default_group == 22) {
            if(document.getElementById('sconto_extra['+id_product+']').value.replace(/\s/g, "").replace(",", ".") > 0) {

            } 
            else {
                var prezzounitarionew = unitario;
                    
                if(sc_riv_2 == 0)
                {
                    sc_riv_1 = sc_riv_1 + (sc_riv_1 / 100 *3);
                    var prezziconfronto = new Array(sc_riv_1, sc_qta_1, sc_qta_2, sc_qta_3);
                    for (var i = 0; i < prezziconfronto.length; ++i) {
                        if(prezziconfronto[i] !=0 && prezziconfronto[i] < prezzounitarionew) { 
                            prezzounitarionew = prezziconfronto[i];
                            
                        } else { }
                    }	
                }
                else
                {
                    var prezziconfronto = new Array(sc_riv_2, sc_qta_1, sc_qta_2, sc_qta_3);
                    for (var i = 0; i < prezziconfronto.length; ++i) {
                        if(prezziconfronto[i] !=0 && prezziconfronto[i] < prezzounitarionew) { 
                            prezzounitarionew = prezziconfronto[i];
                            
                        } else { }
                    }	
                }
                if(prezzounitarionew > unitario)
                    prezzounitarionew = unitario;
                
                var importo = parseFloat(prezzounitarionew*quantity);
                document.getElementById('product_price['+id_product+']').value=prezzounitarionew.toFixed(2).replace(".",",");					
            } 
        // }
            
        // else {
            if(quantity < sc_qta_1_q && sc_qta_1_q != 0) {
                
                if(document.getElementById('sconto_extra['+id_product+']').value.replace(/\s/g, "").replace(",", ".") > 0) {
                } else {
                    document.getElementById('product_price['+id_product+']').value=unitario.toFixed(2).replace(".",",");	
                    var importo = parseFloat(price*quantity);
                }		
            }
            
            else if((quantity >= sc_qta_1_q && quantity < sc_qta_2_q) || (quantity >= sc_qta_1_q && isNaN(parseFloat(sc_qta_2_q)))) {
                if(document.getElementById('sconto_extra['+id_product+']').value.replace(/\s/g, "").replace(",", ".") > 0) {
                } else {
                    var importo = parseFloat(sc_qta_1*quantity);
                    document.getElementById('product_price['+id_product+']').value=sc_qta_1.toFixed(2).replace(".",",");	
                }
            }
            
            else if((quantity >= sc_qta_2_q && quantity < sc_qta_3_q) || (quantity >= sc_qta_2_q && isNaN(parseFloat(sc_qta_3_q == 0)))) {
                if(document.getElementById('sconto_extra['+id_product+']').value.replace(/\s/g, "").replace(",", ".") > 0) {
                } else {
                    var importo = parseFloat(sc_qta_2*quantity);	
                    document.getElementById('product_price['+id_product+']').value=sc_qta_2.toFixed(2).replace(".",",");	
                }
            }
            
            else if(quantity >= sc_qta_3_q && sc_qta_2_q != 0 && sc_qta_3_q != 0 && sc_qta_1_q != 0) {
                if(document.getElementById('sconto_extra['+id_product+']').value.replace(/\s/g, "").replace(",", ".") > 0) {
                } else {
                    var importo = parseFloat(sc_qta_3*quantity);	
                    document.getElementById('product_price['+id_product+']').value=sc_qta_3.toFixed(2).replace(".",",");
                }
            }
        // } 
    }
    else if (document.getElementById('usa_sconti_quantita['+id_product+']').checked == false) {
        var importo = parseFloat(price*quantity);
    }

    document.getElementById('product_price['+id_product+']').value.replace(".",",");
    
    totale = 0;

    document.getElementById('impImporto['+id_product+']').value = importo;
    
    var arrayImporti = document.getElementsByClassName("importo");
    for (var i = 0; i < arrayImporti.length; ++i) {
        var item = parseFloat(arrayImporti[i].value);  
        totale += item;
    }
    
    var totaleacquisti = 0;
    
    var arrayAcquisti = document.getElementsByClassName("prezzoacquisto");
    for (var i = 0; i < arrayAcquisti.length; ++i) {
        arrayAcquisti[i].value = arrayAcquisti[i].value.replace(",",".");
        var item = parseFloat(arrayAcquisti[i].value); 					
        totaleacquisti += item;
    }
    
    totale = totale + commissioni;
    totale = totale - sconti;
    
    var controllo_vendita = document.getElementById('controllo_vendita_'+id_product+'');
    
    if(numunitario < price)
        controllo_vendita.innerHTML = "<img class='img_control' src='../img/admin/error.png' style='padding:0px; width:15px; float:right' title='ATTENZIONE stai vendendo il prodotto a un prezzo maggiore rispetto al prezzo standard di "+document.getElementById('unitario['+id_product+']').value + "' alt='ATTENZIONE stai vendendo il prodotto a un prezzo maggiore rispetto al prezzo standard di "+document.getElementById('unitario['+id_product+']').value + "' />";
    else
        controllo_vendita.innerHTML = "";
    
    var marginalitatotale = (((totale - totaleacquisti)*100) / totale);

    marginalitatotale = marginalitatotale.toFixed(2);

    document.getElementById('marginalitatotale').innerHTML=marginalitatotale.replace(".",",") + "%";
    
    var guadagnototale = totale - totaleacquisti;
    
    guadagnototale = guadagnototale.toFixed(2);
    
    document.getElementById('guadagnototale').innerHTML=guadagnototale.replace(".",",");
    
    importo = importo.toFixed(2);
    importo = importo.replace(".",",");
    
    document.getElementById('valoreImporto['+id_product+']').innerHTML=importo;
    
    totale = totale+spedizione;			
    var totalep = totale;
    totale = totale.toFixed(2);

    document.getElementById('totaleprodotti').value=totale;
    
    document.getElementById('spantotaleprodotti').innerHTML=totale.replace(".",",");
    
    numprice = document.getElementById('product_price['+id_product+']').value;
    
    price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
    
    if(marginalita < 10 && price > 0)
        document.getElementById('tr_'+id_product).style.backgroundColor='#ffcccc';
    else
        document.getElementById('tr_'+id_product).style.backgroundColor='#ffffff';
    
    var marginalita = (((price - acquisto)*100) / price);
    
    marginalita = marginalita.toFixed(2);
    
    document.getElementById('spanmarginalita['+id_product+']').innerHTML=marginalita.replace(".",",")+"%";
    
    $.ajax({
        url:"ajax_products_list2.php?calcola_totale_con_iva=y",
        type: "POST",
        data: { address: "{/literal}{$address_delivery}{literal}",
        customer : "{/literal}{$id_customer}{literal}",
        totalep : totalep
            },
        success:function(resp){
            totale_con_iva = resp;
            totale_con_iva = parseFloat(totale_con_iva);
            totale_con_iva = totale_con_iva.toFixed(2);
            document.getElementById('spantotaleprodottiiva').innerHTML=totale_con_iva.replace(".",",");		
        },
        error: function(xhr,stato,errori){
            alert("Errore nella cancellazione:"+xhr.status);
        }
    });
}

function togliImporto(id_product) {
    var daTogliere = parseFloat(document.getElementById('impImporto['+id_product+']').value.replace(",", "."));
    
    var totale = parseFloat(document.getElementById('totaleprodotti').value.replace(",", "."));
    
    var spedizione = parseFloat(document.getElementById('transport').value.replace(/\s/g, "").replace(",", "."));
    
    var sconti = parseFloat(document.getElementById('total_discounts').value.replace(/\s/g, "").replace(",", "."));
    var commissioni = parseFloat(document.getElementById('total_commissions').value.replace(/\s/g, "").replace(",", "."));
    
    var numacquisto = document.getElementById('wholesale_price['+id_product+']').value;

    var quantity = parseFloat(document.getElementById('product_quantity['+id_product+']').value);

    var acquisto = parseFloat(numacquisto.replace(/\s/g, "").replace(",", "."));

    var totaleacquisti = 0;
    
    var arrayAcquisti = document.getElementsByClassName("prezzoacquisto");
    for (var i = 0; i < arrayAcquisti.length; ++i) {
        arrayAcquisti[i].value = arrayAcquisti[i].value.replace(",",".");
        var item = parseFloat(arrayAcquisti[i].value); 					
        totaleacquisti += item;
    }
    
    var togliAcquisto = (acquisto*quantity);
    
    totaleacquisti = totaleacquisti-togliAcquisto;
    
    totale = totale-daTogliere;
                
    var marginalitatotale = (((totale - totaleacquisti)*100) / totale);

    marginalitatotale = marginalitatotale.toFixed(2);

    document.getElementById('marginalitatotale').innerHTML=marginalitatotale.replace(".",",") + "%";
    
    var guadagnototale = totale - totaleacquisti;
    
    guadagnototale = guadagnototale.toFixed(2);

    document.getElementById('guadagnototale').innerHTML=guadagnototale.replace(".",",");
    
    totale = totale.toFixed(2);
    
    document.getElementById('totaleprodotti').value = totale;
    document.getElementById('spantotaleprodotti').innerHTML=totale.replace(".",",");
}

function deleteRow(rowid) {   
    var row = document.getElementById(rowid);
    row.parentNode.removeChild(row);
}

function delProduct30(id) {
    deleteRow('tr_'+id);

    $("#id_lang").after("<input name='product_delete["+id+"]' id='product_delete["+id+"]' type='hidden' value='"+id+"' />");
    
    $.ajax({
        url:"ajax_products_list2.php?cancellaprodotto_daordine=y",
        type: "POST",
        data: { id_product: id,
        id_order: {/literal}{$smarty.get.id_order}{literal}
        },
        success:function(){
            var excludeIds = document.getElementById("excludeIds").value;
            var da_cancellare = new RegExp(id, "g");
            
            excludeIds = excludeIds.replace(da_cancellare,""); 
            
            document.getElementById("excludeIds").value = excludeIds;
            $("#product_autocomplete_input").val("");
            $("#product_autocomplete_input").setOptions({
                extraParams: {excludeIds : document.getElementById("excludeIds").value}
            });
        },
        error: function(xhr,stato,errori){
            alert("Errore nella cancellazione:"+xhr.status);
        }
    });
}
    
function delProduct30MSG(id) {
    document.getElementById('prodotti_cancellati').value = document.getElementById('prodotti_cancellati').value+","+id;
}

// ---------------------------------------------------------
// Se inserisco un kit, i prodotti vengono caricati singolarmente nella tabella

// $excludeIds: VARIABILE PHP
    var prodotti_nel_carrello = [{/literal}{$excludeIds}{literal}0];
 
function addProduct_TR(event, data, formatted) {

    if(data[6] == "*BUNDLE*") {
        var prodotti_bundle = data[7].split(";");
        for(i=0;i<((prodotti_bundle.length))-1;i++)
        {
            $.ajax({
                type: "GET",
                data: "id_cart=0&id_bundle="+data[1]+"&product_in_bundle="+prodotti_bundle[i],
                async: false,
                url: "ajax_products_list2.php",
                success: function(resp) {
                    addProduct_TR("",resp.split("|"),"");
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    tooltip_content = "";
                    alert("ERROR" + textStatus + " " + XMLHttpRequest.responseText);					
                }
            });
        }
        return false;
    }
    
    document.getElementById("product_autocomplete_input").value = ""; 
    var productId = data[1];
    
    $("#product_delete["+productId+"]").remove();
    
    var excludeIds = document.getElementById("excludeIds").value;
    var arrayescl = excludeIds.split(",");
    
    for(i=0;i<((arrayescl.length));i++)
    {
        if(arrayescl[i].trim() != "" && arrayescl[i].trim() == productId.trim())
        {
            alert("Prodotto già nel carrello");
            return false;
        }
    }

    excludeIds = excludeIds+","+productId+",";
    document.getElementById("excludeIds").value = excludeIds;
    $("#product_autocomplete_input").val("");
            
    $("#product_autocomplete_input").setOptions({
        extraParams: {excludeIds : document.getElementById("excludeIds").value}
    });	

    $("#product_autocomplete_input").focus();
    
    var productPrice = data[2];
    var productScQta = data[3];
    var productRef = data[4];
    var productName = data[5];
    var productQuantity = data[16];
    var tdImporto = data[17];
    var przUnitario = data[18];
    var deleteProd = data[19];
    var marg = data[20];
    var scontoExtra = data[21];
    var sc_qta_1 = data[6];
    var sc_qta_2 = data[7];
    var sc_qta_3 = data[8];
    var sc_riv_1 = data[9];
    var sc_riv_2 = data[10];
    var sc_riv_3 = data[26];
    var sc_qta_1_q = data[11];	
    var sc_qta_2_q = data[12];	
    var sc_qta_3_q = data[13];	
    var sc_riv_1_q = data[14];	
    var sc_riv_2_q = data[15];	
    var sc_riv_3_q = data[27];
    var acquisto = data[30];	
    var src_img = data[35];
    prodotti_nel_carrello.push(parseInt(productId));
    var varspec = data[24];
    var textVarSpec = "";
    
    $("#product_autocomplete_input").setOptions({
        extraParams: {excludeIds : document.getElementById("excludeIds").value}
    });	
    
    $("#product_autocomplete_input_interno").setOptions({
        extraParams: {excludeIds : document.getElementById("excludeIds").value}
    });
            
    if(varspec == 0) {
        
    }
    else {
        textVarSpec = "style='border:1px solid red'"
    }
    
    $("<tr " + textVarSpec + " id='tr_"+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"'><td><img src='"+src_img+"' style='width:30px; height:30px;' alt='' /></td><td><a href='index.php?tab=AdminCatalogExFeatures&id_product="+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"&updateproduct&token={/literal}{$tokenProducts}{literal}' target='_blank'>" + productRef + "</a></td><td><input name='nuovo_name["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]' size='31' type='text' value='"+ productName +"' /></td><td> " + productQuantity + "</td><td>" + productPrice + "<div style='float:right' id='controllo_vendita_"+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"'></div></td><td>" + scontoExtra + "</td><td>" + acquisto + "</td>" + marg + "<td style='text-align:center'> " + productScQta + "</td>" + tdImporto + "<td class='pointer dragHandle center' style='background:url(../img/admin/up-and-down.gif) no-repeat center;'><input type='hidden' name='sort_order["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]' /></td><td style='text-align:center'>" + deleteProd + "<input name='nuovo_prodotto["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]' type='hidden' value='"+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"' />" + sc_qta_1 + " " + sc_qta_2 + " " + sc_qta_3 + " " + sc_riv_1 + " " + sc_riv_2 + " " + sc_riv_3 + " " + sc_qta_1_q + " " + sc_qta_2_q + " " + sc_qta_3_q + " " + sc_riv_1_q + " " + sc_riv_2_q + " " + sc_riv_3_q + " " + przUnitario + "</td></tr>").appendTo("#tableProductsBody");
    
    $("#tableProducts").tableDnD();
    
    productId = productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "");
    calcolaImporto(productId);
    
    var excludeIds = document.getElementById("excludeIds").value;
    excludeIds = excludeIds+productId+",";
    
    document.getElementById("excludeIds").value = excludeIds;
    $("#product_autocomplete_input").val("");
    
    $("#product_autocomplete_input").setOptions({
        extraParams: {excludeIds : document.getElementById("excludeIds").value}
    });	
    
    $("#product_autocomplete_input_interno").setOptions({
        extraParams: {excludeIds : document.getElementById("excludeIds").value}
    });
    
    $("body").trigger("click");			
    $("#product_autocomplete_input").trigger("click");
    $("#product_autocomplete_input").trigger("click");
    
    $("body").trigger("click");			
    $("#product_autocomplete_input_interno").trigger("click");
    $("#product_autocomplete_input_interno").trigger("click");
                                    
    function getExcludeIds() {
        var ids = "";
        ids += $('#excludeIds').val().replace(/\\-/g,',').replace(/\\,$/,'');
        return ids;
    }
}

// ---------------------------------------------------------

// FORM RICERCA PRODOTTO: CONFRONTARE CON QUELLO IN BDL ADD/EDIT (dev'essere uguale ma questo ha in più la ricerca KIT)

$("body").on("click", function (event) {
    if(event.target.tagName.toUpperCase() == "BODY"  || event.target.id == "auto_marca"   || event.target.id == "auto_serie" || event.target.id == "auto_categoria" || event.target.id == "auto_fornitore" || event.target.id == "veditutti" || event.target.id == "prodotti_online"  || event.target.id == "prodotti_offline"   || event.target.id == "prodotti_old" || event.target.id == "prodotti_disponibili" || event.target.id == "product_autocomplete_input")
    {
    event.stopPropagation();
    }
    else
    {
    $('#prodlist').hide();
    $('div.autocomplete_list').hide();
    }
});	
    
urlToCall = null;
    
function getExcludeIds() {
    var ids = "";
    ids += $('#excludeIds').val().replace(/\\-/g,',').replace(/\\,$/,'');
    return ids;
}

function getOnline()
{
    if(document.getElementById("prodotti_online").checked == true)
        return 1;
    else
        return 0;
}

function getOffline()
{
    if(document.getElementById("prodotti_offline").checked == true)
        return 1;
    else
        return 0;
}

function getOld()
{
    if(document.getElementById("prodotti_old").checked == true)
        return 2;
    else
        return 0;
}

function getDisponibili()
{
    if(document.getElementById("prodotti_disponibili").checked == true)
        return 1;
    else
        return 0;
}

function getCopiaDa()
{
        return 0;
}
                    
function getSerie()
{
    if(document.getElementById("auto_serie").value > 0)
        return document.getElementById("auto_serie").value;
    else
        return 0;
}

function getMarca()
{
    if(document.getElementById("auto_marca").value > 0)
        return document.getElementById("auto_marca").value;
    else
        return 0;
}

function getFornitore()
{
    if(document.getElementById("auto_fornitore").value > 0)
        return document.getElementById("auto_fornitore").value;
    else
        return 0;
}

function getCategoria()
{
    if(document.getElementById("auto_categoria").value > 0)
        return document.getElementById("auto_categoria").value;
    else
        return 0;
}
    
function repopulateSeries(id_manufacturer)
{
    $("#auto_serie").empty();
    $.ajax({
        url:"ajax.php?repopulate_series=y",
        type: "POST",
        data: { id_manufacturer: id_manufacturer
        },
        success:function(resp){  
        var newOptions = $.parseJSON(resp);
        
            $("#auto_serie").append($("<option></option>")
                .attr("value", "0").text("Serie..."));
        
        $.each(newOptions, function(key,value) {
            $("#auto_serie").append($("<option></option>")
                .attr("value", value).text(key));
        });
        
        $("#auto_serie").select2({
            placeholder: "Serie..."
        });
        },
        error: function(xhr,stato,errori){
            alert("Errore: impossibile trovare serie ");
        }
    });
}	
    
function clearAutoComplete()
{
    $("#veditutti").trigger("click");
    
}	
    
/* function autocomplete */
$(function() {
    $('#product_autocomplete_input')
    .autocomplete('ajax_products_list2.php?excludeIds='+document.getElementById("excludeIds").value+'&id_cart={/literal}{$id_cart}{literal}', {
    
        minChars: 0,
        autoFill: false,
        max:5000,
        matchContains: true,
        matchSubset: 0,
        mustMatch:true,
        scroll:true,
        useCache:false,
        cacheLength:1,
        formatItem: function(item) {
    
            var color = "";
            if(item[24] == 1) {
                var color = "; color:red";
            }
            if(item[28] == 0) {
                return '</table><br /><div style="position:absolute; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:180px;float:left;margin-left:10px;">Codice</div></th><th style="width:630px;"><div style="width:350px; float:left;">Desc. prodotto<br /><br /></div></th><th style="width:60px; text-align:right"><div style="width:55px;float:left;">Prezzo</div></th><th style="width:45px; text-align:right"><div style="width:40px;float:left; text-align:right">Mag.</div></th><th style="width:45px; text-align:right"><div style="width:40px;float:left; text-align:right">INT</div></th><th style="width:45px; text-align:right"><div style="width:40px;float:left; text-align:right">ALN</div></th><th style="width:40px; text-align:right"><div style="width:40px;float:left; text-align:right">ITA</div></th><th style="width:40px; text-align:right"><div style="width:40px;float:left; text-align:right">Imp</div></th><th style="width:40px; text-align:right"><div style="width:40px;float:left; text-align:center">Tot</div></th></tr></div><table>'
                ;
            }
            else {
                return '<tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="'+item[35]+'" />&nbsp;&nbsp;'+item[4]+'</td><td style="width:450px'+color+'">'+item[5]+'</td><td style="width:60px; text-align:right">'+item[23]+'</td><td style="width:40px; text-align:right">'+item[25]+'</td><td style="width:40px; text-align:right">'+item[33]+'</td><td style="width:40px; text-align:right">'+item[34]+'</td><td style="width:40px; text-align:right">'+item[38]+'</td><td style="width:40px; text-align:right">'+item[37]+'</td><td style="width:40px; text-align:right">'+item[22]+'</td></tr>'
                ;
            }
        }
        
    }).result(addProduct_TR);
    
    $("#product_autocomplete_input").setOptions({
        extraParams: {excludeIds : getExcludeIds()}
    });		
});
            
$("#product_autocomplete_input").css('width','835px');

$("#product_autocomplete_input").keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == "13") {
        $("#veditutti").trigger("click"); 
    event.preventDefault();
    event.stopPropagation();    
    }
});

$("#veditutti").click ( function (zEvent) {
    // LASCIARE COMMENTATO
    /*	$("body").trigger("click");
        $("body").trigger("click");
        $("#product_autocomplete_input").focus();
    //	$("#product_autocomplete_input").val("");
    //	$("#product_autocomplete_input").val(" ");
    */
        var press = jQuery.Event("keypress");
        press.ctrlKey = true;
        if(document.getElementById('product_autocomplete_input').value == "")
        {
            $("#product_autocomplete_input").val(" ");
        }
        $("#product_autocomplete_input").trigger(press);
    
        $("#product_autocomplete_input").trigger("click");
        $("#product_autocomplete_input").trigger("click");
    //	$("#product_autocomplete_input").val(""); // LASCIARE COMMENTATO
} );

// PHP:
// KIT <input size="123" type="text" value="" id="product_autocomplete_input_kit" /><br /><br /> 

var formProduct = "";
var products = new Array();

urlToCall = null;
					
/* function autocomplete */
$(function() {
    $('#product_autocomplete_input_kit')
    .autocomplete('ajax_products_list2.php?id_bundle=0&bundles=y&id_cart={/literal}{$id_cart}{literal}', {
        minChars: 0,
        autoFill: false,
        max:50,
        matchContains: true,
        matchSubset: 0,
        mustMatch:true,
        scroll:true,
        cacheLength:1,
        formatItem: function(item) {
            var color = "";
            if(item[24] == 1) {
                var color = "; color:red";
            }
            if(item[28] == 0) {
                return '</table><div style="position:absolute; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:150px;float:left;margin-left:10px;">Codice</div></th><th style="width:570px;"><div style="width:550px; float:left;">Desc. kit<br /><br /></div></th><th style="width:60px; text-align:right"><div style="width:60px;float:left;">Prezzo</div></th><th style="width:50px; text-align:right"><div style="width:60px;float:left;">Qt. mag.</div></th><th style="width:50px; text-align:right"><div style="width:75px;float:left;">Qt. tot</div></th></tr></div><table><tr><td style="width:145px; text-align:left"><br /><br /><img style="width:35px; height:35px" src="'+item[35]+'" />&nbsp;&nbsp;'+item[4]+'</td><td style="width:555px'+color+'"><br /><br />'+item[5]+'</td><td style="width:50px; text-align:right"><br /><br />'+item[23]+'</td><td style="width:50px; text-align:right"><br /><br />'+item[25]+'</td><td style="width:50px; text-align:right"><br /><br />'+item[22]+'</td></tr></table>'
                ;
            }
            else {
                return '<tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="'+item[35]+'" />&nbsp;&nbsp;'+item[4]+'</td><td style="width:550px'+color+'">'+item[5]+'</td><td style="width:60px; text-align:right">'+item[23]+'</td><td style="width:50px; text-align:right">'+item[25]+'</td><td style="width:50px; text-align:right">'+item[22]+'</td></tr>'
                ;
                }
        }
    }).result(addProduct_TR);
});

$("#product_autocomplete_input_kit").css('width','905px');

$("#product_autocomplete_input_kit").keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == "13") {
        $("#product_autocomplete_input_kit").trigger("click");
        event.preventDefault();
        event.stopPropagation();    
    }
});

$("#order_edit").on("keyup keypress", function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) { 
        e.preventDefault();
        return false;
    }
});

{/literal}
</script>
