<form class="form-horizontal col-lg-12" method="post" name="form_deduplica" id="form_deduplica" onsubmit="return confirm('Sei sicuro? L\'operazione è irreversibile.');">
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-tasks"></i> Deduplica anagrafica
        </div>

        <p>Con questo form puoi deduplicare un'anagrafica. Tutti i dati (ordini, carrelli, fatture, indirizzi, todo, ticket, azioni cliente, contratti, bdl, persone) saranno spostati dall'anagrafica di <strong>partenza</strong> verso l'anagrafica di <strong>arrivo</strong>. Spuntando il flag è possibile anche cancellare l'anagrafica di partenza. Nelle caselle è necessario inserire l'ID CRM delle anagrafiche. Il programma, una volta dato avvio, chiederà la conferma.</p>
        <h2 class="text-muted">
            <strong>ATTENZIONE:</strong> l'operazione è irreversibile.
        </h2>

        <br />

        <div class="row">
                    
            <div class="form-group col-md-2">
                <label for="anagrafica_partenza">ID CRM anagrafica di <strong>PARTENZA</strong></label>
                <p class="form-control-static">
                    <input type="text" size="20" name="partenza" id="partenza" onkeyup="searchCustomerById(this.value, 'an_partenza')" />
                </p>
            </div>

            <div class="form-group col-md-1">
                <label></label>
                <p class="form-control-static text-center">
                    <br />
                    <i class="icon-long-arrow-right"></i>
                </p>
            </div>
            
            <div class="form-group col-md-2">
                <label for="anagrafica_arrivo">ID CRM anagrafica di <strong>ARRIVO</strong>
                    <span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="Tutti i dati saranno spostati su questa anagrafica">
                        <i class="icon-question-sign"></i>
                    </span>
                </label>
                <p class="form-control-static">
                    <input type="text" size="20" name="arrivo" id="arrivo" onkeyup="searchCustomerById(this.value, 'an_arrivo')" />
                </p>
            </div>

            <div class="form-group col-md-1">
                <label></label>
                <p class="form-control-static text-center">
                </p>
            </div>

            <div class="form-group col-md-4">
                <label>Cancellare anagrafica di partenza?
                    <span class="label-tooltip rosso" data-toggle="tooltip" data-html="true" title="NB: l'operazione è irreversibile">
                        <i class="icon-exclamation-sign"></i>
                    </span>
                </label>
                
                <p class="form-control-static">
                    <span class="label-tooltip text-warning" data-toggle="tooltip" data-html="true" title="NB: l'operazione è irreversibile">
                        <input type="checkbox" name="cancella_partenza" id="cancella_partenza" />
                    </span>
                </p>
            </div>
            

        </div>

        <div class="row">

            <div id="an_partenza" class="form-group col-md-2 text-center">
                
            </div>

            <div class="form-group col-md-1 text-center">
                <div id="freccia" style="display: none;">
                    <i class="icon-long-arrow-right"></i>
                </div>
            </div>
            
            <div id="an_arrivo" class="form-group col-md-2 text-center">
                
            </div>

        </div>

        <input type="hidden" id="deduplica_anag_conf" name="deduplica_anag_conf"  />
        <input type="hidden" id="azione" name="azione" value="deduplica" />
        <input type="submit" id="deduplica_anag" value="Avvio" style="width:200px" class="button" name="deduplica_anag"/>

        <script type="text/javascript">
            function searchCustomerById(id, type){
                $.ajax({
                url:"ajax.php?searchCustomerById=y",
                type: "POST",
                data: { id_customer: id
                },
                success:function(r){
                    //alert(r);
                    //$("#"+type).html(r);
                    document.getElementById(type).innerHTML = r;
                    if(document.getElementById("an_partenza").innerHTML == "" && document.getElementById("an_arrivo").innerHTML == ""){
                        document.getElementById("freccia").style.display = "none";
                    } else {
                        document.getElementById("freccia").style.display = "block";
                    }
                }, 
                error: function(xhr,stato,errori){
                    alert("Errore:"+xhr.status);
                }
                });
            }
        </script>

    </div>
</form>