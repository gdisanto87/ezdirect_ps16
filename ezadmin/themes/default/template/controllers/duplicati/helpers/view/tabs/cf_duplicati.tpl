{if count($codici_fiscali['duplicati'])}
    {foreach $codici_fiscali['duplicati'] AS $key => $cf_doppi}
    <form class="form-horizontal col-lg-12"  method="post" enctype="multipart/form-data" novalidate>
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-tasks"></i> {l s='TAX code'}: {$cf_doppi['tax_code']} <span class="badge squadrato">{count($cf_doppi['clienti_doppi'])}</span>

                <div class="panel-heading-action">
                
                </div>
            </div>

            <table class="table">
                <thead>
                    <tr>
                        <th><span class="title_box thick">{l s='ID CRM'}</span></th>
                        <th><span class="title_box thick">{l s='CF'}</span></th>
                        <th><span class="title_box thick">{l s='Email'}</span></th>
                        <th><span class="title_box thick">{l s='Nome'}</span></th>
                        <th><span class="title_box thick">{l s='Cognome'}</span></th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $cf_doppi['clienti_doppi'] AS $key => $clienti_doppi}
                    <tr class="menu_links" onclick="document.location='{$clienti_doppi['href']}';" onmouseover="">
                        <td>{if $clienti_doppi['id_customer'] == NULL}-{else}<a >{$clienti_doppi['id_customer']}</a>{/if}</td>
                        <td>{if $clienti_doppi['tax_code'] == NULL}-{else}{$clienti_doppi['tax_code']}{/if}</td>
                        <td>{if $clienti_doppi['email'] == NULL}-{else}{$clienti_doppi['email']}{/if}</td>
                        <td>{if $clienti_doppi['firstname'] == NULL}-{else}{$clienti_doppi['firstname']}{/if}</td>
                        <td>{if $clienti_doppi['lastname'] == NULL}-{else}{$clienti_doppi['lastname']}{/if}</td>
                    </tr>
                    {/foreach}
                </tbody>
            </table>					
            
        </div>
    </form>
    {/foreach}
{else}
<form class="form-horizontal col-lg-12"method="post" enctype="multipart/form-data" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-warning-sign"></i> {l s='There are no duplicates'}
        </div>

        <h2 class="text-muted text-center">
            {l s='No duplicate TAX codes were found.'} {if isset($easteregg)}(Meglio così){/if}
        </h2>
    </div>
</form>
{/if}