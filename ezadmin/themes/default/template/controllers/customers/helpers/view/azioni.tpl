{*
Lista di azioni e condizioni di verifica
Le view effettive sono nella cartella ./azioni/
*}

{if $smarty.get.azione == "bdl_edit"}
    {include file="./azioni/bdl_edit.tpl"}

{elseif $smarty.get.azione == "bdl_add"}
    {include file="./azioni/bdl_add.tpl"}

{elseif $smarty.get.azione == "bdl_delete"}

{elseif $smarty.get.azione == "bdl_pdf_tecnotel"}
    wip pdf tecnotel

{elseif $smarty.get.azione == "bdl_pdf_ezdirect"}
    wip pdf ezdirect

{elseif $smarty.get.azione == "fattura_view"}
    {include file="./azioni/fattura_view.tpl"}
    
{elseif $smarty.get.azione == "nota_di_credito_view"}
    {include file="./azioni/nota_di_credito_view.tpl"}

{elseif $smarty.get.azione == "tickets"}
    {include file="./azioni/tickets.tpl"}

{elseif $smarty.get.azione == "actions"}
    {include file="./azioni/actions.tpl"}

{elseif $smarty.get.azione == "todo"}
    {include file="./azioni/todo.tpl"}

{elseif $smarty.get.azione == "contratto_view"}
    {include file="./azioni/contratto_view.tpl"}

{elseif $smarty.get.azione == "contratto_edit"}
    {include file="./azioni/contratto_edit.tpl"}

{elseif $smarty.get.azione == "contratto_add"}
    {include file="./azioni/contratto_add.tpl"}
    
{else}
    {include file="./azioni/not_found.tpl"}
{/if}