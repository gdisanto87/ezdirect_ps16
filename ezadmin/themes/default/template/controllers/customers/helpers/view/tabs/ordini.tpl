<form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" name="ordini" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-file"></i> {l s='Orders'} <span class="badge">{count($orders)}</span>
        </div>
        <div class="panel-heading-action">
            <div class="bottoni-tab">
                <a class="btn btn-default" href="{$link->getAdminLink('AdminCarts')}&viewcart&createnew&provvisorio=1&preventivo=2&id_customer={$customer->id}#modifica-carrello">
                    <i class="icon-plus-sign"></i>
                    Nuovo ordine manuale
                </a>
            </div>
        </div>
        {if $orders AND count($orders)}
            {assign var=count_ok value=count($orders_ok)}
            {assign var=count_ko value=count($orders_ko)}
            <div class="panel">
                <div class="row">
                    <div class="col-lg-6">
                        <i class="icon-ok-circle icon-big"></i>
                        {l s='Valid orders:'}
                        <span class="label label-success">{$count_ok}</span>
                        {l s='for a total amount of %s' sprintf=$total_ok}
                    </div>
                    <div class="col-lg-6">
                        <i class="icon-exclamation-sign icon-big"></i>
                        {l s='Invalid orders:'}
                        <span class="label label-danger">{$count_ko}</span>
                    </div>
                </div>
            </div>

            {* Ordini andati a buon fine *}
            {if $count_ok}
                <table class="table tabella_datatable" id="ordiniok_table">
                    <thead>
                        <tr>
                            <th class="center"><span class="title_box ">{l s='ID'}</span></th>
                            <th><span class="title_box">{l s='Date'}</span></th>
                            <th><span class="title_box">{l s='Status'}</span></th>
                            <th><span class="title_box">{l s='Technical status'}</span></th>
                            <th><span class="title_box">{l s='Cart'}</span></th>
                            <th><span class="title_box">{l s='Payment'}</span></th>
                            <th><span class="title_box">{l s='Tracking'}</span></th>
                            <th><span class="title_box">{l s='Products'}</span></th>
                            <th><span class="title_box">{l s='Total spent'}</span></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach $orders_ok AS $key => $order}
                        <tr class="pointer" onclick="document.location = '{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;id_order={$order['id_order']}&amp;vieworder'" {if $order['order_state'] == "Annullato"}style="text-decoration: line-through; color: red;"{/if}>
                            <td>{$order['id_order']}</td>
                            <td><a href="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;id_order={$order['id_order']}&amp;vieworder">{dateFormat date=$order['date_add'] full=0}</a></td>
                            <td>{$order['order_state']}</td>
                            <td>{$order['technical_status']}</td>
                            <td>{$order['cart_id']}</td>
                            <td>{$order['payment']}</td>
                            <td>{$order['tracking']}</td>
                            <td>{$order['nb_products']}</td>
                            <td>{$order['total_paid_real']}</td>
                            <td>
                                <a class="btn btn-default" style="border-radius:0px;" href="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;id_order={$order['id_order']}&amp;vieworder">
                                    <i class='icon-search'></i> {l s='View'}
                                </a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            {/if}

            <hr />

            {* Ordini NON andati a buon fine *}
            {if $count_ko}
                <table class="table tabella_datatable" id="ordiniko_table">
                    <thead>
                        <tr>
                            <th class="center"><span class="title_box ">{l s='ID'}</span></th>
                            <th><span class="title_box">{l s='Date'}</span></th>
                            <th><span class="title_box">{l s='Status'}</span></th>
                            <th><span class="title_box">{l s='Technical status'}</span></th>
                            <th><span class="title_box">{l s='Cart'}</span></th>
                            <th><span class="title_box">{l s='Payment'}</span></th>
                            <th><span class="title_box">{l s='Tracking'}</span></th>
                            <th><span class="title_box">{l s='Products'}</span></th>
                            <th><span class="title_box">{l s='Total spent'}</span></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $orders_ko AS $key => $order}
                        <tr class="pointer" onclick="document.location = '{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;id_order={$order['id_order']}&amp;vieworder'" {if $order['order_state'] == "Annullato"}style="text-decoration: line-through; color: red;"{/if}>
                            <td>{$order['id_order']}</td>
                            <td><a href="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;id_order={$order['id_order']}&amp;vieworder">{dateFormat date=$order['date_add'] full=0}</a></td>
                            <td>{$order['order_state']}</td>
                            <td>{$order['technical_status']}</td>
                            <td>{$order['cart_id']}</td>
                            <td>{$order['payment']}</td>
                            <td>{$order['tracking']}</td>
                            <td>{$order['nb_products']}</td>
                            <td>{$order['total_paid_real']}</td>
                            <td>
                                <a class="btn btn-default" style="border-radius:0px;" href="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;id_order={$order['id_order']}&amp;vieworder">
                                    <i class='icon-search'></i> {l s='View'}
                                </a>
                            </td>
                        </tr>
                        {/foreach}
                    </tbody>
                </table>
            {/if}
        {else}
        <p class="text-muted text-center">
            {l s='%1$s has not placed any orders yet' sprintf=[$customer->company]}
        </p>
        {/if}
    </div>
</form>