<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="form-row">
                <div class="row">
                    <h2 class="blu" style="text-align:center">
                        Modifica - Contratto assistenza n.{$contratti['contratti_edit']['dati']['id_contratto']}
                    </h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-user"></i> 
                Contratto
            </div>

            <form action="{$link_gen_base}tab_name=contracts&amp;azione=contratto_edit&amp;id_contratto={$contratti['contratti_edit']['dati']['id_contratto']}&amp;submitted=y{$token_base}" method="post" onsubmit="return checkFormContratto();">
                <div class="form-row">
                    <div class="row">
                        <div class="form-group col-md-2">
                            <label>N. contratto</label>
                            <p class="form-control-static">
                                <input type="text" name="id_contratto" id="id_contratto" value="{$contratti['contratti_edit']['dati']['id_contratto']}" readonly>
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Rif. fattura</label>
                            <p class="form-control-static">
                                <input type="text" name="rif_fattura" id="rif_fattura" value="{$contratti['contratti_edit']['dati']['rif_fattura']}">
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Rif. noleggio</label>
                            <p class="form-control-static">
                                <input type="text" name="rif_noleggio" id="rif_noleggio" value="{$contratti['contratti_edit']['dati']['rif_noleggio']}">
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Rif. ordine</label>
                            <p class="form-control-static">
                                <input type="text" name="rif_ordine" id="rif_ordine" value="{$contratti['contratti_edit']['dati']['rif_ordine']}">
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Tipo</label>
                            <p class="form-control-static">
                                <select name="tipo" id="tipo">
                                    <option value="1" {if $contratti['contratti_edit']['dati']['tipo'] == 1}selected="selected"{/if}>Base</option>
                                    <option value="2" {if $contratti['contratti_edit']['dati']['tipo'] == 2}selected="selected"{/if}>Top</option>
                                    <option value="3" {if $contratti['contratti_edit']['dati']['tipo'] == 3}selected="selected"{/if}>Gold</option>
                                    <option value="4" {if $contratti['contratti_edit']['dati']['tipo'] == 4}selected="selected"{/if}>Teleassistenza</option>
                                    <option value="5" {if $contratti['contratti_edit']['dati']['tipo'] == 5}selected="selected"{/if}>Noleggio</option>
                                </select>
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Stato</label>
                            <p class="form-control-static">
                                <select name="status" id="status">
                                    <option value="0" {if $contratti['contratti_edit']['dati']['status'] == 0}selected="selected"{/if}>Attivo</option>
                                    <option value="1" {if $contratti['contratti_edit']['dati']['status'] == 1}selected="selected"{/if}>Sospeso</option>
                                    <option value="2" {if $contratti['contratti_edit']['dati']['status'] == 2}selected="selected"{/if}>Cancellato</option>
                                    <option value="3" {if $contratti['contratti_edit']['dati']['status'] == 3}selected="selected"{/if}>Disdetto da cliente</option>
                                    <option value="4" {if $contratti['contratti_edit']['dati']['status'] == 4}selected="selected"{/if}>Scaduto</option>
                                </select>
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <label>Importo</label>
                            <p class="form-control-static">
                                <input type="text" name="importo" id="importo" value="{$contratti['contratti_edit']['dati']['importo']}" disabled>
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Cadenza</label>
                            <p class="form-control-static">
                                <select name="cadenza" id="cadenza">
                                    <option value="Mensile" {if $contratti['contratti_edit']['dati']['cadenza'] == "Mensile"}selected="selected"{/if}>Mensile</option>
                                    <option value="Annuale" {if $contratti['contratti_edit']['dati']['cadenza'] == "Annuale"}selected="selected"{/if}>Annuale</option>
                                    <option value="Bimestrale" {if $contratti['contratti_edit']['dati']['cadenza'] == "Bimestrale"}selected="selected"{/if}>Bimestrale</option>
                                    <option value="Trimestrale" {if $contratti['contratti_edit']['dati']['cadenza'] == "Trimestrale"}selected="selected"{/if}>Trimestrale</option>
                                    <option value="Semestrale" {if $contratti['contratti_edit']['dati']['cadenza'] == "Semestrale"}selected="selected"{/if}>Semestrale</option>
                                </select>
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Rata</label>
                            <p class="form-control-static">
                                <input type="text" name="rata_contratto" id="rata_contratto" value="{$contratti['contratti_edit']['dati']['rata']}" readonly>
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Pagamento</label>
                            <p class="form-control-static">
                                <select name="pagamento" id="pagamento">
                                    <option value="" {if $contratti['contratti_edit']['dati']['pagamento'] == ""}selected="selected"{/if}>Nessun metodo</option>
                                    <option value='Bonifico Bancario' {if $contratti['contratti_edit']['dati']['pagamento'] == 'Bonifico Bancario'}selected="selected"{/if}>Bonifico</option>
                                    <option value='GestPay' {if $contratti['contratti_edit']['dati']['pagamento'] == 'GestPay'}selected="selected"{/if}>Carta di credito (Gestpay)</option>
                                    <option value='PayPal' {if $contratti['contratti_edit']['dati']['pagamento'] == 'PayPal'}selected="selected"{/if}>PayPal</option>
                                    <option value='Contrassegno' {if $contratti['contratti_edit']['dati']['pagamento'] == 'Contrassegno'}selected="selected"{/if}>Contrassegno</option>
                                    <option value='Bonifico 30 gg. fine mese' {if $contratti['contratti_edit']['dati']['pagamento'] == 'Bonifico 30 gg. fine mese'}selected="selected"{/if}>Bonifico 30 gg. fine mese</option>
                                    <option value='Bonifico 60 gg. fine mese' {if $contratti['contratti_edit']['dati']['pagamento'] == 'Bonifico 60 gg. fine mese'}selected="selected"{/if}>Bonifico 60 gg. fine mese</option>
                                    <option value='Bonifico 90 gg. fine mese' {if $contratti['contratti_edit']['dati']['pagamento'] == 'Bonifico 90 gg. fine mese'}selected="selected"{/if}>Bonifico 90 gg. fine mese</option>
                                    <option value='Bonifico 30 gg. 15 mese successivo' {if $contratti['contratti_edit']['dati']['pagamento'] == 'Bonifico 30 gg. 15 mese successivo'}selected="selected"{/if}>Bonifico 30 gg. 15 mese successivo</option>
                                    <option value='R.B. 30 GG. 5 mese successivo' {if $contratti['contratti_edit']['dati']['pagamento'] == 'R.B. 30 GG. 5 mese successivo'}selected="selected"{/if}>R.B. 30 GG. 5 mese successivo</option>
                                    <option value='R.B. 30 GG. 10 mese successivo' {if $contratti['contratti_edit']['dati']['pagamento'] == 'R.B. 30 GG. 10 mese successivo'}selected="selected"{/if}>R.B. 30 GG. 10 mese successivo</option>
                                    <option value='R.B. 60 GG. 5 mese successivo' {if $contratti['contratti_edit']['dati']['pagamento'] == 'R.B. 60 GG. 5 mese successivo'}selected="selected"{/if}>R.B. 60 GG. 5 mese successivo</option>
                                    <option value='R.B. 60 GG. 10 mese successivo' {if $contratti['contratti_edit']['dati']['pagamento'] == 'R.B. 60 GG. 10 mese successivo'}selected="selected"{/if}>R.B. 60 GG. 10 mese successivo</option>
                                    <option value='R.B. 60 GG. D.F. F.M.' {if $contratti['contratti_edit']['dati']['pagamento'] == 'R.B. 60 GG. D.F. F.M.'}selected="selected"{/if}>R.B. 60 GG. D.F. F.M.</option>
                                    <option value='Renting' {if $contratti['contratti_edit']['dati']['pagamento'] == 'Renting'}selected="selected"{/if}>Renting</option>
                                </select>
                            </p>
                        </div>
                        {*if $contratti['contratti_edit']['dati']['status'] == 4}
                        <div class="form-group col-md-2"> 
                            <p class="form-control-static">
                                <label>Rinnova</label>
                                <div id="rinnova_contratto_button">
                                    <button type="button" class="btn btn-secondary" onclick="rinnova_contratto()">
                                        <i class="icon-check" style="color:green" alt="Rinnova" title="Rinnova"></i> Rinnova contratto
                                    </button>
                                </div>
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <p class="form-control-static">
                                <label>Annulla</label>
                                <div id="annulla_rinnova_contratto_button">
                                    <button type="button" class="btn btn-secondary" onclick="annulla_rinnova_contratto({$link_gen_base}tab_name=contracts&amp;azione=contratto_edit&amp;id_contratto={$contratti['contratti_edit']['dati']['id_contratto']}{$token_base})">
                                        <i class="icon-remove" style="color:red" alt="Annulla rinnova" title="Annulla rinnova"></i> Annulla contratto
                                    </button>
                                </div>
                            </p>
                        </div>
                        {/if*}
                    </div>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <label>CIG</label>
                            <p class="form-control-static">
                                <input type="text" name="cig" id="cig" value="{$contratti['contratti_edit']['dati']['cig']}">
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>CUP</label>
                            <p class="form-control-static">
                                <input type="text" name="cup" id="cup" value="{$contratti['contratti_edit']['dati']['cup']}">
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Cod. univoco ufficio x fatt. elettronica</label>
                            <p class="form-control-static">
                                <input type="text" name="univoco" id="univoco" value="{$contratti['contratti_edit']['dati']['univoco']}">
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Data registrazione</label> {* Trasformare in data *}
                            <p class="form-control-static">
                                <input type="text" name="data_registrazione" id="data_registrazione" value="{$contratti['contratti_edit']['dati']['data_registrazione']}">
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Data inizio</label> {* Trasformare in data *}
                            <p class="form-control-static">
                                <input type="text" name="data_inizio" id="data_inizio" value="{$contratti['contratti_edit']['dati']['data_inizio']}">
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Data fine</label> {* Trasformare in data *}
                            <p class="form-control-static">
                                <input type="text" name="data_fine" id="data_fine" value="{$contratti['contratti_edit']['dati']['data_fine']}">
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label>Sede</label>
                            <p class="form-control-static">
                                <select name="indirizzo_contratto" id="indirizzo_contratto">
                                {foreach $contratti['contratti_edit']['dati']['indirizzi'] as $del_a}
                                    <option value="{$del_a['id_address']}" {if $contratti['contratti_edit']['dati']['indirizzo'] == $del_a['id_address']}selected="selected"{/if}>{$del_a['address1']} - {$del_a['city']}</option>
                                {/foreach}
                                </select>
                            </p>
                        </div>
                        <div class="form-group col-md-2 rosso">
                            <label>Blocco amministrativo</label>
                            <p class="form-control-static rosso">
                                <input type="checkbox" name="blocco_amministrativo" id="blocco_amministrativo" {if $contratti['contratti_edit']['dati']['blocco_amministrativo'] == 1}checked="checked"{/if}>
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <br />
                        <strong>Prodotti</strong>

                        <table class='table'>
                            <thead>
                                <tr>
                                    <th><span class="title_box ">Codice</span></th>
                                    <th><span class="title_box ">Descrizione</span></th>
                                    <th><span class="title_box ">Qta</span></th>
                                    <th><span class="title_box ">Prezzo</span></th>
                                    <th><span class="title_box ">Seriale</span></th>
                                    <th><span class="title_box ">Indirizzo</span></th>
                                    {if !$check_admin2}<th><span class="title_box "></span></th>{/if}
                                </tr>
                            </thead>
                            <tbody id='tableProductsBody'>
                                {if count($contratti['contratti_edit']['prodotti'])}
                                {foreach $contratti['contratti_edit']['prodotti'] AS $key => $prodotto}
                                <tr id='tr_{$prodotto['product_id']}'>
                                    <td>{if $prodotto['product_id']}<a class="tooltip_prodotto" title="{$prodotto['tooltip']}" href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}&amp;id_product={$prodotto['product_id']|intval}&amp;updateproduct&amp;token={getAdminToken tab='AdminProducts'}" target="_blank">{$prodotto['reference']}</a>{else}{$prodotto['reference']}{/if}</td>
                                    <td><input type='text' name='prodotto_descrizione[{$prodotto['product_id']}]' value='{$prodotto['name']}' /></td>
                                    <td>
                                        <input type='hidden' name='prodotto[{$prodotto['product_id']}]' checked='checked' />
                                        <input type='text' size='3' id='quantita[{$prodotto['product_id']}]' name='quantita[{$prodotto['product_id']}]' value='{$prodotto['quantita']}' onkeyup='CalcTotContratto();' />
                                    </td>
                                    <td class="text-right"><input type='text' size='7' class='importo' name='prezzo_prodotto[{$prodotto['product_id']}]' id='prezzo_prodotto[{$prodotto['product_id']}]' value='{$prodotto['prezzo_format']}' onkeyup='CalcTotContratto();' /></td>
                                    <td><input type='text' size='7' name='seriale_prodotto[{$prodotto['product_id']}]' value='{$prodotto['seriale']}' /></td>
                                    <td>
                                        <select name="indirizzo_prodotto[{$prodotto['product_id']}]" id="indirizzo_prodotto">
                                        {foreach $contratti['contratti_edit']['dati']['indirizzi'] as $del_a}
                                            <option value="{$del_a['id_address']}" {if $prodotto['indirizzo'] == $del_a['id_address']}selected="selected"{/if}>{$del_a['address1']} - {$del_a['city']}</option>
                                        {/foreach}
                                        </select>
                                    </td>
                                    {if !$check_admin2} {* Disabilitato *}
                                    <td><a style="cursor:pointer" onclick="togliImportoContratto({$prodotto['product_id']}); CalcTotContratto(); delProduct30Contratto({$prodotto['product_id']})"><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a></td>
                                    {/if}
                                </tr>
                                {/foreach}
                                {/if}
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan='2'><span>Tot. Importo Contratto IVA escl.</span></td>
                                    <td></td>
                                    <td><span class='tab-span' id='totale_contratto' style='text-align:right; float:left'>{$contratti['contratti_edit']['dati']['prezzo_format']}</span></td>
                                    <td><input type='hidden' name='totale_importo' id='totale_importo' value='{$contratti['contratti_edit']['dati']['prezzo']}' /></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-8">
                            <label for="note_private">Note private</label>
                            <textarea name="note_private" id="note_private" rows="4">{$contratti['contratti_edit']['dati']['note_private']|escape:"htmlall"}</textarea>
                        </div>
                    </div>

                    <div class="row" id="dettagli_controlli">
                        <div class="col-md-4" id="dettagli_pagamento_contratto">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th><span class="title_box center_text">{l s='Paid'}</span></th>
                                        <th><span class="title_box center_text">{l s='Unpaid'}</span></th>
                                        <th><span class="title_box center_text">{l s='Billed'}</span></th>
                                        <th><span class="title_box center_text">{l s='To be invoiced'}</span></th>
                                    </tr>
                                </thead>
                                <tbody class="center">
                                    <tr>
                                        <td><input class="super_center" type="checkbox" name="pagato" id="pagato"{if $contratti['contratti_edit']['dati']['pagato'] == "1"} checked="checked"{/if} value="1" onchange="verifica_contabilita('pagato')"></td>
                                        <td><input class="super_center" type="checkbox" name="da_pagare" id="da_pagare"{if $contratti['contratti_edit']['dati']['pagato'] == "0"} checked="checked"{/if} value="0" onchange="verifica_contabilita('da_pagare')"></td>
                                        <td><input class="super_center" type="checkbox" name="fatturato" id="fatturato"{if $contratti['contratti_edit']['dati']['fatturato'] == "1"} checked="checked"{/if} value="1" onchange="verifica_contabilita('fatturato')"></td>
                                        <td><input class="super_center" type="checkbox" name="da_fatturare" id="da_fatturare"{if $contratti['contratti_edit']['dati']['fatturato'] == "0"} checked="checked"{/if} value="0" onchange="verifica_contabilita('da_fatturare')"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-1">
                        </div>

                        {if $check_employee}
                        <div class="col-md-2" id="invii_controlli" {if $contratti['contratti_edit']['dati']['invio_contabilita'] == 2} style="display:none"{/if}>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th><span class="title_box center_text">{l s='Accounting check'} {* Conferma per contabilità *} </span></th>
                                    </tr>
                                </thead>
                                <tbody class="center">
                                    <tr>
                                        {if $contratti['contratti_edit']['dati']['invio_contabilita'] == 1 || $contratti['contratti_edit']['dati']['invio_contabilita'] == 2}
                                        <td class="text-center"><input type="hidden" name="invio_contabilita" id="invio_contabilita" value="2" />Gi&agrave; inviato</td>
                                        {else}
                                        <td><input class="super_center" type="checkbox" name="invio_contabilita" id="invio_contabilita" value="1"></td>
                                        {/if}
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        {else}
                            {if $contratti['contratti_edit']['dati']['invio_contabilita'] == 1 || $contratti['contratti_edit']['dati']['invio_contabilita'] == 2}
                            <td><input type="hidden" name="invio_contabilita" id="invio_contabilita" value="2" /></td>
                            {/if}
                        {/if}
                    </div>

                    <div class="row">
                        <input type="hidden" name="rinnova_contratto_ok" id="rinnova_contratto_ok" value="0" />
                        <br />
                        <button type="submit" id="conferma_modifiche_contratto" class="btn btn-secondary">
                            <i class="icon-check" style="color:green" alt="Conferma" title="Conferma"></i> Conferma modifiche
                        </button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-user"></i> 
                Storico
            </div>

            <div class="form-row">
                <div class="row">
                    {if count($contratti['contratti_edit']['storico']) == 0}
                        <p>Nessuna informazione</p>
                    {else}
                        <table class="table">
                            <thead>
                                <tr>
                                    <th><span class="title_box ">Data</span></th>
                                    <th><span class="title_box ">Dati storici</span></th>
                                </tr>
                            </thead>
                            <tbody>
                            {foreach $contratti['contratti_edit']['storico'] as $storico}
                                <tr>
                                    <td>{$storico['data_attivita']}</td>
                                    <td>{$storico['desc_attivita']}</td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    {/if}
                </div>
            </div>
        </div>
    </div>
</div>