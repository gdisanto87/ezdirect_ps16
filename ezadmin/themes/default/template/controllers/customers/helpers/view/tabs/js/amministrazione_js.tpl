<script>

window.onload = function() {
};

{if $blacklist_in}
    tata.error("Il cliente è stato messo in blacklist", "");
{else if $blacklist_out}
    tata.warn("Il cliente è stato tolto dalla blacklist", "");
{/if}

function f_edit_btn(){
    if(document.getElementById("edit_adm").value == 0) {
        // modalità di edit abilitata
        document.getElementById("edit_adm").value = 1;
        document.getElementById("edit_title").innerHTML = "(MODIFICA)";
        document.getElementById("edit_btn").style.display = "none";
        document.getElementById("edit_section").style.display = "block";
        //document.getElementById("save_btn").style.display = "block";

        abilita_tutto();
    } else {
        // disattiva la modalità di edit, in realtà forse sarebbe meglio un refresh per far annullare le modifiche al form
        //window.location.reload();
    }
}

function f_reset_btn() {
    resetta_tutto();
}

/*
function f_no_addebito_commissioni() {
    if(document.getElementById("edit_adm").value == 1){
        if(document.getElementById("no_addebito_commissioni").value == 0){
            //porta ad 1 il valore hidden e modifica la grafica
            //document.getElementById("span_no_addebito_commissioni").className = "label label-warning squadrato";
            document.getElementById("span_no_addebito_commissioni").className = "label label-primary squadrato";
            document.getElementById("icon_no_addebito_commissioni").className = "icon-check";
            document.getElementById("no_addebito_commissioni").value = 1;
        }else{
            //porta a 0 il valore hidden e modifica la grafica
            //document.getElementById("span_no_addebito_commissioni").className = "label label-primary squadrato";
            document.getElementById("span_no_addebito_commissioni").className = "label label-warning squadrato";
            document.getElementById("icon_no_addebito_commissioni").className = "icon-remove";
            document.getElementById("no_addebito_commissioni").value = 0;
        }
    } else {
        alert("Per modificare il campo, selezionare la modalità di modifica");
    }
}
*/

function abilita_tutto() {
    abi("tipo_soggetto");
    abi("codice_fornitore");
    abi("pagamento");
    abi("fido");
    abi("assicurazione_credito");
    abi("data_fido");
    abi("data_rinnovo");
    abi("data_revoca");
    abi("motivo_revoca");
    abi("trasporto_a_mezzo");
    abi("consegna");
    abi("vettore");
    abi("id_corriere");
    abi("trasporto_gratis_da");
    abi("fermo_deposito");
    abi("note_consegna");
    abi("iban");
    abi("swift");
    abi("paypal");
    abi("iva_agevolata");
    abi("sconto_extra");
    abi("per_ordini_da");
    abi("fino_a");
    abi("rebate");
    abi("agente");
    abi("tecnico");
    abi_fnc_all();
}

function abi_fnc_all(){
    abi_addebito_commissioni();
    abi_trasporto_assicurato();
    abi_esportatore_abituale();
    abi_blacklist();
}

function abi_addebito_commissioni(){
    document.getElementById("addebito_commissioni_let").style.display = "none";
    document.getElementById("addebito_commissioni_mod").style.display = "block";
}

function abi_trasporto_assicurato(){
    document.getElementById("trasporto_assicurato_let").style.display = "none";
    document.getElementById("trasporto_assicurato_mod").style.display = "block";
}

function abi_esportatore_abituale(){
    document.getElementById("esportatore_abituale_let").style.display = "none";
    document.getElementById("esportatore_abituale_mod").style.display = "block";
}

function abi_blacklist(){
    document.getElementById("blacklist_let").style.display = "none";
    document.getElementById("blacklist_mod").style.display = "block";
}

function disabilita_tutto() {

}

function resetta_tutto() {
    window.location.reload();
}

function abi(obj){
    document.getElementById(obj).disabled = false;
    document.getElementById(obj).classList.remove("lettura");
}

function rst(obj) {
    // non funzionante ?
    document.getElementById(obj).reset();
}

function hasClass(ele, cls) {
    return ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
}
function addClass(ele, cls) {
    if (!hasClass(ele, cls)) ele.className += " " + cls;
}
function removeClass(ele, cls) {
    if (hasClass(ele, cls)) {
        var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
        ele.className = ele.className.replace(reg, ' ');
    }
}

</script>