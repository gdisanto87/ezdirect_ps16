{if $is_agente == 0}
<form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" novalidate>

    <div class="panel">
        <div class="panel-heading">
            <i class="icon-time"></i> {l s='BDL'} <span class="badge squadrato">{count($bdl['buoni'])}</span>

            <div class="panel-heading-action">
                <a class="btn btn-default" href="{$link_gen_base}tab_name=bdl&amp;azione=bdl_add{$token_base}">
                    <i class="icon-plus-sign"></i>
                    {l s='Generate new BDL'}
                </a>
            </div>
        </div>

        {if count($bdl['buoni'])}
        {* Sono presenti {$count_bdl} buoni di lavoro per questa anagrafica *}
        {*<p>{l s='%1$s Job Vouchers (BDL) found' sprintf=count($bdl['buoni'])}</p> *}
        <table class="table tabella_datatable" id="bdl_table">
            <thead>
            <tr>
                <th><span class="title_box">{l s='N. BDL'}</span></th>
                <th><span class="title_box">{l s='Date BDL'}</span></th>
                <th><span class="title_box">{l s='Reason'}</span></th>
                <th><span class="title_box">{l s='Amount'}</span></th>
                <th><span class="title_box">{l s='Status'}</span></th>
                <th><span class="title_box">{l s='Made by'}</span></th>
                <th><span class="title_box">{l s='Actions'}</span></th>
            </tr>
            </thead>
            <tbody>
            {foreach $bdl['buoni'] AS $key => $righe_buoni}
                <tr class="pointer">
                    <td>{$righe_buoni['id_bdl']}</td>
                    <td>{$righe_buoni['data_bdl']}</td>
                    <td>{$righe_buoni['motivo_chiamata']}</td>
                    <td>{$righe_buoni['importo']}</td>
                    {* qui mi passi l'intera riga class? *}
                    <td><span {$righe_buoni['status_style']}>{$righe_buoni['status']}</span></td>
                    <td>{$righe_buoni['fatto_da']}</td>
                    <td>
                        <a class="btn btn-default" style="border-radius:0px;" href="{$link_gen_base}tab_name=bdl&amp;azione=bdl_edit&amp;id_bdl={$righe_buoni['id_bdl']}{$token_base}" title="Modifica">
                            <i class='icon-pencil'></i> {l s='Edit'}
                        </a>
                        {if !$is_agente}
                        <a class="btn btn-danger" style="border-radius:0px;" href="{$link_gen_base}tab_name=bdl&amp;azione=bdl_delete&amp;id_bdl={$righe_buoni['id_bdl']}{$token_base}" title="Cancella" onclick="return confirm('Sei sicuro/a?')">
                            <i class='icon-trash'></i> {*l s='Delete'*}
                        </a>
                        {/if}
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
        {else}
        {* Non sono presenti buoni di lavoro per questa anagrafica *}
        <p>{l s='No Job Vouchers (BDL) found'}</p>
        {/if}
    </div>
    
</form>
{else}
{$is_agente_activated = 1}
{/if}

