<form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" name="access_denied" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-warning-sign"></i> {l s='Access denied'}
        </div>

        <h2 class="text-muted text-center">
            {l s='You do not have the necessary rights to access '} <a class="text-danger">{$tab_name}</a> {l s='tab'}
        </h2>
    </div>
</form>