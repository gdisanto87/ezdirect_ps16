<form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" name="prodotti_visti" novalidate>
{* Da modificare, far stampare "nessun prodotto visualizzato" anzi che non stampare il panel*}
    {if count($interested)}
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-eye"></i> {l s='Viewed products'} <span class="badge squadrato">{count($interested)}</span>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th><span class="title_box ">{l s='ID'}</span></th>
                    <th><span class="title_box ">{l s='Name'}</span></th>
                </tr>
            </thead>
            <tbody>
            {foreach $interested as $key => $p}
                <tr onclick="document.location = '{$p['url']|escape:'html':'UTF-8'}'">
                    <td>{$p['id']}</td>
                    <td><a href="{$p['url']|escape:'html':'UTF-8'}">{$p['name']}</a></td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
    {/if}
</form>