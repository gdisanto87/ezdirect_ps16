<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading-action">
                <div class="bottoni-tab">
                    <a class="btn btn-default" href="{$link_gen_base}tab_name=credit_notes&amp;{$token_base}">
                        <i class="icon-circle-arrow-left"></i>
                        Vedi note di credito del cliente
                    </a>
                    <a class="btn btn-default" href="{$link->getAdminLink('AdminNoteDiCredito')|escape:'html':'UTF-8'}&amp;token={getAdminToken tab='AdminNoteDiCredito'}">
                        <i class="icon-circle-arrow-left"></i>
                        Vedi tutte le note di credito
                    </a>
                </div>
            </div>
            
            <div class="form-row">
                <div class="row">
                    <h2 class="blu" style="text-align:center">
                        {$note_di_credito["dati"]["info_cliente"]["nome_cliente"]} - {$note_di_credito["dati"]["dati_fattura"]["tipo"]|ucfirst} {$note_di_credito["dati"]["dati_fattura"]["id"]} - {$note_di_credito["dati"]["trasporto"]["data_trasporto"]} - {$note_di_credito["dati"]["tec_ez"]|ucfirst}
                    </h2>
                </div>
            </div>
            <div class="form-row">
                <div class="row" style="text-align:center">
                    <button type="button" class="btn btn-secondary" onclick="window.location.href='{$link_gen_base}tab_name=actions&amp;azione=actions&amp;viewmessage&amp;aprinuovomessaggio{$token_base}'"><i class="icon-comment"></i> Invia un messaggio al cliente</button>
                    <button type="button" class="btn btn-secondary"><i class="icon-download"></i> Scarica PDF</button>
                    <button type="button" class="btn btn-secondary"><i class="icon-envelope"></i> Invia nota al cliente tramite email</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-user"></i> 
                Informazioni sul cliente
            </div>

            <div class="form-row">
                <div class="row">
                    <div class="form-group col-md-2">
                        <label for="tipo_cliente">Tipo cliente</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["info_cliente"]["tipo_cliente"]}
                        </p>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="ragione_sociale">{if $customer->is_company}Ragione sociale{else}Nome e cognome{/if}</label>
                        <p class="form-control-static">
                            <a href="{$link->getAdminLink('AdminCustomers')|escape:'html':'UTF-8'}&amp;id_customer={$customer->id|intval}&amp;viewcustomer&amp;token={getAdminToken tab='AdminCustomers'}" target="_blank">{$note_di_credito["dati"]["info_cliente"]["nome_cliente"]}</a> (#{$customer->id})
                        </p>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="creazione_account">Data creazione account</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["info_cliente"]["data_creazione_account"]}
                        </p>
                    </div>

                    <div class="form-group col-md-2">
                        <label for="ordini_totali">Ordini totali</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["info_cliente"]["ordini_totali"]}
                        </p>
                    </div>

                    <div class="form-group col-md-2">
                        <label for="ordinato_totale">Acquisti totali</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["info_cliente"]["acquisti_totali"]}
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-user"></i> 
                Dati {$note_di_credito["dati"]["dati_fattura"]["tipo"]}
            </div>

            <div class="form-row">
                <div class="row">
                    <div class="form-group col-md-2">
                        <label for="id_fattura">ID {$note_di_credito["dati"]["dati_fattura"]["tipo"]}</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["dati_fattura"]["id"]}
                        </p>
                    </div>

                    <div class="form-group col-md-2">
                        <label for="codice_cliente">Codice cliente</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["dati_fattura"]["codice_cliente"]}
                        </p>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="tax_code">P.IVA - CF</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["dati_fattura"]["piva_cf"]}
                        </p>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="numero_documento">Numero documento</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["dati_fattura"]["numero_documento"]}
                        </p>
                    </div>

                    <div class="form-group col-md-2">
                        <label for="data_documento">Data documento</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["dati_fattura"]["data_documento"]}
                        </p>
                    </div>

                </div>

                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="mod_pagamento">Modalità di pagamento</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["dati_fattura"]["pagamento"]}
                        </p>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="valuta">Valuta</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["dati_fattura"]["valuta"]}
                        </p>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="rif_ordine">Rif. ordine sito</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["dati_fattura"]["rif_ordine_sito"]}
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    {* Indirizzo Fatturazione *}
    <div class="col-md-6">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-edit-sign"></i> 
                Indirizzo fatturazione
            </div>

            <table>
                <tr class="tab_cus">
                    <td class="tab_cus">Spett.le</td>
                    <td class="tab_cus thick">{$note_di_credito["dati"]["fatturazione"]["spettle"]}</td>
                </tr>
                <tr class="tab_cus">
                    <td class="tab_cus">Indirizzo</td>
                    <td class="tab_cus">{$note_di_credito["dati"]["fatturazione"]["indirizzo"]}</td>
                </tr>
                <tr class="tab_cus">
                    <td class="tab_cus">CAP</td>
                    <td class="tab_cus">{$note_di_credito["dati"]["fatturazione"]["cap"]}</td>
                </tr>
                <tr class="tab_cus">
                    <td class="tab_cus">Città</td>
                    <td class="tab_cus">{$note_di_credito["dati"]["fatturazione"]["citta"]}</td>
                </tr>
                <tr class="tab_cus">
                    <td class="tab_cus">Provincia</td>
                    <td class="tab_cus">{$note_di_credito["dati"]["fatturazione"]["prov"]}</td>
                </tr>
            </table>

        </div>
    </div>

    {* Indirizzo consegna *}
    <div class="col-md-6">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-truck"></i> 
                Indirizzo consegna
            </div>

            <table>
                <tr class="tab_cus">
                    <td class="tab_cus">Spett.le</td>
                    <td class="tab_cus thick">{$note_di_credito["dati"]["destinazione"]["spettle"]}</td>
                </tr>
                <tr class="tab_cus">
                    <td class="tab_cus">Indirizzo</td>
                    <td class="tab_cus">{$note_di_credito["dati"]["destinazione"]["indirizzo"]}</td>
                </tr>
                <tr class="tab_cus">
                    <td class="tab_cus">CAP</td>
                    <td class="tab_cus">{$note_di_credito["dati"]["destinazione"]["cap"]}</td>
                </tr>
                <tr class="tab_cus">
                    <td class="tab_cus">Città</td>
                    <td class="tab_cus">{$note_di_credito["dati"]["destinazione"]["citta"]}</td>
                </tr>
                <tr class="tab_cus">
                    <td class="tab_cus">Provincia</td>
                    <td class="tab_cus">{$note_di_credito["dati"]["destinazione"]["prov"]}</td>
                </tr>
            </table>

        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-shopping-cart"></i> Prodotti

                <div class="panel-heading-action">
                
                </div>
            </div>

            {if count($note_di_credito["dati"]["prodotti"])}
            <table class="table">
                <thead>
                    <tr>
                        <th><span class="title_box ">Cod. articolo</span></th>
                        <th><span class="title_box ">Desc. articolo</span></th>
                        <th><span class="title_box ">UM</span></th>
                        <th><span class="title_box ">Qta</span></th>
                        <th><span class="title_box ">Rif. ord</span></th>
                        <th><span class="title_box ">Prezzo</span></th>
                        <th><span class="title_box ">Sconti</span></th>
                        <th><span class="title_box ">Importo</span></th>
                        <th><span class="title_box ">C.I.</span></th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $note_di_credito["dati"]["prodotti"] AS $key => $prodotto}
                    <tr>
                        <td>{if $prodotto['id']}<a class="tooltip_prodotto" {* data-tooltip-content="#tooltip_content" *} title="{$prodotto['tooltip']}" href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}&amp;id_product={$prodotto['id']|intval}&amp;updateproduct&amp;token={getAdminToken tab='AdminProducts'}" target="_blank">{$prodotto['cod']}</a>{else}{$prodotto['cod']}{/if}</td>
                        <td>{$prodotto['desc']}</td>
                        <td>{$prodotto['um']}</td>
                        <td class="text-right">{$prodotto['qta']}</td>
                        <td class="text-right">{$prodotto['rif_ordine']}</td>
                        <td class="text-right">{$prodotto['prezzo']}</td>
                        <td class="text-right">{$prodotto['sconti']}</td>
                        <td class="text-right">{$prodotto['importo']}</td>
                        <td class="text-right">{$prodotto['ci']}</td>
                    </tr>
                    {/foreach}
                </tbody>
            </table>
            {else}
            <p class="text-muted text-center">
                Non sono stati trovati record
            </p>
            {/if}						
            
        </div>
    </div>
</div>
    

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-user"></i> 
                Totali
            </div>

            <div class="form-row">
                <div class="row">

                    <div class="form-group col-md-1">
                    </div>

                    <div class="form-group col-md-2">
                        <label for="tot_merce">Tot. merce</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["totali"]["tot_merce"]}
                        </p>
                    </div>

                    <div class="form-group col-md-2">
                        <label for="c_iva">C.IVA</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["totali"]["c_iva"]} %
                        </p>
                    </div>

                    <div class="form-group col-md-2">
                        <label for="imponibile">Imponibile</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["totali"]["imponibile"]}
                        </p>
                    </div>

                    <div class="form-group col-md-2">
                        <label for="iva">IVA</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["totali"]["iva"]}
                        </p>
                    </div>

                    <div class="form-group col-md-2">
                        <label for="totale_documento">Totale documento</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["totali"]["tot_fattura"]}
                        </p>
                    </div>

                    <div class="form-group col-md-1">
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-truck"></i> 
                Trasporto
            </div>

            <div class="form-row">
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="causale_trasporto">Causale trasporto</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["trasporto"]["causale_trasporto"]}
                        </p>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="trasporto_a_cura">Trasporto a cura</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["trasporto"]["trasporto_a_cura"]}
                        </p>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="vettore">Vettore</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["trasporto"]["vettore"]}
                        </p>
                    </div>

                    

                </div>

                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="porto">Porto</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["trasporto"]["porto"]}
                        </p>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="n_colli">N. Colli</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["trasporto"]["colli"]}
                        </p>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="peso">Peso</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["trasporto"]["peso"]}
                        </p>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="data_ora_trasporto">Data e ora trasporto</label>
                        <p class="form-control-static">
                            {$note_di_credito["dati"]["trasporto"]["data_trasporto"]}
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
