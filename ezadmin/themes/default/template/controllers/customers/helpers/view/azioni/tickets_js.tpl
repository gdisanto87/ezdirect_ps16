<script>

window.onload = function() {
    //listree();
}

/* Note private -> bug: non funziona la modifica di una nota appena creata */

{if $smarty.get.id_customer_thread !== null}

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
	var actions = $("td.azioni_note").html();

    $(".add-new").click(function(){
		$(this).attr("disabled", "disabled");
		var index = $("table.tabella_note > tbody > tr:last-child").index();
        var row = '<tr>' +
            //'<td><input type="text" class="form-control" name="nuova_nota" id="nuova_nota"></td>' +
            '<td><textarea class="form-control" name="nuova_nota" id="nuova_nota"></textarea></td>' +
            '<td>{$mio_nome}</td>' +
            '<td>{$today}</td>' +
			'<td>' + actions + '</td>' +
        '</tr>';
    	$("table.tabella_note").append(row);
		$("table.tabella_note > tbody > tr").eq(index + 1).find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip();
    });

	$(document).on("click", ".add", function(){
		var empty = false;
        var dbid = $(this).parents("tr").attr('dbid');
		//var input = $(this).parents("tr").find('input[type="text"]');
		var input = $(this).parents("tr").find('textarea');
        var questo = this;
        input.each(function(){
			if(!$(this).val()){
				$(this).addClass("error");
				empty = true;
			} else{
                $(this).removeClass("error");
            }
		});
		$(this).parents("tr").find(".error").first().focus();
		if(!empty){
			input.each(function(){
				$(this).parent("td").html($(this).val());
                if(dbid === undefined){
                    $.ajax({
                        url:"ajax.php?creanota_attivita=y",
                        type: "POST",
                        data: { 
                            id_action: {$smarty.get.id_customer_thread},
                            tipo: 'T',
                            nota: $(this).val()
                        },
                        success:function(data){
                            $(questo).parents("tr").attr('dbid', data);
                            tata.success('Nota creata con successo', 'ID '+data);
                        },
                        error: function(xhr,stato,errori){
                            tata.error('Errore nella creazione', xhr.status);
                        }
                    });
                } else {
                    $.ajax({
                        url:"ajax.php?modificanota_attivita=y",
                        type: "POST",
                        data: { 
                            id_nota: dbid,
                            nota: $(this).val()
                        },
                        success:function(){
                            tata.success('Nota modificata con successo', '');
                            succ = true;
                        },
                        error: function(xhr,stato,errori){
                            tata.error('Errore nella modifica', xhr.status);
                        }
                    });
                }
			});			
			$(this).parents("tr").find(".add, .edit").toggle();
			$(".add-new").removeAttr("disabled");
		}		
    });

    $(function() {
        $("input").keypress(function(event) {
            var $self = $(this);
            var comm = $self.val();

            var keycode = (event.keycode ? event.keycode : event.which);
            if (keycode == 13) {
                var comm = $self.val();
                alert(comm);
                event.stopPropagation();
            }
        });
    });

	$(document).on("click", ".edit", function(){		
        $(this).parents("tr").find("td:not(:last-child)").each(function(){
            if($(this).hasClass('mod')){ 
                //$(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">'); 
                $(this).html('<textarea class="form-control" style="height:80px;" >' + $(this).text() + '</textarea>'); 
            }
			
		});		
		$(this).parents("tr").find(".add, .edit").toggle();
		$(".add-new").attr("disabled", "disabled");
    });

	$(document).on("click", ".delete", function(){
        var dbid = $(this).parents("tr").attr('dbid');
        var succ = false;
        var questo = this;
        if(dbid === undefined){
            $(this).parents("tr").remove();
            $(".add-new").removeAttr("disabled");
        } else {
            if(confirm("Vuoi cancellare la nota con id "+dbid+" ?")){
                $.ajax({
                    url:"ajax.php?cancellanota_attivita=y",
                    type: "POST",
                    data: { 
                        cancella_nota: 'y',
                        id_nota: dbid
                    },
                    success:function(){
                        tata.success('Nota cancellata con successo', '');
                        succ = true;
                        $(questo).parents("tr").remove();
                        $(".add-new").removeAttr("disabled");
                    },
                    error: function(xhr,stato,errori){
                        tata.error('Errore nella cancellazione', xhr.status);
                    }
                });

            } else {
                
            }
        }
        /*
        if(succ == true){
            $(this).parents("tr").remove();
            $(".add-new").removeAttr("disabled");
            alert("eliminattoh");
        }

        $(this).parents("tr").remove();
		$(".add-new").removeAttr("disabled");
        */
    });
});

{/if}

/* In carico a */

$(document).ready(function(){
    {if $tickets["ticket_view"]["cronologia"]["impez_selected"] != 0}
    {literal}document.getElementById('conoscenza_{/literal}{$tickets["ticket_view"]["cronologia"]["impez_selected"]}{literal}').disabled = true;{/literal}
    {/if}
});

function changeInCaricoA()
{
    {foreach $tickets['ticket_view']['cronologia']['impiegati_eza'] as $impez}
    {literal}
        if (document.getElementById("in_carico_a").value == "{/literal}{$impez['id_employee']}{literal}") {
            document.getElementById('conoscenza_{/literal}{$impez["id_employee"]}{literal}').disabled = true;
            document.getElementById('conoscenza_{/literal}{$impez["id_employee"]}{literal}').checked = false;
        } 
        else { 
            document.getElementById('conoscenza_{/literal}{$impez["id_employee"]}{literal}').disabled = false; 
        }
    {/literal}
    {/foreach}
}

/* Link utili */

function addUtil_TR(event, data, formatted)
{
    tinyMCE.get('message').focus();

    var $body = $(tinymce.activeEditor.getBody());
    $body.prepend('<p>');
    $body.append('</p>');

    if(document.getElementById("utils_autocomplete_input_tkt").value != "-- Seleziona un link utile --") {
        $body.find("p:last").append('<a href="' + document.getElementById("utils_autocomplete_input_tkt").value + '">' + document.getElementById("utils_autocomplete_input_tkt").value + '</a><br /><br />');
    }
}

/* Precompilati */

function inserisciPrecompilato(msg)
{
    tinyMCE.get('message').focus();

    var $body = $(tinymce.activeEditor.getBody());
    $body.html('');
    $body.prepend('<p>');
    $body.append('</p>');
    if(msg != "-- Seleziona un messaggio --") {
        $body.find("p:last").append(msg);
    }
}

/* Anteprime file */

function handleFileSelect(files) 
{
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0; i < files.length; i++) {
        var f = files[i];
        var name = files[i].name;

        var reader = new FileReader();  
        reader.onload = function(e) {  
        // Render thumbnail.
        var span = document.createElement("span");
        span.innerHTML = ["<a href='",e.target.result,"' target='_blank'>",name,"</a> - "].join("");
        document.getElementById("list-action").insertBefore(span, null);
        };

    // Read in the image file as a data URL.
    reader.readAsDataURL(f);
    }
}

/* Conferma azioni -> CORREGGERE */

$(document).ready(function(){

    /* Conferma uscita dalla pagina se ci sono modifiche non salvate */
						
    dataChanged = 0; // global variable flags unsaved changes      

    function bindForChange(){    
        $('input,checkbox,text,textarea,radio,select').bind('change',function(event) { dataChanged = 1 });
        $('#messaggio').bind('input propertychange', function() { dataChanged = 1 }); // #messaggio non esiste?
        $(':reset,:submit').bind('click',function(event) { dataChanged = 0 });
    }

    function askConfirm(){  
        if (dataChanged){ 
            return "You have some unsaved changes. Press OK to continue without saving." 
        }
    }

    window.onbeforeunload = askConfirm;
    window.onload = bindForChange;

    /* Conferma submit del form di Cronologia */
    // Esiste solo if(!isset($_GET['aprinuovoticket']))
    /*$("#submit_ticket").submit(function(){
        d = document.getElementById("status").value;
        
        if(d != "{$azione_t['status']}") {
            // niente
        }
        else {
            var salvamodifiche = window.confirm("STATUS: OK per procedere, ANNULLA per cambiare."); 
    
            if (salvamodifiche) {
                $('#waiting1').waiting({ 
                    elements: 10, 
                    auto: true 
                });
                var overlay = jQuery('<div id="overlay"></div>');
                var attendere = jQuery('<div id="attendere">ATTENDERE PREGO</div>');
                overlay.appendTo(document.body);
                attendere.appendTo(document.body);

                $("#submitMessage").trigger("click"); // ma siamo sicuri che vale per tutte le azioni e non cambia mai nome?
            } 
            else {
                return false;
            }
        }
    });*/
});

/* Dettaglio costi RMA */

// Calcola il totale
function CalcTotRMA()
{
    var num_costo_materiale = document.getElementById('costo_materiale').value;
    var costo_materiale = parseFloat(num_costo_materiale.replace(/\s/g, "").replace(",", "."));
    if(isNaN(costo_materiale)) { 
        costo_materiale = 0;
    }
    
    var num_costo_manodopera = document.getElementById('costo_manodopera').value;
    var costo_manodopera = parseFloat(num_costo_manodopera.replace(/\s/g, "").replace(",", "."));
    if(isNaN(costo_manodopera)) { 
        costo_manodopera = 0;
    }
    
    var num_totale_trasporti = document.getElementById('totale_trasporti').value;
    var totale_trasporti = parseFloat(num_totale_trasporti.replace(/\s/g, "").replace(",", "."));
    if(isNaN(totale_trasporti)) { 
        totale_trasporti = 0;
    }

    var totale_RMA = costo_manodopera + costo_materiale + totale_trasporti;

    document.getElementById('totale_RMA').value = totale_RMA.toFixed(2).replace('.',',');
}

</script>