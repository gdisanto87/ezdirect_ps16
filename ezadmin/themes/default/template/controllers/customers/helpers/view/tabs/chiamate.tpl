{if $is_agente == 0}
<form class="form-horizontal col-lg-6" action="" method="post" enctype="multipart/form-data" name="telefonate_ricevute" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-mail-reply"></i> {l s='Inbound Calls'} <span class="badge squadrato">{count($telefonate['ricevute'])}</span>

            <div class="panel-heading-action">
            
            </div>
        </div>

        {if count($telefonate['ricevute'])}
        <table class="table">
            <thead>
                <tr>
                    <th><span class="title_box ">{l s='From'}</span></th>
                    <th><span class="title_box ">{l s='To'}</span></th>
                    <th><span class="title_box ">{l s='Date'}</span></th>
                    <th><span class="title_box ">{l s='Time'}</span></th>
                    <th><span class="title_box ">{l s='Status'}</span></th>
                </tr>
            </thead>
            <tbody>
                {foreach $telefonate['ricevute'] AS $key => $call}
                <tr>
                    <td>{$call['da']}</td>
                    <td>{$call['a']}</td>
                    <td>{$call['data']}</td>
                    <td>{$call['durata']}</td>
                    <td>{$call['stato']}</td>
                    
                </tr>
                {/foreach}
            </tbody>
        </table>
        {else}
        <p class="text-muted text-center">
            {l s='No records found'}
        </p>
        {/if}
    </div>
</form>

<form class="form-horizontal col-lg-6" action="" method="post" enctype="multipart/form-data" name="telefonate_inviate" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-mail-forward"></i> {l s='Outbound Calls'} <span class="badge squadrato">{count($telefonate['inviate'])}</span>

            <div class="panel-heading-action">
            
            </div>
        </div>

        {if count($telefonate['inviate'])}
        <table class="table">
            <thead>
                <tr>
                    <th><span class="title_box ">{l s='From'}</span></th>
                    <th><span class="title_box ">{l s='To'}</span></th>
                    <th><span class="title_box ">{l s='Date'}</span></th>
                    <th><span class="title_box ">{l s='Time'}</span></th>
                    <th><span class="title_box ">{l s='Status'}</span></th>
                </tr>
            </thead>
            <tbody>
                {foreach $telefonate['inviate'] AS $key => $call}
                <tr>
                    <td>{$call['da']}</td>
                    <td>{$call['a']}</td>
                    <td>{$call['data']}</td>
                    <td>{$call['durata']}</td>
                    <td>{$call['stato']}</td>
                    {*}
                    <td class="text-right">
                        <div class="btn-group">
                            <a class="btn btn-default squadrato" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;addaddress=1&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
                                <i class="icon-edit"></i> { l s='Edit' }
                            </a>
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> 
                                <span class="caret"></span> 
                                </button> 
                                <ul class="dropdown-menu"> 
                                    <li> 
                            
                            <a class="btn btn-danger squadrato" href="?tab=AdminAddresses&amp;id_address={$address['id_address']}&amp;deleteaddress&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
                                <i class="icon-trash"></i>
                                { l s='Delete' }
                            </a>
                                    </li> 
                            </ul> 
                        </div>
                    </td>
                    {*}
                </tr>
                {/foreach}
            </tbody>
        </table>
        {else}
        <p class="text-muted text-center">
            {l s='No records found'}
        </p>
        {/if}						
        
    </div>
</form>

</div>
<div class="row">
{* STATISTICHE *}
<form class="form-horizontal col-lg-6" action="" method="post" enctype="multipart/form-data" name="statistiche" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-bar-chart"></i> {l s='Total per month'}

            <div class="panel-heading-action">

            </div>
        </div>

        {if count($telefonate['stats_mese'])}
        <table class="table">
            <thead>
                <tr>
                    <th><span class="title_box ">{l s='Month'}</span></th>
                    <th><span class="title_box ">{l s='Total inbound'}</span></th>
                    <th><span class="title_box ">{l s='Duration inbound'}</span></th>
                    <th><span class="title_box ">{l s='Total outbound'}</span></th>
                    <th><span class="title_box ">{l s='Duration outbound'}</span></th>
                </tr>
            </thead>
            <tbody>
                {foreach $telefonate['stats_mese'] AS $key => $stats_mese}
                <tr>
                    <td>{$stats_mese['mese_anno']}</td>
                    <td>{if $stats_mese['totale_ricevute'] == NULL}-{else}{$stats_mese['totale_ricevute']}{/if}</td>
                    <td>{if $stats_mese['durata_ricevute'] == NULL}-{else}{$stats_mese['durata_ricevute']}{/if}</td>
                    <td>{if $stats_mese['totale_inviate'] == NULL}-{else}{$stats_mese['totale_inviate']}{/if}</td>
                    <td>{if $stats_mese['durata_inviate'] == NULL}-{else}{$stats_mese['durata_inviate']}{/if}</td>
                </tr>
                {/foreach}
            </tbody>
            <tfoot>
                <tr>
                    <td>Tot.</td>
                    <td>{$telefonate['stats_totali']['totale_ricevute']}</td>
                    <td>{$telefonate['stats_totali']['durata_ricevute']}</td>
                    <td>{$telefonate['stats_totali']['totale_inviate']}</td>
                    <td>{$telefonate['stats_totali']['durata_inviate']}</td>
                </tr>
            </tfoot>
        </table>
        {else}
        <p class="text-muted text-center">
            {l s='No records found'}
        </p>
        {/if}
    </div>
</form>

<form class="form-horizontal col-lg-6" action="" method="post" enctype="multipart/form-data" name="stats_reparto" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-bar-chart"></i> {l s='Total by department (always)'}</span>

            <div class="panel-heading-action">
            
            </div>
        </div>

        {if count($telefonate['stats_mese_reparto'])}
        <table class="table">
            <thead>
                <tr>
                    <th><span class="title_box ">{l s='Department'}</span></th>
                    <th><span class="title_box ">{l s='Total inbound'}</span></th>
                    <th><span class="title_box ">{l s='Duration inbound'}</span></th>
                    <th><span class="title_box ">{l s='Total outbound'}</span></th>
                    <th><span class="title_box ">{l s='Duration outbound'}</span></th>
                </tr>
            </thead>
            <tbody>
                {foreach $telefonate['stats_mese_reparto'] AS $key => $stats_mese_reparto}
                <tr>
                    <td>{if $stats_mese_reparto['reparto'] == NULL}-{else}{$stats_mese_reparto['reparto']}{/if}</td>
                    <td>{if $stats_mese_reparto['totale_ricevute'] == NULL}-{else}{$stats_mese_reparto['totale_ricevute']}{/if}</td>
                    <td>{if $stats_mese_reparto['durata_ricevute'] == NULL}-{else}{$stats_mese_reparto['durata_ricevute']}{/if}</td>
                    <td>{if $stats_mese_reparto['totale_inviate'] == NULL}-{else}{$stats_mese_reparto['totale_inviate']}{/if}</td>
                    <td>{if $stats_mese_reparto['durata_inviate'] == NULL}-{else}{$stats_mese_reparto['durata_inviate']}{/if}</td>
                    
                </tr>
                {/foreach}
            </tbody>
            <tfoot>
                <tr>
                    <td>Tot.</td>
                    <td>{$telefonate['stats_totali_reparto']['totale_ricevute']}</td>
                    <td>{$telefonate['stats_totali_reparto']['durata_ricevute']}</td>
                    <td>{$telefonate['stats_totali_reparto']['totale_inviate']}</td>
                    <td>{$telefonate['stats_totali_reparto']['durata_inviate']}</td>
                </tr>
            </tfoot>
        </table>
        {else}
        <p class="text-muted text-center">
            {l s='No records found'}
        </p>
        {/if}						
        
    </div>
</form>

{else}
{$is_agente_activated = 1}
{/if}