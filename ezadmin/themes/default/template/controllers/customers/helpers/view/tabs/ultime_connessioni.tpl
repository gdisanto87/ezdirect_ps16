<form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" name="ultime_connessioni" novalidate>
    {if count($connections)}
    {* ROW relativa alle ultime connessioni *}
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-time"></i> {l s='Last connections'}
        </div>
        <table class="table">
            <thead>
            <tr>
                <th><span class="title_box">{l s='ID'}</span></th>
                <th><span class="title_box">{l s='Date'}</span></th>
                <th><span class="title_box">{l s='Pages viewed'}</span></th>
                <th><span class="title_box">{l s='Total time'}</span></th>
                <th><span class="title_box">{l s='Origin'}</span></th>
                <th><span class="title_box">{l s='IP Address'}</span></th>
            </tr>
            </thead>
            <tbody>
            {foreach $connections as $connection}
                <tr>
                    <td>{$connection['id_connections']}</td>
                    <td>{dateFormat date=$connection['date_add'] full=0}</td>
                    <td>{$connection['pages']}</td>
                    <td>{$connection['time']}</td>
                    <td>{$connection['http_referer']}</td>
                    <td>{$connection['ipaddress']}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
    {/if}
</form>