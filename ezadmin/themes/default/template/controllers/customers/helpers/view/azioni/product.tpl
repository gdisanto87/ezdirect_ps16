{* Per una lettura agevolata ( speranzoso di non vedere più html nel php ) *}

<div style='color:#000; z-index:99999999; width:970px !important; height:260px'>
    <h2 style='display:block; float:left'>'.$products['reference'].'</h2>
    <div style='margin-left:50px; float:left'> '.str_replace('|','',str_replace('"',"''",Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'product_lang WHERE id_lang = '.$context->language->id.' AND id_product = "'.$products['id_product'].'"'))).'<!-- <img src='https://www.ezdirect.it/img/'.($products['id_image'] > 0 ? 'p/'.$products['id_product'].'-'.$products['id_image'] : 'm/'.$products['id_manufacturer']).'-small.jpg' /> --> </div>
    <div style='float:left; margin-top:2px; margin-left:50px'>'.($context->employee->id_profile == 7 ? '' : 'Fornitore: <strong>'.$supplier.'</strong>').'</div>
    <div class='clear'></div>
    <div style='float:left'></div>
    <div style='float:left'>
        <strong>Prezzi</strong><br />
        <style type='text/css'>table.tooltip_table tr td, table.tooltip_table tr th { text-align:right }</style>
        <table class='tooltip_table table' style='width:670px; float:left'>
            <tr>
                <th>Listino</th>
                <th>'.($context->employee->id_profile == 7 ? '' : 'Acq.').'</th>
                <th>Vnd. Web</th>
                <th>Qta 1</th>
                <th>Qta2</th>
                <th>Qta 3</th>
                <th>Riv.</th>
                <th>Spec. Vnd.</th>
                <th>'.($context->employee->id_profile == 7 ? '' : 'Spec. Acq.').'</th>
            </tr>
            <tr>
                <td>'.Tools::displayPrice($products['listino']).'</td>
                <td>'.($context->employee->id_profile == 7 ? '' : ($context->employee->id == 1 || $context->employee->id == 2  || $context->employee->id == 3 || $context->employee->id == 6 || $context->employee->id == 15 || $context->employee->id == 7 ? Tools::displayPrice(($products['acquisto'] > 0 ? $products['acquisto'] : ($products['listino']*(100-$products['sc_acq_1'])/100)*((100-$products['sc_acq_2'])/100)*((100-$products['sc_acq_3'])/100))).'' : 'N.D.')).'</td>
                <td>'.Tools::displayPrice($products['vendita']).'</td>
                <td>'.($sc_qta_1 > 0 ? Tools::displayPrice($sc_qta_1) : '--').'</td>
                <td>'.($sc_qta_2 > 0 ? Tools::displayPrice($sc_qta_2) : '--').'</td>
                <td>'.($sc_qta_3> 0 ? Tools::displayPrice($sc_qta_3) : '--').'</td>
                <td>'.($sc_riv_1 > 0 ? Tools::displayPrice($sc_riv_1) : '--').'</td>
                <td>'.($spec_price > 0 ? '<font style='color:red'>'.Tools::displayPrice($spec_price).'</font>' : '--').'</td>
                <td>'.($context->employee->id_profile == 7 ? '' : ($spec_whole > 0 ? '<font style='color:blue'>'.Tools::displayPrice($spec_whole).'</font>' : '--')).'</td>
            </tr>
        </table>

        <br />

        <table class='table tooltip_table'  cellpadding='0' cellspacing='0' style='width:650px; border:1px solid #000; float:left; width:70%'>
            <tr>
                <th style='background: #ebebeb; text-align:center; border-bottom:1px solid #000' colspan='8'>Disponibilit&agrave;</th>
            </tr>        
            <tr>        
                <th style='border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right; width:150px'>Magazzino EZ</th>
                <th style='border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right'>'.$products['magazzino'].'</th>
                <th style='padding:0px; border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right; width:300px'>
                    <table style='width:100%; border-collapse:collapse; margin-top:0px; height:26px; margin-bottom:0px;border-bottom:0px'>
                        <tr>
                            <th style='border-right: 1px solid #bbb; border-bottom:0px solid #000; text-align:right;'>Netto<img class='img_control' src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Netto = Magazzino EZ - Qta ordinata dai clienti (sito)' title='Netto = Magazzino EZ - Qta ordinata dai clienti (sito)' /></th>         
                            <th style='border-right: 1px solid #bbb; border-bottom:0px solid #000;text-align:right'>'.($products['magazzino'] - $qta_ord_clienti).'</th>
                            <th style='border-right: 0px solid #bbb; border-bottom:0px solid #000;text-align:right'>Qta ordinata dai clienti (sito)</th>
                        </tr>
                    </table>
                </th>
                <th style='border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right'>'.$qta_ord_clienti.'</span>
                    <div class='tooltip_templates' style='display:none'>
                        <span id='qta_ord_clienti_tooltip'><table class='table' style='width:250px'><tr><th style='color:#000 !important'>Ordine</th><th style='color:#000 !important'>Stato</th><th style='color:#000 !important'>Quantit&agrave;</th></tr></table></span>
                    </div>
                </th>
                <th style='border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right; width:300px'>Qta impegnata ord. caricati su gestionale (eSolver)</th>
                <th style='border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right'>'.$products['qt_impegnato'].'</th>
            </tr>
            <tr>
                <th style='border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right'>Mag. Amazon</th>   
                <th style='border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right'>'.$products['amazon_qt'].'</th>
                <th style='border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right' >Ordinato al fornitore</th>
                <th style='border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right'>'.$products['qt_ordinato'].'</th>
                <th style='border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right' colspan='1'>Mag. EZ + Ordinato al fornitore - Impegnato sito</th>
                <th style='border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right' >'.($products['magazzino'] + $products['qt_ordinato'] - $qta_ord_clienti).'</th>
            </tr>
            '.($context->employee->id_profile == 7 ? '' : '
            <tr>
                <'.($id_supplier == 11 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>Allnet</'.($id_supplier == 11 ? 'th' : 'td').'>
                <'.($id_supplier == 11 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>'.$products['allnet'].'</'.($id_supplier == 11 ? 'th' : 'td').'>
                <'.($id_supplier == 11 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>In arrivo Allnet</'.($id_supplier == 11 ? 'th' : 'td').'>
                <'.($id_supplier == 11 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>'.$products['qt_arrivo'].'</'.($id_supplier == 11 ? 'th' : 'td').'>
                <'.($id_supplier == 11 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>Data arr. Allnet</'.($id_supplier == 11 ? 'th' : 'td').'>
                <'.($id_supplier == 11 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>'.(($from = $products['data_arrivo'] AND $from != '1946-01-01') ? ($from == '0000-00-00' || $from == '1970-01-01' ? 'ND' : date('d-m-y',strtotime($from))) : date('d-m-y')).'</'.($id_supplier == 11 ? 'th' : 'td').'>
            </tr>
            <tr>
                <'.($id_supplier == 38 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>Esprinet</'.($id_supplier == 38 ? 'th' : 'td').'>
                <'.($id_supplier == 38 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>'.$products['esprinet'].'</'.($id_supplier == 38 ? 'th' : 'td').'>
                <'.($id_supplier == 38 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>In arrivo Esprinet</'.($id_supplier == 38 ? 'th' : 'td').'>
                <'.($id_supplier == 38 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>'.$products['qt_arrivo_esprinet'].'</'.($id_supplier == 38 ? 'th' : 'td').'>
                <'.($id_supplier == 38 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>Data arr. Esprinet</'.($id_supplier == 38 ? 'th' : 'td').'>
                <'.($id_supplier == 38 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>'.(($from = $products['data_arrivo_esprinet'] AND $from != '1946-01-01') ? ($from == '0000-00-00' || $from == '1970-01-01' ? 'ND' : date('d-m-y',strtotime($from))) : date('d-m-y')).'</'.($id_supplier == 38 ? 'th' : 'td').'>
            </tr>
            <tr>
                <'.($id_supplier == 1450 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>Attiva</'.($id_supplier == 1450 ? 'th' : 'td').'>
                <'.($id_supplier == 1450 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>'.$products['attiva'].'</'.($id_supplier == 1450 ? 'th' : 'td').'>
                <'.($id_supplier == 1450 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>In arrivo Attiva</'.($id_supplier == 1450 ? 'th' : 'td').'>
                <'.($id_supplier == 1450 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>'.$products['qt_arrivo_attiva'].'</'.($id_supplier == 1450 ? 'th' : 'td').'>
                <'.($id_supplier == 1450 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>Data arr. Attiva</'.($id_supplier == 1450 ? 'th' : 'td').'>
                <'.($id_supplier == 1450 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>'.(($from = $products['data_arrivo_attiva'] AND $from != '1946-01-01') ? ($from == '0000-00-00' || $from == '1970-01-01' ? 'ND' : date('d-m-y',strtotime($from))) : date('d-m-y')).'</'.($id_supplier == 1450 ? 'th' : 'td').'>
            </tr>
            <tr>
                <'.($id_supplier == 42 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>Itancia</'.($id_supplier == 42 ? 'th' : 'td').'>
                <'.($id_supplier == 42 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>'.$products['itancia'].'</'.($id_supplier == 42 ? 'th' : 'td').'>
                <'.($id_supplier == 42 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>In arrivo Itancia</'.($id_supplier == 42 ? 'th' : 'td').'>
                <'.($id_supplier == 42 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>0</'.($id_supplier == 42 ? 'th' : 'td').'>
                <'.($id_supplier == 42 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>Data arr. Itancia:</'.($id_supplier == 42 ? 'th' : 'td').'>
                <'.($id_supplier == 42 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>ND</'.($id_supplier == 42 ? 'th' : 'td').'>
            </tr>
            <tr>
                <'.($id_supplier == 95 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>Intracom</'.($id_supplier == 95 ? 'th' : 'td').'>
                <'.($id_supplier == 95 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>'.$products['intracom'].'</'.($id_supplier == 95 ? 'th' : 'td').'>
                <'.($id_supplier == 95 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>In arrivo Intracom</'.($id_supplier == 95 ? 'th' : 'td').'>
                <'.($id_supplier == 95 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>'.$products['qt_arrivo_intracom'].'</'.($id_supplier == 95 ? 'th' : 'td').'>
                <'.($id_supplier == 95 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>Data arr. Intracom:</'.($id_supplier == 95 ? 'th' : 'td').'>
                <'.($id_supplier == 95 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>'.(($from_intracom = $products['data_arrivo_intracom'] AND $from_intracom != '1946-01-01') ? ($from_intracom == '0000-00-00' || $from_intracom == '1970-01-01' ? 'ND' : date('d-m-y',strtotime($from_intracom))) : date('d-m-y')).'</'.($id_supplier == 95 ? 'th' : 'td').'>
            </tr>
            <tr>
                <'.($id_supplier == 76 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>ASIT</'.($id_supplier == 76 ? 'th' : 'td').'>
                <'.($id_supplier == 76 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'>'.$products['asit'].'</'.($id_supplier == 76 ? 'th' : 'td').'>
                <'.($id_supplier == 76 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'></'.($id_supplier == 76 ? 'th' : 'td').'>
                <'.($id_supplier == 76 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'></'.($id_supplier == 76 ? 'th' : 'td').'>
                <'.($id_supplier == 76 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'></'.($id_supplier == 76 ? 'th' : 'td').'>
                <'.($id_supplier == 76 ? 'th' : 'td').' style='border-right: 1px solid #bbb; text-align:right'></'.($id_supplier == 76 ? 'th' : 'td').'>
            </tr>
            <tr>
                <td style='background: #ebebeb;border-right: 1px solid #000; border-top: 1px solid #000; text-align:right'><strong>Totale tutti i magazzini (escl. Amazon)</strong></td>
                <td style='background: #ebebeb;border-right: 1px solid #000; border-top: 1px solid #000; text-align:right'>'.$products['qt_tot'].'</td>
                <td style='background: #ebebeb;border-right: 1px solid #000; border-top: 1px solid #000; text-align:right; width:170px'><strong>Tot. magazzini + Ordinato a fornitore - Impegnato sito</strong></td>
                <td style='background: #ebebeb;text-align:right; border-right: 1px solid #000; border-top: 1px solid #000; '>'.(($products['qt_tot'])+$products['qt_ordinato']-$products['qt_impegnato']).'</td>
                <td style='background: #ebebeb;border-right: 1px solid #000; border-top: 1px solid #000; text-align:right; '><strong>'.$img_scorta.' Scorta minima</strong></td>
                <td style='background: #ebebeb;text-align:right; border-top: 1px solid #000; '>  '.$scorta_minima.'</td>
            </tr>').'
        </table>
        <table class='table'  cellpadding='0' cellspacing='0' style='float:left; border:1px solid #000; float:left; width:24%; margin-left:5%'>
            <tr>
                <th style='background: #ebebeb; text-align:center; border-bottom:1px solid #000' colspan='8'>Date qta in arrivo</th>
            </tr>
            <tr>
                <th style='border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right;'>ID Ord.</th>
                <th style='border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right;'>Qt.</th>
                <th style='border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right; width:150px'>Fornitore</th>
                <th style='border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right'>Data</th>
            </tr>
            '.$date_arrivo_ordinato_riga.'
        </table>
    </div>
    <div style='clear:both'></div>
</div>