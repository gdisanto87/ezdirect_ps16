{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

{assign var="link_gen_base" value="index.php?controller=AdminCustomers&amp;id_customer={$customer->id}&amp;viewcustomer&amp;"}
{assign var="link_gen_home" value="index.php?controller=AdminCustomers&amp;id_customer={$customer->id}&amp;viewcustomer"}
{assign var="token_base" value="&amp;token={getAdminToken tab='AdminCustomers'}"}
{*$is_agente = 1*}
{*$not_mio_agente = 1*}


{if $not_mio_agente == 1 OR $auth_carrelli_tipo == 0 }
<div id="container-customer">
	<form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" name="product" novalidate>
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-warning-sign"></i> {l s='Access denied'}
			</div>

			<h2 class="text-muted text-center">
				{l s='You do not have the necessary permissions to view this customer.'}
			</h2>
		</div>
	</form>
</div>
{else}
{*
	<h2>
		{if $smarty.get.viewaction == ""}
			Ok
		{else}
			no
		{/if}
	
	</h2>
*}
{if ! isset($smarty.get.azione)}

<div id="container-customer">

	{include file="./navbar.tpl"} {* Scelta tab *}

	<div class="row">
		
		{if isset($alert)}
		<div class="col-lg-12">
			<div class="row">
				<div class="alert alert-danger">{$alert}</div> {* Alert azioni in sospeso - note private *}
			</div>
		</div>
		{/if}

		{if $riassunto_on && !$not_mio_agente}
			<div class="row">
				{include file="./summary.tpl"} {* Riassunto *}
			</div>
		{/if}
		
		{include file="./pulsanti.tpl"} {* Riga di pulsanti *}
		
		<div class="col-lg-12">

			<div class="row">

{*CUSTOMER DETAILS*}
				{if ! isset($tab_name) || $tab_name == "anagrafica" || $tab_name == "customerdetails"}
				{include file="./tabs/anagrafica.tpl"}

{*ADMINISTRATION*}
				{elseif $tab_name == "administration"}
				{include file="./tabs/amministrazione.tpl"}

{*ADDRESSES*}
				{elseif $tab_name == "addresses"}
				{include file="./tabs/indirizzi.tpl"}

{*PEOPLE*}
				{elseif $tab_name == "people"}
				{include file="./tabs/persone.tpl"}

{*INVOICES*}
				{elseif $tab_name == "invoices"}
				{include file="./tabs/fatture.tpl"}

{*CREDIT NOTES*}
				{elseif $tab_name == "credit_notes"}
				{include file="./tabs/note_di_credito.tpl"}

{*DDT*}
				{elseif $tab_name == "ddt"}
				{include file="./tabs/ddt.tpl"}

{*CARTS*}
				{elseif $tab_name == "carts"}
				{include file="./tabs/carrelli.tpl"}

{*ORDERS*}
				{elseif $tab_name == "orders"}
				{include file="./tabs/ordini.tpl"}

{*VIEWED PRODUCT*}
				{elseif $tab_name == "viewedproduct"}
				{include file="./tabs/prodotti_visti.tpl"}

{*MESSAGES*}
				{elseif $tab_name == "messages"}
				{include file="./tabs/messaggi.tpl"}

{*LAST CONNECTIONS*}
				{elseif $tab_name == "lastconnections"}
				{include file="./tabs/ultime_connessioni.tpl"}

{*GROUPS*}
				{*}
				{elseif $tab_name == "groups"}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-group"></i>
							{l s='Groups'}
							<span class="badge squadrato">{count($groups)}</span>
							<a class="btn btn-default pull-right" href="{$current|escape:'html':'UTF-8'}&amp;updatecustomer&amp;id_customer={$customer->id|intval}&amp;token={$token|escape:'html':'UTF-8'}">
								<i class="icon-edit"></i> {l s='Edit'}
							</a>
						</div>
						{if $groups AND count($groups)}
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='ID'}</span></th>
									<th><span class="title_box ">{l s='Name'}</span></th>
								</tr>
							</thead>
							<tbody>
								{foreach $groups AS $key => $group}
								<tr onclick="document.location = '?tab=AdminGroups&amp;id_group={$group['id_group']|intval}&amp;viewgroup&amp;token={getAdminToken tab='AdminGroups'}'">
									<td>{$group['id_group']}</td>
									<td>
										<a href="?tab=AdminGroups&amp;id_group={$group['id_group']}&amp;viewgroup&amp;token={getAdminToken tab='AdminGroups'}">
											{$group['name']}
										</a>
									</td>
								</tr>
								{/foreach}
							</tbody>
						</table>
						{/if}
					</div>
				</form>
				{*}

{*VOUCHERS*}
				{*}
				{elseif $tab_name == "vouchers"}
				<form class="form-horizontal col-lg-12" action="index.php?controller=AdminCustomers&amp;token=27fed792fa4f93fe53ea37a62f11fd66&amp;updateproduct&amp;id_product=1" method="post" enctype="multipart/form-data" name="product" novalidate>
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-ticket"></i> {l s='Vouchers'} <span class="badge">{count($discounts)}</span>
						</div>
						{if count($discounts)}
							<table class="table">
								<thead>
									<tr>
										<th><span class="title_box">{l s='ID'}</span></th>
										<th><span class="title_box">{l s='Code'}</span></th>
										<th><span class="title_box">{l s='Name'}</span></th>
										<th><span class="title_box">{l s='Status'}</span></th>
										<th><span class="title_box">{l s='Qty available'}</span></th>
										<th><span class="title_box">{l s='Actions'}</span></th>
									</tr>
								</thead>
								<tbody>
							{foreach $discounts AS $key => $discount}
									<tr>
										<td>{$discount['id_cart_rule']}</td>
										<td>{$discount['code']}</td>
										<td>{$discount['name']}</td>
										<td>
											{if $discount['active']}
												<i class="icon-check"></i>
											{else}
												<i class="icon-remove"></i>
											{/if}
										</td>
										<td>{if $discount['quantity'] > 0}{$discount['quantity_for_user']|intval}{else}0{/if}</td>
										<td>
											<a href="?tab=AdminCartRules&amp;id_cart_rule={$discount['id_cart_rule']|intval}&amp;addcart_rule&amp;token={getAdminToken tab='AdminCartRules'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
												<i class="icon-pencil"></i>
											</a>
											<a href="?tab=AdminCartRules&amp;id_cart_rule={$discount['id_cart_rule']|intval}&amp;deletecart_rule&amp;token={getAdminToken tab='AdminCartRules'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
												<i class="icon-remove"></i>
											</a>
										</td>
									</tr>
								</tbody>
							{/foreach}
							</table>
						{else}
						<p class="text-muted text-center">
							{l s='%1$s %2$s has no discount vouchers' sprintf=[$customer->firstname, $customer->lastname]}
						</p>
						{/if}
					</div>
				</form>
				{*}

{* CALLS *}
				{elseif $tab_name == "calls"}
				{include file="./tabs/telefonate.tpl"}

{* MAIL *}
				{elseif $tab_name == "mail"}
				{include file="./tabs/mail.tpl"}

{* STATISTICHE *}
				{elseif $tab_name == "stats"}
				{include file="./tabs/statistiche.tpl"}

{* CLOUD *}
				{elseif $tab_name == "cloud"}
				{include file="./tabs/cloud.tpl"}

{* BDL ?*}
				{elseif $tab_name == "bdl"}
				{include file="./tabs/bdl.tpl"}

{* AZIONI CLIENTE *}
				{elseif $tab_name == "actions"}
				{include file="./tabs/azioni_cliente.tpl"}

{* TO DO *}
				{elseif $tab_name == "todo"}
				{include file="./tabs/todo.tpl"}


{* TICKETS *}
				{elseif $tab_name == "tickets"}
				{include file="./tabs/tickets.tpl"}


{* CONTRATTI *}
				{elseif $tab_name == "contracts"}
				{include file="./tabs/contratti.tpl"}


{* DOCUMENTI *}
				{elseif $tab_name == "documents"}
				{include file="./tabs/documenti.tpl"}

{*ERROR TAB NOT FOUND*}
				{elseif ! isset($is_agente_activated)}
				{include file="./tabs/not_found.tpl"}
				{/if}

				{if isset($is_agente_activated)}
				{include file="./tabs/access_denied.tpl"}
				{/if}

			</div>
		</div>

	






{********************************************************************************************************************************************************************************************************************************}
{********************************************************************************************************************************************************************************************************************************}
{********************************************************************************************************************************************************************************************************************************}
{********************************************************************************************************************************************************************************************************************************}
{********************************************************************************************************************************************************************************************************************************}
{********************************************************************************************************************************************************************************************************************************}


		{*
		{if count($emails)}
		<div class="col-lg-6">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-envelope"></i> {l s='Last emails'}
				</div>
				<table class="table">
					<thead>
					<tr>
						<th><span class="title_box">{l s='Date'}</span></th>
						<th><span class="title_box">{l s='Language'}</span></th>
						<th><span class="title_box">{l s='Subject'}</span></th>
						<th><span class="title_box">{l s='Template'}</span></th>
					</tr>
					</thead>
					<tbody>
					{foreach $emails as $email}
						<tr>
							<td>{dateFormat date=$email['date_add'] full=1}</td>
							<td>{$email['language']}</td>
							<td>{$email['subject']}</td>
							<td>{$email['template']}</td>
						</tr>
					{/foreach}
					</tbody>
				</table>
			</div>
		</div>
		{/if}
		*}

	</div>
</div>

{else}

	{* Scelta tab *}
	{if $navbar_on}
	{include file="./navbar.tpl"}
	{/if}

	{* Riga di pulsanti *}
	{include file="./pulsanti.tpl"}

	{* Azioni *}
	{include file="./azioni.tpl"}
	
{/if}

{/if}

{/block}
