<form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" name="not_found" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-warning-sign"></i> {l s='Error'}
        </div>

        <h2 class="text-muted text-center">
            {l s='Tab'} <a class="text-danger">{$tab_name}</a> {l s='not found'}
        </h2>
    </div>
</form>