{if $amministrazione['count_record'] == 0}
    <div class="col-lg-12">
        <div class="row">
            <div class="alert alert-danger">Il cliente non ha record nella tabella customer_amministrazione.</div>
        </div>
    </div>
{elseif $amministrazione['count_record'] > 1}
    <div class="col-lg-12">
        <div class="row">
            <div class="alert alert-danger">Il cliente ha più di un record nella tabella customer_amministrazione. Contatta il programmatore per risolvere il problema.</div>
        </div>
    </div>
{/if}

<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" name="amministrazione" novalidate name="{$current|escape:'html':'UTF-8'}">
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-legal"></i>
            {l s='Administration'}<span class="rosso" id="edit_title"></span>
            
            {if $amministrazione['count_record'] != 0}
            <div class="panel-heading-action">
                {assign var="back" value="&amp;token={getAdminToken tab='AdminCustomers'}"}

                <div class="bottoni-tab">
                    {if $is_agente != 1 OR true}
                    <button type="button" class="btn btn-default testo-bottoni" id="edit_btn" name="edit_btn" onclick="f_edit_btn()">
                        <i class="icon-edit"></i>
                        {l s='Edit'}
                    </button>
                    <div id="edit_section" style="display: none;">
                        <button type="button" class="btn btn-default testo-bottoni" id="reset_btn" name="reset_btn" onclick="f_reset_btn()">
                            <i class="icon-refresh"></i>
                            {l s='Reset'}
                        </button>
                        <button type="submit" class="btn btn-default testo-bottoni" id="submitAmministrazione" name="submitAmministrazione">
                            <i class="icon-save"></i>
                            {l s='Save'}
                        </button>
                    </div>
                    {/if}
                </div>
            </div>
            {/if}

        </div>

        {if $amministrazione['count_record'] == 0}

            {* Crea un nuovo record di amministrazione *}
            <div class="form-row">
                <div class="row">
                    <button type="submit" class="btn btn-default testo-bottoni" id="creaRecordAmm" name="creaRecordAmm">
                        <i class="icon-add"></i>
                        {l s='Create new administration record'}
                    </button>
                </div>
            </div>

        {else}

            <input type="hidden" id="edit_adm" value="0">

            <div class="form-row">

                <div class="row">
                    
                    <div class="form-group col-md-2">
                        <label for="annual_revenue">{l s='Annual revenue'}</label>
                        <p class="form-control-static">
                            {if isset($amministrazione['fatturato_annuo'])}{$amministrazione['fatturato_annuo']}{else}{l s='Unknown'}{/if}
                        </p>
                    </div>
                    
                    <div class="form-group col-md-2">
                        <label for="total_revenue">{l s='Total revenue'}</label>
                        <p class="form-control-static">
                            {if isset($amministrazione['fatturato_totale'])}{$amministrazione['fatturato_totale']}{else}{l s='Unknown'}{/if}
                        </p>
                    </div>

                    {if $customer->id_default_group == 3 or $customer->id_default_group == 15 or $customer->id_default_group == 22}
                    <div class="form-group col-md-4">
                        <div class="panel">
                            <div class="panel-heading">
                                {l s='Credit request requirements:'}
                                {if ! isset($amministrazione['fido_ok'])}
                                    <span class="label label-default squadrato"><i class="icon-question"></i> {l s='Unknown'}</span>
                                {elseif $amministrazione['fido_ok'] == 1}
                                    <span class="label label-success squadrato"><i class="icon-check"></i> {l s='Yes'}</span>
                                {elseif $amministrazione['fido_ok'] == 0}
                                    <span class="label label-danger squadrato"><i class="icon-remove"></i> {l s='No'}</span>
                                {/if}
                                
                                <span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="{l s='at least € 1500 of orders or 1 order of € 1000 net in the last 3 months'}">
                                    <i class="icon-question-sign"></i>
                                </span>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>{l s='Net last 3 months'}</label>
                                    {if isset($amministrazione['acquisti_tre_mesi'])}
                                    {$amministrazione['acquisti_tre_mesi']}
                                    {else}
                                    <p class="form-control-static">{l s='Unknown'}</p>
                                    {/if}
                                </div>
                                

                                <div class="form-group col-md-6">
                                    <label for="total_revenue">{l s='Order > 1000 Eur ?'}</label>
                                    {if isset($amministrazione['ordine_mille'])}
                                        {if $amministrazione['ordine_mille']}
                                            <span class="label label-primary squadrato"><i class="icon-check"></i> {*l s='Yes'*}</span>
                                        {else}
                                            <span class="label label-danger squadrato"><i class="icon-remove"></i> {*l s='No'*}</span>
                                        {/if}
                                    {else}
                                        <span class="label label-default squadrato"><i class="icon-question"></i> {*l s='Unknown'*}</span>
                                    {/if}
                                </div>
                            </div>
                            <br />
                        </div>
                    </div>
                    {/if}
                    
                </div>

                <div class="row">

                    {* Tipo soggetto *}
                    <div class="form-group col-md-2">
                        <label for="subject_type">{l s='Subject type'}</label>
                        <p class="form-control-static">
                        {* Da finire funzione *}
                            {* assign var=list_tipo_soggetto value=[["0","-- Seleziona --"],["1","Società di capitali"],["2","Società di persone"],["3","Persona fisica"],["4","Altro"]] *}
                            <select id="tipo_soggetto" name="tipo_soggetto" class="form-control textbox-fix lettura" disabled>
                                <option value="0"{if $amministrazione['tipo_soggetto'] == 0} selected{/if}>-- Seleziona --</option>
                                <option value="1"{if $amministrazione['tipo_soggetto'] == 1} selected{/if}>Società di capitali</option>
                                <option value="2"{if $amministrazione['tipo_soggetto'] == 2} selected{/if}>Società di persone</option>
                                <option value="3"{if $amministrazione['tipo_soggetto'] == 3} selected{/if}>Persona fisica</option>
                                <option value="4"{if $amministrazione['tipo_soggetto'] == 4} selected{/if}>Altro</option>
                            </select>
                        </p>
                    </div>

                    {* Codice fornitore *}
                    {if isset($amministrazione['codice_fornitore'])}
                    <div class="form-group col-md-2">
                        <label for="supp_code">{l s='Supplier code'}</label>
                        <p class="form-control-static">
                            <input class="textbox-fix lettura" type="text" name="codice_fornitore" id="codice_fornitore" value="{if isset($amministrazione['codice_fornitore'])}{$amministrazione['codice_fornitore']}{else}{l s='Unknown'}{/if}" disabled>
                        </p>
                    </div>
                    {/if}

                    {* Pagamento *}
                    <div class="form-group col-md-2">
                        <label for="payment">{l s='Payment'}</label>
                        <p class="form-control-static">
                            <select id="pagamento" name="pagamento" class="form-control textbox-fix lettura" disabled>
                                <option value=""                                    {if $amministrazione['pagamento'] == ""} selected{/if}>-- Seleziona --</option>
                                <option value="Bonifico anticipato"                 {if $amministrazione['pagamento'] == "Bonifico anticipato"} selected{/if}>Bonifico anticipato</option>
                                <option value="Carta di credito (gestpay)"          {if $amministrazione['pagamento'] == "Carta di credito (gestpay)"} selected{/if}>Carta di credito (gestpay)</option>
                                <option value="Carta Amazon"                        {if $amministrazione['pagamento'] == "Carta Amazon"} selected{/if}>Carta Amazon</option>
                                <option value="Carta ePrice"                        {if $amministrazione['pagamento'] == "Carta ePrice"} selected{/if}>Carta ePrice</option>
                                <option value="PayPal"                              {if $amministrazione['pagamento'] == "PayPal"} selected{/if}>PayPal</option>
                                <option value="Contrassegno"                        {if $amministrazione['pagamento'] == "Contrassegno"} selected{/if}>Contrassegno</option>
                                <option value="Bonifico 30 gg. fine mese"           {if $amministrazione['pagamento'] == "Bonifico 30 gg. fine mese"} selected{/if}>Bonifico 30 gg. fine mese</option>
                                <option value="Bonifico 60 gg. fine mese"           {if $amministrazione['pagamento'] == "Bonifico 60 gg. fine mese"} selected{/if}>Bonifico 60 gg. fine mese</option>
                                <option value="Bonifico 90 gg. fine mese"           {if $amministrazione['pagamento'] == "Bonifico 90 gg. fine mese"} selected{/if}>Bonifico 90 gg. fine mese</option>
                                <option value="Bonifico 30 gg. 15 mese successivo"  {if $amministrazione['pagamento'] == "Bonifico 30 gg. 15 mese successivo"} selected{/if}>Bonifico 30 gg. 15 mese successivo</option>
                                <option value="R.B. 30 GG. D.F. F.M."               {if $amministrazione['pagamento'] == "R.B. 30 GG. D.F. F.M."} selected{/if}>R.B. 30 GG. D.F. F.M.</option>
                                <option value="R.B. 60 GG. D.F. F.M."               {if $amministrazione['pagamento'] == "R.B. 60 GG. D.F. F.M."} selected{/if}>R.B. 60 GG. D.F. F.M.</option>
                                <option value="R.B. 90 GG. D.F. F.M."               {if $amministrazione['pagamento'] == "R.B. 90 GG. D.F. F.M."} selected{/if}>R.B. 90 GG. D.F. F.M.</option>
                                <option value="R.B. 30 GG. 5 mese successivo"       {if $amministrazione['pagamento'] == "R.B. 30 GG. 5 mese successivo"} selected{/if}>R.B. 30 GG. 5 mese successivo</option>
                                <option value="R.B. 30 GG. 10 mese successivo"      {if $amministrazione['pagamento'] == "R.B. 30 GG. 10 mese successivo"} selected{/if}>R.B. 30 GG. 10 mese successivo</option>
                                <option value="R.B. 60 GG. 5 mese successivo"       {if $amministrazione['pagamento'] == "R.B. 60 GG. 5 mese successivo"} selected{/if}>R.B. 60 GG. 5 mese successivo</option>
                                <option value="R.B. 60 GG. 10 mese successivo"      {if $amministrazione['pagamento'] == "R.B. 60 GG. 10 mese successivo"} selected{/if}>R.B. 60 GG. 10 mese successivo</option>
                                <option value="Renting"                             {if $amministrazione['pagamento'] == "Renting"} selected{/if}>Renting</option>
                            </select>
                        </p>
                    </div>

                    {* Fido *}
                    <div class="form-group col-md-1">
                        <label for="trust">{l s='Trust'}</label>
                        <p class="form-control-static">
                            <div class="input-group">
                                <span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
                                <input class="textbox-fix lettura" type="text" name="fido" id="fido" value="{if isset($amministrazione['fido'])}{$amministrazione['fido']}{else}{l s='Unknown'}{/if}" disabled>
                            </div>
                        </p>
                    </div>

                    {* Fido residuo *}
                    <div class="form-group col-md-2">
                        <label for="residual_trust">{l s='Residual trust'}</label>
                        <p class="form-control-static">
                            <div class="input-group">
                                <span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
                                <input class="textbox-fix lettura" type="text" name="fido_residuo" id="fido_residuo" value="{if isset($amministrazione['fido_residuo'])}{$amministrazione['fido_residuo']}{else}{l s='Unknown'}{/if}" disabled>
                            </div>
                        </p>
                    </div>

                    {* Assicurazione credito *}
                    {*
                    Si = tutto il fido è assicurato con Coface
                    No o vuoto = il fido non è assicurato
                    (N) = il fido è assicurato per N euro
                    Non affidabile = richiesta non accettata da Coface
                    *}
                    <div class="form-group col-md-2">
                        <label for="credit_insurance">{l s='Credit insurance'}
                            <span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="Si = tutto il fido è assicurato con Coface<br />No o vuoto = il fido non è assicurato<br />(N) = il fido è assicurato per N euro<br />Non affidabile = richiesta non accettata da Coface">
                                <i class="icon-question-sign"></i>
                            </span>
                        </label>
                        <p class="form-control-static">                            
                            <select id="assicurazione_credito" name="assicurato" class="form-control textbox-fix lettura" disabled>
                                <option value=""                {if $amministrazione['assicurato'] == ""} selected{/if}>-- Seleziona --</option>
                                <option value="Si"              {if $amministrazione['assicurato'] == "Si"} selected{/if}>Si</option>
                                <option value="No"              {if $amministrazione['assicurato'] == "No"} selected{/if}>No</option>
                                <option value="Non affidabile"  {if $amministrazione['assicurato'] == "Non affidabile"} selected{/if}>Non affidabile</option>
                                <option value="Revocato"        {if $amministrazione['assicurato'] == "Revocato"} selected{/if}>Revocato</option>
                            </select>
                        </p>
                    </div>

                </div>

                <div class="row">
                    {* Data affidamento *}
                    <div class="form-group col-md-2">
                        <label for="data_fido">{l s='Assignment date'}</label>
                        <p class="form-control-static">
                            <input class="textbox-fix lettura" type="date" name="data_fido" id="data_fido" value="{if isset($amministrazione['data_fido'])}{$amministrazione['data_fido']|date_format:"%Y-%m-%d"}{else}{l s='Unknown'}{/if}" disabled>
                        </p>
                    </div>

                    {* Data rinnovo *}
                    <div class="form-group col-md-2">
                        <label for="data_rinnovo">{l s='Renewal date'}</label>
                        <p class="form-control-static">
                            <input class="textbox-fix lettura" type="date" name="data_rinnovo" id="data_rinnovo" value="{if isset($amministrazione['data_rinnovo'])}{$amministrazione['data_rinnovo']|date_format:"%Y-%m-%d"}{else}{l s='Unknown'}{/if}" disabled>
                        </p>
                    </div>

                    {* Data revoca *}
                    <div class="form-group col-md-2">
                        <label for="data_revoca">{l s='Revocation date'}</label>
                        <p class="form-control-static">
                            <input class="textbox-fix lettura" type="date" name="data_revoca" id="data_revoca" value="{if isset($amministrazione['data_revoca'])}{$amministrazione['data_revoca']|date_format:"%Y-%m-%d"}{else}{l s='Unknown'}{/if}" disabled>
                        </p>
                    </div>

                    {* Motivo revoca / Non affidato *}
                    <div class="form-group col-md-3">
                        <label for="reason_revocation">{l s='Reason for revocation / Not entrusted'}</label>
                        <p class="form-control-static">
                            <input class="textbox-fix lettura" type="text" name="motivo_revoca" id="motivo_revoca" value="{if isset($amministrazione['motivo_revoca'])}{$amministrazione['motivo_revoca']}{else}{l s='Unknown'}{/if}" disabled>
                        </p>
                    </div>

                    {* Non addebitare commissioni su pagamento differito *}
                    <div class="form-group col-md-3">
                        <label for="no_addebito_commissioni">{l s='Do not charge fees on deferred payment'}</label>
                        <div id="addebito_commissioni_let">
                            <p class="form-control-static">
                                {if isset($amministrazione['no_addebito_commissioni'])}
                                    {if $amministrazione['no_addebito_commissioni']}
                                        <span class="label label-primary squadrato" id="span_no_addebito_commissioni"><i id="icon_no_addebito_commissioni" class="icon-check"></i> {*l s='Yes'*}</span>
                                        <input type="hidden" id="no_addebito_commissioni_vis" value="1">
                                    {else}
                                        <span class="label label-warning squadrato" id="span_no_addebito_commissioni"><i id="icon_no_addebito_commissioni" class="icon-remove"></i> {*l s='No'*}</span>
                                        <input type="hidden" id="no_addebito_commissioni_vis" value="0">
                                    {/if}
                                {else}
                                    <span class="label label-default squadrato"><i class="icon-question"></i> {*l s='Unknown'*}</span>
                                {/if}
                            </p>
                        </div>
                        <div id="addebito_commissioni_mod" class="nascosto">
                            <p class="form-control-static">
                                <input type="checkbox" class="form-control" name="no_addebito_commissioni" value="1" {if $amministrazione['no_addebito_commissioni'] } checked{/if}>
                            </p>
                        </div>
                    </div>

                </div>

                <div class="row">
                    {* Trasporto a mezzo *}
                    <div class="form-group col-md-2">
                        <label for="trasporto_a_mezzo">{l s='Transport by'}</label>
                        <p class="form-control-static">
                            <select id="trasporto_a_mezzo" name="trasporto_a_mezzo" class="form-control textbox-fix lettura" disabled>
                                <option value="0" {if $amministrazione['trasporto_a_mezzo'] == "0"} selected{/if}>-- Seleziona --</option>
                                <option value="1" {if $amministrazione['trasporto_a_mezzo'] == "1"} selected{/if}>Mittente</option>
                                <option value="2" {if $amministrazione['trasporto_a_mezzo'] == "2"} selected{/if}>Destinatario</option>
                                <option value="3" {if $amministrazione['trasporto_a_mezzo'] == "3"} selected{/if}>Vettore</option>
                                <option value="4" {if $amministrazione['trasporto_a_mezzo'] == "4"} selected{/if}>Ritiro in sede</option>
                            </select>
                        </p>
                    </div>

                    {* Consegna (porto) *}
                    <div class="form-group col-md-2">
                        <label for="consegna">{l s='Delivery (port)'}</label>
                        <p class="form-control-static">
                            <select id="consegna" name="consegna" class="form-control textbox-fix lettura" disabled>
                                <option value="0" {if $amministrazione['consegna'] == "0"} selected{/if}>-- Seleziona --</option>
                                <option value="1" {if $amministrazione['consegna'] == "1"} selected{/if}>Franco</option>
                                <option value="2" {if $amministrazione['consegna'] == "2"} selected{/if}>Assegnato (corriere del cliente)</option>
                                <option value="3" {if $amministrazione['consegna'] == "3"} selected{/if}>Franco Add. in Fattura (ns corriere)</option>
                            </select>
                        </p>
                    </div>

                    {* Vettore *}
                    <div class="form-group col-md-4">
                        <label for="vettore">{l s='Vector'}</label>
                        <p class="form-control-static">
                            <select id="vettore" name="vettore" class="form-control textbox-fix lettura" disabled>
                                <option value="0"    {if $amministrazione['vettore'] == "0"} selected{/if}>-- Seleziona --</option>
                                <option value="1043" {if $amministrazione['vettore'] == "1043"} selected{/if}>GLS National Express s.r.l.</option>
                                <option value="1062" {if $amministrazione['vettore'] == "1062"} selected{/if}>UPS Italia s.r.l.</option>
                                <option value="1245" {if $amministrazione['vettore'] == "1245"} selected{/if}>TNT Global Express spa</option>
                                <option value="1269" {if $amministrazione['vettore'] == "1269"} selected{/if}>SDA Trasporti</option>
                                <option value="1271" {if $amministrazione['vettore'] == "1271"} selected{/if}>DHL Trasporti</option>
                                <option value="1752" {if $amministrazione['vettore'] == "1752"} selected{/if}>Ascoli Trasporti</option>
                                <option value="1753" {if $amministrazione['vettore'] == "1753"} selected{/if}>Poste Trasporti</option>
                            </select>
                        </p>
                    </div>

                    {* ID corriere cliente *}
                    <div class="form-group col-md-4">
                        <label for="customer_courier_id">{l s='Customer courier ID'}</label>
                        <p class="form-control-static">
                            <input class="textbox-fix lettura" type="text" name="id_corriere" id="id_corriere" value="{if isset($amministrazione['id_corriere'])}{$amministrazione['id_corriere']}{else}{l s='Unknown'}{/if}" disabled>
                        </p>
                    </div>
                </div>

                <div class="row">
                    {* Trasporto assicurato *}
                    <div class="form-group col-md-2">
                        <label for="trasporto_assicurato">{l s='Insured transport'}</label>
                        <div id="trasporto_assicurato_let">
                            <p class="form-control-static">
                                {if isset($amministrazione['trasporto_assicurato'])}
                                    {if $amministrazione['trasporto_assicurato']}
                                        <span class="label label-primary squadrato"><i class="icon-check"></i> {*l s='Yes'*}</span>
                                    {else}
                                        <span class="label label-warning squadrato"><i class="icon-remove"></i> {*l s='No'*}</span>
                                    {/if}
                                {else}
                                    <span class="label label-default squadrato"><i class="icon-question"></i> {*l s='Unknown'*}</span>
                                {/if}
                                <input type="hidden" id="trasporto_assicurato" value="0">
                            </p>
                        </div>
                        <div id="trasporto_assicurato_mod" class="nascosto">
                            <p class="form-control-static">
                                <input type="checkbox" class="form-control" name="trasporto_assicurato" value="1" {if $amministrazione['trasporto_assicurato'] } checked{/if}>
                            </p>
                        </div>
                    </div>

                    {* Trasporto gratis da *}
                    <div class="form-group col-md-2">
                        <label for="free_transp_from">{l s='Free transport from'}</label>
                        <p class="form-control-static">
                            <div class="input-group">
                                <span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
                                <input class="textbox-fix lettura" type="text" name="trasporto_gratis_da" id="trasporto_gratis_da" value="{if isset($amministrazione['trasporto_gratis_da'])}{$amministrazione['trasporto_gratis_da']}{else}{l s='Unknown'}{/if}" disabled>
                            </div>
                        </p>
                    </div>

                    {* Fermo deposito *}
                    <div class="form-group col-md-4">
                        <label for="stat_storage">{l s='Stationary storage'}</label>
                        <p class="form-control-static">
                            <input class="textbox-fix lettura" type="text" name="fermo_deposito" id="fermo_deposito" value="{if isset($amministrazione['fermo_deposito'])}{$amministrazione['fermo_deposito']}{else}{l s='Unknown'}{/if}" disabled>
                        </p>
                    </div>

                    {* Note consegna *}
                    <div class="form-group col-md-4">
                        <label for="delivery_notes">{l s='Delivery notes'}</label>
                        <p class="form-control-static">
                            <input class="textbox-fix lettura" type="text" name="note_consegna" id="note_consegna" value="{if isset($amministrazione['note_consegna'])}{$amministrazione['note_consegna']}{else}{l s='Unknown'}{/if}" disabled>
                        </p>
                    </div>
                </div>

                <hr />

                <div class="row">
                    {* IBAN *}
                    <div class="form-group col-md-4">
                        <label for="iban">{l s='IBAN'}</label>
                        <p class="form-control-static">
                            <input class="textbox-fix lettura" type="text" name="iban" id="iban" value="{if isset($amministrazione['iban'])}{$amministrazione['iban']}{else}{l s='Unknown'}{/if}" disabled>
                        </p>
                    </div>

                    {* Swift *}
                    <div class="form-group col-md-4">
                        <label for="swift">{l s='Swift'}</label>
                        <p class="form-control-static">
                            <input class="textbox-fix lettura" type="text" name="swift" id="swift" value="{if isset($amministrazione['swift'])}{$amministrazione['swift']}{else}{l s='Unknown'}{/if}" disabled>
                        </p>
                    </div>

                    {* PayPal *}
                    <div class="form-group col-md-4">
                        <label for="paypal_email">{l s='PayPal'}</label>
                        <p class="form-control-static">
                            <input class="textbox-fix lettura" type="text" name="paypal" id="paypal" value="{if isset($amministrazione['paypal'])}{$amministrazione['paypal']}{else}{l s='Unknown'}{/if}" disabled>
                        </p>
                    </div>
                </div>

                <div class="row">
                    {* IVA agevolata *}
                    <div class="form-group col-md-4">
                        <label for="reduced_vat">{l s='Reduced VAT'}</label>
                        <p class="form-control-static">
                            <div class="input-group">
                                <span class="input-group-addon">%</span>
                                <input class="textbox-fix lettura" type="text" name="iva_agevolata" id="iva_agevolata" value="{if isset($amministrazione['iva_agevolata'])}{$amministrazione['iva_agevolata']}{else}{l s='Unknown'}{/if}" disabled>
                            </div>
                        </p>
                    </div>

                    {* Esportatore abituale *}
                    <div class="form-group col-md-4">
                        <label for="esportatore_abituale">{l s='Regular exporter'}</label>
                        <div id="esportatore_abituale_let">
                            <p class="form-control-static">
                                {if isset($amministrazione['esportatore_abituale'])}
                                    {if $amministrazione['esportatore_abituale']}
                                        <span class="label label-primary squadrato"><i class="icon-check"></i> {*l s='Yes'*}</span>
                                    {else}
                                        <span class="label label-primary squadrato"><i class="icon-remove"></i> {*l s='No'*}</span>
                                    {/if}
                                {else}
                                    <span class="label label-default squadrato"><i class="icon-question"></i> {*l s='Unknown'*}</span>
                                {/if}
                            </p>
                        </div>
                        <div id="esportatore_abituale_mod" class="nascosto">
                            <p class="form-control-static">
                                <input type="checkbox" class="form-control" name="esportatore_abituale" value="1" {if $amministrazione['esportatore_abituale'] } checked{/if}>
                            </p>
                        </div>
                    </div>

                </div>


                <div class="row">
                    {* Sconto extra *}
                    <div class="form-group col-md-4">
                        <label for="extra_discount">{l s='Extra discount'}</label>
                        <p class="form-control-static">
                            <div class="input-group">
                                <span class="input-group-addon">%</span>
                                <input class="textbox-fix lettura" type="text" name="sconto_extra" id="sconto_extra" value="{if isset($amministrazione['sconto_extra'])}{$amministrazione['sconto_extra']}{else}{l s='Unknown'}{/if}" disabled>
                            </div>
                        </p>
                    </div>

                    {* Per ordini da *}
                    <div class="form-group col-md-4">
                        <label for="for_order_from">{l s='For orders from'}</label>
                        <p class="form-control-static">
                            <div class="input-group">
                                <span class="input-group-addon">{$currency->prefix}{$currency->suffix}</span>
                                <input class="textbox-fix lettura" type="text" name="per_ordini_da" id="per_ordini_da" value="{if isset($amministrazione['per_ordini_da'])}{$amministrazione['per_ordini_da']}{else}{l s='Unknown'}{/if}" disabled>
                            </div>
                        </p>
                    </div>

                    {* Fino a (data) *}
                    <div class="form-group col-md-4">
                        <label for="fino_a">{l s='Until (date)'}</label>
                        <p class="form-control-static">
                            <input class="textbox-fix lettura" type="datetime" name="fino_a" id="fino_a" value="{if isset($amministrazione['fino_a'])}{$amministrazione['fino_a']}{else}{l s='Unknown'}{/if}" disabled>
                        </p>
                    </div>
                </div>

                <div class="row">
                    {* Rebate *}
                    <div class="form-group col-md-4">
                        <label for="rebate">{l s='Rebate'}</label>
                        <p class="form-control-static">
                            <div class="input-group">
                                <span class="input-group-addon">%</span>
                                <input class="textbox-fix lettura" type="text" name="rebate" id="rebate" value="{if isset($amministrazione['rebate'])}{$amministrazione['rebate']}{else}{l s='Unknown'}{/if}" disabled>
                            </div>
                        </p>
                    </div>

                    {* Fatturato annuo ( duplicato ) *}
                    {*<div class="form-group col-md-4">
                        <label for="annual_revenue">{l s='Annual revenue'}</label>
                        <p class="form-control-static">
                            {if isset($amministrazione['fatturato_annuo'])}{$amministrazione['fatturato_annuo']}{else}{l s='Unknown'}{/if}
                        </p>
                    </div>*}

                    {* Blacklist *}
                    <div class="form-group col-md-4">
                        <label for="blacklist">{l s='Blacklist'}</label>
                        <div id="blacklist_let">
                            <p class="form-control-static">
                                {if $amministrazione['blacklist']}
                                    <span class="label label-danger squadrato"><i class="icon-check"></i> {*l s='Yes'*}</span>
                                {else}
                                    <span class="label label-success squadrato"><i class="icon-remove"></i> {*l s='No'*}</span>
                                {/if}
                            </p>
                        </div>
                        <div id="blacklist_mod" class="nascosto">
                            <p class="form-control-static">
                                <input type="checkbox" class="form-control" name="blacklist" value="1" {if $amministrazione['blacklist']} checked{/if}>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    {* Agente *}
                    <div class="form-group col-md-4">
                        <label for="agent">{l s='Agent'}</label>
                        <p class="form-control-static">
                            <select id="agente" name="agente" class="form-control textbox-fix lettura" disabled>
                                <option value="0"{if $amministrazione['agente_value'] == 0} selected="selected"{/if}>{l s='Select Agent'}</option>
                                {if count($amministrazione['agenti'])}
                                    {foreach $amministrazione['agenti'] AS $key => $impiegato}
                                    <option value="{$impiegato['id']}"{if $impiegato['id'] == $amministrazione['agente_value']} selected="selected"{/if}>{$impiegato['id']} - {$impiegato['name']}</option>
                                    {/foreach}
                                {/if}
                            </select>
                        </p>
                    </div>

                    {* Tecnico *}
                    <div class="form-group col-md-4">
                        <label for="technician">{l s='Technician'}</label>
                        <p class="form-control-static">
                            <select id="tecnico" name="tecnico" class="form-control textbox-fix lettura" disabled>
                                <option value="0"{if $amministrazione['tecnico_value'] == 0} selected="selected"{/if}>{l s='Select Technician'}</option>
                                {if count($amministrazione['tecnici'])}
                                    {foreach $amministrazione['tecnici'] AS $key => $tecnico}
                                    <option value="{$tecnico['id']}"{if $tecnico['id'] == $amministrazione['tecnico_value']} selected="selected"{/if}>{$tecnico['id']} - {$tecnico['name']}</option>
                                    {/foreach}
                                {/if}
                            </select>
                        </p>
                    </div>

                    {* Installatore *}
                    {*<div class="form-group col-md-4">
                        <label for="installer">{l s='Installer'}</label>
                        <p class="form-control-static">
                            <input class="textbox-fix lettura" type="text" name="installatore" id="installatore" value="{if isset($amministrazione['installatore'])}{$amministrazione['installatore']}{else}{l s='Unknown'}{/if}" disabled>
                        </p>
                    </div>*}
                </div>
            </div>

        {/if}
    </div>
</form>

{* Javascript della pagina *}
{include file="./js/amministrazione_js.tpl"}
