{* CORREGGERE: aggiungere i tax_regime mancanti e controllare il resto; fare abilita modifiche come in amministrazione *}
{assign var=list_tax_regime value=[["0","Base (Italy: VAT 22%)"], ["2","VAT 10%"], ["3","VAT 4%"], ["1","Exempt"], ["4","Split Payment with VAT 22%"], ["10","MGE1 Germania Iva 19%"]]}
{assign var=list_sector value=["-- Seleziona --", "Call Center", "Commercio", "Hotel", "Industria", "Meccanica", "Ristorazione", "Servizi", "Telecomunicazioni", "Turismo", "Altro"]}
{assign var=list_channel value=["-- Seleziona --", "Google", "Amazon", "DEM", "ePrice", "Facebook", "Inserzione sul giornale", "Linkedin", "Link diretto", "Link su altro sito", "Altro motore di ricerca (Bing, Yahoo, Duckduckgo ecc.", "Paid (Adwords, Inserzioni Facebook ecc.)", "Passaparola", "Trovaprezzi"]}
{assign var=list_employees_number value=["Da 1 a 10", "Da 11 a 50", "Più di 50"]}
{* List di profile -> è la lista dei gruppi ed è contenuta in "gruppi" *}

<script>
    /*
    $(document).ready(function () {
        
        $("#compleanno").datepicker();
    });

    $(".compl").each(function() {
        $(this).datepicker('setDate', $(this).val());
    });
    */
</script>

<script src="./js/cf.js"></script>

{* Stile della pagina *}
{include file="./stile/anagrafica_stile.tpl"}


<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" name="anagrafica" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-user"></i>
            {* Verifica se il cliente ha impostato un nome azienda e regola l'interfaccia di conseguenza *}
            {l s='Customer details: '}
            {if isset($customer->company)}
                {$customer->firstname}
                {$customer->lastname}
            {else}
                {$customer->company}
                ({$customer->firstname}
                {$customer->lastname})
            {/if}
            [{$customer->id|string_format:"%06d"}]
            -
            <a href="mailto:{$customer->email}"><i class="icon-envelope"></i>
                {$customer->email}
            </a>
            <span class="rosso" id="edit_title"></span>
            <div class="panel-heading-action">
                <div class="bottoni-tab">
                    {if $is_agente != 1 OR true}
                    <a id="edit_btn" class="btn btn-default" onclick="f_edit_btn()">
                        <i class="icon-edit"></i>
                        {l s='Edit'}
                    </a>
                    <div id="edit_section" style="display: none;">
                        <a id="reset_btn" class="btn btn-default" onclick="f_reset_btn()">
                            <i class="icon-refresh"></i>
                            {l s='Reset'}
                        </a>
                        <a id="save_btn" class="btn btn-default" onclick="">
                            <i class="icon-save"></i>
                            {l s='Save'}
                        </a>
                    </div>
                    {/if}
                </div>
            </div>
        </div>

        <input type="hidden" id="edit_adm" value="0">

        <div class="form-row">
            <div class="row">
                
                <div class="form-group col-md-4">
                    <label for="company">{l s='Company'}</label>
                    <p class="form-control-static">
                        {*}{if isset($customer->company)}{$customer->company}{else}{l s='Unknown'}{/if}{*}
                        <input class="textbox-fix lettura" type="text" size="33" name="nome_azienda" id="nome_azienda" value="{if isset($customer->company)}{$customer->company}{/if}" disabled>
                    </p>
                </div>
                
                <div class="form-group col-md-4">
                    <label for="firstname">{l s='Firstname'}</label>
                    <p class="form-control-static">
                        {*}{if isset($customer->firstname)}{$customer->firstname}{else}{l s='Unknown'}{/if}{*}
                        <input class="textbox-fix lettura" type="text" size="33" name="nome" id="nome" value="{if isset($customer->firstname)}{$customer->firstname}{/if}" disabled>
                    </p>
                </div>
                
                <div class="form-group col-md-4">
                    <label for="lastname">{l s='Lastname'}</label>
                    <p class="form-control-static">
                        {*}{if isset($customer->lastname)}{$customer->lastname}{else}{l s='Unknown'}{/if}{*}
                        <input class="textbox-fix lettura" type="text" size="33" name="cognome" id="cognome" value="{if isset($customer->lastname)}{$customer->lastname}{/if}" disabled>
                    </p>
                </div>

            </div>

            <div class="row">

                <div class="form-group col-md-4">
                    <label for="esolver_code">{l s='eSolver Code'}</label>
                    <p class="form-control-static">
                        {*}{if isset($customer->codice_esolver)}{$customer->codice_esolver}{else}{l s='Unknown'}{/if}{*}
                        <input class="textbox-fix lettura" type="text" size="33" name="codice_esolver" id="codice_esolver" value="{if isset($customer->codice_esolver)}{$customer->codice_esolver}{/if}" disabled>
                    </p>
                </div>
                
                <div class="form-group col-md-4">
                    <label for="vat_code">{l s='VAT Code'}</label>
                    <p class="form-control-static">
                        {*}{if isset($customer->vat_number)}{$customer->vat_number}{else}{l s='Unknown'}{/if}{*}
                        <input class="textbox-fix lettura" type="text" size="33" name="vat_number" id="vat_number" value="{if isset($customer->vat_number)}{$customer->vat_number}{/if}" disabled>
                    </p>
                </div>
                
                <div class="form-group col-md-4">
                    <label for="tax_code">{l s='Tax Code'} <span id="cf_span"></span></label>
                    <p class="form-control-static">
                        {*}{if isset($customer->tax_code)}{$customer->tax_code}{else}{l s='Unknown'}{/if}{*}
                        <input class="textbox-fix lettura" type="text" size="33" name="tax_code" id="tax_code" onblur="controlla_fisco(this.value)" value="{if isset($customer->tax_code)}{$customer->tax_code}{/if}" disabled>
                    </p>
                </div>

            </div>

            <div class="row">
                
                <div class="form-group col-md-4">
                    <label for="email">{l s='Email'}{if isset($customer->email) AND $customer->email != ""} <a href="mailto:{$customer->email}"><i class="icon-envelope"></i> {l s='Send'}</a>{/if}</label>
                    <p class="form-control-static">
                        {*}{if isset($customer->email)}{$customer->email}{else}{l s='Unknown'}{/if}{*}
                        <input class="textbox-fix lettura" type="text" size="33" name="email" id="email" value="{if isset($customer->email)}{$customer->email}{/if}" disabled>
                    </p>
                </div>

                <div class="form-group col-md-4">
                    <label for="pec">{l s='PEC'}</label>
                    <p class="form-control-static">
                        {*}{if isset($customer->pec)}{$customer->pec}{else}{l s='Unknown'}{/if}{*}
                        <input class="textbox-fix lettura" type="text" size="33" name="pec" id="pec" value="{if isset($customer->pec)}{$customer->pec}{/if}" disabled>
                    </p>
                </div>
                
                <div class="form-group col-md-4">
                    <label for="telephone">{l s='Telephone'}{if isset($customer->telefono_principale) AND $customer->telefono_principale != ""} <a href="callto:{$customer->telefono_principale}"><i class="icon-phone"></i> {l s='Call'}</a>{/if}</label>
                    <p class="form-control-static">
                        {*}{if isset($customer->telefono_principale) AND $customer->telefono_principale != ""}{$customer->telefono_principale}{/if}{*}
                        <input class="textbox-fix lettura" type="text" size="33" name="telefono_principale" id="telefono_principale" value="{if isset($customer->telefono_principale)}{$customer->telefono_principale}{/if}" disabled>
                    </p>
                </div>

            </div>

            <div class="row">
                
                <div class="form-group col-md-4">
                    <label for="mobile">{l s='Mobile Phone'}{if isset($customer->cellulare_principale) AND $customer->cellulare_principale != ""} <a href="callto:{$customer->cellulare_principale}"><i class="icon-phone"></i> {l s='Call'}</a>{/if}</label>
                    <p class="form-control-static">
                        {*}{if isset($customer->cellulare_principale)}{$customer->cellulare_principale}{/if}{*}
                        <input class="textbox-fix lettura" type="text" size="33" name="cellulare_principale" id="cellulare_principale" value="{if isset($customer->cellulare_principale)}{$customer->cellulare_principale}{/if}" disabled>
                    </p>
                </div>
                
                <div class="form-group col-md-4">
                    <label for="fax">{l s='Fax'}</label>
                    <p class="form-control-static">
                    {* Controllare quale address prendere per il fax *}
                    {* Funzione da rivedere -------- *}
                        {*}
                        {foreach $addresses AS $key => $address}
                            {if isset($address['fax'])}
                                {$address['fax']}
                            {else}
                                {l s='Unknown'}
                            {/if}
                        {/foreach}
                        {*}
                        <input class="textbox-fix lettura" type="text" size="33" name="fax" id="fax" value="" disabled>
                    </p>
                </div>

                <div class="form-group col-md-4">
                    <label for="new_forced_psw">{l s='New forced password'}</label>
                    <p class="form-control-static">
                        {* Potrebbe essere messo solo in edit (forse corrisponde a "password" di edit) *}
                        {*}<input type="text" size="33" name="passwd" value="" disabled>{*}
                        <input class="textbox-fix lettura" type="text" size="33" name="passwd" id="passwd" value="" disabled>
                    </p>
                </div>

            </div>

            <div class="row">
                
                <div class="form-group col-md-4">
                {* Codice destinatario per fatturazione elettronica (SDI) *}
                    <label for="recipient_code">{l s='Recipient Code (SDI)'} - Per aziende <span id="sdi_span"></span></label></label>
                    <p class="form-control-static">
                        {*}{if isset($customer->codice_univoco)}{$customer->codice_univoco}{else}{l s='Unknown'}{/if}{*}
                        <input class="textbox-fix lettura" type="text" size="33" name="codice_univoco" id="codice_univoco" onblur="controlla_sdi(this.value)" value="{if isset($customer->codice_univoco)}{$customer->codice_univoco}{/if}" disabled>
                    </p>
                </div>
                
                <div class="form-group col-md-4">
                    <label for="ipa">{l s='IPA (Electronic Invoicing Code for PA)'}</label>
                    <p class="form-control-static">
                        {*}{if isset($customer->ipa)}{$customer->ipa}{else}{l s='Unknown'}{/if}{*}
                        <input class="textbox-fix lettura" type="text" size="33" name="ipa" id="ipa" value="{if isset($customer->ipa)}{$customer->ipa}{/if}" disabled>
                    </p>
                </div>
                
                

                <div class="form-group col-md-4">
                    <label for="tax_regime">{l s='Tax Regime'}</label>
                    <p class="form-control-static">
                        <select id="tax_regime" name="tax_regime" class="form-control textbox-fix lettura" disabled>
                        {foreach $list_tax_regime AS $key => $row}
                            {if $row != ""}<option value="{$row[0]}"{if $row[0] == $customer->tax_regime} selected{/if}>{$row[1]}</option>{/if}
                        {/foreach}
                        </select>
                        {*}
                        {if isset($customer->tax_regime)}
                            {if $customer->tax_regime == 0}
                                {l s='Base (Italy: VAT 22%)'}
                            {elseif $customer->tax_regime == 2}
                                {l s='VAT 10%'}
                            {elseif $customer->tax_regime == 3}
                                {l s='VAT 4%'}
                            {elseif $customer->tax_regime == 1}
                                {l s='Exempt'}
                            {elseif $customer->tax_regime == 4}
                                {l s='Split Payment with VAT 22%'}
                            {/if}
                        {else}
                            {l s='Unknown'}
                        {/if}
                        {*}
                    </p>
                    {* Questo va nel form di modifica:
                    <select class="form-control" id="tax_regime" readonly>
                        <option value="0">{l s='Base (Italy: VAT 22%)'}</option>
                        <option value="2">{l s='VAT 10%'}</option>
                        <option value="3">{l s='VAT 4%'}</option>
                        <option value="1">{l s='Exempt'}</option>
                        <option value="4">{l s='Split Payment with VAT 22%'}</option>
                    </select>
                    *}
                </div>

            </div>

            <div class="row">

                <div class="form-group col-md-4">
                    <label for="employees">{l s='Employees'}</label>
                    <p class="form-control-static">
                        {*}{if isset($customer->employees_number)}{$customer->employees_number}{else}{l s='Unknown'}{/if}{*}
                        <select id="employees_number" name="employees_number" class="form-control textbox-fix lettura" disabled>
                        {foreach $list_employees_number AS $key => $row}
                            {if $row != ""}<option value="{$row}"{if $row == $customer->employees_number} selected{/if}>{$row}</option>{/if}
                        {/foreach}
                        </select>
                    </p>
                </div>
                

                <div class="form-group col-md-4">
                    <label for="cust_prod_sector">{l s='Customer\'s product sector'}</label>
                    <p class="form-control-static">
                        {*}{if isset($customer->settore)}{$customer->settore}{else}{l s='Unknown'}{/if}{*}
                        <select id="settore" name="settore" class="form-control textbox-fix lettura" disabled>
                        {foreach $list_sector AS $key => $row}
                            {if $row != ""}<option value="{$row}"{if $row == $customer->settore} selected{/if}>{$row}</option>{/if}
                        {/foreach}
                        </select>
                    </p>

                </div>
                
                <div class="form-group col-md-4">
                    <label for="most_purch_cat">{l s='Most purchased category'}</label>
                    <p class="form-control-static">
                        {*}{if isset($customer->categoria)}{$customer->categoria}{else}{l s='Unknown'}{/if}{*}
                        <input class="textbox-fix lettura" type="text" size="33" name="categoria" id="categoria" value="{if isset($customer->categoria)}{$customer->categoria}{/if}" disabled>
                    </p>
                </div>

            </div>

            <div class="row">

                <div class="form-group col-md-4">
                    <label for="channel">{l s='Channel'}</label>
                    <p class="form-control-static">
                        {*}{if isset($customer->canale)}{$customer->canale}{else}{l s='Unknown'}{/if}{*}
                        <select id="canale" name="canale" class="form-control textbox-fix lettura" disabled>
                        {foreach $list_channel AS $key => $row}
                            {if $row != ""}<option value="{$row}"{if $row == $customer->canale} selected{/if}>{$row}</option>{/if}
                        {/foreach}
                        </select>
                    </p>
                </div>
                
                <div class="form-group col-md-4">
                    <label for="last_contact">{l s='Last contact'}</label>
                    <p class="form-control-static">
                        {*}{if isset($customer->ultimo_contatto)}{$customer->ultimo_contatto}{else}{l s='Unknown'}{/if}{*}
                        <input class="textbox-fix lettura" type="text" size="33" name="ultimo_contatto" id="ultimo_contatto" value="{if isset($customer->ultimo_contatto)}{$customer->ultimo_contatto}{/if}" disabled>
                    </p>
                </div>

                <div class="form-group col-md-4">
                    <label for="profile">{l s='Profile'}</label>
                    <p class="form-control-static">
                        {*}{if isset($customer->id_default_group)}{$groups[0]['name']}{else}{l s='Unknown'}{/if}{*}
                        <select id="profilo" name="profilo" class="form-control textbox-fix lettura" disabled>
                        {foreach $gruppi AS $key => $row}
                            {if $row != ""}<option value="{$row["id_group"]}"{if $row["id_group"] == $customer->id_default_group} selected{/if}>{$row["name"]}</option>{/if}
                        {/foreach}
                        </select>

                    </p>
                </div>
                
            </div>

            <!--<hr />-->

            <div class="row">

                <div class="form-group col-md-2">
                    <label for="customer_type">{l s='Customer type'}</label>
                    <div id="tipo_cliente_let">
                        <p class="form-control-static">
                            {if ! isset($customer->is_company)}
                                <span class="label label-default squadrato">
                                    <i class="icon-question"></i>
                                    {l s='Unknown'}
                                </span>
                            {elseif $customer->is_company == 1}
                                <span class="label label-primary squadrato">
                                    <i class="icon-building"></i>
                                    {l s='Company'}
                                </span>
                            {elseif $customer->is_company == 0}
                                <span class="label label-primary squadrato">
                                    <i class="icon-user"></i>
                                    {l s='Individual'}
                                </span>
                            {/if}
                        </p>
                    </div>
                    <div id="tipo_cliente_mod" class="nascosto">
                        <p class="form-control-static">
                            <select id="tipo_cliente" name="tipo_cliente" class="form-control textbox-fix select2">
                                {if ! isset($customer->is_company)}<option value=""{if $customer->consenso_1 == 1} selected{/if}>?</option>{/if}
                                <option value="azienda"{if $customer->is_company == 1} selected{/if}>Azienda</option>
                                <option value="privato"{if $customer->is_company != 1} selected{/if}>Privato</option>
                            </select>
                        </p>
                    </div>
                </div>
                

                <div class="form-group col-md-2">
                    <label for="supplier">{l s='Supplier'}</label>
                    <div id="fornitore_let">
                        <p class="form-control-static">
                            {if ! isset($customer->supplier)}
                                <span class="label label-default squadrato">
                                    <i class="icon-question"></i>
                                    {*l s='Unknown'*}
                                </span>
                            {elseif $customer->supplier == 1 }
                                <span class="label label-primary squadrato">
                                    <i class="icon-check"></i>
                                    {*l s='Yes'*}
                                </span>
                            {elseif $customer->supplier == 0}
                                <span class="label label-primary squadrato">
                                    <i class="icon-remove"></i>
                                    {*l s='No'*}
                                </span>
                            {/if}
                        </p>
                    </div>
                    <div id="fornitore_mod" class="nascosto">
                        <p class="form-control-static">
                            <input type="checkbox" class="form-control" name="fornitore"{if $customer->supplier == 1 } selected{/if}>
                        </p>
                    </div>
                </div>

                <div class="form-group col-md-2">
                    <label for="risk_customer">{l s='Risk customer'}</label>
                    <div id="cliente_risk_let">
                        <p class="form-control-static">
                            {if ! isset($customer->id_risk)}
                                <span class="label label-default squadrato">
                                    <i class="icon-question"></i>
                                </span>
                            {elseif $customer->id_risk == 1}
                                <span class="label label-danger squadrato">
                                    <i class="icon-check"></i>
                                </span>
                            {elseif $customer->id_risk == 0}
                                <span class="label label-success squadrato">
                                    <i class="icon-remove"></i>
                                </span>
                            {/if}
                        </p>
                    </div>
                    <div id="cliente_risk_mod" class="nascosto">
                        <p class="form-control-static">
                            <input type="checkbox" class="form-control" name="risk"{if $customer->id_risk == 1 } selected{/if}>
                        </p>
                    </div>
                </div>

                <div class="form-group col-md-2">
                    <label for="order_notifications">{l s='Email activity notification'}</label>
                    <div id="notifiche_email_let">
                        <p class="form-control-static">
                            {if ! isset($customer->order_notifications)}
                                <span class="label label-default squadrato">
                                    <i class="icon-question"></i>
                                    {*l s='Unknown'*}
                                </span>
                            {elseif $customer->order_notifications == 1}
                                <span class="label label-primary squadrato">
                                    <i class="icon-check"></i>
                                    {*l s='Yes'*}
                                </span>
                            {elseif $customer->order_notifications == 0}
                                <span class="label label-primary squadrato">
                                    <i class="icon-remove"></i>
                                    {*l s='No'*}
                                </span>
                            {/if}
                        </p>
                    </div>
                    <div id="notifiche_email_mod" class="nascosto">
                        <p class="form-control-static">
                            <input type="checkbox" class="form-control" name="notifiche_email"{if $customer->order_notifications == 1 } selected{/if}>
                        </p>
                    </div>
                </div>

                <div class="form-group col-md-2">
                    <label for="gender">{l s='Gender'}</label>
                    <div id="genere_let">
                        <p class="form-control-static">
                            {if ! isset($customer->id_gender) || $customer->id_gender == 9}
                                <span class="label label-default squadrato">
                                    <i class="icon-question"></i>
                                    {*l s='Unknown'*}
                                </span>
                            {elseif $customer->id_gender == 1}
                                <span class="label label-primary squadrato">
                                    <i class="icon-male"></i>
                                    {*l s='Male'*}
                                </span>
                            {elseif $customer->id_gender == 2}
                                <span class="label label-primary squadrato">
                                    <i class="icon-female"></i>
                                    {*l s='Female'*}
                                </span>
                            {/if}
                        </p>
                    </div>
                    <div id="genere_mod" class="nascosto">
                        <p class="form-control-static">
                            <select id="genere" name="genere" class="form-control textbox-fix select2">
                                <option value="sconosciuto"{if ! isset($customer->id_gender) || $customer->id_gender == 9} selected{/if}>Sconosciuto</option>
                                <option value="maschio"{if $customer->id_gender == 1} selected{/if}>Maschio</option>
                                <option value="femmina"{if $customer->id_gender == 2} selected{/if}>Femmina</option>
                            </select>
                        </p>
                    </div>
                </div>
                
                <div class="form-group col-md-2">
                    <label for="status">{l s='Status'}</label>
                    <div id="status_let">
                    <p class="form-control-static">
                        {if ! isset($customer->active) }
                            <span class="label label-default squadrato">
                                <i class="icon-question"></i>
                                {*l s='Unknown'*}
                            </span>
                        {elseif $customer->active == 1}
                            <span class="label label-tooltip label-primary squadrato" data-toggle="tooltip" data-html="true" title="{l s='Active'}">
                            {*}<span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="{l s='Activity discontinued'}">{*}
                                <i class="icon-check"></i>
                                {*l s='Active'*}
                            </span>
                        {elseif $customer->active == 0}
                            <span class="label label-tooltip label-danger squadrato" data-toggle="tooltip" data-html="true" title="{l s='Inactive'}">
                                <i class="icon-remove"></i>
                                {*l s='Inactive'*}
                            </span>
                        {elseif $customer->active == 2}
                            <span class="label label-tooltip label-warning squadrato" data-toggle="tooltip" data-html="true" title="{l s='Activity discontinued'}">
                                <i class="icon-remove-sign"></i>
                                {*l s='Activity discontinued'*}
                            </span>
                        {/if}
                    </p>
                    </div>
                    <div id="status_mod" class="nascosto">
                        <p class="form-control-static">
                            <select id="genere" name="genere" class="form-control textbox-fix select2">
                                <option value="attiva"{if $customer->active == 1} selected{/if}>{l s='Active'}</option>
                                <option value="non_attiva"{if $customer->active == 0} selected{/if}>{l s='Inactive'}</option>
                                <option value="cessata_attivita"{if $customer->active == 2} selected{/if}>{l s='Activity discontinued'}</option>
                            </select>
                        </p>
                    </div>
                </div>

                
                {*
                <div class="form-group col-md-1">
                    <label for="risk_customer"></label>
                    <p class="form-control-static">
                        {if ! isset($customer->risk_customer)}
                            <span class="label label-default">
                                <i class="icon-question"></i>
                                {l s='Unknown'}
                            </span>
                        {elseif $customer->risk_customer == 0 }
                            <span class="label label-success">
                                <i class="icon-check"></i>
                                {l s='Yes'}
                            </span>
                        {elseif $customer->risk_customer == 1}
                            <span class="label label-danger">
                                <i class="icon-remove"></i>
                                {l s='No'}
                            </span>
                        {/if}
                    </p>
                    
                </div>
                *}

                {*
                <div class="form-group col-md-3">
                    <label for="recipient_code">{l s='Recipient Code (SDI)'}</label>
                    <input type="text" class="form-control"  value="{if isset($customer->recipient_code)}{$customer->recipient_code}{else}{l s='Unknown'}{/if}" readonly>
                </div>
                *}


                
                
            </div>

            <div class="row">
                <div class="form-group col-md-2">
                    <label for="consents">{l s='Consents'}</label>
                    <div id="consensi_let">
                        <p class="form-control-static">
                            {if ! isset($customer->consenso_1)}
                                <span class="label label-default squadrato">
                                    <i class="icon-question"></i>
                                    {l s='Base Consent'}
                                </span>
                            {elseif $customer->consenso_1 == 1}
                                <span class="label label-primary squadrato">
                                    <i class="icon-check"></i>
                                    {l s='Base Consent'}
                                </span>
                            {elseif $customer->consenso_1 == 0}
                                <span class="label label-primary squadrato">
                                    <i class="icon-remove"></i>
                                    {l s='Base Consent'}
                                </span>
                            {/if}
                            <br />
                            {if ! isset($customer->consenso_2)}
                                <span class="label label-default squadrato">
                                    <i class="icon-question"></i>
                                    {l s='Marketing'}
                                </span>
                            {elseif $customer->consenso_2 == 1}
                                <span class="label label-primary squadrato">
                                    <i class="icon-check"></i>
                                    {l s='Marketing'}
                                </span>
                            {elseif $customer->consenso_2 == 0}
                                <span class="label label-primary squadrato">
                                    <i class="icon-remove"></i>
                                    {l s='Marketing'}
                                </span>
                            {/if}
                            <br />
                            {if ! isset($customer->consenso_3)}
                                <span class="label label-default squadrato">
                                    <i class="icon-question"></i>
                                    {l s='3rd Marketing'}
                                </span>
                            {elseif $customer->consenso_3 == 1}
                                <span class="label label-primary squadrato">
                                    <i class="icon-check"></i>
                                    {l s='3rd Marketing'}
                                </span>
                            {elseif $customer->consenso_3 == 0}
                                <span class="label label-primary squadrato">
                                    <i class="icon-remove"></i>
                                    {l s='3rd Marketing'}
                                </span>
                            {/if}
                        </p>
                    </div>
                    <div id="consensi_mod" class="nascosto">
                        <p class="form-control-static">
                            <select id="consensi" name="consensi[]" class="form-control textbox-fix select2" multiple="multiple">
                                <option value="base"{if $customer->consenso_1 == 1} selected{/if}>Base</option>
                                <option value="marketing"{if $customer->consenso_2 == 1} selected{/if}>Marketing</option>
                                <option value="3rdmarketing"{if $customer->consenso_3 == 1} selected{/if}>3rd Marketing</option>
                            </select>
                        </p>
                    </div>
                </div>

                <div class="form-group col-md-2">
                    <label for="birthday">{l s='Birthday (Age)'}</label>
                    <div id="compleanno_let">
                        <p class="form-control-static">
                            <input class="textbox-fix lettura" type="text" size="33" name="compleanno" id="compleanno_let" value="{if isset($customer->birthday) && $customer->birthday != '0000-00-00'}{l s='%1$s (%2$d years old)' sprintf=[$customer_birthday, $customer_stats['age']]}{else}{l s='Unknown'}{/if}" disabled>
                        </p>
                    </div>
                    <div id="compleanno_mod" style="display: none;">
                        <p class="form-control-static">
                            <input class="textbox-fix lettura" type="date" name="compleanno" id="compleanno" value="{if isset($customer->birthday) && $customer->birthday != '0000-00-00'}{l s='%1$s' sprintf=[$customer_birthday]}{/if}">
                        </p>
                    </div>
                </div>

                <div class="form-group col-md-2">
                    <label for="registration_date">{l s='Registration Date'}</label>
                    <p class="form-control-static">
                        <input class="textbox-fix lettura" type="text" size="33" name="data_registrazione" id="data_registrazione" value="{if ! isset($registration_date) }{l s='Unknown'}{else}{$registration_date}{/if}" disabled>
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="last_visit">{l s='Last Visit'}</label>
                    <p class="form-control-static">
                        <input class="textbox-fix lettura" type="text" size="33" name="ultima_visita" id="ultima_visita" value="{if $customer_stats['last_visit']}{$last_visit}{else}{l s='Never'}{/if}" disabled>
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="last_update">{l s='Last Update'}</label>
                    <p class="form-control-static">
                        <input class="textbox-fix lettura" type="text" size="33" name="ultimo_aggiornamento" id="ultimo_aggiornamento" value="{if isset($last_update)}{$last_update}{else}{l s='Unknown'}{/if}" disabled>
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="language">{l s='Language'}</label>
                    <p class="form-control-static">
                        <input class="textbox-fix lettura" type="text" size="33" name="lingua" id="lingua" value="{if isset($customerLanguage->name)}{$customerLanguage->name}{else}{l s='Unknown'}{/if}" disabled>
                    </p>
                </div>

            </div>

        </div>
        {if $customer->isGuest()}
            {l s='This customer is registered as a Guest.'}
            {if !$customer_exists}
            <form method="post" action="index.php?tab=AdminCustomers&amp;id_customer={$customer->id|intval}&amp;token={getAdminToken tab='AdminCustomers'}">
                <input type="hidden" name="id_lang" value="{$id_lang}" />
                <p class="text-center">
                    <input class="button" type="submit" name="submitGuestToCustomer" value="{l s='Transform to a customer account'}" />
                </p>
                {l s='This feature generates a random password before sending an email to your customer.'}
            </form>
            {else}
            <p class="text-muted text-center">
                {l s='A registered customer account using the defined email address already exists. '}
            </p>
            {/if}
        {/if}
    </div>
</form>


<div class="form-horizontal col-lg-12">
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-eye-close"></i> {l s='Private notes'}

            <div class="panel-heading-action">
                <a class="btn btn-default add-new">

                    <i class="icon-plus-sign"></i>
                    {l s='Add private note'}
                </a>
            </div>
        </div>
        <div class="form-group">

            <table class="table table-bordered tabella_note" id="noteTable">
                <thead>
                    <tr>
                        <th>{l s='Note'}</th>
                        <th>{l s='Created by'}</th>
                        <th>{l s='Last edit'}</th>
                        <th>{l s='Actions'}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr style="display:none"> {* Necessario per aggiungere la prima nota in js *}
                        <td class="mod"></td>
                        <td></td>
                        <td></td>
                        <td class="azioni_note">
                            <a class="add" title="Add" data-toggle="tooltip"><i class="icon-save"></i></a>
                            <a class="edit" title="Edit" data-toggle="tooltip"><i class="icon-edit"></i></a>
                            <a class="delete" title="Delete" data-toggle="tooltip"><i class="icon-trash"></i></a>
                        </td>
                    </tr>
                {foreach $customer_note as $key => $nota_privata}
                    <tr dbid="{$nota_privata['id_note']}">
                        <td class="mod">{$nota_privata['note']}</td>
                        <td>{$nota_privata['creato_da']}</td>
                        <td>{$nota_privata['date_upd']}</td>
                        <td class="azioni_note">
                            <a class="add" title="Add" data-toggle="tooltip"><i class="icon-save"></i></a>
                            <a class="edit" title="Edit" data-toggle="tooltip"><i class="icon-edit"></i></a>
                            <a class="delete" title="Delete" data-toggle="tooltip"><i class="icon-trash"></i></a>
                        </td>
                    </tr>
                {/foreach}
   
                </tbody>
            </table>

            {*
            <div class="row">
                <div class="col-lg-7">
                    <p class="thick">{l s='Note'}</p>
                </div>
                <div class="col-lg-2">
                    <p class="thick">{l s='Created by'}</p>
                </div>
                <div class="col-lg-2">
                    <p class="thick">{l s='Last edit'}</p>
                </div>
                <div class="col-lg-1">
                </div>
            </div>
            {foreach $customer_note as $key => $nota_privata}
            <div class="row">
                <input id="id_nota" name="id_nota" type="hidden" value="{$nota_privata['id_note']}">
                <div class="col-lg-7">
                    <textarea id="nota_privata_{$nota_privata['id_note']}" name="nota_privata_{$nota_privata['id_note']}" class="lettura" readonly>{$nota_privata['note']}</textarea>
                </div>
                <div class="col-lg-2">
                    {$nota_privata['creato_da']}
                </div>
                <div class="col-lg-2">
                    {$nota_privata['date_upd']}
                </div>
                <div class="col-lg-1">
                    <button type="submit" id="deleteCustomerNote" class="btn btn-default pull-right">
                        <i class="icon-trash"></i>
                    </button>
                </div>
            </div>
            {/foreach}

            <div id="nuove-note">

            </div>

            *}
        </div>

        <hr />
        
        <form method="post" enctype="multipart/form-data" name="keyword_form" novalidate>
            <div class="row">
                <div class="col-lg-7">
                <label for="keyword">Keyword</label>
                <textarea id="keyword" name="keyword">{$customer->keyword}</textarea>
                
                </div>
                <div class="col-lg-2">
                    <button type="submit" name="submitCustomerKeyword" class="btn btn-default">
                        <i class="icon-save"></i>
                        {l s='Save'}
                    </button>
                </div>
            </div>
        </form>
        
    </div>
</div>
{*
<div style="display: none">
    <div id="nota_privata_cpy" >
        <br />
        <div class="row" >
            <div class="col-lg-7">
                <textarea name="nuova_nota_privata[]"></textarea>
            </div>
            <div class="col-lg-2">
                <p>{$mio_nome}</p>
            </div>
            <div class="col-lg-2">
                <p>{$smarty.now|date_format:"%Y-%m-%d"}</p>
            </div>
            <div class="col-lg-1">
                <button type="submit" id="deleteCustomerNote" class="btn btn-default pull-right" onclick="">
                    <i class="icon-trash"></i>
                </button>
            </div>
            
        </div>
    </div>
</div>
*}

{* Javascript della pagina *}
{include file="./js/anagrafica_js.tpl"}