<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" name="product" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-user"></i>
            Aggiungi BDL {if $smarty.get.riferimento !== null}- <span style="color:orangered">Azione padre: </span><a href="{$link_gen_base}tab_name=tickets&azione=tickets&viewticket&id_customer_thread={$smarty.get.riferimento|substr:1}{$token_base}" target="_blank">{$bdl['buoni']['sigla_ticket']}</a>{/if}
            
            <div class="bottoni-tab">
                <a class="btn btn-default" href="">
                    {l s='Generate PDF TecnoTel'}
                </a>
                <a class="btn btn-default" href="">
                    {l s='Generate PDF Ezdirect'}
                </a>
                <a class="btn btn-default" href="">
                    {l s='Send BDL by mail'}
                </a>
                <a class="btn btn-default" href="{$link_gen_base}tab_name=bdl{$token_base}">
                    <i class="icon-circle-arrow-left"></i>
                    Vedi BDL del cliente
                </a>
                <a class="btn btn-default" href="{$link->getAdminLink('AdminBDL')}">
                    <i class="icon-circle-arrow-left"></i>
                    Vedi tutti i BDL
                </a>
            </div>
        </div>

        <div class="row">
            <div class="form-row">
                <div class="form-group col-md-1">
                    <label for="">{l s='N. BDL'}</label>
                    <input readonly type="text" class="form-control textbox-fix" id="id_bdl" placeholder="Da generare" value="">
                </div>
                <div class="form-group col-md-6">
                    <label for="">{l s='Billing address'}</label>
                    <input readonly type="text" class="form-control textbox-fix" id="indirizzo_fatturazione_bdl" name="indirizzo_fatturazione_bdl" value="{$bdl['buoni']['indirizzo_fatt']['address1']} - {$bdl['buoni']['indirizzo_fatt']['postcode']}  {$bdl['buoni']['indirizzo_fatt']['city']} ({$bdl['buoni']['indirizzo_fatt']['iso_code']})">
                    <input type="hidden" name="indirizzo_bdl" id="indirizzo_bdl" value="{$bdl['buoni']['indirizzo_fatt']['id_address']}">
                </div>
                <div class="form-group col-md-2">
                    <label for="bdl_phone">{l s='Tel.'}</label>
                    <select id="bdl_phone" name="bdl_phone" class="form-control textbox-fix">
                        <option>{l s='Choose...'}</option>
                        {foreach $bdl['buoni']['telefoni'] AS $key => $telefono_mist}
                            {foreach $telefono_mist AS $key => $numero}
                                {if $numero != ""}<option value="{$numero}"{if $bdl['buoni']['phone'] == $numero} selected{/if}>{$numero}</option>{/if}
                            {/foreach}
                        {/foreach}
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label for="">{l s='Other'}</label>
                    <input type="text" class="form-control textbox-fix" id="bdl_phone_altro" name="bdl_phone_altro" value="">
                </div>
                {* Riferimento (ticket - azione padre) *}
                {if $smarty.get.riferimento === null}
                <div class="form-group col-md-1">
                    <label for="riferimento_azione">{l s='Reference'}</label>
                    <select id="riferimento_azione" name="riferimento_azione" class="form-control textbox-fix">
                        <option value="0">{l s='Choose...'}</option>
                        {foreach $bdl['buoni']['rif_tickets'] AS $ticket}
                            <option value="{$ticket['rif']}">{$ticket['sigla']}</option>
                        {/foreach}
                    </select>
                </div>
                {/if}
            </div>
        </div>

        <div class="row">
            <div class="form-row">
                {* Richiesto il giorno *}
                <div class="form-group col-md-3">
                    <div class="form-group col-md-6">
                        <label for="data_richiesta">{l s='Asked on'}</label>
                        <input type="date" class="form-control textbox-fix" id="data_richiesta" name="data_richiesta" value="{$bdl['buoni']['data_richiesta']}">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="ora_richiesta">{l s='At'}</label> {* Alle ore *}
                        <input type="text" id="ora_richiesta" name="ora_richiesta" value="{$bdl['buoni']['ora_richiesta']}">
                    </div>
                </div>
                {* Da *}
                <div class="form-group col-md-2">
                    <label for="inputState">{l s='From'}</label>
                    <select id="richiesto_da" name="richiesto_da" class="form-control textbox-fix">
                        {foreach $bdl['buoni']['persone'] AS $key => $persona}
                            <option value="{$persona['id_persona']}"{if $persona['id_persona'] == $bdl['buoni']['richiesto_da']} selected{/if}>{$persona['firstname']} {$persona['lastname']}</option>
                        {/foreach}
                    </select>
                </div>
                {* Effettuato il giorno *}
                <div class="form-group col-md-3">
                    <div class="form-group col-md-6">
                        <label for="data_effettuato">{l s='Carried out on'}</label>
                        <input type="date" class="form-control textbox-fix" id="data_effettuato" name="data_effettuato" value="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="ora_effettuato">{l s='At'}</label> {* Alle ore *}
                        <input type="text" id="ora_effettuato" name="ora_effettuato" value="00:00">
                    </div>
                </div>
                {* Da *}
                <div class="form-group col-md-2">
                    <label for="effettuato_da">{l s='From'}</label>
                    <select id="effettuato_da" name="effettuato_da" class="form-control textbox-fix">
                    {foreach $bdl['buoni']['impiegati'] AS $key => $impiegato}
                        <option value="{$impiegato['id']}"{if $impiegato['id'] == $bdl['buoni']['effettuato_da']} selected{/if}>{$impiegato['name']}</option>
                    {/foreach}
                    </select>
                </div>
                {* Incluso nell'ordine *}
                <div class="form-group col-md-2">
                    <label for="incluso_in_ordine">{l s='Included in the order'}
                        <span class="label-tooltip text-primary" id="incluso_in_ordine" data-toggle="tooltip" data-html="true" title="<strong>ATTENZIONE</strong>: questo campo va riempito solo se l'attività prevista dal BDL è già inclusa in un ordine. Se devi <strong>addebitare nuovi costi al cliente</strong>, crea un nuovo BDL.">
                            <i class="icon-question-sign"></i>
                        </span>
                    </label>
                    <select id="rif_ordine" name="rif_ordine" class="form-control textbox-fix">
                        <option>--</option>
                        {foreach $bdl['buoni']['ordini'] AS $key => $ordine}
                            <option value="{$ordine['id']}"{if $ordine['id'] == $bdl['buoni']['rif_ordine']} selected{/if}>{$ordine['stringa']}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-row">
                {* Impianto-Ubicazione *}
                <div class="form-group col-md-4">
                    <label for="impianto_ubicazione">{l s='Plant-Location'}</label>
                    <select id="impianto_ubicazione" name="impianto_ubicazione" class="form-control textbox-fix">
                        {foreach $bdl['buoni']['indirizzi_bdl'] AS $key => $indirizzo}
                            <option value="{$indirizzo['id_address']}"{if is_numeric($bdl['buoni']['impianto_ubicazione']) AND $bdl['buoni']['impianto_ubicazione'] == $indirizzo['id_address']} selected{/if}>{$indirizzo['address1']} - {$indirizzo['postcode']} {$indirizzo['city']} ({$indirizzo['iso_code']})</option>
                        {/foreach}
                        <option {if ! is_numeric($bdl['buoni']['impianto_ubicazione']) OR empty($bdl['buoni']['impianto_ubicazione'])} selected {/if}>Altro</option>
                    </select>
                </div>
                {* Se altro, specificare *}
                <div class="form-group col-md-8">
                    <label for="impianto_ubicazione_manuale">{l s='If other, please specify'}</label>
                    <input type="text" class="form-control textbox-fix" id="impianto_ubicazione_manuale" name="impianto_ubicazione_manuale" value="{if ! is_numeric($bdl['buoni']['impianto_ubicazione']) OR empty($bdl['buoni']['impianto_ubicazione'])}{$bdl['buoni']['impianto_ubicazione']}{/if}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-row">
                {* Contratto assistenza *}
                <div class="form-group col-md-4">
                    <label for="">{l s='Service contract'}</label>
                    <input type="checkbox" id="contratto_assistenza" name="contratto_assistenza" value="1" onchange="f_contratto_assistenza()" />
                </div>
                {* Manutenzione ordinaria (= intervento da contratto di assistenza) *}
                <div class="form-group col-md-4">
                    <label for="">{l s='Ordinary maintenance'} 
                        <span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="That is, assistance contract intervention">
                            <i class="icon-question-sign"></i>
                        </span>
                    </label>
                    <input type="checkbox" id="manutenzione_ordinaria" name="manutenzione_ordinaria" value="1" onclick="f_manutenzione_ordinaria()" />
                </div>
                {* Manutenzione straordinaria *}
                <div class="form-group col-md-4">
                    <label for="">{l s='Extraordinary maintenance'}</label>
                    <input type="checkbox" id="manutenzione_straordinaria" name="manutenzione_straordinaria" value="1" onclick="f_manutenzione_straordinaria()" checked="checked" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-row">
                {* Motivo chiamata *}
                <div class="form-group col-md-12">
                    <label for="motivo_chiamata">{l s='Reason for calling'}</label>
                    <input type="text" class="form-control textbox-fix" id="motivo_chiamata" name="motivo_chiamata" value="">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-row">
                {* Descrizione *}
                <div class="form-group col-md-2">
                    <label for="descrizione">{l s='Description'}</label>
                    <select id="descrizione" name="descrizione" class="form-control textbox-fix">
                        <option id="Altro">Altro</option>
                        <option id="Ripristino funzionalita">Ripristino funzionalita</option>
                        <option id="Upgrade firmware">Upgrade firmware</option>
                        <option id="Configurazione servizi">Configurazione servizi</option>
                        <option id="Configurazione nuovo hardware">Configurazione nuovo hardware</option>
                    </select>
                </div>

                {* Se altro, specificare *}
                <div class="form-group col-md-10">
                    <label for="descrizione_altro">{l s='If other, please specify'}</label>
                    <input type="text" class="form-control textbox-fix" id="descrizione_altro" name="descrizione_altro" value="">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-row">
                {* Guasto riscontrato dal tecnico *}
                <div class="form-group col-md-2">
                    <label for="guasto">{l s='Fault found by the technician'}</label>
                    <select id="guasto" name="guasto" class="form-control textbox-fix">
                        <option id="Altro">Altro</option>
                        <option id="Nessuna">Nessuna</option>
                        <option id="Impianto fermo">Impianto fermo</option>
                        <option id="Problemi su linee">Problemi su linee</option>
                        <option id="Problemi di fonia">Problemi di fonia</option>
                    </select>
                </div>

                {* Se altro, specificare *}
                <div class="form-group col-md-10">
                    <label for="guasto_altro">{l s='If other, please specify'}</label>
                    <input type="text" class="form-control textbox-fix" id="guasto_altro" name="guasto_altro" value="">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-row">
                {* Causa del guasto *}
                <div class="form-group col-md-2">
                    <label for="causa_guasto">{l s='Cause of the failure'}</label>
                    <select id="causa_guasto" name="causa_guasto" class="form-control textbox-fix">
                        <option id="Altro">Altro</option>
                        <option id="Nessuno">Nessuno</option>
                        <option id="Sovratensione">Sovratensione</option>
                        <option id="Dolo/Incuria/Accidentale">Dolo/Incuria/Accidentale</option>
                        <option id="Ossido/Umidità">Ossido/Umidità</option>
                        <option id="Usura">Usura</option>
                        <option id="Non definita">Non definita</option>
                    </select>
                </div>

                {* Se altro, specificare *}
                <div class="form-group col-md-10">
                    <label for="causa_guasto_altro">{l s='If other, please specify'}</label>
                    <input type="text" class="form-control textbox-fix" id="causa_guasto_altro" name="causa_guasto_altro" value="">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-row">
                {* Descrizione intervento svolto *}
                <div class="form-group col-md-10">
                    <label for="intervento_svolto">{l s='Description of the intervention carried out'}</label>
                    <textarea class="form-control" id="intervento_svolto" name="intervento_svolto" rows="4"></textarea>
                </div>
            </div>
        </div>

        <div class="row" id="seleziona_prodotto">
            <div class="form-row">
                <div class="form-group col-md-10">
                    <label for="">{l s='Select the product to add:'}</label>
                </div>
            </div>
        </div>

        <div class="row" id="aggiungi_prodotti">
            {* ROW DI AGGIUNTA PRODOTTI ECC - Correggere *}

            <div class="form-row">
                <div class="col-md-1">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                    <label class="form-check-label" for="inlineCheckbox1">Online</label>
                </div>
                <div class="col-md-1">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option1">
                    <label class="form-check-label" for="inlineCheckbox2">Offline</label>
                </div>
                <div class="col-md-1">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option1">
                    <label class="form-check-label" for="inlineCheckbox3">Old</label>
                </div>
                <div class="col-md-1">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option1">
                    <label class="form-check-label" for="inlineCheckbox4">{l s='Only av.'}</label> {* Solo disp. *}
                </div>

                <div class="col-md-1">
                    <select id="brand" class="chosen form-control" name="brand">
                        <option value="">{l s='Brand...'}</option>
                        {foreach $bdl['buoni']['marche'] AS $key => $marche}
                            <option value="{$marche['id']}">{$marche['name']}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-md-1">
                    <select id="serie" class="chosen form-control" name="serie">
                        <option value="">{l s='Series...'}</option>
                        <option value="">Scegli prima un costruttore</option>
                    </select>
                </div>
                <div class="col-md-1">
                    <select id="categorie" class="chosen form-control" name="categorie">
                        <option value="">{l s='Category...'}</option>
                        {foreach $bdl['buoni']['categorie'] AS $key => $categorie}
                            <option value="{$categorie['id']}">{$categorie['name']}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-md-1">
                    <select id="fornitori" class="chosen form-control" name="fornitori">
                        <option value="">{l s='Supplier...'}</option>
                        {foreach $bdl['buoni']['fornitori'] AS $key => $fornitori}
                            <option value="{$fornitori['id']}">{$fornitori['name']}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
        </div>

        <hr />

        <div class="row">
        </div>

        <hr />

        <div class="row" id="tabella_costi">
            <div class="form-group col-md-10">
                <label>Dettaglio economico dell'intervento</label>
            </div>
            <div class="form-group col-md-10">
                <table class="table">
                    <thead>
                        <tr>
                            <th><span class="title_box ">{l s='Detail'}</span></th>
                            <th><span class="title_box ">{l s='Code'}</span></th>
                            <th><span class="title_box ">{l s='Description'}</span></th>
                            <th><span class="title_box ">{l s='Qta'}</span></th>
                            <th><span class="title_box ">{l s='Each'}</span></th>
                            <th><span class="title_box ">{l s='Discount %'}</span></th>
                            <th><span class="title_box ">{l s='Amount'}</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Diritto di chiamata</td>
                            <td id="diritto_chiamata_codice"></td>
                            <td>
                                <select onchange="aggiorna_costo_unitario('diritto_chiamata')" name="diritto_chiamata_tipo" id="diritto_chiamata_tipo">
                                    <option value="">-- Seleziona --</option>
                                    <option value="EZDIRFIX">Fisso intervento < 20 km</option>
                                    <option value="EZDIRFIXF">Fisso intervento < 20 km festivo</option>
                                </select>
                            </td>
                            
                            <td><input onblur="aggiorna_costo_totale_riga('diritto_chiamata')" type="text" id="diritto_chiamata_qta" name="diritto_chiamata_qta"></td>
                            <td><input onblur="aggiorna_costo_totale_riga('diritto_chiamata')" type="text" id="diritto_chiamata_unitario" name="diritto_chiamata_unitario"></td>
                            <td><input onblur="aggiorna_costo_totale_riga('diritto_chiamata')" type="text" id="diritto_chiamata_sconto" name="diritto_chiamata_sconto"></td>
                            <td><input type="text" id="diritto_chiamata_importo" name="diritto_chiamata_importo" readonly></td>
                        </tr>

                        <tr>
                            <td>Chilometri percorsi</td>
                            <td id="chilometri_percorsi_codice"></td>
                            <td>
                                <select onchange="aggiorna_costo_unitario('chilometri_percorsi')" name="chilometri_percorsi_tipo" id="chilometri_percorsi_tipo">
                                    <option value="">-- Seleziona --</option>
                                    <option value="EZKMR">Rimborso km oltre diritto fisso</option>
                                    <option value="EZZONAFIX">Chiamata fissa con partner di zona</option>
                                </select>
                            </td>
                            <td><input onblur="aggiorna_costo_totale_riga('chilometri_percorsi')" type="text" id="chilometri_percorsi_qta" name="chilometri_percorsi_qta"></td>
                            <td><input onblur="aggiorna_costo_totale_riga('chilometri_percorsi')" type="text" id="chilometri_percorsi_unitario" name="chilometri_percorsi_unitario"></td>
                            <td><input onblur="aggiorna_costo_totale_riga('chilometri_percorsi')" type="text" id="chilometri_percorsi_sconto" name="chilometri_percorsi_sconto"></td>
                            <td><input type="text" id="chilometri_percorsi_importo" name="chilometri_percorsi_importo" readonly></td>
                        </tr>

                        <tr>
                            <td>Costo manodopera</td>
                            <td id="costo_manodopera_codice"></td>
                            <td>
                                <select onchange="aggiorna_costo_unitario('costo_manodopera')" name="costo_manodopera_tipo" id="costo_manodopera_tipo">
                                    <option value="">-- Seleziona --</option>
                                    <option value="EZTECORD">Tariffa oraria ordinaria</option>
                                    <option value="EZTEC30">Tariffa 30 minuti ordinaria</option>
                                    <option value="EZTECXTRA">Tariffa oraria straordinaria</option>
                                    <option value="EZTECFEST">Tariffa oraria festiva(modificato)</option>
                                </select>
                            </td>
                            <td><input onblur="aggiorna_costo_totale_riga('costo_manodopera')" type="text" id="costo_manodopera_qta" name="costo_manodopera_qta"></td>
                            <td><input onblur="aggiorna_costo_totale_riga('costo_manodopera')" type="text" id="costo_manodopera_unitario" name="costo_manodopera_unitario"></td>
                            <td><input onblur="aggiorna_costo_totale_riga('costo_manodopera')" type="text" id="costo_manodopera_sconto" name="costo_manodopera_sconto"></td>
                            <td><input type="text" id="costo_manodopera_importo" name="costo_manodopera_importo" readonly></td>
                        </tr>

                        {foreach $dettaglio_economico['asd'] AS $key => $call}
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        {/foreach}
                    </tbody>
                    <tfoot>
                        <td>Tot. importo intervento IVA escl. S.E. & O.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="text" id="totale_BDL" name="totale_BDL" readonly></td>
                    </tfoot>
                </table>
                <input type="hidden" id="num_varie" value="0">
            </div>
        </div>

        <br />

        {* Note bdl *}
        <div class="row" id="note">
            <div class="form-row">
                <div class="form-group col-md-10">
                    <label for="">{l s='Notes'}</label>
                    <textarea class="form-control" id="note_bdl" name="note_bdl" rows="2"></textarea>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-row">

                <div class="col-md-2">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><span class="title_box center_text">{l s='No charge'}</span></th>
                            </tr>
                        </thead>
                        <tbody class="center">
                            <tr>
                                <td><input class="super_center" onchange="verifica_contabilita('nessun_addebito')" type="checkbox" name="nessun_addebito" id="nessun_addebito" value="1" {if $bdl['buoni']['nessun_addebito'] == "1"} checked="checked"{/if}></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="col-md-1">
                </div>

                <div class="col-md-4" id="div_contab1">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><span class="title_box center_text">{l s='Paid'}</span></th>
                                <th><span class="title_box center_text">{l s='Unpaid'}</span></th>
                                <th><span class="title_box center_text">{l s='Billed'}</span></th>
                                <th><span class="title_box center_text">{l s='To be invoiced'}</span></th>
                            </tr>
                        </thead>
                        <tbody class="center">
                            <tr>
                                <td><input class="super_center" onchange="verifica_contabilita('pagato')" type="checkbox" name="pagato" id="pagato" value="1" /></td>
                                <td><input class="super_center" onchange="verifica_contabilita('da_pagare')" type="checkbox" name="da_pagare" id="da_pagare" value="1" checked="checked" /></td>
                                <td><input class="super_center" onchange="verifica_contabilita('fatturato')" type="checkbox" name="fatturato" id="fatturato" value="1" /></td>
                                <td><input class="super_center" onchange="verifica_contabilita('da_fatturare')" type="checkbox" name="da_fatturare" id="da_fatturare" value="1" checked="checked" /></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="col-md-1">
                </div>

                <div class="col-md-2" id="div_contab2">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><span class="title_box center_text">{l s='Send for check'} {* Questo campo va flaggato solo quando si compila un BDL per un'attività non inclusa in un ordine. Il campo serve per inviare il BDL a Ezio in modo tale che Ezio lo possa vistare per l'invio in contabilità. Se il BDL riguarda un'attività già contemplate nell'ordine, non flaggarlo: il sistema opererà in automatico. *}
                                        <span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="This field should only be flagged when completing a BDL for an activity not included in an order. The field is used to send the BDL to Ezio so that Ezio can endorse it for sending to accounting. If the BDL concerns an activity already covered in the order, do not flag it: the system will operate automatically.">
                                            <i class="icon-question-sign"></i>
                                        </span>
                                    </span>
                                </th>
                            </tr>
                        </thead>
                        <tbody class="center">
                            <tr>
                                <td><input class="super_center" type="checkbox" name="invio_controllo" id="invio_controllo" /></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="col-md-1">
                </div>

                {* Correggere il check invio_contabilita *}
                <input type="checkbox" style="display:none" id="invio_contabilita">

            </div>
        </div>

        <hr />
        <input class="btn btn-primary" type="submit" name="bdl_save" value="{l s='Save'}">
        
    </div>
</form>

{* Javascript della pagina *}
{include file="./bdl_add_js.tpl"}