{if $customer->id_default_group != 2}
    <form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" name="persone" novalidate>
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-file"></i> {l s='People'}
                {if count($persone) != 0 OR TRUE}
                <span class="badge" style="border-radius:0px;">
                    {count($persone)}
                </span>
                {/if}
                <span class="rosso" id="edit_title"></span>

                <div class="panel-heading-action">
                    <div class="bottoni-tab">
                        <button type="button" class="btn btn-default testo-bottoni" onclick="add_persone()" {*href="{$current|escape:'html':'UTF-8'}&amp;addcustomer&amp;id_customer_associate={$customer->id|intval}&amp;token={getAdminToken tab='AdminCustomers'}&amp;back={$smarty.server.REQUEST_URI|urlencode}"*}>
                            <i class="icon-plus-sign"></i>
                            {l s='Add'}
                        </button>
                        <button class="btn btn-default testo-bottoni" type="submit" id="submitpersone1" name="submitpersone">
                            <i class="icon-save"></i>
                            {l s='Save'}
                        </button>
                    </div>
                </div>
            </div>
            
            {if count($persone)}
                <div id="persone_view">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="center"><span class="title_box ">{l s='ID'}</span></th>
                                <th><span class="title_box">{l s='Name'}</span></th>
                                <th><span class="title_box">{l s='Surname'}</span></th>
                                <th><span class="title_box">{l s='Role'}</span></th>
                                <th><span class="title_box">{l s='Telephone 1'}</span></th>
                                <th><span class="title_box">{l s='Telephone 2'}</span></th>
                                <th><span class="title_box">{l s='Mobile Phone'}</span></th>
                                <th><span class="title_box">{l s='Email'}</span></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="tablePersoneBody">

                        {foreach $persone AS $key => $persona}
                            <tr id="tr_persona_{$persona['id_persona']}">

                                <td style="text-align: center;"><input type="hidden" name="persona_nuovo[{$persona['id_persona']}]" value="0" /> {$persona['id_persona']}</td>

                                <td><input type="text" id="persona_firstname[{$persona['id_persona']}]" name="persona_firstname[{$persona['id_persona']}]" value="{$persona['firstname']}"></td>

                                <td><input type="text" id="persona_lastname[{$persona['id_persona']}]" name="persona_lastname[{$persona['id_persona']}]" value="{$persona['lastname']}"></td>

                                <td><input type="text" id="persona_role[{$persona['id_persona']}]" name="persona_role[{$persona['id_persona']}]" value="{$persona['role']}"></td>

                                <td><input onkeyup="this.value = this.value.replace(/\s+/g,'');" id="persona_phone[{$persona['id_persona']}]" name="persona_phone[{$persona['id_persona']}]" value="{$persona['phone']}" size="10" {if $persona['no_phone'] == 1}style="font-style: italic;"{/if}>{if $persona['phone'] != ""} <a href="CALLTO::{$persona['phone']}"><i class="icon-phone"></i></a>{/if}</td>

                                <td><input onkeyup="this.value = this.value.replace(/\s+/g,'');" id="persona_phone_2[{$persona['id_persona']}]" name="persona_phone_2[{$persona['id_persona']}]" value="{$persona['phone_2']}" size="10" {if $persona['no_phone_2'] == 1}style="font-style: italic;"{/if}>{if $persona['phone_2'] != ""} <a href="CALLTO::{$persona['phone_2']}"><i class="icon-phone"></i></a>{/if}</td>

                                <td><input onkeyup="this.value = this.value.replace(/\s+/g,'');" id="persona_phone_mobile[{$persona['id_persona']}]" name="persona_phone_mobile[{$persona['id_persona']}]" value="{$persona['phone_mobile']}" size="10" {if $persona['no_phone_mobile'] == 1}style="font-style: italic;"{/if}>{if $persona['phone_mobile'] != ""} <a href="CALLTO::{$persona['phone_mobile']}"><i class="icon-phone"></i></a>{/if}</td>

                                <td><input type="text" id="persona_email[{$persona['id_persona']}]" name="persona_email[{$persona['id_persona']}]" value="{$persona['email']}" {if $persona['no_email'] == 1}style="font-style: italic;"{/if}></td>

                                <td>
                                    {if $persona['messaggio'] != ''}
                                    <a class="btn btn-default squadrato" id="tr_mess_button_{$persona['id_persona']}" title="Invia messaggio" href="{$link_gen_base}tab_name=actions&amp;azione=actions&amp;viewmessage&amp;aprinuovomessaggio&amp;id_persona={$persona['id_persona']}{$token_base}">
                                        <i class='icon-envelope'></i>
                                    </a>
                                    {/if}
                                    {if $persona['email']}
                                    <a class="btn btn-default squadrato" id="invio_mail_outlook_t_{$persona['id_persona']}" title="Invia e-mail" href="mailto:{$persona['email']}">
                                        <img src='../img/admin/outlook.gif' alt='Invia e-mail' title='Invia e-mail' style='cursor:pointer' />
                                    </a>
                                    {/if}
                                    {if $persona['cancella']}
                                    <a class="btn btn-danger squadrato" id="tr_delete_button_{$persona['id_persona']}" title="Cancella" onclick="if(confirm('Sei sicuro?')){literal} { {/literal} togli_riga({$persona['id_persona']}); cancella_persona({$persona['id_persona']}); {literal} } {/literal}">
                                        <i class='icon-trash'></i>
                                    </a>
                                    {/if}
                                    {*
                                    <a class="btn btn-default squadrato" id="tr_save_button_{$persona['id_persona']}" title="Salva">
                                        <i class='icon-save'></i>
                                    </a>
                                    *}
                                </td>
                                
                            </tr>
                            
                            {*
                            <tr id="tr_edit_{$persona['id_persona']}" style="display: none;">
                                <td>{$persona['id_persona']}</td>
                                <td><input value="{$persona['firstname']}"></td>
                                <td><input value="{$persona['lastname']}"></td>
                                <td><input value="{$persona['role']}"></td>
                                <td><input value="{$persona['phone']}"></td>
                                <td><input value="{$persona['phone_2']}"></td>
                                <td><input value="{$persona['phone_mobile']}"></td>
                                <td><input value="{$persona['email']}"></td>

                                <td>
                                    <a class="btn btn-default squadrato" >
                                        <i class='icon-save'></i>
                                    </a>
                                </td>
                            </tr>
                            *}



                        {/foreach}
                        </tbody>
                    </table>
                    <input type="hidden" id="numval" name="numval" value="{count($persone)}">
                </div>
                
                <hr />
                
                <div class="row">
                    <div class="col-lg-12">
                        <button type="submit" id="submitpersone2" name="submitpersone" class="btn btn-default pull-right">
                            <i class="icon-save"></i>
                            {l s='Save'}
                        </button>
                    </div>
                </div>

                {*
                <div id="persone_edit" style="display: none;">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="center"><span class="title_box ">{l s='ID'}</span></th>
                                <th><span class="title_box">{l s='Name'}</span></th>
                                <th><span class="title_box">{l s='Surname'}</span></th>
                                <th><span class="title_box">{l s='Role'}</span></th>
                                <th><span class="title_box">{l s='Telephone 1'}</span></th>
                                <th><span class="title_box">{l s='Telephone 2'}</span></th>
                                <th><span class="title_box">{l s='Mobile Phone'}</span></th>
                                <th><span class="title_box">{l s='Email'}</span></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                        {foreach $persone AS $key => $persona}
                            <tr>
                                <td><input value="{$persona['id_persona']}"></td>
                                <td>{$persona['firstname']}</td>
                                <td>{$persona['lastname']}</td>
                                <td>{$persona['role']}</td>
                                <td {if $persona['no_phone'] == 1}style="font-style: italic;"{/if}>{$persona['phone']}{if $persona['phone'] != ""} <a href="CALLTO::{$persona['phone']}"><i class="icon-phone"></i></a>{/if}</td>
                                <td {if $persona['no_phone_2'] == 1}style="font-style: italic;"{/if}>{$persona['phone_2']}{if $persona['phone_2'] != ""} <a href="CALLTO::{$persona['phone_2']}"><i class="icon-phone"></i></a>{/if}</td>
                                <td {if $persona['no_phone_mobile'] == 1}style="font-style: italic;"{/if}>{$persona['phone_mobile']}{if $persona['phone_mobile'] != ""} <a href="CALLTO::{$persona['phone_mobile']}"><i class="icon-phone"></i></a>{/if}</td>
                                <td {if $persona['no_email'] == 1}style="font-style: italic;"{/if}>{$persona['email']}</td>

                                <td>
                                    <a class="btn btn-default squadrato" >
                                        <i class='icon-save'></i>
                                    </a>
                                    </a>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
                *}
                
            {else}
            <p class="text-muted text-center">
                {*l s='%1$s %2$s has not placed any orders yet' sprintf=[$customer->firstname, $customer->lastname]*}
                {l s='There are no associated people'}
            </p>
            {/if}
        </div>
    </form>
    {/if}


{* Javascript della pagina *}
{include file="./js/persone_js.tpl"}