<script>

window.onload = function() {
    // dev only
    //f_edit_btn();
    //alert(controllaCF("BRSLCU96R12B832E"));
    //alert(sdi_codes[0][0]);
    //tata.success('Test alto', 'Test 2');
};

/* Note private -> bug: non funziona la modifica di una nota appena creata */

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
	var actions = $("td.azioni_note").html();

    $(".add-new").click(function(){
		$(this).attr("disabled", "disabled");
		var index = $("table.tabella_note > tbody > tr:last-child").index();
        var row = '<tr>' +
            //'<td><input type="text" class="form-control" name="nuova_nota" id="nuova_nota"></td>' +
            '<td><textarea class="form-control" name="nuova_nota" id="nuova_nota"></textarea></td>' +
            '<td>{$mio_nome}</td>' +
            '<td>{$today}</td>' +
			'<td>' + actions + '</td>' +
        '</tr>';
    	$("table.tabella_note").append(row);
		$("table.tabella_note > tbody > tr").eq(index + 1).find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip();
    });

	$(document).on("click", ".add", function(){
		var empty = false;
        var dbid = $(this).parents("tr").attr('dbid');
		//var input = $(this).parents("tr").find('input[type="text"]');
		var input = $(this).parents("tr").find('textarea');
        var questo = this;
        input.each(function(){
			if(!$(this).val()){
				$(this).addClass("error");
				empty = true;
			} else{
                $(this).removeClass("error");
            }
		});
		$(this).parents("tr").find(".error").first().focus();
		if(!empty){
			input.each(function(){
				$(this).parent("td").html($(this).val());
                if(dbid === undefined){
                    $.ajax({
                        url:"ajax.php?creanota_customer=y",
                        type: "POST",
                        data: { 
                            id_customer: {$customer->id},
                            nota: $(this).val()
                        },
                        success:function(data){
                            // inserimento in tab dell'id nota creata
                            $(questo).parents("tr").attr('dbid', data);
                            tata.success('Nota creata con successo', 'ID '+data);
                        },
                        error: function(xhr,stato,errori){
                            tata.error('Errore nella creazione', xhr.status);
                        }
                    });
                } else {
                    $.ajax({
                        url:"ajax.php?modificanota_customer=y",
                        type: "POST",
                        data: { 
                            id_nota: dbid,
                            nota: $(this).val()
                        },
                        success:function(){
                            tata.success('Nota modificata con successo', '');
                            succ = true;
                        },
                        error: function(xhr,stato,errori){
                            tata.error('Errore nella modifica', xhr.status);
                        }
                    });
                }
			});			
			$(this).parents("tr").find(".add, .edit").toggle();
			$(".add-new").removeAttr("disabled");
		}		
    });

    $(function() {
        $("input").keypress(function(event) {
            var $self = $(this);
            var comm = $self.val();

            var keycode = (event.keycode ? event.keycode : event.which);
            if (keycode == 13) {
                var comm = $self.val();
                alert(comm);
                event.stopPropagation();
            }
        });
    });

	$(document).on("click", ".edit", function(){		
        $(this).parents("tr").find("td:not(:last-child)").each(function(){
            if($(this).hasClass('mod')){ 
                //$(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">'); 
                $(this).html('<textarea class="form-control" style="height:80px;" >' + $(this).text() + '</textarea>'); 
            }
			
		});		
		$(this).parents("tr").find(".add, .edit").toggle();
		$(".add-new").attr("disabled", "disabled");
    });

	$(document).on("click", ".delete", function(){
        var dbid = $(this).parents("tr").attr('dbid');
        var succ = false;
        var questo = this;
        if(dbid === undefined){
            $(this).parents("tr").remove();
            $(".add-new").removeAttr("disabled");
        } else {
            if(confirm("Vuoi cancellare la nota con id "+dbid+" ?")){
                $.ajax({
                    url:"ajax.php?cancellanota_customer=y",
                    type: "POST",
                    data: { 
                        cancella_nota: 'y',
                        id_nota: dbid
                    },
                    success:function(){
                        tata.success('Nota cancellata con successo', '');
                        succ = true;
                        $(questo).parents("tr").remove();
                        $(".add-new").removeAttr("disabled");
                    },
                    error: function(xhr,stato,errori){
                        tata.error('Errore nella cancellazione', xhr.status);
                    }
                });
            }
        }
        /*
        if(succ == true){
            $(this).parents("tr").remove();
            $(".add-new").removeAttr("disabled");
            alert("eliminattoh");
        }

        $(this).parents("tr").remove();
		$(".add-new").removeAttr("disabled");
        */
    });
});

{literal}

function controlla_fisco(cf){
    var res = controllaCF(cf);
    switch(res){
        case 1:
        cf_corretto();
        break;

        case 2:
        piva_corretta();
        break;

        default:
        cf_errato(res);
    }
}

function controlla_sdi(sdi){
    var res = cercaSDI(sdi);
    switch(res){
        case 1:
        "SDI Non trovato"
        document.getElementById("sdi_span").className = "giallo";
        document.getElementById("sdi_span").innerHTML = "Codice non trovato";
        break;

        case 2:
        document.getElementById("sdi_span").className = "rosso";
        document.getElementById("sdi_span").innerHTML = "Codice Errato";
        break;

        case 3:
        document.getElementById("sdi_span").innerHTML = "";
        break;

        default:
        document.getElementById("sdi_span").className = "verde";
        document.getElementById("sdi_span").innerHTML = "Trovato: " + res;
        
    }
}

function cercaSDI(cod){
    if(cod.length != 0){
        if(cod.length == 7){
            for (var i = 0; i < sdi_codes.length; i++) {
                if(sdi_codes[i][1] == cod){
                    return sdi_codes[i][0];
                }
            }
            return 1;
        } else {
            return 2;
        }
    } else {
        return 3;
    }
}

function normalize(cf){
    return cf.replace(/\s/g, "").toUpperCase();
}


function controllaCF(p){
    if( p.length == 0 )
        return "";
    cf = p.replace(/\s/g, "").toUpperCase();

    if(cf.length == 11){
        //alert("Controllare PIVA");
        return controllaPIVA(p);
    } else {
        if( ! /^[0-9A-Z]{16}$/.test(cf) )
			return "CF: Caratteri non validi";
		var s = 0;
		var even_map = "BAFHJNPRTVCESULDGIMOQKWZYX";
		for(var i = 0; i < 15; i++){
			var c = cf[i];
			var n = 0;
			if( "0" <= c && c <= "9" )
				n = c.charCodeAt(0) - "0".charCodeAt(0);
			else
				n = c.charCodeAt(0) - "A".charCodeAt(0);
			if( (i & 1) === 0 )
				n = even_map.charCodeAt(n) - "A".charCodeAt(0);
			s += n;
		}
		if( s%26 + "A".charCodeAt(0) !== cf.charCodeAt(15) )
			return "CF: Carattere controllo errato";
		return 1;

    }


    /*
    var pattern = new RegExp('^[a-zA-Z]{6}[0-9]{2}[a-zA-Z][0-9]{2}[a-zA-Z][0-9]{3}[a-zA-Z]$');

    if (pattern.test(res)){
        alert("cf errato");
    } else {
        alert("cf forse corretto");
    }
    */

}

function controllaPIVA(p){
    pi = p.replace(/\s/g, "").toUpperCase();
    if( pi.length === 0 )
        return "";
    else if( pi.length !== 11 )
        return "P.IVA: lunghezza errata";
    if( ! /^[0-9]{11}$/.test(pi) )
        return "P.IVA: caratteri non validi";
    var s = 0;
    for(var i = 0; i < 11; i++ ){
        var n = pi.charCodeAt(i) - "0".charCodeAt(0);
        if( (i & 1) === 1 ){
            n *= 2;
            if( n > 9 )
                n -= 9;
        }
        s += n;
    }
    if( s % 10 !== 0 )
        return "P.IVA: carattere controllo errato";
    return 2;
}



function cf_corretto(){
    document.getElementById("cf_span").className = "verde";
    document.getElementById("cf_span").innerHTML = "CF: Corretto";
}

function piva_corretta(){
    document.getElementById("cf_span").className = "verde";
    document.getElementById("cf_span").innerHTML = "P.IVA: Corretta";
}

function cf_errato(s){
    document.getElementById("cf_span").className = "rosso";
    document.getElementById("cf_span").innerHTML = s;
}

{/literal}


/* NOTE PRIVATE */

// Funzioni 1.4 -> per usarle vanno corretti gli id/name in anagrafica.tpl
/*
function add_nota_privata() {
    var possible = "0123456789";
    var randomid = "";
    for( var i=0; i < 11; i++ ) {
        randomid += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    
    // auto_grow si può togliere (la funzione è vuota?)
    $("
    <tr id='tr_note_"+randomid+"'>
        <td>
            <textarea class='textarea_note' name='note_nota[]' onkeyup='auto_grow(this)' id='note_nota[]' style='width:540px;height:16px'></textarea>
            <input type='hidden' name='note_nuova[]' value='1' />
        </td>
        <td>
            <input type='text' readonly='readonly' name='note_id_employee[]' value='{$mio_nome}' />
        </td>
        <td>
            <input type='text' readonly='readonly' name='note_date_add[]' value=''.date('d/m/Y').'' />
        </td>
        <td>
            <a href='javascript:void(0)' onclick='togli_nota("+randomid+");'><img src='../img/admin/delete.gif' alt='Cancella' title='Cancella' style='cursor:pointer' /></a>
        </td>
    </tr>").appendTo("#tableNoteBody");
    
    $(".textarea_note").each(function () {
        this.style.height = ((this.scrollHeight)-4)+"px";
    });
    
    $(".textarea_note").on("change keyup keydown paste cut", "textarea", function () {
        $(this).height(0).height(this.scrollHeight);
    }).find("textarea").change();
    
    autosize($(".textarea_note"));
}
*/
    
function togli_nota(id) {
    document.getElementById('tr_note_'+id).innerHTML = "";
}
    
function cancella_nota(id) {
    $.ajax({
        url:"ajax.php?cancellanota_customer=y",
        type: "POST",
        data: { 
            cancella_nota: 'y',
            id_note: id
        },
        success:function(){
            alert("Nota cancellata con successo"); 
        },
        error: function(xhr,stato,errori){
            alert("Errore nella cancellazione:"+xhr.status);
        }
    });
}

/* 1.6 da correggere */
function add_riga(){

	var parent = document.getElementById("nuove-note");
	/*
	//figlio di id="nuove-note"
	var row = document.createElement('div');
	row.className = "row";

	
	//figlio di div row
	var form_group = document.createElement('div');
	form_group.className = "form-group col-md-2";

	//figli di div form-group
	var label = document.createElement('label');\
	label.innerHTML = "Altri fornitori";
	var elem = document.querySelector('#nota_privata_cpy');
	var select = elem.cloneNode(true);
	// end

	form_group.appendChild(label);
	form_group.appendChild(select);

	row.appendChild(form_group);
    */

    var elem = document.querySelector('#nota_privata_cpy');
	var row = elem.cloneNode(true);
	parent.appendChild(row);
}

function rem_riga(){
	var parent = document.getElementById("nota_privata_cpy");
	parent.removeChild(parent.lastElementChild);
}

function f_edit_btn(){
    if(document.getElementById("edit_adm").value == 0) {
        // modalità di edit abilitata
        document.getElementById("edit_adm").value = 1;
        document.getElementById("edit_title").innerHTML = "(MODIFICA)";
        document.getElementById("edit_btn").style.display = "none";
        document.getElementById("edit_section").style.display = "block";
        //document.getElementById("save_btn").style.display = "block";

        abilita_tutto();
    } else {
        // disattiva la modalità di edit, in realtà forse sarebbe meglio un refresh per far annullare le modifiche al form
        //window.location.reload();
    }
}

function f_reset_btn() {
    resetta_tutto();
}

//Abilita tutto quello che c'è da abilitare per il form di modifica
function abilita_tutto() {
    abi("nome_azienda");
    abi("nome");
    abi("cognome");
    abi("vat_number");
    abi("tax_code");
    abi("email");
    abi("pec");
    abi("telefono_principale");
    abi("cellulare_principale");
    abi("fax");
    abi("passwd");
    abi("codice_univoco");
    abi("ipa");
    abi("tax_regime");
    abi("employees_number");
    abi("settore");
    //abi("categoria");
    abi("canale");
    abi("ultimo_contatto");
    abi("settore");
    abi("profilo");
    abi_fnc_all();
}


function abi_fnc_all(){
    abi_compleanno();
    abi_tipo_cliente();
    abi_fornitore();
    abi_cliente_risk();
    abi_notifiche_email();
    abi_genere()
    abi_status();
    abi_consensi();
}

function abi_compleanno(){
    document.getElementById("compleanno_let").style.display = "none";
    document.getElementById("compleanno_mod").style.display = "block";
}

function abi_tipo_cliente(){
    document.getElementById("tipo_cliente_let").style.display = "none";
    document.getElementById("tipo_cliente_mod").style.display = "block";
}

function abi_fornitore(){
    document.getElementById("fornitore_let").style.display = "none";
    document.getElementById("fornitore_mod").style.display = "block";
}

function abi_cliente_risk(){
    document.getElementById("cliente_risk_let").style.display = "none";
    document.getElementById("cliente_risk_mod").style.display = "block";
}

function abi_notifiche_email(){
    document.getElementById("notifiche_email_let").style.display = "none";
    document.getElementById("notifiche_email_mod").style.display = "block";
}

function abi_genere(){
    document.getElementById("genere_let").style.display = "none";
    document.getElementById("genere_mod").style.display = "block";
}

function abi_status(){
    document.getElementById("status_let").style.display = "none";
    document.getElementById("status_mod").style.display = "block";
}

function abi_consensi(){
    document.getElementById("consensi_let").style.display = "none";
    document.getElementById("consensi_mod").style.display = "block";
}


/*

    $("#cf-validate").submit(function(e){
        e.preventDefault();
        $("#cf-validate #cf-3").removeClass('is-valid is-invalid');
        let cf = $("#cf-validate #cf-3").val();
        if(CodiceFiscale.check(cf)){
            $("#cf-validate #cf-3").addClass('is-valid');
        } else {
            $("#cf-validate #cf-3").addClass('is-invalid');
        }
        
    });


    function controlla_fisco(tipo){
        if(tipo == "cf"){
            if(ControllaCF(document.getElementById("tax_code"))){
                cf_corretto();
            } else {
                cf_errato();
            }
        } else if(tipo == "piva"){

        }
    }



    function ControllaCF(cf) {
        if (cf.length < 12) {
            return ControllaPIVA(cf)
        } else {
        var validi, i, s, set1, set2, setpari, setdisp;
        if (cf == '') return '';
        //cf = cf.toString().toUpperCase();
        if (cf.length != 16)
            return false;
        validi = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        for (i = 0; i < 16; i++) {
            if (validi.indexOf(cf.charAt(i)) == -1)
                return false;
        }
        set1 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        set2 = "ABCDEFGHIJABCDEFGHIJKLMNOPQRSTUVWXYZ";
        setpari = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        setdisp = "BAKPLCQDREVOSFTGUHMINJWZYX";
        s = 0;
        for (i = 1; i <= 13; i += 2)
            s += setpari.indexOf(set2.charAt(set1.indexOf(cf.charAt(i))));
        for (i = 0; i <= 14; i += 2)
            s += setdisp.indexOf(set2.charAt(set1.indexOf(cf.charAt(i))));
        if (s % 26 != cf.charCodeAt(15) - 'A'.charCodeAt(0))
            return false;
        return true;
        }
    }


    function ControllaPIVA(pi) {
        if (pi == '') return '';
        if (pi.length != 11)
            return "La lunghezza della partita IVA non è\n" +
                "corretta: la partita IVA dovrebbe essere lunga\n" +
                "esattamente 11 caratteri.\n";
        validi = "0123456789";
        for (i = 0; i < 11; i++) {
            if (validi.indexOf(pi.charAt(i)) == -1)
                return "La partita IVA contiene un carattere non valido `" +
                    pi.charAt(i) + "'.\nI caratteri validi sono le cifre.\n";
        }
        s = 0;
        for (i = 0; i <= 9; i += 2)
            s += pi.charCodeAt(i) - '0'.charCodeAt(0);
        for (i = 1; i <= 9; i += 2) {
            c = 2 * (pi.charCodeAt(i) - '0'.charCodeAt(0));
            if (c > 9) c = c - 9;
            s += c;
        }
        if ((10 - s % 10) % 10 != pi.charCodeAt(10) - '0'.charCodeAt(0))
            return "La partita IVA non è valida:\n" +
                "il codice di controllo non corrisponde.\n";
        return "";
    }




function controllaCF() {
    var cfi = document.getElementById("tax_code");
    var cf = cfi.toString().toUpperCase();
    var cfReg = /^[A-Z]{6}\d{2}[A-Z]\d{2}[A-Z]\d{3}[A-Z]$/;
    if (!cfReg.test(cf)){
        document.getElementById("cf_span").className = "rosso";
        document.getElementById("cf_span").innerHTML = "CF Errato1"
    }
    
    var set1 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var set2 = "ABCDEFGHIJABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var setpari = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var setdisp = "BAKPLCQDREVOSFTGUHMINJWZYX";
    var s = 0;
    for( i = 1; i <= 13; i += 2 )
        s += setpari.indexOf( set2.charAt( set1.indexOf( cf.charAt(i) )));
    for( i = 0; i <= 14; i += 2 )
        s += setdisp.indexOf( set2.charAt( set1.indexOf( cf.charAt(i) )));
    if ( s%26 != cf.charCodeAt(15)-'A'.charCodeAt(0) ){
        document.getElementById("cf_span").className = "rosso";
        document.getElementById("cf_span").innerHTML = "CF Errato2"
    } else {
        document.getElementById("cf_span").className = "verde";
        document.getElementById("cf_span").innerHTML = "CF Corretto"
    }
}
*/




function disabilita_tutto() {

}

function resetta_tutto() {
    window.location.reload();
}

function abi(obj){
    document.getElementById(obj).disabled = false;
    document.getElementById(obj).classList.remove("lettura");
}

function rst(obj) {
    // non funzionante ?
    document.getElementById(obj).reset();
}

function hasClass(ele, cls) {
    return ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
}
function addClass(ele, cls) {
    if (!hasClass(ele, cls)) ele.className += " " + cls;
}
function removeClass(ele, cls) {
    if (hasClass(ele, cls)) {
        var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
        ele.className = ele.className.replace(reg, ' ');
    }
}



var sdi_codes =[
    ['Aruba', 'KRRH6B9'],
    ['Wolters Kluwer', 'W7YVJK9'],
    ['Fattura PA by Passepartout', '5RUO82D'],
    ['Fatture in Cloud by TeamSystem', 'M5UXCR1'],
    ['Zucchetti', 'SUBM70N'],
    ['Register', 'PZIJH2V'],
    ['Agyo', 'KUPCRMI'],
    ['ARXivar', 'A4707H7'],
    ['WebClient', 'T04ZHR3'],
    ['Sistemi', 'USAL8PV'],
    ['Buffetti', 'BA6ET11'],
    ['CGN by RDV Network', 'SU9YNJA'],
    ['Bluenext', 'X2PH38J'],
    ['SEAC', 'P62QHVQ'],
    ['FatturaOnClick.it', 'WY7PJ6K'],
    ['ARCHIVIA.ONLINE', 'WP7SE2Q'],
    ['Datev Koinos', 'T9K4ZHO'],
    ['Var Group', 'M5ITOJA'],
    ['Credemtel (gruppo Banca Credem)', 'MZO2A0U'],
    ['Infocert', 'XL13LG4'],
    ['SataNet', 'SA0PL6Q'],
    ['Fattura24', 'SZLUBAI'],
    ['FattureGB', 'QULXG4S'],
    ['FtPA', '6EWHWLT'],
    ['Digithera', 'URSWIEX'],
    ['Ksg', 'TRS3OH9'],
    ['DocEasy', 'J6URRTW'],
    ['Metodo', 'W4KYJ8V'],
    ['QuickMastro', 'KJSRCTG'],
    ['Fattura Elettronica APP', 'N92GLON'],
    ['Archivium srl', '3ZJY534'],
    ['IDOCTORS', 'NKNH5UQ'],
    ['Extreme software', 'E2VWRNU'],
    ['Arthur Informatica', 'G4AI1U8'],
    ['Danisoft', 'G1XGCBG'],
    ['MySond', 'H348Q01'],
    ['OLSA Informatica', 'XIT6IP5'],
    ['Unimatica', 'E06UCUD'],
    ['Mustweb Srl', 'P4IUPYH'],
    ['Mustweb srl', '2LCMINU'],
    ['Coldiretti', '5W4A8J1'],
    ['Tech Edge', '0G6TBBX'],
    ['Cia', '6RB0OU9'],
    ['Consorzio CIAT', 'AU7YEU4'],
    ['Alto Trevigiano Servizi', 'C1QQYZR'],
    ['EdiSoftware Srl', 'EH1R83N'],
    ['Datalog Italia Srl', 'G9HZJRW'],
    ['Ediel', 'HHBD9AK'],
    ['Youdox', 'K0ROACV'],
    ['Edok Srl', 'MJEGRSK'],
    ['Credemtel', 'MZO2AOU'],
    ['Kalyos', 'N9KM26R'],
    ['?',	'PR4AG6C'],
    ['B2B easy', 'PXQYICS'],
    ['Intesa San Paolo', 'RWB54P8'],
    ['Ready Pro', 'G4AI1U8'],
    ['Gruppo Segesta', 'SN4CSRI'],
    ['Mister Software', 'SU1UTOG'],
    ['T-Max Service - GeCo', '5D6C7RC'],
    ['Trust Technologies', 'TRTSWMZ'],
    ['Modula3', 'TULURSB'],
    ['Finson', 'UE2LXTM'],
    ['?',	'W840XLE'],
    ['FattApp Cloud Services', 'WHP7LTE'],
    ['OK Copy Italia SRL', 'X46AXNR'],
    ['InTouch SRL', 'YRXHCLN'],
    ['?',	'C4AL1U8'],
    ['?',	'ORIEHPO'],
    ['CloudFinance', '6JXPS2J'],
    ['?',	'UJJIIX9'],
    ['InfoCamere', '66OZKW1'],
    ['IDEA SOFTWARE', 'PPX7BLB'],
    ['?',	'OB9MX5S'],
    ['?',	'C3UCNRB'],
    ['SeDiCo Servizi', 'G9YK3BM'],
    ['MultiWire', 'GR2P7ZP'],
    ['2C Solution', 'T04ZHR3'],
    ['?',	'U21NEXA'],
    ['EdiGest', 'UNI0W8G'],
    ['LaPam', 'XMXAUP4'],
    ['?',	'AGZS6TU'],
    ['Gestionale Amica', 'M62SGNV'],
    ['Ifin', 'MJ1OYNU'],
    ['OrisLine', 'DUDU0GE'],
    ['YouDox', 'KGVVJ2H'],
    ['FatturaPaPerTutti', 'WH2KO8I'],
    ['?',	'Q15GJ0Z'],
    ['?',	'L8WH30T'],
    ['RM-Tech', 'Y4BUAV4'],
    ['Centro software', '7HE8RN5'],
    ['SIWEB', 'RTVLCR1'],
    ['SubitoFattura', 'ISHDUAE'],
    ['Smart Vet', '0KDMVIB'],
    ['MyFiscalCloud', 'I347Y6N'],
    ['Mediatica', '38P86EY'],
    ['FerServizi', 'RYRNP0U'],
    ['?',	'7J6LNAP'],
    ['?',	'9MTMAR3'],
    ['?',	'G9H7JRW'],
    ['Olsa Informatica', 'QDZCM9N'],
    ['?',	'5KQRP7D'],
    ['?',	'39QMOPD'],
    ['?',	'JX8OYTO'],
    ['?',	'ARBTDQ3'],
    ['?',	'94QLT54'],
    ['?',	'NVXARUR'],
    ['GRUPPO CMT', '5P3UNVR'],
    ['Puntanet', 'P83CKOC'],
    ['?',	'ZE7RB0G'],
    ['Assist Informatica Srl', 'DXEBYTP'],
    ['?',	'7G984OP'],
    ['Entaksi', 'G7Q6SPJ'],
    ['?',	'0ZCQR4A'],
    ['?',	'K1L1030'],
    ['?',	'RRG48KY'],
    ['SPSOFT', 'SNT102H'],
    ['Day Ristoservice', 'USA39RA'],
    ['?',	'4157ZO4'],
    ['ANDXOR', 'PAXCCYU'],
    ['Faber System', 'XWJKNZD'],
    ['Profarma', 'QLDR2VY '],
    ['?',	'M5OR0P'],
    ['Mistral di Blusys', 'AO3AEUZ'],
    ['?',	'5YOORSD'],
    ['?',	'ZS100U1'],
    ['?',	'TV195KG'],
    ['?',	'T9U85V9'],
    ['Memory Srl', 'MRCC2DY'],
    ['Olivetti', 'OLCJ82K'],
    ['Olivetti dal 01.03.2020','O8L2VB7'],
    ['?',	'SR77I32'],
    ['?',	'ACTKBBZ'],
    ['Multidialogo', 'JHBM40P'],
    ['Fattutto', 'ROINDUX'],
    ['?',	'RXMENAR'],
    ['?',	'P02CVB5'],
    ['?',	'OUD4SW5'],
    ['?',	'BMWX7J9'],
    ['SkyNet', 'CEORGIG'],
    ['J-Software', 'BLY9JDQ'],
    ['?',	'KXNANZA'],
    ['Unione Agricoltori e Coltivatori Diretti Sudtirolesi', '7035UR5'],
    ['?',	'XN6NBML'],
    ['Confartigianato', 'B66HAMY'],
    ['?',	'I9E3907'],
    ['TRUST INVOICE', 'TPICRCA'],
    ['IEO Informatica', 'I6VXTJA'],
    ['B Console', '8CQGKGJ'],
    ['?',	'UO5NS3A'],
    ['?',	'YMUS82V'],
    ['zerogestionale.it', 'JC7P1DW'],
    ['?',	'HRRH689'],
    ['ondata.cloud', 'KBRM7PS'],
    ['Federfarma', 'RN5Y3PI'],
    ['?',	'LX4U005'],
    ['NADIRSOFTWARE', '596NUAX'],
    ['CofferWeb', 'WNK4HCP'],
    ['PegasoGest', 'RNMN7NC'],
    ['?',	'XLU1JTE'],
    ['Omicron Sistemi', 'QYISEC3'],
    ['Savino Solution S.R.L.', 'ZCK6XHR'],
    ['Mediainx', '4ADX8V9'],
    ['?',	'8C9X0IZ'],
    ['TOKEM S.R.L.', 'HQSIB42'],
    ['ITALSOFT', 'SKUA8Y6'],
    ['Var Group spa', '3G3OPYL'],
    ['?',	'ZBGTBLC'],
    ['?',	'32C3O8A'],
    ['Fattura 1 Click', 'OCCDHSV'],
    ['?',	'3RB98ZT'],
    ['?',	'TA0WO4S'],
    ['?',	'49O3A8R'],
    ['?',	'KBTEL2G']
];

</script>