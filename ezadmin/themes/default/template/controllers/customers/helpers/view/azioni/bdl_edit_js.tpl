<script>

window.onload = function() {
    primo_avvio();
};

function primo_avvio(){
    f_contratto_assistenza();
    f_manutenzione_ordinaria();
    f_manutenzione_straordinaria();
}

function verifica_contabilita(cb){
    var val = document.getElementById(cb).checked;

    //alert(val);

    switch(cb){
        case "pagato":
            if(val){
                document.getElementById("da_pagare").checked = false;
            }else{
                document.getElementById("da_pagare").checked = true;
            }
        break;

        case "da_pagare":
            if(val){
                document.getElementById("pagato").checked = false;
            }else{
                document.getElementById("pagato").checked = true;
            }
        break;

        case "fatturato":
            if(val){
                document.getElementById("da_fatturare").checked = false;
            }else{
                document.getElementById("da_fatturare").checked = true;
            }
        break;

        case "da_fatturare":
            if(val){
                if(document.getElementById("rif_ordine").value > 0){
                    document.getElementById("da_fatturare").checked = false;
                    alert("Attenzione: esiste un riferimento ordine, dunque il BDL è già stato fatturato");
                } else {
                    document.getElementById("fatturato").checked = false;
                }
            }else{
                document.getElementById("fatturato").checked = true;
            }
        break;

        case "nessun_addebito":
            if(val){
                document.getElementById("div_contab1").style.display = "none";
                document.getElementById("div_contab2").style.display = "none";
            }else{
                if(document.getElementById("manutenzione_ordinaria").checked == false){
                    document.getElementById("div_contab1").style.display = "block";
                    document.getElementById("div_contab2").style.display = "block";
                }
            }
        break;
    }
}

function f_contratto_assistenza(){
    if(!document.getElementById("contratto_assistenza").checked){
        document.getElementById("manutenzione_ordinaria").checked = false;
        document.getElementById("manutenzione_straordinaria").checked = true;
        mostra_dettagli();
    }

    if(document.getElementById("contratto_assistenza").checked && document.getElementById("manutenzione_ordinaria").checked){
        // nascondi invia controllo
        document.getElementById("invio_controllo").style.display = "none";
    }
}

function f_manutenzione_ordinaria(){
    if(!document.getElementById("contratto_assistenza").checked){
        document.getElementById("manutenzione_ordinaria").checked = false;
        alert("Non si può selezionare la manutenzione ordinaria senza un contratto di assistenza");
    }
    if(document.getElementById("manutenzione_ordinaria").checked && document.getElementById("contratto_assistenza").checked){
        document.getElementById("manutenzione_straordinaria").checked = false;
        document.getElementById("pagato").checked = true;
        document.getElementById("da_pagare").checked = false;
        document.getElementById("fatturato").checked = true;
        document.getElementById("da_fatturare").checked = false;
        document.getElementById("invio_controllo").checked = true;
        document.getElementById("invio_contabilita").checked = true;

        //nascondo tutto
        nascondi_dettagli();
    }

    if(document.getElementById("manutenzione_ordinaria").checked){
        document.getElementById("manutenzione_straordinaria").checked = false;
    }

    if(!document.getElementById("manutenzione_ordinaria").checked){
        document.getElementById("manutenzione_straordinaria").checked = true;
        //mostro tutto
        mostra_dettagli();
    }

    if(document.getElementById("contratto_assistenza").checked && document.getElementById("manutenzione_ordinaria").checked){
        // nascondo invio controllo
        document.getElementById("invio_controllo").style.display = "none";
    }
}

function f_manutenzione_straordinaria(){
    if(document.getElementById("manutenzione_straordinaria").checked){
        document.getElementById("manutenzione_ordinaria").checked = false;
        document.getElementById("invio_controllo").checked = false;
        document.getElementById("invio_contabilita").checked = false;

        //mostro tutto
        mostra_dettagli();
    }

    if(!document.getElementById("manutenzione_straordinaria").checked && !document.getElementById("contratto_assistenza").checked){
        document.getElementById("manutenzione_straordinaria").checked = true;
        alert("Non si può selezionare la manutenzione ordinaria senza un contratto di assistenza");
    }

    if(!document.getElementById("manutenzione_straordinaria").checked && document.getElementById("contratto_assistenza").checked){
        document.getElementById("manutenzione_ordinaria").checked = true;
    }
}

function mostra_dettagli(){
    document.getElementById("seleziona_prodotto").style.display = "block";
    document.getElementById("aggiungi_prodotti").style.display = "block";
    document.getElementById("tabella_costi").style.display = "block";
    document.getElementById("note").style.display = "block";
    document.getElementById("note_private").style.display = "block";
    document.getElementById("div_contab1").style.display = "block";
    document.getElementById("div_contab2").style.display = "block";
    document.getElementById("ultima_row").style.display = "block";
}

function nascondi_dettagli(){
    document.getElementById("seleziona_prodotto").style.display = "none";
    document.getElementById("aggiungi_prodotti").style.display = "none";
    document.getElementById("tabella_costi").style.display = "none";
    document.getElementById("note").style.display = "none";
    document.getElementById("note_private").style.display = "none";
    document.getElementById("div_contab1").style.display = "none";
    document.getElementById("div_contab2").style.display = "none";
    document.getElementById("ultima_row").style.display = "none";
}


function aggiorna_costo_unitario(nome){
    var costo = 0;

    switch(document.getElementById(nome+"_tipo").value) {
        case "":  
            costo = 0;
        break;

        case "EZDIRFIX":
            costo = "60.00";
        break;

        case "EZDIRFIXF":
            costo = "120.00";
        break;

        case "EZKMR":
            costo = "0.50";
        break;

        case "EZZONAFIX":
            costo = "80.00";
        break;

        case "EZTECORD":
            costo = "60.00";
        break;

        case "EZTEC30":
            costo = "30.00";
        break;

        case "EZTECXTRA":
            costo = "80.00";
        break;

        case "EZTECFEST":
            costo = "90.00";
        break;

        default:
            costo = 0; 
        break;
    }
        
    //return costo;

    document.getElementById(nome+"_unitario").value = costo;
    document.getElementById(nome+"_codice").innerHTML = document.getElementById(nome+"_tipo").value;

    aggiorna_costo_totale_riga(nome);
}

function aggiorna_costo_totale_riga(nome){

    document.getElementById(nome+"_codice").innerHTML = document.getElementById(nome+"_tipo").value;

    if(document.getElementById(nome+"_codice").innerHTML != ""){
        //nome è nome del dettaglio ( es: diritto_chiamata )
        var codice = nome + "_codice" // ( es: diritto_chiamata_codice )
        
        if(document.getElementById(nome+"_qta").value == ""){
            document.getElementById(nome+"_qta").value = 1;
        }


        if(document.getElementById(nome+"_sconto").value == ""){
            document.getElementById(nome+"_sconto").value = 0;
        }

        var importo = 0;

                
        var numqta = document.getElementById(nome+"_qta").value;
        var qta = parseFloat(numqta.replace(/\s/g, "").replace(",", "."));
        var numunitario = document.getElementById(nome+"_unitario").value;
        var numsconto = document.getElementById(nome+"_sconto").value;
        var unitario = parseFloat(numunitario.replace(/\s/g, "").replace(",", "."));
        var sconto = parseFloat(numsconto.replace(/\s/g, "").replace(",", "."));
        document.getElementById(nome+"_importo").value = (qta*(unitario - ((sconto*unitario)/100))).toFixed(2).replace(",", ".");

        

    } else {
        document.getElementById(nome+"_qta").value = "";
        document.getElementById(nome+"_unitario").value = "";
        document.getElementById(nome+"_sconto").value = "";
        document.getElementById(nome+"_importo").value = "";
    }

    calcola_totale();

}

function calcola_totale() {
    
    var num_diritto_chiamata = document.getElementById("diritto_chiamata_importo").value;
    var diritto_chiamata = parseFloat(num_diritto_chiamata.replace(/\s/g, "").replace(",", "."));
    if(isNaN(diritto_chiamata)) { diritto_chiamata = 0; }
    
    var num_chilometri_percorsi = document.getElementById("chilometri_percorsi_importo").value;
    var chilometri_percorsi = parseFloat(num_chilometri_percorsi.replace(/\s/g, "").replace(",", "."));
    if(isNaN(chilometri_percorsi)) { chilometri_percorsi = 0; }
    
    var num_costo_manodopera = document.getElementById("costo_manodopera_importo").value;
    var costo_manodopera = parseFloat(num_costo_manodopera.replace(/\s/g, "").replace(",", "."));
    if(isNaN(costo_manodopera)) { costo_manodopera = 0; }
    
    var num_varie = document.getElementById("num_varie").value;
    
    var tot_varie = 0;
    for(var i=0; i<=num_varie; i++) {
        if(document.getElementById("ricambi_varie_"+i)) {
            var num_ricambi_e_varie = document.getElementById("ricambi_varie_"+i).value;
            var costo_ricambi_e_varie = parseFloat(num_ricambi_e_varie.replace(/\s/g, "").replace(",", "."));
            if(isNaN(costo_ricambi_e_varie)) { costo_ricambi_e_varie = 0; }
            tot_varie = tot_varie+costo_ricambi_e_varie;
        }
    }
    
    var totale_BDL = costo_manodopera + diritto_chiamata + chilometri_percorsi + tot_varie;
    document.getElementById("totale_BDL").value = totale_BDL.toFixed(2).replace(",", ".");

}


function checkFormBDL() {
    var check = document.getElementById("id_bdl").value; 
    var checkphone = document.getElementById("bdl_phone").value; 
    
    var num_totale_BDL = document.getElementById("totale_BDL").innerHTML;
    var totale_BDL = parseFloat(num_totale_BDL.replace(/\s/g, "").replace(",", "."));
    if(isNaN(totale_BDL)) { totale_BDL = 0; }
    
    var num_chilometri_percorsi = document.getElementById("chilometri_percorsi_unitario").value;
    var chilometri_percorsi = parseFloat(num_chilometri_percorsi.replace(/\s/g, "").replace(",", "."));
    if(isNaN(chilometri_percorsi)) { chilometri_percorsi = 0; }
    
    var num_diritto_chiamata = document.getElementById("diritto_chiamata_unitario").value;
    var diritto_chiamata = parseFloat(num_diritto_chiamata.replace(/\s/g, "").replace(",", "."));
    if(isNaN(diritto_chiamata)) { diritto_chiamata = 0; }
    
    var num_costo_manodopera = document.getElementById("costo_manodopera_unitario").value;
    var costo_manodopera = parseFloat(num_costo_manodopera.replace(/\s/g, "").replace(",", "."));
    if(isNaN(costo_manodopera)) { costo_manodopera = 0; }
    
    if(document.getElementById("diritto_chiamata_tipo").value == "" && diritto_chiamata > 0) {
        alert("ATTENZIONE: non è possibile inserire degli importi in diritto chiamata senza selezionare la descrizione. Correggere");
        return false;
    }
    if(document.getElementById("chilometri_percorsi_tipo").value == "" && chilometri_percorsi > 0) {
        alert("ATTENZIONE: non è possibile inserire degli importi in chilometri percorsi senza selezionare la descrizione. Correggere");
        return false;
    }
    if(document.getElementById("costo_manodopera_tipo").value == "" && costo_manodopera > 0) {
        alert("ATTENZIONE: non è possibile inserire degli importi in costo manodopera senza selezionare la descrizione. Correggere");
        return false;
    }
    if(totale_BDL > 0 && document.getElementById("rif_ordine").value > 0) {
        /*alert("ATTENZIONE: non è possibile creare il BDL in quanto è stato scelto un riferimento ordine, ma l"importo del BDL è superiore a zero. Eliminare il riferimento all"ordine.");
        return false;*/
    }
    if(check == "") {
        alert("Il campo N. BDL &egrave; obbligatorio per generare il contratto");
        return false;
    }
    if(checkphone == "") {
        alert("ATTENZIONE: devi inserire un numero di telefono");
        return false;
    }
    if(check != "" && checkphone != "") {
        surec=window.confirm("Sei sicuro?"); 
        if (surec) { 
            return true; 
        } 
        else { 
            return false; 
        }
    }
}

/* Note private -> bug: non funziona la modifica di una nota appena creata */

{if $smarty.get.id_bdl !== null}

$(document).ready(function(){
    var actions = $("td.azioni_note").html();

    $(".add-new").click(function(){
        $(this).attr("disabled", "disabled");
        var index = $("table.tabella_note > tbody > tr:last-child").index();
        var row = '<tr>' +
            //'<td><input type="text" class="form-control" name="nuova_nota" id="nuova_nota"></td>' +
            '<td><textarea class="form-control" name="nuova_nota" id="nuova_nota"></textarea></td>' +
            '<td>{$mio_nome}</td>' +
            '<td>{$today}</td>' +
            '<td>' + actions + '</td>' +
        '</tr>';
        $("table.tabella_note").append(row);
        $("table.tabella_note > tbody > tr").eq(index + 1).find(".add, .edit").toggle();
    });

    $(document).on("click", ".add", function(){
        var empty = false;
        var dbid = $(this).parents("tr").attr('dbid');
        //var input = $(this).parents("tr").find('input[type="text"]');
        var input = $(this).parents("tr").find('textarea');
        var questo = this;
        input.each(function(){
            if(!$(this).val()){
                $(this).addClass("error");
                empty = true;
            } else{
                $(this).removeClass("error");
            }
        });
        $(this).parents("tr").find(".error").first().focus();
        if(!empty){
            input.each(function(){
                $(this).parent("td").html($(this).val());
                if(dbid === undefined){
                    $.ajax({
                        url:"ajax.php?creanota_attivita=y",
                        type: "POST",
                        data: { 
                            id_action: {$smarty.get.id_bdl},
                            tipo: 'B',
                            nota: $(this).val()
                        },
                        success:function(data){
                            $(questo).parents("tr").attr('dbid', data);
                            tata.success('Nota creata con successo', 'ID '+data);
                        },
                        error: function(xhr,stato,errori){
                            tata.error('Errore nella creazione', xhr.status);
                        }
                    });
                } else {
                    $.ajax({
                        url:"ajax.php?modificanota_attivita=y",
                        type: "POST",
                        data: { 
                            id_nota: dbid,
                            nota: $(this).val()
                        },
                        success:function(){
                            tata.success('Nota modificata con successo', '');
                            succ = true;
                        },
                        error: function(xhr,stato,errori){
                            tata.error('Errore nella modifica', xhr.status);
                        }
                    });
                }
            });			
            $(this).parents("tr").find(".add, .edit").toggle();
            $(".add-new").removeAttr("disabled");
        }		
    });

    $(function() {
        $("input").keypress(function(event) {
            var $self = $(this);
            var comm = $self.val();

            var keycode = (event.keycode ? event.keycode : event.which);
            if (keycode == 13) {
                var comm = $self.val();
                alert(comm);
                event.stopPropagation();
            }
        });
    });

    $(document).on("click", ".edit", function(){		
        $(this).parents("tr").find("td:not(:last-child)").each(function(){
            if($(this).hasClass('mod')){ 
                //$(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">'); 
                $(this).html('<textarea class="form-control" style="height:80px;" >' + $(this).text() + '</textarea>'); 
            }
            
        });		
        $(this).parents("tr").find(".add, .edit").toggle();
        $(".add-new").attr("disabled", "disabled");
    });

    $(document).on("click", ".delete", function(){
        var dbid = $(this).parents("tr").attr('dbid');
        var succ = false;
        var questo = this;
        if(dbid === undefined){
            $(this).parents("tr").remove();
            $(".add-new").removeAttr("disabled");
        } else {
            if(confirm("Vuoi cancellare la nota con id "+dbid+" ?")){
                $.ajax({
                    url:"ajax.php?cancellanota_attivita=y",
                    type: "POST",
                    data: { 
                        cancella_nota: 'y',
                        id_nota: dbid
                    },
                    success:function(){
                        tata.success('Nota cancellata con successo', '');
                        succ = true;
                        $(questo).parents("tr").remove();
                        $(".add-new").removeAttr("disabled");
                    },
                    error: function(xhr,stato,errori){
                        tata.error('Errore nella cancellazione', xhr.status);
                    }
                });

            } else {
                
            }
        }
        /*
        if(succ == true){
            $(this).parents("tr").remove();
            $(".add-new").removeAttr("disabled");
            alert("eliminattoh");
        }

        $(this).parents("tr").remove();
        $(".add-new").removeAttr("disabled");
        */
    });
});

{/if}

</script>