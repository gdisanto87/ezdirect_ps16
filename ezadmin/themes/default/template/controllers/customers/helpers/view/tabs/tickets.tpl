{if $not_mio_agente == 0}

{if $smarty.get.error !== null && $smarty.get.error == '1tk'} {* Messaggio vuoto in risposta a un ticket *}
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading rosso">
                    <i class="icon-alert"></i> {l s='ERROR'}
                </div>
                <div class="form-row">
                    <div class="row">
                        <div class="alert alert-danger">Il messaggio era vuoto e non &egrave; stato inviato, ma sono stati aggiornati i campi status e in carico a.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{else if $smarty.get.error !== null && $smarty.get.error == '2tk'} {* Messaggio vuoto in un nuovo ticket *}
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading rosso">
                    <i class="icon-alert"></i> {l s='ERROR'}
                </div>
                <div class="form-row">
                    <div class="row">
                        <div class="alert alert-danger">Il messaggio era vuoto e non &egrave; stato inviato. Il ticket non  &egrave; stato aperto.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/if}

{* <form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" name="tickets" novalidate> *}
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-ticket"></i> {l s='Tickets'}

            <div class="panel-heading-action">
                <div class="bottoni-tab">
                    <a class="btn btn-default" href="{$link_gen_base}tab_name=tickets&amp;azione=tickets&amp;viewticket&amp;aprinuovoticket{$token_base}">
                        <i class="icon-plus-sign"></i>
                        {l s='Open new ticket'}
                    </a>
                </div>
            </div>
        </div>
        {if count($tickets['ticket_list']) && $tickets['specific'] == 'n'}
        <table class="table tabella_datatable" id="tickets_table">
            <thead>
                <tr>
                    <th><span class="title_box ">{l s='ID Action'}</span></th>
                    <th><span class="title_box ">{l s='Status'}</span></th>
                    <th><span class="title_box ">{l s='Edit status'}</span></th>
                    <th><span class="title_box ">{l s='In charge'}</span></th>
                    <th><span class="title_box ">{l s='Type'}</span></th>
                    <th><span class="title_box ">{l s='Q.ta com'}</span></th>
                    <th><span class="title_box ">{l s='Last com.'}</span></th>
                    <th><span class="title_box ">{l s='Opened on'}</span></th>
                    <th><span class="title_box ">{l s='Changed on'}</span></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {foreach $tickets['ticket_list'] AS $key => $ticket}
                <tr id="{$ticket['id']}" class="actions_ticket_tr pointer" rel="{$ticket['rel']}">
                    <td>{$ticket['id_ticket']}</td>
                    <td data-sort="{$ticket['status_azione_sort']}">{$ticket['status_azione']}</td>
                    <td data-sort="{$ticket['status_azione_sort']}">{$ticket['status_edit']}</td>
                    <td data-sort="{$ticket['in_carico']}">{$ticket['in_carico_edit']}</td> 
                    <td data-sort="{$ticket['type']}">{$ticket['tipo']}</td>
                    <td>{$ticket['qta_com']}</td> 
                    <td>{$ticket['ultima_com']}</td>
                    <td>{$ticket['aperto_il']}</td>
                    <td>{$ticket['modificato_il']}</td>
                    <td>
                        <a class="btn btn-default" style="border-radius:0px;" href="{$ticket['href']}" title="Vedi">
                            <i class='icon-search'></i> {l s='View'}
                        </a>
                        {if !$check_admin2}
                        <a class="btn btn-danger" style="border-radius:0px;" href="{$ticket['href_cancella']}" title="Cancella" onclick="return confirm('Sei sicuro/a?')">
                            <i class='icon-trash'></i> {*l s='Delete'*}
                        </a>
                        {/if}
                    </td>
                </tr>
                {/foreach}
            </tbody>
        </table>
        <br />
        Clicca sull'azione per aprirla, leggere i messaggi e rispondere.
        {else}
        <p>{l s='This customer has no ticket.'}</p>
        {/if}
        
    </div>
{* </form> *}
{else}
{$is_agente_activated = 1}
{/if}