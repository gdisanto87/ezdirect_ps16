<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="form-row">
                <div class="row">
                    <h2 class="blu" style="text-align:center">
                        Nuovo contratto - Contratto assistenza n.{$contratti['contratti_add']['id_contratto']}
                    </h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-user"></i> 
                Contratto
            </div>

            <form action="{$link_gen_base}tab_name=contracts&amp;azione=contratto_add&amp;submitted=y{$token_base}" method="post" onsubmit="return checkFormContratto();">
                <div class="form-row">
                    <div class="row">
                        <div class="form-group col-md-2">
                            <label>N. contratto</label>
                            <p class="form-control-static">
                                <input type="text" name="id_contratto" id="id_contratto" value="{$contratti['contratti_add']['id_contratto']}" readonly>
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Rif. fattura</label>
                            <p class="form-control-static">
                                <input type="text" name="rif_fattura" id="rif_fattura" value="">
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Rif. noleggio</label>
                            <p class="form-control-static">
                                <input type="text" name="rif_noleggio" id="rif_noleggio" value="">
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Rif. ordine</label>
                            <p class="form-control-static">
                                <input type="text" name="rif_ordine" id="rif_ordine" value="">
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Tipo</label>
                            <p class="form-control-static">
                                <select name="tipo" id="tipo">
                                    <option value="1">Base</option>
                                    <option value="2">Top</option>
                                    <option value="3">Gold</option>
                                    <option value="4">Teleassistenza</option>
                                    <option value="5">Noleggio</option>
                                </select>
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Stato</label>
                            <p class="form-control-static">
                                <select name="status" id="status">
                                    <option value="0">Attivo</option>
                                    <option value="1">Sospeso</option>
                                    <option value="2">Cancellato</option>
                                    <option value="3">Disdetto da cliente</option>
                                    <option value="4">Scaduto</option>
                                </select>
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <label>Importo</label>
                            <p class="form-control-static">
                                <input type="text" name="importo" id="importo" value="" disabled>
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Cadenza</label>
                            <p class="form-control-static">
                                <select name="cadenza" id="cadenza">
                                    <option value="Mensile">Mensile</option>
                                    <option value="Annuale">Annuale</option>
                                    <option value="Bimestrale">Bimestrale</option>
                                    <option value="Trimestrale">Trimestrale</option>
                                    <option value="Semestrale">Semestrale</option>
                                </select>
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Rata</label>
                            <p class="form-control-static">
                                <input type="text" name="rata_contratto" id="rata_contratto" value="" readonly>
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Pagamento</label>
                            <p class="form-control-static">
                                <select name="pagamento" id="pagamento">
                                    <option value="">Nessun metodo</option>
                                    <option value='Bonifico Bancario'>Bonifico</option>
                                    <option value='GestPay'>Carta di credito (Gestpay)</option>
                                    <option value='PayPal'>PayPal</option>
                                    <option value='Contrassegno'>Contrassegno</option>
                                    <option value='Bonifico 30 gg. fine mese'>Bonifico 30 gg. fine mese</option>
                                    <option value='Bonifico 60 gg. fine mese'>Bonifico 60 gg. fine mese</option>
                                    <option value='Bonifico 90 gg. fine mese'>Bonifico 90 gg. fine mese</option>
                                    <option value='Bonifico 30 gg. 15 mese successivo'>Bonifico 30 gg. 15 mese successivo</option>
                                    <option value='R.B. 30 GG. 5 mese successivo'>R.B. 30 GG. 5 mese successivo</option>
                                    <option value='R.B. 30 GG. 10 mese successivo'>R.B. 30 GG. 10 mese successivo</option>
                                    <option value='R.B. 60 GG. 5 mese successivo'>R.B. 60 GG. 5 mese successivo</option>
                                    <option value='R.B. 60 GG. 10 mese successivo'>R.B. 60 GG. 10 mese successivo</option>
                                    <option value='R.B. 60 GG. D.F. F.M.'>R.B. 60 GG. D.F. F.M.</option>
                                    <option value='Renting'>Renting</option>
                                </select>
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <label>CIG</label>
                            <p class="form-control-static">
                                <input type="text" name="cig" id="cig" value="">
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>CUP</label>
                            <p class="form-control-static">
                                <input type="text" name="cup" id="cup" value="">
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Cod. univoco ufficio x fatt. elettronica</label>
                            <p class="form-control-static">
                                <input type="text" name="univoco" id="univoco" value="">
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Data registrazione</label> {* Trasformare in data *}
                            <p class="form-control-static">
                                <input type="text" name="data_registrazione" id="data_registrazione" value="{$contratti['contratti_add']['oggi']}">
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Data inizio</label> {* Trasformare in data *}
                            <p class="form-control-static">
                                <input type="text" name="data_inizio" id="data_inizio" value="{$contratti['contratti_add']['oggi']}">
                            </p>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Data fine</label> {* Trasformare in data - inserire un value di default *}
                            <p class="form-control-static">
                                <input type="text" name="data_fine" id="data_fine" value="">
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label>Sede</label>
                            <p class="form-control-static">
                                <select name="indirizzo_contratto" id="indirizzo_contratto">
                                {foreach $contratti['contratti_add']['indirizzi'] as $del_a}
                                    <option value="{$del_a['id_address']}">{$del_a['address1']} - {$del_a['city']}</option>
                                {/foreach}
                                </select>
                            </p>
                        </div>
                        <div class="form-group col-md-2 rosso">
                            <label>Blocco amministrativo</label>
                            <p class="form-control-static rosso">
                                <input type="checkbox" name="blocco_amministrativo" id="blocco_amministrativo" value="">
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <br />
                        <strong>Prodotti</strong>

                        <table class='table'>
                            <thead>
                                <tr>
                                    <th><span class="title_box ">Codice</span></th>
                                    <th><span class="title_box ">Descrizione</span></th>
                                    <th><span class="title_box ">Qta</span></th>
                                    <th><span class="title_box ">Prezzo</span></th>
                                    <th><span class="title_box ">Seriale</span></th>
                                    <th><span class="title_box ">Indirizzo</span></th>
                                    {if !$check_admin2}<th><span class="title_box "></span></th>{/if}
                                </tr>
                            </thead>
                            <tbody id='tableProductsBody'>
                                {if count($contratti['contratti_add']['prodotti'])}
                                {foreach $contratti['contratti_add']['prodotti'] AS $key => $prodotto}
                                <tr id='tr_{$prodotto['product_id']}'>
                                    <td>{if $prodotto['product_id']}<a class="tooltip_prodotto" title="{$prodotto['tooltip']}" href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}&amp;id_product={$prodotto['product_id']|intval}&amp;updateproduct&amp;token={getAdminToken tab='AdminProducts'}" target="_blank">{$prodotto['reference']}</a>{else}{$prodotto['reference']}{/if}</td>
                                    <td><input type='text' name='prodotto_descrizione[{$prodotto['product_id']}]' value='{$prodotto['name']}' /></td>
                                    <td>
                                        <input type='hidden' name='prodotto[{$prodotto['product_id']}]' />
                                        <input type='text' size='3' id='quantita[{$prodotto['product_id']}]' name='quantita[{$prodotto['product_id']}]' value='{$prodotto['qt_ord']}' onkeyup='CalcTotContratto();' />
                                    </td>
                                    <td class="text-right"><input type='text' size='7' class='importo' name='prezzo_prodotto[{$prodotto['product_id']}]' id='prezzo_prodotto[{$prodotto['product_id']}]' value='' onkeyup='CalcTotContratto();' /></td>
                                    <td><input type='text' size='7' name='seriale_prodotto[{$prodotto['product_id']}]' value='' /></td>
                                    <td>
                                        <select name="indirizzo_prodotto[{$prodotto['product_id']}]" id="indirizzo_prodotto">
                                        {foreach $contratti['contratti_add']['indirizzi'] as $del_a}
                                            <option value="{$del_a['id_address']}">{$del_a['address1']} - {$del_a['city']}</option>
                                        {/foreach}
                                        </select>
                                    </td>
                                    {if !$check_admin2} {* CORREGGERE: non funziona nella 1.4 *}
                                    <td><a style="cursor:pointer" onclick="togliImportoContratto({$prodotto['product_id']}); CalcTotContratto(); delProduct30Contratto({$prodotto['product_id']})"><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a></td>
                                    {/if}
                                </tr>
                                {/foreach}
                                {/if}
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan='2'><span>Tot. Importo Contratto IVA escl.</span></td>
                                    <td></td>
                                    <td><span class='tab-span' id='totale_contratto' style='text-align:right; float:left'></span></td>
                                    <td><input type='hidden' name='totale_importo' id='totale_importo' value='' /></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-8">
                            <label for="note_private">Note private</label>
                            <textarea name="note_private" id="note_private" rows="4"></textarea>
                        </div>
                    </div>

                    <div class="row" id="dettagli_controlli">
                        <div class="col-md-4" id="dettagli_pagamento_contratto">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th><span class="title_box center_text">{l s='Paid'}</span></th>
                                        <th><span class="title_box center_text">{l s='Unpaid'}</span></th>
                                        <th><span class="title_box center_text">{l s='Billed'}</span></th>
                                        <th><span class="title_box center_text">{l s='To be invoiced'}</span></th>
                                    </tr>
                                </thead>
                                <tbody class="center">
                                    <tr>
                                        <td><input class="super_center" type="checkbox" name="pagato" id="pagato" value="1" disabled></td>
                                        <td><input class="super_center" type="checkbox" name="da_pagare" id="da_pagare" checked="checked" value="0" disabled></td>
                                        <td><input class="super_center" type="checkbox" name="fatturato" id="fatturato" value="1" disabled></td>
                                        <td><input class="super_center" type="checkbox" name="da_fatturare" id="da_fatturare" checked="checked" value="0" disabled></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-1">
                        </div>
                        
                        {if $check_employee}
                        <div class="col-md-2" id="invii_controlli">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th><span class="title_box center_text">{l s='Accounting check'} {* Conferma per contabilità *} </span></th>
                                    </tr>
                                </thead>
                                <tbody class="center">
                                    <tr>
                                        <td><input class="super_center" type="checkbox" name="invio_contabilita" id="invio_contabilita" value="1"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        {/if}
                    </div>

                    <div class="row">
                        <input type="hidden" name="rinnova_contratto_ok" id="rinnova_contratto_ok" value="0" />
                        <br />
                        <button type="submit" id="conferma_contratto" class="btn btn-secondary">
                            <i class="icon-check" style="color:green" alt="Conferma" title="Conferma"></i> Conferma
                        </button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>