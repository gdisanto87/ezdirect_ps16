{assign var=messaggia value=$todo['todo_view']['cronologia']['messaggia']}
{assign var=storico_ticket value=$todo['todo_view']['storico']['storico_ticket']}
{assign var=tree_ul value=$todo['todo_view']['gerarchia']['tree_ul']}

{* Stile della pagina *}
{include file="./todo_stile.tpl"}

{if $todo['todo_view']['generale']['errore']}

    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="form-row">
                    <div class="row">
                        <div class="alert alert-danger">ERRORE - Azione non trovata</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

{else}

{if $smarty.get.id_action !== null && $smarty.get.aprinuovaazione === null}

    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="form-row">
                    <div class="row">
                        <h2 class="blu no-margin" style="text-align:center">
                            ATTIVITÀ INTERNA {$todo['todo_view']['generale']['azione']['id_action']} <span style="color:black"> - In carico a {$todo['todo_view']['generale']['in_carico_a_firstname']} - Status: {$todo['todo_view']['generale']['status_azione']} {$todo['todo_view']['generale']['st_azione']}</span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {* Correggere: togliere &amp;id_action={$smarty.get.id_action} se non serve *}
    <form id="modificaazione" action="{$link_gen_base}tab_name=todo&amp;id_action={$smarty.get.id_action}&amp;submitAzione{if isset($smarty.get.riferimento)}&amp;riferimentoo={$smarty.get.riferimento}{/if}{$token_base}" method="post" autocomplete="off">
        <div class="form-horizontal col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <i class="icon-user"></i>
                    Generale
                </div>

                <div class="form-row">
                    <div class="row">
                        
                        <div class="form-group col-md-12">
                            <label for="azione_subject">Oggetto</label>
                            <p class="form-control-static">
                                <textarea name="azione_subject" id="azione_subjectContent" {if $todo['todo_view']['generale']['oggetto_readonly']}readonly="readonly"{/if}>{$todo['todo_view']['generale']['oggetto']}</textarea>
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <label for="id_azione">ID Azione</label>
                            <p class="form-control-static">
                                <input class="textbox-fix lettura" type="text" id="id_azione" name="id_azione" value="{$todo['todo_view']['generale']['azione']['id_action']}" disabled>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="aperto_da">Aperto da</label>
                            <p class="form-control-static">
                                <input class="textbox-fix lettura" type="text" id="aperto_da" name="aperto_da" value="{$todo['todo_view']['generale']['aperto_da']}" disabled>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="data_apertura">Data apertura</label>
                            <p class="form-control-static">
                                <input class="textbox-fix" type="text" id="data_apertura" name="data_apertura" value="{$todo['todo_view']['generale']['data_apertura']}" disabled>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="data_ultima_com">Data ultima com.</label>
                            <p class="form-control-static">
                                <input class="textbox-fix" type="text" id="data_ultima_com" name="data_ultima_com" value="{$todo['todo_view']['generale']['data_ultima_com']}" disabled>
                            </p>
                        </div>

                        <div class="form-group col-md-4"> {* Correggere: manca calendario *}
                            <label for="action_date">Data azione</label>
                            <div class="row form-control-static">
                                <div class="col-md-6">
                                    <input class="textbox-fix" type="text" id="action_date" name="action_date" value="{$todo['todo_view']['generale']['data_azione']}">
                                </div>
                                <div class="col-md-2">
                                    alle ore
                                </div>
                                <div class="col-md-2">
                                    <input class="textbox-fix" type="text" id="action_hours" name="action_hours" value="{$todo['todo_view']['generale']['data_azione_ore']}">
                                </div>
                                <div class="col-md-2">
                                    <input class="textbox-fix" type="text" id="action_minutes" name="action_minutes" value="{$todo['todo_view']['generale']['data_azione_min']}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <label for="assegnaimpiegatoa">In carico a</label>
                            <p class="form-control-static">
                                <select id="assegnaimpiegatoa" name="assegnaimpiegatoa" class="form-control textbox-fix lettura">
                                <option value="0">Da assegnare</option>
                                {foreach $todo['todo_view']['generale']['in_carico_a_options'] AS $key => $row}
                                <option value="{$row["id_employee"]}"{if $row["id_employee"] == $todo['todo_view']['generale']['azione']['action_to']} selected{/if}>{$row["firstname"]}</option>
                                {/foreach}
                                </select>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="action_type">Tipo azione</label>
                            <p class="form-control-static">
                                <select id="action_type" name="action_type">
                                    {foreach $todo['todo_view']['action_types'] as $tipo}
                                    <option value="{$tipo['value']}" {if $todo['todo_view']['generale']['azione']['action_type'] == "{$tipo['value']}" } selected{/if}>{$tipo['name']}</option>
                                    {/foreach}
                                </select>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="cambiastatusa">Status {$todo['todo_view']['generale']['status_azione']}</label>
                            <p class="form-control-static">
                                <select id="cambiastatusa" name="cambiastatusa" class="form-control textbox-fix lettura">
                                    <option value="closed"{if $todo['todo_view']['generale']['azione']['status'] == "closed" } selected{/if}>Chiuso</option>
                                    <option value="pending1"{if $todo['todo_view']['generale']['azione']['status'] == "pending1" } selected{/if}>In lavorazione</option>
                                    <option value="open"{if $todo['todo_view']['generale']['azione']['status'] == "open" } selected{/if}>Aperto</option>
                                </select>
                            </p>
                        </div>
                    </div>

                    {if $todo['todo_view']['generale']['azione']['riferimento'] != '' OR $todo['todo_view']['generale']['azione_padre'] != ''}
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="azione_padre">Azione padre</label>
                            <p class="form-control-static">
                                {$todo['todo_view']['generale']['azione_padre']}
                            </p>
                        </div>
                    </div>
                    {/if}

                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="promemoria">Promemoria</label>
                            <p class="form-control-static">
                                <button type="button" class="btn btn-secondary" onclick="add_promemoria();">Clicca per aggiungere un promemoria <img src="../img/admin/time.gif" style="width:20px" alt="Promemoria" title="Promemoria" /></button>
                            </p>
                        </div>
                        <div class="form-group col-md-6">
                            <table class="table" id="promemoriaTable">
                                <tbody id="promemoriaTableBody">
                                    {$todo['todo_view']['generale']['tr_promemoria']}
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-primary" name="submitAzione"><i class="icon-save" alt="Salva" title="Salva"></i> {l s='Save'}</button>
                        </div>
                        <div class="form-group col-md-3">
                            <button type="button" class="btn btn-primary" onclick="javascript:var surec=window.confirm('Sei sicuro?'); if (surec) { $('#coll_destinazione').val($('#collega_todo').val()); document.forms['collega_attivita'].submit(); return false; } else { return false; }"><i class="icon-link" alt="Collega" title="Collega"></i> {l s='Connect to'}</button>
                            <p class="form-control-static">
                                <select class="form-control textbox-fix" name="collega_todo" id="collega_todo">
                                    <option value="">-- Seleziona --</option>
                                    {$todo['todo_view']['generale']['collega_a']}
                                </select>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="form-horizontal col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <i class="icon-eye-close"></i> {l s='Private notes'}

                    <div class="panel-heading-action">
                        <a class="btn btn-default add-new">
                            <i class="icon-plus-sign"></i>
                            {l s='Add private note'}
                        </a>
                    </div>
                </div>
                <div class="form-group">
                    <table class="table table-bordered tabella_note" id="noteTable">
                        <thead>
                            <tr>
                                <th>{l s='Note'}</th>
                                <th>{l s='Created by'}</th>
                                <th>{l s='Last edit'}</th>
                                <th>{l s='Actions'}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="display:none"> {* Necessario per aggiungere la prima nota in js *}
                                <td class="mod"></td>
                                <td></td>
                                <td></td>
                                <td class="azioni_note">
                                    <a class="add" title="Add" data-toggle="tooltip"><i class="icon-save"></i></a>
                                    <a class="edit" title="Edit" data-toggle="tooltip"><i class="icon-edit"></i></a>
                                    <a class="delete" title="Delete" data-toggle="tooltip"><i class="icon-trash"></i></a>
                                </td>
                            </tr>
                        {foreach $todo['todo_view']['generale']['azione_note'] as $azione_nota}
                            <tr dbid="{$azione_nota['id_note']}" id="tr_note_{$azione_nota['id_note']}">
                                <td id="note_nota[{$azione_nota['id_note']}]" class="mod">{$azione_nota['note']}</td>
                                <td id="note_id_employee[{$azione_nota['id_note']}]">{$azione_nota['creato_da']}</td>
                                <td id="note_date_add[{$azione_nota['id_note']}]">{$azione_nota['date_upd']}</td>
                                <td class="azioni_note">
                                    <a class="add" title="Add" data-toggle="tooltip"><i class="icon-save"></i></a>
                                    <a class="edit" title="Edit" data-toggle="tooltip"><i class="icon-edit"></i></a>
                                    <a class="delete" title="Delete" data-toggle="tooltip"><i class="icon-trash"></i></a>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </form>

{/if}

    <div class="form-horizontal col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-user"></i>
                {if $smarty.get.id_action !== null && $smarty.get.aprinuovaazione === null}
                Cronologia
                {else}
                Nuovo To Do
                {/if}
            </div>

            {if $smarty.get.id_action !== null && $smarty.get.aprinuovaazione === null}

            {foreach $messaggia AS $key => $messaggioa}
                <div style="background: rgba(255, 192, 203, 0.5); border: 1px solid lightpink; margin-bottom:1%;">
                    <div class="row" style="padding-left:2%; padding-top:1%;">
                        <div class="form-group col-md-3">
                            <label>Da</label>
                            <p class="form-control-static">
                                {$messaggioa['da']}
                            </p>
                        </div>

                        <div class="form-group col-md-3">
                            <label>Data</label>
                            <p class="form-control-static">
                                {$messaggioa['data']}
                            </p>
                        </div>

                        <div class="form-group col-md-3">
                            <label>Allegati</label>
                            <p class="form-control-static">
                                {$messaggioa['allegati']}
                            </p>
                        </div>
                    </div>
                    <div class="row" style="padding-left:2%;">
                        <div class="form-group col-md-8">
                            <label>Messaggio</label>
                            <p class="form-control-static">
                                {$messaggioa['action_message']}
                            </p>
                        </div>
                        <div class="form-group col-md-4">
                            <form method="post">
                                <input type="hidden" name="modifica_messaggio_azione" value="{$messaggioa['action_message']|escape:'htmlall'}" />
                                <input type="hidden" name="modifica_messaggio_azione_id" value="{$messaggioa['id_action_message']}" />
                                <button type="submit" class="btn btn-secondary" name="modifica_messaggio_submit" value="Modifica">Modifica</button> 
                                {if !$check_admin2}
                                <button type="submit" class="btn btn-secondary" name="cancella_messaggio_submit" value="Cancella" onclick="javascript:var surec=window.confirm('Sei sicuro/a?'); if (surec) { return true; } else { return false; }">Cancella</button>
                                {/if}
                            </form>
                        </div>
                    </div>

                    <div class="row" style="padding-left:2%;">
                        <div class="form-group col-md-3">
                            <label>Inviato a</label>
                            <p class="form-control-static">
                                {$messaggioa['employee_to_name']}
                            </p>
                        </div>

                        <div class="form-group col-md-3">
                            <label>In carico a</label>
                            <p class="form-control-static">
                                {$messaggioa['in_carico_a_name']}
                            </p>
                        </div>

                        <div class="form-group col-md-3">
                            <label>Telefono cliente</label>
                            <p class="form-control-static">
                                <a href="callto:{$messaggioa['telefono_cliente']}">{$messaggioa['telefono_cliente']}</a>
                            </p>
                        </div>

                        <div class="form-group col-md-3">
                            <label>CC</label>
                            <p class="form-control-static">
                                {$messaggioa['cc']}
                            </p>
                        </div>
                    </div>
                    <div class="row" style="padding-left:2%;">
                    </div>
                </div>
            {/foreach}

            {/if}


<form name="submit_azione" id="submit_azione" action="{$link_gen_base}tab_name=todo&amp;azione=todo&amp;viewaction{if isset($smarty.get.id_action)}&amp;id_action={$smarty.get.id_action}{else}&amp;aprinuovaazione{/if}{if isset($smarty.get.riferimento)}&amp;riferimentoo={$smarty.get.riferimento}{/if}{$token_base}" method="post">
            
            {if $smarty.get.id_action !== null && $smarty.get.aprinuovaazione === null}
            <h2 id="rispondi-azione">Rispondi a questa azione</h2>
            {else}
            <h2 id="apri-azione">Apri nuova azione</h2>
            {/if}
            
            <input type="hidden" name="id_customer" value="{$customer->id}" />
			<input type="hidden" name="id_customer_thread" value="{$todo['todo_view']['cronologia']['thread']}" /> {* Correggere: verificare che sia giusto *}

            <div class="row">

                {if $smarty.get.id_action !== null && $smarty.get.aprinuovaazione === null}

                <input type="hidden" name="action_type" value="{$todo['todo_view']['cronologia']['azione']['action_type']}" />

                {else}
                
                <input type="hidden" name="nuovaazione" value="1" />
                <input type="hidden" name="riferimento" id="riferimento" value="{if isset($smarty.get.riferimento)}{$smarty.get.riferimento}{/if}" />
                <input type="hidden" name="riferimentoo" id="riferimentoo" value="{if isset($smarty.get.riferimento)}{$smarty.get.riferimento}{/if}" />
                
                <div class="form-group col-md-2">
                    <label for="action_type">Tipo azione</label>
                    <p class="form-control-static">
                        <select id="action_type" name="action_type" class="form-control textbox-fix lettura">
                            {foreach $todo['todo_view']['action_types'] as $tipo} {* Correggere tipo-todo con tipo_todo nel resto del codice *}
                                <option value="{$tipo['value']}" {if isset($smarty.get.tipo_todo) && $smarty.get.tipo_todo == "{$tipo['value']}"} selected{/if}>{$tipo['name']}</option>
                            {/foreach}
                        </select>
                    </p>
                </div>

                {/if}
                
                <div class="form-group col-md-2">
                    <label for="statusa">Stato azione</label>
                    <p class="form-control-static">
                        <select id="statusa" name="statusa" class="form-control textbox-fix lettura">
                            <option value="open" {if $todo['todo_view']['cronologia']['azione']['status'] == "open" } selected{/if}>Aperto</option>
                            <option value="pending1" {if $todo['todo_view']['cronologia']['azione']['status'] == "pending1" } selected{/if}>In lavorazione</option>
                            <option value="closed" {if $todo['todo_view']['cronologia']['azione']['status'] == "closed" } selected{/if}>Chiuso</option>
                        </select>
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="in_carico_a_a">In carico a</label>
                    <p class="form-control-static">
                        <select id="in_carico_a_a" name="in_carico_a_a" class="form-control textbox-fix" onChange="changeInCaricoAA();">
                        {foreach $todo['todo_view']['cronologia']['impiegati_eza'] as $impez}
                            <option value="{$impez['id_employee']}" {if $impez['id_employee'] == $todo['todo_view']['cronologia']['impez_selected']} selected{/if}>{$impez['firstname']}</option>
                        {/foreach}
                        </select>
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label>Tel. Cliente</label>
                    <p class="form-control-static">
                        <select id="telefono_cliente" name="telefono_cliente" class="form-control textbox-fix" onclick="document.getElementById('tel_action').href='callto:'+document.getElementById('telefono_cliente').value">
                            {$todo['todo_view']['cronologia']['telefoni_options']}
                        </select>
                        <a id="tel_action" href="callto:{$todo['todo_view']['cronologia']['attuale_tel']}">Chiama</a>
                    </p>
                </div>

                {if $smarty.get.id_action !== null && $smarty.get.aprinuovaazione === null}
                    {* Data azione è già in Generale *}
                {else}
                <div class="form-group col-md-4"> {* Correggere: manca calendario *}
                    <label for="action_date">Data azione</label>
                    <div class="row form-control-static">
                        <div class="col-md-6">
                            <input class="textbox-fix" type="text" id="action_date" name="action_date" value="{$smarty.now|date_format:"%Y-%m-%d"}">
                        </div>
                        <div class="col-md-2">
                            alle ore
                        </div>
                        <div class="col-md-2">
                            <input class="textbox-fix" type="text" id="action_hours" name="action_hours" value="{$smarty.now|date_format:"%H"}">
                        </div>
                        <div class="col-md-2">
                            <input class="textbox-fix" type="text" id="action_minutes" name="action_minutes" value="{$smarty.now|date_format:"%M"}">
                        </div>
                    </div>
                </div>
                {/if}
            </div>

            <div class="row">
                <label>Conoscenza</label>
                <br />
                {foreach $todo['todo_view']['cronologia']['impiegati_eza'] AS $impez}
                    <input type="checkbox" name="conoscenza_a[]" id="conoscenza_{$impez['id_employee']}" value="{$impez['id_employee']}"> {$impez['firstname']}&nbsp;&nbsp;&nbsp;&nbsp; 
                {/foreach}
            </div>

            <br />

            <div class="row"> {* Correggere anteprima *}
                <div class="form-group col-md-6">
                    <label>Allega file</label>
                    <p class="form-control-static">
                        <input name="MAX_FILE_SIZE" value="20000000" type="hidden">
                        <input type="file" multiple class="multi" name="joinFile[]" id="joinFile" onchange="handleFileSelect(this.files)">
                        <output id="list-action">Anteprime: </output>
                    </p>
                </div>
            </div>

            <br />

            <div class="row" id="aggiungi_prodotti">

                <div class="form-row">
                    <div class="col-md-1">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                        <label class="form-check-label" for="inlineCheckbox1">Online</label>
                    </div>
                    <div class="col-md-1">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                        <label class="form-check-label" for="inlineCheckbox1">Offline</label>
                    </div>
                    <div class="col-md-1">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                        <label class="form-check-label" for="inlineCheckbox1">Old</label>
                    </div>
                    <div class="col-md-1">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                        <label class="form-check-label" for="inlineCheckbox1">{l s='Only av.'}</label> {* Solo disp. *}
                    </div>

                    <div class="col-md-2">
                        <select id="brand" class="chosen form-control" name="brand">
                            <option value="">{l s='Brand...'}</option>
                            {foreach $todo['todo_view']['cronologia']['marche'] AS $key => $marche}
                                <option value="{$marche['id']}">{$marche['name']}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select id="serie" class="chosen form-control" name="serie">
                            <option value="">{l s='Series...'}</option>
                            <option value="">Scegli prima un costruttore</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select id="categorie" class="chosen form-control" name="categorie">
                            <option value="">{l s='Category...'}</option>
                            {foreach $todo['todo_view']['cronologia']['categorie'] AS $key => $categorie}
                                <option value="{$categorie['id']}">{$categorie['name']}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select id="fornitori" class="chosen form-control" name="fornitori">
                            <option value="">{l s='Supplier...'}</option>
                            {foreach $todo['todo_view']['cronologia']['fornitori'] AS $key => $fornitori}
                                <option value="{$fornitori['id']}">{$fornitori['name']}</option>
                            {/foreach}
                        </select>
                    </div>

                    <div class="col-md-12">
                        <input type="text">
                    </div>
                </div>
            </div>

            <br />

            <div class="row">

                <div class="form-group col-md-6">
                    <label for="link_utili">Link utili</label>
                    <p class="form-control-static">
                        <select id="utils_autocomplete_input_action" name="utils_autocomplete_input_action" class="select2 form-control textbox-fix" onchange="addUtil_TR();">
                            {$utils_options}
                        </select>
                    </p>
                </div>

                <div class="form-group col-md-6">
                    <label for="messaggi_precompilati">Messaggi precompilati</label>
                    <p class="form-control-static">
                        <select id="messaggi_precompilati" name="messaggi_precompilati" class="select2 form-control textbox-fix" onchange="inserisciPrecompilato(this.value);">
                            <option value="-- Seleziona un messaggio --">-- Seleziona un messaggio --</option>
                        {foreach $todo['todo_view']['cronologia']['precompilati'] as $precompilato}
                            <option value="{$precompilato['testo']|escape:'htmlall'}">{$precompilato["oggetto"]|escape:'htmlall'}</option>
                        {/foreach}
                        </select>
                    </p>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-12">
                    <label>Messaggio</label>
                    <textarea class="rte autoload_rte" id="messaggioa" name="messaggioa" rows="5" cols="40">{if isset($smarty.post.modifica_messaggio_submit)}{$smarty.post.modifica_messaggio_azione|stripslashes}{/if}</textarea>
                </div>
            </div>

            <br />

            {if $smarty.get.id_action !== null && $smarty.get.aprinuovaazione === null}
                {* I promemoria sono già in Generale *}
            {else}
            <div class="row">
                <div class="form-group col-md-12">
                    <label for="promemoria">Promemoria</label>
                    <p class="form-control-static">
                        <button type="button" class="btn btn-secondary" onclick="add_promemoria();">Clicca per aggiungere un promemoria <img src="../img/admin/time.gif" style="width:20px" alt="Promemoria" title="Promemoria" /></button>
                    </p>
                </div>
                <div class="form-group col-md-6">
                    <table class="table" id="promemoriaTable">
                        <tbody id="promemoriaTableBody">
                        </tbody>
                    </table>
                </div>
            </div>
            {/if}

            {if $todo['todo_view']['cronologia']['riferimento'] != '' && $todo['todo_view']['cronologia']['statusPadre'] != 'closed' && $todo['todo_view']['cronologia']['statusPadre'] != ''}
            <div class="row">
                <div style="border:3px solid red; padding:5px;"><input type="checkbox" name="chiudi_padre" />&nbsp;&nbsp;&nbsp;<strong style="color:red">ATTENZIONE!</strong>  L'azione padre risulta ancora aperta. Spunta questa casella se vuoi chiuderla.</div>
            </div>
            {/if}

            <br />

            {if isset($smarty.post.modifica_messaggio_submit)}
			<input type='hidden' name='modifica_messaggio_azione_id' value='{$smarty.post.modifica_messaggio_azione_id}' />
			{/if}

            <button type="submit" class="btn btn-secondary" name="submitActionReply" id="submitActionReply" value="Modifica">Conferma azione</button>
        </div>
    </div>

    {if isset($smarty.post.modifica_messaggio_azione) && isset($smarty.post.modifica_messaggio_submit)}
    <script type="text/javascript"> window.location = "#rispondi-azione"; </script>
    {/if}

    {if isset($tinymce) && $tinymce}
    <script type="text/javascript">
        var iso = '{$iso|addslashes}';
        var pathCSS = '{$smarty.const._THEME_CSSDIR|addslashes}';
        var ad = '{$ad|addslashes}';

        $(document).ready(function(){
            {block name="autoload_tinyMCE"}
                tinySetup({
                    editor_selector :"autoload_rte"
                });
            {/block}
        });
    </script>
    {/if}

</form>

{if $smarty.get.id_action !== null && $smarty.get.aprinuovaazione === null}

    <div class="form-horizontal col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-user"></i>
                Gerarchia
            </div>

            <div class="tree">
                {$tree_ul}
            </div>
        </div>
    </div>

    <div class="form-horizontal col-lg-6">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-user"></i>
                Storico
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered" id="storicoTable">
                        <thead>
                            <tr>
                                <th>Persona</th>
                                <th>Azione</th>
                                <th>Data</th>
                            </tr>
                        </thead>

                        <tbody>
                        {foreach $storico_ticket AS $key => $storico}
                            <tr>
                                <td>{$storico['persona']}</td>
                                <td>{$storico['azione']}</td>
                                <td>{$storico['data_attivita']}</td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <form name="collega_attivita" id="collega_attivita" action="{$smarty.server.REQUEST_URI}&conf=4" method="post">
        <input type="hidden" name="submitCollegaAttivita" value="" />
        <input type="hidden" name="coll_destinazione" id="coll_destinazione" value="" />
        <input type="hidden" name="coll_partenza" id="coll_partenza" value="A{$todo['todo_view']['generale']['azione']['id_action']}" />
    </form>

{/if}

{/if}

{* Javascript della pagina *}
{include file="./todo_js.tpl"}

<script type="text/javascript" src="jquery.MultiFile.js"></script>