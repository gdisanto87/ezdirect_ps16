<form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" name="pagamenti" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-money"></i> {l s='Pagamenti'}
            <span class="badge squadrato">
                {count($persone)}
            </span>

            <div class="panel-heading-action">
            {* Premendo il tasto "add" si viene reindirizzati alla creazione di un nuovo "customer" con id_customer_associate che sarebbe l'id del customer a cui associare la nuova anagrafica *}
            {* index.php?controller=AdminCustomers *}
                <a class="btn btn-default" href="{$current|escape:'html':'UTF-8'}&amp;addcustomer&amp;id_customer_associate={$customer->id|intval}&amp;token={getAdminToken tab='AdminCustomers'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
                    <i class="icon-plus-sign"></i>
                    {l s='Add'}
                </a>
            </div>
        </div>

        
        {if count($persone)}
            <table class="table">
                <thead>
                    <tr>
                        <th class="center"><span class="title_box ">{l s='ID'}</span></th>
                        <th><span class="title_box">{l s='Name'}</span></th>
                        <th><span class="title_box">{l s='Surname'}</span></th>
                        <th><span class="title_box">{l s='Role'}</span></th>
                        <th><span class="title_box">{l s='Telephone 1'}</span></th>
                        <th><span class="title_box">{l s='Telephone 2'}</span></th>
                        <th><span class="title_box">{l s='Mobile Phone'}</span></th>
                        <th><span class="title_box">{l s='Email'}</span></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                {foreach $persone AS $key => $persona}
                    <tr onclick="document.location = '?tab=AdminOrders&amp;id_order={$order['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}'">
                        <td>{$persona['id_persona']}</td>
                        <td>{$persona['firstname']}</td>
                        <td>{$persona['lastname']}</td>
                        <td>{$persona['role']}</td>
                        <td>{$persona['phone']}</td>
                        <td>{$persona['phone_2']}</td>
                        <td>{$persona['phone_mobile']}</td>
                        <td>
                            <a class="btn btn-default squadrato" href="?tab=AdminOrders&amp;id_order={$order['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}">
                                <i class='icon-edit'></i>
                            </a>
                            <a class="btn btn-default squadrato" href="?tab=AdminOrders&amp;id_order={$order['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}">
                                <i class='icon-envelope'></i>
                            </a>
                            <a class="btn btn-default squadrato" href="?tab=AdminOrders&amp;id_order={$order['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}">
                                <i class='icon-windows'></i>
                            </a>
                            <a class="btn btn-danger squadrato" href="?tab=AdminOrders&amp;id_order={$order['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}">
                                <i class='icon-trash'></i>
                            </a>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
            
        {else}
        <p class="text-muted text-center">
            {l s='There are no associated payments'}
        </p>
        {/if}
    </div>
</form>