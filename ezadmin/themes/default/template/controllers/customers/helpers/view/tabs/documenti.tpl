{if $is_agente == 0}
<form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-paper-clip"></i> {l s='Documents'}

            <div class="panel-heading-action">
                
            </div>
        </div>

        {if $check_cartella_documenti == true}
            {if $documenti['count_docs'] != 0}
            <p>{l s='Sono presenti %1$d file e %2$d sottocartelle in questa cartella.' sprintf=[$documenti['count_sub_files'], $documenti['count_sub_cartelle']]}</p>
            {else}
            <p>{l s='Non sono presenti file né sottocartelle in questa cartella.'}</p>
            {/if}
            <table class="table">
                <thead>
                    <tr>
                        <th><span class="title_box ">{l s='File'}</span></th>
                        <th><span class="title_box ">{l s='Size'}</span></th>
                        <th><span class="title_box ">{l s='Format'}</span></th>
                        <th><span class="title_box ">{l s='Last edit'}</span></th>
                        <th style='text-align:center;'><span class="title_box ">{l s='Select'}</span></th>
                    </tr>
                </thead>
                <tbody>
                    {if $documenti['l_sup'] == true}
                    <tr class="pointer">
                        <td><a href="{$smarty.server.REQUEST_URI}{$documenti['l_sup_href']}"><i class="icon-level-up"></i> Livello superiore</a></td> {* Per andare alla parent folder *}
                        <td></td>
                        <td></td> 
                        <td></td>
                        <td></td>
                    </tr>
                    {/if}
                    {foreach $documenti['cartelle'] AS $key => $documento}
                    <tr class="pointer">
                        <td><i class="icon-folder"></i> <a href="{$smarty.server.REQUEST_URI}{$documento['gotodir']}">{$documento['nome']}</a></td> {* Aggiungere in cima icona *}
                        <td>{if $documento['dimensioni'] != 0}{$documento['dimensioni']}{/if}</td>
                        <td>{$documento['formato']}</td> 
                        <td>{$documento['ultima_modifica']}</td>
                        <td style='text-align:center;'><input type='checkbox' name='selectdocfile[]' value='{$documento['nome']}' /></td>
                    </tr>
                    {/foreach}
                    {foreach $documenti['files'] AS $key => $documento}
                    <tr class="pointer">
                        <td><i class="{$documento['icona']}"></i> <a href="{$documento['href']}" target="_blank">{$documento['nome']}</a></td> {* Aggiungere in cima icona *}
                        <td>{if $documento['dimensioni'] != 0}{$documento['dimensioni']}{/if}</td>
                        <td>{$documento['formato']}</td> 
                        <td>{$documento['ultima_modifica']}</td>
                        <td style='text-align:center;'><input type='checkbox' name='selectdocfile[]' value='{$documento['nome']}' /></td>
                    </tr>
                    {/foreach}
                </tbody>
            </table>

            <br />

            {* i pulsanti sarebbero dentro ad un form, vediamo se funziona anche usando il form generale *}
            <div class="row"> {* testare! *}
                <input type="hidden" name="current_dir" value="{if $smarty.get.gotodir}{$smarty.get.gotodir}{else}{$directory_base}{/if}" />

                <div class="col-md-1">
                    <button type="submit" id="delete_documento" name="delete_documento" class="btn btn-default">
                        <i class="icon-trash"></i>
                        Elimina selezione
                    </button>
                </div>

                <div class="col-md-3">
                </div>

                <div class="col-md-3">
                Sposta o copia documenti selezionati in: 
                </div>

                <div class="col-md-3">
                    <select id="sposta_in" name="sposta_in" class="form-control textbox-fix">
                    {foreach $documenti['cartelle'] AS $key => $row}
                        <option value="{$row["nome"]}">{$row["nome"]}</option>
                    {/foreach}
                    </select>
                </div>
                <div class="col-md-1">
                    <button type="submit" id="move_documento" name="move_documento" class="btn btn-default">
                        <i class="icon-cut"></i> 
                        Sposta
                    </button>
                </div>
                <div class="col-md-1">
                    <button type="submit" id="copy_documento" name="copy_documento" class="btn btn-default">
                        <i class="icon-copy"></i> 
                        Copia
                    </button>
                </div>
            </div>

        {else}
            <p>
            {l s='The customer has no associated documents folder.'} {* Non esiste una cartella documenti associata a questa anagrafica. *}
            <a href="{$link->getAdminLink('AdminCustomers')|escape:'html':'UTF-8'}&amp;id_customer={$customer->id}&amp;viewcustomer&amp;tab_name=documents&amp;createdocsdir"> Clicca qui per crearla.</a>
            </p>
        {/if}

        <hr />

        <div class="row">
            <div class="col-md-2 form-group">
                <label>Carica un nuovo file</label>
                <p class="form-control-static">
                    <input type="hidden" name="MAX_FILE_SIZE" value="200000000" >
                    <input type="hidden" name="cartella" value="{$documenti['entire_dir']}">
                    <input type="file" multiple id="aggiungiFile" name="aggiungiFile[]" onchange="handleFileSelect(this.files)">
                    <br />
                    <button type="submit" id="carica_file" name="carica_file" class="btn btn-default">
                    <i class="icon-upload-alt"></i>
                    Carica
                </button>
                </p>
            </div>
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-2">
                <label>Crea nuova sottocartella</label>
                <input type="hidden" name="cartella" value="{$documenti['entire_dir']}">
                <input type="text" id="nome_cartella" name="nome_cartella" value="">
                <br />
                <button type="submit" id="crea_sottocartella" name="crea_sottocartella" class="btn btn-default"><i class="icon-folder-close"></i> Crea</button>
            </div>
            <div class="col-md-1">
            </div>
            <div class="col-md-1">
            </div>
        </div>
        
    </div>
</form>
{else}
{$is_agente_activated = 1}
{/if}

{* Javascript della pagina *}
{include file="./js/documenti_js.tpl"}