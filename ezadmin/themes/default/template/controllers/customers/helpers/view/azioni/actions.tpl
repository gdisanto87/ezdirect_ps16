
{* CORREGGERE: aggiungere avviso modifiche all'uscita della pagina, anche in todo.tpl (vedi 1.4) *}

{if $smarty.get.viewpreventivo !== null}
    {assign var=view_get value=$smarty.get.viewpreventivo}
    {assign var=id_get value=$smarty.get.id_thread}
    {assign var=mp value="p"}
    {assign var=mex_preventivo value="preventivo"}
    {assign var=msg_prv value="prv"}
    {assign var=old value=$azioni_cliente['azioni_view']['generale']['preventivo_old']}
    {assign var=old_nota value=$azioni_cliente['azioni_view']['generale']['preventivo_nota']}
    {assign var=messaggia value=$azioni_cliente['azioni_view']['cronologia']['messaggip']}
{elseif $smarty.get.viewmessage !== null}
    {assign var=view_get value=$smarty.get.viewmessage}
    {assign var=id_get value=$smarty.get.id_mex}
    {assign var=mp value="m"}
    {assign var=mex_preventivo value="mex"}
    {assign var=msg_prv value="msg"}
    {assign var=old value=$azioni_cliente['azioni_view']['generale']['mex_old']}
    {assign var=old_nota value=$azioni_cliente['azioni_view']['generale']['mex_nota']}
    {assign var=messaggia value=$azioni_cliente['azioni_view']['cronologia']['messaggixt']}
{/if}

{assign var=azione_mp value=$azioni_cliente['azioni_view']['cronologia']['azione']}
{assign var=storico_ticket value=$azioni_cliente['azioni_view']['storico']['storico_ticket']}
{assign var=tree_ul value=$azioni_cliente['azioni_view']['gerarchia']['tree_ul']}

{* Stile della pagina *}
{include file="./actions_stile.tpl"}

{if $azioni_cliente['azioni_view']['generale']['errore']}

    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="form-row">
                    <div class="row">
                        <div class="alert alert-danger">ERRORE - Azione non trovata</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

{else}

{if $id_get !== null && $smarty.get.aprinuovomessaggio === null}

    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="form-row">
                    <div class="row">
                        <h2 class="blu no-margin" style="text-align:center">
                            {if $smarty.get.id_thread !== null}
                            {if $azioni_cliente['azioni_view']['generale']['tipo_richiesta_p'] == 'tirichiamiamonoi'}TI RICHIAMIAMO NOI{else}PREVENTIVO{/if} {$azioni_cliente['azioni_view']['generale']['idunivoco']} <span style="color:black"> - In carico a {$azioni_cliente['azioni_view']['generale']['in_carico_a_firstname']} - Status: {$azioni_cliente['azioni_view']['generale']['status_azione']} {$azioni_cliente['azioni_view']['generale']['st_azione']}</span>
                            {else}
                            MESSAGGIO {$azioni_cliente['azioni_view']['generale']['idunivoco']} {if $azioni_cliente['azioni_view']['generale']['ctrl_mex'] >= 1}<span style="color:black"> - In carico a {$azioni_cliente['azioni_view']['generale']['in_carico_a_firstname']} - Status: {$azioni_cliente['azioni_view']['generale']['status_azione']} {$azioni_cliente['azioni_view']['generale']['st_azione']}</span>{/if}
                            {/if}
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form name="modifica{$mex_preventivo}" id="modifica{$mex_preventivo}" action="{$link_gen_base}tab_name=actions&amp;submit{$mex_preventivo|ucfirst}{if isset($smarty.get.riferimento)}&amp;riferimentoo={$smarty.get.riferimento}{/if}{$token_base}" method="post" autocomplete="off">
        <div class="form-horizontal col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <i class="icon-user"></i>
                    Generale
                </div>

                <div class="form-row">
                    <div class="row">
                        
                        {if $smarty.get.id_thread !== null}
                        <input type="hidden" name="id_customer" value="{$customer->id}" />
						<input type="hidden" name="id_thread" value="{$smarty.get.id_thread}" />
                        <input type="hidden" name="id_preventivo_univoco" value="{$azioni_cliente['azioni_view']['generale']['idunivoco']}" />
                        {else}
                        <input type="hidden" name="id_mex" value="{$smarty.get.id_mex}" />
                        <input type="hidden" name="id_messaggio_univoco" value="{$azioni_cliente['azioni_view']['generale']['idunivoco']}" />
                        {/if}

                        <div class="form-group col-md-12">
                            <label for="{$mex_preventivo}_subject">Oggetto</label>
                            <p class="form-control-static">
                                <textarea name="{$mex_preventivo}_subject" id="{$mex_preventivo}_subjectContent" {if $azioni_cliente['azioni_view']['generale']['oggetto_readonly']}readonly="readonly"{/if}>{$azioni_cliente['azioni_view']['generale']['oggetto']}</textarea>
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <label for="id_azione">ID {if $smarty.get.id_thread !== null && $azioni_cliente['azioni_view']['generale']['tipo_richiesta_p'] == 'tirichiamiamonoi'}Ti richiamiamo noi{elseif $smarty.get.id_thread !== null}Preventivo{else}Messaggio{/if}</label>
                            <p class="form-control-static">
                                <input class="textbox-fix lettura" type="text" id="id_azione" name="id_azione" value="{$azioni_cliente['azioni_view']['generale']['idunivoco']}" disabled>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="aperto_da">Aperto da</label>
                            <p class="form-control-static">
                                <input class="textbox-fix lettura" type="text" id="aperto_da" name="aperto_da" value="{$azioni_cliente['azioni_view']['generale']['aperto_da']}" disabled>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="data_apertura">Data apertura</label>
                            <p class="form-control-static">
                                <input class="textbox-fix" type="text" id="data_apertura" name="data_apertura" value="{$azioni_cliente['azioni_view']['generale']['data_apertura']}" disabled>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="data_ultima_com">Data ultima com.</label>
                            <p class="form-control-static">
                                <input class="textbox-fix" type="text" id="data_ultima_com" name="data_ultima_com" value="{$azioni_cliente['azioni_view']['generale']['data_ultima_com']}" disabled>
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <label for="assegnaimpiegato{$mp}">In carico a</label>
                            <p class="form-control-static">
                                <select id="assegnaimpiegato{$mp}" name="assegnaimpiegato{$mp}" class="form-control textbox-fix lettura">
                                    <option value="0" {if $row["id_employee"] == 0} selected{/if}>Da assegnare</option>
                                    {foreach $azioni_cliente['azioni_view']['generale']['in_carico_a_options'] AS $key => $row}
                                    <option value="{$row["id_employee"]}"{if $row["id_employee"] == $azioni_cliente['azioni_view']['generale']['azione']['id_employee']} selected{/if}>{$row["firstname"]}</option>
                                    {/foreach}
                                </select>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="id_contact">Tipo {if $smarty.get.id_thread !== null}richiesta{else}attività{/if}</label>
                            <p class="form-control-static">
                                <select id="id_contact" name="id_contact">
                                    {foreach $azioni_cliente['azioni_view']['action_types'] as $tipo}
                                    <option value="{$tipo['value']}" {if $azioni_cliente['azioni_view']['action_type'] == "{$tipo['value']}" } selected{/if}>{$tipo['name']}</option>
                                    {/foreach}
                                </select>
                            </p>
                        </div>

                        {if $smarty.get.id_thread !== null || $azioni_cliente['azioni_view']['generale']['ctrl_mex'] >= 1}
                        <div class="form-group col-md-2">
                            <label for="cambiastatus{$mp}">Status {$azioni_cliente['azioni_view']['generale']['status_azione']}</label>
                            <p class="form-control-static">
                                <select id="cambiastatus{$mp}" name="cambiastatus{$mp}" class="form-control textbox-fix lettura">
                                    <option value="closed"{if $azioni_cliente['azioni_view']['generale']['azione']['status'] == "closed" } selected{/if}>Chiuso</option>
                                    <option value="pending1"{if $azioni_cliente['azioni_view']['generale']['azione']['status'] == "pending1" } selected{/if}>In lavorazione</option>
                                    <option value="open"{if $azioni_cliente['azioni_view']['generale']['azione']['status'] == "open" || $azioni_cliente['azioni_view']['generale']['azione']['status'] == ""} selected{/if}>Aperto</option>
                                </select>
                            </p>
                        </div>
                        {/if}
                    </div>

                    {if $azioni_cliente['azioni_view']['generale']['azione']['riferimento'] != '' OR $azioni_cliente['azioni_view']['generale']['azione_padre'] != ''}
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="azione_padre">Azione padre</label>
                            <p class="form-control-static">
                                {$azioni_cliente['azioni_view']['generale']['azione_padre']}
                            </p>
                        </div>
                    </div>
                    {/if}

                    {if $smarty.get.id_thread !== null}
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="azione_padre">Categoria / Prodotto di interesse</label>
                            <p class="form-control-static">
                                <input type="text" value="{$azioni_cliente['azioni_view']['generale']['cp_interest']}" readonly />
                            </p>
                        </div>
                    </div>
                    {/if}

                    {if $old}
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="{$mex_preventivo}_note">Nota</label>
                            <p class="form-control-static">
                                <textarea name="{$mex_preventivo}_note" id="{$mex_preventivo}noteContent">{$old_nota|escape:'htmlall'}</textarea>
                            </p>
                        </div>
                    </div>
                    <br /><br />
                    {/if}

                    <div class="row">
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-primary" name="submit{$mex_preventivo|ucfirst}"><i class="icon-save" alt="Salva" title="Salva"></i> {l s='Save'}</button>
                        </div>
                        <div class="form-group col-md-3">
                            <button type="button" class="btn btn-primary" onclick="javascript:var surec=window.confirm('Sei sicuro?'); if (surec) { $('#coll_destinazione').val($('#collega_{$msg_prv}').val()); document.forms['collega_attivita'].submit(); return false; } else { return false; }"><i class="icon-link" alt="Collega" title="Collega"></i> {l s='Connect to'}</button>
                            <p class="form-control-static">
                                <select class="form-control textbox-fix" name="collega_{$msg_prv}" id="collega_{$msg_prv}">
                                    <option value="">-- Seleziona --</option>
                                    {$azioni_cliente['azioni_view']['generale']['collega_a']}
                                </select>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="form-horizontal col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <i class="icon-eye-close"></i> {l s='Private notes'}

                    <div class="panel-heading-action">
                        <a class="btn btn-default add-new">
                            <i class="icon-plus-sign"></i>
                            {l s='Add private note'}
                        </a>
                    </div>
                </div>
                <div class="form-group">
                    <table class="table table-bordered tabella_note" id="noteTable">
                        <thead>
                            <tr>
                                <th>{l s='Note'}</th>
                                <th>{l s='Created by'}</th>
                                <th>{l s='Last edit'}</th>
                                <th>{l s='Actions'}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="display:none"> {* Necessario per aggiungere la prima nota in js *}
                                <td class="mod"></td>
                                <td></td>
                                <td></td>
                                <td class="azioni_note">
                                    <a class="add" title="Add" data-toggle="tooltip"><i class="icon-save"></i></a>
                                    <a class="edit" title="Edit" data-toggle="tooltip"><i class="icon-edit"></i></a>
                                    <a class="delete" title="Delete" data-toggle="tooltip"><i class="icon-trash"></i></a>
                                </td>
                            </tr>
                        {foreach $azioni_cliente['azioni_view']['generale']['azione_note'] as $azione_nota}
                            <tr dbid="{$azione_nota['id_note']}" id="tr_note_{$azione_nota['id_note']}">
                                <td id="note_nota[{$azione_nota['id_note']}]" class="mod">{$azione_nota['note']}</td>
                                <td id="note_id_employee[{$azione_nota['id_note']}]">{$azione_nota['creato_da']}</td>
                                <td id="note_date_add[{$azione_nota['id_note']}]">{$azione_nota['date_upd']}</td>
                                <td class="azioni_note">
                                    <a class="add" title="Add" data-toggle="tooltip"><i class="icon-save"></i></a>
                                    <a class="edit" title="Edit" data-toggle="tooltip"><i class="icon-edit"></i></a>
                                    <a class="delete" title="Delete" data-toggle="tooltip"><i class="icon-trash"></i></a>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </form>

{/if}

    <div class="form-horizontal col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-user"></i>
                {if $id_get !== null && $smarty.get.aprinuovomessaggio === null}
                Cronologia
                {else}
                Nuovo Messaggio
                {/if}
            </div>

            {if $id_get !== null && $smarty.get.aprinuovomessaggio === null}

            {foreach $messaggia AS $key => $messaggioa}
                <div style="{if $messaggioa['id_employee'] != 0} background: rgba(255, 192, 203, 0.5); {/if} border: 1px solid lightpink; margin-bottom:1%;">
                    <div class="row" style="padding-left:2%; padding-top:1%;">
                        <div class="form-group col-md-3">
                            <label>Da</label>
                            <p class="form-control-static">
                                {$messaggioa['da']}
                            </p>
                        </div>

                        <div class="form-group col-md-3">
                            <label>Data</label>
                            <p class="form-control-static">
                                {$messaggioa['data']}
                            </p>
                        </div>

                        <div class="form-group col-md-3">
                            <label>Allegati</label>
                            <p class="form-control-static">
                                {$messaggioa['allegati']}
                            </p>
                        </div>
                    </div>

                    <div class="row" style="padding-left:2%;">
                        <div class="form-group col-md-8">
                            <label>Messaggio</label>
                            <p class="form-control-static">
                                {$messaggioa['message']}
                            </p>
                        </div>
                        {if $messaggioa['id_employee'] == 0}
                            {* E' un messaggio del cliente, quindi non si può modificare *}
                        {else}
                        <div class="form-group col-md-4">
                            <form method="post">
                                {if $smarty.get.id_thread !== null}
                                <input type="hidden" name="modifica_messaggio_preventivo" value="{$messaggioa['message_modifica']|escape:'htmlall'}" />
                                {else}
                                <input type="hidden" name="modifica_messaggio_messaggio" value="{$messaggioa['message_modifica']|escape:'htmlall'}" />
                                {/if}
                                <button type="submit" class="btn btn-secondary" name="modifica_messaggio_submit" value="Modifica">Modifica</button> 
                            </form>
                            {if $smarty.get.id_thread !== null}
                            <a class="button_invio" style="cursor:pointer" onclick="javascript: var sure=window.confirm('Sei sicuro di reinviare questo messaggio?'); if (sure) { submitFormOkay = false; document.location = '{$link_gen_base}tab_name=actions&amp;azione=actions&amp;viewpreventivo&id_thread={$id_get}&reinvia={$messaggioa['id_message']}{$token_base}' } else { }">
                                <img src="../img/admin/email.gif" alt="Reinvia" title="Reinvia" />&nbsp;&nbsp;&nbsp;Reinvia
                            </a>
                            {/if}
                        </div>
                        {/if}
                    </div>

                    {if $smarty.get.id_thread !== null && $messaggioa['note_private']}
                    <div class="row rosso" style="padding-left:2%;">
                        <div class="form-group col-md-3">
                            <label>Nota privata</label>
                            <p class="form-control-static">
                                {$messaggioa['note_private']}
                            </p>
                        </div>
                    </div>
                    {/if}

                    <div class="row" style="padding-left:2%;">
                        {if $messaggioa['email'] != ''}
                        <div class="form-group col-md-3">
                            <label>Inviato a</label>
                            <p class="form-control-static">
                                {$messaggioa['employee_to_name']}
                            </p>
                        </div>
                        {/if}

                        <div class="form-group col-md-3">
                            <label>In carico a</label>
                            <p class="form-control-static">
                                {$messaggioa['in_carico_a_name']}
                            </p>
                        </div>

                        {if $smarty.get.id_thread !== null}
                            {* Correggere: Non mostro telefono cliente? *}
                        {else}
                        <div class="form-group col-md-3">
                            <label>Telefono cliente</label>
                            <p class="form-control-static">
                                <a href="callto:{$messaggioa['telefono_cliente']}">{$messaggioa['telefono_cliente']}</a>
                            </p>
                        </div>
                        {/if}

                        <div class="form-group col-md-3">
                            <label>CC</label>
                            <p class="form-control-static">
                                {$messaggioa['cc']}
                            </p>
                        </div>
                    </div>

                    {if $check_employee}
                    <div class="row" style="padding-left:2%;">
                        <div class="form-group col-md-12">
                            <label>Inoltra CC a</label>
                            <p class="form-control-static">
                                {$messaggioa['inoltra_string']}
                                <button class="btn btn-secondary" onclick="javascript:var surec=window.confirm('Sei sicuro?'); if (surec) { var checkedVals = $('.inoltrap:checkbox:checked').map(function() { return this.value; }).get(); inoltraP(checkedVals.join(',')); } ">
                                    <img src="../img/admin/email.gif" alt="Inoltra" title="Inoltra" />&nbsp;&nbsp;&nbsp;Inoltra
                                </button>
                            </p>
                        </div>
                    </div>
                    {/if}

                    <div class="row" style="padding-left:2%;">
                    </div>
                </div>
            {/foreach}

            {/if}


<form name="submit_{$mex_preventivo}" id="submit_{$mex_preventivo}" action="{$link_gen_base}tab_name=actions&amp;azione=actions&amp;{if $id_get !== null && $smarty.get.aprinuovomessaggio === null}{if $smarty.get.viewpreventivo !== null}viewpreventivo&amp;id_thread={$id_get}{else}viewmessage&amp;id_mex={$id_get}{/if}{else}&amp;aprinuovomessaggio{/if}{if isset($smarty.get.riferimento)}&amp;riferimentoo={$smarty.get.riferimento}{/if}{$token_base}" method="post" enctype="multipart/form-data">
            
            {if $id_get !== null && $smarty.get.aprinuovomessaggio === null}
            <h2 id="rispondi-azione">Rispondi al cliente</h2>
            {elseif $smarty.get.aprinuovomessaggio !== null}
            <h2 id="apri-azione">Invia messaggio al cliente</h2>
            {elseif $smarty.get.aprinuovopreventivo !== null} {* Verificare che questa azione esista ancora *}
            <h2 id="apri-azione">Invia preventivo al cliente</h2>
            {/if}

            <br />
            
            <input type="hidden" name="id_customer" value="{$customer->id}" />
            {if $smarty.get.id_thread !== null}
			<input type="hidden" name="id_thread" value="{$smarty.get.id_thread}" />
            {else}
			<input type="hidden" name="id_customer_thread" value="{$azioni_cliente['azioni_view']['cronologia']['thread']}" />
            {/if}

            {if $smarty.get.id_thread !== null && $smarty.get.aprinuovomessaggio === null}
            <div class="row">
                <span><strong>Prodotto / Categoria di interesse:</strong> {$azioni_cliente['azioni_view']['cronologia']['cp_interest']}</span>
            </div>
            <br />
            {/if}

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="email">Email</label>
                    <div class="row form-control-static">
                        {if $customer->is_company == 1}
                        <div class="col-md-3">
                            <select name="seleziona-persona{$mp}" onchange="javascript:document.getElementById('customer_email_msg_{$mp}').value = (this.value);">
								<option value=''>-- Seleziona persona --</option>
								{foreach $azioni_cliente['azioni_view']['cronologia']['persone'] as $persona}
								<option value="{$persona['email']}" {if $persona['id_persona'] == $smarty.get.id_persona || $persona['id_persona'] == $azioni_cliente['azioni_view']['cronologia']['persona_preventivo']}selected{/if}>{$persona['firstname']} {$persona['lastname']}</option>
                                {/foreach}
                            </select>
                        </div>
                        {/if}
                        <div class="col-md-9">
                            <input class="textbox-fix" type="text" name="customer_email_msg_{$mp}" id="customer_email_msg_{$mp}" value="{if !$azione_mp['email']}{$azioni_cliente['azioni_view']['cronologia']['mail_persona']}{else}{$azione_mp['email']}{/if}" />
                            <span style="font-size:11px">In caso di più indirizzi mail, separare con punto e virgola (;)</span>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-2">
                    <label>Tel. Cliente</label>
                    <p class="form-control-static">
                        <input type="text" name="telefono_cliente" id="telefono_cliente" value="{$azioni_cliente['azioni_view']['cronologia']['telefono_cliente']}" readonly />
                        <a id="tel_action" href="callto:{$azioni_cliente['azioni_view']['cronologia']['telefono_cliente']}">Chiama</a>
                    </p>
                </div>
            </div>

            {if $id_get !== null && $smarty.get.aprinuovomessaggio === null}

                {if $smarty.get.id_mex !== null}
                <input type="hidden" name="isMail" value="1"> {* Eliminabile: i ticket e i messaggi sono stati separati *}
                <input type="hidden" name="isAnswer" value="1" />
                {/if}

                <div class="row">
                    <input type="hidden" name="action_type" value="{$azione_mp['action_type']}" />

                    <div class="form-group col-md-2">
                        <label for="status{$mp}">Stato azione</label>
                        <p class="form-control-static">
                            <select id="status{$mp}" name="status{$mp}" class="form-control textbox-fix lettura">
                                <option value="open" {if $azione_mp['status'] == "open" } selected{/if}>Aperto</option>
                                <option value="pending1" {if $azione_mp['status'] == "pending1" } selected{/if}>In lavorazione</option>
                                <option value="closed" {if $azione_mp['status'] == "closed" } selected{/if}>Chiuso</option>
                            </select>
                        </p>
                    </div>

                    <div class="form-group col-md-2">
                        <label for="in_carico_a_{$mp}">In carico a</label>
                        <p class="form-control-static">
                            <select id="in_carico_a_{$mp}" name="in_carico_a_{$mp}" class="form-control textbox-fix" onChange="changeInCaricoA{$mp|upper}();">
                            {foreach $azioni_cliente['azioni_view']['cronologia']['impiegati_eza'] as $impez}
                                <option value="{$impez['id_employee']}" {if $impez['id_employee'] == $azioni_cliente['azioni_view']['cronologia']['impez_selected']} selected{/if}>{$impez['firstname']}</option>
                            {/foreach}
                            </select>
                        </p>
                    </div>
                </div>

            {else}
                <input type="hidden" name="isMail" value="1"> {* Eliminabile: i ticket e i messaggi sono stati separati *}
                <input type="hidden" name="nuovomessaggio" value="1" />
                <input type="hidden" name="riferimento" id="riferimento" value="{if isset($smarty.get.riferimento)}{$smarty.get.riferimento}{/if}" />
                <input type="hidden" name="riferimentoo" id="riferimentoo" value="{if isset($smarty.get.riferimento)}{$smarty.get.riferimento}{/if}" />
            {/if}

            <div class="row">
                <label>Conoscenza</label>
                <br />
                {foreach $azioni_cliente['azioni_view']['cronologia']['impiegati_eza'] AS $impez}
                    <input type="checkbox" name="conoscenza_{$mp}[]" id="conoscenza_{$impez['id_employee']}" value="{$impez['id_employee']}"> {$impez['firstname']}&nbsp;&nbsp;&nbsp;&nbsp; 
                {/foreach}
            </div>

            <br />

            <div class="row"> {* Correggere anteprima *}
                <div class="form-group col-md-6">
                    <label>Allega file</label>
                    <p class="form-control-static">
                        <input type="hidden" name="MAX_FILE_SIZE" value="20000000">
                        <input type="file" multiple class="multi" name="joinFile[]" id="joinFile" onchange="handleFileSelect(this.files)">
                        <output id="list-action">Anteprime: </output>
                    </p>
                </div>
            </div>

            <br />

            <div class="row" id="aggiungi_prodotti">

                <div class="form-row">
                    <div class="col-md-1">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                        <label class="form-check-label" for="inlineCheckbox1">Online</label>
                    </div>
                    <div class="col-md-1">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                        <label class="form-check-label" for="inlineCheckbox1">Offline</label>
                    </div>
                    <div class="col-md-1">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                        <label class="form-check-label" for="inlineCheckbox1">Old</label>
                    </div>
                    <div class="col-md-1">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                        <label class="form-check-label" for="inlineCheckbox1">{l s='Only av.'}</label> {* Solo disp. *}
                    </div>

                    <div class="col-md-2">
                        <select id="brand" class="chosen form-control" name="brand">
                            <option value="">{l s='Brand...'}</option>
                            {foreach $azioni_cliente['azioni_view']['cronologia']['marche'] AS $key => $marche}
                                <option value="{$marche['id']}">{$marche['name']}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select id="serie" class="chosen form-control" name="serie">
                            <option value="">{l s='Series...'}</option>
                            <option value="">Scegli prima un costruttore</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select id="categorie" class="chosen form-control" name="categorie">
                            <option value="">{l s='Category...'}</option>
                            {foreach $azioni_cliente['azioni_view']['cronologia']['categorie'] AS $key => $categorie}
                                <option value="{$categorie['id']}">{$categorie['name']}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select id="fornitori" class="chosen form-control" name="fornitori">
                            <option value="">{l s='Supplier...'}</option>
                            {foreach $azioni_cliente['azioni_view']['cronologia']['fornitori'] AS $key => $fornitori}
                                <option value="{$fornitori['id']}">{$fornitori['name']}</option>
                            {/foreach}
                        </select>
                    </div>

                    <div class="col-md-12">
                        <input type="text">
                    </div>
                </div>
            </div>

            <br />

            <div class="row">

                <div class="form-group col-md-6">
                    <label for="link_utili">Link utili</label>
                    <p class="form-control-static">
                        <select {if $smarty.get.id_thread !== null}id="utils_autocomplete_input_preventivo" name="utils_autocomplete_input_preventivo"{else}id="utils_autocomplete_input_messaggio" name="utils_autocomplete_input_messaggio"{/if} class="select2 form-control textbox-fix" onchange="addUtil_TR();">
                            {$utils_options}
                        </select>
                    </p>
                </div>

                <div class="form-group col-md-6">
                    <label for="messaggi_precompilati">Messaggi precompilati</label>
                    <p class="form-control-static">
                        <select id="messaggi_precompilati" name="messaggi_precompilati" class="select2 form-control textbox-fix" onchange="inserisciPrecompilato(this.value);">
                            <option value="-- Seleziona un messaggio --">-- Seleziona un messaggio --</option>
                        {foreach $azioni_cliente['azioni_view']['cronologia']['precompilati'] as $precompilato}
                            <option value="{$precompilato['testo']|escape:'htmlall'}">{$precompilato["oggetto"]|escape:'htmlall'}</option>
                        {/foreach}
                        </select>
                    </p>
                </div>

            </div>

		    {if $azioni_cliente['azioni_view']['cronologia']['allega_bdl'] && $azioni_cliente['azioni_view']['cronologia']['allega_bdl'] !== ''}
                <input type="hidden" name="invio_con_bdl_pdf" value="{$azioni_cliente['azioni_view']['allega_bdl']}" />
                <a href="{$link_gen_base}tab_name=bdl&amp;id_bdl={$azioni_cliente['azioni_view']['cronologia']['allega_bdl']}&amp;getPDFBDL=ezdirect{$token_base}">{$azioni_cliente['azioni_view']['cronologia']['allega_bdl']}-buono-di-lavoro.pdf</a>
                <br /><br />
            {/if}

            <div class="row">
                <div class="col-lg-12">
                    <label>Messaggio</label>
                    {if $smarty.get.id_thread !== null}
                    <textarea class="rte autoload_rte" id="messaggiop" name="messaggiop" rows="5" cols="40">{if isset($smarty.post.modifica_messaggio_submit)}{$smarty.post.modifica_messaggio_preventivo|stripslashes}{/if}</textarea>
                    {else}
                    <textarea class="rte autoload_rte" id="messaggio" name="messaggio" rows="5" cols="40">{if isset($smarty.post.modifica_messaggio_submit)}{$smarty.post.modifica_messaggio_messaggio|stripslashes}{/if}</textarea>
                    {/if}
                </div>
            </div>

            <br />

            <div class="row">
                <div class="col-lg-12">
                    <label class="rosso">Note private visibili solo da staff</label>
                    <span class="button thick pointer" style="display:block; text-align:center; background-color:#f0f0ee; border:1px solid #cccccc; text-decoration:none;" onclick="$('#mostra_nota_privata').slideToggle();"><img src="../img/admin/arrow.gif" alt="Dettaglio note private" title="Dettaglio note private" />Clicca qui per inserire note private/riservate - Non visibili all'utente.</span>
                    <div id="mostra_nota_privata" style="display:none;">
                        <textarea class="rte autoload_rte" id="note_private" name="note_private" rows="5" cols="20"></textarea>
                    </div>
                </div>
            </div>

            <br />

            {if $azioni_cliente['azioni_view']['cronologia']['riferimento'] != '' && $azioni_cliente['azioni_view']['cronologia']['statusPadre'] != 'closed' && $azioni_cliente['azioni_view']['cronologia']['statusPadre'] != ''}
            <div class="row">
                <div style="border:3px solid red; padding:5px;"><input type="checkbox" name="chiudi_padre" />&nbsp;&nbsp;&nbsp;<strong style="color:red">ATTENZIONE!</strong> L'azione padre risulta ancora aperta. Spunta questa casella se vuoi chiuderla.</div>
            </div>
            {/if}

            <br />

            {if isset($smarty.post.modifica_messaggio_submit)} {* correggere *}
			<input type='hidden' name='modifica_messaggio_azione_id' value='{$smarty.post.modifica_messaggio_azione_id}' />
			{/if}

            {if $smarty.get.id_thread !== null}
            <button type="submit" class="btn btn-secondary" name="submitPreventivoReply" id="submitPreventivoReply" value="Modifica"><i class="icon-envelope"></i> Invia</button>
            {else}
            <input name="mail_solo_testo" type="checkbox" />Invia come mail di solo testo (senza HTML)
            <br /><br />
            <button type="submit" class="btn btn-secondary" name="submitMessage" id="submitMessage" value="Modifica"><i class="icon-envelope"></i> Invia</button>
            {/if}

        </div>
    </div>

    {if isset($smarty.post.modifica_messaggio_submit)}
    <script type="text/javascript"> window.location = "#rispondi-azione"; </script>
    {/if}

    {if isset($tinymce) && $tinymce}
    <script type="text/javascript">
        var iso = '{$iso|addslashes}';
        var pathCSS = '{$smarty.const._THEME_CSSDIR|addslashes}';
        var ad = '{$ad|addslashes}';

        $(document).ready(function(){
            {block name="autoload_tinyMCE"}
                tinySetup({
                    editor_selector :"autoload_rte"
                });
            {/block}
        });
    </script>
    {/if}

</form>

{if $id_get !== null && $smarty.get.aprinuovomessaggio === null}

    <div class="form-horizontal col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-user"></i>
                Gerarchia
            </div>

            <div class="tree">
                {$tree_ul}
            </div>
        </div>
    </div>

    <div class="form-horizontal col-lg-6">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-user"></i>
                Storico
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered" id="storicoTable">
                        <thead>
                            <tr>
                                <th>Persona</th>
                                <th>Azione</th>
                                <th>Data</th>
                            </tr>
                        </thead>

                        <tbody>
                        {foreach $storico_ticket AS $key => $storico}
                            <tr>
                                <td>{$storico['persona']}</td>
                                <td>{$storico['azione']}</td>
                                <td>{$storico['data_attivita']}</td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <form name="collega_attivita" id="collega_attivita" action="{$smarty.server.REQUEST_URI}&conf=4" method="post">
        <input type="hidden" name="submitCollegaAttivita" value="" />
        <input type="hidden" name="coll_destinazione" id="coll_destinazione" value="" />
        {if $smarty.get.id_thread !== null}
        <input type="hidden" name="coll_partenza" id="coll_partenza" value="P{$azioni_cliente['azioni_view']['generale']['azione']['id_thread']}" />
        {else}
        <input type="hidden" name="coll_partenza" id="coll_partenza" value="T{$azioni_cliente['azioni_view']['generale']['azione']['id_customer_thread']}" />
        {/if}
    </form>

{/if}

{/if}

{* Javascript della pagina *}
{include file="./actions_js.tpl"}

<script type="text/javascript" src="jquery.MultiFile.js"></script>