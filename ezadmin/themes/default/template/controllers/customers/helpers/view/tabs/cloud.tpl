{if $is_agente == 0}
				
{* CENTRALINO YEASTAR CLOUD *}
<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-envelope"></i> {l s='Active YEASTAR CLOUD virtual PBX'} <span class="badge squadrato">{count($yeastar_cloud['centralini'])}</span>

            <div class="panel-heading-action">
                {if ! count($ezcloud['centralini'])}
                <a class="btn btn-default" href="{$current|escape:'html':'UTF-8'}&amp;updatecustomer&amp;id_customer={$customer->id|intval}&amp;token={$token|escape:'html':'UTF-8'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
                    <i class="icon-plus-sign"></i>
                    {l s='Activate Ezdirect PBX for this customer'}
                </a>
                {/if}
            </div>
        </div>
        {if count($yeastar_cloud['centralini'])}
        <table class="table">
            <thead>
                <tr>
                    <th><span class="title_box ">{l s='ID'}</span></th>
                    <th><span class="title_box ">{l s='Activated on'}</span></th>
                    <th><span class="title_box ">{l s='Renewal date'}</span></th>
                    <th><span class="title_box ">{l s='Qt. internal'}</span></th>
                    <th><span class="title_box ">{l s='Qt. cc'}</span></th>
                    <th><span class="title_box ">{l s='Internal single cost'}</span></th>
                    <th><span class="title_box ">{l s='Total cost'}</span></th>
                    <th><span class="title_box ">{l s='Paid'}</span></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {foreach $yeastar_cloud['centralini'] AS $key => $centralino}
                <tr>
                    <td>{$centralino['id_ezcloud']}</td>
                    <td>{$centralino['date_add']}</td>
                    <td>{$centralino['date_ren']}</td>
                    <td>{$centralino['interni']}</td> 
                    <td>{$centralino['linee']}</td> 
                    <td>{$centralino['prezzo']}</td>
                    <td>{$centralino['prezzo_totale']}</td>
                    <td>{if $centralino['pagato'] == 0}<i class="icon-remove rosso"></i>{elseif $centralino['pagato'] == 1}<i class="icon-check verde"></i>{else}<i class="icon-question"></i>{/if}</td>
                    <td>
                        <button type="button" class="btn btn-default" data-toggle="collapse" data-target="#dett_ye_{$centralino['id_ezcloud']}" aria-expanded="false" aria-controls="collapse_ra"><i class="icon-zoom-in"></i></button>
                        <button type="button" class="btn btn-default">{l s='act'}</button>
                    </td>
                    
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <div class="collapse" id="dett_ye_{$centralino['id_ezcloud']}">
                        <h2>Yeastar Cloud n.{$centralino['id_ezcloud']}</h2>
                        <h4>{l s='Paid: '}{$centralino['pagato']}</h4>
                        {*
                        <h4>{l s='Customer evaluation'}</h4>
                        
                        {l s='Quality of service: '}{$centralino['servizio']}
                        <br />
                        {l s='PBX Functionality: '}{$centralino['funzionalita']}
                        <br />
                        {l s='Support quality: '}{$centralino['supporto']}
                        
                        <br />
                        <br />
                        *}
                        </div>
                    </td>
                </tr>
                
                {/foreach}
            </tbody>
        </table>
        {else}
        <p>{l s='This customer does not yet have any active Yeastar PBX.'}</p>
        {/if}
        
    </div>
</form>


{* CENTRALINO EZCLOUD *}
<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-envelope"></i> {l s='Active EZCLOUD virtual PBX'} <span class="badge squadrato">{count($ezcloud['centralini'])}</span>

            <div class="panel-heading-action">
                {if ! count($ezcloud['centralini'])}
                <a class="btn btn-default" href="{$current|escape:'html':'UTF-8'}&amp;updatecustomer&amp;id_customer={$customer->id|intval}&amp;token={$token|escape:'html':'UTF-8'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
                    <i class="icon-plus-sign"></i>
                    {l s='Activate Ezdirect PBX for this customer'}
                </a>
                {/if}
            </div>
        </div>
        {if count($ezcloud['centralini'])}
        <table class="table">
            <thead>
                <tr>
                <th><span class="title_box ">{l s='ID'}</span></th>
                <th><span class="title_box ">{l s='Activated on'}</span></th>
                <th><span class="title_box ">{l s='Renewal date'}</span></th>
                <th><span class="title_box ">{l s='Qt. internal'}</span></th>
                <th><span class="title_box ">{l s='Qt. lines'}</span></th>
                <th><span class="title_box ">{l s='Internal single cost'}</span></th>
                <th><span class="title_box ">{l s='Total cost'}</span></th>
                <th><span class="title_box ">{l s='Paid'}</span></th>
                <th></th>
                </tr>
            </thead>
            <tbody>
                {foreach $ezcloud['centralini'] AS $key => $centralino}
                <tr>
                <td>{$centralino['id_ezcloud']}</td>
                <td>{$centralino['date_add']}</td>
                <td>{$centralino['date_ren']}</td>
                <td>{$centralino['interni']}</td> 
                <td>{$centralino['linee']}</td> 
                <td>{$centralino['prezzo']}</td>
                <td>{$centralino['prezzo_totale']}</td>
                <td>{if $centralino['pagato'] == 0}<i class="icon-remove rosso"></i>{elseif $centralino['pagato'] == 1}<i class="icon-check verde"></i>{else}<i class="icon-question"></i>{/if}</td>
                <td>
                    <button type="button" class="btn btn-default" data-toggle="collapse" data-target="#dett_ez_{$centralino['id_ezcloud']}" aria-expanded="false" aria-controls="collapse_ra"><i class="icon-zoom-in"></i></button>
                    <button type="button" class="btn btn-default">{l s='act'}</button>
                </td>
                    
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <div class="collapse" id="dett_ez_{$centralino['id_ezcloud']}">
                        <h2>Ezcloud n.{$centralino['id_ezcloud']}</h2>
                        <h4>{l s='Paid: '}{$centralino['pagato']}</h4>
                        <h4>{l s='Customer evaluation'}</h4>
                        
                        {l s='Quality of service: '}{$centralino['servizio']}
                        <br />
                        {l s='PBX Functionality: '}{$centralino['funzionalita']}
                        <br />
                        {l s='Support quality: '}{$centralino['supporto']}
                        
                        <br />
                        <br />
                        </div>
                    </td>
                </tr>
                
                {/foreach}
            </tbody>
        </table>
        {else}
        <p>{l s='This customer does not yet have any active Ezcloud PBX.'}</p>
        {/if}
        
    </div>
</form>

{else}
{$is_agente_activated = 1}
{/if}