<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading-action">
                <div class="bottoni-tab">
                    <a class="btn btn-default" href="{$link_gen_base}tab_name=contracts{$token_base}">
                        <i class="icon-circle-arrow-left"></i>
                        Vedi contratti del cliente
                    </a>
                    <a class="btn btn-default" href="{$link->getAdminLink('AdminContratti')|escape:'html':'UTF-8'}">
                        <i class="icon-circle-arrow-left"></i>
                        Vedi tutti i contratti
                    </a>
                </div>
            </div>
            
            <div class="form-row">
                <div class="row">
                    <h2 class="blu" style="text-align:center">
                        Contratto assistenza n.{$contratti['contratti_view']['dati']['id_contratto']}
                    </h2>
                </div>
            </div>

            <div class="form-row">
                <div class="row" style="text-align:center">
                    <a class="btn btn-primary" href="{$link_gen_base}tab_name=contracts&amp;azione=contratto_edit&amp;id_contratto={$contratti['contratti_view']['dati']['id_contratto']}{$token_base}"><i class="icon-pencil"></i> Modifica il contratto</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-user"></i> 
                Contratto
            </div>

            <div class="form-row">
                <div class="row">
                    <div class="form-group col-md-2">
                        <label>N. contratto</label>
                        <p class="form-control-static">
                            {$contratti['contratti_view']['dati']['id_contratto']}
                        </p>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Rif. fattura</label>
                        <p class="form-control-static">
                            {$contratti['contratti_view']['dati']['rif_fattura']}
                        </p>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Rif. noleggio</label>
                        <p class="form-control-static">
                            {$contratti['contratti_view']['dati']['rif_noleggio']}
                        </p>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Rif. ordine</label>
                        <p class="form-control-static">
                            {$contratti['contratti_view']['dati']['rif_ordine']}
                        </p>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Tipo</label>
                        <p class="form-control-static">
                            {$contratti['contratti_view']['dati']['tipo_contratto']}
                        </p>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Stato</label>
                        <p class="form-control-static">
                            {$contratti['contratti_view']['dati']['status_contratto']}
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-2">
                        <label>Importo</label>
                        <p class="form-control-static">
                            {$contratti['contratti_view']['dati']['importo']}
                        </p>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Cadenza</label>
                        <p class="form-control-static">
                            {$contratti['contratti_view']['dati']['cadenza']}
                        </p>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Competenza dal</label>
                        <p class="form-control-static">
                            {$contratti['contratti_view']['dati']['competenza_dal']}
                        </p>
                    </div>
                    <div class="form-group col-md-2">
                        <label>al</label>
                        <p class="form-control-static">
                            {$contratti['contratti_view']['dati']['competenza_al']}
                        </p>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Rata</label>
                        <p class="form-control-static">
                            {$contratti['contratti_view']['dati']['rata']}
                        </p>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Pagamento</label>
                        <p class="form-control-static">
                            {$contratti['contratti_view']['dati']['pagamento']}
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-2">
                        <label>CIG</label>
                        <p class="form-control-static">
                            {$contratti['contratti_view']['dati']['cig']}
                        </p>
                    </div>
                    <div class="form-group col-md-2">
                        <label>CUP</label>
                        <p class="form-control-static">
                            {$contratti['contratti_view']['dati']['cup']}
                        </p>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Cod. univoco ufficio x fatt. elettronica</label>
                        <p class="form-control-static">
                            {$contratti['contratti_view']['dati']['univoco']}
                        </p>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Data registrazione</label>
                        <p class="form-control-static">
                            {$contratti['contratti_view']['dati']['data_registrazione']}
                        </p>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Data inizio</label>
                        <p class="form-control-static">
                            {$contratti['contratti_view']['dati']['data_inizio']}
                        </p>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Data fine</label>
                        <p class="form-control-static">
                            {$contratti['contratti_view']['dati']['data_fine']}
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-4">
                        <label>Sede</label>
                        <p class="form-control-static">
                            {$contratti['contratti_view']['dati']['sede']}
                        </p>
                    </div>
                    {if $contratti['contratti_view']['dati']['blocco_amministrativo'] == 1}
                    <div class="form-group col-md-2 rosso">
                        <label>Blocco amministrativo</label>
                        <p class="form-control-static rosso">
                            PRESENTE
                        </p>
                    </div>
                    {/if}

                    <div class="col-md-2">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th><span class="title_box center_text">{l s='Paid'}</span></th>
                                    <th><span class="title_box center_text">{l s='Billed'}</span></th>
                                </tr>
                            </thead>
                            <tbody class="center">
                                <tr>
                                    <td class="text-center"><span name="pagato" id="pagato">{if $contratti['contratti_view']['dati']['pagato'] == "1"}<i class="icon-check verde"></i>{else}<i class="icon-remove rosso"></i>{/if}</span></td>
                                    <td class="text-center"><span name="fatturato" id="fatturato">{if $contratti['contratti_view']['dati']['fatturato'] == "1"}<i class="icon-check verde"></i>{else}<i class="icon-remove rosso"></i>{/if}</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <br />
                    <strong>Prodotti</strong>

                    <table class='table'>
                        <thead>
                            <tr>
                                <th><span class="title_box ">Codice</span></th>
                                <th><span class="title_box ">Descrizione</span></th>
                                <th><span class="title_box ">Qta</span></th>
                                <th><span class="title_box ">Prezzo</span></th>
                                <th><span class="title_box ">Seriale</span></th>
                                <th><span class="title_box ">Indirizzo </span></th>
                            </tr>
                        </thead>
                        <tbody id='tableProductsBody'>
                            {if count($contratti['contratti_view']['prodotti'])}
                            {foreach $contratti['contratti_view']['prodotti'] AS $key => $prodotto}
                            <tr id='tr_{$prodotto['id_product']}'>
                                <td>{if $prodotto['id_product']}<a class="tooltip_prodotto" title="{$prodotto['tooltip']}" href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}&amp;id_product={$prodotto['id_product']|intval}&amp;updateproduct&amp;token={getAdminToken tab='AdminProducts'}" target="_blank">{$prodotto['codice_prodotto']}</a>{else}{$prodotto['codice_prodotto']}{/if}</td>
                                <td>{$prodotto['descrizione_prodotto']}</td>
                                <td class="text-right">{$prodotto['quantita']}</td>
                                <td class="text-right">{$prodotto['prezzo_format']}</td>
                                <td>{$prodotto['seriale']}</td>
                                <td>{$prodotto['indirizzo']}</td>
                            </tr>
                            {/foreach}
                            {/if}
                        </tbody>
                    </table>
                </div>

                <div class="row">
                    <div class="form-group col-md-8">
                        <label for="note_private">Note private</label>
                        <textarea name="note_private" id="note_private" rows="4" readonly="readonly">{$contratti['contratti_view']['dati']['note_private']|escape:"htmlall"}</textarea>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-user"></i> 
                Storico
            </div>

            <div class="form-row">
                <div class="row">
                    {if count($contratti['contratti_view']['storico']) == 0}
                        <p>Nessuna informazione</p>
                    {else}
                        <table class="table">
                            <thead>
                                <tr>
                                    <th><span class="title_box ">Data</span></th>
                                    <th><span class="title_box ">Dati storici</span></th>
                                </tr>
                            </thead>
                            <tbody>
                            {foreach $contratti['contratti_view']['storico'] as $storico}
                                <tr>
                                    <td>{$storico['data_attivita']}</td>
                                    <td>{$storico['desc_attivita']}</td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    {/if}
                </div>
            </div>
        </div>
    </div>
</div>