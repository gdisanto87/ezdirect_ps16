{if $not_mio_agente == 0}
{* <form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" name="todo" novalidate> *}
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-ticket"></i> {l s='TO DO'}

            <div class="panel-heading-action">
                <div class="bottoni-tab">
                    <a class="btn btn-default" href="{$link_gen_base}tab_name=todo&amp;azione=todo&amp;viewaction&amp;aprinuovaazione{$token_base}">
                        <i class="icon-plus-sign"></i>
                        {l s='Add new to-do'}
                    </a>
                </div>
            </div>
        </div>
        {if count($todo['todo_list']) && $todo['specific'] == 'n'}
        <table class="table tabella_datatable" id="todo_table">
            <thead>
                <tr>
                    <th><span class="title_box ">{l s='ID Action'}</span></th>
                    <th><span class="title_box ">{l s='Status'}</span></th>
                    <th><span class="title_box ">{l s='Edit status'}</span></th>
                    <th><span class="title_box ">{l s='In charge'}</span></th>
                    <th><span class="title_box ">{l s='Type'}</span></th>
                    <th><span class="title_box ">{l s='Q.ta com'}</span></th>
                    <th><span class="title_box ">{l s='Last com.'}</span></th>
                    <th><span class="title_box ">{l s='Opened on'}</span></th>
                    <th><span class="title_box ">{l s='Changed on'}</span></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {foreach $todo['todo_list'] AS $key => $todo_row}
                <tr id="{$todo_row['id']}" class="actions_ticket_tr pointer" rel="{$todo_row['rel']}">
                    <td>{$todo_row['id_azione']}</td>
                    <td data-sort="{$todo_row['status_azione_sort']}">{$todo_row['status_azione']}</td>
                    <td data-sort="{$todo_row['status_azione_sort']}">{$todo_row['status_edit']}</td>
                    <td data-sort="{$todo_row['in_carico']}">{$todo_row['in_carico_edit']}</td> 
                    <td data-sort="{$todo_row['type']}">{$todo_row['tipo']}</td>
                    <td>{$todo_row['qta_com']}</td> 
                    <td>{$todo_row['ultima_com']}</td>
                    <td>{$todo_row['aperto_il']}</td>
                    <td>{$todo_row['modificato_il']}</td>
                    <td>
                        <a class="btn btn-default" style="border-radius:0px;" href="{$todo_row['href']}" title="Vedi">
                            <i class='icon-search'></i> {l s='View'}
                        </a>
                        {if !$check_admin2}
                        <a class="btn btn-danger" style="border-radius:0px;" href="{$todo_row['href_cancella']}" title="Cancella" onclick="return confirm('Sei sicuro/a?')">
                            <i class='icon-trash'></i> {*l s='Delete'*}
                        </a>
                        {/if}
                    </td>
                </tr>
                {/foreach}
            </tbody>
        </table>
        <br />
        Clicca sull'azione per aprirla, leggere i messaggi e rispondere.
        {else}
        <p>{l s='This customer has no todo.'}</p>
        {/if}
        
    </div>
{* </form> *}
{else}
{$is_agente_activated = 1}
{/if}