{assign var="descrizione" value="0"}
{assign var="guasto" value="0" }
{assign var="causa_guasto" value="0"}

<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" name="product" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-user"></i>
            Aggiungi BDL
            
            <div class="bottoni-tab">
                <a class="btn btn-default" href="">
                    {l s='Generate PDF TecnoTel'}
                </a>
                <a class="btn btn-default" href="">
                    {l s='Generate PDF Ezdirect'}
                </a>
                <a class="btn btn-default" href="">
                    {l s='Send BDL by mail'}
                </a>
                <a class="btn btn-default" href="{$link_gen_base}tab_name=bdl&amp;{$token_base}">
                    <i class="icon-circle-arrow-left"></i>
                    Torna a BDL cliente
                </a>
            </div>
        </div>

        <div class="row">
            <div class="form-row">
                <div class="form-group col-md-1">
                    <label for="">{l s='N. BDL'}</label>
                    <input readonly type="text" class="form-control textbox-fix" id="id_bdl" placeholder="" value="Da generare">
                </div>
                <div class="form-group col-md-6">
                    <label for="">{l s='Billing address'}</label>
                    <input readonly type="text" class="form-control textbox-fix" id="" placeholder="" value="{$bdl['buoni']['indirizzo_fatt']['address1']} - {$bdl['buoni']['indirizzo_fatt']['postcode']}  {$bdl['buoni']['indirizzo_fatt']['city']} ({$bdl['buoni']['indirizzo_fatt']['iso_code']})">
                </div>
                <div class="form-group col-md-2">
                    <label for="inputState">{l s='Tel.'}</label>
                    <select id="inputState" class="form-control textbox-fix">
                        <option>-- Seleziona --</option>
                        {foreach $bdl['buoni']['telefoni'] AS $key => $telefono_mist}
                            {foreach $telefono_mist AS $key => $numero}
                                {if $numero != ""}<option value="{$numero}">{$numero}</option>{/if}
                            {/foreach}
                        {/foreach}
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label for="">{l s='Other'}</label>
                    <input type="text" class="form-control textbox-fix" id="" placeholder="">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-row">
                {* Richiesto il giorno *}
                <div class="form-group col-md-3">
                    <label for="">{l s='Asked on'}</label>
                    <input type="datetime-local" class="form-control textbox-fix" id="data_richiesta" placeholder="">
                </div>
                {* Da *}
                <div class="form-group col-md-2">
                    <label for="inputState">{l s='From (name)'}</label>
                    <select id="da_persona" class="form-control textbox-fix">
                        <option>-- Seleziona --</option>
                        {foreach $bdl['buoni']['persone'] AS $key => $persona}
                            <option value="{$persona['id_persona']}">{$persona['firstname']} {$persona['lastname']}</option>
                        {/foreach}
                    </select>
                </div>
                {* Effettuato il giorno *}
                <div class="form-group col-md-3">
                    <label for="">{l s='Carried out on the day'}</label>
                    <input type="datetime-local" class="form-control textbox-fix" id="data_effettuato" placeholder="">
                </div>
                {* Da *}
                <div class="form-group col-md-2">
                    <label for="inputState">{l s='From (name)'}</label>
                    <select id="inputState" class="form-control textbox-fix">
                        <option>-- Seleziona --</option>
                    {foreach $bdl['buoni']['impiegati'] AS $key => $impiegato}
                        <option value="{$impiegato['id']}">{$impiegato['name']}</option>
                    {/foreach}
                    </select>
                </div>
                {* Incluso nell'ordine *}
                {* tooltip "<strong>ATTENZIONE</strong>: questo campo va riempito solo se l'attività prevista dal BDL è già inclusa in un ordine. Se devi <strong>addebitare nuovi costi al cliente</strong>, crea un nuovo BDL." *}
                <div class="form-group col-md-2">
                    <label for="inputState">{l s='Included in the order'}
                        <span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="<strong>ATTENZIONE</strong>: questo campo va riempito solo se l'attività prevista dal BDL è già inclusa in un ordine. Se devi <strong>addebitare nuovi costi al cliente</strong>, crea un nuovo BDL.">
                            <i class="icon-question-sign"></i>
                        </span>
                    </label>
                    <select id="inputState" class="form-control textbox-fix">
                        <option>-- Seleziona --</option>
                        {foreach $bdl['buoni']['ordini'] AS $key => $ordine}
                            <option value="{$ordine['id']}">{$ordine['stringa']}</option>
                        {/foreach}
                    </select>
                    <input type="hidden" id="rif_ordine" value="{$bdl['buoni']['rif_ordine']}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-row">
                {* Impianto-Ubicazione *}
                <div class="form-group col-md-4">
                    <label for="inputState">{l s='Plant-Location'}</label>
                    <select id="inputState" class="form-control textbox-fix">
                        {foreach $bdl['buoni']['indirizzi_bdl'] AS $key => $indirizzo}
                            <option value="{$indirizzo['id_address']}"{if is_numeric($bdl['buoni']['impianto_ubicazione']) AND $bdl['buoni']['impianto_ubicazione'] == $indirizzo['id_address']} selected{/if}>{$indirizzo['address1']} - {$indirizzo['postcode']} {$indirizzo['city']} ({$indirizzo['iso_code']})</option>
                        {/foreach}
                        <option {if ! is_numeric($bdl['buoni']['impianto_ubicazione']) OR empty($bdl['buoni']['impianto_ubicazione'])} selected {/if}>Altro</option>
                    </select>
                </div>
                {* Se altro, specificare *}
                <div class="form-group col-md-8">
                    <label for="">{l s='If other, please specify'}</label>
                    <input type="text" class="form-control textbox-fix" id="" placeholder="" value="{if ! is_numeric($bdl['buoni']['impianto_ubicazione']) OR empty($bdl['buoni']['impianto_ubicazione'])}{$bdl['buoni']['impianto_ubicazione']}{/if}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-row">
                {* Contratto assistenza *}
                <div class="form-group col-md-4">
                    <label for="">{l s='Service contract'}</label>
                    <input type="checkbox" class="form-control" id="contratto_assistenza" placeholder="" onchange="f_contratto_assistenza()" checked>
                </div>
                {* Manutenzione ordinaria (= intervento da contratto di assistenza) *}
                <div class="form-group col-md-4">
                    <label for="">{l s='Ordinary maintenance'} 
                        <span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="That is, assistance contract intervention">
                            <i class="icon-question-sign"></i>
                        </span>
                    </label>
                    <input type="checkbox" class="form-control" id="manutenzione_ordinaria" placeholder="" onclick="f_manutenzione_ordinaria()">
                </div>
                {* Manutenzione straordinaria *}
                <div class="form-group col-md-4">
                    <label for="">{l s='Extraordinary maintenance'}</label>
                    <input type="checkbox" class="form-control" id="manutenzione_straordinaria" placeholder="" onclick="f_manutenzione_straordinaria()" checked>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-row">
                {* Motivo chiamata *}
                <div class="form-group col-md-12">
                    <label for="">{l s='Reason for calling'}</label>
                    <input type="text" class="form-control textbox-fix" id="" placeholder="" value="{$bdl['buoni']['motivo_chiamata']}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-row">
                {* Descrizione *}
                <div class="form-group col-md-2">
                    <label for="inputState">{l s='Description'}</label>
                    <select id="descrizione" class="form-control textbox-fix">
                        <option id="Altro">Altro</option>
                        <option id="Ripristino funzionalita"{if $bdl['buoni']['descrizione'] == "Ripristino funzionalita"}{$descrizione = "1"} selected{/if}>Ripristino funzionalita</option>
                        <option id="Upgrade firmware"{if $bdl['buoni']['descrizione'] == "Upgrade firmware"}{$descrizione = "1"} selected{/if}>Upgrade firmware</option>
                        <option id="Configurazione servizi"{if $bdl['buoni']['descrizione'] == "Configurazione servizi"}{$descrizione = "1"} selected{/if}>Configurazione servizi</option>
                        <option id="Configurazione nuovo hardware"{if $bdl['buoni']['descrizione'] == "Configurazione nuovo hardware"}{$descrizione = "1"} selected{/if}>Configurazione nuovo hardware</option>
                    </select>
                </div>

                {* Se altro, specificare *}
                <div class="form-group col-md-10">
                    <label for="">{l s='If other, please specify'}</label>
                    <input type="text" class="form-control textbox-fix" id="" placeholder="" value="{if $descrizione == "0"}{$bdl['buoni']['descrizione']}{/if}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-row">
                {* Guasto riscontrato dal tecnico *}
                <div class="form-group col-md-2">
                    <label for="inputState">{l s='Fault found by the technician'}</label>
                    <select id="inputState" class="form-control textbox-fix">
                        <option id="Altro">Altro</option>
                        <option id="Nessuna"{if $bdl['buoni']['guasto'] == "Nessuna"}{$guasto = "1"} selected{/if}>Nessuna</option>
                        <option id="Impianto fermo"{if $bdl['buoni']['guasto'] == "Impianto fermo"}{$guasto = "1"} selected{/if}>Impianto fermo</option>
                        <option id="Problemi su linee"{if $bdl['buoni']['guasto'] == "Problemi su linee"}{$guasto = "1"} selected{/if}>Problemi su linee</option>
                        <option id="Problemi di fonia"{if $bdl['buoni']['guasto'] == "Problemi di fonia"}{$guasto = "1"} selected{/if}>Problemi di fonia</option>
                    </select>
                </div>

                {* Se altro, specificare *}
                <div class="form-group col-md-10">
                    <label for="">{l s='If other, please specify'}</label>
                    <input type="text" class="form-control textbox-fix" id="" placeholder="" value="{if $guasto == "0"}{$bdl['buoni']['guasto']}{/if}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-row">
                {* Causa del guasto *}
                <div class="form-group col-md-2">
                    <label for="inputState">{l s='Cause of the failure'}</label>
                    <select id="inputState" class="form-control textbox-fix">
                        <option id="Altro">Altro</option>
                        <option id="Nessuno"{if $bdl['buoni']['causa_guasto'] == "Nessuno"}{$causa_guasto = "1"} selected{/if}>Nessuno</option>
                        <option id="Sovratensione"{if $bdl['buoni']['causa_guasto'] == "Sovratensione"}{$causa_guasto = "1"} selected{/if}>Sovratensione</option>
                        <option id="Dolo/Incuria/Accidentale"{if $bdl['buoni']['causa_guasto'] == "Dolo/Incuria/Accidentale"}{$causa_guasto = "1"} selected{/if}>Dolo/Incuria/Accidentale</option>
                        <option id="Ossido/Umidità"{if $bdl['buoni']['causa_guasto'] == "Ossido/Umidità"}{$causa_guasto = "1"} selected{/if}>Ossido/Umidità</option>
                        <option id="Usura"{if $bdl['buoni']['causa_guasto'] == "Usura"}{$causa_guasto = "1"} selected{/if}>Usura</option>
                        <option id="Non definita"{if $bdl['buoni']['causa_guasto'] == "Non definita"}{$causa_guasto = "1"} selected{/if}>Non definita</option>
                    </select>
                </div>

                {* Se altro, specificare *}
                <div class="form-group col-md-10">
                    <label for="">{l s='If other, please specify'}</label>
                    <input type="text" class="form-control textbox-fix" id="" placeholder="" value="{if $causa_guasto == "0"}{$bdl['buoni']['causa_guasto']}{/if}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-row">
                {* Descrizione intervento svolto *}
                <div class="form-group col-md-10">
                    <label for="">{l s='Description of the intervention carried out'}</label>
                    <textarea class="form-control" id="intervento_svolto" rows="4" value="{$bdl['buoni']['intervento_svolto']}"></textarea>
                </div>
            </div>
        </div>

        <div class="row" id="seleziona_prodotto">
            <div class="form-row">
                <div class="form-group col-md-10">
                    <label for="">{l s='Select the product to add:'}</label>
                </div>
            </div>
        </div>

        <div class="row" id="aggiungi_prodotti">
            {* ROW DI AGGIUNTA PRODOTTI ECC *}

            <div class="form-row">
                <div class="col-md-1">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                    <label class="form-check-label" for="inlineCheckbox1">Online</label>
                </div>
                <div class="col-md-1">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                    <label class="form-check-label" for="inlineCheckbox1">Offline</label>
                </div>
                <div class="col-md-1">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                    <label class="form-check-label" for="inlineCheckbox1">Old</label>
                </div>
                <div class="col-md-1">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                    <label class="form-check-label" for="inlineCheckbox1">{l s='Only av.'}</label> {* Solo disp. *}
                </div>

                <div class="col-md-1">
                    <select id="brand" class="chosen form-control" name="brand">
                        <option value="">{l s='Brand...'}</option>
                        {foreach $bdl['buoni']['marche'] AS $key => $marche}
                            <option value="{$marche['id']}">{$marche['name']}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-md-1">
                    <select id="serie" class="chosen form-control" name="serie">
                        <option value="">{l s='Series...'}</option>
                        <option value="">Scegli prima un costruttore</option>
                    </select>
                </div>
                <div class="col-md-1">
                    <select id="categorie" class="chosen form-control" name="categorie">
                        <option value="">{l s='Category...'}</option>
                        {foreach $bdl['buoni']['categorie'] AS $key => $categorie}
                            <option value="{$categorie['id']}">{$categorie['name']}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-md-1">
                    <select id="fornitori" class="chosen form-control" name="fornitori">
                        <option value="">{l s='Supplier...'}</option>
                        {foreach $bdl['buoni']['fornitori'] AS $key => $fornitori}
                            <option value="{$fornitori['id']}">{$fornitori['name']}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
        </div>

        <hr />

        <div class="row">
        </div>

        <hr />

        <div class="row" id="tabella_costi">
            <div class="form-group col-md-10">
                <table class="table">
                    <thead>
                        <tr>
                            <th><span class="title_box ">{l s='Detail'}</span></th>
                            <th><span class="title_box ">{l s='Code'}</span></th>
                            <th><span class="title_box ">{l s='Description'}</span></th>
                            <th><span class="title_box ">{l s='Qta'}</span></th>
                            <th><span class="title_box ">{l s='Each'}</span></th>
                            <th><span class="title_box ">{l s='Discount %'}</span></th>
                            <th><span class="title_box ">{l s='Amount'}</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Diritto di chiamata</td>
                            <td id="diritto_chiamata_code"></td>
                            <td>
                                <select onchange="aggiorna_costo_unitario('diritto_chiamata')" name="diritto_chiamata" id="diritto_chiamata">
                                    <option value="">-- Seleziona --</option>
                                    <option value="EZDIRFIX">Fisso intervento < 20 km</option>
                                    <option value="EZDIRFIXF">Fisso intervento < 20 km festivo</option>
                                </select>
                            </td>
                            
                            <td><input onblur="aggiorna_costo_totale_riga('diritto_chiamata')" type="text" id="diritto_chiamata_qta" name="diritto_chiamata_qta"></td>
                            <td><input onblur="aggiorna_costo_totale_riga('diritto_chiamata')" type="text" id="diritto_chiamata_unitario" name="diritto_chiamata_unitario"></td>
                            <td><input onblur="aggiorna_costo_totale_riga('diritto_chiamata')" type="text" id="diritto_chiamata_sconto" name="diritto_chiamata_sconto"></td>
                            <td><input type="text" id="diritto_chiamata_importo" name="diritto_chiamata_importo" readonly></td>
                        </tr>

                        <tr>
                            <td>Chilometri percorsi</td>
                            <td id="chilometri_percorsi_code"></td>
                            <td>
                                <select onchange="aggiorna_costo_unitario('chilometri_percorsi')" name="chilometri_percorsi" id="chilometri_percorsi">
                                    <option value="">-- Seleziona --</option>
                                    <option value="EZKMR">Rimborso km oltre diritto fisso</option>
                                    <option value="EZZONAFIX">Chiamata fissa con partner di zona</option>
                                </select>
                            </td>
                            <td><input onblur="aggiorna_costo_totale_riga('chilometri_percorsi')" type="text" id="chilometri_percorsi_qta" name="chilometri_percorsi_qta"></td>
                            <td><input onblur="aggiorna_costo_totale_riga('chilometri_percorsi')" type="text" id="chilometri_percorsi_unitario" name="chilometri_percorsi_unitario"></td>
                            <td><input onblur="aggiorna_costo_totale_riga('chilometri_percorsi')" type="text" id="chilometri_percorsi_sconto" name="chilometri_percorsi_sconto"></td>
                            <td><input type="text" id="chilometri_percorsi_importo" name="chilometri_percorsi_importo" readonly></td>
                        </tr>

                        <tr>
                            <td>Costo manodopera</td>
                            <td id="costo_manodopera_code"></td>
                            <td>
                                <select onchange="aggiorna_costo_unitario('costo_manodopera')" name="costo_manodopera" id="costo_manodopera">
                                    <option value="">-- Seleziona --</option>
                                    <option value="EZTECORD">Tariffa oraria ordinaria</option>
                                    <option value="EZTEC30">Tariffa 30 minuti ordinaria</option>
                                    <option value="EZTECXTRA">Tariffa oraria straordinaria</option>
                                    <option value="EZTECFEST">Tariffa oraria festiva(modificato)</option>
                                </select>
                            </td>
                            <td><input onblur="aggiorna_costo_totale_riga('costo_manodopera')" type="text" id="costo_manodopera_qta" name="costo_manodopera_qta"></td>
                            <td><input onblur="aggiorna_costo_totale_riga('costo_manodopera')" type="text" id="costo_manodopera_unitario" name="costo_manodopera_unitario"></td>
                            <td><input onblur="aggiorna_costo_totale_riga('costo_manodopera')" type="text" id="costo_manodopera_sconto" name="costo_manodopera_sconto"></td>
                            <td><input type="text" id="costo_manodopera_importo" name="costo_manodopera_importo" readonly></td>
                        </tr>

                        {foreach $dettaglio_economico['asd'] AS $key => $call}
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        {/foreach}
                    </tbody>
                    <tfoot>
                        <td>Tot. importo intervento IVA escl. S.E. & O.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="text" id="totale_importo" name="totale_importo" readonly></td>
                    </tfoot>
                </table>
                <input type="hidden" id="num_varie" value="{$bdl['buoni']['num_varie']}">
            </div>
        </div>

        <br />

        <div class="row" id="note_uno">
            <div class="form-row">
                {* Note *}
                <div class="form-group col-md-10">
                    <label for="">{l s='Notes'}</label>
                    <textarea class="form-control" id="descrizione_intervento_svolto" rows="2"></textarea>
                </div>
            </div>
        </div>

        <div class="row" id="nota_privata">
            <a href=""><i class="icon-plus-sign"></i> Clicca per aggiungere una nota privata</a>
        </div>

        <br />

        <div class="row" id="note_due">
            <div class="form-row">
                <div class="form-group col-md-10">
                    <label for="">{l s='Notes (?)'}</label>
                    <textarea class="form-control" id="descrizione_intervento_svolto" rows="2"></textarea>
                </div>
            </div>
        </div>



        <div class="row">
            {* ROW DI AGGIUNTA PRODOTTI ECC *}

            <div class="form-row">

                <div class="col-md-2">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><span class="title_box center_text">{l s='No charge'}</span></th>
                            </tr>
                        </thead>
                        <tbody class="center">
                            <tr>
                                <td><input class="super_center" onchange="verifica_contabilita('nessun_addebito')" type="checkbox" id="nessun_addebito"{if $bdl['buoni']['nessun_addebito'] == "1"} checked{/if}></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="col-md-1">
                </div>

                <div class="col-md-4" id="div_contab1">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><span class="title_box center_text">{l s='Paid'}</span></th>
                                <th><span class="title_box center_text">{l s='Unpaid'}</span></th>
                                <th><span class="title_box center_text">{l s='Billed'}</span></th>
                                <th><span class="title_box center_text">{l s='To be invoiced'}</span></th>
                            </tr>
                        </thead>
                        <tbody class="center">
                            <tr>
                                <td><input class="super_center" onchange="verifica_contabilita('pagato')" type="checkbox" name="contab" id="pagato"></td>
                                <td><input class="super_center" onchange="verifica_contabilita('da_pagare')" type="checkbox" name="contab" id="da_pagare" checked></td>
                                <td><input class="super_center" onchange="verifica_contabilita('fatturato')" type="checkbox" name="contab" id="fatturato"></td>
                                <td><input class="super_center" onchange="verifica_contabilita('da_fatturare')" type="checkbox" name="contab" id="da_fatturare" checked></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="col-md-1">
                </div>

                {*}
                Questo campo va flaggato solo quando si compila un BDL per un'attività non inclusa in un ordine. Il campo serve per inviare il BDL a Ezio in modo tale che Ezio lo possa vistare per l'invio in contabilità. Se il BDL riguarda un'attività già contemplate nell'ordine, non flaggarlo: il sistema opererà in automatico.
                {*}

                <div class="col-md-2" id="div_contab2">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><span class="title_box center_text">{l s='Send for check'} 
                                        <span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="This field should only be flagged when completing a BDL for an activity not included in an order. The field is used to send the BDL to Ezio so that Ezio can endorse it for sending to accounting. If the BDL concerns an activity already covered in the order, do not flag it: the system will operate automatically.">
                                            <i class="icon-question-sign"></i>
                                        </span>
                                    </span>
                                </th>
                            </tr>
                        </thead>
                        <tbody class="center">
                            <tr>
                                <td><input class="super_center" type="checkbox" id="invia_controllo"{if $bdl['buoni']['invio_controllo'] == "1"} checked{/if}></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="col-md-1">
                </div>

            </div>
        </div>

        <br />
        <br />

        <div class="row" id="ultima_row">
            <div class="form-row">
                <div class="form-group col-md-1">
                    <input class="btn btn-primary icon-save" type="submit" value="{l s='Connect to'}">
                </div>
                <div class="form-group col-md-2">
                    <select id="inputState" class="form-control textbox-fix">
                        <option selected>-- Seleziona --</option>
                        <option>No</option>
                        <option>... wip</option>
                    </select>
                </div>
                
            </div>
        </div>

        <input type="checkbox" style="display:none" id="invio_contabilita">

        <hr />
        <input class="btn btn-primary icon-save" type="submit" name="bdl_save" value="{l s='Save'}">
		
    </div>
</form>

{*$bdl['buoni'][0]['id_bdl']*}

{*}{$bdl|debug_print_var}{*}

{* Javascript della pagina *}
{include file="./bdl_edit_js.tpl"}

