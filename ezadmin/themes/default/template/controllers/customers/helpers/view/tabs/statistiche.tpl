{if $is_agente == 0}
				
{* DATI STATISTICI CLIENTE *}
<form class="form-horizontal col-lg-6" action="" method="post" enctype="multipart/form-data" name="dati_stat_cliente" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-envelope"></i> {l s='Customer statistical data'} <span class="badge squadrato">{count($mail['inviate'])}</span>

            <div class="panel-heading-action">
            
            </div>
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th><span class="title_box ">{l s='Data'}</span></th>
                    <th><span class="title_box ">{l s='Value'}</span></th>
                </tr>
            </thead>
            <tbody>
                {if $customer->is_company == 1}
                <tr>
                    <td class="thick">{l s='Number of employees'}</td>
                    <td>{if isset($customer->employees_number) AND $customer->employees_number != NULL}{$customer->employees_number}{else}-{/if}</td>
                </tr>
                {/if}
                <tr>
                    <td class="thick">{l s='Interest'}</td>
                    {foreach $stat['interesse'] as $key => $interesse}
                        <td>{$interesse['category']}</td>
                    {/foreach}								
                </tr>
                <tr>
                    <td class="thick">{l s='First purchase'}</td>
                    {foreach $stat['primo_acquisto'] as $key => $primo}
                        <td>{$primo['product_name']}, {$primo['product_quantity']} pz.</td>
                    {/foreach}
                </tr>
                <tr>
                    <td class="thick">{l s='Last purchase'}</td>
                    {if count($stat['ultimo_acquisto'])}
                    {foreach $stat['ultimo_acquisto'] as $key => $ultimo}
                        <td>{if isset($ultimo['product_name']) AND $ultimo['product_name'] != NULL}{$ultimo['product_name']}{/if}, {if isset($ultimo['product_quantity']) AND $ultimo['product_quantity'] != NULL}{$ultimo['product_quantity']}{/if} pz.</td>
                    {/foreach}
                    {else}
                    <td><td>
                    {/if}
                </tr>
            </tbody>
        </table>
    </div>
</form>

{* PRODOTTI NEL CARRELLO *}
<form class="form-horizontal col-lg-6" action="" method="post" enctype="multipart/form-data" name="prodotti_nel_carrello" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-shopping-cart"></i> {l s='Products in the cart'} <span class="badge squadrato">{count($interested)}</span>
        </div>
        <table class="table tabella_datatable">
            <thead>
                <tr>
                    <th><span class="title_box ">{l s='Added'}</span></th>
                    <th><span class="title_box ">{l s='Product ID'}</span></th>
                    <th><span class="title_box ">{l s='Product name'}</span></th>
                </tr>
            </thead>
            <tbody>
            {foreach $interested as $key => $p}
                <tr>
                    <td>{$p['date_add']}</td>
                    {if $p['id'] != 0}
                    <td>{$p['id']}</td>
                    <td><a href="{$p['url']|escape:'html':'UTF-8'}" target="_blank">{$p['name']}</a></td>
                    {else}
                    <td></td>
                    <td>{$p['name']} - {l s='Product no longer existing'}</td>
                    {/if}
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
</form>

</div>
<div class="row">

{* PRODOTTI ORDINATI *}
<form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" name="prodotti_ordinati" novalidate>
    
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-archive"></i> {l s='Purchased products'} <span class="badge squadrato">{count($products)}</span>
        </div>
        {if $products AND count($products)}
        <table class="table tabella_datatable">
            <thead>
                <tr>
                    <th><span class="title_box">{l s='Date'}</span></th>
                    <th><span class="title_box">{l s='Order N.'}</span></th>
                    <th><span class="title_box">{l s='Name'}</span></th>
                    <th><span class="title_box">{l s='Manufacturer'}</span></th>
                    <th><span class="title_box">{l s='Category'}</span></th>
                    <th><span class="title_box">{l s='Quantity'}</span></th>
                    <th><span class="title_box">{l s='Total (always)'}</span></th>
                </tr>
            </thead>
            <tbody>
                {foreach $stat['prodotti_ordinati'] AS $key => $product}
                <tr class="pointer" onclick="document.location = '?controller=AdminOrders&amp;id_order={$product['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}'">
                    <td>{dateFormat date=$product['date_add'] full=0}</td>
                    <td class="text-right">{$product['id_order']}</td>
                    <td>
                        <a href="?controller=AdminOrders&amp;id_order={$product['id_order']}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}">
                            {$product['product_name']}
                        </a>
                    </td>
                    <td>{$product['manufacturer']}</td>
                    <td>{$product['category']}</td>
                    <td class="text-right">{$product['product_quantity']}</td>
                    <td class="text-right">{$product['total']}</td>
                </tr>
                {/foreach}
            </tbody>
        </table>
        {else}
            {l s='This customer has never purchased on Ezdirect.'}
        {/if}
    </div>
    
</form>

</div>
<div class="row">

{* TOTALE ACQUISTI *}
<form class="form-horizontal col-lg-4" action="" method="post" enctype="multipart/form-data" name="totale_acquisti" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-envelope"></i> {l s='Total purchases'} <span class="badge squadrato">{count($mail['inviate'])}</span>

            <div class="panel-heading-action">
            
            </div>
        </div>
        {if count($stat['acquisti_per_anno'])}
        <table class="table">
            <thead>
                <tr>
                    <th><span class="title_box ">{l s='Year'}</span></th>
                    <th><span class="title_box ">{l s='Total paid'}</span></th>
                    <th><span class="title_box ">{l s='Total VAT excluded'}</span></th>
                </tr>
            </thead>
            <tbody>
                {foreach $stat['acquisti_per_anno'] AS $key => $acq_anno}
                <tr>
                    <td class="thick">{$acq_anno['anno']}</td>
                    <td>{$acq_anno['totale']}</td>
                    <td>{$acq_anno['totale_senza_iva']}</td> 
                </tr>
                {/foreach}
            </tbody>
            <tfoot>
                <tr>
                    <td class="thick">{l s='Total'}</td>
                    <td>{$stat['totale_ok_con_iva']}</td>
                    <td>{$stat['totale_ok_senza_iva']}</td>
                </tr>
            </tfoot>
        </table>
        {else}
        <p>{l s='This customer has never purchased on Ezdirect.'}</p>
        {/if}
    </div>
</form>

{* ULTIME CONNESSIONI *}
<form class="form-horizontal col-lg-8" action="" method="post" enctype="multipart/form-data" name="ultime_connessioni" novalidate>

    {* ROW relativa alle ultime connessioni *}
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-time"></i> {l s='Last connections'}
        </div>
        {if count($connections)}
        <table class="table">
            <thead>
            <tr>
                <th><span class="title_box">{l s='ID'}</span></th>
                <th><span class="title_box">{l s='Date'}</span></th>
                <th><span class="title_box">{l s='Pages viewed'}</span></th>
                <th><span class="title_box">{l s='Total time'}</span></th>
                <th><span class="title_box">{l s='Origin'}</span></th>
                <th><span class="title_box">{l s='IP Address'}</span></th>
            </tr>
            </thead>
            <tbody>
            {foreach $connections as $connection}
                <tr>
                    <td>{$connection['id_connections']}</td>
                    <td>{dateFormat date=$connection['date_add'] full=0}</td>
                    <td>{$connection['pages']}</td>
                    <td>{$connection['time']}</td>
                    <td>{$connection['http_referer']}</td>
                    <td>{$connection['ipaddress']}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
        {else}
        <p>{l s='This customer has never logged in.'}</p>
        {/if}
    </div>
    
</form>
{*}
<form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-share-alt"></i> {l s='Referrers'}
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th><span class="title_box ">{l s='Date'}</span></th>
                    <th><span class="title_box ">{l s='Name'}</span></th>
                    {if $shop_is_feature_active}<th>{l s='Shop'}</th>{/if}
                </tr>
            </thead>
            <tbody>
                {foreach $referrers as $referrer}
                <tr>
                    <td>{dateFormat date=$order['date_add'] full=0}</td>
                    <td>{$referrer['name']}</td>
                    {if $shop_is_feature_active}<td>{$referrer['shop_name']}</td>{/if}
                </tr>
                {/foreach}
            </tbody>
        </table>
    </div>

</form>
{*}
{else}
{$is_agente_activated = 1}
{/if}