{if $not_mio_agente == 0}

{if $smarty.get.error !== null && ($smarty.get.error == '1pv' || $smarty.get.error == '1ms')}
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading rosso">
                    <i class="icon-alert"></i> {l s='ERROR'}
                </div>
                <div class="form-row">
                    <div class="row">
                        <div class="alert alert-danger">Il messaggio era vuoto e non &egrave; stato inviato, ma sono stati aggiornati i campi status e in carico a.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{else if $smarty.get.error !== null && $smarty.get.error == '2ms'} {* Nuovo messaggio vuoto *}
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading rosso">
                    <i class="icon-alert"></i> {l s='ERROR'}
                </div>
                <div class="form-row">
                    <div class="row">
                        <div class="alert alert-danger">Il messaggio era vuoto e non &egrave; stato inviato.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/if}

{*<form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" name="azioni_cliente" novalidate>*}
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-ticket"></i> {l s='Customer actions'}

            <div class="panel-heading-action">
                <div class="bottoni-tab">
                    <a class="btn btn-default" href="{$link_gen_base}tab_name=actions&amp;azione=actions&amp;viewmessage&amp;aprinuovomessaggio{$token_base}">
                        <i class="icon-plus-sign"></i>
                        {l s='Send new message'}
                    </a>
                </div>
            </div>
        </div>
        {if count($azioni_cliente['azioni_list']) && $azioni_cliente['specific'] == 'n'}
        <table class="table tabella_datatable" id="azioni_cliente_table">
            <thead>
                <tr>
                    <th><span class="title_box ">{l s='ID Action'}</span></th>
                    <th><span class="title_box ">{l s='Status'}</span></th>
                    <th><span class="title_box ">{l s='Edit status'}</span></th>
                    <th><span class="title_box ">{l s='In charge'}</span></th>
                    <th><span class="title_box ">{l s='Type'}</span></th>
                    <th><span class="title_box ">{l s='Q.ta com'}</span></th>
                    <th><span class="title_box ">{l s='Last com.'}</span></th>
                    <th><span class="title_box ">{l s='Opened on'}</span></th>
                    <th><span class="title_box ">{l s='Changed on'}</span></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {foreach $azioni_cliente['azioni_list'] AS $key => $azione}
                <tr id="{$azione['id']}" class="actions_ticket_tr pointer" rel="{$azione['rel']}">
                    <td>{$azione['id_azione']}</td>
                    <td data-sort="{$azione['status_azione_sort']}">{$azione['status_azione']}</td> {* Se vuoto (solo per id_contact = 7 - messaggio), il cliente non ha mai risposto al thread *}
                    <td data-sort="{$azione['status_azione_sort']}">{$azione['status_edit']}</td> {* Se vuoto (solo per id_contact = 7 - messaggio), il cliente non ha mai risposto al thread *}
                    <td data-sort="{$azione['in_carico']}">{$azione['in_carico_edit']}</td> 
                    <td data-sort="{$azione['type']}">{$azione['tipo']}</td>
                    <td>{$azione['qta_com']}</td> 
                    <td>{$azione['ultima_com']}</td>
                    <td>{$azione['aperto_il']}</td>
                    <td>{$azione['modificato_il']}</td>
                    <td>
                        <a class="btn btn-default" style="border-radius:0px;" href="{$azione['href']}" title="Vedi">
                            <i class='icon-search'></i> {l s='View'}
                        </a>
                        {if !$check_admin2}
                        <a class="btn btn-danger" style="border-radius:0px;" href="{$azione['href_cancella']}" title="Cancella" onclick="return confirm('Sei sicuro/a?')">
                            <i class='icon-trash'></i> {*l s='Delete'*}
                        </a>
                        {/if}
                    </td>
                </tr>
                {/foreach}
            </tbody>
        </table>
        <br />
        Clicca sull'azione per aprirla, leggere i messaggi e rispondere.
        {else}
        <p>{l s='This customer has no action.'}</p>
        {/if}
        
    </div>
{*</form>*}
{else}
{$is_agente_activated = 1}
{/if}