{if $is_agente == 0}
<form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" name="note_credito" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-file"></i> {l s='Credit Notes'}
            {if $count_note_di_credito != 0}
            <span class="badge">
                {$count_note_di_credito}
            </span>
            {/if}
        </div>
        {if $count_note_di_credito > 0}
            <table class="table" id="notecredito_table"> {* Non uso datatables per caricare correttamente le righe nascoste *}
                <thead>
                    <tr>
                        <th><span class="title_box">{l s='Credit Note ID'}</span></th>
                        <th><span class="title_box">{l s='Historical ID'}</span></th>
                        <th><span class="title_box">{l s='Site order ref.'}</span></th>
                        <th><span class="title_box">{l s='eSolver order ref.'}</span></th>
                        <th><span class="title_box">{l s='Credit note date'}</span></th>
                        <th><span class="title_box">{l s='Payment'}</span></th>
                        <th><span class="title_box">{l s='Taxable'}</span></th>
                        <th><span class="title_box">{l s='Total'}</span></th>
                        <th><span class="title_box">{l s='Send credit note via e-mail'}</span></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                {foreach $note_di_credito['note_di_credito_rows'] AS $key => $nota}
                    <tr>
                        <td><img class="pointer" src="../img/admin/add.gif" alt="Espandi" title="Espandi" data-toggle="collapse" data-target="#dett_nota_{$nota['id_fattura']|intval}" aria-expanded="false" aria-controls="collapse_ra"/> {$nota['id_fattura']}</td>
                        <td>{$nota['id_storico']}</td>
                        <td>{$nota['rif_vs_ordine']}</td>
                        <td>{$nota['rif_ordine']}</td>
                        <td>{$nota['data_fattura']}</td>
                        <td>{$nota['pagamento']}</td>
                        <td>{$nota['imponibile']}</td>
                        <td>{$nota['totale_fattura']}</td>
                        <td>
                            <a class="btn btn-default squadrato" id="invia_fattura_anagrafica_{$nota['id_storico']}" title="Invia e-mail" href="javascript:void(0)" {*href="mailto:{$customer->email}"*}>
                                <img src='../img/admin/outlook.gif' alt='Invia e-mail' title='Invia e-mail' style='cursor:pointer' /> {l s='Send'}
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-default" href="{$link_gen_base}tab_name=credit_notes&amp;azione=nota_di_credito_view&amp;id_fattura={$nota['id_fattura']}&amp;tipo={$nota['tipo']}&amp;tipo_fattura=nota{$token_base}">
                                <i class='icon-search'></i> {l s='View'}
                            </a>
                        </td>
                    </tr>

                    {* Collapsable *}
                    <tr>
                        <td colspan="10">
                            <div class="collapse" id="dett_nota_{$nota['id_fattura']|intval}">
                                {if count($nota['prodotti'])}
                                <div class="row" style="background-color:lightgrey;">
                                    <div class="col-md-2">
                                        <strong>Codice</strong>
                                    </div>
                                    <div class="col-md-3">
                                        <strong>Nome prodotto</strong>
                                    </div>
                                    <div class="col-md-1">
                                        <strong>Qta</strong>
                                    </div>
                                    <div class="col-md-2">
                                        <strong>Unitario</strong>
                                    </div>
                                    <div class="col-md-2">
                                        <strong>Totale</strong>
                                    </div>
                                </div>
                                {foreach $nota['prodotti'] AS $prodotto}
                                    <div class="row">
                                        <div class="col-md-2">
                                            {if $prodotto['id']}<a class="tooltip_prodotto" {* data-tooltip-content="#tooltip_content" *} title="{$prodotto['tooltip']}" href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}&amp;id_product={$prodotto['id']|intval}&amp;updateproduct" target="_blank">{$prodotto['cod_articolo']}</a>{else}{$prodotto['cod_articolo']}{/if}
                                        </div>
                                        <div class="col-md-3" style="overflow:hidden; white-space:nowrap; text-overflow:ellipsis;">
                                            <span title="{$prodotto['desc_articolo']}">{$prodotto['desc_articolo']}</span>
                                        </div>
                                        <div class="col-md-1">
                                            {$prodotto['qt_sped']}
                                        </div>
                                        <div class="col-md-2">
                                            {$prodotto['prezzo']}
                                        </div>
                                        <div class="col-md-2">
                                            {$prodotto['importo_netto']}
                                        </div>
                                    </div>
                                {/foreach}
                                {/if}
                            </div>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        {else}
        <p class="text-muted text-center">
            {l s="No credit notes found"}
        </p>
        {/if}
    </div>
</form>
{else}
{$is_agente_activated = 1}
{/if}

