<nav class="navbar navbar-default">
    <div class="container-fluid">
        {* Questo è necessario per far funzionare la modalità mobile *}
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li {if $tab_name == "tickets"}class="active"{/if}><a href="{$link_gen_base}tab_name=tickets{$token_base}">Tickets ({if $count_ticket_aperti > 0} <span class="rosso">{$count_ticket_aperti}</span>{else}{$count_ticket_aperti}{/if} / {$count_ticket})</a></li>
                <li {if $tab_name == "actions"}class="active"{/if}><a href="{$link_gen_base}tab_name=actions{$token_base}">Azioni cliente ({if $count_azioni_cliente_aperte > 0} <span class="rosso">{$count_azioni_cliente_aperte}</span>{else}{$count_azioni_cliente_aperte}{/if} / {$count_azioni_cliente})</a></li>
                <li {if $tab_name == "todo"}class="active"{/if}><a href="{$link_gen_base}tab_name=todo{$token_base}">TO DO ({if $count_todo_aperti > 0} <span class="rosso">{$count_todo_aperti}</span>{else}{$count_todo_aperti}{/if} / {$count_todo})</a></li>
                {if $is_agente == 0}<li {if $tab_name == "stats"}class="active"{/if}><a href="{$link_gen_base}tab_name=stats{$token_base}">Stat</a></li>{/if}
                {if $is_agente == 0}<li {if $tab_name == "contracts"}class="active"{/if}><a href="{$link_gen_base}tab_name=contracts{$token_base}">Contratti ({if $count_contratti_attivi > 0} <span class="rosso">{$count_contratti_attivi}</span>{else}{$count_contratti_attivi}{/if} / {$count_contratti})</a></li>{/if}
                {if $is_agente == 0}<li {if $tab_name == "bdl"}class="active"{/if}><a href="{$link_gen_base}tab_name=bdl{$token_base}">BDL ({$count_bdl_aperti} / {$count_bdl_totali})</a></li>{/if}
                {if $is_agente == 0}<li {if $tab_name == "mail"}class="active"{/if}><a href="{$link_gen_base}tab_name=mail{$token_base}">Mail</a></li>{/if}
                {if $is_agente == 0}<li {if $tab_name == "calls"}class="active"{/if}><a href="{$link_gen_base}tab_name=calls{$token_base}">Telefonate ({$count_telefonate_ricevute}-{$count_telefonate_inviate})</a></li>{/if}
                <li {if ! isset($tab_name) OR $tab_name == "anagrafica" OR $tab_name == "customerdetails"}class="active"{/if}><a href="{$link_gen_home}{$token_base}">Anagrafica</a></li>
                <li {if $tab_name == "addresses"}class="active"{/if}><a href="{$link_gen_base}tab_name=addresses{$token_base}">Indirizzi</a></li>
                {if $customer->id_default_group != 2}<li {if $tab_name == "people"}class="active"{/if}><a href="{$link_gen_base}tab_name=people{$token_base}">Persone ({$count_persone})</a></li>{/if}
                <li {if $tab_name == "administration"}class="active"{/if}><a href="{$link_gen_base}tab_name=administration{$token_base}">Amministrazione</a></li>
                <li {if $tab_name == "orders"}class="active"{/if}><a href="{$link_gen_base}tab_name=orders{$token_base}">Ordini ({$count_ordini})</a></li>
                <li {if $tab_name == "carts"}class="active"{/if}><a href="{$link_gen_base}tab_name=carts{$token_base}">Carrelli ({$count_carrelli})</a></li>
                {if $is_agente == 0}<li {if $tab_name == "invoices"}class="active"{/if}><a href="{$link_gen_base}tab_name=invoices{$token_base}">Fatture ({$count_fatture})</a></li>{/if}
                {if $is_agente == 0}<li {if $tab_name == "credit_notes"}class="active"{/if}><a href="{$link_gen_base}tab_name=credit_notes{$token_base}">Note di credito ({$count_note_di_credito})</a></li>{/if}
                {if $is_agente == 0}<li {if $tab_name == "ddt"}class="active"{/if}><a href="{$link_gen_base}tab_name=ddt{$token_base}">DDT ({$count_ddt})</a></li>{/if}
                {if $is_agente == 0}<li {if $tab_name == "cloud"}class="active"{/if}><a href="{$link_gen_base}tab_name=cloud{$token_base}">Cloud</a></li>{/if}
                {if $is_agente == 0}<li {if $tab_name == "documents"}class="active"{/if}><a href="{$link_gen_base}tab_name=documents{$token_base}">Documenti ({$count_documenti_dir}-{$count_documenti_files})</a></li>{/if}
                {* Pagamenti ( pagamenti pendenti - totale pagamenti ) ? diventa rosso nel caso in cui ci sono pagamenti ancora da effettuare *}
                {*}{if $is_agente == 0}<li {if $tab_name == "pagamenti"}class="active"{/if}><a href="{$link_gen_base}tab_name=pagamenti{$token_base}">Pagamenti ({if $count_pagamenti_mancanti > 0} <span class="rosso">{$count_pagamenti_mancanti}</span>{else}{$count_pagamenti_mancanti}{/if}-{$count_pagamenti_totali})</a></li>{/if}{*}
            </ul>
        </div>
        
    </div>
</nav>