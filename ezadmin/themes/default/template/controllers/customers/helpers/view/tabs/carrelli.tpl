<form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" name="product" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-shopping-cart"></i> {l s='Carts'} <span class="badge" style="border-radius:0px;">{count($carts)}</span>
        </div>
        <div class="panel-heading-action">
            <div class="bottoni-tab">
                <a class="btn btn-default" href="{$link->getAdminLink('AdminCarts')}&viewcart&createnew&provvisorio=1&preventivo=1&id_customer={$customer->id}#modifica-carrello">
                    <i class="icon-plus-sign"></i>
                    Nuovo preventivo
                </a>
            </div>
        </div>
        {if $carts AND count($carts)} {* CORREGGERE: manca il dettaglio di ogni riga (la tabella accessibile tramite il tasto +) *}
            <table class="table tabella_datatable" id="carrelli_table">
                <thead>
                    <tr>
                        <th><span class="title_box ">{l s='ID'}</span></th>
                        <th><span class="title_box ">{l s='Date'}</span></th>
                        <th><span class="title_box ">{l s='Quote'}</span></th>
                        <th><span class="title_box ">{l s='Read'}</span></th>
                        <th><span class="title_box ">{l s='Conv.'}</span></th>
                        <th><span class="title_box ">{l s='ID Order'}</span></th>
                        <th><span class="title_box ">{l s='Total'}</span></th>
                        <th><span class="title_box ">{l s='Subject'}</span></th>
                        <th><span class="title_box ">{l s='Created by'}</span></th>
                        <th><span class="title_box ">{l s='In charge'}</span></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                {foreach $carts AS $key => $cart}
                    <tr class="pointer" onclick="document.location = '{$link->getAdminLink('AdminCarts')|escape:'html':'UTF-8'}&amp;id_cart={$cart['id_cart']}&amp;viewcart'">
                        <td style="{$cart['td_style']}" data-sort="{$cart['id_carrello']}">{$cart['id_cart_complete']}</td>
                        <td style="{$cart['td_style']}">{dateFormat date=$cart['date_upd'] full=0}</td>
                        <td style="{$cart['td_style']}">
                            {if ! isset($cart['quote'])}
                                <i class="icon-question"></i>
                            {elseif $cart['quote'] == 0}
                                {* Lo span serve per il corretto ordinamento della colonna con datatable *}
                                <i class="icon-shopping-cart" title="Carrello"><span style="display:none">Carrello</span></i>
                            {elseif $cart['quote'] == 1}
                                <i class="icon-calculator" style="color:green" title="Preventivo"><span style="display:none">Preventivo</span></i>
                            {/if}
                        </td>
                        <td style="{$cart['td_style']}">
                            {if ! isset($cart['read'])}
                                <i class="icon-question"></i>
                            {elseif $cart['read']}
                                <i class="icon-check" style="color:green"></i>
                            {/if}
                        </td>
                        <td style="{$cart['td_style']}">
                            {if ! isset($cart['conv'])}
                                <i class="icon-question"></i>
                            {elseif $cart['conv'] == 1}
                                <i class="icon-check" style="color:green"></i>
                            {/if}
                        </td>
                        <td style="{$cart['td_style']}">{if isset($cart['ordine'])} {$cart['ordine']} {else} - {/if}</td>
                        <td style="{$cart['td_style']}">{if isset($cart['totale'])} {$cart['totale']} {else} - {/if}</td>
                        <td style="{$cart['td_style']}">{if isset($cart['name'])} {$cart['name']} {else} - {/if}</td>
                        <td style="{$cart['td_style']}">{if isset($cart['creato_da'])} {$cart['creato_da']} {else} - {/if}</td>
                        <td style="{$cart['td_style']}">{if isset($cart['in_carico'])} {$cart['in_carico']} {else} - {/if}</td>
                        <td style="text-align: right; {$cart['td_style']}" id="azioni_carrello">
                            {if $cart['converti_cart']}
                                <a class="btn btn-default" style="border-radius:0px;" href="{$link->getAdminLink('AdminCarts')}&amp;id_customer={$customer->id}&amp;id_cart={$cart['id_carrello']}&amp;convertcart" onclick="return confirm('Sei sicuro?')">
                                    Converti
                                </a>
                            {/if}
                            {if $cancella_cart}
                            <a class="btn btn-danger" style="border-radius:0px;" href="{$link->getAdminLink('AdminCarts')}&amp;id_customer={$customer->id}&amp;id_cart={$cart['id_carrello']}&amp;deleteccart" onclick="return confirm('Sei sicuro?')">
                                <i class='icon-trash'></i> {*l s='Delete'*}
                            </a>
                            {/if}
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        {else}
        <p class="text-muted text-center">
            {l s='No cart is available'}
        </p>
        {/if}
    </div>
    {* if $products AND count($products)}
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-archive"></i> {l s='Purchased products'} <span class="badge">{count($products)}</span>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th><span class="title_box">{l s='Date'}</span></th>
                    <th><span class="title_box">{l s='Name'}</span></th>
                    <th><span class="title_box">{l s='Quantity'}</span></th>
                </tr>
            </thead>
            <tbody>
                {foreach $products AS $key => $product}
                <tr onclick="document.location = '?tab=AdminOrders&amp;id_order={$product['id_order']|intval}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}'">
                    <td>{dateFormat date=$product['date_add'] full=0}</td>
                    <td>
                        <a href="?tab=AdminOrders&amp;id_order={$product['id_order']}&amp;vieworder&amp;token={getAdminToken tab='AdminOrders'}">
                            {$product['product_name']}
                        </a>
                    </td>
                    <td>{$product['product_quantity']}</td>
                </tr>
                {/foreach}
            </tbody>
        </table>
    </div>
    {/if *}
</form>