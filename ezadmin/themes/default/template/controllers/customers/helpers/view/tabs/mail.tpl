{if $is_agente == 0}
<form class="form-horizontal col-lg-6" action="" method="post" enctype="multipart/form-data" name="mail_ricevute" novalidate>
<div class="panel">
    <div class="panel-heading">
        <i class="icon-mail-reply"></i> {l s='Received'} <span class="badge squadrato">{count($mail['ricevute'])}</span>

        <div class="panel-heading-action">
        
        </div>
    </div>

    {if count($mail['ricevute'])}
    <table class="table tabella_datatable" id="mail_ricevute">
        <thead>
            <tr>
                <th><span class="title_box ">{l s='From'}</span></th>
                <th><span class="title_box ">{l s='To'}</span></th>
                <th><span class="title_box ">{l s='Subject'}</span></th>
                <th><span class="title_box ">{l s='Date'}</span></th>
                <th><span class="title_box ">{l s='Attachments'}</span></th>
                <th><span class="title_box ">{l s='View'}</span></th>
            </tr>
        </thead>
        <tbody>
            {foreach $mail['ricevute'] AS $key => $email}
            <tr class="pointer" onclick="openMail({$email['id']})">
                <td>{$email['da_intestazione']}</td>
                <td>{$email['a_intestazione']}</td>
                <td>{$email['oggetto']}</td>
                <td>{$email['data']}</td>
                <td>{if $email['numero_allegati'] > 0}{$email['numero_allegati']|intval}{/if}</td>
                
                <td class="text-right">
                    <div class="btn-group">
                        <a class="btn btn-default squadrato">
                            <i class="icon-search" onclick="openMail({$email['id']})"></i>
                        </a>
                    </div>
                </td>
            </tr>
            {/foreach}

            {*<td>TESTgino@gmail.com</td>
            <td>info@asd.it</td>
            <td>Guasto pc</td>
            <td>12:34 01 Gennaio 2020</td>
            <td>NO</td>
            
            <td class="text-right">
                <div class="btn-group">
                    <a class="btn btn-default squadrato" href="">
                        <i class="icon-search"></i>
                    </a>
                </div>
            </td>*}
        </tbody>
    </table>
    {else}
    <p class="text-muted text-center">
        {l s='No mail was found'}
    </p>
    {/if}
</div>
</form>

<form class="form-horizontal col-lg-6" action="" method="post" enctype="multipart/form-data" name="mail_inviate" novalidate>
<div class="panel">
    <div class="panel-heading">
        <i class="icon-mail-forward"></i> {l s='Sent'} <span class="badge squadrato">{count($mail['inviate'])}</span>

        <div class="panel-heading-action">
        
        </div>
    </div>

    {if count($mail['inviate'])}
    <table class="table tabella_datatable" id="mail_inviate">
        <thead>
            <tr>
                <th><span class="title_box ">{l s='From'}</span></th>
                <th><span class="title_box ">{l s='To'}</span></th>
                <th><span class="title_box ">{l s='Subject'}</span></th>
                <th><span class="title_box ">{l s='Date'}</span></th>
                <th><span class="title_box ">{l s='Attachments'}</span></th>
                <th><span class="title_box ">{l s='View'}</span></th>
            </tr>
        </thead>
        <tbody>
            {foreach $mail['inviate'] AS $key => $email}
            <tr class="pointer" onclick="openMail({$email['id']})"> {* Apre il contenuto della mail sotto alla table *}
                <td>{$email['da_intestazione']}</td>
                <td>{$email['a_intestazione']}</td>
                <td>{$email['oggetto']}</td>
                <td>{$email['data']}</td>
                <td>{if $email['numero_allegati'] > 0}{$email['numero_allegati']|intval}{/if}</td>
                
                <td class="text-right">
                    <div class="btn-group">
                        <a class="btn btn-default squadrato">
                            <i class="icon-search" onclick="openMail({$email['id']})"></i>
                        </a>
                    </div>
                </td>
            </tr>
            {/foreach}

            {*<td>TESTgino@gmail.com</td>
            <td>info@asd.it</td>
            <td>Guasto pc</td>
            <td>12:34 01 Gennaio 2020</td>
            <td>NO</td>
            
            <td class="text-right">
                <div class="btn-group">
                    <a class="btn btn-default squadrato" href="">
                        <i class="icon-search"></i>
                    </a>
                </div>
            </td>*}
        </tbody>
    </table>
    {else}
    <p class="text-muted text-center">
        {l s='No mail was found'}
    </p>
    {/if}
</div>
</form>

{* Da testare! Copiato dalla 1.4, qui viene inserito il contenuto della mail aperta; cos'è scoper.min.js? *}
<section>
    <div id="mail_info"></div>
</section>
{* <script type="text/javascript" src="../js/jquery/scoper.min.js"></script> *}

{* Javascript della pagina *}
{include file="./js/mail_js.tpl"}

{else}
{$is_agente_activated = 1}
{/if}