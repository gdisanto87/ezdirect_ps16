{if $is_agente == 0}
<form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" name="ddt" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-file"></i> {l s='DDT'}
            {if $count_ddt != 0}
            <span class="badge">
                {$count_ddt}
            </span>
            {/if}
        </div>
        {if $count_ddt > 0}
            <table class="table tabella_datatable" id="ddt_table">
                <thead>
                    <tr>
                        <th><span class="title_box">{l s='TEST DDT'}</span></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        {else}
        <p class="text-muted text-center">
            {l s="No ddt found"}
        </p>
        {/if}
    </div>
</form>
{else}
{$is_agente_activated = 1}
{/if}

