<form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" name="messaggi" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-envelope"></i> {l s='Messages'} <span class="badge">{count($messages)}</span>
        </div>
        {if count($messages)}
            <table class="table">
                <thead>
                    <th><span class="title_box">{l s='Status'}</span></th>
                    <th><span class="title_box">{l s='Message'}</span></th>
                    <th><span class="title_box">{l s='Sent on'}</span></th>
                </thead>
                {foreach $messages AS $message}
                    <tr>
                        <td>{$message['status']}</td>
                        <td>
                            <a href="index.php?tab=AdminCustomerThreads&amp;id_customer_thread={$message.id_customer_thread}&amp;viewcustomer_thread&amp;token={getAdminToken tab='AdminCustomerThreads'}">
                                {$message['message']}...
                            </a>
                        </td>
                        <td>{$message['date_add']}</td>
                    </tr>
                {/foreach}
            </table>
        {else}
        <p class="text-muted text-center">
            {l s='%1$s %2$s has never contacted you' sprintf=[$customer->firstname, $customer->lastname]}
        </p>
        {/if}
    </div>
</form>