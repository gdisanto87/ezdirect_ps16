{if $is_agente == 0}
<form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-comments"></i> {l s='Assistance contracts'}

            <div class="panel-heading-action">
                <a class="btn btn-default" href="{$link_gen_base}tab_name=contracts&amp;azione=contratto_add{$token_base}">
                    <i class="icon-plus-sign"></i>
                    {l s='Generate new contract'}
                </a>
            </div>
        </div>
        {if $count_contratti > 0}
        <table class="tabella_datatable">
            <thead>
                <tr>
                    <th><span class="title_box ">{l s='Contract Number'}</span></th> {* N. contratto *}
                    <th><span class="title_box ">{l s='Type'}</span></th> {* Tipo *}
                    <th><span class="title_box ">{l s='Status'}</span></th> {* Stato *}
                    <th><span class="title_box ">{l s='Starts on'}</span></th> {* Inizia il *}
                    <th><span class="title_box ">{l s='Expires'}</span></th> {* Scade il *}
                    <th><span class="title_box ">{l s='Amount'}</span></th> {* Importo *}
                    <th><span class="title_box ">{l s='Headquarters'}</span></th> {* Sede *}
                    <th></th> {* Azioni *}
                </tr>
            </thead>
            <tbody>
                {foreach $contratti['contratti_list'] AS $key => $contratto}
                <tr class="pointer" {if $contratto['status'] == 0}style="background: rgba(255, 192, 203, 0.5)"{/if}>
                    <td>{$contratto['id_contratto']}</td>
                    <td>{$contratto['tipo_contratto']}</td>
                    <td>{$contratto['status_contratto']}</td> 
                    <td>{$contratto['inizia_il']}</td>
                    <td>{$contratto['scade_il']}</td> 
                    <td>{$contratto['importo']}</td>
                    <td>{$contratto['sede']}</td>
                    <td>
                        <a class="btn btn-default" style="border-radius:0px;" href="{$link_gen_base}tab_name=contracts&amp;azione=contratto_view&amp;id_contratto={$contratto['id_contratto']}{$token_base}" title="Vedi">
                            <i class='icon-search'></i> {l s='View'}
                        </a>
                        <a class="btn btn-default" style="border-radius:0px;" href="{$link_gen_base}tab_name=contracts&amp;azione=contratto_edit&amp;id_contratto={$contratto['id_contratto']}{$token_base}" title="Modifica">
                            <i class='icon-pencil'></i> {*l s='Edit'*}
                        </a>
                        <a class="btn btn-danger" style="border-radius:0px;" href="{$link_gen_base}tab_name=contracts&amp;azione=contratto_delete&amp;id_contratto={$contratto['id_contratto']}{$token_base}" title="Cancella" onclick="javascript: var surec=window.confirm('Sei sicuro/a?'); if (surec) { return true; } else { return false; }">
                            <i class='icon-trash'></i> {*l s='Delete'*}
                        </a>
                    </td>
                </tr>
                {/foreach}
            </tbody>
        </table>
        {else}
        <p>{l s='This customer has no service contracts.'}</p>
        {/if}
        
    </div>
</form>
{else}
{$is_agente_activated = 1}
{/if}