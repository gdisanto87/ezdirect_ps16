<form class="form-horizontal col-lg-12" action="" method="post" enctype="multipart/form-data" name="indirizzi" novalidate>
    {* display hook specified to this page : AdminCustomers *}
    {hook h="displayAdminCustomers" id_customer=$customer->id|intval}

    <div class="panel">
        <div class="panel-heading">
            <i class="icon-map-marker"></i> {l s='Addresses'} <span class="badge">{count($addresses)}</span>

            <div class="panel-heading-action">
                <div class="bottoni-tab">
                    <a class="btn btn-default" href="{$link->getAdminLink('AdminAddresses')|escape:'html':'UTF-8'}&amp;addaddress&amp;id_customer={$customer->id|intval}&amp;tipo=0&amp;back={$smarty.server.REQUEST_URI|urlencode}">
                        <i class="icon-plus-sign"></i>
                        {l s='Add delivery address'} {* Aggiungi indirizzo di consegna *}
                    </a>
                    {if $indirizzi['aggiungi_fatt']}
                        <a class="btn btn-default" href="{$link->getAdminLink('AdminAddresses')|escape:'html':'UTF-8'}&amp;addaddress&amp;id_customer={$customer->id|intval}&amp;tipo=1&amp;back={$smarty.server.REQUEST_URI|urlencode}">
                            <i class="icon-plus-sign"></i>
                            {l s='Add invoice address'} {* Aggiungi indirizzo di fatturazione *}
                        </a>
                    {/if}
                </div>
            </div>
        </div>
        
        {if count($addresses)}
            {if !count($addresses_fatt)}
                <p class="alert alert-warning">
                    {l s='The customer has not registered any invoice addresses yet'} {* Il cliente non ha ancora aggiunto un indirizzo di fatturazione *}
                </p>
            {/if}
            {if !count($addresses_cons)}
                <p class="alert alert-warning">
                    {l s='The customer has not registered any delivery addresses yet'} {* Il cliente non ha ancora aggiunto un indirizzo di consegna *}
                </p>
            {/if}
            <table class="table">
                <thead>
                    <tr>
                        <th><span class="title_box ">{l s='Type'}</span></th>
                        <th><span class="title_box ">{l s='Company'}</span></th>
                        <th><span class="title_box ">{l s='Name'}</span></th>
                        <th><span class="title_box ">{l s='Address'}</span></th>
                        <th><span class="title_box ">{l s='Zip/Postal Code'}</span></th>
                        <th><span class="title_box ">{l s='City'}</span></th>
                        <th><span class="title_box ">{l s='State'}</span></th>
                        <th><span class="title_box ">{l s='Country'}</span></th>
                        <th><span class="title_box ">{l s='Phone number(s)'}</span></th>
                        <th><span class="title_box ">{l s='Actions'}</span></th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $addresses AS $key => $address}
                    <tr>
                        <td {if $address['fatturazione']}style="color:#268ccd;"{/if}>{if $address['fatturazione']}{l s='Fatturazione'}{else}{l s='Consegna'}{/if}</td>
                        <td>{if $address['company']}{$address['company']}{else}--{/if}</td>
                        <td>{$address['firstname']} {$address['lastname']}</td>
                        <td>{$address['address1']} {if $address['address2']}{$address['address2']}{/if}</td>
                        <td>{$address['postcode']}</td>
                        <td>{$address['city']}</td>
                        <td>{$address['state']}</td>
                        <td>{$address['country']}</td>
                        <td>
                            {if $address['phone']}
                                {$address['phone']}
                                {if $address['phone_mobile']}<br />{$address['phone_mobile']}{/if}
                            {else}
                                {if $address['phone_mobile']}<br />{$address['phone_mobile']}{else}--{/if}
                            {/if}
                        </td>
                        <td class="text-right">
                            <div class="btn-group">
                                <a class="btn btn-default" style="border-radius:0px;" href="{$link->getAdminLink('AdminAddresses')|escape:'html':'UTF-8'}&amp;id_address={$address['id_address']}&amp;updateaddress&amp;back={$smarty.server.REQUEST_URI|urlencode}">
                                    <i class="icon-edit"></i> {* l s='Edit' *}
                                </a>
                                {* <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> *}
                                {*	<span class="caret"></span> *}
                                {* </button> *}
                                {* <ul class="dropdown-menu"> *}
                                    {* <li> *}
                                {if $indirizzi['cancella_ind']}
                                    <a class="btn btn-danger" style="border-radius:0px;" href="{$link->getAdminLink('AdminAddresses')|escape:'html':'UTF-8'}&amp;id_address={$address['id_address']}&amp;deleteaddress&amp;back={$smarty.server.REQUEST_URI|urlencode}" onclick="return confirm('Sei sicuro?')">
                                        <i class="icon-trash"></i>
                                        {* l s='Delete' *}
                                    </a>
                                {/if}
                                    {* </li> *}
                                {* </ul> *}
                            </div>
                        </td>
                    </tr>
                    {/foreach}
                </tbody>
            </table>
        {else}
            <p class="text-muted text-center">
                {*l s='%1$s %2$s has not registered any addresses yet' sprintf=[$customer->firstname, $customer->lastname]*}
                {l s='The customer has not registered any addresses yet'}
            </p>
        {/if}
    </div>
</form>