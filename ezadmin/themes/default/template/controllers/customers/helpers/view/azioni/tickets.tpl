{* CORREGGERE: aggiungere avviso modifiche all'uscita della pagina *}

{if $smarty.get.aprinuovoticket === null}
    {assign var=old value=$tickets['ticket_view']['generale']['ticket_old']}
    {assign var=old_nota value=$tickets['ticket_view']['generale']['ticket_nota']}

    {if $tickets['ticket_view']['generale']['azione']['id_contact'] == 9} {* RMA *}
        {assign var=dati_rma value=$tickets['ticket_view']['generale']['dati_rma']}
    {/if}
{/if}

{assign var=azione_t value=$tickets['ticket_view']['cronologia']['azione']}
{assign var=messaggia value=$tickets['ticket_view']['cronologia']['messaggit']}
{assign var=storico_ticket value=$tickets['ticket_view']['storico']['storico_ticket']}
{assign var=tree_ul value=$tickets['ticket_view']['gerarchia']['tree_ul']}
{assign var=bdl value=$tickets['ticket_view']['bdl']}

{* Stile della pagina *}
{include file="./tickets_stile.tpl"}

{if $tickets['ticket_view']['generale']['errore']}

    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="form-row">
                    <div class="row">
                        <div class="alert alert-danger">ERRORE - Ticket non trovato</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

{else}

{if $smarty.get.id_customer_thread !== null && $smarty.get.aprinuovoticket === null}

    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="form-row">
                    <div class="row">
                        <h2 class="blu no-margin" style="text-align:center">
                            {if $tickets['ticket_view']['generale']['azione']['id_contact'] == 9}RMA{else}TICKET{/if} {$tickets['ticket_view']['generale']['idunivoco']} <span style="color:black"> - In carico a {$tickets['ticket_view']['generale']['in_carico_a_firstname']} - Status: {$tickets['ticket_view']['generale']['status_azione']} {$tickets['ticket_view']['generale']['st_azione']}</span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form id="modificaticket" action="{$link_gen_base}tab_name=tickets&amp;submitTicket{if isset($smarty.get.riferimento)}&amp;riferimentoo={$smarty.get.riferimento}{/if}{$token_base}" method="post" autocomplete="off">
        <div class="form-horizontal col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <i class="icon-user"></i>
                    Generale
                </div>

                <div class="form-row">
                    <div class="row">

                        <input type="hidden" name="id_ticket_univoco" value="{$tickets['ticket_view']['generale']['idunivoco']}" />
                        <input type="hidden" name="id_customer" value="{$customer->id}" />
						<input type="hidden" name="id_customer_thread" value="{$smarty.get.id_customer_thread}" />
                        
                        <div class="form-group col-md-12">
                            <label for="ticket_subject">Oggetto</label>
                            <p class="form-control-static">
                                <textarea name="ticket_subject" id="ticket_subjectContent">{$tickets['ticket_view']['generale']['oggetto']}</textarea>
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <label for="id_azione">ID Ticket</label>
                            <p class="form-control-static">
                                <input class="textbox-fix lettura" type="text" id="id_azione" name="id_azione" value="{$tickets['ticket_view']['generale']['idunivoco']}" disabled>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="aperto_da">Aperto da</label>
                            <p class="form-control-static">
                                <input class="textbox-fix lettura" type="text" id="aperto_da" name="aperto_da" value="{$tickets['ticket_view']['generale']['aperto_da']}" disabled>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="data_apertura">Data apertura</label>
                            <p class="form-control-static">
                                <input class="textbox-fix" type="text" id="data_apertura" name="data_apertura" value="{$tickets['ticket_view']['generale']['data_apertura']}" disabled>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="data_ultima_com">Data ultima com.</label>
                            <p class="form-control-static">
                                <input class="textbox-fix" type="text" id="data_ultima_com" name="data_ultima_com" value="{$tickets['ticket_view']['generale']['data_ultima_com']}" disabled>
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <label for="assegnaimpiegato">In carico a</label>
                            <p class="form-control-static">
                                <select id="assegnaimpiegato" name="assegnaimpiegato" class="form-control textbox-fix lettura">
                                    <option value="0">Da assegnare</option>
                                    {foreach $tickets['ticket_view']['generale']['in_carico_a_options'] AS $key => $row}
                                    <option value="{$row["id_employee"]}"{if $row["id_employee"] == $tickets['ticket_view']['generale']['azione']['id_employee']} selected{/if}>{$row["firstname"]}</option>
                                    {/foreach}
                                </select>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="id_contact">Tipo ticket</label>
                            <p class="form-control-static">
                                {if $tickets['ticket_view']['generale']['azione']['id_contact'] == 9}
                                <input class="textbox-fix" type="text" id="contact_rma" name="contact_rma" value="RMA" disabled>
                                <input type="hidden" name="id_contact" id="id_contact" value="9">
                                {else}
                                <select id="id_contact" name="id_contact">
                                    {foreach $tickets['ticket_view']['action_types'] as $tipo}
                                    <option value="{$tipo['value']}" {if $tickets['ticket_view']['generale']['azione']['id_contact'] == "{$tipo['value']}" } selected{/if}>{$tipo['name']}</option>
                                    {/foreach}
                                </select>
                                {/if}
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="cambiastatus">Status {$tickets['ticket_view']['generale']['status_azione']}</label>
                            <p class="form-control-static">
                                <select id="cambiastatus" name="cambiastatus" class="form-control textbox-fix lettura">
                                    <option value="closed"{if $tickets['ticket_view']['generale']['azione']['status'] == "closed" } selected{/if}>Chiuso</option>
                                    <option value="pending1"{if $tickets['ticket_view']['generale']['azione']['status'] == "pending1" } selected{/if}>In lavorazione</option>
                                    <option value="open"{if $tickets['ticket_view']['generale']['azione']['status'] == "open" } selected{/if}>Aperto</option>
                                </select>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="cambiapriorita">Urgenza</label>
                            <p class="form-control-static">
                                <select id="cambiapriorita" name="cambiapriorita" class="form-control textbox-fix lettura">
                                    <option value=""{if $tickets['ticket_view']['generale']['azione']['priorita'] == "" } selected{/if}>--</option>
                                    <option value="low"{if $tickets['ticket_view']['generale']['azione']['priorita'] == "low" } selected{/if}>Bassa</option>
                                    <option value="medium"{if $tickets['ticket_view']['generale']['azione']['priorita'] == "medium" } selected{/if}>Media</option>
                                    <option value="high"{if $tickets['ticket_view']['generale']['azione']['priorita'] == "high" } selected{/if}>Alta</option>
                                </select>
                            </p>
                        </div>
                    </div>

                    {if $tickets['ticket_view']['generale']['azione']['id_contact'] == 9} {* RMA *}
                    <div class="row">
                        <div class="form-group col-md-2">
                            <label for="id_order">Numero fattura</label>
                            <p class="form-control-static">
                                <select id="id_order" name="id_order" class="form-control textbox-fix lettura">
                                    <option value="0">--</option>
                                    {$tickets['ticket_view']['generale']['rma_fatture_options']}
                                </select>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="codice_rma">Codice RMA</label>
                            <p class="form-control-static">
                                <input class="textbox-fix lettura" type="text" id="codice_rma" name="codice_rma" value="{$dati_rma['codice_rma']}" readonly>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="seriale">Codice seriale prodotto</label>
                            <p class="form-control-static">
                                <input class="textbox-fix" type="text" id="seriale" name="seriale" value="{$dati_rma['seriale']}">
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="qta">Qta prodotto</label>
                            <p class="form-control-static">
                                <input class="textbox-fix" type="text" id="qta" name="qta" value="{$dati_rma['qta']}">
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="in_garanzia">In garanzia?</label>
                            <p class="form-control-static">
                                <input class="textbox-fix" type="checkbox" id="in_garanzia" name="in_garanzia" value="1" {if $dati_rma['in_garanzia'] == 1}checked{/if}>
                            </p>
                        </div>
                    </div>
                    {/if}

                    {if $tickets['ticket_view']['generale']['azione']['riferimento'] != '' OR $tickets['ticket_view']['generale']['azione_padre'] != ''}
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="azione_padre">Azione padre</label>
                            <p class="form-control-static">
                                {$tickets['ticket_view']['generale']['azione_padre']}
                            </p>
                        </div>
                    </div>
                    {/if}

                    <div class="row">
                        <div class="form-group col-md-8">
                            <label for="id_product">Prodotto di riferimento</label>
                            <p class="form-control-static">
                                <select id="id_product" name="id_product" class="form-control textbox-fix">
                                    <option value="">--</option>
                                    {$tickets['ticket_view']['generale']['prodotto_rif_options']}
                                </select>
                            </p>
                        </div>
                        <div class="form-group col-md-4">
                            <p class="form-control-static">{$tickets['ticket_view']['generale']['prodotto_rif']}</p>
                        </div>
                    </div>

                    {if $tickets['ticket_view']['generale']['azione']['id_contact'] == 4 && $smarty.get.id_customer_thread > 0} {* Assistenza tecnica *}
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="descrizione_altro_tkt">Motivo chiamata</label>
                            <p class="form-control-static">
                                <input type="text" value="{$tickets['ticket_view']['generale']['azione']['motivo_chiamata']}" id="motivo_chiamata_tkt" name="motivo_chiamata_tkt" />
                            </p>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="descrizione_tkt">Descrizione</label>
                            <p class="form-control-static">
                                <select class="form-control textbox-fix" value="{$tickets['ticket_view']['generale']['azione']['descrizione']}" id="descrizione_tkt" name="descrizione_tkt" onchange="if(this.value != 'Altro') { document.getElementById('descrizione_altro_tkt').value = '' }">
                                    <option value="Ripristino funzionalita" {if $tickets['ticket_view']['generale']['azione']['descrizione'] == 'Ripristino funzionalita'}selected="selected"{/if}>Ripristino funzionalit&agrave;</option>
                                    <option value="Upgrade firmware" {if $tickets['ticket_view']['generale']['azione']['descrizione'] == 'Upgrade firmware'}selected="selected"{/if}>Upgrade firmware</option>
                                    <option value="Configurazione servizi" {if $tickets['ticket_view']['generale']['azione']['descrizione'] == 'Configurazione servizi'}selected="selected"{/if}>Configurazione servizi</option>
                                    <option value="Configurazione nuovo hardware" {if $tickets['ticket_view']['generale']['azione']['descrizione'] == 'Configurazione nuovo hardware'}selected="selected"{/if}>Configurazione nuovo hardware</option>
                                    <option value="Altro" {if $tickets['ticket_view']['generale']['azione']['descrizione']|substr:0:5 == 'Altro'}selected="selected"{/if}>Altro</option>
                                </select>
                            </p>
                        </div>

                        <div class="form-group col-md-8">
                            <label for="descrizione_altro_tkt">Se altro, specificare</label>
                            <p class="form-control-static">
                                <input type="text" id="descrizione_altro_tkt" name="descrizione_altro_tkt" value="{$tickets['ticket_view']['generale']['altro_descrizione']}" onkeyup="document.getElementById('descrizione_tkt').value = 'Altro';" />
                            </p>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="guasto_tkt">Guasto riscontrato dal tecnico</label>
                            <p class="form-control-static">
                                <select class="form-control textbox-fix" value="{$tickets['ticket_view']['generale']['azione']['guasto']}" id="guasto_tkt" name="guasto_tkt" onchange="if(this.value != 'Altro') { document.getElementById('guasto_altro_tkt').value = '' }">
                                    <option value="Nessuna" {if $tickets['ticket_view']['generale']['azione']['guasto'] == 'Nessuna' || $tickets['ticket_view']['generale']['azione']['guasto'] == ''}selected="selected"{/if}>Nessuno</option> {* correggere: posso cambiare Nessuna in Nessuno? *}
                                    <option value="Impianto fermo" {if $tickets['ticket_view']['generale']['azione']['guasto'] == 'Impianto fermo'}selected="selected"{/if}>Impianto fermo</option>
                                    <option value="Problemi su linee telefoniche" {if $tickets['ticket_view']['generale']['azione']['guasto'] == 'Problemi su linee telefoniche'}selected="selected"{/if}>Problemi su linee telefoniche</option>
                                    <option value="Problemi di fonia" {if $tickets['ticket_view']['generale']['azione']['guasto'] == 'Problemi di fonia'}selected="selected"{/if}>Problemi di fonia</option>
                                    <option value="Altro" {if $tickets['ticket_view']['generale']['azione']['guasto']|substr:0:5 == 'Altro'}selected="selected"{/if}>Altro</option>
                                </select>
                            </p>
                        </div>

                        <div class="form-group col-md-8">
                            <label for="guasto_altro_tkt">Se altro, specificare</label>
                            <p class="form-control-static">
                                <input type="text" id="guasto_altro_tkt" name="guasto_altro_tkt" value="{$tickets['ticket_view']['generale']['altro_guasto']}" onkeyup="document.getElementById('guasto_tkt').value = 'Altro';" />
                            </p>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="causa_guasto_tkt">Causa guasto</label>
                            <p class="form-control-static">
                                <select class="form-control textbox-fix" value="{$tickets['ticket_view']['generale']['azione']['causa_guasto']}" id="causa_guasto_tkt" name="causa_guasto_tkt" onchange="if(this.value != 'Altro') { document.getElementById('causa_guasto_altro_tkt').value = '' }">
                                    <option value="Nessuno" {if $tickets['ticket_view']['generale']['azione']['causa_guasto'] == 'Nessuno' || $tickets['ticket_view']['generale']['azione']['causa_guasto'] == ''}selected="selected"{/if}>Nessuno</option>
                                    <option value="Sovratensione" {if $tickets['ticket_view']['generale']['azione']['causa_guasto'] == 'Sovratensione'}selected="selected"{/if}>Sovratensione</option>
                                    <option value="Dolo/Incuria/Accidentale" {if $tickets['ticket_view']['generale']['azione']['causa_guasto'] == 'Dolo/Incuria/Accidentale'}selected="selected"{/if}>Dolo/Incuria/Accidentale</option>
                                    <option value="Ossido/Umidita" {if $tickets['ticket_view']['generale']['azione']['causa_guasto'] == 'Ossido/Umidita'}selected="selected"{/if}>Ossido/Umidita</option>
                                    <option value="Usura" {if $tickets['ticket_view']['generale']['azione']['causa_guasto'] == 'Usura'}selected="selected"{/if}>Usura</option>
                                    <option value="Non definita" {if $tickets['ticket_view']['generale']['azione']['causa_guasto'] == 'Non definita'}selected="selected"{/if}>Non definita</option>
                                    <option value="Altro" {if $tickets['ticket_view']['generale']['azione']['causa_guasto']|substr:0:5 == 'Altro'}selected="selected"{/if}>Altro</option>
                                </select>
                            </p>
                        </div>

                        <div class="form-group col-md-8">
                            <label for="causa_guasto_altro_tkt">Se altro, specificare</label>
                            <p class="form-control-static">
                                <input type="text" id="causa_guasto_altro_tkt" name="causa_guasto_altro_tkt" value="{$tickets['ticket_view']['generale']['altro_causa_guasto']}" onkeyup="document.getElementById('causa_guasto_tkt').value = 'Altro';" />
                            </p>
                        </div>
                    </div>
                    {/if}

                    {if $tickets['ticket_view']['generale']['azione']['id_contact'] == 9} {* RMA *}
                    <div class="row">
                        <div class="form-group col-md-2">
                            <label for="rma_type">Tipo RMA</label>
                            <p class="form-control-static">
                                <select id="rma_type" name="rma_type" class="form-control textbox-fix">
                                    <option value="Reso" {if $dati_rma['rma_type'] == "Reso"}selected{/if}>Reso</option>
                                    <option value="Riparazione" {if $dati_rma['rma_type'] == "Riparazione"}selected{/if}>Riparazione</option>
                                </select>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="rma_shipping">Spedizione</label>
                            <p class="form-control-static">
                                <select id="rma_shipping" name="rma_shipping" class="form-control textbox-fix">
                                    <option value="Corriere Ezdirect" {if $dati_rma['rma_shipping'] == "Corriere Ezdirect"}selected{/if}>Corriere Ezdirect</option>
                                    <option value="Corriere Cliente" {if $dati_rma['rma_shipping'] == "Corriere Cliente"}selected{/if}>Corriere Cliente</option>
                                </select>
                            </p>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="indirizzo_rma">Indirizzo spedizione reso</label>
                            <p class="form-control-static">
                                <select id="indirizzo_rma" name="indirizzo_rma" class="form-control textbox-fix">
                                    {foreach $dati_rma['indirizzi_rma'] as $indirizzo_rma}
                                    <option value="{$indirizzo_rma['id_address']}" {if $indirizzo_rma['id_address'] == $dati_rma['id_address']}selected{/if}>{$indirizzo_rma['address1']} - {$indirizzo_rma['postcode']} {$indirizzo_rma['city']} ({$indirizzo_rma['iso_code']})</option>
                                    {/foreach}
                                </select>
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <label for="arrivoezcli">Arrivato c/o EZ</label>
                            <p class="form-control-static">
                                <input class="textbox-fix" type="checkbox" id="arrivoezcli" name="arrivoezcli" value="1" {if $dati_rma['arrivoezcli'] == 1}checked{/if} />
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="arrivocli">Data arrivo cliente</label>
                            <p class="form-control-static"> {* Correggere: trasformare in data *}
                                <input class="textbox-fix" type="text" id="arrivocli" name="arrivocli" value="{$dati_rma['arrivocli']}" />
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="a_carico">A carico di</label>
                            <p class="form-control-static">
                                <div class="form-group col-md-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" id="a_carico_0" name="a_carico" value="0" {if $dati_rma['a_carico'] == 0}checked{/if} />
                                        <label class="form-check-label" style="font-weight:normal;" for="a_carico">Cliente</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" id="a_carico_1" name="a_carico" value="1" {if $dati_rma['a_carico'] == 1}checked{/if} />
                                        <label class="form-check-label" style="font-weight:normal;" for="a_carico">EZ</label>
                                    </div>
                                </div>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="corriere">Corriere</label>
                            <p class="form-control-static">
                                <select id="corriere" name="corriere" class="form-control textbox-fix">
                                    <option value="0" {if $dati_rma['corriere'] == 0 || $dati_rma['corriere'] == ''}selected{/if}>GLS</option>
                                    <option value="1" {if $dati_rma['corriere'] == 1}selected{/if}>Bartolini</option>
                                    <option value="2" {if $dati_rma['corriere'] == 2}selected{/if}>TNT</option>
                                    <option value="3" {if $dati_rma['corriere'] == 3}selected{/if}>Poste</option>
                                    <option value="5" {if $dati_rma['corriere'] == 5}selected{/if}>DHL</option>
                                    <option value="6" {if $dati_rma['corriere'] == 6}selected{/if}>UPS</option>
                                    <option value="7" {if $dati_rma['corriere'] == 7}selected{/if}>SDA</option>
                                    <option value="4" {if $dati_rma['corriere'] == 4}selected{/if}>Altro</option>
                                </select>
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <label for="test">Testato</label>
                            <p class="form-control-static"> {* Correggere: controllare che i 2 valori non siano scambiati *}
                                <div class="form-group col-md-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" id="test_0" name="test" value="0" {if $dati_rma['test'] == 0}checked{/if} />
                                        <label class="form-check-label" style="font-weight:normal;" for="test">S&igrave;</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" id="test_1" name="test" value="1" {if $dati_rma['test'] == 1}checked{/if} />
                                        <label class="form-check-label" style="font-weight:normal;" for="test">No</label>
                                    </div>
                                </div>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="test_in_carico_a">Test in carico a</label>
                            <p class="form-control-static">
                                <select id="test_in_carico_a" name="test_in_carico_a" class="form-control textbox-fix">
                                    <option value="0" {if $dati_rma['test_in_carico_a'] == 0}selected{/if}>Da assegnare</option>
                                    {foreach $dati_rma['impiegati_test'] as $impezt}
                                    <option value="{$impezt['id_employee']}" {if $impezt['id_employee'] == $dati_rma['test_in_carico_a']}selected{/if}>{$impezt['firstname']}</option>
                                    {/foreach}
                                </select>
                            </p>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="problematica_test">Problematica da testare</label>
                            <p class="form-control-static">
                                <input class="textbox-fix" type="text" id="problematica_test" name="problematica_test" value="{$dati_rma['problematica_test']}" />
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="arrivoezcli">Urgente</label>
                            <p class="form-control-static">
                                <input class="textbox-fix" type="checkbox" id="urgente" name="urgente" value="1" {if $dati_rma['urgente'] == 1}checked{/if} />
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-2">
                            <label for="richiesto_rma">Richiesto forn.</label>
                            <p class="form-control-static">
                                <input class="textbox-fix" type="checkbox" id="richiesto_rma" name="richiesto_rma" value="1" {if $dati_rma['richiesto_rma'] == 1}checked{/if} />
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="data_richiesta">Data richiesta</label>
                            <p class="form-control-static"> {* Correggere: trasformare in data *}
                                <input class="textbox-fix" type="text" id="data_richiesta" name="data_richiesta" value="{$dati_rma['data_richiesta']}" />
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="codice_rma_fornitore">Codice RMA forn.</label>
                            <p class="form-control-static">
                                <input class="textbox-fix" type="text" id="codice_rma_fornitore" name="codice_rma_fornitore" value="{$dati_rma['codice_rma_fornitore']}" />
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="">Fornitore</label>
                            <p class="form-control-static"> {* correggere: vedi 1.4 *}
                                <select id="" name="" class="form-control textbox-fix">
                                    <option value="prova">Da finire...</option>
                                    {* finire... *}
                                </select>
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="arrivoezfornitore">Arrivato c/o EZ dal forn.</label>
                            <p class="form-control-static"> {* Correggere: trasformare in data *}
                                <input class="textbox-fix" type="text" id="arrivoezfornitore" name="arrivoezfornitore" value="{$dati_rma['arrivoezfornitore']}" />
                            </p>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="sostituitoriparato">Sostituito / Riparato</label>
                            <p class="form-control-static">
                                <div class="form-group col-md-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" id="sostituitoriparato_0" name="sostituitoriparato" value="0" {if $dati_rma['sostituitoriparato'] == 0}checked{/if} />
                                        <label class="form-check-label" style="font-weight:normal;" for="sostituitoriparato">Sostituito</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" id="sostituitoriparato_1" name="sostituitoriparato" value="1" {if $dati_rma['sostituitoriparato'] == 1}checked{/if} />
                                        <label class="form-check-label" style="font-weight:normal;" for="sostituitoriparato">Riparato</label>
                                    </div>
                                </div>
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="attivita_svolta">Attivit&agrave; svolta</label>
                            <p class="form-control-static">
                                <select id="attivita_svolta" name="attivita_svolta" class="form-control textbox-fix">
                                    <option value="0" {if $dati_rma['attivita_svolta'] == 0 || $dati_rma['attivita_svolta'] == ''}selected{/if}>Nessuna</option>
                                    <option value="1" {if $dati_rma['attivita_svolta'] == 1}selected{/if}>Sostituito con nuovo</option>
                                    <option value="2" {if $dati_rma['attivita_svolta'] == 2}selected{/if}>Riparato</option>
                                    <option value="3" {if $dati_rma['attivita_svolta'] == 3}selected{/if}>Inviato al centro assistenza</option>
                                    <option value="4" {if $dati_rma['attivita_svolta'] == 4}selected{/if}>Altro</option>
                                </select>
                            </p>
                        </div>

                        <div class="form-group col-md-8">
                            <label for="altra_attivita">Se altro, specificare</label>
                            <p class="form-control-static">
                                <input type="text" id="altra_attivita" name="altra_attivita" value="{$dati_rma['altra_attivita']}" />
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel col-md-5">
                            <div class="panel-heading">
                                Dettaglio costi
                            </div>
                            <div class="form-group">
                                <table class="table">
                                    <tr>
                                        <td>Costo materiale</td>
                                        <td></td>
                                        <td>
                                            <div class="input-group">
                                                <span class="input-group-addon"> €</span>
                                                <input type="text" name="costo_materiale" id="costo_materiale" value="{$dati_rma['costo_materiale']}" style="text-align:right" onkeyup="CalcTotRMA();" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Costo manodopera</td>
                                        <td>
                                            <div class="input-group">
                                                <span class="input-group-addon"> Ore</span>
                                                <select name="manodopera_qta" id="manodopera_qta" onchange="document.getElementById('costo_manodopera').value = document.getElementById('manodopera_qta').value*40; CalcTotRMA();">
                                                    <option value="0"   {if $dati_rma['manodopera_qta'] == 0}  selected{/if}>Scegli</option>
                                                    <option value="0.5" {if $dati_rma['manodopera_qta'] == 0.5}selected{/if}>0,5</option>
                                                    <option value="1"   {if $dati_rma['manodopera_qta'] == 1}  selected{/if}>1</option>
                                                    <option value="1.5" {if $dati_rma['manodopera_qta'] == 1.5}selected{/if}>1,5</option>
                                                    <option value="2"   {if $dati_rma['manodopera_qta'] == 2}  selected{/if}>2</option>
                                                    <option value="2.5" {if $dati_rma['manodopera_qta'] == 2.5}selected{/if}>2,5</option>
                                                    <option value="3"   {if $dati_rma['manodopera_qta'] == 3}  selected{/if}>3</option>
                                                    <option value="3.5" {if $dati_rma['manodopera_qta'] == 3.5}selected{/if}>3,5</option>
                                                    <option value="4"   {if $dati_rma['manodopera_qta'] == 4}  selected{/if}>4</option>
                                                    <option value="4.5" {if $dati_rma['manodopera_qta'] == 4.5}selected{/if}>4,5</option>
                                                    <option value="5"   {if $dati_rma['manodopera_qta'] == 5}  selected{/if}>5</option>
                                                    <option value="5.5" {if $dati_rma['manodopera_qta'] == 5.5}selected{/if}>5,5</option>
                                                    <option value="6"   {if $dati_rma['manodopera_qta'] == 6}  selected{/if}>6</option>
                                                    <option value="6.5" {if $dati_rma['manodopera_qta'] == 6.5}selected{/if}>6,5</option>
                                                    <option value="7"   {if $dati_rma['manodopera_qta'] == 7}  selected{/if}>7</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <span class="input-group-addon"> €</span>
                                                <input type="text" name="costo_manodopera" id="costo_manodopera" value="{$dati_rma['costo_manodopera']}" style="text-align:right" onkeyup="CalcTotRMA();" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Trasporti qta</td>
                                        <td>
                                            <div>
                                                <input type="text" name="trasporti_qta" id="trasporti_qta" value="{$dati_rma['trasporti_qta']}" style="text-align:right" onkeyup="document.getElementById('totale_trasporti').value = (document.getElementById('trasporti_qta').value*9.90).toFixed(2).replace('.',','); CalcTotRMA();" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <span class="input-group-addon"> €</span>
                                                <input type="text" name="totale_trasporti" id="totale_trasporti" value="{$dati_rma['totale_trasporti']}" style="text-align:right" onkeyup="CalcTotRMA();" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Totale</td>
                                        <td></td>
                                        <td>
                                            <div class="input-group">
                                                <span class="input-group-addon"> €</span>
                                                <input type="text" name="totale_RMA" id="totale_RMA" value="{$dati_rma['totale_rma']}" style="text-align:right" readonly />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="form-group col-md-5">
                            <label for="template_rma">Template RMA</label>
                            <p class="form-control-static">
                                <span id="template_rma" name="template_rma"><a href="http://www.ezdirect.it/rma/{$dati_rma['codice_rma']}.html" target="_blank">http://www.ezdirect.it/rma/{$dati_rma['codice_rma']}.html</a></span>
                            </p>
                        </div>
                    </div>
                    {/if}

                    {if $old}
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="ticket_note">Nota</label>
                            <p class="form-control-static">
                                <textarea name="ticket_note" id="ticketnoteContent">{$old_nota|escape:'htmlall'}</textarea>
                            </p>
                        </div>
                    </div>
                    <br /><br />
                    {/if}

                    <div class="row">
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-primary" name="submitTicket"><i class="icon-save" alt="Salva" title="Salva"></i> {l s='Save'}</button>
                        </div>
                        <div class="form-group col-md-3">
                            <button type="button" class="btn btn-primary" onclick="javascript:var surec=window.confirm('Sei sicuro?'); if (surec) { $('#coll_destinazione').val($('#collega_ticket').val()); document.forms['collega_attivita'].submit(); return false; } else { return false; }"><i class="icon-link" alt="Collega" title="Collega"></i> {l s='Connect to'}</button>
                            <p class="form-control-static">
                                <select class="form-control textbox-fix" name="collega_ticket" id="collega_ticket">
                                    <option value="">-- Seleziona --</option>
                                    {$tickets['ticket_view']['generale']['collega_a']}
                                </select>
                            </p>
                        </div>
                        <div class="form-group col-md-4">
                            {if $check_programmer}<a class="btn btn-primary" href="{$link_gen_base}tab_name=tickets&amp;azione=tickets&amp;viewticket&amp;id_customer_thread={$smarty.get.id_customer_thread}&amp;getPDF&amp;id={$smarty.get.id_customer_thread}&amp;tipo=T{$token_base}">{l s='PDF Export'}</a>{/if}
                            {if $bdl['bdl_ticket'] == 0}<a class="btn btn-primary" href="{$link_gen_base}tab_name=bdl&amp;azione=bdl_add&amp;riferimento=T{$smarty.get.id_customer_thread}{$token_base}" target="_blank"><i class="icon-wrench" alt="Nuovo BDL" title="Nuovo BDL"></i> {l s='Generate new BDL'}</a>{/if}
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="form-horizontal col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    <i class="icon-eye-close"></i> {l s='Private notes'}

                    <div class="panel-heading-action">
                        <a class="btn btn-default add-new">
                            <i class="icon-plus-sign"></i>
                            {l s='Add private note'}
                        </a>
                    </div>
                </div>
                <div class="form-group">
                    <table class="table table-bordered tabella_note" id="noteTable">
                        <thead>
                            <tr>
                                <th>{l s='Note'}</th>
                                <th>{l s='Created by'}</th>
                                <th>{l s='Last edit'}</th>
                                <th>{l s='Actions'}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="display:none"> {* Necessario per aggiungere la prima nota in js *}
                                <td class="mod"></td>
                                <td></td>
                                <td></td>
                                <td class="azioni_note">
                                    <a class="add" title="Add" data-toggle="tooltip"><i class="icon-save"></i></a>
                                    <a class="edit" title="Edit" data-toggle="tooltip"><i class="icon-edit"></i></a>
                                    <a class="delete" title="Delete" data-toggle="tooltip"><i class="icon-trash"></i></a>
                                </td>
                            </tr>
                        {foreach $tickets['ticket_view']['generale']['azione_note'] as $azione_nota}
                            <tr dbid="{$azione_nota['id_note']}" id="tr_note_{$azione_nota['id_note']}">
                                <td id="note_nota[{$azione_nota['id_note']}]" class="mod">{$azione_nota['note']}</td>
                                <td id="note_id_employee[{$azione_nota['id_note']}]">{$azione_nota['creato_da']}</td>
                                <td id="note_date_add[{$azione_nota['id_note']}]">{$azione_nota['date_upd']}</td>
                                <td class="azioni_note">
                                    <a class="add" title="Add" data-toggle="tooltip"><i class="icon-save"></i></a>
                                    <a class="edit" title="Edit" data-toggle="tooltip"><i class="icon-edit"></i></a>
                                    <a class="delete" title="Delete" data-toggle="tooltip"><i class="icon-trash"></i></a>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </form>

{/if}

    <div class="form-horizontal col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-user"></i>
                {if $smarty.get.id_customer_thread !== null && $smarty.get.aprinuovoticket === null}
                Cronologia
                {else}
                Nuovo Ticket
                {/if}
            </div>

            {if $smarty.get.id_customer_thread !== null && $smarty.get.aprinuovoticket === null}

            {foreach $messaggia AS $key => $messaggioa}
                <div style="background: rgba(255, 192, 203, 0.5); border: 1px solid lightpink; margin-bottom:1%;">
                    <div class="row" style="padding-left:2%; padding-top:1%;">
                        <div class="form-group col-md-3">
                            <label>Da</label>
                            <p class="form-control-static">
                                {$messaggioa['da']}
                            </p>
                        </div>

                        <div class="form-group col-md-3">
                            <label>Data</label>
                            <p class="form-control-static">
                                {$messaggioa['data']}
                            </p>
                        </div>

                        <div class="form-group col-md-3">
                            <label>Allegati</label>
                            <p class="form-control-static">
                                {$messaggioa['allegati']}
                            </p>
                        </div>
                    </div>

                    <div class="row" style="padding-left:2%;">
                        <div class="form-group col-md-8">
                            <label>Messaggio</label>
                            <p class="form-control-static">
                                {$messaggioa['message']}
                            </p>
                        </div>
                        <div class="form-group col-md-4">
                            <form method="post">
                                <input type="hidden" name="modifica_messaggio_ticket" value="{$messaggioa['message']|escape:'htmlall'}" />
                                <button type="submit" class="btn btn-secondary" name="modifica_messaggio_submit" value="Modifica">Modifica</button> 
                            </form>
                        </div>
                    </div>

                    {if $messaggioa['note_private']}
                    <div class="row rosso" style="padding-left:2%;">
                        <div class="form-group col-md-3">
                            <label>Nota privata</label>
                            <p class="form-control-static">
                                {$messaggioa['note_private']}
                            </p>
                        </div>
                    </div>
                    {/if}

                    <div class="row" style="padding-left:2%;">
                        {if $messaggioa['email']}
                        <div class="form-group col-md-3">
                            <label>Inviato a</label>
                            <p class="form-control-static">
                                {$messaggioa['employee_to_name']}
                            </p>
                        </div>
                        {/if}

                        <div class="form-group col-md-3">
                            <label>In carico a</label>
                            <p class="form-control-static">
                                {$messaggioa['in_carico_a_name']}
                            </p>
                        </div>

                        <div class="form-group col-md-3">
                            <label>CC</label>
                            <p class="form-control-static">
                                {$messaggioa['cc']}
                            </p>
                        </div>
                    </div>

                    {if $messaggioa['riferimenti'] != ""}
                    <div class="row" style="padding-left:2%;">
                        <div class="form-group col-md-12">
                            <p class="form-control-static">
                                {$messaggioa['riferimenti']}
                            </p>
                        </div>
                    </div>
                    {/if}

                    <div class="row" style="padding-left:2%;">
                    </div>
                </div>
            {/foreach}

            {/if}


<form name="submit_ticket" id="submit_ticket" action="{$link_gen_base}tab_name=tickets&amp;azione=tickets&amp;viewticket{if isset($smarty.get.id_customer_thread)}&amp;id_customer_thread={$smarty.get.id_customer_thread}{else}&amp;aprinuovoticket{/if}{if isset($smarty.get.riferimento)}&amp;riferimentoo={$smarty.get.riferimento}{/if}{$token_base}" method="post">
            
            {if $smarty.get.id_customer_thread !== null && $smarty.get.aprinuovoticket === null}
            <h2 id="rispondi-azione">Rispondi al cliente</h2>
            {else}
            <h2 id="apri-azione">Apri ticket per conto del cliente</h2>
            <strong style="color:red">ATTENZIONE!!!!!!</strong>: quando si apre un ticket per il cliente, il testo del ticket viene notificato anche al cliente tramite mail.
            <br /><br />
            {/if}
            
            <input type="hidden" name="id_customer" value="{$customer->id}" />
			<input type="hidden" name="id_customer_thread" value="{$smarty.get.id_customer_thread}" />

            <div class="row">

                {if $smarty.get.id_customer_thread !== null && $smarty.get.aprinuovoticket === null}
                <input type="hidden" name="id_contact" value="{$azione_t['id_contact']}" />
                <input type="hidden" name="isAnswer" value="1" />

                {else}
                
                <input type="hidden" name="nuovoticket" value="1" />
                <input type="hidden" name="riferimento" id="riferimento" value="{if isset($smarty.get.riferimento)}{$smarty.get.riferimento}{/if}" />
                <input type="hidden" name="riferimentoo" id="riferimentoo" value="{if isset($smarty.get.riferimento)}{$smarty.get.riferimento}{/if}" />
                
                <div class="form-group col-md-2">
                    <label for="id_contact">Tipo ticket</label>
                    <p class="form-control-static">
                        <select id="id_contact" name="id_contact" class="form-control textbox-fix lettura">
                            {foreach $tickets['ticket_view']['ticket_types'] as $tipo} {* Correggere tipo-ticket con tipo_ticket nel resto del codice *}
                                <option value="{$tipo['value']}" {if isset($smarty.get.tipo_ticket) && $smarty.get.tipo_ticket == "{$tipo['value']}"} selected{/if}>{$tipo['name']}</option>
                            {/foreach}
                        </select>
                    </p>
                </div>

                {/if}
                
                <div class="form-group col-md-2">
                    <label for="status">Stato ticket</label>
                    <p class="form-control-static">
                        <select id="status" name="status" class="form-control textbox-fix lettura">
                            <option value="open" {if $azione_t['status'] == "open" } selected{/if}>Aperto</option>
                            <option value="pending1" {if $azione_t['status'] == "pending1" } selected{/if}>In lavorazione</option>
                            <option value="closed" {if $azione_t['status'] == "closed" } selected{/if}>Chiuso</option>
                        </select>
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="priorita">Urgenza</label>
                    <p class="form-control-static">
                        <select id="priorita" name="priorita" class="form-control textbox-fix lettura">
                            <option value=""{if $tickets['ticket_view']['generale']['azione']['priorita'] == "" } selected{/if}>--</option>
                            <option value="low"{if $tickets['ticket_view']['generale']['azione']['priorita'] == "low" } selected{/if}>Bassa</option>
                            <option value="medium"{if $tickets['ticket_view']['generale']['azione']['priorita'] == "medium" } selected{/if}>Media</option>
                            <option value="high"{if $tickets['ticket_view']['generale']['azione']['priorita'] == "high" } selected{/if}>Alta</option>
                        </select>
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="in_carico_a">In carico a</label>
                    <p class="form-control-static">
                        <select id="in_carico_a" name="in_carico_a" class="form-control textbox-fix" onChange="changeInCaricoA();">
                        {foreach $tickets['ticket_view']['cronologia']['impiegati_eza'] as $impez}
                            <option value="{$impez['id_employee']}" {if $impez['id_employee'] == $tickets['ticket_view']['cronologia']['impez_selected']} selected{/if}>{$impez['firstname']}</option>
                        {/foreach}
                        </select>
                    </p>
                </div>

                {if $smarty.get.id_customer_thread !== null && $smarty.get.aprinuovoticket === null}

                {else}

                <div class="form-group col-md-2">
                    <label for="apri-todo">Apri un todo a</label>
                    <p class="form-control-static">
                        <select id="apri-todo" name="apri-todo" class="form-control textbox-fix">
                            <option value="">Nessuno</option>
                            {foreach $tickets['ticket_view']['cronologia']['impiegati_eza'] as $impez}
                            <option value="{$impez['id_employee']}">{$impez['firstname']}</option>
                            {/foreach}
                        </select>
                    </p>
                </div>

                {/if}

            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="email">Email</label>
                    <div class="row form-control-static">
                        {if $customer->is_company == 1}
                        <div class="col-md-3">
                            <select name="seleziona-personat" onchange="javascript:document.getElementById('customer_email_msg').value = (this.value);">
								<option value=''>-- Seleziona persona --</option>
								{foreach $tickets['ticket_view']['cronologia']['persone'] as $persona}
								<option value="{$persona['email']}" {if $persona['id_persona'] == $smarty.get.id_persona || $persona['id_persona'] == $tickets['ticket_view']['cronologia']['persona_preventivo']}selected{/if}>{$persona['firstname']} {$persona['lastname']}</option>
                                {/foreach}
                            </select>
                        </div>
                        {/if}
                        <div class="col-md-9">
                            <input class="textbox-fix" type="text" name="customer_email_msg" id="customer_email_msg" value="{if !$azione_t['email']}{$tickets['ticket_view']['cronologia']['mail_persona']}{else}{$azione_t['email']}{/if}" />
                            <span style="font-size:11px">In caso di più indirizzi mail, separare con punto e virgola (;)</span>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-2">
                    <label>Tel. Cliente</label>
                    <p class="form-control-static">
                        <input type="text" name="telefono_cliente" id="telefono_cliente" value="{$tickets['ticket_view']['cronologia']['telefono_cliente']}" readonly />
                        <a id="tel_action" href="callto:{$tickets['ticket_view']['cronologia']['telefono_cliente']}">Chiama</a>
                    </p>
                </div>
            </div>

            <div class="row">
                <label>Conoscenza</label>
                <br />
                {foreach $tickets['ticket_view']['cronologia']['impiegati_eza'] AS $impez}
                    <input type="checkbox" name="conoscenza_t[]" id="conoscenza_{$impez['id_employee']}" value="{$impez['id_employee']}"> {$impez['firstname']}&nbsp;&nbsp;&nbsp;&nbsp; 
                {/foreach}
            </div>

            <br />

            <div class="row"> {* Correggere anteprima e KCFINDER - condizione if($customer->id == 0) *}
                <div class="form-group col-md-6">
                    <label>Allega file</label>
                    <p class="form-control-static">
                        <input name="MAX_FILE_SIZE" value="20000000" type="hidden">
                        <input type="file" multiple class="multi" name="joinFile[]" id="joinFile" onchange="handleFileSelect(this.files)">
                        <output id="list-action">Anteprime: </output>
                    </p>
                </div>
            </div>

            <br />

            <div class="row" id="aggiungi_prodotti">

                <div class="form-row">
                    <div class="col-md-1">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                        <label class="form-check-label" for="inlineCheckbox1">Online</label>
                    </div>
                    <div class="col-md-1">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                        <label class="form-check-label" for="inlineCheckbox1">Offline</label>
                    </div>
                    <div class="col-md-1">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                        <label class="form-check-label" for="inlineCheckbox1">Old</label>
                    </div>
                    <div class="col-md-1">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                        <label class="form-check-label" for="inlineCheckbox1">{l s='Only av.'}</label> {* Solo disp. *}
                    </div>

                    <div class="col-md-2">
                        <select id="brand" class="chosen form-control" name="brand">
                            <option value="">{l s='Brand...'}</option>
                            {foreach $tickets['ticket_view']['cronologia']['marche'] AS $key => $marche}
                                <option value="{$marche['id']}">{$marche['name']}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select id="serie" class="chosen form-control" name="serie">
                            <option value="">{l s='Series...'}</option>
                            <option value="">Scegli prima un costruttore</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select id="categorie" class="chosen form-control" name="categorie">
                            <option value="">{l s='Category...'}</option>
                            {foreach $tickets['ticket_view']['cronologia']['categorie'] AS $key => $categorie}
                                <option value="{$categorie['id']}">{$categorie['name']}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select id="fornitori" class="chosen form-control" name="fornitori">
                            <option value="">{l s='Supplier...'}</option>
                            {foreach $tickets['ticket_view']['cronologia']['fornitori'] AS $key => $fornitori}
                                <option value="{$fornitori['id']}">{$fornitori['name']}</option>
                            {/foreach}
                        </select>
                    </div>

                    <div class="col-md-12">
                        <input type="text">
                    </div>
                </div>
            </div>

            <br />

            <div class="row">

                <div class="form-group col-md-6">
                    <label for="link_utili">Link utili</label>
                    <p class="form-control-static">
                        <select id="utils_autocomplete_input_tkt" name="utils_autocomplete_input_tkt" class="select2 form-control textbox-fix" onchange="addUtil_TR();">
                            {$utils_options}
                        </select>
                    </p>
                </div>

                <div class="form-group col-md-6">
                    <label for="messaggi_precompilati">Messaggi precompilati</label>
                    <p class="form-control-static">
                        <select id="messaggi_precompilati" name="messaggi_precompilati" class="select2 form-control textbox-fix" onchange="inserisciPrecompilato(this.value);">
                            <option value="-- Seleziona un messaggio --">-- Seleziona un messaggio --</option>
                        {foreach $tickets['ticket_view']['cronologia']['precompilati'] as $precompilato}
                            <option value="{$precompilato['testo']|escape:'htmlall'}">{$precompilato["oggetto"]|escape:'htmlall'}</option>
                        {/foreach}
                        </select>
                    </p>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-12">
                    <label>Messaggio</label>
                    <textarea class="rte autoload_rte" id="message" name="message" rows="5" cols="40">{if isset($smarty.post.modifica_messaggio_submit)}{$smarty.post.modifica_messaggio_ticket|stripslashes}{/if}</textarea>
                </div>
            </div>

            <br />

            <div class="row">
                <div class="col-lg-12">
                    <label class="rosso">Note private visibili solo da staff</label>
                    <span class="button thick pointer" style="display:block; text-align:center; background-color:#f0f0ee; border:1px solid #cccccc; text-decoration:none;" onclick="$('#mostra_nota_privata').slideToggle();"><img src="../img/admin/arrow.gif" alt="Dettaglio note private" title="Dettaglio note private" />Clicca qui per inserire note private/riservate - Non visibili all'utente.</span>
                    <div id="mostra_nota_privata" style="display:none;">
                        <textarea class="autoload_rte" id="note_private" name="note_private" rows="5" cols="20"></textarea>
                    </div>
                </div>
            </div>

            <br />

            {if $tickets['ticket_view']['cronologia']['riferimento'] != '' && $tickets['ticket_view']['cronologia']['statusPadre'] != 'closed' && $tickets['ticket_view']['cronologia']['statusPadre'] != ''}
            <div class="row">
                <div style="border:3px solid red; padding:5px;"><input type="checkbox" name="chiudi_padre" />&nbsp;&nbsp;&nbsp;<strong style="color:red">ATTENZIONE!</strong>  L'azione padre risulta ancora aperta. Spunta questa casella se vuoi chiuderla.</div>
            </div>
            {/if}

            <br />

            <button type="submit" class="btn btn-secondary" name="submitMessageTicket" id="submitMessageTicket" value="Modifica"><i class="icon-envelope"></i> Invia</button>
        </div>
    </div>

    {if isset($smarty.post.modifica_messaggio_ticket) && isset($smarty.post.modifica_messaggio_submit)}
    <script type="text/javascript"> window.location = "#rispondi-azione"; </script>
    {/if}

    {if isset($tinymce) && $tinymce}
    <script type="text/javascript">
        var iso = '{$iso|addslashes}';
        var pathCSS = '{$smarty.const._THEME_CSSDIR|addslashes}';
        var ad = '{$ad|addslashes}';

        $(document).ready(function(){
            {block name="autoload_tinyMCE"}
                tinySetup({
                    editor_selector :"autoload_rte"
                });
            {/block}
        });
    </script>
    {/if}

</form>

{if $smarty.get.id_customer_thread !== null && $smarty.get.aprinuovoticket === null}

    <div class="form-horizontal col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-user"></i>
                Gerarchia
            </div>

            <div class="tree">
                {$tree_ul}
            </div>
        </div>
    </div>

    <div class="form-horizontal col-lg-6">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-user"></i>
                Storico
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered" id="storicoTable">
                        <thead>
                            <tr>
                                <th>Persona</th>
                                <th>Azione</th>
                                <th>Data</th>
                            </tr>
                        </thead>

                        <tbody>
                        {foreach $storico_ticket AS $key => $storico}
                            <tr>
                                <td>{$storico['persona']}</td>
                                <td>{$storico['azione']}</td>
                                <td>{$storico['data_attivita']}</td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {if $bdl['bdl_ticket'] > 0}
    <div class="form-horizontal col-lg-6">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-wrench"></i>
                BDL
            </div>

            <div class="row">
                <div class="col-md-3">
                    <a class="btn btn-primary" href="{$link_gen_base}tab_name=bdl&amp;azione=bdl_edit&amp;id_bdl={$bdl['bdl_ticket']}{$token_base}"><i class="icon-wrench" alt="Vedi BDL" title="Vedi BDL"></i> Vedi BDL associato (n.{$bdl['bdl_ticket']})</a>
                </div>
            </div>
        </div>
    </div>
    {/if}

    <form name="collega_attivita" id="collega_attivita" action="{$smarty.server.REQUEST_URI}&conf=4" method="post">
        <input type="hidden" name="submitCollegaAttivita" value="" />
        <input type="hidden" name="coll_destinazione" id="coll_destinazione" value="" />
        <input type="hidden" name="coll_partenza" id="coll_partenza" value="T{$tickets['ticket_view']['generale']['azione']['id_customer_thread']}" />
    </form>

{/if}

{/if}

{* Javascript della pagina *}
{include file="./tickets_js.tpl"}

<script type="text/javascript" src="jquery.MultiFile.js"></script>