<script>

window.onload = function() {
    // dev only
};


function f_edit_btn(id_persona){
        //document.getElementById("edit_title").innerHTML = "(MODIFICA)";
        edit_campo("firstname",id_persona);
        edit_campo("lastname",id_persona);
        edit_campo("role",id_persona);
        edit_campo("phone",id_persona);
        edit_campo("phone_2",id_persona);
        edit_campo("phone_mobile",id_persona);
        edit_campo("email",id_persona);
        edit_campo("buttons",id_persona);
        

        //document.getElementById("tr_view_firstname_"+id_persona).style.display = "none";
        //document.getElementById("tr_edit_firstname_"+id_persona).style.display = "block";
}

function edit_campo(campo,id_persona){
    document.getElementById("tr_edit_"+campo+"_"+id_persona).style.display = "block";
    document.getElementById("tr_view_"+campo+"_"+id_persona).style.display = "none";
}

{literal}

function add_persone() {
    var possible = "0123456789";
    var randomid = "";
    for( var i=0; i < 11; i++ ) {
        randomid += possible.charAt(Math.floor(Math.random() * possible.length));
    }
            
    $("<tr id='tr_persona_"+randomid+"'><td><input type='hidden' name='persona_nuovo[]' value='1' /></td><td><input type='text' size='15' name='persona_firstname[]' /></td><td><input type='text' size='15' name='persona_lastname[]' /></td><td><input type='text' size='15' name='persona_role[]' /></td><td><input type='text' size='10' name='persona_phone[]' /></td><td><input type='text' size='10' name='persona_phone_2[]' /></td><td><input type='text' size='10' name='persona_phone_mobile[]' /></td><td><input type='text' size='10' name='persona_email[]' /></td><td><a class='btn btn-danger squadrato' href='#' onclick='togli_riga("+randomid+");'><i class='icon-trash' title='Cancella'></i></a></td></tr>").appendTo("#tablePersoneBody");

}
    
function togli_riga(id) {
    document.getElementById('tr_persona_'+id).innerHTML = "";
}
    
    
function cancella_persona(id) {
    $.ajax({
        url: {/literal}"{$link->getAdminLink('AdminCustomers')}&id_customer={$customer->id}&viewcustomer&tab_name=people&cancellapersone"{literal},
        type: "POST",
        data: { 
            cancella_persona: 'y',
            id_persona: id
        },
        success: function(){
            
        },
        error: function(xhr,stato,errori){
            alert("Errore nella cancellazione:"+xhr.status);
        }
    });

}
{/literal}

</script>