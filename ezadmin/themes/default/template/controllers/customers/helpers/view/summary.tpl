{if $riassunto_on && !$not_mio_agente}
<div class="col-lg-12">

    <div class="panel">
        <div class="panel-heading">
            <i class="icon-home"></i>
            {l s='Summary'}
        </div>
        <div class="form-row">
            <div class="row">
                
                <div class="form-group col-md-2">
                    <label for="customer" class="summary">{l s='Customer'}</label>
                    <p class="form-control-static">
                        {if isset($riassunto['cliente'])}
                            {if $customer->active == 0 OR $customer->active == 2}
                                <span class="rosso">{$riassunto['cliente']}</span>
                            {else}
                                {$riassunto['cliente']}
                            {/if}
                        {else}
                            {l s='Unknown'}
                        {/if}
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="codes" class="summary">{l s='CRM / eSolver'}</label>
                    <p class="form-control-static">
                        {if isset($riassunto['codici'])}{$riassunto['codici']}{else}{l s='Unknown'}{/if}
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="profile" class="summary">{l s='Profile'}</label>
                    <p class="form-control-static">
                        {if isset($riassunto['profilo'])}{$riassunto['profilo']}{else}{l s='Unknown'}{/if}
                        <span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="{$riassunto['tipo_profilo_title']}">
                            {$riassunto['tipo_profilo']}
                        </span>
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="customer_type" class="summary">{l s='Customer type'}</label>
                    <p class="form-control-static">
                        {if isset($riassunto['tipo_cliente'])}{$riassunto['tipo_cliente']}{else}{l s='Unknown'}{/if}
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="status" class="summary">{l s='Status'}</label>
                    <p class="form-control-static">
                        {if isset($riassunto['status'])}
                            {if $customer->active == 1}
                            <span class="verde">{$riassunto['status']}</span>
                            {else}
                            <span class="rosso">{$riassunto['status']}</span>
                            {/if}
                        {else}
                        {l s='Unknown'}
                        {/if}
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="registration" class="summary">{l s='Registration'}</label>
                    <p class="form-control-static">
                        {if isset($riassunto['registrazione'])}{$riassunto['registrazione']}{else}{l s='Unknown'}{/if}
                    </p>
                </div>

            </div>
            <div class="row">
                
                <div class="form-group col-md-2">
                    <label for="referent" class="summary">{l s='Referent'}</label>
                    <p class="form-control-static">
                        <select class="form-control" id="select_referent" onchange="aggiornaDatiCliente('referente', this.value);">
                            <option value="">{l s='Select Referent'}</option>
                            {if count($riassunto['referenti'])}
                                {foreach $riassunto['referenti'] AS $key => $referenti}
                                <option value="{$referenti['id']}"{if $referenti['id'] == $riassunto['referente']['id']} selected="selected"{/if}>{$referenti['id']} - {$referenti['name']}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="zone" class="summary">{l s='Zone'}</label>
                    <p class="form-control-static">
                        {if isset($riassunto['zona'])}{$riassunto['zona']}{else}{l s='Unknown'}{/if}
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="orders_6" class="summary">{l s='Orders (6 months)'}</label>
                    <p class="form-control-static">
                        {if isset($riassunto['ord_6'])}{$riassunto['ord_6']}{else}{l s='Unknown'}{/if}
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="orders_12" class="summary">{l s='Orders (1 year)'}</label>
                    <p class="form-control-static">
                        {if isset($riassunto['ord_12'])}{$riassunto['ord_12']}{else}{l s='Unknown'}{/if}
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="tot_ord" class="summary">{l s='Total orders'}</label>
                    <p class="form-control-static">
                        {if isset($riassunto['ord_tot'])}{$riassunto['ord_tot']}{else}{l s='Unknown'}{/if}
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="num_ord" class="summary">{l s='Num. orders'}</label>
                    <p class="form-control-static">
                        {if isset($riassunto['num_ord'])}{$riassunto['num_ord']|intval}{else}{l s='Unknown'}{/if}
                    </p>
                </div>

            </div>
            <div class="row">
                
                <div class="form-group col-md-2">
                    <label for="agent" class="summary">{l s='Agent'}</label>
                    <p class="form-control-static">
                        <select class="form-control{if $is_agente == 1} lettura{/if}" id="select_agent"{if $is_agente == 1} disabled{/if} onchange="aggiornaDatiCliente('agente', this.value);">
                            <option value="">{l s='Select Agent'}</option>
                            {if count($riassunto['impiegati'])}
                                {foreach $riassunto['impiegati'] AS $key => $impiegato}
                                <option value="{$impiegato['id']}"{if $impiegato['id'] == $riassunto['agente']['id']} selected="selected"{/if}>{$impiegato['id']} - {$impiegato['name']}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="technician" class="summary">{l s='Technician'}</label>
                    <p class="form-control-static">
                        <select class="form-control" id="select_tech" onchange="aggiornaDatiCliente('tecnico', this.value);">
                            <option value="">{l s='Select Technician'}</option>
                            {if count($riassunto['tecnici'])}
                                {foreach $riassunto['tecnici'] AS $key => $tecnico}
                                <option value="{$tecnico['id']}"{if $tecnico['id'] == $riassunto['tecnico']['id']} selected="selected"{/if}>{$tecnico['id']} - {$tecnico['name']}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="visits" class="summary">{l s='Visits'}</label>
                    <p class="form-control-static">
                        {if isset($riassunto['visite'])}{$riassunto['visite']}{else}{l s='Unknown'}{/if}
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="pages_visited" class="summary">{l s='Pages visited'}</label>
                    <p class="form-control-static">
                        {if isset($riassunto['pagine_viste'])}{$riassunto['pagine_viste']}{else}{l s='Unknown'}{/if}
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="last_product" class="summary">{l s='Last product'}</label>
                    <p class="form-control-static">
                        
                        {if isset($riassunto['ultimo_prod'])}
                            {if isset($riassunto['ultimo_prod_title'])}
                            <span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="{$riassunto['ultimo_prod_title']}">
                                {$riassunto['ultimo_prod']}
                            </span>
                            {else}
                            <p class="form-control-static">{$riassunto['ultimo_prod']}</p>
                            {/if}
                        {else}
                        <p class="form-control-static">{l s='Unknown'}</p>
                        {/if}
                        
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="last_pur" class="summary">{l s='Last purchase'}</label>
                    <p class="form-control-static">
                        {if isset($riassunto['ultimo_acq'])}{$riassunto['ultimo_acq']}{else}{l s='Unknown'}{/if}
                    </p>
                </div>

            </div>
            <div class="row">

                <div class="form-group col-md-2">
                    <label for="last_ch_ric" class="summary">{l s='Last ch. ric.'}</label>
                    <p class="form-control-static">
                        {if isset($riassunto['ultima_ch_ric'])}{$riassunto['ultima_ch_ric']}{else}{l s='Unknown'}{/if}
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="last_ch_inv" class="summary">{l s='Last ch. inv.'}</label>
                    <p class="form-control-static">
                        {if isset($riassunto['ultima_ch_inv'])}{$riassunto['ultima_ch_inv']}{else}{l s='Unknown'}{/if}
                    </p>
                </div>

                <div class="form-group col-md-2">
                    <label for="last_action" class="summary">{l s='Last action'}</label>
                    <p class="form-control-static">
                        {if isset($riassunto['ultima_azione'])}{$riassunto['ultima_azione']}{else}{l s='Unknown'}{/if}
                    </p>
                </div>

            </div>
        </div>
    </div>
</div>

{* Spostare in un file js *}
<script>
{literal}
function aggiornaDatiCliente(tipo, valore)
{
    $.ajax({
        url:'ajax.php?aggiorna_dati_cliente=y',
        type: 'POST',
        data: { 
            tipo: tipo, 
            valore: valore, 
            cliente: {/literal}{$customer->id}{literal}
        },
        success:function(resp){
        alert(resp + ' modificato');
        },
    });
}
{/literal}
</script>

{/if}
