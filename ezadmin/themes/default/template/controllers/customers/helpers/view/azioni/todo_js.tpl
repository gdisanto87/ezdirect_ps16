<script>

window.onload = function() {
    //listree();
}

/* Note private -> bug: non funziona la modifica di una nota appena creata */

{if $smarty.get.id_action !== null}

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
	var actions = $("td.azioni_note").html();

    $(".add-new").click(function(){
		$(this).attr("disabled", "disabled");
		var index = $("table.tabella_note > tbody > tr:last-child").index();
        var row = '<tr>' +
            //'<td><input type="text" class="form-control" name="nuova_nota" id="nuova_nota"></td>' +
            '<td><textarea class="form-control" name="nuova_nota" id="nuova_nota"></textarea></td>' +
            '<td>{$mio_nome}</td>' +
            '<td>{$today}</td>' +
			'<td>' + actions + '</td>' +
        '</tr>';
    	$("table.tabella_note").append(row);
		$("table.tabella_note > tbody > tr").eq(index + 1).find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip();
    });

	$(document).on("click", ".add", function(){
		var empty = false;
        var dbid = $(this).parents("tr").attr('dbid');
		//var input = $(this).parents("tr").find('input[type="text"]');
		var input = $(this).parents("tr").find('textarea');
        var questo = this;
        input.each(function(){
			if(!$(this).val()){
				$(this).addClass("error");
				empty = true;
			} else{
                $(this).removeClass("error");
            }
		});
		$(this).parents("tr").find(".error").first().focus();
		if(!empty){
			input.each(function(){
				$(this).parent("td").html($(this).val());
                if(dbid === undefined){
                    $.ajax({
                        url:"ajax.php?creanota_attivita=y",
                        type: "POST",
                        data: { 
                            id_action: {$smarty.get.id_action},
                            tipo: 'A',
                            nota: $(this).val()
                        },
                        success:function(data){
                            $(questo).parents("tr").attr('dbid', data);
                            tata.success('Nota creata con successo', 'ID '+data);
                        },
                        error: function(xhr,stato,errori){
                            tata.error('Errore nella creazione', xhr.status);
                        }
                    });
                } else {
                    $.ajax({
                        url:"ajax.php?modificanota_attivita=y",
                        type: "POST",
                        data: { 
                            id_nota: dbid,
                            nota: $(this).val()
                        },
                        success:function(){
                            tata.success('Nota modificata con successo', '');
                            succ = true;
                        },
                        error: function(xhr,stato,errori){
                            tata.error('Errore nella modifica', xhr.status);
                        }
                    });
                }
			});			
			$(this).parents("tr").find(".add, .edit").toggle();
			$(".add-new").removeAttr("disabled");
		}		
    });

    $(function() {
        $("input").keypress(function(event) {
            var $self = $(this);
            var comm = $self.val();

            var keycode = (event.keycode ? event.keycode : event.which);
            if (keycode == 13) {
                var comm = $self.val();
                alert(comm);
                event.stopPropagation();
            }
        });
    });

	$(document).on("click", ".edit", function(){		
        $(this).parents("tr").find("td:not(:last-child)").each(function(){
            if($(this).hasClass('mod')){ 
                //$(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">'); 
                $(this).html('<textarea class="form-control" style="height:80px;" >' + $(this).text() + '</textarea>'); 
            }
			
		});		
		$(this).parents("tr").find(".add, .edit").toggle();
		$(".add-new").attr("disabled", "disabled");
    });

	$(document).on("click", ".delete", function(){
        var dbid = $(this).parents("tr").attr('dbid');
        var succ = false;
        var questo = this;
        if(dbid === undefined){
            $(this).parents("tr").remove();
            $(".add-new").removeAttr("disabled");
        } else {
            if(confirm("Vuoi cancellare la nota con id "+dbid+" ?")){
                $.ajax({
                    url:"ajax.php?cancellanota_attivita=y",
                    type: "POST",
                    data: { 
                        cancella_nota: 'y',
                        id_nota: dbid
                    },
                    success:function(){
                        tata.success('Nota cancellata con successo', '');
                        succ = true;
                        $(questo).parents("tr").remove();
                        $(".add-new").removeAttr("disabled");
                    },
                    error: function(xhr,stato,errori){
                        tata.error('Errore nella cancellazione', xhr.status);
                    }
                });

            } else {
                
            }
        }
        /*
        if(succ == true){
            $(this).parents("tr").remove();
            $(".add-new").removeAttr("disabled");
            alert("eliminattoh");
        }

        $(this).parents("tr").remove();
		$(".add-new").removeAttr("disabled");
        */
    });
});

{/if}

/* Promemoria */

function add_promemoria() {
    var possible = "0123456789";
    var randomid = "";
    for( var i=0; i < 11; i++ ) {
        randomid += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    $("<tr id='tr_promemoria_"+randomid+"'><td><input type='hidden' name='promemoria_nuovo[]' value='1' />Inviami promemoria:</td><td><input type='text' name='action_minuti[]' value='10' /></td><td><select name='action_tipo_promemoria[]'><option value='MINUTE'>Minuti</option><option value='HOUR'>Ore</option><option value='DAY'>Giorni</option></select></td><td> prima <a href='javascript:void(0)' onclick='togli_riga("+randomid+");'><img src='../img/admin/delete.gif' alt='Cancella' title='Cancella' style='cursor:pointer' /></a></td></tr>").appendTo("#promemoriaTableBody");
}

function deletePromemoria(id, tipo_att, ora, conteggio)
{
    $.ajax({
        url:"ajax.php?deletePromemoria=y",
        type: "POST",
        data: { msg: id, tipo_att: tipo_att, ora: ora
        },
        success:function(){
            $("#pm_"+conteggio).hide();
            tata.success('Promemoria cancellato con successo', ' ');
        },
        error: function(xhr,stato,errori){
            tata.error('Errore nella cancellazione', xhr.status);
        }
    });
}

// Correggere: non funziona se cancello la prima riga prima dell'ultima
function togli_riga(id) {
    document.getElementById('tr_promemoria_'+id).innerHTML = "";
}

/* In carico a */

$(document).ready(function(){
    {if $todo["todo_view"]["cronologia"]["impez_selected"] != 0}
        {literal}document.getElementById('conoscenza_{/literal}{$todo["todo_view"]["cronologia"]["impez_selected"]}{literal}').disabled = true;{/literal}
    {/if}
});

function changeInCaricoAA()
{
    {foreach $todo['todo_view']['cronologia']['impiegati_eza'] as $impez}
    {literal}
        if (document.getElementById("in_carico_a_a").value == "{/literal}{$impez['id_employee']}{literal}") {
            document.getElementById('conoscenza_{/literal}{$impez["id_employee"]}{literal}').disabled = true;
            document.getElementById('conoscenza_{/literal}{$impez["id_employee"]}{literal}').checked = false;
        } 
        else { 
            document.getElementById('conoscenza_{/literal}{$impez["id_employee"]}{literal}').disabled = false; 
        }
    {/literal}
    {/foreach}
}

/* Link utili */

function addUtil_TR(event, data, formatted)
{
    tinyMCE.get('messaggioa').focus();
    var $body = $(tinymce.activeEditor.getBody());
    $body.prepend('<p>');
    $body.append('</p>');
    if(document.getElementById("utils_autocomplete_input_action").value != "-- Seleziona un link utile --") {
        $body.find("p:last").append('<a href="' + document.getElementById("utils_autocomplete_input_action").value + '">' + document.getElementById("utils_autocomplete_input_action").value + '</a><br /><br />');
    }
}

/* Precompilati */

function inserisciPrecompilato(msg)
{
    tinyMCE.get('messaggioa').focus();
    var $body = $(tinymce.activeEditor.getBody());
    $body.html('');
    $body.prepend('<p>');
    $body.append('</p>');
    if(msg != "-- Seleziona un messaggio --") {
        $body.find("p:last").append(msg);
    }
}

/* Anteprime file */

function handleFileSelect(files) 
{
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0; i < files.length; i++) {
        var f = files[i];
        var name = files[i].name;

        var reader = new FileReader();  
        reader.onload = function(e) {  
        // Render thumbnail.
        var span = document.createElement("span");
        span.innerHTML = ["<a href='",e.target.result,"' target='_blank'>",name,"</a> - "].join("");
        document.getElementById("list-action").insertBefore(span, null);
        };

    // Read in the image file as a data URL.
    reader.readAsDataURL(f);
    }
}

/* Conferma uscita dalla pagina se ci sono modifiche non salvate */

$(document).ready(function(){
						
    dataChanged = 0; // global variable flags unsaved changes      

    function bindForChange(){    
        $('input,checkbox,text,textarea,radio,select').bind('change',function(event) { dataChanged = 1 });
        $('#messaggiop').bind('input propertychange', function() { dataChanged = 1 });
        $(':reset,:submit').bind('click',function(event) { dataChanged = 0 });
    }

    function askConfirm(){  
        if (dataChanged){ 
            return "You have some unsaved changes. Press OK to continue without saving." 
        }
    }

    window.onbeforeunload = askConfirm;
    window.onload = bindForChange;
});

</script>