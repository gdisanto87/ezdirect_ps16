<script>

function handleFileSelect(files) {
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0; i < files.length; i++) {
        var f = files[i];
        var name = files[i].name;

        var reader = new FileReader();  
        reader.onload = function(e) {  
            // Render thumbnail.
            var span = document.createElement("span");
            span.innerHTML = ["<a href='",e.target.result,"' target='_blank'>",name,"</a> - "].join("");
            document.getElementById("list-tkt").insertBefore(span, null);
        };

        // Read in the image file as a data URL.
        reader.readAsDataURL(f);
    }
}

</script>