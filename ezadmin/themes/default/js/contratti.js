/* Funzioni per rinnovo contratto -> DISABILITATE */
function rinnova_contratto()
{
    document.getElementById("rif_fattura").value = "";
    document.getElementById("rif_ordine").value = "";
    document.getElementById("rif_noleggio").value = "";
    document.getElementById("cig").value = "";
    document.getElementById("cup").value = "";
    document.getElementById("univoco").value = "";
    document.getElementById("data_inizio").value = "";
    document.getElementById("data_fine").value = "";
    document.getElementById("cadenza").value = "";
    document.getElementById("competenza_dal").value = "";
    document.getElementById("competenza_al").value = "";
    document.getElementById("rinnova_contratto_ok").value = 1;
    document.getElementById("annulla_rinnova_contratto_button").style.display = "block";
    document.getElementById("rinnova_contratto_button").style.display = "none";
    document.getElementById("invii_controlli").style.display = "none";
}

function annulla_rinnova_contratto(url) 
{
    window.location.href = url;
}
/** */

// CORREGGERE; Serve per il form di ricerca e aggiunta prodotti
function addProductAssistenza(event, data, formatted)
{
    var productId = data[1];
    var productRef = data[4];
    var productName = data[5];
    var productPrice = data[23];
    productPrice = productPrice.replace(/\s\&euro\;/, "").replace(/\s\s*$/, "");
    
    //var productPriceP = (parseFloat(productPrice.replace(/\s/g, "").replace(",", ".")));
    
    productId = productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "");
    
    $("body").trigger("click");
    
    $("#product_autocomplete_input").trigger("click");
    $("#product_autocomplete_input_interno").trigger("click");
    $("#product_autocomplete_input").trigger("click");
    
    $("#product_autocomplete_input_interno").trigger("click");
    
    $("<tr id='tr_"+productId+"'><td>"+productRef+"</td><td><input type='text' name='prodotto_descrizione["+productId+"]' style='width:250px' value='"+productName+"' /></td><td><input type='text' id='quantita["+productId+"]' name='quantita["+productId+"]'  onkeyup='CalcTotContratto();' size='3' value='1' /></td><td><input type='text' value='"+productPrice+"' name='prezzo_prodotto["+productId+"]' onkeyup='CalcTotContratto();' class='importo' style='text-align:right' id='prezzo_prodotto["+productId+"]' onkeyup='CalcTotContratto();' size='7' /></td><td><input type='text' name='seriale_prodotto["+productId+"]' size='7' /></td><td><select name='indirizzo_prodotto["+productId+"]' style='width:200px'>'; foreach ($indirizzi as $del_a) { echo '<option value=''.$del_a['id_address'].''>'.$del_a['address1'].' - '.$del_a['city'].'</option>'; } echo '</select><input type='hidden' id='prodotto_id["+productId+"]' name='prodotto_id["+productId+"]' value='"+productId+"' /><input type='hidden' name='prodotto["+productId+"]' checked='checked' /></td><td><a style='cursor:pointer' onclick='CalcTotContratto(); delProduct30Contratto("+productId+")'><img src='../img/admin/delete.gif' alt='Cancella' title='Cancella' /></a></td></tr>").appendTo("#tableProductsBody");
    
    CalcTotContratto();

    var excludeIds = document.getElementById("excludeIds").value;
    excludeIds = excludeIds+productId+",";
    document.getElementById("excludeIds").value = excludeIds;
    $("#product_autocomplete_input").val("");
    
    $("#product_autocomplete_input").setOptions({
        extraParams: {excludeIds : document.getElementById("excludeIds").value}
    });	
    
    $("#product_autocomplete_input_interno").val("");
    
    $("#product_autocomplete_input_interno").setOptions({
        extraParams: {excludeIds : document.getElementById("excludeIds").value}
    });	
}

function calcolaRataContratto() 
{
    var prezzo_contratto = parseFloat(document.getElementById("totale_contratto").innerHTML.replace(",", "."));
    var periodicita = parseFloat(document.getElementById("periodicita").value.replace(",", "."));

    var rata = prezzo_contratto / periodicita;

    rata = rata.toFixed(2);
    rata = rata.replace(".", ",");

    document.getElementById("rata_contratto").innerHTML = rata;
}

// CORREGGERE: la quantità non conta ai fini del calcolo?
function CalcTotContratto() 
{
    var tot_varie = 0;
    
    var arrayImporti = document.getElementsByClassName("importo");
    
    for (var i = 0; i < arrayImporti.length; ++i) {
        var item = parseFloat(arrayImporti[i].value.replace(',','.'));  
        tot_varie += item;
    }
    
    var totale_contratto = tot_varie;
    
    document.getElementById("totale_importo").value = totale_contratto.toFixed(2);
    document.getElementById("totale_contratto").innerHTML = totale_contratto.toFixed(2).replace('.',',');
    calcolaRataContratto();
}

function verifica_contabilita(cb)
{
    var val = document.getElementById(cb).checked;

    switch(cb){
        case "pagato":
            if(val){
                document.getElementById("da_pagare").checked = false;
            }else{
                document.getElementById("da_pagare").checked = true;
            }
        break;

        case "da_pagare":
            if(val){
                document.getElementById("pagato").checked = false;
            }else{
                document.getElementById("pagato").checked = true;
            }
        break;

        case "fatturato":
            if(val){
                document.getElementById("da_fatturare").checked = false;
            }else{
                document.getElementById("da_fatturare").checked = true;
            }
        break;

        case "da_fatturare":
            if(val){
                document.getElementById("fatturato").checked = false;
            }else{
                document.getElementById("fatturato").checked = true;
            }
        break;
    }
}

function checkFormContratto() 
{
    var check = document.getElementById("id_contratto").value; 
    if(check == "") {
        alert("Il campo N. contratto è obbligatorio per generare il contratto");
        return false;
    }
    else {
        return true;
    }
}

/* Funzioni per cancellare un prodotto dal contratto */

// DISABILITATA
function togliImportoContratto(id_product)
{
    /*
    var quantity = document.getElementById('quantita['+id_product+']').value;
    var numPrice = document.getElementById('prezzo_prodotto['+id_product+']').value;
    var productPriceP = parseFloat(numPrice.replace(/\s/g, "").replace(",", "."));
    var numimporto = document.getElementById('prezzo_contratto').value;
    var importo = parseFloat(numimporto.replace(/\s/g, "").replace(",", "."));
    
    importo = importo - (productPriceP*quantity);
    
    importo = importo.toFixed(2);
    importo = importo.replace(".",",");

    document.getElementById('prezzo_contratto').value=importo;
    */
}

function deleteRowContratto(rowid)  
{   
    var row = document.getElementById(rowid);
    row.parentNode.removeChild(row);
}

function delProduct30Contratto(id)
{
    deleteRowContratto('tr_'+id);
}

/** */