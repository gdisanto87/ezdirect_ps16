function add_riga()
{
	var parent = document.getElementById("altri-fornitori");
	
	// Figlio di id="altri-fornitori"
	var row = document.createElement('div');
	row.className = "row";
	
	// Figlio di div row
	var form_group = document.createElement('div');
	form_group.className = "form-group col-md-2";

	// Figli di div form-group
	var label = document.createElement('label');
	label.innerHTML = "Altri fornitori";
	var elem = document.querySelector('#other_suppliers_cpy');
	var select = elem.cloneNode(true);
	// end

	form_group.appendChild(label);
	form_group.appendChild(select);

	row.appendChild(form_group);

	parent.appendChild(row);
}

function rem_riga()
{
	var parent = document.getElementById("altri-fornitori");
	parent.removeChild(parent.lastElementChild);
}