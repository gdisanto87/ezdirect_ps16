/* Funzioni per Strumenti -> Messaggi Precompilati */

window.onload = function() {
    var el = document.createElement("div");

    el.innerHTML = '<div id="msg-bozza" style="display: none;"><div class="alert alert-warning">Il messaggio sarà salvato come bozza</div></div>';
    //el.style.display = "none";
    var div = document.getElementById("fieldset_0");
    insertAfter(div, el);
};

function insertAfter(referenceNode, newNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

// Funziona, ma il metodo è da correggere; il msg va spostato in un posto più visibile; non deve essere esclusivamente per "active"
function displayDraftWarning(vari) {
    if(vari == "active_off"){
        if(!document.getElementById("msg-bozza")){
            document.body.innerHTML += '<div id="msg-bozza"><div class="alert alert-warning">Il messaggio sarà salvato come bozza</div></div>';
        } else {
            document.getElementById("msg-bozza").style.display = "block";
        }
    } else {
        if (vari == "active_on"){
            document.getElementById("msg-bozza").style.display = "none";
        }
    }
}

/* Fine funzioni per Strumenti -> Messaggi Precompilati */
