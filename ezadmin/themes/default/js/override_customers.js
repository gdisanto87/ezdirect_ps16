/* Vai e chiudi azione corrente */
function closethis(tipo, riferimento, link)
{
    $.ajax({
        url:'ajax.php?closethis=y',
        type: 'POST',
        data: { 
            tipo: tipo, 
            id: riferimento
        },
        success:function(resp){
        if(resp == 'ok')
        {
            alert('Azione chiusa con successo');
            window.location.href = link;
        }
        else
            alert('Errore durante la chiusura dell\'azione');
        },
        error: function(xhr,stato,errori){
            alert('ERRORE: '+xhr.status);
        }
    });
}

// Correggere: aggiungere funzioni per tooltip messaggio etc...