function calcolaPeriodoTutti()
{
    var periodo_tutti = parseFloat(document.getElementById("periodo_tutti").value);
    
    var arrayIds = document.getElementsByClassName("id_productClass");
    for (var i = 0; i < arrayIds.length; ++i) {
        var id = parseFloat(arrayIds[i].value);  
        document.getElementById("periodo_"+id).value = periodo_tutti;
        calcolaPeriodo(id);
    }          
}

function calcolaPeriodo(id)
{
    var ordine = parseFloat(document.getElementById("ordine_per_periodo_"+id).value);
    var periodo = parseFloat(document.getElementById("periodo_"+id).value);
    var netto = parseFloat(document.getElementById("netto_"+id).value);
    var ordinato = parseFloat(document.getElementById("ordinato_"+id).value);
    if(ordine > 3)
        ordine = Math.round(ordine);
    
    $.ajax({
        url:"ajax.php?aggiornaPeriodoForecast=y",
        type: "POST",
        data: { id_product: id,
        periodo: periodo
        },
        success:function(r){
            // alert(r);
        },
        error: function(xhr,stato,errori){
            alert("Errore durante l\'operazione:"+xhr.status);
        }
    });
    
    var calcolo = ordine * periodo;
    var ord_tot = (netto + ordinato) - calcolo;
    
    if(calcolo > 3)
        calcolo = calcolo.toFixed(0);
    else {
        calcolo = calcolo.toFixed(2);
        calcolo = calcolo.replace(".",",");
    }
    
    if(ord_tot > 0)
        ord_tot = 0;
    
    var ord_tot = 0-ord_tot;
    
    if(ord_tot > 3)
        ord_tot = ord_tot.toFixed(0);
    else {
        ord_tot = ord_tot.toFixed(2);
        ord_tot = ord_tot.replace(".",",");
    }
    
    document.getElementById("calcolo_"+id).innerHTML = calcolo;
    document.getElementById("ord_tot_"+id).innerHTML = ord_tot;
}