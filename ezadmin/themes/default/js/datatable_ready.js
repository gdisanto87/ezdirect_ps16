$(document).ready(function () { 
    $('.tabella_datatable').DataTable({
        "language": {
            "lengthMenu": "Record per pagina: _MENU_",
        },
    });

    // Le ultime attività in view employee vengono ordinate per date_add DESC
    $('#tabella_employees_ra').DataTable({
        "order": [[ 10, "desc" ]],
        "language": {
            "lengthMenu": "Record per pagina: _MENU_",
        },
    });

    /* PLANNER */

    // Ultime attività in view planner
    $('#table-ultime-att').DataTable({
        "paging": false,
        "searching": false,
        "info": false,
        "scrollY": "300px",
        "scrollCollapse": true,
        "language": {
            "lengthMenu": "Record per pagina: _MENU_",
        },
    });

    // Vedi https://www.datatables.net/blog/2014-10-02 per espansione riga e testala
    /*$('#table-ultime-att tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
     
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );*/

    // Attività dell'employee loggato in view planner
    $('#tabella_mie_attivita').DataTable({
        // "processing": true, // icona di caricamento; usare solo con tabelle con tanti record
        initComplete: function () { // Inizializza i filtri select - option -> correggere: meglio metterli nello stesso clone di thead dei filtri senza select
            this.api().columns([1,3,4,5,7,8]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        },
        "language": {
            "lengthMenu": "Record per pagina: _MENU_",
        },
    });

    // TEST Filtri! Correggere, non funzionano e dovrei usarli con thead senza sdoppiare le freccette di ordinamento
    $('#tabella_mie_attivita tfoot tr').clone(true).appendTo('#tabella_mie_attivita tfoot').show();
    $('#tabella_mie_attivita tfoot tr:eq(1) th.filtro').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    /* // bisogna includere https://cdn.datatables.net/fixedheader/3.1.8/js/dataTables.fixedHeader.min.js
    var table = $('#tabella_mie_attivita').DataTable( {
        orderCellsTop: true,
        fixedHeader: true
    } );*/

    /* FINE PLANNER */

    /* // Con esportazione in excel semplice
    $('.tabella_datatable').DataTable( {
        dom: 'Bfrtip',
        buttons: [ {
            extend: 'excelHtml5',
            customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 
                //$('row c[r^="C"]', sheet).attr( 's', '2' );
                //$('row c[r^="B"]', sheet).attr( 'a', '4' );
            },
            exportOptions: {
                columns: [ 0, 1, 2, 5 ]
            }
        } ]
    } );
    */
});


