// Correggere: Apre la modifica dell'allegato tramite click con il tasto destro del mouse (?)
$("#selectAttachment1").bind("contextmenu",function(e){
	// Decommentare quello giusto dopo test; correggere tokenAttachments
	window.open("https:/46.101.235.11/ezadmin/index.php?controller=AdminAttachments&id_attachment="+this.options[this.selectedIndex].value+"&updateattachment","_blank");
	// window.open("https://www.ezdirect.it/ezadmin/index.php?tab=AdminAttachments&id_attachment="+this.options[this.selectedIndex].value+"&updateattachment&token='.$tokenAttachments.'","_blank");
	return false;
}); 

// Apre l'allegato tramite doppio click sul nome
document.getElementById("selectAttachment1").ondblclick = function(){
	// testare con server prova! Il file attachment.php non esiste più; aggiungerlo o usare override/controllers/front/AttachmentController.php
	window.open("http://46.101.235.11/attachment.php?id_attachment="+this.options[this.selectedIndex].value,"_blank");
	//window.open("http://www.ezdirect.it/attachment.php?id_attachment="+this.options[this.selectedIndex].value,"_blank");
};

// Elimina l'allegato dal database
function togli_allegati(id) {
	$.ajax({
		url:"ajax.php?togli_allegati=y",
		type: "POST",
		data: { 
			togli_allegati: 'y',
			id_allegati: id
		},
		success:function(resp){
		},
		error: function(xhr,stato,errori){
			alert("Errore nella cancellazione:"+xhr.status);
		}
	});
}