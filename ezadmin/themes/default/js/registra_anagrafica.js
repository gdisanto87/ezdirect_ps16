// TUTTO DA CORREGGERE! E' una copia di una parte di themes/ezdirect-new/js/tools.js della 1.4; si può eliminare una volta importato quel file

function countryChange()
{
	var country_select = document.getElementById('id_country');
	
	var country = country_select.options[country_select.selectedIndex].value;
	if(country == 10)
	{
		
		$('.ee_address').hide();
		$('.it_address').show();
		$('#city').attr('name', 'city1');
		$('#postcode').attr('name', 'postcode1');
		$('#id_state').attr('name', 'id_state1');
		
		$('#citta').attr('name', 'city');
		$('#cap').attr('name', 'postcode');
		$('#provincia_hidden').attr('name', 'id_state');
		
	}
	else
	{
		$('.it_address').hide();
		$('.ee_address').show();
		
		$('#city').attr('name', 'city');
		$('#postcode').attr('name', 'postcode');
		$('#id_state').attr('name', 'id_state');
		
		$('#citta').attr('name', 'city1');
		$('#cap').attr('name', 'postcode1');
		$('#provincia_hidden').attr('name', 'id_state1');
	}
}


function cercaCitta() {
	var this_cap = $('#cap').val();
	$.ajax({
	  // url:"https://www.ezdirect.it/cap.php?cercacitta=y",
	  url:"cap.php?cercacitta=y",
      type: "POST",
	  data: { 
	  cap: this_cap
	  },
	  success:function(r){
		 if(r != '')
		 {
			$( "#citta" ).html( r );
			console.log(r);
			cercaProvincia();
		 }	
	  },
	  error: function(xhr,stato,errori){
	 alert("Errore:"+xhr.status);
	  }
	});

}

function cercaProvincia() {
	var this_citta = $('#citta option:selected').attr('rel');
	$.ajax({
	  // url:"https://www.ezdirect.it/cap.php?cercaprovincia=y",
	  url:"cap.php?cercaprovincia=y",
      type: "POST",
	  data: { 
	  citta: this_citta
	  },
	  success:function(r){
		  if(r != '')
		  {
			  var prv = r.split(':::');
			$( "#provincia" ).val(prv[0]);
			$( "#provincia_hidden" ).val(prv[1]);
			
		}
	  },
	  error: function(xhr,stato,errori){
	 alert("Errore:"+xhr.status);
	  }
	});

}


function countryChange2()
{
	var country_select = document.getElementById('id_country2');
	var country = country_select.options[country_select.selectedIndex].value;
	if(country == 10)
	{
		$('.ee_address').hide();
		$('.it_address').show();
		
		$('#city2').attr('name', 'city2');
		$('#postcode2').attr('name', 'postcode2');
		$('#id_state2').attr('name', 'id_state2');
		
		$('#citta2').attr('name', 'city');
		$('#cap2').attr('name', 'postcode');
		$('#provincia_hidden2').attr('name', 'id_state');
		
	}
	else
	{
		$('.it_address').hide();
		$('.ee_address').show();
		
		$('#city2').attr('name', 'city');
		$('#postcode2').attr('name', 'postcode');
		$('#id_state2').attr('name', 'id_state');
		
		$('#citta2').attr('name', 'city2');
		$('#cap2').attr('name', 'postcode2');
		$('#provincia_hidden2').attr('name', 'id_state2');
	}
}

function cercaCitta2() {
	var this_cap = $('#cap2').val();
	$.ajax({
	  // url:"https://www.ezdirect.it/cap.php?cercacitta=y",
	  url:"cap.php?cercacitta=y",
      type: "POST",
	  data: { 
	  cap: this_cap
	  },
	  success:function(r){
		 if(r != '')
		 {
			$( "#citta2" ).html( r );
			console.log(r);
			cercaProvincia2();
		 }	
	  },
	  error: function(xhr,stato,errori){
	 alert("Errore:"+xhr.status);
	  }
	});

}

function cercaProvincia2() {
	var this_citta = $('#citta2 option:selected').attr('rel');
	$.ajax({
	  // url:"https://www.ezdirect.it/cap.php?cercaprovincia=y",
	  url:"cap.php?cercaprovincia=y",
      type: "POST",
	  data: { 
	  citta: this_citta
	  },
	  success:function(r){
		  if(r != '')
		  {
			  var prv = r.split(':::');
			$( "#provincia2" ).val(prv[0]);
			$( "#provincia_hidden2" ).val(prv[1]);
			
		}
	  },
	  error: function(xhr,stato,errori){
	 alert("Errore:"+xhr.status);
	  }
	});

}


function countryChange_invoice()
{
	var country_select = document.getElementById('id_country_invoice');
	var country = country_select.options[country_select.selectedIndex].value;
	if(country == 10)
	{
		$('.ee_address_invoice').hide();
		$('.it_address_invoice').show();
		
		$('#city_invoice').attr('name', 'city1');
		$('#postcode_invoice').attr('name', 'postcode1');
		$('#id_state_invoice').attr('name', 'id_state1');
		
		$('#citta_invoice').attr('name', 'city_invoice');
		$('#cap_invoice').attr('name', 'postcode_invoice');
		$('#provincia_hidden_invoice').attr('name', 'id_state_invoice');
		
	}
	else
	{
		$('.it_address_invoice').hide();
		$('.ee_address_invoice').show();
		
		$('#city_invoice').attr('name', 'city_invoice');
		$('#postcode_invoice').attr('name', 'postcode_invoice');
		$('#id_state_invoice').attr('name', 'id_state_invoice');
		
		$('#citta_invoice').attr('name', 'city1');
		$('#cap_invoice').attr('name', 'postcode1');
		$('#provincia_hidden_invoice').attr('name', 'id_state1');
	}
}


function cercaCitta_invoice() {
	var this_cap = $('#cap_invoice').val();
	$.ajax({
	  // url:"https://www.ezdirect.it/cap.php?cercacitta=y",
	  url:"cap.php?cercacitta=y",
      type: "POST",
	  data: { 
	  cap: this_cap
	  },
	  success:function(r){
		 if(r != '')
		 {
			$( "#citta_invoice" ).html( r );
			console.log(r);
			cercaProvincia_invoice();
		 }	
	  },
	  error: function(xhr,stato,errori){
	 alert("Errore:"+xhr.status);
	  }
	});

}

function cercaProvincia_invoice() {
	var this_citta = $('#citta_invoice option:selected').attr('rel');
	$.ajax({
	  // url:"https://www.ezdirect.it/cap.php?cercaprovincia=y",
	  url:"cap.php?cercaprovincia=y",
      type: "POST",
	  data: { 
	  citta: this_citta
	  },
	  success:function(r){
		  if(r != '')
		  {
			  var prv = r.split(':::');
			$( "#provincia_invoice" ).val(prv[0]);
			$( "#provincia_hidden_invoice" ).val(prv[1]);
			
		}
	  },
	  error: function(xhr,stato,errori){
	 alert("Errore:"+xhr.status);
	  }
	});

}