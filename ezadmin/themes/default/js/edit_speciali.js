$(document).ready(function(){

    // Salvataggio riga -> correggere - fare ajax solo 1 volta per riga e non per ogni input
    $(document).on("click", ".add", function(){
		var empty = false;
        var dbid = $(this).parents("tr").find("td:eq(1)").html(); // correggere: va bene solo se c'è più di un elemento nella list, sennò diventa eq(0)
		var input = $(this).parents("tr").find('input[type="text"]');

        input.each(function(){
			if(!$(this).val()){
				$(this).addClass("error");
				empty = true;
			} else{
                $(this).removeClass("error");
            }
		});
		$(this).parents("tr").find(".error").first().focus();

		if(!empty){
            if(dbid === undefined){
                // Aggiungi nuova riga -> disattivato
            } 
            else {
                var valori = [];
                input.each(function(){
                    var len = valori.push($(this).val());
                });
                // correggere: funziona solo se non viene cambiato l'ordine delle colonne; altrimenti usare id degli input
                $.ajax({
                    url:"ajax.php?modifica_speciale=y",
                    type: "POST",
                    data: { 
                        id_speciale: dbid,
                        prezzo_speciale: valori[0],
                        partenza: valori[1],
                        fine: valori[2]
                    },
                    success:function(){
                        tata.success('Prezzo modificato con successo', '');
                        succ = true;
                    },
                    error: function(xhr,stato,errori){
                        tata.error('Errore nella modifica', xhr.status);
                    }
                });
            }
		}

        $(this).parents("tr").find("td").each(function(){
            if($(this).hasClass('mod')){
                // $(this).children("input").prop("readonly", true); // non funziona, ma se stampo in console il valore è giusto
                $(this).children("b").children("input").prop("readonly", true); // non funziona, ma se stampo in console il valore è giusto

                if($(this).hasClass('mod_data_p') || $(this).hasClass('mod_data_f')){
                    $(this).parent("td").html('<span>' + $(this).val() + '</span>'); // non funziona
                }
            }
		});
		$(this).parent("tr").find("a.edit, a.add").toggle();
    });

    // Gestione tasto Invio
    $(function() {
        $("input").keypress(function(event) {
            //var $self = $(this);
            //var comm = $self.val();

            var keycode = (event.keycode ? event.keycode : event.which);
            if (keycode == 13) {
                //var comm = $self.val();
                //alert(comm);
                event.stopPropagation();
            }
        });
    });

    // Attivazione modifica riga
	$(document).on("click", ".edit", function(){
        var dbid = $(this).parents("tr").find("td:eq(1)").html(); // correggere: va bene solo se c'è più di un elemento nella list, sennò diventa eq(0)

        $(this).parent("tr").find("td").each(function(){
            if($(this).hasClass('mod')){
                // $(this).children("input").prop("readonly", false);
                $(this).children("b").children("input").prop("readonly", false);

                if($(this).hasClass('mod_data_p')){
                    var testo = $(this).find('span').html();
                    testo = String(testo);
                    testo = testo.substring(0,10);
                    $(this).html('<input type="text" class="form-control" id="data_partenza_'+dbid+'" name="data_partenza_'+dbid+'" value="'+ testo +'">');
                }

                if($(this).hasClass('mod_data_f')){
                    var testo = $(this).find('span').html();
                    testo = String(testo);
                    testo = testo.substring(0,10);
                    $(this).html('<input type="text" class="form-control" id="data_fine_'+dbid+'" name="data_fine_'+dbid+'" value="'+ testo +'">');
                }
            }
		});
		$(this).parent("tr").find("a.edit, a.add").toggle();
    });
});