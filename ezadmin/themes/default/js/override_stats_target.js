function aggiornaTarget(tipo,anno,riga) 
{
    venduto = document.getElementById(tipo+"-"+riga+"-"+anno+"-vend").innerHTML;
    venduto = parseFloat(venduto.replace(/\s/g, "").replace(/\./g,"").replace(",", "."));
    
    target = document.getElementById(tipo+"-"+riga+"-"+anno+"-target").value;
    target = parseFloat(target.replace(/\s/g, "").replace(/\./g,"").replace(",", "."));
    
    var qtot = 
    parseFloat((document.getElementById(tipo+"-q1-"+anno+"-target").value).replace(/\s/g, "").replace(/\./g,"").replace(",", ".")) + 
    parseFloat((document.getElementById(tipo+"-q2-"+anno+"-target").value).replace(/\s/g, "").replace(/\./g,"").replace(",", ".")) + 
    parseFloat((document.getElementById(tipo+"-q3-"+anno+"-target").value).replace(/\s/g, "").replace(/\./g,"").replace(",", ".")) + 
    parseFloat((document.getElementById(tipo+"-q4-"+anno+"-target").value).replace(/\s/g, "").replace(/\./g,"").replace(",", ".")) 
    ;
    
    var qtotfloat = qtot;
    
    qtot = qtot.toFixed(2);
    qtot = qtot.toString();				
    qtot = qtot.replace(".",",");
    
    document.getElementById(tipo+"-totale-"+anno+"-target").value = qtot;
    
    risultato = venduto*100/target;
    risultato = risultato.toFixed(2);
    risultato = risultato.toString();				
    risultato = risultato.replace(".",",");
    
    var vendutotot = document.getElementById(tipo+"-tot-"+anno+"-vend").innerHTML;
    vendutotot = vendutotot.replace(/<\/?[^>]+(>|$)/g, "");
    vendutotot = parseFloat(vendutotot.replace(/\s/g, "").replace(/\./g,"").replace(",", "."));
    
    risultatotot = vendutotot*100/qtotfloat;
    risultatotot = risultatotot.toFixed(2);
    risultatotot = risultatotot.toString();				
    risultatotot = risultatotot.replace(".",",");
    
    document.getElementById(tipo+"-"+riga+"-"+anno+"-perc").innerHTML = risultato;
    document.getElementById(tipo+"-tot-"+anno+"-perc").innerHTML = risultatotot;
}

// Correggere le variabili smarty
function saveTrgt(tipo)
{
    $("#save_anno_"+tipo).html("<img src='../img/loader.gif' />").show();
    var descrizione = $("#"+tipo+"-descrizione").val();
    var anno = $("#"+tipo+"-anno").val();
    var totale = $("#"+tipo+"-anno-{$anno_di_riferimento}-target").val().replace(/\s/g, "").replace(/\./g,"").replace(",", ".");
    var q1 = $("#"+tipo+"-q1-{$anno_di_riferimento}-target").val().replace(/\s/g, "").replace(/\./g,"").replace(",", ".");
    var q2 = $("#"+tipo+"-q2-{$anno_di_riferimento}-target").val().replace(/\s/g, "").replace(/\./g,"").replace(",", ".");
    var q3 = $("#"+tipo+"-q3-{$anno_di_riferimento}-target").val().replace(/\s/g, "").replace(/\./g,"").replace(",", ".");
    var q4 = $("#"+tipo+"-q4-{$anno_di_riferimento}-target").val().replace(/\s/g, "").replace(/\./g,"").replace(",", ".");
    $.ajax({
        type: "POST", 
        url:"ajax.php", 
        async:false, 
        data: {submitTrgt:1,anno:anno,descrizione:descrizione,totale:totale,q1:q1,q2:q2,q3:q3,q4:q4}, 
        success:function (r) {
        
        if (r == "ok")
        {
            $("#save_anno_"+tipo).html("<b style='color:green'>Configurazione salvata</b>").fadeIn(400);
            /* window.location.href = "http://www.ezdirect.it/{$currentIndex}&token={$this->token}";	*/
        }
        else if (r == "error:validation")
            $("#save_anno_"+tipo).html("<b style='color:red'>Errore: configurazione non valida</b>").fadeIn(400);
        else if (r == "error:update")
            $("#save_anno_"+tipo).html("<b style='color:red'>Errore: impossibile salvare configurazione</b>").fadeIn(400);
        $("#save_anno_"+tipo).fadeOut(3000);
        }
    });
    /* window.location.href = "http://www.ezdirect.it/{$currentIndex}&token={$this->token}"; */	
}

function delTrgt(tipo)
{	
    $.post("ajax.php", {deleteTrgt:1,descrizione:tipo}, function (r) {
        
        if (r == "ok")
        {
            document.getElementById(tipo).innerHTML = "";
        }
        else
        {
            alert("ERROR");
        }
    });
}