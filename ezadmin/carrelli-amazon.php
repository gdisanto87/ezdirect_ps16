<?php

	include_once("functions.php");

	if (!defined('_PS_ADMIN_DIR_')) {
		define('_PS_ADMIN_DIR_', getcwd());
	}
	require_once(_PS_ADMIN_DIR_.'/../config/config.inc.php');
	require_once(_PS_ADMIN_DIR_.'/init.php');

	ini_set("memory_limit","892M");
	ini_set("display_errors","on");
	set_time_limit(3600);
	
	require_once 'esportazione-catalogo/Classes/PHPExcel.php';

	$objPHPExcel = new PHPExcel();

	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', 'eSolver')
	->setCellValue('B1', 'SKU')
	->setCellValue('C1', 'Nome prodotto')
	->setCellValue('D1', 'Costruttore')
	->setCellValue('E1', 'Fornitore')
	->setCellValue('F1', 'Qt. carrello')
	->setCellValue('G1', 'Id carrello')
	->setCellValue('H1', 'Data carrello')
	->setCellValue('I1', 'Qt. Ez')
	->setCellValue('J1', 'Qt. Esprinet')
	->setCellValue('K1', 'Qt. Allnet')
	->setCellValue('L1', 'Qt. Itancia')
	->setCellValue('M1', 'Qt. Intracom')
	->setCellValue('N1', 'Qt. Asit')
	->setCellValue('O1', 'Ord. a forn.')
	;
	
	$k = 2;
	
	$dataTable = Db::getInstance()->executeS('
		SELECT cp.id_cart as carrello, c.date_add as data_carrello, reference, supplier_reference, cp.quantity as quantita_carrello, sku_amazon, fnsku, pl.name, asin, stock_quantity, ordinato_quantity, impegnato_quantity, impegnato_amazon, scorta_minima_amazon, esprinet_quantity, supplier_quantity, itancia_quantity, intracom_quantity, asit_quantity, m.name manufacturer, s.name supplier 
		FROM '._DB_PREFIX_.'product p 
			JOIN (SELECT * FROM '._DB_PREFIX_.'product_lang WHERE id_lang = 5) pl ON p.id_product = pl.id_product 
			JOIN '._DB_PREFIX_.'manufacturer m ON m.id_manufacturer = p.id_manufacturer 
			JOIN '._DB_PREFIX_.'supplier s ON p.id_supplier = s.id_supplier 
			JOIN '._DB_PREFIX_.'cart_product cp ON cp.id_product = p.id_product 
			JOIN '._DB_PREFIX_.'cart c ON cp.id_cart = c.id_cart 
		WHERE c.name LIKE "%AMAZON%" 
			AND c.date_add > "2020-01-01" 
			AND c.id_cart NOT IN (SELECT id_cart FROM '._DB_PREFIX_.'orders) 
		ORDER BY c.id_cart DESC
	');
		
	foreach ($dataTable as $row) {
	
		if($row['ordinato_quantity'] - $row['quantita_carrello'] <= 0)
		{
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue("A$k", $row['reference'])
				->setCellValue("B$k", $row['supplier_reference'])
				->setCellValue("C$k", $row['name'])
				->setCellValue("D$k", $row['manufacturer'])
				->setCellValue("E$k", $row['supplier'])
				->setCellValue("F$k", $row['quantita_carrello'])
				->setCellValue("G$k", $row['carrello'])
				->setCellValue("H$k", date("d-m-Y",strtotime($row['data_carrello'])))
				->setCellValue("I$k", $row['stock_quantity'])
				->setCellValue("J$k", $row['esprinet_quantity'])
				->setCellValue("K$k", $row['supplier_quantity'])
				->setCellValue("L$k", $row['itancia_quantity'])
				->setCellValue("M$k", $row['intracom_quantity'])
				->setCellValue("N$k", $row['asit_quantity'])
				->setCellValue("O$k", $row['ordinato_quantity'])
			;

			$k++;
		}
	
	}
	
	$objPHPExcel->getActiveSheet()->setTitle('Prodotti da inviare ad Amazon');

	$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
	$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFill()->getStartColor()->setRGB('FFFF00');	

	$objPHPExcel->getActiveSheet()->getStyle("A1:O$k")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true);

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
	$objPHPExcel->getActiveSheet()->freezePane('A2');
	$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
	$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
	PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

	$data = date("YmdHis");
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->setPreCalculateFormulas(false);
	$objWriter->setIncludeCharts(true);
	$objWriter->save(str_replace(".php", ".xls", "esportazione-catalogo/catalogo_xls/carrelli-amazon.php"));

	// Correggere: test sul server 46.101.235.11
	header("Location: http://46.101.235.11/ezadmin/esportazione-catalogo/catalogo_xls/carrelli-amazon.xls");
	//header("Location: http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/carrelli-amazon.xls");

?>
