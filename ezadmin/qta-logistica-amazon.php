<?php

	include_once("functions.php");
	
	if (!defined('_PS_ADMIN_DIR_')) {
		define('_PS_ADMIN_DIR_', getcwd());
	}
	require_once(_PS_ADMIN_DIR_.'/../config/config.inc.php');
	require_once(_PS_ADMIN_DIR_.'/init.php');

	ini_set("memory_limit","892M");
	ini_set("display_errors","on");
	set_time_limit(3600);
	
	require_once 'esportazione-catalogo/Classes/PHPExcel.php';
	
	$objPHPExcel = new PHPExcel();

	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', 'eSolver')
	->setCellValue('B1', 'ASIN')
	->setCellValue('C1', 'FNSKU')
	->setCellValue('D1', 'Qt. Log. Amazon')
	->setCellValue('E1', 'Qt. Mag. 30 eSolver');
	
	$k = 2;
	
	$dataTable = Db::getInstance()->executeS('
		SELECT reference, impegnato_amazon, amazon_quantity, asin, fnsku 
		FROM '._DB_PREFIX_.'product 
		WHERE amazon != "" 
			AND (impegnato_amazon > 0 OR amazon_quantity > 0) 
		ORDER BY reference ASC
	');
		
	foreach ($dataTable as $row) 
	{
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue("A$k", $row['reference'])
			->setCellValue("B$k", $row['asin'])
			->setCellValue("C$k", $row['fnsku'])
			->setCellValue("D$k", $row['impegnato_amazon'])
			->setCellValue("E$k", $row['amazon_quantity'])
		;
		
		$k++;
	}

	$objPHPExcel->getActiveSheet()->setTitle('Prodotti Logistica Amazon');

	$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
	$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->getStartColor()->setRGB('FFFF00');	

	$objPHPExcel->getActiveSheet()->getStyle("A1:E$k")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFont()->setBold(true);

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);

	$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
	$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
	PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

	$data = date("Ymd");

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->setPreCalculateFormulas(false);
	$objWriter->setIncludeCharts(true);
	$objWriter->save(str_replace(".php", ".xls", "esportazione-catalogo/catalogo_xls/prodotti-logistica-amazon.php"));
	
	// CORREGGERE: Location di prova sul server 46.101.235.11
	header("Location: http://46.101.235.11/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-logistica-amazon.xls");
	//header("Location: http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-logistica-amazon.xls");

?>
