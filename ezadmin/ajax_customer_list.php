<?php

if (!defined('_PS_ADMIN_DIR_')) {
    define('_PS_ADMIN_DIR_', getcwd());
}
define('PS_ADMIN_DIR', _PS_ADMIN_DIR_); // Retro-compatibility

include(PS_ADMIN_DIR.'/../config/config.inc.php');
/* Getting cookie or logout */
require_once(dirname(__FILE__).'/init.php');

$query = Tools::getValue('q', false);
if (!$query OR $query == '' OR strlen($query) < 1)
	die();

/*
 * In the SQL request the "q" param is used entirely to match result in database.
 * In this way if string:"(ref : #ref_pattern#)" is displayed on the return list,
 * they are no return values just because string:"(ref : #ref_pattern#)"
 * is not write in the name field of the product.
 * So the ref pattern will be cut for the search request.
 */
if ($pos = strpos($query, ' (ref:'))
	$query = substr($query, 0, $pos);

$excludeIds = Tools::getValue('excludeIds', false);
if ($excludeIds && $excludeIds != 'NaN')
	$excludeIds = implode(',', array_map('intval', explode(',', $excludeIds)));
else
	$excludeIds = '';

// Excluding downloadable products from packs because download from pack is not supported
$excludeVirtuals = (bool)Tools::getValue('excludeVirtuals', false);

$items = Db::getInstance()->executeS('
	SELECT id_customer, company, firstname, lastname, vat_number, tax_code 
	FROM '._DB_PREFIX_.'customer 
	WHERE active = 1 AND 
		(company LIKE \'%'.pSQL($query).'%\' OR lastname LIKE \'%'.pSQL($query).'%\' OR tax_code LIKE \'%'.pSQL($query).'%\' OR id_customer LIKE \'%'.pSQL($query).'%\' OR vat_number LIKE \'%'.pSQL($query).'%\' OR firstname LIKE \'%'.pSQL($query).'%\')
');

foreach ($items AS $item) {
	if(strlen(trim($item['company']) > 25)) {
		$company = substr($item['company'],0,25)."... ";
	}
	else {
		$company = $item['company'];
	}
	echo $item['id_customer']." | ".$company." | ".$item['firstname']." | ".$item['lastname']." | ".$item['vat_number']." | ".$item['tax_code']."\n";
}
