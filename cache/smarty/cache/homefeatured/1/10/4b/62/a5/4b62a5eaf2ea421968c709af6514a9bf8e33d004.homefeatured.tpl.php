<?php /*%%SmartyHeaderCode:4023428506193923c4b68c6-96554014%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4b62a5eaf2ea421968c709af6514a9bf8e33d004' => 
    array (
      0 => '/var/www/html/themes/ez20/modules/homefeatured/homefeatured.tpl',
      1 => 1637749975,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4023428506193923c4b68c6-96554014',
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61ae244314ae49_28649270',
  'has_nocache_code' => false,
  'cache_lifetime' => 31536000,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61ae244314ae49_28649270')) {function content_61ae244314ae49_28649270($_smarty_tpl) {?></div>
<div id="home_banners">
	<script type="text/javascript" src="http://46.101.235.11/themes/ez20/js/unslider.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$(function() {
			$('.banner').unslider({
				speed: 500,               //  The speed to animate each slide (in milliseconds)
				delay: 5000,              //  The delay between slide animations (in milliseconds)
				keys: true,               //  Enable keyboard (left, right) arrow shortcuts
				dots: true,               //  Display dot navigation
			});
		});
	});
	</script>
	
	


	
</div>
<div style="clear:both"></div>




<div class="center_column" style="width: 100%; ">

	<div class="container" >

		<h4 class='homepage' style="width:100%; margin: 10px auto; height: fit-content;">Le migliori soluzioni per comunicare</h4>

		<div id="categories-home">

			<div class="row" >

				<div class="col-12 col-sm-6 col-lg-4">
				
					<div class='categories-home-in' style="overflow-x:auto;">
						
						<table style='width:100%; height:100%; table-layout:fixed;'>
							<tr>
								<td style="width:250px;">
									<h3><a href="http://46.101.235.11/index.php?id_category=23&amp;controller=category">Centralini telefonici</a></h3>
								</td>

							</tr>

							<tr style="display: block;">
								<td style='width:50%; vertical-align:top  !important'>
									
									<ul style="margin-left:18px;">
									<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo_linee-analogica_voip_isdn/tipo_di_derivati_int_collegabili-analogici_voip-analogici_digitali_voip/supporta_linee_voip_sip-si-si_opzionale">VoIP</a></li>
									<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo-centralino_virtuale">Cloud</a></li>
									<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo-centralino_software">Solo Software</a></li>
									<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo-centralino_dect">DECT</a></li>
									<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo_di_derivati_int_collegabili-analogici_voip-analogici_digitali_voip-analogici-int_analogici_digita">Analogici</a></li>
									<li><a href="https://www.ezdirect.it/centralini-telefonici/">Tutti</a></li>
									
									
									</ul>
								</td>
								
								<td style='width:50%; vertical-align:middle !important'>
								<div class='categories-home-in-img'>
									<img src='http://46.101.235.11/img/centralini-home.png'  alt='Centralini telefonici' title='Centralini telefonici' />
								</div>
								</td>
							</tr>
						</table>
					</div>
				</div>

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
				
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="http://46.101.235.11/index.php?id_category=115&amp;controller=category">Telefoni</a></h3>
								</td>

							</tr>
							
							<tr style="display: block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							<li><a href="https://www.ezdirect.it/telefoni-fissi/">Fissi</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/#/tipo-cordless_tipo_voip">Cordless IP</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-fissi/#/ottimizzato_per-microsoft_teams">Teams</a></li>
							<li><a href="https://www.ezdirect.it/cerca-ricerca-cercare-prodotti-telefonia?orderby=position&orderway=desc&nnn=on&cod=on&sd=on&search_query=certificato%20zoom&submit_search=Cerca">Zoom</a></li>
							<li><a href="https://www.ezdirect.it/cerca-ricerca-cercare-prodotti-telefonia?orderby=position&orderway=desc&nnn=on&cod=on&sd=on&search_query=videotelefoni&submit_search=Cerca">Videotelefoni</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/#/tipo-cordless_tipo_wifi">WiFi</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-fissi/">Tutti i fissi</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/">Tutti i cordless</a></li>
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<img src='http://46.101.235.11/img/telefoni-home.png' alt='Ezdirect' title='Ezdirect' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>

				
			

			

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
			
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="http://46.101.235.11/index.php?id_category=106&amp;controller=category">Cuffie e auricolari</a></h3>
								</td>

							</tr>
						<tr style="display:block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-con_filo">Con filo</a></li>
							<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-senza_filo">Senza filo</a></li>
							<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-ibrida">Ibride</a></li>
							<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-con_filo/utilizzabile_con-pc_usb_bluetooth-telefono_fisso_pc_usb">Multiuso</a></li>
							<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/ottimizzato_per-microsoft_teams">Teams</a></li>
							<li><a href="https://www.ezdirect.it/cuffie-telefoniche/">Tutti</a></li>
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<img src='http://46.101.235.11/img/cuffie-home.png' alt='Cuffie e auricolari' title='Cuffie e auricolari' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
				
						<table style='width:100%; height:100%; table-layout:fixed;'>
						

							<tr>
								<td style="width:250px;">
									<h3><a href="http://46.101.235.11/index.php?id_category=75&amp;controller=category">Audioconferenza</a></h3>
								</td>

							</tr>
						<tr style="display:block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-audc_usb">USB</a></li>
							<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-audioconferenze_voip">IP</a></li>
							<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-multiuso">Multiuso</a></li>
							<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-gsm">GSM</a></li>
							<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-sistema_pro">Sistema PRO</a></li>
							<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/">Tutti</a></li>
							
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<img src='http://46.101.235.11/img/audioconferenza.png' alt='Audioconferenza' title='Audioconferenza' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
				
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="http://46.101.235.11/index.php?id_category=76&amp;controller=category">Videoconferenza</a></h3>
								</td>

							</tr>
							<tr style="display: block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-videocamera_usb">Videocamera USB</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-sistema">Sistema</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-servizio_cloud">Cloud</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-clickshare">Clickshare</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-accessori_clickshare">Accessori clickshare</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-room_manager">Room Management</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/">Tutti</a></li>
							
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<img src='http://46.101.235.11/img/videoconferenza.png' alt='Videoconferenza' title='Videoconferenza' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>
				

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in' style="width:100%;">
			
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="http://46.101.235.11/index.php?id_category=208&amp;controller=category">Citofoni IP</a></h3>
								</td>

							</tr>
						<tr style="display: block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/tipo-voipcitofoni">Videocitofoni IP</a></li>
							<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/tipo-controllo_accessi">Controllo Accessi</a></li>
							<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/tipo-cit_an">Interfacce</a></li>
							<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/tipo-cit_an">Analogici</a></li>
							<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/">Tutti</a></li>
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important; padding-top: 4px;'>
							<div class='categories-home-in-img'>
								<img src='http://46.101.235.11/img/2nhome.png' alt='Citofoni IP' title='Citofoni IP' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>

			

			

				

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
				
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="http://46.101.235.11/index.php?id_category=46&amp;controller=category">Gateway VoIP</a></h3>
								</td>

							</tr>
						<tr style="display: block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							
							<li><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/tipo-gateway_isdn">ISDN</a></li>
							<li><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/porte_fxs-2-4-1-8-24-16-32-48-fino_a_288">Analogico (FXS)</a></li>
							<li><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/porte_fxo-4-2-8-1-16">Analogico (FXO)</a></li>
							<li><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/tipo-gateway_ibrido">Ibrido</a></li>
							<li><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/">Tutti</a></li>
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<img src='http://46.101.235.11/img/gateway-voip.png' alt='Gateway VoIP' title='Gateway VoIP' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
				
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="http://46.101.235.11/index.php?id_category=27&amp;controller=category">Gateway GSM</a></h3>
								</td>

							</tr>
						<tr style="display: block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-gateway_gsm">GSM</a></li>
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-gateway_umts-router_umts">WCDMA</a></li>
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-gateway_lte_4g-scheda_lte-ripetitore_lte_gsm-router_lte_4g">LTE</a></li>
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/connessione-analogica">Analogico</a></li>
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/connessione-isdn">ISDN</a></li>
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-ripetitore_gsm_umts_lte-ripetitore_umts_gsm-ripetitore_lte_gsm-ripetitore_gsm_umts_dcs">Ripetitore di segnale</a></li>
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/">Tutti</a></li>
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<img src='http://46.101.235.11/img/gateway-gsm.png' alt='Gateway GSM' title='Gateway GSM' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
				
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="https://www.ezdirect.it/offerte-speciali">Offerte Speciali</a></h3>
								</td>

							</tr>
						<tr style="display: block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul id="categories-home-in-menu">
							<li><a href="https://www.ezdirect.it/offerte-speciali">Approfitta subito delle nostre promozioni!</a></li>
							
							
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<a href="https://www.ezdirect.it/offerte-speciali">
									<img style="width:100%;" src='http://46.101.235.11/img/offerte-speciali.png' alt='Offerte speciali' title='Offerte speciali' />
								</a>
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>
			</div>
			
			
			
			
			
			
			
			
			
			
		
		
		</div>
	</div>
	
</div>

<!--I NOSTRI MARCHI-->
<div class="marchi">
	<h2 style="color:#000099; text-align:center; padding: 30px 0; font-size:25px; text-transform: uppercase; letter-spacing: 4px; font-weight: 600; ">I nostri migliori marchi</h2>

	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators" style="display:none;">
		<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		<li data-target="#carousel-example-generic" data-slide-to="1"></li>
		<li data-target="#carousel-example-generic" data-slide-to="2"></li>
	</ol>

	<!-- Wrapper for slides -->
	<div class="carousel-inner" role="listbox">
		<div class="item active">
			<div class="container">
				<div class="row col-sm-12" style="display: flex; align-items: center;">
					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/2n.png" alt="Banner">
						<p id="nome-marchio">2n</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/yeastar.png" alt="Banner">
						<p id="nome-marchio">Yeastar</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/3cx.png" alt="Banner">
						<p id="nome-marchio">3cx</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/jabra.png" alt="Banner">
						<p id="nome-marchio">jabra</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/openvox.jpeg" alt="Banner">
						<p id="nome-marchio">Open Vox</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/ezdirect.png" alt="Banner">
						<p id="nome-marchio">EzDirect</p></a>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
		<div class="container">
				<div class="row col-sm-12" style="display: flex; align-items: center;">
					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/patton.png" alt="Banner">
						<p id="nome-marchio">Patton</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/ascom.png" alt="Banner">
						<p id="nome-marchio">Ascom</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/cisco.png" alt="Banner">
						<p id="nome-marchio">Cisco</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/fanvil.png" alt="Banner">
						<p id="nome-marchio">Fanvil</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/gigaset.png" alt="Banner">
						<p id="nome-marchio">Gigaset</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/snom.png" alt="Banner">
						<p id="nome-marchio">Snom</p></a>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="container">
				<div class="row col-sm-12" style="display: flex; align-items: center;">
					<div class="col-sm-2" id="col-marchio-nome">
						<a href="https://shop.samsung.com/it/?cid=it_pd_ppc_google_samsung_brand_shop_text_text_brand-kw&gclid=Cj0KCQjwkIGKBhCxARIsAINMioIwvbdiBw2CoJ4ZV0Eeue3wrPQ7whtIY0e1eu_6hUUVgHUabp9oLB0aAnyBEALw_wcB&gclsrc=aw.ds" id="marchio-link"><img src="http://46.101.235.11/img/samsung.png" alt="Banner">
						<p id="nome-marchio">Samsung</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/panasonic.png" alt="Banner">
						<p id="nome-marchio">Panasonic</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/dahua.png" alt="Banner">
						<p id="nome-marchio">Dahua</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/yealink.png" alt="Banner">
						<p id="nome-marchio">Yealink</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/hiboost.png" alt="Banner">
						<p id="nome-marchio">Hiboost</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/grandstream.png" alt="Banner">
						<p id="nome-marchio">Grandstream</p></a>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="container">
				<div class="row col-sm-12" style="display: flex; align-items: center;">
					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/dinstar.png" alt="Banner">
						<p id="nome-marchio">Dinstar</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/lucky-tone.jpeg" alt="Banner">
						<p id="nome-marchio">Lucky Tone</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/avaya.png" alt="Banner">
						<p id="nome-marchio">Avaya</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/alcatel.png" alt="Banner">
						<p id="nome-marchio">Alcatel</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/epos.png" alt="Banner">
						<p id="nome-marchio">Epos</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="http://46.101.235.11/img/poly.png" alt="Banner">
						<p id="nome-marchio">Poly</p></a>
					</div>
				</div>
			</div>
		</div>


	

	<!-- Controls -->
	<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev" style="color: #f4f4f4; background-image: none; width: 85px; background-color: white; opacity: 1; z-index: 0;">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next" style="color: #f4f4f4; background-image: none; width: 115px; background-color: white; opacity: 1; z-index: 0;">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
	</div>
</div>

</div>

<!--fine I NOSTRI MARCHI-->

		

		


<div class="center_column">

<h4 class='homepage'>In evidenza</h4>

	<!-- MODULE Home Featured Products -->
	<div id="home_featured_module">
		<div id='coin-slider'>
																			
				
																		<div class='home-low-level' style=' '>
						
			<div style=''>
			
			
		
						
						
												
						<a href="http://46.101.235.11/index.php?id_product=393102&controller=product" style="display:block; position:relative" title="Centralino IP per ufficio e albergo Openvox UC501 800 interni"  class="product_img_link"><img src="https://www.ezdirect.it/img/p/393102-737391-home.jpg"  class="classhome" alt="Centralino IP per ufficio e albergo Openvox UC501 800 interni" /></a>
			</div>
			
			
			
			
			<div style='margin-top:20px'>
			<h5 style='font-size:15px'><a class="product_name_title" style="line-height:22px" href="http://46.101.235.11/index.php?id_product=393102&controller=product" title="Centralino IP per ufficio e albergo Openvox UC501 800 interni">Centralino IP per ufficio e albergo Openvox UC501 800 interni</a></h5>
			</div>	
			
			<div class='home-medium-category'>
<span><a style='font-weight:normal' href=""></a></span>
			</div>

			
			
<div style="display: flex; justify-content: space-between; align-items: center; ">
			<div class='home-medium-price'>
			
															<span style='font-size:25px; font-weight:bold; color: #ff6600;'><strong>										349,00 €										</strong></span>												
			</div>
						<div class="home-medium-cart-slot">
			
			
									
									
				<a name="393102" href="http://46.101.235.11/index.php?controller=cart?qty=1&amp;id_product=393102&amp;token=47ceb63b233502e24a558cfc729aae93&amp;add" id="ajax_id_product_393102" title="Add Centralino IP per ufficio e albergo Openvox UC501 800 interni to cart">
									
					<i class="fas fa-cart-plus" id="cart-plus" style="color: #ff6600; font-size: 26px;"></i>
									
				</a>
			</div>	
				</div>
					
							</div>
								
				
																				<div class='home-low-level' style=' '>
						
			<div style=''>
			
			
		
						
						
												
						<a href="http://46.101.235.11/index.php?id_product=378637&controller=product" style="display:block; position:relative" title="Fanvil i32V videocitofono IP IP65 RFID antivandalo IK10"  class="product_img_link"><img src="https://www.ezdirect.it/img/p/378637-202830-home.jpg"  class="classhome" alt="Fanvil i32V videocitofono IP IP65 RFID antivandalo IK10" /></a>
			</div>
			
			
			
			
			<div style='margin-top:20px'>
			<h5 style='font-size:15px'><a class="product_name_title" style="line-height:22px" href="http://46.101.235.11/index.php?id_product=378637&controller=product" title="Fanvil i32V videocitofono IP IP65 RFID antivandalo IK10">Fanvil i32V videocitofono IP IP65 RFID antivandalo IK10</a></h5>
			</div>	
			
			<div class='home-medium-category'>
<span><a style='font-weight:normal' href=""></a></span>
			</div>

			
			
<div style="display: flex; justify-content: space-between; align-items: center; ">
			<div class='home-medium-price'>
			
															<span style='font-size:25px; font-weight:bold; color: #ff6600;'><strong>										180,00 €										</strong></span>												
			</div>
						<div class="home-medium-cart-slot">
			
			
									
									
				<a name="378637" href="http://46.101.235.11/index.php?controller=cart?qty=1&amp;id_product=378637&amp;token=47ceb63b233502e24a558cfc729aae93&amp;add" id="ajax_id_product_378637" title="Add Fanvil i32V videocitofono IP IP65 RFID antivandalo IK10 to cart">
									
					<i class="fas fa-cart-plus" id="cart-plus" style="color: #ff6600; font-size: 26px;"></i>
									
				</a>
			</div>	
				</div>
					
							</div>
								
				
																				<div class='home-low-level' style=' '>
						
			<div style=''>
			
			
		
						
						
												
						<a href="http://46.101.235.11/index.php?id_product=393267&controller=product" style="display:block; position:relative" title="SIP Horn Speaker 30W PoE++ 120dB IP67 Lucky Tone"  class="product_img_link"><img src="https://www.ezdirect.it/img/p/393267-737559-home.jpg"  class="classhome" alt="SIP Horn Speaker 30W PoE++ 120dB IP67 Lucky Tone" /></a>
			</div>
			
			
			
			
			<div style='margin-top:20px'>
			<h5 style='font-size:15px'><a class="product_name_title" style="line-height:22px" href="http://46.101.235.11/index.php?id_product=393267&controller=product" title="SIP Horn Speaker 30W PoE++ 120dB IP67 Lucky Tone">SIP Horn Speaker 30W PoE++ 120dB IP67 Lucky Tone</a></h5>
			</div>	
			
			<div class='home-medium-category'>
<span><a style='font-weight:normal' href=""></a></span>
			</div>

			
			
<div style="display: flex; justify-content: space-between; align-items: center; ">
			<div class='home-medium-price'>
			
															<span style='font-size:25px; font-weight:bold; color: #ff6600;'><strong>										379,00 €										</strong></span>												
			</div>
						<div class="home-medium-cart-slot">
			
			
									
									
				<a name="393267" href="http://46.101.235.11/index.php?controller=cart?qty=1&amp;id_product=393267&amp;token=47ceb63b233502e24a558cfc729aae93&amp;add" id="ajax_id_product_393267" title="Add SIP Horn Speaker 30W PoE++ 120dB IP67 Lucky Tone to cart">
									
					<i class="fas fa-cart-plus" id="cart-plus" style="color: #ff6600; font-size: 26px;"></i>
									
				</a>
			</div>	
				</div>
					
							</div>
								
				
																				<div class='home-low-level' style='margin-right:-1px '>
						
			<div style=''>
			
			
		
						
						
												
						<a href="http://46.101.235.11/index.php?id_product=393420&controller=product" style="display:block; position:relative" title="Sennheiser PC8"  class="product_img_link"><img src="https://www.ezdirect.it/img/p/393420-737763-home.jpg"  class="classhome" alt="Sennheiser PC8" /></a>
			</div>
			
			
			
			
			<div style='margin-top:20px'>
			<h5 style='font-size:15px'><a class="product_name_title" style="line-height:22px" href="http://46.101.235.11/index.php?id_product=393420&controller=product" title="Sennheiser PC8">Sennheiser PC8</a></h5>
			</div>	
			
			<div class='home-medium-category'>
<span><a style='font-weight:normal' href=""></a></span>
			</div>

			
			
<div style="display: flex; justify-content: space-between; align-items: center; ">
			<div class='home-medium-price'>
			
															<span style='font-size:25px; font-weight:bold; color: #ff6600;'><strong>										24,90 €										</strong></span>												
			</div>
						<div class="home-medium-cart-slot">
			
			
									
									
				<a name="393420" href="http://46.101.235.11/index.php?controller=cart?qty=1&amp;id_product=393420&amp;token=47ceb63b233502e24a558cfc729aae93&amp;add" id="ajax_id_product_393420" title="Add Sennheiser PC8 to cart">
									
					<i class="fas fa-cart-plus" id="cart-plus" style="color: #ff6600; font-size: 26px;"></i>
									
				</a>
			</div>	
				</div>
					
							</div>
								
				
																				<div class='home-low-level' style=' '>
						
			<div style=''>
			
			
		
						
						
												
						<a href="http://46.101.235.11/index.php?id_product=336258&controller=product" style="display:block; position:relative" title="IPN W980 cuffia wireless per PC e telefono con archetto"  class="product_img_link"><img src="https://www.ezdirect.it/img/p/336258-21027-home.jpg"  class="classhome" alt="IPN W980 cuffia wireless per PC e telefono con archetto" /></a>
			</div>
			
			
			
			
			<div style='margin-top:20px'>
			<h5 style='font-size:15px'><a class="product_name_title" style="line-height:22px" href="http://46.101.235.11/index.php?id_product=336258&controller=product" title="IPN W980 cuffia wireless per PC e telefono con archetto">IPN W980 cuffia wireless per PC e telefono con archetto</a></h5>
			</div>	
			
			<div class='home-medium-category'>
<span><a style='font-weight:normal' href=""></a></span>
			</div>

			
			
<div style="display: flex; justify-content: space-between; align-items: center; ">
			<div class='home-medium-price'>
			
															<span style='font-size:25px; font-weight:bold; color: #ff6600;'><strong>										145,00 €										</strong></span>												
			</div>
						<div class="home-medium-cart-slot">
			
			
									
									
				<a name="336258" href="http://46.101.235.11/index.php?controller=cart?qty=1&amp;id_product=336258&amp;token=47ceb63b233502e24a558cfc729aae93&amp;add" id="ajax_id_product_336258" title="Add IPN W980 cuffia wireless per PC e telefono con archetto to cart">
									
					<i class="fas fa-cart-plus" id="cart-plus" style="color: #ff6600; font-size: 26px;"></i>
									
				</a>
			</div>	
				</div>
					
							</div>
								
				
																				<div class='home-low-level' style=' '>
						
			<div style=''>
			
			
		
						
						
												
						<a href="http://46.101.235.11/index.php?id_product=331514&controller=product" style="display:block; position:relative" title="Cuffia USB biauricolare S4B Ezlight UC duo"  class="product_img_link"><img src="https://www.ezdirect.it/img/p/331514-46599-home.jpg"  class="classhome" alt="Cuffia USB biauricolare S4B Ezlight UC duo" /></a>
			</div>
			
			
			
			
			<div style='margin-top:20px'>
			<h5 style='font-size:15px'><a class="product_name_title" style="line-height:22px" href="http://46.101.235.11/index.php?id_product=331514&controller=product" title="Cuffia USB biauricolare S4B Ezlight UC duo">Cuffia USB biauricolare S4B Ezlight UC duo</a></h5>
			</div>	
			
			<div class='home-medium-category'>
<span><a style='font-weight:normal' href=""></a></span>
			</div>

			
			
<div style="display: flex; justify-content: space-between; align-items: center; ">
			<div class='home-medium-price'>
			
															<span style='font-size:25px; font-weight:bold; color: #ff6600;'><strong>										46,90 €										</strong></span>												
			</div>
						<div class="home-medium-cart-slot">
			
			
									
									
				<a name="331514" href="http://46.101.235.11/index.php?controller=cart?qty=1&amp;id_product=331514&amp;token=47ceb63b233502e24a558cfc729aae93&amp;add" id="ajax_id_product_331514" title="Add Cuffia USB biauricolare S4B Ezlight UC duo to cart">
									
					<i class="fas fa-cart-plus" id="cart-plus" style="color: #ff6600; font-size: 26px;"></i>
									
				</a>
			</div>	
				</div>
					
							</div>
								
				
																				<div class='home-low-level' style=' '>
						
			<div style=''>
			
			
		
						
						
												
						<a href="http://46.101.235.11/index.php?id_product=318719&controller=product" style="display:block; position:relative" title="Cuffia con microfono Ultra Noise Cancelling Ezlight Top mono"  class="product_img_link"><img src="https://www.ezdirect.it/img/p/318719-9243-home.jpg"  class="classhome" alt="Cuffia con microfono Ultra Noise Cancelling Ezlight Top mono" /></a>
			</div>
			
			
			
			
			<div style='margin-top:20px'>
			<h5 style='font-size:15px'><a class="product_name_title" style="line-height:22px" href="http://46.101.235.11/index.php?id_product=318719&controller=product" title="Cuffia con microfono Ultra Noise Cancelling Ezlight Top mono">Cuffia con microfono Ultra Noise Cancelling Ezlight Top mono</a></h5>
			</div>	
			
			<div class='home-medium-category'>
<span><a style='font-weight:normal' href=""></a></span>
			</div>

			
			
<div style="display: flex; justify-content: space-between; align-items: center; ">
			<div class='home-medium-price'>
			
															<span style='font-size:25px; font-weight:bold; color: #ff6600;'><strong>										40,80 €										</strong></span>												
			</div>
						<div class="home-medium-cart-slot">
			
			
									
									
				<a name="318719" href="http://46.101.235.11/index.php?controller=cart?qty=1&amp;id_product=318719&amp;token=47ceb63b233502e24a558cfc729aae93&amp;add" id="ajax_id_product_318719" title="Add Cuffia con microfono Ultra Noise Cancelling Ezlight Top mono to cart">
									
					<i class="fas fa-cart-plus" id="cart-plus" style="color: #ff6600; font-size: 26px;"></i>
									
				</a>
			</div>	
				</div>
					
							</div>
								
				
																				<div class='home-low-level' style='margin-right:-1px '>
						
			<div style=''>
			
			
		
						
						
												
						<a href="http://46.101.235.11/index.php?id_product=393357&controller=product" style="display:block; position:relative" title="Yealink WH67 UC cuffia wireless multiuso USB bluetooth"  class="product_img_link"><img src="https://www.ezdirect.it/img/p/393357-737658-home.jpg"  class="classhome" alt="Yealink WH67 UC cuffia wireless multiuso USB bluetooth" /></a>
			</div>
			
			
			
			
			<div style='margin-top:20px'>
			<h5 style='font-size:15px'><a class="product_name_title" style="line-height:22px" href="http://46.101.235.11/index.php?id_product=393357&controller=product" title="Yealink WH67 UC cuffia wireless multiuso USB bluetooth">Yealink WH67 UC cuffia wireless multiuso USB bluetooth</a></h5>
			</div>	
			
			<div class='home-medium-category'>
<span><a style='font-weight:normal' href=""></a></span>
			</div>

			
			
<div style="display: flex; justify-content: space-between; align-items: center; ">
			<div class='home-medium-price'>
			
															<span style='font-size:25px; font-weight:bold; color: #ff6600;'><strong>										196,52 €										</strong></span>												
			</div>
						<div class="home-medium-cart-slot">
			
			
									
									
				<a name="393357" href="http://46.101.235.11/index.php?controller=cart?qty=1&amp;id_product=393357&amp;token=47ceb63b233502e24a558cfc729aae93&amp;add" id="ajax_id_product_393357" title="Add Yealink WH67 UC cuffia wireless multiuso USB bluetooth to cart">
									
					<i class="fas fa-cart-plus" id="cart-plus" style="color: #ff6600; font-size: 26px;"></i>
									
				</a>
			</div>	
				</div>
					
							</div>
								
				
													<div style='clear:both'></div>

		
	

<script type="text/javascript">
	/*$(document).ready(function(){
		$(function() {
			$('.banner-parlano').unslider({
				speed: 500,               //  The speed to animate each slide (in milliseconds)
				delay: 5000,              //  The delay between slide animations (in milliseconds)
				keys: true,               //  Enable keyboard (left, right) arrow shortcuts
				dots: true,               //  Display dot navigation
			});
		});
	});*/
	</script>
	

	<div id="banner-parlano">
		<h4 class='homepage' style="margin-bottom: 0;">Parlano di noi</h4><br /><br />
		<div class="banner-parlano">
			<ul style="height:40px">
			
			
				<li style="width:100% !important">
				<a href='https://www.notizie.it/economia/2019/10/01/ezdirect-vendita-online-soluzioni-per-comunicare/'><img src='https://www.ezdirect.it/themes/ez20/img/parlano/notizie.jpg' width='150' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.datamanager.it/2021/01/ezdirect-le-soluzioni-per-comunicare/'><img src='https://www.ezdirect.it/themes/ez20/img/parlano/datamanager.jpg' width='150' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.punto-informatico.it/ezdirect-soluzioni-per-la-unified-communication/'><img src='https://www.ezdirect.it/themes/ez20/img/parlano/puntoinformatico.jpg' width='150' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.adnkronos.com/ezdirect-la-risposta-professionale-alle-esigenze-di-soluzioni-per-comunicare-delle-imprese_1oRbAnwIjAeMOM9mIbBjRf?refresh_ce'><img src='https://www.ezdirect.it/themes/ez20/img/parlano/adnkronos.jpg' width='150' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.affaritaliani.it/comunicati/notiziario/ezdirect_la_risposta_%E2%80%9Cprofessionale%E2%80%9D_alle_esigenze_di_soluzioni_per_comunicare_delle_imprese-86213.html?refresh_cens'><img width='150' src='https://www.ezdirect.it/themes/ez20/img/parlano/affariitaliani.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.techcompany360.it/distributori/le-competenze-ezdirect-sempre-in-linea-con-le-esigenze-dei-clienti/'><img src='https://www.ezdirect.it/themes/ez20/img/parlano/techcompany360.jpg' width='150' alt='Ezdirect' title='Ezdirect' /></a>
				</li>			
					
			</ul><br /><br /><br />
		</div>
</div>

<!-- /MODULE Home Featured Products -->
<?php }} ?>
