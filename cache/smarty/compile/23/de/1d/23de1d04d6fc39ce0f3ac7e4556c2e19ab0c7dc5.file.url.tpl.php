<?php /* Smarty version Smarty-3.1.19, created on 2021-11-24 13:06:39
         compiled from "/var/www/html/modules/gestpay/views/templates/admin/tabs/url.tpl" */ ?>
<?php /*%%SmartyHeaderCode:269289582619e2acf6c5ec0-83713409%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '23de1d04d6fc39ce0f3ac7e4556c2e19ab0c7dc5' => 
    array (
      0 => '/var/www/html/modules/gestpay/views/templates/admin/tabs/url.tpl',
      1 => 1634899860,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '269289582619e2acf6c5ec0-83713409',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'module_display' => 0,
    'response_url' => 0,
    'ip' => 0,
    'module_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_619e2acf6cfbf9_67759303',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_619e2acf6cfbf9_67759303')) {function content_619e2acf6cfbf9_67759303($_smarty_tpl) {?>
<?php if (@constant('_PS_VERSION_')>=1.6) {?>
<h3><i class="icon-link"></i> <?php echo smartyTranslate(array('s'=>'URL','mod'=>'gestpay'),$_smarty_tpl);?>
 <small><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_display']->value, ENT_QUOTES, 'UTF-8', true);?>
</small></h3>
<div class="media">
        <p><?php echo smartyTranslate(array('s'=>'In the "Response Address" area, set the link below for both "URL for positive response" and "URL for negative response" fields:','mod'=>'gestpay'),$_smarty_tpl);?>
<br />
        <span style="color: red"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['response_url']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
validation.php</span></p>
        <p><?php echo smartyTranslate(array('s'=>'In the "Response Address" area, set the link below "URL Server to Server" field:','mod'=>'gestpay'),$_smarty_tpl);?>
<br />
        <span style="color: red"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['response_url']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
s2s.php</span></p>
        <p><?php echo smartyTranslate(array('s'=>'In the "Ip Address" field, insert the ip or range ip of your server (please verify this address with your server provider)','mod'=>'gestpay'),$_smarty_tpl);?>
<br />
        <span style="color: red"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['ip']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</span></p>
</div>
<?php } else { ?>
<fieldset>
    <legend><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
views/img/information.png" alt="<?php echo smartyTranslate(array('s'=>'URL','mod'=>'gestpay'),$_smarty_tpl);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_display']->value, ENT_QUOTES, 'UTF-8', true);?>
"><?php echo smartyTranslate(array('s'=>'URL','mod'=>'gestpay'),$_smarty_tpl);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_display']->value, ENT_QUOTES, 'UTF-8', true);?>
</legend>
    <div class="media">
        <p><?php echo smartyTranslate(array('s'=>'In the "Response Address" area, set the link below for both "URL for positive response" and "URL for negative response" fields:','mod'=>'gestpay'),$_smarty_tpl);?>
<br />
            <span style="color: red"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['response_url']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
validation.php</span></p>
        <p><?php echo smartyTranslate(array('s'=>'In the "Response Address" area, set the link below "URL Server to Server" field:','mod'=>'gestpay'),$_smarty_tpl);?>
<br />
            <span style="color: red"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['response_url']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
s2s.php</span></p>
        <p><?php echo smartyTranslate(array('s'=>'In the "Ip Address" field, insert the ip or range ip of your server (please verify this address with your server provider)','mod'=>'gestpay'),$_smarty_tpl);?>
<br />
            <span style="color: red"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['ip']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</span></p>
    </div>
</fieldset>
<?php }?><?php }} ?>
