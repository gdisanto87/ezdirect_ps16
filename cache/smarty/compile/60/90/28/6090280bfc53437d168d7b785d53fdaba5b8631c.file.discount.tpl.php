<?php /* Smarty version Smarty-3.1.19, created on 2021-12-01 09:54:55
         compiled from "/var/www/html/themes/ez20/discount.tpl" */ ?>
<?php /*%%SmartyHeaderCode:26188186261a7385f3a8991-48922750%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6090280bfc53437d168d7b785d53fdaba5b8631c' => 
    array (
      0 => '/var/www/html/themes/ez20/discount.tpl',
      1 => 1637749847,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '26188186261a7385f3a8991-48922750',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'navigationPipe' => 0,
    'discount' => 0,
    'nbDiscounts' => 0,
    'discountDetail' => 0,
    'img_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61a7385f3c11e9_12583245',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61a7385f3c11e9_12583245')) {function content_61a7385f3c11e9_12583245($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/html/tools/smarty/plugins/modifier.date_format.php';
?>

<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account.php',true);?>
"><?php echo smartyTranslate(array('s'=>'My account'),$_smarty_tpl);?>
</a><span class="navigation-pipe"><?php echo $_smarty_tpl->tpl_vars['navigationPipe']->value;?>
</span><?php echo smartyTranslate(array('s'=>'My Vouchers'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<h1><?php echo smartyTranslate(array('s'=>'My Vouchers'),$_smarty_tpl);?>
</h1>

<?php if (isset($_smarty_tpl->tpl_vars['discount']->value)&&count($_smarty_tpl->tpl_vars['discount']->value)&&$_smarty_tpl->tpl_vars['nbDiscounts']->value) {?>
<table id="discount-table" class="std2" style="width:100%">
	<thead>
		<tr>
			<th style="background-color:#8899cc;" class="discount_code first_item"><?php echo smartyTranslate(array('s'=>'Code'),$_smarty_tpl);?>
</th>
			<th style="background-color:#99aadd;" class="discount_description item"><?php echo smartyTranslate(array('s'=>'Description'),$_smarty_tpl);?>
</th>
			<th style="background-color:#8899cc;" class="discount_quantity item"><?php echo smartyTranslate(array('s'=>'Quantity'),$_smarty_tpl);?>
</th>
			<th style="background-color:#99aadd;" class="discount_value item"><?php echo smartyTranslate(array('s'=>'Value'),$_smarty_tpl);?>
*</th>
			<th style="background-color:#8899cc;" class="discount_minimum item"><?php echo smartyTranslate(array('s'=>'Minimum'),$_smarty_tpl);?>
</th>
			<th style="background-color:#99aadd;" class="discount_cumulative item"><?php echo smartyTranslate(array('s'=>'Cumulative'),$_smarty_tpl);?>
</th>
			<th style="background-color:#8899cc;" class="discount_expiration_date last_item"><?php echo smartyTranslate(array('s'=>'Expiration date'),$_smarty_tpl);?>
</th>
		</tr>
	</thead>
	<tbody>
	<?php  $_smarty_tpl->tpl_vars['discountDetail'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['discountDetail']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['discount']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['discountDetail']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['discountDetail']->iteration=0;
 $_smarty_tpl->tpl_vars['discountDetail']->index=-1;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['myLoop']['index']=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['discountDetail']->key => $_smarty_tpl->tpl_vars['discountDetail']->value) {
$_smarty_tpl->tpl_vars['discountDetail']->_loop = true;
 $_smarty_tpl->tpl_vars['discountDetail']->iteration++;
 $_smarty_tpl->tpl_vars['discountDetail']->index++;
 $_smarty_tpl->tpl_vars['discountDetail']->first = $_smarty_tpl->tpl_vars['discountDetail']->index === 0;
 $_smarty_tpl->tpl_vars['discountDetail']->last = $_smarty_tpl->tpl_vars['discountDetail']->iteration === $_smarty_tpl->tpl_vars['discountDetail']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['myLoop']['first'] = $_smarty_tpl->tpl_vars['discountDetail']->first;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['myLoop']['index']++;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['myLoop']['last'] = $_smarty_tpl->tpl_vars['discountDetail']->last;
?>
		<?php if (smarty_modifier_date_format($_smarty_tpl->tpl_vars['discountDetail']->value['date_to'],"%Y%m%d")<smarty_modifier_date_format(time(),"%Y%m%d")) {?>
		<?php } else { ?>
			<tr class="<?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['myLoop']['first']) {?>first_item<?php } elseif ($_smarty_tpl->getVariable('smarty')->value['foreach']['myLoop']['last']) {?>last_item<?php } else { ?>item<?php }?> <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['myLoop']['index']%2) {?>alternate_item<?php }?>">
				<td class="discount_code"><?php echo $_smarty_tpl->tpl_vars['discountDetail']->value['name'];?>
</td>
				<td class="discount_description"><?php echo $_smarty_tpl->tpl_vars['discountDetail']->value['description'];?>
</td>
				<td class="discount_quantity" style="text-align:center"><?php echo $_smarty_tpl->tpl_vars['discountDetail']->value['quantity_for_user'];?>
</td>
				<td class="discount_value" style="text-align:center">
					<?php if ($_smarty_tpl->tpl_vars['discountDetail']->value['id_discount_type']==1) {?>
						<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['discountDetail']->value['value'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
%
					<?php } elseif ($_smarty_tpl->tpl_vars['discountDetail']->value['id_discount_type']==2) {?>
						<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['discountDetail']->value['value']),$_smarty_tpl);?>

					<?php } else { ?>
						<?php echo smartyTranslate(array('s'=>'Free shipping'),$_smarty_tpl);?>

					<?php }?>
				</td>
				<td class="discount_minimum" style="text-align:center">
					<?php if ($_smarty_tpl->tpl_vars['discountDetail']->value['minimal']==0) {?>
						<?php echo smartyTranslate(array('s'=>'none'),$_smarty_tpl);?>

					<?php } else { ?>
						<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['discountDetail']->value['minimal']),$_smarty_tpl);?>

					<?php }?>
				</td>
				<td class="discount_cumulative" style="text-align:center">
					<?php if ($_smarty_tpl->tpl_vars['discountDetail']->value['cumulable']==1) {?>
						<img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/yes.gif" alt="<?php echo smartyTranslate(array('s'=>'Yes'),$_smarty_tpl);?>
" class="icon" width="16" height="16" />
					<?php } else { ?>
						<img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/no.gif" alt="<?php echo smartyTranslate(array('s'=>'No'),$_smarty_tpl);?>
" class="icon" width="16" height="16" />
					<?php }?>
				</td>
				<td class="discount_expiration_date"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0][0]->dateFormat(array('date'=>$_smarty_tpl->tpl_vars['discountDetail']->value['date_to']),$_smarty_tpl);?>
</td>
			</tr>
		<?php }?>
	<?php } ?>
	</tbody>
</table>
<p>
	*<?php echo smartyTranslate(array('s'=>'Tax included'),$_smarty_tpl);?>

</p>
<?php } else { ?>
	<p class="warning"><?php echo smartyTranslate(array('s'=>'You do not possess any vouchers.'),$_smarty_tpl);?>
</p>
<?php }?>

<?php }} ?>
