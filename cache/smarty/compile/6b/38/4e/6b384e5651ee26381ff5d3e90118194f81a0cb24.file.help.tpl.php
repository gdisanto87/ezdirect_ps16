<?php /* Smarty version Smarty-3.1.19, created on 2021-11-24 13:06:39
         compiled from "/var/www/html/modules/gestpay/views/templates/admin/tabs/help.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1628916415619e2acf6d4520-41051278%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6b384e5651ee26381ff5d3e90118194f81a0cb24' => 
    array (
      0 => '/var/www/html/modules/gestpay/views/templates/admin/tabs/help.tpl',
      1 => 1634899860,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1628916415619e2acf6d4520-41051278',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'module_display' => 0,
    'ps_url' => 0,
    'module_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_619e2acf6df0b7_24719636',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_619e2acf6df0b7_24719636')) {function content_619e2acf6df0b7_24719636($_smarty_tpl) {?>
<?php if (@constant('_PS_VERSION_')>=1.6) {?>
<h3><i class="icon-book"></i> <?php echo smartyTranslate(array('s'=>'Help','mod'=>'gestpay'),$_smarty_tpl);?>
 <small><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_display']->value, ENT_QUOTES, 'UTF-8', true);?>
</small></h3>
<div class="media">
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ps_url']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
doc/readme_it.pdf" target="_blank">
        	<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ps_url']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
views/img/pdf.png" style="margin-right: 10px; margin-bottom: 15px"><?php echo smartyTranslate(array('s'=>'Axerve module manual','mod'=>'gestpay'),$_smarty_tpl);?>

        </a>
        <p><?php echo smartyTranslate(array('s'=>'Axerve documentation:','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
        <ul style='list-style-type:circle;'>
            <li><a href="http://docs.gestpay.it/gs/super-quick-start-guide.html" target="_blank"><?php echo smartyTranslate(array('s'=>'Quick start guide','mod'=>'gestpay'),$_smarty_tpl);?>
</a></li>
            <li><a href="https://gestpayws.sella.it/backoffice" target="_blank"><?php echo smartyTranslate(array('s'=>'Axerve control panel','mod'=>'gestpay'),$_smarty_tpl);?>
</a></li>
        </ul>
</div>
<?php } else { ?>
<fieldset>
    <legend><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
views/img/information.png" alt="<?php echo smartyTranslate(array('s'=>'Help','mod'=>'gestpay'),$_smarty_tpl);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_display']->value, ENT_QUOTES, 'UTF-8', true);?>
"><?php echo smartyTranslate(array('s'=>'Help','mod'=>'gestpay'),$_smarty_tpl);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_display']->value, ENT_QUOTES, 'UTF-8', true);?>
</legend>
    <div class="media">
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
doc/readme_it.pdf" target="_blank">
            <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
views/img/pdf.png" style="margin-right: 10px; margin-bottom: 15px; vertical-align: middle !important"><?php echo smartyTranslate(array('s'=>'Axerve module manual','mod'=>'gestpay'),$_smarty_tpl);?>

        </a>
        <p><?php echo smartyTranslate(array('s'=>'Axerve documentation:','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
        <ul style='list-style-type:circle; padding-left: 20px'>
            <li><a href="http://docs.gestpay.it/gs/super-quick-start-guide.html" target="_blank"><?php echo smartyTranslate(array('s'=>'Quick start guide','mod'=>'gestpay'),$_smarty_tpl);?>
</a></li>
            <li><a href="https://ecomm.sella.it/gestpay" target="_blank"><?php echo smartyTranslate(array('s'=>'Axerve control panel','mod'=>'gestpay'),$_smarty_tpl);?>
</a></li>
        </ul>
    </div>
</fieldset>
<?php }?><?php }} ?>
