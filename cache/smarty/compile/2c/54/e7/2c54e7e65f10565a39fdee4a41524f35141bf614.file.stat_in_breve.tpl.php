<?php /* Smarty version Smarty-3.1.19, created on 2022-01-04 10:44:39
         compiled from "/var/www/html/ezadmin/themes/default/template/controllers/dashboard/helpers/view/stat_in_breve.tpl" */ ?>
<?php /*%%SmartyHeaderCode:204359465261d41707237d26-77853043%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2c54e7e65f10565a39fdee4a41524f35141bf614' => 
    array (
      0 => '/var/www/html/ezadmin/themes/default/template/controllers/dashboard/helpers/view/stat_in_breve.tpl',
      1 => 1624543663,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '204359465261d41707237d26-77853043',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'stat_in_breve' => 0,
    'link' => 0,
    'periodo' => 0,
    'anno' => 0,
    'clienti' => 0,
    'g' => 0,
    'mese' => 0,
    'homeStats' => 0,
    'efficienza' => 0,
    'target' => 0,
    'hm' => 0,
    'c' => 0,
    'costruttori' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61d4170724f5f9_47544103',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61d4170724f5f9_47544103')) {function content_61d4170724f5f9_47544103($_smarty_tpl) {?>

<?php $_smarty_tpl->tpl_vars['periodo'] = new Smarty_variable($_smarty_tpl->tpl_vars['stat_in_breve']->value['periodo'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['anno'] = new Smarty_variable($_smarty_tpl->tpl_vars['stat_in_breve']->value['anno'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['clienti'] = new Smarty_variable($_smarty_tpl->tpl_vars['stat_in_breve']->value['clienti'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['efficienza'] = new Smarty_variable($_smarty_tpl->tpl_vars['stat_in_breve']->value['efficienza'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['target'] = new Smarty_variable($_smarty_tpl->tpl_vars['stat_in_breve']->value['target'], null, 0);?>

<section class="panel" id="stat_in_breve">
    <h3><i class="icon-signal"></i> Statistiche in breve</h3>

    <section id="base_periodo">
        <header>Su base periodo - <a style="color:white; font-size:90%" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminStats');?>
">vai a stats</a></header>
        <table class="table">
            <tr><td>Ricavi</td><td style="text-align:right"><?php echo $_smarty_tpl->tpl_vars['periodo']->value['ricavi'];?>
</td></tr>
            <tr><td>di cui Amazon</td><td style="text-align:right"><?php echo $_smarty_tpl->tpl_vars['periodo']->value['amazon'];?>
</td></tr>
            <tr><td>di cui Servizi</td><td style="text-align:right"><?php echo $_smarty_tpl->tpl_vars['periodo']->value['servizi'];?>
</td></tr>
            <tr><td>Ordini</td><td style="text-align:right"><?php echo $_smarty_tpl->tpl_vars['periodo']->value['ordini'];?>
</td></tr>
            <tr><td>Target quarter % (<?php echo $_smarty_tpl->tpl_vars['periodo']->value['target'];?>
)</td><td style="text-align:right"><?php echo $_smarty_tpl->tpl_vars['periodo']->value['target_p'];?>
 %</td></tr>
        </table>
    </section>

    <section id="base_anno">
        <header>Su base anno - <a style="color:white; font-size:90%" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminStats');?>
">vai a stats</a></header>
        <table class="table">
            <tr><td>Ricavi</td><td style="text-align:right"><?php echo $_smarty_tpl->tpl_vars['anno']->value['ricavi'];?>
</td></tr>
            <tr><td>di cui Amazon</td><td style="text-align:right"><?php echo $_smarty_tpl->tpl_vars['anno']->value['amazon'];?>
</td></tr>
            <tr><td>di cui Servizi</td><td style="text-align:right"><?php echo $_smarty_tpl->tpl_vars['anno']->value['servizi'];?>
</td></tr>
            <tr><td>Ordini</td><td style="text-align:right"><?php echo $_smarty_tpl->tpl_vars['anno']->value['ordini'];?>
</td></tr>
            <tr><td>Target anno % (<?php echo $_smarty_tpl->tpl_vars['anno']->value['target'];?>
)</td><td style="text-align:right"><?php echo $_smarty_tpl->tpl_vars['anno']->value['target_p'];?>
 %</td></tr>
        </table>
    </section>

    <section id="clienti_totale">
        <header>Clienti (totale)</header>
        <table class="table">
            <?php  $_smarty_tpl->tpl_vars['g'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['g']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['clienti']->value['gruppi_aziende']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['g']->key => $_smarty_tpl->tpl_vars['g']->value) {
$_smarty_tpl->tpl_vars['g']->_loop = true;
?>
            <tr><td><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminCustomers');?>
&submitFiltercustomer=1&customerFilter_a!id_default_group=<?php echo $_smarty_tpl->tpl_vars['g']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['g']->value['nome'];?>
</a></td><td style="text-align:right"><?php echo $_smarty_tpl->tpl_vars['g']->value['tot'];?>
</td></tr>
            <?php } ?>
			<tr><td><strong>Totale aziende</strong></td><td style="text-align:right"><strong><?php echo $_smarty_tpl->tpl_vars['clienti']->value['totale_aziende'];?>
</strong></td></tr>
            <?php  $_smarty_tpl->tpl_vars['g'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['g']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['clienti']->value['gruppi_privati']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['g']->key => $_smarty_tpl->tpl_vars['g']->value) {
$_smarty_tpl->tpl_vars['g']->_loop = true;
?>
            <tr><td><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminCustomers');?>
&submitFiltercustomer=1&customerFilter_a!id_default_group=<?php echo $_smarty_tpl->tpl_vars['g']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['g']->value['nome'];?>
</a></td><td style="text-align:right"><?php echo $_smarty_tpl->tpl_vars['g']->value['tot'];?>
</td></tr>
            <?php } ?>
			<tr><td><strong>Totale anagrafiche clienti</strong></td><td style="text-align:right"><strong><?php echo $_smarty_tpl->tpl_vars['clienti']->value['totale_privati'];?>
</strong></td></tr>
            <?php  $_smarty_tpl->tpl_vars['g'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['g']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['clienti']->value['gruppi_altri']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['g']->key => $_smarty_tpl->tpl_vars['g']->value) {
$_smarty_tpl->tpl_vars['g']->_loop = true;
?>
            <tr><td><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminCustomers');?>
&submitFiltercustomer=1&customerFilter_a!id_default_group=<?php echo $_smarty_tpl->tpl_vars['g']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['g']->value['nome'];?>
</a></td><td style="text-align:right"><?php echo $_smarty_tpl->tpl_vars['g']->value['tot'];?>
</td></tr>
            <?php } ?>
			<tr><td><strong>Totale anagrafiche</strong></td><td style="text-align:right"><strong><?php echo $_smarty_tpl->tpl_vars['clienti']->value['totale_clienti'];?>
</strong></td></tr>
        </table>
    </section>

    <section id="efficienza_periodo">
        <header>Efficienza (periodo: <?php echo $_smarty_tpl->tpl_vars['mese']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['homeStats']->value['year'];?>
)</header>
        <table class="table">
			<tr><td>Chiusura ticket</td><td style="text-align:right"><?php echo $_smarty_tpl->tpl_vars['efficienza']->value['hourstkt'];?>
 ore lav.</td></tr>
			<tr><td>Chiusura preventivo</td><td style="text-align:right"><?php echo $_smarty_tpl->tpl_vars['efficienza']->value['hoursprv'];?>
 ore lav.</td></tr>
			<tr><td>Chiusura todo</td><td style="text-align:right"><?php echo $_smarty_tpl->tpl_vars['efficienza']->value['hourstodo'];?>
 ore lav.</td></tr>
		</table>
    </section>

    <section id="target_anno">
        <header>Target anno - <a style="color:white; font-size:90%" href="javascript:void(0)" onclick="javascript:accoda()";>Clicca per aggiungere costr.</a></header>
        <table class="table">
            <?php  $_smarty_tpl->tpl_vars['hm'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['hm']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['target']->value['hms']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['hm']->key => $_smarty_tpl->tpl_vars['hm']->value) {
$_smarty_tpl->tpl_vars['hm']->_loop = true;
?>
            <tr id="tr_manufacturer_<?php echo $_smarty_tpl->tpl_vars['hm']->value['id'];?>
">
                <td>
                    <select class="select-costruttori" id="choose-manufacturer-<?php echo $_smarty_tpl->tpl_vars['hm']->value['id'];?>
" onchange="getManufacturerTarget(this.value, 'm-choose-manufacturer-<?php echo $_smarty_tpl->tpl_vars['hm']->value['id'];?>
')">
                        <option value="0">-- Seleziona costruttore --</option>
                        <?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['target']->value['costruttori']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value) {
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['c']->value['id_manufacturer'];?>
" <?php if ($_smarty_tpl->tpl_vars['hm']->value['id']==$_smarty_tpl->tpl_vars['c']->value['id_manufacturer']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['c']->value['name'];?>
</option>
                        <?php } ?>
                    </select>
                    <br />Target: <?php echo $_smarty_tpl->tpl_vars['hm']->value['target'];?>

                </td>
                <td><a href="javascript:void(0)" onclick="javascript:rimuovi(<?php echo $_smarty_tpl->tpl_vars['hm']->value['id'];?>
)";><img src="../img/admin/delete.gif" alt="Togli" title="Togli" /></a></td>
                <td style="text-align:right" id="m-choose-manufacturer-<?php echo $_smarty_tpl->tpl_vars['hm']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['hm']->value['target_percent'];?>
 %</td>
            </tr>
            <?php } ?>
        </table>
    </section>
</section>


<script type="text/javascript">
    
    function getManufacturerTarget(id_manufacturer, id_element) {
        var costruttori = "";
        var arrayCostruttori = document.getElementsByClassName("select-costruttori");
        for (var i = 0; i < arrayCostruttori.length; ++i) {
            var item = (arrayCostruttori[i].value);  
            costruttori += item+";";
        }

        $.ajax({
            url:"ajax.php?getManufacturerQuarter=y",
            type: "POST",
            data: {
                costruttori: costruttori,
                getManufacturerQuarter: "y",
                id_manufacturer: id_manufacturer
            },
            success:function(resp){
                document.getElementById(id_element).innerHTML = resp + "%";
            },
            error: function(xhr,stato,errori){
                return "Errore!";
            }
        });
    }
    
    function accoda() {
        var possible = "0123456789";
        var randomid = "";
        for(var i=0; i < 11; i++) {
            randomid += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        
        // Correggere - testare:
        var string = '<tr id="tr_manufacturer_'+randomid+'"><td><select class="select-costruttori" id="choose-manufacturer-'+randomid+'" onchange="getManufacturerTarget(this.value, \'m-choose-manufacturer-'+randomid+'\')"><option value="0">-- Seleziona costruttore --</option>';
        
        
        <?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['costruttori']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value) {
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
            string = string + '<option value="<?php echo $_smarty_tpl->tpl_vars['c']->value['id_manufacturer'];?>
"><?php echo $_smarty_tpl->tpl_vars['c']->value['name'];?>
</option>'; 
        <?php } ?>
        

        string = string + '</select><a href="javascript:void(0)" onclick="javascript:rimuovi('+randomid+')";><img src="../img/admin/delete.gif" alt="Togli" title="Togli" /> </a></td><td style="text-align:right" id="m-choose-manufacturer-'+randomid+'"></td></tr>';
        $(string)).appendTo("#table_target");
    }

    function rimuovi(id) {
        document.getElementById('tr_manufacturer_'+id).innerHTML = "";
        
        var costruttori = "";
        var arrayCostruttori = document.getElementsByClassName("select-costruttori");
        for (var i = 0; i < arrayCostruttori.length; ++i) {
            var item = (arrayCostruttori[i].value);  
            costruttori += item+";";
        }
        
        $.ajax({
            url:"ajax.php?removeManufacturerFromCookie=y",
            type: "POST",
            data: { 
                costruttori: costruttori
            },
            success:function(resp){
                alert("Costruttore rimosso con successo");
            },
            error: function(xhr,stato,errori){
                return "Errore!";
            }
        });
    }
    
</script><?php }} ?>
