<?php /* Smarty version Smarty-3.1.19, created on 2021-12-01 09:54:45
         compiled from "/var/www/html/modules/miefatture/fatture-ez20.tpl" */ ?>
<?php /*%%SmartyHeaderCode:73382361161a73855746736-82391325%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ccdcb89c6c70dcfc929ec85bd04de17ea64253ef' => 
    array (
      0 => '/var/www/html/modules/miefatture/fatture-ez20.tpl',
      1 => 1637751437,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '73382361161a73855746736-82391325',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'navigationPipe' => 0,
    'fatture' => 0,
    'fattura' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61a738557515a7_68266199',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61a738557515a7_68266199')) {function content_61a738557515a7_68266199($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/html/tools/smarty/plugins/modifier.date_format.php';
?>

<div id="miefatture">
	<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account.php',true);?>
"><?php echo smartyTranslate(array('s'=>'My account','mod'=>'miefatture'),$_smarty_tpl);?>
</a><span class="navigation-pipe"><?php echo $_smarty_tpl->tpl_vars['navigationPipe']->value;?>
</span><?php echo smartyTranslate(array('s'=>'My invoices','mod'=>'miefatture'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<h2><?php echo smartyTranslate(array('s'=>'My invoices','mod'=>'miefatture'),$_smarty_tpl);?>
</h2>
	<br />
	
	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	
	
	
		<?php if ($_smarty_tpl->tpl_vars['fatture']->value) {?>
			<table class="std2">
				<thead>
					<tr>
						<th class="first_item" style="background-color:#8899cc;text-align:left"><?php echo smartyTranslate(array('s'=>'Invoice ID','mod'=>'miefatture'),$_smarty_tpl);?>
</th>
						
						<th style="background-color:#99aadd; text-align:left"><?php echo smartyTranslate(array('s'=>'Ref. order','mod'=>'miefatture'),$_smarty_tpl);?>
</th>
						<th style="background-color:#8899cc;text-align:left"><?php echo smartyTranslate(array('s'=>'Invoice date','mod'=>'miefatture'),$_smarty_tpl);?>
</th>
						<th class="last_item" style="background-color:#99aadd; text-align:left"><?php echo smartyTranslate(array('s'=>'Download','mod'=>'miefatture'),$_smarty_tpl);?>
</th>
					</tr>
				</thead>
				<tbody>
				<?php  $_smarty_tpl->tpl_vars['fattura'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['fattura']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fatture']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['fattura']->key => $_smarty_tpl->tpl_vars['fattura']->value) {
$_smarty_tpl->tpl_vars['fattura']->_loop = true;
?>
				
				
				
				
				
				<tr>
					<td>
				
					<?php echo $_smarty_tpl->tpl_vars['fattura']->value['id_fattura'];?>

				
				</td>
					<td><?php echo $_smarty_tpl->tpl_vars['fattura']->value['rif_vs_ordine'];?>
</td>
					
					<td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['fattura']->value['data_fattura'],"%d/%m/%Y");?>
</td>
					<td><a href="/fattura.php?id_fattura=<?php echo $_smarty_tpl->tpl_vars['fattura']->value['id_fattura'];?>
&id_customer=<?php echo $_smarty_tpl->tpl_vars['fattura']->value['id_customer'];?>
"><?php echo smartyTranslate(array('s'=>'Click here to download your invoice','mod'=>'miefatture'),$_smarty_tpl);?>
</a></td>
					
					
				</tr>
				</tbody>
			<?php } ?>
			</table>
		
		<?php } else { ?>
		
			<p class="warning"><?php echo smartyTranslate(array('s'=>'You have no invoices','mod'=>'miefatture'),$_smarty_tpl);?>
.</p>
		
		<?php }?>
		
	
</div>
<?php }} ?>
