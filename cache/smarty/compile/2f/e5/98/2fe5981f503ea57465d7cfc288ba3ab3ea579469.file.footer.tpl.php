<?php /* Smarty version Smarty-3.1.19, created on 2022-01-07 19:15:28
         compiled from "/var/www/html/themes/ez20/footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:109282446061d8834083b4a5-90080435%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2fe5981f503ea57465d7cfc288ba3ab3ea579469' => 
    array (
      0 => '/var/www/html/themes/ez20/footer.tpl',
      1 => 1637749853,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '109282446061d8834083b4a5-90080435',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'id_cms_category' => 0,
    'content_only' => 0,
    'page_name' => 0,
    'msg' => 0,
    'nw_error' => 0,
    'value' => 0,
    'link' => 0,
    'img_dir' => 0,
    'img_ps_dir' => 0,
    'footer_mobile' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61d8834085aa62_23866766',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61d8834085aa62_23866766')) {function content_61d8834085aa62_23866766($_smarty_tpl) {?>

<!--Bootstrap 3.4-->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


<?php if ($_smarty_tpl->tpl_vars['id_cms_category']->value!=7) {?>

		<?php if (!$_smarty_tpl->tpl_vars['content_only']->value) {?>
				</div>

<!-- Right -->
				
			</div>

<!-- Footer -->
			<div style="clear:both"></div>
		</div>
	<?php }?>
	

</div> <!-- contenitore -->

<div id="footer">


<?php if ($_smarty_tpl->tpl_vars['page_name']->value=='index') {?>

<!-- Block Newsletter module-->
<?php if (isset($_smarty_tpl->tpl_vars['msg']->value)&&$_smarty_tpl->tpl_vars['msg']->value) {?>
		
		<div style="z-index:2147483647" id="<?php if ($_smarty_tpl->tpl_vars['nw_error']->value) {?>error_newsletter<?php } else { ?>success_newsletter<?php }?>">
		<p class="<?php if ($_smarty_tpl->tpl_vars['nw_error']->value) {?>warning_inline<?php } else { ?>success_inline<?php }?>"><a name="newsletter-success" id="newsletter-success"><?php echo $_smarty_tpl->tpl_vars['msg']->value;?>
</a></p>
		</div>
		
		<script type="text/javascript">
		<?php if ($_smarty_tpl->tpl_vars['nw_error']->value) {?>
		
		$(document).ready(function () {
			$('#error_newsletter').delay(7000).fadeOut('slow');
			$('#error_newsletter').css('zIndex', '2147483647');
		});
		
		<?php } else { ?>
		
		$(document).ready(function () {
			$('#success_newsletter').delay(7000).fadeOut('slow');
			$('#success_newsletter').css('zIndex', '2147483647');
		});
		
		<?php }?>
		</script>
	<?php }?>
	
<div id="newsletter_block_left">
	<div class="container" id="newsletter_block_left_in">
		<?php if (isset($_smarty_tpl->tpl_vars['msg']->value)&&$_smarty_tpl->tpl_vars['msg']->value) {?>
			<p class="<?php if ($_smarty_tpl->tpl_vars['nw_error']->value) {?>warning_inline<?php } else { ?>success_inline<?php }?>"><a name="newsletter-success" id="newsletter-success"><?php echo $_smarty_tpl->tpl_vars['msg']->value;?>
</a></p>
		<?php }?>

		
			<form action="https://www.ezdirect.it/index.php#newsletter-success" method="post">

				
				
				<div  style='text-align:left; line-height:25px; color:#4d4b4b; '>
					<div style='font-size:22px; text-align:center; padding-bottom:10px; '> Newsletter <i class="fas fa-envelope-open-text"></i></div>
					
					
					
					<div class="row" style="display:flex; justify-content: end;">
						<div class="col-sm-6" style="align-self:center;"><input type="text" name="email" style="width:100%; color: #bbbbbb; background-color:transparent; border: none; border-bottom: 1px solid #ff6600;" value="<?php if (isset($_smarty_tpl->tpl_vars['value']->value)&&$_smarty_tpl->tpl_vars['value']->value) {?><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Inserisci la tua e-mail','mod'=>'blocknewsletter'),$_smarty_tpl);?>
<?php }?>" onfocus="javascript:if(this.value=='<?php echo smartyTranslate(array('s'=>'Inserisci la tua e-mail','mod'=>'blocknewsletter'),$_smarty_tpl);?>
')this.value='';" onblur="javascript:if(this.value=='')this.value='<?php echo smartyTranslate(array('s'=>'Inserisci la tua e-mail','mod'=>'blocknewsletter'),$_smarty_tpl);?>
';" /><br /> </div>
						
						
					
						<div class="col-sm-4"> <button type="submit" class="button" style="width:50%; padding:7px; border:none; line-height:normal; background-color:#ff6600; color: white;" name="submitNewsletter">Iscriviti </button></div>
					
					</div>
					<div style="clear:both"></div>
					<div class="row" style="display: flex; justify-content: center; padding-top: 15px; align-items:center;">
						<input type="hidden" name="action" value="0" /><input type="checkbox" name="privacy_newsletter" />
						<a href="#" onclick="window.open('<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('privacy.php',true);?>
')" style="text-decoration:none; color:#4d4b4b; font-size: 12px; padding-left:5px;">Acconsento al trattamento dei dati</a><br />
					</div>
					
						
					
					
				</div>

				
			</form>
		
			
		
	</div>
</div>

<!-- /Block Newsletter module-->

<?php }?>

<!--prova I nostri marchi -->




<!-- End prova I nostri marchi -->




<div id="footer-in">


	
	
<br />
<?php if ($_smarty_tpl->tpl_vars['page_name']->value=='index') {?>
		
		
			<p style='text-align:center; background-color:#ffffff; padding:3px; '>
		<?php echo smartyTranslate(array('s'=>'I nostri prodotti in ordine alfabetico'),$_smarty_tpl);?>
<br />
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_a/" >A</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_b/" >B</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_c/" >C</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_d/" >D</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_e/" >E</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_f/" >F</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_g/" >G</a> - 
		H - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_i/" >I</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_j/" >J</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_k/" >K</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_l/" >L</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_m/" >M</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_n/" >N</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_o/" >O</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_p_1/" >PA-PK</a> - 
		<a style='padding:2px;' href="https://www.ezdirect.it/lettera_p_2/" >PL-PZ</a> - 
		Q - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_r/" >R</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_s/" >S</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_t/" >T</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_u/" >U</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_v/" >V</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_w/" >W</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_x/" >X</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_y/" >Y</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_z/" >Z</a> - 
		</p>
			<br />

			
	<h4 class="homepage" style="font-size:16px; color: darkgrey; margin-top:0; "><i class="fas fa-hand-point-right"></i><a id="guide-e-consigli" href='https://www.ezdirect.it/guide/category/5-guide-e-consigli' target='_blank'>Guide e consigli all'acquisto</a><i class="fas fa-hand-point-left"></i></h4>

<?php }?>

			<hr class="footer_hr" />
			<br />
			<div style="width: 100%; position:relative">



	



 <table id="footer-table-blocks" style="table-layout:fixed;">
  <tbody><tr style="display: flex;" id="footer-tab-responsive">

	<div class="container">
		<td class="tdfooter">
			<h3>Azienda</h3>
			<ul class="menufooter">
				<li><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink2(4,$_smarty_tpl->tpl_vars['link']->value->getCMSRewrite(4));?>
" title="Chi siamo">Chi siamo</a></li>
				<li><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink2(39,$_smarty_tpl->tpl_vars['link']->value->getCMSRewrite(39));?>
" title="Contatti">Contatti</a></li>
				<li><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink2(3,$_smarty_tpl->tpl_vars['link']->value->getCMSRewrite(3));?>
" title="Condizioni">Condizioni</a></li>
				<li><a target="_blank" href="https://www.ezdirect.it/sitemap.php" title="Sitemap">Sitemap</a></li>
				<li><a target="_blank" href="https://www.ezdirect.it/guide/6-informativa-sulla-privacy" title="Sitemap">Privacy</a></li>
				<li><a target="_blank" href="https://www.ezdirect.it/ti-richiamiamo-noi" title="Sitemap">Consulente dedicato</a></li>
				<li><a target="_blank" href="https://www.ezdirect.it/guide/69-ezdirect-azienda-accreditata-mepa" title="Sitemap">Acquisti in rete</a></li>
   			</ul>
		</td>
    	<td class="tdfooter">
			<h3>Pagamenti</h3>
			<ul class="menufooter">
				<li><a rel="nofollow" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink2(3,$_smarty_tpl->tpl_vars['link']->value->getCMSRewrite(3));?>
#contrassegno" title="Contrassegno">Contrassegno</a></li>
				<li><a rel="nofollow" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink2(3,$_smarty_tpl->tpl_vars['link']->value->getCMSRewrite(3));?>
#bonifico" title="Bonifico all'ordine">Bonifico all'ordine</a></li>
				<li><a rel="nofollow" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink2(3,$_smarty_tpl->tpl_vars['link']->value->getCMSRewrite(3));?>
#paypal" title="Paypal">Paypal</a></li>
				<li><a rel="nofollow" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink2(3,$_smarty_tpl->tpl_vars['link']->value->getCMSRewrite(3));?>
#noleggio" title="Noleggio Leasing">Noleggio Leasing</a></li>
				<li><a rel="nofollow" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink2(3,$_smarty_tpl->tpl_vars['link']->value->getCMSRewrite(3));?>
#prezzi" title="Prezzi">Prezzi</a></li>
    		</ul>
		</td>
    	<td class="tdfooter">
			<h3 style="width:100%;">Ordini e consegne</h3>
				<ul class="menufooter">
					<li><a rel="nofollow" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink2(3,$_smarty_tpl->tpl_vars['link']->value->getCMSRewrite(3));?>
#consegne" title="Spedizione">Spedizione</a></li>
					<li><a rel="nofollow" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink2(3,$_smarty_tpl->tpl_vars['link']->value->getCMSRewrite(3));?>
#consegne" title="Modalita di consegna">Modalit&agrave;&nbsp;di consegna</a></li>
					<li><a rel="nofollow" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink2(3,$_smarty_tpl->tpl_vars['link']->value->getCMSRewrite(3));?>
#consegne" title="Spese di consegna">Spese di consegna</a></li>
					<li><a rel="nofollow" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink2(3,$_smarty_tpl->tpl_vars['link']->value->getCMSRewrite(3));?>
#consegne" title="Fatturazione">Fatturazione</a></li>
					<li><a rel="nofollow" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink2(3,$_smarty_tpl->tpl_vars['link']->value->getCMSRewrite(3));?>
#consegne" title="Fatturazione">RMA + Diritto di recesso</a></li>
					
				</ul>
		</td>
    	<td class="tdfooter">
			<div style="">
				<h3>Servizi</h3>
	
				<ul class="menufooter">
					<li><a rel="nofollow" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink2(3,$_smarty_tpl->tpl_vars['link']->value->getCMSRewrite(3));?>
#supporto" title="Supporto pre vendita">Supporto pre vendita</a></li>
					<li><a rel="nofollow" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink2(3,$_smarty_tpl->tpl_vars['link']->value->getCMSRewrite(3));?>
#assistenza" title="Assistenza tecnica">Assistenza tecnica</a></li>
					<li><a rel="nofollow" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCMSLink2(3,$_smarty_tpl->tpl_vars['link']->value->getCMSRewrite(3));?>
#rma" title="Riparazioni">Riparazioni</a></li>
					<li><a href="https://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini" title="Preventivi">Preventivi</a></li>
					<li><a rel="nofollow" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('authentication.php',true);?>
?cliente=rivenditore">Rivenditori</a></li>
				</ul>
			</div>
		</td>
    </div>
	 	
		 
	<td class="tdfooter" style="vertical-align:middle; padding-top: 50px;">
	
   
       
	   <!-- eKomiWidget START -->
<div id="eKomiWidget_default"></div>
<!-- eKomiWidget END -->

<!-- eKomiLoader START, only needed once per page -->
<script type="text/javascript"> 

	(function(){
		eKomiIntegrationConfig = new Array(
			{certId:'44D97E5AAD9DE69'}
		);
		if(typeof eKomiIntegrationConfig != "undefined"){for(var eKomiIntegrationLoop=0;eKomiIntegrationLoop<eKomiIntegrationConfig.length;eKomiIntegrationLoop++){
			var eKomiIntegrationContainer = document.createElement('script');
			eKomiIntegrationContainer.type = 'text/javascript'; eKomiIntegrationContainer.defer = true;
			eKomiIntegrationContainer.src = (document.location.protocol=='https:'?'https:':'http:') +"//connect.ekomi.de/integration_1609180206/" + eKomiIntegrationConfig[eKomiIntegrationLoop].certId + ".js";
			document.getElementsByTagName("head")[0].appendChild(eKomiIntegrationContainer);
		}}else{if('console' in window){ console.error('connectEkomiIntegration - Cannot read eKomiIntegrationConfig'); }}
	})();

</script>
<!-- eKomiLoader END, only needed once per page -->

	   
	   </td>
	
  </tr>
</tbody></table>
<br />
			<hr class="footer_hr" />
			<br />

	<div style="padding-bottom: 20px;">
		<div class="row" style="display: flex; align-items: center; flex-wrap: wrap; justify-content: center;">
			<div class="col-sm-6">
				<div id="carte-ssl">
	
				<img  src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
carte-footer-new.png' alt='Carte' title='Carte' />

				<img  style="height: 28px;" src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
gls-logo.png' alt='Corriere GLS' title='Corriere GLS' />

				<span style=" height:40px;"><img id="pagamento-ssl" style="height:100%" src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
pagamento-sicuro.jpeg' alt='Pagamento sicuro' title='Pagamento sicuro' /></span>
	
				</div>
			</div>

			<div class="col-sm-3">
				<div id="servizio-clienti" style="text-align:end;">
						
				<?php if (isset($_GET['id_cms'])&&$_GET['id_cms']==37) {?>
				<?php } else { ?>
				<a href="tel:+39-0585821163">
								
					<img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
esperto-risponde.jpg" alt="<?php echo smartyTranslate(array('s'=>'Servizio clienti'),$_smarty_tpl);?>
" style="border:0px; margin-bottom: 5px;" title="<?php echo smartyTranslate(array('s'=>'Servizio clienti'),$_smarty_tpl);?>
 0585 821163" /> 
								
				</a>
				<?php }?>
				</div>
			</div>

			<div class="col-sm-3">
				<div style="display: flex; align-items: stretch; justify-content: center; height: 38px;">
	
					
	
					<a href='https://it-it.facebook.com/pages/Ezdirect-srl/53805768063' target='_blank' title='Facebook' rel='nofollow'><i style="    color: #f47a3c; font-size: 23px; padding: 7px;"  class="fab fa-facebook"></i></a>
					<a href='https://twitter.com/ezdirect_it' target='_blank'  title='Twitter' rel='nofollow'><i style="    color: #f47a3c; font-size: 23px; padding: 7px;" class="fab fa-twitter"></i></a>
					<a href='http://www.linkedin.com/company/ezdirect-srl' target='_blank'  title='Linkedin' rel='nofollow'><i style="    color: #f47a3c; font-size: 23px; padding: 7px;" class="fab fa-linkedin"></i></a>
					<a href='https://www.youtube.com/user/ezdirectit' target='_blank'  title='Youtube' rel='nofollow'><i style="    color: #f47a3c; font-size: 23px; padding: 7px;" class="fab fa-youtube"></i></a>
				</div>
			</div>
		</div>
	</div>
	

	
	
	
</div>


</div>
			
</div>	<!-- fine footer -->

<div id="footer-2">
	<div id="footer-2-in">
			
			<div id="footer-bottom">
				
				<div>
					<p style="font-size: 13px; font-family: arial; margin:0;">&copy; Ezdirect srl. P. IVA CCIAA 01164670455 - Via Nerino Garbuio snc, 54038, Montignoso, MS - Tel. 0585 821163 - SDI: USAL8PV<br />
					
					 
					</p><br />
				</div>

			</div>
			<div style="clear:both"></div>
		</div>	
	</div>	
			
			
			
	
	
<!--	<?php echo $_smarty_tpl->tpl_vars['footer_mobile']->value;?>
 -->
	
	<a href="#0" class="to-top" title="Back To Top"><div class="back-to-top-arrow-up"></div></a>
	
	
		
	</body>
</html>

<?php } else { ?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./landing_footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php }?>

<?php }} ?>
