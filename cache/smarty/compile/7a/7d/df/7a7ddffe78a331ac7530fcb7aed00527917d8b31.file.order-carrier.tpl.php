<?php /* Smarty version Smarty-3.1.19, created on 2021-11-16 15:07:47
         compiled from "/var/www/html/themes/ez20/order-carrier.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11012987546193bb3399dc65-53958434%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7a7ddffe78a331ac7530fcb7aed00527917d8b31' => 
    array (
      0 => '/var/www/html/themes/ez20/order-carrier.tpl',
      1 => 1636107486,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11012987546193bb3399dc65-53958434',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'opc' => 0,
    'currencySign' => 0,
    'currencyRate' => 0,
    'currencyFormat' => 0,
    'currencyBlank' => 0,
    'virtual_cart' => 0,
    'giftAllowed' => 0,
    'cart' => 0,
    'link' => 0,
    'conditions' => 0,
    'cms_id' => 0,
    'checkedTOS' => 0,
    'carriers' => 0,
    'HOOK_BEFORECARRIER' => 0,
    'isVirtualCart' => 0,
    'recyclablePackAllowed' => 0,
    'recyclable' => 0,
    'carrier' => 0,
    'isLogged' => 0,
    'checked' => 0,
    'priceDisplay' => 0,
    'use_taxes' => 0,
    'HOOK_EXTRACARRIER' => 0,
    'gift_wrapping_price' => 0,
    'total_wrapping_tax_exc_cost' => 0,
    'total_wrapping_cost' => 0,
    'back' => 0,
    'is_guest' => 0,
    'oldMessage' => 0,
    'img_ps_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_6193bb339cc362_56116734',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6193bb339cc362_56116734')) {function content_6193bb339cc362_56116734($_smarty_tpl) {?>

<?php if (!$_smarty_tpl->tpl_vars['opc']->value) {?>
	<script type="text/javascript">
	//<![CDATA[
		var orderProcess = 'order';
		var currencySign = '<?php echo html_entity_decode($_smarty_tpl->tpl_vars['currencySign']->value,2,"UTF-8");?>
';
		var currencyRate = '<?php echo floatval($_smarty_tpl->tpl_vars['currencyRate']->value);?>
';
		var currencyFormat = '<?php echo intval($_smarty_tpl->tpl_vars['currencyFormat']->value);?>
';
		var currencyBlank = '<?php echo intval($_smarty_tpl->tpl_vars['currencyBlank']->value);?>
';
		var txtProduct = "<?php echo smartyTranslate(array('s'=>'product'),$_smarty_tpl);?>
";
		var txtProducts = "<?php echo smartyTranslate(array('s'=>'products'),$_smarty_tpl);?>
";

		var msg = "<?php echo smartyTranslate(array('s'=>'You must agree to the terms of service before continuing.','js'=>1),$_smarty_tpl);?>
";
		
		function acceptCGV()
		{
			if ($('#cgv').length && !$('input#cgv:checked').length)
			{
				alert(msg);
				return false;
			}
			else
				return true;
		}
		
	//]]>
	</script>
<?php } else { ?>
	<script type="text/javascript">
		var txtFree = "<?php echo smartyTranslate(array('s'=>'Free!'),$_smarty_tpl);?>
";
	</script>
<?php }?>

<?php if (!$_smarty_tpl->tpl_vars['virtual_cart']->value&&$_smarty_tpl->tpl_vars['giftAllowed']->value&&$_smarty_tpl->tpl_vars['cart']->value->gift==1) {?>
<script type="text/javascript">

// <![CDATA[
    $('document').ready( function(){
		if ($('input#gift').is(':checked'))
			$('p#gift_div').show();
    });
//]]>

</script>
<?php }?>

<?php if (!$_smarty_tpl->tpl_vars['opc']->value) {?>
<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'Shipping'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php }?>

<?php if (!$_smarty_tpl->tpl_vars['opc']->value) {?><h1><?php echo smartyTranslate(array('s'=>'Shipping'),$_smarty_tpl);?>
</h1><?php } else { ?><h2>2. <?php echo smartyTranslate(array('s'=>'Delivery methods'),$_smarty_tpl);?>
</h2><?php }?>

<?php if (!$_smarty_tpl->tpl_vars['opc']->value) {?>
<?php $_smarty_tpl->tpl_vars['current_step'] = new Smarty_variable('shipping', null, 0);?>


<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./order-steps.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<div id="address-form-short">
<div class="shopping_cart_table">
<form id="form" action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('order.php',true);?>
" method="post" onsubmit="return acceptCGV();">
<?php } else { ?>
<div id="opc_delivery_methods" class="opc-main-block">
	<div id="opc_delivery_methods-overlay" class="opc-overlay" style="display: none;"></div>
<?php }?>

<div style="margin-bottom: 35px;">
<?php if ($_smarty_tpl->tpl_vars['conditions']->value&&$_smarty_tpl->tpl_vars['cms_id']->value) {?>
	<h3 class="condition_title"><?php echo smartyTranslate(array('s'=>'Terms of service'),$_smarty_tpl);?>
</h3>
	<p class="checkbox" id="conditions">
		<input style="margin-left: 0;" type="checkbox" name="cgv" id="cgv" value="1" <?php if ($_smarty_tpl->tpl_vars['checkedTOS']->value) {?>checked="checked"<?php }?> />
		<label for="cgv"><?php echo smartyTranslate(array('s'=>'I agree to the terms of service and adhere to them unconditionally.'),$_smarty_tpl);?>
<a href="cms.php?id_cms=3" target="_blank"><?php echo smartyTranslate(array('s'=>'(read)'),$_smarty_tpl);?>
</a></label> 
	</p>
	<script type="text/javascript">$('a.iframe').fancybox();</script>
<?php }?>
</div>

<div style="margin-bottom: 35px;">
<?php if ($_smarty_tpl->tpl_vars['virtual_cart']->value) {?>
	<input id="input_virtual_carrier" class="hidden" type="hidden" name="id_carrier" value="0" />
<?php } else { ?>
	<h3 class="carrier_title"><?php echo smartyTranslate(array('s'=>'Choose your delivery method'),$_smarty_tpl);?>
</h3>

	<div id="HOOK_BEFORECARRIER"><?php if (isset($_smarty_tpl->tpl_vars['carriers']->value)) {?><?php echo $_smarty_tpl->tpl_vars['HOOK_BEFORECARRIER']->value;?>
<?php }?></div>
	<?php if (isset($_smarty_tpl->tpl_vars['isVirtualCart']->value)&&$_smarty_tpl->tpl_vars['isVirtualCart']->value) {?>
	<p class="warning"><?php echo smartyTranslate(array('s'=>'No carrier needed for this order'),$_smarty_tpl);?>
</p>
	<?php } else { ?>
	<?php if ($_smarty_tpl->tpl_vars['recyclablePackAllowed']->value) {?>
	<p class="checkbox">
		<input type="checkbox" name="recyclable" id="recyclable" value="1" <?php if ($_smarty_tpl->tpl_vars['recyclable']->value==1) {?>checked="checked"<?php }?> />
		<label for="recyclable"><?php echo smartyTranslate(array('s'=>'I agree to receive my order in recycled packaging'),$_smarty_tpl);?>
.</label>
	</p>
	<?php }?>
	<p class="warning" id="noCarrierWarning" <?php if (isset($_smarty_tpl->tpl_vars['carriers']->value)&&$_smarty_tpl->tpl_vars['carriers']->value&&count($_smarty_tpl->tpl_vars['carriers']->value)) {?>style="display:none;"<?php }?>><?php echo smartyTranslate(array('s'=>'There are no carriers available that deliver to this address.'),$_smarty_tpl);?>
</p>
	<table id="carrierTable" class="std" <?php if (!isset($_smarty_tpl->tpl_vars['carriers']->value)||!$_smarty_tpl->tpl_vars['carriers']->value||!count($_smarty_tpl->tpl_vars['carriers']->value)) {?>style="display:none;"<?php }?>>
		<thead>
			<tr>
				<th class="carrier_action first_item"></th>
				<th class="carrier_name item"><?php echo smartyTranslate(array('s'=>'Carrier'),$_smarty_tpl);?>
</th>
				<th class="carrier_infos item"><?php echo smartyTranslate(array('s'=>'Information'),$_smarty_tpl);?>
</th>
				<th class="carrier_price last_item"><?php echo smartyTranslate(array('s'=>'Price'),$_smarty_tpl);?>
</th>
			</tr>
		</thead>
		<tbody>
		<?php if (isset($_smarty_tpl->tpl_vars['carriers']->value)) {?>
			<?php  $_smarty_tpl->tpl_vars['carrier'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['carrier']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['carriers']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['carrier']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['carrier']->iteration=0;
 $_smarty_tpl->tpl_vars['carrier']->index=-1;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['myLoop']['index']=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['carrier']->key => $_smarty_tpl->tpl_vars['carrier']->value) {
$_smarty_tpl->tpl_vars['carrier']->_loop = true;
 $_smarty_tpl->tpl_vars['carrier']->iteration++;
 $_smarty_tpl->tpl_vars['carrier']->index++;
 $_smarty_tpl->tpl_vars['carrier']->first = $_smarty_tpl->tpl_vars['carrier']->index === 0;
 $_smarty_tpl->tpl_vars['carrier']->last = $_smarty_tpl->tpl_vars['carrier']->iteration === $_smarty_tpl->tpl_vars['carrier']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['myLoop']['first'] = $_smarty_tpl->tpl_vars['carrier']->first;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['myLoop']['index']++;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['myLoop']['last'] = $_smarty_tpl->tpl_vars['carrier']->last;
?>
				<tr class="<?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['myLoop']['first']) {?>first_item<?php } elseif ($_smarty_tpl->getVariable('smarty')->value['foreach']['myLoop']['last']) {?>last_item<?php }?> <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['myLoop']['index']%2) {?>alternate_item<?php } else { ?>item<?php }?>">
					<td class="carrier_action radio">
						<input style="margin-left: 0;" type="radio" name="id_carrier" value="<?php echo intval($_smarty_tpl->tpl_vars['carrier']->value['id_carrier']);?>
" id="id_carrier<?php echo intval($_smarty_tpl->tpl_vars['carrier']->value['id_carrier']);?>
"  <?php if ($_smarty_tpl->tpl_vars['opc']->value) {?>onclick="updateCarrierSelectionAndGift();"<?php }?> <?php if (!($_smarty_tpl->tpl_vars['carrier']->value['is_module']&&$_smarty_tpl->tpl_vars['opc']->value&&!$_smarty_tpl->tpl_vars['isLogged']->value)) {?><?php if ($_smarty_tpl->tpl_vars['carrier']->value['id_carrier']==$_smarty_tpl->tpl_vars['checked']->value) {?>checked="checked"<?php }?><?php } else { ?>disabled="disabled"<?php }?> />
					</td>
					<td class="carrier_name">
						<label style="margin-left: 20px; margin-top: 12px;" for="id_carrier<?php echo intval($_smarty_tpl->tpl_vars['carrier']->value['id_carrier']);?>
">
							<?php if ($_smarty_tpl->tpl_vars['carrier']->value['img']) {?><img src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['carrier']->value['img'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['carrier']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" /><?php } else { ?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['carrier']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>
						</label>
					</td>
					<td class="carrier_infos"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['carrier']->value['delay'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
					<td class="carrier_price">
						<?php if ($_smarty_tpl->tpl_vars['carrier']->value['price']) {?>
							<span class="price">
								<?php if ($_smarty_tpl->tpl_vars['priceDisplay']->value==1) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['carrier']->value['price_tax_exc']),$_smarty_tpl);?>
<?php } else { ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['carrier']->value['price']),$_smarty_tpl);?>
<?php }?>
							</span>
							<?php if ($_smarty_tpl->tpl_vars['use_taxes']->value) {?><?php if ($_smarty_tpl->tpl_vars['priceDisplay']->value==1) {?> <?php echo smartyTranslate(array('s'=>'(tax excl.)'),$_smarty_tpl);?>
<?php } else { ?> <?php echo smartyTranslate(array('s'=>'(tax incl.)'),$_smarty_tpl);?>
<?php }?><?php }?>
						<?php } else { ?>
							<?php echo smartyTranslate(array('s'=>'Free!'),$_smarty_tpl);?>

						<?php }?>
					</td>
				</tr>
			<?php } ?>
			<tr id="HOOK_EXTRACARRIER"><?php echo $_smarty_tpl->tpl_vars['HOOK_EXTRACARRIER']->value;?>
</tr>
		<?php }?>
		</tbody>
	</table>
	<div style="display: none;" id="extra_carrier"></div>

		<?php if ($_smarty_tpl->tpl_vars['giftAllowed']->value) {?>
		<h3 class="gift_title"><?php echo smartyTranslate(array('s'=>'Gift'),$_smarty_tpl);?>
</h3>
		<p class="checkbox">
			<input type="checkbox" name="gift" id="gift" value="1" <?php if ($_smarty_tpl->tpl_vars['cart']->value->gift==1) {?>checked="checked"<?php }?> onclick="$('#gift_div').toggle('slow');" />
			<label for="gift"><?php echo smartyTranslate(array('s'=>'I would like the order to be gift-wrapped.'),$_smarty_tpl);?>
</label>
			<br />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<?php if ($_smarty_tpl->tpl_vars['gift_wrapping_price']->value>0) {?>
				(<?php echo smartyTranslate(array('s'=>'Additional cost of'),$_smarty_tpl);?>

				<span class="price" id="gift-price">
					<?php if ($_smarty_tpl->tpl_vars['priceDisplay']->value==1) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['total_wrapping_tax_exc_cost']->value),$_smarty_tpl);?>
<?php } else { ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['total_wrapping_cost']->value),$_smarty_tpl);?>
<?php }?>
				</span>
				<?php if ($_smarty_tpl->tpl_vars['use_taxes']->value) {?><?php if ($_smarty_tpl->tpl_vars['priceDisplay']->value==1) {?> <?php echo smartyTranslate(array('s'=>'(tax excl.)'),$_smarty_tpl);?>
<?php } else { ?> <?php echo smartyTranslate(array('s'=>'(tax incl.)'),$_smarty_tpl);?>
<?php }?><?php }?>)
			<?php }?>
		</p>
		<p id="gift_div" class="textarea">
			<label for="gift_message"><?php echo smartyTranslate(array('s'=>'If you wish, you can add a note to the gift:'),$_smarty_tpl);?>
</label>
			<textarea rows="5" cols="35" id="gift_message" name="gift_message"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value->gift_message, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</textarea>
		</p>
		<?php }?>
	<?php }?>
<?php }?>
</div>

<?php if (!$_smarty_tpl->tpl_vars['opc']->value) {?>
	<p class="cart_navigation submit" id="prev-next-carrier">
		<input type="hidden" name="step" value="3" />
		<input type="hidden" name="back" value="<?php echo $_smarty_tpl->tpl_vars['back']->value;?>
" />
		<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('order.php',true);?>
<?php if (!$_smarty_tpl->tpl_vars['is_guest']->value) {?>?step=1<?php if ($_smarty_tpl->tpl_vars['back']->value) {?>&back=<?php echo $_smarty_tpl->tpl_vars['back']->value;?>
<?php }?><?php }?>" title="<?php echo smartyTranslate(array('s'=>'Previous'),$_smarty_tpl);?>
"  class="button_large" style="padding: 8px 20px; height: auto;">&laquo; <?php echo smartyTranslate(array('s'=>'Previous'),$_smarty_tpl);?>
</a>
		<input type="submit" name="processCarrier" value="<?php echo smartyTranslate(array('s'=>'Next'),$_smarty_tpl);?>
 &raquo;" class="exclusive" style="padding: 8px 20px; height: auto;" />
	</p>
	<br /><br />
</form>
<?php } else { ?>
	<h3><?php echo smartyTranslate(array('s'=>'Leave a message'),$_smarty_tpl);?>
</h3>
	<div>
		<p><?php echo smartyTranslate(array('s'=>'If you would like to add a comment about your order, please write it below.'),$_smarty_tpl);?>
</p>
		<p><textarea cols="120" rows="3" name="message" id="message"><?php if (isset($_smarty_tpl->tpl_vars['oldMessage']->value)) {?><?php echo $_smarty_tpl->tpl_vars['oldMessage']->value;?>
<?php }?></textarea></p>
	</div>
</div>
<br /><br />
</div>
<?php }?>


</div>

</div>
<br />
<div style="border: 1px solid #e5e8eb; 
			box-shadow: 0 1px 1px rgba(0,0,0,0.15), 
        	0 2px 2px rgba(0,0,0,0.15), 
            0 4px 4px rgba(0,0,0,0.15), 
        	0 8px 8px rgba(0,0,0,0.15);
			border-radius:12px;
			padding:30px; 
			display: flex;
			width: 100%;" id="need-help">
		<div style="height: 140px; padding-right: 30px;">
			<img style="height:100%;" src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
business-woman.jpg' alt='Aiuto' title='Aiuto' />
		</div>
		<div>
			<h5 style="font-size: 22px; font-weight: 600;">Hai bisogno di aiuto con il tuo ordine?</h5>
			<div style="display:flex; align-items:center;" id="numero-verde"><p>Chiamaci al numero verde</p><a href="#" style="padding-left: 12px; padding-bottom: 10px;"><img style="height:50px;" src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
numero-verde.jpg' alt='Numero verde' /></a></div>
			
			<p>Oppure apri un ticket cliccando <a target='_blank' href='https://www.ezdirect.it/contattaci?step=assistenza-ordini'>qui</a>. Un esperto di risponderà quanto prima.</p>
		</div>
</div>

<p class="clear"></p>
<?php }} ?>
