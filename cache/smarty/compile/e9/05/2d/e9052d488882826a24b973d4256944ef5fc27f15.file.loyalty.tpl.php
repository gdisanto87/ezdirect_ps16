<?php /* Smarty version Smarty-3.1.19, created on 2021-12-06 16:27:15
         compiled from "/var/www/html/modules/loyalty/loyalty.tpl" */ ?>
<?php /*%%SmartyHeaderCode:210407825161ae2bd3825d87-27332966%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e9052d488882826a24b973d4256944ef5fc27f15' => 
    array (
      0 => '/var/www/html/modules/loyalty/loyalty.tpl',
      1 => 1638264471,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '210407825161ae2bd3825d87-27332966',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'navigationPipe' => 0,
    'id_group' => 0,
    'orders' => 0,
    'totalPoints' => 0,
    'displayorders' => 0,
    'order' => 0,
    'nbpagination' => 0,
    'page' => 0,
    'pagination_link' => 0,
    'p_previous' => 0,
    'max_page' => 0,
    'p_next' => 0,
    'nArray' => 0,
    'nValue' => 0,
    'categories' => 0,
    'transformation_allowed' => 0,
    'voucher' => 0,
    'nbDiscounts' => 0,
    'discounts' => 0,
    'discount' => 0,
    'myorder' => 0,
    'minimalLoyalty' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61ae2bd3861551_27668836',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61ae2bd3861551_27668836')) {function content_61ae2bd3861551_27668836($_smarty_tpl) {?>

<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account.php',true);?>
"><?php echo smartyTranslate(array('s'=>'My account','mod'=>'loyalty'),$_smarty_tpl);?>
</a><span class="navigation-pipe"><?php echo $_smarty_tpl->tpl_vars['navigationPipe']->value;?>
</span><?php echo smartyTranslate(array('s'=>'My loyalty points','mod'=>'loyalty'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php if ($_smarty_tpl->tpl_vars['id_group']->value!=3&&$_smarty_tpl->tpl_vars['id_group']->value!=15) {?> 
<br />
<h1 style='text-align:left'><?php echo smartyTranslate(array('s'=>'My loyalty points','mod'=>'loyalty'),$_smarty_tpl);?>
</h1>
<br />
<?php echo smartyTranslate(array('s'=>'Here you can find your loyalty points. Your points must be convalidated before you can convert it to coupon codes. Your points will be convalidated when the products you have bought will be shipped. If the order will be canceled, the loyalty points will be canceled too. ','mod'=>'loyalty'),$_smarty_tpl);?>
<br /><br />
<?php echo smartyTranslate(array('s'=>'You must earn points for a total of 5 euros in order to convert them. When your points will be convalidated, you can click on "Transform my points into a voucher code" to convert them. As soon as you have clicked, you will se your coupon in the box below. A coupon has a 30 days validity and you will able to use it on your next order. You must only insert the coupon code (eg. FID****) in the cart process.','mod'=>'loyalty'),$_smarty_tpl);?>
 

<?php if ($_smarty_tpl->tpl_vars['orders']->value) {?>
<div class="block-center" id="block-history">
	<?php if ($_smarty_tpl->tpl_vars['orders']->value&&count($_smarty_tpl->tpl_vars['orders']->value)) {?>
	<br /><br />
	<table id="order-list" class="std2">
		<thead>
			<tr>
				<th class="first_item" style="background-color:#8899cc"><?php echo smartyTranslate(array('s'=>'Order','mod'=>'loyalty'),$_smarty_tpl);?>
</th>
				<th class="item" style="background-color:#99aadd"><?php echo smartyTranslate(array('s'=>'Date','mod'=>'loyalty'),$_smarty_tpl);?>
</th>
				<th class="item" style="background-color:#8899cc"><?php echo smartyTranslate(array('s'=>'Points','mod'=>'loyalty'),$_smarty_tpl);?>
</th>
				<th class="last_item" style="background-color:#99aadd"><?php echo smartyTranslate(array('s'=>'Points Status','mod'=>'loyalty'),$_smarty_tpl);?>
</th>
			</tr>
		</thead>
		<tfoot>
			<tr class="alternate_item">
				<td colspan="2" class="history_method bold" style="text-align:center;"><?php echo smartyTranslate(array('s'=>'Total points available:','mod'=>'loyalty'),$_smarty_tpl);?>
</td>
				<td class="history_method" style="text-align:left;"><?php echo intval($_smarty_tpl->tpl_vars['totalPoints']->value);?>
</td>
				<td class="history_method">&nbsp;</td>
			</tr>
		</tfoot>
		<tbody>
		<?php  $_smarty_tpl->tpl_vars['order'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['order']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['displayorders']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['order']->key => $_smarty_tpl->tpl_vars['order']->value) {
$_smarty_tpl->tpl_vars['order']->_loop = true;
?>
			<tr class="alternate_item">
				<td class="history_link bold"><?php echo smartyTranslate(array('s'=>'#','mod'=>'loyalty'),$_smarty_tpl);?>
<?php echo sprintf("%06d",$_smarty_tpl->tpl_vars['order']->value['id']);?>
</td>
				<td class="history_date"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0][0]->dateFormat(array('date'=>$_smarty_tpl->tpl_vars['order']->value['date'],'full'=>1),$_smarty_tpl);?>
</td>
				<td class="history_method" style='text-align:center'><?php echo intval($_smarty_tpl->tpl_vars['order']->value['points']);?>
</td>
				<td class="history_method" style='text-align:center'><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['order']->value['state'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
	<div id="block-order-detail" class="hidden">&nbsp;</div>
	<?php } else { ?>
		<p class="warning"><?php echo smartyTranslate(array('s'=>'You have not placed any orders.'),$_smarty_tpl);?>
</p>
	<?php }?>
</div>
<div id="pagination" class="pagination">
	<?php if ($_smarty_tpl->tpl_vars['nbpagination']->value<count($_smarty_tpl->tpl_vars['orders']->value)) {?>
		<ul class="pagination">
		<?php if ($_smarty_tpl->tpl_vars['page']->value!=1) {?>
			<?php $_smarty_tpl->tpl_vars['p_previous'] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value-1, null, 0);?>
			<li id="pagination_previous"><a href="<?php echo $_smarty_tpl->tpl_vars['pagination_link']->value;?>
?p=<?php echo $_smarty_tpl->tpl_vars['p_previous']->value;?>
&n=<?php echo $_smarty_tpl->tpl_vars['nbpagination']->value;?>
">
			&laquo;&nbsp;<?php echo smartyTranslate(array('s'=>'Previous','mod'=>'loyalty'),$_smarty_tpl);?>
</a></li>
		<?php } else { ?>
			<li id="pagination_previous" class="disabled"><span>&laquo;&nbsp;<?php echo smartyTranslate(array('s'=>'Previous','mod'=>'loyalty'),$_smarty_tpl);?>
</span></li>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['page']->value>2) {?>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['pagination_link']->value;?>
?p=1&n=<?php echo $_smarty_tpl->tpl_vars['nbpagination']->value;?>
">1</a></li>
			<?php if ($_smarty_tpl->tpl_vars['page']->value>3) {?>
				<li class="truncate">...</li>
			<?php }?>
		<?php }?>
		<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['pagination'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['name'] = 'pagination';
$_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['start'] = (int) $_smarty_tpl->tpl_vars['page']->value-1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['page']->value+2) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['step'] = ((int) 1) == 0 ? 1 : (int) 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['pagination']['total']);
?>
			<?php if ($_smarty_tpl->tpl_vars['page']->value==$_smarty_tpl->getVariable('smarty')->value['section']['pagination']['index']) {?>
				<li class="current"><span><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['page']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</span></li>
			<?php } elseif ($_smarty_tpl->getVariable('smarty')->value['section']['pagination']['index']>0&&count($_smarty_tpl->tpl_vars['orders']->value)+$_smarty_tpl->tpl_vars['nbpagination']->value>($_smarty_tpl->getVariable('smarty')->value['section']['pagination']['index'])*($_smarty_tpl->tpl_vars['nbpagination']->value)) {?>
				<li><a href="<?php echo $_smarty_tpl->tpl_vars['pagination_link']->value;?>
?p=<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['pagination']['index'];?>
&n=<?php echo $_smarty_tpl->tpl_vars['nbpagination']->value;?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->getVariable('smarty')->value['section']['pagination']['index'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</a></li>
			<?php }?>
		<?php endfor; endif; ?>
		<?php if ($_smarty_tpl->tpl_vars['max_page']->value-$_smarty_tpl->tpl_vars['page']->value>1) {?>
			<?php if ($_smarty_tpl->tpl_vars['max_page']->value-$_smarty_tpl->tpl_vars['page']->value>2) {?>
				<li class="truncate">...</li>
			<?php }?>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['pagination_link']->value;?>
?p=<?php echo $_smarty_tpl->tpl_vars['max_page']->value;?>
&n=<?php echo $_smarty_tpl->tpl_vars['nbpagination']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['max_page']->value;?>
</a></li>
		<?php }?>
		<?php if (count($_smarty_tpl->tpl_vars['orders']->value)>$_smarty_tpl->tpl_vars['page']->value*$_smarty_tpl->tpl_vars['nbpagination']->value) {?>
			<?php $_smarty_tpl->tpl_vars['p_next'] = new Smarty_variable($_smarty_tpl->tpl_vars['page']->value+1, null, 0);?>
			<li id="pagination_next"><a href="<?php echo $_smarty_tpl->tpl_vars['pagination_link']->value;?>
?p=<?php echo $_smarty_tpl->tpl_vars['p_next']->value;?>
&n=<?php echo $_smarty_tpl->tpl_vars['nbpagination']->value;?>
"><?php echo smartyTranslate(array('s'=>'Next','mod'=>'loyalty'),$_smarty_tpl);?>
&nbsp;&raquo;</a></li>
		<?php } else { ?>
			<li id="pagination_next" class="disabled"><span><?php echo smartyTranslate(array('s'=>'Next','mod'=>'loyalty'),$_smarty_tpl);?>
&nbsp;&raquo;</span></li>
		<?php }?>
		</ul>
	<?php }?>
	<?php if (count($_smarty_tpl->tpl_vars['orders']->value)>10) {?>
		<form action="<?php echo $_smarty_tpl->tpl_vars['pagination_link']->value;?>
" method="get" class="pagination">
			<p>
				<input type="submit" class="button_mini" value="<?php echo smartyTranslate(array('s'=>'OK'),$_smarty_tpl);?>
" />
				<label for="nb_item"><?php echo smartyTranslate(array('s'=>'items:','mod'=>'loyalty'),$_smarty_tpl);?>
</label>
				<select name="n" id="nb_item">
				<?php  $_smarty_tpl->tpl_vars['nValue'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['nValue']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['nArray']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['nValue']->key => $_smarty_tpl->tpl_vars['nValue']->value) {
$_smarty_tpl->tpl_vars['nValue']->_loop = true;
?>
					<?php if ($_smarty_tpl->tpl_vars['nValue']->value<=count($_smarty_tpl->tpl_vars['orders']->value)) {?>
						<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['nValue']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['nbpagination']->value==$_smarty_tpl->tpl_vars['nValue']->value) {?>selected="selected"<?php }?>><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['nValue']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
					<?php }?>
				<?php } ?>
				</select>
				<input type="hidden" name="p" value="1" />
			</p>
		</form>
	<?php }?>
	</div>

<br /><?php echo smartyTranslate(array('s'=>'Vouchers generated here are usable in the following categories : ','mod'=>'loyalty'),$_smarty_tpl);?>

<?php if ($_smarty_tpl->tpl_vars['categories']->value) {?><?php echo $_smarty_tpl->tpl_vars['categories']->value;?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'All','mod'=>'loyalty'),$_smarty_tpl);?>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['transformation_allowed']->value) {?>
<p style="text-align:center; margin-top:20px">
<?php if ($_smarty_tpl->tpl_vars['voucher']->value<5) {?>
<?php echo smartyTranslate(array('s'=>'You must earn at least a 5 euros coupon to convert it into a discount.','mod'=>'loyalty'),$_smarty_tpl);?>

<?php } else { ?>
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/loyalty/loyalty-program.php',true);?>
?transform-points=true" onclick="return confirm('<?php echo smartyTranslate(array('s'=>'Are you sure you want to transform your points into vouchers?','mod'=>'loyalty','js'=>1),$_smarty_tpl);?>
');"><?php echo smartyTranslate(array('s'=>'Transform my points into a voucher of','mod'=>'loyalty'),$_smarty_tpl);?>
 <span class="price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['voucher']->value),$_smarty_tpl);?>
</span>.</a>
<?php }?>
</p>
<?php }?>

<br /><br />
<h2><?php echo smartyTranslate(array('s'=>'My vouchers from loyalty points','mod'=>'loyalty'),$_smarty_tpl);?>
</h2>

<?php if ($_smarty_tpl->tpl_vars['nbDiscounts']->value) {?>
<div class="block-center" id="block-history">
<br /><br />
	<table id="order-list" class="std">
		<thead>
			<tr>
				<th class="first_item"><?php echo smartyTranslate(array('s'=>'Created','mod'=>'loyalty'),$_smarty_tpl);?>
</th>
				<th class="item"><?php echo smartyTranslate(array('s'=>'Value','mod'=>'loyalty'),$_smarty_tpl);?>
</th>
				<th class="item"><?php echo smartyTranslate(array('s'=>'Code','mod'=>'loyalty'),$_smarty_tpl);?>
</th>
				<th class="item"><?php echo smartyTranslate(array('s'=>'Valid from','mod'=>'loyalty'),$_smarty_tpl);?>
</th>
				<th class="item"><?php echo smartyTranslate(array('s'=>'Valid until','mod'=>'loyalty'),$_smarty_tpl);?>
</th>
				<th class="item"><?php echo smartyTranslate(array('s'=>'Status','mod'=>'loyalty'),$_smarty_tpl);?>
</th>
				<th class="last_item"><?php echo smartyTranslate(array('s'=>'Details','mod'=>'loyalty'),$_smarty_tpl);?>
</th>
			</tr>
		</thead>
		<tbody>
		<?php  $_smarty_tpl->tpl_vars['discount'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['discount']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['discounts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['discount']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['discount']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['discount']->key => $_smarty_tpl->tpl_vars['discount']->value) {
$_smarty_tpl->tpl_vars['discount']->_loop = true;
 $_smarty_tpl->tpl_vars['discount']->iteration++;
 $_smarty_tpl->tpl_vars['discount']->last = $_smarty_tpl->tpl_vars['discount']->iteration === $_smarty_tpl->tpl_vars['discount']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['myLoop']['last'] = $_smarty_tpl->tpl_vars['discount']->last;
?>
			<tr class="alternate_item">
				<td class="history_date"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0][0]->dateFormat(array('date'=>$_smarty_tpl->tpl_vars['discount']->value->date_add),$_smarty_tpl);?>
</td>
				<td class="history_price"><span class="price"><?php if ($_smarty_tpl->tpl_vars['discount']->value->id_discount_type==1) {?>
						<?php echo $_smarty_tpl->tpl_vars['discount']->value->value;?>
%
					<?php } elseif ($_smarty_tpl->tpl_vars['discount']->value->id_discount_type==2) {?>
						<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['discount']->value->value,'currency'=>$_smarty_tpl->tpl_vars['discount']->value->id_currency),$_smarty_tpl);?>

					<?php } else { ?>
						<?php echo smartyTranslate(array('s'=>'Free shipping','mod'=>'loyalty'),$_smarty_tpl);?>

					<?php }?></span></td>
				<td class="history_method bold"><?php echo $_smarty_tpl->tpl_vars['discount']->value->name;?>
</td>
				<td class="history_date"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0][0]->dateFormat(array('date'=>$_smarty_tpl->tpl_vars['discount']->value->date_from),$_smarty_tpl);?>
</td>
				<td class="history_date"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0][0]->dateFormat(array('date'=>$_smarty_tpl->tpl_vars['discount']->value->date_to),$_smarty_tpl);?>
</td>
				<td class="history_method bold"><?php if ($_smarty_tpl->tpl_vars['discount']->value->quantity>0) {?><?php echo smartyTranslate(array('s'=>'To use','mod'=>'loyalty'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Used','mod'=>'loyalty'),$_smarty_tpl);?>
<?php }?></td>
				<td class="history_method"><a href="<?php echo $_SERVER['SCRIPT_NAME'];?>
" onclick="return false" class="tips" title="<?php echo smartyTranslate(array('s'=>'Generated by these following orders','mod'=>'loyalty'),$_smarty_tpl);?>
|<?php  $_smarty_tpl->tpl_vars['myorder'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['myorder']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['discount']->value->orders; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['myorder']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['myorder']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['myorder']->key => $_smarty_tpl->tpl_vars['myorder']->value) {
$_smarty_tpl->tpl_vars['myorder']->_loop = true;
 $_smarty_tpl->tpl_vars['myorder']->iteration++;
 $_smarty_tpl->tpl_vars['myorder']->last = $_smarty_tpl->tpl_vars['myorder']->iteration === $_smarty_tpl->tpl_vars['myorder']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['myLoop']['last'] = $_smarty_tpl->tpl_vars['myorder']->last;
?><?php echo smartyTranslate(array('s'=>'Order #','mod'=>'loyalty'),$_smarty_tpl);?>
<?php echo $_smarty_tpl->tpl_vars['myorder']->value['id_order'];?>
 (<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['myorder']->value['total_paid'],'currency'=>$_smarty_tpl->tpl_vars['myorder']->value['id_currency']),$_smarty_tpl);?>
) : <?php if ($_smarty_tpl->tpl_vars['myorder']->value['points']>0) {?><?php echo $_smarty_tpl->tpl_vars['myorder']->value['points'];?>
 <?php echo smartyTranslate(array('s'=>'points.','mod'=>'loyalty'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Cancelled','mod'=>'loyalty'),$_smarty_tpl);?>
<?php }?><?php if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['myLoop']['last']) {?>|<?php }?><?php } ?>"><?php echo smartyTranslate(array('s'=>'more...','mod'=>'loyalty'),$_smarty_tpl);?>
</a></td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
	<div id="block-order-detail" class="hidden">&nbsp;</div>
</div>

<?php if ($_smarty_tpl->tpl_vars['minimalLoyalty']->value>0) {?><p><?php echo smartyTranslate(array('s'=>'The minimum order amount in order to use these vouchers is:','mod'=>'loyalty'),$_smarty_tpl);?>
 <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['minimalLoyalty']->value),$_smarty_tpl);?>
</p><?php }?>

<script type="text/javascript">

$(document).ready(function()
{
	$('a.tips').cluetip({
		showTitle: false,
		splitTitle: '|',
		arrows: false,
		fx: {
			open: 'fadeIn',
			openSpeed: 'fast'
		}
	});
});

</script>
<?php } else { ?>
<p class="warning"><?php echo smartyTranslate(array('s'=>'No vouchers yet.','mod'=>'loyalty'),$_smarty_tpl);?>
</p>
<?php }?>
<?php } else { ?>
<p class="warning"><?php echo smartyTranslate(array('s'=>'No reward points yet.','mod'=>'loyalty'),$_smarty_tpl);?>
</p>
<?php }?>

<?php } else { ?>
<?php echo smartyTranslate(array('s'=>'Loyalty program is not available for resellers','mod'=>'loyalty'),$_smarty_tpl);?>
.

<?php }?>
<?php }} ?>
