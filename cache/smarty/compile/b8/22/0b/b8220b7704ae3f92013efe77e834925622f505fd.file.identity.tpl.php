<?php /* Smarty version Smarty-3.1.19, created on 2021-12-13 15:26:07
         compiled from "/var/www/html/themes/ez20/identity.tpl" */ ?>
<?php /*%%SmartyHeaderCode:63109375861b757ffe08510-01580264%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b8220b7704ae3f92013efe77e834925622f505fd' => 
    array (
      0 => '/var/www/html/themes/ez20/identity.tpl',
      1 => 1637919819,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '63109375861b757ffe08510-01580264',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'navigationPipe' => 0,
    'confirmation' => 0,
    'pwd_changed' => 0,
    'email' => 0,
    'cookie' => 0,
    'is_company' => 0,
    'days' => 0,
    'v' => 0,
    'sl_day' => 0,
    'months' => 0,
    'k' => 0,
    'sl_month' => 0,
    'years' => 0,
    'sl_year' => 0,
    'newsletter' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61b757ffe51564_34432322',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61b757ffe51564_34432322')) {function content_61b757ffe51564_34432322($_smarty_tpl) {?>

<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account.php',true);?>
"><?php echo smartyTranslate(array('s'=>'My account'),$_smarty_tpl);?>
</a><span class="navigation-pipe"><?php echo $_smarty_tpl->tpl_vars['navigationPipe']->value;?>
</span><?php echo smartyTranslate(array('s'=>'Your personal information'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<h1><?php echo smartyTranslate(array('s'=>'Your personal information'),$_smarty_tpl);?>
</h1>

<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php if (isset($_smarty_tpl->tpl_vars['confirmation']->value)&&$_smarty_tpl->tpl_vars['confirmation']->value) {?>
	<p class="success">
		<?php echo smartyTranslate(array('s'=>'Your personal information has been successfully updated.'),$_smarty_tpl);?>

		<?php if (isset($_smarty_tpl->tpl_vars['pwd_changed']->value)) {?><br /><?php echo smartyTranslate(array('s'=>'Your password has been sent to your e-mail:'),$_smarty_tpl);?>
 <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['email']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>
	</p>
<?php } else { ?>
	<h3><?php echo smartyTranslate(array('s'=>'Please do not hesitate to update your personal information if it has changed.'),$_smarty_tpl);?>
</h3>
	<?php echo smartyTranslate(array('s'=>'Once you have inserted your tax code or your vat number, you will be no more able to edit them. If you want to change your tax code or your vat number, or if you have any kind of problem, please call our telephone number +39 0585 821163'),$_smarty_tpl);?>
.
	<p class="required" style="padding-top: 25px;"><sup>*</sup><?php echo smartyTranslate(array('s'=>'Required field'),$_smarty_tpl);?>
</p>
	<form action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('identity.php',true);?>
" method="post" class="std">
	<p style='font-size:16px; font-weight:bold; text-align:center'><?php echo smartyTranslate(array('s'=>'Your customer code is'),$_smarty_tpl);?>
: <?php echo $_smarty_tpl->tpl_vars['cookie']->value->id_customer;?>
</p>
	<div class="personal-information">
		<?php if ($_smarty_tpl->tpl_vars['is_company']->value==0) {?>
		
		<fieldset id="identity-field-responsive">
			
			<div class="required text">
				<label for="firstname"><?php echo smartyTranslate(array('s'=>'First name'),$_smarty_tpl);?>
</label><sup>*</sup>
				<input style="border:0px" type="text" id="firstname" name="firstname" value="<?php echo $_POST['firstname'];?>
" /> 
			</div>
			<div class="required text">
				<label for="lastname"><?php echo smartyTranslate(array('s'=>'Last name'),$_smarty_tpl);?>
</label><sup>*</sup>
				<input style="border:0px" type="text" name="lastname" id="lastname" value="<?php echo $_POST['lastname'];?>
" /> 
			</div>
						<div class="required text">
				<label for="tax_code"><?php echo smartyTranslate(array('s'=>'Tax code'),$_smarty_tpl);?>
</label><sup>*</sup>
				<input style="border:0px" type="text" name="tax_code" id="tax_code" 
				<?php if ($_POST['tax_code']!='') {?>
				<?php if (strlen($_POST['tax_code'])==16) {?>
				readonly="readonly" style="background-color:#cecece; color:#6b6b6b" 
				<?php } elseif (strlen($_POST['tax_code'])!=16) {?>
				<?php if (strlen($_POST['tax_code'])==11) {?>
				readonly="readonly" style="background-color:#cecece; color:#6b6b6b" 
				<?php } else { ?>
				<?php }?>
				<?php }?>
				<?php } else { ?>
				<?php }?> 
				
				value="<?php echo $_POST['tax_code'];?>
" /> 
			</div>
			<div class="required text">
				<label for="email"><?php echo smartyTranslate(array('s'=>'E-mail'),$_smarty_tpl);?>
</label> <sup>*</sup>
				<input type="text" name="email" readonly="readonly" style="border:0px; color:#6b6b6b" id="email" value="<?php echo $_POST['email'];?>
" />
			</div>
			<div class="required text">
				<label for="old_passwd"><?php echo smartyTranslate(array('s'=>'Current Password'),$_smarty_tpl);?>
</label> <sup>*</sup>
				<input style="border:0px" type="password" name="old_passwd" id="old_passwd" />
			</div>
			<div class="password">
				<label for="passwd"><?php echo smartyTranslate(array('s'=>'New Password'),$_smarty_tpl);?>
</label>
				<input style="border:0px" type="password" name="passwd" id="passwd" />
			</div>
			<div class="password">
				<label for="confirmation"><?php echo smartyTranslate(array('s'=>'Conferma password'),$_smarty_tpl);?>
</label>
				<input style="border:0px" type="password" name="confirmation" id="confirmation" />
			</div>
			<div class="select">
				<label><?php echo smartyTranslate(array('s'=>'Date of Birth'),$_smarty_tpl);?>
</label>
				<select name="days" id="days">
					<option value="">-</option>
					<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['days']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
						<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['v']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php if (($_smarty_tpl->tpl_vars['sl_day']->value==$_smarty_tpl->tpl_vars['v']->value)) {?>selected="selected"<?php }?>><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['v']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
&nbsp;&nbsp;</option>
					<?php } ?>
				</select>
				
				<select id="months" name="months">
					<option value="">-</option>
					<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['months']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
						<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['k']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php if (($_smarty_tpl->tpl_vars['sl_month']->value==$_smarty_tpl->tpl_vars['k']->value)) {?>selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>((string)$_smarty_tpl->tpl_vars['v']->value)),$_smarty_tpl);?>
&nbsp;</option>
					<?php } ?>
				</select>
				<select id="years" name="years">
					<option value="">-</option>
					<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['years']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
						<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['v']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php if (($_smarty_tpl->tpl_vars['sl_year']->value==$_smarty_tpl->tpl_vars['v']->value)) {?>selected="selected"<?php }?>><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['v']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
&nbsp;&nbsp;</option>
					<?php } ?>
				</select>
			</div>
			<?php if ($_smarty_tpl->tpl_vars['newsletter']->value) {?>
			<div class="checkbox" style="display: flex; align-items: center;">
				<input type="checkbox" id="newsletter" name="newsletter" value="1" <?php if (isset($_POST['newsletter'])&&$_POST['newsletter']==1) {?> checked="checked"<?php }?> />
				<label for="newsletter" ><?php echo smartyTranslate(array('s'=>'Sign up for our newsletter'),$_smarty_tpl);?>
</label>
			</div>
			<div>
			<?php echo smartyTranslate(array('s'=>'Once inserted, your tax number cannot be modified. If you need to edit your tax number, please call our customer service (0585 - 821163).'),$_smarty_tpl);?>

			</div>
			
			<?php }?>
			<div class="submit" style="display: flex; justify-content: center; width: 100%; border: none;">
				<input style="border:0px; background-color: #ef6018; width: 237px; padding: 6px 0;" type="submit" class="button_large" name="submitIdentity" value="<?php echo smartyTranslate(array('s'=>'Save'),$_smarty_tpl);?>
" />
			</div>
		</fieldset>
		<?php } else { ?>
		<fieldset>
			<div class="required text">
			
				<label for="company"><?php echo smartyTranslate(array('s'=>'Company'),$_smarty_tpl);?>
<sup>*</sup></label>
				<input style="border:0px" type="text" id="company" name="company" value="<?php echo $_POST['company'];?>
" /> 
			
			</div>
			
			<div class="required text">
			
				<label for="firstname"><?php echo smartyTranslate(array('s'=>'Representative\'s first name'),$_smarty_tpl);?>
</label> <sup>*</sup>
				<input style="border:0px" type="text" id="firstname" name="firstname" value="<?php echo $_POST['firstname'];?>
" />
				<input style="border:0px" type='hidden' id='is_company' name='is_company' value='1' />
			</div>
			<div class="required text">
				<label for="lastname"><?php echo smartyTranslate(array('s'=>'Representative\'s last name'),$_smarty_tpl);?>
</label><sup>*</sup>
				<input style="border:0px" type="text" name="lastname" id="lastname" value="<?php echo $_POST['lastname'];?>
" /> 
			</div>
					<div class="required text">
				<label for="tax_code"><?php echo smartyTranslate(array('s'=>'Tax code'),$_smarty_tpl);?>
</label> <sup>*</sup>
				<input type="text" name="tax_code" id="tax_code" 
				<?php if ($_POST['tax_code']!='') {?>
				<?php if (strlen($_POST['tax_code'])==16) {?>
				readonly="readonly" style="border:0px; background-color:#cecece; color:#6b6b6b" 
				<?php } elseif (strlen($_POST['tax_code'])!=16) {?>
				<?php if (strlen($_POST['tax_code'])==11) {?>
				readonly="readonly" style="border:0px; background-color:#cecece; color:#6b6b6b" 
				<?php } else { ?>
				<?php }?>
				<?php }?>
				<?php } else { ?>
				<?php }?> 
				
				value="<?php echo $_POST['tax_code'];?>
" />
			</div>
			<div class="required text">
				<label for="vat_number"><?php echo smartyTranslate(array('s'=>'VAT number'),$_smarty_tpl);?>
</label><sup>*</sup>
				<input  type="text" name="vat_number" id="vat_number" <?php if (strlen($_POST['vat_number'])==11) {?>readonly="readonly" style="border:0px; background-color:#cecece; color:#6b6b6b"<?php } else { ?><?php }?>  value="<?php echo $_POST['vat_number'];?>
" /> 
			</div>
			<div class="required text">
						<label for="employees_number"><?php echo smartyTranslate(array('s'=>'Employees number'),$_smarty_tpl);?>
</label><sup>*</sup>
						<select name="employees_number">
						<option value="Da 1 a 10" <?php if ($_POST['employees_number']=="Da 1 a 10") {?>selected="selected"<?php }?> ><?php echo smartyTranslate(array('s'=>'1 to 10'),$_smarty_tpl);?>
</option>
						<option value="Da 11 a 50" <?php if ($_POST['employees_number']=="Da 11 a 50") {?>selected="selected"<?php }?> ><?php echo smartyTranslate(array('s'=>'11 to 50'),$_smarty_tpl);?>
</option>
						<option value="50 piu" <?php if ($_POST['employees_number']=="50 piu") {?>selected="selected"<?php }?> ><?php echo smartyTranslate(array('s'=>'More than 50'),$_smarty_tpl);?>
</option>
						</select>
						
						
					</div>	
			<div class="required text">
				<label for="codice_univoco"><?php echo smartyTranslate(array('s'=>'SDI'),$_smarty_tpl);?>
</label>
				<input type="text" name="codice_univoco" id="codice_univoco" <?php if (strlen($_POST['codice_univoco'])==11) {?>readonly="readonly" style="border:0px; background-color:#cecece; color:#6b6b6b"<?php } else { ?>style="border:0px"<?php }?>  value="<?php echo $_POST['codice_univoco'];?>
" /> 
			</div>
		<div class="required text">
				<label for="pec"><?php echo smartyTranslate(array('s'=>'PEC'),$_smarty_tpl);?>
</label>
				<input type="text" name="pec" id="pec" <?php if (strlen($_POST['pec'])==11) {?>readonly="readonly" style="border:0px; background-color:#cecece; color:#6b6b6b"<?php } else { ?>style="border:0px"<?php }?>  value="<?php echo $_POST['pec'];?>
" />
			</div>			
			<div class="required text">
				<label for="email"><?php echo smartyTranslate(array('s'=>'E-mail'),$_smarty_tpl);?>
</label><sup>*</sup>
				<input style="border:0px" type="text" name="email" id="email" value="<?php echo $_POST['email'];?>
" /> 
			</div>
			<div class="required text">
				<label for="old_passwd"><?php echo smartyTranslate(array('s'=>'Current Password'),$_smarty_tpl);?>
</label><sup>*</sup>
				<input style="border:0px" type="password" name="old_passwd" id="old_passwd" /> 
			</div>
			<div class="password">
				<label for="passwd"><?php echo smartyTranslate(array('s'=>'New Password'),$_smarty_tpl);?>
</label>
				<input style="border:0px" type="password" name="passwd" id="passwd" />
			</div>
			<div class="password">
				<label for="confirmation"><?php echo smartyTranslate(array('s'=>'Confirmation'),$_smarty_tpl);?>
</label>
				<input style="border:0px" type="password" name="confirmation" id="confirmation" />
			</div>
			<div class="select">
				<label><?php echo smartyTranslate(array('s'=>'Representative\'s date of Birth'),$_smarty_tpl);?>
</label>
				<select name="days" id="days">
					<option value="">-</option>
					<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['days']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
						<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['v']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php if (($_smarty_tpl->tpl_vars['sl_day']->value==$_smarty_tpl->tpl_vars['v']->value)) {?>selected="selected"<?php }?>><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['v']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
&nbsp;&nbsp;</option>
					<?php } ?>
				</select>
				
				<select id="months" name="months">
					<option value="">-</option>
					<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['months']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
						<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['k']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php if (($_smarty_tpl->tpl_vars['sl_month']->value==$_smarty_tpl->tpl_vars['k']->value)) {?>selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>((string)$_smarty_tpl->tpl_vars['v']->value)),$_smarty_tpl);?>
&nbsp;</option>
					<?php } ?>
				</select>
				<select id="years" name="years">
					<option value="">-</option>
					<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['years']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
						<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['v']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php if (($_smarty_tpl->tpl_vars['sl_year']->value==$_smarty_tpl->tpl_vars['v']->value)) {?>selected="selected"<?php }?>><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['v']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
&nbsp;&nbsp;</option>
					<?php } ?>
				</select>
			</div>
			<?php if ($_smarty_tpl->tpl_vars['newsletter']->value) {?>
			<p class="checkbox">
				<input style="border:0px" type="checkbox" id="newsletter" name="newsletter" value="1" <?php if (isset($_POST['newsletter'])&&$_POST['newsletter']==1) {?> checked="checked"<?php }?> />
				<label for="newsletter"><?php echo smartyTranslate(array('s'=>'Sign up for our newsletter'),$_smarty_tpl);?>
</label>
			</p>
				<p>
			<?php echo smartyTranslate(array('s'=>'Once inserted, your tax number and your vat number cannot be modified. If you need to edit your tax number or your vat number, please call our customer service (0585 - 821163).'),$_smarty_tpl);?>

			</p>
			<?php }?>
			<p class="submit">
				<input style="border:0px" type="submit" class="button_large" name="submitIdentity" value="<?php echo smartyTranslate(array('s'=>'Save'),$_smarty_tpl);?>
" />
			</p>
		</fieldset>
		
		
		
		
		<?php }?>
	</div>
	</form>
	<p id="security_informations">
		<?php echo smartyTranslate(array('s'=>'[Insert customer data privacy clause or law here, if applicable]'),$_smarty_tpl);?>

	</p>
<?php }?>

</div>
<?php }} ?>
