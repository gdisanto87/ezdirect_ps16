<?php /* Smarty version Smarty-3.1.19, created on 2022-01-07 19:15:20
         compiled from "/var/www/html/themes/ez20/shopping-cart-product-line.tpl" */ ?>
<?php /*%%SmartyHeaderCode:198549999361d88338cb62e7-02027544%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9b01c609901250c9aeb5549275f0954e6d2d8fe8' => 
    array (
      0 => '/var/www/html/themes/ez20/shopping-cart-product-line.tpl',
      1 => 1641579097,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '198549999361d88338cb62e7-02027544',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'product' => 0,
    'productId' => 0,
    'productAttributeId' => 0,
    'customizedDatas' => 0,
    'quantityDisplayed' => 0,
    'link' => 0,
    'mediumSize' => 0,
    'PS_CATALOG_MODE' => 0,
    'img_dir' => 0,
    'priceDisplay' => 0,
    'preventivo' => 0,
    'token_cart' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61d88338ce0ee7_43362905',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61d88338ce0ee7_43362905')) {function content_61d88338ce0ee7_43362905($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/html/tools/smarty/plugins/modifier.date_format.php';
?>




<tr id="product_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product_attribute'];?>
" class="<?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['productLoop']['last']) {?>last_item<?php } elseif ($_smarty_tpl->getVariable('smarty')->value['foreach']['productLoop']['first']) {?>first_item<?php }?><?php if (isset($_smarty_tpl->tpl_vars['customizedDatas']->value[$_smarty_tpl->tpl_vars['productId']->value][$_smarty_tpl->tpl_vars['productAttributeId']->value])&&$_smarty_tpl->tpl_vars['quantityDisplayed']->value==0) {?>alternate_item<?php }?> cart_item">
	<td class="cart_product" style=" border-right:0px">
		<?php if ($_smarty_tpl->tpl_vars['product']->value['active']==1) {?><a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getProductLink($_smarty_tpl->tpl_vars['product']->value['id_product'],$_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['category']), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php } else { ?><?php }?><img style="border:0px" src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['id_image'],'medium');?>
" alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php if (isset($_smarty_tpl->tpl_vars['mediumSize']->value)) {?>width="<?php echo $_smarty_tpl->tpl_vars['mediumSize']->value['width'];?>
" height="<?php echo $_smarty_tpl->tpl_vars['mediumSize']->value['height'];?>
" <?php }?> /><?php if ($_smarty_tpl->tpl_vars['product']->value['active']==1) {?></a><?php } else { ?><?php }?>
	</td>

	
	<td class="cart_description" style="border-left:0px; width:264px; display:flex; flex-direction: column;">
		<div class="cart_ref" style="color: #9e9e9e; font-size: 12px;"><?php if ($_smarty_tpl->tpl_vars['product']->value['reference']) {?><?php echo smartyTranslate(array('s'=>'Cod : '),$_smarty_tpl);?>
<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['reference'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php } else { ?>--<?php }?></div>
		<h5 style="font-weight:600; font-size: 16px;"><?php if ($_smarty_tpl->tpl_vars['product']->value['active']==1) {?><a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getProductLink($_smarty_tpl->tpl_vars['product']->value['id_product'],$_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['category']), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" style="font-size:16px; color:#000099"><?php } else { ?><span style="font-size:16px; color:#000099"><?php }?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php if ($_smarty_tpl->tpl_vars['product']->value['active']==1) {?></a><?php } else { ?></span><?php }?></h5>
		
		<span style="font-size: 12px;"><?php echo $_smarty_tpl->tpl_vars['product']->value['description_short'];?>
</span>
		<?php if (isset($_smarty_tpl->tpl_vars['product']->value['attributes'])&&$_smarty_tpl->tpl_vars['product']->value['attributes']) {?><br /><br /><?php if ($_smarty_tpl->tpl_vars['product']->value['active']==1) {?><a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getProductLink($_smarty_tpl->tpl_vars['product']->value['id_product'],$_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['category']), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php } else { ?><?php }?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['attributes'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php if ($_smarty_tpl->tpl_vars['product']->value['active']==1) {?></a><?php } else { ?><?php }?><?php }?>
		<?php if (smarty_modifier_date_format(time(),"%Y-%m-%d")<$_smarty_tpl->tpl_vars['product']->value['date_available']) {?>
		<br />
		<?php echo smartyTranslate(array('s'=>'This product will be available from'),$_smarty_tpl);?>
 <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['product']->value['date_available'],"%d-%m-%Y");?>

		<?php } else { ?>
		<?php }?>
	</td>
	
	
	
	
	
	
	
	<td class="cart_quantity" style="text-align: center; vertical-align:center"<?php if (isset($_smarty_tpl->tpl_vars['customizedDatas']->value[$_smarty_tpl->tpl_vars['productId']->value][$_smarty_tpl->tpl_vars['productAttributeId']->value])&&$_smarty_tpl->tpl_vars['quantityDisplayed']->value==0) {?> <?php }?>>
		<?php if (isset($_smarty_tpl->tpl_vars['customizedDatas']->value[$_smarty_tpl->tpl_vars['productId']->value][$_smarty_tpl->tpl_vars['productAttributeId']->value])&&$_smarty_tpl->tpl_vars['quantityDisplayed']->value==0) {?><span id="cart_quantity_custom_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product_attribute'];?>
" ><?php echo $_smarty_tpl->tpl_vars['product']->value['customizationQuantityTotal'];?>
</span><?php }?>
		<?php if (!isset($_smarty_tpl->tpl_vars['customizedDatas']->value[$_smarty_tpl->tpl_vars['productId']->value][$_smarty_tpl->tpl_vars['productAttributeId']->value])||$_smarty_tpl->tpl_vars['quantityDisplayed']->value>0) {?>
		

		<div class="quantity-div" style="width: 100%; display: flex; justify-content: space-between; padding-left:6px; align-items:center;">
									<div style="float:left; padding-top:4px; text-transform:uppercase; font-size:10px; width:100%;"><?php echo smartyTranslate(array('s'=>'Qtà : '),$_smarty_tpl);?>

									</div>
								
									
									<div class="quantity_input_div" style="display:flex; border: 1px solid #f4f4f4; padding: 6px 0px 6px 12px;">
										<div class="quantity_input_buttons" style="border:none;">
											
											<span class="span_down"  onclick="quantity_cart_rmv('quantity_wanted_<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
');">-</span>
										</div>
										<input type="text" style="border:none; padding:0px 10px;" name="ajax_qty_to_add_to_cart[<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
]" id="quantity_wanted_<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
" class="text quantity_input" value="<?php if ($_smarty_tpl->tpl_vars['quantityDisplayed']->value==0&&isset($_smarty_tpl->tpl_vars['customizedDatas']->value[$_smarty_tpl->tpl_vars['productId']->value][$_smarty_tpl->tpl_vars['productAttributeId']->value])) {?><?php echo count($_smarty_tpl->tpl_vars['customizedDatas']->value[$_smarty_tpl->tpl_vars['productId']->value][$_smarty_tpl->tpl_vars['productAttributeId']->value]);?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['product']->value['cart_quantity']-$_smarty_tpl->tpl_vars['quantityDisplayed']->value;?>
<?php }?>" size="2" maxlength="3" />
										<div class="quantity_input_buttons"  style="border:none;">
											<span id="cart_quantity_up_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product_attribute'];?>
"  class="span_up cart_quantity_span cart_quantity_up" onclick="quantity_cart_add('quantity_wanted_<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
');">+</span>
											
										</div>
									</div>
									<div style="clear:both"></div>
								</div>
		<?php }?>
	</td>
	<td class="cart_total" style="padding-right:5px;">

		<div class="cart_availability" style="padding-right: 30px; padding-bottom: 20px;">
			<?php if ($_smarty_tpl->tpl_vars['product']->value['active']&&$_smarty_tpl->tpl_vars['product']->value['quantity_def']>0&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
				<img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
green_alert_2.jpg" alt="<?php echo smartyTranslate(array('s'=>'Available'),$_smarty_tpl);?>
" />
			<?php } else { ?>
				<img style='width:13px; margin-bottom: 3px; margin-right: 3px;' src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
orange_alert_2.jpg'  alt='<?php echo smartyTranslate(array('s'=>'Ordinabile'),$_smarty_tpl);?>
' />
			<?php }?>
		</div>

		<div style="text-align:center">
			<span class="price" id="total_product_price_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product_attribute'];?>
">
				<?php if ($_smarty_tpl->tpl_vars['quantityDisplayed']->value==0&&isset($_smarty_tpl->tpl_vars['customizedDatas']->value[$_smarty_tpl->tpl_vars['productId']->value][$_smarty_tpl->tpl_vars['productAttributeId']->value])) {?>
					<?php if (!$_smarty_tpl->tpl_vars['priceDisplay']->value) {?><span style="font-size: 17px; font-weight:600; color: #333333;"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['product']->value['total_customization_wt']),$_smarty_tpl);?>
</span><span style="font-size: 11px; color:#545454; padding-left: 10px; font-weight:100;"><?php echo smartyTranslate(array('s'=>'I.V.A. esclusa'),$_smarty_tpl);?>
</span><?php } else { ?><span style="font-size: 17px; font-weight:600; color: #333333;"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['product']->value['total_customization']),$_smarty_tpl);?>
</span><span style="font-size: 11px; color:#545454; padding-left: 10px; font-weight:100;"><?php echo smartyTranslate(array('s'=>'I.V.A. esclusa'),$_smarty_tpl);?>
</span><?php }?>
				<?php } else { ?>
					<?php if (!$_smarty_tpl->tpl_vars['priceDisplay']->value) {?><span style="font-size: 17px; font-weight:600; color: #333333;"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['product']->value['total_wt']),$_smarty_tpl);?>
</span><span style="font-size: 11px; color:#545454; padding-left: 10px;  font-weight:100;"><?php echo smartyTranslate(array('s'=>'I.V.A. esclusa'),$_smarty_tpl);?>
</span><?php } else { ?><span style="font-size: 17px; font-weight:600; color: #333333;"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['product']->value['total']),$_smarty_tpl);?>
</span><span style="font-size: 11px; color:#545454; padding-left: 10px; font-weight:100;"><?php echo smartyTranslate(array('s'=>'I.V.A. esclusa'),$_smarty_tpl);?>
</span><?php }?>
				<?php }?>
			</span>
		</div>
		<div style="text-align:center; font-size: 12px; padding-top: 30px;">
			<?php if ($_smarty_tpl->tpl_vars['preventivo']->value==1) {?><?php } else { ?><a rel="nofollow" style="color:#545454; text-decoration:none;" class="cart_quantity_delete_edit" id="<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product_attribute'];?>
" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('cart.php',true);?>
&delete&amp;id_product=<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
&amp;ipa=<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product_attribute']);?>
&amp;token=<?php echo $_smarty_tpl->tpl_vars['token_cart']->value;?>
" title="<?php echo smartyTranslate(array('s'=>'Delete'),$_smarty_tpl);?>
"><i style="color:#545454;" class="fas fa-trash-alt"></i> Rimuovi dal carrello</a>
				
				
			<?php }?>
		</div>
		
	</td>
</tr>
<?php }} ?>
