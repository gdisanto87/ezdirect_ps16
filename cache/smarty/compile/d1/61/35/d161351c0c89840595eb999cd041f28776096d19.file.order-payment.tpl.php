<?php /* Smarty version Smarty-3.1.19, created on 2021-11-16 15:07:55
         compiled from "/var/www/html/themes/ez20/order-payment.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14354737686193bb3bec4539-33010336%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd161351c0c89840595eb999cd041f28776096d19' => 
    array (
      0 => '/var/www/html/themes/ez20/order-payment.tpl',
      1 => 1636107591,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14354737686193bb3bec4539-33010336',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'opc' => 0,
    'currencySign' => 0,
    'currencyRate' => 0,
    'currencyFormat' => 0,
    'currencyBlank' => 0,
    'cfcheck' => 0,
    'link' => 0,
    'check_tx' => 0,
    'check_vat' => 0,
    'check_phone' => 0,
    'HOOK_TOP_PAYMENT' => 0,
    'HOOK_PAYMENT' => 0,
    'total_price' => 0,
    'taxes_enabled' => 0,
    'img_ps_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_6193bb3bee05f6_16381513',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6193bb3bee05f6_16381513')) {function content_6193bb3bee05f6_16381513($_smarty_tpl) {?>

<?php if (!$_smarty_tpl->tpl_vars['opc']->value) {?>
	<script type="text/javascript">
	// <![CDATA[
	var currencySign = '<?php echo html_entity_decode($_smarty_tpl->tpl_vars['currencySign']->value,2,"UTF-8");?>
';
	var currencyRate = '<?php echo floatval($_smarty_tpl->tpl_vars['currencyRate']->value);?>
';
	var currencyFormat = '<?php echo intval($_smarty_tpl->tpl_vars['currencyFormat']->value);?>
';
	var currencyBlank = '<?php echo intval($_smarty_tpl->tpl_vars['currencyBlank']->value);?>
';
	var txtProduct = "<?php echo smartyTranslate(array('s'=>'product'),$_smarty_tpl);?>
";
	var txtProducts = "<?php echo smartyTranslate(array('s'=>'products'),$_smarty_tpl);?>
";
	// ]]>
	</script>

	<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'Your payment method'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php }?>

<?php if (!$_smarty_tpl->tpl_vars['opc']->value) {?><h1><?php echo smartyTranslate(array('s'=>'Choose your payment method'),$_smarty_tpl);?>
</h1><?php } else { ?><h2>3. <?php echo smartyTranslate(array('s'=>'Choose your payment method'),$_smarty_tpl);?>
</h2><?php }?>

<?php if (!$_smarty_tpl->tpl_vars['opc']->value) {?>
	<?php $_smarty_tpl->tpl_vars['current_step'] = new Smarty_variable('payment', null, 0);?>
	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./order-steps.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="shopping_cart_table">
	
<?php } else { ?>

	<div id="opc_payment_methods" class="opc-main-block">
		<div id="opc_payment_methods-overlay" class="opc-overlay" style="display: none;"></div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['cfcheck']->value==0) {?>
<?php echo smartyTranslate(array('s'=>'There are some details missing in your account: if you are a end user customer you must specify your tax code in your account details, and if you are a company you must specify both tax code and vat number'),$_smarty_tpl);?>
. <a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account.php',true);?>
"><?php echo smartyTranslate(array('s'=>'Click here to go to your account and update your profile details'),$_smarty_tpl);?>
</a><br /><br />
<?php echo smartyTranslate(array('s'=>'Here there are your missing details'),$_smarty_tpl);?>
:<br /><br />
<?php if ($_smarty_tpl->tpl_vars['check_tx']->value==1) {?> <a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('identity.php',true);?>
" target="_blank"><?php echo smartyTranslate(array('s'=>'Tax code'),$_smarty_tpl);?>
</a><?php } else { ?><?php }?>
<?php if ($_smarty_tpl->tpl_vars['check_vat']->value==1) {?> <a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('identity.php',true);?>
" target="_blank"><?php echo smartyTranslate(array('s'=>'VAT number'),$_smarty_tpl);?>
</a><?php } else { ?><?php }?>
<?php if ($_smarty_tpl->tpl_vars['check_phone']->value==1) {?> <a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('addresses.php',true);?>
" target="_blank"><?php echo smartyTranslate(array('s'=>'Phone number'),$_smarty_tpl);?>
</a><?php } else { ?><?php }?>
<?php } else { ?>
<div  id="HOOK_TOP_PAYMENT"><?php echo $_smarty_tpl->tpl_vars['HOOK_TOP_PAYMENT']->value;?>
</div>

<div id="address-form-short">
<?php if ($_smarty_tpl->tpl_vars['HOOK_PAYMENT']->value) {?>
	<?php if (!$_smarty_tpl->tpl_vars['opc']->value) {?><h4><?php echo smartyTranslate(array('s'=>'Please select your preferred payment method to pay the amount of'),$_smarty_tpl);?>
&nbsp;<span class="price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['total_price']->value),$_smarty_tpl);?>
</span> <?php if ($_smarty_tpl->tpl_vars['taxes_enabled']->value) {?><?php echo smartyTranslate(array('s'=>'(tax incl.)'),$_smarty_tpl);?>
<?php }?></h4><br /><?php }?>
	<?php if ($_smarty_tpl->tpl_vars['opc']->value) {?><div id="opc_payment_methods-content"><?php }?>
		<div style="" id="metodi-pagamento" id="HOOK_PAYMENT"><?php echo $_smarty_tpl->tpl_vars['HOOK_PAYMENT']->value;?>
</div>
	<?php if ($_smarty_tpl->tpl_vars['opc']->value) {?></div><?php }?>
<?php } else { ?>
	<p class="warning"><?php echo smartyTranslate(array('s'=>'No payment modules have been installed.'),$_smarty_tpl);?>
</p>
<?php }?>

<?php if (!$_smarty_tpl->tpl_vars['opc']->value) {?>
<div>
	<p class="cart_navigation"><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('order.php',true);?>
?step=2" title="<?php echo smartyTranslate(array('s'=>'Previous'),$_smarty_tpl);?>
"  class="button_large" style="padding: 8px 20px; height:auto;">&laquo; <?php echo smartyTranslate(array('s'=>'Previous'),$_smarty_tpl);?>
</a></p>
</div>
<?php } else { ?>
	</div>
	
<?php }?>
<?php }?>
<br /><br />
</div>


</div>

<br />
<div style="border: 1px solid #e5e8eb; 
			box-shadow: 0 1px 1px rgba(0,0,0,0.15), 
        	0 2px 2px rgba(0,0,0,0.15), 
            0 4px 4px rgba(0,0,0,0.15), 
        	0 8px 8px rgba(0,0,0,0.15);
			border-radius:12px;
			padding:30px; 
			display: flex;
			width: 100%;" id="need-help">
		<div style="height: 140px; padding-right: 30px;">
			<img style="height:100%;" src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
business-woman.jpg' alt='Aiuto' title='Aiuto' />
		</div>
		<div>
			<h5 style="font-size: 22px; font-weight: 600;">Hai bisogno di aiuto con il tuo ordine?</h5>
			<div style="display:flex; align-items:center;" id="numero-verde"><p>Chiamaci al numero verde</p><a href="#" style="padding-left: 12px; padding-bottom: 10px;"><img style="height:50px;" src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
numero-verde.jpg' alt='Numero verde' /></a></div>
			
			<p>Oppure apri un ticket cliccando <a target='_blank' href='https://www.ezdirect.it/contattaci?step=assistenza-ordini'>qui</a>. Un esperto di risponderà quanto prima.</p>
		</div>
</div>

<p class="clear"></p><?php }} ?>
