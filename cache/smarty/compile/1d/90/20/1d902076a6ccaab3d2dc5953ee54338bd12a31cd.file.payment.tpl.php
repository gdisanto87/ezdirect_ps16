<?php /* Smarty version Smarty-3.1.19, created on 2021-11-16 15:07:51
         compiled from "/var/www/html/modules/bankwire/views/templates/hook/payment.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1897075086193bb3737c3a0-71266516%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1d902076a6ccaab3d2dc5953ee54338bd12a31cd' => 
    array (
      0 => '/var/www/html/modules/bankwire/views/templates/hook/payment.tpl',
      1 => 1635156160,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1897075086193bb3737c3a0-71266516',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_6193bb37382266_70893286',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6193bb37382266_70893286')) {function content_6193bb37382266_70893286($_smarty_tpl) {?>

<p class="payment_module">
	<a style="display: flex; flex-direction: column; text-align: center;" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('bankwire','payment'), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Pay by bank wire','mod'=>'bankwire'),$_smarty_tpl);?>
">
		<i style="font-size: 60px; color: #30b1c7;" class="fas fa-file-invoice-dollar"></i>
		<span style="color: #545454; font-weight: bold; padding-top: 15px;"><?php echo smartyTranslate(array('s'=>'Pay by bank wire','mod'=>'bankwire'),$_smarty_tpl);?>
</span>
	</a>
</p><?php }} ?>
