<?php /* Smarty version Smarty-3.1.19, created on 2021-12-05 09:55:42
         compiled from "/var/www/html/modules/emarketing/views/templates/admin/configure.tpl" */ ?>
<?php /*%%SmartyHeaderCode:177131605861ac7e8e897696-59436636%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1d1da1e744d7215e3a6442adb512dbdb5770f07f' => 
    array (
      0 => '/var/www/html/modules/emarketing/views/templates/admin/configure.tpl',
      1 => 1617002911,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '177131605861ac7e8e897696-59436636',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'module_dir' => 0,
    'emptyShopToken' => 0,
    'signupUrl' => 0,
    'linkUrl' => 0,
    'loginUrl' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61ac7e8e8a5710_58398881',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61ac7e8e8a5710_58398881')) {function content_61ac7e8e8a5710_58398881($_smarty_tpl) {?>

<div>
    <div class="panel emarketing informations col-lg-10 col-lg-offset-1">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="row emarketing_header">
                <div class="presentation col-lg-6">
                    <div class="col-lg-12">
                        <div class="row">
                            <p style="font-size: 40px; font-weight: bold; margin-top: 55px; margin-bottom: 20px; margin-left: -10px;"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
views/img/ps-logo.png" alt="Prestashop Ads" style="width: 440px; height: 110px;" /></p>
                            <p style="margin-bottom: 20px;">
                                <b><?php echo smartyTranslate(array('s'=>'Welcome to PrestaShop Ads.','mod'=>'emarketing'),$_smarty_tpl);?>
</b> <?php echo smartyTranslate(array('s'=>'This plugin generates a product data feed for Google Shopping and Facebook / Instagram. Furthermore it generates Conversion Tracking and installs it to your Webshop.','mod'=>'emarketing'),$_smarty_tpl);?>

                            </p>
                            <?php if ($_smarty_tpl->tpl_vars['emptyShopToken']->value) {?>
                                <p><?php echo smartyTranslate(array('s'=>'Click [1]Sign up[/1] to create your PrestaShop Ads account.','tags'=>array('<b>'),'mod'=>'emarketing'),$_smarty_tpl);?>
<br>
                                    <?php echo smartyTranslate(array('s'=>'To connect your existing PrestaShop Ads account, click [1]Merge account[/1].','tags'=>array('<b>'),'mod'=>'emarketing'),$_smarty_tpl);?>
</p>
                                <p>
                                    <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['signupUrl']->value, ENT_QUOTES, 'UTF-8', true);?>
" target="_blank" rel="noopener noreferrer" onclick="setTimeout(refresh, 5000);"><?php echo smartyTranslate(array('s'=>'Sign up','mod'=>'emarketing'),$_smarty_tpl);?>
</a>
                                    <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['linkUrl']->value, ENT_QUOTES, 'UTF-8', true);?>
" target="_blank" rel="noopener noreferrer" onclick="setTimeout(refresh, 5000);"><?php echo smartyTranslate(array('s'=>'Merge account','mod'=>'emarketing'),$_smarty_tpl);?>
</a>
                                </p>
                            <?php } else { ?>
                                <p><?php echo smartyTranslate(array('s'=>'Click [1]Login[/1] to log in to your PrestaShop Ads account.','tags'=>array('<b>'),'mod'=>'emarketing'),$_smarty_tpl);?>
</p>
                                <p><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['loginUrl']->value, ENT_QUOTES, 'UTF-8', true);?>
" target="_blank" rel="noopener noreferrer"><?php echo smartyTranslate(array('s'=>'Login','mod'=>'emarketing'),$_smarty_tpl);?>
</a></p>
                            <?php }?>
                            <p>
                                <a href="https://intercom.help/prestashop-ads/" target="_blank" class="faqbutton" rel="noopener noreferrer">FAQ</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="bgimage col-lg-6 text-center">
                    <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
views/img/shape_bg_img.png" style="width: 434px; height: 255px; position: absolute;" />
                    <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
views/img/PS_ads_img.png" style="width: 296px; height: 224px; position: relative; left: 75px; top: 35px;" />
                </div>
            </div>
            <div class="row emarketing-content">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="row">
                        <div class="col-lg-2">
                            <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
views/img/catalog_upload_img.png"/>
                        </div>
                        <div class="col-lg-10">
                            <p class="title"><?php echo smartyTranslate(array('s'=>'Simplified catalog upload','mod'=>'emarketing'),$_smarty_tpl);?>
</p>
                            <p class="content"><?php echo smartyTranslate(array('s'=>'We help you create an account, generate your product feed and upload it to Google Merchant Center. With a new account, Google will credit back what you spend in the first 30 days, up to 120€.','mod'=>'emarketing'),$_smarty_tpl);?>
</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
views/img/google_smart_shopping_campaign_img.png"/>
                        </div>
                        <div class="col-lg-10">
                            <p class="title"><?php echo smartyTranslate(array('s'=>'Smart Shopping campaigns on Google','mod'=>'emarketing'),$_smarty_tpl);?>
</p>
                            <p class="content"><?php echo smartyTranslate(array('s'=>'On PrestaShop Ads, you only need to set your budget to launch a Smart Shopping campaign on Google. Your products will appear on Google Search, Display, Gmail and YouTube, on mobile and desktop.','mod'=>'emarketing'),$_smarty_tpl);?>
</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
views/img/competitors_analysis_img.png"/>
                        </div>
                        <div class="col-lg-10">
                            <p class="title"><?php echo smartyTranslate(array('s'=>'Competitors Analysis','mod'=>'emarketing'),$_smarty_tpl);?>
</p>
                            <p class="content"><?php echo smartyTranslate(array('s'=>'If you sell products that others are selling, you can monitor competitors\' prices on Amazon, eBay and Shopping ads. Create alerts to reprice your articles and focus your ads on your most promising products.','mod'=>'emarketing'),$_smarty_tpl);?>
</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="clear: both;">&nbsp;</div>
</div>

<style type="text/css">
    .informations {
        font-family: OpenSans-Regular, sans-serif;
        font-size: 16px;
        color: #363A41;
        letter-spacing: 0;
        line-height: 24px;
    }
    .emarketing {
        padding: 0 16px 20px !important;
    }
    .emarketing img {
        display: block;
        height: 257px;
        margin: 0;
    }
    .emarketing a, .emarketing a:hover, .emarketing a:active, .emarketing a:focus {
        display: inline-block;
        background-color: #24B9D7;
        border: 1px solid #24B9D7;
        border-radius: 1px;
        padding: 5px 12px;
        margin-right: 20px;
        text-decoration: none;
        color: #fff;
        font-size: 14px;
        font-weight: bold;
    }
    a.faqbutton {
        color: #24B9D7 !important;
        background-color: #fff !important;
    }
    .emarketing_header {
        border-bottom: 1px solid #ccc;
        height: 435px;
    }
    .emarketing-content {
        margin: 20px 0;
    }
    .emarketing-content img {
        width: 90px;
        height: 90px;
        margin-bottom: 15px;
    }
    .emarketing-content .row {
        margin: 15px 0 35px 0;
    }
    .emarketing-content .row:last-child{
        margin-bottom: 0;
    }
    .emarketing-content .row i{
        font-size: 34px;
    }
    .emarketing-content .row .title{
        color: #363A41;
        font-family: "Open Sans";
        font-size: 28px;
        line-height: 19px;
        font-weight: normal;
        margin-top: 6px;
        margin-bottom: 20px;
    }
    .emarketing-content .row .content{
        color: #6C868E;
        font-family: "Open Sans";
        font-size: 14px;
        line-height: 19px;
    }
    .emarketing-content .material-icons.green {
        color: #00b555;
    }
    .emarketing-content .material-icons.red {
        color: #f9001a;
    }
    .emarketing-content .material-icons.yellow {
        color: #ffc100;
    }

    @media only screen and (max-width: 1240px) {
        .bgimage {
            display: none;
        }
    }
</style>
<script type="application/javascript">
    function refresh()
    {
        location.reload();
        return false;
    }
</script>
<?php }} ?>
