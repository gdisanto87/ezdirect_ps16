<?php /* Smarty version Smarty-3.1.19, created on 2021-12-13 17:37:38
         compiled from "/var/www/html/themes/ez20/my-account.tpl" */ ?>
<?php /*%%SmartyHeaderCode:67228279661b776d2ee2ca9-59561135%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '38121d41618821a5c132c14c42b7cfaf62183f1d' => 
    array (
      0 => '/var/www/html/themes/ez20/my-account.tpl',
      1 => 1638347307,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '67228279661b776d2ee2ca9-59561135',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'cookie' => 0,
    'link' => 0,
    'base_dir_ssl' => 0,
    'data_reg' => 0,
    'orders' => 0,
    'order' => 0,
    'img_dir' => 0,
    'invoiceAllowed' => 0,
    'are_there_offers' => 0,
    'img_ps_dir' => 0,
    'fatturazione' => 0,
    'spedizione1' => 0,
    'fido_tot' => 0,
    'crediti_fido' => 0,
    'fido' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61b776d301f346_53907966',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61b776d301f346_53907966')) {function content_61b776d301f346_53907966($_smarty_tpl) {?>


<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'Il mio profilo'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<div style="padding: 25px 0; display: none;">
	<?php if (($_smarty_tpl->tpl_vars['cookie']->value->customer_phone==''&&$_smarty_tpl->tpl_vars['cookie']->value->customer_phone_mobile=='')||$_smarty_tpl->tpl_vars['cookie']->value->customer_tax_code==''||($_smarty_tpl->tpl_vars['cookie']->value->is_company==1&&$_smarty_tpl->tpl_vars['cookie']->value->customer_vat_number=='')) {?>
	<strong style="color:red"><?php echo smartyTranslate(array('s'=>'WARNING'),$_smarty_tpl);?>
</strong>
	<br />
	<?php echo smartyTranslate(array('s'=>'There are some missing required data in your account. If you do not fill them you will not able to order products on our website. Here there are the missing or not valid fields:'),$_smarty_tpl);?>

	<br /><br />
	<?php if ($_smarty_tpl->tpl_vars['cookie']->value->customer_phone==''&&$_smarty_tpl->tpl_vars['cookie']->value->customer_phone_mobile=='') {?>
	<?php echo smartyTranslate(array('s'=>'Phone number'),$_smarty_tpl);?>
 - <a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('addresses.php',true);?>
"><?php echo smartyTranslate(array('s'=>'Click here to update your phone number'),$_smarty_tpl);?>
</a><br />
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['cookie']->value->customer_tax_code=='') {?>
	<?php echo smartyTranslate(array('s'=>'Tax code'),$_smarty_tpl);?>
 - <a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('identity.php',true);?>
"><?php echo smartyTranslate(array('s'=>'Click here to update your tax code'),$_smarty_tpl);?>
</a><br />
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['cookie']->value->is_company==1) {?>
	<?php if ($_smarty_tpl->tpl_vars['cookie']->value->customer_vat_number=='') {?>
	<?php echo smartyTranslate(array('s'=>'VAT number'),$_smarty_tpl);?>
 - <a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('identity.php',true);?>
"><?php echo smartyTranslate(array('s'=>'Click here to update your vat number'),$_smarty_tpl);?>
</a><br />
	<?php }?>
	<?php }?>
</div>

<div style="display: flex; justify-content: space-between; padding-top: 30px;">
	<div id="left-column-profile">
		<div> 

			<div style="width: 80%; border-bottom: 1px solid #ddd9d9; margin-left: 20px;"><h4 id="my-profile-title">Area dati personali <span><button id="change-icon" onclick="myFunction()"  style="border:none;"><i class="fas fa-chevron-down"></i></button></span></h4></div>
		
			<ul id="myDIV">
				<li><a style="color: #545454" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('identity.php',true);?>
" title=""><?php echo smartyTranslate(array('s'=>'I miei dati personali'),$_smarty_tpl);?>
</a></li>
				<li><a style="color: #545454" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('addresses.php',true);?>
" title=""><?php echo smartyTranslate(array('s'=>'I miei indirizzi'),$_smarty_tpl);?>
</a></li>
				<li><a style="color: #545454" href="<?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
modules/mytickets/tickets.php" title=""><?php echo smartyTranslate(array('s'=>'I miei ticket'),$_smarty_tpl);?>
</a> </li>
				
			</ul>
		</div>

		<div> 
			<div style="width: 80%; border-bottom: 1px solid #ddd9d9; margin-left: 20px;"><h4 id="my-profile-title">Area ordini e offerte <span><button id="change-icon1" onclick="myFunction1()"  style="border:none;"><i class="fas fa-chevron-down"></i></button></span></h4></div>
			<ul id="myDIV1">
				<li><a style="color: #545454" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('history.php',true);?>
" title=""><?php echo smartyTranslate(array('s'=>'I miei ordini'),$_smarty_tpl);?>
</a></li>
				<li><a style="color: #545454" href="<?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
modules/mieofferte/offerte.php" title=""><?php echo smartyTranslate(array('s'=>'Le mie offerte'),$_smarty_tpl);?>
</a></li>
				<li><a style="color: #545454" href="<?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
modules/miefatture/fatture.php" title=""><?php echo smartyTranslate(array('s'=>'Le mie fatture'),$_smarty_tpl);?>
</a></li>
				<li><a style="color: #545454" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('order-slip.php',true);?>
" title=""><?php echo smartyTranslate(array('s'=>'Le mie note di credito'),$_smarty_tpl);?>
</a></li>
				<li><a style="color: #545454" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('discount.php',true);?>
" title=""><?php echo smartyTranslate(array('s'=>'I miei buoni'),$_smarty_tpl);?>
</a> </li>
				<li><a style="color: #545454" href="<?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
modules/loyalty/loyalty-program.php" title=""><?php echo smartyTranslate(array('s'=>'I miei punti fedeltà'),$_smarty_tpl);?>
</a></li>
			</ul>
		</div>

		<div> 
			<div style="width: 80%; border-bottom: 1px solid #ddd9d9; margin-left: 20px;"><h4 id="my-profile-title">Area contatti <span><button id="change-icon2" onclick="myFunction2()" style="border:none;"><i class="fas fa-chevron-down"></i></button></span></h4></div>
			<ul id="myDIV2">
				<li><a style="color: #545454" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('contact-form.php',true);?>
?step=contabilita" title=""><?php echo smartyTranslate(array('s'=>'Amministrazione contabilità'),$_smarty_tpl);?>
</a></li>
				<li><a style="color: #545454" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('contact-form.php',true);?>
?step=assistenza-ordini" title=""><?php echo smartyTranslate(array('s'=>'Amministrazione ordini'),$_smarty_tpl);?>
</a></li>
				<li><a style="color: #545454" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('contact-form.php',true);?>
?step=tecnica" title=""><?php echo smartyTranslate(array('s'=>'Assistenza tecnica'),$_smarty_tpl);?>
</a></li>
				<li><a style="color: #545454" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('cms.php?id_cms=36',true);?>
" title=""><?php echo smartyTranslate(array('s'=>'RMA'),$_smarty_tpl);?>
</a></li>
				<li><a style="color: #545454" href="<?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
modules/formprevendita/form.php" title=""><?php echo smartyTranslate(array('s'=>'Quotazioni'),$_smarty_tpl);?>
</a></li>
				<li><a style="color: #545454" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('contact-form.php',true);?>
?step=rivenditori" title=""><?php echo smartyTranslate(array('s'=>'Rivenditori'),$_smarty_tpl);?>
</a></li>
			</ul>
		</div>

		<div> 
			<div style="width: 80%; border-bottom: 1px solid #ddd9d9; margin-left: 20px;"><h4 id="my-profile-title">
				<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('index.php');?>
?logout" id="logout-link" rel="nofollow" title=""><?php echo smartyTranslate(array('s'=>'Esci','mod'=>'blockuserinfo'),$_smarty_tpl);?>
</a>
			</h4></div>
			
		</div>

		<div> 
			<div style="width: 80%; margin-left: 20px;"><h4 id="my-profile-title" style="text-transform: none;">
				<a href="https://www.ezdirect.it/modules/cancellami/cancella-miei-dati.php"><?php echo smartyTranslate(array('s'=>'Cancella dati'),$_smarty_tpl);?>
</a>
			</h4></div>
			
		</div>
	</div>

	<script>

		

		function myFunction() {

		$('#change-icon').find("i").toggleClass("fa-chevron-down fa-chevron-right");
		var x = document.getElementById("myDIV");
	
		if (x.style.display === "none") {
			x.style.display = "block";
		
		} else {
			x.style.display = "none";
		
		}
		}

		function myFunction1() {

		$('#change-icon1').find("i").toggleClass("fa-chevron-down fa-chevron-right");
		var x = document.getElementById("myDIV1");
		
		if (x.style.display === "none") {
			x.style.display = "block";
			
		} else {
			x.style.display = "none";
			
		}
		}

		function myFunction2() {

		$('#change-icon2').find("i").toggleClass("fa-chevron-down fa-chevron-right");
		var x = document.getElementById("myDIV2");
		
		if (x.style.display === "none") {
			x.style.display = "block";
			
		} else {
			x.style.display = "none";
			
		}
		}

		
	</script>

	<div id="center-col-my-profile">
		<div style="background-color: #f4f4f4; padding: 10px 20px; width: 100%; margin-bottom: 25px;">
		<h1 style='text-align:left; color: #000099; font-size: 25px; margin: 0; padding: 0;'><?php echo smartyTranslate(array('s'=>'Il mio profilo'),$_smarty_tpl);?>
</h1>
		<br />
		<h4 style="font-size: 14px; font-weight: normal; color: #62717C; line-height: 23px;"><?php echo smartyTranslate(array('s'=>'Benvenuto nella tua area personale! Qui potrai gestire i tuoi ordini, modificare dati personali, modificare e aggiungere indirizzi, gestire le comunicazioni con Ezdirect. '),$_smarty_tpl);?>
</h4>

		<p><br /></p>
		<p style="color: #62717C;">CODICE CLIENTE: <strong><?php echo $_smarty_tpl->tpl_vars['cookie']->value->id_customer;?>
</strong><span style='margin-left:13px;margin-right:13px'> | </span> REGISTRATO DAL: <strong><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0][0]->dateFormat(array('date'=>$_smarty_tpl->tpl_vars['data_reg']->value,'full'=>0),$_smarty_tpl);?>
</strong> <span style='margin-left:13px;margin-right:13px'> | </span>  EMAIL DI REGISTRAZIONE: <strong><?php echo $_smarty_tpl->tpl_vars['cookie']->value->email;?>
</strong></p>
		<p></p>
		<br />

		<div><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('index.php');?>
?logout" id="logout-link-responsive" rel="nofollow" title=""><?php echo smartyTranslate(array('s'=>'Esci','mod'=>'blockuserinfo'),$_smarty_tpl);?>
</a></div>

		</div>




		

			<div class='my-account-block' >
			<h4 class="h4tab1" style="text-align:center;"><?php echo smartyTranslate(array('s'=>'Last orders'),$_smarty_tpl);?>
 <a style="font-size:11px; color:#ff6600;" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('history.php',true);?>
" title="">(<?php echo smartyTranslate(array('s'=>'View all'),$_smarty_tpl);?>
)</a></h4>
			<table id="order-list" class="std" style="width:100%">
				<thead>
					<tr id="order-my-profile">
						<td><strong><?php echo smartyTranslate(array('s'=>'Order'),$_smarty_tpl);?>
</strong></th>
						<td><strong><?php echo smartyTranslate(array('s'=>'Date'),$_smarty_tpl);?>
</strong></th>
						<td><strong><?php echo smartyTranslate(array('s'=>'Total price'),$_smarty_tpl);?>
</strong></th>
						<td><strong><?php echo smartyTranslate(array('s'=>'Payment'),$_smarty_tpl);?>
</strong></th>
						<td><strong><?php echo smartyTranslate(array('s'=>'Status'),$_smarty_tpl);?>
</strong></th>
						<td><strong><?php echo smartyTranslate(array('s'=>'Invoice'),$_smarty_tpl);?>
</strong></th>
						<td style="width:65px">&nbsp;</strong></th>
					</tr>
				</thead>
				<tbody>
				<?php  $_smarty_tpl->tpl_vars['order'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['order']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['orders']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['order']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['order']->iteration=0;
 $_smarty_tpl->tpl_vars['order']->index=-1;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['myLoop']['index']=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['order']->key => $_smarty_tpl->tpl_vars['order']->value) {
$_smarty_tpl->tpl_vars['order']->_loop = true;
 $_smarty_tpl->tpl_vars['order']->iteration++;
 $_smarty_tpl->tpl_vars['order']->index++;
 $_smarty_tpl->tpl_vars['order']->first = $_smarty_tpl->tpl_vars['order']->index === 0;
 $_smarty_tpl->tpl_vars['order']->last = $_smarty_tpl->tpl_vars['order']->iteration === $_smarty_tpl->tpl_vars['order']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['myLoop']['first'] = $_smarty_tpl->tpl_vars['order']->first;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['myLoop']['index']++;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['myLoop']['last'] = $_smarty_tpl->tpl_vars['order']->last;
?>
					<tr class="<?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['myLoop']['first']) {?>first_item<?php } elseif ($_smarty_tpl->getVariable('smarty')->value['foreach']['myLoop']['last']) {?>last_item<?php } else { ?>item<?php }?> <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['myLoop']['index']%2) {?>alternate_item<?php }?>">
						<td class="history_link bold">
							<?php if (isset($_smarty_tpl->tpl_vars['order']->value['invoice'])&&$_smarty_tpl->tpl_vars['order']->value['invoice']&&isset($_smarty_tpl->tpl_vars['order']->value['virtual'])&&$_smarty_tpl->tpl_vars['order']->value['virtual']) {?><img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/download_product.gif" class="icon" alt="<?php echo smartyTranslate(array('s'=>'Products to download'),$_smarty_tpl);?>
" width="22" height="22" title="<?php echo smartyTranslate(array('s'=>'Products to download'),$_smarty_tpl);?>
" /><?php }?>
							<?php echo smartyTranslate(array('s'=>'#'),$_smarty_tpl);?>
<?php echo $_smarty_tpl->tpl_vars['order']->value['id_order'];?>

						</td>
						<td class="history_date bold"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0][0]->dateFormat(array('date'=>$_smarty_tpl->tpl_vars['order']->value['date_add'],'full'=>0),$_smarty_tpl);?>
</td>
						<td class="history_price"><span class="price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['order']->value['total_paid_real'],'currency'=>$_smarty_tpl->tpl_vars['order']->value['id_currency'],'no_utf8'=>false,'convert'=>false),$_smarty_tpl);?>
</span></td>
						<td class="history_method"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['order']->value['payment'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
						<td class="history_state"><?php if (isset($_smarty_tpl->tpl_vars['order']->value['order_state'])) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['order']->value['order_state'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?></td>
						<td class="history_invoice">
						<?php if ((isset($_smarty_tpl->tpl_vars['order']->value['invoice'])&&$_smarty_tpl->tpl_vars['order']->value['invoice']&&isset($_smarty_tpl->tpl_vars['order']->value['invoice_number'])&&$_smarty_tpl->tpl_vars['order']->value['invoice_number'])&&isset($_smarty_tpl->tpl_vars['invoiceAllowed']->value)&&$_smarty_tpl->tpl_vars['invoiceAllowed']->value==true) {?>
							<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('pdf-invoice.php',true);?>
?id_order=<?php echo intval($_smarty_tpl->tpl_vars['order']->value['id_order']);?>
" title="<?php echo smartyTranslate(array('s'=>'Invoice'),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/pdf.gif" alt="<?php echo smartyTranslate(array('s'=>'Invoice'),$_smarty_tpl);?>
" width="16" height="16" class="icon" /></a>
							<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('pdf-invoice.php',true);?>
?id_order=<?php echo intval($_smarty_tpl->tpl_vars['order']->value['id_order']);?>
" title="<?php echo smartyTranslate(array('s'=>'Invoice'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'PDF'),$_smarty_tpl);?>
</a>
						<?php } else { ?>-<?php }?>
						</td>
						<td class="history_detail">
							
						</td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
			
			<br />
			</div>
		
		<div id="block-order-detail" class="hidden">&nbsp;</div>
		<?php } else { ?>
			
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['are_there_offers']->value==1) {?>
		<div class='my-account-block' style='font-size:16px; padding-top:5px; padding-bottom:5px; text-align:center'> 
		<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/mieofferte/offerte.php',true);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
ci-sono-offerte-per-te.jpg" alt="<?php echo smartyTranslate(array('s'=>'There are offers for you'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'There are offers for you'),$_smarty_tpl);?>
" class="icon" /></a>
		</div>
		<?php }?>
		
		<div class='account-areas'>
		<div class='my-account-block'> 
		<h4 class="h4tab1" style='text-align:center'><?php echo smartyTranslate(array('s'=>'My addresses'),$_smarty_tpl);?>
 <a style="font-size:11px; color:#ff6600" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('addresses.php',true);?>
" title="">(<?php echo smartyTranslate(array('s'=>'View all'),$_smarty_tpl);?>
)</a></h4>
		<div style='display:block; float:left; width: 250px; margin-left:15px; text-align:left'>
		<strong><?php echo smartyTranslate(array('s'=>'Invoice address'),$_smarty_tpl);?>
</strong><br /><br />
		<table class="my_account_table">
		<tr><td style="width:90px"><?php echo smartyTranslate(array('s'=>'Name'),$_smarty_tpl);?>
</td><td><strong><?php echo $_smarty_tpl->tpl_vars['fatturazione']->value['firstname'];?>
</strong></td></tr>
		<tr><td><?php echo smartyTranslate(array('s'=>'Last name'),$_smarty_tpl);?>
</td><td><strong><?php echo $_smarty_tpl->tpl_vars['fatturazione']->value['lastname'];?>
</strong></td></tr>
		<?php if ($_smarty_tpl->tpl_vars['fatturazione']->value['is_company']==1) {?>
		<tr><td><?php echo smartyTranslate(array('s'=>'Company'),$_smarty_tpl);?>
</td><td><strong><?php echo $_smarty_tpl->tpl_vars['fatturazione']->value['company'];?>
</strong></td></tr>
		<tr><td><?php echo smartyTranslate(array('s'=>'VAT number'),$_smarty_tpl);?>
</td><td><strong><?php echo $_smarty_tpl->tpl_vars['fatturazione']->value['vat_number'];?>
</strong></td></tr>
		<?php } else { ?><?php }?>
		<tr><td><?php echo smartyTranslate(array('s'=>'Tax code'),$_smarty_tpl);?>
</td><td><strong><?php echo $_smarty_tpl->tpl_vars['fatturazione']->value['tax_code'];?>
</strong></td></tr>
		<tr><td><?php echo smartyTranslate(array('s'=>'Address'),$_smarty_tpl);?>
</td><td><strong><?php echo $_smarty_tpl->tpl_vars['fatturazione']->value['address1'];?>
</strong></td></tr>
		<tr><td><?php echo smartyTranslate(array('s'=>'Zip'),$_smarty_tpl);?>
</td><td><strong><?php echo $_smarty_tpl->tpl_vars['fatturazione']->value['postcode'];?>
</strong></td></tr>
		<tr><td><?php echo smartyTranslate(array('s'=>'City'),$_smarty_tpl);?>
</td><td><strong><?php echo $_smarty_tpl->tpl_vars['fatturazione']->value['city'];?>
</strong></td></tr>
		<tr><td><?php echo smartyTranslate(array('s'=>'State'),$_smarty_tpl);?>
</td><td><strong><?php echo $_smarty_tpl->tpl_vars['fatturazione']->value['iso_code'];?>
</strong></td></tr>
		<tr><td><?php echo smartyTranslate(array('s'=>'Country'),$_smarty_tpl);?>
</td><td><strong><?php echo $_smarty_tpl->tpl_vars['fatturazione']->value['country'];?>
</strong></td></tr>
		</table>
		</div>
		
		<div style='display:block; float:left; width: 250px; margin-left:15px; text-align:left'>
			<strong><?php echo smartyTranslate(array('s'=>'Shipping address'),$_smarty_tpl);?>
</strong><br /><br />
			<table class="my_account_table">
			<tr><td style="width:80px"><?php echo smartyTranslate(array('s'=>'Name'),$_smarty_tpl);?>
</td><td><strong><?php echo $_smarty_tpl->tpl_vars['spedizione1']->value['firstname'];?>
</strong></td></tr>
			<tr><td><?php echo smartyTranslate(array('s'=>'Last name'),$_smarty_tpl);?>
</td><td><strong><?php echo $_smarty_tpl->tpl_vars['spedizione1']->value['lastname'];?>
</strong></td></tr>
			
			<tr><td><?php echo smartyTranslate(array('s'=>'Address'),$_smarty_tpl);?>
</td><td><strong><?php echo $_smarty_tpl->tpl_vars['spedizione1']->value['address1'];?>
</strong></td></tr>
			<tr><td><?php echo smartyTranslate(array('s'=>'Zip'),$_smarty_tpl);?>
</td><td><strong><?php echo $_smarty_tpl->tpl_vars['spedizione1']->value['postcode'];?>
</strong></td></tr>
			<tr><td><?php echo smartyTranslate(array('s'=>'City'),$_smarty_tpl);?>
</td><td><strong><?php echo $_smarty_tpl->tpl_vars['spedizione1']->value['city'];?>
</strong></td></tr>
			<tr><td><?php echo smartyTranslate(array('s'=>'State'),$_smarty_tpl);?>
</td><td><strong><?php echo $_smarty_tpl->tpl_vars['spedizione1']->value['iso_code'];?>
</strong></td></tr>
			<tr><td><?php echo smartyTranslate(array('s'=>'Country'),$_smarty_tpl);?>
</td><td><strong><?php echo $_smarty_tpl->tpl_vars['spedizione1']->value['country'];?>
</strong></td></tr>
			</table>
		</div>
		
		
			
		
		
			
		
		
		
		<div style='clear:both'></div>
		<br />
		</div>
		
		<?php if ($_smarty_tpl->tpl_vars['fido_tot']->value>0) {?>
		<div class='my-account-block'> 
		<h4 class="h4tab1"  style='text-align:center'><?php echo smartyTranslate(array('s'=>'Area finance'),$_smarty_tpl);?>
</h4>
		
		<p style='text-align:center'>
		<?php echo smartyTranslate(array('s'=>'Hai un credito residuo di'),$_smarty_tpl);?>
<br /><br />
		<span style="font-size:24px; <?php if ($_smarty_tpl->tpl_vars['crediti_fido']->value>0) {?> color:#0c8f2d; <?php } else { ?> <?php }?>"><?php echo $_smarty_tpl->tpl_vars['crediti_fido']->value;?>
 &euro;</span><br /><br />
		Il tuo fido &egrave; di <span><?php echo $_smarty_tpl->tpl_vars['fido']->value;?>
 &euro;</span>
		</p>
			
		</div>
		
		<?php }?>
		
		<div class='my-account-block'> 
		<h4 class="h4tab1" style='text-align:center'><?php echo smartyTranslate(array('s'=>'Account area'),$_smarty_tpl);?>
</h4>
		<table style='width:100%'>
		<tr id="my-profile-tr">
			<td style='width:120px'>
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('identity.php',true);?>
"><img width="36" height="36" src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/account/user.png' alt="<?php echo smartyTranslate(array('s'=>'Information'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'Information'),$_smarty_tpl);?>
" /></a><br />
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('identity.php',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Information'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'My personal information'),$_smarty_tpl);?>
</a>
			</td>
			
			<td style='width:120px'>
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('addresses.php',true);?>
"><img width="36" height="36" src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/account/address.png' alt="<?php echo smartyTranslate(array('s'=>'Addresses'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'Addresses'),$_smarty_tpl);?>
" /></a><br />
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('addresses.php',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Addresses'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'My addresses'),$_smarty_tpl);?>
</a>
			</td>
			
			<td style='width:120px'>
			<a href="<?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
modules/mieofferte/offerte.php"><img src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/account/offerte.png' alt="<?php echo smartyTranslate(array('s'=>'My offers'),$_smarty_tpl);?>
" width="36" height="36" title="<?php echo smartyTranslate(array('s'=>'My offers'),$_smarty_tpl);?>
" /></a><br />
			<a href="<?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
modules/mieofferte/offerte.php" title="<?php echo smartyTranslate(array('s'=>'My offers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'My offers'),$_smarty_tpl);?>
</a>
			</td>
			
			<td style='width:120px'>
			<a href="<?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
modules/mytickets/tickets.php"><img width="36" height="36" src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/account/ticket.png' alt="<?php echo smartyTranslate(array('s'=>'My tickets'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'My tickets'),$_smarty_tpl);?>
" /></a><br />
			<a href="<?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
modules/mytickets/tickets.php" title="<?php echo smartyTranslate(array('s'=>'My tickets'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'My tickets'),$_smarty_tpl);?>
</a>
			</td>
		
			
			
		</tr>
		</table>
			
		</div>

		
		
			<div class='my-account-block'> 
		<h4 class="h4tab1" style='text-align:center'><?php echo smartyTranslate(array('s'=>'Orders area'),$_smarty_tpl);?>
</h4>
		<table style='width:100%'>
		<tr id="my-profile-tr">
			<td style='width:110px'>
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('history.php',true);?>
"><img src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/account/dettagli-ordini.png' alt="<?php echo smartyTranslate(array('s'=>'Orders'),$_smarty_tpl);?>
" width="36" height="36" title="<?php echo smartyTranslate(array('s'=>'Orders'),$_smarty_tpl);?>
" /></a><br />
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('history.php',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Orders'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'History and details of my orders'),$_smarty_tpl);?>
</a>
			</td>
			
			
			
			
			
			<td style='width:110px'>
			<a href="<?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
modules/miefatture/fatture.php"><img src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/account/fatture.png' alt="<?php echo smartyTranslate(array('s'=>'My invoices'),$_smarty_tpl);?>
" width="36" height="36" title="<?php echo smartyTranslate(array('s'=>'My invoices'),$_smarty_tpl);?>
" /></a><br />
			<a href="<?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
modules/miefatture/fatture.php" title="<?php echo smartyTranslate(array('s'=>'My invoices'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'My invoices'),$_smarty_tpl);?>
</a>
			</td>
			
			<td style='width:110px'>
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('order-slip.php',true);?>
"><img src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/account/note.png' alt="<?php echo smartyTranslate(array('s'=>'Credit slips'),$_smarty_tpl);?>
" width="36" height="36" title="<?php echo smartyTranslate(array('s'=>'Credit slips'),$_smarty_tpl);?>
" /></a><br />
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('order-slip.php',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Credit slips'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'My credit slips'),$_smarty_tpl);?>
</a>
			</td>
			
			<td style='width:110px'>
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('discount.php',true);?>
"><img src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/account/coupon.png' alt="<?php echo smartyTranslate(array('s'=>'Vouchers'),$_smarty_tpl);?>
" width="36" height="36" title="<?php echo smartyTranslate(array('s'=>'Vouchers'),$_smarty_tpl);?>
" /></a><br />
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('discount.php',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Vouchers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'My vouchers'),$_smarty_tpl);?>
</a>
			</td>
			<?php if ($_smarty_tpl->tpl_vars['cookie']->value->id_default_group==3) {?>
		<?php } else { ?>
			<td style='width:110px'>
			<a href="<?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
modules/loyalty/loyalty-program.php"><img src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/account/punti.png' alt="<?php echo smartyTranslate(array('s'=>'My loyalty points'),$_smarty_tpl);?>
" width="36" height="36" title="<?php echo smartyTranslate(array('s'=>'My loyalty points'),$_smarty_tpl);?>
" /></a><br />
			<a href="<?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
modules/loyalty/loyalty-program.php" title="<?php echo smartyTranslate(array('s'=>'My loyalty points'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'My loyalty points'),$_smarty_tpl);?>
</a>
			</td>
		<?php }?>
		</tr>
		</table>
		
		</div>

			<div class='my-account-block '> 
		<h4 class="h4tab1" style='text-align:center'><?php echo smartyTranslate(array('s'=>'Contacts area'),$_smarty_tpl);?>
</h4>
		<table style='width:100%; vertical-align:top'>
		<tr id="my-profile-tr">
			<td style='width:110px; vertical-align:top'>
			<a href="<?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
modules/formprevendita/form.php"><img src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/account/quotazione.png' title="<?php echo smartyTranslate(array('s'=>'Products quotation'),$_smarty_tpl);?>
" width="36" height="36" alt="<?php echo smartyTranslate(array('s'=>'Products quotation'),$_smarty_tpl);?>
" /></a><br />
			<a href="<?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
modules/formprevendita/form.php" title="<?php echo smartyTranslate(array('s'=>'Products quotation'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Products quotation'),$_smarty_tpl);?>
</a>
			</td>
			
			<td style='width:110px; vertical-align:top'>
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('contact-form.php',true);?>
?step=contabilita"><img src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/account/amministrazione.png' width="36" height="36" title="<?php echo smartyTranslate(array('s'=>'Administrative assistance'),$_smarty_tpl);?>
" alt="<?php echo smartyTranslate(array('s'=>'Administrative assistance'),$_smarty_tpl);?>
" /></a><br />
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('contact-form.php',true);?>
?step=contabilita" title="<?php echo smartyTranslate(array('s'=>'Administrative assistance'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Administrative assistance'),$_smarty_tpl);?>
</a>
			</td>
			
			<td style='width:110px; vertical-align:top'>
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('contact-form.php',true);?>
?step=assistenza-ordini"><img src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/account/ordini.png' width="36" height="36" title="<?php echo smartyTranslate(array('s'=>'Administrative assistance'),$_smarty_tpl);?>
" alt="<?php echo smartyTranslate(array('s'=>'Orders assistance'),$_smarty_tpl);?>
" /></a><br />
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('contact-form.php',true);?>
?step=assistenza-ordini" title="<?php echo smartyTranslate(array('s'=>'Orders assistance'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Orders assistance'),$_smarty_tpl);?>
</a>
			</td>
			
			<td style='width:110px; vertical-align:top'>
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('contact-form.php',true);?>
?step=tecnica"><img src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/account/assistenza.png' width="36" height="36" title="<?php echo smartyTranslate(array('s'=>'Technical assistance'),$_smarty_tpl);?>
" alt="<?php echo smartyTranslate(array('s'=>'Technical assistance'),$_smarty_tpl);?>
" /></a><br />
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('contact-form.php',true);?>
?step=tecnica" title="<?php echo smartyTranslate(array('s'=>'Technical assistance'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Technical assistance'),$_smarty_tpl);?>
</a>
			</td>
			
			<td style='width:110px; vertical-align:top'>
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('cms.php?id_cms=36',true);?>
"><img src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/account/reso.png' title="<?php echo smartyTranslate(array('s'=>'RMA'),$_smarty_tpl);?>
" width="36" height="36" alt="<?php echo smartyTranslate(array('s'=>'RMA'),$_smarty_tpl);?>
" /></a><br />
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('cms.php?id_cms=36',true);?>
" title="<?php echo smartyTranslate(array('s'=>'RMA'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'RMA'),$_smarty_tpl);?>
</a>
			</td>
		
		
			
			<td style='width:110px; vertical-align:top'>
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('contact-form.php',true);?>
?step=rivenditori"><img src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/account/riivenditori.png' width="36" height="36" title="<?php echo smartyTranslate(array('s'=>'Resellers'),$_smarty_tpl);?>
" alt="<?php echo smartyTranslate(array('s'=>'Resellers'),$_smarty_tpl);?>
" /></a><br />
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('contact-form.php',true);?>
?step=rivenditori" title="<?php echo smartyTranslate(array('s'=>'Resellers'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Resellers'),$_smarty_tpl);?>
</a>
			</td>
		
		</tr>
		
		</table>
			
		</div>
		
		<div style="width: 100%; text-align:center;"> 
		
		<table style='width:100%; vertical-align:top'>
		<tr>
			<td style='width:110px; vertical-align:top'>
			<a href="https://www.ezdirect.it/modules/cancellami/cancella-miei-dati.php"><img width="36" height="36" src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/account/delete.png' alt="<?php echo smartyTranslate(array('s'=>'Data deletion area'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'Data deletion area'),$_smarty_tpl);?>
" /></a><br />
			
			<a href="https://www.ezdirect.it/modules/cancellami/cancella-miei-dati.php"><?php echo smartyTranslate(array('s'=>'Cancella i tuoi dati'),$_smarty_tpl);?>
</a> <br /><br />
			</td>
		
		</tr>
		
		</table>
			
		</div>
		
		</div>
		
		
	</div>	
</div>


<?php }} ?>
