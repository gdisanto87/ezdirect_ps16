<?php /* Smarty version Smarty-3.1.19, created on 2021-11-16 15:07:51
         compiled from "/var/www/html/modules/cheque/views/templates/hook/payment.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1348364596193bb3738a2a1-80565954%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8e685f7e8e852f468d18246062cd10db9904dcaa' => 
    array (
      0 => '/var/www/html/modules/cheque/views/templates/hook/payment.tpl',
      1 => 1635154916,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1348364596193bb3738a2a1-80565954',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_6193bb3738f9e1_68664400',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6193bb3738f9e1_68664400')) {function content_6193bb3738f9e1_68664400($_smarty_tpl) {?>

<p class="payment_module">
	<a style="display: flex; flex-direction: column; text-align: center;" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('cheque','payment',array(),true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'Pay by check','mod'=>'cheque'),$_smarty_tpl);?>
">
		<i style="font-size: 60px; color: #30b1c7;"  class="fas fa-money-check-alt"></i>
		<span style="color: #545454; font-weight: bold; padding-top: 15px;"><?php echo smartyTranslate(array('s'=>'Pay by check','mod'=>'cheque'),$_smarty_tpl);?>
</span>
	</a>
</p>
<?php }} ?>
