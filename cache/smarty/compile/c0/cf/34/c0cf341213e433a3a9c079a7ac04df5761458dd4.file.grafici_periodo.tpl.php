<?php /* Smarty version Smarty-3.1.19, created on 2022-01-04 10:44:39
         compiled from "/var/www/html/ezadmin/themes/default/template/controllers/dashboard/helpers/view/grafici_periodo.tpl" */ ?>
<?php /*%%SmartyHeaderCode:82861080361d417072258b5-85995057%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c0cf341213e433a3a9c079a7ac04df5761458dd4' => 
    array (
      0 => '/var/www/html/ezadmin/themes/default/template/controllers/dashboard/helpers/view/grafici_periodo.tpl',
      1 => 1624528321,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '82861080361d417072258b5-85995057',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'grafici_periodo' => 0,
    'homeStats' => 0,
    'anno_attuale' => 0,
    'i' => 0,
    'mese' => 0,
    'categorie' => 0,
    'costruttori' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61d41707235287_59327331',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61d41707235287_59327331')) {function content_61d41707235287_59327331($_smarty_tpl) {?>

<?php $_smarty_tpl->tpl_vars['categorie'] = new Smarty_variable($_smarty_tpl->tpl_vars['grafici_periodo']->value['categorie'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['costruttori'] = new Smarty_variable($_smarty_tpl->tpl_vars['grafici_periodo']->value['costruttori'], null, 0);?>

<section class="panel" id="seleziona_periodo">
    <form method="post"> 
        <div class="form-row">
            <div class="row">
                <div class="form-group col-md-12" style="margin-bottom:0px;">
                    <label class="col-md-3">Seleziona il periodo:</label>
                    <select class="col-md-3" style="margin-right:10px;" name="select-month" id="select-month">
                        <option value="Jan" <?php if ($_smarty_tpl->tpl_vars['homeStats']->value['period']=='Jan') {?> selected="selected"<?php }?>>Gennaio</option>
                        <option value="Feb" <?php if ($_smarty_tpl->tpl_vars['homeStats']->value['period']=='Feb') {?> selected="selected"<?php }?>>Febbraio</option>
                        <option value="Mar" <?php if ($_smarty_tpl->tpl_vars['homeStats']->value['period']=='Mar') {?> selected="selected"<?php }?>>Marzo</option>
                        <option value="Apr" <?php if ($_smarty_tpl->tpl_vars['homeStats']->value['period']=='Apr') {?> selected="selected"<?php }?>>Aprile</option>
                        <option value="May" <?php if ($_smarty_tpl->tpl_vars['homeStats']->value['period']=='May') {?> selected="selected"<?php }?>>Maggio</option>
                        <option value="Jun" <?php if ($_smarty_tpl->tpl_vars['homeStats']->value['period']=='Jun') {?> selected="selected"<?php }?>>Giugno</option>
                        <option value="Jul" <?php if ($_smarty_tpl->tpl_vars['homeStats']->value['period']=='Jul') {?> selected="selected"<?php }?>>Luglio</option>
                        <option value="Aug" <?php if ($_smarty_tpl->tpl_vars['homeStats']->value['period']=='Aug') {?> selected="selected"<?php }?>>Agosto</option>
                        <option value="Sep" <?php if ($_smarty_tpl->tpl_vars['homeStats']->value['period']=='Sep') {?> selected="selected"<?php }?>>Settembre</option>
                        <option value="Oct" <?php if ($_smarty_tpl->tpl_vars['homeStats']->value['period']=='Oct') {?> selected="selected"<?php }?>>Ottobre</option>
                        <option value="Nov" <?php if ($_smarty_tpl->tpl_vars['homeStats']->value['period']=='Nov') {?> selected="selected"<?php }?>>Novembre</option>
                        <option value="Dec" <?php if ($_smarty_tpl->tpl_vars['homeStats']->value['period']=='Dec') {?> selected="selected"<?php }?>>Dicembre</option>
                        <option value="q1" <?php if ($_smarty_tpl->tpl_vars['homeStats']->value['period']=='q1') {?> selected="selected"<?php }?>>Quarter 1</option>
                        <option value="q2" <?php if ($_smarty_tpl->tpl_vars['homeStats']->value['period']=='q2') {?> selected="selected"<?php }?>>Quarter 2</option>
                        <option value="q3" <?php if ($_smarty_tpl->tpl_vars['homeStats']->value['period']=='q3') {?> selected="selected"<?php }?>>Quarter 3</option>
                        <option value="q4" <?php if ($_smarty_tpl->tpl_vars['homeStats']->value['period']=='q4') {?> selected="selected"<?php }?>>Quarter 4</option>
                        <option value="anno" <?php if ($_smarty_tpl->tpl_vars['homeStats']->value['period']=='anno') {?> selected="selected"<?php }?>>Intero anno</option>
                    </select>
                    <select class="col-md-1" style="margin-right:10px;" name="select-year" id="select-year" style="height:28px;">
                        <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? $_smarty_tpl->tpl_vars['anno_attuale']->value+1 - (2010) : 2010-($_smarty_tpl->tpl_vars['anno_attuale']->value)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 2010, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['homeStats']->value['year']==$_smarty_tpl->tpl_vars['i']->value) {?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                        <?php }} ?>
                    </select>
                    <input type="submit" name="vai-home" value="Vai" class="btn btm-secondary col-md-2" style="margin-right:10px;" />
                    <input type="submit" name="reset-home" value="Resetta" class="btn btm-secondary col-md-2" style="margin-right:10px;" />
                </div>
            </div>
        </div>
	</form>
</section>

<section class="panel" id="grafici_periodo">
    <h3><i class="icon-signal"></i> Statistiche del periodo</h3>

    <section id="grafico_categorie">
        <header><?php echo $_smarty_tpl->tpl_vars['mese']->value;?>
 - Categorie</header>

        <div id="chart_categorie_div"></div>

        <script type="text/javascript">
            
            // Load the Visualization API and the corechart package.
            google.charts.load("current", {"packages":["corechart"]});

            // Set a callback to run when the Google Visualization API is loaded.
            google.charts.setOnLoadCallback(drawChart);

            // Callback that creates and populates a data table, instantiates the pie chart, passes in the data and draws it.
            function drawChart() {
                // Create the data table.
                var data = google.visualization.arrayToDataTable([
                    ['Categoria', 'Vendite'],
                    <?php echo $_smarty_tpl->tpl_vars['categorie']->value['category_data_chart'];?>

                    ['Altre', <?php echo $_smarty_tpl->tpl_vars['categorie']->value['other_category_tot'];?>
]
                ]);

                // Set chart options
                var options = {
                    "title":"Grafico categorie",
                    is3D: true,
                    legend: "labeled",
                    pieSliceText: "none",
                    chartArea:{left:20,top:0,width:"97%",height:"97%"},
                    "width":600,
                    "height":300
                };

                var chart = new google.visualization.PieChart(document.getElementById("chart_categorie_div"));
                chart.draw(data, options);
            }
            
        </script>

    </section>

    <section id="grafico_costruttori">
        <header><?php echo $_smarty_tpl->tpl_vars['mese']->value;?>
 - Costruttori</header>

        <div id="chart_costruttori_div"></div>

        <script type="text/javascript">
            
            // Load the Visualization API and the corechart package.
            // => già fatto nello script precedente

            // Set a callback to run when the Google Visualization API is loaded.
            google.charts.setOnLoadCallback(drawChart2);

            // Callback that creates and populates a data table, instantiates the pie chart, passes in the data and draws it.
            function drawChart2() {
                // Create the data table.
                var data = google.visualization.arrayToDataTable([
                    ['Costruttore', 'Vendite'],
                    <?php echo $_smarty_tpl->tpl_vars['costruttori']->value['manufacturer_data_chart'];?>

                    ['Altri', <?php echo $_smarty_tpl->tpl_vars['costruttori']->value['other_manufacturer_tot'];?>
]
                ]);

                // Set chart options
                var options = {
                    "title":"Grafico costruttori",
                    is3D: true,
                    legend: "labeled",
                    pieSliceText: "none",
                    chartArea:{left:20,top:0,width:"97%",height:"97%"},
                    "width":600,
                    "height":300
                };

                // Instantiate and draw our chart, passing in some options.
                var chart = new google.visualization.PieChart(document.getElementById("chart_costruttori_div"));
                chart.draw(data, options);
            }
            
        </script>

    </section>
</section><?php }} ?>
