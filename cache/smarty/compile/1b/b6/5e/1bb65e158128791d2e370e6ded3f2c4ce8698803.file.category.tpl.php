<?php /* Smarty version Smarty-3.1.19, created on 2022-01-07 19:04:10
         compiled from "/var/www/html/themes/ez20/category.tpl" */ ?>
<?php /*%%SmartyHeaderCode:207550886261d8809a155e74-85063188%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1bb65e158128791d2e370e6ded3f2c4ce8698803' => 
    array (
      0 => '/var/www/html/themes/ez20/category.tpl',
      1 => 1638358120,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '207550886261d8809a155e74-85063188',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'category' => 0,
    'scenes' => 0,
    'subcategories' => 0,
    'subcategory' => 0,
    'link' => 0,
    'img_cat_dir' => 0,
    'mediumSize' => 0,
    'products' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61d8809a1776e7_09522798',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61d8809a1776e7_09522798')) {function content_61d8809a1776e7_09522798($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_regex_replace')) include '/var/www/html/tools/smarty/plugins/modifier.regex_replace.php';
if (!is_callable('smarty_modifier_replace')) include '/var/www/html/tools/smarty/plugins/modifier.replace.php';
?>

<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<div id="wrapper" style="">
	<?php if (isset($_smarty_tpl->tpl_vars['category']->value)) {?>
		<?php if ($_smarty_tpl->tpl_vars['category']->value->id&&$_smarty_tpl->tpl_vars['category']->value->active) {?>

			<h1 class='category'>
				<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['category']->value->name, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</h1>
		
			<?php if ($_smarty_tpl->tpl_vars['scenes']->value) {?>
				<!-- Scenes -->
				<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./scenes.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('scenes'=>$_smarty_tpl->tpl_vars['scenes']->value), 0);?>

			<?php } else { ?>

			<?php }?>
		
			
		
			<div id="category-description">
			<?php if ($_smarty_tpl->tpl_vars['category']->value->description) {?>
				<div class="cat_desc" style="display: flex; width: 100%;">

					

					<div>
						<?php echo smarty_modifier_replace(smarty_modifier_regex_replace($_smarty_tpl->tpl_vars['category']->value->description,"/http\:\/\/www\.ez/","https://www.ez"),'<td>Si</td>','<td><img src="https://www.ezdirect.it/images/si.gif" alt="s&igrave;" title="s&igrave;" width="14" height="14" /></td>');?>

					
						

						
					</div>
					
				</div>
				<div style="clear:both"></div>
			<?php }?>
			</div>

			
			 <?php if (isset($_smarty_tpl->tpl_vars['subcategories']->value)) {?>
			<!-- Subcategories -->
			<div style="display:flex; align-items: center;">
				<div>
					<button id="slideBack" type="button" style="background-color: white; border: none;"><i class="fas fa-chevron-left" style="color: #545454; font-size: 30px;"></i></button>
				</div>
				<div class="scoll-pane" id="subcategories" style="display: flex; align-items: center;">
					
					
					
					<ul class="inline_list" >
						
					<?php  $_smarty_tpl->tpl_vars['subcategory'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['subcategory']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['subcategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['subcategory']->key => $_smarty_tpl->tpl_vars['subcategory']->value) {
$_smarty_tpl->tpl_vars['subcategory']->_loop = true;
?>
						
						<li>
							<a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['subcategory']->value['id_category'],$_smarty_tpl->tpl_vars['subcategory']->value['link_rewrite']), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['subcategory']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
								<?php if ($_smarty_tpl->tpl_vars['subcategory']->value['id_image']) {?>
									<img src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCatImageLink($_smarty_tpl->tpl_vars['subcategory']->value['link_rewrite'],$_smarty_tpl->tpl_vars['subcategory']->value['id_image'],'medium');?>
" alt="" width="90" height="90" style="border:0px; display:block; margin:0 auto" />
								<?php } else { ?>
									<img src="<?php echo $_smarty_tpl->tpl_vars['img_cat_dir']->value;?>
default-medium.jpg" title="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['subcategory']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['subcategory']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" width="<?php echo $_smarty_tpl->tpl_vars['mediumSize']->value['width'];?>
" height="<?php echo $_smarty_tpl->tpl_vars['mediumSize']->value['height'];?>
" />
								<?php }?>
							</a>
							<br />
							<div><a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['subcategory']->value['id_category'],$_smarty_tpl->tpl_vars['subcategory']->value['link_rewrite']), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['subcategory']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</a></div>
						</li>
					<?php } ?>

					
					<?php if ($_smarty_tpl->tpl_vars['category']->value->id==23) {?>
					<li>
						<a href="https://www.ezdirect.it/centralino-virtuale/" title="Centralino virtuale">
							<img src="https://www.ezdirect.it/367442-40898-large/ezcloud-basic-centralino-virtuale-cloud.jpg" alt="" width="90" height="90" style="border:0px; display:block; margin:0 auto" />
						</a>
						<br />
						<a href="https://www.ezdirect.it/centralino-virtuale/">Centralino virtuale</a>
					</li>
					<?php }?>

					
					</ul>
					
					
				
					
					<br class="clear"/>
					
				</div>

				<div>
					
				
					<button id="slide" type="button" style="background-color: white; border: none; margin-left: 30px;"><i class="fas fa-chevron-right" style="color: #545454; font-size: 30px;"></i></button>
				</div>
			</div>
					  <script>
					  	
							var button = document.getElementById('slide');
							button.onclick = function () {
								var container = document.getElementById('subcategories');
								sideScroll(container,'right',25,100,10);
							};

							var back = document.getElementById('slideBack');
							back.onclick = function () {
								var container = document.getElementById('subcategories');
								sideScroll(container,'left',25,100,10);
							};

							function sideScroll(element,direction,speed,distance,step){
								scrollAmount = 0;
								var slideTimer = setInterval(function(){
									if(direction == 'left'){
										element.scrollLeft -= step;
									} else {
										element.scrollLeft += step;
									}
									scrollAmount += step;
									if(scrollAmount >= distance){
										window.clearInterval(slideTimer);
									}
								}, speed);
							}  
						
					</script>
			<?php }?> 

		
	<br />

</div>			
<div class='block_listing_header'>
	

	<div class="articoli" style="display: flex; align-items: baseline; line-height: 30px; color: #737373">
		<i class="fas fa-list" style="padding-left: 10px;"></i>
		<p style="padding: 0px 10px;">Articoli trovati: </p>
		<?php if ($_smarty_tpl->tpl_vars['products']->value) {?>
			&nbsp;&nbsp;&nbsp;<span class="category-product-count"><?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./category-count.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
</span><br />
	</div>
	<div class='block-sort'>
					<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./product-sort.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	</div>
	</div>		
		
					<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./product-list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('products'=>$_smarty_tpl->tpl_vars['products']->value), 0);?>


					<div id="pagination-responsive">
							
						<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


						<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./product-compare.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

					</div>
				<?php } elseif (!isset($_smarty_tpl->tpl_vars['subcategories']->value)) {?>
					<p class="warning"><?php echo smartyTranslate(array('s'=>'There are no products in this category.'),$_smarty_tpl);?>
</p>
				<?php }?>
		<?php } elseif ($_smarty_tpl->tpl_vars['category']->value->id) {?>
			<p class="warning"><?php echo smartyTranslate(array('s'=>'This category is currently unavailable.'),$_smarty_tpl);?>
</p>
		<?php }?>
	<?php }?>
	<br /><br />

		


</div><?php }} ?>
