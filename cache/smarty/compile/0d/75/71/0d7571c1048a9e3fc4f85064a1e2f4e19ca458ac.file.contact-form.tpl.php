<?php /* Smarty version Smarty-3.1.19, created on 2021-12-16 05:39:21
         compiled from "/var/www/html/themes/ez20/contact-form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:123986227561bac2f96ba218-06401989%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0d7571c1048a9e3fc4f85064a1e2f4e19ca458ac' => 
    array (
      0 => '/var/www/html/themes/ez20/contact-form.tpl',
      1 => 1637749857,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '123986227561bac2f96ba218-06401989',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'isLogged' => 0,
    'link' => 0,
    'back' => 0,
    'confirmation' => 0,
    'rma_template' => 0,
    'company' => 0,
    'customer_name' => 0,
    'address' => 0,
    'phone' => 0,
    'fax' => 0,
    'email' => 0,
    'product_name' => 0,
    'fattura' => 0,
    'rma_type' => 0,
    'rma_motivation' => 0,
    'rma_shipping' => 0,
    'base_dir' => 0,
    'img_dir' => 0,
    'alreadySent' => 0,
    'primoPassaggioOk' => 0,
    'tickets' => 0,
    'ticket' => 0,
    'sigla' => 0,
    'anno' => 0,
    'id_ticket' => 0,
    'customerThread' => 0,
    'request_uri' => 0,
    'id_customer' => 0,
    'contacts' => 0,
    'contact' => 0,
    'type' => 0,
    'customerEmail' => 0,
    'addresses' => 0,
    'PS_CATALOG_MODE' => 0,
    'js_dir' => 0,
    'orderedProductList' => 0,
    'rma_product' => 0,
    'invoiceList' => 0,
    'orderList' => 0,
    'ticketList' => 0,
    'cookie' => 0,
    'quantity' => 0,
    'seriale' => 0,
    'tuttiprodotti' => 0,
    'fileupload' => 0,
    'message' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61bac2f9748e77_38121898',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61bac2f9748e77_38121898')) {function content_61bac2f9748e77_38121898($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/html/tools/smarty/plugins/modifier.date_format.php';
?>

<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'Contact'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<script type='text/javascript'>
<?php $_smarty_tpl->tpl_vars["type"] = new Smarty_variable('', null, 0);?>
$(document).ready(function () {
<?php if ($_GET['step']=='contabilita') {?>
<?php $_smarty_tpl->tpl_vars['type'] = new Smarty_variable(2, null, 0);?>
$('option[value="2"]').attr('selected', 'selected').parent().focus();
<?php } elseif ($_GET['step']=='tecnica') {?>
<?php $_smarty_tpl->tpl_vars['type'] = new Smarty_variable(4, null, 0);?>
$('option[value="4"]').attr('selected', 'selected').parent().focus();
<?php } elseif ($_GET['step']=='rivenditori') {?>
<?php $_smarty_tpl->tpl_vars['type'] = new Smarty_variable(3, null, 0);?>
$('option[value="3"]').attr('selected', 'selected').parent().focus();
<?php } elseif ($_GET['step']=='quotazione') {?>
<?php $_smarty_tpl->tpl_vars['type'] = new Smarty_variable(5, null, 0);?>
$('option[value="5"]').attr('selected', 'selected').parent().focus();
<?php } elseif ($_GET['step']=='callmeback') {?>
<?php $_smarty_tpl->tpl_vars['type'] = new Smarty_variable(6, null, 0);?>
$('option[value="6"]').attr('selected', 'selected').parent().focus();
<?php } elseif ($_GET['step']=='assistenza-ordini') {?>
<?php $_smarty_tpl->tpl_vars['type'] = new Smarty_variable(8, null, 0);?>
$('option[value="8"]').attr('selected', 'selected').parent().focus();
<?php } elseif ($_GET['step']=='messaggi') {?>
<?php $_smarty_tpl->tpl_vars['type'] = new Smarty_variable(8, null, 0);?>
$('option[value="7"]').attr('selected', 'selected').parent().focus();
<?php } elseif ($_GET['step']=='rma') {?>
<?php $_smarty_tpl->tpl_vars['type'] = new Smarty_variable(9, null, 0);?>
$('option[value="9"]').attr('selected', 'selected').parent().focus();
<?php } elseif ($_GET['step']=='') {?>
<?php $_smarty_tpl->tpl_vars['type'] = new Smarty_variable(0, null, 0);?>
$('option[value="0"]').attr('selected', 'selected').parent().focus();
<?php }?>
$('#id_contact').trigger('change');
});

	function getOrderId() {
		var productId = document.getElementById('tuttiprodotti').value;
		$.ajax({
			   type: 'GET',
			   url: baseDir + 'contact-form.php',
			   async: true,
			   cache: false,
			   data: '&getOrderId=true&id_product='+productId ,
			   success: function(resp) {
				$('#id_order').val(resp);
			   },
			   error: function(XMLHttpRequest, textStatus, errorThrown)
				{
					alert('ERROR' + textStatus + ' ' + errorThrown);					
				}
		});
 



	}



</script>

<h1><?php echo smartyTranslate(array('s'=>'Customer Service'),$_smarty_tpl);?>
</h1>

<?php if (!$_smarty_tpl->tpl_vars['isLogged']->value) {?>

<?php echo smartyTranslate(array('s'=>'Technical assistance module is only for registered users'),$_smarty_tpl);?>



<div id="authentication_form">
	<div class="block">
		<form action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('authentication.php',true);?>
<?php if ($_GET['cliente']=='rivenditore') {?>?cliente=rivenditore<?php } else { ?><?php }?>" method="post" class="std">
			<fieldset>
			
				<h3><?php echo smartyTranslate(array('s'=>'Create your account'),$_smarty_tpl);?>
</h3>
				<p style="padding:10px"><strong><?php if ($_GET['cliente']=='rivenditore') {?><?php echo smartyTranslate(array('s'=>'Partners and resellers area'),$_smarty_tpl);?>
.<?php } else { ?><?php echo smartyTranslate(array('s'=>'Enter your e-mail address to create an account'),$_smarty_tpl);?>
. <?php }?></strong></p>
				<p style="padding:10px">
			<span><input type="text" id="email_create" name="email_create" style="margin-top:-10px" value="<?php if (isset($_POST['email_create'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['email_create'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php }?>" class="account_input" /></span>
				
				<?php if (isset($_smarty_tpl->tpl_vars['back']->value)) {?><input type="hidden" class="hidden" name="back" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['back']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" /><?php }?>
				<div style="clear:both"></div>
					<input type="submit" id="SubmitCreate" name="SubmitCreate" class="button_large" style="margin-left:12px; margin-top:16px" value="<?php echo smartyTranslate(array('s'=>'Create your account'),$_smarty_tpl);?>
" />
					<input type="hidden" class="hidden" name="SubmitCreate" value="<?php echo smartyTranslate(array('s'=>'Create your account'),$_smarty_tpl);?>
" />
				</p>
				<?php if ($_GET['cliente']=='rivenditore') {?>
				<p style="padding:10px"><strong>
				<?php echo smartyTranslate(array('s'=>'In the next step you will receive the instructions for the reserved area'),$_smarty_tpl);?>
.</strong></p><?php } else { ?><?php }?>
			</fieldset>
		</form>
	</div>
	<div class="block" style="margin-left:20px">
		<form action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('authentication.php',true);?>
<?php if ($_GET['cliente']=='rivenditore') {?>?cliente=rivenditore<?php } else { ?><?php }?>" method="post" class="std">
			<fieldset>
				<h3><?php echo smartyTranslate(array('s'=>'Already registered ?'),$_smarty_tpl);?>
</h3>
				<table class='authentication'>
				<tr>
				<td style='text-align:right; padding-right:10px'><?php echo smartyTranslate(array('s'=>'E-mail address'),$_smarty_tpl);?>
</td>
					<td><input type="text" id="email" name="email" style="width:250px" value="<?php if (isset($_POST['email'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['email'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php }?>" class="account_input" /></td></tr>
				<tr>
				<td style='text-align:right; padding-right:10px'><?php echo smartyTranslate(array('s'=>'Password'),$_smarty_tpl);?>
</td>
					<td><input type="password" id="passwd" name="passwd" style="width:250px; background: transparent url('themes/ezdirect/img/icon/login_password.gif') 0px 50% no-repeat" value="<?php if (isset($_POST['passwd'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['passwd'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php }?>" class="account_input" /></td>
				</tr>
				<tr>
				<td></td>
				<td>
				<br />
					<?php if (isset($_smarty_tpl->tpl_vars['back']->value)) {?><input type="hidden" class="hidden" name="back" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['back']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
			<?php } else { ?><input type="hidden" class="hidden" name="back" value="<?php if ($_GET['id_customer_thread']>0) {?>contact-form.php?id_customer_thread=<?php echo $_GET['id_customer_thread'];?>
&token=<?php echo $_GET['token'];?>
 <?php } else { ?>contact-form.php?step=<?php echo $_GET['step'];?>
<?php }?>" />
			<?php }?>
					<input type="submit" id="SubmitLogin" name="SubmitLogin" class="button_large" value="<?php echo smartyTranslate(array('s'=>'Log in'),$_smarty_tpl);?>
" />
				</td></tr>
				</table>
				<p class="lost_password"><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('password.php');?>
"><?php echo smartyTranslate(array('s'=>'Forgot your password?'),$_smarty_tpl);?>
</a></p>
			</fieldset>
		</form>
	</div>
</div>
<?php } else { ?>


	<?php if (isset($_smarty_tpl->tpl_vars['confirmation']->value)) {?>
		<p><?php echo smartyTranslate(array('s'=>'Your message has been successfully sent to our team.'),$_smarty_tpl);?>
</p>
		
		<?php if (isset($_smarty_tpl->tpl_vars['rma_template']->value)&&!isset($_GET['id_customer_thread'])) {?>
			<div align="center">
				<table border="1" cellpadding="0" style="border-collapse: collapse" width="595" id="table1" bordercolor="#C0C0C0">
				<tr>
				  <td>
				  <table border="0" cellpadding="4" style="border-collapse: collapse" width="100%" id="table2">
					<tr>
					  <td width="170">
					  <img border="0" src="https://www.ezdirect.it/rma/logo_ez.jpg" width="170" height="57"></td>
					  <td align="left" valign="top"><strong><font face="Verdana" size="2"><u>NON 
					  SI ACCETTA MERCE SENZA NUMERO RMA<br />
					  </u></font><font face="Verdana"></font></strong><br />
						<script language="Javascript1.2">
						<!--
						var message = "Stampa RMA";
						function printpage() {
						window.print();  
						}
						document.write("<form><input type=button "
						+"value=\""+message+"\" onClick=\"printpage()\"></form>");

						//-->
						</script>

					</td>
					</tr>
				  </table>
				  <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" width="100%" id="table3" bordercolor="#C0C0C0">
					<tr>
					  <td bordercolor="#F7F7F7" bgcolor="#EBEBEB">
					  <table border="0" cellpadding="0" style="border-collapse: collapse" width="100%" id="table6">
						<tr>
						  <td width="27%" align="left" valign="top"><font face="Verdana" size="2">Ragione Sociale/Nome:</font></td>
						  <td width="73%" align="left" valign="top"><font face="Verdana" size="2"> 
					  <strong><?php echo $_smarty_tpl->tpl_vars['company']->value;?>
</strong></font></td>
						</tr>
						<tr>
						  <td width="27%" align="left" valign="top">
						  <font face="Verdana" size="2">Persona di riferimento</font></td>
						  <td width="73%" align="left" valign="top"> <font face="Verdana" size="2"> 
					  <strong><?php echo $_smarty_tpl->tpl_vars['customer_name']->value;?>
</strong></font></td>
						</tr>
						<tr>
						  <td width="27%" align="left" valign="top">
						  <font face="Verdana" size="2">Indirizzo</font></td>
						  <td width="73%" align="left" valign="top"> <font face="Verdana" size="2"> 
					  <strong><?php echo $_smarty_tpl->tpl_vars['address']->value;?>
</strong></font></td>
						</tr>
						<tr>
						  <td width="27%" align="left" valign="top"><font face="Verdana" size="2">N.Telefono</font></td>
						  <td width="73%" align="left" valign="top"> <strong><font face="Verdana" size="2"><?php echo $_smarty_tpl->tpl_vars['phone']->value;?>
</font></strong></td>
						</tr>
						<tr>
						  <td width="27%" align="left" valign="top"><font face="Verdana" size="2">Fax:</font></td>
						  <td width="73%" align="left" valign="top"><font face="Verdana" size="2"> <strong><?php echo $_smarty_tpl->tpl_vars['fax']->value;?>
</strong></font></td>
						</tr>
						<tr>
						  <td width="27%" align="left" valign="top"><font face="Verdana" size="2">E-Mail:</font></td>
						  <td width="73%" align="left" valign="top"><font face="Verdana" size="2"> <strong><?php echo $_smarty_tpl->tpl_vars['email']->value;?>
</strong></font></td>
						</tr>
						<tr>
						  <td width="27%" align="left" valign="top"><font face="Verdana" size="2">Nome prodotto:</font></td>
						  <td width="73%" align="left" valign="top"><font face="Verdana" size="2"> <strong><?php echo $_smarty_tpl->tpl_vars['product_name']->value;?>
</strong></font></td>
						</tr>
						<tr>
						  <td width="27%" align="left" valign="top"><font face="Verdana" size="2">DDT/fatt.acquisto:</font></td>
						  <td width="73%" align="left" valign="top"><font face="Verdana" size="2"> <strong><?php echo $_smarty_tpl->tpl_vars['fattura']->value;?>
</strong></font></td>
						</tr>
					  </table>
					  </td>
					</tr>
					</table>
				  <p><font face="Verdana"><strong><font size="2">Tipo di RMA:</font>
				  </strong><font size="2"><?php echo $_smarty_tpl->tpl_vars['rma_type']->value;?>
<br />
				  <strong><br />
				  </strong>Motivazione del reso merce o descrizione problema 
				  riscontrato:<strong><br />
				 <?php echo $_smarty_tpl->tpl_vars['rma_motivation']->value;?>
</strong><br /></font></font>
				  <strong><font face="Verdana" size="2"><br />
				  Tipo di trasporto: <?php echo $_smarty_tpl->tpl_vars['rma_shipping']->value;?>

				  </font></strong></p>
				  <table border="0" cellpadding="0" style="border-collapse: collapse" width="100%" id="table4" bgcolor="#000000">
					<tr>
					  <td><strong><font color="#FFFFFF" face="Verdana">AUTORIZZAZIONE AL RESO 
					  (Riservato Ezdirect)</font></strong></td>
					</tr>
				  </table>
				  <p><font size="4" face="Verdana"><strong>Numero RMA </font>
				  <font size="6" face="Verdana"> <br />
				  </font><font size="4" face="Verdana"><br />
				  </font>
				  </strong><font face="Verdana"><strong><font size="2">Note:___________________________________________________________</font></strong><br />
				  &nbsp;</font><table border="0" cellpadding="0" style="border-collapse: collapse" width="100%" id="table5">
					<tr>
					  <td width="33%"><font face="Verdana"><strong><font size="2">Data: 
					  _______________</font></strong></font></td>
					  <td width="67%"><strong><font face="Verdana" size="2">Operatore 
					  Ezdirect:___________________________</font></strong></td>
					  </tr>
					</table>
					<table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" width="100%" id="table7" bordercolor="#000000">
					  <tr>
						<td width="197">&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td width="197"><strong><font face="Verdana" size="2">Data arrivo da cliente</font></strong></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td width="197"><strong><font face="Verdana" size="2">Data invio (reso) a cliente</font></strong></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td width="197"><strong><font face="Verdana" size="2">Verifica confezione</font></strong></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td width="197"><strong><font face="Verdana" size="2">Inviato in 
						riparazione il</font></strong></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td width="197"><strong><font face="Verdana" size="2">Inviato a</font></strong></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td width="197"><strong><font face="Verdana" size="2">DDT N</font></strong></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td width="197"><strong><font face="Verdana" size="2">Esito test</font></strong></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td width="197"><strong><font face="Verdana" size="2">Prove effettuate da</font></strong></td>
						<td>&nbsp;</td>
					  </tr>
				  </table>
				  <p align="center"><font face="Verdana"><strong><font size="2">
					INDICARE IL NUMERO RMA ALL'ESTERNO DELLA CONFEZIONE<br />
					NON UTILIZZARE LA CONFEZIONE ORIGINALE DEL PRODOTTO PER IL TRASPORTO<br />
					<br />
					Ezdirect srl <br />Via Nerino Garbuio Snc 54038 Montignoso Massa Carrara</font></strong></font></td>
				</tr>
			  </table>
			</div>

			<p align="center">&nbsp;</p>
			<div align="center">
			  <table border="0" cellpadding="0" style="border-collapse: collapse" width="595" id="table8" bordercolor="#C0C0C0">
				<tr>
				  <td>
				  <p class="MsoNormal"><strong>
				  <span style="font-size:10.0pt;font-family:Verdana">Grazie per aver 
				  richiesto il modulo RMA.</span></strong><span style="font-size:10.0pt;font-family:Verdana">&nbsp;</span></p>
				  <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Verdana">
				  Per noi &egrave; importante rispondere nel minor tempo possibile alle esigenze 
				  del cliente. <br />
				  Per consentirci di avviare al meglio la procedura di rientro e reso dei 
				  prodotti, ti chiediamo di prestare attenzione ai nostri consigli:<br />
				  <br />
				  <strong>Non utilizzare la confezione originale del prodotto per l'invio, </strong>ma 
				  inseriscila all'interno di un'ulteriore scatola - confezione (In caso di 
				  reso, se il prodotto non &egrave; perfetto in ogni sua parte, quindi anche nel 
				  confezionamento, non sar&agrave; accettato).<br />
				  <strong><br />
				  Esegui l'imballo con cura </strong>proteggendo al meglio i prodotti dalle 
				  conseguenze di eventuali urti accidentali.</span></p>
				  <p class="MsoNormal"><strong>
				  <span style="font-size:10.0pt;font-family:Verdana">Applica la parte 
				  sottostante sulla scatola </span></strong>
				  <span style="font-size:10.0pt;font-family:Verdana">e assicurati che 
				  l'etichetta non sia posticcia (che non si stacchi quindi durante il 
				  trasporto a causa di un nastro&nbsp; poco adesivo)</span></p>
				  <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Verdana">
				
				  Grazie per la collaborazione<br />
				  
				  Staff Ezdirect</span></td>
				</tr>
				<tr>
				  <td>
				  <p align="center">************
				  <span style="font-size: 30.0pt; font-family: Wingdings 2">%</span> 
				  ************ </td>
				</tr>
				<tr>
				  <td><hr size="1">
				  <p><strong><font size="5" face="Verdana">Numero RMA: </font>
				  <font size="7" face="Verdana"></font></strong></p>
				  <hr size="1">
				  <p><strong><font face="Verdana" size="5">Destinatario<br />
				  </font><font face="Verdana" size="7">Ezdirect Srl<br />
				  </font><font face="Verdana" size="5">Via Nerino Garbuio Snc<br />
				  54038 Montignoso (MS)</font></strong></p>
				  <hr size="1">
				  <p><strong><font face="Verdana">Mittente: </font></strong><font face="Verdana" size="2"> 
					  <strong><?php echo $_smarty_tpl->tpl_vars['company']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['customer_name']->value;?>
<br />
				  <?php echo $_smarty_tpl->tpl_vars['address']->value;?>
<br />
				  Telefono: </strong></font> <strong><font face="Verdana" size="2"><?php echo $_smarty_tpl->tpl_vars['phone']->value;?>
</font></strong></p>
				  <p><strong><font face="Verdana" size="2">VETTORE: </font></strong></td>
				</tr>
				<tr>
				  <td>
				  <p align="center">************
				  <span style="font-size: 30.0pt; font-family: Wingdings 2">%</span>************
				  </td>
				</tr>
			  </table>
			</div>
		
		
		
		<?php }?>
		
		
		
		
		
		<ul class="footer_links">
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
"><img class="icon" alt="" src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/home.gif" width="22" height="22" /></a><a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
"><?php echo smartyTranslate(array('s'=>'Home'),$_smarty_tpl);?>
</a></li>
		</ul>
	<?php } elseif (isset($_smarty_tpl->tpl_vars['alreadySent']->value)) {?>
		<p><?php echo smartyTranslate(array('s'=>'Your message has already been sent.'),$_smarty_tpl);?>
</p>
		<ul class="footer_links">
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
"><img class="icon" alt="" src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/home.gif" width="22" height="22" /></a><a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
"><?php echo smartyTranslate(array('s'=>'Home'),$_smarty_tpl);?>
</a></li>
		</ul>
	<?php } else { ?>
	
		<?php if (!($_smarty_tpl->tpl_vars['primoPassaggioOk']->value)&&!($_GET['token'])&&!($_GET['rif'])) {?>
		
			<h2 style="margin-top:16px; font-size:20px">La tua domanda riguarda un caso gi&agrave; trattato? Se s&igrave;, sceglilo da questa lista:</h2><br /><br />
		
			<table class="std">
				<thead>
					<tr>
						<th class="first_item" style="text-align:left"><?php echo smartyTranslate(array('s'=>'Numero ticket'),$_smarty_tpl);?>
</th>
						<th style="text-align:left"><?php echo smartyTranslate(array('s'=>'Cosa riguarda il caso...','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
						<th style="text-align:left"><?php echo smartyTranslate(array('s'=>'Data ultimo messaggio','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
						<th class="last_item" style="text-align:left"><?php echo smartyTranslate(array('s'=>'Rispondi','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
					</tr>
				</thead>
				<tbody>
				<?php  $_smarty_tpl->tpl_vars['ticket'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ticket']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tickets']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ticket']->key => $_smarty_tpl->tpl_vars['ticket']->value) {
$_smarty_tpl->tpl_vars['ticket']->_loop = true;
?>
				
				
				
				
				
				<tr>
					<td><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/mytickets/tickets.php',true);?>
?id_customer_thread=<?php echo $_smarty_tpl->tpl_vars['ticket']->value['id_customer_thread'];?>
<?php if ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']=='preventivo'||$_smarty_tpl->tpl_vars['ticket']->value['id_contact']=='tirichiamiamonoi') {?>&type=pv<?php }?>">
				
			
					<?php $_smarty_tpl->tpl_vars['anno'] = new Smarty_variable(substr($_smarty_tpl->tpl_vars['ticket']->value['date_add'],2,2), null, 0);?>
				
					<?php if ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']==2) {?>
					<?php $_smarty_tpl->tpl_vars['sigla'] = new Smarty_variable('A', null, 0);?>
					<?php } elseif ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']==4) {?>
					<?php $_smarty_tpl->tpl_vars['sigla'] = new Smarty_variable('T', null, 0);?>
					<?php } elseif ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']==3) {?>
					<?php $_smarty_tpl->tpl_vars['sigla'] = new Smarty_variable('V', null, 0);?>
					<?php } elseif ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']==7) {?>
					<?php $_smarty_tpl->tpl_vars['sigla'] = new Smarty_variable('M', null, 0);?>
					<?php } elseif ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']==6) {?>
					<?php $_smarty_tpl->tpl_vars['sigla'] = new Smarty_variable('R', null, 0);?>
					<?php } elseif ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']==8) {?>
					<?php $_smarty_tpl->tpl_vars['sigla'] = new Smarty_variable('D', null, 0);?>
					<?php } elseif ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']==9) {?>
					<?php $_smarty_tpl->tpl_vars['sigla'] = new Smarty_variable('S', null, 0);?>
					<?php } elseif ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']=='preventivo') {?>
					<?php $_smarty_tpl->tpl_vars['sigla'] = new Smarty_variable('P', null, 0);?>
					<?php } elseif ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']=='tirichiamiamonoi') {?>
					<?php $_smarty_tpl->tpl_vars['sigla'] = new Smarty_variable('R', null, 0);?>
					<?php }?>
					
					<?php $_smarty_tpl->tpl_vars['id_ticket'] = new Smarty_variable((($_smarty_tpl->tpl_vars['sigla']->value).($_smarty_tpl->tpl_vars['anno']->value)).($_smarty_tpl->tpl_vars['ticket']->value['id_customer_thread']), null, 0);?>
				
					<?php echo $_smarty_tpl->tpl_vars['id_ticket']->value;?>

					</a>
				</td>
					
					<td><?php echo html_entity_decode($_smarty_tpl->tpl_vars['ticket']->value['message']);?>
</td>
					<td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['ticket']->value['last_msg'],"%d/%m/%Y, %H:%M:%S");?>
</td>
					<td><a class="button_large" style="width: 100px;

height: 20px;

display: block;

text-decoration: none;

vertical-align: middle;

padding-top: 7px;

margin-top: 3px;

margin-bottom: 5px;" href="<?php if ($_smarty_tpl->tpl_vars['ticket']->value['status']=='open'||$_smarty_tpl->tpl_vars['ticket']->value['status']=='pending1') {?>contact-form.php?id_customer_thread=<?php echo $_smarty_tpl->tpl_vars['ticket']->value['id_customer_thread'];?>
&token=<?php echo $_smarty_tpl->tpl_vars['ticket']->value['token'];?>
 <?php } else { ?>contact-form.php?rif=T<?php echo $_smarty_tpl->tpl_vars['ticket']->value['id_customer_thread'];?>
<?php }?>&step=<?php echo $_GET['step'];?>
"><?php echo smartyTranslate(array('s'=>'Apri ticket'),$_smarty_tpl);?>
...</a></td>
					
				</tr>
				</tbody>
			<?php } ?>
			</table>
		
			<form method="post" action="contact-form.php?step=<?php echo $_GET['step'];?>
">
			<input type='hidden' name='primoPassaggioOk' value='<?php echo $_smarty_tpl->tpl_vars['primoPassaggioOk']->value;?>
' />
			<p style="text-align:center">
			<input type="submit" class="button_large" style="width:300px; height:40px; background-color: transparent; color: #ff6600; " value="Se il caso &egrave; nuovo, clicca qui per continuare" />
			</form>
			</p>
			
		<?php } else { ?>
			<?php if ($_smarty_tpl->tpl_vars['customerThread']->value['status']=='closed'&&$_GET['step']=='reopen') {?>
			
				<?php if ($_smarty_tpl->tpl_vars['customerThread']->value['id_contact']==4) {?>
				<?php $_smarty_tpl->tpl_vars['contact_step'] = new Smarty_variable("tecnica", null, 0);?>
				<?php } elseif ($_smarty_tpl->tpl_vars['customerThread']->value['id_contact']==2) {?>
				<?php $_smarty_tpl->tpl_vars['contact_step'] = new Smarty_variable("contabilita", null, 0);?>
				<?php } elseif ($_smarty_tpl->tpl_vars['customerThread']->value['id_contact']==3) {?>
				<?php $_smarty_tpl->tpl_vars['contact_step'] = new Smarty_variable("rivenditori", null, 0);?>
				<?php } elseif ($_smarty_tpl->tpl_vars['customerThread']->value['id_contact']==6) {?>
				<?php $_smarty_tpl->tpl_vars['contact_step'] = new Smarty_variable("callmeback", null, 0);?>
				<?php } elseif ($_smarty_tpl->tpl_vars['customerThread']->value['id_contact']==8) {?>
				<?php $_smarty_tpl->tpl_vars['contact_step'] = new Smarty_variable("assistenza-ordini", null, 0);?>
				<?php } elseif ($_smarty_tpl->tpl_vars['customerThread']->value['id_contact']==7) {?>
				<?php $_smarty_tpl->tpl_vars['contact_step'] = new Smarty_variable("messaggi", null, 0);?>
				<?php } elseif ($_smarty_tpl->tpl_vars['customerThread']->value['id_contact']==9) {?>
				<?php $_smarty_tpl->tpl_vars['contact_step'] = new Smarty_variable("rma", null, 0);?>
				<?php }?>
				
				
					<?php echo smartyTranslate(array('s'=>'You cannot answer to this ticket because it has been closed by our staff'),$_smarty_tpl);?>
. 
					<br /><br />
					<?php echo smartyTranslate(array('s'=>'If you must write something more'),$_smarty_tpl);?>
, <a href="contact-form.php?id_customer_thread=<?php echo $_GET['id_customer_thread'];?>
&token=<?php echo $_GET['token'];?>
&reopen"><?php echo smartyTranslate(array('s'=>'please click on this link in order to reopen this ticket'),$_smarty_tpl);?>
</a>
		
		
			<?php } else { ?>
				<?php if (isset($_GET['step'])) {?>
				<?php } else { ?>
					<p><?php echo smartyTranslate(array('s'=>'For questions about an order or for more information about our products'),$_smarty_tpl);?>
. <br />
					<strong><?php echo smartyTranslate(array('s'=>'Marked fields (*) are required'),$_smarty_tpl);?>
</strong>.</p>
				<?php }?>
					<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

					<form action="<?php echo $_smarty_tpl->tpl_vars['request_uri']->value;?>
" method="post" class="std" enctype="multipart/form-data">
					<input type='hidden' name='id_customer' value='<?php echo $_smarty_tpl->tpl_vars['id_customer']->value;?>
' />
					<input type='hidden' name='primoPassaggioOk' value='<?php echo $_smarty_tpl->tpl_vars['primoPassaggioOk']->value;?>
' />
						<fieldset>
							<p class="select">
								<label for="id_contact"><strong><?php echo smartyTranslate(array('s'=>'Subject Heading'),$_smarty_tpl);?>
</strong></label>
							<?php if (isset($_smarty_tpl->tpl_vars['customerThread']->value['id_contact'])) {?>
								<?php  $_smarty_tpl->tpl_vars['contact'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['contact']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['contacts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['contact']->key => $_smarty_tpl->tpl_vars['contact']->value) {
$_smarty_tpl->tpl_vars['contact']->_loop = true;
?>
									<?php if ($_smarty_tpl->tpl_vars['contact']->value['id_contact']==$_smarty_tpl->tpl_vars['customerThread']->value['id_contact']) {?>
										<input type="text" style="width:400px" id="contact_name" name="contact_name" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['contact']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" readonly="readonly" />
										<input type="hidden" name="id_contact" value="<?php echo $_smarty_tpl->tpl_vars['contact']->value['id_contact'];?>
" />
									<?php }?>
								<?php } ?>
							</p>
							<?php } else { ?>
								<?php if (isset($_GET['step'])&&$_GET['step']!='') {?>
								<input type="hidden" id="id_contact" name="id_contact" style="width:400px" value="<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
" />
									<?php  $_smarty_tpl->tpl_vars['contact'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['contact']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['contacts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['contact']->key => $_smarty_tpl->tpl_vars['contact']->value) {
$_smarty_tpl->tpl_vars['contact']->_loop = true;
?>
										<?php if (isset($_smarty_tpl->tpl_vars['type']->value)&&$_smarty_tpl->tpl_vars['type']->value==$_smarty_tpl->tpl_vars['contact']->value['id_contact']) {?>
											&nbsp;&nbsp;&nbsp;<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['contact']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

										<?php }?>
									<?php } ?>
								
								
								<?php } else { ?>
								
									<select id="id_contact" name="id_contact" style="width:400px" onchange="showElemFromSelect('id_contact', 'desc_contact')">
										<option value="0"><?php echo smartyTranslate(array('s'=>'-- Choose --'),$_smarty_tpl);?>
</option>
									<?php  $_smarty_tpl->tpl_vars['contact'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['contact']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['contacts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['contact']->key => $_smarty_tpl->tpl_vars['contact']->value) {
$_smarty_tpl->tpl_vars['contact']->_loop = true;
?>
									
										<?php if ($_smarty_tpl->tpl_vars['isLogged']->value==1) {?>
											<?php if ($_smarty_tpl->tpl_vars['contact']->value['id_contact']==6||(isset($_POST['id_contact'])&&$_smarty_tpl->tpl_vars['contact']->value['id_contact']==9&&$_POST['id_contact']!=$_smarty_tpl->tpl_vars['contact']->value['id_contact'])||($_smarty_tpl->tpl_vars['contact']->value['id_contact']==9&&$_GET['step']!='rma')) {?>
											<?php } else { ?>
												<option value="<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['id_contact']);?>
" <?php if (isset($_POST['id_contact'])&&$_POST['id_contact']==$_smarty_tpl->tpl_vars['contact']->value['id_contact']) {?>selected="selected"<?php }?>><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['contact']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
											<?php }?>
										
										<?php } else { ?>
											<option value="<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['id_contact']);?>
" <?php if (isset($_POST['id_contact'])&&$_POST['id_contact']==$_smarty_tpl->tpl_vars['contact']->value['id_contact']) {?>selected="selected"<?php }?>><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['contact']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
											
										<?php }?>
									<?php } ?>
									</select>
								
								<br />
								<?php  $_smarty_tpl->tpl_vars['contact'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['contact']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['contacts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['contact']->key => $_smarty_tpl->tpl_vars['contact']->value) {
$_smarty_tpl->tpl_vars['contact']->_loop = true;
?>
									<p id="desc_contact<?php echo intval($_smarty_tpl->tpl_vars['contact']->value['id_contact']);?>
" class="desc_contact" style="display:none; margin-left:235px">
										<?php echo $_smarty_tpl->tpl_vars['contact']->value['description'];?>

									</p>
								<?php } ?>
								<?php }?>
							</p>
							<?php if (!$_GET['step']||$_GET['step']=='messaggi'||$_GET['step']=='') {?>
						
					<?php } else { ?>
					<?php }?>
							<?php }?>
							<p class="text">
								<label for="email"><strong><?php echo smartyTranslate(array('s'=>'E-mail address'),$_smarty_tpl);?>
*</strong></label>
								<?php if (isset($_smarty_tpl->tpl_vars['customerEmail']->value)) {?>
									<input type="text" id="email" style="width:400px" name="from" value="<?php echo $_smarty_tpl->tpl_vars['customerEmail']->value;?>
" readonly="readonly" />
								<?php } else { ?>
									<input type="text" id="email" style="width:400px" name="from" value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
" />
								<?php }?>
							</p>
							<p class="text">
								<label for="phone"><strong><?php echo smartyTranslate(array('s'=>'Phone number'),$_smarty_tpl);?>
*</strong></label>
								<?php if (!empty($_smarty_tpl->tpl_vars['customerThread']->value['phone'])) {?>
									<input type="text" style="width:400px" name="phone" value="<?php echo $_smarty_tpl->tpl_vars['customerThread']->value['phone'];?>
" readonly="readonly" />
								<?php } else { ?>
									<input type="text" style="width:400px" name="phone" value="<?php echo $_smarty_tpl->tpl_vars['phone']->value;?>
" />
								<?php }?>
							</p>
							<?php if ($_smarty_tpl->tpl_vars['contact']->value['id_contact']==9) {?>
								<p class="text">
								<label for="address"><strong><?php echo smartyTranslate(array('s'=>'Please select your address'),$_smarty_tpl);?>
*</strong><br /></label>
								<select name='address' style="width:400px">
								<?php  $_smarty_tpl->tpl_vars['address'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['address']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['addresses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['address']->key => $_smarty_tpl->tpl_vars['address']->value) {
$_smarty_tpl->tpl_vars['address']->_loop = true;
?>
								<option value='<?php echo $_smarty_tpl->tpl_vars['address']->value['id_address'];?>
'><?php echo $_smarty_tpl->tpl_vars['address']->value['address1'];?>
 - <?php echo $_smarty_tpl->tpl_vars['address']->value['postcode'];?>
 <?php echo $_smarty_tpl->tpl_vars['address']->value['city'];?>
 (<?php echo $_smarty_tpl->tpl_vars['address']->value['iso_code'];?>
)</option>
								<?php } ?>
								</select>
							
							<?php }?>
						<?php if (!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
							
							<script src="<?php echo $_smarty_tpl->tpl_vars['js_dir']->value;?>
select2.js" type="text/javascript"></script>
					
								<script type="text/javascript">
								$(document).ready(function() { $("#tuttiprodotti").select2(); });
								
								</script>
								
							
							
							<?php if (isset($_smarty_tpl->tpl_vars['isLogged']->value)&&$_smarty_tpl->tpl_vars['isLogged']->value) {?>
								<p class="text">
								<label for="id_product"><strong><?php echo smartyTranslate(array('s'=>'Product'),$_smarty_tpl);?>
<?php if ($_smarty_tpl->tpl_vars['contact']->value['id_contact']==9) {?>*<?php }?></strong>
								<?php if ($_GET['step']=='tecnica') {?><br />
								<?php echo smartyTranslate(array('s'=>'Please select the product for which you are requesting assistance'),$_smarty_tpl);?>

								<?php } else { ?><br />
								<?php echo smartyTranslate(array('s'=>'Please select a product you have bought or a product in our catalog'),$_smarty_tpl);?>

								<?php }?>
								</label>
								<?php if (empty($_smarty_tpl->tpl_vars['customerThread']->value['id_product'])) {?>
									<select style="display:block; margin-left:10px; width:400px" id="tuttiprodotti" name="id_product"><option value="0"><?php echo smartyTranslate(array('s'=>'-- Choose --'),$_smarty_tpl);?>
</option><?php echo $_smarty_tpl->tpl_vars['orderedProductList']->value;?>
</select>
								<?php } elseif ($_smarty_tpl->tpl_vars['customerThread']->value['id_product']>0) {?>
									<input style="width:400px" type="text" name="id_product" id="id_product" value="<?php echo intval($_smarty_tpl->tpl_vars['customerThread']->value['id_product']);?>
" readonly="readonly" />
								<?php }?>
								</p>
								
								
								<?php if ($_smarty_tpl->tpl_vars['contact']->value['id_contact']==9) {?>
								<br />
								<p class="text">
									<label for="rma_product"><?php echo smartyTranslate(array('s'=>'If the product is not in the list above, please write it in this box'),$_smarty_tpl);?>

									</label>
									<input type="text" style="width:400px" name="rma_product" value="<?php echo $_smarty_tpl->tpl_vars['rma_product']->value;?>
" />
									</p>
								<br />
								<?php }?>
								<?php if ((!isset($_smarty_tpl->tpl_vars['customerThread']->value['id_order'])||$_smarty_tpl->tpl_vars['customerThread']->value['id_order']>0)) {?>
								<p class="text">
								<?php if ($_smarty_tpl->tpl_vars['contact']->value['id_contact']==9) {?>
									<label for="id_order"><strong><?php echo smartyTranslate(array('s'=>'Invoice ID'),$_smarty_tpl);?>
*</strong><br />
									<?php echo smartyTranslate(array('s'=>'Please specify the ID of your invoice'),$_smarty_tpl);?>

								<?php } else { ?>
									<label for="id_order"><strong><?php echo smartyTranslate(array('s'=>'Order ID'),$_smarty_tpl);?>
<?php if ($_smarty_tpl->tpl_vars['contact']->value['id_contact']==9) {?>*<?php }?></strong><br />
									<?php echo smartyTranslate(array('s'=>'Please specify the ID of your order'),$_smarty_tpl);?>

								<?php }?>
									</label>
									<?php if (!isset($_smarty_tpl->tpl_vars['customerThread']->value['id_order'])&&isset($_smarty_tpl->tpl_vars['isLogged']->value)&&$_smarty_tpl->tpl_vars['isLogged']->value==1) {?>
										
										<select name="id_order" style="width:400px" id="id_order" ><option value="0"><?php echo smartyTranslate(array('s'=>'-- Choose --'),$_smarty_tpl);?>
</option>
										<?php if ($_smarty_tpl->tpl_vars['contact']->value['id_contact']==9) {?>
											<?php echo $_smarty_tpl->tpl_vars['invoiceList']->value;?>

										<?php } else { ?>
											<?php echo $_smarty_tpl->tpl_vars['orderList']->value;?>

										<?php }?>
										</select>
									<?php } elseif (!isset($_smarty_tpl->tpl_vars['customerThread']->value['id_order'])&&!isset($_smarty_tpl->tpl_vars['isLogged']->value)) {?>
										<input type="text" name="id_order" style="width:400px" id="id_order" value="<?php if (isset($_smarty_tpl->tpl_vars['customerThread']->value['id_order'])&&$_smarty_tpl->tpl_vars['customerThread']->value['id_order']>0) {?><?php echo intval($_smarty_tpl->tpl_vars['customerThread']->value['id_order']);?>
<?php } else { ?><?php if (isset($_POST['id_order'])) {?><?php echo intval($_POST['id_order']);?>
<?php }?><?php }?>" />
									<?php } elseif ($_smarty_tpl->tpl_vars['customerThread']->value['id_order']>0) {?>
										<input type="text" name="id_order" style="width:400px" id="id_order" value="<?php echo intval($_smarty_tpl->tpl_vars['customerThread']->value['id_order']);?>
" readonly="readonly" />
									<?php }?>
								</p>
								<?php }?>
								
								
								<?php if (empty($_smarty_tpl->tpl_vars['customerThread']->value['id_product'])) {?>
								<p class="text">
								<label for="id_ticket"><strong><?php echo smartyTranslate(array('s'=>'Ticket'),$_smarty_tpl);?>
</strong><br />
								
								<?php echo smartyTranslate(array('s'=>'Please select a ticket'),$_smarty_tpl);?>

								
								</label>
								
									<select style="width:400px" id="id_ticket" name="id_ticket"><option value="0"><?php echo smartyTranslate(array('s'=>'-- Choose --'),$_smarty_tpl);?>
</option><?php echo $_smarty_tpl->tpl_vars['ticketList']->value;?>
</select>
								
								</p>
								<?php }?>
								
								<?php if ($_GET['step']=='tecnica') {?>
								<p class="text">
								<label for="priorita"><strong><?php echo smartyTranslate(array('s'=>'Priority'),$_smarty_tpl);?>
</strong><br />
								
								<?php echo smartyTranslate(array('s'=>'Please select the priority'),$_smarty_tpl);?>

								
								</label>
								
									<select style="width:400px" id="priorita" name="priorita">
									<option value="low"><?php echo smartyTranslate(array('s'=>'Low'),$_smarty_tpl);?>
</option>
									<option value="medium"><?php echo smartyTranslate(array('s'=>'Medium'),$_smarty_tpl);?>
</option>
									<?php if ($_smarty_tpl->tpl_vars['cookie']->value->id_default_group==8||$_smarty_tpl->tpl_vars['cookie']->value->id_default_group==19) {?><option value="high" onclick="var txt; var r = confirm('Si tratta davvero di una richiesta di alta urgenza?'); if (r == true) {
	} else { this.value = 'medium' ;} ">{l s='High'}</option><?php }?>
									
									</select>
								
								</p>
								<?php }?>
								
								<?php if ($_POST['id_contact']==9||($_smarty_tpl->tpl_vars['contact']->value['id_contact']==9&&$_GET['step']=='rma')) {?>
									
								
									<p class="text">
									<label for="quantity"><strong><?php echo smartyTranslate(array('s'=>'Please select product quantity'),$_smarty_tpl);?>
*</strong>
									</label>
									<input type="text" style="width:400px" name="quantity" value="<?php echo $_smarty_tpl->tpl_vars['quantity']->value;?>
" />
									</p>
									
									<p class="text">
									<label for="seriale"><strong><?php echo smartyTranslate(array('s'=>'Please insert the product serial'),$_smarty_tpl);?>
</strong><br />
									<?php echo smartyTranslate(array('s'=>'If your product does not have a serial number, please leave this field blank'),$_smarty_tpl);?>

									</label>
									<input type="text" style="width:400px" name="seriale" value="<?php echo $_smarty_tpl->tpl_vars['seriale']->value;?>
" />
									</p>
									
									<p class="text">
									<label for="quantity"><strong><?php echo smartyTranslate(array('s'=>'Please select the RMA type'),$_smarty_tpl);?>
*</strong>
									</label>
									<div id="desc_rma_type" class="desc_contact" style="margin-left:245px; margin-top:-10px; width:400px; text-align:justify">
										
									<input type="radio" name="rma_type" value="Riparazione" /><strong><?php echo smartyTranslate(array('s'=>'REPAIR'),$_smarty_tpl);?>
</strong><br /><?php echo smartyTranslate(array('s'=>'Customers accept repairing their products if the repairing costs of products without warranty is lower than 20,00 euro including VAT and excluding transportation costs (this rate is applied also for not accepted repair quotations). For costs greater than 20,00 euros, we will send a quotation to your fax number or your email address. If we are not receiving an anwer by two days, your products will be returned with charge of costs.'),$_smarty_tpl);?>
<br /><br />
									<input type="radio" name="rma_type" value="Reso" /><strong><?php echo smartyTranslate(array('s'=>'RETURNED GOODS'),$_smarty_tpl);?>
</strong><br /><?php echo smartyTranslate(array('s'=>'We accept returned goods only if perfectly packaged with all their accessories and if not broken, scratched and in their original packages without stickers and tags other than the originals. PLEASE DO NOT USE the product packaging as container: please insert it in a packing box. The authorization for the returned goods, except in the case of specific indications provided by our staff, is  subject to the paragraphs 5.1, 5.2 and 5.3 of our'),$_smarty_tpl);?>
 <a href='https://www.ezdirect.it/guide/3-termini-e-condizioni-acquisto-on-line-ezdirect#recesso'><?php echo smartyTranslate(array('s'=>'terms and conditions'),$_smarty_tpl);?>
</a>.
									</p>
									</div>
									
									<p class="text">
									<label for="quantity"><strong><?php echo smartyTranslate(array('s'=>'Please select the shipping type'),$_smarty_tpl);?>
*</strong>
									</label>
									<div id="desc_rma_type" class="desc_contact" style="margin-left:245px; margin-top:-10px; width:400px; text-align:justify">
									<input type="radio" name="rma_shipping" value="Corriere Cliente" /><strong><?php echo smartyTranslate(array('s'=>'Customer carrier'),$_smarty_tpl);?>
</strong><br />
									<input type="radio" name="rma_shipping" value="Corriere Ezdirect" /><strong><?php echo smartyTranslate(array('s'=>'Ezdirect carrier - charge of 12 euros VAT excluded for each run.'),$_smarty_tpl);?>
</strong><br />
									<?php echo smartyTranslate(array('s'=>'ATTENTION PLEASE: if you want to use Ezdirect carrier you have to book the pick up calling GLS carrier number 199 151 188'),$_smarty_tpl);?>

									</p>
									</div>
								<?php } else { ?>
								<?php }?>
							<?php } else { ?>
							
								<?php if ($_smarty_tpl->tpl_vars['contact']->value['id_contact']==5) {?>
									
								
									<p class="text">
									<label for="id_product"><strong><?php echo smartyTranslate(array('s'=>'Product'),$_smarty_tpl);?>
</strong>
									<?php if ($_GET['step']=='tecnica') {?><br />
									<?php echo smartyTranslate(array('s'=>'Please select the product for which you are requesting assistance'),$_smarty_tpl);?>

									<?php } else { ?><br />
									<?php echo smartyTranslate(array('s'=>'Please select a product you have bought or a product in our catalog'),$_smarty_tpl);?>

									<?php }?>
									</label>
									<select style="display:block; margin-left:10px; width:400px" id="tuttiprodotti" name="id_product" style="width:300px;"><option value="0"><?php echo smartyTranslate(array('s'=>'-- Choose --'),$_smarty_tpl);?>
</option><?php echo $_smarty_tpl->tpl_vars['tuttiprodotti']->value;?>
</select>
									</p>
									<?php if ((!isset($_smarty_tpl->tpl_vars['customerThread']->value['id_order'])||$_smarty_tpl->tpl_vars['customerThread']->value['id_order']>0)) {?>
									<p class="text">
										<label for="id_order"><strong><?php echo smartyTranslate(array('s'=>'Order ID'),$_smarty_tpl);?>
<?php if ($_smarty_tpl->tpl_vars['contact']->value['id_contact']==9) {?>*<?php }?></strong><br />
										<?php echo smartyTranslate(array('s'=>'Please specify the ID of your order'),$_smarty_tpl);?>

										
										</label>
										<?php if (!isset($_smarty_tpl->tpl_vars['customerThread']->value['id_order'])&&isset($_smarty_tpl->tpl_vars['isLogged']->value)&&$_smarty_tpl->tpl_vars['isLogged']->value==1) {?>
											<select name="id_order" style="width:400px" id="id_order" ><option value="0"><?php echo smartyTranslate(array('s'=>'-- Choose --'),$_smarty_tpl);?>
</option><?php echo $_smarty_tpl->tpl_vars['orderList']->value;?>
</select>
										<?php } elseif (!isset($_smarty_tpl->tpl_vars['customerThread']->value['id_order'])&&!isset($_smarty_tpl->tpl_vars['isLogged']->value)) {?>
											<input type="text" name="id_order" style="width:400px" id="id_order" value="<?php if (isset($_smarty_tpl->tpl_vars['customerThread']->value['id_order'])&&$_smarty_tpl->tpl_vars['customerThread']->value['id_order']>0) {?><?php echo intval($_smarty_tpl->tpl_vars['customerThread']->value['id_order']);?>
<?php } else { ?><?php if (isset($_POST['id_order'])) {?><?php echo intval($_POST['id_order']);?>
<?php }?><?php }?>" />
										<?php } elseif ($_smarty_tpl->tpl_vars['customerThread']->value['id_order']>0) {?>
											<input type="text" name="id_order" style="width:400px" id="id_order" value="<?php echo intval($_smarty_tpl->tpl_vars['customerThread']->value['id_order']);?>
" readonly="readonly" />
										<?php }?>
									</p>
									<?php }?>
								<?php } else { ?>
								<?php }?>
								
								
							
							<?php }?>
							
							
							
						<?php }?>
						<?php if ($_smarty_tpl->tpl_vars['fileupload']->value==1) {?>
							<p class="text">
							<label for="fileUpload"><strong><?php echo smartyTranslate(array('s'=>'Attach File'),$_smarty_tpl);?>
</strong><br />
							<?php echo smartyTranslate(array('s'=>'Use this field if you want to send us files'),$_smarty_tpl);?>

							</label>
								<input type="hidden" name="MAX_FILE_SIZE" value="200000000" />
								<input style="width:400px" type="file" name="fileUpload[]" id="fileUpload1" /><br />
								<input style="width:400px; margin-top:2px" type="file" name="fileUpload[]" id="fileUpload2" />
							</p>
						<?php }?>
						<p class="textarea">
							<?php if ($_smarty_tpl->tpl_vars['contact']->value['id_contact']==9) {?>
							<label for="message"><strong><?php echo smartyTranslate(array('s'=>'Please insert a reason for your RMA'),$_smarty_tpl);?>
*</strong><br />
							<?php } else { ?>
							<label for="message"><strong><?php echo smartyTranslate(array('s'=>'Notes'),$_smarty_tpl);?>
*</strong><br />
							<?php }?>
							<?php echo smartyTranslate(array('s'=>'Message'),$_smarty_tpl);?>
</label>
							 <textarea id="message" name="message" rows="15" cols="20" style="width:400px;height:220px"><?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['message']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php }?></textarea>
						</p>
						
						<p class="text">
								<label for="privacy"><strong><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('privacy.php',true);?>
" target="_blank"><?php echo smartyTranslate(array('s'=>'I accept privacy terms'),$_smarty_tpl);?>
</a>*</strong><br />
								</label>
								 <input type="checkbox" name="privacy" />
							</p>
						
						<p class="submit">
							<input type="submit" name="submitMessage" id="submitMessage" value="<?php echo smartyTranslate(array('s'=>'Send'),$_smarty_tpl);?>
" class="button_large" onclick="$(this).hide(); fbq('track', 'Contact');" />
						</p>
					</fieldset>
				</form>
			<?php }?>
		<?php }?>	
	<?php }?>
<?php }?><?php }} ?>
