<?php /* Smarty version Smarty-3.1.19, created on 2021-12-05 09:59:11
         compiled from "/var/www/html/ezadmin/themes/default/template/controllers/prodotti_amazon/helpers/view/view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:44325142461ac7f5fba7a71-83873338%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6237bb10991acb4d318962e997c28e78b0de3af1' => 
    array (
      0 => '/var/www/html/ezadmin/themes/default/template/controllers/prodotti_amazon/helpers/view/view.tpl',
      1 => 1627660244,
      2 => 'file',
    ),
    '65752a427c94173eeeb5b077cba376b60bae6f03' => 
    array (
      0 => '/var/www/html/ezadmin/themes/default/template/helpers/view/view.tpl',
      1 => 1604653169,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '44325142461ac7f5fba7a71-83873338',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'name_controller' => 0,
    'hookName' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61ac7f5fbce863_51916600',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61ac7f5fbce863_51916600')) {function content_61ac7f5fbce863_51916600($_smarty_tpl) {?>

<div class="leadin"></div>



<div id="container-prodotti-amazon">
	<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" novalidate>
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-shopping-cart"></i> Prodotti Amazon
			</div>

			<p>In questa tabella si possono vedere i prodotti attivi su Amazon. Sono presenti tutti i prodotti che hanno il flag sul nostro portale per l'uscita su Amazon (almeno uno).</p>

			<p><span class="thick">LEGENDA</span></p>
			<p>Flag verde (<i class="icon-check" style="color:green"></i>) = il prodotto è attivo sul marketplace relativo alla colonna (IT, FR ecc.) in cui si trova.</p>
			<p>Flag grigio (<i class="icon-check"></i>) = il prodotto ha il flag per essere pubblicato su Amazon ma non è attivo nel marketplace relativo alla colonna.</p>
			<p>Numero vicino al flag = quantità pubblicata sul marketplace.</p>
			<p>La colonna "Tipo QT" indica che tipo di disponibilità pubblichiamo su Amazon. Se "EZ", su Amazon pubblichiamo solo disponibilità del magazzino interno. Se "EZ+F", pubblichiamo disponibilità magazzino interno e magazzino fornitore principale.</p>
			<p>Un prodotto può avere i flag ma non essere attivo sul marketplace per vari motivi (non disponibile in magazzino, descrizione troppo lunga per Amazon, EAN assente, rifiuto di Amazon per altri motivi).</p>
			<br />
			<p><span class="thick">Scorrendo il mouse sul logo prime, si può vedere il prezzo che il prodotto ha su Prime.</span></p>
			<br />
			<p><span class="rosso">ATTENZIONE: i prodotti si aggiornano due volte al giorno (alle 07:05 e alle 23:05). Se si vede solo una colonna con i flag verdi (o due, o comunque non tutte) significa che la procedura è in corso di aggiornamento, quindi attendere qualche minuto e poi aggiornare con F5. La procedura può richiedere anche 20-30 minuti per completarsi causa tempi di Amazon.</span></p>
			<br />

		<?php if ($_POST['cerca']===null) {?>

			<p><span class="thick">Seleziona i prodotti con i filtri sotto</span> (lasciali vuoti se vuoi la lista completa di tutti i prodotti):</p>
			<form>
				<div class="form-group">
					<label>Per codice prodotto</label>
					<select id="per_prodotto" name="per_prodotto[]" class="form-control textbox-fix select2mul" multiple="multiple">
						<?php if (count($_smarty_tpl->tpl_vars['prodotti']->value)) {?>
							<?php  $_smarty_tpl->tpl_vars['row'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['row']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['prodotti']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['row']->key => $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['row']->key;
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['name'];?>
 (<?php echo $_smarty_tpl->tpl_vars['row']->value['reference'];?>
) - ID <?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
</option>
							<?php } ?>
						<?php }?>
					</select>
				</div>

				<div class="form-group">
					<label>Per macrocategoria</label>
					<select id="per_macrocategoria" name="per_macrocategoria[]" class="form-control textbox-fix select2mul" multiple="multiple">
						<?php if (count($_smarty_tpl->tpl_vars['macrocat']->value)) {?>
							<?php  $_smarty_tpl->tpl_vars['row'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['row']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['macrocat']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['row']->key => $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['row']->key;
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['name'];?>
</option>
							<?php } ?>
						<?php }?>
					</select>
				</div>

				<div class="form-group">
					<label>Per sottocategoria</label>
					<select id="per_sottocategoria" name="per_sottocategoria[]" class="form-control textbox-fix select2mul" multiple="multiple">
						<?php if (count($_smarty_tpl->tpl_vars['sottocat']->value)) {?>
							<?php  $_smarty_tpl->tpl_vars['row'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['row']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['sottocat']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['row']->key => $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['row']->key;
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['name'];?>
</option>
							<?php } ?>
						<?php }?>
					</select>
				</div>

				<div class="form-group">
					<label>Per marchio</label>
					<select id="per_marchio" name="per_marchio[]" class="form-control textbox-fix select2mul" multiple="multiple">
						<?php if (count($_smarty_tpl->tpl_vars['marchi']->value)) {?>
							<?php  $_smarty_tpl->tpl_vars['row'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['row']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['marchi']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['row']->key => $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['row']->key;
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['name'];?>
</option>
							<?php } ?>
						<?php }?>
					</select>
				</div>

				<button type="submit" class="btn btn-default" name="cerca">Cerca</button>
			</form>

		<?php } else { ?>

			<?php if ($_smarty_tpl->tpl_vars['striscia_prodotti']->value||$_smarty_tpl->tpl_vars['striscia_macrocat']->value||$_smarty_tpl->tpl_vars['striscia_cat']->value||$_smarty_tpl->tpl_vars['striscia_marchi']->value) {?>
				<p><span class="thick">Hai cercato...</span>
				<br />
				<?php if ($_smarty_tpl->tpl_vars['striscia_prodotti']->value) {?>
					<p><?php echo $_smarty_tpl->tpl_vars['striscia_prodotti']->value;?>
</p>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['striscia_macrocat']->value) {?>
					<p><?php echo $_smarty_tpl->tpl_vars['striscia_macrocat']->value;?>
</p>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['striscia_cat']->value) {?>
					<p><?php echo $_smarty_tpl->tpl_vars['striscia_cat']->value;?>
</p>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['striscia_marchi']->value) {?>
					<p><?php echo $_smarty_tpl->tpl_vars['striscia_marchi']->value;?>
</p>
				<?php }?>
				<br />
			<?php }?>

			<div class="row">
				<div class="form-group col-md-12">
					<table class="table tabella_datatable" id="tabella_prodotti_amazon">
						<thead>
							<tr>
								<th><span class="title_box ">Codice</span></th>
								<th><span class="title_box ">Codice SKU</span></th>
								<th><span class="title_box ">EAN</span></th>
								<th><span class="title_box ">ASIN</span></th>
								<th><span class="title_box ">Prodotto</span></th>
								<th><span class="title_box ">Costruttore</span></th>
								<th><span class="title_box ">Mag. EZ</span></th>
								<th><span class="title_box ">Mag. Forn.</span></th>
								<th><span class="title_box ">Tipo QT</span></th>
								<th><span class="title_box ">Prezzo IT</span></th>
								<th><span class="title_box ">Mag. Amz.</span></th>
								<th><span class="title_box ">Scorta min. Amz.</span></th>
								<th><span class="title_box ">IT</span></th>
								<th><span class="title_box ">FR</span></th>
								<th><span class="title_box ">UK</span></th>
								<th><span class="title_box ">DE</span></th>
								<th><span class="title_box ">ES</span></th>
								<th><span class="title_box ">NL</span></th>
								<th><span class="title_box ">SE</span></th>
								<th><span class="title_box ">PL</span></th>
							</tr>
						</thead>
						<tbody>
							<?php  $_smarty_tpl->tpl_vars['row'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['row']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['lista_prodotti']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['row']->key => $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['row']->key;
?>
							<tr style="<?php echo $_smarty_tpl->tpl_vars['row']->value['row_style'];?>
">
								<td><a class="tooltip_prodotto" title="<?php echo $_smarty_tpl->tpl_vars['row']->value['tooltip'];?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminProducts'), ENT_QUOTES, 'UTF-8', true);?>
&amp;id_product=<?php echo intval($_smarty_tpl->tpl_vars['row']->value['id']);?>
&amp;updateproduct" target="_blank"><?php echo $_smarty_tpl->tpl_vars['row']->value['reference'];?>
</a></td>
								<td><?php echo $_smarty_tpl->tpl_vars['row']->value["supplier_reference"];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['row']->value["ean13"];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['row']->value["asin"];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['row']->value["name"];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['row']->value["manufacturer"];?>
 <?php echo $_smarty_tpl->tpl_vars['row']->value["manufacturer_prime"];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['row']->value["stock_quantity"];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['row']->value["quantita_fornitore"];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['row']->value["tipo_qt"];?>
</td>
								<td style="<?php echo $_smarty_tpl->tpl_vars['row']->value['prezzo_it_style'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value["prezzo_it"];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['row']->value["amazon_quantity"];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['row']->value["scorta_minima_amazon"];?>
</td>
								<td><?php echo $_smarty_tpl->tpl_vars['row']->value["it"];?>
 </td>
								<td><?php echo $_smarty_tpl->tpl_vars['row']->value["fr"];?>
 </td>
								<td><?php echo $_smarty_tpl->tpl_vars['row']->value["uk"];?>
 </td>
								<td><?php echo $_smarty_tpl->tpl_vars['row']->value["de"];?>
 </td>
								<td><?php echo $_smarty_tpl->tpl_vars['row']->value["es"];?>
 </td>
								<td><?php echo $_smarty_tpl->tpl_vars['row']->value["nl"];?>
 </td>
								<td><?php echo $_smarty_tpl->tpl_vars['row']->value["se"];?>
 </td>
								<td><?php echo $_smarty_tpl->tpl_vars['row']->value["pl"];?>
 </td>
							</tr>
							<?php } ?>
						</tbody>
					</table>

					<hr />

					<a class="btn btn-default" href="">
						Esporta i dati in formato Excel
					</a>
					
					<a class="btn btn-default" href="">
						Nuova ricerca
					</a>
				</div>
			</div>
		<?php }?>

		</div>
	</form>
</div>



<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayAdminView'),$_smarty_tpl);?>

<?php if (isset($_smarty_tpl->tpl_vars['name_controller']->value)) {?>
	<?php $_smarty_tpl->_capture_stack[0][] = array('hookName', 'hookName', null); ob_start(); ?>display<?php echo ucfirst($_smarty_tpl->tpl_vars['name_controller']->value);?>
View<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
	<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>$_smarty_tpl->tpl_vars['hookName']->value),$_smarty_tpl);?>

<?php } elseif (isset($_GET['controller'])) {?>
	<?php $_smarty_tpl->_capture_stack[0][] = array('hookName', 'hookName', null); ob_start(); ?>display<?php echo htmlentities(ucfirst($_GET['controller']));?>
View<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
	<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>$_smarty_tpl->tpl_vars['hookName']->value),$_smarty_tpl);?>

<?php }?>
<?php }} ?>
