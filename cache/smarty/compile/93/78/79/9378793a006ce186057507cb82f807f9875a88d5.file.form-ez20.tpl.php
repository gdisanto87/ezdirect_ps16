<?php /* Smarty version Smarty-3.1.19, created on 2021-12-13 17:37:50
         compiled from "/var/www/html/modules/formprevendita/form-ez20.tpl" */ ?>
<?php /*%%SmartyHeaderCode:142070086861b776de419422-64663260%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9378793a006ce186057507cb82f807f9875a88d5' => 
    array (
      0 => '/var/www/html/modules/formprevendita/form-ez20.tpl',
      1 => 1639413349,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '142070086861b776de419422-64663260',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'back' => 0,
    'countries' => 0,
    'country' => 0,
    'state' => 0,
    'address' => 0,
    'provincia' => 0,
    'vat_management' => 0,
    'inviato' => 0,
    'id_thread' => 0,
    'firstname' => 0,
    'lastname' => 0,
    'company' => 0,
    'vat_number' => 0,
    'tax_code' => 0,
    'phone' => 0,
    'email' => 0,
    'address1' => 0,
    'postcode' => 0,
    'city' => 0,
    'v' => 0,
    'paese' => 0,
    'categoryric' => 0,
    'prodottorichiesto' => 0,
    'category' => 0,
    'message' => 0,
    'base_dir' => 0,
    'img_dir' => 0,
    'idrichiesta' => 0,
    'categoria' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61b776de46f746_89370256',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61b776de46f746_89370256')) {function content_61b776de46f746_89370256($_smarty_tpl) {?>



<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'Contact','mod'=>'formprevendita'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<script type='text/javascript'>
$(document).ready(function(){
	$('select#id_country').change(function(){
		updateState();
		updateNeedIDNumber();
		updateZipCode();
	});
	
	$('select#id_country2').change(function(){
		updateState2();
		updateNeedIDNumber();
		updateZipCode();
	});
	
	updateState2();
	updateState();
	
	updateNeedIDNumber();
	updateZipCode();
	
	if ($('select#id_country_invoice').length != 0)
	{
		$('select#id_country_invoice').change(function(){
			updateState('invoice');
			updateNeedIDNumber('invoice');
			updateZipCode();
		});
		if ($('select#id_country_invoice:visible').length != 0)
		{
			updateState('invoice');
			updateNeedIDNumber('invoice');
			updateZipCode('invoice');
		}
	}
});

function updateState(suffix)
{
	$('select#id_state'+(suffix !== undefined ? '_'+suffix : '')+' option:not(:first-child)').remove();
	var states = countries[$('select#id_country'+(suffix !== undefined ? '_'+suffix : '')).val()];
	if(typeof(states) != 'undefined')
	{
		$(states).each(function (key, item){
			$('select#id_state'+(suffix !== undefined ? '_'+suffix : '')).append('<option value="'+item.id+'"'+ (idSelectedCountry == item.id ? ' selected="selected"' : '') + '>'+item.name+'</option>');
		});
		
		$('p.id_state'+(suffix !== undefined ? '_'+suffix : '')+':hidden').slideDown('slow');
	}
	else
		$('p.id_state'+(suffix !== undefined ? '_'+suffix : '')).slideUp('fast');
		
}	
	
function updateState2(suffix)
{
	$('select#id_state2'+(suffix !== undefined ? '_'+suffix : '')+' option:not(:first-child)').remove();
	var states = countries[$('select#id_country2'+(suffix !== undefined ? '_'+suffix : '')).val()];
	if(typeof(states) != 'undefined')
	{
		$(states).each(function (key, item){
			$('select#id_state2'+(suffix !== undefined ? '_'+suffix : '')).append('<option value="'+item.id+'"'+ (idSelectedCountry == item.id ? ' selected="selected"' : '') + '>'+item.name+'</option>');
		});
		
		$('p.id_state'+(suffix !== undefined ? '_'+suffix : '')+':hidden').slideDown('slow');
	}
	else
		$('p.id_state'+(suffix !== undefined ? '_'+suffix : '')).slideUp('fast');
		
}


function updateNeedIDNumber(suffix)
{
	var idCountry = parseInt($('select#id_country'+(suffix !== undefined ? '_'+suffix : '')).val());

	if ($.inArray(idCountry, countriesNeedIDNumber) >= 0)
		$('.dni'+(suffix !== undefined ? '_'+suffix : '')).slideDown('slow');
	else
		$('.dni'+(suffix !== undefined ? '_'+suffix : '')).slideUp('fast');
}

function updateZipCode(suffix)
{
	var idCountry = parseInt($('select#id_country'+(suffix !== undefined ? '_'+suffix : '')).val());
	
	var idCountry = parseInt($('select#id_country2'+(suffix !== undefined ? '_'+suffix : '')).val());
	
	if (countriesNeedZipCode[idCountry] != 0)
		$('.postcode'+(suffix !== undefined ? '_'+suffix : '')).slideDown('slow');
	else
		$('.postcode'+(suffix !== undefined ? '_'+suffix : '')).slideUp('fast');
}
</script>

<?php if (!isset($_GET['step'])) {?>

	<?php if (isset($_GET['category'])) {?>
	<?php $_smarty_tpl->tpl_vars['category'] = new Smarty_variable($_GET['category'], null, 0);?>
	<?php } elseif (isset($_POST['category'])) {?>
	<?php $_smarty_tpl->tpl_vars['category'] = new Smarty_variable($_POST['category'], null, 0);?>
	<?php } else { ?>
	<?php $_smarty_tpl->tpl_vars['category'] = new Smarty_variable('', null, 0);?>
	<?php }?>
	
		<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			<style type="text/css">
	#account_list li {
	margin-top:10px;
	font-size:16px;
	margin-bottom:15px;
	width:77%;
}
		</style>
		<div id="preventivo_h" style="height: 584px;
margin-top: -29px;
background-image: url('/themes/ez20/img/preventivo-bg.jpg');
padding-top: 20px;
padding-left: 10px;background-position: top right;
background-repeat: no-repeat;">
		<br />
		<h1 style="font-size:24px; text-align:left">Richiedi subito <br />il tuo preventivo gratuito</h1>
		
			<br /><br />
		<div>
		<strong style="font-size:16px">Perché scegliere noi</strong>

		<ul id="account_list" style="margin-left:10px; list-style-type:none">
		<li>✔ Oltre 30 anni di esperienza</li>
		<li>✔ Try & Buy su molti prodotti</li>
		<li>✔ 10.000 prodotti a catalogo</li>
		<li>✔ 60.000 clienti soddisfatti</li>
		<li>✔ Garanzia soddisfatto al 100% su tutti i preventivi</li>
		<li>✔ Pre-Configurazione prodotti</li>
		<li>✔ Supporto da remoto</li>
		<li>✔ Installazione On Site in tutta Italia</li>
		<li>✔ Assistenza postvendita</li>
		<li>✔ Pagamenti differiti  - Noleggio</li>
		</ul>
		</div>	
</div>		
			

		
			<div id="div-preventivo">
				<div id="form-preventivo">
				<table style="border: 0px none; width: 100%; display: block; margin-left: auto; margin-right: auto;">
<tbody>
<tr>
<td style="vertical-align:top; border: 0px; width: 48%; padding-right: 2%; padding-bottom: 3%;">
<h2 style="color: #009; text-align: left; font-size: 20px;">Sei gi&agrave; registrato?</h2>
<h3 style="text-align: left; margin-top: 3px; color: #009;">Accedi con il tuo account</h3>
<div style="width: 100%; padding: 5px; background-color: #ebebeb;">
<div><span style="font-size: 14px; font-family: arial,helvetica,sans-serif;">&nbsp;</span></div>
<div style='text-align:left'>Se sei cliente gi&agrave; registrato, fai login con le credenziali del tuo account (email e password)</div>
					
					
						<form action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('authentication.php',true);?>
" method="post" id="login_form" class="std">
						
						<table class='authentication'>
				<tr>
				<td style='text-align:right; padding-right:10px'><?php echo smartyTranslate(array('s'=>'E-mail address'),$_smarty_tpl);?>
</td>
					<td><input type="text" id="email" name="email" style="background: #fff;
border: 1px solid #ddd;
height: 30px;
width: 100%;
padding: 3px;" value="<?php if (isset($_POST['email'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['email'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php }?>" class="account_input" /></td></tr>
				<tr>
				<td style='text-align:right; padding-right:10px'><?php echo smartyTranslate(array('s'=>'Password'),$_smarty_tpl);?>
</td>
					<td><input type="password" id="passwd" name="passwd" style="background: #fff;
border: 1px solid #ddd;
height: 30px;
width: 100%;
padding: 3px;" value="<?php if (isset($_POST['passwd'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['passwd'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php }?>" class="account_input" /></td>
				</tr>
				<tr>
				<td colspan="2" style="text-align:center">
				<br />
					<input type="hidden" class="hidden" name="category" value="<?php echo $_GET['category'];?>
" />
					<input type="hidden" class="hidden" name="back" value="preventivi-quotazioni-miglior-prezzo-cuffie-centralini<?php if (isset($_GET['category'])) {?>?category=<?php echo $_GET['category'];?>
<?php }?>" />
					<input type="hidden" class="bottone-preventivo" value=""  id="SubmitLogin" name="SubmitLogin" />
					<?php if (isset($_smarty_tpl->tpl_vars['back']->value)) {?><input type="hidden" class="hidden" name="back" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['back']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" /><?php }?>
					<input type="submit" style="width:90%" class="button_large" value="Richiedi preventivo"  id="SubmitLogin2" name="SubmitLogin" />
				</td></tr>
				</table>
				
					
			</form>
			<div style='text-align:center'><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('password.php');?>
" style="" target="_blank">Hai dimenticato la password? Clicca qui</a></div>
</div>
</td>
<td style="vertical-align:top;  border: 0px; width: 48%; padding-left: 2%;">
<h2 style="color: #009; text-align: left; font-size: 20px;">Non sei registrato?</h2>
<h3 style="text-align: left; margin-top: 3px; color: #009;">Registrati come nostro cliente</h3>
<div style="width: 98%; padding: 5px; background-color: #ebebeb;">
<div><span style="font-size: 12px;"><span style="font-size: 14px; font-family: arial,helvetica,sans-serif;">&nbsp;</span></span></div>
<div style="text-align: left;"><span style="font-size: 12px;"><span style="font-size: 14px; font-family: arial,helvetica,sans-serif;">Se non sei registrato, clicca sul pulsante sotto.</span></span></div>
<div><span style="font-size: 16px;">&nbsp;</span></div>
<form action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/formprevendita/form.php',true);?>
?step=2" method="post" class="std">
<input type="hidden" class="hidden" name="category" value="<?php echo $_GET['category'];?>
" />
					<input type="hidden" class="bottone-preventivo" id="SubmitPreventivo" value="" />
<div><input type="submit" class="button_large" style="width: 230px; display: block; margin: 0 auto; height: 30px; padding-top: 5px; background-color: #000099;" value="Richiedi preventivo"  id="SubmitPreventivo2" name="SubmitLogin" /><br /></div>

					</form>
</div>
</td>
</tr>

</tbody>
</table>

</div>
				
				
<h2>Garanzia soddisfatto 100%</h2><br />
<div>
Per tutelare il nostro cliente, che ripone in noi la sua fiducia e nutre aspettative di servizi di alto livello, è valida la garanzia "preventivo soddisfatto o rimborsato al 100%". È possibile sostituire il prodotto fornito con altro di pari o superiore valore o procedere alla restituzione e richiedere il rimborso, qualora la configurazione proposta non sia in linea con le esigenze espresse dal cliente. Garanzia accessoria, riservata ad imprese, professionisti, possessori di P.IVA e P.A..
</div><br /><br />
<h2>
Oltre 30 anni di esperienza<br />
</h2>
<div>
Non lasciamo nulla al caso. Analizziamo le tue esigenze, proponiamo la soluzione migliore, al miglior prezzo, riducendo a zero il margine di errore. Questo per noi significa salvaguardare il tuo investimento, migliorare la produttività della tua azienda, meritare la tua fiducia.
	</div>	
		
		
<?php } elseif ((isset($_GET['step'])&&$_GET['step']==2)) {?>

	
<script type="text/javascript">
// <![CDATA[
idSelectedCountry = <?php if (isset($_POST['id_state'])) {?><?php echo intval($_POST['id_state']);?>
<?php } else { ?>10<?php }?>;
countries = new Array();
countriesNeedIDNumber = new Array();
countriesNeedZipCode = new Array();
<?php if (isset($_smarty_tpl->tpl_vars['countries']->value)) {?>
	<?php  $_smarty_tpl->tpl_vars['country'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['country']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['country']->key => $_smarty_tpl->tpl_vars['country']->value) {
$_smarty_tpl->tpl_vars['country']->_loop = true;
?>
		<?php if (isset($_smarty_tpl->tpl_vars['country']->value['states'])&&$_smarty_tpl->tpl_vars['country']->value['contains_states']) {?>
			countries[<?php echo intval($_smarty_tpl->tpl_vars['country']->value['id_country']);?>
] = new Array();
			<?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['country']->value['states']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value) {
$_smarty_tpl->tpl_vars['state']->_loop = true;
?>
				countries[<?php echo intval($_smarty_tpl->tpl_vars['country']->value['id_country']);?>
].push({'id' : '<?php echo $_smarty_tpl->tpl_vars['state']->value['id_state'];?>
', 'name' : '<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
'});
			<?php } ?>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['country']->value['need_identification_number']) {?>
			countriesNeedIDNumber.push(<?php echo intval($_smarty_tpl->tpl_vars['country']->value['id_country']);?>
);
		<?php }?>
		<?php if (isset($_smarty_tpl->tpl_vars['country']->value['need_zip_code'])) {?>
			countriesNeedZipCode[<?php echo intval($_smarty_tpl->tpl_vars['country']->value['id_country']);?>
] = <?php echo $_smarty_tpl->tpl_vars['country']->value['need_zip_code'];?>
;
		<?php }?>
	<?php } ?>
<?php }?>
$(function(){
	$('.id_state option[value=<?php if (isset($_POST['id_state'])) {?><?php echo $_POST['id_state'];?>
<?php } else { ?><?php if (isset($_smarty_tpl->tpl_vars['address']->value)) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['address']->value->id_state, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['provincia']->value;?>
<?php }?><?php }?>]').attr('selected', 'selected');
});
//]]>
<?php if ($_smarty_tpl->tpl_vars['vat_management']->value) {?>
	
	$(document).ready(function() {
		$('#company').blur(function(){
			vat_number();
		});
		vat_number();
		function vat_number()
		{
			if ($('#company').val() != '')
				$('#vat_number').show();
			else
				$('#vat_number').hide();
		}
	});
	
<?php }?>
</script>
	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	<?php $_smarty_tpl->tpl_vars['stateExist'] = new Smarty_variable(false, null, 0);?>
	
		<?php if (!$_smarty_tpl->tpl_vars['inviato']->value) {?>
				<div style="width:60%; display:block; margin:0 auto">
					<form method="post" class="std" enctype="multipart/form-data">
					<fieldset>
					<div class="personal-information-2">
						<div class="select">
							<label for="id_contact"><strong><?php echo smartyTranslate(array('s'=>'Subject Heading','mod'=>'formprevendita'),$_smarty_tpl);?>
</strong></label>
						
							&nbsp;&nbsp;&nbsp;<?php echo smartyTranslate(array('s'=>'Customer service','mod'=>'formprevendita'),$_smarty_tpl);?>

							
						</div>
<div class="select">
							<label for="required"><strong>&nbsp;</strong></label>
					&nbsp;&nbsp;&nbsp;
I campi contrassegnati con l'asterisco (*) sono obbligatori
							
						</div>
					
						<input type="hidden" id="id_thread" name="id_thread" value="<?php echo $_smarty_tpl->tpl_vars['id_thread']->value;?>
" />
						<input type="hidden" id="tipo_richiesta" name="tipo_richiesta" value="preventivo" />
						<div class="text">
							<label for="firstname"><strong><?php echo smartyTranslate(array('s'=>'Firstname','mod'=>'formprevendita'),$_smarty_tpl);?>
*</strong></label>
								<input type="text" id="firstname" style="width:400px" name="firstname" value="<?php if (!empty($_POST['firstname'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['firstname'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['firstname']->value;?>
<?php }?>" />
							
						</div>
						<div class="text">
							<label for="lastname"><strong><?php echo smartyTranslate(array('s'=>'Lastname','mod'=>'formprevendita'),$_smarty_tpl);?>
*</strong></label>
								<input type="text" id="lastname" style="width:400px" name="lastname" value="<?php if (!empty($_POST['lastname'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['lastname'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['lastname']->value;?>
<?php }?>" />
							
						</div>
							<div class="text">
							<label for="company"><strong><?php echo smartyTranslate(array('s'=>'Company','mod'=>'formprevendita'),$_smarty_tpl);?>
*</strong></label>
								<input type="text" id="company" style="width:400px" name="company" value="<?php if (!empty($_POST['company'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['company'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['company']->value;?>
<?php }?>" />
							
						</div>
						
						<div class="text">
							<label for="vat_number"><strong><?php echo smartyTranslate(array('s'=>'VAT Number','mod'=>'formprevendita'),$_smarty_tpl);?>
</strong></label>
								<input type="text" id="vat_number" style="width:400px" name="vat_number" value="<?php if (!empty($_POST['vat_number'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['vat_number'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['vat_number']->value;?>
<?php }?>" />
							
						</div>
						<div class="text">
							<label for="tax_code"><strong><?php echo smartyTranslate(array('s'=>'Tax code','mod'=>'formprevendita'),$_smarty_tpl);?>
</strong></label>
								<input type="text" id="tax_code" style="width:400px" name="tax_code" value="<?php if (!empty($_POST['tax_code'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['tax_code'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['tax_code']->value;?>
<?php }?>" />
							
						</div>
						<div class="text">
							<label for="phone"><strong><?php echo smartyTranslate(array('s'=>'Phone','mod'=>'formprevendita'),$_smarty_tpl);?>
*</strong></label>
								<input type="text" id="phone" style="width:400px" name="phone" value="<?php if (!empty($_POST['phone'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['phone'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['phone']->value;?>
<?php }?>" />
							
						</div>
						
						<div class="text">
							<label for="email"><strong><?php echo smartyTranslate(array('s'=>'E-mail address','mod'=>'formprevendita'),$_smarty_tpl);?>
*</strong></label>
								<input type="text" id="email" style="width:400px" name="email" value="<?php if (!empty($_POST['email'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['email'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['email']->value;?>
<?php }?>" />
							
						</div>
							<div class="text" style="display:none">
							<label for="address1"><strong><?php echo smartyTranslate(array('s'=>'Address','mod'=>'formprevendita'),$_smarty_tpl);?>
*</strong></label>
								<input type="text" id="address1" style="width:400px" name="address1" value="<?php if (!empty($_POST['address1'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['address1'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['address1']->value;?>
<?php }?>" />
							
						</div>
							<div class="text" style="display:none">
							<label for="postcode"><strong><?php echo smartyTranslate(array('s'=>'Postcode','mod'=>'formprevendita'),$_smarty_tpl);?>
*</strong></label>
								<input type="text" id="postcode" style="width:400px" name="postcode" value="<?php if (!empty($_POST['postcode'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['postcode'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['postcode']->value;?>
<?php }?>" />
							
						</div>
							<div class="text" style="display:none">
							<label for="city"><strong>Citt&agrave;*</strong></label>
								<input type="text" id="city" style="width:400px" name="city" value="<?php if (!empty($_POST['city'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['city'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['city']->value;?>
<?php }?>" />
							
						</div>
						<div class="required id_state select" style="display:none">
									<label for="id_state"><strong><?php echo smartyTranslate(array('s'=>'State','mod'=>'formprevendita'),$_smarty_tpl);?>
*</strong></label>
									<select name="id_state" id="id_state">
										<option value="">-</option>
									</select>
									<sup>*</sup>
								</div>
								
						<div class="required select" style="display:none">
									<label for="id_country"><strong><?php echo smartyTranslate(array('s'=>'Country','mod'=>'formprevendita'),$_smarty_tpl);?>
*</strong></label>
									<select name="id_country" id="id_country">
										<option value="">-</option>
										<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
										<option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_country'];?>
" <?php if (!isset($_POST['id_country'])) {?><?php if (10==$_smarty_tpl->tpl_vars['v']->value['id_country']) {?><?php }?><?php }?><?php if (($_smarty_tpl->tpl_vars['paese']->value==$_smarty_tpl->tpl_vars['v']->value['id_country'])) {?> selected="selected"<?php } else { ?><?php if (($_POST['id_country']==$_smarty_tpl->tpl_vars['v']->value['id_country'])) {?>selected="selected"<?php } else { ?><?php }?><?php }?>><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['v']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
										<?php } ?>
									</select>
									<sup>*</sup>
								</div>
								<?php $_smarty_tpl->tpl_vars['stateExist'] = new Smarty_variable(true, null, 0);?>

								
							
						
						<div class="select">
									<?php if (isset($_GET['category'])) {?>
									<?php $_smarty_tpl->tpl_vars['categoryric'] = new Smarty_variable($_GET['category'], null, 0);?>
									<?php } elseif (isset($_POST['category'])&&is_numeric($_POST['category'])) {?>
									<?php $_smarty_tpl->tpl_vars['categoryric'] = new Smarty_variable($_POST['category'], null, 0);?>
									<?php } else { ?>
									<?php $_smarty_tpl->tpl_vars['categoryric'] = new Smarty_variable('', null, 0);?>
									<?php }?>
		
									<?php if ($_smarty_tpl->tpl_vars['categoryric']->value!='') {?> 
									<label for="category"><strong><?php echo smartyTranslate(array('s'=>'Requested product','mod'=>'formprevendita'),$_smarty_tpl);?>
*</strong></label>
									<input type="hidden" name="category" value="<?php echo $_smarty_tpl->tpl_vars['categoryric']->value;?>
" />
									&nbsp;&nbsp;&nbsp;
									<?php echo $_smarty_tpl->tpl_vars['prodottorichiesto']->value;?>

									<?php } else { ?>
									<label for="category"><strong><?php echo smartyTranslate(array('s'=>'Requested category','mod'=>'formprevendita'),$_smarty_tpl);?>
*</strong></label>
									<select name="category" id="category" autocomplete="off">
										<option value="cuffie" name="cuffie" <?php if ($_smarty_tpl->tpl_vars['category']->value=='cuffie') {?>selected="selected"<?php } else { ?><?php }?>>Cuffie</option>
										<option value="centralini" name="centralini" <?php if ($_smarty_tpl->tpl_vars['category']->value=='centralini') {?>selected="selected"<?php } else { ?><?php }?>>Centralini</option>
										<option value="fissi" name="fissi" <?php if ($_smarty_tpl->tpl_vars['category']->value=='fissi') {?>selected="selected"<?php } else { ?><?php }?>>Telefoni fissi</option>
										<option value="cordless" name="cordless" <?php if ($_smarty_tpl->tpl_vars['category']->value=='cordless') {?>selected="selected"<?php } else { ?><?php }?>>Telefoni cordless</option>
										<option value="audioconferenze" name="audioconferenze" <?php if (($_smarty_tpl->tpl_vars['category']->value=="audioconferenze")) {?>selected="selected"<?php } else { ?><?php }?>>Audioconferenze</option>
										<option value="audioguide" name="audioguide" <?php if ($_smarty_tpl->tpl_vars['category']->value=='audioguide') {?>selected="selected"<?php } else { ?><?php }?>>Audioguide</option>
										<option value="ricetrasmittenti" name="ricetrasmittenti" <?php if ($_smarty_tpl->tpl_vars['category']->value=='ricetrasmittenti') {?>selected="selected"<?php } else { ?><?php }?>>Ricetrasmittenti</option>
										<option value="registratori" name="registratori" <?php if ($_smarty_tpl->tpl_vars['category']->value=='registratori') {?>selected="selected"<?php } else { ?><?php }?>>Registratori</option>
										<option value="gsm" name="gsm" <?php if ($_smarty_tpl->tpl_vars['category']->value=='gsm') {?>selected="selected"<?php } else { ?><?php }?>>GSM-UMTS</option>
										<option value="gateway" name="gateway" <?php if ($_smarty_tpl->tpl_vars['category']->value=='gateway') {?>selected="selected"<?php } else { ?><?php }?>>VoIP Gateway</option>
										<option value="hotspot" name="hotspot" <?php if ($_smarty_tpl->tpl_vars['category']->value=='hotspot') {?>selected="selected"<?php } else { ?><?php }?>>Networking Hot-spot Wi-fi</option>
										<option value="ipcamera" name="ipcamera" <?php if ($_smarty_tpl->tpl_vars['category']->value=='ipcamera') {?>selected="selected"<?php } else { ?><?php }?>>IP Camera</option>
										<option value="centralino-virtuale" name="centralino-virtuale" <?php if ($_smarty_tpl->tpl_vars['category']->value=='centralino-virtuale') {?>selected="selected"<?php } else { ?><?php }?>>Centralino virtuale</option>
										<option value="altro" name="altro" <?php if ($_smarty_tpl->tpl_vars['category']->value=='altro') {?>selected="selected"<?php } else { ?><?php }?>>Altro</option>
									</select>
									<sup>*</sup>
									<?php }?>
								</div>
								
							<div class="text">
						<label for="fileUpload"><strong><?php echo smartyTranslate(array('s'=>'Attach File','mod'=>'formprevendita'),$_smarty_tpl);?>
</strong><br />
						<?php echo smartyTranslate(array('s'=>'Use this field if you want to send us files','mod'=>'formprevendita'),$_smarty_tpl);?>

						</label>
							<input type="hidden" name="MAX_FILE_SIZE" value="200000000" />
							<input style="width:400px" type="file" name="fileUpload[]" id="fileUpload1" /><br />
							<input style="width:400px; margin-top:2px" type="file" name="fileUpload[]" id="fileUpload2" />
						</div>
					
					<div class="textarea">
						<label for="message"><strong><?php echo smartyTranslate(array('s'=>'Indicaci le tue esigenze','mod'=>'formprevendita'),$_smarty_tpl);?>
*</strong></label>
						 <textarea id="message" name="message" rows="15" cols="20" style="width:400px;height:220px"><?php if (!empty($_POST['message'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['message'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php } else { ?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['message']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php }?></textarea>
					</div>
					</div>
					<style>
.g-recaptcha{
   margin: 0px auto !important;
   width: auto !important;
   height: auto !important;
   text-align: -webkit-center;
   text-align: -moz-center;
   text-align: -o-center;
   text-align: -ms-center;
   margin-left: -33px !important;
}

.grecaptcha-badge { 
    visibility: hidden;
}

.rc-anchor-normal .rc-anchor-pt {
visibility:hidden !important;
	margin: 2px 11px 0 0;
padding-right: 2px;
position: relative !important;
text-align: right !important;
width: 86px !important;
	}
</style>

					<script src="https://www.google.com/recaptcha/api.js" async defer></script>
					
<div class="text-xs-center" >
					 
					 
					<div style="clear:both"></div>								
					<div style="display:block; width:90%">La quotazione personalizzata sar&agrave; disponibile entro un'ora circa dalla presa in carico della tua richiesta.
Per visualizzare, stampare e modificare il preventivo, dovrai accedere al tuo account.
Per ricevere supporto puoi contattare il nostro servizio clienti al <br /><span style="color:green"><strong>numero verde 800 529767</strong></span>
					</div>
					<div style="text-align:center; display:block; width:90%">
						Ho letto e compreso la Vostra <a href='https://www.ezdirect.it/guide/6-informativa-sulla-privacy' target='_blank'>Privacy Policy</a> e:
						<br /><br />
							
							<input type="radio" name="consenso_1" id="consenso11" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
							<input type="radio" name="consenso_1" id="consenso10" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
							
							<br /><br />
							al trattamento dei dati personali per l’esecuzione dei servizi forniti tramite il Sito da parte di Ezdirect S.r.l., o ad una o più obbligazioni contrattualmente convenute, ai sensi dell’art. 1, comma 1, lettera a), b) e c) della citata informativa (consenso obbligatorio; la mancata prestazione comporterà l’impossibilità di fruire dei servizi offerti, di effettuare l’iscrizione, e di effettuare acquisti tramite il Sito).
							<br /><br />
							
							<input type="radio" name="consenso_2"  id="consenso21" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
							<input type="radio" name="consenso_2"  id="consenso20" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
							
							<br /><br />
							al trattamento dei dati personali per la realizzazione, da parte di Ezdirect S.r.l., di indagini dirette a verificare il grado di soddisfazione sui servizi offerti, ai sensi dell’art. 1, comma 2, lettera a) della citata informativa (consenso facoltativo).

							<br /><br />
							
							<input type="radio" name="consenso_3"  id="consenso31" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
							<input type="radio" name="consenso_3"  id="consenso30" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
							
							<br /><br />
							al trattamento dei dati personali a fini di marketing e/o comunicazione commerciale da parte di Ezdirect S.r.l., connesse alle attività svolte da parte della stessa e/o da parte di soggetti terzi, ai sensi dell’art. 1, comma 2, lettera b), della citata informativa (consenso facoltativo).
							<br /><br />
							

						</div>
				
					</div>
					 <div class="g-recaptcha" style="border:0px" data-sitekey="6LciANsZAAAAALhFLBBxrvJJICdKqsPVxXHfPh2i"></div>

					<div style='text-align:center; width:93%'>
						<br />
						<input type="submit" name="submitMessage" id="submitMessage" value="<?php echo smartyTranslate(array('s'=>'Send','mod'=>'formprevendita'),$_smarty_tpl);?>
" class="button_large" onclick="$(this).hide(); fbq('track', 'Contact');" />
					</div>
					</fieldset>
				</div>
				</form>	
			</div>
	<?php } else { ?>

		<div><?php echo smartyTranslate(array('s'=>'Your message has been successfully sent to our team.','mod'=>'formprevendita'),$_smarty_tpl);?>
</div>
		<ul class="footer_links">
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
"><img class="icon" alt="" width="22" height="22" src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/home.gif"/></a><a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
"><?php echo smartyTranslate(array('s'=>'Home','mod'=>'formprevendita'),$_smarty_tpl);?>
</a></li>
		</ul>
				 <!-- Google Code for Richiesta di preventivo Conversion Page -->
				 <?php if ($_POST['id_thread']=='') {?>
					<script type="text/javascript">
					/* <![CDATA[ */
					var google_conversion_id = 1058514372;
					var google_conversion_language = "en";
					var google_conversion_format = "3";
					var google_conversion_color = "ffffff";
					var google_conversion_label = "8C5eCMypmAQQxMve-AM";
					var google_conversion_value = 0;
					/* ]]> */
					</script>
					<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
					</script>
					<noscript>
					<div style="display:inline;">
					<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1058514372/?value=0&amp;label=8C5eCMypmAQQxMve-AM&amp;guid=ON&amp;script=0"/>
					</div>
					</noscript>
					
					<script type="text/javascript">
					var _gaq = _gaq || [];
					_gaq.push(['_setAccount', 'UA-19658774-1']);
					// Recommanded value by Google doc and has to before the trackPageView
					_gaq.push(['_setSiteSpeedSampleRate', 5]);

					  _gaq.push(['_addTrans',
						'<?php echo $_smarty_tpl->tpl_vars['idrichiesta']->value;?>
',		
						'Ezdirect',		
						'0.01',		
						'',			
						'',	
						'<?php echo $_POST['city'];?>
',	
					    '',		
						''		
					  ]);

						
							_gaq.push(['_addItem',
							'<?php echo $_smarty_tpl->tpl_vars['idrichiesta']->value;?>
',	
							'<?php echo $_smarty_tpl->tpl_vars['categoria']->value;?>
',			
							'<?php echo $_smarty_tpl->tpl_vars['categoria']->value;?>
',	
							'',		
							'0.01',	
							'1'		
							]);
					
					
					  _gaq.push(['_trackTrans']);	
					
				
					
					(function() {
						var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
						ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
						var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
					})(); 
					</script>

				<?php } else { ?>
				<?php }?>
		
		
		<?php }?>



<?php }?>	
	
	
	
	<?php }} ?>
