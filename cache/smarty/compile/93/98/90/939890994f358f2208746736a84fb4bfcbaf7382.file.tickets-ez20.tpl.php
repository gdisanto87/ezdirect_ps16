<?php /* Smarty version Smarty-3.1.19, created on 2021-12-06 15:36:43
         compiled from "/var/www/html/modules/mytickets/tickets-ez20.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7824124161ae1ffbc4be32-63705679%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '939890994f358f2208746736a84fb4bfcbaf7382' => 
    array (
      0 => '/var/www/html/modules/mytickets/tickets-ez20.tpl',
      1 => 1637920945,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7824124161ae1ffbc4be32-63705679',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'navigationPipe' => 0,
    'id_ticket' => 0,
    'ticket_data' => 0,
    'in_charge_to' => 0,
    'id_fattura' => 0,
    'data_fattura' => 0,
    'messages' => 0,
    'tickets' => 0,
    'ticket' => 0,
    'sigla' => 0,
    'anno' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61ae1ffbc84d33_51787255',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61ae1ffbc84d33_51787255')) {function content_61ae1ffbc84d33_51787255($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/html/tools/smarty/plugins/modifier.date_format.php';
?>

<div id="mytickets">
	<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account.php',true);?>
"><?php echo smartyTranslate(array('s'=>'My account','mod'=>'mytickets'),$_smarty_tpl);?>
</a><span class="navigation-pipe"><?php echo $_smarty_tpl->tpl_vars['navigationPipe']->value;?>
</span><?php echo smartyTranslate(array('s'=>'My tickets','mod'=>'mytickets'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<br />
	<h1 style='text-align:left'><?php echo smartyTranslate(array('s'=>'My tickets','mod'=>'mytickets'),$_smarty_tpl);?>
</h2>
	<br />
	
	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	
	<?php if (isset($_GET['id_customer_thread'])) {?>
	
		<table class="std2">
				<thead>
					<tr>
						<th class="first_item" style="background-color:#8899cc; text-align:left"><?php echo smartyTranslate(array('s'=>'Ticket ID','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
						<th style="background-color:#99aadd; text-align:left"><?php echo smartyTranslate(array('s'=>'Type','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
						<th style="background-color:#8899cc; text-align:left"><?php echo smartyTranslate(array('s'=>'Status','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
						<th style="background-color:#99aadd; text-align:left"><?php echo smartyTranslate(array('s'=>'In charge to','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
						<th style="background-color:#8899cc; text-align:left"><?php echo smartyTranslate(array('s'=>'Open date','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
						<th style="background-color:#99aadd; text-align:left"><?php echo smartyTranslate(array('s'=>'Last message date','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
					</tr>
				</thead>
				<tbody>
				<tr>
					<td><?php echo $_smarty_tpl->tpl_vars['id_ticket']->value;?>
</td>
					<td><?php echo $_smarty_tpl->tpl_vars['ticket_data']->value['name'];?>
</td>
					<td><?php if ($_smarty_tpl->tpl_vars['ticket_data']->value['id_contact']==7) {?>--<?php } else { ?><?php if ($_smarty_tpl->tpl_vars['ticket_data']->value['status']=='open') {?> <?php echo smartyTranslate(array('s'=>'Open','mod'=>'mytickets'),$_smarty_tpl);?>
 <?php } elseif ($_smarty_tpl->tpl_vars['ticket_data']->value['status']=='closed') {?> <?php echo smartyTranslate(array('s'=>'Closed','mod'=>'mytickets'),$_smarty_tpl);?>
 <?php } elseif ($_smarty_tpl->tpl_vars['ticket_data']->value['status']=='pending1') {?> <?php echo smartyTranslate(array('s'=>'Pending','mod'=>'mytickets'),$_smarty_tpl);?>
 <?php } else { ?> <?php }?><?php }?></td>
					<td><?php echo $_smarty_tpl->tpl_vars['in_charge_to']->value;?>
</td>
					<td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['ticket_data']->value['date_add'],"%d/%m/%Y, %H:%M:%S");?>
</td>
					<td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['ticket_data']->value['last_msg'],"%d/%m/%Y, %H:%M:%S");?>
</td>
				</tr>
				</tbody>
		</table>
		<br />
		<table class="std2">
			<thead>
				<tr>
		<?php if ($_smarty_tpl->tpl_vars['ticket_data']->value['id_contact']==9) {?>
		<th class="first_item" style="background-color:#8899cc; text-align:left"><?php echo smartyTranslate(array('s'=>'Invoice ID','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
		<th class="first_item" style="background-color:#99aadd;text-align:left"><?php echo smartyTranslate(array('s'=>'Product','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
		</tr></thead>
		<tbody>
		<tr>
		<td><?php if ($_smarty_tpl->tpl_vars['ticket_data']->value['id_order']!=0) {?> <?php echo $_smarty_tpl->tpl_vars['id_fattura']->value;?>
 del <?php echo $_smarty_tpl->tpl_vars['data_fattura']->value;?>
 <?php } else { ?> - <?php }?></td>
		<td><?php if ($_smarty_tpl->tpl_vars['ticket_data']->value['product']!='') {?> <?php echo $_smarty_tpl->tpl_vars['ticket_data']->value['product'];?>
 <?php } else { ?> - <?php }?>
		</td>
		</tr></tbody>
		</table>
		<?php } else { ?>
		<th class="first_item" style="background-color:#8899cc;text-align:left"><?php echo smartyTranslate(array('s'=>'Invoice ID','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
		<th class="first_item" style="background-color:#99aadd;text-align:left"><?php echo smartyTranslate(array('s'=>'Product','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
		</tr></thead>
		<tbody>
		<tr>
		<td><?php if ($_smarty_tpl->tpl_vars['ticket_data']->value['id_order']!=0) {?> <?php echo $_smarty_tpl->tpl_vars['ticket_data']->value['id_order'];?>
 <?php } else { ?> - <?php }?></td>
		</tr>
		<tr><td><?php if ($_smarty_tpl->tpl_vars['ticket_data']->value['product']!='') {?> <?php echo $_smarty_tpl->tpl_vars['ticket_data']->value['product'];?>
 <?php } else { ?> - <?php }?>
		</td>
		</tr></tbody>
		</table>
		<?php }?>
	
		
		<?php if ($_smarty_tpl->tpl_vars['ticket_data']->value['id_contact']==9) {?>
		<br />
		<table class="std2">
			<thead>
				<tr>
		<th style="background-color:#8899cc";><?php echo smartyTranslate(array('s'=>'RMA Type','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
		<th  style="background-color:#99aadd;"><?php echo smartyTranslate(array('s'=>'RMA Shipping','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
		<th style="background-color:#8899cc;"><?php echo smartyTranslate(array('s'=>'Address','mod'=>'mytickets'),$_smarty_tpl);?>
</th></tr>
		</thead><tbody>
		<tr><td> <?php if ($_smarty_tpl->tpl_vars['ticket_data']->value['rma_type']!='') {?> <?php echo $_smarty_tpl->tpl_vars['ticket_data']->value['rma_type'];?>
 <?php } else { ?> - <?php }?></td><td><?php if ($_smarty_tpl->tpl_vars['ticket_data']->value['rma_shipping']!='') {?> <?php echo $_smarty_tpl->tpl_vars['ticket_data']->value['rma_shipping'];?>
 <?php } else { ?> - <?php }?></td><td><?php if ($_smarty_tpl->tpl_vars['ticket_data']->value['rma_address']!='') {?> <?php echo $_smarty_tpl->tpl_vars['ticket_data']->value['rma_address'];?>
 <?php } else { ?> - <?php }?></td></tr>
		</tbody></table>
		
		<?php }?>
					<br /><br />
		
		<h2><?php echo smartyTranslate(array('s'=>'Communications','mod'=>'mytickets'),$_smarty_tpl);?>
</h2>
		<?php echo $_smarty_tpl->tpl_vars['messages']->value;?>

	
		<p style='text-align:center'> <?php if ($_smarty_tpl->tpl_vars['ticket_data']->value['status']!='closed') {?><a href="
		<?php if ($_smarty_tpl->tpl_vars['ticket_data']->value['id_contact']=='preventivo') {?>
		<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/formprevendita/form.php',true);?>
?id_thread=<?php echo $_smarty_tpl->tpl_vars['ticket_data']->value['id_customer_thread'];?>
&amp;token=<?php echo $_smarty_tpl->tpl_vars['ticket_data']->value['token'];?>

		<?php } elseif ($_smarty_tpl->tpl_vars['ticket_data']->value['id_contact']=='tirichiamiamonoi') {?>
		<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/formprevendita/tirichiamiamonoi.php',true);?>
?id_thread=<?php echo $_smarty_tpl->tpl_vars['ticket_data']->value['id_customer_thread'];?>
&amp;token=<?php echo $_smarty_tpl->tpl_vars['ticket_data']->value['token'];?>

		<?php } else { ?>
		<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('contact-form.php',true);?>
?id_customer_thread=<?php echo $_smarty_tpl->tpl_vars['ticket_data']->value['id_customer_thread'];?>
&amp;token=<?php echo $_smarty_tpl->tpl_vars['ticket_data']->value['token'];?>

		<?php }?>
		"><?php echo smartyTranslate(array('s'=>'Reply to this ticket','mod'=>'mytickets'),$_smarty_tpl);?>
</a> | <?php }?> <a href="/contattaci?rif=<?php if ($_smarty_tpl->tpl_vars['ticket_data']->value['id_contact']=='preventivo') {?>P<?php echo $_smarty_tpl->tpl_vars['ticket_data']->value['id_customer_thread'];?>
<?php } elseif ($_smarty_tpl->tpl_vars['ticket_data']->value['id_contact']=='tirichiamiamonoi') {?>R<?php echo $_smarty_tpl->tpl_vars['ticket_data']->value['id_customer_thread'];?>
<?php } else { ?>T<?php echo $_smarty_tpl->tpl_vars['ticket_data']->value['id_customer_thread'];?>
<?php }?>"><?php echo smartyTranslate(array('s'=>'Create new ticket','mod'=>'mytickets'),$_smarty_tpl);?>
</a> | <a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/mytickets/tickets.php',true);?>
"><?php echo smartyTranslate(array('s'=>'Back to your ticket list','mod'=>'mytickets'),$_smarty_tpl);?>
</a></p>
	
	
	<?php } else { ?>
	
		<?php if ($_smarty_tpl->tpl_vars['tickets']->value) {?>
			<table class="std2">
				<thead>
					<tr>
						<th class="first_item" style="background-color:#8899cc; text-align:left"><?php echo smartyTranslate(array('s'=>'Ticket ID','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
						<th style="background-color:#99aadd; text-align:left"><?php echo smartyTranslate(array('s'=>'Type','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
						<th style="background-color:#8899cc; text-align:left"><?php echo smartyTranslate(array('s'=>'Status','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
						<th style="background-color:#99aadd; text-align:left"><?php echo smartyTranslate(array('s'=>'In charge to','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
						<th style="background-color:#8899cc; text-align:left"><?php echo smartyTranslate(array('s'=>'Open date','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
						<th style="background-color:#99aadd; text-align:left"><?php echo smartyTranslate(array('s'=>'Last message date','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
						<th style="background-color:#8899cc; text-align:left"><?php echo smartyTranslate(array('s'=>'Dettagli','mod'=>'mytickets'),$_smarty_tpl);?>
</th>
					</tr>
				</thead>
				<tbody>
				<?php  $_smarty_tpl->tpl_vars['ticket'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ticket']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tickets']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ticket']->key => $_smarty_tpl->tpl_vars['ticket']->value) {
$_smarty_tpl->tpl_vars['ticket']->_loop = true;
?>
				
				
				
				
				
				<tr>
					<td><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/mytickets/tickets.php',true);?>
?id_customer_thread=<?php echo $_smarty_tpl->tpl_vars['ticket']->value['id_customer_thread'];?>
<?php if ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']=='preventivo'||$_smarty_tpl->tpl_vars['ticket']->value['id_contact']=='tirichiamiamonoi') {?>&type=pv<?php }?>">
				
			
					<?php $_smarty_tpl->tpl_vars['anno'] = new Smarty_variable(substr($_smarty_tpl->tpl_vars['ticket']->value['date_add'],2,2), null, 0);?>
				
					<?php if ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']==2) {?>
					<?php $_smarty_tpl->tpl_vars['sigla'] = new Smarty_variable('A', null, 0);?>
					<?php } elseif ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']==4) {?>
					<?php $_smarty_tpl->tpl_vars['sigla'] = new Smarty_variable('T', null, 0);?>
					<?php } elseif ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']==3) {?>
					<?php $_smarty_tpl->tpl_vars['sigla'] = new Smarty_variable('V', null, 0);?>
					<?php } elseif ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']==7) {?>
					<?php $_smarty_tpl->tpl_vars['sigla'] = new Smarty_variable('M', null, 0);?>
					<?php } elseif ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']==6) {?>
					<?php $_smarty_tpl->tpl_vars['sigla'] = new Smarty_variable('R', null, 0);?>
					<?php } elseif ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']==8) {?>
					<?php $_smarty_tpl->tpl_vars['sigla'] = new Smarty_variable('D', null, 0);?>
					<?php } elseif ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']==9) {?>
					<?php $_smarty_tpl->tpl_vars['sigla'] = new Smarty_variable('S', null, 0);?>
					<?php } elseif ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']=='preventivo') {?>
					<?php $_smarty_tpl->tpl_vars['sigla'] = new Smarty_variable('P', null, 0);?>
					<?php } elseif ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']=='tirichiamiamonoi') {?>
					<?php $_smarty_tpl->tpl_vars['sigla'] = new Smarty_variable('R', null, 0);?>
					<?php }?>
					
					<?php $_smarty_tpl->tpl_vars['id_ticket'] = new Smarty_variable((($_smarty_tpl->tpl_vars['sigla']->value).($_smarty_tpl->tpl_vars['anno']->value)).($_smarty_tpl->tpl_vars['ticket']->value['id_customer_thread']), null, 0);?>
				
					<?php echo $_smarty_tpl->tpl_vars['id_ticket']->value;?>

					</a>
				</td>
					
					<td><?php echo $_smarty_tpl->tpl_vars['ticket']->value['name'];?>
</td>
					<td><?php if ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']==7) {?><?php } else { ?><?php if ($_smarty_tpl->tpl_vars['ticket']->value['status']=='open') {?> <?php echo smartyTranslate(array('s'=>'Open','mod'=>'mytickets'),$_smarty_tpl);?>
 <?php } elseif ($_smarty_tpl->tpl_vars['ticket']->value['status']=='closed') {?> <?php echo smartyTranslate(array('s'=>'Closed','mod'=>'mytickets'),$_smarty_tpl);?>
 <?php } elseif ($_smarty_tpl->tpl_vars['ticket']->value['status']=='pending1') {?> <?php echo smartyTranslate(array('s'=>'Pending','mod'=>'mytickets'),$_smarty_tpl);?>
 <?php } else { ?> <?php }?><?php }?></td>
					<td><?php if ($_smarty_tpl->tpl_vars['ticket']->value['id_employee']==0) {?> - <?php } else { ?><?php echo $_smarty_tpl->tpl_vars['ticket']->value['firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['ticket']->value['lastname'];?>
<?php }?></td>
					<td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['ticket']->value['date_add'],"%d/%m/%Y, %H:%M:%S");?>
</td>
					<td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['ticket']->value['last_msg'],"%d/%m/%Y, %H:%M:%S");?>
</td>
					<td><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/mytickets/tickets.php',true);?>
?id_customer_thread=<?php echo $_smarty_tpl->tpl_vars['ticket']->value['id_customer_thread'];?>
<?php if ($_smarty_tpl->tpl_vars['ticket']->value['id_contact']=='preventivo'||$_smarty_tpl->tpl_vars['ticket']->value['id_contact']=='tirichiamiamonoi') {?>&type=pv<?php }?>"><img src='/themes/ez20/img/icona-vai.jpg'></a></td>
					
				</tr>
				</tbody>
			<?php } ?>
			</table>
		
		<?php } else { ?>
		
			<p class="warning"><?php echo smartyTranslate(array('s'=>'You have not opened any ticket','mod'=>'mytickets'),$_smarty_tpl);?>
.</p>
		
		<?php }?>
		<br />
		<div  id="my-tickets-responsive">
			<a href="/contattaci?step=assistenza-ordini" class="button_large" style="display:block; float:left;   width: auto; padding: 10px; "><?php echo smartyTranslate(array('s'=>'Create new ticket orders','mod'=>'mytickets'),$_smarty_tpl);?>
</a> 
			<a href="/contattaci?step=contabilita" class="button_large"  style="display:block; float:left;  width: auto; padding: 10px;"><?php echo smartyTranslate(array('s'=>'Create new ticket administrative assistance','mod'=>'mytickets'),$_smarty_tpl);?>
</a> 
			<a href="/cms.php?id_cms=36" class="button_large" style="display:block; float:left; width: auto; padding: 10px; "><?php echo smartyTranslate(array('s'=>'Create new ticket RMA','mod'=>'mytickets'),$_smarty_tpl);?>
</a> 
			<a href="/contattaci?step=tecnica" class="button_large" style="display:block; float:left;  width: auto; padding: 10px; "><?php echo smartyTranslate(array('s'=>'Create new ticket technical assistance','mod'=>'mytickets'),$_smarty_tpl);?>
</a> 
		</div>
		<br />
		
	<?php }?>

	
</div>
<?php }} ?>
