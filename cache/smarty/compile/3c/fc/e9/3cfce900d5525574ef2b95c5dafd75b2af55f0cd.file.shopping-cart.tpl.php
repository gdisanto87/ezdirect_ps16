<?php /* Smarty version Smarty-3.1.19, created on 2021-12-13 16:52:22
         compiled from "/var/www/html/modules/loyalty/shopping-cart.tpl" */ ?>
<?php /*%%SmartyHeaderCode:131482661761b76c36d5be95-90438045%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3cfce900d5525574ef2b95c5dafd75b2af55f0cd' => 
    array (
      0 => '/var/www/html/modules/loyalty/shopping-cart.tpl',
      1 => 1638264471,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '131482661761b76c36d5be95-90438045',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'points' => 0,
    'module_template_dir' => 0,
    'voucher' => 0,
    'guest_checkout' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61b76c36d631b7_75554673',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61b76c36d631b7_75554673')) {function content_61b76c36d631b7_75554673($_smarty_tpl) {?>

<!-- MODULE Loyalty -->
<?php if ($_smarty_tpl->tpl_vars['points']->value>0) {?>
<div class="shopping_cart_table" id="shopping_cart_table_loyalty" style="width:99%">

<br />

<p id="loyalty">
	<img src="<?php echo $_smarty_tpl->tpl_vars['module_template_dir']->value;?>
loyalty.gif" alt="<?php echo smartyTranslate(array('s'=>'loyalty','mod'=>'loyalty'),$_smarty_tpl);?>
" class="icon" width="16" height="16" />
	
		<?php echo smartyTranslate(array('s'=>'By checking out of this shopping cart you can collect up to','mod'=>'loyalty'),$_smarty_tpl);?>
 <b><?php echo $_smarty_tpl->tpl_vars['points']->value;?>
 
		<?php if ($_smarty_tpl->tpl_vars['points']->value>1) {?><?php echo smartyTranslate(array('s'=>'loyalty points','mod'=>'loyalty'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'loyalty point','mod'=>'loyalty'),$_smarty_tpl);?>
<?php }?></b> 
		<?php echo smartyTranslate(array('s'=>'that can be converted into a voucher of','mod'=>'loyalty'),$_smarty_tpl);?>
 <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['voucher']->value),$_smarty_tpl);?>
<?php if (isset($_smarty_tpl->tpl_vars['guest_checkout']->value)&&$_smarty_tpl->tpl_vars['guest_checkout']->value) {?><sup>*</sup><?php }?>. <?php echo smartyTranslate(array('s'=>'When your products will be shipped your points will be available and you will be able to use them on your next order. In order to see your points, clic on "My account" and then on "My loyalty points".','mod'=>'loyalty'),$_smarty_tpl);?>
<br />
		<?php if (isset($_smarty_tpl->tpl_vars['guest_checkout']->value)&&$_smarty_tpl->tpl_vars['guest_checkout']->value) {?><sup>*</sup> <?php echo smartyTranslate(array('s'=>'Not available for Instant checkout order','mod'=>'loyalty'),$_smarty_tpl);?>
<?php }?>
	
	
<br />
</p>

</div>
<!-- END : MODULE Loyalty -->
<?php }?><?php }} ?>
