<?php /* Smarty version Smarty-3.1.19, created on 2021-11-17 06:08:43
         compiled from "/var/www/html/themes/ez20/search.tpl" */ ?>
<?php /*%%SmartyHeaderCode:156858631061948e5bcf3ea1-07190341%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '92aebf6e89135c5dd26781ad4bc49161e67805b0' => 
    array (
      0 => '/var/www/html/themes/ez20/search.tpl',
      1 => 1635933351,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '156858631061948e5bcf3ea1-07190341',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'instantSearch' => 0,
    'nbProducts' => 0,
    'search_query' => 0,
    'search_tag' => 0,
    'ref' => 0,
    'search_products' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61948e5bd238c5_38967940',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61948e5bd238c5_38967940')) {function content_61948e5bd238c5_38967940($_smarty_tpl) {?>

<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'Search'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<!-- <?php if (basename($_SERVER['PHP_SELF'])!="search2.php") {?><h1 <?php if (isset($_smarty_tpl->tpl_vars['instantSearch']->value)&&$_smarty_tpl->tpl_vars['instantSearch']->value) {?>id="instant_search_results"<?php }?>>
<?php echo smartyTranslate(array('s'=>'Search'),$_smarty_tpl);?>
&nbsp;<?php if ($_smarty_tpl->tpl_vars['nbProducts']->value>0) {?>"<?php if (isset($_smarty_tpl->tpl_vars['search_query']->value)&&$_smarty_tpl->tpl_vars['search_query']->value) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_query']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php } elseif ($_smarty_tpl->tpl_vars['search_tag']->value) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_tag']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php } elseif ($_smarty_tpl->tpl_vars['ref']->value) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['ref']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"<?php }?>
<?php if (isset($_smarty_tpl->tpl_vars['instantSearch']->value)&&$_smarty_tpl->tpl_vars['instantSearch']->value) {?><a href="#" class="close"><?php echo smartyTranslate(array('s'=>'Return to previous page'),$_smarty_tpl);?>
</a><?php }?>
</h1><?php }?> -->
<h1><?php echo smartyTranslate(array('s'=>'Advanced search filters'),$_smarty_tpl);?>
</h1>

<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php if (mb_strtolower($_smarty_tpl->tpl_vars['search_query']->value, 'UTF-8')=='centralino virtuale'||mb_strtolower($_smarty_tpl->tpl_vars['search_query']->value, 'UTF-8')=='centralino'||mb_strtolower($_smarty_tpl->tpl_vars['search_query']->value, 'UTF-8')=='virtuale'||mb_strtolower($_smarty_tpl->tpl_vars['search_query']->value, 'UTF-8')=='centralini'||mb_strtolower($_smarty_tpl->tpl_vars['search_query']->value, 'UTF-8')=='virtuali'||mb_strtolower($_smarty_tpl->tpl_vars['search_query']->value, 'UTF-8')=='centralini virtuali'||mb_strtolower($_smarty_tpl->tpl_vars['search_query']->value, 'UTF-8')=='ezcloud'||mb_strtolower($_smarty_tpl->tpl_vars['search_query']->value, 'UTF-8')=='centralino ezcloud'||mb_strtolower($_smarty_tpl->tpl_vars['search_query']->value, 'UTF-8')=='centralino virtuale ezcloud'||mb_strtolower($_smarty_tpl->tpl_vars['search_query']->value, 'UTF-8')=='ezcloud centralino'||mb_strtolower($_smarty_tpl->tpl_vars['search_query']->value, 'UTF-8')=='ezcloud centralini'||mb_strtolower($_smarty_tpl->tpl_vars['search_query']->value, 'UTF-8')=='ezcloud centralini virtuali'||mb_strtolower($_smarty_tpl->tpl_vars['search_query']->value, 'UTF-8')=='cnetralini'||mb_strtolower($_smarty_tpl->tpl_vars['search_query']->value, 'UTF-8')=='cetralini'||mb_strtolower($_smarty_tpl->tpl_vars['search_query']->value, 'UTF-8')=='cnetralino'||mb_strtolower($_smarty_tpl->tpl_vars['search_query']->value, 'UTF-8')=='cetralino') {?>
	<p style='text-align:center'>
	<a href='https://www.ezdirect.it/centralino-virtuale/'><img src='https://www.ezdirect.it/img/cms/banner%20categoria%20ezcloud.jpg' alt='Centralino virtuale Ezcloud' title='Centralino virtuale Ezcloud' /></a>
	</p>
<?php }?>
	
<?php if (!$_smarty_tpl->tpl_vars['nbProducts']->value) {?>
	<p class="warning">
		<?php if (isset($_smarty_tpl->tpl_vars['search_query']->value)&&$_smarty_tpl->tpl_vars['search_query']->value) {?>
			<?php echo smartyTranslate(array('s'=>'No results found for your search'),$_smarty_tpl);?>
&nbsp;"<?php if (isset($_smarty_tpl->tpl_vars['search_query']->value)) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_query']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"
		<?php } elseif (isset($_smarty_tpl->tpl_vars['search_tag']->value)&&$_smarty_tpl->tpl_vars['search_tag']->value) {?>
			<?php echo smartyTranslate(array('s'=>'No results found for your search'),$_smarty_tpl);?>
&nbsp;"<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_tag']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"
		<?php } else { ?>
			<?php echo smartyTranslate(array('s'=>'Please type a search keyword'),$_smarty_tpl);?>

		<?php }?>
	</p>
	
	
	<form action='<?php echo $_SERVER['REQUEST_URI'];?>
' name='estensione' method='get'>

	<input type='hidden' name='orderby' value='<?php echo $_GET['orderby'];?>
' />
	<input type='hidden' name='orderway' value='<?php echo $_GET['orderway'];?>
' />
	<input type='hidden' name='search_query' value='<?php echo $_GET['search_query'];?>
' />
	<input type='checkbox' value='on' name='nnn' <?php if ($_GET['nnn']=='on') {?>checked='checked'<?php }?> /><?php echo smartyTranslate(array('s'=>'Product name'),$_smarty_tpl);?>

	<input type='checkbox' value='on' name='cod' <?php if ($_GET['cod']=='on') {?>checked='checked'<?php }?> /><?php echo smartyTranslate(array('s'=>'Reference'),$_smarty_tpl);?>

	<input type='checkbox' value='on' name='sd' <?php if ($_GET['sd']=='on') {?>checked='checked'<?php }?> /><?php echo smartyTranslate(array('s'=>'Short description'),$_smarty_tpl);?>

	<input type='checkbox' value='on' name='ld' <?php if ($_GET['ld']=='on') {?>checked='checked'<?php }?> /><?php echo smartyTranslate(array('s'=>'Long description'),$_smarty_tpl);?>

	<input type='checkbox' value='on' name='td' <?php if ($_GET['td']=='on') {?>checked='checked'<?php }?> /><?php echo smartyTranslate(array('s'=>'Technical details'),$_smarty_tpl);?>

	<?php if (preg_match("/ or /",$_GET['search_query'])) {?>
	<?php } else { ?>
	<input type='checkbox' value='on' name='xctm' <?php if ($_GET['xctm']=='on') {?>checked='checked'<?php }?> /><?php echo smartyTranslate(array('s'=>'Search for the exact match'),$_smarty_tpl);?>

	<?php }?>

	<input type='submit' name='submit_search' class='button_large' value='<?php echo smartyTranslate(array('s'=>'Extend your search'),$_smarty_tpl);?>
' />
	</form>
	<br /><br />
	
<?php } else { ?>
	<!-- <h3><span class="big"><?php echo intval($_smarty_tpl->tpl_vars['nbProducts']->value);?>
</span>&nbsp;<?php if ($_smarty_tpl->tpl_vars['nbProducts']->value==1) {?><?php echo smartyTranslate(array('s'=>'result has been found.'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'results have been found.'),$_smarty_tpl);?>
<?php }?></h3> -->
	
	
	<form action='<?php echo $_SERVER['REQUEST_URI'];?>
' name='estensione' method='get'>

	<input type='hidden' name='orderby' value='<?php echo $_GET['orderby'];?>
' />
	<input type='hidden' name='orderway' value='<?php echo $_GET['orderway'];?>
' />
	<input type='hidden' name='search_query' value='<?php echo $_GET['search_query'];?>
' />
	<input type='checkbox' value='on' name='nnn' <?php if ($_GET['nnn']=='on') {?>checked='checked'<?php }?> /><?php echo smartyTranslate(array('s'=>'Product name'),$_smarty_tpl);?>

	<input type='checkbox' value='on' name='cod' <?php if ($_GET['cod']=='on') {?>checked='checked'<?php }?> /><?php echo smartyTranslate(array('s'=>'Reference'),$_smarty_tpl);?>

	<input type='checkbox' value='on' name='sd' <?php if ($_GET['sd']=='on') {?>checked='checked'<?php }?> /><?php echo smartyTranslate(array('s'=>'Short description'),$_smarty_tpl);?>

	<input type='checkbox' value='on' name='ld' <?php if ($_GET['ld']=='on') {?>checked='checked'<?php }?> /><?php echo smartyTranslate(array('s'=>'Long description'),$_smarty_tpl);?>

	<input type='checkbox' value='on' name='td' <?php if ($_GET['td']=='on') {?>checked='checked'<?php }?> /><?php echo smartyTranslate(array('s'=>'Technical details'),$_smarty_tpl);?>

	<?php if (preg_match("/ or /",$_GET['search_query'])) {?>
	<?php } else { ?>
	<input type='checkbox' value='on' name='xctm' <?php if ($_GET['xctm']=='on') {?>checked='checked'<?php }?> /><?php echo smartyTranslate(array('s'=>'Search for the exact match'),$_smarty_tpl);?>

	<?php }?>

	<input type='submit' name='submit_search' class='button_large' style='width:110px' value='<?php echo smartyTranslate(array('s'=>'Extend your search'),$_smarty_tpl);?>
' />
	</form>
	<br /><br />
	
	
	<?php if (!isset($_smarty_tpl->tpl_vars['instantSearch']->value)||(isset($_smarty_tpl->tpl_vars['instantSearch']->value)&&!$_smarty_tpl->tpl_vars['instantSearch']->value)) {?>
		<div class='block_listing_header'>
<div class='block-compare'>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./product-compare.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

&nbsp;&nbsp;&nbsp;<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./category-count.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div>
<div class='block-sort'>
				<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./product-sort.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div>
</div>	
	<?php }?>
	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./product-list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('products'=>$_smarty_tpl->tpl_vars['search_products']->value), 0);?>

	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./product-compare.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<br /><br />
	<?php if (!isset($_smarty_tpl->tpl_vars['instantSearch']->value)||(isset($_smarty_tpl->tpl_vars['instantSearch']->value)&&!$_smarty_tpl->tpl_vars['instantSearch']->value)) {?><?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }?>
<?php }?>

<script type="text/javascript">
   fbq('track', 'Search', {search_string: '<?php echo $_GET['search_query'];?>
'}); 
</script><?php }} ?>
