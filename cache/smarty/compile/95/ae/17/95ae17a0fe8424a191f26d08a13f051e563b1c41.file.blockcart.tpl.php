<?php /* Smarty version Smarty-3.1.19, created on 2022-01-07 19:15:28
         compiled from "/var/www/html/themes/ez20/modules/blockcart/blockcart.tpl" */ ?>
<?php /*%%SmartyHeaderCode:113146437461d883406ceb34-94677629%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '95ae17a0fe8424a191f26d08a13f051e563b1c41' => 
    array (
      0 => '/var/www/html/themes/ez20/modules/blockcart/blockcart.tpl',
      1 => 1637749975,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '113146437461d883406ceb34-94677629',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'ajax_allowed' => 0,
    'CUSTOMIZE_TEXTFIELD' => 0,
    'cart_qties' => 0,
    'link' => 0,
    'priceDisplay' => 0,
    'product_total' => 0,
    'colapseExpandStatus' => 0,
    'products' => 0,
    'product' => 0,
    'productId' => 0,
    'productAttributeId' => 0,
    'customizedDatas' => 0,
    'id_customization' => 0,
    'static_token' => 0,
    'customization' => 0,
    'cart_ez' => 0,
    'discounts' => 0,
    'discount' => 0,
    'img_dir' => 0,
    'shipping_cost' => 0,
    'total_wt' => 0,
    'tax_cost' => 0,
    'total' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61d88340707d57_40186314',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61d88340707d57_40186314')) {function content_61d88340707d57_40186314($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/var/www/html/tools/smarty/plugins/modifier.replace.php';
?>




<?php if ($_smarty_tpl->tpl_vars['ajax_allowed']->value) {?>
<script type="text/javascript">
/* var CUSTOMIZE_TEXTFIELD = '<?php echo $_smarty_tpl->tpl_vars['CUSTOMIZE_TEXTFIELD']->value;?>
'; */
var customizationIdMessage = '<?php echo smartyTranslate(array('s'=>'Customization #','mod'=>'blockcart','js'=>1),$_smarty_tpl);?>
';
var removingLinkText = '<?php echo smartyTranslate(array('s'=>'remove this product from my cart','mod'=>'blockcart','js'=>1),$_smarty_tpl);?>
';
</script>
<?php }?>

<!-- MODULE Block cart -->
<div id="cart_block2" class="block exclusive" style="border:0px;" >

	<div id="cart_block2_in">
		<div id="cart_block2_in_spans">
			<span class="ajax_cart_quantity" <?php if ($_smarty_tpl->tpl_vars['cart_qties']->value<=0) {?>style="display:none;"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink("order.php",true);?>
"><?php echo $_smarty_tpl->tpl_vars['cart_qties']->value;?>
</a></span>
		<span class="ajax_cart_product_txt_s" <?php if ($_smarty_tpl->tpl_vars['cart_qties']->value<=1) {?>style="display:none"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink("order.php",true);?>
"><?php echo smartyTranslate(array('s'=>'products','mod'=>'blockcart'),$_smarty_tpl);?>
</a></span>
		<span class="ajax_cart_product_txt" <?php if ($_smarty_tpl->tpl_vars['cart_qties']->value>1) {?>style="display:none"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink("order.php",true);?>
"><?php echo smartyTranslate(array('s'=>'prod.','mod'=>'blockcart'),$_smarty_tpl);?>
</a></span>
		<span class="ajax_cart_no_product" <?php if ($_smarty_tpl->tpl_vars['cart_qties']->value!=0) {?>style="display:none"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink("order.php",true);?>
"><?php echo smartyTranslate(array('s'=>'(empty)','mod'=>'blockcart'),$_smarty_tpl);?>
</a></span>
		<br />
		
		<span class="ajax_cart_total" style="font-weight:bold<?php if ($_smarty_tpl->tpl_vars['cart_qties']->value<=0) {?>;display:none<?php }?>"><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink("order.php",true);?>
"><?php if ($_smarty_tpl->tpl_vars['priceDisplay']->value==1) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['product_total']->value),$_smarty_tpl);?>
<?php } else { ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['product_total']->value),$_smarty_tpl);?>
<?php }?></a></span>

		</div>
		
		<div id="cart_icon" onmouseover="document.getElementById('cart_block_modal_fixed').style.display = 'block';">
		
		
		 

		<span><a style="display: inline-block; height: 100%;" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink("order.php",true);?>
" class="cart-block-span"><i style="font-size: 25px; color: #ff6600;" class="fas fa-shopping-cart"></i></a></span>
		</div>
		
	</div>
	<div class="block_content" style="position:relative">
	<!-- block summary -->
	<div id="cart_block_summary2" style="position:relative; margin-top:-25x; left:0px; z-index:9999" class="<?php if (isset($_smarty_tpl->tpl_vars['colapseExpandStatus']->value)&&$_smarty_tpl->tpl_vars['colapseExpandStatus']->value=='expanded'||!$_smarty_tpl->tpl_vars['ajax_allowed']->value||!isset($_smarty_tpl->tpl_vars['colapseExpandStatus']->value)) {?>collapsed<?php } else { ?>expanded<?php }?>">
	
		
	</div>
	<!-- block list of products -->
	</div>

<!-- /MODULE Block cart -->

</div>



<div id="cart_block_modal_fixed" class="block exclusive" >

	<div id="modal_cart_title">
		<table style="width:100%"><tr><td><a style="text-decoration:none;" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink("order.php",true);?>
"><h3 style="margin-left: 50px;
    color: #545454;
    font-size: 18px;
	font-weight:600;
    text-align: center;
    padding-bottom: 15px;
    text-transform: uppercase;">Il tuo carrello</h3></a></td><td style="text-align:right"><a href="javascript:void(0)" style="cursor:pointer" onclick="document.getElementById('cart_block_modal_fixed').style.display = 'none';" title="Chiudi" ><img src="https://www.ezdirect.it/img/icon-close.jpg" alt="Chiudi" title="Chiudi"></a></td></tr></table>
<br />
	<table class="block_cart_table">
	<!-- block summary -->
	
	
	<!-- block list of products -->
	
	<?php if ($_smarty_tpl->tpl_vars['products']->value) {?>
		
		<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
		<?php $_smarty_tpl->tpl_vars['productId'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['id_product'], null, 0);?>
			<?php $_smarty_tpl->tpl_vars['productAttributeId'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['id_product_attribute'], null, 0);?>
			
			<tr>
			<td style="border-top:1px solid #e5e5e5">
			<img src='https://www.ezdirect.it/img/p/<?php echo $_smarty_tpl->tpl_vars['product']->value['id_image'];?>
-medium.jpg' height="80" width="80" alt="" title="" />
			</td>
			<td style="border-top:1px solid #e5e5e5">
			<a class="cart_block_product_name" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getProductLink($_smarty_tpl->tpl_vars['product']->value['id_product'],$_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['category']);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" style="color:#0000ff; font-weight:bold"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</a>
			
			
			
				<!-- Customizable datas -->
				<?php if (isset($_smarty_tpl->tpl_vars['customizedDatas']->value[$_smarty_tpl->tpl_vars['productId']->value][$_smarty_tpl->tpl_vars['productAttributeId']->value])) {?>
					<br /><br />
					<ul class="cart_block_customizations" id="customization_<?php echo $_smarty_tpl->tpl_vars['productId']->value;?>
_<?php echo $_smarty_tpl->tpl_vars['productAttributeId']->value;?>
">
						<?php  $_smarty_tpl->tpl_vars['customization'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['customization']->_loop = false;
 $_smarty_tpl->tpl_vars['id_customization'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['customizedDatas']->value[$_smarty_tpl->tpl_vars['productId']->value][$_smarty_tpl->tpl_vars['productAttributeId']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['customization']->key => $_smarty_tpl->tpl_vars['customization']->value) {
$_smarty_tpl->tpl_vars['customization']->_loop = true;
 $_smarty_tpl->tpl_vars['id_customization']->value = $_smarty_tpl->tpl_vars['customization']->key;
?>
							<li name="customization">
								<div class="deleteCustomizableProduct" id="deleteCustomizableProduct_<?php echo intval($_smarty_tpl->tpl_vars['id_customization']->value);?>
_<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
_<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product_attribute']);?>
"><a class="ajax_cart_block_remove_link" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('cart.php');?>
&delete&amp;id_product=<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
&amp;ipa=<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product_attribute']);?>
&amp;id_customization=<?php echo $_smarty_tpl->tpl_vars['id_customization']->value;?>
&amp;token=<?php echo $_smarty_tpl->tpl_vars['static_token']->value;?>
"> </a></div>
								<span class="quantity-formated"><span class="quantity"><?php echo $_smarty_tpl->tpl_vars['customization']->value['quantity'];?>
</span>x</span><?php if (isset($_smarty_tpl->tpl_vars['customization']->value['datas'][$_smarty_tpl->tpl_vars['CUSTOMIZE_TEXTFIELD']->value][0])) {?>
								<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate(smarty_modifier_replace(htmlspecialchars($_smarty_tpl->tpl_vars['customization']->value['datas'][$_smarty_tpl->tpl_vars['CUSTOMIZE_TEXTFIELD']->value][0]['value'], ENT_QUOTES, 'UTF-8', true),"<br />"," "),28);?>

								<?php } else { ?>
								<?php echo smartyTranslate(array('s'=>'Customization #','mod'=>'blockcart'),$_smarty_tpl);?>
<?php echo intval($_smarty_tpl->tpl_vars['id_customization']->value);?>
<?php echo smartyTranslate(array('s'=>':','mod'=>'blockcart'),$_smarty_tpl);?>

								<?php }?>
							</li>
						<?php } ?>
					</ul>
					<?php if (!isset($_smarty_tpl->tpl_vars['product']->value['attributes_small'])) {?></dd><?php }?>
				<?php }?>
				
			<br /><br />
			<span class="price"><strong><?php if ($_smarty_tpl->tpl_vars['priceDisplay']->value==@constant('PS_TAX_EXC')) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayWtPrice'][0][0]->displayWtPrice(array('p'=>((string)$_smarty_tpl->tpl_vars['product']->value['total'])),$_smarty_tpl);?>
<?php } else { ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayWtPrice'][0][0]->displayWtPrice(array('p'=>((string)$_smarty_tpl->tpl_vars['product']->value['total_wt'])),$_smarty_tpl);?>
<?php }?></strong></span>
			</td>
			
			<td style="border-top:1px solid #e5e5e5">
				<span class="quantity-formated"><span class="quantity"><?php echo $_smarty_tpl->tpl_vars['product']->value['cart_quantity'];?>
</span></span>
			</td>	
			<td style="border-top:1px solid #e5e5e5">	
				<?php if ($_smarty_tpl->tpl_vars['cart_ez']->value==0) {?>
					<span class="remove_link"><?php if (!isset($_smarty_tpl->tpl_vars['customizedDatas']->value[$_smarty_tpl->tpl_vars['productId']->value][$_smarty_tpl->tpl_vars['productAttributeId']->value])) {?><a rel="nofollow" class="ajax_cart_block_remove_link" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('cart.php');?>
&delete&amp;id_product=<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
&amp;ipa=<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product_attribute'];?>
&amp;token=<?php echo $_smarty_tpl->tpl_vars['static_token']->value;?>
" title="<?php echo smartyTranslate(array('s'=>'remove this product from my cart','mod'=>'blockcart'),$_smarty_tpl);?>
"><img style="margin-bottom: 2px;" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/icon_delete.gif" alt="Cancella" title="Cancella" /></a><?php }?></span>
				<?php } else { ?>
					<span class="remove_link" style="visibility:hidden"><?php if (!isset($_smarty_tpl->tpl_vars['customizedDatas']->value[$_smarty_tpl->tpl_vars['productId']->value][$_smarty_tpl->tpl_vars['productAttributeId']->value])) {?><a rel="nofollow" style="visibility:hidden" class="ajax_cart_block_remove_link" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('cart.php');?>
&delete&amp;id_product=<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
&amp;ipa=<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product_attribute'];?>
&amp;token=<?php echo $_smarty_tpl->tpl_vars['static_token']->value;?>
" title="<?php echo smartyTranslate(array('s'=>'remove this product from my cart','mod'=>'blockcart'),$_smarty_tpl);?>
"></a><?php }?></span>
				<?php }?>
			</td>	
		</tr>
		<?php } ?>
		
	<?php }?>
	</table>
<br /><hr class="footer_hr" /><br />
	<table class="block_cart_table_2">
		<tr <?php if ($_smarty_tpl->tpl_vars['products']->value) {?>style="display:none"<?php }?>><td colspan="2"><br /><?php echo smartyTranslate(array('s'=>'Nessun prodotto','mod'=>'blockcart'),$_smarty_tpl);?>
</td></tr>

		<?php if (count($_smarty_tpl->tpl_vars['discounts']->value)>0) {?><table id="vouchers">
			<tr>
			<?php  $_smarty_tpl->tpl_vars['discount'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['discount']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['discounts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['discount']->key => $_smarty_tpl->tpl_vars['discount']->value) {
$_smarty_tpl->tpl_vars['discount']->_loop = true;
?>
					<td class="name" title="<?php echo $_smarty_tpl->tpl_vars['discount']->value['description'];?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate((($_smarty_tpl->tpl_vars['discount']->value['name']).(' : ')).($_smarty_tpl->tpl_vars['discount']->value['description']),18,'...'), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 <a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink("order.php",true);?>
&deleteDiscount=<?php echo $_smarty_tpl->tpl_vars['discount']->value['id_discount'];?>
" title="<?php echo smartyTranslate(array('s'=>'Delete'),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/delete.gif" width="11" height="11" alt="<?php echo smartyTranslate(array('s'=>'Delete'),$_smarty_tpl);?>
" width="11" height="13" class="icon" /></a></td>
					<td class="price">-<?php if ($_smarty_tpl->tpl_vars['discount']->value['value_real']!='!') {?><?php if ($_smarty_tpl->tpl_vars['priceDisplay']->value==1) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['discount']->value['value_tax_exc']),$_smarty_tpl);?>
<?php } else { ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['discount']->value['value_real']),$_smarty_tpl);?>
<?php }?><?php }?></td>
					
			</tr>
			<?php } ?>
			
		<?php }?>

		<tr>
		<td><br /><span><?php echo smartyTranslate(array('s'=>'Spedizione','mod'=>'blockcart'),$_smarty_tpl);?>
</td>
		</td><td  class="price"><br /><?php echo $_smarty_tpl->tpl_vars['shipping_cost']->value;?>
</td>

<tr><td><?php echo smartyTranslate(array('s'=>'Imponibile','mod'=>'blockcart'),$_smarty_tpl);?>
</td>
<td  class="price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['total_wt']->value),$_smarty_tpl);?>
</td>
			</tr>
			

			<tr><td><?php echo smartyTranslate(array('s'=>'IVA','mod'=>'blockcart'),$_smarty_tpl);?>
</td>
				<td class="price"><?php echo $_smarty_tpl->tpl_vars['tax_cost']->value;?>
</td>
			
			
	</table>		
	<hr class="footer_hr" /><br />
	<table class="block_cart_table_2">		
		<tr><td><br /><?php echo smartyTranslate(array('s'=>'Totale','mod'=>'blockcart'),$_smarty_tpl);?>
</td>
			<td class="price" style="font-size:20px" ><br /><?php echo $_smarty_tpl->tpl_vars['total']->value;?>
</td></tr></table>
	<hr class="footer_hr" /><br />	

		<p id="cart-buttons" style="text-align:center; margin-bottom:100px;">
			<a class="ajax_add_to_cart_button_home" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink("order.php",true);?>
" id="button_order_cart" title="<?php echo smartyTranslate(array('s'=>'Check out','mod'=>'blockcart'),$_smarty_tpl);?>
" style="margin: 0 auto;
display: block;
width: 85%;
padding-top: 12px; " ><?php echo smartyTranslate(array('s'=>'Vai alla cassa','mod'=>'blockcart'),$_smarty_tpl);?>
</a>
<br />
<img src="https://www.ezdirect.it/themes/ez20/img/carte-carrello.jpg" alt="" title="">
		</p>
		<div style="clear:both"></div>
	</div>
	</div>
</div>
</div> <!-- fine mostra carrello -->

<script type="text/javascript">
var element = $('#cart_block_modal_fixed').detach();
$('#menu-alto').append(element);
</script><?php }} ?>
