<?php /* Smarty version Smarty-3.1.19, created on 2022-01-07 19:04:10
         compiled from "/var/www/html/themes/ez20/product-sort.tpl" */ ?>
<?php /*%%SmartyHeaderCode:29567421261d8809a1a9bc8-27575498%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a14292feb80086357d48e32ee602bc15f533a47e' => 
    array (
      0 => '/var/www/html/themes/ez20/product-sort.tpl',
      1 => 1637749855,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '29567421261d8809a1a9bc8-27575498',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'orderby' => 0,
    'orderway' => 0,
    'category' => 0,
    'link' => 0,
    'manufacturer' => 0,
    'supplier' => 0,
    'request' => 0,
    'img_dir' => 0,
    'orderbydefault' => 0,
    'PS_CATALOG_MODE' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61d8809a1d8a66_12068735',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61d8809a1d8a66_12068735')) {function content_61d8809a1d8a66_12068735($_smarty_tpl) {?>

<?php if (isset($_smarty_tpl->tpl_vars['orderby']->value)&&isset($_smarty_tpl->tpl_vars['orderway']->value)) {?>
<!-- Sort products -->
<?php if (isset($_GET['id_category'])&&$_GET['id_category']) {?>
	<?php $_smarty_tpl->tpl_vars['request'] = new Smarty_variable($_smarty_tpl->tpl_vars['link']->value->getPaginationLink('category',$_smarty_tpl->tpl_vars['category']->value,false,true), null, 0);?>
<?php } elseif (isset($_GET['id_manufacturer'])&&$_GET['id_manufacturer']) {?>
	<?php $_smarty_tpl->tpl_vars['request'] = new Smarty_variable($_smarty_tpl->tpl_vars['link']->value->getPaginationLink('manufacturer',$_smarty_tpl->tpl_vars['manufacturer']->value,false,true), null, 0);?>
<?php } elseif (isset($_GET['id_supplier'])&&$_GET['id_supplier']) {?>
	<?php $_smarty_tpl->tpl_vars['request'] = new Smarty_variable($_smarty_tpl->tpl_vars['link']->value->getPaginationLink('supplier',$_smarty_tpl->tpl_vars['supplier']->value,false,true), null, 0);?>
<?php } else { ?>
	<?php $_smarty_tpl->tpl_vars['request'] = new Smarty_variable($_smarty_tpl->tpl_vars['link']->value->getPaginationLink(false,false,false,true), null, 0);?>
<?php }?>
<script type="text/javascript">


//<![CDATA[

$(document).ready(function()
{

	


	$('#selectPrductSort').change(function()
	{
		var requestSortProducts = '<?php echo $_smarty_tpl->tpl_vars['request']->value;?>
';
		var splitData = $(this).val().split(':');
		document.location.href = requestSortProducts + ((requestSortProducts.indexOf('?') < 0) ? '?' : '&') + 'orderby=' + splitData[0] + '&orderway=' + splitData[1];
	});
	
	$('#nameasc').bind('click', function() {
		$('#nameascord').attr("selected", "selected"); 
		$('#nameascord').trigger('change');
	});

		$('#namedesc').bind('click', function() {
		$('#namedescord').attr("selected", "selected"); 
		$('#namedescord').trigger('change');
	});
	
		$('#priceasc').bind('click', function() {
		$('#priceascord').attr("selected", "selected"); 
		$('#priceascord').trigger('change');
	});
	
		$('#pricedesc').bind('click', function() {
		$('#pricedescord').attr("selected", "selected"); 
		$('#pricedescord').trigger('change');
	});
	
		$('#dateadddesc').bind('click', function() {
		$('#dateadddescord').attr("selected", "selected"); 
		$('#dateadddescord').trigger('change');
	});
	
		$('#dateadddescimg').bind('click', function() {
		$('#dateadddescord').attr("selected", "selected"); 
		$('#dateadddescord').trigger('change');
	});
	
		$('#vendutidesc').bind('click', function() {
		$('#vendutidescord').attr("selected", "selected"); 
		$('#vendutidescord').trigger('change');
	});
	
		$('#vendutidescimg').bind('click', function() {
		$('#vendutidescord').attr("selected", "selected"); 
		$('#vendutidescord').trigger('change');
	});
	
		$('#disponibilidescimg').bind('click', function() {
		$('#disponibilidescord').attr("selected", "selected"); 
		$('#disponibilidescord').trigger('change');
	});
	
		$('#disponibilidesc').bind('click', function() {
		$('#disponibilidescord').attr("selected", "selected"); 
		$('#disponibilidescord').trigger('change');
	});
});
//]]>


</script>


<div id="ordinamento-prodotti">
	<div style="display:none">
	<div style="float:left" id="the_products">
	<img <?php if ($_GET['orderby']) {?><?php } else { ?><?php }?> src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/alfabetico.png" style="float:left; margin-right:3px" width="26" height="26" alt="<?php echo smartyTranslate(array('s'=>'Sort by name'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'Sort by name'),$_smarty_tpl);?>
" />
	<img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/sort_asc.gif" id="nameasc" class="ordinamentoimg" alt="<?php echo smartyTranslate(array('s'=>'Sort by name from A to Z'),$_smarty_tpl);?>
" height="9" width="9" title="<?php echo smartyTranslate(array('s'=>'Sort by name from A to Z'),$_smarty_tpl);?>
" /><br /><br />
	<img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/sort_desc.gif" class="ordinamentoimg" id="namedesc" style="display:block; margin-top:-15px" height="9" width="9" alt="<?php echo smartyTranslate(array('s'=>'Sort by name from Z to A'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'Sort by name from Z to A'),$_smarty_tpl);?>
" />
	</div>

	<div style="display:block; float:left; margin-left:40px">
	<img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/euro.png" style="float:left; margin-right:3px" alt="<?php echo smartyTranslate(array('s'=>'Sort by price'),$_smarty_tpl);?>
" height="26" width="26"  title="<?php echo smartyTranslate(array('s'=>'Sort by price'),$_smarty_tpl);?>
" />
	<img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/sort_asc.gif" class="ordinamentoimg" id="priceasc" alt="<?php echo smartyTranslate(array('s'=>'Sort by price from less to more expensive'),$_smarty_tpl);?>
" height="9" width="9" title="<?php echo smartyTranslate(array('s'=>'Sort by price from less to more expensive'),$_smarty_tpl);?>
" /><br /><br />
	<img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/sort_desc.gif" class="ordinamentoimg" id="pricedesc" style="display:block; margin-top:-15px" height="9" width="9" alt="<?php echo smartyTranslate(array('s'=>'Sort by price from more to less expensive'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'Sort by price from more to less expensive'),$_smarty_tpl);?>
" />
	</div>

	<div style="display:block; float:left; margin-left:40px">
	<img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/clock.png" style="display:block; float:left; margin-right:3px; cursor:pointer" id="dateadddescimg" height="26" width="26" alt="<?php echo smartyTranslate(array('s'=>'New products'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'New products'),$_smarty_tpl);?>
" />
	<span style="display:block; float:left;  width:50px; margin-top:-2px"><a id="dateadddesc" style="cursor:pointer">Nuovi arrivi</a></span>
	</div>

	<div style="display:block; float:left; margin-left:40px">
	<img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/chart.png" style="display:block; float:left; margin-right:3px; cursor:pointer" height="26" width="26" id="vendutidescimg" alt="<?php echo smartyTranslate(array('s'=>'Best sellers'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'Best sellers'),$_smarty_tpl);?>
" />
	<span style="display:block;float:left; margin-top:-2px; width:50px"><a id="vendutidesc" style="cursor:pointer">I pi&ugrave; venduti</a></span>
	</div>
	
	
	</div>
	<form id="productsSortForm" name="formordinamento" action="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['request']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
		<fieldset>
			<select name="selectordinamento" id="selectPrductSort" style="color: #737373">
				<option value="posizione:asc" <?php if ($_smarty_tpl->tpl_vars['orderby']->value==$_smarty_tpl->tpl_vars['orderbydefault']->value) {?>selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>'--'),$_smarty_tpl);?>
</option>
				<?php if (!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
					<option id="priceascord" value="price:asc" <?php if ($_smarty_tpl->tpl_vars['orderby']->value=='price'&&$_smarty_tpl->tpl_vars['orderway']->value=='asc') {?>selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>'Price: lowest first'),$_smarty_tpl);?>
</option>
					<option id="pricedescord" value="price:desc" <?php if ($_smarty_tpl->tpl_vars['orderby']->value=='price'&&$_smarty_tpl->tpl_vars['orderway']->value=='desc') {?>selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>'Price: highest first'),$_smarty_tpl);?>
</option>
				<?php }?>
				<option id="nameascord" value="name:asc" <?php if ($_smarty_tpl->tpl_vars['orderby']->value=='name'&&$_smarty_tpl->tpl_vars['orderway']->value=='asc') {?>selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>'Product Name: A to Z'),$_smarty_tpl);?>
</option>
				<option id="namedescord" value="name:desc" <?php if ($_smarty_tpl->tpl_vars['orderby']->value=='name'&&$_smarty_tpl->tpl_vars['orderway']->value=='desc') {?>selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>'Product Name: Z to A'),$_smarty_tpl);?>
</option>
				<?php if (!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
					<option id="dateadddescord" value="date_add:desc" <?php if ($_smarty_tpl->tpl_vars['orderby']->value=='date_add'&&$_smarty_tpl->tpl_vars['orderway']->value=='desc') {?>selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>'Date added'),$_smarty_tpl);?>
</option>
					<option id="vendutidescord" value="venduti:desc" <?php if ($_smarty_tpl->tpl_vars['orderby']->value=='venduti'&&$_smarty_tpl->tpl_vars['orderway']->value=='desc') {?>selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>'Best sellers'),$_smarty_tpl);?>
</option>
					<option id="disponibilidescord" value="disponibili:desc" <?php if ($_smarty_tpl->tpl_vars['orderby']->value=='disponibili'&&$_smarty_tpl->tpl_vars['orderway']->value=='desc') {?>selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>'In stock'),$_smarty_tpl);?>
</option>
				<?php }?>
			</select>
			<label for="selectPrductSort" style="color: #737373; font-weight: 400; padding-right: 10px;"><?php echo smartyTranslate(array('s'=>'Ordina per'),$_smarty_tpl);?>
</label>
		</fieldset>
	</form>


</div>

<!-- /Sort products -->
<?php }?>
<?php }} ?>
