<?php /* Smarty version Smarty-3.1.19, created on 2022-01-07 19:15:20
         compiled from "/var/www/html/themes/ez20/order-steps.tpl" */ ?>
<?php /*%%SmartyHeaderCode:185981134661d88338c9e893-77755005%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dd4c39338209342bb990c98cb2ad1c745f386433' => 
    array (
      0 => '/var/www/html/themes/ez20/order-steps.tpl',
      1 => 1637749849,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '185981134661d88338c9e893-77755005',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'opc' => 0,
    'current_step' => 0,
    'link' => 0,
    'back' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61d88338cb3e53_69880088',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61d88338cb3e53_69880088')) {function content_61d88338cb3e53_69880088($_smarty_tpl) {?>


<?php if (!$_smarty_tpl->tpl_vars['opc']->value) {?>
<!-- Steps -->

<ul class="crumb-trail clearfix">
	
			<li style="background-image:none; " class="crumb pull-left" class="<?php if ($_smarty_tpl->tpl_vars['current_step']->value=='summary') {?>step_current<?php } else { ?><?php if ($_smarty_tpl->tpl_vars['current_step']->value=='payment'||$_smarty_tpl->tpl_vars['current_step']->value=='shipping'||$_smarty_tpl->tpl_vars['current_step']->value=='thanks'||$_smarty_tpl->tpl_vars['current_step']->value=='address'||$_smarty_tpl->tpl_vars['current_step']->value=='login') {?>step_start<?php } else { ?>step_todo<?php }?><?php }?>">
				<?php if ($_smarty_tpl->tpl_vars['current_step']->value=='payment'||$_smarty_tpl->tpl_vars['current_step']->value=='shipping'||$_smarty_tpl->tpl_vars['current_step']->value=='summary'||$_smarty_tpl->tpl_vars['current_step']->value=='address'||$_smarty_tpl->tpl_vars['current_step']->value=='login') {?>
				<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('order.php',true);?>
<?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>?back=<?php echo $_smarty_tpl->tpl_vars['back']->value;?>
<?php }?>">
					1. <?php echo smartyTranslate(array('s'=>'Summary'),$_smarty_tpl);?>

				</a>
				<?php } else { ?>
				1. <?php echo smartyTranslate(array('s'=>'Summary'),$_smarty_tpl);?>

				<?php }?>
			</li>
			

			<li style="background-image:none; " class="crumb pull-left" class="<?php if ($_smarty_tpl->tpl_vars['current_step']->value=='login'||$_smarty_tpl->tpl_vars['current_step']->value=='thanks') {?>step_current<?php } else { ?><?php if ($_smarty_tpl->tpl_vars['current_step']->value=='payment'||$_smarty_tpl->tpl_vars['current_step']->value=='shipping'||$_smarty_tpl->tpl_vars['current_step']->value=='address') {?>step_done<?php } else { ?>step_todo<?php }?><?php }?>">
				<?php if ($_smarty_tpl->tpl_vars['current_step']->value=='payment'||$_smarty_tpl->tpl_vars['current_step']->value=='shipping'||$_smarty_tpl->tpl_vars['current_step']->value=='address') {?>
				<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('order.php',true);?>
?step=1<?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>&amp;back=<?php echo $_smarty_tpl->tpl_vars['back']->value;?>
<?php }?>">
					2. <?php echo smartyTranslate(array('s'=>'Login'),$_smarty_tpl);?>

				</a>
				<?php } else { ?>
				2. <?php echo smartyTranslate(array('s'=>'Login'),$_smarty_tpl);?>

				<?php }?>
			</li>


			<li style="background-image:none; " class="crumb pull-left" class="<?php if ($_smarty_tpl->tpl_vars['current_step']->value=='address') {?>step_current<?php } else { ?><?php if ($_smarty_tpl->tpl_vars['current_step']->value=='payment'||$_smarty_tpl->tpl_vars['current_step']->value=='shipping') {?>step_done<?php } else { ?>step_todo<?php }?><?php }?>">
				<?php if ($_smarty_tpl->tpl_vars['current_step']->value=='payment'||$_smarty_tpl->tpl_vars['current_step']->value=='shipping') {?>
				<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('order.php',true);?>
?step=1<?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>&amp;back=<?php echo $_smarty_tpl->tpl_vars['back']->value;?>
<?php }?>">
					3. <?php echo smartyTranslate(array('s'=>'Address'),$_smarty_tpl);?>

				</a>
				<?php } else { ?>
				3. <?php echo smartyTranslate(array('s'=>'Address'),$_smarty_tpl);?>

				<?php }?>
			</li>


			<li style="background-image:none; " class="crumb pull-left" class="<?php if ($_smarty_tpl->tpl_vars['current_step']->value=='shipping') {?>step_current<?php } else { ?><?php if ($_smarty_tpl->tpl_vars['current_step']->value=='payment') {?>step_done<?php } else { ?>step_todo<?php }?><?php }?>">
				<?php if ($_smarty_tpl->tpl_vars['current_step']->value=='payment') {?>
				<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('order.php',true);?>
?step=2<?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>&amp;back=<?php echo $_smarty_tpl->tpl_vars['back']->value;?>
<?php }?>">
					4. <?php echo smartyTranslate(array('s'=>'Shipping'),$_smarty_tpl);?>

				</a>
				<?php } else { ?>
				4. <?php echo smartyTranslate(array('s'=>'Shipping'),$_smarty_tpl);?>

				<?php }?>
			</li>

			<li style="background-image:none; " class="crumb pull-left" id="step_end" class="<?php if ($_smarty_tpl->tpl_vars['current_step']->value=='payment') {?>step_current<?php } else { ?>step_todo<?php }?>">
				5. <?php echo smartyTranslate(array('s'=>'Payment'),$_smarty_tpl);?>

			</li>
</ul>






<!-- /Steps -->
<?php }?>
<div style="clear:both"></div><?php }} ?>
