<?php /* Smarty version Smarty-3.1.19, created on 2021-11-24 13:06:39
         compiled from "/var/www/html/modules/gestpay/views/templates/admin/configuration.tpl" */ ?>
<?php /*%%SmartyHeaderCode:51234484619e2acf66d104-14789017%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9d572d497bd225138626483eeaec01da7ba5bcbf' => 
    array (
      0 => '/var/www/html/modules/gestpay/views/templates/admin/configuration.tpl',
      1 => 1634899860,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '51234484619e2acf66d104-14789017',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'controller_url' => 0,
    'controller_name' => 0,
    'module_dir' => 0,
    'request_uri' => 0,
    'current_view' => 0,
    'module_version' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_619e2acf67f445_25196683',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_619e2acf67f445_25196683')) {function content_619e2acf67f445_25196683($_smarty_tpl) {?>


<script>
	var admin_module_ajax_url = '<?php echo strtr($_smarty_tpl->tpl_vars['controller_url']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
';
	var admin_module_controller = "<?php echo strtr($_smarty_tpl->tpl_vars['controller_name']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
";
</script>

<?php if (@constant('_PS_VERSION_')>=1.6) {?>
	<!-- Module content -->
	<div class="text-center header-container">
		<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
views/img/header_img.png" itemprop="logo" class="header_img">	
	</div>
	<div id="modulecontent" class="clearfix">
		<!-- Nav tabs -->
		<div class="col-lg-2">
			<div class="list-group left_menu">				
				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['request_uri']->value, ENT_QUOTES, 'UTF-8', true);?>
&current_view=config" class="list-group-item <?php if ($_smarty_tpl->tpl_vars['current_view']->value=='config'||$_smarty_tpl->tpl_vars['current_view']->value=='') {?>active<?php }?>"><i class="icon-wrench"></i> <?php echo smartyTranslate(array('s'=>'General configuration','mod'=>'gestpay'),$_smarty_tpl);?>
</a>
				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['request_uri']->value, ENT_QUOTES, 'UTF-8', true);?>
&current_view=url" class="list-group-item <?php if ($_smarty_tpl->tpl_vars['current_view']->value=='url') {?>active<?php }?>"><i class="icon-link"></i> <?php echo smartyTranslate(array('s'=>'Url','mod'=>'gestpay'),$_smarty_tpl);?>
</a>
				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['request_uri']->value, ENT_QUOTES, 'UTF-8', true);?>
&current_view=log_table" class="list-group-item <?php if ($_smarty_tpl->tpl_vars['current_view']->value=='log_table') {?>active<?php }?>"><i class="icon-table"></i> <?php echo smartyTranslate(array('s'=>'Transactions log','mod'=>'gestpay'),$_smarty_tpl);?>
</a>				
				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['request_uri']->value, ENT_QUOTES, 'UTF-8', true);?>
&current_view=help" class="list-group-item <?php if ($_smarty_tpl->tpl_vars['current_view']->value=='help') {?>active<?php }?>"><i class="icon-book"></i> <?php echo smartyTranslate(array('s'=>'Help','mod'=>'gestpay'),$_smarty_tpl);?>
</a>
				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['request_uri']->value, ENT_QUOTES, 'UTF-8', true);?>
&current_view=logs" class="list-group-item <?php if ($_smarty_tpl->tpl_vars['current_view']->value=='logs') {?>active<?php }?>"><i class="icon-gear"></i> <?php echo smartyTranslate(array('s'=>'Logs','mod'=>'gestpay'),$_smarty_tpl);?>
</a>
				<p class="text-center version_info"><i class="icon-info-circle"></i> <?php echo smartyTranslate(array('s'=>'Version','mod'=>'gestpay'),$_smarty_tpl);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_version']->value, ENT_QUOTES, 'UTF-8', true);?>
</p>
			</div>
		</div>
		<!-- Tab panes -->
		<div class="tab-content col-lg-10">   
			<div class="tab-pane panel <?php if ($_smarty_tpl->tpl_vars['current_view']->value=='config'||$_smarty_tpl->tpl_vars['current_view']->value=='') {?>active<?php }?>" id="config">
                <?php echo $_smarty_tpl->getSubTemplate ("./tabs/config.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			</div>
			<div class="tab-pane panel <?php if ($_smarty_tpl->tpl_vars['current_view']->value=='url') {?>active<?php }?>" id="url">
                <?php echo $_smarty_tpl->getSubTemplate ("./tabs/url.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			</div>
			<div class="tab-pane panel <?php if ($_smarty_tpl->tpl_vars['current_view']->value=='log_table') {?>active<?php }?>" id="log_table">
                <?php echo $_smarty_tpl->getSubTemplate ("./tabs/log_table.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			</div>
			<div class="tab-pane panel <?php if ($_smarty_tpl->tpl_vars['current_view']->value=='help') {?>active<?php }?>" id="help">
                <?php echo $_smarty_tpl->getSubTemplate ("./tabs/help.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			</div>
			<div class="tab-pane panel <?php if ($_smarty_tpl->tpl_vars['current_view']->value=='logs') {?>active<?php }?>" id="logs">
                <?php echo $_smarty_tpl->getSubTemplate ("./tabs/logs.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			</div>
		</div>
	</div>
<?php } else { ?>
    <!-- Module content -->
	<div class="text-center header-container">
		<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
views/img/header_img.png" itemprop="logo" class="header_img">	
		<p class="text-center version_info"><i class="icon-info-circle"></i> <?php echo smartyTranslate(array('s'=>'Version','mod'=>'gestpay'),$_smarty_tpl);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_version']->value, ENT_QUOTES, 'UTF-8', true);?>
</p>
	</div>
	<div id="modulecontent" class="clearfix">		
		<!-- Tab panes -->
		<div class="tab-content col-lg-10">   
			<div class="tab-pane panel" id="config">
                <?php echo $_smarty_tpl->getSubTemplate ("./tabs/config.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			</div>
            <br/>
			<div class="tab-pane panel id="url">
                <?php echo $_smarty_tpl->getSubTemplate ("./tabs/url.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			</div>			
            <br/>
            <div class="tab-pane panel" id="help_gestpay">
                <?php echo $_smarty_tpl->getSubTemplate ("./tabs/help.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			</div>
			<br/>
			<div class="tab-pane panel" id="log_table">
                <?php echo $_smarty_tpl->getSubTemplate ("./tabs/log_table.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			</div>
			<div class="tab-pane panel" id="logs">
                <?php echo $_smarty_tpl->getSubTemplate ("./tabs/logs.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			</div>
		</div>
	</div>
<?php }?><?php }} ?>
