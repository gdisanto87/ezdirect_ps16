<?php /* Smarty version Smarty-3.1.19, created on 2021-12-01 09:54:50
         compiled from "/var/www/html/modules/mieofferte/offerte-ez20.tpl" */ ?>
<?php /*%%SmartyHeaderCode:103589109261a7385adbe582-30667911%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '945e57acb472957394dbe6a04796f9732f14bea5' => 
    array (
      0 => '/var/www/html/modules/mieofferte/offerte-ez20.tpl',
      1 => 1637753753,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '103589109261a7385adbe582-30667911',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'navigationPipe' => 0,
    'carrelli' => 0,
    'carrello' => 0,
    'img_dir' => 0,
    'base_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61a7385ade1376_21376107',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61a7385ade1376_21376107')) {function content_61a7385ade1376_21376107($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/html/tools/smarty/plugins/modifier.date_format.php';
?>

<div id="mieofferte">
	<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account.php',true);?>
"><?php echo smartyTranslate(array('s'=>'My account','mod'=>'mieofferte'),$_smarty_tpl);?>
</a><span class="navigation-pipe"><?php echo $_smarty_tpl->tpl_vars['navigationPipe']->value;?>
</span><?php echo smartyTranslate(array('s'=>'My tickets','mod'=>'mieofferte'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	<h2><?php echo smartyTranslate(array('s'=>'My offers','mod'=>'mieofferte'),$_smarty_tpl);?>
</h2>
	<br />
	
	<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


	
	
	
		<?php if ($_smarty_tpl->tpl_vars['carrelli']->value) {?>
			<table class="std2">
				<thead>
					<tr>
						<th class="first_item" style="background-color:#8899cc;text-align:left"><?php echo smartyTranslate(array('s'=>'ID offer','mod'=>'mieofferte'),$_smarty_tpl);?>
</th>
						<th style="background-color:#99aadd; text-align:left"><?php echo smartyTranslate(array('s'=>'Offer name','mod'=>'mieofferte'),$_smarty_tpl);?>
</th>
						<th style="background-color:#8899cc;text-align:left"><?php echo smartyTranslate(array('s'=>'Offer date','mod'=>'mieofferte'),$_smarty_tpl);?>
</th>
						<th style="background-color:#99aadd; text-align:left"><?php echo smartyTranslate(array('s'=>'Offer deadline','mod'=>'mieofferte'),$_smarty_tpl);?>
</th>
						<th style="background-color:#8899cc;text-align:left"><?php echo smartyTranslate(array('s'=>'View offer','mod'=>'mieofferte'),$_smarty_tpl);?>
</th>
						<th class="last_item" style="background-color:#99aadd; text-align:left"><?php echo smartyTranslate(array('s'=>'Download','mod'=>'mieofferte'),$_smarty_tpl);?>
</th>
					</tr>
				</thead>
				<tbody>
				<?php  $_smarty_tpl->tpl_vars['carrello'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['carrello']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['carrelli']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['carrello']->key => $_smarty_tpl->tpl_vars['carrello']->value) {
$_smarty_tpl->tpl_vars['carrello']->_loop = true;
?>
				
				
				
				
				
				<tr>
					<td style="cursor:pointer" onclick="window.location='<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/mieofferte/offerte.php',true);?>
?id_cart=<?php echo $_smarty_tpl->tpl_vars['carrello']->value['id_cart'];?>
&id_customer=<?php echo $_smarty_tpl->tpl_vars['carrello']->value['id_customer'];?>
'">
				
					<?php echo $_smarty_tpl->tpl_vars['carrello']->value['id_cart'];?>
-<?php echo $_smarty_tpl->tpl_vars['carrello']->value['revisioni'];?>

				
				</td>
					
						<td style="cursor:pointer" onclick="'window.location=<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/mieofferte/offerte.php',true);?>
?id_cart=<?php echo $_smarty_tpl->tpl_vars['carrello']->value['id_cart'];?>
&id_customer=<?php echo $_smarty_tpl->tpl_vars['carrello']->value['id_customer'];?>
'"><?php echo $_smarty_tpl->tpl_vars['carrello']->value['name'];?>
</td>
				
				<td style="cursor:pointer" onclick="'window.location=<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/mieofferte/offerte.php',true);?>
?id_cart=<?php echo $_smarty_tpl->tpl_vars['carrello']->value['id_cart'];?>
&id_customer=<?php echo $_smarty_tpl->tpl_vars['carrello']->value['id_customer'];?>
'"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['carrello']->value['date_add'],"%d/%m/%Y");?>
</td>
					
					<td style="cursor:pointer" onclick="'window.location=<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/mieofferte/offerte.php',true);?>
?id_cart=<?php echo $_smarty_tpl->tpl_vars['carrello']->value['id_cart'];?>
&id_customer=<?php echo $_smarty_tpl->tpl_vars['carrello']->value['id_customer'];?>
'"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['carrello']->value['validita'],"%d/%m/%Y");?>
</td>
					
					<td style="cursor:pointer" onclick="'window.location=<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/mieofferte/offerte.php',true);?>
?id_cart=<?php echo $_smarty_tpl->tpl_vars['carrello']->value['id_cart'];?>
&id_customer=<?php echo $_smarty_tpl->tpl_vars['carrello']->value['id_customer'];?>
'">
					<?php if ($_smarty_tpl->tpl_vars['carrello']->value['id_order']>0) {?>
					
						<?php echo smartyTranslate(array('s'=>'You have already used this offer','mod'=>'mieofferte'),$_smarty_tpl);?>

					<?php } else { ?>
						<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/mieofferte/offerte.php',true);?>
?id_cart=<?php echo $_smarty_tpl->tpl_vars['carrello']->value['id_cart'];?>
&id_customer=<?php echo $_smarty_tpl->tpl_vars['carrello']->value['id_customer'];?>
"><?php echo smartyTranslate(array('s'=>'Click here to view your offer','mod'=>'mieofferte'),$_smarty_tpl);?>
</a>
					<?php }?>
					</td>
					<td><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/mieofferte/download_offerta.php',true);?>
?download=yes&originale=y&id_cart=<?php echo $_smarty_tpl->tpl_vars['carrello']->value['id_cart'];?>
&id_customer=<?php echo $_smarty_tpl->tpl_vars['carrello']->value['id_customer'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/pdf.gif" alt="" class="icon" /></a><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/mieofferte/download_offerta.php',true);?>
?download=yes&originale=y&id_cart=<?php echo $_smarty_tpl->tpl_vars['carrello']->value['id_cart'];?>
&id_customer=<?php echo $_smarty_tpl->tpl_vars['carrello']->value['id_customer'];?>
"><?php echo smartyTranslate(array('s'=>'Click here to download your offer','mod'=>'mieofferte'),$_smarty_tpl);?>
</a></td>
					
				</tr>
				</tbody>
			<?php } ?>
			</table>
		
		<?php } else { ?>
		
			<p class="warning"><?php echo smartyTranslate(array('s'=>'You have no offers','mod'=>'mieofferte'),$_smarty_tpl);?>
.</p>
		
		<?php }?>
		<br />
		
		
		<ul class="footer_links">
		
		
		<li><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/formprevendita/form.php',true);?>
"><img src="https://www.ezdirect.it/themes/ez20/img/icon/account/quotazione.png" alt="" class="icon" height="36" width="36" /></a><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/formprevendita/form.php',true);?>
"><?php echo smartyTranslate(array('s'=>'Click here to ask for a new quotation','mod'=>'mieofferte'),$_smarty_tpl);?>
</a></li>
		
		<li><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/mieofferte/offerte.php',true);?>
?new_cart=yes"><img height="36" width="36" src="https://www.ezdirect.it/themes/ez20/img/icon/account/ordini.png" alt="" class="icon" /></a><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('modules/mieofferte/offerte.php',true);?>
?new_cart=yes"><?php echo smartyTranslate(array('s'=>'Click here to create a new cart','mod'=>'mieofferte'),$_smarty_tpl);?>
</a></li>
		
		<li><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account.php',true);?>
"><img height="36" width="36" src="https://www.ezdirect.it/themes/ez20/img/icon/account/user.png" alt="" class="icon" /></a><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account.php',true);?>
"><?php echo smartyTranslate(array('s'=>'Back to Your Account','mod'=>'mieofferte'),$_smarty_tpl);?>
</a></li>
		<li><a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
"><img height="36" width="36" src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/home.gif" alt="" class="icon" /></a><a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
"><?php echo smartyTranslate(array('s'=>'Home','mod'=>'mieofferte'),$_smarty_tpl);?>
</a></li>
		</ul>
		
	
</div>
<?php }} ?>
