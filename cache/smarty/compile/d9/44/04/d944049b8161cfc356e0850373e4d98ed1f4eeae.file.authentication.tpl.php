<?php /* Smarty version Smarty-3.1.19, created on 2022-01-04 10:10:51
         compiled from "/var/www/html/themes/ez20/authentication.tpl" */ ?>
<?php /*%%SmartyHeaderCode:47409175861d40f1b83b6f5-81995115%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd944049b8161cfc356e0850373e4d98ed1f4eeae' => 
    array (
      0 => '/var/www/html/themes/ez20/authentication.tpl',
      1 => 1637749847,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '47409175861d40f1b83b6f5-81995115',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'dlv_all_fields' => 0,
    'js_dir' => 0,
    'countries' => 0,
    'country' => 0,
    'state' => 0,
    'address' => 0,
    'vat_management' => 0,
    'back' => 0,
    'email_create' => 0,
    'link' => 0,
    'inOrderProcess' => 0,
    'PS_GUEST_CHECKOUT_ENABLED' => 0,
    'HOOK_CREATE_ACCOUNT_TOP' => 0,
    'days' => 0,
    'day' => 0,
    'sl_day' => 0,
    'months' => 0,
    'k' => 0,
    'sl_month' => 0,
    'years' => 0,
    'year' => 0,
    'sl_year' => 0,
    'v' => 0,
    'sl_country' => 0,
    'this_state' => 0,
    'stateExist' => 0,
    'HOOK_CREATE_ACCOUNT_FORM' => 0,
    'img_dir' => 0,
    'field_name' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61d40f1b8efc19_12695884',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61d40f1b8efc19_12695884')) {function content_61d40f1b8efc19_12695884($_smarty_tpl) {?>






<?php if (!isset($_smarty_tpl->tpl_vars['dlv_all_fields']->value)) {?>
		<?php $_smarty_tpl->createLocalArrayVariable('dlv_all_fields', null, 0);
$_smarty_tpl->tpl_vars['dlv_all_fields']->value[0] = 'company';?>
		<?php $_smarty_tpl->createLocalArrayVariable('dlv_all_fields', null, 0);
$_smarty_tpl->tpl_vars['dlv_all_fields']->value[1] = 'firstname';?>
		<?php $_smarty_tpl->createLocalArrayVariable('dlv_all_fields', null, 0);
$_smarty_tpl->tpl_vars['dlv_all_fields']->value[2] = 'lastname';?>
		<?php $_smarty_tpl->createLocalArrayVariable('dlv_all_fields', null, 0);
$_smarty_tpl->tpl_vars['dlv_all_fields']->value[3] = 'address1';?>
		<?php $_smarty_tpl->createLocalArrayVariable('dlv_all_fields', null, 0);
$_smarty_tpl->tpl_vars['dlv_all_fields']->value[4] = 'address2';?>
		<?php $_smarty_tpl->createLocalArrayVariable('dlv_all_fields', null, 0);
$_smarty_tpl->tpl_vars['dlv_all_fields']->value[5] = 'suburb';?>
		<?php $_smarty_tpl->createLocalArrayVariable('dlv_all_fields', null, 0);
$_smarty_tpl->tpl_vars['dlv_all_fields']->value[6] = 'c_o';?>
		<?php $_smarty_tpl->createLocalArrayVariable('dlv_all_fields', null, 0);
$_smarty_tpl->tpl_vars['dlv_all_fields']->value[7] = 'postcode';?>
		<?php $_smarty_tpl->createLocalArrayVariable('dlv_all_fields', null, 0);
$_smarty_tpl->tpl_vars['dlv_all_fields']->value[8] = 'city';?>
		<?php $_smarty_tpl->createLocalArrayVariable('dlv_all_fields', null, 0);
$_smarty_tpl->tpl_vars['dlv_all_fields']->value[9] = 'country';?>
		<?php $_smarty_tpl->createLocalArrayVariable('dlv_all_fields', null, 0);
$_smarty_tpl->tpl_vars['dlv_all_fields']->value[10] = 'state';?>
<?php }?>

<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'Login'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['js_dir']->value;?>
etc/noSpaces.js">
</script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['js_dir']->value;?>
etc/profileChange.js">
</script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['js_dir']->value;?>
tools.js">
</script>

<script type="text/javascript">
// <![CDATA[
idSelectedCountry = <?php if (isset($_POST['id_state'])) {?><?php echo intval($_POST['id_state']);?>
<?php } else { ?>false<?php }?>;
countries = new Array();
countriesNeedIDNumber = new Array();
countriesNeedZipCode = new Array();
<?php if (isset($_smarty_tpl->tpl_vars['countries']->value)) {?>
	<?php  $_smarty_tpl->tpl_vars['country'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['country']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['country']->key => $_smarty_tpl->tpl_vars['country']->value) {
$_smarty_tpl->tpl_vars['country']->_loop = true;
?>
		<?php if (isset($_smarty_tpl->tpl_vars['country']->value['states'])&&$_smarty_tpl->tpl_vars['country']->value['contains_states']) {?>
			countries[<?php echo intval($_smarty_tpl->tpl_vars['country']->value['id_country']);?>
] = new Array();
			<?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['country']->value['states']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value) {
$_smarty_tpl->tpl_vars['state']->_loop = true;
?>
				countries[<?php echo intval($_smarty_tpl->tpl_vars['country']->value['id_country']);?>
].push({'id' : '<?php echo $_smarty_tpl->tpl_vars['state']->value['id_state'];?>
', 'name' : '<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
'});
			<?php } ?>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['country']->value['need_identification_number']) {?>
			countriesNeedIDNumber.push(<?php echo intval($_smarty_tpl->tpl_vars['country']->value['id_country']);?>
);
		<?php }?>
		<?php if (isset($_smarty_tpl->tpl_vars['country']->value['need_zip_code'])) {?>
			countriesNeedZipCode[<?php echo intval($_smarty_tpl->tpl_vars['country']->value['id_country']);?>
] = <?php echo $_smarty_tpl->tpl_vars['country']->value['need_zip_code'];?>
;
		<?php }?>
	<?php } ?>
<?php }?>
$(function(){
	$('.id_state option[value=<?php if (isset($_POST['id_state'])) {?><?php echo $_POST['id_state'];?>
<?php } else { ?><?php if (isset($_smarty_tpl->tpl_vars['address']->value)) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['address']->value->id_state, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?><?php }?>]').attr('selected', 'selected');
});
//]]>
<?php if ($_smarty_tpl->tpl_vars['vat_management']->value) {?>
	
	$(document).ready(function() {
		$('#company').blur(function(){
			vat_number();
		});
		vat_number();
		function vat_number()
		{
			if ($('#company').val() != '')
				$('#vat_number').show();
			else
				$('#vat_number').hide();
		}
	});
	
<?php }?>
</script>
<br />
<?php $_smarty_tpl->tpl_vars['current_step'] = new Smarty_variable('login', null, 0);?>


<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php $_smarty_tpl->tpl_vars['stateExist'] = new Smarty_variable(false, null, 0);?>

<?php if ($_smarty_tpl->tpl_vars['back']->value=='order.php') {?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./order-steps.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="shopping_cart_table" id="authentication_form">
<?php } else { ?>
<div id="authentication_form" style="display:flex; flex-direction:column; justify-content: space-around; ">
<?php }?>
<?php if (!isset($_smarty_tpl->tpl_vars['email_create']->value)) {?>
<div class="row" style="display:flex; margin-left:10px;">
	<div class="row" style="display:flex; flex-direction: column;" id="left-col-login">
		<div class="block" style="width:100%;">
			<form id="form-auth-padd" action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('authentication.php',true);?>
<?php if ($_GET['back']) {?>?back=<?php echo $_GET['back'];?>
<?php }?><?php if ($_GET['cliente']=='rivenditore') {?>?cliente=rivenditore<?php } else { ?><?php }?>" method="post" class="std">
			
				<fieldset>
					<h3><?php echo smartyTranslate(array('s'=>'Already registered ?'),$_smarty_tpl);?>
<br />
					<span style="color:#009; font-size:0.8em">Accedi con il tuo account</span></h3>
					<table class='authentication' style=" width: 100%; ">
					<tr>
					
						<td><input type="text" placeholder="Inserisci la tua e-mail" id="email" name="email" style=" background:transparent" value="<?php if (isset($_POST['email'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['email'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php }?>" class="account_input" /></td></tr>
					<tr>
					
						<td><input type="password" placeholder="Inserisci la tua password" id="passwd" name="passwd" style=" background:transparent" value="<?php if (isset($_POST['passwd'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['passwd'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php }?>" class="account_input" /></td>
					</tr>
					<tr>
					<td colspan="2" style=" width:100%;">
					<br />
						<?php if (isset($_smarty_tpl->tpl_vars['back']->value)) {?><input type="hidden" class="hidden" name="back" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['back']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" /><?php }?>
						<input type="submit" style=" font-weight:400; background-color: #ff6600;" id="SubmitLogin" name="SubmitLogin" class="button_large" value="<?php echo smartyTranslate(array('s'=>'Log in'),$_smarty_tpl);?>
" />
					</td></tr>
					</table>
					<p class="lost_password_2" style="padding: 3px 0;" id="lost-password"><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('password.php');?>
"><?php echo smartyTranslate(array('s'=>'Forgot your password?'),$_smarty_tpl);?>
</a></p>
				</fieldset>
			</form>
		</div>

		<div class="block" style="width:100%;">
			<form action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('authentication.php',true);?>
<?php if ($_GET['back']) {?>?back=<?php echo $_GET['back'];?>
<?php }?><?php if ($_GET['cliente']=='rivenditore') {?>?cliente=rivenditore<?php } else { ?><?php }?>" method="post" class="std">
			
				<fieldset>
					<h3><?php echo smartyTranslate(array('s'=>'Crea il tuo account rivenditore'),$_smarty_tpl);?>
<br />
					<span style="color:#009; font-size:0.8em">Registrati come nostro rivenditore</span></h3>
					<table class='authentication' style="margin-left: 10px;">
					<tr>
					<p style="padding:10px"><strong><?php if ($_GET['cliente']=='rivenditore') {?><?php echo smartyTranslate(array('s'=>'Partners and resellers area'),$_smarty_tpl);?>
.<?php } else { ?><?php echo smartyTranslate(array('s'=>'Enter your e-mail address to create an account'),$_smarty_tpl);?>
. <?php }?></strong></p>
			<span style="margin-left: 10px;"><input type="text" id="email_create" name="email_create" style="margin-top:-10px;" value="<?php if (isset($_POST['email_create'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['email_create'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php }?>" class="account_input" /></span>
				
				<?php if (isset($_smarty_tpl->tpl_vars['back']->value)) {?><input type="hidden" class="hidden" name="back" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['back']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" /><?php }?>
				<div style="clear:both"></div>
					<input type="submit" id="SubmitCreate" name="SubmitCreate" class="button_large" style=" margin-top:28px; background-color: #00c; margin-left: 10px; font-weight:400;" value="<?php echo smartyTranslate(array('s'=>'Create your account'),$_smarty_tpl);?>
" />
					<input type="hidden" class="hidden" name="SubmitCreate" value="<?php echo smartyTranslate(array('s'=>'Create your account'),$_smarty_tpl);?>
" />
				</p>
				<?php if ($_GET['cliente']=='rivenditore') {?>
				<p style="padding:10px"><strong>
				<?php echo smartyTranslate(array('s'=>'In the next step you will receive the instructions for the reserved area'),$_smarty_tpl);?>
.</strong></p><?php } else { ?><?php }?>
					</table>
					
				</fieldset>
			</form>
		</div>
	</div>
	<div class="block" style="margin-left:20px; width: 50%;">
		<form action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('authentication.php',true);?>
<?php if ($_GET['back']) {?>?back=<?php echo $_GET['back'];?>
<?php }?><?php if ($_GET['cliente']=='rivenditore') {?>?cliente=rivenditore<?php } else { ?><?php }?>" method="post" class="std">
			<fieldset>
			
				<h3><?php echo smartyTranslate(array('s'=>'Create your account'),$_smarty_tpl);?>
<br />
				<span style="color:#009; font-size:0.8em">Registrati come nostro cliente</span></h3>
				
				<p style="padding:10px">
				<strong style="font-size:14px">Perché scegliere noi</strong>

				<ul id="account_list" style="margin-left:10px">
				<li>&#10004; Oltre 70.000 clienti soddisfatti</li>
				<li>&#10004; 30 anni di esperienza nel settore</li>
				<li>&#10004; Consulenza prevendita</li>
				<li>&#10004; Assistenza post vendita</li>
				<li>&#10004; Consulente dedicato</li>
				<li>&#10004; Supporto all'installazione</li>
				<li>&#10004; Installazione On Site</li>
				<li>&#10004; Soddisfatti o rimborsati al 100% su tutti i nostri preventivi</li>
				</ul>
								
				</p>
				
				
				<p style="padding:10px"><strong><?php if ($_GET['cliente']=='rivenditore') {?><?php echo smartyTranslate(array('s'=>'Partners and resellers area'),$_smarty_tpl);?>
.<?php } else { ?><?php echo smartyTranslate(array('s'=>'Enter your e-mail address to create an account'),$_smarty_tpl);?>
. <?php }?></strong></p>
			<span id="margin-left"><input type="text" id="email_create" name="email_create" style="margin-top:-10px; " value="<?php if (isset($_POST['email_create'])) {?><?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_POST['email_create'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php }?>" class="account_input" /></span>
				
				<?php if (isset($_smarty_tpl->tpl_vars['back']->value)) {?><input type="hidden" class="hidden" name="back" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['back']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" /><?php }?>
				<div style="clear:both"></div>
					<input type="submit" id="SubmitCreate" name="SubmitCreate" class="button_large"  style=" margin-top:28px; background-color: #00c;  font-weight:400; "  value="<?php echo smartyTranslate(array('s'=>'Create your account'),$_smarty_tpl);?>
" />
					<input type="hidden" class="hidden" name="SubmitCreate" value="<?php echo smartyTranslate(array('s'=>'Create your account'),$_smarty_tpl);?>
" />
				</p>
				<?php if ($_GET['cliente']=='rivenditore') {?>
				<p style="padding:10px"><strong>
				<?php echo smartyTranslate(array('s'=>'In the next step you will receive the instructions for the reserved area'),$_smarty_tpl);?>
.</strong></p><?php } else { ?><?php }?>
			</fieldset>
		</form>
	</div>
</div>
	
		<?php if ($_smarty_tpl->tpl_vars['back']->value=='order.php') {?>
</div>
<?php }?>
	<?php if (isset($_smarty_tpl->tpl_vars['inOrderProcess']->value)&&$_smarty_tpl->tpl_vars['inOrderProcess']->value&&$_smarty_tpl->tpl_vars['PS_GUEST_CHECKOUT_ENABLED']->value) {?>
	<?php }?>
<?php } else { ?>
<form action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('authentication.php',true);?>
<?php if ($_GET['cliente']=='rivenditore') {?>?cliente=rivenditore<?php } else { ?><?php }?>" method="post" id="account-creation_form" class="std">
	<?php echo $_smarty_tpl->tpl_vars['HOOK_CREATE_ACCOUNT_TOP']->value;?>

	<fieldset style="width: 100%" class="account_creation">
	<h3><?php echo smartyTranslate(array('s'=>'Your profile'),$_smarty_tpl);?>
</h3>
	<p class="required text" style="text-align:center">
	<?php if ($_GET['cliente']=='rivenditore') {?>
<input type="hidden" name="is_company" value="1" id="company">

<?php } else { ?>
	
	
	<table cellspacing="30" style='text-align:center; width: 50%; margin:0 auto; vertical-align:top; border:0px'>
	<tr style="text-align:center; " id="auth-form">
		<td id="company-box" style='width: 200px; border-radius: 12px; background-color: #f4f4f4; padding: 20px; text-align: center; vertical-align: top; font-size: 18px;'>
<span style="color: #000099; font-weight: bold;"><?php echo smartyTranslate(array('s'=>'Company'),$_smarty_tpl);?>
</span><br />


<input type="radio" name="is_company" value="1" id="company"
<?php if (isset($_POST['is_company1'])&&($_POST['is_company1']=='1')) {?>
checked="checked" 
<?php } else { ?>

<?php }?>
<?php if ($_GET['cliente']=='rivenditore') {?>
checked="checked"<?php } else { ?>
<?php }?>
 onclick="profileChange(1);profileChange2(1)"><br />
<span style="font-size:12px"><?php echo smartyTranslate(array('s'=>'Enti, aziende, professionisti, pubblica amministrazione, possessori di partita I.V.A.'),$_smarty_tpl);?>
</span>
</td>

<?php if ($_GET['cliente']=='rivenditore') {?><?php } else { ?>
	<td style='width: 200px; border-radius: 12px; background-color: #f4f4f4; padding: 20px; text-align: center; vertical-align: top; font-size: 18px;'>
	<span style="color: #ff6600; font-weight: bold;"><?php echo smartyTranslate(array('s'=>'End user'),$_smarty_tpl);?>
</span><br />
<input type='radio' name='is_company' value='0' id='private'
<?php if (isset($_POST['is_company1'])&&($_POST['is_company1']=='0')) {?>
checked='checked' 
<?php } else { ?>
<?php }?>
onclick='profileChange(0);profileChange2(0)'><br />
<span style='font-size:12px'><?php echo smartyTranslate(array('s'=>'End users with a tax code.'),$_smarty_tpl);?>
</span><br />
	</td><?php }?>

</tr>
</table>
<?php }?>
</p>

<?php if ($_GET['cliente']=='rivenditore') {?><br /><br />
<p style='font-size:12px; margin-top:-40px; margin-left:20px; margin-right:20px; width:80%; text-align:left'>
Area riservata  Partner - Rivenditori<br /><br />
<strong>Per poter accedere al listino riservato ai partners e rivenditori:</strong>
<br /><br />
1. Compila il modulo con i dati anagrafici richiesti per la registrazione come utente.<br /><br />
2. Invia una visura camerale utilizzando l'apposito modulo che trovi <a href='<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('contact-form.php',true);?>
?step=rivenditori' target='_blank'>cliccando su questo link</a>. Allega la tua visura camerale selezionando il file nel campo "Allega file".<br /><br />
3. Riceverai un messaggio di posta elettronica per abilitarti con profilo rivenditore (salvo verifica della visura, che dovr&agrave; espressamente indicare le categorie merceologiche e /o l'attivit&agrave; sociale compatibile con l'attivit&agrave; di impiantistica, installazione, rivendita di sistemi, software, e prodotti informatici, TLC, forniture per ufficio etc).<br /><br />

Grazie per la tua cortese collaborazione.
<br /><br />
Staff Ezdirect.it</p><?php } else { ?><?php }?>


	</fieldset>
<div id="company_data2" name="company_data2" style="
<?php if (isset($_POST['is_company1'])&&($_POST['is_company1']==0)) {?>
<?php echo 'display:inline';?>

<?php } elseif (isset($_POST['is_company1'])&&($_POST['is_company1']=='1')) {?>
<?php echo 'display:none';?>

<?php } elseif (!isset($_POST['is_company1'])) {?>
<?php echo 'display:none';?>

<?php } else { ?>
<?php echo 'display:none';?>

<?php }?>




">
	<form action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('authentication.php',true);?>
<?php if ($_GET['cliente']=='rivenditore') {?>?cliente=rivenditore<?php } else { ?><?php }?>" method="post" id="account-creation_form" class="std">
	<fieldset  class="account_creation">
		<h3><?php echo smartyTranslate(array('s'=>'Your personal information'),$_smarty_tpl);?>
</h3> <span style="margin-left:10px;"><sup style="color:red;">*</sup><?php echo smartyTranslate(array('s'=>'Required field'),$_smarty_tpl);?>
</span>

		<p class="required text">
			<label for="customer_firstname"><?php echo smartyTranslate(array('s'=>'First name'),$_smarty_tpl);?>
</label>
			<input onkeyup="$('#firstname').val(this.value);" type="text" class="text" id="customer_firstname" name="customer_firstname" value="<?php if (isset($_POST['customer_firstname'])) {?><?php echo $_POST['customer_firstname'];?>
<?php }?>" />
			<sup>*</sup>
		</p>
		<p class="required text">
			<label for="customer_lastname"><?php echo smartyTranslate(array('s'=>'Last name'),$_smarty_tpl);?>
</label>
			<input onkeyup="$('#lastname').val(this.value);" type="text" class="text" id="customer_lastname" name="customer_lastname" value="<?php if (isset($_POST['customer_lastname'])) {?><?php echo $_POST['customer_lastname'];?>
<?php }?>" />
			<sup>*</sup>
		</p>
		<p class="required text">
			<label for="email"><?php echo smartyTranslate(array('s'=>'E-mail'),$_smarty_tpl);?>
</label>
			<input type="text" class="text" id="email" name="email" value="<?php if (isset($_POST['email'])) {?><?php echo $_POST['email'];?>
<?php }?>" />
			<sup>*</sup>
		</p>
		
		<p class="required password">
			<label for="passwd"><?php echo smartyTranslate(array('s'=>'Password'),$_smarty_tpl);?>
</label>
			<input type="password" class="text" name="passwd" id="passwd" />
			<sup>*</sup>
			<span class="form_info"><?php echo smartyTranslate(array('s'=>'(5 characters min.)'),$_smarty_tpl);?>
</span>
		</p>
		<p class="required password">
<label for="passwd_confirm"><?php echo smartyTranslate(array('s'=>'Confirm password'),$_smarty_tpl);?>
</label>
<input type="password" class="text" name="passwd_confirm" id="passwd_confirm" />
<sup>*</sup>
</p>
<script type="text/javascript">
$(function(){
$("#passwd_confirm").parents("form").submit(function(){
if ($('#passwd').attr('value') == $('#passwd_confirm').attr('value'))
return true;
alert("<?php echo smartyTranslate(array('s'=>'Yours both passwords filled in do not match.'),$_smarty_tpl);?>
");
return false;
});
});
</script> 
<p class="required text">
						<label for="tax_code"><?php echo smartyTranslate(array('s'=>'Tax code'),$_smarty_tpl);?>
</label>
						<input style="width: 17em;" type="text" class="no_spaces" name="tax_code" value="<?php if (isset($_POST['tax_code'])) {?><?php echo $_POST['tax_code'];?>
<?php }?>" />
						
						 (<?php echo smartyTranslate(array('s'=>'Insert a 16 characters tax code'),$_smarty_tpl);?>
)
					</p>
				<p class="required text">
						<label for="codice_univoco"><?php echo smartyTranslate(array('s'=>'SDI'),$_smarty_tpl);?>
</label>
						<input type="text" class="text" id="codice_univoco" name="codice_univoco" value="<?php if (isset($_POST['codice_univoco'])) {?><?php echo $_POST['codice_univoco'];?>
<?php }?>" />
					</p>
					<p class="required text">
						<label for="pec"><?php echo smartyTranslate(array('s'=>'PEC'),$_smarty_tpl);?>
</label>
						<input type="text" class="text" id="pec" name="pec" value="<?php if (isset($_POST['pec'])) {?><?php echo $_POST['pec'];?>
<?php }?>" />
					</p>	
		<p class="select">
			<span style="display: block; padding-bottom: 5px;"><?php echo smartyTranslate(array('s'=>'Date of Birth'),$_smarty_tpl);?>
</span>
			<select id="days" name="days">
				<option value="">-</option>
				<?php  $_smarty_tpl->tpl_vars['day'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['day']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['days']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['day']->key => $_smarty_tpl->tpl_vars['day']->value) {
$_smarty_tpl->tpl_vars['day']->_loop = true;
?>
					<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['day']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php if (($_smarty_tpl->tpl_vars['sl_day']->value==$_smarty_tpl->tpl_vars['day']->value)) {?> selected="selected"<?php }?>><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['day']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
&nbsp;&nbsp;</option>
				<?php } ?>
			</select>
			
			<select id="months" name="months">
				<option value="">-</option>
				<?php  $_smarty_tpl->tpl_vars['month'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['month']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['months']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['month']->key => $_smarty_tpl->tpl_vars['month']->value) {
$_smarty_tpl->tpl_vars['month']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['month']->key;
?>
					<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['k']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php if (($_smarty_tpl->tpl_vars['sl_month']->value==$_smarty_tpl->tpl_vars['k']->value)) {?> selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>((string)$_smarty_tpl->tpl_vars['month']->value)),$_smarty_tpl);?>
&nbsp;</option>
				<?php } ?>
			</select>
			<select id="years" name="years">
				<option value="">-</option>
				<?php  $_smarty_tpl->tpl_vars['year'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['year']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['years']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['year']->key => $_smarty_tpl->tpl_vars['year']->value) {
$_smarty_tpl->tpl_vars['year']->_loop = true;
?>
					<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['year']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php if (($_smarty_tpl->tpl_vars['sl_year']->value==$_smarty_tpl->tpl_vars['year']->value)) {?> selected="selected"<?php }?>><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['year']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
&nbsp;&nbsp;</option>
				<?php } ?>
			</select>
		</p>
		
		
	</fieldset>
	<fieldset  class="account_creation">
		<h3><?php echo smartyTranslate(array('s'=>'Your address'),$_smarty_tpl);?>
</h3>
			
				
					<input type="hidden" class="text" id="firstname" name="firstname" value="<?php if (isset($_POST['firstname'])) {?><?php echo $_POST['firstname'];?>
<?php }?>" />
			
					<input type="hidden" class="text" id="lastname" name="lastname" value="<?php if (isset($_POST['lastname'])) {?><?php echo $_POST['lastname'];?>
<?php }?>" />
				
				<p class="required select">
					<label for="id_country"><?php echo smartyTranslate(array('s'=>'Country'),$_smarty_tpl);?>
</label>
					<select style="width: 17em;" name="id_country" id="id_country" onchange="countryChange()";>
						<option value="">-</option>
						<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
						<option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_country'];?>
" <?php if (($_smarty_tpl->tpl_vars['sl_country']->value==$_smarty_tpl->tpl_vars['v']->value['id_country'])) {?> selected="selected"<?php }?>><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['v']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
						<?php } ?>
					</select>
					<sup>*</sup>
				</p>
			<script type="text/javascript">
				countryChange();
			</script>
			<div class="ee_address">
				<p class="required postcode text">
					<label for="postcode"><?php echo smartyTranslate(array('s'=>'Zip / Postal Code'),$_smarty_tpl);?>
</label>
					<input type="text" class="text" name="postcode" id="postcode" value="<?php if (isset($_POST['postcode'])) {?><?php echo $_POST['postcode'];?>
<?php }?>" onkeyup="$('#postcode').val($('#postcode').val().toUpperCase());" />
					<sup>*</sup>
				</p>
				<p class="required text">
					<label for="city"><?php echo smartyTranslate(array('s'=>'City'),$_smarty_tpl);?>
</label>
					<input type="text" class="text" name="city" id="city" value="<?php if (isset($_POST['city'])) {?><?php echo $_POST['city'];?>
<?php }?>" />
					<sup>*</sup>
				</p>
				<!--
					if customer hasn't update his layout address, country has to be verified
					but it's deprecated
				-->
				<?php $_smarty_tpl->tpl_vars['stateExist'] = new Smarty_variable(true, null, 0);?>
				<p class="required id_state select">
					<label for="id_state"><?php echo smartyTranslate(array('s'=>'State'),$_smarty_tpl);?>
</label>
					<select name="id_state" id="id_state">
						<option value="">-</option>
					</select>
					<sup>*</sup>
				</p>
			</div>
			<div class="it_address">
				<p class="required postcode text">
					<label for="cap"><?php echo smartyTranslate(array('s'=>'Zip / Postal Code'),$_smarty_tpl);?>
</label>
					<input type="text" class="text" name="postcode" id="cap" value="<?php if (isset($_POST['postcode'])) {?><?php echo $_POST['postcode'];?>
<?php }?>" onblur="$('#cap').val($('#cap').val().toUpperCase());" onkeyup="cercaCitta();">
					<sup>*</sup>
				</p>
			
				<p class="required text">
					<label for="citta"><?php echo smartyTranslate(array('s'=>'City'),$_smarty_tpl);?>
</label>
					<select style="width: 17em;" name='city' id='citta' onchangeonclick='cercaProvincia();'>
						<option name=''>-- Prima selezionare CAP --</option>
						<?php if (isset($_POST['city'])) {?>
							<option name='<?php echo $_POST['city'];?>
' selected='selected'><?php echo $_POST['city'];?>
</option>
						<?php } else { ?>
							<?php if (isset($_smarty_tpl->tpl_vars['address']->value->city)) {?>
								<option name='<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['address']->value->city, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
' selected='selected'><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['address']->value->city, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
							<?php }?>
						<?php }?>
						
					</select><sup style="padding-left:4px;">*</sup>
				</p>
					<!--
						if customer hasn't update his layout address, country has to be verified
						but it's deprecated
					-->
				<?php $_smarty_tpl->tpl_vars['stateExist'] = new Smarty_variable(true, null, 0);?>

				<p class="required id_state select">
					<label for="provincia"><?php echo smartyTranslate(array('s'=>'State'),$_smarty_tpl);?>
</label>
					<input style="width: 17em;" id="provincia" name="provincia_mark" readonly="readonly" type="text" value="<?php if (isset($_POST['provincia_mark'])) {?><?php echo $_POST['provincia_mark'];?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['this_state']->value;?>
<?php }?>" />
					<input id="provincia_hidden" name="id_state" type="hidden" value="<?php if (isset($_POST['id_state'])) {?><?php echo $_POST['id_state'];?>
<?php } else { ?><?php if (isset($_smarty_tpl->tpl_vars['address']->value->id_state)) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['address']->value->id_state, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?><?php }?>" />
					<sup>*</sup>
	
				</p>
				
			</div>
		<?php if ($_smarty_tpl->tpl_vars['stateExist']->value==false) {?>
			<p class="required id_state select">
				<label for="id_state"><?php echo smartyTranslate(array('s'=>'State'),$_smarty_tpl);?>
</label>
				<select name="id_state" id="id_state">
					<option value="">-</option>
				</select>
				<sup>*</sup>
			</p>
		<?php }?>
		<p class="required text">
		<script type="text/javascript">
		function suggerisciIndirizzo(indirizzo, cap, citta, campo) {
			$.ajax({
				  url:"cap.php?suggerisciIndirizzo=y",
				  type: "POST",
				  data: { 
				  indirizzo: indirizzo,
				  cap: cap,
				  citta: citta,
				  campo: campo
				  },
				  success:function(r){
					console.log(r);
					$("#address1_consigli").html(r)
				  },
				  error: function(xhr,stato,errori){
					
				  }
			});
		
		}
		</script>
			<label for="address1"><?php echo smartyTranslate(array('s'=>'Address'),$_smarty_tpl);?>
</label>
			<input type="text" class="text" name="address1" id="address1" value="<?php if (isset($_POST['address1'])) {?><?php echo $_POST['address1'];?>
<?php }?>"  />
			<sup>*</sup>
			
			<div id="address1_consigli">
			
			</div>
		</p>
		<p style="text-align:center;"><?php echo smartyTranslate(array('s'=>'You must register at least one phone number'),$_smarty_tpl);?>
 </p>
		<p class="required text">
			<label for="phone"><?php echo smartyTranslate(array('s'=>'Home phone'),$_smarty_tpl);?>
</label>
			<input type="text" class="text" onkeyup="this.value = this.value.replace(/\s/g,'');" name="phone" id="phone" value="<?php if (isset($_POST['phone'])) {?><?php echo $_POST['phone'];?>
<?php }?>" />
		</p>
		<p class="required text">
			<label for="phone_mobile"><?php echo smartyTranslate(array('s'=>'Mobile phone'),$_smarty_tpl);?>
</label>
			<input type="text" class="text" onkeyup="this.value = this.value.replace(/\s/g,'');" name="phone_mobile" id="phone_mobile" value="<?php if (isset($_POST['phone_mobile'])) {?><?php echo $_POST['phone_mobile'];?>
<?php }?>" />
		</p>
			<p class=" required text">
			<label for="fax"><?php echo smartyTranslate(array('s'=>'Fax number'),$_smarty_tpl);?>
</label>
			<input type="text" class="text" name="fax" id="fax" value="<?php if (isset($_POST['fax'])) {?><?php echo $_POST['fax'];?>
<?php }?>" />
		</p>
		<p class="required text" id="address_alias">
			<label for="alias"><?php echo smartyTranslate(array('s'=>'Assign an address title for future reference'),$_smarty_tpl);?>
 </label>
			<input type='hidden' id='fatturazione' name='fatturazione' value='1' />
			<input type="text" class="text" name="alias" id="alias" value="<?php if (isset($_POST['alias'])) {?><?php echo $_POST['alias'];?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'My address'),$_smarty_tpl);?>
<?php }?>" />
			<sup>*</sup>
		</p>
	
</fieldset>
		<br /><br />
		<p class=" text"  style="text-align: center;">
			
			<label for="newsletter"><?php echo smartyTranslate(array('s'=>'Sign up for our newsletter'),$_smarty_tpl);?>
</label>
			<input type="checkbox" name="newsletter" id="newsletter" value="1"  />
		</p>
		<p style="text-align:center; display:block; width:100%">
		Ho letto e compreso la Vostra <a href='https://www.ezdirect.it/guide/6-informativa-sulla-privacy' target='_blank'>Privacy Policy</a> e:
		<br /><br />
			
			<input type="radio" name="consenso_1" id="consenso11" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
			<input type="radio" name="consenso_1" id="consenso10" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
			
			<br /><br />
			al trattamento dei dati personali per l’esecuzione dei servizi forniti tramite il Sito da parte di Ezdirect S.r.l., o ad una o più obbligazioni contrattualmente convenute, ai sensi dell’art. 1, comma 1, lettera a), b) e c) della citata informativa (consenso obbligatorio; la mancata prestazione comporterà l’impossibilità di fruire dei servizi offerti, di effettuare l’iscrizione, e di effettuare acquisti tramite il Sito).
			<br /><br />
			
			<input type="radio" name="consenso_2"  id="consenso21" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
			<input type="radio" name="consenso_2"  id="consenso20" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
			
			<br /><br />
			al trattamento dei dati personali per la realizzazione, da parte di Ezdirect S.r.l., di indagini dirette a verificare il grado di soddisfazione sui servizi offerti, ai sensi dell’art. 1, comma 2, lettera a) della citata informativa (consenso facoltativo).

			<br /><br />
			
			<input type="radio" name="consenso_3"  id="consenso31" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
			<input type="radio" name="consenso_3"  id="consenso30" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
			
			<br /><br />
			al trattamento dei dati personali a fini di marketing e/o comunicazione commerciale da parte di Ezdirect S.r.l., connesse alle attività svolte da parte della stessa e/o da parte di soggetti terzi, ai sensi dell’art. 1, comma 2, lettera b), della citata informativa (consenso facoltativo).
			<br /><br />
			

		</p>
		

				<input type='hidden' id='is_company1' name='is_company1' value='0' />
	

<?php echo $_smarty_tpl->tpl_vars['HOOK_CREATE_ACCOUNT_FORM']->value;?>


	<p style="text-align:center;">
		<input type="hidden" name="email_create" value="1" />
		<input type="hidden" name="is_new_customer" value="1" />
		<?php if (isset($_smarty_tpl->tpl_vars['back']->value)) {?>
			<?php if ($_smarty_tpl->tpl_vars['back']->value=='order.php') {?>
				<input type="hidden" class="hidden" name="back" value="thank-you.php?ordine=si" />
			<?php } else { ?>
				<input type="hidden" class="hidden" name="back" value="thank-you.php" />
			<?php }?>
		<?php }?>
		
		<!-- <script src="https://www.google.com/recaptcha/api.js" async defer></script> -->
		
		<input type="submit" name="submitAccount" id="submitAccount" value="<?php echo smartyTranslate(array('s'=>'Register'),$_smarty_tpl);?>
" class="button_large" style="padding: 6px 0;"/>
		
							
		<div class="text-xs-center">
			<!-- <div class="g-recaptcha" data-sitekey="6LciANsZAAAAALhFLBBxrvJJICdKqsPVxXHfPh2i"></div> -->
		</div>
		<br /><br />
		
		
	</p>
	

	
</form>

</div>

<!-- COMPANY ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->




<div id="company_data" name="company_data" style="
<?php if (isset($_POST['is_company1'])&&($_POST['is_company1']==0)) {?>
<?php echo 'display:none';?>

<?php } elseif (isset($_POST['is_company1'])&&($_POST['is_company1']=='1')) {?>
<?php echo 'display:inline';?>

<?php } elseif (!isset($_POST['is_company1'])) {?>
<?php echo 'display:none';?>

<?php } else { ?>
<?php echo 'display:none';?>

<?php }?>
<?php if ($_GET['cliente']=='rivenditore') {?>display:inline<?php } else { ?><?php }?>

">
<form action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('authentication.php',true);?>
<?php if ($_GET['cliente']=='rivenditore') {?>?cliente=rivenditore<?php } else { ?><?php }?>" method="post" id="account-creation_form_company" class="std">


	<fieldset  class="account_creation">
		<h3><?php echo smartyTranslate(array('s'=>'Company\'s data'),$_smarty_tpl);?>
</h3><span style="margin-left:10px"><sup style="color:red;">*</sup><?php echo smartyTranslate(array('s'=>'Required field'),$_smarty_tpl);?>
</span>	
		<?php if ($_GET['cliente']=='rivenditore') {?><input type='hidden' value='1' name='rivenditore' id='rivenditore' />
		<input type="hidden" class="hidden" name="back" value="thank-you.php?cliente=rivenditore" />
		<?php } else { ?><?php }?>

		<p class="required text">
			<label for="customer_firstname2"><?php echo smartyTranslate(array('s'=>'Representative\'s first name'),$_smarty_tpl);?>
</label>
			<input onkeyup="$('#firstname2').val(this.value);" type="text" class="text" id="customer_firstname2" name="customer_firstname" value="<?php if (isset($_POST['customer_firstname'])) {?><?php echo $_POST['customer_firstname'];?>
<?php }?>" />
			<input type='hidden' id='is_company1' name='is_company1' value='1' />
			<sup>*</sup>
		</p>
		<p class="required text">
			<label for="customer_lastname2"><?php echo smartyTranslate(array('s'=>'Representative\'s last name'),$_smarty_tpl);?>
</label>
			<input onkeyup="$('#lastname2').val(this.value);" type="text" class="text" id="customer_lastname2" name="customer_lastname" value="<?php if (isset($_POST['customer_lastname'])) {?><?php echo $_POST['customer_lastname'];?>
<?php }?>" />
			<sup>*</sup>
		</p>
		<p class="required text">
			<label for="ruolo2"><?php echo smartyTranslate(array('s'=>'Role'),$_smarty_tpl);?>
</label>
			<input onkeyup="$('#ruolo2').val(this.value);" type="text" class="text" id="ruolo2" name="ruolo" value="<?php if (isset($_POST['ruolo'])) {?><?php echo $_POST['ruolo'];?>
<?php }?>" />
			
		</p>
		
				<p class="required text">
			<label for="customer_company"><?php echo smartyTranslate(array('s'=>'Company'),$_smarty_tpl);?>
</label>
			<input type="text" class="text" id="company" name="company" value="<?php if (isset($_POST['company'])) {?><?php echo $_POST['company'];?>
<?php }?>" />
			<sup>*</sup>
		</p>
<p class="required text">
						<label for="vat_number"><?php echo smartyTranslate(array('s'=>'VAT number'),$_smarty_tpl);?>
</label>
						<input style="width: 17em;" type="text" class="no_spaces" name="vat_number" value="<?php if (isset($_POST['vat_number'])) {?><?php echo $_POST['vat_number'];?>
<?php }?>" />
						
						(<?php echo smartyTranslate(array('s'=>'Insert a 11 characters vat number'),$_smarty_tpl);?>
)
					</p>
<p class="required text">
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['js_dir']->value;?>
jquery.tooltip.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['js_dir']->value;?>
etc/tax_code_help.js">
</script>

						<label for="tax_code"><?php echo smartyTranslate(array('s'=>'Company tax code'),$_smarty_tpl);?>
</label>
						<input style="width: 17em;" type="text" class="no_spaces" id="tax_code" name="tax_code" value="<?php if (isset($_POST['tax_code'])) {?><?php echo $_POST['tax_code'];?>
<?php }?>" />
						<sup><span id="tax_code_help" style="cursor:pointer; position:absolute; right:90px" title="Se operi per conto di una societ&agrave; di capitali (esempio S.R.L o S.P.A.) il C.F. da inserire &egrave; sempre quello aziendale, quindi non devi usare il C.F. personale. Se operi per conto di una societ&agrave; di persone o ditta individuale, il C.F. necessario per la registrazione, &egrave; comuqnue quello aziendale, che, in alcuni casi, potrebbe coincidere con quello personale (es. ditte individuali). Per avere supporto in fase di registrazione, contatta il nostro staff al numero 0585821163. Grazie per la cortese collaborazione." ><img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/help.gif" alt="<?php echo smartyTranslate(array('s'=>'Help'),$_smarty_tpl);?>
" /></span></sup>
						(<?php echo smartyTranslate(array('s'=>'Insert a 11 or 16 characters tax code'),$_smarty_tpl);?>
)
					</p>
					<p class="required text">
						<label for="employees_number"><?php echo smartyTranslate(array('s'=>'Employees number'),$_smarty_tpl);?>
</label>
						<select style="width: 17em;" name="employees_number">
						<option value="Da 1 a 10"><?php echo smartyTranslate(array('s'=>'1 to 10'),$_smarty_tpl);?>
</option>
						<option value="Da 11 a 50"><?php echo smartyTranslate(array('s'=>'11 to 50'),$_smarty_tpl);?>
</option>
						<option value="50 piu"><?php echo smartyTranslate(array('s'=>'More than 50'),$_smarty_tpl);?>
</option>
						</select>
						
						<sup>*</sup>
					</p>
		<p class="required text">
			<label for="email"><?php echo smartyTranslate(array('s'=>'E-mail'),$_smarty_tpl);?>
</label>
			<input type="text" class="text" id="email2" name="email" value="<?php if (isset($_POST['email'])) {?><?php echo $_POST['email'];?>
<?php }?>" />
			<sup>*</sup>
		</p>
		
		<p class="required password">
			<label for="passwd"><?php echo smartyTranslate(array('s'=>'Password'),$_smarty_tpl);?>
</label>
			<input type="password" class="text" name="passwd" id="passwd2" />
			<sup>*</sup>
			<span class="form_info"><?php echo smartyTranslate(array('s'=>'(5 characters min.)'),$_smarty_tpl);?>
</span>
		</p>
		<p class="required password">
<label for="passwd_confirm"><?php echo smartyTranslate(array('s'=>'Confirm password'),$_smarty_tpl);?>
</label>
<input type="password" class="text" name="passwd_confirm" id="passwd_confirm2" />
<sup>*</sup>
</p>
<script type="text/javascript">
$(function(){
$("#passwd_confirm").parents("form").submit(function(){
if ($('#passwd').attr('value') == $('#passwd_confirm').attr('value'))
return true;
alert("<?php echo smartyTranslate(array('s'=>'Yours both passwords filled in do not match.'),$_smarty_tpl);?>
");
return false;
});
});
</script> 
<p class="required text">
						<label for="codice_univoco"><?php echo smartyTranslate(array('s'=>'SDI'),$_smarty_tpl);?>
</label>
						<input type="text" class="text" id="codice_univoco" name="codice_univoco" value="<?php if (isset($_POST['codice_univoco'])) {?><?php echo $_POST['codice_univoco'];?>
<?php }?>" />
						<sup>**</sup>
					</p>
					<p class="required text">
						<label for="pec"><?php echo smartyTranslate(array('s'=>'PEC'),$_smarty_tpl);?>
</label>
						<input type="text" class="text" id="pec" name="pec" value="<?php if (isset($_POST['pec'])) {?><?php echo $_POST['pec'];?>
<?php }?>" />
						<sup>**</sup>
					</p>
					
		<p class="select">
			<span style="display: block; padding-bottom: 5px;"><?php echo smartyTranslate(array('s'=>'Representative\'s date of birth'),$_smarty_tpl);?>
</span>
			<select id="days2" name="days">
				<option value="">-</option>
				<?php  $_smarty_tpl->tpl_vars['day'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['day']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['days']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['day']->key => $_smarty_tpl->tpl_vars['day']->value) {
$_smarty_tpl->tpl_vars['day']->_loop = true;
?>
					<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['day']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php if (($_smarty_tpl->tpl_vars['sl_day']->value==$_smarty_tpl->tpl_vars['day']->value)) {?> selected="selected"<?php }?>><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['day']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
&nbsp;&nbsp;</option>
				<?php } ?>
			</select>
			
			<select id="months2" name="months">
				<option value="">-</option>
				<?php  $_smarty_tpl->tpl_vars['month'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['month']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['months']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['month']->key => $_smarty_tpl->tpl_vars['month']->value) {
$_smarty_tpl->tpl_vars['month']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['month']->key;
?>
					<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['k']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php if (($_smarty_tpl->tpl_vars['sl_month']->value==$_smarty_tpl->tpl_vars['k']->value)) {?> selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>((string)$_smarty_tpl->tpl_vars['month']->value)),$_smarty_tpl);?>
&nbsp;</option>
				<?php } ?>
			</select>
			<select id="years2" name="years">
				<option value="">-</option>
				<?php  $_smarty_tpl->tpl_vars['year'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['year']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['years']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['year']->key => $_smarty_tpl->tpl_vars['year']->value) {
$_smarty_tpl->tpl_vars['year']->_loop = true;
?>
					<option value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['year']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php if (($_smarty_tpl->tpl_vars['sl_year']->value==$_smarty_tpl->tpl_vars['year']->value)) {?> selected="selected"<?php }?>><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['year']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
&nbsp;&nbsp;</option>
				<?php } ?>
			</select>
		</p>
	
		<p class="text">
						<label for="fte_notes">**</label>
						<textarea readonly="readonly" id="fte_note" style="overflow:hidden; border:0px; background:#ffffff;  height:70px">Le aziende italiane sono tenute a inserire almeno o il codice SDI o la PEC. Non accettiamo comunicazioni del codice SDI via mail. Ti ricordiamo che in mancanza di tali dati, potrai collegarti al sito dell'Agenzia delle Entrate e scaricare la fattura.</textarea>
					</p>
		
	</fieldset>
	<fieldset  class="account_creation">
		<h3><?php echo smartyTranslate(array('s'=>'Company\'s address'),$_smarty_tpl);?>
</h3>
		<?php  $_smarty_tpl->tpl_vars['field_name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field_name']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['dlv_all_fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field_name']->key => $_smarty_tpl->tpl_vars['field_name']->value) {
$_smarty_tpl->tpl_vars['field_name']->_loop = true;
?>
		
				<?php if ($_smarty_tpl->tpl_vars['field_name']->value=="firstname") {?>
				
					<input type="hidden" class="text" id="firstname2" name="firstname" value="<?php if (isset($_POST['firstname'])) {?><?php echo $_POST['firstname'];?>
<?php }?>" />
				<?php }?>	
			<?php if ($_smarty_tpl->tpl_vars['field_name']->value=="lastname") {?>
			
					<input type="hidden" class="text" id="lastname2" name="lastname" value="<?php if (isset($_POST['lastname'])) {?><?php echo $_POST['lastname'];?>
<?php }?>" />
			<?php }?>		
			<?php if ($_smarty_tpl->tpl_vars['field_name']->value=="company") {?>
			
	
					<input type="hidden" class="text" id="company2" name="company2" value="<?php if (isset($_POST['company'])) {?><?php echo $_POST['company'];?>
<?php }?>" />
			<?php }?>
			
			<script type="text/javascript">
				countryChange2();
			</script>
			<?php if ($_smarty_tpl->tpl_vars['field_name']->value=="Country:name"||$_smarty_tpl->tpl_vars['field_name']->value=="country") {?>
				<p class="required select">
					<label for="id_country"><?php echo smartyTranslate(array('s'=>'Country'),$_smarty_tpl);?>
</label>
					<select style="width: 17em;" name="id_country" id="id_country2" onchange="countryChange2()";>
						<option value="">-</option>
						<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
						<option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_country'];?>
" <?php if (($_smarty_tpl->tpl_vars['sl_country']->value==$_smarty_tpl->tpl_vars['v']->value['id_country'])) {?> selected="selected"<?php }?>><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['v']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
						<?php } ?>
					</select>
					<sup>*</sup>
				</p>
			<?php }?>
			
			<div class="ee_address">
			<?php if ($_smarty_tpl->tpl_vars['field_name']->value=="postcode") {?>
				<p class="required postcode text">
					<label for="postcode"><?php echo smartyTranslate(array('s'=>'Zip / Postal Code'),$_smarty_tpl);?>
</label>
					<input type="text" class="text" name="postcode" id="postcode2" value="<?php if (isset($_POST['postcode'])) {?><?php echo $_POST['postcode'];?>
<?php }?>" onkeyup="$('#postcode').val($('#postcode').val().toUpperCase());" />
					<sup>*</sup>
				</p>
			<?php }?>	
						
			<?php if ($_smarty_tpl->tpl_vars['field_name']->value=="city") {?>
				<p class="required text">
					<label for="city"><?php echo smartyTranslate(array('s'=>'City'),$_smarty_tpl);?>
</label>
					<input type="text" class="text" name="city" id="city2" value="<?php if (isset($_POST['city'])) {?><?php echo $_POST['city'];?>
<?php }?>" />
					<sup>*</sup>
				</p>
				<!--
					if customer hasn't update his layout address, country has to be verified
					but it's deprecated
				-->
			<?php }?>
			
			<?php if ($_smarty_tpl->tpl_vars['field_name']->value=="State:name"||$_smarty_tpl->tpl_vars['field_name']->value=='state') {?>
				<?php $_smarty_tpl->tpl_vars['stateExist'] = new Smarty_variable(true, null, 0);?>
				<p class="required id_state select">
					<label for="id_state"><?php echo smartyTranslate(array('s'=>'State'),$_smarty_tpl);?>
</label>
					<select name="id_state" id="id_state2">
						<option value="">-</option>
					</select>
					<sup>*</sup>
				</p>
			<?php }?>
			</div>
			
			<div class="it_address">
				<?php if ($_smarty_tpl->tpl_vars['field_name']->value=="postcode") {?>
				<p class="required postcode text">
					<label for="cap2"><?php echo smartyTranslate(array('s'=>'Zip / Postal Code'),$_smarty_tpl);?>
</label>
					<input type="text" class="text" name="postcode" id="cap2" value="<?php if (isset($_POST['postcode'])) {?><?php echo $_POST['postcode'];?>
<?php }?>" onblur="$('#cap2').val($('#cap2').val().toUpperCase());" onkeyup="cercaCitta2();">
					<sup>*</sup>
				</p>
				<?php }?>
				
				<?php if ($_smarty_tpl->tpl_vars['field_name']->value=="city") {?>
				<p class="required text">
					<label for="citta2"><?php echo smartyTranslate(array('s'=>'City'),$_smarty_tpl);?>
</label>
					<select style="width: 17em;" name='city' id='citta2' onclick='cercaProvincia2();'>
						<option name=''>-- Prima selezionare CAP --</option>
						<?php if (isset($_POST['city'])) {?>
							<option name='<?php echo $_POST['city'];?>
' selected='selected'><?php echo $_POST['city'];?>
</option>
						<?php } else { ?>
							<?php if (isset($_smarty_tpl->tpl_vars['address']->value->city)) {?>
								<option name='<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['address']->value->city, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
' selected='selected'><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['address']->value->city, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
							<?php }?>
						<?php }?>
						
					</select><sup style="padding-left:4px;">*</sup>
				</p>
					<!--
						if customer hasn't update his layout address, country has to be verified
						but it's deprecated
					-->
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['field_name']->value=="State:name"||$_smarty_tpl->tpl_vars['field_name']->value=='state') {?>
				<?php $_smarty_tpl->tpl_vars['stateExist'] = new Smarty_variable(true, null, 0);?>

				<p class="required id_state select">
					<label for="provincia2"><?php echo smartyTranslate(array('s'=>'State'),$_smarty_tpl);?>
</label>
					<input style="width: 17em;" id="provincia2" name="provincia_mark" readonly="readonly" type="text" value="<?php if (isset($_POST['provincia_mark'])) {?><?php echo $_POST['provincia_mark'];?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['this_state']->value;?>
<?php }?>" />
					<input id="provincia_hidden2" name="id_state" type="hidden" value="<?php if (isset($_POST['id_state'])) {?><?php echo $_POST['id_state'];?>
<?php } else { ?><?php if (isset($_smarty_tpl->tpl_vars['address']->value->id_state)) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['address']->value->id_state, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?><?php }?>" />
					<sup>*</sup>
	
				</p>
				<?php }?>
			</div>
		<?php } ?>
		<?php if ($_smarty_tpl->tpl_vars['stateExist']->value==false) {?>
			<p class="required id_state select">
				<label for="id_state"><?php echo smartyTranslate(array('s'=>'State'),$_smarty_tpl);?>
</label>
				<select name="id_state" id="id_state2">
					<option value="">-</option>
				</select>
				<sup>*</sup>
			</p>
		<?php }?>
		<p class="required text">
		
		<script type="text/javascript">
		function suggerisciIndirizzo2(indirizzo, cap, citta, campo) {
			$.ajax({
				  url:"cap.php?suggerisciIndirizzo=y",
				  type: "POST",
				  data: { 
				  indirizzo: indirizzo,
				  cap: cap,
				  citta: citta,
				  campo: campo
				  },
				  success:function(r){
					console.log(r);
					$("#address12_consigli").html(r)
				  },
				  error: function(xhr,stato,errori){
					
				  }
			});
		
		}
		</script>
		
			<label for="address1"><?php echo smartyTranslate(array('s'=>'Address'),$_smarty_tpl);?>
</label>
			<input type="text" class="text" name="address1" id="address12" value="<?php if (isset($_POST['address1'])) {?><?php echo $_POST['address1'];?>
<?php }?>" />
			<sup>*</sup>
			<!-- <span class="inline-infos"><?php echo smartyTranslate(array('s'=>'Street address, P.O. box, company name, c/o'),$_smarty_tpl);?>
</span> -->
		</p>
		
			
			
			<div id="address12_consigli">
			
			</div>
	
		<p style="text-align:center;"><?php echo smartyTranslate(array('s'=>'You must register at least one phone number'),$_smarty_tpl);?>
 </p>
		<p class="required text">
			<label for="phone"><?php echo smartyTranslate(array('s'=>'Home phone'),$_smarty_tpl);?>
</label>
			<input type="text" class="text" name="phone" onkeyup="this.value = this.value.replace(/\s/g,'');" id="phone2" value="<?php if (isset($_POST['phone'])) {?><?php echo $_POST['phone'];?>
<?php }?>" />
		</p>
		<p class="required text">
			<label for="phone_mobile"><?php echo smartyTranslate(array('s'=>'Mobile phone'),$_smarty_tpl);?>
</label>
			<input type="text" class="text" name="phone_mobile" id="phone_mobile2" onkeyup="this.value = this.value.replace(/\s/g,'');" value="<?php if (isset($_POST['phone_mobile'])) {?><?php echo $_POST['phone_mobile'];?>
<?php }?>" />
		</p>
			<p class="required text">
			<label for="fax"><?php echo smartyTranslate(array('s'=>'Fax number'),$_smarty_tpl);?>
</label>
			<input type="text" class="text" name="fax" id="fax2" value="<?php if (isset($_POST['fax'])) {?><?php echo $_POST['fax'];?>
<?php }?>" />
		</p>
		<p class="required text" id="address_alias">
			<label for="alias"><?php echo smartyTranslate(array('s'=>'Assign an address title for future reference'),$_smarty_tpl);?>
 </label>
			<input type='hidden' id='fatturazione' name='fatturazione' value='1' />
			<input type="text" class="text" name="alias" id="alias2" value="<?php if (isset($_POST['alias'])) {?><?php echo $_POST['alias'];?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'My address'),$_smarty_tpl);?>
<?php }?>" />
			<sup>*</sup>
		</p>
			
	</fieldset>
		<br /><br />

		<p class="text" style="text-align: center;">
			
			<label for="newsletter"><?php echo smartyTranslate(array('s'=>'Sign up for our newsletter'),$_smarty_tpl);?>
</label>
			<input type="checkbox" name="newsletter" id="newsletter2" value="1"  />
		</p>
	
		<p style="text-align:center; display:block; width:100%">
		Ho letto e compreso la Vostra <a href='https://www.ezdirect.it/guide/6-informativa-sulla-privacy' target='_blank'>Privacy Policy</a> e:
		<br /><br />
			
			<input type="radio" name="consenso_1" id="consenso121" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
			<input type="radio" name="consenso_1" id="consenso120" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
			
			<br /><br />
			al trattamento dei dati personali per l’esecuzione dei servizi forniti tramite il Sito da parte di Ezdirect S.r.l., o ad una o più obbligazioni contrattualmente convenute, ai sensi dell’art. 1, comma 1, lettera a), b) e c) della citata informativa (consenso obbligatorio; la mancata prestazione comporterà l’impossibilità di fruire dei servizi offerti, di effettuare l’iscrizione, e di effettuare acquisti tramite il Sito).
			<br /><br />
			
			<input type="radio" name="consenso_2" id="consenso221" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
			<input type="radio" name="consenso_2" id="consenso220" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
			
			<br /><br />
			al trattamento dei dati personali per la realizzazione, da parte di Ezdirect S.r.l., di indagini dirette a verificare il grado di soddisfazione sui servizi offerti, ai sensi dell’art. 1, comma 2, lettera a) della citata informativa (consenso facoltativo).

			<br /><br />
			
			<input type="radio" name="consenso_3" id="consenso321" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
			<input type="radio" name="consenso_3" id="consenso320" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
			
			<br /><br />
			al trattamento dei dati personali a fini di marketing e/o comunicazione commerciale da parte di Ezdirect S.r.l., connesse alle attività svolte da parte della stessa e/o da parte di soggetti terzi, ai sensi dell’art. 1, comma 2, lettera b), della citata informativa (consenso facoltativo).
			<br /><br />
			

		</p>
		
<?php echo $_smarty_tpl->tpl_vars['HOOK_CREATE_ACCOUNT_FORM']->value;?>

	<p style="text-align:center;">
		<input type="hidden" name="email_create" value="1" />
		<input type="hidden" name="is_new_customer" value="1" />
		<?php if (isset($_smarty_tpl->tpl_vars['back']->value)) {?>
		
			<?php if ($_smarty_tpl->tpl_vars['back']->value=='order.php') {?>
				<input type="hidden" class="hidden" name="back" value="thank-you.php?ordine=si" />
			<?php } else { ?>
				<input type="hidden" class="hidden" name="back" value="thank-you.php" />
			<?php }?>
		
		<?php }?>
		
		
		<!-- <script src="https://www.google.com/recaptcha/api.js" async defer></script> -->
		
		<input type="submit" name="submitAccount" id="submitAccount2" value="<?php echo smartyTranslate(array('s'=>'Register'),$_smarty_tpl);?>
" class="button_large" style="padding: 6px 0;"/>
						
		
		<div class="text-xs-center">
			<!-- <div class="g-recaptcha" data-sitekey="6LciANsZAAAAALhFLBBxrvJJICdKqsPVxXHfPh2i"></div> -->
		</div>
		<br /><br />
		
	</p>
	
</form>
</div>

<?php }?>

<?php }} ?>
