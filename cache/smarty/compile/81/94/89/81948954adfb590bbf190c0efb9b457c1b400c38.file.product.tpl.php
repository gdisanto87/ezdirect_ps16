<?php /* Smarty version Smarty-3.1.19, created on 2022-01-07 19:09:10
         compiled from "/var/www/html/modules/loyalty/product.tpl" */ ?>
<?php /*%%SmartyHeaderCode:198600687661d881c65d4ec9-89463069%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '81948954adfb590bbf190c0efb9b457c1b400c38' => 
    array (
      0 => '/var/www/html/modules/loyalty/product.tpl',
      1 => 1638264468,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '198600687661d881c65d4ec9-89463069',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'js_dir' => 0,
    'points' => 0,
    'no_pts_discounted' => 0,
    'module_template_dir' => 0,
    'voucher' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61d881c65e01b4_86414191',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61d881c65e01b4_86414191')) {function content_61d881c65e01b4_86414191($_smarty_tpl) {?>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['js_dir']->value;?>
jquery.tooltip.js"></script>
<script type="text/javascript">
$(function() {
$('#loyalty-icon, #span-punti').tooltip({ showURL: false  });
});
</script>
<div id="loyalty-div">
<span class="span-punti" id="span-punti" title="<?php if ($_smarty_tpl->tpl_vars['points']->value) {?>
		<?php echo smartyTranslate(array('s'=>'By buying this product you can collect up to','mod'=>'loyalty'),$_smarty_tpl);?>
 <?php echo $_smarty_tpl->tpl_vars['points']->value;?>
 
		<?php if ($_smarty_tpl->tpl_vars['points']->value>1) {?><?php echo smartyTranslate(array('s'=>'loyalty points','mod'=>'loyalty'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'loyalty point','mod'=>'loyalty'),$_smarty_tpl);?>
<?php }?> <?php echo smartyTranslate(array('s'=>'You will be able to convert your points when they will be greather than 5 euros, by clicking on "My account", then on "My loyalty points"','mod'=>'loyalty'),$_smarty_tpl);?>
.
	<?php } else { ?>
		<?php if (isset($_smarty_tpl->tpl_vars['no_pts_discounted']->value)&&$_smarty_tpl->tpl_vars['no_pts_discounted']->value==1) {?>
			<?php echo smartyTranslate(array('s'=>'No reward points for this product because there\'s already a discount.','mod'=>'loyalty'),$_smarty_tpl);?>

		<?php } else { ?>
			<?php echo smartyTranslate(array('s'=>'No reward points for this product.','mod'=>'loyalty'),$_smarty_tpl);?>

		<?php }?>
	<?php }?>">
<?php echo $_smarty_tpl->tpl_vars['points']->value;?>
<br /><span class="punti-scritta"> <?php echo smartyTranslate(array('s'=>'points','mod'=>'loyalty'),$_smarty_tpl);?>
</span></span> 

	<img src="<?php echo $_smarty_tpl->tpl_vars['module_template_dir']->value;?>
loyalty4.gif" width="46" height="46" id="loyalty-icon" alt="<?php echo smartyTranslate(array('s'=>'Loyalty program','mod'=>'loyalty'),$_smarty_tpl);?>
" class="icon" title="<?php if ($_smarty_tpl->tpl_vars['points']->value) {?>
		<?php echo smartyTranslate(array('s'=>'By buying this product you can collect up to','mod'=>'loyalty'),$_smarty_tpl);?>
 <?php echo $_smarty_tpl->tpl_vars['points']->value;?>
 
		<?php if ($_smarty_tpl->tpl_vars['points']->value>1) {?><?php echo smartyTranslate(array('s'=>'loyalty points','mod'=>'loyalty'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'loyalty point','mod'=>'loyalty'),$_smarty_tpl);?>
<?php }?>. 
	 <?php echo smartyTranslate(array('s'=>'that can be converted into a voucher of','mod'=>'loyalty'),$_smarty_tpl);?>
 
		<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['voucher']->value),$_smarty_tpl);?>
. <?php echo smartyTranslate(array('s'=>'You will be able to convert your points when they will be greather than 5 euros, by clicking on "My account", then on "My loyalty points"','mod'=>'loyalty'),$_smarty_tpl);?>
.
	<?php } else { ?>
		<?php if (isset($_smarty_tpl->tpl_vars['no_pts_discounted']->value)&&$_smarty_tpl->tpl_vars['no_pts_discounted']->value==1) {?>
			<?php echo smartyTranslate(array('s'=>'No reward points for this product because there\'s already a discount.','mod'=>'loyalty'),$_smarty_tpl);?>

		<?php } else { ?>
			<?php echo smartyTranslate(array('s'=>'No reward points for this product.','mod'=>'loyalty'),$_smarty_tpl);?>

		<?php }?>
	<?php }?>" />

</div><?php }} ?>
