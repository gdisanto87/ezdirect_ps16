<?php /* Smarty version Smarty-3.1.19, created on 2021-11-24 11:58:17
         compiled from "/var/www/html/themes/ez20/modules/homefeatured/homefeatured.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4023428506193923c4b68c6-96554014%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4b62a5eaf2ea421968c709af6514a9bf8e33d004' => 
    array (
      0 => '/var/www/html/themes/ez20/modules/homefeatured/homefeatured.tpl',
      1 => 1637749975,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4023428506193923c4b68c6-96554014',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_6193923c532811_51148313',
  'variables' => 
  array (
    'js_dir' => 0,
    'link' => 0,
    'img_ps_dir' => 0,
    'products' => 0,
    'nbLi' => 0,
    'nbItemsPerLine' => 0,
    'nbLines' => 0,
    'liHeight' => 0,
    'i' => 0,
    'product' => 0,
    'xx' => 0,
    'quantity_discount' => 0,
    'restricted_country_mode' => 0,
    'PS_CATALOG_MODE' => 0,
    'priceDisplay' => 0,
    'static_token' => 0,
    'img_dir' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6193923c532811_51148313')) {function content_6193923c532811_51148313($_smarty_tpl) {?><?php if (!is_callable('smarty_function_math')) include '/var/www/html/tools/smarty/plugins/function.math.php';
?>
</div>
<div id="home_banners">
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['js_dir']->value;?>
unslider.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$(function() {
			$('.banner').unslider({
				speed: 500,               //  The speed to animate each slide (in milliseconds)
				delay: 5000,              //  The delay between slide animations (in milliseconds)
				keys: true,               //  Enable keyboard (left, right) arrow shortcuts
				dots: true,               //  Display dot navigation
			});
		});
	});
	</script>
	
	


	
</div>
<div style="clear:both"></div>




<div class="center_column" style="width: 100%; ">

	<div class="container" >

		<h4 class='homepage' style="width:100%; margin: 10px auto; height: fit-content;"><?php echo smartyTranslate(array('s'=>'Le migliori soluzioni per comunicare','mod'=>'homefeatured'),$_smarty_tpl);?>
</h4>

		<div id="categories-home">

			<div class="row" >

				<div class="col-12 col-sm-6 col-lg-4">
				
					<div class='categories-home-in' style="overflow-x:auto;">
						
						<table style='width:100%; height:100%; table-layout:fixed;'>
							<tr>
								<td style="width:250px;">
									<h3><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCategoryLink(23), ENT_QUOTES, 'UTF-8', true);?>
">Centralini telefonici</a></h3>
								</td>

							</tr>

							<tr style="display: block;">
								<td style='width:50%; vertical-align:top  !important'>
									
									<ul style="margin-left:18px;">
									<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo_linee-analogica_voip_isdn/tipo_di_derivati_int_collegabili-analogici_voip-analogici_digitali_voip/supporta_linee_voip_sip-si-si_opzionale">VoIP</a></li>
									<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo-centralino_virtuale">Cloud</a></li>
									<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo-centralino_software">Solo Software</a></li>
									<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo-centralino_dect">DECT</a></li>
									<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo_di_derivati_int_collegabili-analogici_voip-analogici_digitali_voip-analogici-int_analogici_digita">Analogici</a></li>
									<li><a href="https://www.ezdirect.it/centralini-telefonici/">Tutti</a></li>
									
									
									</ul>
								</td>
								
								<td style='width:50%; vertical-align:middle !important'>
								<div class='categories-home-in-img'>
									<img src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
centralini-home.png'  alt='Centralini telefonici' title='Centralini telefonici' />
								</div>
								</td>
							</tr>
						</table>
					</div>
				</div>

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
				
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCategoryLink(115), ENT_QUOTES, 'UTF-8', true);?>
">Telefoni</a></h3>
								</td>

							</tr>
							
							<tr style="display: block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							<li><a href="https://www.ezdirect.it/telefoni-fissi/">Fissi</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/#/tipo-cordless_tipo_voip">Cordless IP</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-fissi/#/ottimizzato_per-microsoft_teams">Teams</a></li>
							<li><a href="https://www.ezdirect.it/cerca-ricerca-cercare-prodotti-telefonia?orderby=position&orderway=desc&nnn=on&cod=on&sd=on&search_query=certificato%20zoom&submit_search=Cerca">Zoom</a></li>
							<li><a href="https://www.ezdirect.it/cerca-ricerca-cercare-prodotti-telefonia?orderby=position&orderway=desc&nnn=on&cod=on&sd=on&search_query=videotelefoni&submit_search=Cerca">Videotelefoni</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/#/tipo-cordless_tipo_wifi">WiFi</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-fissi/">Tutti i fissi</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/">Tutti i cordless</a></li>
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<img src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
telefoni-home.png' alt='Ezdirect' title='Ezdirect' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>

				
			

			

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
			
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCategoryLink(106), ENT_QUOTES, 'UTF-8', true);?>
">Cuffie e auricolari</a></h3>
								</td>

							</tr>
						<tr style="display:block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-con_filo">Con filo</a></li>
							<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-senza_filo">Senza filo</a></li>
							<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-ibrida">Ibride</a></li>
							<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-con_filo/utilizzabile_con-pc_usb_bluetooth-telefono_fisso_pc_usb">Multiuso</a></li>
							<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/ottimizzato_per-microsoft_teams">Teams</a></li>
							<li><a href="https://www.ezdirect.it/cuffie-telefoniche/">Tutti</a></li>
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<img src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
cuffie-home.png' alt='Cuffie e auricolari' title='Cuffie e auricolari' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
				
						<table style='width:100%; height:100%; table-layout:fixed;'>
						

							<tr>
								<td style="width:250px;">
									<h3><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCategoryLink(75), ENT_QUOTES, 'UTF-8', true);?>
">Audioconferenza</a></h3>
								</td>

							</tr>
						<tr style="display:block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-audc_usb">USB</a></li>
							<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-audioconferenze_voip">IP</a></li>
							<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-multiuso">Multiuso</a></li>
							<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-gsm">GSM</a></li>
							<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-sistema_pro">Sistema PRO</a></li>
							<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/">Tutti</a></li>
							
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<img src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
audioconferenza.png' alt='Audioconferenza' title='Audioconferenza' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
				
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCategoryLink(76), ENT_QUOTES, 'UTF-8', true);?>
">Videoconferenza</a></h3>
								</td>

							</tr>
							<tr style="display: block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-videocamera_usb">Videocamera USB</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-sistema">Sistema</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-servizio_cloud">Cloud</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-clickshare">Clickshare</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-accessori_clickshare">Accessori clickshare</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-room_manager">Room Management</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/">Tutti</a></li>
							
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<img src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
videoconferenza.png' alt='Videoconferenza' title='Videoconferenza' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>
				

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in' style="width:100%;">
			
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCategoryLink(208), ENT_QUOTES, 'UTF-8', true);?>
">Citofoni IP</a></h3>
								</td>

							</tr>
						<tr style="display: block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/tipo-voipcitofoni">Videocitofoni IP</a></li>
							<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/tipo-controllo_accessi">Controllo Accessi</a></li>
							<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/tipo-cit_an">Interfacce</a></li>
							<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/tipo-cit_an">Analogici</a></li>
							<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/">Tutti</a></li>
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important; padding-top: 4px;'>
							<div class='categories-home-in-img'>
								<img src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
2nhome.png' alt='Citofoni IP' title='Citofoni IP' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>

			

			

				

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
				
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCategoryLink(46), ENT_QUOTES, 'UTF-8', true);?>
">Gateway VoIP</a></h3>
								</td>

							</tr>
						<tr style="display: block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							
							<li><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/tipo-gateway_isdn">ISDN</a></li>
							<li><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/porte_fxs-2-4-1-8-24-16-32-48-fino_a_288">Analogico (FXS)</a></li>
							<li><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/porte_fxo-4-2-8-1-16">Analogico (FXO)</a></li>
							<li><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/tipo-gateway_ibrido">Ibrido</a></li>
							<li><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/">Tutti</a></li>
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<img src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
gateway-voip.png' alt='Gateway VoIP' title='Gateway VoIP' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
				
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCategoryLink(27), ENT_QUOTES, 'UTF-8', true);?>
">Gateway GSM</a></h3>
								</td>

							</tr>
						<tr style="display: block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-gateway_gsm">GSM</a></li>
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-gateway_umts-router_umts">WCDMA</a></li>
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-gateway_lte_4g-scheda_lte-ripetitore_lte_gsm-router_lte_4g">LTE</a></li>
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/connessione-analogica">Analogico</a></li>
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/connessione-isdn">ISDN</a></li>
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-ripetitore_gsm_umts_lte-ripetitore_umts_gsm-ripetitore_lte_gsm-ripetitore_gsm_umts_dcs">Ripetitore di segnale</a></li>
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/">Tutti</a></li>
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<img src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
gateway-gsm.png' alt='Gateway GSM' title='Gateway GSM' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
				
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="https://www.ezdirect.it/offerte-speciali">Offerte Speciali</a></h3>
								</td>

							</tr>
						<tr style="display: block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul id="categories-home-in-menu">
							<li><a href="https://www.ezdirect.it/offerte-speciali">Approfitta subito delle nostre promozioni!</a></li>
							
							
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<a href="https://www.ezdirect.it/offerte-speciali">
									<img style="width:100%;" src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
offerte-speciali.png' alt='Offerte speciali' title='Offerte speciali' />
								</a>
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>
			</div>
			
			
			
			
			
			
			
			
			
			
		
		
		</div>
	</div>
	
</div>

<!--I NOSTRI MARCHI-->
<div class="marchi">
	<h2 style="color:#000099; text-align:center; padding: 30px 0; font-size:25px; text-transform: uppercase; letter-spacing: 4px; font-weight: 600; ">I nostri migliori marchi</h2>

	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators" style="display:none;">
		<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		<li data-target="#carousel-example-generic" data-slide-to="1"></li>
		<li data-target="#carousel-example-generic" data-slide-to="2"></li>
	</ol>

	<!-- Wrapper for slides -->
	<div class="carousel-inner" role="listbox">
		<div class="item active">
			<div class="container">
				<div class="row col-sm-12" style="display: flex; align-items: center;">
					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
2n.png" alt="Banner">
						<p id="nome-marchio">2n</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
yeastar.png" alt="Banner">
						<p id="nome-marchio">Yeastar</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
3cx.png" alt="Banner">
						<p id="nome-marchio">3cx</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
jabra.png" alt="Banner">
						<p id="nome-marchio">jabra</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
openvox.jpeg" alt="Banner">
						<p id="nome-marchio">Open Vox</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
ezdirect.png" alt="Banner">
						<p id="nome-marchio">EzDirect</p></a>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
		<div class="container">
				<div class="row col-sm-12" style="display: flex; align-items: center;">
					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
patton.png" alt="Banner">
						<p id="nome-marchio">Patton</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
ascom.png" alt="Banner">
						<p id="nome-marchio">Ascom</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
cisco.png" alt="Banner">
						<p id="nome-marchio">Cisco</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
fanvil.png" alt="Banner">
						<p id="nome-marchio">Fanvil</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
gigaset.png" alt="Banner">
						<p id="nome-marchio">Gigaset</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
snom.png" alt="Banner">
						<p id="nome-marchio">Snom</p></a>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="container">
				<div class="row col-sm-12" style="display: flex; align-items: center;">
					<div class="col-sm-2" id="col-marchio-nome">
						<a href="https://shop.samsung.com/it/?cid=it_pd_ppc_google_samsung_brand_shop_text_text_brand-kw&gclid=Cj0KCQjwkIGKBhCxARIsAINMioIwvbdiBw2CoJ4ZV0Eeue3wrPQ7whtIY0e1eu_6hUUVgHUabp9oLB0aAnyBEALw_wcB&gclsrc=aw.ds" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
samsung.png" alt="Banner">
						<p id="nome-marchio">Samsung</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
panasonic.png" alt="Banner">
						<p id="nome-marchio">Panasonic</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
dahua.png" alt="Banner">
						<p id="nome-marchio">Dahua</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
yealink.png" alt="Banner">
						<p id="nome-marchio">Yealink</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
hiboost.png" alt="Banner">
						<p id="nome-marchio">Hiboost</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
grandstream.png" alt="Banner">
						<p id="nome-marchio">Grandstream</p></a>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="container">
				<div class="row col-sm-12" style="display: flex; align-items: center;">
					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
dinstar.png" alt="Banner">
						<p id="nome-marchio">Dinstar</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
lucky-tone.jpeg" alt="Banner">
						<p id="nome-marchio">Lucky Tone</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
avaya.png" alt="Banner">
						<p id="nome-marchio">Avaya</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
alcatel.png" alt="Banner">
						<p id="nome-marchio">Alcatel</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
epos.png" alt="Banner">
						<p id="nome-marchio">Epos</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
poly.png" alt="Banner">
						<p id="nome-marchio">Poly</p></a>
					</div>
				</div>
			</div>
		</div>


	

	<!-- Controls -->
	<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev" style="color: #f4f4f4; background-image: none; width: 85px; background-color: white; opacity: 1; z-index: 0;">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next" style="color: #f4f4f4; background-image: none; width: 115px; background-color: white; opacity: 1; z-index: 0;">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
	</div>
</div>

</div>

<!--fine I NOSTRI MARCHI-->

		

		


<div class="center_column">

<h4 class='homepage'><?php echo smartyTranslate(array('s'=>'In evidenza','mod'=>'homefeatured'),$_smarty_tpl);?>
</h4>

	<!-- MODULE Home Featured Products -->
	<div id="home_featured_module">
		<div id='coin-slider'>
			<?php if (isset($_smarty_tpl->tpl_vars['products']->value)&&$_smarty_tpl->tpl_vars['products']->value) {?>
				<?php $_smarty_tpl->tpl_vars['liHeight'] = new Smarty_variable(342, null, 0);?>
				<?php $_smarty_tpl->tpl_vars['nbItemsPerLine'] = new Smarty_variable(4, null, 0);?>
				<?php $_smarty_tpl->tpl_vars['nbLi'] = new Smarty_variable(count($_smarty_tpl->tpl_vars['products']->value), null, 0);?>
				<?php echo smarty_function_math(array('equation'=>"nbLi/nbItemsPerLine",'nbLi'=>$_smarty_tpl->tpl_vars['nbLi']->value,'nbItemsPerLine'=>$_smarty_tpl->tpl_vars['nbItemsPerLine']->value,'assign'=>'nbLines'),$_smarty_tpl);?>

				<?php echo smarty_function_math(array('equation'=>"nbLines*liHeight",'nbLines'=>ceil($_smarty_tpl->tpl_vars['nbLines']->value),'liHeight'=>$_smarty_tpl->tpl_vars['liHeight']->value,'assign'=>'ulHeight'),$_smarty_tpl);?>

				<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable('0', null, 0);?>
				<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
				<?php if ($_smarty_tpl->tpl_vars['i']->value==0||$_smarty_tpl->tpl_vars['i']->value==1||$_smarty_tpl->tpl_vars['i']->value==2||$_smarty_tpl->tpl_vars['i']->value==3||$_smarty_tpl->tpl_vars['i']->value==4||$_smarty_tpl->tpl_vars['i']->value==5||$_smarty_tpl->tpl_vars['i']->value==6||$_smarty_tpl->tpl_vars['i']->value==7||$_smarty_tpl->tpl_vars['i']->value==8||$_smarty_tpl->tpl_vars['i']->value==9||$_smarty_tpl->tpl_vars['i']->value==10||$_smarty_tpl->tpl_vars['i']->value==11||$_smarty_tpl->tpl_vars['i']->value==12||$_smarty_tpl->tpl_vars['i']->value==13||$_smarty_tpl->tpl_vars['i']->value==14||$_smarty_tpl->tpl_vars['i']->value==15||$_smarty_tpl->tpl_vars['i']->value==16||$_smarty_tpl->tpl_vars['i']->value==17||$_smarty_tpl->tpl_vars['i']->value==18||$_smarty_tpl->tpl_vars['i']->value==19||$_smarty_tpl->tpl_vars['i']->value==20||$_smarty_tpl->tpl_vars['i']->value==21||$_smarty_tpl->tpl_vars['i']->value==22||$_smarty_tpl->tpl_vars['i']->value==23) {?>
			<?php if ($_smarty_tpl->tpl_vars['i']->value==3||$_smarty_tpl->tpl_vars['i']->value==7||$_smarty_tpl->tpl_vars['i']->value==11||$_smarty_tpl->tpl_vars['i']->value==15||$_smarty_tpl->tpl_vars['i']->value==19||$_smarty_tpl->tpl_vars['i']->value==23) {?>
			<div class='home-low-level' style='margin-right:-1px <?php if ($_smarty_tpl->tpl_vars['i']->value>15) {?>; height:280px<?php }?>'>
			<?php } else { ?>
			<div class='home-low-level' style=' <?php if ($_smarty_tpl->tpl_vars['i']->value>15) {?>; height:280px<?php }?>'>
			<?php }?>
			
			<div style=''>
			
			
		
						
						
						<?php if ($_smarty_tpl->tpl_vars['product']->value['cheapnet']=='cheapnet_6') {?>
			<div class='cheapnet' style="top:20px; left:0px; width:30px; z-index:9">
			<a href='https://www.ezdirect.it/cheapnetcoupons.php'><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
cheapnet-300.png" alt="Con questo prodotto hai diritto a un Coupon Cheapnet da 300 minuti gratis" title="Con questo prodotto hai diritto a un Coupon Cheapnet da 300 minuti gratis" width="57" height="57" /></a>
			</div>
			<?php } elseif ($_smarty_tpl->tpl_vars['product']->value['cheapnet']=='cheapnet_30') {?>
			<div class='cheapnet' style="top:20px; left:0px;  width:30px; z-index:9">
			<a href='https://www.ezdirect.it/cheapnetcoupons.php'><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
cheapnet-600.png" alt="Con questo prodotto hai diritto a un Coupon Cheapnet da 600 minuti gratis" title="Con questo prodotto hai diritto a un Coupon Cheapnet da 600 minuti gratis" width="57" height="57" /></a>
			</div>
			<?php } else { ?>
			<?php }?>			
						<a href="<?php echo $_smarty_tpl->tpl_vars['product']->value['link'];?>
" style="display:block; position:relative" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
"  class="product_img_link"><img src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['id_image'],'home');?>
" <?php if ($_smarty_tpl->tpl_vars['product']->value['secondary_image']!='') {?>onmouseover="this.src='<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['secondary_image'],'home');?>
'" onmouseout="this.src='<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['id_image'],'home');?>
'"<?php }?> class="classhome" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" /></a>
			</div>
			
			
			
			
			<div style='margin-top:20px'>
			<h5 style='font-size:15px'><a class="product_name_title" style="line-height:22px" href="<?php echo $_smarty_tpl->tpl_vars['product']->value['link'];?>
" title="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate(mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'),75,'...');?>
</a></h5>
			</div>	
			
			<div class='home-medium-category'>
<span><a style='font-weight:normal' href="<?php echo $_smarty_tpl->tpl_vars['product']->value['macro_link'];?>
"><?php if ($_smarty_tpl->tpl_vars['product']->value['macrocategory']=='Dispositivi di protezione individuale DPI') {?>Dispositivi DPI<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['product']->value['macrocategory'];?>
<?php }?></a></span>
			</div>

			
			
<div style="display: flex; justify-content: space-between; align-items: center; ">
			<div class='home-medium-price'>
		<?php if ($_smarty_tpl->tpl_vars['product']->value['fuori_produzione']==1) {?>
<?php } else { ?>	
			<?php if ($_smarty_tpl->tpl_vars['product']->value['prezzospeciale']==1) {?>
					<span style='font-size:25px; font-weight:bold; color: #ff6600;'><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['product']->value['special_price']),$_smarty_tpl);?>
</span>
				<?php } else { ?>
							<?php if ($_smarty_tpl->tpl_vars['product']->value['quantity_discount']) {?>
						<?php $_smarty_tpl->tpl_vars["xx"] = new Smarty_variable("0", null, 0);?>
						<?php $_smarty_tpl->tpl_vars['numItems'] = new Smarty_variable(count($_smarty_tpl->tpl_vars['product']->value['quantity_discount']), null, 0);?>
						<?php  $_smarty_tpl->tpl_vars['quantity_discount'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['quantity_discount']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product']->value['quantity_discount']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['quantity_discount']->key => $_smarty_tpl->tpl_vars['quantity_discount']->value) {
$_smarty_tpl->tpl_vars['quantity_discount']->_loop = true;
?>
						<?php if ($_smarty_tpl->tpl_vars['xx']->value==0) {?>				
						<?php echo smartyTranslate(array('s'=>'From','mod'=>'homefeatured'),$_smarty_tpl);?>

						
						<span style='font-size:25px; font-weight:bold; color: #ff6600;'><strong><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['quantity_discount']->value['price']-floatval(($_smarty_tpl->tpl_vars['quantity_discount']->value['price']*$_smarty_tpl->tpl_vars['quantity_discount']->value['reduction']))),$_smarty_tpl);?>
</strong></span> 
						<?php } else { ?>	
						<?php }?>
						<?php $_smarty_tpl->tpl_vars['xx'] = new Smarty_variable($_smarty_tpl->tpl_vars['xx']->value+1, null, 0);?>
						<?php } ?>
						
					<?php } else { ?>
					<?php if ($_smarty_tpl->tpl_vars['product']->value['show_price']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?><span style='font-size:25px; font-weight:bold; color: #ff6600;'><strong><?php if (!$_smarty_tpl->tpl_vars['priceDisplay']->value) {?><?php if ($_smarty_tpl->tpl_vars['product']->value['price_tax_exc']>0) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['product']->value['price_tax_exc']),$_smarty_tpl);?>
<?php }?><?php } else { ?>
					<?php if ($_smarty_tpl->tpl_vars['product']->value['price_tax_exc']>0) {?>
					<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['product']->value['price_tax_exc']),$_smarty_tpl);?>
<?php }?>
					<?php }?>
					</strong></span><?php } else { ?><?php }?>
					<?php }?>
				<?php }?>
		<?php }?>	
			</div>
			<?php if ($_smarty_tpl->tpl_vars['i']->value==16||$_smarty_tpl->tpl_vars['i']->value==17||$_smarty_tpl->tpl_vars['i']->value==18||$_smarty_tpl->tpl_vars['i']->value==19||$_smarty_tpl->tpl_vars['i']->value==20||$_smarty_tpl->tpl_vars['i']->value==21||$_smarty_tpl->tpl_vars['i']->value==22||$_smarty_tpl->tpl_vars['i']->value==23) {?>
			<?php } else { ?>
			<div class="home-medium-cart-slot">
			
			
									
									
				<a name="<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('cart.php');?>
?qty=1&amp;id_product=<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
&amp;token=<?php echo $_smarty_tpl->tpl_vars['static_token']->value;?>
&amp;add" id="ajax_id_product_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
" title="<?php echo smartyTranslate(array('s'=>'Add'),$_smarty_tpl);?>
 <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 <?php echo smartyTranslate(array('s'=>'to cart'),$_smarty_tpl);?>
">
									
					<i class="fas fa-cart-plus" id="cart-plus" style="color: #ff6600; font-size: 26px;"></i>
									
				</a>
			</div>	
				</div>
		<?php }?>
			
			<?php if ($_smarty_tpl->tpl_vars['i']->value==15) {?>
			
			</div><div style='clear:both'></div>
			
			
			<div id="home_center">
				<div id="home_center_1">
					<a href='https://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini' target='_blank'><img style="display:block; float:left; margin-right: 27px" src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
preventivo-gratuito-2.jpg' alt='Preventivo gratuito' title='Preventivo gratuito' /></a>
					<a href='https://www.ezdirect.it/ti-richiamiamo-noi' target='_blank'><img style="display:block; float:left; margin-right: 27px" src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
ti-richiamiamo-noi-2.jpg' alt='Ti richiamiamo noi' title='Ti richiamiamo noi' /></a>
					<a href='https://www.ezdirect.it/guide/65-corporate-account-ezdirect-account-manager-dedicato-per-grandi-aziende-e-pa' target='_blank'><img style="display:block; float:left; margin-right: 27px" src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
corporate-account-2.jpg' alt='Corporate account' title='Corporate account' /></a>
					<a href='https://www.ezdirect.it/guide/69-ezdirect-azienda-accreditata-mepa' target='_blank'><img style="display:block; float:left;" src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
acquisti-in-rete-2.jpg' alt='Acquisti in rete' title='Acquisti in rete' /></a>
					<div style="clear:both"></div>
				</div>
			
			</div>
			
			
			
<script type="text/javascript">
	$(document).ready(function(){
		$(function() {
			$('.banner-manufacturer').unslider({
				speed: 500,               //  The speed to animate each slide (in milliseconds)
				delay: 5000,              //  The delay between slide animations (in milliseconds)
				keys: true,               //  Enable keyboard (left, right) arrow shortcuts
				dots: true,               //  Display dot navigation
			});
		});
	});
	</script>
<h4 class='homepage'><?php echo smartyTranslate(array('s'=>'I nostri marchi','mod'=>'homefeatured'),$_smarty_tpl);?>
</h4><br /><br />
	<div id="banner-manufacturers">
		<div class="banner-manufacturer">
			<ul style="height:40px">
			
			
				<li style="width:100% !important">
				<a href='https://www.ezdirect.it/105m-ezdirect/'><img src='https://www.ezdirect.it/img/m/105-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/10m-plantronics/'><img src='https://www.ezdirect.it/img/m/10-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/12m-panasonic/'><img src='https://www.ezdirect.it/img/m/12-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/37m-jabra/'><img src='https://www.ezdirect.it/img/m/37-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/17m-gigaset/'><img src='https://www.ezdirect.it/img/m/17-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/27m-sennheiser/'><img src='https://www.ezdirect.it/img/m/27-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				</li>			
					
				<li style="width:100% !important">
				<a href='https://www.ezdirect.it/43m-grandstream/'><img src='https://www.ezdirect.it/img/m/43-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/47m-plantronics/'><img src='https://www.ezdirect.it/img/m/47-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/172m-flir/'><img src='https://www.ezdirect.it/img/m/172-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/179m-3cx/'><img src='https://www.ezdirect.it/img/m/37-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/117m-yealink/'><img src='https://www.ezdirect.it/img/m/117-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/132m-yeastar/'><img src='https://www.ezdirect.it/img/m/132-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				</li>	
				
			</ul>
		</div>
</div>
	<br /><br /><h5 style="text-align: center;
color: #009;
font-size: 16px;">I marchi in ordine alfabetico</h5><br />
			
			
			<table style='text-align:left; width:80%; margin:0 auto'>

	<tr><td>
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('108','2N');?>
">2N</a><br />
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('179','3CX');?>
">3CX</a><br />
	
	
	
	
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('158','Akuvox');?>
">Akuvox</a><br />
	
		<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('124','Alcatel');?>
">Alcatel</a><br />
	
	
	
		
		<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('45','Alcatel Lucent');?>
">Alcatel Lucent&nbsp;&nbsp;</a><br />
	
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('122','Ascom');?>
">Ascom</a><br />
	
	
	
		
			
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('199','Aten');?>
">Aten</a><br />
	</td><td>
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('47','Audiocodes');?>
">Audiocodes</a><br />
	
	
	
	
		
			<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('46','Avaya');?>
">Avaya</a><br />
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('219','Barco');?>
">Barco</a><br />	
	
	
	
	
		<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('103','Cisco');?>
">Cisco</a><br />
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('206','Dahua');?>
">Dahua</a><br />
	
	
	
	
		
		<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('202','Eaton');?>
">Eaton</a><br />
		
		

	
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('130','Engenius');?>
">Engenius</a><br />
	
	
	</td><td>
		
		<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('134','Escene');?>
">Escene</a><br />
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('105','Ezdirect');?>
">Ezdirect</a><br />
	
	
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('133','Fanvil');?>
">Fanvil</a><br />
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('17','Gigaset');?>
">Gigaset</a><br />
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('43','Grandstream');?>
">Grandstream</a><br />
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('182','Hiboost');?>
">Hiboost</a><br />
	
		
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('195','Huawei');?>
">Huawei</a><br />
	</td><td>
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('37','Jabra');?>
">Jabra GN</a><br />
	
	


<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('57','Konftel');?>
">Konftel</a><br />


	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('50','Logitech');?>
">Logitech</a><br />
	
	
		
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('106','Mitel Aastra');?>
">Mitel Aastra</a><br />	
	
		<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('191','Openvox');?>
">Openvox</a><br />
	
		
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('12','Panasonic');?>
">Panasonic</a><br />
	
		
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('120','Patton');?>
">Patton</a><br />
	
	
	</td><td>
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('142','Peltor 3M');?>
">Peltor 3M</a><br />
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('10','Plantronics');?>
">Plantronics</a><br />
	
		
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('25','Polycom');?>
">Polycom</a><br />	
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('31','Samsung');?>
">Samsung</a><br />
	
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('27','Sennheiser');?>
">Sennheiser</a><br />
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('111','Siemens');?>
">Siemens</a><br />
	
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('109','Snom');?>
">Snom</a><br />
	</td><td>
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('146','Spectralink');?>
">Spectralink</a><br />
	
	
	
	
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('192','Starleaf Italia');?>
">Starleaf Italia</a><br />
	
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('116','Teltonika');?>
">Teltonika</a><br />
	
	
	
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('136','Twig');?>
">Twig</a><br />
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('221','Univois');?>
">Univois</a><br />
	
	
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('117','Yealink');?>
">Yealink</a><br />
	
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getManufacturerLink('132','Yeastar');?>
">Yeastar</a><br />
	</td></tr>
	</table>
	
			
				<h4 class='homepage'><a href="https://www.ezdirect.it/prodotti-venduti-migliori-sito-web-ezdirect">In evidenza</a></h4><br />
				<?php } elseif ($_smarty_tpl->tpl_vars['i']->value==19) {?>
				</div><div style='clear:both'></div>
					<h4 class='homepage'><a href="https://www.ezdirect.it/nuovi-prodotti-telefoni-fissi-voip-cordless-centralini"><?php echo smartyTranslate(array('s'=>'New products','mod'=>'homefeatured'),$_smarty_tpl);?>
</a></h4>
				
				<?php } else { ?>
				</div>
				<?php }?>
				
				
				<?php } elseif ($_smarty_tpl->tpl_vars['i']->value==16||$_smarty_tpl->tpl_vars['i']->value==17||$_smarty_tpl->tpl_vars['i']->value==18||$_smarty_tpl->tpl_vars['i']->value==19) {?>
				<?php }?>
			<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
			<?php } ?>
			<div style='clear:both'></div>

	<?php } else { ?>
		<p><?php echo smartyTranslate(array('s'=>'No featured products','mod'=>'homefeatured'),$_smarty_tpl);?>
</p>
	<?php }?>
	
	

<script type="text/javascript">
	/*$(document).ready(function(){
		$(function() {
			$('.banner-parlano').unslider({
				speed: 500,               //  The speed to animate each slide (in milliseconds)
				delay: 5000,              //  The delay between slide animations (in milliseconds)
				keys: true,               //  Enable keyboard (left, right) arrow shortcuts
				dots: true,               //  Display dot navigation
			});
		});
	});*/
	</script>
	

	<div id="banner-parlano">
		<h4 class='homepage' style="margin-bottom: 0;"><?php echo smartyTranslate(array('s'=>'Parlano di noi','mod'=>'homefeatured'),$_smarty_tpl);?>
</h4><br /><br />
		<div class="banner-parlano">
			<ul style="height:40px">
			
			
				<li style="width:100% !important">
				<a href='https://www.notizie.it/economia/2019/10/01/ezdirect-vendita-online-soluzioni-per-comunicare/'><img src='https://www.ezdirect.it/themes/ez20/img/parlano/notizie.jpg' width='150' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.datamanager.it/2021/01/ezdirect-le-soluzioni-per-comunicare/'><img src='https://www.ezdirect.it/themes/ez20/img/parlano/datamanager.jpg' width='150' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.punto-informatico.it/ezdirect-soluzioni-per-la-unified-communication/'><img src='https://www.ezdirect.it/themes/ez20/img/parlano/puntoinformatico.jpg' width='150' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.adnkronos.com/ezdirect-la-risposta-professionale-alle-esigenze-di-soluzioni-per-comunicare-delle-imprese_1oRbAnwIjAeMOM9mIbBjRf?refresh_ce'><img src='https://www.ezdirect.it/themes/ez20/img/parlano/adnkronos.jpg' width='150' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.affaritaliani.it/comunicati/notiziario/ezdirect_la_risposta_%E2%80%9Cprofessionale%E2%80%9D_alle_esigenze_di_soluzioni_per_comunicare_delle_imprese-86213.html?refresh_cens'><img width='150' src='https://www.ezdirect.it/themes/ez20/img/parlano/affariitaliani.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.techcompany360.it/distributori/le-competenze-ezdirect-sempre-in-linea-con-le-esigenze-dei-clienti/'><img src='https://www.ezdirect.it/themes/ez20/img/parlano/techcompany360.jpg' width='150' alt='Ezdirect' title='Ezdirect' /></a>
				</li>			
					
			</ul><br /><br /><br />
		</div>
</div>

<!-- /MODULE Home Featured Products -->
<?php }} ?>
