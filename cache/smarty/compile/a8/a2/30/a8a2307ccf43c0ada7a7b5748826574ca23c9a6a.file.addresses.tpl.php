<?php /* Smarty version Smarty-3.1.19, created on 2021-12-13 15:26:14
         compiled from "/var/www/html/themes/ez20/addresses.tpl" */ ?>
<?php /*%%SmartyHeaderCode:149480549661b75806080df8-30535843%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a8a2307ccf43c0ada7a7b5748826574ca23c9a6a' => 
    array (
      0 => '/var/www/html/themes/ez20/addresses.tpl',
      1 => 1637749858,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '149480549661b75806080df8-30535843',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'multipleAddresses' => 0,
    'addresses' => 0,
    'address' => 0,
    'address_key' => 0,
    'ignoreList' => 0,
    'address_number' => 0,
    'address_key_number' => 0,
    'address_content' => 0,
    'addresses_style' => 0,
    'js_dir' => 0,
    'link' => 0,
    'navigationPipe' => 0,
    'img_dir' => 0,
    'pattern' => 0,
    'addressKey' => 0,
    'key' => 0,
    'addresses_names' => 0,
    'base_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61b758060b49c4_74589090',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61b758060b49c4_74589090')) {function content_61b758060b49c4_74589090($_smarty_tpl) {?><?php if (!is_callable('smarty_function_counter')) include '/var/www/html/tools/smarty/plugins/function.counter.php';
?>





<?php if (!isset($_smarty_tpl->tpl_vars['multipleAddresses']->value)) {?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[0] = "id_address";?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[1] = "id_country";?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[2] = "id_state";?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[3] = "id_customer";?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[4] = "id_manufacturer";?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[5] = "id_supplier";?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[6] = "date_add";?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[7] = "date_upd";?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[8] = "active";?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[9] = "deleted";?>	
	
	
	<?php if (isset($_smarty_tpl->tpl_vars['addresses']->value)) {?>
		<?php $_smarty_tpl->tpl_vars['address_number'] = new Smarty_variable(0, null, 0);?>
		<?php  $_smarty_tpl->tpl_vars['address'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['address']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['addresses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['address']->key => $_smarty_tpl->tpl_vars['address']->value) {
$_smarty_tpl->tpl_vars['address']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['address']->key;
?>
			<?php echo smarty_function_counter(array('start'=>0,'skip'=>1,'assign'=>'address_key_number'),$_smarty_tpl);?>

			<?php  $_smarty_tpl->tpl_vars['address_content'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['address_content']->_loop = false;
 $_smarty_tpl->tpl_vars['address_key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['address']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['address_content']->key => $_smarty_tpl->tpl_vars['address_content']->value) {
$_smarty_tpl->tpl_vars['address_content']->_loop = true;
 $_smarty_tpl->tpl_vars['address_key']->value = $_smarty_tpl->tpl_vars['address_content']->key;
?>
				<?php if (!in_array($_smarty_tpl->tpl_vars['address_key']->value,$_smarty_tpl->tpl_vars['ignoreList']->value)) {?>
					<?php $_smarty_tpl->createLocalArrayVariable('multipleAddresses', null, 0);
$_smarty_tpl->tpl_vars['multipleAddresses']->value[$_smarty_tpl->tpl_vars['address_number']->value]['ordered'][$_smarty_tpl->tpl_vars['address_key_number']->value] = $_smarty_tpl->tpl_vars['address_key']->value;?>
					<?php $_smarty_tpl->createLocalArrayVariable('multipleAddresses', null, 0);
$_smarty_tpl->tpl_vars['multipleAddresses']->value[$_smarty_tpl->tpl_vars['address_number']->value]['formated'][$_smarty_tpl->tpl_vars['address_key']->value] = $_smarty_tpl->tpl_vars['address_content']->value;?>
					<?php echo smarty_function_counter(array(),$_smarty_tpl);?>

				<?php }?>
			<?php } ?>
		<?php $_smarty_tpl->createLocalArrayVariable('multipleAddresses', null, 0);
$_smarty_tpl->tpl_vars['multipleAddresses']->value[$_smarty_tpl->tpl_vars['address_number']->value]['object'] = $_smarty_tpl->tpl_vars['address']->value;?>
		<?php $_smarty_tpl->tpl_vars['address_number'] = new Smarty_variable($_smarty_tpl->tpl_vars['address_number']->value+1, null, 0);?>
		<?php } ?>
	<?php }?>
<?php }?>



<?php if (!isset($_smarty_tpl->tpl_vars['addresses_style']->value)) {?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['company'] = 'address_company';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['vat_number'] = 'address_company';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['firstname'] = 'address_name';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['lastname'] = 'address_name';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['address1'] = 'address_address1';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['address2'] = 'address_address2';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['suburb'] = 'address_suburb';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['c_o'] = 'address_c_o';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['city'] = 'address_city';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['country'] = 'address_country';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['phone'] = 'address_phone';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['phone_mobile'] = 'address_phone_mobile';?>
		<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['fax'] = 'address_fax';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['alias'] = 'address_title';?>
<?php }?>

<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['js_dir']->value;?>
etc/resizeAddressesBox.js">
</script>

<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account.php',true);?>
"><?php echo smartyTranslate(array('s'=>'My account'),$_smarty_tpl);?>
</a><span class="navigation-pipe"><?php echo $_smarty_tpl->tpl_vars['navigationPipe']->value;?>
</span><?php echo smartyTranslate(array('s'=>'My addresses'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<h1><?php echo smartyTranslate(array('s'=>'My addresses'),$_smarty_tpl);?>
</h1>
<p><?php echo smartyTranslate(array('s'=>'Please configure the desired billing and delivery addresses to be preselected when placing an order. You may also add additional addresses, useful for sending gifts or receiving your order at the office.'),$_smarty_tpl);?>
</p>

<?php if (isset($_smarty_tpl->tpl_vars['multipleAddresses']->value)&&$_smarty_tpl->tpl_vars['multipleAddresses']->value) {?>
<div class="addresses" style="width:100%">
	<h3><?php echo smartyTranslate(array('s'=>'Your addresses are listed below.'),$_smarty_tpl);?>
</h3>
	<p><?php echo smartyTranslate(array('s'=>'Be sure to update them if they have changed.'),$_smarty_tpl);?>
</p>
	<?php $_smarty_tpl->tpl_vars["adrs_style"] = new Smarty_variable($_smarty_tpl->tpl_vars['addresses_style']->value, null, 0);?>
	
	<?php  $_smarty_tpl->tpl_vars['address'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['address']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['multipleAddresses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['address']->key => $_smarty_tpl->tpl_vars['address']->value) {
$_smarty_tpl->tpl_vars['address']->_loop = true;
?>
		<?php if ($_smarty_tpl->tpl_vars['address']->value['object']['fatturazione']==1) {?>
		<img src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/fatturazione.gif' width="25" height="25" alt="<?php echo smartyTranslate(array('s'=>'Invoice address'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'Invoice address'),$_smarty_tpl);?>
" style="display:block; float:left" />
		<strong><?php echo smartyTranslate(array('s'=>'Invoice address'),$_smarty_tpl);?>
</strong><br />
		<?php echo smartyTranslate(array('s'=>'This is your invoice address. If you want to change your tax code and/or your VAT number, please go to your personal details page'),$_smarty_tpl);?>
.
			<p class="clear" ></p>
		<ul class="address">
			<li><strong><?php echo smartyTranslate(array('s'=>'Address name:'),$_smarty_tpl);?>
 <?php echo $_smarty_tpl->tpl_vars['address']->value['object']['alias'];?>
</strong></li>
			<li>
			<table>
			<?php  $_smarty_tpl->tpl_vars['pattern'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['pattern']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['address']->value['ordered']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['pattern']->key => $_smarty_tpl->tpl_vars['pattern']->value) {
$_smarty_tpl->tpl_vars['pattern']->_loop = true;
?>
				<?php $_smarty_tpl->tpl_vars['addressKey'] = new Smarty_variable(explode(" ",$_smarty_tpl->tpl_vars['pattern']->value), null, 0);?>
				
				
				<?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['addressKey']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
?>
				
				<?php if ($_smarty_tpl->tpl_vars['address']->value['formated'][$_smarty_tpl->tpl_vars['key']->value]=='') {?>
				<?php } else { ?>
					<tr><td style="width:80px; border-bottom:1px solid #d4d4d4">
					<?php echo $_smarty_tpl->tpl_vars['addresses_names']->value[$_smarty_tpl->tpl_vars['key']->value];?>
</td><td style="width:230px; border-bottom:1px solid #d4d4d4"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['address']->value['formated'][$_smarty_tpl->tpl_vars['key']->value], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td></tr>
					
				<?php }?>
				<?php } ?>
				</li>
			<?php } ?>
			</table>
			
			<li class="address_update"><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('address.php',true);?>
?id_address=<?php echo intval($_smarty_tpl->tpl_vars['address']->value['object']['id']);?>
" title="<?php echo smartyTranslate(array('s'=>'Update'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Update'),$_smarty_tpl);?>
</a></li>
			<?php if ($_smarty_tpl->tpl_vars['address']->value['object']['fatturazione']==1) {?>
			<?php } else { ?>
			<li class="address_delete"><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('address.php',true);?>
?id_address=<?php echo intval($_smarty_tpl->tpl_vars['address']->value['object']['id']);?>
&amp;delete" onclick="return confirm('<?php echo smartyTranslate(array('s'=>'Are you sure?'),$_smarty_tpl);?>
');" title="<?php echo smartyTranslate(array('s'=>'Delete'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Delete'),$_smarty_tpl);?>
</a></li>
			<?php }?>
		</ul>
		<?php } else { ?>
		<?php }?>
	<?php } ?>
	
	
			<p class="clear" style="height: 10px;"></p>
		<img src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/spedizione.gif' width="25" height="25" alt="<?php echo smartyTranslate(array('s'=>'Shipping addresses'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'Shipping addresses'),$_smarty_tpl);?>
" style="display:block; float:left" />
	<strong><?php echo smartyTranslate(array('s'=>'Shipping addresses'),$_smarty_tpl);?>
</strong><br />
	<p style="padding-top: 7px;"><?php echo smartyTranslate(array('s'=>'Here you can find your delivery addresses. If your invoice address and your delivery address are the same, do not enter any address here. If your delivery address is different from your invoice address then click on "Add address" and add a new delivery address'),$_smarty_tpl);?>
.</p>
	<br /><br />
	<div id="shipping_addresses">
	<?php  $_smarty_tpl->tpl_vars['address'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['address']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['multipleAddresses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['address']->key => $_smarty_tpl->tpl_vars['address']->value) {
$_smarty_tpl->tpl_vars['address']->_loop = true;
?>
		<?php if ($_smarty_tpl->tpl_vars['address']->value['object']['fatturazione']==0) {?>
		<ul class="address">
			<li><strong><?php echo smartyTranslate(array('s'=>'Address name:'),$_smarty_tpl);?>
 <?php echo $_smarty_tpl->tpl_vars['address']->value['object']['alias'];?>
</strong></li>
			
			<li>
			<table>
			<?php  $_smarty_tpl->tpl_vars['pattern'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['pattern']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['address']->value['ordered']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['pattern']->key => $_smarty_tpl->tpl_vars['pattern']->value) {
$_smarty_tpl->tpl_vars['pattern']->_loop = true;
?>
				<?php $_smarty_tpl->tpl_vars['addressKey'] = new Smarty_variable(explode(" ",$_smarty_tpl->tpl_vars['pattern']->value), null, 0);?>
				
				<?php  $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['key']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['addressKey']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['key']->key => $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
?>
				<?php if ($_smarty_tpl->tpl_vars['address']->value['formated'][$_smarty_tpl->tpl_vars['key']->value]=='') {?>
				<?php } else { ?>
					<tr><td style="width:80px; border-bottom:1px solid #d4d4d4">
					<?php echo $_smarty_tpl->tpl_vars['addresses_names']->value[$_smarty_tpl->tpl_vars['key']->value];?>
</td><td style="width:230px; border-bottom:1px solid #d4d4d4">
						<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['address']->value['formated'][$_smarty_tpl->tpl_vars['key']->value], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 <?php }?>
					</td></tr>
				<?php } ?>
			<?php } ?>
			</table>
			</li>
			
			<li class="address_update"><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('address.php',true);?>
?id_address=<?php echo intval($_smarty_tpl->tpl_vars['address']->value['object']['id']);?>
" title="<?php echo smartyTranslate(array('s'=>'Update'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Update'),$_smarty_tpl);?>
</a></li>
			<li class="address_delete"><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('address.php',true);?>
?id_address=<?php echo intval($_smarty_tpl->tpl_vars['address']->value['object']['id']);?>
&amp;delete" onclick="return confirm('<?php echo smartyTranslate(array('s'=>'Are you sure?'),$_smarty_tpl);?>
');" title="<?php echo smartyTranslate(array('s'=>'Delete'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Delete'),$_smarty_tpl);?>
</a></li>
			
		</ul>
		<?php } else { ?>
		<?php }?>
	<?php } ?>
	</div>
	<p class="clear" >
	<br /><br />
</div>
<?php } else { ?>
	<p class="warning"><?php echo smartyTranslate(array('s'=>'No addresses available.'),$_smarty_tpl);?>
</p>
<?php }?>

<div class="clear address_add" style="text-align: center;"><a  style="padding: 10px 15px; background-color: #ff6600;
    color: white;" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('address.php',true);?>
" title="<?php echo smartyTranslate(array('s'=>'Add an address'),$_smarty_tpl);?>
" class="button_large"><?php echo smartyTranslate(array('s'=>'Add an address'),$_smarty_tpl);?>
</a></div>

<div class="footer_links" style="display: flex; justify-content: space-between;">
	<div style="display: flex; align-items: center;"><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account.php',true);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/my-account.gif" width="22" height="22" alt="<?php echo smartyTranslate(array('s'=>'Back to Your Account'),$_smarty_tpl);?>
" class="icon" /></a><a style="margin-bottom: 14px;" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account.php',true);?>
"><?php echo smartyTranslate(array('s'=>'Back to Your Account'),$_smarty_tpl);?>
</a></div>
	<div style="display: flex; align-items: center;"><a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/home.gif" alt="" class="icon" /></a><a a style="margin-bottom: 14px;" href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
"><?php echo smartyTranslate(array('s'=>'Home'),$_smarty_tpl);?>
</a></div>
</div><?php }} ?>
