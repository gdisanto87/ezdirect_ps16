<?php /* Smarty version Smarty-3.1.19, created on 2021-11-22 12:06:31
         compiled from "/var/www/html/ezadmin/themes/default/template/controllers/products/informations.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2076651511619b79b70cba30-73058186%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a862f967a15380ffcdd54197953e1c7c0225e3d5' => 
    array (
      0 => '/var/www/html/ezadmin/themes/default/template/controllers/products/informations.tpl',
      1 => 1626882282,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2076651511619b79b70cba30-73058186',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'check_product_association_ajax' => 0,
    'ps_force_friendly_product' => 0,
    'PS_ALLOW_ACCENTED_CHARS_URL' => 0,
    'combinationImagesJs' => 0,
    'link' => 0,
    'id_lang' => 0,
    'display_common_field' => 0,
    'bullet_common_field' => 0,
    'product_type' => 0,
    'is_in_pack' => 0,
    'product' => 0,
    'avviso_doppio' => 0,
    'languages' => 0,
    'class_input_ajax' => 0,
    'canonical' => 0,
    'padrecanonical' => 0,
    'figlio' => 0,
    'data_margin_cli' => 0,
    'data_margin_riv' => 0,
    'data_supplier' => 0,
    'data_othersuppliers' => 0,
    'fornitori' => 0,
    'row' => 0,
    'altri_fornitori' => 0,
    'product_name_redirected' => 0,
    'display_multishop_checkboxes' => 0,
    'PS_PRODUCT_SHORT_DESC_LIMIT' => 0,
    'images' => 0,
    'key' => 0,
    'image' => 0,
    'imagesTypes' => 0,
    'type' => 0,
    'punti_di_forza' => 0,
    'rowsvideos' => 0,
    'rowvideos' => 0,
    'amazon_check_it' => 0,
    'amazon_it_style' => 0,
    'amazon_check_fr' => 0,
    'amazon_fr_style' => 0,
    'amazon_check_es' => 0,
    'amazon_es_style' => 0,
    'amazon_check_uk' => 0,
    'amazon_uk_style' => 0,
    'amazon_check_de' => 0,
    'amazon_de_style' => 0,
    'amazon_check_nl' => 0,
    'amazon_nl_style' => 0,
    'amazon_check_se' => 0,
    'amazon_se_style' => 0,
    'amazon_check_pl' => 0,
    'amazon_pl_style' => 0,
    'language' => 0,
    'table' => 0,
    'default_form_language' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_619b79b715fd48_60934188',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_619b79b715fd48_60934188')) {function content_619b79b715fd48_60934188($_smarty_tpl) {?>
<?php if ($_smarty_tpl->tpl_vars['check_product_association_ajax']->value) {?>
	<?php $_smarty_tpl->tpl_vars['class_input_ajax'] = new Smarty_variable('check_product_name ', null, 0);?>
<?php } else { ?>
	<?php $_smarty_tpl->tpl_vars['class_input_ajax'] = new Smarty_variable('', null, 0);?>
<?php }?>

<div id="product-informations" class="panel product-tab">
	<input type="hidden" name="submitted_tabs[]" value="Informations" />
	<h3 class="tab"> <i class="icon-info"></i> <?php echo smartyTranslate(array('s'=>'Information'),$_smarty_tpl);?>
</h3>
	<script type="text/javascript">

		var msg_select_one = "<?php echo smartyTranslate(array('s'=>'Please select at least one product.','js'=>1),$_smarty_tpl);?>
";
		var msg_set_quantity = "<?php echo smartyTranslate(array('s'=>'Please set a quantity to add a product.','js'=>1),$_smarty_tpl);?>
";

		<?php if (isset($_smarty_tpl->tpl_vars['ps_force_friendly_product']->value)&&$_smarty_tpl->tpl_vars['ps_force_friendly_product']->value) {?>
			var ps_force_friendly_product = 1;
		<?php } else { ?>
			var ps_force_friendly_product = 0;
		<?php }?>
		<?php if (isset($_smarty_tpl->tpl_vars['PS_ALLOW_ACCENTED_CHARS_URL']->value)&&$_smarty_tpl->tpl_vars['PS_ALLOW_ACCENTED_CHARS_URL']->value) {?>
			var PS_ALLOW_ACCENTED_CHARS_URL = 1;
		<?php } else { ?>
			var PS_ALLOW_ACCENTED_CHARS_URL = 0;
		<?php }?>
		<?php echo $_smarty_tpl->tpl_vars['combinationImagesJs']->value;?>

		<?php if ($_smarty_tpl->tpl_vars['check_product_association_ajax']->value) {?>
				var search_term = '';
				$('document').ready( function() {
					$(".check_product_name")
						.autocomplete(
							'<?php echo addslashes($_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminProducts',true));?>
', {
								minChars: 3,
								max: 10,
								width: $(".check_product_name").width(),
								selectFirst: false,
								scroll: false,
								dataType: "json",
								formatItem: function(data, i, max, value, term) {
									search_term = term;
									// adding the little
									if ($('.ac_results').find('.separation').length == 0)
										$('.ac_results').css('background-color', '#EFEFEF')
											.prepend('<div style="color:#585A69; padding:2px 5px"><?php echo smartyTranslate(array('s'=>'Use a product from the list'),$_smarty_tpl);?>
<div class="separation"></div></div>');
									return value;
								},
								parse: function(data) {
									var mytab = new Array();
									for (var i = 0; i < data.length; i++)
										mytab[mytab.length] = { data: data[i], value: data[i].name };
									return mytab;
								},
								extraParams: {
									ajax: 1,
									action: 'checkProductName',
									id_lang: <?php echo $_smarty_tpl->tpl_vars['id_lang']->value;?>

								}
							}
						)
						.result(function(event, data, formatted) {
							// keep the searched term in the input
							$('#name_<?php echo $_smarty_tpl->tpl_vars['id_lang']->value;?>
').val(search_term);
							jConfirm('<?php echo smartyTranslate(array('s'=>'Do you want to use this product?'),$_smarty_tpl);?>
&nbsp;<strong>'+data.name+'</strong>', '<?php echo smartyTranslate(array('s'=>'Confirmation'),$_smarty_tpl);?>
', function(confirm){
								if (confirm == true)
									document.location.href = '<?php echo $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminProducts',true);?>
&updateproduct&id_product='+data.id_product;
								else
									return false;
							});
						});
				});
		<?php }?>
	</script>

	<?php if (isset($_smarty_tpl->tpl_vars['display_common_field']->value)&&$_smarty_tpl->tpl_vars['display_common_field']->value) {?>
	<div class="alert alert-warning" style="display: block"><?php echo smartyTranslate(array('s'=>'Warning, if you change the value of fields with an orange bullet %s, the value will be changed for all other shops for this product','sprintf'=>$_smarty_tpl->tpl_vars['bullet_common_field']->value),$_smarty_tpl);?>
</div>
	<?php }?>

	<?php echo $_smarty_tpl->getSubTemplate ("controllers/products/multishop/check_fields.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product_tab'=>"Informations"), 0);?>


	<div class="form-group">
		<label class="control-label col-lg-1" for="simple_product">
			<?php echo $_smarty_tpl->tpl_vars['bullet_common_field']->value;?>
 <?php echo smartyTranslate(array('s'=>'Type'),$_smarty_tpl);?>

		</label>
		<div class="col-lg-3">
			<div class="radio">
				<label for="simple_product">
					<input type="radio" name="type_product" id="simple_product" value="<?php echo Product::PTYPE_SIMPLE;?>
" <?php if ($_smarty_tpl->tpl_vars['product_type']->value==Product::PTYPE_SIMPLE) {?>checked="checked"<?php }?> >
					<?php echo smartyTranslate(array('s'=>'Standard product'),$_smarty_tpl);?>
</label>
			</div>
			<div class="radio">
				<label for="pack_product">
					<input type="radio" name="type_product" <?php if ($_smarty_tpl->tpl_vars['is_in_pack']->value) {?>disabled="disabled"<?php }?> id="pack_product" value="<?php echo Product::PTYPE_PACK;?>
" <?php if ($_smarty_tpl->tpl_vars['product_type']->value==Product::PTYPE_PACK) {?>checked="checked"<?php }?> > <?php echo smartyTranslate(array('s'=>'Pack of existing products'),$_smarty_tpl);?>
</label>
			</div>
			<div class="radio">
				<label for="virtual_product">
					<input type="radio" name="type_product" id="virtual_product" <?php if ($_smarty_tpl->tpl_vars['is_in_pack']->value) {?>disabled="disabled"<?php }?> value="<?php echo Product::PTYPE_VIRTUAL;?>
" <?php if ($_smarty_tpl->tpl_vars['product_type']->value==Product::PTYPE_VIRTUAL) {?>checked="checked"<?php }?> >
					<?php echo smartyTranslate(array('s'=>'Virtual product (services, booking, downloadable products, etc.)'),$_smarty_tpl);?>
</label> 
			</div>
			<div class="row row-padding-top">
				<div id="warn_virtual_combinations" class="alert alert-warning" style="display:none"><?php echo smartyTranslate(array('s'=>'You cannot use combinations with a virtual product.'),$_smarty_tpl);?>
</div>
				<div id="warn_pack_combinations" class="alert alert-warning" style="display:none"><?php echo smartyTranslate(array('s'=>'You cannot use combinations with a pack.'),$_smarty_tpl);?>
</div>
			</div>
		</div>

		<span><?php echo $_smarty_tpl->getSubTemplate ("controllers/products/multishop/checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('field'=>"visibility",'type'=>"default"), 0);?>
</span>
		<label class="control-label col-lg-2" for="visibility">
			<?php echo smartyTranslate(array('s'=>'Visibility'),$_smarty_tpl);?>

		</label>
		<div class="col-lg-2">
			<select name="visibility" id="visibility">
				<option value="both" <?php if ($_smarty_tpl->tpl_vars['product']->value->visibility=='both') {?>selected="selected"<?php }?> ><?php echo smartyTranslate(array('s'=>'Everywhere'),$_smarty_tpl);?>
</option>
				<option value="catalog" <?php if ($_smarty_tpl->tpl_vars['product']->value->visibility=='catalog') {?>selected="selected"<?php }?> ><?php echo smartyTranslate(array('s'=>'Catalog only'),$_smarty_tpl);?>
</option>
				<option value="search" <?php if ($_smarty_tpl->tpl_vars['product']->value->visibility=='search') {?>selected="selected"<?php }?> ><?php echo smartyTranslate(array('s'=>'Search only'),$_smarty_tpl);?>
</option>
				<option value="none" <?php if ($_smarty_tpl->tpl_vars['product']->value->visibility=='none') {?>selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>'Nowhere'),$_smarty_tpl);?>
</option>
			</select>
		</div>

	</div>

	<div id="product-pack-container" <?php if ($_smarty_tpl->tpl_vars['product_type']->value!=Product::PTYPE_PACK) {?>style="display:none"<?php }?>></div>

	<hr />

	<?php if (isset($_smarty_tpl->tpl_vars['avviso_doppio']->value)) {?>
		<div class="col-lg-12">
			<div class="row">
				<div class="alert alert-danger"><?php echo $_smarty_tpl->tpl_vars['avviso_doppio']->value;?>
</div> 
			</div>
		</div>
	<?php }?>

	
	<div class="row">
		
		<div class="col-lg-6">
			<div id="product-informations-left" class="panel product-tab">
				<input type="hidden" name="submitted_tabs[]" value="informations_left" />

				<div class="form-group">
					<label class="control-label col-lg-3 required" for="name_<?php echo $_smarty_tpl->tpl_vars['id_lang']->value;?>
">
						<span class="label-tooltip" data-toggle="tooltip" title="<?php echo smartyTranslate(array('s'=>'The public name for this product.'),$_smarty_tpl);?>
 <?php echo smartyTranslate(array('s'=>'Invalid characters:'),$_smarty_tpl);?>
 &lt;&gt;;=#{}<?php echo smartyTranslate(array('s'=>'Keep within 55 characters'),$_smarty_tpl);?>
">
							<?php echo smartyTranslate(array('s'=>'Name'),$_smarty_tpl);?>

						</span>
					</label>
					<div class="col-lg-9">
						<?php ob_start();?><?php if (!$_smarty_tpl->tpl_vars['product']->value->id||Configuration::get('PS_FORCE_FRIENDLY_PRODUCT')) {?><?php echo "copy2friendlyUrl";?><?php }?><?php $_tmp1=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("controllers/products/input_text_lang.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('languages'=>$_smarty_tpl->tpl_vars['languages']->value,'input_class'=>((string)$_smarty_tpl->tpl_vars['class_input_ajax']->value).$_tmp1." updateCurrentText",'input_value'=>$_smarty_tpl->tpl_vars['product']->value->name,'input_name'=>"name",'required'=>true), 0);?>

					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-3 " for="reference">
						<span>
							Codice eSolver
						</span>
					</label>
					<div class="col-lg-5">
						<input required class="textbox-fix" type="text" id="reference" name="reference" value="<?php echo smarty_modifier_htmlentitiesUTF8($_smarty_tpl->tpl_vars['product']->value->reference);?>
">
					</div>
					<div class="col-md-2">
						<input type="button" value="Verifica" name="verifica_esolver" onclick='$("#checkid").html("Attendi..."); $.get("checkreference.php",{ cmd: "checkref", id_product: "<?php echo $_smarty_tpl->tpl_vars['product']->value->id;?>
", reference: $("#reference").val() } ,function(data){  $("#checkid").html(data); });'>
						<span style="font: bold 12px;" id="checkid"></span> 
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-3 " for="supplier_reference">
						<span>
							Codice SKU
						</span>
					</label>
					<div class="col-lg-5">
						<input class="textbox-fix" type="text" id="supplier_reference" name="supplier_reference" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->supplier_reference;?>
">
					</div>
					<div class="col-md-2">
						<input type="button" value="Verifica" name="verifica_sku" onclick='$("#checkidsup").html("Attendi..."); $.get("checkreference.php",{ cmd: "checksupref", id_product: "<?php echo $_smarty_tpl->tpl_vars['product']->value->id;?>
", supplier_reference: $("#supplier_reference").val() } ,function(data){  $("#checkidsup").html(data); });'>
						<span style="font: bold 12px;" id="checkidsup"></span> 
					</div>
					<div class="col-md-2">
						( <input type="checkbox" <?php if ($_smarty_tpl->tpl_vars['product']->value->reference=='') {?> checked="checked" <?php }?> name="sku_provvisorio"> Prov. )
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-3 " for="ean13">
						<span class="label-tooltip" data-toggle="tooltip"
							title="<?php echo smartyTranslate(array('s'=>'This type of product code is specific to Europe and Japan, but is widely used internationally. It is a superset of the UPC code: all products marked with an EAN will be accepted in North America.'),$_smarty_tpl);?>
">
							<?php echo $_smarty_tpl->tpl_vars['bullet_common_field']->value;?>
 <?php echo smartyTranslate(array('s'=>'EAN-13 or JAN barcode'),$_smarty_tpl);?>

						</span>
					</label>
					<div class="col-lg-5">
						<input class="textbox-fix" maxlength="13" type="text" id="ean13" name="ean13" value="<?php echo smarty_modifier_htmlentitiesUTF8($_smarty_tpl->tpl_vars['product']->value->ean13);?>
">
					</div>
					<div class="col-md-4">
						Fuori produzione? <input type="checkbox" id="fuori_produzione" name="fuori_produzione" value="1" <?php if ($_smarty_tpl->tpl_vars['product']->value->fuori_produzione) {?> checked="checked" <?php }?>>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3" for="upc">
						<span>
							Rif. fornitore 1
						</span>
					</label>
					<div class="col-md-3">
						<input class="textbox-fix" type="text" id="upc" name="upc" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->upc, ENT_QUOTES, 'UTF-8', true);?>
">
					</div>
					<label class="control-label col-md-3" for="rif_fornitore_2">
						<span>
							Rif. fornitore 2
						</span>
					</label>
					<div class="col-md-3">
						<input class="textbox-fix" type="text" id="rif_fornitore_2" name="rif_fornitore_2" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->rif_fornitore_2, ENT_QUOTES, 'UTF-8', true);?>
">
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-lg-3" for="asin">
						<span>
							ASIN
						</span>
					</label>
					<div class="col-lg-5">
						<input class="textbox-fix" type="text" id="asin" name="asin" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->asin;?>
">
					</div>
					<div class="col-md-4">
						Trasp. gratis Amazon.it? 
						<input type="checkbox" id="amazon_free_shipping" name="amazon_free_shipping" value="1" <?php if ($_smarty_tpl->tpl_vars['product']->value->amazon_free_shipping) {?> checked="checked" <?php }?>>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-3">
						<span>
							Dimensioni (cm)
						</span>
					</label>
					<div class="col-sm-2">
						<input class="textbox-fix" type="text" id="width" name="width" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->width;?>
" onkeyup="if (isArrowKey(event)) return; this.value = this.value.replace(/,/g, '.');">
					</div>
					<div class="col-sm-1" style="padding-top: 5px;">
						Larg.
					</div>
					<div class="col-sm-2">
						<input class="textbox-fix" type="text" id="height" name="height" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->height;?>
" onkeyup="if (isArrowKey(event)) return; this.value = this.value.replace(/,/g, '.');">
					</div>
					<div class="col-sm-1" style="padding-top: 5px;">
						Lung.
					</div>
					<div class="col-sm-2">
						<input class="textbox-fix" type="text" id="depth" name="depth" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->depth;?>
" onkeyup="if (isArrowKey(event)) return; this.value = this.value.replace(/,/g, '.');">
					</div>
					<div class="col-sm-1" style="padding-top: 5px;">
						Alt.
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-3 " for="weight">
						<span>
							Peso
						</span>
					</label>
					<div class="col-sm-2">
						<input class="textbox-fix" type="text" id="weight" name="weight" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->weight;?>
" onkeyup="if (isArrowKey(event)) return; this.value = this.value.replace(/,/g, '.');">
					</div>
					<div class="col-sm-1" style="padding-top: 5px;">
						g
					</div>
					<div class="col-sm-4" style="padding-top: 5px;">
						Scorta min. EZ / Amz
					</div>
					<div class="col-sm-2">
						<div class="row">
							<div class="col-sm-5">
								<input class="textbox-fix" type="text" id="scorta_minima" name="scorta_minima" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->scorta_minima;?>
">
							</div>
							<div class="col-sm-1" style="padding-top: 5px; text-align: center;"> 
								/ 
							</div>
							<div class="col-sm-5">
								<input class="textbox-fix" type="text" id="scorta_minima_amazon" name="scorta_minima_amazon" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->scorta_minima_amazon;?>
">
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">

					<div class="col-sm-6">
						Servizio con canone ricorrente? <input type="checkbox" id="prodotto_con_canone" name="prodotto_con_canone" value="1" <?php if ($_smarty_tpl->tpl_vars['product']->value->prodotto_con_canone) {?> checked="checked" <?php }?> >
					</div>
					
				</div>

				<?php if ($_smarty_tpl->tpl_vars['canonical']->value) {?>
				<div class="row">
					<div class="col-sm-12">
						<p><strong class="rosso">ATTENZIONE</strong>: questo prodotto è il canonical di altri prodotti. Questi sono i codici dei prodotti:</p>
						<?php  $_smarty_tpl->tpl_vars['figlio'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['figlio']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['padrecanonical']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['figlio']->key => $_smarty_tpl->tpl_vars['figlio']->value) {
$_smarty_tpl->tpl_vars['figlio']->_loop = true;
?> 
							<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminProducts');?>
&updateproduct&id_product=<?php echo $_smarty_tpl->tpl_vars['figlio']->value['id_product'];?>
&id_category=<?php echo $_smarty_tpl->tpl_vars['figlio']->value['id_category_default'];?>
" target="_blank" style="text-decoration:underline"><?php echo $_smarty_tpl->tpl_vars['figlio']->value['reference'];?>
</a> 
						<?php } ?>
						<p><strong>FARE ATTENZIONE se si vuole cancellare questo prodotto, il canonical non esisterebbe più.</p>
					</div>
				</div>
				<?php }?>

				<div class="row">
					<div class="col-sm-6">
						Data inserimento <span class="thick"><?php echo $_smarty_tpl->tpl_vars['product']->value->date_add;?>
</span>
					</div>

					<div class="col-sm-6">
						Ultimo agg. <span class="thick"><?php echo $_smarty_tpl->tpl_vars['product']->value->date_upd;?>
</span>
					</div>
				</div>

			</div>
		</div>
		
		<div class="col-lg-6">
			<div id="product-informations-right" class="panel product-tab">
				<input type="hidden" name="submitted_tabs[]" value="informations_right" />

				<div class="form-group">
					<label class="control-label col-sm-3 " for="active">
						<span>
							Status
						</span>
					</label>
					<div class="col-sm-2">
						<input type="radio" name="active" id="active_on" onclick="toggleDraftWarning(false);showOptions(true);" value="1"<?php if ($_smarty_tpl->tpl_vars['product']->value->active==1) {?> checked="checked"<?php }?>> <?php echo smartyTranslate(array('s'=>"Enabled"),$_smarty_tpl);?>

					</div>
					<div class="col-sm-2">
						<input type="radio" name="active" id="active_off" onclick="toggleDraftWarning(true);showOptions(false);" value="0"<?php if ($_smarty_tpl->tpl_vars['product']->value->active==0) {?> checked="checked"<?php }?>> <?php echo smartyTranslate(array('s'=>"Disabled"),$_smarty_tpl);?>

					</div>
					<div class="col-sm-2">
						<input type="radio" name="active" id="active_old" value="2"<?php if ($_smarty_tpl->tpl_vars['product']->value->active==2) {?> checked="checked"<?php }?>> <?php echo smartyTranslate(array('s'=>"Old"),$_smarty_tpl);?>

					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-3 " for="condition">
						<span>
							Condizione
						</span>
					</label>
					<div class="col-sm-9">
						<select name="condition" id="condition">
							<option value="new" <?php if ($_smarty_tpl->tpl_vars['product']->value->condition=='new') {?>selected="selected"<?php }?> ><?php echo smartyTranslate(array('s'=>'New'),$_smarty_tpl);?>
</option>
							<option value="used" <?php if ($_smarty_tpl->tpl_vars['product']->value->condition=='used') {?>selected="selected"<?php }?> ><?php echo smartyTranslate(array('s'=>'Used'),$_smarty_tpl);?>
</option>
							<option value="refurbished" <?php if ($_smarty_tpl->tpl_vars['product']->value->condition=='refurbished') {?>selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>'Refurbished'),$_smarty_tpl);?>
</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-3 " for="dispo_type">
						<span>
							Tipo disponibilità
						</span>
					</label>
					<div class="col-sm-9">
						<select name="dispo_type" id="dispo_type">							
							<option value="0" <?php if ($_smarty_tpl->tpl_vars['product']->value->dispo_type==0) {?>selected="selected"<?php }?>>Tutto</option>
							<option value="1" <?php if ($_smarty_tpl->tpl_vars['product']->value->dispo_type==1) {?>selected="selected"<?php }?>>Solo magazzino EZ</option>
							<option value="2" <?php if ($_smarty_tpl->tpl_vars['product']->value->dispo_type==2) {?>selected="selected"<?php }?>>Fornitore principale + Mag. EZ</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-3 " for="date_available">
						<span>
							Disponibile da
						</span>
					</label>
					<div class="col-sm-9">
						<input type="date" name="date_available" id="date_available" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->date_available;?>
">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-3 " for="data_arrivo">
						<span>
							Data arrivo Allnet
						</span>
					</label>
					<div class="col-sm-9">
						<input type="date" name="data_arrivo" id="data_arrivo" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->data_arrivo;?>
">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-3" for="id_manufacturer"><?php echo smartyTranslate(array('s'=>'Manufacturer'),$_smarty_tpl);?>
</label>
					<div class="col-sm-6">
						<select name="id_manufacturer" id="id_manufacturer" onchange="/*getSupplierByManufacturer();*/ getMargins()">
							<option value="0">- <?php echo smartyTranslate(array('s'=>'Choose (optional)'),$_smarty_tpl);?>
 -</option>
							<?php if ($_smarty_tpl->tpl_vars['product']->value->id_manufacturer) {?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['product']->value->id_manufacturer;?>
" selected="selected" data-margin-cli="<?php echo $_smarty_tpl->tpl_vars['data_margin_cli']->value;?>
" data-margin-riv="<?php echo $_smarty_tpl->tpl_vars['data_margin_riv']->value;?>
" data-supplier="<?php echo $_smarty_tpl->tpl_vars['data_supplier']->value;?>
" data-othersuppliers="<?php echo $_smarty_tpl->tpl_vars['data_othersuppliers']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['product']->value->manufacturer_name;?>
</option>
							<?php }?>
							<option disabled="disabled">-</option>
						</select>
					</div>
					<div class="col-sm-1">
					</div>
					<div class="col-sm-2">
						<a class="btn btn-link bt-icon confirm_leave" style="margin-bottom:0px" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminManufacturers'), ENT_QUOTES, 'UTF-8', true);?>
&amp;addmanufacturer">
							<i class="icon-plus-sign"></i> <?php echo smartyTranslate(array('s'=>'Create'),$_smarty_tpl);?>
 <i class="icon-external-link-sign"></i>
						</a>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-3" for="id_supplier"><?php echo smartyTranslate(array('s'=>'Supplier'),$_smarty_tpl);?>
</label>
					<div class="col-sm-6">
						<select name="id_supplier" id="id_supplier">
							<option value="0">- <?php echo smartyTranslate(array('s'=>'Choose (optional)'),$_smarty_tpl);?>
 -</option>
							<?php if ($_smarty_tpl->tpl_vars['product']->value->id_supplier) {?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['product']->value->id_supplier;?>
" selected="selected"><?php echo $_smarty_tpl->tpl_vars['product']->value->supplier_name;?>
</option>
							<?php }?>
							<option disabled="disabled">-</option>
						</select>
					</div>
					<div class="col-sm-1">
					</div>
					<div class="col-sm-2">
						<a class="btn btn-link bt-icon confirm_leave" style="margin-bottom:0px" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminSuppliers'), ENT_QUOTES, 'UTF-8', true);?>
&amp;addsupplier">
							<i class="icon-plus-sign"></i> <?php echo smartyTranslate(array('s'=>'Create'),$_smarty_tpl);?>
 <i class="icon-external-link-sign"></i>
						</a>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-sm-3 " for="altri-fornitori">
						<span>
							Altri fornitori
						</span>
					</label>
					
					<div id="altri-fornitori">
						<div class="row">
							<label class="control-label col-sm-3">
							</label>
							<div class="form-group col-sm-9">
								<select id="other_suppliers" name="other_suppliers[]" class="form-control textbox-fix select2mul" multiple="multiple">
									<option value="0">-- Seleziona --</option>
								<?php  $_smarty_tpl->tpl_vars['row'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['row']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fornitori']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['row']->key => $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['row']->key;
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['row']->value["id_supplier"];?>
"<?php if (in_array($_smarty_tpl->tpl_vars['row']->value["id_supplier"],$_smarty_tpl->tpl_vars['altri_fornitori']->value)) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['row']->value["name"];?>
</option>
								<?php } ?>
								</select>
							</div>
						</div>
					</div>
				</div>

				
				

				<div class="form-group">
					<label class="control-label col-sm-3 " for="note_fornitore">
						<span>
							Note forn.
						</span>
					</label>
					<div class="col-sm-9">
						<textarea name="note_fornitore" id="note_fornitore"><?php echo $_smarty_tpl->tpl_vars['product']->value->note_fornitore;?>
</textarea>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	

	

	

	

	

	<hr/>

	<div class="form-group" style="display:none">
		<div class="col-lg-1"><span class="pull-right"><?php echo $_smarty_tpl->getSubTemplate ("controllers/products/multishop/checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('field'=>"active",'type'=>"radio",'onclick'=>''), 0);?>
</span></div>
		<label class="control-label col-lg-2">
			<?php echo smartyTranslate(array('s'=>'Enabled'),$_smarty_tpl);?>

		</label>
		<div class="col-lg-9">
			<span class="switch prestashop-switch fixed-width-lg">
				<input onclick="toggleDraftWarning(false);showOptions(true);showRedirectProductOptions(false);" type="radio" name="active_originale" id="active_on_originale" value="1" <?php if ($_smarty_tpl->tpl_vars['product']->value->active||!$_smarty_tpl->tpl_vars['product']->value->isAssociatedToShop()) {?>checked="checked" <?php }?> />
				<label for="active_on_originale" class="radioCheck">
					<?php echo smartyTranslate(array('s'=>'Yes'),$_smarty_tpl);?>

				</label>
				<input onclick="toggleDraftWarning(true);showOptions(false);showRedirectProductOptions(true);"  type="radio" name="active_originale" id="active_off_originale" value="0" <?php if (!$_smarty_tpl->tpl_vars['product']->value->active&&$_smarty_tpl->tpl_vars['product']->value->isAssociatedToShop()) {?>checked="checked"<?php }?> />
				<label for="active_off_originale" class="radioCheck">
					<?php echo smartyTranslate(array('s'=>'No'),$_smarty_tpl);?>

				</label>
				<a class="slide-button btn"></a>
			</span>
		</div>
	</div>

	<div class="form-group redirect_product_options" style="display:none">
		<div class="col-lg-1"><span class="pull-right"><?php echo $_smarty_tpl->getSubTemplate ("controllers/products/multishop/checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('field'=>"redirect_type",'type'=>"radio",'onclick'=>''), 0);?>
</span></div>
		<label class="control-label col-lg-2" for="redirect_type">
			<?php echo smartyTranslate(array('s'=>'Redirect when disabled'),$_smarty_tpl);?>

		</label>
		<div class="col-lg-5">
			<select name="redirect_type" id="redirect_type">
				<option value="404" <?php if ($_smarty_tpl->tpl_vars['product']->value->redirect_type=='404') {?> selected="selected" <?php }?>><?php echo smartyTranslate(array('s'=>'No redirect (404)'),$_smarty_tpl);?>
</option>
				<option value="301" <?php if ($_smarty_tpl->tpl_vars['product']->value->redirect_type=='301') {?> selected="selected" <?php }?>><?php echo smartyTranslate(array('s'=>'Redirected permanently (301)'),$_smarty_tpl);?>
</option>
				<option value="302" <?php if ($_smarty_tpl->tpl_vars['product']->value->redirect_type=='302') {?> selected="selected" <?php }?>><?php echo smartyTranslate(array('s'=>'Redirected temporarily (302)'),$_smarty_tpl);?>
</option>
			</select>
		</div>
	</div>
	<div class="form-group redirect_product_options" style="display:none">
		<div class="col-lg-9 col-lg-offset-3">
			<div class="alert alert-info">
				<?php echo smartyTranslate(array('s'=>'404 Not Found = Do not redirect and display a 404 page.'),$_smarty_tpl);?>
<br/>
				<?php echo smartyTranslate(array('s'=>'301 Moved Permanently = Permanently display another product instead.'),$_smarty_tpl);?>
<br/>
				<?php echo smartyTranslate(array('s'=>'302 Moved Temporarily = Temporarily display another product instead.'),$_smarty_tpl);?>

			</div>
		</div>
	</div>

	<div class="form-group redirect_product_options redirect_product_options_product_choise" style="display:none">
		<div class="col-lg-1"><span class="pull-right"><?php echo $_smarty_tpl->getSubTemplate ("controllers/products/multishop/checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('field'=>"id_product_redirected",'type'=>"radio",'onclick'=>''), 0);?>
</span></div>
		<label class="control-label col-lg-2" for="related_product_autocomplete_input">
			<?php echo smartyTranslate(array('s'=>'Related product:'),$_smarty_tpl);?>

		</label>
		<div class="col-lg-7">
			<input type="hidden" value="" name="id_product_redirected" />

			<div class="input-group">
				<input type="text" id="related_product_autocomplete_input" name="related_product_autocomplete_input" autocomplete="off" class="ac_input" />
				<span class="input-group-addon"><i class="icon-search"></i></span>
			</div>

			<div class="form-control-static">
				<span id="related_product_name"><i class="icon-warning-sign"></i>&nbsp;<?php echo smartyTranslate(array('s'=>'No related product.'),$_smarty_tpl);?>
</span>
				<span id="related_product_remove" style="display:none">
					<a class="btn btn-default" href="#" onclick="removeRelatedProduct(); return false" id="related_product_remove_link">
						<i class="icon-remove text-danger"></i>
					</a>
				</span>
			</div>

		</div>
		<script type="text/javascript">
			var no_related_product = "<?php echo smartyTranslate(array('s'=>'No related product'),$_smarty_tpl);?>
";
			var id_product_redirected = <?php echo intval($_smarty_tpl->tpl_vars['product']->value->id_product_redirected);?>
;
			var product_name_redirected = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_name_redirected']->value, ENT_QUOTES, 'UTF-8', true);?>
";
		</script>
	</div> 

	

	<div id="product_options" class="form-group" style="display:none">
		<div class="col-lg-12">
			<div class="form-group">
				<div class="col-lg-1">
					<span class="pull-right">
						<?php if (isset($_smarty_tpl->tpl_vars['display_multishop_checkboxes']->value)&&$_smarty_tpl->tpl_vars['display_multishop_checkboxes']->value) {?>
							<?php echo $_smarty_tpl->getSubTemplate ("controllers/products/multishop/checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('only_checkbox'=>"true",'field'=>"available_for_order",'type'=>"default"), 0);?>

							<?php echo $_smarty_tpl->getSubTemplate ("controllers/products/multishop/checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('only_checkbox'=>"true",'field'=>"show_price",'type'=>"show_price"), 0);?>

							<?php echo $_smarty_tpl->getSubTemplate ("controllers/products/multishop/checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('only_checkbox'=>"true",'field'=>"online_only",'type'=>"default"), 0);?>

						<?php }?>
					</span>
				</div>
				<label class="control-label col-lg-2" for="available_for_order">
					<?php echo smartyTranslate(array('s'=>'Options'),$_smarty_tpl);?>

				</label>
				<div class="col-lg-9">
					<div class="checkbox">
						<label for="available_for_order">
							<input type="checkbox" name="available_for_order" id="available_for_order" value="1" <?php if ($_smarty_tpl->tpl_vars['product']->value->available_for_order) {?>checked="checked"<?php }?> >
							<?php echo smartyTranslate(array('s'=>'Available for order'),$_smarty_tpl);?>
</label>
					</div>
					<div class="checkbox">
						<label for="show_price">
							<input type="checkbox" name="show_price" id="show_price" value="1" <?php if ($_smarty_tpl->tpl_vars['product']->value->show_price) {?>checked="checked"<?php }?> <?php if ($_smarty_tpl->tpl_vars['product']->value->available_for_order) {?>disabled="disabled"<?php }?> >
							<?php echo smartyTranslate(array('s'=>'Show price'),$_smarty_tpl);?>
</label>
					</div>
					<div class="checkbox">
						<label for="online_only">
							<input type="checkbox" name="online_only" id="online_only" value="1" <?php if ($_smarty_tpl->tpl_vars['product']->value->online_only) {?>checked="checked"<?php }?> >
							<?php echo smartyTranslate(array('s'=>'Online only (not sold in your retail store)'),$_smarty_tpl);?>
</label>
					</div>
				</div>
			</div>
			
		</div>
	</div>

	
	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right"><?php echo $_smarty_tpl->getSubTemplate ("controllers/products/multishop/checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('field'=>"description_short",'type'=>"tinymce",'multilang'=>"true"), 0);?>
</span></div>
		<label class="control-label col-lg-2" for="description_short_<?php echo $_smarty_tpl->tpl_vars['id_lang']->value;?>
">
			<span class="label-tooltip" data-toggle="tooltip" title="<?php echo smartyTranslate(array('s'=>'Appears in the product list(s), and at the top of the product page.'),$_smarty_tpl);?>
">
				<?php echo smartyTranslate(array('s'=>'Short description'),$_smarty_tpl);?>

			</span>
		</label>
		<div class="col-lg-9">
			<?php echo $_smarty_tpl->getSubTemplate ("controllers/products/textarea_lang.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('languages'=>$_smarty_tpl->tpl_vars['languages']->value,'input_name'=>'description_short','class'=>"autoload_rte",'input_value'=>$_smarty_tpl->tpl_vars['product']->value->description_short,'max'=>$_smarty_tpl->tpl_vars['PS_PRODUCT_SHORT_DESC_LIMIT']->value), 0);?>

		</div>
	</div>

	
	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right"><?php echo $_smarty_tpl->getSubTemplate ("controllers/products/multishop/checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('field'=>"cat_homepage",'type'=>"default",'multilang'=>"true"), 0);?>
</span></div>
		<label class="control-label col-lg-2" for="cat_homepage_<?php echo $_smarty_tpl->tpl_vars['id_lang']->value;?>
">
			<span class="label-tooltip" data-toggle="tooltip"
				title="<?php echo smartyTranslate(array('s'=>'Forbidden characters: <>;=#{}'),$_smarty_tpl);?>
">
				<?php echo smartyTranslate(array('s'=>'Home page category'),$_smarty_tpl);?>

			</span>
		</label>
		<div class="col-lg-9">
			<?php echo $_smarty_tpl->getSubTemplate ("controllers/products/input_text_lang.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('languages'=>$_smarty_tpl->tpl_vars['languages']->value,'input_name'=>'cat_homepage','input_value'=>$_smarty_tpl->tpl_vars['product']->value->cat_homepage,'maxchar'=>100), 0);?>

		</div>
	</div>
	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right"><?php echo $_smarty_tpl->getSubTemplate ("controllers/products/multishop/checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('field'=>"desc_homepage",'type'=>"default",'multilang'=>"true"), 0);?>
</span></div>
		<label class="control-label col-lg-2" for="desc_homepage_<?php echo $_smarty_tpl->tpl_vars['id_lang']->value;?>
">
			<span class="label-tooltip" data-toggle="tooltip"
				title="<?php echo smartyTranslate(array('s'=>'Forbidden characters: <>;=#{}'),$_smarty_tpl);?>
">
				<?php echo smartyTranslate(array('s'=>'Home page description'),$_smarty_tpl);?>

			</span>
		</label>
		<div class="col-lg-9">
			<?php echo $_smarty_tpl->getSubTemplate ("controllers/products/textarea_lang.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('languages'=>$_smarty_tpl->tpl_vars['languages']->value,'input_name'=>'desc_homepage','input_value'=>$_smarty_tpl->tpl_vars['product']->value->desc_homepage,'max'=>110), 0);?>

		</div>
	</div>
	

	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right"><?php echo $_smarty_tpl->getSubTemplate ("controllers/products/multishop/checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('field'=>"description",'type'=>"tinymce",'multilang'=>"true"), 0);?>
</span></div>
		<label class="control-label col-lg-2" for="description_<?php echo $_smarty_tpl->tpl_vars['id_lang']->value;?>
">
			<span class="label-tooltip" data-toggle="tooltip"
				title="<?php echo smartyTranslate(array('s'=>'Appears in the body of the product page.'),$_smarty_tpl);?>
">
				<?php echo smartyTranslate(array('s'=>'Description'),$_smarty_tpl);?>

			</span>
		</label>
		<div class="col-lg-9">
			<?php echo $_smarty_tpl->getSubTemplate ("controllers/products/textarea_lang.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('languages'=>$_smarty_tpl->tpl_vars['languages']->value,'input_name'=>'description','class'=>"autoload_rte",'input_value'=>$_smarty_tpl->tpl_vars['product']->value->description), 0);?>

		</div>
	</div>
	<?php if ($_smarty_tpl->tpl_vars['images']->value) {?>
	<div class="form-group">
		<div class="col-lg-9 col-lg-offset-3">
			<div class="alert alert-info">
				<?php $_smarty_tpl->_capture_stack[0][] = array('default', null, null); ob_start(); ?><a class="addImageDescription" href="javascript:void(0);"><?php echo smartyTranslate(array('s'=>'Click here'),$_smarty_tpl);?>
</a><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
				<?php echo smartyTranslate(array('s'=>'Would you like to add an image in your description? %s and paste the given tag in the description.','sprintf'=>Smarty::$_smarty_vars['capture']['default']),$_smarty_tpl);?>

			</div>
		</div>
	</div>
	<div id="createImageDescription" class="panel" style="display:none">
		<div class="form-group">
			<label class="control-label col-lg-3" for="smallImage_0"><?php echo smartyTranslate(array('s'=>'Select your image'),$_smarty_tpl);?>
</label>
			<div class="col-lg-9">
				<ul class="list-inline">
					<?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value) {
$_smarty_tpl->tpl_vars['image']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['image']->key;
?>
					<li>
						<input type="radio" name="smallImage" id="smallImage_<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['image']->value['id_image'];?>
" <?php if ($_smarty_tpl->tpl_vars['key']->value==0) {?>checked="checked"<?php }?> >
						<label for="smallImage_<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" >
							<img src="<?php echo $_smarty_tpl->tpl_vars['image']->value['src'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['image']->value['legend'];?>
" />
						</label>
					</li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3" for="leftRight_1"><?php echo smartyTranslate(array('s'=>'Position'),$_smarty_tpl);?>
</label>
			<div class="col-lg-5">
				<p class="checkbox">
					<input type="radio" name="leftRight" id="leftRight_1" value="left" checked>
					<label for="leftRight_1" ><?php echo smartyTranslate(array('s'=>'left'),$_smarty_tpl);?>
</label>
				</p>
				<p class="checkbox">
					<input type="radio" name="leftRight" id="leftRight_2" value="right">
					<label for="leftRight_2" ><?php echo smartyTranslate(array('s'=>'right'),$_smarty_tpl);?>
</label>
				</p>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3" for="imageTypes_0"><?php echo smartyTranslate(array('s'=>'Select the type of picture'),$_smarty_tpl);?>
</label>
			<div class="col-lg-5">
				<?php  $_smarty_tpl->tpl_vars['type'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['type']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['imagesTypes']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['type']->key => $_smarty_tpl->tpl_vars['type']->value) {
$_smarty_tpl->tpl_vars['type']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['type']->key;
?>
				<p class="checkbox">
					<input type="radio" name="imageTypes" id="imageTypes_<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['type']->value['name'];?>
" <?php if ($_smarty_tpl->tpl_vars['key']->value==0) {?>checked="checked"<?php }?>>
					<label for="imageTypes_<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" >
						<?php echo $_smarty_tpl->tpl_vars['type']->value['name'];?>
 <span><?php echo smartyTranslate(array('s'=>'%dpx by %dpx','sprintf'=>array($_smarty_tpl->tpl_vars['type']->value['width'],$_smarty_tpl->tpl_vars['type']->value['height'])),$_smarty_tpl);?>
</span>
					</label>
				</p>
				<?php } ?>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3" for="resultImage">
				<span class="label-tooltip" data-toggle="tooltip"
				title="<?php echo smartyTranslate(array('s'=>'The tag to copy/paste into the description.'),$_smarty_tpl);?>
">
					<?php echo smartyTranslate(array('s'=>'Image tag to insert'),$_smarty_tpl);?>

				</span>
			</label>
			<div class="col-lg-4">
				<input type="text" id="resultImage" name="resultImage" />
			</div>
			<p class="help-block"></p>
		</div>
	</div>
	<?php }?>

	
	<div class="form-group">
		<div class="col-lg-3"><span class="pull-right"></span></div>
		<div class="col-lg-7">
			<button type="button" class="btn btn-default" style="background-color:black; color:white" data-toggle="collapse" data-target="#collapse_amz" aria-expanded="false" aria-controls="collapse_amz"><?php echo smartyTranslate(array('s'=>'Show/Hide Amazon'),$_smarty_tpl);?>
</button>
		</div>
	</div>

	<div class="collapse" id="collapse_amz">

		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right"><?php echo $_smarty_tpl->getSubTemplate ("controllers/products/multishop/checkbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('field'=>"description_amazon",'type'=>"tinymce",'multilang'=>"true"), 0);?>
</span></div>
			<label class="control-label col-lg-2" for="description_amazon_<?php echo $_smarty_tpl->tpl_vars['id_lang']->value;?>
">
				<span>
					<?php echo smartyTranslate(array('s'=>'Amazon Description'),$_smarty_tpl);?>

				</span>
			</label>
			<div class="col-lg-9">
				<?php echo $_smarty_tpl->getSubTemplate ("controllers/products/textarea_lang.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('languages'=>$_smarty_tpl->tpl_vars['languages']->value,'input_name'=>'description_amazon','class'=>"autoload_rte",'input_value'=>$_smarty_tpl->tpl_vars['product']->value->description_amazon), 0);?>

			</div>
		</div>
		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right"></span></div>
			<label class="control-label col-lg-2" for="no_amazon">
				<span>
					<?php echo smartyTranslate(array('s'=>'Motivo mancata pubblicazione su Amazon'),$_smarty_tpl);?>

				</span>
			</label>
			<div class="col-lg-7">
				<input type="text" id="no_amazon" name="no_amazon" value="<?php echo smarty_modifier_htmlentitiesUTF8($_smarty_tpl->tpl_vars['product']->value->no_amazon);?>
" />
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right"></span></div>
			<label class="control-label col-lg-2" for="sku_amazon">
				<span>
					<?php echo smartyTranslate(array('s'=>'Amazon Logistics SKU'),$_smarty_tpl);?>
 
				</span>
			</label>
			<div class="col-lg-3">
				<input type="text" id="sku_amazon" name="sku_amazon" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->sku_amazon;?>
" readonly="readonly" />
			</div>

			<label class="control-label col-lg-1" for="fnsku">
				<span>
					<?php echo smartyTranslate(array('s'=>'FNSKU'),$_smarty_tpl);?>

				</span>
			</label>
			<div class="col-lg-3">
				<input type="text" id="fnsku" name="fnsku" value="<?php echo $_smarty_tpl->tpl_vars['product']->value->fnsku;?>
" readonly="readonly" />
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right"></span></div>
			<label class="control-label col-lg-2" for="punti_di_forza_amazon">
				<span>
					Punti di forza Amazon
				</span>
			</label>
		</div>

		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right"></span></div>
			<label class="control-label col-lg-2" for="punti_di_forza_0">
				<span>
					1
				</span>
			</label>
			<div class="col-lg-7">
				<?php echo $_smarty_tpl->getSubTemplate ("controllers/products/input_text_lang.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('languages'=>$_smarty_tpl->tpl_vars['languages']->value,'input_value'=>$_smarty_tpl->tpl_vars['punti_di_forza']->value[0],'input_name'=>"punti_di_forza_0"), 0);?>

			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right"></span></div>
			<label class="control-label col-lg-2" for="punti_di_forza_1">
				<span>
					2
				</span>
			</label>
			<div class="col-lg-7">
				<?php echo $_smarty_tpl->getSubTemplate ("controllers/products/input_text_lang.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('languages'=>$_smarty_tpl->tpl_vars['languages']->value,'input_value'=>$_smarty_tpl->tpl_vars['punti_di_forza']->value[1],'input_name'=>"punti_di_forza_1"), 0);?>

			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right"></span></div>
			<label class="control-label col-lg-2" for="punti_di_forza_2">
				<span>
					3
				</span>
			</label>
			<div class="col-lg-7">
				<?php echo $_smarty_tpl->getSubTemplate ("controllers/products/input_text_lang.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('languages'=>$_smarty_tpl->tpl_vars['languages']->value,'input_value'=>$_smarty_tpl->tpl_vars['punti_di_forza']->value[2],'input_name'=>"punti_di_forza_2"), 0);?>

			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right"></span></div>
			<label class="control-label col-lg-2" for="punti_di_forza_3">
				<span>
					4
				</span>
			</label>
			<div class="col-lg-7">
				<?php echo $_smarty_tpl->getSubTemplate ("controllers/products/input_text_lang.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('languages'=>$_smarty_tpl->tpl_vars['languages']->value,'input_value'=>$_smarty_tpl->tpl_vars['punti_di_forza']->value[3],'input_name'=>"punti_di_forza_3"), 0);?>

			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right"></span></div>
			<label class="control-label col-lg-2" for="punti_di_forza_4">
				<span>
					5
				</span>
			</label>
			<div class="col-lg-7">
				<?php echo $_smarty_tpl->getSubTemplate ("controllers/products/input_text_lang.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('languages'=>$_smarty_tpl->tpl_vars['languages']->value,'input_value'=>$_smarty_tpl->tpl_vars['punti_di_forza']->value[4],'input_name'=>"punti_di_forza_4"), 0);?>

			</div>
		</div>

	</div>

	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right"></span></div>
		<label class="control-label col-lg-2" for="note">
			<span>
				<?php echo smartyTranslate(array('s'=>"Note"),$_smarty_tpl);?>

			</span>
		</label>
		<div class="col-lg-7">
			<textarea rows="6" cols="100" name="note" id="note"><?php echo $_smarty_tpl->tpl_vars['product']->value->note;?>
</textarea>
		</div>
	</div>

	<?php  $_smarty_tpl->tpl_vars['rowvideos'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['rowvideos']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['rowsvideos']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['rowvideos']->key => $_smarty_tpl->tpl_vars['rowvideos']->value) {
$_smarty_tpl->tpl_vars['rowvideos']->_loop = true;
?>
		<div class="form-group">
			<div class="col-lg-1"><span class="pull-right"></span></div>
			<label class="control-label col-lg-6" for="cancellavideo_<?php echo $_smarty_tpl->tpl_vars['rowvideos']->value["id"];?>
">
				<span> 
					<?php echo smartyTranslate(array('s'=>"Check to delete video"),$_smarty_tpl);?>
 - <a href='<?php echo $_smarty_tpl->tpl_vars['rowvideos']->value["videoc"];?>
' target='_blank'><?php echo $_smarty_tpl->tpl_vars['rowvideos']->value["videoc"];?>
</a>
				</span>
			</label>
			<div class="col-lg-3">
				<input type='checkbox' id='cancellavideo_<?php echo $_smarty_tpl->tpl_vars['rowvideos']->value["id"];?>
' name='cancellavideo[]' value='<?php echo $_smarty_tpl->tpl_vars['rowvideos']->value["id"];?>
' />
			</div>
		</div>
	<?php } ?>

	
	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right"></span></div>
		<label class="control-label col-lg-2" for="video1">
			<span>
				<?php echo smartyTranslate(array('s'=>"Video 1"),$_smarty_tpl);?>

			</span>
		</label>
		<div class="col-lg-7">
			<input type="text" id="video1" name="video1" value="" disabled />
		</div>
	</div>

	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right"></span></div>
		<label class="control-label col-lg-2" for="video2">
			<span>
				<?php echo smartyTranslate(array('s'=>"Video 2"),$_smarty_tpl);?>

			</span>
		</label>
		<div class="col-lg-7">
			<input type="text" id="video2" name="video2" value="" disabled />
		</div>
	</div>


	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right"></span></div>
		<label class="control-label col-lg-2" for="comparaprezzi">
			<span>
				Comparaprezzi
			</span>
		</label>
		<div class="col-lg-7" id='comparaprezzifields'>
			<input type="hidden" name="tutti_i_comparaprezzi" />
			<input type="checkbox" name="trovaprezzi_add" <?php if ($_smarty_tpl->tpl_vars['product']->value->trovaprezzi) {?>checked="checked"<?php }?>/> Trovaprezzi &nbsp
			<input type="checkbox" name="google_shopping_add" <?php if ($_smarty_tpl->tpl_vars['product']->value->google_shopping) {?>checked="checked"<?php }?>/> Google Shopping &nbsp<br />
			<input type="checkbox" name="amazon_it_checkbox" <?php if ($_smarty_tpl->tpl_vars['amazon_check_it']->value) {?>checked="checked"<?php }?><?php if (isset($_smarty_tpl->tpl_vars['amazon_it_style']->value)&&$_smarty_tpl->tpl_vars['amazon_it_style']->value!='') {?> style="<?php echo $_smarty_tpl->tpl_vars['amazon_it_style']->value;?>
"<?php }?>/> Amazon.it &nbsp
			<input type="checkbox" name="amazon_fr_checkbox" <?php if ($_smarty_tpl->tpl_vars['amazon_check_fr']->value) {?>checked="checked"<?php }?><?php if (isset($_smarty_tpl->tpl_vars['amazon_fr_style']->value)&&$_smarty_tpl->tpl_vars['amazon_fr_style']->value!='') {?> style="<?php echo $_smarty_tpl->tpl_vars['amazon_fr_style']->value;?>
"<?php }?>/> Amazon.fr &nbsp
			<input type="checkbox" name="amazon_es_checkbox" <?php if ($_smarty_tpl->tpl_vars['amazon_check_es']->value) {?>checked="checked"<?php }?><?php if (isset($_smarty_tpl->tpl_vars['amazon_es_style']->value)&&$_smarty_tpl->tpl_vars['amazon_es_style']->value!='') {?> style="<?php echo $_smarty_tpl->tpl_vars['amazon_es_style']->value;?>
"<?php }?>/> Amazon.es &nbsp
			<input type="checkbox" name="amazon_uk_checkbox" <?php if ($_smarty_tpl->tpl_vars['amazon_check_uk']->value) {?>checked="checked"<?php }?><?php if (isset($_smarty_tpl->tpl_vars['amazon_uk_style']->value)&&$_smarty_tpl->tpl_vars['amazon_uk_style']->value!='') {?> style="<?php echo $_smarty_tpl->tpl_vars['amazon_uk_style']->value;?>
"<?php }?>/> Amazon.co.uk &nbsp
			<input type="checkbox" name="amazon_de_checkbox" <?php if ($_smarty_tpl->tpl_vars['amazon_check_de']->value) {?>checked="checked"<?php }?><?php if (isset($_smarty_tpl->tpl_vars['amazon_de_style']->value)&&$_smarty_tpl->tpl_vars['amazon_de_style']->value!='') {?> style="<?php echo $_smarty_tpl->tpl_vars['amazon_de_style']->value;?>
"<?php }?>/> Amazon.de &nbsp
			<input type="checkbox" name="amazon_nl_checkbox" <?php if ($_smarty_tpl->tpl_vars['amazon_check_nl']->value) {?>checked="checked"<?php }?><?php if (isset($_smarty_tpl->tpl_vars['amazon_nl_style']->value)&&$_smarty_tpl->tpl_vars['amazon_nl_style']->value!='') {?> style="<?php echo $_smarty_tpl->tpl_vars['amazon_nl_style']->value;?>
"<?php }?>/> Amazon.nl &nbsp
			<input type="checkbox" name="amazon_se_checkbox" <?php if ($_smarty_tpl->tpl_vars['amazon_check_se']->value) {?>checked="checked"<?php }?><?php if (isset($_smarty_tpl->tpl_vars['amazon_se_style']->value)&&$_smarty_tpl->tpl_vars['amazon_se_style']->value!='') {?> style="<?php echo $_smarty_tpl->tpl_vars['amazon_se_style']->value;?>
"<?php }?>/> Amazon.se &nbsp
			<input type="checkbox" name="amazon_pl_checkbox" <?php if ($_smarty_tpl->tpl_vars['amazon_check_pl']->value) {?>checked="checked"<?php }?><?php if (isset($_smarty_tpl->tpl_vars['amazon_pl_style']->value)&&$_smarty_tpl->tpl_vars['amazon_pl_style']->value!='') {?> style="<?php echo $_smarty_tpl->tpl_vars['amazon_pl_style']->value;?>
"<?php }?>/> Amazon.pl &nbsp<br />
			<input type="checkbox" name="eprice_add" <?php if ($_smarty_tpl->tpl_vars['product']->value->eprice) {?>checked="checked"<?php }?>/> ePrice &nbsp<br /><br />
			<input type="checkbox" onclick="checkUncheckSome('tutti_add','comparaprezzifields')" id="tutti_add" name="tutti_add" /> Tutti &nbsp<br />
		</div>
	</div>


	

	<div class="form-group">
		<label class="control-label col-lg-3" for="tags_<?php echo $_smarty_tpl->tpl_vars['id_lang']->value;?>
">
			<span class="label-tooltip" data-toggle="tooltip"
				title="<?php echo smartyTranslate(array('s'=>'Will be displayed in the tags block when enabled. Tags help customers easily find your products.'),$_smarty_tpl);?>
">
				<?php echo smartyTranslate(array('s'=>'Tags:'),$_smarty_tpl);?>

			</span>
		</label>
		<div class="col-lg-9">
			<?php if (count($_smarty_tpl->tpl_vars['languages']->value)>1) {?>
			<div class="row">
			<?php }?>
				<?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
					
					<script type="text/javascript">
						$().ready(function () {
							var input_id = 'tags_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
';
							$('#'+input_id).tagify({delimiters: [13,44], addTagPrompt: '<?php echo smartyTranslate(array('s'=>'Add tag','js'=>1),$_smarty_tpl);?>
'});
							$('#<?php echo $_smarty_tpl->tpl_vars['table']->value;?>
_form').submit( function() {
								$(this).find('#'+input_id).val($('#'+input_id).tagify('serialize'));
							});
						});
					</script>
					
				<?php if (count($_smarty_tpl->tpl_vars['languages']->value)>1) {?>
				<div class="translatable-field lang-<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
">
					<div class="col-lg-9">
				<?php }?>
						<input type="text" id="tags_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" class="tagify updateCurrentText" name="tags_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" value="<?php echo smarty_modifier_htmlentitiesUTF8($_smarty_tpl->tpl_vars['product']->value->getTags($_smarty_tpl->tpl_vars['language']->value['id_lang'],true));?>
" />
				<?php if (count($_smarty_tpl->tpl_vars['languages']->value)>1) {?>
					</div>
					<div class="col-lg-2">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
							<?php echo $_smarty_tpl->tpl_vars['language']->value['iso_code'];?>

							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
							<li>
								<a href="javascript:tabs_manager.allow_hide_other_languages = false;hideOtherLanguage(<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
);"><?php echo $_smarty_tpl->tpl_vars['language']->value['name'];?>
</a>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
				<?php }?>
				<?php } ?>
			<?php if (count($_smarty_tpl->tpl_vars['languages']->value)>1) {?>
			</div>
			<?php }?>
		</div>
		<div class="col-lg-9 col-lg-offset-3">
			<div class="help-block"><?php echo smartyTranslate(array('s'=>'Each tag has to be followed by a comma. The following characters are forbidden: %s','sprintf'=>'!&lt;;&gt;;?=+#&quot;&deg;{}_$%.'),$_smarty_tpl);?>

			</div>
		</div>
	</div>

	

	

	<div class="panel-footer">
		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminProducts'), ENT_QUOTES, 'UTF-8', true);?>
<?php if (isset($_REQUEST['page'])&&$_REQUEST['page']>1) {?>&amp;submitFilterproduct=<?php echo intval($_REQUEST['page']);?>
<?php }?>" class="btn btn-default"><i class="process-icon-cancel"></i> <?php echo smartyTranslate(array('s'=>'Cancel'),$_smarty_tpl);?>
</a>
		<button type="submit" name="submitAddproduct" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> <?php echo smartyTranslate(array('s'=>'Save'),$_smarty_tpl);?>
</button>
		<button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> <?php echo smartyTranslate(array('s'=>'Save and stay'),$_smarty_tpl);?>
</button>
	</div>
</div>

<br /><br />


<script type="text/javascript">
	hideOtherLanguage(<?php echo $_smarty_tpl->tpl_vars['default_form_language']->value;?>
);
	var missing_product_name = "<?php echo smartyTranslate(array('s'=>'Please fill product name input field','js'=>1),$_smarty_tpl);?>
";
</script>


<script type='text/javascript'>

function checkUncheckSome(controller,theElements) {
	var formElements = theElements.split(',');
	var theController = document.getElementById(controller);
	for(var z=0; z<formElements.length;z++){
		theItem = document.getElementById(formElements[z]);
		if(theItem.type && theItem.type=='checkbox'){
				theItem.checked=theController.checked;
		} 
		else {
			theInputs = theItem.getElementsByTagName('input');
			for(var y=0; y<theInputs.length; y++){
				if(theInputs[y].type == 'checkbox' && theInputs[y].id != theController.id){
					theInputs[y].checked = theController.checked;
				}
			}
		}
	}
}

function uncheckTutti(){
	document.getElementById("tutti_checkbox_comparaprezzi").checked = false;
	//wip
}

</script>







<script type="text/javascript">
	
	/*function getSupplierByManufacturer() // inutilizzata (cambiato metodo per aggiungere e rimuovere fornitori)
	{
		var supplier = $("option:selected", "#id_manufacturer").attr("data-supplier");
		document.getElementById("id_supplier").value = supplier;
		var other_suppliers = $("option:selected", "#id_manufacturer").attr("data-othersuppliers");
		var other_suppliers_array = other_suppliers.split(";");
		for(var z=0; z<other_suppliers_array.length;z++){
			item = other_suppliers_array[z];
			if(item.length != 0)
				add_riga_selected(item);
		}
	}*/
	
	function getMargins()
	{
		var margin_min_cli = $("option:selected", "#id_manufacturer").attr("data-margin-cli");
		var margin_min_riv = $("option:selected", "#id_manufacturer").attr("data-margin-riv");
		
		// console.log("cli: "+margin_min_cli+"   riv: "+margin_min_riv);
		
		document.getElementById("margin_min_cli").value = margin_min_cli;
		document.getElementById("margin_min_riv").value = margin_min_riv;
	}
	
</script>

<?php }} ?>
