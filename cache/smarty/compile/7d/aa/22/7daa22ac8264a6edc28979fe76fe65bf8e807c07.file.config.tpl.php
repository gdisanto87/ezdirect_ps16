<?php /* Smarty version Smarty-3.1.19, created on 2021-11-24 13:06:39
         compiled from "/var/www/html/modules/gestpay/views/templates/admin/tabs/config.tpl" */ ?>
<?php /*%%SmartyHeaderCode:649429435619e2acf681ba2-03394522%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7daa22ac8264a6edc28979fe76fe65bf8e807c07' => 
    array (
      0 => '/var/www/html/modules/gestpay/views/templates/admin/tabs/config.tpl',
      1 => 1634899860,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '649429435619e2acf681ba2-03394522',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'module_display' => 0,
    'submit_message' => 0,
    'request_uri' => 0,
    'api_key_error' => 0,
    'api_key_error_message' => 0,
    'config_error' => 0,
    'config_error_message' => 0,
    'gestpay_shop_login' => 0,
    'platforms' => 0,
    'platform_selected' => 0,
    'gestpay_test_mode' => 0,
    'gestpay_language' => 0,
    'gestpay_iframe' => 0,
    'gestpay_token' => 0,
    'gestpay_token_enabled' => 0,
    'gestpay_s2s' => 0,
    'gestpay_s2s_auto' => 0,
    'gestpay_s2s_auto_enabled' => 0,
    'gestpay_tls_endpoint' => 0,
    'gestpay_list_methods' => 0,
    'gestpay_apikey' => 0,
    'gestpay_remove_table' => 0,
    'gestpay_debug' => 0,
    'module_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_619e2acf6c0fc2_22882052',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_619e2acf6c0fc2_22882052')) {function content_619e2acf6c0fc2_22882052($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include '/var/www/html/tools/smarty/plugins/function.html_options.php';
?>
<?php if (@constant('_PS_VERSION_')>=1.6) {?>
<h3>
	<i class="icon-wrench"></i> <?php echo smartyTranslate(array('s'=>'General configuration','mod'=>'gestpay'),$_smarty_tpl);?>
 <small><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_display']->value, ENT_QUOTES, 'UTF-8', true);?>
</small>
</h3>
<?php if (isset($_smarty_tpl->tpl_vars['submit_message']->value['config'])) {?>
<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['submit_message']->value['config']);?>

<?php }?>
<form role="form"  class="defaultForm form-horizontal" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['request_uri']->value, ENT_QUOTES, 'UTF-8', true);?>
" method="post">
    <!-- API Key -->
    <?php if ($_smarty_tpl->tpl_vars['api_key_error']->value==1) {?>
    <div class="alert alert-danger"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['api_key_error_message']->value, ENT_QUOTES, 'UTF-8', true);?>
</div>
    <?php }?>
    <!-- Config -->
    <?php if ($_smarty_tpl->tpl_vars['config_error']->value==1) {?>
    <div class="alert alert-warning"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config_error_message']->value, ENT_QUOTES, 'UTF-8', true);?>
</div>
    <?php }?>
        <div class="form-wrapper">
            <div class="form-group">
                <label class="control-label col-lg-3 required"><?php echo smartyTranslate(array('s'=>'Shop Login','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="col-lg-3 ">
                    <div class="input-group">
						<input type="text" name="GESTPAY_SHOP_LOGIN" id="GESTPAY_SHOP_LOGIN" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['gestpay_shop_login']->value, ENT_QUOTES, 'UTF-8', true);?>
" class="" required="required" />
					</div>
					<p class="help-block"><?php echo smartyTranslate(array('s'=>'Insert the full shop login as appears in the Axerve Backoffice','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
				</div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 required"><?php echo smartyTranslate(array('s'=>'Platform','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="col-lg-3 ">
                     <select name="GESTPAY_PLATFORM" class="fixed-width-xxl" id="GESTPAY_PLATFORM">
                        <?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['platforms']->value,'selected'=>$_smarty_tpl->tpl_vars['platform_selected']->value),$_smarty_tpl);?>

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3"><?php echo smartyTranslate(array('s'=>'Enable test mode','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="col-lg-9 ">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_TEST_MODE" id="GESTPAY_TEST_MODE_on" value="1" <?php if ($_smarty_tpl->tpl_vars['gestpay_test_mode']->value==1) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_TEST_MODE_on"><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <input type="radio" name="GESTPAY_TEST_MODE" id="GESTPAY_TEST_MODE_off" value="0" <?php if ($_smarty_tpl->tpl_vars['gestpay_test_mode']->value==0) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_TEST_MODE_off"><?php echo smartyTranslate(array('s'=>'No','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block"><?php echo smartyTranslate(array('s'=>'Enables the gateway test mode. Use only to check if the module is working. Please note that the test mode Shop Login can be different from the production mode.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3"><?php echo smartyTranslate(array('s'=>'Enable payment page localization','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="col-lg-9 ">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_LANGUAGE" id="GESTPAY_LANGUAGE_on" value="1" <?php if ($_smarty_tpl->tpl_vars['gestpay_language']->value==1) {?>checked="checked"<?php }?>/>
                        <label  for="GESTPAY_LANGUAGE_on"><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <input type="radio" name="GESTPAY_LANGUAGE" id="GESTPAY_LANGUAGE_off" value="0" <?php if ($_smarty_tpl->tpl_vars['gestpay_language']->value==0) {?>checked="checked"<?php }?>/>
                        <label  for="GESTPAY_LANGUAGE_off"><?php echo smartyTranslate(array('s'=>'No','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block"><?php echo smartyTranslate(array('s'=>'Enables or disables payment page translation according to the language used by the customer (multilanguage store). Please note that you have to enable the language parameter in the configuration page.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3"><?php echo smartyTranslate(array('s'=>'Enable iframe payment','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="col-lg-9 ">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_IFRAME" id="GESTPAY_IFRAME_on" value="1" <?php if ($_smarty_tpl->tpl_vars['gestpay_iframe']->value==1) {?>checked="checked"<?php }?>/>
                        <label  for="GESTPAY_IFRAME_on"><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <input type="radio" name="GESTPAY_IFRAME" id="GESTPAY_IFRAME_off" value="0" <?php if ($_smarty_tpl->tpl_vars['gestpay_iframe']->value==0) {?>checked="checked"<?php }?>/>
                        <label  for="GESTPAY_IFRAME_off"><?php echo smartyTranslate(array('s'=>'No','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block"><?php echo smartyTranslate(array('s'=>'Pay directly on the store checkout page, without being redirected to Axerve payment page.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3"><?php echo smartyTranslate(array('s'=>'Enable tokens','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="col-lg-9 ">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_TOKEN" id="GESTPAY_TOKEN_on" value="1" <?php if ($_smarty_tpl->tpl_vars['gestpay_token']->value==1) {?>checked="checked"<?php }?> <?php if ($_smarty_tpl->tpl_vars['gestpay_token_enabled']->value==0) {?>disabled="disabled"<?php }?>/>
                        <label  for="GESTPAY_TOKEN_on"><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <input type="radio" name="GESTPAY_TOKEN" id="GESTPAY_TOKEN_off" value="0" <?php if ($_smarty_tpl->tpl_vars['gestpay_token']->value==0) {?>checked="checked"<?php }?> <?php if ($_smarty_tpl->tpl_vars['gestpay_token_enabled']->value==0) {?>disabled="disabled"<?php }?>/>
                        <label  for="GESTPAY_TOKEN_off"><?php echo smartyTranslate(array('s'=>'No','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block"><?php echo smartyTranslate(array('s'=>'Tokenization means the replacement of the credit card number with a Token. Please note that this setting has to be configured on the Axerve backend as well. Please refer to the manual for further info.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3"><?php echo smartyTranslate(array('s'=>'Use Sales API','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="col-lg-9 ">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_S2S" id="GESTPAY_S2S_on" value="1" <?php if ($_smarty_tpl->tpl_vars['gestpay_s2s']->value==1) {?>checked="checked"<?php }?>/>
                        <label  for="GESTPAY_S2S_on"><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <input type="radio" name="GESTPAY_S2S" id="GESTPAY_S2S_off" value="0" <?php if ($_smarty_tpl->tpl_vars['gestpay_s2s']->value==0) {?>checked="checked"<?php }?>/>
                        <label  for="GESTPAY_S2S_off"><?php echo smartyTranslate(array('s'=>'No','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block"><?php echo smartyTranslate(array('s'=>'Use Sales API for Settle and Refund actions. Available for Unlimited version only.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3"><?php echo smartyTranslate(array('s'=>'Sales API automation','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="col-lg-9">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_S2S_AUTO" id="GESTPAY_S2S_AUTO_on" value="1" <?php if ($_smarty_tpl->tpl_vars['gestpay_s2s_auto']->value==1) {?>checked="checked"<?php }?> <?php if ($_smarty_tpl->tpl_vars['gestpay_s2s_auto_enabled']->value==0) {?>disabled="disabled"<?php }?>/>
                        <label for="GESTPAY_S2S_AUTO_on"><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <input type="radio" name="GESTPAY_S2S_AUTO" id="GESTPAY_S2S_AUTO_off" value="0" <?php if ($_smarty_tpl->tpl_vars['gestpay_s2s_auto']->value==0) {?>checked="checked"<?php }?> <?php if ($_smarty_tpl->tpl_vars['gestpay_s2s_auto_enabled']->value==0) {?>disabled="disabled"<?php }?>/>
                        <label for="GESTPAY_S2S_AUTO_off"><?php echo smartyTranslate(array('s'=>'No','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block"><?php echo smartyTranslate(array('s'=>'Automate refund action when order is canceled or credit slip is created.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3"><?php echo smartyTranslate(array('s'=>'Use TLS endpoints','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="col-lg-9"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_TLS_ENDPOINT" id="GESTPAY_TLS_ENDPOINT_on" value="1" <?php if ($_smarty_tpl->tpl_vars['gestpay_tls_endpoint']->value==1) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_TLS_ENDPOINT_on"><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <input type="radio" name="GESTPAY_TLS_ENDPOINT" id="GESTPAY_TLS_ENDPOINT_off" value="0" <?php if ($_smarty_tpl->tpl_vars['gestpay_tls_endpoint']->value==0) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_TLS_ENDPOINT_off"><?php echo smartyTranslate(array('s'=>'No','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block"><?php echo smartyTranslate(array('s'=>'If your server does NOT support TLS set this flag to false.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3"><?php echo smartyTranslate(array('s'=>'Single button per payment','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="col-lg-9">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_LIST_METHODS" id="GESTPAY_LIST_METHODS_on" value="1" <?php if ($_smarty_tpl->tpl_vars['gestpay_list_methods']->value==1) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_LIST_METHODS_on"><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <input type="radio" name="GESTPAY_LIST_METHODS" id="GESTPAY_LIST_METHODS_off" value="0" <?php if ($_smarty_tpl->tpl_vars['gestpay_list_methods']->value==0) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_LIST_METHODS_off"><?php echo smartyTranslate(array('s'=>'No','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block"><?php echo smartyTranslate(array('s'=>'Available from professional and up and with apikey set.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3"><?php echo smartyTranslate(array('s'=>'API key','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="col-lg-9">
                    <div class="input-group col-lg-9">
						<input type="text" name="GESTPAY_APIKEY" id="GESTPAY_APIKEY" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['gestpay_apikey']->value, ENT_QUOTES, 'UTF-8', true);?>
" class="" />
					</div>
					<p class="help-block"><?php echo smartyTranslate(array('s'=>'To enable single button payment methods, this value is mandatory.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
				</div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3"></label>
                <div class="col-lg-3 ">
                    <button type="button" id="test_connection" name="test_connection" class="btn btn-default pull-left"><?php echo smartyTranslate(array('s'=>'Test connection','mod'=>'gestpay'),$_smarty_tpl);?>
</button>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3"><?php echo smartyTranslate(array('s'=>'Remova data on uninstall','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="col-lg-9 ">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_REMOVE_TABLE" id="GESTPAY_REMOVE_TABLE_on" value="1" <?php if ($_smarty_tpl->tpl_vars['gestpay_remove_table']->value==1) {?>checked="checked"<?php }?>/>
                        <label  for="GESTPAY_REMOVE_TABLE_on"><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <input type="radio" name="GESTPAY_REMOVE_TABLE" id="GESTPAY_REMOVE_TABLE_off" value="0" <?php if ($_smarty_tpl->tpl_vars['gestpay_remove_table']->value==0) {?>checked="checked"<?php }?>/>
                        <label  for="GESTPAY_REMOVE_TABLE_off"><?php echo smartyTranslate(array('s'=>'No','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block"><?php echo smartyTranslate(array('s'=>'When enabled and the module is uninstalled, all the module data will be deleted, including transaction logs.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3"><?php echo smartyTranslate(array('s'=>'Enable module debugging log','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="col-lg-9 ">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_DEBUG" id="GESTPAY_DEBUG_on" value="1" <?php if ($_smarty_tpl->tpl_vars['gestpay_debug']->value==1) {?>checked="checked"<?php }?>/>
                        <label  for="GESTPAY_DEBUG_on"><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <input type="radio" name="GESTPAY_DEBUG" id="GESTPAY_DEBUG_off" value="0" <?php if ($_smarty_tpl->tpl_vars['gestpay_debug']->value==0) {?>checked="checked"<?php }?>/>
                        <label  for="GESTPAY_DEBUG_off"><?php echo smartyTranslate(array('s'=>'No','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block"><?php echo smartyTranslate(array('s'=>'Enable debug logs. Activate only on request by technical support.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="form-group">
            <button type="submit" value="1" id="submitConfig" name="submitConfig" class="btn btn-default pull-right">
                <i class="process-icon-save"></i><?php echo smartyTranslate(array('s'=>'Save','mod'=>'gestpay'),$_smarty_tpl);?>

            </button>
            </div>
        </div>
</form>
<?php } else { ?>
<fieldset>
    <legend><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
views/img/information.png" alt="<?php echo smartyTranslate(array('s'=>'General configuration','mod'=>'gestpay'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'General configuration','mod'=>'gestpay'),$_smarty_tpl);?>
</legend>
    <?php if (isset($_smarty_tpl->tpl_vars['submit_message']->value['config'])) {?>
    <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['submit_message']->value['config']);?>

    <?php }?>
    <form role="form"  class="defaultForm form-horizontal" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['request_uri']->value, ENT_QUOTES, 'UTF-8', true);?>
" method="post">
    <!-- API Key -->
    <?php if ($_smarty_tpl->tpl_vars['api_key_error']->value==1) {?>
    <div class="alert alert-danger"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['api_key_error_message']->value, ENT_QUOTES, 'UTF-8', true);?>
</div>
    <?php }?>
    <!-- Config -->
    <?php if ($_smarty_tpl->tpl_vars['config_error']->value==1) {?>
    <div class="alert alert-warning"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config_error_message']->value, ENT_QUOTES, 'UTF-8', true);?>
</div>
    <?php }?>
        <div class="form-wrapper">
            <div class="form-group">
                <label class="control-label required"><?php echo smartyTranslate(array('s'=>'Shop Login','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="margin-form">
                    <div class="input-group">
						<input type="text" name="GESTPAY_SHOP_LOGIN" id="GESTPAY_SHOP_LOGIN" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['gestpay_shop_login']->value, ENT_QUOTES, 'UTF-8', true);?>
" class="" required="required" />
					</div>
					<p class="preference_description"><?php echo smartyTranslate(array('s'=>'Insert the full shop login as appears in the Axerve Backoffice','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
				</div>
            </div>
            <div class="form-group">
                <label class="control-label required"><?php echo smartyTranslate(array('s'=>'Platform','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="margin-form">
                     <select name="GESTPAY_PLATFORM" class="fixed-width-xxl" id="GESTPAY_PLATFORM">
                        <?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['platforms']->value,'selected'=>$_smarty_tpl->tpl_vars['platform_selected']->value),$_smarty_tpl);?>

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label"><?php echo smartyTranslate(array('s'=>'Enable test mode','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="margin-form"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_TEST_MODE" id="GESTPAY_TEST_MODE_on" value="1" <?php if ($_smarty_tpl->tpl_vars['gestpay_test_mode']->value==1) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_TEST_MODE_on" class="t"><strong><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'gestpay'),$_smarty_tpl);?>
</strong></label>
                        <input type="radio" name="GESTPAY_TEST_MODE" id="GESTPAY_TEST_MODE_off" value="0" <?php if ($_smarty_tpl->tpl_vars['gestpay_test_mode']->value==0) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_TEST_MODE_off" class="t"><strong><?php echo smartyTranslate(array('s'=>'No','mod'=>'gestpay'),$_smarty_tpl);?>
</strong></label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="preference_description"><?php echo smartyTranslate(array('s'=>'Enables the gateway test mode. Use only to check if the module is working. Please note that the test mode Shop Login can be different from the production mode.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label"><?php echo smartyTranslate(array('s'=>'Enable payment page localization','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="margin-form"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_LANGUAGE" id="GESTPAY_LANGUAGE_on" value="1" <?php if ($_smarty_tpl->tpl_vars['gestpay_language']->value==1) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_LANGUAGE_on" class="t"><strong><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'gestpay'),$_smarty_tpl);?>
</strong></label>
                        <input type="radio" name="GESTPAY_LANGUAGE" id="GESTPAY_LANGUAGE_off" value="0" <?php if ($_smarty_tpl->tpl_vars['gestpay_language']->value==0) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_LANGUAGE_off" class="t"><strong><?php echo smartyTranslate(array('s'=>'No','mod'=>'gestpay'),$_smarty_tpl);?>
</strong></label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="preference_description"><?php echo smartyTranslate(array('s'=>'Enables or disables payment page translation according to the language used by the customer (multilanguage store). Please note that you have to enable the language parameter in the configuration page.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label"><?php echo smartyTranslate(array('s'=>'Enable iframe payment','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="margin-form"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_IFRAME" id="GESTPAY_IFRAME_on" value="1" <?php if ($_smarty_tpl->tpl_vars['gestpay_iframe']->value==1) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_IFRAME_on" class="t"><strong><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'gestpay'),$_smarty_tpl);?>
</strong></label>
                        <input type="radio" name="GESTPAY_IFRAME" id="GESTPAY_IFRAME_off" value="0" <?php if ($_smarty_tpl->tpl_vars['gestpay_iframe']->value==0) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_IFRAME_off" class="t"><strong><?php echo smartyTranslate(array('s'=>'No','mod'=>'gestpay'),$_smarty_tpl);?>
</strong></label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="preference_description"><?php echo smartyTranslate(array('s'=>'Pay directly on the store checkout page, without being redirected to Axerve payment page. Please note that this setting has to be configured on the Axerve backend as well.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label"><?php echo smartyTranslate(array('s'=>'Enable tokens','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="margin-form"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_TOKEN" id="GESTPAY_TOKEN_on" value="1" <?php if ($_smarty_tpl->tpl_vars['gestpay_token']->value==1) {?>checked="checked"<?php }?> <?php if ($_smarty_tpl->tpl_vars['gestpay_token_enabled']->value==0) {?>disabled="disabled"<?php }?>/>
                        <label for="GESTPAY_TOKEN_on" class="t"><strong><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'gestpay'),$_smarty_tpl);?>
</strong></label>
                        <input type="radio" name="GESTPAY_TOKEN" id="GESTPAY_TOKEN_off" value="0" <?php if ($_smarty_tpl->tpl_vars['gestpay_token']->value==0) {?>checked="checked"<?php }?> <?php if ($_smarty_tpl->tpl_vars['gestpay_token_enabled']->value==0) {?>disabled="disabled"<?php }?>/>
                        <label for="GESTPAY_TOKEN_off" class="t"><strong><?php echo smartyTranslate(array('s'=>'No','mod'=>'gestpay'),$_smarty_tpl);?>
</strong></label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="preference_description"><?php echo smartyTranslate(array('s'=>'Tokenization means the replacement of the credit card number with a Token. Please note that this setting has to be configured on the Axerve backend as well. Please refer to the manual for further info.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label"><?php echo smartyTranslate(array('s'=>'Use Sales API','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="margin-form"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_S2S" id="GESTPAY_S2S_on" value="1" <?php if ($_smarty_tpl->tpl_vars['gestpay_s2s']->value==1) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_S2S_on" class="t"><strong><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'gestpay'),$_smarty_tpl);?>
</strong></label>
                        <input type="radio" name="GESTPAY_S2S" id="GESTPAY_S2S_off" value="0" <?php if ($_smarty_tpl->tpl_vars['gestpay_s2s']->value==0) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_S2S_off" class="t"><strong><?php echo smartyTranslate(array('s'=>'No','mod'=>'gestpay'),$_smarty_tpl);?>
</strong></label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="preference_description"><?php echo smartyTranslate(array('s'=>'Use Sales API for Settle and Refund actions. Available for Unlimited version only.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label"><?php echo smartyTranslate(array('s'=>'Sales API automation','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="margin-form"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_S2S_AUTO" id="GESTPAY_S2S_AUTO_on" value="1" <?php if ($_smarty_tpl->tpl_vars['gestpay_s2s_auto']->value==1) {?>checked="checked"<?php }?> <?php if ($_smarty_tpl->tpl_vars['gestpay_s2s_auto_enabled']->value==0) {?>disabled="disabled"<?php }?>/>
                        <label for="GESTPAY_S2S_AUTO_on" class="t"><strong><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'gestpay'),$_smarty_tpl);?>
</strong></label>
                        <input type="radio" name="GESTPAY_S2S_AUTO" id="GESTPAY_S2S_AUTO_off" value="0" <?php if ($_smarty_tpl->tpl_vars['gestpay_s2s_auto']->value==0) {?>checked="checked"<?php }?> <?php if ($_smarty_tpl->tpl_vars['gestpay_s2s_auto_enabled']->value==0) {?>disabled="disabled"<?php }?>/>
                        <label for="GESTPAY_S2S_AUTO_off" class="t"><strong><?php echo smartyTranslate(array('s'=>'No','mod'=>'gestpay'),$_smarty_tpl);?>
</strong></label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="preference_description"><?php echo smartyTranslate(array('s'=>'Automate refund action when order is canceled or credit slip is created.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label"><?php echo smartyTranslate(array('s'=>'Use TLS endpoints','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="margin-form"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_TLS_ENDPOINT" id="GESTPAY_TLS_ENDPOINT_on" value="1" <?php if ($_smarty_tpl->tpl_vars['gestpay_tls_endpoint']->value==1) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_TLS_ENDPOINT_on" class="t"><strong><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'gestpay'),$_smarty_tpl);?>
</strong></label>
                        <input type="radio" name="GESTPAY_TLS_ENDPOINT" id="GESTPAY_TLS_ENDPOINT_off" value="0" <?php if ($_smarty_tpl->tpl_vars['gestpay_tls_endpoint']->value==0) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_TLS_ENDPOINT_off" class="t"><strong><?php echo smartyTranslate(array('s'=>'No','mod'=>'gestpay'),$_smarty_tpl);?>
</strong></label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="preference_description"><?php echo smartyTranslate(array('s'=>'If your server does NOT support TLS set this flag to false.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label"></label>
                <div class="margin-form">
                    <button type="button" id="test_connection" name="test_connection" class="btn btn-default button pull-left"><?php echo smartyTranslate(array('s'=>'Test connection','mod'=>'gestpay'),$_smarty_tpl);?>
</button>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label"><?php echo smartyTranslate(array('s'=>'Remova data on uninstall','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="margin-form"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_REMOVE_TABLE" id="GESTPAY_REMOVE_TABLE_on" value="1" <?php if ($_smarty_tpl->tpl_vars['gestpay_remove_table']->value==1) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_REMOVE_TABLE_on" class="t"><strong><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'gestpay'),$_smarty_tpl);?>
</strong></label>
                        <input type="radio" name="GESTPAY_REMOVE_TABLE" id="GESTPAY_REMOVE_TABLE_off" value="0" <?php if ($_smarty_tpl->tpl_vars['gestpay_remove_table']->value==0) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_REMOVE_TABLE_off" class="t"><strong><?php echo smartyTranslate(array('s'=>'No','mod'=>'gestpay'),$_smarty_tpl);?>
</strong></label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="preference_description"><?php echo smartyTranslate(array('s'=>'When enabled and the module is uninstalled, all the module data will be deleted, including transaction logs.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label"><?php echo smartyTranslate(array('s'=>'Enable module debugging log','mod'=>'gestpay'),$_smarty_tpl);?>
</label>
                <div class="margin-form"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_DEBUG" id="GESTPAY_DEBUG_on" value="1" <?php if ($_smarty_tpl->tpl_vars['gestpay_debug']->value==1) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_DEBUG_on" class="t"><strong><?php echo smartyTranslate(array('s'=>'Yes','mod'=>'gestpay'),$_smarty_tpl);?>
</strong></label>
                        <input type="radio" name="GESTPAY_DEBUG" id="GESTPAY_DEBUG_off" value="0" <?php if ($_smarty_tpl->tpl_vars['gestpay_debug']->value==0) {?>checked="checked"<?php }?>/>
                        <label for="GESTPAY_DEBUG_off" class="t"><strong><?php echo smartyTranslate(array('s'=>'No','mod'=>'gestpay'),$_smarty_tpl);?>
</strong></label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="preference_description"><?php echo smartyTranslate(array('s'=>'Enable debug logs. Activate only on request by technical support.','mod'=>'gestpay'),$_smarty_tpl);?>
</p>
                </div>
            </div>
            <div class="form-group">
            <label class="control-label">&nbsp;</label>
            <button type="submit" value="1" id="submitConfig" name="submitConfig" class="btn btn-default button pull-right">
                <i class="process-icon-save"></i><?php echo smartyTranslate(array('s'=>'Save','mod'=>'gestpay'),$_smarty_tpl);?>

            </button>
            </div>
        </div>
</form>
</fieldset>
<?php }?><?php }} ?>
