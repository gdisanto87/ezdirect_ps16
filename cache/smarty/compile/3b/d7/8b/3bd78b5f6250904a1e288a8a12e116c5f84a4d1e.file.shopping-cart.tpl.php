<?php /* Smarty version Smarty-3.1.19, created on 2022-01-07 19:15:20
         compiled from "/var/www/html/themes/ez20/shopping-cart.tpl" */ ?>
<?php /*%%SmartyHeaderCode:206453271161d88338c1d0a2-68660954%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3bd78b5f6250904a1e288a8a12e116c5f84a4d1e' => 
    array (
      0 => '/var/www/html/themes/ez20/shopping-cart.tpl',
      1 => 1637749855,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '206453271161d88338c1d0a2-68660954',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'empty' => 0,
    'PS_CATALOG_MODE' => 0,
    'currencySign' => 0,
    'currencyRate' => 0,
    'currencyFormat' => 0,
    'currencyBlank' => 0,
    'productNumber' => 0,
    'products' => 0,
    'product' => 0,
    'productId' => 0,
    'productAttributeId' => 0,
    'customizedDatas' => 0,
    'id_customization' => 0,
    'customization' => 0,
    'type' => 0,
    'CUSTOMIZE_FILE' => 0,
    'datas' => 0,
    'pic_dir' => 0,
    'picture' => 0,
    'CUSTOMIZE_TEXTFIELD' => 0,
    'textField' => 0,
    'link' => 0,
    'token_cart' => 0,
    'img_dir' => 0,
    'quantityDisplayed' => 0,
    'discounts' => 0,
    'discount' => 0,
    'prodotti_con_sconto' => 0,
    'i' => 0,
    'prodotto_con_sconto' => 0,
    'opc' => 0,
    'priceDisplay' => 0,
    'use_taxes' => 0,
    'display_tax_label' => 0,
    'total_products' => 0,
    'total_products_wt' => 0,
    'total_discounts' => 0,
    'total_discounts_tax_exc' => 0,
    'total_wrapping' => 0,
    'total_wrapping_tax_exc' => 0,
    'shippingCostTaxExc' => 0,
    'shippingCost' => 0,
    'total_price_without_tax' => 0,
    'total_tax' => 0,
    'total_price' => 0,
    'free_ship' => 0,
    'isVirtualCart' => 0,
    'back' => 0,
    'id_group' => 0,
    'voucherAllowed' => 0,
    'errors_discount' => 0,
    'error' => 0,
    'discount_name' => 0,
    'displayVouchers' => 0,
    'voucher' => 0,
    'HOOK_SHOPPING_CART' => 0,
    'addresses_style' => 0,
    'carrier' => 0,
    'virtualCart' => 0,
    'delivery' => 0,
    'invoice' => 0,
    'HOOK_SHOPPING_CART_EXTRA' => 0,
    'img_ps_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61d88338c85a28_55237681',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61d88338c85a28_55237681')) {function content_61d88338c85a28_55237681($_smarty_tpl) {?>

<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'Your shopping cart'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div style="font-family:Verdana;">
<h1 id="cart_title" style="color: #000099;
	font-size: 18px;
	text-align: left; font-weight: 600; padding: 20px 0;"><?php echo smartyTranslate(array('s'=>'Il tuo carrello'),$_smarty_tpl);?>
</h1>

<?php $_smarty_tpl->tpl_vars['current_step'] = new Smarty_variable('summary', null, 0);?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>




<?php if (isset($_smarty_tpl->tpl_vars['empty']->value)) {?>
	<p class="warning"><?php echo smartyTranslate(array('s'=>'Your shopping cart is empty.'),$_smarty_tpl);?>
</p>
<?php } elseif ($_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
	<p class="warning"><?php echo smartyTranslate(array('s'=>'This store has not accepted your new order.'),$_smarty_tpl);?>
</p>
<?php } else { ?>
	<script type="text/javascript">
	// <![CDATA[
	var currencySign = '<?php echo html_entity_decode($_smarty_tpl->tpl_vars['currencySign']->value,2,"UTF-8");?>
';
	var currencyRate = '<?php echo floatval($_smarty_tpl->tpl_vars['currencyRate']->value);?>
';
	var currencyFormat = '<?php echo intval($_smarty_tpl->tpl_vars['currencyFormat']->value);?>
';
	var currencyBlank = '<?php echo intval($_smarty_tpl->tpl_vars['currencyBlank']->value);?>
';
	var txtProduct = "<?php echo smartyTranslate(array('s'=>'product'),$_smarty_tpl);?>
";
	var txtProducts = "<?php echo smartyTranslate(array('s'=>'products'),$_smarty_tpl);?>
";
	// ]]>
	</script>
	<p style="display:none" id="emptyCartWarning" class="warning"><?php echo smartyTranslate(array('s'=>'Your shopping cart is empty.'),$_smarty_tpl);?>
</p>
<p><?php echo smartyTranslate(array('s'=>'Your shopping cart contains'),$_smarty_tpl);?>
 <span id="summary_products_quantity"><?php echo $_smarty_tpl->tpl_vars['productNumber']->value;?>
 <?php if ($_smarty_tpl->tpl_vars['productNumber']->value==1) {?><?php echo smartyTranslate(array('s'=>'product'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'products'),$_smarty_tpl);?>
<?php }?></span></p>



<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./order-steps.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="shopping_cart_table">

	<table id="cart_summary" style=" padding:5px; border: 1px solid #e5e8eb; border-top: none;" class="std">
		<thead>
			<tr style="display:none;">
				<th style="background-image: none; background-color: #8095c4; padding-left: 10px;" class="cart_product first_item"><?php echo smartyTranslate(array('s'=>'Product'),$_smarty_tpl);?>
</th>
				<th style="background-image: none; background-color: #9baad1; padding-left: 10px;" class="cart_description item"><?php echo smartyTranslate(array('s'=>'Description'),$_smarty_tpl);?>
</th>
				<th style="background-image: none; background-color: #8095c4; padding-left: 10px;" class="cart_ref item"><?php echo smartyTranslate(array('s'=>'Ref.'),$_smarty_tpl);?>
</th>
				<th style="background-image: none; background-color: #9baad1; padding-left: 10px;" class="cart_availability item"><?php echo smartyTranslate(array('s'=>'Avail.'),$_smarty_tpl);?>
</th>
				<th style="background-image: none; background-color: #8095c4; padding-left: 10px;" class="cart_unit item"><?php echo smartyTranslate(array('s'=>'Unit price'),$_smarty_tpl);?>
</th>
				<th style="background-image: none; background-color: #9baad1; padding-left: 10px;" class="cart_quantity item"><?php echo smartyTranslate(array('s'=>'Delete'),$_smarty_tpl);?>
</th>
				<th style="background-image: none; background-color: #8095c4; padding-left: 10px;" class="cart_quantity item"><?php echo smartyTranslate(array('s'=>'Qty'),$_smarty_tpl);?>
</th>
				<th style="background-image: none; background-color: #9baad1; padding-left: 10px;" class="cart_total last_item"><?php echo smartyTranslate(array('s'=>'Total'),$_smarty_tpl);?>
</th>
			</tr>
		</thead>
		
		<tbody>
		<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
			<?php $_smarty_tpl->tpl_vars['productId'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['id_product'], null, 0);?>
			<?php $_smarty_tpl->tpl_vars['productAttributeId'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['id_product_attribute'], null, 0);?>
			<?php $_smarty_tpl->tpl_vars['quantityDisplayed'] = new Smarty_variable(0, null, 0);?>
			
			<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./shopping-cart-product-line.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			
			<?php if (isset($_smarty_tpl->tpl_vars['customizedDatas']->value[$_smarty_tpl->tpl_vars['productId']->value][$_smarty_tpl->tpl_vars['productAttributeId']->value])) {?>
				<?php  $_smarty_tpl->tpl_vars['customization'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['customization']->_loop = false;
 $_smarty_tpl->tpl_vars['id_customization'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['customizedDatas']->value[$_smarty_tpl->tpl_vars['productId']->value][$_smarty_tpl->tpl_vars['productAttributeId']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['customization']->key => $_smarty_tpl->tpl_vars['customization']->value) {
$_smarty_tpl->tpl_vars['customization']->_loop = true;
 $_smarty_tpl->tpl_vars['id_customization']->value = $_smarty_tpl->tpl_vars['customization']->key;
?>
					<tr style="border-bottom:1px solid #f4f4f4;" id="product_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product_attribute'];?>
_<?php echo $_smarty_tpl->tpl_vars['id_customization']->value;?>
" class="alternate_item cart_item">
						<td colspan="5">
							<?php  $_smarty_tpl->tpl_vars['datas'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['datas']->_loop = false;
 $_smarty_tpl->tpl_vars['type'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['customization']->value['datas']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['datas']->key => $_smarty_tpl->tpl_vars['datas']->value) {
$_smarty_tpl->tpl_vars['datas']->_loop = true;
 $_smarty_tpl->tpl_vars['type']->value = $_smarty_tpl->tpl_vars['datas']->key;
?>
								<?php if ($_smarty_tpl->tpl_vars['type']->value==$_smarty_tpl->tpl_vars['CUSTOMIZE_FILE']->value) {?>
									<div class="customizationUploaded">
										<ul class="customizationUploaded">
											<?php  $_smarty_tpl->tpl_vars['picture'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['picture']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['datas']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['picture']->key => $_smarty_tpl->tpl_vars['picture']->value) {
$_smarty_tpl->tpl_vars['picture']->_loop = true;
?><li><img src="<?php echo $_smarty_tpl->tpl_vars['pic_dir']->value;?>
<?php echo $_smarty_tpl->tpl_vars['picture']->value['value'];?>
_small" alt="" class="customizationUploaded" /></li><?php } ?>
										</ul>
									</div>
								<?php } elseif ($_smarty_tpl->tpl_vars['type']->value==$_smarty_tpl->tpl_vars['CUSTOMIZE_TEXTFIELD']->value) {?>
									<ul class="typedText">
										<?php  $_smarty_tpl->tpl_vars['textField'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['textField']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['datas']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['typedText']['index']=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['textField']->key => $_smarty_tpl->tpl_vars['textField']->value) {
$_smarty_tpl->tpl_vars['textField']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['typedText']['index']++;
?><li><?php if ($_smarty_tpl->tpl_vars['textField']->value['name']) {?><?php echo $_smarty_tpl->tpl_vars['textField']->value['name'];?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Text #'),$_smarty_tpl);?>
<?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['typedText']['index']+1;?>
<?php }?><?php echo smartyTranslate(array('s'=>':'),$_smarty_tpl);?>
 <?php echo $_smarty_tpl->tpl_vars['textField']->value['value'];?>
</li><?php } ?>
									</ul>
								<?php }?>
							<?php } ?>
						</td>
						<td class="cart_quantity">
							<div style="float:right">
								<a rel="nofollow" id="<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product_attribute'];?>
_<?php echo $_smarty_tpl->tpl_vars['id_customization']->value;?>
" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('cart.php',true);?>
&delete&amp;id_product=<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
&amp;ipa=<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product_attribute']);?>
&amp;id_customization=<?php echo $_smarty_tpl->tpl_vars['id_customization']->value;?>
&amp;token=<?php echo $_smarty_tpl->tpl_vars['token_cart']->value;?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/delete.gif" alt="<?php echo smartyTranslate(array('s'=>'Delete'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'Delete this customization'),$_smarty_tpl);?>
" width="18" height="18" class="icon" /></a>
							</div>
							<div id="cart_quantity_button" style="float:left">
							<a rel="nofollow" id="cart_quantity_up_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product_attribute'];?>
_<?php echo $_smarty_tpl->tpl_vars['id_customization']->value;?>
" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('cart.php',true);?>
?add&amp;id_product=<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
&amp;ipa=<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product_attribute']);?>
&amp;id_customization=<?php echo $_smarty_tpl->tpl_vars['id_customization']->value;?>
&amp;token=<?php echo $_smarty_tpl->tpl_vars['token_cart']->value;?>
" title="<?php echo smartyTranslate(array('s'=>'Add'),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/quantity_up.gif" alt="<?php echo smartyTranslate(array('s'=>'Add'),$_smarty_tpl);?>
" width="14" height="9" /></a><br />
							<?php if ($_smarty_tpl->tpl_vars['product']->value['minimal_quantity']<($_smarty_tpl->tpl_vars['customization']->value['quantity']-$_smarty_tpl->tpl_vars['quantityDisplayed']->value)||$_smarty_tpl->tpl_vars['product']->value['minimal_quantity']<=1) {?>
							<a rel="nofollow" id="cart_quantity_down_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product_attribute'];?>
_<?php echo $_smarty_tpl->tpl_vars['id_customization']->value;?>
" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('cart.php',true);?>
?add&amp;id_product=<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
&amp;ipa=<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product_attribute']);?>
&amp;id_customization=<?php echo $_smarty_tpl->tpl_vars['id_customization']->value;?>
&amp;op=down&amp;token=<?php echo $_smarty_tpl->tpl_vars['token_cart']->value;?>
" title="<?php echo smartyTranslate(array('s'=>'Subtract'),$_smarty_tpl);?>
">
								<img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/quantity_down.gif" alt="<?php echo smartyTranslate(array('s'=>'Subtract'),$_smarty_tpl);?>
" width="14" height="9" />
							</a>
							<?php } else { ?>
							<a style="opacity: 0.3;" id="cart_quantity_down_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product_attribute'];?>
_<?php echo $_smarty_tpl->tpl_vars['id_customization']->value;?>
" href="#" title="<?php echo smartyTranslate(array('s'=>'Subtract'),$_smarty_tpl);?>
">
								<img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/quantity_down.gif" alt="<?php echo smartyTranslate(array('s'=>'Subtract'),$_smarty_tpl);?>
" width="14" height="9" />
							</a>
							<?php }?>
							</div>
							<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['customization']->value['quantity'];?>
" name="quantity_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product_attribute'];?>
_<?php echo $_smarty_tpl->tpl_vars['id_customization']->value;?>
_hidden"/>
							<input size="2" type="text" value="<?php echo $_smarty_tpl->tpl_vars['customization']->value['quantity'];?>
" class="cart_quantity_input" name="quantity_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product_attribute'];?>
_<?php echo $_smarty_tpl->tpl_vars['id_customization']->value;?>
"/>
						</td>
						<td class="cart_total"></td>
					</tr>
					<?php $_smarty_tpl->tpl_vars['quantityDisplayed'] = new Smarty_variable($_smarty_tpl->tpl_vars['quantityDisplayed']->value+$_smarty_tpl->tpl_vars['customization']->value['quantity'], null, 0);?>
				<?php } ?>
				
				<?php if ($_smarty_tpl->tpl_vars['product']->value['quantity']-$_smarty_tpl->tpl_vars['quantityDisplayed']->value>0) {?><?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./shopping-cart-product-line.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }?>
			<?php }?>
		<?php } ?>
		</tbody>
	<?php if (sizeof($_smarty_tpl->tpl_vars['discounts']->value)) {?>
		<tbody>
		<?php  $_smarty_tpl->tpl_vars['discount'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['discount']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['discounts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['discount']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['discount']->iteration=0;
 $_smarty_tpl->tpl_vars['discount']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['discount']->key => $_smarty_tpl->tpl_vars['discount']->value) {
$_smarty_tpl->tpl_vars['discount']->_loop = true;
 $_smarty_tpl->tpl_vars['discount']->iteration++;
 $_smarty_tpl->tpl_vars['discount']->index++;
 $_smarty_tpl->tpl_vars['discount']->first = $_smarty_tpl->tpl_vars['discount']->index === 0;
 $_smarty_tpl->tpl_vars['discount']->last = $_smarty_tpl->tpl_vars['discount']->iteration === $_smarty_tpl->tpl_vars['discount']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['discountLoop']['first'] = $_smarty_tpl->tpl_vars['discount']->first;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['discountLoop']['last'] = $_smarty_tpl->tpl_vars['discount']->last;
?>
			<tr class="cart_discount <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['discountLoop']['last']) {?>last_item<?php } elseif ($_smarty_tpl->getVariable('smarty')->value['foreach']['discountLoop']['first']) {?>first_item<?php } else { ?>item<?php }?>" id="cart_discount_<?php echo $_smarty_tpl->tpl_vars['discount']->value['id_discount'];?>
">
				<td class="cart_discount_name" colspan="1"><?php echo $_smarty_tpl->tpl_vars['discount']->value['name'];?>
</td>
				<td class="cart_discount_name" colspan="1">
				<?php if ($_smarty_tpl->tpl_vars['prodotti_con_sconto']->value) {?>
					<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable(0, null, 0);?>
					
					<?php echo smartyTranslate(array('s'=>'Discount applied to: '),$_smarty_tpl);?>

					<?php  $_smarty_tpl->tpl_vars['prodotto_con_sconto'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['prodotto_con_sconto']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['prodotti_con_sconto']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['prodotto_con_sconto']->key => $_smarty_tpl->tpl_vars['prodotto_con_sconto']->value) {
$_smarty_tpl->tpl_vars['prodotto_con_sconto']->_loop = true;
?>
						<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
						<?php echo $_smarty_tpl->tpl_vars['prodotto_con_sconto']->value;?>

						<?php if ($_smarty_tpl->tpl_vars['i']->value==(count($_smarty_tpl->tpl_vars['prodotti_con_sconto']->value))) {?><?php } else { ?> - <?php }?> 
						
					<?php } ?>
				<?php } else { ?>
				<?php }?>
				</td>
				<td class="cart_discount_description" colspan="3"><?php echo $_smarty_tpl->tpl_vars['discount']->value['description'];?>
</td>
				<td class="cart_discount_delete"><a href="<?php if ($_smarty_tpl->tpl_vars['opc']->value) {?><?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('order-opc.php',true);?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('order.php',true);?>
<?php }?>&deleteDiscount=<?php echo $_smarty_tpl->tpl_vars['discount']->value['id_discount'];?>
" title="<?php echo smartyTranslate(array('s'=>'Delete'),$_smarty_tpl);?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
icon/delete.gif" alt="<?php echo smartyTranslate(array('s'=>'Delete'),$_smarty_tpl);?>
" class="icon" width="18" height="18" /></a></td>
				<td class="cart_discount_price"><span class="price-discount">
					<?php if ($_smarty_tpl->tpl_vars['discount']->value['value_real']>0) {?>
						<?php if (!$_smarty_tpl->tpl_vars['priceDisplay']->value) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['discount']->value['value_real']*-1),$_smarty_tpl);?>
<?php } else { ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['discount']->value['value_tax_exc']*-1),$_smarty_tpl);?>
<?php }?>
					<?php }?>
				</span></td>
			</tr>
		<?php } ?>
		</tbody>
	<?php }?>
	</table>

		<div  id="riepilogo-carrello">

		<div class="row" id="codice-sconto">
				<div style="background-color: #f4f4f4; padding: 20px; border: 1px solid #e5e8eb; margin-bottom: 30px;">
					<h5 style="    padding: 20px 30px; font-size: 16px; text-align: center;">Hai un codice sconto?</h5>

					<div id="coupon-input">
						<input style="    color: #f4f4f4; padding: 8px;  " for="discount-code" placeholder="Inserisci il tuo codice">
						<button style="    background-color: #ef6018; color: white; border: none; width:100%; padding: 5px;">Applica codice</button>
					</div>
				</div>

				
				
			</div>

		<div>
			<div style="    padding: 20px; background-color: #f4f4f4; display: flex; flex-direction: column-reverse; border: 1px solid #e5e8eb;">
				<tr>
				<td colspan="3">
					<table>
						<tbody>
							<tr>
								<?php if ($_smarty_tpl->tpl_vars['use_taxes']->value) {?>
								<?php if ($_smarty_tpl->tpl_vars['priceDisplay']->value) {?>
								<tr class="cart_total_price">
								<td colspan="7" style="padding: 5px 30px 10px 0px;"> <span><?php echo smartyTranslate(array('s'=>'Importo prodotti'),$_smarty_tpl);?>
 </span><?php if ($_smarty_tpl->tpl_vars['display_tax_label']->value) {?>  <?php echo smartyTranslate(array('s'=>'(iva escl.)'),$_smarty_tpl);?>
<?php }?><?php echo smartyTranslate(array('s'=>':'),$_smarty_tpl);?>
</td>
								<td class="price" style="font-size:16px; text-align: right;" id="total_product"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['total_products']->value),$_smarty_tpl);?>
</td>
							</tr>
							<?php } else { ?>
							<tr class="cart_total_price">
								<td colspan="7" style="padding: 5px 30px 10px 0px;"><span><?php echo smartyTranslate(array('s'=>'Importo prodotti'),$_smarty_tpl);?>
 </span><?php if ($_smarty_tpl->tpl_vars['display_tax_label']->value) {?> <?php echo smartyTranslate(array('s'=>'(iva incl.)'),$_smarty_tpl);?>
<?php }?><?php echo smartyTranslate(array('s'=>':'),$_smarty_tpl);?>
</td>
								<td class="price" style="font-size:16px; text-align: right;" id="total_product"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['total_products_wt']->value),$_smarty_tpl);?>
</td>
							</tr>
							<?php }?>
							<?php } else { ?>
							<tr class="cart_total_price">
								<td colspan="7" style="padding: 5px 30px 10px 0px;"><span><?php echo smartyTranslate(array('s'=>'Importo prodotti'),$_smarty_tpl);?>
</span></td>
								<td class="price" style="font-size:16px; text-align: right;" id="total_product"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['total_products']->value),$_smarty_tpl);?>
</td>
							</tr>
							<?php }?>
							<tr class="cart_total_voucher" <?php if ($_smarty_tpl->tpl_vars['total_discounts']->value==0) {?>style="display: none;"<?php }?>>
								<td colspan="7" style="padding: 5px 30px 10px 0px;">
								<?php if ($_smarty_tpl->tpl_vars['use_taxes']->value) {?>
									<?php if ($_smarty_tpl->tpl_vars['priceDisplay']->value) {?>
										<span ><?php echo smartyTranslate(array('s'=>'Total vouchers'),$_smarty_tpl);?>
</span><?php if ($_smarty_tpl->tpl_vars['display_tax_label']->value) {?> <?php echo smartyTranslate(array('s'=>'(iva escl.)'),$_smarty_tpl);?>
<?php }?><?php echo smartyTranslate(array('s'=>':'),$_smarty_tpl);?>

									<?php } else { ?>
										<span ><?php echo smartyTranslate(array('s'=>'Total vouchers'),$_smarty_tpl);?>
</span><?php if ($_smarty_tpl->tpl_vars['display_tax_label']->value) {?> <?php echo smartyTranslate(array('s'=>'(iva incl.)'),$_smarty_tpl);?>
<?php }?><?php echo smartyTranslate(array('s'=>':'),$_smarty_tpl);?>

									<?php }?>
								<?php } else { ?>
									<span ><?php echo smartyTranslate(array('s'=>'Total vouchers:'),$_smarty_tpl);?>
</span>
								<?php }?>
								</td>
								<td class="price-discount" id="total_discount">
								<?php if ($_smarty_tpl->tpl_vars['use_taxes']->value) {?>
									<?php if ($_smarty_tpl->tpl_vars['priceDisplay']->value) {?>
										<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['total_discounts_tax_exc']->value),$_smarty_tpl);?>

									<?php } else { ?>
										<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['total_discounts']->value),$_smarty_tpl);?>

									<?php }?>
								<?php } else { ?>
									<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['total_discounts_tax_exc']->value),$_smarty_tpl);?>

								<?php }?>
							</tr>

							<tr class="cart_total_voucher" <?php if ($_smarty_tpl->tpl_vars['total_wrapping']->value==0) {?>style="display: none;"<?php }?>>
								<td colspan="7" style="padding: 5px 30px 10px 0px;">
								<?php if ($_smarty_tpl->tpl_vars['use_taxes']->value) {?>
									<?php if ($_smarty_tpl->tpl_vars['priceDisplay']->value) {?>
										<span ><?php echo smartyTranslate(array('s'=>'Total gift-wrapping'),$_smarty_tpl);?>
</span><?php if ($_smarty_tpl->tpl_vars['display_tax_label']->value) {?> <?php echo smartyTranslate(array('s'=>'(iva escl.)'),$_smarty_tpl);?>
<?php }?><?php echo smartyTranslate(array('s'=>':'),$_smarty_tpl);?>

									<?php } else { ?>
										<span ><?php echo smartyTranslate(array('s'=>'Total gift-wrapping'),$_smarty_tpl);?>
</span><?php if ($_smarty_tpl->tpl_vars['display_tax_label']->value) {?> <?php echo smartyTranslate(array('s'=>'(iva incl.)'),$_smarty_tpl);?>
<?php }?><?php echo smartyTranslate(array('s'=>':'),$_smarty_tpl);?>

									<?php }?>
								<?php } else { ?>
									<span ><?php echo smartyTranslate(array('s'=>'Total gift-wrapping:'),$_smarty_tpl);?>
</span>
								<?php }?>
								</td>
								<td class="price-discount" id="total_wrapping">
								<?php if ($_smarty_tpl->tpl_vars['use_taxes']->value) {?>
									<?php if ($_smarty_tpl->tpl_vars['priceDisplay']->value) {?>
										<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['total_wrapping_tax_exc']->value),$_smarty_tpl);?>

									<?php } else { ?>
										<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['total_wrapping']->value),$_smarty_tpl);?>

									<?php }?>
								<?php } else { ?>
									<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['total_wrapping_tax_exc']->value),$_smarty_tpl);?>

								<?php }?>
								</td>
							</tr>
							<?php if ($_smarty_tpl->tpl_vars['use_taxes']->value) {?>
								<?php if ($_smarty_tpl->tpl_vars['priceDisplay']->value) {?>
									<tr class="cart_total_delivery">
										<td colspan="7" style="padding: 5px 30px 10px 0px;"><span><?php echo smartyTranslate(array('s'=>'Spedizione'),$_smarty_tpl);?>
</span><?php if ($_smarty_tpl->tpl_vars['display_tax_label']->value) {?> <?php echo smartyTranslate(array('s'=>'(iva escl.)'),$_smarty_tpl);?>
<?php }?><?php echo smartyTranslate(array('s'=>':'),$_smarty_tpl);?>
</td>
										<td class="price" syle="text-align: right;" id="total_shipping"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['shippingCostTaxExc']->value),$_smarty_tpl);?>
</td>
									</tr>
								<?php } else { ?>
									<tr class="cart_total_delivery">
										<td colspan="7" style="padding: 5px 30px 10px 0px;"><span ><?php echo smartyTranslate(array('s'=>'Spedizione'),$_smarty_tpl);?>
</span><?php if ($_smarty_tpl->tpl_vars['display_tax_label']->value) {?> <?php echo smartyTranslate(array('s'=>'(iva incl.)'),$_smarty_tpl);?>
<?php }?><?php echo smartyTranslate(array('s'=>':'),$_smarty_tpl);?>
</td>
										<td class="price" style="text-align: right;" id="total_shipping" ><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['shippingCost']->value),$_smarty_tpl);?>
</td>
									</tr>
								<?php }?>
							<?php } else { ?>
								<tr class="cart_total_delivery">
									<td colspan="7" style="padding: 5px 30px 10px 0px;"><span ><?php echo smartyTranslate(array('s'=>'Spedizione'),$_smarty_tpl);?>
</span></td>
									<td class="price" style="text-align: right;" id="total_shipping" ><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['shippingCostTaxExc']->value),$_smarty_tpl);?>
</td>
								</tr>
							<?php }?>

							<?php if ($_smarty_tpl->tpl_vars['use_taxes']->value) {?>
							<tr class="cart_total_price">
								<td colspan="7" style="padding: 5px 30px 10px 0px;">
									<?php if ($_smarty_tpl->tpl_vars['display_tax_label']->value) {?>
										<span ><?php echo smartyTranslate(array('s'=>'Totale'),$_smarty_tpl);?>
</span>  <?php echo smartyTranslate(array('s'=>'(iva escl.):'),$_smarty_tpl);?>

									<?php } else { ?>
										<span ><?php echo smartyTranslate(array('s'=>'Subtotale'),$_smarty_tpl);?>
</span>
									<?php }?>
								</td>
								<td class="price" style="font-size:16px; text-align: right;" id="total_price_without_tax"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['total_price_without_tax']->value),$_smarty_tpl);?>
</td>
							</tr>
							<tr class="cart_total_tax" style="border-bottom: 1px solid #d6d3d3;">
								<td colspan="7" style="padding: 5px 30px 10px 0px;">
									<?php if ($_smarty_tpl->tpl_vars['display_tax_label']->value) {?>
										<span ><?php echo smartyTranslate(array('s'=>'I.V.A.*'),$_smarty_tpl);?>
</span>
									<?php } else { ?>
										<span ><?php echo smartyTranslate(array('s'=>'I.V.A.*'),$_smarty_tpl);?>
</span>
									<?php }?>
								</td>
								<td class="price" style="text-align: right;" id="total_tax"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['total_tax']->value),$_smarty_tpl);?>
</td>
							</tr>
							<tr class="cart_total_price">
								<td colspan="7" style="padding: 15px 30px 10px 0px;">
									<?php if ($_smarty_tpl->tpl_vars['display_tax_label']->value) {?>
										<span ><?php echo smartyTranslate(array('s'=>'Totale'),$_smarty_tpl);?>
</span>  <?php echo smartyTranslate(array('s'=>'(iva incl.):'),$_smarty_tpl);?>

									<?php } else { ?>
										<span ><?php echo smartyTranslate(array('s'=>'Totale'),$_smarty_tpl);?>
</span>
									<?php }?>
									
								</td>
								<br /><span style="font-size: 10px;">*I.V.A. dove applicabile</span>
								<td class="price" style="font-size:25px; text-align: right;  font-weight:600; color: #b7160f;" id="total_price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['total_price']->value),$_smarty_tpl);?>
</td>
							</tr>
							<?php } else { ?>
							<tr class="cart_total_price">
								<td colspan="7" style="padding: 5px 30px 10px 0px;"><span ><?php echo smartyTranslate(array('s'=>'Totale'),$_smarty_tpl);?>
</span></td>
								<td class="price" style="font-size:25px; text-align: right; font-weight:600; color: #b7160f;" id="total_price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['total_price_without_tax']->value),$_smarty_tpl);?>
</td>
							</tr>
							<?php }?>
							<tr class="cart_free_shipping" <?php if ($_smarty_tpl->tpl_vars['free_ship']->value<=0||$_smarty_tpl->tpl_vars['isVirtualCart']->value) {?> style="display: none;" <?php }?>>
									<td colspan="7" style="padding: 5px 30px 10px 0px;" style="white-space: normal;"><?php echo smartyTranslate(array('s'=>'Remaining amount to be added to your cart in order to obtain free shipping:'),$_smarty_tpl);?>
</td>
									<td id="free_shipping" class="price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayPrice'][0][0]->displayPriceSmarty(array('price'=>$_smarty_tpl->tpl_vars['free_ship']->value),$_smarty_tpl);?>
</td>
							</tr>

							<p class="cart_navigation" >
								<?php if (!$_smarty_tpl->tpl_vars['opc']->value) {?><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('order.php',true);?>
?step=1<?php if ($_smarty_tpl->tpl_vars['back']->value) {?>&amp;back=<?php echo $_smarty_tpl->tpl_vars['back']->value;?>
<?php }?>" class="exclusive" title="<?php echo smartyTranslate(array('s'=>'Next'),$_smarty_tpl);?>
" style="padding: 10px 10px; font-size:12px; height: 35px;"><?php echo smartyTranslate(array('s'=>'Next'),$_smarty_tpl);?>
 &raquo;</a><?php }?>
								<a href="<?php if ((isset($_SERVER['HTTP_REFERER'])&&strstr($_SERVER['HTTP_REFERER'],$_smarty_tpl->tpl_vars['link']->value->getPageLink('order.php')))||!isset($_SERVER['HTTP_REFERER'])) {?><?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('index.php');?>
<?php } else { ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['secureReferrer'][0][0]->secureReferrer(mb_convert_encoding(htmlspecialchars($_SERVER['HTTP_REFERER'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
<?php }?>" class="button_large" title="<?php echo smartyTranslate(array('s'=>'Continua gli acquisti'),$_smarty_tpl);?>
" style="height: 35px; 
								padding: 10px 10px; font-size:12px; background-color: #ef6018;">&laquo; <?php echo smartyTranslate(array('s'=>'Continua gli acquisti'),$_smarty_tpl);?>
</a>
							</p>

							
						</tbody>
					</table>
				</td>
			</tr>

		
			</div>

			

		</div>
			
			
				
			
		</div>
</div>
<br /><br />
<?php if ($_smarty_tpl->tpl_vars['id_group']->value==3||$_smarty_tpl->tpl_vars['id_group']->value==10||$_smarty_tpl->tpl_vars['id_group']->value==12) {?>
<?php } else { ?>
<div class="shopping_cart_table">
<?php if ($_smarty_tpl->tpl_vars['voucherAllowed']->value) {?>
<div id="cart_voucher" class="table_block" style="width:700px; border:0px" >
	<?php if (isset($_smarty_tpl->tpl_vars['errors_discount']->value)&&$_smarty_tpl->tpl_vars['errors_discount']->value) {?>
		<ul class="error" >
		<?php  $_smarty_tpl->tpl_vars['error'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['error']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['errors_discount']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['error']->key => $_smarty_tpl->tpl_vars['error']->value) {
$_smarty_tpl->tpl_vars['error']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['error']->key;
?>
			<li><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['error']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</li>
		<?php } ?>
		</ul>
	<?php }?>
	<form action="<?php if ($_smarty_tpl->tpl_vars['opc']->value) {?><?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('order-opc.php',true);?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('order.php',true);?>
<?php }?>" method="post" id="voucher" style="border:0px" >
		<fieldset style="width:700px; border:0px; margin-bottom:-30px">
			<h4><?php echo smartyTranslate(array('s'=>'Vouchers'),$_smarty_tpl);?>
</h4>
			<p style="border:0px" >
				<label for="discount_name"><?php echo smartyTranslate(array('s'=>'Code:'),$_smarty_tpl);?>
</label>
				<input type="text" id="discount_name" name="discount_name" value="<?php if (isset($_smarty_tpl->tpl_vars['discount_name']->value)&&$_smarty_tpl->tpl_vars['discount_name']->value) {?><?php echo $_smarty_tpl->tpl_vars['discount_name']->value;?>
<?php }?>" />
			</p>
			<p class="submit"><input type="hidden" name="submitDiscount" /><input type="submit" name="submitAddDiscount" id="submitAddDiscount" value="<?php echo smartyTranslate(array('s'=>'Add'),$_smarty_tpl);?>
" class="button" style="font-weight:bold; height:25px" /></p>
		<?php if ($_smarty_tpl->tpl_vars['displayVouchers']->value) {?>
			<h4><?php echo smartyTranslate(array('s'=>'Take advantage of our offers:'),$_smarty_tpl);?>
</h4>
			<div id="display_cart_vouchers" style="border:0px" >
			<?php  $_smarty_tpl->tpl_vars['voucher'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['voucher']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['displayVouchers']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['voucher']->key => $_smarty_tpl->tpl_vars['voucher']->value) {
$_smarty_tpl->tpl_vars['voucher']->_loop = true;
?>
				<span onclick="$('#discount_name').val('<?php echo $_smarty_tpl->tpl_vars['voucher']->value['name'];?>
');return false;" class="voucher_name"><?php echo $_smarty_tpl->tpl_vars['voucher']->value['name'];?>
</span> - <?php echo $_smarty_tpl->tpl_vars['voucher']->value['description'];?>
 <br />
			<?php } ?>
			</div>
		<?php }?>
		</fieldset>
	</form>
</div>
<?php }?>
</div>
<?php }?>


<br /><br />
<div id="HOOK_SHOPPING_CART" style="border:0px"><?php echo $_smarty_tpl->tpl_vars['HOOK_SHOPPING_CART']->value;?>
</div>



<?php if (!isset($_smarty_tpl->tpl_vars['addresses_style']->value)) {?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['company'] = 'address_company';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['vat_number'] = 'address_company';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['firstname'] = 'address_name';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['lastname'] = 'address_name';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['address1'] = 'address_address1';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['address2'] = 'address_address2';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['city'] = 'address_city';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['country'] = 'address_country';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['phone'] = 'address_phone';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['phone_mobile'] = 'address_phone_mobile';?>
	<?php $_smarty_tpl->createLocalArrayVariable('addresses_style', null, 0);
$_smarty_tpl->tpl_vars['addresses_style']->value['alias'] = 'address_title';?>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['carrier']->value->id&&!isset($_smarty_tpl->tpl_vars['virtualCart']->value)) {?>
	
	<?php }?>
<?php if ((($_smarty_tpl->tpl_vars['carrier']->value->id&&!isset($_smarty_tpl->tpl_vars['virtualCart']->value))||$_smarty_tpl->tpl_vars['delivery']->value->id||$_smarty_tpl->tpl_vars['invoice']->value->id)&&!$_smarty_tpl->tpl_vars['opc']->value) {?>
<div class="order_delivery">
	
	
</div>

<?php }?>

<p class="clear"><br /><br /></p>
<div class="clear"></div>
<p class="cart_navigation_extra">
	<span id="HOOK_SHOPPING_CART_EXTRA"><?php echo $_smarty_tpl->tpl_vars['HOOK_SHOPPING_CART_EXTRA']->value;?>
</span>
</p>
<?php }?>
</div>






	<div style="border: 1px solid #e5e8eb; 
			box-shadow: 0 1px 1px rgba(0,0,0,0.15), 
        	0 2px 2px rgba(0,0,0,0.15), 
            0 4px 4px rgba(0,0,0,0.15), 
        	0 8px 8px rgba(0,0,0,0.15);
			border-radius:12px;
			padding:30px; 
			display: flex;" id="need-help">
		<div style="height: 140px; padding-right: 30px;">
			<img style="height:100%;" src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
business-woman.jpg' alt='Aiuto' title='Aiuto' />
		</div>
		<div>
			<h5 style="font-size: 22px; font-weight: 600;">Hai bisogno di aiuto con il tuo ordine?</h5>
			<div style="display:flex; align-items:center;" id="numero-verde"><p>Chiamaci al numero verde</p><a href="#" style="padding-left: 12px; padding-bottom: 10px;"><img style="height:50px;" src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
numero-verde.jpg' alt='Numero verde' /></a></div>
			
			<p>Oppure apri un ticket cliccando <a target='_blank' href='https://www.ezdirect.it/contattaci?step=assistenza-ordini'>qui</a>. Un esperto di risponderà quanto prima.</p>
		</div>
	</div>


	<div id="home-title-outer" style=" padding:20px 0; width:100%; height:auto;">
			<div class="container" id="home-title">
				<div class="row" style="display: flex; justify-content: center;">
					

					<div class="col-sm-3" id="orange_section">
						<img src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
consulenza.png' alt='Consulenza' title='Consulenza' />
						<h5><strong>Consulenza e assistenza</strong></h5>
						<p>Assistenza specializzata</p>
					</div>

					<div class="col-sm-3" id="orange_section">
						<img src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
spedizione24.png' alt='Spedizione24' title='Spedizione24' />
						<h5><strong>Spedizione in 24/48h</strong></h5>
						<p>Per i prodotti a stock</p>
					</div>

					<div class="col-sm-3" id="orange_section">
						<img src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
pagamento-sicuro.png' alt='Prodotti' title='Prodotti' />
						<h5><strong>Pagamento sicuro</strong></h5>
						<p>Paga online in totale tranquillità</p>
					</div>
				</div>
			</div>
		</div><?php }} ?>
