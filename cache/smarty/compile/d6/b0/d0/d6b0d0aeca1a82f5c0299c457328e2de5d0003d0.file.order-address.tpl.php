<?php /* Smarty version Smarty-3.1.19, created on 2021-11-16 15:07:44
         compiled from "/var/www/html/themes/ez20/order-address.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10709298836193bb30e181b4-97064041%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd6b0d0aeca1a82f5c0299c457328e2de5d0003d0' => 
    array (
      0 => '/var/www/html/themes/ez20/order-address.tpl',
      1 => 1636107412,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10709298836193bb30e181b4-97064041',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'opc' => 0,
    'formatedAddressFieldsValuesList' => 0,
    'addresses' => 0,
    'address' => 0,
    'address_key' => 0,
    'ignoreList' => 0,
    'id_address' => 0,
    'address_key_number' => 0,
    'address_content' => 0,
    'currencySign' => 0,
    'currencyRate' => 0,
    'currencyFormat' => 0,
    'currencyBlank' => 0,
    'type' => 0,
    'field_name' => 0,
    'pattern_name' => 0,
    'link' => 0,
    'back_order_page' => 0,
    'back' => 0,
    'cart' => 0,
    'oldMessage' => 0,
    'cfcheck' => 0,
    'check_tx' => 0,
    'check_vat' => 0,
    'check_phone' => 0,
    'img_ps_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_6193bb30e793b9_26085885',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6193bb30e793b9_26085885')) {function content_6193bb30e793b9_26085885($_smarty_tpl) {?><?php if (!is_callable('smarty_function_counter')) include '/var/www/html/tools/smarty/plugins/function.counter.php';
?>

<?php if ($_smarty_tpl->tpl_vars['opc']->value) {?>
	<?php $_smarty_tpl->tpl_vars["back_order_page"] = new Smarty_variable("order-opc.php", null, 0);?>
<?php } else { ?>
	<?php $_smarty_tpl->tpl_vars["back_order_page"] = new Smarty_variable("order.php", null, 0);?>
<?php }?>




<?php if (!isset($_smarty_tpl->tpl_vars['formatedAddressFieldsValuesList']->value)) {?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[0] = "id_address";?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[1] = "id_country";?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[2] = "id_state";?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[3] = "id_customer";?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[4] = "id_manufacturer";?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[5] = "id_supplier";?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[6] = "date_add";?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[7] = "date_upd";?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[8] = "active";?>
	<?php $_smarty_tpl->createLocalArrayVariable('ignoreList', null, 0);
$_smarty_tpl->tpl_vars['ignoreList']->value[9] = "deleted";?>	
	
	
	<?php if (isset($_smarty_tpl->tpl_vars['addresses']->value)) {?>
		<?php  $_smarty_tpl->tpl_vars['address'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['address']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['addresses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['address']->key => $_smarty_tpl->tpl_vars['address']->value) {
$_smarty_tpl->tpl_vars['address']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['address']->key;
?>
			<?php echo smarty_function_counter(array('start'=>0,'skip'=>1,'assign'=>'address_key_number'),$_smarty_tpl);?>

			<?php $_smarty_tpl->tpl_vars['id_address'] = new Smarty_variable($_smarty_tpl->tpl_vars['address']->value['id_address'], null, 0);?>
			<?php  $_smarty_tpl->tpl_vars['address_content'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['address_content']->_loop = false;
 $_smarty_tpl->tpl_vars['address_key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['address']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['address_content']->key => $_smarty_tpl->tpl_vars['address_content']->value) {
$_smarty_tpl->tpl_vars['address_content']->_loop = true;
 $_smarty_tpl->tpl_vars['address_key']->value = $_smarty_tpl->tpl_vars['address_content']->key;
?>
				<?php if (!in_array($_smarty_tpl->tpl_vars['address_key']->value,$_smarty_tpl->tpl_vars['ignoreList']->value)) {?>
					<?php $_smarty_tpl->createLocalArrayVariable('formatedAddressFieldsValuesList', null, 0);
$_smarty_tpl->tpl_vars['formatedAddressFieldsValuesList']->value[$_smarty_tpl->tpl_vars['id_address']->value]['ordered_fields'][$_smarty_tpl->tpl_vars['address_key_number']->value] = $_smarty_tpl->tpl_vars['address_key']->value;?>
					<?php $_smarty_tpl->createLocalArrayVariable('formatedAddressFieldsValuesList', null, 0);
$_smarty_tpl->tpl_vars['formatedAddressFieldsValuesList']->value[$_smarty_tpl->tpl_vars['id_address']->value]['formated_fields_values'][$_smarty_tpl->tpl_vars['address_key']->value] = $_smarty_tpl->tpl_vars['address_content']->value;?>
					<?php echo smarty_function_counter(array(),$_smarty_tpl);?>

				<?php }?>
			<?php } ?>
		<?php } ?>
	<?php }?>
<?php }?>

<script type="text/javascript">
// <![CDATA[
	<?php if (!$_smarty_tpl->tpl_vars['opc']->value) {?>
	var orderProcess = 'order';
	var currencySign = '<?php echo html_entity_decode($_smarty_tpl->tpl_vars['currencySign']->value,2,"UTF-8");?>
';
	var currencyRate = '<?php echo floatval($_smarty_tpl->tpl_vars['currencyRate']->value);?>
';
	var currencyFormat = '<?php echo intval($_smarty_tpl->tpl_vars['currencyFormat']->value);?>
';
	var currencyBlank = '<?php echo intval($_smarty_tpl->tpl_vars['currencyBlank']->value);?>
';
	var txtProduct = "<?php echo smartyTranslate(array('s'=>'product'),$_smarty_tpl);?>
";
	var txtProducts = "<?php echo smartyTranslate(array('s'=>'products'),$_smarty_tpl);?>
";
	<?php }?>

	var formatedAddressFieldsValuesList = new Array();

	<?php  $_smarty_tpl->tpl_vars['type'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['type']->_loop = false;
 $_smarty_tpl->tpl_vars['id_address'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['formatedAddressFieldsValuesList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['type']->key => $_smarty_tpl->tpl_vars['type']->value) {
$_smarty_tpl->tpl_vars['type']->_loop = true;
 $_smarty_tpl->tpl_vars['id_address']->value = $_smarty_tpl->tpl_vars['type']->key;
?>
		formatedAddressFieldsValuesList[<?php echo $_smarty_tpl->tpl_vars['id_address']->value;?>
] =
		{
			'ordered_fields':[
				<?php  $_smarty_tpl->tpl_vars['field_name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field_name']->_loop = false;
 $_smarty_tpl->tpl_vars['num_field'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['type']->value['ordered_fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['field_name']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['field_name']->key => $_smarty_tpl->tpl_vars['field_name']->value) {
$_smarty_tpl->tpl_vars['field_name']->_loop = true;
 $_smarty_tpl->tpl_vars['num_field']->value = $_smarty_tpl->tpl_vars['field_name']->key;
 $_smarty_tpl->tpl_vars['field_name']->index++;
 $_smarty_tpl->tpl_vars['field_name']->first = $_smarty_tpl->tpl_vars['field_name']->index === 0;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['inv_loop']['first'] = $_smarty_tpl->tpl_vars['field_name']->first;
?>
					<?php if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['inv_loop']['first']) {?>,<?php }?>"<?php echo $_smarty_tpl->tpl_vars['field_name']->value;?>
"
				<?php } ?>
			],
			'formated_fields_values':{
					<?php  $_smarty_tpl->tpl_vars['field_name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field_name']->_loop = false;
 $_smarty_tpl->tpl_vars['pattern_name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['type']->value['formated_fields_values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['field_name']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['field_name']->key => $_smarty_tpl->tpl_vars['field_name']->value) {
$_smarty_tpl->tpl_vars['field_name']->_loop = true;
 $_smarty_tpl->tpl_vars['pattern_name']->value = $_smarty_tpl->tpl_vars['field_name']->key;
 $_smarty_tpl->tpl_vars['field_name']->index++;
 $_smarty_tpl->tpl_vars['field_name']->first = $_smarty_tpl->tpl_vars['field_name']->index === 0;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['inv_loop']['first'] = $_smarty_tpl->tpl_vars['field_name']->first;
?>
						<?php if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['inv_loop']['first']) {?>,<?php }?>"<?php echo $_smarty_tpl->tpl_vars['pattern_name']->value;?>
":"<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['field_name']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"
					<?php } ?>
				}
		}
	<?php } ?>

	function getAddressesTitles()
	{
		return {
						'invoice': "<?php echo smartyTranslate(array('s'=>'Your billing address'),$_smarty_tpl);?>
",
						'delivery': "<?php echo smartyTranslate(array('s'=>'Your delivery address'),$_smarty_tpl);?>
"
			};

	}


	function buildAddressBlock(id_address, address_type, dest_comp)
	{
		var adr_titles_vals = getAddressesTitles();
		/*var li_content = formatedAddressFieldsValuesList[id_address]['formated_fields_values'];*/
var li_content = '';
		var ordered_fields_name = ['title'];

		ordered_fields_name = ordered_fields_name.concat(formatedAddressFieldsValuesList[id_address]['ordered_fields']);
		ordered_fields_name = ordered_fields_name.concat(['update']);
		
		dest_comp.html('');

		li_content['title'] = adr_titles_vals[address_type];
		li_content['update'] = '<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('address.php',true);?>
?id_address='+id_address+'&amp;back=<?php echo $_smarty_tpl->tpl_vars['back_order_page']->value;?>
?step=1<?php if ($_smarty_tpl->tpl_vars['back']->value) {?>&mod=<?php echo $_smarty_tpl->tpl_vars['back']->value;?>
<?php }?>" title="<?php echo smartyTranslate(array('s'=>'Update'),$_smarty_tpl);?>
"></a>';

		appendAddressList(dest_comp, li_content, ordered_fields_name);
	}

	function appendAddressList(dest_comp, values, fields_name)
	{
		for (var item in fields_name)
		{
			var name = fields_name[item];
			var value = getFieldValue(name, values);
			if (value != "")
			{
				var new_li = document.createElement('li');
				new_li.className = 'address_'+ name;

				
				if(name == 'firstname' || name == 'lastname') { 
				new_li.className = '';
				}
				else {
				new_li.className = 'address_'+ name;
				}
				
				if(name == 'firstname') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-15px"><tr><td style="width:80px">Nome</td> <td>' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else if(name == 'cognome') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-15px"><tr><td style="width:80px">Cognome</td> <td> ' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else if(name == 'address1') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-15px"><tr><td style="width:80px">Indirizzo</td> <td> ' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else if(name == 'postcode') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-15px"><tr><td style="width:80px">CAP</td> <td>' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else if(name == 'city') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-15px"><tr><td style="width:80px">Citt&agrave;</td> <td> ' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else if(name == 'State:name') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-15px"><tr><td style="width:80px">Provincia</td> <td>' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else if(name == 'Country:name') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-15px"><tr><td style="width:80px">Nazione</td> <td>' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else if(name == 'phone') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-15px"><tr><td style="width:80px">Telefono</td> <td>' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else if(name == 'phone_mobile') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-15px"><tr><td style="width:80px">Cellulare</td> <td>' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else {
				new_li.innerHTML = '' + getFieldValue(name, values) + '';
				}
				
				
				dest_comp.append(new_li);
			}
		}
	}

	function getFieldValue(field_name, values)
	{
		var reg=new RegExp("[ ]+", "g");

		var items = field_name.split(reg);
		var vals = new Array();

		for (var field_item in items)
			vals.push(values[items[field_item]]);
		return vals.join(" ");
	}

//]]>
</script>

<?php if (!$_smarty_tpl->tpl_vars['opc']->value) {?>
<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'Addresses'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php }?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php if (!$_smarty_tpl->tpl_vars['opc']->value) {?><h1><?php echo smartyTranslate(array('s'=>'Addresses'),$_smarty_tpl);?>
</h1><?php } else { ?><h2>1. <?php echo smartyTranslate(array('s'=>'Addresses'),$_smarty_tpl);?>
</h2><?php }?>

<?php if (!$_smarty_tpl->tpl_vars['opc']->value) {?>
<?php $_smarty_tpl->tpl_vars['current_step'] = new Smarty_variable('address', null, 0);?>

<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./order-steps.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div id="address-form-short">
<div class="shopping_cart_table">

<form  action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink($_smarty_tpl->tpl_vars['back_order_page']->value,true);?>
"  method="post">
	<?php } else { ?>
	<div id="opc_account" class="opc-main-block">
		<div id="opc_account-overlay" class="opc-overlay" style="display:none;"></div>
	<?php }?>
		<div style="text-align:center; padding-top: 25px;">
			<p class="address_delivery select">
				<label for="id_address_delivery"><?php echo smartyTranslate(array('s'=>'Choose a delivery address:'),$_smarty_tpl);?>
</label>
				<select name="id_address_delivery" id="id_address_delivery" class="address_select" onchange="updateAddressesDisplay();<?php if ($_smarty_tpl->tpl_vars['opc']->value) {?>updateAddressSelection();<?php }?>">

				<?php  $_smarty_tpl->tpl_vars['address'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['address']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['addresses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['address']->key => $_smarty_tpl->tpl_vars['address']->value) {
$_smarty_tpl->tpl_vars['address']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['address']->key;
?>
					<option value="<?php echo intval($_smarty_tpl->tpl_vars['address']->value['id_address']);?>
" <?php if ($_smarty_tpl->tpl_vars['address']->value['id_address']==$_smarty_tpl->tpl_vars['cart']->value->id_address_delivery) {?>selected="selected"<?php }?>><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['address']->value['alias'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
				<?php } ?>
				
				</select>
			</p>
			<p class="checkbox" style="display:none;">
				<input type="checkbox" name="same" id="addressesAreEquals" value="1" onclick="updateAddressesDisplay();<?php if ($_smarty_tpl->tpl_vars['opc']->value) {?>updateAddressSelection();<?php }?>" <?php if ($_smarty_tpl->tpl_vars['cart']->value->id_address_invoice==$_smarty_tpl->tpl_vars['cart']->value->id_address_delivery||count($_smarty_tpl->tpl_vars['addresses']->value)==1) {?><?php }?> />
				<label for="addressesAreEquals"><?php echo smartyTranslate(array('s'=>'Use the same address for billing.'),$_smarty_tpl);?>
</label>
			</p>
			
			<p id="address_invoice_form" class="select" style="display: none;">
			
			
				<label for="id_address_invoice" style="display: none;" class="strong"><?php echo smartyTranslate(array('s'=>'Choose a billing address:'),$_smarty_tpl);?>
</label>
				<select style="display:none" name="id_address_invoice" id="id_address_invoice" class="address_select">
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['address'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['address']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['address']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['addresses']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['address']['name'] = 'address';
$_smarty_tpl->tpl_vars['smarty']->value['section']['address']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['address']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['address']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['address']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['address']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['address']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['address']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['address']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['address']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['address']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['address']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['address']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['address']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['address']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['address']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['address']['total']);
?>
					<option value="<?php echo intval($_smarty_tpl->tpl_vars['addresses']->value[$_smarty_tpl->getVariable('smarty')->value['section']['address']['index']]['id_address']);?>
" <?php if ($_smarty_tpl->tpl_vars['addresses']->value[$_smarty_tpl->getVariable('smarty')->value['section']['address']['index']]['id_address']==$_smarty_tpl->tpl_vars['cart']->value->id_address_invoice&&$_smarty_tpl->tpl_vars['cart']->value->id_address_delivery!=$_smarty_tpl->tpl_vars['cart']->value->id_address_invoice) {?>selected="selected"<?php }?>><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['addresses']->value[$_smarty_tpl->getVariable('smarty')->value['section']['address']['index']]['alias'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
				<?php endfor; endif; ?>
				</select>
				
			</p>
			<div style="display: none;">
				<ul class="address alternate_item <?php if ($_smarty_tpl->tpl_vars['cart']->value->isVirtualCart()) {?>full_width<?php }?>" id="address_invoice">
				</ul>

				<ul class="address item" id="address_delivery" <?php if ($_smarty_tpl->tpl_vars['cart']->value->isVirtualCart()) {?>style="display:none;"<?php }?>>
			
				</ul>
			</div>
			

			<p>Oppure</p>
			
			<p class="address_add submit">
				<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('address.php',true);?>
?back=<?php echo $_smarty_tpl->tpl_vars['back_order_page']->value;?>
?step=1<?php if ($_smarty_tpl->tpl_vars['back']->value) {?>&mod=<?php echo $_smarty_tpl->tpl_vars['back']->value;?>
<?php }?>" title="<?php echo smartyTranslate(array('s'=>'Add'),$_smarty_tpl);?>
" class="button_large"  id="add-new-address"><?php echo smartyTranslate(array('s'=>'Add a new address'),$_smarty_tpl);?>
</a>
			</p>
			
		</div>
		<?php if (!$_smarty_tpl->tpl_vars['opc']->value) {?>
		</div>
		<?php } else { ?>
		</div>
		<?php }?>

		<?php if (!$_smarty_tpl->tpl_vars['opc']->value) {?>
				<div id="ordermsg" style="padding-top: 25px; width: 100%;">
					<p><?php echo smartyTranslate(array('s'=>'If you would like to add a comment about your order, please write it below.'),$_smarty_tpl);?>
</p>
					<p class="textarea"><textarea cols="90" rows="3" name="message" style="width: 100%;"><?php if (isset($_smarty_tpl->tpl_vars['oldMessage']->value)) {?><?php echo $_smarty_tpl->tpl_vars['oldMessage']->value;?>
<?php }?></textarea></p>
				</div>
		<?php }?>




		

			
				
				<?php if ($_smarty_tpl->tpl_vars['cfcheck']->value==0) {?>
				<p style="text-align: center; color: #ff2b2b; font-size: 25px; font-weight: bold;">Attenzione!</p>
		<?php echo smartyTranslate(array('s'=>'There are some details missing in your account: if you are a end user customer you must specify your tax code in your account details, and if you are a company you must specify both tax code and vat number'),$_smarty_tpl);?>
. <a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account.php',true);?>
"><?php echo smartyTranslate(array('s'=>'Click here to go to your account and update your profile details'),$_smarty_tpl);?>
</a><br /><br />
		<?php echo smartyTranslate(array('s'=>'Here there are your missing details'),$_smarty_tpl);?>
:<br /><br />

		<?php if ($_smarty_tpl->tpl_vars['check_tx']->value==1) {?> <a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('identity.php',true);?>
" target="_blank"><?php echo smartyTranslate(array('s'=>'Tax code'),$_smarty_tpl);?>
</a><?php } else { ?><?php }?>
		<?php if ($_smarty_tpl->tpl_vars['check_vat']->value==1) {?> <a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('identity.php',true);?>
" target="_blank"><?php echo smartyTranslate(array('s'=>'VAT number'),$_smarty_tpl);?>
</a><?php } else { ?><?php }?>
		<?php if ($_smarty_tpl->tpl_vars['check_phone']->value==1) {?> <a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('addresses.php',true);?>
" target="_blank"><?php echo smartyTranslate(array('s'=>'Phone number'),$_smarty_tpl);?>
</a><?php } else { ?><?php }?>

		<?php } else { ?>
			<p class="cart_navigation submit" id="prev-next-carrier-col">
				<input type="submit" name="processAddress" value="<?php echo smartyTranslate(array('s'=>'Next'),$_smarty_tpl);?>
 &raquo;" class="exclusive" style="display: none;" />
				<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink($_smarty_tpl->tpl_vars['back_order_page']->value,false);?>
?step=2" title="<?php echo smartyTranslate(array('s'=>'Next'),$_smarty_tpl);?>
" class="button_large" style="padding: 8px 20px; height: auto;"> <?php echo smartyTranslate(array('s'=>'Next'),$_smarty_tpl);?>
 &raquo;</a> 
				<?php }?>
				<input type="hidden" class="hidden" name="step" value="2" />
				<input type="hidden" name="back" value="<?php echo $_smarty_tpl->tpl_vars['back']->value;?>
" />
				<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink($_smarty_tpl->tpl_vars['back_order_page']->value,true);?>
?step=0<?php if ($_smarty_tpl->tpl_vars['back']->value) {?>&back=<?php echo $_smarty_tpl->tpl_vars['back']->value;?>
<?php }?>" title="<?php echo smartyTranslate(array('s'=>'Previous'),$_smarty_tpl);?>
" class="button_large" style="padding: 8px 20px; height: auto;">&laquo; <?php echo smartyTranslate(array('s'=>'Previous'),$_smarty_tpl);?>
</a>
			</p>
	

</form>

</div>




<div style="border: 1px solid #e5e8eb; 
			box-shadow: 0 1px 1px rgba(0,0,0,0.15), 
        	0 2px 2px rgba(0,0,0,0.15), 
            0 4px 4px rgba(0,0,0,0.15), 
        	0 8px 8px rgba(0,0,0,0.15);
			border-radius:12px;
			padding:30px; 
			display: flex;
			width: 100%;" id="need-help">
		<div style="height: 140px; padding-right: 30px;">
			<img style="height:100%;" src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
business-woman.jpg' alt='Aiuto' title='Aiuto' />
		</div>
		<div>
			<h5 style="font-size: 22px; font-weight: 600;">Hai bisogno di aiuto con il tuo ordine?</h5>
			<div style="display:flex; align-items:center;" id="numero-verde"><p>Chiamaci al numero verde</p><a href="#" style="padding-left: 12px; padding-bottom: 10px;"><img style="height:50px;" src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
numero-verde.jpg' alt='Numero verde' /></a></div>
			
			<p>Oppure apri un ticket cliccando <a target='_blank' href='https://www.ezdirect.it/contattaci?step=assistenza-ordini'>qui</a>. Un esperto di risponderà quanto prima.</p>
		</div>
</div><?php }} ?>
