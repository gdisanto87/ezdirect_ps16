<?php /* Smarty version Smarty-3.1.19, created on 2022-01-07 19:04:10
         compiled from "/var/www/html/themes/ez20/product-list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:32169883261d8809a1dc912-22057801%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '863769799336cea1f4f7c0a622ff1f276f06499a' => 
    array (
      0 => '/var/www/html/themes/ez20/product-list.tpl',
      1 => 1638351698,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '32169883261d8809a1dc912-22057801',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'js_dir' => 0,
    'products' => 0,
    'product' => 0,
    'img_ps_dir' => 0,
    'link' => 0,
    'search_products' => 0,
    'cookie' => 0,
    'comparator_max_item' => 0,
    'compareProducts' => 0,
    'img_dir' => 0,
    'i' => 0,
    'quantity_discount' => 0,
    'restricted_country_mode' => 0,
    'priceDisplay' => 0,
    'PS_CATALOG_MODE' => 0,
    'add_prod_display' => 0,
    'quantityBackup' => 0,
    'static_token' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61d8809a258f92_88060410',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61d8809a258f92_88060410')) {function content_61d8809a258f92_88060410($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/var/www/html/tools/smarty/plugins/modifier.replace.php';
?>

<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['js_dir']->value;?>
etc/stars.js">
</script>




<?php if (isset($_smarty_tpl->tpl_vars['products']->value)) {?>

	<!-- Products list -->
	<ul id="product_list" class="clear">
	<?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['product']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['product']->iteration=0;
 $_smarty_tpl->tpl_vars['product']->index=-1;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['products']['index']=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
 $_smarty_tpl->tpl_vars['product']->iteration++;
 $_smarty_tpl->tpl_vars['product']->index++;
 $_smarty_tpl->tpl_vars['product']->first = $_smarty_tpl->tpl_vars['product']->index === 0;
 $_smarty_tpl->tpl_vars['product']->last = $_smarty_tpl->tpl_vars['product']->iteration === $_smarty_tpl->tpl_vars['product']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['products']['first'] = $_smarty_tpl->tpl_vars['product']->first;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['products']['index']++;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['products']['last'] = $_smarty_tpl->tpl_vars['product']->last;
?>
	
		<?php if ($_GET['id_category']==204||$_GET['id_category']==86||$_GET['id_category']==189) {?>
		<?php } else { ?>
			<li class="ajax_block_product <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['products']['first']) {?>first_item<?php } elseif ($_smarty_tpl->getVariable('smarty')->value['foreach']['products']['last']) {?>last_item<?php }?> <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['products']['index']%2) {?>alternate_item<?php } else { ?>item<?php }?> clearfix">
			
			<div class="center_block">
				<div class="center_block_left">
				<?php if ($_smarty_tpl->tpl_vars['product']->value['location']=='bundle') {?>
				<div class="kit-listing">
				<img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
kit.png" alt="Kit" title="Kit" width="45" height="45" />
				</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['product']->value['cheapnet']=='cheapnet_6') {?>
				<div class="cheapnet">
				<a href='https://www.ezdirect.it/cheapnetcoupons.php'><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
cheapnet-300.png" alt="Con questo prodotto hai diritto a un Coupon Cheapnet da 300 minuti gratis" title="Con questo prodotto hai diritto a un Coupon Cheapnet da 300 minuti gratis" width="57" height="57" /></a>
				</div>
				<?php } elseif ($_smarty_tpl->tpl_vars['product']->value['cheapnet']=='cheapnet_30') {?>
				<div class="cheapnet">
				<a href='https://www.ezdirect.it/cheapnetcoupons.php'><img src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
cheapnet-600.png" alt="Con questo prodotto hai diritto a un Coupon Cheapnet da 600 minuti gratis" title="Con questo prodotto hai diritto a un Coupon Cheapnet da 600 minuti gratis" width="57" height="57" /></a>
				</div>

				<?php } else { ?>
				<?php }?>
				
				
				
				<?php if ($_smarty_tpl->tpl_vars['product']->value['condition']=='refurbished') {?>
					<div style="position:absolute; top:0px; left:0px"><img style="width:40px" src="https://www.ezdirect.it/img/ricondizionato.png" title="Ricondizionato" alt="Ricondizionato" /></div>
				<?php }?>
				
				<!-- <?php if ($_smarty_tpl->tpl_vars['product']->value['migliorprezzo_link']==1&&$_smarty_tpl->tpl_vars['product']->value['virtual']==0&&$_smarty_tpl->tpl_vars['product']->value['price_tax_exc']>0&&($_smarty_tpl->tpl_vars['product']->value['fuori_produzione']!=1&&!preg_match("/fuori produzione/i",$_smarty_tpl->tpl_vars['product']->value['description_short']))) {?> -->
						
					<?php if ($_smarty_tpl->tpl_vars['product']->value['id_manufacturer']==133) {?> <div class="ribbon_gold" style="right:5px; cursor:pointer" onclick="window.location = '<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
'"><span>Miglior prezzo</span></div> <?php }?>
					
				<!-- <?php }?> -->
				
				<?php if ($_smarty_tpl->tpl_vars['product']->value['abilita_banda']==1) {?>
					<div class="ribbon" style="right:5px; cursor:pointer" onclick="window.location = '<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
'"><span>Offerta speciale</span></div>
				<?php }?>				

<?php if ($_smarty_tpl->tpl_vars['product']->value['fuori_produzione']==1||preg_match("/fuori produzione/i",$_smarty_tpl->tpl_vars['product']->value['description_short'])) {?>
				<div class="ribbon" style="right:5px; cursor:pointer" onclick="window.location = '<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
'"><span><?php echo smartyTranslate(array('s'=>'Out of production'),$_smarty_tpl);?>
</span></div>
			<?php }?>
			
					<a class="product_img_link" href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><img class="product_img_link_2" src="<?php if (strpos($_smarty_tpl->tpl_vars['product']->value['id_product'],"b")) {?>https://www.ezdirect.it/img/b/<?php echo $_smarty_tpl->tpl_vars['product']->value['id_image'];?>
-large.jpg<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['id_image'],'medium');?>
<?php }?>" alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['legend'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"  <?php if ($_smarty_tpl->tpl_vars['product']->value['secondary_image']!='') {?>onmouseover="this.src='<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['secondary_image'],'medium');?>
'" onmouseout="this.src='<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['id_image'],'medium');?>
'"<?php }?> width="160" height="160" /></a>
					<?php if ($_smarty_tpl->tpl_vars['search_products']->value) {?><br />
						<span class="improve-your-search">
						<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['product']->value['id_category_default'],$_smarty_tpl->tpl_vars['link']->value->getCatRewrite($_smarty_tpl->tpl_vars['product']->value['id_category_default']));?>
"><?php echo smartyTranslate(array('s'=>'Improve your search'),$_smarty_tpl);?>
</a>
						</span>
					<?php }?>
				</div>
					
				<h3 class="listing_titles"><?php if (isset($_smarty_tpl->tpl_vars['product']->value['new'])&&$_smarty_tpl->tpl_vars['product']->value['new']==1) {?><?php }?><a class="product_name_title" href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php if (strpos($_smarty_tpl->tpl_vars['product']->value['id_product'],"b")) {?>id="bundle_name_title_bundle-<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['product']->value['id_product'],'b','');?>
"<?php }?>><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</a></h3>
				<p class="product_codes">
				<?php if ($_smarty_tpl->tpl_vars['product']->value['reference']) {?>
					<?php echo smartyTranslate(array('s'=>'Reference:'),$_smarty_tpl);?>
 <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['reference'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

				<?php } else { ?><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['product']->value['supplier_reference']) {?>
					&nbsp;&nbsp;&nbsp;&nbsp; <?php echo smartyTranslate(array('s'=>'SKU reference:'),$_smarty_tpl);?>
 <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['supplier_reference'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

				<?php } else { ?><?php }?>
					
				</p>
				<p class="product_desc" style="font-size:12px; padding-right: 30px;"><a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate(strip_tags($_smarty_tpl->tpl_vars['product']->value['description_short']),140,'...');?>
"><?php echo strip_tags($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['description_short'],140,'...'));?>
</a>
				<br /><br />
				<?php if ($_smarty_tpl->tpl_vars['product']->value['punti_di_forza']) {?>
					
					
					<br /><br />
				<?php } else { ?>
				<?php }?>
				
					
				<div id="bottom-line" style="line-height: 17px; display: flex; flex-direction: column;">
	
					<div style="margin-bottom: 20px;">
						<?php if ($_smarty_tpl->tpl_vars['product']->value['login_for_offer']==1&&!$_smarty_tpl->tpl_vars['cookie']->value->isLogged()) {?>
							<a rel="nofollow" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('authentication.php',true);?>
"><strong style="background-color: #fff785; color:#ff0000" class="listing_titles">Accedi per vedere il prezzo scontato</strong></a>
					
						<?php }?>
					</div>
					
				
					<div style="display: flex;">
						<div class="reviews-line">
							<a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
#idTab5" rel="nofollow"><span class="stars"><?php echo $_smarty_tpl->tpl_vars['product']->value['averageTotal'];?>
</span> <span class="total_reviews listing_titles">(<?php echo $_smarty_tpl->tpl_vars['product']->value['total_reviews'];?>
 <?php echo smartyTranslate(array('s'=>'reviews'),$_smarty_tpl);?>
)</span></a>
						</div>
						<div class="reviews-line comparator-item">
							<?php if (isset($_smarty_tpl->tpl_vars['comparator_max_item']->value)&&$_smarty_tpl->tpl_vars['comparator_max_item']->value) {?>
								<?php if ($_smarty_tpl->tpl_vars['product']->value['location']=='bundle') {?>
								<?php } else { ?>
									<input type="checkbox" style="margin-top:2px" class="comparator" id="comparator_item_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
" value="comparator_item_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['compareProducts']->value)&&in_array($_smarty_tpl->tpl_vars['product']->value['id_product'],$_smarty_tpl->tpl_vars['compareProducts']->value)) {?>checked<?php }?>/> <label for="comparator_item_<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
"></label> <?php echo smartyTranslate(array('s'=>'Confronta'),$_smarty_tpl);?>

								<?php }?>
							<?php }?>
						</div>
					</div>
					<?php if ($_smarty_tpl->tpl_vars['product']->value['migliorprezzo_link']==1&&$_smarty_tpl->tpl_vars['product']->value['price_tax_exc']>0&&($_smarty_tpl->tpl_vars['product']->value['fuori_produzione']!=1&&!preg_match("/fuori produzione/i",$_smarty_tpl->tpl_vars['product']->value['description_short']))) {?>
						
						<!-- <div class="reviews-line get-best-price"><a rel="nofollow" href="https://www.ezdirect.it/modules/formprevendita/form.php?category=<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
"><img src='<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
logo-miglior-prezzo.png' alt='Ottieni miglior prezzo' title='Ottieni miglior prezzo' /><strong style="color:#ff0000" class="listing_titles">Ottieni miglior prezzo</strong></a></div> -->
					<?php }?>
					
					
				<div style="clear:both"></div>
				</div>
				</p>
			</div>	
			<div class="list_right_block">			
					<div class='acquista-listing' style="padding:15px;">
						<table style="width:100%">
						<tr>
							
								<p style='text-align:left;'>
						
						
									<?php if ($_smarty_tpl->tpl_vars['product']->value['location']=='bundle') {?>
										<?php if ($_smarty_tpl->tpl_vars['product']->value['real_quantity']>0) {?>
											<img style="width: 13px; height: auto; margin-right: 3px; margin-bottom: 2px;" src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
available.png' alt='<?php echo smartyTranslate(array('s'=>'Available'),$_smarty_tpl);?>
' /><span style="font-size: 12px;"><?php echo smartyTranslate(array('s'=>'Prodotto disponibile'),$_smarty_tpl);?>
</span>
										<?php } else { ?>
											<p style="font-size: 13px; "><img style='width:13px; margin-bottom: 3px; margin-right: 3px;' src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
orange_alert_2.jpg'  alt='<?php echo smartyTranslate(array('s'=>'Ordinabile'),$_smarty_tpl);?>
' /><?php echo smartyTranslate(array('s'=>'Prodotto ordinabile'),$_smarty_tpl);?>
</p>
										<?php }?>
									<?php } elseif ($_smarty_tpl->tpl_vars['product']->value['fuori_produzione']==0&&$_smarty_tpl->tpl_vars['product']->value['price']>0||($_smarty_tpl->tpl_vars['product']->value['fuori_produzione']==1&&$_smarty_tpl->tpl_vars['product']->value['price']>0&&$_smarty_tpl->tpl_vars['product']->value['real_quantity']>0)) {?>
										<?php if ($_smarty_tpl->tpl_vars['product']->value['id_category_default']==119) {?>
											<img style="width: 13px; height: auto; margin-right: 3px; margin-bottom: 2px;" src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
available.png' alt='<?php echo smartyTranslate(array('s'=>'Available'),$_smarty_tpl);?>
' /><span style="font-size: 12px;"><?php echo smartyTranslate(array('s'=>'Prodotto disponibile'),$_smarty_tpl);?>
</span>
										<?php } else { ?>
											<?php if ($_smarty_tpl->tpl_vars['product']->value['real_quantity']>0) {?>
												
												
												<img style="width: 13px; height: auto; margin-right: 3px; margin-bottom: 2px;" src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
available.png' alt='<?php echo smartyTranslate(array('s'=>'Available'),$_smarty_tpl);?>
' /><span style="font-size: 12px;"><?php echo smartyTranslate(array('s'=>'Prodotto disponibile'),$_smarty_tpl);?>
</span>
												
											<?php } else { ?>
												<p style="font-size: 13px; "><img style='width:13px; margin-bottom: 3px; margin-right: 3px;' src='<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
orange_alert_2.jpg'  alt='<?php echo smartyTranslate(array('s'=>'Ordinabile'),$_smarty_tpl);?>
' /><?php echo smartyTranslate(array('s'=>'Prodotto ordinabile'),$_smarty_tpl);?>
</p>
											<?php }?>
										<?php }?>
									<?php } else { ?>
									<div style="width:110px">&nbsp;</div>
									<?php }?>
							
								</p>
							
						</tr>
						<?php if ($_smarty_tpl->tpl_vars['product']->value['listino']>0&&$_smarty_tpl->tpl_vars['product']->value['price']>0&&$_smarty_tpl->tpl_vars['product']->value['fuori_produzione']==0&&$_smarty_tpl->tpl_vars['product']->value['listino']>$_smarty_tpl->tpl_vars['product']->value['price_tax_exc']||($_smarty_tpl->tpl_vars['product']->value['fuori_produzione']==1&&$_smarty_tpl->tpl_vars['product']->value['price']>0&&$_smarty_tpl->tpl_vars['product']->value['quantity']>0)) {?>
							<tr><td style="width:100%;  font-size: 13px;"><?php echo smartyTranslate(array('s'=>'Prezzo di listino'),$_smarty_tpl);?>
</td> <td style="text-align:right"><span style="text-decoration:line-through; color:#383838"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['product']->value['listino']),$_smarty_tpl);?>
</span> <?php echo smartyTranslate(array('s'=>'t.e.'),$_smarty_tpl);?>
</td></tr>
						<?php } else { ?>
						<?php }?>
						<tr><td style="width:40%; font-size: 13px; ">Prezzo <?php if ($_smarty_tpl->tpl_vars['product']->value['prezzospeciale']==1) {?>speciale<?php }?></td><td style="text-align:right; ">
						<?php if (($_smarty_tpl->tpl_vars['product']->value['quantity_discount']&&$_smarty_tpl->tpl_vars['product']->value['price']>0&&$_smarty_tpl->tpl_vars['product']->value['fuori_produzione']==0)||($_smarty_tpl->tpl_vars['product']->value['fuori_produzione']==1&&$_smarty_tpl->tpl_vars['product']->value['price']>0&&$_smarty_tpl->tpl_vars['product']->value['quantity_discount']&&$_smarty_tpl->tpl_vars['product']->value['quantity']>0)) {?>
							
								<?php $_smarty_tpl->tpl_vars["i"] = new Smarty_variable("0", null, 0);?>
								<?php $_smarty_tpl->tpl_vars['numItems'] = new Smarty_variable(count($_smarty_tpl->tpl_vars['product']->value['quantity_discount']), null, 0);?>
								<?php  $_smarty_tpl->tpl_vars['quantity_discount'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['quantity_discount']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product']->value['quantity_discount']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['quantity_discount']->key => $_smarty_tpl->tpl_vars['quantity_discount']->value) {
$_smarty_tpl->tpl_vars['quantity_discount']->_loop = true;
?>
									
									<?php if ($_smarty_tpl->tpl_vars['i']->value==0) {?>				
										<span class="price"> <?php echo smartyTranslate(array('s'=>'Da'),$_smarty_tpl);?>

										<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['quantity_discount']->value['price']*floatval((1-$_smarty_tpl->tpl_vars['quantity_discount']->value['reduction']))),$_smarty_tpl);?>
<?php echo smartyTranslate(array('s'=>'t.e.'),$_smarty_tpl);?>
</span>
									<?php } else { ?>	
									<?php }?>
									<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
								<?php } ?>
							
						
						<?php } elseif (($_smarty_tpl->tpl_vars['product']->value['price']>0&&$_smarty_tpl->tpl_vars['product']->value['fuori_produzione']==0)||($_smarty_tpl->tpl_vars['product']->value['fuori_produzione']==1&&$_smarty_tpl->tpl_vars['product']->value['price']>0&&$_smarty_tpl->tpl_vars['product']->value['quantity']>0)) {?>
								<?php if (isset($_smarty_tpl->tpl_vars['product']->value['show_price'])&&$_smarty_tpl->tpl_vars['product']->value['price']>0&&$_smarty_tpl->tpl_vars['product']->value['show_price']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)) {?><span class="price"><?php if (!$_smarty_tpl->tpl_vars['priceDisplay']->value) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['product']->value['price']),$_smarty_tpl);?>
<?php } else { ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['product']->value['price_tax_exc']),$_smarty_tpl);?>
<?php }?> <?php echo smartyTranslate(array('s'=>'t.e.'),$_smarty_tpl);?>
<?php if ($_smarty_tpl->tpl_vars['product']->value['prezzospeciale']==1) {?>*<?php }?></span><?php }?>
						
							<?php }?>
						</td>
						</tr>
						
						
						<?php if ($_smarty_tpl->tpl_vars['product']->value['fuori_produzione']==0&&$_smarty_tpl->tpl_vars['product']->value['quantity_discount']&&$_smarty_tpl->tpl_vars['product']->value['price']>0||($_smarty_tpl->tpl_vars['product']->value['fuori_produzione']==1&&$_smarty_tpl->tpl_vars['product']->value['price']>0&&$_smarty_tpl->tpl_vars['product']->value['quantity_discount']&&$_smarty_tpl->tpl_vars['product']->value['quantity']>0)) {?>
								<tr><td colspan="2"><img style="border:0px; margin-left:-2px" class="sconti_quantita" src="<?php echo $_smarty_tpl->tpl_vars['img_ps_dir']->value;?>
sconti-quantita.jpg" alt="<?php echo smartyTranslate(array('s'=>'Quantity discounts'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'Quantity discounts'),$_smarty_tpl);?>
" /></td></tr>
							<?php }?>
						<?php }?>
						
						</table>
							<p>	
							<?php if ($_smarty_tpl->tpl_vars['cookie']->value->id_default_group==3||$_smarty_tpl->tpl_vars['cookie']->value->id_default_group==10||$_smarty_tpl->tpl_vars['cookie']->value->id_default_group==12) {?>
							<?php } else { ?> 
								<?php if ($_smarty_tpl->tpl_vars['product']->value['freeshipping']==true||$_smarty_tpl->tpl_vars['product']->value['price_tax_exc']>4999999999999999) {?>
									<strong><?php echo smartyTranslate(array('s'=>'Free shipping'),$_smarty_tpl);?>
</strong><br />
								<?php }?>
							<?php }?>	
							
							<?php if ($_smarty_tpl->tpl_vars['product']->value['prezzospeciale']==1) {?>
								<div>
								<?php if ($_smarty_tpl->tpl_vars['product']->value['pezzi_offerta']>0) {?>
									<span style="color:red"><strong>*</strong></span><strong style="color:#000000"><?php echo $_smarty_tpl->tpl_vars['product']->value['pezzi_offerta'];?>
</strong>
									<?php if ($_smarty_tpl->tpl_vars['product']->value['pezzi_offerta']==1) {?>
										<strong><?php echo smartyTranslate(array('s'=>'piece'),$_smarty_tpl);?>
 disponibile</strong>
									<?php } else { ?>
										<strong><?php echo smartyTranslate(array('s'=>'pieces'),$_smarty_tpl);?>
 disponibili</strong>
									<?php }?>
									<span style="color:red">
									<strong><?php echo smartyTranslate(array('s'=>'in special price until '),$_smarty_tpl);?>
 <?php echo $_smarty_tpl->tpl_vars['product']->value['scontofinoa'];?>
</strong></span>
								<?php } else { ?>
									<span style="color:red"><strong><?php echo smartyTranslate(array('s'=>'Special price until '),$_smarty_tpl);?>
 <?php echo $_smarty_tpl->tpl_vars['product']->value['scontofinoa'];?>
</strong></span>
								<?php }?>
								</div>
							<?php }?>
							
						</p>
						
						<?php if (isset($_smarty_tpl->tpl_vars['product']->value['on_sale'])&&$_smarty_tpl->tpl_vars['product']->value['on_sale']&&isset($_smarty_tpl->tpl_vars['product']->value['show_price'])&&$_smarty_tpl->tpl_vars['product']->value['show_price']&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?><span class="on_sale"><?php echo smartyTranslate(array('s'=>'On sale!'),$_smarty_tpl);?>
</span>
						<?php } elseif (isset($_smarty_tpl->tpl_vars['product']->value['quantity_discounts'])) {?><span class="discount"><?php echo smartyTranslate(array('s'=>'Reduced price!'),$_smarty_tpl);?>
</span><?php }?>
							<?php if (isset($_smarty_tpl->tpl_vars['product']->value['online_only'])&&$_smarty_tpl->tpl_vars['product']->value['online_only']) {?><span class="online_only"><?php echo smartyTranslate(array('s'=>'Online only!'),$_smarty_tpl);?>
</span><?php }?>
							<?php if ((!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value&&((isset($_smarty_tpl->tpl_vars['product']->value['show_price'])&&$_smarty_tpl->tpl_vars['product']->value['show_price'])||(isset($_smarty_tpl->tpl_vars['product']->value['available_for_order'])&&$_smarty_tpl->tpl_vars['product']->value['available_for_order'])))) {?>
						
						
							
						
						
						<?php if ($_smarty_tpl->tpl_vars['product']->value['migliorprezzo_link']==1) {?>
						<!-- <a href="https://www.ezdirect.it/modules/formprevendita/form.php?category=<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
" rel="nofollow"><?php echo smartyTranslate(array('s'=>'Ottieni il miglior prezzo'),$_smarty_tpl);?>
</a> -->
						<?php }?>
						<br />
						</div>			
					<div class="right_block">
					<?php if ($_smarty_tpl->tpl_vars['product']->value['price_tax_exc']==0||($_smarty_tpl->tpl_vars['product']->value['fuori_produzione']==1&&$_smarty_tpl->tpl_vars['product']->value['quantity']==0)) {?>
					
						<?php if ($_smarty_tpl->tpl_vars['product']->value['fuori_produzione']==1||preg_match("/fuori produzione/i",$_smarty_tpl->tpl_vars['product']->value['description_short'])) {?>  
							<p class="fp" style="padding-left: 30px; ">
							<strong><?php echo smartyTranslate(array('s'=>'This product is out of production'),$_smarty_tpl);?>
.</strong><br /><br />
							<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['product']->value['id_category_default'],$_smarty_tpl->tpl_vars['link']->value->getCatRewrite($_smarty_tpl->tpl_vars['product']->value['id_category_default']));?>
"><?php echo smartyTranslate(array('s'=>'Search for similar products'),$_smarty_tpl);?>
</a>
							
							</p>

							
						<?php } else { ?>
							<p class="fp" style="text-align: center;">
							
							
							<a href="https://www.ezdirect.it/modules/formprevendita/form.php?category=<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
" rel="nofollow" style="background-color: #f0a841; color: white; font-weight: 400; padding: 5px 20px; "><?php echo smartyTranslate(array('s'=>'Ask for a quotation'),$_smarty_tpl);?>
</a>
							</p>
						
						
						<?php }?>
					

					<?php } else { ?>
					
				
						
						
					
					
					
						
						<?php if ($_smarty_tpl->tpl_vars['product']->value['fuori_produzione']==0&&($_smarty_tpl->tpl_vars['product']->value['id_product_attribute']==0||(isset($_smarty_tpl->tpl_vars['add_prod_display']->value)&&($_smarty_tpl->tpl_vars['add_prod_display']->value==1)))&&$_smarty_tpl->tpl_vars['product']->value['available_for_order']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&$_smarty_tpl->tpl_vars['product']->value['minimal_quantity']<=1&&$_smarty_tpl->tpl_vars['product']->value['customizable']!=2&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value||($_smarty_tpl->tpl_vars['product']->value['fuori_produzione']==1&&$_smarty_tpl->tpl_vars['product']->value['price']>0&&$_smarty_tpl->tpl_vars['product']->value['quantity']>0)) {?>
							
								<?php if ($_smarty_tpl->tpl_vars['product']->value['location']=='bundle') {?>
								<div class="quantity-div">			
									<div style="text-align:left;  font-size:13px;"><?php echo smartyTranslate(array('s'=>'Quantity :'),$_smarty_tpl);?>
</div>
									<div class="quantity_input_div">
										<input type="text" name="qty_bdl_<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['product']->value['id_product'],'b','');?>
" id="qty_bdl_<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['product']->value['id_product'],'b','');?>
" class="text quantity_input" value="<?php if (isset($_smarty_tpl->tpl_vars['quantityBackup']->value)) {?><?php echo intval($_smarty_tpl->tpl_vars['quantityBackup']->value);?>
<?php } else { ?>1<?php }?>" size="2" maxlength="3" />
										<div class="quantity_input_buttons">
											<span class="span_up" onclick="quantity_cart_add('qty_bdl_<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['product']->value['id_product'],'b','');?>
');">+</span>
											<span class="span_down" onclick="quantity_cart_rmv('qty_bdl_<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['product']->value['id_product'],'b','');?>
');">-</span>
										</div>
									</div>
									<div style="clear:both"></div>
								</div>	
									<input type="hidden" value="https://www.ezdirect.it/img/b/<?php echo $_smarty_tpl->tpl_vars['product']->value['id_image'];?>
-small.jpg" id="bundle_img_link_bundle-<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['product']->value['id_product'],'b','');?>
" />
								
									<div style="display:block; height:5px"></div>
								<div class="list_right_block">		
									<div class="acquista-listing-button">
										
										<form id="buy_block" action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('cart.php');?>
" method="post">
										<input type="hidden" name="bundle" value="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['product']->value['id_product'],'b','');?>
" />
										
										<input type="submit" value="<?php echo smartyTranslate(array('s'=>'Add to cart'),$_smarty_tpl);?>
" title="<?php echo smartyTranslate(array('s'=>'Aggiungi al carrello'),$_smarty_tpl);?>
" id="bundle_<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['product']->value['id_product'],'b','');?>
" class="ajax_add_to_cart_button" style="width:140px; font-family: Arial; background-color:#e62026; color:#ffffff; border:1px solid #bdc2c9" />
									</form>
									
									</div><div style="clear:both"></div>
								</div>
								<?php } else { ?>
								
								
								
								<div class="quantity-div" style="width: 100%; display: flex; justify-content: space-between; padding-left:6px; align-items:center;">
									<div style="float:left; padding-top:4px;  font-size:13px;"><?php echo smartyTranslate(array('s'=>'Quantità:'),$_smarty_tpl);?>

									</div>
								
									
									<div class="quantity_input_div" style="display:flex; border: 1px solid #f4f4f4; padding: 6px 0px 6px 12px;">
										<div class="quantity_input_buttons" style="border:none;">
											
											<span class="span_down"  onclick="quantity_cart_rmv('quantity_wanted_<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
');">-</span>
										</div>
										<input type="text" style="border:none; padding:0px 10px;" name="ajax_qty_to_add_to_cart[<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
]" id="quantity_wanted_<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
" class="text quantity_input" value="<?php if (isset($_smarty_tpl->tpl_vars['quantityBackup']->value)) {?><?php echo intval($_smarty_tpl->tpl_vars['quantityBackup']->value);?>
<?php } else { ?>1<?php }?>" size="2" maxlength="3" />
										<div class="quantity_input_buttons"  style="border:none;">
											<span class="span_up" onclick="quantity_cart_add('quantity_wanted_<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
');">+</span>
											
										</div>
									</div>
									<div style="clear:both"></div>
								</div>
								<div style="display:block; height:5px"></div>
								<div class="acquista-listing-button">
									
									<a rel="nofollow" class="ajax_add_to_cart_button" style="background-color:#e62026; color:#ffffff; border:1px solid #bdc2c9; font-size: 13px; font-weight: 400;" name="<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
" id="ajax_id_product_<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('cart.php');?>
?id_product=<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
<?php if (isset($_smarty_tpl->tpl_vars['static_token']->value)) {?>&amp;token=<?php echo $_smarty_tpl->tpl_vars['static_token']->value;?>
<?php }?>&amp;add" title="<?php echo smartyTranslate(array('s'=>'Aggiungi'),$_smarty_tpl);?>
 <?php echo $_smarty_tpl->tpl_vars['product']->value['name'];?>
 <?php echo smartyTranslate(array('s'=>'al carrello'),$_smarty_tpl);?>
" onclick="document.getElementById('ajax_id_product_<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
').href; " ><?php echo smartyTranslate(array('s'=>'Aggiungi al carrello'),$_smarty_tpl);?>
</a>
								</div>
								<div style="clear:both"></div>
								<?php }?>
								
						<?php }?>
						
					
					<?php }?>		
			
			
				</div>	
			</div>
				
			</li>
		<?php }?> 
		<?php if (isset($_smarty_tpl->tpl_vars['product']->value['bundle_layered'])&&$_smarty_tpl->tpl_vars['product']->value['bundle_layered']!='') {?>
			<?php echo $_smarty_tpl->tpl_vars['product']->value['bundle_layered'];?>

		
		<?php } else { ?>
		<?php }?>
	<?php } ?>
	</ul>
	<!-- /Products list -->
<?php }?>
<?php }} ?>
