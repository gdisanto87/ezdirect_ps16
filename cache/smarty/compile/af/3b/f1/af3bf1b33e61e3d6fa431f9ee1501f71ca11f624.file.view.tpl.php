<?php /* Smarty version Smarty-3.1.19, created on 2021-12-05 09:59:48
         compiled from "/var/www/html/ezadmin/themes/default/template/controllers/forecast_stock/helpers/view/view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:22393665461ac7f843a8809-10146813%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'af3bf1b33e61e3d6fa431f9ee1501f71ca11f624' => 
    array (
      0 => '/var/www/html/ezadmin/themes/default/template/controllers/forecast_stock/helpers/view/view.tpl',
      1 => 1615901619,
      2 => 'file',
    ),
    '65752a427c94173eeeb5b077cba376b60bae6f03' => 
    array (
      0 => '/var/www/html/ezadmin/themes/default/template/helpers/view/view.tpl',
      1 => 1604653169,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '22393665461ac7f843a8809-10146813',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'name_controller' => 0,
    'hookName' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_61ac7f843c9a77_46459959',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_61ac7f843c9a77_46459959')) {function content_61ac7f843c9a77_46459959($_smarty_tpl) {?>

<div class="leadin"></div>



<?php if ($_smarty_tpl->tpl_vars['forecast']->value!=1) {?>

<div id="container-forecast-stock">
	<form class="form-horizontal col-lg-12" method="post">
		<div class="panel">
			<div class="panel-heading">
				<i class="icon-truck"></i> Forecast Stock
			</div>
			<div class="form-group">
				<label>Cerca in base al marchio:</label>
				<p class="form-control-static">
					<select id="per_marchio" name="per_marchio[]" class="form-control textbox-fix select2mul" multiple="multiple">
						<option value="">-- Seleziona --</option>
						<?php if (count($_smarty_tpl->tpl_vars['marchi']->value)) {?>
							<?php  $_smarty_tpl->tpl_vars['row'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['row']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['marchi']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['row']->key => $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['row']->key;
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['name'];?>
</option>
							<?php } ?>
						<?php }?>
					</select>
				</p>
			</div>

			<div class="form-group">
				<label>Seleziona i fornitori</label>
				<p class="form-control-static">
					<select id="per_fornitore" name="per_fornitore[]" class="form-control textbox-fix select2mul" multiple="multiple">
						<option value="">-- Seleziona --</option>
						<?php if (count($_smarty_tpl->tpl_vars['fornitori']->value)) {?>
							<?php  $_smarty_tpl->tpl_vars['row'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['row']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['fornitori']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['row']->key => $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['row']->key;
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value['name'];?>
</option>
							<?php } ?>
						<?php }?>
					</select>
				</p>
			</div>

			<input type="hidden" name="azione" value="vai-forecast" />
			<button type="submit" class="btn btn-default">Cerca</button>
		</div>
	</form>
</div>

<?php } else { ?>

<form class="form-horizontal col-lg-12" method="post" enctype="multipart/form-data" name="product" novalidate>
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-user"></i>
            Ricerca forecast stock
            
            <div class="bottoni-tab">
                <a class="btn btn-default" href="">
                    Esporta i dati in formato Excel
				</a>
				
				<a class="btn btn-default" href="">
                    Torna a ricerca
                </a>
            </div>
		</div>

		<div class="row">
            <div class="form-group col-md-12">
				<label for="periodo_tutti">Periodo forecast:</label>
				<input type="text" name="periodo_tutti" id="periodo_tutti" onkeyup="calcolaPeriodoTutti()" value="<?php echo round($_smarty_tpl->tpl_vars['forecast_registrato_2']->value,2);?>
" style="max-width: 200px;">
                <table class="table tabella_datatable" id="tabella_forecast_stock">
                    <thead>
                        <tr>
                            <th><span class="title_box ">Codice</span></th>
                            <th><span class="title_box ">Descrizione</span></th>
                            <th><span class="title_box ">Costruttore</span></th>
                            <th><span class="title_box ">Fornitore</span></th>
                            <th><span class="title_box ">Mese</span></th>
                            <th class="corner-cut corner-red" title="2 mesi"><span class="title_box ">2 m.</span></th>
							<th class="corner-cut corner-red" title="3 mesi"><span class="title_box ">3 m.</span></th>
							<th class="corner-cut corner-red" title="6 mesi"><span class="title_box ">6 m.</span></th>
							<th class="corner-cut corner-red" title="12 mesi"><span class="title_box ">12 m.</span></th>
							<th><span class="title_box ">Ordine 1 mese</span></th>
							<th><span class="title_box ">Forecast periodo</span></th>
							<th class="corner-cut corner-red" title="Disponibilita netta = Magazzino EZ - Impegnato"><span class="title_box ">Netto disp.</span></th>
							<th class="corner-cut corner-red" title="Ordinato a fornitore"><span class="title_box ">Ord.</span></th>
							<th class="corner-cut corner-red" title="Ordinato al fornitore + magazzino EZ netto - Impegnato"><span class="title_box ">Ord. + netto</span></th>
							<th><span class="title_box ">Ord. per periodo</span></th>
							<th style="display:none"><span class="title_box ">Previsione</span></th>
							<th style="display:none"><span class="title_box ">Calcola Periodo</span></th>
							<th style="display:none"><span class="title_box ">ID Prodotto</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php  $_smarty_tpl->tpl_vars['row'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['row']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['prodotti']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['row']->key => $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['row']->key;
?>
                        <tr>
							<td><a class="tooltip_prodotto" title="<?php echo $_smarty_tpl->tpl_vars['row']->value['tooltip'];?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminProducts'), ENT_QUOTES, 'UTF-8', true);?>
&amp;id_product=<?php echo intval($_smarty_tpl->tpl_vars['row']->value['id_product']);?>
&amp;updateproduct" target="_blank"><?php echo $_smarty_tpl->tpl_vars['row']->value['reference'];?>
</a></td>
                            <td><?php echo $_smarty_tpl->tpl_vars['row']->value["nome_prodotto"];?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['row']->value["costruttore"];?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['row']->value["fornitore"];?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['row']->value["venduto30"];?>
</td>
                            <td><?php echo $_smarty_tpl->tpl_vars['row']->value["venduto60"];?>
</td>
							<td><?php echo $_smarty_tpl->tpl_vars['row']->value["venduto90"];?>
</td>
							<td><?php echo $_smarty_tpl->tpl_vars['row']->value["venduto180"];?>
</td>
							<td><?php echo $_smarty_tpl->tpl_vars['row']->value["venduto365"];?>
</td>
							<td><?php echo $_smarty_tpl->tpl_vars['row']->value["ord_mese"];?>
</td>
							<td id="calcolo_<?php echo $_smarty_tpl->tpl_vars['row']->value['id_product'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value["forecast"];?>
</td>
							<td><?php echo $_smarty_tpl->tpl_vars['row']->value["netto"];?>
 <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['netto'];?>
" id="netto_<?php echo $_smarty_tpl->tpl_vars['row']->value['id_product'];?>
" /> </td>
							<td><?php echo $_smarty_tpl->tpl_vars['row']->value["ord"];?>
 <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['ord'];?>
" id="ordinato_<?php echo $_smarty_tpl->tpl_vars['row']->value['id_product'];?>
" /> </td>
							<td><?php echo $_smarty_tpl->tpl_vars['row']->value["ord_netto"];?>
</td>
							<td id="ord_tot_<?php echo $_smarty_tpl->tpl_vars['row']->value['id_product'];?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value["ord_periodo"];?>
</td>
							<td style="display:none"><input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['previsione_raw'];?>
" id="ordine_per_periodo_<?php echo $_smarty_tpl->tpl_vars['row']->value['id_product'];?>
" /><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['previsione'];?>
</strong></td>
							<td style="display:none"><input type="text" size="3" id="periodo_<?php echo $_smarty_tpl->tpl_vars['row']->value['id_product'];?>
" onkeyup="calcolaPeriodo(<?php echo $_smarty_tpl->tpl_vars['row']->value['id_product'];?>
)" name="periodo[<?php echo $_smarty_tpl->tpl_vars['row']->value['id_product'];?>
]" style="text-align:right" value="<?php echo $_smarty_tpl->tpl_vars['forecast_registrato']->value;?>
" /></td>
							<td style="display:none"><input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['row']->value['id_product'];?>
" class="id_productClass" id="id_<?php echo $_smarty_tpl->tpl_vars['row']->value['id_product'];?>
" name="id_<?php echo $_smarty_tpl->tpl_vars['row']->value['id_product'];?>
" /><strong><?php echo $_smarty_tpl->tpl_vars['row']->value['id_product'];?>
</strong></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>

				<hr />

				<a class="btn btn-default" href="">
                    Esporta i dati in formato Excel
				</a>
				
				<a class="btn btn-default" href="">
                    Torna a ricerca
                </a>
            </div>
        </div>
		
	</div>
</form>

<?php }?>



<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayAdminView'),$_smarty_tpl);?>

<?php if (isset($_smarty_tpl->tpl_vars['name_controller']->value)) {?>
	<?php $_smarty_tpl->_capture_stack[0][] = array('hookName', 'hookName', null); ob_start(); ?>display<?php echo ucfirst($_smarty_tpl->tpl_vars['name_controller']->value);?>
View<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
	<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>$_smarty_tpl->tpl_vars['hookName']->value),$_smarty_tpl);?>

<?php } elseif (isset($_GET['controller'])) {?>
	<?php $_smarty_tpl->_capture_stack[0][] = array('hookName', 'hookName', null); ob_start(); ?>display<?php echo htmlentities(ucfirst($_GET['controller']));?>
View<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
	<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>$_smarty_tpl->tpl_vars['hookName']->value),$_smarty_tpl);?>

<?php }?>
<?php }} ?>
