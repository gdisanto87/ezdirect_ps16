<?php return array (
  'AbstractExtension' => 
  array (
    'path' => 'classes/html2pdf1.6/Extension/AbstractExtension.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'AbstractHtmlTag' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/AbstractHtmlTag.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'AbstractLogger' => 
  array (
    'path' => '',
    'type' => 'abstract class',
    'override' => false,
  ),
  'AbstractLoggerCore' => 
  array (
    'path' => 'classes/log/AbstractLogger.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'AbstractSvgTag' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/AbstractSvgTag.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'AbstractTag' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/AbstractTag.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'AbstractTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/AbstractTest.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'Adapter_AddressFactory' => 
  array (
    'path' => 'Adapter/Adapter_AddressFactory.php',
    'type' => 'class',
    'override' => false,
  ),
  'Adapter_CacheManager' => 
  array (
    'path' => 'Adapter/Adapter_CacheManager.php',
    'type' => 'class',
    'override' => false,
  ),
  'Adapter_Configuration' => 
  array (
    'path' => 'Adapter/Adapter_Configuration.php',
    'type' => 'class',
    'override' => false,
  ),
  'Adapter_Database' => 
  array (
    'path' => 'Adapter/Adapter_Database.php',
    'type' => 'class',
    'override' => false,
  ),
  'Adapter_EntityMapper' => 
  array (
    'path' => 'Adapter/Adapter_EntityMapper.php',
    'type' => 'class',
    'override' => false,
  ),
  'Adapter_EntityMetaDataRetriever' => 
  array (
    'path' => 'Adapter/Adapter_EntityMetaDataRetriever.php',
    'type' => 'class',
    'override' => false,
  ),
  'Adapter_Exception' => 
  array (
    'path' => 'Adapter/Adapter_Exception.php',
    'type' => 'class',
    'override' => false,
  ),
  'Adapter_HookManager' => 
  array (
    'path' => 'Adapter/Adapter_HookManager.php',
    'type' => 'class',
    'override' => false,
  ),
  'Adapter_PackItemsManager' => 
  array (
    'path' => 'Adapter/Adapter_PackItemsManager.php',
    'type' => 'class',
    'override' => false,
  ),
  'Adapter_ProductPriceCalculator' => 
  array (
    'path' => 'Adapter/Adapter_ProductPriceCalculator.php',
    'type' => 'class',
    'override' => false,
  ),
  'Adapter_ServiceLocator' => 
  array (
    'path' => 'Adapter/Adapter_ServiceLocator.php',
    'type' => 'class',
    'override' => false,
  ),
  'Adapter_StockManager' => 
  array (
    'path' => 'Adapter/Adapter_StockManager.php',
    'type' => 'class',
    'override' => false,
  ),
  'Address' => 
  array (
    'path' => 'override/classes/Address.php',
    'type' => 'class',
    'override' => false,
  ),
  'AddressController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AddressControllerCore' => 
  array (
    'path' => 'controllers/front/AddressController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AddressCore' => 
  array (
    'path' => 'classes/Address.php',
    'type' => 'class',
    'override' => false,
  ),
  'AddressFormat' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AddressFormatCore' => 
  array (
    'path' => 'classes/AddressFormat.php',
    'type' => 'class',
    'override' => false,
  ),
  'AddressesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AddressesControllerCore' => 
  array (
    'path' => 'controllers/front/AddressesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAccessController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAccessControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminAccessController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAddonsCatalogController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAddonsCatalogControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminAddonsCatalogController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAddressesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAddressesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminAddressesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAdminPreferencesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAdminPreferencesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminAdminPreferencesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAgentiController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAgentiControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminAgentiController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAggiornaFornitoriController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAggiornaFornitoriControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminAggiornaFornitoriController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAllocatoController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAllocatoControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminAllocatoController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminApprovvigionamentoController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminApprovvigionamentoControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminApprovvigionamentoController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAttachmentsController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAttachmentsControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminAttachmentsController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAttributeGeneratorController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAttributeGeneratorControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminAttributeGeneratorController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAttributesGroupsController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminAttributesGroupsControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminAttributesGroupsController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminBDLController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminBDLControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminBDLController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminBackupController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminBackupControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminBackupController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCapController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCapControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminCapController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCarrelliTipoController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCarrelliTipoControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminCarrelliTipoController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCarrierWizardController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCarrierWizardControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminCarrierWizardController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCarriersController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCarriersControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminCarriersController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCartRulesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCartRulesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminCartRulesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCartsController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCartsControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminCartsController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCategoriesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCategoriesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminCategoriesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCercaSerialiController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCercaSerialiControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminCercaSerialiController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminClientiAffidatiController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminClientiAffidatiControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminClientiAffidatiController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminClientiCloudController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminClientiCloudControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminClientiCloudController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCmsCategoriesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCmsCategoriesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminCmsCategoriesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCmsContentController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCmsContentControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminCmsContentController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCmsController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCmsControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminCmsController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminContactsController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminContactsControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminContactsController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminContrattiController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminContrattiControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminContrattiController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminController' => 
  array (
    'path' => 'override/classes/controller/AdminController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminControllerCore' => 
  array (
    'path' => 'classes/controller/AdminController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminControlloImgController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminControlloImgControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminControlloImgController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminControlloPagamentiController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminControlloPagamentiControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminControlloPagamentiController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCorrezioniController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCorrezioniControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminCorrezioniController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCountriesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCountriesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminCountriesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCouponCheapnetController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCouponCheapnetControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminCouponCheapnetController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCurrenciesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCurrenciesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminCurrenciesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCustomerPreferencesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCustomerPreferencesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminCustomerPreferencesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCustomerThreadsController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCustomerThreadsControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminCustomerThreadsController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCustomersController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminCustomersControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminCustomersController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminDashboardController' => 
  array (
    'path' => 'override/controllers/admin/AdminDashboardController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminDashboardControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminDashboardController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminDeliverySlipController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminDeliverySlipControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminDeliverySlipController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminDuplicatiController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminDuplicatiControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminDuplicatiController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminEmailsController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminEmailsControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminEmailsController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminEmployeesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminEmployeesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminEmployeesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminExportController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminExportControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminExportController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminFattureController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminFattureControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminFattureController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminFeaturesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminFeaturesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminFeaturesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminForecastStockController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminForecastStockControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminForecastStockController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminGendersController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminGendersControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminGendersController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminGeolocationController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminGeolocationControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminGeolocationController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminGroupsController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminGroupsControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminGroupsController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminGuideController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminGuideControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminGuideController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminImagesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminImagesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminImagesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminImportController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminImportControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminImportController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminInformationController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminInformationControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminInformationController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminInvendutiController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminInvendutiControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminInvendutiController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminInvoicesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminInvoicesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminInvoicesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminKitController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminKitControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminKitController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminLanguagesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminLanguagesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminLanguagesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminLicenzeEzdialController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminLicenzeEzdialControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminLicenzeEzdialController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminLocalizationController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminLocalizationControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminLocalizationController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminLoginController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminLoginControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminLoginController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminLogisticaAmazonController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminLogisticaAmazonControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminLogisticaAmazonController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminLogsController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminLogsControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminLogsController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminMaintenanceController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminMaintenanceControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminMaintenanceController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminManufacturersController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminManufacturersControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminManufacturersController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminMappaturaImmagineController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminMappaturaImmagineControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminMappaturaImmagineController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminMarketingClientiController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminMarketingClientiControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminMarketingClientiController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminMarketingClientiPreventiviController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminMarketingClientiPreventiviControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminMarketingClientiPreventiviController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminMarketingController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminMarketingControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminMarketingController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminMarketingTelefonateController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminMarketingTelefonateControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminMarketingTelefonateController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminMessaggiClienteController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminMessaggiClienteControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminMessaggiClienteController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminMetaController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminMetaControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminMetaController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminModulesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminModulesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminModulesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminModulesPositionsController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminModulesPositionsControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminModulesPositionsController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminMovimentiMagazzinoController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminMovimentiMagazzinoControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminMovimentiMagazzinoController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminNotFoundController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminNotFoundControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminNotFoundController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminNoteDiCreditoController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminNoteDiCreditoControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminNoteDiCreditoController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminOrderMessageController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminOrderMessageControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminOrderMessageController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminOrderPreferencesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminOrderPreferencesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminOrderPreferencesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminOrdersController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminOrdersControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminOrdersController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminOrdiniNonConclusiController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminOrdiniNonConclusiControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminOrdiniNonConclusiController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminOrdiniNonProcessatiController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminOrdiniNonProcessatiControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminOrdiniNonProcessatiController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminOrdiniPDFController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminOrdiniPDFControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminOrdiniPDFController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminOreFerieImpiegatiController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminOreFerieImpiegatiControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminOreFerieImpiegatiController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminOrigineOrdiniController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminOrigineOrdiniControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminOrigineOrdiniController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminOutstandingController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminOutstandingControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminOutstandingController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPPreferencesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPPreferencesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminPPreferencesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPagamentiAmazonController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPagamentiAmazonControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminPagamentiAmazonController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPagamentiController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPagamentiControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminPagamentiController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPatternsController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPatternsControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminPatternsController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPaymentController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPaymentControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminPaymentController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPdfController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPdfControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminPdfController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPerformanceController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPerformanceControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminPerformanceController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPinCentralinoController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPinCentralinoControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminPinCentralinoController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPlannerController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPlannerControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminPlannerController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPrecompilatiController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPrecompilatiControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminPrecompilatiController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPreferencesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPreferencesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminPreferencesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPrezziBloccatiController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminPrezziBloccatiControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminPrezziBloccatiController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminProdottiAmazonController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminProdottiAmazonControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminProdottiAmazonController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminProdottiOldController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminProdottiOldControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminProdottiOldController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminProductsController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminProductsControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminProductsController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminProfilesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminProfilesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminProfilesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminQuickAccessesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminQuickAccessesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminQuickAccessesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminRMAController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminRMAControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminRMAController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminRangePriceController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminRangePriceControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminRangePriceController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminRangeWeightController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminRangeWeightControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminRangeWeightController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminRecuperaCsvController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminRecuperaCsvControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminRecuperaCsvController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminReferrersController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminReferrersControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminReferrersController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminRegistraAnagraficaController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminRegistraAnagraficaControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminRegistraAnagraficaController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminRequestSqlController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminRequestSqlControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminRequestSqlController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminReturnController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminReturnControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminReturnController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminRicercaPrezziController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminRicercaPrezziControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminRicercaPrezziController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminRiepiloghiPDFController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminRiepiloghiPDFControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminRiepiloghiPDFController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminRintracciareController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminRintracciareControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminRintracciareController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminScenesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminScenesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminScenesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminSearchConfController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminSearchConfControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminSearchConfController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminSearchController' => 
  array (
    'path' => 'override/controllers/admin/AdminSearchController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminSearchControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminSearchController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminSearchEnginesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminSearchEnginesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminSearchEnginesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminShippingController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminShippingControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminShippingController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminShopController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminShopControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminShopController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminShopGroupController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminShopGroupControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminShopGroupController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminShopUrlController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminShopUrlControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminShopUrlController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminSlipController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminSlipControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminSlipController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminSpecificPriceRuleController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminSpecificPriceRuleControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminSpecificPriceRuleController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStatesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStatesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminStatesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStatsAcquistiController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStatsAcquistiControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminStatsAcquistiController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStatsController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStatsControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminStatsController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStatsProdottiController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStatsProdottiControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminStatsProdottiController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStatsTabController' => 
  array (
    'path' => '',
    'type' => 'abstract class',
    'override' => false,
  ),
  'AdminStatsTabControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminStatsTabController.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'AdminStatsTargetController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStatsTargetControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminStatsTargetController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStatusesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStatusesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminStatusesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStockConfigurationController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStockConfigurationControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminStockConfigurationController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStockCoverController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStockCoverControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminStockCoverController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStockInstantStateController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStockInstantStateControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminStockInstantStateController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStockManagementController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStockManagementControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminStockManagementController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStockMvtController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStockMvtControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminStockMvtController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStoresController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStoresControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminStoresController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStrumentiAmazonController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStrumentiAmazonControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminStrumentiAmazonController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStrumentiCataloghiFornitoriController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStrumentiCataloghiFornitoriControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminStrumentiCataloghiFornitoriController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStrumentiController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminStrumentiControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminStrumentiController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminSuppliersController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminSuppliersControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminSuppliersController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminSupplyOrdersController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminSupplyOrdersControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminSupplyOrdersController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminTESTController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminTESTControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminTESTController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminTab' => 
  array (
    'path' => '',
    'type' => 'abstract class',
    'override' => false,
  ),
  'AdminTabCore' => 
  array (
    'path' => 'classes/AdminTab.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'AdminTabsController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminTabsControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminTabsController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminTagsController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminTagsControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminTagsController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminTaxRulesGroupController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminTaxRulesGroupControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminTaxRulesGroupController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminTaxesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminTaxesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminTaxesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminThemesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminThemesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminThemesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminTrackingController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminTrackingControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminTrackingController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminTranslationsController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminTranslationsControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminTranslationsController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminTuttiPrezziAcqSpecialiController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminTuttiPrezziAcqSpecialiControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminTuttiPrezziAcqSpecialiController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminTuttiPrezziSpecialiController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminTuttiPrezziSpecialiControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminTuttiPrezziSpecialiController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminValorizzazioneMagazzinoController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminValorizzazioneMagazzinoControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminValorizzazioneMagazzinoController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminWarehousesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminWarehousesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminWarehousesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminWebserviceController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminWebserviceControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminWebserviceController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AdminZonesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AdminZonesControllerCore' => 
  array (
    'path' => 'controllers/admin/AdminZonesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'Alias' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AliasCore' => 
  array (
    'path' => 'classes/Alias.php',
    'type' => 'class',
    'override' => false,
  ),
  'Attachment' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AttachmentController' => 
  array (
    'path' => 'override/controllers/front/AttachmentController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AttachmentControllerCore' => 
  array (
    'path' => 'controllers/front/AttachmentController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AttachmentCore' => 
  array (
    'path' => 'classes/Attachment.php',
    'type' => 'class',
    'override' => false,
  ),
  'Attribute' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AttributeCore' => 
  array (
    'path' => 'classes/Attribute.php',
    'type' => 'class',
    'override' => false,
  ),
  'AttributeGroup' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'AttributeGroupCore' => 
  array (
    'path' => 'classes/AttributeGroup.php',
    'type' => 'class',
    'override' => false,
  ),
  'AuthController' => 
  array (
    'path' => 'override/controllers/front/AuthController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AuthControllerCore' => 
  array (
    'path' => 'controllers/front/AuthController.php',
    'type' => 'class',
    'override' => false,
  ),
  'AverageTaxOfProductsTaxCalculator' => 
  array (
    'path' => 'classes/tax/AverageTaxOfProductsTaxCalculator.php',
    'type' => 'class',
    'override' => false,
  ),
  'B' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Html/B.php',
    'type' => 'class',
    'override' => false,
  ),
  'BDL' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'BDLCore' => 
  array (
    'path' => 'classes/Bdl.php',
    'type' => 'class',
    'override' => false,
  ),
  'BackgroundErrorTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Image/BackgroundErrorTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'BackgroundOkTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Image/BackgroundOkTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'BestSalesController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'BestSalesControllerCore' => 
  array (
    'path' => 'controllers/front/BestSalesController.php',
    'type' => 'class',
    'override' => false,
  ),
  'Big' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Html/Big.php',
    'type' => 'class',
    'override' => false,
  ),
  'Blowfish' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'BlowfishCore' => 
  array (
    'path' => 'classes/Blowfish.php',
    'type' => 'class',
    'override' => false,
  ),
  'Bookmark' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Html/Bookmark.php',
    'type' => 'class',
    'override' => false,
  ),
  'CMS' => 
  array (
    'path' => 'override/classes/CMS.php',
    'type' => 'class',
    'override' => false,
  ),
  'CMSCategory' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CMSCategoryCore' => 
  array (
    'path' => 'classes/CMSCategory.php',
    'type' => 'class',
    'override' => false,
  ),
  'CMSCore' => 
  array (
    'path' => 'classes/CMS.php',
    'type' => 'class',
    'override' => false,
  ),
  'CMSRole' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CMSRoleCore' => 
  array (
    'path' => 'classes/CMSRole.php',
    'type' => 'class',
    'override' => false,
  ),
  'CSV' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CSVCore' => 
  array (
    'path' => 'classes/CSV.php',
    'type' => 'class',
    'override' => false,
  ),
  'Cache' => 
  array (
    'path' => '',
    'type' => 'abstract class',
    'override' => false,
  ),
  'CacheApc' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CacheApcCore' => 
  array (
    'path' => 'classes/cache/CacheApc.php',
    'type' => 'class',
    'override' => false,
  ),
  'CacheCore' => 
  array (
    'path' => 'classes/cache/Cache.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'CacheFs' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CacheFsCore' => 
  array (
    'path' => 'classes/cache/CacheFs.php',
    'type' => 'class',
    'override' => false,
  ),
  'CacheMemcache' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CacheMemcacheCore' => 
  array (
    'path' => 'classes/cache/CacheMemcache.php',
    'type' => 'class',
    'override' => false,
  ),
  'CacheMemcached' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CacheMemcachedCore' => 
  array (
    'path' => 'classes/cache/CacheMemcached.php',
    'type' => 'class',
    'override' => false,
  ),
  'CacheXcache' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CacheXcacheCore' => 
  array (
    'path' => 'classes/cache/CacheXcache.php',
    'type' => 'class',
    'override' => false,
  ),
  'Carrier' => 
  array (
    'path' => 'override/classes/Carrier.php',
    'type' => 'class',
    'override' => false,
  ),
  'CarrierCore' => 
  array (
    'path' => 'classes/Carrier.php',
    'type' => 'class',
    'override' => false,
  ),
  'CarrierModule' => 
  array (
    'path' => '',
    'type' => 'abstract class',
    'override' => false,
  ),
  'CarrierModuleCore' => 
  array (
    'path' => 'classes/module/CarrierModule.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'Cart' => 
  array (
    'path' => 'override/classes/Cart.php',
    'type' => 'class',
    'override' => false,
  ),
  'CartController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CartControllerCore' => 
  array (
    'path' => 'controllers/front/CartController.php',
    'type' => 'class',
    'override' => false,
  ),
  'CartCore' => 
  array (
    'path' => 'classes/Cart.php',
    'type' => 'class',
    'override' => false,
  ),
  'CartRule' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CartRuleCore' => 
  array (
    'path' => 'classes/CartRule.php',
    'type' => 'class',
    'override' => false,
  ),
  'Category' => 
  array (
    'path' => 'override/classes/Category.php',
    'type' => 'class',
    'override' => false,
  ),
  'CategoryController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CategoryControllerCore' => 
  array (
    'path' => 'controllers/front/CategoryController.php',
    'type' => 'class',
    'override' => false,
  ),
  'CategoryCore' => 
  array (
    'path' => 'classes/Category.php',
    'type' => 'class',
    'override' => false,
  ),
  'ChangeCurrencyController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ChangeCurrencyControllerCore' => 
  array (
    'path' => 'controllers/front/ChangeCurrencyController.php',
    'type' => 'class',
    'override' => false,
  ),
  'Chart' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ChartCore' => 
  array (
    'path' => 'classes/Chart.php',
    'type' => 'class',
    'override' => false,
  ),
  'Circle' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Svg/Circle.php',
    'type' => 'class',
    'override' => false,
  ),
  'CircleErrorTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Tag/Svg/CircleErrorTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'Cite' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Html/Cite.php',
    'type' => 'class',
    'override' => false,
  ),
  'CmsController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CmsControllerCore' => 
  array (
    'path' => 'controllers/front/CmsController.php',
    'type' => 'class',
    'override' => false,
  ),
  'Combination' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CombinationCore' => 
  array (
    'path' => 'classes/Combination.php',
    'type' => 'class',
    'override' => false,
  ),
  'CompareController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CompareControllerCore' => 
  array (
    'path' => 'controllers/front/CompareController.php',
    'type' => 'class',
    'override' => false,
  ),
  'CompareProduct' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CompareProductCore' => 
  array (
    'path' => 'classes/CompareProduct.php',
    'type' => 'class',
    'override' => false,
  ),
  'Configuration' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ConfigurationCore' => 
  array (
    'path' => 'classes/Configuration.php',
    'type' => 'class',
    'override' => false,
  ),
  'ConfigurationKPI' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ConfigurationKPICore' => 
  array (
    'path' => 'classes/ConfigurationKPI.php',
    'type' => 'class',
    'override' => false,
  ),
  'ConfigurationTest' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ConfigurationTestCore' => 
  array (
    'path' => 'classes/ConfigurationTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'Connection' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ConnectionCore' => 
  array (
    'path' => 'classes/Connection.php',
    'type' => 'class',
    'override' => false,
  ),
  'ConnectionsSource' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ConnectionsSourceCore' => 
  array (
    'path' => 'classes/ConnectionsSource.php',
    'type' => 'class',
    'override' => false,
  ),
  'Contact' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ContactController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ContactControllerCore' => 
  array (
    'path' => 'controllers/front/ContactController.php',
    'type' => 'class',
    'override' => false,
  ),
  'ContactCore' => 
  array (
    'path' => 'classes/Contact.php',
    'type' => 'class',
    'override' => false,
  ),
  'Context' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ContextCore' => 
  array (
    'path' => 'classes/Context.php',
    'type' => 'class',
    'override' => false,
  ),
  'Controller' => 
  array (
    'path' => '',
    'type' => 'abstract class',
    'override' => false,
  ),
  'ControllerCore' => 
  array (
    'path' => 'classes/controller/Controller.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'ControllerFactory' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ControllerFactoryCore' => 
  array (
    'path' => 'classes/ControllerFactory.php',
    'type' => 'class',
    'override' => false,
  ),
  'Cookie' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CookieCore' => 
  array (
    'path' => 'classes/Cookie.php',
    'type' => 'class',
    'override' => false,
  ),
  'Core_Business_CMS_CMSRepository' => 
  array (
    'path' => 'Core/Business/CMS/Core_Business_CMS_CMSRepository.php',
    'type' => 'class',
    'override' => false,
  ),
  'Core_Business_CMS_CMSRoleRepository' => 
  array (
    'path' => 'Core/Business/CMS/Core_Business_CMS_CMSRoleRepository.php',
    'type' => 'class',
    'override' => false,
  ),
  'Core_Business_ConfigurationInterface' => 
  array (
    'path' => 'Core/Business/Core_Business_ConfigurationInterface.php',
    'type' => 'interface',
    'override' => false,
  ),
  'Core_Business_ContainerBuilder' => 
  array (
    'path' => 'Core/Business/Core_Business_ContainerBuilder.php',
    'type' => 'class',
    'override' => false,
  ),
  'Core_Business_Email_EmailLister' => 
  array (
    'path' => 'Core/Business/Email/Core_Business_Email_EmailLister.php',
    'type' => 'class',
    'override' => false,
  ),
  'Core_Business_Payment_PaymentOption' => 
  array (
    'path' => 'Core/Business/Payment/Core_Business_Payment_PaymentOption.php',
    'type' => 'class',
    'override' => false,
  ),
  'Core_Business_Stock_StockManager' => 
  array (
    'path' => 'Core/Business/Stock/Core_Business_Stock_StockManager.php',
    'type' => 'class',
    'override' => false,
  ),
  'Core_Foundation_Database_DatabaseInterface' => 
  array (
    'path' => 'Core/Foundation/Database/Core_Foundation_Database_DatabaseInterface.php',
    'type' => 'interface',
    'override' => false,
  ),
  'Core_Foundation_Database_EntityInterface' => 
  array (
    'path' => 'Core/Foundation/Database/Core_Foundation_Database_EntityInterface.php',
    'type' => 'interface',
    'override' => false,
  ),
  'Core_Foundation_Database_EntityManager' => 
  array (
    'path' => 'Core/Foundation/Database/Core_Foundation_Database_EntityManager.php',
    'type' => 'class',
    'override' => false,
  ),
  'Core_Foundation_Database_EntityManager_QueryBuilder' => 
  array (
    'path' => 'Core/Foundation/Database/EntityManager/Core_Foundation_Database_EntityManager_QueryBuilder.php',
    'type' => 'class',
    'override' => false,
  ),
  'Core_Foundation_Database_EntityMetaData' => 
  array (
    'path' => 'Core/Foundation/Database/Core_Foundation_Database_EntityMetaData.php',
    'type' => 'class',
    'override' => false,
  ),
  'Core_Foundation_Database_EntityRepository' => 
  array (
    'path' => 'Core/Foundation/Database/Core_Foundation_Database_EntityRepository.php',
    'type' => 'class',
    'override' => false,
  ),
  'Core_Foundation_Database_Exception' => 
  array (
    'path' => 'Core/Foundation/Database/Core_Foundation_Database_Exception.php',
    'type' => 'class',
    'override' => false,
  ),
  'Core_Foundation_Exception_Exception' => 
  array (
    'path' => 'Core/Foundation/Exception/Core_Foundation_Exception_Exception.php',
    'type' => 'class',
    'override' => false,
  ),
  'Core_Foundation_FileSystem_Exception' => 
  array (
    'path' => 'Core/Foundation/Filesystem/Core_Foundation_FileSystem_Exception.php',
    'type' => 'class',
    'override' => false,
  ),
  'Core_Foundation_FileSystem_FileSystem' => 
  array (
    'path' => 'Core/Foundation/Filesystem/Core_Foundation_FileSystem_FileSystem.php',
    'type' => 'class',
    'override' => false,
  ),
  'Core_Foundation_IoC_Container' => 
  array (
    'path' => 'Core/Foundation/IoC/Core_Foundation_IoC_Container.php',
    'type' => 'class',
    'override' => false,
  ),
  'Core_Foundation_IoC_Exception' => 
  array (
    'path' => 'Core/Foundation/IoC/Core_Foundation_IoC_Exception.php',
    'type' => 'class',
    'override' => false,
  ),
  'Country' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CountryCore' => 
  array (
    'path' => 'classes/Country.php',
    'type' => 'class',
    'override' => false,
  ),
  'County' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CountyCore' => 
  array (
    'path' => 'classes/County.php',
    'type' => 'class',
    'override' => false,
  ),
  'Css' => 
  array (
    'path' => 'classes/html2pdf1.6/Parsing/Css.php',
    'type' => 'class',
    'override' => false,
  ),
  'CssConverter' => 
  array (
    'path' => 'classes/html2pdf1.6/CssConverter.php',
    'type' => 'class',
    'override' => false,
  ),
  'CssConverterTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/CssConverterTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'Currency' => 
  array (
    'path' => 'override/classes/Currency.php',
    'type' => 'class',
    'override' => false,
  ),
  'CurrencyCore' => 
  array (
    'path' => 'classes/Currency.php',
    'type' => 'class',
    'override' => false,
  ),
  'Customer' => 
  array (
    'path' => 'override/classes/Customer.php',
    'type' => 'class',
    'override' => false,
  ),
  'CustomerCore' => 
  array (
    'path' => 'classes/Customer.php',
    'type' => 'class',
    'override' => false,
  ),
  'CustomerMessage' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CustomerMessageCore' => 
  array (
    'path' => 'classes/CustomerMessage.php',
    'type' => 'class',
    'override' => false,
  ),
  'CustomerThread' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CustomerThreadCore' => 
  array (
    'path' => 'classes/CustomerThread.php',
    'type' => 'class',
    'override' => false,
  ),
  'Customization' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CustomizationCore' => 
  array (
    'path' => 'classes/Customization.php',
    'type' => 'class',
    'override' => false,
  ),
  'CustomizationField' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'CustomizationFieldCore' => 
  array (
    'path' => 'classes/CustomizationField.php',
    'type' => 'class',
    'override' => false,
  ),
  'Datamatrix' => 
  array (
    'path' => 'classes/TCPDF-master/include/barcodes/datamatrix.php',
    'type' => 'class',
    'override' => false,
  ),
  'DateRange' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'DateRangeCore' => 
  array (
    'path' => 'classes/DateRange.php',
    'type' => 'class',
    'override' => false,
  ),
  'Db' => 
  array (
    'path' => '',
    'type' => 'abstract class',
    'override' => false,
  ),
  'DbCore' => 
  array (
    'path' => 'classes/db/Db.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'DbMySQLi' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'DbMySQLiCore' => 
  array (
    'path' => 'classes/db/DbMySQLi.php',
    'type' => 'class',
    'override' => false,
  ),
  'DbPDO' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'DbPDOCore' => 
  array (
    'path' => 'classes/db/DbPDO.php',
    'type' => 'class',
    'override' => false,
  ),
  'DbQuery' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'DbQueryCore' => 
  array (
    'path' => 'classes/db/DbQuery.php',
    'type' => 'class',
    'override' => false,
  ),
  'Debug' => 
  array (
    'path' => 'classes/html2pdf1.6/Debug/Debug.php',
    'type' => 'class',
    'override' => false,
  ),
  'DebugInterface' => 
  array (
    'path' => 'classes/html2pdf1.6/Debug/DebugInterface.php',
    'type' => 'interface',
    'override' => false,
  ),
  'DebugTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Debug/DebugTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'Del' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Html/Del.php',
    'type' => 'class',
    'override' => false,
  ),
  'Delivery' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'DeliveryCore' => 
  array (
    'path' => 'classes/Delivery.php',
    'type' => 'class',
    'override' => false,
  ),
  'Discount' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'DiscountController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'DiscountControllerCore' => 
  array (
    'path' => 'controllers/front/DiscountController.php',
    'type' => 'class',
    'override' => false,
  ),
  'DiscountCore' => 
  array (
    'path' => 'classes/Discount.php',
    'type' => 'class',
    'override' => false,
  ),
  'Dispatcher' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'DispatcherCore' => 
  array (
    'path' => 'classes/Dispatcher.php',
    'type' => 'class',
    'override' => false,
  ),
  'DivTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Tag/DivTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'Ellipse' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Svg/Ellipse.php',
    'type' => 'class',
    'override' => false,
  ),
  'EllipseErrorTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Tag/Svg/EllipseErrorTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'Em' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Html/Em.php',
    'type' => 'class',
    'override' => false,
  ),
  'Employee' => 
  array (
    'path' => 'override/classes/Employee.php',
    'type' => 'class',
    'override' => false,
  ),
  'EmployeeCore' => 
  array (
    'path' => 'classes/Employee.php',
    'type' => 'class',
    'override' => false,
  ),
  'ExamplesTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/ExamplesTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'ExceptionFormatter' => 
  array (
    'path' => 'classes/html2pdf1.6/Exception/ExceptionFormatter.php',
    'type' => 'class',
    'override' => false,
  ),
  'ExceptionFormatterTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Exception/ExceptionFormatterTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'ExtensionInterface' => 
  array (
    'path' => 'classes/html2pdf1.6/Extension/ExtensionInterface.php',
    'type' => 'interface',
    'override' => false,
  ),
  'Fattura' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'FatturaCore' => 
  array (
    'path' => 'classes/Fattura.php',
    'type' => 'class',
    'override' => false,
  ),
  'Feature' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'FeatureCore' => 
  array (
    'path' => 'classes/Feature.php',
    'type' => 'class',
    'override' => false,
  ),
  'FeatureValue' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'FeatureValueCore' => 
  array (
    'path' => 'classes/FeatureValue.php',
    'type' => 'class',
    'override' => false,
  ),
  'FileLogger' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'FileLoggerCore' => 
  array (
    'path' => 'classes/log/FileLogger.php',
    'type' => 'class',
    'override' => false,
  ),
  'FileNameOkTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Output/FileNameOkTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'FileUploader' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'FileUploaderCore' => 
  array (
    'path' => 'classes/FileUploader.php',
    'type' => 'class',
    'override' => false,
  ),
  'Font' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Html/Font.php',
    'type' => 'class',
    'override' => false,
  ),
  'FrontController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'FrontControllerCore' => 
  array (
    'path' => 'classes/controller/FrontController.php',
    'type' => 'class',
    'override' => false,
  ),
  'G' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Svg/G.php',
    'type' => 'class',
    'override' => false,
  ),
  'GErrorTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Tag/Svg/GErrorTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'Gender' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'GenderCore' => 
  array (
    'path' => 'classes/Gender.php',
    'type' => 'class',
    'override' => false,
  ),
  'GetFileController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'GetFileControllerCore' => 
  array (
    'path' => 'controllers/front/GetFileController.php',
    'type' => 'class',
    'override' => false,
  ),
  'Group' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'GroupCore' => 
  array (
    'path' => 'classes/Group.php',
    'type' => 'class',
    'override' => false,
  ),
  'GroupReduction' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'GroupReductionCore' => 
  array (
    'path' => 'classes/GroupReduction.php',
    'type' => 'class',
    'override' => false,
  ),
  'Guest' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'GuestCore' => 
  array (
    'path' => 'classes/Guest.php',
    'type' => 'class',
    'override' => false,
  ),
  'GuestTrackingController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'GuestTrackingControllerCore' => 
  array (
    'path' => 'controllers/front/GuestTrackingController.php',
    'type' => 'class',
    'override' => false,
  ),
  'HTMLTemplate' => 
  array (
    'path' => '',
    'type' => 'abstract class',
    'override' => false,
  ),
  'HTMLTemplateCore' => 
  array (
    'path' => 'classes/pdf/HTMLTemplate.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'HTMLTemplateDeliverySlip' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'HTMLTemplateDeliverySlipCore' => 
  array (
    'path' => 'classes/pdf/HTMLTemplateDeliverySlip.php',
    'type' => 'class',
    'override' => false,
  ),
  'HTMLTemplateInvoice' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'HTMLTemplateInvoiceCore' => 
  array (
    'path' => 'classes/pdf/HTMLTemplateInvoice.php',
    'type' => 'class',
    'override' => false,
  ),
  'HTMLTemplateOrderReturn' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'HTMLTemplateOrderReturnCore' => 
  array (
    'path' => 'classes/pdf/HTMLTemplateOrderReturn.php',
    'type' => 'class',
    'override' => false,
  ),
  'HTMLTemplateOrderSlip' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'HTMLTemplateOrderSlipCore' => 
  array (
    'path' => 'classes/pdf/HTMLTemplateOrderSlip.php',
    'type' => 'class',
    'override' => false,
  ),
  'HTMLTemplateSupplyOrderForm' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'HTMLTemplateSupplyOrderFormCore' => 
  array (
    'path' => 'classes/pdf/HTMLTemplateSupplyOrderForm.php',
    'type' => 'class',
    'override' => false,
  ),
  'Helper' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'HelperCalendar' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'HelperCalendarCore' => 
  array (
    'path' => 'classes/helper/HelperCalendar.php',
    'type' => 'class',
    'override' => false,
  ),
  'HelperCore' => 
  array (
    'path' => 'classes/helper/Helper.php',
    'type' => 'class',
    'override' => false,
  ),
  'HelperForm' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'HelperFormCore' => 
  array (
    'path' => 'classes/helper/HelperForm.php',
    'type' => 'class',
    'override' => false,
  ),
  'HelperImageUploader' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'HelperImageUploaderCore' => 
  array (
    'path' => 'classes/helper/HelperImageUploader.php',
    'type' => 'class',
    'override' => false,
  ),
  'HelperKpi' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'HelperKpiCore' => 
  array (
    'path' => 'classes/helper/HelperKpi.php',
    'type' => 'class',
    'override' => false,
  ),
  'HelperKpiRow' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'HelperKpiRowCore' => 
  array (
    'path' => 'classes/helper/HelperKpiRow.php',
    'type' => 'class',
    'override' => false,
  ),
  'HelperList' => 
  array (
    'path' => 'override/classes/helper/HelperList.php',
    'type' => 'class',
    'override' => false,
  ),
  'HelperListCore' => 
  array (
    'path' => 'classes/helper/HelperList.php',
    'type' => 'class',
    'override' => false,
  ),
  'HelperOptions' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'HelperOptionsCore' => 
  array (
    'path' => 'classes/helper/HelperOptions.php',
    'type' => 'class',
    'override' => false,
  ),
  'HelperShop' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'HelperShopCore' => 
  array (
    'path' => 'classes/helper/HelperShop.php',
    'type' => 'class',
    'override' => false,
  ),
  'HelperTreeCategories' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'HelperTreeCategoriesCore' => 
  array (
    'path' => 'classes/helper/HelperTreeCategories.php',
    'type' => 'class',
    'override' => false,
  ),
  'HelperTreeShops' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'HelperTreeShopsCore' => 
  array (
    'path' => 'classes/helper/HelperTreeShops.php',
    'type' => 'class',
    'override' => false,
  ),
  'HelperUploader' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'HelperUploaderCore' => 
  array (
    'path' => 'classes/helper/HelperUploader.php',
    'type' => 'class',
    'override' => false,
  ),
  'HelperView' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'HelperViewCore' => 
  array (
    'path' => 'classes/helper/HelperView.php',
    'type' => 'class',
    'override' => false,
  ),
  'HistoryController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'HistoryControllerCore' => 
  array (
    'path' => 'controllers/front/HistoryController.php',
    'type' => 'class',
    'override' => false,
  ),
  'Hook' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'HookCore' => 
  array (
    'path' => 'classes/Hook.php',
    'type' => 'class',
    'override' => false,
  ),
  'Html' => 
  array (
    'path' => 'classes/html2pdf1.6/Parsing/Html.php',
    'type' => 'class',
    'override' => false,
  ),
  'Html2Pdf' => 
  array (
    'path' => 'classes/html2pdf1.6/Html2Pdf.php',
    'type' => 'class',
    'override' => false,
  ),
  'Html2PdfException' => 
  array (
    'path' => 'classes/html2pdf1.6/Exception/Html2PdfException.php',
    'type' => 'class',
    'override' => false,
  ),
  'Html2PdfTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Html2PdfTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'HtmlExtension' => 
  array (
    'path' => 'classes/html2pdf1.6/Extension/Core/HtmlExtension.php',
    'type' => 'class',
    'override' => false,
  ),
  'HtmlLexer' => 
  array (
    'path' => 'classes/html2pdf1.6/Parsing/HtmlLexer.php',
    'type' => 'class',
    'override' => false,
  ),
  'HtmlLexerTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Parsing/HtmlLexerTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'HtmlParsingException' => 
  array (
    'path' => 'classes/html2pdf1.6/Exception/HtmlParsingException.php',
    'type' => 'class',
    'override' => false,
  ),
  'HtmlTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Parsing/HtmlTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'I' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Html/I.php',
    'type' => 'class',
    'override' => false,
  ),
  'ITreeToolbar' => 
  array (
    'path' => '',
    'type' => 'interface',
    'override' => false,
  ),
  'ITreeToolbarButton' => 
  array (
    'path' => '',
    'type' => 'interface',
    'override' => false,
  ),
  'ITreeToolbarButtonCore' => 
  array (
    'path' => 'classes/tree/ITreeToolbarButton.php',
    'type' => 'interface',
    'override' => false,
  ),
  'ITreeToolbarCore' => 
  array (
    'path' => 'classes/tree/ITreeToolbar.php',
    'type' => 'interface',
    'override' => false,
  ),
  'IdentityController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'IdentityControllerCore' => 
  array (
    'path' => 'controllers/front/IdentityController.php',
    'type' => 'class',
    'override' => false,
  ),
  'Image' => 
  array (
    'path' => 'override/classes/Image.php',
    'type' => 'class',
    'override' => false,
  ),
  'ImageCore' => 
  array (
    'path' => 'classes/Image.php',
    'type' => 'class',
    'override' => false,
  ),
  'ImageException' => 
  array (
    'path' => 'classes/html2pdf1.6/Exception/ImageException.php',
    'type' => 'class',
    'override' => false,
  ),
  'ImageManager' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ImageManagerCore' => 
  array (
    'path' => 'classes/ImageManager.php',
    'type' => 'class',
    'override' => false,
  ),
  'ImageType' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ImageTypeCore' => 
  array (
    'path' => 'classes/ImageType.php',
    'type' => 'class',
    'override' => false,
  ),
  'ImportModule' => 
  array (
    'path' => '',
    'type' => 'abstract class',
    'override' => false,
  ),
  'ImportModuleCore' => 
  array (
    'path' => 'classes/module/ImportModule.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'IndexController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'IndexControllerCore' => 
  array (
    'path' => 'controllers/front/IndexController.php',
    'type' => 'class',
    'override' => false,
  ),
  'Ins' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Html/Ins.php',
    'type' => 'class',
    'override' => false,
  ),
  'Label' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Html/Label.php',
    'type' => 'class',
    'override' => false,
  ),
  'Language' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'LanguageCore' => 
  array (
    'path' => 'classes/Language.php',
    'type' => 'class',
    'override' => false,
  ),
  'Line' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Svg/Line.php',
    'type' => 'class',
    'override' => false,
  ),
  'LineErrorTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Tag/Svg/LineErrorTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'Link' => 
  array (
    'path' => 'override/classes/Link.php',
    'type' => 'class',
    'override' => false,
  ),
  'LinkCore' => 
  array (
    'path' => 'classes/Link.php',
    'type' => 'class',
    'override' => false,
  ),
  'Locale' => 
  array (
    'path' => 'classes/html2pdf1.6/Locale.php',
    'type' => 'class',
    'override' => false,
  ),
  'LocaleException' => 
  array (
    'path' => 'classes/html2pdf1.6/Exception/LocaleException.php',
    'type' => 'class',
    'override' => false,
  ),
  'LocaleTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/LocaleTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'LocalizationPack' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'LocalizationPackCore' => 
  array (
    'path' => 'classes/LocalizationPack.php',
    'type' => 'class',
    'override' => false,
  ),
  'LongSentenceException' => 
  array (
    'path' => 'classes/html2pdf1.6/Exception/LongSentenceException.php',
    'type' => 'class',
    'override' => false,
  ),
  'LongSentenceExceptionTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Exception/LongSentenceExceptionTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'Mail' => 
  array (
    'path' => 'override/classes/Mail.php',
    'type' => 'class',
    'override' => false,
  ),
  'MailCore' => 
  array (
    'path' => 'classes/Mail.php',
    'type' => 'class',
    'override' => false,
  ),
  'Manufacturer' => 
  array (
    'path' => 'override/classes/Manufacturer.php',
    'type' => 'class',
    'override' => false,
  ),
  'ManufacturerController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ManufacturerControllerCore' => 
  array (
    'path' => 'controllers/front/ManufacturerController.php',
    'type' => 'class',
    'override' => false,
  ),
  'ManufacturerCore' => 
  array (
    'path' => 'classes/Manufacturer.php',
    'type' => 'class',
    'override' => false,
  ),
  'Media' => 
  array (
    'path' => 'override/classes/Media.php',
    'type' => 'class',
    'override' => false,
  ),
  'MediaCore' => 
  array (
    'path' => 'classes/Media.php',
    'type' => 'class',
    'override' => false,
  ),
  'Message' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'MessageCore' => 
  array (
    'path' => 'classes/Message.php',
    'type' => 'class',
    'override' => false,
  ),
  'Meta' => 
  array (
    'path' => 'override/classes/Meta.php',
    'type' => 'class',
    'override' => false,
  ),
  'MetaCore' => 
  array (
    'path' => 'classes/Meta.php',
    'type' => 'class',
    'override' => false,
  ),
  'Module' => 
  array (
    'path' => 'override/classes/module/Module.php',
    'type' => 'class',
    'override' => false,
  ),
  'ModuleAdminController' => 
  array (
    'path' => '',
    'type' => 'abstract class',
    'override' => false,
  ),
  'ModuleAdminControllerCore' => 
  array (
    'path' => 'classes/controller/ModuleAdminController.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'ModuleCore' => 
  array (
    'path' => 'classes/module/Module.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'ModuleFrontController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ModuleFrontControllerCore' => 
  array (
    'path' => 'classes/controller/ModuleFrontController.php',
    'type' => 'class',
    'override' => false,
  ),
  'ModuleGraph' => 
  array (
    'path' => '',
    'type' => 'abstract class',
    'override' => false,
  ),
  'ModuleGraphCore' => 
  array (
    'path' => 'classes/module/ModuleGraph.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'ModuleGraphEngine' => 
  array (
    'path' => '',
    'type' => 'abstract class',
    'override' => false,
  ),
  'ModuleGraphEngineCore' => 
  array (
    'path' => 'classes/module/ModuleGraphEngine.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'ModuleGrid' => 
  array (
    'path' => '',
    'type' => 'abstract class',
    'override' => false,
  ),
  'ModuleGridCore' => 
  array (
    'path' => 'classes/module/ModuleGrid.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'ModuleGridEngine' => 
  array (
    'path' => '',
    'type' => 'abstract class',
    'override' => false,
  ),
  'ModuleGridEngineCore' => 
  array (
    'path' => 'classes/module/ModuleGridEngine.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'MustHaveTagsTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Tag/MustHaveTagsTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'MyAccountController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'MyAccountControllerCore' => 
  array (
    'path' => 'controllers/front/MyAccountController.php',
    'type' => 'class',
    'override' => false,
  ),
  'MyPdf' => 
  array (
    'path' => 'classes/html2pdf1.6/MyPdf.php',
    'type' => 'class',
    'override' => false,
  ),
  'MySQL' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'MySQLCore' => 
  array (
    'path' => 'classes/db/MySQL.php',
    'type' => 'class',
    'override' => false,
  ),
  'NewProductsController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'NewProductsControllerCore' => 
  array (
    'path' => 'controllers/front/NewProductsController.php',
    'type' => 'class',
    'override' => false,
  ),
  'Node' => 
  array (
    'path' => 'classes/html2pdf1.6/Parsing/Node.php',
    'type' => 'class',
    'override' => false,
  ),
  'Notification' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'NotificationCore' => 
  array (
    'path' => 'classes/Notification.php',
    'type' => 'class',
    'override' => false,
  ),
  'ObjectModel' => 
  array (
    'path' => '',
    'type' => 'abstract class',
    'override' => false,
  ),
  'ObjectModelCore' => 
  array (
    'path' => 'classes/ObjectModel.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'Order' => 
  array (
    'path' => 'override/classes/order/Order.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderCarrier' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'OrderCarrierCore' => 
  array (
    'path' => 'classes/order/OrderCarrier.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderCartRule' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'OrderCartRuleCore' => 
  array (
    'path' => 'classes/order/OrderCartRule.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderConfirmationController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'OrderConfirmationControllerCore' => 
  array (
    'path' => 'controllers/front/OrderConfirmationController.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderController' => 
  array (
    'path' => 'override/controllers/front/OrderController.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderControllerCore' => 
  array (
    'path' => 'controllers/front/OrderController.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderCore' => 
  array (
    'path' => 'classes/order/Order.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderDetail' => 
  array (
    'path' => 'override/classes/order/OrderDetail.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderDetailController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'OrderDetailControllerCore' => 
  array (
    'path' => 'controllers/front/OrderDetailController.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderDetailCore' => 
  array (
    'path' => 'classes/order/OrderDetail.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderDiscount' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'OrderDiscountCore' => 
  array (
    'path' => 'classes/order/OrderDiscount.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderFollowController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'OrderFollowControllerCore' => 
  array (
    'path' => 'controllers/front/OrderFollowController.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderHistory' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'OrderHistoryCore' => 
  array (
    'path' => 'classes/order/OrderHistory.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderInvoice' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'OrderInvoiceCore' => 
  array (
    'path' => 'classes/order/OrderInvoice.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderMessage' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'OrderMessageCore' => 
  array (
    'path' => 'classes/order/OrderMessage.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderOpcController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'OrderOpcControllerCore' => 
  array (
    'path' => 'controllers/front/OrderOpcController.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderPayment' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'OrderPaymentCore' => 
  array (
    'path' => 'classes/order/OrderPayment.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderReturn' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'OrderReturnController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'OrderReturnControllerCore' => 
  array (
    'path' => 'controllers/front/OrderReturnController.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderReturnCore' => 
  array (
    'path' => 'classes/order/OrderReturn.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderReturnState' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'OrderReturnStateCore' => 
  array (
    'path' => 'classes/order/OrderReturnState.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderSlip' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'OrderSlipController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'OrderSlipControllerCore' => 
  array (
    'path' => 'controllers/front/OrderSlipController.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderSlipCore' => 
  array (
    'path' => 'classes/order/OrderSlip.php',
    'type' => 'class',
    'override' => false,
  ),
  'OrderState' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'OrderStateCore' => 
  array (
    'path' => 'classes/order/OrderState.php',
    'type' => 'class',
    'override' => false,
  ),
  'PDF' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'PDF417' => 
  array (
    'path' => 'classes/TCPDF-master/include/barcodes/pdf417.php',
    'type' => 'class',
    'override' => false,
  ),
  'PDFCore' => 
  array (
    'path' => 'classes/pdf/PDF.php',
    'type' => 'class',
    'override' => false,
  ),
  'PDFGenerator' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'PDFGeneratorCore' => 
  array (
    'path' => 'classes/pdf/PDFGenerator.php',
    'type' => 'class',
    'override' => false,
  ),
  'Pack' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'PackCore' => 
  array (
    'path' => 'classes/Pack.php',
    'type' => 'class',
    'override' => false,
  ),
  'Page' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'PageCore' => 
  array (
    'path' => 'classes/Page.php',
    'type' => 'class',
    'override' => false,
  ),
  'PageNotFoundController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'PageNotFoundControllerCore' => 
  array (
    'path' => 'controllers/front/PageNotFoundController.php',
    'type' => 'class',
    'override' => false,
  ),
  'ParentOrderController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ParentOrderControllerCore' => 
  array (
    'path' => 'controllers/front/ParentOrderController.php',
    'type' => 'class',
    'override' => false,
  ),
  'ParsingTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Parsing/ParsingTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'PasswordController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'PasswordControllerCore' => 
  array (
    'path' => 'controllers/front/PasswordController.php',
    'type' => 'class',
    'override' => false,
  ),
  'Path' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Svg/Path.php',
    'type' => 'class',
    'override' => false,
  ),
  'PathErrorTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Tag/Svg/PathErrorTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'PathInvalidTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Tag/Svg/PathInvalidTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'PaymentCC' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'PaymentCCCore' => 
  array (
    'path' => 'classes/PaymentCC.php',
    'type' => 'class',
    'override' => false,
  ),
  'PaymentModule' => 
  array (
    'path' => '',
    'type' => 'abstract class',
    'override' => false,
  ),
  'PaymentModuleCore' => 
  array (
    'path' => 'classes/PaymentModule.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'PdfInvoiceController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'PdfInvoiceControllerCore' => 
  array (
    'path' => 'controllers/front/PdfInvoiceController.php',
    'type' => 'class',
    'override' => false,
  ),
  'PdfOrderReturnController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'PdfOrderReturnControllerCore' => 
  array (
    'path' => 'controllers/front/PdfOrderReturnController.php',
    'type' => 'class',
    'override' => false,
  ),
  'PdfOrderSlipController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'PdfOrderSlipControllerCore' => 
  array (
    'path' => 'controllers/front/PdfOrderSlipController.php',
    'type' => 'class',
    'override' => false,
  ),
  'Polygon' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Svg/Polygon.php',
    'type' => 'class',
    'override' => false,
  ),
  'PolygonErrorTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Tag/Svg/PolygonErrorTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'Polyline' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Svg/Polyline.php',
    'type' => 'class',
    'override' => false,
  ),
  'PolylineErrorTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Tag/Svg/PolylineErrorTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'Precompilato' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'PrecompilatoCore' => 
  array (
    'path' => 'classes/Precompilato.php',
    'type' => 'class',
    'override' => false,
  ),
  'PrestaShopAutoload' => 
  array (
    'path' => 'classes/PrestaShopAutoload.php',
    'type' => 'class',
    'override' => false,
  ),
  'PrestaShopBackup' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'PrestaShopBackupCore' => 
  array (
    'path' => 'classes/PrestaShopBackup.php',
    'type' => 'class',
    'override' => false,
  ),
  'PrestaShopCollection' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'PrestaShopCollectionCore' => 
  array (
    'path' => 'classes/PrestaShopCollection.php',
    'type' => 'class',
    'override' => false,
  ),
  'PrestaShopDatabaseException' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'PrestaShopDatabaseExceptionCore' => 
  array (
    'path' => 'classes/exception/PrestaShopDatabaseException.php',
    'type' => 'class',
    'override' => false,
  ),
  'PrestaShopException' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'PrestaShopExceptionCore' => 
  array (
    'path' => 'classes/exception/PrestaShopException.php',
    'type' => 'class',
    'override' => false,
  ),
  'PrestaShopLogger' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'PrestaShopLoggerCore' => 
  array (
    'path' => 'classes/PrestaShopLogger.php',
    'type' => 'class',
    'override' => false,
  ),
  'PrestaShopModuleException' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'PrestaShopModuleExceptionCore' => 
  array (
    'path' => 'classes/exception/PrestaShopModuleException.php',
    'type' => 'class',
    'override' => false,
  ),
  'PrestaShopPaymentException' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'PrestaShopPaymentExceptionCore' => 
  array (
    'path' => 'classes/exception/PrestaShopPaymentException.php',
    'type' => 'class',
    'override' => false,
  ),
  'PricesDropController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'PricesDropControllerCore' => 
  array (
    'path' => 'controllers/front/PricesDropController.php',
    'type' => 'class',
    'override' => false,
  ),
  'Product' => 
  array (
    'path' => 'override/classes/Product.php',
    'type' => 'class',
    'override' => false,
  ),
  'ProductController' => 
  array (
    'path' => 'override/controllers/front/ProductController.php',
    'type' => 'class',
    'override' => false,
  ),
  'ProductControllerCore' => 
  array (
    'path' => 'controllers/front/ProductController.php',
    'type' => 'class',
    'override' => false,
  ),
  'ProductCore' => 
  array (
    'path' => 'classes/Product.php',
    'type' => 'class',
    'override' => false,
  ),
  'ProductDownload' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ProductDownloadCore' => 
  array (
    'path' => 'classes/ProductDownload.php',
    'type' => 'class',
    'override' => false,
  ),
  'ProductSale' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ProductSaleCore' => 
  array (
    'path' => 'classes/ProductSale.php',
    'type' => 'class',
    'override' => false,
  ),
  'ProductSupplier' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ProductSupplierCore' => 
  array (
    'path' => 'classes/ProductSupplier.php',
    'type' => 'class',
    'override' => false,
  ),
  'Profile' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ProfileCore' => 
  array (
    'path' => 'classes/Profile.php',
    'type' => 'class',
    'override' => false,
  ),
  'QRcode' => 
  array (
    'path' => 'classes/html2pdf/_tcpdf_5.0.002/qrcode.php',
    'type' => 'class',
    'override' => false,
  ),
  'QuickAccess' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'QuickAccessCore' => 
  array (
    'path' => 'classes/QuickAccess.php',
    'type' => 'class',
    'override' => false,
  ),
  'RangePrice' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'RangePriceCore' => 
  array (
    'path' => 'classes/range/RangePrice.php',
    'type' => 'class',
    'override' => false,
  ),
  'RangeWeight' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'RangeWeightCore' => 
  array (
    'path' => 'classes/range/RangeWeight.php',
    'type' => 'class',
    'override' => false,
  ),
  'Rect' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Svg/Rect.php',
    'type' => 'class',
    'override' => false,
  ),
  'RectErrorTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Tag/Svg/RectErrorTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'Referrer' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ReferrerCore' => 
  array (
    'path' => 'classes/Referrer.php',
    'type' => 'class',
    'override' => false,
  ),
  'RequestSql' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'RequestSqlCore' => 
  array (
    'path' => 'classes/RequestSql.php',
    'type' => 'class',
    'override' => false,
  ),
  'Rijndael' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'RijndaelCore' => 
  array (
    'path' => 'classes/Rijndael.php',
    'type' => 'class',
    'override' => false,
  ),
  'Risk' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'RiskCore' => 
  array (
    'path' => 'classes/Risk.php',
    'type' => 'class',
    'override' => false,
  ),
  'S' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Html/S.php',
    'type' => 'class',
    'override' => false,
  ),
  'Samp' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Html/Samp.php',
    'type' => 'class',
    'override' => false,
  ),
  'Scene' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'SceneCore' => 
  array (
    'path' => 'classes/Scene.php',
    'type' => 'class',
    'override' => false,
  ),
  'Search' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'SearchController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'SearchControllerCore' => 
  array (
    'path' => 'controllers/front/SearchController.php',
    'type' => 'class',
    'override' => false,
  ),
  'SearchCore' => 
  array (
    'path' => 'classes/Search.php',
    'type' => 'class',
    'override' => false,
  ),
  'SearchEngine' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'SearchEngineCore' => 
  array (
    'path' => 'classes/SearchEngine.php',
    'type' => 'class',
    'override' => false,
  ),
  'Shop' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ShopCore' => 
  array (
    'path' => 'classes/shop/Shop.php',
    'type' => 'class',
    'override' => false,
  ),
  'ShopGroup' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ShopGroupCore' => 
  array (
    'path' => 'classes/shop/ShopGroup.php',
    'type' => 'class',
    'override' => false,
  ),
  'ShopUrl' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ShopUrlCore' => 
  array (
    'path' => 'classes/shop/ShopUrl.php',
    'type' => 'class',
    'override' => false,
  ),
  'SitemapController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'SitemapControllerCore' => 
  array (
    'path' => 'controllers/front/SitemapController.php',
    'type' => 'class',
    'override' => false,
  ),
  'Small' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Html/Small.php',
    'type' => 'class',
    'override' => false,
  ),
  'SmartyCustom' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'SmartyCustomCore' => 
  array (
    'path' => 'classes/SmartyCustom.php',
    'type' => 'class',
    'override' => false,
  ),
  'Span' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Html/Span.php',
    'type' => 'class',
    'override' => false,
  ),
  'SpecificPrice' => 
  array (
    'path' => 'override/classes/SpecificPrice.php',
    'type' => 'class',
    'override' => false,
  ),
  'SpecificPriceCore' => 
  array (
    'path' => 'classes/SpecificPrice.php',
    'type' => 'class',
    'override' => false,
  ),
  'SpecificPriceRule' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'SpecificPriceRuleCore' => 
  array (
    'path' => 'classes/SpecificPriceRule.php',
    'type' => 'class',
    'override' => false,
  ),
  'SrcErrorTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Image/SrcErrorTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'SrcOkTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Image/SrcOkTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'State' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'StateCore' => 
  array (
    'path' => 'classes/State.php',
    'type' => 'class',
    'override' => false,
  ),
  'StatisticsController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'StatisticsControllerCore' => 
  array (
    'path' => 'controllers/front/StatisticsController.php',
    'type' => 'class',
    'override' => false,
  ),
  'Stock' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'StockAvailable' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'StockAvailableCore' => 
  array (
    'path' => 'classes/stock/StockAvailable.php',
    'type' => 'class',
    'override' => false,
  ),
  'StockCore' => 
  array (
    'path' => 'classes/stock/Stock.php',
    'type' => 'class',
    'override' => false,
  ),
  'StockManager' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'StockManagerCore' => 
  array (
    'path' => 'classes/stock/StockManager.php',
    'type' => 'class',
    'override' => false,
  ),
  'StockManagerFactory' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'StockManagerFactoryCore' => 
  array (
    'path' => 'classes/stock/StockManagerFactory.php',
    'type' => 'class',
    'override' => false,
  ),
  'StockManagerInterface' => 
  array (
    'path' => 'classes/stock/StockManagerInterface.php',
    'type' => 'interface',
    'override' => false,
  ),
  'StockManagerModule' => 
  array (
    'path' => '',
    'type' => 'abstract class',
    'override' => false,
  ),
  'StockManagerModuleCore' => 
  array (
    'path' => 'classes/stock/StockManagerModule.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'StockMvt' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'StockMvtCore' => 
  array (
    'path' => 'classes/stock/StockMvt.php',
    'type' => 'class',
    'override' => false,
  ),
  'StockMvtReason' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'StockMvtReasonCore' => 
  array (
    'path' => 'classes/stock/StockMvtReason.php',
    'type' => 'class',
    'override' => false,
  ),
  'StockMvtWS' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'StockMvtWSCore' => 
  array (
    'path' => 'classes/stock/StockMvtWS.php',
    'type' => 'class',
    'override' => false,
  ),
  'Store' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'StoreCore' => 
  array (
    'path' => 'classes/Store.php',
    'type' => 'class',
    'override' => false,
  ),
  'StoresController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'StoresControllerCore' => 
  array (
    'path' => 'controllers/front/StoresController.php',
    'type' => 'class',
    'override' => false,
  ),
  'Strong' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Html/Strong.php',
    'type' => 'class',
    'override' => false,
  ),
  'Sub' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Html/Sub.php',
    'type' => 'class',
    'override' => false,
  ),
  'Sup' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Html/Sup.php',
    'type' => 'class',
    'override' => false,
  ),
  'Supplier' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'SupplierController' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'SupplierControllerCore' => 
  array (
    'path' => 'controllers/front/SupplierController.php',
    'type' => 'class',
    'override' => false,
  ),
  'SupplierCore' => 
  array (
    'path' => 'classes/Supplier.php',
    'type' => 'class',
    'override' => false,
  ),
  'SupplyOrder' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'SupplyOrderCore' => 
  array (
    'path' => 'classes/stock/SupplyOrder.php',
    'type' => 'class',
    'override' => false,
  ),
  'SupplyOrderDetail' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'SupplyOrderDetailCore' => 
  array (
    'path' => 'classes/stock/SupplyOrderDetail.php',
    'type' => 'class',
    'override' => false,
  ),
  'SupplyOrderHistory' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'SupplyOrderHistoryCore' => 
  array (
    'path' => 'classes/stock/SupplyOrderHistory.php',
    'type' => 'class',
    'override' => false,
  ),
  'SupplyOrderReceiptHistory' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'SupplyOrderReceiptHistoryCore' => 
  array (
    'path' => 'classes/stock/SupplyOrderReceiptHistory.php',
    'type' => 'class',
    'override' => false,
  ),
  'SupplyOrderState' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'SupplyOrderStateCore' => 
  array (
    'path' => 'classes/stock/SupplyOrderState.php',
    'type' => 'class',
    'override' => false,
  ),
  'SvgDrawer' => 
  array (
    'path' => 'classes/html2pdf1.6/SvgDrawer.php',
    'type' => 'class',
    'override' => false,
  ),
  'SvgDrawerTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/SvgDrawerTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'SvgExtension' => 
  array (
    'path' => 'classes/html2pdf1.6/Extension/Core/SvgExtension.php',
    'type' => 'class',
    'override' => false,
  ),
  'TCPDF' => 
  array (
    'path' => 'classes/html2pdf/_tcpdf_5.0.002/tcpdf.php',
    'type' => 'class',
    'override' => false,
  ),
  'TCPDF_COLORS' => 
  array (
    'path' => 'classes/TCPDF-master/include/tcpdf_colors.php',
    'type' => 'class',
    'override' => false,
  ),
  'TCPDF_FILTERS' => 
  array (
    'path' => 'classes/TCPDF-master/include/tcpdf_filters.php',
    'type' => 'class',
    'override' => false,
  ),
  'TCPDF_FONTS' => 
  array (
    'path' => 'classes/TCPDF-master/include/tcpdf_fonts.php',
    'type' => 'class',
    'override' => false,
  ),
  'TCPDF_FONT_DATA' => 
  array (
    'path' => 'classes/TCPDF-master/include/tcpdf_font_data.php',
    'type' => 'class',
    'override' => false,
  ),
  'TCPDF_IMAGES' => 
  array (
    'path' => 'classes/TCPDF-master/include/tcpdf_images.php',
    'type' => 'class',
    'override' => false,
  ),
  'TCPDF_IMPORT' => 
  array (
    'path' => 'classes/TCPDF-master/tcpdf_import.php',
    'type' => 'class',
    'override' => false,
  ),
  'TCPDF_PARSER' => 
  array (
    'path' => 'classes/TCPDF-master/tcpdf_parser.php',
    'type' => 'class',
    'override' => false,
  ),
  'TCPDF_STATIC' => 
  array (
    'path' => 'classes/TCPDF-master/include/tcpdf_static.php',
    'type' => 'class',
    'override' => false,
  ),
  'Tab' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'TabCore' => 
  array (
    'path' => 'classes/Tab.php',
    'type' => 'class',
    'override' => false,
  ),
  'TableException' => 
  array (
    'path' => 'classes/html2pdf1.6/Exception/TableException.php',
    'type' => 'class',
    'override' => false,
  ),
  'Tag' => 
  array (
    'path' => 'override/classes/Tag.php',
    'type' => 'class',
    'override' => false,
  ),
  'TagCore' => 
  array (
    'path' => 'classes/Tag.php',
    'type' => 'class',
    'override' => false,
  ),
  'TagInterface' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/TagInterface.php',
    'type' => 'interface',
    'override' => false,
  ),
  'TagParser' => 
  array (
    'path' => 'classes/html2pdf1.6/Parsing/TagParser.php',
    'type' => 'class',
    'override' => false,
  ),
  'TagParserTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Parsing/TagParserTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'Tax' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'TaxCalculator' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'TaxCalculatorCore' => 
  array (
    'path' => 'classes/tax/TaxCalculator.php',
    'type' => 'class',
    'override' => false,
  ),
  'TaxCore' => 
  array (
    'path' => 'classes/tax/Tax.php',
    'type' => 'class',
    'override' => false,
  ),
  'TaxManagerFactory' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'TaxManagerFactoryCore' => 
  array (
    'path' => 'classes/tax/TaxManagerFactory.php',
    'type' => 'class',
    'override' => false,
  ),
  'TaxManagerInterface' => 
  array (
    'path' => 'classes/tax/TaxManagerInterface.php',
    'type' => 'interface',
    'override' => false,
  ),
  'TaxManagerModule' => 
  array (
    'path' => '',
    'type' => 'abstract class',
    'override' => false,
  ),
  'TaxManagerModuleCore' => 
  array (
    'path' => 'classes/tax/TaxManagerModule.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'TaxRule' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'TaxRuleCore' => 
  array (
    'path' => 'classes/tax/TaxRule.php',
    'type' => 'class',
    'override' => false,
  ),
  'TaxRulesGroup' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'TaxRulesGroupCore' => 
  array (
    'path' => 'classes/tax/TaxRulesGroup.php',
    'type' => 'class',
    'override' => false,
  ),
  'TaxRulesTaxManager' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'TaxRulesTaxManagerCore' => 
  array (
    'path' => 'classes/tax/TaxRulesTaxManager.php',
    'type' => 'class',
    'override' => false,
  ),
  'TdTooLongTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Tag/TdTooLongTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'TextParser' => 
  array (
    'path' => 'classes/html2pdf1.6/Parsing/TextParser.php',
    'type' => 'class',
    'override' => false,
  ),
  'TextParserTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Parsing/TextParserTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'Theme' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ThemeCore' => 
  array (
    'path' => 'classes/Theme.php',
    'type' => 'class',
    'override' => false,
  ),
  'Token' => 
  array (
    'path' => 'classes/html2pdf1.6/Parsing/Token.php',
    'type' => 'class',
    'override' => false,
  ),
  'TokenTest' => 
  array (
    'path' => 'classes/html2pdf1.6/Tests/Parsing/TokenTest.php',
    'type' => 'class',
    'override' => false,
  ),
  'Tools' => 
  array (
    'path' => 'override/classes/Tools.php',
    'type' => 'class',
    'override' => false,
  ),
  'ToolsCore' => 
  array (
    'path' => 'classes/Tools.php',
    'type' => 'class',
    'override' => false,
  ),
  'Translate' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'TranslateCore' => 
  array (
    'path' => 'classes/Translate.php',
    'type' => 'class',
    'override' => false,
  ),
  'TranslatedConfiguration' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'TranslatedConfigurationCore' => 
  array (
    'path' => 'classes/TranslatedConfiguration.php',
    'type' => 'class',
    'override' => false,
  ),
  'Tree' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'TreeCore' => 
  array (
    'path' => 'classes/tree/Tree.php',
    'type' => 'class',
    'override' => false,
  ),
  'TreeToolbar' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'TreeToolbarButton' => 
  array (
    'path' => '',
    'type' => 'abstract class',
    'override' => false,
  ),
  'TreeToolbarButtonCore' => 
  array (
    'path' => 'classes/tree/TreeToolbarButton.php',
    'type' => 'abstract class',
    'override' => false,
  ),
  'TreeToolbarCore' => 
  array (
    'path' => 'classes/tree/TreeToolbar.php',
    'type' => 'class',
    'override' => false,
  ),
  'TreeToolbarLink' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'TreeToolbarLinkCore' => 
  array (
    'path' => 'classes/tree/TreeToolbarLink.php',
    'type' => 'class',
    'override' => false,
  ),
  'TreeToolbarSearch' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'TreeToolbarSearchCategories' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'TreeToolbarSearchCategoriesCore' => 
  array (
    'path' => 'classes/tree/TreeToolbarSearchCategories.php',
    'type' => 'class',
    'override' => false,
  ),
  'TreeToolbarSearchCore' => 
  array (
    'path' => 'classes/tree/TreeToolbarSearch.php',
    'type' => 'class',
    'override' => false,
  ),
  'U' => 
  array (
    'path' => 'classes/html2pdf1.6/Tag/Html/U.php',
    'type' => 'class',
    'override' => false,
  ),
  'Upgrader' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'UpgraderCore' => 
  array (
    'path' => 'classes/Upgrader.php',
    'type' => 'class',
    'override' => false,
  ),
  'Uploader' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'UploaderCore' => 
  array (
    'path' => 'classes/Uploader.php',
    'type' => 'class',
    'override' => false,
  ),
  'Validate' => 
  array (
    'path' => 'override/classes/Validate.php',
    'type' => 'class',
    'override' => false,
  ),
  'ValidateCore' => 
  array (
    'path' => 'classes/Validate.php',
    'type' => 'class',
    'override' => false,
  ),
  'Warehouse' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'WarehouseCore' => 
  array (
    'path' => 'classes/stock/Warehouse.php',
    'type' => 'class',
    'override' => false,
  ),
  'WarehouseProductLocation' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'WarehouseProductLocationCore' => 
  array (
    'path' => 'classes/stock/WarehouseProductLocation.php',
    'type' => 'class',
    'override' => false,
  ),
  'WebserviceException' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'WebserviceExceptionCore' => 
  array (
    'path' => 'classes/webservice/WebserviceException.php',
    'type' => 'class',
    'override' => false,
  ),
  'WebserviceKey' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'WebserviceKeyCore' => 
  array (
    'path' => 'classes/webservice/WebserviceKey.php',
    'type' => 'class',
    'override' => false,
  ),
  'WebserviceOutputBuilder' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'WebserviceOutputBuilderCore' => 
  array (
    'path' => 'classes/webservice/WebserviceOutputBuilder.php',
    'type' => 'class',
    'override' => false,
  ),
  'WebserviceOutputInterface' => 
  array (
    'path' => 'classes/webservice/WebserviceOutputInterface.php',
    'type' => 'interface',
    'override' => false,
  ),
  'WebserviceOutputJSON' => 
  array (
    'path' => 'classes/webservice/WebserviceOutputJSON.php',
    'type' => 'class',
    'override' => false,
  ),
  'WebserviceOutputXML' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'WebserviceOutputXMLCore' => 
  array (
    'path' => 'classes/webservice/WebserviceOutputXML.php',
    'type' => 'class',
    'override' => false,
  ),
  'WebserviceRequest' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'WebserviceRequestCore' => 
  array (
    'path' => 'classes/webservice/WebserviceRequest.php',
    'type' => 'class',
    'override' => false,
  ),
  'WebserviceSpecificManagementImages' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'WebserviceSpecificManagementImagesCore' => 
  array (
    'path' => 'classes/webservice/WebserviceSpecificManagementImages.php',
    'type' => 'class',
    'override' => false,
  ),
  'WebserviceSpecificManagementInterface' => 
  array (
    'path' => 'classes/webservice/WebserviceSpecificManagementInterface.php',
    'type' => 'interface',
    'override' => false,
  ),
  'WebserviceSpecificManagementSearch' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'WebserviceSpecificManagementSearchCore' => 
  array (
    'path' => 'classes/webservice/WebserviceSpecificManagementSearch.php',
    'type' => 'class',
    'override' => false,
  ),
  'Zone' => 
  array (
    'path' => '',
    'type' => 'class',
    'override' => false,
  ),
  'ZoneCore' => 
  array (
    'path' => 'classes/Zone.php',
    'type' => 'class',
    'override' => false,
  ),
); ?>