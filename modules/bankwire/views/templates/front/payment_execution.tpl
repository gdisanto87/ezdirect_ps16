{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}
	<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html':'UTF-8'}" title="{l s='Go back to the Checkout' mod='bankwire'}">{l s='Checkout' mod='bankwire'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='Bank-wire payment' mod='bankwire'}
{/capture}

{include file="$tpl_dir./breadcrumb.tpl"}


<h2>{l s='Order summary' mod='bankwire'}</h2>
	


{assign var='current_step' value='payment'}


{if $nbProducts <= 0}
	<p class="warning">{l s='Your shopping cart is empty.' mod='bankwire'}</p>
{else}

<h3 style="">{l s='Bank-wire payment' mod='bankwire'}</h3>
{include file="$tpl_dir./order-steps.tpl"}
<form action="{$link->getModuleLink('bankwire', 'validation', [], true)|escape:'html'}" method="post" id="address-form-long">
<p style="    display: flex; flex-direction: column; text-align: center;">
	<i style="font-size: 60px; color: #30b1c7; padding-bottom: 10px;" class="fas fa-file-invoice-dollar"></i>
	{l s='You have chosen to pay by bank wire.' mod='bankwire'}
	<br/><br />
	
</p>
<div style="
    padding-top: 20px;">
<p>{l s='Here is a short summary of your order:' mod='bankwire'}</p>
<p style="margin-top:20px;">

	- {l s='The total amount of your order is' mod='bankwire'}
	<span id="amount" class="price">{displayPrice price=$total}</span>
	{if $use_taxes == 1}
    	{l s='(I.V.A. incl.)' mod='bankwire'}
    {/if}
</p>
<p>
	-
	{if $currencies|@count > 1}
		{l s='We allow several currencies to be sent via bank wire.' mod='bankwire'}
		<br /><br />
		{l s='Choose one of the following:' mod='bankwire'}
		<select id="currency_payement" name="currency_payement" onchange="setCurrency($('#currency_payement').val());">
			{foreach from=$currencies item=currency}
				<option value="{$currency.id_currency}" {if $currency.id_currency == $cust_currency}selected="selected"{/if}>{$currency.name}</option>
			{/foreach}
		</select>
	{else}
		{l s='We allow the following currency to be sent via bank wire:' mod='bankwire'}&nbsp;<b>{$currencies.0.name}</b>
		<input type="hidden" name="currency_payement" value="{$currencies.0.id_currency}" />
	{/if}
</p>
<p>
	{l s='Bank wire account information will be displayed on the next page.' mod='bankwire'}
	<br /><br />
	<b>{l s='Please confirm your order by clicking "I confirm my order".' mod='bankwire'}</b>
</p>
</div>
<p class="cart_navigation submit"  id="prev-next-carrier">
	<input type="submit" value="{l s='I confirm my order' mod='bankwire'}" class="exclusive_large" style="padding: 8px 20px; border: none; " />
	<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html'}" class="button_large" style=" padding: 8px 20px; border: none; font-weight: normal; ">{l s='Other payment methods' mod='bankwire'}</a>
</p>
</form>
{/if}
