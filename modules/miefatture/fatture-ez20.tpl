{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div id="miefatture">
	{capture name=path}<a href="{$link->getPageLink('my-account.php', true)}">{l s='My account' mod='miefatture'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='My invoices' mod='miefatture'}{/capture}
	{include file="$tpl_dir./breadcrumb.tpl"}

	<h2>{l s='My invoices' mod='miefatture'}</h2>
	<br />
	
	{include file="$tpl_dir./errors.tpl"}

	
	
	
		{if $fatture}
			<table class="std2">
				<thead>
					<tr>
						<th class="first_item" style="background-color:#8899cc;text-align:left">{l s='Invoice ID' mod='miefatture'}</th>
						
						<th style="background-color:#99aadd; text-align:left">{l s='Ref. order' mod='miefatture'}</th>
						<th style="background-color:#8899cc;text-align:left">{l s='Invoice date' mod='miefatture'}</th>
						<th class="last_item" style="background-color:#99aadd; text-align:left">{l s='Download' mod='miefatture'}</th>
					</tr>
				</thead>
				<tbody>
				{foreach from=$fatture item=fattura}
				
				
				
				
				
				<tr>
					<td>
				
					{$fattura.id_fattura}
				
				</td>
					<td>{$fattura.rif_vs_ordine}</td>
					
					<td>{$fattura.data_fattura|date_format:"%d/%m/%Y"}</td>
					<td><a href="/fattura.php?id_fattura={$fattura.id_fattura}&id_customer={$fattura.id_customer}">{l s='Click here to download your invoice' mod='miefatture'}</a></td>
					
					
				</tr>
				</tbody>
			{/foreach}
			</table>
		
		{else}
		
			<p class="warning">{l s='You have no invoices' mod='miefatture'}.</p>
		
		{/if}
		
	
</div>
