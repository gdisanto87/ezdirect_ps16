<?php
/* SSL Management */
$useSSL = true;

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');
// include_once(dirname(__FILE__).'/mytickets.php');

$errors = array();


	if ($cookie->isLogged())
	{
		
		
		
		$customer = $cookie->id_customer;
		$language = $cookie->id_lang;
		
		if(!Tools::getValue('id_fattura')) {
		
			$fatture = Db::getInstance()->ExecuteS("SELECT * FROM "._DB_PREFIX_."fattura WHERE tipo != 'tecnotel' AND id_customer = ".$customer." GROUP BY id_fattura ORDER BY data_fattura DESC");
			
		
			$smarty->assign(array(
			'fatture' => $fatture,
			));
		
		}
		
		else {
			
			$fattura_data = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."fattura WHERE tipo != 'tecnotel' AND id_customer = ".$customer." AND id_fattura = ".Tools::getValue('id_fattura')."");
			
			$smarty->assign(array(
			'fattura' => $fattura,
			'link' => '/fattura.php?id_fattura='.$fattura_data['id_fattura'].'&id_customer='.$cookie->id_customer.''
			));
		
		}
		
		
		if (_THEME_NAME_ == 'ez20')
			$smarty->display(dirname(__FILE__).'/fatture-ez20.tpl');
		else
			$smarty->display(dirname(__FILE__).'/fatture.tpl');
		
	}
	
	else {
		$errors[] = Tools::displayError('You must be logged in to see your invoices.'); 
		$smarty->assign(array(
			'errors' => $errors
		));
	}

	include(dirname(__FILE__).'/../../footer.php');
