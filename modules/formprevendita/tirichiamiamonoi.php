<?php
/* SSL Management */
$useSSL = true;

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');


$errors = array();

global $cookie;
global $smarty;



/*
	if (Tools::isSubmit('submitPIVA'))
		{
		if(strlen(Tools::getValue('vat_number')) != 11) {
			$ctz = Tools::getValue('category');
			if(!empty($ctz)) {
				header("Location: http://www.ezdirect.it/modules/formprevendita/tirichiamiamonoi.php?error=1&category=$ctz");
			}
			else {
				header("Location: http://www.ezdirect.it/modules/formprevendita/tirichiamiamonoi.php?error=1");
			}
		}
		

	}
	if (Tools::isSubmit('submitCF'))
		{
		if(strlen(Tools::getValue('tax_code')) != 11 && strlen(Tools::getValue('tax_code')) != 16) {
			$ctz = Tools::getValue('category');
			if(!empty($ctz)) {
				header("Location: http://www.ezdirect.it/modules/formprevendita/tirichiamiamonoi.php?error=2&category=$ctz");
			}
			else {
				header("Location: http://www.ezdirect.it/modules/formprevendita/tirichiamiamonoi.php?error=2");
			}
			
		}

	}
	*/
	if(isset($_GET['error']) && $_GET['error'] == 1) {
	$errors[] = Tools::displayError('VAT Number is not valid');				
			$smarty->assign(array(
				'errors' => $errors
			));
	}
	
	if(isset($_GET['error']) && $_GET['error'] == 2) {
	$errors[] = Tools::displayError('Tax code is not valid');				
			$smarty->assign(array(
				'errors' => $errors
			));
	}
	
	if(isset($_GET['id_thread']) && isset($_GET['token'])) {
		
		$ilthread = Db::getInstance()->getValue("SELECT id_thread FROM form_prevendita_thread WHERE id_thread = '$_GET[id_thread]' AND token = '$_GET[token]'");
		
		if(!empty($ilthread)) {
			
			header("Location: http://www.ezdirect.it/modules/formprevendita/tirichiamiamonoi.php?step=2&id_thread2=$_GET[id_thread]&token2=$_GET[token]");
		}
			
		else {
			

		}
			
	}

	if(!isset($_GET['step'])) {
		if ($cookie->isLogged())
		{
			if(isset($_GET['id_thread']) && isset($_GET['token'])) {
			
				header("Location: http://www.ezdirect.it/modules/formprevendita/tirichiamiamonoi.php?step=2&id_thread2=$_GET[id_thread]&token2=$_GET[token]");
			
			}
			else if(isset($_GET['category'])) {
			header("Location: http://www.ezdirect.it/modules/formprevendita/tirichiamiamonoi.php?step=2&category=$_GET[category]");
			}
			else {
			header("Location: http://www.ezdirect.it/modules/formprevendita/tirichiamiamonoi.php?step=2");
			}
		}
	} else {
	
	}
	
	if(isset($_GET['step']) && $_GET['step'] == 2) {
		if ($cookie->isLogged())
		{
			
			$smarty->assign('isLogged', 1);
			$id_customer = $cookie->id_customer;
			$where = "WHERE c.id_customer = ".$id_customer."";
			
		}	
		
		else if(Tools::getValue('id_customer')) {
			$where = "WHERE c.id_customer = '".Tools::getValue('id_customer')."' AND c.active = 1";
			
		}
		
		/*else if(Tools::getValue('tax_code')) {
			$where = "WHERE c.vat_number != '' AND c.active = 1 AND c.tax_code != '' AND (c.vat_number = '".Tools::getValue('tax_code')."' OR c.tax_code = '".Tools::getValue('tax_code')."')";
			
		}*/
		
		if(Tools::getValue('id_thread2') && Tools::getValue('token2')) {
		
			$customerthr = Db::getInstance()->getValue("SELECT id_customer FROM form_prevendita_thread WHERE id_thread = '$_GET[id_thread2]' AND token = '$_GET[token2]'");
		
			$ilthread = Db::getInstance()->getValue("SELECT id_thread FROM form_prevendita_thread WHERE id_thread = '$_GET[id_thread2]' AND token = '$_GET[token2]'");
			$categoria = Db::getInstance()->getValue("SELECT category FROM form_prevendita_thread WHERE id_thread = '$_GET[id_thread2]' AND token = '$_GET[token2]'");
			$statusp = Db::getInstance()->getValue("SELECT status FROM form_prevendita_thread WHERE id_thread = '$_GET[id_thread2]' AND token = '$_GET[token2]'");
			
			$smarty->assign('statusp', $statusp);
			$smarty->assign('id_thread', $ilthread);
			$smarty->assign('category', $categoria);
			$where = "WHERE c.id_customer = ".$customerthr."";
			
		}
		if(!$where) {
			
		
		
		}
		else {
			$row = Db::getInstance()->getRow("SELECT c.id_customer, c.firstname, c.lastname, c.company, c.tax_code, c.vat_number, c.email, a.address1, a.postcode, a.city, s.iso_code AS state, a.id_country AS country, a.id_state AS state, a.phone FROM customer c JOIN address a ON c.id_customer = a.id_customer LEFT JOIN state s ON a.id_state = s.id_state LEFT JOIN country co ON a.id_country = co.id_country ".$where."  AND a.fatturazione = 1 AND a.active = 1 AND a.deleted = 0");
			$countries = Country::getCountries((int)$cookie->id_lang, true);
		}
		if (isset($_GET['category'])) {
			$categoryric = $_GET['category'];
		}
		else if (isset($_POST['category'])) {
			$categoryric = $_POST['category'];
		}
		else {
			$categoryric = "";
		}
		
		$prodottorichiesto =  Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_product = ".$categoryric." AND id_lang = 5");
		
		$smarty->assign('countries', $countries);
		$smarty->assign('id_customer', $row['id_customer']);
		$smarty->assign('firstname', $row['firstname']);
		$smarty->assign('lastname', $row['lastname']);
		$smarty->assign('tax_code', $row['tax_code']);
		$smarty->assign('vat_number', $row['vat_number']);
		$smarty->assign('company', $row['company']);
		$smarty->assign('prodottorichiesto', $prodottorichiesto);
		$smarty->assign('email', $row['email']);
		$smarty->assign('address1', $row['address1']);
		$smarty->assign('postcode', $row['postcode']);
		$smarty->assign('city', $row['city']);
		$smarty->assign('provincia', $row['state']);
		$smarty->assign('paese', $row['country']);
		$smarty->assign('phone', $row['phone']);
	
	}	
	
	
	if (Tools::isSubmit('submitMessage'))
		{
		
			if (isset($_FILES['fileUpload'])) {
			
				$files=$_FILES["fileUpload"];

				$files=array();
				$fdata=$_FILES['fileUpload'];
				if(is_array($fdata['name'])){
					for($i=0;$i<count($fdata['name']);++$i){
						$files[]=array(
							'name'    =>$fdata['name'][$i],
							'tmp_name'=>$fdata['tmp_name'][$i],
							'type' => $fdata['type'][$i],
							'size' => $fdata['size'][$i],
							'error' => $fdata['error'][$i],
								 
						);
					}
				}
				else $files[]=$fdata;
											
				$attachments = array();
				
				foreach($files as $file) {
					$extension = array('.txt', '.rtf', '.doc', '.docx', '.pdf', '.zip', '.png', '.jpeg', '.gif', '.jpg');
					if (!empty($file['name']) AND $file['error'] != 0)
					$errors[] = Tools::displayError('An error occurred during the file upload');
					elseif (!empty($file['name']) AND !in_array(substr($file['name'], -4), $extension) AND !in_array(substr($file['name'], -5), $extension))
					$errors[] = Tools::displayError('Bad file extension');
					$filename = md5(uniqid().substr($file['name'], -5));
					$fileAttachment['content'] = file_get_contents($file['tmp_name']);
					$fileAttachment['name'] = $file['name'];
					$fileAttachment['mime'] = $file['type'];
					if (isset($filename) AND rename($file['tmp_name'], _PS_MODULE_DIR_.'../upload/'.$filename)) {
						$filename = $filename.":::".$file['name'];
						$file_name .= $filename.";";
					}
					
					$attachments[] = $fileAttachment;
				}
				
				
				
			}
			
			else {
			
				$attachments = NULL;
			
			}
			
			if (empty($_FILES['fileUpload'])) {

				$attachments = NULL;
				
			}
			
			
			$message = Tools::htmlentitiesUTF8(Tools::getValue('message'));
	
			if (!($from = trim(Tools::getValue('email'))) OR !Validate::isEmail($from))
				$errors[] = Tools::displayError('Invalid e-mail address');
			if (!($message = nl2br2($message)))
				$errors[] = Tools::displayError('Message cannot be blank');
			if (!Validate::isCleanHtml($message))
				$errors[] = Tools::displayError('Invalid message');
			if (Tools::strlen(Tools::getValue('firstname')) < 2)
				$errors[] = Tools::displayError('Firstname is required');
			if (Tools::strlen(Tools::getValue('lastname')) < 2)
				$errors[] = Tools::displayError('Lastname is required');
			/*if (
			
			(Tools::strlen(Tools::getValue('tax_code')) > 0 AND (Tools::strlen(Tools::getValue('tax_code')) != 11 || Tools::strlen(Tools::getValue('tax_code')) != 16))
			
			
			)
				$errors[] = Tools::displayError('Tax code is not valid');
			*/
			/*if (
			
			(Tools::strlen(Tools::getValue('vat_number')) > 0 AND Tools::strlen(Tools::getValue('vat_number')) != 11)
			|| (Tools::strlen(Tools::getValue('vat_number')) > 0 AND Customer::ControllaPIVA(Tools::getValue('vat_number')) == false)
			
			)
				$errors[] = Tools::displayError('VAT Number is not valid');*/
			if(Tools::strlen(Tools::getValue('vat_number')) > 1 AND Customer::vatNumberExists(Tools::getValue('vat_number')))
				$pic = 'y';
			if(Tools::strlen(Tools::getValue('tax_code')) > 1 AND Customer::taxCodeExists(Tools::getValue('tax_code')))
				$cfc = 'y';
			/*if (Tools::strlen(Tools::getValue('vat_number')) > 0 AND Tools::strlen(Tools::getValue('company')) < 2)
				$errors[] = Tools::displayError('Company is required');*/
			/*if (Tools::getValue('id_country') == 10 AND (Tools::strlen(Tools::getValue('tax_code')) != 16 && Tools::strlen(Tools::getValue('tax_code')) != 11))
				$errors[] = Tools::displayError('Tax code is not valid');*/
			/*if (Tools::strlen(Tools::getValue('tax_code')) < 2)
				$errors[] = Tools::displayError('Tax code is not valid');*/
			/*if (Tools::strlen(Tools::getValue('address1')) < 2)
				$errors[] = Tools::displayError('Address is required');
			if (Tools::strlen(Tools::getValue('postcode')) == 0)
				$errors[] = Tools::displayError('Postcode is required');
			if (Tools::strlen(Tools::getValue('city')) < 2)
				$errors[] = Tools::displayError('City is required');
			if (Tools::strlen(Tools::getValue('id_country')) == 0)
				$errors[] = Tools::displayError('Country is required');	
			if (Country::containsStates(Tools::getValue('id_country')) AND !(int)(Tools::getValue('id_state')))
				$errors[] = Tools::displayError('This country requires a state selection.');*/
			if (Tools::strlen(Tools::getValue('phone')) < 2)
				$errors[] = Tools::displayError('Phone is required');	
			if (!(Tools::getValue('consenso_1')))
				$errors[] = Tools::displayError('You must accept privacy terms in order to complete this module');	
			
			if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {
				
				//your site secret key
				$secret = '6LciANsZAAAAAA7BMGycAorSGIzFaiDLqtCo0wiO';
				//get verify response data
				$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
				$responseData = json_decode($verifyResponse);
				if($responseData->success)
				{
					
					
				}
				else
				{
					$errors[] = Tools::displayError('Non hai superato la verifica robot');	
				}
			}
			else
			{
				$errors[] = Tools::displayError('Non hai superato la verifica robot');	
			}
			
			if (empty($errors))
			{
				
				
				$smarty->assign(array(
				'inviato' => 1
				));
				
				$ultimothread = Db::getInstance()->getValue("SELECT id_thread FROM form_prevendita_thread ORDER BY id_thread DESC");
				
				if(!$ultimothread) {
					$ultimothread = 0;
				}
						
				$token = Tools::passwdGen(12);
				
				if(!$cookie->id_customer) {
				
					/*if(!Tools::getValue('vat_number')) {
					
						$customer = Db::getInstance()->getValue("SELECT id_customer FROM customer WHERE tax_code = '".Tools::getValue('tax_code')."'");
					
					}
					
					else {
					
						$customer = Db::getInstance()->getValue("SELECT id_customer FROM customer WHERE vat_number = '".Tools::getValue('vat_number')."'");
					
					}*/
					
					if(Tools::getValue('id_customer')) {
					
						$customer = Db::getInstance()->getValue("SELECT id_customer FROM customer WHERE id_customer = '".Tools::getValue('id_customer')."'");
					
					}
				
				
				}
				
				else {
				
					$customer = $cookie->id_customer;
				
				}
				/*$piva = Tools::getValue('vat_number');
				if(!empty($piva)) {
					$query = Db::getInstance()->getValue("SELECT count(id_customer) FROM customer WHERE (vat_number = '".Tools::getValue('vat_number')."' OR email = '".trim(Tools::getValue('email'))."')");
				}
				else {
					$query = Db::getInstance()->getValue("SELECT count(id_customer) FROM customer WHERE (tax_code = '".Tools::getValue('tax_code')."' OR email = '".trim(Tools::getValue('email'))."')");
				}*/
			
				$rmail = Tools::getValue('email');
				if(!empty($rmail)) {
					$query = Db::getInstance()->getValue("SELECT count(id_customer) FROM customer WHERE (email = '".trim(Tools::getValue('email'))."')");
				}
				
				if($query > 0) {
					$nome = Tools::getValue('firstname');
					$cognome = Tools::getValue('lastname');
					/*if(!empty($piva))
						$numcustomer = Db::getInstance()->getValue("SELECT id_customer FROM customer WHERE (vat_number = '".Tools::getValue('vat_number')."' OR email = '".trim(Tools::getValue('email'))."')");
					else
						$numcustomer = Db::getInstance()->getValue("SELECT id_customer FROM customer WHERE (tax_code = '".Tools::getValue('tax_code')."' OR email = '".trim(Tools::getValue('email'))."')");
					*/
					$numcustomer = Db::getInstance()->getValue("SELECT id_customer FROM customer WHERE (email = '".trim(Tools::getValue('email'))."')");
					$customer = $numcustomer;
				
					$persona = Db::getInstance()->getValue("SELECT count(id_customer) FROM persone WHERE firstname = '".addslashes($nome)."' AND lastname = '".addslashes($cognome)."' AND id_customer = ".$numcustomer."");
					$is_company = Db::getInstance()->getValue("SELECT is_company FROM customer WHERE id_customer = ".$numcustomer."");
		
					if($persona == 0 && $is_company == 1)
					{
						
						Db::getInstance()->ExecuteS("INSERT INTO persone (id_persona, firstname, lastname, email, email_aziendale, phone, phone_mobile, id_customer, tax_code, vat_number) VALUES ('NULL', '".addslashes($nome)."', '".addslashes($cognome)."', '".addslashes(trim(Tools::getValue('email')))."', '".addslashes(trim(Tools::getValue('email')))."', '".addslashes(Tools::getValue('phone'))."', '', $numcustomer, '".addslashes(Tools::getValue('tax_code'))."', '".addslashes(Tools::getValue('vat_number'))."')");
						
						$id_persona = Db::getInstance()->getValue('SELECT id_persona FROM persone WHERE id_customer = '.$numcustomer.'');
						Db::getInstance()->executeS('UPDATE customer SET referente = '.$id_persona.' WHERE id_customer = '.$numcustomer.'');
					}
				}
				
				else {
				
					$numcustomer = Db::getInstance()->getValue("SELECT id_customer FROM customer ORDER BY id_customer DESC");
					$numaddress = Db::getInstance()->getValue("SELECT id_address FROM address ORDER BY id_address DESC"); 
					$numcustomer = $numcustomer+1;
					$numaddress = $numaddress+1;
					$profilo = 1;
					$azienda = Tools::getValue('company');
					$partita_iva_azienda = Tools::getValue('vat_number');
				
					if(!empty($azienda)) {
						$company = 1;
						$gruppo = 1;
						$nome = Tools::getValue('firstname');
						$cognome = Tools::getValue('lastname');
						$impiegati = 'Da 1 a 10';
						$azienda = Tools::getValue('company');
						$password_d = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(8/strlen($x)) )),1,8);
						//$password_d = 'password';
						$password = Tools::encrypt($password_d);
					}
					else {
						$company = 0;
						$gruppo = 2;
						$cognome = Tools::getValue('lastname');
						$nome = Tools::getValue('firstname');
						$impiegati = '';
						$azienda = '';
						$password_d = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(8/strlen($x)) )),1,8);
						//$password_d = 'password';
						$password = Tools::encrypt($password_d);
					}
				
					$date_add = date("Y-m-d H:i:s");
					$date_upd = date("Y-m-d H:i:s");
					
					$customer = $numcustomer;
				
					//CREO L'ANAGRAFICA DEL NUOVO CLIENTE
					
					if($pic == 'y')
						$numcustomer = Db::getInstance()->getValue('SELECT id_customer FROM customer WHERE vat_number = "'.Tools::getValue('vat_number').'"');
					
					if($cfc == 'y')
						$numcustomer = Db::getInstance()->getValue('SELECT id_customer FROM customer WHERE tax_code = "'.Tools::getValue('tax_code').'"');
					
					$customer = $numcustomer;
					
					if($pic != 'y' && $cfc != 'y')
					{
					
						Db::getInstance()->ExecuteS("INSERT INTO customer (id_customer, id_gender, id_default_group, is_company, firstname, lastname, email, passwd, last_passwd_gen, birthday, tax_code, vat_number, company, employees_number, newsletter, ip_registration_newsletter, newsletter_date_add, optin, secure_key, note, active, is_guest, deleted, date_add, date_upd, consenso_1, consenso_2, consenso_3) VALUES ($numcustomer, 1, ".$gruppo.", '".addslashes($company)."', '".addslashes($nome)."', '".addslashes($cognome)."', '".addslashes(trim(Tools::getValue('email')))."', '$password', '$date_add', '', '".addslashes(Tools::getValue('tax_code'))."', '".addslashes(Tools::getValue('vat_number'))."', '".addslashes($azienda)."', '$impiegati', 1, '', '$date_add', 0, '', '', 1, 0, 0, '$date_add', '$date_upd', '".Tools::getValue('consenso_1')."', '".Tools::getValue('consenso_2')."', '".Tools::getValue('consenso_3')."');");
						
						
						
						
						if($company == 1)
							Db::getInstance()->ExecuteS("INSERT INTO customer_group (id_customer, id_group) VALUES ($numcustomer, 1);");
						else
							Db::getInstance()->ExecuteS("INSERT INTO customer_group (id_customer, id_group) VALUES ($numcustomer, 2);");
						
						if(strpos(strtolower($company), 's.p.a') !== false || strpos(strtolower($company), 's.p.a.') !== false || strpos(strtolower($company), ' spa') !== false || strpos(strtolower($company), ' spa ') !== false)
						{
							Db::getInstance()->executeS("DELETE FROM customer_group WHERE id_customer = ".$numcustomer);

							Db::getInstance()->executeS("INSERT INTO customer_group (id_customer, id_group) VALUES (".$numcustomer.", 8)");
							Db::getInstance()->executeS('UPDATE customer SET id_default_group = 8 WHERE id_customer = '.$numcustomer.'');	
							
						}
								
								
						
						if(!Tools::getValue('id_country'))
							$id_country = 10;
							
						$address1 = Tools::getValue('address1');
						if($address1 == '')
							$address1 = ' ';
							
						$city = Tools::getValue('city');
						if($city == '')
							$city = ' ';
							
						Db::getInstance()->ExecuteS("INSERT INTO address (id_address, id_country, id_state, id_customer, id_manufacturer, id_supplier, alias, company, lastname, firstname, address1, address2, postcode, suburb, city, other, phone, phone_mobile, fax, vat_number, tax_code, company_tax_code, dni, date_add, date_upd, active, deleted, fatturazione) VALUES ($numaddress, '".$id_country."', '".Tools::getValue('id_state')."', $numcustomer, 0, 0, 'Il mio indirizzo', '".addslashes($azienda)."', '".addslashes($cognome)."', '".addslashes($nome)."', '".addslashes($address1)."', '', '".addslashes(Tools::getValue('postcode'))."', '', '".addslashes($city)."', '', '".addslashes(Tools::getValue('phone'))."', '', '', '', '', '', '', '$date_add', '$date_upd', 1,0,1);");
						
						if($company == 1) {
						
							Db::getInstance()->ExecuteS("INSERT INTO persone (id_persona, firstname, lastname, email, email_aziendale, phone, phone_mobile, id_customer, tax_code, vat_number) VALUES ('NULL', '".addslashes($nome)."', '".addslashes($cognome)."', '".addslashes(trim(Tools::getValue('email')))."', '".addslashes(trim(Tools::getValue('email')))."', '".addslashes(Tools::getValue('phone'))."', '', $numcustomer, '".addslashes(Tools::getValue('tax_code'))."', '".addslashes(Tools::getValue('vat_number'))."')");
							
							$id_persona = Db::getInstance()->getValue('SELECT id_persona FROM persone WHERE id_customer = '.$numcustomer.'');
							Db::getInstance()->executeS('UPDATE customer SET referente = '.$id_persona.' WHERE id_customer = '.$numcustomer.'');
						
						}
						
						else {
						
						
						}
						
						$params = array('{msg}' => "Gentile cliente, <br /><br />
							Grazie per la tua richiesta. 
							<br /><br />
							Ti informiamo che a seguito di questa richiesta, &egrave; stata effettuata una preregistrazione per consentire al nostro staff di gestire la richiesta sul tuo account.<br /><br />
							Per accedere al tuo account, puoi utilizzare come user-name la tua email <strong>".trim(Tools::getValue('email'))."</strong> e come password provvisoria: <strong>".$password_d."</strong>.<br /><br />
							Potrai cambiare la password in autonomia in qualunque momento.<br /><br />
							Se hai bisogno di supporto contatta il nostro staff. Grazie per la tua attenzione,<br /><br />
							<strong>Lo staff di Ezdirect</strong>");
								
							
					
					}
					else
					{
						if(!Tools::getValue('id_country'))
							$id_country = 10;
						
						$emaile = Db::getInstance()->getValue('SELECT email FROM customer WHERE id_customer = '.$numcustomer);
						if($emaile != Tools::getValue('email'))
						{
							if(Tools::getValue('vat_number') != '')
							{
								Db::getInstance()->ExecuteS("INSERT INTO persone (id_persona, firstname, lastname, email, email_aziendale, phone, phone_mobile, id_customer, tax_code, vat_number) VALUES ('NULL', '".addslashes($nome)."', '".addslashes($cognome)."', '".addslashes(trim(Tools::getValue('email')))."', '".addslashes(trim(Tools::getValue('email')))."', '".addslashes(Tools::getValue('phone'))."', '', $numcustomer, '".addslashes(Tools::getValue('tax_code'))."', '".addslashes(Tools::getValue('vat_number'))."')");
							
								$id_persona = Db::getInstance()->getValue('SELECT id_persona FROM persone WHERE id_customer = '.$numcustomer.'');
								Db::getInstance()->executeS('UPDATE customer SET email = "'.Tools::getValue('email').'", referente = '.$id_persona.' WHERE id_customer = '.$numcustomer.'');
							}
							else
							{
								Db::getInstance()->executeS('UPDATE customer SET email = "'.Tools::getValue('email').'" WHERE id_customer = '.$numcustomer.'');
							}
						}
					
						
					}
					$params = array('{msg}' => "Gentile cliente, <br /><br />
						Grazie per la tua richiesta.
						<br /><br />
						Puoi visualizzare lo stato della richiesta nel tuo account.<br /><br />
						Per accedere al tuo account, puoi utilizzare come user-name la tua email <strong>".Tools::getValue('email')."</strong>. Se non ricordi la password, puoi <a href='/password-recupero' target='_blank'>recuperarla sul nostro sito web</a>.
						Potrai poi cambiare la password in autonomia in qualunque momento.<br /><br />
						Se hai bisogno di supporto contatta il nostro staff. Grazie per la tua attenzione,<br /><br />
						<strong>Lo staff di Ezdirect</strong>");
						
						Mail::Send(5, 'msg_base', Mail::l('Richiesta inviata con successo', 5), 
							$params, Tools::getValue('email'), NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
							_PS_MAIL_DIR_, true);
				}
				
				$id_thread = Tools::getValue('id_thread');
				
				$categoria = Tools::getValue('category');

				
				if(is_numeric($categoria)) {
				
					$intcat = "Prodotto di interesse";
					$categoria =  Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_product = '$categoria' AND id_lang = 5");
					$ttcat = "<tr>
						<td style='width:150px'>".$intcat."</td><td><strong>".$categoria."</strong></td>
						</tr>";
					$inta = "<strong>Cliente interessato a</strong>: ".$categoria."<br />";
				}
				else {
					$ttcat = "";
				}
				
				$subject = "*** RICHIESTA TI RICHIAMIAMO NOI DA ".(Tools::getValue('azienda') != ''? Tools::getValue('azienda') : Tools::getValue('firstname')." ".Tools::getValue('lastname'))." - Tipo richiesta: Telefonata *** ";
				
				if(empty($id_thread)) {
			
					$thread = $ultimothread+1;
					
					Customer::Storico($thread, 'P', 0, 18);
					
					Db::getInstance()->ExecuteS("INSERT INTO form_prevendita_thread (id_thread, tipo_richiesta, id_customer, id_employee, subject, tax_code, vat_number, firstname, lastname, company, address1, postcode, state, country, email, phone, token, status, category, date_add, date_upd) VALUES ('$thread', '".addslashes(Tools::getValue('tipo_richiesta'))."', '".$customer."', 1, '".addslashes($subject)."', '".addslashes(Tools::getValue('tax_code'))."', '".addslashes(Tools::getValue('vat_number'))."', '".addslashes(Tools::getValue('firstname'))."', '".addslashes(Tools::getValue('lastname'))."', '".addslashes(Tools::getValue('company'))."', '".addslashes(Tools::getValue('address1'))."', '".addslashes(Tools::getValue('postcode'))."', '".addslashes(Tools::getValue('id_state'))."', '".addslashes(Tools::getValue('id_country'))."', '".addslashes(trim(Tools::getValue('email')))."', '".addslashes(Tools::getValue('phone'))."', '$token', 'open', '".addslashes(Tools::getValue('category'))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");   
					}
				
				else {
					
					$thread = $id_thread;
					
					Customer::Storico($thread, 'P', 0, 8);
					
					Db::getInstance()->ExecuteS("UPDATE form_prevendita_thread SET firstname = '".addslashes(Tools::getValue('firstname'))."', lastname = '".addslashes(Tools::getValue('lastname'))."', tax_code = '".addslashes(Tools::getValue('tax_code'))."', vat_number = '".addslashes(Tools::getValue('vat_number'))."', company = '".addslashes(Tools::getValue('company'))."', address1 = '".addslashes(Tools::getValue('address1'))."', postcode = '".addslashes(Tools::getValue('postcode'))."', state = '".Tools::getValue('id_state')."', country = '".Tools::getValue('id_country')."', email = '".trim(Tools::getValue('email'))."', phone = '".addslashes(Tools::getValue('phone'))."', status = 'open', category = '".addslashes(Tools::getValue('category'))."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_thread = '".$id_thread."'");  
					
					Db::getInstance()->ExecuteS("UPDATE form_prevendita_thread SET status = 'open', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_thread = '".$id_thread."'");  

				}

$ultimo_thread_azioni = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
$thread_azioni = $ultimo_thread_azioni++;
				
				Db::getInstance()->ExecuteS("INSERT INTO form_prevendita_message (id_message, id_thread, id_customer, id_employee, in_carico, file_name, message, date_add, date_upd) VALUES (NULL, '$thread', ".$customer.", 0, 1, '".addslashes($file_name)."', '".addslashes(Tools::getValue('message'))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
				
/*
				Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, action_date, date_add, date_upd) VALUES ('$thread_azioni', 'Telefonata', '".$customer."', '".addslashes($subject)."', 0, 1, 'open', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");   
					

				Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, file_name, action_message, date_add, date_upd) VALUES (NULL, '$thread_azioni', ".$customer.", 0, 1, '', '<strong>*** TI RICHIAMIAMO NOI***</strong><br /><br /><strong>TELEFONO</strong>: ".Tools::getValue('phone')."<br />".$inta."<strong>Messaggio del cliente</strong>: ".addslashes(Tools::getValue('message'))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
	*/
				
				$gruppo = Db::getInstance()->getValue("SELECT group_lang.name FROM group_lang JOIN customer_group ON group_lang.id_group = customer_group.id_group WHERE customer_group.id_customer = '".$customer."' AND group_lang.id_lang = 5");
				
				$provincia = Db::getInstance()->getValue("SELECT iso_code FROM state WHERE id_state = '".Tools::getValue('id_state')."'");
				$nazione = Db::getInstance()->getValue("SELECT name FROM country_lang WHERE id_country = '".Tools::getValue('id_country')."' AND id_lang = 5");
				//INVIO MAIL E CREAZIONE CSV
				$company = Db::getInstance()->getValue("SELECT is_company FROM customer WHERE id_customer = '".$customer."'");
				
				$categoria = Tools::getValue('category');
				
				if(is_numeric($categoria)) {
				
					$intcat = "Prodotto di interesse";
					$categoria =  Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_product = '$categoria' AND id_lang = 5");
		
				
				}
				
				else {
				
					$intcat = "Categoria di interesse";
					$categoria = Tools::getValue('category');
				
				
				}
				
				
				$dati = $ttcat."
						<tr>
						<td style='width:150px'>Tipo richiesta</td><td><strong>TI RICHIAMIAMO NOI</strong></td>
						</tr>
						<tr>
						<td style='width:150px'>Recapito telefonico</td><td>".Tools::getValue('phone')."</td>
						</tr>
						<tr>
						<td style='width:150px'>Tipo cliente</td><td>".($company == '1' ? 'Azienda' : 'Privato')."</td>
						</tr>
						<tr>
						<td style='width:150px'>Gruppo del cliente</td><td>".$gruppo."</td>
						</tr>
						<tr>
						<td style='width:150px'>Azienda</td><td>".(Tools::getValue('company') == '' ? '<em>Dato non presente</em>' : Tools::getValue('company'))."</td>
						</tr>
						<tr>
						<td style='width:150px'>Nome</td><td>".(Tools::getValue('firstname') == '' ? '<em>Dato non presente</em>' : Tools::getValue('firstname'))."</td>
						</tr>
						<tr>
						<td style='width:150px'>Cognome</td><td>".(Tools::getValue('lastname') == '' ? '<em>Dato non presente</em>' : Tools::getValue('lastname'))."</td>
						</tr>
						<tr>
						<td style='width:150px'>Codice fiscale</td><td>".(Tools::getValue('tax_code') == '' ? '<em>Dato non presente</em>' : Tools::getValue('tax_code'))."</td>
						</tr>
						<tr>
						<td style='width:150px'>Partita IVA</td><td>".(Tools::getValue('vat_number') == '' ? '<em>Dato non presente</em>' : Tools::getValue('vat_number'))."</td>
						</tr>
						<tr>
						<td style='width:150px'>Indirizzo</td><td>".(Tools::getValue('address1') == '' ? '<em>Dato non presente</em>' : Tools::getValue('address1'))."</td>
						</tr>
						<tr>
						<td style='width:150px'>CAP</td><td>".(Tools::getValue('postcode') == '' ? '<em>Dato non presente</em>' : Tools::getValue('postcode'))."</td>
						</tr>
						<tr>
						<td style='width:150px'>Citt&agrave;</td><td>".(Tools::getValue('city') == '' ? '<em>Dato non presente</em>' : Tools::getValue('city'))."</td>
						</tr>
						<tr>
						<td style='width:150px'>Provincia</td><td>".($provincia == '' ? '<em>Dato non presente</em>' : $provincia)."</td>
						</tr>
						<tr>
						<td style='width:150px'>Nazione</td><td>".($nazione == '' ? '<em>Dato non presente</em>' : $nazione)."</td>
						</tr>
						";
				
						$messaggioclientepercsv = Tools::getValue('message');
						$messaggioclientepercsv = strip_tags($messaggioclientepercsv);
						$messaggioclientepercsv = str_replace(";", ",", $messaggioclientepercsv);
					
						$messaggioclientepercsv = str_replace(chr(10), " ", $messaggioclientepercsv); //remove carriage returns
						$messaggioclientepercsv = str_replace(chr(13), " ", $messaggioclientepercsv); //remove carriage returns	
						$email = Tools::getValue('email');
						$phone = Tools::getValue('phone');
				
				
						$ref = rand(0, 99999); $ref2 = rand(0, 99999);
					
						$data_quotazione = date("d/m/Y H:i:s");

						
						
						$idmessaggio = Db::getInstance()->getValue('SELECT id_thread FROM form_prevendita_thread ORDER BY id_thread DESC');
						
						
						if(Tools::getValue('tipo_richiesta') == 'preventivo') {
							$sigla = 'P';
						}
						else if(Tools::getValue('tipo_richiesta') == 'tirichiamiamonoi') {
							$sigla = 'R';
						}
						
						$anno = date('Y');
						$anno = substr($anno, 2,2);
						
						$idrichiesta = $sigla.$anno.$thread;
						$numerounivoco = $sigla.$anno.$thread.$idmessaggio;
						
						$smarty->assign(array(
							'idrichiesta' => $idrichiesta,
							'categoria_di_interesse' => $categoria
						));
						
						$primoticket = Db::getInstance()->getValue("SELECT id_message FROM form_prevendita_message WHERE id_thread = $thread ORDER BY id_message ASC");
						
						if(!$primoticket) {
							
							$primoticket = $idmessaggio;
						
						}
						
						else {
						
						
						
						}
						
						$primoticket = $sigla.$anno.$thread.$primoticket;
						$file_csv=fopen("../../cms/quotazioni/tirichiamiamonoi.csv","a+");
						$riga_richiesta = $data_quotazione.";".date('Ymd')."-".$ref."-".$ref2.";".Tools::getValue('company').";;".Tools::getValue('firstname').";".Tools::getValue('lastname').";".Tools::getValue('address1').";".Tools::getValue('postcode').";".Tools::getValue('city').";".$provincia.";".$phone.";;".$email.";;;".Tools::getValue('vat_number').";".Tools::getValue('tax_code').";Messaggio del cliente: ".$messaggioclientepercsv.";;ACCETTATA;".$idmessaggio.";".$thread.";".$numerounivoco.";".$primoticket.";prevendita;D;".$idrichiesta."\n";
						@fwrite($file_csv,$riga_richiesta);
						
						@fclose("../../cms/quotazioni/tirichiamiamonoi.csv");	
						
				
						$tokenezio = Tools::getAdminToken("AdminCustomers"."2"."1");
						$linkezio = "/ezadmin/index.php?tab=AdminCustomers&id_customer=".$customer."&viewcustomer&viewpreventivo&id_thread=".$thread."&token=".$tokenezio.'&tab-container-1=7';
			
						$tokenfederico = Tools::getAdminToken("AdminCustomers"."2"."6");
						$linkfederico = "/ezadmin/index.php?tab=AdminCustomers&id_customer=".$customer."&viewcustomer&viewpreventivo&id_thread=".$thread."&token=".$tokenfederico.'&tab-container-1=7';		
						
						$tokenbarbara = Tools::getAdminToken("AdminCustomers"."2"."2");
						$linkbarbara = "/ezadmin/index.php?tab=AdminCustomers&id_customer=".$customer."&viewcustomer&viewpreventivo&id_thread=".$thread."&token=".$tokenbarbara.'&tab-container-1=7';
						
						$tokenmatteo = Tools::getAdminToken("AdminCustomers"."2"."7");
						$linkmatteo = "/ezadmin/index.php?tab=AdminCustomers&id_customer=".$customer."&viewcustomer&viewpreventivo&id_thread=".$thread."&token=".$tokenmatteo.'&tab-container-1=7';
						
						$tokenmassimo = Tools::getAdminToken("AdminCustomers"."2"."4");
						$linkmassimo = "/ezadmin/index.php?tab=AdminCustomers&id_customer=".$customer."&viewcustomer&viewpreventivo&id_thread=".$thread."&token=".$tokenmassimo.'&tab-container-1=7';
						
						$tokericcardo = Tools::getAdminToken("AdminCustomers"."2"."3");
						$linkriccardo = "/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewpreventivo&id_thread=".$ct->id."&token=".$tokenriccardo.'&tab-container-1=7';
							
						$tokenpaolo = Tools::getAdminToken("AdminCustomers"."2"."5");
						$linkpaolo = "/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewpreventivo&id_thread=".$ct->id."&token=".$tokenpaolo.'&tab-container-1=7';
						
						$tokenlorenzo = Tools::getAdminToken("AdminCustomers"."2"."12");
						$linklorenzo = "/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewpreventivo&id_thread=".$ct->id."&token=".$tokenpaolo.'&tab-container-1=7';
						
						$adminlink = "
						<tr><td colspan='2'>Ezio - <a href='".$linkezio."'>Clicca qui per collegarti al CRM</a></td></tr>
						
						";
						
						Mail::Send((int)$cookie->id_lang, 'contact', "Ti richiamiamo noi", array('{email}' => $email, '{dati}' => $dati, '{id_richiesta}' => "", '{link}' => $adminlink, '{tipo_contatto}' => "Ti richiamiamo noi", '{message}' => stripslashes(Tools::getValue('message'))), "ezio.delgiudice@ezdirect.it", Tools::getValue('firstname')." ".Tools::getValue('lastname'), 'preventivi@ezdirect.it', 'Ezdirect', $attachments);
						
						
						Mail::Send((int)$cookie->id_lang, 'contact_form', Mail::l('Your message has been correctly sent', (int)$cookie->id_lang), array( '{id_richiesta}' => "", '{message}' => stripslashes(Tools::getValue('message'))), $email);
						
				//FINE INVIO MAIL E CREAZIONE CSV
				
				Tools::redirect('modules/formprevendita/tirichiamiamonoi-grazie.php');
				
			}
		
		if (count($errors) >= 1)
					array_unique($errors);
					
		$smarty->assign(array(
			'errors' => $errors
		));
			
		}	
			


	

if (_THEME_NAME_ == 'prestashop_mobile') { 

	$smarty->display(dirname(__FILE__).'/tirichiamiamonoi-mobile.tpl');
}

	
else if (_THEME_NAME_ == 'ez20') { 

	$smarty->display(dirname(__FILE__).'/tirichiamiamonoi-ez20.tpl');
}

else {

	$smarty->display(dirname(__FILE__).'/tirichiamiamonoi.tpl');

}


include(dirname(__FILE__).'/../../footer.php');
