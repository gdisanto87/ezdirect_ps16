{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}



{capture name=path}{l s='Contact' mod='formprevendita'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}
<script type='text/javascript'>
$(document).ready(function(){
	$('select#id_country').change(function(){
		updateState();
		updateNeedIDNumber();
		updateZipCode();
	});
	
	$('select#id_country2').change(function(){
		updateState2();
		updateNeedIDNumber();
		updateZipCode();
	});
	
	updateState2();
	updateState();
	
	updateNeedIDNumber();
	updateZipCode();
	
	if ($('select#id_country_invoice').length != 0)
	{
		$('select#id_country_invoice').change(function(){
			updateState('invoice');
			updateNeedIDNumber('invoice');
			updateZipCode();
		});
		if ($('select#id_country_invoice:visible').length != 0)
		{
			updateState('invoice');
			updateNeedIDNumber('invoice');
			updateZipCode('invoice');
		}
	}
});

function updateState(suffix)
{
	$('select#id_state'+(suffix !== undefined ? '_'+suffix : '')+' option:not(:first-child)').remove();
	var states = countries[$('select#id_country'+(suffix !== undefined ? '_'+suffix : '')).val()];
	if(typeof(states) != 'undefined')
	{
		$(states).each(function (key, item){
			$('select#id_state'+(suffix !== undefined ? '_'+suffix : '')).append('<option value="'+item.id+'"'+ (idSelectedCountry == item.id ? ' selected="selected"' : '') + '>'+item.name+'</option>');
		});
		
		$('p.id_state'+(suffix !== undefined ? '_'+suffix : '')+':hidden').slideDown('slow');
	}
	else
		$('p.id_state'+(suffix !== undefined ? '_'+suffix : '')).slideUp('fast');
		
}	
	
function updateState2(suffix)
{
	$('select#id_state2'+(suffix !== undefined ? '_'+suffix : '')+' option:not(:first-child)').remove();
	var states = countries[$('select#id_country2'+(suffix !== undefined ? '_'+suffix : '')).val()];
	if(typeof(states) != 'undefined')
	{
		$(states).each(function (key, item){
			$('select#id_state2'+(suffix !== undefined ? '_'+suffix : '')).append('<option value="'+item.id+'"'+ (idSelectedCountry == item.id ? ' selected="selected"' : '') + '>'+item.name+'</option>');
		});
		
		$('p.id_state'+(suffix !== undefined ? '_'+suffix : '')+':hidden').slideDown('slow');
	}
	else
		$('p.id_state'+(suffix !== undefined ? '_'+suffix : '')).slideUp('fast');
		
}


function updateNeedIDNumber(suffix)
{
	var idCountry = parseInt($('select#id_country'+(suffix !== undefined ? '_'+suffix : '')).val());

	if ($.inArray(idCountry, countriesNeedIDNumber) >= 0)
		$('.dni'+(suffix !== undefined ? '_'+suffix : '')).slideDown('slow');
	else
		$('.dni'+(suffix !== undefined ? '_'+suffix : '')).slideUp('fast');
}

function updateZipCode(suffix)
{
	var idCountry = parseInt($('select#id_country'+(suffix !== undefined ? '_'+suffix : '')).val());
	
	var idCountry = parseInt($('select#id_country2'+(suffix !== undefined ? '_'+suffix : '')).val());
	
	if (countriesNeedZipCode[idCountry] != 0)
		$('.postcode'+(suffix !== undefined ? '_'+suffix : '')).slideDown('slow');
	else
		$('.postcode'+(suffix !== undefined ? '_'+suffix : '')).slideUp('fast');
}
</script>

{if !isset($smarty.get.step)}

	{if isset($smarty.get.category)}
	{assign var='category' value=$smarty.get.category}
	{else if isset($smarty.post.category)}
	{assign var='category' value=$smarty.post.category}
	{else}
	{assign var='category' value=''}
	{/if}
	
		{include file="$tpl_dir./errors.tpl"}
			<style type="text/css">
	#account_list li {
	margin-top:10px;
	font-size:16px;
	margin-bottom:15px;
	width:77%;
}
		</style>
		<div id="preventivo_h" style="height: 584px;
margin-top: -29px;
background-image: url('/themes/ez20/img/preventivo-bg.jpg');
padding-top: 20px;
padding-left: 10px;background-position: top right;
background-repeat: no-repeat;">
		<br />
		<h1 style="font-size:24px; text-align:left">Richiedi subito <br />il tuo preventivo gratuito</h1>
		
			<br /><br />
		<div>
		<strong style="font-size:16px">Perché scegliere noi</strong>

		<ul id="account_list" style="margin-left:10px; list-style-type:none">
		<li>✔ Oltre 30 anni di esperienza</li>
		<li>✔ Try & Buy su molti prodotti</li>
		<li>✔ 10.000 prodotti a catalogo</li>
		<li>✔ 60.000 clienti soddisfatti</li>
		<li>✔ Garanzia soddisfatto al 100% su tutti i preventivi</li>
		<li>✔ Pre-Configurazione prodotti</li>
		<li>✔ Supporto da remoto</li>
		<li>✔ Installazione On Site in tutta Italia</li>
		<li>✔ Assistenza postvendita</li>
		<li>✔ Pagamenti differiti  - Noleggio</li>
		</ul>
		</div>	
</div>		
			

		
			<div id="div-preventivo">
				<div id="form-preventivo">
				<table style="border: 0px none; width: 100%; display: block; margin-left: auto; margin-right: auto;">
<tbody>
<tr>
<td style="vertical-align:top; border: 0px; width: 48%; padding-right: 2%; padding-bottom: 3%;">
<h2 style="color: #009; text-align: left; font-size: 20px;">Sei gi&agrave; registrato?</h2>
<h3 style="text-align: left; margin-top: 3px; color: #009;">Accedi con il tuo account</h3>
<div style="width: 100%; padding: 5px; background-color: #ebebeb;">
<div><span style="font-size: 14px; font-family: arial,helvetica,sans-serif;">&nbsp;</span></div>
<div style='text-align:left'>Se sei cliente gi&agrave; registrato, fai login con le credenziali del tuo account (email e password)</div>
					
					
						<form action="{$link->getPageLink('authentication.php', true)}" method="post" id="login_form" class="std">
						
						<table class='authentication'>
				<tr>
				<td style='text-align:right; padding-right:10px'>{l s='E-mail address'}</td>
					<td><input type="text" id="email" name="email" style="background: #fff;
border: 1px solid #ddd;
height: 30px;
width: 100%;
padding: 3px;" value="{if isset($smarty.post.email)}{$smarty.post.email|escape:'htmlall':'UTF-8'|stripslashes}{/if}" class="account_input" /></td></tr>
				<tr>
				<td style='text-align:right; padding-right:10px'>{l s='Password'}</td>
					<td><input type="password" id="passwd" name="passwd" style="background: #fff;
border: 1px solid #ddd;
height: 30px;
width: 100%;
padding: 3px;" value="{if isset($smarty.post.passwd)}{$smarty.post.passwd|escape:'htmlall':'UTF-8'|stripslashes}{/if}" class="account_input" /></td>
				</tr>
				<tr>
				<td colspan="2" style="text-align:center">
				<br />
					<input type="hidden" class="hidden" name="category" value="{$smarty.get.category}" />
					<input type="hidden" class="hidden" name="back" value="preventivi-quotazioni-miglior-prezzo-cuffie-centralini{if isset($smarty.get.category)}?category={$smarty.get.category}{/if}" />
					<input type="hidden" class="bottone-preventivo" value=""  id="SubmitLogin" name="SubmitLogin" />
					{if isset($back)}<input type="hidden" class="hidden" name="back" value="{$back|escape:'htmlall':'UTF-8'}" />{/if}
					<input type="submit" style="width:90%" class="button_large" value="Richiedi preventivo"  id="SubmitLogin2" name="SubmitLogin" />
				</td></tr>
				</table>
				
					
			</form>
			<div style='text-align:center'><a href="{$link->getPageLink('password.php')}" style="" target="_blank">Hai dimenticato la password? Clicca qui</a></div>
</div>
</td>
<td style="vertical-align:top;  border: 0px; width: 48%; padding-left: 2%;">
<h2 style="color: #009; text-align: left; font-size: 20px;">Non sei registrato?</h2>
<h3 style="text-align: left; margin-top: 3px; color: #009;">Registrati come nostro cliente</h3>
<div style="width: 98%; padding: 5px; background-color: #ebebeb;">
<div><span style="font-size: 12px;"><span style="font-size: 14px; font-family: arial,helvetica,sans-serif;">&nbsp;</span></span></div>
<div style="text-align: left;"><span style="font-size: 12px;"><span style="font-size: 14px; font-family: arial,helvetica,sans-serif;">Se non sei registrato, clicca sul pulsante sotto.</span></span></div>
<div><span style="font-size: 16px;">&nbsp;</span></div>
<form action="{$link->getPageLink('modules/formprevendita/form.php', true)}?step=2" method="post" class="std">
<input type="hidden" class="hidden" name="category" value="{$smarty.get.category}" />
					<input type="hidden" class="bottone-preventivo" id="SubmitPreventivo" value="" />
<div><input type="submit" class="button_large" style="width: 230px; display: block; margin: 0 auto; height: 30px; padding-top: 5px; background-color: #000099;" value="Richiedi preventivo"  id="SubmitPreventivo2" name="SubmitLogin" /><br /></div>

					</form>
</div>
</td>
</tr>

</tbody>
</table>

</div>
				
				
<h2>Garanzia soddisfatto 100%</h2><br />
<div>
Per tutelare il nostro cliente, che ripone in noi la sua fiducia e nutre aspettative di servizi di alto livello, è valida la garanzia "preventivo soddisfatto o rimborsato al 100%". È possibile sostituire il prodotto fornito con altro di pari o superiore valore o procedere alla restituzione e richiedere il rimborso, qualora la configurazione proposta non sia in linea con le esigenze espresse dal cliente. Garanzia accessoria, riservata ad imprese, professionisti, possessori di P.IVA e P.A..
</div><br /><br />
<h2>
Oltre 30 anni di esperienza<br />
</h2>
<div>
Non lasciamo nulla al caso. Analizziamo le tue esigenze, proponiamo la soluzione migliore, al miglior prezzo, riducendo a zero il margine di errore. Questo per noi significa salvaguardare il tuo investimento, migliorare la produttività della tua azienda, meritare la tua fiducia.
	</div>	
		
		
{else if (isset($smarty.get.step) && $smarty.get.step == 2)}

	
<script type="text/javascript">
// <![CDATA[
idSelectedCountry = {if isset($smarty.post.id_state)}{$smarty.post.id_state|intval}{else}10{/if};
countries = new Array();
countriesNeedIDNumber = new Array();
countriesNeedZipCode = new Array();
{if isset($countries)}
	{foreach from=$countries item='country'}
		{if isset($country.states) && $country.contains_states}
			countries[{$country.id_country|intval}] = new Array();
			{foreach from=$country.states item='state' name='states'}
				countries[{$country.id_country|intval}].push({ldelim}'id' : '{$state.id_state}', 'name' : '{$state.name|escape:'htmlall':'UTF-8'}'{rdelim});
			{/foreach}
		{/if}
		{if $country.need_identification_number}
			countriesNeedIDNumber.push({$country.id_country|intval});
		{/if}
		{if isset($country.need_zip_code)}
			countriesNeedZipCode[{$country.id_country|intval}] = {$country.need_zip_code};
		{/if}
	{/foreach}
{/if}
$(function(){ldelim}
	$('.id_state option[value={if isset($smarty.post.id_state)}{$smarty.post.id_state}{else}{if isset($address)}{$address->id_state|escape:'htmlall':'UTF-8'}{else}{$provincia}{/if}{/if}]').attr('selected', 'selected');
{rdelim});
//]]>
{if $vat_management}
	{literal}
	$(document).ready(function() {
		$('#company').blur(function(){
			vat_number();
		});
		vat_number();
		function vat_number()
		{
			if ($('#company').val() != '')
				$('#vat_number').show();
			else
				$('#vat_number').hide();
		}
	});
	{/literal}
{/if}
</script>
	{include file="$tpl_dir./errors.tpl"}
	{assign var='stateExist' value=false}
	
		{if !$inviato}
				<div style="width:60%; display:block; margin:0 auto">
					<form method="post" class="std" enctype="multipart/form-data">
					<fieldset>
					<div class="personal-information-2">
						<div class="select">
							<label for="id_contact"><strong>{l s='Subject Heading' mod='formprevendita'}</strong></label>
						
							&nbsp;&nbsp;&nbsp;{l s='Customer service' mod='formprevendita'}
							
						</div>
<div class="select">
							<label for="required"><strong>&nbsp;</strong></label>
					&nbsp;&nbsp;&nbsp;
I campi contrassegnati con l'asterisco (*) sono obbligatori
							
						</div>
					
						<input type="hidden" id="id_thread" name="id_thread" value="{$id_thread}" />
						<input type="hidden" id="tipo_richiesta" name="tipo_richiesta" value="preventivo" />
						<div class="text">
							<label for="firstname"><strong>{l s='Firstname' mod='formprevendita'}*</strong></label>
								<input type="text" id="firstname" style="width:400px" name="firstname" value="{if !empty($smarty.post.firstname)}{$smarty.post.firstname|escape:'htmlall':'UTF-8'|stripslashes}{else}{$firstname}{/if}" />
							
						</div>
						<div class="text">
							<label for="lastname"><strong>{l s='Lastname' mod='formprevendita'}*</strong></label>
								<input type="text" id="lastname" style="width:400px" name="lastname" value="{if !empty($smarty.post.lastname)}{$smarty.post.lastname|escape:'htmlall':'UTF-8'|stripslashes}{else}{$lastname}{/if}" />
							
						</div>
							<div class="text">
							<label for="company"><strong>{l s='Company' mod='formprevendita'}*</strong></label>
								<input type="text" id="company" style="width:400px" name="company" value="{if !empty($smarty.post.company)}{$smarty.post.company|escape:'htmlall':'UTF-8'|stripslashes}{else}{$company}{/if}" />
							
						</div>
						
						<div class="text">
							<label for="vat_number"><strong>{l s='VAT Number' mod='formprevendita'}</strong></label>
								<input type="text" id="vat_number" style="width:400px" name="vat_number" value="{if !empty($smarty.post.vat_number)}{$smarty.post.vat_number|escape:'htmlall':'UTF-8'|stripslashes}{else}{$vat_number}{/if}" />
							
						</div>
						<div class="text">
							<label for="tax_code"><strong>{l s='Tax code' mod='formprevendita'}</strong></label>
								<input type="text" id="tax_code" style="width:400px" name="tax_code" value="{if !empty($smarty.post.tax_code)}{$smarty.post.tax_code|escape:'htmlall':'UTF-8'|stripslashes}{else}{$tax_code}{/if}" />
							
						</div>
						<div class="text">
							<label for="phone"><strong>{l s='Phone' mod='formprevendita'}*</strong></label>
								<input type="text" id="phone" style="width:400px" name="phone" value="{if !empty($smarty.post.phone)}{$smarty.post.phone|escape:'htmlall':'UTF-8'|stripslashes}{else}{$phone}{/if}" />
							
						</div>
						
						<div class="text">
							<label for="email"><strong>{l s='E-mail address' mod='formprevendita'}*</strong></label>
								<input type="text" id="email" style="width:400px" name="email" value="{if !empty($smarty.post.email)}{$smarty.post.email|escape:'htmlall':'UTF-8'|stripslashes}{else}{$email}{/if}" />
							
						</div>
							<div class="text" style="display:none">
							<label for="address1"><strong>{l s='Address' mod='formprevendita'}*</strong></label>
								<input type="text" id="address1" style="width:400px" name="address1" value="{if !empty($smarty.post.address1)}{$smarty.post.address1|escape:'htmlall':'UTF-8'|stripslashes}{else}{$address1}{/if}" />
							
						</div>
							<div class="text" style="display:none">
							<label for="postcode"><strong>{l s='Postcode' mod='formprevendita'}*</strong></label>
								<input type="text" id="postcode" style="width:400px" name="postcode" value="{if !empty($smarty.post.postcode)}{$smarty.post.postcode|escape:'htmlall':'UTF-8'|stripslashes}{else}{$postcode}{/if}" />
							
						</div>
							<div class="text" style="display:none">
							<label for="city"><strong>Citt&agrave;*</strong></label>
								<input type="text" id="city" style="width:400px" name="city" value="{if !empty($smarty.post.city)}{$smarty.post.city|escape:'htmlall':'UTF-8'|stripslashes}{else}{$city}{/if}" />
							
						</div>
						<div class="required id_state select" style="display:none">
									<label for="id_state"><strong>{l s='State' mod='formprevendita'}*</strong></label>
									<select name="id_state" id="id_state">
										<option value="">-</option>
									</select>
									<sup>*</sup>
								</div>
								
						<div class="required select" style="display:none">
									<label for="id_country"><strong>{l s='Country' mod='formprevendita'}*</strong></label>
									<select name="id_country" id="id_country">
										<option value="">-</option>
										{foreach from=$countries item=v}
										<option value="{$v.id_country}" {if !isset($smarty.post.id_country)}{if 10 == $v.id_country}{/if}{/if}{if ($paese == $v.id_country)} selected="selected"{else}{if ($smarty.post.id_country == $v.id_country)}selected="selected"{else}{/if}{/if}>{$v.name|escape:'htmlall':'UTF-8'}</option>
										{/foreach}
									</select>
									<sup>*</sup>
								</div>
								{assign var='stateExist' value=true}

								
							
						
						<div class="select">
									{if isset($smarty.get.category)}
									{assign var='categoryric' value=$smarty.get.category}
									{else if isset($smarty.post.category) && is_numeric($smarty.post.category)}
									{assign var='categoryric' value=$smarty.post.category}
									{else}
									{assign var='categoryric' value=''}
									{/if}
		
									{if $categoryric != ''} 
									<label for="category"><strong>{l s='Requested product' mod='formprevendita'}*</strong></label>
									<input type="hidden" name="category" value="{$categoryric}" />
									&nbsp;&nbsp;&nbsp;
									{$prodottorichiesto}
									{else}
									<label for="category"><strong>{l s='Requested category' mod='formprevendita'}*</strong></label>
									<select name="category" id="category" autocomplete="off">
										<option value="cuffie" name="cuffie" {if $category == 'cuffie'}selected="selected"{else}{/if}>Cuffie</option>
										<option value="centralini" name="centralini" {if $category == 'centralini'}selected="selected"{else}{/if}>Centralini</option>
										<option value="fissi" name="fissi" {if $category == 'fissi'}selected="selected"{else}{/if}>Telefoni fissi</option>
										<option value="cordless" name="cordless" {if $category == 'cordless'}selected="selected"{else}{/if}>Telefoni cordless</option>
										<option value="audioconferenze" name="audioconferenze" {if ($category == "audioconferenze")}selected="selected"{else}{/if}>Audioconferenze</option>
										<option value="audioguide" name="audioguide" {if $category == 'audioguide'}selected="selected"{else}{/if}>Audioguide</option>
										<option value="ricetrasmittenti" name="ricetrasmittenti" {if $category == 'ricetrasmittenti'}selected="selected"{else}{/if}>Ricetrasmittenti</option>
										<option value="registratori" name="registratori" {if $category == 'registratori'}selected="selected"{else}{/if}>Registratori</option>
										<option value="gsm" name="gsm" {if $category == 'gsm'}selected="selected"{else}{/if}>GSM-UMTS</option>
										<option value="gateway" name="gateway" {if $category == 'gateway'}selected="selected"{else}{/if}>VoIP Gateway</option>
										<option value="hotspot" name="hotspot" {if $category == 'hotspot'}selected="selected"{else}{/if}>Networking Hot-spot Wi-fi</option>
										<option value="ipcamera" name="ipcamera" {if $category == 'ipcamera'}selected="selected"{else}{/if}>IP Camera</option>
										<option value="centralino-virtuale" name="centralino-virtuale" {if $category == 'centralino-virtuale'}selected="selected"{else}{/if}>Centralino virtuale</option>
										<option value="altro" name="altro" {if $category == 'altro'}selected="selected"{else}{/if}>Altro</option>
									</select>
									<sup>*</sup>
									{/if}
								</div>
								
							<div class="text">
						<label for="fileUpload"><strong>{l s='Attach File' mod='formprevendita'}</strong><br />
						{l s='Use this field if you want to send us files' mod='formprevendita'}
						</label>
							<input type="hidden" name="MAX_FILE_SIZE" value="200000000" />
							<input style="width:400px" type="file" name="fileUpload[]" id="fileUpload1" /><br />
							<input style="width:400px; margin-top:2px" type="file" name="fileUpload[]" id="fileUpload2" />
						</div>
					
					<div class="textarea">
						<label for="message"><strong>{l s='Indicaci le tue esigenze' mod='formprevendita'}*</strong></label>
						 <textarea id="message" name="message" rows="15" cols="20" style="width:400px;height:220px">{if !empty($smarty.post.message)}{$smarty.post.message|escape:'htmlall':'UTF-8'|stripslashes}{else}{$message|escape:'htmlall':'UTF-8'|stripslashes}{/if}</textarea>
					</div>
					</div>
					<style>
.g-recaptcha{
   margin: 0px auto !important;
   width: auto !important;
   height: auto !important;
   text-align: -webkit-center;
   text-align: -moz-center;
   text-align: -o-center;
   text-align: -ms-center;
   margin-left: -33px !important;
}

.grecaptcha-badge { 
    visibility: hidden;
}

.rc-anchor-normal .rc-anchor-pt {
visibility:hidden !important;
	margin: 2px 11px 0 0;
padding-right: 2px;
position: relative !important;
text-align: right !important;
width: 86px !important;
	}
</style>

					<script src="https://www.google.com/recaptcha/api.js" async defer></script>
					
<div class="text-xs-center" >
					 
					 
					<div style="clear:both"></div>								
					<div style="display:block; width:90%">La quotazione personalizzata sar&agrave; disponibile entro un'ora circa dalla presa in carico della tua richiesta.
Per visualizzare, stampare e modificare il preventivo, dovrai accedere al tuo account.
Per ricevere supporto puoi contattare il nostro servizio clienti al <br /><span style="color:green"><strong>numero verde 800 529767</strong></span>
					</div>
					<div style="text-align:center; display:block; width:90%">
						Ho letto e compreso la Vostra <a href='https://www.ezdirect.it/guide/6-informativa-sulla-privacy' target='_blank'>Privacy Policy</a> e:
						<br /><br />
							
							<input type="radio" name="consenso_1" id="consenso11" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
							<input type="radio" name="consenso_1" id="consenso10" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
							
							<br /><br />
							al trattamento dei dati personali per l’esecuzione dei servizi forniti tramite il Sito da parte di Ezdirect S.r.l., o ad una o più obbligazioni contrattualmente convenute, ai sensi dell’art. 1, comma 1, lettera a), b) e c) della citata informativa (consenso obbligatorio; la mancata prestazione comporterà l’impossibilità di fruire dei servizi offerti, di effettuare l’iscrizione, e di effettuare acquisti tramite il Sito).
							<br /><br />
							
							<input type="radio" name="consenso_2"  id="consenso21" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
							<input type="radio" name="consenso_2"  id="consenso20" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
							
							<br /><br />
							al trattamento dei dati personali per la realizzazione, da parte di Ezdirect S.r.l., di indagini dirette a verificare il grado di soddisfazione sui servizi offerti, ai sensi dell’art. 1, comma 2, lettera a) della citata informativa (consenso facoltativo).

							<br /><br />
							
							<input type="radio" name="consenso_3"  id="consenso31" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
							<input type="radio" name="consenso_3"  id="consenso30" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
							
							<br /><br />
							al trattamento dei dati personali a fini di marketing e/o comunicazione commerciale da parte di Ezdirect S.r.l., connesse alle attività svolte da parte della stessa e/o da parte di soggetti terzi, ai sensi dell’art. 1, comma 2, lettera b), della citata informativa (consenso facoltativo).
							<br /><br />
							

						</div>
				
					</div>
					 <div class="g-recaptcha" style="border:0px" data-sitekey="6LciANsZAAAAALhFLBBxrvJJICdKqsPVxXHfPh2i"></div>

					<div style='text-align:center; width:93%'>
						<br />
						<input type="submit" name="submitMessage" id="submitMessage" value="{l s='Send' mod='formprevendita'}" class="button_large" onclick="$(this).hide(); fbq('track', 'Contact');" />
					</div>
					</fieldset>
				</div>
				</form>	
			</div>
	{else}

		<div>{l s='Your message has been successfully sent to our team.' mod='formprevendita'}</div>
		<ul class="footer_links">
			<li><a href="{$base_dir}"><img class="icon" alt="" width="22" height="22" src="{$img_dir}icon/home.gif"/></a><a href="{$base_dir}">{l s='Home' mod='formprevendita'}</a></li>
		</ul>
				 <!-- Google Code for Richiesta di preventivo Conversion Page -->
				 {if $smarty.post.id_thread == ""}
					<script type="text/javascript">
					/* <![CDATA[ */
					var google_conversion_id = 1058514372;
					var google_conversion_language = "en";
					var google_conversion_format = "3";
					var google_conversion_color = "ffffff";
					var google_conversion_label = "8C5eCMypmAQQxMve-AM";
					var google_conversion_value = 0;
					/* ]]> */
					</script>
					<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
					</script>
					<noscript>
					<div style="display:inline;">
					<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1058514372/?value=0&amp;label=8C5eCMypmAQQxMve-AM&amp;guid=ON&amp;script=0"/>
					</div>
					</noscript>
					
					<script type="text/javascript">
					var _gaq = _gaq || [];
					_gaq.push(['_setAccount', 'UA-19658774-1']);
					// Recommanded value by Google doc and has to before the trackPageView
					_gaq.push(['_setSiteSpeedSampleRate', 5]);

					  _gaq.push(['_addTrans',
						'{$idrichiesta}',		
						'Ezdirect',		
						'0.01',		
						'',			
						'',	
						'{$smarty.post.city}',	
					    '',		
						''		
					  ]);

						
							_gaq.push(['_addItem',
							'{$idrichiesta}',	
							'{$categoria}',			
							'{$categoria}',	
							'',		
							'0.01',	
							'1'		
							]);
					
					{literal}
					  _gaq.push(['_trackTrans']);	
					{/literal}
				
					{literal}
					(function() {
						var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
						ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
						var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
					})(); {/literal}
					</script>

				{else}
				{/if}
		
		
		{/if}



{/if}	
	
	
	
	