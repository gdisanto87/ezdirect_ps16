<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

$sql = array();

/**
* Install transaction Table
*/
$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'gestpay_transaction` (
        `id_transaction` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `id_cart` INT(10) UNSIGNED NOT NULL,
        `id_customer` INT(10) UNSIGNED NOT NULL,
        `id_language` INT(10) UNSIGNED NOT NULL,
        `id_shop` INT(10) UNSIGNED NOT NULL,
        `id_currency` INT(10) UNSIGNED NOT NULL,
        `id_country` INT(10) UNSIGNED NOT NULL,
        `id_order` INT(10) DEFAULT NULL,
        `currency` INT(4) NOT NULL,
        `amount` DECIMAL(11,2) NOT NULL,
        `test_mode` INT(2) NOT NULL,
        `shoptransactionid` VARCHAR(50),
        `transactionresult` CHAR(2),
        `authorizationcode` VARCHAR(6),
        `banktransactionid` INT(10),
        `errorcode` INT(10),
        `errordescription` VARCHAR(255),
        `alertcode` INT(10),
        `alertdescription` VARCHAR(255),
        `paymentmethod` VARCHAR(100),
        `payment_date` DATETIME NOT NULL,
        `last_update` DATETIME NOT NULL,
        `encString` TEXT NULL,
        `transKey` TEXT NULL) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;';

/**
* Install token Table
*/
$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'gestpay_token` (
        `id_token` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `id_customer` INT(10) UNSIGNED NOT NULL,
        `token` VARCHAR(16) NOT NULL,
        `token_expiry_month` INT(10) UNSIGNED NOT NULL,
        `token_expiry_year` INT(10) UNSIGNED NOT NULL,
        `save_token` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
