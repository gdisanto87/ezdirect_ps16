{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{literal}
<script>
    var gestpay_method = "{/literal}{$gestpay_method|escape:'htmlall':'UTF-8'}{literal}";
	var gestpay_PARes = "{/literal}{$gestpay_pa_res|escape:'htmlall':'UTF-8'}{literal}";
	var gestpay_payment_message = "{/literal}{l s='Payment in progress...' mod='gestpay'}{literal}";
    var gestpay_browser_error = "{/literal}{l s='Browser not supported' mod='gestpay'}{literal}";
	var gestpay_trans_key = "{/literal}{$gestpay_trans_key|escape:'htmlall':'UTF-8'}{literal}";
	var gestpay_response_url = "{/literal}{$gestpay_response_url|escape:'htmlall':'UTF-8'}{literal}";
	var gestpay_shop_login = "{/literal}{$gestpay_shop_login|escape:'htmlall':'UTF-8'}{literal}";
	var gestpay_ajax_url = "{/literal}{$gestpay_ajax_url|escape:'htmlall':'UTF-8'}{literal}";
	var gestpay_enc_string = "{/literal}{$gestpay_enc_string|escape:'htmlall':'UTF-8'}{literal}";
	var gestpay_payment_url = "{/literal}{$gestpay_payment_url|escape:'htmlall':'UTF-8'}{literal}";
	var gestpay_auth_url = "{/literal}{$gestpay_auth_url|escape:'htmlall':'UTF-8'}{literal}";
	var gestpay_buyer_name = "{/literal}{$gestpay_buyer_name|escape:'htmlall':'UTF-8'}{literal}";
	var gestpay_buyer_email = "{/literal}{$gestpay_buyer_email|escape:'htmlall':'UTF-8'}{literal}";
</script>
{/literal}
<div id="GestpayDialog" style="display:none;">
    <img class="dialog_logo" src="{$gestpay_images_path|escape:'html':'UTF-8'}axerve_300.png" />
    <img class="loader_img" src="{$gestpay_images_path|escape:'html':'UTF-8'}loader.gif"/>
    <span id="GestpayMessage">{l s='Loading...' mod='gestpay'}</span>
</div>
<div id="GestpayContent">    
	<!-- Credit card form -->
	<div class="credit-card">
      <div class="form-header">
        <h4 class="title"><img src="{$gestpay_images_path|escape:'html':'UTF-8'}iframe.png"/> <span>{l s='Pay with card' mod='gestpay'}</span></h4>
      </div>
	  <div class="form-body-left">
	  	{l s='Pay with serenity: this site uses Axerve technology to send credit card data securely via SSL encryption.' mod='gestpay'}
	  </div>
      <div class="form-body-right">
      <!-- ErrorBox -->        
        <div id="GestpayErrorBox" class="notif notif_error">
          <div class="error_content">    
              <h1 class="error_title">Error</h1>
        	  <span class="error_subtitle">Sorry about that. Retry</span>
          </div>  
        </div>
        <div id="GestpayLoadingBox" class="">
           <img class="loader_img_not-form" src="{$gestpay_images_path|escape:'html':'UTF-8'}loader.gif"/>
           <span>{l s='Loading credit card form' mod='gestpay'}</span>
        </div>
        <form name="GestpayIframeForm" action= "#" id="GestpayIframeForm">   
		<label>{l s='Card Number' mod='gestpay'}  
        <input type="text" class="card-number" name="gestpay_cc" id="gestpay_cc" placeholder="{l s='Card Number' mod='gestpay'}">
		</label>   
        <!-- Date Field -->
        <div class="date-field">
          <div class="month">  
		  	<label>{l s='Expire month' mod='gestpay'}            
             <select name="gestpay_expmm" id="gestpay_expmm">
                <option value="">-</option>
                {foreach from=$gestpay_months item=v}
                   <option value="{$v|escape:'htmlall':'UTF-8'}">{$v|escape:'htmlall':'UTF-8'}</option>
                {/foreach}
             </select>
			 </label>
          </div>
          <div class="year">
		  <label>{l s='Expire year' mod='gestpay'}    
            <select name="gestpay_expyy" id="gestpay_expyy">
                <option value="">-</option>
                {foreach from=$gestpay_years key=k item=v}
                   <option value="{$k|escape:'htmlall':'UTF-8'}">{$v|escape:'htmlall':'UTF-8'}</option>
                {/foreach}
            </select>
			</label>
          </div>
        </div>
        <!-- Card Verification Field -->
        <div class="card-verification">
          <div class="cvv-input">
		  	<label>{l s='CVV' mod='gestpay'}    
                <input type="password" name="gestpay_cvv2" id="gestpay_cvv2" placeholder="CVV">
			</label>
          </div>
          <div class="cvv-details">
            <p>{l s='3 or 4 digits usually found' mod='gestpay'}<br>{l s=' on the signature strip' mod='gestpay'}</p>
          </div>
        </div>
        {if $gestpay_use_token}
        <div class="card-verification">
            <p class="checkbox">                
                <input type="checkbox" name="gestpay_save_token" id="gestpay_save_token" {if $gestpay_save_token eq 1}checked{/if}>
                <label for="gestpay_save_token" class="save_token_label">{l s='Would you like to save your credit card?' mod='gestpay'}</label>
            </p>
        </div>
        {/if}
		<button type="submit" id="GestpaySubmit" class="proceed-btn">
		    <span>{l s='Proceed' mod='gestpay'}</span>
		</button>    
		</form>
      </div>
	  <div class="GestpayClearfix"></div>
    </div>
</div>