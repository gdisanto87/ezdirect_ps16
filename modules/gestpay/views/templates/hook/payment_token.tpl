{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $smarty.const._PS_VERSION_ >= 1.7}
<section>
  <p>
    <img src="{$gestpay_images_path|escape:'htmlall':'UTF-8'}axerve_300.png" style="max-width:150px;margin-bottom:15px;" alt="{l s='Pay by Credit Card' d='Modules.GestPay'}" /></br>
    {l s='The payment will be made with your credit card' d='Modules.GestPay'} XXXX-XXXX-XXXX-{$gestpay_token|escape:'htmlall':'UTF-8'}
  </p>
</section>
{elseif $smarty.const._PS_VERSION_ >= 1.6 && $smarty.const._PS_VERSION_ < 1.7}
<p class="payment_module">
	<a class="gestpay" href="{$gestpay_redirect_url|escape:'html'}" title="{l s='Pay now!' mod='gestpay'}">
		{l s='Pay now!' mod='gestpay'}
		<span>{l s='The payment will be made with your credit card' mod='gestpay'} XXXX-XXXX-XXXX-{$gestpay_token|escape:'htmlall':'UTF-8'}</span>
	</a>
	
</p>
{elseif $smarty.const._PS_VERSION_ <= 1.5}
<p class="payment_module">
	<a class="gestpay" href="{$gestpay_redirect_url|escape:'html'}" title="{l s='Pay now!' mod='gestpay'}">
		<img src="{$gestpay_images_path|escape:'htmlall':'UTF-8'}axerve_15.png" alt="{l s='Pay now!' mod='gestpay'}" width="86" height="49"/>
		{l s='Pay now!' mod='gestpay'}
		<span>{l s='The payment will be made with your credit card' mod='gestpay'} XXXX-XXXX-XXXX-{$gestpay_token|escape:'htmlall':'UTF-8'}</span>
	</a>
</p>
{/if}

{if $smarty.const._PS_VERSION_ >= 1.6 && $smarty.const._PS_VERSION_ < 1.7}
	<style type="text/css">
		{literal}
			p.payment_module a.gestpay {
				background: url({/literal}{$gestpay_images_path|escape:'htmlall':'UTF-8'}{literal}gestpay.png) 15px 15px no-repeat #fbfbfb;
			}
			p.payment_module a.gestpay:after {
				display: block;
				content: "\f054";
				position: absolute;
				right: 15px;
				margin-top: -11px;
				top: 50%;
				font-family: "FontAwesome";
				font-size: 25px;
				height: 22px;
				width: 14px;
				color: #777;
			}
			p.payment_module a.gestpay:hover {
				background-color: #f6f6f6;
			}
		{/literal}
	</style>
{/if}