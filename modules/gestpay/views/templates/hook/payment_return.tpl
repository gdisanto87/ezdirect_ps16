{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $smarty.const._PS_VERSION_ >= 1.7}
<div style="margin-left: 30px">
	{if $gestpay_result eq 'ok'}
		<h3>{l s='Thank you!' d='Modules.GestPay'}</h3>
		<p style="font-size: 14px; font-weight: bold">{l s='Your order is complete.' d='Modules.GestPay'}</p>
		<p style="font-size: 14px">{l s='We will process it very soon.' d='Modules.GestPay'}</p>
		<p style="font-size: 14px">{l s='For any questions or for further information, please contact our' d='Modules.GestPay'}</p>
		&raquo; <a style="font-size: 14px; font-weight: bold" href="{$link->getPageLink('contact', true)|escape:'htmlall':'UTF-8'}">{l s='customer support' d='Modules.GestPay'}</a>.
	{else}
		<h3>{l s='Sorry' d='Modules.GestPay'}</h3>
		<p style="font-size: 14px">{l s='We noticed a problem with your order. If you think this is an error, you can contact our' d='Modules.GestPay'}</p>
		&raquo; <a style="font-size: 14px; font-weight: bold" href="{$link->getPageLink('contact', true)|escape:'htmlall':'UTF-8'}">{l s='customer support' d='Modules.GestPay'}</a>.
	{/if}
</div>
{else}
<div style="margin-left: 30px">
	{if $gestpay_result eq 'ok'}
		<h3>{l s='Thank you!' mod='gestpay'}</h3>
		<p style="font-size: 14px; font-weight: bold">{l s='Your order on %s is complete.' sprintf=$shop_name mod='gestpay'}</p>
		<p style="font-size: 14px">{l s='We will process it very soon.' mod='gestpay'}</p>
		<p style="font-size: 14px">{l s='For any questions or for further information, please contact our' mod='gestpay'}</p>
		&raquo; <a style="font-size: 14px; font-weight: bold" href="{$link->getPageLink('contact', true)|escape:'htmlall':'UTF-8'}">{l s='customer support' mod='gestpay'}</a>.
	{else}
		<h3>{l s='Sorry' mod='gestpay'}</h3>
		<p style="font-size: 14px">{l s='We noticed a problem with your order. If you think this is an error, you can contact our' mod='gestpay'}</p>
		&raquo; <a style="font-size: 14px; font-weight: bold" href="{$link->getPageLink('contact', true)|escape:'htmlall':'UTF-8'}">{l s='customer support' mod='gestpay'}</a>.
	{/if}
</div>
{/if}

