{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2017 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}
{if $smarty.const._PS_VERSION_ >= 1.6}
<h3>
	<i class="icon-wrench"></i> {l s='General configuration' mod='gestpay'} <small>{$module_display|escape:'html':'UTF-8'}</small>
</h3>
{if isset($submit_message['config'])}
{$submit_message['config']|escape:'quotes':'UTF-8'}
{/if}
<form role="form"  class="defaultForm form-horizontal" action="{$request_uri|escape:'html':'UTF-8'}" method="post">
    <!-- API Key -->
    {if $api_key_error eq 1}
    <div class="alert alert-danger">{$api_key_error_message|escape:'html':'UTF-8'}</div>
    {/if}
    <!-- Config -->
    {if $config_error eq 1}
    <div class="alert alert-warning">{$config_error_message|escape:'html':'UTF-8'}</div>
    {/if}
        <div class="form-wrapper">
            <div class="form-group">
                <label class="control-label col-lg-3 required">{l s='Shop Login' mod='gestpay'}</label>
                <div class="col-lg-3 ">
                    <div class="input-group">
						<input type="text" name="GESTPAY_SHOP_LOGIN" id="GESTPAY_SHOP_LOGIN" value="{$gestpay_shop_login|escape:'html':'UTF-8'}" class="" required="required" />
					</div>
					<p class="help-block">{l s='Insert the full shop login as appears in the Axerve Backoffice' mod='gestpay'}</p>
				</div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 required">{l s='Platform' mod='gestpay'}</label>
                <div class="col-lg-3 ">
                     <select name="GESTPAY_PLATFORM" class="fixed-width-xxl" id="GESTPAY_PLATFORM">
                        {html_options options=$platforms selected=$platform_selected}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">{l s='Enable test mode' mod='gestpay'}</label>
                <div class="col-lg-9 ">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_TEST_MODE" id="GESTPAY_TEST_MODE_on" value="1" {if $gestpay_test_mode eq 1}checked="checked"{/if}/>
                        <label for="GESTPAY_TEST_MODE_on">{l s='Yes' mod='gestpay'}</label>
                        <input type="radio" name="GESTPAY_TEST_MODE" id="GESTPAY_TEST_MODE_off" value="0" {if $gestpay_test_mode eq 0}checked="checked"{/if}/>
                        <label for="GESTPAY_TEST_MODE_off">{l s='No' mod='gestpay'}</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block">{l s='Enables the gateway test mode. Use only to check if the module is working. Please note that the test mode Shop Login can be different from the production mode.' mod='gestpay'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">{l s='Enable payment page localization' mod='gestpay'}</label>
                <div class="col-lg-9 ">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_LANGUAGE" id="GESTPAY_LANGUAGE_on" value="1" {if $gestpay_language eq 1}checked="checked"{/if}/>
                        <label  for="GESTPAY_LANGUAGE_on">{l s='Yes' mod='gestpay'}</label>
                        <input type="radio" name="GESTPAY_LANGUAGE" id="GESTPAY_LANGUAGE_off" value="0" {if $gestpay_language eq 0}checked="checked"{/if}/>
                        <label  for="GESTPAY_LANGUAGE_off">{l s='No' mod='gestpay'}</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block">{l s='Enables or disables payment page translation according to the language used by the customer (multilanguage store). Please note that you have to enable the language parameter in the configuration page.' mod='gestpay'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">{l s='Enable iframe payment' mod='gestpay'}</label>
                <div class="col-lg-9 ">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_IFRAME" id="GESTPAY_IFRAME_on" value="1" {if $gestpay_iframe eq 1}checked="checked"{/if}/>
                        <label  for="GESTPAY_IFRAME_on">{l s='Yes' mod='gestpay'}</label>
                        <input type="radio" name="GESTPAY_IFRAME" id="GESTPAY_IFRAME_off" value="0" {if $gestpay_iframe eq 0}checked="checked"{/if}/>
                        <label  for="GESTPAY_IFRAME_off">{l s='No' mod='gestpay'}</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block">{l s='Pay directly on the store checkout page, without being redirected to Axerve payment page.' mod='gestpay'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">{l s='Enable tokens' mod='gestpay'}</label>
                <div class="col-lg-9 ">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_TOKEN" id="GESTPAY_TOKEN_on" value="1" {if $gestpay_token eq 1}checked="checked"{/if} {if $gestpay_token_enabled eq 0}disabled="disabled"{/if}/>
                        <label  for="GESTPAY_TOKEN_on">{l s='Yes' mod='gestpay'}</label>
                        <input type="radio" name="GESTPAY_TOKEN" id="GESTPAY_TOKEN_off" value="0" {if $gestpay_token eq 0}checked="checked"{/if} {if $gestpay_token_enabled eq 0}disabled="disabled"{/if}/>
                        <label  for="GESTPAY_TOKEN_off">{l s='No' mod='gestpay'}</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block">{l s='Tokenization means the replacement of the credit card number with a Token. Please note that this setting has to be configured on the Axerve backend as well. Please refer to the manual for further info.' mod='gestpay'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">{l s='Use Sales API' mod='gestpay'}</label>
                <div class="col-lg-9 ">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_S2S" id="GESTPAY_S2S_on" value="1" {if $gestpay_s2s eq 1}checked="checked"{/if}/>
                        <label  for="GESTPAY_S2S_on">{l s='Yes' mod='gestpay'}</label>
                        <input type="radio" name="GESTPAY_S2S" id="GESTPAY_S2S_off" value="0" {if $gestpay_s2s eq 0}checked="checked"{/if}/>
                        <label  for="GESTPAY_S2S_off">{l s='No' mod='gestpay'}</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block">{l s='Use Sales API for Settle and Refund actions. Available for Unlimited version only.' mod='gestpay'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">{l s='Sales API automation' mod='gestpay'}</label>
                <div class="col-lg-9">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_S2S_AUTO" id="GESTPAY_S2S_AUTO_on" value="1" {if $gestpay_s2s_auto eq 1}checked="checked"{/if} {if $gestpay_s2s_auto_enabled eq 0}disabled="disabled"{/if}/>
                        <label for="GESTPAY_S2S_AUTO_on">{l s='Yes' mod='gestpay'}</label>
                        <input type="radio" name="GESTPAY_S2S_AUTO" id="GESTPAY_S2S_AUTO_off" value="0" {if $gestpay_s2s_auto eq 0}checked="checked"{/if} {if $gestpay_s2s_auto_enabled eq 0}disabled="disabled"{/if}/>
                        <label for="GESTPAY_S2S_AUTO_off">{l s='No' mod='gestpay'}</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block">{l s='Automate refund action when order is canceled or credit slip is created.' mod='gestpay'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">{l s='Use TLS endpoints' mod='gestpay'}</label>
                <div class="col-lg-9"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_TLS_ENDPOINT" id="GESTPAY_TLS_ENDPOINT_on" value="1" {if $gestpay_tls_endpoint eq 1}checked="checked"{/if}/>
                        <label for="GESTPAY_TLS_ENDPOINT_on">{l s='Yes' mod='gestpay'}</label>
                        <input type="radio" name="GESTPAY_TLS_ENDPOINT" id="GESTPAY_TLS_ENDPOINT_off" value="0" {if $gestpay_tls_endpoint eq 0}checked="checked"{/if}/>
                        <label for="GESTPAY_TLS_ENDPOINT_off">{l s='No' mod='gestpay'}</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block">{l s='If your server does NOT support TLS set this flag to false.' mod='gestpay'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">{l s='Single button per payment' mod='gestpay'}</label>
                <div class="col-lg-9">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_LIST_METHODS" id="GESTPAY_LIST_METHODS_on" value="1" {if $gestpay_list_methods eq 1}checked="checked"{/if}/>
                        <label for="GESTPAY_LIST_METHODS_on">{l s='Yes' mod='gestpay'}</label>
                        <input type="radio" name="GESTPAY_LIST_METHODS" id="GESTPAY_LIST_METHODS_off" value="0" {if $gestpay_list_methods eq 0}checked="checked"{/if}/>
                        <label for="GESTPAY_LIST_METHODS_off">{l s='No' mod='gestpay'}</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block">{l s='Available from professional and up and with apikey set.' mod='gestpay'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">{l s='API key' mod='gestpay'}</label>
                <div class="col-lg-9">
                    <div class="input-group col-lg-9">
						<input type="text" name="GESTPAY_APIKEY" id="GESTPAY_APIKEY" value="{$gestpay_apikey|escape:'html':'UTF-8'}" class="" />
					</div>
					<p class="help-block">{l s='To enable single button payment methods, this value is mandatory.' mod='gestpay'}</p>
				</div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3"></label>
                <div class="col-lg-3 ">
                    <button type="button" id="test_connection" name="test_connection" class="btn btn-default pull-left">{l s='Test connection' mod='gestpay'}</button>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">{l s='Remova data on uninstall' mod='gestpay'}</label>
                <div class="col-lg-9 ">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_REMOVE_TABLE" id="GESTPAY_REMOVE_TABLE_on" value="1" {if $gestpay_remove_table eq 1}checked="checked"{/if}/>
                        <label  for="GESTPAY_REMOVE_TABLE_on">{l s='Yes' mod='gestpay'}</label>
                        <input type="radio" name="GESTPAY_REMOVE_TABLE" id="GESTPAY_REMOVE_TABLE_off" value="0" {if $gestpay_remove_table eq 0}checked="checked"{/if}/>
                        <label  for="GESTPAY_REMOVE_TABLE_off">{l s='No' mod='gestpay'}</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block">{l s='When enabled and the module is uninstalled, all the module data will be deleted, including transaction logs.' mod='gestpay'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">{l s='Enable module debugging log' mod='gestpay'}</label>
                <div class="col-lg-9 ">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_DEBUG" id="GESTPAY_DEBUG_on" value="1" {if $gestpay_debug eq 1}checked="checked"{/if}/>
                        <label  for="GESTPAY_DEBUG_on">{l s='Yes' mod='gestpay'}</label>
                        <input type="radio" name="GESTPAY_DEBUG" id="GESTPAY_DEBUG_off" value="0" {if $gestpay_debug eq 0}checked="checked"{/if}/>
                        <label  for="GESTPAY_DEBUG_off">{l s='No' mod='gestpay'}</label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block">{l s='Enable debug logs. Activate only on request by technical support.' mod='gestpay'}</p>
                </div>
            </div>
            <div class="form-group">
            <button type="submit" value="1" id="submitConfig" name="submitConfig" class="btn btn-default pull-right">
                <i class="process-icon-save"></i>{l s='Save' mod='gestpay'}
            </button>
            </div>
        </div>
</form>
{else}
<fieldset>
    <legend><img src="{$module_dir|escape:'html':'UTF-8'}views/img/information.png" alt="{l s='General configuration' mod='gestpay'}">{l s='General configuration' mod='gestpay'}</legend>
    {if isset($submit_message['config'])}
    {$submit_message['config']|escape:'quotes':'UTF-8'}
    {/if}
    <form role="form"  class="defaultForm form-horizontal" action="{$request_uri|escape:'html':'UTF-8'}" method="post">
    <!-- API Key -->
    {if $api_key_error eq 1}
    <div class="alert alert-danger">{$api_key_error_message|escape:'html':'UTF-8'}</div>
    {/if}
    <!-- Config -->
    {if $config_error eq 1}
    <div class="alert alert-warning">{$config_error_message|escape:'html':'UTF-8'}</div>
    {/if}
        <div class="form-wrapper">
            <div class="form-group">
                <label class="control-label required">{l s='Shop Login' mod='gestpay'}</label>
                <div class="margin-form">
                    <div class="input-group">
						<input type="text" name="GESTPAY_SHOP_LOGIN" id="GESTPAY_SHOP_LOGIN" value="{$gestpay_shop_login|escape:'html':'UTF-8'}" class="" required="required" />
					</div>
					<p class="preference_description">{l s='Insert the full shop login as appears in the Axerve Backoffice' mod='gestpay'}</p>
				</div>
            </div>
            <div class="form-group">
                <label class="control-label required">{l s='Platform' mod='gestpay'}</label>
                <div class="margin-form">
                     <select name="GESTPAY_PLATFORM" class="fixed-width-xxl" id="GESTPAY_PLATFORM">
                        {html_options options=$platforms selected=$platform_selected}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">{l s='Enable test mode' mod='gestpay'}</label>
                <div class="margin-form"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_TEST_MODE" id="GESTPAY_TEST_MODE_on" value="1" {if $gestpay_test_mode eq 1}checked="checked"{/if}/>
                        <label for="GESTPAY_TEST_MODE_on" class="t"><strong>{l s='Yes' mod='gestpay'}</strong></label>
                        <input type="radio" name="GESTPAY_TEST_MODE" id="GESTPAY_TEST_MODE_off" value="0" {if $gestpay_test_mode eq 0}checked="checked"{/if}/>
                        <label for="GESTPAY_TEST_MODE_off" class="t"><strong>{l s='No' mod='gestpay'}</strong></label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="preference_description">{l s='Enables the gateway test mode. Use only to check if the module is working. Please note that the test mode Shop Login can be different from the production mode.' mod='gestpay'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">{l s='Enable payment page localization' mod='gestpay'}</label>
                <div class="margin-form"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_LANGUAGE" id="GESTPAY_LANGUAGE_on" value="1" {if $gestpay_language eq 1}checked="checked"{/if}/>
                        <label for="GESTPAY_LANGUAGE_on" class="t"><strong>{l s='Yes' mod='gestpay'}</strong></label>
                        <input type="radio" name="GESTPAY_LANGUAGE" id="GESTPAY_LANGUAGE_off" value="0" {if $gestpay_language eq 0}checked="checked"{/if}/>
                        <label for="GESTPAY_LANGUAGE_off" class="t"><strong>{l s='No' mod='gestpay'}</strong></label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="preference_description">{l s='Enables or disables payment page translation according to the language used by the customer (multilanguage store). Please note that you have to enable the language parameter in the configuration page.' mod='gestpay'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">{l s='Enable iframe payment' mod='gestpay'}</label>
                <div class="margin-form"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_IFRAME" id="GESTPAY_IFRAME_on" value="1" {if $gestpay_iframe eq 1}checked="checked"{/if}/>
                        <label for="GESTPAY_IFRAME_on" class="t"><strong>{l s='Yes' mod='gestpay'}</strong></label>
                        <input type="radio" name="GESTPAY_IFRAME" id="GESTPAY_IFRAME_off" value="0" {if $gestpay_iframe eq 0}checked="checked"{/if}/>
                        <label for="GESTPAY_IFRAME_off" class="t"><strong>{l s='No' mod='gestpay'}</strong></label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="preference_description">{l s='Pay directly on the store checkout page, without being redirected to Axerve payment page. Please note that this setting has to be configured on the Axerve backend as well.' mod='gestpay'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">{l s='Enable tokens' mod='gestpay'}</label>
                <div class="margin-form"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_TOKEN" id="GESTPAY_TOKEN_on" value="1" {if $gestpay_token eq 1}checked="checked"{/if} {if $gestpay_token_enabled eq 0}disabled="disabled"{/if}/>
                        <label for="GESTPAY_TOKEN_on" class="t"><strong>{l s='Yes' mod='gestpay'}</strong></label>
                        <input type="radio" name="GESTPAY_TOKEN" id="GESTPAY_TOKEN_off" value="0" {if $gestpay_token eq 0}checked="checked"{/if} {if $gestpay_token_enabled eq 0}disabled="disabled"{/if}/>
                        <label for="GESTPAY_TOKEN_off" class="t"><strong>{l s='No' mod='gestpay'}</strong></label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="preference_description">{l s='Tokenization means the replacement of the credit card number with a Token. Please note that this setting has to be configured on the Axerve backend as well. Please refer to the manual for further info.' mod='gestpay'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">{l s='Use Sales API' mod='gestpay'}</label>
                <div class="margin-form"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_S2S" id="GESTPAY_S2S_on" value="1" {if $gestpay_s2s eq 1}checked="checked"{/if}/>
                        <label for="GESTPAY_S2S_on" class="t"><strong>{l s='Yes' mod='gestpay'}</strong></label>
                        <input type="radio" name="GESTPAY_S2S" id="GESTPAY_S2S_off" value="0" {if $gestpay_s2s eq 0}checked="checked"{/if}/>
                        <label for="GESTPAY_S2S_off" class="t"><strong>{l s='No' mod='gestpay'}</strong></label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="preference_description">{l s='Use Sales API for Settle and Refund actions. Available for Unlimited version only.' mod='gestpay'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">{l s='Sales API automation' mod='gestpay'}</label>
                <div class="margin-form"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_S2S_AUTO" id="GESTPAY_S2S_AUTO_on" value="1" {if $gestpay_s2s_auto eq 1}checked="checked"{/if} {if $gestpay_s2s_auto_enabled eq 0}disabled="disabled"{/if}/>
                        <label for="GESTPAY_S2S_AUTO_on" class="t"><strong>{l s='Yes' mod='gestpay'}</strong></label>
                        <input type="radio" name="GESTPAY_S2S_AUTO" id="GESTPAY_S2S_AUTO_off" value="0" {if $gestpay_s2s_auto eq 0}checked="checked"{/if} {if $gestpay_s2s_auto_enabled eq 0}disabled="disabled"{/if}/>
                        <label for="GESTPAY_S2S_AUTO_off" class="t"><strong>{l s='No' mod='gestpay'}</strong></label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="preference_description">{l s='Automate refund action when order is canceled or credit slip is created.' mod='gestpay'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">{l s='Use TLS endpoints' mod='gestpay'}</label>
                <div class="margin-form"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_TLS_ENDPOINT" id="GESTPAY_TLS_ENDPOINT_on" value="1" {if $gestpay_tls_endpoint eq 1}checked="checked"{/if}/>
                        <label for="GESTPAY_TLS_ENDPOINT_on" class="t"><strong>{l s='Yes' mod='gestpay'}</strong></label>
                        <input type="radio" name="GESTPAY_TLS_ENDPOINT" id="GESTPAY_TLS_ENDPOINT_off" value="0" {if $gestpay_tls_endpoint eq 0}checked="checked"{/if}/>
                        <label for="GESTPAY_TLS_ENDPOINT_off" class="t"><strong>{l s='No' mod='gestpay'}</strong></label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="preference_description">{l s='If your server does NOT support TLS set this flag to false.' mod='gestpay'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label"></label>
                <div class="margin-form">
                    <button type="button" id="test_connection" name="test_connection" class="btn btn-default button pull-left">{l s='Test connection' mod='gestpay'}</button>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">{l s='Remova data on uninstall' mod='gestpay'}</label>
                <div class="margin-form"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_REMOVE_TABLE" id="GESTPAY_REMOVE_TABLE_on" value="1" {if $gestpay_remove_table eq 1}checked="checked"{/if}/>
                        <label for="GESTPAY_REMOVE_TABLE_on" class="t"><strong>{l s='Yes' mod='gestpay'}</strong></label>
                        <input type="radio" name="GESTPAY_REMOVE_TABLE" id="GESTPAY_REMOVE_TABLE_off" value="0" {if $gestpay_remove_table eq 0}checked="checked"{/if}/>
                        <label for="GESTPAY_REMOVE_TABLE_off" class="t"><strong>{l s='No' mod='gestpay'}</strong></label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="preference_description">{l s='When enabled and the module is uninstalled, all the module data will be deleted, including transaction logs.' mod='gestpay'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">{l s='Enable module debugging log' mod='gestpay'}</label>
                <div class="margin-form"">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="GESTPAY_DEBUG" id="GESTPAY_DEBUG_on" value="1" {if $gestpay_debug eq 1}checked="checked"{/if}/>
                        <label for="GESTPAY_DEBUG_on" class="t"><strong>{l s='Yes' mod='gestpay'}</strong></label>
                        <input type="radio" name="GESTPAY_DEBUG" id="GESTPAY_DEBUG_off" value="0" {if $gestpay_debug eq 0}checked="checked"{/if}/>
                        <label for="GESTPAY_DEBUG_off" class="t"><strong>{l s='No' mod='gestpay'}</strong></label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="preference_description">{l s='Enable debug logs. Activate only on request by technical support.' mod='gestpay'}</p>
                </div>
            </div>
            <div class="form-group">
            <label class="control-label">&nbsp;</label>
            <button type="submit" value="1" id="submitConfig" name="submitConfig" class="btn btn-default button pull-right">
                <i class="process-icon-save"></i>{l s='Save' mod='gestpay'}
            </button>
            </div>
        </div>
</form>
</fieldset>
{/if}