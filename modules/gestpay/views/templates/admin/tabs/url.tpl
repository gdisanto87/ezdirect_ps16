{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2017 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}
{if $smarty.const._PS_VERSION_ >= 1.6}
<h3><i class="icon-link"></i> {l s='URL' mod='gestpay'} <small>{$module_display|escape:'html':'UTF-8'}</small></h3>
<div class="media">
        <p>{l s='In the "Response Address" area, set the link below for both "URL for positive response" and "URL for negative response" fields:' mod='gestpay'}<br />
        <span style="color: red">{$response_url|escape:'htmlall':'UTF-8'}validation.php</span></p>
        <p>{l s='In the "Response Address" area, set the link below "URL Server to Server" field:' mod='gestpay'}<br />
        <span style="color: red">{$response_url|escape:'htmlall':'UTF-8'}s2s.php</span></p>
        <p>{l s='In the "Ip Address" field, insert the ip or range ip of your server (please verify this address with your server provider)' mod='gestpay'}<br />
        <span style="color: red">{$ip|escape:'htmlall':'UTF-8'}</span></p>
</div>
{else}
<fieldset>
    <legend><img src="{$module_dir|escape:'html':'UTF-8'}views/img/information.png" alt="{l s='URL' mod='gestpay'} {$module_display|escape:'html':'UTF-8'}">{l s='URL' mod='gestpay'} {$module_display|escape:'html':'UTF-8'}</legend>
    <div class="media">
        <p>{l s='In the "Response Address" area, set the link below for both "URL for positive response" and "URL for negative response" fields:' mod='gestpay'}<br />
            <span style="color: red">{$response_url|escape:'htmlall':'UTF-8'}validation.php</span></p>
        <p>{l s='In the "Response Address" area, set the link below "URL Server to Server" field:' mod='gestpay'}<br />
            <span style="color: red">{$response_url|escape:'htmlall':'UTF-8'}s2s.php</span></p>
        <p>{l s='In the "Ip Address" field, insert the ip or range ip of your server (please verify this address with your server provider)' mod='gestpay'}<br />
            <span style="color: red">{$ip|escape:'htmlall':'UTF-8'}</span></p>
    </div>
</fieldset>
{/if}