{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2017 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}
{if $smarty.const._PS_VERSION_ >= 1.6}
<h3><i class="icon-book"></i> {l s='Help' mod='gestpay'} <small>{$module_display|escape:'html':'UTF-8'}</small></h3>
<div class="media">
        <a href="{$ps_url|escape:'html':'UTF-8'}{$module_dir|escape:'html':'UTF-8'}doc/readme_it.pdf" target="_blank">
        	<img src="{$ps_url|escape:'html':'UTF-8'}{$module_dir|escape:'html':'UTF-8'}views/img/pdf.png" style="margin-right: 10px; margin-bottom: 15px">{l s='Axerve module manual' mod='gestpay'}
        </a>
        <p>{l s='Axerve documentation:' mod='gestpay'}</p>
        <ul style='list-style-type:circle;'>
            <li><a href="http://docs.gestpay.it/gs/super-quick-start-guide.html" target="_blank">{l s='Quick start guide' mod='gestpay'}</a></li>
            <li><a href="https://gestpayws.sella.it/backoffice" target="_blank">{l s='Axerve control panel' mod='gestpay'}</a></li>
        </ul>
</div>
{else}
<fieldset>
    <legend><img src="{$module_dir|escape:'html':'UTF-8'}views/img/information.png" alt="{l s='Help' mod='gestpay'} {$module_display|escape:'html':'UTF-8'}">{l s='Help' mod='gestpay'} {$module_display|escape:'html':'UTF-8'}</legend>
    <div class="media">
        <a href="{$module_dir|escape:'html':'UTF-8'}doc/readme_it.pdf" target="_blank">
            <img src="{$module_dir|escape:'html':'UTF-8'}views/img/pdf.png" style="margin-right: 10px; margin-bottom: 15px; vertical-align: middle !important">{l s='Axerve module manual' mod='gestpay'}
        </a>
        <p>{l s='Axerve documentation:' mod='gestpay'}</p>
        <ul style='list-style-type:circle; padding-left: 20px'>
            <li><a href="http://docs.gestpay.it/gs/super-quick-start-guide.html" target="_blank">{l s='Quick start guide' mod='gestpay'}</a></li>
            <li><a href="https://ecomm.sella.it/gestpay" target="_blank">{l s='Axerve control panel' mod='gestpay'}</a></li>
        </ul>
    </div>
</fieldset>
{/if}