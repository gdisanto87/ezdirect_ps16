{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $smarty.const._PS_VERSION_ >= '1.7.7'}
	<div class="card mt-2 d-print-none">
	    <div class="card-header">
	        <div class="row">
	            <div class="col-md-6">
	                <h3 class="card-header-title">
	                    <img src="{$base_url|escape:'htmlall':'UTF-8'}modules/{$module_name|escape:'htmlall':'UTF-8'}/logo.png"
	                        alt="" height="30" width="30" /> {l s='Axerve Payment' mod='gestpay'}
	                </h3>
	            </div>
	        </div>
	    </div>
	    <div class="card-body">
	        <dl class="well list-detail">
	            <dt>{l s='Transaction status' mod='gestpay'}</dt>
	            <dd><span class="badge">{$status_msg|escape:'htmlall':'UTF-8'}</span></dd>
	            <dt>{l s='Shop Transaction ID' mod='gestpay'}</dt>
	            <dd>{$shop_transaction_id|escape:'htmlall':'UTF-8'}</dd>
	            <dt>{l s='Authorization Code' mod='gestpay'}</dt>
	            <dd>{$authorization_code|escape:'htmlall':'UTF-8'}</dd>
	            <dt>{l s='Company' mod='gestpay'}</dt>
	            <dd>{$company|escape:'htmlall':'UTF-8'}</dd>
	            {if $status == 'AUT'}
    	            <form method="post" action="{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}">
    	                <input type="hidden" name="id_order" value="{$params.id_order|intval}" />
    	                <div class="row">
    	                    <div class="col-lg-3">
    	                        <button type="submit" name="submitGestPaySettle" class="btn btn-primary"
    	                            onclick="if (!confirm('{l s='Are you sure?' mod='gestpay'}'))return false;">
    	                            {l s='Settle amount' mod='gestpay'}
    	                        </button>
    	                    </div>
    	                    <div class="col-lg-3">
    	                        <button type="submit" name="submitGestPayDelete" class="btn btn-warning"
    	                            style="text-transform:uppercase;"
    	                            onclick="if (!confirm('{l s='Are you sure?' mod='gestpay'}'))return false;">
    	                            {l s='Cancel authorization' mod='gestpay'}
    	                        </button>
    	                    </div>
    	                </div>
    	            </form>
	            {/if}
	            {if $status == 'MOV' && $total_refundable > 0}
    	            <dt>{l s='Information' mod='gestpay'}</dt>
    	            <dd>{l s='Any amount refunded manually will not create any order slip or modify order status.' mod='gestpay'}
    	            </dd>
    	            <dt>{l s='Manual refund amount' mod='gestpay'}</dt>
    	            <form method="post" action="{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}">
    	                <input type="hidden" name="id_order" value="{$params.id_order|intval}" />
    	                <div class="row">
    	                    <div class="col-sm-5">
    	                        <input type="text" class="form-control" name="refundAmount"
    	                            placeholder="{l s='Enter the amount to refund. Max:' mod='gestpay'} {$total_refundable|escape:'htmlall':'UTF-8'}" />
    	                    </div>
    	                    <div class="col-auto">
    	                        <button type="submit" name="submitGestPayRefund" class="btn btn-primary"
    	                            onclick="if (!confirm('{l s='Are you sure?' mod='gestpay'}'))return false;">
    	                            {l s='Refund amount' mod='gestpay'}
    	                        </button>
    	                    </div>
    	                </div>
    	            </form>
	            {/if}
	        </dl>
	        <table class="table">
	            <thead>
	                <tr>
	                    <th><span class="title_box ">{l s='Date' mod='gestpay'}</span></th>
	                    <th><span class="title_box ">{l s='Amount' mod='gestpay'}</span></th>
	                    <th><span class="title_box ">{l s='Operation' mod='gestpay'}</span></th>
	                </tr>
	            </thead>
	            {foreach from=$history item=list}
    	            <tbody>
    	                <tr>
    	                    <td>{$list->eventdate|escape:'htmlall':'UTF-8'}</td>
    	                    <td>{$list->eventamount|escape:'htmlall':'UTF-8'}</td>
    	                    <td>{$list->eventtype|escape:'htmlall':'UTF-8'}</td>
    	                </tr>
    	            </tbody>
	            {/foreach}
	        </table>
	    </div>
	</div>
{elseif $smarty.const._PS_VERSION_ < '1.7.7' and $smarty.const._PS_VERSION_ >= '1.6'}
<div class="panel">
	<div class="panel-heading"><img src="{$base_url|escape:'htmlall':'UTF-8'}modules/{$module_name|escape:'htmlall':'UTF-8'}/logo.png" alt="" height="30" width="30" /> {l s='Axerve Payment' mod='gestpay'}</div>
	<dl class="well list-detail">
		<dt>{l s='Transaction status' mod='gestpay'}</dt>
		<dd><span class="badge">{$status_msg|escape:'htmlall':'UTF-8'}</span></dd>
		<dt>{l s='Shop Transaction ID' mod='gestpay'}</dt>
		<dd>{$shop_transaction_id|escape:'htmlall':'UTF-8'}</dd>
		<dt>{l s='Authorization Code' mod='gestpay'}</dt>
		<dd>{$authorization_code|escape:'htmlall':'UTF-8'}</dd>
		<dt>{l s='Company' mod='gestpay'}</dt>
		<dd>{$company|escape:'htmlall':'UTF-8'}</dd>	
		{if $status == 'AUT'}			
    		<form method="post" action="{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}">
    			<input type="hidden" name="id_order" value="{$params.id_order|intval}" />				
    		    <div class="row">
    				<div class="col-lg-3">
    					<button type="submit" name="submitGestPaySettle" class="btn btn-primary" onclick="if (!confirm('{l s='Are you sure?' mod='gestpay'}'))return false;">
    						{l s='Settle amount' mod='gestpay'}
    					</button>
    				</div>
    				<div class="col-lg-3">
    					<button type="submit" name="submitGestPayDelete" class="btn btn-warning" style="text-transform:uppercase;" onclick="if (!confirm('{l s='Are you sure?' mod='gestpay'}'))return false;">
    						{l s='Cancel authorization' mod='gestpay'}
    					</button>
    				</div>
    	        </div>
    		</form>
    	{/if}
    	{if $status == 'MOV' && $total_refundable > 0}
    	    <dt>{l s='Information' mod='gestpay'}</dt>
    	    <dd>{l s='Any amount refunded manually will not create any order slip or modify order status.' mod='gestpay'}</dd>
    	    <dt>{l s='Manual refund amount' mod='gestpay'}</dt>
    		<form method="post" action="{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}">
    		<input type="hidden" name="id_order" value="{$params.id_order|intval}" />
    		    <div class="row">
    				<div class="col-lg-8">
    					<input type="text" name="refundAmount" placeholder="{l s='Enter the amount to refund. Max:' mod='gestpay'} {$total_refundable|escape:'htmlall':'UTF-8'}"/>
    				</div>
    				<div class="col-lg-4">
    					<button type="submit" name="submitGestPayRefund" class="btn btn-primary" onclick="if (!confirm('{l s='Are you sure?' mod='gestpay'}'))return false;">
    						{l s='Refund amount' mod='gestpay'}
    					</button>
    				</div>
    	        </div>
    		</form>
    	{/if}		
	</dl>
	<div class="table-responsive">
    	<table class="table">
        	<thead>
        	  <tr>
        	    <th><span class="title_box ">{l s='Date' mod='gestpay'}</span></th>
        	    <th><span class="title_box ">{l s='Amount' mod='gestpay'}</span></th> 
        	    <th><span class="title_box ">{l s='Operation' mod='gestpay'}</span></th>
        	  </tr>
        	</thead>	  
    	    {foreach from=$history item=list}
    	    <tbody>
            	<tr>
            	    <td>{$list->eventdate|escape:'htmlall':'UTF-8'}</td>
            	    <td>{$list->eventamount|escape:'htmlall':'UTF-8'}</td> 
            	    <td>{$list->eventtype|escape:'htmlall':'UTF-8'}</td>
            	</tr>
            </tbody>
    	    {/foreach}
    	</table>
	</div>
</div>
{else}
<br />
<fieldset>
	<legend><img src="{$base_url|escape:'htmlall':'UTF-8'}modules/{$module_name|escape:'htmlall':'UTF-8'}/logo.png" alt="" height="30" width="30" />{l s='Axerve Payment' mod='gestpay'}</legend>
	<table class="table" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<th>{l s='Date' mod='gestpay'}</th>
			<th>{l s='Amount' mod='gestpay'}</th> 
			<th>{l s='Operation' mod='gestpay'}</th>
		</tr>	
		{foreach from=$history item=list}
		  <tr>
		    <td>{$list->eventdate|escape:'htmlall':'UTF-8'}</td>
		    <td>{$list->eventamount|escape:'htmlall':'UTF-8'}</td>
		    <td>{$list->eventtype|escape:'htmlall':'UTF-8'}</td>
		  </tr>
		{/foreach}
	</table>
	<p><b>{l s='Transaction status' mod='gestpay'}:</b> {$status_msg|escape:'htmlall':'UTF-8'}</p>
	<p><b>{l s='Shop Transaction ID' mod='gestpay'}:</b> {$shop_transaction_id|escape:'htmlall':'UTF-8'}</p>
	<p><b>{l s='Authorization Code' mod='gestpay'}:</b> {$authorization_code|escape:'htmlall':'UTF-8'}</p>
	<p><b>{l s='Company' mod='gestpay'}:</b> {$company|escape:'htmlall':'UTF-8'}</p>	
	{if $status == 'AUT'}
			<form method="post" action="{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}">
				<input type="hidden" name="id_order" value="{$params.id_order|intval}" />				
				<p class="center">
					<button type="submit" class="btn btn-default" name="submitGestPaySettle" onclick="if (!confirm('{l s='Are you sure?' mod='gestpay'}'))return false;">
						<i class="icon-undo"></i>
						{l s='Settle amount' mod='gestpay'}
					</button>
					<button type="submit" class="btn btn-warning" style="text-transform:uppercase;" name="submitGestPayDelete" onclick="if (!confirm('{l s='Are you sure?' mod='gestpay'}'))return false;">
						<i class="icon-undo"></i>
						{l s='Cancel authorization' mod='gestpay'}
					</button>
				</p>
			</form>
	{/if}
	{if $status == 'MOV' && $total_refundable > 0}
    			<p><b>{l s='Information:' mod='gestpay'}</b> {l s='Any amount refunded manually will not create any order slip or modify order status.' mod='gestpay'}</p>
    			<form method="post" action="{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}">
    				<input type="hidden" name="id_order" value="{$params.id_order|intval}" />
    				<input type="text" name="refundAmount" placeholder="{l s='Enter the amount to refund. Max:' mod='gestpay'} {$total_refundable|escape:'htmlall':'UTF-8'}"/>
    				<p class="center">
    					<button type="submit" class="btn btn-default" name="submitGestPayRefund" onclick="if (!confirm('{l s='Are you sure?' mod='gestpay'}'))return false;">
    						<i class="icon-undo"></i>
    						{l s='Refund amount' mod='gestpay'}
    					</button>
    				</p>
    			</form>
	{/if}
</fieldset>
{/if}