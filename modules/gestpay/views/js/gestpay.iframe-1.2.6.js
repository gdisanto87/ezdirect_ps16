/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2017 PrestaShop SA
* @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*/
var GestpayObj = {};
GestpayObj.PaymentPageLoad = function(PageLoadResult){
    if(PageLoadResult.ErrorCode == 10 ){
        if (gestpay_PARes.length > 0){
            $("#GestpayLoadingBox").hide();
            $("#GestpayMessage").text(gestpay_payment_message);            
            $.blockUI({ 
                message: $('#GestpayDialog'), 
                css: {},
            }); 
            GestPay.SendPayment({PARes:gestpay_PARes,TransKey:gestpay_trans_key},GestpayObj.PaymentCallBack);
        }else{
            $("#GestpayLoadingBox").hide();
            $.unblockUI();
            $("#GestpayIframeForm").show();
        }
    }else{
        //An error has occurred, check the Result.ErrorCode and Result.ErrorDescription
        $("#GestpayErrorBox").text(PageLoadResult.ErrorDescription);
        $("#GestpayErrorBox").show();
        $.unblockUI();
    }
}
GestpayObj.PaymentCallBack = function (PaymentCallBackResult){
    if(PaymentCallBackResult.ErrorCode == 0 ){
        document.location.replace(gestpay_response_url+'?a='+gestpay_shop_login+'&b='+ Result.EncryptedString);
    }else{
        if(PaymentCallBackResult.ErrorCode == 8006){
            var TransKey = Result.TransKey
            $.ajax({
                type: "POST",
                url: gestpay_ajax_url,
                data: {
                    ajax : true,
                    transKey : TransKey.toString(),
                    encString : gestpay_enc_string,
                    action: "save3DSecure",
                },
                dataType : 'text'
            }).success(function(status){
                var VBVRisp = Result.VBVRisp
                var a = gestpay_shop_login;
                var b = VBVRisp;
                var c = gestpay_payment_url;
                var AuthUrl = gestpay_auth_url;
                document.location.replace(AuthUrl+'?a='+a+'&b='+b+'&c='+c);
            });
        }else{
            //Hide overlapping layer
            $.unblockUI();
            $("#GestpaySubmit").prop( "disabled", false );

            //Check the ErrorCode and ErrorDescription
            if(PaymentCallBackResult.ErrorCode == 1119 || PaymentCallBackResult.ErrorCode == 1120){
                $("#GestpayErrorBox").text(PaymentCallBackResult.ErrorDescription);
                $("#GestpayErrorBox").show();
                $("#gestpay_cc").focus();
            }
            if(PaymentCallBackResult.ErrorCode == 1124 || PaymentCallBackResult.ErrorCode == 1126){
                $("#GestpayErrorBox").text(PaymentCallBackResult.ErrorDescription);
                $("#GestpayErrorBox").show();
                $("#gestpay_expmm").focus();
            }
            if(PaymentCallBackResult.ErrorCode == 1125){
                $("#GestpayErrorBox").text(PaymentCallBackResult.ErrorDescription);
                $("#GestpayErrorBox").show();
                $("#gestpay_expyy").focus();
            }
            if(PaymentCallBackResult.ErrorCode == 1149){
                $("#GestpayErrorBox").text(PaymentCallBackResult.ErrorDescription);
                $("#GestpayErrorBox").show();
                $("#gestpay_cvv2").focus();
            }
            if(PaymentCallBackResult.ErrorCode != 1149 || PaymentCallBackResult.ErrorCode != 1119 || PaymentCallBackResult.ErrorCode != 1120 || PaymentCallBackResult.ErrorCode != 1124 || PaymentCallBackResult.ErrorCode != 1126 || PaymentCallBackResult.ErrorCode != 1125){
                $("#GestpayErrorBox").text(PaymentCallBackResult.ErrorDescription);
                $("#GestpayErrorBox").show();

            }
        }
    }
}
function CheckCC(){
    $("#GestpaySubmit").prop( "disabled", true );
    $("#GestpayMessage").text(gestpay_payment_message);
    $.blockUI({ 
            message: $('#GestpayDialog'), 
            css: {},
    }); 
    GestPay.SendPayment ({
        CC : $('#gestpay_cc').val(),
        EXPMM : $('#gestpay_expmm').val(),
        EXPYY : $('#gestpay_expyy').val(),
        CVV2: $('#gestpay_cvv2').val(),
    },GestpayObj.PaymentCallBack);
    return false;
}
$( document ).ready(function() {
    if(gestpay_method == 'iframe') {
        $("#GestpayIframeForm").hide();
        $("#GestpayErrorBox").hide();        
        if(BrowserEnabled && ChkResultTE){
            GestPay.CreatePaymentPage(gestpay_shop_login, gestpay_enc_string, GestpayObj.PaymentPageLoad);
        } else {            
            $("#GestpayLoadingBox").hide();
            $("#GestpayErrorBox").text(gestpay_browser_error);
            $("#GestpayErrorBox").show();
        }
    }    
    $("#GestpayIframeForm").submit(function( event ) {
        CheckCC();
        event.preventDefault();
    });
    $('#gestpay_save_token').change(function() {
        $("#GestpaySubmit").prop( "disabled", true );
        $.ajax({
            type: "POST",
            url: gestpay_ajax_url,
            data: {
                ajax : true,
                saveToken : (this.checked ? 1 : 0),
                action: "saveToken",
            },
            dataType : 'text'
        }).success(function(result){
            $("#GestpaySubmit").prop( "disabled", false );
        }).error(function(){
            $("#GestpaySubmit").prop( "disabled", false );
        });
    });
});
