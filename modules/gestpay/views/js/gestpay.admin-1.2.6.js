/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2017 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*/

$(document).ready(function() {    
	$.ajaxSetup({
		type: "POST",
		url: admin_module_ajax_url,
		data: {
			ajax : true,
			controller : admin_module_controller,
		}
	});

    $("#test_connection").click(function (e) {
    	$('#test_connection').attr("disabled", true);
    	var shopLogin = $('#GESTPAY_SHOP_LOGIN').val();
    	var platform = $('#GESTPAY_PLATFORM').val();
    	var testMode = $("input[name='GESTPAY_TEST_MODE']:checked").val();
    	var language = $("input[name='GESTPAY_LANGUAGE']:checked").val();
    	var s2s = $("input[name='GESTPAY_S2S']:checked").val();
    	var tls = $("input[name='GESTPAY_TLS_ENDPOINT']:checked").val();
    	$.ajax({
    		data: {
    			shopLogin : shopLogin,
    			platform : platform,
    			testMode : testMode,
    			language : language,
    			s2s : s2s,
    			tls : tls,
    			action: 'testConnection',
    		},
    		dataType : 'json'
    	}).success(function(json){
    		alert(json['messages']);
    		$('#test_connection').attr("disabled", false);
    	});
    });
});