<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class GestPay extends PaymentModule
{
    protected $js_path = null;
    protected $css_path = null;
    public $api = null;
    
    public function __construct()
    {
        include_once(_PS_MODULE_DIR_.'gestpay/libraries/GestPayAPI.php');
        
        $this->name = 'gestpay';
        $this->tab = 'payments_gateways';
        $this->version = '1.3.5';
        $this->author = 'Axerve';
        $this->module_key = '79c84251f771f88f6b694dcd416e49c2';
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
        
        $this->currencies = true;
        $this->currencies_mode = 'checkbox';
        $this->bootstrap = true;
        $this->need_instance = false;
        $this->is_eu_compatible = true;
        
        parent::__construct();

        $this->displayName = $this->l('Axerve');
        $this->description = $this->l('The official Axerve plugin for PrestaShop enables you to accept any form of payment supported by Axerve payment gateway.');
        $this->confirmUninstall = $this->l('Are you sure you want to delete module data?');
        
        /* paths */
        $this->js_path = $this->_path.'views/js/';
        $this->css_path = $this->_path.'views/css/';
        $this->debug_log_file = $this->getLocalPath().'logs/debug.log';
    }
    
    /**
    * Hook used for versions <= 1.6 to add css and js for iframe
    */
    public function hookDisplayHeader($params)
    {
        if (!$this->active) {
            return;
        }
        /* add js for TLS check */
        if (Tools::substr($this->context->controller->php_self, 0, 5) === "order") {
            $this->context->controller->addJS('https://sandbox.gestpay.net/pagam/javascript/TLSCHK_TE.js');
            $this->context->controller->addJS('https://ecomm.sella.it/pagam/javascript/TLSCHK_PRO.js');
            $this->context->controller->addJS('https://www.gestpay.it/checkbrowser/checkBrowser.js');
        }
        if ($this->context->controller->php_self == 'order-opc') {
            /* if 1 step order process add js and css here */
            if (Configuration::get('PS_ORDER_PROCESS_TYPE') == 1) {
                if ($this->connect()) {
                    if (Configuration::get('GESTPAY_IFRAME')) {
                        if (!$this->payByToken($this->context->customer->id)) {
                            $this->context->controller->addCSS($this->css_path.'gestpay.iframe_1.6.css');
                            $this->context->controller->addJS($this->api->getJavaScriptUrl());
                            $this->context->controller->addJS($this->js_path.'jquery.blockUI.js');
                            $this->context->controller->addJS($this->js_path.'gestpay.iframe-1.2.6.js');
                        }
                    }
                }
            }
        }
    }
    
    /**
    * Hook version <= 1.6
    */
    public function hookDisplayPayment($params)
    {
        if (!$this->active) {
            return;
        }
        if (!$this->checkCurrency($params['cart'])) {
            return;
        }
        if (Tools::getValue('gestpay_error_msg')) {
            $this->context->controller->errors[] = Tools::getValue('gestpay_error_msg');
        }
        if ($this->connect()) {
            $content = '';
            $with_iframe = Configuration::get('GESTPAY_IFRAME');
            if ($with_iframe) {
                /* if 5 steps order process add js and css here to avoid js errors in the steps before */
                if (Configuration::get('PS_ORDER_PROCESS_TYPE') == 0) {
                    $this->context->controller->addCSS($this->css_path.'gestpay.iframe_1.6.css');
                }
                $token = $this->payByToken($this->context->customer->id);
                if ($token) {
                    $content = $this->tokenGetContent($token);
                } else {
                    if (Configuration::get('PS_ORDER_PROCESS_TYPE') == 0) {
                        $this->context->controller->addJS($this->api->getJavaScriptUrl());
                        $this->context->controller->addJS($this->js_path.'jquery.blockUI.js');
                        $this->context->controller->addJS($this->js_path.'gestpay.iframe-1.2.6.js');
                    }
                    $content = $this->iframeGetContent(Configuration::get('GESTPAY_TOKEN'));
                }
            }          
            // if list methods available show a single button fo every payment type
            if (Configuration::get('GESTPAY_LIST_METHODS') && $methods = $this->api->getPaymentMethods()) { 
                foreach($methods as $method) {
                    if ($with_iframe && $method->paymentType == 'CREDITCARD') {
                        continue;
                    }
                    $redirectUrl = $this->api->getRedirectUrl(true, false, array($method->paymentType));
                    if ($redirectUrl) {
                        $this->context->smarty->assign(
                            array(
                                'gestpay_images_path' => $this->_path.'views/img/',
                                'gestpay_redirect_url' => $redirectUrl,
                                'gestpay_method_name' => ucfirst(strtolower($method->name)),
                                'gestpay_logo' => $method->logo->large,
                                'gestpay_payment_type' => $method->paymentType,
                            )
                        );
                        $content .= $this->display(__FILE__, 'payment_single.tpl');
                    }
                }
            } else {
                $redirectUrl = $this->api->getRedirectUrl();
                if ($redirectUrl) {
                    $this->context->smarty->assign(
                        array(
                            'gestpay_images_path' => $this->_path.'views/img/',
                            'gestpay_redirect_url' => $redirectUrl,
                        )
                    );
                    $content .= $this->display(__FILE__, 'payment.tpl');
                }
            }
            return $content;
        }
    }
    /**
    * New hook version 1.7 payment api
    */
    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }
        if (!$this->checkCurrency($params['cart'])) {
            return;
        }
        if (Tools::getValue('gestpay_error_msg')) {
            $this->context->controller->errors[] = Tools::getValue('gestpay_error_msg');
        }
        if ($this->connect()) {
            $payment_options = array();
            $with_iframe = Configuration::get('GESTPAY_IFRAME');
            if ($with_iframe) {
                $token = $this->payByToken($this->context->customer->id);
                if ($token) {
                    $action = $this->context->link->getModuleLink('gestpay', 'validation', array('is_token' => true));
                    $info = $this->tokenGetContent($token);
                    $externalOption = new PrestaShop\PrestaShop\Core\Payment\PaymentOption();
                    $externalOption->setCallToActionText($this->l('Pay by credit card'))
                                   ->setAdditionalInformation($info)
                                   ->setAction($action);
                    $payment_options[] = $externalOption;
                } else {
                    $info = $this->iframeGetContent(Configuration::get('GESTPAY_TOKEN'));
                    $externalOption = new PrestaShop\PrestaShop\Core\Payment\PaymentOption();
                    $externalOption->setCallToActionText($this->l('Pay by credit card'))
                                   ->setAdditionalInformation($info)
                                   ->setBinary(true);
                    $payment_options[] = $externalOption;
                }
            }
            // if list methods available show a single button fo every payment type
            if (Configuration::get('GESTPAY_LIST_METHODS') && $methods = $this->api->getPaymentMethods()) { 
                foreach($methods as $method) {
                    if ($with_iframe && $method->paymentType == 'CREDITCARD') {
                        continue;
                    }
                    $redirectUrl = $this->api->getRedirectUrl(true, false, array($method->paymentType));
                    if ($redirectUrl) {
                        $this->context->smarty->assign(
                            array(
                                'gestpay_images_path' => $this->_path.'views/img/',
                                'gestpay_method_name' => $method->logo->large,
                                'gestpay_payment_type' => $method->paymentType,
                            )
                        );
                        $info = $this->fetch('module:gestpay/views/templates/hook/payment_info_single.tpl');
                        $externalOption = new PrestaShop\PrestaShop\Core\Payment\PaymentOption();
                        $externalOption->setCallToActionText($this->l('Pay with ') . ucfirst(strtolower($method->name)))
                                       ->setAction($redirectUrl)
                                       ->setAdditionalInformation($info);
                        $payment_options[] = $externalOption;
                    }
                }
            } else {
                $redirectUrl = $this->api->getRedirectUrl();
                if ($redirectUrl) {
                    $this->context->smarty->assign('gestpay_images_path', $this->_path.'views/img/');
                    $info = $this->fetch('module:gestpay/views/templates/hook/payment_info.tpl');
                    $externalOption = new PrestaShop\PrestaShop\Core\Payment\PaymentOption();
                    $externalOption->setCallToActionText($this->l('Pay with card or other payment methods'))
                                   ->setAction($redirectUrl)
                                   ->setAdditionalInformation($info);
                    $payment_options[] = $externalOption;
                }
            }
            return $payment_options;
        }
    }
    /**
    * Hook needed by version 1.7 to load css and js for iframe
    */
    public function hookActionFrontControllerSetMedia($params)
    {
        if ($this->context->controller->php_self === 'order') {
            $this->context->controller->registerJavascript(
                'gestpay-TLSCHK_TE',
                'https://sandbox.gestpay.net/pagam/javascript/TLSCHK_TE.js',
                array('server' => 'remote', 'position' => 'head', 'priority' => 100)
            );
            $this->context->controller->registerJavascript(
                'gestpay-TLSCHK_PRO',
                'https://ecomm.sella.it/pagam/javascript/TLSCHK_PRO.js',
                array('server' => 'remote', 'position' => 'head', 'priority' => 110)
            );
            $this->context->controller->registerJavascript(
                'gestpay-checkBrowser',
                'https://www.gestpay.it/checkbrowser/checkBrowser.js',
                array('server' => 'remote', 'position' => 'head', 'priority' => 120)
            );
            if (Configuration::get('GESTPAY_IFRAME')) {
                if ($this->connect()) {
                    /* add js and css */
                    $this->context->controller->registerStylesheet(
                        'gestpay-iframe-css',
                        'modules/'.$this->name.'/views/css/gestpay.iframe_1.7.css',
                        array('media' => 'all','priority' => 800)
                    );
                    $this->context->controller->registerJavascript(
                        'gestpay-js',
                        $this->api->getJavaScriptUrl(),
                        array('server' => 'remote', 'position' => 'bottom', 'priority' => 450)
                    );
                    $this->context->controller->registerJavascript(
                        'gestpay-block-ui',
                        'modules/'.$this->name.'/views/js/jquery.blockUI.js',
                        array('position' => 'bottom', 'priority' => 400)
                    );
                    $this->context->controller->registerJavascript(
                        'gestpay-iframe',
                        'modules/'.$this->name.'/views/js/gestpay.iframe-1.2.6.js',
                        array('position' => 'bottom', 'priority' => 500)
                    );
                    $this->context->controller->addJquery();
                }
            }
        }
    }
    
    public function hookDisplayPaymentReturn($params)
    {
        if (!$this->active) {
            return;
        }
        if (!$this->checkCurrency($params['cart'])) {
            return;
        }
        if (version_compare(_PS_VERSION_, '1.7', '>')) {
            $state = $params['order']->getCurrentState();
        } else {
            $state = $params['objOrder']->getCurrentState();
        }
        if ($state == Configuration::get('PS_OS_PAYMENT') || $state == Configuration::get('PS_OS_OUTOFSTOCK')) {
            $this->context->smarty->assign('gestpay_result', 'ok');
            $this->context->smarty->assign('shop_name', $this->context->shop->name);
            return $this->display(__FILE__, 'payment_return.tpl');
        } else {
            $this->context->smarty->assign($this->name.'_result', 'error');
            $this->context->smarty->assign('shop_name', $this->context->shop->name);
            return $this->display(__FILE__, 'payment_return.tpl');
        }
    }
    
    /**
    * Show payment info on order page version 1.7.7 and up
    */
    public function hookDisplayAdminOrderMain($params)
    {        
        return $this->displayPanel($params);
    }  
    /**
    * Show payment info on order page version 1.6 and up
    */
    public function hookDisplayAdminOrderLeft($params)
    {
        return $this->displayPanel($params);
    }
    
    /**
    * Show payment info on order page version 1.5
    */
    public function hookAdminOrder($params)
    {
        return $this->displayPanel($params);
    }
    
    /**
    * Called when new orderslip is added, used for Sales API
    */
    public function hookActionObjectOrderSlipAddAfter($params)
    {
        if (!Configuration::get('GESTPAY_S2S_AUTO') ||
            Tools::isSubmit('generateDiscount') ||
            Tools::isSubmit('generateDiscountRefund') ||
            !($order_slip = $params['object']) ||
            !Validate::isLoadedObject($order_slip)) {
            return false;
        }
        if (version_compare(_PS_VERSION_, '1.6', '>=')) {
            $amount = (float) $order_slip->total_products_tax_incl + $order_slip->total_shipping_tax_incl;
        } else {
            $amount = (float) $order_slip->amount;
        }
        $this->refund((int)$order_slip->id_order, $amount);
    }
    
    /**
    * Called when order status is set to void, used for Sales API
    */
    public function hookActionOrderStatusPostUpdate($params)
    {
        if (Configuration::get('GESTPAY_S2S_AUTO')) {
            if ($params['newOrderStatus']->id == Configuration::get('PS_OS_CANCELED')) {
                $this->refund((int)$params['id_order']);
            }
        }
    }
    
    /**
    * Can pay with token?
    */
    private function payByToken($customer_id)
    {
        $use_iframe = Configuration::get('GESTPAY_IFRAME');
        $use_token = Configuration::get('GESTPAY_TOKEN');
        $token = GestPayToken::fetchByIdCustomer($customer_id);
        $year = Tools::substr(date('Y'), 0, 2);
        if ($use_token &&
            $use_iframe &&
            $token &&
            isset($token->token) &&
            Tools::strlen($token->token) > 0 &&
            isset($token->token_expiry_month) &&
            isset($token->token_expiry_year) &&
            strtotime($year."{$token->token_expiry_year}-{$token->token_expiry_month}") >= strtotime(date("Y-m"))) {
            return $token->token;
        } else {
            return false;
        }
    }
    
    /**
    * Show token content
    */
    private function tokenGetContent($token)
    {
        $images_path = $this->_path.'views/img/';
        $redirect_url = $this->context->link->getModuleLink('gestpay', 'validation', array('is_token' => true));
        $this->context->smarty->assign(
            array(
                'gestpay_redirect_url' => $redirect_url,
                'gestpay_images_path' => $images_path,
                'gestpay_token' => Tools::substr($token, -4),
            )
        );
        return $this->display(__FILE__, 'payment_token.tpl');
    }
    
    /**
    * Show iframe content
    */
    private function iframeGetContent($use_token)
    {
        $ps_url = Tools::usingSecureMode() ? Tools::getShopDomainSsl(true) : Tools::getShopDomain(true);
        $modules_url = $ps_url.__PS_BASE_URI__.'modules/';
        $response_url = $modules_url . $this->name . '/remote/validation.php';
        $ajax_url = $this->context->link->getModuleLink($this->name, 'iframe', array(), true);
        $images_path = $this->_path.'views/img/';
        $shop_login = $this->api->shopLogin;
        /* requested parameters for 3D secure check */
        $pa_res = '';
        $trans_key = '';
        $enc_string = '';
        $auth_url = $this->api->getAuthUrl();
        $payment_url = $modules_url . $this->name . '/remote/iframe.php';
        if (Tools::getIsset('PaRes')) {
            /* returning from 3D secure check */
            $pa_res = preg_replace("/\r|\n|\r\n/", "", trim(Tools::getValue('PaRes')));
            $shoptransactionid = "GP_" . $this->context->cart->id;
            $t = GestPayTransaction::fetchByShopTransactionId($shoptransactionid);
            $trans_key = $t->transKey;
            $enc_string = $t->encString;
        } else {
            if ($response = $this->api->getRedirectUrl(false, $use_token)) {
                $enc_string = $response['encString'];
                $buyer_name = $response['buyerName'];
                $buyer_email = $response['buyerEmail'];
                $save_token = false;
                $token = GestPayToken::fetchByIdCustomer($this->context->cart->id_customer);
                if ($token) {
                    $save_token = $token->save_token;
                } else {
                    $token = new GestPayToken();
                    $token->id_customer = $this->context->cart->id_customer;
                    $token->save_token = false;
                    $token->save();
                }
            }
        }
        if (Tools::strlen($enc_string) > 0) {
            $this->context->smarty->assign(
                array(
                    'gestpay_method' => 'iframe',
                    'gestpay_shop_login' => $shop_login,
                    'gestpay_enc_string' => $enc_string,
                    'gestpay_pa_res' => $pa_res,
                    'gestpay_trans_key' => $trans_key,
                    'gestpay_response_url' => $response_url,
                    'gestpay_auth_url' => $auth_url,
                    'gestpay_ajax_url' => $ajax_url,
                    'gestpay_payment_url' => $payment_url,
                    'gestpay_years' => self::dateYears(),
                    'gestpay_months' => self::dateMonths(),
                    'gestpay_images_path' => $images_path,
                    'gestpay_use_token' => $use_token,
                    'gestpay_buyer_name' => $buyer_name,
                    'gestpay_buyer_email' => $buyer_email,
                    'gestpay_save_token' => $save_token
                )
            );
            return $this->display(__FILE__, 'payment_iframe.tpl');
        } else {
            return false;
        }
    }
    
    private static function dateYears()
    {
        $tab = array();
        for ($i = date('Y'); $i <= date('Y') + 10; $i++) {
            $tab[Tools::substr($i, 2)] = $i;
        }
        return $tab;
    }
    public static function dateMonths()
    {
        $tab = array();
        for ($i = 1; $i != 13; $i++) {
            $tab[] = str_pad($i, 2, "0", STR_PAD_LEFT);
        }
        return $tab;
    }
    private function displayPanel($params)
    {
        /* manage actions */
        if (Tools::isSubmit('submitGestPayRefund')) {
            if ($amount = Tools::getValue('refundAmount')) {
                $this->refund((int)$params['id_order'], $amount);
            }
        } elseif (Tools::isSubmit('submitGestPaySettle')) {
            $this->settle((int)$params['id_order']);
        } elseif (Tools::isSubmit('submitGestPayDelete')) {
            $this->delete((int)$params['id_order']);
        }
    
        /* Display info */
        if ($transaction = GestPayTransaction::fetchByIdOrder((int)$params['id_order'])) {
            if ($this->connect()) {
                if ($this->api->isSalesAPIAvailable()) {
                    $request = new GestPayReadTrxRequest(
                        $this->api->shopLogin,
                        $transaction->shoptransactionid,
                        $transaction->banktransactionid
                    );
                    if ($response = $this->api->readTrx($request)) {
                        if ($response->ErrorCode == '0') {
                            $ShopTransactionID = $response->ShopTransactionID;
                            $AuthorizationCode = $response->AuthorizationCode;
                            $TransactionState = $response->TransactionState;
                            $Company = $response->Company;
                            $total_refundable = Tools::ps_round($this->api->getTotalRefundable($response), 2);
                            $history = $this->api->getDecodedEvents($response);
                            $can_refund = false;
                            if ($TransactionState == 'AUT') {
                                $status_msg = $this->l('Payment authorized, needs settle');
                            } elseif ($TransactionState == 'CAN') {
                                $status_msg = $this->l('Payment canceled');
                            } else {
                                if ($total_refundable > 0) {
                                    $status_msg = $this->l('Payment settled');
                                    $can_refund = true;
                                } else {
                                    $status_msg = $this->l('Payment completely refunded');
                                }
                            }
                            $this->context->smarty->assign(
                                array(
                                    'base_url' => Tools::getHttpHost(true).__PS_BASE_URI__,
                                    'module_name' => $this->name,
                                    'params' => $params,
                                    'history' => $history,
                                    'total_refundable' => $total_refundable,
                                    'status' => $TransactionState,
                                    'status_msg' => $status_msg,
                                    'can_refund' => $can_refund,
                                    'shop_transaction_id' => $ShopTransactionID,
                                    'authorization_code' => $AuthorizationCode,
                                    'company' => $Company,
                                )
                            );
                            return $this->display(__FILE__, '/views/templates/admin/order/gestpay.tpl');
                        }
                    }
                }
            }
        }
    }
    /**
    * Request refund
    */
    private function refund($id_order, $amount = 0)
    {
        if ($transaction = GestPayTransaction::fetchByIdOrder((int)$id_order)) {
            if ($this->connect()) {
                if ($this->api->isSalesAPIAvailable()) {
                    if (Validate::isFloat($amount)) {
                        $request = new GestPayReadTrxRequest(
                            $this->api->shopLogin,
                            $transaction->shoptransactionid,
                            $transaction->banktransactionid
                        );
                        if ($response = $this->api->readTrx($request)) {
                            if ($response->ErrorCode == '0') {
                                $totalRefundable = $this->api->getTotalRefundable($response);
                                if ($amount > 0 && $amount <= $totalRefundable) {
                                    $refund = $amount;
                                } else {
                                    $refund = $totalRefundable;
                                }
                                $currency = new Currency((int)$transaction->id_currency);
                                $uicCode = GestPayAPI::GetUicCode($currency->iso_code_num);
                                $request = new GestPayRefundRequest(
                                    $this->api->shopLogin,
                                    $uicCode,
                                    $refund,
                                    $transaction->shoptransactionid,
                                    $transaction->banktransactionid
                                );
                                $this->api->refund($request);
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
    /**
    * Request settle
    */
    private function settle($id_order)
    {
        if ($transaction = GestPayTransaction::fetchByIdOrder((int)$id_order)) {
            if ($this->connect()) {
                if ($this->api->isSalesAPIAvailable()) {
                    $currency = new Currency((int)$transaction->id_currency);
                    $uicCode = GestPayAPI::GetUicCode($currency->iso_code_num);
                    $request = new GestPaySettleRequest(
                        $this->api->shopLogin,
                        $uicCode,
                        $transaction->amount,
                        $transaction->shoptransactionid,
                        $transaction->banktransactionid
                    );
                    return $this->api->settle($request);
                }
            }
        }
        return false;
    }
    
    /**
    * Request delete
    */
    private function delete($id_order)
    {
        if ($transaction = GestPayTransaction::fetchByIdOrder((int)$id_order)) {
            if ($this->connect()) {
                if ($this->api->isSalesAPIAvailable()) {
                    $request = new GestPayDeleteRequest(
                        $this->api->shopLogin,
                        $transaction->shoptransactionid,
                        $transaction->banktransactionid
                    );
                    return $this->api->delete($request);
                }
            }
        }
        return false;
    }
    
    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }
        /* set default values */
        if (!Configuration::get('GESTPAY_SHOP_LOGIN')) {
            Configuration::updateValue('GESTPAY_SHOP_LOGIN', '');
        }
        if (!Configuration::get('GESTPAY_PLATFORM')) {
            Configuration::updateValue('GESTPAY_PLATFORM', 'starter');
        }
        if (!Configuration::get('GESTPAY_TEST_MODE')) {
            Configuration::updateValue('GESTPAY_TEST_MODE', true);
        }
        if (!Configuration::get('GESTPAY_LANGUAGE')) {
            Configuration::updateValue('GESTPAY_LANGUAGE', false);
        }
        if (!Configuration::get('GESTPAY_S2S')) {
            Configuration::updateValue('GESTPAY_S2S', false);
        }
        if (!Configuration::get('GESTPAY_S2S_AUTO')) {
            Configuration::updateValue('GESTPAY_S2S_AUTO', false);
        }
        if (!Configuration::get('GESTPAY_IFRAME')) {
            Configuration::updateValue('GESTPAY_IFRAME', false);
        }
        if (!Configuration::get('GESTPAY_TOKEN')) {
            Configuration::updateValue('GESTPAY_TOKEN', false);
        }
        if (!Configuration::get('GESTPAY_REMOVE_TABLE')) {
            Configuration::updateValue('GESTPAY_REMOVE_TABLE', false);
        }
        if (!Configuration::get('GESTPAY_DEBUG')) {
            Configuration::updateValue('GESTPAY_DEBUG', false);
        }
        if (!Configuration::get('GESTPAY_TLS_ENDPOINT')) {
            Configuration::updateValue('GESTPAY_TLS_ENDPOINT', true);
        }
        if (!Configuration::get('GESTPAY_APIKEY')) {
            Configuration::updateValue('GESTPAY_APIKEY', '');
        }
        if (!Configuration::get('GESTPAY_LIST_METHODS')) {
            Configuration::updateValue('GESTPAY_LIST_METHODS', false);
        }

        /* register hooks based on PS Version */
        if (version_compare(_PS_VERSION_, '1.7.7', '>=')) {
            if (parent::install()) {
                include(dirname(__FILE__) . '/sql/install.php');
                return $this->registerHook('actionObjectOrderSlipAddAfter') &&
                       $this->registerHook('displayAdminOrderMain') &&
                       $this->registerHook('actionOrderStatusPostUpdate') &&
                       $this->registerHook('actionFrontControllerSetMedia') &&
                       $this->registerHook('paymentOptions') &&
                       $this->registerHook('paymentReturn');
            }
            return false;
        } elseif (version_compare(_PS_VERSION_, '1.7.0', '>=') && version_compare(_PS_VERSION_, '1.7.7', '<')) {
            if (parent::install()) {
                include(dirname(__FILE__) . '/sql/install.php');
                return $this->registerHook('actionObjectOrderSlipAddAfter') &&
                       $this->registerHook('displayAdminOrderLeft') &&
                       $this->registerHook('actionOrderStatusPostUpdate') &&
                       $this->registerHook('actionFrontControllerSetMedia') &&
                       $this->registerHook('paymentOptions') &&
                       $this->registerHook('paymentReturn');
            }
            return false;
        } elseif (version_compare(_PS_VERSION_, '1.6.0', '>=') && version_compare(_PS_VERSION_, '1.7.0', '<')) {
            if (parent::install()) {
                include(dirname(__FILE__) . '/sql/install.php');
                return $this->registerHook('actionObjectOrderSlipAddAfter') &&
                       $this->registerHook('displayAdminOrderLeft') &&
                       $this->registerHook('actionOrderStatusPostUpdate') &&
                       $this->registerHook('payment') &&
                       $this->registerHook('header') &&
                       $this->registerHook('paymentReturn');
            }
            return false;
        } elseif (version_compare(_PS_VERSION_, '1.5.0', '>=') && version_compare(_PS_VERSION_, '1.6.0', '<')) {
            if (parent::install()) {
                include(dirname(__FILE__) . '/sql/install.php');
                return $this->registerHook('actionObjectOrderSlipAddAfter') &&
                       $this->registerHook('adminOrder') &&
                       $this->registerHook('actionOrderStatusPostUpdate') &&
                       $this->registerHook('payment') &&
                       $this->registerHook('header') &&
                       $this->registerHook('paymentReturn');
            }
            return false;
        } else {
            return false;
        }
    }

    public function uninstall()
    {
        if (Configuration::get('GESTPAY_REMOVE_TABLE')) {
            Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'gestpay_transaction`');
            Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'gestpay_token`');
        }
        if (!Configuration::deleteByName('GESTPAY_SHOP_LOGIN') ||
            !Configuration::deleteByName('GESTPAY_TEST_MODE') ||
            !Configuration::deleteByName('GESTPAY_LANGUAGE') ||
            !Configuration::deleteByName('GESTPAY_S2S') ||
            !Configuration::deleteByName('GESTPAY_S2S_AUTO') ||
            !Configuration::deleteByName('GESTPAY_IFRAME') ||
            !Configuration::deleteByName('GESTPAY_TOKEN') ||
            !Configuration::deleteByName('GESTPAY_REMOVE_TABLE') ||
            !Configuration::deleteByName('GESTPAY_DEBUG') ||
            !Configuration::deleteByName('GESTPAY_TLS_ENDPOINT') ||
            !Configuration::deleteByName('GESTPAY_PLATFORM') ||
            !Configuration::deleteByName('GESTPAY_APIKEY') ||
            !Configuration::deleteByName('GESTPAY_LIST_METHODS') ||
            !parent::uninstall()) {
            return false;
        }
        return true;
    }
    
   
    /**
    * Create api object
    * @return boolean
    */
    public function connect()
    {
        if (!Configuration::get('GESTPAY_SHOP_LOGIN') &&
            !Configuration::get('GESTPAY_PLATFORM') &&
            !Configuration::get('GESTPAY_TEST_MODE') &&
            !Configuration::get('GESTPAY_LANGUAGE') &&
            !Configuration::get('GESTPAY_S2S') &&
            !Configuration::get('GESTPAY_DEBUG') &&
            !Configuration::get('GESTPAY_TLS_ENDPOINT') &&
            !Configuration::get('GESTPAY_APIKEY')
        ) {
            return false;
        }
        $this->api = new GestPayAPI(
            trim(Configuration::get('GESTPAY_SHOP_LOGIN')),
            Configuration::get('GESTPAY_PLATFORM'),
            Configuration::get('GESTPAY_TEST_MODE'),
            Configuration::get('GESTPAY_LANGUAGE'),
            Configuration::get('GESTPAY_S2S'),            
            Configuration::get('GESTPAY_DEBUG'),
            Configuration::get('GESTPAY_TLS_ENDPOINT'),
            $this->debug_log_file,
            Configuration::get('GESTPAY_APIKEY')
        );
        return true;
    }  

    public function getContent()
    {        
        $this->context->controller->addCSS($this->css_path.$this->name.'.admin.css');
        $this->context->controller->addJS($this->js_path.$this->name.'.admin-1.2.6.js');
        /* data for ui */
        $controller_name = 'AdminGestPay';
        $controller_url = $this->context->link->getAdminLink($controller_name);
        $ps_url = Tools::usingSecureMode() ? Tools::getShopDomainSsl(true) : Tools::getShopDomain(true);
        $modules_url = $ps_url.__PS_BASE_URI__.'modules/';
        $response_url = $modules_url . $this->name . '/remote/';
        $ip = gethostbyname($_SERVER['SERVER_NAME']);
        $current_view = Tools::getValue('current_view');
        $request_uri = basename($_SERVER['REQUEST_URI']);
        $api_key_error = 0;
        $api_key_error_message = '';
        $config_error = 0;
        $config_error_message = '';
        if (!$logs = Tools::file_get_contents($this->debug_log_file, false, null, -30)) {
            $logs = $this->l('Logs not found.');
        }
        /* Forms submit manager  */
        $submit_message = array();
        if (Tools::isSubmit('submitConfig')) {
            Configuration::updateValue('GESTPAY_SHOP_LOGIN', trim(pSQL(Tools::getValue('GESTPAY_SHOP_LOGIN'))));
            Configuration::updateValue('GESTPAY_TEST_MODE', (int)Tools::getValue('GESTPAY_TEST_MODE'));
            Configuration::updateValue('GESTPAY_LANGUAGE', (int)Tools::getValue('GESTPAY_LANGUAGE'));
            Configuration::updateValue('GESTPAY_S2S', (int)Tools::getValue('GESTPAY_S2S'));
            Configuration::updateValue('GESTPAY_S2S_AUTO', (int)Tools::getValue('GESTPAY_S2S_AUTO'));
            Configuration::updateValue('GESTPAY_IFRAME', (int)Tools::getValue('GESTPAY_IFRAME'));
            Configuration::updateValue('GESTPAY_TOKEN', (int)Tools::getValue('GESTPAY_TOKEN'));
            Configuration::updateValue('GESTPAY_REMOVE_TABLE', (int)Tools::getValue('GESTPAY_REMOVE_TABLE'));
            Configuration::updateValue('GESTPAY_DEBUG', (int)Tools::getValue('GESTPAY_DEBUG'));
            Configuration::updateValue('GESTPAY_PLATFORM', pSQL(Tools::getValue('GESTPAY_PLATFORM')));
            Configuration::updateValue('GESTPAY_TLS_ENDPOINT', pSQL(Tools::getValue('GESTPAY_TLS_ENDPOINT')));
            Configuration::updateValue('GESTPAY_APIKEY', pSQL(Tools::getValue('GESTPAY_APIKEY')));
            Configuration::updateValue('GESTPAY_LIST_METHODS', pSQL(Tools::getValue('GESTPAY_LIST_METHODS')));
            $submit_message['config'] = $this->displayConfirmation($this->l('Configuration updated.'));
        }
        /* Test config  */
        if ($current_view == 'config' or $current_view == '') {
            $result = $this->testConfig();
            if (!$result['status']) {
                $config_error = 1;
                $config_error_message = nl2br($result['messages']);
            }
            /* Test api */
            if (Configuration::get('GESTPAY_SHOP_LOGIN') && $this->connect()) {
                $result = $this->api->testConnection();
                if (!$result['status']) {
                    $api_key_error = 1;
                    $api_key_error_message = nl2br($result['messages']);
                }
            }
        }
        $list = $this->getTransactionTable();
        $this->context->smarty->assign(array(
            'submit_message' => $submit_message,
            'current_view' => $current_view,
            'modules_url' => $modules_url,
            'module_display' => $this->displayName,
            'request_uri' => $request_uri,
            'ps_url' => $ps_url,
            'response_url' => $response_url,
            'ip' => $ip,
            'controller_url' => $controller_url,
            'controller_name' => $controller_name,
            'config_error' => $config_error,
            'config_error_message' => $config_error_message,
            'api_key_error' => $api_key_error,
            'api_key_error_message' => $api_key_error_message,
            'gestpay_shop_login' => trim(pSQL(Configuration::get('GESTPAY_SHOP_LOGIN'))),
            'platforms' => GestPayAPI::getPlatforms(),
            'platform_selected' => pSQL(Configuration::get('GESTPAY_PLATFORM')),
            'gestpay_test_mode' => pSQL(Configuration::get('GESTPAY_TEST_MODE')),
            'gestpay_language' => pSQL(Configuration::get('GESTPAY_LANGUAGE')),
            'gestpay_s2s' => pSQL(Configuration::get('GESTPAY_S2S')),
            'gestpay_s2s_auto' => (pSQL(Configuration::get('GESTPAY_S2S')) ?
                                   pSQL(Configuration::get('GESTPAY_S2S_AUTO')) : 0),
            'gestpay_s2s_auto_enabled' => pSQL(Configuration::get('GESTPAY_S2S')),
            'gestpay_iframe' => pSQL(Configuration::get('GESTPAY_IFRAME')),
            'gestpay_token' => (pSQL(Configuration::get('GESTPAY_IFRAME')) ?
                                pSQL(Configuration::get('GESTPAY_TOKEN')) : 0),
            'gestpay_token_enabled' => pSQL(Configuration::get('GESTPAY_IFRAME')),
            'gestpay_remove_table' => pSQL(Configuration::get('GESTPAY_REMOVE_TABLE')),
            'gestpay_tls_endpoint' => pSQL(Configuration::get('GESTPAY_TLS_ENDPOINT')),
            'gestpay_debug' => pSQL(Configuration::get('GESTPAY_DEBUG')),
            'gestpay_apikey' => pSQL(Configuration::get('GESTPAY_APIKEY')),
            'gestpay_list_methods' => pSQL(Configuration::get('GESTPAY_LIST_METHODS')),
            'list' => $list,
            'module_version' => $this->version,
            'logs' => nl2br($logs)
        ));
        $display = $this->display(__FILE__, 'views/templates/admin/configuration.tpl');
        return $display;
    }

    /**
    * Test configuration for common issues
    *
    * @return array Return array of messages (warnings)
    */
    public function testConfig()
    {
        $status = true;
        $messages = array();
    
        if (extension_loaded('suhosin')) {
            if (ini_get('suhosin.get.max_totalname_length') < 1024 || ini_get('suhosin.get.max_value_length') < 1024) {
                $status = false;
                $messages[] = $this->l('PHP module Suhosin is installed, 
                                        please set get.max_totalname_length and 
                                        get.max_value_length values to 1024 or more.');
            }
        }
        if (extension_loaded('curl')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://sandbox.gestpay.net/pagam/javascript/TLSCHK_TE.js");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            if (curl_exec($ch) === false) {
                $status = false;
                $messages[] = $this->l('TLS 1.2 is not supported by this server.');
            }
            curl_close($ch);
        } else {
            $status = false;
            $messages[] = $this->l('PHP module cURL is not installed.');
        }
        
        
        if (!count(Currency::checkPaymentCurrencies($this->id))) {
            $status = false;
            $messages[] = $this->l('No currency has been set for this module.');
        }
        return array('status'=>$status, 'messages'=>implode($messages, "\n"));
    }

   
    public function checkCurrency($cart)
    {
        $currency_order = new Currency((int)$cart->id_currency);
        $currencies_module = $this->getCurrency((int)$cart->id_currency);
        if (is_array($currencies_module)) {
            foreach ($currencies_module as $currency_module) {
                if ($currency_order->id == $currency_module['id_currency']) {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
    * Get transactions table contents
    * @return html
    */
    protected function getTransactionTable()
    {
        $actions = array();

        $fields_list = array(
            'id_transaction' => array(
                'title' => $this->l('ID'),
                'type' => 'text',
                'class' => 'fixed-width-xs'),
            'id_order' => array(
                'title' => $this->l('Order'),
                'type' => 'text',
                'class' => 'fixed-width-xs'),
            'currency' => array(
                'title' => $this->l('Currency'),
                'type' => 'text',
                'class' => 'fixed-width-xs'),
            'amount' => array(
                'title' => $this->l('Amount'),
                'type' => 'decimal',
                'class' => 'fixed-width-xs'),
            'test_mode' => array(
                'title' => $this->l('Is test?'),
                'type'  => 'bool',
                'class' => 'fixed-width-xs'),
            'shoptransactionid' => array(
                    'title' => $this->l('Shop ID'),
                    'type' => 'text',
                    'class' => 'fixed-width-xs'),
            'paymentmethod' => array(
                'title' => $this->l('Payment Method'),
                'type' => 'text',
                'class' => 'fixed-width-xs'),
            'transactionresult' => array(
                'title' => $this->l('Result'),
                'type' => 'text',
                'class' => 'fixed-width-xs'),
            'authorizationcode' => array(
                'title' => $this->l('Auth'),
                'type' => 'text',
                'class' => 'fixed-width-xs'),
            'banktransactionid' => array(
                'title' => $this->l('Bank ID'),
                'type' => 'text',
                'class' => 'fixed-width-xs'),
            'errorcode' => array(
                'title' => $this->l('Error'),
                'type' => 'text',
                'class' => 'fixed-width-xs'),
            'errordescription' => array(
                'title' => $this->l('Info'),
                'type' => 'text',
                'class' => 'fixed-width-xs'),
            'payment_date' => array(
                'title' => $this->l('Date'),
                'type'  => 'datetime',
                'class' => 'fixed-width-xs')
            );
        
        $toolbar_btn = array(
            'dummy' =>
            array(
                'href' => AdminController::$currentIndex.'$current_view&configure='.
                          $this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );
        $id_shop = Shop::getContextShopID();
        $l = new GestPayListManager(
            'GestPayTransaction',
            $this->l('Transaction list'),
            $fields_list,
            $actions,
            '`id_shop` = ' .  (int)$id_shop,
            $toolbar_btn
        );
        return $l->renderList();
    }
}
