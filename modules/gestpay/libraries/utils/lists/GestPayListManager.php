<?php
/*
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2018 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class GestPayListManager
{
    protected $fields_list;
    protected $actions;
    protected $caption;
    protected $context;
    protected $table;
    protected $identifier;
    protected $base_filter;
    protected $toolbar_btn;

    public $name = '';

    public function __construct($class_name, $caption, $fields_list, $actions = array(), $base_filter = '', $toolbar_btn = array())
    {
        $def = ObjectModel::getDefinition($class_name);
        $this->caption = $caption;
        $this->table = $def['table'];
        $this->identifier = $def['primary'];
        $this->fields_list = $fields_list;
        $this->actions = $actions;
        $this->context = Context::getContext();
        $this->base_filter = $base_filter;
        $this->toolbar_btn = $toolbar_btn;
    }

    public function renderList()
    {
        $content = $this->getListContent();

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->actions = $this->actions;
        $helper->simple_header = false;
        $helper->show_toolbar = true;
        $helper->show_filters = true;
        $helper->toolbar_btn = true;
        $helper->module = $this;
        $helper->toolbar_scroll = true;
        $helper->listTotal = $this->getListCount();
        $helper->identifier = $this->identifier;
        $helper->title = $this->caption;
        $helper->table = $this->table;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->orderBy = $this->context->cookie->{$this->table.'Orderby'};
        $helper->orderWay = $this->context->cookie->{$this->table.'Orderway'};
        $helper->currentIndex = AdminController::$currentIndex.'&configure=gestpay&current_view=log_table';
        $helper->toolbar_btn = $this->toolbar_btn;

        return $helper->generateList($content, $this->fields_list);
    }
    protected function getListContent()
    {
        $dbquery = new DbQuery();
        $select = '';
        foreach ($this->fields_list as $field_name => $field_settings) {
            if (strlen($select) > 0) {
                $select .= ',';
            }
            if (isset($field_settings['sql']) && strlen($field_settings['sql']) > 0) {
                $select .= '(' . $field_settings['sql'] . ') AS ' . $field_name;
            } else {
                $select .= "`$field_name`";
            }
        }
        $dbquery->select($select);
        $dbquery->from($this->table);
        /* ordering */
        if (Tools::getValue($this->table . 'Orderby') &&
            Validate::isOrderBy(Tools::getValue($this->table.'Orderby')) &&
            Tools::getValue($this->table . 'Orderway') &&
            Validate::isOrderWay(Tools::getValue($this->table.'Orderway'))) {            
            $this->context->cookie->{$this->table.'Orderby'} = Tools::getValue($this->table.'Orderby');
            $this->context->cookie->{$this->table.'Orderway'} = Tools::strtoupper(Tools::getValue($this->table.'Orderway'));
        }
        if (isset($this->context->cookie->{$this->table.'Orderby'}) && isset($this->context->cookie->{$this->table.'Orderway'})) {
            $dbquery->orderBy($this->context->cookie->{$this->table.'Orderby'} . ' ' . $this->context->cookie->{$this->table.'Orderway'});
        }
        /* search */
        foreach ($this->getSearch() as $condition) {
            $dbquery->where($condition);
        }
        /* Paginate the result */
        $page = ($page = Tools::getValue('submitFilter'.$this->table)) ? $page : 1;
        if (Tools::getValue($this->table.'_pagination')) {
            $this->context->cookie->{$this->table.'_pagination'} = pSQL(Tools::getValue($this->table.'_pagination'));
        }
        $pagination = ($pagination = $this->context->cookie->{$this->table.'_pagination'}) ? $pagination : 50;
        $dbquery->limit($pagination, ($page - 1) * $pagination);
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbquery->build());
    }
    protected function getListCount()
    {
        $dbquery = new DbQuery();
        $dbquery->select('COUNT(*)');
        $dbquery->from($this->table);
        foreach ($this->getSearch() as $condition) {
            $dbquery->where($condition);
        }
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($dbquery->build());
    }

    protected function getSearch()
    {
        // submitReset
        if (Tools::isSubmit('submitReset'.$this->table)) {
            foreach ($this->fields_list as $field_name => $field_settings) {
                unset($this->context->cookie->{$this->table . 'Filter_' . $field_name});
            }
            $_POST = array();
        }
        $where = array();
        if ($this->base_filter != '') {
            $where[] = $this->base_filter;
        }
        foreach ($this->fields_list as $field_name => $field_settings) {
            if ($value = Tools::getValue($this->table . 'Filter_' . $field_name)) {
                if ($value === '') {
                    unset($this->context->cookie->{$this->table . 'Filter_' . $field_name});
                } else {
                    $this->context->cookie->{$this->table . 'Filter_' . $field_name} = !is_array($value) ? $value : Tools::jsonEncode($value);
                }
            }
            $value = $this->context->cookie->{$this->table . 'Filter_' . $field_name};
            if (($field_settings['type'] == 'date' || $field_settings['type'] == 'datetime') && is_string($value)) {
                $value = Tools::jsonDecode($value);
            }
            switch ($field_settings['type']) {
                case 'bool':
                if (isset($value) && !empty($value)) {
                    $where[] = "`$field_name` = '" . pSQL($value) ."'";
                }
                break;
                case 'text':
                if (isset($value) && !empty($value)) {
                    $where[] = "`$field_name` LIKE '%" . pSQL($value) .'%\'';
                }
                break;
                case 'date':
                case 'datetime':
                if (is_array($value)) {
                    if (isset($value[0]) && !empty($value[0])) {
                        if (!Validate::isDate($value[0])) {
                            $this->errors[] = Tools::displayError('The \'From\' date format is invalid (YYYY-MM-DD)');
                        } else {
                            $where[] = "`$field_name` >= '" . pSQL($value[0]) . "'";
                        }
                    }
                    if (isset($value[1]) && !empty($value[1])) {
                        if (!Validate::isDate($value[1])) {
                            $this->errors[] = Tools::displayError('The \'From\' date format is invalid (YYYY-MM-DD)');
                        } else {
                            $where[] = "`$field_name` <= '" . pSQL($value[1]) . "'";
                        }
                    }
                }
                break;
            }
        }
        return $where;
    }
}
