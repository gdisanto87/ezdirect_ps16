<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2018 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

if(!class_exists('nusoap_base')){
    require_once 'utils/soap/Nusoap.php';
}
require_once 'utils/lists/GestPayListManager.php';
require_once 'classes/GestPayDecryptRequest.php';
require_once 'classes/GestPayDeleteRequest.php';
require_once 'classes/GestPayEncryptRequest.php';
require_once 'classes/GestPayPagamRequest.php';
require_once 'classes/GestPayReadTrxRequest.php';
require_once 'classes/GestPayRefundRequest.php';
require_once 'classes/GestPaySettleRequest.php';
require_once 'classes/GestPayToken.php';
require_once 'classes/GestPayTransaction.php';
require_once 'classes/GestPayS2S.php';

class GestPayAPI extends Module
{
    protected $soapClientEncrypt;
    protected $soapClientS2S;
    protected $javaScriptUrl;
    protected $authUrl;
    protected $formUrl;
    protected $platform;
    protected $language;
    protected $s2s;
    protected $tls;
    protected $restUrl;
    protected $apikey;
    protected $debugLogFile;

    public $shopLogin;
    public $testMode;
    public $debug;

    public function __construct($shopLogin, $platform = 'starter', $testMode = true, $language = false, $s2s = false, $debug = true, $tls = true, $debugLogFile = '', $apikey = '')
    {
        $this->shopLogin = $shopLogin;
        $this->platform = $platform;
        $this->testMode = $testMode;
        $this->language = $language;
        $this->s2s = $s2s;        
        $this->debug = $debug;
        $this->tls = $tls;
        $this->debugLogFile = $debugLogFile;
        $this->apikey = $apikey;
        $this->name = 'gestpay';
        if ($testMode) {
            $this->formUrl = 'https://sandbox.gestpay.net/pagam/pagam.aspx';
            if ($tls) {
                $this->soapClientEncrypt = new nusoap_client('https://sandbox.gestpay.net/gestpay/GestPayWS/WsCryptDecrypt.asmx?wsdl', true);
            } else {
                $this->soapClientEncrypt = new nusoap_client('https://sandbox.gestpay.net/crypthttp/WSCryptDecrypt.asmx?WSDL', true);
            }
            $this->soapClientS2S = new nusoap_client('https://sandbox.gestpay.net/gestpay/gestpayws/WSs2s.asmx?WSDL', true);
            $this->javaScriptUrl = 'https://sandbox.gestpay.net/Pagam/JavaScript/js_GestPay.js';
            $this->authUrl = 'https://sandbox.gestpay.net/pagam/pagam3d.aspx';
            $this->restUrl = 'https://sandbox.gestpay.net/api/v1/';
        } else {
            $this->formUrl = 'https://ecomm.sella.it/pagam/pagam.aspx';
            if ($tls) {
                $this->soapClientEncrypt = new nusoap_client('https://ecomms2s.sella.it/gestpay/gestpayws/WSCryptDecrypt.asmx?WSDL', true);
            } else {
                $this->soapClientEncrypt = new nusoap_client('https://ecomms2s.sella.it/crypthttp/WSCryptDecrypt.asmx?WSDL', true);
            }
            $this->soapClientS2S = new nusoap_client('https://ecomms2s.sella.it/gestpay/gestpayws/WSs2s.asmx?WSDL', true);
            $this->javaScriptUrl = 'https://ecomm.sella.it/Pagam/JavaScript/js_GestPay.js';
            $this->authUrl = 'https://ecomm.sella.it/pagam/pagam3d.aspx';
            $this->restUrl = 'https://ecomms2s.sella.it/api/v1/';
        }
        $this->soapClientEncrypt->decode_utf8 = false;
    }

    /**
    * encrypt
    * @return Object
    */
    public function encrypt(GestPayEncryptRequest $request)
    {
        $params = array(self::objectToArray($request));
        $result = $this->soapClientEncrypt->call('Encrypt', $params);
        $soapError = $this->soapClientEncrypt->getError();
        if (!$soapError) {
            return self::arrayToObject($result['EncryptResult']['GestPayCryptDecrypt']);
        } else {
            if ($this->debug) {
                self::debugLog(__CLASS__.'->'.__FUNCTION__, $soapError);
            }
            return false;
        }
    }

    /**
    * decrypt
    * @return Object
    */
    public function decrypt(GestPayDecryptRequest $request)
    {
        $params = array(self::objectToArray($request));
        $result = $this->soapClientEncrypt->call('Decrypt', $params);
        $soapError = $this->soapClientEncrypt->getError();
        if (!$soapError) {
            return self::arrayToObject($result['DecryptResult']['GestPayCryptDecrypt']);
        } else {
            if ($this->debug) {
                self::debugLog(__FUNCTION__, $soapError);
            }
            return false;
        }
    }

    /**
    * Pagam
    * @return Object
    */
    public function pagam(GestPayPagamRequest $request)
    {
        $params = array(self::objectToArray($request));
        $result = $this->soapClientS2S->call('callPagamS2S', $params);
        $soapError = $this->soapClientS2S->getError();
        if (!$soapError) {
            return self::arrayToObject($result['callPagamS2SResult']['GestPayS2S']);
        } else {
            if ($this->debug) {
                self::debugLog(__CLASS__.'->'.__FUNCTION__, $soapError);
            }
            return false;
        }
    }

    /**
    * ReadTrx
    * @return Object
    */
    public function readTrx(GestPayReadTrxRequest $request)
    {
        $params = array(self::objectToArray($request));
        $result = $this->soapClientS2S->call('callReadTrxS2S', $params);
        $soapError = $this->soapClientS2S->getError();
        if (!$soapError) {
            return self::arrayToObject($result['callReadTrxS2SResult']['GestPayS2S']);
        } else {
            if ($this->debug) {
                self::debugLog(__CLASS__.'->'.__FUNCTION__, $soapError);
            }
            return false;
        }
    }
    /**
    * Get current transaction total refundable
    */
    public function getTotalRefundable($readTrxResponse)
    {
        $total = 0;
        $events = array();
        if (!is_array($readTrxResponse->Events->event)) {
            $events[] = $readTrxResponse->Events->event;
        } else {
            $events = $readTrxResponse->Events->event;
        }
        foreach ($events as $event) {
            if ($event->eventtype == 'MOV') {
                $total += (float) $event->eventamount;
            } elseif ($event->eventtype == 'STO') {
                $total -= (float) $event->eventamount;
            }
        }
        return $total;
    }
    /**
    * Get current transaction total refundable
    */
    public function getDecodedEvents($readTrxResponse)
    {
        $events = array();
        $history = array();
        if (!is_array($readTrxResponse->Events->event)) {
            $events[] = $readTrxResponse->Events->event;
        } else {
            $events = $readTrxResponse->Events->event;
        }
        foreach ($events as $event) {
            switch ($event->eventtype) {
                case 'AUT':
                $event->eventtype = $this->l('Authorized', 'gestpayapi');
                break;
                case 'CAN':
                $event->eventtype = $this->l('Canceled', 'gestpayapi');
                break;
                case 'MOV':
                $event->eventtype = $this->l('Settled', 'gestpayapi');
                break;
                case 'STO':
                $event->eventtype = $this->l('Refunded', 'gestpayapi');
                break;
            }
            $history[] = $event;
        }
        return $history;
    }

    /**
    * Settle
    * @return Object
    */
    public function settle(GestPaySettleRequest $request)
    {
        $params = array(self::objectToArray($request));
        $result = $this->soapClientS2S->call('callSettleS2S', $params);
        $soapError = $this->soapClientS2S->getError();
        if (!$soapError) {
            return self::arrayToObject($result['callSettleS2SResult']['GestPayS2S']);
        } else {
            if ($this->debug) {
                self::debugLog(__CLASS__.'->'.__FUNCTION__, $soapError);
            }
            return false;
        }
    }
    
    /**
    * Delete
    * @return Object
    */
    public function delete(GestPayDeleteRequest $request)
    {
        $params = array(self::objectToArray($request));
        $result = $this->soapClientS2S->call('callDeleteS2S', $params);
        $soapError = $this->soapClientS2S->getError();
        if (!$soapError) {
            return self::arrayToObject($result['callDeleteS2SResult']['GestPayS2S']);
        } else {
            if ($this->debug) {
                self::debugLog(__CLASS__.'->'.__FUNCTION__, $soapError);
            }
            return false;
        }
    }
    
    /**
    * Refund
    * @return Object
    */
    public function refund(GestPayRefundRequest $request)
    {
        $params = array(self::objectToArray($request));
        $result = $this->soapClientS2S->call('callRefundS2S', $params);
        $soapError = $this->soapClientS2S->getError();
        if (!$soapError) {
            return self::arrayToObject($result['callRefundS2SResult']['GestPayS2S']);
        } else {
            if ($this->debug) {
                self::debugLog(__CLASS__.'->'.__FUNCTION__, $soapError);
            }
            return false;
        }
    }

    /**
    * getRedirectUrl
    * @return string
    */
    public function getRedirectUrl($url = true, $request_token = false, $payment_types = null)
    {
        $context = Context::getContext();
        $cart = $context->cart;
        $id_cart = (int)$cart->id;
        if ($id_cart == 0) {
            if ($this->debug) {
                self::debugLog(__FUNCTION__, "Cart ID ($id_cart) is missing");
            }
            return false;
        }
        /* get customer data */
        $customer = new Customer((int)$cart->id_customer);
        $buyerName = preg_replace("/[^A-Za-z ]/", '', Tools::ucfirst(Tools::strtolower($customer->firstname)) . ' ' . Tools::ucfirst(Tools::strtolower($customer->lastname)));
        $buyerEmail = $customer->email;
        /* currency */
        $currency = new Currency((int)$cart->id_currency);
        $uicCode = self::GetUicCode($currency->iso_code_num);
        /* Set amount, note that Yen has no decimals */
        if ($currency->iso_code_num == 392) {
            $amount = number_format($cart->getOrderTotal(true, Cart::BOTH), 0, '.', '');
        } else {
            $amount = number_format($cart->getOrderTotal(true, Cart::BOTH), 2, '.', '');
        }
        $shopTransactionId = 'GP_' . $id_cart;
        $request = new GestPayEncryptRequest($this->shopLogin, $uicCode, $amount, $shopTransactionId, $payment_types);

        if ($this->platform != 'starter') {
            $request->buyerName = $buyerName;
            $request->buyerEmail = $buyerEmail;
            if ($this->language) {
                $request->languageId = self::GetLanguageId($context->language->iso_code);
            }
        }
        if ($request_token) {
            $request->requestToken = 'MASKEDPAN';
        }
        // 3DS 2.0 additional fields        
        if ($transDetails = $this->getTransDetails($context)) {
            $request->transDetails = $transDetails;
        }
        $response = $this->encrypt($request);
        if ($response) {
            if ($response->ErrorCode == '0' && $response->TransactionResult == 'OK') {
                $transaction = GestPayTransaction::fetchByShopTransactionId($shopTransactionId);
                if (!$transaction) {
                    $transaction = new GestPayTransaction();
                }
                $transaction->id_order = -1;
                $transaction->id_cart = (int)$id_cart;
                $transaction->id_customer = (int)$context->customer->id;
                $transaction->id_language = (int)$context->language->id;
                $transaction->id_shop = (int)$context->shop->id;
                $transaction->id_currency =(int)$context->currency->id;
                $transaction->id_country = (int)$context->country->id;
                $transaction->currency = pSQL($uicCode);
                $transaction->amount = pSQL($amount);
                $transaction->test_mode = $this->testMode;
                $transaction->shoptransactionid = pSQL($shopTransactionId);
                $transaction->payment_date = date('Y-m-d H:i:s');
                $transaction->last_update = date('Y-m-d H:i:s');
                $transaction->save();
                if ($url) {
                    return $this->formUrl . "?a={$this->shopLogin}&b={$response->CryptDecryptString}";
                } else {
                    return array('formUrl' => $this->formUrl,
                                 'shopLogin' => $this->shopLogin,
                                 'encString' => $response->CryptDecryptString,
                                 'buyerName' => $buyerName,
                                 'buyerEmail' => $buyerEmail);
                }
            } else {
                if ($this->debug) {
                    self::debugLog(__FUNCTION__, " Error Code: {$response->ErrorCode} - Error Description: {$response->ErrorDescription}");
                }
                return false;
            }
        } else {
            return false;
        }
    }

    /**
    * Get transDetails elements for 3DS 2.0
    * this is a complex object defined as stdClass
    * @return string
    */
    public function getTransDetails($context)
    {        
        $customer = new Customer((int)$context->cart->id_customer);
        if (!Validate::isLoadedObject($customer)) {
            return false;
        }
        $billing_address = new Address((int)$context->cart->id_address_invoice);
        if (!Validate::isLoadedObject($billing_address)) {
            return false;
        }
        $shipping_address = new Address((int)$context->cart->id_address_delivery);
        if (!Validate::isLoadedObject($shipping_address)) {
            return false;
        }
        $today = new Datetime();
        $transDetails = new stdClass();
        $transDetails->type = 'EC';
        $transDetails->threeDsContainer = new stdClass();
        /* threeDsContainer */
        $transDetails->threeDsContainer->transTypeReq = 'P';
        $transDetails->threeDsContainer->acquirerBIN = null;
        $transDetails->threeDsContainer->acquirerMerchantID = null;
        $transDetails->threeDsContainer->exemption = '';
        /* buyerDetails */
        $transDetails->threeDsContainer->buyerDetails = new stdClass();
        /* profileDetails */
        $transDetails->threeDsContainer->buyerDetails->profileDetails = new stdClass();
        $transDetails->threeDsContainer->buyerDetails->profileDetails->cardholderID = null;
        $transDetails->threeDsContainer->buyerDetails->profileDetails->authData = null;
        $transDetails->threeDsContainer->buyerDetails->profileDetails->authMethod = null;
        $transDetails->threeDsContainer->buyerDetails->profileDetails->authTimeStamp = null;
        /* billingAddress */        
        $transDetails->threeDsContainer->buyerDetails->billingAddress = new stdClass();
        $transDetails->threeDsContainer->buyerDetails->billingAddress->city = $billing_address->city;
        $country = new Country($billing_address->id_country);
        $transDetails->threeDsContainer->buyerDetails->billingAddress->country = $country->iso_code;
        $transDetails->threeDsContainer->buyerDetails->billingAddress->line1 = substr($billing_address->address1,0,50);
        $transDetails->threeDsContainer->buyerDetails->billingAddress->line2 = substr($billing_address->address2,0,50);
        $transDetails->threeDsContainer->buyerDetails->billingAddress->line3 = '';
        $transDetails->threeDsContainer->buyerDetails->billingAddress->postCode = $billing_address->postcode;
        if ($billing_address->id_state) {
            $state = new State($billing_address->id_state);
            $transDetails->threeDsContainer->buyerDetails->billingAddress->state = $state->name;
        } else {
            $transDetails->threeDsContainer->buyerDetails->billingAddress->state = null;
        }       
        /* shippingAddress */        
        $transDetails->threeDsContainer->buyerDetails->shippingAddress = new stdClass();
        $transDetails->threeDsContainer->buyerDetails->shippingAddress->city = $shipping_address->city;
        $country = new Country($shipping_address->id_country);
        $transDetails->threeDsContainer->buyerDetails->shippingAddress->country = $country->iso_code;
        $transDetails->threeDsContainer->buyerDetails->shippingAddress->line1 = substr($shipping_address->address1,0,50);
        $transDetails->threeDsContainer->buyerDetails->shippingAddress->line2 = substr($shipping_address->address2,0,50);
        $transDetails->threeDsContainer->buyerDetails->shippingAddress->line3 = '';
        $transDetails->threeDsContainer->buyerDetails->shippingAddress->postCode = $shipping_address->postcode;
        if ($shipping_address->id_state) {
            $state = new State($shipping_address->id_state);
            $transDetails->threeDsContainer->buyerDetails->shippingAddress->state = $state->name;
        } else {
            $transDetails->threeDsContainer->buyerDetails->shippingAddress->state = null;
        }
        $transDetails->threeDsContainer->addrMatch = ($context->cart->id_address_delivery == $context->cart->id_address_invoice ? 'Y' : 'N');
        /* cardholder */        
        $transDetails->threeDsContainer->cardholder = new stdClass();
        $transDetails->threeDsContainer->cardholder->name = preg_replace("/[^A-Za-z ]/", '', Tools::ucfirst(Tools::strtolower($customer->firstname)) . ' ' . Tools::ucfirst(Tools::strtolower($customer->lastname)));
        $transDetails->threeDsContainer->cardholder->email = $customer->email;
        $transDetails->threeDsContainer->cardholder->homePhone_cc = null;
        $transDetails->threeDsContainer->cardholder->homePhone_num = $billing_address->phone;
        $transDetails->threeDsContainer->cardholder->mobilePhone_cc = null;
        $transDetails->threeDsContainer->cardholder->mobilePhone_num = $billing_address->phone_mobile;
        $transDetails->threeDsContainer->cardholder->workPhone_cc = null;
        $transDetails->threeDsContainer->cardholder->workPhone_num = null;
        /* accInfo */
        $stats = $customer->getStats();
        $transDetails->threeDsContainer->accInfo = new stdClass();
        try {
            $date_add = new DateTime($customer->date_add);
        } catch (Exception $e) {
            return false;
        }        
        $interval_add = $date_add->diff($today, true);
        if ($interval_add->days = 0) {
            $transDetails->threeDsContainer->accInfo->chAccAgeInd = '02';
        } elseif ($interval_add->days > 0 && $interval_add->days < 30) {
            $transDetails->threeDsContainer->accInfo->chAccAgeInd = '03';
        } elseif ($interval_add->days >= 30 && $interval_add->days < 60) {
            $transDetails->threeDsContainer->accInfo->chAccAgeInd = '04';
        } else {
            $transDetails->threeDsContainer->accInfo->chAccAgeInd = '05';
        }
        try {
            $date_upd = new DateTime($customer->date_upd);
        } catch (Exception $e) {
            return false;
        }        
        $interval_upd = $date_upd->diff($today, true);       
        $transDetails->threeDsContainer->accInfo->chAccChange = $date_upd->format('Ymd');
        if ($interval_upd->days = 0) {
            $transDetails->threeDsContainer->accInfo->chAccChangeInd = '01';
        } elseif ($interval_upd->days > 0 && $interval_upd->days < 30) {
            $transDetails->threeDsContainer->accInfo->chAccChangeInd = '02';
        } elseif ($interval_upd->days >= 30 && $interval_upd->days < 60) {
            $transDetails->threeDsContainer->accInfo->chAccChangeInd = '03';
        } else {
            $transDetails->threeDsContainer->accInfo->chAccChangeInd = '04';
        }
        $transDetails->threeDsContainer->accInfo->chAccDate = $date_add->format('Ymd');
        try {
            $last_passwd_gen = new DateTime($customer->last_passwd_gen);
        } catch (Exception $e) {
            return false;
        }        
        $interval_last_passwd_gen = $last_passwd_gen->diff($today, true);
        $transDetails->threeDsContainer->accInfo->chAccPwChange = $last_passwd_gen->format('Ymd');
        if ($interval_last_passwd_gen->days = 0) {
            $transDetails->threeDsContainer->accInfo->chAccPwChangeInd = '01';
        } elseif ($interval_last_passwd_gen->days > 0 && $interval_last_passwd_gen->days < 30) {
            $transDetails->threeDsContainer->accInfo->chAccPwChangeInd = '02';
        } elseif ($interval_last_passwd_gen->days >= 30 && $interval_last_passwd_gen->days < 60) {
            $transDetails->threeDsContainer->accInfo->chAccPwChangeInd = '03';
        } else {
            $transDetails->threeDsContainer->accInfo->chAccPwChangeInd = '04';
        }
        $transDetails->threeDsContainer->accInfo->nbPurchaseAccount = $stats['nb_orders'];
        $transDetails->threeDsContainer->accInfo->provisionAttemptsDays = null;
        $transDetails->threeDsContainer->accInfo->txnActivityDay = null;
        $transDetails->threeDsContainer->accInfo->txnActivityYear = null;
        $transDetails->threeDsContainer->accInfo->paymentAccAge = null; 
        $transDetails->threeDsContainer->accInfo->paymentAccInd = null;
        try {
            $date_add_ship = new DateTime($shipping_address->date_add);
        } catch (Exception $e) {
            return false;
        }        
        $interval_date_add_ship = $date_add_ship->diff($today, true);
        $transDetails->threeDsContainer->accInfo->shipAddressUsage = $date_add_ship->format('Ymd');
        if ($interval_date_add_ship->days = 0) {
            $transDetails->threeDsContainer->accInfo->shipAddressUsageInd = '01';
        } elseif ($interval_date_add_ship->days > 0 && $interval_date_add_ship->days < 30) {
            $transDetails->threeDsContainer->accInfo->shipAddressUsageInd = '02';
        } elseif ($interval_date_add_ship->days >= 30 && $interval_date_add_ship->days < 60) {
            $transDetails->threeDsContainer->accInfo->shipAddressUsageInd = '03';
        } else {
            $transDetails->threeDsContainer->accInfo->shipAddressUsageInd = '04';
        }
        $transDetails->threeDsContainer->accInfo->shipNameIndicator = null;
        $transDetails->threeDsContainer->accInfo->suspiciousAccActivity = null;
        /* merchantRiskIndicator */
        $transDetails->threeDsContainer->merchantRiskIndicator = new stdClass();
        $transDetails->threeDsContainer->merchantRiskIndicator->deliveryEmailAddress = null;
        $transDetails->threeDsContainer->merchantRiskIndicator->deliveryTimeframe = null;
        $transDetails->threeDsContainer->merchantRiskIndicator->giftCardAmount = null;
        $transDetails->threeDsContainer->merchantRiskIndicator->giftCardCount = null;
        $transDetails->threeDsContainer->merchantRiskIndicator->giftCardCurr = null;
        $transDetails->threeDsContainer->merchantRiskIndicator->preOrderDate = null;
        $transDetails->threeDsContainer->merchantRiskIndicator->preOrderPurchaseInd = null;
        $transDetails->threeDsContainer->merchantRiskIndicator->reorderItemsInd = null;
        $transDetails->threeDsContainer->merchantRiskIndicator->shipIndicator = null;
        /* recurringTransaction */
        $transDetails->recurringTransaction = new stdClass();
        $transDetails->recurringTransaction->installNo = null;
        $transDetails->recurringTransaction->expiry = null;
        $transDetails->recurringTransaction->frequency = null;
        /* previousTransDetails */
        $transDetails->previousTransDetails = new stdClass();     
        $transDetails->previousTransDetails->bankTransactionID = null;
        return $transDetails;
    }

    /**
    * getAuthUrl
    * @return array of objects
    */
    public function getPaymentMethods()
    {
        $methods = array();
        if (strlen($this->apikey) > 0 && strlen($this->shopLogin) > 0 && $this->platform <> 'starter') {
            $url = $this->restUrl . 'shop/methods/' . $this->shopLogin;
            $headers = array('Authorization: apikey ' . $this->apikey,'Content-Type: application/json');
            if ($json = $this->getRestResponse($url, $headers)) {
                $response = json_decode($json);                
                if ($response->error->code == 0) {
                    foreach($response->payload->paymentMethod as $paymentMethod) {
                        if ($paymentMethod->paymentPage == "visible") {
                            $methods[$paymentMethod->paymentType] = $paymentMethod;
                        }
                    }
                    if (count($methods) > 0) {
                        return $methods;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
    * getJavaScriptUrl
    * @return string
    */
    public function getJavaScriptUrl()
    {
        return $this->javaScriptUrl;
    }

    /**
    * getAuthUrl
    * @return string
    */
    public function getAuthUrl()
    {
        return $this->authUrl;
    }

    /**
    * addOrderMessage
    * @return array
    */
    public function addOrderMessage($id_order, $msg)
    {
        $message = new Message();
        $msg = strip_tags($msg, '<br>');
        if (Validate::isCleanHtml($msg)) {
            $message->message = $msg;
            $message->id_order = (int) $id_order;
            $message->private = true;
            return $message->add();
        } else {
            return false;
        }
    }

    /**
    * testConnection
    * @return array
    */
    public function testConnection()
    {
        $status = true;
        $messages = array();
        /* test encrypt service */
        $shopTransactionId = 'TEST';
        $uicCode = 242;
        $amount = 0.01;
        $request = new GestPayEncryptRequest($this->shopLogin, $uicCode, $amount, $shopTransactionId);
        if ($this->platform != 'starter') {
            $request->buyerName = 'TEST';
            $request->buyerEmail = 'test@test.com';
            if ($this->language) {
                $request->languageId = 2;
            }
        }
        $response = $this->encrypt($request);
        if ($response->ErrorCode != '0') {
            $status = false;
            $messages[] = $this->l('Error in Payment API. The error is: ', 'gestpayapi') . $response->ErrorCode . ' - ' . $response->ErrorDescription;
        }
        /* test XML service */
        if ($this->s2s) {
            $request = new GestPayReadTrxRequest($this->shopLogin, 'TEST');
            $response = $this->readTrx($request);
            if ($response->ErrorCode != '2011') {
                $status = false;
                $messages[] = $this->l('Error in Sales API. The error is: ', 'gestpayapi') . $response->ErrorCode . ' - ' . $response->ErrorDescription;
            }
        }
        if (empty($messages)) {
            $messages[] = $this->l('Success! Connection established with Axerve with given details', 'gestpayapi');
        }
        return array('status'=>$status,'messages'=>implode($messages, "\n"));
    }
    /**
    * Check if Sales API is available
    */
    public function isSalesAPIAvailable()
    {
        return $this->s2s;
    }
    /**
    * Transaction data to text
    */
    public function transactionToText(GestPayTransaction $transaction)
    {
        $text = $this->l('Axerve Transaction informations:', 'gestpayapi') . PHP_EOL;
        $text .= $this->l('Shop Transaction ID:', 'gestpayapi') . $transaction->shoptransactionid . PHP_EOL ;
        $text .= $this->l('Transaction Result:', 'gestpayapi') . $transaction->transactionresult . PHP_EOL ;
        $text .= $this->l('Bank Transaction ID:', 'gestpayapi') . $transaction->banktransactionid . PHP_EOL;
        $text .= $this->l('Amount:', 'gestpayapi') . $transaction->amount . PHP_EOL;
        $text .= $this->l('Payment Method:', 'gestpayapi') . $transaction->paymentmethod . PHP_EOL;
        return $text;
    }
    /**
    * Get list of available Gestpay platforms
    * @return array
    */
    public static function getPlatforms()
    {
        return array('starter' => 'Starter',
        'professional' => 'Professional',
        'unlimited' => 'Unlimited');
    }

    /**
    * Get gestpay currency code
    * @return integer
    */
    public static function GetUicCode($iso_code_num)
    {
        $currency_list = array(784 => 187,  //United Arab Emirates dirham
                               36  => 109,  //Australian dollar
                               975 => 262,  //Bulgarian lev
                               986 => 234,  //Brazilian real
                               124 => 12,   //Canadian dollar
                               756 => 3,    //Swiss franc
                               156 => 144,  //Chinese yuan
                               203 => 223,  //Czech koruna
                               208 => 7,    //Danish krone
                               978 => 242,  //Euro
                               826 => 2,    //Pound sterling
                               344 => 103,  //Hong Kong dollar
                               191 => 229,  //Croatian kuna
                               348 => 153,  //Hungarian forint
                               360 => 123,  //Indonesian rupiah
                               376 => 203,  //Israeli new shekel
                               356 => 31,   //Indian rupee
                               352 => 62,   //Icelandic króna
                               392 => 71,   //Japanese yen
                               410 => 119,  //South Korean won
                               458 => 55,   //Malaysian ringgit
                               566 => 81,   //Nigerian naira
                               578 => 8,    //Norwegian krone
                               554 => 113,  //New Zealand dollar
                               608 => 66,   //Philippine peso
                               586 => 26,   //Pakistani rupee
                               985 => 237,  //Polish zloty
                               946 => 270,  //Romanian leu
                               643 => 244,  //Russian ruble
                               682 => 75,   //Saudi riyal
                               752 => 9,    //Swedish krona/kronor
                               702 => 124,  //Singapore dollar
                               764 => 73,   //Thai baht
                               949 => 267,  //Turkish lira
                               901 => 143,  //New Taiwan dollar
                               980 => 241,  //Ukrainian hryvnia
                               840 => 1,    //United States dollar
                               704 => 145,  //Vietnamese dang
                               710 => 82);  //South African rand
        if (array_key_exists($iso_code_num,$currency_list)) {
            return $currency_list[$iso_code_num];
        } else {
            return false;
        }
    }

    /**
    * Get gestpay language code
    * @return integer
    */
    public static function GetLanguageId($iso_code_lang)
    {
        switch ($iso_code_lang) {
            case 'it':
                return 1;
                break;
            case 'es':
                return 3;
                break;
            case 'fr':
                return 4;
                break;
            case 'de':
                return 5;
                break;
            default:
                return 2;
                break;
        }
    }

    /**
    * debug log writer
    * @return boolean
    */
    public function debugLog($function, $error)
    {
        $message = "[" . date('Y-m-d H:i:s') . "] {$function} failed with error: {$error}\r\n";
        $fh = fopen($this->debugLogFile, 'a+');
        fwrite($fh, $message);
        fclose($fh);
    }

    /**
    * objectToArray
    * @return array
    */
    public static function objectToArray($object)
    {
        return json_decode(json_encode($object), true);
    }
    /**
    * arrayToObject
    * @return object
    */
    public static function arrayToObject($array)
    {
        return json_decode(json_encode($array), false);
    }

    private function getRestResponse($url, $headers = null) {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        if ($headers && !empty($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        if (curl_errno($ch)) {
            return false;
        }
        curl_close($ch);
        return $data;
    }
}
