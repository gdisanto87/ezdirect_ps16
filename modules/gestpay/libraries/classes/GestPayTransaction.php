<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2018 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*/

class GestPayTransaction extends ObjectModel
{
    public $id_transaction;
    public $id_cart;
    public $id_customer;
    public $id_language;
    public $id_shop;
    public $id_currency;
    public $id_country;
    public $id_order;
    public $currency;
    public $amount;
    public $test_mode;
    public $shoptransactionid;
    public $transactionresult;
    public $authorizationcode;
    public $banktransactionid;
    public $errorcode;
    public $errordescription;
    public $alertcode;
    public $alertdescription;
    public $paymentmethod;
    public $payment_date;    
    public $last_update;
    public $encString;
    public $transKey;
    

    /**
    * @see ObjectModel::$definition
    */
    public static $definition = array(
        'table' => 'gestpay_transaction',
        'primary' => 'id_transaction',
        'fields' => array(
            'id_cart' =>                    array('type' => self::TYPE_INT),
            'id_customer' =>                array('type' => self::TYPE_INT),
            'id_language' =>                array('type' => self::TYPE_INT),
            'id_shop' =>                    array('type' => self::TYPE_INT),
            'id_currency' =>                array('type' => self::TYPE_INT),
            'id_country' =>                 array('type' => self::TYPE_INT),
            'id_order' =>                   array('type' => self::TYPE_INT),
            'currency' =>                   array('type' => self::TYPE_STRING),
            'amount' =>                     array('type' => self::TYPE_FLOAT),
            'test_mode' =>                  array('type' => self::TYPE_BOOL),
            'shoptransactionid' =>          array('type' => self::TYPE_STRING),
            'transactionresult' =>          array('type' => self::TYPE_STRING),
            'authorizationcode' =>          array('type' => self::TYPE_STRING),
            'banktransactionid' =>          array('type' => self::TYPE_INT),
            'errorcode' =>                  array('type' => self::TYPE_INT),
            'errordescription' =>           array('type' => self::TYPE_STRING),
            'alertcode' =>                  array('type' => self::TYPE_INT),
            'alertdescription' =>           array('type' => self::TYPE_STRING),
            'paymentmethod' =>              array('type' => self::TYPE_STRING),
            'payment_date' =>               array('type' => self::TYPE_DATE),            
            'last_update' =>                array('type' => self::TYPE_DATE),
            'encString' =>                  array('type' => self::TYPE_STRING),
            'transKey' =>                   array('type' => self::TYPE_STRING)
            )
    );

    /**
    * Get transaction by shoptransactionid
    */
    public static function fetchByShopTransactionId($shoptransactionid)
    {
        if (class_exists('PrestaShopCollection')) {
            $trans = new PrestaShopCollection('GestPayTransaction');
        } elseif (class_exists('Collection')) {
            $trans = new Collection('GestPayTransaction');
        }
        $trans->where('shoptransactionid', '=', pSQL($shoptransactionid));
        return $trans->getFirst();
    }
    /**
    * Get transaction by id_order
    */
    public static function fetchByIdOrder($id_order)
    {
        if (class_exists('PrestaShopCollection')) {
            $trans = new PrestaShopCollection('GestPayTransaction');
        } elseif (class_exists('Collection')) {
            $trans = new Collection('GestPayTransaction');
        }
        $trans->where('id_order', '=', pSQL($id_order));
        return $trans->getFirst();
    }
}
