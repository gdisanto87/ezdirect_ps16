<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2018 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class GestPayEncryptRequest
{
    public $shopLogin = null;
    public $uicCode = null;
    public $amount = null;
    public $shopTransactionId = null;
    public $cardNumber = null;
    public $expiryMonth = null;
    public $expiryYear = null;
    public $buyerName = null;
    public $buyerEmail = null;
    public $languageId = null;
    public $cvv = null;
    public $customInfo = null;
    public $requestToken = null;
    public $ppSellerProtection = null;
    public $shippingDetails = null;
    public $paymentTypes = null;
    public $paymentTypeDetail = null;
    public $redFraudPrevention = null;
    public $Red_CustomerInfo = null;
    public $Red_ShippingInfo = null;
    public $Red_BillingInfo = null;
    public $Red_CustomerData = null;
    public $Red_CustomInfo = null;
    public $Red_Items = null;
    public $Consel_MerchantPro = null;
    public $Consel_CustomerInfo = null;
    public $payPalBillingAgreementDescription = null;
    public $OrderDetails = null;

    public function __construct($shopLogin, $uicCode, $amount, $shopTransactionId, $paymentTypes = null)
    {
        $this->shopLogin = $shopLogin;
        $this->uicCode = $uicCode;
        $this->amount = $amount;
        $this->shopTransactionId = $shopTransactionId;
        if($paymentTypes && is_array($paymentTypes)) {
            if(count($paymentTypes) > 1) {
                foreach($paymentTypes as $paymentType) {
                    $payment = new stdClass();
                    $payment->paymentType = $paymentType;
                    $this->paymentTypes[] = $payment;
                }
            } else {
                $this->paymentTypes = new stdClass();
                $this->paymentTypes->paymentType = $paymentTypes[0];
            }
            
        }        
    }
}
