<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2018 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*/

class GestPayToken extends ObjectModel
{
    public $id_token;
    public $id_customer;
    public $token;
    public $token_expiry_month;
    public $token_expiry_year;
    public $save_token;

    /**
    * @see ObjectModel::$definition
    */
    public static $definition = array(
        'table' => 'gestpay_token',
        'primary' => 'id_token',
        'fields' => array(
            'id_customer' =>                array('type' => self::TYPE_INT),
            'token' =>                      array('type' => self::TYPE_STRING),
            'token_expiry_month' =>         array('type' => self::TYPE_INT),
            'token_expiry_year' =>          array('type' => self::TYPE_INT),
            'save_token' =>                 array('type' => self::TYPE_BOOL)
        )
    );

    /**
    * Get token by id_customer
    */
    public static function fetchByIdCustomer($id_customer)
    {
        if (class_exists('PrestaShopCollection')) {
            $token = new PrestaShopCollection('GestPayToken');
        } elseif (class_exists('Collection')) {
            $token = new Collection('GestPayToken');
        }
        $token->where('id_customer', '=', pSQL($id_customer));
        return $token->getFirst();
    }
}
