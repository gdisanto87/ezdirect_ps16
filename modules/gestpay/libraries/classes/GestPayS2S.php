<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class GestPayS2S extends GestPay
{
    public function insertOrder($shopLogin, $CryptedString)
    {
        if ($this->connect()) {
            $debug =  $this->api->debug;
            if ($shopLogin && $CryptedString) {
                $request = new GestPayDecryptRequest($shopLogin, $CryptedString);
                $response = $this->api->decrypt($request);
                if ($response) {
                    $transaction = GestPayTransaction::fetchByShopTransactionId($response->ShopTransactionID);
                    if ($transaction) {
                        $transaction->shoptransactionid = $response->ShopTransactionID;
                        $transaction->transactionresult = $response->TransactionResult;
                        $transaction->authorizationcode = $response->AuthorizationCode;
                        $transaction->banktransactionid = $response->BankTransactionID;
                        $transaction->errorcode = $response->ErrorCode;
                        $transaction->errordescription = $response->ErrorDescription;
                        $transaction->alertcode = $response->AlertCode;
                        $transaction->alertdescription = $response->AlertDescription;
                        $transaction->paymentmethod = $response->PaymentMethod;
                        $transaction->amount = $response->Amount;
                       
                        if ($response->ErrorCode == '0' &&
                            $response->TransactionResult == 'OK'
                            && isset($response->AuthorizationCode)
                            && is_numeric($response->BankTransactionID)) {
                            $this->context->cart = new Cart($transaction->id_cart);
                            $this->context->customer = new Customer($transaction->id_customer);
                            $this->context->language = new Language($transaction->id_language);
                            $this->context->shop = new Shop($transaction->id_shop);
                            $this->context->currency = new Currency($transaction->id_currency);
                            $this->context->country = new Country($transaction->id_country);
                            try {
                                if ($this->validateOrder(
                                    (int)$this->context->cart->id,
                                    Configuration::get('PS_OS_PAYMENT'),
                                    (float)$response->Amount,
                                    $this->name,
                                    $this->api->transactionToText($transaction),
                                    array('transaction_id' => $response->ShopTransactionID),
                                    $this->context->currency->id,
                                    false,
                                    $this->context->customer->secure_key,
                                    $this->context->shop
                                )) {
                                    $transaction->id_order = (int)$this->currentOrder;
                                } else {
                                    if ($debug) {
                                        $this->api->debugLog(
                                            __CLASS__.'->'.__FUNCTION__,
                                            'ValidateOrder generic error, check prestashop logs.'
                                        );
                                    }
                                }
                            } catch (PrestaShopException $e) {
                                if ($debug) {
                                    $this->api->debugLog(
                                        __CLASS__.'->'.__FUNCTION__,
                                        'ValidateOrder exception:' . $e->getMessage()
                                    );
                                }
                            }
                            /* Save TOKEN */
                            if (isset($response->TOKEN) && Tools::strlen($response->TOKEN) > 0) {
                                $token = GestPayToken::fetchByIdCustomer($transaction->id_customer);
                                if (!$token) {
                                    $token = new GestPayToken();
                                    $token->id_customer = $transaction->id_customer;
                                    $token->save_token = true;
                                }
                                if ($token->save_token) {
                                    $token->token = $response->TOKEN;
                                    $token->token_expiry_month = $response->TokenExpiryMonth;
                                    $token->token_expiry_year = $response->TokenExpiryYear;
                                    $token->save();
                                }
                            }
                        }
                        $transaction->save();
                    } else {
                        if ($debug) {
                            $this->api->debugLog(
                                __CLASS__.'->'.__FUNCTION__,
                                "Transaction data not found"
                            );
                        }
                    }
                }
            } else {
                if ($debug) {
                    $this->api->debugLog(
                        __CLASS__.'->'.__FUNCTION__,
                        "S2S parameters missing"
                    );
                }
            }
        }
    }
}