<?php
/**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2018 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once _PS_MODULE_DIR_.'gestpay/gestpay.php';

class GestPayValidationModuleFrontController extends ModuleFrontController
{
    public function __construct()
    {
        parent::__construct();
        $this->ssl = true;
        $this->context = Context::getContext();
    }

    public function postProcess()
    {
        /* we are returing form payment gateway */
        if (Tools::getIsset('a') && Tools::getIsset('b')) {
            $gestpay = new GestPay();
            if ($gestpay->connect()) {
                $request = new GestPayDecryptRequest(Tools::getValue('a'), Tools::getValue('b'));
                if ($response = $gestpay->api->decrypt($request)) {
                    $transaction = GestPayTransaction::fetchByShopTransactionId($response->ShopTransactionID);
                    if ($transaction->errorcode == '0' &&
                        $transaction->transactionresult == 'OK' &&
                        isset($transaction->authorizationcode) &&
                        is_numeric($transaction->banktransactionid)) {
                        $id_cart = (int)$transaction->id_cart;
                        $customer = new Customer((int)$transaction->id_customer);
                        $id_order = (int)$transaction->id_order;
                        $this->redirectConfirmation($this->module->id, $id_cart, $id_order, $customer->secure_key);
                    } else {
                        $this->redirectFailedPayment($transaction->errordescription);
                    }
                } else {
                    $this->redirectFailedPayment($this->module->l('Technical error on gateway response.
                                                                   Please check if the order was added. Error code: ', 'validation') . '002');
                }
            } else {
                $this->redirectFailedPayment($this->module->l('Technical error on gateway response.
                                                               Please check if the order was added. Error code: ', 'validation') . '001');
            }
        } elseif (Tools::getIsset('is_token')) {
            $cart = $this->context->cart;
            if ($cart->id_customer == 0 ||
                $cart->id_address_delivery == 0 ||
                $cart->id_address_invoice == 0 ||
                !$this->module->active) {
                Tools::redirect('index.php?controller=order&step=1');
            }

            // Check that this payment option is still available in case the customer changed
            // his address just before the end of the checkout process
            $authorized = false;
            foreach (Module::getPaymentModules() as $module) {
                if ($module['name'] == 'gestpay') {
                    $authorized = true;
                    break;
                }
            }
            if (!$authorized) {
                die($this->module->l('This payment method is not available.', 'validation'));
            }

            $customer = new Customer($cart->id_customer);
            if (!Validate::isLoadedObject($customer)) {
                Tools::redirect('index.php?controller=order&step=1');
            }
            $gestpay = new GestPay();
            if ($gestpay->connect()) {
                $debug =  $gestpay->api->debug;
                /* get customer data */
                $buyerName = Tools::ucfirst(Tools::strtolower($customer->firstname)) . ' ' .
                             Tools::ucfirst(Tools::strtolower($customer->lastname));
                $buyerEmail = $customer->email;
                /* currency */
                $currency = new Currency((int)$cart->id_currency);
                $uicCode = GestPayAPI::GetUicCode($currency->iso_code_num);
                /* Set amount, note that Yen has no decimals */
                if ($currency->iso_code_num == 392) {
                    $amount = number_format($cart->getOrderTotal(true, Cart::BOTH), 0, '.', '');
                } else {
                    $amount = number_format($cart->getOrderTotal(true, Cart::BOTH), 2, '.', '');
                }
                $shopTransactionId = 'GP_' . (int)$cart->id;
                /* Get token */
                $token = GestPayToken::fetchByIdCustomer($this->context->customer->id);
                $request = new GestPayPagamRequest(
                    $gestpay->api->shopLogin,
                    $uicCode,
                    $amount,
                    $shopTransactionId,
                    $token->token
                );
                if ($this->platform != 'starter') {
                    $request->buyerName = $buyerName;
                    $request->buyerEmail = $buyerEmail;
                }
                /* returning from 3D secure? */
                if (Tools::getIsset('PaRes')) {
                    $request->PARes = preg_replace("/\r|\n|\r\n/", "", trim(Tools::getValue('PaRes')));
                    $request->transKey = Tools::getValue('transKey');
                }
                $response = $gestpay->api->pagam($request);
                $transaction = GestPayTransaction::fetchByShopTransactionId($shopTransactionId);
                if (!$transaction) {
                    $transaction = new GestPayTransaction();
                }
                $transaction->id_cart = (int)$cart->id;
                $transaction->id_customer = (int)$this->context->customer->id;
                $transaction->id_language = (int)$this->context->language->id;
                $transaction->id_shop = (int)$this->context->shop->id;
                $transaction->id_currency =(int)$this->context->currency->id;
                $transaction->id_country = (int)$this->context->country->id;
                $transaction->currency = pSQL($uicCode);
                $transaction->amount = pSQL($amount);
                $transaction->test_mode = $gestpay->api->testMode;
                $transaction->shoptransactionid = pSQL($shopTransactionId);
                $transaction->payment_date = date('Y-m-d H:i:s');
                $transaction->last_update = date('Y-m-d H:i:s');
                $transaction->transactionresult = $response->TransactionResult;
                $transaction->authorizationcode = $response->AuthorizationCode;
                $transaction->banktransactionid = $response->BankTransactionID;
                $transaction->errorcode = $response->ErrorCode;
                $transaction->errordescription = $response->ErrorDescription;
                $transaction->alertcode = $response->AlertCode;
                $transaction->alertdescription = $response->AlertDescription;
                $transaction->paymentmethod = $response->PaymentMethod;
                $transaction->amount = $response->Amount;
                if ($response->ErrorCode == '0' &&
                    $response->TransactionResult == 'OK' &&
                    isset($response->AuthorizationCode) &&
                    is_numeric($response->BankTransactionID)) {
                    try {
                        if ($this->module->validateOrder(
                            (int)$this->context->cart->id,
                            Configuration::get('PS_OS_PAYMENT'),
                            (float)$response->Amount,
                            $gestpay->api->name,
                            $gestpay->api->transactionToText($transaction),
                            array('transaction_id' => $response->ShopTransactionID),
                            $this->context->currency->id,
                            false,
                            $this->context->customer->secure_key,
                            $this->context->shop
                        )) {
                            $transaction->id_order = (int)$this->module->currentOrder;
                            $transaction->save();
                            $this->redirectConfirmation(
                                $this->module->id,
                                (int)$cart->id,
                                (int)$this->module->currentOrder,
                                $customer->secure_key
                            );
                        } else {
                            if ($debug) {
                                $gestpay->api->debugLog(
                                    __CLASS__.'->'.__FUNCTION__,
                                    'ValidateOrder generic error, check prestashop logs.'
                                );
                            }
                        }
                    } catch (PrestaShopException $e) {
                        if ($debug) {
                            $gestpay->api->debugLog(
                                __CLASS__.'->'.__FUNCTION__,
                                'ValidateOrder exception:' . $e->getMessage()
                            );
                        }
                    }
                } elseif ($response->ErrorCode == '8006') {
                    /* redirection 3DSecure */
                    $authUrl = $gestpay->api->getAuthUrl();
                    $shopLogin = $gestpay->api->shopLogin;
                    $transKey = pSQL($response->TransactionKey);
                    $VbVRisp = pSQL($response->VbV->VbVRisp);
                    $returnUrl = $this->context->link->getModuleLink(
                        'gestpay',
                        'validation',
                        array('is_token' => true, 'transKey' => $transKey)
                    );
                    $redirect = $authUrl . '?a=' . $shopLogin . '&b=' . $VbVRisp . '&c=' . $returnUrl;
                    Tools::redirect($redirect);
                } else {
                    $transaction->save();
                    $this->redirectFailedPayment($transaction->errordescription . ' (' . $response->ErrorCode . ')');
                }
            } else {
                $this->redirectFailedPayment($this->module->l('Technical error on gateway response.
                                                               Please check if the order was added. Error code: ', 'validation') . '004');
            }
        } else {
            $this->redirectFailedPayment($this->module->l('Technical error on gateway response.
                                                           Please check if the order was added. Error code: ', 'validation') . '003');
        }
    }

    private function redirectFailedPayment($error = '')
    {
        if (Configuration::get('PS_ORDER_PROCESS_TYPE')) {
            Tools::redirect('index.php?controller=order-opc&isPaymentStep=true&gestpay_error_msg='.urlencode($error));
        } else {
            Tools::redirect('index.php?controller=order&step=3&gestpay_error_msg='.urlencode($error));
        }
    }

    private function redirectConfirmation($id_gestpay, $id_cart, $id_order, $secure_key)
    {
        Tools::redirect('index.php?controller=order-confirmation&id_cart='.$id_cart.
                        '&id_module='.$id_gestpay.'&id_order='.$id_order.'&key='.$secure_key);
    }
}
