{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14489 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}<a href="{$link->getPageLink('my-account.php', true)}">{l s='My account' mod='loyalty'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='My loyalty points' mod='loyalty'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

{if $id_group != 3 && $id_group != 15} 
<br />
<h1 style='text-align:left'>{l s='My loyalty points' mod='loyalty'}</h1>
<br />
{l s='Here you can find your loyalty points. Your points must be convalidated before you can convert it to coupon codes. Your points will be convalidated when the products you have bought will be shipped. If the order will be canceled, the loyalty points will be canceled too. ' mod='loyalty'}<br /><br />
{l s='You must earn points for a total of 5 euros in order to convert them. When your points will be convalidated, you can click on "Transform my points into a voucher code" to convert them. As soon as you have clicked, you will se your coupon in the box below. A coupon has a 30 days validity and you will able to use it on your next order. You must only insert the coupon code (eg. FID****) in the cart process.' mod='loyalty'} 

{if $orders}
<div class="block-center" id="block-history">
	{if $orders && count($orders)}
	<br /><br />
	<table id="order-list" class="std2">
		<thead>
			<tr>
				<th class="first_item" style="background-color:#8899cc">{l s='Order' mod='loyalty'}</th>
				<th class="item" style="background-color:#99aadd">{l s='Date' mod='loyalty'}</th>
				<th class="item" style="background-color:#8899cc">{l s='Points' mod='loyalty'}</th>
				<th class="last_item" style="background-color:#99aadd">{l s='Points Status' mod='loyalty'}</th>
			</tr>
		</thead>
		<tfoot>
			<tr class="alternate_item">
				<td colspan="2" class="history_method bold" style="text-align:center;">{l s='Total points available:' mod='loyalty'}</td>
				<td class="history_method" style="text-align:left;">{$totalPoints|intval}</td>
				<td class="history_method">&nbsp;</td>
			</tr>
		</tfoot>
		<tbody>
		{foreach from=$displayorders item='order'}
			<tr class="alternate_item">
				<td class="history_link bold">{l s='#' mod='loyalty'}{$order.id|string_format:"%06d"}</td>
				<td class="history_date">{dateFormat date=$order.date full=1}</td>
				<td class="history_method" style='text-align:center'>{$order.points|intval}</td>
				<td class="history_method" style='text-align:center'>{$order.state|escape:'htmlall':'UTF-8'}</td>
			</tr>
		{/foreach}
		</tbody>
	</table>
	<div id="block-order-detail" class="hidden">&nbsp;</div>
	{else}
		<p class="warning">{l s='You have not placed any orders.'}</p>
	{/if}
</div>
<div id="pagination" class="pagination">
	{if $nbpagination < $orders|@count}
		<ul class="pagination">
		{if $page != 1}
			{assign var='p_previous' value=$page-1}
			<li id="pagination_previous"><a href="{$pagination_link}?p={$p_previous}&n={$nbpagination}">
			&laquo;&nbsp;{l s='Previous' mod='loyalty'}</a></li>
		{else}
			<li id="pagination_previous" class="disabled"><span>&laquo;&nbsp;{l s='Previous' mod='loyalty'}</span></li>
		{/if}
		{if $page > 2}
			<li><a href="{$pagination_link}?p=1&n={$nbpagination}">1</a></li>
			{if $page > 3}
				<li class="truncate">...</li>
			{/if}
		{/if}
		{section name=pagination start=$page-1 loop=$page+2 step=1}
			{if $page == $smarty.section.pagination.index}
				<li class="current"><span>{$page|escape:'htmlall':'UTF-8'}</span></li>
			{elseif $smarty.section.pagination.index > 0 && $orders|@count+$nbpagination > ($smarty.section.pagination.index)*($nbpagination)}
				<li><a href="{$pagination_link}?p={$smarty.section.pagination.index}&n={$nbpagination}">{$smarty.section.pagination.index|escape:'htmlall':'UTF-8'}</a></li>
			{/if}
		{/section}
		{if $max_page-$page > 1}
			{if $max_page-$page > 2}
				<li class="truncate">...</li>
			{/if}
			<li><a href="{$pagination_link}?p={$max_page}&n={$nbpagination}">{$max_page}</a></li>
		{/if}
		{if $orders|@count > $page * $nbpagination}
			{assign var='p_next' value=$page+1}
			<li id="pagination_next"><a href="{$pagination_link}?p={$p_next}&n={$nbpagination}">{l s='Next' mod='loyalty'}&nbsp;&raquo;</a></li>
		{else}
			<li id="pagination_next" class="disabled"><span>{l s='Next' mod='loyalty'}&nbsp;&raquo;</span></li>
		{/if}
		</ul>
	{/if}
	{if $orders|@count > 10}
		<form action="{$pagination_link}" method="get" class="pagination">
			<p>
				<input type="submit" class="button_mini" value="{l s='OK'}" />
				<label for="nb_item">{l s='items:' mod='loyalty'}</label>
				<select name="n" id="nb_item">
				{foreach from=$nArray item=nValue}
					{if $nValue <= $orders|@count}
						<option value="{$nValue|escape:'htmlall':'UTF-8'}" {if $nbpagination == $nValue}selected="selected"{/if}>{$nValue|escape:'htmlall':'UTF-8'}</option>
					{/if}
				{/foreach}
				</select>
				<input type="hidden" name="p" value="1" />
			</p>
		</form>
	{/if}
	</div>

<br />{l s='Vouchers generated here are usable in the following categories : ' mod='loyalty'}
{if $categories}{$categories}{else}{l s='All' mod='loyalty'}{/if}

{if $transformation_allowed}
<p style="text-align:center; margin-top:20px">
{if $voucher < 5}
{l s='You must earn at least a 5 euros coupon to convert it into a discount.' mod='loyalty'}
{else}
	<a href="{$link->getPageLink('modules/loyalty/loyalty-program.php', true)}?transform-points=true" onclick="return confirm('{l s='Are you sure you want to transform your points into vouchers?' mod='loyalty' js=1}');">{l s='Transform my points into a voucher of' mod='loyalty'} <span class="price">{convertPrice price=$voucher}</span>.</a>
{/if}
</p>
{/if}

<br /><br />
<h2>{l s='My vouchers from loyalty points' mod='loyalty'}</h2>

{if $nbDiscounts}
<div class="block-center" id="block-history">
<br /><br />
	<table id="order-list" class="std">
		<thead>
			<tr>
				<th class="first_item">{l s='Created' mod='loyalty'}</th>
				<th class="item">{l s='Value' mod='loyalty'}</th>
				<th class="item">{l s='Code' mod='loyalty'}</th>
				<th class="item">{l s='Valid from' mod='loyalty'}</th>
				<th class="item">{l s='Valid until' mod='loyalty'}</th>
				<th class="item">{l s='Status' mod='loyalty'}</th>
				<th class="last_item">{l s='Details' mod='loyalty'}</th>
			</tr>
		</thead>
		<tbody>
		{foreach from=$discounts item=discount name=myLoop}
			<tr class="alternate_item">
				<td class="history_date">{dateFormat date=$discount->date_add}</td>
				<td class="history_price"><span class="price">{if $discount->id_discount_type == 1}
						{$discount->value}%
					{elseif $discount->id_discount_type == 2}
						{displayPrice price=$discount->value currency=$discount->id_currency}
					{else}
						{l s='Free shipping' mod='loyalty'}
					{/if}</span></td>
				<td class="history_method bold">{$discount->name}</td>
				<td class="history_date">{dateFormat date=$discount->date_from}</td>
				<td class="history_date">{dateFormat date=$discount->date_to}</td>
				<td class="history_method bold">{if $discount->quantity > 0}{l s='To use' mod='loyalty'}{else}{l s='Used' mod='loyalty'}{/if}</td>
				<td class="history_method"><a href="{$smarty.server.SCRIPT_NAME}" onclick="return false" class="tips" title="{l s='Generated by these following orders' mod='loyalty'}|{foreach from=$discount->orders item=myorder name=myLoop}{l s='Order #' mod='loyalty'}{$myorder.id_order} ({displayPrice price=$myorder.total_paid currency=$myorder.id_currency}) : {if $myorder.points > 0}{$myorder.points} {l s='points.' mod='loyalty'}{else}{l s='Cancelled' mod='loyalty'}{/if}{if !$smarty.foreach.myLoop.last}|{/if}{/foreach}">{l s='more...' mod='loyalty'}</a></td>
			</tr>
		{/foreach}
		</tbody>
	</table>
	<div id="block-order-detail" class="hidden">&nbsp;</div>
</div>

{if $minimalLoyalty > 0}<p>{l s='The minimum order amount in order to use these vouchers is:' mod='loyalty'} {convertPrice price=$minimalLoyalty}</p>{/if}

<script type="text/javascript">
{literal}
$(document).ready(function()
{
	$('a.tips').cluetip({
		showTitle: false,
		splitTitle: '|',
		arrows: false,
		fx: {
			open: 'fadeIn',
			openSpeed: 'fast'
		}
	});
});
{/literal}
</script>
{else}
<p class="warning">{l s='No vouchers yet.' mod='loyalty'}</p>
{/if}
{else}
<p class="warning">{l s='No reward points yet.' mod='loyalty'}</p>
{/if}

{else}
{l s='Loyalty program is not available for resellers' mod='loyalty'}.

{/if}
