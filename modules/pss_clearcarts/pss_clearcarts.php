<?php
/**
 * Exports datas related to newsletter purposes.
 *
 * See LICENCE.txt for terms of use
 * History :
 * 	@version 0.1 : 
 *		list & clean carts
 *		compatible Prestashop 1.3.x & 1.4.0
 * 	@version 0.2 : 
 *		delete cross-indexed core tables table and customizable products uploaded files
 *		a single module for all supported prestashop versions
 * 	@version 0.3 : 
 * 		upload path was incorrect for 1.4.0 version
 * 	@version 0.4 : 
 * 		Compatible with Prestashop 1.4.1
 * 	@version 0.5 : 
 * 		Compatible with Prestashop 1.4.2
 * 	@version 0.6 : 
 * 		Compatible with Prestashop 1.4.3 & 1.2.5
 *  @version 0.7 (2011-11-07) : Thanks to leeloo !
 * 		Links from cart items to customer & cart details
 *		Display date according to user locale
 *		Button to list all carts
 *		Design changes in cart list
 * 		Correct translation bug for pre 1.4.x versions
 * 	@version 1.0 : 
 * 		Function to validate carts into orders
 *		
 */
class pss_clearcarts extends Module
{
    private $_postErrors = array();
	private $_tabClass = 'AdminPssClearCarts';

	// config default set
	public static $_config = array(
		'PSS_CLEANCARTS_PAYMOD' => 'paypal',
		'PSS_CLEANCARTS_PAYMOD_DISPLAY' => 'PayPal',
	);
	
    function __construct()
    {
        $this->name = 'pss_clearcarts';
		// some changes between 1.3.x and 1.4.x Prestashop versions
		if (self::isPs12x() || self::isPs13x()) 
		{
			$this->tab = 'Prestascope';
		} 
		else if (self::isPs14x()) 
		{
			$this->tab = 'administration';
			$this->author = 'PrestaScope';
		}
			
        $this->version = '1.0';

        parent::__construct();

        $this->displayName = $this->l('Clear carts');
		$buf = '<style type="text/css">';
		$buf .= 'a.descriptionLink {color:blue;text-decoration:underline;} a.descriptionLink:hover{color:red;text-decoration:none;}';
		$buf .= 'div.pssDescriptionDiv {background:#6a6a6a; color:white; padding:3px; margin-top:2px;}';
		$buf .= '</style>';
		$buf .= '<div class="pssDescriptionDiv">';
		$buf .= $this->l('This module allows to clear carts. Access after installation by the "Customers" Admin Tab.');
		$buf .= '</div>';
        $this->description = $buf;
    }

    function install()
    {
		// check required version
		if (!self::isPs12x() && !self::isPs13x() && !self::isPs14x()) 
		{
			return false;
		}
		// normal install
		if(!parent::install() || !$this->_installTab() || !$this->_installConfig())
			return false;
		return true;
    }
	private function _installTab() {
		@copy(_PS_MODULE_DIR_ . $this->name . '/logo.gif', _PS_IMG_DIR_ . 't/' . $this->_tabClass . '.gif');
		$tab = new Tab();
		
		foreach (Language::getLanguages() as $language)
			$tab->name[$language['id_lang']] = 'Clear carts';
		$tab->name[2] = 'Nettoyer paniers';
		$tab->class_name = $this->_tabClass;
		$tab->module = $this->name;
		$tab->id_parent = Tab::getIdFromClassName('AdminCustomers');
		if (!$tab->save())
			return false;
		return true;
	}
	private function _installConfig()
	{
		foreach (self::$_config as $key => $value) 
			Configuration::updateValue($key, $value);
		return true;
	}
	public function uninstall() 
	{
		if(!parent::uninstall() || !$this->_uninstallTab() || !$this->_uninstallConfig())
			return false;
		return true;
	}
	private function _uninstallTab() 
	{
		$idTab = Tab::getIdFromClassName($this->_tabClass);
		if ($idTab != 0) 
		{
			@unlink(_PS_IMG_DIR_ . 't/' . $this->_tabClass . '.gif');
			$tab = new Tab($idTab);
			$tab->delete();
			return true;
		}
		return false;
	}
	private function _uninstallConfig() 
	{
		foreach (self::$_config as $key => $value) 
			Configuration::deleteByName($key);
		return true;
	}
	/**
	 * Check if installed Prestashop is a 1.2.x version
	 */
	public static function isPs12x() 
	{
		return self::checkPsVersion('1.2');
	}
	/**
	 * Check if installed Prestashop is a 1.3.x version
	 */
	public static function isPs13x() 
	{
		return self::checkPsVersion('1.3');
	}
	/**
	 * Check if installed Prestashop is a 1.4.x version
	 */
	public static function isPs14x() 
	{
		return self::checkPsVersion('1.4');
	}
	/**
	 * Check if installed Prestashop is a version
	 */
	public static function checkPsVersion($radixVersion) 
	{
		// get PS version
		$psVersion = _PS_VERSION_;
		if ($psVersion==null)
			return false;
		
		// ps version must have a specific length
		$expectedLength = 5;
		// but it has changed with version 1.4.3 !! Thanks all.
		if ($psVersion==='1.4.3')
			$expectedLength = 4;
		
		if ($psVersion==null || strlen($psVersion)<=$expectedLength)
			return false;

		// look for version like 1.3.7.0
		$subVersions = explode('.', $psVersion);
		$searchVersions = explode('.', $radixVersion);
		
		for ($i=0;$i<count($searchVersions);$i++) 
		{
			// compare each sub part of version
			if ($subVersions[$i] !== $searchVersions[$i]) 
				return false;
		}
		return true;
	}
	
}
?>
