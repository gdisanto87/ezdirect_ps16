# GLS Corriere Espresso - Manage your shipments
Do you want to automate your shipments? This module do this for free.
Exporting and updating orders, creating shipments and labels and tracking shipments won’t be an issue any more.
Sell&Send has been created exactly for e-commerce sellers and with it you can: 
- load orders in real time, including the ones coming from other e-commerce platforms
- create shipments and printing labels with 3 clicks  
- monitor deliveries thanks to a user-friendly control panel
- configure and customize emails to inform your customers on the shipment progress and, at the same time,  to send them promotional messages
- update automatically the orders status on each e-commerce platform connected with Sell&Send
Alternatively you can use WebLabeling to export Prestashop orders in a file and to load them creating shipments in a simple and error-proof manner.
Both solutions automatically update the order status on Prestashop.