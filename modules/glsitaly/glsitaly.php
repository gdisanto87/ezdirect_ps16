<?php
/**
 * 2017-2018 Blulab S.r.l. - http://www.blulab.net/
 *
 *  @author    Blulab S.r.l <info@blulab.net>
 *  @copyright 2017-2018 Blulab S.r.l.
 *  @license   please contact Blulab S.r.l. http://www.blulab.net/
 *  Property of Blulab s.r.l. & GLS
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class Glsitaly extends Module
{

    const APIURL_SENDEMAIL = 'https://partners-subscribe.prestashop.com/gls/request.php'; // 'https://sellsend-api.gls-italy.com/sendEmail/';
    const APIURL_CHECKAPI = 'https://sellsend-api.gls-italy.com/checkAPI/?auth=%1$s';
    const APIURL_GETTRACK = 'https://sellsend-api.gls-italy.com/getTrack/?auth=%1$s&trackingNumber=%2$s&lang=%3$s';
    const APIURL_PUSHORDER = 'https://sellsend-api.gls-italy.com/pushOrder/';
    const APIURL_PUSHTRACK = 'https://sellsend-api.gls-italy.com/pushTrack/';
    const POST_CURL_TIMEOUT = 5;

    protected $contacts_fields = array('name', 'address', 'zip', 'city', 'state', 'email', 'phone', 'referent', 'privacy');
    protected $sellandsend_fields = array('apikey', 'status', 'shipping', 'delivered');
    protected $WebLabeling_fields = array('payment', 'status', 'modulocontrasegno');
    protected $WebLabeling_payment = array(
        'CONT' => 'CONTANTE',
        'AB' => 'ASSEGNO BANCARIO',
        'ABC' => 'ASSEGNO CIRCOLARE/BANCARIO (NO POSTALE)',
        'ABP' => 'ASSEGNO BANCARIO/POSTALE (NO CIRCOLARE)',
        'AC' => 'ASSEGNO CIRCOLARE',
        'AP' => 'ASSEGNO POSTALE',
        'ASS' => 'ASSEGNO CIRCOLARE/BANCARIO/POSTALE'
    );

    public function __construct()
    {
        $this->name = 'glsitaly';
        $this->tab = 'shipping_logistics';
        $this->version = '0.2.0';
        $this->author = 'Blulab s.r.l.';
        $this->need_instance = 0;
        $this->bootstrap = true;
        $this->module_key = 'e1f5021b6db9bef176b6d79640666325';

        parent::__construct();

        $this->displayName = $this->l('GLS Express Courier - Manage your shipments');
        $this->description = $this->l('GLS: Integration with WebLabeling and Sell And Send');
        $this->confirmUninstall = $this->l('Are you shure to uninstall GLS module?');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        Configuration::updateValue('GLSITALY_INSTALLED', true);
//
        $return = parent::install();
        if ($return) {
            $this->installTab();
            $this->registerHook('actionOrderStatusPostUpdate');
            $this->registerHook('displayBackofficeHeader');
            $this->registerHook('displayOrderDetail');
            //creo stato ordine
            $spedizioneStateOrderName = array(
                'it' => 'Esportato per GLS WebLabeling',
                'en' => 'Export on GLS WebLabeling'
            );
            $chiaveStatoOrdineSpedizione = 'GLSITALY_OS_EXPORT';
            $this->createOrderState($spedizioneStateOrderName, $chiaveStatoOrdineSpedizione);
        }
        return $return;
    }

    protected function unInstallTab()
    {
        $tab_id = (int) Tab::getIdFromClassName('AdminGlsitaly');
        if ($tab_id > 0) {
            $tab = new Tab($tab_id);
            $tab->delete();
        }
    }

    protected function installTab()
    {
        //prestashop 1.7 bug: need to add and next to update to be a fully functional tab
        $parent_tab_id = Tab::getIdFromClassName('AdminParentOrders');
        if ($parent_tab_id !== false) {
            $tab_id = Tab::getIdFromClassName('AdminGlsitaly');
            if ($tab_id === false) {
                $tab = new Tab();
                $languages = Language::getLanguages(false);
                foreach ($languages as $language) {
                    $tab->name[$language['id_lang']] = 'GLS WebLabeling';
                }
                $tab->class_name = 'AdminGlsitaly';
                $tab->id_parent = (int) $parent_tab_id;
                $tab->module = $this->name;
                $tab->icon = 'shopping_basket';
                $tab->add();
            } else {
                $tab = new Tab($tab_id);
                $languages = Language::getLanguages(false);
                foreach ($languages as $language) {
                    $tab->name[$language['id_lang']] = 'GLS WebLabeling';
                }
                $tab->id_parent = (int) $parent_tab_id;
                $tab->update();
            }
        }
    }

    public function createOrderState($nomeOrderState, $chiaveStatoOrdine)
    {
        $id_state = Configuration::get($chiaveStatoOrdine);
        if (!(isset($id_state) && !empty($id_state) && Validate::isLoadedObject(new OrderState($id_state)))) {
            $order_state = new OrderState();
            $order_state->name = array();
            foreach (Language::getLanguages() as $language) {
                if (Tools::strtolower($language['iso_code']) == 'it') {
                    $order_state->name[$language['id_lang']] = $nomeOrderState['it'];
                } else {
                    $order_state->name[$language['id_lang']] = $nomeOrderState['en'];
                }
            }
            $order_state->send_email = false;
            $order_state->color = '#DDEEFF';
            $order_state->hidden = true;
            $order_state->delivery = false;
            $order_state->logable = true;
            $order_state->invoice = false;
            $order_state->module_name = $this->name;
            if ($order_state->add()) {
                $source = _PS_MODULE_DIR_ . $this->name . '/views/img/shipped.gif';
                $destination = _PS_IMG_DIR_ . 'os/' . (int) $order_state->id . '.gif';
                Tools::copy($source, $destination);
            }
            Configuration::updateValue($chiaveStatoOrdine, (int) $order_state->id);
        }
    }

    public function uninstall()
    {
        Configuration::deleteByName('GLSITALY_INSTALLED');
        //Configuration::deleteByName('GLSITALY_OS_EXPORT'); //not to remove, avoid duplicate in module reinstall
        Configuration::deleteByName('GLSITALY_SS_APIKEY');
        Configuration::deleteByName('GLSITALY_SS_APIKEY_OK');
        Configuration::deleteByName('GLSITALY_SS_STATE_DELIVERED');
        Configuration::deleteByName('GLSITALY_SS_STATE_SHIPPING');
        Configuration::deleteByName('GLSITALY_SS_STATUS');
        Configuration::deleteByName('GLSITALY_WL_CONTRASSSEGNO');
        Configuration::deleteByName('GLSITALY_WL_PAYMENT');
        Configuration::deleteByName('GLSITALY_WL_STATUS');
        $this->unInstallTab();
        return parent::uninstall();
    }

    protected function getInstalledPaymentModules()
    {
        $modules = PaymentModule::getInstalledPaymentModules();
        $iso = Tools::substr(Context::getContext()->language->iso_code, 0, 2);
        foreach ($modules as $key => $module) {
            $configFile = _PS_MODULE_DIR_ . $module['name'] . '/config_' . $iso . '.xml';
            if (!is_file($configFile)) {
                $configFile = _PS_MODULE_DIR_ . $module['name'] . '/config.xml';
            }
            if (is_file($configFile)) {
                $errors = array();
                libxml_use_internal_errors(true);
                $xml_module = simplexml_load_file($configFile);
                foreach (libxml_get_errors() as $error) {
                    $errors[] = '[' . $module['name'] . '] Error found in config file ' . htmlentities($error->message);
                }
                libxml_clear_errors();
                if (count($errors) == 0) {
                    $st = Module::configXmlStringFormat($xml_module->displayName);
                    $txt = Translate::getModuleTranslation((string) $xml_module->name, $st, (string) $xml_module->name);
                    $modules[$key]['displayName'] = Tools::stripslashes($txt);
                }
            }
        }
        return $modules;
    }

    public function getContent()
    {
        $idCountry = Country::getByIso('it');
        $states = State::getStatesByIdCountry($idCountry);
        $province = array();
        foreach ($states as $state) {
            $province[$state['iso_code']] = $state['name'];
        }
        $order_states = OrderState::getOrderStates($this->context->language->id);
        $statiOrdine = array();
        foreach ($order_states as $state) {
            $statiOrdine[$state['id_order_state']] = $state['name'];
        }
        $modules = $this->getInstalledPaymentModules();
        $paymentModules = array();
        foreach ($modules as $module) {
            if (isset($module['displayName'])) {
                $name = $module['displayName'];
            } else {
                $name = $module['name']; //Prestashop 1.7 preinstalled payment modules bug workaround
            }
            $paymentModules[$module['name']] = $name;
        }

        $this->context->smarty->assign('province', $province);
        $this->context->smarty->assign('WebLabeling_payment', $this->WebLabeling_payment);
        $this->context->smarty->assign('stati_ordine', $statiOrdine);
        $this->context->smarty->assign('metodi_pagamento', $paymentModules);

        $smaryContactsVar = array();
        $smaryContactsVarErr = array();
        $smarySellandsendVar = array();
        $smaryWebLabelingVar = array();
        $contattoFormError = false;

        $fomsubmitted = false;

        //contact;
        foreach ($this->contacts_fields as $field) {
            $key = "GLSITALY_CONTACT_" . $field;
            $default = '';
            switch ($field) {
                case 'name':
                    $default = Configuration::get('PS_SHOP_NAME');
                    break;
                case 'email':
                    $default = Configuration::get('PS_SHOP_EMAIL');
                    break;
                case 'address':
                    $default = Configuration::get('PS_SHOP_ADDR1');
                    break;
                case 'zip':
                    $default = Configuration::get('PS_SHOP_CODE');
                    break;
                case 'city':
                    $default = Configuration::get('PS_SHOP_CITY');
                    break;
                case 'state':
                    $id_provincia = Configuration::get('PS_SHOP_STATE_ID');
                    $state = new State((int) $id_provincia);
                    $default = $state->iso_code;
                    break;
                case 'phone':
                    $default = Configuration::get('PS_SHOP_PHONE');
                    break;
            }
            $smaryContactsVar[$field] = Tools::getValue($key, $default);
            $smaryContactsVarErr[$field] = false;
            if (Tools::isSubmit($key) || ($fomsubmitted && $field == 'privacy')) {
                $fomsubmitted = true;
                if (empty($smaryContactsVar[$field])) {
                    if ($field !== 'phone') {
                        $smaryContactsVarErr[$field] = true;
                    }
                } else {
                    switch ($field) {
                        case 'name':
                            //$smaryContactsVarErr[$field] = !Validate::isName($smaryContactsVar[$field]);
                            break;
                        case 'email':
                            $smaryContactsVarErr[$field] = !Validate::isEmail($smaryContactsVar[$field]);
                            break;
                        case 'address':
                            $smaryContactsVarErr[$field] = !Validate::isAddress($smaryContactsVar[$field]);
                            break;
                        case 'zip':
                            $country_id = Configuration::get('PS_SHOP_COUNTRY_ID');
                            $country = new Country($country_id);
                            if (Tools::strtolower($country->iso_code) == 'it') {
                                $smaryContactsVarErr[$field] = !$this->isItalianCAP($smaryContactsVar[$field]);
                            } else {
                                $smaryContactsVarErr[$field] = !Validate::isZipCodeFormat($smaryContactsVar[$field]);
                            }
                            break;
                        case 'city':
                            $smaryContactsVarErr[$field] = !Validate::isCityName($smaryContactsVar[$field]);
                            break;
                        case 'state':
                            $smaryContactsVarErr[$field] = !Validate::isName($smaryContactsVar[$field]);
                            break;
                        case 'phone':
                            $smaryContactsVarErr[$field] = !empty($smaryContactsVar[$field]) && !Validate::isPhoneNumber($smaryContactsVar[$field]);
                            break;
                    }
                }
                if ($smaryContactsVarErr[$field]) {
                    $contattoFormError = true;
                }
            }
        }

        //sellandsend
        foreach ($this->sellandsend_fields as $field) {
            $key = "GLSITALY_SELLANDSEND_" . $field;
            $default = '';
            switch ($field) {
                case 'apikey':
                    $default = $this->getWithDefault('GLSITALY_SS_APIKEY', '');
                    break;
                case 'status':
                    $default = (int) $this->getWithDefault('GLSITALY_SS_STATUS', 0);
                    break;
                case 'shipping':
                    $default = (int) $this->getWithDefault('GLSITALY_SS_STATE_SHIPPING', Configuration::get('PS_OS_SHIPPING'));
                    break;
                case 'delivered':
                    $default = (int) $this->getWithDefault('GLSITALY_SS_STATE_DELIVERED', Configuration::get('PS_OS_DELIVERED'));
                    break;
            }
            $smarySellandsendVar[$field] = Tools::getValue($key, $default);
        }

        //WebLabeling
        foreach ($this->WebLabeling_fields as $field) {
            $key = "GLSITALY_WebLabeling_" . $field;
            $default = '';
            switch ($field) {
                case 'payment':
                    $default = $this->getWithDefault('GLSITALY_WL_PAYMENT', 'CONT');
                    break;
                case 'status':
                    $default = (int) $this->getWithDefault('GLSITALY_WL_STATUS', 0);
                    break;
                case 'modulocontrasegno':
                    $default = $this->getWithDefault('GLSITALY_WL_CONTRASSSEGNO', '');
                    break;
            }
            $smaryWebLabelingVar[$field] = Tools::getValue($key, $default);
        }
        $updateOutput = "";
        if (((bool) Tools::isSubmit('submitGlsitalyModule')) == true) {
            $section = (int) Tools::getValue('submitGlsitalyModule', 0);
            switch ($section) {
                case 1: //contatti
                    if ($contattoFormError) {
                        $this->context->smarty->assign('contattosubmit', false);
                    } else {
                        $res = $this->postProcessContatti($smaryContactsVar);
                        $this->context->smarty->assign('contattosubmit', $res);
                    }
                    break;
                case 2: //sellandsend
                    $updateOutput .= $this->postProcessSellAndSend($smarySellandsendVar);
                    break;
                case 3: //WebLabeling
                    $updateOutput .= $this->postProcessWebLabeling($smaryWebLabelingVar);
                    break;
            }
        }

        $this->context->smarty->assign('contatto', $smaryContactsVar);
        $this->context->smarty->assign('contattoErrors', $smaryContactsVarErr);
        $this->context->smarty->assign('sellandsend', $smarySellandsendVar);
        $this->context->smarty->assign('WebLabeling', $smaryWebLabelingVar);
        $this->context->smarty->assign('server2server', $this->context->link->getModuleLink($this->name, 'status'));

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');

        return $updateOutput . $output;
    }

    protected function isItalianCAP($cap)
    {
        return preg_match('/^[0-9]{5}$/', $cap);
    }

    protected function postProcessSellAndSend(&$data)
    {
        $submitRes = true;
        if (empty($data['apikey'])) {
            if ($data['status']) {
                $submitRes = false;
                $data['status'] = false;
            }
        } else {
            $submitRes = false;
            $url = sprintf(self::APIURL_CHECKAPI, rawurlencode($data['apikey']));
            $res = Tools::file_get_contents($url);
            if ($res) {
                $json = Tools::jsonDecode($res, true);
                if (is_array($json) && isset($json['checkAPI']['result'])) {
                    $response = (string) $json['checkAPI']['result'];
                    if ($response == 'OK') {
                        $submitRes = true;
                    }
                }
            }
            if (!$submitRes) {
                $data['apikey'] = '';
                $data['status'] = false;
            }
        }
        Configuration::updateValue('GLSITALY_SS_APIKEY', (string) $data['apikey']);
        Configuration::updateValue('GLSITALY_SS_STATUS', ((bool) $data['status']));
        Configuration::updateValue('GLSITALY_SS_STATE_SHIPPING', $data['shipping']);
        Configuration::updateValue('GLSITALY_SS_STATE_DELIVERED', $data['delivered']);


        if ($submitRes) {
            return $this->displayConfirmation($this->l('Successfully updated', 'glsitaly'));
        } else {
            return $this->displayError($this->l('Error: Invalid API KEY.', 'glsitaly'));
        }
    }

    protected function postProcessWebLabeling(&$data)
    {
        Configuration::updateValue('GLSITALY_WL_PAYMENT', (string) $data['payment']);
        Configuration::updateValue('GLSITALY_WL_STATUS', ((bool) $data['status']));
        Configuration::updateValue('GLSITALY_WL_CONTRASSSEGNO', $data['modulocontrasegno']);
        $this->installTab(); //secont time: Prestashop 1.7 bug workaround

        $tab_id = (int) Tab::getIdFromClassName('AdminGlsitaly');
        if ($tab_id > 0) {
            $tab = new Tab($tab_id);
            $tab->active = (bool) $data['status'];
            $tab->update();
        }

        return $this->displayConfirmation($this->l('Successfully updated', 'glsitaly'));
    }

    protected function postProcessContatti(&$data)
    {
        $res = false;
        $data['apiKey'] = '50c684964358f3f76d3265fc6d365139049c10375bc828b1ec6d851068e9900c';
        $post = Tools::jsonEncode($data);
        $contextdata = array(
            'http' => array(
                'method' => 'POST',
                'content' => $post,
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'timeout' => self::POST_CURL_TIMEOUT
            )
        );
        $stream_context = stream_context_create($contextdata);
        $prestashopQS = "?ps_version=" . _PS_VERSION_ . "&module_version=" . $this->version . "&url=" . rawurlencode(Configuration::get('PS_SHOP_DOMAIN')) . "&email=" . rawurlencode(Configuration::get('PS_SHOP_EMAIL'));
        $result = Tools::file_get_contents(self::APIURL_SENDEMAIL . $prestashopQS, false, $stream_context, self::POST_CURL_TIMEOUT);
        if ($result) {
            $resData = Tools::jsonDecode($result, true);
            if (isset($resData['response']['sendEmail']) && isset($resData['response']['sendEmail']['result'])) {
                $result = Tools::strtolower(trim($resData['response']['sendEmail']['result']));
                $res = ($result == 'ok') ? true : false;
            }
        }
        return $res;
    }

    public function getWithDefault($key, $default, $id_lang = null, $id_shop_group = null, $id_shop = null)
    {
        $res = Configuration::get($key, $id_lang, $id_shop_group, $id_shop);
        if ($res === false) {
            $res = $default;
        }
        return $res;
    }

    public function hookActionOrderStatusPostUpdate($params)
    {
        if ($this->getWithDefault('GLSITALY_SS_STATUS', false)) {
            $this->orderToSellAndSend($params['id_order']);
        }
    }

    protected function orderToSellAndSend($idOrder)
    {
        $logreason = $this->l('Unknown', 'glsitaly');
        $logseverity = 3;
        $logcode = 0;
        if ($this->getWithDefault('GLSITALY_SS_STATUS', false)) {
            if (!empty($this->getWithDefault('GLSITALY_SS_APIKEY', ''))) {
                $order = new Order((int) $idOrder);
                if (Validate::isLoadedObject($order)) {
                    $customer = new Customer($order->id_customer);
                    if (Validate::isLoadedObject($customer)) {
                        $address = new Address($order->id_address_delivery);
                        if (Validate::isLoadedObject($address)) {
                            $state = new State((int) ($address->id_state));
                            if (Validate::isLoadedObject($state)) {
                                $country = new Country((int) ($address->id_country));
                                if (Validate::isLoadedObject($country)) {
                                    $orderstate = new OrderState($order->current_state);
                                    if (Validate::isLoadedObject($orderstate)) {
                                        $phone = (empty($address->phone) ? $address->phone_mobile : $address->phone);
                                        $pushOrder = array(
                                            'id' => $order->getUniqReference(),
                                            'orderId' => $order->id,
                                            'status' => $orderstate->name[$this->context->language->id],
                                            'createdAt' => $order->date_add,
                                            'updatedAt' => $order->date_upd,
                                            'customerName' => $customer->lastname . " " . $customer->firstname,
                                            'customerAddress' => $address->address1 . " " . $address->address2,
                                            'customerCity' => $address->city,
                                            'customerState' => $state->iso_code,
                                            'customerZip' => $address->postcode,
                                            'customerCountry' => $country->iso_code,
                                            'customerEmail' => $customer->email,
                                            'customerTelephone' => $phone,
                                            'amount' => 'EUR ' . round($order->total_paid_tax_incl, 2),
                                            'paymentType' => $order->payment,
                                        );
                                        $key = $this->getWithDefault('GLSITALY_SS_APIKEY', false);
                                        $post = Tools::jsonEncode(array(
                                                'apiKey' => $key,
                                                'source' => 'prestashop',
                                                'pushOrder' => array(
                                                    $pushOrder
                                                )
                                        ));
                                        $contextdata = array(
                                            'http' => array(
                                                'method' => 'POST',
                                                'content' => $post,
                                                'header' => 'Content-type: application/x-www-form-urlencoded',
                                                'timeout' => self::POST_CURL_TIMEOUT
                                            )
                                        );
                                        $stream_context = stream_context_create($contextdata);
                                        $res = Tools::file_get_contents(self::APIURL_PUSHORDER, false, $stream_context, self::POST_CURL_TIMEOUT);
                                        $resData = Tools::jsonDecode($res);
                                        if (isset($resData->pushOrder)) {
                                            if ($resData->pushOrder->result == 'OK') { //1 info 3 error
                                                $logreason = $this->l('Order correctly sent', 'glsitaly');
                                                $logseverity = 1;
                                            } else {
                                                $txt = $this->l('Date', 'glsitaly') . ': ' . print_r($pushOrder, true);
                                                $logreason = $this->l('Error', 'glsitaly') . ': ' . print_r($resData->pushOrder, true) . ' - ' . $txt;
                                                $logcode = 10;
                                            }
                                        } else {
                                            $logcode = 9;
                                            $logreason = $this->l('No response', 'glsitaly');
                                        }
                                    } else {
                                        $logcode = 8;
                                        $logreason = $this->l('Orderstate not valid', 'glsitaly');
                                    }
                                } else {
                                    $logcode = 7;
                                    $logreason = $this->l('Country not valid', 'glsitaly');
                                }
                            } else {
                                $logcode = 6;
                                $logreason = $this->l('State not valid', 'glsitaly');
                            }
                        } else {
                            $logcode = 5;
                            $logreason = $this->l('Address not valid', 'glsitaly');
                        }
                    } else {
                        $logcode = 4;
                        $logreason = $this->l('Customer not valid', 'glsitaly');
                    }
                } else {
                    $logcode = 3;
                    $logreason = $this->l('Order not valid', 'glsitaly');
                }
            } else {
                $logcode = 1;
                $logreason = $this->l('Module not configured', 'glsitaly');
            }
            $id = (isset($order->id) ? (int) $order->id : 0);
            $employee = (isset($this->context->employee->id) ? (int) $this->context->employee->id : 0);
            $str = $this->l('GLS PushOrder API', 'glsitaly');
            PrestaShopLogger::addLog($str . ": " . $logreason, $logseverity, $logcode, 'Order', $id, true, $employee);
        }
    }

    public function hookDisplayOrderDetail($params)
    {
        if ($this->getWithDefault('GLSITALY_SS_STATUS', false)) {
            if (!empty($this->getWithDefault('GLSITALY_SS_APIKEY', ''))) {
                if (isset($params['order']->reference) && !empty($params['order']->reference)) {
                    $reference = $params['order']->getUniqReference();
                    $key = $this->getWithDefault('GLSITALY_SS_APIKEY', '');
                    switch ($this->context->language->iso_code) {
                        case 'it':
                            $lang = 'ita';
                            break;
                        default:
                            $lang = 'eng';
                    }
                    $url = sprintf(self::APIURL_GETTRACK, rawurlencode($key), rawurlencode($reference), rawurlencode($lang));
                    $res = Tools::file_get_contents($url);
                    if ($res) {
                        $json = Tools::jsonDecode($res, true);
                        if (is_array($json) && isset($json['getTrack']['error'])) {
                            $jsonData = $json['getTrack'];
                            $this->context->smarty->assign($jsonData);
                            $html = $this->display(__FILE__, 'displayorderdetail.tpl');
                            return $html;
                        }
                    }
                }
            }
        }
        return '';
    }

    public function hookDisplayBackofficeHeader()
    {
        $this->context->controller->addCSS($this->_path . 'views/css/back.css');
    }
}
