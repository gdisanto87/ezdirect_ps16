<?php
/**
 * 2017-2018 Blulab S.r.l. - http://www.blulab.net/
 *
 *  @author    Blulab S.r.l <info@blulab.net>
 *  @copyright 2017-2018 Blulab S.r.l.
 *  @license   please contact Blulab S.r.l. http://www.blulab.net/
 *  Property of Blulab s.r.l. & GLS
 */

class AdminGlsitalyController extends ModuleAdminController
{

    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'order';
        $this->className = 'Order';
        $this->lang = false;
        //$this->addRowAction('view');
        $this->addRowAction('export');
        $this->context = Context::getContext();
        //$this->list_no_link = true;

        $os_id = (int) Configuration::get('GLSITALY_OS_EXPORT');
        $this->_select = '
		a.id_currency,
		CONCAT(LEFT(c.`firstname`, 1), \'. \', c.`lastname`) AS `customer`,
		osl.`name` AS `osname`,
		os.`color`,
		(
                SELECT oh.date_add FROM `' . _DB_PREFIX_ . 'order_history` AS oh 
                WHERE a.`id_order` = oh.`id_order` 
                AND oh.`id_order_state` = ' . (int) $os_id . '
                ORDER BY date_add DESC LIMIT 1
                ) AS esportato,
		IF(a.valid, 1, 0) badge_success';

        $this->_join = '
		LEFT JOIN `' . _DB_PREFIX_ . 'customer` c 
                    ON (c.`id_customer` = a.`id_customer`)
		LEFT JOIN `' . _DB_PREFIX_ . 'address` address 
                    ON address.id_address = a.id_address_delivery
		LEFT JOIN `' . _DB_PREFIX_ . 'order_state` os 
                    ON (os.`id_order_state` = a.`current_state`)
		LEFT JOIN `' . _DB_PREFIX_ . 'order_state_lang` osl 
                    ON (os.`id_order_state` = osl.`id_order_state` 
                    AND osl.`id_lang` = ' . (int) $this->context->language->id . ')';

        $this->_defaultOrderBy = 'id_order';
        $this->_defaultOrderWay = 'DESC';
        $this->_use_found_rows = true;

        $statuses = OrderState::getOrderStates((int) $this->context->language->id);
        foreach ($statuses as $status) {
            $this->statuses_array[$status['id_order_state']] = $status['name'];
        }

        $this->shopLinkType = 'shop';
        $this->shopShareDatas = Shop::SHARE_ORDER;

        if (Tools::isSubmit('id_order')) {
            // Save context (in order to apply cart rule)
            $order = new Order((int) Tools::getValue('id_order'));
            $this->context->cart = new Cart($order->id_cart);
            $this->context->customer = new Customer($order->id_customer);
        }

        parent::__construct();

        $this->fields_list = array(
            'id_order' => array(
                'title' => $this->module->l('ID', 'adminglsitalycontroller'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs'
            ),
            'esportato' => array(
                'title' => $this->module->l('Exported on', 'adminglsitalycontroller'),
                'align' => 'text-right',
                'type' => 'datetime',
                'havingFilter' => true,
            ),
            'reference' => array(
                'title' => $this->module->l('Reference', 'adminglsitalycontroller')
            ),
            'customer' => array(
                'title' => $this->module->l('Customer', 'adminglsitalycontroller'),
                'havingFilter' => true,
            ),
            'total_paid_tax_incl' => array(
                'title' => $this->module->l('Total', 'adminglsitalycontroller'),
                'align' => 'text-right',
                'type' => 'price',
                'currency' => true,
                'callback' => 'setOrderCurrency',
                'badge_success' => true
            ),
            'payment' => array(
                'title' => $this->module->l('Payment', 'adminglsitalycontroller')
            ),
            'osname' => array(
                'title' => $this->module->l('Status', 'adminglsitalycontroller'),
                'type' => 'select',
                'color' => 'color',
                'list' => $this->statuses_array,
                'filter_key' => 'os!id_order_state',
                'filter_type' => 'int',
                'order_key' => 'osname'
            ),
            'date_add' => array(
                'title' => $this->module->l('Date', 'adminglsitalycontroller'),
                'align' => 'text-right',
                'type' => 'datetime',
                'filter_key' => 'a!date_add'
            )
        );
        $this->bulk_actions = array(
            'Esporta' => array(
                'text' => $this->module->l('Export selected', 'adminglsitalycontroller'),
                'confirm' => $this->module->l('Export selected item(s)?', 'adminglsitalycontroller'),
                'icon' => 'icon-cloud-upload'
            )
        );
    }

    public function initToolbarTitle()
    {
        $this->page_header_toolbar_title = $this->module->l('GLS WebLabeling', 'adminglsitalycontroller');
        $this->toolbar_title = $this->module->l('Select order(s) to export', 'adminglsitalycontroller');
    }

    public function initToolbar()
    {
        parent::initToolbar();
        if (isset($this->toolbar_btn['new'])) {
            unset($this->toolbar_btn['new']);
        }
        $this->toolbar_btn['export'] = array(
            'href' => self::$currentIndex . '&export' . $this->table . '&token=' . $this->token,
            'desc' => $this->module->l('Export all orders not yet exported', 'adminglsitalycontroller')
        );
    }

    public function renderForm()
    {
        if (Tools::isSubmit('id_order')) {
            $id_order = (int) Tools::getValue('id_order');
            $url = $this->context->link->getAdminLink('AdminOrders') . '&id_order=' . $id_order . '&vieworder';
            Tools::redirectAdmin($url);
        }
    }

    public function renderView()
    {
        if (Tools::isSubmit('id_order')) {
            $id_order = (int) Tools::getValue('id_order');
            $url = $this->context->link->getAdminLink('AdminOrders') . '&id_order=' . $id_order . '&vieworder';
            Tools::redirectAdmin($url);
        }
    }

    public function renderList()
    {
        if ((int) $this->module->getWithDefault('GLSITALY_WL_STATUS', 0)) {
            $this->displayInformation('
<strong>' . $this->module->l('How to load a file?', 'adminglsitalycontroller') . '</strong><br />
<ul>
        <li>' . $this->module->l('Click "Export" to export a single order, or', 'adminglsitalycontroller') . '</li>
        <li>' . $this->module->l('Select multiple orders and in "Massive Actions" select "Export Selected"', 'adminglsitalycontroller') . '</li>
        <li>' . $this->module->l('After downloading the file, import it into GLS WebLabeling software', 'adminglsitalycontroller') . '</li>
</ul>');
        } else {
            $this->display = null;
            $this->displayWarning($this->module->l('This page is active only if you use GLS WebLabeling', 'adminglsitalycontroller'));
        }
        return parent::renderList();
    }

    public function displayExportLink($token, $id)
    {
        unset($token); //only to validate the module
        $tpl = $this->createTemplate('list_action_export.tpl');
        $tpl->assign(array(
            'href' => self::$currentIndex . '&token=' . $this->token . '&' . $this->identifier . '=' . $id . '&export' . $this->table . '=1',
            'action' => $this->module->l('Export', 'adminglsitalycontroller')
        ));
        return $tpl->fetch();
    }

    public static function setOrderCurrency($echo, $tr)
    {
        $order = new Order($tr['id_order']);
        return Tools::displayPrice($echo, (int) $order->id_currency);
    }

    protected function processBulkEsporta()
    {
        $result = false;
        if (is_array($this->boxes) && !empty($this->boxes)) {
            $result = $this->esporta($this->boxes);
        } else {
            $this->errors[] = $this->module->l('You must select at least one item.', 'adminglsitalycontroller');
        }
        return $result;
    }

    public function processExport()
    {
        $esporta = false;
        $id_order = (int) Tools::getValue('id_order');
        if ($id_order) {
            $this->esporta((array) $id_order);
            $esporta = true;
        } else {
            $this->getList($this->context->language->id, null, null, 0, false);
            if (!count($this->_list)) {
                return;
            }
            $arr = array();
            foreach ($this->_list as $data) {
                if (empty($data['esportato'])) {
                    $arr[] = $data['id_order'];
                }
            }
            if (count($arr)) {
                $this->esporta($arr);
                $esporta = true;
            }
        }
        if (!$esporta) {
            $this->errors[] = $this->module->l('No order to export', 'adminglsitalycontroller');
        }
        return $esporta;
    }

    protected function esporta($arr)
    {
        // clean buffer
        if (ob_get_level() && ob_get_length() > 0) {
            ob_clean();
        }

        header('Content-type: text/csv');
        header('Content-Type: application/force-download; charset=ISO-8859-1'); //UTF-8
        header('Cache-Control: no-store, no-cache');
        header('Content-disposition: attachment; filename="GLS' . date('ymdHis') . '.txt"');
        $string = '';
        $os = (int) $this->module->getWithDefault('GLSITALY_OS_EXPORT', 0);
        $id_employee = 0;
        if (isset($this->context->employee)) {
            $id_employee = (int) $this->context->employee->id;
        }
        foreach ($arr as $id_order) {
//            if (!empty($string)) {
//                $string .= "\r\n";
//            }
            $string .= $this->orderToWebLabeling($id_order) . "\r\n";
            $this->orderChangeState($id_order, $os, $id_employee);
        }
        $fd = fopen('php://output', 'wb');
        fwrite($fd, $string, Tools::strlen($string));
        @fclose($fd);
        die;
    }

    protected function orderChangeState($id_order, $os, $id_employee)
    {
        if ($os) {
            $order = new Order((int) $id_order);
            if (Validate::isLoadedObject($order)) {
                $order->setCurrentState($os, $id_employee);
            }
        }
    }

    protected function orderToWebLabeling($id_order)
    {
        $txt = '';
        $logreason = $this->module->l('Unknown', 'adminglsitalycontroller');
        $logseverity = 3;
        $logcode = 0;
        $order = new Order((int) $id_order);
        if (Validate::isLoadedObject($order)) {
            $customer = new Customer($order->id_customer);
            if (Validate::isLoadedObject($customer)) {
                $address = new Address($order->id_address_delivery);
                if (Validate::isLoadedObject($address)) {
                    $state = new State((int) ($address->id_state));
                    if (Validate::isLoadedObject($state)) {
                        $country = new Country((int) ($address->id_country));
                        if (Validate::isLoadedObject($country)) {
                            $orderstate = new OrderState($order->current_state);
                            if (Validate::isLoadedObject($orderstate)) {
                                $telefono = trim($address->phone_mobile);
                                if (empty($telefono)) {
                                    $telefono = $address->phone;
                                }
                                $txt .= $this->limitstring($customer->lastname . " " . $customer->firstname, 35, false);
                                $txt .= $this->limitstring($address->address1 . " " . $address->address2, 35, false);
                                $txt .= $this->limitstring($address->city, 30, false);
                                $txt .= $this->limitstring($address->postcode, 5, true);
                                $txt .= $this->limitstring($state->iso_code, 2, false);
                                $txt .= $this->limitstring($order->getUniqReference(), 10, false);
                                $txt .= $this->limitstring(date("ymd", strtotime((string) $order->date_add)), 6, false);
                                $txt .= $this->limitstring('1', 5, true);
                                $txt .= $this->limitstring('00', 2, true);
                                $peso = (float) $order->getTotalWeight();
                                if ($peso < 1) {
                                    $peso = 1;
                                }
                                $txt .= $this->limitstring((string) number_format($peso, 1, ",", ""), 6, true);
                                $contrassegno = $this->module->getWithDefault('GLSITALY_WL_CONTRASSSEGNO', '');
                                if (Tools::strtolower(trim($order->module)) == Tools::strtolower(trim($contrassegno))) {
                                    $txt .= $this->limitstring((string) number_format((float) $order->total_paid_tax_incl, 2, ",", ""), 10, true);
                                } else {
                                    $txt .= $this->limitstring('0000000000', 10, fase);
                                }
                                $note = trim($order->getFirstMessage() . " " . $address->other);
                                if (empty($note)) {
                                    $note = "Tel." . $telefono;
                                }
                                $txt .= $this->limitstring($note, 40, false);
                                $txt = str_pad($txt, 206, " "); //il prossimo deve essere al 207 elemento
                                $txt .= $this->limitstring("F", 1, false);
                                $txt = str_pad($txt, 222, " "); //il prossimo deve essere al x elemento
                                $txt .= $this->limitstring('00000000000', 11, fase); //importo assicurazione
                                $txt .= $this->limitstring('00000000000', 11, fase); //pesovolume in kg
                                $txt .= $this->limitstring('0', 1, fase); //tipocollo 0 normale 4 plus
                                $riferimenti = "Customer:" . $order->id_customer . "|";
                                $riferimenti .= "Order:" . $order->id . "(" . $order->getUniqReference() . ")|";
                                $riferimenti .= trim(str_replace("|", "", $order->getFirstMessage())) . "|";
                                $riferimenti .= trim(str_replace("|", "", $address->other));
                                $txt = str_pad($txt, 256, " "); //il prossimo deve essere al x elemento
                                $txt .= $this->limitstring($riferimenti, 600, false);
                                $txt = str_pad($txt, 856, " "); //il prossimo deve essere al x elemento
                                $txt .= $this->limitstring($address->other . (empty($address->other) ? "" : " - ") . $order->getFirstMessage(), 40, false);
                                $txt .= $this->limitstring("", 30, false);
                                $txt .= $this->limitstring("", 11, false);
                                $txt .= $this->limitstring("", 15, false);
                                $txt .= $this->limitstring("", 15, false);
                                $txt .= $this->limitstring($customer->email, 70, false, false);
                                $txt .= $this->limitstring($telefono, 20, false);
                                $txt .= $this->limitstring("", 50, false);
                                $contrassegno = $this->module->getWithDefault('GLSITALY_WL_CONTRASSSEGNO', '');
                                if (Tools::strtolower(trim($order->module)) == Tools::strtolower(trim($contrassegno))) {
                                    $txt .= $this->limitstring($this->module->getWithDefault('GLSITALY_WL_PAYMENT', 'CONT'), 4, false);
                                } else {
                                    $txt .= $this->limitstring("", 4, false);
                                }
                                $txt = str_pad($txt, 1298, " ");
                            } else {
                                $logcode = 8;
                                $logreason = $this->module->l('Orderstate not valid', 'adminglsitalycontroller');
                            }
                        } else {
                            $logcode = 7;
                            $logreason = $this->module->l('Country not valid', 'adminglsitalycontroller');
                        }
                    } else {
                        $logcode = 6;
                        $logreason = $this->module->l('State not valid', 'adminglsitalycontroller');
                    }
                } else {
                    $logcode = 5;
                    $logreason = $this->module->l('Address not valid', 'adminglsitalycontroller');
                }
            } else {
                $logcode = 4;
                $logreason = $this->module->l('Customer not valid', 'adminglsitalycontroller');
            }
        } else {
            $logcode = 3;
            $logreason = $this->module->l('Order not valid', 'adminglsitalycontroller');
        }
        PrestaShopLogger::addLog($this->module->l('GLS WebLabeling', 'adminglsitalycontroller') . ": " . $logreason, $logseverity, $logcode, 'Order', (isset($order->id) ? (int) $order->id : 0), true, (isset($this->context->employee->id) ? (int) $this->context->employee->id : 0));
        return $txt;
    }

    protected function limitstring($str, $limit, $isNumber = false, $strToUpper = true)
    {
        if ((bool) $isNumber) {
            $pad_string = "0";
            $pad_type = STR_PAD_LEFT;
        } else {
            $pad_string = " ";
            $pad_type = STR_PAD_RIGHT;
        }
        $unwanted_array = array(
            'Š' => 'S', 'š' => 's', 'Ž' => 'Z', 'ž' => 'z',
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A',
            'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C',
            'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E',
            'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
            'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O',
            'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U',
            'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y',
            'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a',
            'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a',
            'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e',
            'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i',
            'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n',
            'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o',
            'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u',
            'û' => 'u', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y',
            'Ğ' => 'G', 'İ' => 'I', 'Ş' => 'S', 'ğ' => 'g',
            'ı' => 'i', 'ş' => 's', 'ü' => 'u'
        );
        $utf8 = html_entity_decode($str, ENT_COMPAT, 'UTF-8');
        $strNoAccented = strtr($utf8, $unwanted_array);
        $strNoExtra = str_ireplace(array("\t", "\r", "\n", "\r\n"), " ", strip_tags($strNoAccented));
        $trim = utf8_decode(trim($strToUpper ? Tools::strtoupper($strNoExtra) : $strNoExtra));
        return Tools::substr(str_pad($trim, $limit, $pad_string, $pad_type), 0, $limit);
    }
}
