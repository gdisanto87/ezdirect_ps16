<?php
/**
 * 2017-2018 Blulab S.r.l. - http://www.blulab.net/
 *
 *  @author    Blulab S.r.l <info@blulab.net>
 *  @copyright 2017-2018 Blulab S.r.l.
 *  @license   please contact Blulab S.r.l. http://www.blulab.net/
 *  Property of Blulab s.r.l. & GLS
 */

class GlsitalyStatusModuleFrontController extends ModuleFrontController
{

    public function __construct()
    {
        parent:: __construct();
        $this->ajax = true;
        $this->ssl = true;
        $this->display_column_left = false;
        $this->display_column_right = false;
    }

    public function initContent()
    {
        header('Content-Type: application/json; charset="utf-8"');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');
        header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
        header("Pragma: no-cache");
        header("Expires: " . gmdate('D, d M Y H:i:s', time() - 3600));
        parent::initContent();
    }

    public function postProcess()
    {
        $res = false;
        if (Configuration::get('GLSITALY_SS_STATUS') && !empty(Configuration::get('GLSITALY_SS_APIKEY'))) {
            if (Tools::isSubmit('token')) {
                $token = Tools::getValue('token');
                if (Tools::isSubmit('ts')) {
                    $ts = Tools::getValue('ts');
                    $res = $this->validISO8601Date($ts);
                    if ($res) {
                        $knownToken = $this->getToken($ts);
                        $res = $this->compareToken($knownToken, $token);
                        if ($res) {
                            if (Tools::isSubmit('order') || Tools::isSubmit('reference')) {
                                $res = true;
                                if (Tools::isSubmit('order')) {
                                    $id_order = (int) Tools::getValue('order');
                                    $order = new Order($id_order);
                                } else {
                                    $reference = Tools::getValue('reference');

                                    $referenceParts = explode("#", $reference);
                                    $orderPart = -1; //package reference
                                    if (count($referenceParts) == 2) {
                                        $orderPart = ((int) $referenceParts[1]) - 1;
                                        $reference = $referenceParts[0];
                                    }
                                    $orders = Order::getByReference($reference);
                                    $nb_orders = count($orders);

                                    if ($nb_orders <= 0) {
                                        echo Tools::jsonEncode(array('error' => 1, 'message' => $this->module->l('Order reference not found', 'status')));
                                        $res = false;
                                    } else {
                                        if ($nb_orders == 1 && $orderPart == -1) {
                                            $orderPart = 0;
                                        }
                                        if (isset($orders[$orderPart])) {
                                            $order = $orders[$orderPart];
                                            $id_order = (int) $order->id;
                                        } else {
                                            if ($nb_orders > 1) {
                                                $ids = array();
                                                foreach ($orders as $order) {
                                                    $ids[] = $order->id;
                                                }
                                                echo Tools::jsonEncode(array('error' => 1, 'ids' => $ids, 'message' => $this->module->l('Multiple order with same reference', 'status')));
                                                $res = false;
                                            } else {
                                                echo Tools::jsonEncode(array('error' => 1, 'message' => $this->module->l('Order reference not found', 'status')));
                                                $res = false;
                                            }
                                        }
                                    }
                                    unset($orders);
                                }
                                if ($res) {
                                    if (validate::isLoadedObject($order)) {
                                        if (Tools::isSubmit('action')) {
                                            $action = Tools::getValue('action');
                                            switch ($action) {
                                                case 'setTracking':
                                                    if (Tools::isSubmit('trackingNumber')) {
                                                        $trackingNumber = Tools::getValue('trackingNumber');
                                                        $res = self::updateTrackingNumber($id_order, $trackingNumber);
                                                        if ($res) {
                                                            echo Tools::jsonEncode(array('error' => 0, 'message' => $this->module->l('Success', 'status')));
                                                        } else {
                                                            echo Tools::jsonEncode(array('error' => 1, 'message' => $this->module->l('Error in setTracking', 'status')));
                                                        }
                                                    } else {
                                                        echo Tools::jsonEncode(array('error' => 1, 'message' => $this->module->l('Missing trackingNumber', 'status')));
                                                    }
                                                    break;
                                                case 'setShipped':
                                                    $id_status = (int) $this->module->getWithDefault('GLSITALY_SS_STATE_SHIPPING', 0);
                                                    $orderStatus = new OrderState($id_status);
                                                    if (validate::isLoadedObject($orderStatus)) {
                                                        $res = self::updateStatus($id_order, $id_status);
                                                        if ($res) {
                                                            if (Tools::isSubmit('trackingNumber')) {
                                                                $trackingNumber = Tools::getValue('trackingNumber');
                                                                $res = self::updateTrackingNumber($id_order, $trackingNumber);
                                                                if ($res) {
                                                                    echo Tools::jsonEncode(array('error' => 0, 'message' => $this->module->l('Success', 'status')));
                                                                } else {
                                                                    echo Tools::jsonEncode(array('error' => 1, 'message' => $this->module->l('Error in setTracking', 'status')));
                                                                }
                                                            } else {
                                                                echo Tools::jsonEncode(array('error' => 0, 'message' => $this->module->l('Success', 'status')));
                                                            }
                                                        } else {
                                                            echo Tools::jsonEncode(array('error' => 1, 'message' => $this->module->l('Error in setShipped', 'status')));
                                                        }
                                                    } else {
                                                        echo Tools::jsonEncode(array('error' => 1, 'message' => $this->module->l('Missing setShipped order status ID', 'status')));
                                                    }
                                                    break;
                                                case 'setDelivered':
                                                    $id_status = (int) $this->module->getWithDefault('GLSITALY_SS_STATE_DELIVERED', 0);
                                                    $orderStatus = new OrderState($id_status);
                                                    if (validate::isLoadedObject($orderStatus)) {
                                                        $res = self::updateStatus($id_order, $id_status);
                                                        if ($res) {
                                                            echo Tools::jsonEncode(array('error' => 0, 'message' => $this->module->l('Success', 'status')));
                                                        } else {
                                                            echo Tools::jsonEncode(array('error' => 1, 'message' => $this->module->l('Error in setDelivered', 'status')));
                                                        }
                                                    } else {
                                                        echo Tools::jsonEncode(array('error' => 1, 'message' => $this->module->l('Missing setDelivered order status ID', 'status')));
                                                    }
                                                    break;
                                                default:
                                                    echo Tools::jsonEncode(array('error' => 1, 'message' => $this->module->l('Action not valid', 'status')));
                                            }
                                        } else {
                                            echo Tools::jsonEncode(array('error' => 1, 'message' => $this->module->l('Missing action', 'status')));
                                        }
                                    } else {
                                        echo Tools::jsonEncode(array('error' => 1, 'message' => $this->module->l('Order not valid', 'status')));
                                    }
                                }
                            } else {
                                echo Tools::jsonEncode(array('error' => 1, 'message' => $this->module->l('Missing order', 'status')));
                            }
                        } else {
                            echo Tools::jsonEncode(array('error' => 1, 'message' => $this->module->l('Token not valid', 'status')));
                        }
                    } else {
                        echo Tools::jsonEncode(array('error' => 1, 'message' => $this->module->l('Timestamp not valid', 'status')));
                    }
                } else {
                    echo Tools::jsonEncode(array('error' => 1, 'message' => $this->module->l('Timestamp not available', 'status')));
                }
            } else {
                echo Tools::jsonEncode(array('error' => 1, 'message' => $this->module->l('Token not available', 'status')));
            }
        } else {
            echo Tools::jsonEncode(array('error' => 1, 'message' => $this->module->l('API key not valid', 'status')));
        }
        die();
    }

    private function compareToken($known_string, $user_string)
    {
        if (function_exists('hash_equals')) {
            return hash_equals($known_string, $user_string);
        } else {
            if (Tools::strlen($known_string) != Tools::strlen($user_string)) {
                return false;
            } else {
                $res = $known_string ^ $user_string;
                $ret = 0;
                for ($i = Tools::strlen($res) - 1; $i >= 0; $i--) {
                    $ret |= ord($res[$i]);
                }
                return !$ret;
            }
        }
    }

    private function getToken($ts)
    {
        $key = Configuration::get('GLSITALY_SS_APIKEY');
        if (function_exists('hash_hmac')) {
            $function = 'hash_hmac'; //workaround - prestashop module validator doensn't accept 'hash_hmac' function (but there are no known issue and Prestashop Tool Class has not alternatives coded...)
            $token = $function("sha256", (string) $ts, (string) $key, false);
        } else {
            echo Tools::jsonEncode(array('error' => 1, 'message' => $this->module->l('php hash_hmac function not available in this server', 'status')));
            die();
        }
        return $token;
    }

//    $ts = gmdate('Y-m-d\TH:i:s.u\Z'); //nota: no millisecondi
//    $token = $this->getToken($ts);
                
    private function validISO8601Date($value)
    {
        try {
            $regex = '/^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/';
            if (preg_match($regex, $value, $parts)) {
                $anno = (int) $parts[1];
                $mese = (int) $parts[5];
                $giorno = (int) $parts[7];
                $res = checkdate($mese, $giorno, $anno);
                if ($res) {
                    $datetime1 = date_create($value);
                    $datetime2 = date_create(gmdate('c'));
                    $interval = date_diff($datetime1, $datetime2);
                    //$giorni = (int)$interval->format("%r%a");//con + e - (- futuro, + passato=
                    $giorni = $interval->format("%a"); //valore assoluto
                    if ((int) $giorni <= (int) (int) $this->module->getWithDefault('QAPLATOKENVALIDAYS', 2)) {
                        return true;
                    }
                }
            }
        } catch (Exception $e) {
            echo Tools::jsonEncode(array('error' => 1, 'message' => $e->getMessage()));
            die();
        }
        return false;
    }

    private static function updateStatus($id_order, $id_status)
    {
        $order = new Order((int) $id_order);
        if (validate::isLoadedObject($order)) {
            $new_os = new OrderState((int) $id_status, Context::getContext()->language->id);
            $old_os = $order->getCurrentOrderState();
            if ($new_os->id != $old_os->id) {
                $order->setCurrentState($id_status);
            }
            return true;
        } else {
            return false;
        }
    }

    private static function updateTrackingNumber($id_order, $trackingNumber)
    {
        $res = false;
        $order = new Order((int) $id_order);
        if (validate::isLoadedObject($order)) {
            $order->shipping_number = $trackingNumber;
            $res = $order->update();
            if ($res) {
                $id_order_carrier = Db::getInstance()->getValue('SELECT `id_order_carrier`
                        FROM `' . _DB_PREFIX_ . 'order_carrier`
                        WHERE `id_order` = ' . (int) $id_order);
                if ($id_order_carrier) {
                    $order_carrier = new OrderCarrier($id_order_carrier);
                    $order_carrier->tracking_number = $trackingNumber;
                    $res = $order_carrier->update();
                    if ($res) {
                        $customer = new Customer((int) $order->id_customer);
                        $carrier = new Carrier((int) $order->id_carrier, $order->id_lang);
                        if (Validate::isLoadedObject($customer) && Validate::isLoadedObject($carrier)) {
                            Hook::exec('actionAdminOrdersTrackingNumberUpdate', array('order' => $order, 'customer' => $customer, 'carrier' => $carrier), null, false, true, false, $order->id_shop);
                        }
                    }
                }
            }
            return $res;
        } else {
            return false;
        }
    }
}
