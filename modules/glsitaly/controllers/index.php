<?php
/**
 * 2017-2018 Blulab S.r.l. - http://www.blulab.net/
 *
 *  @author    Blulab S.r.l <info@blulab.net>
 *  @copyright 2017-2018 Blulab S.r.l.
 *  @license   please contact Blulab S.r.l. http://www.blulab.net/
 *  Property of Blulab s.r.l. & Qapla' s.r.l.
*/

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');

header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');

header('Location: ../');
exit;
