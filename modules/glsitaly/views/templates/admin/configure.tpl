{*
* 2017-2018 Blulab S.r.l. - http://www.blulab.net/
*
*  @author    Blulab S.r.l <info@blulab.net>
*  @copyright 2019-2020 Blulab S.r.l.
*  @license   please contact Blulab S.r.l. http://www.blulab.net/
*  Property of Blulab s.r.l. & GLS
*}

<div class="panel">
    <h3><i class="icon icon-credit-card"></i> {l s='Configuration' mod='glsitaly'}</h3>
    <div class="{*container*}">
        <div class="row">
            <div class="col-xs-12">
                <div class="well well-sm">
                    <h2><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/gls-logo-negative.jpg" class="logo" style="float:right;padding: 0 0 5px 1px; max-width: 180px" width="100%" />{l s='GLS Express Courier - Manage your shipments' mod='glsitaly'}</h2>
                    <p>                        
                        {l s='Do you want to automate your shipments? This module do this for free.' mod='glsitaly'}<br />
                        {l s='Exporting and updating orders, creating shipments and labels and tracking shipments won\'t be an issue anymore.' mod='glsitaly'}<br />
                        {l s='Sell&Send has been created exactly for e-commerce sellers and with it you can:' mod='glsitaly'}
                    </p>
                    <ul>
                        <li>{l s='Load orders in real time, including the ones coming from other e-commerce platforms' mod='glsitaly'}</li>
                        <li>{l s='Create shipments and printing labels with 3 clicks' mod='glsitaly'}</li>
                        <li>{l s='Monitor deliveries thanks to a user-friendly control panel' mod='glsitaly'}</li>
                        <li>{l s='Configure and customize emails to inform your customers on the shipment progress and, at the same time, to send them promotional messages' mod='glsitaly'}</li>
                        <li>{l s='Update automatically the orders status on each e-commerce platform connected with Sell&Send' mod='glsitaly'}</li>
                    </ul>
                    <p>
                        {l s='Alternatively you can use WebLabeling to export Prestashop orders in a file and to load them creating shipments in a simple and error-proof manner.' mod='glsitaly'}<br />
                        {l s='Both solutions automatically update the order status on Prestashop.' mod='glsitaly'}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="glsitalyadmin">
        <div class="row row-eq-height">
            <div class="col-md-4">
                <div class="well well-contatti">
                    <form id="module_form" class="defaultForm form-horizontal" method="post" enctype="multipart/form-data" novalidate="">
                        <div class='form-interno'>
                            <h2>{l s='Contact me' mod='glsitaly'}</h2>
                            {if isset($contattosubmit)}
                                {if $contattosubmit}
                                    <div class="alert alert-success">
                                        <strong>{l s='Thank you' mod='glsitaly'}</strong> {l s='You will be contacted as soon as possible' mod='glsitaly'}
                                    </div>
                                {else}
                                    <div class="alert alert-danger">
                                        <strong>{l s='Warning!' mod='glsitaly'}</strong> {l s='It was not possible to submit the form, please verify the data you enetered.' mod='glsitaly'}
                                    </div>
                                {/if}
                            {/if}
                            {if !isset($contattosubmit) || (isset($contattosubmit) && $contattosubmit==false)}
                                <p>
                                    {l s='If you don\'t have a GLS account yet, please fill in and send this form to receive our contact to quickly start using our services for the e-commerce.' mod='glsitaly'}
                                </p>
                                <input name="submitGlsitalyModule" value="1" type="hidden">
                                <div class="form-wrapper">
                                    <div class="form-group">
                                        <label class="control-label col-lg-2">
                                            {l s='Name' mod='glsitaly'}<sup>*</sup>
                                        </label>
                                        <div class="col-lg-10">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="icon icon-user"></i>
                                                </span>
                                                <input name="GLSITALY_CONTACT_name" value="{$contatto.name|escape:'html':'UTF-8'}" class="" type="text" autocomplete="off" required="required" placeholder="{l s='Name' mod='glsitaly'}">
                                            </div>
                                            {if $contattoErrors.name}
                                                <div class="alert alert-danger" role="alert">
                                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                    <span class="sr-only">{l s='Error' mod='glsitaly'}:</span>
                                                    {l s='Enter a name' mod='glsitaly'}
                                                </div>
                                            {/if}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-lg-2">
                                            {l s='Address' mod='glsitaly'}<sup>*</sup>
                                        </label>
                                        <div class="col-lg-10">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="icon icon-road"></i>
                                                </span>
                                                <input name="GLSITALY_CONTACT_address" value="{$contatto.address|escape:'html':'UTF-8'}" class="" type="text" autocomplete="off" required="required" placeholder="{l s='Address' mod='glsitaly'}">
                                            </div>
                                            {if $contattoErrors.address}
                                                <div class="alert alert-danger" role="alert">
                                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                    <span class="sr-only">{l s='Error' mod='glsitaly'}:</span>
                                                    {l s='Enter an address' mod='glsitaly'}
                                                </div>
                                            {/if}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-lg-2">
                                            {l s='City' mod='glsitaly'}<sup>*</sup>
                                        </label>
                                        <div class="col-lg-10">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="icon icon-road"></i>
                                                </span>
                                                <input name="GLSITALY_CONTACT_city" value="{$contatto.city|escape:'html':'UTF-8'}" class="" type="text" autocomplete="off" required="required" placeholder="{l s='City' mod='glsitaly'}">
                                            </div>
                                            {if $contattoErrors.city}
                                                <div class="alert alert-danger" role="alert">
                                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                    <span class="sr-only">{l s='Error' mod='glsitaly'}:</span>
                                                    {l s='Enter a city' mod='glsitaly'}
                                                </div>
                                            {/if}
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="control-label col-lg-2">
                                            {l s='ZIP' mod='glsitaly'}<sup>*</sup>
                                        </label>
                                        <div class="col-lg-10">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="icon icon-road"></i>
                                                </span>
                                                <input name="GLSITALY_CONTACT_zip" value="{$contatto.zip|escape:'html':'UTF-8'}" class="" type="text" autocomplete="off" required="required" placeholder="{l s='ZIP' mod='glsitaly'}">
                                            </div>
                                            {if $contattoErrors.zip}
                                                <div class="alert alert-danger" role="alert">
                                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                    <span class="sr-only">{l s='Error' mod='glsitaly'}:</span>
                                                    {l s='Enter a valid zip' mod='glsitaly'}
                                                </div>
                                            {/if}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-lg-2">
                                            {l s='State' mod='glsitaly'}<sup>*</sup>
                                        </label>
                                        <div class="col-lg-10">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="icon icon-road"></i>
                                                </span>
                                                <select  name="GLSITALY_CONTACT_state" placeholder="{l s='State' mod='glsitaly'}">
                                                    <option value="">{l s='State' mod='glsitaly'}</option>
                                                    {foreach $province as $sigla=>$provincia}
                                                        <option value="{$sigla|escape:'html':'UTF-8'}" {if $contatto.state==$sigla}selected="selected"{/if}>{$provincia|escape:'html':'UTF-8'}</option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                            {if $contattoErrors.state}
                                                <div class="alert alert-danger" role="alert">
                                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                    <span class="sr-only">{l s='Error' mod='glsitaly'}:</span>
                                                    {l s='Enter a state' mod='glsitaly'}
                                                </div>
                                            {/if}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-lg-2">
                                            {l s='E-mail' mod='glsitaly'}<sup>*</sup>
                                        </label>
                                        <div class="col-lg-10">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="icon icon-envelope"></i>
                                                </span>
                                                <input name="GLSITALY_CONTACT_email" value="{$contatto.email|escape:'html':'UTF-8'}" class="" type="text" autocomplete="off" required="required" placeholder="{l s='E-mail' mod='glsitaly'}">
                                            </div>
                                            {if $contattoErrors.email}
                                                <div class="alert alert-danger" role="alert">
                                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                    <span class="sr-only">{l s='Error' mod='glsitaly'}:</span>
                                                    {l s='Enter a valid email address' mod='glsitaly'}
                                                </div>
                                            {/if}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-lg-2">
                                            {l s='Phone' mod='glsitaly'}{*<sup>*</sup>*}
                                        </label>
                                        <div class="col-lg-10">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="icon icon-phone"></i>
                                                </span>
                                                <input name="GLSITALY_CONTACT_phone" value="{$contatto.phone|escape:'html':'UTF-8'}" class="" type="text" autocomplete="off" required="required" placeholder="{l s='Phone' mod='glsitaly'}">
                                            </div>
                                            {if $contattoErrors.phone}
                                                <div class="alert alert-danger" role="alert">
                                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                    <span class="sr-only">{l s='Error' mod='glsitaly'}:</span>
                                                    {l s='Enter a valid phone number' mod='glsitaly'}
                                                </div>
                                            {/if}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-lg-2">
                                            {l s='Person to contact' mod='glsitaly'}<sup>*</sup>
                                        </label>
                                        <div class="col-lg-10">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="icon icon-user"></i>
                                                </span>
                                                <input name="GLSITALY_CONTACT_referent" value="{$contatto.referent|escape:'html':'UTF-8'}" class="" type="text" autocomplete="off" required="required" placeholder="{l s='Person to contact' mod='glsitaly'}">
                                            </div>
                                            {if $contattoErrors.referent}
                                                <div class="alert alert-danger" role="alert">
                                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                    <span class="sr-only">{l s='Error' mod='glsitaly'}:</span>
                                                    {l s='Enter a contatct referent' mod='glsitaly'}
                                                </div>
                                            {/if}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-12">
                                            <div class="input-group">
                                                <input name="GLSITALY_CONTACT_privacy" id="GLSITALY_CONTACT_privacy" value="1" type="checkbox">
                                                {l s='Having read the' mod='glsitaly'} <a id="GLSITALY_CONTACT_privacy_popup"  title="{l s='GLS ITALY - Privacy informations' mod='glsitaly'}" href="#GLSITALY_CONTACT_privacy_popup_content">{l s='information' mod='glsitaly'}</a>
                                                {l s='I' mod='glsitaly'}
                                                <strong>{l s='give permission' mod='glsitaly'}</strong>
                                                {l s='for communications (and related processing) of your personal data to third parties mentioned in the information for sending information and promotional material.' mod='glsitaly'}<sup>*</sup>
                                            </div>
                                            {if $contattoErrors.privacy}
                                                <div class="alert alert-danger" role="alert">
                                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                    <span class="sr-only">{l s='Error' mod='glsitaly'}:</span>
                                                    {l s='Consent the privacy statement' mod='glsitaly'}
                                                </div>
                                            {/if}
                                        </div>
                                    </div>
                                </div>
                            {/if}
                        </div>
                        {if !isset($contattosubmit) || (isset($contattosubmit) && $contattosubmit==false)}
                            <div class="form-buttons">
                                <div class="form-wrapper">
                                    <div class="form-group">
                                        <div class="col-xs-8 text-left"><em><sup>*</sup>{l s='All fields are mandatory' mod='glsitaly'}</em></div>
                                        <div class="col-xs-4">
                                            <div class="text-right">
                                                <button type="submit" disabled="disabled" id="GLSITALY_CONTACT_submit" class="btn btn-default"><i class="process-icon-next"></i> {l s='Submit' mod='glsitaly'}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {/if}
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <div class="well well-ss">
                    <form id="module_form" class="defaultForm form-horizontal" method="post" enctype="multipart/form-data" novalidate="">
                        <div class='form-interno'>
                            <h2>{l s='I want to use GLS Sell&Send' mod='glsitaly'}</h2>
                            <p><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/logoglssellandsend.jpg" class="logo" style="float:left;padding:5px;max-width: 150px;" width="100%" /> {l s='Sell&Send is an innovative and complete solution for an e-commerce seller! Using Sell&Send you can automate the shipment process integrating at the same time different e-commerce platforms used for selling goods online.' mod='glsitaly'}<br>
                                {l s='Activate this option to connect your Prestashop with Sell&Send. ' mod='glsitaly'}<br>
                                {l s='If you don’t have a Sell&Send account yet, please contact your GLS branch to start right now!' mod='glsitaly'}</p>
                            <input name="submitGlsitalyModule" value="2" type="hidden">
                            <div class="form-wrapper">
                                <div class="form-group">
                                    <label class="control-label col-lg-2">
                                        {l s='Endpoint' mod='glsitaly'}
                                    </label>
                                    <div class="col-lg-10">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="icon icon-link"></i>
                                            </span>
                                            <input name="GLSITALY_SELLANDSEND_endpoint" value="{$server2server|escape:'html':'UTF-8'}" class="" type="text" autocomplete="off" readonly="readonly" placeholder="{l s='Endpoint' mod='glsitaly'}">
                                        </div>
                                        <p class="help-block">
                                            <i class="icon icon-info-sign"></i> {l s='Copy and paste on Sell & Send App' mod='glsitaly'}
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">
                                        {l s='API Key' mod='glsitaly'}
                                    </label>
                                    <div class="col-lg-10">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="icon icon-lock"></i>
                                            </span>
                                            <input name="GLSITALY_SELLANDSEND_apikey" value="{$sellandsend.apikey|escape:'html':'UTF-8'}" class="" type="text" autocomplete="off" required="required" placeholder="{l s='API Key' mod='glsitaly'}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">
                                        {l s='Sent' mod='glsitaly'}
                                    </label>
                                    <div class="col-lg-10">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="icon icon-share"></i>
                                            </span>
                                            <select  name="GLSITALY_SELLANDSEND_shipping" placeholder="{l s='Sent' mod='glsitaly'}">
                                                {foreach $stati_ordine as $id=>$nome}
                                                    <option value="{$id|escape:'html':'UTF-8'}" {if $sellandsend.shipping==$id}selected="selected"{/if}>{$nome|escape:'html':'UTF-8'}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">
                                        {l s='Delivered' mod='glsitaly'}
                                    </label>
                                    <div class="col-lg-10">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="icon icon-check"></i>
                                            </span>
                                            <select  name="GLSITALY_SELLANDSEND_delivered" placeholder="{l s='Delivered' mod='glsitaly'}">
                                                {foreach $stati_ordine as $id=>$nome}
                                                    <option value="{$id|escape:'html':'UTF-8'}" {if $sellandsend.delivered==$id}selected="selected"{/if}>{$nome|escape:'html':'UTF-8'}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">
                                        {l s='Status' mod='glsitaly'}
                                    </label>
                                    <div class="col-lg-10">
                                        <div class="input-group">
                                            <span class="switch prestashop-switch fixed-width-lg"> 
                                                <input type="radio" name="GLSITALY_SELLANDSEND_status" id="GLSITALY_SELLANDSEND_status_on" value="1"{if $sellandsend.status == 1} checked="checked"{/if}/>
                                                {strip}
                                                    <label for="GLSITALY_SELLANDSEND_status_on">
                                                        {l s='Enabled' mod='glsitaly'}
                                                    </label>
                                                {/strip}
                                                <input type="radio" name="GLSITALY_SELLANDSEND_status" id="GLSITALY_SELLANDSEND_status_off" value="0" {if $sellandsend.status == 0} checked="checked"{/if}/>
                                                {strip}
                                                    <label for="GLSITALY_SELLANDSEND_status_off">
                                                        {l s='Disabled' mod='glsitaly'}
                                                    </label>
                                                {/strip}
                                                <a class="slide-button btn"></a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-buttons">
                            <div class="form-group">
                                <label class="control-label col-lg-2"></label>
                                <div class="col-lg-10">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-default"><i class="process-icon-save"></i> {l s='Save' mod='glsitaly'}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <div class="well well-wl">
                    <form id="module_form" class="defaultForm form-horizontal" method="post" enctype="multipart/form-data" novalidate="">
                        <div class='form-interno'>
                            <h2>{l s='I want to use GLS WebLabeling' mod='glsitaly'}</h2>
                            <p>
                                {l s='With WebLabeling you can simplify the shipment process.' mod='glsitaly'}<br>
                                {l s='Activate this option to start exporting orders in a file so that you can directly load it onto WebLabeling creating the shipments you need in a simple and error-proof manner.' mod='glsitaly'}<br>
                                {l s='If you don\'t have a WebLabeling account yet, please contact your GLS branch to start right now!' mod='glsitaly'}
                            </p>
                            <input name="submitGlsitalyModule" value="3" type="hidden">
                            <div class="form-wrapper">
                                <div class="form-group">
                                    <label class="control-label col-lg-2">
                                        {l s='Cash on delivery' mod='glsitaly'}
                                    </label>
                                    <div class="col-lg-10">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="icon icon-check"></i>
                                            </span>
                                            <select  name="GLSITALY_WebLabeling_modulocontrasegno" placeholder="{l s='Cash on delivery' mod='glsitaly'}">
                                                <option value="">{l s='The store does not accept cash on delivery' mod='glsitaly'}</option>
                                                {foreach $metodi_pagamento as $id=>$nome}
                                                    <option value="{$id|escape:'html':'UTF-8'}" {if $WebLabeling.modulocontrasegno==$id}selected="selected"{/if}>{$nome|escape:'html':'UTF-8'}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">
                                        {l s='Payment' mod='glsitaly'}
                                    </label>
                                    <div class="col-lg-10">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="icon icon-usd"></i>
                                            </span>
                                            <select name="GLSITALY_WebLabeling_payment">
                                                {foreach $WebLabeling_payment as $sigla=>$testo}
                                                    <option value="{$sigla|escape:'html':'UTF-8'}" {if $WebLabeling.payment==$sigla}selected="selected"{/if}>{$testo|escape:'html':'UTF-8'}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">
                                        {l s='Status' mod='glsitaly'}
                                    </label>
                                    <div class="col-lg-10">
                                        <div class="input-group">
                                            <span class="switch prestashop-switch fixed-width-lg"> 
                                                <input type="radio" name="GLSITALY_WebLabeling_status" id="GLSITALY_WebLabeling_status_on" value="1"{if $WebLabeling.status == 1} checked="checked"{/if}/>
                                                {strip}
                                                    <label for="GLSITALY_WebLabeling_status_on">
                                                        {l s='Enabled' mod='glsitaly'}
                                                    </label>
                                                {/strip}
                                                <input type="radio" name="GLSITALY_WebLabeling_status" id="GLSITALY_WebLabeling_status_off" value="0" {if $WebLabeling.status == 0} checked="checked"{/if}/>
                                                {strip}
                                                    <label for="GLSITALY_WebLabeling_status_off">
                                                        {l s='Disabled' mod='glsitaly'}
                                                    </label>
                                                {/strip}
                                                <a class="slide-button btn"></a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-buttons">
                            <div class="form-group">
                                <label class="control-label col-lg-2"></label>
                                <div class="col-lg-10">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-default"><i class="process-icon-save"></i> {l s='Save' mod='glsitaly'}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                &nbsp;
            </div>
        </div>
        <div class="well well-sm">
            <div class="row">
                <div class="col-xs-12 text-right">
                    <p><a class="btn-large btn btn-info" role="button" target="_blank" href="https://addons.prestashop.com/it/contact-us?id_product=28374"> {l s='Need help' mod='glsitaly'} <i class="icon icon-question-sign"></i> </a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="GLSITALY_CONTACT_privacy_popup_content">
    <h3>{l s='Privacy notice under article 13 of Legislative Decree No 196/03 and EU Regulation 679/2016' mod='glsitaly'}</h3>
    <p>{l s='Personal data gathered in the registration form are processed by Prestashop and General Logistics Systems SpA (“GLS”) to provide the service that you request and, with your permission – that you can agree or not at the bottom of the form – to allow GLS to pass your data to third parties (for example, companies belonging to GLS Group, franchisees and GLS commercial partners) and thus allow these parties to send you information and promotional material related to the services they offer. The full list of these commercial partners is available upon request to the email address ' mod='glsitaly'} 
        <a href="mailto:customerservice@gls-italy.com">customerservice@gls-italy.com</a>.<br>
        {l s='GLS, whenever necessary to satisfy your request, will make use of third parties like companies of the Group and franchisees, who act as autonomous data controllers. With this purpose, personal data treatment is necessary to execute pre-contractual measures adopted upon your request, respecting law regulations, therefore, you will not be asked to provide any agreement; your agreement instead, if you decide to give it, will be the juridical ground for further marketing activities done by individual subjects' mod='glsitaly'} <br>
        {l s='Purpose above described will be pursued through both automated and not automated means (emails, text messages, postal mail etc.)' mod='glsitaly'}.
    </p>
    <p>{l s='Given the nature of the data and treatment purpose, providing personal data in the fields with an asterisk (i.e. name, location and e-mail) is mandatory to satisfy your request. Being these data missing, it will not be possible to give you any response. It is entirely up to you, however, whether providing data in the fields without asterisk and agreement for marketing purpose: not providing these data will not preclude in any way your registration and the possibility to obtain the requested service' mod='glsitaly'}.<br>
        {l s='We inform you that, in order to execute the activities described in this notice, data can be passed to the companies authorized on the territory to manage the services offered by GLS Italy. These subjects will deal data as supervisors or autonomous data controllers. In order to obtain information on this communication single recipient, please write to the e-mail address' mod='glsitaly'} 
        <a href="mailto:customerservice@gls-italy.com">customerservice@gls-italy.com</a>.<br>
        {l s='Data will not be spread and will be stored for 2 years unless you decide to cancel your registration and/or revoke your agreement given for marketing purpose. We inform you that, anyway, personal data gathered can be stored, in anonymized way, for a time longer than the time mentioned in this paragraph, for analysis and statistics purpose' mod='glsitaly'}.<br>
        {l s='Article 13 of Legislative Decree No 196/03 and articles 13 and following ones of EU Regulation 679/2016 assigned to involved parties specific rights. In particular, you can obtain that the data controller confirms or denies the existence of your personal data, you can ask to access the data in order to know, for example, where the data came from and how and why it is processed. You can have the data deleted, anonymized or frozen if it has been processed unlawfully.You can have the data updated, corrected or, if you wish, added to. You can also object, for legitimate reasons, to your data being processed. You can always exercise your right to object, even in part, to being sent commercial and advertising material. Moreover, you have the right, when you believe your data have been violated, to present a claim to data protection Guarantor. To exercise your rights, please email us at ' mod='glsitaly'} 
        <a href="mailto:customerservice@gls-italy.com">customerservice@gls-italy.com</a>.<br> 
        {l s='Data controller is PrestaShop S.A, in rue d’Amsterdam n. 12, 75009 Paris and GLS Italy S.p.A., in Via Basento n. 19, 20098 San Giuliano Milanese, Milano. Data supervisor is the acting manager of the GLS Italy legal and company affairs office, who can provide you with a full list of the other data supervisors writing to ' mod='glsitaly'} 
        <a href="mailto:customerservice@gls-italy.com">customerservice@gls-italy.com</a>.
    </p>
    <a href="javascript:;" onclick="$.fancybox.close();"><i class="icon icon-remove-circle"></i> {l s='Close window' mod='glsitaly'}</a>
</div>
<script>
    $(document).ready(function () {
        $('#GLSITALY_CONTACT_privacy').change(function (e) {
            var res = $(this).is(':checked');
            $("#GLSITALY_CONTACT_submit").prop("disabled", !res);
        });
        $("#GLSITALY_CONTACT_privacy_popup").fancybox({
            autoDimensions: false,
            autoSize: false,
            width: 700,
            autoHeight: true,
        });
    });
</script>