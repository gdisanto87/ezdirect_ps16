{*
 * 2017-2018 Blulab S.r.l. - http://www.blulab.net/
 *
 *  @author    Blulab S.r.l <info@blulab.net>
 *  @copyright 2019-2020 Blulab S.r.l.
 *  @license   please contact Blulab S.r.l. http://www.blulab.net/
 *  Property of Blulab s.r.l. & GLS
*}
<div class="box box-small clearfix">
    <p>
        <img src="{$module_dir|escape:'htmlall':'UTF-8'}logo.png" class="logo" width="64" height="64" /> <img src="{$icon|escape:'html':'UTF-8'}" alt="{$status|escape:'html':'UTF-8'}" > {$statusDate|escape:'html':'UTF-8'} <strong>{$status|escape:'html':'UTF-8'}</strong> <a href="{$trackingUrl|escape:'html':'UTF-8'}" target="_blank" class="btn btn-primary"><i class="icon icon-tasks"></i>&nbsp;{l s='Tracking' mod='glsitaly'}<i class="icon-chevron-right right"></i></a>
    </p>
</div>
