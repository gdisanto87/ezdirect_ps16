<?php
/* SSL Management */
$useSSL = true;

$php_self_module = 'modules/mytickets/tickets.php';

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');
include_once(dirname(__FILE__).'/mytickets.php');

$errors = array();


	if ($cookie->isLogged())
	{
		
		
		
		$customer = $cookie->id_customer;
		$language = $cookie->id_lang;
		
		if(!Tools::getValue('id_customer_thread')) {
		
			$tickets = Db::getInstance()->ExecuteS("SELECT 
			ct.id_customer_thread, ct.id_contact, ct.id_lang, ct.id_customer, ct.status, ct.email, ct.date_add, ct.date_upd, ct.date_upd AS last_msg, cl.name, ct.id_employee, (CASE WHEN e.id_employee = 19 THEN 'Staff' ELSE e.firstname END) firstname, (CASE WHEN e.id_employee = 19 THEN 'Ezdirect' ELSE e.lastname END) lastname 
			
			FROM "._DB_PREFIX_."customer_thread ct 
			
				JOIN (SELECT id_customer_thread, date_add FROM "._DB_PREFIX_."customer_message ORDER BY id_customer_thread DESC) cm ON ct.id_customer_thread = cm.id_customer_thread 
				JOIN "._DB_PREFIX_."contact_lang cl ON ct.id_contact = cl.id_contact 
				LEFT JOIN employee e ON ct.id_employee = e.id_employee WHERE ct.id_customer = ".$customer." AND cl.id_lang = ".$language." GROUP BY ct.id_customer_thread 
			
			UNION	
				
			SELECT 
			ft.id_thread AS id_customer_thread, ft.tipo_richiesta AS id_contact, 5 AS id_lang, ft.id_customer, ft.status, ft.email, ft.date_add, ft.date_upd, ft.date_upd AS last_msg, ft.tipo_richiesta AS name, ft.id_employee, (CASE WHEN e.id_employee = 19 THEN 'Staff' ELSE e.firstname END) firstname, (CASE WHEN e.id_employee = 19 THEN 'Ezdirect' ELSE e.lastname END) lastname 
			
			FROM "._DB_PREFIX_."form_prevendita_thread ft 
			
				JOIN (SELECT id_thread, date_add FROM "._DB_PREFIX_."form_prevendita_message ORDER BY date_add DESC) fm ON ft.id_thread = fm.id_thread 
				LEFT JOIN employee e ON ft.id_employee = e.id_employee WHERE ft.id_customer = ".$customer." GROUP BY ft.id_thread ORDER BY id_customer_thread DESC	
				
				");
			
				
			$smarty->assign(array(
			'tickets' => $tickets,
			));
		
		}
		
		else {
			if(Tools::getIsset('type') && Tools::getValue('type') == 'pv')
			{	
				$ticket_data = Db::getInstance()->getRow("SELECT ft.id_thread AS id_customer_thread, ft.tipo_richiesta AS id_contact, ft.token AS token, '' AS id_order, 5 as id_lang, ft.id_customer, ft.status, ft.email, ft.id_employee, ft.date_add, ft.date_upd, fm.date_add AS last_msg, ft.tipo_richiesta as name, pl.name AS product FROM "._DB_PREFIX_."form_prevendita_thread ft JOIN (SELECT id_thread, date_add FROM "._DB_PREFIX_."form_prevendita_message ORDER BY date_add DESC) fm ON ft.id_thread = fm.id_thread LEFT JOIN (SELECT * FROM "._DB_PREFIX_."product_lang WHERE id_lang = 5) pl ON ft.category = pl.id_product WHERE ft.id_customer = ".$customer." AND ft.id_thread = ".Tools::getValue('id_customer_thread')." GROUP BY ft.id_thread");
				
			}
			else
			{	
				$ticket_data = Db::getInstance()->getRow("SELECT ct.id_customer_thread, ct.id_contact, ct.token AS token, ct.id_order, ct.id_lang, ct.id_customer, ct.status, ct.email, ct.id_employee, ct.date_add, ct.date_upd, cm.date_add AS last_msg, cl.name, pl.name AS product FROM "._DB_PREFIX_."customer_thread ct JOIN (SELECT id_customer_thread, date_add FROM "._DB_PREFIX_."customer_message ORDER BY date_add DESC) cm ON ct.id_customer_thread = cm.id_customer_thread JOIN "._DB_PREFIX_."contact_lang cl ON ct.id_contact = cl.id_contact LEFT JOIN (SELECT * FROM "._DB_PREFIX_."product_lang WHERE id_lang = 5) pl ON ct.id_product = pl.id_product WHERE ct.id_customer = ".$customer." AND ct.id_customer_thread = ".Tools::getValue('id_customer_thread')." AND cl.id_lang = ".$language." GROUP BY ct.id_customer_thread");
			}
			$dati_fattura = Db::getInstance()->getRow("SELECT * FROM fattura WHERE id_riga = ".$ticket_data['id_order']."");
			
			$anno = substr($ticket_data['date_add'],2,2);
			if($ticket_data['id_contact'] == 2) {
				$sigla = "A";
			}
			else if($ticket_data['id_contact'] == 4) {
				$sigla = "T";
			}
			else if($ticket_data['id_contact'] == 3) {
				$sigla = "V";
			}
			else if($ticket_data['id_contact'] == 6) {
				$sigla = "R";
			}
			else if($ticket_data['id_contact'] == 9) {
				$sigla = "S";
			}
			else if($ticket_data['id_contact'] == 8) {
				$sigla = "D";
			}
			else if($ticket_data['id_contact'] == 'preventivo') {
				$sigla = "P";
			}
			else if($ticket_data['id_contact'] == 'tirichiamiamonoi') {
				$sigla = "R";
			}
			
			if($ticket_data['id_contact'] == 9) {
				$rma_data = Db::getInstance()->getRow("SELECT * FROM rma WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
				$ticket_data['rma_type'] = $rma_data['rma_type'];
				$ticket_data['rma_shipping'] = $rma_data['rma_shipping'];
				$rma_address_data = Db::getInstance()->getRow("SELECT * FROM address a JOIN state s ON a.id_state = s.id_state WHERE id_address = ".$rma_data['id_address']."");
				$rma_address = $rma_address_data['address1']." - ".$rma_address_data['postcode']." ".$rma_address_data['city']." (".$rma_address_data['iso_code'].")";
				$ticket_data['rma_address'] = $rma_address;
			}
			
			$id_ticket = $sigla.$anno.$ticket_data['id_customer_thread'];
			$message_table = '';
			
			if(Tools::getIsset('type') && Tools::getValue('type') == 'pv')
			{	
			
			$messages = Db::getInstance()->ExecuteS("SELECT fm.id_message AS id_message, fm.id_thread AS id_customer_thread, fm.id_employee AS id_employee, fm.message AS message, fm.file_name AS file_name, fm.date_add AS date_add, ft.category AS id_product, '' AS id_order, ft.status AS status, ft.tipo_richiesta AS contact, e.firstname AS firstname, e.lastname AS lastname, pl.name AS product
			FROM "._DB_PREFIX_."form_prevendita_message fm 

				JOIN "._DB_PREFIX_."form_prevendita_thread ft ON fm.id_thread = ft.id_thread 
				LEFT JOIN "._DB_PREFIX_."employee e ON fm.id_employee = e.id_employee 
				LEFT JOIN (SELECT * FROM "._DB_PREFIX_."product_lang WHERE id_lang = 5) pl ON ft.category = pl.id_product 
			WHERE ft.id_customer = ".$customer." AND fm.id_thread = ".Tools::getValue('id_customer_thread')." ORDER BY fm.id_message DESC");
			
				foreach($messages as $messaggiot)
				{
					$allg = '';
					
					if(!empty($messaggiot['file_name'])) {
							
						$allegati = explode(";",$messaggiot['file_name']);
						$nall = 1;
						foreach($allegati as $allegato) {
							
							
							if(strpos($allegato, ":::")) {
				
								$parti = explode(":::", $allegato);
								$nomevero = $parti[1];
								
							}
				
							else {
								$nomevero = $allegato;
				
							}
				
							if($allegato == "") { } else {
								if($nall == 1) { } else { echo " - "; }
								
								
									$allg .= '<a href="modules/mytickets/tickets_att.php?openAllegato=y&t=pv&id_customer='.$cookie->id_customer.'&id_ticket='.Tools::getValue('id_customer_thread').'&filename='.$allegato.'"><span style="color:red">'.$nomevero.'</span></a>&nbsp;&nbsp;&nbsp;';
									$nall++;
								
							}
						}
					}
					
					if($messaggiot['id_employee'] == 19)
					{
						$messaggiot['firstname'] = 'Staff';
						$messaggiot['lastname'] = 'Ezdirect';
					}
					
					$message_table .= "<table style='width:100%; 
					border:1px solid #dfd5c3; 
					".($messaggiot['id_employee'] == 0 ? 'background-color: #fffff0;' : 'background-color: #ffecf2;')." margin-bottom:15px'>
						<tr>
							<td style='width:15%'><strong>Da</strong></td>
							<td style='font-size:14px'><strong>".($messaggiot['id_employee'] == 0 ? $cookie->customer_firstname.' '.$cookie->customer_lastname : $messaggiot['firstname'].' '.$messaggiot['lastname']).'</strong></td>
						</tr>
						<tr>
							<td><strong>Data</strong></td>
							<td>'.date('d/m/Y, H:i:s', strtotime($messaggiot['date_add'])).'</td>
						</tr>
				
						<tr>
							<td><strong>Messaggio</strong></td>
							<td>'.$messaggiot['message'].'</td>
						</tr>
						
						'.(!empty($messaggiot['file_name']) ? '<tr>
							<td><strong>Allegati</strong></td>
							<td>'.$allg.'</td>
						</tr>' : '').'
					</table>';
							
				}
			
			}
			else
			{	
				$messages = Db::getInstance()->ExecuteS("SELECT cm.id_customer_message AS id_message, cm.id_customer_thread AS id_customer_thread, cm.id_employee AS id_employee, cm.message AS message, cm.file_name AS file_name, cm.date_add AS date_add, ct.id_product AS id_product, ct.id_order AS id_order, ct.status AS status, cl.name AS contact, e.firstname AS firstname, e.lastname AS lastname, pl.name AS product 
				FROM "._DB_PREFIX_."customer_message cm 
					JOIN "._DB_PREFIX_."customer_thread ct ON cm.id_customer_thread = ct.id_customer_thread 
					LEFT JOIN "._DB_PREFIX_."employee e ON cm.id_employee = e.id_employee 
					LEFT JOIN (SELECT * FROM "._DB_PREFIX_."product_lang WHERE id_lang = 5) pl ON ct.id_product = pl.id_product 
					JOIN "._DB_PREFIX_."contact_lang cl ON ct.id_contact = cl.id_contact 
				WHERE ct.id_customer = ".$customer." AND cm.id_customer_thread = ".Tools::getValue('id_customer_thread')." AND cl.id_lang = ".$language." ORDER BY cm.id_customer_message DESC");
				
				foreach($messages as $messaggiot)
				{
					$allg = '';
					
					if(!empty($messaggiot['file_name'])) {
							
						$allegati = explode(";",$messaggiot['file_name']);
						$nall = 1;
						foreach($allegati as $allegato) {
							
							
							if(strpos($allegato, ":::")) {
				
								$parti = explode(":::", $allegato);
								$nomevero = $parti[1];
								
							}
				
							else {
								$nomevero = $allegato;
				
							}
				
							if($allegato == "") { } else {
								if($nall == 1) { } else { echo " - "; }
								
								
									$allg .= '<a href="modules/mytickets/tickets_att.php?openAllegato=y&t=t&id_customer='.$cookie->id_customer.'&id_ticket='.Tools::getValue('id_customer_thread').'&filename='.$allegato.'"><span style="color:red">'.$nomevero.'</span></a>&nbsp;&nbsp;&nbsp;';
									$nall++;
								
							}
						}
					}
					
					if($messaggiot['id_employee'] == 19)
					{
						$messaggiot['firstname'] = 'Staff';
						$messaggiot['lastname'] = 'Ezdirect';
					}
					
					$message_table .= "<table style='width:100%; 
					border:1px solid #dfd5c3; 
					".($messaggiot['id_employee'] == 0 ? 'background-color: #fffff0;' : 'background-color: #ffecf2;')." margin-bottom:15px'>
						<tr>
							<td style='width:15%'><strong>Da</strong></td>
							<td style='font-size:14px'><strong>".($messaggiot['id_employee'] == 0 ? $cookie->customer_firstname.' '.$cookie->customer_lastname : $messaggiot['firstname'].' '.$messaggiot['lastname']).'</strong></td>
						</tr>
						<tr>
							<td><strong>Data</strong></td>
							<td>'.date('d/m/Y, H:i:s', strtotime($messaggiot['date_add'])).'</td>
						</tr>
				
						<tr>
							<td><strong>Messaggio</strong></td>
							<td>'.html_entity_decode($messaggiot['message']).'</td>
						</tr>
						
						'.(!empty($messaggiot['file_name']) ? '<tr>
							<td><strong>Allegati</strong></td>
							<td>'.$allg.'</td>
						</tr>' : '').'
					</table>';
							
				}
			}
			if($ticket_data['id_employee'] == 0) {
				$in_charge_to = '-';
			} else {
				$employee = Db::getInstance()->getRow("SELECT firstname, lastname FROM employee WHERE id_employee = '".$ticket_data['id_employee']."'");
				$in_charge_to = $employee['firstname']." ".$employee['lastname'];
				
				if($ticket_data['id_employee'] == 19)
					$in_charge_to = 'Staff Ezdirect';
			}
			
			$smarty->assign(array(
			'id_ticket' => $id_ticket,
			'ticket_data' => $ticket_data,
			'id_fattura' => $dati_fattura['id_fattura'],
			'data_fattura' => Tools::displayDate($dati_fattura['data_fattura'], 5),
			'in_charge_to' => $in_charge_to,
			'messages' => $message_table,
			));
		
		}
		
		
		if (_THEME_NAME_ == 'ez20')
			$smarty->display(dirname(__FILE__).'/tickets-ez20.tpl');
		else
			$smarty->display(dirname(__FILE__).'/tickets.tpl');
		
	}
	
	else {
		$errors[] = Tools::displayError('You must be logged in to see your tickets.'); 
		$smarty->assign(array(
			'errors' => $errors
		));
	}

	include(dirname(__FILE__).'/../../footer.php');
