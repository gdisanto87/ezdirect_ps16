<?php

class MyTickets extends Module
{
	function __construct()
	{
		$this->name = 'mytickets';
		$this->tab = 'front_office_features';
		$this->version = '1.1';
		$this->author = 'Losna';
		$this->need_instance = 0;

		parent::__construct();

		$this->displayName = $this->l('MyTickets');
		$this->description = $this->l('Allow your users to see their customer threads in their account');
		$this->secure_key = Tools::encrypt($this->name);
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall MyTickets?');
	}

	public function install()
	{
        return (parent::install() AND $this->registerHook('customerAccount'));
	}

	function uninstall()
	{
		if (!parent::uninstall())
			return false;
		return true;
	}
	

		public function hookCustomerAccount($params)
	{
		global $smarty;
		
		
		return $this->display(__FILE__, 'my-account.tpl');
		
	}
	
	
}
