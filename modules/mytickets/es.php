<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{mytickets}prestashop>my-account_417246b8b34b186642f2263751e66c16'] = 'Mi tickets';
$_MODULE['<{mytickets}prestashop>mytickets_e1ab6f12cbb3f04f3b36c267c4dccccf'] = 'MyTickets';
$_MODULE['<{mytickets}prestashop>mytickets_4c0b96fb3effb0aa2f1109560bf3f1a5'] = 'Permite a los usuarios de ver los tickets en su cuenta';
$_MODULE['<{mytickets}prestashop>mytickets_bf9a8bc87fbfb6b4364217bb33604df9'] = 'Está seguro de disinstalar MyTickets?';
$_MODULE['<{mytickets}prestashop>tickets_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'Mi cuenta';
$_MODULE['<{mytickets}prestashop>tickets_417246b8b34b186642f2263751e66c16'] = 'Mi tickets';
$_MODULE['<{mytickets}prestashop>tickets_fa85a135c0aceb222a9b381ba067173d'] = 'ID ticket';
$_MODULE['<{mytickets}prestashop>tickets_a1fa27779242b4902f7ae3bdd5c6d508'] = 'Tipo';
$_MODULE['<{mytickets}prestashop>tickets_ec53a8c4f07baed5d8825072c89799be'] = 'Estado';
$_MODULE['<{mytickets}prestashop>tickets_ca528d836417871a349312db705a1951'] = 'Fecha apertura';
$_MODULE['<{mytickets}prestashop>tickets_97cdb337e7135dc0909145a30252fa40'] = 'Fecha último mensaje';
$_MODULE['<{mytickets}prestashop>tickets_c3bf447eabe632720a3aa1a7ce401274'] = 'Abierto';
$_MODULE['<{mytickets}prestashop>tickets_03f4a47830f97377a35321051685071e'] = 'Cerrado';
$_MODULE['<{mytickets}prestashop>tickets_d79cf3f429596f77db95c65074663a54'] = 'ID orden';
$_MODULE['<{mytickets}prestashop>tickets_deb10517653c255364175796ace3553f'] = 'Producto';
$_MODULE['<{mytickets}prestashop>tickets_5da618e8e4b89c66fe86e32cdafde142'] = 'Desde';
$_MODULE['<{mytickets}prestashop>tickets_44749712dbec183e983dcd78a7736c41'] = 'Fecha';
$_MODULE['<{mytickets}prestashop>tickets_4c2a8fe7eaf24721cc7a9f0175115bd4'] = 'Mensaje';
$_MODULE['<{mytickets}prestashop>tickets_2f241fa3371a31099cb19470eb4ff43d'] = 'Responder a este mensaje';
$_MODULE['<{mytickets}prestashop>tickets_6f882b30221b582c0e68346a19c770e2'] = 'Volver a la lista de los tickets';
$_MODULE['<{mytickets}prestashop>tickets_3ec365dd533ddb7ef3d1c111186ce872'] = 'Detalles';
$_MODULE['<{mytickets}prestashop>tickets_6e9783376d951cfd780e9fdb67a0f02c'] = 'Ver detalles';
$_MODULE['<{mytickets}prestashop>tickets_538fc4a1d2c2cb43d9040f96ec91f30b'] = 'Usted no ha encora abierto algún ticket';
$_MODULE['<{mytickets}prestashop>tickets_0b3db27bc15f682e92ff250ebb167d4b'] = 'Volver a la cuenta';
$_MODULE['<{mytickets}prestashop>tickets_8cf04a9734132302f96da8e113e80ce5'] = 'Home';
