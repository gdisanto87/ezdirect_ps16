{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div id="mytickets">
	{capture name=path}<a href="{$link->getPageLink('my-account.php', true)}">{l s='My account' mod='mytickets'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='My tickets' mod='mytickets'}{/capture}
	{include file="$tpl_dir./breadcrumb.tpl"}
<br />
	<h1 style='text-align:left'>{l s='My tickets' mod='mytickets'}</h2>
	<br />
	
	{include file="$tpl_dir./errors.tpl"}

	
	{if isset($smarty.get.id_customer_thread)}
	
		<table class="std2">
				<thead>
					<tr>
						<th class="first_item" style="background-color:#8899cc; text-align:left">{l s='Ticket ID' mod='mytickets'}</th>
						<th style="background-color:#99aadd; text-align:left">{l s='Type' mod='mytickets'}</th>
						<th style="background-color:#8899cc; text-align:left">{l s='Status' mod='mytickets'}</th>
						<th style="background-color:#99aadd; text-align:left">{l s='In charge to' mod='mytickets'}</th>
						<th style="background-color:#8899cc; text-align:left">{l s='Open date' mod='mytickets'}</th>
						<th style="background-color:#99aadd; text-align:left">{l s='Last message date' mod='mytickets'}</th>
					</tr>
				</thead>
				<tbody>
				<tr>
					<td>{$id_ticket}</td>
					<td>{$ticket_data.name}</td>
					<td>{if $ticket_data.id_contact == 7}--{else}{if $ticket_data.status == 'open'} {l s='Open' mod='mytickets'} {else if $ticket_data.status == 'closed'} {l s='Closed' mod='mytickets'} {else if $ticket_data.status == 'pending1'} {l s='Pending' mod='mytickets'} {else} {/if}{/if}</td>
					<td>{$in_charge_to}</td>
					<td>{$ticket_data.date_add|date_format:"%d/%m/%Y, %H:%M:%S"}</td>
					<td>{$ticket_data.last_msg|date_format:"%d/%m/%Y, %H:%M:%S"}</td>
				</tr>
				</tbody>
		</table>
		<br />
		<table class="std2">
			<thead>
				<tr>
		{if $ticket_data.id_contact == 9}
		<th class="first_item" style="background-color:#8899cc; text-align:left">{l s='Invoice ID' mod='mytickets'}</th>
		<th class="first_item" style="background-color:#99aadd;text-align:left">{l s='Product' mod='mytickets'}</th>
		</tr></thead>
		<tbody>
		<tr>
		<td>{if $ticket_data.id_order != 0} {$id_fattura} del {$data_fattura} {else} - {/if}</td>
		<td>{if $ticket_data.product != ''} {$ticket_data.product} {else} - {/if}
		</td>
		</tr></tbody>
		</table>
		{else}
		<th class="first_item" style="background-color:#8899cc;text-align:left">{l s='Invoice ID' mod='mytickets'}</th>
		<th class="first_item" style="background-color:#99aadd;text-align:left">{l s='Product' mod='mytickets'}</th>
		</tr></thead>
		<tbody>
		<tr>
		<td>{if $ticket_data.id_order != 0} {$ticket_data.id_order} {else} - {/if}</td>
		</tr>
		<tr><td>{if $ticket_data.product != ''} {$ticket_data.product} {else} - {/if}
		</td>
		</tr></tbody>
		</table>
		{/if}
	
		
		{if $ticket_data.id_contact == 9}
		<br />
		<table class="std2">
			<thead>
				<tr>
		<th style="background-color:#8899cc";>{l s='RMA Type' mod='mytickets'}</th>
		<th  style="background-color:#99aadd;">{l s='RMA Shipping' mod='mytickets'}</th>
		<th style="background-color:#8899cc;">{l s='Address' mod='mytickets'}</th></tr>
		</thead><tbody>
		<tr><td> {if $ticket_data.rma_type != ''} {$ticket_data.rma_type} {else} - {/if}</td><td>{if $ticket_data.rma_shipping != ''} {$ticket_data.rma_shipping} {else} - {/if}</td><td>{if $ticket_data.rma_address != ''} {$ticket_data.rma_address} {else} - {/if}</td></tr>
		</tbody></table>
		
		{/if}
					<br /><br />
		
		<h2>{l s='Communications' mod='mytickets'}</h2>
		{$messages}
	
		<p style='text-align:center'> {if $ticket_data.status != 'closed'}<a href="
		{if $ticket_data.id_contact == 'preventivo'}
		{$link->getPageLink('modules/formprevendita/form.php', true)}?id_thread={$ticket_data.id_customer_thread}&amp;token={$ticket_data.token}
		{else if $ticket_data.id_contact == 'tirichiamiamonoi'}
		{$link->getPageLink('modules/formprevendita/tirichiamiamonoi.php', true)}?id_thread={$ticket_data.id_customer_thread}&amp;token={$ticket_data.token}
		{else}
		{$link->getPageLink('contact-form.php', true)}?id_customer_thread={$ticket_data.id_customer_thread}&amp;token={$ticket_data.token}
		{/if}
		">{l s='Reply to this ticket' mod='mytickets'}</a> | {/if} <a href="/contattaci?rif={if $ticket_data.id_contact == 'preventivo'}P{$ticket_data.id_customer_thread}{else if $ticket_data.id_contact == 'tirichiamiamonoi'}R{$ticket_data.id_customer_thread}{else}T{$ticket_data.id_customer_thread}{/if}">{l s='Create new ticket' mod='mytickets'}</a> | <a href="{$link->getPageLink('modules/mytickets/tickets.php', true)}">{l s='Back to your ticket list' mod='mytickets'}</a></p>
	
	
	{else}
	
		{if $tickets}
			<table class="std2">
				<thead>
					<tr>
						<th class="first_item" style="background-color:#8899cc; text-align:left">{l s='Ticket ID' mod='mytickets'}</th>
						<th style="background-color:#99aadd; text-align:left">{l s='Type' mod='mytickets'}</th>
						<th style="background-color:#8899cc; text-align:left">{l s='Status' mod='mytickets'}</th>
						<th style="background-color:#99aadd; text-align:left">{l s='In charge to' mod='mytickets'}</th>
						<th style="background-color:#8899cc; text-align:left">{l s='Open date' mod='mytickets'}</th>
						<th style="background-color:#99aadd; text-align:left">{l s='Last message date' mod='mytickets'}</th>
						<th style="background-color:#8899cc; text-align:left">{l s='Dettagli' mod='mytickets'}</th>
					</tr>
				</thead>
				<tbody>
				{foreach from=$tickets item=ticket}
				
				
				
				
				
				<tr>
					<td><a href="{$link->getPageLink('modules/mytickets/tickets.php', true)}?id_customer_thread={$ticket.id_customer_thread}{if $ticket.id_contact == 'preventivo' || $ticket.id_contact == 'tirichiamiamonoi'}&type=pv{/if}">
				
			
					{assign var='anno' value=$ticket.date_add|substr:2:2}
				
					{if $ticket.id_contact == 2}
					{assign var='sigla' value='A'}
					{else if $ticket.id_contact == 4}
					{assign var='sigla' value='T'}
					{else if $ticket.id_contact == 3}
					{assign var='sigla' value='V'}
					{else if $ticket.id_contact == 7}
					{assign var='sigla' value='M'}
					{else if $ticket.id_contact == 6}
					{assign var='sigla' value='R'}
					{else if $ticket.id_contact == 8}
					{assign var='sigla' value='D'}
					{else if $ticket.id_contact == 9}
					{assign var='sigla' value='S'}
					{else if $ticket.id_contact == 'preventivo'}
					{assign var='sigla' value='P'}
					{else if $ticket.id_contact == 'tirichiamiamonoi'}
					{assign var='sigla' value='R'}
					{/if}
					
					{assign var='id_ticket' value=$sigla|cat:$anno|cat:$ticket.id_customer_thread}
				
					{$id_ticket}
					</a>
				</td>
					
					<td>{$ticket.name}</td>
					<td>{if $ticket.id_contact == 7}{else}{if $ticket.status == 'open'} {l s='Open' mod='mytickets'} {else if $ticket.status == 'closed'} {l s='Closed' mod='mytickets'} {else if $ticket.status == 'pending1'} {l s='Pending' mod='mytickets'} {else} {/if}{/if}</td>
					<td>{if $ticket.id_employee == 0} - {else}{$ticket.firstname} {$ticket.lastname}{/if}</td>
					<td>{$ticket.date_add|date_format:"%d/%m/%Y, %H:%M:%S"}</td>
					<td>{$ticket.last_msg|date_format:"%d/%m/%Y, %H:%M:%S"}</td>
					<td><a href="{$link->getPageLink('modules/mytickets/tickets.php', true)}?id_customer_thread={$ticket.id_customer_thread}{if $ticket.id_contact == 'preventivo' || $ticket.id_contact == 'tirichiamiamonoi'}&type=pv{/if}"><img src='/themes/ez20/img/icona-vai.jpg'></a></td>
					
				</tr>
				</tbody>
			{/foreach}
			</table>
		
		{else}
		
			<p class="warning">{l s='You have not opened any ticket' mod='mytickets'}.</p>
		
		{/if}
		<br />
		<div  id="my-tickets-responsive">
			<a href="/contattaci?step=assistenza-ordini" class="button_large" style="display:block; float:left;   width: auto; padding: 10px; ">{l s='Create new ticket orders' mod='mytickets'}</a> 
			<a href="/contattaci?step=contabilita" class="button_large"  style="display:block; float:left;  width: auto; padding: 10px;">{l s='Create new ticket administrative assistance' mod='mytickets'}</a> 
			<a href="/cms.php?id_cms=36" class="button_large" style="display:block; float:left; width: auto; padding: 10px; ">{l s='Create new ticket RMA' mod='mytickets'}</a> 
			<a href="/contattaci?step=tecnica" class="button_large" style="display:block; float:left;  width: auto; padding: 10px; ">{l s='Create new ticket technical assistance' mod='mytickets'}</a> 
		</div>
		<br />
		
	{/if}

	
</div>
