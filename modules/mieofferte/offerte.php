<?php
/* SSL Management */
$useSSL = true;

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');
include_once(dirname(__FILE__).'/mieofferte.php');

$errors = array();


	if ($cookie->isLogged())
	{
		
		
		
		$customer = $cookie->id_customer;
		$language = $cookie->id_lang;
		
		if(!Tools::getValue('id_cart') && !Tools::getValue('new_cart')) {
		
			$carrelli = Db::getInstance()->ExecuteS("SELECT c.*, o.id_order FROM cart c LEFT JOIN orders o ON c.id_cart = o.id_cart WHERE c.id_customer = ".$customer." AND c.provvisorio = 0 AND c.id_employee > 0 ORDER BY c.id_cart DESC");
			
		
			$smarty->assign(array(
			'carrelli' => $carrelli,
			));
		
		}
		
		else if(Tools::getValue('id_cart')) { 
		
			if(!Tools::getValue('download')) {
			
				$carrello = Db::getInstance()->getValue("SELECT id_cart FROM cart WHERE id_customer = ".Tools::getValue('id_customer')." AND provvisorio = 0 AND id_cart = ".Tools::getValue('id_cart')."");
				
				if($carrello > 0) {
				
					$array_carrello_originale = Db::getInstance()->executeS("SELECT c.id_customer, cp.* FROM carrelli_creati c JOIN carrelli_creati_prodotti cp ON c.id_cart = cp.id_cart WHERE c.id_customer = ".Tools::getValue('id_customer')." AND cp.id_cart = ".Tools::getValue('id_cart')."");
					
					$array_carrello = Db::getInstance()->executeS("SELECT c.id_customer, cp.* FROM cart c JOIN cart_product cp ON c.id_cart = cp.id_cart WHERE c.id_customer = ".Tools::getValue('id_customer')." AND cp.id_cart = ".Tools::getValue('id_cart')."");
					
					if($array_carrello_originale === $array_carrello) {
					
					//	echo "Originale uguale al carrello";
					
					}

					else {
					
						if(empty($array_carrello_originale)) {
						
						}
						
						else {
							//echo "Originale non uguale al carrello";
							
							/*Db::getInstance()->executeS("DELETE FROM cart_product WHERE id_cart = ".Tools::getValue('id_cart')."");
							
							foreach($array_carrello_originale as $ins) {
							
								Db::getInstance()->executeS("INSERT INTO cart_product (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, sconto_extra, sort_order, date_add) VALUES ('".$ins['id_cart']."', '".$ins['id_product']."', '".$ins['id_product_attribute']."', '".$ins['quantity']."', '".$ins['price']."', '".$ins['free']."', '".$ins['name']."', '".$ins['sc_qta']."', '".$ins['sconto_extra']."', '".$ins['sort_order']."', '".$ins['date_add']."')");
							
							}
							*/
						}
						
					}
					$cookie->id_cart = Tools::getValue('id_cart');
					//echo $cookie->id_cart;
					header("Location: ../../order-opc.php");
				}
				
				else {
				
					echo "<strong>ERROR</strong>";
				
				}
			
			}
			
			else {
				
				$carrello = Db::getInstance()->getValue("SELECT id_cart FROM cart WHERE id_customer = ".Tools::getValue('id_customer')." AND id_cart = ".Tools::getValue('id_cart')."");
				
				if($carrello > 0) {
					require_once('../../classes/html2pdf/html2pdf.class.php');
					$content = Cart::getCartPDF(Tools::getValue('id_cart'), Tools::getValue('id_customer'));
					ob_clean();
					$html2pdf = new HTML2PDF('P','A4','it');
					$html2pdf->WriteHTML($content);
					$html2pdf->Output(Tools::getValue('id_customer').'-offerta-'.Tools::getValue('id_cart').'.pdf', 'D'); 
				}
				
				else {
				
					echo "<strong>ERROR</strong>";
				
				}
			
			
			}
		}
		
		else if(Tools::getValue('new_cart')) { 
		
			$cookie->id_cart = '';
			$cookie->id_carrier = '';
			header("Location: ../../order-opc.php");
		}

		if (_THEME_NAME_ == 'ez20')
			$smarty->display(dirname(__FILE__).'/offerte-ez20.tpl');
		else
			$smarty->display(dirname(__FILE__).'/offerte.tpl');
		
	}
	
	else {
		
		
		header('location: /authentication.php?back=le-mie-offerte'.(Tools::getValue('id_cart') ? '|id_cart='.Tools::getValue('id_cart').'*id_customer='.Tools::getValue('id_customer') : ''));
		
		echo "Devi fare il <strong>login</strong> per vedere le tue offerte. <a href='/my-account.php?back=modules/mieofferte/offerte.php'>Clicca qui per fare il login</a>";
		$errors[] = Tools::displayError('You must be logged in to see your invoices.'); 
		$smarty->assign(array(
			'errors' => $errors
		));
	}

	include(dirname(__FILE__).'/../../footer.php');
