{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div id="mieofferte">
	{capture name=path}<a href="{$link->getPageLink('my-account.php', true)}">{l s='My account' mod='mieofferte'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='My tickets' mod='mieofferte'}{/capture}
	{include file="$tpl_dir./breadcrumb.tpl"}

	<h2>{l s='My offers' mod='mieofferte'}</h2>
	<br />
	
	{include file="$tpl_dir./errors.tpl"}

	
	
	
		{if $carrelli}
			<table class="std2">
				<thead>
					<tr>
						<th class="first_item" style="background-color:#8899cc;text-align:left">{l s='ID offer' mod='mieofferte'}</th>
						<th style="background-color:#99aadd; text-align:left">{l s='Offer name' mod='mieofferte'}</th>
						<th style="background-color:#8899cc;text-align:left">{l s='Offer date' mod='mieofferte'}</th>
						<th style="background-color:#99aadd; text-align:left">{l s='Offer deadline' mod='mieofferte'}</th>
						<th style="background-color:#8899cc;text-align:left">{l s='View offer' mod='mieofferte'}</th>
						<th class="last_item" style="background-color:#99aadd; text-align:left">{l s='Download' mod='mieofferte'}</th>
					</tr>
				</thead>
				<tbody>
				{foreach from=$carrelli item=carrello}
				
				
				
				
				
				<tr>
					<td style="cursor:pointer" onclick="window.location='{$link->getPageLink('modules/mieofferte/offerte.php', true)}?id_cart={$carrello.id_cart}&id_customer={$carrello.id_customer}'">
				
					{$carrello.id_cart}-{$carrello.revisioni}
				
				</td>
					
						<td style="cursor:pointer" onclick="'window.location={$link->getPageLink('modules/mieofferte/offerte.php', true)}?id_cart={$carrello.id_cart}&id_customer={$carrello.id_customer}'">{$carrello.name}</td>
				
				<td style="cursor:pointer" onclick="'window.location={$link->getPageLink('modules/mieofferte/offerte.php', true)}?id_cart={$carrello.id_cart}&id_customer={$carrello.id_customer}'">{$carrello.date_add|date_format:"%d/%m/%Y"}</td>
					
					<td style="cursor:pointer" onclick="'window.location={$link->getPageLink('modules/mieofferte/offerte.php', true)}?id_cart={$carrello.id_cart}&id_customer={$carrello.id_customer}'">{$carrello.validita|date_format:"%d/%m/%Y"}</td>
					
					<td style="cursor:pointer" onclick="'window.location={$link->getPageLink('modules/mieofferte/offerte.php', true)}?id_cart={$carrello.id_cart}&id_customer={$carrello.id_customer}'">
					{if $carrello.id_order > 0}
					
						{l s='You have already used this offer' mod='mieofferte'}
					{else}
						<a href="{$link->getPageLink('modules/mieofferte/offerte.php', true)}?id_cart={$carrello.id_cart}&id_customer={$carrello.id_customer}">{l s='Click here to view your offer' mod='mieofferte'}</a>
					{/if}
					</td>
					<td><a href="{$link->getPageLink('modules/mieofferte/download_offerta.php', true)}?download=yes&originale=y&id_cart={$carrello.id_cart}&id_customer={$carrello.id_customer}"><img src="{$img_dir}icon/pdf.gif" alt="" class="icon" /></a><a href="{$link->getPageLink('modules/mieofferte/download_offerta.php', true)}?download=yes&originale=y&id_cart={$carrello.id_cart}&id_customer={$carrello.id_customer}">{l s='Click here to download your offer' mod='mieofferte'}</a></td>
					
				</tr>
				</tbody>
			{/foreach}
			</table>
		
		{else}
		
			<p class="warning">{l s='You have no offers' mod='mieofferte'}.</p>
		
		{/if}
		<br />
		
		
		<ul class="footer_links">
		
		
		<li><a href="{$link->getPageLink('modules/formprevendita/form.php', true)}"><img src="https://www.ezdirect.it/themes/ez20/img/icon/account/quotazione.png" alt="" class="icon" height="36" width="36" /></a><a href="{$link->getPageLink('modules/formprevendita/form.php', true)}">{l s='Click here to ask for a new quotation' mod='mieofferte'}</a></li>
		
		<li><a href="{$link->getPageLink('modules/mieofferte/offerte.php', true)}?new_cart=yes"><img height="36" width="36" src="https://www.ezdirect.it/themes/ez20/img/icon/account/ordini.png" alt="" class="icon" /></a><a href="{$link->getPageLink('modules/mieofferte/offerte.php', true)}?new_cart=yes">{l s='Click here to create a new cart' mod='mieofferte'}</a></li>
		
		<li><a href="{$link->getPageLink('my-account.php', true)}"><img height="36" width="36" src="https://www.ezdirect.it/themes/ez20/img/icon/account/user.png" alt="" class="icon" /></a><a href="{$link->getPageLink('my-account.php', true)}">{l s='Back to Your Account' mod='mieofferte'}</a></li>
		<li><a href="{$base_dir}"><img height="36" width="36" src="{$img_dir}icon/home.gif" alt="" class="icon" /></a><a href="{$base_dir}">{l s='Home' mod='mieofferte'}</a></li>
		</ul>
		
	
</div>
