<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require('/var/www/vhosts/ezdirect.it/httpdocs/config/config.inc.php');
require('/var/www/vhosts/ezdirect.it/httpdocs/init.php');

	if(isset($_GET['download']) && $_GET['download'] == 'pdf') {
	
		require_once('/var/www/vhosts/ezdirect.it/httpdocs/classes/html2pdf/html2pdf.class.php');
		$content = Cart::getCartPDF($cookie->id_cart, $cookie->id_customer, 'n', 0);
		ob_clean();
		$html2pdf = new HTML2PDF('P','A4','it');
		$html2pdf->WriteHTML($content);
		$html2pdf->Output($cookie->id_customer.'-offerta-'.$cookie->id_cart.'.pdf', 'D'); 
	
	}
	else
	{

		if(Tools::getValue('id_cart')) { 
		
			if(!Tools::getValue('download')) {
			
				$carrello = Db::getInstance()->getValue("SELECT id_cart FROM cart WHERE id_customer = ".Tools::getValue('id_customer')." AND provvisorio = 0 AND id_cart = ".Tools::getValue('id_cart')."");
				
				if($carrello > 0) {
				
					$array_carrello_originale = Db::getInstance()->executeS("SELECT c.id_customer, cp.* FROM carrelli_creati c JOIN carrelli_creati_prodotti cp ON c.id_cart = cp.id_cart WHERE c.id_customer = ".Tools::getValue('id_customer')." AND cp.id_cart = ".Tools::getValue('id_cart')."");
					
					$array_carrello = Db::getInstance()->executeS("SELECT c.id_customer, cp.* FROM cart c JOIN cart_product cp ON c.id_cart = cp.id_cart WHERE c.id_customer = ".Tools::getValue('id_customer')." AND cp.id_cart = ".Tools::getValue('id_cart')."");
					
					if($array_carrello_originale === $array_carrello) {
					
					//	echo "Originale uguale al carrello";
					
					}

					else {
					
						if(empty($array_carrello_originale)) {
						
						}
						
						else {
							//echo "Originale non uguale al carrello";
							
							Db::getInstance()->executeS("DELETE FROM cart_product WHERE id_cart = ".Tools::getValue('id_cart')."");
							
							foreach($array_carrello_originale as $ins) {
							
								Db::getInstance()->executeS("INSERT INTO cart_product (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, sconto_extra, sort_order, date_add) VALUES ('".$ins['id_cart']."', '".$ins['id_product']."', '".$ins['id_product_attribute']."', '".$ins['quantity']."', '".$ins['price']."', '".$ins['free']."', '".$ins['name']."', '".$ins['sc_qta']."', '".$ins['sconto_extra']."', '".$ins['sort_order']."', '".$ins['date_add']."')");
							
							}
						}
						
					}
					$cookie->id_cart = Tools::getValue('id_cart');
					//echo $cookie->id_cart;
					header("Location: ../../order-opc.php");
				}
				
				else {
				
					echo "<strong>ERROR</strong>";
				
				}
			
			}
			
			else {
				
				$carrello = Db::getInstance()->getValue("SELECT id_cart FROM cart WHERE id_customer = ".Tools::getValue('id_customer')." AND id_cart = ".Tools::getValue('id_cart')."");
				
				if($carrello > 0) {
					require_once('../../classes/html2pdf/html2pdf.class.php');
					$content = Cart::getCartPDF(Tools::getValue('id_cart'), Tools::getValue('id_customer'));
					ob_clean();
					$html2pdf = new HTML2PDF('P','A4','it');
					$html2pdf->WriteHTML($content);
					$html2pdf->Output(Tools::getValue('id_customer').'-offerta-'.Tools::getValue('id_cart').'.pdf', 'D'); 
				}
				
				else {
				
					echo "<strong>ERROR</strong>";
				
				}
			
			
			}
		}
	}