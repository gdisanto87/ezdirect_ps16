<?php

class PrecompilatoCore extends ObjectModel
{
    public $id; // id_precompilato

    public $id_employee;
    public $oggetto;
    public $testo;
    public $date_add;
    public $date_upd;
    public $active;

    public static $definition = array(
        'table' => 'precompilato',
        'primary' => 'id_precompilato',
        'fields' => array(
            'id_employee' =>  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'oggetto' =>      array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 200),
            'testo' =>        array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml'),
            'date_add' =>     array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            'date_upd' =>     array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            'active' =>       array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
        ),
    );
	
	public function add($autodate = true, $null_values = false)
	{ 
		return parent::add($autodate, true); 
	}
	
	public function update($null_values = false)
	{
		if (parent::update($null_values))
			return true;
		
		return false;
	}
	
	public function delete()
	{
	 	if (parent::delete())
			return true;
		
		return false;
    }
}
