<?php

class BDLCore extends ObjectModel
{
    public $id; // id_bdl

    public $id_customer;
    public $id_address;
    public $phone;
    public $data_richiesta;
    public $richiesto_da;
    public $data_effettuato;
    public $effettuato_da;
    public $impianto_ubicazione;
    public $contratto_assistenza;
    public $manutenzione_ordinaria;
    public $manutenzione_straordinaria;
    public $motivo_chiamata;
    public $guasto;
    public $causa_guasto;
    public $descrizione;
    public $intervento_svolto;
    public $dettaglio_costi;
    public $note;
    public $rif_ordine;
    public $pagato = false;
    public $fatturato = false;
    public $date_add;
    public $date_upd;

    public static $definition = array(
        'table' => 'bdl',
        'primary' => 'id_bdl',
        'fields' => array(
            'id_customer' =>  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'id_address' =>  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'phone' =>  array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 100),
            'data_richiesta' =>     array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'richiesto_da' =>  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'/*, 'copy_post' => false*/),
            'data_effettuato' =>     array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'effettuato_da' =>  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'/*, 'copy_post' => false*/),
            'impianto_ubicazione' =>  array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'/*, 'size' => */),
            'contratto_assistenza' =>  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            // correggere: le manutenzioni nel db sono varchar, modificarle!
            'manutenzione_ordinaria' =>  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'manutenzione_straordinaria' =>  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'motivo_chiamata' =>  array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
            'guasto' =>  array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
            'causa_guasto' =>  array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
            'descrizione' =>  array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
            'intervento_svolto' =>  array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
            'dettaglio_costi' =>  array('type' => self::TYPE_STRING/*, 'validate' => 'isGenericName'*/),
            'note' =>  array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
            'rif_ordine' =>  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            // CORREGGERE: aggiungere tutti i campi mancanti
            'pagato' =>       array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'fatturato' =>    array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'date_add' =>     array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            'date_upd' =>     array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
        ),
    );
	
	public function add($autodate = true, $null_values = false)
	{ 
		return parent::add($autodate, true); 
	}
	
	public function update($null_values = false)
	{
		if (parent::update($null_values))
			return true;
		
		return false;
    }

    public function delete()
	{
	 	if (parent::delete())
			return true;
		
		return false;
    }

    // da correggere; $vergine può diventare anzi true/false? Oppure si può usare solo $id_bdl = 0
    // Eliminato codice spring
    public static function generatePDFBDL($id_bdl, $tipo_az = 'ezdirect', $vergine = 'n')
    {
        // BDL esistente
        if($vergine == 'n')
		{
            //$bdl = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'bdl WHERE id_bdl = '.$id_bdl);
			//$customer = Db::getInstance()->getRow('SELECT * FROM customer WHERE id_customer = '.$bdl['id_customer'].'');
            $bdl = new BDL((int)$id_bdl);
            $customer = new Customer((int)$bdl->id_customer);
            
			if($customer->is_company == 1)
				$cliente = $customer->company;
			else
                $cliente = $customer->firstname." ".$customer->lastname;
                
			$indirizzo = Db::getInstance()->getRow('
                SELECT * 
                FROM '._DB_PREFIX_.'address a 
                    JOIN '._DB_PREFIX_.'state s ON a.id_state = s.id_state 
                WHERE a.id_address = '.$bdl->id_address
            ); 
			
			$impianto_ubicazione = $bdl->impianto_ubicazione;
			if(is_numeric($impianto_ubicazione)) 
			{
				$indirizzo_impianto = Db::getInstance()->getRow('
                    SELECT * 
                    FROM '._DB_PREFIX_.'address a 
                        JOIN '._DB_PREFIX_.'state s ON a.id_state = s.id_state 
                    WHERE a.id_address = '.$impianto_ubicazione
                );

				$impianto_ubicazione = $indirizzo_impianto['address1']." - ".$indirizzo_impianto['postcode']." ".$indirizzo_impianto['city']." (".$indirizzo_impianto['iso_code'].")";
			}
			
			$richiesto_da = Db::getInstance()->getRow('
                SELECT fistname, lastname 
                FROM persone 
                WHERE id_persona = '.$bdl->richiesto_da
            );

			$effettuato_da = Db::getInstance()->getRow('
                SELECT fistname, lastname 
                FROM '._DB_PREFIX_.'employee 
                WHERE id_employee = '.$bdl->effettuato_da
            );
        }
        // BDL vuoto
		else
		{
            // $bdl = new BDL();
            $bdl = ''; 
            $customer = ''; 
            $cliente = ''; 
            $indirizzo = ''; 
            $impianto_ubicazione = ''; 
            $indirizzo_impianto = ''; 
            $richiesto_da = ''; 
            $effettuato_da = '';
		}
		
        $content = '
            <page>
                <div id="contenitore" style="width:770px; height:1070px; font-family: Arial, Verdana, Sans-Serif; display:block; position:relative; margin:0 auto;">
                    
                    <div style="position:absolute; top:0px; left:0px">
                        '.($tipo_az == 'tecnotel' ? '<img src="http://www.ezdirect.it/img/Tecnotel-centralini.gif" alt="Tecnotel" />' : '<img src="http://www.ezdirect.it/img/logo.jpg" alt="Ezdirect" />').'
                    </div>
            
                    <div style="position:absolute; top:10px; text-align:right; right:25px; height:45x; font-size:12px;">
                        Via Nerino Garbuio snc - 54038 Montignoso (MS) - Tel. 0585 821163 - Fax 0585 821286<br />
                        '.($tipo_az == 'tecnotel' ? 'P.IVA 00601870454 - Email info@tecnotel.net - Sito web www.tecnotel.net' : 'P.IVA - CF 01164670455 - REA 118272 - Email info@ezdirect.it - Sito web www.ezdirect.it').'
                    </div>
                
                    <div style="position:absolute; top:65px; text-align:right; right:25px; height:30px; font-size:15px"></div>

                    <div style="clear:both">&nbsp;</div>

                    <div style="position:relative; margin-top:65px; padding:5px; height:710px">
            
                        <style type="text/css">
                            .dettagli_tabella td {
                                font-size:13px;
                                height:20px;
                                vertical-align:middle;
                                border:1px solid black;
                                padding:0px;
                                padding-top:1px;
                                padding-bottom:1px;
                            }	
                        </style>

                        <table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="width:355px; border:0px; border-right:1px solid black"></td>
                                <td style="width:65px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Data</td>
                                <td style="width:100px; border-bottom:0px">&nbsp;'.($vergine == 'y' ? '' : date("d-m-Y", strtotime($bdl->data_effettuato))).'</td>
                                <td style="width:108px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Buono di Lavoro N°</td> 
                                <td style="width:100px; border-bottom:0px">&nbsp;'.$bdl->id.'</td>
                            </tr>
                        </table>
                        
                        <table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">	
                            <tr>
                                <td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Utente</td>
                                <td style="width:303px; border-bottom:0px">&nbsp;'.$cliente.'</td>
                            </tr>
                        </table>
                        
                        <table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
                            <tr>
                                <td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Indirizzo</td>
                                <td style="width:585px; border-bottom:0px">&nbsp;'.$indirizzo['address1'].'</td>
                            </tr>
                        </table>
                
                        <table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
                            <tr>
                                <td style="width:150px; background-color:#f3f3f3; font-size:11px">&nbsp;CAP - Citt&agrave;</td>
                                <td style="width:303px">&nbsp;'.($vergine == 'y' ? '' : $indirizzo['postcode'].' - '.$indirizzo['city']).'</td>
                                <td style="width:35px; background-color:#f3f3f3; font-size:11px">&nbsp;Prov.</td>
                                <td style="width:50px">&nbsp;'.$indirizzo['iso_code'].'</td>
                                <td style="width:35px; background-color:#f3f3f3; font-size:11px">&nbsp;Tel.</td>
                                <td style="width:150px;">&nbsp;'.(substr($bdl->phone, 0, 5) == 'Altro' ? (substr($bdl->phone, 5, strlen($bdl->phone))) : $bdl->phone).'</td>
                            </tr>
                        </table>

                        <table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
                            <tr>
                                <td style="width:150px; background-color:#f3f3f3; border-top:0px; font-size:11px">&nbsp;Richiesto il giorno</td>
                                <td style="width:140px; border-top:0px">&nbsp;'.($vergine == 'y' ? '' : date("d-m-Y", strtotime($bdl->data_richiesta))).'</td>
                                <td style="width:50px; background-color:#f3f3f3; border-top:0px; font-size:11px">&nbsp;alle ore</td>
                                <td style="width:107px; border-top:0px">&nbsp;'.($vergine == 'y' ? '' : date("H:i", strtotime($bdl->data_richiesta))).'</td>
                                <td style="width:35px; background-color:#f3f3f3; border-top:0px; font-size:11px">&nbsp;da</td>
                                <td style="width:241px; border-top:0px">&nbsp;'.$richiesto_da['firstname']." ".$richiesto_da['lastname'].'</td>
                            </tr>	
                            <tr>
                                <td style="width:150px; background-color:#f3f3f3; border-top:0px; font-size:11px">&nbsp;Effettuato il giorno</td>
                                <td style="width:140px; border-top:0px">&nbsp;'.($vergine == 'y' ? '' : date("d-m-Y", strtotime($bdl->data_effettuato))).'</td>
                                <td style="width:50px; background-color:#f3f3f3; border-top:0px; font-size:11px">&nbsp;alle ore</td>
                                <td style="width:107px; border-top:0px">&nbsp;'.($vergine == 'y' ? '' : date("H:i", strtotime($bdl->data_effettuato))).'</td>
                                <td style="width:35px; background-color:#f3f3f3; border-top:0px; font-size:11px">&nbsp;da</td>
                                <td style="width:241px; border-top:0px">&nbsp;'.$effettuato_da['firstname']." ".$effettuato_da['lastname'].'</td>
                            </tr>
                        </table>

                        <table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
                            <tr>
                                <td style="width:150px; background-color:#f3f3f3; border-top:0px; border-bottom:0px; font-size:11px">&nbsp;Impianto-Ubicazione</td>
                                <td style="width:585px; border-top:0px; border-bottom:0px">&nbsp;'.$impianto_ubicazione.'</td>
                            </tr>
                        </table>
                
                        <table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">			
                            <tr>
                                <td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Contratto di assistenza</td>
                                <td style="width:20px; border-bottom:0px; position:relative;">'.($bdl->contratto_assistenza == 1 ? '<div style="position:absolute; font-size:18px; left:42px">X</div>' : '').'SI</td>
                                <td style="width:20px; border-bottom:0px; position:relative;">'.($vergine == 'y' ? '' : ($bdl->contratto_assistenza == 0 ? '<div style="position:absolute; font-size:18px; left:20px">X</div>' : '')).'NO</td>
                                <td style="width:245px; background-color:#f3f3f3; border-bottom:0px; text-align:right; font-size:11px">Manutenzione ordinaria&nbsp;</td>
                                <td style="width:20px; text-align:center; border-bottom:0px">'.($bdl->manutenzione_ordinaria == 1 ? 'X' : '').'</td>
                                <td style="width:245px; background-color:#f3f3f3; border-bottom:0px; text-align:right; font-size:11px">&nbsp;Manutenzione straordinaria&nbsp;</td>
                                <td style="width:20px; text-align:center; border-bottom:0px">'.($bdl->manutenzione_straordinaria == 1 ? 'X' : '').'</td>
                            </tr>
                        </table>

                        <table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
                            <tr>
                                <td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Motivo chiamata</td>
                                <td style="width:575px; padding-left:5px; border-bottom:0px">'.$bdl->motivo_chiamata.'</td>
                            </tr>
                        </table>

                        <table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
                            <tr>
                                <td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Descrizione</td>
                                <td style="width:575px; padding-left:5px; border-bottom:0px">'.(substr($bdl->descrizione, 0, 5) == 'Altro' || ($bdl->id < 392 && $bdl->descrizione != 'Nessuno') ? (substr($bdl->descrizione, 0, 5) == 'Altro' ? substr($bdl->descrizione, 5, strlen($bdl->descrizione)) : $bdl->descrizione) : $bdl->descrizione).'</td>
                            </tr>
                        </table>
                
                        <table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
                            <tr>
                                <td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Guasto riscontrato dal tecnico</td>
                                <td style="width:575px; padding-left:5px; border-bottom:0px">'.(substr($bdl->guasto, 0, 5) == 'Altro' || ($bdl->id < 392 && $bdl->guasto != 'Nessuno') ? (substr($bdl->guasto, 0, 5) == 'Altro' ? substr($bdl->guasto, 5, strlen($bdl->guasto)) : $bdl->guasto) : $bdl->guasto).'</td>
                            </tr>
                        </table>
                
                        <table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
                            <tr>
                                <td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Causa guasto</td>
                                <td style="width:575px; padding-left:5px; border-bottom:0px">'.(substr($bdl->causa_guasto, 0, 5) == 'Altro' || ($bdl->id < 392 && $bdl->causa_guasto != 'Nessuno') ? (substr($bdl->causa_guasto, 0, 5) == 'Altro' ? substr($bdl->causa_guasto, 5, strlen($bdl->causa_guasto)) : $bdl->causa_guasto) : $bdl->causa_guasto).'</td>
                            </tr>
                        </table>

                        <table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
                            <tr>
                                <td style="width:738px; background-color:#f3f3f3; border-bottom:1px solid black; text-align:center; font-size:11px">&nbsp;Intervento svolto</td>
                            </tr>
                        </table>
                
                        <table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
                            <tr>
                                <td style="width:728px; padding-left:5px; border-top:0px; border-bottom:1px solid black;">'.$bdl->intervento_svolto.'</td>
                            </tr>
                            '.(strlen($bdl->intervento_svolto) > 50 ? '' : '
                            <tr>
                                <td style="width:728px; padding-left:5px; border-top:0px; border-bottom:1px solid black;"></td>
                            </tr>
                            <tr>
                                <td style="width:728px; padding-left:5px; border-top:0px; border-bottom:1px solid black;"></td>
                            </tr>
                            <tr>
                                <td style="width:728px; padding-left:5px; border-top:0px; border-bottom:1px solid black;"></td>
                            </tr>').'
                        </table>
		';
        
        // Correggere: se vergine = y, dettaglio_costi = '' ?
		$dettaglio_costi = $bdl->dettaglio_costi;
		$dettaglio_costi = unserialize($dettaglio_costi);
        
        // Dettaglio Economico dell'intervento
		if($bdl->rif_ordine <= 0)
		{
			$content .= '
                <table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
                    <tr style="text-align:center">
                        <td colspan="2" style="width:494px; background-color:#f3f3f3; border-top:0px; font-size:11px">Dettaglio Economico dell\'intervento</td>
                        <td style="width:50px; border-top:0px; background-color:#f3f3f3; font-size:11px ">&nbsp;Q.t&agrave;</td>
                        <td style="width:60px; border-top:0px; background-color:#f3f3f3; font-size:11px">&nbsp;Unitario</td>
                        <td style="width:60px; border-top:0px; background-color:#f3f3f3; font-size:11px">&nbsp;Sconto %</td>
                        <td style="width:60px; border-top:0px; background-color:#f3f3f3; font-size:11px">&nbsp;Importo</td>
                    </tr>
            ';
			
            //	if($dettaglio_costi['diritto_chiamata_qta']*$dettaglio_costi['diritto_chiamata_unitario'] > 0)
            //	{
                switch($dettaglio_costi['diritto_chiamata_tipo'])
                {
					case 'EZDIRFIX': $tipo = 'Fisso intervento &lt; 20 km'; break;
					case 'EZDIRFIXF': $tipo = 'Fisso intervento &lt; 20 km festivo'; break;
					default: $tipo = ''; break;
                }
                
                $content .= '
                    <tr>
                        <td style="width:96px; background-color:#f3f3f3; font-size:11px">&nbsp;Diritto fisso di chiamata</td>
                        <td style="width:397px">&nbsp;'.$tipo.'</td>
                        <td style="width:50px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($dettaglio_costi['diritto_chiamata_qta'],2,",","")).'&nbsp;</td>
                        <td style="width:60px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($dettaglio_costi['diritto_chiamata_unitario'],2,",","")).'&nbsp;</td>
                        <td style="width:60px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($dettaglio_costi['diritto_chiamata_sconto'],2,",","")).'&nbsp;</td>
                        <td style="width:60px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format(($dettaglio_costi['diritto_chiamata_unitario']-(($dettaglio_costi['diritto_chiamata_unitario']*$dettaglio_costi['diritto_chiamata_sconto'])/100))*($dettaglio_costi['diritto_chiamata_qta']),2,",","")).'&nbsp;</td>
                    </tr>
                ';
            //	}
            //	if($dettaglio_costi['chilometri_percorsi_qta']*$dettaglio_costi['chilometri_percorsi_unitario'] > 0)
            //	{
				switch($dettaglio_costi['chilometri_percorsi_tipo'])
				{
					case 'EZKMR': $tipo = 'Rimborso km oltre diritto fisso'; break;
					case 'EZZONAFIX': $tipo = 'Chiamata fissa con partner di zona'; break;
					default: $tipo = ''; break;
                }
                
				$content .= '
                    <tr>
                        <td style="width:96px; background-color:#f3f3f3; font-size:11px">&nbsp;Chilometri percorsi</td>
                        <td style="width:397px">&nbsp;'.$tipo.'</td>
                        <td style="width:50px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($dettaglio_costi['chilometri_percorsi_qta'],2,",","")).'&nbsp;</td>
                        <td style="width:60px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($dettaglio_costi['chilometri_percorsi_unitario'],2,",","")).'&nbsp;</td>
                        <td style="width:60px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($dettaglio_costi['chilometri_percorsi_sconto'],2,",","")).'&nbsp;</td>
                        <td style="width:60px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format(($dettaglio_costi['chilometri_percorsi_unitario']-(($dettaglio_costi['chilometri_percorsi_unitario']*$dettaglio_costi['chilometri_percorsi_sconto'])/100))*($dettaglio_costi['chilometri_percorsi_qta']),2,",","")).'&nbsp;</td>
                    </tr>
                ';
            //	}
                
            //	if($dettaglio_costi['costo_manodopera_qta']*$dettaglio_costi['costo_manodopera_unitario'] > 0)
            //	{
				switch($dettaglio_costi['costo_manodopera_tipo'])
				{
					case 'EZTECORD': $tipo = 'Tariffa oraria ordinaria'; break;
					case 'EZTEC30': $tipo = 'Tariffa 30 minuti ordinaria'; break;
					case 'EZTECXTRA': $tipo = 'Tariffa oraria straordinaria'; break;
					case 'EZTECFEST': $tipo = 'Tariffa oraria festivo'; break;
					default: $tipo = ''; break;
				}
				
				$content .= '
                    <tr>
                        <td style="width:96px; background-color:#f3f3f3; font-size:11px">&nbsp;Costo manodopera</td>
                        <td style="width:397px">&nbsp;'.$tipo.'</td>
                        <td style="width:50px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($dettaglio_costi['costo_manodopera_qta'],2,",","")).'&nbsp;</td>
                        <td style="width:60px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($dettaglio_costi['costo_manodopera_unitario'],2,",","")).'&nbsp;</td>
                        <td style="width:60px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($dettaglio_costi['costo_manodopera_sconto'],2,",","")).'&nbsp;</td>
                        <td style="width:60px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format(($dettaglio_costi['costo_manodopera_unitario']-(($dettaglio_costi['costo_manodopera_unitario']*$dettaglio_costi['costo_manodopera_sconto'])/100))*($dettaglio_costi['costo_manodopera_qta']),2,",","")).'&nbsp;</td>
                    </tr>
                ';
		    //	}
			
			$tot_varie = 0;
			$tot_varie = (($dettaglio_costi['diritto_chiamata_unitario']-(($dettaglio_costi['diritto_chiamata_unitario']*$dettaglio_costi['diritto_chiamata_sconto'])/100))*($dettaglio_costi['diritto_chiamata_qta']))+(($dettaglio_costi['chilometri_percorsi_unitario']-(($dettaglio_costi['chilometri_percorsi_unitario']*$dettaglio_costi['chilometri_percorsi_sconto'])/100))*($dettaglio_costi['chilometri_percorsi_qta']))+(($dettaglio_costi['costo_manodopera_unitario']-(($dettaglio_costi['costo_manodopera_unitario']*$dettaglio_costi['costo_manodopera_sconto'])/100))*($dettaglio_costi['costo_manodopera_qta']));

			$num_varie = $dettaglio_costi['num_varie'];
				
			for($i=0; $i<=$num_varie; $i++) {
			
                if(empty($dettaglio_costi['ricambi_e_varie_'.$i.'_descrizione']) && $dettaglio_costi['ricambi_e_varie_'.$i.'_unitario'] == 0) {

                }
                else
                {
                    $content .= '
                        <tr>
                            <td style="width:96px; background-color:#f3f3f3; font-size:11px">&nbsp;Ricambi e Varie</td>
                            <td style="width:397px">&nbsp;'.$dettaglio_costi['ricambi_e_varie_'.$i.'_descrizione'].'</td>
                            <td style="width:50px; text-align:right">&nbsp;'.number_format($dettaglio_costi['ricambi_e_varie_'.$i.'_qta'],2,",","").'&nbsp;</td>
                            <td style="width:60px; text-align:right">&nbsp;'.number_format($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario'],2,",","").'&nbsp;</td>
                            <td style="width:60px; text-align:right">&nbsp;'.number_format($dettaglio_costi['ricambi_e_varie_'.$i.'_sconto'],2,",","").'&nbsp;</td>
                            <td style="width:60px; text-align:right">&nbsp;'.number_format(($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario']-(($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario']*$dettaglio_costi['ricambi_e_varie_'.$i.'_sconto'])/100))*($dettaglio_costi['ricambi_e_varie_'.$i.'_qta']),2,",","").'&nbsp;</td>
                        </tr>
                    ';
                }
                    
                $tot_varie += ($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario']-(($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario']*$dettaglio_costi['ricambi_e_varie_'.$i.'_sconto'])/100))*($dettaglio_costi['ricambi_e_varie_'.$i.'_qta']);
			}
            
            // ERRORE? Ripetuto 3 volte
            $content .= '
                <tr>
					<td style="width:96px; background-color:#f3f3f3; font-size:11px">&nbsp;Ricambi e Varie</td>
					<td style="width:397px">&nbsp;</td>
					<td style="width:50px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
                </tr>
            ';
            // ERRORE? Ripetuto 3 volte
            $content .= '
                <tr>
					<td style="width:96px; background-color:#f3f3f3; font-size:11px">&nbsp;Ricambi e Varie</td>
					<td style="width:397px">&nbsp;</td>
					<td style="width:50px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
                </tr>
            ';
            // ERRORE? Ripetuto 3 volte
            $content .= '
                <tr>
					<td style="width:96px; background-color:#f3f3f3; font-size:11px">&nbsp;Ricambi e Varie</td>
					<td style="width:397px">&nbsp;</td>
					<td style="width:50px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
                </tr>
            ';
			
			if($vergine == 'y')
			{
                $content .= '
                    <tr>
                        <td style="width:96px; background-color:#f3f3f3; font-size:11px">&nbsp;Ricambi e Varie</td>
                        <td style="width:397px">&nbsp;</td>
                        <td style="width:50px; text-align:right">&nbsp;</td>
                        <td style="width:60px; text-align:right">&nbsp;</td>
                        <td style="width:60px; text-align:right">&nbsp;</td>
                        <td style="width:60px; text-align:right">&nbsp;</td>
                    </tr>
                ';
			}
			
			$content .= '
                </table>
            ';
            
            $content .= '
                <table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">				
                    <tr>
                        <td style="width:496px; background-color:#f3f3f3; text-align:right; border-top:0px; font-size:11px">IMPORTO INTERVENTO IVA esclusa S.E. &amp; O.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; EURO&nbsp;</td>
                        <td style="width:239px; border-top:0px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($tot_varie,2,",","")).'&nbsp;</td>
                    </tr>
                </table>
            ';
		}
		
		$content .= '
                        <table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">			
                            <tr>
                                <td style="width:150px; border-top:0px; background-color:#f3f3f3; font-size:11px">&nbsp;Note</td>
                                <td style="width:585px; border-top:0px">&nbsp;'.$bdl->note.'</td>
                            </tr>
                            <tr>
                                <td style="width:150px; border-top:0px; background-color:#f3f3f3; font-size:11px">&nbsp;Rif. ordine</td>
                                <td style="width:585px; border-top:0px">&nbsp;'.($bdl->rif_ordine == 0 ? '--' : $bdl->rif_ordine).'</td>
                            </tr>
                        </table>

                        <table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">			
                            <tr>
                                <td style="width:20px; text-align:center; border-top:0px">'.($bdl->pagato == 1 ? 'X' : '').'</td>
                                <td style="width:160px; border-top:0px; font-size:11px">&nbsp;Pagato&nbsp;</td>
                                <td style="width:20px; text-align:center; border-top:0px">'.($vergine == 'y' ? '' : ($bdl->pagato == 0 ? 'X' : '')).'</td>
                                <td style="width:160px; border-top:0px; font-size:11px">&nbsp;Da pagare&nbsp;</td>
                                <td style="width:20px; text-align:center; border-top:0px">'.($bdl->fatturato == 1 ? 'X' : '').'</td>
                                <td style="width:160px; border-top:0px; font-size:11px">&nbsp;Fatturato&nbsp;</td>
                                <td style="width:20px; text-align:center; border-top:0px">'.($vergine == 'y' ? '' : ($bdl->fatturato == 0 ? 'X' : '')).'</td>
                                <td style="width:157px; border-top:0px; font-size:11px">&nbsp;Da fatturare&nbsp;</td>
                            </tr>
                        </table>
                    
                    </div>

                    <p style="font-size:11px; padding:5px">
                    Il documento sar&agrave; reso perfetto in sede '.($tipo_az == 'tecnotel' ? 'TecnoTel' : 'Ezdirect').' qualora debbano essere consuntivati alcuni costi come riparazione in laboratorio interno o esterno etc. Altres&igrave; in caso di installazione o fornitura di merce non preventivata e/o non confermata attraverso conferma d\'ordine. Una copia completamente compilata sar&agrave; allegata alla fattura commerciale. Il cliente accetta e sottoscrive la buona riuscita dell\'intervento tecnico e conferma, dopo verifica effettuata con il tecnico '.($tipo_az == 'tecnotel' ? 'TecnoTel' : 'Ezdirect').', la risoluzione dei problemi, per i quali l\'intervento stesso &egrave; stato richiesto.
                    
                    </p>
                    
                    <div style="padding:5px; float:left">

                        <table style="display:block;margin:0 auto; width:700px; text-align:center">
                            <tr>&nbsp;<td style="width:350px; font-size:13px">&nbsp;<strong>'.($tipo_az == 'tecnotel' ? 'TecnoTel' : 'Ezdirect').'</strong>&nbsp;</td>&nbsp;<td style="width:350px; font-size:13px">&nbsp;<strong>L\'Utente (timbro e firma)</strong>&nbsp;</td>&nbsp;</tr>&nbsp;
                        </table>
                        <br /><br /><br /><br />
                        
                        <table style="display:block;margin:0 auto; width:735px; font-size:11px; border-collapse:collapse" class="dettagli_tabella" >
                            <tr>
                                <td style="width:115px; padding-left:5px; background-color:#f3f3f3; font-size:11px">&nbsp;Fatturare a</td>
                                <td style="width:230px; padding-left:5px;">&nbsp;</td>
                                <td style="width:115px; padding-left:5px; background-color:#f3f3f3; font-size:11px">&nbsp;Approvato da</td>
                                <td style="width:230px; padding-left:5px;">&nbsp;</td>
                            </tr>
                        </table>

                    </div>

                </div>
            </page>
        ';

	    return $content;
    }
}
