<?php

class FatturaCore extends ObjectModel
{
    public $id; // id_riga
	public $id_customer;
	public $codice_cliente;
	public $id_fattura;
	public $id_storico;
	public $tipo;
	public $data_fattura;
	public $cod_articolo;
	public $desc_articolo;
	public $qt_ord;
	public $qt_sped;
	public $prezzo;
	public $importo_netto;
	public $sconto1;
	public $sconto2;
	public $sconto3;
	public $totale_fattura;
	public $rif_ordine;
	public $rif_vs_ordine;
	public $col_note;
	public $tipo_fattura;
	public $causale_trasporto;
	public $desc_porto;
	public $desc_aspetto_beni;
	public $trasp_mezzo;
	public $codice_vettore;
	public $dati_vettore;
	public $num_colli;
	public $peso;
	public $iva;
	public $imponibile;
	public $desc_fattura;
	public $dati_fatturazione;
	public $dati_spedizione;
	public $banca_appoggio;
	public $iva_percent;
	public $num_riga;
	public $pagamento;
	public $id_category;
	public $id_manufacturer;
	public $data_odv;
	public $ddt;
	public $data_ddt;

    public static $definition = array(
        'table' => 'fattura',
        'primary' => 'id_riga',
        'fields' => array(
            'id_customer' =>  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'codice_cliente' =>  array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 100),
            'id_fattura' =>  array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 100, 'copy_post' => false),
            'id_storico' =>  array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 100, 'copy_post' => false),
            'tipo' =>  array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 25),
            'data_fattura' =>  array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
			'cod_articolo' =>  array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 200),
			'desc_articolo' =>  array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 200),
            'qt_ord' =>  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'qt_sped' =>  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'prezzo' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'importo_netto' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'sconto1' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'sconto2' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'sconto3' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'totale_fattura' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
			'rif_ordine' =>  array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 200),
			'rif_vs_ordine' =>  array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 100),
			'col_note' =>  array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 200),
			'tipo_fattura' =>  array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 100),
			'causale_trasporto' =>  array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 100),
			'desc_porto' =>  array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 100),
			'desc_aspetto_beni' =>  array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 150),
			'trasp_mezzo' =>  array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 100),
			'codice_vettore' =>  array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 50),
			'dati_vettore' =>  array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 400),
            'num_colli' =>  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'peso' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'iva' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'imponibile' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
			'desc_fattura' =>  array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 200),
			'dati_fatturazione' =>	array('type' => self::TYPE_STRING),
			'dati_spedizione' =>	array('type' => self::TYPE_STRING),
			'banca_appoggio' =>	array('type' => self::TYPE_STRING),
            'iva_percent' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'num_riga' =>  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'pagamento' =>  array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 150),
            'id_category' =>  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_manufacturer' =>  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'data_odv' =>  array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'ddt' =>  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'data_ddt' =>  array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
        ),
    );

    // CORREGGERE! Crea il PDF di una fattura; usata in fattura.php (azione del pulsante Scarica PDF), in StrumentiAmazon e in AdminOrders per getDDT e getFF 
    // usa html2pdf
    public function getFattura($id_fattura, $id_customer, $tipo = 'Fattura', $save = 'no') 
	{
		//if(!$this->context)
			$context = Context::getContext();
		//else
			//$context = $this->context;

		$id_lang = $context->language->id;

		if($tipo == 'Fattura' || $tipo == 'Nota di credito')
		{
			$datiFattura = Db::getInstance()->getRow("SELECT * FROM (SELECT * FROM "._DB_PREFIX_.(Tools::getIsset('tipo_fattura') && Tools::getValue('tipo_fattura') == 'nota' ? 'note_di_credito' : 'fattura')." WHERE id_fattura = '$id_fattura' AND iva_percent >= 0 AND id_customer = $id_customer ORDER BY num_riga) t GROUP BY $id_fattura");
			
			$numero_righe = Db::getInstance()->getValue("SELECT count(num_riga) AS tot FROM "._DB_PREFIX_.(Tools::getIsset('tipo_fattura') && Tools::getValue('tipo_fattura') == 'nota' ? 'note_di_credito' : 'fattura')." WHERE id_fattura = '$id_fattura' AND iva_percent >= 0 AND id_customer = $id_customer ORDER BY num_riga");
			
			$datiCliente = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."customer WHERE id_customer = ".$datiFattura['id_customer']."");
		
			$fatturazione = $datiFattura['dati_fatturazione'];
			$spedizione = $datiFattura['dati_spedizione'];
			
			switch ($datiFattura['iva_percent']) {
				case '4': $desc_iva = "4%"; break;
				case '10': $desc_iva = "10%"; break;
				case '21': $desc_iva = "21%"; break;
				case '58': $desc_iva = "Art.7-DPR633/72"; break;
				case '59': $desc_iva = "Corr.Autoc.Vent."; break;
				case '60': $desc_iva = "Corr. da vent."; break;
				case '61': $desc_iva = "Viaggi CEE"; break;
				case '62': $desc_iva = "Viag. fuori CEE"; break;
				case '63': $desc_iva = "Viag. misti CEE"; break;
				case '64': $desc_iva = "Viag.mist.f.CEE"; break;
				case '65': $desc_iva = "Art.8-DPR633/72"; break;
				case '66': $desc_iva = "Exp. a.8 1c B"; break;
				case '67': $desc_iva = "A.8.9.72 DPR633"; break;
				case '68': $desc_iva = "Art.8- lett.A"; break;
				case '69': $desc_iva = "Art.8 1c let.c"; break;
				case '70': $desc_iva = "Art.8-8 bis-9"; break;
				case '71': $desc_iva = "Art.26 BIS"; break;
				case '72': $desc_iva = "A.41,331/93"; break;
				case '73': $desc_iva = "A.71 1?c.Vatic."; break;
				case '74': $desc_iva = "A.71 1?c.S.Mar."; break;
				case '75': $desc_iva = "Servizi e.art.9"; break;
				case '76': $desc_iva = "A.40,c.5 331/93"; break;
				case '77': $desc_iva = "A.40,c.6 331/93"; break;
				case '78': $desc_iva = "Art.9 - 2? c."; break;
				case '79': $desc_iva = "Art.8 - 2? C"; break;
				case '80': $desc_iva = "Esenti art. 10"; break;
				default: ''; break;
			}
			
			$datafattura_raw = $datiFattura['data_fattura'];
			$datafattura = date("d/m/Y", strtotime($datafattura_raw));
			
			$dati_fatturazione = explode('--', $fatturazione);
			$dati_spedizione = explode('--', $spedizione);
			$numero_documento = explode('-', $datiFattura['id_fattura']);
			$riferimento_ordine = explode(' ', $datiFattura['rif_ordine']);
		}
		else if($tipo == 'ff')
		{
			$numero_documento = Db::getInstance()->getValue('SELECT nm FROM '._DB_PREFIX_.'ff WHERE rd = '.$id_fattura);
			if($numero_documento == 0)
			{
				$ultima_ddt = Db::getInstance()->getValue('SELECT nm FROM '._DB_PREFIX_.'ff ORDER BY nm DESC');
				$numero_documento = $ultima_ddt+1;
				$products = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'order_detail WHERE id_order = '.$id_fattura);
				$products_ddt_array = array();

				foreach($products as $productddt)
				{
					$prdarray = array();
					$prdarray['cod_articolo'] = $productddt['product_reference'];
					$prdarray['desc_articolo'] = $productddt['product_name'];
					$prdarray['qt_sped'] = $productddt['product_quantity'];
					$prdarray['prezzo'] = ($productddt['product_price'] - ($productddt['product_price'] * $productddt['reduction_percent']/100));
					$prdarray['sconto1'] = "";
					$prdarray['iva_percent'] = $productddt['tax_rate'];
					$prdarray['importo_netto'] = $productddt['product_quantity'] * ($productddt['product_price'] - ($productddt['product_price'] * $productddt['reduction_percent']/100));
					$prdarray['peso'] = $productddt['product_weight'];
					$products_ddt_array[] = $prdarray;
				}
				
				//trasp
				$trasp = Db::getInstance()->getValue('SELECT total_shipping FROM '._DB_PREFIX_.'orders WHERE id_order = '.$id_fattura);
				$carrier_tax_rate = Db::getInstance()->getValue('SELECT carrier_tax_rate FROM '._DB_PREFIX_.'orders WHERE id_order = '.$id_fattura);
				
				if($trasp > 0)
				{
					$prdarray = array();
					$prdarray['cod_articolo'] = 'TRASP';
					$prdarray['desc_articolo'] = 'Trasporto';
					$prdarray['qt_sped'] = 1;
					$prdarray['prezzo'] = $trasp / ((100 + $carrier_tax_rate) / 100);
					$prdarray['sconto1'] = "";
					$prdarray['iva_percent'] = $carrier_tax_rate;
					$prdarray['importo_netto'] = $trasp / ((100 + $carrier_tax_rate) / 100);
					$products_ddt_array[] = $prdarray;
				}

				//commiss
				$comms = Db::getInstance()->getValue('SELECT total_commissions FROM '._DB_PREFIX_.'orders WHERE id_order = '.$id_fattura);

				if($comms > 0)
				{
					$carrier_tax_rate = Db::getInstance()->getValue('SELECT carrier_tax_rate FROM '._DB_PREFIX_.'orders WHERE id_order = '.$id_fattura);
					$prdarray = array();
					$prdarray['cod_articolo'] = 'COMMIS';
					$prdarray['desc_articolo'] = 'Commissioni';
					$prdarray['qt_sped'] = 1;
					$prdarray['prezzo'] = $comms;
					$prdarray['sconto1'] = "";
					$prdarray['iva_percent'] = $carrier_tax_rate;
					$prdarray['importo_netto'] = $comms;
					$products_ddt_array[] = $prdarray;
				}

				$products_ddt_array = serialize($products_ddt_array);

				Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'ff (nm, an, dt, rd, ct, tp) VALUES ('.$numero_documento.', "'.date('y').'", "'.date('Y-m-d H:i:s').'", '.$id_fattura.', \''.$products_ddt_array.'\', "A")'); 
			}
			else
			{
				$products = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'order_detail WHERE id_order = '.$id_fattura);

				$products_ddt_array = array();
				foreach($products as $productddt) {
					$products_ddt_array[$productddt['product_id']] = $productddt['ddt'];
				}	
				$products_ddt_array = serialize($products_ddt_array);
				
				$attuale_ddt = Db::getInstance()->getValue('SELECT ct FROM '._DB_PREFIX_.'ff WHERE rd = '.$id_fattura.' ORDER BY nm DESC');
				/*
				per parziali... vedere come fare... 
				if($products_ddt_array != $attuale_ddt && $attuale_ddt != '')
				{
					$ultima_ddt = Db::getInstance()->getValue('SELECT ddt FROM '._DB_PREFIX_.'orders ORDER BY ddt DESC');
					$numero_documento = $ultima_ddt+1;
					Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'ddt (ddt, ddt_date id_order, products) VALUES ('.$numero_documento.', "'.date('Y-m-d H:i:s').'", '.$id_fattura.', \''.$products_ddt_array.'\')'); 
					Db::getInstance()->executeS('UPDATE '._DB_PREFIX_.'orders SET ddt = '.$numero_documento.' WHERE id_order = '.$id_fattura); 
				}
				
				Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'ddt SET products = \''.$products_ddt_array.'\' WHERE id_order = '.$id_fattura); 
				*/
			}	
			
			$real_numero_documento = Db::getInstance()->getValue('
				SELECT CONCAT(LPAD(nm,10,"0"),"-",an )
				FROM '._DB_PREFIX_.'ff
				WHERE ff.nm ='.$numero_documento
			);
				
			$numero_documento = $real_numero_documento;
			$id_fatturazione = Db::getInstance()->getValue('SELECT id_address_invoice FROM '._DB_PREFIX_.'orders WHERE id_order = '.$id_fattura);
			$id_spedizione = Db::getInstance()->getValue('SELECT id_address_delivery FROM '._DB_PREFIX_.'orders WHERE id_order = '.$id_fattura);
			$datiCliente = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'customer WHERE id_customer = '.$id_customer.'');
			$datiFattura['codice_cliente'] = $datiCliente['codice_spring'];
			$datafattura_raw = Db::getInstance()->getValue('SELECT dt FROM '._DB_PREFIX_.'ff WHERE rd = '.$id_fattura);
			$datafattura = date('d/m/Y', strtotime($datafattura_raw));
			$datiFattura['rif_vs_ordine'] = $id_fattura;
			$datiFattura['pagamento'] = Db::getInstance()->getValue('SELECT payment FROM '._DB_PREFIX_.'orders WHERE id_order = '.$id_fattura);
			$dati_fatturazione = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'address WHERE id_address = '.$id_fatturazione.'');
			$dati_spedizione = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'address WHERE id_address = '.$id_spedizione.'');
			$datiFattura['iva_percent'] = $carrier_tax_rate;
		}
		else
		{
			$numero_documento = Db::getInstance()->getValue('SELECT ddt FROM '._DB_PREFIX_.'orders WHERE id_order = '.$id_fattura);

			if($numero_documento == 0)
			{
				$ultima_ddt = Db::getInstance()->getValue('SELECT ddt FROM '._DB_PREFIX_.'orders ORDER BY ddt DESC');
				$numero_documento = $ultima_ddt+1;
				$products = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'order_detail WHERE ddt != 1 AND id_order = '.$id_fattura);
			
				$products_ddt_array = array();
				foreach($products as $productddt)
				{
					$products_ddt_array[$productddt['product_id']] = $productddt['ddt'];
				}	
				$products_ddt_array = serialize($products_ddt_array);

				Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'ddt (ddt, ddt_date, id_order, products) VALUES ('.$numero_documento.', "'.date('Y-m-d H:i:s').'", '.$id_fattura.', \''.$products_ddt_array.'\')'); 
				Db::getInstance()->executeS('UPDATE '._DB_PREFIX_.'orders SET ddt = '.$numero_documento.' WHERE id_order = '.$id_fattura); 
			}
			else
			{
				$products = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'order_detail WHERE ddt != 1 AND id_order = '.$id_fattura);
				
				$products_ddt_array = array();
				foreach($products as $productddt) {
					$products_ddt_array[$productddt['product_id']] = $productddt['ddt'];
				}
				$products_ddt_array = serialize($products_ddt_array);
				
				$attuale_ddt = Db::getInstance()->getValue('SELECT products FROM '._DB_PREFIX_.'ddt WHERE id_order = '.$id_fattura.' ORDER BY ddt DESC');
				if($products_ddt_array != $attuale_ddt && $attuale_ddt != '')
				{
					$ultima_ddt = Db::getInstance()->getValue('SELECT ddt FROM '._DB_PREFIX_.'orders ORDER BY ddt DESC');
					$numero_documento = $ultima_ddt+1;
					Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'ddt (ddt, ddt_date id_order, products) VALUES ('.$numero_documento.', "'.date('Y-m-d H:i:s').'", '.$id_fattura.', \''.$products_ddt_array.'\')'); 
					Db::getInstance()->executeS('UPDATE '._DB_PREFIX_.'orders SET ddt = '.$numero_documento.' WHERE id_order = '.$id_fattura); 
				}
				
				Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'ddt SET products = \''.$products_ddt_array.'\' WHERE id_order = '.$id_fattura); 
			}	
			
			$real_numero_documento = Db::getInstance()->getValue('
				SELECT CONCAT(o1.num,"-",year(o1.date_add) )
				FROM (

				SELECT @row_number :=
				CASE
				WHEN @year_no = year( orders.ddt_date )
				THEN @row_number +1
				ELSE 1
				END AS num, @year_no := year( orders.ddt_date ) AS oYear, orders. *
				FROM (

				SELECT orders.*, ddt.ddt_date
				FROM '._DB_PREFIX_.'orders
				JOIN '._DB_PREFIX_.'ddt ON ddt.id_order = orders.id_order
				WHERE orders.ddt > 0
				GROUP BY orders.ddt
				ORDER BY ddt.ddt_date ASC
				)orders, (

				SELECT @row_number :=0
				)t, (

				SELECT @year_no :=0
				)y
				)o1
				WHERE o1.ddt ='.$numero_documento
			);
				
			$numero_documento = $real_numero_documento;
			$id_fatturazione = Db::getInstance()->getValue('SELECT id_address_invoice FROM '._DB_PREFIX_.'orders WHERE id_order = '.$id_fattura);
			$id_spedizione = Db::getInstance()->getValue('SELECT id_address_delivery FROM '._DB_PREFIX_.'orders WHERE id_order = '.$id_fattura);
			$datiCliente = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'customer WHERE id_customer = '.$id_customer.'');
			$datiFattura['codice_cliente'] = $datiCliente['codice_spring'];
			$datafattura_raw = Db::getInstance()->getValue('SELECT date_add FROM '._DB_PREFIX_.'orders WHERE id_order = '.$id_fattura);
			$datafattura = date('d/m/Y', strtotime($datafattura_raw));
			$datiFattura['rif_vs_ordine'] = $id_fattura;
			$datiFattura['pagamento'] = Db::getInstance()->getValue('SELECT payment FROM '._DB_PREFIX_.'orders WHERE id_order = '.$id_fattura);
			$dati_fatturazione = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'address WHERE id_address = '.$id_fatturazione.'');
			$dati_spedizione = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'address WHERE id_address = '.$id_spedizione.'');
		}

		$content = '<page><div id="contenitore" style="width:720px; height:1070px; font-family: Arial, Verdana, Sans-Serif; display:block; position:relative; margin:0 auto; margin-left:15px">
			<table style="width:720px; font-size:10px; display:block; margin:0 auto">
			<tr>
			<td style="width:235px">
			<img src="https://www.ezdirect.it/img/logo-ezdirect-it.jpg" alt="Ezdirect"  title="Ezdirect"  style="width:200px; display:block; margin:0 auto" /><br /><br />
			</td>
			<td style="width:215px; padding-left:5px; padding-right:11px">
			<br /><span style="font-size:10px; color:#7d7d7d">Documento non valido ai fini fiscali ai sensi art. 21 DPR 633/72. L\'originale &egrave; disponibile all\'indirizzo PEC o codice SDI fornito, oppure nell\'area riservata dell\'Agenzia delle Entrate.</span>
			</td>
			<td style="width:235px">
			<h1 style="text-align:left; font-size:15px; margin-top:5px; margin-bottom:5px; font-weight:normal; color:#00009a">
				'.($tipo == 'Fattura' || $tipo == 'ff' ? ($tipo == 'Fattura' ? 'Fattura Accompagnatoria' : 'Fattura pro forma') : ($tipo == 'Nota di credito' ? 'Nota di credito' : 'Documento di Trasporto (D.D.T.)')).'
			</h1>
			Codice Cliente <strong>'.$id_customer.'</strong>
			</td>
			</tr>
			</table>
			<table style="width:720px; font-size:10px; display:block; margin:0 auto">
				<tr>
				<td style="width:240px">
				<strong style="font-size:12px">ezdirect srl a socio unico</strong><br />
				Via Nerino Garbuio snc - 54038 Montignoso (MS)<br />
				Tel: 0585 821163 - Fax: 0240708331<br />
				info@ezdirect.it - www.ezdirect.it
				</td>
				<td style="width:240px">
				<br />
				P.IVA-C.F. 01164670455 REA:118272 04-05-07<br />
				REGISTRO A.E.E. IT14020000008270<br />
				Ai fini durc: Inail 18183206 Inps 4602927808
				</td>
				<td style="width:240px">
				Banca d\'appoggio:
				Banco Popolare CR Lucca Pisa Livorno<br />
				ABI: 05034 CAB: 69948 CIN:T<br />
				C/C 001454 SWIFT:BAPPIT21S44<br />
				IBAN: IT58T0503469948000000001454
				</td>
				</tr>
			</table>
			<br />
		';
		
		if($tipo == 'Fattura' || $tipo == 'Nota di credito')
		{
		
			$content .= '
			<table style="width:720px; font-size:12px; display:block; margin:0 auto">
				<tr>
				<td style="width:360px">
				<strong>INTESTAZIONE '.($tipo == 'Nota di credito' ? 'NOTA DI CREDITO' : 'FATTURA').'</strong><br />
				<strong style="font-size:16px">'.$dati_fatturazione[0].'</strong><br /><br />
				'.$dati_fatturazione[1].'<br />
				'.$dati_fatturazione[2].' '.$dati_fatturazione[3].' ('.trim($dati_fatturazione[4]).')
				<br /><br />
				<span style="font-size:10px">PARTITA IVA <strong>'.$datiCliente['vat_number'].'</strong></span> <br />
				<span style="font-size:10px">COD. FISCALE <strong>'.$datiCliente['tax_code'].'</strong></span>
				</td>
				<td style="width:360px">
				<strong>DESTINAZIONE DELLA MERCE</strong><br />
				<strong style="font-size:16px">'.$dati_spedizione[0].'</strong><br /><br />
				'.$dati_spedizione[1].'<br />
				'.$dati_spedizione[2].' '.$dati_spedizione[3].' ('.trim($dati_spedizione[4]).')
				</td>
				</tr>
			</table>';
		}
		else
		{
			$content .= '
			<table style="width:720px; font-size:12px; display:block; margin:0 auto">
				<tr>
				<td style="width:360px">
				<strong>INTESTAZIONE '.($tipo == 'ff' ? 'FATTURA' : 'DDT').'</strong><br />
				<strong style="font-size:16px">'.($dati_fatturazione['company'] == '' ? $dati_fatturazione['firstname']." ".$dati_fatturazione['lastname'] : $dati_fatturazione['company']).'</strong><br /><br />
				'.$dati_fatturazione['address1'].'<br />
				'.$dati_fatturazione['postcode'].' '.$dati_fatturazione['city'].' ('.Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'state WHERE id_state = '.$dati_fatturazione['id_state']).')
			<br />'.Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'country_lang WHERE id_lang = '.$id_lang.' AND id_country = '.$dati_fatturazione['id_country']).'
			<br /><br />
				<span style="font-size:10px">PARTITA IVA <strong>'.$datiCliente['vat_number'].'</strong></span> <br />
				<span style="font-size:10px">COD. FISCALE <strong>'.$datiCliente['tax_code'].'</strong></span>
				</td>
				<td style="width:360px">
				<strong>DESTINAZIONE DELLA MERCE</strong><br />
				<strong style="font-size:16px">'.($dati_spedizione['company'] == '' ? $dati_spedizione['firstname']." ".$dati_spedizione['lastname'] : $dati_spedizione['company']).'</strong><br /><br />
				'.$dati_spedizione['address1'].'<br />
				'.$dati_spedizione['postcode'].' '.$dati_spedizione['city'].' ('.Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'state WHERE id_state = '.$dati_spedizione['id_state']).')
			<br />'.Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'country_lang WHERE id_lang = '.$id_lang.' AND id_country = '.$dati_spedizione['id_country']).'
				</td>
				</tr>
			</table>';
			
		}
		
		$content .= '
			<table style="width:690px; border-top:1px solid #f86000; border-bottom:1px solid black; display:block; margin:0 auto">
			
				<tr style="font-size:9px">
				<td style="width:123px"><em>PAGAMENTO</em></td>
				<td style="width:247px"><em>DESCRIZIONE PAGAMENTO</em></td>
				<td style="width:70px"><em></em></td>
				<td style="width:125px"><em>'.($tipo == 'Fattura' || $tipo == 'ff' ? 'FATTURA N.' : ($tipo == 'Nota di credito' ? 'NOTA DI CREDITO' : 'DDT')).'</em></td>
				<td style="width:125px"><em>DATA '.($tipo == 'Fattura' || $tipo == 'ff' ? 'FATTURA' : ($tipo == 'Nota di credito' ? 'NOTA DI CREDITO' : 'DDT')).'</em></td>
				</tr>
				<tr style="font-size:12px; font-weight:bold">
				<td style="width:123px"></td>
				<td style="width:247px">'.$datiFattura['pagamento'].'</td>
				<td style="width:70px"></td>
				<td style="width:125px">'.($tipo == 'Fattura' || $tipo == 'Nota di credito' ? $numero_documento[0].'/'.date('Y',strtotime($datafattura_raw)) : $numero_documento).'</td>
				<td style="width:125px">'.($tipo == 'Fattura' || $tipo == 'Nota di credito' || $tipo == 'ff' ? $datafattura : date('d/m/Y')).'</td>
				</tr>
			</table>

			<table style="width:690px; border-bottom:1px solid black; display:block; margin:0 auto">
				<tr style="font-size:9px">
				<td style="width:123px"><em>'.($tipo == 'Fattura' || $tipo == 'Nota di credito' || $tipo == 'ff' ? '' : 'ORDINE').'</em></td>
				<td style="width:219px"><em>VOSTRA BANCA DI APPOGGIO</em></td>
				<td style="width:202px"><em></em></td>
				<td style="width:155px"><em></em></td>
				</tr>
				<tr style="font-size:12px; font-weight:bold">
				<td style="width:123px">'.($tipo == 'Fattura' || $tipo == 'Nota di credito' || $tipo == 'ff' ? '' : $id_fattura).'</td>
				<td style="width:219px"></td>
				<td style="width:202px"></td>
				<td style="width:155px"></td>
				</tr>
			</table>
			<br /><br />
			
			<table cellpadding="0" cellspacing="0" style="width:750px; border-top:1px solid #f86000; display:block; margin:0 auto">
				<tr style="font-size:10px; height:22px; width:690px; background-color:#ffedd9; border-bottom:1px solid #f86000; color:#000; font-weight:bold">
				<td style="width:'.($tipo == 'Fattura' || $tipo == 'Nota di credito' || $tipo == 'ff' ? '100' : '250').'px">Articolo</td>
				<td style="width:'.($tipo == 'Fattura' || $tipo == 'Nota di credito' || $tipo == 'ff' ? '276' : '386').'px">Descrizione / Annotazioni</td>
				<td style="width:13px">UM</td>
				<td style="width:58px; text-align:right">Quantità</td>
				'.($tipo == 'Fattura' || $tipo == 'Nota di credito' || $tipo == 'ff' ? '<td style="width:58px; text-align:right">Prezzo</td>
				<td style="width:58px; text-align:right">% Sc.</td>
				<td style="width:68px; text-align:right">Importo</td>
				<td style="width:62px; text-align:right">Iva</td>' : '').'
				</tr>
			</table>
			<table style="width:750px; border-top:1px solid #f86000; display:block; margin:0 auto">
		';
		
		if($tipo == 'ff')
		{
			$peso_complessivo = 0;
			$imponibile = 0;
			$prodottiFattura = Db::getInstance()->getValue('SELECT ct FROM '._DB_PREFIX_.'ff WHERE rd = '.$id_fattura);
			$prodottiFattura = unserialize($prodottiFattura);
			foreach($prodottiFattura as $p)
			{
				$peso += $p['peso'];
				$imponibile += $p['importo_netto'];
			}
			
			$datiFattura['imponibile'] = $imponibile;
			$datiFattura['iva'] = $imponibile * ($datiFattura['iva_percent'] / 100);
			$datiFattura['totale_fattura'] = $imponibile + $datiFattura['iva'];
		}

		if($tipo == 'Fattura' || $tipo == 'Nota di credito' || $tipo == 'ff')
		{
			$rif_ordine_rig = 0;
			
			if($tipo == 'ff')
			{
				
				$rif_ordine_rig = $id_fattura;
				$prodottiFattura = Db::getInstance()->getValue('SELECT ct FROM '._DB_PREFIX_.'ff WHERE rd = '.$id_fattura);
				
				if($prodottiFattura == '')
					$prodottiFattura = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."order_detail JOIN "._DB_PREFIX_."orders o ON o.id_order = order_detail.id_order WHERE ddt != 1 AND o.id_order = '$id_fattura' AND o.id_customer = $id_customer");
				else
					$prodottiFattura = unserialize($prodottiFattura);
			
			}
			else
			{
				$prodottiFattura = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_.($tipo == 'Nota di credito' ? 'note_di_credito' : 'fattura')." WHERE id_fattura = '$id_fattura' AND id_customer = $id_customer ORDER BY rif_ordine ASC, num_riga ASC");
			}
			$num_prod = 0;
			$rif_ordine_rig = 0;
			$rif_ddt = 0;
			
			foreach($prodottiFattura as $rigaFattura) {
				
				if($tipo == 'ff')
					$rigaFattura['rif_ordine'] = $id_fattura;
			
			$num_prod++;
			
			if($num_prod > 13) {
			}
			else {
			
					if(empty($rigaFattura['desc_articolo']))  {
				
					}
				
					else {
						
					if($rigaFattura['rif_ordine'] != $rif_ordine_rig)
					{
						$rif_ordine_rig = $rigaFattura['rif_ordine'];
						
						if($rif_ordine_rig != 0)
						{
							
							$content .= '
							<tr style="font-size:10px;">
							<td style="width:100px"></td>
							<td style="width:276px"><strong>Rif. Vs. Ordine n. '.$rif_ordine_rig.' del '.date('d/m/Y', strtotime(Db::getInstance()->getValue('SELECT date_add FROM '._DB_PREFIX_.'orders WHERE id_order = '.$rif_ordine_rig))).'</strong></td>
							<td style="width:13px;"></td>
							<td style="width:48px; text-align:right"></td><td style="width:50px; text-align:right"></td><td style="width:58px; text-align:right"></td><td style="width:68px; text-align:right"></td><td style="width:62px; text-align:right"></td>
							</tr>';
						}
						
					}	

					if($rigaFattura['ddt'] != $rif_ddt && $rigaFattura['ddt'] > 0)
					{
						$rif_ddt = $rigaFattura['ddt'];
						
						if($rif_ddt != 0)
						{
							
							$content .= '
							<tr style="font-size:10px;">
							<td style="width:100px"></td>
							<td style="width:276px"><strong>Rif. Documento di trasporto n. '.$rigaFattura['ddt'].' del '.date('d/m/Y', strtotime($rigaFattura['data_ddt'])).'</strong></td>
							<td style="width:13px;"></td>
							<td style="width:48px; text-align:right"></td><td style="width:50px; text-align:right"></td><td style="width:58px; text-align:right"></td><td style="width:68px; text-align:right"></td><td style="width:62px; text-align:right"></td>
							</tr>';
						}	
					}
				
					$content .= '
						<tr style="font-size:10px;">
						<td style="width:100px">'.$rigaFattura['cod_articolo'].'</td>
						<td style="width:276px">'.htmlentities($rigaFattura['desc_articolo']).'</td>
						<td style="width:13px;">'.($rigaFattura['qt_sped'] == 0 ? '' : 'N.').'</td>
						<td style="width:48px; text-align:right">'.($rigaFattura['qt_sped'] == 0 ? '' : $rigaFattura['qt_sped']).'</td>
						<td style="width:50px; text-align:right">'.($rigaFattura['qt_sped'] == 0 ? '' : number_format($rigaFattura['prezzo'],2,',','')).'</td>
						<td style="width:58px; text-align:right">'.($rigaFattura['sconto1'] != 0 ? number_format($rigaFattura['sconto1'],2,',','') : '').'</td>
						<td style="width:68px; text-align:right">'.($rigaFattura['qt_sped'] == 0 ? '' : number_format($rigaFattura['importo_netto'],2,',','')).'</td>
						<td style="width:62px; text-align:right">'.($rigaFattura['qt_sped'] == 0 ? '' : (int)$rigaFattura['iva_percent']).'</td>
						</tr>';
					}
				}
			}
		}
		else
		{
			if(Tools::getIsset('id_ddt'))
				$prodottiFattura = Db::getInstance()->getValue('SELECT products FROM '._DB_PREFIX_.'ddt WHERE id_order = '.$id_fattura.' AND ddt = '.Tools::getValue('id_ddt'));
			else
				$prodottiFattura = Db::getInstance()->getValue('SELECT products FROM '._DB_PREFIX_.'ddt WHERE id_order = '.$id_fattura.' ORDER BY ddt DESC');
			
			if($prodottiFattura == '')
				$prodottiFattura = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."order_detail JOIN "._DB_PREFIX_."orders o ON o.id_order = order_detail.id_order WHERE ddt != 1 AND o.id_order = '$id_fattura' AND o.id_customer = $id_customer");
			else
				$prodottiFattura = unserialize($prodottiFattura);
			
			$num_prod = 0;
			
			foreach($prodottiFattura as $key => $value) {
			
				if($value['product_id'] > 0)
					$key1 = $value['product_id'];
				else
					$key1 = $key;
				$num_prod++;
				
				if($num_prod > 24) {
				}
				else {
					
					$rigaFattura = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."order_detail JOIN "._DB_PREFIX_."orders o ON o.id_order = order_detail.id_order WHERE o.id_order = '$id_fattura' AND o.id_customer = $id_customer AND order_detail.product_id = ".$key1."");
					
					if(empty($rigaFattura['product_name']))  {
				
					}
				
				
					else {
				
					$content .= '
						<tr style="font-size:10px;">
						<td style="width:250px">'.$rigaFattura['product_reference'].'</td>
						<td style="width:386px">'.htmlentities($rigaFattura['product_name']).'</td>
						<td style="width:13px;">'.($rigaFattura['product_quantity'] == 0 ? '' : 'N.').'</td>
						<td style="width:49px; text-align:right">'.($rigaFattura['ddt'] == '0' || $rigaFattura['ddt'] == '' ? $rigaFattura['product_quantity'] : $rigaFattura['product_quantity']).'</td>
						</tr>';
					}
				}
			}
		}
		
		$content .= '</table>';
		
		if($numero_righe > 13) {
			$content .= '*SEGUE*';
		}
		
		$tax_regime = Db::getInstance()->getValue('SELECT tax_regime FROM '._DB_PREFIX_.'customer WHERE id_customer = '.$id_customer);
		
		if($tipo == 'Fattura' || $tipo == 'Nota di credito' || $tipo == 'ff')
		{
			$content .= '<div style="display:block; margin:0 auto; position:absolute; bottom:240px">
				<table style="width:690px; text-align:right; border-top:1px solid black; border-bottom:1px solid black; display:block; margin:0 auto;">
					<tr style="font-size:9px">
					<td style="width:130px"><em>SPESE BOLLO</em></td>
					<td style="width:100px"><em>SPESE INCASSO</em></td>
					<td style="width:100px"><em>SPESE TRASPORTO</em></td>
					<td style="width:100px"><em>SCONTI</em></td>
					<td style="width:100px"><em>SCONTO PAGAMENTO</em></td>
					<td style="width:160px"><em>IMPORTO SCONTATO</em></td>
					</tr>
					<tr style="font-size:11px; font-weight:bold">
					<td style="width:130px"></td>
					<td style="width:100px"></td>
					<td style="width:100px"></td>
					<td style="width:100px"></td>
					<td style="width:100px"></td>
					<td style="width:160px">'.($numero_righe > 13 ? '*SEGUE*' : number_format($datiFattura['imponibile'],2,',','')).'</td>
					</tr>
				</table>
				<table style="width:690px; text-align:right; border-bottom:1px solid black; display:block; margin:0 auto; ">
					<tr style="font-size:9px">
					<td style="width:115px; text-align:left"><em>COD. IVA</em></td>
					<td style="width:110px"><em>IMPONIBILE</em></td>
					<td style="width:200px"><em>% IVA - DESCRIZIONE IVA</em></td>
					<td style="width:110px"><em>IMPORTO IVA</em></td>
					<td style="width:160px"><em>TOTALE IMPONIBILE</em></td>
					</tr>
					<tr style="font-size:11px; font-weight:bold">
					<td style="width:115px; text-align:left">'.($numero_righe > 13 ? '*SEGUE*' : (int)$datiFattura['iva_percent']).'</td>
					<td style="width:110px">'.($numero_righe > 13 ? '' : number_format($datiFattura['imponibile'],2,',','')).'</td>
					<td style="width:200px">'.($numero_righe > 13 ? '' : $desc_iva).'</td>
					<td style="width:110px">'.($numero_righe > 13 ? '' : number_format($datiFattura['iva'],2,',','')).'</td>
					<td style="width:160px">'.($numero_righe > 13 ? '' : number_format($datiFattura['imponibile'],2,',','')).'<br /><br />
					<span style="font-size:9px; font-weight:normal"><em>TOTALE IVA</em></span><br />
					'.($numero_righe > 13 ? '' : number_format($datiFattura['iva'],2,',','')).'
					</td>
					</tr>
				</table>
					
				<table style="width:690px; text-align:right; display:block; margin:0 auto; ">
					<tr style="font-size:9px">
					<td '.($tax_regime == 4 ? 'colspan="2" style="width:470px; text-align:left"' : 'style="width:190px; text-align:left"').'>'.($tax_regime == 4 ? 'Fattura emessa in regime di scissione dei pagamenti ex art. 17-ter DPR 633/72<br /><br /><em>Scadenza</em><br />'.$datafattura.'<br /><em>Importo</em><br />'.number_format($datiFattura['imponibile'],2,',','') : '<em>A VISTA</em>').'</td>
					'.($tax_regime == 4 ? '' : '<td style="width:280px"><em>SCADENZE ED IMPORTI</em></td>').'
					<td style="width:235px"><em>TOTALE DOCUMENTO</em></td>
					</tr>
					<tr style="font-size:11px; font-weight:bold">
					<td style="width:190px; text-align:left">'.($numero_righe > 13 ? '' : ($tax_regime == 4 ? '' : number_format($datiFattura['totale_fattura'],2,',',''))).'</td>
					<td style="width:280px"></td>
					<td style="width:235px">'.($numero_righe > 13 ? '*SEGUE*' : number_format($datiFattura['totale_fattura'],2,',','')).'</td>
					</tr>
				</table>
			</div>
			';
		}
		
		if($tipo == 'DDT' || $tipo == 'ff')
		{
			$datiFattura['causale_trasporto'] = 'Vendita';
			$datiFattura['desc_porto'] = 'Franco Add.Fatt';
			$datiFattura['trasp_mezzo'] = 'Vettore';
			$datiFattura['dati_vettore'] = 'GLS National Express s.r.l.<br />Viale San Bartolomeo 725 La Spezia<br />N. Iscrizione all\'albo: SP3601824D';
		}
		
		if($tipo == 'ff')
		{
			$datiFattura['num_colli'] = 1;
			$datiFattura['peso'] = number_format(($peso_complessivo/1000),2,',','');
		}
		
		if($tipo == 'DDT')
		{
			$content .= '<div style="position:absolute; padding:5px; border:1px solid black; font-size:10px; bottom:200px;  left:0px; height:20px; width:712px;">
			Seguir&agrave; fattura commerciale (sar&agrave; disponibile sul tuo account a partire dal primo giorno successivo al mese in corso). I nostri colli sono sigillati da nastro con logo Ezdirect. In caso di assenza o manomissione, accettare con riserva. Contestazioni, smarrimenti, danneggiamenti dei colli, vanno segnalati entro 8 gg dal ricevimento. 
			</div>';
		}
		
			
		$content .= '
			<div style="position:absolute; border:1px solid black; font-size:10px; '.($tipo == 'DDT' ? ' bottom:20px' : 'bottom:80px').'; left:0px;  '.($tipo == 'DDT' ? ' height:164px' : 'height:130px').'; width:540px;">
				<table style="width:450px; font-size:10px">
				<tr>
				<td style="width:230px">
				Causale del trasporto
				</td>
				<td style="width:120px">
				Porto
				</td>
				<td style="width:50px">
				N. Colli
				</td>
				<td style="width:50px">
				Peso
				</td>
				</tr>
				<tr>
				<td style="width:230px; font-size:12px">
				'.($numero_righe > 13 ? '' : $datiFattura['causale_trasporto']).'
				</td>
				<td style="width:120px; font-size:12px">
				'.($numero_righe > 13 ? '' : $datiFattura['desc_porto']).'
				</td>
				<td style="width:50px; font-size:12px">
				'.($numero_righe > 13 ? '' : $datiFattura['num_colli']).'
				</td>
				<td style="width:50px; font-size:12px">
				'.($tipo == 'Fattura' || $tipo == 'Nota di credito' ? ($numero_righe > 13 ? '' : number_format($datiFattura['peso'],2,',','')) : '').'
				</td>
				
				</tr>
				<tr>
				<td style="width:230px">
				Trasporto a cura
				</td>
				<td style="width:120px" colspan="2">
				Data inizio trasporto
				</td>
				<td style="width:100px">
				Data e ora del ritiro
				</td>
				</tr>
				<tr>
				<td style="width:230px; font-size:12px">
				'.($numero_righe > 13 ? '' : $datiFattura['trasp_mezzo']).'
				</td>
				<td style="width:120px; font-size:12px" colspan="2">
				
				</td>
				<td style="width:100px; font-size:12px">
				'.($tipo == 'Fattura'  || $tipo == 'Nota di credito' ? ($numero_righe > 13 ? '' : $datafattura) : '').'
				</td>
				
				</tr>
					<tr>
				<td style="width:350px"  colspan="3">
				Vettore
				</td>
				<td style="width:100px">
				
				</td>
				</tr>
				<tr>
				<td style="width:350px; font-size:12px" colspan="3">
				'.($numero_righe > 13 ? '' : $datiFattura['dati_vettore']).'
				</td>
				<td style="width:100px; font-size:12px">
					
				</td>
				
				
				</tr>
				</table>
			
			
			</div>
			
			<div style="position:absolute; font-size:9px; bottom:30px; left:0px; width:540px;">
		
				'.($tipo == 'Fattura'  || $tipo == 'Nota di credito' ? '
				I reclami devono essere inoltrati entro 8 gg dal ricevimento della merce.<br />
				In caso di ritardato pagamento saranno addebitati gli interessi di mora di cui al D.L.G.S. 231 del 09/10/02.<br /><br />
				Compilatore Fattura______________________ Verifica Package________________________' :
				'').'
			</div>
			
			<div style="position:absolute; border:1px solid black; font-size:10px; '.($tipo == 'DDT' ? ' bottom:20px' : 'bottom:45px').'; left:550px; height:162px; width:170px;">
				<div style="height:54px; border-bottom:1px solid black; text-align:center">
				Firma del conducente
				</div>
				<div style="height:54px; border-bottom:1px solid black; text-align:center">
				Firma del destinatario
				</div>
				<div style="height:54px; text-align:center">
				Firma del vettore
				</div>
			</div>
			</div></page>
		'; 
			
		if($numero_righe > 13) {
			
			$content .= '<page><div id="contenitore" style="width:770px; height:1070px; font-family: Arial, Verdana, Sans-Serif; display:block; position:relative; margin:0 auto">
				<table style="width:720px; font-size:10px; display:block; margin:0 auto">
				<tr>
				<td style="width:485px">
				<img src="https://www.ezdirect.it/img/logo-ezdirect-it.jpg" alt="Ezdirect"  title="Ezdirect"  style="width:200px; display:block; margin:0 auto" /><br /><br />
				</td>
				<td style="width:235px">
				<h1 style="text-align:left; font-size:15px; margin-top:5px; margin-bottom:5px; font-weight:normal; color:#00009a">
					'.($tipo == 'Fattura' ? 'Fattura Accompagnatoria' :  ($tipo == 'Nota di credito' ? 'Nota di credito' : 'Documento di Trasporto (D.D.T.)')).'
				</h1>
				Codice Cliente <strong>'.$id_customer.'</strong>
				</td>
				</tr>
				</table>
				<table style="width:720px; font-size:10px; display:block; margin:0 auto">
					<tr>
					<td style="width:240px">
					<strong style="font-size:12px">ezdirect srl a socio unico</strong><br />
					Via Nerino Garbuio snc - 54038 Montignoso (MS)<br />
					Tel: 0585 821163 - Fax: 0240708331<br />
					info@ezdirect.it - www.ezdirect.it
					</td>
					<td style="width:240px">
					<br />
					P.IVA-C.F. 01164670455 REA:118272 04-05-07<br />
					REGISTRO A.E.E. IT14020000008270<br />
					Ai fini durc: Inail 18183206 Inps 4602927808
					</td>
					<td style="width:240px">
					Banca d\'appoggio:
					Banco Popolare CR Lucca Pisa Livorno<br />
					ABI: 05034 CAB: 69948 CIN:T<br />
					C/C 001454 SWIFT:BAPPIT21S44<br />
					IBAN: IT58T0503469948000000001454
					</td>
					</tr>
				</table>

				<table style="width:690px; border-top:1px solid #f86000; border-bottom:1px solid black; display:block; margin:0 auto">
					
					<tr style="font-size:9px">
					<td style="width:123px"><em>PAGAMENTO</em></td>
					<td style="width:247px"><em>DESCRIZIONE PAGAMENTO</em></td>
					<td style="width:70px"><em></em></td>
					<td style="width:125px"><em>'.($tipo == 'Fattura' ? 'FATTURA N.' :  ($tipo == 'Nota di credito' ? 'NOTA DI CREDITO N.' : 'DDT N.')).'</em></td>
					<td style="width:125px"><em>DATA '.($tipo == 'Fattura' ? 'FATTURA' :  ($tipo == 'Nota di credito' ? 'NOTA DI CREDITO' : 'DDT')).'</em></td>
					</tr>
					<tr style="font-size:12px; font-weight:bold">
					<td style="width:123px"></td>
					<td style="width:247px">'.$datiFattura['pagamento'].'</td>
					<td style="width:70px"></td>
					<td style="width:125px">'.$numero_documento[0].($numero_documento[1] != '' ? '/'.date('Y',strtotime($datafattura_raw)) : '').'</td>
					<td style="width:125px">'.($tipo == 'Fattura' || $tipo == 'Nota di credito' ? $datafattura : date('d/m/Y')).'</td>
					</tr>
				</table>

				<table style="width:720px; border-bottom:1px solid black; display:block; margin:0 auto">
					<tr style="font-size:9px">
					<td style="width:57px"><em></em></td>
					<td style="width:57px"><em></em></td>
					<td style="width:239px"><em>VOSTRA BANCA DI APPOGGIO</em></td>
					<td style="width:212px"><em></em></td>
					<td style="width:155px"><em></em></td>
					</tr>
					<tr style="font-size:12px; font-weight:bold">
					<td style="width:57px"><br /></td>
					<td style="width:57px"></td>
					<td style="width:239px"></td>
					<td style="width:212px"></td>
					<td style="width:155px"></td>
					</tr>
				</table>
				<br /><br />
				
				<table style="width:720px; border-top:1px solid #f86000; background-color:#ffedd9; display:block; margin:0 auto">
					<tr style="font-size:10px; height:22px; width:690px; background-color:#ffedd9; border-bottom:1px solid #f86000; color:#000; font-weight:bold">
					<td style="width:100px">Articolo</td>
					<td style="width:276px">Descrizione / Annotazioni</td>
					<td style="width:13px">UM</td>
					<td style="width:58px; text-align:right">Prezzo</td>
					<td style="width:58px; text-align:right">Quantità</td>
					<td style="width:58px; text-align:right">% Sc.</td>
					<td style="width:68px; text-align:right">Importo</td>
					<td style="width:62px; text-align:right">Iva</td>
					</tr>
				</table>
				<table style="width:720px; border-top:1px solid #f86000; display:block; margin:0 auto">
			';
				
			$prodottiFattura = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_.($tipo == 'Nota di credito' ? 'note_di_credito' : 'fattura')." WHERE id_fattura = '$id_fattura' AND id_customer = $id_customer ORDER BY num_riga");
			$num_prod_2 = 0;

			foreach($prodottiFattura as $rigaFattura) {
			
				$rif_ordine_rig_2 = 0;
				$rif_ddt_2 = 0;
				$num_prod_2++;
			
				if($num_prod_2 < 14) {

				}
				else {
					if(empty($rigaFattura['desc_articolo']))  {
				
					}
					else {						
						if(empty($rigaFattura['desc_articolo']))  {
					
						}
						else {
							
							if($rigaFattura['rif_ordine'] != $rif_ordine_rig_2)
							{
								$rif_ordine_rig_2 = $rigaFattura['rif_ordine'];
								
								if($rif_ordine_rig_2 != 0)
								{
									$content .= '
									<tr style="font-size:10px;">
									<td style="width:100px"></td>
									<td style="width:276px"><strong>Rif. Vs. Ordine n. '.$rif_ordine_rig_2.' del '.date('d/m/Y', strtotime(Db::getInstance()->getValue('SELECT date_add FROM '._DB_PREFIX_.'orders WHERE id_order = '.$rif_ordine_rig_2))).'</strong></td>
									<td style="width:13px;"></td>
									<td style="width:48px; text-align:right"></td><td style="width:50px; text-align:right"></td><td style="width:58px; text-align:right"></td><td style="width:68px; text-align:right"></td><td style="width:62px; text-align:right"></td>
									</tr>';
								}
								
							}	
							
							if($rigaFattura['ddt'] != $rif_ddt_2 && $rigaFattura['ddt'] > 0)
							{
								$rif_ddt_2 = $rigaFattura['ddt'];
								if($rif_ddt_2 != 0)
								{
									$content .= '
									<tr style="font-size:10px;">
									<td style="width:100px"></td>
									<td style="width:276px"><strong>Rif. Documento di trasporto n. '.$rigaFattura['ddt'].' del '.date('d/m/Y', strtotime($rigaFattura['data_ddt'])).'</strong></td>
									<td style="width:13px;"></td>
									<td style="width:48px; text-align:right"></td><td style="width:50px; text-align:right"></td><td style="width:58px; text-align:right"></td><td style="width:68px; text-align:right"></td><td style="width:62px; text-align:right"></td>
									</tr>';
								}	
							}
						
						}

						$content .= '
							<tr style="font-size:10px;">
								<td style="width:100px">'.$rigaFattura['cod_articolo'].'</td>
								<td style="width:276px">'.htmlentities($rigaFattura['desc_articolo']).'</td>
								<td style="width:13px;">'.($rigaFattura['qt_sped'] == 0 ? '' : 'N.').'</td>
								<td style="width:48px; text-align:right">'.($rigaFattura['qt_sped'] == 0 ? '' : $rigaFattura['qt_sped']).'</td>
								<td style="width:50px; text-align:right">'.($rigaFattura['qt_sped'] == 0 ? '' : number_format($rigaFattura['prezzo'],2,',','')).'</td>
								<td style="width:58px; text-align:right">'.($rigaFattura['sconto1'] != 0 ? number_format($rigaFattura['sconto1'],2,',','') : '').'</td>
								<td style="width:68px; text-align:right">'.($rigaFattura['qt_sped'] == 0 ? '' : number_format($rigaFattura['importo_netto'],2,',','')).'</td>
								<td style="width:62px; text-align:right">'.($rigaFattura['qt_sped'] == 0 ? '' : (int)$rigaFattura['iva_percent']).'</td>
								</tr>
						';
					}
				}
			}
				
			$content .= '</table>
					<div style="display:block; margin:0 auto; position:absolute; bottom:240px">
						<table style="width:720px; text-align:right; border-top:1px solid black; border-bottom:1px solid black; display:block; margin:0 auto;">
							<tr style="font-size:9px">
							<td style="width:130px"><em>SPESE BOLLO</em></td>
								<td style="width:100px"><em>SPESE INCASSO</em></td>
								<td style="width:100px"><em>SPESE TRASPORTO</em></td>
								<td style="width:100px"><em>SCONTI</em></td>
								<td style="width:100px"><em>SCONTO PAGAMENTO</em></td>
								<td style="width:160px"><em>IMPORTO SCONTATO</em></td>
							</tr>
							<tr style="font-size:11px; font-weight:bold">
							<td style="width:160px"></td>
							<td style="width:100px"></td>
							<td style="width:100px"></td>
							<td style="width:100px"></td>
							<td style="width:100px"></td>
							<td style="width:160px">'.number_format($datiFattura['imponibile'],2,',','').'</td>
							</tr>
						</table>
						<table style="width:720px; text-align:right; border-bottom:1px solid black; display:block; margin:0 auto; ">
							<tr style="font-size:9px">
							<td style="width:145px; text-align:left"><em>COD.IVA</em></td>
							<td style="width:200px"><em>% IVA - DESCRIZIONE IVA</em></td>
							<td style="width:110px"><em>IMPONIBILE IVA</em></td>
							<td style="width:110px"><em>IMPORTO IVA</em></td>
							<td style="width:160px"><em>TOTALE IMPONIBILE</em></td>
							</tr>
							<tr style="font-size:11px; font-weight:bold">
							<td style="width:145px; text-align:left">'.(int)$datiFattura['iva_percent'].'</td>
							<td style="width:200px">'.$desc_iva.'</td>
							<td style="width:110px">'.number_format($datiFattura['imponibile'],2,',','').'</td>
							<td style="width:110px">'.number_format($datiFattura['iva'],2,',','').'</td>
							<td style="width:160px">'.number_format($datiFattura['imponibile'],2,',','').'<br /><br />
							<span style="font-size:9px; font-weight:normal"><em>TOTALE IVA</em></span><br />
							'.number_format($datiFattura['iva'],2,',','').'
							</td>
							</tr>
						</table>
							
						<table style="width:720px; text-align:right; display:block; margin:0 auto; ">
								<tr style="font-size:9px">
								<td '.($tax_regime == 4 ? 'colspan="2" style="width:500px; text-align:left"' : 'style="width:220px; text-align:left"').'>'.($tax_regime == 4 ? 'Fattura emessa in regime di scissione dei pagamenti ex art. 17-ter DPR 633/72<br /><br /><em>Scadenza</em><br />'.$datafattura.'<br /><em>Importo</em><br />'.number_format($datiFattura['imponibile'],2,',','') : '<em>A VISTA</em>').'</td>
								'.($tax_regime == 4 ? '' : '<td style="width:280px"><em>SCADENZE ED IMPORTI</em></td>').'
								<td style="width:235px"><em>TOTALE DOCUMENTO</em></td>
								</tr>
								<tr style="font-size:11px; font-weight:bold">
								<td style="width:220px; text-align:left">'.($numero_righe > 13 ? '' : ($tax_regime == 4 ? '' : number_format($datiFattura['totale_fattura'],2,',',''))).'</td>
								<td style="width:280px"></td>
								<td style="width:235px">'.($numero_righe > 13 ? '*SEGUE*' : number_format($datiFattura['totale_fattura'],2,',','')).'</td>
								</tr>
							</table>
					</div>

					<div style="position:absolute; border:1px solid black; font-size:10px; bottom:80px; left:0px; height:130px; width:540px;">
						<table style="width:480px; font-size:10px">
						<tr>
						<td style="width:260px">
						Causale del trasporto
						</td>
						<td style="width:120px">
						Porto
						</td>
						<td style="width:50px">
						N. Colli
						</td>
						<td style="width:50px">
						Peso
						</td>
						</tr>
						<tr>
						<td style="width:260px; font-size:12px">
						'.$datiFattura['causale_trasporto'].'
						</td>
						<td style="width:120px; font-size:12px">
						'.$datiFattura['desc_porto'].'
						</td>
						<td style="width:50px; font-size:12px">
						'.$datiFattura['num_colli'].'
						</td>
						<td style="width:50px; font-size:12px">
						'.number_format($datiFattura['peso'],2,',','').'
						</td>
						
						</tr>
						<tr>
						<td style="width:260px">
						Trasporto a cura
						</td>
						<td style="width:120px" colspan="2">
						Data inizio trasporto
						</td>
						<td style="width:100px">
						Data e ora del ritiro
						</td>
						</tr>
						<tr>
						<td style="width:260px; font-size:12px">
						'.$datiFattura['trasp_mezzo'].'
						</td>
						<td style="width:120px; font-size:12px" colspan="2">
						
						</td>
						<td style="width:100px; font-size:12px">
						'.$datafattura.'
						</td>
						
						</tr>
							<tr>
						<td style="width:380px"  colspan="3">
						Vettore
						</td>
						<td style="width:100px">
						
						</td>
						</tr>
						<tr>
						<td style="width:380px; font-size:12px" colspan="3">
						'.$datiFattura['dati_vettore'].'
						</td>
						<td style="width:100px; font-size:12px">
						
						</td>
						
						
						</tr>
						</table>
					
					
					</div>
					
					<div style="position:absolute; font-size:9px; bottom:30px; left:0px; width:540px; '.($tipo == 'DDT' ? 'height:162px' : '').'">
					
						'.($tipo == 'Fattura' ? '
						I reclami devono essere inoltrati entro 8 gg dal ricevimento della merce.<br />
						In caso di ritardato pagamento saranno addebitati gli interessi di mora di cui al D.L.G.S. 231 del 09/10/02.<br /><br />
						Compilatore Fattura______________________ Verifica Package________________________' :
						'').'
					</div>
					
					<div style="position:absolute; border:1px solid black; font-size:10px; bottom:45px; left:580px; height:162px; width:170px;">
						<div style="height:54px; border-bottom:1px solid black; text-align:center">
						Firma del conducente
						</div>
						<div style="height:54px; border-bottom:1px solid black; text-align:center">
						Firma del destinatario
						</div>
						<div style="height:54px; text-align:center">
						Firma del vettore
						</div>
					</div>
				</div>
				</page>
			';
		}
		
		if($tipo == 'DDT')
			$content .= $content.$content;
			
		require_once('html2pdf/html2pdf.class.php');
		$html2pdf = new HTML2PDF('P','A4','it');
		$html2pdf->WriteHTML($content);
		
		if($save == 'yes')
			return $html2pdf->Output('', true); 

		if($tipo == 'Fattura')
			$html2pdf->Output($id_customer.'-fattura-'.$id_fattura.'.pdf');
		else if($tipo == 'Nota di credito')
			$html2pdf->Output($id_customer.'-nota-'.$id_fattura.'.pdf');
		else if($tipo == 'ff')
			$html2pdf->Output($id_customer.'-ff-'.$id_fattura.'.pdf');
		else
			$html2pdf->Output($id_customer.'-ddt-'.$id_fattura.'.pdf');
	}
}
