<?php

/* Estende defines.inc.php */

if (defined('_PS_BO_ALL_THEMES_DIR_')) {
    // Percorso del tema Back-Office (BO) di default
    define('_PS_BO_DEFAULT_THEME_DIR_', _PS_BO_ALL_THEMES_DIR_.'default/');
}

if (defined('_PS_BO_DEFAULT_THEME_DIR_')) {
    // Percorso della cartella JS del tema BO di default
    define('_PS_BO_DEFAULT_THEME_JS_DIR_', _PS_BO_DEFAULT_THEME_DIR_.'js/');
    // Percorso della cartella CSS del tema BO di default
    define('_PS_BO_DEFAULT_THEME_CSS_DIR_', _PS_BO_DEFAULT_THEME_DIR_.'css/');
}

// Percorso della cartella Docs
define('_PS_DOCS_DIR_', _PS_ROOT_DIR_.'/docs/');
