<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14007 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
header('Access-Control-Allow-Origin: https://185.81.4.51'); 
include(dirname(__FILE__).'/config/config.inc.php'); 
include(dirname(__FILE__).'init.php');
global $cookie;


if(Tools::getIsset('suggerisciIndirizzo'))
{

	Address::suggestAddress(Tools::getValue('indirizzo'), Tools::getValue('cap'), Tools::getValue('citta'), Tools::getValue('campo'));
}

if(Tools::getIsset('cercacitta'))
{
	if($_POST['cap'] == '')
		die();
	
	$cittas = Db::getInstance()->executeS('SELECT * FROM cap WHERE cap = "'.$_POST['cap'].'"');

	if(count($cittas) == 0)
	{
		$cittas = Db::getInstance()->executeS('SELECT * FROM cap WHERE cap LIKE "'.substr($_POST['cap'],0,4).'x%"');
	}

	if(count($cittas) == 0)
	{
		$cittas = Db::getInstance()->executeS('SELECT * FROM cap WHERE cap LIKE "'.substr($_POST['cap'],0,3).'xx%"');
	}

	$options_citta = '';

	foreach($cittas as $citta)
		$options_citta .= '<option rel="'.$citta['cod_istat'].'" name="'.$citta['comune'].'">'.$citta['comune'].'</option>';
		
	die($options_citta);
}

if(Tools::getIsset('cercaprovincia'))
{
	if($_POST['citta'] == '')
		die();
	
	$provincia_cod = Db::getInstance()->getValue('SELECT cod_provincia FROM comuni WHERE cod_istat = "'.$_POST['citta'].'"');
	$provincia = Db::getInstance()->getRow('SELECT * FROM province WHERE cod_provincia = "'.$provincia_cod.'"');
	
	$id_state = Db::getInstance()->getValue('SELECT id_state FROM state WHERE id_country = 10 AND iso_code = "'.$provincia['sigla'].'"');
	
	die($provincia['provincia'].':::'.$id_state);
}
?>