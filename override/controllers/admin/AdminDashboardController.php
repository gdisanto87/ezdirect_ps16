<?php

// Nella 1.4 si chiamava AdminHome
class AdminDashboardController extends AdminDashboardControllerCore
{
    public function renderView()
    {
        $is_agente = ($this->context->employee->id_profile == 7 ? true : false);

        // Gli agenti non possono vedere le statistiche della dashboard
        if($is_agente) 
        {
            $this->tpl_view_vars = array(
                'is_agente' => true,
            );
        }
        else
        {
            if (Tools::isSubmit('profitability_conf')) {
                return parent::renderOptions();
            }

            // $translations = array(
            // 	'Calendar' => $this->l('Calendar', 'AdminStatsTab'),
            // 	'Day' => $this->l('Day', 'AdminStatsTab'),
            // 	'Month' => $this->l('Month', 'AdminStatsTab'),
            // 	'Year' => $this->l('Year', 'AdminStatsTab'),
            // 	'From' => $this->l('From:', 'AdminStatsTab'),
            // 	'To' => $this->l('To:', 'AdminStatsTab'),
            // 	'Save' => $this->l('Save', 'AdminStatsTab')
            // );

            $test_stats_date_update = $this->context->cookie->__get('stats_date_update');
            if (!empty($test_stats_date_update) && $this->context->cookie->__get('stats_date_update') < strtotime(date('Y-m-d'))) {
                switch ($this->context->employee->preselect_date_range) {
                    case 'day':
                        $date_from = date('Y-m-d');
                        $date_to = date('Y-m-d');
                        break;
                    case 'prev-day':
                        $date_from = date('Y-m-d', strtotime('-1 day'));
                        $date_to = date('Y-m-d', strtotime('-1 day'));
                        break;
                    case 'month':
                    default:
                        $date_from = date('Y-m-01');
                        $date_to = date('Y-m-d');
                        break;
                    case 'prev-month':
                        $date_from = date('Y-m-01', strtotime('-1 month'));
                        $date_to = date('Y-m-t', strtotime('-1 month'));
                        break;
                    case 'year':
                        $date_from = date('Y-01-01');
                        $date_to = date('Y-m-d');
                        break;
                    case 'prev-year':
                        $date_from = date('Y-m-01', strtotime('-1 year'));
                        $date_to = date('Y-12-t', strtotime('-1 year'));
                        break;
                }
                $this->context->employee->stats_date_from = $date_from;
                $this->context->employee->stats_date_to = $date_to;
                $this->context->employee->update();
                $this->context->cookie->__set('stats_date_update', strtotime(date('Y-m-d')));
                $this->context->cookie->write();
            }

            $calendar_helper = new HelperCalendar();

            $calendar_helper->setDateFrom(Tools::getValue('date_from', $this->context->employee->stats_date_from));
            $calendar_helper->setDateTo(Tools::getValue('date_to', $this->context->employee->stats_date_to));

            $stats_compare_from = $this->context->employee->stats_compare_from;
            $stats_compare_to = $this->context->employee->stats_compare_to;

            if (is_null($stats_compare_from) || $stats_compare_from == '0000-00-00') {
                $stats_compare_from = null;
            }

            if (is_null($stats_compare_to) || $stats_compare_to == '0000-00-00') {
                $stats_compare_to = null;
            }

            $calendar_helper->setCompareDateFrom($stats_compare_from);
            $calendar_helper->setCompareDateTo($stats_compare_to);
            $calendar_helper->setCompareOption(Tools::getValue('compare_date_option', $this->context->employee->stats_compare_option));

            $params = array(
                'date_from' => $this->context->employee->stats_date_from,
                'date_to' => $this->context->employee->stats_date_to
            );

            /* Override */

            // Abilitati a vedere le statistiche: Ezio, Federico, Matteo, Carolina
            $employee_stats = ($this->context->employee->id == 1 || $this->context->employee->id == 6 || $this->context->employee->id == 7 || $this->context->employee->id == 22 || $this->context->employee->id == 28 ? true : false);

            // Selezione periodo per grafici e statistiche in breve

            if($this->context->cookie->homeStats == '')
            {
                $homeStats = array();
                $homeStats['period'] = date('M');
                $homeStats['year'] = date('Y');
                $homeStats = serialize($homeStats);
                $this->context->cookie->homeStats = $homeStats;
            }
            
            // Tasto Vai
            if(Tools::getIsset('vai-home'))
            {
                $homeStats = array();
                $homeStats['period'] = Tools::getValue('select-month');
                $homeStats['year'] = Tools::getValue('select-year');
                $homeStats = serialize($homeStats);
                $this->context->cookie->homeStats = $homeStats;
            }	
            
            // Tasto Resetta
            if(Tools::getIsset('reset-home'))
            {
                $homeStats = array();
                $homeStats['period'] = date('M');
                $homeStats['year'] = date('Y');
                $homeStats = serialize($homeStats);
                $this->context->cookie->homeStats = $homeStats;
            }	
            
            $homeStats = unserialize($this->context->cookie->homeStats);

            // Federico, Carolina
            if($this->context->employee->id == 6 || $this->context->employee->id == 22)
            {
                // Correggere: manca il value in ps_configuration
                $modalita_esolver = Configuration::get('MODALITA_ESOLVER');
                
                if(Tools::getIsset('modalita'))
                {
                    if(Tools::getValue('modalita') == 1)
                        Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'configuration SET value = 0 WHERE name = "MODALITA_ESOLVER"');
                    else
                    {
                        Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'configuration SET value = 1 WHERE name = "MODALITA_ESOLVER"');
                        Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'product SET qt_esolver = stock_quantity WHERE 1');
                        Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'product SET ord_esolver = ordinato_quantity WHERE 1');
                    }
                    header("Refresh:0");
                }

                // Correggere, inserire nel TPL:
                /*
                <form method="post" id="form_cambia_modalita" action="https://www.ezdirect.it/ezadmin/index.php?tab=AdminHome&token='.$this->token.'&conf=3">
                    <input name="modalita" type="hidden" value="'.$modalita_esolver.'" />
                    Il sito &egrave; attualmente configurato in modalit&agrave; '.($modalita_esolver == 1 ? 'eSolver' : 'CRM').' per il carico/scarico magazzino. <input type="button" class="button" id="cambia_modalita" name="cambia_modalita" value="Passa alla modalit&agrave; '.($modalita_esolver == 1 ? 'CRM' : 'eSolver').'" /><br /><br />
                </form>
                
                // Scaricare sweetalert e inserirlo con addJS oppure usare TaTa.js (già installato, da inserire con addJS)
                <script type="text/javascript" src="https://www.ezdirect.it/js/sweetalert2.js"></script>
                <link rel="stylesheet" href="https://www.ezdirect.it/css/sweetalert2.css" />
                <script type="text/javascript">
                    $(document).ready(function(){
                        $("#cambia_modalita").on("click",function(e){
                            var valid = false;
                            swal({
                            title: 'Sei sicuro/a?',
                            text: 'Questa operazione cambia la modalità di carico/scarico magazzino',
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#6A9944',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'S&igrave;, voglio continuare!'
                            }).then(function () {
                                $("#form_cambia_modalita").submit();
                                console.log("OK");
                            });
                            
                        });
                    });
                    </script>
                */
            }

            // Gli impiegati abilitati vedono le statistiche
            if($employee_stats)
            {
                $mese_attuale = date('M');

                switch($homeStats['period']) {
                    case 'Jan': $mese = 'Gennaio'; $mese_inizio = '01'; $mese_fine = '01'; $td = cal_days_in_month(CAL_GREGORIAN, 1, $homeStats['year']); break;		
                    case 'Feb': $mese = 'Febbraio'; $mese_inizio = '02'; $mese_fine = '02'; $td = cal_days_in_month(CAL_GREGORIAN, 2, $homeStats['year']); break;
                    case 'Mar': $mese = 'Marzo'; $mese_inizio = '03'; $mese_fine = '03'; $td = cal_days_in_month(CAL_GREGORIAN, 3, $homeStats['year']); break;
                    case 'Apr': $mese = 'Aprile'; $mese_inizio = '04'; $mese_fine = '04'; $td = cal_days_in_month(CAL_GREGORIAN, 4, $homeStats['year']); break;
                    case 'May': $mese = 'Maggio'; $mese_inizio = '05'; $mese_fine = '05'; $td = cal_days_in_month(CAL_GREGORIAN, 5, $homeStats['year']); break;
                    case 'Jun': $mese = 'Giugno'; $mese_inizio = '06'; $mese_fine = '06'; $td = cal_days_in_month(CAL_GREGORIAN, 6, $homeStats['year']); break;
                    case 'Jul': $mese = 'Luglio'; $mese_inizio = '07'; $mese_fine = '07'; $td = cal_days_in_month(CAL_GREGORIAN, 7, $homeStats['year']); break;
                    case 'Aug': $mese = 'Agosto'; $mese_inizio = '08'; $mese_fine = '08'; $td = cal_days_in_month(CAL_GREGORIAN, 8, $homeStats['year']); break;
                    case 'Sep': $mese = 'Settembre'; $mese_inizio = '09'; $mese_fine = '09'; $td = cal_days_in_month(CAL_GREGORIAN, 9, $homeStats['year']); break;
                    case 'Oct': $mese = 'Ottobre'; $mese_inizio = '10'; $mese_fine = '10'; $td = cal_days_in_month(CAL_GREGORIAN, 10, $homeStats['year']); break;
                    case 'Nov': $mese = 'Novembre'; $mese_inizio = '11'; $mese_fine = '11'; $td = cal_days_in_month(CAL_GREGORIAN, 11, $homeStats['year']); break;
                    case 'Dec': $mese = 'Dicembre'; $mese_inizio = '12'; $mese_fine = '12'; $td = cal_days_in_month(CAL_GREGORIAN, 12, $homeStats['year']); break;
                    case 'q1': $mese = 'Q1: Gennaio - Marzo'; $mese_inizio = '01'; $mese_fine = '03'; $td = '31'; break;
                    case 'q2': $mese = 'Q2: Aprile - Giugno'; $mese_inizio = '04'; $mese_fine = '06'; $td = '30'; break;
                    case 'q3': $mese = 'Q3: Luglio - Settembre'; $mese_inizio = '07'; $mese_fine = '09'; $td = '30'; break;
                    case 'q4': $mese = 'Q4: Ottobre - Dicembre'; $mese_inizio = '10'; $mese_fine = '12'; $td = '31'; break;
                    case 'anno': $mese = 'Intero anno'; $mese_inizio = '01'; $mese_fine = '12'; $td = '31'; break;
                    default: $mese = $mese_attuale; break;
                }

                $tot_bdl = Db::getInstance()->getValue('
                    SELECT SUM(importo) 
                    FROM '._DB_PREFIX_.'bdl 
                    WHERE importo > 0 
                        AND invio_contabilita > 0 
                        AND rif_ordine = 0 
                        AND date_add BETWEEN "'.date($homeStats['year'].'-'.$mese_inizio.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$mese_fine.'-'.$td).' 23:59:59"
                ');

                /* Statistiche del periodo selezionato - grafici a torta */

                // Categorie

                $cat_month_stats = Db::getInstance()->executeS('
                    SELECT 
                        SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) as orderSum, SUM(od.product_quantity) AS orderQty, cl.name, AVG((od.`product_price` - ((od.product_price * od.reduction_percent) / 100))) as priceAvg,
                        SUM(((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) - ((CASE WHEN (od.wholesale_price = 0 AND od.no_acq = 0) THEN p.wholesale_price ELSE od.wholesale_price END)*od.product_quantity)) as margine,
                        SUM((CASE WHEN (od.wholesale_price = 0 AND od.no_acq = 0) THEN p.wholesale_price ELSE od.wholesale_price END)*od.product_quantity) as tot_acquisti,
                        (CASE WHEN ((SELECT (CASE WHEN (c4.id_category = 0 OR c4.id_category = 1 OR c4.id_category IS NULL) THEN 
                                (CASE WHEN (c3.id_category = 0 OR c3.id_category = 1  OR c3.id_category IS NULL) THEN 
                                    (CASE WHEN (c2.id_category = 0 OR c2.id_category = 1  OR c2.id_category IS NULL) THEN 
                                        c1.id_category 
                                        ELSE c2.id_category END) 
                                ELSE c3.id_category END) 
                            ELSE c4.id_category END) FROM '._DB_PREFIX_.'category AS c1 LEFT JOIN '._DB_PREFIX_.'category AS c2 ON( c1.id_parent = c2.id_category ) LEFT JOIN '._DB_PREFIX_.'category AS c3 ON( c2.id_parent = c3.id_category ) LEFT JOIN '._DB_PREFIX_.'category AS c4 ON( c3.id_parent = c4.id_category ) WHERE c1.id_category = p.id_category_default)) IS NULL THEN p.id_category_default ELSE ((SELECT (CASE WHEN (c4.id_category = 0 OR c4.id_category = 1 OR c4.id_category IS NULL) THEN 
                                (CASE WHEN (c3.id_category = 0 OR c3.id_category = 1  OR c3.id_category IS NULL) THEN 
                                    (CASE WHEN (c2.id_category = 0 OR c2.id_category = 1  OR c2.id_category IS NULL) THEN 
                                        c1.id_category 
                                        ELSE c2.id_category END) 
                                ELSE c3.id_category END) 
                            ELSE c4.id_category END) FROM '._DB_PREFIX_.'category AS c1 LEFT JOIN '._DB_PREFIX_.'category AS c2 ON( c1.id_parent = c2.id_category ) LEFT JOIN '._DB_PREFIX_.'category AS c3 ON( c2.id_parent = c3.id_category ) LEFT JOIN '._DB_PREFIX_.'category AS c4 ON( c3.id_parent = c4.id_category ) WHERE c1.id_category = p.id_category_default)) END) as macro_category
                    FROM `'._DB_PREFIX_.'orders` o
                    LEFT JOIN `'._DB_PREFIX_.'order_detail` od ON o.id_order = od.id_order
                    LEFT JOIN `'._DB_PREFIX_.'product` p ON p.id_product = od.product_id
                    LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (((SELECT (CASE WHEN (c4.id_category = 0 OR c4.id_category = 1 OR c4.id_category IS NULL) THEN 
                            (CASE WHEN (c3.id_category = 0 OR c3.id_category = 1  OR c3.id_category IS NULL) THEN 
                                (CASE WHEN (c2.id_category = 0 OR c2.id_category = 1  OR c2.id_category IS NULL) THEN 
                                    c1.id_category 
                                    ELSE c2.id_category END) 
                            ELSE c3.id_category END) 
                        ELSE c4.id_category END) FROM '._DB_PREFIX_.'category AS c1 LEFT JOIN '._DB_PREFIX_.'category AS c2 ON( c1.id_parent = c2.id_category ) LEFT JOIN '._DB_PREFIX_.'category AS c3 ON( c2.id_parent = c3.id_category ) LEFT JOIN '._DB_PREFIX_.'category AS c4 ON( c3.id_parent = c4.id_category ) WHERE c1.id_category = p.id_category_default)) = cl.id_category AND cl.id_lang = '.(int)($this->context->language->id).')
                    WHERE (o.valid = 1 OR (o.valid = 0 AND ((o.module = "bankwire"  OR o.module = "other_payment"))))
                        AND o.id_order NOT IN (SELECT id_order FROM '._DB_PREFIX_.'order_history WHERE id_order_state = 6 OR id_order_state = 35)
                        AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-'.$mese_inizio.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$mese_fine.'-'.$td).' 23:59:59"
                    GROUP BY macro_category
                    ORDER BY (CASE WHEN macro_category = 119 THEN 4 WHEN macro_category = 249 THEN 3 WHEN macro_category = 248 THEN 2 WHEN macro_category = 240 OR macro_category = 246 THEN 1 ELSE 0 END), orderSum DESC
                ');

                $category_count_chart = 0;
			    $category_data_chart = '';
                $other_category_tot = 0;
			
                foreach ($cat_month_stats as $catrow) 
                {
                    if($category_count_chart < 8) {
                        $category_data_chart .= '["'.$catrow['name'].'", '.number_format($catrow['orderSum'],2,".","").'],';
                        $category_count_chart++;
                    }	
                    else {
                        $other_category_tot += number_format($catrow['orderSum'],2,".","");	
                    }	
                }	
                
                $other_category_tot += $tot_bdl;

                $categorie = array(
                    'category_data_chart' => $category_data_chart,
                    'other_category_tot' => $other_category_tot,
                );

                // Costruttori

                $man_month_stats = Db::getInstance()->executeS('
                    SELECT
                        (CASE WHEN m.id_manufacturer = 105 THEN SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate)+(IFNULL((SELECT SUM(importo) FROM '._DB_PREFIX_.'bdl b WHERE rif_ordine = 0 AND importo > 0 AND invio_contabilita > 0 AND b.`date_add` BETWEEN "'.date($homeStats['year'].'-'.$mese_inizio.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$mese_fine.'-'.$td).' 23:59:59"),0)) ELSE SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) END) as orderSum2,
                        (CASE WHEN m.id_manufacturer = 105 THEN SUM(od.product_quantity)+(SELECT COUNT(*) FROM '._DB_PREFIX_.'bdl b2 WHERE rif_ordine = 0 AND importo > 0 AND invio_contabilita > 0 AND b2.`date_add` BETWEEN "'.date($homeStats['year'].'-'.$mese_inizio.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$mese_fine.'-'.$td).' 23:59:59") ELSE SUM(od.product_quantity) END) as orderQty2,
                        SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) as orderSum, SUM(od.product_quantity) AS orderQty, m.name, AVG((od.`product_price` - ((od.product_price * od.reduction_percent) / 100))) as priceAvg,
                        SUM(((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) - ((CASE WHEN (od.wholesale_price = 0 AND od.no_acq = 0) THEN p.wholesale_price ELSE od.wholesale_price END)*od.product_quantity)) as margine,
                        SUM((CASE WHEN (od.wholesale_price = 0 AND od.no_acq = 0) THEN p.wholesale_price ELSE od.wholesale_price END)*od.product_quantity) as tot_acquisti,
                        m.id_manufacturer as manufacturer
                    FROM `'._DB_PREFIX_.'orders` o
                    LEFT JOIN `'._DB_PREFIX_.'order_detail` od ON o.id_order = od.id_order
                    LEFT JOIN '._DB_PREFIX_.'order_history oh ON oh.id_order_state = 6 AND oh.id_order = o.id_order
                    LEFT JOIN `'._DB_PREFIX_.'product` p ON p.id_product = od.product_id
                    LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (p.id_manufacturer = m.id_manufacturer)
                    WHERE (o.valid = 1 OR (o.valid = 0 AND ((o.module = "bankwire"  OR o.module = "other_payment"))))
                        AND oh.id_order_state IS NULL
                        AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-'.$mese_inizio.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$mese_fine.'-'.$td).' 23:59:59"
                    GROUP BY m.id_manufacturer 
                    ORDER BY orderSum2 DESC
                ');
                
                $manufacturer_count_chart = 0;
                $manufacturer_data_chart = '';
                $other_manufacturer_tot = 0;
                
                foreach ($man_month_stats as $manrow) 
                {
                    if($manufacturer_count_chart < 12) {
                        if($manrow['manufacturer'] == 105)
                            $manufacturer_data_chart .= '["'.$manrow['name'].'", '.(number_format($manrow['orderSum'] + $tot_bdl, 2, ".", "")).'],';
                        else
                            $manufacturer_data_chart .= '["'.$manrow['name'].'", '.number_format($manrow['orderSum'], 2, ".", "").'],';
                        $manufacturer_count_chart++;
                    }	
                    else {
                        if($manrow['manufacturer'] == 105)
                            $other_manufacturer_tot += number_format(($manrow['orderSum'] + $this->t13), 2, ".", ""); // Correggere: $this->t13 = ?
                        else
                            $other_manufacturer_tot += number_format($manrow['orderSum'], 2, ".", "");	
                    }
                }

                $costruttori = array(
                    'manufacturer_data_chart' => $manufacturer_data_chart,
                    'other_manufacturer_tot' => $other_manufacturer_tot,
                );

                $grafici_periodo = array(
                    'categorie' => $categorie,
                    'costruttori' => $costruttori,
                );

                /* Fine Statistiche del periodo selezionato */

                /* Statistiche in breve */

                $target_anno = Db::getInstance()->getValue('SELECT configurazione FROM trgt WHERE descrizione = "Anno" AND anno = "'.date('Y').'"');
                $target_anno = unserialize($target_anno);

                // Su base periodo

                $vendite_mese =  Db::getInstance()->getValue('
                    SELECT SUM(CASE WHEN o.date_add BETWEEN "'.date($homeStats['year'].'-'.$mese_inizio.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$mese_fine.'-'.$td).' 23:59:59" THEN ((od.`product_price` - (CASE WHEN od.reduction_amount > 0 THEN od.reduction_amount ELSE ((od.product_price * od.reduction_percent) / 100) END)) * od.`product_quantity` / o.conversion_rate) ELSE 0 END) as orderSum
                    FROM '._DB_PREFIX_.'orders o
                    LEFT JOIN '._DB_PREFIX_.'order_detail od ON o.id_order = od.id_order
                    WHERE (o.valid = 1 OR (o.valid = 0 AND ((o.module = "bankwire" OR o.module = "other_payment"))))
                        AND o.id_order NOT IN (SELECT id_order FROM '._DB_PREFIX_.'order_history WHERE id_order_state = 6 OR id_order_state = 35)
                        AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-'.$mese_inizio.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$mese_fine.'-'.$td).' 23:59:59"
                ');

                // $vendite_mese += $tot_bdl; // Correggere: perchè i bdl non vengono contati solo qui?
                
                switch($homeStats['period'])
                {
                    case 'Jan': case 'Feb': case 'Mar': $quarter_first_month = '01'; $quarter_last_month = '03'; $target_quarter = 'q1'; break;
                    case 'Apr': case 'May': case 'Jun': $quarter_first_month = '04'; $quarter_last_month = '06'; $target_quarter = 'q2'; break;
                    case 'Jul': case 'Aug': case 'Sep': $quarter_first_month = '07'; $quarter_last_month = '09'; $target_quarter = 'q3'; break;
                    case 'Oct': case 'Nov': case 'Dec': $quarter_first_month = '10'; $quarter_last_month = '12'; $target_quarter = 'q4'; break;
                    case 'q1': case 'q2': case 'q3': case 'q4': $quarter_first_month = $mese_inizio; $quarter_last_month = $mese_fine; $target_quarter = $homeStats['period']; break;
                    case 'anno': $quarter_first_month = '01';  $quarter_last_month = '12'; $target_quarter = 'anno'; break;
                    default: $quarter_first_month = '01'; $quarter_last_month = '03'; $target_quarter = 'q1'; break;
                }
                
                $vendite_quarter = Db::getInstance()->getValue('
                    SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) as total
                    FROM '._DB_PREFIX_.'orders o 
                    JOIN '._DB_PREFIX_.'customer c ON o.id_customer = c.id_customer 
                    LEFT JOIN '._DB_PREFIX_.'order_history oh ON oh.id_order_state = 6 AND oh.id_order = o.id_order
                    LEFT JOIN '._DB_PREFIX_.'order_detail od ON o.id_order = od.id_order 
                    WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire"  OR o.module = "other_payment"))) 
                        AND oh.id_order_state IS NULL 
                        AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-'.$quarter_first_month.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$quarter_last_month.'-'.$td).' 23:59:59"
                ');

                $tot_bdl_quarter = Db::getInstance()->getValue('
                    SELECT SUM(importo) 
                    FROM '._DB_PREFIX_.'bdl 
                    WHERE importo > 0 
                        AND invio_contabilita > 0 
                        AND rif_ordine = 0 
                        AND date_add BETWEEN "'.date($homeStats['year'].'-'.$quarter_first_month.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$quarter_last_month.'-'.$td).' 23:59:59"
                ');

                $vendite_quarter += $tot_bdl_quarter;

                $amazon_mese = Db::getInstance()->getValue('
                    SELECT sum(total_products) 
                    FROM '._DB_PREFIX_.'orders o 
                    JOIN commissioni_marketplace cm ON o.id_order = cm.id_order 
                    WHERE cm.tipo = "Amazon" 
                        AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-'.$mese_inizio.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$mese_fine.'-'.$td).' 23:59:59"
                ');

                $servizi_mese = Db::getInstance()->getValue('
                    SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) as total
                    FROM '._DB_PREFIX_.'orders o 
                    JOIN '._DB_PREFIX_.'customer c ON o.id_customer = c.id_customer 
                    LEFT JOIN '._DB_PREFIX_.'order_history oh ON oh.id_order_state = 6 AND oh.id_order = o.id_order
                    LEFT JOIN '._DB_PREFIX_.'order_detail od ON o.id_order = od.id_order 
                    WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire" OR o.module = "other_payment"))) 
                        AND oh.id_order_state IS NULL 
                        AND (od.category_id = 248 OR od.category_id = 249 OR od.category_id = 119) 
                        AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-'.$mese_inizio.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$mese_fine.'-'.$td).' 23:59:59"
                ');

                $servizi_mese += $tot_bdl;

                $ricavi_periodo = Tools::displayPrice($vendite_mese, $this->context->currency->id);
                $amazon_periodo = Tools::displayPrice($amazon_mese, $this->context->currency->id);
                $servizi_periodo = Tools::displayPrice($servizi_mese, $this->context->currency->id);
                $ordini_periodo = Db::getInstance()->getValue('
                    SELECT COUNT(*) 
                    FROM '._DB_PREFIX_.'orders o 
                    LEFT JOIN '._DB_PREFIX_.'order_history oh ON oh.id_order_state = 6 AND oh.id_order = o.id_order 
                    WHERE (o.valid = 1 OR (o.valid = 0 AND ((o.module = "bankwire"  OR o.module = "other_payment")))) 
                        AND oh.id_order_state IS NULL 
                        AND o.date_add BETWEEN "'.date($homeStats['year'].'-'.$mese_inizio.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$mese_fine.'-'.$td).' 23:59:59"
                ');
                $target_quarter = number_format($target_anno[$target_quarter], 2, ",", ".");
                $target_quarter_p = number_format((($vendite_quarter * 100) / $target_anno[$target_quarter]), 2, ",", "");
                    
                $stat_periodo = array(
                    'ricavi' => $ricavi_periodo,
                    'amazon' => $amazon_periodo,
                    'servizi' => $servizi_periodo,
                    'ordini' => $ordini_periodo,
                    'target' => $target_quarter,
                    'target_p' => $target_quarter_p,
                );

                // Su base anno

                $vendite_anno = Db::getInstance()->getValue('
                    SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) as total
                    FROM '._DB_PREFIX_.'orders o 
                    JOIN '._DB_PREFIX_.'customer c ON o.id_customer = c.id_customer 
                    LEFT JOIN '._DB_PREFIX_.'order_history oh ON oh.id_order_state = 6 AND oh.id_order = o.id_order
                    LEFT JOIN '._DB_PREFIX_.'order_detail od ON o.id_order = od.id_order 
                    WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire" OR o.module = "other_payment"))) 
                        AND oh.id_order_state IS NULL 
                        AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-01-01').' 00:00:00" AND "'.date($homeStats['year'].'-12-31').' 23:59:59"
                ');

                $tot_bdl_anno = Db::getInstance()->getValue('
                    SELECT SUM(importo)
                    FROM '._DB_PREFIX_.'bdl 
                    WHERE importo > 0 
                        AND invio_contabilita > 0 
                        AND rif_ordine = 0
                        AND date_add BETWEEN "'.date($homeStats['year'].'-01-01').' 00:00:00" AND "'.date($homeStats['year'].'-12-31').' 23:59:59"
                ');

                $vendite_anno += $tot_bdl_anno;

                $amazon_anno = Db::getInstance()->getValue('
                    SELECT sum(total_products) 
                    FROM '._DB_PREFIX_.'orders o 
                        JOIN commissioni_marketplace cm ON o.id_order = cm.id_order 
                    WHERE cm.tipo = "Amazon" 
                        AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-01-01').' 00:00:00" AND "'.date($homeStats['year'].'-12-31').' 23:59:59"
                ');

                $servizi_anno = Db::getInstance()->getValue('
                    SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) as total
                    FROM '._DB_PREFIX_.'orders o 
                    JOIN '._DB_PREFIX_.'customer c ON o.id_customer = c.id_customer 
                    LEFT JOIN '._DB_PREFIX_.'order_history oh ON oh.id_order_state = 6 AND oh.id_order = o.id_order
                    LEFT JOIN '._DB_PREFIX_.'order_detail od ON o.id_order = od.id_order 
                    WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire" OR o.module = "other_payment"))) 
                        AND oh.id_order_state IS NULL 
                        AND (od.category_id = 248 OR od.category_id = 249 OR od.category_id = 119) 
                        AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-01-01').' 00:00:00" AND "'.date($homeStats['year'].'-12-31').' 23:59:59"
                ');
                    
                $servizi_anno += $tot_bdl_anno;

                $ricavi_anno = Tools::displayPrice($vendite_anno, $this->context->currency->id);
                $amazon_anno = Tools::displayPrice($amazon_anno, $this->context->currency->id);
                $servizi_anno = Tools::displayPrice($servizi_anno, $this->context->currency->id);
                $ordini_anno = Db::getInstance()->getValue('
                    SELECT COUNT(*) 
                    FROM '._DB_PREFIX_.'orders o 
                    LEFT JOIN '._DB_PREFIX_.'order_history oh ON oh.id_order_state = 6 AND oh.id_order = o.id_order 
                    WHERE (o.valid = 1 OR (o.valid = 0 AND ((o.module = "bankwire"  OR o.module = "other_payment")))) 
                        AND oh.id_order_state IS NULL 
                        AND o.date_add BETWEEN "'.date($homeStats['year'].'-01-01').' 00:00:00" AND "'.date($homeStats['year'].'-12-31').' 23:59:59"
                ');
                $target_anno = number_format($target_anno['totale'], 2, ",", ".");
                $target_anno_p = number_format((($vendite_anno * 100) / $target_anno['totale']), 2, ",", "");

                $stat_anno = array(
                    'ricavi' => $ricavi_anno,
                    'amazon' => $amazon_anno,
                    'servizi' => $servizi_anno,
                    'ordini' => $ordini_anno,
                    'target' => $target_anno,
                    'target_p' => $target_anno_p,
                );

                // Clienti

                // correggere: cambiare la query per mostrare anche le righe con tot = 0?
                $gruppi_aziende = Db::getInstance()->executeS('
                    SELECT cg.id_group as id, gl.name as nome, COUNT(DISTINCT cg.id_customer) as tot
                    FROM '._DB_PREFIX_.'customer_group cg
                    NATURAL JOIN '._DB_PREFIX_.'group_lang gl
                    LEFT JOIN '._DB_PREFIX_.'customer c ON c.id_customer = cg.id_customer
                    WHERE (cg.id_group = 1 OR cg.id_group = 3 OR cg.id_group = 4 OR cg.id_group = 5
                        OR cg.id_group = 8 OR cg.id_group = 15 OR cg.id_group = 16 OR cg.id_group = 17
                        OR cg.id_group = 18 OR cg.id_group = 19 OR cg.id_group = 22)
                        AND gl.id_lang = '.$this->context->language->id.'
                        AND c.deleted = 0
                    GROUP BY cg.id_group
                ');

                foreach($gruppi_aziende as $g)
                    $totale_aziende += $g['tot'];

                // correggere: cambiare la query per mostrare anche le righe con tot = 0?
                $gruppi_privati = Db::getInstance()->executeS('
                    SELECT cg.id_group as id, gl.name as nome, COUNT(DISTINCT cg.id_customer) as tot
                    FROM '._DB_PREFIX_.'customer_group cg
                    NATURAL JOIN '._DB_PREFIX_.'group_lang gl
                    LEFT JOIN '._DB_PREFIX_.'customer c ON c.id_customer = cg.id_customer
                    WHERE cg.id_group = 2
                        AND gl.id_lang = '.$this->context->language->id.'
                        AND c.deleted = 0
                    GROUP BY cg.id_group
                ');

                foreach($gruppi_privati as $g)
                    $totale_privati += $g['tot'];

                // correggere: cambiare la query per mostrare anche le righe con tot = 0?
                $gruppi_altri = Db::getInstance()->executeS('
                    SELECT cg.id_group as id, gl.name as nome, COUNT(DISTINCT cg.id_customer) as tot
                    FROM '._DB_PREFIX_.'customer_group cg
                    NATURAL JOIN '._DB_PREFIX_.'group_lang gl
                    LEFT JOIN '._DB_PREFIX_.'customer c ON c.id_customer = cg.id_customer
                    WHERE (cg.id_group = 11 OR cg.id_group = 12)
                        AND gl.id_lang = '.$this->context->language->id.'
                        AND c.deleted = 0
                    GROUP BY cg.id_group
                ');

                foreach($gruppi_altri as $g)
                    $totale_altri += $g['tot'];

                $totale_clienti = $totale_aziende + $totale_privati + $totale_altri;

                $stat_clienti = array(
                    'gruppi_aziende' => $gruppi_aziende,
                    'gruppi_privati' => $gruppi_privati,
                    'gruppi_altri' => $gruppi_altri,
                    'totale_aziende' => $totale_aziende,
                    'totale_privati' => $totale_privati,
                    'totale_clienti' => $totale_clienti,
                );

                // Efficienza

                $stats_ticket_todo = Db::getInstance()->getRow("
                    SELECT 
                    (SELECT AVG(TIMESTAMPDIFF(MINUTE, date_add, date_upd)/60.0 - 12*(TIMESTAMPDIFF(DAY,'2000-01-03',date_upd)-TIMESTAMPDIFF(DAY,'2000-01-03',date_add)) - 24*(TIMESTAMPDIFF(WEEK, '2000-01-03',date_upd)-TIMESTAMPDIFF(WEEK,'2000-01-03',date_add))) FROM "._DB_PREFIX_."customer_thread WHERE id_contact != 7 AND date_add BETWEEN '".date($homeStats['year'].'-'.$mese_inizio.'-01')." 00:00:00' AND '".date($homeStats['year'].'-'.$mese_fine.'-'.$td)." 23:59:59' AND status = 'closed') as hourstkt,
                    (SELECT AVG(TIMESTAMPDIFF(MINUTE, date_add, date_upd)/60.0 - 12*(TIMESTAMPDIFF(DAY,'2000-01-03',date_upd)-TIMESTAMPDIFF(DAY,'2000-01-03',date_add)) - 24*(TIMESTAMPDIFF(WEEK, '2000-01-03',date_upd)-TIMESTAMPDIFF(WEEK,'2000-01-03',date_add))) FROM action_thread WHERE date_add BETWEEN '".date($homeStats['year'].'-'.$mese_inizio.'-01')." 00:00:00' AND '".date($homeStats['year'].'-'.$mese_fine.'-'.$td)." 23:59:59' AND status = 'closed') as hourstodo,
                    (SELECT AVG(TIMESTAMPDIFF(MINUTE, date_add, date_upd)/60.0 - 12*(TIMESTAMPDIFF(DAY,'2000-01-03',date_upd)-TIMESTAMPDIFF(DAY,'2000-01-03',date_add)) - 24*(TIMESTAMPDIFF(WEEK, '2000-01-03',date_upd)-TIMESTAMPDIFF(WEEK,'2000-01-03',date_add))) FROM form_prevendita_thread WHERE date_add BETWEEN '".date($homeStats['year'].'-'.$mese_inizio.'-01')." 00:00:00' AND '".date($homeStats['year'].'-'.$mese_fine.'-'.$td)." 23:59:59' AND status = 'closed') as hoursprv
                ");

                $hourstkt = number_format($stats_ticket_todo['hourstkt'], 0);
                $hoursprv = number_format($stats_ticket_todo['hoursprv'], 0);
                $hourstodo = number_format($stats_ticket_todo['hourstodo'], 0);

                $stat_efficienza = array(
                    'hourstkt' => $hourstkt,
                    'hoursprv' => $hoursprv,
                    'hourstodo' => $hourstodo,
                );

                // Target anno

                $tutti_i_costruttori = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'manufacturer ORDER BY name ASC');
                $home_manufacturers = unserialize(Db::getInstance()->getValue('SELECT value FROM fixed_ck WHERE id_employee = '.$this->context->employee->id.' AND name = "home_manufacturers"'));
                $hms = array();

                foreach($home_manufacturers as $hm)
                {
                    $hms2['id'] = $hm;

                    $vendite_anno_hm = (Db::getInstance()->getValue('
                        SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) as total
                        FROM '._DB_PREFIX_.'orders o 
                        JOIN '._DB_PREFIX_.'customer c ON o.id_customer = c.id_customer 
                        LEFT JOIN '._DB_PREFIX_.'order_history oh ON oh.id_order_state = 6 AND oh.id_order = o.id_order
                        LEFT JOIN '._DB_PREFIX_.'order_detail od ON o.id_order = od.id_order 
                        WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire" OR o.module = "other_payment"))) AND oh.id_order_state IS NULL AND od.manufacturer_id = '.$hm.' AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-01-01').' 00:00:00" AND "'.date($homeStats['year'].'-12-31').' 23:59:59"
                    '));
                        
                    if($hms2['id'] == 105) {
                        $tot_bdl_anno_hm = Db::getInstance()->getValue('SELECT SUM(importo) FROM '._DB_PREFIX_.'bdl WHERE importo > 0 AND invio_contabilita > 0 AND rif_ordine = 0 AND date_add BETWEEN "'.date($homeStats['year'].'-01-01').' 00:00:00" AND "'.date($homeStats['year'].'-12-31').' 23:59:59"');
                        $vendite_anno_hm += $tot_bdl_anno_hm;
                    }

                    $target_anno_hm = Db::getInstance()->getValue('SELECT configurazione FROM trgt WHERE descrizione = "m-'.$hm.'-'.$homeStats['year'].'" AND anno = "'.$homeStats['year'].'"');
                    $target_anno_hm = unserialize($target_anno_hm);
                    
                    $target_hm = $target_anno_hm['q1'] + $target_anno_hm['q2'] + $target_anno_hm['q3'] + $target_anno_hm['q4'];
                    $hms2['target'] = number_format($target, 2, ",", ".");

                    $target_percent_hm = ($vendite_anno_hm * 100) / ($target_anno_hm['q1'] + $target_anno_hm['q2'] + $target_anno_hm['q3'] + $target_anno_hm['q4']);
                    $hms2['target_percent'] = number_format($target_percent_hm, 2, ",", "");

                    $hms[] = $hms2;
                }

                $stat_target = array(
                    'costruttori' => $tutti_i_costruttori,
                    'home_manufacturers' => $home_manufacturers,
                    'hms' => $hms,
                );

                $stat_in_breve = array(
                    'periodo' => $stat_periodo,
                    'anno' => $stat_anno,
                    'clienti' => $stat_clienti,
                    'efficienza' => $stat_efficienza,
                    'target' => $stat_target,
                );

                /* Fine Statistiche in breve */

                $this->addJS('https://www.gstatic.com/charts/loader.js');

                $this->tpl_view_vars = array(
                    'date_from' => $this->context->employee->stats_date_from,
                    'date_to' => $this->context->employee->stats_date_to,
                    'hookDashboardZoneOne' => Hook::exec('dashboardZoneOne', $params),
                    'hookDashboardZoneTwo' => Hook::exec('dashboardZoneTwo', $params),
                    //'translations' => $translations,
                    'action' => '#',
                    'warning' => $this->getWarningDomainName(),
                    'new_version_url' => Tools::getCurrentUrlProtocolPrefix()._PS_API_DOMAIN_.'/version/check_version.php?v='._PS_VERSION_.'&lang='.$this->context->language->iso_code.'&autoupgrade='.(int)(Module::isInstalled('autoupgrade') && Module::isEnabled('autoupgrade')).'&hosted_mode='.(int)defined('_PS_HOST_MODE_'),
                    'dashboard_use_push' => Configuration::get('PS_DASHBOARD_USE_PUSH'),
                    'calendar' => $calendar_helper->generate(),
                    'PS_DASHBOARD_SIMULATION' => Configuration::get('PS_DASHBOARD_SIMULATION'),
                    'datepickerFrom' => Tools::getValue('datepickerFrom', $this->context->employee->stats_date_from),
                    'datepickerTo' => Tools::getValue('datepickerTo', $this->context->employee->stats_date_to),
                    'preselect_date_range' => Tools::getValue('preselectDateRange', $this->context->employee->preselect_date_range),
                    // Override
                    'mese' => $mese, // Nome del mese o periodo selezionato
                    'anno_attuale' => (int)(date('Y')), // Anno in corso
                    'homeStats' => $homeStats,
                    'grafici_periodo' => $grafici_periodo,
                    'stat_in_breve' => $stat_in_breve,
                    'costruttori' => $tutti_i_costruttori,
                    'is_agente' => false,
                    'employee_stats' => $employee_stats,
                );
            }
            // Gli altri vedono le loro ultime attività
            else
            {
                $ultime_attivita_impiegato_group = Db::getInstance()->executeS('
                    SELECT DISTINCT id_attivita 
                    FROM storico_attivita 
                    WHERE id_employee = '.$this->context->employee->id.' 
                    ORDER BY data_attivita DESC 
                    LIMIT 60
                ');
				
				$ultime_att_gr = '(';
				foreach($ultime_attivita_impiegato_group as $u)
					$ultime_att_gr .= $u['id_attivita'].',';

				$ultime_att_gr .= '9999999999999)';

				$ultime_attivita_impiegato = Db::getInstance()->executeS('
                    SELECT * 
                    FROM storico_attivita 
                    WHERE tipo_attivita != "AE" AND id_attivita IN '.$ultime_att_gr.' 
                    GROUP BY id_attivita 
                    ORDER BY data_attivita DESC
                ');

                // Correggere: bootstrappizzare; la variabile serve solo se usata come return di funzione
                // Correggere: manca la classe paginated
				$content_ua = '<table class="table paginated paginated-20"><tr><th>Tipo attivit&agrave;</th><th>Id attivit&agrave;</th><th>Cliente</th><th>In carico a</th><th>Status</th><th>Data attivit&agrave;</th></tr><tbody>';
				
                $uaii = 0;

				foreach($ultime_attivita_impiegato as $uai)
				{
					if($uai['tipo_attivita'] == 'T')
					{
						$id_contact = Db::getInstance()->getValue('
                            SELECT id_contact 
                            FROM '._DB_PREFIX_.'customer_thread 
                            WHERE id_customer_thread = '.$uai['id_attivita']
                        );

						if($id_contact == 7)
							$uai['tipo_attivita'] = 'M';
					}

                    switch($uai['tipo_attivita'])
                    {
                        case 'A': 
                            $tipo_attivita_uai = 'Azione'; 
                            $table = 'action_thread'; 
                            $attivita_key = 'id_action'; 
                            $id_attivita = 'TODO'.$uai['id_attivita']; 
                            break;
                        case 'P': 
                            $tipo_attivita_uai = 'Richiesta preventivo'; 
                            $table = 'form_prevendita_thread'; 
                            $attivita_key = 'id_thread'; 
                            $id_attivita = Customer::trovaSigla($uai['id_attivita'], 'P'); 
                            break;
                        case 'C': 
                            $tipo_attivita_uai = 'Carrello'; 
                            $table = _DB_PREFIX_.'cart'; 
                            $attivita_key = 'id_cart'; 
                            $id_attivita = $uai['id_attivita']; 
                            break;
                        case 'O': 
                            $tipo_attivita_uai = 'Ordine'; 
                            $table = _DB_PREFIX_.'orders'; 
                            $attivita_key = 'id_order'; 
                            $id_attivita = $uai['id_attivita']; 
                            break;
                        case 'L': 
                            $tipo_attivita_uai = 'BDL'; 
                            $table = _DB_PREFIX_.'bdl'; 
                            $attivita_key = 'id_bdl'; 
                            $id_attivita = 'BDL'.$uai['id_attivita']; 
                            break;
                        case 'T': 
                            $tipo_attivita_uai = 'Ticket'; 
                            $table = _DB_PREFIX_.'customer_thread'; 
                            $attivita_key = 'id_customer_thread'; 
                            $id_attivita = Customer::trovaSigla($uai['id_attivita'], 'T'); 
                            break;	
                        case 'M': 
                            $tipo_attivita_uai = 'Messaggio'; 
                            $table = _DB_PREFIX_.'customer_thread'; 
                            $attivita_key = 'id_customer_thread'; 
                            $id_attivita = Customer::trovaSigla($uai['id_attivita'], 'M'); 
                            break;	
                    }
					
					$id_customer = Db::getInstance()->getValue('
                        SELECT id_customer 
                        FROM '.$table.' 
                        WHERE '.$attivita_key.' = '.$uai['id_attivita']
                    );
					
                    $uai_link = '/ezadmin/index.php?controller=AdminCustomers&id_customer='.$id_customer.'&viewcustomer';
                    $tokenCustomers = Tools::getAdminTokenLite('AdminCustomers');
                    $tokenCarts = Tools::getAdminTokenLite('AdminCarts');
                    $tokenOrders = Tools::getAdminTokenLite('AdminOrders');
                    
                    $uai_employee_act = Db::getInstance()->getValue('
                        SELECT CONCAT(firstname," ",LEFT(lastname,1),".") impiegato 
                        FROM '._DB_PREFIX_.'employee 
                        WHERE id_employee = '.$uai['id_employee']
                    );

                    switch($uai['tipo_attivita'])
                    {
                        case 'A': 
                            $uai_link .= '&tab_name=todo&azione=todo&viewaction&id_action='.$uai['id_attivita'].'&token='.$tokenCustomers; 
                            $uai_status = Db::getInstance()->getValue('SELECT status FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                            $in_carico_a = Db::getInstance()->getValue('SELECT action_to FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                            break;
                        case 'P': 
                            $uai_link .= '&tab_name=actions&azione=actions&viewpreventivo&id_thread='.$uai['id_attivita'].'&token='.$tokenCustomers; 
                            $uai_status = Db::getInstance()->getValue('SELECT status FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                            $in_carico_a = Db::getInstance()->getValue('SELECT id_employee FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                            break;
                        case 'C': 
                            $uai_link = '/ezadmin/index.php?controller=AdminCarts&id_cart='.$uai['id_attivita'].'&viewcart&token='.$tokenCarts; 
                            $uai_status = Db::getInstance()->getValue('SELECT visualizzato FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                            $in_carico_a = Db::getInstance()->getValue('SELECT in_carico_a FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                            break;
                        case 'O': 
                            $uai_link = '/ezadmin/index.php?controller=AdminOrders&id_order='.$uai['id_attivita'].'&vieworder&token='.$tokenOrders;
                            $uai_status = Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'order_state_lang WHERE id_lang = '.$this->context->language->id.' AND id_order_state = (SELECT id_order_state FROM '._DB_PREFIX_.'order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `'._DB_PREFIX_.'order_history` moh WHERE moh.`id_order` = '.$uai['id_attivita'].' GROUP BY moh.`id_order`))'); 
                            $in_carico_a = ''; 
                            break;
                        case 'L': 
                            $uai_link .= '&tab_name=bdl&azione=bdl_edit&id_bdl='.$uai['id_attivita'].'&token='.$tokenCustomers; 
                            $uai_status = Db::getInstance()->getValue('SELECT (CASE WHEN a.rif_ordine > 0 THEN "closed" WHEN a.id_bdl < 384 THEN "closed" WHEN a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 0 THEN "pending1" WHEN a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 1 THEN "closed" WHEN a.invio_controllo = 2 AND a.invio_contabilita = 2 THEN "closed" WHEN a.rif_ordine = 0 AND a.invio_controllo = 0 THEN "open" ELSE "open" END) stato FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                            $in_carico_a = Db::getInstance()->getValue('SELECT effettuato_da FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                            break;
                        case 'T': 
                            $uai_link .= '&tab_name=tickets&azione=tickets&viewticket&id_customer_thread='.$uai['id_attivita'].'&token='.$tokenCustomers; 
                            $uai_status = Db::getInstance()->getValue('SELECT status FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                            $in_carico_a = Db::getInstance()->getValue('SELECT id_employee FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                            break;
                        case 'M': 
                            $uai_link .= '&tab_name=actions&azione=actions&viewmessage&id_mex='.$uai['id_attivita'].'&token='.$tokenCustomers; 
                            $uai_status = Db::getInstance()->getValue('SELECT status FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                            $in_carico_a = Db::getInstance()->getValue('SELECT id_employee FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                            break;	
                    }
                
                    $uai_employee = Db::getInstance()->getValue('
                        SELECT CONCAT(firstname," ",LEFT(lastname,1),".") impiegato 
                        FROM '._DB_PREFIX_.'employee 
                        WHERE id_employee = '.$in_carico_a
                    );

                    $uai_status_style = '';
										
                    switch($uai_status)
                    {
                        case 'open': 
                            $uai_status = 'Aperto';
                            $uai_status_style = 'class="badge badge-danger" style="font-weight:bold; background-color:red; border-radius:0px;"';
                            break;
                        case 'closed': 
                            $uai_status = 'Chiuso';
                            $uai_status_style = 'class="badge badge-success" style="font-weight:bold; border-radius:0px;"';
                            break;
                        case 'pending1': 
                            $uai_status = 'In lavorazione';
                            $uai_status_style = 'class="badge badge-warning" style="font-weight:bold; border-radius:0px;"';
                            break;
                        case 'pending2': 
                            $uai_status = 'In lavorazione';
                            $uai_status_style = 'class="badge badge-warning" style="font-weight:bold; border-radius:0px;"';
                            break;
                        case 1:
                            $uai_status = 'Letto';
                            $uai_status_style = 'style="font-weight:bold; color:green;"';
                            // $uai_status_style = '><img src="../img/admin/enabled.gif" alt="Letto" title="Letto"';
                            break;
                        case 0:
                            $uai_status = 'Non letto'; 
                            $uai_status_style = 'style="font-weight:bold; color:red;"';
                            // $uai_status_style = '><img src="../img/admin/red_no.gif" alt="Non letto" title="Non letto"';
                            break;
                        case 999: 
                            $uai_status = 'Carrello Amazon convertito'; 
                            break;
                        default: 
                            $uai_status; 
                            break;
                    }

                    $dati_cliente = Db::getInstance()->getValue('
                        SELECT (CASE WHEN is_company = 1 THEN company ELSE CONCAT(firstname," ",lastname) END) cliente 
                        FROM '._DB_PREFIX_.'customer 
                        WHERE id_customer = '.$id_customer
                    );
                
                    if($uai['tipo_attivita'] == 'T')
                    {
                        $check_resp = 1;

                        $id_contact = Db::getInstance()->getValue('
                            SELECT id_contact 
                            FROM '._DB_PREFIX_.'customer_thread 
                            WHERE id_customer_thread = '.$uai['id_attivita']
                        );

                        if($id_contact == 7)
                        {
                            $tipo_attivita_uai = 'Messaggio';
                            $count_msg = Db::getInstance()->getValue('
                                SELECT count(id_customer_message) 
                                FROM '._DB_PREFIX_.'customer_message 
                                WHERE id_employee = 0 
                                    AND id_customer_thread = '.$uai['id_attivita']
                            );

                            if($count_msg == 0)
                                $check_resp = 0;
                        }	
                    }
                    
                    if($uai['tipo_attivita'] == 'P')
                    {
                        $id_contact = Db::getInstance()->getValue('
                            SELECT tipo_richiesta 
                            FROM form_prevendita_thread 
                            WHERE id_thread = '.$uai['id_attivita']
                        );

                        if($id_contact == 'tirichiamiamonoi')
                            $tipo_attivita_uai = 'Ti richiamiamo noi';
                    }
                    
                    if(substr($uai['desc_attivita'],0,22) == 'Ha salvato il carrello')
                        $desc_attivita = 'Ha lavorato sul carrello';
                    else
                        $desc_attivita = $uai['desc_attivita'];
                    
                    if(is_numeric(substr($desc_attivita, -1)))
                    {	
                        $employee = filter_var($desc_attivita, FILTER_SANITIZE_NUMBER_INT);
                        $name_employee = Db::getInstance()->getValue('
                            SELECT concat(firstname, " ", LEFT(lastname,1),".") 
                            FROM '._DB_PREFIX_.'employee 
                            WHERE id_employee = '.$employee
                        );
                        $desc_attivita = 'Ha messo l\'attivit&agrave; in carico a '.$name_employee;
                    }

                    if($dati_cliente != '')
                    {
                        // Test: tooltip al posto di Espandi dettagli riga
                        // $tooltip_espandi = $uai_employee_act.' '.$desc_attivita;

                        // $content_ua .= '<tr '.($uaii%2 == 0 ? '' : 'style="background-color:#f5f5f5"').' class="riga"><td data-sort="'.$tipo_attivita_uai.'" id="td_espandi_'.$uai['id_attivita'].'"><i class="icon-question-sign pointer" title="'.$tooltip_espandi.'"></i> | '.$tipo_attivita_uai.'';
                        $content_ua .= '<tr '.($uaii%2 == 0 ? '' : 'style="background-color:#f5f5f5"').' class="riga"><td data-sort="'.$tipo_attivita_uai.'" id="td_espandi_'.$uai['id_attivita'].'"><img class="pointer" src="../img/admin/add.gif" alt="Espandi" title="Espandi" data-toggle="collapse" '.($uai['tipo_attivita'] == 'C' || $uai['tipo_attivita'] == 'O' ? 'data-target="#cart_riga_show_'.$uai['id_attivita'].'"' : 'data-target="#dett_att_'.$uai['id_attivita'].'"').' aria-expanded="false" />'.$tipo_attivita_uai.'';
                        
                        // Espandi dettagli riga
                        if($uai['tipo_attivita'] == 'C' || $uai['tipo_attivita'] == 'O')
                        {
                            // Correggere: errore durante l'operazione! risolvere e spostare in un file js
                            $content_ua .= '
                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $("#espandi_'.$uai['id_attivita'].', #td_espandi_'.$uai['id_attivita'].'").click(function(){
                                        
                                        if($(this).attr("src") == ("../img/admin/add.gif")) {
                                            $(this).attr("src", "../img/admin/forbbiden.gif");
                                            $.ajax({
                                                url:"ajax.php?get'.($uai['tipo_attivita'] == 'O' ? 'Order' : 'Cart').'Display=y",
                                                type: "POST",
                                                data: {
                                                id_'.($uai['tipo_attivita'] == 'O' ? 'order' : 'cart').': '.$uai['id_attivita'].'
                                                },
                                                success:function(r){
                                                    $("#cart_riga_show_'.$uai['id_attivita'].'_td").html(r);
                                                    $("#cart_riga_show_'.$uai['id_attivita'].'").show();
                                                },
                                                error: function(xhr,stato,errori){
                                                    alert("Errore durante l\'operazione:"+xhr.status);
                                                }
                                            });
                                        }
                                        else {
                                            $(this).attr("src", "../img/admin/add.gif");
                                            $("#cart_riga_show_'.$uai['id_attivita'].'").hide();
                                        }
                                        return false;
                                    });
                                    
                                });
                            </script>';
                        }
                        else
                        {
                            // Correggere: spostare in un file js
                            /*$content_ua .= '
                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $("#espandi_'.$uai['id_attivita'].', #td_espandi_'.$uai['id_attivita'].'").click(function(){
                                        $("tr.sub_'.$uai['id_attivita'].'").toggle();
                                        if($("tr.sub_'.$uai['id_attivita'].':visible").length) {
                                            $(this).attr("src", "../img/admin/forbbiden.gif");	
                                        }
                                        else {
                                            $(this).attr("src", "../img/admin/add.gif");
                                            $("tr.sub_'.$uai['id_attivita'].'").hide();
                                        }
                                        return false;
                                    });
                                    
                                });
                            </script>';*/
                        }

                        $content_ua .= '</td>';
                        $content_ua .= '<td><a href="'.$uai_link.'">'.$id_attivita.'</a></td><td><a href="'.$uai_link.'">'.$dati_cliente.'</a></td><td><a href="'.$uai_link.'">'.$uai_employee.'</a></td><td><a href="'.$uai_link.'" '.$uai_status_style.'>'.($tipo_attivita_uai == 'Messaggio' && $check_resp == 0 ? '' : $uai_status).'</a></td><td><a href="'.$uai_link.'">'.Tools::displayDate($uai['data_attivita'], $this->context->language->id, true).'</a></td></tr>';
                        
                        if($uai['tipo_attivita'] == 'O' || $uai['tipo_attivita'] == 'C') {
                            // Correggere: Originale 1.4
                            /*$content_ua .= '
                                <tr id="cart_riga_show_'.$uai['id_attivita'].'" class="invisible-table-row subs sub_'.$uai['id_attivita'].'" style="display:none '.($uaii%2 == 0 ? '' : '; background-color:#f5f5f5"').'">
                                    <td id="cart_riga_show_'.$uai['id_attivita'].'_td"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            ';*/

                            // Non compatibile con datatables
                            $content_ua .= '
                                <tr>
                                    <td colspan="6">
                                        <div class="collapse" id="cart_riga_show_'.$uai['id_attivita'].'">
                                            <div class="row" '.($uaii % 2 == 0 ? '' : 'style="background-color:#F5F5F5;"').'>
                                                <div class="col-md-7" id="cart_riga_show_'.$uai['id_attivita'].'_td"></div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-1"></div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            ';
                        }	
                        else {
                            // Non compatibile con datatables
                            $content_ua .= '
                                <tr>
                                    <td colspan="6">
                                        <div class="collapse" id="dett_att_'.$uai['id_attivita'].'">
                                            <div class="row" '.($uaii % 2 == 0 ? '' : 'style="background-color:#F5F5F5;"').'>
                                                <div class="col-md-7">'.$uai_employee_act.' '.$desc_attivita.'</div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-1"></div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            ';
                        }
                    }

					$uaii++;
				}

                $content_ua .= '</tbody></table>';

                $this->tpl_view_vars = array(
                    'is_agente' => false,
                    'employee_stats' => $employee_stats,
                    'ultime_attivita' => $content_ua,
                );
            }

            /*$this->addJS('https://www.gstatic.com/charts/loader.js');

            $this->tpl_view_vars = array(
                'date_from' => $this->context->employee->stats_date_from,
                'date_to' => $this->context->employee->stats_date_to,
                'hookDashboardZoneOne' => Hook::exec('dashboardZoneOne', $params),
                'hookDashboardZoneTwo' => Hook::exec('dashboardZoneTwo', $params),
                //'translations' => $translations,
                'action' => '#',
                'warning' => $this->getWarningDomainName(),
                'new_version_url' => Tools::getCurrentUrlProtocolPrefix()._PS_API_DOMAIN_.'/version/check_version.php?v='._PS_VERSION_.'&lang='.$this->context->language->iso_code.'&autoupgrade='.(int)(Module::isInstalled('autoupgrade') && Module::isEnabled('autoupgrade')).'&hosted_mode='.(int)defined('_PS_HOST_MODE_'),
                'dashboard_use_push' => Configuration::get('PS_DASHBOARD_USE_PUSH'),
                'calendar' => $calendar_helper->generate(),
                'PS_DASHBOARD_SIMULATION' => Configuration::get('PS_DASHBOARD_SIMULATION'),
                'datepickerFrom' => Tools::getValue('datepickerFrom', $this->context->employee->stats_date_from),
                'datepickerTo' => Tools::getValue('datepickerTo', $this->context->employee->stats_date_to),
                'preselect_date_range' => Tools::getValue('preselectDateRange', $this->context->employee->preselect_date_range),
                // Override
                'mese' => $mese, // Nome del mese o periodo selezionato
                'anno_attuale' => (int)(date('Y')), // Anno in corso
                'homeStats' => $homeStats,
                'grafici_periodo' => $grafici_periodo,
                'stat_in_breve' => $stat_in_breve,
                'costruttori' => $tutti_i_costruttori,
                'is_agente' => false,
                'employee_stats' => $employee_stats,
                'ultime_attivita' => $content_ua,
            );*/
        }

        return AdminController::renderView();
    }
}