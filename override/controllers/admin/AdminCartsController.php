<?php

class AdminCartsController extends AdminCartsControllerCore
{
    public function renderView()
    {
		$html .= '<h1>*********</h1>';	
		
		$html = $this->viewDetails();
				
        /** @var Cart $cart */
        if (!($cart = $this->loadObject(true))) {
            return;
        }
        $customer = new Customer($cart->id_customer);
        $currency = new Currency($cart->id_currency);
        $this->context->cart = $cart;
        $this->context->currency = $currency;
        $this->context->customer = $customer;
        $this->toolbar_title = sprintf($this->l('Cart #%06d'), $this->context->cart->id);
        $products = $cart->getProductsOld();
        $customized_datas = Product::getAllCustomizedDatas((int)$cart->id);
        Product::addCustomizationPrice($products, $customized_datas);
        $summary = $cart->getSummaryDetails();

        /* Display order information */
        $id_order = (int)Order::getOrderByCartId($cart->id);
        $order = new Order($id_order);
        if (Validate::isLoadedObject($order)) {
            $tax_calculation_method = $order->getTaxCalculationMethod();
            $id_shop = (int)$order->id_shop;
        } else {
            $id_shop = (int)$cart->id_shop;
            $tax_calculation_method = Group::getPriceDisplayMethod(Group::getCurrent()->id);
        }

        if ($tax_calculation_method == PS_TAX_EXC) {
            $total_products = $summary['total_products'];
            $total_discounts = $summary['total_discounts_tax_exc'];
            $total_wrapping = $summary['total_wrapping_tax_exc'];
            $total_price = $summary['total_price_without_tax'];
            $total_shipping = $summary['total_shipping_tax_exc'];
        } else {
            $total_products = $summary['total_products_wt'];
            $total_discounts = $summary['total_discounts'];
            $total_wrapping = $summary['total_wrapping'];
            $total_price = $summary['total_price'];
            $total_shipping = $summary['total_shipping'];
        }
        foreach ($products as $k => &$product) {
            if ($tax_calculation_method == PS_TAX_EXC) {
                $product['product_price'] = $product['price'];
                $product['product_total'] = $product['total'];
            } else {
                $product['product_price'] = $product['price_wt'];
                $product['product_total'] = $product['total_wt'];
            }
            $image = array();
            if (isset($product['id_product_attribute']) && (int)$product['id_product_attribute']) {
                $image = Db::getInstance()->getRow('SELECT id_image FROM '._DB_PREFIX_.'product_attribute_image WHERE id_product_attribute = '.(int)$product['id_product_attribute']);
            }
            if (!isset($image['id_image'])) {
                $image = Db::getInstance()->getRow('SELECT id_image FROM '._DB_PREFIX_.'image WHERE id_product = '.(int)$product['id_product'].' AND cover = 1');
            }

            $product['qty_in_stock'] = StockAvailable::getQuantityAvailableByProduct($product['id_product'], isset($product['id_product_attribute']) ? $product['id_product_attribute'] : null, (int)$id_shop);

            $image_product = new Image($image['id_image']);
            $product['image'] = (isset($image['id_image']) ? ImageManager::thumbnail(_PS_IMG_DIR_.'p/'.$image_product->getExistingImgPath().'.jpg', 'product_mini_'.(int)$product['id_product'].(isset($product['id_product_attribute']) ? '_'.(int)$product['id_product_attribute'] : '').'.jpg', 45, 'jpg') : '--');
        }

        $helper = new HelperKpi();
        $helper->id = 'box-kpi-cart';
        $helper->icon = 'icon-shopping-cart';
        $helper->color = 'color1';
        $helper->title = $this->l('Total Cart', null, null, false);
        $helper->subtitle = sprintf($this->l('Cart #%06d', null, null, false), $cart->id);
        $helper->value = Tools::displayPrice($total_price, $currency);
        $kpi = $helper->generate();

        $this->tpl_view_vars = array(
            'kpi' => $kpi,
            'products' => $products,
            'discounts' => $cart->getCartRules(),
            'order' => $order,
            'cart' => $cart,
            'currency' => $currency,
            'customer' => $customer,
            'customer_stats' => $customer->getStats(),
            'total_products' => $total_products,
            'total_discounts' => $total_discounts,
            'total_wrapping' => $total_wrapping,
            'total_price' => $total_price,
            'total_shipping' => $total_shipping,
            'customized_datas' => $customized_datas,
            'tax_calculation_method' => $tax_calculation_method
        );

        return $html;
    }
	
	public function bindDatepickerZ($id, $time)
	{
		if ($time)
			$html .= '
			var dateObj = new Date();
			var hours = dateObj.getHours();
			var mins = dateObj.getMinutes();
			var secs = dateObj.getSeconds();
			if (hours < 10) { hours = "0" + hours; }
			if (mins < 10) { mins = "0" + mins; }
			if (secs < 10) { secs = "0" + secs; }
			var time = " "+hours+":"+mins+":"+secs;';

		$html .= '
		$(function() {
			$("#'.Tools::htmlentitiesUTF8($id).'").datepicker({
				prevText:"",
				nextText:"",
				dateFormat:"dd-mm-yy"});
		});';
		
		echo $html;
	}

	// id can be a identifier or an array of identifiers
	public function includeDatepickerZ($id, $time = false)
	{
		$context = Context::getContext();

		$html .= '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery-ui-1.8.10.custom.min.js"></script>';
		$iso = Db::getInstance()->getValue('SELECT '._DB_PREFIX_.'iso_code FROM '._DB_PREFIX_.'lang WHERE `id_lang` = '.(int)$context->language->id);
		if ($iso != 'en')
			$html .= '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/datepicker/ui/i18n/ui.datepicker-'.Tools::htmlentitiesUTF8($iso).'.js"></script>';
		$html .= '<script type="text/javascript">';
			if (is_array($id))
				foreach ($id as $id2)
					AdminCarts::bindDatepickerZ($id2, $time);
			else
				AdminCarts::bindDatepickerZ($id, $time);
		$html .= '</script>';
		echo $html;
	}
	
	// Correggere: spostare tutta la construct del parent controller qui in override
	public function __construct()
	{
		/*if(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== false)
			flush();*/

		parent::__construct();

		// Delete dalla list di AdminCarts
		if(isset($_GET['deletecart'])) {
			Db::getInstance()->execute("
				DELETE 
				FROM noleggio 
				WHERE id_cart = ".$_GET['id_cart']."
			");
			
			Db::getInstance()->execute("
				DELETE 
				FROM "._DB_PREFIX_."cart_ezcloud 
				WHERE id_cart = ".$_GET['id_cart']."
			");
		}
		
		if(isset($_GET['convertcart'])) {
		
			$fatturazione = Db::getInstance()->getValue('
				SELECT id_address_invoice 
				FROM '._DB_PREFIX_.'cart 
				WHERE id_cart = '.Tools::getValue('id_cart')
			);

			$spedizione =  Db::getInstance()->getValue('
				SELECT id_address_delivery 
				FROM '._DB_PREFIX_.'cart 
				WHERE id_cart = '.Tools::getValue('id_cart')
			);

			if($fatturazione == 0 || $spedizione == 0) {
				$html .= '<div class="error">ERRORE
				<br />
				'.($fatturazione == 0 ? 'Devi specificare un indirizzo di fatturazione per quest\'ordine<br />' : '').'
				'.($spedizione == 0 ? 'Devi specificare un indirizzo di spedizione per quest\'ordine<br />' : '').'
				</div>';
			}
			else {
				$address_fatturazione = Db::getInstance()->getRow('
					SELECT address1, postcode, city, id_country 
					FROM '._DB_PREFIX_.'address 
					WHERE id_address = '.$fatturazione.'
				');

				$address_spedizione = Db::getInstance()->getRow('
					SELECT address1, postcode, city, id_country 
					FROM '._DB_PREFIX_.'address 
					WHERE id_address = '.$spedizione.'
				');

				if($address_spedizione['id_country'] != 65 && $address_fatturazione['id_country'] != 65 && $address_spedizione['id_country'] != 152 && $address_fatturazione['id_country'] != 152 && (trim($address_fatturazione['address1']) == '' || $address_fatturazione['postcode'] == '' || trim($address_fatturazione['city']) == '' || trim($address_spedizione['address1']) == '' || $address_spedizione['postcode'] == '' || trim($address_spedizione['city']) == '')) {
					$html .= '<div class="error">ERRORE
					<br />
					'.(trim($address_fatturazione['address1']) == '' ? 'Indirizzo di fatturazione non impostato <br />' : '').'
					'.($address_fatturazione['postcode'] == '' ? 'CAP dell\'indirizzo di fatturazione non impostato <br />' : '').'
					'.(trim($address_fatturazione['city']) == '' ? 'Citt&agrave; dell\'indirizzo di fatturazione non impostata <br />' : '').'
					'.(trim($address_fatturazione['address1']) == '' ? 'Indirizzo di spedizione non impostato <br />' : '').'
					'.($address_fatturazione['postcode'] == '' ? 'CAP dell\'indirizzo di spedizione non impostato <br />' : '').'
					'.(trim($address_fatturazione['city']) == '' ? 'Citt&agrave; dell\'indirizzo di spedizione non impostata <br />' : '').'
					</div>';
				}
				else {
					// correggere: manca modulo pss_clearcarts
					include("../modules/pss_clearcarts/pss_clearcarts.php");
					include("../modules/pss_clearcarts/AdminPssClearCarts.php");
					$apcc = new AdminPssClearCarts();
				
					if(Tools::getValue('no_csv') == 'on')
						$no_csv = true; 
					else
						$no_csv = false;
					
					$apcc->orderThisCart(Tools::getValue('id_cart'), $no_csv);
					$order = Db::getInstance()->getValue('
						SELECT id_order 
						FROM '._DB_PREFIX_.'orders 
						WHERE id_cart = '.Tools::getValue('id_cart').'
					');

					$customer = Db::getInstance()->getValue('
						SELECT id_customer 
						FROM '._DB_PREFIX_.'orders 
						WHERE id_cart = '.Tools::getValue('id_cart').'
					');

					$tokenOrders = Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$this->context->employee->id);
					Tools::redirectAdmin('index.php?controller=AdminOrders&id_order='.$order.'&vieworder&token='.$tokenOrders);
				}
			}
		}
		
		// Delete dalla lista di carrelli di un cliente
		if(isset($_GET['deleteccart'])) {
			Db::getInstance()->execute("
				DELETE 
				FROM carrelli_creati 
				WHERE id_cart = ".$_GET['id_cart']."
			");

			Db::getInstance()->execute("
				DELETE 
				FROM carrelli_creati_prodotti 
				WHERE id_cart = ".$_GET['id_cart']."
			");

			Db::getInstance()->execute("
				DELETE 
				FROM "._DB_PREFIX_."cart 
				WHERE id_cart = ".$_GET['id_cart']."
			");

			Db::getInstance()->execute("
				DELETE 
				FROM cart_note
				WHERE id_cart = ".$_GET['id_cart']."
			");

			Db::getInstance()->execute("
				DELETE 
				FROM storico_attivita 
				WHERE tipo_attivita = 'C' 
					AND id_attivita = ".$_GET['id_cart']."
			");

			Db::getInstance()->execute("
				DELETE 
				FROM noleggio 
				WHERE id_cart = ".$_GET['id_cart']."
			");

			Db::getInstance()->execut("
				DELETE 
				FROM "._DB_PREFIX_."cart_product 
				WHERE id_cart = ".$_GET['id_cart']."
			");

			Db::getInstance()->execute("
				DELETE 
				FROM "._DB_PREFIX_."cart_ezcloud 
				WHERE id_cart = ".$_GET['id_cart']."
			");
			
			//mysql_select_db(_DB_REV_NAME_);
			Db::getInstance()->execute("
				DELETE 
				FROM "._DB_PREFIX_."cart_revisioni 
				WHERE id_cart = ".$_GET['id_cart']."
			");

			Db::getInstance()->execute("
				DELETE 
				FROM "._DB_PREFIX_."cart_product_revisioni 
				WHERE id_cart = ".$_GET['id_cart']."
			");

			//mysql_select_db(_DB_NAME_);
			
			Tools::redirectAdmin("index.php?controller=AdminCustomers&id_customer=".$_GET['id_customer']."&viewcustomer&tab_name=carts&token=".Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($this->context->employee->id)));
		}
	}

	public function renderList()
    {
        if (!($this->fields_list && is_array($this->fields_list))) {
            return false;
        }
        $this->getList($this->context->language->id);

        $helper = new HelperList();

        // Empty list is ok
        if (!is_array($this->_list)) {
            $this->displayWarning($this->l('Bad SQL query', 'Helper').'<br />'.htmlspecialchars($this->_list_error));
            return false;
        }

		/* OVERRIDE */

		// Filtri di ricerca per prodotto
		$prodotti = Db::getInstance()->executeS('
			SELECT p.reference as reference, p.id_product as id, pl.name as name
			FROM '._DB_PREFIX_.'product p 
			LEFT JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product = pl.id_product 
			WHERE p.active = 1 
				AND pl.id_lang = '.$this->context->language->id.'
			GROUP BY p.reference, p.id_product
		');

        $this->tpl_list_vars['prodotti'] = $prodotti;

        $this->tpl_list_vars['cercacarrelloprodotto'] = $this->context->cookie->_cercacarrelloprodotto;
        $this->tpl_list_vars['cercatestoprodottocarrello'] = $this->context->cookie->_cercatestoprodottocarrello;

        // Datatable per le ultime attività
        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'datatable_ready.js');
        $this->addJS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/DataTables/datatables.min.js');
        $this->addCSS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/DataTables/datatables.min.css');

		// Select2
        $this->addjQueryPlugin(array(
            'select2'
        ));

        $this->addJS(_PS_JS_DIR_.'jquery/plugins/select2/select2_locale_'.$this->context->language->iso_code.'.js');
        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'select2_ready.js');

        /* FINE OVERRIDE */

        $this->setHelperDisplay($helper);
        $helper->tpl_vars = $this->tpl_list_vars;
        $helper->tpl_delete_link_vars = $this->tpl_delete_link_vars;

        // For compatibility reasons, we have to check standard actions in class attributes
        foreach ($this->actions_available as $action) {
            if (!in_array($action, $this->actions) && isset($this->$action) && $this->$action) {
                $this->actions[] = $action;
            }
        }
        $helper->is_cms = $this->is_cms;
        $skip_list = array();
        
        foreach ($this->_list as $row) {
            if (isset($row['id_order']) && is_numeric($row['id_order'])) {
                $skip_list[] = $row['id_cart'];
            }
        }

        if (array_key_exists('delete', $helper->list_skip_actions)) {
            $helper->list_skip_actions['delete'] = array_merge($helper->list_skip_actions['delete'], (array)$skip_list);
        } else {
            $helper->list_skip_actions['delete'] = (array)$skip_list;
        }

		// Override: Attivo l'attributo 'color' di tr per usare callback_color
		$helper->colorOnBackground = true;

        $list = $helper->generateList($this->_list, $this->fields_list);
        return $list;
    }

	// spostare in una classe / file di utilità ed usarla dichiarandola una sola volta
    public function format_prezzo($value)
	{
        // CON SIMBOLO EURO E . PER I DECIMALI
        /* $prezzo = (float)(str_replace(",",".",$value));
        $prezzo = Tools::displayPrice($prezzo, $this->context->currency); */

        $prezzo = number_format($value, 2, ',', '.');

        return '<span>'.$prezzo.'</span>';
    }

	public function viewDetails()
	{
		$context = Context::getContext();

		// Tooltip prodotti
		$this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'tooltip_prodotto.js');
		
		$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$context->employee->id);
		$tokenAddresses = Tools::getAdminToken('AdminAddresses'.(int)(Tab::getIdFromClassName('AdminAddresses')).(int)($context->employee->id));
		
		if(Tools::getIsset('viewcart')) {
			$id_customer = Db::getInstance()->getValue('
				SELECT id_customer 
				FROM '._DB_PREFIX_.'cart 
				WHERE id_cart = "'.Tools::getValue('id_cart').'"'
			);
			
			$agente = Db::getInstance()->getValue('
				SELECT agente 
				FROM customer_amministrazione 
				WHERE id_customer = '.$id_customer
			);
			
			$cart_name = Db::getInstance()->getValue('
				SELECT name 
				FROM '._DB_PREFIX_.'cart 
				WHERE id_cart = "'.Tools::getValue('id_cart').'"'
			);

			//Tools::redirectAdmin('index.php?controller=AdminCustomers&id_customer='.$id_customer.'&viewcustomer&tab_name=carts&id_cart='.Tools::getValue('id_cart').'&viewcart&tab_name=carts&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($context->employee->id)).'');
			
			// aggiornamento costante prezzi carrello tipo
			if($id_customer == 44431) {
				$prdcts = Db::getInstance()->executeS('
					SELECT * 
					FROM '._DB_PREFIX_.'cart_product 
					WHERE  id_cart = '.Tools::getValue('id_cart')
				);
				
				foreach($prdcts as $products) {
					$rebate = Db::getInstance()->getRow('
						SELECT * 
						FROM product_esolver 
						WHERE id_product = '.$products['id_product']
					);
							
					$ws_base = Db::getInstance()->getValue("
						SELECT wholesale_price 
						FROM "._DB_PREFIX_."specific_price_wholesale spw 
						WHERE spw.id_product = '".$products['id_product']."' 
							AND spw.from < '".date('Y-m-d H:i:s')."' 
							AND spw.to > '".date('Y-m-d H:i:s')."' 
							AND (spw.pieces = '' OR spw.pieces > 0)
					");
							
					$vnd_base = Product::trovaMigliorPrezzo($products['id_product'],1,$products['quantity']);
							
					$base_vnd_offerta = ($products['price'] == 0 ? $vnd_base : round($products['price'] / (1- ($products['sconto_extra']/100)),2));
							

					$ws_base_2 = ($products['listino']*(100-$products['sc_acq_1'])/100)*((100-$products['sc_acq_2'])/100)*((100-$products['sc_acq_3'])/100) * ((100-$rebate['rebate_1'])/100)*((100-$rebate['rebate_2'])/100)*((100-$rebate['rebate_3'])/100);
					
					if($ws_base == 0 || $ws_base == '')
						$ws_base = $ws_base_2;
					else
					{
						if($ws_base > $ws_base_2)
							$ws_base = $ws_base_2;
					}
					
					$wholesale_price = ($products_acquisto > 0 ? $products_acquisto : ($products['no_acq'] == 1 ? $products_acquisto : ($products['listino']*(100-$products['sc_acq_1'])/100)*((100-$products['sc_acq_2'])/100)*((100-$products['sc_acq_3'])/100) * ((100-$rebate['rebate_1'])/100)*((100-$rebate['rebate_2'])/100)*((100-$rebate['rebate_3'])/100) ));
												
					$prezzo_partenza = ($products['price'] == 0 && $products['free'] == 0 && $products['sc_qta'] == 1 ? Product::trovaMigliorPrezzo($products['id_product'],$customer->id_default_group,$products['quantity']) - (Product::trovaMigliorPrezzo($products['id_product'],$customer->id_default_group,$products['quantity']) * ($products['sconto_extra']/100)) : ($products['price'] == 0 && $products['free'] == 0 && $products['sc_qta'] == 0 ? Product::trovaMigliorPrezzo($products['id_product'],1,1) - ( Product::trovaMigliorPrezzo($products['id_product'],1,1) * ($products['sconto_extra'] / 100))  : $products['price']));
			
					Db::getInstance()->execute('
						UPDATE '._DB_PREFIX_.'cart_product 
						SET price = '.$prezzo_partenza.', prezzo_acquisto = '.$wholesale_price.' 
						WHERE id_cart = '.Tools::getValue('id_cart').' 
						AND id_product = '.$products['id_product']
					);
				}	  
			}		  
		}
				
		/*if (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== false) {
				flush();
		}*/
		
		if(isset($_GET['getPDF'])) {
			ob_start();
			require_once('../classes/html2pdf/html2pdf.class.php');
			
			$id_cst = Db::getInstance()->getValue("
				SELECT id_customer 
				FROM "._DB_PREFIX_."cart 
				WHERE id_cart = ".$_GET['id_cart']."
			");

			if(Tools::getIsset('revisione'))
				$content = Cart::getCartPDF($_GET['id_cart'], $id_cst, 'y', Tools::getValue('revisione'));
			else
				$content = Cart::getCartPDF($_GET['id_cart'], $id_cst, 'y');

			ob_clean(); 
			ob_end_clean(); 

			$html2pdf = new HTML2PDF('P','A4','it');
			$html2pdf->WriteHTML($content);

			if(Tools::getValue('originale') == 'y') {
				$html2pdf->Output($id_cst.'-offerta-'.$_GET['id_cart'].'-originale.pdf', 'D'); 
			}
			else {
				if(Tools::getIsset('revisione'))
					$html2pdf->Output($id_cst.'-offerta-'.$_GET['id_cart'].'-revisione-'.$_GET['revisione'].'.pdf', 'D'); 
				else
					$html2pdf->Output($id_cst.'-offerta-'.$_GET['id_cart'].'.pdf', 'D'); 
			}
		}
		
		if(isset($_GET['getPDFezcloudA'])) {
			require_once('../classes/html2pdf/html2pdf.class.php');
			
			$id_cst = Db::getInstance()->getValue("
				SELECT id_customer 
				FROM "._DB_PREFIX_."cart 
				WHERE id_cart = ".$_GET['id_cart']."
			");

			$content = Cart::getCartPDF_ezcloud_A($_GET['id_cart'], $id_cst);

			ob_clean(); ob_end_clean(); 
			$html2pdf = new HTML2PDF('P','A4','it');
			$html2pdf->WriteHTML($content);
			$html2pdf->Output($id_cst.'-offerta-ezcloud-'.$_GET['id_cart'].'-allegato-A.pdf', 'D'); 
		}
		
		if (isset($_GET['filename'])) {
			$filename = $_GET['filename'];

			if(strpos($filename, ":::")) {
				$parti = explode(":::", $filename);
				$nomecodificato = $parti[0];
				$nomevero = $parti[1];
			}
			else {
				$nomecodificato = $filename;
				$nomevero = $filename;
			}
						
			if (file_exists(_PS_UPLOAD_DIR_.$nomecodificato)) {
				AdminCarts::openUploadedFile();
			}
		}

		// Nuovo Preventivo e Ordine Manuale
		if(isset($_GET['createnew'])) {
			
			$getLastCart = Db::getInstance()->getValue("
				SELECT id_cart 
				FROM "._DB_PREFIX_."cart 
				ORDER BY id_cart DESC
			");

			$newCart = $getLastCart + 1;

			$address_fatturazione = Db::getInstance()->getValue("
				SELECT id_address 
				FROM "._DB_PREFIX_."address 
				WHERE customer = ".$_GET['id_customer']." 
					AND fatturazione = 1 
					AND deleted = 0 
					AND active = 1 
					AND id_country != 0
			");
				
			Db::getInstance()->execute("
				INSERT INTO "._DB_PREFIX_."cart (id_cart, id_carrier, id_lang, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, date_add, date_upd, created_by, id_employee, preventivo, provvisorio) 
				VALUES (
			'".$newCart."', 0, 5, '".$address_fatturazione."', '".$address_fatturazione."', 1, '".$_GET['id_customer']."', 0, '', 0,0,'', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."', ".$context->employee->id.", ".$context->employee->id.", ".(Tools::getIsset('preventivo') ? (Tools::getValue('preventivo') == 2 ? 2 : Tools::getValue('preventivo')) : Tools::getValue('preventivo')).", 1)
			");
			
			$is = Db::getInstance()->getValue('
				SELECT id_cart 
				FROM '._DB_PREFIX_.'cart_ezcloud 
				WHERE id_cart = '.$newCart
			);
			
			Customer::Storico($newCart, 'C', $context->employee->id, 19);
				
			Db::getInstance()->execute("
				INSERT INTO carrelli_creati (id_cart, id_carrier, id_lang, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, date_add, date_upd, created_by, id_employee, preventivo, provvisorio) 
				VALUES (
			'".$newCart."', 0, 5, '".$address_fatturazione."', '".$address_fatturazione."', 1, '".$_GET['id_customer']."', 0, '', 0,0,'', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."', ".$context->employee->id.", ".$context->employee->id.", ".(Tools::getIsset('preventivo') ? (Tools::getValue('preventivo') == 2 ? 2 : Tools::getValue('preventivo')) : Tools::getValue('preventivo')).", 1)
			");
			
		
			if(Tools::getIsset('rif_prev')) {
				Db::getInstance()->execute("
					UPDATE "._DB_PREFIX_."cart 
					SET rif_prev = '".Tools::getValue('rif_prev')."' 
					WHERE id_cart = '".$newCart."'
				");
				
				if(is_numeric(Tools::getValue('rif_prev'))) {
					//Db::getInstance()->execute("UPDATE form_prevendita_thread SET status='closed' WHERE id_thread = ".Tools::getValue('rif_prev')."");
					Db::getInstance()->execute("
						UPDATE "._DB_PREFIX_."cart 
						SET esigenze = '".addslashes(Db::getInstance()->getValue('
							SELECT message 
							FROM form_prevendita_message 
							WHERE id_thread='.Tools::getValue('rif_prev').' 
							ORDER BY id_message ASC
						'))."' 
						WHERE id_cart = '".$newCart."'
					");
					
					Db::getInstance()->execute("
						UPDATE carrelli_creati 
						SET esigenze = '".Db::getInstance()->getValue('
							SELECT message 
							FROM form_prevendita_message 
							WHERE id_thread='.Tools::getValue('rif_prev').' 
							ORDER BY id_message DESC
						')."' 
						WHERE id_cart = '".$newCart."'
					");
					
					$note_da_copiare = Db::getInstance()->executeS('
						SELECT * 
						FROM '._DB_PREFIX_.'note_attivita 
						WHERE id_attivita = "'.Tools::getValue('rif_prev').'" 
							AND tipo_attivita = "P"
					');
						
					foreach($note_da_copiare as $ndc)
					{
						if(strip_tags($ndc['note']) != '')
							Db::getInstance()->execute('
								INSERT INTO cart_note (id_note, id_cart, id_employee, note, date_add, date_upd) 
								VALUES (NULL, '.$newCart.', "'.$ndc['id_employee'].'","'.$ndc['note'].'", "'.date('Y-m-d H:i:s').'","'.date('Y-m-d H:i:s').'")
							');
					}
				}
				else {
					$first = substr(Tools::getValue('rif_prev'),0,1);
					$thread = substr(Tools::getValue('rif_prev'),1,99);
					
					if($first == 'T') {
						//Db::getInstance()->execute("UPDATE "._DB_PREFIX_."customer_thread SET status='closed' WHERE id_customer_thread = ".$thread."");
						
						Db::getInstance()->executeS("
							UPDATE "._DB_PREFIX_."cart 
							SET esigenze = '".addslashes(Db::getInstance()->getValue('
								SELECT message 
								FROM '._DB_PREFIX_.'customer_message 
								WHERE id_customer_thread='.$thread.' 
								ORDER BY id_customer_message ASC
							'))."' 
							WHERE id_cart = '".$newCart."'
						");
						
						Db::getInstance()->execute("
							UPDATE carrelli_creati 
							SET esigenze = '".addslashes(Db::getInstance()->getValue('
								SELECT message 
								FROM '._DB_PREFIX_.'customer_message 
								WHERE id_customer_thread='.$thread.' 
								ORDER BY id_customer_message ASC
							'))."' 
							WHERE id_cart = '".$newCart."'
						");
						
						$note_da_copiare = Db::getInstance()->executeS('
							SELECT * 
							FROM '._DB_PREFIX_.'note_attivita 
							WHERE id_attivita = "'.$thread.'" 
								AND tipo_attivita = "T"
						');
					}
					else if($first == 'O') {
						$note_da_copiare = Db::getInstance()->executeS('
							SELECT * 
							FROM cart_note 
							WHERE id_cart = "'.Db::getInstance()->getValue('
								SELECT id_cart 
								FROM '._DB_PREFIX_.'orders 
								WHERE id_order = '.$thread
							).'"
						');
					}
					else if($first == 'A') {
						//Db::getInstance()->execute("UPDATE action_thread SET status='closed' WHERE id_action = ".$thread."");
						
						/*Db::getInstance()->executeS("
							UPDATE cart 
							SET esigenze = '".addslashes(Db::getInstance()->getValue('
								SELECT action_message 
								FROM action_message 
								WHERE id_action ='.$thread.' 
								ORDER BY id_action_message ASC
							'))."' 
							WHERE id_cart = '".$newCart."'
						");*/
						
						/*Db::getInstance()->execute("UPDATE cart SET note_private = '".addslashes(Db::getInstance()->getValue('SELECT action_message FROM action_message WHERE id_action='.$thread.' ORDER BY id_action_message ASC'))."' WHERE id_cart = '".$newCart."'");
						
						Db::getInstance()->execute("UPDATE carrelli_creati SET note_private = '".addslashes(Db::getInstance()->getValue('SELECT action_message FROM action_message WHERE id_action='.$thread.' ORDER BY id_action_message ASC'))."' WHERE id_cart = '".$newCart."'");*/
						
						$note_da_copiare = Db::getInstance()->executeS('
							SELECT * 
							FROM '._DB_PREFIX_.'note_attivita 
							WHERE id_attivita = "'.$thread.'" AND tipo_attivita = "A"
						');
					
						if((strip_tags(Db::getInstance()->getValue('
							SELECT action_message 
							FROM action_message 
							WHERE id_action='.$thread.' 
							ORDER BY id_action_message ASC'))) != '') {
							Db::getInstance()->executeS("
								INSERT INTO cart_note
								(id_note,
								id_cart,
								id_employee,
								note,
								date_add,
								date_upd)
								VALUES (
								NULL,
								'".$newCart."',
								'".addslashes(strip_tags(Db::getInstance()->getValue('SELECT action_m_from FROM action_message WHERE id_action='.$thread.' ORDER BY id_action_message ASC')))."',
								'".addslashes(strip_tags(Db::getInstance()->getValue('SELECT action_message FROM action_message WHERE id_action='.$thread.' ORDER BY id_action_message ASC')))."',
								'".addslashes(strip_tags(Db::getInstance()->getValue('SELECT date_add FROM action_message WHERE id_action='.$thread.' ORDER BY id_action_message ASC')))."',
								'".addslashes(strip_tags(Db::getInstance()->getValue('SELECT date_add FROM action_message WHERE id_action='.$thread.' ORDER BY id_action_message ASC')))."'
							)");
						}	
					}
					else if($first == 'P') {
						//Db::getInstance()->execute("UPDATE form_prevendita_thread SET status='closed' WHERE id_thread = ".$thread."");
						Db::getInstance()->execute("
							UPDATE "._DB_PREFIX_."cart 
							SET esigenze = '".addslashes(Db::getInstance()->getValue('
								SELECT message 
								FROM form_prevendita_message 
								WHERE id_thread='.$thread.' ORDER BY id_message ASC
							'))."' 
							WHERE id_cart = '".$newCart."'
						");
						
						Db::getInstance()->execute("
							UPDATE carrelli_creati 
							SET esigenze = '".Db::getInstance()->getValue('
								SELECT message 
								FROM form_prevendita_message 
								WHERE id_thread='.$thread.' 
								ORDER BY id_message DESC
							')."' 
							WHERE id_cart = '".$newCart."'
						");
						
						$note_da_copiare = Db::getInstance()->executeS('
							SELECT * 
							FROM '._DB_PREFIX_.'note_attivita 
							WHERE id_attivita = "'.$thread.'" 
								AND tipo_attivita = "P"
						');
					}
						
					foreach($note_da_copiare as $ndc) {
						if(strip_tags($ndc['note']) != '')
							Db::getInstance()->execute('
								INSERT INTO cart_note (id_note, id_cart, id_employee, note, date_add, date_upd) 
								VALUES (NULL, '.$newCart.', "'.$ndc['id_employee'].'","'.$ndc['note'].'", "'.date('Y-m-d H:i:s').'","'.date('Y-m-d H:i:s').'")
							');
					}
				}
			}
			else {
				//Db::getInstance()->execute("UPDATE customer_thread SET status='closed' WHERE id_customer_thread = ".substr(Tools::getValue('rif_prev'),0,-1)."");
				Db::getInstance()->execute("
					UPDATE "._DB_PREFIX_."cart 
					SET esigenze = '".(strpos(Tools::getValue('rif_prev'), 'T') !== false ? addslashes(Db::getInstance()->getValue('
						SELECT message 
						FROM '._DB_PREFIX_.'customer_message 
						WHERE id_customer_thread='.substr(Tools::getValue('rif_prev'),0,-1).' 
						ORDER BY id_customer_message ASC')) : addslashes(Db::getInstance()->getValue('
							SELECT action_message 
							FROM action_message 
							WHERE id_action='.substr(Tools::getValue('rif_prev'),0,-1).' 
							ORDER BY id_action_message ASC')))."' 
					WHERE id_cart = '".$newCart."'
				");
				
				Db::getInstance()->execute("
					UPDATE carrelli_creati 
					SET esigenze = '".(strpos(Tools::getValue('rif_prev'), 'T') !== false ? addslashes(Db::getInstance()->getValue('
						SELECT message 
						FROM '._DB_PREFIX_.'customer_message 
						WHERE id_customer_thread='.substr(Tools::getValue('rif_prev'),0,-1).' 
						ORDER BY id_customer_message ASC')) : addslashes(Db::getInstance()->getValue('
							SELECT action_message 
							FROM action_message 
							WHERE id_action='.substr(Tools::getValue('rif_prev'),0,-1).' 
							ORDER BY id_action_message ASC')))."' 
					WHERE id_cart = '".$newCart."'
				");
			}
			
			if(Tools::getIsset('riferimento') && Tools::getValue('riferimento') != '')
			{
				Db::getInstance()->execute("
					UPDATE "._DB_PREFIX_."cart 
					SET riferimento = ".Tools::getValue('riferimento')." 
					WHERE id_cart = '".$newCart."'
				");
				
				Db::getInstance()->execute("
					UPDATE carrelli_creati 
					SET riferimento = ".Tools::getValue('riferimento')." 
					WHERE id_cart = '".$newCart."'
				");
			}
			
			if(Tools::getIsset('riferimento') && Tools::getValue('riferimento') == '') {
				$cstp = new Customer($_GET['id_customer']);
				$persona = Db::getInstance()->getValue('
					SELECT id_persona 
					FROM persone 
					WHERE firstname = "'.$cstp->firstname.'" 
						AND lastname = "'.$cstp->lastname.'"
				');
			
				Db::getInstance()->execute("
					UPDATE "._DB_PREFIX_."cart 
					SET riferimento = ".$persona." 
					WHERE id_cart = '".$newCart."'
				");
				
				Db::getInstance()->execute("
					UPDATE carrelli_creati 
					SET riferimento = ".$persona." 
					WHERE id_cart = '".$newCart."'
				");
			
			}
			
			if(!Tools::getIsset('riferimento')) {
				$cstp = new Customer($_GET['id_customer']);
				$persona = Db::getInstance()->getValue('
					SELECT id_persona 
					FROM persone
					WHERE firstname = "'.$cstp->firstname.'" 
						AND lastname = "'.$cstp->lastname.'"
				');
			
				Db::getInstance()->execute("
					UPDATE "._DB_PREFIX_."cart 
					SET riferimento = ".$persona." 
					WHERE id_cart = '".$newCart."'
				");

				Db::getInstance()->execute("
					UPDATE carrelli_creati 
					SET riferimento = ".$persona." 
					WHERE id_cart = '".$newCart."'
				");
			}
			
			if(Tools::getIsset('rif_prev') && Tools::getValue('id_cart') != '') {
				Tools::redirectAdmin(self::$currentIndex.'&id_cart='.$newCart.'&viewcart&copyfromtemplatetoexistent=y&id_template="'.Tools::getValue('id_cart').'"&id_customer='.$customer->id.'&conf=4'.'&token='.$this->token);
			}
			
			Tools::redirectAdmin(self::$currentIndex.'&id_cart='.$newCart.'&viewcart&preventivo='.Tools::getValue('preventivo').(Tools::getIsset('rif_prev') ? '&rif_prev='.Tools::getValue('rif_prev') : '').'&id_customer='.$_GET['id_customer'].'&conf=4'.'&token='.$this->token);
		}

		if(Tools::getIsset('copyfromtemplatetoexistent')) {
			$prezzi_carrello_corrente = Db::getInstance()->getValue('
				SELECT prezzi_carrello 
				FROM '._DB_PREFIX_.'cart 
				WHERE id_cart = '.Tools::getValue('id_cart')
			);

			$prezzi_carrello_template = Db::getInstance()->getValue('
				SELECT prezzi_carrello 
				FROM '._DB_PREFIX_.'cart 
				WHERE id_cart = '.Tools::getValue('id_template')
			);
			
			$prodotti_da_copiare = Db::getInstance()->executeS('
				SELECT * 
				FROM '._DB_PREFIX_.'cart_product 
				WHERE id_cart = '.trim(Tools::getValue('id_template')).'
			');
			
			$customer = Db::getInstance()->getValue('
				SELECT id_customer 
				FROM  '._DB_PREFIX_.'cart 
				WHERE id_cart = '.trim(Tools::getValue('id_template')).'
			');
			
			$gruppo = Db::getInstance()->getValue('
				SELECT id_default_group 
				FROM '._DB_PREFIX_.'customer 
				WHERE id_customer = '.$customer
			);
			
			$customer_to = Db::getInstance()->getValue('
				SELECT id_customer 
				FROM  '._DB_PREFIX_.'cart 
				WHERE id_cart = '.trim(Tools::getValue('id_cart')).'
			');
			
			$note = Db::getInstance()->getRow('
				SELECT premessa, note, risorse, attachment, name 
				FROM '._DB_PREFIX_.'cart 
				WHERE id_cart = '.trim(Tools::getValue('id_template')).'
			');
			
			Db::getInstance()->execute('
				UPDATE '._DB_PREFIX_.'cart 
				SET name = "'.addslashes($note['name']).'", template = "'.addslashes($note['name']).'", risorse = "'.addslashes($note['risorse']).'", premessa = "'.addslashes($note['premessa']).'", note = "'.addslashes($note['note']).'", attachment = "'.addslashes($note['attachment']).'" WHERE id_cart = '.Tools::getValue('id_cart')
			);

			Db::getInstance()->execute('
				UPDATE carrelli_creati 
			SET name = "'.addslashes($note['name']).'", premessa = "'.addslashes($note['premessa']).'", risorse = "'.addslashes($note['risorse']).'", note = "'.addslashes($note['note']).'" WHERE id_cart = '.Tools::getValue('id_cart')
			);

			if(trim(Tools::getValue('id_template')) != 70146) {
				
				//$products_count = Db::getInstance()->getValue('SELECT count(id_product) FROM  cart_product WHERE id_cart = '.trim(Tools::getValue('id_cart')).'');
					
				//if($products_count == 0)
				//{
					Db::getInstance()->execute('
						DELETE 
						FROM '._DB_PREFIX_.'cart_product 
						WHERE id_cart = '.Tools::getValue('id_cart')
					);

					Db::getInstance()->execute('
						DELETE 
						FROM carrelli_creati_prodotti 
						WHERE id_cart = '.Tools::getValue('id_cart')
					);
				
					foreach($prodotti_da_copiare as $prodotto_da_copiare) {
						$vecchio_prezzo = 0;
						$sconto_extra_v = 0;
						if($_POST['mantieni_stessi_prezzi'] == 's') {
							$unitario = Db::getInstance()->getValue('
								SELECT price 
								FROM '._DB_PREFIX_.'product 
								WHERE id_product = '.$prodotto_da_copiare['id_product']
							);

							/*$speciale = Product::trovaPrezzoSpeciale($prodotto_da_copiare['id_product'], 1, 0);
			
							if($speciale < $unitario && $speciale != 0) {
								$unitario = $speciale;
							}*/

							$unitario = Product::trovaMigliorPrezzo($prodotto_da_copiare['id_product'], $gruppo, $prodotto_da_copiare['quantity']);
							
							if($unitario != $prodotto_da_copiare['price']) {
								$vecchio_prezzo = $prodotto_da_copiare['price'];
								$sconto_extra_v = (($unitario - $prodotto_da_copiare['price'])*100)/$unitario;
							}	
							else {	
								$vecchio_prezzo = $prodotto_da_copiare['price'];
								$sconto_extra_v = 0;
							}
						}
						else {
							$sconto_extra_v = 0;
						}
					
						if($prodotto_da_copiare['id_product'] != 0) {
							if($prezzi_carrello_corrente == 3) {
								$vecchio_prezzo = Product::trovaMigliorPrezzo($prodotto_da_copiare['id_product'],3,999999);
								$sconto_extra_v = 0;
							}
							else if($prezzi_carrello_corrente == 15) {
								$new_template_price = Product::trovaMigliorPrezzo($prodotto_da_copiare['id_product'],3,999999);
								$unitario =  Product::trovaMigliorPrezzo($prodotto_da_copiare['id_product'],1,$prodotto_da_copiare['quantity']);
								$new_template_price = $new_template_price+(($new_template_price/100)*3);
								
								if($new_template_price < Product::trovaMigliorPrezzo($prodotto_da_copiare['id_product'],1,1)) {	
									$sconto_extra_v = (($unitario - $new_template_price)*100)/$unitario;
									$vecchio_prezzo = $new_template_price;
								}	
							}

							Db::getInstance()->execute('
								INSERT INTO '._DB_PREFIX_.'cart_product (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, prezzo_acquisto, no_acq, sconto_extra, sort_order, bundle, date_add) 
								VALUES ('.Tools::getValue('id_cart').', "'.$prodotto_da_copiare['id_product'].'", "'.$prodotto_da_copiare['id_product_attribute'].'", "'.$prodotto_da_copiare['quantity'].'", "'.$vecchio_prezzo.'","'.$prodotto_da_copiare['free'].'","'.addslashes($prodotto_da_copiare['name']).'","1","'.($_POST['mantieni_stessi_prezzi'] == 's' ? 0 : 0).'","'.$prodotto_da_copiare['no_acq'].'","'.$sconto_extra_v.'","'.$prodotto_da_copiare['sort_order'].'","'.$prodotto_da_copiare['bundle'].'","'.date('Y-m-d H:i:s').'")
							');   
						
							Db::getInstance()->execute('
								INSERT INTO carrelli_creati_prodotti (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, prezzo_acquisto, no_acq, sconto_extra, sort_order, bundle, date_add) 
								VALUES ('.Tools::getValue('id_cart').', "'.$prodotto_da_copiare['id_product'].'", "'.$prodotto_da_copiare['id_product_attribute'].'", "'.$prodotto_da_copiare['quantity'].'", "'.$vecchio_prezzo.'","'.$prodotto_da_copiare['free'].'","'.addslashes($prodotto_da_copiare['name']).'","1","'.($_POST['mantieni_stessi_prezzi'] == 's' ? 0 : 0).'","'.$prodotto_da_copiare['no_acq'].'","'.$sconto_extra_v.'","'.$prodotto_da_copiare['sort_order'].'","'.$prodotto_da_copiare['bundle'].'","'.date('Y-m-d H:i:s').'")
							');
						}
					}
				//}
			}
		
			$template_redirection = (self::$currentIndex.'&id_cart='.Tools::getValue('id_cart').'&viewcart&id_customer='.$customer_to.'&conf=4'.'&token='.$this->token);

			Tools::redirectAdmin($template_redirection);
		}
		
		if(($context->employee->id_profile == 7 && $agente == $context->employee->id) || $context->employee->id_profile != 7) {
			/*if (!($cart = AdminCarts::loadObject(true)))
				return;*/
			$cart = new Cart(Tools::getValue('id_cart'));			
			$customer = new Customer($cart->id_customer);
			$customerStats = $customer->getStats();
			$products = $cart->getProductsOld();
			$customizedDatas = Product::getAllCustomizedDatas((int)($cart->id));
			
			Product::addCustomizationPrice($products, $customizedDatas);
			
			$discounts = $cart->getDiscounts();
			$currency = new Currency($cart->id_currency);
			$currentLanguage = new Language((int)($context->language->id));
			$id_order = (int)(Order::getOrderByCartId($cart->id));
			$order = new Order($id_order);
			
			if($id_order > 0)
				$summary = $cart->getSummaryDetails();
			
			if(Tools::getIsset('cancellarevisione')) {
				Customer::Storico(Tools::getValue('id_cart'), 'C', $context->employee->id, 20);
				//mysql_select_db(_DB_REV_NAME_);
				// correggere: deleted non esiste
				/*Db::getInstance()->execute('
					UPDATE '._DB_PREFIX_.'cart_revisioni 
					SET deleted = 1 
					WHERE id_revisione = '.Tools::getValue('cancellarevisione')
				);*/

				Db::getInstance()->execute('
					DELETE 
					FROM '._DB_PREFIX_.'cart_product_revisioni 
					WHERE id_revisione = '.Tools::getValue('cancellarevisione')
				);

				//mysql_select_db(_DB_NAME_);
				if(Tools::getIsset('customers') && Tools::getValue('customers') == 'y')
					Tools::redirectAdmin("index.php?controller=AdminCustomers&id_customer=".$customer->id."&viewcustomer&tab_name=carts&id_cart=".$_GET['id_cart']."&viewcart&token=".$tokenCustomers);
				else
					Tools::redirectAdmin(self::$currentIndex.'&id_cart='.$_GET['id_cart'].'&viewcart&id_customer='.$customer->id.'&conf=4&token='.$this->token);
			}
			
			if(Tools::getIsset('vedirevisione')) {
				Customer::Storico(Tools::getValue('id_cart'), 'C', $context->employee->id, 21);
				//mysql_select_db(_DB_REV_NAME_);
				$id_order = "REVISIONE";
				$order = new Order(0);
				$order->id = "REVISIONE";
				$tot_revisioni = Db::getInstance()->executeS('SELECT id_revisione FROM '._DB_PREFIX_.'cart_revisioni WHERE id_cart = '.Tools::getValue('id_cart').' ORDER BY id_revisione ASC');
				$ord_rev = 0;
				foreach($tot_revisioni as $tot_rev)
				{
					if(Tools::getValue('vedirevisione') == $tot_rev['id_revisione'])
						$ord_revisione = $ord_rev;
						
					$ord_rev++;
				}
				//mysql_select_db(_DB_NAME_);
			}
			else {
				//mysql_select_db(_DB_REV_NAME_);
				$tot_revisioni = Db::getInstance()->getValue('SELECT count(id_revisione) FROM '._DB_PREFIX_.'cart_revisioni WHERE id_cart = '.Tools::getValue('id_cart').' ORDER BY id_revisione ASC');
				$ord_rev = $tot_revisioni;
				//mysql_select_db(_DB_NAME_);
			}
			
			$creato_da = Db::getInstance()->getValue("SELECT created_by FROM "._DB_PREFIX_."cart WHERE id_cart = '".$cart->id."'");
			$creato_da_nome = Db::getInstance()->getValue("SELECT CONCAT(firstname,' ',LEFT(lastname, 1),'.') AS name FROM "._DB_PREFIX_."employee WHERE id_employee = '".$creato_da."'");
			
			$visualizzato = Db::getInstance()->getValue("SELECT visualizzato FROM "._DB_PREFIX_."cart WHERE id_cart = '".$_GET['id_cart']."' ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
			$name = Db::getInstance()->getValue("SELECT name FROM "._DB_PREFIX_."cart WHERE id_cart = '".$_GET['id_cart']."' ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
			
			switch($visualizzato) {
				case '997': $style = 'style=";padding:3px; background-color:#47a4c9; color:#ffffff"'; break;
				case '999': $style = 'style=";padding:3px; background-color:#000000; color:#ffffff"'; break;
				case '998': $style = 'style=";padding:3px; background-color:#ffff00; color:#000000"'; break;
				default: $style = ''; break;
			}
				
			/*if(Tools::getIsset('vedirevisione')) {
				//mysql_select_db(_DB_REV_NAME_);
			}*/
			
				$validita = Db::getInstance()->getValue("SELECT validita FROM "._DB_PREFIX_."cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				
				$default_payment = Db::getInstance()->getValue('SELECT pagamento FROM customer_amministrazione WHERE id_customer = '.$customer->id);
				
				$payment = Db::getInstance()->getValue("SELECT payment FROM "._DB_PREFIX_."cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				$name = Db::getInstance()->getValue("SELECT name FROM "._DB_PREFIX_."cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				$in_carico_a = Db::getInstance()->getValue("SELECT in_carico_a FROM "._DB_PREFIX_."cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				
				$riferimento = Db::getInstance()->getValue("SELECT riferimento FROM "._DB_PREFIX_."cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				$rif_prev = Db::getInstance()->getValue("SELECT rif_prev FROM "._DB_PREFIX_."cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				$consegna_tempi = Db::getInstance()->getValue("SELECT consegna FROM "._DB_PREFIX_."cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				$template_cart = Db::getInstance()->getValue("SELECT template FROM "._DB_PREFIX_."cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				
				if(Tools::getIsset('vedirevisione'))
					$note = Db::getInstance()->getValue("SELECT note FROM "._DB_PREFIX_."cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				else
					$note = Db::getInstance()->getValue("SELECT note FROM carrelli_creati WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");

				if(stripslashes($note) == '')
				{	
					$note = Db::getInstance()->getValue("SELECT note FROM "._DB_PREFIX_."cart WHERE id_cart = ".$_GET['id_cart']);	
					
				}
				$esigenze = Db::getInstance()->getValue("SELECT esigenze FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				$risorse = Db::getInstance()->getValue("SELECT risorse FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				$premessa = Db::getInstance()->getValue("SELECT premessa FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				$note_private = Db::getInstance()->getValue("SELECT note_private FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				$attachment = Db::getInstance()->getValue("SELECT attachment FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				$id_persona = Db::getInstance()->getValue("SELECT id_persona FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				$impiegato = Db::getInstance()->getValue("SELECT id_employee FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				$preventivo = Db::getInstance()->getValue("SELECT preventivo FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				$prezzi_carrello = Db::getInstance()->getValue("SELECT prezzi_carrello FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				$rif_ordine = Db::getInstance()->getValue("SELECT rif_ordine FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				$cig = Db::getInstance()->getValue("SELECT cig FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				$cup = Db::getInstance()->getValue("SELECT cup FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				$ipa = Db::getInstance()->getValue("SELECT ipa FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				
				$esito = Db::getInstance()->getValue("SELECT esito FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				
				$causa = Db::getInstance()->getValue("SELECT causa FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				
				$concorrente = Db::getInstance()->getValue("SELECT concorrente FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				
				$data_ordine_mepa = Db::getInstance()->getValue("SELECT data_ordine_mepa FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				
				$competenza_dal = Db::getInstance()->getValue("SELECT competenza_dal FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				
				$competenza_al = Db::getInstance()->getValue("SELECT competenza_al FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				
				$cadenza = Db::getInstance()->getValue("SELECT cadenza FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				
				$scadenza = Db::getInstance()->getValue("SELECT scadenza FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				
				$decorrenza = Db::getInstance()->getValue("SELECT decorrenza FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				
				$tipo_carrello = 'Preventivo';
				
				if($preventivo == 0 && $creato_da == 0 && $impiegato == 0)
					$tipo_carrello = 'Carrello cliente';
				else if ($creato_da > 0 && $preventivo == 0)
					$tipo_carrello = 'Carrello';
				else if ($creato_da > 0 && $preventivo == 2)
					$tipo_carrello = 'Carrello per ordine manuale';
				else if ($creato_da > 0 && $preventivo == 1)
					$tipo_carrello = 'Preventivo';
				else if($preventivo == 0 && $creato_da == 0 && $impiegato > 0)
					$tipo_carrello = 'Carrello cliente modificato da noi';
				
				
				$is_copy = Db::getInstance()->getValue('SELECT desc_attivita FROM storico_attivita WHERE id_attivita = '.$cart->id.' AND tipo_attivita = "C" AND desc_attivita LIKE "%copiato questo carrello a partire%"');
				if($is_copy != '')
				{	
					$copiato_da = str_replace('Ha copiato questo carrello a partire dal carrello n. ', '',$is_copy);
					$copiato_da = strip_tags($copiato_da);
					$copiato_da = '(Copiato da <a href="'.self::$currentIndex.'&id_cart='.$copiato_da.'&viewcart&token='.$this->token.'">'.$copiato_da.'</a>)';
				}	
				else
					$copiato_da = '';
				
				// display cart header
				$html .= '<h2 '.$style.'><a href="?controller=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($context->employee->id)).'">'.(($customer->id) ? ($customer->is_company == 1 ? $customer->company : $customer->firstname.' '.$customer->lastname) : ('Guest')).'</a> - '.($ord_rev > 0 ? 'Revisione' : '').' '.('Carrello ')."<span id='cart_id_span'>".$cart->id.(Tools::getIsset('vedirevisione') ? '-'.$ord_revisione : ($ord_rev > 0 ? "-".$ord_rev : '-0')).'</span> - Tipo: '.$tipo_carrello.' '.$copiato_da.($id_order > 0 ? ' - '.(Tools::getValue('tab') == 'AdminCustomers' ? '<a href="?controller=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&id_order='.(int)($order->id).'&vieworder&token='.$tokenCustomers.'&tab_name=orders">' : '<a href="?controller=AdminOrders&id_order='.(int)($order->id).'&vieworder&token='.Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).(int)($context->employee->id)).'">').' '.('Ordine n.').sprintf('%06d', $order->id).' convertito il '.Tools::displayDate($order->date_add, (int)$context->language->id, true).'</a>' : '').' '.($visualizzato > 997 ? ' - '.$name : '').'</h2><br />';
				
				if($prezzi_carrello == 3)
					$customer->id_default_group = 3;
				
				$revisioni = Db::getInstance()->getValue("SELECT revisioni FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				
				$date_upd = Db::getInstance()->getValue("SELECT date_upd FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				
			if(Tools::getIsset('vedirevisione'))
			{
				//mysql_select_db(_DB_NAME_);
			}
			
				if($impiegato != 0) { $nome_impiegato = Db::getInstance()->getValue("SELECT CONCAT(firstname,' ',LEFT(lastname, 1),'.') AS name FROM "._DB_PREFIX_."employee WHERE id_employee = ".$impiegato.""); } else { $nome_impiegato = 'Cliente'; }
			
			$html .= '<fieldset style="background-color:#ffffff; margin-top:-30px; border:0px">';
			$html .= '
				<script type="text/javascript">
				$(document).ready(function() {
						$("#products input").bind("keypress", function(e) {
							if (e.keyCode == 13) {
								return false;
							}
						});
					});
				</script>';
				
				$html .= '
				<script type="text/javascript">  
				var iso = \''.$isoTinyMCE.'\' ;  
				var pathCSS = \''._THEME_CSS_DIR_.'\' ;  
				var ad = \''.$ad.'\' ;  
				</script>  
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce3/tiny_mce.js"></script>  
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>
				
				
				<script>
					$(document).ready(function() {
						$(".span-reference-2").tooltipster({interactive: true});
					});
				</script>
				
				<script type="text/javascript">
					function aggiungiSuggerito(id_product) {

						$.ajax({
						type: "GET",
						data: "id_cart='.$_GET['id_cart'].'&id_bundle=0&product_in_bundle="+id_product,
						async: false,
						url: "ajax_products_list2.php",
						success: function(resp)
							{
								addProduct_TR("",resp.split("|"),"");
							},
							error: function(XMLHttpRequest, textStatus, errorThrown)
							{
								tooltip_content = "";
								alert("ERROR" + textStatus + " " + XMLHttpRequest.responseText);					
							}
													
						});
					}
					
				</script>';		
				
			
			
			// correggere: inutilizzato?
			if(Tools::getIsset('submitCollegaAttivita'))
			{
				$id_thread = substr(Tools::getValue('coll_partenza'),1);
				
				Customer::Storico(Tools::getValue('id_cart'), 'C', $context->employee->id, 'Ha collegato ad attivit&agrave; n. '.Customer::trovaSigla(substr(Tools::getValue('coll_destinazione'),1), substr(Tools::getValue('coll_destinazione'),0,1)));
				
				switch(substr(Tools::getValue('coll_partenza'), 0,1))
				{
					case 'T': Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'customer_thread SET riferimento = "'.Tools::getValue('coll_destinazione').'" WHERE id_customer_thread = '.$id_thread.''); break;
					case 'P': Db::getInstance()->execute('UPDATE form_prevendita_thread SET riferimento = "'.Tools::getValue('coll_destinazione').'" WHERE id_thread = '.$id_thread.''); break;
					case 'A': Db::getInstance()->execute('UPDATE action_thread SET riferimento = "'.Tools::getValue('coll_destinazione').'" WHERE id_action = '.$id_thread.''); break;
					default: ''; break;
				}
			}
			
				$html .= '
				<link rel="stylesheet" href="../css/jquery.treeview.css" type="text/css" />

				<script type="text/javascript" src="../js/jquery.treeview.js"></script>

				<script type="text/javascript">

				// build treeview after web page has been loaded

				$(document).ready(function(){

					$(".tree-menu").treeview();
					
					$(".tasti-apertura").mouseover(function() {
						$(this).children(".menu-apertura").show();
					}).mouseout(function() {
						$(this).children(".menu-apertura").hide();
					});
					
					$(".tasti-apertura-in").mouseover(function() {
						$(this).children(".menu-apertura-in").show();
					}).mouseout(function() {
						$(this).children(".menu-apertura-in").hide();
					});


				});

				</script>
				';

				/*$html .= '
				
				<div class="tasti-apertura">
				<a class="button" style="display:block" href="#"><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" />&nbsp;&nbsp;&nbsp;Aggiungi nuova azione padre</a>
					<div class="menu-apertura">
						<ul class="dropdown">
							<li class="tasti-apertura-in"><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewticket&aprinuovoticket&token='.$tokenCustomers.'&tab-container-1=6"><img src="../img/admin/AdminTools.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;Ticket</a>
								<div class="menu-apertura-in" style="position:absolute; left:100px">
									<ul class="dropdown">
										<li><a style="color:#000" href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewticket&aprinuovoticket&token='.$tokenCustomers.'&tipo-ticket=2&tab-container-1=6"><img src="../img/admin/icons/contabilita.gif" alt="Contabilita" title="Contabilita" />&nbsp;&nbsp;&nbsp;Contabilit&agrave;</a></li>
										
										<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewticket&aprinuovoticket&token='.$tokenCustomers.'&tipo-ticket=8&tab-container-1=6"><img src="../img/admin/icons/ordini.gif" alt="Ordini" title="Ordini" />&nbsp;&nbsp;&nbsp;Ordini</a></li>
										
										<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewticket&aprinuovoticket&token='.$tokenCustomers.'&tipo-ticket=4&tab-container-1=6"><img src="../img/admin/icons/assistenza.gif" alt="Assistenza tecnica" title="Assistenza tecnica" />&nbsp;&nbsp;&nbsp;Assistenza tecnica</a></li>
										
										<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewticket&aprinuovoticket&token='.$tokenCustomers.'&tipo-ticket=9&tab-container-1=6"><img src="../img/admin/icons/rma.gif" alt="RMA" title="RMA" />&nbsp;&nbsp;&nbsp;RMA</a></li>
										
										<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewticket&aprinuovoticket&token='.$tokenCustomers.'&tipo-ticket=3&tab-container-1=6"><img src="../img/admin/icons/rivenditori.gif" alt="Rivenditori" title="Rivenditori" />&nbsp;&nbsp;&nbsp;Rivenditori</a></li>
										
									</ul>
								</div>
							</li>
							<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewmessage&aprinuovomessaggio&token='.$tokenCustomers.'&tab-container-1=7"><img src="../img/admin/email.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;Messaggio</a></li>
							<li class="tasti-apertura-in"><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewaction&aprinuovaazione&token='.$tokenCustomers.'&tab-container-1=10"><img src="../img/admin/return.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;To-do</a>
								<div class="menu-apertura-in" style="position:absolute; left:100px">
									<ul class="dropdown">
										<li><a style="color:#000" href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewaction&aprinuovaazione&token='.$tokenCustomers.'&tipo_todo=Attivita&tab-container-1=10"><img src="../img/admin/icons/attivita.gif" alt="Attivita" title="Attivita" />&nbsp;&nbsp;&nbsp;Attivit&agrave;</a></li>
										
										<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewaction&aprinuovaazione&token='.$tokenCustomers.'&tipo_todo=Telefonata&tab-container-1=10"><img src="../img/admin/icons/telefonata.gif" alt="Telefonata" title="Telefonata" />&nbsp;&nbsp;&nbsp;Telefonata</a></li>
										
										<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewaction&aprinuovaazione&token='.$tokenCustomers.'&tipo_todo=Visita&tab-container-1=10"><img src="../img/admin/icons/visita.gif" alt="Visita" title="Visita" />&nbsp;&nbsp;&nbsp;Visita</a></li>
										
										<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewaction&aprinuovaazione&token='.$tokenCustomers.'&tipo_todo=Caso&tab-container-1=10"><img src="../img/admin/icons/caso.gif" alt="Caso" title="Caso" />&nbsp;&nbsp;&nbsp;Caso</a></li>
										
										<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewaction&aprinuovaazione&token='.$tokenCustomers.'&tipo_todo=Intervento&tab-container-1=10"><img src="../img/admin/icons/intervento.gif" alt="Intervento" title="Intervento" />&nbsp;&nbsp;&nbsp;Intervento</a></li>
										
										<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewaction&aprinuovaazione&token='.$tokenCustomers.'&tipo_todo=Richiesta_RMA&tab-container-1=10"><img src="../img/admin/icons/richiesta_rma.gif" alt="Richiesta RMA" title="Richiesta RMA" />&nbsp;&nbsp;&nbsp;Richiesta RMA</a></li>
									</ul>
								</div>

							</li>
							<li class="tasti-apertura-in"><a href="index.php?controller=AdminCustomers&viewcart&editcart&createnew&provvisorio=1&id_customer='.$cart->id_customer.'&viewcustomer&token='.$tokenCustomers.'&tab_name=carts&preventivo=1#modifica-carrello" onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }" id="link-preventivo"><img src="../img/admin/charged_ok.gif" alt="Nuovo preventivo" title="Nuovo preventivo" /> Preventivo</a>';
								
								$orders_telegest = Db::getInstance()->getValue('SELECT count(id_order) FROM '._DB_PREFIX_.'orders WHERE id_customer = '.$cart->id_customer);
								//if($orders_telegest > 0)
								//{
									$orders_telegest_a = Db::getInstance()->executeS('SELECT DISTINCT id_order FROM '._DB_PREFIX_.'orders WHERE id_customer = '.$cart->id_customer);
									foreach($orders_telegest_a as $ot)
									{
										$orders_t_string .= ($otz % 9 == 0 ? '</tr><tr>' : '').'<td><input type="checkbox" class="orders_telegest_c" name="orders_telegest[]" value="'.$ot['id_order'].'" /> '.$ot['id_order'].'&nbsp;&nbsp;&nbsp;&nbsp;</td>';
										$otz++;
									}	
										
									$html .= '
										<div class="hider" id="hider_telegest_form3" style="display:none"></div>
										<div class="popup_box" id="popup_telegest_form3" style="display:none; width:50%; text-align:center; z-index:999999999"><form method="post" action="index.php?controller=AdminCustomers&viewcart&editcart&provvisorio=1&id_customer='.$cart->id_customer.'&amp;copy_to='.$cart->id_customer.'&copia_carrello_conferma=y&id_cart=84366&viewcustomer&token='.$tokenCustomers.'&tab_name=carts&preventivo=1&rif_prev='.$rif.'#modifica-carrello"> Seleziona gli ordini su cui applicare la telegestione: <br /><table><tr>'.$orders_t_string.'</tr></table><br />
										<input type="checkbox" onchange="$(\'.orders_telegest_c\').not(this).prop(\'checked\', this.checked);" />Seleziona tutti<br /><br />
										<input type="submit" class="button" value="Conferma" />
										
										<button type="button" class="button"  onclick="$(\'#hider_telegest_form3\').hide(); $(\'#popup_telegest_form3\').hide();">Chiudi</button>
										
										</form>
										</div>
										<div class="menu-apertura-in" style="position:absolute; left:100px">
											<ul class="dropdown">
												<li><a style="color:#000; cursor:pointer" onclick="$(\'#hider_telegest_form3\').show(); $(\'#popup_telegest_form3\').show(); "><img src="../img/admin/icons/attivita.gif" alt="Telegestione annuale" title="Telegestione annuale" />&nbsp;&nbsp;&nbsp;Telegestione annuale</a></li>
											</ul>
										</div>
									';
								//}
								$html .= '</li>
							
							<li><a href="index.php?controller=AdminCustomers&viewcart&editcart&createnew&provvisorio=1&id_customer='.$cart->id_customer.'&viewcustomer&token='.$tokenCustomers.'&tab_name=carts&preventivo=2#modifica-carrello" onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }" id="link-ordine-manuale"><img src="../img/admin/cart.gif" alt="Nuovo ordine manuale" title="Nuovo ordine manuale" /> Ordine manuale</a></li>
							
							<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&nuovobdl&token='.$tokenCustomers.'&tab-container-1=14"><img src="../img/admin/prefs.gif" alt="Buono di lavoro" title="Buono di lavoro" />&nbsp;&nbsp;&nbsp;Buono di lavoro (BDL)</a></li>
							
						</ul>
					</div>
				</div>';*/
				
				if(Tools::getIsset('id_customer_thread') || Tools::getIsset('id_thread') || Tools::getIsset('id_mex') || Tools::getIsset('id_action') || Tools::getIsset('id_order') || Tools::getIsset('id_cart')) {

					if(Tools::getIsset('id_customer_thread'))
						$rif = 'T'.Tools::getValue('id_customer_thread');
					else if(Tools::getIsset('id_thread'))
						$rif = 'P'.Tools::getValue('id_thread');
					else if(Tools::getIsset('id_mex'))
						$rif = 'T'.Tools::getValue('id_mex');
					else if(Tools::getIsset('id_action'))
						$rif = 'A'.Tools::getValue('id_action');
					else if(Tools::getIsset('id_order'))
						$rif = 'O'.Tools::getValue('id_order');
					else if(Tools::getIsset('id_cart'))
						$rif = 'C'.Tools::getValue('id_cart');
					
					/*$html .= '
					<div class="tasti-apertura">
						<a style="display:block" class="button" href="#"><img src="../img/admin/tab-groups.gif" alt="Aggiungi figlia" title="Aggiungi figlia" />&nbsp;&nbsp;&nbsp;Aggiungi azione figlia</a>
						<div class="menu-apertura">
							<ul class="dropdown">
								<li class="tasti-apertura-in"><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewticket&aprinuovoticket&riferimento='.$rif.'&token='.$tokenCustomers.'&tab-container-1=6"><img src="../img/admin/AdminTools.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;Ticket</a>
									<div class="menu-apertura-in" style="position:absolute; left:100px">
										<ul class="dropdown">
											<li><a style="color:#000" href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewticket&aprinuovoticket&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo-ticket=2&tab-container-1=6"><img src="../img/admin/icons/contabilita.gif" alt="Contabilita" title="Contabilita" />&nbsp;&nbsp;&nbsp;Contabilit&agrave;</a></li>
											
											<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewticket&aprinuovoticket&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo-ticket=8&tab-container-1=6"><img src="../img/admin/icons/ordini.gif" alt="Ordini" title="Ordini" />&nbsp;&nbsp;&nbsp;Ordini</a></li>
											
											<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewticket&aprinuovoticket&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo-ticket=4&tab-container-1=6"><img src="../img/admin/icons/assistenza.gif" alt="Assistenza tecnica" title="Assistenza tecnica" />&nbsp;&nbsp;&nbsp;Assistenza tecnica</a></li>
											
											<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewticket&aprinuovoticket&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo-ticket=9&tab-container-1=6"><img src="../img/admin/icons/rma.gif" alt="RMA" title="RMA" />&nbsp;&nbsp;&nbsp;RMA</a></li>
											
											<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewticket&aprinuovoticket&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo-ticket=3&tab-container-1=6"><img src="../img/admin/icons/rivenditori.gif" alt="Rivenditori" title="Rivenditori" />&nbsp;&nbsp;&nbsp;Rivenditori</a></li>
											
										</ul>
									</div>
								</li>
								<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewmessage&aprinuovomessaggio&riferimento='.$rif.'&token='.$tokenCustomers.'&tab-container-1=7"><img src="../img/admin/email.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;Messaggio</a></li>
								<li class="tasti-apertura-in"><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$tokenCustomers.'&tab-container-1=10"><img src="../img/admin/return.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;To-do</a>
									<div class="menu-apertura-in" style="position:absolute; left:100px">
										<ul class="dropdown">
											<li><a style="color:#000" href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo_todo=Attivita&tab-container-1=10"><img src="../img/admin/icons/attivita.gif" alt="Attivita" title="Attivita" />&nbsp;&nbsp;&nbsp;Attivit&agrave;</a></li>
											
											<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo_todo=Telefonata&tab-container-1=10"><img src="../img/admin/icons/telefonata.gif" alt="Telefonata" title="Telefonata" />&nbsp;&nbsp;&nbsp;Telefonata</a></li>
											
											<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo_todo=Visita&tab-container-1=10"><img src="../img/admin/icons/visita.gif" alt="Visita" title="Visita" />&nbsp;&nbsp;&nbsp;Visita</a></li>
											
											<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo_todo=Caso&tab-container-1=10"><img src="../img/admin/icons/caso.gif" alt="Caso" title="Caso" />&nbsp;&nbsp;&nbsp;Caso</a></li>
											
											<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo_todo=Intervento&tab-container-1=10"><img src="../img/admin/icons/intervento.gif" alt="Intervento" title="Intervento" />&nbsp;&nbsp;&nbsp;Intervento</a></li>
											
											<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo_todo=Richiesta_RMA&tab-container-1=10"><img src="../img/admin/icons/richiesta_rma.gif" alt="Richiesta RMA" title="Richiesta RMA" />&nbsp;&nbsp;&nbsp;Richiesta RMA</a></li>
										</ul>
									</div>
								</li>
								<li class="tasti-apertura-in"><a href="index.php?controller=AdminCustomers&viewcart&editcart&createnew&provvisorio=1&id_customer='.$cart->id_customer.'&viewcustomer&token='.$tokenCustomers.'&tab_name=carts&preventivo=1&rif_prev='.$rif.'#modifica-carrello" onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }" id="link-preventivo"><img src="../img/admin/charged_ok.gif" alt="Nuovo preventivo" title="Nuovo preventivo" /> Preventivo</a>
								';
								
								$orders_telegest = Db::getInstance()->getValue('
									SELECT count(id_order) 
									FROM '._DB_PREFIX_.'orders 
									WHERE id_customer = '.$cart->id_customer
								);

								//if($orders_telegest > 0) {
									$orders_telegest_a = Db::getInstance()->executeS('
										SELECT DISTINCT id_order 
										FROM '._DB_PREFIX_.'orders 
										WHERE id_customer = '.$cart->id_customer
									);

									foreach($orders_telegest_a as $ot)
									{
										
										$orders_t_stringb .= ($otzb % 9 == 0 ? '</tr><tr>' : '').'<td><input type="checkbox" class="orders_telegest_c" name="orders_telegest[]" value="'.$ot['id_order'].'" /> '.$ot['id_order'].'&nbsp;&nbsp;&nbsp;&nbsp;</td>';
										$otzb++;
									}
										
									$html .= '
										<div class="hider" id="hider_telegest_form4" style="display:none"></div>
										<div class="popup_box" id="popup_telegest_form4" style="display:none; width:50%; text-align:center; z-index:999999999"><form method="post" action="index.php?controller=AdminCustomers&viewcart&editcart&provvisorio=1&id_customer='.$cart->id_customer.'&amp;copy_to='.$cart->id_customer.'&copia_carrello_conferma=y&id_cart=84366&viewcustomer&token='.$tokenCustomers.'&tab_name=carts&preventivo=1&rif_prev='.$rif.'#modifica-carrello"> Seleziona gli ordini su cui applicare la telegestione: <br /><table><tr>'.$orders_t_stringb.'</tr></table><br /><br />
										
										<input type="submit" class="button" value="Conferma" />
										
										<button type="button" class="button"  onclick="$(\'#hider_telegest_form4\').hide(); $(\'#popup_telegest_form4\').hide();">Chiudi</button>
										
										</form>
										</div>
										<div class="menu-apertura-in" style="position:absolute; left:100px">
											<ul class="dropdown">
												<li><a style="color:#000; cursor:pointer" onclick="$(\'#hider_telegest_form4\').show(); $(\'#popup_telegest_form4\').show(); "><img src="../img/admin/icons/attivita.gif" alt="Telegestione annuale" title="Telegestione annuale" />&nbsp;&nbsp;&nbsp;Telegestione annuale</a></li>
											</ul>
										</div>
									';
								//}

								$html .= '
								</li> 
								
								<li><a href="index.php?controller=AdminCustomers&viewcart&editcart&createnew&provvisorio=1&id_customer='.$cart->id_customer.'&viewcustomer&token='.$tokenCustomers.'&tab_name=carts&preventivo=2&rif_prev='.$rif.'#modifica-carrello" onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }" id="link-ordine-manuale"><img src="../img/admin/cart.gif" alt="Nuovo ordine manuale" title="Nuovo ordine manuale" /> Ordine manuale</a></li> 
								
								<li><a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&nuovobdl&token='.$tokenCustomers.'&riferimento='.$rif.'&tab-container-1=14"><img src="../img/admin/prefs.gif" alt="Buono di lavoro" title="Buono di lavoro" />&nbsp;&nbsp;&nbsp;Buono di lavoro (BDL)</a></li>
							</ul>
						</div>
					</div>';*/
				}
				
				$html .= '
				
				';
			
				$html .= '
				<script type="text/javascript" src="../js/yetii.js"></script>
				<script type="text/javascript" src="../js/tooltipster.bundle.min.js"></script>
				<link type="text/css" rel="stylesheet" href="../css/tooltipster.bundle.min.css" />
				<div id="tab-container-cart" style="background-color:#fffff !important">
				<ul id="tab-container-cart-nav" class="tab-containers-nav">
				<li><a href="#cart-1"><strong>Generale</strong></a></li>
				<li><a href="#cart-3"><strong>Gerarchia</strong></a></li>
				<li><a href="#cart-4"><strong>Storico</strong></a></li>
				</ul>
				';
				
				if($preventivo == 1 && $visualizzato == 1)
					$html .= '<strong style="color:green">Il cliente ha visualizzato il preventivo</strong><br />';
				
				$html .= '
				<div class="yetii-cart" id="cart-1">
					<table><tr>
				';
				$html .= '<td style="width:70px">Carrello n.</td><td><input type="text" id="id_cart" style="width:150px; margin-right:20px" 	readonly="readonly" name="id" value="'.$cart->id.'-'.$revisioni.'" /></td>';
				$html .= '<td style="width:70px">Creato il</td><td><input type="text" id="creato_il" style="width:130px; margin-right:20px"  	readonly="readonly" name="creato_il" value="'.date("d/m/Y H:i:s", strtotime($cart->date_add)).'" /></td>';
				$html .= '<td style="width:70px">Creato da</td><td><input type="text"  readonly="readonly" id="creato_da "style="width:130px"  	name="creato_da" value="'.($creato_da == 0 ? 'Cliente' : ''.$creato_da_nome).'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
				$html .= '<td style="width:160px">
					<div style="float:left">
						<script type="text/javascript">
							function saveCreaRevisione() {	
								$("#risposta_feedback").html("<img src=\"../img/loader.gif\" />").show();
								
								$.post("ajax.php", {submitCreaRevisione:1,id_employee:'.$context->employee->id.',id_cart:'.Tools::getValue('id_cart').'}, function (res) {
								$("#risposta_feedback").html("").hide();
								
								var r = res.split(";"); 
								
								if (r[0] == "ok")
								{
									$("#risposta_feedback").html("<img src=\"../img/admin/enabled.gif\" />").fadeIn(400);
								}
								else if (r[0] == "error:validation")
									$("#risposta_feedback").html("<b style=\"color:red\">'.addslashes($this->l('Errore')).'</b>").fadeIn(400);
								else if (r[0] == "error:update")
									$("#risposta_feedback").html("<b style=\"color:red\">'.addslashes($this->l('Errore')).'</b>").fadeIn(400);
								$("#risposta_feedback").fadeOut(3000);
								
								$("#cart_id_span").html("'.Tools::getValue('id_cart').'-"+r[1]);
								
								if(r[1] == 1)
								{
									var onclick_apri = \'var id_rev = document.getElementById("revisioni").value; if(id_rev == "totrevisioni") { window.open("index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&tab_name=carts&id_cart='.$cart->id.'&viewcart'.(Tools::getIsset('id_customer') ? '&id_customer='.Tools::getValue('id_customer').'&viewcustomer' : '').'&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($context->employee->id)).'"); } else { window.open("index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&tab_name=carts&id_cart='.$cart->id.'&viewcart&vedirevisione="+id_rev+"'.(Tools::getIsset('id_customer') ? '&id_customer='.Tools::getValue('id_customer').'&viewcustomer' : '').'&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($context->employee->id)).'"); }\';
									$("#select-revisioni").html("<select name=\'revisioni\' id=\'revisioni\' style=\'width:75px; height:25px; display:block; float:left\'>"+r[3]+"</select><a href=\'#\' class=\'button\' style=\'display:block; float:left\' onclick=\'"+onclick_apri+"\' >Apri</a>").fadeIn(400);
								}
								else
								{
									$("#revisioni option[value=\'totrevisioni\']").remove();
									$("#revisioni").append(r[3]);
								}
							});
						}
					</script></div>
					'.($preventivo == 999 ? '' : '<a class="button" href="ajax.php?id_cart='.$cart->id.'&getPDF=y&token='.$this->token.'"  onclick="window.onbeforeunload = null" style="display:block; text-align:center; width:145px">PDF ultima off. salvata</a>').'
					
				</td><td></td>
				';
				$html .= '</tr>';

				$ultima_mod_imp = Db::getInstance()->getValue('
					SELECT date_upd 
					FROM carrelli_creati 
					WHERE id_cart = '.Tools::getValue('id_cart')
				);

				if($ultima_mod_imp == '' || !$ultima_mod_imp)
					$ultima_mod_imp = $cart->date_upd;
				
				$html .= "<tr>";
				$html .= '<td>Cliente</td><td><input type="text" id="cliente" style="width:150px" readonly="readonly" name="cliente" value="'.(($customer->id) ? ($customer->is_company == 1 ? $customer->company: $customer->firstname.' '.$customer->lastname) : ('Guest')).'" /></td>';
				$html .= '<td>Ultima mod.</td><td><input type="text" id="ultima_mod" style="width:130px"  readonly="readonly" name="ultima_mod" value="'.(Tools::getIsset('vedirevisione') ? date("d/m/Y H:i:s", strtotime($date_upd)) : date("d/m/Y H:i:s", strtotime($ultima_mod_imp))).'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
				$html .= '<td>Mod. da</td><td><input type="text"  readonly="readonly" id="modificato_da" style="width:130px"  name="modificato_da" value="'.$nome_impiegato.'" /></td>';
				$html .= '<td style="width:70px" >
				'.($order->id > 0 ? '' : '
				<button type="button" class="button" style="cursor:pointer; display: block; width:153px"; onclick="var surec = window.confirm(\'Sei sicuro/a?\'); if (surec) { document.getElementById(\'crea_una_revisione\').checked = true; $(\'#Apply\').trigger(\'click\'); } else { return false; }"><img src="../img/admin/AdminBackup.gif" alt="Revisione" title="Revisione" />&nbsp;Crea revisione</button>').'
				</td><td></td>';				
				$html .= '</tr>';
				$html .= '</table>';
			/* Display customer information */

			/*
			$html .= '<fieldset style="margin-top:-30px; background-color:#ffffff; border:0px">';
			//	<legend><img src="../img/admin/tab-customers.gif" /> '.('Customer information').'</legend>
				$html .= '<span style="font-weight: bold; font-size: 14px;">';
				if ($customer->id) {
					$html .= '
				<a href="?controller=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($context->employee->id)).'"> '.($customer->is_company == 1 ? $customer->company: $customer->firstname.' '.$customer->lastname).'</a></span> ('.('#').$customer->id.') - 
				<strong>Gruppo</strong>: '.($customer->id_default_group == 3 ? 'Rivenditori' : 'Clienti web').' - <strong>Email</strong>: <a href="mailto:'.$customer->email.'">'.$customer->email.'</a> - ';
				
				$customer_phone = Db::getInstance()->getRow("SELECT	phone, phone_mobile FROM address WHERE id_customer = ".$customer->id." AND deleted = 0 AND fatturazione = 1 AND active = 1");
				
				if($customer_phone['phone'] == '') { $telefono_cliente = $customer_phone['phone_mobile']; } else { $telefono_cliente = $customer_phone['phone']; }
				
				$html .= '<strong>Telefono</strong>: '.$telefono_cliente.'';
				
			
				} else {
					$html .= ('Guest not registered').'</span>';
				}
			$html .= '</fieldset>';*/
		

			/* Display order information */
		
			
				
			if ($order->getTaxCalculationMethod() == PS_TAX_EXC)
			{
				$total_products = $summary['total_products'];
				$total_discount = $summary['total_discounts_tax_exc'];
				$total_wrapping = $summary['total_wrapping_tax_exc'];
				$total_price = $summary['total_price_without_tax'];
				$total_shipping = $summary['total_shipping_tax_exc'];
			} else {
				$total_products = $summary['total_products_wt'];
				$total_discount = $summary['total_discounts'];
				$total_wrapping = $summary['total_wrapping'];
				$total_price = $summary['total_price'];
				$total_shipping = $summary['total_shipping'];
			}
			
			
			
			
			if ($order->id) {				
				/*$html .= '
					<span style="font-weight: bold; font-size: 14px;">';
					if ($order->id)
						$html .= '
					<a href="?controller=AdminOrders&id_order='.(int)($order->id).'&vieworder&token='.Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).(int)($context->employee->id)).'"> '.('Order #').sprintf('%06d', $order->id).'</a></span>
					
					'.('Made on:').' '.Tools::displayDate($order->date_add, (int)$context->language->id, true).'';
					else
						$html .= ('No order created from this cart').'</span>';
					$originale = Db::getInstance()->getValue("SELECT count(*) FROM carrelli_creati WHERE id_cart = ".$_GET['id_cart']."");
					
					$html .= '</div><br /><br />';
				*/
			}
			else {
				include_once('functions.php');
				//$this->includeDatepickerZ(array('validita','data_ordine_mepa', 'scadenza', 'competenza_dal', 'competenza_al', 'decorrenza'), true);
				
				//	$date_validita = date('d/m/Y', strtotime($Date. ' + 15 days'));
				
				//	<legend><img src="../img/admin/cart.gif" /> '.('Informazioni sul carrello').'</legend>';
			
			$html .= '
				<form name="products" id="products" method="post" action="'.self::$currentIndex.'&id_cart='.$_GET['id_cart'].'&id_customer='.$customer->id.'&viewcustomer&viewcart&cartupdated&tab_name=carts&token='.$this->token.'#modifica-carrello" enctype="multipart/form-data">
				';
				$html .= "<table><tr>";
				$html .= '<td style="width:70px">Oggetto</td><td><input type="text" id="name" style="width:150px; margin-right:20px" name="name" value="'.$name.'" /> '.($order->id == 0 ? ' <div style="display:none"><input name="crea_una_revisione" id="crea_una_revisione" type="checkbox" /> Crea revisione</div>' : '').'</td>';
				$html .= '<td style="width:70px">Validit&agrave;</td><td><input type="hidden" value="" id="price_updates" name="price_updates" /><input type="text" id="validita" style="width:130px; margin-right:20px"  name="validita" value="'.(((isset($validita) && $validita != '0000-00-00 00:00:00' && $validita != '1942-01-01')) ? date("d/m/Y", strtotime($validita)) : date("d/m/Y", strtotime($Date .'+15 days'))).'" /></td>';
				$html .= '<td style="width:70px">Consegna</td><td><input type="text" id="consegna" style="width:130px"  name="consegna" value="'.(!empty($consegna_tempi) ? $consegna_tempi : ($preventivo == 1 ? "2-5 gg." : "")).'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';	
				$html .= '<td style="width:70px; position:relative"><div>Rif. ord. 
				 <select style="position:absolute; top:1px; height:22px; width:105px; left:50px" onchange="this.nextElementSibling.value=this.value">
					<option value=""></option>
					<option value="NC">NC</option>
				</select>
				<input type="text" name="rif_ordine" id="rif_ordine" value="'.$rif_ordine.'" style="position:absolute; top:2px; left:51px; border:0px; width:77px" />
				
				
				</div></td></tr>';
				
				/*if($ipa == '')
					$ipa = Db::getInstance()->getValue('SELECT codice_univoco FROM '._DB_PREFIX_.'customer WHERE id_customer = '.Tools::getValue('id_customer'));
				*/
				
				$html .= "<tr>";
				$html .= '<td style="width:70px">CIG</td><td><input type="text" id="cig" style="width:150px; margin-right:20px" name="cig" value="'.$cig.'" /></td>';
				$html .= '<td style="width:70px">CUP</td><td><input type="text" id="cup" style="width:130px; margin-right:20px" name="cup" value="'.$cup.'" /></td>';
				$html .= '<td style="width:70px">IPA <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Codice univoco ufficio per fatturazione elettronica" title="Codice univoco ufficio per fatturazione elettronica" /></td><td><input type="text" id="ipa" style="width:130px; margin-right:20px" name="ipa" value="'.$ipa.'" /></td>';
				
				
				$html .= '<td>Data ordine <input type="text" style="width:70px" name="data_ordine_mepa" id="data_ordine_mepa" value="'.(((isset($data_ordine_mepa) && $data_ordine_mepa != '0000-00-00' && $data_ordine_mepa != '1942-01-01')) ? date("d-m-Y", strtotime($data_ordine_mepa)) : '').'" />
				
				
				</td></tr>';
				
		
				$html .= '
				<td style="width:70px">File </td><td><input name="MAX_FILE_SIZE" value="20000000" type="hidden"><input name="joinFile[]" style="width:150px" class="multi" id="joinFile" type="file" onchange="handleFileSelect(this.files)" multiple></td>
				';
				
				$html .= '<td style="width:70px">In carico a</td>';
				
				$html .= '<td>
				<input type="hidden" name="preventivo" value="'.(Tools::getIsset('preventivo') ? (Tools::getValue('preventivo') == 2 ? 2 : Tools::getValue('preventivo')) : Db::getInstance()->getValue('SELECT preventivo FROM '._DB_PREFIX_.'cart WHERE id_cart = '.$cart->id.'')).'" />
				
				
				<input type="hidden" name="rif_prev" value="'.(Tools::getIsset('rif_prev') ? Tools::getValue('rif_prev') : Db::getInstance()->getValue('SELECT rif_prev FROM '._DB_PREFIX_.'cart WHERE id_cart = '.$cart->id.'')).'" />
				
				<select id="in_carico_a" style="width:140px" name="in_carico_a" >';
				
				$impiegati_ez = Db::getInstance()->ExecuteS("SELECT id_employee, firstname, lastname FROM "._DB_PREFIX_."employee WHERE active = 1 ORDER BY CASE WHEN id_employee = 1 THEN 0 ELSE 1 END, firstname ASC, lastname ASC");

					foreach ($impiegati_ez as $impez) {
					
						//if($impez['id_employee'] != 4 && $impez['id_employee'] != 5 && $impez['id_employee'] != 11 && $impez['id_employee'] != 12)
							$html .= "<option value='".$impez['id_employee']."' ".($in_carico_a == 0 ? ($impez['id_employee'] == $context->employee->id ? 'selected="selected"' : '') : ($in_carico_a == $impez['id_employee'] ? 'selected="selected"' : '' ) )." >".$impez['firstname']." ".substr($impez['lastname'],0,1).".</option>";
					
					}

					$html .= '</select></td>
					';
					
					if($customer->is_company == 1) {
					
				$html .= '<td style="width:70px">Persona</td>';
				
				$html .= '<td><select id="riferimento" style="width:140px" name="riferimento" >';
				$html .= "<option value='0'>--</option>";
				$persone_rif = Db::getInstance()->ExecuteS("SELECT * FROM persone WHERE id_customer = ".$customer->id);

					foreach ($persone_rif as $persona_rif) {
					
							$html .= "<option value='".$persona_rif['id_persona']."' ".($riferimento == $persona_rif['id_persona'] ? 'selected="selected"' : '')." >".$persona_rif['firstname']." ".$persona_rif['lastname']." - ".$persona_rif['phone']."</option>";
					
					}

					$html .= '</select></td>';
					
					
					}
					else 
					{
						$html .= '<td></td><td></td>';
					}
				
				$html .= '<td>';
				
					$template_carrelli = Db::getInstance()->executeS('SELECT id_cart, name FROM '._DB_PREFIX_.'cart WHERE id_customer = 44431 ORDER BY name ASC');
					
					$html .= '<script type="text/javascript">
					function loadTemplate(id)
					{
						var load = "'.(self::$currentIndex.'&id_cart='.Tools::getValue('id_cart').'&viewcart&copyfromtemplatetoexistent=y&id_template="+id+"&id_customer='.$customer->id.'&viewcustomer&conf=4'.'&token='.$this->token).'";
						
						window.location.href = load;
						var premessa = document.getElementById("premessa").value;
						var risorse = document.getElementById("risorse").value;
						var esigenze = document.getElementById("esigenze").value;
						
						$.ajax({
						  url:"ajax.php?load_template=y",
						  type: "POST",
						  data: { id_template: id,
						  id_cart: '.Tools::getValue('id_cart').',
						  premessa: premessa,
						  esigenze: esigenze,
						  risorse: risorse
						  },
						  success:function(resp){
							
							var valori_template = resp.split("|||");
							
							$("#name").val(valori_template[0]);
							
							$(tinymce.editors[\'note\'].getBody()).html(valori_template[1]);
						  },
						  error: function(xhr,stato,errori){
							 alert("Errore ");
						  }
						});
						
						
					}
					</script>';

					$html .= '<select style="width:155px" onchange="var surec = window.confirm(\'Se selezioni un template, le note attuali saranno sovrascritte e i prodotti saranno cancellati e sostituiti da quelli del carrello tipo (tranne che in caso di PREVENTIVO BASE). Sei sicuro/a?\'); if (surec) { loadTemplate(this.value); return true; } else { return false; }"  name="template_carrello1" id="template_carrello1"><option value="">-- Template --</option>';
					foreach($template_carrelli as $template) 
					{
						$html .= '<option value="'.$template['id_cart'].'" '.($name == $template['name'] ? 'selected="selected"' : '').'>'.$template['name'].'</option>';
						
					}
					$html .= '</select>';
				
				
				
				
				$html .= '</td>';


				
					$html .= '</tr>';
					
					$html .= "<tr>";
				$html .= '<td style="width:70px">Esito</td><td>
				
				<select id="esito" style="width:160px; margin-right:20px" name="esito">
				<option value="0" '.($esito == '0' || $esito == '' ? 'selected="selected"' : '').'> -- Seleziona -- </option>
				<option value="1" '.($esito == 1 ? 'selected="selected"' : '').'>Convertito</option>
				<option value="2" '.($esito == 2 ? 'selected="selected"' : '').'>Annullato</option>
				<option value="3" '.($esito == 3 ? 'selected="selected"' : '').'>Perso</option>
				<option value="4" '.($esito == 4 ? 'selected="selected"' : '').'>Rimandato</option>
				</select>
				
				</td>';
				$html .= '<td style="width:70px">Causa</td><td><select id="causa" style="width:140px; margin-right:20px" name="causa">
				<option value="" '.($causa == '' ? 'selected="selected"' : '').'> -- Seleziona -- </option>
				<option value="Stesso prodotto con miglior prezzo" '.($causa == 'Stesso prodotto con miglior prezzo' ? 'selected="selected"' : '').'>Stesso prodotto con miglior prezzo</option>
				<option value="Altro prodotto miglior prezzo" '.($causa == 'Altro prodotto miglior prezzo' ? 'selected="selected"' : '').'>Altro prodotto miglior prezzo</option>
				<option value="Altro prodotto" '.($causa == 'Altro prodotto' ? 'selected="selected"' : '').'>Altro prodotto</option>
				<option value="Requisiti tecnici" '.($causa == 'Requisiti tecnici' ? 'selected="selected"' : '').'>Requisiti tecnici</option>
				<option value="Prova NOK" '.($causa == 'Prova NOK' ? 'selected="selected"' : '').'>Prova NOK</option>
				</select>
				</td>';
				$html .= '<td style="width:70px; ">Concorrente</td><td style="position:relative">
				
				 <select style="position:absolute; top:1px; height:22px; width:140px" onchange="this.nextElementSibling.value=this.value">
					<option value=""></option>
					<option value="Onedirect">Onedirect</option>
					<option value="Allnet">Allnet</option>
					<option value="eBay">eBay</option>
					<option value="Amazon">Amazon</option>
				</select>
				<input type="text" name="concorrente" id="concorrente" value="'.$concorrente.'" style="position:absolute; top:2px; left:2px; border:0px; width:110px" />
				</td>';
				
				
				$html .= '<td>	<a href="javascript:void(0)" style="cursor:pointer; display: block; width:146px" class="button" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { resetThisForm(); } else { return false; }"> <img src="../img/admin/quick.gif" alt="Pulisci form" title="Pulisci form" />&nbsp;Pulisci form </a>	
				</td></tr>';
					
					$html .= '</table>
					';
					
				$fatturazione = Db::getInstance()->getValue("SELECT id_address_invoice FROM "._DB_PREFIX_."cart WHERE id_cart = ".$_GET['id_cart']."");
				
				// Correggere: errore se $consegna = null (cioè $cart->id_address_delivery = 0) -> obbligare a selezionare un indirizzo
				$consegna = Db::getInstance()->getValue("SELECT id_address_delivery FROM "._DB_PREFIX_."cart WHERE id_cart = ".$_GET['id_cart']."");
				
				$indirizzi_di_fatturazione = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."address WHERE id_customer = ".$customer->id." AND fatturazione = 1 AND deleted = 0 AND active = 1  AND id_country != 0 ORDER BY id_address ASC");
				
				$indirizzi_di_consegna = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."address WHERE id_customer = ".$customer->id." AND deleted = 0 AND active = 1  AND id_country != 0 ORDER BY fatturazione DESC, id_address ASC");
				
				$html .= '
				<script type="text/javascript">
		var stopKeyUp = false;
		
		</script>
				<output id="list-tkt"></output>
				<script type="text/javascript" src="jquery.MultiFile.js"></script>
									<script type="text/javascript">
								function handleFileSelect(files) {
								
									// Loop through the FileList and render image files as thumbnails.
									 for (var i = 0; i < files.length; i++) {
										var f = files[i];
										var name = files[i].name;
			  
										var reader = new FileReader();  
										reader.onload = function(e) {  
										  // Render thumbnail.
										  var span = document.createElement("span");
										  document.getElementById("list-tkt").innerHTML += ["<a href=\'",e.target.result,"\' target=\'_blank\'>",name,"</a> - "].join("");
										};

									  // Read in the image file as a data URL.
									  reader.readAsDataURL(f);
									}
								  }
								 
								</script>';
				
								
				if($attachment != '') {
					$html .= '<div style="float:left">Allegati:</div> ';
					if(!empty($attachment)) {
								
						$allegati = explode(";",$attachment);
						$nall = 1;
						foreach($allegati as $allegato) {
							
							if(strpos($allegato, ":::")) {
							
								$parti = explode(":::", $allegato);
								$nomevero = $parti[1];
											
							}
							
							else {
								$nomevero = $allegato;
							
							}
										
							if($allegato == "") { } else {
								if($nall == 1) { } else { $html .= " - "; }
								$html .= '<div id="all_'.$parti[0].'" style="float:left"><a href="ajax.php?controller=AdminCarts&id_cart='.$cart->id.'&viewcart&token='.$this->token.'&filename='.$allegato.'"><span style="color:red">'.$nomevero.'</span></a> <a href="javascript:void(0)" onclick="var surec = window.confirm(\'Sei sicuro/a?\'); if (surec) { cancella_allegato(\''.$allegato.'\', \''.$parti[0].'\') } else { return false; } "><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a></div>';
								$nall++;
							}
						}
						
						if (strpos($template_cart, 'zcloud') == true || strpos($template_cart, 'EASTAR-CLOUD') == true )
						{
							
							$ezcloud_nr = Db::getInstance()->getValue('SELECT id_cart FROM '._DB_PREFIX_.'cart_ezcloud WHERE id_cart = '.Tools::getValue('id_cart'));
							$allegato_a = Db::getInstance()->getValue('SELECT allegato_a FROM '._DB_PREFIX_.'cart_ezcloud WHERE id_cart = '.Tools::getValue('id_cart'));
							
							if($ezcloud_nr == Tools::getValue('id_cart'))
							{
								$html .= '<a href="https://www.ezdirect.it/ezadmin/ajax.php?id_cart='.Tools::getValue('id_cart').'&viewcart&getPDFezcloudA=y&token=f02b6e90c31e449237e6e96e1dca037d">Allegato A</a> <input type="checkbox" name="allegato_a" '.($allegato_a == 1 ? 'checked="checked"' : '').' /> (spunta per inviarlo)';
							}
							else
							{
								$html .= '<strong>Il PDF dell\'allegato A verr&agrave; prodotto dopo il salvataggio</strong>';
							}
							
						}
						$html .= '<div class="clear"></div>';
								
					}
								
								else { $html .= 'Nessun allegato'; }
				}
				if($attachment != '') { $html .= '<br />';
				
				$html .= '<script type="text/javascript">
				function cancella_allegato(nome, identificativo)
				{
					$.ajax({
					  url:"ajax.php?cancella_allegato_carrello=y",
					  type: "POST",
					  data: { nome: nome,
					  id_cart: '.Tools::getValue('id_cart').'
					  },
					  success:function(resp){
					  
						$("#all_"+identificativo).hide();
						
						// alert("Allegato cancellato con successo");
					  },
					  error: function(xhr,stato,errori){
						 alert("Errore ");
					  }
					});
				}
				</script>';
				
				
				// $html .= '<input name="cancella_tutti_allegati" type="checkbox" /> Cancella tutti gli allegati';
				
			
				}
				
				$html .= '<br />
				<table style="margin-bottom:-20px">
				<tr>
				<td><strong>Fatturazione: </strong></td><td> <select name="id_address_invoice" style="width:321px" onchange="document.getElementById(\'inv_mod\').href = \'?controller=AdminAddresses&id_address=\'+this.value+\'&updateaddress&token='.$tokenAddresses.'\'">';
				
				foreach ($indirizzi_di_fatturazione as $inv_a) {
					$html .= '<option value="'.$inv_a['id_address'].'" '.($fatturazione == $inv_a['id_address'] ? 'selected="selected"' : '').'>'.$inv_a['address1'].' - '.$inv_a['city'].'</option>';
				}
				
				$html .= '</select>';

				//$html .= '<a style="cursor:pointer" onclick="window.open(\'?controller=AdminAddresses&addaddress&fatturazione=1&idclientee='.$customer->id.'&back='.urlencode($_SERVER['REQUEST_URI']).'&token='.$tokenAddresses.'\', \'blank\', \'height=600,width=1200,modal=yes,alwaysRaised=yes,scrollbars=yes\');" ><img src="../img/admin/add.gif" alt="" title="" /></a>';
				
				$html .= ' <a style="float:right" target="_blank" href="index.php?controller=AdminAddresses&addaddress&id_customer='.$customer->id.'&tipo=1&token='.$tokenAddresses.'"><img src="../img/admin/add.gif" alt="" title="" /></a>
				
				<a style="float:right; margin-left:5px" id="inv_mod" target="_blank" href="index.php?controller=AdminAddresses&id_address='.($fatturazione > 0 ? $fatturazione : $inv_a['id_address']).'&updateaddress&token='.$tokenAddresses.'" ><img src="../img/admin/edit.gif" /></a>';
				
				$cons_default = 0;
				$html .= '</td>
				<td><strong>Consegna: </strong></td><td> <select name="id_address_delivery" id="id_address_delivery" onchange="calcolaSpedizionePerIndirizzo(); document.getElementById(\'del_mod\').href = \'?controller=AdminAddresses&id_address=\'+this.value+\'&updateaddress&token='.$tokenAddresses.'\'" style="width:322px">';
				
				foreach ($indirizzi_di_consegna as $del_a) {	
					if($cons_default == 0)
						$cons_default = $del_a['id_address'];
					
					$html .= '<option value="'.$del_a['id_address'].'" '.($consegna == $del_a['id_address'] ? 'selected="selected"' : '').'>'.$del_a['address1'].' - '.$del_a['city'].'</option>';
				}

				$html .= '</select>';
				
				//$html .= '<a style="cursor:pointer" onclick="window.open(\'?controller=AdminAddresses&addaddress&idclientee='.$customer->id.'&back='.urlencode($_SERVER['REQUEST_URI']).'&token='.$tokenAddresses.'\', \'blank\', \'height=600,width=1200,modal=yes,alwaysRaised=yes,scrollbars=yes\');"><img src="../img/admin/add.gif" alt="" title="" /></a>';
				
				$html .= '<a style="float:right" target="_blank" href="index.php?controller=AdminAddresses&addaddress&id_customer='.$customer->id.'&tipo=0&token='.$tokenAddresses.'" ><img src="../img/admin/add.gif" alt="" title="" /></a> 
				<a style="float:right; margin-left:5px" id="del_mod" target="_blank" href="index.php?controller=AdminAddresses&id_address='.($consegna > 0 ? $consegna : $cons_default).'&updateaddress&token='.$tokenAddresses.'"><img src="../img/admin/edit.gif" /></a>';
				
				$html .= '
				</td></tr></table>
				';
				
				if (strpos($template_cart, 'zcloud') == true || strpos($template_cart, 'EASTAR-CLOUD') == true )
				{ 
			
					$ezcloud_dati = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'cart_ezcloud WHERE id_cart = '.$cart->id);
					//$this->includeDatepickerZ(array('data_fatturazione_ezcloud'), true);
					$html .= '<br /><br /><fieldset style="background-color:#ffffff"><legend><img src="http://www.ezdirect.it/img/p/367491-40906-small.jpg" alt="ezcloud" style="width:16px" />Cloud</legend>
					
					Periodicit&agrave; <select name="ezcloud_periodicita"><option value="mese" '.($ezcloud_dati['periodicita'] == 'mese' ? 'selected="selected"' : '').'>Mensile</option><option value="anno" '.($ezcloud_dati['periodicita'] == 'anno' ? 'selected="selected"' : '').'>Annuale</option></select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Interni <input id="ezcloud_interni" name="ezcloud_interni" type="text" style="width:30px" value="'.$ezcloud_dati['interni'].'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Linee <input name="ezcloud_linee" type="text" style="width:30px" value="'.$ezcloud_dati['linee'].'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Canali <input name="ezcloud_canali" type="text" style="width:30px" value="'.$ezcloud_dati['canali'].'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					'.(strpos($template_cart, 'Canone') == true ? 'Canone <input name="ezcloud_canone" type="text" style="width:70px" value="'.$ezcloud_dati['canone'].'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' : '').'
					'.(strpos($template_cart, 'zcloud Bas') == true ? '<input style="display:none"  type="radio" id="ezcloud_basic" name="ezcloud_tipo[]" onclick="document.getElementById(\'ezcloud_altro\').value = \'\'" value="basic" checked="checked" />Tipo: <strong>Basic</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' : (strpos($template_cart, 'EASTAR-CLOUD') == true ? '<input style="display:none"  type="radio" id="yeastar_cloud_5" checked="checked" />' : '<input style="display:none"  type="radio" id="ezcloud_business" name="ezcloud_tipo[]" onclick="document.getElementById(\'ezcloud_altro\').value = \'\'" value="business" checked="checked" />Tipo: <strong>Business</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;')).'
					
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					
					<!-- Fatturare a partire dal: <input id="data_fatturazione_ezcloud" name="data_fatturazione_ezcloud" type="text" value="'.($ezcloud_dati['data_fatturazione_ezcloud'] != '' && $ezcloud_dati['data_fatturazione_ezcloud'] != '0000-00-00' ? date('d-m-Y', strtotime($ezcloud_dati['data_fatturazione_ezcloud'])) : '').'" /> -->
					
					<br /><br />
					Rinnovo automatico <select name="ezcloud_rinnovo"><option value="1"  '.($ezcloud_dati['rinnovo_automatico'] == '1' ? 'selected="selected"' : '').'>1 mese</option><option value="12"  '.($ezcloud_dati['rinnovo_automatico'] == '12' ? 'selected="selected"' : '').'>12 mesi</option></select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					
					
					<input type="checkbox" id="configurazione_iniziale" name="ezcloud_configurazione_iniziale"  '.($ezcloud_dati['configurazione_iniziale'] == '1' ? 'checked="checked"' : '').' />Configurazione iniziale&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" id="ezcloud_attivazione" name="ezcloud_attivazione" '.($ezcloud_dati['attivazione'] == '1' ? 'checked="checked"' : '').' />Attivazione&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="ezcloud_agente" '.($ezcloud_dati['agente'] == '1' ? 'checked="checked"' : '').' />Agente&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

					<a name="genera_ezcloud" id="genera_ezcloud" class="button" onclick="javascript:void(0)">Genera preventivo</a>
					
					
					<script type="text/javascript">
					$(document).ready(function(){
						$("#genera_ezcloud").click(function(){
							delProduct30("367442"); delProduct30("367491"); delProduct30("367642"); delProduct30("367645"); delProduct30("367498"); delProduct30("367499");  
							var excludeIds = document.getElementById("excludeIds").value;
							var arrayescl = excludeIds.split(",");
							if($("#ezcloud_basic").is(":checked")) { 
					
								var prodotti_bundle = "";		

								if(($.inArray("367442",arrayescl) == -1 )) {
									prodotti_bundle += "367442;";
								}	
																
								if($("#ezcloud_attivazione").is(":checked")) { 
										if(($.inArray("367498",arrayescl) == -1 )){
											prodotti_bundle += "367498;";
										}			
								}		
								if($("#configurazione_iniziale").is(":checked")) { 
									if(($.inArray("367642",arrayescl) == -1 )){
										prodotti_bundle += "367642;";
									}
								}		
									prodotti_bundle += "999999";
									prodotti_bundle = prodotti_bundle.split(";");
									for(i=0;i<((prodotti_bundle.length))-1;i++)
									{
										
										$.ajax({
											type: "GET",
											data: "id_cart='.$_GET['id_cart'].'&id_bundle=0&product_in_bundle="+prodotti_bundle[i],
											async: false,
											url: "ajax_products_list2.php",
											success: function(resp)
											{
												addProduct_TR("",resp.split("|"),"");
											},
											error: function(XMLHttpRequest, textStatus, errorThrown)
											{
												tooltip_content = "";
												alert("ERROR" + textStatus + " " + XMLHttpRequest.responseText);					
											}
																	
										});
									}
									if(document.getElementById("product_quantity[367442]").value != null)
									{
										document.getElementById("product_quantity[367442]").value = document.getElementById("ezcloud_interni").value;
										
										
									}
									calcolaImporto(367442);
										
							}
							else if($("#ezcloud_business").is(":checked")) { 
								
								var prodotti_bundle = "";	
								if(($.inArray("367491",arrayescl) == -1 )){
									prodotti_bundle += "367491;";
								}								
								if($("#ezcloud_attivazione").is(":checked")) { 
										if(($.inArray("367499",arrayescl) == -1 )){
											prodotti_bundle += "367499;";
										}			
								}		
								if($("#configurazione_iniziale").is(":checked")) { 
									if(($.inArray("367645",arrayescl) == -1 )){
										prodotti_bundle += "367645;";
									}
								}		
									
									prodotti_bundle += "999999";
									prodotti_bundle = prodotti_bundle.split(";");
									for(i=0;i<((prodotti_bundle.length))-1;i++)
									{
										$.ajax({
											type: "GET",
											data: "id_cart='.$_GET['id_cart'].'&id_bundle=0&product_in_bundle="+prodotti_bundle[i],
											async: false,
											url: "ajax_products_list2.php",
											success: function(resp)
											{
												addProduct_TR("",resp.split("|"),"");
											},
											error: function(XMLHttpRequest, textStatus, errorThrown)
											{
												tooltip_content = "";
												alert("ERROR" + textStatus + " " + XMLHttpRequest.responseText);					
											}
																	
										});
									}
									if(document.getElementById("product_quantity[367491]").value != null)
									{
										document.getElementById("product_quantity[367491]").value = document.getElementById("ezcloud_interni").value;
										
									}
								
									calcolaImporto(367491);
										
									
								
							}	
							else if($("#yeastar_cloud_5").is(":checked")) { 
								
								var prodotti_bundle = "";	
								if(($.inArray("387752",arrayescl) == -1 )){
									prodotti_bundle += "387752;";
								}								
								if($("#ezcloud_attivazione").is(":checked")) { 
										if(($.inArray("367499",arrayescl) == -1 )){
											prodotti_bundle += "367499;";
										}			
								}		
								if($("#configurazione_iniziale").is(":checked")) { 
									if(($.inArray("367645",arrayescl) == -1 )){
										prodotti_bundle += "367645;";
									}
								}		
									
									prodotti_bundle += "999999";
									prodotti_bundle = prodotti_bundle.split(";");
									for(i=0;i<((prodotti_bundle.length))-1;i++)
									{
										$.ajax({
											type: "GET",
											data: "id_cart='.$_GET['id_cart'].'&id_bundle=0&product_in_bundle="+prodotti_bundle[i],
											async: false,
											url: "ajax_products_list2.php",
											success: function(resp)
											{
												addProduct_TR("",resp.split("|"),"");
											},
											error: function(XMLHttpRequest, textStatus, errorThrown)
											{
												tooltip_content = "";
												alert("ERROR" + textStatus + " " + XMLHttpRequest.responseText);					
											}
																	
										});
									}
									if(document.getElementById("product_quantity[387752]").value != null)
									{
										document.getElementById("product_quantity[387752]").value = document.getElementById("ezcloud_interni").value;
										
									}
								
									calcolaImporto(387752);
										
									
								
							}	
						});
						return false;
					});
					
					</script>
					<input type="hidden" name="ezcloud_template" value="1" />
					</fieldset>';
				}
			}
			// List of products
			/*$html .= '
			<br style="clear:both;" />
					';
					*/
					
			if ($order->id) {
							
					$html .= '<table>
					<tr>';
				$html .= '<td style="width:70px">Oggetto</td><td><input type="text" id="name" style="width:150px; margin-right:20px" readonly="readonly" name="name" value="'.$name.'" /></td>';

				
				$html .= '<td style="width:70px">Validit&agrave;</td><td><input type="text" id="validita" style="width:130px;margin-right:20px"  readonly="readonly" name="validita" value="'.((isset($validita) && $validita != '0000-00-00 00:00:00' && $validita != '1942-01-01') ? date("d/m/Y", strtotime($validita)) : $date_validita).'" /></td>';
				$html .= '<td style="width:70px">Consegna</td><td><input type="text"  readonly="readonly" id="consegna"style="width:130px"  name="consegna" value="'.(!empty($consegna_tempi) ? $consegna_tempi : ($preventivo == 1 ? "2-5 gg." : "")).'" /></td>';

				$html .= '<td style="width:70px">Rif. Ord.</td><td><input type="text"  readonly="readonly" id="rif_ordine" "style="width:85px" name="rif_ordine" value="'.$rif_ordine.'" /></td></tr>';
				
				$html .= '<tr>';
				$html .= '<td style="width:70px">CIG</td><td><input type="text" id="cig" style="width:150px; margin-right:20px" readonly="readonly" name="cig" value="'.$cig.'" /></td>';

				
				$html .= '<td style="width:70px">CUP</td><td><input type="text" id="cup" style="width:130px;margin-right:20px"  readonly="readonly" name="cup" value="'.$cup.'" /></td>';
				$html .= '<td style="width:70px">IPA <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Codice univoco ufficio per fatturazione elettronica" title="Codice univoco ufficio per fatturazione elettronica" /></td><td><input type="text"  readonly="readonly" id="ipa" "style="width:130px"  name="ipa" value="'.$ipa.'" /></td>';

				$html .= '<td style="width:70px">Data ordine</td><td><input type="text"  readonly="readonly" id="data_ordine_mepa" "style="width:85px" name="data_ordine_mepa" value="'.$data_ordine_mepa.'" /></td></tr>';
			
				
				$html .= '<tr><td style="width:70px">In carico a</td>';
				
				$html .= '<td><input type="text" readonly="readonly" value="'.($in_carico_a == 0 ? 'Nessuno' : Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$in_carico_a.'')).'" style="width:150px" name="in_carico_a" >';
				
				$history = Db::getInstance()->getValue('SELECT MAX(id_order_history) FROM '._DB_PREFIX_.'order_history WHERE id_order = '.$order->id);
				$order_state = Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'order_state_lang ol JOIN '._DB_PREFIX_.'order_state os ON ol.id_order_state = os.id_order_state JOIN '._DB_PREFIX_.'order_history oh ON oh.id_order_state = os.id_order_state WHERE ol.id_lang = '.$context->language->id.' AND oh.id_order_history = '.$history);

					$html .= '</td><td>Stato ord.</td><td colspan="3"><input type="text" readonly="readonly" style="width:368px" value="'.$order_state.'" /></td></tr>';
					
					$html .= '<tr>';
				$html .= '<td style="width:70px">Esito</td><td><input type="text" id="esito" style="width:150px; margin-right:20px" readonly="readonly" name="esito" value="Convertito" /></td>';

				
				$html .= '<td style="width:70px">Causa</td><td><input type="text" id="causa" style="width:130px;margin-right:20px"  readonly="readonly" name="causa" value="'.$causa.'" /></td>';
				$html .= '<td style="width:70px">Concorrente</td><td><input type="text"  readonly="readonly" id="concorrente" "style="width:130px"  name="concorrente" value="'.$concorrente.'" /></td>';

				$html .= '<td style="width:70px">Data ordine</td><td><input type="text"  readonly="readonly" id="data_ordine_mepa" "style="width:85px" name="data_ordine_mepa" value="'.$data_ordine_mepa.'" /></td></tr>';
				
					
					$html .= '</table>
					';
				
				$fatturazione = Db::getInstance()->getValue("SELECT id_address_invoice FROM "._DB_PREFIX_."cart WHERE id_cart = ".$_GET['id_cart']."");

				// Correggere: errore se $consegna = null (cioè $cart->id_address_delivery = 0) -> obbligare a selezionare un indirizzo
				$consegna = Db::getInstance()->getValue("SELECT id_address_delivery FROM "._DB_PREFIX_."cart WHERE id_cart = ".$_GET['id_cart']."");
				
				$inv_a = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."address WHERE id_address = ".$fatturazione."");
				
				$del_a = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."address WHERE id_address = ".$consegna."");
				
				$html .= '<br />
				<table style="margin-bottom:-20px">
				<tr>
				<td><strong>Fatturazione: </strong></td><td> <input type="text" readonly="readonly" value="'.$inv_a['address1'].' - '.$inv_a['city'].'" name="id_address_invoice" style="width:360px"></td>
				<td><strong>Consegna: </strong></td><td> <input type="text" readonly="readonly" value="'.$del_a['address1'].' - '.$del_a['city'].'" name="id_address_delivery" style="width:351px">';
				
				$html .= '
				</td></tr></table>
				';
				
					$html .= '<br /><br />';
					
				  
				  
				  
			}
			else {
				$html .= '<br />';
				$ctrl_creato_nuovo = Db::getInstance()->getValue("
					SELECT count(id_cart) 
					FROM carrelli_creati 
					WHERE id_cart = ".$_GET['id_cart']."
				");

				/*if($ctrl_creato_nuovo == 0) {}
				else {}*/
				?>
				<?php
			}
			$html .= '
			<script type="text/javascript" src="../js/jquery.tablednd_0_5.js"></script>
				<script type="text/javascript">
				$(document).ready(function() {
						$("#tableProducts").tableDnD({
						onDrop: function(table, row) {
						}
					});
				
					/*
					$("#tableProducts").hover(function() {
						$(this.cells[8]).addClass(\'showDragHandle\');
					}, function() {
						  $(this.cells[8]).removeClass(\'showDragHandle\');
					});
					*/
				});
				</script>
				
			<script type="text/javascript">
			$(document).ready(function() {
			
				$(".wholesale_price_class").each(function() {
				
							var productId = $(this).attr("id").substring($(this).attr("id").indexOf("[") + 1,$(this).attr("id").indexOf("]",$(this).attr("id").indexOf("[") + 1));
							
							var acq = document.getElementById("wholesale_price["+productId+"]"); var last = acq.value;
								
								var ctrl_m_c = 0;
								
								//if(ctrl_m_c == 0) {
								//	$(document.getElementById("wholesale_price["+productId+"]")).one(\'click\', function() {
								//		alert(\'Attenzione: stai cambiando il prezzo di acquisto\');
								//		ctrl_m_c = 1;
								//	});
								//}
								
								
								//if(ctrl_m_c == 0) {
									//$(document.getElementById("wholesale_price["+productId+"]")).one(\'keydown\', function(event) {
									
										//var code = (event.keyCode ? event.keyCode : event.which);
										
										//if(code == 9 || code == 37 || code == 38 || code == 39 || code == 40) {
											
										//}
										//else {
											//alert(\'Attenzione: stai cambiando il prezzo di acquisto\');
											//ctrl_m_c = 1;
										//}
									//});
								//}
								
								
								// Setup the event
								$(document.getElementById("wholesale_price["+productId+"]")).focusout(function() {
									ctrl_m_c = 0;
									if (last != $(this).val()) {
										
										var surec = window.confirm(\'Stai cambiando un prezzo di acquisto. Sei sicuro?\'); 
										if (surec) { return true; } else { document.getElementById("wholesale_price["+productId+"]").value = last; calcolaImporto(productId); }
									}
								});
							});
						
						
					});
					
					
			</script>
			
			<script type="text/javascript">
			function ristabilisciPrezzo(id_product) {
			
				var numprice = document.getElementById(\'product_price[\'+id_product+\']\').value;
				document.getElementById(\'sconto_extra[\'+id_product+\']\').value = 0;
				var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
				var unitario = parseFloat(document.getElementById(\'unitario[\'+id_product+\']\').value);
				unitario = unitario.toFixed(2);
				unitario = unitario.toString();				
				unitario = unitario.replace(".",",");
				document.getElementById(\'product_price[\'+id_product+\']\').value=unitario;
				price=unitario;
			
			}
			function calcolaProvvigione(id_product)
			
			{
				return 0;
				
			}
			function calcolaProvvigione1(id_product)
			{
				var numunitario = document.getElementById(\'unitario[\'+id_product+\']\').value;
				var unitario = parseFloat(numunitario.replace(/\s/g, "").replace(",", "."));
				
				var numsconto_extra = document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".");
				var sconto_extra = parseFloat(numsconto_extra);
				
				var numprice = document.getElementById(\'product_price[\'+id_product+\']\').value;	
				var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
				
				var numacquisto = document.getElementById(\'wholesale_price[\'+id_product+\']\').value;
				var acquisto = parseFloat(numacquisto.replace(/\s/g, "").replace(",", "."));
				
				var mrg = price-acquisto;
				var mrg_prc = (((price-acquisto)*100)/price);
				
				
				if(mrg_prc > 20)
					var mrg_prov = mrg/100*30;
				else
					var mrg_prov = mrg/100*25;
				
					
				var prov = mrg_prov * 100 / price;
				
				
				if(prov < 0)
					prov = 0;
				
				prov = price * (prov/100);
				
				prov = prov.toFixed(2);
				prov = prov.toString();				
				prov = prov.replace(".",",");
				
				
				document.getElementById(\'provvigione[\'+id_product+\']\').value=prov;
				var totale=0;
				var arrayProvvigioni = document.getElementsByClassName("provvigione");
					for (var i = 0; i < arrayProvvigioni.length; ++i) {
						var item = parseFloat(arrayProvvigioni[i].value.replace(/\s/g, "").replace(",", "."));
						totale += item;
						
						
					}
				totale = totale.toFixed(2);
		
				document.getElementById(\'spantotaleprovvigioni\').innerHTML=totale.replace(".",",");
			}
			
			
			function calcolaPrezzoScontoExtra(id_product, tipo) {
			
				var numunitario = document.getElementById(\'unitario[\'+id_product+\']\').value;
			
				var unitario = parseFloat(numunitario.replace(/\s/g, "").replace(",", "."));
				
				if (unitario == 0) {
				unitario = 0.001;
				}
				else {
				}
				
				var quantity = parseFloat(document.getElementById(\'product_quantity[\'+id_product+\']\').value);
				var numsconto_extra = document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".");
				
				var numprice = document.getElementById(\'product_price[\'+id_product+\']\').value;	
				var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
				var sconto_extra = parseFloat(numsconto_extra);
				
				if(sconto_extra == null)
					sconto_extra = 0;
				
				var sc_qta_1 = parseFloat(document.getElementById(\'sc_qta_1[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
				var sc_qta_2 = parseFloat(document.getElementById(\'sc_qta_2[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
				var sc_qta_3 = parseFloat(document.getElementById(\'sc_qta_3[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
				var sc_riv_1 = parseFloat(document.getElementById(\'sc_riv_1[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
				var sc_riv_2 = parseFloat(document.getElementById(\'sc_riv_2[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
				var sc_riv_3 = parseFloat(document.getElementById(\'sc_riv_3[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
				var sc_qta_1_q = parseFloat(document.getElementById(\'sc_qta_1_q[\'+id_product+\']\').value);
				var sc_qta_2_q = parseFloat(document.getElementById(\'sc_qta_2_q[\'+id_product+\']\').value);
				var sc_qta_3_q = parseFloat(document.getElementById(\'sc_qta_3_q[\'+id_product+\']\').value);
				var sc_riv_1_q = parseFloat(document.getElementById(\'sc_riv_1_q[\'+id_product+\']\').value);
				var sc_riv_2_q = parseFloat(document.getElementById(\'sc_riv_2_q[\'+id_product+\']\').value);	
				var sc_riv_3_q = parseFloat(document.getElementById(\'sc_riv_3_q[\'+id_product+\']\').value);	
				var finito = unitario - ((sconto_extra*unitario)/100);
					
				if (document.getElementById(\'usa_sconti_quantita[\'+id_product+\']\').checked == true) {
			
				';	
				if ($customer->id_default_group == 3) {
					$html .= '
						if(sc_riv_1 > unitario || sc_riv_1 == 0)
							sc_riv_1 = unitario;
						
						if(tipo == "sconto") {
							var finito = sc_riv_1 - ((sconto_extra*sc_riv_1)/100);
						}
						else if (tipo == "inverso") {
							var finito = ((sc_riv_1 - price)/sc_riv_1)*100;
						}
					';
				}
				else if ($customer->id_default_group == 12) {
					$html .= '
						if(sc_riv_2 > unitario || sc_riv_2 == 0)
							sc_riv_2 = unitario;
						
						if(tipo == "sconto") {
							var finito = sc_riv_3 - ((sconto_extra*sc_riv_3)/100);
						}
						else if (tipo == "inverso") {
							var finito = ((sc_riv_3 - price)/sc_riv_3)*100;
						}
					';
				}
				if ($customer->id_default_group == 22) {
					$html .= '
						if(sc_riv_2 > 0) {
							if(sc_riv_2 > unitario)
								sc_riv_2 = unitario;
							
							if(tipo == "sconto") {
								var finito = sc_riv_2 - ((sconto_extra*sc_riv_2)/100);
							}
							else if (tipo == "inverso") {
								var finito = ((sc_riv_2 - price)/sc_riv_2)*100;
							}
						}
						else  {
							sc_riv_1 = sc_riv_1 + (sc_riv_1 / 100 * 3);
							if(sc_riv_1 > unitario || sc_riv_1 == 0)
							sc_riv_1 = unitario;
						
							if(tipo == "sconto") {
								var finito = sc_riv_1 - ((sconto_extra*sc_riv_1)/100);
							}
							else if (tipo == "inverso") {
								var finito = ((sc_riv_1 - price)/sc_riv_1)*100;
							}
						}
					';
				}
				else {
					$html .= '
						if(quantity < sc_qta_1_q && sc_qta_1_q != 0) {
							if(tipo == "sconto") {
								var finito = unitario - ((sconto_extra*unitario)/100);
							}
							else if (tipo == "inverso") {
								var finito = ((unitario - price)/unitario)*100;
							}
						}
						else if((quantity >= sc_qta_1_q && quantity < sc_qta_2_q) || (quantity >= sc_qta_1_q && isNaN(parseFloat(sc_qta_2_q)))) {
							
							if(sc_qta_1 > unitario || sc_qta_1 == 0)
								sc_qta_1 = unitario;
							
							if(tipo == "sconto") {
								var finito = sc_qta_1 - ((sconto_extra*sc_qta_1)/100);	
							}
							else if (tipo == "inverso") {
								var finito = ((sc_qta_1 - price)/sc_qta_1)*100;
							}
						}
						else if((quantity >= sc_qta_2_q && quantity < sc_qta_3_q) || (quantity >= sc_qta_2_q && isNaN(parseFloat(sc_qta_3_q == 0)))) {
							
							if(sc_qta_2 > unitario || sc_qta_2 == 0)
								sc_qta_2 = unitario;
							
							if(tipo == "sconto") {
								var finito = sc_qta_2 - ((sconto_extra*sc_qta_2)/100);
							}
							else if (tipo == "inverso") {
								var finito = ((sc_qta_2 - price)/sc_qta_2)*100;
							}
						}
						else if(quantity >= sc_qta_3_q && sc_qta_2_q != 0 && sc_qta_3_q != 0 && sc_qta_1_q != 0) {
							
							if(sc_qta_3 > unitario || sc_qta_3 == 0)
								sc_qta_3 = unitario;
							
							if(tipo == "sconto") {
								var finito = sc_qta_3 - ((sconto_extra*sc_qta_3)/100);					
							}
							else if (tipo == "inverso") {
								var finito = ((sc_qta_3 - price)/sc_qta_3)*100;
							}
						}	
						if(finito > unitario)
							finito = unitario;
					';
				} 

				$html .= '				
				}
				else {
						var finito = unitario - ((sconto_extra*unitario)/100);
						
						if (tipo == "sconto") {
							var finito = unitario - ((sconto_extra*unitario)/100);
						}
						else if (tipo == "inverso") {
							var finito = ((unitario - price)/unitario)*100;
						}
					}
					
			
					finito = finito.toFixed(2);
					finito = finito.toString();				
					finito = finito.replace(".",",");
					
					if (tipo == "sconto") {
						document.getElementById(\'product_price[\'+id_product+\']\').value=finito;
					}
					else if (tipo == "inverso") {
						document.getElementById(\'sconto_extra[\'+id_product+\']\').value=finito;
					}
				}
				
				function calcolaImportoConSpedizione(id_metodo) {
		
					if(document.getElementById(\'trasporto_modificato\').value == "y") {
						var metodo_price = parseFloat(document.getElementById(\'transport\').value.replace(/\s/g, "").replace(",", "."));
					}
					else {
						var metodo_price = parseFloat(document.getElementById(\'metodo[\'+id_metodo+\']\').getAttribute("rel"));
					}
					var totale = 0;
					var totale_con_iva = 0;
					var current_products = "";
					var current_prices = "";
					var arrayImporti = document.getElementsByClassName("importo");
						for (var i = 0; i < arrayImporti.length; ++i) {
							current_products += arrayImporti[i].id;
							current_prices += arrayImporti[i].id+"_"+arrayImporti[i].value;
							var item = parseFloat(arrayImporti[i].value);  
							totale += item;
						}
					console.log(current_prices);
					totale = totale+metodo_price;
					var totalep = totale;
					totale = totale.toFixed(2);
					document.getElementById(\'totaleprodotti\').value=totale;
					document.getElementById(\'spantotaleprodotti\').innerHTML=totale.replace(".",",");
					

					$.ajax({
						url:"ajax_products_list2.php?calcola_totale_con_iva=y",
						type: "POST",
						data: { address: document.getElementById("id_address_delivery").value,
						customer : '.$customer->id.',
						current_prices : current_prices,
						id_cart: '.Tools::getValue('id_cart').',
						totalep : totalep,
						 },
						success:function(resp){
							totale_con_iva = parseFloat(resp);
							totale_con_iva = totale_con_iva.toFixed(2);
							document.getElementById(\'spantotaleprodottiiva\').innerHTML=totale_con_iva.replace(".",",");
						},
						error: function(xhr,stato,errori){
							// alert("Errore nella cancellazione:"+xhr.status);
						}
					});
					
					calcola_noleggio();
				}
				
				function resetThisForm() {
					document.getElementById("name").value = "";
					document.getElementById("validita").value = "";
					document.getElementById("consegna").value = "";
					document.getElementById("rif_ordine").value = "";
					document.getElementById("cig").value = "";
					document.getElementById("cup").value = "";
					document.getElementById("ipa").value = "";
					document.getElementById("payment").value = "nessuno";
					document.getElementById("data_ordine_mepa").value = "";
					document.getElementById("cadenza").value = "";
					document.getElementById("scadenza").value = "";
					document.getElementById("decorrenza").value = "";
					document.getElementById("esigenze").value = "";
					document.getElementById("risorse").value = "";
					document.getElementById("esito").value = "";
					document.getElementById("causa").value = "";
					document.getElementById("concorrente").value = "";
					$("#template_carrello1").prop("selectedIndex", 0);
					document.getElementById("altropagamento").value = "";
					$(tinymce.editors[\'note\'].getBody()).html("");
					$(tinymce.editors[\'esigenze\'].getBody()).html("");
					$(tinymce.editors[\'risorse\'].getBody()).html("");
					// $(tinymce.editors[\'note\'].getBody()).empty();
					// $(tinymce.editors[\'esigenze\'].getBody()).empty();
					// $(tinymce.editors[\'risorse\'].getBody()).empty();
					$(\'#tableProducts > tbody\').html(\'\');
					// $(\'#tableProducts > tbody\').empty();					
					document.getElementById("guadagnototale").innerHTML = "0,00";
					document.getElementById("totaleacquisti").innerHTML = "0,00";
					document.getElementById("marginalitatotale").innerHTML = "0,00%";
					document.getElementById("spantotaleprodotti").innerHTML = "0,00";
					
					document.getElementById("guadagnototale_prodotti").innerHTML = "0,00";
					document.getElementById("totaleacquisti_prodotti").innerHTML = "0,00";
					document.getElementById("marginalitatotale_prodotti").innerHTML = "0,00%";
					document.getElementById("spantotaleprodotti_prodotti").innerHTML = "0,00";
					
					 var checkboxes = document.getElementsByTagName("input");
					 
					 for (var i = 0; i < checkboxes.length; i++) {
						 console.log(i)
						 if (checkboxes[i].type == "checkbox") {
							 checkboxes[i].checked = false;
						 }
					 }
					calcolaSpedizionePerIndirizzo(); 
					';
					
					$allegati = explode(";",$attachment);
					
					foreach($cart_note as $cart_nota) {
						$html .= 'togli_nota('.$cart_nota['id_note'].'); cancella_nota('.$cart_nota['id_note'].');';
					}
					
					foreach($allegati as $allegato) {
							
						if(strpos($allegato, ":::")) {
							$parti = explode(":::", $allegato);
							$nomevero = $parti[1];
						}
						else
							$nomevero = $allegato;

						if(!($nall == 1))
							$html .= " - "; 

						$html .= 'cancella_allegato(\''.$allegato.'\', \''.$parti[0].'\');';
					}
					
					$html .= '
				}
				
				function calcoloTelegestione() {
					var totale = 0;
					
					var arrayImporti = document.getElementsByClassName("importo_telegestione");
					for (var i = 0; i < arrayImporti.length; ++i) {
						var item = parseFloat(arrayImporti[i].value);  
						totale += item;
					}
					
				
					// calcolo telegestione
				
					if ($("#product_id_336063").length > 0) {
						//totale -= (document.getElementById("valoreImporto[336063]").innerHTML.replace(",","."));
						// metterlo in sola lettura
						//document.getElementById("product_price[336063]").readOnly = true;
						//document.getElementById("product_quantity[336063]").readOnly = true;
						//document.getElementById("product_price[336063]").style.backgroundColor = "#f0ebd6";
						//document.getElementById("product_quantity[336063]").style.backgroundColor = "#f0ebd6";
						
						var valore_telegestione;
						
						if(totale < 500)
							valore_telegestione = 155;
						else  {
							var undici_per_cento = (totale / 100) * 11;
							valore_telegestione = 90 + undici_per_cento;
						}
						
						if(isNaN(valore_telegestione)) {		
							valore_telegestione = 0;
						}
						document.getElementById("unitario[336063]").value = valore_telegestione;
						document.getElementById("impImporto[336063]").value = valore_telegestione;
						
						totale += valore_telegestione;
						
						valore_telegestione = valore_telegestione.toFixed(2);
						valore_telegestione = valore_telegestione.replace(".",",");
						
						document.getElementById("product_price[336063]").value = valore_telegestione;
						document.getElementById("valoreImporto[336063]").innerHTML = valore_telegestione;
						
					}
					
					if ($("#product_id_336064").length > 0) {
						totale -= (document.getElementById("valoreImporto[336064]").innerHTML.replace(",","."));
						// metterlo in sola lettura
						document.getElementById("product_price[336064]").readOnly = true;
						document.getElementById("product_quantity[336064]").readOnly = true;
						document.getElementById("product_price[336064]").style.backgroundColor = "#f0ebd6";
						document.getElementById("product_quantity[336064]").style.backgroundColor = "#f0ebd6";
						
						var valore_telegestione;
						
						if(totale < 500)
							valore_telegestione = 155;
						else 
						{
							var undici_per_cento = (totale / 100) * 11;
							valore_telegestione = 90 + undici_per_cento;
						}
						
						if(isNaN(valore_telegestione))
						{		
							valore_telegestione = 0;
						}
						
						valore_telegestione = valore_telegestione*3;
						
						document.getElementById("unitario[336064]").value = valore_telegestione;
						document.getElementById("impImporto[336064]").value = valore_telegestione;
						
						totale += valore_telegestione;
						
						valore_telegestione = valore_telegestione.toFixed(2);
						valore_telegestione = valore_telegestione.replace(".",",");
						
						document.getElementById("product_price[336064]").value = valore_telegestione;
						document.getElementById("valoreImporto[336064]").innerHTML = valore_telegestione;
						
					}
				}
				
				function calcolaImporto(id_product) {
					
						var numprice = document.getElementById(\'product_price[\'+id_product+\']\').value;
						var numunitario = document.getElementById(\'unitario[\'+id_product+\']\').value;
						var numacquisto = document.getElementById(\'wholesale_price[\'+id_product+\']\').value;
						
						
						var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
									
						var quantity = parseFloat(document.getElementById(\'product_quantity[\'+id_product+\']\').value);

						var unitario = parseFloat(numunitario.replace(/\s/g, "").replace(",", "."));
						var acquisto = parseFloat(numacquisto.replace(/\s/g, "").replace(",", "."));
						
						var totaleacquisto = acquisto * quantity;
						
						document.getElementById(\'totaleacquisto[\'+id_product+\']\').value = totaleacquisto;

						var totale = parseFloat(document.getElementById(\'totaleprodotti\').value);
								
						var sc_qta_1 = parseFloat(document.getElementById(\'sc_qta_1[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
						var sc_qta_2 = parseFloat(document.getElementById(\'sc_qta_2[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
						var sc_qta_3 = parseFloat(document.getElementById(\'sc_qta_3[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
						var sc_riv_1 = parseFloat(document.getElementById(\'sc_riv_1[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
						var sc_riv_2 = parseFloat(document.getElementById(\'sc_riv_2[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
						var sc_riv_3 = parseFloat(document.getElementById(\'sc_riv_3[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));				
						var sc_qta_1_q = parseFloat(document.getElementById(\'sc_qta_1_q[\'+id_product+\']\').value);
						var sc_qta_2_q = parseFloat(document.getElementById(\'sc_qta_2_q[\'+id_product+\']\').value);
						var sc_qta_3_q = parseFloat(document.getElementById(\'sc_qta_3_q[\'+id_product+\']\').value);
						var sc_riv_1_q = parseFloat(document.getElementById(\'sc_riv_1_q[\'+id_product+\']\').value);
						var sc_riv_2_q = parseFloat(document.getElementById(\'sc_riv_2_q[\'+id_product+\']\').value);
						var sc_riv_3_q = parseFloat(document.getElementById(\'sc_riv_3_q[\'+id_product+\']\').value);
						
						
						var importo = parseFloat(price*quantity);
						
						if (document.getElementById(\'usa_sconti_quantita[\'+id_product+\']\').checked == true) {

						';
							if ($customer->id_default_group == 3 || $customer->id_default_group == 12) {
							
								$html .= '
								if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".") > 0) {
								} else {
									var prezzounitarionew = unitario;
									var prezziconfronto = new Array(sc_riv_1, '.($customer->id_default_group == 12 ? 'sc_riv_3, ': '').'sc_qta_1, sc_qta_2, sc_qta_3);
									for (var i = 0; i < prezziconfronto.length; ++i) {
										if(prezziconfronto[i] !=0 && prezziconfronto[i] < prezzounitarionew) { 
											prezzounitarionew = prezziconfronto[i];
											
										} else { }
									}	
									
									if(prezzounitarionew > unitario)
										prezzounitarionew = unitario;
									
									var importo = parseFloat(prezzounitarionew*quantity);
									document.getElementById(\'product_price[\'+id_product+\']\').value=prezzounitarionew.toFixed(2).replace(".",",");					
								}
							';
							
							}
							else if ($customer->id_default_group == 22) {
							
								$html .= '
								if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".") > 0) {
								} else {
									var prezzounitarionew = unitario;
									
									if(sc_riv_2 == 0)
									{
										sc_riv_1 = sc_riv_1 + (sc_riv_1 / 100 *3);
										var prezziconfronto = new Array(sc_riv_1, sc_qta_1, sc_qta_2, sc_qta_3);
										for (var i = 0; i < prezziconfronto.length; ++i) {
											if(prezziconfronto[i] !=0 && prezziconfronto[i] < prezzounitarionew) { 
												prezzounitarionew = prezziconfronto[i];
												
											} else { }
										}	
									}
									else
									{
										var prezziconfronto = new Array(sc_riv_2, sc_qta_1, sc_qta_2, sc_qta_3);
										for (var i = 0; i < prezziconfronto.length; ++i) {
											if(prezziconfronto[i] !=0 && prezziconfronto[i] < prezzounitarionew) { 
												prezzounitarionew = prezziconfronto[i];
												
											} else { }
										}	
									}
									if(prezzounitarionew > unitario)
										prezzounitarionew = unitario;
									
									var importo = parseFloat(prezzounitarionew*quantity);
									document.getElementById(\'product_price[\'+id_product+\']\').value=prezzounitarionew.toFixed(2).replace(".",",");					
								}
							';
							}
							else {
							
								$html .= '
								if(quantity < sc_qta_1_q && sc_qta_1_q != 0) {
									if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".") > 0) {
									} else {
										document.getElementById(\'product_price[\'+id_product+\']\').value=unitario.toFixed(2).replace(".",",");	
										var importo = parseFloat(price*quantity);
									}		
								}
								else if((quantity >= sc_qta_1_q && quantity < sc_qta_2_q) || (quantity >= sc_qta_1_q && isNaN(parseFloat(sc_qta_2_q)))) {
									if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".") > 0) {
									} else {
										if(unitario < sc_qta_1)
											sc_qta_1 = unitario;
									
										var importo = parseFloat(sc_qta_1*quantity);
										document.getElementById(\'product_price[\'+id_product+\']\').value=sc_qta_1.toFixed(2).replace(".",",");									}
								}
								else if((quantity >= sc_qta_2_q && quantity < sc_qta_3_q) || (quantity >= sc_qta_2_q && isNaN(parseFloat(sc_qta_3_q == 0)))) {
									if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".") > 0) {
									} 
									else {
										if(unitario < sc_qta_2)
											sc_qta_2 = unitario;
											
										var importo = parseFloat(sc_qta_2*quantity);	
										document.getElementById(\'product_price[\'+id_product+\']\').value=sc_qta_2.toFixed(2).replace(".",",");
									}
								}
								else if(quantity >= sc_qta_3_q && sc_qta_2_q != 0 && sc_qta_3_q != 0 && sc_qta_1_q != 0) {
									if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".") > 0) {
									} 
									else {
										if(unitario < sc_qta_3)
											sc_qta_3 = unitario;
											
										var importo = parseFloat(sc_qta_3*quantity);	
										document.getElementById(\'product_price[\'+id_product+\']\').value=sc_qta_3.toFixed(2).replace(".",",");
									}
								}';
							
							} 

						$html .= '
						}
						
						else if (document.getElementById(\'usa_sconti_quantita[\'+id_product+\']\').checked == false) {
							var importo = parseFloat(price*quantity);
						}

						document.getElementById(\'product_price[\'+id_product+\']\').value.replace(".",",");
						
						totale = 0;
						var totale_prodotti = 0;
						var totale_ezcloud = 0;
						
						
						document.getElementById(\'impImporto[\'+id_product+\']\').value = importo;
						
						//calcoloTelegestione();
						
						
						var arrayImporti = document.getElementsByClassName("importo");
						for (var i = 0; i < arrayImporti.length; ++i) {
							var item = parseFloat(arrayImporti[i].value);  
							totale += item;
						}
						
						var arrayImporti_prodotti = document.getElementsByClassName("importo_prodotti");
						for (var i = 0; i < arrayImporti_prodotti.length; ++i) {
							var item = parseFloat(arrayImporti_prodotti[i].value);  
							totale_prodotti += item;
						}
						
						var arrayImporti_ezcloud = document.getElementsByClassName("importo_ezcloud");
						for (var i = 0; i < arrayImporti_ezcloud.length; ++i) {
							var item = parseFloat(arrayImporti_ezcloud[i].value);  
							totale_ezcloud += item;
						}
						
						var totaleacquisti = 0;
						var totaleacquisti_ezcloud = 0;
						var totaleacquisti_prodotti = 0;
						
						var arrayAcquisti = document.getElementsByClassName("prezzoacquisto");
						for (var i = 0; i < arrayAcquisti.length; ++i) {
							arrayAcquisti[i].value = arrayAcquisti[i].value.replace(",",".");
							var item = parseFloat(arrayAcquisti[i].value); 					
							totaleacquisti += item;
						}
						
						var arrayAcquisti_prodotti = document.getElementsByClassName("prezzoacquisto_prodotti");
						for (var i = 0; i < arrayAcquisti_prodotti.length; ++i) {
							arrayAcquisti_prodotti[i].value = arrayAcquisti_prodotti[i].value.replace(",",".");
							var item = parseFloat(arrayAcquisti_prodotti[i].value); 					
							totaleacquisti_prodotti += item;
						}
								
						var arrayAcquisti_ezcloud = document.getElementsByClassName("prezzoacquisto_ezcloud");
						for (var i = 0; i < arrayAcquisti_ezcloud.length; ++i) {
							arrayAcquisti_ezcloud[i].value = arrayAcquisti_ezcloud[i].value.replace(",",".");
							var item = parseFloat(arrayAcquisti_ezcloud[i].value); 					
							totaleacquisti_ezcloud += item;
						}
						
						var controllo_vendita = document.getElementById(\'controllo_vendita_\'+id_product+\'\');
						
						if(numunitario < price)
							controllo_vendita.innerHTML = "<span class=\'span-reference\' title=\'ATTENZIONE stai vendendo il prodotto a un prezzo maggiore rispetto al prezzo standard di "+document.getElementById(\'unitario[\'+id_product+\']\').value + "\'><img class=\'img_control\' src=\'../img/admin/error.png\' style=\'padding:0px; width:15px; float:right\' alt=\'ATTENZIONE stai vendendo il prodotto a un prezzo maggiore rispetto al prezzo standard di "+document.getElementById(\'unitario[\'+id_product+\']\').value + "\' /></span>";
						else
							controllo_vendita.innerHTML = "";
									
						var marginalitatotale = (((totale - totaleacquisti)*100) / totale);

						if(totale < 0)
							marginalitatotale = 0-marginalitatotale;
			
						marginalitatotale = marginalitatotale.toFixed(2);
			
						document.getElementById(\'marginalitatotale\').innerHTML=marginalitatotale.replace(".",",") + "%";
						
						var guadagnototale = totale - totaleacquisti;
						guadagnototale = guadagnototale.toFixed(2);
						
						document.getElementById(\'guadagnototale\').innerHTML=guadagnototale.replace(".",",");
						
						totaleacquisti = totaleacquisti.toFixed(2);
						
						document.getElementById(\'totaleacquisti\').innerHTML=totaleacquisti.replace(".",",");
						
						importo = importo.toFixed(2);
						importo = importo.replace(".",",");
						
						if(id_product != 336063 && id_product != 336064)
							document.getElementById(\'valoreImporto[\'+id_product+\']\').innerHTML=importo;
						
						
						/*
						for (var i = 0; i < document.forms.products.elements.metodo.length; i++) {
							var button = document.forms.products.elements.metodo[i];
							
							if (button.checked) {
								
								totale += parseFloat(button.getAttribute("rel"));
							}
							else {
							
							}
						}*/
						';
						
						if($customer->id_default_group == 1) {
						$html .= '	
							if(totale < 399) {
								var supplemento_contrassegno = 3.5;
							}
							else {
								var supplemento_contrassegno = 0;
								
							}';
						}
						else if($customer->id_default_group  == 3) {
							$html .= 'if(totale < 516) {
								var supplemento_contrassegno = 5;
							}
							else {
								var supplemento_contrassegno = ((totale/100)*1.5);
							}';
								
						}
						else {
							$html .= 'if(totale < 516) {
								var supplemento_contrassegno = 5;
							}
							else {
								var supplemento_contrassegno = ((totale/100)*1.5);
							}';
						}
						
						$html .= '
						supplemento_contrassegno = supplemento_contrassegno.toFixed(2);
										
						document.getElementById(\'supplemento_contrassegno\').innerHTML=supplemento_contrassegno.replace(".",",");	

						calcolaSpedizionePerIndirizzo();				
						
						totale = totale.toFixed(2);
					
						document.getElementById(\'totaleprodotti\').value=totale;
						document.getElementById(\'spantotaleprodotti\').innerHTML=totale.replace(".",",");
						
						numprice = document.getElementById(\'product_price[\'+id_product+\']\').value;
						price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
						
						var marginalita = (((price - acquisto)*100) / price);
						
						if(price < 0)
							marginalita = 0-marginalita;
						
						if(marginalita < 10 && price > 0)
							document.getElementById(\'tr_\'+id_product).style.backgroundColor=\'#ffcccc\';
						else
							document.getElementById(\'tr_\'+id_product).style.backgroundColor=\'#ffffff\';
						
						if(isNaN(marginalita))
						{
							marginalita = 0;
						}
						
						marginalita = marginalita.toFixed(2);
						
						
						document.getElementById(\'spanmarginalita[\'+id_product+\']\').innerHTML=marginalita.replace(".",",")+"%";
						
						calcola_noleggio();
						
						var marginalitatotale_prodotti = (((totale_prodotti - totaleacquisti_prodotti)*100) / totale_prodotti);

						if(totale_prodotti < 0)
							marginalitatotale_prodotti = 0-marginalitatotale_prodotti;
						
						marginalitatotale_prodotti = marginalitatotale_prodotti.toFixed(2);
						document.getElementById(\'marginalitatotale_prodotti\').innerHTML = marginalitatotale_prodotti.replace(".",",") + "%";
						
						var guadagnototale_prodotti = totale_prodotti - totaleacquisti_prodotti;
						guadagnototale_prodotti = guadagnototale_prodotti.toFixed(2);
						document.getElementById(\'guadagnototale_prodotti\').innerHTML=guadagnototale_prodotti.replace(".",",");
						
						totaleacquisti_prodotti = totaleacquisti_prodotti.toFixed(2);
						document.getElementById(\'totaleacquisti_prodotti\').innerHTML=totaleacquisti_prodotti.replace(".",",");
						
						totale_prodotti = totale_prodotti.toFixed(2);
						document.getElementById(\'spantotaleprodotti_prodotti\').innerHTML=totale_prodotti.replace(".",",");
						
						var marginalitatotale_ezcloud = (((totale_ezcloud - totaleacquisti_ezcloud)*100) / totale_ezcloud);
						if(totale_ezcloud < 0)
							marginalitatotale_ezcloud = 0-marginalitatotale_ezcloud;
						
						marginalitatotale_ezcloud = marginalitatotale_ezcloud.toFixed(2);
						document.getElementById(\'marginalitatotale_ezcloud\').innerHTML=marginalitatotale_ezcloud.replace(".",",") + "%";
						
						var guadagnototale_ezcloud = totale_ezcloud - totaleacquisti_ezcloud;
						guadagnototale_ezcloud = guadagnototale_ezcloud.toFixed(2);
						document.getElementById(\'guadagnototale_ezcloud\').innerHTML=guadagnototale_ezcloud.replace(".",",");
						
						totaleacquisti_ezcloud = totaleacquisti_ezcloud.toFixed(2);
						document.getElementById(\'totaleacquisti_ezcloud\').innerHTML=totaleacquisti_ezcloud.replace(".",",");
						
						totale_ezcloud = totale_ezcloud.toFixed(2);
						document.getElementById(\'spantotaleprodotti_ezcloud\').innerHTML=totale_ezcloud.replace(".",",");
						
						if(document.getElementById(\'trasporto_modificato\').value == "y")
							var metodo_price = parseFloat(document.getElementById(\'transport\').value.replace(/\s/g, "").replace(",", "."));

						var totale = 0;
						var totale_con_iva = 0;
						var arrayImporti = document.getElementsByClassName("importo");
						var current_products = "";
						var current_prices = "";
						for (var i = 0; i < arrayImporti.length; ++i) {
							current_products += arrayImporti[i].id;
							current_prices += arrayImporti[i].id+"_"+arrayImporti[i].value;
							var item = parseFloat(arrayImporti[i].value);  
							totale += item;
						}
						console.log(current_prices);
						totale = totale+metodo_price;
						var totalep = totale;
						totale = totale.toFixed(2);
							
						document.getElementById(\'totaleprodotti\').value=totale;
						document.getElementById(\'spantotaleprodotti\').innerHTML=totale.replace(".",",");

						$.ajax({
							url:"ajax_products_list2.php?calcola_totale_con_iva=y",
							type: "POST",
							data: { address: document.getElementById("id_address_delivery").value,
							customer : '.$customer->id.',
							current_prices : current_prices,
							id_cart: '.Tools::getValue('id_cart').',
							totalep : totalep,
							 },
							success:function(resp){
								totale_con_iva = parseFloat(resp);
								totale_con_iva = totale_con_iva.toFixed(2);
								document.getElementById(\'spantotaleprodottiiva\').innerHTML=totale_con_iva.replace(".",",");
							},
							error: function(xhr,stato,errori){
								// alert("Errore nella cancellazione:"+xhr.status);
							}
						});
						
						calcola_noleggio();
					}
					
					function togliImporto(id_product) {
					
						var totale = 0;
						var arrayImporti = document.getElementsByClassName("importo");

						for (var i = 0; i < arrayImporti.length; ++i) {
							var item = parseFloat(arrayImporti[i].value);  
							totale += item;
						}
					';
					
						if($customer->id_default_group == 1) {
					
							$html .= '	
								if(totale < 399) {
									var supplemento_contrassegno = 3.5;
								}
								else {
									var supplemento_contrassegno = 0;
								}
							';
						}
							
						else if($customer->id_default_group  == 3) {
								
							$html .= '
								if(totale < 516) {
									var supplemento_contrassegno = 5;
								}
								else {
									var supplemento_contrassegno = ((totale/100)*1.5);
								
								}
							';
						}
						else {
							$html .= 'if(totale < 516) {
								var supplemento_contrassegno = 5;
							}
							else {
								var supplemento_contrassegno = ((totale/100)*1.5);
							}';
						}
						
						$html .= ' 
						supplemento_contrassegno = supplemento_contrassegno.toFixed(2);
										
						document.getElementById(\'supplemento_contrassegno\').innerHTML=supplemento_contrassegno.replace(".",",");
						
						var totaleacquisti = 0;
						
						var arrayAcquisti = document.getElementsByClassName("prezzoacquisto");
						for (var i = 0; i < arrayAcquisti.length; ++i) {
							arrayAcquisti[i].value = arrayAcquisti[i].value.replace(",",".");
							var item = parseFloat(arrayAcquisti[i].value); 					
							totaleacquisti += item;
						}
								
						var marginalitatotale = (((totale - totaleacquisti)*100) / totale);
			
						if(totale < 0)
							marginalitatotale = 0-marginalitatotale;
							
						marginalitatotale = marginalitatotale.toFixed(2);
						document.getElementById(\'marginalitatotale\').innerHTML=marginalitatotale.replace(".",",") + "%";
						
						var guadagnototale = totale - totaleacquisti;
						guadagnototale = guadagnototale.toFixed(2);
						totaleacquisti = totaleacquisti.toFixed(2);
						document.getElementById(\'totaleacquisti\').innerHTML=totaleacquisti.replace(".",",");
						document.getElementById(\'guadagnototale\').innerHTML=guadagnototale.replace(".",",");
					
						totale = totale.toFixed(2);
						document.getElementById(\'totaleprodotti\').value=totale;
						
						document.getElementById(\'spantotaleprodotti\').innerHTML=totale.replace(".",",");
					
						calcola_noleggio();
						var totale_prodotti = 0;
						var totale_ezcloud = 0;

						var arrayImporti_prodotti = document.getElementsByClassName("importo_prodotti");
						for (var i = 0; i < arrayImporti_prodotti.length; ++i) {
							var item = parseFloat(arrayImporti_prodotti[i].value);  
							totale_prodotti += item;
						}

						var arrayImporti_ezcloud = document.getElementsByClassName("importo_ezcloud");
						for (var i = 0; i < arrayImporti_ezcloud.length; ++i) {
							var item = parseFloat(arrayImporti_ezcloud[i].value);  
							totale_ezcloud += item;
						}

						var totaleacquisti_ezcloud = 0;
						var totaleacquisti_prodotti = 0;

						var arrayAcquisti_prodotti = document.getElementsByClassName("prezzoacquisto_prodotti");
						for (var i = 0; i < arrayAcquisti_prodotti.length; ++i) {
							arrayAcquisti_prodotti[i].value = arrayAcquisti_prodotti[i].value.replace(",",".");
							var item = parseFloat(arrayAcquisti_prodotti[i].value); 					
							totaleacquisti_prodotti += item;
						}
								
						var arrayAcquisti_ezcloud = document.getElementsByClassName("prezzoacquisto_ezcloud");
						for (var i = 0; i < arrayAcquisti_ezcloud.length; ++i) {
							arrayAcquisti_ezcloud[i].value = arrayAcquisti_ezcloud[i].value.replace(",",".");
							var item = parseFloat(arrayAcquisti_ezcloud[i].value); 					
							totaleacquisti_ezcloud += item;
						}

						var marginalitatotale_prodotti = (((totale_prodotti - totaleacquisti_prodotti)*100) / totale_prodotti);
						if(totale_prodotti < 0)
							marginalitatotale_prodotti = 0-marginalitatotale_prodotti;

						marginalitatotale_prodotti = marginalitatotale_prodotti.toFixed(2);
						document.getElementById(\'marginalitatotale_prodotti\').innerHTML = marginalitatotale_prodotti.replace(".",",") + "%";

						var guadagnototale_prodotti = totale_prodotti - totaleacquisti_prodotti;
						guadagnototale_prodotti = guadagnototale_prodotti.toFixed(2);
						document.getElementById(\'guadagnototale_prodotti\').innerHTML=guadagnototale_prodotti.replace(".",",");

						totaleacquisti_prodotti = totaleacquisti_prodotti.toFixed(2);
						document.getElementById(\'totaleacquisti_prodotti\').innerHTML=totaleacquisti_prodotti.replace(".",",");

						totale_prodotti = totale_prodotti.toFixed(2);
						document.getElementById(\'spantotaleprodotti_prodotti\').innerHTML=totale_prodotti.replace(".",",");

						var marginalitatotale_ezcloud = (((totale_ezcloud - totaleacquisti_ezcloud)*100) / totale_ezcloud);
						if(totale_ezcloud < 0)
							marginalitatotale_ezcloud = 0-marginalitatotale_ezcloud;

						marginalitatotale_ezcloud = marginalitatotale_ezcloud.toFixed(2);
						document.getElementById(\'marginalitatotale_ezcloud\').innerHTML=marginalitatotale_ezcloud.replace(".",",") + "%";

						var guadagnototale_ezcloud = totale_ezcloud - totaleacquisti_ezcloud;
						guadagnototale_ezcloud = guadagnototale_ezcloud.toFixed(2);
						document.getElementById(\'guadagnototale_ezcloud\').innerHTML=guadagnototale_ezcloud.replace(".",",");

						totaleacquisti_ezcloud = totaleacquisti_ezcloud.toFixed(2);
						document.getElementById(\'totaleacquisti_ezcloud\').innerHTML=totaleacquisti_ezcloud.replace(".",",");

						totale_ezcloud = totale_ezcloud.toFixed(2);
						document.getElementById(\'spantotaleprodotti_ezcloud\').innerHTML=totale_ezcloud.replace(".",",");
					
					}
				
					function deleteRow(rowid) {   
						var row = document.getElementById(rowid);
						if(row == null)
						{
							
						}
						else
						{
							row.parentNode.removeChild(row);
						}
					}
				
				function delProduct30(id)
				{
					deleteRow(\'tr_\'+id);
					//calcoloTelegestione();
					$("#id_lang").after("<input name=\'product_delete["+id+"]\' id=\'product_delete["+id+"]\' type=\'hidden\' value=\'"+id+"\' />");
					$.ajax({
					  url:"ajax_products_list2.php?cancellaprodotto=y",
					  type: "POST",
					  data: { id_product: id,
					  id_cart: '.$_GET['id_cart'].'
					  },
					  success:function(){
							var excludeIds = document.getElementById("excludeIds").value;
							
							var da_cancellare = new RegExp(id, "g");
							
							excludeIds = excludeIds.replace(da_cancellare,""); 
							document.getElementById("excludeIds").value = excludeIds;
							$("#product_autocomplete_input").val("");
							
							calcolaSpedizionePerIndirizzo();	
							
					  },
					  error: function(xhr,stato,errori){
						 // alert("Errore nella cancellazione:"+xhr.status);
					  }
					});
					
					
				}
					
					
					
				function calcola_noleggio(eliminate) {
					
					var numtotale = document.getElementById("spantotaleprodotti").innerHTML;

					var totale = parseFloat(numtotale.replace(/\s/g, "").replace(",", "."));
					
					if(eliminate != false)
					{
						if(totale >= 1500)
						{
							document.getElementById("mesi_noleggio").selectedIndex = 3;
							mesi_noleggio = 36;
						}
						if(totale <= 1500)
						{
							document.getElementById("mesi_noleggio").selectedIndex = 0;
							mesi_noleggio = 0;
						}
					}
					if(totale <= 5000) { document.getElementById("spese_contratto_noleggio").value = 50 }
					if(totale >= 5001 && totale <= 10000) { document.getElementById("spese_contratto_noleggio").value = 75 }
					if(totale >= 10001 && totale <= 25000) { document.getElementById("spese_contratto_noleggio").value = 100 }
					if(totale >= 25001 && totale <= 50000) { document.getElementById("spese_contratto_noleggio").value = 100 }
					if(totale >= 50001 && totale <= 100000) { document.getElementById("spese_contratto_noleggio").value = 150 }
					if(totale >= 100001) { document.getElementById("spese_contratto_noleggio").value = 200 }
					
					var mesi_noleggio = document.getElementById("mesi_noleggio").value;
					var parametro = 0;
					
					if(mesi_noleggio == 0) {
						parametro = 0;
						document.getElementById("spese_contratto_noleggio").value = 0;
					}
					
					else if(mesi_noleggio == 18) {
					
						if(totale <= 5000) { parametro = 6.208; }
						if(totale >= 5001 && totale <= 15000) { parametro = 6.145; }
						if(totale >= 15001 && totale <= 25000) { parametro = 6.116; }
						if(totale >= 25001 && totale <= 50000) { parametro = 6.087; }
						if(totale >= 50001 && totale <= 100000) { parametro = 6.099; }
						if(totale >= 100001) { parametro = 6.092; }

					}
					
					else if(mesi_noleggio == 24) {
					
						if(totale <= 5000) { parametro = 4.758; }
						if(totale >= 5001 && totale <= 15000) { parametro = 4.695; }
						if(totale >= 15001 && totale <= 25000) { parametro = 4.667; }
						if(totale >= 25001 && totale <= 50000) { parametro = 4.638; }
						if(totale >= 50001 && totale <= 100000) { parametro = 4.650; }
						if(totale >= 100001) { parametro = 4.643; }

					}
					
					else if(mesi_noleggio == 36) {
					
						if(totale <= 5000) { parametro = 3.310; }
						if(totale >= 5001 && totale <= 15000) { parametro = 3.247; }
						if(totale >= 15001 && totale <= 25000) { parametro = 3.218; }
						if(totale >= 25001 && totale <= 50000) { parametro = 3.190; }
						if(totale >= 50001 && totale <= 100000) { parametro = 3.202; }
						if(totale >= 100001) { parametro = 3.195; }

					}
					
					else if(mesi_noleggio == 48) {
					
						if(totale <= 5000) { parametro = 2.640; }
						if(totale >= 5001 && totale <= 15000) { parametro = 2.576; }
						if(totale >= 15001 && totale <= 25000) { parametro = 2.546; }
						if(totale >= 25001 && totale <= 50000) { parametro = 2.517; }
						if(totale >= 50001 && totale <= 100000) { parametro = 2.529; }
						if(totale >= 100001) { parametro = 2.522; }

					}
					
					else if(mesi_noleggio == 60) {
					
						if(totale <= 5000) { parametro = 2.210; }
						if(totale >= 5001 && totale <= 15000) { parametro = 2.144; }
						if(totale >= 15001 && totale <= 25000) { parametro = 2.114; }
						if(totale >= 25001 && totale <= 50000) { parametro = 2.084; }
						if(totale >= 50001 && totale <= 100000) { parametro = 2.097; }
						if(totale >= 100001) { parametro = 2.089; }

					}
					document.getElementById("parametro_noleggio").value = parametro;
					
					var rata_mensile = totale*(parametro/100);
					
					if(totale > 1500 && totale < 2999)
					{	
						rata_mensile = rata_mensile*3;
						document.getElementById("tipo_rata").innerHTML = "trimestrale";
					}
					else
						document.getElementById("tipo_rata").innerHTML = "mensile";
					
					rata_mensile = rata_mensile.toFixed(2);
					
					document.getElementById("importo_rata_mensile_noleggio").value = rata_mensile.replace(".",",");
					
					if(mesi_noleggio == 0) {
					
						document.getElementById("importo_rata_mensile_noleggio").value = 0;
						document.getElementById("parametro_noleggio").value = 0;
					
					}
					
				}
				
				
				function calcolaSpedizionePerIndirizzo() {
					
					var totale = 0;
					var totale_con_iva = 0;
					var current_products = "";
					var current_prices = "";
					var sconti_extra = 0;
					var arrayImporti = document.getElementsByClassName("importo");
						for (var i = 0; i < arrayImporti.length; ++i) {
							current_products += arrayImporti[i].id;
							current_prices += arrayImporti[i].id+"_"+arrayImporti[i].value;
							var item = parseFloat(arrayImporti[i].value);  
							totale += item;
						}
					console.log(current_prices);	
					var arraySconti = document.getElementsByClassName("sconto_extra");
					for (var i = 0; i < arraySconti.length; ++i) {
						var item = parseFloat(arraySconti[i].value);
						
						if(item > 0)
						{
							sconti_extra = 1;
						}
					}
					console.log(sconti_extra);
					
					
					var address_delivery = document.getElementById("id_address_delivery").value;
					var carrier_cart = $("input:radio[name=metodo]:checked").val();
					var trasporto_modificato = document.getElementById("valore_trasporto_modificato").value;
					trasporto_modificato = trasporto_modificato.replace(",",".");
					if(trasporto_modificato == "") { trasporto_modificato = 0; }
					
					';
				
					if($customer->id_default_group == 1) {
				
					$html .= '	
						if(totale < 399) {
							var supplemento_contrassegno = 3.5;
						}
						else {
							var supplemento_contrassegno = 0;
						}';
					}
						
					else if($customer->id_default_group  == 3) {
							
						$html .= 'if(totale < 516) {
							var supplemento_contrassegno = 5;
						}
						else {
							var supplemento_contrassegno = ((totale/100)*1.5);
						}';
					}
						
					else {
						$html .= 'if(totale < 516) {
							var supplemento_contrassegno = 5;
						}
						else {
							var supplemento_contrassegno = ((totale/100)*1.5);
						}';
					}
					
					$html .= '
					supplemento_contrassegno = supplemento_contrassegno.toFixed(2);		
					document.getElementById(\'supplemento_contrassegno\').innerHTML=supplemento_contrassegno.replace(".",",");	
					
					$.ajax({
						type: "POST",
						async: true,
						url: "calcola_costo_spedizione.php",
						data: "id_cart='.$cart->id.'&products="+current_products+"&sconti_extra="+sconti_extra+"&totale="+totale+"&address_delivery="+address_delivery+"&carrier_cart="+carrier_cart+"&trasporto_modificato="+trasporto_modificato,
						success: function(resp) {

							var values = resp.split("|");
							var carri = parseFloat(values[0]);
							var trasp = parseFloat(values[1]);
							trasp = parseFloat(trasp);

							totale = parseFloat(totale);
							totale = totale+trasp;
							totalep = totale;
							totale = totale.toFixed(2);

							trasp = trasp.toFixed(2);
							trasp = trasp.replace(".",",");
							
							if(!document.getElementById(\'metodo_costo[\'+carrier_cart+\']\')){}
							else {
								document.getElementById(\'metodo_costo[\'+carrier_cart+\']\').innerHTML=trasp;
							}

							document.getElementById(\'transport\').value=trasp;
							document.getElementById(\'importo_trasporto\').innerHTML=trasp;
							document.getElementById(\'totaleprodotti\').value=totale;
							document.getElementById(\'spantotaleprodotti\').innerHTML=totale.replace(".",",");		
							
							$.ajax({
								url:"ajax_products_list2.php?calcola_totale_con_iva=y",
								type: "POST",
								data: { 
									address: address_delivery,
									current_prices: current_prices,
									customer : '.$customer->id.',
									id_cart: '.Tools::getValue('id_cart').',
									totalep : totalep
								},
								success:function(resp){
									totale_con_iva = resp;
									totale_con_iva = parseFloat(totale_con_iva);
									totale_con_iva = totale_con_iva.toFixed(2);
									document.getElementById(\'spantotaleprodottiiva\').innerHTML=totale_con_iva.replace(".",",");		
								},
								error: function(xhr,stato,errori){
									// alert("Errore nella cancellazione:"+xhr.status);
								}
							});
							
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							alert("ERROR 1 "+XMLHttpRequest+" "+textStatus+" "+errorThrown);
						}
					});	
				}
				
				function vediTrasportoGratuito() {
					if(document.getElementById("transport").value == 0) {
						$("input:radio[class=gratis]").attr("checked", true);
					}
					else {
						$("input:radio[class=a_pagamento]").attr("checked", true);
					}
				}
					
			</script>
		';

		$tokenProducts = Tools::getAdminToken('AdminProducts'.(int)(Tab::getIdFromClassName('AdminProducts')).(int)($context->employee->id));
		// Correggere: attivare solo se necessario dopo aver installato il modulo ExFeatures; in quel caso sostituire tutti gli AdminProducts con AdminCatalogExFeatures
		// $tokenProducts = Tools::getAdminToken('AdminCatalogExFeatures'.(int)(Tab::getIdFromClassName('AdminCatalogExFeatures')).(int)($context->employee->id));

		$excludeIds = "";

		$array_prodotti_da_escludere = Db::getInstance()->executeS("
			SELECT * 
			FROM "._DB_PREFIX_."cart_product 
			WHERE id_cart=".$_GET['id_cart']."
		");

		foreach($array_prodotti_da_escludere as $escl) {
			$excludeIds .= $escl['id_product'].",";
		}

		$excludeIds .= 0;

		$html .= '<input autocomplete="off" id="excludeIds" type="hidden" value="'.$excludeIds.'" />';			
					
		$html .= '
			<script type="text/javascript">
			
			var prodotti_nel_carrello = ['.$excludeIds.'0];
 
				function addProduct_TR(event, data, formatted)
				{
					
					if(data[6] == "*BUNDLE*") {
						
						var prodotti_bundle = data[7].split(";");
						for(i=0;i<((prodotti_bundle.length))-1;i++)
						{
							
							$.ajax({
							type: "GET",
							data: "id_cart='.$_GET['id_cart'].'&id_bundle="+data[1]+"&product_in_bundle="+prodotti_bundle[i],
							async: false,
							url: "ajax_products_list2.php",
							success: function(resp)
								{
									addProduct_TR("",resp.split("|"),"");
								},
								error: function(XMLHttpRequest, textStatus, errorThrown)
								{
									tooltip_content = "";
									alert("ERROR" + textStatus + " " + XMLHttpRequest.responseText);					
								}
														
							});
						}
						return false;
					
					}
					
					document.getElementById("product_autocomplete_input").value = ""; 
					var productId = data[1];
					
					$("#product_delete["+productId+"]").remove();
					
					var excludeIds = document.getElementById("excludeIds").value;
					var arrayescl = excludeIds.split(",");
					
					for(i=0;i<((arrayescl.length));i++)
					{
						if(arrayescl[i].trim() != "" && arrayescl[i].trim() == productId.trim())
						{
							if(productId.trim != 31109 && productId.trim != 31110)
							{	
								 /* alert("Prodotto già nel carrello"); */
								return false;
							}
						}
					}
					excludeIds = excludeIds+","+productId+",";
					document.getElementById("excludeIds").value = excludeIds;
					$("#product_autocomplete_input").val("");
							
						
				
					$("#product_autocomplete_input").focus();
					
					var productPrice = data[2];
					var productScQta = data[3];
					var productRef = data[4];
					var productName = data[5];
					var productQuantity = data[16];
					var tdImporto = data[17];
					var przUnitario = data[18];
					var deleteProd = data[19];
					var marg = data[20];
					var scontoExtra = data[21];
					var sc_qta_1 = data[6];
					var sc_qta_2 = data[7];
					var sc_qta_3 = data[8];
					var sc_riv_1 = data[9];
					var sc_riv_2 = data[10];
					var sc_riv_3 = data[26];
					var sc_qta_1_q = data[11];	
					var sc_qta_2_q = data[12];	
					var sc_qta_3_q = data[13];	
					var sc_riv_1_q = data[14];	
					var sc_riv_2_q = data[15];	
					var sc_riv_3_q = data[27];	
					var acquisto = data[30];	
					var src_img = data[35];
					prodotti_nel_carrello.push(parseInt(productId));
					var varspec = data[24];
					var textVarSpec = "";
					var section = data[40];
					var suggerimenti = data[48];
					
					console.log(suggerimenti);
					
					'.(strpos($template_cart, 'zcloud') == true || strpos($template_cart, 'EASTAR-CLOUD') == true ? '' : 'section = "0";').'
					
					
					
					if(varspec == 0) {
					}
					else {
						
						textVarSpec = "style=\'border:1px solid red\'"
					}
					
					var appendto = "#tableProductsBody";
					
					if(section == "1")
						appendto = "#tableProductsBody_ezcloud";
					
					
					if(productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "") == 367642 || productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "") == 367645 || productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "") == 367498 || productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "") == 367499 )
					{
						$(appendto).prepend("<tr " + textVarSpec + " id=\'tr_"+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"\'><td><img src=\'"+src_img+"\' style=\'width:30px; height:30px;\' alt=\'\' /><input type=\'hidden\' name=\'section["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' value=\'"+section+"\' /></td><td><a href=\'index.php?controller=AdminProducts&id_product="+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"&updateproduct&token='.$tokenProducts.'\' target=\'_blank\'>" + productRef + "</a></td><td>" + suggerimenti + "</td><td><input name=\'nuovo_name["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' size=\'31\' type=\'text\' value=\'"+ productName +"\' /></td><td> " + productQuantity + "</td><td>" + productPrice + "<div style=\'float:right\' id=\'controllo_vendita_"+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"\'></div></td><td>" + scontoExtra + "</td><td>" + acquisto + "</td>" + marg + "<td style=\'text-align:center\'> " + productScQta + "</td>" + tdImporto + "<td class=\'pointer dragHandle center\' style=\'background:url(../img/admin/up-and-down.gif) no-repeat center;\'><input type=\'hidden\' name=\'sort_order["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' /></td><td style=\'text-align:center\'>" + deleteProd + "<input name=\'nuovo_prodotto["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' type=\'hidden\' value=\'"+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"\' />" + sc_qta_1 + " " + sc_qta_2 + " " + sc_qta_3 + " " + sc_riv_1 + " " + sc_riv_2 + " " + sc_riv_3 + " " + sc_qta_1_q + " " + sc_qta_2_q + " " + sc_qta_3_q + " " + sc_riv_1_q + " " + sc_riv_2_q + " " + sc_riv_3_q + " " + przUnitario + "</td></tr>");
					}
					else
					{
						$("<tr " + textVarSpec + " id=\'tr_"+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"\'><td><img src=\'"+src_img+"\' style=\'width:30px; height:30px;\' alt=\'\' /><input type=\'hidden\' name=\'section["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' value=\'"+section+"\' /></td><td><a href=\'index.php?controller=AdminProducts&id_product="+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"&updateproduct&token='.$tokenProducts.'\' target=\'_blank\'>" + productRef + "</a></td><td>" + suggerimenti + "</td><td><input name=\'nuovo_name["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' size=\'31\' type=\'text\' value=\'"+ productName +"\' /></td><td> " + productQuantity + "</td><td>" + productPrice + "<div style=\'float:right\' id=\'controllo_vendita_"+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"\'></div></td><td>" + scontoExtra + "</td><td>" + acquisto + "</td>" + marg + "<td style=\'text-align:center\'> " + productScQta + "</td>" + tdImporto + "<td class=\'pointer dragHandle center\' style=\'background:url(../img/admin/up-and-down.gif) no-repeat center;\'><input type=\'hidden\' name=\'sort_order["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' /></td><td style=\'text-align:center\'>" + deleteProd + "<input name=\'nuovo_prodotto["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' type=\'hidden\' value=\'"+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"\' />" + sc_qta_1 + " " + sc_qta_2 + " " + sc_qta_3 + " " + sc_riv_1 + " " + sc_riv_2 + " " + sc_riv_3 + " " + sc_qta_1_q + " " + sc_qta_2_q + " " + sc_qta_3_q + " " + sc_riv_1_q + " " + sc_riv_2_q + " " + sc_riv_3_q + " " + przUnitario + "</td></tr>").appendTo(appendto);
					}
					
					';

					//CONTROLLO PER COUPON CHEAPNET
					/*			
					$conto_iniziale = Db::getInstance()->getValue("SELECT COUNT(usato) FROM "._DB_PREFIX_."cheapnetcoupons WHERE usato = 0");
					if($conto_iniziale == 0) {
					}
					else {
						$prodotti_con_coupon = Db::getInstance()->getValue("SELECT value FROM "._DB_PREFIX_."configuration WHERE name = 'PS_CHEAPNET_COUPONS'");
		
						$prodotti = explode("-", $prodotti_con_coupon);

						$prodotti_da_6 = str_replace(";",",",$prodotti[0]);
						$prodotti_da_30 = str_replace(";",",",$prodotti[1]);
						
						$conto_iniziale_6 = Db::getInstance()->getValue("SELECT COUNT(usato) FROM "._DB_PREFIX_."cheapnetcoupons WHERE tipo = 6 AND usato = 0");
						if($conto_iniziale_6 == 0) {
						}
						else {
							$html .= 'var prodotti_da_6 = ['.$prodotti_da_6.'0]; ';
						}
						
						$conto_iniziale_30 = Db::getInstance()->getValue("SELECT COUNT(usato) FROM "._DB_PREFIX_."cheapnetcoupons WHERE tipo = 12 AND usato = 0");
						if($conto_iniziale_30 == 0) {
							$html .= 'var prodotti_da_30 = [0]; ';
						}
						else {
							$html .= 'var prodotti_da_30 = ['.$prodotti_da_30.'0]; ';
						}
					}
					*/	
					// FINE CONTROLLO PER COUPON CHEAPNET
					
					 $html .= '
						
						/*
						if($.inArray(parseInt(productId),prodotti_da_6) > -1){
							if(($.inArray("31110",arrayescl) == -1 ) && ($.inArray("31109",arrayescl) == -1 )){
								var prodotti_bundle = "31110;999999999999".split(";");
								for(i=0;i<((prodotti_bundle.length))-1;i++)
								{
									$.ajax({
									type: "GET",
									data: "id_cart='.$_GET['id_cart'].'&id_bundle=0&product_in_bundle="+prodotti_bundle[i],
									async: false,
									url: "ajax_products_list2.php",
									success: function(resp)
										{
											addProduct_TR("",resp.split("|"),"");
										},
										error: function(XMLHttpRequest, textStatus, errorThrown)
										{
											tooltip_content = "";
											alert("ERROR" + textStatus + " " + XMLHttpRequest.responseText);					
										}
																
									});
								}
							} else { }
						} 
						else if($.inArray(parseInt(productId),prodotti_da_30) > -1){
							if(($.inArray("31110",arrayescl) == -1 ) && ($.inArray("31109",arrayescl) == -1 )){
								var prodotti_bundle = "31109;999999999999".split(";");
								for(i=0;i<((prodotti_bundle.length))-1;i++)
								{
									$.ajax({
									type: "GET",
									data: "id_cart='.$_GET['id_cart'].'&id_bundle=0&product_in_bundle="+prodotti_bundle[i],
									async: false,
									url: "ajax_products_list2.php",
									success: function(resp)
										{
											addProduct_TR("",resp.split("|"),"");
										},
										error: function(XMLHttpRequest, textStatus, errorThrown)
										{
											tooltip_content = "";
											alert("ERROR" + textStatus + " " + XMLHttpRequest.responseText);					
										}
																
									});
								}
							}	
						}
						else {
						}
						*/
						
						var string_prodotti_nel_carrello = "";
						for (var i = 0; i < prodotti_nel_carrello.length; ++i) {
							var item = parseFloat(prodotti_nel_carrello[i]);  
							string_prodotti_nel_carrello += item+\',\';
						}
						string_prodotti_nel_carrello += "0";
						
					$(".wholesale_price_class").unbind();
					
					$(".wholesale_price_class").each(function() {
				
					
							var productId = $(this).attr("id").substring($(this).attr("id").indexOf("[") + 1,$(this).attr("id").indexOf("]",$(this).attr("id").indexOf("[") + 1));
							
							var acq = document.getElementById("wholesale_price["+productId+"]"); var last = acq.value;
								
								var ctrl_m_c = 0;
								';
								/*
								if(ctrl_m_c == 0) {
									$(document.getElementById("wholesale_price["+productId+"]")).one(\'click\', function() {
										alert(\'Attenzione: stai cambiando il prezzo di acquisto\');
										ctrl_m_c = 1;
									});
								}
								
								if(ctrl_m_c == 0) {
									$(document.getElementById("wholesale_price["+productId+"]")).one(\'keydown\', function(event) {
									
										var code = (event.keyCode ? event.keyCode : event.which);
										
										if(code == 9 || code == 37 || code == 38 || code == 39 || code == 40) {
											
										}
										else {
											alert(\'Attenzione: stai cambiando il prezzo di acquisto\');
											ctrl_m_c = 1;
										}
									});
								}
								*/
								
								$html .= '
								// Setup the event
								$(document.getElementById("wholesale_price["+productId+"]")).focusout(function() {
									ctrl_m_c = 0;
									if (last != $(this).val()) {
										
										var surec = window.confirm(\'Stai cambiando un prezzo di acquisto. Sei sicuro?\'); 
										if (surec) { return true; } else { document.getElementById("wholesale_price["+productId+"]").value = last; calcolaImporto(productId); }
									}
								});
							});
					
					
					
					
					/*$(appendto).tableDnD({
						onDrop: function(table, row) {
						},
						dragHandle: ".dragHandle"
					});*/
								
					$(appendto).hover(function() {
						  $(this.cells[8]).addClass(\'showDragHandle\');
					}, function() {
						  $(this.cells[8]).removeClass(\'showDragHandle\');
					});
					
					productId = productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "");
					
					$("#span-reference-"+productId+"").tooltip({ showURL: false  });
					
					$(".img_control").tooltip({ showURL: false  });
					
					var postdata =  $("#products").serialize().replace(/%5B/g, "[").replace(/%5D/g, "]");
					
					/*
					$.ajax({
					   type: "POST",
					   url: \''.self::$currentIndex.'&id_cart='.$_GET['id_cart'].'&viewcart&id_customer='.$customer->id.'&viewcustomer&ajaxsave&cartupdated&token='.$this->token.'#modifica-carrello\',
					   data: postdata+"&Apply=Conferma", 
					   success: function(data)
					   {
						  calcolaSpedizionePerIndirizzo();	
					   }
					 });
					 */
					 
					 calcolaImporto(productId);
					 '.($context->employee->id_profile == 7 ? 'calcolaProvvigione(productId);' : '').'
				}
				
			</script>
		';				

			$html .= '<br />';
			$html .= '<span class="button pointer" onclick="$(\'#mostra_premessa\').slideToggle();" style=" display:block; font-size:14px; font-weight:bold; width:902px; border:1px solid #cccccc; text-align:center; background-color:#f0f0ee; text-decoration:none">Premessa (clic per aprire)</span><br />';
				
				$html .= '<div id="mostra_premessa" style="'.(strip_tags(str_replace(" ","",$premessa)) == '' ? 'display:none;' : '').' margin-top:-15px">';
			
				$html .= '<br />Messaggi precompilati</td><td><select name="messaggi_precompilati" id="messaggi_precompilati" style="width:730px" onchange="inserisciPrecompilato(this.value);">
				';
				
				$precompilati = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'precompilato WHERE active = 1');
					$html .= '<option value="-- Seleziona un messaggio --">-- Seleziona un messaggio --</option>';
					foreach($precompilati as $precompilato)
					{
						$html .= '<option value="'.htmlentities($precompilato['testo'], ENT_QUOTES).'">'.htmlentities($precompilato['oggetto'], ENT_QUOTES).'</option>';
					}
				$html .= '
				</select>
				<script type="text/javascript">
				function inserisciPrecompilato(msg)
				{
					tinyMCE.get(\'premessa\').focus();
					var $body = $(tinymce.activeEditor.getBody());
					$body.html(\'\');
					// $body.empty();
					$body.prepend(\'<p>\');
					$body.append(\'</p>\');
					if(msg != "-- Seleziona un messaggio --") {
						$body.find("p:last").append(msg);
					}
				}
				</script>
				<br /><br />
				';
				
				
			$html .= ' '.($order->id? '<div style="width:100%; background-color:#f0ebd6; border:1px solid #cccccc; overflow-y:auto; height:150px">'.$premessa.'</div>' : '<textarea name="premessa" id="premessa" style="width:100%; height:50px !important" class="rte">'.$premessa.'</textarea>').'</td></tr></table></div>';
			
			if($order->id) { } else {
			$html .= '<input name="id_cart" type="hidden" value="'.$_GET['id_cart'].'" /><br />
			<strong>Cerca:</strong>
			<input type="checkbox" checked="checked" id="prodotti_online" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Online &nbsp;&nbsp;
			<input type="checkbox" id="prodotti_offline" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Offline &nbsp;
		<input type="checkbox" id="prodotti_old" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Old &nbsp;&nbsp;
			<input type="checkbox" id="prodotti_disponibili" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Solo disp. &nbsp;&nbsp;&nbsp;
			
			<script type="text/javascript" src="../js/select2.js"></script>
					<script type="text/javascript">
					$(document).ready(function() { $("#auto_marca").select2(); $("#auto_serie").select2(); $("#auto_fornitore").select2();  $("#auto_categoria").select2(); });
					</script>
					
			<select id="auto_marca" name="auto_marca" onchange="repopulateSeries(this.value); clearAutoComplete();" style="width:130px">
			<option value="0">Marca...</option>
			';
			$marche = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'manufacturer ORDER BY name ASC');
			foreach($marche as $marca)
				$html .= '<option value="'.$marca['id_manufacturer'].'">'.$marca['name'].'</option>';
			$html .= '
			</select>
			
					
			<select id="auto_serie" name="auto_serie" onchange="clearAutoComplete();" style="width:130px">
			<option value="0">Serie...</option>
			<option value="0">Scegli prima un costruttore</option>
			';
			$html .= '
			</select>
			
			<select id="auto_categoria" name="auto_categoria" onchange="clearAutoComplete();" style="width:130px">
			<option value="0">Categoria...</option>
			';
			$categorie = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'category c JOIN '._DB_PREFIX_.'category_lang cl ON c.id_category = cl.id_category WHERE cl.id_lang = '.$context->language->id.' AND c.id_parent = 1 ORDER BY cl.name ASC');
			foreach($categorie as $categoria)
				$html .= '<option value="'.$categoria['id_category'].'">'.$categoria['name'].'</option>';
			$html .= '
			</select>
			
			<select id="auto_fornitore" name="auto_fornitore" onchange="clearAutoComplete(); " style="width:130px">
			<option value="0">Fornitore...</option>
			';
			
			$fornitori = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'supplier WHERE name != "" ORDER BY name ASC');
			foreach($fornitori as $fornitore)
				$html .= '<option value="'.$fornitore['id_supplier'].'">'.$fornitore['name'].'</option>';
			$html .= '
			</select>
			
			<input id="reset" type="button" value="Reset" class="button" onclick="document.getElementById(\'prodotti_online\').checked = true; document.getElementById(\'prodotti_offline\').checked = true; document.getElementById(\'prodotti_old\').checked = true; document.getElementById(\'prodotti_disponibili\').checked = false; document.getElementById(\'auto_categoria\').options[0].selected = true; $(\'#auto_categoria\').select2();  document.getElementById(\'auto_marca\').options[0].selected = true; $(\'#auto_marca\').select2();  
			document.getElementById(\'auto_serie\').options[0].selected = true; $(\'#auto_serie\').select2();  
			document.getElementById(\'auto_fornitore\').options[0].selected = true; $(\'#auto_fornitore\').select2(); $(\'#prodlist\').hide(); $(\'div.autocomplete_list\').hide();" />
			
			<br />
			<input size="123" type="text" value="" id="product_autocomplete_input" style="margin-top:3px; " /> <input id="veditutti2" type="button" value="Cerca" class="button" onclick=\'$("#veditutti").trigger("click");\' /> 
			<input id="veditutti" type="button" value="Cerca" class="button" style="display:none" /> 
			
			<br /><br />
		
			<script type="text/javascript">
			
			/*
				//setup before functions
				var typingTimer;                //timer identifier
				var doneTypingInterval = 3000;  //time in ms, 3 second for example
				var $ainput = $("#product_autocomplete_input");

				//on keyup, start the countdown
				$ainput.on("keyup", function () {
				  clearTimeout(typingTimer);
				  typingTimer = setTimeout(doneTyping, doneTypingInterval);
				});

				//on keydown, clear the countdown 
				$ainput.on("keydown", function () {
				  clearTimeout(typingTimer);
				});

				//user is "finished typing," do something
				function doneTyping () {
					lastKeyPressCode = event.keyCode;
					if(lastKeyPressCode != 37 && lastKeyPressCode != 38 && lastKeyPressCode != 39 && lastKeyPressCode != 40)
						$ainput.trigger("click");
				}
			*/
			
					var formProduct = "";
					var products = new Array();
				</script>
				<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete3.js"></script>
				
				<script type="text/javascript">
		

				$("body").on("click", function (event) {
					 if(event.target.tagName.toUpperCase() == "BODY"  || event.target.id == "auto_marca"   || event.target.id == "auto_serie" || event.target.id == "auto_categoria" || event.target.id == "auto_fornitore" || event.target.id == "veditutti" || event.target.id == "prodotti_online"  || event.target.id == "prodotti_offline" || event.target.id == "prodotti_old"  || event.target.id == "prodotti_disponibili" || event.target.id == "product_autocomplete_input")
					 {
						event.stopPropagation();
						
					 }
					 else
					 {
						$(\'#prodlist\').hide();
						$(\'div.autocomplete_list\').hide();
					 }
				});	
				
					urlToCall = null;
					
					function getExcludeIds() {
						var ids = "";
						ids += $(\'#excludeIds\').val().replace(/\\-/g,\',\').replace(/\\,$/,\'\');
						return ids;
					}
					
					function getOnline()
					{
						if(document.getElementById("prodotti_online").checked == true)
							return 1;
						else
							return 0;
					}
					
					function getOffline()
					{
						if(document.getElementById("prodotti_offline").checked == true)
							return 1;
						else
							return 0;
					}

					function getOld()
					{
						if(document.getElementById("prodotti_old").checked == true)
							return 1;
						else
							return 0;
					}
					
					function getDisponibili()
					{
						if(document.getElementById("prodotti_disponibili").checked == true)
							return 1;
						else
							return 0;
					}
					
					function getCopiaDa()
					{
							return 0;
					}
					
					function getSerie()
					{
						if(document.getElementById("auto_serie").value > 0)
							return document.getElementById("auto_serie").value;
						else
							return 0;
					}
					
					function getMarca()
					{
						if(document.getElementById("auto_marca").value > 0)
							return document.getElementById("auto_marca").value;
						else
							return 0;
					}
					
					function getFornitore()
					{
						if(document.getElementById("auto_fornitore").value > 0)
							return document.getElementById("auto_fornitore").value;
						else
							return 0;
					}
					
					function getCategoria()
					{
						if(document.getElementById("auto_categoria").value > 0)
							return document.getElementById("auto_categoria").value;
						else
							return 0;
					}
					
					function repopulateSeries(id_manufacturer)
					{
						$("#auto_serie").empty();
						$.ajax({
						  url:"ajax.php?repopulate_series=y",
						  type: "POST",
						  data: { id_manufacturer: id_manufacturer
						  },
						  success:function(resp){  
							var newOptions = $.parseJSON(resp);
							
							 $("#auto_serie").append($("<option></option>")
								 .attr("value", "0").text("Serie..."));
							
							$.each(newOptions, function(key,value) {
							  $("#auto_serie").append($("<option></option>")
								 .attr("value", value).text(key));
							});
							
							$("#auto_serie").select2({
								placeholder: "Serie..."
							});
						  },
						  error: function(xhr,stato,errori){
							 alert("Errore: impossibile trovare serie ");
						  }
						});
							
						
					}	
					
					function clearAutoComplete()
					{
						$("#veditutti").trigger("click");
						
					}	
					
					/* function autocomplete */
					$(function() {
					
						$(\'#product_autocomplete_input\')
							.autocomplete(\'ajax_products_list2.php?excludeIds=\'+document.getElementById("excludeIds").value+\'&id_cart='.$_GET['id_cart'].'\', {
								minChars: 0,
								autoFill: false,
								max:5000,
								matchContains: true,
								matchSubset: 0,
								mustMatch:true,
								scroll:true,
								useCache:false,
								cacheLength:1,
								formatItem: function(item) {
							
									var color = "";
									if(item[24] == 1) {
										var color = "; color:red";
									}
									if(item[28] == 0) {
										
										return \'</table><br /><div onclick="return false;" style="position:absolute; z-index:99997; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:180px;float:left;margin-left:10px;">Codice</div></th><th style="width:630px;"><div style="width:260px; float:left;">Desc. prodotto<br /><br /></div></th><th style="width:60px; text-align:right"><div style="width:55px;float:left;">Prezzo</div></th><th style="width:45px; text-align:right"><div style="width:40px;float:left; text-align:right">EZ</div></th><th style="width:45px; text-align:right"><div style="width:40px;float:left; text-align:right">ESP</div></th><th style="width:45px; text-align:right"><div style="width:40px;float:left; text-align:right">INT</div></th><th style="width:45px; text-align:right"><div style="width:40px;float:left; text-align:right">ALN</div></th><th style="width:40px; text-align:right"><div style="width:40px;float:left; text-align:right">ITA</div></th><th style="width:40px; text-align:right"><div style="width:40px;float:left; text-align:right">ASI</div></th><th style="width:40px; text-align:right"><div style="width:40px;float:left; text-align:right">Imp</div></th><th style="width:40px; text-align:right"><div style="width:40px;float:left; text-align:center">Tot</div></th></tr></div><table>\'
										
										;
									}
									else {
										return \'<tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:350px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:40px; text-align:right">\'+item[25]+\'</td><td style="width:40px; text-align:right">\'+item[33]+\'</td><td style="width:40px; text-align:right">\'+item[49]+\'</td><td style="width:40px; text-align:right">\'+item[34]+\'</td><td style="width:40px; text-align:right">\'+item[38]+\'</td><td style="width:40px; text-align:right">\'+item[50]+\'</td><td style="width:40px; text-align:right">\'+item[37]+\'</td><td style="width:40px; text-align:right">\'+item[22]+\'</td></tr>\'
										
										;
										}
								}
								
							}).result(addProduct_TR);
							
								
						
					});
					
					
				
					$("#product_autocomplete_input").focus();
						
					$("#product_autocomplete_input").css(\'width\',\'835px\');
					$("#product_autocomplete_input").keypress(function(event){
								
					  var keycode = (event.keyCode ? event.keyCode : event.which);
					  if (keycode == "13") {
							console.log("CLICCO RICERCA");
						  $("#veditutti").trigger("click");
						event.preventDefault();
						event.stopPropagation();    
					  }
					});
							
						
					
				</script>
				
				<script type="text/javascript">
				
				$("#veditutti2").click ( function (zEvent) {
					
					/*	$("body").trigger("click");
						$("body").trigger("click");
						
						
						
						$("#product_autocomplete_input").focus();
					//	$("#product_autocomplete_input").val("");
					//	$("#product_autocomplete_input").val(" ");
					*/
						var press = jQuery.Event("keypress");
						press.which = 13;
						if(document.getElementById(\'product_autocomplete_input\').value == "")
						{
							$("#product_autocomplete_input").val(" ");
						}
						$("#product_autocomplete_input").trigger(press);
					
						$("#product_autocomplete_input").trigger("click");
						$("#product_autocomplete_input").trigger("click");
					//	$("#product_autocomplete_input").val("");
						
				} );


				</script>
				
				KIT <input size="83" type="text" value="" id="product_autocomplete_input_kit" /> <input id="azzera_carrello" type="button" value="Azzera" class="button" onclick="$(\'#tableProducts > tbody\').html(\'\'); calcolaSpedizionePerIndirizzo(); calcolaImporto(367442);" /> <br /><br /> 
			<script type="text/javascript">
					var formProduct = "";
					var products = new Array();
				</script>
				<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete3.js"></script>
				
				
				<script type="text/javascript">
					urlToCall = null;
					
					/* function autocomplete */
					$(function() {
						$(\'#product_autocomplete_input_kit\')
							.autocomplete(\'ajax_products_list2.php?id_bundle=0&bundles=y&id_cart='.$_GET['id_cart'].'\', {
								minChars: 0,
								autoFill: false,
								max:50,
								matchContains: true,
								matchSubset: 0,
								mustMatch:true,
								scroll:true,
								cacheLength:1,
								formatItem: function(item) {
							
											var color = "";
											if(item[24] == 1) {
												var color = "; color:red";
											}
											if(item[28] == 0) {
												return \'</table><div onclick="return false;" style="position:absolute; z-index:99997; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:150px;float:left;margin-left:10px;">Codice</div></th><th style="width:570px;"><div style="width:550px; float:left;">Desc. kit<br /><br /></div></th><th style="width:60px; text-align:right"><div style="width:60px;float:left;">Prezzo</div></th><th style="width:50px; text-align:right"><div style="width:60px;float:left;">Qt. mag.</div></th><th style="width:50px; text-align:right"><div style="width:75px;float:left;">Qt. tot</div></th></tr></div><br />\'
												
												;
											}
											else {
												return \'<tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:550px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
												
												;
												}
											}
								
							}).result(addProduct_TR);
						
						
					});
				
					$("#product_autocomplete_input_kit").css(\'width\',\'810px\');
					$("#product_autocomplete_input_kit").keypress(function(event){
								
					  var keycode = (event.keyCode ? event.keyCode : event.which);
					  if (keycode == "13") {
						$("#product_autocomplete_input_kit").trigger("click");
						event.preventDefault();
						event.stopPropagation();    
					  }
					});
								
					
				</script>';
			
			}
			
			if (strpos($name, 'AMAZON') !== false) {
				$items_number = Db::getInstance()->getValue('
					SELECT count(id_product) 
					FROM '._DB_PREFIX_.'cart_product 
					WHERE id_cart = '.Tools::getValue('id_cart')
				);

				if($items_number == 0)
					$html .= '<h3><a href="recupero-prodotti-amazon.php?id_customer='.Tools::getValue('id_customer').'&amazon_order='.Db::getInstance()->getValue('
						SELECT id_order_amazon 
						FROM '._DB_PREFIX_.'amazon_orders 
						WHERE id_order = '.Tools::getValue('id_cart')
					).'&id_cart='.Tools::getValue('id_cart').'&token='.Tools::getValue('token').'">Carrello senza prodotti. Clicca qui per inviare una richiesta di recupero prodotti ad Amazon</a></h3>';
			}
			
			$gruppo_cliente = Db::getInstance()->getValue('
				SELECT id_default_group 
				FROM '._DB_PREFIX_.'customer 
				WHERE id_customer = '.$customer->id
			);

			// $url_to_update = ('index.php?controller=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&id_cart='.Tools::getValue('id_cart').'&viewcart&tab_name=cartstoken='.$tokenCustomers.'');
			
			if($gruppo_cliente != 3 && $gruppo_cliente != 12)
				$html .= '<div style="position:relative; float:left; margin-top:5px; margin-right:30px"><input type="checkbox" name="prezzi_carrello_c" id="prezzi_carrello_c" '.($prezzi_carrello == 3 && $prezzi_carrello != 12 ? 'checked="checked"' : '').' onclick="if(document.getElementById(\'prezzi_carrello_c15\').checked == true) { var c15 = true; document.getElementById(\'prezzi_carrello_c15\').checked = false } var surec=window.confirm(\'Sei sicuro?\'); if (surec) { if(this.checked) { document.forms[\'products\'].action =  \''.self::$currentIndex.'&id_cart='.$_GET['id_cart'].'&viewcart&id_customer='.$customer->id.'&viewcustomer&cartupdated&token='.$this->token.'&prezzi_carrello=3#modifica-carrello\'; $(\'#Apply\').trigger(\'click\'); } else { document.forms[\'products\'].action =  \''.self::$currentIndex.'&id_cart='.$_GET['id_cart'].'&viewcart&id_customer='.$customer->id.'&viewcustomer&cartupdated&token='.$this->token.'&prezzi_carrello=0#modifica-carrello\'; $(\'#Apply\').trigger(\'click\'); } } else { if(c15 == true) { document.getElementById(\'prezzi_carrello_c15\').checked = true; } return false; }" /> Applica prezzi rivenditore&nbsp;&nbsp;</div>';
			
			if($gruppo_cliente != 3 && $gruppo_cliente != 12)
				$html .= '<div style="position:relative; float:left; margin-top:5px; margin-right:30px"><input type="checkbox" name="prezzi_carrello_c" id="prezzi_carrello_c15" '.($prezzi_carrello == 15 ? 'checked="checked"' : '').' onclick="if(document.getElementById(\'prezzi_carrello_c\').checked == true) { var c = true; document.getElementById(\'prezzi_carrello_c\').checked = false } var surec=window.confirm(\'Sei sicuro?\'); if (surec) { if(this.checked) { document.forms[\'products\'].action =  \''.self::$currentIndex.'&id_cart='.$_GET['id_cart'].'&viewcart&id_customer='.$customer->id.'&viewcustomer&cartupdated&token='.$this->token.'&prezzi_carrello=15#modifica-carrello\'; $(\'#Apply\').trigger(\'click\'); } else { document.forms[\'products\'].action =  \''.self::$currentIndex.'&id_cart='.$_GET['id_cart'].'&viewcart&id_customer='.$customer->id.'&viewcustomer&cartupdated&token='.$this->token.'&prezzi_carrello=0#modifica-carrello\'; $(\'#Apply\').trigger(\'click\'); } } else { if(c == true) { document.getElementById(\'prezzi_carrello_c\').checked = true; } return false; }" /> Applica prezzi Riv. 2 (= Riv. + 3%)&nbsp;&nbsp;</div>';
			
			
			//mysql_select_db(_DB_REV_NAME_);
			
			$html .= 'Letto? '.($visualizzato == 1 ? '<img src="../img/admin/enabled.gif" alt="Si" title="Si" />' : '<img src="../img/admin/red_no.gif" alt="No" title="No" />'); 
			
			$count_revisioni = Db::getInstance()->getValue('
				SELECT count(id_revisione) 
				FROM '._DB_PREFIX_.'cart_revisioni 
				WHERE id_cart = '.Tools::getValue('id_cart').' 
				GROUP BY id_cart
			');

			if($count_revisioni > 0) {
				$html .= '<div id="select-revisioni" style="float:left;margin-bottom:8px;  margin-left:0px; margin-right:30px"><div style="display:block; padding-top:3px; margin-bottom:5px; float:left">Revisioni:</div> <select name="revisioni" id="revisioni" style="position:relative; height:25px; display:block; float:left">';

				$revisioni = Db::getInstance()->executeS('
					SELECT * 
					FROM '._DB_PREFIX_.'cart_revisioni 
					WHERE id_cart = '.Tools::getValue('id_cart').' 
					ORDER BY id_revisione ASC
				');
				
				$rv_tot = 0;
				foreach($revisioni as $revisione) {
					//mysql_select_db(_DB_NAME_);
					$impiegato = Db::getInstance()->getValue('
						SELECT firstname 
						FROM '._DB_PREFIX_.'employee 
						WHERE id_employee = '.$revisione['id_employee'].'
					');
					//mysql_select_db(_DB_REV_NAME_);
					 
					if($revisione['deleted'] == 0)
						$html .= '<option onclick="// window.open(\'ajax.php?id_cart='.$cart->id.'&viewcart&revisione='.$revisione['id_revisione'].'&getPDF=y&token='.$this->token.'\')" value="'.$revisione['id_revisione'].'" '.(Tools::getIsset('vedirevisione') ? (Tools::getValue('vedirevisione') == $revisione['id_revisione'] ? 'selected="selected"' : '') : '').'>'.$revisione['id_cart'].'-'.$rv_tot.' - '.Tools::displayDate($revisione['date_add'], $context->language->id, true).' ('.$impiegato.')</option>';
						
					$rv_tot++;
				}
				$html .= '<option value="totrevisioni" '.(Tools::getIsset('vedirevisione') ? '' : 'selected="selected"').'>'.$revisione['id_cart'].'-'.($rv_tot).' - '.Tools::displayDate($revisione['date_add'], $context->language->id, false).' ('.$impiegato.')</option>';
				
				$html .= '</select>';
				
				/*$html .= '<script type="text/javascript">
				$(document).ready(function() {
					$("#revisioni").val("'.Tools::getValue('vedirevisione').'");
				});
				</script>
				';*/
	
				$html .= '<a href="#" class="button" style="display:block; float:left" onclick="var id_rev = document.getElementById(\'revisioni\').value; if(id_rev == \'totrevisioni\') { window.open(\'index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&tab_name=carts&id_cart='.$cart->id.'&viewcart'.(Tools::getIsset('id_customer') ? '&id_customer='.Tools::getValue('id_customer').'&viewcustomer' : '').'&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($context->employee->id)).'\'); } else { window.open(\'index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&tab_name=carts&id_cart='.$cart->id.'&viewcart&vedirevisione=\'+id_rev+\'&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($context->employee->id)).'\'); }">Apri</a>
				
				<a href="#" class="button" style="display:block; float:left" onclick="var id_rev = document.getElementById(\'revisioni\').value; if(id_rev == \'totrevisioni\') { alert(\'Non puoi cancellare la revisione su cui stai lavorando\'); return false; } else { var surec = window.confirm(\'Sei sicuro/a?\'); if (surec) { window.open(\'index.php?controller=AdminCarts'.(Tools::getValue('tab') == 'AdminCustomers' ? '&customers=y' : '').'&id_cart='.$cart->id.'&viewcart&cancellarevisione=\'+id_rev+\''.(Tools::getIsset('id_customer') ? '&id_customer='.Tools::getValue('id_customer').'&viewcustomer' : '').'&token='.$this->token.'\', \'_self\'); return true; } else { return false; } } "><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a>
				';
				$html .= '</div><div style="clear:both"></div>';
			}
			else  {
				$html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Non ci sono revisioni';
			}

			//mysql_select_db(_DB_NAME_);

			if(Tools::getIsset('vedirevisione')) {
				//mysql_select_db(_DB_REV_NAME_);
				$query = " 
					SELECT cp.*
					FROM "._DB_PREFIX_."cart_product_revisioni cp 
					WHERE id_revisione = ".Tools::getValue('vedirevisione')." 
						AND id_cart=".$_GET['id_cart']." 
					ORDER BY cp.sort_order ASC
				";
			}
			else {
				$query=" 
					SELECT cp.*,t.rate AS tax_rate,p.quantity AS stock,p.id_tax_rules_group,p.id_manufacturer,p.price AS vendita,p.price AS product_price,p.listino AS listino,p.wholesale_price AS acquisto,p.sconto_acquisto_1 AS sc_acq_1, p.sconto_acquisto_2 AS sc_acq_2, p.sconto_acquisto_3 AS sc_acq_3, p.quantity AS qt_tot, p.stock_quantity AS magazzino, p.supplier_quantity AS allnet, p.esprinet_quantity AS esprinet, p.attiva_quantity AS attiva, p.itancia_quantity AS itancia, p.ordinato_quantity AS qt_ordinato, p.impegnato_quantity AS qt_impegnato, p.arrivo_quantity AS qt_arrivo, p.arrivo_esprinet_quantity AS qt_arrivo_esprinet, p.arrivo_attiva_quantity AS qt_arrivo_attiva, p.reference AS product_reference,pl.name AS product_name,i.id_image 
					FROM "._DB_PREFIX_."cart_product cp left join "._DB_PREFIX_."product p ON cp.id_product=p.id_product left join (select * from "._DB_PREFIX_."image 
					WHERE cover = 1 group by id_product) i ON i.id_product = p.id_product left join "._DB_PREFIX_."product_lang pl ON cp.id_product=pl.id_product 
					LEFT JOIN `"._DB_PREFIX_."tax_rule` tr ON (p.`id_tax_rules_group` = tr.`id_tax_rules_group`
						AND tr.`id_country` = ".(int)Country::getDefaultCountryId()."
						AND tr.`id_state` = 0)
					LEFT JOIN `"._DB_PREFIX_."tax` t ON (t.`id_tax` = tr.`id_tax`)			
					WHERE pl.id_lang = ".$context->language->id." 
						AND id_cart=".$_GET['id_cart']." ORDER BY cp.sort_order ASC
				";
			}
				 
			/*if(Tools::getIsset('vedirevisione')) {
				//mysql_select_db(_DB_NAME_);
			}*/
				
			$canoni = '';
			$prodotti = '';
			$totale = 0;
			$totaleprovvigioni = 0;
			$res1 = Db::getInstance()->executeS($query);
			foreach($res1 as $products) {
				
				$print_products = '';
				if(Tools::getIsset('vedirevisione')) {
					
					$products['tax_rate'] = Db::getInstance()->getValue('
						SELECT rate AS tax_rate 
						FROM '._DB_PREFIX_.'tax 
						WHERE id_tax = 1
					');

					$products['stock'] = Db::getInstance()->getValue('
						SELECT quantity 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);

					$products['id_tax_rules_group'] = Db::getInstance()->getValue('
						SELECT id_tax_rules_group 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);

					$products['product_price'] = Db::getInstance()->getValue('
						SELECT price 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);

					$products['listino'] = Db::getInstance()->getValue('
						SELECT listino 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);

					$products['vendita'] = Db::getInstance()->getValue(
						'SELECT price 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);

					$products['acquisto'] = Db::getInstance()->getValue('
						SELECT wholesale_price 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);
					
					$products['sc_acq_1'] = Db::getInstance()->getValue('
						SELECT sconto_acquisto_1 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);

					$products['sc_acq_2'] = Db::getInstance()->getValue('
						SELECT sconto_acquisto_2 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);

					$products['sc_acq_3'] = Db::getInstance()->getValue('
						SELECT sconto_acquisto_3 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);
					
					/*if($context->employee->id_profile == 7) {
						$products['acquisto'] = 0;
						$products['sc_acq_1'] = 0;
						$products['sc_acq_2'] = 0;
						$products['sc_acq_3'] = 0;
					}*/

					$products['qt_tot'] = Db::getInstance()->getValue('
						SELECT quantity 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);

					$products['magazzino'] = Db::getInstance()->getValue('
						SELECT stock_quantity 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);

					$products['allnet'] = Db::getInstance()->getValue('
						SELECT supplier_quantity 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);

					$products['esprinet'] = Db::getInstance()->getValue('
						SELECT esprinet_quantity 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);

					$products['attiva'] = Db::getInstance()->getValue('
						SELECT attiva_quantity 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);

					$products['itancia'] = Db::getInstance()->getValue('
						SELECT itancia_quantity 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);

					$products['qt_ordinato'] = Db::getInstance()->getValue('
						SELECT ordinato_quantity 
						FROM '._DB_PREFIX_.'product 
						HERE id_product = '.$products['id_product']
					);

					$products['qt_impegnato'] = Db::getInstance()->getValue('
						SELECT impegnato_quantity 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);

					$products['qt_arrivo'] = Db::getInstance()->getValue('
						SELECT arrivo_quantity 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);

					$products['qt_arrivo_esprinet'] = Db::getInstance()->getValue('
						SELECT arrivo_esprinet_quantity 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);

					$products['qt_arrivo_attiva'] = Db::getInstance()->getValue('
						SELECT arrivo_attiva_quantity 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);

					$products['product_reference'] = Db::getInstance()->getValue('
						SELECT reference 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$products['id_product']
					);

					$products['product_name'] = Db::getInstance()->getValue('
						SELECT name 
						FROM '._DB_PREFIX_.'product_lang 
						WHERE id_lang = '.$context->language->id.' 
							AND id_product = '.$products['id_product']
					);
				}	
				
				
				$tax_rate=$products['tax_rate'];
				$sc_qta_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM "._DB_PREFIX_."specific_price WHERE specific_price_name = 'sc_qta_1' AND id_product = '".$products['id_product']."'");
				$sc_qta_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM "._DB_PREFIX_."specific_price WHERE specific_price_name = 'sc_qta_2' AND id_product = '".$products['id_product']."'");
				$sc_qta_3_r = Db::getInstance()->getRow("SELECT price, reduction FROM "._DB_PREFIX_."specific_price WHERE specific_price_name = 'sc_qta_3' AND id_product = '".$products['id_product']."'");	
				$sc_riv_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM "._DB_PREFIX_."specific_price WHERE specific_price_name = 'sc_riv_1' AND id_product = '".$products['id_product']."'");	
				$sc_riv_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM "._DB_PREFIX_."specific_price WHERE specific_price_name = 'sc_riv_2' AND id_product = '".$products['id_product']."'");
				$sc_riv_3_r = Db::getInstance()->getRow("SELECT price, reduction FROM "._DB_PREFIX_."specific_price WHERE specific_price_name = 'sc_riv_3' AND id_product = '".$products['id_product']."'");
				
				$sc_qta_1 = $sc_qta_1_r['price'] - ($sc_qta_1_r['price'] * $sc_qta_1_r['reduction']);
				$sc_qta_2 = $sc_qta_2_r['price'] - ($sc_qta_2_r['price'] * $sc_qta_2_r['reduction']);
				$sc_qta_3 = $sc_qta_3_r['price'] - ($sc_qta_3_r['price'] * $sc_qta_3_r['reduction']);
				$sc_riv_1 = $sc_riv_1_r['price'] - ($sc_riv_1_r['price'] * $sc_riv_1_r['reduction']);
				$sc_riv_2 = $sc_riv_2_r['price'] - ($sc_riv_2_r['price'] * $sc_riv_2_r['reduction']);
				$sc_riv_3 = $sc_riv_3_r['price'] - ($sc_riv_3_r['price'] * $sc_riv_3_r['reduction']);
				
				$unitario = Db::getInstance()->getValue("SELECT price FROM "._DB_PREFIX_."product WHERE id_product = '".$products['id_product']."'");
				
				$sc_qta_1_q = Db::getInstance()->getValue("SELECT from_quantity FROM "._DB_PREFIX_."specific_price WHERE specific_price_name = 'sc_qta_1' AND id_product = '".$products['id_product']."'");
				$sc_qta_2_q = Db::getInstance()->getValue("SELECT from_quantity FROM "._DB_PREFIX_."specific_price WHERE specific_price_name = 'sc_qta_2' AND id_product = '".$products['id_product']."'");
				$sc_qta_3_q = Db::getInstance()->getValue("SELECT from_quantity FROM "._DB_PREFIX_."specific_price WHERE specific_price_name = 'sc_qta_3' AND id_product = '".$products['id_product']."'");
				
				$sc_riv_1_q = Db::getInstance()->getValue("SELECT from_quantity FROM "._DB_PREFIX_."specific_price WHERE specific_price_name = 'sc_riv_1' AND id_product = '".$products['id_product']."'");
				$sc_riv_2_q = Db::getInstance()->getValue("SELECT from_quantity FROM "._DB_PREFIX_."specific_price WHERE specific_price_name = 'sc_riv_2' AND id_product = '".$products['id_product']."'");
				$sc_riv_3_q = Db::getInstance()->getValue("SELECT from_quantity FROM "._DB_PREFIX_."specific_price WHERE specific_price_name = 'sc_riv_3' AND id_product = '".$products['id_product']."'"); 
				
				$speciale = Product::trovaPrezzoSpeciale($products['id_product'], 1, 0);
	
				if($speciale < $unitario && $speciale != 0)
					$unitario = $speciale;
				
				if($creato_da == 0 && $impiegato == 0)
					$products['sc_qta'] = 1;
				
				if($customer->id_default_group == 3) {
					$ctrl_sc_qt = Db::getInstance()->getValue("SELECT count(id_product) FROM "._DB_PREFIX_."specific_price WHERE id_product = ".$products['id_product']." AND (specific_price_name = 'sc_riv_1' OR specific_price_name = 'sc_riv_2')");
					
					if($ctrl_sc_qt > 0) {
						$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['id_product'].']" name="usa_sconti_quantita['.$products['id_product'].']" type="checkbox" '.($products['sc_qta'] == 0 ? '' : 'checked="checked"').' onclick="ristabilisciPrezzo('.$products['id_product'].'); calcolaImporto('.$products['id_product'].'); " />';
					}
					else {
						$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['id_product'].']" name="usa_sconti_quantita['.$products['id_product'].']" type="hidden" value="0" />';
					}
				}
				
				if($prezzi_carrello == 1599999)
					$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['id_product'].']" name="usa_sconti_quantita['.$products['id_product'].']" type="hidden" value="0" />';
				else if($prezzi_carrello ==  3 || $prezzi_carrello == 15)
					$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['id_product'].']" name="usa_sconti_quantita['.$products['id_product'].']" type="checkbox" style="display:none" '.($products['sconto_extra'] == 0 ? 'checked="checked"' : '').' />';
				
				else if($customer->id_default_group == 3 || $customer->id_default_group == 22) {
					$ctrl_sc_qt = Db::getInstance()->getValue("
						SELECT count(id_product) 
						FROM "._DB_PREFIX_."specific_price 
						WHERE id_product = ".$products['id_product']." 
							AND (specific_price_name = 'sc_riv_1' 
							OR specific_price_name = 'sc_riv_2' 
							OR specific_price_name = 'sc_riv_3')
					");
					
					if($ctrl_sc_qt > 0) {
						$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['id_product'].']" name="usa_sconti_quantita['.$products['id_product'].']" type="checkbox" '.($products['sc_qta'] == 0 ? '' : 'checked="checked"').' onclick="ristabilisciPrezzo('.$products['id_product'].'); calcolaImporto('.$products['id_product'].'); " />';
					}
					else {
						$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['id_product'].']" name="usa_sconti_quantita['.$products['id_product'].']" type="hidden" value="0" />';
					}
				}
				else {
					$ctrl_sc_qt = Db::getInstance()->getValue("
						SELECT count(id_product) 
						FROM "._DB_PREFIX_."specific_price 
						WHERE id_product = ".$products['id_product']." 
							AND (specific_price_name = 'sc_qta_1' 
							OR specific_price_name = 'sc_qta_2' 
							OR specific_price_name = 'sc_qta_3')
					");
					
					if($ctrl_sc_qt > 0) {
						$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['id_product'].']" name="usa_sconti_quantita['.$products['id_product'].']" type="checkbox" '.($products['sc_qta'] == 0 ? '' : 'checked="checked"').' onclick="ristabilisciPrezzo('.$products['id_product'].'); calcolaImporto('.$products['id_product'].'); " />';
					}
					else {
						$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['id_product'].']" name="usa_sconti_quantita['.$products['id_product'].']" type="hidden" value="0" />';
					}
				}

				/*if($cart->date_upd > '2014-10-25') {
					$products['acquisto'] = $products['prezzo_acquisto'];
				}
				else {
					if($products['prezzo_acquisto'] > 0) */
						$products_acquisto = $products['prezzo_acquisto'];
				//}
				
				if($customer->id == 44431) { 
					$products['price'] = 0;
					$products['free'] = 0;
					$products['name'] = Db::getInstance()->getValue('
						SELECT name 
						FROM '._DB_PREFIX_.'product_lang 
						WHERE id_lang = '.$context->language->id.' 
							AND id_product = '.$products['id_product']
					);
				}

				$rebate = Db::getInstance()->getRow('
					SELECT * 
					FROM product_esolver 
					WHERE id_product = '.$products['id_product']
				);
				
				$ws_base = Db::getInstance()->getValue("
					SELECT wholesale_price 
					FROM "._DB_PREFIX_."specific_price_wholesale spw 
					WHERE spw.id_product = '".$products['id_product']."' 
						AND spw.from < '".date('Y-m-d H:i:s')."' 
						AND spw.to > '".date('Y-m-d H:i:s')."' 
						AND (spw.pieces = '' OR spw.pieces > 0)
				");
				

					$ws_base_2 = ($products['listino']*(100-$products['sc_acq_1'])/100)*((100-$products['sc_acq_2'])/100)*((100-$products['sc_acq_3'])/100) * ((100-$rebate['rebate_1'])/100)*((100-$rebate['rebate_2'])/100)*((100-$rebate['rebate_3'])/100);
					
					if($ws_base == 0 || $ws_base == '')
						$ws_base = $ws_base_2;
					else
					{
						if($ws_base > $ws_base_2)
							$ws_base = $ws_base_2;
					}
				
				if($prezzi_carrello == 3 && $products['sconto_extra'] > 0)
					$vnd_base = Product::trovaMigliorPrezzo($products['id_product'],1,$products['quantity']);
				else
					$vnd_base = Product::trovaMigliorPrezzo($products['id_product'],$customer->id_default_group,$products['quantity']);
				
				$base_vnd_offerta = ($products['price'] == 0 ? $vnd_base : round($products['price'] / (1- ($products['sconto_extra']/100)),2));
				
				
			   	$wholesale_price = ($products_acquisto > 0 ? $products_acquisto : ($products['no_acq'] == 1 ? $products_acquisto : ($products['listino']*(100-$products['sc_acq_1'])/100)*((100-$products['sc_acq_2'])/100)*((100-$products['sc_acq_3'])/100) * ((100-$rebate['rebate_1'])/100)*((100-$rebate['rebate_2'])/100)*((100-$rebate['rebate_3'])/100) ));
			   
			  	$productprice = ($products['price'] == 0 && $products['free'] == 0 && $products['sc_qta'] == 1 ? number_format(Product::trovaMigliorPrezzo($products['id_product'],$customer->id_default_group,$products['quantity']) - (Product::trovaMigliorPrezzo($products['id_product'],$customer->id_default_group,$products['quantity']) * ($products['sconto_extra']/100)),2,",","") : ($products['price'] == 0 && $products['free'] == 0 && $products['sc_qta'] == 0 ? number_format(Product::trovaMigliorPrezzo($products['id_product'],1,1) - ( Product::trovaMigliorPrezzo($products['id_product'],1,1) * ($products['sconto_extra'] / 100)),2,",","")  : number_format($products['price'],2,",","")));
			  
				$prezzo_partenza = ($products['price'] == 0 && $products['free'] == 0 && $products['sc_qta'] == 1 ? Product::trovaMigliorPrezzo($products['id_product'],$customer->id_default_group,$products['quantity']) - (Product::trovaMigliorPrezzo($products['id_product'],$customer->id_default_group,$products['quantity']) * ($products['sconto_extra']/100)) : ($products['price'] == 0 && $products['free'] == 0 && $products['sc_qta'] == 0 ? Product::trovaMigliorPrezzo($products['id_product'],1,1) - ( Product::trovaMigliorPrezzo($products['id_product'],1,1) * ($products['sconto_extra'] / 100))  : $products['price']));
			  
				if($context->employee->id_profile == 7) {
					  /*$ws_base = 0;
					  $wholesale_price = 0;*/
				}

				if($order->id && !Tools::getIsset('vedirevisione')) {
					if($prezzo_partenza == 0)
						$prezzo_partenza = Db::getInstance()->getValue('
							SELECT (price - ((price*sconto_extra)/100)) 
							FROM '._DB_PREFIX_.'cart_product 
							WHERE id_product = '.$products['id_product'].' AND id_cart = '.Tools::getValue('id_cart')
						);
							
					if($prezzo_partenza == 0) {	
						$is_product = Db::getInstance()->getValue('
							SELECT product_id 
							FROM '._DB_PREFIX_.'order_detail 
							WHERE product_id = '.$products['id_product']
						);
						
						if($is_product > 0) {
							$prezzo_partenza = number_format(Db::getInstance()->getValue('
								SELECT (product_price - ((product_price*reduction_percent)/100)) price 
								FROM '._DB_PREFIX_.'order_detail 
								WHERE product_id = '.$products['id_product'].' 
									AND id_order = '.$order->id),2,',','
							');

							$productprice = $prezzo_partenza;
						}	
					}
				}
					
				$margine = $prezzo_partenza - $wholesale_price;
				$marginalita = ((($prezzo_partenza - $wholesale_price)*100)/$prezzo_partenza);
				if($prezzo_partenza < 0)
					$marginalita = 0-$marginalita;
				
				if($marginalita > 20)
					$margine_provvigione = $margine / 100 * 30;
				else
					$margine_provvigione = $margine / 100 * 25;
				
				$provvigione = $margine_provvigione * 100 / $prezzo_partenza;
		
				$provvigione_personalizzata = Db::getInstance()->getValue('SELECT provvigione FROM '._DB_PREFIX_.'product WHERE id_product = "'.$obj->id.'"');
				
				if($provvigione_personalizzata > 0)
					$provvigione = $provvigione_personalizzata;
				
				
				$provvigione = $prezzo_partenza * ($provvigione/100);
				
				$totaleprovvigioni += $provvigione;
				
			//if($context->employee->id == 6 || $context->employee->id == 22)	
			//{
				$current_product = new Product($products['id_product']);
				$suggeriti .= '<div style="display:none"><div id="prodotti_suggeriti_'.$products['id_product'].'"><h2>Prodotti suggeriti</h2><table class="table"><tr><th></th><th>Codice</th><th>Desc. prodotto</th><th>Prezzo</th><th>EZ</th><th>Ord.</th><th></th></tr>';
				$products_suggeriti = Db::getInstance()->executeS('SELECT p.id_product, p.id_manufacturer, p.reference, p.supplier_reference, p.stock_quantity AS magazzino, sku_amazon, fnsku, asin, scorta_minima_amazon, impegnato_amazon, p.scorta_minima, pl.name AS nome_prodotto, p.ordinato_quantity AS qt_ordinato, p.supplier_quantity AS allnet, p.esprinet_quantity AS esprinet, p.attiva_quantity AS attiva, p.itancia_quantity AS itancia, p.quantity AS totale, pl.name, m.name AS costruttore FROM '._DB_PREFIX_.'product p JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product = pl.id_product JOIN '._DB_PREFIX_.'manufacturer m ON p.id_manufacturer = m.id_manufacturer LEFT JOIN '._DB_PREFIX_.'supplier s ON p.id_supplier = s.id_supplier WHERE pl.id_lang = '.$context->language->id.' AND stock_quantity + ordinato_quantity > 0 AND p.id_category_default = '.$current_product->id_category_default.' AND p.id_product NOT IN (SELECT id_product FROM '._DB_PREFIX_.'cart_product WHERE id_cart = '.$_GET['id_cart'].') AND p.active = 1 GROUP BY p.id_product ORDER BY reference asc LIMIT 25');			

								
				
				foreach($products_suggeriti as $suggerito) {
					$immagine_suggerito = Db::getInstance()->getValue('
						SELECT id_image 
						FROM '._DB_PREFIX_.'image 
						WHERE id_product = '.$suggerito['id_product'].' 
							AND cover = 1
						');
				
					if($prezzi_carrello_corrente == 3) {	
						$suggerito_prezzo = Product::trovaMigliorPrezzo($suggerito['id_product'],3,999999);
					}
					else if($prezzi_carrello_corrente == 15) {
						$suggerito_prezzo = Product::trovaMigliorPrezzo($suggerito['id_product'],3,999999);
						
						
						$suggerito_prezzo = $suggerito_prezzo+(($suggerito_prezzo/100)*3);
					}	
					else
						$suggerito_prezzo = Product::trovaMigliorPrezzo($suggerito['id_product'],$customer->id_default_group,999999);
					
					if($immagine_suggerito > 0)
						$src_img_suggerito = 'http://www.ezdirect.it/img/p/'.$suggerito['id_product'].'-'.$immagine_suggerito.'-small.jpg';
					else
						$src_img_suggerito = 'http://www.ezdirect.it/img/m/'.$suggerito['id_manufacturer'].'-small.jpg';	
					
					// riga sotto: '.Product::showProductTooltip($suggerito['id_product']).'
					
					$suggeriti .= '<tr><td style="text-align:left"><img src="'.$src_img_suggerito.'" alt="" title="" /></td><td style="text-align:left"> <span style="cursor:pointer" class="span-reference" title=""><a href="index.php?controller=AdminProducts&id_product='.$suggerito['id_product'].'&updateproduct&token='.$tokenProducts.'" target="_blank">'.$suggerito['reference'].'</a></td><td style="text-align:left">'.$suggerito['nome_prodotto'].'</td><td style="text-align:right">'.number_format($suggerito_prezzo,2,",",".").'</td><td style="text-align:right">'.$suggerito['magazzino'].'</td><td style="text-align:right">'.$suggerito['qt_ordinato'].'</td><td><button class="button" onclick="aggiungiSuggerito('.$suggerito['id_product'].')">Aggiungi al carrello</button></td>';
				}
				
				$suggeriti .= '</table></div></div>';
				
				$link_suggeriti = '<span style="cursor:pointer" class="span-reference-2" data-tooltip-content="#prodotti_suggeriti_'.$products['id_product'].'"><a href="javascript:void()"><img src="https://www.ezdirect.it/img/admin/tab-products.gif" alt="Prodotti suggeriti" title="Prodotti suggeriti" /></a>';
				
				$suggerimenti = $link_suggeriti.$suggeriti;
			//}
				
				
			  $print_products .=  '<tr id="tr_'.$products['id_product'].'" '.($marginalita < 10 && $prezzo_partenza > 0 ? "style='background-color:#ffcccc'" : "").'>';
			
			  $print_products .=  '<td><img style="width:30px; height:30px; float:left" src="http://www.ezdirect.it/img/'.($products['id_image'] > 0 ? 'p/'.$products['id_product'].'-'.$products['id_image'] : 'm/'.$products['id_manufacturer']).'-small.jpg" alt="" /><input type="hidden" name="section['.$products['id_product'].']" value="'.$products['section'].'" /></td><td> <span style="cursor:pointer" class="span-reference tooltip_prodotto" title="'.Product::showProductTooltip($products['id_product']).'"><a href="index.php?controller=AdminProducts&id_product='.$products['id_product'].'&updateproduct&token='.$tokenProducts.'" target="_blank">  '.$products['product_reference'].'</a> </span>
			  </td>';
			  $print_products .=  ' <td>'.$suggerimenti.'</td> <td><input size="31" name="product_name['.$products['id_product'].']" '.($order->id? 'readonly="readonly"' : '').' type="text" value="'.(empty($products['name']) ? $products['product_name'] : $products['name']).'" /> </td>';
			  
			  $tolleranza = $base_vnd_offerta/100;
			  
			  $print_products .=  '  <td><input style="text-align:right;" name="product_quantity['.$products['id_product'].']" id="product_quantity['.$products['id_product'].']" type="text" '.($order->id  || $products['id_product'] == 3360639 || $products['id_product'] == 3360649 ? 'readonly="readonly"' : '').' size="3" value="'.$products['quantity'].'" onkeyup="if (isArrowKey(event)) return; if (document.getElementById(\'usa_sconti_quantita['.$products['id_product'].']\').checked == true) { calcolaPrezzoScontoExtra('.$products['id_product'].', \'sconto\'); }  calcolaImporto('.$products['id_product'].'); '.($context->employee->id_profile == 7 ? 'calcolaProvvigione('.$products['id_product'].');' : '').'" /></td>';
				
			  $print_products .=  '  <td><input size="7" style="text-align:right" onkeyup="if(stopKeyUp == false) { document.getElementById(\'usa_sconti_quantita['.$products['id_product'].']\').checked = false; calcolaPrezzoScontoExtra('.$products['id_product'].', \'inverso\'); calcolaImporto('.$products['id_product'].'); '.($context->employee->id_profile == 7 ? 'calcolaProvvigione('.$products['id_product'].');' : '').' }" name="product_price['.$products['id_product'].']" id="product_price['.trim($products['id_product']).']" type="text" '.($order->id || $products['id_product'] == 3360639 || $products['id_product'] == 3360649 ? 'readonly="readonly"' : '').' value="'.$productprice.'" /><div style="float:right" id="controllo_vendita_'.$products['id_product'].'">'.((( ((str_replace(",",".",$productprice) > $unitario) && $unitario > 0) || ($vnd_base - $base_vnd_offerta > $tolleranza || $vnd_base - $base_vnd_offerta < (0-$tolleranza))) && $unitario > 0) ? '<span class="span-reference" title="
			  '.(str_replace(",",".",$productprice) > $unitario && $unitario > 0 ? 'ATTENZIONE stai vendendo il prodotto a un prezzo maggiore rispetto al prezzo standard unitario di '.number_format($unitario,2,",","") : '').'
			  
			  '.(((($vnd_base - $base_vnd_offerta > $tolleranza || $vnd_base - $base_vnd_offerta < (0-$tolleranza))) && $unitario > 0) ? 'ATTENZIONE stai vendendo a '.$productprice.' '.($products['sconto_extra'] > 0 ? ' con sconto del '.number_format($products['sconto_extra'],2,",","").'%, quindi il prezzo unitario senza sconto ('.number_format($base_vnd_offerta,2,",","").') &egrave; diverso dal prezzo standard unitario, di '.number_format($vnd_base,2,",","") : ' quindi a un prezzo diverso dal prezzo standard unitario, di '.number_format($vnd_base,2,",","")).'' : '').'
			  "><img class="img_control" src="../img/admin/error.png" style="width:15px; padding:0px; float:right" alt="ATTENZIONE" /></span>'  : '').'</div></td>';
			  /*$print_products .=  '  <td>'.$products['tax_rate'].'%</td>';
			  $print_products .=  '  <td>'.number_format($products['product_price']*(1+$products['tax_rate']/100),3, ',', '').'</td>';  */
			  
			   $print_products .=  '  <td style="text-align:right">';
			  
			  // le tre righe seguenti introdotte per evitare casini sugli sconti extra derivanti da cambi di gruppo o manovre strane
				$real_sconto_extra = 100 * ($unitario - str_replace(",",".",$productprice)) / $unitario;
				if($real_sconto_extra != $products['sconto_extra'] && $products['sc_qta'] == 0)
					$products['sconto_extra'] = $real_sconto_extra;
			   
			   
			   
			   $print_products .=  '<input style="text-align:right'.($unitario == 0 ? ';display:none' : '').'" class="sconto_extra" name="sconto_extra['.$products['id_product'].']" id="sconto_extra['.$products['id_product'].']" type="text" '.($order->id? 'readonly="readonly"' : '').' size="2" value="'.number_format($products['sconto_extra'],2,",","").'" onkeyup="if (isArrowKey(event)) return; document.getElementById(\'usa_sconti_quantita['.$products['id_product'].']\').checked = false; calcolaPrezzoScontoExtra('.$products['id_product'].', \'sconto\'); calcolaImporto('.$products['id_product'].'); '.($context->employee->id_profile == 7 ? 'calcolaProvvigione('.$products['id_product'].');' : '').'" /></td>';
			   
			 
			   $print_products .=  '<td>
			  '.($context->employee->id_profile == 7 ? '<input type="hidden" class="wholesale_price_class"  name="wholesale_price['.$products['id_product'].']" id="wholesale_price['.$products['id_product'].']" value="'. number_format(round($wholesale_price,2), 2, ',', '').'" /><input type="hidden" style="text-align:right" size="7" readonly="readonly" name="provvigione['.$products['id_product'].']" id="provvigione['.$products['id_product'].']" class="provvigione" value="'.number_format(round($provvigione,2),2,",","").'" />' : '<input type="text" size="7" class="wholesale_price_class" '.($order->id > 0 ? 'readonly="readonly"' : '').'  style="text-align:right" onkeyup=" calcolaImporto('.$products['id_product'].');" onfocusout="if(this.value == \'\') { this.value = \'0\'}" name="wholesale_price['.$products['id_product'].']" id="wholesale_price['.$products['id_product'].']" value="'. number_format(round($wholesale_price,2), 2, ',', '').'" />
			   '.(number_format(round($ws_base,2),2,",","") != number_format(round($wholesale_price,2),2,",","") ? '<span class="span-reference" title="ATTENZIONE! Il prezzo d\'acquisto di questo prodotto sul CRM ('.number_format($ws_base,2,",","").') &egrave; diverso dal prezzo d\'acquisto impostato nell\'offerta  ('.number_format($wholesale_price,2,",","").')"><img src="../img/admin/error.png" alt="Attenzione prezzi acquisto"  style=\'padding:0px; width:15px; float:right\' /></span>' : '').'').'
			   </td>';
			   
			   //javascript:document.getElementById(\'usa_sconti_quantita['.$products['id_product'].']\').checked = false;
			   
				$print_products .=  '  <td style="text-align:right">
				<input type="hidden" name="sconto_acquisto_1['.$products['id_product'].']" id="sconto_acquisto_1['.$products['id_product'].']" value="'.$products['sc_acq_1'].'" />
				<input type="hidden" name="sconto_acquisto_2['.$products['id_product'].']" id="sconto_acquisto_2['.$products['id_product'].']" value="'.$products['sc_acq_2'].'" />
				<input type="hidden" name="sconto_acquisto_3['.$products['id_product'].']" id="sconto_acquisto_3['.$products['id_product'].']" value="'.$products['sc_acq_3'].'" />
				';
				
				$print_products .=  '
				
				<input type="hidden" class="prezzoacquisto '.($products['section'] == 1 ? 'prezzoacquisto_ezcloud' : 'prezzoacquisto_prodotti').'" name="totaleacquisto['.$products['id_product'].']" id="totaleacquisto['.$products['id_product'].']" value="'.number_format(round($wholesale_price*$products['quantity'],2), 2, ',', '').'" />
				';
				
				$totaleacquisti += $wholesale_price*$products['quantity'];
				
				$impImporto = ($products['price'] == 0 && $products['free'] == 0 && $products['sc_qta'] == 1 ? Product::trovaMigliorPrezzo($products['id_product'],$customer->id_default_group,$products['quantity']) - (Product::trovaMigliorPrezzo($products['id_product'],$customer->id_default_group,$products['quantity']) * ($products['sconto_extra']/100)) : ($products['price'] == 0 && $products['free'] == 0 && $products['sc_qta'] == 0 ? Product::trovaMigliorPrezzo($products['id_product'],1,1) - ( Product::trovaMigliorPrezzo($products['id_product'],1,1) * ($products['sconto_extra'] / 100))  : $products['price']));
				
				if($order->id)
					$impImporto = $prezzo_partenza;
				
				$print_products .=  '<span id="spanmarginalita['.$products['id_product'].']" '. ($context->employee->id_profile == 7 ? 'style="display:none"' : '').'>'. ($context->employee->id_profile == 7 ? '' : number_format($marginalita, 2, ',', '').'%').'</span></td>';
			  $print_products .=  '  <td style="text-align:center" style="width:30px">'.$usa_sconti_quantita.' <input id="impImporto['.$products['id_product'].']" class="importo '.($products['section'] == 1 ? 'importo_ezcloud' : 'importo_prodotti').'" type="hidden" value="'.$impImporto*$products['quantity'].'" /></td>';
			  $print_products .=  '  <td style="text-align:right" id="valoreImporto['.$products['id_product'].']">
			 
			  '.number_format(($prezzo_partenza)*$products['quantity'],2, ',', '').'
							 
			  
			  
			  </td>';  
			   $totale += ($prezzo_partenza)*$products['quantity'];
				   
				if($products['section'] == 1) {
				   $totale_ezcloud += ($prezzo_partenza)*$products['quantity'];
				   $totaleacquisti_ezcloud += $wholesale_price*$products['quantity'];
			    }
			   	else {
				   $totale_prodotti += ($prezzo_partenza)*$products['quantity'];
				   $totaleacquisti_prodotti += $wholesale_price*$products['quantity'];
			   	}
			   
				   
				  /*$print_products .=  '  <td>'.number_format($products['product_price']*$products['quantity']*(1+$products['tax_rate']/100),2, ',', '').'</td>';  */
				  
				  $print_products .=  '<td class="pointer dragHandle center" style="background:url(../img/admin/up-and-down.gif) no-repeat center;">
				  <input type="hidden" name="sort_order['.$products['id_product'].']" /></td>';
				  
				  $print_products .=  '  <td style="text-align:center">
				  <a style="cursor:pointer" onclick="delProduct30('.$products['id_product'].'); togliImporto('.$products['id_product'].'); "><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a>
				  </td>';
				  $print_products .=  '  <input name="product_id['.$products['id_product'].']" id="product_id_'.$products['id_product'].'" type="hidden" value="'.$products['id_product'].'" class="product_ids" />';
				  
					
					$print_products .=  '<input type="hidden" id="unitario['.$products['id_product'].']" value="'.$unitario.'" />';
					$print_products .=  '<input type="hidden" id="sc_qta_1['.$products['id_product'].']" value="'.$sc_qta_1.'" />';
					$print_products .=  '<input type="hidden" id="sc_qta_2['.$products['id_product'].']" value="'.$sc_qta_2.'" />';
					$print_products .=  '<input type="hidden" id="sc_qta_3['.$products['id_product'].']" value="'.$sc_qta_3.'" />';
					$print_products .=  '<input type="hidden" id="sc_riv_1['.$products['id_product'].']" value="'.$sc_riv_1.'" />';
					$print_products .=  '<input type="hidden" id="sc_riv_2['.$products['id_product'].']" value="'.$sc_riv_2.'" />';
					$print_products .=  '<input type="hidden" id="sc_riv_3['.$products['id_product'].']" value="'.$sc_riv_3.'" />';
	
					$print_products .=  '<input type="hidden" id="sc_qta_1_q['.$products['id_product'].']" value="'.$sc_qta_1_q.'" />';
					$print_products .=  '<input type="hidden" id="sc_qta_2_q['.$products['id_product'].']" value="'.$sc_qta_2_q.'" />';
					$print_products .=  '<input type="hidden" id="sc_qta_3_q['.$products['id_product'].']" value="'.$sc_qta_3_q.'" />';
					$print_products .=  '<input type="hidden" id="sc_riv_1_q['.$products['id_product'].']" value="'.$sc_riv_1_q.'" />';
					$print_products .=  '<input type="hidden" id="sc_riv_2_q['.$products['id_product'].']" value="'.$sc_riv_2_q.'" />';
					$print_products .=  '<input type="hidden" id="sc_riv_3_q['.$products['id_product'].']" value="'.$sc_riv_3_q.'" />';
					
					$print_products .=  '<input type="hidden" id="oldQuantity['.$products['id_product'].']" value="'.$products['quantity'].'" />';
					$print_products .=  '<input type="hidden" id="oldPrice['.trim($products['id_product']).']" value="'.
				  ($products['price'] == 0 && $products['free'] == 0 ? ($customer->id_default_group == 3 ? $sc_riv_1 : ($customer->id_default_group == 12 ? $sc_riv_1 : $products['product_price'])) : $products['price']).'" />';
					//$print_products .=  '  <td>'.$products['id_product'].'</td>';
			   $print_products .=  '</tr> ';
			   
			   if($products['section'] == 1)
			   {
				   $canoni .= $print_products;
			   }   
			   else
			   {
				   $prodotti .= $print_products;
			   } 
			}
				
			$marginalitatotale_ezcloud = ((($totale_ezcloud - $totaleacquisti_ezcloud)*100) / $totale_ezcloud);
				if($totale_ezcloud < 0)
					$marginalitatotale_ezcloud = 0-$marginalitatotale_ezcloud;
					
				$guadagno_ezcloud = $totale_ezcloud - $totaleacquisti_ezcloud;
				$totale_senza_spedizione_ezcloud = $totale_ezcloud;
				
						
								
						$html .= '<br />
					<fieldset style="background-color:#ffffff '.(strpos($template_cart, 'zcloud') == true  || strpos($template_cart, 'EASTAR-CLOUD') == true ? '' : '; display:none').'" >
					<legend><img src="../img/admin/cart.gif" alt="Canoni" />Canoni</legend>
					';
						$html .= '<table width="100%" class="table" id="tableProducts_ezcloud">
				<thead>
				  <tr>
					<th style="width:32px !important"></th>
					<th style="width:85px !important">Codice + Dati</th>
					<th style="width:32px !important"></th>
					<th style="width:183px !important">Nome prodotto</th>
					<th style="width:46px !important">Qta</th>
					<th style="width:68px !important">Unitario</th>
					
					
					<th style="width:42px !important; text-align:center">Sconto<br />extra</th>
					<th style="width:66px !important"><span '.($context->employee->id_profile == 7 ? 'style="display:none">Premio' : '>Acquisto').'</span></th>
					<th style="width:44px !important">'.($context->employee->id_profile == 7 ? '' : 'Marg').'</th>
					<th style="width:56px !important; text-align:center">'.($customer->id_default_group == 3 || $customer->id_default_group == 12 || $prezzi_carrello == 15 ? 'Sc.rv?' : 'Sc.qt?').'</th>
					<th style="width:56px !important">Importo</th>
					<th style="width:20px !important"><img src="../img/admin/up-and-down.gif" alt="Ordina" title="Ordina" /></th>
					<th style="width:20px !important"><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></th>
					<!-- <th style="width:45px">ID</th> -->
				  </tr>
				  </thead>
				  <tbody id="tableProductsBody_ezcloud">'.$canoni.'</tbody>';
				  
				  $html .= '<tfoot>
			
			
				<tr><td></td><td style="width:130px"></td><td></td><td style="width:180px">'.($context->employee->id_profile == 7 ? '<span id="guadagnototale_ezcloud" style="display:none"></span>' : 'Guadagno euro: <strong><span id="guadagnototale_ezcloud">'.number_format($guadagno_ezcloud,2, ',', '.').'</span> &euro;</strong>').'</td><td style="width:45px"></td><td style="width:75px"></td><td style="width:45px"></td><td style="width:75px;text-align:center">'.($context->employee->id_profile == 7 ? '<span id="totaleacquisti_ezcloud" style="display:none"></span>' : '<span id="totaleacquisti_ezcloud">'.number_format($totaleacquisti_ezcloud,2, ',', '').'</span>').'</td><td style="width:55px">'.($context->employee->id_profile == 7 ? '<span id="marginalitatotale_ezcloud" style="display:none"></span>' : '<span id="marginalitatotale_ezcloud">'.number_format($marginalitatotale_ezcloud,2, ',', '').'%</span>').'</td><td style="width:25px"><strong>Imponibile</strong></td><td style="width:85px; text-align:right"><span id="spantotaleprodotti_ezcloud"> '.number_format($totale_ezcloud,2, ',', '').'</span></td><td style="width:10px"></td>
			<td style="width:10px"></td><!-- <td style="width:45px"></td> --></tr>
			
			
			
			<tr><td></td><td style="width:130px"></td><td></td><td style="width:180px"></td><td style="width:45px"></td><td style="width:75px"></td><td style="width:45px"></td><td style="width:75px"></td><td style="width:55px"></td><td style="width:25px"></td><td style="width:85px; text-align:right"></td><td style="width:10px"></td>
			<td style="width:10px"></td><!-- <td style="width:45px"></td> --></tr></tfoot>';
			
				  
				  $html .= '</table></fieldset>';
				  
		// }
					$html .= '<br />
					<fieldset style="background-color:#ffffff">
					<legend><img src="../img/admin/cart.gif" alt="Prodotti" />Prodotti</legend>
					';
					
					$html .= '
					
					
				<table width="100%" class="table" id="tableProducts">
				<thead>
				  <tr>
					<th style="width:32px !important"></th>
					<th style="width:85px !important">Codice + Dati</th>
					<th style="width:32px !important"></th>
					<th style="width:183px !important">Nome prodotto</th>
					<th style="width:46px !important">Qta</th>
					<th style="width:68px !important">Unitario</th>
					
					
					<th style="width:42px !important; text-align:center">Sconto<br />extra</th>
					<th style="width:66px !important"><span '.($context->employee->id_profile == 7 ? 'style="display:none">Premio' : '>Acquisto').'</span></th>
					<th style="width:44px !important">'.($context->employee->id_profile == 7 ? '' : 'Marg').'</th>
					<th style="width:56px !important; text-align:center">'.($customer->id_default_group == 3 || $customer->id_default_group == 12 || $prezzi_carrello == 15 ? 'Sc.rv?' : 'Sc.qt?').'</th>
					<th style="width:56px !important">Importo</th>
					<th style="width:20px !important"><img src="../img/admin/up-and-down.gif" alt="Ordina" title="Ordina" /></th>
					<th style="width:20px !important"><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></th>
					<!-- <th style="width:45px">ID</th> -->
				  </tr>
				  </thead>
				  <tbody id="tableProductsBody">';
				  	$html .= $prodotti;
			
				$cart_ctrl = new Cart($_GET['id_cart']);
				$carrier_cart = Db::getInstance()->getValue("SELECT id_carrier FROM "._DB_PREFIX_."cart WHERE id_cart = ".$_GET['id_cart']."");
				
				/* Add TinyMCE */ 

				$iso = Language::getIsoById((int)($context->language->id));  
				$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');  
				$ad = dirname($_SERVER["PHP_SELF"]);
				
				$provincia_cliente = Db::getInstance()->getValue("SELECT id_state FROM "._DB_PREFIX_."address WHERE id_customer = ".$customer->id." AND active = 1 AND deleted = 0 AND fatturazione = 1");
				
				
				$nazione_cliente = Db::getInstance()->getValue("SELECT id_country FROM "._DB_PREFIX_."address WHERE id_customer = ".$customer->id." AND active = 1 AND deleted = 0 AND fatturazione = 1");
					
				if($provincia_cliente == 0) {
					$zona_cliente = Db::getInstance()->getValue("SELECT id_zone FROM "._DB_PREFIX_."country WHERE id_country = ".$nazione_cliente."");
				}
				else {
					$zona_cliente = Db::getInstance()->getValue("SELECT id_zone FROM "._DB_PREFIX_."state WHERE id_state = ".$provincia_cliente."");
				}
				
				$gruppi_per_spedizione = array();
				$gruppi_per_spedizione[] = $customer->id_default_group;
				
				$metodi_spedizione = Cart::getCarriersForEditOrder($zona_cliente, $gruppi_per_spedizione, $_GET['id_cart']);
				
				if(sizeof($metodi_spedizione) == 0)
					$metodi_spedizione = Cart::getCarriersForEditOrder(9, $gruppi_per_spedizione, $_GET['id_cart']);
				
				foreach($metodi_spedizione as $metodo) {
					if($metodo['name'] != 'Ritiro in sede Ezdirect' && $metodo['name'] != 'Trasporto gratuito') {
						$default_carrier = $metodo['id_carrier'];
					}
					else {
					}
				}

				$spedizione_default = Db::getInstance()->getValue('SELECT d.`price` FROM `'._DB_PREFIX_.'delivery` d LEFT JOIN `'._DB_PREFIX_.'range_price` r ON d.`id_range_price` = r.`id_range_price` WHERE d.`id_zone` = "'.$zona_cliente.'" AND 1 >= r.`delimiter1` AND 156 < r.`delimiter2` AND d.`id_carrier` = "'.$default_carrier.'" ORDER BY r.`delimiter1` ASC');
				
				$costo_trasporto_modificato_r = Db::getInstance()->getValue("SELECT transport FROM "._DB_PREFIX_."cart WHERE id_cart = ".$_GET['id_cart']."");
				$costo_trasporto_modificato_r = explode(":",$costo_trasporto_modificato_r);
				$costo_trasporto_modificato = $costo_trasporto_modificato_r[1];
				if($carrier_cart != 0) {
					if($costo_trasporto_modificato != "" && $costo_trasporto_modificato > 0) {
						$costo_spedizione = $costo_trasporto_modificato;
					}
					else {				
						$costo_spedizione = $cart_ctrl->getOrderShippingCost($carrier_cart, false);
						$costo_spedizione = $cart_ctrl->getOrderShippingCostByTotalAndDelivery($carrier_cart, $totale, $consegna, $customer->id_default_group, false);
					}
				}
				else {
					$costo_spedizione = $cart_ctrl->getOrderShippingCostByTotalAndDelivery($default_carrier, $totale, $consegna, $customer->id_default_group, false);
				}
				
				$marginalitatotale = ((($totale - $totaleacquisti)*100) / $totale);
				if($totale < 0)
					$marginalitatotale = 0-$marginalitatotale;
					
				$guadagno = $totale - $totaleacquisti;
				$totale_senza_spedizione = $totale;
				
				$marginalitatotale_prodotti = ((($totale_prodotti - $totaleacquisti_prodotti)*100) / $totale_prodotti);
				if($totale_prodotti < 0)
					$marginalitatotale_prodotti = 0-$marginalitatotale_prodotti;
					
				$guadagno_prodotti = $totale_prodotti - $totaleacquisti_prodotti;
				$totale_senza_spedizione_prodotti = $totale_prodotti;
				
				
				
		/*
			if($totale_senza_spedizione == 0) {	
				$costo_spedizione = $spedizione_default;
			}
		*/	
		
			if($order->id)
				$costo_spedizione = Db::getInstance()->getValue('SELECT (total_shipping / ((carrier_tax_rate/100)+1)) spedizione FROM '._DB_PREFIX_.'orders WHERE id_order = '.$order->id);
				
			$totale += $costo_spedizione; 
			
			$tax_regime = Db::getInstance()->getValue("SELECT tax_regime FROM "._DB_PREFIX_."customer WHERE id_customer = ".$customer->id."");
			
			if(!$tax_regime)
				$tax_regime = 0;
			
			if($tax_regime == 0 || $tax_regime == 4) {
				$id_tax = 1;
			}
			else {
				$id_tax = $tax_regime;
			}
			$tax_rate = Db::getInstance()->getValue("SELECT rate FROM "._DB_PREFIX_."tax WHERE id_tax = ".$id_tax."");
				
			if($tax_regime == 1) {
				$tax_rate = 0;
			}
			
			$carrellon = new Cart(Tools::getValue('id_cart'));
			
			$id_country = Db::getInstance()->getValue("SELECT id_country FROM "._DB_PREFIX_."address WHERE id_address = ".$cart->id_address_delivery."");
			
			if($id_country == 10) {
				
				
				$iva = $carrellon->getOrderTotal() - $carrellon->getOrderTotal(false);
				
			}
				
			else {
				
				if($customer->is_company == 1 || $customer->is_company == 0 && $id_country == 19) 
					$iva = 0;
				else
					$iva = $carrellon->getOrderTotal() - $carrellon->getOrderTotal(false);
			}
			
			if(!$id_country)
				$iva = $carrellon->getOrderTotal() - $carrellon->getOrderTotal(false);
			
			
			
			
				
			if($tax_regime == 1) {

				$iva = 0;
					
			}
			
			$totaleprovvigioni = 0;
			
			
			$html .= '  
			</tbody>
			<tfoot>
			
			
				<tr><td></td><td style="width:130px"></td><td></td><td style="width:180px">'.($context->employee->id_profile == 7 ? '<span id="guadagnototale_prodotti" style="display:none"></span>' : 'Guadagno euro: <strong><span id="guadagnototale_prodotti">'.number_format($guadagno_prodotti,2, ',', '.').'</span> &euro;</strong>').'</td><td style="width:45px"></td><td style="width:75px"></td><td style="width:45px"></td><td style="width:75px;text-align:center">'.($context->employee->id_profile == 7 ? '<span id="totaleacquisti_prodotti" style="display:none"></span>' : '<span id="totaleacquisti_prodotti">'.number_format($totaleacquisti_prodotti,2, ',', '').'</span>').'</td><td style="width:55px">'.($context->employee->id_profile == 7 ? '<span id="marginalitatotale_prodotti" style="display:none"></span>' : '<span id="marginalitatotale_prodotti">'.number_format($marginalitatotale_prodotti,2, ',', '').'%</span>').'</td><td style="width:25px"><strong>Imponibile</strong></td><td style="width:85px; text-align:right"><span id="spantotaleprodotti_prodotti"> '.number_format($totale_prodotti,2, ',', '').'</span></td><td style="width:10px"></td>
			<td style="width:10px"></td><!-- <td style="width:45px"></td> --></tr>
			
			
			
			<tr><td></td><td style="width:130px"></td><td></td><td style="width:180px"></td><td style="width:45px"></td><td style="width:75px"></td><td style="width:45px"></td><td style="width:75px"></td><td style="width:55px"></td><td style="width:25px"></td><td style="width:85px; text-align:right"></td><td style="width:10px"></td>
			<td style="width:10px"></td><!-- <td style="width:45px"></td> --></tr>
			
			<tr><th colspan="13">Totali</th></tr>
			
			<tr id="trasp_carrello">
			<td></td><td>TRASP</td><td></td><td>Trasporti (non modificare per calcolare in automatico)</td><td style="text-align:right">1</td>
			<input type="hidden" id="trasporto_modificato" value="'.($costo_trasporto_modificato == '' || $costo_trasporto_modificato == 0 ? 'n' : 'y').'" />
			<td id="costo_trasporto"><input type="text" '.($order->id? 'readonly="readonly"' : '').' value="'.($costo_trasporto_modificato == '' || $costo_trasporto_modificato == 0 ? number_format($costo_spedizione,2,',','') : str_replace(".",",",$costo_trasporto_modificato)).'" size="7" onkeyup="document.getElementById(\'valore_trasporto_modificato\').value = this.value; vediTrasportoGratuito(); document.getElementById(\'trasporto_modificato\').value = \'y\'; document.getElementById(\'importo_trasporto\').innerHTML=this.value; calcolaImportoConSpedizione('.$carrier_cart.'); " name="transport" id="transport" style="text-align:right" /><input type="hidden" id="valore_trasporto_modificato" value="'.($costo_trasporto_modificato == '' || $costo_trasporto_modificato == 0 ? '' : $costo_trasporto_modificato).'" /></td><td style="text-align:right"></td><td style="text-align:right"></td><td></td><td></td><td style="text-align:right" id="importo_trasporto">'.($costo_trasporto_modificato == '' || $costo_trasporto_modificato == 0 ? number_format($costo_spedizione,2,',','') : str_replace(".",",",$costo_trasporto_modificato)).'</td><td></td><td></td></tr>
			
			<tr><td></td><td style="width:130px"></td><td></td><td style="width:180px"></td><td style="width:45px"></td><td style="width:75px"></td><td style="width:45px"></td><td style="width:75px"></td><td style="width:55px"></td><td style="width:25px"></td><td style="width:85px; text-align:right"></td><td style="width:10px"></td>
			<td style="width:10px"></td><!-- <td style="width:45px"></td> --></tr>
			
		
			<tr><td></td><td style="width:130px"></td><td></td><td style="width:180px">'.($context->employee->id_profile == 7 ? '<span id="guadagnototale" style="display:none"></span>' : 'Guadagno euro: <strong><span id="guadagnototale">'.number_format($guadagno,2, ',', '.').'</span> &euro;</strong>').'</td><td style="width:45px"></td><td style="width:75px"></td><td style="width:45px"></td><td style="width:75px;text-align:center">'.($context->employee->id_profile == 7 ? '<span id="totaleacquisti" style="display:none"></span>' : '<span id="totaleacquisti">'.number_format($totaleacquisti,2, ',', '').'</span>').'</td><td style="width:55px">'.($context->employee->id_profile == 7 ? '<span id="marginalitatotale" style="display:none"></span>' : '<span id="marginalitatotale">'.number_format($marginalitatotale,2, ',', '').'%</span>').'</td><td style="width:25px"><strong>Imponibile</strong></td><td style="width:85px; text-align:right"><span id="spantotaleprodotti"> '.number_format($totale,2, ',', '').'</span></td><td style="width:10px"></td>
			<td style="width:10px"></td><!-- <td style="width:45px"></td> --></tr>
			
			<tr><td></td><td style="width:130px"></td><td></td><td style="width:180px"></td><td style="width:45px"></td><td style="width:75px"></td><td style="width:45px"></td><td style="width:75px"></td><td style="width:55px"></td><td style="width:25px"><strong>IVA incl.</strong></td><td style="width:85px; text-align:right"><span id="spantotaleprodottiiva"> '.number_format($totale+$iva,2, ',', '').'</span></td><td style="width:10px"></td>
			<td style="width:10px"></td><!-- <td style="width:45px"></td> --></tr>
			
			'.($context->employee->id_profile == 7 ? '<tr><td></td><td style="width:130px"></td><td></td><td style="width:180px"></td><td style="width:45px"></td><td style="width:75px"></td><td style="width:45px"></td><td style="width:75px"></td><td style="width:55px"></td><td style="width:25px"><strong>Provvigioni</strong></td><td style="width:85px; text-align:right"><span id="spantotaleprovvigioni" style="display:none"> '.number_format($totaleprovvigioni,2, ',', '').'</span></td><td style="width:10px"></td>
			<td style="width:10px"></td><!-- <td style="width:45px"></td> --></tr>' : '').'
			
			</tfoot>
			</table><table class="table">
			</table>
			
			</fieldset>';
			


				if($payment != "" && $payment != "Bonifico" && $payment != "nessuno" && $payment != "Bonifico Bancario" && $payment != "Bonifico anticipato" && $payment != "Bonifico Anticipato" && $payment != "Carta" && $payment != "GestPay" && $payment != "Carta Amazon"  && $payment != "Amazon" && $payment != "Carta ePrice"  && $payment != "ePrice" && $payment != "Paypal" && $payment != "PayPal" && $payment != "Contrassegno" && $payment != "Bonifico 30 gg. fine mese" && $payment != "Bonifico 60 gg. fine mese" && $payment != "Bonifico 90 gg. fine mese" && $payment != "Bonifico 30 gg. 15 mese successivo" && $payment != 'R.B. 30 GG. D.F. F.M.' && $payment != 'R.B. 60 GG. D.F. F.M.' && $payment != 'R.B. 90 GG. D.F. F.M.' && $payment != 'R.B. 30 GG. 5 mese successivo' && $payment != 'R.B. 30 GG. 10 mese successivo' && $payment != 'R.B. 60 GG. 5 mese successivo' && $payment != 'R.B. 60 GG. 10 mese successivo') {
					$check_payment = 1;
				} else {
					$check_payment = 0;
				}
				
				
				if($payment == "") {
					$payment = Db::getInstance()->getValue("SELECT pagamento FROM customer_amministrazione WHERE id_customer = ".$customer->id."");
				}
				
				if($payment == "") {
					$payment = 'Bonifico anticipato';
				}
				
				//$html .= '<span style="background-color:#ffcccc">Eventuale riga in rosa = prodotto con margine sotto 10%</span><br />';
				
				$telegestione = Db::getInstance()->getValue('SELECT telegestione FROM '._DB_PREFIX_.'cart WHERE id_cart = '.$cart->id);
				
				if($telegestione != '')
				{
					
					$html .= '<br />
					<input type="hidden" name="telegestione" value="yes" />
					<fieldset style="background-color:#ffffff">
					<legend><img src="../img/admin/cart.gif" alt="Prodotti" />Prodotti in telegestione (ATTENZIONE: contenuto non visibile al ciente)</legend>
					<script type="text/javascript">
						function delProduct30Telegest(id)
						{
							var row = document.getElementById(\'tr_telegest_\'+id);
							row.parentNode.removeChild(row);
							//calcoloTelegestione();
						}
						
					</script>
					
					<table width="100%" class="table" id="tableProducts">
					<thead id="theadProducts">
					  <tr>
						<th></th>
						<th style="width:100px">Codice + Dati</th>
						<th></th>
						<th style="width:250px">Nome prodotto</th>
						<th style="width:45px">Qta</th>
						<th style="width:55px">Unitario</th>
						
						
						<th style="width:55px">Acquisto</th>
						
						<th style="width:55px">Importo</th>
						<th style="width:10px"><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></th>
						<!-- <th style="width:45px">ID</th> -->
					  </tr>
					  </thead><tbody>';
					  $telegestione = unserialize($telegestione);
					  foreach($telegestione as $pt)
					  {
						$pt_price = ($pt['no_acq'] == 1 ? 0 : ($pt['price'] == 0 ? Product::trovaMigliorPrezzo($products['id_product'],$customer->id_default_group,$pt['quantity']) : $pt['price'] - (($pt['price'] / 100 * $pt['reduction_percent']))));  
						$pt_image = Db::getInstance()->getValue('SELECT id_image FROM '._DB_PREFIX_.'image WHERE id_product = '.$pt['id_product'].' AND cover = 1');
						$pt_manufacturer = Db::getInstance()->getValue('SELECT id_manufacturer FROM '._DB_PREFIX_.'product WHERE id_product = '.$pt['id_product']);
						$html .= '<tr id="tr_telegest_'.$pt['id_product'].'">';
						$html .= '<td style="width:32px"><img style="width:30px; height:30px; float:left" src="http://www.ezdirect.it/img/'.($pt_image > 0 ? 'p/'.$pt['id_product'].'-'.$pt_image : 'm/'.$pt_manufacturer).'-small.jpg" alt="" /></td>';
						  
						$html .= '<td style="width:100px"><span style="cursor:pointer" class="span-reference tooltip_prodotto" title="'.Product::showProductTooltip($pt['id_product']).'"><a href="index.php?controller=AdminProducts&id_product='.$pt['id_product'].'&updateproduct&token='.$tokenProducts.'" target="_blank">'.Db::getInstance()->getValue('SELECT reference FROM '._DB_PREFIX_.'product WHERE id_product = '.$pt['id_product']).'</a></span></td>';
						$html .= '<td>'.Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'product_lang WHERE id_lang = '.$context->language->id.' AND id_product = '.$pt['id_product']).'<input id="prodotto_telegestione['.$pt['id_product'].']" name="prodotto_telegestione['.$pt['id_product'].']" value="'.$pt['id_product'].'" type="hidden" />
						<input id="quantity_telegestione['.$pt['id_product'].']" name="quantity_telegestione['.$pt['id_product'].']" value="'.$pt['quantity'].'" type="hidden" />
						<input id="price_telegestione['.$pt['id_product'].']" name="price_telegestione['.$pt['id_product'].']" value="'.($pt['price'] - (($pt['price'] / 100 * $pt['reduction_percent']))).'" type="hidden" />
						<input id="no_acq_telegestione['.$pt['id_product'].']" name="no_acq_telegestione['.$pt['id_product'].']" value="'.$pt['no_acq'].'" type="hidden" />
						<input id="reduction_percent_telegestione['.$pt['id_product'].']" name="no_acq_telegestione['.$pt['id_product'].']" value="'.$pt['reduction_percent'].'" type="hidden" />
						<input id="wholesale_price_telegestione['.$pt['id_product'].']" name="wholesale_price_telegestione['.$pt['id_product'].']" value="'.$pt['wholesale_price'].'" type="hidden" />
						</td>';
						$html .= '<td style="text-align:right">'.$pt['quantity'].'</td>';
						$html .= '<td style="text-align:right">'.number_format(($pt['price'] - (($pt['price'] / 100 * $pt['reduction_percent']))),2,",","").'</td>';
					
						$html .= '<td style="text-align:right">'.number_format($pt['wholesale_price'],2,",","").'</td>';
						$html .= '<td style="text-align:right">'.number_format($pt_price*$pt['quantity'],2,",","").'
						<input class="importo_telegestione" type="hidden" value="'.$pt_price*$pt['quantity'].'"/>
						</td>';
						$html .= '<td><a style="cursor:pointer" onclick="delProduct30Telegest('.$pt['id_product'].')"><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a></td>';
						$html .= '</tr>';
					  }
					$html .= '</tbody></table>
					
					<p style="text-align:center"><a class="button" href="javascript:void(0)" onclick="calcoloTelegestione();">Ricalcola in automatico telegestione</a></p>
					
					</fieldset>';
				}
				else
					$html .= '<input type="hidden" name="telegestione" value="no" />';
				
				$html .= "<br /><a href='javascript:void(0)' class='button' style='cursor:pointer;' onclick=\"$('#mostra_contratto').slideToggle();\"><strong>Contratto (clic per aprire)</strong></a> <span id='mostra_contratto' style='".($cadenza != '' ? '' : 'display:none')."' >
				
				<input type='hidden' name='competenza_dal' style='text-align:right; width:70px' id='competenza_dal' value='".(((isset($competenza_dal) && $competenza_dal != '0000-00-00' && $competenza_dal != '--' && $competenza_dal != '1970-01-01' && $competenza_dal != '' && $competenza_dal != '1942-01-01')) ? date("d-m-Y", strtotime($competenza_dal)) : '')."' > 
				
				<input type='hidden' name='competenza_al' style='text-align:right; width:70px' id='competenza_al' value='".(((isset($competenza_al) && $competenza_al != '0000-00-00' && $competenza_al != '1970-01-01' && $competenza_al != '' && $competenza_al != '1942-01-01')) ? date("d-m-Y", strtotime($competenza_al)) : '')."' > 
				
				Cadenza"; $html .= ' <select name="cadenza" id="cadenza"  style="width:100px">
								<option value=""> -- Seleziona -- </option>
								<option value="1" '.($cadenza == 1 ? 'selected="selected"' : '').'>Annuale</option>
								<option value="12" '.($cadenza == 12 ? 'selected="selected"' : '').'>Mensile</option>
								<option value="6" '.($cadenza == 6 ? 'selected="selected"' : '').'>Bimestrale</option>
								<option value="4" '.($cadenza == 4 ? 'selected="selected"' : '').'>Trimestrale</option>
								<option value="2" '.($cadenza == 2 ? 'selected="selected"' : '').'>Semestrale</option>
							</select>';
							
							$html .= "
				
				
				Scadenza <input type='text' name='scadenza' style='text-align:right; width:70px' id='scadenza' value='".(((isset($scadenza) && $scadenza != '0000-00-00' && $scadenza != '1970-01-01' && $scadenza != '--' && $scadenza != '' && $scadenza != '1942-01-01')) ? date("d-m-Y", strtotime($scadenza)) : '')."' >   <img class='img_control' src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' title='Se non compilata, viene calcolata da un anno a partire dalla data di spedizione' /> &nbsp;&nbsp;&nbsp;
				Decorrenza <input type='text' name='decorrenza' style='text-align:right; width:70px' id='decorrenza' value='".(((isset($decorrenza) && $decorrenza != '0000-00-00' && $decorrenza != '1970-01-01' && $decorrenza != '--' && $decorrenza != '' && $decorrenza != '1942-01-01')) ? date("d-m-Y", strtotime($decorrenza)) : '')."'></span><br /> ";
				
				$html .= "<br /><strong>Metodo di pagamento</strong>: ";
					
				if($order->id) { 
					
					$payment = Db::getInstance()->getValue("SELECT payment FROM "._DB_PREFIX_."orders WHERE id_order = ".$order->id."");
					$html .= "<input type='text' readonly='readonly' style='width:250px' value='".str_replace("'",htmlentities("'",ENT_QUOTES),$payment)."' />&nbsp;&nbsp;&nbsp;";
				} 
				else {
					$html .= "<select name='payment' id='payment'>
					<option value='nessuno' ".($payment == "nessuno" ? "selected='selected'" : "").">Nessun metodo - lascia scelta al cliente</option>
					<option value='Bonifico Anticipato' ".($payment == "Bonifico" || $payment == "Bonifico Anticipato" || $payment == "Bonifico anticipato" || $payment == "Bonifico bancario" || $payment == "Bonifico Bancario" ? "selected='selected'" : "").">Bonifico anticipato</option>
					<option value='Carta' ".($payment == "Carta" || $payment == "GestPay" ? "selected='selected'" : "").">Carta di credito (Gestpay)</option>
					<option value='Carta Amazon' ".($payment == "Carta Amazon" || $payment == "Amazon" ? "selected='selected'" : "").">Carta Amazon</option>
					<option value='Carta ePrice' ".($payment == "Carta ePrice" || $payment == "ePrice" ? "selected='selected'" : "").">Carta ePrice</option>
					<option value='Paypal' ".($payment == "Paypal" || $payment == "PayPal" ? "selected='selected'" : "").">Paypal</option>
					<option value='Contrassegno' ".($payment == "Contrassegno" ? "selected='selected'" : "").">Contrassegno</option>
					<option value='Bonifico 30 gg. fine mese' ".($payment == "Bonifico 30 gg. fine mese" ? "selected='selected'" : "").">Bonifico 30 gg. fine mese</option>
					<option value='Bonifico 60 gg. fine mese' ".($payment == "Bonifico 60 gg. fine mese" ? "selected='selected'" : "").">Bonifico 60 gg. fine mese</option>
					<option value='Bonifico 90 gg. fine mese' ".($payment == "Bonifico 90 gg. fine mese" ? "selected='selected'" : "").">Bonifico 90 gg. fine mese</option>
					<option value='Bonifico 30 gg. 15 mese successivo' ".($payment == "Bonifico 30 gg. 15 mese successivo" ? "selected='selected'" : "").">Bonifico 30 gg. 15 mese successivo</option>
					<option value='R.B. 30 GG. D.F. F.M.' ".($payment == "R.B. 30 GG. D.F. F.M." ? "selected='selected'" : "").">R.B. 30 GG. D.F. F.M.</option>
					<option value='R.B. 60 GG. D.F. F.M.' ".($payment == "R.B. 60 GG. D.F. F.M." ? "selected='selected'" : "").">R.B. 60 GG. D.F. F.M.</option>
					<option value='R.B. 90 GG. D.F. F.M.' ".($payment == "R.B. 90 GG. D.F. F.M." ? "selected='selected'" : "").">R.B. 90 GG. D.F. F.M.</option>
					<option value='R.B. 30 GG. 5 mese successivo' ".($payment == "R.B. 30 GG. 5 mese successivo" ? "selected='selected'" : "").">R.B. 30 GG. 5 mese successivo</option>
					<option value='R.B. 30 GG. 10 mese successivo' ".($payment == "R.B. 30 GG. 10 mese successivo" ? "selected='selected'" : "").">R.B. 30 GG. 10 mese successivo</option>
					<option value='R.B. 60 GG. 5 mese successivo' ".($payment == "R.B. 60 GG. 5 mese successivo" ? "selected='selected'" : "").">R.B. 60 GG. 5 mese successivo</option>
					<option value='R.B. 60 GG. 10 mese successivo' ".($payment == "R.B. 60 GG. 10 mese successivo" ? "selected='selected'" : "").">R.B. 60 GG. 10 mese successivo</option>
					<option value='Renting' ".($payment == "Renting" ? "selected='selected'" : "").">Renting</option>
					<option value='Altro' ".($check_payment == 1 ? "selected='selected'": "").">Altro</option>
					</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					$html .= "Altro metodo: <input type='text' name='altropagamento' id='altropagamento' value='".($check_payment == 1 ? str_replace("'",htmlentities("'",ENT_QUOTES),$payment) : "")."' /><br />";
					
					if($customer->id_default_group == 1) {
						
						if($totale < 399) {
							$supplemento_contrassegno = 3.5;
						}
						else {
							$supplemento_contrassegno = 0;
							
						}
					}
						
					else if($customer->id_default_group  == 3) {
							
						if($totale < 516) {
							$supplemento_contrassegno = 5;
						}
						else {
							$supplemento_contrassegno = (($totale/100)*1.5);
						
						}
							
					}
						
					else {
						if($totale < 516) {
							$supplemento_contrassegno = 5;
						}
						else {
							$supplemento_contrassegno = (($totale/100)*1.5);
							
						}
					
					}

					$html .= "Il pagamento in contrassegno prevede un supplemento di euro <span id='supplemento_contrassegno'>".number_format($supplemento_contrassegno,2,",","")."</span> iva esclusa
								
					<br /><br />
					";
				}
				
				$no_addebito_commissioni = Db::getInstance()->getValue('SELECT no_addebito_commissioni FROM '._DB_PREFIX_.'cart WHERE id_cart = '.Tools::getValue('id_cart'));
				
				$html .= 'Se pagamento differito, non addebitare le commissioni: <input type="checkbox" name="no_addebito_commissioni" '.($no_addebito_commissioni == 1 ? 'checked="checked"' : '').' /><br /><br />';
				
				if($order->id) { 
					$mtds = Db::getInstance()->getValue("SELECT name FROM "._DB_PREFIX_."carrier WHERE id_carrier = ".$carrier_cart."");
					$html .= "<strong>Metodo spedizione</strong>: <input type='text' readonly='readonly' style='width:250px' value='".$mtds."' /><br />";
				} 
				else {
					$html .= "<strong>Seleziona metodo di spedizione</strong> (se l'importo prevede consegna gratuita, il costo sar&agrave; ricalcolato automaticamente dopo aver fatto clic sul tasto Conferma):";
					
					$array_id_metodi = array();
					
					foreach ($metodi_spedizione as $metodo) {
						$array_id_metodi[] = $metodo['id_carrier'];
					}
					
					$i_metodi_spedizione = 0;
					foreach ($metodi_spedizione as $metodo) {
						$i_metodi_spedizione++;
						if ($costo_trasporto_modificato == '' || $costo_trasporto_modificato == 0) {
							$metodo['price_tax_exc'] = $metodo['price_tax_exc'];
						}
						else {
							if ($carrier_cart == $metodo['id_carrier']) {
								$metodo['price_tax_exc'] = $costo_trasporto_modificato;
							}
							else {
								$metodo['price_tax_exc'] = $metodo['price_tax_exc'];
							}
						}

						$html .= "<input type='radio' id='metodo[".$metodo['id_carrier']."]' ".(strpos($metodo['name'],'grat') !== false ? "class='gratis'" : (strpos($metodo['name'],'itiro') !== false ? "class='ritiro'" : "class='a_pagamento'"))." name='metodo' rel='".$metodo['price_tax_exc']."' value='".$metodo['id_carrier']."' ".($carrier_cart == $metodo['id_carrier'] ? "checked='checked'" : ($carrier_cart == '' || $carrier_cart == 0 || !in_array($carrier_cart, $array_id_metodi)  ? ($metodo['name'] != 'Ritiro in sede Ezdirect' && $metodo['name'] != 'Trasporto gratuito' ? "checked='checked'" : "") : ""))." onclick='document.getElementById(\"valore_trasporto_modificato\").value = 0; calcolaSpedizionePerIndirizzo(); document.getElementById(\"transport\").value = \"".number_format($metodo['price_tax_exc'],2,",","")."\"; document.getElementById(\"importo_trasporto\").innerHTML = \"".number_format($metodo['price_tax_exc'],2,",","")."\"; calcolaImportoConSpedizione(".$metodo['id_carrier']."); ' />".$metodo['name']." (<span id='metodo_costo[".$metodo['id_carrier']."]'>".number_format($metodo['price_tax_exc'],2,",","")."</span> &euro;)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					}
				}
				
				if(Tools::getValue('preventivo') == 222) {}
				else {
					//link utili
					$array_utils = array();
					$array_utils[] = '<option name="" value="-- Seleziona un link utile --" selected="selected">-- Seleziona un link utile --</option>';
					$array_utils[] = '<option name="Preventivi" value="https://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini">Preventivi</option>';
					$array_utils[] = '<option name="Ti richiamiamo noi" value="https://www.ezdirect.it/ti-richiamiamo-noi">Ti richiamiamo noi</option>';
					$array_utils[] = '<option name="Il tuo account" value="https://www.ezdirect.it/autenticazione">Il tuo account</option>';
					$array_utils[] = '<option name="Blog" value="http://www.ezdirect.it/blog/">Blog</option>';
					$array_utils[] = '<option name="Offerte speciali" value="http://www.ezdirect.it/offerte-speciali">Offerte speciali</option>';
					$array_utils[] = '<option name="Assistenza tecnica postvendita" value="http://www.ezdirect.it/contattaci?step=tecnica">Assistenza tecnica postvendita</option>';
					$array_utils[] = '<option name="Assistenza ordini" value="http://www.ezdirect.it/contattaci?step=assistenza-ordini">Assistenza ordini</option>';
					$array_utils[] = '<option name="Assistenza contabilita" value="http://www.ezdirect.it/contattaci?step=contabilita">Assistenza contabilità</option>';
					$array_utils[] = '<option name="Rivenditori" value="https://www.ezdirect.it/autenticazione?cliente=rivenditore">Rivenditori</option>';
					$array_utils[] = '<option name="Condizioni di vendita" value="http://www.ezdirect.it/guide/3-termini-e-condizioni-acquisto-on-line-ezdirect">Condizioni di vendita</option>';
					$array_utils[] = '<option name="Cashback Jabra" value="http://www.ezdirect.it/blog/2017/01/acqistare-al-miglior-prezzo-jabra-rimborso-su-prodotto-comprato/">Cashback Jabra</option>';


					$cmss = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'cms_lang WHERE id_lang = '.$context->language->id.'');
					foreach ($cmss as $cms)
					{
						$array_utils[] = '<option name="'.$cms['meta_title'].'" value="http://www.ezdirect.it/guide/'.$cms['id_cms'].'-'.$cms['link_rewrite'].'.php">'.$cms['meta_title'].'</option>';
					}
					
					sort($array_utils, SORT_STRING);
					
					$utils_options = '';
					
					foreach($array_utils as $util)
						$utils_options .= $util;
				
					//
					
					$html .= '
					<style type="text/css">
					.mceEditor td.mceIframeContainer iframe {
						min-height: 30px !important;
					}
					</style>
					<p>
					<table style="margin-left:-3px"><tr><td><strong>Esigenze del cliente</strong>: 
					
					'.($order->id? '<div style="width:445px; background-color:#f0ebd6; border:1px solid #cccccc; overflow-y:auto; height:150px">'.$esigenze.'</div>' : '<textarea name="esigenze" id="esigenze" style="width:445px; " class="rte" rows="3">'.$esigenze.'</textarea>').'</td><td style="padding-left:20px">
					<strong>Risorse</strong> <!-- (<a href="javascript:return false;" onclick="javascript:window.open(\'configuratore_preventivi.php?id_cart='.$cart->id.'\', \'Configuratore preventivi\', \'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=1, width=900,height=700\');">apri questionario</a>) -->: '.($order->id? '<div style="width:445px; background-color:#f0ebd6; border:1px solid #cccccc; overflow-y:auto; height:150px">'.$risorse.'</div>' : '<textarea name="risorse" id="risorse" style="width:445px" class="rte">'.$risorse.'</textarea>').'</td></tr></table><br />
					
					
					<strong>Note</strong>:<br /> 
					'.($order->id ? '' : 'Link prodotto: <input size="123" type="text" value="" id="product_autocomplete_input_tkt" />
					<br /><br />
					<script type="text/javascript" src="../js/select2.js"></script>
						<script type="text/javascript">
						function addUtil_TR(event, data, formatted)
						{
							var $body = $(tinymce.editors[\'note\'].getBody());
							$body.prepend(\'<p>\');
							$body.append(\'</p>\');
							if(document.getElementById("utils_autocomplete_input_tkt").value != "-- Seleziona un link utile --")
							{
								$body.find("p:last").append(\'<a href="\' + document.getElementById("utils_autocomplete_input_tkt").value + \'">\' + document.getElementById("utils_autocomplete_input_tkt").value + \'<\/a><br /><br />\');
							}
						}	
						
						$(document).ready(function() { $("#utils_autocomplete_input_tkt").select2();});
					</script>
					Link utili:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <select name="utils_autocomplete_input_tkt" onchange="addUtil_TR();" id="utils_autocomplete_input_tkt" style="display:block; width:714px; margin-left:27px">'.$utils_options.'</select><br /><br />
					
					<script type="text/javascript">
					
						function addProductLink_TR(event, data, formatted)
						{
							document.getElementById("product_autocomplete_input_tkt").value = ""; 
							var link_rewrite = data[31];
							var productName = data[5];
							var $body = $(tinymce.editors[\'note\'].getBody());
							$body.prepend(\'<p>\');
							$body.append(\'</p>\');
							$body.find("p:last").append(\'<br />\' + productName + \'<br /><a href="\' + link_rewrite + \'">\' + link_rewrite + \'<\/a><br /><br />\');
							
						}

						$(function() {
							$(\'#product_autocomplete_input_tkt\')
								.autocomplete(\'ajax_products_list2.php?products_and_bundles=y&id_customer='.Tools::getValue('id_customer').'\', {
									minChars: 2,
									autoFill: false,
									max:50,
									matchContains: true,
									matchSubset: 0,
									mustMatch:true,
									scroll:true,
									cacheLength:1,
									formatItem: function(item) {
								
												var color = "";
												if(item[24] == 1) {
													var color = "; color:red";
												}
												if(item[28] == 0) {
													return \'</table><br /><div  onclick="return false;" style="position:absolute; z-index:99997; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:150px;float:left;margin-left:10px;">Codice</div></th><th style="width:530px;"><div style="width:210px; float:left;">Desc. prodotto<br /><br /></div></th><th style="width:80px; text-align:right"><div style="width:60px;float:left;">Prezzo</div></th><th style="width:50px; text-align:right"><div style="width:50px;float:left;">Mag.</div></th><th style="width:50px; text-align:right"><div style="width:45px;float:left;">INT</div></th><th style="width:45px; text-align:right"><div style="width:45px;float:left;">ALN</div></th><th style="width:45px; text-align:right"><div style="width:45px;float:left;">ITA</div></th><th style="width:40px; text-align:right"><div style="width:45px;float:left;">Imp</div></th><th style="width:40px; text-align:right"><div style="width:40px;float:left;">Tot</div></th></tr></div><table>\'
													
													;
												}
												else {
													return \'<tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:250px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[33]+\'</td><td style="width:50px; text-align:right">\'+item[34]+\'</td><td style="width:50px; text-align:right">\'+item[38]+\'</td><td style="width:50px; text-align:right">\'+item[37]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
													
													;
													}
												}
									
								}).result(addProductLink_TR);
							
						});
					
						$("#product_autocomplete_input_tkt").css(\'width\',\'705px\');
						$("#product_autocomplete_input_tkt").keypress(function(event){
									
									  var keycode = (event.keyCode ? event.keyCode : event.which);
									  if (keycode == "13") {
										  $("#product_autocomplete_input_tkt").trigger("click");
										event.preventDefault();
										event.stopPropagation();    
									  }
									});
									
						
					
					</script>').'
					
					
					'.($order->id? '<div style="width:910px; background-color:#f0ebd6; border:1px solid #cccccc; overflow-y:auto; height:150px">'.$note.'</div>' : '<textarea name="note" id="note" style="width:910px" class="rte">'.$note.'</textarea>').'<br />
					
			
					';
					
					/*if(Tools::getIsset('vedirevisione'))
					{
						//mysql_select_db(_DB_REV_NAME_);
					}*/
					
					$numero_notifiche =  Db::getInstance()->getValue("SELECT numero_notifiche FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
					$data_ultima_notifica =  Db::getInstance()->getValue("SELECT date_add FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
					$provvisorio =  Db::getInstance()->getValue("SELECT provvisorio FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
					
					
					$notifiche = Db::getInstance()->getValue("SELECT notifiche FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
					
					/*if(Tools::getIsset('vedirevisione'))
					{
						//mysql_select_db(_DB_NAME_);
					}*/
					
					$impiegati_ez = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."employee WHERE active = 1 ORDER BY firstname ASC, lastname ASC");
					
					$notifiche = unserialize($notifiche);
					
					$cart_note = Db::getInstance()->executeS('SELECT * FROM cart_note WHERE id_cart = '.Tools::getValue('id_cart').'');
					
				
							
					$html .= '<span class="button"  style=" display:block; color:red !important; font-size:14px; font-weight:bold; width:902px; border:1px solid #cccccc; text-align:center; background-color:#f0f0ee; text-decoration:none">Note private</span><br />';
					
					$html .= '<div id="mostra_nota_privata" style="'.(strip_tags(str_replace(" ","",$note_private)) == '' && count($cart_note) == 0 ? '' : '').' margin-top:-15px">';
					
					
					$html .= '
					<script type="text/javascript" src="../js/autosize.js"></script>
					<script type="text/javascript">
						function add_nota_privata() {
							var possible = "0123456789";
							var randomid = "";
							for( var i=0; i < 11; i++ ) {
								randomid += possible.charAt(Math.floor(Math.random() * possible.length));
							}
									
							$("<tr id=\'tr_note_"+randomid+"\'><td><textarea class=\'textarea_note\' name=\'note_nota[]\' onkeyup=\'auto_grow(this)\' id=\'note_nota[]\' style=\'width:620px;height:16px\'></textarea><input type=\'hidden\' name=\'note_nuova[]\' value=\'1\' /></td><td><input type=\'text\' readonly=\'readonly\' name=\'note_id_employee[]\' value=\''.Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$context->employee->id).'\' /></td><td><input type=\'text\' readonly=\'readonly\' name=\'note_date_add[]\' value=\''.date('d/m/Y').'\' /></td><td><a href=\'javascript:void(0)\' onclick=\'togli_nota("+randomid+");\'><img src=\'../img/admin/delete.gif\' alt=\'Cancella\' title=\'Cancella\' style=\'cursor:pointer\' /></a></td></tr>").appendTo("#tableNoteBody");
							
							$(".textarea_note").each(function () {
								this.style.height = ((this.scrollHeight)-4)+"px";
							});
							
							$(".textarea_note").on("change keyup keydown paste cut", "textarea", function () {
								$(this).height(0).height(this.scrollHeight);
							}).find("textarea").change();
							
							autosize($(".textarea_note"));

						}
							
						function togli_nota(id) {
							document.getElementById(\'tr_note_\'+id).innerHTML = "";
						}
							
							
						function cancella_nota(id) {
							$.ajax({
								  url:"ajax.php?cancellanota_cart=y",
								  type: "POST",
								  data: { 
								  cancella_nota: \'y\',
								  id_note: id
								  },
								  success:function(){
									alert("Nota cancellata con successo"); 
								  },
								  error: function(xhr,stato,errori){
									 // alert("Errore nella cancellazione:"+xhr.status);
								  }
							});
						
						}
						
						function auto_grow(element) {
						}

						
						
						</script>
						
						<table><thead>'.(count($cart_note) > 0 ? '<tr><th>Nota</th><th>Creata da</th><th>Ultima modifica</th><th></th></tr>' : '' ).'</thead><tbody id="tableNoteBody">
					';
					
					
					foreach($cart_note as $cart_nota) {
						$html .= '<tr id="tr_note_'.$cart_nota['id_note'].'">';
						$html .= '
						<td>
						<textarea class="textarea_note" name="note_nota['.$cart_nota['id_note'].']" id="note_nota['.$cart_nota['id_note'].']" style="width:620px; height:16px" onkeyup="auto_grow(this)">'.htmlentities($cart_nota['note'], ENT_COMPAT, 'UTF-8').'</textarea><input type="hidden" name="note_nuova['.$cart_nota['id_note'].']" id="note_nuova['.$cart_nota['id_note'].']" value="0" /></td>
						<td><input type="text" readonly="readonly" name="note_id_employee['.$cart_nota['id_note'].']" value="'.Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$cart_nota['id_employee']).'" /></td>
						<td><input type="text" readonly="readonly" name="note_date_add['.$cart_nota['id_note'].']" value="'.Tools::displayDate($cart_nota['date_upd'], 5).'" /></td>';
						$html .= '<td><a href="javascript:void(0)" onclick="javascript:var surec=window.confirm(\'Sei sicuro?\'); if (surec) { togli_nota('.$cart_nota['id_note'].'); cancella_nota('.$cart_nota['id_note'].'); } else { return false; }; "><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" style="cursor:pointer" /></a></td>';
						$html .='
						</tr>
						';
					}
					
					$html .= '</tbody></table><br /><script type="text/javascript">
					$(document).ready(function() {
						
							$(".textarea_note").each(function () {
								this.style.height = ((this.scrollHeight)-4)+"px";
							});
							
							$(".textarea_note").on("change keyup keydown paste cut", "textarea", function () {
								$(this).height(0).height(this.scrollHeight);
							}).find("textarea").change();
							
							autosize($(".textarea_note"));
												
						});
						
					</script>';
					
					$html .= '
					<script type="text/javascript">add_nota_privata();</script>
					<a href="javascript:void(0)" onclick="javascript:add_nota_privata()";><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" /> Clicca per aggiungere una nota privata</a> <br /><br />';
					
					/*
					if($cart->id < 96145 || $note_private != '')
					{
						$html .= '<p>'.($order->id? '<div style="width:908px; margin-top:14px; margin-left:0px; background-color:#f0ebd6; border:1px solid #cccccc; overflow-y:auto; height:150px">'.$note_private.'</div>' : '<textarea name="note_private" id="note_private"  style="width:910px; color:red"  class="rte">'.$note_private.'</textarea>').'<br />
						<script type="text/javascript">
					
					
						</script>';
					}*/
					
					$html .= '				
					</div>';
					
					$noleggio = Db::getInstance()->getValue("SELECT id_cart FROM noleggio WHERE id_cart = ".$_GET['id_cart']."");
					if($noleggio > 0)
						$datiNoleggio = Db::getInstance()->getRow("SELECT * FROM noleggio WHERE id_cart = ".$_GET['id_cart']."");
					
					//$html .= '<div '.($customer->is_company == 0 ? '<!-- style="display:none" -->' : '').'>';
					
					$html .= '<div>';
					
						$html .= '
						<div id="dettaglio_noleggio" '.($customer->id_default_group == 3 || $customer->id_default_group == 15 ? 'style="display:none"' : '').'>
						<table style="margin-left:-3px">
						<tr>
						
						<td>';
						
						if($order->id) {
							$html .= '<input type="text" readonly="readonly"  name="mesi_noleggio" id="mesi_noleggio" value="'.$datiNoleggio['mesi'].'" />';
						}
						else {
						
							
							/*$html .= '<input type="checkbox" name="attiva_noleggio" '.($noleggio > 0 ? 'checked="checked"' : '').'> <strong>ATTIVA NOLEGGIO</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';*/
							
							$html .= '<strong>Mesi noleggio</strong> (imposta su 0 per disattivare noleggio)<select name="mesi_noleggio" id="mesi_noleggio" onchange="calcola_noleggio(false)";>
							<option value="0">0</option>
							<option value="18" '.($noleggio > 0 ? ($datiNoleggio['mesi'] == 18 ? 'selected="selected"' : '') : '').'>18</option>
							<option value="24" '.($noleggio > 0 ? ($datiNoleggio['mesi'] == 24 ? 'selected="selected"' : '') : '').'>24</option>
							<option value="36" '.($noleggio > 0 ? ($datiNoleggio['mesi'] == 36 ? 'selected="selected"' : '') : '').'>36</option>
							<option value="48" '.($noleggio > 0 ? ($datiNoleggio['mesi'] == 48 ? 'selected="selected"' : '') : '').'>48</option>
							<option value="60" '.($noleggio > 0 ? ($datiNoleggio['mesi'] == 60 ? 'selected="selected"' : '') : '').'>60</option>
							
							</select>&nbsp;&nbsp;&nbsp;';
							
						}
						
						$html .= '
						</td>
						<td><strong>Spese contratto</strong></td>
						<td><input readonly="readonly" type="text" name="spese_contratto_noleggio" style="text-align:right; background-color:#f0ebd6" id="spese_contratto_noleggio" size="5"  '.($noleggio > 0 ? 'value="'.number_format($datiNoleggio['spese_contratto'],2,",","").'"' : '').' /> &euro; &nbsp;&nbsp;&nbsp;</td>
						
						<td><strong>Importo rata <span id="tipo_rata">'.($totale > 1500 && $totale < 2999 ? 'trimestrale' : 'mensile').'</span></strong></td>
						
						<td><input readonly="readonly" type="text" name="importo_rata_mensile_noleggio" style="text-align:right; background-color:#f0ebd6" id="importo_rata_mensile_noleggio" size="5" '.($noleggio > 0 ? 'value="'.($totale > 1500 && $totale < 2999 ? number_format(($totale*$datiNoleggio['parametri']/100)*3,2,",","").'"' : number_format($totale*$datiNoleggio['parametri']/100,2,",","").'"') : '').' /> &euro; &nbsp;&nbsp;&nbsp;
						<input type="hidden" id="parametro_noleggio" name="parametro_noleggio" value="'.$datiNoleggio['parametri'].'" />
						
						</td>
						
						
						</tr>
						</table>
						';
						
						
						$html .= '
						</div>';
					
						
					$html .= '</div>
					<br />
					
					';
					
					
					

					if($provvisorio == 1 && $context->employee->id != 1 && $context->employee->id != 2 && $context->employee->id != 3 && $context->employee->id != 6 && $context->employee->id != 22 && $context->employee->id != 7 && $context->employee->id != 14 && $context->employee->id != 17 && $context->employee->id != 12  && $context->employee->id != 19) 
					{
						if($order->id) {
						}
						else
						{
							$html .= '<input name="provvisorio" id="provvisorio" type="checkbox" checked="checked" style="display:none;" />';
						}
					}
					else
					{
						if($order->id) {
						}
						else
						{
							$html .= '
							<input name="provvisorio" id="provvisorio" onclick="if(this.checked) { document.getElementById(\'notifica_mail\').checked = false; document.getElementById(\'notifica_con_pdf\').checked = false;  }" type="checkbox" '.($provvisorio == 1 ? 'checked="checked"' : '').' />Carrello provvisorio? Se spuntato, non verr&agrave; mostrato al cliente<br /> <br />';
						}
					}
				
					if($order->id) {
					}
					else
					{
						// prima dell'onclick, per i tecnici: checked="checked" type="hidden" value="1"
						$html .= '
						
						<table style="margin-left:-3px">
						<tr>
						<td style="width:370px">
						<input name="'.($context->employee->id != 1 &&  $context->employee->id != 2 &&  $context->employee->id != 3 &&  $context->employee->id != 6 && $context->employee->id != 22 && $context->employee->id != 7 && $context->employee->id != 14 && $context->employee->id != 17  && $context->employee->id != 12  && $context->employee->id != 19 ? "prepara_mail" : "notifica_mail").'" id="'.($context->employee->id != 1 &&  $context->employee->id != 2 &&  $context->employee->id != 3 &&  $context->employee->id != 6 && $context->employee->id != 22 && $context->employee->id != 7 && $context->employee->id != 14 && $context->employee->id != 17  && $context->employee->id != 12  && $context->employee->id != 19 ? "prepara_mail" : "notifica_mail").'" '.($context->employee->id != 1 &&  $context->employee->id != 2 &&  $context->employee->id != 3 &&  $context->employee->id != 6 && $context->employee->id != 22 && $context->employee->id != 7 && $context->employee->id != 14  && $context->employee->id != 17 && $context->employee->id != 12  && $context->employee->id != 19 ? 'checked="checked" type="hidden" value="1"' : 'onclick="if(this.checked && document.getElementById(\'provvisorio\').checked ) { document.getElementById(\'provvisorio\').checked = false; }  else { }" type="checkbox"').' />'.($context->employee->id != 1 &&  $context->employee->id != 2 &&  $context->employee->id != 3 &&  $context->employee->id != 6 && $context->employee->id != 22 && $context->employee->id != 7 && $context->employee->id != 14  && $context->employee->id != 17 && $context->employee->id != 12  && $context->employee->id != 19 ? "Prepara le mail a cui inviare la notifica" : "Spunta questo flag se vuoi inviare una notifica via mail").' ';
						

						$html .= '
						
						
						</td><td style="width:370px">';
						$html .= ''.($context->employee->id != 1 &&  $context->employee->id != 2 &&  $context->employee->id != 3 &&  $context->employee->id != 6 && $context->employee->id != 22 && $context->employee->id != 7 && $context->employee->id != 14 && $context->employee->id != 17  && $context->employee->id != 12  && $context->employee->id != 19 ? '' : '<input name="notifica_con_pdf" onclick="if(this.checked && document.getElementById(\'provvisorio\').checked) { document.getElementById(\'provvisorio\').checked = false; } else { }" type="checkbox" id="notifica_con_pdf" /> ').($context->employee->id != 1 &&  $context->employee->id != 2 &&  $context->employee->id != 3 &&  $context->employee->id != 6 && $context->employee->id != 22 && $context->employee->id != 7 && $context->employee->id != 14   && $context->employee->id != 17 && $context->employee->id != 12  && $context->employee->id != 19 ? "" : "Vuoi inviare il PDF dell'offerta in allegato alla mail?").'</td></tr>';
						
						if(strpos($cart_name, 'Rinnovo') !== false && ($context->employee->id == 6 || $context->employee->id == 14))
						{
							$html .= '<tr><td><input name="notifica_contratto" onclick="if(this.checked && document.getElementById(\'provvisorio\').checked) { document.getElementById(\'provvisorio\').checked = false; } else { }" type="checkbox" id="notifica_contratto" /> Invia notifica contratto</td><td></td></tr>';
						}
					}
					
					$mail_notifiche = '';
						
					$ultima_notifica_normale = '';
					$ultima_notifica_pdf = '';
					$data_ultima_notifica_normale = '';
					$data_ultima_notifica_pdf = '';
					
					
					if(!empty($notifiche)) {
						$last_notifica = 0;
						$numNotifiche = count($notifiche);
						foreach($notifiche as $notifica) {
							
							$persona = Db::getInstance()->getValue('
								SELECT CONCAT(firstname," ",lastname) 
								FROM persone
								WHERE email = "'.$notifica['email'].'" 
									AND id_customer = '.$customer->id.'
							');
							
							if($persona == '')
								$persona = '--';
							
							$mail_notifiche .= "<tr><td>".$persona."</td><td><a href='mailto:".$notifica['email']."'>".$notifica['email']."</a></td><td>".Tools::displayDate($notifica['data'], (int)$context->language->id, true)."</td><td>".($notifica['tipo'] == '' ? '--' : $notifica['tipo'])."</td></tr>";
								$ultima_mail_notifica = $notifica['email'];
								
								if($notifica['tipo'] == 'PDF')
								{
									
									//if($data_ultima_notifica_pdf <= $notifica['data'] || $data_ultima_notifica_pdf == '')
									//{	
										$data_ultima_notifica_pdf = $notifica['data'];
										
									$ultima_notifica_pdf = '<img src="../img/admin/notifica-pdf.png" alt="Notifica letta" title="Il PDF di questo carrello &egrave; gi&agrave; stato inviato al cliente. L\'ultima volta in data '.Tools::displayDate($data_ultima_notifica_pdf, (int)($context->language->id)).'" class="img_control" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
									//}
								}	
								
								if($preventivo == 1 && $visualizzato == 1)
									$ultima_notifica_mail = '<img src="../img/admin/notifica-letta.png" alt="Carrello letto" title="Il cliente ha visualizzato il preventivo" class="img_control" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
								else
									$ultima_notifica_mail = '<img src="../img/admin/notifica-non-letta.png" alt="Carrello non letto" title="Il cliente non ha ancora visualizzato il preventivo" class="img_control" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					
							
								if($data_ultima_notifica_normale <= $notifica['data'] || $data_ultima_notifica_normale = '')
									$data_ultima_notifica_normale = $notifica['data'];
								
								
							
								
							
							if(++$last_notifica === $numNotifiche) 
								$ultima_mail_notifica = $notifica['email'];
						}
					}
					
					
					if($rif_prev != '' && $rif_prev != 0) 
					{
						if(is_numeric($rif_prev) && $rif_prev != 0)
						{
							$status = Db::getInstance()->getValue("SELECT status FROM form_prevendita_thread WHERE id_thread = ".$rif_prev."");
						}
						else
						{
							$first = substr($rif_prev,0,1);
							$thread = substr($rif_prev,1,99);
							
							
							if($first == 'T')
							{
								$status = Db::getInstance()->getValue("SELECT status FROM "._DB_PREFIX_."customer_thread WHERE id_customer_thread = ".$thread."");
							}
							else if($first == 'A')
							{
								$status = Db::getInstance()->getValue("SELECT status FROM action_thread WHERE id_action = ".$thread."");
							}
							else if($first == 'P')
							{
								$status = Db::getInstance()->getValue("SELECT status FROM form_prevendita_thread WHERE id_thread = ".$thread."");
							}
						}
						
						if($status != 'closed' && $status != '')
						{	
							
							$html .= '<tr><td colspan="2"><br /><div style="border:3px solid red; padding:5px; margin-left:-7px"><input type="checkbox" name="chiudi_padre" />&nbsp;&nbsp;&nbsp;<strong style="color:red">ATTENZIONE!</strong>  L\'azione padre di questo carrello risulta ancora aperta. Spunta questa casella se vuoi chiuderla.</div>';
						}
						$html .= "<br /></td></tr>";
					}
					
					
					$ultima_notifica_normale = '<img src="../img/admin/notifica-inviata.png" alt="Notifica letta" title="Questo carrello &egrave; gi&agrave; stato notificato al cliente. L\'ultima volta in data '.Tools::displayDate($data_ultima_notifica_normale, (int)($context->language->id)).'" class="img_control" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					
					$html .= ($numero_notifiche == 0 || $ultima_notifica_normale == '' ? '</table>' : '<tr><td colspan="2"><br />'.$ultima_notifica_normale.$ultima_notifica_mail.$ultima_notifica_pdf.'<a href="index.php?controller=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&tab_name=todo&azione=todo&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo_todo=Telefonata" target="_blank"><img src="../img/admin/todo-telefonata.png" alt="Apri telefonata" title="Apri to-do telefonata per questo carrello" class="img_control" /></a></td></tr></table><br /> '.'<strong>Vedi lista notifiche:</strong><br />').'
					';
					
						
					if ($context->employee->id != 1 &&  $context->employee->id != 2 &&  $context->employee->id != 3 &&  $context->employee->id != 6 && $context->employee->id != 22 && $context->employee->id != 7 && $context->employee->id != 14 && $context->employee->id != 17 && $context->employee->id != 12  && $context->employee->id != 19) {
						
						if($provvisorio == 1)
							$html .= '<strong style="color:red">ATTENZIONE: carrello provvisorio. Per renderlo definitivo e visibile dal cliente, sentire qualcuno del reparto commerciale</strong><br /><br />';
					
						if(isset($_GET['notifica_inviata']) && $numero_notifiche > 0) {
							$html .= "<script type='text/javascript'>alert('Notifica correttamente inviata al cliente');</script>";
						} 
						else if(isset($_GET['notifica_inviata']) && $numero_notifiche == 0) {
							$html .= "<script type='text/javascript'>alert('Notifica NON INVIATA al cliente. Assicurarsi di aver scelto bene l\'indirizzo mail.');</script>";
						}
						else {
						
						}
					
					}
					
						
					if(!empty($notifiche)) {
							
						//$html .= '<span onclick="$(\'#dettaglio_notifiche\').slideToggle(); " style="cursor: pointer; color:blue; text-decoration:underline"><img src="../img/admin/arrow.gif" alt="'.$this->l('Dettaglio notifiche').'" title="'.$this->l('Dettaglio notifiche').'" style="float:left; margin-right:5px;"/>'.$this->l('Clicca qui per aprire il dettaglio notifiche').'</span>
						$html .= '<div id="dettaglio_notifiche">
						<table class="table">
						<tr>
						<th style="width:250px">Notificata a</th><th>Email</th><th style="width:250px">In data</th><th>Tipo</th></tr>
						';
						$html .= $mail_notifiche;
							
							
						$html .='
						</table>
						</div>
						';
					}
					else 
					{ 
						
					}
					$html .= '<br /><table class="table">
					<tr>
					<th style="width:250px">Telefono cliente</th><th style="width:250px">Cellulare cliente</th>';
							
					$html .= '</th></tr>';
					$customer_phone = Db::getInstance()->getRow("SELECT	phone, phone_mobile FROM "._DB_PREFIX_."address WHERE id_customer = ".$customer->id." AND deleted = 0 AND fatturazione = 1 AND active = 1");
						
					$html .= '<tr><td>'.(!$customer_phone['phone'] ? 'Non presente' : '<a href="callto:'.$customer_phone['phone'].'">'.$customer_phone['phone'].'</a>').'</td><td>'.(!$customer_phone['phone_mobile'] ? 'Non presente' : '<a href="callto:'.$customer_phone['phone_mobile'].'">'.$customer_phone['phone_mobile'].'</a>').'</td></tr>';
					$html .= '</table><br />';
						
						
					if($order->id) {
					}
					
					else {
					$html .= '<table><tr><td>';
						if ($context->employee->id != 1 &&  $context->employee->id != 2 &&  $context->employee->id != 3 &&  $context->employee->id != 6 && $context->employee->id != 22 && $context->employee->id != 7 && $context->employee->id != 14 && $context->employee->id != 17 && $context->employee->id != 12  && $context->employee->id != 19) {
							$html .= 'Seleziona le mail da preparare per la notifica: ';
						}
						else {
							$html .= 'Seleziona la persona a cui vuoi inviare la notifica: ';
							
						}
						$persone = Db::getInstance()->executeS("SELECT id_persona, firstname, lastname, email FROM persone WHERE id_customer = ".$customer->id." AND email != ''");
							
						$email_preparate = Db::getInstance()->getValue("SELECT email_preparate FROM "._DB_PREFIX_."cart WHERE id_cart = ".Tools::getValue('id_cart')."");
					
						$email_preparate = unserialize($email_preparate);
								
						$indirizzo_email_persona_a = Db::getInstance()->getValue("SELECT email FROM persone WHERE id_persona = ".$id_persona."");
								
						$html .= '<script type="text/javascript" src="../js/select2.js"></script>
						<script type="text/javascript">
						'.($context->employee->id == 111111111111 ? '' : '$(document).ready(function() { $("#seleziona-persona").select2(); });').'
						</script>';

						$html .= '</td><td>
						<select multiple id="seleziona-persona" name="seleziona-persona[]" style="width:500px">';
						if($customer->is_company == 1) {
							$html .= "<option value=''>-- Seleziona persona --</option>";
									
							if($numero_notifiche > 0 && empty($email_preparate)) {
									
								if($customer->email != '')	
									$html .= "<option ".($numero_notifiche > 0 ? ($ultima_mail_notifica == $customer->email ? "selected='selected'" : "selected='selected'") : "selected='selected'")." value='".$customer->email."'>".$customer->email." (Mail dell'account)</option>";
									
							}
								
							else  {
								if(!empty($email_preparate)) {
									$id_persona_base = Db::getInstance()->getValue('SELECT id_persona FROM persone WHERE id_customer = '.$customer->id.' AND email = "'.$customer->email.'" AND email != ""');
									$html .= "<option ".(in_array($id_persona_base, $email_preparate) ? "selected='selected'" : "")." value='".$customer->email."'>".$customer->email." (Mail dell'account)</option>";
								}
								else {
									if($customer->email != '')
										$html .= "<option selected='selected' value='".$customer->email."'>".$customer->email." (Mail dell'account)</option>";
								}
							}
									
							$i_persona = 0;
							$mail_persone = array();
									
							foreach ($persone as $persona) {
											
								if(trim($persona['email']) == trim($customer->email))
								{
								}
										
								else 
								{
										
									if(in_array($persona['email'], $mail_persone)) {
														
									}
									else {
											
										if($numero_notifiche > 0 && empty($email_preparate)) {
											
											$html .= "<option ".($numero_notifiche > 0 && empty($email_preparate) ? ($ultima_mail_notifica == $persona['email'] ? "selected='selected'" : "") : ($i_persona == 0 ? "selected='selected'" : ""))." value='".$persona['email']."'>".$persona['firstname']." ".$persona['lastname']." - ".$persona['email']."</option>";
										}
												
										else {
											if(!empty($email_preparate)) {
												$html .= "<option ".(in_array($persona['id_persona'], $email_preparate) ? "selected='selected'" : "")." value='".$persona['email']."'>".$persona['firstname']." ".$persona['lastname']." - ".$persona['email']."</option>";
											}
											else {
												$html .= "<option value='".$persona['email']."'>".$persona['firstname']." ".$persona['lastname']." - ".$persona['email']."</option>";
											}
																
										}
											
												
									}
									$mail_persone[] = $persona['email'];
									$i_persona++;
								}
							}
										
							
						}
						else {
								
							if($numero_notifiche > 0) {
								$stop_mail_cliente = 0;
								$mail_persone = array();
								foreach($notifiche as $notifica) {
									if($notifica['email'] == $customer->email) {
										if($stop_mail_cliente == 0) {
											$html .= "<option value='".$customer->email."' ".($ultima_mail_notifica == $notifica['email'] ? "selected='selected'" : "").">".$customer->firstname." ".$customer->lastname." - ".$customer->email."</option>";
											$stop_mail_cliente = 1;
										}
									}
									else {
										if(in_array($notifica['email'], $mail_persone)) {
														
										}
										else {
											$html .= "<option value='".$notifica['email']."' ".($ultima_mail_notifica == $notifica['email'] ? "selected='selected'" : "").">".$notifica['email']."</option>";
											$mail_persone[] = $notifica['email'];
										}
									}
												
								}
							}
							else {
								$html .= "<option value='".$customer->email."' selected='selected'>".$customer->firstname." ".$customer->lastname." - ".$customer->email."</option>";
							}
									
						}						
						$html .= "</select></td></tr><tr><td>";

						$html .= 'Se le persone non sono nell\'elenco, inserisci in questo campo gli indirizzi email separati da punto e virgola</td><td> <input type="text" style="width:500px" autocomplete="off" name="customer_email_msg_cart" id="customer_email_msg_cart" value="" /></td></tr></table>';
							
						if ($context->employee->id != 1 &&  $context->employee->id != 2 &&  $context->employee->id != 3 &&  $context->employee->id != 6 && $context->employee->id != 22 && $context->employee->id != 7 && $context->employee->id != 14 && $context->employee->id != 17 && $context->employee->id != 12  && $context->employee->id != 19) {
							
						}
						else {
							$html .= "<br /><br />";
							$html .= '
							<strong style="font-size:14px">Copia conoscenza:</strong> <br />'; 
									
							foreach ($impiegati_ez as $impez) {
								
								$html .= "<div style='float:left; margin-right:10px'><input type='checkbox' name='conoscenza[]' id='conoscenza_".$impez['id_employee']."'value='".$impez['id_employee']."' /> ".$impez['firstname']."  ".substr($impez['lastname'],0,1).".".'</div>';
													
							}
								
						}
								
						$html .= "<br /><br />";
							
						$html .= '<script type="text/javascript">
							function check_notifica_provvisorio() {
								
								var today = new Date();
								var today = today.getTime();
								
								var validita = document.getElementById("validita").value;
								if(validita.indexOf("-") > -1)
									validita = validita.split("-");
								else
									validita = validita.split("/");
								
								var newValidita=validita[1]+"/"+validita[0]+"/"+validita[2];
								
								var validitaTime = new Date(newValidita).getTime();
								
								if(validitaTime < today)
									if (confirm("ATTENZIONE! La data di validità di questa offerta "+String.fromCharCode(0xE8)+" scaduta. Vuoi aggiornarla? Premi \'OK\' per aggiornarla di 15 giorni rispetto alla data di oggi, \'Annulla\' per lasciarla com\'"+String.fromCharCode(0xE8)+". ") == true)
									{
										var todayNew = "'.date('d/m/Y', strtotime($Date. ' + 15 days')).'";
										document.getElementById("validita").value = todayNew;
										document.getElementById("price_updates").value = "y";
									}
									else
									{

									}
								
								if(document.getElementById("provvisorio") && document.getElementById("notifica_mail")) 
								{
								
									if (document.getElementById("provvisorio").checked && document.getElementById("notifica_mail").checked) {
										alert("ATTENZIONE! Per notificare questo carrello al cliente "+String.fromCharCode(0xE8)+" necessario togliere il flag del carrello provvisorio. Il carrello sar"+String.fromCharCode(0xE0)+" adesso salvato, ma non verr"+String.fromCharCode(0xE0)+" inviata alcuna notifica al cliente (solo allo staff, se selezionato).");
										
									}
								}
								
						}
							
							
						</script>
						';
					
					
						if($visualizzato == 1) {
							$html .= "<strong>Il cliente ha visualizzato questa offerta sul sito</strong>";
						}
						else {
							$html .= "<strong>Il cliente non ha ancora visualizzato questa offerta sul sito</strong>";
						}
					}	
					$html .= "";
					
					if($provvisorio == 0 && !$order->id) 
						$html .= '<div style="border:3px solid red; padding:5px; '.($context->employee->id == 7 || $context->employee->id == 6 || $context->employee->id == 22  || $context->employee->id == 22 || $context->employee->id == 14 || $context->employee->id == 17 || $context->employee->id == 12  && $context->employee->id != 19 ? 'display:block; ' : 'display:none' ).'" ><input type="checkbox" name="no_csv" id="no_csv" /> Spunta questo flag per NON creare il csv per importazione ordine su spring.</div>';

					$html .= "<br /><br />";
					
					if(Tools::getValue('preventivo') == 2 || $preventivo == 2) {
						$html .=  '
						<button name="Apply" id="Apply" type="submit" class="button" onclick="var rif_ordine = document.getElementById(\'rif_ordine\').value; if(rif_ordine == \'\') { alert (\'ATTENZIONE! Devi inserire un riferimento ordine per procedere con un ordine manuale.\'); $(\'html, body\').animate({ scrollTop: $(\'#rif_ordine\').offset().top }, 1500); return false; } else { check_notifica_provvisorio(); } /* javascript:var surec=window.confirm(\'Pagamento selezionato per questo carrello: \'+document.getElementById(\'payment\').value+\'. Vuoi confermarlo?\'); if (surec) { } else { $(\'html, body\').animate({ scrollTop: $(\'#payment\').offset().top }, 1500); return false; } */" value="Conferma ordine" /><img src="../img/admin/enabled-2.gif" alt="Conferma ordine" title="Conferma ordine" />&nbsp;Conferma ordine
						</button>
						<br />
						  <input name="totaleprodotti" id="totaleprodotti" type="hidden" value="'.$totale.'" />
						  <input name="id_order" type="hidden" value="'. $_GET['id_order'] .'" />
						  <input name="tax_rate" type="hidden" value="'. $tax_rate .'" />
						  <input name="id_lang" type="hidden" value="'. $id_lang .'" />
						';
						
						
					
					}
					if(Tools::getIsset('vedirevisione')){}
					else {
						
						$html .=  '<button name="Apply" id="Apply" type="submit" class="button" onclick="'.(Tools::getValue('preventivo') == 2 || $preventivo == 2 ? 'var rif_ordine = document.getElementById(\'rif_ordine\').value; if(rif_ordine == \'\') { /* alert (\'ATTENZIONE! Devi inserire un riferimento ordine per procedere con un ordine manuale.\'); $(\'html, body\').animate({ scrollTop: $(\'#rif_ordine\').offset().top }, 1500); return false; */ } else { check_notifica_provvisorio(); }' : ' check_notifica_provvisorio();').' /* javascript:var surec=window.confirm(\'Pagamento selezionato per questo carrello: \'+document.getElementById(\'payment\').value+\'. Vuoi confermare questo pagamento?\'); if (surec) { } else { $(\'html, body\').animate({ scrollTop: $(\'#payment\').offset().top }, 1500); return false; } */" /><img src="../img/admin/enabled-2.gif" alt="Conferma" title="Conferma" />&nbsp;'.(Tools::getValue('preventivo') == 2 || $preventivo == 2 ? 'Conferma solo carrello'  : 'Conferma').'
						</button>';
						
						
						$html .= '
						
						<br />
						
						
						  <input name="totaleprodotti" id="totaleprodotti" type="hidden" value="'.$totale.'" />
						  <input name="id_order" type="hidden" value="'. $_GET['id_order'] .'" />
						  <input name="tax_rate" type="hidden" value="'. $tax_rate .'" />
						  <input name="id_lang" id="id_lang" type="hidden" value="'. $id_lang .'" />
						';
					}
				}
				$html .= '<br />
				
				
				</form>
				';
				if(!isset($_POST['copia_carrello'])) {
					if(Tools::getIsset('vedirevisione')){}
					else {
						$html .= '
					
						<p style="margin-top:-50px; padding-left:150px">
						<form action="#copia-carrello" method="post">
						<input type="hidden" name="id_cart" value="'.$_GET['id_cart'].'" />
						<button style="display:block; float:left; '.(Tools::getValue('preventivo') == 2 || $preventivo == 2 ? 'margin-left:180px' : 'margin-left:150px').'; z-index:999999;" name="copia_carrello" type="submit" class="button" onclick="$(\'#Apply\').val(\'copia_carrello\'); $(\'#Apply\').trigger(\'click\'); return false;"><img src="../img/admin/copy_files.gif" alt="Copia" title="Copia" />&nbsp;Copia su altro cliente</button>
						
						<button style="display:block; float:left; '.(Tools::getValue('preventivo') == 2 || $preventivo == 2 ? 'margin-left:20px' : 'margin-left:30px').'; z-index:99999;" name="copia_carrello_stesso" type="submit" onclick="$(\'#Apply\').val(\'copia_carrello_stesso\'); $(\'#Apply\').trigger(\'click\'); return false;" class="button"><img src="../img/admin/return.gif" alt="Copia su questo cliente" title="Copia su questo cliente" />&nbsp;Copia su questo cliente</button>
						
						'.($order->id > 0 ? '' : '<button style="display:block; float:left; '.(Tools::getValue('preventivo') == 2 || $preventivo == 2 ? 'margin-left:20px' : 'margin-left:30px').'; z-index:99999;" name="sposta_carrello" type="submit" class="button" onclick="$(\'#Apply\').val(\'sposta_carrello\'); $(\'#Apply\').trigger(\'click\'); return false;"><img src="../img/admin/move.gif" alt="Sposta" title="Sposta" />&nbsp;Sposta su altro cliente</button>').'
						</form>
						</p>';
					
					}
				}

				if($provvisorio == 0 && Tools::getValue('preventivo') != 2 && !$order->id && ($context->employee->id == 1 ||  $context->employee->id == 2 || $context->employee->id == 3 || $context->employee->id == 6 || $context->employee->id == 22 || $context->employee->id == 7 || $context->employee->id == 14 || $context->employee->id == 17 || $context->employee->id == 12 ||  $context->employee->id == 19)) {			
					$html .= '<div style="position:relative; margin-top:-30px;float:left; margin-left:730px;">';
					$html .= '<a class="button" id="converti_in_ordine" style=" margin-left:0px; margin-top:3px; height:19px; display:block; color:black; text-decoration:none;"  href="#" onclick="
					'.($creato_da >= 0 ? 'var rif_ordine = document.getElementById(\'rif_ordine\').value; if(rif_ordine == \'\') { alert (\'ATTENZIONE! Devi inserire un riferimento ordine per procedere con un ordine manuale. Inseriscilo e clicca su CONFERMA prima di procedere con la conversione.\'); return false; }' : '').'this.href = \'index.php?controller=AdminCarts&id_customer='.$customer->id.'&id_cart='.$_GET['id_cart'].'&viewcart&no_csv=\'+document.getElementById(\'no_csv\').value+\'&convertcart&token='.$this->token.'\'; var surec=window.confirm(\'Sei sicuro?\'); if (surec) { $(\'#Apply\').val(\'Conferma ordine\'); $(\'#Apply\').trigger(\'click\'); return false; } else { return false; }"><img src="../img/admin/money.gif" alt="Converti" title="Converti" />&nbsp;Converti in ordine</a>
					</div>
					';
				}
			
			if(Tools::getIsset('vedirevisione')){}
			else {
				$html .= '<div style="position:relative; margin-top:-30px;float:left; margin-left:650px;"><form action="ajax.php" method="post" onsubmit="saveCreaRevisione(); return false;" id="crea_revisione">';
				// $html .= '<input type="submit" id="submitCreaRevisione" class="button" onclick="var surec = window.confirm(\'Sei sicuro/a?\'); if (surec) {  } else { return false; }"  value="'.$this->l('Crea revisione (prima salvare)').'"  />';
				$html .= '
				<div id="note_feedback"></div><div id="risposta_feedback" style="float:left"></div></form></div>';
			}
					
			
			if($order->id) {
				if(!Tools::getIsset('vedirevisione')) {
					$tokenOrders = Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$this->context->employee->id);

					$html .= '<div style="position:relative; margin-top:-20px;float:left; height:19px; margin-left:740px;">';
					$html .= '<a class="button" href="?controller=AdminOrders&id_order='.(int)($order->id).'&vieworder&token='.$tokenOrders.'">Vai all\'ordine</a>';
					$html .= '</div>
					';
				}
			}
			
			$html .= '<div style="clear:both"></div><br /><br /><a href="?controller=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($context->employee->id)).'&tab_name=carts" class="button" id="converti_in_ordine" style=" margin-left:0px; margin-top:3px; height:19px; color:black; text-decoration:none;" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }"><img src="../img/admin/close.png" alt="Chiudi" title="Chiudi" />&nbsp;Chiudi senza salvare</a>
			
			
			<button class="button" style="margin-top:-2px; margin-left:30px" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { resetThisForm(); } else { return false; }"> <img src="../img/admin/quick.gif" alt="Pulisci form" title="Pulisci form" />&nbsp;Pulisci form </button>
			
			</form>
			<br /><br />';
			
			if(Tools::getIsset('copia_carrello') || Tools::getIsset('copia_carrello_stesso') || Tools::getIsset('sposta_carrello')) {
			
				$html .= "<p id='copia-carrello' style='margin-top:-20px'>";
				
				$html .= '
					<script type="text/javascript">
						$(document).ready(function () {
							$("#hider").fadeIn("slow");
							$("#popup_box").fadeIn("slow");
						});
					</script>

					<div class="hider"></div>
					<div class="popup_box"><form action="#copia-carrello" method="post">
				';
				
				if(!Tools::getIsset('copia_carrello_stesso')) {
					$html .= '
						<input type="hidden" name="id_cart" value="'.Tools::getValue('id_cart').'" />
						<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
								<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete.js"></script>
						Seleziona il cliente su cui '.(Tools::getIsset('copia_carrello') ? 'copiare' : 'spostare').' il carrello scegliendolo da questo modulo. '.(Tools::getIsset('copia_carrello') ? '<strong>ATTENZIONE</strong>: tutte le voci saranno aggiornate secondo i prezzi attualmente in vigore.' : '').'<br /><br />
						<input size="123" type="text" value="" id="customer_autocomplete_input" /><br /><br />
							<script type="text/javascript">
									var formProduct = "";
									var products = new Array();
							</script>
							
							<script type="text/javascript">
							
							function addCustomer_TR(event, data, formatted)
							{
								var id_customer = data[0];
								var company = data[1];
								var firstname = data[2];
								var lastname = data[3];
								var vat_number = data[4];
								var tax_code = data[5];
								
								document.getElementById("customer_autocomplete_input").value = id_customer+" - "+company+" - "+firstname+" "+lastname+" - "+vat_number+" - "+tax_code; 
								document.getElementById("copy_to").value = id_customer;
							}
							
							/* function autocomplete */
							$(function() {
								$(\'#customer_autocomplete_input\')
									.autocomplete(\'ajax_customer_list.php\', {
										minChars: 1,
										max:50,
										scroll:true,
										formatItem: function(item, position) {
														return \'<tr><td style="width:50px; text-align:right">\'+item[0]+\'</td><td style="width:200px; text-align:left">\'+item[1]+\'</td><td style="width:150px;text-align:left">\'+item[2]+\'</td><td style="width:150px;text-align:left ">\'+item[3]+\'</td><td style="width:125px;text-align:left">\'+item[4]+\'</td><td style="width:125px;text-align:left">\'+item[5]+\'</td></tr>\'
														
														;
													}	
										
									}).result(addCustomer_TR);	
							});
							
							$("#customer_autocomplete_input").css(\'width\',\'850px\');
							$("#customer_autocomplete_input").keypress(function(event){
								var keycode = (event.keyCode ? event.keyCode : event.which);
								if (keycode == "13") {
								event.preventDefault();
								event.stopPropagation();    
								}
							});
								
							</script>
						<input name="copy_to" id="copy_to" value="" type="hidden" />
					';
				}
				else
				{
					$html .= '<input name="copia_carrello_stesso" id="copia_carrello_stesso" value="" type="hidden" />';
				}

				$html .= '
					<span style="">Vuoi mantenere gli stessi margini? <select style=""  name="mantieni_stessi_prezzi"><option value="s">S&igrave;, mantieni</option><option value="n">No, aggiorna i margini</option></select> </span>
					
					<button name="'.(Tools::getIsset('copia_carrello') || Tools::getIsset('copia_carrello_stesso') ? 'copia' : 'sposta').'_carrello_conferma" type="submit" class="button">'.(Tools::getIsset('copia_carrello')  || Tools::getIsset('copia_carrello_stesso') ? '<img src="../img/admin/copy_files.gif" alt="Copia" title="Copia" />&nbsp;Conferma copia' : '<img src="../img/admin/move.gif" alt="Sposta" title="Sposta" />&nbsp;Conferma sposta').' carrello </button>
					<a href="'.self::$currentIndex.'&id_cart='.Tools::getValue('id_cart').'&viewcart&token='.$this->token.'" class="button"><img src="../img/admin/disabled.gif" alt="Annulla" title="Annulla" />&nbsp;Annulla</a>
					
					</form></div>
				';
				
				$html .= "<p>";
			}
			
			if(Tools::getIsset('copia_carrello_conferma')) {
			
				if(Tools::getIsset('copia_carrello_stesso'))
					$copy_to = $customer->id;
				else
					$copy_to = trim(Tools::getValue('copy_to'));
				
				if(Tools::getValue('id_cart') <= 0 || !is_numeric(Tools::getValue('id_cart'))) 
					Tools::redirectAdmin(self::$currentIndex."&viewcart&createnew&provvisorio=1&preventivo=1&id_customer=".$copy_to."&token=".$this->token."#modifica-carrello");
				
				/*if(Tools::getIsset('copia_carrello_stesso'))
					$_POST['mantieni_stessi_prezzi'] = 's';*/
				
				$nuovo_carrello = Db::getInstance()->getValue("SELECT id_cart FROM "._DB_PREFIX_."cart ORDER BY id_cart DESC") or die (mysql_error());
				$nuovo_carrello = $nuovo_carrello + 1;
								
				Customer::Storico($nuovo_carrello, 'C', $context->employee->id, 'Ha copiato questo carrello a partire dal carrello n. <a href=\'index.php?controller=AdminCarts&id_cart='.Tools::getValue('id_cart').'&viewcart&token='.$this->token.'\'>'.Tools::getValue('id_cart').'</a>');
				
				$da_copiare = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'cart WHERE id_cart = '.Tools::getValue('id_cart').'');
				$da_copiare_2 = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'cart_product WHERE id_cart = '.Tools::getValue('id_cart').'');
				
				if(Tools::getIsset('preventivo'))
					$da_copiare['preventivo'] = Tools::getValue('preventivo');
					
				$telegestione = $da_copiare['telegestione'];

				// Carrelli Tipo - Telegestione Annuale
				if(Tools::getValue('id_cart') == 84366)	
				{
					$ordine_rif = str_replace('O','',Tools::getValue('rif_prev'));
					
					$ordine_rif = '';
					
					foreach($_POST['orders_telegest'] as $key => $value)
						$ordine_rif .= ' id_order = "'.$value.'" OR';
										
					$ordine_rif .= ' id_order = "99999999999999999"';
					
					$prodotti_rif = Db::getInstance()->executeS('SELECT product_id, product_price, wholesale_price, sum(product_quantity) product_quantity, reduction_percent FROM '._DB_PREFIX_.'order_detail WHERE ('.$ordine_rif.') GROUP BY product_id');
					
					$telegestione = array();
					$exclude_sum = 0;

					foreach($prodotti_rif as $prodotto_rif)
					{
						$categoria_prodotto_telegest = Db::getInstance()->getValue('SELECT id_category_default FROM '._DB_PREFIX_.'product WHERE id_product = '.$prodotto_rif['product_id']);
								
						if($categoria_prodotto_telegest != 119)
						{
							$telegestione_prod = array();
							$telegestione_prod['id_product'] = $prodotto_rif['product_id'];
							$telegestione_prod['price'] = $prodotto_rif['product_price'];
							$telegestione_prod['no_acq'] = $prodotto_rif['no_acq'];
							$telegestione_prod['reduction_percent'] = $prodotto_rif['reduction_percent'];
							$telegestione_prod['wholesale_price'] = $prodotto_rif['wholesale_price'];
							$telegestione_prod['quantity'] = $prodotto_rif['product_quantity'];
							$telegestione[] = $telegestione_prod;
						}
						else
						{
							$exclude_sum += ((($prodotto_rif['product_price'] - ($prodotto_rif['product_price']/100 * $prodotto_rif['reduction_percent']))) * $prodotto_rif['product_quantity']);
						}
					}
					
					$order_total = Db::getInstance()->getValue('SELECT sum(total_products) FROM '._DB_PREFIX_.'orders WHERE ('.$ordine_rif.')');
					$order_total -= $exclude_sum;
					
					if($order_total < 500)
						$telegest_value = 155;
					else {
						$undici_per_cento = ($order_total / 100) * 11;
						$telegest_value = 90 + $undici_per_cento;
					}
								
					$telegestione = serialize($telegestione);
					$telegestione = addslashes($telegestione);
				}
				
				$cstp = new Customer($copy_to);

				$personac = Db::getInstance()->getValue('
					SELECT id_persona 
					FROM persone
					WHERE firstname = "'.$cstp->firstname.'" 
						AND lastname = "'.$cstp->lastname.'"
				');
				
				Db::getInstance()->execute('
					INSERT INTO '._DB_PREFIX_.'cart (id_cart, id_carrier, id_lang, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, date_add, date_upd, rif_prev, name, riferimento, premessa, esigenze, risorse, note, note_private, preventivo, consegna, transport, in_carico_a, total_products, attachment, created_by, id_employee, notifiche, telegestione, numero_notifiche, provvisorio, prezzi_carrello, rif_ordine, cig, cup, ipa, data_ordine_mepa, template, competenza_dal, competenza_al, cadenza, scadenza, decorrenza, esito, causa, concorrente) 
					VALUES ('.$nuovo_carrello.', "'.($customer->id == 44431 && Tools::getValue('id_cart') != 84366 ? 0 : $da_copiare['id_carrier']).'", "'.$da_copiare['id_lang'].'","'.addslashes($da_copiare['id_address_delivery']).'","'.addslashes($da_copiare['id_address_invoice']).'",1,'.$copy_to.',0,"",0,0,"","'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'","'.(Tools::getIsset('rif_prev') ? Tools::getValue('rif_prev') : addslashes($da_copiare['rif_prev'])).'","'.addslashes($da_copiare['name']).'","'.$personac.'","'.addslashes($da_copiare['premessa']).'","'.addslashes(($da_copiare['esigenze'] != '' ? $da_copiare['esigenze'] : $da_copiare_2['esigenze'])).'", "'.addslashes($da_copiare['risorse']).'", "'.($da_copiare['note'] != '' ? addslashes($da_copiare['note']) : addslashes($da_copiare_2['note'])).'","'.addslashes(($da_copiare['note_private'] != '' ? $da_copiare['note_private'] : $da_copiare_2['note_private'])).'","'.addslashes($da_copiare['preventivo']).'","'.addslashes($da_copiare['consegna']).'","'.($_POST['mantieni_stessi_prezzi'] == 's' ? addslashes($da_copiare['transport']) : addslashes($da_copiare['transport'])).'","'.addslashes($da_copiare['in_carico_a']).'","'.$da_copiare['total_products'].'","'.$da_copiare['attachment'].'",'.$context->employee->id.','.$context->employee->id.',"","'.$telegestione.'",0,1, "'.$da_copiare['prezzi_carrello'].'","'.$da_copiare['rif_ordine'].'","'.$da_copiare['cig'].'","'.$da_copiare['cup'].'","'.$da_copiare['ipa'].'","'.$da_copiare['data_ordine_mepa'].'", "'.addslashes($da_copiare['name']).'", "'.addslashes($da_copiare['competenza_dal']).'", "'.addslashes($da_copiare['competenza_al']).'", "'.addslashes($da_copiare['cadenza']).'", "'.addslashes($da_copiare['scadenza']).'", "'.addslashes($da_copiare['decorrenza']).'", "'.addslashes($da_copiare['esito']).'", "'.addslashes($da_copiare['causa']).'", "'.addslashes($da_copiare['concorrente']).'")
				');
				
				Db::getInstance()->execute('
					INSERT INTO carrelli_creati (id_cart, id_carrier, id_lang, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, date_add, date_upd, rif_prev, name, riferimento, premessa, esigenze, risorse, note, note_private, preventivo, consegna, transport, in_carico_a, total_products, attachment, created_by, id_employee, notifiche, telegestione, numero_notifiche, provvisorio, prezzi_carrello, rif_ordine, cig, cup, ipa, data_ordine_mepa, template, competenza_dal, competenza_al, cadenza, scadenza, decorrenza, esito, causa, concorrente) 
					VALUES ('.$nuovo_carrello.', "'.($customer->id == 44431 && Tools::getValue('id_cart') != 84366 ? 0 : $da_copiare['id_carrier']).'", "'.$da_copiare['id_lang'].'","'.addslashes($da_copiare['id_address_delivery']).'","'.addslashes($da_copiare['id_address_invoice']).'",1,'.$copy_to.',0,"",0,0,"","'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'","'.(Tools::getIsset('rif_prev') ? Tools::getValue('rif_prev') : addslashes($da_copiare['rif_prev'])).'","'.addslashes($da_copiare['name']).'","'.$personac.'","'.addslashes($da_copiare['premessa']).'","'.addslashes(($da_copiare['esigenze'] != '' ? $da_copiare['esigenze'] : $da_copiare_2['esigenze'])).'", "'.addslashes($da_copiare['risorse']).'", "'.($da_copiare['note'] != '' ? addslashes($da_copiare['note']) : addslashes($da_copiare_2['note'])).'","'.addslashes(($da_copiare['note_private'] != '' ? $da_copiare['note_private'] : $da_copiare_2['note_private'])).'","'.addslashes($da_copiare['preventivo']).'","'.addslashes($da_copiare['consegna']).'","'.($_POST['mantieni_stessi_prezzi'] == 's' ? addslashes($da_copiare['transport']) : addslashes($da_copiare['transport'])).'","'.addslashes($da_copiare['in_carico_a']).'","'.$da_copiare['total_products'].'","'.$da_copiare['attachment'].'",'.$context->employee->id.','.$context->employee->id.',"","'.$telegestione.'",0,1, "'.$da_copiare['prezzi_carrello'].'","'.$da_copiare['rif_ordine'].'","'.$da_copiare['cig'].'","'.$da_copiare['cup'].'","'.$da_copiare['ipa'].'","'.$da_copiare['data_ordine_mepa'].'", "'.addslashes($da_copiare['name']).'", "'.addslashes($da_copiare['competenza_dal']).'", "'.addslashes($da_copiare['competenza_al']).'", "'.addslashes($da_copiare['cadenza']).'", "'.addslashes($da_copiare['scadenza']).'", "'.addslashes($da_copiare['decorrenza']).'", "'.addslashes($da_copiare['esito']).'", "'.addslashes($da_copiare['causa']).'", "'.addslashes($da_copiare['concorrente']).'")
				');
				
				$cart_name = Db::getInstance()->getValue('
					SELECT name 
					FROM '._DB_PREFIX_.'cart 
					WHERE id_cart = '.Tools::getValue('id_cart')
				);

				$cart_template = Db::getInstance()->getValue('
					SELECT template 
					FROM '._DB_PREFIX_.'cart 
					WHERE id_cart = '.Tools::getValue('id_cart')
				);
				
				// Correggere: i carrelli 103055 e 104306 non esistono?
				if(Tools::getValue('id_cart') == 103055 || Tools::getValue('id_cart') == 104306 || (strpos($cart_template, 'zcloud ') == true)  || (strpos($cart_name, 'zcloud ') == true) || strpos($template_cart, 'EASTAR-CLOUD') == true ) 
				{
					$is = Db::getInstance()->getValue('
						SELECT id_cart 
						FROM '._DB_PREFIX_.'cart_ezcloud 
						WHERE id_cart = '.Tools::getValue('id_cart')
					);

					if($is <= 0) {
						Db::getInstance()->executeS('
							INSERT INTO '._DB_PREFIX_.'cart_ezcloud 
							VALUES ('.$nuovo_carrello.', "'.$_POST['ezcloud_periodicita'].'",  "'.$_POST['ezcloud_interni'].'", "'.$_POST['ezcloud_linee'].'", "'.$_POST['ezcloud_canali'].'", "'.(strpos($cart_name, 'usiness ') ? 'business' : ((strpos($cart_template, 'usiness ') ? 'business' : 'basic'))).'", "'.$_POST['ezcloud_altro'].'", "'.$_POST['ezcloud_rinnovo'].'", "'.(isset($_POST['ezcloud_configurazione_iniziale']) ? 1 : 0).'", "'.(isset($_POST['ezcloud_attivazione']) ? 1 : 0).'", "'.(isset($_POST['ezcloud_agente']) ? 1 : 0).'", 0, "")
						');
					}
					
					if((strpos($cart_name, 'zcloud ') == true) || strpos($template_cart, 'EASTAR-CLOUD') == true )
					{
						Db::getInstance()->executeS('
							UPDATE '._DB_PREFIX_.'cart_ezcloud
							SET tipo = yeastar_cloud_5
							WHERE id_cart = '.Tools::getValue('id_cart')
						);
					}
					
					Db::getInstance()->executeS('
						UPDATE '._DB_PREFIX_.'cart_product 
						SET free = 1 
						WHERE id_product = 367442 
							AND id_cart = '.Tools::getValue('id_cart')
					);

					Db::getInstance()->executeS('
						UPDATE '._DB_PREFIX_.'cart_product 
						SET free = 1 
						WHERE id_product = 367491 
							AND id_cart = '.Tools::getValue('id_cart')
					);
				}
				
				// se rif_prev è un numero
				if(Tools::getIsset('rif_prev') && is_numeric(Tools::getValue('rif_prev'))) 
				{
					//Db::getInstance()->execute("UPDATE form_prevendita_thread SET status='closed' WHERE id_thread = ".Tools::getValue('rif_prev')."");
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart SET esigenze = '".addslashes(Db::getInstance()->getValue('SELECT message FROM form_prevendita_message WHERE id_thread='.Tools::getValue('rif_prev').' ORDER BY id_message ASC'))."' WHERE id_cart = '".$nuovo_carrello."'");
					Db::getInstance()->execute("UPDATE carrelli_creati SET esigenze = '".Db::getInstance()->getValue('SELECT message FROM form_prevendita_message WHERE id_thread='.Tools::getValue('rif_prev').' ORDER BY id_message DESC')."' WHERE id_cart = '".$nuovo_carrello."'");
				}
				// se rif_prev è una stringa
				else if(Tools::getIsset('rif_prev'))
				{
					/*if(substr(Tools::getValue('rif_prev'),0,1) == 'A')
						Db::getInstance()->execute("UPDATE action_thread SET status='closed' WHERE id_action = ".substr(Tools::getValue('rif_prev'),0,-1)."");
					else
						Db::getInstance()->execute("UPDATE customer_thread SET status='closed' WHERE id_customer_thread = ".substr(Tools::getValue('rif_prev'),0,-1)."");
					*/
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart SET esigenze = '".(strpos(Tools::getValue('rif_prev'), 'T') !== false ? addslashes(Db::getInstance()->getValue('SELECT message FROM '._DB_PREFIX_.'customer_message WHERE id_customer_thread='.substr(Tools::getValue('rif_prev'),0,-1).' ORDER BY id_customer_message ASC')) : addslashes(Db::getInstance()->getValue('SELECT action_message FROM action_message WHERE id_action='.substr(Tools::getValue('rif_prev'),0,-1).' ORDER BY id_action_message ASC')))."' WHERE id_cart = '".$nuovo_carrello."'");
					Db::getInstance()->execute("UPDATE carrelli_creati SET esigenze ='".(strpos(Tools::getValue('rif_prev'), 'T') !== false ? addslashes(Db::getInstance()->getValue('SELECT message FROM '._DB_PREFIX_.'customer_message WHERE id_customer_thread='.substr(Tools::getValue('rif_prev'),0,-1).' ORDER BY id_customer_message ASC')) : addslashes(Db::getInstance()->getValue('SELECT action_message FROM action_message WHERE id_action='.substr(Tools::getValue('rif_prev'),0,-1).' ORDER BY id_action_message ASC')))."' WHERE id_cart = '".$nuovo_carrello."'");
				}

				$prodotti_da_copiare = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'cart_product WHERE id_cart = '.trim(Tools::getValue('id_cart')).'');
			
				foreach($prodotti_da_copiare as $prodotto_da_copiare) {
					
					if($_POST['mantieni_stessi_prezzi'] == 's')
					{
						$prezzi_carrello = Db::getInstance()->getValue("SELECT prezzi_carrello FROM "._DB_PREFIX_."cart".(Tools::getIsset('vedirevisione') ? "_revisioni" : '')." WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
						
						if($prodotto_da_copiare['price'] == 0)
						{
							$id_order = Order::getOrderByCartId($cart->id);
							
							if($id_order > 0)
							{
								$prodotto_da_copiare['price'] = Db::getInstance()->getValue('SELECT product_price - ((product_price * reduction_percent) / 100) FROM '._DB_PREFIX_.'order_detail WHERE product_id = '.$prodotto_da_copiare['id_product'].' AND id_order = '.$id_order);
							}
							else
								$prodotto_da_copiare['price'] = Product::trovaMigliorPrezzo($prodotto_da_copiare['id_product'],$customer->id_default_group,999999);
							
						}
						
						$mrg_att = (((($prodotto_da_copiare['price'] - $prodotto_da_copiare['prezzo_acquisto']) * 100) / $prodotto_da_copiare['price']));
						
						if($prezzi_carrello == 3)
						{
							$prodotto_da_copiare['price'] = Product::trovaMigliorPrezzo($prodotto_da_copiare['id_product'],3,999999);
						}
						else if($prezzi_carrello == 15)
						{
							$prodotto_da_copiare['price'] = Product::trovaMigliorPrezzo($prodotto_da_copiare['id_product'],3,999999);
							$prodotto_da_copiare['price'] = $prodotto_da_copiare['price']+(($prodotto_da_copiare['price']/100)*3);
						}
						
						
						$acq_agg = Product::trovaMigliorPrezzoAcquisto($prodotto_da_copiare['id_product'], $cstp->id_default_group, $prodotto_da_copiare['quantity'], 'y');
						
						$vecchio_prezzo = 0-(($acq_agg*100)/($mrg_att-100));
						
						if($prodotto_da_copiare['price'] == 0)
							$vecchio_prezzo = 0;
						
						if($mrg_att <= 0)
						{
							$vecchio_prezzo = $acq_agg;
						}
						
						$unitario = Product::trovaMigliorPrezzo($prodotto_da_copiare['id_product'], $cstp->id_default_group, $prodotto_da_copiare['quantity']);
						
						
						/*if($unitario != $prodotto_da_copiare['price'])
						{
							$vecchio_prezzo = $prodotto_da_copiare['price'];
							$sconto_extra_v = (($unitario - $prodotto_da_copiare['price'])*100)/$unitario;
						}	
						else
						{	
							$vecchio_prezzo = $prodotto_da_copiare['price'];
							$sconto_extra_v = 0;
						}*/
						
						$sconto_extra_v = (($unitario - $vecchio_prezzo)*100)/$unitario;
						
					}	
					
					//$vecchio_prezzo =  $prodotto_da_copiare['price'];
					
					if($prodotto_da_copiare['id_product'] == 336063 || $prodotto_da_copiare['id_product'] == 336064)
						$vecchio_prezzo = $telegest_value;
				
					if($prodotto_da_copiare['id_product'] != 0)
					{
						Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'cart_product (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, prezzo_acquisto, no_acq, sconto_extra, sort_order, bundle, section, date_add) VALUES ('.$nuovo_carrello.', "'.$prodotto_da_copiare['id_product'].'", "'.$prodotto_da_copiare['id_product_attribute'].'", "'.$prodotto_da_copiare['quantity'].'", "'.($_POST['mantieni_stessi_prezzi'] == 's' ? $vecchio_prezzo : ($prodotto_da_copiare['id_product'] != 336063 && $prodotto_da_copiare['id_product'] != 336064 ? 0 : $vecchio_prezzo )).'","'.($customer->id == 44431 ? 0 : $prodotto_da_copiare['free']).'","'.addslashes(($customer->id == 44431 ? Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'product_lang WHERE id_lang = '.$context->language->id.' AND id_product = '.$prodotto_da_copiare['id_product']) : $prodotto_da_copiare['name'])).'","'.($customer->id == 44431 ? 1 : $prodotto_da_copiare['sc_qta']).'","'.($_POST['mantieni_stessi_prezzi'] == 's' ? ($customer->id == 44431 ?  0  :  0 ) : 0).'","'.$prodotto_da_copiare['no_acq'].'","'.($_POST['mantieni_stessi_prezzi'] == 's' && $customer->id != 44431 ? $sconto_extra_v : 0).'","'.$prodotto_da_copiare['sort_order'].'","'.$prodotto_da_copiare['bundle'].'","'.$prodotto_da_copiare['section'].'","'.date('Y-m-d H:i:s').'")');   
						Db::getInstance()->execute('INSERT INTO carrelli_creati_prodotti (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, prezzo_acquisto, no_acq, sconto_extra, sort_order, bundle,section, date_add) VALUES ('.$nuovo_carrello.', "'.$prodotto_da_copiare['id_product'].'", "'.$prodotto_da_copiare['id_product_attribute'].'", "'.$prodotto_da_copiare['quantity'].'", "'.($_POST['mantieni_stessi_prezzi'] == 's' ? $vecchio_prezzo : ($prodotto_da_copiare['id_product'] != 336063 && $prodotto_da_copiare['id_product'] != 336064 ? 0 : $vecchio_prezzo)).'","'.($customer->id == 44431 ? 0 : $prodotto_da_copiare['free']).'","'.addslashes(($customer->id == 44431 ? Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'product_lang WHERE id_lang = '.$context->language->id.' AND id_product = '.$prodotto_da_copiare['id_product']) : $prodotto_da_copiare['name'])).'","'.($customer->id == 44431 ? 1 : $prodotto_da_copiare['sc_qta']).'","'.($_POST['mantieni_stessi_prezzi'] == 's' ? ($customer->id == 44431 ?  0  :  0 ) : 0).'","'.$prodotto_da_copiare['no_acq'].'","'.($_POST['mantieni_stessi_prezzi'] == 's' && $customer->id != 44431 ? $sconto_extra_v : 0).'","'.$prodotto_da_copiare['sort_order'].'","'.$prodotto_da_copiare['bundle'].'","'.$prodotto_da_copiare['section'].'","'.date('Y-m-d H:i:s').'")');
					}
				}
				
				//controllo per coupon cheapnet
				/*
				if($customer->id == 44431)
				{
					foreach($prodotti_da_copiare as $prodotto_da_copiare) {
						$prodotti_con_coupon = Db::getInstance()->getValue("SELECT value FROM "._DB_PREFIX_."configuration WHERE name = 'PS_CHEAPNET_COUPONS'");
						
						$prodotti = explode("-", $prodotti_con_coupon);

						$prodotti_da_6 = explode(";",$prodotti[0]);
						$prodotti_da_30 = explode(";",$prodotti[1]);
						
						if(in_array($prodotto_da_copiare['id_product'],$prodotti_da_30))
						{
							Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'cart_product (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, prezzo_acquisto, no_acq, sconto_extra, sort_order, bundle, date_add) VALUES ('.$nuovo_carrello.', "31109", "0", "0", "0","0","","0","0","0","0","","0","'.date('Y-m-d H:i:s').'")');   
							
							Db::getInstance()->execute('INSERT INTO carrelli_creati_prodotti (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, prezzo_acquisto, no_acq, sconto_extra, sort_order, bundle, date_add) VALUES ('.$nuovo_carrello.', "31109", "0", "0", "0","0","","0","0","0","0","","0","'.date('Y-m-d H:i:s').'")');   
							break;
						}
						else if(in_array($prodotto_da_copiare['id_product'],$prodotti_da_6))
						{
							Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'cart_product (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, prezzo_acquisto, no_acq, sconto_extra, sort_order, bundle, date_add) VALUES ('.$nuovo_carrello.', "31110", "0", "0", "0","0","","0","0","0","0","","0","'.date('Y-m-d H:i:s').'")');   
							
							Db::getInstance()->execute('INSERT INTO carrelli_creati_prodotti (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, prezzo_acquisto, no_acq, sconto_extra, sort_order, bundle, date_add) VALUES ('.$nuovo_carrello.', "31110", "0", "0", "0","0","","0","0","0","0","","0","'.date('Y-m-d H:i:s').'")');   
							break;
						}
						
					}	
				}
				*/
				//fine controllo per coupon cheapnet
			
				Tools::redirectAdmin(self::$currentIndex.'&id_cart='.$nuovo_carrello.'&viewcart&id_customer='.$copy_to.'&preventivo='.Tools::getValue('preventivo').'&conf=4&token='.$this->token.(Tools::getIsset('notifica_mail') ? '&notifica_inviata=y' : ''));
			}
			
			if(Tools::getIsset('sposta_carrello_conferma')) {
							
				$copy_to = trim(Tools::getValue('copy_to'));
				
				Customer::Storico(Tools::getValue('id_cart'), 'C', $context->employee->id, 'Ha spostato questo carrello (in origine su cliente <a href=\'index.php?controller=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.$tokenCustomers.'\'>'.($customer->is_company == 1 ? $customer->company : $customer->firstname.' '.$customer->lastname).'</a>');
				
				if(Tools::getValue('id_cart') <= 0 || !is_numeric(Tools::getValue('id_cart'))) 
					Tools::redirectAdmin(self::$currentIndex."&viewcart&createnew&provvisorio=1&preventivo=1&id_customer=".$copy_to."&token=".$this->token."#modifica-carrello");
				
				Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'cart SET notifiche = "", numero_notifiche = 0, id_customer = "'.$copy_to.'", id_address_delivery = 0, id_address_invoice = 0 WHERE id_cart = '.Tools::getValue('id_cart').'');
				Db::getInstance()->execute('UPDATE carrelli_creati SET notifiche = "", numero_notifiche = 0, id_customer = "'.$copy_to.'", id_address_delivery = 0, id_address_invoice = 0 WHERE id_cart = '.Tools::getValue('id_cart').'');

				Tools::redirectAdmin(self::$currentIndex.'&id_cart='.Tools::getValue('id_cart').'&viewcart&id_customer='.$copy_to.'&preventivo='.Tools::getValue('preventivo').'&conf=4&token='.$this->token.(Tools::getIsset('notifica_mail') ? '&notifica_inviata=y' : '') );
			}
				
					$html .= '
					</div>
					<div id="cart-3" class="yetii-cart" style="display:block">
					
					';
					
					$html .= '
					<link rel="stylesheet" href="../css/jquery.treeview.css" type="text/css" />


					<script type="text/javascript" src="../js/jquery.treeview.js"></script>

					<script type="text/javascript">

					// build treeview after web page has been loaded

					$(document).ready(function(){

					$(".tree-menu").treeview();
					
					$(".tasti-apertura").mouseover(function() {
						$(this).children(".menu-apertura").show();
					}).mouseout(function() {
						$(this).children(".menu-apertura").hide();
					});


					});

					</script>
					';
						
						$first = Customer::HierarchyFirst(Tools::getValue('id_cart'), 'C');
					
						$html .= '<div class="tree">';
						$html .= '<ul class="tree-menu"><li>'.Customer::trovaSiglaLinkPerAlbero(substr($first,1), substr($first,0,1), '').'';
						$html .= str_replace('<ul></ul>','',Customer::HierarchyFindChildren($first, $children, 'C'.Tools::getValue('id_cart')));
						$html .= '</li></ul>';
						$html .= '</div>';
						$html .= '<div style="clear:both"></div><br /><br />
					
					</div>
					';
					// chiudo cart 3
					
					// apro cart 4
					
					if($context->employee->id_profile == 7){
						// soluzione provvisoria: non vedo storico;
						// -> ricordarsi di nascondere solo margine e acquisto!
					}
					else{
						$html .= '<div id="cart-4" class="yetii-cart" style="display:block">';
						
						$storico_cart = Db::getInstance()->executeS('SELECT * FROM storico_attivita WHERE id_attivita = '.Tools::getValue('id_cart').' AND tipo_attivita = "C" ORDER BY data_attivita DESC, desc_attivita DESC');
						
						if(sizeof($storico_cart) > 0)
						{
							$html .= '<table class="table">';
							$html .= '<thead><tr><th>Persona</th><th>Azione</th><th>Data</th></tr></thead><tbody>';
							
							foreach($storico_cart as $storico)
							{
								if(is_numeric(substr($storico['desc_attivita'], -1)))
								{	
									$employee = filter_var($storico['desc_attivita'], FILTER_SANITIZE_NUMBER_INT);
									$name_employee = Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$employee);
									$desc_attivita = 'Ha messo l\'attivit&agrave; in carico a '.$name_employee;
								}
								else
									$desc_attivita = $storico['desc_attivita'];
								
								$html .= '<tr><td '.($storico['id_employee'] == 0 ? 'style="color:red; font-weight:bold"' : '').'>'.($storico['id_employee'] == 0 ? 'Cliente' : Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$storico['id_employee'])).'</td><td '.($storico['id_employee'] == 0 ? 'style="color:red; font-weight:bold"' : '').'>'.$desc_attivita.'</td><td '.($storico['id_employee'] == 0 ? 'style="color:red; font-weight:bold"' : '').'>'.Tools::displayDate($storico['data_attivita'],$context->language->id, true).'</td></tr>'; 
								
							}	
							
							$html .= '</tbody></table><br /><br />';
						}
						
						
						
						$html .= '</div>'; // chiudo cart 4
					}
					
					$html .= '</fieldset> ';
					
					$html .= '	<script type="text/javascript">

					$(\'.button_invio\').live(\'click\', function(e) {
						$(\'#waiting1\').waiting({ 
						elements: 10, 
						auto: true 
						});
						var overlay = jQuery(\'<div id="overlay"></div>\');
						var attendere = jQuery(\'<div id="attendere">ATTENDERE PREGO</div>\');
						overlay.appendTo(document.body);
						attendere.appendTo(document.body);
					});
						
				</script>';
		
			
			
			if(isset($_GET['cartupdated'])) {
				
				$id_cart_g = htmlspecialchars(Tools::getValue('id_cart'));
				$action_g = ""; 
				$id_product_g = htmlspecialchars($_POST['id_product']);
				$id_lang_g = 5;

				if ($id_order_g) 
				$_POST['id_order']=$id_order_g;

				if ($_POST['id_cart']) {
					/*
						$query="UPDATE "._DB_PREFIX_."orders SET ";
						$query.=" total_discounts=".$this->price($_POST['total_discounts']);
						$query.=" ,total_shipping=".$this->price($_POST['total_shipping']);
						$query.=" WHERE id_order=".$_POST['id_order'];
						$query.=" limit 1";
						//$html .= $query;
						Db::getInstance()->executeS($query) or die(mysql_error());
					*/

					if (Tools::getIsset('Apply')) 
					{
						if(!Tools::getIsset('ajaxsave')) {
							/*Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."cart_product WHERE bundle = 88888877 AND id_cart = ".$_POST['id_cart']."");
							Db::getInstance()->execute("DELETE FROM carrelli_creati_prodotti WHERE bundle = 88888877 AND id_cart = ".$_POST['id_cart']."");
							Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."cart_product WHERE bundle = 88888878 AND id_cart = ".$_POST['id_cart']."");
							Db::getInstance()->execute("DELETE FROM carrelli_creati_prodotti WHERE bundle = 88888878 AND id_cart = ".$_POST['id_cart']."");
							*/
						}

						$creato_da = Db::getInstance()->getValue("
							SELECT created_by 
							FROM "._DB_PREFIX_."cart 
							WHERE id_cart = ".$_POST['id_cart']."
						");

						$impiegato_ultima_modifica = Db::getInstance()->getValue("
							SELECT id_employee 
							FROM "._DB_PREFIX_."cart 
							WHERE id_cart = ".$_POST['id_cart']."
						");
				
						if($creato_da == 0 && $impiegato_ultima_modifica == 0) { 
						
							Customer::Storico(Tools::getValue('id_cart'), 'C', $context->employee->id, 23);
						
							$da_copiare = Db::getInstance()->getRow('
								SELECT * 
								FROM '._DB_PREFIX_.'cart 
								WHERE id_cart = '.$_POST['id_cart'].'
							');
			
							Db::getInstance()->execute('
								INSERT INTO carrelli_creati (id_cart, id_carrier, id_lang, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, date_add, date_upd, rif_prev, name, premessa, riferimento, risorse, esigenze, note, note_private, preventivo, validita, consegna, in_carico_a, transport, attachment, created_by, id_employee, numero_notifiche, rif_ordine, cig, cup, ipa, data_ordine_mepa, competenza_dal, competenza_al, cadenza, scadenza, decorrenza, esito, causa, concorrente) 
								VALUES ('.$_POST['id_cart'].', "'.$da_copiare['id_carrier'].'", "'.$da_copiare['id_lang'].'","'.addslashes($da_copiare['id_address_delivery']).'","'.addslashes($da_copiare['id_address_invoice']).'",1,"'.$da_copiare['id_customer'].'",0,"",0,0,"","'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'", "'.addslashes($da_copiare['rif_prev']).'", "'.addslashes($da_copiare['name']).'", "'.addslashes($da_copiare['premessa']).'", "'.addslashes($da_copiare['riferimento']).'", "'.addslashes($da_copiare['risorse']).'","'.addslashes($da_copiare['esigenze']).'","'.$da_copiare['note'].'","'.$da_copiare['note_private'].'","'.addslashes($da_copiare['preventivo']).'","'.$da_copiare['validita'].'","'.addslashes($da_copiare['consegna']).'","'.addslashes($da_copiare['in_carico_a']).'","'.addslashes($da_copiare['transport']).'","'.$da_copiare['attachment'].'","'.$context->employee->id.'","'.$context->employee->id.'",0,"'.$da_copiare['rif_ordine'].'","'.$da_copiare['cig'].'","'.$da_copiare['cup'].'","'.$da_copiare['ipa'].'","'.$da_copiare['data_ordine_mepa'].'","'.$da_copiare['competenza_dal'].'","'.$da_copiare['competenza_al'].'","'.$da_copiare['cadenza'].'","'.$da_copiare['scadenza'].'","'.$da_copiare['decorrenza'].'", "'.addslashes($da_copiare['esito']).'", "'.addslashes($da_copiare['causa']).'", "'.addslashes($da_copiare['concorrente']).'")
							');  

							$prodotti_da_copiare = Db::getInstance()->executeS('
								SELECT * 
								FROM '._DB_PREFIX_.'cart_product 
								WHERE id_cart = '.trim($_POST['id_cart']).'
							');
			
							foreach($prodotti_da_copiare as $prodotto_da_copiare) {
				
								if($prodotto_da_copiare['id_product'] != 0)
									Db::getInstance()->execute('
										INSERT INTO carrelli_creati_prodotti (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, sconto_extra, prezzo_acquisto, no_acq, sort_order, bundle, date_add) 
										VALUES ('.$_POST['id_cart'].', "'.$prodotto_da_copiare['id_product'].'", "'.$prodotto_da_copiare['id_product_attribute'].'", "'.$prodotto_da_copiare['quantity'].'", "'.$prodotto_da_copiare['price'].'","'.$prodotto_da_copiare['free'].'","'.addslashes($prodotto_da_copiare['name']).'","'.$prodotto_da_copiare['sc_qta'].'","'.$prodotto_da_copiare['sconto_extra'].'","'.$prodotto_da_copiare['wholesale_price'].'","'.$prodotto_da_copiare['no_acq'].'","'.$prodotto_da_copiare['sort_order'].'","'.$prodotto_da_copiare['bundle'].'","'.date('Y-m-d H:i:s').'")
									');					
							}
						}
						
						if(Tools::getIsset('chiudi_padre')) {
							$rif_prev = Db::getInstance()->getValue('
								SELECT rif_prev 
								FROM '._DB_PREFIX_.'cart 
								WHERE id_cart = '.trim($_POST['id_cart']).'
							');
							
							if($rif_prev != '' && $rif_prev != 0) {
								if(is_numeric($rif_prev) && $rif_prev != 0) {
									Db::getInstance()->execute("
										UPDATE form_prevendita_thread 
										SET status = 'closed' 
										WHERE id_thread = ".$rif_prev."
									");
								}
								else {
									$first = substr($rif_prev,0,1);
									$thread = substr($rif_prev,1,99);
									
									if($first == 'T') {
										Db::getInstance()->execute("
											UPDATE "._DB_PREFIX_."customer_thread 
											SET status = 'closed' 
											WHERE id_customer_thread = ".$thread."
										");
									}
									else if($first == 'A') {
										Db::getInstance()->execute("
											UPDATE action_thread 
											SET status = 'closed' 
											WHERE id_action = ".$thread."
										");
									}
									else if($first == 'P') {
										Db::getInstance()->execute("
											UPDATE form_prevendita_thread 
											SET status = 'closed' 
											WHERE id_thread = ".$thread."
										");
									}
								}

								$html .= "<br /><br />";
							}
						}

						// delete product
						if (Tools::getValue('product_delete')) {
							Customer::Storico(Tools::getValue('id_cart'), 'C', $context->employee->id, 24);
							
							foreach ($_POST['product_delete'] as $id_product=>$value) {
								Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."cart_product WHERE id_cart = ".$_POST['id_cart']." AND id_product=".$id_product);
								Db::getInstance()->execute("DELETE FROM carrelli_creati_prodotti WHERE id_cart = ".$_POST['id_cart']." AND id_product=".$id_product);
							}
						}
						
						if($_POST['altropagamento'] != '') {
							$pagamento = $_POST['altropagamento'];
						}
						else {
							$pagamento = $_POST['payment'];
						}
						
						if($_POST['validita'] == '')
							$_POST['validita'] = date('d-m-Y', strtotime($Date.' + 15 days'));
						
						$validita_corretto = str_replace("/","-",$_POST['validita']);
						
						$validita_corretto = date('Y-m-d 23:59:59', strtotime($validita_corretto));
						/*if(date("Y-m-d H:i:s", strtotime($validita_corretto)) < date("Y-m-d H:i:s"))
							$validita_corretto = date('Y-m-d 23:59:59', strtotime($Date. ' + 15 days'));
						*/
						
						if($_POST['ezcloud_template'] == 1) {
							
							foreach($_POST['ezcloud_tipo'] as $ezcltipo) {
								if($ezcltipo == 'basic')
									$ezcloud_tipo = 'basic';
								else if($ezcltipo == 'business')
									$ezcloud_tipo = 'business';
								else if($ezcltipo == 'yeastar_cloud_5')
									$ezcloud_tipo = 'yeastar_cloud_5';
								else
									$ezcloud_tipo = 'altro';
							}
							
							if($ezcloud_tipo == 'business') {
								$canone_mensile = $_POST['product_price']['367491'] * $_POST['product_quantity']['367491'];
								if(!$_POST['product_price']['367491'] || empty($_POST['product_price']['367491']))
									$canone_mensile = $_POST['nuovo_price']['367491'] * $_POST['nuovo_quantity']['367491'];
							}
							else if($ezcloud_tipo == 'basic') {
								$canone_mensile = $_POST['product_price']['367442'] * $_POST['product_quantity']['367442'];
								if(!$_POST['product_price']['367442'] || empty($_POST['product_price']['367442']))
									$canone_mensile = $_POST['nuovo_price']['367442'] * $_POST['nuovo_quantity']['367442'];
							}
							else if($ezcloud_tipo == 'yeastar_cloud_5') {
								$canone_mensile = $_POST['product_price']['387752'] * $_POST['product_quantity']['387752'];
								if(!$_POST['product_price']['387752'] || empty($_POST['product_price']['387752']))
									$canone_mensile = $_POST['nuovo_price']['387752'] * $_POST['nuovo_quantity']['387752'];
							}
							
							$is = Db::getInstance()->getValue('
								SELECT id_cart 
								FROM '._DB_PREFIX_.'cart_ezcloud 
								WHERE id_cart = '.$_POST['id_cart']
							);
							if($is > 0) {
								Db::getInstance()->executeS('
									UPDATE '._DB_PREFIX_.'cart_ezcloud 
									SET periodicita = "'.$_POST['ezcloud_periodicita'].'", interni = "'.$_POST['ezcloud_interni'].'", linee = "'.$_POST['ezcloud_linee'].'", canali = "'.$_POST['ezcloud_canali'].'",  canone = "'.$_POST['ezcloud_canone'].'", tipo = "'.$ezcloud_tipo.'", tipo_altro = "'.$_POST['ezcloud_altro'].'", data_fatturazione_ezcloud = "'.date('Y-m-d', strtotime($_POST['data_fatturazione_ezcloud'])).'", rinnovo_automatico = "'.$_POST['ezcloud_rinnovo'].'", configurazione_iniziale = "'.(isset($_POST['ezcloud_configurazione_iniziale']) ? 1 : 0).'", attivazione = "'.(isset($_POST['ezcloud_attivazione']) ? 1 : 0).'", canone_mensile = "'.$canone_mensile.'", agente = "'.(isset($_POST['ezcloud_agente']) ? 1 : 0).'", allegato_a = "'.(isset($_POST['allegato_a']) ? 1 : 0).'" 
									WHERE id_cart = '.$_POST['id_cart']
								);
							}
							else {
								Db::getInstance()->executeS('
									INSERT INTO '._DB_PREFIX_.'cart_ezcloud (id_cart, periodicita, interni, linee, canali, canone, tipo, tipo_altro, rinnovo_automatico, configurazione_iniziale, attivazione, agente, data_fatturazione_ezcloud, canone_mensile) 
									VALUES ('.$_POST['id_cart'].', "'.$_POST['ezcloud_periodicita'].'",  "'.$_POST['ezcloud_interni'].'", "'.$_POST['ezcloud_linee'].'", "'.$_POST['ezcloud_canali'].'", "'.$_POST['ezcloud_canone'].'", "'.$ezcloud_tipo.'", "'.$_POST['ezcloud_altro'].'", "'.$_POST['ezcloud_rinnovo'].'", "'.(isset($_POST['ezcloud_configurazione_iniziale']) ? 1 : 0).'", "'.(isset($_POST['ezcloud_attivazione']) ? 1 : 0).'", "'.(isset($_POST['ezcloud_agente']) ? 1 : 0).'", "'.date('Y-m-d', strtotime($_POST['data_fatturazione_ezcloud'])).'", "'.$canone_mensile.'")
								');
							}
						}

						if($_POST['telegestione'] == 'yes') {
							$telegestione = array();
							foreach($_POST['prodotto_telegestione'] as $key => $value) {
								$categoria_prodotto_telegest = Db::getInstance()->getValue('SELECT id_category_default FROM '._DB_PREFIX_.'product WHERE id_product = '.$_POST['prodotto_telegestione'][$value]);
								
								if($categoria_prodotto_telegest != 119) {
									$html .= $value.'<br />';
									$telegestione_prod = array();
									$telegestione_prod['id_product'] = $_POST['prodotto_telegestione'][$value];
									$telegestione_prod['price'] = $_POST['price_telegestione'][$value];
									$telegestione_prod['no_acq'] = $_POST['no_acq_telegestione'][$value];
									$telegestione_prod['reduction_percent'] = $_POST['reduction_percent_telegestione'][$value];
									$telegestione_prod['wholesale_price'] = $_POST['wholesale_price_telegestione'][$value];
									$telegestione_prod['quantity'] = $_POST['quantity_telegestione'][$value];
									$telegestione[] = $telegestione_prod;
								}
							}
							
							$telegestione = serialize($telegestione);
							$telegestione = addslashes($telegestione);
						}
						else
							$telegestione = '';
						
						$dom = explode('-',$_POST['data_ordine_mepa']);
						$dom2 = explode('-',$_POST['scadenza']);
						$dom3 = explode('-',$_POST['decorrenza']);
						$dom4 = explode('-',$_POST['competenza_dal']);
						$dom5 = explode('-',$_POST['competenza_al']);					
						
						Db::getInstance()->execute("
							UPDATE "._DB_PREFIX_."cart 
							SET date_upd = '".date("Y-m-d H:i:s")."', id_employee = ".$context->employee->id.", premessa='".addslashes($_POST['premessa'])."', esigenze='".addslashes($_POST['esigenze'])."',risorse='".addslashes($_POST['risorse'])."',note='".preg_replace('/(<[^>]+) style=".*?"/i', '',addslashes($_POST['note']))."', note_private='".addslashes($_POST['note_private'])."', preventivo='".$_POST['preventivo']."', id_address_invoice='".$_POST['id_address_invoice']."', id_address_delivery='".$_POST['id_address_delivery']."', id_carrier='".$_POST['metodo']."', rif_prev='".$_POST['rif_prev']."', name='".addslashes($_POST['name'])."', riferimento='".$_POST['riferimento']."', validita='".date("Y-m-d H:i:s", strtotime($validita_corretto))."',in_carico_a='".$_POST['in_carico_a']."',".'payment="'.$pagamento.'"'."".", telegestione='".$telegestione."', consegna='".$_POST['consegna']."', rif_ordine='".$_POST['rif_ordine']."', cig='".$_POST['cig']."', cup='".$_POST['cup']."', ipa='".$_POST['ipa']."' , data_ordine_mepa='".$dom[2].'-'.$dom[1].'-'.$dom[0]."', scadenza='".$dom2[2].'-'.$dom2[1].'-'.$dom2[0]."', decorrenza='".$dom3[2].'-'.$dom3[1].'-'.$dom3[0]."' , competenza_dal='".$dom4[2].'-'.$dom4[1].'-'.$dom4[0]."', competenza_al='".$dom5[2].'-'.$dom5[1].'-'.$dom5[0]."', cadenza='".$_POST['cadenza']."', esito='".$_POST['esito']."', causa='".$_POST['causa']."', concorrente='".$_POST['concorrente']."' 
							WHERE id_cart = ".$_POST['id_cart']
						);
						
						Db::getInstance()->execute("
							UPDATE carrelli_creati 
							SET date_upd = '".date("Y-m-d H:i:s")."', id_employee = ".$context->employee->id.", premessa='".addslashes($_POST['premessa'])."', esigenze='".addslashes($_POST['esigenze'])."', risorse='".addslashes($_POST['risorse'])."', note='".preg_replace('/(<[^>]+) style=".*?"/i', '', addslashes($_POST['note']))."', note_private='".addslashes($_POST['note_private'])."', preventivo='".$_POST['preventivo']."', id_address_invoice='".$_POST['id_address_invoice']."', id_address_delivery='".$_POST['id_address_delivery']."', id_carrier='".$_POST['metodo']."', rif_prev='".$_POST['rif_prev']."', name='".addslashes($_POST['name'])."', riferimento='".$_POST['riferimento']."', validita='".date("Y-m-d H:i:s", strtotime($validita_corretto))."',in_carico_a='".$_POST['in_carico_a']."',".'payment="'.$pagamento.'"'."".",telegestione='".$telegestione."',  consegna='".$_POST['consegna']."', rif_ordine='".$_POST['rif_ordine']."', cig='".$_POST['cig']."', cup='".$_POST['cup']."', ipa='".$_POST['ipa']."', data_ordine_mepa='".$dom[2].'-'.$dom[1].'-'.$dom[0]."', scadenza='".$dom2[2].'-'.$dom2[1].'-'.$dom2[0]."', decorrenza='".$dom3[2].'-'.$dom3[1].'-'.$dom3[0]."' , competenza_dal='".$dom4[2].'-'.$dom4[1].'-'.$dom4[0]."', competenza_al='".$dom5[2].'-'.$dom5[1].'-'.$dom5[0]."', cadenza='".$_POST['cadenza']."', esito='".$_POST['esito']."', causa='".$_POST['causa']."', concorrente='".$_POST['concorrente']."' 
							WHERE id_cart = ".$_POST['id_cart']
						);
						
						foreach ($_POST['note_nuova'] as $id_nota=>$nota_id) {
							if($_POST['note_nuova'][$id_nota] == 0) {
								$testo_nota = Db::getInstance()->getValue('SELECT note FROM cart_note WHERE id_note = '.$id_nota.'');
								if($testo_nota != $_POST['note_nota'][$id_nota])
									Db::getInstance()->execute("UPDATE cart_note SET note = '".addslashes($_POST['note_nota'][$id_nota])."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_note = ".$id_nota.""); 
							}
							else {
								if(strip_tags($_POST['note_nota'][$id_nota]) != '') {
									Db::getInstance()->execute("
										INSERT INTO cart_note
											(id_note,
											id_cart,
											id_employee,
											note,
											date_add,
											date_upd)
										VALUES (
											NULL,
											'".Tools::getValue('id_cart')."',
											'".$context->employee->id."',
											'".addslashes($_POST['note_nota'][$id_nota])."',
											'".date("Y-m-d H:i:s")."',
											'".date("Y-m-d H:i:s")."'
									)");
								}
							}
						}
						
						if($_POST['mesi_noleggio'] > 0) 
						{
							$noleggio = Db::getInstance()->getValue("
								SELECT id_cart 
								FROM noleggio 
								WHERE id_cart = ".$_POST['id_cart']."
							");

							if($noleggio > 0 && $_POST['parametro_noleggio'] > 0) {
								Db::getInstance()->execute("
									UPDATE noleggio 
									SET total = ".$_POST['totaleprodotti'].", mesi = ".$_POST['mesi_noleggio'].", spese_contratto = ".$_POST['spese_contratto_noleggio'].", parametri = '".$_POST['parametro_noleggio']."', date_upd = '".date("Y-m-d H:i:s")."' 
									WHERE id_cart = '".$_POST['id_cart']."'
								");
							}
							else if ( $_POST['parametro_noleggio'] > 0 ) {
								Db::getInstance()->execute("
									INSERT INTO noleggio (id_noleggio, id_cart, total, mesi, spese_contratto, parametri, date_add, date_upd) 
									VALUES (NULL, ".$_POST['id_cart'].", ".$_POST['totaleprodotti'].", ".$_POST['mesi_noleggio'].", ".$_POST['spese_contratto_noleggio'].", '".$_POST['parametro_noleggio']."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')
								");
							}
						}
						else {
							Db::getInstance()->execute("
								DELETE 
								FROM noleggio 
								WHERE id_cart = '".$_POST['id_cart']."'
							");
						}

						$update_provvisorio = (Tools::getIsset('provvisorio') ? 1 : 0);

						Db::getInstance()->execute("
							UPDATE "._DB_PREFIX_."cart 
							SET provvisorio = ".$update_provvisorio."
							WHERE id_cart = ".$_POST['id_cart']
						);
						Db::getInstance()->execute("
							UPDATE carrelli_creati 
							SET provvisorio = ".$update_provvisorio."
							WHERE id_cart = ".$_POST['id_cart']
						);
						
						// MODIFICA PER PROVVISORIO OBBLIGATORIO PER TECNICI
						if($context->employee->id != 1 &&  $context->employee->id != 2 && $context->employee->id != 6 && $context->employee->id != 22 && $context->employee->id != 7 && $context->employee->id != 14 && $context->employee->id != 17 && $context->employee->id != 12  && $context->employee->id != 19) {
							
							if($provvisorio == 0) {
								$action_threadz = Db::getInstance()->getValue("
									SELECT id_action 
									FROM action_thread 
									ORDER BY id_action DESC
								");

								$action_threadz++;
								$action_subjectz = "*** TECNICO HA MODIFICATO CARRELLO N. ".$cart->id." - VERIFICARE *** ";
								$id_employee = $in_carico_a;
								
								Db::getInstance()->execute("
									INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd, promemoria) 
									VALUES ('$action_threadz', 'Attivita', '".$customer->id."', '".addslashes($action_subjectz)."', ".$context->employee->id.", ".$id_employee.", 'open', 'C".$cart->id."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."',1)
								");   

								Db::getInstance()->execute("
									INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) 
									VALUES (NULL, '$action_threadz', ".$customer->id.", ".$context->employee->id.", ".$id_employee.", ".$id_employee.", '', '<strong>*** TECNICO HA MODIFICATO CARRELLO ***</strong><br /><br />"
										.Db::getInstance()->getValue('
											SELECT CONCAT(firstname," ",lastname) 
											FROM '._DB_PREFIX_.'employee 
											WHERE id_employee = '.$context->employee->id)." ha modificato il carrello n. ".$cart->id.". Il carrello &egrave; adesso in status provvisorio e il cliente non pu&ograve; vederlo. Verificare e confermare per renderlo nuovamente visibile al cliente.', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."
										')
								");  
							}

							Db::getInstance()->execute("
								UPDATE "._DB_PREFIX_."cart 
								SET provvisorio = 1 
								WHERE id_cart = ".$_POST['id_cart']
							);

							Db::getInstance()->execute("
								UPDATE carrelli_creati 
								SET provvisorio = 1 
								WHERE id_cart = ".$_POST['id_cart']
							);
						}
						
						if(str_replace(",",".",$_POST['transport']) == $costo_spedizione) {
					
							$transport = Db::getInstance()->getValue("
								SELECT transport 
								FROM "._DB_PREFIX_."cart 
								WHERE id_cart = ".$_POST['id_cart']
							);

							if($transport != '') {	
								Db::getInstance()->execute("
									UPDATE "._DB_PREFIX_."cart 
									SET transport = '".$_POST['metodo'].":".str_replace(",",".",$_POST['transport'])."' 
									WHERE id_cart = ".$_POST['id_cart']
								);

								Db::getInstance()->execute("
									UPDATE carrelli_creati 
									SET transport = '".$_POST['metodo'].":".str_replace(",",".",$_POST['transport'])."' 
									WHERE id_cart = ".$_POST['id_cart']
								);
							}
						}
						else {
							Db::getInstance()->execute("
								UPDATE "._DB_PREFIX_."cart 
								SET transport = '".$_POST['metodo'].":".str_replace(",",".",$_POST['transport'])."' 
								WHERE id_cart = ".$_POST['id_cart']
							);

							Db::getInstance()->execute("
								UPDATE carrelli_creati 
								SET transport = '".$_POST['metodo'].":".str_replace(",",".",$_POST['transport'])."' 
								WHERE id_cart = ".$_POST['id_cart']
							);
						}
						
						$table_per_storico = '<table class=\'table\'><thead><tr><th>Codice</th><th>Nome prodotto</th><th>Qta</th><th>Unitario</th><th>Listino</th><th>Sconto extra</th>
						'.($context->employee->id_profile == 7 ? '' : '<th>Acquisto</th><th>Marg</th>').'
						<th>Importo</th></tr></thead><tbody>';
						
						$totale_per_storico = 0;
						
						foreach ($_POST['nuovo_prodotto'] as $id_product_g=>$id_product) {
							//if(isset($_POST['product_delete'][$id_product_g])) {
							
								/*Db::getInstance()->getValue("DELETE FROM "._DB_PREFIX_."cart_product WHERE id_product = ".$_POST['product_delete'][$id_product_g]." AND id_cart = ".$_POST['id_cart']."");
								Db::getInstance()->getValue("DELETE FROM carrelli_creati_prodotti WHERE id_product = ".$_POST['product_delete'][$id_product_g]." AND id_cart = ".$_POST['id_cart']."");*/
							//}
							//else {
								$price = str_replace(',', '.', $_POST['nuovo_price'][trim($id_product_g)]);
								$_POST['nuovo_price'][trim($id_product_g)] = str_replace(',', '.', $_POST['nuovo_price'][trim($id_product_g)]);
								$sconto_extra = str_replace(',', '.', $_POST['sconto_extra'][trim($id_product_g)]);	
								$acquisto = str_replace(',', '.', $_POST['wholesale_price'][trim($id_product_g)]);	

								$nome_prodotto = Db::getInstance()->getValue('
									SELECT name 
									FROM '._DB_PREFIX_.'product_lang 
									WHERE id_lang = '.$context->language->id.' 
										AND id_product = '.$id_product_g
								);

							
								$is_there_product = Db::getInstance()->getValue("
									SELECT count(id_product) 
									FROM "._DB_PREFIX_."cart_product 
									WHERE id_product = ".$_POST['nuovo_prodotto'][$id_product_g]." 
										AND id_cart = ".$_POST['id_cart']."
								");
									
								if($is_there_product > 0) {
		
									$query="UPDATE "._DB_PREFIX_."cart_product SET ";
									$query.="id_product_attribute = '0', quantity = '".$_POST['nuovo_quantity'][trim($id_product_g)]."', price = ";
									$query.=$price.",".(isset($_GET['ajaxsave']) ? '' : ' bundle=0, ')."free = '".($_POST['nuovo_price'][$id_product_g] == 0 ? '1' : '0')."',name = '".$_POST['nuovo_name'][$id_product_g]."', sc_qta = '".($_POST['usa_sconti_quantita'][$id_product_g] == 0 ? '1' : '0')."', prezzo_acquisto = '".$acquisto."', sconto_extra = '".$sconto_extra."' WHERE id_cart = ".$_POST['id_cart']." AND id_product = ".$_POST['nuovo_prodotto'][$id_product_g]."";
										
									$query2="UPDATE carrelli_creati_prodotti SET ";
									$query2.="id_product_attribute = '0', quantity = '".$_POST['nuovo_quantity'][trim($id_product_g)]."', price = ";
									$query2.=$price.",".(isset($_GET['ajaxsave']) ? '' : ' bundle=0, ')." free = '".($_POST['nuovo_price'][$id_product_g] == 0 ? '1' : '0')."',name = '".$_POST['nuovo_name'][$id_product_g]."', sc_qta = '".($_POST['usa_sconti_quantita'][$id_product_g] == 0 ? '1' : '0')."', prezzo_acquisto = '".$acquisto."', sconto_extra = '".$sconto_extra."' WHERE id_cart = ".$_POST['id_cart']." AND id_product = ".$_POST['nuovo_prodotto'][$id_product_g]."";
								}
								else {
									if($_POST['nuovo_prodotto'][$id_product_g] > 0) {
										$query="insert into "._DB_PREFIX_."cart_product (id_cart,id_product,id_product_attribute,quantity,price,free,bundle,name,sc_qta,sconto_extra,prezzo_acquisto,section,date_add) values  ";
										$query.="(".$_POST['id_cart'].",".$_POST['nuovo_prodotto'][$id_product_g].",'0','".$_POST['nuovo_quantity'][trim($id_product_g)]."',";
										$query.=$price.",'".($_POST['nuovo_price'][$id_product_g] == 0 ? '1' : '0')."','".(isset($_GET['ajaxsave']) ? '999999' : '0')."','".$_POST['nuovo_name'][$id_product_g]."','".($_POST['usa_sconti_quantita'][$id_product_g] == 0 ? '1' : '0')."','".$sconto_extra."','".$acquisto."','".$_POST['section'][trim($id_product_g)]."','".date("Y-m-d H:i:s")."')";
											
										$query2="insert into carrelli_creati_prodotti (id_cart,id_product,id_product_attribute,quantity,price,free,bundle,name,sc_qta,sconto_extra,prezzo_acquisto,section,date_add) values  ";
										$query2.="(".$_POST['id_cart'].",".$_POST['nuovo_prodotto'][$id_product_g].",'0','".$_POST['nuovo_quantity'][trim($id_product_g)]."',";
										$query2.=$price.",'".($_POST['nuovo_price'][$id_product_g] == 0 ? '1' : '0')."','".(isset($_GET['ajaxsave']) ? '999999' : '0')."','".$_POST['nuovo_name'][$id_product_g]."','".($_POST['usa_sconti_quantita'][$id_product_g] == 0 ? '1' : '0')."','".$sconto_extra."','".$acquisto."','".$_POST['section'][trim($id_product_g)]."','".date("Y-m-d H:i:s")."')";
									}	
								}
								$unitario_st = Db::getInstance()->getValue("
									SELECT price 
									FROM "._DB_PREFIX_."product 
									WHERE id_product = '".trim($id_product_g)."'
								");
								
								$prezzo_iniziale = ($price*100)/(100-$sconto_extra);
								
								$table_per_storico .= '<tr><td>'.Db::getInstance()->getValue('
									SELECT reference 
									FROM '._DB_PREFIX_.'product 
									WHERE id_product = '.$_POST['nuovo_prodotto'][$id_product_g]
									).'</td><td>'.Db::getInstance()->getValue('
										SELECT name 
										FROM '._DB_PREFIX_.'product_lang 
										WHERE id_lang = '.$context->language->id.' 
											AND id_product = '.$_POST['nuovo_prodotto'][$id_product_g]
										)
									.'</td><td style=\'text-align:right\'>'.$_POST['nuovo_quantity'][trim($id_product_g)].'</td><td style=\'text-align:right\'>'.number_format($price,2,",","").'</td><td style=\'text-align:right\'>'.number_format($prezzo_iniziale,2,",","").'</td><td style=\'text-align:right\'>'.($unitario_st == 0 ? '' : number_format($sconto_extra,2,",","")).'</td>'.($context->employee->id_profile == 7 ? '' : '<td style=\'text-align:right\'>'.number_format($acquisto,2,",","").'</td><td>'.number_format((((($price - $acquisto)*100) / $price)),2,",","").'%</td>').'<td style=\'text-align:right\'>'.number_format(($price*$_POST['nuovo_quantity'][trim($id_product_g)]),2,",","").'</td></tr>';
								
								$totale_per_storico += ($price*$_POST['nuovo_quantity'][trim($id_product_g)]);
						
								$html .= $query;
								
								Db::getInstance()->execute($query);
								Db::getInstance()->execute($query2);
							//}
						}
						
						foreach ($_POST['product_name'] as $id_product=>$product_name) {
							$name = Db::getInstance()->getValue("
								SELECT name 
								FROM "._DB_PREFIX_."product_lang 
								WHERE id_lang = ".$context->language->id." 
									AND id_product = ".$id_product."
							");
								
							if($name != $_POST['product_name'][$id_product]) {
									Db::getInstance()->execute("
										UPDATE "._DB_PREFIX_."cart_product SET name='".addslashes($_POST['product_name'][$id_product])."' 
										WHERE id_cart = '".$_POST['id_cart']."' 
											AND id_product=".$id_product
									);

									Db::getInstance()->executeS("
										UPDATE carrelli_creati_prodotti 
										SET name='".addslashes($_POST['product_name'][$id_product])."' 
										WHERE id_cart = '".$_POST['id_cart']."' 
											AND id_product=".$id_product
									);
								}
						}

						if ($_POST['product_price']) {
						
							foreach ($_POST['product_price'] as $id_product=>$price_product) {

								if(!empty($_POST['usa_sconti_quantita'][$id_product])) 
								{
									$price_product = str_replace(',', '.', $_POST['product_price'][$id_product]);
									Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart_product SET sc_qta=1 WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
								}
								else {
									$price_product = str_replace(',', '.', $_POST['product_price'][$id_product]);
									Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart_product SET sc_qta=0 WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
								}
								
								Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart_product SET ".(isset($_GET['ajaxsave']) ? '' : 'bundle=0,')." price=".$price_product." WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
								Db::getInstance()->execute("UPDATE carrelli_creati_prodotti SET ".(isset($_GET['ajaxsave']) ? '' : 'bundle=0,')."  price=".$price_product." WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
								
								if($price_product == 0) 
								{
									Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart_product SET free=1 WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
									Db::getInstance()->execute("UPDATE carrelli_creati_prodotti SET free=1 WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
								}
								else
								{
									Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart_product SET free=0 WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
									Db::getInstance()->execute("UPDATE carrelli_creati_prodotti SET free=0 WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
								}
								
								Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart_product SET quantity=".$_POST['product_quantity'][$id_product]." WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
								Db::getInstance()->execute("UPDATE carrelli_creati_prodotti SET quantity=".$_POST['product_quantity'][$id_product]." WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
								
								$sconto_extra = str_replace(',', '.', $_POST['sconto_extra'][$id_product]);
								
								Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart_product SET sconto_extra=".$sconto_extra." WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
								Db::getInstance()->execute("UPDATE carrelli_creati_prodotti SET sconto_extra=".$sconto_extra." WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
								
								$acquisto = str_replace(',', '.', $_POST['wholesale_price'][$id_product]);
								
								Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart_product SET prezzo_acquisto=".$acquisto.", no_acq = ".($acquisto == 0 ? 1 : 0)." WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
								Db::getInstance()->execute("UPDATE carrelli_creati_prodotti SET prezzo_acquisto=".$acquisto.", no_acq = ".($acquisto == 0 ? 1 : 0)." WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
								
								$unitario_st = Db::getInstance()->getValue("SELECT price FROM "._DB_PREFIX_."product WHERE id_product = '".$id_product."'");
								
								$prezzo_iniziale = ($price_product*100)/(100-$sconto_extra);
								
								$table_per_storico .= '<tr><td>'.Db::getInstance()->getValue('SELECT reference FROM '._DB_PREFIX_.'product WHERE id_product = '.$id_product).'</td><td>'.Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'product_lang WHERE id_lang = '.$context->language->id.' AND id_product = '.$id_product).'</td><td style=\'text-align:right\'>'.$_POST['product_quantity'][$id_product].'</td><td style=\'text-align:right\'>'.number_format($price_product,2,",","").'</td><td style=\'text-align:right\'>'.number_format($prezzo_iniziale,2,",","").'</td><td style=\'text-align:right\'>'.($unitario_st == 0 ? '' : number_format($sconto_extra,2,",","")).'</td>'.($context->employee->id_profile == 7 ? '' : '<td style=\'text-align:right\'>'.number_format($acquisto,2,",","").'</td><td>'.number_format((((($price_product - $acquisto)*100) / $price)),2,",","").'%</td>').'<td style=\'text-align:right\'>'.number_format(($price_product*$_POST['product_quantity'][$id_product]),2,",","").'</td></tr>';
								
								$totale_per_storico += $price_product*$_POST['product_quantity'][$id_product];
							}
							
							/*$total_products=$total_products*(1+$_POST['tax_rate']/100);
							Db::getInstance()->execute("UPDATE "._DB_PREFIX_."orders SET total_products=".$total_products." WHERE id_order=".$_POST['id_order']);
							$this->update_total($_POST['id_order']);*/
						}
						
						$table_per_storico .= '</tbody></table>';
						
						$i_ord = 0;
						foreach ($_POST['sort_order'] as $id_product_g=>$id_product) {
							Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart_product SET sort_order=$i_ord WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product_g)."<br />";
							Db::getInstance()->execute("UPDATE carrelli_creati_prodotti SET sort_order=$i_ord WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product_g);
							$i_ord++;
						}
						
						$files=$_FILES["joinFile"];
			
						$attachment = Db::getInstance()->getValue("
							SELECT attachment 
							FROM "._DB_PREFIX_."cart 
							WHERE id_cart = ".$_GET['id_cart']."
						");

						$attachments = array();
						$attachments1 = array();
						$file_name = $attachment;
						
						if(isset($_POST['cancella_tutti_allegati']))
							$file_name = "";
						
						$files=array();
						$fdata=$_FILES['joinFile'];

						if(is_array($fdata['name'])){
							$count_name = count($fdata['name']);
							for($i=0;$i<$count_name;++$i){
								$files[]=array(
									'name'    => $fdata['name'][$i],
									'tmp_name'=> $fdata['tmp_name'][$i],
									'type'    => $fdata['type'][$i],
									'size'    => $fdata['size'][$i],
									'error'   => $fdata['error'][$i],
								);
							}
						}
						else $files[]=$fdata;
											
						//require_once('../classes/html2pdf/html2pdf.class.php');

						if($attachment != '') {
							if(!empty($attachment)) {
										
								$allegati = explode(";",$attachment);
								foreach($allegati as $allegato) {
									
									if(strpos($allegato, ":::")) {
										$parti = explode(":::", $allegato);
										$nomevero = $parti[1];
										$nomecodificato = $parti[0];
									}
									else {
										$nomevero = $allegato;
									}
												
									if($allegato == "") { } else {
										$fileAttachment2['content'] = file_get_contents(_PS_UPLOAD_DIR_.$nomecodificato);
										$fileAttachment2['name'] = $nomevero;
										$fileAttachment2['mime'] = 'application/x-download';
										$attachments[] = $fileAttachment2;
									}
								}
								
								if(strpos($template_cart, 'zcloud') == true || strpos($template_cart, 'EASTAR-CLOUD') == true) { 
									if(Tools::getIsset('allegato_a')) {
										$id_cst = Db::getInstance()->getValue("
											SELECT id_customer 
											FROM "._DB_PREFIX_."cart 
											WHERE id_cart = ".$_GET['id_cart']."
										");

										$content_ezcloud = Cart::getCartPDF_ezcloud_A($_GET['id_cart'], $id_cst);
										$html2pdf_ezcloud  = new HTML2PDF('P','A4','it');
										$html2pdf_ezcloud ->WriteHTML($content_ezcloud);
										
										
										$pdfdoc_ezcloud  = $html2pdf_ezcloud ->Output('', true);

										$fileAttachment2['content'] = $pdfdoc_ezcloud ;
										$fileAttachment2['name'] = $id_cst.'-offerta-ezcloud-'.$_POST['id_cart'].'-allegato-A.pdf';
										$fileAttachment2['mime'] = 'application/x-download';

										$attachments[] = $fileAttachment2;
									}	
								}
							}
						}
							
						$id_cst = Db::getInstance()->getValue("
							SELECT id_customer 
							FROM "._DB_PREFIX_."cart 
							WHERE id_cart = ".$_POST['id_cart']."
						");

						$content = Cart::getCartPDF($_POST['id_cart'], $id_cst, 'y');
						
						/*if(Tools::getIsset('notifica_con_pdf')) {
							$html2pdf = new HTML2PDF('P','A4','it');
							$html2pdf->WriteHTML($content);
							
							$pdfdoc = $html2pdf->Output('', true);

							$fileAttachment1['content'] = $pdfdoc;
							$fileAttachment1['name'] = $id_cst.'-offerta-'.$_POST['id_cart'].'.pdf';
							$fileAttachment1['mime'] = 'application/x-download';

							$attachments1[] = $fileAttachment1;
						}
						else {	*/
							foreach($files as $file) {
							
								if (isset($files) AND !empty($file['name']) AND $file['error'] != 0)
									$this->_errors[] = Tools::displayError('An error occurred with the file upload.');
				
								if (!empty($file['name'])) {
									$extension = array('.txt', '.rtf', '.doc', '.docx', '.pdf', '.zip', '.png', '.jpeg', '.gif', '.jpg');
									$filename = md5(uniqid().substr($file['name'], -5));
									$fileAttachment['content'] = file_get_contents($file['tmp_name']);
									$fileAttachment['name'] = $file['name'];
									$fileAttachment['mime'] = $file['type'];
								}
								
								if (isset($filename) AND rename($file['tmp_name'], _PS_MODULE_DIR_.'../upload/'.$filename)) {
									$filename = $filename.":::".$file['name'];
									$file_name .= $filename.";";
								}

								$attachments[] = $fileAttachment;
							}
						//}

						Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart SET attachment='".$file_name."' WHERE id_cart = ".$_POST['id_cart']);
						Db::getInstance()->execute("UPDATE carrelli_creati SET attachment='".$file_name."' WHERE id_cart = ".$_POST['id_cart']);
						
						if(Tools::getIsset('rif_prev') && is_numeric(Tools::getValue('rif_prev'))) 
							Db::getInstance()->execute("UPDATE form_prevendita_thread SET status='closed' WHERE id_thread = '".Tools::getValue('rif_prev')."'");
						else
							Db::getInstance()->execute("UPDATE "._DB_PREFIX_."customer_thread SET status='closed' WHERE id_customer_thread = '".substr(Tools::getValue('rif_prev'),0,-1)."'");
						
						$customer_login = Db::getInstance()->getValue("SELECT count(id_guest) FROM "._DB_PREFIX_."guest WHERE id_customer = ".$customer->id."");
						
						$notifiche = Db::getInstance()->getValue("SELECT notifiche FROM "._DB_PREFIX_."cart WHERE id_cart = ".$_POST['id_cart']."");
						$notifiche = unserialize($notifiche);
						
						$mail_verso_cui_inviare = Tools::getValue('customer_email_msg_cart');
							
						switch($context->employee->id) {
							case 1: $interno = "(201)";
							case 2: $interno = "(202)";
							case 3: $interno = "(204)";
							case 4: $interno = "(205)";
							case 5: $interno = "(203)";
							case 6: $interno = "";
							case 7: $interno = "(207)";
							default: $interno = "";
						}

						$employeemess = new Employee($context->employee->id);

						// INIZIO TICKET
						$gentilecliente = "Gentile cliente, <br />";
						
						$firma = '<br />
							Cordiali saluti <br /><br />
							'.$employeemess->firstname." ".$employeemess->lastname.'<br /><br />
							Ezdirect srl <a href="http://www.ezdirect.it">www.ezdirect.it</a><br /><br />

							Tel +39 0585821163 '.$interno.'<br />							
							</p>';
							
						$params = array('{link}' => 'https://www.ezdirect.it/guide/39-contatti', '{msg}' => "Gentile cliente, <br /><br />
						ti ringraziamo per la richiesta del preventivo (preventivo n&deg;: <strong>".$_POST['id_cart']."</strong>).<br /><br />
						Per visualizzarlo devi prima accedere al portale <a href='https://www.ezdirect.it'>www.ezdirect.it</a> (per la login utilizza l'indirizzo email che hai fornito in fase di richiesta/registrazione.
						<br /><br />
						Una volta eseguito l'accesso, clicca su <strong>\"Vedi ultimo preventivo\".</strong><br /><br />
						Se hai pi&ugrave; offerte, seleziona <strong>\"Lista preventivi\".</strong><br /><br />
						In entrambi i casi &egrave; possibile scaricare in formato PDF il documento.
						<br /><br />
						Il nostro staff &egrave; a tua completa disposizione per qualunque genere di supporto.
						<br /><br />
						Puoi contattarci allo 0585821163. 
						<br /><br />
						Se hai bisogno di supporto chiamaci al numero 0585821163. Grazie per la tua attenzione!".$firma);
						
						//Una volta effettuato l'accesso, seleziona <a href='http://www.ezdirect.it/modules/mieofferte/offerte.php'>\"Le mie offerte\"</a>. Troverai uno o pi&ugrave; carrelli con il contenuto dettagliato.
							
						if((isset($_POST['notifica_mail']) || isset($_POST['notifica_con_pdf'])  || isset($_POST['notifica_contratto']) ) && ($context->employee->id == 1 ||  $context->employee->id == 2 ||  $context->employee->id == 3 ||  $context->employee->id == 6 || $context->employee->id == 22 || $context->employee->id == 7 || $context->employee->id == 14 || $context->employee->id == 17 || $context->employee->id == 12  || $context->employee->id == 19) && !isset($_POST['provvisorio'])) 
						{
							$indirizzi_notifica = '';
							$numero_notifiche++;

							$s_mails = explode(";", Tools::getValue('customer_email_msg_cart'));
							foreach ($s_mails as $s_mail) {
								if($s_mail == '') {
								}
								else {
									Customer::findEmailPersona(trim($s_mail), $customer->id);
									
									$id_persona = Db::getInstance()->getValue("SELECT id_persona FROM persone WHERE email = '".trim($s_mail)."'");
							
									if(Tools::getIsset('notifica_mail'))
									{	
										if(Mail::Send(5, 'msg_base', Mail::l('La tua quotazione su Ezdirect', 5), $params, trim($s_mail), NULL, 'nonrispondere@ezdirect.it', 'Ezdirect',$attachments, NULL, _PS_MAIL_DIR_, true)) {
											
											$indirizzi_notifica .= $s_mail.', ';
											Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart SET numero_notifiche = ".$numero_notifiche.", id_persona = ".$id_persona.", data_ultima_notifica = '".date("Y-m-d H:i:s")."' WHERE id_cart = ".$_POST['id_cart']."");
											Db::getInstance()->execute("UPDATE carrelli_creati SET numero_notifiche = ".$numero_notifiche.", id_persona = ".$id_persona.", data_ultima_notifica = '".date("Y-m-d H:i:s")."' WHERE id_cart = ".$_POST['id_cart']."");
										}
										$notifica = array();
										$notifica['email'] = $s_mail;
										$notifica['data'] = date("Y-m-d H:i:s");
										$notifica['tipo'] = 'Normale';
										$notifiche[] = $notifica;
									} 
									if(Tools::getIsset('notifica_con_pdf'))
									{
										
										$params = array('{msg}' => "Gentile cliente, <br /><br />
										grazie ancora per averci richiesto un preventivo. Lo abbiamo preparato e puoi visualizzarlo, quando vuoi, via web (preventivo n&deg;: <strong>".$_POST['id_cart']."</strong>). Per visualizzare il preventivo in formato PDF puoi <a href='https://www.ezdirect.it/modules/mieofferte/download_offerta.php?download=yes&originale=y&id_cart=".$_POST['id_cart']."&id_customer=".$customer->id."' target='_blank'>cliccare su questo link</a>.<br /><br />".$firma);
										if(Mail::Send(5, 'msg_base', Mail::l('Il tuo preventivo Ezdirect', 5), $params, trim($s_mail), NULL, 'nonrispondere@ezdirect.it', 'Ezdirect',$attachments1, NULL, _PS_MAIL_DIR_, true)) {
											
											$indirizzi_notifica .= $s_mail.', ';
											Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart SET numero_notifiche = ".$numero_notifiche.", id_persona = ".$id_persona.", data_ultima_notifica = '".date("Y-m-d H:i:s")."' WHERE id_cart = ".$_POST['id_cart']."");
											Db::getInstance()->execute("UPDATE carrelli_creati SET numero_notifiche = ".$numero_notifiche.", id_persona = ".$id_persona.", data_ultima_notifica = '".date("Y-m-d H:i:s")."' WHERE id_cart = ".$_POST['id_cart']."");
										}
										$notifica = array();
										$notifica['email'] = $s_mail;
										$notifica['data'] = date("Y-m-d H:i:s");
										$notifica['tipo'] = 'PDF';
										$notifiche[] = $notifica;
									}
									
									if(Tools::getIsset('notifica_contratto'))
									{
										$names = explode(' ',$cart_name);
										$id_contratto = $names[2];
										$anno = $names[4];
										$scadenza_contratto = date('Y-m-d',strtotime(date("Y-m-d H:i:s", strtotime(Db::getInstance()->getValue('SELECT data_fine FROM '._DB_PREFIX_.'contratto_assistenza WHERE id_contratto = '.$id_contratto)) . " + 365 day")));
										$importo_contratto = Db::getInstance()->getValue('SELECT importo FROM '._DB_PREFIX_.'contratto_assistenza WHERE id_contratto = '.$id_contratto);
										
										$params = array('{msg}' => 'Gentile cliente, ti informiamo che il tuo servizio di assistenza tecnica* sta per scadere (scadenza: '.Tools::displayDate($scadenza_contratto,5).').<br />
										Se desideri disdire il servizio (evitando il rinnovo), comunicacelo utilizzando i moduli di contatto del nostro sito web (ti preghiamo di non inviare email) o chiamaci al numero 0585 821163.
										<br /><br />
											<strong>Per rinnovare il servizio, fai login su Ezdirect.it e accedi al tuo carrello <a href="https://www.ezdirect.it/le-mie-offerte?id_cart='.$_POST['id_cart'].'&id_customer='.$customer->id.'">cliccando qui</a> (oppure entra nell\'area "<a href="https://www.ezdirect.it/le-mie-offerte">Le mie offerte</a>"), quindi conferma ordine/carrello che abbiamo gi&agrave; predisposto per te</strong>.
										<br /><br />* Il servizio include telegestione, supporto telefonico e via web, aggiornamenti firmware, precedenza sulla gestione dei ticket. Ti ricordiamo che, non rinnovando il servizio, potrai sempre usufruire dei servizi di assistenza, previa richiesta via ticket on line o chiamando il nostro servizio tecnico, il quale ti comunicher&agrave; ad ogni esigenza il pacchetto assistenza da acquistare, sulla base dell\'intervento da effettuare.'.$firma);
										if(Mail::Send(5, 'msg_base', Mail::l('Contratto in scadenza', 5), $params, trim($s_mail), NULL, 'nonrispondere@ezdirect.it', 'Ezdirect',$attachments1, NULL, _PS_MAIL_DIR_, true)) {
											
										}
									}

									$log_mail=fopen("../import/log-mail.txt","a+");

									if(Tools::getIsset('notifica_mail'))
										$riga_log = "CAR | Mail inviata a ".trim($s_mail)." in data ".date("Y-m-d H:i:s")."\n";
									if(Tools::getIsset('notifica_con_pdf'))
										$riga_log = "CPD | Mail inviata a ".trim($s_mail)." in data ".date("Y-m-d H:i:s")."\n";
									
									@fwrite($log_mail,$riga_log);
									@fclose($log_mail);						
								}
							}
						
							foreach ($_POST['seleziona-persona'] as $persona) {
				
								$html .= $persona."<br />";
								$notifica = array();
								
								
								if(Tools::getIsset('notifica_mail')) {
									if (Mail::Send(5, 'msg_base', Mail::l('La tua quotazione su Ezdirect', 5), $params, trim($persona), NULL, 'nonrispondere@ezdirect.it', 'Ezdirect',$attachments, NULL, _PS_MAIL_DIR_, true)) {

										$indirizzi_notifica .= $persona.', ';
										Db::getInstance()->execute("
											UPDATE "._DB_PREFIX_."cart 
											SET numero_notifiche = ".$numero_notifiche.", id_persona = ".$id_persona.", data_ultima_notifica = '".date("Y-m-d H:i:s")."' 
											WHERE id_cart = ".$_POST['id_cart']."
										");

										Db::getInstance()->execute("
											UPDATE carrelli_creati 
											SET numero_notifiche = ".$numero_notifiche.", id_persona = ".$id_persona.", data_ultima_notifica = '".date("Y-m-d H:i:s")."' 
											WHERE id_cart = ".$_POST['id_cart']."
										");
									}
										
									$notifica['email'] = $persona;
									$notifica['data'] = date("Y-m-d H:i:s");
									$notifica['tipo'] = 'Normale';
									$notifiche[] = $notifica;
								}

								if(Tools::getIsset('notifica_con_pdf')) {
									
									$params = array('{msg}' => "Gentile cliente, <br /><br />
									grazie ancora per averci richiesto un preventivo. Lo abbiamo preparato e puoi visualizzarlo, quando vuoi, via web (preventivo n&deg;: <strong>".$_POST['id_cart']."</strong>). Per visualizzare il preventivo in formato PDF puoi <a href='https://www.ezdirect.it/modules/mieofferte/download_offerta.php?download=yes&originale=y&id_cart=".$_POST['id_cart']."&id_customer=".$customer->id."' target='_blank'>cliccare su questo link</a>.<br /><br />".$firma);

									if(Mail::Send(5, 'msg_base', Mail::l('Preventivo PDF Ezdirect', 5), $params, trim($persona), NULL, 'nonrispondere@ezdirect.it', 'Ezdirect',$attachments1, NULL, _PS_MAIL_DIR_, true)) {
										$indirizzi_notifica .= $persona.', ';
										Db::getInstance()->execute("
											UPDATE "._DB_PREFIX_."cart 
											SET numero_notifiche = ".$numero_notifiche.", id_persona = ".$id_persona.", data_ultima_notifica = '".date("Y-m-d H:i:s")."' 
											WHERE id_cart = ".$_POST['id_cart']."
										");

										Db::getInstance()->execute("
											UPDATE carrelli_creati 
											SET numero_notifiche = ".$numero_notifiche.", id_persona = ".$id_persona.", data_ultima_notifica = '".date("Y-m-d H:i:s")."' 
											WHERE id_cart = ".$_POST['id_cart']."
										");
									}

									$notifica['email'] = $persona;
									$notifica['data'] = date("Y-m-d H:i:s");
									$notifica['tipo'] = 'PDF';
									$notifiche[] = $notifica;
								}

								$log_mail=fopen("../import/log-mail.txt","a+");

								if(Tools::getIsset('notifica_mail'))
									$riga_log = "CAR | Mail inviata a ".trim($persona)." in data ".date("Y-m-d H:i:s")."\n";
								if(Tools::getIsset('notifica_con_pdf'))
									$riga_log = "CPD | Mail inviata a ".trim($persona)." in data ".date("Y-m-d H:i:s")."\n";

								@fwrite($log_mail,$riga_log);
								@fclose($log_mail);							
							}

							if(Tools::getIsset('notifica_mail')) 
								Customer::Storico(Tools::getValue('id_cart'), 'C', $context->employee->id, 'Ha notificato il carrello al cliente a questi indirizzi: '.substr($indirizzi_notifica,0,-2));
							if(Tools::getIsset('notifica_con_pdf'))
								Customer::Storico(Tools::getValue('id_cart'), 'C', $context->employee->id, 'Ha inviato il PDF del carrello al cliente a questi indirizzi: '.substr($indirizzi_notifica,0,-2));
						}
						
						$indirizzi_cc = '';
						
						foreach($_POST['conoscenza'] as $conoscenza) {
						
							$numero_notifiche++;
							$employeemess = new Employee($context->employee->id);
							$mailconoscenza = Db::getInstance()->getValue("
								SELECT email 
								FROM "._DB_PREFIX_."employee 
								WHERE id_employee = $conoscenza
							");

							$tokenimp = Tools::getAdminToken("AdminCustomers".(int)(Tab::getIdFromClassName('AdminCustomers')).$conoscenza);
							$linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&tab_name=carts&id_cart=".Tools::getValue('id_cart')."&viewcart&token=".$tokenimp.'';
							
							$params4 = array(
								'{reply}' => "<strong>".$employeemess->firstname."</strong> ti ha inviato una notifica carrello in copia conoscenza.
								<br /><br />
								L'ID del carrello &egrave;: <strong>".Tools::getValue('id_cart')."</strong><br /><br />
								
								<a href='".$linkimp."'>Clicca qui per aprire il carrello</a>. ",
								'{firma}' => "",
								'{id_richiesta}' => "",
								'{link}' => ""
							);
							
							$indirizzi_cc .= $mailconoscenza.', ';
						
							Mail::Send((int)$context->language->id, 'senzagrafica', 'Notifica carrello in copia conoscenza', 
							$params4, $mailconoscenza, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
							_PS_MAIL_DIR_, true);
							
							$notifica = array();
							$notifica['email'] = $mailconoscenza;
							$notifica['data'] = date("Y-m-d H:i:s");
							$notifica['tipo'] = 'CC';
							
							$notifiche[] = $notifica;
							
						}
						
						if($indirizzi_cc != '')
							Customer::Storico(Tools::getValue('id_cart'), 'C', $context->employee->id, 'Ha inviato la notifica del carrello in copia conoscenza a: '.substr($indirizzi_cc,0,-2));
						
						$notifiche = serialize($notifiche);
						
						$notifiche =  Db::getInstance()->execute("
							UPDATE "._DB_PREFIX_."cart 
							SET notifiche = '".$notifiche."' 
							WHERE id_cart = ".$_POST['id_cart']."
						");

						$notifiche =  Db::getInstance()->execute("
							UPDATE carrelli_creati 
							SET notifiche = '".$notifiche."' 
							WHERE id_cart = ".$_POST['id_cart']."
						");

						$id_order = "REVISIONE";
						$order = new Order(0);
						$order->id = "REVISIONE";
						$tot_revisioni = Db::getInstance()->getValue('
							SELECT count(id_revisione) 
							FROM '._DB_PREFIX_.'cart_revisioni 
							WHERE id_cart = '.Tools::getValue('id_cart').' 
							ORDER BY id_revisione ASC
						');
						
						if(isset($_POST['provvisorio']))
						{
							$notifiche = array();
							$s_mails = explode(";", Tools::getValue('customer_email_msg_cart'));
							foreach ($s_mails as $s_mail) {
								if($s_mail == '') {
								}
								else {
									Customer::findEmailPersona(trim($s_mail), $customer->id);
									
									$id_persona = Db::getInstance()->getValue("SELECT id_persona FROM persone WHERE email = '".trim($s_mail)."'");
									
									$notifiche[] = $id_persona;
								}
							}
							
							foreach ($_POST['seleziona-persona'] as $persona) {
								$id_persona = Db::getInstance()->getValue("SELECT id_persona FROM persone WHERE email = '".trim($persona)."' AND id_customer = '".$customer->id."'");
								$notifiche[] = $id_persona;
							}
							
							$notifiche = serialize($notifiche);
							
							Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart SET email_preparate = '".$notifiche."' WHERE id_cart = ".$_POST['id_cart']."");
							Db::getInstance()->execute("UPDATE carrelli_creati SET email_preparate = '".$notifiche."' WHERE id_cart = ".$_POST['id_cart']."");
						}
						
						$cart_ctrl = new Cart(Tools::getValue('id_cart'));
						$carrier_cart = Db::getInstance()->getValue("SELECT id_carrier FROM "._DB_PREFIX_."cart WHERE id_cart = ".$cart_ctrl->id."");
						$costo_trasporto_modificato_r = Db::getInstance()->getValue("SELECT transport FROM "._DB_PREFIX_."cart WHERE id_cart = ".Tools::getValue('id_cart')."");
						$costo_trasporto_modificato_r = explode(":",$costo_trasporto_modificato_r);
						$costo_trasporto_modificato = $costo_trasporto_modificato_r[1];

						if($carrier_cart != 0) {
							if($costo_trasporto_modificato != "" && $costo_trasporto_modificato > 0) {
								$costo_spedizione = $costo_trasporto_modificato;
							}
							else {				
								$costo_spedizione = $cart_ctrl->getOrderShippingCost($carrier_cart, false);
								$costo_spedizione = $cart_ctrl->getOrderShippingCostByTotalAndDelivery($carrier_cart, $totale, $consegna, $customer->id_default_group, false);
							}
						}
						else {
							$costo_spedizione = $cart_ctrl->getOrderShippingCostByTotalAndDelivery($default_carrier, $totale, $consegna, $customer->id_default_group, false);
						}
						
						$marginalitatotale = ((($totale - $totaleacquisti)*100) / $totale);
						if($totale < 0)
							$marginalitatotale = 0-$marginalitatotale;
						$guadagno = $totale - $totaleacquisti;
						$totale_senza_spedizione = $totale;

						$marginalitatotale_prodotti = ((($totale_prodotti - $totaleacquisti_prodotti)*100) / $totale_prodotti);
						if($totale < 0)
							$marginalitatotale_prodotti = 0-$marginalitatotale_prodotti;
							
						$guadagno_prodotti = $totale_prodotti - $totaleacquisti_prodotti;
					
						$marginalitatotale_ezcloud = ((($totale_ezcloud - $totaleacquisti_ezcloud)*100) / $totale_ezcloud);
						if($totale_ezcloud < 0)
							$marginalitatotale_ezcloud = 0-$marginalitatotale_ezcloud;
							
						$guadagno_ezcloud = $totale_ezcloud - $totaleacquisti_ezcloud;
						$totale_senza_spedizione_ezcloud = $totale_ezcloud;		
						
						$table_per_storico .= '<br /><strong>Trasporto</strong>: '.($costo_trasporto_modificato == '' || $costo_trasporto_modificato == 0 ? number_format($costo_spedizione,2,',','') : str_replace(".",",",$costo_trasporto_modificato)).'<br /><strong>Totale</strong>: '.number_format($totale_per_storico+(($costo_trasporto_modificato == '' || $costo_trasporto_modificato == 0 ? $costo_spedizione : $costo_trasporto_modificato)),2,",","").'<br />';
						
						Customer::Storico(Tools::getValue('id_cart'), 'C', $context->employee->id, 'Ha salvato il carrello con questa configurazione<br /><br />'.$table_per_storico);
						
						if(isset($_POST['prepara_mail']) && ($context->employee->id != 1 &&  $context->employee->id != 2 &&  $context->employee->id != 3 &&  $context->employee->id != 6 && $context->employee->id != 22 && $context->employee->id != 7 && $context->employee->id != 14 && $context->employee->id != 17 && $context->employee->id != 12  && $context->employee->id != 19)) 
						{
							$notifiche = array();
							$s_mails = explode(";", Tools::getValue('customer_email_msg_cart'));

							foreach ($s_mails as $s_mail) {
								if($s_mail == '') {
									// niente
								}
								else {
									Customer::findEmailPersona(trim($s_mail), $customer->id);
									
									$id_persona = Db::getInstance()->getValue("SELECT id_persona FROM persone WHERE email = '".trim($s_mail)."'");
							
									$notifiche[] = $id_persona;
								}
							}
							
							foreach ($_POST['seleziona-persona'] as $persona) {
								$id_persona = Db::getInstance()->getValue("SELECT id_persona FROM persone WHERE email = '".trim($persona)."' AND id_customer = '".$customer->id."'");
								$notifiche[] = $id_persona;
							}
							
							$notifiche = serialize($notifiche);
							
							Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart SET email_preparate = '".$notifiche."' WHERE id_cart = ".$_POST['id_cart']."");
							Db::getInstance()->execute("UPDATE carrelli_creati SET email_preparate = '".$notifiche."' WHERE id_cart = ".$_POST['id_cart']."");
						}
						
						if(Tools::getIsset('prezzi_carrello'))
						{
							
							$provincia_cliente = Db::getInstance()->getValue("SELECT id_state FROM "._DB_PREFIX_."address WHERE id_customer = ".$customer->id." AND active = 1 AND deleted = 0 AND fatturazione = 1");
							
							$nazione_cliente = Db::getInstance()->getValue("SELECT id_country FROM "._DB_PREFIX_."address WHERE id_customer = ".$customer->id." AND active = 1 AND deleted = 0 AND fatturazione = 1");
								
							if($provincia_cliente == 0) {
								$zona_cliente = Db::getInstance()->getValue("SELECT id_zone FROM "._DB_PREFIX_."country WHERE id_country = ".$nazione_cliente."");
							}
							else {
								$zona_cliente = Db::getInstance()->getValue("SELECT id_zone FROM "._DB_PREFIX_."state WHERE id_state = ".$nazione_cliente."");
							}
							$gruppi_cli = array();
							
							if(Tools::getValue('prezzi_carrello') == 3)
								$gruppi_cli[] = 3;
							else if(Tools::getValue('prezzi_carrello') == 15)
								$gruppi_cli[] = 15;
							else if(Tools::getValue('prezzi_carrello') == '' || Tools::getValue('prezzi_carrello') == 0 || !Tools::getValue('prezzi_carrello'))
								$gruppi_cli[] = Db::getInstance()->getValue('SELECT id_default_group FROM '._DB_PREFIX_.'customer WHERE id_customer = '.Tools::getValue('id_customer'));
							
							Customer::Storico(Tools::getValue('id_cart'), 'C', $context->employee->id, 'Ha applicato prezzi '.(Tools::getValue('prezzi_carrello') == 3 ? 'rivenditore' : 'cliente web'));
							
							$metodi_spedizione = Cart::getCarriersForEditOrder($zona_cliente, $gruppi_cli, Tools::getValue('id_cart'));
						
							foreach($metodi_spedizione as $metodo) {
								if($metodo['name'] != 'Ritiro in sede Ezdirect' && $metodo['name'] != 'Trasporto gratuito') {
									$default_carrier = $metodo['id_carrier'];
								}
								else {
								}
							}
							
							$costo_spedizione = $cart_ctrl->getOrderShippingCostByTotalAndDelivery($default_carrier, 999999999, $customer->id_address_delivery, Tools::getValue('prezzi_carrello'), false);
							
							$gruppi_riv = array();
							$gruppi_riv[] = 3;
							
							$metodi_spedizione_riv = Cart::getCarriersForEditOrder($zona_cliente, $gruppi_riv, Tools::getValue('id_cart'));
						
							foreach($metodi_spedizione_riv as $metodo_riv) {
								if($metodo_riv['name'] != 'Ritiro in sede Ezdirect' && $metodo_riv['name'] != 'Trasporto gratuito') {
									$default_carrier_riv = $metodo_riv['id_carrier'];
								}
								else {
								}
							}
							
							$costo_spedizione_riv = $cart_ctrl->getOrderShippingCostByTotalAndDelivery($default_carrier_riv, 999999999, $customer->id_address_delivery, Tools::getValue('prezzi_carrello'), false);
							
							$cart_ctrl = new Cart(Tools::getValue('id_cart'));
							
							$html .= Tools::getValue('prezzi_carrello').'<br />';
							foreach($gruppi_cli as $g)
								$html .= $g.'<br />';
							if(Tools::getValue('prezzi_carrello') == 15)
								$transportz = $default_carrier.':'.$costo_spedizione_riv;
							else
								$transportz = $default_carrier.':'.$costo_spedizione;
							
							Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'cart SET id_carrier = "'.$default_carrier.'", transport = "'.$transportz.'", prezzi_carrello = '.Tools::getValue('prezzi_carrello').' WHERE id_cart = '.Tools::getValue('id_cart'));
							Db::getInstance()->execute('UPDATE carrelli_creati SET id_carrier = "'.$default_carrier.'", transport = "'.$transportz.'", prezzi_carrello = '.Tools::getValue('prezzi_carrello').', sc_qta = 1 WHERE id_cart = '.Tools::getValue('id_cart'));
							
							$products_to_update = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'cart_product WHERE id_cart = '.Tools::getValue('id_cart'));
							foreach($products_to_update as $ptu)
							{
								if(Tools::getValue('prezzi_carrello') == 3)
								{
									//$new_price = Product::trovaMigliorPrezzo($ptu['id_product'],3,999999) - (Product::trovaMigliorPrezzo($ptu['id_product'],3,999999)*($ptu['sconto_extra']/100));
									
									$new_price = Product::trovaMigliorPrezzo($ptu['id_product'],3,999999);
									
									Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'cart_product SET sconto_extra = 0, price = '.$new_price.', sc_qta = 1 WHERE id_cart = '.Tools::getValue('id_cart').' AND id_product = '.$ptu['id_product']);
									Db::getInstance()->execute('UPDATE carrelli_creati_prodotti SET sconto_extra = 0, price = '.$new_price.', sc_qta = 1 WHERE id_cart = '.Tools::getValue('id_cart').' AND id_product = '.$ptu['id_product']);
								}
								else if(Tools::getValue('prezzi_carrello') == 15)
								{
									$new_price = Product::trovaMigliorPrezzo($ptu['id_product'],3,999999);
									
									$unitario =  Product::trovaMigliorPrezzo($ptu['id_product'],1,$ptu['quantity']);
									
									$new_price = $new_price+(($new_price/100)*3);
									
									if($new_price < Product::trovaMigliorPrezzo($ptu['id_product'],1,1))
									{	
										$sconto_extra = (($unitario - $new_price)*100)/$unitario;
									
										//if($sconto_extra < 0)
										//{	
											Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'cart_product SET sconto_extra = '.$sconto_extra.', price = '.$new_price.', sc_qta = 0 WHERE id_cart = '.Tools::getValue('id_cart').' AND id_product = '.$ptu['id_product']);
											
											Db::getInstance()->execute('UPDATE carrelli_creati_prodotti SET sconto_extra =  '.$sconto_extra.', price = '.$new_price.', sc_qta = 0 WHERE id_cart = '.Tools::getValue('id_cart').' AND id_product = '.$ptu['id_product']);
										//}
									}
								}	
								else
								{
									//$new_price = Product::trovaMigliorPrezzo($ptu['id_product'],1,$ptu['quantity'])  - (Product::trovaMigliorPrezzo($ptu['id_product'],1,$ptu['quantity'])*($ptu['sconto_extra']/100));
									$new_price = Product::trovaMigliorPrezzo($ptu['id_product'],1,$ptu['quantity']);
									Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'cart_product SET sconto_extra = 0, price = '.$new_price.' WHERE id_cart = '.Tools::getValue('id_cart').' AND id_product = '.$ptu['id_product']);
									Db::getInstance()->execute('UPDATE carrelli_creati_prodotti SET sconto_extra = 0, price = '.$new_price.' WHERE id_cart = '.Tools::getValue('id_cart').' AND id_product = '.$ptu['id_product']);
								}
							}
						}
					
						if(Tools::getIsset('crea_una_revisione'))
						{
							//Customer::Storico(Tools::getValue('id_cart'), 'C', $context->employee->id, 'Ha salvato il carrello con questa configurazione<br /><br />'.$table_per_storico);
							Customer::Storico(Tools::getValue('id_cart'), 'C', $context->employee->id, 'Ha creato una revisione');
							
							$id_cart = Tools::getValue('id_cart');
							$da_copiare = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'cart WHERE id_cart = '.$id_cart.'');
							$data_rev = date('Y-m-d H:i:s')	;
							//mysql_select_db (_DB_REV_NAME_);
													
							Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'cart_revisioni (id_revisione, id_cart, id_carrier, id_lang, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, date_add, date_upd, rif_prev, name, riferimento, premessa, risorse, esigenze, note, note_private, preventivo, validita, consegna, in_carico_a, payment, attachment, created_by, id_employee, data_ultima_notifica, notifiche, numero_notifiche, rif_ordine, cig, cup, ipa, data_ordine_mepa, competenza_dal, competenza_al, cadenza, scadenza, decorrenza, esito, causa, concorrente) VALUES (NULL, '.$id_cart.', "'.$da_copiare['id_carrier'].'", "'.$da_copiare['id_lang'].'","'.addslashes($da_copiare['id_address_delivery']).'","'.addslashes($da_copiare['id_address_invoice']).'",1,'.$da_copiare['id_customer'].',0,"",0,0,"","'.$data_rev.'", "'.$data_rev.'", "'.addslashes($da_copiare['rif_prev']).'", "'.addslashes($da_copiare['name']).'", "'.addslashes($da_copiare['riferimento']).'", "'.addslashes($da_copiare['premessa']).'","'.addslashes($da_copiare['risorse']).'","'.addslashes($da_copiare['esigenze']).'","'.addslashes($da_copiare['note']).'","'.addslashes($da_copiare['note_private']).'","'.addslashes($da_copiare['preventivo']).'","'.$da_copiare['validita'].'","'.addslashes($da_copiare['consegna']).'","'.addslashes($da_copiare['in_carico_a']).'","'.$da_copiare['payment'].'","'.$da_copiare['attachment'].'",'.$context->employee->id.', '.$context->employee->id.',  \''.$da_copiare['data_ultima_notifica'].'\', \''.$da_copiare['notifiche'].'\', '.$da_copiare['numero_notifiche'].',"'.$da_copiare['rif_ordine'].'","'.$da_copiare['cig'].'","'.$da_copiare['cup'].'","'.$da_copiare['ipa'].'","'.$da_copiare['data_ordine_mepa'].'","'.$da_copiare['competenza_dal'].'","'.$da_copiare['competenza_al'].'","'.$da_copiare['cadenza'].'","'.$da_copiare['scadenza'].'","'.$da_copiare['decorrenza'].'","'.$da_copiare['esito'].'","'.$da_copiare['causa'].'","'.$da_copiare['concorrente'].'")');
														
							//mysql_select_db (_DB_NAME_);
							$prodotti_da_copiare = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'cart_product WHERE id_cart = '.trim($id_cart).'');
												
							//mysql_select_db (_DB_REV_NAME_);
							$id_revisione  = Db::getInstance()->getValue('SELECT id_revisione FROM '._DB_PREFIX_.'cart_revisioni ORDER BY id_revisione DESC');
												
							foreach($prodotti_da_copiare as $prodotto_da_copiare) 
							{
								if($prodotto_da_copiare['id_product'] != 0)
									Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'cart_product_revisioni (id_revisione, id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, sconto_extra, prezzo_acquisto, no_acq, sort_order, bundle,section,  date_add) VALUES ('.$id_revisione.', '.$id_cart.', "'.$prodotto_da_copiare['id_product'].'", "'.$prodotto_da_copiare['id_product_attribute'].'", "'.$prodotto_da_copiare['quantity'].'", "'.$prodotto_da_copiare['price'].'","'.$prodotto_da_copiare['free'].'","'.addslashes($prodotto_da_copiare['name']).'","'.$prodotto_da_copiare['sc_qta'].'","'.$prodotto_da_copiare['sconto_extra'].'","'.$prodotto_da_copiare['prezzo_acquisto'].'","'.$prodotto_da_copiare['no_acq'].'","'.$prodotto_da_copiare['sort_order'].'","'.$prodotto_da_copiare['bundle'].'","'.$prodotto_da_copiare['section'].'","'.date('Y-m-d H:i:s').'")'); 
								
								//mysql_select_db (_DB_NAME_);
								/*if($prodotto_da_copiare['sconto_extra'] == 0)
								{
									//if($prodotto_da_copiare['sc_qta'] == 1)
										$price = Product::trovaMigliorPrezzo($prodotto_da_copiare['id_product'], $customer->id_default_group, $prodotto_da_copiare['quantity']);
									//else
									//	$price = 0;
								}
								else
								{
									$price = Db::getInstance()->getValue('SELECT price FROM '._DB_PREFIX_.'product WHERE id_product = '.$prodotto_da_copiare['id_product']);
									
									//if($prodotto_da_copiare['sc_qta'] == 1)
										$price = Product::trovaMigliorPrezzo($prodotto_da_copiare['id_product'], $customer->id_default_group, $prodotto_da_copiare['quantity']);
									
									$price = $price - ($price * ($prodotto_da_copiare['sconto_extra'] / 100));
								}
								
								Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'cart_product SET price = '.$price.' WHERE id_product = '.$prodotto_da_copiare['id_product'].' AND id_cart = '.trim($id_cart).'');
								Db::getInstance()->execute('UPDATE carrelli_creati_prodotti SET price = '.$price.' WHERE id_product = '.$prodotto_da_copiare['id_product'].' AND id_cart = '.trim($id_cart).'');*/
								//mysql_select_db (_DB_REV_NAME_);
							}
							
							//mysql_select_db (_DB_NAME_);
												
							$revisioni = Db::getInstance()->getValue('SELECT revisioni FROM '._DB_PREFIX_.'cart WHERE id_cart = '.trim($id_cart).'');
							$revisioni = $revisioni + 1;

							Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'cart SET notifiche = "", numero_notifiche = "", revisioni = '.$revisioni.' WHERE id_cart = '.trim($id_cart).'');
							Db::getInstance()->execute('UPDATE carrelli_creati SET notifiche = "", numero_notifiche = "", revisioni = '.$revisioni.' WHERE id_cart = '.trim($id_cart).'');
							
							if($_POST['price_updates'] == 'y')
								Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'cart_product SET prezzo_acquisto = 0 WHERE id_cart = '.trim($id_cart).'');

							Tools::redirectAdmin(self::$currentIndex.'&id_cart='.Tools::getValue('id_cart').'&viewcart&id_customer='.$customer->id.'&viewcustomer&conf=4&token='.$this->token);
						}
					
						if($_POST['Apply'] == 'Conferma ordine') 
						{
							Customer::Storico(Tools::getValue('id_cart'), 'C', $context->employee->id, 'Ha convertito in ordine');
							Tools::redirectAdmin('index.php?controller=AdminCarts&id_customer='.$customer->id.'&id_cart='.$_GET['id_cart'].'&viewcart&no_csv='.Tools::getValue('no_csv').'&convertcart&token='.$this->token.'');
						}
					}
					else
					{
						Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'cart_product SET bundle = 0 WHERE id_cart = '.Tools::getValue('id_cart').' AND bundle = 88888877');
						Db::getInstance()->execute('UPDATE carrelli_creati SET bundle = 0 WHERE id_cart = '.Tools::getValue('id_cart').' AND bundle = 88888877');
						Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'cart_product SET bundle = 0 WHERE id_cart = '.Tools::getValue('id_cart').' AND bundle = 88888878');
						Db::getInstance()->execute('UPDATE carrelli_creati SET bundle = 0 WHERE id_cart = '.Tools::getValue('id_cart').' AND bundle = 88888878');
					}
				} 
				
				if(strtotime($validita_corretto) < strtotime(date('Y-m-d H:i:s'))) 
				{
					foreach ($_POST['product_price'] as $id_product=>$price_product) 
					{
						$unitario = Db::getInstance()->getValue('SELECT price FROM '._DB_PREFIX_.'product WHERE id_product = '.$id_product);
						$acquisto = Db::getInstance()->getValue('SELECT wholesale_price FROM '._DB_PREFIX_.'product WHERE id_product = '.$id_product);
						
						if(!empty($_POST['usa_sconti_quantita'][$id_product])) 
						{
							$price_product = Product::trovaMigliorPrezzo($id_product, $customer->id_default_group, $_POST['product_quantity'][$id_product]);
							$price_product = $price_product - ($price_product * ( str_replace(',', '.',$_POST['sconto_extra'][$id_product])/100));
							$price_product = str_replace(',', '.', $price_product);
							
							Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart_product SET sc_qta=1 WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							Db::getInstance()->execute("UPDATE carrelli_creati SET sc_qta=1 WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
						}
						else 
						{
							$price_product = str_replace(',', '.', $unitario);
							$price_product = $price_product - ($price_product * ( str_replace(',', '.',$_POST['sconto_extra'][$id_product])/100));
							
							Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart_product SET sc_qta=0 WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							Db::getInstance()->execute("UPDATE carrelli_creati SET sc_qta=0 WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
						}
						
						Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart_product SET ".(isset($_GET['ajaxsave']) ? '' : 'bundle=0,')." price=".$price_product." WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
						Db::getInstance()->execute("UPDATE carrelli_creati_prodotti SET ".(isset($_GET['ajaxsave']) ? '' : 'bundle=0,')."  price=".$price_product." WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
						
						if($price_product == 0) {
							Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart_product SET free=1 WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							Db::getInstance()->execute("UPDATE carrelli_creati_prodotti SET free=1 WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
						}
						
						$acquisto = str_replace(',', '.', $acquisto);
						
						Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart_product SET prezzo_acquisto=".$acquisto.", no_acq = ".($acquisto == 0 ? 1 : 0)." WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
						Db::getInstance()->execute("UPDATE carrelli_creati_prodotti SET prezzo_acquisto=".$acquisto.", no_acq = ".($acquisto == 0 ? 1 : 0)." WHERE id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
					}
					
					$nuova_validita = date('Y-m-d', strtotime($validita_corretto. ' + 15 days'));
					
					//Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cart SET validita = '".$validita_corretto."' WHERE id_cart = ".$_POST['id_cart']);
					//Db::getInstance()->execute("UPDATE carrelli_creati SET validita = '".$validita_corretto."' WHERE id_cart = ".$_POST['id_cart']);
				}
				
				$cart_name = Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'cart WHERE id_cart = '.Tools::getValue('id_cart'));
				$cart_template = Db::getInstance()->getValue('SELECT template FROM '._DB_PREFIX_.'cart WHERE id_cart = '.Tools::getValue('id_cart'));

				// Correggere: i carrelli 103055 e 104306 non esistono?
				if(Tools::getValue('id_cart') == 103055 || Tools::getValue('id_cart') == 104306 || (strpos($cart_template, 'zcloud ') == true)  || (strpos($cart_name, 'zcloud ') == true)  || strpos($template_cart, 'EASTAR-CLOUD') == true)
				{
					Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'cart_product SET free = 1 WHERE id_product = 367442 AND id_cart = '.Tools::getValue('id_cart'));
					Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'cart_product SET free = 1 WHERE id_product = 367491 AND id_cart = '.Tools::getValue('id_cart'));
				}
				
				if(Tools::getIsset('no_addebito_commissioni'))
					$no_addebito_commissioni = 1;
				else
					$no_addebito_commissioni = 0;
				
				Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'cart SET no_addebito_commissioni = '.$no_addebito_commissioni.' WHERE id_cart = '.Tools::getValue('id_cart'));
				
				$cart = new Cart(Tools::getValue('id_cart'));
				$total_cart = $cart->getOrderTotal(false, Cart::BOTH);
				Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'cart SET total_products = '.$total_cart.' WHERE id_cart = '.Tools::getValue('id_cart'));
				Db::getInstance()->execute('UPDATE carrelli_creati SET total_products = '.$total_cart.' WHERE id_cart = '.Tools::getValue('id_cart'));
				
				Tools::redirectAdmin(self::$currentIndex.'&id_cart='.Tools::getValue('id_cart').'&id_customer='.$customer->id.'&viewcustomer&conf=4&tab_name=carts&preventivo='.Tools::getValue('preventivo').'&viewcart'.'&token='.$this->token.(isset($_POST['notifica_mail']) ? '&notifica_inviata=y' : '').(Tools::getValue('Apply') == 'copia_carrello_stesso' ? '&copia_carrello_stesso=y' : '').(Tools::getValue('Apply') == 'copia_carrello' ? '&copia_carrello=y' : '').(Tools::getValue('Apply') == 'sposta_carrello' ? '&sposta_carrello=y' : '') );
			}		
			
			$html .= '
			<script type="text/javascript">
			var tabber2 = new Yetii({
			id: "tab-container-cart",
			tabclass: "yetii-cart",
			});
			</script>';
		}
		else
			die('Non hai i permessi per visualizzare questo carrello/preventivo');
		
		return $html;
	}

	private function displayCustomizedDatas(&$customizedDatas, &$product, &$currency, &$image, $tokenProducts, &$stock)
	{
		if (!($order = AdminCarts::loadObject(true)))
			return;

		if (is_array($customizedDatas) AND isset($customizedDatas[(int)($product['id_product'])][(int)($product['id_product_attribute'])]))
		{
			if ($image = new Image($image['id_image']))
				$html .= '
					<tr>
						<td align="center">'.(isset($image->id_image) ? cacheImage(_PS_IMG_DIR_.'p/'.$image->getExistingImgPath().'.jpg',
						'product_mini_'.(int)($product['id_product']).(isset($product['id_product_attribute']) ? '_'.(int)($product['id_product_attribute']) : '').'.jpg', 45, 'jpg') : '--').'</td>
						<td><a href="index.php?controller=AdminProducts&id_product='.$product['id_product'].'&updateproduct&token='.$tokenProducts.'">
							<span class="productName">'.$product['name'].'</span>'.(isset($product['attributes']) ? '<br />'.$product['attributes'] : '').'<br />
							'.($product['reference'] ? ('Ref:').' '.$product['reference'] : '')
							.(($product['reference'] AND $product['supplier_reference']) ? ' / '.$product['supplier_reference'] : '')
							.'</a></td>
						<td align="center">'.Tools::displayPrice($product['price_wt'], $currency, false).'</td>
						<td align="center" class="productQuantity">'.$product['customizationQuantityTotal'].'</td>
						<td align="center" class="productQuantity">'.(int)($stock['quantity']).'</td>
						<td align="right">'.Tools::displayPrice($product['total_customization_wt'], $currency, false).'</td>
					</tr>';
			foreach ($customizedDatas[(int)($product['id_product'])][(int)($product['id_product_attribute'])] AS $customization)
			{
				$html .= '
				<tr>
					<td colspan="2">';
				foreach ($customization['datas'] AS $type => $datas)
					if ($type == _CUSTOMIZE_FILE_)
					{
						$i = 0;
						$html .= '<ul style="margin: 0; padding: 0; list-style-type: none;">';
						foreach ($datas AS $data)
							$html .= '<li style="display: inline; margin: 2px;">
									<a href="displayImage.php?img='.$data['value'].'&name='.(int)($order->id).'-file'.++$i.'" target="_blank"><img src="'._THEME_PROD_PIC_DIR_.$data['value'].'_small" alt="" /></a>
								</li>';
						$html .= '</ul>';
					}
					elseif ($type == _CUSTOMIZE_TEXTFIELD_)
					{
						$i = 0;
						$html .= '<ul style="margin-bottom: 4px; padding: 0; list-style-type: none;">';
						foreach ($datas AS $data)
							$html .= '<li>'.($data['name'] ? $data['name'] : ('Text #').++$i).(':').' <b>'.$data['value'].'</b></li>';
						$html .= '</ul>';
					}
				$html .= '</td>
					<td align="center"></td>
					<td align="center" class="productQuantity">'.$customization['quantity'].'</td>
					<td align="center" class="productQuantity"></td>
					<td align="center"></td>
				</tr>';
			}
		}
	}
	
	protected function _displayDeleteLink($token = NULL, $id)
	{		
		foreach ($this->_list as $cart)
			if ($id == $cart['id_cart'])
				if ($cart['id_order'])
					return;
		
		$html .= '
			<a href="'.self::$currentIndex.'&'.$this->identifier.'='.$id.'&delete'.$this->table.'&token='.($token != null ? $token : $this->token).'" onclick="return confirm(\''.$_cacheLang['DeleteItem'].'\');">
			<img src="../img/admin/delete.gif" alt="'.$_cacheLang['Delete'].'" title="'.$_cacheLang['Delete'].'" /></a>
		';
	}
	
	public function openUploadedFile()
	{
		$filename = $_GET['filename'];
		
		if(strpos($filename, ":::")) {
			$parti = explode(":::", $filename);
			$nomecodificato = $parti[0];
			$nomevero = $parti[1];
		}
		else {
			$nomecodificato = $filename;
			$nomevero = $filename;
		}
		
		$extensions = array('.txt' => 'text/plain', '.rtf' => 'application/rtf', '.doc' => 'application/msword', '.docx'=> 'application/msword',
		'.pdf' => 'application/pdf', '.zip' => 'multipart/x-zip', '.png' => 'image/png', '.jpeg' => 'image/jpeg', '.gif' => 'image/gif', '.jpg' => 'image/jpeg');

		$extension = '';

		foreach ($extensions AS $key => $val)
			if (substr($nomevero, -4) == $key OR substr($nomevero, -5) == $key) {
				$extension = $val;
				break;
			}

		$html .= $nomecodificato."<br />";
		$html .= $nomevero;
		ob_end_clean();
		header('Content-Type: '.$extension);
		header('Content-Disposition:attachment;filename="'.$nomevero.'"');
		readfile(_PS_UPLOAD_DIR_.$nomecodificato);
		die;
	}
}