<?php

class AdminSearchController extends AdminSearchControllerCore
{
    public function searchCatalog()
    {
        $this->context = Context::getContext();

        $array_opzioni = array();
		
		$array_opzioni['online'] = Tools::getValue('prodotti_online_src');
		$array_opzioni['offline'] = Tools::getValue('prodotti_offline_src');
		$array_opzioni['old'] = Tools::getValue('prodotti_old_src');
		$array_opzioni['disponibili'] = Tools::getValue('prodotti_disponibili_src');
		$array_opzioni['marca'] = Tools::getValue('auto_marca_src');
		$array_opzioni['serie'] = Tools::getValue('auto_serie_src');
		$array_opzioni['categoria'] = Tools::getValue('auto_categoria_src');
		$array_opzioni['fornitore'] = Tools::getValue('auto_fornitore_src');
		$array_opzioni['note'] = Tools::getValue('prodotti_note_src');
		
		$opzioni = serialize($array_opzioni);

        // Correggere: aggiungere parametri alla funzione nella classe?
        $this->_list['products'] = Product::searchByName($this->context->language->id, $this->query, '', $opzioni);

        if(Tools::getIsset('qta_prev'))
			$this->_list['products'] = Product::searchByName($this->context->language->id, $this->query, 'qta_prev', $opzioni);
		
		if(Tools::getIsset('old') && Tools::getValue('old') == 'yes')
		{
			$this->_list['products'] = Product::searchByName($this->context->language->id, $this->query, '', $opzioni);
		}
		if (!empty($this->_list['products']))
			for ($i = 0; $i < count($this->_list['products']); $i++)
				$this->_list['products'][$i]['nameh'] = str_ireplace($this->query, '<span class="highlight">'.Tools::htmlentitiesUTF8($this->query).'</span>', $this->_list['products'][$i]['name']);

        $this->_list['categories'] = Category::searchByName($this->context->language->id, $this->query);
    }

    /*public function searchBundles() //  Correggere: implementare se usiamo i bundle 1.4
	{
		$this->_list['bundles'] = Bundle::searchByName((int)$cookie->id_lang, $this->query);
	}*/
	
	public function searchTickets()
	{
		$this->_list['tickets'] = Customer::searchTicketById($this->query);
	}
	
	public function searchOrders()
	{
		$this->_list['orders'] = Order::searchOrderById($this->query);
	}
	
	public function searchCarts()
	{
		//$this->_list['carts'] = Cart::searchCartById($this->query);
	}
	
	public function searchBDL()
	{
		$this->_list['bdl'] = Customer::searchBDLById($this->query); // correggere: spostare in class BDL?
	}
	
	public function searchPersone()
	{
		$searchType = (int)Tools::getValue('bo_search_type');
		$this->_list['customers'] = Customer::searchByNameWithNotes($this->query);
	}

    public function searchCustomer()
	{
		//$searchType = (int)Tools::getValue('bo_search_type');
		
		$this->_list['customers'] = Customer::searchByName($this->query);
		
		if(Tools::getValue('note_persone'))
			$this->_list['customers'] = Customer::searchByNameWithNotes($this->query);
	}
	
	public function searchCustomerPI()
	{
		//$searchType = (int)Tools::getValue('bo_search_type');

        $this->_list['customers'] = Customer::searchByPI($this->query);
	}

    protected function initProductList()
    {
        $this->show_toolbar = false;
        $this->fields_list['products'] = array(
            'id_product' => array('title' => $this->l('ID'), 'width' => 25),
			'image' => array('title' => $this->l('Photo')),
			'name' => array('title' => $this->l('Name'), 'width' => 'auto'),
			'reference' => array('title' => $this->l('Reference'), 'orderby' => 'p.reference'),
			'ean13' => array('title' => $this->l('EAN'), 'orderby' => 'p.ean13'),
			'price_tax_excl' => array('title' => $this->l('Base price'), 'orderby' => 'p.price', 'type' => 'price'),
			'login_for_offer' => array('title' => $this->l('Login for offer')),
            'manufacturer_name' => array('title' => $this->l('Manufacturer'), 'align' => 'center', 'width' => 200),
			'listino' => array('title' => $this->l('List price'), 'orderby' => 'p.listino', 'type' => 'price'),
			'stock_quantity' => array('title' => $this->l('EZ_ '), 'title_description' => 'Quantita magazzino EZ', 'width' => 30,  'align' => 'right', 'orderby' => 'p.stock_quantity'),
			'ordinato_quantity' => array('title' => $this->l('OEZ'),  'width' => 30, 'title_description' => 'Quantita ordinato al fornitore (caricata su eSolver)', 'align' => 'right', 'orderby' => 'p.ordinato_quantity'),
			'amazon_quantity' => array('title' => $this->l('AMZ'),  'width' => 30, 'title_description' => 'Magazzino Amazon', 'align' => 'right', 'orderby' => 'p.amazon_quantity'),
			'first_supplier' => array('title' => $this->l('Forn.1'),  'title_description' => 'Quantita disponibile Fornitore 1', 'width' => 30,  'align' => 'right','orderby' => 'p.supplier_quantity'),
			'arrivo_supplier' => array('title' => $this->l('Arrivo'),  'title_description' => 'Quantita in arrivo Fornitore 1', 'width' => 30,  'align' => 'right','orderby' => 'p.arrivo_quantity'),
			'second_supplier' => array('title' => $this->l('Forn.2'),  'title_description' => 'Quantita disponibile Fornitore 2', 'width' => 30,  'align' => 'right','orderby' => 'p.itancia_quantity'),
			'impegnato_quantity' => array('title' => $this->l('Imp.Sito'), 'title_description' => 'Impegnato da sito (quantita ordinata da clienti su sito)', 'width' => 30,  'align' => 'right','orderby' => 'p.impegnato_quantity'),
			'prev' => array('title' => $this->l('Prev.'),  'width' => 30, 'title_description' => 'Quantita prodotto dentro preventivi aperti', 'align' => 'right','orderby' => 'cap.prev'),
            'active' => array('title' => $this->l('St.'), 'width' => 70, 'active' => 'status', 'align' => 'center', 'type' => 'bool', 'callback' => 'printVerificatoIcon')
        );

        /*'ID' => array('title' => $this->l('ID'), 'orderby' => 'p.id_product'),
		
		'name' => array('title' => $this->l('Name'), 'orderby' => 'pl.name'),
		
		
		
		
		
		'action' => array('title' => $this->l('Actions'))*/
    }

    protected function initCustomerList()
    {
        $this->fields_list['customers'] = (array(
            'id_customer' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 25), // solo 'id'?
            'codice_esolver' => array('title' => $this->l('eSolver')),
            'company' => array('title' => $this->l('Company')),
            'lastname' => array('title' => $this->l('Name'), 'align' => 'left', 'width' => 'auto'),
            'firstname' => array('title' => $this->l('First Name'), 'align' => 'left', 'width' => 150),
            //'email' => array('title' => $this->l('Email address'), 'align' => 'left', 'width' => 250), // 'e-mail'?
            'group_name' => array('title' => $this->l('Gruppo')),
            'iso_code' => array('title' => $this->l('Prov.')),
            'tax_code' => array('title' => $this->l('PI/CF')),
            'phone' => array('title' => $this->l('Tel.')),
            //'birthday' => array('title' => $this->l('Birth date'), 'align' => 'center', 'type' => 'date', 'width' => 75),
            //'orders' => array('title' => $this->l('Orders'), 'align' => 'center', 'width' => 50),
            'active' => array('title' => $this->l('Enabled'), 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'width' => 25), // 'status'?
            //'actions' => array('title' => $this->l('Actions'))
        ));
    }

    protected function initOrderList()
    {
        $this->fields_list['orders'] = array(
            'reference' => array('title' => $this->l('Reference'), 'align' => 'center', 'width' => 65),
            'id_order' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 25),
            'preventivo' => array('title' => $this->l('preventivo')),
            'customer' => array('title' => $this->l('Customer')),
            'esolver'  => array('title' => $this->l('eSolver'), 'orderby' => 'codice_esolver'),
            'gruppo' => array('title' => $this->l('Gruppo'), 'orderby' => 'gruppo'),
            // regola: ($this->context->employee->id == 2 ? 'total_paid_real' : 'total_products')
            //'total_products' => array('title' => $this->l('T.Netto'), 'orderby' => 'total_products'),
            'total_paid_tax_incl' => array('title' => $this->l('Total'), 'width' => 70, 'align' => 'right', 'type' => 'price', 'currency' => true),
            'payment' => array( 'title' => $this->l('Payment'), 'width' => 100),
            'osname' => array('title' => $this->l('Status'), 'width' => 280),
            'date_add' => array('title' => $this->l('Date'), 'width' => 130, 'align' => 'right', 'type' => 'datetime'),
        );
    }

    protected function initCartList()
    {
        $this->fields_list['carts'] = (array(
            'id_cart' => array('title' => $this->l('ID'), 'orderby' => 'id_order'),
            'prev' => array('title' => $this->l('Prev.?')),
            'conv' => array('title' => $this->l('Conv.?')),
            'letto' => array('title' => $this->l('Letto?')),
            'id_order' => array('title' => $this->l('N.ORD.')),
            'customer' => array('title' => $this->l('Cliente'), 'orderby' => 'customer'),
            'name' => array('title' => $this->l('Oggetto'), 'orderby' => 'name'),
            'created_by' => array('title' => $this->l('Creato da'), 'orderby' => 'created_by'),
            'in_carico' => array('title' => $this->l('In carico a'), 'orderby' => 'in_carico'),
            'date_add' => array('title' => $this->l('Data'), 'type' => 'datetime', 'orderby' => 'date_add')
        ));
    }

    protected function initTicketList()
    {
        $this->fields_list['tickets'] = (array(
			'ticket' => array('title' => $this->l('ID')),
			'contact' =>  array('title' => $this->l('Tipo')),
			'customer' => array('title' => $this->l('Cliente')),
			'stato' => array('title' => $this->l('Status')),
			'data_act' => array('title' => $this->l('Aperto il')
		)));
    }

    protected function initBdlList()
    {
        $this->fields_list['bdl'] = (array(
            'ticket' => array('title' => $this->l('ID'), 'orderby' => 'a.id_bdl'),
            'customer' => array('title' => $this->l('Cliente')),
            'stato' => array('title' => $this->l('Status')),
            'data_richiesta' => array('title' => $this->l('Richiesto il')
        )));
    }

    public function postProcess()
    {
        $this->context = Context::getContext();
        $this->query = trim(Tools::getValue('bo_query'));
        $searchType = (int)Tools::getValue('bo_search_type');

        if(Tools::getIsset('note_persone'))
			$searchType = 2;

        if($this->context->employee->id_profile == 7) // Agente
			$searchType = 8;

        // Correggere: assegnare datatable a products

        /* Handle empty search field */
        if (!empty($this->query) || (empty($this->query) && (Tools::getValue('auto_marca_src') != 0 || Tools::getValue('auto_serie_src') != 0 || Tools::getValue('auto_categoria_src') != 0 || Tools::getValue('auto_fornitore_src') != 0))) {
            if (!$searchType && strlen($this->query) > 1) {
                $this->searchFeatures();
            }

            /* Product research */
            if (!$searchType || $searchType == 1) {
                /* Handle product ID */
                /*if ($searchType == 1 && (int)$this->query && Validate::isUnsignedInt((int)$this->query)) {
                    if (($product = new Product($this->query)) && Validate::isLoadedObject($product)) {
                        Tools::redirectAdmin('index.php?tab=AdminProducts&id_product='.(int)($product->id).'&token='.Tools::getAdminTokenLite('AdminProducts'));
                    }
                }*/

                /* Normal catalog search */
                $this->searchCatalog();
            }

            /* Bundles research -> correggere e implementare se usiamo i bundle 1.4 */
			/*if (!$searchType && !empty($this->query))
			{
				$this->fieldsDisplay['bundles'] = (array(
					'ID' => array('title' => $this->l('ID'), 'orderby' => 'p.id_product'),
					'image' => array('title' => $this->l('Photo')),
					'name' => array('title' => $this->l('Name'), 'orderby' => 'pl.name'),
					'reference' => array('title' => $this->l('Reference'), 'orderby' => 'p.reference'),
					'priceCLI' => array('title' => $this->l('Price CLI'), 'orderby' => 'p.price'),
					'priceRIV' => array('title' => $this->l('PriceRIV'), 'orderby' => 'p.listino'),
					'date_add' => array('title' => $this->l('Added on'), 'type' => 'datetime', 'orderby' => 'p.date_add'),
					'date_upd' => array('title' => $this->l('Updated on'), 'type' => 'datetime', 'orderby' => 'p.date_upd'),
					'status' => array('title' => $this->l('Status')),
					'action' => array('title' => $this->l('Actions'))
				));

				$this->searchBundles($this->query);
			}*/
			
            /* Tickets research */
			if (!$searchType && !empty($this->query) && ((strtolower(substr($this->query, 0, 4)) == 'todo') || (ctype_alpha((strtolower(substr($this->query, 0, 1)))) && is_numeric(strtolower(substr($this->query, 1, -1))))))
			{
				$this->searchTickets($this->query);
			}
			
            /* BDL research */
			if (!$searchType && !empty($this->query) && (strtolower(substr($this->query, 0, 3)) == 'bdl'))
			{
				$this->searchBDL($this->query);
			}

            /* Customer */
            if (!$searchType || $searchType == 2 || $searchType == 6 || $searchType == 7 || $searchType == 8) {
                if (!$searchType || $searchType == 2) {
                    /* Handle customer ID */
                    /*if ($searchType && (int)$this->query && Validate::isUnsignedInt((int)$this->query)) {
                        if (($customer = new Customer($this->query)) && Validate::isLoadedObject($customer)) {
                            Tools::redirectAdmin('index.php?tab=AdminCustomers&id_customer='.(int)$customer->id.'&viewcustomer'.'&token='.Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$this->context->employee->id));
                        }
                    }*/

                    /* Normal customer search */
                    $this->searchCustomer();
                }

                if ($searchType == 6) {
                    $this->searchIP();
                }
                
                if ($searchType == 7) {
                    $this->searchPersone();
                }

                if ($searchType == 8) {
					$this->searchCustomerPI();
				}
            }

            /* Order */
            if (!$searchType || $searchType == 3) {
                if (Validate::isUnsignedInt(trim($this->query)) && (int)$this->query && ($order = new Order((int)$this->query)) && Validate::isLoadedObject($order)) {
                    if ($searchType == 3) {
                        Tools::redirectAdmin('index.php?tab=AdminOrders&id_order='.(int)$order->id.'&vieworder'.'&token='.Tools::getAdminTokenLite('AdminOrders'));
                    } else {
                        $row = get_object_vars($order);
                        $row['id_order'] = $row['id'];
                        $customer = $order->getCustomer();
                        $row['customer'] = $customer->firstname.' '.$customer->lastname;
                        $order_state = $order->getCurrentOrderState();
                        $row['osname'] = $order_state->name[$this->context->language->id];
                        $this->_list['orders'] = array($row);
                    }
                } else {
                    $orders = Order::getByReference($this->query);
                    $nb_orders = count($orders);
                    if ($nb_orders == 1 && $searchType == 3) {
                        Tools::redirectAdmin('index.php?tab=AdminOrders&id_order='.(int)$orders[0]->id.'&vieworder'.'&token='.Tools::getAdminTokenLite('AdminOrders'));
                    } elseif ($nb_orders) {
                        $this->_list['orders'] = array();
                        foreach ($orders as $order) {
                            /** @var Order $order */
                            $row = get_object_vars($order);
                            $row['id_order'] = $row['id'];
                            $customer = $order->getCustomer();
                            $row['customer'] = $customer->firstname.' '.$customer->lastname;
                            $order_state = $order->getCurrentOrderState();
                            $row['osname'] = $order_state->name[$this->context->language->id];
                            $this->_list['orders'][] = $row;
                        }
                    } elseif ($searchType == 3) {
                        $this->errors[] = Tools::displayError('No order was found with this ID:').' '.Tools::htmlentitiesUTF8($this->query);
                    }
                }
            }

            /* Invoices */
            if ($searchType == 4) {
                if (Validate::isOrderInvoiceNumber($this->query) && ($invoice = OrderInvoice::getInvoiceByNumber($this->query))) {
                    // Correggere? Nella 1.4 era: Tools::redirectAdmin('pdf.php?id_order='.(int)($invoice['id_order']).'&pdf');
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminPdf').'&submitAction=generateInvoicePDF&id_order='.(int)($invoice->id_order));
                }
                $this->errors[] = Tools::displayError('No invoice was found with this ID:').' '.Tools::htmlentitiesUTF8($this->query);
            }

            /* Cart */
            if (!$searchType || $searchType == 5) {
                // Correggere: Chiedere a federico a cosa serve
                if(strlen($query) > 10 && substr_count($query, '-') > 1)
                {
                    $id_cart_new = Db::getInstance()->getValue('SELECT id_cart FROM '._DB_PREFIX_.'cart WHERE name LIKE "%'.$query.'%"');
                    $cart = new Cart($id_cart_new);
                    Tools::redirectAdmin('index.php?tab=AdminCarts&id_cart='.(int)($cart->id).'&viewcart'.'&token='.Tools::getAdminToken('AdminCarts'.(int)(Tab::getIdFromClassName('AdminCarts')).(int)$this->context->employee->id));
                }

                if ((int)$this->query && Validate::isUnsignedInt((int)$this->query) && ($cart = new Cart($this->query)) && Validate::isLoadedObject($cart)) {
                    Tools::redirectAdmin('index.php?tab=AdminCarts&id_cart='.(int)($cart->id).'&viewcart'.'&token='.Tools::getAdminToken('AdminCarts'.(int)(Tab::getIdFromClassName('AdminCarts')).(int)$this->context->employee->id));
                }
                else {
                    if($searchType == 5 && !$this->searchCarts())
                        $this->errors[] = Tools::displayError('No cart was found with this ID:').' '.Tools::htmlentitiesUTF8($this->query);
                    else
                        $this->searchCarts();
                }
            }
            /* IP */
            // 6 - but it is included in the customer block

            /* Module search */
            if (!$searchType || $searchType == 7) {
                /* Handle module name */
                if ($searchType == 7 && Validate::isModuleName($this->query) and ($module = Module::getInstanceByName($this->query)) && Validate::isLoadedObject($module)) {
                    Tools::redirectAdmin('index.php?tab=AdminModules&tab_module='.$module->tab.'&module_name='.$module->name.'&anchor='.ucfirst($module->name).'&token='.Tools::getAdminTokenLite('AdminModules'));
                }

                /* Normal catalog search */
                $this->searchModule();
            }
        }
        $this->display = 'view';
    }

    public function renderView()
    {
        $this->tpl_view_vars['query'] = Tools::safeOutput($this->query);
        $this->tpl_view_vars['show_toolbar'] = true;

        if (count($this->errors)) {
            return AdminController::renderView();
        } else {
            $nb_results = 0;
            foreach ($this->_list as $list) {
                if ($list != false) {
                    $nb_results += count($list);
                }
            }
            $this->tpl_view_vars['nb_results'] = $nb_results;

            /* CORREGGERE: ricontrollare tutti gli if, forse manca qualcosa */
            if (isset($this->_list['features']) && count($this->_list['features'])) {
                $this->tpl_view_vars['features'] = $this->_list['features'];
            }
            if (isset($this->_list['categories']) && count($this->_list['categories'])) {
                $categories = array();
                foreach ($this->_list['categories'] as $category) {
                    $categories[] = getPath($this->context->link->getAdminLink('AdminCategories', false), $category['id_category']);
                }
                $this->tpl_view_vars['categories'] = $categories;
            }
            if (isset($this->_list['products']) && count($this->_list['products'])) { // TUTTO DA CORREGGERE
                $view = '';
                $this->initProductList();

                $helper = new HelperList();
                $helper->shopLinkType = '';
                $helper->simple_header = true;
                $helper->identifier = 'id_product';
                $helper->actions = array('edit');
                $helper->show_toolbar = false;
                $helper->table = 'product';
                $helper->currentIndex = $this->context->link->getAdminLink('AdminProducts', false);

                $query = trim(Tools::getValue('bo_query'));
                $searchType = (int)Tools::getValue('bo_search_type');

                if ($query) {
                    $helper->currentIndex .= '&bo_query='.$query.'&bo_search_type='.$searchType;
                }

                $helper->token = Tools::getAdminTokenLite('AdminProducts');

                if ($this->_list['products']) {
                    $view = $helper->generateList($this->_list['products'], $this->fields_list['products']);
                }

                $this->tpl_view_vars['products'] = $view;
            }
            // Manca: '.($cookie->profile == 7 ? '' : '<a href="'.$currentIndex.'?tab=AdminSearch&bo_query='.$query.'&token='.$this->token.'&note_persone=yes">Clicca qui se vuoi cercare anche nelle note e nelle persone</a> (ricerca più lenta)<br /><br />').'
            if (isset($this->_list['customers']) && count($this->_list['customers'])) {
                $view = '';
                $this->initCustomerList();

                $helper = new HelperList();
                $helper->shopLinkType = '';
                $helper->simple_header = true;
                $helper->identifier = 'id_customer';
                $helper->actions = array('edit', 'view');
                $helper->show_toolbar = false;
                $helper->table = 'customer';
                $helper->currentIndex = $this->context->link->getAdminLink('AdminCustomers', false);
                $helper->token = Tools::getAdminTokenLite('AdminCustomers');

                if ($this->_list['customers']) {
                    foreach ($this->_list['customers'] as $key => $val) {
                        $this->_list['customers'][$key]['orders'] = Order::getCustomerNbOrders((int)$val['id_customer']);
                    }
                    $view = $helper->generateList($this->_list['customers'], $this->fields_list['customers']);
                }
                $this->tpl_view_vars['customers'] = $view;
            }
            if (isset($this->_list['orders']) && count($this->_list['orders'])) { // CORREGGERE!
                $view = '';
                $this->initOrderList();

                $helper = new HelperList();
                $helper->shopLinkType = '';
                $helper->simple_header = true;
                $helper->identifier = 'id_order';
                $helper->actions = array('view');
                $helper->show_toolbar = false;
                $helper->table = 'order';
                $helper->currentIndex = $this->context->link->getAdminLink('AdminOrders', false);
                $helper->token = Tools::getAdminTokenLite('AdminOrders');

                if ($this->_list['orders']) {
                    $view = $helper->generateList($this->_list['orders'], $this->fields_list['orders']);
                }
                $this->tpl_view_vars['orders'] = $view;
            }

            /* Correggere: mancano i tickets */
            if (isset($this->_list['carts']) && count($this->_list['carts'])) { // CORREGGERE!
                $view = '';
                $this->initCartList();

                $helper = new HelperList();
                $helper->shopLinkType = '';
                $helper->simple_header = true;
                $helper->identifier = 'id_cart';
                $helper->actions = array('view');
                $helper->show_toolbar = false;
                $helper->table = 'cart';
                $helper->currentIndex = $this->context->link->getAdminLink('AdminCarts', false);
                $helper->token = Tools::getAdminTokenLite('AdminCarts');

                if ($this->_list['carts']) {
                    $view = $helper->generateList($this->_list['carts'], $this->fields_list['carts']);
                }
                $this->tpl_view_vars['carts'] = $view;
            }
            if (isset($this->_list['bdl']) && count($this->_list['bdl'])) { // CORREGGERE!
                $view = '';
                $this->initBdlList();

                $helper = new HelperList();
                $helper->shopLinkType = '';
                $helper->simple_header = true;
                $helper->identifier = 'id_bdl';
                $helper->actions = array('view');
                $helper->show_toolbar = false;
                $helper->table = 'bdl';
                $helper->currentIndex = $this->context->link->getAdminLink('AdminBDL', false);
                $helper->token = Tools::getAdminTokenLite('AdminBDL');

                if ($this->_list['bdl']) {
                    $view = $helper->generateList($this->_list['bdl'], $this->fields_list['bdl']);
                }
                $this->tpl_view_vars['bdl'] = $view;
            }
            /* Correggere: aggiungere bundles se usiamo quelli 1.4 */

            if (isset($this->_list['modules']) && count($this->_list['modules'])) {
                $this->tpl_view_vars['modules'] = $this->_list['modules'];
            }
            if (isset($this->_list['addons']) && count($this->_list['addons'])) {
                $this->tpl_view_vars['addons'] = $this->_list['addons'];
            }

            return AdminController::renderView();
        }
    }
}