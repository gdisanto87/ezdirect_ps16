<?php

class AttachmentController extends AttachmentControllerCore
{
    public function postProcess()
    {
        /* OVERRIDE -> controllare se va fatto o no e testare */
        // canonicalRedirection();
        /* FINE OVERRIDE */

        $a = new Attachment(Tools::getValue('id_attachment'), $this->context->language->id);
        if (!$a->id) {
            Tools::redirect('index.php');
        }

        Hook::exec('actionDownloadAttachment', array('attachment' => &$a));

        if (ob_get_level() && ob_get_length() > 0) {
            ob_end_clean();
        }

        header('Content-Transfer-Encoding: binary');
        header('Content-Type: '.$a->mime);
        header('Content-Length: '.filesize(_PS_DOWNLOAD_DIR_.$a->file));
        header('Content-Disposition: attachment; filename="'.utf8_decode($a->file_name).'"');
        @set_time_limit(0);
        readfile(_PS_DOWNLOAD_DIR_.$a->file);
        exit;
    }

    /* OVERRIDE */
    /*function canonicalRedirection()
	{
		$attachment = Tools::getValue('id_attachment');
		$file_name = Db::getInstance()->getValue("SELECT file_name FROM "._DB_PREFIX_."attachment WHERE id_attachment = ".$attachment."");
		$link = new Link();
		$canonicalURL = $link->getAttachmentLink($attachment, $file_name);
		
		if($file_name == '')
		{
			header('HTTP/1.0 404 Error');
		}
		
		if (!preg_match('/^'.Tools::pRegexp($canonicalURL, '/').'([&?].*)?$/', (isset($_SERVER['HTTPS']) ? "https" : "http").'://'.$_SERVER['HTTPS_HOST'].$_SERVER['REQUEST_URI']))
		{
			// header('HTTP/1.0 301 Moved');
			// header('Cache-Control: no-cache');
			// if (defined('_PS_MODE_DEV_') AND _PS_MODE_DEV_)
			// 	die('[Debug] This page has moved<br />Please use the following URL instead: <a href="'.$canonicalURL.'">'.$canonicalURL.'</a>');
			// Tools::redirectLink($canonicalURL);
		}
	}*/
    /* FINE OVERRIDE */
}