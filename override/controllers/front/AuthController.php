<?php

class AuthController extends AuthControllerCore
{
    protected function assignCountries()
    {
		
        $this->id_country = (int)Tools::getCountry();
        //if (Configuration::get('PS_RESTRICT_DELIVERED_COUNTRIES')) {
        //    $countries = Carrier::getDeliveredCountries($this->context->language->id, true, true);
        //} else {
            $countries = Country::getCountries($this->context->language->id, true);
        // }
		
		
        $this->context->smarty->assign(array(
                'countries' => $countries,
                'PS_REGISTRATION_PROCESS_TYPE' => Configuration::get('PS_REGISTRATION_PROCESS_TYPE'),
                'sl_country' => (int)$this->id_country,
                'vat_management' => Configuration::get('VATNUMBER_MANAGEMENT')
            ));
    }
}