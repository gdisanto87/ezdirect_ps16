<?php
/*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class OrderController extends OrderControllerCore
{
  
	public function initContent()
    {
        parent::initContent();

        if (Tools::isSubmit('ajax') && Tools::getValue('method') == 'updateExtraCarrier') {
            // Change virtualy the currents delivery options
            $delivery_option = $this->context->cart->getDeliveryOption();
            $delivery_option[(int)Tools::getValue('id_address')] = Tools::getValue('id_delivery_option');
            $this->context->cart->setDeliveryOption($delivery_option);
            $this->context->cart->save();
            $return = array(
                'content' => Hook::exec(
                    'displayCarrierList',
                    array(
                        'address' => new Address((int)Tools::getValue('id_address'))
                    )
                )
            );
            $this->ajaxDie(Tools::jsonEncode($return));
        }

        if ($this->nbProducts) {
            $this->context->smarty->assign('virtual_cart', $this->context->cart->isVirtualCart());
        }

        if (!Tools::getValue('multi-shipping')) {
            $this->context->cart->setNoMultishipping();
        }
		
		if ($this->context->customer->id) {
		
			
		
			$cus = $this->context->customer->id;
			
			$row = Db::getInstance()->getRow("SELECT is_company, tax_code, vat_number FROM "._DB_PREFIX_."customer WHERE id_customer = $cus");
			
			$qryph = ("SELECT id_address, address1, postcode, city, id_country, id_state, phone, phone_mobile, id_country FROM "._DB_PREFIX_."address WHERE fatturazione = 1 AND active = 1 AND deleted = 0 AND id_customer = ".$cus."");
			
			$rph = Db::getInstance()->getRow($qryph);
			
			$has_address = Db::getInstance()->getValue("SELECT count(id_address) FROM "._DB_PREFIX_."address WHERE fatturazione = 1 AND active = 1 AND deleted = 0 AND id_customer = ".$cus."");
			
			$blacklist = Db::getInstance()->getValue("SELECT blacklist FROM customer_amministrazione WHERE id_customer = ".$cus."");
			
			 $this->context->smarty->assign(array(
						'blacklist' => $blacklist,
						));
			if(!$has_address || $has_address == 0) {
			
				 $this->context->smarty->assign(array(
						'cfcheck' => 0,
						'check_address' => 1,
						));
			}
			
			if($rph['id_country'] == 10) {
			
				 $this->context->smarty->assign(array(
					'check_country' => 0,
					));
					
				if($row['is_company'] == 1) {
				
			
					if(strlen($row['tax_code']) != 16) {
					
					
						if(strlen($row['tax_code']) == 11) {
						
							 $this->context->smarty->assign(array(
							'cfcheck' => 1,
							));
						
						}	
						else {
						
							 $this->context->smarty->assign(array(
							'cfcheck' => 0,
							'check_tx' => 1,
							));
						}
				
					}
					if(strlen($row['vat_number']) != 11) {
				
						 $this->context->smarty->assign(array(
						'cfcheck' => 0,
						'check_vat' => 1,
						));
			
					}
				
					if(trim($rph['phone']) == "" && trim($rph['phone_mobile']) == "") {
					
						 $this->context->smarty->assign(array(
						'cfcheck' => 0,
						'check_phone' => 1,
						));
			
					}
					
					if(trim($rph['address1']) == "") {
					
						 $this->context->smarty->assign(array(
						'cfcheck' => 0,
						'check_address' => 1,
						));
			
					}
					
					if(trim($rph['postcode']) == "") {
					
						 $this->context->smarty->assign(array(
						'cfcheck' => 0,
						'check_postcode' => 1,
						));
			
					}
					
					if(trim($rph['city']) == "") {
					
						 $this->context->smarty->assign(array(
						'cfcheck' => 0,
						'check_city' => 1,
						));
			
					}
					
					if($rph['id_state'] == "") {
					
						 $this->context->smarty->assign(array(
						'cfcheck' => 0,
						'check_state' => 1,
						));
			
					}
					
					if (($rph['phone'] != "" || $rph['phone_mobile'] != "") && trim($rph['address1']) != '' && trim($rph['postcode']) != '' && trim($rph['city']) != '' && $rph['id_state'] != '' && trim($row['tax_code']) != "" && strlen($row['vat_number']) == 11 && (strlen($row['tax_code']) == 11 || strlen($row['tax_code']) == 16)){
						 $this->context->smarty->assign(array(
						'cfcheck' => 1,
						));
			
					}
		
			
				}	else if($row['is_company'] == 0) {
				
					if(strlen($row['tax_code']) != 16) {
				
						 $this->context->smarty->assign(array(
						'cfcheck' => 0,
						'check_tx' => 1,
						));
					
					}	
			
			
					if($rph['phone'] == "" && $rph['phone_mobile'] == "") {
			
						 $this->context->smarty->assign(array(
						'cfcheck' => 0,
						'check_phone' => 1,
					));
					
					if($rph['address1'] == "") {
					
						 $this->context->smarty->assign(array(
						'cfcheck' => 0,
						'check_address' => 1,
						));
			
					}
					
					if($rph['postcode'] == "") {
					
						 $this->context->smarty->assign(array(
						'cfcheck' => 0,
						'check_postcode' => 1,
						));
			
					}
					
					if($rph['city'] == "") {
					
						 $this->context->smarty->assign(array(
						'cfcheck' => 0,
						'check_city' => 1,
						));
			
					}
					
					if($rph['id_state'] == "") {
					
						 $this->context->smarty->assign(array(
						'cfcheck' => 0,
						'check_state' => 1,
						));
			
					}
		
			
				}
				if (($rph['phone'] != "" || $rph['phone_mobile'] != "") && $rph['address1'] != '' && $rph['city'] != '' && $rph['postcode'] != '' && $rph['id_state'] != '' && $row['tax_code'] != "" && strlen($row['tax_code']) == 16){
					 $this->context->smarty->assign(array(
					'cfcheck' => 1,
					
					));
			
				}
		
			
				}
					else {
					
					
		
				}	
			}
			else {
			
				if($rph['id_country'] == "") {
					
						 $this->context->smarty->assign(array(
						'cfcheck' => 0,
						'check_country' => 1,
						));
			
				}
				
				if($rph['phone'] == "" && $rph['phone_mobile'] == "") {
					
						 $this->context->smarty->assign(array(
						'cfcheck' => 0,
						'check_phone' => 1,
						));
			
					}
					
					if($rph['address1'] == "") {
					
						 $this->context->smarty->assign(array(
						'cfcheck' => 0,
						'check_address' => 1,
						));
			
					}
					
					if($rph['postcode'] == "") {
					
						 $this->context->smarty->assign(array(
						'cfcheck' => 0,
						'check_postcode' => 1,
						));
			
					}
					
					if($rph['city'] == "") {
					
						 $this->context->smarty->assign(array(
						'cfcheck' => 0,
						'check_city' => 1,
						));
			
					}
					
					if (($rph['phone'] != "" || $rph['phone_mobile'] != "") && $rph['id_country'] != '' && $rph['address1'] != '' && $rph['postcode'] != '' && $rph['city'] != ''){
						 $this->context->smarty->assign(array(
						'cfcheck' => 1,
						));
			
					}
					
				else
				{				
					 $this->context->smarty->assign(array(
					'cfcheck' => 1,
					));
				}
			}
			
			 $this->context->smarty->assign(array(
				'id_address' => $rph['id_address'],
				));
				
			/*	
			$criteo_shopping_cart_items = "";
			$criteo_products = Db::getInstance()->executeS("SELECT * FROM cart_product WHERE id_cart = ".$cookie->id_cart."");
			foreach($criteo_products as $prd)
			{
				if($prd['price'] == 0)
					$price = Product::trovaMigliorPrezzo($prd['id_product'], $cookie->id_default_group, $prd['quantity']);
				else
					$price = $prd['price'];
				
				$cr_ref = Db::getInstance()->getValue('SELECT reference FROM product WHERE id_product = '.$prd['id_product']);
				
				$criteo_shopping_cart_items .= '{ id: "'.$prd['id_product'].'", price: '.$price.', quantity: '.$prd['quantity'].'},';
				
			}	
			
			 $this->context->smarty->assign(array(
			'criteo_shopping_cart_items' => $criteo_shopping_cart_items,
			));
			*/
		}
		
		else {
				
				 $this->context->smarty->assign(array(
				'cfcheck' => 2,
				));
	
		}

        // Check for alternative payment api
        $is_advanced_payment_api = (bool)Configuration::get('PS_ADVANCED_PAYMENT_API');

        // 4 steps to the order
        switch ((int)$this->step) {

            case OrderController::STEP_SUMMARY_EMPTY_CART:
                $this->context->smarty->assign('empty', 1);
                $this->setTemplate(_PS_THEME_DIR_.'shopping-cart.tpl');
            break;

            case OrderController::STEP_ADDRESSES:
                $this->_assignAddress();
                $this->processAddressFormat();
                if (Tools::getValue('multi-shipping') == 1) {
                    $this->_assignSummaryInformations();
                    $this->context->smarty->assign('product_list', $this->context->cart->getProducts());
                    $this->setTemplate(_PS_THEME_DIR_.'order-address-multishipping.tpl');
                } else {
                    $this->setTemplate(_PS_THEME_DIR_.'order-address.tpl');
                }
            break;

            case OrderController::STEP_DELIVERY:
                if (Tools::isSubmit('processAddress')) {
                    $this->processAddress();
                }
                $this->autoStep();
                $this->_assignCarrier();
                $this->setTemplate(_PS_THEME_DIR_.'order-carrier.tpl');
            break;

            case OrderController::STEP_PAYMENT:
                // Check that the conditions (so active) were accepted by the customer
                $cgv = Tools::getValue('cgv') || $this->context->cookie->check_cgv;

                if ($is_advanced_payment_api === false && Configuration::get('PS_CONDITIONS')
                    && (!Validate::isBool($cgv) || $cgv == false)) {
                    Tools::redirect('index.php?controller=order&step=2');
                }

                if ($is_advanced_payment_api === false) {
                    Context::getContext()->cookie->check_cgv = true;
                }

                // Check the delivery option is set
                if ($this->context->cart->isVirtualCart() === false) {
                    if (!Tools::getValue('delivery_option') && !Tools::getValue('id_carrier') && !$this->context->cart->delivery_option && !$this->context->cart->id_carrier) {
                        Tools::redirect('index.php?controller=order&step=2');
                    } elseif (!Tools::getValue('id_carrier') && !$this->context->cart->id_carrier) {
                        $deliveries_options = Tools::getValue('delivery_option');
                        if (!$deliveries_options) {
                            $deliveries_options = $this->context->cart->delivery_option;
                        }

                        foreach ($deliveries_options as $delivery_option) {
                            if (empty($delivery_option)) {
                                Tools::redirect('index.php?controller=order&step=2');
                            }
                        }
                    }
                }

                $this->autoStep();

                // Bypass payment step if total is 0
                if (($id_order = $this->_checkFreeOrder()) && $id_order) {
                    if ($this->context->customer->is_guest) {
                        $order = new Order((int)$id_order);
                        $email = $this->context->customer->email;
                        $this->context->customer->mylogout(); // If guest we clear the cookie for security reason
                        Tools::redirect('index.php?controller=guest-tracking&id_order='.urlencode($order->reference).'&email='.urlencode($email));
                    } else {
                        Tools::redirect('index.php?controller=history');
                    }
                }
                $this->_assignPayment();

                if ($is_advanced_payment_api === true) {
                    $this->_assignAddress();
                }

                // assign some informations to display cart
                $this->_assignSummaryInformations();
                $this->setTemplate(_PS_THEME_DIR_.'order-payment.tpl');
            break;

            default:
                $this->_assignSummaryInformations();
                $this->setTemplate(_PS_THEME_DIR_.'shopping-cart.tpl');
            break;
        }
    }
   
}
