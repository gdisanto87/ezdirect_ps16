<?php

class Order extends OrderCore
{
    public $acconto;
    public $tax_regime;
    public $total_commissions;
    public $csv; // bool
    public $analytics; // bool
    public $ddt; // bool
    public $spring; // bool
    public $check_pi; // bool
    public $check_esolver; // bool
    public $pag_amz_ric; // bool
    public $verificato; // bool
    public $verificato_da;
    public $rif_ordine;
    public $cig;
    public $cup;
    public $ipa;
    public $data_ordine_mepa;
    public $no_feedback;
    public $no_feedback_cause;

    public static $definition = array(
        'table' => 'orders',
        'primary' => 'id_order',
        'fields' => array(
            'id_address_delivery' =>        array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_address_invoice' =>        array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_cart' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_currency' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_shop_group' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_shop' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_lang' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_customer' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_carrier' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'current_state' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'secure_key' =>                array('type' => self::TYPE_STRING, 'validate' => 'isMd5'),
            'payment' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true),
            'module' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isModuleName', 'required' => true),
            'recyclable' =>                array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'gift' =>                        array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'gift_message' =>                array('type' => self::TYPE_STRING, 'validate' => 'isMessage'),
            'mobile_theme' =>                array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'total_discounts' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'total_discounts_tax_incl' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'total_discounts_tax_excl' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'total_paid' =>                array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice', 'required' => true),
            'total_paid_tax_incl' =>        array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'total_paid_tax_excl' =>        array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'total_paid_real' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice', 'required' => true),
            'total_products' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice', 'required' => true),
            'total_products_wt' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice', 'required' => true),
            'total_shipping' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'total_shipping_tax_incl' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'total_shipping_tax_excl' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'carrier_tax_rate' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'total_wrapping' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'total_wrapping_tax_incl' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'total_wrapping_tax_excl' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'round_mode' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'round_type' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'shipping_number' =>            array('type' => self::TYPE_STRING, 'validate' => 'isTrackingNumber'),
            'conversion_rate' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
            'invoice_number' =>            array('type' => self::TYPE_INT),
            'delivery_number' =>            array('type' => self::TYPE_INT),
            'invoice_date' =>                array('type' => self::TYPE_DATE),
            'delivery_date' =>                array('type' => self::TYPE_DATE),
            'valid' =>                        array('type' => self::TYPE_BOOL),
            'reference' =>                    array('type' => self::TYPE_STRING),
            'date_add' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            // Override
            'acconto' =>                    array('type' => self::TYPE_FLOAT/*, 'validate' => 'isFloat'*/), // oppure isPrice?
            'tax_regime' =>                 array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'total_commissions' =>          array('type' => self::TYPE_FLOAT/*, 'validate' => 'isFloat'*/), // oppure isPrice?
            'csv' =>                        array('type' => self::TYPE_INT/*, 'validate' => 'isBool'*/), // perchè non BOOL qui e TINYINT nel DB?
            'analytics' =>                  array('type' => self::TYPE_INT/*, 'validate' => 'isBool'*/), // perchè non BOOL qui e TINYINT nel DB?
            'ddt' =>                        array('type' => self::TYPE_INT/*, 'validate' => 'isBool'*/), // perchè non BOOL qui e TINYINT nel DB?
            'spring' =>                     array('type' => self::TYPE_INT/*, 'validate' => 'isBool'*/), // perchè non BOOL qui e TINYINT nel DB?
            'check_pi' =>                   array('type' => self::TYPE_INT/*, 'validate' => 'isBool'*/), // perchè non BOOL qui e TINYINT nel DB?
            'check_esolver' =>              array('type' => self::TYPE_INT/*, 'validate' => 'isBool'*/), // perchè non BOOL qui e TINYINT nel DB?
            'pag_amz_ric' =>                array('type' => self::TYPE_INT/*, 'validate' => 'isBool'*/), // perchè non BOOL qui e TINYINT nel DB?
            'verificato' =>                 array('type' => self::TYPE_INT/*, 'validate' => 'isBool'*/), // perchè non BOOL qui e TINYINT nel DB?
            'verificato_da' =>              array('type' => self::TYPE_STRING), // perchè non INT?
            'rif_ordine' =>                 array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 70),
            'cig' =>                        array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 40),
            'cup' =>                        array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 40),
            'ipa' =>                        array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 40),
            'data_ordine_mepa' =>           array('type' => self::TYPE_DATE),
            'no_feedback' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'no_feedback_cause' =>          array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 300),
        ),
    );

    // Correggere: Vedi tutte le funzioni 1.4

    public function hasBeenShippedPartially()
	{
        // correggere: aggiungere PS_OS_SHIPPING_PART in ps_configuration
        return count($this->getHistory((int)$this->id_lang, false, false, Configuration::get('PS_OS_SHIPPING_PART')));
	}

    // Aggiunto controllo agenti
    public static function getCustomerOrders($id_customer, $show_hidden_status = false, Context $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }

        $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
        SELECT o.*, (SELECT SUM(od.`product_quantity`) FROM `'._DB_PREFIX_.'order_detail` od WHERE od.`id_order` = o.`id_order`) nb_products
        FROM `'._DB_PREFIX_.'orders` o
        '.($context->employee->id_profile == 7 ? ' JOIN '._DB_PREFIX_.'cart ca ON o.id_cart = ca.id_cart' : '').'
        WHERE o.`id_customer` = '.(int)$id_customer.
        Shop::addSqlRestriction(Shop::SHARE_ORDER).'
        '.($context->employee->id_profile == 7 ? ' AND (ca.id_employee = '.$context->employee->id.' OR ca.in_carico_a = '.$context->employee->id.')' : '').'
        GROUP BY o.`id_order`
        ORDER BY o.`date_add` DESC');
        if (!$res) {
            return array();
        }

        foreach ($res as $key => $val) {
            $res2 = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT os.`id_order_state`, osl.`name` AS order_state, os.`invoice`, os.`color` as order_state_color
				FROM `'._DB_PREFIX_.'order_history` oh
				LEFT JOIN `'._DB_PREFIX_.'order_state` os ON (os.`id_order_state` = oh.`id_order_state`)
				INNER JOIN `'._DB_PREFIX_.'order_state_lang` osl ON (os.`id_order_state` = osl.`id_order_state` AND osl.`id_lang` = '.(int)$context->language->id.')
			WHERE oh.`id_order` = '.(int)$val['id_order'].(!$show_hidden_status ? ' AND os.`hidden` != 1' : '').'
				ORDER BY oh.`date_add` DESC, oh.`id_order_history` DESC
			LIMIT 1');

            if ($res2) {
                $res[$key] = array_merge($res[$key], $res2[0]);
            }
        }
        return $res;
    }

    // getCustomerOrders con ticket e contratti assistenza
    public static function getCustomerOrdersWithTickets($id_customer, $show_hidden_status = false, Context $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }

        $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
        SELECT o.*, (SELECT SUM(od.`product_quantity`) FROM `'._DB_PREFIX_.'order_detail` od WHERE od.`id_order` = o.`id_order`) nb_products,
            co.id_contratto AS contract, ct.status as ticket
        FROM `'._DB_PREFIX_.'orders` o
        LEFT JOIN '._DB_PREFIX_.'contratto_assistenza co ON co.rif_ordine = o.id_order
		LEFT JOIN '._DB_PREFIX_.'customer_thread ct ON ct.riferimento = CONCAT("O", o.id_order)
        '.($context->employee->id_profile == 7 ? ' JOIN '._DB_PREFIX_.'cart ca ON o.id_cart = ca.id_cart' : '').'
        WHERE o.`id_customer` = '.(int)$id_customer.
        Shop::addSqlRestriction(Shop::SHARE_ORDER).'
        '.($context->employee->id_profile == 7 ? ' AND (ca.id_employee = '.$context->employee->id.' OR ca.in_carico_a = '.$context->employee->id.')' : '').'
        GROUP BY o.`id_order`
        ORDER BY o.`date_add` DESC');
        if (!$res) {
            return array();
        }

        foreach ($res as $key => $val) {
            $res2 = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT os.`id_order_state`, osl.`name` AS order_state, os.`invoice`, os.`color` as order_state_color
				FROM `'._DB_PREFIX_.'order_history` oh
				LEFT JOIN `'._DB_PREFIX_.'order_state` os ON (os.`id_order_state` = oh.`id_order_state`)
				INNER JOIN `'._DB_PREFIX_.'order_state_lang` osl ON (os.`id_order_state` = osl.`id_order_state` AND osl.`id_lang` = '.(int)$context->language->id.')
			WHERE oh.`id_order` = '.(int)$val['id_order'].(!$show_hidden_status ? ' AND os.`hidden` != 1' : '').'
				ORDER BY oh.`date_add` DESC, oh.`id_order_history` DESC
			LIMIT 1');

            if ($res2) {
                $res[$key] = array_merge($res[$key], $res2[0]);
            }
        }
        return $res;
    }

    // Correggere: testare override, non so se è giusto
    public function setProductPrices(&$row)
    {
        $tax_calculator = OrderDetail::getTaxCalculatorStatic((int)$row['id_order_detail']);
        $row['tax_calculator'] = $tax_calculator;
        $row['tax_rate'] = $tax_calculator->getTotalRate();

        $row['product_price'] = Tools::ps_round($row['unit_price_tax_excl'], 2);
        $row['product_price_wt'] = Tools::ps_round($row['unit_price_tax_incl'], 2);

        // Override
        if ($row['reduction_amount'] != 0) {
			$row['product_price'] = ($row['product_price'] - ($row['reduction_amount']));
            $row['product_price'] = Tools::ps_round($row['product_price'], 2);
        }

        $group_reduction = 1;
        if ($row['group_reduction'] > 0) {
            $group_reduction = 1 - $row['group_reduction'] / 100;
        }

        $row['product_price_wt_but_ecotax'] = $row['product_price_wt'] - $row['ecotax'];

        $row['total_wt'] = $row['total_price_tax_incl'];
        $row['total_price'] = $row['total_price_tax_excl'];
    }

    // Aggiunto join con image
    public function getProductsDetail()
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT od.*
		FROM `'._DB_PREFIX_.'order_detail` od
		LEFT JOIN `'._DB_PREFIX_.'product` p ON (p.id_product = od.product_id)
		LEFT JOIN `'._DB_PREFIX_.'product_shop` ps ON (ps.id_product = p.id_product AND ps.id_shop = od.id_shop)
        LEFT JOIN (SELECT * FROM `'._DB_PREFIX_.'image` WHERE cover = 1) image ON od.product_id = image.id_product
		WHERE od.`id_order` = '.(int)$this->id);
    }

    public function getOrderPDF($id_order, $id_customer)
    {
        $context = Context::getContext();

		$datiCliente = Db::getInstance()->getRow("
            SELECT c.*, c.tax_code as cf, c.vat_number as pi, a.*, s.iso_code AS provincia 
            FROM "._DB_PREFIX_."customer c 
            JOIN "._DB_PREFIX_."address a ON c.id_customer = a.id_customer 
            LEFT JOIN "._DB_PREFIX_."state s ON a.id_state = s.id_state 
            WHERE c.id_customer = ".$id_customer." AND a.fatturazione = 1 AND a.deleted = 0 AND a.active = 1
        ");	
		
		$datiOrdine = Db::getInstance()->getRow("
            SELECT * 
            FROM "._DB_PREFIX_."orders 
            WHERE id_order = ".$id_order." AND id_customer = ".$id_customer."
        ");	 
		
		$prodottiOrdine = Db::getInstance()->executeS("
            SELECT od.*, p.reference, pl.name as nome_prodotto 
            FROM "._DB_PREFIX_."order_detail od 
            JOIN "._DB_PREFIX_."product p ON od.product_id = p.id_product 
            JOIN "._DB_PREFIX_."product_lang pl ON od.product_id = pl.id_product 
            WHERE pl.id_lang = ".$context->language->id." AND od.id_order = ".$datiOrdine['id_order']."
        ");
				
		$indirizzoFatturazione = Db::getInstance()->getRow("
            SELECT a.*, s.iso_code AS provincia 
            FROM "._DB_PREFIX_."customer c 
            JOIN "._DB_PREFIX_."address a ON c.id_customer = a.id_customer 
            LEFT JOIN "._DB_PREFIX_."state s ON a.id_state = s.id_state 
            WHERE c.id_customer = ".$id_customer." AND a.id_address = ".$datiOrdine['id_address_invoice']."
        ");	
		
		$indirizzoSpedizione = Db::getInstance()->getRow("
            SELECT a.*, s.iso_code AS provincia 
            FROM "._DB_PREFIX_."customer c 
            JOIN "._DB_PREFIX_."address a ON c.id_customer = a.id_customer 
            LEFT JOIN "._DB_PREFIX_."state s ON a.id_state = s.id_state 
            WHERE c.id_customer = ".$id_customer." AND a.id_address = ".$datiOrdine['id_address_delivery']."
        ");	
		
		$id_country = Db::getInstance()->getValue("
            SELECT id_country 
            FROM "._DB_PREFIX_."address 
            WHERE id_address = ".$datiOrdine['id_address_delivery']."
        ");
		
		$iva = 0;
        $corriere = Db::getInstance()->getValue("SELECT name FROM "._DB_PREFIX_."carrier WHERE id_carrier = ".$datiOrdine['id_carrier']);
		
		$content = '<page>
            <div id="contenitore" style="width:770px; height:1070px; font-family: Arial, Verdana, Sans-Serif; display:block; position:relative; margin:0 auto">

                <div style="width:770px">
                    <img src="https://www.ezdirect.it/img/intestazione.jpg" alt="Ezdirect" title="Ezdirect"  style="display:block; margin:0px" />
                    
                </div>
                <table style="width:770px; font-size:11px; display:block; margin:0 auto">
                    <tr>
                    <td style="width:385px">
                    <strong style="font-size:16px">ezdirect srl - societ&agrave; con socio unico</strong><br />
                    Via Nerino Garbuio snc - 54038 Montignoso (MS)<br />
                    Tel: 0585 821163 - Fax: 0240708331<br />
                    P.IVA-C.F. 01164670455<br />
                    REA:118272 04-05-07 Reg.Imp.01164670455
                    </td>
                    <td style="width:385px">
                    <strong style="font-size:16px">Banca d\'appoggio</strong><br />
                    Banco Popolare, Ag. Montignoso<br />
                    ABI: 05034 CAB: 69948 CIN:T<br />
                    C/C 001454 SWIFT: BAPPIT21S44<br />
                    IBAN: IT58T0503469948000000001454
                    </td>
                    </tr>
                </table>
                <br />
                <table style="width:770px; font-size:16px; display:block; margin:0 auto">
                    <tr>
                    <td style="width:385px">
                    <strong>SPETT.LE</strong><br />
                    <strong>'.($datiCliente['is_company'] == 1 ? $datiCliente['company'] : $datiCliente['firstname'].' '.$datiCliente['lastname']).'</strong><br />
                    <strong>'.($datiCliente['is_company'] == 1 ? "PI: ".$datiCliente['pi']." - " : "").' CF: '.$datiCliente['cf'].'</strong>
                    <br /><br />
                    <strong>Indirizzo di fatturazione</strong><br />
                    '.($indirizzoFatturazione['company'] == '' ? $indirizzoFatturazione['firstname']." ".$indirizzoFatturazione['lastname'] : $indirizzoFatturazione['company']).'<br />
                    '.($indirizzoFatturazione['company'] != '' ? 'Rif.: '.$indirizzoFatturazione['firstname']." ".$indirizzoFatturazione['lastname'] : '').'<br />
                    '.$indirizzoFatturazione['address1'].'<br />'.$indirizzoFatturazione['postcode'].' '.$indirizzoFatturazione['city'].''.(empty($indirizzoFatturazione['provincia']) ? '' :  ' ('.$indirizzoFatturazione['provincia'].')').'
                    </td>
                    <td style="width:385px">
                    <br /><br /><br /><br />
                    <strong>Indirizzo di consegna</strong><br />
                    '.($indirizzoSpedizione['company'] == '' ? $indirizzoSpedizione['firstname']." ".$indirizzoSpedizione['lastname'] : $indirizzoSpedizione['company']).'<br />
                    '.($indirizzoSpedizione['company'] != '' ? 'Rif.: '.$indirizzoSpedizione['firstname']." ".$indirizzoSpedizione['lastname'] : '').'<br />
                    '.$indirizzoSpedizione['address1'].'<br />'.$indirizzoSpedizione['postcode'].' '.$indirizzoSpedizione['city'].''.(empty($indirizzoSpedizione['provincia']) ? '' :  ' ('.$indirizzoSpedizione['provincia'].')').'
                    </td>
                    </tr>
                </table>
                <h1 style="text-align:left; font-size:22px; font-weight:normal; display:block; width:210px; height:24px; background-color:#e6e6fa">
                    <em>ORDINE N. '.$datiOrdine['id_order'].'</em>
                </h1>
                <table style="width:715px; border-top:1px solid black; border-bottom:1px solid black; display:block; margin:0 auto">
                    <tr style="font-size:9px">
                    <td style="width:173px"><em>PARTITA IVA - CODICE FISCALE</em></td>
                    <td style="width:180px"><em>DATA ORDINE</em></td>
                    <td style="width:181px"><em>METODO DI PAGAMENTO</em></td>
                    <td style="width:181px"><em>METODO DI SPEDIZIONE</em></td>
                    
                    </tr>
                    <tr style="font-size:12px; font-weight:bold">
                    <td style="width:173px">'.$datiCliente['pi'].' '.$datiCliente['cf'].'</td>
                    <td style="width:180px">'.Tools::displayDate($datiOrdine['date_add'], (int)($context->language->id), true).'</td>
                    <td style="width:181px">'.($datiOrdine['payment']).'</td>
                    <td style="width:181px">'.$corriere.'</td>
                    </tr>
                </table>
                
                
                <br />
                <table style="width:720px; border-top:1px solid black; display:block; margin:0 auto">
                    <tr style="font-size:10px; height:22px; width:720px; border-bottom:1px solid black; color:#656565; font-weight:bold">
                    <td style="width:100px">CODICE ARTICOLO</td>
                    <td style="width:342px">DESCRIZIONE ARTICOLO - ANNOTAZIONI</td>
                    <td style="width:40px">UM</td>
                    <td style="width:58px; text-align:right">QUANTITA\'</td>
                    <td style="width:58px; text-align:right">PREZZO</td>
                    <td style="width:58px; text-align:right">IMPORTO</td>
                    <td style="width:42px; text-align:right">C.I.</td>
                    </tr>
                </table>
                <table style="width:720px; border-top:1px solid black; display:block; margin:0 auto">
        ';
			
        $importo = 0;
        $tax_regime = Db::getInstance()->getValue("SELECT tax_regime FROM "._DB_PREFIX_."customer WHERE id_customer = ".$id_customer."");
        if($tax_regime == 0) {
            $id_tax = 1;
        }
        else {
            $id_tax = $tax_regime;
        }
        $tax_rate = Db::getInstance()->getValue("SELECT rate FROM "._DB_PREFIX_."tax WHERE id_tax = ".$id_tax."");
        
        if($tax_regime == 1) {
            $tax_rate = 0;
        }
        
        foreach ($prodottiOrdine as $prodotto) {
            
            if($prodotto['reduction_amount'] > 0)
            {
                $importoprodotto = ($prodotto['product_price']-($prodotto['reduction_amount']))*$prodotto['product_quantity'];
                $prezzo_prodotto = $prodotto['product_price']-$prodotto['reduction_amount'];
            }
            else
            {
                $importoprodotto = ($prodotto['product_price']-($prodotto['product_price']*$prodotto['reduction_percent']/100))*$prodotto['product_quantity'];
                $prezzo_prodotto = $prodotto['product_price']-($prodotto['product_price']*$prodotto['reduction_percent']/100);
            }
            
            $tax_rate_p = Tax::getProductTaxRate((int)$prodotto['product_id'], Configuration::get('PS_TAX_ADDRESS_TYPE'));
            
            $iva += ($importoprodotto ) * ($tax_rate_p/100);
            
            $content .= '
                <tr style="font-size:10px;">
                <td style="width:100px">'. (empty($prodotto['product_reference']) ? $prodotto['reference'] : $prodotto['product_reference']).'</td>
                <td style="width:342px">'.(empty($prodotto['product_name']) ? $prodotto['name'] : $prodotto['product_name']).'</td>
                <td style="width:40px;">N.</td>
                <td style="width:58px; text-align:right">'.$prodotto['product_quantity'].'</td>
                <td style="width:58px; text-align:right">'.number_format($prezzo_prodotto,2,",",".").'</td>
                <td style="width:58px; text-align:right">'.number_format($importoprodotto,2,",",".").'</td>
                <td style="width:42px; text-align:right">'.number_format($tax_rate_p,0,",",".").'</td>
                </tr>
            ';
            // MODIFICATO 07/10/2020
            $importo += $importoprodotto;
        }

        // MODIFICATO 07/10/2020
        //$importo = $datiOrdine['total_products'];
			
        if($datiOrdine['id_carrier'] != 0) {

            $carrier_tax_rate = $datiOrdine['carrier_tax_rate'];
            $carrier_tax_rate = ($carrier_tax_rate + 100) / 100;
            
            $costo_spedizione = $datiOrdine['total_shipping'] / $carrier_tax_rate;
            
            // MODIFICATO 07/10/2020
            $carrier_tax_rate = $datiOrdine['carrier_tax_rate'];
            
            $content .= '
                <tr style="font-size:10px;">
                <td style="width:100px">TRASP</td>
                <td style="width:342px">Trasporti</td>
                <td style="width:40px;">N.</td>
                <td style="width:58px; text-align:right">1</td>
                <td style="width:58px; text-align:right">'.number_format($costo_spedizione,2,",",".").'</td>
                <td style="width:58px; text-align:right">'.number_format($costo_spedizione,2,",",".").'</td>
                <td style="width:42px; text-align:right">'.number_format($carrier_tax_rate,0,",",".").'</td>
                </tr>
            ';

            $importo += $costo_spedizione; 
        }

        /*
        // MODIFICATO 07/10/2020
        $subtotale = $importo;
        $totale_pagato = $datiOrdine['total_paid'];
        $tasse = $totale_pagato - $subtotale;
        $importo_ok = $importo + $tasse;
        */

        $iva += ($datiOrdine['total_shipping'] - $costo_spedizione);
        
        if($id_country == 10) {
            //$iva = ($importo * $tax_rate)/100;
        }
        else {
            if($datiCliente['is_company'] == 1  || $datiCliente['is_company'] == 0 && $id_country == 19)
                $iva = 0;
            //else
            //	$iva = ($importo * $tax_rate)/100;
        }
        
        if($datiCliente['tax_regime'] == 1) {
            $iva = 0;
        }
        
        if($iva == 0)
            $tax_rate = 0;
        
        $carrellon = new Cart(Db::getInstance()->getValue('SELECT id_cart FROM '._DB_PREFIX_.'orders WHERE id_order = '.$id_order));
        
        $imponibile = $datiOrdine['total_products'] + $costo_spedizione;
        
        $iva = $datiOrdine['total_paid_real'] - $imponibile;
        
        $content .= '</table>
            <br /><br />
            <table style="width:705px; text-align:right; border-top:1px solid black; border-bottom:1px solid black; display:block; margin:0 auto;">
                <tr style="font-size:9px">
                <td style="width:145px"><em>BOLLI</em></td>
                <td style="width:100px"><em>SPESE DI INCASSO</em></td>
                <td style="width:100px"><em>SPESE DI SPEDIZIONE</em></td>
                <td style="width:100px"><em>SPESE VARIE</em></td>
                <td style="width:100px"><em>SCONTO %</em></td>
                <td style="width:160px"><em>TOTALE MERCE SCONTATO</em></td>
                </tr>
                <tr style="font-size:11px; font-weight:bold">
                <td style="width:105px"></td>
                <td style="width:100px"></td>
                <td style="width:100px"></td>
                <td style="width:100px"></td>
                <td style="width:100px"></td>
                <td style="width:160px">'.number_format($importo,2,",",".").'</td>
                </tr>
            </table>
            <table style="width:705px; text-align:right; border-bottom:1px solid black; display:block; margin:0 auto; ">
                <tr style="font-size:9px">
                <td style="width:130px; text-align:left"><em>C.IVA</em></td>
                <td style="width:110px"><em>IMPONIBILE</em></td>
                <td style="width:200px"><em>% IVA - DESCRIZIONE IVA</em></td>
                <td style="width:110px"><em>IMPORTO IVA</em></td>
                <td style="width:160px"><em>TOTALE IMPONIBILE</em></td>
                </tr>
                <tr style="font-size:11px; font-weight:bold">
                <td style="width:130px; text-align:left">'.number_format($tax_rate,0,",",".").'</td>
                <td style="width:110px">'.number_format($importo,2,",",".").'</td>
                <td style="width:200px">'.number_format($tax_rate,0,",",".").'%</td>
                <td style="width:110px">'.number_format($iva,2,",",".").'</td>
                <td style="width:160px">'.number_format($importo,2,",",".").'<br /><br /><span style="font-size:9px; font-weight:normal"><em>(IVA dove applicabile)</em></span>
                <span style="font-size:9px; font-weight:normal"><em>TOTALE IVA</em></span><br />
                '.number_format($iva,2,",","").'
                </td>
                </tr>
            </table>
                
            <table style="width:715px; text-align:right; display:block; margin:0 auto; ">
                <tr style="font-size:9px">
                <td style="width:215px; text-align:left"><em>A VISTA</em></td>
                <td style="width:272px"><em>SCADENZE ED IMPORTI</em></td>
                <td style="width:235px"><em>TOTALE DOCUMENTO</em></td>
                </tr>
                <tr style="font-size:11px; font-weight:bold">
                <td style="width:210px; text-align:left">'.number_format($importo+$iva,2,",",".").'</td>
                <td style="width:272px"></td>
                <td style="width:235px">'.number_format($importo+$iva,2,",",".").'</td>
                </tr>
            </table>
        ';
			
        $datiCarrello = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."cart WHERE id_cart = ".$datiOrdine['id_cart']." AND id_customer = ".$id_customer."");	 
			
		$content .= '<br />

            <style type="text/css">
                #note p {
                    margin-top:0px;
                }
            </style>

            <div id="note" style="border:1px solid black; width:730px; padding:5px; font-size:11px">
                '.(empty($datiCarrello['esigenze']) ? '' : '<strong>Esigenze del cliente</strong><br />'.$datiCarrello['esigenze'].'<br /><br />').'
                '.(empty($datiCarrello['risorse'])  ? '' : '<strong>Risorse</strong> <br />'.$datiCarrello['risorse'] .'<br /><br />').'
                
                <strong>Note</strong><br />
                '.(empty($datiCarrello['note']) ? ' - ' : $datiCarrello['note']).'
                </div>
		';
		
		$noleggio = Db::getInstance()->getValue("SELECT id_cart FROM noleggio WHERE id_cart = ".$id_cart."");

        if(!$context->customer)
            $context->customer = new Customer($id_customer);
		
		if($context->customer->id_default_group == 3 || $context->customer->id_default_group == 15 || $context->customer->id_default_group == 22)
			$noleggio = 0;
		
        if($noleggio > 0) {
            $datiNoleggio = Db::getInstance()->getRow("SELECT * FROM noleggio WHERE id_cart = ".$id_cart."");
            
            $content .= '
                <div id="proposta_di_noleggio" style="border:1px solid black; width:730px; padding:5px; font-size:11px; margin-top:10px">
                
                <strong>Proposta di noleggio</strong><br />
                <strong>Proposta di noleggio (IVA esclusa)</strong>, valida per acquisti di apparecchiature hardware/elettronica su ordini di almeno 1.500 &euro; + iva <br />
                Contatta il tuo consulente Ezdirect al numero 0585 821163 per attivare la formula noleggio, da 18 a 60 mesi. La nostra guida sul noleggio: <strong style="color:blue; text-decoration:underline">https://www.ezdirect.it/guide/22-noleggio-centralini-telefonici-impianti-telefoni-voip-noleggiare</strong>. IMPORTO (IVA escl.) SPESE UNA TANTUM CADENZA RATA DURATA RISCATTO 13.586,00 100,00 Mensile 441,14 36 mesi 441,14<br /><br /><br /><br />
                <table style="width:686px; text-align:right;display:block; margin:0 auto; ">
                <tr style="font-size:9px">
                <td style="width:113px; text-align:left"></td>
                <td style="width:100px"><em>IMPORTO</em></td>
                <td style="width:110px"><em>SPESE CONTRATTO</em></td>
                <td style="width:160px"><em>RATA MENSILE</em></td>				
                <td style="width:110px"><em>MESI</em></td>
                <td style="width:100px"><em>RISCATTO</em></td>
                
                </tr>
                <tr style="font-size:11px; font-weight:bold">
                <td style="width:113px; text-align:left"></td>
                <td style="width:100px">'.number_format($importo,2,",",".").'</td>
                <td style="width:110px">'.number_format($datiNoleggio['spese_contratto'],2,",","").'</td>
                <td style="width:160px">'.number_format($importo*$datiNoleggio['parametri']/100,2,",",".").'</td>
                <td style="width:110px">'.$datiNoleggio['mesi'].'</td>
                <td style="width:100px">1%</td>
                
                </tr>
                </table></div>
            ';
        }

		$content .= '</div></page>';

		return $content;
    }

    // Usata in AdminSearch
    public static function searchOrderById($query)
	{
		$query = (string)$query;
		$query2 = explode('-',$query);
		$query_f = $query2[0];
		
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT o.id_order, 
                (CASE WHEN ca.preventivo = 0
                    THEN "--"
                    WHEN ca.preventivo = ""
                    THEN "--"
                    WHEN ca.preventivo IS NULL
                    THEN "--"
                    ELSE
                    o.id_cart
                END) AS `preventivo`, 
                (CASE c.is_company
                    WHEN 0
                    THEN CONCAT(c.firstname," ",c.lastname)
                    WHEN 1
                    THEN c.company
                    ELSE
                    CONCAT(c.firstname," ",c.lastname)
                END) AS customer, 
                c.id_customer, c.codice_esolver, c.id_default_group as gruppo, 
                o.total_products, o.total_paid, o.total_paid_real, o.payment, osl.`name` AS `osname`, o.date_add 
            FROM '._DB_PREFIX_.'orders o 
            JOIN '._DB_PREFIX_.'customer c on o.id_customer = c.id_customer 
            JOIN '._DB_PREFIX_.'cart ca on o.id_cart = ca.id_cart 
            LEFT JOIN `'._DB_PREFIX_.'order_history` oh ON (oh.`id_order` = o.`id_order`) 
            LEFT JOIN `'._DB_PREFIX_.'order_state` os ON (os.`id_order_state` = oh.`id_order_state`)
            LEFT JOIN `'._DB_PREFIX_.'order_state_lang` osl ON (os.`id_order_state` = osl.`id_order_state` AND osl.`id_lang` = 5) 
            WHERE o.id_order = '.$query_f.' 
                AND oh.`id_order_history` = (SELECT MAX(`id_order_history`) FROM `'._DB_PREFIX_.'order_history` moh WHERE moh.`id_order` = o.`id_order` GROUP BY moh.`id_order`)
        ');
		
	}

    public static function getTechState($id_order, $date, $type)
	{
		$context = Context::getContext();

		$value = Db::getInstance()->getRow('
            SELECT oh.id_order_state, osl.name name 
            FROM '._DB_PREFIX_.'order_history oh 
            JOIN '._DB_PREFIX_.'order_state_lang osl ON osl.id_order_state = oh.id_order_state 
            WHERE osl.id_lang = '.$context->language->id.'
                AND oh.id_order_history = (SELECT MAX(id_order_history) FROM '._DB_PREFIX_.'order_history WHERE id_order = '.$id_order.' AND '.($type == 'pay' ? '(id_order_state != 24 AND id_order_state != 25 AND id_order_state != 27 AND id_order_state != 28)' : '(id_order_state = 24 OR id_order_state = 25 OR id_order_state = 27 OR id_order_state = 28)').' 
                AND date_add < "'.$date.'")
        ');
		
		if($value['name'] != '')
			return '<img src="../img/os/'.$value['id_order_state'].'.gif" /> '.stripslashes($value['name']);
		else
			return '--';
	}
	
	public static function eSolverStatus($id_order, $id_order_state = 0)
	{
		if($id_order_state == 0)
			$id_order_state = Db::getInstance()->getValue('SELECT id_order_state FROM '._DB_PREFIX_.'order_history WHERE id_order = '.$id_order.' ORDER BY id_order_history DESC');

		if($id_order_state == 25)
		{
			$id_order_state_conclusa = 25;
			$id_order_state_history = Db::getInstance()->getValue('SELECT MAX(id_order_history) FROM '._DB_PREFIX_.'order_history WHERE id_order_state != 24 AND id_order_state != 25 AND id_order_state != 27 AND id_order_state != 28 AND id_order = '.$id_order.'');
			$id_order_state = Db::getInstance()->getValue('SELECT id_order_state FROM '._DB_PREFIX_.'order_history WHERE id_order_history = '.$id_order_state_history.' AND id_order = '.$id_order);
		}
		
		if($id_order_state == 24)
			return 0;
		
		if($id_order_state == 1 || $id_order_state == 10 || $id_order_state == 11 || $id_order_state == 17 || $id_order_state == 26)
			$stato = 1;
		else if($id_order_state == 2 || $id_order_state == 3 || $id_order_state == 4 || $id_order_state == 5 || $id_order_state == 6 || $id_order_state == 7 || $id_order_state == 12 || $id_order_state == 13 || $id_order_state == 14 || $id_order_state == 15 || $id_order_state == 16 || $id_order_state == 18 || $id_order_state == 19 || $id_order_state == 20 || $id_order_state == 21 || $id_order_state == 22 || $id_order_state == 23 || $id_order_state == 28 || $id_order_state == 29 || $id_order_state == 34)
			$stato = 2;
		else if($id_order_state == 17 || $id_order_state == 26)
			$stato = 0;
		
		$is_verifica = Db::getInstance()->getValue('SELECT id_order_history FROM '._DB_PREFIX_.'order_history WHERE id_order_state = 24 AND id_order = '.$id_order.' AND id_order NOT IN (SELECT id_order FROM '._DB_PREFIX_.'order_history WHERE id_order = '.$id_order.' AND id_order_state = 25)');
			
		if($is_verifica > 0 && $is_verifica != '' && $id_order_state_conclusa != 25)
			$stato = 0;
		
		$check_verifica = Db::getInstance()->getValue('SELECT id_order_state FROM '._DB_PREFIX_.'order_history WHERE id_order_history = (SELECT MAX(id_order_history) FROM (SELECT * FROM '._DB_PREFIX_.'order_history WHERE id_order_state = 24 OR id_order_state = 25) s WHERE s.id_order = '.$id_order.') ORDER BY date_add DESC');

		if($check_verifica == 24 && $id_order_state_conclusa != 25)
			$stato = 0;
		
		return $stato;
	}
}