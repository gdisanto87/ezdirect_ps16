<?php

class Meta extends MetaCore
{
    public static function getPages($exclude_filled = false, $add_page = false)
    {
        $selected_pages = array();
        if (!$files = Tools::scandir(_PS_CORE_DIR_.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.'front'.DIRECTORY_SEPARATOR, 'php', '', true)) {
            die(Tools::displayError('Cannot scan "root" directory'));
        }

        if (!$override_files = Tools::scandir(_PS_CORE_DIR_.DIRECTORY_SEPARATOR . 'override' . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR . 'front' . DIRECTORY_SEPARATOR, 'php', '', true)) {
            die(Tools::displayError('Cannot scan "override" directory'));
        }

        $files = array_values(array_unique(array_merge($files, $override_files)));

        // Exclude pages forbidden
        $exlude_pages = array(
            'category', 'changecurrency', 'cms', 'footer', 'header',
            'pagination', 'product', 'product-sort', 'statistics'
        );

        foreach ($files as $file) {
            if ($file != 'index.php' && !in_array(strtolower(str_replace('Controller.php', '', $file)), $exlude_pages)) {
                $class_name = str_replace('.php', '', $file);
                $reflection = class_exists($class_name) ? new ReflectionClass(str_replace('.php', '', $file)) : false;
                $properties = $reflection ? $reflection->getDefaultProperties() : array();
                if (isset($properties['php_self'])) {
                    $selected_pages[$properties['php_self']] = $properties['php_self'];
                } elseif (preg_match('/^[a-z0-9_.-]*\.php$/i', $file)) {
                    $selected_pages[strtolower(str_replace('Controller.php', '', $file))] = strtolower(str_replace('Controller.php', '', $file));
                } elseif (preg_match('/^([a-z0-9_.-]*\/)?[a-z0-9_.-]*\.php$/i', $file)) {
                    $selected_pages[strtolower(sprintf(Tools::displayError('%2$s (in %1$s)'), dirname($file), str_replace('Controller.php', '', basename($file))))] = strtolower(str_replace('Controller.php', '', basename($file)));
                }
            }
        }

        // Add modules controllers to list (this function is cool !)
        foreach (glob(_PS_MODULE_DIR_.'*/controllers/front/*.php') as $file) {
            $filename = Tools::strtolower(basename($file, '.php'));
            if ($filename == 'index') {
                continue;
            }

            $module = Tools::strtolower(basename(dirname(dirname(dirname($file)))));
            $selected_pages[$module.' - '.$filename] = 'module-'.$module.'-'.$filename;
        }
        /*
        // Il foreach sopra sostituisce questo? Se sì, eliminare override
        $selectedPages[] = 'modules/mytickets/tickets';
		$selectedPages[] = 'modules/formprevendita/form';
		$selectedPages[] = 'modules/formprevendita/tirichiamiamonoi';
		$selectedPages[] = 'modules/mailalerts/myalerts';
		$selectedPages[] = 'modules/mieofferte/offerte';
		$selectedPages[] = 'modules/miefatture/fatture';
		$selectedPages[] = 'modules/loyalty/loyalty-program';
        */
        // Exclude page already filled
        if ($exclude_filled) {
            $metas = Meta::getMetas();
            foreach ($metas as $meta) {
                if (in_array($meta['page'], $selected_pages)) {
                    unset($selected_pages[array_search($meta['page'], $selected_pages)]);
                }
            }
        }
        // Add selected page
        if ($add_page) {
            $name = $add_page;
            if (preg_match('#module-([a-z0-9_-]+)-([a-z0-9]+)$#i', $add_page, $m)) {
                $add_page = $m[1].' - '.$m[2];
            }
            $selected_pages[$add_page] = $name;
            asort($selected_pages);
        }
        return $selected_pages;
    }
}