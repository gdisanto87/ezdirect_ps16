<?php

class Address extends AddressCore
{
    // DA CORREGGERE!

    public 		$suburb;
	public		$c_o;
	public 		$fax;
	public 		$fatturazione;
    /*
    public 		$is_company; // inutilizzato?
    
    // nella class ci sono anche lastname e firstname
    protected	$fieldsRequired = array('id_country', 'alias', 'address1', 'city');
    
    // size che cambiano:
    'alias' => 320, 
    'lastname' => 320,  
    'company' => 320,
    'phone' => 16, 
    'phone_mobile' => 16, 
    'c_o' => ?
                                        
    // stile 1.6, vedi se si può fare anzi merge
    public static $definition = array(
        'table' => 'address',
        'primary' => 'id_address',
        'fields' => array(
            'id_customer' =>        array('type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId', 'copy_post' => false),
            'id_manufacturer' =>    array('type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId', 'copy_post' => false),
            'id_supplier' =>        array('type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId', 'copy_post' => false),
            'id_warehouse' =>        array('type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId', 'copy_post' => false),
            'id_country' =>        array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_state' =>            array('type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId'),
            'alias' =>                array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true, 'size' => 320),
            'company' =>            array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 320),
            'lastname' =>            array('type' => self::TYPE_STRING, 'validate' => 'isName', 'required' => true, 'size' => 320),
            'firstname' =>            array('type' => self::TYPE_STRING, 'validate' => 'isName', 'required' => true, 'size' => 32),
            'vat_number' =>            array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
            'address1' =>            array('type' => self::TYPE_STRING, 'validate' => 'isAddress', 'required' => true, 'size' => 128),
            'address2' =>            array('type' => self::TYPE_STRING, 'validate' => 'isAddress', 'size' => 128),
            'postcode' =>            array('type' => self::TYPE_STRING, 'validate' => 'isPostCode', 'size' => 12),
            'city' =>                array('type' => self::TYPE_STRING, 'validate' => 'isCityName', 'required' => true, 'size' => 64),
            'other' =>                array('type' => self::TYPE_STRING, 'validate' => 'isMessage', 'size' => 300),
            'phone' =>                array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 16),
            'phone_mobile' =>        array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 16),
            'dni' =>                array('type' => self::TYPE_STRING, 'validate' => 'isDniLite', 'size' => 16),
            'deleted' =>            array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'date_add' =>            array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            'date_upd' =>            array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            // override
            'suburb' =>             array('type' => self::TYPE_STRING, 'validate' => 'isSuburb', 'size' => 64),
            'c_o' =>                array('type' => self::TYPE_STRING, 'validate' => 'isSuburb', 'size' => 16),
            'fatturazione' =>       array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 1),
            'fax' =>                array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 16),
        ),
    );
	public function getFields()
	{
		if(empty($this->address1))
			$this->address1 = ' ';
		
		//parent::validateFields();
		if (isset($this->id))
			$fields['id_address'] = (int)($this->id);
		$fields['id_customer'] = is_null($this->id_customer) ? 0 : (int)($this->id_customer);
		$fields['id_manufacturer'] = is_null($this->id_manufacturer) ? 0 : (int)($this->id_manufacturer);
		$fields['id_supplier'] = is_null($this->id_supplier) ? 0 : (int)($this->id_supplier);
		$fields['id_country'] = (int)($this->id_country);
		$fields['id_state'] = (int)($this->id_state);
		$fields['alias'] = pSQL($this->alias);
		$fields['company'] = pSQL($this->company);
		$fields['lastname'] = pSQL($this->lastname);
		$fields['firstname'] = pSQL($this->firstname);
		$fields['address1'] = pSQL($this->address1);
		$fields['address2'] = pSQL($this->address2);
		$fields['suburb'] = pSQL($this->suburb);
		$fields['c_o'] = pSQL($this->c_o);
		$fields['postcode'] = pSQL($this->postcode);
		$fields['city'] = pSQL($this->city);
		$fields['other'] = pSQL($this->other);
		$fields['phone'] = pSQL($this->phone);
		$fields['phone_mobile'] = pSQL($this->phone_mobile);
		$fields['fax'] = pSQL($this->fax);
		$fields['fatturazione'] = pSQL($this->fatturazione);
		$fields['dni'] = pSQL($this->dni);
		$fields['deleted'] = (int)($this->deleted);
		$fields['date_add'] = pSQL($this->date_add);
		$fields['date_upd'] = pSQL($this->date_upd);
		Address::avviso_fatturazione();
		Address::updateCartAddressDelivery($this->id, $this->id_customer);
		
		return $fields;
    }
    */

    public function checkAddress($indirizzo, $cap, $localita){
        $ch = curl_init();
		$url = "https://weblabeling.gls-italy.com/utility/wsCheckAddress.asmx/CheckAddress";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'SedeGls=MS&CodiceClienteGls=8917&PasswordClienteGls=ezdirect&SiglaProvincia=&Cap='.$cap.'&Localita='.$localita.'&Indirizzo='.$indirizzo);
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $output = curl_exec($ch);
        curl_close($ch);
        $xml = simplexml_load_string($output);
		$json = json_encode($xml);
		$arr = json_decode($json,true);
		return $arr['Esito'];
    }

    // inutilizzata?
    /*public function suggestAddress($indirizzo, $cap, $localita, $campo){}*/
	
	// usata solo in getfields di override address 1.4
    /*public function updateCartAddressDelivery($id_address, $id_customer) {}*/

    // usata solo in getfields di override address 1.4
    /*public function avviso_fatturazione() {}*/
}