<?php

class Customer extends CustomerCore
{
	public $active = 1; // active diventa int (valori possibili 0,1,2)
    public $is_company;
    public $codice_esolver;
    public $pec;
    public $telefono_principale;
    public $cellulare_principale;
    public $referente;
    public $tax_code;
    public $vat_number;
    public $employees_number;
    public $tax_regime;
    public $order_notifications;
    public $keyword;
    public $supplier;
    public $cartella_documenti;
    public $ex_rivenditore;
    public $codice_univoco;
    public $ipa;
    public $settore;
    public $categoria;
    public $canale;
    public $ultimo_contatto;
    public $consenso_1;
    public $consenso_2;
    public $consenso_3;
    public $created_by;

	public static $definition = array(
        'table' => 'customer',
        'primary' => 'id_customer',
        'fields' => array(
            'secure_key' =>                  array('type' => self::TYPE_STRING, 'validate' => 'isMd5', 'copy_post' => false),
            'lastname' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isCustomerName', 'required' => true, 'size' => 32),
            'firstname' =>                   array('type' => self::TYPE_STRING, 'validate' => 'isCustomerName', 'required' => true, 'size' => 32),
            'email' =>                       array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'required' => true, 'size' => 128),
            'passwd' =>                      array('type' => self::TYPE_STRING, 'validate' => 'isPasswd', 'required' => true, 'size' => 32),
            'last_passwd_gen' =>             array('type' => self::TYPE_STRING, 'copy_post' => false),
            'id_gender' =>                   array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'birthday' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isBirthDate'),
            'newsletter' =>                  array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'newsletter_date_add' =>         array('type' => self::TYPE_DATE, 'copy_post' => false),
            'ip_registration_newsletter' =>  array('type' => self::TYPE_STRING, 'copy_post' => false),
            'optin' =>                       array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'website' =>                     array('type' => self::TYPE_STRING, 'validate' => 'isUrl'),
            'company' =>                     array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
            'siret' =>                       array('type' => self::TYPE_STRING, 'validate' => 'isSiret'),
            'ape' =>                         array('type' => self::TYPE_STRING, 'validate' => 'isApe'),
            'outstanding_allow_amount' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'copy_post' => false),
            'show_public_prices' =>          array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'id_risk' =>                     array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
            'max_payment_days' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
            'active' =>                      array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false), // modificato da bool a int
            'deleted' =>                     array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'note' =>                        array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml', 'size' => 65000, 'copy_post' => false),
            'is_guest' =>                    array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'id_shop' =>                     array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'id_shop_group' =>               array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'id_default_group' =>            array('type' => self::TYPE_INT, 'copy_post' => false),
            'id_lang' =>                     array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
            'date_add' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            'date_upd' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            // Override
            'is_company' =>                  array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
            'codice_esolver' =>              array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
            'pec' =>                         array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'size' => 128),
            'telefono_principale' =>         array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 100),
            'cellulare_principale' =>        array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 100),
            'referente' =>                   array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'tax_code' =>                    array('type' => self::TYPE_STRING, /*'validate' => '',*/ 'size' => 50),
            'vat_number' =>                  array('type' => self::TYPE_STRING, /*'validate' => '',*/ 'size' => 20),
            'employees_number' =>            array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 20),
            'tax_regime' =>                  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
            'order_notifications' =>         array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
            'keyword' =>                     array('type' => self::TYPE_STRING, /*'validate' => 'isGenericName',*/ 'copy_post' => false),
            'supplier' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
            'cartella_documenti' =>          array('type' => self::TYPE_STRING, 'size' => 250, 'copy_post' => false),
            'ex_rivenditore' =>              array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
            'codice_univoco' =>              array('type' => self::TYPE_STRING, 'size' => 150),
            'ipa' =>                         array('type' => self::TYPE_STRING, 'size' => 50),
            'settore' =>                     array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 150),
            'categoria' =>                   array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 150),
            'canale' =>                      array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 150),
            'ultimo_contatto' =>             array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 50, 'copy_post' => false),
            'consenso_1' =>                  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'consenso_2' =>                  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'consenso_3' =>                  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'created_by' =>                  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
        ),
    );
	
    // Sostituisce la funzione addGroups per assegnare un solo gruppo di default ad ogni cliente
    public function addDefaultGroup($group)
    {
        $this->cleanGroups();
        $row = array('id_customer' => (int)$this->id, 'id_group' => (int)$group);
        Db::getInstance()->insert('customer_group', $row, false, true, Db::INSERT_IGNORE);
    }

    // Sostituisce la funzione updateGroup quando è assegnato un solo gruppo di default ad ogni cliente
    public function updateDefaultGroup($list)
    {
        if ($list && !empty($list)) {
            $this->cleanGroups();
            $this->addDefaultGroup($list);
        } else {
            $this->addDefaultGroup(array($this->id_default_group));
        }
	}

	// Aggiunti DELETE FROM customer_amministrazione e persone
	public function delete()
    {
        if (!count(Order::getCustomerOrders((int)$this->id))) {
            $addresses = $this->getAddresses((int)Configuration::get('PS_LANG_DEFAULT'));
            foreach ($addresses as $address) {
                $obj = new Address((int)$address['id_address']);
                $obj->delete();
            }
		}
		
		// Override
        Db::getInstance()->execute('DELETE FROM customer_amministrazione WHERE id_customer = '.(int)$this->id);
        Db::getInstance()->execute('DELETE FROM persone WHERE id_customer = '.(int)$this->id);
		// correggere: mancano delete note private, azioni e todo?
		// Fine override
		
		Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'customer_group` WHERE `id_customer` = '.(int)$this->id);
        Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'message WHERE id_customer='.(int)$this->id);
        Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'specific_price WHERE id_customer='.(int)$this->id);
        Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'compare WHERE id_customer='.(int)$this->id);

        $carts = Db::getInstance()->executes('SELECT id_cart FROM '._DB_PREFIX_.'cart WHERE id_customer='.(int)$this->id);

        if ($carts) {
            foreach ($carts as $cart) {
                Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'cart WHERE id_cart='.(int)$cart['id_cart']);
                Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'cart_product WHERE id_cart='.(int)$cart['id_cart']);
            }
        }

        $cts = Db::getInstance()->executes('SELECT id_customer_thread FROM '._DB_PREFIX_.'customer_thread WHERE id_customer='.(int)$this->id);
		
        if ($cts) {
            foreach ($cts as $ct) {
                Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'customer_thread WHERE id_customer_thread='.(int)$ct['id_customer_thread']);
                Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'customer_message WHERE id_customer_thread='.(int)$ct['id_customer_thread']);
            }
        }

        CartRule::deleteByIdCustomer((int)$this->id);
        return parent::delete(); // Correggere: sostituire con return ObjectModel::delete(); ? Testare
    }
	
	// Vedere i commenti nella classe principale
	// public function transformToCustomer($id_lang, $password = null){}

	// Restituisce tutti gli indirizzi del cliente con active = 1
	public function getAddressesActive($id_lang)
    {
		$sql = '
			SELECT DISTINCT a.*, cl.`name` AS country, s.name AS state, s.iso_code AS state_iso
			FROM `'._DB_PREFIX_.'address` a
				LEFT JOIN `'._DB_PREFIX_.'country` c ON (a.`id_country` = c.`id_country`)
				LEFT JOIN `'._DB_PREFIX_.'country_lang` cl ON (c.`id_country` = cl.`id_country`)
				LEFT JOIN `'._DB_PREFIX_.'state` s ON (s.`id_state` = a.`id_state`)
			WHERE `id_lang` = '.(int)$id_lang.' AND `id_customer` = '.(int)$this->id.' AND a.`deleted` = 0
				AND a.`active` = 1
		';

		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
    }

	// Non devo considerare solo active = 1?
    public function getAddressesFatturazione($id_lang)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT a.*, cl.`name` AS country, s.name AS state, s.iso_code AS state_iso
			FROM `'._DB_PREFIX_.'address` a
			LEFT JOIN `'._DB_PREFIX_.'country` c ON (a.`id_country` = c.`id_country`)
			LEFT JOIN `'._DB_PREFIX_.'country_lang` cl ON (c.`id_country` = cl.`id_country`)
			LEFT JOIN `'._DB_PREFIX_.'state` s ON (s.`id_state` = a.`id_state`)
			WHERE `id_lang` = '.(int)($id_lang).' 
				AND `id_customer` = '.(int)($this->id).' 
				AND a.`deleted` = 0 
				AND a.`fatturazione` = 1 
		');
	}

	// Non devo considerare solo active = 1?
	public function getAddressesConsegna($id_lang)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT a.*, cl.`name` AS country, s.name AS state, s.iso_code AS state_iso
			FROM `'._DB_PREFIX_.'address` a
			LEFT JOIN `'._DB_PREFIX_.'country` c ON (a.`id_country` = c.`id_country`)
			LEFT JOIN `'._DB_PREFIX_.'country_lang` cl ON (c.`id_country` = cl.`id_country`)
			LEFT JOIN `'._DB_PREFIX_.'state` s ON (s.`id_state` = a.`id_state`)
			WHERE `id_lang` = '.(int)($id_lang).' 
				AND `id_customer` = '.(int)($this->id).' 
				AND a.`deleted` = 0 
				AND a.`fatturazione` = 0 
        ');
    }

	// Non devo considerare solo active = 1?
	// getAddresses con ordinamento per tipo: prima fatturazione e poi consegna
	public function getAddressesFattCons($id_lang)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT a.*, cl.`name` AS country, s.name AS state, s.iso_code AS state_iso
			FROM `'._DB_PREFIX_.'address` a
			LEFT JOIN `'._DB_PREFIX_.'country` c ON (a.`id_country` = c.`id_country`)
			LEFT JOIN `'._DB_PREFIX_.'country_lang` cl ON (c.`id_country` = cl.`id_country`)
			LEFT JOIN `'._DB_PREFIX_.'state` s ON (s.`id_state` = a.`id_state`)
			WHERE `id_lang` = '.(int)($id_lang).' 
				AND `id_customer` = '.(int)($this->id).' 
				AND a.`deleted` = 0 
			ORDER BY a.`fatturazione` DESC
        ');
    }

    // Ridefinita per togliere ambiguità di date_add
    public function getBoughtProducts()
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT o.date_add, o.id_order, od.product_id, od.product_name, p.id_manufacturer, p.id_category_default, od.product_quantity
            FROM `'._DB_PREFIX_.'orders` o
            LEFT JOIN `'._DB_PREFIX_.'order_detail` od ON o.id_order = od.id_order
            LEFT JOIN `'._DB_PREFIX_.'product` p ON od.product_id = p.id_product
            WHERE o.valid = 1 
                AND o.`id_customer` = '.(int)$this->id.'
            ORDER BY o.date_add DESC
        ');
    }

    // Da correggere! Produce un risultato diverso rispetto a getBoughtProducts; manca anche id_lang
    public function getBoughtProducts2($sort)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT o.date_add, o.id_order, cl.id_lang, od.product_id, od.product_name, p.id_manufacturer, m.name, p.id_category_default, cl.name AS catname, od.product_quantity 
			FROM `'._DB_PREFIX_.'orders` o
			LEFT JOIN `'._DB_PREFIX_.'order_detail` od ON o.id_order = od.id_order
			LEFT JOIN `'._DB_PREFIX_.'product` p ON od.product_id = p.id_product
			LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON p.id_manufacturer = m.id_manufacturer
			LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON p.id_category_default = cl.id_category
			WHERE o.valid = 1 
				AND o.`id_customer` = '.(int)($this->id).' 
			ORDER BY '.$sort.'
		');
    }

    public function getTotalBoughtProducts($id_prod, $id_customer)
	{
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
			SELECT sum(od.product_quantity) AS tot 
			FROM `'._DB_PREFIX_.'orders` o
			LEFT JOIN `'._DB_PREFIX_.'order_detail` od ON o.id_order = od.id_order
			WHERE o.valid = 1 
				AND od.`product_id` = '.$id_prod.' 
				AND o.`id_customer` = '.$id_customer.' 
			GROUP BY od.product_id
		');
	}
	
	// Usata in AdminCustomers -> Stat
    public function ultimoAcquistoPrimoAcquisto($tipo)
	{
		if($tipo == "ultimo")
			$sort = "o.date_add DESC";
		
		if($tipo == "primo")
			$sort = "o.date_add ASC";
	
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT o.date_add, o.id_order, od.product_id, od.product_name, p.id_manufacturer, p.id_category_default, od.product_quantity 
			FROM `'._DB_PREFIX_.'orders` o
			LEFT JOIN `'._DB_PREFIX_.'order_detail` od ON o.id_order = od.id_order
			LEFT JOIN `'._DB_PREFIX_.'product` p ON od.product_id = p.id_product
			WHERE o.valid = 1 
				AND o.`id_customer` = '.(int)($this->id).' 
			ORDER BY '.$sort.' 
			LIMIT 1
		');
	}

	// Usata in AdminCustomers -> Stat
	public function getInteresse()
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT count(p.id_category_default) AS contocat, o.date_add, o.id_order, od.product_id, od.product_name, p.id_manufacturer, p.id_category_default, od.product_quantity 
			FROM `'._DB_PREFIX_.'orders` o
			LEFT JOIN `'._DB_PREFIX_.'order_detail` od ON o.id_order = od.id_order
			LEFT JOIN `'._DB_PREFIX_.'product` p ON od.product_id = p.id_product
			WHERE o.valid = 1 
				AND o.`id_customer` = '.(int)($this->id).' 
			GROUP BY p.id_category_default 
			ORDER BY contocat DESC 
			LIMIT 1
		');
	}
	
	// Usata in AdminCustomers -> Stat
	public function getOrdersByYear() 
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT year(date_add) AS anno, sum(total_paid_real) AS totale, sum(total_products) AS totale_senza_iva 
			FROM `'._DB_PREFIX_.'orders`
			WHERE valid = 1 
				AND id_customer = '.(int)($this->id).' 
			GROUP BY anno
		');
	}

	// Dove viene utilizzata?
	public function getOrdersProfilation($period) 
	{
		/*switch($period){
			case '3mesi': $from = 
		}*/
		
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT year(date_add) AS anno, sum(total_paid) AS totale, sum(total_products) AS totale_senza_iva 
			FROM `'._DB_PREFIX_.'orders` 
			WHERE valid = 1 
				AND id_customer = '.(int)($this->id).' 
			GROUP BY anno
		');
	}

	// Usata in AdminSearch; ridefinisce la funzione già presente nella classe
	public static function searchByName($query, $limit = null)
	{
		$context = Context::getContext();
		
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT c.*, gl.name AS group_name, a.phone, a.phone_mobile, s.iso_code
			FROM `'._DB_PREFIX_.'customer` c 
			LEFT JOIN (
				SELECT * 
				FROM `'._DB_PREFIX_.'address` 
				WHERE active = 1 
					AND deleted = 0 
					AND fatturazione = 1
				) a ON c.id_customer = a.id_customer 
			LEFT JOIN `'._DB_PREFIX_.'state` s ON a.id_state = s.id_state
			LEFT JOIN (
				SELECT * 
				FROM `'._DB_PREFIX_.'group_lang` 
				WHERE id_lang = '.$context->language->id.'
				) gl ON c.id_default_group = gl.id_group
			WHERE 
			-- a.active = 1 AND a.deleted = 0 AND a.fatturazione = 1 AND -- 
			(c.`email` LIKE \'%'.pSQL($query).'%\'
				OR c.`id_customer` LIKE \'%'.pSQL($query).'%\'
				OR c.`company` LIKE \'%'.pSQL($query).'%\'
				OR CONCAT(c.firstname," ",c.lastname) LIKE \'%'.pSQL($query).'%\'
				OR CONCAT(c.lastname," ",c.firstname) LIKE \'%'.pSQL($query).'%\'
				OR c.`tax_code` LIKE \'%'.pSQL($query).'%\'
				OR c.`codice_esolver` LIKE \'%'.pSQL($query).'%\'
				OR c.`vat_number` LIKE \'%'.pSQL($query).'%\'
				OR REPLACE(a.`phone`, " ","") LIKE \'%'.pSQL($query).'%\'
				OR REPLACE(a.`phone_mobile`, " ","") LIKE \'%'.pSQL($query).'%\'
				OR c.`lastname` LIKE \'%'.pSQL($query).'%\'
				OR c.`keyword` LIKE \'%'.pSQL($query).'%\'
				OR c.`company` LIKE \'%'.pSQL($query).'%\'
				OR c.`firstname` LIKE \'%'.pSQL($query).'%\')  
			GROUP BY c.id_customer 
			'.(isset($_GET['orderby']) ? 'ORDER BY '.$_GET['orderby'].(isset($_GET['orderway']) ? ' '.$_GET['orderway'] : 'ASC') : 'ORDER BY c.company ASC').'
			'.($limit ? 'LIMIT '.(int)$limit : '').'
		');
	}

	public static function searchByPI($query)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT c.*, a.phone, a.phone_mobile, s.iso_code
			FROM `'._DB_PREFIX_.'customer` c 
			LEFT JOIN `'._DB_PREFIX_.'address` a ON c.id_customer = a.id_customer 
			LEFT JOIN `'._DB_PREFIX_.'state` s ON a.id_state = s.id_state
			WHERE 
			-- a.active = 1 AND a.deleted = 0 AND a.fatturazione = 1 AND -- 
			((c.`tax_code` LIKE "'.pSQL($query).'" AND c.tax_code != "")
				OR (c.`vat_number` LIKE "'.pSQL($query).'"  AND c.vat_number != ""))  
			GROUP BY c.id_customer 
			'.(isset($_GET['orderby']) ? 'ORDER BY '.$_GET['orderby'].(isset($_GET['orderway']) ? ' '.$_GET['orderway'] : 'ASC') : 'ORDER BY c.company ASC').'
		');
	}

	public static function searchByNameWithNotes($query)
	{		
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT * FROM (
				SELECT c.*, a.phone, a.phone_mobile, s.iso_code
				FROM `'._DB_PREFIX_.'customer` c 
				LEFT JOIN `'._DB_PREFIX_.'address` a ON c.id_customer = a.id_customer 
				LEFT JOIN `'._DB_PREFIX_.'state` s ON a.id_state = s.id_state
				JOIN persone p ON c.id_customer = p.id_customer 
				WHERE p.firstname LIKE  \'%'.pSQL($query).'%\'
					OR p.`phone` LIKE \'%'.pSQL($query).'%\'
					OR p.`phone_2` LIKE \'%'.pSQL($query).'%\'
					OR p.`phone_mobile` LIKE \'%'.pSQL($query).'%\' OR p.lastname LIKE  \'%'.pSQL($query).'%\' OR CONCAT(p.firstname," ",p.lastname) LIKE \'%'.pSQL($query).'%\' 
				
				UNION
				
				(
				SELECT c.*, a.phone, a.phone_mobile, s.iso_code
				FROM `'._DB_PREFIX_.'customer` c 
				LEFT JOIN `'._DB_PREFIX_.'address` a ON c.id_customer = a.id_customerustomer 
				LEFT JOIN `'._DB_PREFIX_.'state` s ON a.id_state = s.id_state
				LEFT JOIN customer_note cn ON c.id_customer = cn.id_customer
				WHERE (c.`email` LIKE \'%'.pSQL($query).'%\'
					OR c.`id_customer` LIKE \'%'.pSQL($query).'%\'
					OR CONCAT(c.firstname," ",c.lastname) LIKE \'%'.pSQL($query).'%\'
					OR c.`tax_code` LIKE \'%'.pSQL($query).'%\'
					OR c.`codice_esolver` LIKE \'%'.pSQL($query).'%\'
					OR c.`vat_number` LIKE \'%'.pSQL($query).'%\'
					OR REPLACE(a.`phone`, " ","") LIKE \'%'.pSQL($query).'%\'
					OR REPLACE(a.`phone_mobile`, " ","") LIKE \'%'.pSQL($query).'%\'
					OR c.`lastname` LIKE \'%'.pSQL($query).'%\'
					OR cn.`note` LIKE \'%'.pSQL($query).'%\'
					OR c.`keyword` LIKE \'%'.pSQL($query).'%\'
					OR c.`company` LIKE \'%'.pSQL($query).'%\'
					OR c.`firstname` LIKE \'%'.pSQL($query).'%\') 
				GROUP BY c.id_customer 
				'.(isset($_GET['orderby']) ? 'ORDER BY '.$_GET['orderby'].(isset($_GET['orderway']) ? ' '.$_GET['orderway'] : 'ASC') : 'ORDER BY c.company ASC').' 
				)
			) c 
			GROUP BY c.id_customer
		');
	}

	/* 
	Se $return_id = true: se esiste, restituisce l'id del cliente con P.IVA $vat_number, 0 altrimenti;
	Se $return_id = false: restituisce true se esiste un cliente con P.IVA $vat_number, false altrimenti;
	*/
	public static function vatNumberExists($vat_number, $return_id = false, $ignoreGuest = true)
	{
		$result = Db::getInstance()->getRow('
			SELECT `id_customer`
			FROM `'._DB_PREFIX_.'customer`
			WHERE `vat_number` = \''.pSQL($vat_number).'\'
			'.($ignoreGuest ? 'AND `is_guest` = 0' : '')
		);

		if($return_id)
			return (int)($result['id_customer']);
		else
			return isset($result['id_customer']);
	}
 
	// Usata in IdentityController 1.4 ma non capisco perchè non usi anzi vatNumberExists:
	// sono uguali a parte id_customer = ...; al massimo si può aggiungere un parametro a vatNumberExists
	public static function notMyVatNumberExists($vat_number, $return_id = false, $ignoreGuest = true)
	{
		$result = Db::getInstance()->getRow('
			SELECT `id_customer`
			FROM `'._DB_PREFIX_.'customer`
			WHERE id_customer = '.Tools::getValue('id_customer').' 
				AND `vat_number` = \''.pSQL($vat_number).'\''
				.($ignoreGuest ? 'AND `is_guest` = 0' : '')
		);

		if ($return_id)
			return (int)($result['id_customer']);
		else
			return isset($result['id_customer']);
	}

	public static function taxCodeExists($tax_code, $return_id = false, $ignoreGuest = true)
	{
		$result = Db::getInstance()->getRow('
			SELECT `id_customer`
			FROM `'._DB_PREFIX_.'customer`
			WHERE `tax_code` = \''.pSQL($tax_code).'\''
			.($ignoreGuest ? 'AND `is_guest` = 0' : '')
		);

		if ($return_id)
			return (int)($result['id_customer']);
		else
			return isset($result['id_customer']);
	}

	// Stesso discorso di notMyVatNumberExists
	public static function notMyTaxCodeExists($tax_code, $return_id = false, $ignoreGuest = true)
	{
		$result = Db::getInstance()->getRow('
			SELECT `id_customer`
			FROM `'._DB_PREFIX_.'customer`
			WHERE id_customer = '.Tools::getValue('id_customer').' 
			AND `tax_code` = \''.pSQL($tax_code).'\''
			.($ignoreGuest ? 'AND `is_guest` = 0' : '')
		);

		if ($return_id)
			return (int)($result['id_customer']);
		else
			return isset($result['id_customer']);
	}

	// Stesso discorso di notMyVatNumberExists
	public static function customerExistsNotMyMail($email, $return_id = false, $ignoreGuest = true)
	{
	 	if (!Validate::isEmail($email))
	 		die (Tools::displayError());

		$result = Db::getInstance()->getRow('
			SELECT `id_customer`
			FROM `'._DB_PREFIX_.'customer`
			WHERE id_customer = '.Tools::getValue('id_customer').' 
				AND `email` = \''.pSQL($email).'\''
				.($ignoreGuest ? 'AND `is_guest` = 0' : '')
		);

		if ($return_id)
			return (int)($result['id_customer']);
		else
			return isset($result['id_customer']);
	}
	
	public static function findEmailPersona($emails, $id_customer) 
	{
		$emails = explode(";",$emails);
		foreach($emails as $email) {
			if(!($email == "")){
				$count = Db::getInstance()->getValue("
					SELECT count(id_persona) 
					FROM persone 
					WHERE email = '".$email."' 
						AND id_customer = '".$id_customer."'
				");
				
				if($count<=0){
					Db::getInstance()->execute("
						INSERT INTO persone (id_persona, id_customer, email) 
						VALUES (NULL, ".$id_customer.", '".$email."')
					");
				}
			}
		}
	}

	public static function trovaSigla($id_thread, $tipo) 
	{
		if($tipo == 'ticket' || $tipo == 'messaggio' || $tipo == 'T' || $tipo == 'M') 
		{
			$query_tm = Db::getInstance()->getRow("
				SELECT id_contact , date_add
				FROM "._DB_PREFIX_."customer_thread 
				WHERE id_customer_thread = ".$id_thread."
			");

			$contact = $query_tm['id_contact'];
			$data = $query_tm['date_add'];

			switch ($contact) {
				case 2:
					$siglat = 'A';
					break;
				case 3:
					$siglat = 'V';
					break;
				case 4:
					$siglat = 'T';
					break;
				case 6:
					$siglat = 'R';
					break;
				case 7:
					$siglat = 'M';
					break;
				case 8:
					$siglat = 'D';
					break;
				case 9:
					$siglat = 'S';
					break;

				default:
					break;
			}
			
			$annot = substr($data,2,2);			
			$idticketunivoco = $siglat.$annot.$id_thread;
		}
		else if($tipo == 'preventivo' || $tipo == 'tirichiamiamonoi' || $tipo == 'P') 
		{
			if($tipo == 'preventivo' || $tipo == 'P') {
				$siglat = 'P'; 
			} 
			else if($tipo == 'tirichiamiamonoi' || $tipo == 'R') { 
				$siglat = 'R'; 
			}
		
			$data = Db::getInstance()->getValue("
				SELECT date_add 
				FROM form_prevendita_thread 
				WHERE id_thread = ".$id_thread."
			");
			
			$annot = substr($data,2,2);
			$idticketunivoco = $siglat.$annot.$id_thread;
		}
		else if($tipo == 'todo' || $tipo == 'to-do' || $tipo == 'A') {	
			$idticketunivoco = 'TODO'.$id_thread;	
		}
		else if($tipo == 'O') {	
			$idticketunivoco = 'Ordine '.$id_thread;		
		}
		else if($tipo == 'C') {	
			$idticketunivoco = 'Carrello '.$id_thread;
		}
		else if($tipo == 'BDL' || $tipo == 'L') {
			$idticketunivoco = 'BDL '.$id_thread;
		}
		else {
			$idticketunivoco = $id_thread;
		}
		
		return $idticketunivoco;
	}

	// Correggere: non sarebbe più comodo assegnare ai messaggi il codice M in storico e separarli completamente dai ticket?
	public static function cambiaIncaricoVeloce($tipo, $cambiaincarico, $id_thread, $precedente_incarico, $id_ticket_univoco, $id_customer) 
	{
		$context = Context::getContext();

		$employeemess = new Employee($context->employee->id);
		$customer = new Customer($id_customer);

		if($tipo == 'ticket' || $tipo == 'messaggio') {
			
			if($precedente_incarico != $cambiaincarico)
				Customer::Storico($id_thread, 'T', $context->employee->id, 2, $cambiaincarico);

			Db::getInstance()->execute("
				UPDATE "._DB_PREFIX_."customer_thread 
				SET id_employee = '".$cambiaincarico."', date_upd = '".date("Y-m-d H:i:s")."' 
				WHERE id_customer_thread = '".$id_thread."'
			");
			
			if($precedente_incarico != 0) {
				
				$mailimprev = Db::getInstance()->getValue("
					SELECT email 
					FROM "._DB_PREFIX_."employee 
					WHERE id_employee = '".$precedente_incarico."'
				");

				$mailimprev = 'carolina.carusi@ezdirect.it'; // prova - eliminare dopo test - correggere
				
				$nomeimprev = Db::getInstance()->getValue("
					SELECT firstname 
					FROM "._DB_PREFIX_."employee 
					WHERE id_employee = '".$precedente_incarico."'
				");
				
				if($cambiaincarico != 0){
					$nomeimp_att = Db::getInstance()->getValue("
						SELECT firstname
						FROM "._DB_PREFIX_."employee 
						WHERE id_employee = '".$cambiaincarico."'
					");
				}
				else
					$nomeimp_att = 'Nessuno';
	
				$msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione del ".(substr($id_ticket_univoco,0,1) == 'M' ? 'messaggio' : 'ticket')." numero ".$id_ticket_univoco." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect e l'ha passata a ".$nomeimp_att.".";

				$params = array('{msg}' => $msgimprev);

				if(!($precedente_incarico == $context->employee->id && $precedente_incarico != $cambiaincarico)) {
					if($cambiaincarico != $context->employee->id)
						Mail::Send($context->language->id, 'msg_base', Mail::l('Gestione '.(substr($id_ticket_univoco,0,1) == 'M' ? 'messaggio' : 'ticket').' revocata', $context->language->id), $params, $mailimprev, NULL, NULL, NULL, NULL, NULL, _PS_MAIL_DIR_, true);
				}
			}

			$tokenimp = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').$cambiaincarico);

			$linkimp_ticket = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".$id_customer."&viewcustomer&tab_name=tickets&azione=tickets&viewticket&id_customer_thread=".$id_thread."&token=".$tokenimp;
			$linkimp_msg = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".$id_customer."&viewcustomer&tab_name=actions&azione=actions&viewmessage&id_mex=".$id_thread."&token=".$tokenimp;
			$linkimp = (substr($id_ticket_univoco,0,1) == 'M' ? $linkimp_msg : $linkimp_ticket);

			$msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato il ".(substr($id_ticket_univoco,0,1) == 'M' ? 'messaggio' : 'ticket')." numero ".$id_ticket_univoco." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirlo</a>.";
			
			$params = array('{msg}' => $msgimp);

			$mailimp = Db::getInstance()->getValue("SELECT email FROM "._DB_PREFIX_."employee WHERE id_employee = '".$cambiaincarico."'");
			$mailimp = 'carolina.carusi@ezdirect.it'; // prova - eliminare dopo test - correggere

			if($cambiaincarico != $context->employee->id)
				Mail::Send($context->language->id, 'msg_base', Mail::l(''.(substr($id_ticket_univoco,0,1) == 'M' ? 'Messaggio' : 'Ticket').' assegnato a te su Ezdirect', $context->language->id), $params, $mailimp, NULL, NULL, NULL, NULL, NULL, _PS_MAIL_DIR_, true);
		}
		else if($tipo == 'preventivo' || $tipo == 'tirichiamiamonoi' ) {
		
			if($precedente_incarico != $cambiaincarico)
				Customer::Storico($id_thread, 'P', $context->employee->id, 2, $cambiaincarico);
			
			Db::getInstance()->execute("UPDATE form_prevendita_thread SET id_employee = '".$cambiaincarico."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_thread = '".$id_thread."'");
			
			if($precedente_incarico != 0) {
			
				$mailimprev = Db::getInstance()->getValue("
					SELECT email 
					FROM "._DB_PREFIX_."employee 
					WHERE id_employee = '".$precedente_incarico."'
				");
				$mailimprev = 'carolina.carusi@ezdirect.it'; // prova - eliminare dopo test - correggere
				
				$nomeimprev = Db::getInstance()->getValue("
					SELECT firstname 
					FROM "._DB_PREFIX_."employee 
					WHERE id_employee = '".$precedente_incarico."'
				");
				
				if($cambiaincarico != 0){
					$nomeimp_att = Db::getInstance()->getValue("
						SELECT firstname 
						FROM "._DB_PREFIX_."employee 
						WHERE id_employee = '".$cambiaincarico."'
					");
				}
				else
					$nomeimp_att = 'Nessuno';
				
				$msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione del ".$tipo." numero ".$id_ticket_univoco." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect e l'ha passata a ".$nomeimp_att.".";

				$params = array('{msg}' => $msgimprev);
							
				Mail::Send($context->language->id, 'msg_base', Mail::l('Gestione '.$tipo.' revocata', $context->language->id), $params, $mailimprev, NULL, NULL, NULL, NULL, NULL, _PS_MAIL_DIR_, true);
			}
			
			$tokenimp = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').$cambiaincarico);
			$linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".$id_customer."&viewcustomer&tab_name=actions&azione=actions&viewpreventivo&id_thread=".$id_thread."&token=".$tokenimp;

			$msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato il ".$tipo." numero ".$id_ticket_univoco." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirlo</a>.";
			
			$params = array('{msg}' => $msgimp);

			$mailimp = Db::getInstance()->getValue("SELECT email FROM "._DB_PREFIX_."employee WHERE id_employee = '".$cambiaincarico."'");
			$mailimp = 'carolina.carusi@ezdirect.it'; // prova - eliminare dopo test - correggere

			Mail::Send($context->language->id, 'msg_base', Mail::l(''.$tipo." numero ".$id_ticket_univoco.' assegnato a te su Ezdirect', $context->language->id), $params, $mailimp, NULL, NULL, NULL, NULL, NULL, _PS_MAIL_DIR_, true);
		}
		else if($tipo == 'to-do') {
		
			if($precedente_incarico != $cambiaincarico)
				Customer::Storico($id_thread, 'A', $context->employee->id, 2, $cambiaincarico);
			
			if($precedente_incarico != 0) {
				
				$mailimprev = Db::getInstance()->getValue("
					SELECT email 
					FROM "._DB_PREFIX_."employee 
					WHERE id_employee = '".$precedente_incarico."'
				");
				$mailimprev = 'carolina.carusi@ezdirect.it'; // prova - eliminare dopo test - correggere
				
				$nomeimprev = Db::getInstance()->getValue("
					SELECT firstname 
					FROM "._DB_PREFIX_."employee 
					WHERE id_employee = '".$precedente_incarico."'
				");
				
				if($cambiaincarico != 0){
					$nomeimp_att = Db::getInstance()->getValue("
						SELECT firstname 
						FROM "._DB_PREFIX_."employee 
						WHERE id_employee = '".$cambiaincarico."'
					");
				}
				else
					$nomeimp_att = 'Nessuno';
				
				$msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione dell'azione numero ".$id_ticket_univoco." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect e l'ha passata a ".$nomeimp_att.".";

				$params = array('{msg}' => $msgimprev);
				
				Mail::Send($context->language->id, 'msg_base', Mail::l('Gestione azione revocata', $context->language->id), $params, $mailimprev, NULL, NULL, NULL, NULL, NULL, _PS_MAIL_DIR_, true);
			}
			
			$tokenimp = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').$cambiaincarico);
			$linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".$id_customer."&viewcustomer&tab_name=todo&azione=todo&viewaction&id_action=".$id_thread."&token=".$tokenimp;
			
			$msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato l'azione numero ".$id_ticket_univoco." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirla</a>.";
			
			$params = array('{msg}' => $msgimp);

			$mailimp = Db::getInstance()->getValue("
				SELECT email 
				FROM "._DB_PREFIX_."employee 
				WHERE id_employee = '".$cambiaincarico."'
			");
			$mailimp = 'carolina.carusi@ezdirect.it'; // prova - eliminare dopo test - correggere
			
			Mail::Send($context->language->id, 'msg_base', Mail::l('Azione assegnata a te su Ezdirect', $context->language->id), $params, $mailimp, NULL, NULL, NULL, NULL, NULL, _PS_MAIL_DIR_, true);
		
			Db::getInstance()->execute("
				UPDATE action_thread 
				SET action_to = '".$cambiaincarico."', date_upd = '".date("Y-m-d H:i:s")."' 
				WHERE id_action = '".$id_thread."'
			");
		}
		else if($tipo == 'to-do-employee') {
			
			if($precedente_incarico != $cambiaincarico)
				Customer::Storico($id_thread, 'AE', $context->employee->id, 2, $cambiaincarico);
			
			if($precedente_incarico != 0) {
				$mailimprev = Db::getInstance()->getValue("
					SELECT email 
					FROM "._DB_PREFIX_."employee 
					WHERE id_employee = '".$precedente_incarico."'
				");
				$mailimprev = 'carolina.carusi@ezdirect.it'; // prova - eliminare dopo test - correggere
				
				$nomeimprev = Db::getInstance()->getValue("
					SELECT firstname 
					FROM "._DB_PREFIX_."employee 
					WHERE id_employee = '".$precedente_incarico."'
				");
				
				if($cambiaincarico != 0){
					$nomeimp_att = Db::getInstance()->getValue("
						SELECT firstname 
						FROM "._DB_PREFIX_."employee 
						WHERE id_employee = '".$cambiaincarico."'
					");
				}
				else
					$nomeimp_att = 'Nessuno';
				
				$msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione dell'azione staff numero ".$id_ticket_univoco." su Ezdirect e l'ha passata a ".$nomeimp_att.".";

				$params = array('{msg}' => $msgimprev);
							
				Mail::Send($context->language->id, 'msg_base', Mail::l('Gestione azione revocata', $context->language->id), $params, $mailimprev, NULL, NULL, NULL, NULL, NULL, _PS_MAIL_DIR_, true);
			}
			
			// Correggere a seconda dell'uso di AdminPlanner / AdminCustomerThreads
			$tokenimp = Tools::getAdminToken('AdminPlanner'.(int)Tab::getIdFromClassName('AdminPlanner').$cambiaincarico);
			//$tokenimp = Tools::getAdminToken("AdminCustomerThreads".(int)Tab::getIdFromClassName("AdminCustomerThreads").$cambiaincarico);
					
			$linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminPlanner&id_employee=".$id_employee."&updateemployee&viewaction&id_action=".$id_thread."&token=".$tokenimp;
			//$linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomerThreads&id_employee=".$id_employee."&updateemployee&viewaction&id_action=".$id_thread."&token=".$tokenimp;
			
			$msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato l'azione staff numero ".$id_ticket_univoco." su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirla</a>.";
			
			$params = array(
				'{msg}' => $msgimp);

			$mailimp = Db::getInstance()->getValue("
				SELECT email 
				FROM "._DB_PREFIX_."employee 
				WHERE id_employee = '".$cambiaincarico."'
			");
			$mailimp = 'carolina.carusi@ezdirect.it'; // prova - eliminare dopo test - correggere
			
			Mail::Send($context->language->id, 'msg_base', Mail::l('Azione staff assegnata a te su Ezdirect', $context->language->id), $params, $mailimp, NULL, NULL, NULL, NULL, NULL, 
			_PS_MAIL_DIR_, true);
		
			Db::getInstance()->execute("
				UPDATE action_thread_employee 
				SET action_to = '".$cambiaincarico."', date_upd = '".date("Y-m-d H:i:s")."' 
				WHERE id_action = '".$id_thread."'
			");
		}
	}

	public static function iconaCRM($tipo)
	{
		switch($tipo) {				
			case 'Assistenza tecnica postvendita':
			case 'Assistenza':
			case '4':
				return '<img src="https://www.ezdirect.it/img/admin/icons/assistenza.gif" alt="'.$tipo.'" title="'.$tipo.'" />'; 
				break;
			case 'Contabilita':
			case 'Contabilit&agrave;':
			case '2':
			case 'Contabilit?': 
				return '<img src="https://www.ezdirect.it/img/admin/icons/contabilita.gif" alt="'.$tipo.'" title="'.$tipo.'" />'; 
				break;
			case 'Messaggi': 
			case '7':
			case 'Messaggio': 
				return '<img src="https://www.ezdirect.it/img/admin/icons/messaggio.gif" alt="'.$tipo.'" title="'.$tipo.'" />'; 
				break;
			case '8':
			case 'Ordini': 
				return '<img src="https://www.ezdirect.it/img/admin/icons/ordini.gif" alt="'.$tipo.'" title="'.$tipo.'" />'; 
				break;
			case '3':
			case 'Rivenditori': 
				return '<img src="https://www.ezdirect.it/img/admin/icons/rivenditori.gif" alt="'.$tipo.'" title="'.$tipo.'" />'; 
				break;
			case '9':
			case 'RMA': 
				return '<img src="https://www.ezdirect.it/img/admin/icons/rma.gif" alt="'.$tipo.'" title="'.$tipo.'" />'; 
				break;
			case 'Telefonata': 
				return '<img src="https://www.ezdirect.it/img/admin/icons/telefonata.gif" alt="'.$tipo.'" title="'.$tipo.'" />'; 
				break;
			case 'TODO Amazon': 
				return '<img src="https://www.ezdirect.it/img/tmp/order_state_mini_22.gif" alt="'.$tipo.'" title="'.$tipo.'" />'; 
				break;
			case 'Attivita': 
				return '<img src="https://www.ezdirect.it/img/admin/icons/attivita.gif" alt="'.$tipo.'" title="'.$tipo.'" />'; 
				break;
			case 'Caso': 
				return '<img src="https://www.ezdirect.it/img/admin/icons/caso.gif" alt="'.$tipo.'" title="'.$tipo.'" />'; 
				break;
			case 'Intervento': 
				return '<img src="https://www.ezdirect.it/img/admin/icons/intervento.gif" alt="'.$tipo.'" title="'.$tipo.'" />'; 
				break;
			case 'preventivo': 
				return '<img src="https://www.ezdirect.it/img/admin/icons/commerciale.gif" alt="'.$tipo.'" title="'.$tipo.'" />'; 
				break;
			case 'Commerciale': 
				return '<img src="https://www.ezdirect.it/img/admin/icons/commerciale.gif" alt="'.$tipo.'" title="'.$tipo.'" />'; 
				break;
			case 'Richiesta_RMA': 
				return '<img src="https://www.ezdirect.it/img/admin/icons/richiesta_rma.gif" alt="'.$tipo.'" title="'.$tipo.'" />'; 
				break;
			case 'Richiamiamo': 
				return '<img src="https://www.ezdirect.it/img/admin/icons/tirichiamiamonoi.gif" alt="'.$tipo.'" title="'.$tipo.'" />'; 
				break;
			case 'tirichiamiamonoi': 
				return '<img src="https://www.ezdirect.it/img/admin/icons/tirichiamiamonoi.gif" alt="'.$tipo.'" title="'.$tipo.'" />';
				break;
			default: 
				return $tipo; 
				break;						
		}		
	}

	// Usata in convertiTicket
	public function switchTicketType($id) 
	{
		// Valori possibili:
		// ticket:assistenza; ticket:ordini; ticket:contabilita; ticket:rma; ticket:rivenditori; ticket:messaggi; prevendita:preventivo; prevendita:tirichiamiamonoi

		switch($id) {
			case 4: 
				$type = 'ticket:assistenza'; 
				break;
			case 8: 
				$type = 'ticket:ordini'; 
				break;
			case 2: 
				$type = 'ticket:contabilita'; 
				break;
			case 9: 
				$type = 'ticket:rma'; 
				break;
			case 3: 
				$type = 'ticket:rivenditori'; 
				break;
			case 7: 
				$type = 'ticket:messaggi'; 
				break;
			case 'preventivo': 
				$type = 'prevendita:preventivo'; 
				break;
			case 'tirichiamiamonoi'; 
				$type = 'prevendita:tirichiamiamonoi'; 
				break;
			default: 
				$type = '';
				break;
		}

		return $type;
	}

	public function convertiTicket($id, $from, $to)
	{
		$context = Context::getContext();

		$destination_id = $to;
		
		$from = Customer::switchTicketType($from);
		$to = Customer::switchTicketType($to);
		
		$new_id = '';
		$origine = explode(":",$from);
		$destinazione = explode(":",$to);
		
		if($origine[0] == $destinazione[0]) // Passaggio ad un'azione dello stesso tipo
		{
			if($origine[0] == 'prevendita') {

				Db::getInstance()->execute('
					UPDATE form_prevendita_thread 
					SET tipo_richiesta = "'.$destination_id.'", date_upd = "'.date("Y-m-d H:i:s").'" 
					WHERE id_thread = '.$id
				);

				Customer::Storico($id, 'P', $context->employee->id, 'Ha cambiato tipo attivit&agrave; in '.$destination_id);
				
				$old_id = Customer::trovaSigla($id, $origine[1]);
				$new_id = Customer::trovaSigla($id, $destination_id);
				
				Db::getInstance()->execute("
					INSERT INTO ticket_convertiti (old_id, new_id, t_from, t_to, date_add) 
					VALUES ('".$old_id."', '".$new_id."', '".$from."', '".$to."', '".date("Y-m-d H:i:s")."')
				");
			}
			else if($origine[0] == 'ticket') {
				
				Db::getInstance()->execute('
					UPDATE '._DB_PREFIX_.'customer_thread 
					SET id_contact = '.$destination_id.', date_upd = "'.date("Y-m-d H:i:s").'" 
					WHERE id_customer_thread = '.$id
				);
				
				$destinazione_nome = Db::getInstance()->getValue('
					SELECT name 
					FROM '._DB_PREFIX_.'contact_lang 
					WHERE id_lang = '.$context->language->id.' 
						AND id_contact = '.$destination_id
				);
				
				Customer::Storico($id, 'T', $context->employee->id, 'Ha cambiato tipo attivit&agrave; in '.$destinazione_nome);
				
				$old_id = Customer::trovaSigla($id, 'ticket');
				$new_id = Customer::trovaSigla($id, 'ticket');
				
				Db::getInstance()->execute("
					INSERT INTO ticket_convertiti (old_id, new_id, t_from, t_to, date_add) 
					VALUES ('".$old_id."', '".$new_id."', '".$from."', '".$to."', '".date("Y-m-d H:i:s")."')
				"); 
				
				$customer_id = Db::getInstance()->getValue('SELECT id_customer FROM '._DB_PREFIX_.'customer_thread WHERE id_customer_thread = '.$id);
				$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$context->employee->id);
				
				if($destination_id == 7) // Convertito in messaggio
					Tools::redirectAdmin('index.php?controller=AdminCustomers&id_customer='.$customer_id.'&viewcustomer&tab_name=actions&azione=actions&viewmessage&id_mex='.$id.'&conf=4&token='.$tokenCustomers);
				else
					Tools::redirectAdmin('index.php?controller=AdminCustomers&id_customer='.$customer_id.'&viewcustomer&tab_name=tickets&azione=tickets&viewticket&id_customer_thread='.$id.'&conf=4&token='.$tokenCustomers);
			}
		}
		else // Passaggio da prevendita a ticket e viceversa
		{
			if($origine[0] == 'prevendita' && $destinazione[0] == 'ticket') {
				
				$ultimothread = Db::getInstance()->getValue("
					SELECT id_customer_thread 
					FROM "._DB_PREFIX_."customer_thread 
					ORDER BY id_customer_thread DESC
				");
				
				$thread = $ultimothread + 1; // id del record da generare
				$token = Tools::passwdGen(12);
				
				$dati_thread = Db::getInstance()->getRow("
					SELECT * 
					FROM form_prevendita_thread 
					WHERE id_thread = ".$id."
				");

				$customer_id = $dati_thread['id_customer'];
				
				Db::getInstance()->execute("
					INSERT INTO "._DB_PREFIX_."customer_thread (id_customer_thread, id_lang, id_contact, id_customer, id_employee, subject, id_order, id_product, status, email, phone, token, date_add, date_upd, note) 
					VALUES ('".$thread."', ".$context->language->id.", '".$destination_id."', '".$customer_id."', '".$dati_thread['id_employee']."', '".addslashes($dati_thread['subject'])."', 0, 0, '".$dati_thread['status']."', '".$dati_thread['email']."', '".$dati_thread['phone']."', '".$token."', '".$dati_thread['date_add']."',  '".$dati_thread['date_upd']."', '')
				"); 
				
				$destinazione_nome = Db::getInstance()->getValue('
					SELECT name 
					FROM '._DB_PREFIX_.'contact_lang 
					WHERE id_lang = '.$context->language->id.' 
						AND id_contact = '.$destination_id
				);
				
				// Correggere: non dovrei spostare le note private in note_attivita al nuovo id/tipo?
				// Correggere: non ci andrebbe inserito l'id nuovo, $thread? Non dovrei spostare lo storico vecchio al nuovo id?
				Customer::Storico($id, 'T', $context->employee->id, 'Ha cambiato tipo attivit&agrave; in '.$destinazione_nome);
				
				$dati_messaggio = Db::getInstance()->executeS("
					SELECT * 
					FROM form_prevendita_message 
					WHERE id_thread = ".$id."
				");
			
				foreach($dati_messaggio as $m) {
					Db::getInstance()->execute("
						INSERT INTO "._DB_PREFIX_."customer_message (id_customer_message, id_customer_thread, id_employee, in_carico, email, file_name, message, ip_address, user_agent, date_add, date_upd) 
						VALUES (NULL, '".$thread."', '".$m['id_employee']."', '".$m['in_carico']."', '".$m['email']."', '".addslashes($m['file_name'])."', '".addslashes($m['message'])."', '', '', '".$m['date_add']."', '".$m['date_upd']."')
					");  
				}
				
				$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$context->employee->id);
				
				$old_id = Customer::trovaSigla($id, $origine[1]);
				$new_id = Customer::trovaSigla($thread, 'ticket');
				
				Db::getInstance()->execute("
					DELETE 
					FROM form_prevendita_thread 
					WHERE id_thread = ".$id."
				");
				
				Db::getInstance()->execute("
					DELETE 
					FROM form_prevendita_message 
					WHERE id_thread = ".$id."
				");
				
				Db::getInstance()->execute("
					INSERT INTO ticket_convertiti (old_id, new_id, t_from, t_to, date_add) 
					VALUES ('".$old_id."', '".$new_id."', '".$from."', '".$to."', '".date("Y-m-d H:i:s")."')
				");
				
				if($destination_id == 7) // Convertito in messaggio
					Tools::redirectAdmin('index.php?controller=AdminCustomers&id_customer='.$customer_id.'&viewcustomer&tab_name=actions&azione=actions&viewmessage&id_mex='.$thread.'&conf=4&token='.$tokenCustomers);
				else
					Tools::redirectAdmin('index.php?controller=AdminCustomers&id_customer='.$customer_id.'&viewcustomer&tab_name=tickets&azione=tickets&viewticket&id_customer_thread='.$thread.'&conf=4&token='.$tokenCustomers);
			}
			else if($origine[0] == 'ticket' && $destinazione[0] == 'prevendita') {
				
				$ultimothread = Db::getInstance()->getValue("
					SELECT id_thread 
					FROM form_prevendita_thread 
					ORDER BY id_thread DESC
				");
				
				$thread = $ultimothread + 1; // id del record da generare
				$token = Tools::passwdGen(12);
				
				// Correggere: non dovrei spostare le note private in note_attivita al nuovo id/tipo?
				// Correggere: non ci andrebbe inserito l'id nuovo, $thread? Non dovrei spostare lo storico vecchio al nuovo id/tipo?
				Customer::Storico($id, 'P', $context->employee->id, 'Ha cambiato tipo attivit&agrave; in '.$destination_id);
				
				$dati_thread = Db::getInstance()->getRow("
					SELECT * 
					FROM "._DB_PREFIX_."customer_thread 
					WHERE id_customer_thread = ".$id."
				");

				$customer_id = $dati_thread['id_customer'];
				
				// Correggere: errore se il cliente non ha un indirizzo di fatturazione attivo
				$dati_cliente = Db::getInstance()->getRow("
					SELECT c.*, c.tax_code as cf, c.vat_number as pi, a.*
					FROM "._DB_PREFIX_."customer c 
					JOIN "._DB_PREFIX_."address a ON c.id_customer = a.id_customer 
					LEFT JOIN "._DB_PREFIX_."state s ON a.id_state = s.id_state 
					WHERE c.id_customer = ".$customer_id." 
						AND a.fatturazione = 1 
						AND a.deleted = 0 
						AND a.active = 1
				");	
				// Correggere: id_employee deve sempre essere 1 (Ezio)?
				Db::getInstance()->execute("
					INSERT INTO form_prevendita_thread (id_thread, tipo_richiesta, id_customer, id_employee, subject, tax_code, vat_number, firstname, lastname, company, address1, postcode, state, country, email, phone, token, status, category, date_add, date_upd) 
					VALUES ('".$thread."', '".$destination_id."', '".$customer_id."', 1, '".addslashes($dati_thread['subject'])."', '".addslashes($dati_cliente['tax_code'])."', '".addslashes($dati_cliente['vat_number'])."', '".addslashes($dati_cliente['firstname'])."', '".addslashes($dati_cliente['lastname'])."', '".addslashes($dati_cliente['company'])."', '".addslashes($dati_cliente['address1'])."', '".addslashes($dati_cliente['postcode'])."', '".addslashes($dati_cliente['id_state'])."', '".addslashes($dati_cliente['id_country'])."', '".addslashes($dati_thread['email'])."', '".addslashes($dati_cliente['phone'])."', '$token', '".$dati_thread['status']."', '', '".$dati_thread['date_add']."',  '".$dati_thread['date_upd']."')
				");   
				
				$dati_messaggio = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."customer_message WHERE id_customer_thread = ".$id."");
			
				foreach($dati_messaggio as $m) {
					Db::getInstance()->execute("
						INSERT INTO form_prevendita_message (id_message, id_thread, id_customer, id_employee, in_carico, email, file_name, message, date_add, date_upd) 
						VALUES (NULL, '".$thread."', '".$customer_id."', '".$m['id_employee']."', '".$m['in_carico']."', '".$m['email']."', '".addslashes($m['file_name'])."', '".addslashes($m['message'])."', '".$m['date_add']."',  '".$m['date_upd']."')
					");  
				}
				
				$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$context->employee->id);
				
				$old_id = Customer::trovaSigla($id, 'ticket');
				$new_id = Customer::trovaSigla($thread, $destination_id);
				
				Db::getInstance()->execute("
					DELETE FROM "._DB_PREFIX_."customer_thread 
					WHERE id_customer_thread = ".$id."
				");
				
				Db::getInstance()->execute("
					DELETE FROM "._DB_PREFIX_."customer_message 
					WHERE id_customer_thread = ".$id."
				");
				
				Db::getInstance()->execute("
					INSERT INTO ticket_convertiti (old_id, new_id, t_from, t_to, date_add) 
					VALUES ('".$old_id."', '".$new_id."', '".$from."', '".$to."', '".date("Y-m-d H:i:s")."')
				"); 
				
				Tools::redirectAdmin('index.php?controller=AdminCustomers&id_customer='.$customer_id.'&viewcustomer&tab_name=actions&azione=actions&viewpreventivo&id_thread='.$thread.'&conf=4&token='.$tokenCustomers);
			}
		}
	}



	/* INIZIO funzioni da controllare */

	public static function chiudiOrdineDaAzione($id_action)
	{
		$context = Context::getContext();

		$ast = Db::getInstance()->getRow('
			SELECT am.id_action, am.id_action_message, a.status, am.action_message, a.date_upd 
			FROM action_message am 
			JOIN action_thread a ON am.id_action = a.id_action 
			WHERE (a.status = "closed" OR a.status = "pending1" OR a.status = "pending2") 
				AND am.id_action = '.$id_action.' 
				AND am.action_message LIKE "%<strong>*** ASSISTENZA TECNICA ***</strong><br /><br />%" 
			GROUP BY a.id_action
		');
	
		if($ast['id_action_message'] > 0)
		{
			$id_order = str_replace(')','',substr($ast['action_message'],-6));
			
			$status = Db::getInstance()->getRow('
				SELECT id_order_state, name 
				FROM (
					SELECT osl.name, os.id_order_state 
					FROM '._DB_PREFIX_.'orders o 
					JOIN '._DB_PREFIX_.'order_history oh ON (oh.id_order_history = (
						SELECT MAX(id_order_history) 
						FROM '._DB_PREFIX_.'order_history moh 
						WHERE moh.id_order = o.id_order 
						GROUP BY moh.id_order
						)
					) 
					JOIN '._DB_PREFIX_.'order_state os ON (os.id_order_state = oh.id_order_state) 
					JOIN '._DB_PREFIX_.'order_state_lang osl ON os.id_order_state = osl.id_order_state 
					WHERE osl.id_lang = '.$context->language->id.'
						AND o.id_order = '.$id_order.' 
					GROUP BY o.id_order
				) x
			');
	
			if($status['id_order_state'] != "" || $status['id_order_state'] != 4 || $status['id_order_state'] != 5 || $status['id_order_state'] != 6 || $status['id_order_state'] != 7 || $status['id_order_state'] != 8 || $status['id_order_state'] != 14 || $status['id_order_state'] != 15 || $status['id_order_state'] != 16 || $status['id_order_state'] != 20) 
			{
				if($ast['status'] == 'closed') {
					Customer::Storico($id_order, 'O', $context->employee->id, 'Ha chiuso todo assistenza');
					Db::getInstance()->execute('
						INSERT INTO '._DB_PREFIX_.'order_history (id_order_history, id_employee, id_order, id_order_state, date_add) 
						VALUES (NULL, '.$context->employee->id.', '.$id_order.', 28, "'.$ast['date_upd'].'")
					');
					
					$tot_products = Db::getInstance()->getValue("
						SELECT count(*) AS tot_assistenza 
						FROM "._DB_PREFIX_."order_detail 
						WHERE id_order = ".$id_order." 
							AND (product_reference NOT LIKE '%astec%' 
							AND product_reference NOT LIKE '%inst%')
					");
					
					if($tot_products <= 0) {
						Db::getInstance()->execute('
							INSERT INTO '._DB_PREFIX_.'order_history (id_order_history, id_employee, id_order, id_order_state, date_add) 
							VALUES (NULL, '.$context->employee->id.', '.$id_order.', 16, "'.$ast['date_upd'].'")
						');
					}
				}
				else {
					Customer::Storico($id_order, 'O', $context->employee->id, 'Ha messo in lavorazione todo assistenza, dunque l\'ordine &egrave; stato messo in status lavorazione tecnica');
					
					Db::getInstance()->executeS('
						INSERT INTO '._DB_PREFIX_.'order_history (id_order_history, id_employee, id_order, id_order_state, date_add) 
						VALUES (NULL, '.$context->employee->id.', '.$id_order.', 27, "'.$ast['date_upd'].'")
					');
				}	
			}
		}	
	}

	// Usata in AdminSearch
	public static function searchTicketById($query)
	{
		$context = Context::getContext();

		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT a.id_customer_thread, a.id_lang, a.id_contact, a.id_customer, a.id_employee, a.status, a.date_add, a.date_upd, CONCAT( CASE a.id_contact WHEN 2 THEN "A" WHEN 4 THEN "T" WHEN 3 THEN "V" WHEN 6 THEN "R" WHEN 7 THEN "M" WHEN 8 THEN "D" WHEN 9 THEN "S" END , SUBSTRING( a.date_add, 3, 2 ) , a.id_customer_thread ) AS ticket, (CASE c.id_default_group WHEN 1 THEN "Clienti web" WHEN 3 THEN "Rivenditori" ELSE "Altro" END ) AS gruppo_clienti, c.company, (CASE c.is_company WHEN 1 THEN c.vat_number ELSE c.tax_code END) AS cfpi, c.vat_number, c.tax_code, (CASE c.is_company WHEN 0 THEN CONCAT(c.firstname," ",c.lastname) WHEN 1 THEN c.company ELSE CONCAT(c.firstname," ",c.lastname) END )as customer, cl.id_contact as contact_type, (CASE WHEN cl.name LIKE "%contabil%" THEN "Contabilita" WHEN cl.name LIKE "%ordini%" THEN "Ordini" WHEN cl.name = "Assistenza tecnica postvendita" THEN "Assistenza" WHEN cl.name = "Rivenditori" THEN "Rivenditori" WHEN cl.name = "Messaggi" THEN "Messaggi" WHEN cl.name = "RMA" THEN "RMA" ELSE "Altro" END ) AS contact, a.status AS stato, a.date_add AS data_act 
			FROM `'._DB_PREFIX_.'customer_thread` a 
			LEFT JOIN `'._DB_PREFIX_.'customer` c ON c.`id_customer` = a.`id_customer` 
			LEFT JOIN `'._DB_PREFIX_.'contact_lang` cl ON (cl.`id_contact` = a.`id_contact` AND cl.`id_lang` = '.$context->language->id.') 
			WHERE 1 
				AND (a.id_contact != 7 OR (
					a.id_customer_thread IN (
						SELECT ct.id_customer_thread 
						FROM '._DB_PREFIX_.'customer_message cm 
						JOIN '._DB_PREFIX_.'customer_thread ct ON cm.id_customer_thread = ct.id_customer_thread 
						WHERE ct.id_contact = 7 
							AND cm.id_employee = 0
						)
					)
				)			
				AND CONCAT( CASE a.id_contact WHEN 2 THEN "A" WHEN 4 THEN "T" WHEN 3 THEN "V" WHEN 6 THEN "R" WHEN 7 THEN "M" WHEN 8 THEN "D" WHEN 9 THEN "S" END , SUBSTRING( a.date_add, 3, 2 ) , a.id_customer_thread ) LIKE "%'.pSQL(strtoupper($query)).'%" 
			GROUP BY a.id_customer_thread 
			
			UNION 

			SELECT ft.id_thread AS id_customer_thread, "'.$context->language->id.'" AS id_lang, ft.tipo_richiesta AS id_contact, ft.id_customer AS id_customer, ft.id_employee AS id_employee, ft.status AS status, ft.date_add AS date_add, ft.date_upd AS date_upd, (CASE ft.tipo_richiesta WHEN "preventivo" THEN CONCAT( "P", SUBSTRING( ft.date_add, 3, 2 ) , ft.id_thread ) WHEN "tirichiamiamonoi" THEN CONCAT( "R", SUBSTRING( ft.date_add, 3, 2 ) , ft.id_thread ) ELSE "--" END )AS ticket, (CASE c.id_default_group WHEN 1 THEN "Clienti web" WHEN 3 THEN "Rivenditori" ELSE "Altro" END ) AS gruppo_clienti, c.company AS company, (CASE c.is_company WHEN 1 THEN ft.vat_number ELSE ft.tax_code END) AS cfpi, ft.vat_number AS vat_number, ft.tax_code AS tax_code, (CASE c.is_company WHEN 0 THEN CONCAT(c.firstname," ",c.lastname) WHEN 1 THEN c.company ELSE CONCAT(c.firstname," ",c.lastname) END ) as customer, "" AS contact_type, (CASE ft.tipo_richiesta WHEN "preventivo" THEN "Commerciale" WHEN "tirichiamiamonoi" THEN "Richiamiamo" ELSE "--" END ) AS contact, ft.status AS stato, ft.date_add AS data_act 
			FROM form_prevendita_thread ft 
			LEFT JOIN `'._DB_PREFIX_.'customer` c ON c.`id_customer` = ft.`id_customer` 
			WHERE 1 
				AND (CASE ft.tipo_richiesta WHEN "preventivo" THEN CONCAT( "P", SUBSTRING( ft.date_add, 3, 2 ) , ft.id_thread ) WHEN "tirichiamiamonoi" THEN CONCAT( "R", SUBSTRING( ft.date_add, 3, 2 ) , ft.id_thread ) ELSE "--" END ) LIKE "%'.pSQL(strtoupper($query)).'%"  
			GROUP BY ft.id_thread 
			
			UNION 

			SELECT ac.id_action AS id_customer_thread, "'.$context->language->id.'" AS id_lang, ac.action_type AS id_contact, ac.id_customer AS id_customer, ac.action_to AS id_employee, ac.status AS status, ac.date_add AS date_add, ac.date_upd AS date_upd, CONCAT("TODO", ac.id_action) AS ticket, (CASE c.id_default_group WHEN 1 THEN "Clienti web" WHEN 3 THEN "Rivenditori" ELSE "Altro" END ) AS gruppo_clienti, c.company AS company, (CASE c.is_company WHEN 1 THEN c.vat_number ELSE c.tax_code END) AS cfpi, c.vat_number AS vat_number, c.tax_code AS tax_code, (CASE c.is_company WHEN 0 THEN CONCAT(c.firstname," ",c.lastname) WHEN 1 THEN c.company ELSE CONCAT(c.firstname," ",c.lastname) END ) as customer, "" AS contact_type, ac.action_type AS contact, ac.status AS stato, (CASE ac.action_date WHEN "0000-00-00 00:00:00" THEN ac.date_add ELSE ac.action_date END) AS data_act 
			FROM action_thread ac 
			LEFT JOIN `'._DB_PREFIX_.'customer` c ON c.`id_customer` = ac.`id_customer` 
			WHERE 1 
				AND CONCAT("TODO", ac.id_action) LIKE "%'.pSQL(strtoupper($query)).'%" 
			GROUP BY ac.id_action
		');
	}

	// Usata in AdminSearch
	public static function searchBDLById($query)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT SQL_CALC_FOUND_ROWS * 
			FROM (
				SELECT a.id_bdl, a.id_customer, a.effettuato_da, a.data_richiesta, CONCAT("BDL" , a.id_bdl ) AS ticket, 
				( 
					SELECT IFNULL(CONCAT(e.firstname," ",LEFT(e.lastname, 1),"."), "--") 
					FROM '._DB_PREFIX_.'bdl 
					INNER JOIN '._DB_PREFIX_.'employee e ON e.id_employee = bdl.effettuato_da 
					WHERE bdl.effettuato_da > 0 
						AND bdl.id_bdl = a.id_bdl 
					LIMIT 1
				) as employee, 
				(CASE c.is_company WHEN 0 THEN CONCAT(c.firstname," ",c.lastname) WHEN 1 THEN c.company ELSE CONCAT(c.firstname," ",c.lastname) END ) as customer, (
					CASE 
						WHEN a.rif_ordine > 0
							THEN "closed"
						WHEN a.id_bdl < 384
							THEN "closed"
						WHEN a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 0
							THEN "pending1"
						WHEN a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 1
							THEN "closed"
						WHEN a.invio_controllo = 2 AND a.invio_contabilita = 2
							THEN "closed"
						WHEN a.rif_ordine = 0 AND a.invio_controllo = 0
							THEN "open"
						ELSE "open"
					END
				) stato 
				FROM '._DB_PREFIX_.'bdl a 
				JOIN '._DB_PREFIX_.'customer c ON a.id_customer = c.id_customer
			) tmpTable 
			WHERE 1 
				AND `ticket` LIKE "%'.pSQL(strtoupper($query)).'%" 
				'.(isset($_GET['orderby']) ? 'ORDER BY '.$_GET['orderby'].(isset($_GET['orderway']) ? ' '.$_GET['orderway'] : 'ASC') : 'ORDER BY (CASE WHEN stato = "open" THEN 3 WHEN stato = "pending1" THEN 2 WHEN stato = "pending2" THEN 1 WHEN stato = "closed" THEN 0 END) DESC, (CASE WHEN effettuato_da = 6 THEN 0 ELSE 1 END), data_richiesta ASC').'
		');
	}

	/*	// correggere
	// usata nel modulo statsforecast (stats-6)
	public function returnStatisticheStorico($tipo_attivita, $tipo_specifico, $tipo_num, $data_in, $id_employee = 0)
	{
		if($tipo_attivita == 'a')
			$query = ('
				SELECT z.action_to id_employee, AVG( FLOOR( HOUR( TIMEDIFF( z.data_attivita, z.date_add ) ) /24 ) ) giorni, AVG( MOD( HOUR( TIMEDIFF( z.data_attivita, z.date_add ) ) ,24 ) ) ore, AVG(  MINUTE( TIMEDIFF( z.data_attivita, z.date_add  ) ) ) minuti 
				FROM (
					SELECT id_action, action_type, id_customer, action_to, at.status, date_add, min( sa.data_attivita ) data_attivita 
					FROM action_thread at 
					JOIN (
						SELECT * FROM storico_attivita 
						WHERE tipo_attivita = "'.$tipo_attivita.'" 
						ORDER BY data_attivita ASC
					) sa 
					ON at.id_action = sa.id_attivita 
					WHERE '.($id_employee > 0 ? 'at.action_to = '.$id_employee.' AND ' : '').' at.date_add > "2016-03-21 00:00:00" 
						AND sa.data_attivita BETWEEN '.$data_in.' AND sa.tipo_num = '.$tipo_num.' 
						AND at.action_type = "'.$tipo_specifico.'" 
					GROUP BY at.id_action 
					ORDER BY at.id_action DESC
				) z 
				GROUP BY z.action_to
			');
		else if($tipo_attivita == 't')
			$query = ('
				SELECT z.id_employee, AVG( FLOOR( HOUR( TIMEDIFF( z.data_attivita, z.date_add ) ) /24 ) ) giorni, AVG( MOD( HOUR( TIMEDIFF( z.data_attivita, z.date_add ) ) ,24 ) ) ore, AVG(  MINUTE( TIMEDIFF( z.data_attivita, z.date_add  ) ) ) minuti 
				FROM (
					SELECT id_customer_thread, id_contact, id_customer, id_employee, at.status, date_add, min( sa.data_attivita ) data_attivita 
					FROM customer_thread at 
					JOIN (
						SELECT * 
						FROM storico_attivita 
						WHERE tipo_attivita = "'.$tipo_attivita.'" 
						ORDER BY data_attivita ASC
					) sa 
					ON at.id_customer_thread = sa.id_attivita 
					WHERE  '.($id_employee > 0 ? 'at.id_employee = '.$id_employee.' AND ' : '').'  at.date_add > "2016-03-21 00:00:00" 
						AND sa.data_attivita BETWEEN '.$data_in.' AND sa.tipo_num = '.$tipo_num.' 
						AND at.id_contact = "'.$tipo_specifico.'" 
					GROUP BY at.id_customer_thread 
					ORDER BY at.id_customer_thread DESC
				) z 
				GROUP BY z.id_employee
			');
		else
			$query = '';
		
		if($id_employee == 0)
			return Db::getInstance()->executeS($query);
		else
			return Db::getInstance()->getRow($query);	
	}

	// usata nel modulo statsforecast (stats-6); quasi uguale alla func sopra, potrei unirle aggiungendo un parametro "media" o tenerne solo una se non servono entrambe
	public function returnStatisticheStoricoMedia($tipo_attivita, $tipo_specifico, $tipo_num, $data_in)
	{
		if($tipo_attivita == 'a')
			return Db::getInstance()->getRow('
				SELECT AVG( FLOOR( HOUR( TIMEDIFF( z.data_attivita, z.date_add ) ) /24 ) ) giorni, AVG(TIMESTAMPDIFF(MINUTE, z.date_add, z.data_attivita)/60.0 - 12*(TIMESTAMPDIFF(DAY,"2000-01-03",z.data_attivita)-TIMESTAMPDIFF(DAY,"2000-01-03",z.date_add)) - 24*(TIMESTAMPDIFF(WEEK, "2000-01-03",z.data_attivita)-TIMESTAMPDIFF(WEEK,"2000-01-03",z.date_add))) ore, AVG(  MINUTE( TIMEDIFF( z.data_attivita, z.date_add  ) ) ) minuti 
				FROM (
					SELECT id_action, action_type, id_customer, tipo_attivita, at.status, date_add, min( sa.data_attivita ) data_attivita 
					FROM action_thread at 
					JOIN (
						SELECT * 
						FROM storico_attivita 
						WHERE tipo_attivita = "'.$tipo_attivita.'" 
						ORDER BY data_attivita ASC
					) sa 
					ON at.id_action = sa.id_attivita 
					WHERE '.($id_employee > 0 ? 'at.action_to = '.$id_employee.' AND ' : '').' at.date_add > "2016-03-21 00:00:00" 
						AND sa.data_attivita BETWEEN '.$data_in.' AND sa.tipo_num = '.$tipo_num.' 
						AND at.action_type = "'.$tipo_specifico.'" 
					GROUP BY at.id_action 
					ORDER BY at.id_action DESC
				) z 
				GROUP BY z.tipo_attivita
			');
		else if($tipo_attivita == 't')
			return Db::getInstance()->getRow('
				SELECT AVG( FLOOR( HOUR( TIMEDIFF( z.data_attivita, z.date_add ) ) /24 ) ) giorni, AVG(TIMESTAMPDIFF(MINUTE, z.date_add, z.data_attivita)/60.0 - 12*(TIMESTAMPDIFF(DAY,"2000-01-03",z.data_attivita)-TIMESTAMPDIFF(DAY,"2000-01-03",z.date_add)) - 24*(TIMESTAMPDIFF(WEEK, "2000-01-03",z.data_attivita)-TIMESTAMPDIFF(WEEK,"2000-01-03",z.date_add))) ore, AVG(  MINUTE( TIMEDIFF( z.data_attivita, z.date_add  ) ) ) minuti 
				FROM (
					SELECT id_customer_thread, id_contact, id_customer, tipo_attivita, at.status, date_add, min( sa.data_attivita ) data_attivita 
					FROM customer_thread at 
					JOIN (
						SELECT * 
						FROM storico_attivita 
						WHERE tipo_attivita = "'.$tipo_attivita.'" 
						ORDER BY data_attivita ASC
					) sa 
					ON at.id_customer_thread = sa.id_attivita 
					WHERE  '.($id_employee > 0 ? 'at.id_employee = '.$id_employee.' AND ' : '').'  at.date_add > "2016-03-21 00:00:00" 
						AND sa.data_attivita BETWEEN '.$data_in.' AND sa.tipo_num = '.$tipo_num.' 
						AND at.id_contact = "'.$tipo_specifico.'" 
					ORDER BY at.id_customer_thread DESC
				) z 
				GROUP BY z.tipo_attivita
			');
		else
			return '';
	}
	*/

	public static function statusPadre($rif)
	{
		if(is_numeric($rif)) {
			$status = Db::getInstance()->getValue("
				SELECT status 
				FROM form_prevendita_thread 
				WHERE id_thread = ".$rif."
			");
		}
		else {
			$first = substr($rif,0,1);
			$thread = substr($rif,1,99);
			
			if($first == 'T') {
				$status = Db::getInstance()->getValue("
					SELECT status 
					FROM "._DB_PREFIX_."customer_thread 
					WHERE id_customer_thread = ".$thread."
				");
			}
			else if($first == 'A') {
				$status = Db::getInstance()->getValue("
					SELECT status 
					FROM action_thread 
					WHERE id_action = ".$thread."
				");
			}
			else if($first == 'P') {
				$status = Db::getInstance()->getValue("
					SELECT status 
					FROM form_prevendita_thread 
					WHERE id_thread = ".$thread."
				");
			}

			// Correggere: nel caso default $status = ? (casi M-R)
		}
		
		return $status;
	}

	public function chiudiPadre($rif)
	{
		if(Tools::getIsset('chiudi_padre')){
			if($rif != '') {
				if(is_numeric($rif)) {
					Db::getInstance()->execute("
						UPDATE form_prevendita_thread 
						SET status = 'closed' 
						WHERE id_thread = ".$rif."
					");
				}
				else{
					$first = substr($rif,0,1);
					$thread = substr($rif,1,99);
					
					if($first == 'T'){
						Db::getInstance()->execute("
							UPDATE "._DB_PREFIX_."customer_thread 
							SET status = 'closed' 
							WHERE id_customer_thread = ".$thread."
						");
					}
					else if($first == 'A') {
						Db::getInstance()->execute("
							UPDATE action_thread 
							SET status = 'closed' 
							WHERE id_action = ".$thread."
						");
					}
					else if($first == 'P') {
						Db::getInstance()->execute("
							UPDATE form_prevendita_thread 
							SET status = 'closed' 
							WHERE id_thread = ".$thread."
						");
					}
				}
			}
		}	
	}

	// Genera il form di selezione template e pulsante NUOVO PREVENTIVO nell'anagrafica cliente
	// correggere: testare; è giusto rif_prev = A come caso default quando non dovrebbe esserci?
	public static function displayFormPreventivo($customer, $preventivo)
	{
		$context = Context::getContext();

		$tokenCarts = Tools::getAdminToken('AdminCarts'.(int)Tab::getIdFromClassName('AdminCarts').(int)$context->employee->id);
		
		$template_carrelli = Db::getInstance()->executeS('
			SELECT id_cart, name 
			FROM '._DB_PREFIX_.'cart 
			WHERE id_customer = 44431 
			ORDER BY name ASC
		');
		
		$display = '<form name="new_preventivo_form_in" action="index.php?tab=AdminCarts&viewcart&provvisorio=1&id_customer='.$customer->id.'&preventivo=1&rif_prev='.(Tools::getIsset('id_thread') ? Tools::getValue('id_thread') : ( Tools::getValue('id_customer_thread') ? 'T'.Tools::getIsset('id_customer_thread') : (Tools::getIsset('id_mex') ? 'T'.Tools::getValue('id_mex') : 'A'.Tools::getValue('id_action') ))).'&riferimento='.Tools::getValue('seleziona_personap').'&md&token='.$tokenCarts.'#modifica-carrello" method="post">';
			
		$template_carrelli = Db::getInstance()->executeS('
			SELECT id_cart, name 
			FROM '._DB_PREFIX_.'cart 
			WHERE id_customer = 44431 
			ORDER BY name ASC
		');
		
		$display .= '<input type="hidden" name="copy_to" value="'.$customer->id.'" />';
		$display .= '<input type="hidden" name="preventivo" value="1" />';
		$display .= '<input type="hidden" name="copia_carrello_conferma" value="y" />';
		
		$persona_preventivo = Db::getInstance()->getValue('
			SELECT id_persona 
			FROM persone 
			WHERE '.(Tools::getIsset('viewpreventivo') ? 'firstname = "'.$preventivo['firstname'].'" AND lastname = "'.$preventivo['lastname'].'" AND email = "'.$preventivo['email'].'" AND' : '').' id_customer = '.$customer->id.' 
			ORDER BY id_persona DESC
		');
		
		$display .= '<input type="hidden" name="riferimento" value="'.$persona_preventivo.'" />';
		$display .= '<select class="col-md-6" style="display:block; float:left" name="id_cart" id="template_carrello" onchange="$(\'#link-preventivo1\').attr(\'href\', \'index.php?tab=AdminCarts&viewcart&preventivo=1&amp;copy_to='.$customer->id.'&provvisorio=1&id_customer='.$customer->id.'&copia_carrello_conferma=y&id_cart=\'+this.value+\'&token='.$tokenCarts.'&preventivo=1'.(Tools::getIsset('viewpreventivo') || Tools::getIsset('viewticket') ? '&rif_prev='.(Tools::getIsset('id_thread') ? Tools::getValue('id_thread') : ( Tools::getValue('id_customer_thread') ? 'T'.Tools::getValue('id_customer_thread') : 'A'.Tools::getValue('id_action') )) : '').'&riferimento='.$persona_preventivo.'&md#modifica-carrello\');";><option value="">-- Prima selezionare template --</option>';
		
		foreach($template_carrelli as $template) {
			$display .= '<option value="'.$template['id_cart'].'">'.$template['name'].'</option>';
		}
		
		$display .=  '</select>';
		$display .=  '<div class="col-md-6"><a href="index.php?tab=AdminCarts&viewcart&createnew&provvisorio=1&id_customer='.$customer->id.'&preventivo=1&rif_prev='.(Tools::getIsset('id_thread') ? Tools::getValue('id_thread') : ( Tools::getIsset('id_customer_thread') ? 'T'.Tools::getValue('id_customer_thread') : (Tools::getIsset('id_mex') ? 'T'.Tools::getValue('id_mex') : 'A'.Tools::getValue('id_action') ))).'&riferimento='.$persona_preventivo.'&md&token='.$tokenCarts.'#modifica-carrello" style=" width:150px" class="button"  onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }" id="link-preventivo1"><span class="nuovo_preventivo"><img src="../img/admin/charged_ok.gif" alt="Nuovo preventivo" title="Nuovo preventivo" /> NUOVO PREVENTIVO</span></a></div>';
		
		$display .=  '</form><br /><br />';
		
		return $display;
	}
	
	// Genera il PDF di una conversazione
	public function exportConversation($id, $tipo)
	{
		if($tipo == 'T' || $tipo == 'M') {
			if($tipo == 'T')
				$oggetto = 'Ticket n. '.Customer::trovaSigla($id, $tipo);
			else
				$oggetto = 'Messaggio n. '.Customer::trovaSigla($id, $tipo);

			$row_ct =  Db::getInstance()->getRow('
				SELECT date_add, date_upd, id_employee, status, id_contact
				FROM '._DB_PREFIX_.'customer_thread
				WHERE id_customer_thread = '.$id
			);

			$date_add = $row_ct['date_add'];
			$date_upd = $row_ct['date_upd'];
			$in_carico_a = $row_ct['id_employee'];
			$status = $row_ct['status'];
			$id_contact = $row_ct['id_contact'];
			
			$aperto_da = Db::getInstance()->getValue('
				SELECT id_employee 
				FROM '._DB_PREFIX_.'customer_message 
				WHERE id_customer_thread = '.$id.' 
				ORDER BY id_customer_message ASC
			');
			
			if($aperto_da)
				$aperto_da = Db::getInstance()->getValue('
					SELECT CONCAT(firstname," ",lastname) 
					FROM '._DB_PREFIX_.'employee 
					WHERE id_employee = '.$aperto_da
				);
			else
				$aperto_da = 'Cliente';

			$conversazione = Db::getInstance()->executeS('
				SELECT id_employee AS mittente, date_add AS data, message AS messaggio 
				FROM '._DB_PREFIX_.'customer_message 
				WHERE id_customer_thread = '.$id
			);
			
			switch($id_contact) {
				case 2: $type = "Ticket per amministrazione - contabilita"; break;
				case 3: $type = "Ticket rivenditori"; break;
				case 4: $type = "Ticket per assistenza"; break;
				case 5: $type = "Ticket servizio clienti e ufficio vendite"; break;
				case 6: $type = "Ticket ti richiamiamo noi"; break;
				case 7: $type = "Messaggio semplice"; break;
				case 8: $type = "Ticket per amministrazione - ordini"; break;
				default: $type = "Messaggio da modulo di contatto"; break;
			}
		}
		else if($tipo == 'P' || $tipo == 'R'){
			if($tipo == 'P')
				$oggetto = 'Richiesta preventivo n. '.Customer::trovaSigla($id, $tipo);
			else
				$oggetto = 'Ti richiamiamo noi n. '.Customer::trovaSigla($id, $tipo);

			$row_fpt =  Db::getInstance()->getRow('
				SELECT date_add, date_upd, id_employee, status
				FROM form_prevendita_thread
				WHERE id_thread = '.$id
			);

			$date_add = $row_fpt['date_add'];
			$date_upd = $row_fpt['date_upd'];
			$in_carico_a = $row_fpt['id_employee'];
			$status = $row_fpt['status'];
			
			$aperto_da = Db::getInstance()->getValue('
				SELECT id_employee 
				FROM form_prevendita_message 
				WHERE id_thread = '.$id.' 
				ORDER BY id_message ASC
			');
				
			if($aperto_da != 0)
				$aperto_da = Db::getInstance()->getValue('
					SELECT CONCAT(firstname," ",lastname) 
					FROM '._DB_PREFIX_.'employee
					WHERE id_employee = '.$aperto_da
				);
			else
				$aperto_da = 'Cliente';
			
			$conversazione = Db::getInstance()->executeS('
				SELECT id_employee AS mittente, date_add AS data, message AS messaggio 
				FROM form_prevendita_message 
				WHERE id_thread = '.$id
			);
			
			if($tipo == 'P')
				$type = 'Richiesta preventivo';
			else
				$type = 'Ti richiamiamo noi';
		}	
		else {
			$oggetto = 'To-do n. '.Customer::trovaSigla($id, $tipo);

			$row_at =  Db::getInstance()->getRow('
				SELECT date_add, date_upd, action_to, status
				FROM action_thread
				WHERE id_action = '.$id
			);

			$date_add = $row_at['date_add'];
			$date_upd = $row_at['date_upd'];
			$in_carico_a = $row_at['action_to'];
			$status = $row_at['status'];

			$aperto_da = Db::getInstance()->getValue('
				SELECT action_m_from 
				FROM action_message 
				WHERE id_action = '.$id.' 
				ORDER BY id_action_message ASC
			');

			$aperto_da = Db::getInstance()->getValue('
				SELECT CONCAT(firstname," ",lastname) 
				FROM employee 
				WHERE id_employee = '.$aperto_da
			);

			$conversazione = Db::getInstance()->executeS('
				SELECT action_m_from AS mittente, date_add AS data, action_message AS messaggio 
				FROM action_message 
				WHERE id_action = '.$id
			);

			$type = Db::getInstance()->getValue('
				SELECT action_type 
				FROM action
				WHERE id_action = '.$id
			);
		}

		// correggere: se conviene, metterlo in join di $row_...
		$in_carico_a = Db::getInstance()->getValue('
			SELECT CONCAT(firstname," ",lastname) 
			FROM '._DB_PREFIX_.'employee 
			WHERE id_employee = '.$in_carico_a
		);
		
		switch($status) {
			case 'open': $stato = 'Aperto'; break;
			case 'pending1': $stato = 'In lavorazione'; break;
			case 'pending2': $stato = 'In lavorazione'; break;
			case 'closed': $stato = 'Chiuso'; break;
		}
		
		$pdf_conversazione = '
			<page footer="page">
				<page_header>
				</page_header> 
				<page_footer>
				</page_footer>
				<div id="contenitore" style="width:770px; height:1070px; font-family: Arial, Verdana, Sans-Serif; display:block; position:relative; margin:0 auto">
					<div style="width:770px">
						<img src="https://www.ezdirect.it/img/intestazione.jpg" alt="Ezdirect" title="Ezdirect"  style="display:block; margin:0px" />
					</div>
					<h1 style="text-align:left; font-size:22px; font-weight:normal; display:block; width:210px; height:24px; background-color:#e6e6fa">
						<em>'.$oggetto.'</em>
					</h1>
					<table style="width:715px; border-top:1px solid black; border-bottom:1px solid black; display:block; margin:0 auto">
						<tr style="font-size:9px">
						<td style="width:119px"><em>Aperto da</em></td>
						<td style="width:119px"><em>In carico a</em></td>
						<td style="width:119px"><em>Data apertura</em></td>
						<td style="width:119px"><em>Ultima comunicazione</em></td>
						<td style="width:180px"><em>Tipo ticket</em></td>
						<td style="width:59px"><em>Status</em></td>
						</tr>
						<tr style="font-size:12px; font-weight:bold">
						<td style="width:119px">'.$aperto_da.'</td>
						<td style="width:119px">'.$in_carico_a.'</td>
						<td style="width:119px">'.Tools::displayDate($date_add, 5, true).'</td>
						<td style="width:119px">'.Tools::displayDate($date_upd, 5, true).'</td>
						<td style="width:180px">'.$type.'</td>
						<td style="width:59px">'.$stato.'</td>
						</tr>
					</table>
				
					<h2 style="text-align:left; font-size:22px; font-weight:normal; display:block; width:210px; height:24px; background-color:#e6e6fa">
						<em>Comunicazioni</em>
					</h2>
				
					<table style="width:750px">
		';
		
		$i = 0;
		foreach($conversazione as $messaggio) {
			$pdf_conversazione .= '<tr><td style="width:100%; border:1px solid #d4d4d4; padding:8px; '.($i % 2 == 0 ? '' : 'background-color:#ebebeb' ).'">Inviato da '.$messaggio['mittente'].' in data '.date('d/m/Y H:i:s', strtotime($messaggio['data'])).'<br /><br />
			'.html_entity_decode(mb_convert_encoding($messaggio['messaggio'], "UTF-8", "HTML-ENTITIES")).'
			</td>
			</tr>';
			$i++;
		}
		
		$pdf_conversazione .= '
					</table>
				</div>
			</page>
		';
		
		return $pdf_conversazione;
	}

	/* FINE funzioni da controllare */

	

	public static function HierarchyFirst($id, $tipo)
	{	
		switch($tipo) {
			case 'T': 
				$f = Db::getInstance()->getValue('
					SELECT riferimento 
					FROM '._DB_PREFIX_.'customer_thread 
					WHERE id_customer_thread = '.$id
				); 
				break;
			case 'A': 
				$f = Db::getInstance()->getValue('
					SELECT riferimento 
					FROM action_thread 
					WHERE id_action = '.$id
				); 
				break;
			case 'P': 
				$f = Db::getInstance()->getValue('
					SELECT riferimento 
					FROM form_prevendita_thread 
					WHERE id_thread = '.$id
				); 
				break;
			case 'L': 
				$f = Db::getInstance()->getValue('
					SELECT riferimento 
					FROM '._DB_PREFIX_.'bdl 
					WHERE id_bdl = '.$id
				); 
				break;
			case 'AE': 
				$f = Db::getInstance()->getValue('
					SELECT riferimento 
					FROM action_thread_employee 
					WHERE id_action = '.$id
				); 
				break;
			case 'O': 
				return 'O'.$id;
			case 'C': 
				$f = Db::getInstance()->getValue('
					SELECT rif_prev 
					FROM '._DB_PREFIX_.'cart 
					WHERE id_cart = '.$id
				); 
				if($f != 0) { 
					if(is_numeric(substr($f,0,1))) { 
						$f = 'P'.$f; 
					} 
				} 
				break;
			default: 
				return $tipo.$id; 
				break;
		}
		
		if($f == '0' || $f == 'O' || $f == 'C' || $f == 'T' || $f == 'A' || $f == 'P' || $f == 'L')
			$f = '';

		if($f != '') {
			$tipo = substr($f,0,1);
			return Customer::HierarchyFirst(substr($f,1),$tipo);
		}
		else {
			return $tipo.$id;
		}
	}

	/*
	// eliminabile?
	public function findHierarchyLevel($id, $tipo, $level = 0)
	{
		switch($tipo) {
			case 'T': 
				$f = Db::getInstance()->getValue('
					SELECT riferimento 
					FROM customer_thread 
					WHERE id_customer_thread = '.$id
				); 
				break;
			case 'A': 
				$f = Db::getInstance()->getValue('
					SELECT riferimento 
					FROM action_thread 
					WHERE id_action = '.$id
				);
				break;
			case 'P': 
				$f = Db::getInstance()->getValue('
					SELECT riferimento 
					FROM form_prevendita_thread 
					WHERE id_thread = '.$id
				); 
				break;
			case 'L': 
				$f = Db::getInstance()->getValue('
					SELECT riferimento 
					FROM bdl 
					WHERE id_bdl = '.$id
				); 
				break;
			case 'O': 
				return 'O'.$id;
			case 'C': 
				$f = Db::getInstance()->getValue('
					SELECT rif_prev 
					FROM cart 
					WHERE id_cart = '.$id
				); 
				if($f != 0) { 
					if(is_numeric(substr($f,0,1))) { 
						$f = 'P'.$f; 
					} 
				} 
				break;
			default: 
				return $level; 
				break;
		}
		
		if($f == '0' || $f == 'O' || $f == 'C' || $f == 'T' || $f == 'A' || $f == 'P' || $f == 'L')
			$f = '';
		
		if($f != ''){
			$tipo = substr($f,0,1);
			return 2;
		}
		else{
			return 3;
		}
	}
	*/
	
	public static function HierarchyFindChildren($id, $children_final, $current, $format = 'list')
	{
		if($id == '0' || $id == 'O' || $id == 'C' || $id == 'T' || $id == 'A' || $id == 'AE' || $id == 'P' || $id == 'L')
			$id = '';
		
		switch (strtoupper(substr($id,0,1))) {
			case 'A': 
				$tipods = 'to-do'; 
				$id_customer = Db::getInstance()->getValue('
					SELECT id_customer 
					FROM action_thread 
					WHERE id_action = '.substr($id,1)
				); 
				break;
			case 'AE': 
				$tipods = 'to-do-employee'; 
				$id_customer = Db::getInstance()->getValue('
					SELECT id_customer 
					FROM action_thread_employee 
					WHERE id_action = '.substr($id,1)
				); 
				break;
			case 'T': 
				$tipods = 'ticket'; 
				$id_customer = Db::getInstance()->getValue('
					SELECT id_customer 
					FROM '._DB_PREFIX_.'customer_thread 
					WHERE id_customer_thread = '.substr($id,1)
				); 
				break;
			case 'P': 
				$tipods = 'preventivo'; 
				$id_customer = Db::getInstance()->getValue('
					SELECT id_customer 
					FROM form_prevendita_thread 
					WHERE id_thread = '.substr($id,1)
				); 
				break;
			case 'C': 
				$tipods = 'carrello'; 
				$id_customer = Db::getInstance()->getValue('
					SELECT id_customer 
					FROM '._DB_PREFIX_.'cart 
					WHERE id_cart = '.substr($id,1)
				); 
				break;
			case 'O': 
				$tipods = 'ordine'; 
				$id_customer = Db::getInstance()->getValue('
					SELECT id_customer 
					FROM '._DB_PREFIX_.'orders 
					WHERE id_order = '.substr($id,1)
				); 
				break;
			case 'L': 
				$tipods = 'bdl'; 
				$id_customer = Db::getInstance()->getValue('
					SELECT id_customer 
					FROM '._DB_PREFIX_.'bdl 
					WHERE id_bdl = '.substr($id,1)
				); 
				break;
			default: 
				$tipods = ''; 
				$id_customer = 0; 
				break;
		}
		
		if($id != '') {
			$start_time = time();
			
			if((time() - $start_time) > 30) {
				return ('timeout'); // timeout, function took longer than 300 seconds
			}
			
			$children_t = Db::getInstance()->executeS('
				SELECT id_customer_thread 
				FROM '._DB_PREFIX_.'customer_thread 
				WHERE riferimento = "'.$id.'"
			');
			
			$children_a = Db::getInstance()->executeS('
				SELECT id_action 
				FROM action_thread 
				WHERE riferimento = "'.$id.'"
			');
			
			$children_ae = Db::getInstance()->executeS('
				SELECT id_action 
				FROM action_thread_employee 
				WHERE riferimento = "'.$id.'"
			');
			
			$children_p = Db::getInstance()->executeS('
				SELECT id_thread 
				FROM form_prevendita_thread 
				WHERE riferimento = "'.$id.'"
			');
			
			$children_c = Db::getInstance()->executeS('
				SELECT id_cart 
				FROM '._DB_PREFIX_.'cart 
				WHERE (rif_prev = "'.$id.'" OR rif_prev = "'.substr($id,1).'")
			');
			
			$children_bdl = Db::getInstance()->executeS('
				SELECT id_bdl 
				FROM '._DB_PREFIX_.'bdl 
				WHERE riferimento = "'.$id.'"
			');
			
			$children = array();
			
			if($format == 'list')
				$list = '<ul>';
			
			foreach($children_t as $ct)
				$children[] = 'T'.$ct['id_customer_thread'];
			foreach($children_a as $ca)
				$children[] = 'A'.$ca['id_action'];
				foreach($children_ae as $cae)
				$children[] = 'AE'.$cae['id_action'];
			foreach($children_p as $cp)
				$children[] = 'P'.$cp['id_thread'];
			foreach($children_c as $cc)
				$children[] = 'C'.$cc['id_cart'];
			foreach($children_bdl as $cbdl)
				$children[] = 'L'.$cbdl['id_bdl'];
				
			if(!empty($children)) {
				foreach($children as $c) {
					$children_final[] = $c;
				}
				foreach($children as $c) {
					// <a href="#" '.($c == $current ? 'style="background-color:#f5a0a0"' : '').'>
					if($format == 'list') {	
						$list .= '<li>'.Customer::trovaSiglaLinkPerAlbero(substr($c,1), substr($c,0,1), $current).'';
					}
					else { // correggere la funzione!
						$list .= Customer::BuildListingReturnString($id_customer, $tipods, substr($c,1));
					}
					$list .= Customer::HierarchyFindChildren($c, $children_final, $current, $format);
				}
				
				if($format == 'list')
					$list .= '</li>';
			}

			if($format == 'list')
				$list .= '</ul>';
		}
		else
			$list = '';
		
		return $list;
	}

	public static function Storico($id, $tipo_att, $id_employee, $desc, $in_carico = 0)
	{
		if(!is_numeric($desc)){
			$string = true;
			$ins_desc_string = $desc;
			$desc = 0;
		}
		else
			$string = false;

		switch($desc){
			case 0:  $ins_desc = $desc; break;
			case 1:  $ins_desc = 'Ha letto per la prima volta questa attivit&agrave;'; break;
			case 2:  $ins_desc = 'Ha messo in carico l\'attivit&agrave; a '.$in_carico; break;
			case 3:  $ins_desc = 'Ha messo in status APERTO'; break;
			case 4:  $ins_desc = 'Ha messo in status IN LAVORAZIONE'; break;
			case 5:  $ins_desc = 'Ha messo in status CHIUSO'; break;
			case 6:  $ins_desc = 'Ha fatto modifiche'; break;
			case 7:  $ins_desc = 'Ha inviato una comunicazione al cliente'; break;
			case 8:  $ins_desc = 'Ha inviato una comunicazione allo staff'; break;
			case 9:  $ins_desc = 'Ha creato una revisione'; break;
			case 10: $ins_desc = 'Ha cambiato lo status'; break;
			case 11: $ins_desc = 'Ha aperto il ticket per conto del cliente'; break;
			case 12: $ins_desc = 'Ha reinviato una comunicazione al cliente'; break;
			case 13: $ins_desc = 'Ha creato l\'attivit&agrave;'; break;
			case 14: $ins_desc = 'Ha letto l\'attivit&agrave;'; break;
			case 15: $ins_desc = 'Ha aggiunto una comunicazione'; break;
			case 16: $ins_desc = 'Ha modificato un messaggio'; break;
			case 17: $ins_desc = 'Ha aperto il ticket'; break;
			case 18: $ins_desc = 'Ha aperto la richiesta'; break;
			case 19: $ins_desc = 'Ha creato il carrello/offerta'; break;
			case 20: $ins_desc = 'Ha cancellato una revisione'; break;
			case 21: $ins_desc = 'Ha letto una revisione'; break;
			case 22: $ins_desc = 'Ha notificato il carrello al cliente'; break;
			case 23: $ins_desc = 'Ha modificato il carrello originariamente creato dal cliente'; break;
			case 24: $ins_desc = 'Ha cancellato uno o pi&ugrave; prodotti dal carrello'; break;
			case 25: $ins_desc = 'Ha visualizzato il carrello/offerta'; break;
			case 26: $ins_desc = 'Ha visualizzato il PDF di questo/a carrello/offerta'; break;
			case 27: $ins_desc = 'Ha inviato un messaggio al cliente'; break;
			case 28: $ins_desc = 'Ha modificato il contratto'; break;
			case 29: $ins_desc = 'Ha modificato l\'ordine'; break;
			default: $ins_desc = $desc; break;	
		}

		$execute = false;
		
		if($desc == 14) {
			$data_att = Db::getInstance()->getValue('
				SELECT data_attivita 
				FROM storico_attivita 
				WHERE id_attivita = '.$id.' 
					AND tipo_attivita = "'.$tipo_att.'" 
					AND id_employee = "'.$id_employee.'"
				ORDER BY data_attivita DESC
			');
			
			$from_time = strtotime($data_att);
			$to_time = strtotime(date('Y-m-d H:i:s'));
			$minutes_diff = round(abs($to_time - $from_time) / 60, 2);
			
			if($minutes_diff > 5)
				$execute = true;
		}
		else {
			$execute = true;
		}

		if($string)
			$ins_desc = $ins_desc_string;

		if($execute){
			Db::getInstance()->execute('
				INSERT INTO storico_attivita (id_storico, id_attivita, tipo_attivita, tipo_num, data_attivita, id_employee, desc_attivita)
				VALUES (NULL, "'.$id.'", "'.$tipo_att.'", '.$desc.', "'.date('Y-m-d H:i:s').'", '.$id_employee.', "'.$ins_desc.'")
			');
		}
	}

	public static function trovaSiglaLinkPerAlbero($id_thread, $tipo, $current, $vaiechiudi = 'n')
	{
		$context = Context::getContext();
		
		$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$context->employee->id);
		$tokenCarts = Tools::getAdminToken('AdminCarts'.(int)Tab::getIdFromClassName('AdminCarts').(int)$context->employee->id);
		$tokenOrders = Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$context->employee->id);
		
		$date_add = '';
		$aperto_da = '';
		
		if($tipo == 'T') {

			$query_t = Db::getInstance()->getRow("
				SELECT id_contact, date_add, id_customer, status
				FROM "._DB_PREFIX_."customer_thread 
				WHERE id_customer_thread = ".$id_thread
			);

			$contact = $query_t['id_contact'];
			$date_add = date('d/m/Y H:i:s',strtotime($query_t['date_add']));
			$customer = $query_t['id_customer'];
			$status = $query_t['status'];
			
			$data = $query_t['date_add'];
			$annot = substr($data,2,2);

			$aperto_da = Db::getInstance()->getValue("
				SELECT CONCAT(firstname,' ',lastname) 
				FROM "._DB_PREFIX_."employee 
				WHERE id_employee IN (
					SELECT id_employee 
					FROM "._DB_PREFIX_."customer_thread
					WHERE id_customer_thread = ".$id_thread."
				)
			");
			
			if($aperto_da == '')
				$aperto_da = 'Cliente';
						
			$ultimomess = Db::getInstance()->getValue("
				SELECT message 
				FROM "._DB_PREFIX_."customer_message 
				WHERE id_customer_thread = ".$id_thread." 
				ORDER BY id_customer_message DESC
			");

			switch ($status) {
				case 'pending1':
					$status_light = 'class="icon-circle" style="color:orange"';
					break;
				case 'open':
					$status_light = 'class="icon-circle" style="color:red"';
					break;
				case 'closed':
					$status_light = 'class="icon-circle" style="color:green"';
					break;
				
				default:
					$status_light = '';
					break;
			}

			switch ($contact) {
				case 2:
					$siglat = 'A';
					break;
				case 3:
					$siglat = 'V';
					break;
				case 4:
					$siglat = 'T';
					break;
				case 6:
					$siglat = 'R';
					break;
				case 7:
					$siglat = 'M';
					break;
				case 8:
					$siglat = 'D';
					break;
				case 9:
					$siglat = 'S';
					break;

				default:
					break;
			}
			
			if($siglat == 'M')
				$link = 'index.php?controller=AdminCustomers&id_customer='.$customer.'&viewcustomer&tab_name=actions&azione=actions&viewmessage&id_mex='.$id_thread.'&token='.$tokenCustomers;
			else
				$link = 'index.php?controller=AdminCustomers&id_customer='.$customer.'&viewcustomer&tab_name=tickets&azione=tickets&viewticket&id_customer_thread='.$id_thread.'&token='.$tokenCustomers;
			
			$idticketunivoco = $siglat.$annot.$id_thread;
			$tipo2 = $contact;
		}
		else if($tipo == 'P') {

			$query_p = Db::getInstance()->getRow("
				SELECT tipo_richiesta, date_add, id_customer, status
				FROM form_prevendita_thread
				WHERE id_thread = ".$id_thread
			);

			$contact = $query_p['tipo_richiesta'];
			$date_add = date('d/m/Y H:i:s',strtotime($query_p['date_add']));
			$customer = $query_p['id_customer'];
			$status = $query_p['status'];

			$data = $query_p['date_add'];
			$annot = substr($data,2,2);
			
			$aperto_da = Db::getInstance()->getValue("
				SELECT CONCAT(firstname,' ',lastname) 
				FROM "._DB_PREFIX_."employee 
				WHERE id_employee IN (
					SELECT id_employee 
					FROM form_prevendita_thread 
					WHERE id_thread = ".$id_thread."
				)
			");
			
			if($aperto_da == '')
				$aperto_da = 'Cliente';
			
			switch ($status) {
				case 'pending1':
					$status_light = 'class="icon-circle" style="color:orange"';
					break;
				case 'open':
					$status_light = 'class="icon-circle" style="color:red"';
					break;
				case 'closed':
					$status_light = 'class="icon-circle" style="color:green"';
					break;
				
				default:
					$status_light = '';
					break;
			}

			switch ($contact) {
				case 'preventivo':
					$siglat = 'P';
					break;
				case 'tirichiamiamonoi':
					$siglat = 'R';
					break;
				
				default:
					break;
			}
			
			$link = 'index.php?controller=AdminCustomers&id_customer='.$customer.'&viewcustomer&tab_name=actions&azione=actions&viewpreventivo&id_thread='.$id_thread.'&token='.$tokenCustomers;
			
			$idticketunivoco = $siglat.$annot.$id_thread;
			$tipo2 = $contact;
		}
		else if($tipo == 'A') {

			$query_a = Db::getInstance()->getRow("
				SELECT date_add, id_customer, status
				FROM action_thread
				WHERE id_action = ".$id_thread
			);

			$date_add = date('d/m/Y H:i:s',strtotime($query_a['date_add']));
			$customer = $query_a['id_customer'];
			$status = $query_a['status'];
			
			$aperto_da = Db::getInstance()->getValue("
				SELECT CONCAT(firstname,' ',lastname) 
				FROM "._DB_PREFIX_."employee 
				WHERE id_employee IN (
					SELECT action_from 
					FROM action_thread 
					WHERE id_action = ".$id_thread."
				)
			");
			
			if($aperto_da == '')
				$aperto_da = 'Sistema';
			
			switch ($status) {
				case 'pending1':
					$status_light = 'class="icon-circle" style="color:orange"';
					break;
				case 'open':
					$status_light = 'class="icon-circle" style="color:red"';
					break;
				case 'closed':
					$status_light = 'class="icon-circle" style="color:green"';
					break;
				
				default:
					$status_light = '';
					break;
			}
			
			$link = 'index.php?controller=AdminCustomers&id_customer='.$customer.'&viewcustomer&tab_name=todo&azione=todo&viewaction&id_action='.$id_thread.'&token='.$tokenCustomers;
			
			$idticketunivoco = 'TODO'.$id_thread;
			$tipo2 = "todo";
		}
		else if($tipo == 'C') {

			$query_c = Db::getInstance()->getRow("
				SELECT date_add, id_customer
				FROM "._DB_PREFIX_."cart
				WHERE id_cart = ".$id_thread
			);

			$date_add = date('d/m/Y H:i:s',strtotime($query_c['date_add']));
			$customer = $query_c['id_customer'];

			$aperto_da = Db::getInstance()->getValue("
				SELECT CONCAT(firstname,' ',lastname) 
				FROM "._DB_PREFIX_."employee
				WHERE id_employee IN (
					SELECT created_by 
					FROM "._DB_PREFIX_."cart 
					WHERE id_cart = ".$id_thread."
				)
			");
			
			if($aperto_da == '')
				$aperto_da = 'Cliente';
			
			$link = 'index.php?controller=AdminCarts&id_cart='.$id_thread.'&viewcart&token='.$tokenCarts;
			//$link = 'index.php?controller=AdminCustomers&id_customer='.$customer.'&viewcustomer&tab_name=carts&id_cart='.$id_thread.'&viewcart&token='.$tokenCustomers;
			
			$idticketunivoco = 'Carrello n. '.$id_thread;
			$tipo2 = "#";
			$id_thread = "#";
		}
		else if($tipo == 'BDL' || $tipo == 'L') {

			$query_bl = Db::getInstance()->getRow("
				SELECT date_add, id_customer, ( 
					CASE 
						WHEN rif_ordine > 0
							THEN 'closed'
						WHEN id_bdl < 384
							THEN 'closed'
						WHEN rif_ordine = 0 AND invio_controllo = 1 AND invio_contabilita = 0
							THEN 'pending1'
						WHEN rif_ordine = 0 AND invio_controllo = 1 AND invio_contabilita = 1
							THEN 'closed'
						WHEN invio_controllo = 2 AND invio_contabilita = 2
							THEN 'closed'
						WHEN rif_ordine = 0 AND invio_controllo = 0
							THEN 'open'
						ELSE 'open'
					END
					) stato 
				FROM "._DB_PREFIX_."bdl
				WHERE id_bdl = ".$id_thread
			);

			$date_add = date('d/m/Y H:i:s',strtotime($query_bl['date_add']));
			$customer = $query_bl['id_customer'];
			$status = $query_bl['stato'];

			$aperto_da = Db::getInstance()->getValue("
				SELECT CONCAT(firstname,' ',lastname) 
				FROM "._DB_PREFIX_."employee 
				WHERE id_employee IN (
					SELECT created_by 
					FROM "._DB_PREFIX_."cart 
					WHERE id_cart = ".$id_thread."
				)
			");
			
			if($aperto_da == '')
				$aperto_da = 'Cliente';
			
			switch ($status) {
				case 'pending1':
					$status_light = 'class="icon-circle" style="color:orange"';
					break;
				case 'open':
					$status_light = 'class="icon-circle" style="color:red"';
					break;
				case 'closed':
					$status_light = 'class="icon-circle" style="color:green"';
					break;
				
				default:
					$status_light = '';
					break;
			}
			
			$link = 'index.php?controller=AdminCustomers&id_customer='.$customer.'&viewcustomer&tab_name=bdl&azione=bdl_edit&id_bdl='.$id_thread.'&token='.$tokenCustomers;
			
			$idticketunivoco = 'BDL n. '.$id_thread;
			$tipo2 = "bdl";
		}
		else if($tipo == 'O') {

			$query_o = Db::getInstance()->getRow("
				SELECT id_customer, id_cart, date_add
				FROM "._DB_PREFIX_."orders 
				WHERE id_order = ".$id_thread."
			");
			
			$customer = $query_o['id_customer'];
			$order_cart = $query_o['id_cart'];
			$date_add = date('d/m/Y H:i:s',strtotime($query_o['date_add']));
			
			if($order_cart){
				$aperto_da = Db::getInstance()->getValue("
					SELECT CONCAT(firstname,' ',lastname) 
					FROM "._DB_PREFIX_."employee 
					WHERE id_employee IN (
						SELECT created_by 
						FROM "._DB_PREFIX_."cart 
						WHERE id_cart = ".$order_cart."
					)
				");
			}
			
			if($aperto_da == '' || !$aperto_da)
				$aperto_da = 'Cliente';
			
			$link = 'index.php?controller=AdminOrders&vieworder&id_order='.$id_thread.'&token='.$tokenOrders;

			$idticketunivoco = 'Ordine n. '.$id_thread;
			$tipo2 = "#";
			$id_thread = "#";
		}
		
		if($vaiechiudi != 'n') {
			$vaiechiudi_data = explode(':',$vaiechiudi);
			$tipo_cur = $vaiechiudi_data[1];
			$rife_cur = $vaiechiudi_data[2];
			$stat_cur = $vaiechiudi_data[3];
		}
		else {	
			$tipo_cur = '';
			$rife_cur = '';
			$stat_cur = '';
		}
		
		return '<a id="'.$id_thread.'" class="actions_ticket_tr" rel="'.$tipo2.'" href="'.$link.'" target="_blank" '.($tipo.$id_thread == $current ? 'style="background-color:#f5a0a0"' : '').'>'.$idticketunivoco.'&nbsp;&nbsp;&nbsp;<i '.$status_light.' alt="'.$status.'" title="'.$status.'"></i></a>'.($vaiechiudi != 'n' && ($stat_cur == 'open' || $stat_cur == 'pending1' || $stat_cur == 'pending2') && ($tipo_cur == 'L' || $tipo_cur == 'A' || $tipo_cur == 'P' || $tipo_cur == 'T') ? ' - <a href="javascript:void(0)" onclick="closethis(\''.$tipo_cur.'\',\''.$rife_cur.'\',\''.$link.'\')">Vai e chiudi azione corrente</a>' : '').'<span style="display:block; float:right; text-align:right; width: 250px;">Data creazione: <strong>'.$date_add.'</strong></span><span style="display:block; float:right; text-align:right; width: 250px;">Aperto da <strong>'.$aperto_da.'</strong></span>';
	}

	public static function BuildSelectForLinking($id_customer, $id_thread, $tipo)
	{
		$select = '';
		
		//ticket
		$ticket = Db::getInstance()->executeS('
			SELECT * 
			FROM '._DB_PREFIX_.'customer_thread
			WHERE id_customer = '.$id_customer.' '.($tipo == 'T' ? 'AND id_customer_thread != '.$id_thread : '').' 
			ORDER BY (CASE WHEN id_contact = 7 THEN 1 ELSE 0 END), id_customer_thread DESC
		');
		
		foreach ($ticket as $t)
			$select .= '<option value="T'.$t['id_customer_thread'].'">'.Customer::trovaSigla($t['id_customer_thread'], 'ticket').'</option>';
			
		//fp
		$fps = Db::getInstance()->executeS('
			SELECT * 
			FROM form_prevendita_thread 
			WHERE id_customer = '.$id_customer.' '.($tipo == 'P' ? 'AND id_thread != '.$id_thread : '').' 
			ORDER BY id_thread DESC
		');
		
		foreach ($fps as $fp)
			$select .= '<option value="P'.$fp['id_thread'].'">'.Customer::trovaSigla($fp['id_thread'], $fp['tipo_richiesta']).'</option>';
		
		//ordini
		$orders = Db::getInstance()->executeS('
			SELECT * 
			FROM '._DB_PREFIX_.'orders 
			WHERE id_customer = '.$id_customer.' '.($tipo == 'O' ? 'AND id_order != '.$id_thread : '').' 
			ORDER BY id_order DESC
		');
		
		foreach ($orders as $o)
			$select .= '<option value="O'.$o['id_order'].'">Ordine n. '.$o['id_order'].'</option>';
			
		//carrelli
		$carts = Db::getInstance()->executeS('
			SELECT * 
			FROM '._DB_PREFIX_.'cart 
			WHERE id_customer = '.$id_customer.' '.($tipo == 'C' ? 'AND id_cart != '.$id_thread : '').' 
			ORDER BY id_cart DESC
		');
		
		foreach ($carts as $c)
			$select .= '<option value="C'.$c['id_cart'].'">Carrello n. '.$c['id_cart'].'</option>';
		
		//azioni
		$azioni = Db::getInstance()->executeS('
			SELECT * 
			FROM action_thread 
			WHERE id_customer = '.$id_customer.' '.($tipo == 'A' ? 'AND id_action != '.$id_thread : '').' 
			ORDER BY id_action DESC
		');
		
		foreach ($azioni as $a)
			$select .= '<option value="A'.$a['id_action'].'">TODO'.$a['id_action'].'</option>';
			
		//bdl
		$bdl = Db::getInstance()->executeS('
			SELECT * 
			FROM '._DB_PREFIX_.'bdl 
			WHERE id_customer = '.$id_customer.' '.($tipo == 'L' ? 'AND id_bdl != '.$id_thread : '').' 
			ORDER BY id_bdl DESC
		');
		
		foreach ($bdl as $l)
			$select .= '<option value="L'.$l['id_bdl'].'">BDL n. '.$l['id_bdl'].'</option>';
		
		return $select;
	}

	// Restituisce la variabile $azioni da inviare al tpl. Correggere to-do-employee e inner join! 
	public static function BuildListing($id_customer, $tipo, $specific = 'n')
	{
		$context = Context::getContext();

		$customer = new Customer($id_customer);
		$currentIndex = 'index.php?controller=AdminCustomers'; // correggere se mettiamo i link direttamente nel tpl
		$tokenCustomers = Tools::getAdminTokenLite('AdminCustomers'); // correggere se mettiamo i link direttamente nel tpl
		
		// Correggere: probabilmente i to-do-employee possono essere eliminati (sentire se li usano)
		if($tipo == 'to-do-employee') { // correggere se decidiamo di usare AdminPlanner
			$tokenCustomers = Tools::getAdminToken('AdminPlanner'.(int)Tab::getIdFromClassName('AdminPlanner').(int)$context->employee->id);
		}

		if($tipo == 'to-do') {	// correggere inner join (non prende l'ultimo messaggio)
			$azioni = Db::getInstance()->ExecuteS("
				SELECT ac.id_action as id, ac.status, ac.action_to as in_carico, ac.action_type as type, ac.id_customer, ac.subject, ac.date_add, ac.date_upd, ac.riferimento, ac.action_date, am.action_message as message
				FROM action_thread ac 
				INNER JOIN (select action_message, id_action, max(date_add) from action_message group by id_action) am ON ac.id_action = am.id_action
				WHERE ac.id_action> 0 ".($specific != 'n' ? 'AND ac.id_action = "'.substr($specific, 1).'"' : "AND ac.id_customer = ".$customer->id."")." ".($context->employee->id_profile == 7 ? ' AND (action_from = '.$context->employee->id.' OR action_to = '.$context->employee->id.')' : '')."
				ORDER BY ac.date_upd DESC
			");
			
			$tab_name = 'todo'; // $tab_container = '10';
			$view = 'action';
			$rel = 'todo';
			$sigla = 'a';
			$message_table = 'action_message';
			$thread_key = 'id_action';
			$message_key = 'id_action_message';
			$message_field = 'action_message';
		}
		else if($tipo == 'to-do-employee') {
			$customer = new Employee($id_customer);
			
			$azioni = Db::getInstance()->ExecuteS("
				SELECT ac.id_action as id, ac.status, ac.action_to as in_carico, ac.action_type as type, ac.id_employee, ac.subject, ac.date_add, ac.date_upd, ac.riferimento, ac.action_date, am.action_message as message
				FROM action_thread_employee ac 
				NATURAL JOIN action_message am
				WHERE id_action> 0  ".($specific != 'n' ? 'AND ac.id_action = "'.substr($specific,1).'"' : "AND (id_employee = ".$customer->id." OR action_to = ".$customer->id." OR id_action IN (
					SELECT id_action_message_employee 
					FROM action_message_employee 
					WHERE id_action_message_employee IN (
						SELECT msg 
						FROM ticket_cc 
						WHERE type LIKE '%ae%' 
							AND CONCAT(';',cc) LIKE '%;".$customer->id.";%')))"
				)." 
				AND am.date_add >= (select max(am2.date_add) from action_message am2 where am2.id_action = am.id_action)
				GROUP BY id_action 
				ORDER BY ac.date_upd DESC
			");
			
			$tab_container = '3';
			$view = 'action';
			$rel = 'todoe';
			$sigla = 'ae';
			$message_table = 'action_message_employee';
			$thread_key = 'id_action';
			$message_key = 'id_action_message_employee';
			$message_field = 'action_message_employee';
		}
		else if($tipo == 'ticket') { // Correggere: l'inner join è sbagliato, non prende l'ultimo messaggio
			$azioni = Db::getInstance()->executeS("
				SELECT ac.id_customer_thread as id, ac.status, ac.id_employee as in_carico, ac.id_contact as type, ac.id_customer, ac.subject, ac.date_add, ac.date_upd, ac.riferimento, cm.message as message 
				FROM "._DB_PREFIX_."customer_thread ac
				INNER JOIN (SELECT message, id_customer_thread, MAX(date_add) FROM "._DB_PREFIX_."customer_message GROUP BY id_customer_thread) cm ON ac.id_customer_thread = cm.id_customer_thread
				WHERE ac.id_contact != 7 
					AND ac.id_customer = ".$customer->id." ".($context->employee->id_profile == 7 ? ' AND (ac.id_employee = '.$context->employee->id.')' : '')." 
				ORDER BY ac.date_upd DESC
			");
			
			$tab_name = 'tickets'; // $tab_container = '6';
			$view = 'ticket';
			$rel = 'ticket';
			$sigla = 't';
			$message_table = _DB_PREFIX_.'customer_message';
			$thread_key = 'id_customer_thread';
			$message_key = 'id_customer_message';
			$message_field = 'message';
		}		
		else if($tipo == 'preventivo') { // Correggere: perchè in_carico è id_employee di thread e non in_carico di message?
			$preventivi = Db::getInstance()->ExecuteS("
				SELECT ft.id_thread as id, ft.status, ft.id_employee as in_carico, ft.tipo_richiesta as type, ft.id_customer, ft.subject, ft.date_add, ft.date_upd, ft.riferimento, fm.message as message
				FROM form_prevendita_thread ft
				INNER JOIN (select message, id_thread, date_add from form_prevendita_message) fm ON ft.id_thread = fm.id_thread
				WHERE ft.id_customer = ".$customer->id." ".($context->employee->id_profile == 7 ? ' AND (ft.id_employee = '.$context->employee->id.')' : '')." 
				AND fm.date_add = (select max(date_add) from form_prevendita_message where id_customer = ".$customer->id." and id_thread = ft.id_thread)
				ORDER BY ft.date_upd DESC
			");
			
			$messaggi = Db::getInstance()->ExecuteS("
				SELECT ac.id_customer_thread as id, ac.status, ac.id_employee as in_carico, ac.id_contact as type, ac.id_customer, ac.subject, ac.date_add, ac.date_upd, ac.riferimento, cm.message as message
				FROM "._DB_PREFIX_."customer_thread ac
				INNER JOIN (select message, id_customer_thread, max(date_add) from "._DB_PREFIX_."customer_message group by id_customer_thread) cm ON ac.id_customer_thread = cm.id_customer_thread
				WHERE ac.id_contact = 7 
					AND ac.id_customer = ".$customer->id." ".($context->employee->id_profile == 7 ? ' AND (ac.id_employee = '.$context->employee->id.')' : '')." 
				ORDER BY ac.date_upd DESC");
			
			$azioni = array_merge($preventivi, $messaggi);
			$date_azioni = array();
			
			foreach ($azioni as $key => $row) {
				$date_azioni[$key] = $row['date_upd'];
			}
			
			array_multisort($date_azioni, SORT_DESC, $azioni);
			
			$tab_name = 'actions'; // $tab_container = '7';
			$view = 'preventivo';
			$rel = 'preventivo';
			$sigla = 'p';
			$message_table = 'form_prevendita_message';
			$thread_key = 'id_thread';
			$message_key = 'id_message';
			$message_field = 'message';
		}

		if(sizeof($azioni)) {

			$employees = Employee::getEmployees();
			
			foreach ($azioni AS &$azione) {
				
				if($tipo == 'preventivo' || $tipo == 'tirichiamiamonoi' || $tipo == 'messaggio'){
					switch($azione['type']) {
						case 'tirichiamiamonoi': 
							$tipo = 'tirichiamiamonoi'; 
							$view = 'preventivo'; 
							$rel = 'preventivo'; 
							$sigla = 'p'; 
							$message_table = 'form_prevendita_message'; 
							$thread_key = 'id_thread'; 
							$message_key = 'id_message';
							$message_field = 'message'; 
							break;
						case 'preventivo':  
							$tipo = 'preventivo'; 
							$view = 'preventivo'; 
							$rel = 'preventivo';
							$sigla = 'p'; 
							$message_table = 'form_prevendita_message'; 
							$thread_key = 'id_thread'; 
							$message_key = 'id_message'; 
							$message_field = 'message'; 
							break;
						case 7: 
							$tipo = 'messaggio'; 
							$view = 'message'; 
							$rel = 'ticket';
							$sigla = 't'; 
							$message_table = _DB_PREFIX_.'customer_message'; 
							$thread_key = 'id_customer_thread'; 
							$message_key = 'id_customer_message'; 
							$message_field = 'message'; 
							break;
						default: 
							$tipo = 'preventivo'; 
							$view = 'preventivo'; 
							$rel = 'preventivo';
							$sigla = 'p'; 
							$message_table = 'form_prevendita_message'; 
							$thread_key = 'id_thread'; 
							$message_key = 'id_message'; 
							$message_field = 'message'; 
							break;
					}
				}
				
				if($tipo == 'messaggio')
					$ctrl_mex = Db::getInstance()->getValue("
						SELECT COUNT(id_customer_message) 
						FROM "._DB_PREFIX_."customer_thread ct 
						JOIN "._DB_PREFIX_."customer_message cm ON ct.id_customer_thread = cm.id_customer_thread 
						WHERE cm.id_employee = 0 
							AND ct.id_customer_thread = ".$azione['id']."
					");	
				else
					$ctrl_mex = 1;
				
				switch($azione['status']) {
					case 'open': 
						$status_azione = "<i class='icon-circle' style='color:red' title='Aperto' /></i>"; 
						$status_azione_sort = 0;
						break;
					case 'closed': 
						$status_azione = "<i class='icon-circle' style='color:green' title='Chiuso' /></i>"; 
						$status_azione_sort = 2;
						break;
					case 'pending1': 
						$status_azione = "<i class='icon-circle' style='color:orange' title='In lavorazione' /></i>"; 
						$status_azione_sort = 1;
						break;
					default: 
						$status_azione = "<i class='icon-circle' style='color:red' title='Aperto' /></i>"; 
						$status_azione_sort = 0;
						break;
				}

				// Variabili aggiunte per return $azione				
				$azione['id_azione'] = Customer::trovaSigla($azione['id'], $tipo);
				$azione['id_ticket'] = $azione['id_azione'];
				$azione['status_azione'] = ($ctrl_mex >= 1 ? $status_azione : '');
				$azione['status_azione_sort'] = ($ctrl_mex >= 1 ? $status_azione_sort : '');
				$azione['tipo'] = Customer::iconaCRM($azione['type']);
				$azione['qta_com'] = Db::getInstance()->getValue("SELECT COUNT(".$message_key.") FROM ".$message_table." WHERE ".$thread_key." = ".$azione['id']."");
				$azione['ultima_com'] = substr(strip_tags(html_entity_decode($azione['message'], ENT_NOQUOTES, 'UTF-8')), 0, 75); // correggere: inserire il mess per intero e impostare l'overflow
				$azione['aperto_il'] = Tools::displayDate($azione['date_add'], (int)($context->language->id), false);
				$azione['modificato_il'] = Tools::displayDate($azione['date_upd'], (int)($context->language->id), false);

				// Modifico $thread_key per href del messaggio
				if($azione['type'] == 7)
					$thread_key = 'id_mex';

				// Aggiunto &azione= per coerenza con le altre azioni in view customers
				$get_azione = $tab_name;
				$azione['href'] = $currentIndex.'&id_customer='.$customer->id.'&viewcustomer&tab_name='.$tab_name.'&azione='.$get_azione.'&view'.$view.'&'.$thread_key.'='.$azione['id'].'&token='.$tokenCustomers;
				// Correggere href di delete in customerthreads con planner dove presente nel resto del codice
				$azione['href_cancella'] = 'index.php?controller=AdminPlanner&id_customer_thread='.$azione['id'].'&del'.(Tools::getIsset('id_employee') ? '&id_employee='.Tools::getValue('id_employee') : '').'&typedel='.$tipo.'&tab_name='.$tab_name.'&token='.Tools::getAdminToken('AdminPlanner'.(int)Tab::getIdFromClassName('AdminPlanner').(int)$context->employee->id).'&backtocustomers='.$customer->id;
				
				$azione['rel'] = ($tipo == 'ticket' || $tipo == 'messaggio' ? $azione['type'] : $rel);

				// Resetto $thread_key del messaggio
				if($azione['type'] == 7)
					$thread_key = 'id_customer_thread';

				// Correggere: perchè un solo form non appare nell'html???
				$azione['status_edit'] = ($ctrl_mex >= 1 ? 
					'<form style="margin-left:5px; display:block;" name="cambiastato'.$sigla.'_'.$azione['id'].'" id="cambiastato'.$sigla.'_'.$azione['id'].'" action="" method="post" />	
						<input type="hidden" name="id_customer" value="'.$azione['id_customer'].'" />
						<input type="hidden" name="'.$thread_key.'" value="'.$azione['id'].'" />
						<input type="hidden" name="back" value="1" />
						<select name="cambiastatus'.$sigla.'_listing" onchange="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { document.forms[\'cambiastato'.$sigla.'_'.$azione['id'].'\'].submit(); } else { this.value=\''.$azione['status'].'\'}">
							<option style="padding-left:30px;" value="closed" '.($azione['status'] == 'closed' ? 'selected="selected"' : '').'>C</option>
							<option style="padding-left:30px;" value="pending1" '.($azione['status'] == 'pending1' ? 'selected="selected"' : '').'>L</option>
							<option style="padding-left:30px;" value="open" '.($azione['status'] == 'open' ? 'selected="selected"' : '').'>A</option>
						</select>
					</form>'
				: '');

				$azione['in_carico_edit'] = '
					<form style="margin-left:5px; display:block;" name="cambiaincarico'.$sigla.'_'.$azione['id'].'" id="cambiaincarico'.$sigla.'_'.$azione['id'].'" action="" method="post" />
						<a id="'.$azione['id'].'"> </a>
						<input type="hidden" name="id_customer" value="'.$azione['id_customer'].'" />
						<input type="hidden" name="id_ticket_univoco" value="'.$azione['id_azione'].'" />
						<input type="hidden" name="back" value="1" />
						<input type="hidden" name="id_thread" value="'.$azione['id'].'" />
						<input type="hidden" name="precedente_incarico" value="'.$azione['in_carico'].'" />
						<input type="hidden" name="tipo" value="'.$tipo.'" />
						<select name="cambiaincarico'.$sigla.'" onchange="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { document.forms[\'cambiaincarico'.$sigla.'_'.$azione['id'].'\'].submit(); } else { this.value=\''.$azione['in_carico'].'\'}">
							<option value="0" '.($azione['in_carico'] == '0' ? 'selected="selected"' : '').'>--Nessuno--</option>
				';

				foreach($employees as $employee)
					$azione['in_carico_edit'] .= '<option value="'.$employee['id_employee'].'" '.($azione['in_carico'] == $employee['id_employee'] ? 'selected="selected"' : '').'>'.$employee['firstname'].' '.substr($employee['lastname'], 0, 1).'</option>';
				
				$azione['in_carico_edit'] .= '
						</select>
					</form>
				';
			}
		}
		
		// Correggere: non esiste più $listing; mettere il link giusto nei tpl
		if($tipo == 'to-do-employee')
		{
			// Correggere
			$listing = str_replace('AdminCustomers', 'AdminPlanner', $listing);
			$listing = str_replace('viewcustomer', 'updateemployee', $listing);
			$listing = str_replace('id_customer', 'id_employee', $listing);
		}
		
		return $azioni;
	}

	// Restituisce listing (come la funzione 1.4). Testare!
	public static function BuildListingReturnString($id_customer, $tipo, $specific = 'n')
	{
		$context = Context::getContext();

		$customer = new Customer($id_customer);
		$currentIndex = 'index.php?controller=AdminCustomers';
		$tokenCustomers = Tools::getAdminTokenLite('AdminCustomers');

		$listing = '';
		
		if($tipo == 'to-do-employee') { // correggere se decidiamo di usare AdminPlanner
			$tokenCustomers = Tools::getAdminToken('AdminPlanner'.(int)Tab::getIdFromClassName('AdminPlanner').(int)$context->employee->id);
		}
						
		if($tipo == 'to-do') {	// Correggere: l'inner join è sbagliato, non prende l'ultimo messaggio
			$azioni = Db::getInstance()->ExecuteS("
				SELECT ac.id_action as id, ac.status, ac.action_to as in_carico, ac.action_type as type, ac.id_customer, ac.subject, ac.date_add, ac.date_upd, ac.riferimento, ac.action_date, am.action_message as message
				FROM action_thread ac 
				INNER JOIN (select action_message, id_action, max(date_add) from action_message group by id_action) am ON ac.id_action = am.id_action
				WHERE ac.id_action > 0 ".($specific != 'n' ? 'AND ac.id_action = "'.substr($specific, 1).'"' : "AND ac.id_customer = ".$customer->id."")." ".($context->employee->id_profile == 7 ? ' AND (action_from = '.$context->employee->id.' OR action_to = '.$context->employee->id.')' : '')."
				ORDER BY ac.date_upd DESC
			");
			
			$tab_name = 'todo'; // $tab_container = '10';
			$view = 'action';
			$rel = 'todo';
			$sigla = 'a';
			$message_table = 'action_message';
			$thread_key = 'id_action';
			$message_key = 'id_action_message';
			$message_field = 'action_message';
		}
		else if($tipo == 'to-do-employee') {	
			$customer = new Employee($id_customer);
			
			$azioni = Db::getInstance()->ExecuteS("
				SELECT ac.id_action as id, ac.status, ac.action_to as in_carico, ac.action_type as type, ac.id_employee, ac.subject, ac.date_add, ac.date_upd, ac.riferimento, ac.action_date, am.action_message as message
				FROM action_thread_employee ac 
				NATURAL JOIN action_message am
				WHERE id_action> 0  ".($specific != 'n' ? 'AND ac.id_action = "'.substr($specific,1).'"' : "AND (id_employee = ".$customer->id." OR action_to = ".$customer->id." OR id_action IN (
					SELECT id_action_message_employee 
					FROM action_message_employee 
					WHERE id_action_message_employee IN (
						SELECT msg 
						FROM ticket_cc 
						WHERE type LIKE '%ae%' 
							AND CONCAT(';',cc) LIKE '%;".$customer->id.";%')))"
				)." 
				AND am.date_add >= (select max(am2.date_add) from action_message am2 where am2.id_action = am.id_action)
				GROUP BY id_action 
				ORDER BY ac.date_upd DESC
			");
			
			$tab_container = '3'; // correggere! cos'è?
			$view = 'action';
			$rel = 'todoe';
			$sigla = 'ae';
			$message_table = 'action_message_employee';
			$thread_key = 'id_action';
			$message_key = 'id_action_message_employee';
			$message_field = 'action_message_employee';
		}
		else if($tipo == 'ticket') { // Correggere: l'inner join è sbagliato, non prende l'ultimo messaggio
			$azioni = Db::getInstance()->executeS("
				SELECT ac.id_customer_thread as id, ac.status, ac.id_employee as in_carico, ac.id_contact as type, ac.id_customer, ac.subject, ac.date_add, ac.date_upd, ac.riferimento, cm.message as message 
				FROM "._DB_PREFIX_."customer_thread ac
				INNER JOIN (SELECT message, id_customer_thread, MAX(date_add) FROM "._DB_PREFIX_."customer_message GROUP BY id_customer_thread) cm ON ac.id_customer_thread = cm.id_customer_thread
				WHERE ac.id_contact != 7 
					AND ac.id_customer = ".$customer->id." ".($context->employee->id_profile == 7 ? ' AND (ac.id_employee = '.$context->employee->id.')' : '')." 
				ORDER BY ac.date_upd DESC
			");
			
			$tab_name = 'tickets'; // $tab_container = '6';
			$view = 'ticket';
			$rel = 'ticket';
			$sigla = 't';
			$message_table = _DB_PREFIX_.'customer_message';
			$thread_key = 'id_customer_thread';
			$message_key = 'id_customer_message';
			$message_field = 'message';
		}		
		else if($tipo == 'preventivo') { // Correggere: perchè in_carico è id_employee di thread e non in_carico di message?
			$preventivi = Db::getInstance()->ExecuteS("
				SELECT ft.id_thread as id, ft.status, ft.id_employee as in_carico, ft.tipo_richiesta as type, ft.id_customer, ft.subject, ft.date_add, ft.date_upd, ft.riferimento, fm.message as message
				FROM form_prevendita_thread ft
				INNER JOIN (select message, id_thread, date_add from form_prevendita_message) fm ON ft.id_thread = fm.id_thread
				WHERE ft.id_customer = ".$customer->id." ".($context->employee->id_profile == 7 ? ' AND (ft.id_employee = '.$context->employee->id.')' : '')." 
				AND fm.date_add = (select max(date_add) from form_prevendita_message where id_customer = ".$customer->id." and id_thread = ft.id_thread)
				ORDER BY ft.date_upd DESC
			");
			
			$messaggi = Db::getInstance()->ExecuteS("
				SELECT ac.id_customer_thread as id, ac.status, ac.id_employee as in_carico, ac.id_contact as type, ac.id_customer, ac.subject, ac.date_add, ac.date_upd, ac.riferimento, cm.message as message
				FROM "._DB_PREFIX_."customer_thread ac
				INNER JOIN (select message, id_customer_thread, max(date_add) from "._DB_PREFIX_."customer_message group by id_customer_thread) cm ON ac.id_customer_thread = cm.id_customer_thread
				WHERE ac.id_contact = 7 
					AND ac.id_customer = ".$customer->id." ".($context->employee->id_profile == 7 ? ' AND (ac.id_employee = '.$context->employee->id.')' : '')." 
				ORDER BY ac.date_upd DESC");
			
			$azioni = array_merge($preventivi, $messaggi);
			$date_azioni = array();
			
			foreach ($azioni as $key => $row) {
				$date_azioni[$key] = $row['date_upd'];
			}
			
			array_multisort($date_azioni, SORT_DESC, $azioni);
			
			$tab_name = 'actions'; // $tab_container = '7';
			$view = 'preventivo';
			$rel = 'preventivo';
			$sigla = 'p';
			$message_table = 'form_prevendita_message';
			$thread_key = 'id_thread';
			$message_key = 'id_message';
			$message_field = 'message';
		}

		if(sizeof($azioni)) {

			$employees = Employee::getEmployees();

			if($specific == 'n') {	
				$listing .= '
				<table id="tabella_listing" cellspacing="0" cellpadding="0" class="table tabella_datatable">
					<thead><tr>
						<th class="center">ID '.($tipo == 'ticket' ? 'Ticket' : 'Azione').'</th>
						<th class="center" style="min-width:80px">Status</th>
						<th style="text-align:left">In carico</th>
						<th class="center">Tipo</th>
						<th style="text-align:center">Q.ta com.</th>
						<th class="center">Ultima com.</th>
						<th class="center sorter-shortDate dateFormat-ddmmyyyy">Aperto il</th>
						<th class="center sorter-shortDate dateFormat-ddmmyyyy">Modificato il</th>
						<th class="center">Azioni</th>
					</tr></thead>
				';
			}
			
			foreach ($azioni AS &$azione) {
				
				if($tipo == 'preventivo' || $tipo == 'tirichiamiamonoi' || $tipo == 'messaggio'){
					switch($azione['type']) {
						case 'tirichiamiamonoi': 
							$tipo = 'tirichiamiamonoi'; 
							$view = 'preventivo'; 
							$rel = 'preventivo'; 
							$sigla = 'p'; 
							$message_table = 'form_prevendita_message'; 
							$thread_key = 'id_thread'; 
							$message_key = 'id_message';
							$message_field = 'message'; 
							break;
						case 'preventivo':  
							$tipo = 'preventivo'; 
							$view = 'preventivo'; 
							$rel = 'preventivo';
							$sigla = 'p'; 
							$message_table = 'form_prevendita_message'; 
							$thread_key = 'id_thread'; 
							$message_key = 'id_message'; 
							$message_field = 'message'; 
							break;
						case 7: 
							$tipo = 'messaggio'; 
							$view = 'message'; 
							$rel = 'ticket';
							$sigla = 't'; 
							$message_table = _DB_PREFIX_.'customer_message'; 
							$thread_key = 'id_customer_thread'; 
							$message_key = 'id_customer_message'; 
							$message_field = 'message'; 
							break;
						default: 
							$tipo = 'preventivo'; 
							$view = 'preventivo'; 
							$rel = 'preventivo';
							$sigla = 'p'; 
							$message_table = 'form_prevendita_message'; 
							$thread_key = 'id_thread'; 
							$message_key = 'id_message'; 
							$message_field = 'message'; 
							break;
					}
				}
				
				if($tipo == 'messaggio')
					$ctrl_mex = Db::getInstance()->getValue("
						SELECT COUNT(id_customer_message) 
						FROM "._DB_PREFIX_."customer_thread ct 
						JOIN "._DB_PREFIX_."customer_message cm ON ct.id_customer_thread = cm.id_customer_thread 
						WHERE cm.id_employee = 0 
							AND ct.id_customer_thread = ".$azione['id']."
					");	
				else
					$ctrl_mex = 1;
				
				switch($azione['status']) {
					case 'open': 
						$status_azione = "<i class='icon-circle' style='color:red' title='Aperto' /></i>"; 
						break;
					case 'closed': 
						$status_azione = "<i class='icon-circle' style='color:green' title='Chiuso' /></i>"; 
						break;
					case 'pending1': 
						$status_azione = "<i class='icon-circle' style='color:orange' title='In lavorazione' /></i>"; 
						break;
					default: 
						$status_azione = "<i class='icon-circle' style='color:red' title='Aperto' /></i>"; 
						break;
				}

				// Variabili aggiunte per return $azione
				$azione['id_azione'] = Customer::trovaSigla($azione['id'], $tipo);
				$azione['id_ticket'] = $azione['id_azione'];
				$azione['status_azione'] = ($ctrl_mex >= 1 ? $status_azione : '');
				$azione['tipo'] = Customer::iconaCRM($azione['type']);
				$azione['qta_com'] = Db::getInstance()->getValue("SELECT COUNT(".$message_key.") FROM ".$message_table." WHERE ".$thread_key." = ".$azione['id']."");
				$azione['ultima_com'] = substr(strip_tags(html_entity_decode($azione['message'], ENT_NOQUOTES, 'UTF-8')), 0, 75); // correggere: inserire il mess per intero e impostare l'overflow
				$azione['aperto_il'] = Tools::displayDate($azione['date_add'], (int)($context->language->id), false);
				$azione['modificato_il'] = Tools::displayDate($azione['date_upd'], (int)($context->language->id), false);

				$azione['rel'] = ($tipo == 'ticket' || $tipo == 'messaggio' ? $azione['type'] : $rel);

				$azione['status_edit']  = ($ctrl_mex >= 1 ? '
					<form style="margin-left:5px; display:block;" name="cambiastato'.$sigla.'_'.$azione['id'].'" id="cambiastato'.$sigla.'_'.$azione['id'].'" action="" method="post" />	
						<input type="hidden" name="id_customer" value="'.$azione['id_customer'].'" />
						<input type="hidden" name="'.$thread_key.'" value="'.$azione['id'].'" />
						<input type="hidden" name="back" value="1" />
						<select name="cambiastatus'.$sigla.'_listing" onchange="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { document.forms[\'cambiastato'.$sigla.'_'.$azione['id'].'\'].submit(); } else { this.value=\''.$azione['status'].'\'}">
							<option style="padding-left:30px;" value="closed" '.($azione['status'] == 'closed' ? 'selected="selected"' : '').'>C</option>
							<option style="padding-left:30px;" value="pending1" '.($azione['status'] == 'pending1' ? 'selected="selected"' : '').'>L</option>
							<option style="padding-left:30px;" value="open" '.($azione['status'] == 'open' ? 'selected="selected"' : '').'>A</option>
						</select>
					</form>' : '')
				;

				$azione['in_carico_edit'] = '
					<form style="margin-left:5px; display:block;" name="cambiaincarico'.$sigla.'_'.$azione['id'].'" id="cambiaincarico'.$sigla.'_'.$azione['id'].'" action="" method="post" />
						<a id="'.$azione['id'].'"> </a>
						<input type="hidden" name="id_customer" value="'.$azione['id_customer'].'" />
						<input type="hidden" name="id_ticket_univoco" value="'.$azione['id_azione'].'" />
						<input type="hidden" name="back" value="1" />
						<input type="hidden" name="id_thread" value="'.$azione['id'].'" />
						<input type="hidden" name="precedente_incarico" value="'.$azione['in_carico'].'" />
						<input type="hidden" name="tipo" value="'.$tipo.'" />
						<select name="cambiaincarico'.$sigla.'" onchange="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { document.forms[\'cambiaincarico'.$sigla.'_'.$azione['id'].'\'].submit(); } else { this.value=\''.$azione['in_carico'].'\'}">
							<option value="0" '.($azione['in_carico'] == '0' ? 'selected="selected"' : '').'>--Nessuno--</option>
				';

				foreach($employees as $employee)
					$azione['in_carico_edit'] .= '<option value="'.$employee['id_employee'].'" '.($azione['in_carico'] == $employee['id_employee'] ? 'selected="selected"' : '').'>'.$employee['firstname'].' '.substr($employee['lastname'], 0, 1).'</option>';
				
				$azione['in_carico_edit'] .= '
						</select>
					</form>
				';

				$listing .= '
					<tr id="'.$azione['id'].'" class="actions_ticket_tr" rel="'.($tipo == 'ticket' || $tipo == 'messaggio' ? $azione['type'] : $rel).'">
				';
				$listing .= '
					<td>'.$azione['id_azione'].' </td>
					<td>'.$azione['status_edit'].'</td>
					<td><a>
				';
				$listing .= $azione['in_carico_edit']; 
				$listing .= '</a></td>';
				$listing .= '<td>'.$azione['tipo'].'</td>';
				
				$nummess = Db::getInstance()->getValue("SELECT COUNT(".$message_key.") FROM ".$message_table." WHERE ".$thread_key." = ".$azione['id']."");

				// Per href messaggio
				if($azione['type'] == 7)
					$thread_key = 'id_mex';

				$href_azione = $currentIndex.'&id_customer='.$customer->id.'&viewcustomer&tab_name='.$tab_name.'&view'.$view.'&'.$thread_key.'='.$azione['id'].'&token='.$tokenCustomers;
				// Correggere: customerthreads o planner?
				$href_cancella = 'index.php?controller=AdminCustomerThreads&id_customer_thread='.$azione['id'].'&del'.(Tools::getIsset('id_employee') ? '&id_employee='.Tools::getValue('id_employee') : '').'&typedel='.$tipo.'&token='.Tools::getAdminToken('AdminCustomerThreads'.(int)Tab::getIdFromClassName('AdminCustomerThreads').(int)$context->employee->id).'&backtocustomers='.$customer->id.'&tab_name='.$tab_name;
				
				$listing .= '<td style="text-align:center"><a href="'.$href_azione.'">'.$nummess.'</a></td>';
				$listing .= '<td><a href="'.$href_azione.'">'.substr(strip_tags(html_entity_decode($azione['message'], ENT_NOQUOTES, 'UTF-8')), 0, 75).'...</a></td>
				<td><a href="'.$href_azione.'">'.Tools::displayDate($azione['date_add'], (int)($context->language->id), false).'</a></td>
				<td><a href="'.$href_azione.'">'.Tools::displayDate($azione['date_upd'], (int)($context->language->id), false).'</a></td>';
				$listing .= '<td>'.($context->employee->id_profile != 8 ? '<a href="'.$href_cancella.'" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }"><img src="../img/admin/delete.gif" alt="Cancella" /></a>' : '').'</td>';
					
				$listing .= '</tr>';

				// Commentato nella 1.4
				/*
				$ultimomess = Db::getInstance()->getValue("SELECT ".$message_field." FROM ".$message_table." WHERE ".$message_field." != '' AND ".$thread_key." = ".$azione['id']." ORDER BY ".$message_key." ".($context->employee->id == 2 ? 'DESC' : 'ASC')." ");
				$dataultimomess = Db::getInstance()->getValue("SELECT date_add FROM (SELECT ".$thread_key.", date_add FROM ".$message_table." WHERE ".$thread_key." = ".$azione['id']." ORDER BY ".$message_key." DESC ) ".$message_table." ORDER BY date_add DESC");

				$first = Customer::HierarchyFirst($azione['id'], $sigla);
				$figli = Customer::HierarchyFindChildren($first, $children, 'A'.$azione['id'], 'table');
				if($figli != '' && $specific == 'n')
				{	
					$listing .= $figli;
				}*/
			}

			if($specific == 'n')
			{
				$listing .= '</table>';
				$listing .= '<br />Clicca sull\'azione per aprirla, leggere i messaggi e rispondere.';
			}
		}
		else
		{
			if($specific == 'n' && ($tipo != 'to-do-employee'))
				$listing .= $customer->firstname.' '.$customer->lastname.' non ha alcuna azione.';
		}
		
		if($tipo == 'to-do-employee')
		{
			// Correggere
			$listing = str_replace('AdminCustomers', 'AdminPlanner', $listing);
			$listing = str_replace('viewcustomer', 'updateemployee', $listing);
			$listing = str_replace('id_customer', 'id_employee', $listing);
		}
		
		return $listing;
	}

	// sarebbe meglio mettere la C minuscola
	public function ControllaPIVA($pi)
	{
		$errore_lunghezza = 0; // a cosa serve?

		// Caratteri non validi (valide solo cifre)
		if( preg_match("/^[0-9]+\$/", $pi) != 1 )
			return false;
			
		$s = 0;

		for( $i = 0; $i <= 9; $i += 2 )
			$s += ord($pi[$i]) - ord('0');

		for( $i = 1; $i <= 9; $i += 2 ){
			$c = 2*( ord($pi[$i]) - ord('0') );
			if( $c > 9 )  $c = $c - 9;
			$s += $c;
		}

		// Se il codice di controllo non corrisponde
		if((10 - $s%10)%10 != ord($pi[10]) - ord('0'))
			return false;
		
		return true;
	}

	// La funzione ControllaCF nella 1.4 è commentata (ritorna true), se non serve usare solo questa e togliere il 2 dal nome e dove viene usata; sarebbe meglio mettere anche la C minuscola
	public function ControllaCF2($cf)
	{
		$cf = strtoupper($cf);
		
		// Caratteri non validi (valide solo lettere e cifre)
		if(preg_match("/^[A-Z0-9]+\$/", $cf) != 1)	
			return false;
		
		if(strlen($cf) == 11)
			return Customer::ControllaPIVA($cf);
		
		$s = 0;

		for($i = 1; $i <= 13; $i += 2){
			$c = $cf[$i];
			if(strcmp($c, "0") >= 0 and strcmp($c, "9") <= 0)
				$s += ord($c) - ord('0');
			else
				$s += ord($c) - ord('A');
		}
		
		for($i = 0; $i <= 14; $i += 2){
			$c = $cf[$i];
			switch($c){
				case '0':  $s += 1;  break;
				case '1':  $s += 0;  break;
				case '2':  $s += 5;  break;
				case '3':  $s += 7;  break;
				case '4':  $s += 9;  break;
				case '5':  $s += 13;  break;
				case '6':  $s += 15;  break;
				case '7':  $s += 17;  break;
				case '8':  $s += 19;  break;
				case '9':  $s += 21;  break;
				case 'A':  $s += 1;  break;
				case 'B':  $s += 0;  break;
				case 'C':  $s += 5;  break;
				case 'D':  $s += 7;  break;
				case 'E':  $s += 9;  break;
				case 'F':  $s += 13;  break;
				case 'G':  $s += 15;  break;
				case 'H':  $s += 17;  break;
				case 'I':  $s += 19;  break;
				case 'J':  $s += 21;  break;
				case 'K':  $s += 2;  break;
				case 'L':  $s += 4;  break;
				case 'M':  $s += 18;  break;
				case 'N':  $s += 20;  break;
				case 'O':  $s += 11;  break;
				case 'P':  $s += 3;  break;
				case 'Q':  $s += 6;  break;
				case 'R':  $s += 8;  break;
				case 'S':  $s += 12;  break;
				case 'T':  $s += 14;  break;
				case 'U':  $s += 16;  break;
				case 'V':  $s += 10;  break;
				case 'W':  $s += 22;  break;
				case 'X':  $s += 25;  break;
				case 'Y':  $s += 24;  break;
				case 'Z':  $s += 23;  break;
			}
		}
		
		// Se il codice di controllo non corrisponde
		if(chr($s%26 + ord('A')) != $cf[15])			
			return false;
		
		return true;
	}
	
	// RIFATTA IN CONTROLLER E TPL
	// Crea il form di add e edit bdl; viene usata anche nei tickets
	/*
	function BDLform($customer, $bdl_ticket = 0){
		
		if(Tools::getIsset('edit_bdl') || $bdl_ticket > 0) {
			if($bdl_ticket > 0)
				$edit_bdl = $bdl_ticket;
			else
				$edit_bdl = Tools::getValue('edit_bdl');
		}
	
		if(Tools::getIsset('edit_bdl') || $bdl_ticket > 0) {
			$bdl = Db::getInstance()->getRow("SELECT * FROM bdl WHERE id_bdl = ".$edit_bdl."");
			
			echo '<strong>Modifica BDL numero '.$edit_bdl.'</strong> <a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&id_bdl='.$edit_bdl.'&getPDFBDL=tecnotel&token='.$cl->token.'&tab-container-1=14" class="button">Genera PDF TecnoTel</a> <a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&id_bdl='.$edit_bdl.'&getPDFBDL=ezdirect&token='.$cl->token.'&tab-container-1=14" class="button">Genera PDF Ezdirect</a>
			
			<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewmessage&aprinuovomessaggio&allega_bdl='.$edit_bdl.'&token='.$cl->token.'&tab-container-1=7" class="button">Invia BDL via mail</a>
			';
		}
		else {
			echo '<strong>Genera nuovo BDL</strong>';
		}

		$dati_ticket = Db::getInstance()->getRow('
			SELECT * 
			FROM customer_thread 
			WHERE id_customer_thread = '.Tools::getValue('id_customer_thread')
		);

		if($dati_ticket['id_customer_thread'] > 0) {					
			if($bdl['descrizione'] == '')
				$bdl['descrizione'] = $dati_ticket['descrizione'];
			
			if($bdl['motivo_chiamata'] == '')
				$bdl['motivo_chiamata'] = $dati_ticket['motivo_chiamata'];
			
			if($bdl['causa_guasto'] == '')
				$bdl['causa_guasto'] = $dati_ticket['causa_guasto'];
			
			if($bdl['guasto'] == '')
				$bdl['guasto'] = $dati_ticket['guasto'];
		}
		
		echo ' '.(Tools::getIsset('riferimento') ? '- <strong>Azione padre</strong>: '.Tools::getValue('riferimento') : '').' '.($bdl['riferimento'] != '' ? '- <strong>Azione padre</strong>: '.Customer::trovaSiglaLinkPerAlbero(substr($bdl['riferimento'],1), substr($bdl['riferimento'],0,1), $bdl['id_bdl']) : '');

		echo '<br /><br />';

		if(Tools::getIsset('edit_bdl') || $bdl_ticket > 0) {
			echo "<form action='".$currentIndex."&id_customer=".$customer->id."&viewcustomer".(Tools::getIsset('viewticket') ? '&viewticket&id_customer_thread='.Tools::getValue('id_customer_thread') : '')."&edit_bdl=".$edit_bdl."&submitted=y&token=".$cl->token."&tab-container-1=14&".(Tools::getIsset('backz') && Tools::getValue('backz') == 'lista' ? '&backz=lista' : '')."' method='post' onsubmit='return checkFormBDL();'>";
		}
		else {
			echo "<form action='".$currentIndex."&id_customer=".$customer->id."&viewcustomer".(Tools::getIsset('viewticket') ? '&viewticket&id_customer_thread='.Tools::getValue('id_customer_thread') : '')."&nuovobdl&submitted=y&riferimento=".Tools::getValue('riferimento')."&token=".$cl->token."&tab-container-1=14' method='post' onsubmit='return checkFormBDL();'>";
			
			$id_bdl = Db::getInstance()->getValue("
				SELECT id_bdl 
				FROM bdl 
				ORDER BY id_bdl DESC
			");
			
			$id_bdl++;
		}

		echo '
		<div class="anagrafica-cliente" style="width:100px">N. BDL 	<input type="text" size="33" name="id_bdl" id="id_bdl" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? $bdl['id_bdl'] : 'Dopo salvataggio').'" readonly="readonly" style="width:100px; background-color:#f0ebd6" /> 
		</div>';

		$indirizzi_bdl = Db::getInstance()->executeS('
			SELECT * 
			FROM address a 
			JOIN state s ON a.id_state = s.id_state 
			WHERE a.active = 1 
				AND a.deleted = 0 
				AND a.id_customer='.$customer->id.'
		'); 

		$indirizzo_fatturazione_bdl = Db::getInstance()->getRow('
			SELECT * 
			FROM address a 
			JOIN state s ON a.id_state = s.id_state 
			WHERE a.active = 1 
				AND a.deleted = 0 
				AND a.fatturazione = 1 
				AND a.id_customer='.$customer->id.'
		'); 

		echo '
		<div class="anagrafica-cliente" style="width:490px">
			Indirizzo di fatturazione<br />
			<input type="hidden" readonly="readonly" name="indirizzo_bdl" value="'.$indirizzo_fatturazione_bdl['id_address'].'" >
			<input type="text" readonly="readonly" name="indirizzo_fatturazione_bdl" style="width:490px" value="'.$indirizzo_fatturazione_bdl['address1'].' - '.$indirizzo_fatturazione_bdl['postcode'].' '.$indirizzo_fatturazione_bdl['city'].' ('.$indirizzo_fatturazione_bdl['iso_code'].')">
		</div>';

		echo '
		<div class="anagrafica-cliente" style="width:230px">Tel. <br />

			<select name="bdl_phone" id="bdl_phone" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? $bdl['phone'] : '').'" style="width:100px; height:21px" onchange="if(this.value != \'Altro\') { document.getElementById(\'bdl_phone_altro\').value = \'\' }"> 
			';
			
			$telefoni_cliente = Db::getInstance()->executeS('SELECT phone, phone_mobile FROM address WHERE id_customer = '.Tools::getValue('id_customer'));
			foreach ($telefoni_cliente as $tel)
			{
				if($tel['phone'] != 0)
					echo '<option value="'.$tel['phone'].'" '.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? ($bdl['phone'] == $tel['phone'] ? 'selected = "selected"' : '') : '').'>'.$tel['phone'].'</option>';
			
				if($tel['phone_mobile'] != 0)
					echo '<option value="'.$tel['phone_mobile'].'" '.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? ($bdl['phone'] == $tel['phone_mobile'] ? 'selected = "selected"' : '') : '').'>'.$tel['phone_mobile'].'</option>';
			}

			echo '
			<option value="Altro" '.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? (substr($bdl['phone'],0,5) == 'Altro' ? 'selected = "selected"' : '') : '').'>Altro</option>
			</select>
			
			Altro: <input style="width:88px; margin-top:-5px" type="text" id="bdl_phone_altro" name="bdl_phone_altro" onkeyup="document.getElementById(\'bdl_phone\').value = \'Altro\';" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? (substr($bdl['phone'],0,5) == 'Altro' ? substr($bdl['phone'],5,strlen($bdl['phone'])) : '') : '').'" />
		</div>';
		
		echo '<div style="clear:both"></div>';
		echo '<br />';
		
		$persone = Db::getInstance()->executeS("
			SELECT * 
			FROM persone 
			WHERE id_customer = '".$customer->id."'
		");
		
		includeDatepicker3(array('data_richiesta', 'data_effettuato'), true);
		
		echo '
		<div class="anagrafica-cliente" style="width:100px">Richiesto il giorno
			<input type="text" size="33" id="data_richiesta" name="data_richiesta" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? date("d-m-Y", strtotime($bdl['data_richiesta'])) : date('d-m-Y')).'" style="width:100px" />
		</div>';
		
		echo '
		<div class="anagrafica-cliente" style="width:50px">Alle ore
			<input type="text" size="33" id="ora_richiesta" name="ora_richiesta" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? date("H:i", strtotime($bdl['data_richiesta'])) : '00:00').'" style="width:50px" />
		</div>';
		
		echo '
		<div class="anagrafica-cliente" style="width:130px">Da<br />
			<select name="richiesto_da" style="width:130px">';
			foreach ($persone as $persona) {
				echo '<option value="'.$persona['id_persona'].'" '.($persona['id_persona'] == $bdl['richiesto_da'] ? 'selected="selected"' : '').'>'.$persona['firstname']." ".$persona['lastname'].'</option>';	
			}
			echo '</select>
		</div>';

		echo '
		<div class="anagrafica-cliente" style="width:100px">Effettuato il giorno
			<input type="text" size="33" id="data_effettuato" name="data_effettuato" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? date("d-m-Y", strtotime($bdl['data_effettuato'])) : '').'" style="width:100px" />
		</div>';
		
		echo '
		<div class="anagrafica-cliente" style="width:50px">Alle ore
			<input type="text" size="33" id="ora_effettuato" name="ora_effettuato" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? date("H:i", strtotime($bdl['data_effettuato'])) : '00:00').'" style="width:50px" />
		</div>';
		
		echo '
		<div class="anagrafica-cliente" style="width:130px">Da<br />
			<select name="effettuato_da" style="width:130px">';
			$employees = Employee::getEmployees();
			foreach ($employees as $employee) {
				if($employee['id_employee'] == 3 || $employee['id_employee'] == 4 || $employee['id_employee'] == 5  || $employee['id_employee'] == 12 || $employee['id_employee'] == 17) {
					echo '<option value="'.$employee['id_employee'].'" '.($employee['id_employee'] == $bdl['effettuato_da'] ? 'selected="selected"' : '').'>'.$employee['name'].'</option>';
				}
			}
			echo '</select>
		</div>';

		$rif_ordini = Db::getInstance()->executeS('SELECT * FROM orders WHERE id_customer = '.$customer->id);
		$tokenOrders = Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).(int)($context->employee->id));

		echo '
		<div class="anagrafica-cliente" style="width:150px"><span id="incluso_in_ordine">'.($bdl['rif_ordine'] > 0 ? '<a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&id_order='.$bdl['rif_ordine'].'&vieworder&token='.$cl->token.'&tab-container-1=4" target="_blank">Incluso in ordine</a>' : 'Incluso in ordine').'</span>: 
			<span class="aiuto" title="<strong>ATTENZIONE</strong>: questo campo va riempito solo se l\'attivit&agrave; prevista dal BDL &egrave; gi&agrave; inclusa in un ordine. Se devi <strong>addebitare nuovi costi al cliente</strong>, crea un nuovo BDL.">
				<img src="https://www.ezdirect.it/themes/ezdirect/img/icon/help.gif" alt="Aiuto"  style="margin-top:-10px" />
			</span><br /> 
			<select style="width:142px" type="text" id="rif_ordine" name="rif_ordine" onchange="aggiorna_link(this.value); if(this.value == 0) {  $(\'#dettaglio_economico_bdl\').show(); $(\'#invii_controlli\').show(); document.getElementById(\'fatturato\').checked = false; document.getElementById(\'da_fatturare\').checked = true; } else { $(\'#dettaglio_economico_bdl\').hide(); $(\'#invii_controlli\').hide(); document.getElementById(\'fatturato\').checked = true; document.getElementById(\'da_fatturare\').checked = false; }">';
			echo '<option value="0" '.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? ($bdl['rif_ordine'] == 0 ? 'selected="selected"' : '') : 'selected="selected"').'>--</option>';
			foreach ($rif_ordini as $ordine) {
				echo '<option value="'.$ordine['id_order'].'" '.((Tools::getIsset('edit_bdl') || $bdl_ticket) > 0 ? ($bdl['rif_ordine'] == $ordine['id_order'] ? 'selected="selected"' : '') : '').'>'.$ordine['id_order'].' del '.Tools::displayDate($ordine['date_add'], 5).' ('.Tools::displayPrice($ordine['total_products'], 1).')</option>';
			}
		echo '</select></div>';
		echo '<div style="clear:both"></div>';
		echo '<br />';
		echo 'Impianto-Ubicazione: <select name="impianto_ubicazione" style="width:250px"> ';
		foreach ($indirizzi_bdl as $indirizzo_bdl) {
		
		echo '<option value="'.$indirizzo_bdl['id_address'].'" '.($indirizzo_bdl['id_address'] == $bdl['impianto_ubicazione'] ? 'selected="selected"' : '').'>'.$indirizzo_bdl['address1'].' - '.$indirizzo_bdl['postcode'].' '.$indirizzo_bdl['city'].' ('.$indirizzo_bdl['iso_code'].')</option>';
		
		}
		echo '<option value="Altro" '.(!is_numeric($bdl['impianto_ubicazione']) || empty($bdl['impianto_ubicazione']) ? 'selected="selected"' : '').'>Altro</option>';
		echo '</select> Se altro, specificare:  <input style="width:365px" type="text" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? (!is_numeric($bdl['impianto_ubicazione']) ? $bdl['impianto_ubicazione'] : '') : '').'" id="impianto_ubicazione_manuale" name="impianto_ubicazione_manuale" /><br /><br />';
		echo '
		<div class="anagrafica-cliente" style="width:150px">Contratto assistenza<br />
			<select name="contratto_assistenza" id="contratto_assistenza" style="width:150px" onchange="
				if(this.value == 1) { } 
				else { 
					document.getElementById(\'manutenzione_straordinaria\').checked = true; 
					document.getElementById(\'manutenzione_ordinaria\').checked = false; 
				}">
			<option value="1" '.($bdl['contratto_assistenza'] == 1 ? 'selected="selected"' : '').' >S&igrave;</option>
			<option value="0" '.($bdl['contratto_assistenza'] == 0 ? 'selected="selected"' : '').'>No</option>
			</select>
		</div>';
		
		echo '
		<div class="anagrafica-cliente" style="width:350px">
		Manutenzione ordinaria (= intervento da contratto di assistenza)<br />
		<input type="checkbox" name="manutenzione_ordinaria" id="manutenzione_ordinaria" '.($bdl['manutenzione_ordinaria'] == 1 ? 'checked="checked"' : '').'  onclick="
		if(document.getElementById(\'contratto_assistenza\').value == 0) { 
			alert(\'Non si puo selezionare la manutenzione ordinaria senza un contratto di assistenza\'); 
			this.checked = false; 
		} 
		else if(document.getElementById(\'contratto_assistenza\').value == 1) {   
			if(this.checked) { 
				document.getElementById(\'manutenzione_straordinaria\').checked = false;  
				$(\'#dettagli_pagamento_bdl\').hide(); 
				document.getElementById(\'pagato\').checked = true; 
				document.getElementById(\'da_pagare\').checked = false;
				document.getElementById(\'fatturato\').checked = true; 
				document.getElementById(\'da_fatturare\').checked = false; 
				$(\'#dettaglio_economico_bdl\').hide(); 
				$(\'#invii_controlli\').hide(); 
				document.getElementById(\'invio_controllo\').checked = true; 
				document.getElementById(\'invio_contabilita\').checked = true; 
			} 
		}
		
		if(this.checked) { 
			document.getElementById(\'manutenzione_straordinaria\').checked = false;
		} 
		else if(this.checked == false) { 
			document.getElementById(\'manutenzione_straordinaria\').checked = true;
			$(\'#dettagli_pagamento_bdl\').show(); 
			$(\'#dettaglio_economico_bdl\').show(); 
			$(\'#invii_controlli\').show(); 
			document.getElementById(\'invio_controllo\').checked = false; 
			document.getElementById(\'invio_contabilita\').checked = false; 
		}" 
		/><br />
		</div>
		';
		
		echo '<div class="anagrafica-cliente" style="width:150px">
		Manutenzione straordinaria<br />
		<input type="checkbox" name="manutenzione_straordinaria" id="manutenzione_straordinaria" '.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? ($bdl['manutenzione_straordinaria'] == 1 ? 'checked="checked"' : '') : 'checked="checked"').' onclick="
			if(this.checked) {  
				$(\'#dettagli_pagamento_bdl\').show(); 
				document.getElementById(\'manutenzione_ordinaria\').checked = false;
				$(\'#dettaglio_economico_bdl\').show();
				$(\'#invii_controlli\').show(); 
				document.getElementById(\'invio_controllo\').checked = false; 
				document.getElementById(\'invio_contabilita\').checked = false;  
			} 
			else if(this.checked == false) {  
				if(document.getElementById(\'contratto_assistenza\').value == 0) { 
					alert(\'Non si puo selezionare la manutenzione ordinaria senza un contratto di assistenza\'); 
					this.checked = true;  
				} 
				else { 
					document.getElementById(\'manutenzione_ordinaria\').checked = true;
				} 
			}" 
			/><br />
		</div>';
		
		echo '<div style="clear:both"></div>';
		echo '<br />';
		echo 'Motivo chiamata: <input style="width:850px" type="text" value="'.$bdl['motivo_chiamata'].'" id="motivo_chiamata" name="motivo_chiamata" /><br /><br />';
		
		echo '<div style="display:block; float:left; width:170px">Descrizione:</div> <select style="width:150px" type="text" value="'.$bdl['descrizione'].'" id="descrizione" name="descrizione" onchange="if(this.value != \'Altro\') { document.getElementById(\'descrizione_altro\').value = \'\' }">
		<option value="Ripristino funzionalita" '.($bdl['descrizione'] == 'Ripristino funzionalita' ? 'selected = "selected"' : '').'>Ripristino funzionalit&agrave;</option>
		<option value="Upgrade firmware" '.($bdl['descrizione'] == 'Upgrade firmware' ? 'selected = "selected"' : '').'>Upgrade firmware</option>
		<option value="Configurazione servizi" '.($bdl['descrizione'] == 'Configurazione servizi' ? 'selected = "selected"' : '').'>Configurazione servizi</option>
		<option value="Configurazione nuovo hardware" '.($bdl['descrizione'] == 'Configurazione nuovo hardware' ? 'selected = "selected"' : '').'>Configurazione nuovo hardware</option>
		<option value="Altro" '.(substr($bdl['descrizione'],0,5) == 'Altro' || ($dati_ticket['id_customer_thread'] <= 0 && $bdl['id_bdl'] < 392 && $bdl['descrizione'] != '--')  ? 'selected = "selected"' : '').'>Altro</option>
		
		</select> Se altro, specificare: <input style="width:413px" type="text" id="descrizione_altro" name="descrizione_altro" value="'.(substr($bdl['descrizione'],0,5) == 'Altro' || ($dati_ticket['id_customer_thread'] <= 0 && $bdl['id_bdl'] < 392 && $bdl['descrizione'] != 'Nessuno') ? (substr($bdl['descrizione'],0,5) == 'Altro' ? substr($bdl['descrizione'],5,strlen($bdl['descrizione'])) : $bdl['descrizione']) : '').'" onkeyup="document.getElementById(\'descrizione\').value = \'Altro\';" /><br /><br />';
		
		echo '<div style="display:block; float:left; width:170px">Guasto riscontrato dal tecnico:</div> <select style="width:150px" type="text" value="'.$bdl['guasto'].'" id="guasto" name="guasto" onchange="if(this.value != \'Altro\') { document.getElementById(\'guasto_altro\').value = \'\' }">
		
		<option value="Nessuna" '.($bdl['guasto'] == 'Nessuna' ? 'selected = "selected"' : '').'>Nessuna</option>
		<option value="Impianto fermo" '.($bdl['guasto'] == 'Impianto fermo' ? 'selected = "selected"' : '').'>Impianto fermo</option>
		<option value="Problemi su linee telefoniche" '.($bdl['guasto'] == 'Problemi su linee telefoniche' ? 'selected = "selected"' : '').'>Problemi su linee telefoniche</option>
		<option value="Problemi di fonia" '.($bdl['guasto'] == 'Problemi di fonia' ? 'selected = "selected"' : '').'>Problemi di fonia</option>
		<option value="Altro" '.(substr($bdl['guasto'],0,5) == 'Altro' || ($dati_ticket['id_customer_thread'] <= 0 && $bdl['id_bdl'] < 392 && $bdl['guasto'] != 'Nessuno')  ? 'selected = "selected"' : '').'>Altro</option>
		
		</select> Se altro, specificare: <input style="width:413px" type="text" id="guasto_altro" name="guasto_altro" value="'.(substr($bdl['guasto'],0,5) == 'Altro' || ($dati_ticket['id_customer_thread'] <= 0 && $bdl['id_bdl'] < 392 && $bdl['guasto'] != 'Nessuno') ? (substr($bdl['guasto'],0,5) == 'Altro' ? substr($bdl['guasto'],5,strlen($bdl['guasto'])) : $bdl['guasto']) : '').'" onkeyup="document.getElementById(\'guasto\').value = \'Altro\';" /><br /><br />';
	
		echo '<div style="display:block; float:left; width:170px">Causa guasto:</div> <select style="width:150px" type="text" value="'.$bdl['causa_guasto'].'" id="causa_guasto" name="causa_guasto" onchange="if(this.value != \'Altro\') { document.getElementById(\'causa_guasto_altro\').value = \'\' }">
		
				<option value="Nessuno" '. ($bdl['causa_guasto'] == 'Nessuno' ? 'selected = "selected"' : '').'>Nessuno</option>
		<option value="Sovratensione" '. ($bdl['causa_guasto'] == 'Sovratensione' ? 'selected = "selected"' : '').'>Sovratensione</option>
		<option value="Dolo/Incuria/Accidentale" '. ($bdl['causa_guasto'] == 'Dolo/Incuria/Accidentale' ? 'selected = "selected"' : '').'>Dolo/Incuria/Accidentale</option>
		<option value="Ossido/Umidita" '. ($bdl['causa_guasto'] == 'Ossido/Umidita' ? 'selected = "selected"' : '').'>Ossido/Umidita</option>
		<option value="Usura" '. ($bdl['causa_guasto'] == 'Usura' ? 'selected = "selected"' : '').'>Usura</option>
		<option value="Non definita" '. ($bdl['causa_guasto'] == 'Non definita' ? 'selected = "selected"' : '').'>Non definita</option>
		<option value="Altro" '. (substr($bdl['causa_guasto'],0,5) == 'Altro' || ($dati_ticket['id_customer_thread'] <= 0 && $bdl['id_bdl'] < 392 && $bdl['causa_guasto'] != 'Nessuno')  ? 'selected = "selected"' : '').'>Altro</option>
		
		</select> Se altro, specificare: <input style="width:413px" type="text" id="causa_guasto_altro" name="causa_guasto_altro" value="'. (substr($bdl['causa_guasto'],0,5) == 'Altro' || ($dati_ticket['id_customer_thread'] <= 0 && $bdl['id_bdl'] < 392 && $bdl['causa_guasto'] != 'Nessuno') ? (substr($bdl['causa_guasto'],0,5) == 'Altro' ? substr($bdl['causa_guasto'],5,strlen($bdl['causa_guasto'])) : $bdl['causa_guasto']) : '').'" onkeyup="document.getElementById(\'causa_guasto\').value = \'Altro\';" /><br /><br />';

		echo 'Descrizione intervento svolto: <textarea id="intervento_svolto" name="intervento_svolto" style="width:850px; height:50px">'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? $bdl['intervento_svolto'] : '').'</textarea><br /><br />';

		echo "<div id='dettaglio_economico_bdl' ".($bdl['rif_ordine'] > 0 ? "style='display:none'" : "").">";
		
		echo '			Seleziona il prodotto da aggiungere: <link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
		<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete2.js"></script>
		<div id="ajax_choose_product" style="padding:6px; padding-top:2px; width:800px;">
			<input type="checkbox" checked="checked" id="prodotti_online" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Online &nbsp;&nbsp;
			<input type="checkbox" id="prodotti_offline" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Offline &nbsp;
			<input type="checkbox" id="prodotti_old" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Old &nbsp;&nbsp;
			<input type="checkbox" id="prodotti_disponibili" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Solo disp. &nbsp;&nbsp;&nbsp;
			
			<script type="text/javascript" src="../js/select2.js"></script>
			
			<script type="text/javascript">
				$(document).ready(function() { 
					$("#auto_marca").select2(); 
					$("#auto_serie").select2(); 
					$("#auto_fornitore").select2();  
					$("#auto_categoria").select2(); 
				});
			</script>
					
			<select id="auto_marca" name="auto_marca" onchange="repopulateSeries(this.value); clearAutoComplete();" style="width:100px">
			<option value="0">Marca...</option>
			';
			
			$marche = Db::getInstance()->executeS('
				SELECT id_manufacturer as id, name
				FROM manufacturer 
				ORDER BY name ASC
			');
			
			foreach($marche as $marca)
				echo '<option value="'.$marca['id_manufacturer'].'">'.$marca['name'].'</option>';
			echo '
			</select>
					
			<select id="auto_serie" name="auto_serie" onchange="clearAutoComplete();" style="width:100px">
			<option value="0">Serie...</option>
			<option value="0">Scegli prima un costruttore</option>
			';
			
			echo '
			</select>
			
			<select id="auto_categoria" name="auto_categoria" onchange="clearAutoComplete();" style="width:100px">
			<option value="0">Categoria...</option>
			';
			
			$categorie = Db::getInstance()->executeS('
				SELECT * 
				FROM category c 
				JOIN category_lang cl ON c.id_category = cl.id_category 
				WHERE cl.id_lang = 5 
					AND c.id_parent = 1 
					ORDER BY cl.name ASC
			');
			
			foreach($categorie as $categoria)
				echo '<option value="'.$categoria['id_category'].'">'.$categoria['name'].'</option>';
			echo '
			</select>
			
			<select id="auto_fornitore" name="auto_fornitore" onchange="clearAutoComplete(); " style="width:100px">
			<option value="0">Fornitore...</option>
			';
			
			$fornitori = Db::getInstance()->executeS('
				SELECT * 
				FROM supplier 
				WHERE id_supplier = 8 
					OR id_supplier = 11 
					OR id_supplier = 42 
				ORDER BY name ASC
			');
			
			foreach($fornitori as $fornitore)
				echo '<option value="'.$fornitore['id_supplier'].'">'.$fornitore['name'].'</option>';
			echo '
			</select>
			
			<input id="reset" type="button" value="Reset" class="button" onclick="document.getElementById(\'prodotti_online\').checked = true; document.getElementById(\'prodotti_offline\').checked = true; document.getElementById(\'prodotti_old\').checked = true; document.getElementById(\'prodotti_disponibili\').checked = false; document.getElementById(\'auto_categoria\').options[0].selected = true; $(\'#auto_categoria\').select2();  document.getElementById(\'auto_marca\').options[0].selected = true; $(\'#auto_marca\').select2();  
			document.getElementById(\'auto_serie\').options[0].selected = true; $(\'#auto_serie\').select2();  
			document.getElementById(\'auto_fornitore\').options[0].selected = true; $(\'#auto_fornitore\').select2(); $(\'#prodlist\').hide(); $(\'div.autocomplete_list\').hide();" />
			
			<br />
			<input id="veditutti" type="button" value="Cerca" class="button" style="display:none" /> 
			<input size="123" type="text" value="" id="product_autocomplete_input" /><br /><br />
			<input id="excludeIds" type="hidden" value="" />
			<input size="123" type="text" value="" id="product_autocomplete_input_interno" style="display:none" />

			echo "<strong>Dettaglio economico dell'intervento</strong><br />";
				
			if((Tools::getIsset('edit_bdl') || $bdl_ticket > 0)) {
				$dettaglio_costi = Db::getInstance()->getValue("SELECT dettaglio_costi FROM bdl WHERE id_bdl = '".$edit_bdl."'");
				$dettaglio_costi = unserialize($dettaglio_costi);
			}
			
			echo '
			<table style="font-size:12px; width:860px" class="table">
			<thead>
			<tr><th style="width:105px">Dettaglio</th><th style="width:105px">Codice</th><th style="width:280px"> Descrizione</th><th>Qta</th><th>Unitario</th><th>Sconto %</th><th>Importo</th>
			</thead>
			<tbody id="dettaglio_costi_bdl">
			<tr><td>Diritto di chiamata</td>
			<td id="codice_diritto_chiamata">'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? $dettaglio_costi['diritto_chiamata_tipo'] : '').'</td>
			<td>
			<select name="diritto_chiamata_tipo" id="diritto_chiamata_tipo" style="width:305px" onmouseup="aggiorna_selezione_costi(\'diritto_chiamata\',\'diritto_chiamata_unitario\',this.value)">
			<option value="" '.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? ($dettaglio_costi['diritto_chiamata_tipo'] == '' ? 'selected="selected"' : '') : '').'>-- Seleziona --</option>
			
			<option value="EZDIRFIX" '.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? ($dettaglio_costi['diritto_chiamata_tipo'] == 'EZDIRFIX' ? 'selected="selected"' : '') : '').'>Fisso intervento < 20 km</option>
			<option value="EZDIRFIXF" '.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? ($dettaglio_costi['diritto_chiamata_tipo'] == 'EZDIRFIXF' ? 'selected="selected"' : '') : '').'>Fisso intervento < 20 km festivo</option>
			</select>
			</td>
			<td>
			<input type="text" name="diritto_chiamata_qta" id="diritto_chiamata_qta" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? number_format($dettaglio_costi['diritto_chiamata_qta'],0,",","") : '').'" onkeyup="moltiplicaCosto(\'diritto_chiamata\'); CalcTotBDL();"  style="width:30px; text-align:right" /></td>
			<td><input type="text" name="diritto_chiamata_unitario" id="diritto_chiamata_unitario" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? number_format($dettaglio_costi['diritto_chiamata_unitario'],2,",","") : '').'" onkeyup="moltiplicaCosto(\'diritto_chiamata\'); CalcTotBDL();"  style="width:50px; text-align:right" /></td>
			<td><input type="text" name="diritto_chiamata_sconto" onkeyup="moltiplicaCosto(\'diritto_chiamata\'); CalcTotBDL();" id="diritto_chiamata_sconto" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? number_format($dettaglio_costi['diritto_chiamata_sconto'],2,",","") : '').'" style="width:50px; text-align:right" /></td>
			<td><input type="text" name="diritto_chiamata" readonly="readonly" id="diritto_chiamata" value="'.(Tools::getIsset('edit_bdl') ? number_format(($dettaglio_costi['diritto_chiamata_unitario']-(($dettaglio_costi['diritto_chiamata_unitario']*$dettaglio_costi['diritto_chiamata_sconto'])/100))*($dettaglio_costi['diritto_chiamata_qta']),2,",","") : '').'" style="width:50px; text-align:right" /> &euro;</td></tr>
			
			<tr><td>Chilometri percorsi</td>
			<td id="codice_chilometri_percorsi">'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? $dettaglio_costi['chilometri_percorsi_tipo'] : '').'</td>
			<td>
			<select name="chilometri_percorsi_tipo" id="chilometri_percorsi_tipo" style="width:305px" onmouseup="aggiorna_selezione_costi(\'chilometri_percorsi\',\'chilometri_percorsi_unitario\',this.value)">
			<option value="" '.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? ($dettaglio_costi['diritto_chiamata_tipo'] == '' ? 'selected="selected"' : '') : '').'>-- Seleziona --</option>
			
			<option	value="EZKMR" '.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? ($dettaglio_costi['chilometri_percorsi_tipo'] == 'EZKMR' ? 'selected="selected"' : '') : '').'>Rimborso km oltre diritto fisso</option>
			<option value="EZZONAFIX" '.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? ($dettaglio_costi['chilometri_percorsi_tipo'] == 'EZZONAFIX' ? 'selected="selected"' : '') : '').'>Chiamata fissa con partner di zona</option>
			
			</select>
			</td>
			
			<td><input type="text" name="chilometri_percorsi_qta" id="chilometri_percorsi_qta" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? number_format($dettaglio_costi['chilometri_percorsi_qta'],0,",","") : '').'" onkeyup="moltiplicaCosto(\'chilometri_percorsi\'); CalcTotBDL();"  style="width:30px; text-align:right" /></td>
			<td><input type="text" name="chilometri_percorsi_unitario" id="chilometri_percorsi_unitario" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? number_format($dettaglio_costi['chilometri_percorsi_unitario'],2,",","") : '').'" onkeyup="moltiplicaCosto(\'chilometri_percorsi\'); CalcTotBDL();" style="width:50px; text-align:right" /></td>
			<td><input type="text" name="chilometri_percorsi_sconto" onkeyup="moltiplicaCosto(\'chilometri_percorsi\'); CalcTotBDL();"  id="chilometri_percorsi_sconto" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? number_format($dettaglio_costi['chilometri_percorsi_sconto'],2,",","") : '').'" style="width:50px; text-align:right" /></td>
			<td><input type="text" name="chilometri_percorsi" readonly="readonly" id="chilometri_percorsi" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ?  number_format(($dettaglio_costi['chilometri_percorsi_unitario']-(($dettaglio_costi['chilometri_percorsi_unitario']*$dettaglio_costi['chilometri_percorsi_sconto'])/100))*($dettaglio_costi['chilometri_percorsi_qta']),2,",","") : '').'" style="width:50px; text-align:right" /> &euro;</td></tr>
			
			<tr><td>Costo manodopera</td>
			<td id="codice_costo_manodopera">'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? $dettaglio_costi['costo_manodopera_tipo'] : '').'</td>
			<td>
			<select name="costo_manodopera_tipo" id="costo_manodopera_tipo" style="width:305px" onmouseup="aggiorna_selezione_costi(\'costo_manodopera\',\'costo_manodopera_unitario\',this.value)">
			<option value="" '.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? ($dettaglio_costi['diritto_chiamata_tipo'] == '' ? 'selected="selected"' : '') : '').'>-- Seleziona --</option>
			<option value="EZTECORD" '.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? ($dettaglio_costi['costo_manodopera_tipo'] == 'EZTECORD' ? 'selected="selected"' : '') : '').'>Tariffa oraria ordinaria</option>
			<option value="EZTEC30" '.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? ($dettaglio_costi['costo_manodopera_tipo'] == 'EZTEC30' ? 'selected="selected"' : '') : '').'>Tariffa 30 minuti ordinaria</option>
			<option value="EZTECXTRA" '.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? ($dettaglio_costi['costo_manodopera_tipo'] == 'EZTECXTRA' ? 'selected="selected"' : '') : '').'>Tariffa oraria straordinaria</option>
			<option value="EZTECFEST" '.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? ($dettaglio_costi['costo_manodopera_tipo'] == 'EZTECFEST' ? 'selected="selected"' : '') : '').'>Tariffa oraria festiva</option>
			</select>
			</td>
			
			<td><input type="text" name="costo_manodopera_qta" id="costo_manodopera_qta" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? number_format($dettaglio_costi['costo_manodopera_qta'],0,",","") : '').'" onkeyup="moltiplicaCosto(\'costo_manodopera\'); CalcTotBDL(); document.getElementById(\'costo_manodopera_qta\').value.replace(\',\',\'\');"  style="width:30px; text-align:right" /></td>
			<td><input type="text" name="costo_manodopera_unitario" id="costo_manodopera_unitario" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? number_format($dettaglio_costi['costo_manodopera_unitario'],2,",","") : '').'" onkeyup="moltiplicaCosto(\'costo_manodopera\'); CalcTotBDL();" style="width:50px; text-align:right" /></td>
			<td><input type="text" name="costo_manodopera_sconto" onkeyup="moltiplicaCosto(\'costo_manodopera\'); CalcTotBDL();" style="width:50px; text-align:right" id="costo_manodopera_sconto" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? number_format($dettaglio_costi['costo_manodopera_sconto'],2,",","") : '').'" style="width:50px; text-align:right" /></td>
			<td><input type="text" name="costo_manodopera" readonly="readonly" id="costo_manodopera" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? number_format(($dettaglio_costi['costo_manodopera_unitario']-(($dettaglio_costi['costo_manodopera_unitario']*$dettaglio_costi['costo_manodopera_sconto'])/100))*($dettaglio_costi['costo_manodopera_qta']),2,",","") : '').'" style="width:50px; text-align:right" /> &euro;</td></tr>';
			
			$num_varie = $dettaglio_costi['num_varie'];
			
			for($i=0; $i<=$num_varie; $i++) {
				if(empty($dettaglio_costi['ricambi_e_varie_'.$i.'_descrizione']) && $dettaglio_costi['ricambi_e_varie_'.$i.'_unitario'] == 0) {
					// ?
				}
				else {
					echo '<tr><td>Ricambi e varie</td><td id="codice_ricambi_e_varie_'.$i.'">'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? $dettaglio_costi['ricambi_e_varie_'.$i.'_tipo'] : '').'</td><td><input type="text" style="width:295px" name="ricambi_e_varie_'.$i.'_descrizione" id="ricambi_e_varie_'.$i.'_descrizione" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? $dettaglio_costi['ricambi_e_varie_'.$i.'_descrizione'] : '').'" />
					<input type="hidden" size="30" name="ricambi_e_varie_'.$i.'_tipo" id="ricambi_e_varie_'.$i.'_tipo" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? $dettaglio_costi['ricambi_e_varie_'.$i.'_tipo'] : '').'" />
					</td><td><input type="text" name="ricambi_e_varie_'.$i.'_qta" id="ricambi_e_varie_'.$i.'_qta" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? number_format($dettaglio_costi['ricambi_e_varie_'.$i.'_qta'],0,",","") : '').'" onkeyup="moltiplicaCosto(\'ricambi_e_varie_'.$i.'\'); CalcTotBDL();"  style="width:30px; text-align:right" /></td>
					<td><input type="text" name="ricambi_e_varie_'.$i.'_unitario" id="ricambi_e_varie_'.$i.'_unitario" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? number_format($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario'],2,",","") : '').'" onkeyup="moltiplicaCosto(\'ricambi_e_varie_'.$i.'\'); CalcTotBDL();" style="width:50px; text-align:right" /></td>
						<td><input type="text" name="ricambi_e_varie_'.$i.'_sconto" onkeyup="moltiplicaCosto(\'ricambi_e_varie_'.$i.'\'); CalcTotBDL();" id="ricambi_e_varie_'.$i.'_sconto" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? number_format($dettaglio_costi['ricambi_e_varie_'.$i.'_sconto'],2,",","") : '').'" style="width:50px; text-align:right" /></td>
						
					<td><input type="text" name="ricambi_e_varie_'.$i.'" readonly="readonly" id="ricambi_e_varie_'.$i.'" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? number_format(($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario']-(($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario']*$dettaglio_costi['ricambi_e_varie_'.$i.'_sconto'])/100))*($dettaglio_costi['ricambi_e_varie_'.$i.'_qta']),2,",","") : '').'" style="width:50px; text-align:right" /> &euro; <a style="cursor:pointer" onclick="rimuovi();"><img src="../img/admin/delete.gif" alt="Rimuovi riga" title="Rimuovi riga" /></a></td></tr>';
				}
			}
			
			$totale_bdl = 0;
			
			if((Tools::getIsset('edit_bdl') || $bdl_ticket > 0)) {
				$totale_bdl += (($dettaglio_costi['diritto_chiamata_unitario']-(($dettaglio_costi['diritto_chiamata_unitario']*$dettaglio_costi['diritto_chiamata_sconto'])/100))*$dettaglio_costi['diritto_chiamata_qta'])+(($dettaglio_costi['chilometri_percorsi_unitario']-(($dettaglio_costi['chilometri_percorsi_unitario']*$dettaglio_costi['chilometri_percorsi_sconto'])/100))*$dettaglio_costi['chilometri_percorsi_qta'])+(($dettaglio_costi['costo_manodopera_unitario']-(($dettaglio_costi['costo_manodopera_unitario']*$dettaglio_costi['costo_manodopera_sconto'])/100))*$dettaglio_costi['costo_manodopera_qta']);
				
				for($i=0; $i<=$num_varie; $i++) {
				
					$totale_bdl += ($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario']-(($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario']*$dettaglio_costi['ricambi_e_varie_'.$i.'_sconto'])/100))*$dettaglio_costi['ricambi_e_varie_'.$i.'_qta'];
				}
			}

			echo '</tbody>
				<tfoot>
					<tr>
					<td colspan="2"><span style="font-size:11px">Tot. Importo intervento IVA escl. S.E. &amp; O.</span></td><td></td><td></td><td></td><td></td><td><span class="tab-span" id="totale_BDL" style="width:50px; margin-right:3px; text-align:right; float:left">'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? number_format(($totale_bdl),2,",","") : '').'</span> &euro;
					</tr>
				</tfoot>
			</table><br />';
			
			echo '<input type="hidden" name="num_varie" id="num_varie" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? $dettaglio_costi['num_varie'] : '0').'" />';
			echo '</div>';
			echo '<table><tr><td>Note:<br /> <input style="width:847px" type="text" value="'.((Tools::getIsset('edit_bdl') || $bdl_ticket > 0) ? $bdl['note'] : '').'" id="note_bdl" name="note_bdl" /></td></tr></table><br />';
			
			echo '<span class="button" onclick="$(\'#mostra_nota_privata\').slideToggle(); " style="cursor: pointer; display:block; color:red; font-size:14px; font-weight:bold; width:852px; border:1px solid #cccccc; text-align:center; background-color:#f0f0ee; text-decoration:none"><img src="../img/admin/arrow.gif" alt="Dettaglio note private" title="Dettaglio note private" style="float:left; margin-right:5px;"/>Clicca qui per inserire note private/riservate - Non visibili all\'utente.</span>';
			
			$bdl_note = Db::getInstance()->executeS('SELECT * FROM note_attivita WHERE tipo_attivita = "B" AND id_attivita = '.$bdl['id_bdl'].'');
				
			
			//echo '<p><div id="mostra_nota_privata" style="'.(strip_tags(str_replace(" ","",$bdl['note_private'])) == '' && count($bdl_note) == 0 ? 'display:none;' : '').' margin-top:-15px">';
			//echo '<table><thead>'.(count($bdl_note) > 0 ? '<tr><th>Nota</th><th>Creata da</th><th>Ultima modifica</th><th></th></tr>' : '' ).'</thead><tbody id="tableNoteBody">';
			
				
			echo '<p><div id="mostra_nota_privata" style="margin-top:-15px">';
				
			echo '<table><thead>'.(count($bdl_note) > 0 ? '<tr><th>Nota</th><th>Creata da</th><th>Ultima modifica</th><th></th></tr>' : '' ).'</thead><tbody id="tableNoteBody">
				';
				
			foreach($bdl_note as $bdl_nota)
			{
				echo '<tr id="tr_note_'.$bdl_nota['id_note'].'">';
				echo '
				<td>
				<textarea class="textarea_note" name="note_nota['.$bdl_nota['id_note'].']" id="note_nota['.$bdl_nota['id_note'].']" style="width:540px; height:16px" onkeyup="auto_grow(this)">'.htmlentities($bdl_nota['note'], ENT_COMPAT, 'UTF-8').'</textarea><input type="hidden" name="note_nuova['.$bdl_nota['id_note'].']" id="note_nuova['.$bdl_nota['id_note'].']" value="0" /></td>
				<td><input type="text" readonly="readonly" name="note_id_employee['.$bdl_nota['id_note'].']" value="'.Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM employee WHERE id_employee = '.$bdl_nota['id_employee']).'" /></td>
				<td><input type="text" readonly="readonly" name="note_date_add['.$bdl_nota['id_note'].']" value="'.Tools::displayDate($bdl_nota['date_upd'], 5).'" /></td>';
				echo '<td><a href="javascript:void(0)" onclick="javascript:var surec=window.confirm(\'Sei sicuro?\'); if (surec) { togli_nota('.$bdl_nota['id_note'].'); cancella_nota_attivita('.$bdl_nota['id_note'].'); } else { return false; }; "><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" style="cursor:pointer" /></a></td>';
				echo'
				</tr>
				';
			}
			
			echo '</tbody></table><br />';
			
			echo '
			<a href="javascript:void(0)" onclick="javascript:add_nota_privata()";><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" /> Clicca per aggiungere una nota privata</a> <br /><br />';
			
			if($bdl['date_add'] < '2017-12-08')
			{
				echo ''.($order->id? '<div style="width:847px; background-color:#f0ebd6; border:1px solid #cccccc; overflow-y:auto; height:150px">'.$note_private.'</div>' : '<textarea name="note_private_bdl" id="note_private_bdl"  style="width:850px; color:red">'.$bdl['note_private'].'</textarea>').'<br />';
			}
			
			echo '
			</div>
			</div>
			<input type="checkbox" name="nessun_addebito" id="nessun_addebito" '.($bdl['nessun_addebito'] == 1 ? 'checked="checked"' : '').' onclick="if(this.checked) { $(\'#dettagli_controlli\').hide(); } else if(this.checked == false) {  $(\'#dettagli_controlli\').show(); }" />Nessun addebito<br /><br />
			';
				
			echo '<div id="dettagli_controlli" '.($bdl['nessun_addebito'] == 1 ? 'style="display:none"' : '').'><div style="float:left; margin-left:0px '.($bdl['contratto_assistenza'] == 1 && $bdl['manutenzione_ordinaria'] == 1 ? ';display:none' : '').'" id="dettagli_pagamento_bdl"><table class="table"><tr><th>Pagato</th><th>Da pagare</th><th>Fatturato</th><th>Da fatturare</th></tr>
			
			<tr>
			<td style="text-align:center"><input type="checkbox" onclick="if(this.checked) { document.getElementById(\'da_pagare\').checked = false; } else if(this.checked == false) {  document.getElementById(\'da_pagare\').checked = true; }" id="pagato" name="pagato" '.($bdl['pagato'] == 1 ? 'checked="checked"' : '').' value="1" /></td>
			<td style="text-align:center"><input type="checkbox" onclick="if(this.checked) { document.getElementById(\'pagato\').checked = false; } else if(this.checked == false) {  document.getElementById(\'pagato\').checked = true; }"id="da_pagare" name="da_pagare" '.($bdl['pagato'] == 0 ? 'checked="checked"' : '').' value="0" /></td>
			<td style="text-align:center"><input type="checkbox" id="fatturato" name="fatturato" '.($bdl['fatturato'] == 1 ? 'checked="checked"' : '').' onclick="if(this.checked) { document.getElementById(\'da_fatturare\').checked = false; } else if(this.checked == false) {  document.getElementById(\'da_fatturare\').checked = true; }" value="1" /></td>
			<td style="text-align:center"><input type="checkbox" id="da_fatturare" name="da_fatturare" '.($bdl['fatturato'] == 0 ? 'checked="checked"' : '').' onclick="if(document.getElementById(\'rif_ordine\').value > 0) { alert(\'Attenzione: esiste un riferimento ordine, dunque il BDL &egrave; gi&agrave; stato fatturato\');  this.checked=false; return false; } else { if(this.checked) { document.getElementById(\'fatturato\').checked = false; } else if(this.checked == false) {  document.getElementById(\'fatturato\').checked = true; } }" value="0" /></td>
			</tr></table></div>';
			
			echo '<div id="invii_controlli" style="float:left; margin-left:70px '.($bdl['contratto_assistenza'] == 1 && $bdl['manutenzione_ordinaria'] == 1 ? ';display:none' : '').' '.($bdl['invio_controllo'] == 2 && $bdl['invio_contabilita'] == 2 ? '; display:none' : '').'">
			<table class="table"><tr><th>Invia per controllo</th>'.($context->employee->id != 1 && $context->employee->id != 6  && $context->employee->id != 22 ? '' : '<th>Conferma per contabilità</th>').'</tr>
			
			<tr>
			<td style="text-align:center">'.($bdl['invio_controllo'] == 1 || $bdl['invio_controllo'] == 2 ? '<input type="hidden" name="invio_controllo" value="2" />Gi&agrave; inviato' : '<input type="checkbox" id="invio_controllo" name="invio_controllo" value="1" /> 
			<span class="aiuto" title="Questo campo va flaggato solo quando si compila un BDL per un\'attivit&agrave; non inclusa in un ordine. Il campo serve per inviare il BDL a Ezio in modo tale che Ezio lo possa vistare per l\'invio in contabilit&agrave;. Se il BDL riguarda un\'attivit&agrave; gi&agrave; contemplate nell\'ordine, non flaggarlo: il sistema operer&agrave; in automatico.">
				<img src="https://www.ezdirect.it/themes/ezdirect/img/icon/help.gif" alt="Aiuto" style="margin-top:-5px" />
			</span><br /> ').'
				
				
			</td>
			'.($context->employee->id != 1 && $context->employee->id != 6 && $context->employee->id != 22 ? ($bdl['invio_contabilita'] == 1  || $bdl['invio_contabilita'] == 2 ? '<input type="hidden" name="invio_contabilita" value="2" />' : '') : '<td style="text-align:center"> '.($bdl['invio_contabilita'] == 1  || $bdl['invio_contabilita'] == 2 ? '<input type="hidden" name="invio_contabilita" value="2" />Gi&agrave; inviato' :  '<input type="checkbox" value="1" id="invio_contabilita" name="invio_contabilita" />').'</td>').'
			</tr></table>
			
			</div></div>';

			if((Tools::getIsset('edit_bdl') || $bdl_ticket > 0)) {
				echo '<div style="float:left; margin-left:50px"><br /><button type="submit" class="button">
				<img src="../img/admin/enabled-2.gif" alt="Conferma" title="Conferma" />&nbsp;&nbsp;&nbsp;Salva
				</button></div>';

				echo '<div style="clear:both"></div>';
				echo '<br /><br /><button class="button" onclick="javascript:var surec=window.confirm(\'Sei sicuro?\'); if (surec) { $(\'#coll_destinazione\').val($(\'#collega_bdl\').val()); document.forms[\'collega_attivita\'].submit(); return false; } else { return false; }; " style="margin-left:0px; margin-right:0px">
				<img src="../img/admin/link.gif" alt="Collega" title="Collega" />&nbsp;&nbsp;&nbsp;Collega a:
				</button>
				<select name="collega_bdl" id="collega_bdl" style="margin-left:-4px; height:27px;width:150px" >
					<option value="">-- Seleziona --</option>
					'.Customer::BuildSelectForLinking($customer->id, $bdl['id_bdl'], 'L').'
				</select>
				';
			}
			else {
				echo '
				<div style="margin-left:348px"><br />
					<button type="submit" class="button">
						<img src="../img/admin/enabled-2.gif" alt="Conferma" title="Conferma" />&nbsp;&nbsp;&nbsp;Salva
					</button>
				</div>
				';
			}

			echo "</form><br /><br />";

		}
	}
	*/
}