<?php

class Validate extends ValidateCore
{
	public static function isSuburb($suburb)
	{
		return preg_match('/^[^!<>;?=+@#"°{}_$%]*$/u', $suburb);
	}

    // Permette il carattere = nelle reference / codici prodotto
	public static function isReference($reference)
	{
        return preg_match(Tools::cleanNonUnicodeSupport('/^[^<>;{}]*$/u'), $reference);
	}

	public static function isPrice($price)
	{
		$price = str_replace(',','.',trim($price));
		return preg_match('/^[0-9]{1,10}(\.[0-9]{1,9})?$/', $price);
	}

	// Sostituisce isTagsList permettendo il carattere ° nei tag
	public static function isTagsListModificata($list)
	{
        return preg_match(Tools::cleanNonUnicodeSupport('/^[^!<>;?=+#"{}_$%]*$/u'), $list);
	}

	// Permette i caratteri ; e =
	public static function isName($name)
    {
        return preg_match(Tools::cleanNonUnicodeSupport('/^[^0-9!<>,?+()@#"°{}_$%:]*$/u'), $name);
    }

	// Permette i caratteri ; e = nel nome prodotto
	public static function isCatalogName($name)
    {
        return preg_match(Tools::cleanNonUnicodeSupport('/^[^<>#{}]*$/u'), $name);
    }
}