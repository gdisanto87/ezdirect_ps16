<?php

class Product extends ProductCore
{
	public $date_available = '1946-01-01';
	public $data_arrivo = '1946-01-01';
	public $data_arrivo_ordinato = '';
	public $data_arrivo_esprinet = '1946-01-01';
	
	public $dispo_type;
	public $listino;
	public $rif_fornitore_2;
	public $cod_fornitore_1;
	public $cod_fornitore_2;
	public $asin;
	public $sku_amazon;
	public $fnsku;

	public $margin_min_cli;
	public $margin_min_riv;
	public $margin_login_for_offer;

	public $map;
	public $other_suppliers;
	public $comparaprezzi_check;

	public $scontolistinovendita;

	public $sconto_acquisto_1;
	public $sconto_acquisto_2;
	public $sconto_acquisto_3;

	public $serial;

	public $scorta_minima;
	public $scorta_minima_amazon;

	public $no_amazon;

	public $stock_quantity;
	public $arrivo_quantity;
	public $supplier_quantity;
	public $esprinet_quantity;
	public $attiva_quantity;
	public $itancia_quantity;
	public $techdata_quantity;
	public $asit_quantity;
	public $amazon_quantity;

	public $arrivo_esprinet_quantity;
	public $arrivo_attiva_quantity;
	public $arrivo_techdata_quantity;

	public $ordinato_quantity;
	public $impegnato_quantity;
	public $impegnato_amazon;

	public $trovaprezzi;
	public $google_shopping;
	public $amazon;
	public $amazon_free_shipping;
	public $amazon_it;
	public $amazon_fr;
	public $amazon_uk;
	public $amazon_es;
	public $amazon_de;
	public $amazon_nl;
	public $amazon_se;
	public $amazon_pl;
	public $eprice;

	public $ottieni_miglior_prezzo_flag;

	public $data_arrivo_techdata;

	public $intracom_quantity;
	public $arrivo_intracom_quantity;
	public $data_arrivo_intracom;

	public $canonical;
	public $redirect_category;
	public $note;
	public $note_fornitore;
	public $fuori_produzione = false;
	public $data_uscita_produzione = false;
	public $acquisto_in_dollari = false;
	public $costo_dollari;
	public $blocco_prezzi;
	public $login_for_offer;
	public $noindex;
	public $prodotto_con_canone;
	public $qt_esolver;
	public $ord_esolver;
	public $provvigione;
	public $date_available_msg;
	public $id_category_gst;

	// Lang fields
	public $description_amazon;
	public $cat_homepage;
	public $desc_homepage;

	public static $definition = array(
        'table' => 'product',
        'primary' => 'id_product',
        'multilang' => true,
        'multilang_shop' => true,
        'fields' => array(
            /* Classic fields */
            'id_shop_default' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_manufacturer' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_supplier' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'reference' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isReference', 'size' => 32),
            'supplier_reference' =>        array('type' => self::TYPE_STRING, 'validate' => 'isReference', 'size' => 32),
            'location' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isReference', 'size' => 64),
            'width' =>                        array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
            'height' =>                    array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
            'depth' =>                        array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
            'weight' =>                    array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
            'quantity_discount' =>            array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'ean13' =>                        array('type' => self::TYPE_STRING, 'validate' => 'isEan13', 'size' => 13),
            'upc' =>                        array('type' => self::TYPE_STRING, /*'validate' => 'isUpc',*/ 'size' => 12),
            'cache_is_pack' =>                array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'cache_has_attachments' =>        array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'is_virtual' =>                array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			/* OVERRIDE */
			'dispo_type' =>					array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'listino' => 					array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
			'rif_fornitore_2' =>			array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 250),
			'cod_fornitore_1' =>			array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 150),
			'cod_fornitore_2' =>			array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 150),
			'asin' =>						array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 200), // Correggere: size = 10?
			'sku_amazon' =>					array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 50),
			'fnsku' =>						array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 50),
			'margin_min_cli' => 			array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
			'margin_min_riv' => 			array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
			'margin_login_for_offer' => 	array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
			'map' => 						array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
			'other_suppliers' =>			array('type' => self::TYPE_STRING, 'size' => 300),
			'comparaprezzi_check' =>		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'scontolistinovendita' => 		array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
			'sconto_acquisto_1' => 			array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
			'sconto_acquisto_2' => 			array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
			'sconto_acquisto_3' => 			array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
			'serial' =>						array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 50),
			'scorta_minima' =>				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'scorta_minima_amazon' =>		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'no_amazon' =>					array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 150),
			'stock_quantity' =>				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'arrivo_quantity' =>			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'supplier_quantity' =>			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'esprinet_quantity' =>			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'attiva_quantity' =>			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'itancia_quantity' =>			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'techdata_quantity' =>			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'asit_quantity' =>				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'amazon_quantity' =>			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'arrivo_esprinet_quantity' =>	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'arrivo_attiva_quantity' =>		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'arrivo_techdata_quantity' =>	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'ordinato_quantity' =>			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'impegnato_quantity' =>			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'impegnato_amazon' =>			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'trovaprezzi' =>				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'google_shopping' =>			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'amazon' =>						array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 20),
			'amazon_free_shipping' =>		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'amazon_it' => 					array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
			'amazon_fr' => 					array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
			'amazon_uk' => 					array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
			'amazon_es' => 					array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
			'amazon_de' => 					array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
			'amazon_nl' => 					array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
			'amazon_se' => 					array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
			'amazon_pl' => 					array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
			'eprice' =>						array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'ottieni_miglior_prezzo_flag' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'data_arrivo' =>				array('type' => self::TYPE_DATE/*, 'validate' => 'isDate'*/),
			'data_arrivo_ordinato' =>		array('type' => self::TYPE_STRING),
			'data_arrivo_esprinet' =>		array('type' => self::TYPE_DATE/*, 'validate' => 'isDate'*/),
			'data_arrivo_techdata' =>		array('type' => self::TYPE_DATE/*, 'validate' => 'isDate'*/),
			'intracom_quantity' =>			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'arrivo_intracom_quantity' =>	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'data_arrivo_intracom' =>		array('type' => self::TYPE_DATE/*, 'validate' => 'isDate'*/),
			'canonical' =>					array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'redirect_category' =>			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'note' =>						array('type' => self::TYPE_STRING),
			'note_fornitore' =>				array('type' => self::TYPE_STRING),
			'fuori_produzione' =>			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'data_uscita_produzione' =>		array('type' => self::TYPE_DATE/*, 'validate' => 'isDate'*/),
			'acquisto_in_dollari' =>		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'costo_dollari' => 				array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
			'blocco_prezzi' =>				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'login_for_offer' =>			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'noindex' =>					array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'prodotto_con_canone' =>		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'qt_esolver' =>					array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'ord_esolver' =>				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'provvigione' => 				array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
			'date_available' =>				array('type' => self::TYPE_DATE/*, 'validate' => 'isDate'*/),
			'date_available_msg' =>			array('type' => self::TYPE_STRING),
			'id_category_gst' =>			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			/* FINE OVERRIDE */

            /* Shop fields */
            'id_category_default' =>        array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedId'),
            'id_tax_rules_group' =>        array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedId'),
            'on_sale' =>                    array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'online_only' =>                array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'ecotax' =>                    array('type' => self::TYPE_FLOAT, 'shop' => true, 'validate' => 'isPrice'),
            'minimal_quantity' =>            array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),
            'price' =>                        array('type' => self::TYPE_FLOAT, 'shop' => true, 'validate' => 'isPrice', 'required' => true),
            'wholesale_price' =>            array('type' => self::TYPE_FLOAT, 'shop' => true, 'validate' => 'isPrice'),
            'unity' =>                        array('type' => self::TYPE_STRING, 'shop' => true, 'validate' => 'isString'),
            'unit_price_ratio' =>            array('type' => self::TYPE_FLOAT, 'shop' => true),
            'additional_shipping_cost' =>    array('type' => self::TYPE_FLOAT, 'shop' => true, 'validate' => 'isPrice'),
            'customizable' =>                array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),
            'text_fields' =>                array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),
            'uploadable_files' =>            array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),
			'active' =>                    array('type' => self::TYPE_INT, 'shop' => true/*, 'validate' => 'isUnsignedInt'*/),
            'redirect_type' =>                array('type' => self::TYPE_STRING, 'shop' => true, 'validate' => 'isString'),
            'id_product_redirected' =>        array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedId'),
            'available_for_order' =>        array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'available_date' =>            array('type' => self::TYPE_DATE, 'shop' => true, 'validate' => 'isDateFormat'),
            'condition' =>                    array('type' => self::TYPE_STRING, 'shop' => true, 'validate' => 'isGenericName', 'values' => array('new', 'used', 'refurbished'), 'default' => 'new'),
            'show_price' =>                array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'indexed' =>                    array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'visibility' =>                array('type' => self::TYPE_STRING, 'shop' => true, 'validate' => 'isProductVisibility', 'values' => array('both', 'catalog', 'search', 'none'), 'default' => 'both'),
            'cache_default_attribute' =>    array('type' => self::TYPE_INT, 'shop' => true),
            'advanced_stock_management' =>    array('type' => self::TYPE_BOOL, 'shop' => true, 'validate' => 'isBool'),
            'date_add' =>                    array('type' => self::TYPE_DATE, 'shop' => true, 'validate' => 'isDate'),
            'date_upd' =>                    array('type' => self::TYPE_DATE, 'shop' => true, 'validate' => 'isDate'),
            'pack_stock_type' =>            array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedInt'),

            /* Lang fields */
            'meta_description' =>            array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'meta_keywords' =>                array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'meta_title' =>                array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 128),
            'link_rewrite' =>    array(
                'type' => self::TYPE_STRING,
                'lang' => true,
                'validate' => 'isLinkRewrite',
                'required' => true,
                'size' => 128,
                'ws_modifier' => array(
                    'http_method' => WebserviceRequest::HTTP_POST,
                    'modifier' => 'modifierWsLinkRewrite'
                )
            ),
            'name' =>                        array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCatalogName', 'required' => true, 'size' => 128),
            'description' =>                array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
            'description_short' =>            array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
            'available_now' =>                array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'available_later' =>            array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'IsGenericName', 'size' => 255),
			/* OVERRIDE Lang fields */
            'description_amazon' => 	array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
            'cat_homepage' => 			array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString', 'size' => 100),
            'desc_homepage' => 			array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString', 'size' => 110),
			/* FINE OVERRIDE */
        ),
        'associations' => array(
            'manufacturer' =>                array('type' => self::HAS_ONE),
            'supplier' =>                    array('type' => self::HAS_ONE),
            'default_category' =>            array('type' => self::HAS_ONE, 'field' => 'id_category_default', 'object' => 'Category'),
            'tax_rules_group' =>            array('type' => self::HAS_ONE),
            'categories' =>                    array('type' => self::HAS_MANY, 'field' => 'id_category', 'object' => 'Category', 'association' => 'category_product'),
            'stock_availables' =>            array('type' => self::HAS_MANY, 'field' => 'id_stock_available', 'object' => 'StockAvailable', 'association' => 'stock_availables'),
        ),
    );

    
	// Controlla se un prodotto può essere eliminato,
    // cioè se non si trova in un ordine non valido (o verificato?)
	protected function isDeletable()
	{
		$is_deletable = Db::getInstance()->getValue('
            SELECT COUNT(`product_id`) as `nb_product`
            FROM `'._DB_PREFIX_.'order_detail` od
                LEFT JOIN `'._DB_PREFIX_.'orders` o ON (o.`id_order` = od.`id_order`)
            WHERE o.valid = 0
                AND o.id_order NOT IN (SELECT id_order FROM '._DB_PREFIX_.'order_history WHERE id_order_state = 6 OR id_order_state = 8)
                AND `product_id` = '.(int)$this->id.'
		');

		return !(bool)$is_deletable;
	}

    // Aggiunto controllo isDeletable
    public function delete()
    {
        if (!$this->isDeletable())
			return false;

        if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && $this->advanced_stock_management) {
            $stock_manager = StockManagerFactory::getManager();
            $physical_quantity = $stock_manager->getProductPhysicalQuantities($this->id, 0);
            $real_quantity = $stock_manager->getProductRealQuantities($this->id, 0);
            if ($physical_quantity > 0) {
                return false;
            }
            if ($real_quantity > $physical_quantity) {
                return false;
            }

            $warehouse_product_locations = Adapter_ServiceLocator::get('Core_Foundation_Database_EntityManager')->getRepository('WarehouseProductLocation')->findByIdProduct($this->id);
            foreach ($warehouse_product_locations as $warehouse_product_location) {
                $warehouse_product_location->delete();
            }

            $stocks = Adapter_ServiceLocator::get('Core_Foundation_Database_EntityManager')->getRepository('Stock')->findByIdProduct($this->id);
            foreach ($stocks as $stock) {
                $stock->delete();
            }
        }
        $result = parent::delete();

        // Removes the product from StockAvailable, for the current shop
        StockAvailable::removeProductFromStockAvailable($this->id);
        $result &= ($this->deleteProductAttributes() && $this->deleteImages() && $this->deleteSceneProducts());
        // If there are still entries in product_shop, don't remove completely the product
        if ($this->hasMultishopEntries()) {
            return true;
        }

        Hook::exec('actionProductDelete', array('id_product' => (int)$this->id, 'product' => $this));
        if (!$result ||
            !GroupReduction::deleteProductReduction($this->id) ||
            !$this->deleteCategories(true) ||
            !$this->deleteProductFeatures() ||
            !$this->deleteTags() ||
            !$this->deleteCartProducts() ||
            !$this->deleteAttributesImpacts() ||
            !$this->deleteAttachments(false) ||
            !$this->deleteCustomization() ||
            !SpecificPrice::deleteByProductId((int)$this->id) ||
            !$this->deletePack() ||
            !$this->deleteProductSale() ||
            !$this->deleteSearchIndexes() ||
            !$this->deleteAccessories() ||
            !$this->deleteFromAccessories() ||
            !$this->deleteFromSupplier() ||
            !$this->deleteDownload() ||
            !$this->deleteFromCartRules()) {
            return false;
        }

        return true;
    }

    // Uguale a quella della class principale a parte SELECT *?
    //public static function duplicateAttachments($id_product_old, $id_product_new){}

    // Aggiunto $withGroups
    public function getFrontFeatures($id_lang, $withGroups = true)
    {
        if (!$withGroups)
            return parent::getFrontFeatures($id_lang);

        $id_product = $this->id;

        //if (!array_key_exists($id_product.'-'.$id_lang, self::$_frontFeaturesCache))
		//{
           /* self::$_frontFeaturesCache[$id_product.'-'.$id_lang] = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
                SELECT 
                    fl.name AS feature_name, 
                    value, 
                    pf.id_feature, 
                    fgl.id_group, 
                    fgl.name AS feature_group_name,
                    fg.position AS group_position,
                    f.position AS feature_position
                FROM '._DB_PREFIX_.'feature_product pf
                LEFT JOIN '._DB_PREFIX_.'feature f ON pf.id_feature=f.id_feature
                LEFT JOIN '._DB_PREFIX_.'feature_group fg ON f.id_group=fg.id_group
                LEFT JOIN '._DB_PREFIX_.'feature_group_lang fgl ON (fg.id_group=fgl.id_group AND fgl.id_lang = '.(int)$id_lang.')
                LEFT JOIN '._DB_PREFIX_.'feature_lang fl ON (fl.id_feature = pf.id_feature AND fl.id_lang = '.(int)$id_lang.')
                LEFT JOIN '._DB_PREFIX_.'feature_value_lang fvl ON (fvl.id_feature_value = pf.id_feature_value AND fvl.id_lang = '.(int)$id_lang.')
                '.Shop::addSqlAssociation('feature', 'f').'
                WHERE pf.id_product = '.(int)$id_product.' 
                ORDER BY fg.position ASC, fl.name ASC
            ');*/
		//}
        
        $result = array();
        foreach (self::$_frontFeaturesCache[$id_product.'-'.$id_lang] as $frecord) {
            if (!$frecord['id_group'] || !$frecord['id_feature'])
                continue;
            if (!array_key_exists($frecord["id_group"], $result))
                $result[$frecord["id_group"]] = array(
					"name" => $frecord['feature_group_name'], 
					"position" => $frecord["group_position"],
					'features' => array()
				);
            $result[$frecord['id_group']]['features'][$frecord['id_feature']] = array(
				"name" => $frecord['feature_name'], 
				"position" => $frecord["feature_position"],
				'value'=>$frecord["value"]
			);
        }
        
        /* 
        uasort($result, array($this, 'sortFeaturesGroupsByPositionCallback'));
        foreach ($result as $group_id=>&$group_data) {
			uasort($group_data["features"], array($this, 'sortFeaturesByPositionCallback'));
		}
        */
        return $result;
    }

    // Usata nella funzione getFrontFeatures
    public function sortFeaturesByPositionCallback($a, $b)
	{
		return intval($a["position"]) < intval($b["position"]) ? -1 : 1;
	}
	
    // Usata nella funzione getFrontFeatures
	public function sortFeaturesGroupsByPositionCallback($a, $b)
	{
		return intval($a["position"]) < intval($b["position"]) ? -1 : 1;
	}

    // Usata nelle funzioni showProductTooltip, showProductDisponibilita e getProductProperties
    public static function calcolaImpegnato($id_product, $id_order = 0)
	{
		$qta_ord_clienti = Db::getInstance()->getValue('
            SELECT SUM(od.product_quantity - (REPLACE(cons,"0_","")))
            FROM '._DB_PREFIX_.'order_detail od 
                JOIN '._DB_PREFIX_.'orders o ON o.id_order = od.id_order
                JOIN '._DB_PREFIX_.'cart c ON c.id_cart = o.id_cart
                JOIN '._DB_PREFIX_.'order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM '._DB_PREFIX_.'order_history moh WHERE '.($id_order > 0 ? 'moh.id_order != '.$id_order.' AND' : '').' moh.id_order_state != 18 AND moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) 
                JOIN '._DB_PREFIX_.'order_state os ON (os.id_order_state = oh.id_order_state) 
            WHERE c.name NOT LIKE "%prime%" 
                AND od.product_id = "'.$id_product.'"
                AND os.id_order_state != 4 
                AND os.id_order_state != 5 
                AND os.id_order_state != 6 
                AND os.id_order_state != 7 
                AND os.id_order_state != 8 
                AND os.id_order_state != 14 
                AND os.id_order_state != 16 
                AND os.id_order_state != 20 
                AND os.id_order_state != 26 
                AND os.id_order_state != 29 
                AND os.id_order_state != 31 
                AND os.id_order_state != 32 
                AND os.id_order_state != 34 
                AND os.id_order_state != 35 
            GROUP BY od.product_id
        ');
		
		return $qta_ord_clienti;
	}

	// Genera il contenuto del tooltip di "Qta ordinata dai clienti" (impegnato); usata nella funzione showProductDisponibilita
	public static function tooltipImpegnato($id_product, $id_order = 0)
	{
		$context = Context::getContext();
		
		$ordini_impegnati = Db::getInstance()->executeS('
			SELECT o.id_order, os.id_order_state, o.id_customer, SUM(product_quantity - (REPLACE(cons,"0_",""))) qt 
            FROM '._DB_PREFIX_.'order_detail od 
                JOIN '._DB_PREFIX_.'orders o ON o.id_order = od.id_order
                JOIN '._DB_PREFIX_.'cart c ON c.id_cart = o.id_cart
                JOIN '._DB_PREFIX_.'order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM '._DB_PREFIX_.'order_history moh WHERE '.($id_order > 0 ? 'moh.id_order != '.$id_order.' AND' : '').' moh.id_order_state != 18 AND moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) 
                JOIN '._DB_PREFIX_.'order_state os ON (os.id_order_state = oh.id_order_state) 
            WHERE c.name NOT LIKE "%prime%" 
                AND od.product_id = "'.$id_product.'"
                AND os.id_order_state != 4
                AND os.id_order_state != 5
                AND os.id_order_state != 6
                AND os.id_order_state != 7
                AND os.id_order_state != 8
                AND os.id_order_state != 14
                AND os.id_order_state != 16
                AND os.id_order_state != 20
                AND os.id_order_state != 26
                AND os.id_order_state != 29
                AND os.id_order_state != 31
                AND os.id_order_state != 32
                AND os.id_order_state != 35
            GROUP BY od.product_id
        ');

		$ordini_impegnati_html = '';
		
		if($ordini_impegnati) {
			foreach($ordini_impegnati as $oo) {
				if($oo['qt'] != 0) {
					$token = Tools::getAdminTokenLite('AdminOrders');
					$href = '/ezadmin/index.php?controller=AdminOrders&id_order='.$oo["id_order"].'&vieworder&token='.$token;
					$stato = Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'order_state_lang WHERE id_lang = '.$context->language->id.' AND id_order_state = '.$oo['id_order_state']);
					$ordini_impegnati_html .= '
						<tr>
							<td><a href=\''.$href.'\' target=\'_blank\'>'.$oo['id_order'].'</a></td>
							<td>'.$stato.'</td>
							<td>'.$oo['qt'].'</td>
						</tr>
					';
				}
			}
		}

		return $ordini_impegnati_html;
	}

    // Genera il contenuto del tooltip prodotto
    public static function showProductTooltip($id_product)
	{
        $context = Context::getContext();

		$products = Db::getInstance()->getRow('
			SELECT 
                p.id_product, id_manufacturer, reference, wholesale_price AS acquisto, 
                sconto_acquisto_1 AS sc_acq_1, sconto_acquisto_2 AS sc_acq_2, sconto_acquisto_3 AS sc_acq_3, 
                listino AS listino, price AS vendita, 
                stock_quantity AS magazzino, supplier_quantity AS allnet, intracom_quantity AS intracom, itancia_quantity AS itancia, asit_quantity AS asit, ingram_quantity as ingram, amazon_quantity AS amazon_qt, esprinet_quantity as esprinet, 
                arrivo_esprinet_quantity as qt_arrivo_esprinet, attiva_quantity as attiva, arrivo_attiva_quantity as qt_arrivo_attiva, 
                ordinato_quantity AS qt_ordinato, impegnato_quantity AS qt_impegnato, quantity AS qt_tot, 
                arrivo_quantity AS qt_arrivo, arrivo_intracom_quantity AS qt_arrivo_intracom, arrivo_ingram_quantity as qt_arrivo_ingram,
                data_arrivo, data_arrivo_intracom, data_arrivo_esprinet, data_arrivo_ingram
                date_available, id_image 
			FROM '._DB_PREFIX_.'product p
                LEFT JOIN (
                    SELECT * 
                    FROM '._DB_PREFIX_.'image 
                    WHERE cover = 1
                ) image ON p.id_product = image.id_product 
			WHERE p.id_product = "'.$id_product.'"
		');
		
		$arrivo_ordinato = Db::getInstance()->getValue('
            SELECT data_arrivo_ordinato 
            FROM '._DB_PREFIX_.'product 
            WHERE id_product = "'.$id_product.'"
        ');
							
		if($arrivo_ordinato == '')
			$date_arrivo_ordinato_riga = '<tr><td colspan=\'3\'>Nessun ordine in arrivo</td></tr>';
		else
		{
			$date_arrivo_ordinato_riga = '';
			$arrivo_ordinato = unserialize($arrivo_ordinato);
			
			foreach($arrivo_ordinato as $ao)
				$date_arrivo_ordinato_riga .= '<tr><td style=\'text-align:right;\'>'.$ao['ordine'].'</td><td style=\'text-align:right;\'>'.round($ao['quantita'],0).'</td><td>'.Db::getInstance()->getValue('SELECT company FROM '._DB_PREFIX_.'customer WHERE codice_esolver = \''.$ao['fornitore'].'\'').'</td><td>'.date('d/m/Y',strtotime($ao['data'])).'</td></tr>';
		}
		
		$qta_ord_clienti = Product::calcolaImpegnato($id_product);
						
		if(!$qta_ord_clienti || $qta_ord_clienti == '')
			$qta_ord_clienti = 0;
		
		$qtas = Db::getInstance()->executeS("
            SELECT price, reduction, specific_price_name 
            FROM "._DB_PREFIX_."specific_price 
            WHERE (specific_price_name = 'sc_qta_1' OR specific_price_name = 'sc_qta_2' OR specific_price_name = 'sc_qta_3' OR specific_price_name = 'sc_riv_1' OR specific_price_name = 'sc_riv_2' OR specific_price_name = 'sc_riv_3') 
                AND id_product = '".$id_product."'
        ");
		
		$scorta_minima = Db::getInstance()->getValue('
			SELECT scorta_minima 
			FROM '._DB_PREFIX_.'product 
			WHERE id_product = "'.$id_product.'"
		');
		$scorta_minima = (int)$scorta_minima;

		$sc_att = $products['magazzino'] + $products['qt_ordinato'] - $products['qt_impegnato'];
		$sc_att = (int)$sc_att;
		
		if($scorta_minima == 0)
			$img_scorta = '';
		else if($sc_att < $scorta_minima)
			$img_scorta = "<img style='border:2px solid red' src='../img/admin/error2.png' alt='Sotto scorta' title='Sotto scorta'>";
		else {
			if($products['qt_ordinato'] > 0 && $sc_att > $scorta_minima)
				$img_scorta = "<img style='border:2px solid green' src='../img/admin/delivery.gif' alt='In arrivo' title='In arrivo'>";
			else
				$img_scorta = '';
		}

		foreach($qtas as $qta) {
			if($qta['specific_price_name'] == 'sc_qta_1')
				$sc_qta_1_r = $qta;
			else if($qta['specific_price_name'] == 'sc_qta_2')
				$sc_qta_2_r = $qta;
			else if($qta['specific_price_name'] == 'sc_qta_3')
				$sc_qta_3_r = $qta;
			else if($qta['specific_price_name'] == 'sc_riv_1')
				$sc_riv_1_r = $qta;
			else if($qta['specific_price_name'] == 'sc_riv_2')
				$sc_riv_2_r = $qta;
			else if($qta['specific_price_name'] == 'sc_riv_3')
				$sc_riv_3_r = $qta;
		}
		
		$id_supplier = Db::getInstance()->getValue("
			SELECT id_supplier 
			FROM "._DB_PREFIX_."product 
			WHERE id_product = '".$id_product."'
		");
		
		$spec_price = Db::getInstance()->getValue("
			SELECT price 
			FROM "._DB_PREFIX_."specific_price sp 
			WHERE sp.specific_price_name = '' 
				AND sp.to > '".date('Y-m-d H:i:s')."' 
				AND sp.id_product = '".$id_product."'
		");
		
		$spec_whole = Db::getInstance()->getValue("
			SELECT wholesale_price 
			FROM "._DB_PREFIX_."specific_price_wholesale sp 
			WHERE sp.to > '".date('Y-m-d H:i:s')."' 
				AND sp.id_product = '".$id_product."'
		");
		
		$sc_qta_1 = $sc_qta_1_r['price'] - ($sc_qta_1_r['price'] * $sc_qta_1_r['reduction']);
		$sc_qta_2 = $sc_qta_2_r['price'] - ($sc_qta_2_r['price'] * $sc_qta_2_r['reduction']);
		$sc_qta_3 = $sc_qta_3_r['price'] - ($sc_qta_3_r['price'] * $sc_qta_3_r['reduction']);
		$sc_riv_1 = $sc_riv_1_r['price'] - ($sc_riv_1_r['price'] * $sc_riv_1_r['reduction']);
		$sc_riv_2 = $sc_riv_2_r['price'] - ($sc_riv_2_r['price'] * $sc_riv_2_r['reduction']);
		$sc_riv_3 = $sc_riv_3_r['price'] - ($sc_riv_3_r['price'] * $sc_riv_3_r['reduction']);
		
		$supplier = Db::getInstance()->getValue('
            SELECT name 
            FROM '._DB_PREFIX_.'supplier 
            WHERE id_supplier = "'.$id_supplier.'"
        ');
		
		$tooltip = '
            <div style=\'color:#000; z-index:99999999; width:1000px !important; height:500px; background-color: white; border: 3px solid black\'>
                <h2 style=\'display:block; float:left; color: #268ccd; font-size:20px; margin-left:10px; margin-top:10px;\'>'.$products['reference'].'</h2>
                <div style=\'margin-left:50px; margin-top:14px; float:left\'> '.str_replace('|','',str_replace('"',"''",Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'product_lang WHERE id_lang = '.$context->language->id.' AND id_product = "'.$products['id_product'].'"'))).'<!-- <img src=\'https://www.ezdirect.it/img/'.($products['id_image'] > 0 ? 'p/'.$products['id_product'].'-'.$products['id_image'] : 'm/'.$products['id_manufacturer']).'-small.jpg\' /> --> </div>
                <div style=\'margin-left:50px; margin-top:14px; float:left;\'>'.($context->employee->id_profile == 7 ? '' : 'Fornitore: <strong>'.$supplier.'</strong>').'</div>
                <div class=\'clear\'></div>
                <div style=\'float:left; margin-left:10px;\'></div>
                <div style=\'float:left; margin-left:10px;\'>
                    <strong>Prezzi</strong><br />
                    <!-- <style type=\'text/css\'>table.tooltip_table tr td, table.tooltip_table tr th { text-align:right; background-color:#F4E6C9 }</style> -->
                    
                    <table class=\'table tooltip_table\' style=\'width:75%; float:left\'>
                        <tr>
                            <th>Listino</th>
                            <th>'.($context->employee->id_profile == 7 ? '' : 'Acq.').'</th>
                            <th>Vnd. Web</th>
                            <th>Qta 1</th>
                            <th>Qta2</th>
                            <th>Qta 3</th>
                            <th>Riv.</th>
                            <th>Spec. Vnd.</th>
                            <th>'.($context->employee->id_profile == 7 ? '' : 'Spec. Acq.').'</th>
                        </tr>
                        <tr>
                            <td>'.Tools::displayPrice($products['listino']).'</td>
                            <td>'.($context->employee->id_profile == 7 ? '' : ($context->employee->id == 1 || $context->employee->id == 2  || $context->employee->id == 3 || $context->employee->id == 6 || $context->employee->id == 15 || $context->employee->id == 7 ? Tools::displayPrice(($products['acquisto'] > 0 ? $products['acquisto'] : ($products['listino']*(100-$products['sc_acq_1'])/100)*((100-$products['sc_acq_2'])/100)*((100-$products['sc_acq_3'])/100))).'' : 'N.D.')).'</td>
                            <td>'.Tools::displayPrice($products['vendita']).'</td>
                            <td>'.($sc_qta_1 > 0 ? Tools::displayPrice($sc_qta_1) : '--').'</td>
                            <td>'.($sc_qta_2 > 0 ? Tools::displayPrice($sc_qta_2) : '--').'</td>
                            <td>'.($sc_qta_3> 0 ? Tools::displayPrice($sc_qta_3) : '--').'</td>
                            <td>'.($sc_riv_1 > 0 ? Tools::displayPrice($sc_riv_1) : '--').'</td>
                            <td>'.($spec_price > 0 ? '<font style=\'color:red\'>'.Tools::displayPrice($spec_price).'</font>' : '--').'</td>
                            <td>'.($context->employee->id_profile == 7 ? '' : ($spec_whole > 0 ? '<font style=\'color:blue\'>'.Tools::displayPrice($spec_whole).'</font>' : '--')).'</td>
                        </tr>
                    </table>

                    <br />

                    <table class=\'table tooltip_table\' cellpadding=\'0\' cellspacing=\'0\' style=\' border:1px solid #000; float:left; width:75%\'>
                        <tr>
                            <th style=\'background: #ebebeb; text-align:center; border-bottom:1px solid #000\' colspan=\'8\'>Disponibilit&agrave;</th>
                        </tr>        
                        <tr>        
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right; width:130px\'>Magazzino EZ</th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right;\'>'.$products['magazzino'].'</th>
                            <th style=\'padding:0px; border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right; width:300px\'>
                                <table style=\'width:100%; border-collapse:collapse; margin-top:0px; height:26px; margin-bottom:0px;border-bottom:0px\'>
                                    <tr>
                                        <th style=\'border-right: 1px solid #bbb; border-bottom:0px solid #000; text-align:right;\'>Netto<img class=\'img_control\' src=\'https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif\' alt=\'Netto = Magazzino EZ - Qta ordinata dai clienti (sito)\' title=\'Netto = Magazzino EZ - Qta ordinata dai clienti (sito)\' /></th>         
                                        <th style=\'border-right: 1px solid #bbb; border-bottom:0px solid #000;text-align:right\'>'.($products['magazzino'] - $qta_ord_clienti).'</th>
                                        <th style=\'border-right: 0px solid #bbb; border-bottom:0px solid #000;text-align:right\'>Qta ordinata dai clienti (sito)</th>
                                    </tr>
                                </table>
                            </th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right\'>'.$qta_ord_clienti.'</th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right; width:300px\'>Qta impegnata ord. caricati su gestionale (eSolver)</th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right\'>'.$products['qt_impegnato'].'</th>
                        </tr>
                        <tr>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right\'>Mag. Amazon</th>   
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right\'>'.$products['amazon_qt'].'</th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right\' >Ordinato al fornitore</th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right\'>'.$products['qt_ordinato'].'</th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right\' colspan=\'1\'>Mag. EZ + Ordinato al fornitore - Impegnato sito</th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right\' >'.($products['magazzino'] + $products['qt_ordinato'] - $qta_ord_clienti).'</th>
                        </tr>
                        '.($context->employee->id_profile == 7 ? '' : '
                        <tr>
                            <'.($id_supplier == 11 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Allnet</'.($id_supplier == 11 ? 'th' : 'td').'>
                            <'.($id_supplier == 11 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['allnet'].'</'.($id_supplier == 11 ? 'th' : 'td').'>
                            <'.($id_supplier == 11 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>In arrivo Allnet</'.($id_supplier == 11 ? 'th' : 'td').'>
                            <'.($id_supplier == 11 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['qt_arrivo'].'</'.($id_supplier == 11 ? 'th' : 'td').'>
                            <'.($id_supplier == 11 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Data arr. Allnet</'.($id_supplier == 11 ? 'th' : 'td').'>
                            <'.($id_supplier == 11 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right; width:80px;\'>'.(($from = $products['data_arrivo'] AND $from != '1946-01-01') ? ($from == '0000-00-00' || $from == '1970-01-01' ? 'ND' : date('d-m-y',strtotime($from))) : date('d-m-y')).'</'.($id_supplier == 11 ? 'th' : 'td').'>
                        </tr>
                        <tr>
                            <'.($id_supplier == 38 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Esprinet</'.($id_supplier == 38 ? 'th' : 'td').'>
                            <'.($id_supplier == 38 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['esprinet'].'</'.($id_supplier == 38 ? 'th' : 'td').'>
                            <'.($id_supplier == 38 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>In arrivo Esprinet</'.($id_supplier == 38 ? 'th' : 'td').'>
                            <'.($id_supplier == 38 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['qt_arrivo_esprinet'].'</'.($id_supplier == 38 ? 'th' : 'td').'>
                            <'.($id_supplier == 38 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Data arr. Esprinet</'.($id_supplier == 38 ? 'th' : 'td').'>
                            <'.($id_supplier == 38 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.(($from = $products['data_arrivo_esprinet'] AND $from != '1946-01-01') ? ($from == '0000-00-00' || $from == '1970-01-01' ? 'ND' : date('d-m-y',strtotime($from))) : date('d-m-y')).'</'.($id_supplier == 38 ? 'th' : 'td').'>
                        </tr>
                        <tr>
                            <'.($id_supplier == 1450 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Attiva</'.($id_supplier == 1450 ? 'th' : 'td').'>
                            <'.($id_supplier == 1450 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['attiva'].'</'.($id_supplier == 1450 ? 'th' : 'td').'>
                            <'.($id_supplier == 1450 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>In arrivo Attiva</'.($id_supplier == 1450 ? 'th' : 'td').'>
                            <'.($id_supplier == 1450 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['qt_arrivo_attiva'].'</'.($id_supplier == 1450 ? 'th' : 'td').'>
                            <'.($id_supplier == 1450 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Data arr. Attiva</'.($id_supplier == 1450 ? 'th' : 'td').'>
                            <'.($id_supplier == 1450 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.(($from = $products['data_arrivo_attiva'] AND $from != '1946-01-01') ? ($from == '0000-00-00' || $from == '1970-01-01' ? 'ND' : date('d-m-y',strtotime($from))) : date('d-m-y')).'</'.($id_supplier == 1450 ? 'th' : 'td').'>
                        </tr>
                        <tr>
                            <'.($id_supplier == 42 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Itancia</'.($id_supplier == 42 ? 'th' : 'td').'>
                            <'.($id_supplier == 42 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['itancia'].'</'.($id_supplier == 42 ? 'th' : 'td').'>
                            <'.($id_supplier == 42 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>In arrivo Itancia</'.($id_supplier == 42 ? 'th' : 'td').'>
                            <'.($id_supplier == 42 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>0</'.($id_supplier == 42 ? 'th' : 'td').'>
                            <'.($id_supplier == 42 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Data arr. Itancia:</'.($id_supplier == 42 ? 'th' : 'td').'>
                            <'.($id_supplier == 42 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>ND</'.($id_supplier == 42 ? 'th' : 'td').'>
                        </tr>
                        <tr>
                            <'.($id_supplier == 95 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Intracom</'.($id_supplier == 95 ? 'th' : 'td').'>
                            <'.($id_supplier == 95 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['intracom'].'</'.($id_supplier == 95 ? 'th' : 'td').'>
                            <'.($id_supplier == 95 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>In arrivo Intracom</'.($id_supplier == 95 ? 'th' : 'td').'>
                            <'.($id_supplier == 95 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['qt_arrivo_intracom'].'</'.($id_supplier == 95 ? 'th' : 'td').'>
                            <'.($id_supplier == 95 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Data arr. Intracom:</'.($id_supplier == 95 ? 'th' : 'td').'>
                            <'.($id_supplier == 95 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.(($from_intracom = $products['data_arrivo_intracom'] AND $from_intracom != '1946-01-01') ? ($from_intracom == '0000-00-00' || $from_intracom == '1970-01-01' ? 'ND' : date('d-m-y',strtotime($from_intracom))) : date('d-m-y')).'</'.($id_supplier == 95 ? 'th' : 'td').'>
                        </tr>
                        <tr>
                            <'.($id_supplier == 76 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>ASIT</'.($id_supplier == 76 ? 'th' : 'td').'>
                            <'.($id_supplier == 76 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['asit'].'</'.($id_supplier == 76 ? 'th' : 'td').'>
                            <'.($id_supplier == 76 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'></'.($id_supplier == 76 ? 'th' : 'td').'>
                            <'.($id_supplier == 76 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'></'.($id_supplier == 76 ? 'th' : 'td').'>
                            <'.($id_supplier == 76 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'></'.($id_supplier == 76 ? 'th' : 'td').'>
                            <'.($id_supplier == 76 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'></'.($id_supplier == 76 ? 'th' : 'td').'>
                        </tr>
						<tr>
                            <'.($id_supplier == 1461 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Ingram</'.($id_supplier == 1461 ? 'th' : 'td').'>
                            <'.($id_supplier == 1461 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['ingram'].'</'.($id_supplier == 1461 ? 'th' : 'td').'>
							<'.($id_supplier == 1461 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>In arrivo Ingram</'.($id_supplier == 1461 ? 'th' : 'td').'>
                            <'.($id_supplier == 1461 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['qt_arrivo_ingram'].'</'.($id_supplier == 1461 ? 'th' : 'td').'>
                            <'.($id_supplier == 1461 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Data arr. Ingram:</'.($id_supplier == 1461 ? 'th' : 'td').'>
                            <'.($id_supplier == 1461 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.(($from_ingram = $products['data_arrivo_ingram'] AND $from_ingram != '1946-01-01') ? ($from_ingram == '0000-00-00' || $from_ingram == '1970-01-01' ? 'ND' : date('d-m-y',strtotime($from_ingram))) : date('d-m-y')).'</'.($id_supplier == 1461 ? 'th' : 'td').'>
                        </tr>
                        <tr>
                            <td style=\'background: #ebebeb; border-right: 1px solid #000; border-top: 1px solid #000; text-align:right\'><strong>Totale tutti i magazzini (escl. Amazon)</strong></td>
                            <td style=\'background: #ebebeb; border-right: 1px solid #000; border-top: 1px solid #000; text-align:right\'>'.$products['qt_tot'].'</td>
                            <td style=\'background: #ebebeb; border-right: 1px solid #000; border-top: 1px solid #000; text-align:right; width:170px\'><strong>Tot. magazzini + Ordinato a fornitore - Impegnato sito</strong></td>
                            <td style=\'background: #ebebeb; border-right: 1px solid #000; border-top: 1px solid #000; text-align:right; \'>'.(($products['qt_tot'])+$products['qt_ordinato']-$products['qt_impegnato']).'</td>
                            <td style=\'background: #ebebeb; border-right: 1px solid #000; border-top: 1px solid #000; text-align:right; \'><strong>'.$img_scorta.' Scorta minima</strong></td>
                            <td style=\'background: #ebebeb; border-top: 1px solid #000; text-align:right; \'>'.$scorta_minima.'</td>
                        </tr>').'
                    </table>
                    
                    <table class=\'table tooltip_table\' cellpadding=\'0\' cellspacing=\'0\' style=\'float:left; border:1px solid #000; float:left; width:23%; margin-left:1%\'>
                        <tr>
                            <th style=\'background: #ebebeb; text-align:center; border-bottom:1px solid #000\' colspan=\'8\'>Date qta in arrivo</th>
                        </tr>
                        <tr>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right;\'>ID Ord.</th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right;\'>Qt.</th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right; width:150px\'>Fornitore</th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right\'>Data</th>
                        </tr>
                        '.$date_arrivo_ordinato_riga.'
                    </table>
                </div>
                <div style=\'clear:both\'></div>
            </div>
        ';

		return $tooltip;
	}

	// Genera il contenuto del panel Disponibilità di edit prodotto (tab Info e Offerte speciali)
	public static function showProductDisponibilita($id_product)
	{
        $context = Context::getContext();

		$products = Db::getInstance()->getRow('
			SELECT 
                p.id_product, id_manufacturer, reference, wholesale_price AS acquisto, 
                sconto_acquisto_1 AS sc_acq_1, sconto_acquisto_2 AS sc_acq_2, sconto_acquisto_3 AS sc_acq_3, 
                listino AS listino, price AS vendita, 
                stock_quantity AS magazzino, supplier_quantity AS allnet, intracom_quantity AS intracom, itancia_quantity AS itancia, asit_quantity AS asit, ingram_quantity as ingram, amazon_quantity AS amazon_qt, esprinet_quantity as esprinet, 
                arrivo_esprinet_quantity as qt_arrivo_esprinet, attiva_quantity as attiva, arrivo_attiva_quantity as qt_arrivo_attiva, 
                ordinato_quantity AS qt_ordinato, impegnato_quantity AS qt_impegnato, quantity AS qt_tot, 
                arrivo_quantity AS qt_arrivo, arrivo_intracom_quantity AS qt_arrivo_intracom, arrivo_ingram_quantity as qt_arrivo_ingram,
                data_arrivo, data_arrivo_intracom, data_arrivo_esprinet, data_arrivo_ingram
                date_available, id_image 
			FROM '._DB_PREFIX_.'product p
                LEFT JOIN (
                    SELECT * 
                    FROM '._DB_PREFIX_.'image 
                    WHERE cover = 1
                ) image ON p.id_product = image.id_product 
			WHERE p.id_product = "'.$id_product.'"
		');
		
		$arrivo_ordinato = Db::getInstance()->getValue('
            SELECT data_arrivo_ordinato 
            FROM '._DB_PREFIX_.'product 
            WHERE id_product = "'.$id_product.'"
        ');
							
		if($arrivo_ordinato == '')
			$date_arrivo_ordinato_riga = '<tr><td colspan=\'3\'>Nessun ordine in arrivo</td></tr>';
		else
		{
			$date_arrivo_ordinato_riga = '';
			$arrivo_ordinato = unserialize($arrivo_ordinato);
			
			foreach($arrivo_ordinato as $ao)
				$date_arrivo_ordinato_riga .= '<tr><td style=\'text-align:right;\'>'.$ao['ordine'].'</td><td style=\'text-align:right;\'>'.round($ao['quantita'],0).'</td><td>'.Db::getInstance()->getValue('SELECT company FROM '._DB_PREFIX_.'customer WHERE codice_esolver = \''.$ao['fornitore'].'\'').'</td><td>'.date('d/m/Y',strtotime($ao['data'])).'</td></tr>';
		}
		
		$qta_ord_clienti = Product::calcolaImpegnato($id_product);

		$ordini_impegnati_html = Product::tooltipImpegnato($id_product);
		$tooltip_impegnato = '
			<div>
				<span id=\'qta_ord_clienti_tooltip\'><table class=\'table\' style=\'width:250px\'><tr><th style=\'color:#000 !important\'>Ordine</th><th style=\'color:#000 !important\'>Stato</th><th style=\'color:#000 !important\'>Quantit&agrave;</th></tr>'.$ordini_impegnati_html.'</table></span>
			</div>
		';
						
		if(!$qta_ord_clienti || $qta_ord_clienti == '')
			$qta_ord_clienti = 0;
		
		$scorta_minima = Db::getInstance()->getValue('
			SELECT scorta_minima 
			FROM '._DB_PREFIX_.'product 
			WHERE id_product = "'.$id_product.'"
		');
		$scorta_minima = (int)$scorta_minima;

		$sc_att = $products['magazzino'] + $products['qt_ordinato'] - $products['qt_impegnato'];
		$sc_att = (int)$sc_att;
		
		if($scorta_minima == 0)
			$img_scorta = '';
		else if($sc_att < $scorta_minima)
			$img_scorta = "<img style='border:2px solid red' src='../img/admin/error2.png' alt='Sotto scorta' title='Sotto scorta'>";
		else {
			if($products['qt_ordinato'] > 0 && $sc_att > $scorta_minima)
				$img_scorta = "<img style='border:2px solid green' src='../img/admin/delivery.gif' alt='In arrivo' title='In arrivo'>";
			else
				$img_scorta = '';
		}
		
		$id_supplier = Db::getInstance()->getValue("
			SELECT id_supplier 
			FROM "._DB_PREFIX_."product 
			WHERE id_product = '".$id_product."'
		");
		
		$disponibilita = '
            <div style=\'color:#000; z-index:99999999; background-color: white;\'>
                <div style=\'float:left; margin-left:10px;\'>
                    <table class=\'table tooltip_table\' cellpadding=\'0\' cellspacing=\'0\' style=\' border:1px solid #000; float:left; width:75%\'>
                        <tr>
                            <th style=\'background: #ebebeb; text-align:center; border-bottom:1px solid #000\' colspan=\'8\'>Disponibilit&agrave;</th>
                        </tr>        
                        <tr>        
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right; width:130px\'>Magazzino EZ</th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right;\'>'.$products['magazzino'].'</th>
                            <th style=\'padding:0px; border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right; width:300px\'>
                                <table style=\'width:100%; border-collapse:collapse; margin-top:0px; height:26px; margin-bottom:0px;border-bottom:0px\'>
                                    <tr>
                                        <th style=\'border-right: 1px solid #bbb; border-bottom:0px solid #000; text-align:right;\'>Netto<img class=\'img_control\' src=\'https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif\' alt=\'Netto = Magazzino EZ - Qta ordinata dai clienti (sito)\' title=\'Netto = Magazzino EZ - Qta ordinata dai clienti (sito)\' /></th>         
                                        <th style=\'border-right: 1px solid #bbb; border-bottom:0px solid #000;text-align:right\'>'.($products['magazzino'] - $qta_ord_clienti).'</th>
                                        <th style=\'border-right: 0px solid #bbb; border-bottom:0px solid #000;text-align:right\'>Qta ordinata dai clienti (sito)</th>
                                    </tr>
                                </table>
                            </th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right\'><span style="cursor:pointer"><a class="label-tooltip" data-toggle="tooltip" data-html="true" title="" data-original-title="'.$tooltip_impegnato.'" href="javascript:void(0)">'.$qta_ord_clienti.'</a></span>
                            </th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right; width:300px\'>Qta impegnata ord. caricati su gestionale (eSolver)</th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right\'>'.$products['qt_impegnato'].'</th>
                        </tr>
                        <tr>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right\'>Mag. Amazon</th>   
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right\'>'.$products['amazon_qt'].'</th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right\' >Ordinato al fornitore</th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right\'>'.$products['qt_ordinato'].'</th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right\' colspan=\'1\'>Mag. EZ + Ordinato al fornitore - Impegnato sito</th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right\' >'.($products['magazzino'] + $products['qt_ordinato'] - $qta_ord_clienti).'</th>
                        </tr>
                        '.($context->employee->id_profile == 7 ? '' : '
                        <tr>
                            <'.($id_supplier == 11 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Allnet</'.($id_supplier == 11 ? 'th' : 'td').'>
                            <'.($id_supplier == 11 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['allnet'].'</'.($id_supplier == 11 ? 'th' : 'td').'>
                            <'.($id_supplier == 11 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>In arrivo Allnet</'.($id_supplier == 11 ? 'th' : 'td').'>
                            <'.($id_supplier == 11 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['qt_arrivo'].'</'.($id_supplier == 11 ? 'th' : 'td').'>
                            <'.($id_supplier == 11 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Data arr. Allnet</'.($id_supplier == 11 ? 'th' : 'td').'>
                            <'.($id_supplier == 11 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right; width:80px;\'>'.(($from = $products['data_arrivo'] AND $from != '1946-01-01') ? ($from == '0000-00-00' || $from == '1970-01-01' ? 'ND' : date('d-m-y',strtotime($from))) : date('d-m-y')).'</'.($id_supplier == 11 ? 'th' : 'td').'>
                        </tr>
                        <tr>
                            <'.($id_supplier == 38 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Esprinet</'.($id_supplier == 38 ? 'th' : 'td').'>
                            <'.($id_supplier == 38 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['esprinet'].'</'.($id_supplier == 38 ? 'th' : 'td').'>
                            <'.($id_supplier == 38 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>In arrivo Esprinet</'.($id_supplier == 38 ? 'th' : 'td').'>
                            <'.($id_supplier == 38 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['qt_arrivo_esprinet'].'</'.($id_supplier == 38 ? 'th' : 'td').'>
                            <'.($id_supplier == 38 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Data arr. Esprinet</'.($id_supplier == 38 ? 'th' : 'td').'>
                            <'.($id_supplier == 38 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.(($from = $products['data_arrivo_esprinet'] AND $from != '1946-01-01') ? ($from == '0000-00-00' || $from == '1970-01-01' ? 'ND' : date('d-m-y',strtotime($from))) : date('d-m-y')).'</'.($id_supplier == 38 ? 'th' : 'td').'>
                        </tr>
                        <tr>
                            <'.($id_supplier == 1450 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Attiva</'.($id_supplier == 1450 ? 'th' : 'td').'>
                            <'.($id_supplier == 1450 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['attiva'].'</'.($id_supplier == 1450 ? 'th' : 'td').'>
                            <'.($id_supplier == 1450 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>In arrivo Attiva</'.($id_supplier == 1450 ? 'th' : 'td').'>
                            <'.($id_supplier == 1450 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['qt_arrivo_attiva'].'</'.($id_supplier == 1450 ? 'th' : 'td').'>
                            <'.($id_supplier == 1450 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Data arr. Attiva</'.($id_supplier == 1450 ? 'th' : 'td').'>
                            <'.($id_supplier == 1450 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.(($from = $products['data_arrivo_attiva'] AND $from != '1946-01-01') ? ($from == '0000-00-00' || $from == '1970-01-01' ? 'ND' : date('d-m-y',strtotime($from))) : date('d-m-y')).'</'.($id_supplier == 1450 ? 'th' : 'td').'>
                        </tr>
                        <tr>
                            <'.($id_supplier == 42 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Itancia</'.($id_supplier == 42 ? 'th' : 'td').'>
                            <'.($id_supplier == 42 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['itancia'].'</'.($id_supplier == 42 ? 'th' : 'td').'>
                            <'.($id_supplier == 42 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>In arrivo Itancia</'.($id_supplier == 42 ? 'th' : 'td').'>
                            <'.($id_supplier == 42 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>0</'.($id_supplier == 42 ? 'th' : 'td').'>
                            <'.($id_supplier == 42 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Data arr. Itancia:</'.($id_supplier == 42 ? 'th' : 'td').'>
                            <'.($id_supplier == 42 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>ND</'.($id_supplier == 42 ? 'th' : 'td').'>
                        </tr>
                        <tr>
                            <'.($id_supplier == 95 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Intracom</'.($id_supplier == 95 ? 'th' : 'td').'>
                            <'.($id_supplier == 95 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['intracom'].'</'.($id_supplier == 95 ? 'th' : 'td').'>
                            <'.($id_supplier == 95 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>In arrivo Intracom</'.($id_supplier == 95 ? 'th' : 'td').'>
                            <'.($id_supplier == 95 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['qt_arrivo_intracom'].'</'.($id_supplier == 95 ? 'th' : 'td').'>
                            <'.($id_supplier == 95 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Data arr. Intracom:</'.($id_supplier == 95 ? 'th' : 'td').'>
                            <'.($id_supplier == 95 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.(($from_intracom = $products['data_arrivo_intracom'] AND $from_intracom != '1946-01-01') ? ($from_intracom == '0000-00-00' || $from_intracom == '1970-01-01' ? 'ND' : date('d-m-y',strtotime($from_intracom))) : date('d-m-y')).'</'.($id_supplier == 95 ? 'th' : 'td').'>
                        </tr>
                        <tr>
                            <'.($id_supplier == 76 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>ASIT</'.($id_supplier == 76 ? 'th' : 'td').'>
                            <'.($id_supplier == 76 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['asit'].'</'.($id_supplier == 76 ? 'th' : 'td').'>
                            <'.($id_supplier == 76 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'></'.($id_supplier == 76 ? 'th' : 'td').'>
                            <'.($id_supplier == 76 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'></'.($id_supplier == 76 ? 'th' : 'td').'>
                            <'.($id_supplier == 76 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'></'.($id_supplier == 76 ? 'th' : 'td').'>
                            <'.($id_supplier == 76 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'></'.($id_supplier == 76 ? 'th' : 'td').'>
                        </tr>
						<tr>
                            <'.($id_supplier == 1461 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Ingram</'.($id_supplier == 1461 ? 'th' : 'td').'>
                            <'.($id_supplier == 1461 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['ingram'].'</'.($id_supplier == 1461 ? 'th' : 'td').'>
                            <'.($id_supplier == 1461 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>In arrivo Ingram</'.($id_supplier == 1461 ? 'th' : 'td').'>
                            <'.($id_supplier == 1461 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['qt_arrivo_ingram'].'</'.($id_supplier == 1461 ? 'th' : 'td').'>
                            <'.($id_supplier == 1461 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Data arr. Ingram:</'.($id_supplier == 1461 ? 'th' : 'td').'>
                            <'.($id_supplier == 1461 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.(($from_ingram = $products['data_arrivo_ingram'] AND $from_ingram != '1946-01-01') ? ($from_ingram == '0000-00-00' || $from_ingram == '1970-01-01' ? 'ND' : date('d-m-y',strtotime($from_ingram))) : date('d-m-y')).'</'.($id_supplier == 1461 ? 'th' : 'td').'>
                        </tr>
                        <tr>
                            <td style=\'background: #ebebeb; border-right: 1px solid #000; border-top: 1px solid #000; text-align:right\'><strong>Totale tutti i magazzini (escl. Amazon)</strong></td>
                            <td style=\'background: #ebebeb; border-right: 1px solid #000; border-top: 1px solid #000; text-align:right\'>'.$products['qt_tot'].'</td>
                            <td style=\'background: #ebebeb; border-right: 1px solid #000; border-top: 1px solid #000; text-align:right; width:170px\'><strong>Tot. magazzini + Ordinato a fornitore - Impegnato sito</strong></td>
                            <td style=\'background: #ebebeb; border-right: 1px solid #000; border-top: 1px solid #000; text-align:right; \'>'.(($products['qt_tot'])+$products['qt_ordinato']-$products['qt_impegnato']).'</td>
                            <td style=\'background: #ebebeb; border-right: 1px solid #000; border-top: 1px solid #000; text-align:right; \'><strong>'.$img_scorta.' Scorta minima</strong></td>
                            <td style=\'background: #ebebeb; border-top: 1px solid #000; text-align:right; \'>'.$scorta_minima.'</td>
                        </tr>').'
                    </table>
                    
                    <table class=\'table tooltip_table\' cellpadding=\'0\' cellspacing=\'0\' style=\'float:left; border:1px solid #000; float:left; width:23%; margin-left:1%\'>
                        <tr>
                            <th style=\'background: #ebebeb; text-align:center; border-bottom:1px solid #000\' colspan=\'8\'>Date qta in arrivo</th>
                        </tr>
                        <tr>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right;\'>ID Ord.</th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right;\'>Qt.</th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right; width:150px\'>Fornitore</th>
                            <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right\'>Data</th>
                        </tr>
                        '.$date_arrivo_ordinato_riga.'
                    </table>
                </div>
                <div style=\'clear:both\'></div>
            </div>
        ';

		return $disponibilita;
	}

	// CORREGGERE: togliere gli argomenti in più dalle chiamate di funzione nel codice
	public static function trovaMigliorPrezzoAcquisto($id_product) 
	{
		$product_row = Db::getInstance()->getRow("
			SELECT wholesale_price, listino 
			FROM "._DB_PREFIX_."product 
			WHERE id_product = '".$id_product."'
		");

		$wholesale = $product_row['wholesale_price'];
		$listino = $product_row['listino'];

		$special = Db::getInstance()->getRow("
			SELECT * 
			FROM "._DB_PREFIX_."specific_price_wholesale spw 
			WHERE spw.id_product = '".$id_product."' 
				AND spw.from < '".date('Y-m-d H:i:s')."' 
				AND spw.to > '".date('Y-m-d H:i:s')."' 
				AND (spw.pieces = '' OR spw.pieces > 0)
		");
		
		if(($special['wholesale_price'] != 0 || ($special['wholesale_price'] == 0 && $special['reduction_1'] == 100)) && $special['wholesale_price'] < $wholesale)
			return $special['wholesale_price'];
		else
			return $wholesale;
	}

	// Trova il miglior prezzo di vendita
	public static function trovaMigliorPrezzo($id_product, $id_group, $quantity = 0, $abilita_speciale = 'y', $user_login = false) 
	{
		$context = Context::getContext();
	
		$row = Db::getInstance()->getRow("
			SELECT price, wholesale_price, login_for_offer, margin_login_for_offer 
			FROM "._DB_PREFIX_."product 
			WHERE id_product = '".$id_product."'
		");
		
		$speciale = Product::trovaPrezzoSpeciale($id_product, $id_group, $quantity);

		$unitario = $row['price'];
		$acquisto = $row['wholesale_price'];
		$login_for_offer = $row['login_for_offer'];
		$margin_login_for_offer = $row['margin_login_for_offer'];
		
		$prezzo_login_for_offer = (($acquisto * 100) / (100 - $margin_login_for_offer));
		
		if($abilita_speciale == 'y') {
			if($speciale != 0) {
				if($unitario > $speciale) {
					$unitario = $speciale;
				}
			}
		}
		
		$qtas = Db::getInstance()->executeS("
			SELECT price, reduction, specific_price_name, from_quantity 
			FROM "._DB_PREFIX_."specific_price 
			WHERE (specific_price_name = 'sc_qta_1' OR specific_price_name = 'sc_qta_2' OR specific_price_name = 'sc_qta_3' OR specific_price_name = 'sc_riv_1' OR specific_price_name = 'sc_riv_2' OR specific_price_name = 'sc_riv_3') 
				AND id_product = '".$id_product."'
		");
		
		foreach($qtas as $qta) {
			if($qta['specific_price_name'] == 'sc_qta_1'){	
				$sc_qta_1_r = $qta;
				$sc_qta_1_q = $qta['from_quantity'];
			}
			else if($qta['specific_price_name'] == 'sc_qta_2'){
				$sc_qta_2_r = $qta;
				$sc_qta_2_q = $qta['from_quantity'];
			}
			else if($qta['specific_price_name'] == 'sc_qta_3'){
				$sc_qta_3_r = $qta;
				$sc_qta_3_q = $qta['from_quantity'];
			}
			else if($qta['specific_price_name'] == 'sc_riv_1'){
				$sc_riv_1_r = $qta;
			}
			else if($qta['specific_price_name'] == 'sc_riv_2'){
				$sc_riv_2_r = $qta;
			}
			else if($qta['specific_price_name'] == 'sc_riv_3'){
				$sc_riv_3_r = $qta;
			}
		}
			
		$sc_qta_1 = $sc_qta_1_r['price'] - ($sc_qta_1_r['price'] * $sc_qta_1_r['reduction']);
		$sc_qta_2 = $sc_qta_2_r['price'] - ($sc_qta_2_r['price'] * $sc_qta_2_r['reduction']);
		$sc_qta_3 = $sc_qta_3_r['price'] - ($sc_qta_3_r['price'] * $sc_qta_3_r['reduction']);
		$sc_riv_1 = $sc_riv_1_r['price'] - ($sc_riv_1_r['price'] * $sc_riv_1_r['reduction']);
		$sc_riv_2 = $sc_riv_2_r['price'] - ($sc_riv_2_r['price'] * $sc_riv_2_r['reduction']);
		$sc_riv_3 = $sc_riv_3_r['price'] - ($sc_riv_3_r['price'] * $sc_riv_3_r['reduction']);
		
		// correggere: non sono sicura che vada bene $context->customer->isLogged()
		if($login_for_offer == 1 && ($user_login == true /*|| $context->customer->isLogged()*/))
			$unitario = $prezzo_login_for_offer;

		$prezzo_migliore = $unitario;

		// Rivenditori 1 o Riv. 2 senza sconto
		if($id_group == 3 || ($id_group == 22 && $sc_riv_2 == 0)) {

			$confronto_prezzi = array($sc_riv_1, $sc_qta_1, $sc_qta_2, $sc_qta_3);

			foreach($confronto_prezzi as $prz) {
				if($prz != 0 && $prz < $prezzo_migliore) {
					$prezzo_migliore = $prz;
					if($id_group == 22 && $sc_riv_2 == 0) {	
						$prezzo_migliore = $prezzo_migliore + ($prezzo_migliore / 100 * 3);
						if($prezzo_migliore > Product::trovaMigliorPrezzo($id_product, 1, 999999))
							$prezzo_migliore = Product::trovaMigliorPrezzo($id_product, 1, 999999);
					}
				}
			}
		
			if($speciale != 0) {
				if($prezzo_migliore > $speciale) {
					$prezzo_migliore = $speciale;
				}
			}
		}
		// Riv. 2 con sconto
		else if($id_group == 22 && $sc_riv_2 != 0) {

			$confronto_prezzi = array($sc_riv_2, $sc_qta_1, $sc_qta_2, $sc_qta_3);

			foreach($confronto_prezzi as $prz) {
				if($prz != 0 && $prz < $prezzo_migliore) {
					$prezzo_migliore = $prz;
				}
			}
		
			if($speciale != 0) {
				if($prezzo_migliore > $speciale) {
					$prezzo_migliore = $speciale;
				}
			}
		}
		// Distributori
		else if($id_group == 12) {

			$confronto_prezzi = array($sc_riv_1, $sc_riv_3, $sc_qta_1, $sc_qta_2, $sc_qta_3);

			foreach($confronto_prezzi as $prz) {
				if($prz != 0 && $prz < $prezzo_migliore) {
					$prezzo_migliore = $prz;
				}
			}
		
			if($speciale != 0) {
				if($prezzo_migliore > $speciale)
					$prezzo_migliore = $speciale;
			}
		}
		else if($sc_qta_1 != 0) {
			switch ($quantity) {
				case $quantity < $sc_qta_1_q: 
					$prezzo_migliore = $unitario; 
					break;
				case $quantity >= $sc_qta_1_q && $quantity < $sc_qta_2_q: 
					$prezzo_migliore = $sc_qta_1; 
					break;
				case $quantity >= $sc_qta_2_q && $quantity < $sc_qta_3_q: 
					($sc_qta_2 != 0 ? $prezzo_migliore = $sc_qta_2 :  $prezzo_migliore = $sc_qta_1); 
					break;
				case $quantity >= $sc_qta_3_q: 
					($sc_qta_3 != 0 ? $prezzo_migliore = $sc_qta_3 : ($sc_qta_2 != 0 ? $prezzo_migliore = $sc_qta_2 :  $prezzo_migliore = $sc_qta_1)); 
					break;
				default: 
					$prezzo_migliore = $unitario; 
					break;
			}
		}

		$prezzo_migliore = round($prezzo_migliore, 2);

		return $prezzo_migliore;
	}

	public static function trovaPrezzoSpeciale($id_product, $id_group, $quantity = 0)
	{
		$context = Context::getContext();

		$id_customer = (isset($context->customer->id) && $context->customer->id) ? (int)($context->customer->id) : 0;
		$id_country = (int)($id_customer ? Customer::getCurrentCountry($id_customer) : Configuration::get('PS_COUNTRY_DEFAULT'));
				
		$today = date("Y-m-d H:i:s");
		$righe = Db::getInstance()->executeS("
			SELECT * 
			FROM "._DB_PREFIX_."specific_price sp 
			WHERE sp.id_product = '".$id_product."' 
				AND sp.specific_price_name = ''
				 AND sp.from <= '".$today."' 
				 AND sp.to >= '".$today."'
		");

		foreach($righe as $riga) {
			if($riga['price'] == 0) {
				$riga['price'] = Db::getInstance()->getValue("
					SELECT price 
					FROM "._DB_PREFIX_."product 
					WHERE id_product = ".$id_product
				);
			}
			
			if($riga['id_group'] == $id_group) {
			
				if($riga['reduction_type'] == 'amount') {
					$prezzo_da_ritornare = $riga['price'];
				}
				else if($riga['reduction_type'] == 'percentage') {
					$prezzo_da_ritornare = $riga['price']-($riga['price']*$riga['reduction']);
				}
				
				// VERIFICO CHE CI SIA ANCORA DISPONIBILITA'
				$pz_tot = Db::getInstance()->getRow("
					SELECT stock, pieces 
					FROM "._DB_PREFIX_."specific_price_pieces 
					WHERE id_specific_price = ".$riga['id_specific_price']."
				");

				$pz = $pz_tot['stock'];
				$pz_orig = $pz_tot['pieces'];
								
				if($pz_orig != '' && $pz_orig == 0) {
					$prezzo_da_ritornare = Db::getInstance()->getValue('
						SELECT price 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$id_product
					);
				}
				
				// VERIFICO NAZIONE
				$sp_country = Db::getInstance()->getValue("
					SELECT id_country 
					FROM "._DB_PREFIX_."specific_price 
					WHERE id_specific_price = ".$riga['id_specific_price']."
				");
				
				if($sp_country != 0 && $sp_country != $id_country) {
					$prezzo_da_ritornare = Db::getInstance()->getValue('
						SELECT price 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$id_product
					);
				}

				return $prezzo_da_ritornare;
			}
		}
		
		return 0;
	}

	public static function trovaIdPrezzoSpeciale($id_product, $tipo = 'y')
	{
		$today = date('Y-m-d H:i:s');
		$riga = Db::getInstance()->getRow("
			SELECT * 
			FROM "._DB_PREFIX_."specific_price sp 
			WHERE sp.id_product = '".$id_product."'
				AND sp.specific_price_name = '' 
				AND sp.from < '".$today."' 
				AND sp.to > '".$today."' 
			ORDER BY id_specific_price DESC
		");

		if($riga['id_specific_price'] > 0){
			// VERIFICO NAZIONE
			/*
			$sp_country = Db::getInstance()->getValue("SELECT id_country FROM "._DB_PREFIX_."specific_price WHERE id_specific_price = ".$riga['id_specific_price']."");
					
			if($sp_country != 0 && $sp_country != $id_country)
				return 0;
			*/
			
			// VERIFICO CHE CI SIA ANCORA DISPONIBILITA
			$pz_tot = Db::getInstance()->getRow("
				SELECT stock, pieces 
				FROM "._DB_PREFIX_."specific_price_pieces 
				WHERE id_specific_price = ".$riga['id_specific_price']."
			");
	
			$pz = $pz_tot['stock'];
			$pz_orig = $pz_tot['pieces'];
									
			if($pz_orig != '' && $pz == 0){
				if($tipo == 'y')
					return 0;
			}
			
			return $riga['id_specific_price'];
		}			
		else
			return 0;
	}

	public static function getCurrentWholesalePrice($id_product)
	{
		$special_wholesale = Db::getInstance()->getValue("
			SELECT wholesale_price 
			FROM "._DB_PREFIX_."specific_price_wholesale sp 
			WHERE sp.to > '".date('Y-m-d H:i:s')."' 
				AND sp.id_product = '".$id_product."'
		");
		
		$normal_wholesale = Db::getInstance()->getValue("
			SELECT wholesale_price 
			FROM "._DB_PREFIX_."product 
			WHERE id_product = '".$id_product."'
		");
		
		if($special_wholesale > 0)
			return $special_wholesale;
		else
			return $normal_wholesale;
		
	}

    // CORREGGERE SE CAMBIA LA GESTIONE BUNDLE!
    // Aggiunto bundle: CORREGGERE! Spostare id_bundle come ultimo argomento? Dipende come usa la funzione PS, controllare
    public static function DACORREGGEREgetProductProperties($id_lang, $row, $id_bundle = 0, Context $context = null)
    {
        if (!$row['id_product']) {
            return false;
        }

        if ($context == null) {
            $context = Context::getContext();
        }

        $id_product_attribute = $row['id_product_attribute'] = (!empty($row['id_product_attribute']) ? (int)$row['id_product_attribute'] : null);

        // Product::getDefaultAttribute is only called if id_product_attribute is missing from the SQL query at the origin of it:
        // consider adding it in order to avoid unnecessary queries
        $row['allow_oosp'] = Product::isAvailableWhenOutOfStock($row['out_of_stock']);
        if (Combination::isFeatureActive() && $id_product_attribute === null
            && ((isset($row['cache_default_attribute']) && ($ipa_default = $row['cache_default_attribute']) !== null)
                || ($ipa_default = Product::getDefaultAttribute($row['id_product'], !$row['allow_oosp'])))) {
            $id_product_attribute = $row['id_product_attribute'] = $ipa_default;
        }
        if (!Combination::isFeatureActive() || !isset($row['id_product_attribute'])) {
            $id_product_attribute = $row['id_product_attribute'] = 0;
        }

        // Tax
        $usetax = Tax::excludeTaxeOption();

        $cache_key = $row['id_product'].'-'.$id_product_attribute.'-'.$id_lang.'-'.(int)$usetax;
        if (isset($row['id_product_pack'])) {
            $cache_key .= '-pack'.$row['id_product_pack'];
        }

        if (isset(self::$producPropertiesCache[$cache_key])) {
            return array_merge($row, self::$producPropertiesCache[$cache_key]);
        }

        // OVERRIDE
        if (isset($row['id_bundle_layered']) && $row['id_bundle_layered'] > 0)
			$row['bundle_layered'] = Bundle::getBundleForLayered($row['id_bundle_layered']);

        // Datas
        $row['category'] = Category::getLinkRewrite((int)$row['id_category_default'], (int)$id_lang);
        $row['link'] = $context->link->getProductLink((int)$row['id_product'], $row['link_rewrite'], $row['category'], $row['ean13']);

        // OVERRIDE???
        $catn = new Category($row['id_category_default']);
		$row['category_name'] = $catn->getName($id_lang);

        $row['attribute_price'] = 0;
        if ($id_product_attribute) {
            $row['attribute_price'] = (float)Product::getProductAttributePrice($id_product_attribute);
        }

        // OVERRIDE
        if (strpos($row['id_product'],'b') !== false) {
            $father = Db::getInstance()->getValue('SELECT father FROM bundle WHERE id_bundle = '.str_replace('b','',$row['id_product']).'');
			$father = new Product($father);
			if($father->fuori_produzione == 1)
				$row['fuori_produzione'] = 1;
			//if(array_shift(explode('?', basename($_SERVER['PHP_SELF']))) == 'category.php' || array_shift(explode('?', basename($_SERVER['PHP_SELF']))) == 'blocklayered-ajax.php')
			//{
				//$father = Db::getInstance()->getRow('SELECT pl.link_rewrite lnk, pl.id_lang, cl.link_rewrite AS category, p.id_product AS prd, supplier_reference FROM product p JOIN product_lang pl ON p.id_product = pl.id_product JOIN category_lang cl ON p.id_category_default = cl.id_category WHERE cl.id_lang = '.$id_lang.' AND pl.id_lang = '.$id_lang.' AND p.id_product = (SELECT father FROM bundle WHERE id_bundle = '.(str_replace('b','',$row['id_product'])).')'); 
				//$row['link'] = $context->link->getProductLink($father['prd'], $father['lnk'], $father['category'], $father['supplier_reference']);
				//$row['link'] .= '#bundles?bundles-selection=selected';
			//}
			//else {
				$row['link'] = $context->link->getBundleLink(str_replace('b','',$row['id_product']));
			//}
			$row['id_image'] = $row['id_image']."-".str_replace('b','',$row['id_product']);
			
			$row['price_tax_exc'] = Bundle::getBundlePrice(str_replace('b','',$row['id_product']), 1);
			$row['price_tax_exc'] = Tools::ps_round($row['price_tax_exc'], 2);
			$row['price'] = Bundle::getBundlePrice(str_replace('b','',$row['id_product']), 1);
			$row['price_without_reduction'] = Bundle::getBundlePrice(str_replace('b','',$row['id_product']), 1);
        }
        else {
            $row['link'] = $context->link->getProductLink((int)$row['id_product'], $row['link_rewrite'], $row['category'], $row['supplier_reference']);
			$row['price_tax_exc'] = Product::getPriceStatic((int)$row['id_product'], false, ((isset($row['id_product_attribute']) AND !empty($row['id_product_attribute'])) ? (int)($row['id_product_attribute']) : NULL), (self::$_taxCalculationMethod == PS_TAX_EXC ? 2 : 6));
			if(!$row['id_image'] || $row['id_image'] == '') 
				$row['id_image'] = Db::getInstance()->getValue('SELECT id_image FROM '._DB_PREFIX_.'image WHERE id_product = '.$row['id_product'].' AND cover = 1');
			$row['id_image'] = $row['id_product']."-".$row['id_image'];
			
			$secondary_image = Db::getInstance()->getValue('SELECT id_image FROM '._DB_PREFIX_.'image WHERE id_product = '.$row['id_product'].' AND cover = 0 ORDER BY position ASC');
			
			if($secondary_image > 0)
				$row['secondary_image'] =  $row['id_product']."-".$secondary_image;
			else
				$row['secondary_image'] = '';
                
            $row['price_tax_exc'] = Product::getPriceStatic(
                (int)$row['id_product'],
                false,
                $id_product_attribute,
                (self::$_taxCalculationMethod == PS_TAX_EXC ? 2 : 6)
            );

            // Originariamente fuori dall'if-else
            if (self::$_taxCalculationMethod == PS_TAX_EXC) {
                $row['price_tax_exc'] = Tools::ps_round($row['price_tax_exc'], 2);
                $row['price'] = Product::getPriceStatic(
                    (int)$row['id_product'],
                    true,
                    $id_product_attribute,
                    6
                );
                $row['price_without_reduction'] = Product::getPriceStatic(
                    (int)$row['id_product'],
                    false,
                    $id_product_attribute,
                    2,
                    null,
                    false,
                    false
                );
            } else {
                $row['price'] = Tools::ps_round(
                    Product::getPriceStatic(
                        (int)$row['id_product'],
                        true,
                        $id_product_attribute,
                        6
                    ),
                    (int)Configuration::get('PS_PRICE_DISPLAY_PRECISION')
                );
                $row['price_without_reduction'] = Product::getPriceStatic(
                    (int)$row['id_product'],
                    true,
                    $id_product_attribute,
                    6,
                    null,
                    false,
                    false
                );
            }
        }
        // FINE OVERRIDE

        $row['reduction'] = Product::getPriceStatic(
            (int)$row['id_product'],
            (bool)$usetax,
            $id_product_attribute,
            6,
            null,
            true,
            true,
            1,
            true,
            null,
            null,
            null,
            $specific_prices
        );

        $row['specific_prices'] = $specific_prices;

        $row['quantity'] = Product::getQuantity(
            (int)$row['id_product'],
            0,
            isset($row['cache_is_pack']) ? $row['cache_is_pack'] : null
        );

        $row['quantity_all_versions'] = $row['quantity'];

        if ($row['id_product_attribute']) {
            $row['quantity'] = Product::getQuantity(
                (int)$row['id_product'],
                $id_product_attribute,
                isset($row['cache_is_pack']) ? $row['cache_is_pack'] : null
            );
        }

        $row['id_image'] = Product::defineProductImage($row, $id_lang); // commentare?
        $row['features'] = Product::getFrontFeaturesStatic((int)$id_lang, $row['id_product']);

        $row['attachments'] = array();
        if (!isset($row['cache_has_attachments']) || $row['cache_has_attachments']) {
            $row['attachments'] = Product::getAttachmentsStatic((int)$id_lang, $row['id_product']);
        }

        $row['virtual'] = ((!isset($row['is_virtual']) || $row['is_virtual']) ? 1 : 0);

        // OVERRIDE???
        $row['virtual'] = ProductDownload::getIdFromIdProduct($row['id_product']);

        // Pack management
        $row['pack'] = (!isset($row['cache_is_pack']) ? Pack::isPack($row['id_product']) : (int)$row['cache_is_pack']);
        $row['packItems'] = $row['pack'] ? Pack::getItemTable($row['id_product'], $id_lang) : array();
        $row['nopackprice'] = $row['pack'] ? Pack::noPackPrice($row['id_product']) : 0;

        // OVERRIDE
        if ($id_bundle > 0)
			$row['quantity_in_bundle'] = Product::getQuantityInBundle($row['id_product'], $id_bundle);

        if (strpos($row['id_product'],'b') !== false) {
            $father = Db::getInstance()->getValue('SELECT father FROM bundle WHERE id_bundle = '.(str_replace('b','',$row['id_product'])));
        }
        
        if($row['stock_quantity'] < 0)
            $row['stock_quantity'] = 0;
        
        if($row['supplier_quantity'] < 0)
            $row['supplier_quantity'] = 0;
        
        if($row['asit_quantity'] < 0)
            $row['asit_quantity'] = 0;
        
        if($row['esprinet_quantity'] < 0)
            $row['esprinet_quantity'] = 0;
        
        if($row['itancia_quantity'] < 0)
            $row['itancia_quantity'] = 0;
        
        if($row['intracom_quantity'] < 0)
            $row['intracom_quantity'] = 0;
        
        if($row['attiva_quantity'] < 0)
            $row['attiva_quantity'] = 0;

		// Correggere: aggiungere ingram in questa funzione o no?
		//if($row['ingram_quantity'] < 0)
            //$row['ingram_quantity'] = 0;
        
        $supplier = Db::getInstance()->getValue('SELECT id_supplier FROM '._DB_PREFIX_.'product WHERE id_product = '.(strpos($row['id_product'],'b') !== false ? $father : $row['id_product']));
        
        switch($supplier)
        {
            case 11: $main_supplier = 'supplier_quantity '; break;
            case 38: $main_supplier = 'esprinet_quantity '; break;
            case 42: $main_supplier = 'itancia_quantity '; break;
            case 76: $main_supplier = 'asit_quantity '; break;
            case 95: $main_supplier = 'intracom_quantity '; break;
            case 1450: $main_supplier = 'attiva_quantity '; break;
            //case 1461: $main_supplier = 'ingram_quantity '; break;
            default: $main_supplier = '0 '; break;
        }	
        
        $dispo_type = Db::getInstance()->getValue('SELECT dispo_type FROM '._DB_PREFIX_.'product WHERE id_product = '.(strpos($row['id_product'],'b') !== false ? $father : $row['id_product']));
        
        switch($dispo_type)
        {
            case 0: $row['quantity'] = $row['quantity']; break;
            case 1: $row['quantity'] = $row['stock_quantity']; break;
            case 2: $row['quantity'] = Db::getInstance()->getValue('SELECT '.$main_supplier.' AS main_quantity FROM '._DB_PREFIX_.'product WHERE id_product = '.(strpos($row['id_product'],'b') !== false ? $father : $row['id_product'])) + $row['stock_quantity']; break;
            default: $row['quantity'] = $row['quantity']; break;
        }	
        //applico regola switch a tutti i prodotti... nuove disposizioni di Ezio
        //$row['quantity'] = Db::getInstance()->getValue('SELECT '.$main_supplier.' AS main_quantity FROM '._DB_PREFIX_.'product WHERE id_product = '.(strpos($row['id_product'],'b') !== false ? $father : $row['id_product'])); 
        
        if($row['quantity'] < 0)
            $row['quantity'] = 0;
        
        //$row['quantity'] += $row['stock_quantity'];

        //$esprinet_blacklist = array(25,120,43,117,133,109,37,10);
        //if(in_array($row['id_manufacturer'],$esprinet_blacklist))
            //$row['quantity'] =  Db::getInstance()->getValue('SELECT supplier_quantity + stock_quantity AS main_quantity FROM '._DB_PREFIX_.'product WHERE id_product = '.(strpos($row['id_product'],'b') !== false ? $father : $row['id_product']));
        
        if($row['id_manufacturer'] == 27){
            $row['itancia_quantity'] = 0;
            $row['quantity'] = $row['stock_quantity'] - $row['itancia_quantity'] + $row['asit_quantity'];
        }
         
        if(strpos($row['id_product'],'b') !== false)
            $qta_ord_clienti = Product::calcolaImpegnato($father);
        else
            $qta_ord_clienti = Product::calcolaImpegnato($row['id_product']);

        if(!$qta_ord_clienti)
            $qta_ord_clienti = 0;
        
        $row['qta_ord_clienti'] = $qta_ord_clienti;
        $row['real_quantity'] = $row['quantity'] - $qta_ord_clienti;

        // FINE OVERRIDE

        if ($row['pack'] && !Pack::isInStock($row['id_product'])) {
            $row['quantity'] = 0;
        }

        $row['customization_required'] = false;
        if (isset($row['customizable']) && $row['customizable'] && Customization::isFeatureActive()) {
            if (count(Product::getRequiredCustomizableFieldsStatic((int)$row['id_product']))) {
                $row['customization_required'] = true;
            }
        }

        // OVERRIDE
        
        if($row['id_manufacturer'] == 132 || $row['id_manufacturer'] == 133 || $row['id_manufacturer'] == 37 || $row['id_manufacturer'] == 10 || $row['id_manufacturer'] == 27 || $row['id_manufacturer'] == 105 || $row['id_manufacturer'] == 117 || $row['id_manufacturer'] == 25 || $row['id_manufacturer'] == 120 || $row['id_manufacturer'] == 43 || $row['id_manufacturer'] == 109 || $row['id_manufacturer'] == 179 || $row['id_manufacturer'] == 184 || $row['id_manufacturer'] == 172 || $row['id_manufacturer'] == 108 || $row['id_manufacturer'] == 182 || $row['id_manufacturer'] == 113 || $row['id_manufacturer'] == 116 || $row['id_manufacturer'] == 181 || $row['id_manufacturer'] == 191)
            $row['migliorprezzo_link'] = 1;
        else
            $row['migliorprezzo_link'] = 0;

        //if(Product::checkPrezzoSpeciale($row['id_product'],1,999999) == 'yes')
			//$row['migliorprezzo_link'] = 1;
		//else
			//$row['migliorprezzo_link'] = 0;

        $row['total_reviews'] = (int)(Product::getCommentNumber((int)$row['id_product']));

		$averageTotal = 0;
        $averages = Product::getAveragesByProduct($row['id_product'], (int)$id_lang);
		foreach ($averages AS $average)
			$averageTotal += (float)($average);

		$averageTotal = count($averages) ? ($averageTotal / count($averages)) : 0;
		$row['averageTotal'] = $averageTotal;

        // FINE OVERRIDE

        $row = Product::getTaxesInformations($row, $context);
        self::$producPropertiesCache[$cache_key] = $row;
        return self::$producPropertiesCache[$cache_key];
    }

    // CORREGGERE SE CAMBIA LA GESTIONE BUNDLE!
    // Aggiunto bundle: cambiare nome dopo aver corretto la funzione sopra!
    public static function DACORREGGEREgetProductsProperties($id_lang, $query_result, $id_bundle = 0)
    {
        $results_array = array();

        if (is_array($query_result)) {
            foreach ($query_result as $row) {
                if ($row2 = Product::getProductProperties($id_lang, $row, $id_bundle)) {
                    $results_array[] = $row2;
                }
            }
        }

        return $results_array;
    }

    // Aggiunto supplier_reference
    public static function getUrlRewriteInformations($id_product)
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT pl.`id_lang`, pl.`link_rewrite`, p.`ean13`, cl.`link_rewrite` AS category_rewrite, p.`supplier_reference`
			FROM `'._DB_PREFIX_.'product` p
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product`'.Shop::addSqlRestrictionOnLang('pl').')
			'.Shop::addSqlAssociation('product', 'p').'
			LEFT JOIN `'._DB_PREFIX_.'lang` l ON (pl.`id_lang` = l.`id_lang`)
			LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (cl.`id_category` = product_shop.`id_category_default`  AND cl.`id_lang` = pl.`id_lang`'.Shop::addSqlRestrictionOnLang('cl').')
			WHERE p.`id_product` = '.(int)$id_product.'
			AND l.`active` = 1
		');
    }

	function checkPrezzoSpeciale($id_product, $id_group, $quantity = 0, $type = 0) {
		
		$today = date("Y-m-d H:i:s");
		$righe = Db::getInstance()->executeS("
			SELECT * 
			FROM "._DB_PREFIX_."specific_price sp 
			WHERE sp.id_product = '".$id_product."' 
				AND sp.specific_price_name = '' 
				AND sp.from < '".$today."' 
				AND sp.to > '".$today."'
		");

		foreach($righe as $riga) {
			
			if($riga['price'] == 0) {
				$riga['price'] = Db::getInstance()->getValue("SELECT price FROM FROM "._DB_PREFIX_."product WHERE id_product = ".$id_product."");
			}

			if($riga['id_group'] = $id_group || $riga['id_group'] == 0) {
			
				if($riga['reduction_type'] == 'amount') {
					$prezzo_da_ritornare = $riga['price'];
				}
				else if($riga['reduction_type'] == 'percentage') {
					$prezzo_da_ritornare = $riga['price']*$riga['reduction'];
				}
				
				$pz_tot = Db::getInstance()->getRow("
					SELECT stock, pieces 
					FROM FROM "._DB_PREFIX_."specific_price_pieces 
					WHERE id_specific_price = ".$riga['id_specific_price']."
				");

				$pz = $pz_tot['stock'];
				$pz_orig = $pz_tot['pieces'];
				
				if($type == 1) {
					return $riga['id_specific_price'];
				}
				else {
					return "yes";
				}
			}
		}
		
		if($type == 1) {
			return 0;
		}
		else {
			return "no";
		}
	}
	
    // $extra inutilizzato, eliminare? -> se sì, anche da tutte le chiamate di funzione
    public static function searchByName($id_lang, $query, $extra = '', $opzioni = '', Context $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }

        $opzioni = unserialize($opzioni);
		
		//OR (MATCH(pl.name) AGAINST ("+'.str_replace(' ',' +',$query).'" IN BOOLEAN MODE))
		$sql = '
			SELECT p.`id_product`, pl.`name`, p.`active`, p.`reference`, p.`listino`, p.`id_category_default`, p.id_supplier, p.ean13, p.asin, p.`stock_quantity`, p.`ordinato_quantity`, p.`arrivo_quantity`, p.`supplier_quantity`, p.`esprinet_quantity`, p.`attiva_quantity`, p.amazon_quantity, p.itancia_quantity, p.asit_quantity, p.impegnato_quantity, p.id_manufacturer, p.`date_add`, p.`date_upd`, m.`name` AS manufacturer_name ,
			(CASE p.id_supplier WHEN 11 THEN supplier_quantity WHEN 38 THEN esprinet_quantity WHEN 1450 THEN attiva_quantity WHEN 42 THEN itancia_quantity WHEN 95 THEN intracom_quantity WHEN 76 THEN asit_quantity ELSE 0 END) first_supplier,
			(CASE WHEN p.other_suppliers LIKE "%11%" THEN supplier_quantity WHEN p.other_suppliers LIKE "%38%" THEN esprinet_quantity WHEN p.other_suppliers LIKE "%1450%" THEN attiva_quantity WHEN p.other_suppliers LIKE "%42%" THEN itancia_quantity WHEN p.other_suppliers LIKE "%95%" THEN intracom_quantity WHEN p.other_suppliers LIKE "%76%" THEN asit_quantity ELSE 0 END) second_supplier,
			(CASE p.id_supplier WHEN 11 THEN "Allnet" WHEN 38 THEN "Esprinet" WHEN 42 THEN "Itancia" WHEN 95 THEN "Intracom" WHEN 76 THEN "Asit" ELSE "N.D." END) first_supplier_name,
			(CASE WHEN p.other_suppliers LIKE "%11%" THEN "Allnet" WHEN p.other_suppliers LIKE "%38%" THEN "Esprinet" WHEN p.other_suppliers LIKE "%42%" THEN "Itancia" WHEN p.other_suppliers LIKE "%95%" THEN "Intracom" WHEN p.other_suppliers LIKE "%76%" THEN "Asit" ELSE "N.D." END) second_supplier_name,
			(CASE p.id_supplier WHEN 11 THEN arrivo_quantity WHEN 38 THEN arrivo_esprinet_quantity WHEN 1450 THEN arrivo_attiva_quantity WHEN 42 THEN 0 WHEN 95 THEN arrivo_intracom_quantity WHEN 76 THEN 0 ELSE 0 END) arrivo_supplier,
			cap.prev
			FROM `'._DB_PREFIX_.'product` p
			LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON p.`id_product` = cp.`id_product`
			
			LEFT JOIN (SELECT sum(quantity) prev, cp.id_product FROM '._DB_PREFIX_.'cart_product cp JOIN '._DB_PREFIX_.'cart c ON c.id_cart = cp.id_cart WHERE c.preventivo = 1 AND c.date_upd >= ( CURDATE() - INTERVAL 15 DAY ) GROUP BY cp.id_product) cap ON cap.id_product = p.id_product
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')
			LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON m.`id_manufacturer` = p.`id_manufacturer`
			
			WHERE '.($query != '' ? '(p.id_product LIKE \'%'.pSQL($query).'%\' OR pl.`name` LIKE \'%'.pSQL($query).'%\' OR p.`reference` LIKE \'%'.pSQL($query).'%\' OR p.`fnsku` LIKE \'%'.pSQL($query).'%\' OR p.`supplier_reference` LIKE \'%'.pSQL($query).'%\' OR p.`ean13` LIKE \'%'.pSQL($query).'%\'  OR p.`upc` LIKE \'%'.pSQL($query).'%\' OR p.`asin` LIKE \'%'.pSQL($query).'%\'  OR m.`name` LIKE \'%'.pSQL($query).'%\'
			OR REPLACE(pl.name," ","") LIKE \'%'.pSQL($query).'%\'
			OR REPLACE(p.`reference`," ","") LIKE \'%'.pSQL($query).'%\'
			OR REPLACE(p.`supplier_reference`," ","") LIKE \'%'.pSQL($query).'%\'
			
			OR REPLACE(pl.name," ","") LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\'
			OR REPLACE(p.`reference`," ","") LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\'
			OR REPLACE(p.`supplier_reference`," ","") LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\'
		
			OR (pl.name) LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\'
			OR (p.`reference`) LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\'
			OR (p.`supplier_reference`) LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\'
			
			'.($opzioni['note'] == 'on' ? 'OR REPLACE(p.`note`," ","") LIKE \'%'.pSQL($query).'%\' OR REPLACE(p.`note_fornitore`," ","") LIKE \'%'.pSQL($query).'%\'' : '').'
			
			) ' : 'p.id_product > 0 ').'
			
			AND (
			'.($opzioni['online'] == 'on' ? 'p.active = 1' : 'p.active >= 0').'
			'.($opzioni['offline'] == 'on' ? 'OR p.active = 0' : '').'
			'.($opzioni['old'] == 'on' ? 'OR p.active = 2' : '').')
			
			'.($opzioni['disponibili'] == 'on' ? 'AND p.quantity > 0' : '').'
			'.($opzioni['marca'] != 0 ? 'AND p.id_manufacturer = '.$opzioni['marca'] : '').'
			'.($opzioni['serie'] != 0 ? 'AND p.id_product IN (SELECT p.id_product FROM '._DB_PREFIX_.'feature_value fv JOIN '._DB_PREFIX_.'feature_product fp ON fp.id_feature_value = fv.id_feature_value JOIN '._DB_PREFIX_.'product p ON fp.id_product = p.id_product WHERE fv.id_feature_value = '.$opzioni['serie'].')' : '').'
			'.($opzioni['fornitore'] != 0 ? 'AND p.id_supplier = '.$opzioni['fornitore'] : '').'
			GROUP BY `id_product`
			'.(isset($_GET['orderby']) ? 'ORDER BY '.$_GET['orderby'].(isset($_GET['orderway']) ? ' '.$_GET['orderway'] : 'ASC') : 'ORDER BY pl.`name` ASC').'
        ';

        $result = Db::getInstance()->executeS($sql);

        if (!$result) {
            return false;
        }

        $results_array = array();
        foreach ($result as $row) {
            $row['price_tax_incl'] = Product::getPriceStatic($row['id_product'], true, null, 2); // 'priceii' 1.4
            $row['price_tax_excl'] = Product::getPriceStatic($row['id_product'], false, null, 2);
			$row['wholesale_price'] = Product::getCurrentWholesalePrice($row['id_product']);
			$row['quantity'] = Product::getQuantity($row['id_product']);
            $results_array[] = $row;
        }
        return $results_array;
    }

    // CORREGGERE QUERY e nome funzione
    public static function DACORREGGEREgetNewProducts($id_lang, $page_number = 0, $nb_products = 10, $count = false, $order_by = null, $order_way = null, Context $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }

        $front = true;
        if (!in_array($context->controller->controller_type, array('front', 'modulefront'))) {
            $front = false;
        }

        if ($page_number < 0) {
            $page_number = 0;
        }
        if ($nb_products < 1) {
            $nb_products = 10;
        }
        if (empty($order_by) || $order_by == 'position') {
            $order_by = 'date_add';
        }
        if (empty($order_way)) {
            $order_way = 'DESC';
        }
        if ($order_by == 'id_product' || $order_by == 'price' || $order_by == 'date_add' || $order_by == 'date_upd') {
            $order_by_prefix = 'product_shop';
        } elseif ($order_by == 'name') {
            $order_by_prefix = 'pl';
        }
        if (!Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way)) {
            die(Tools::displayError());
        }

        $sql_groups = '';
        if (Group::isFeatureActive()) {
            $groups = FrontController::getCurrentCustomerGroups();
            $sql_groups = ' AND EXISTS(SELECT 1 FROM `'._DB_PREFIX_.'category_product` cp
				JOIN `'._DB_PREFIX_.'category_group` cg ON (cp.id_category = cg.id_category AND cg.`id_group` '.(count($groups) ? 'IN ('.implode(',', $groups).')' : '= 1').')
                WHERE cp.`id_product` = p.`id_product`)';
        }

        if (strpos($order_by, '.') > 0) {
            $order_by = explode('.', $order_by);
            $order_by_prefix = $order_by[0];
            $order_by = $order_by[1];
        }

        if ($count) {
            $sql = 'SELECT COUNT(p.`id_product`) AS nb
					FROM `'._DB_PREFIX_.'product` p
					'.Shop::addSqlAssociation('product', 'p').'
					WHERE product_shop.`active` = 1
					AND product_shop.`date_add` > "'.date('Y-m-d', strtotime('-'.(Configuration::get('PS_NB_DAYS_NEW_PRODUCT') ? (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY')).'"
					'.($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '').'
					'.$sql_groups;
            return (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
        }

        // OVERRIDE
        $id_category = $_GET['cat']; 

        // correggere!!!!
		$sql_correggere = '
            SELECT p.*, pl.`description`, pl.`description_short`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`,  pl.`cat_homepage`,  pl.`desc_homepage`, pl.`meta_title`, pl.`name`, p.`ean13`, p.`upc`,
                i.`id_image`, il.`legend`, t.`rate`, m.`name` AS manufacturer_name,
                (p.`price` * ((100 + (t.`rate`))/100)) AS orderprice, pa.id_product_attribute
            FROM `'._DB_PREFIX_.'product` p
            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')
            LEFT OUTER JOIN `'._DB_PREFIX_.'product_attribute` pa ON (p.`id_product` = pa.`id_product` AND `default_on` = 1)
            LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
            LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)($id_lang).')
            LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON cp.`id_product` = p.`id_product`
            LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (p.`id_tax_rules_group` = tr.`id_tax_rules_group`
            AND tr.`id_country` = '.(int)Country::getDefaultCountryId().'
            AND tr.`id_state` = 0)
            LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
            LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
            WHERE p.`active` = 1'.($id_category > 0 ? ' AND cp.`id_category` = '.$id_category : '').'
            AND p.id_category_default != 246 AND p.id_category_default != 256
            AND p.`id_product` IN (
                SELECT cp.`id_product`
                FROM `'._DB_PREFIX_.'category_group` cg
                LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON (cp.`id_category` = cg.`id_category`)
                WHERE cg.`id_group` '.$sqlGroups.'
            )
            GROUP BY p.id_product
            
            ORDER BY p.id_product DESC, '.(isset($orderByPrefix) ? pSQL($orderByPrefix).'.' : '').'`'.pSQL($orderBy).'` '.pSQL($orderWay).'
            
            LIMIT 15
        ';

        // originale 1.6 per confronto
        /*
        $sql = new DbQuery();
        $sql->select(
            'p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity, pl.`description`, pl.`description_short`, pl.`link_rewrite`, pl.`meta_description`,
			pl.`meta_keywords`, pl.`meta_title`, pl.`name`, pl.`available_now`, pl.`available_later`, image_shop.`id_image` id_image, il.`legend`, m.`name` AS manufacturer_name,
			product_shop.`date_add` > "'.date('Y-m-d', strtotime('-'.(Configuration::get('PS_NB_DAYS_NEW_PRODUCT') ? (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY')).'" as new'
        );

        $sql->from('product', 'p');
        $sql->join(Shop::addSqlAssociation('product', 'p'));
        $sql->leftJoin('product_lang', 'pl', '
			p.`id_product` = pl.`id_product`
			AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl')
        );
        $sql->leftJoin('image_shop', 'image_shop', 'image_shop.`id_product` = p.`id_product` AND image_shop.cover=1 AND image_shop.id_shop='.(int)$context->shop->id);
        $sql->leftJoin('image_lang', 'il', 'image_shop.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang);
        $sql->leftJoin('manufacturer', 'm', 'm.`id_manufacturer` = p.`id_manufacturer`');

        $sql->where('product_shop.`active` = 1');
        if ($front) {
            $sql->where('product_shop.`visibility` IN ("both", "catalog")');
        }
        $sql->where('product_shop.`date_add` > "'.date('Y-m-d', strtotime('-'.(Configuration::get('PS_NB_DAYS_NEW_PRODUCT') ? (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY')).'"');
        if (Group::isFeatureActive()) {
            $groups = FrontController::getCurrentCustomerGroups();
            $sql->where('EXISTS(SELECT 1 FROM `'._DB_PREFIX_.'category_product` cp
				JOIN `'._DB_PREFIX_.'category_group` cg ON (cp.id_category = cg.id_category AND cg.`id_group` '.(count($groups) ? 'IN ('.implode(',', $groups).')' : '= 1').')
				WHERE cp.`id_product` = p.`id_product`)');
        }

        $sql->orderBy((isset($order_by_prefix) ? pSQL($order_by_prefix).'.' : '').'`'.pSQL($order_by).'` '.pSQL($order_way));
        $sql->limit($nb_products, $page_number * $nb_products);

        if (Combination::isFeatureActive()) {
            $sql->select('product_attribute_shop.minimal_quantity AS product_attribute_minimal_quantity, IFNULL(product_attribute_shop.id_product_attribute,0) id_product_attribute');
            $sql->leftJoin('product_attribute_shop', 'product_attribute_shop', 'p.`id_product` = product_attribute_shop.`id_product` AND product_attribute_shop.`default_on` = 1 AND product_attribute_shop.id_shop='.(int)$context->shop->id);
        }
        $sql->join(Product::sqlStock('p', 0));
        */

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        if (!$result) {
            return false;
        }

        if ($order_by == 'price') {
            Tools::orderbyPrice($result, $order_way);
        }

        $products_ids = array();
        foreach ($result as $row) {
            $products_ids[] = $row['id_product'];
        }
        // Thus you can avoid one query per product, because there will be only one query for all the products of the cart
        Product::cacheFrontFeatures($products_ids, $id_lang);
        return Product::getProductsProperties((int)$id_lang, $result);
    }

    public static function getNewProductsImgOnly($id_lang, $order_by = NULL, $order_way = NULL)
	{
        if (empty($order_by) || $order_by == 'position') {
            $order_by = 'date_add';
        }
        if (empty($order_way)) {
            $order_way = 'DESC';
        }
        if ($order_by == 'id_product' || $order_by == 'price' || $order_by == 'date_add' || $order_by == 'date_upd') {
            $order_by_prefix = 'p';
        } elseif ($order_by == 'name') {
            $order_by_prefix = 'pl';
        }
        if (!Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way)) {
            die(Tools::displayError());
        }
		
        $id_category = $_GET['cat'];
	
		$sql = '
            SELECT p.*, pl.`description`, pl.`description_short`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`,  pl.`cat_homepage`,  pl.`desc_homepage`, pl.`meta_title`, pl.`name`, p.`ean13`, p.`upc`,
                i.`id_image`, il.`legend`
            FROM `'._DB_PREFIX_.'product` p
            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')
            LEFT OUTER JOIN `'._DB_PREFIX_.'product_attribute` pa ON (p.`id_product` = pa.`id_product` AND `default_on` = 1)
            JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
            LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)($id_lang).')
            WHERE p.`active` = 1'.($id_category > 0 ? ' AND cp.`id_category` = '.$id_category : '').'
            AND p.id_category_default != 246 AND p.id_category_default != 256
            
            GROUP BY p.id_product
            
            ORDER BY (CASE WHEN p.price = 0 THEN 1 ELSE 0 END), p.id_product DESC, '.(isset($order_by_prefix) ? pSQL($order_by_prefix).'.' : '').'`'.pSQL($order_by).'` '.pSQL($order_way).'
            
            LIMIT 4
        ';

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        if (!$result) {
            return false;
        }

        if ($order_by == 'price') {
            Tools::orderbyPrice($result, $order_way);
        }

        $products_ids = array();
        foreach ($result as $row) {
            $products_ids[] = $row['id_product'];
        }
        // Thus you can avoid one query per product, because there will be only one query for all the products of the cart
        Product::cacheFrontFeatures($products_ids, $id_lang);
        return Product::getProductsProperties((int)$id_lang, $result);
	}

    // Usata in AdminProductsController (duplicazione prodotto)
    public function duplicateComparaprezzi($id_product_old, $id_product_new)
	{
		$query = '
            SELECT id_category_gst, trovaprezzi, google_shopping, eprice, amazon, comparaprezzi_check
            FROM `'._DB_PREFIX_.'product`
            WHERE `id_product` = '.(int)($id_product_old).'
        ';
		
		$row = Db::getInstance()->getRow($query);

		Db::getInstance()->executeS("
            UPDATE "._DB_PREFIX_."product 
            SET id_category_gst = $row[id_category_gst], eprice = $row[eprice], amazon = '".$row['amazon']."', trovaprezzi = $row[trovaprezzi], google_shopping = $row[google_shopping], comparaprezzi_check = $row[comparaprezzi_check] 
            WHERE id_product = ".$id_product_new."
        ");
	}

    // da fare
    // public static function priceCalculation();

    // vedi altre funzioni in override 1.4 che mancano


    /* TAGS */

    // Funzione per aggiungere i tag in automatico
    // CONTROLLARE! Usare alias al posto di nome tabella e spostare echo nei tpl
	public static function addTagAuto($id_prodotto, $id_lang) 
	{
		$tags = array();

		$tagq = Db::getInstance()->executeS("SELECT name FROM "._DB_PREFIX_."tag WHERE id_lang = $id_lang") ;
		foreach($tagq as $rowtag) {
			$tags[] = $rowtag['name'];
		}

		echo "<br /><br /><br /><br /><br />";
		
		// Cerco il mio prodotto
		$query = "SELECT product.id_product AS id, product_lang.name AS prodotto, product.reference AS codice, category_lang.name AS categoria, manufacturer.name AS produttore, category.id_category FROM "._DB_PREFIX_."product JOIN "._DB_PREFIX_."product_lang ON product.id_product = product_lang.id_product JOIN "._DB_PREFIX_."manufacturer ON product.id_manufacturer = manufacturer.id_manufacturer JOIN "._DB_PREFIX_."category_product ON product_lang.id_product = category_product.id_product JOIN "._DB_PREFIX_."category_lang ON category_product.id_category = category_lang.id_category JOIN "._DB_PREFIX_."category ON category_product.id_category = category.id_category WHERE product.id_product = ".$id_prodotto." AND product_lang.id_lang = ".$id_lang;
		$rows = Db::getInstance()->executeS($query);

		foreach($rows as $row) {
			$id_prodotto = (int)$id_prodotto;
			
			// Scompongo il nome del prodotto
			$arrprodotto = explode(" ",$row['prodotto']);
			$stringa = "";

			// Per ogni parola del prodotto, controllo che non sia gi? nei tag
			foreach($arrprodotto as $elemento) {
				if (strlen($elemento) > 3 && !is_numeric($elemento)) {
					// Se la parola ? gi? nei tag non faccio niente
					if(in_array(ucwords($elemento), $tags)) {

					}
					// Altrimenti la inserisco ex novo nella tabella dei tag
					else {
						Db::getInstance()->executeS("REPLACE INTO "._DB_PREFIX_."tag (id_tag, id_lang, name) VALUES ('NULL', 5, '".addslashes(ucwords($elemento))."')") ;

						$tags[] = ucwords($elemento);
						$last_id = mysql_insert_id() ;
					}

					//Seleziono quindi l'elemento della tabella tag che corrisponde alla parola che sto percorrendo nel ciclo foreach
					$qx = ("SELECT id_tag FROM "._DB_PREFIX_."tag WHERE name =  '".addslashes(ucwords($elemento))."'");
					$rowx = Db::getInstance()->getRow($qx);
					
					//... e quindi la associo come tag del prodotto
					Db::getInstance()->executeS("INSERT IGNORE INTO "._DB_PREFIX_."product_tag (id_product, id_tag) VALUES ('".$id_prodotto."', '".$rowx['id_tag']."')") ;

					//Semplicemente per visualizzare
					$stringa.= $elemento." - ";
				}
				else { 

                }
			}

			// Controllo che il produttore non sia gi? nei tag
			if(in_array(ucwords($row['produttore']), $tags)) {

			}
			// Se non c'?, lo aggiungo alla tabella tag nel database
			else {
				Db::getInstance()->executeS("REPLACE INTO "._DB_PREFIX_."tag (id_tag, id_lang, name) VALUES ('NULL', 5, '".addslashes(ucwords($row['produttore']))."')") ;
		
				$tags[] = ucwords($row['produttore']);
			}

			// Seleziono quindi l'elemento della tabella tag che corrisponde al produttore
			$qx = ("SELECT id_tag FROM "._DB_PREFIX_."tag WHERE name =  '".addslashes(ucwords($row['produttore']))."'") ;
			$rowx = Db::getInstance()->getRow($qx) ;
			
			// ... e quindi lo associo come tag del prodotto
			Db::getInstance()->executeS("INSERT IGNORE INTO "._DB_PREFIX_."product_tag (id_product, id_tag) VALUES ($id_prodotto, $rowx[id_tag])") ;

            // Da questa riga fino al prossimo commento, faccio la stessa cosa per la categoria
			if(in_array(ucwords($row['categoria']), $tags)) {

			}
			else {
				$active_category = Db::getInstance()->getValue('SELECT active FROM '._DB_PREFIX_.'category WHERE id_category = '.$row['category']);
				
                if($active_category == 1) {	
					Db::getInstance()->executeS("REPLACE INTO "._DB_PREFIX_."tag (id_tag, id_lang, name) VALUES ('NULL', 5, '".addslashes(ucwords($row['categoria']))."')") ;

					$tags[] = ucwords($row['categoria']);
				}
			}

			$qx = ("SELECT id_tag FROM "._DB_PREFIX_."tag WHERE name =  '".addslashes(ucwords($row['categoria']))."'") ;
			$rowx = Db::getInstance()->getRow($qx);
			Db::getInstance()->executeS("INSERT IGNORE INTO "._DB_PREFIX_."product_tag (id_product, id_tag) VALUES ($id_prodotto, $rowx[id_tag])") ;

			// Aggiungo categoria, codice e produttore alla stringa
			$stringa.= " - ".$row['categoria']." - ".$row['produttore']." - ".$row['codice']." - ";
			$array_delle_specifiche = array(778,779,781,783,786,785,789,788,776,775,429,793,791,811,795,799,797,798,801,802,805,804,807);
		
			foreach($array_delle_specifiche as $specifica) {
				// Cerco tutti i prodotti che hanno la specifica "Ottimizzata per"
				$query4 = "SELECT feature_value_lang.value FROM "._DB_PREFIX_."feature_value_lang JOIN "._DB_PREFIX_."feature_product ON feature_value_lang.id_feature_value = feature_product.id_feature_value JOIN "._DB_PREFIX_."feature_value ON feature_value.id_feature_value = feature_value_lang.id_feature_value WHERE feature_product.id_product = ".$id_prodotto." AND feature_value_lang.id_lang = ".$id_lang." AND feature_value.id_feature = 431";
				
				$row4 = Db::getInstance()->getRow($query4);

				// Se il valore della specifica ? gi? presente nei tag, non faccio niente
				if(in_array(ucwords($row4['value']), $tags)) {

				}
				// Altrimenti lo inserisco nella tabella dei tag
				else {
					Db::getInstance()->executeS("REPLACE INTO "._DB_PREFIX_."tag (id_tag, id_lang, name) VALUES ('NULL', 5, '".addslashes(ucwords($row4['value']))."')") ;

					$tags[] = ucwords($row4['value']);
				}
			
				// Seleziono quindi il tag corrispondente al valore della specifica "Ottimizzata per"
				$qx = ("SELECT id_tag FROM "._DB_PREFIX_."tag WHERE name =  '".addslashes(ucwords($row4['value']))."'") ;
				$rowx = Db::getInstance()->getRow($qx);
			
				// ... e lo associo al prodotto
				Db::getInstance()->executeS("INSERT IGNORE INTO "._DB_PREFIX_."product_tag (id_product, id_tag) VALUES ($id_prodotto, $rowx[id_tag])") ;

				// $stringa.= " - ".$row4['value'];
			}
			
			// Seleziono il valore della specifica Compatibilit? Skype
			$query6 = "SELECT feature_value_lang.value FROM "._DB_PREFIX_."feature_value_lang JOIN "._DB_PREFIX_."feature_product ON feature_value_lang.id_feature_value = feature_product.id_feature_value JOIN "._DB_PREFIX_."feature_value ON feature_value.id_feature_value = feature_value_lang.id_feature_value WHERE feature_product.id_product = ".$id_prodotto." AND feature_value_lang.id_lang = ".$id_lang." AND feature_value.id_feature = 771";
			
			$row6 = Db::getInstance()->getRow($query6);

			// Se il valore ? "S?", allora creo una variabile di tipo stringa chiamandola "Compatibile Skype" 
			if($row6['value'] == 's?') {
				$vv = "Compatibile Skype";
		
				// Se quest'ultima è già nel db non faccio niente
				if(in_array("Compatibile Skype", $tags)) {
				}
				// Altrimenti la inserisco nella tabella tag
				else {
					Db::getInstance()->executeS("REPLACE INTO "._DB_PREFIX_."tag (id_tag, id_lang, name) VALUES ('NULL', 5, 'Compatibile Skype')") ;
					$tags[] = "Compatibile Skype";
				}
				
				// Quindi cerco il tag che si chiama "Compatibile Skype" e gli associo il prodotto
				$qx = ("
					SELECT id_tag 
					FROM "._DB_PREFIX_."tag 
					WHERE name =  'Compatibile Skype'
				");
				$rowx = Db::getInstance()->getRow($qx);
				Db::getInstance()->executeS("INSERT IGNORE INTO "._DB_PREFIX_."product_tag (id_product, id_tag) VALUES ($id_prodotto, $rowx[id_tag])");
			}
			// Se il valore non c'? lascio la stringa vuota
			else {
			    $vv = "";
			}

			$stringa.= " - ".$vv;

			// Ripeto la stessa operazione per i prodotti compatibili con Mac
			$query7 = "SELECT feature_value_lang.value FROM "._DB_PREFIX_."feature_value_lang JOIN "._DB_PREFIX_."feature_product ON feature_value_lang.id_feature_value = feature_product.id_feature_value JOIN "._DB_PREFIX_."feature_value ON feature_value.id_feature_value = feature_value_lang.id_feature_value WHERE feature_product.id_product = ".$id_prodotto." AND feature_value_lang.id_lang = ".$id_lang." AND feature_value.id_feature = 763";
			
			$row7 = Db::getInstance()->getRow($query7);

			if($row7['value'] == 's?') {
				$xx = "Compatibile Mac";

				if(in_array("Compatibile Mac", $tags)) {

				}
				else {
					Db::getInstance()->executeS("REPLACE INTO "._DB_PREFIX_."tag (id_tag, id_lang, name) VALUES ('NULL', 5, 'Compatibile Mac')") ;

					$tags[] = "Compatibile Mac";
				}
				$qx = ("SELECT id_tag FROM "._DB_PREFIX_."tag WHERE name =  'Compatibile Mac'") ;
				$rowx = Db::getInstance()->getRow($qx);
				Db::getInstance()->executeS("INSERT IGNORE INTO "._DB_PREFIX_."product_tag (id_product, id_tag) VALUES ($id_prodotto, $rowx[id_tag])");
			}
			else {
				$xx = "";
			}

			$stringa.= " - ".$xx;
			echo $stringa;
			echo "<br />";
		}

		// E per finire cancello tutti i tag vuoti e tutte le associazioni tra prodotti e tag vuoti.
		mysql_query("
			DELETE product_tag.*, tag.* 
			FROM "._DB_PREFIX_."tag 
			JOIN "._DB_PREFIX_."product_tag ON tag.id_tag = product_tag.id_tag 
			WHERE tag.name = ''
		");
	}

    // Aggiunto controllo $tag_name non vuoto
    public function getTags($id_lang)
    {
        if (!$this->isFullyLoaded && is_null($this->tags)) {
            $this->tags = Tag::getProductTags($this->id);
        }

        if (!($this->tags && array_key_exists($id_lang, $this->tags))) {
            return '';
        }

        $result = '';
        foreach ($this->tags[$id_lang] as $tag_name) {
            if($tag_name != '') // override
                $result .= $tag_name.', ';
        }

        return rtrim($result, ', ');
    }


    /* ACCESSORI */

    // Aggiunte condizioni WHERE
    public static function getAccessoriesLight($id_lang, $id_product)
    {
        return Db::getInstance()->executeS('
			SELECT p.`id_product`, p.`reference`, pl.`name`
			FROM `'._DB_PREFIX_.'accessory`
			LEFT JOIN `'._DB_PREFIX_.'product` p ON (p.`id_product`= `id_product_2`)
			'.Shop::addSqlAssociation('product', 'p').'
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (
				p.`id_product` = pl.`id_product`
				AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
			)
			WHERE p.price > 0 
                AND p.active = 1 
                AND `id_product_1` = '.(int)$id_product
        );
    }

    public static function getAccessoriesLightAll($id_lang, $id_product)
	{
		return Db::getInstance()->ExecuteS('
			SELECT id_product_2 `id_product`, p.`reference`, pl.`name`
			FROM `'._DB_PREFIX_.'accessory`
			LEFT JOIN `'._DB_PREFIX_.'product` p ON (p.`id_product`= `id_product_2`)
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (
                p.`id_product` = pl.`id_product` 
                AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
            )
			WHERE id_product_2 != 0 
				AND `id_product_1` = '.(int)($id_product)
		);
	}

    public function getAccessories($id_lang, $active = true)
    {
        $sql = '
			SELECT p.*, pl.`description`, pl.`description_short`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`cat_homepage`, pl.`desc_homepage`, pl.`meta_title`, pl.`name`, p.`ean13`, p.`upc`,
                i.`id_image`, il.`legend`, t.`rate`, m.`name` as manufacturer_name, cl.`name` AS category_default, 
                DATEDIFF(
                    p.`date_add`, 
                    DATE_SUB(NOW(), INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY)
                ) > 0 AS new
			FROM `'._DB_PREFIX_.'accessory`
                LEFT JOIN `'._DB_PREFIX_.'product` p ON p.`id_product` = `id_product_2`
                LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (
                    p.`id_product` = pl.`id_product` 
                    AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
                )
                LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (
                    p.`id_category_default` = cl.`id_category` 
                    AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
                )
                LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
                LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (
                    i.`id_image` = il.`id_image` 
                    AND il.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
                )
                LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (p.`id_manufacturer`= m.`id_manufacturer`)
                LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (
                    p.`id_tax_rules_group` = tr.`id_tax_rules_group`
                    AND tr.`id_country` = '.(int)Country::getDefaultCountryId().'
                    AND tr.`id_state` = 0
                )
                LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
			WHERE p.price > 0 
				AND `id_product_1` = '.(int)$this->id.'
				'.($active ? 'AND p.`active` = 1' : 'AND p.`active` = 1').'
		';

        if (!$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql)) {
            return false;
        }

        foreach ($result as &$row) {
            $row['id_product_attribute'] = Product::getDefaultAttribute((int)$row['id_product']);
        }

        return $this->getProductsProperties($id_lang, $result);
    }

    // Correggere se cambiano i bundle!
    public static function getAccessoryById($accessoryId)
	{
		$is_product = Db::getInstance()->getValue('
			SELECT `name` 
			FROM `'._DB_PREFIX_.'product_lang` 
			WHERE `id_product` = '.(int)($accessoryId)
		);

		if($is_product == '') {
			$is_product = Db::getInstance()->getValue('
				SELECT `bundle_name` 
				FROM `'._DB_PREFIX_.'bundle` 
				WHERE `id_bundle` = '.(int)($accessoryId)
			);
		
			if($is_product != '') {	
				return Db::getInstance()->getRow('
					SELECT `id_bundle` id_product, `bundle_name` name 
					FROM `'._DB_PREFIX_.'bundle` 
					WHERE `id_bundle` = '.(int)($accessoryId)
				);
			}
			else
				return '';
		}
		else {
			return Db::getInstance()->getRow('
				SELECT `id_product`, `name` 
				FROM `'._DB_PREFIX_.'product_lang` 
				WHERE `id_product` = '.(int)($accessoryId)
			);
		}
	}


    /* ALTERNATIVE */

    public static function getAlternativesLightAll($id_lang, $id_product)
	{
		return Db::getInstance()->ExecuteS('
			SELECT id_product_2 `id_product`, p.`reference`, pl.`name`
			FROM `'._DB_PREFIX_.'alternative`
			LEFT JOIN `'._DB_PREFIX_.'product` p ON (p.`id_product`= `id_product_2`)
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')
			WHERE id_product_2 != 0 
				AND `id_product_1` = '.(int)($id_product)
		);
	}

    // Correggere se cambiano i bundle!
    public static function getAlternativeById($alternativeId)
	{
		$is_product = Db::getInstance()->getValue('
			SELECT `name` 
			FROM `'._DB_PREFIX_.'product_lang` 
			WHERE `id_product` = '.(int)($alternativeId)
		);
		
		if($is_product == '') {
			$is_product = Db::getInstance()->getValue('
				SELECT `bundle_name` 
				FROM `'._DB_PREFIX_.'bundle` 
				WHERE `id_bundle` = '.(int)($alternativeId)
			);
		
			if($is_product != '') {
				return Db::getInstance()->getRow('
					SELECT `id_bundle` id_product, `bundle_name` name 
					FROM `'._DB_PREFIX_.'bundle` 
					WHERE `id_bundle` = '.(int)($alternativeId)
				);
			}
			else
				return '';
		}
		else {
			return Db::getInstance()->getRow('
				SELECT `id_product`, `name` 
				FROM `'._DB_PREFIX_.'product_lang` 
				WHERE `id_product` = '.(int)($accessoryId)
			);
		}
	}
	
	public function changeAlternatives($accessories_id)
	{
		foreach ($accessories_id as $id_product_2)
			Db::getInstance()->AutoExecute(_DB_PREFIX_.'alternative', array('id_product_1' => (int)($this->id), 'id_product_2' => (int)($id_product_2)), 'INSERT');
	}
	
	public function deleteAlternatives()
	{
		return Db::getInstance()->Execute('
			DELETE FROM `'._DB_PREFIX_.'alternative` 
		    WHERE `id_product_1` = '.(int)($this->id)
	    );
	}
	
	public static function duplicateAlternatives($id_product_old, $id_product_new)
	{
		$return = true;

		$result = Db::getInstance()->ExecuteS('
			SELECT *
			FROM `'._DB_PREFIX_.'alternative`
			WHERE `id_product_1` = '.(int)($id_product_old)
		);

		foreach ($result as $row) {
			$data = array(
				'id_product_1' => (int)($id_product_new),
				'id_product_2' => (int)($row['id_product_2']));
			$return &= Db::getInstance()->AutoExecute(_DB_PREFIX_.'alternative', $data, 'INSERT');
		}

		return $return;
	}

    
    /* CSV */

    // CORREGGERE: decommentare il percorso giusto del file dopo le prove
    // vedi nota di Federico
    public function RecuperaCSV($id_order)
	{
        $context = Context::getContext();

		$query = "
			SELECT DISTINCT
				c.id_customer AS identificatore_univoco_cliente,
				c.firstname AS nome_cliente,
				c.lastname AS cognome_cliente,
				c.is_company AS e_azienda,
				c.pec AS pec,
				c.codice_univoco AS codice_univoco,
				o.id_order AS codice_ordine,
				o.date_add AS data_ordine,
				o.payment AS pagamento,
				o.payment AS pagamento_e,
				o.total_commissions AS commissioni,
				o.total_paid AS importo,
				c.vat_number AS partita_iva_cliente,
				c.company AS ragione_sociale,
				c.tax_code AS codice_fiscale,
				address1.address1 AS indirizzo,
				address1.postcode AS cap,
				address1.city AS localita,
				state1.iso_code AS provincia,
				state1.name AS provincia_esteso,
				address1.phone AS telefono_1,
				address1.phone_mobile AS telefono_2,
				address1.fax AS fax,
				c.email AS indirizzo_email,
				address2.company AS spedizione_ragione_sociale,
				address2.firstname AS nome_spedizione,
				address2.lastname AS cognome_spedizione,
				address2.address1 AS spedizione_indirizzo,
				address2.postcode AS spedizione_cap,
				address2.city AS spedizione_citta,
				state2.iso_code AS spedizione_provincia,
				co.iso_code AS codice_nazione,
				country2.iso_code AS spedizione_nazione,
				o.date_add AS data_ordine,
				od.product_reference AS codice_articolo,
				od.product_supplier_reference AS codice_sku,
				od.product_id AS id_product,
				od.product_name AS descrizione_articolo,
				od.product_quantity AS quantita_ordinata,
				od.wholesale_price AS prezzo_acquisto,
				o.total_shipping AS spedizione,
				o.carrier_tax_rate AS iva_spedizione,
				o.id_carrier AS corriere,
				o.id_cart AS id_cart,
				c.id_default_group AS gruppo_cliente,
				od.product_price AS prezzo_unitario
			FROM "._DB_PREFIX_."order_detail od
                JOIN "._DB_PREFIX_."orders o ON (o.id_order = od.id_order)
                JOIN "._DB_PREFIX_."customer c ON (o.id_customer = c.id_customer)
                JOIN "._DB_PREFIX_."address AS address1 ON (o.id_address_invoice = address1.id_address)
                JOIN "._DB_PREFIX_."address AS address2 ON (o.id_address_delivery = address2.id_address)
                LEFT JOIN "._DB_PREFIX_."country co ON (address1.id_country = co.id_country)
                LEFT JOIN "._DB_PREFIX_."state AS state1 ON (address1.id_state = state1.id_state)
                LEFT JOIN "._DB_PREFIX_."state AS state2 ON (address2.id_state = state2.id_state)
                LEFT JOIN "._DB_PREFIX_."country AS country2 ON (address2.id_country = country2.id_country)
			WHERE o.id_order = ".$id_order."
		";
			
		$rows = Db::getInstance()->executeS($query);
		
		$riga_ordine = '';
		$riga_cli = '';
		$tipo_ordine = 1;
		
		/*
		tipo ordine 0: da confermare
		tipo ordine 1: confermato
		tipo ordine 2: prenotato
		*/
		
		foreach($rows as $row) 
        {
			$identificatore_univoco_cliente = $row["id_order"]; //1 
			$identificatore_univoco_cliente = $row["identificatore_univoco_cliente"]; //1 
			$codice_ordine = $row["codice_ordine"]; //2
			$partita_iva_cliente = $row["partita_iva_cliente"]; //3 
			$codice_fiscale = $row["codice_fiscale"]; //4 
			$ragione_sociale = $row["ragione_sociale"]; //5 
			$indirizzo = $row["indirizzo"]; //6 
			$cap = $row["cap"]; //7 
			$localita = $row["localita"]; //8 
			$provincia = $row["provincia"]; //9 
			$telefono_1 = $row["telefono_1"]; //10 
			$telefono_2 = ""; //11
			$fax = $row["fax"]; //12 
			$indirizzo_email= $row["indirizzo_email"]; //13 
			$codice_del_listino = ""; //14 
			$codice_pagamento = ""; //15 

			if($row["spedizione_ragione_sociale"] != '') {
				$spedizione_ragione_sociale = $row["spedizione_ragione_sociale"]; //16 
			}
			else {
				$spedizione_ragione_sociale = $row["nome_spedizione"]." ".$row["cognome_spedizione"]; //16 
			}

			$spedizione_indirizzo = $row["spedizione_indirizzo"]; //17 
			$spedizione_cap = $row["spedizione_cap"]; //18 
			$spedizione_provincia = $row["spedizione_provincia"]; //19
			$data_ordine = $row["data_ordine"]; //20 
			$numero_ordine = $row["codice_ordine"]; //21 
			$codice_articolo = $row["codice_articolo"]; //22 
			$codice_articolo_del_produttore = ""; //23
			$descrizione_articolo = $row["descrizione_articolo"]; //24
			$quantita_ordinata = $row["quantita_ordinata"]; //25
			
			$sconto_1 = ""; //27
			$sconto_2 = ""; //28
			$sconto_3 = ""; //29
			$codice_iva_vendite = ""; //30
			$codice_deposito = ""; //31 
			$codice_del_listino = ""; //32 
			$codice_aspetto_dei_beni = ""; //33 
			$codice_porto = ""; //34
			$codice_trasporto_a_mezzo = ""; //35
			$codice_vettore = ""; //36
			$riga_omaggio = ""; //37
			$pagamento = $row['pagamento'];
			$id_product = $row["id_product"];  
			$gruppo_cliente = $row["gruppo_cliente"]; 

			if(strtolower($ragione_sociale) == 'geometra' || strtolower($ragione_sociale) == 'geom' || strtolower($ragione_sociale) == 'avv' || strtolower($ragione_sociale) == 'avvocato' || strtolower($ragione_sociale) == 'dott' || strtolower($ragione_sociale) == 'dottore' || strtolower($ragione_sociale) == 'commercialista' || strtolower($ragione_sociale) == 'notaio' || strtolower($ragione_sociale) == 'studio medico' || strtolower($ragione_sociale) == 'studio legale' || strtolower($ragione_sociale) == 'studio dentistico' || strtolower($ragione_sociale) == 'studio notarile' || strtolower($ragione_sociale) == 'studio tecnico' || strtolower($ragione_sociale) == 'studio associato') {
				$ragione_sociale.= " ".$row['nome_cliente']." ".$row['cognome_cliente'];
			}

			if(strtolower($ragione_sociale) == 'geometra' || strtolower($ragione_sociale) == 'geom' || strtolower($ragione_sociale) == 'avv' || strtolower($ragione_sociale) == 'avvocato' || strtolower($ragione_sociale) == 'dott' || strtolower($ragione_sociale) == 'dottore' || strtolower($ragione_sociale) == 'commercialista' || strtolower($ragione_sociale) == 'notaio' || strtolower($ragione_sociale) == 'studio medico' || strtolower($ragione_sociale) == 'studio legale' || strtolower($ragione_sociale) == 'studio dentistico' || strtolower($ragione_sociale) == 'studio notarile' || strtolower($ragione_sociale) == 'studio tecnico' || strtolower($ragione_sociale) == 'studio associato') {
				$spedizione_ragione_sociale.= " ".$row['nome_spedizione']." ".$row['cognome_spedizione'];
			}

			$queryprice = ("SELECT product_price, reduction_percent, reduction_amount FROM "._DB_PREFIX_."order_detail WHERE product_id = ".$id_product." AND id_order = ".$id_order."");
			$rowprice = Db::getInstance()->getRow($queryprice);

			$numpri = mysqli_num_rows($queryprice);
			
			if($rowprice['reduction_amount'] > 0)
				$prezzo_unitario = ($rowprice['product_price']-$rowprice['reduction_amount']);
			else {	
				$sconto = ($rowprice['product_price']*$rowprice['reduction_percent'])/100;
				$prezzo_unitario = ($rowprice['product_price']-$sconto);
			}
			
			$prezzo_unitario = number_format($prezzo_unitario, 2, ',', '');
			$prezzo_unitario_e = $prezzo_unitario;
			$prezzo_unitario = "€".$prezzo_unitario;

			if($row['e_azienda'] == 1) {
				$ragsoc = $row['ragione_sociale'];
			}
			else {
				$ragsoc = $row['nome_cliente']." ".$row['cognome_cliente'];
			}

			if($row["spedizione_ragione_sociale"] != "") {
				if($row['e_azienda'] == 1) {
					if((strtolower($row['nome_cliente']." ".$row['cognome_cliente'])) != (strtolower($row['nome_spedizione']." ".$row['cognome_spedizione']))) {
						$ragsoc2 = $row['nome_spedizione']." ".$row['cognome_spedizione'];
					}
					else {
						$ragsoc2 = "";
					}
				} 
				else {
					$ragsoc2 = $row['nome_spedizione']." ".$row['cognome_spedizione'];
				}
			}

			/*
            $iva_spedizione = (int)($row['iva_spedizione']);
			$iva_spedizione = (string)($iva_spedizione);
			$iva_spedizione = "1.".$iva_spedizione;
			$iva_spedizione = (double)($iva_spedizione);
			*/

			$iva_spedizione = (int)($row['iva_spedizione']);
			$iva_spedizione = (100 + $iva_spedizione) / 100;
			$spedizione_citta = $row["spedizione_citta"]; //18 
			$spedizione = ($row['spedizione'] / $iva_spedizione);
			$spedizione = number_format($spedizione, 2, ',', '');
			
			$arraydata1 = explode(" ",$data_ordine);
			$arraydata2 = explode("-",$arraydata1[0]);

			$data_nuova = $arraydata2[1]."/".$arraydata2[2]."/".$arraydata2[0];
			$data_nuova_e = $arraydata2[2]."/".$arraydata2[1]."/".$arraydata2[0];
			// esolver
			
			$is_verifica = Db::getInstance()->getValue('SELECT id_order_history FROM '._DB_PREFIX_.'order_history WHERE id_order_state = 24 AND id_order = '.$id_order.' AND id_order NOT IN (SELECT id_order FROM '._DB_PREFIX_.'order_history WHERE id_order = '.$id_order.' AND id_order_state = 25)');
		
			if($is_verifica > 0 && $is_verifica != '')
				$stato_conferma = 0;
			
			$check_verifica = Db::getInstance()->getValue('SELECT id_order_state FROM '._DB_PREFIX_.'order_history WHERE id_order_history = (SELECT max( id_order_history ) FROM (SELECT * FROM '._DB_PREFIX_.'order_history WHERE id_order_state = 24 OR id_order_state =25) s WHERE s.id_order = '.$id_order.') ORDER BY date_add DESC');
			
			$stato_attuale = Db::getInstance()->getValue('SELECT id_order_state FROM '._DB_PREFIX_.'order_history WHERE id_order = '.$id_order.' ORDER BY date_add DESC');
			
			if($stato_attuale == 30)
				$magazzino = 30;
			else
				$magazzino = 1;
			
			$check_prime = Db::getInstance()->getValue('SELECT id_order_state FROM '._DB_PREFIX_.'order_history WHERE id_order_history = (SELECT max( id_order_history ) FROM (SELECT * FROM '._DB_PREFIX_.'order_history WHERE id_order_state = 30) s WHERE s.id_order = '.$id_order.') ORDER BY date_add DESC');
			
			if($check_prime > 0)
			{	
				$casella_prime = 'PRIME';
				$magazzino = 30;
			}	
			else
				$casella_prime = '';
			
			// sotto: != 24
			//if($check_verifica != 24)
			//{
				if($pagamento == "Pagamento alla consegna (COD)") {
					$pagamento_e = "X002"; 
					$stato_conferma = 2;
				}

				else if($pagamento == "Bonifico Bancario" || $pagamento == "Bonifico Anticipato" || $pagamento == "Bonifico anticipato" || $pagamento == "Bonifico bancario" || $pagamento == "Bonifico") {
					$pagamento_e = "BB369"; 
					$stato_conferma = 1;

					// CORREGGERE: aggiungere verificare che sia dopo stato 10? (era una nota di Federico)
					$is_conf = Db::getInstance()->getValue('SELECT id_order_history FROM '._DB_PREFIX_.'order_history WHERE id_order = '.$id_order.' AND id_order_state = 18');

					if($is_conf > 0)
						$stato_conferma = 2;
					
					if($stato_attuale == 10)
						$stato_conferma = 1;

				}

				else if($pagamento == "Bonifico 30 gg. fine mese") {
					$pagamento_e = "BB430"; 
					$stato_conferma = 2;
				}

				else if($pagamento == "Bonifico 60 gg. fine mese") {
					$pagamento_e = "BB431"; 
					$stato_conferma = 2;
				}
				
				else if($pagamento == "Bonifico 90 gg. fine mese") {
					$pagamento_e = "BB432"; 
					$stato_conferma = 2;
				}
				else if($pagamento == "Bonifico 30 gg. 15 mese successivo") {
					$pagamento_e = "BB463"; 
					$stato_conferma = 2;
				}
				
				else if($pagamento == "R.B. 30 GG. D.F. F.M.") {
					$pagamento_e = "RB230"; 
					$stato_conferma = 2;
				}
				
				else if($pagamento == "R.B. 60 GG. D.F. F.M.") {
					$pagamento_e = "RB231";
					$stato_conferma = 2;
				}
				
				else if($pagamento == "R.B. 90 GG. D.F. F.M.") {
					$pagamento_e = "RB232";
					$stato_conferma = 2;
				}
				
				else if($pagamento == "R.B. 30 GG. 5 mese successivo") {
					$pagamento_e = "RB253"; 
					$stato_conferma = 2;
				}
				else if($pagamento == "R.B. 30 GG. 10 mese successivo") {
					$pagamento_e = "RB250"; 
					$stato_conferma = 2;
				}
				else if($pagamento == "R.B. 60 GG. 5 mese successivo") {
					$pagamento_e = "RB258"; 
					$stato_conferma = 2;
				}
				else if($pagamento == "R.B. 60 GG. 10 mese successivo") {
					$pagamento_e = "RB249"; 
					$stato_conferma = 2;
				}
				
				else if($pagamento == "Contrassegno") {
					$pagamento_e = "X002";
					$stato_conferma = 2;
				}

				else if($pagamento == "Contrassegno" || $pagamento == "Contanti alla consegna (non si accettano assegni)") {
					$pagamento_e = "X002";
					$stato_conferma = 2;
				}
				
				else if($pagamento == "Carta Amazon") {
					$pagamento_e = "X011";
					$stato_conferma = 2;
				}
				
				else if($pagamento == "Carta ePrice") {
					$pagamento_e = "X012";
					$stato_conferma = 2;
				}
				
				else if($pagamento == "PayPal" || $pagamento == "Paypal") {
					$pagamento_e = "X013";
					$stato_conferma = 2;
				}
				
				else if($pagamento == "GestPay") {
					// gestpay
					$pagamento_e = "X007";
					$stato_conferma = 2;
				}
				else
				{
					$pagamento_e = "BB369";
					$stato_conferma = 2;
					
				}
			//}
			
			if($check_verifica == 24)
				$stato_conferma_ver = 0;
			else if($check_verifica == 25)
				$stato_conferma_ver = 1;
			else
				$stato_conferma_ver = 1;
			
			if($stato_conferma_ver == 0)
				$stato_conferma = 0;
			
			if($row['codice_articolo'] == 'CHEAP-300-O' || $row['codice_articolo'] == 'CHEAP-1500-O' || $prezzo_unitario == "€"."0,00") {
			    $prezzo_unitario = "€"."1,00";
			}
		
			if($codice_articolo == 'OMAGGIO') {
		
				$queryprice = ("SELECT product_name FROM "._DB_PREFIX_."order_detail WHERE product_id = $id_product AND id_order = ".$id_order."");
				$rowprice = Db::getInstance()->getRow($queryprice);
				
				$riga_nome_omaggio = $rowprice['product_name'];
				
				$elenco_prodotti = explode(":", $riga_nome_omaggio);
				
				$qtprod = explode(";", $elenco_prodotti[1]);
			
				
				foreach($qtprod as $prodotto) {
					if(!($prodotto == '')) {
						$qtnpr = explode("x ",$prodotto);
						$id_art = Db::getInstance()->getValue("
							SELECT id_product 
							FROM "._DB_PREFIX_."product_lang 
							WHERE id_lang = ".$context->language->id."
								AND name = '".$qtnpr[1]."'
						");

						$cd_art = Db::getInstance()->getValue("
							SELECT reference 
							FROM "._DB_PREFIX_."product 
							WHERE id_product = '".$id_art."'
						");
						
						$riga_ordine .= '"'.$row['identificatore_univoco_cliente'].'";"'.$row['partita_iva_cliente'].'";"'.$row['codice_fiscale'].'";"'.$row['identificatore_univoco_cliente'].'";"";"'.$row['codice_ordine'].'";"'.$data_nuova.'";"'.$pagamento.'";"";"'.$spedizione_ragione_sociale.'";"'.$ragsoc2.'";"'.$row['spedizione_indirizzo'].'";"'.$row['spedizione_cap'].'";"'.$row['spedizione_citta'].'";"'.$row['spedizione_provincia'].'";"";"";"";"";"";"";"";"";"";"";"";"'.$row['codice_ordine'].'";"'.$cd_art.'";"";"";"'.trim($qtnpr[0]).'";"";"";"";"";"";"";"";"'."€"."1,00".'";"";"";"";"";"";"";"";"";"0"';
						$riga_ordine .= "\n";
						
						/* esolver */
						$riga_ord_prod_e .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||4|'.$cd_art.'|'.$qtnpr[1].'|'.$qtnpr[0].'|0,00|||||'.$stato_conferma.'|'.$magazzino.'|||||EOR';
						$riga_ord_prod_e .= "\n";
						/* fine esolver */
					}
				}
			}		
			else {
				$riga_ordine .= '"'.$row['identificatore_univoco_cliente'].'";"'.$row['partita_iva_cliente'].'";"'.$row['codice_fiscale'].'";"'.$row['identificatore_univoco_cliente'].'";"";"'.$row['codice_ordine'].'";"'.$data_nuova.'";"'.$pagamento.'";"";"'.$spedizione_ragione_sociale.'";"'.$ragsoc2.'";"'.$row['spedizione_indirizzo'].'";"'.$row['spedizione_cap'].'";"'.$row['spedizione_citta'].'";"'.$row['spedizione_provincia'].'";"";"";"";"";"";"";"";"";"";"";"";"'.$row['codice_ordine'].'";"'.$row['codice_articolo'].'";"";"";"'.$row['quantita_ordinata'].'";"";"";"";"";"";"";"";"'.$prezzo_unitario.'";"";"";"";"";"";"";"";"";"0"';
				/*esolver */
				
				$category_product = Db::getInstance()->getValue('SELECT id_category_default FROM '._DB_PREFIX_.'product WHERE id_product = '.$row['id_product']);
				
                if(($category_product == 119 || $category_product == 248 || $category_product == 249 || $category_product == 250 || $category_product == 258 || $category_product == 259)) {
					$tipo_articolo = 85;
				}
				else {
					$tipo_ordine = 0;
					$tipo_articolo = 1;
				}
				
				/*if($stato_attuale == 30)
					$tipo_ordine = 3;
				*/
				
				if($row['codice_articolo'] == 'CHEAP-300-O' || $row['codice_articolo'] == 'CHEAP-600-O') {
					$riga_ord_prod_e .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||'.$tipo_articolo.'|'.$row['codice_articolo'].'|'.$row['descrizione_articolo'].'|'.$row['quantita_ordinata'].'|'.$prezzo_unitario_e.'|||||'.$stato_conferma.'|'.$magazzino.'|||||EOR';
				}
				else if($row['codice_articolo'] == 'TRASP') {
					$riga_ord_prod_e .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||40|TRASP|Trasporto|1|'.$prezzo_unitario_e.'|||||'.$stato_conferma.'|'.$magazzino.'|||||EOR';
				}
				else {
					$riga_ord_prod_e .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||'.$tipo_articolo.'|'.$row['codice_articolo'].'|'.$row['descrizione_articolo'].'|'.$row['quantita_ordinata'].'|'.$prezzo_unitario_e.'|||||'.$stato_conferma.'|'.$magazzino.'|||||EOR';
				}
					
				$riga_ord_prod_e .= "\n";
				/* fine esolver */
			}

			$riga_ordine .= "\n";

			$riga_cli .= '"'.$row['identificatore_univoco_cliente'].'";"'.$row['partita_iva_cliente'].'";"'.$row['codice_fiscale'].'";"";"";"";"'.$ragione_sociale.'";"";"'.$row['indirizzo'].'";"'.$row['cap'].'";"'.$row['localita'].'";"'.$row['provincia'].'";"";"'.$row['telefono_1'].'";"'.$row['telefono_2'].'";"'.$row['fax'].'";"'.$row['indirizzo_email'].'";"";"'.$row['codice_nazione'].'";"'.$row['cognome_cliente'].'";"'.$row['nome_cliente'].'";"";"";"";"";"";"";"";"";"'.$pagamento.'";"";"";"";"";"";"";"";"";"";"";"";"";"";"'.$ragione_sociale.'";"";"'.$row['spedizione_indirizzo'].'";"'.$row['spedizione_cap'].'";"'.$row['spedizione_citta'].'";"'.$row['spedizione_provincia'].'";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"1"';
			
			$riga_cli .= "\n";
			
			$riga_cli .= '"'.$row['identificatore_univoco_cliente'].'";"'.$row['partita_iva_cliente'].'";"'.$row['codice_fiscale'].'";"";"";"";"'.$spedizione_ragione_sociale.'";"";"'.$row['spedizione_indirizzo'].'";"'.$row['spedizione_cap'].'";"";"'.$row['spedizione_provincia'].'";"";"";"";"";"'.$row['indirizzo_email'].'";"";"'.$row['codice_nazione'].'";"";"";"";"";"";"";"";"";"";"";"'.$pagamento.'";"";"";"";"";"";"";"";"";"";"";"";"";"";"'.$spedizione_ragione_sociale.'";"";"'.$row['spedizione_indirizzo'].'";"'.$row['spedizione_cap'].'";"'.$row['spedizione_citta'].'";"'.$row['spedizione_provincia'].'";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"1"';

			$riga_cli .= "\n";
			
			/* esolver */
			
			if($row['e_azienda'] == 1) {
				
			}
			else {
				$ragione_sociale = $row['nome_cliente']." ".$row['cognome_cliente'];
			}

			if(strpos(strtolower($ragione_sociale), 'srl') !== false || strpos(strtolower($ragione_sociale), 's.r.l') !== false || strpos(strtolower($ragione_sociale), 's.p.a') !== false || strpos(strtolower($ragione_sociale), ' spa') !== false || strpos(strtolower($ragione_sociale), ' spa ') !== false)
				$tipo_soggetto = 1;
			else if(strpos(strtolower($ragione_sociale), ' sas') !== false || strpos(strtolower($ragione_sociale), ' sas ') !== false || strpos(strtolower($ragione_sociale), 's.a.s') !== false || strpos(strtolower($ragione_sociale), 's.n.c') !== false || strpos(strtolower($ragione_sociale), 'snc') !== false)
				$tipo_soggetto = 2;
			else if($row['partita_iva_cliente'] != $row['codice_fiscale'])
				$tipo_soggetto = 3;
			else
				$tipo_soggetto = 4;
			
			
			$gr_d = Db::getInstance()->getValue('
				SELECT id_default_group 
				FROM '._DB_PREFIX_.'customer 
				WHERE id_customer = '.$identificatore_univoco_cliente
			);
			
			if($gr_d == 16)
				$tipo_soggetto = 4;
			
			$ragione_sociale_e = explode(';;',wordwrap($ragione_sociale, 30, ";;", true));
			
			$ragione_sociale_prima_parte = $ragione_sociale_e[0];
			$ragione_sociale_seconda_parte = $ragione_sociale_e[1];
			
			
			$spedizione_ragione_sociale_e = explode(';;',wordwrap($spedizione_ragione_sociale, 30, ";;", true));
			
			$spedizione_ragione_sociale_prima_parte = $spedizione_ragione_sociale_e[0];
			$spedizione_ragione_sociale_seconda_parte = $spedizione_ragione_sociale_e[1];
			
			$indirizzo_e = explode(';;',wordwrap($row['indirizzo'], 35, ";;", true));
			
			$indirizzo_prima_parte = $indirizzo_e[0];
			$indirizzo_seconda_parte = $indirizzo_e[1];
			
			if($row['codice_nazione'] == 'IT')
				$codice_iva_agevolata = '411';
			else
				$codice_iva_agevolata = '412';
			
			if($tipo_soggetto == 3 && $codice_iva_agevolata == '411')
				$stato_nascita = 'IT';
			else
				$stato_nascita = '';
			
			$size_ordini = Db::getInstance()->executeS('
				SELECT id_customer 
				FROM '._DB_PREFIX_.'orders 
				WHERE id_customer = '.$row['identificatore_univoco_cliente']
			);

			if(sizeof($size_ordini) == 1)
				$nuovo = '*';
			
			$pagamento_cli = Db::getInstance()->getValue('
				SELECT pagamento 
				FROM customer_amministrazione 
				WHERE id_customer = '.$identificatore_univoco_cliente
			);		
			
			if($pagamento_cli == '' || !isset($pagamento_cli))
				$pagamento_cli = $pagamento_e;
			
			// esolver
			if($pagamento_cli == "Pagamento alla consegna (COD)") {
				$pagamento_cli_e = "X002"; 
			}

			else if($pagamento_cli == "Bonifico Bancario" || $pagamento_cli == "Bonifico bancario" || $pagamento_cli == "Bonifico") {
				$pagamento_cli_e = "BB369"; 
			}
			else if($pagamento_cli == "Bonifico 30 gg. fine mese") {
				$pagamento_cli_e = "BB430"; 
			}

			else if($pagamento_cli == "Bonifico 60 gg. fine mese") {
				$pagamento_cli_e = "BB431"; 
			}
			
			else if($pagamento_cli == "Bonifico 90 gg. fine mese") {
				$pagamento_cli_e = "BB432"; 
			}
			else if($pagamento_cli == "Bonifico 30 gg. 15 mese successivo") {
				$pagamento_cli_e = "BB463"; 
			}
			else if($pagamento_cli == "R.B. 30 GG. D.F. F.M.") {
				$pagamento_cli_e = "RB230"; 
			}
			
			else if($pagamento_cli == "R.B. 60 GG. D.F. F.M.") {
				$pagamento_cli_e = "RB231"; 
			}
			
			else if($pagamento_cli == "R.B. 90 GG. D.F. F.M.") {
				$pagamento_cli_e = "RB232"; 
			}
			
			else if($pagamento_cli == "R.B. 30 GG. 5 mese successivo") {
				$pagamento_cli_e = "RB253"; 
			}
			else if($pagamento_cli == "R.B. 30 GG. 10 mese successivo") {
				$pagamento_cli_e = "RB250"; 
			}
			else if($pagamento_cli == "R.B. 60 GG. 5 mese successivo") {
				$pagamento_cli_e = "RB258"; 
			}
			else if($pagamento_cli == "R.B. 60 GG. 10 mese successivo") {
				$pagamento_cli_e = "RB249"; 
			}
			
			else if($pagamento_cli == "Contrassegno") {
				$pagamento_cli_e = "X002"; 
			}

			else if($pagamento_cli == "Contrassegno") {
				$pagamento_cli_e = "X002"; 
			}
			
			else if($pagamento_cli == "Carta Amazon") {
				$pagamento_cli_e = "X011"; 
			}
			
			else if($pagamento_cli == "Carta ePrice") {
				$pagamento_cli_e = "X012"; 
			}
			
			else if($pagamento_cli == "PayPal") {
				$pagamento_cli_e = "X013"; 
			}
			
			else if($pagamento_cli == "GestPay") {
				// gestpay
				$pagamento_cli_e = "X007"; 
			}
			else
			{
				$pagamento_cli_e = "BB369";
			}
			// fine esolver
		
			// $riga_cli_e = 'GEN|'.$row['identificatore_univoco_cliente'].'|'.$row['partita_iva_cliente'].'|'.$row['codice_fiscale'].'|'.($row['partita_iva_cliente'] != '' ? 0 : 1).'||'.($row['gruppo_cliente'] == 5 ? '1' : '0').'||'.($row['gruppo_cliente'] == 5 ? '4' : $tipo_soggetto).'|'.$ragione_sociale_prima_parte.'|'.$ragione_sociale_seconda_parte.'|'.$indirizzo_prima_parte.'|'.$indirizzo_seconda_parte.'|'.$row['cap'].'|'.$row['localita'].'||'.$row['provincia'].'|'.$row['codice_nazione'].'|'.$row['telefono_1'].'|'.$row['telefono_2'].'|'.$row['fax'].'|'.$row['indirizzo_email'].'|'.$row['nome_cliente'].' '.$row['cognome_cliente'].'|'.$row['cognome_cliente'].'|'.$row['nome_cliente'].'|'.'|'.'|'.'|'.'|'.$stato_nascita.'|'.$codice_iva_agevolata.'|'.$row['codice_nazione'].$row['provincia'].'|'.$row['codice_nazione'].'-'.$row['provincia_esteso'].'|||'.$nuovo.'|'.$pagamento_cli_e.'|'."\n";
			
			// 1.1.2019 campo 7: pa = 1 , codice univoco = 2, estero = 0, 3 pec, 4 altro canale
			// campo 8: pa (1) = codice univoco ufficio, altri (2) = cod. destinatario sdi
			$fte = 2;
			
			if($row['gruppo_cliente'] == 5)
				$fte = 1;
			else {
				if($row['codice_univoco'] != '')
					$fte = 2;
				else if($row['pec'] != '')
					$fte = 2;
			}
			
			if($tipo_ordine == 0)
				$estero_fte = 711;
			
			if($row['codice_nazione'] != 'IT') {
				$fte = 0;
				
				if($tipo_ordine == 0)
					$estero_fte = 717;
			}
			
			if($tipo_ordine == 1)
				$estero_fte = 710;
			
			$zero_o_uno = 0;
			
			if($row['partita_iva_cliente'] != '')
				$zero_o_uno = 0;
			else
				$zero_o_uno = 1;
			
			if($row['gruppo_cliente'] == 5) {
				$zero_o_uno = 0;
				$tipo_soggetto = 4;
			}

			$row['cognome_cliente'] = '';
			$row['nome_cliente'] = '';
			
			$riga_cli_e = 'GEN|'.$row['identificatore_univoco_cliente'].'|'.$row['partita_iva_cliente'].'|'.$row['codice_fiscale'].'|'.$zero_o_uno.'||'.$fte.'|'.($row['codice_univoco'] != '' ? $row['codice_univoco'] : '').'|'.$tipo_soggetto.'|'.$ragione_sociale_prima_parte.'|'.$ragione_sociale_seconda_parte.'|'.$indirizzo_prima_parte.'|'.$indirizzo_seconda_parte.'|'.$row['cap'].'|'.$row['localita'].'||'.$row['provincia'].'|'.$row['codice_nazione'].'|'.$row['telefono_1'].'|'.$row['telefono_2'].'|'.$row['fax'].'|'.$row['indirizzo_email'].'|'.$row['nome_cliente'].' '.$row['cognome_cliente'].'|'.$row['cognome_cliente'].'|'.$row['nome_cliente'].'|'.'|'.'|'.'|'.'|'.$stato_nascita.'|'.$codice_iva_agevolata.'|'.$row['codice_nazione'].$row['provincia'].'|'.$row['codice_nazione'].'-'.$row['provincia_esteso'].'|||'.$nuovo.'|'.$pagamento_cli_e.'||'.$row['pec'].''."\n";
				
			if($row['spedizione_nazione'] == "IT")
				$codice_esterno_fornitore = 33153; // GLS
			else
				$codice_esterno_fornitore = 33153; // GLS
				
			// $codice_esterno_fornitore = 35432; // TNT
					
			$corriere_nome = Db::getInstance()->getValue('
				SELECT name 
				FROM '._DB_PREFIX_.'carrier 
				WHERE id_carrier = '.$row['corriere']
			);
			
			if(strpos($corriere_nome, 'BRT') !== false)
				$codice_esterno_fornitore = 39140;
			
			if(strpos($corriere_nome, 'Ritiro') !== false)
				$codice_trasporto_a_mezzo = '2';
			else
				$codice_trasporto_a_mezzo = '3';
			
			if($codice_trasporto_a_mezzo == '2')
				$codice_esterno_fornitore = '';
			
			$nuovo = '';
			
			$cart_subject = Db::getInstance()->getValue('
				SELECT riferimento 
				FROM '._DB_PREFIX_.'cart 
				WHERE id_cart = '.$row['id_cart']
			);

			$cart_subject_2 = Db::getInstance()->getValue('
				SELECT name 
				FROM '._DB_PREFIX_.'cart 
				WHERE id_cart = '.$row['id_cart']
			);

			if(substr($cart_subject,0,3) == 'BDL') {
				$bdl_dati = Db::getInstance()->getRow('
					SELECT * 
					FROM '._DB_PREFIX_.'bdl 
					WHERE id_bdl = '.str_replace('BDL','',$cart_subject)
				);

				$bdl_ticket = Customer::trovaSigla(substr($bdl_dati['riferimento'],1), substr($bdl_dati['riferimento'],0,1));
				$data_ticket_r = Db::getInstance()->getValue('
					SELECT date_add 
					FROM '._DB_PREFIX_.'customer_thread 
					WHERE id_customer_thread = '.substr($bdl_dati['riferimento'],1)
				);
				
				if($data_ticket_r == '')
					$data_ticket = $data_nuova_e;
				else {
					$data_ticket_r = explode(' ',$data_ticket_r);
					$data_ticket_r = explode('-',$data_ticket_r[0]);
					$data_ticket = $data_ticket_r[2].'/'.$data_ticket_r[1].'/'.$data_ticket_r[0];
				}	
				
				$riga_ord_bdl = '';
				
				if($bdl_dati['descrizione'] != '')
					$riga_ord_bdl .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||20|||||||||||||Descrizione: '.(substr($bdl_dati['descrizione'],0,5) == 'Altro' ? substr($bdl_dati['descrizione'],5,strlen($bdl_dati['descrizione'])) : $bdl_dati['descrizione']).'||EOR'."\n";
				
				if($bdl_dati['motivo_chiamata'] != '')
					$riga_ord_bdl .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||20|||||||||||||Motivo chiamata: '.$bdl_dati['motivo_chiamata'].'||EOR'."\n";
				
				if($bdl_dati['guasto'] != '' && $bdl_dati['guasto'] != 'Nessuna')
					$riga_ord_bdl .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||20|||||||||||||Guasto: '.(substr($bdl_dati['guasto'],0,5) == 'Altro' ? substr($bdl_dati['guasto'],5,strlen($bdl_dati['guasto'])) : $bdl_dati['guasto']).'||EOR'."\n";
				
				if($bdl_dati['causa_guasto'] != '' && $bdl_dati['causa_guasto'] != 'Nessuno')
					$riga_ord_bdl .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||20|||||||||||||Causa guasto: '.(substr($bdl_dati['causa_guasto'],0,5) == 'Altro' ? substr($bdl_dati['causa_guasto'],5,strlen($bdl_dati['causa_guasto'])) : $bdl_dati['causa_guasto']).'||EOR'."\n";

			}
			else if(substr($cart_subject_2,0,9) == 'Contratto') {
				$contratto_dati = Db::getInstance()->getRow('
					SELECT * 
					FROM '._DB_PREFIX_.'contratto_assistenza 
					WHERE id_contratto = '.str_replace('Contratto N. ','',$cart_subject_2)
				);
				
				$competenza = 'Competenza: dal '.date('d/m/Y',strtotime($contratto_dati['data_inizio'])).' al '.date('d/m/Y',strtotime($contratto_dati['data_fine'])).'';
				
				$riga_ord_bdl = '';
				
				$riga_ord_bdl .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||20|||||||||||||'.$cart_subject_2.'||EOR'."\n";
				
				$riga_ord_bdl .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||20|||||||||||||'.$competenza.'||EOR'."\n";	
			}
			else {
				$riga_ord_bdl = '';
				$bdl_ticket = '';
				$data_ticket = '';
			}
			
			/*
            if($row['spedizione_nazione'] != 'IT')
				$tipo_ordine = 4;
			*/
			
			// ultimo campo: se estero 717, sennò non mandarlo
			
			$riga_ord_e = 'TES|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'||||'.$pagamento_e.'|'.number_format($row['importo'],2,",","").'|'.$tipo_ordine.'||'.$casella_prime.'|||'.$spedizione_ragione_sociale_prima_parte.'|'.$spedizione_ragione_sociale_seconda_parte.'|'.$row['spedizione_indirizzo'].'|'.$row['spedizione_cap'].'|'.$row['spedizione_citta'].'||'.$row['spedizione_provincia'].'|'.$row['spedizione_nazione'].'||1|VENDITA|1|FRANCO ASSEGNATO|1|SCATOLA|'.$codice_trasporto_a_mezzo.'|'.($codice_trasporto_a_mezzo == 1 ? 'VETTORE' : 'PROPRIO').'|'.$codice_esterno_fornitore.'|'.$trasporto.'|||||||||||||'.$stato_conferma.'|'.$magazzino.'|'.$data_ticket.'|'.$bdl_ticket.'||'.$estero_fte.'|EOR'."\n";
			
			$riga_ord_e .= $riga_ord_prod_e;
			
			//COMMDIL
			//COMMISSIONI PER PAGAMENTO DILAZIONATO 
			
			if($row['commissioni'] > 0) {	
				if($pagamento == "GestPay")
					$riga_ord_e .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||40|COMMIS|COMMISSIONE PAGAMENTO CARTA|1|'.number_format($row['commissioni'],2,",","").'|||||'.$stato_conferma.'|'.$magazzino.'|||||EOR'."\n";
				else
					$riga_ord_e .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||40|COMMDIL|COMMISSIONI PER PAGAMENTO DILAZIONATO|1|'.number_format($row['commissioni'],2,",","").'|||||'.$stato_conferma.'|'.$magazzino.'|||||EOR'."\n";
			}

			if($spedizione > 0) {	
				$riga_ord_e .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||40|TRASP|Trasporto|1|'.$spedizione.'|||||'.$stato_conferma.'|'.$magazzino.'|||||EOR';
			
				$riga_ord_e .= "\n";
			}

			$riga_ord_e .= $riga_ord_bdl;
			/* fine esolver */
		}
			
		$riga_ordine .= '"'.$identificatore_univoco_cliente.'";"'.$partita_iva_cliente.'";"'.$codice_fiscale.'";"'.$identificatore_univoco_cliente.'";"";"'.$codice_ordine.'";"'.$data_nuova.'";"'.$pagamento.'";"";"'.$spedizione_ragione_sociale.'";"";"'.$spedizione_indirizzo.'";"'.$spedizione_cap.'";"'.$spedizione_citta.'";"'.$spedizione_provincia.'";"";"";"";"";"";"";"";"";"";"";"";"'.$codice_ordine.'";"TRASP";"";"";"1";"";"";"";"";"";"";"";"'.$spedizione.'";"";"";"";"";"";"";"";"";"0"';
		
		$ccod = $codice_ordine;
		
		$file_csv = "ORD.$ccod.csv";
		$file_csv2 = "CLI.$ccod.csv";

		// Correggere: decommentare dopo test
		/*$log_recuperi = fopen("/var/www/vhosts/ezdirect.it/httpdocs/import/log-recuperi-csv.txt","a+");
		$riga_log = $context->employee->id." - ".$ccod." - ".date("Y-m-d H:i:s")."\n";
		fwrite($log_recuperi, $riga_log);*/

		/* esolver */
		
		$file_csv_e = "ORD.$ccod.csv";
		$file_csv2_e = "CLI.$ccod.csv";
				
		$file_csv3_e_prov = "ORD".$ccod."-PROV.csv";
		$file_csv4_e_prov = "CLI".$ccod."-PROV.csv";

		$file_csv3_e = "ORD.".$ccod.".csv";
		$file_csv4_e = "CLI.".$ccod.".csv";	
		
		/* // Correggere: DECOMMENTARE dopo test
		if(!is_dir("/var/www/vhosts/ezdirect.it/httpdocs/csv-ordini-nuovi-esolver")) {
		    mkdir("/var/www/vhosts/ezdirect.it/httpdocs/csv-ordini-nuovi-esolver");
		}*/

		// TEST! Correggere
		$percorso_esolver = _PS_ROOT_DIR_.'ezadmin/test-files/';
		$percorso_esolvertest = _PS_ROOT_DIR_.'ezadmin/test-files-2/';

		//$percorso_esolver = '/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/';
		//$percorso_esolvertest = '/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolvertest';

		$file_csv3_e_prov_stream = fopen($percorso_esolver.$file_csv3_e_prov,"a+");
		fwrite($file_csv3_e_prov_stream, $riga_ord_e);
		fclose($percorso_esolver.$file_csv3_e_prov);
		
		$file_csv4_e_prov_stream = fopen($percorso_esolver.$file_csv4_e_prov,"a+");
		fwrite($file_csv4_e_prov_stream, $riga_cli_e);
		fclose($percorso_esolver.$file_csv4_e_prov);
		
        // CORREGGERE: TEST DA ELIMINARE?
		$file_csv3_e_prov_stream = fopen($percorso_esolvertest.$file_csv3_e_prov,"a+");
		fwrite($file_csv3_e_prov_stream, $riga_ord_e);
		fclose($percorso_esolvertest.$file_csv3_e_prov);
		
        // CORREGGERE: TEST DA ELIMINARE?
		$file_csv4_e_prov_stream = fopen($percorso_esolvertest.$file_csv4_e_prov,"a+");
		fwrite($file_csv4_e_prov_stream, $riga_cli_e);
		fclose($percorso_esolvertest.$file_csv4_e_prov);
		
		$data = file_get_contents($percorso_esolver.$file_csv3_e_prov);
		$data = mb_convert_encoding($data, 'ISO-8859-1');
		file_put_contents($percorso_esolver.$file_csv3_e, $data);
		
		$data_2 = file_get_contents($percorso_esolver.$file_csv4_e_prov);
		$data_2 = mb_convert_encoding($data_2, 'ISO-8859-1');
		file_put_contents($percorso_esolver.$file_csv4_e, $data_2);
		
		unlink($percorso_esolver.$file_csv3_e_prov);
		unlink($percorso_esolver.$file_csv4_e_prov);
		
		unlink($percorso_esolver."ORD..csv");
		unlink($percorso_esolver."CLI..csv");
		
		Db::getInstance()->execute("UPDATE "._DB_PREFIX_."orders SET csv = 1 WHERE id_order = ".$ccod."");
		
		Product::anagraficheArticoliEsolver($ccod);

		/* fine esolver */
	}

    // Usata nella funzione RecuperaCSV e in AdminProducts (submit accoda_esolver)
    // CORREGGERE: decommentare il percorso giusto del file dopo le prove
    public function anagraficheArticoliEsolver($id_order = 0)
	{
        $context = Context::getContext();

		// Correggere dopo test
		$percorso_file = _PS_ROOT_DIR_.'ezadmin/test-files/';
		// $percorso_file = _PS_DOCS_DIR_.'disponibilita/esolver/';
		// $percorso_file = '/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/';

		$file_csv_prov = "EZ-ARTICOLI-PROV.csv";
		$file_csv = "EZ-ARTICOLI.csv";
		$file_csv_listino = "EZ-LISTINI.csv";
		$file_csv_vendita = "EZ-VENDITA.csv";
		$string_products = '';
		
		if($id_order == 0)
			$string_products = '';
		else {
			$string_products .= 'AND ((';
			$products = Db::getInstance()->executeS('
				SELECT product_id 
				FROM '._DB_PREFIX_.'order_detail 
				WHERE id_order = '.$id_order
			);

			foreach($products as $product) {
				$string_products .= 'p.id_product = '.$product['product_id'].' OR ';
			}		

			$string_products .= 'p.id_product = 9999999999999) OR (p.active = 1 AND p.date_add > DATE_ADD(NOW(), INTERVAL -5 DAY)))';
		}	
		
		if(!filemtime($percorso_file.$file_csv_listino))
			$data_ultima_modifica = date('Y-m-d 00:00:00');
		else
			$data_ultima_modifica = date('Y-m-d H:i:s', filemtime($percorso_file.$file_csv_listino));

		$prima_riga = "Tipo record|Tipo anagrafica|Modello|Codice articolo|Codice esterno|Cod. Produttore|Cod. EAN|Cod. EAN descrizione|Cod. EAN quantità|Cod. EAN tipo|Descrizione|Descrizione estesa|Stato|Usato|Codice marca|Descrizione marca|Codice famiglia|Descrizione famiglia|Peso KG|CAcquisti Est.|CVendite Est.|Cod. Fornitore Esterno Abituale|Codice fornitore esterno|Codifica dell'articolo c/o fornitore|Tempo di approvvigionamento|Codice a barre c/o fornitore|Descr. Articolo|Note 1|Note 1|EOR\n";
		
		$prima_riga = "";
		
		$righe = '';
		$righe_listino = '';
		$righe_vendita = '';
		
		if($id_order == 0) {
			$prodotti = Db::getInstance()->executeS('
				SELECT p.*, pe.wholesale_price as pa_esolver, pl.name as nome_prodotto, m.name as costruttore, cl.name as categoria, s.name as fornitore 
				FROM '._DB_PREFIX_.'product p 
                    LEFT JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product = pl.id_product 
                    LEFT JOIN product_esolver pe ON p.id_product = pe.id_product 
                    LEFT JOIN '._DB_PREFIX_.'manufacturer m ON p.id_manufacturer = m.id_manufacturer 
                    LEFT JOIN '._DB_PREFIX_.'supplier s ON p.id_supplier = s.id_supplier 
                    LEFT JOIN '._DB_PREFIX_.'category_lang cl ON p.id_category_default = cl.id_category
				WHERE ((p.active >= 0 AND p.date_upd > "'.date('Y-m-d', strtotime('-5 days', strtotime(date('Y-m-d')))).'")
					OR p.id_product IN (
						SELECT id_product 
						FROM storico_prezzi 
						WHERE date > "'.date('Y-m-d', strtotime('-5 days', strtotime(date('Y-m-d')))).'"
					)
				) 
					AND pl.id_lang = '.$context->language->id.' 
				GROUP BY p.id_product
			');
		}
		else {
			$prodotti = Db::getInstance()->executeS('
				SELECT p.*, pe.wholesale_price as pa_esolver, pl.name as nome_prodotto, m.name as costruttore, cl.name as categoria, s.name as fornitore 
				FROM '._DB_PREFIX_.'product p 
                    LEFT JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product = pl.id_product 
                    LEFT JOIN product_esolver pe ON p.id_product = pe.id_product 
                    LEFT JOIN '._DB_PREFIX_.'manufacturer m ON p.id_manufacturer = m.id_manufacturer 
                    LEFT JOIN '._DB_PREFIX_.'supplier s ON p.id_supplier = s.id_supplier 
                    LEFT JOIN '._DB_PREFIX_.'category_lang cl ON p.id_category_gst = cl.id_category 
				WHERE p.reference != "" 
					AND pl.id_lang = '.$context->language->id.' 
                    '.$string_products.' 
				GROUP BY p.id_product
			');
		}

		foreach($prodotti as $p) {
			$virtual = ProductDownload::getIdFromIdProduct($p['id_product']);

			if(($p['id_category_default'] == 119 || $p['id_category_default'] == 248 || $p['id_category_default'] == 249 || $p['id_category_default'] == 250)) {	
				$tipo_anagrafica = 4;
				$modello = 4;
			}
			else {	
				$tipo_anagrafica = 1;
				$modello = 1;
			}
			
			if($p['id_supplier'] == 11)
				$fornitore_abituale = $p['cod_fornitore_1'];
			else
				$fornitore_abituale = $p['cod_fornitore_2'];
			
			if($p['upc'] != '' && ($p['id_supplier'] != 11 && $p['id_supplier'] != 8))
				$fornitore_abituale = $p['upc'];
			
			$data_inserimento = date('dmY', strtotime($p['date_add']));
			$data_aggiornamento = date('dmY', strtotime($p['date_upd']));
			
			$codice_marca = substr(strtoupper($p['costruttore']),0,3).sprintf("%'.03d", $p['id_manufacturer']);
			$codice_categoria = substr(strtoupper($p['categoria']),0,3).sprintf("%'.03d", $p['id_category_gst']);
			$codice_fornitore = substr(strtoupper($p['fornitore']),0,2).sprintf("%'.03d", $p['id_supplier']);
			
			if($p['id_category_gst'] == 0 || $p['id_category_gst'] == '') {	
				$codice_categoria = '';
				$p['categoria'] = '';
			}
			$fornitore = 0;
			
			switch($p['id_supplier'])
			{
				case 67: $fornitore = 36501; break; 
				case 3: $fornitore = 29645; break; 
				case 4: $fornitore = 36383; break; 
				case 8: $fornitore = 35420; break; 
				case 10: $fornitore = 36462; break; 
				case 11: $fornitore = 36448; break; 
				case 12: $fornitore = 39403; break; 
				case 13: $fornitore = 36561; break; 
				case 14: $fornitore = 36559; break; 
				case 31: $fornitore = 42585; break; 
				case 32: $fornitore = 38692; break; 
				case 33: $fornitore = 41822; break; 
				case 34: $fornitore = 36847; break; 
				case 35: $fornitore = 45936; break; 
				case 36: $fornitore = 39731; break; 
				case 37: $fornitore = 35184; break; 
				case 38: $fornitore = 36541; break; 
				case 39: $fornitore = 50971; break; 
				case 40: $fornitore = 34696; break; 
				case 41: $fornitore = 33790; break; 
				case 42: $fornitore = 35732; break; 
				case 43: $fornitore = 29350; break; 
				case 44: $fornitore = 37916; break; 
				case 45: $fornitore = 55321; break; 
				case 47: $fornitore = 36373; break; 
				case 48: $fornitore = 35401; break; 
				case 49: $fornitore = 36471; break; 
				case 50: $fornitore = 30647; break; 
				case 52: $fornitore = 34316; break; 
				case 53: $fornitore = 38045; break; 
				case 54: $fornitore = 35521; break; 
				case 55: $fornitore = 36459; break; 
				case 56: $fornitore = 45255; break; 
				case 57: $fornitore = 52060; break; 
				case 58: $fornitore = 42121; break; 
				case 60: $fornitore = 55322; break; 
				case 61: $fornitore = 36307; break; 
				case 66: $fornitore = 55323; break; 
				case 68: $fornitore = 42583; break; 
				case 70: $fornitore = 53695; break; 
				case 76: $fornitore = 58350; break; 
				case 1458: $fornitore = 46215; break; 
				default: $fornitore = ''; break;
			}
	
			$codice_fornitore = 'A.451'.$codice_fornitore;
	
			if($p['pa_esolver'] > 0)
				$acquisto = $p['pa_esolver'];
			else
				$acquisto = $p['wholesale_price'];
			
			$serie = Db::getInstance()->getValue('
				SELECT value 
				FROM '._DB_PREFIX_.'product p 
                    JOIN '._DB_PREFIX_.'feature_product fp ON p.id_product = fp.id_product 
                    JOIN '._DB_PREFIX_.'feature_value_lang fvl ON fp.id_feature_value = fvl.id_feature_value 
				WHERE fvl.id_lang = '.$context->language->id.'
					AND fp.id_feature = 917 
					AND p.id_product = '.$p['id_product']
			);
			
			$tipo = Db::getInstance()->getValue("
				SELECT value 
				FROM "._DB_PREFIX_."product p 
                    JOIN "._DB_PREFIX_."feature_product fp ON p.id_product = fp.id_product 
                    JOIN "._DB_PREFIX_."feature_value_lang fvl ON fp.id_feature_value = fvl.id_feature_value 
                    JOIN "._DB_PREFIX_."feature_lang fl ON fp.id_feature = fl.ID_FEATURE 
				WHERE fvl.id_lang = ".$context->language->id." 
					AND fl.id_lang = ".$context->language->id." 
					AND (fl.name = 'Tipo' OR fl.name = 'Tipo radioguide' OR fl.name = 'Tipo (domotica)' OR fl.name = 'Tipo (videoconferenza)')
					AND  p.id_product = ".$p['id_product']
			);
			//if(($p['id_category_default'] == 119 || $p['id_category_default'] == 248 || $p['id_category_default'] == 249))
			//{	

			$current_wholesale = Db::getInstance()->getRow("
				SELECT * 
				FROM "._DB_PREFIX_."specific_price_wholesale sp 
				WHERE sp.to > '".date('Y-m-d H:i:s')."' 
					AND sp.id_product = '".$p['id_product']."'
			");
			
			if($current_wholesale['wholesale_price'] > 0) {
				$p['sconto_acquisto_1'] = $current_wholesale['reduction_1'];
				$p['sconto_acquisto_2'] = $current_wholesale['reduction_2'];
				$p['sconto_acquisto_3'] = $current_wholesale['reduction_3'];
			}
			
			$righe .= "GEN|".$tipo_anagrafica."|".$modello."|".strtoupper(str_replace(array("\n\r", "\n", "\r", "\r\n"), "", $p['reference']))."|".$p['id_product']."|".strtoupper(str_replace(array("\n\r", "\n", "\r", "\r\n"), "", $p['supplier_reference']))."|".$p['ean13']."||||".substr(str_replace(array("\n\r", "\n", "\r", "\r\n"), "", $p['nome_prodotto']),0,50)."|".str_replace(array("\n\r", "\n", "\r", "\r\n"), "",$p['nome_prodotto'])."|".($p['fuori_produzione'] == 1 ? "2" : "0")."|".($p['condition'] == "new" ? "0" : "1")."|".$codice_marca."|".$p['costruttore']."|".$codice_categoria."|".$p['categoria']."|".number_format(($p['weight']/1000),3,",",".")."|||".$fornitore."|".$fornitore."|".$fornitore_abituale."||||".str_replace("\r\n"," ",$p['note'])."|".str_replace("\r\n"," ",$p['note_fornitore'])."|EOR"; $righe.="\n";
			
			if($p['costo_dollari'] > 0)
				$righe_listino .= $p['reference']."|".$p['id_product']."|".substr(str_replace(array("\n\r", "\n", "\r", "\r\n"), "", $p['nome_prodotto']),0,50)."|".$fornitore."|".number_format($p['costo_dollari'],2,",","")."|0|0|0||||USD|EOR";
			else
				$righe_listino .= $p['reference']."|".$p['id_product']."|".substr(str_replace(array("\n\r", "\n", "\r", "\r\n"), "", $p['nome_prodotto']),0,50)."|".$fornitore."|".number_format($p['listino'],2,",","")."|".number_format($p['sconto_acquisto_1'],2,",","")."|".number_format($p['sconto_acquisto_2'],2,",","")."|".number_format($p['sconto_acquisto_3'],2,",","")."||||EUR|EOR";
				
			$righe_listino.="\n";	
			
			if($spedizione > 0)
				$righe_vendita .= $p['supplier_reference']."|".$p['id_product']."|".substr(str_replace(array("\n\r", "\n", "\r", "\r\n"), "", $p['nome_prodotto']),0,50)."||".number_format($p['listino'],2,",","")."|0||||||EOR";
			
			$righe_vendita.="\n";			

			$listini_fornitori = Db::getInstance()->executeS('
				SELECT s.name as fornitore, pli.id_supplier, pli.listino, pli.sconto_acquisto_1, pli.sconto_acquisto_2, pli.sconto_acquisto_3, pli.wholesale_price 
				FROM product_listini pli 
                    JOIN '._DB_PREFIX_.'product p ON p.id_product = pli.id_product 
                    JOIN '._DB_PREFIX_.'supplier s ON pli.id_supplier = s.id_supplier 
				WHERE p.id_supplier != pli.id_supplier 
					AND p.id_product = '.$p['id_product']
			);
			
			foreach($listini_fornitori as $f) {
				switch($f['id_supplier'])
				{
					case 8: $fornitore_f = 35420; break; 
					case 11: $fornitore_f = 36448; break; 
					case 38: $fornitore_f = 36541; break; 
					case 42: $fornitore_f = 35732; break;
					case 76: $fornitore_f = 58350; break; 
				case 1458: $fornitore_f = 46215; break; 
					default: $fornitore_f = ''; break;
				}
				
				$current_wholesale_s = Db::getInstance()->getRow("
					SELECT * 
					FROM "._DB_PREFIX_."specific_price_wholesale sp 
					WHERE sp.to > '".date('Y-m-d H:i:s')."' 
						AND sp.id_product = '".$p['id_product']."' 
						AND sp.id_supplier = ".$f['id_supplier']
				);
		
				if($current_wholesale_s['wholesale_price'] > 0) {
					$f['sconto_acquisto_1'] = $current_wholesale_s['reduction_1'];
					$f['sconto_acquisto_2'] = $current_wholesale_s['reduction_2'];
					$f['sconto_acquisto_3'] = $current_wholesale_s['reduction_3'];
				}
		
				$righe_listino .= $p['supplier_reference']."|".$p['id_product']."|".substr(str_replace(array("\n\r", "\n", "\r", "\r\n"), "", $p['nome_prodotto']),0,50)."|".$fornitore_f."|".number_format($f['listino'],2,",",".")."|".number_format($f['sconto_acquisto_1'],2,",",".")."|".number_format($f['sconto_acquisto_2'],2,",",".")."|".number_format($f['sconto_acquisto_3'],2,",",".")."||||EOR";
				$righe_listino.="\n";	
			}				
			//}
		}	

		$file_csv_prov = fopen($percorso_file.$file_csv_prov,"a+");
		@fwrite($file_csv_prov,$prima_riga.$righe);
		@fclose($percorso_file.$file_csv_prov);
		
		$data = file_get_contents($percorso_file."EZ-ARTICOLI-PROV.csv");
		$data = mb_convert_encoding($data, 'ISO-8859-1');
		file_put_contents($percorso_file."EZ-ARTICOLI.csv", $data);

		unlink($percorso_file."EZ-ARTICOLI-PROV.csv");
		
		$file_csv_listino = fopen($percorso_file.$file_csv_listino,"a+");
		@fwrite($file_csv_listino,$righe_listino);
		@fclose($percorso_file.$file_csv_listino);
				
		$file_csv_vendita = fopen($percorso_file.$file_csv_vendita,"a+");
		@fwrite($file_csv_vendita,$righe_vendita);
		@fclose($percorso_file.$file_csv_vendita);
	}

    // CORREGGERE: commentare il percorso giusto del file per fare prove
    public function RecuperaBDL($id_bdl)
	{
		$dati_bdl = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'bdl WHERE id_bdl = '.$id_bdl);
		
		$customer = new Customer($dati_bdl['id_customer']);
		
		$identificatore_univoco_cliente = $customer->id;
		$partita_iva_cliente = $customer->vat_number;
		$codice_fiscale =  $customer->tax_code;
		
		$data_ordine = date("d/m/Y");
		$pagamento_e = Db::getInstance()->getValue('SELECT pagamento FROM customer_amministrazione WHERE id_customer = '.$customer->id); // pag def cli
		$pagamento = Db::getInstance()->getValue('SELECT pagamento FROM customer_amministrazione WHERE id_customer = '.$customer->id); // pag def cli
		$spedizione_ragione_sociale = '--';
		$ragione_sociale = $customer->company;
		$address_cli = new Address(Db::getInstance()->getValue('SELECT id_address FROM '._DB_PREFIX_.'address WHERE id_customer = '.$customer->id.' AND active = 1 AND deleted = 0 AND fatturazione = 1'));
			
		$indirizzo = $address_cli->address1;
		$cap = $address_cli->postcode;
		$citta = $address_cli->city;
		$localita = $address_cli->suburb;
		$provincia = Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'state WHERE id_state = '.$address_cli->id_state);
		$telefono1 = $address_cli->phone;
		$telefono2 = $address_cli->phone_mobile;
		$fax = $address_cli->fax;
		$indirizzo_email = $customer->email;
		$cognome_cliente = $customer->lastname;
		$nome_cliente = $customer->firstname;
		$spedizione_indirizzo = '--';
		$spedizione_cap = '--';
		$spedizione_citta = '--';
		$spedizione_provincia = '--';
		$spedizione_nazione = 'IT';
		$corriere = '';
		
		$codice_ordine = '1'.$id_bdl;
			
		$riga_ordine = '';
		
		/*esolver */
		$riga_ord_prod_e = '';
		/* fine esolver */
		
		// esolver
		if($pagamento == "Pagamento alla consegna (COD)") {
			$pagamento_e = "X002"; 
		}

		else if($pagamento == "Bonifico Bancario" || $pagamento == "Bonifico bancario" || $pagamento == "Bonifico") {
			$pagamento_e = "BB369"; 
		}
		else if($pagamento == "Bonifico 30 gg. fine mese") {
			$pagamento_e = "BB430"; 
		}

		else if($pagamento == "Bonifico 60 gg. fine mese") {
			$pagamento_e = "BB431"; 
		}
		
		else if($pagamento == "Bonifico 90 gg. fine mese") {
			$pagamento_e = "BB432"; 
		}
		else if($pagamento == "Bonifico 30 gg. 15 mese successivo") {
			$pagamento_e = "BB463"; 
		}
		else if($pagamento == "R.B. 30 GG. D.F. F.M.") {
			$pagamento_e = "RB230"; 
		}
		
		else if($pagamento == "R.B. 60 GG. D.F. F.M.") {
			$pagamento_e = "RB231"; 
		}
		
		else if($pagamento == "R.B. 90 GG. D.F. F.M.") {
			$pagamento_e = "RB232"; 
		}
		
		else if($pagamento_cli == "R.B. 30 GG. 5 mese successivo") {
					$pagamento_e = "RB253"; 
		}
		else if($pagamento_cli == "R.B. 30 GG. 10 mese successivo") {
			$pagamento_e = "RB250"; 
		}
		else if($pagamento_cli == "R.B. 60 GG. 5 mese successivo") {
			$pagamento_e = "RB258"; 
		}
		else if($pagamento_cli == "R.B. 60 GG. 10 mese successivo") {
			$pagamento_e = "RB249"; 
		}
		
		else if($pagamento == "Contrassegno") {
			$pagamento_e = "X002"; 
		}

		else if($pagamento == "Contrassegno") {
			$pagamento_e = "X002"; 
		}
		
		else if($pagamento == "Carta Amazon") {
			$pagamento_e = "X011"; 
		}
		
		else if($pagamento == "Carta ePrice") {
			$pagamento_e = "X012"; 
		}
		
		else if($pagamento == "PayPal") {
			$pagamento_e = "X013"; 
		}
		
		else if($pagamento == "GestPay") {
			// gestpay
			$pagamento_e = "X007"; 
		}

		else {
			$pagamento_e = "BB369"; 
		}
		// fine esolver
			
		$riga_cli = '';
		if($pagamento == "Pagamento alla consegna (COD)") {
			$pagamento = "Contrassegno."; 
		}

		else if($pagamento == "Bonifico Bancario") {
			$pagamento = "Bonifico Bancario"; 
		}

		else if($pagamento == "Contrassegno") {
			$pagamento = "Contrassegno."; 
		}
		
		else if($pagamento == "Bonifico 30 gg. fine mese") {
			$pagamento = "BB30"; 
		}
		
		else if($pagamento == "Bonifico 60 gg. fine mese" || $pagamento == "Bonifico 90 gg. fine mese" ) {
			$pagamento = "posticipato"; 
		}
		
		else if($pagamento == "Bonifico 30 gg. 15 mese successivo") {
			$pagamento = "BB463"; 
		}
		
		else if($pagamento == "R.B. 60 GG. D.F. F.M." || $pagamento == "R.B. 90 GG. D.F. F.M." ) {
			$pagamento = "60RB"; 
		}
		
		else if($pagamento == "R.B. 30 GG. D.F. F.M.") {
			$pagamento = "30RB"; 
		}

		else {
			$pagamento = "<p><strong>CARTA DI CREDITO:</st"; 
		}
			
		$riga_cli .= '"'.$identificatore_univoco_cliente.'";"'.$partita_iva_cliente.'";"'.$codice_fiscale.'";"";"";"";"'.$ragione_sociale.'";"";"'.$indirizzo.'";"'.$cap.'";"'.$citta.'";"'.$provincia.'";"";"'.$telefono1.'";"'.$telefono2.'";"'.$fax.'";"'.$indirizzo_email.'";"";"";"'.$cognome_cliente.'";"'.$nome_cliente.'";"";"";"";"";"";"";"";"";"'.$pagamento.'";"";"";"";"";"";"";"";"";"";"";"";"";"";"'.$ragione_sociale.'";"";"'.$spedizione_indirizzo.'";"'.$spedizione_cap.'";"'.$spedizione_citta.'";"'.$spedizione_provincia.'";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"1"';

		$riga_cli .= "\n";
		
		$riga_cli .= '"'.$identificatore_univoco_cliente.'";"'.$partita_iva_cliente.'";"'.$codice_fiscale.'";"";"";"";"'.$spedizione_ragione_sociale.'";"";"'.$spedizione_indirizzo.'";"'.$spedizione_cap.'";"";"'.$spedizione_provincia.'";"";"";"";"";"'.$indirizzo_email.'";"";"";"";"";"";"";"";"";"";"";"";"";"'.$pagamento.'";"";"";"";"";"";"";"";"";"";"";"";"";"";"'.$spedizione_ragione_sociale.'";"";"'.$spedizione_indirizzo.'";"'.$spedizione_cap.'";"'.$spedizione_citta.'";"'.$spedizione_provincia.'";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"1"';

		$riga_cli .= "\n";
	
		$dettaglio_bdl = array();
		
		/* esolver */
		$data_nuova_e = date('d/m/Y', strtotime($dati_bdl['date_add']));
		/* fine esolver */
		
		$note_bdl = 'BDL '.$id_bdl.' del '.date("d/m/Y", strtotime(Db::getInstance()->getValue('SELECT data_effettuato FROM '._DB_PREFIX_.'bdl WHERE id_bdl = '.$id_bdl)));
	
		$riga_ordine .= '"'.$identificatore_univoco_cliente.'";"'.$partita_iva_cliente.'";"'.$codice_fiscale.'";"'.$identificatore_univoco_cliente.'";"";"'.$codice_ordine.'";"'.$data_ordine.'";"'.$pagamento.'";"";"'.$spedizione_ragione_sociale.'";"'.$ragione_sociale.'";"'.$spedizione_indirizzo.'";"'.$spedizione_cap.'";"'.$spedizione_citta.'";"'.$spedizione_provincia.'";"";"";"";"";"";"";"";"";"";"";"";"'.$codice_ordine.'";"RIFBDL";"";"";"1";"";"";"";"";"";"";"";"'."€"."1,00".'";"";"";"";"";"";"";"'.$note_bdl.'";"";"0"';
		$riga_ordine .= "\n";
		
		$ccod = $codice_ordine;
		$file_csv = "ORD.$ccod.csv";
		$file_csv2 = "CLI.$ccod.csv";

		/* esolver */
		if($customer->is_company == 1) {
	
		}
		else {
			$ragione_sociale = $nome_cliente." ".$cognome_cliente;
		}
		
		$ragione_sociale_e = explode(';;',wordwrap($ragione_sociale, 30, ";;", true));

		$ragione_sociale_prima_parte = $ragione_sociale_e[0];
		$ragione_sociale_seconda_parte = $ragione_sociale_e[1];
		
		$indirizzo_e = explode(';;',wordwrap($indirizzo, 35, ";;", true));
		
		$indirizzo_prima_parte = $indirizzo_e[0];
		$indirizzo_seconda_parte = $indirizzo_e[1];
		
		if($spedizione_nazione == "IT")
			$codice_esterno_fornitore = 33153; // GLS
		else
			$codice_esterno_fornitore = 33153; // GLS

		//	$codice_esterno_fornitore = 35432; // TNT
		
		$corriere_nome = Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'carrier WHERE id_carrier = '.$corriere);
		
		if(strpos($corriere_nome, 'Ritiro') !== false)
			$codice_trasporto_a_mezzo = '2';
		else
			$codice_trasporto_a_mezzo = '1';
		
		if($codice_trasporto_a_mezzo == '2')
			$codice_esterno_fornitore = '';

		$riga_ord_e = 'TES|'.$data_nuova_e.'|'.$codice_ordine.'|'.$identificatore_univoco_cliente.'||||'.$pagamento_e.'|'.number_format($totale_bdl_u,2,",","").'|1|||||'.$ragione_sociale_prima_parte.'|'.$ragione_sociale_seconda_parte.'|'.$spedizione_indirizzo.'|'.$spedizione_cap.'|'.$spedizione_citta.'||'.$spedizione_provincia.'|'.$spedizione_nazione.'||1||1||1||1||33153|0||||||||||||1||EOR'."";
		
		$riga_ord_e .= $riga_ord_prod_e;
		
		if(strpos(strtolower($ragione_sociale), 'srl') !== false || strpos(strtolower($ragione_sociale), 's.r.l') !== false || strpos(strtolower($ragione_sociale), 's.p.a') !== false || strpos(strtolower($ragione_sociale), ' spa ') !== false)
			$tipo_soggetto = 1;
		else if(strpos(strtolower($ragione_sociale), ' sas ') !== false || strpos(strtolower($ragione_sociale), 's.a.s') !== false || strpos(strtolower($ragione_sociale), 's.n.c') !== false || strpos(strtolower($ragione_sociale), 'snc') !== false)
			$tipo_soggetto = 2;
		else if($partita_iva_cliente != $codice_fiscale)
			$tipo_soggetto = 3;
		else
			$tipo_soggetto = 4;

		$codice_nazione = Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'country WHERE id_country = '.$address_cli->id_country);
		$provincia_esteso = Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'state WHERE id_state = '.$address_cli->id_state);
		$gruppo_cliente = $customer->id_default_group;
		
		if($row['codice_nazione'] == 'IT')
			$codice_iva_agevolata = '411';
		else
			$codice_iva_agevolata = '412';
		
		if($tipo_soggetto == 3 && $codice_iva_agevolata == '411')
			$stato_nascita = 'IT';
		else
			$stato_nascita = '';
		
		$size_ordini = Db::getInstance()->executeS('
			SELECT id_customer 
			FROM '._DB_PREFIX_.'orders 
			WHERE id_customer = '.$identificatore_univoco_cliente
		);

		if(sizeof($size_ordini) == 1)
			$nuovo = '*';
		
		$pagamento_cli = Db::getInstance()->getValue('
			SELECT pagamento 
			FROM customer_amministrazione 
			WHERE id_customer = '.$identificatore_univoco_cliente
		);
				
		if($pagamento_cli != '' || !isset($pagamento_cli))
			$pagamento_cli = $pagamento_e;
		
		// esolver
		if($pagamento_cli == "Pagamento alla consegna (COD)") {
			$pagamento_cli_e = "X002"; 
		}

		else if($pagamento_cli == "Bonifico Bancario" || $pagamento_cli == "Bonifico bancario" || $pagamento_cli == "Bonifico") {
			$pagamento_cli_e = "BB369"; 
		}
		else if($pagamento_cli == "Bonifico 30 gg. fine mese") {
			$pagamento_cli_e = "BB430"; 
		}

		else if($pagamento_cli == "Bonifico 60 gg. fine mese") {
			$pagamento_cli_e = "BB431"; 
		}
		
		else if($pagamento_cli == "Bonifico 90 gg. fine mese") {
			$pagamento_cli_e = "BB432"; 
		}
		
		else if($pagamento_cli == "Bonifico 30 gg. 15 mese successivo") {
			$pagamento_cli_e = "BB463"; 
		}
		
		else if($pagamento_cli == "R.B. 30 GG. D.F. F.M.") {
			$pagamento_cli_e = "RB230"; 
		}
		
		else if($pagamento_cli == "R.B. 60 GG. D.F. F.M.") {
			$pagamento_cli_e = "RB231"; 
		}
		
		else if($pagamento_cli == "R.B. 90 GG. D.F. F.M.") {
			$pagamento_cli_e = "RB232"; 
		}
		
		else if($pagamento_cli == "R.B. 30 GG. 5 mese successivo") {
			$pagamento_cli_e = "RB253"; 
		}
		else if($pagamento_cli == "R.B. 30 GG. 10 mese successivo") {
			$pagamento_cli_e = "RB250"; 
		}
		else if($pagamento_cli == "R.B. 60 GG. 5 mese successivo") {
			$pagamento_cli_e = "RB258"; 
		}
		else if($pagamento_cli == "R.B. 60 GG. 10 mese successivo") {
			$pagamento_cli_e = "RB249"; 
		}
		else if($pagamento_cli == "Contrassegno") {
			$pagamento_cli_e = "X002"; 
		}

		else if($pagamento_cli == "Contrassegno") {
			$pagamento_cli_e = "X002"; 
		}
		
		else if($pagamento_cli == "Carta Amazon") {
			$pagamento_cli_e = "X011"; 
		}
		
		else if($pagamento_cli == "Carta ePrice") {
			$pagamento_cli_e = "X012"; 
		}
		
		else if($pagamento_cli == "PayPal") {
			$pagamento_cli_e = "X013"; 
		}
		
		else if($pagamento_cli == "GestPay") {
			// gestpay
			$pagamento_cli_e = "X007"; 
		}
		else
		{
			$pagamento_cli_e = "BB369"; 
			
		}
		// fine esolver
		
		$riga_cli_e = 'GEN|'.$identificatore_univoco_cliente.'|'.$partita_iva_cliente.'|'.$codice_fiscale.'|'.($partita_iva_cliente != '' ? 0 : 1).'||'.($gruppo_cliente == 5 ? '1' : '0').'||'.$tipo_soggetto.'|'.$ragione_sociale_prima_parte.'|'.$ragione_sociale_seconda_parte.'|'.$indirizzo_prima_parte.'|'.$indirizzo_seconda_parte.'|'.$cap.'|'.$citta.'||'.$provincia.'|'.$codice_nazione.'|'.$telefono1.'|'.$telefono2.'|'.$fax.'|'.$indirizzo_email.'|'.$nome_cliente.' '.$cognome_cliente.'|'.$cognome_cliente.'|'.$nome_cliente.'|'.'|'.'|'.'|'.'|'.$stato_nascita.'|'.$codice_iva_agevolata.'|'.$codice_nazione.$provincia.'|'.$codice_nazione.'-'.$provincia_esteso.'|||'.$nuovo.'|'.$pagamento_cli_e.'|'."\n";
		
		
		$ccod = $codice_ordine;
		$file_csv = "BDL.$ccod.csv";
		$file_csv2 = "CLI.$ccod.csv";

		// TEST! Correggere
		$percorso_file = _PS_ROOT_DIR_.'ezadmin/test-files/';
		// $percorso_file = _PS_DOCS_DIR_.'disponibilita/esolver/';
		// $percorso_file = '/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/';
		
		if(!$customer->id) {

		}
		else {
            $file_csv = fopen($percorso_file.$file_csv,"a+");
            @fwrite($file_csv,$riga_ord_e);

            $file_csv2 = fopen($percorso_file.$file_csv2,"a+");
            @fwrite($file_csv2,$riga_cli_e);
		}
		
		/*fine esolver */
	}

    /* BDL */

    // DA CONTROLLARE
    //public function BDLtoCart($id_bdl){}


	/* KIT (BUNDLE) */

	function controllaSeFaParteDiUnBundle($id_order, $id_product)
	{
		$bundle = Db::getInstance()->getValue("
			SELECT cp.bundle 
			FROM "._DB_PREFIX_."cart_product cp 
				JOIN "._DB_PREFIX_."cart c ON cp.id_cart = c.id_cart 
				JOIN "._DB_PREFIX_."orders o ON o.id_cart = c.id_cart 
			WHERE o.id_order = ".$id_order." 
				AND cp.id_product = ".$id_product
		);

		if($bundle > 0)
			return 1;
		else
			return 0;
	}
}
