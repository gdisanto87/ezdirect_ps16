<?php

class Manufacturer extends ManufacturerCore
{
	public 		$margin_min_cli;
	public 		$margin_min_riv;
	public 		$margin_login_for_offer;
	public 		$amazon_it;
	public 		$amazon_fr;
	public 		$amazon_es;
	public 		$amazon_nl;
	public 		$amazon_de;
	public 		$amazon_uk;
	public 		$amazon_se;
	public 		$amazon_pl;
	public		$website;

	public static $definition = array(
        'table' => 'manufacturer',
        'primary' => 'id_manufacturer',
        'multilang' => true,
        'fields' => array(
            'name' =>                array('type' => self::TYPE_STRING, 'validate' => 'isCatalogName', 'required' => true, 'size' => 64),
            'active' =>            array('type' => self::TYPE_BOOL),
            'date_add' =>            array('type' => self::TYPE_DATE),
            'date_upd' =>            array('type' => self::TYPE_DATE),

            /* Lang fields */
            'description' =>        array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
            'short_description' =>    array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
            'meta_title' =>        array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 128),
            'meta_description' =>    array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'meta_keywords' =>        array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName'),

			/* Override */
			// aggiungere i campi nuovi
        ),
    );

	// Vedi funzioni in override 1.4 -> controlla se vengono utilizzate tutte

	public static function getSupplierById($id_manufacturer)
	{
		return Db::getInstance()->getValue('SELECT `supplier` FROM `'._DB_PREFIX_.'manufacturer` WHERE `id_manufacturer` = '.(int)($id_manufacturer).' AND `active` = 1');
	}

	public static function getOtherSuppliersById($id_manufacturer)
	{
		$suppliers = Db::getInstance()->getValue('SELECT `other_suppliers` FROM `'._DB_PREFIX_.'manufacturer` WHERE `id_manufacturer` = '.(int)($id_manufacturer).' AND `active` = 1');
		$suppliers = unserialize($suppliers);

		$suppliers_string = '';

		foreach($suppliers as $supplier) 
			$suppliers_string .= $supplier.";";

		return $suppliers_string;
	}
}
