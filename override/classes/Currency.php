<?php

class Currency extends CurrencyCore
{
    public static function refreshCurrencies()
    {
        // Parse
        if (!$feed = Tools::simplexml_load_file(_PS_CURRENCY_FEED_URL_)) {
            return Tools::displayError('Cannot parse feed.');
        }

        // Default feed currency (EUR)
        $isoCodeSource = strval($feed->source['iso_code']);

        if (!$default_currency = Currency::getDefaultCurrency()) {
            return Tools::displayError('No default currency');
        }

        $currencies = Currency::getCurrencies(true, false, true);
        foreach ($currencies as $currency) {

            // Spostare dentro all'if? aggiungere last_conversion_rate nel db
            /*
            $last_conversion = Db::getInstance()->getValue('
                SELECT `conversion_rate`
                FROM '._DB_PREFIX_.'currency
                WHERE `id_currency` = '.$currency->id
            );

            Db::getInstance()->execute('
                UPDATE '._DB_PREFIX_.'currency 
                SET `last_conversion_rate` = '.$last_conversion.'
                WHERE `id_currency` = '.$currency->id
            );
            */
            /** @var Currency $currency */
            if ($currency->id != $default_currency->id) {
                $currency->refreshCurrency($feed->list, $isoCodeSource, $default_currency);
            }
        }
    }
}