<?php
class Link extends LinkCore
{
	
	public function getImageLink($name, $ids, $type = NULL)
	{
		global $protocol_content;

		// legacy mode or default image
		/*if ((Configuration::get('PS_LEGACY_IMAGES') 
			&& (file_exists(_PS_PROD_IMG_DIR_.$ids.($type ? '-'.$type : '').'.jpg')))
			|| strpos($ids, 'default') !== false)
		{*/
			if($ids > 0)
			{	
				if ($this->allow == 1)
					$uri_path = __PS_BASE_URI__.$ids.($type ? '-'.$type : '').'/'.$name.'.jpg';
				else
					$uri_path = _THEME_PROD_DIR_.$ids.($type ? '-'.$type : '').'.jpg';
			}
			else
			{
				$id_product = Db::getInstance()->getValue('SELECT id_product FROM product_lang WHERE link_rewrite = "'.$name.'"');
				$manufacturer = Db::getInstance()->getValue('SELECT id_manufacturer FROM product WHERE id_product = '.$id_product);
				$uri_path = "/img/m/".$manufacturer."-medium.jpg";
			}	
		/*}
		else
		{
			// if ids if of the form id_product-id_image, we want to extract the id_image part
			$split_ids = explode('-', $ids);
			$id_image = (isset($split_ids[1]) ? $split_ids[1] : $split_ids[0]);
			
			if($id_image > 0)
			{	
				if ($this->allow == 1)
					$uri_path = __PS_BASE_URI__.$id_image.($type ? '-'.$type : '').'/'.$name.'.jpg';
				else
					$uri_path = _THEME_PROD_DIR_.Image::getImgFolderStatic($id_image).$id_image.($type ? '-'.$type : '').'.jpg';
			}
			else
			{
				$id_product = Db::getInstance()->getValue('SELECT id_product FROM product_lang WHERE link_rewrite = "'.$name.'"');
				$manufacturer = Db::getInstance()->getValue('SELECT id_manufacturer FROM product WHERE id_product = '.$id_product);
				
				$uri_path = "/img/m/".$manufacturer."-medium.jpg";
				//$uri_path = __PS_BASE_URI__.$id_image.($type ? '-'.$type : '').'/'.$name.'.jpg';
			}
		}*/
		
		return 'https://www.ezdirect.it'.$uri_path;
	}
	

	
	public function getCMSLink($cms, $alias = null, $ssl = false, $id_lang = NULL)
	{
		$base = (($ssl AND Configuration::get('PS_SSL_ENABLED')) ? Tools::getShopDomainSsl(true) : Tools::getShopDomain(true));
	
		if (is_object($cms))
		{
			return ($this->allow == 1) ? 
				($base.__PS_BASE_URI__.$this->getLangLink((int)($id_lang)).'guide/'.(int)($cms->id).'-'.$cms->link_rewrite) :
				($base.__PS_BASE_URI__.'cms.php?id_cms='.(int)($cms->id));
		}
		
		if ($alias)
			return ($this->allow == 1) ? ($base.__PS_BASE_URI__.$this->getLangLink((int)($id_lang)).'guide/'.(int)($cms).'-'.$alias) :
			($base.__PS_BASE_URI__.'cms.php?id_cms='.(int)($cms));
		return $base.__PS_BASE_URI__.'cms.php?id_cms='.(int)($cms);
	}

public function getCMSLink2($cms, $alias = null, $ssl = false, $id_lang = NULL)
	{
		$base = (($ssl AND Configuration::get('PS_SSL_ENABLED')) ? Tools::getShopDomainSsl(true) : Tools::getShopDomainSsl(true));
	
		if (is_object($cms))
		{
			return ($this->allow == 1) ? 
				($base.__PS_BASE_URI__.$this->getLangLink((int)($id_lang)).'guide/'.(int)($cms->id).'-'.$cms->link_rewrite) :
				($base.__PS_BASE_URI__.'cms.php?id_cms='.(int)($cms->id));
		}
		
		if ($alias)
			return ($this->allow == 1) ? ($base.__PS_BASE_URI__.$this->getLangLink((int)($id_lang)).'guide/'.(int)($cms).'-'.$alias) :
			($base.__PS_BASE_URI__.'cms.php?id_cms='.(int)($cms));
		return $base.__PS_BASE_URI__.'cms.php?id_cms='.(int)($cms);
	}

	public function getCMSRewrite($id) {
       
	   global $cookie;
	   $lang = (int)$cookie->id_lang;
	   $rew = Db::getInstance()->getValue("SELECT link_rewrite FROM "._DB_PREFIX_."cms_lang WHERE id_cms = $id AND id_lang = $lang");

	   return $rew;
	   
	}
	
	 public function getCatRewrite($id) {
       
	   global $cookie;
	   $lang = (int)$cookie->id_lang;
	   $query = "SELECT link_rewrite FROM "._DB_PREFIX_."category_lang WHERE id_category = $id AND id_lang = $lang";
	  
	   $row = Db::getInstance()->getRow($query);
	   
	   return $row['link_rewrite']."";
	   
	}
	
			public function getManufacturerLink2($id_manufacturer, $alias = NULL, $id_lang = NULL)
	{
		if (is_object($id_manufacturer))
			return ($this->allow == 1) ? (_PS_BASE_URL_SSL_.__PS_BASE_URI__.$this->getLangLink((int)($id_lang)).(int)($id_manufacturer->id)."m-".$id_manufacturer->link_rewrite."/") :
			(_PS_BASE_URL_.__PS_BASE_URI__.'manufacturer.php?id_manufacturer='.(int)($id_manufacturer->id));
		if ($alias)
			return ($this->allow == 1) ? (_PS_BASE_URL_SSL_.__PS_BASE_URI__.$this->getLangLink((int)($id_lang)).(int)($id_manufacturer)."m-".strtolower(str_replace(array(" ","."),array("-",""),$alias))."/") :
			(_PS_BASE_URL_SSL_.__PS_BASE_URI__.'manufacturer.php?id_manufacturer='.(int)($id_manufacturer));
		return _PS_BASE_URL_SSL_.__PS_BASE_URI__.'manufacturer.php?id_manufacturer='.(int)($id_manufacturer);
	}

	public function getProductLink2($id_product, $alias = NULL, $category = NULL, $supplier_reference = NULL, $id_lang = NULL)
	{
		global $cookie;
		if (is_object($id_product))
		{		
		
			$link = '';
			if ($this->allow == 1)
			{
				$link .= (_PS_BASE_URL_SSL_.__PS_BASE_URI__.$this->getLangLink((int)$id_lang));
				
				if (isset($id_product->category) AND !empty($id_product->category) AND $id_product->category != 'home')
					$link .= $id_product->category.'/';
				else
					$link .= '';

				$link .= (int)$id_product->id.'-';
				if (is_array($id_product->link_rewrite))
					$link.= $id_product->link_rewrite[(int)$cookie->id_lang];
				else 
				 	$link.= $id_product->link_rewrite;
				if ($id_product->supplier_reference) {
				
					}
				else {
					$link .= '';
				}
				$link .= '.html';
			}
			else 
			{

				$link .= (_PS_BASE_URL_SSL_.__PS_BASE_URI__.'product.php?id_product='.(int)$id_product->id);
			}
			return $link;
		}
		
		else if ($alias)
		{
		
			$link = '';
			if ($this->allow == 1)
			{
				$link .= (_PS_BASE_URL_SSL_.__PS_BASE_URI__.$this->getLangLink((int)$id_lang));
				
				if ($category AND $category != 'home')
					$link .= $category.'/';
				else 
				 	$link .= '';
				 
				$link .= (int)$id_product.'-'.$alias;
				
				
				
				/*if ($supplier_reference) {
				if(preg_match('/\//', $supplier_reference)) {
				$supplier_reference = preg_replace('/\//', '-', $supplier_reference);
				$link .= '-'.$supplier_reference;
				}
				else if(preg_match('/[\s]+/', $supplier_reference)) {
				$supplier_reference = preg_replace('/\./', '-', $supplier_reference);
				$link .= '-'.$supplier_reference;
				}
					else if(preg_match('/\%20/', $supplier_reference)) {
				$supplier_reference = preg_replace('/\./', '-', $supplier_reference);
				$link .= '-'.$supplier_reference;
				}
				else if(preg_match('/\./', $supplier_reference)) {
				$supplier_reference = preg_replace('/\./', '-', $supplier_reference);
				$link .= '-'.$supplier_reference;
				}
				else {
					$link .='-'.$supplier_reference;
					}
					}
				else  {
					$link .= '';
			}*/
				$link .= '.html';
			}
			else
				$link .=(_PS_BASE_URL_SSL_.__PS_BASE_URI__.'product.php?id_product='.(int)$id_product);
				$link = str_replace(' ', '-', $link);
				$link = str_replace('%20', '-', $link);
			return $link;
		}
		
		else
			return _PS_BASE_URL_SSL_.__PS_BASE_URI__.'product.php?id_product='.(int)$id_product;
	}
	
	
	
public function goPage2($url, $p)
	{
$url = str_replace('http:', 'https:', $url);
		if(!strstr($url, 'submit_search') && !strstr($url, '?cat=') && !strstr($url, 'mobile_site') && !strstr($url, 'produttori-') && !strstr($url, 'prodotti-venduti') && !strstr($url, 'offerte-speciali') && !strstr($url, '?category=')) {
			return $url.($p == 1 ? '' : (!strstr($url, '?') ? '?' : '&amp;').'p='.(int)($p));
			//return $url.($p == 1 ? '' : (!strstr($url, '?') ? '' : '/').'pag/'.(int)($p));
		}
		else {
			if (strstr($url, 'mobile_site')) {
				$url = str_replace('?ps_mobile_site=1', '', $url);
				return $url.($p == 1 ? '' : (!strstr($url, '?') ? '?' : '&amp;').'p='.(int)($p));
				//return $url.($p == 1 ? '' : (!strstr($url, '?') ? '' : '/').'pag/'.(int)($p))."?ps_mobile_site=1";
			}
			else {
				//return $url.($p == 1 ? '' : (!strstr($url, '?') ? '' : '/').'pag/'.(int)($p));
				return $url.($p == 1 ? '' : (!strstr($url, '?') ? '?' : '&amp;').'p='.(int)($p));
			}
		}

	}
	
}