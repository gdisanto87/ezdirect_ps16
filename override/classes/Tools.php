<?php

class Tools extends ToolsCore
{
    // Aggiunto l'argomento $params per tenere i parametri in GET dell'url di partenza, esclusi controller e token
    public static function redirectAdmin($url, $params = false)
    {
        if($params == true) {
            if ($_SERVER['QUERY_STRING'] !== null) {
                $get_params = $_SERVER['QUERY_STRING'];

                $controller = (Tools::getIsset('tab') ? Tools::getValue('tab') : Tools::getValue('controller')); // Compatibilità 1.4
                $pos_controller = strpos($get_params, ''.$controller.'');
                $pos_controller = $pos_controller + strlen(''.$controller.'');
                $get_params = substr($get_params, $pos_controller); // Ipotizzo che il controller sia sempre il primo parametro della stringa

                $pos_token_i = strpos($get_params, '&token');
                $pos_token_f = $pos_token_i + strlen('&token=') + strlen(''.Tools::getValue('token').'');
                
                $get_params_1 = substr($get_params, 0, $pos_token_i);
                $get_params_2 = substr($get_params, $pos_token_f);
                $get_params = $get_params_1.$get_params_2;

                // Correggere: si può migliorare non mettendo il token in $url ma definendolo qui per il nuovo controller per metterlo in fondo alla stringa
            } 
            else {
                $get_params = '';
            }

            $url = $url.$get_params;
        }

        header('Location: '.$url);
        exit;
    }
}