<?php

class Tag extends TagCore
{
    // Aggiunto controllo isTagsListModificata
    public static function addTags($id_lang, $id_product, $tag_list, $separator = ',')
    {
        if (!Validate::isUnsignedId($id_lang) OR !Validate::isTagsListModificata($tag_list)) {
            return false;
        }

        if (!is_array($tag_list)) {
            $tag_list = array_filter(array_unique(array_map('trim', preg_split('#\\'.$separator.'#', $tag_list, null, PREG_SPLIT_NO_EMPTY))));
        }

        $list = array();
        if (is_array($tag_list)) {
            foreach ($tag_list as $tag) {
                if (!Validate::isGenericName($tag)) {
                    return false;
                }
                $tag = trim(Tools::substr($tag, 0, self::$definition['fields']['name']['size']));
                $tag_obj = new Tag(null, $tag, (int)$id_lang);

                /* Tag does not exist in database */
                if (!Validate::isLoadedObject($tag_obj)) {
                    $tag_obj->name = $tag;
                    $tag_obj->id_lang = (int)$id_lang;
                    $tag_obj->add();
                }
                if (!in_array($tag_obj->id, $list)) {
                    $list[] = $tag_obj->id;
                }
            }
        }
        $data = '';
        foreach ($list as $tag) {
            $data .= '('.(int)$tag.','.(int)$id_product.','.(int)$id_lang.'),';
        }
        $data = rtrim($data, ',');

        $result = Db::getInstance()->execute('
		INSERT INTO `'._DB_PREFIX_.'product_tag` (`id_tag`, `id_product`, `id_lang`)
		VALUES '.$data);

        if ($list != array()) {
            self::updateTagCount($list);
        }

        return $result;
    }
}