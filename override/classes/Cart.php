<?php

class Cart extends CartCore
{
	public $preventivo = 0;
    public $convertito = 0;
    public $visualizzato = 0;

	public $created_by;
	public $in_carico_a;
    public $name;
    public $id_persona;
    public $rif_prev;
    public $riferimento;

	public $premessa;
	public $esigenze;
	public $risorse;
	public $note;

	public $note_private;
	public $provvisorio;
	public $consegna;
	public $transport;
	public $payment;
	public $validita;
	public $total_products;
	public $attachment;
	public $id_employee;
	public $notifiche;
	public $telegestione;
	public $revisioni;
	public $email_preparate;
	public $numero_notifiche;
	public $prezzi_carrello;
	public $no_addebito_commissioni;
	public $rif_ordine;
	public $cig;
	public $cup;
	public $ipa;
	public $template;
	public $cadenza;
	public $scadenza;
	public $decorrenza;
	public $esito;
	public $causa;
	public $concorrente;
	public $data_ordine_mepa;
	public $competenza_dal;
	public $competenza_al;

	// correggere: spostare in $definition
	protected $fieldsValidate = array(
		'id_address_delivery' => 'isUnsignedId', 
		'id_address_invoice' => 'isUnsignedId',
		'id_currency' => 'isUnsignedId', 
		'id_customer' => 'isUnsignedId', 
		'id_guest' => 'isUnsignedId', 
		'id_lang' => 'isUnsignedId',
		'id_carrier' => 'isUnsignedId', 
		'recyclable' => 'isUnsignedId', 
		'gift' => 'isBool', 
		'gift_message' => 'isMessage'
	);

	public static $definition = array(
        'table' => 'cart',
        'primary' => 'id_cart',
        'fields' => array(
            'id_shop_group' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_shop' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_address_delivery' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_address_invoice' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_carrier' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_currency' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_customer' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_guest' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_lang' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'recyclable' =>            array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'gift' =>                    array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'gift_message' =>            array('type' => self::TYPE_STRING, 'validate' => 'isMessage'),
            'mobile_theme' =>            array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'delivery_option' =>        array('type' => self::TYPE_STRING),
            'secure_key' =>            array('type' => self::TYPE_STRING, 'size' => 32),
            'allow_seperated_package' =>array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'date_add' =>                array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' =>                array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            // Override
            'created_by' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'convertito' =>            array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'visualizzato' =>          array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'preventivo' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'name' =>                  array('type' => self::TYPE_STRING),
			// correggere: da finire...
        ),
    );

	const ONLY_COMMISSIONS = 9;
    const SPLIT_PAYMENT = 10;
    
	public static function searchCartById($query)
	{
		$query = (string)$query;
		$query2 = explode('-',$query);
		$query_f = $query2[0];
		
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('SELECT ca.id_cart, (CASE WHEN ca.preventivo = 0
			THEN "0"
			ELSE
			ca.preventivo
			END) AS `prev`, (CASE c.is_company
			WHEN 0
			THEN CONCAT(c.firstname," ",c.lastname)
			WHEN 1
			THEN c.company
			ELSE
			CONCAT(c.firstname," ",c.lastname)
			END
			)as customer, 
			(CASE WHEN o.id_order IS NULL THEN 0 ELSE 1 END) `conv`,
			(CASE WHEN ca.created_by > 0 THEN (CONCAT(cr.firstname," ",LEFT(cr.lastname, 1),".")) ELSE "Cliente" END) created_by,
			(CASE WHEN ca.id_employee > 0 THEN (CONCAT(ic.firstname," ",LEFT(ic.lastname, 1),".")) ELSE "--" END) in_carico,
			ca.visualizzato, c.id_customer, c.id_default_group as gruppo, o.id_order, ca.date_add FROM '._DB_PREFIX_.'cart ca LEFT JOIN '._DB_PREFIX_.'customer c on ca.id_customer = c.id_customer LEFT JOIN '._DB_PREFIX_.'orders o on o.id_cart = ca.id_cart LEFT JOIN '._DB_PREFIX_.'employee cr on ca.created_by = cr.id_employee LEFT JOIN '._DB_PREFIX_.'employee ic on ca.id_employee = ic.id_employee where ca.id_cart = "'.$query_f.'"');
	}
	
	public static function searchCartByProductName($query)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT ca.id_cart, 
				(CASE WHEN ca.preventivo = 0 THEN "0" ELSE ca.preventivo END) AS `prev`, 
				(CASE c.is_company
					WHEN 0
					THEN CONCAT(c.firstname," ",c.lastname)
					WHEN 1
					THEN c.company
					ELSE CONCAT(c.firstname," ",c.lastname)
					END
				) AS customer, 
				(CASE WHEN o.id_order IS NULL THEN 0 ELSE 1 END) `conv`,
				(CASE WHEN ca.created_by > 0 THEN (CONCAT(cr.firstname," ",LEFT(cr.lastname, 1),".")) ELSE "Cliente" END) created_by,
				(CASE WHEN ca.id_employee > 0 THEN (CONCAT(ic.firstname," ",LEFT(ic.lastname, 1),".")) ELSE "--" END) in_carico,
				ca.visualizzato, c.id_customer, c.id_default_group AS gruppo, o.id_order, ca.date_add 
			FROM '._DB_PREFIX_.'cart ca 
			LEFT JOIN '._DB_PREFIX_.'customer c ON ca.id_customer = c.id_customer 
			LEFT JOIN '._DB_PREFIX_.'orders o ON o.id_cart = ca.id_cart 
			LEFT JOIN '._DB_PREFIX_.'employee cr ON ca.created_by = cr.id_employee 
			LEFT JOIN '._DB_PREFIX_.'employee ic ON ca.id_employee = ic.id_employee 
			WHERE ca.id_cart IN (
				SELECT id_cart 
				FROM '._DB_PREFIX_.'cart_product cp 
				JOIN '._DB_PREFIX_.'product p ON cp.id_product = p.id_product 
				WHERE name LIKE "%'.$query.'%" OR reference LIKE "%'.$query.'%")
		');	
	}
	
	
    // vedi file 1.4

    // per confronto; vedi anche cart rules; chiedi a federico, le regole sui carrelli sono cambiate molto
    public function getOrderTotalVECCHIA($with_taxes = true, $type = Cart::BOTH, $user_login = false)
	{
		if (!$this->id)
            return 0;
            
		$type = (int)($type);
		if (!in_array($type, array(
				Cart::ONLY_PRODUCTS,
				Cart::ONLY_DISCOUNTS,
				Cart::BOTH,
				Cart::BOTH_WITHOUT_SHIPPING,
				Cart::ONLY_SHIPPING,
				Cart::ONLY_WRAPPING,
				Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING,
				Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING,
				Cart::SPLIT_PAYMENT, // agg
				Cart::ONLY_COMMISSIONS, // agg
			)
		))
			die(Tools::displayError());

		// no shipping cost if is a cart with only virtuals products
		$virtual = $this->isVirtualCart();
		if ($virtual AND $type == Cart::ONLY_SHIPPING)
			return 0;
		if ($virtual AND $type == Cart::BOTH)
            $type = Cart::BOTH_WITHOUT_SHIPPING;
            
        /* // CANC
        if (!in_array($type, array(Cart::BOTH_WITHOUT_SHIPPING, Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING, Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING)))
			$shipping_fees = $this->getOrderShippingCost(null, (int)($with_taxes));
		else
			$shipping_fees = 0;*/

		if ($type == Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING)
			$type = Cart::ONLY_PRODUCTS;

		$products = $this->getProducts();
		
		if ($type == Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING) {
			foreach ($products as $key => $product)
				if (ProductDownload::getIdFromIdProduct($product['id_product']))
					unset($products[$key]);
			$type = Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING;
		}
		
		$order_total = 0;
		$total_tax = 0; // AGG
		
		$wtxc = 0; // AGG
		
		if (Tax::excludeTaxeOption())
            $with_taxes = false;
            
		foreach ($products AS $product) {
			if ($this->_taxCalculationMethod == PS_TAX_EXC) {
				$row['cart_free'] = $product['cart_free']; // AGG
				// Here taxes are computed only once the quantity has been applied to the product price
                // AGG TUTTO IF-ELSE
                // PRIMA C'ERA SOLO $price = Product::getPriceStatic((int)$product['id_product'], false, (int)$product['id_product_attribute'], 2, NULL, false, true, $product['cart_quantity'], false, (int)$this->id_customer ? (int)$this->id_customer : NULL, (int)$this->id, ($this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}));
                if($product['cart_price'] == 0 && $product['cart_free'] == 0) {
					$id_group = Customer::getDefaultGroupId($this->id_customer);
					if($id_group == 0) {
						$id_group = 1;
					}
					
					$quantity_now = Db::getInstance()->getValue("SELECT quantity FROM cart_product WHERE id_product = ".$product['id_product']." AND id_cart = ".$this->id."");
				
					$price = Db::getInstance()->getValue("SELECT price FROM "._DB_PREFIX_."cart_product WHERE id_product = ".$product['id_product']." AND id_cart = ".$this->id."");
					
					if($price == 0) {
						$price = Product::trovaMigliorPrezzo($product['id_product'],$id_group,$quantity_now,'y',$user_login);
					}
					
					/*$price = Product::getPriceStatic((int)$product['id_product'], false, (int)$product['id_product_attribute'], 2, NULL, false, true, $product['cart_quantity'], false, (int)$this->id_customer ? (int)$this->id_customer : NULL, (int)$this->id, ($this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}));*/
				} 
				else {
					$price = $product['price'];
                }
                
				$total_ecotax = $product['ecotax'] * (int)$product['cart_quantity'];
				$total_price = $price * (int)$product['cart_quantity'];

				if ($with_taxes) {
					$total_price =  Tools::ps_round(($total_price - $total_ecotax) * (1 + (float)(Tax::getProductTaxRate((int)$product['id_product'], (int)$this->{Configuration::get('PS_TAX_ADDRESS_TYPE')})) / 100), 2);
					//$total_ecotax = $total_ecotax * (1 + Tax::getProductEcotaxRate((int)$this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}) / 100);
					//$total_price = Tools::ps_round($total_price + $total_ecotax, 2);
				}
			}
			else {
                // AGG TUTTO IF-ELSE
                // PRIMA C'ERA SOLO $price = Product::getPriceStatic((int)($product['id_product']), true, (int)($product['id_product_attribute']), 2, NULL, false, true, $product['cart_quantity'], false, ((int)($this->id_customer) ? (int)($this->id_customer) : NULL), (int)($this->id), ((int)($this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}) ? (int)($this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}) : NULL));
				if($product['cart_price'] == 0 && $product['cart_free'] == 0) {
					
					$id_group = Customer::getDefaultGroupId($this->id_customer);
					if($id_group == 0) {
						$id_group = 1;
					}
					
					$quantity_now = Db::getInstance()->getValue("
						SELECT quantity 
						FROM "._DB_PREFIX_."cart_product 
						WHERE id_product = ".$product['id_product']." 
							AND id_cart = ".$this->id."
					");
				
					$price = Db::getInstance()->getValue("
						SELECT price 
						FROM "._DB_PREFIX_."cart_product 
						WHERE id_product = ".$product['id_product']." 
							AND id_cart = ".$this->id."
					");
                    
                    // AGG
					if($price == 0) {
						$price = Product::trovaMigliorPrezzo($product['id_product'],$id_group,$quantity_now,'y',$user_login);
					}
					/*$price = Product::getPriceStatic((int)($product['id_product']), true, (int)($product['id_product_attribute']), 2, NULL, false, true, $product['cart_quantity'], false, ((int)($this->id_customer) ? (int)($this->id_customer) : NULL), (int)($this->id), ((int)($this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}) ? (int)($this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}) : NULL));*/
				}
				else {
					$price = $product['price'];
                }
                
				$total_price = Tools::ps_round($price, 2) * (int)($product['cart_quantity']);
				
				if (!$with_taxes)
					$total_price = Tools::ps_round($total_price / (1 + ((float)(Tax::getProductTaxRate((int)$product['id_product'], (int)$this->{Configuration::get('PS_TAX_ADDRESS_TYPE')})) / 100)), 2);
			}
			
			$total_tax +=  0;
			
			$order_total += $total_price;
			
			$txc = Tax::getProductTaxRate((int)$product['id_product'], (int)$this->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
			
			if($txc > 0)
				$wtxc = 1;
		}
		
		$order_total_products = $order_total;
        
        // AGG
		if (!in_array($type, array(Cart::BOTH_WITHOUT_SHIPPING, Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING, Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING))) {
			if($wtxc == 1)
				$shipping_fees = $this->getOrderShippingCost(null, (int)($with_taxes));
			else
				$shipping_fees = $this->getOrderShippingCost(null, 0);
		}
		else
			$shipping_fees = 0;
		
		if ($type == Cart::ONLY_DISCOUNTS)
			$order_total = 0;
		// Wrapping Fees
		$wrapping_fees = 0;
		$commissions = 0; // AGG
		if ($this->gift) {
			$wrapping_fees = (float)(Configuration::get('PS_GIFT_WRAPPING_PRICE'));
			if ($with_taxes) {
				$wrapping_fees_tax = new Tax((int)(Configuration::get('PS_GIFT_WRAPPING_TAX')));
				$wrapping_fees *= 1 + (((float)($wrapping_fees_tax->rate) / 100));
			}
			$wrapping_fees = Tools::convertPrice(Tools::ps_round($wrapping_fees, 2), Currency::getCurrencyInstance((int)($this->id_currency)));
		}
        if ($type != Cart::ONLY_PRODUCTS) 
        {
			$discounts = array();
			/* Firstly get all discounts, looking for a free shipping one (in order to substract shipping fees to the total amount) */
			if ($discountIds = $this->getDiscounts(true)) {
                foreach ($discountIds AS $id_discount) 
                {
					$discount = new Discount((int)($id_discount['id_discount']));
					if (Validate::isLoadedObject($discount)) {
						$discounts[] = $discount;
						if ($discount->id_discount_type == 3)
							foreach($products AS $product) {
								$categories = Discount::getCategories($discount->id);
								if(!empty($categories)) { // AGG SOLO IF (NON CONTENUTO)
									if (count($categories) AND Product::idIsOnCategoryId($product['id_product'], $categories)) {
										if ($type == Cart::ONLY_DISCOUNTS)
											$order_total -= $shipping_fees;
										$shipping_fees = 0;
										break;
									}
								}
								else { // AGG TUTTO ELSE - TOGLIERE
									$prodotticoncoupon = Discount::getProducts($discount->id);
									$index_products = array();
									foreach ($prodotticoncoupon AS $k => $row) {
										$index_products[] = $row['id_product'];
									}
									foreach ($products AS $product) {
										if (in_array($product['id_product'], $index_products)) {
											if ($type == Cart::ONLY_DISCOUNTS)
											$order_total -= $shipping_fees;
										$shipping_fees = 0;
										break;
										}
									}
								}
							}
					}
				}
				/* Secondly applying all vouchers to the correct amount */
				$shrunk = false;
				foreach ($discounts AS $discount)
					if ($discount->id_discount_type != 3) {
						$order_total -= Tools::ps_round((float)($discount->getValue(sizeof($discounts), $order_total_products, $shipping_fees, $this->id, (int)($with_taxes))), 2);
						if ($discount->id_discount_type == 2)
							if (in_array($discount->behavior_not_exhausted, array(1,2)))
								$shrunk = true;
					}

				$order_total_discount = 0;
				if ($shrunk AND $order_total < (-$wrapping_fees - $order_total_products - $shipping_fees))
					$order_total_discount = -$wrapping_fees - $order_total_products - $shipping_fees;
				else
					$order_total_discount = $order_total;
			}
		}
        
        // AGG
		if($type == Cart::SPLIT_PAYMENT)
			$order_total_split_payment = $order_total;
		
		if ($with_taxes) {
            // MOD
			$order_total = $order_total + $total_tax;  
		}
        
        // AGG
		if($type == Cart::SPLIT_PAYMENT) {
			$order_total = $order_total_split_payment;
			$tax_rate_spl = Db::getInstance()->getValue('SELECT rate FROM '._DB_PREFIX_.'tax WHERE id_tax = 1');
			$tax_rate_split = (100+$tax_rate_spl)/100;
			$shipping_fees = $shipping_fees / $tax_rate_split;
			$wrapping_fees = $wrapping_fees / $tax_rate_split;
        }
        // AGG ONLY_COMMISSIONS E SPLIT_PAYMENT
		if ($type == Cart::ONLY_COMMISSIONS) return $commissions;
		if ($type == Cart::ONLY_SHIPPING) return $shipping_fees;
		if ($type == Cart::ONLY_WRAPPING) return $wrapping_fees;
		if ($type == Cart::SPLIT_PAYMENT) $order_total += $shipping_fees + $wrapping_fees;
		if ($type == Cart::BOTH) $order_total += $shipping_fees + $wrapping_fees;
		if ($order_total < 0 AND $type != Cart::ONLY_DISCOUNTS) return 0;
		if ($type == Cart::ONLY_DISCOUNTS AND isset($order_total_discount))
			return Tools::ps_round((float)($order_total_discount), 2);
		return Tools::ps_round((float)($order_total), 2);
    }
    
    // da correggere
    // Aggiunti SPLIT_PAYMENT e ONLY_COMMISSIONS
    public function getOrderTotalNUOVA($with_taxes = true, $type = Cart::BOTH, $products = null, $id_carrier = null, $use_cache = true)
    {
        // Dependencies
        $address_factory    = Adapter_ServiceLocator::get('Adapter_AddressFactory');
        $price_calculator    = Adapter_ServiceLocator::get('Adapter_ProductPriceCalculator');
        $configuration        = Adapter_ServiceLocator::get('Core_Business_ConfigurationInterface');

        $ps_tax_address_type = $configuration->get('PS_TAX_ADDRESS_TYPE');
        $ps_use_ecotax = $configuration->get('PS_USE_ECOTAX');
        $ps_round_type = $configuration->get('PS_ROUND_TYPE');
        $ps_ecotax_tax_rules_group_id = $configuration->get('PS_ECOTAX_TAX_RULES_GROUP_ID');
        $compute_precision = $configuration->get('_PS_PRICE_COMPUTE_PRECISION_');

        if (!$this->id) {
            return 0;
        }

        $type = (int)$type;
        $array_type = array(
            Cart::ONLY_PRODUCTS,
            Cart::ONLY_DISCOUNTS,
            Cart::BOTH,
            Cart::BOTH_WITHOUT_SHIPPING,
            Cart::ONLY_SHIPPING,
            Cart::ONLY_WRAPPING,
            Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING,
            Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING,
            Cart::SPLIT_PAYMENT,
            Cart::ONLY_COMMISSIONS,
        );

        // Define virtual context to prevent case where the cart is not the in the global context
        $virtual_context = Context::getContext()->cloneContext();
        $virtual_context->cart = $this;

        if (!in_array($type, $array_type)) {
            die(Tools::displayError());
        }

        $with_shipping = in_array($type, array(Cart::BOTH, Cart::ONLY_SHIPPING));

        // if cart rules are not used
        if ($type == Cart::ONLY_DISCOUNTS && !CartRule::isFeatureActive()) {
            return 0;
        }

        // no shipping cost if is a cart with only virtuals products
        $virtual = $this->isVirtualCart();
        if ($virtual && $type == Cart::ONLY_SHIPPING) {
            return 0;
        }

        if ($virtual && $type == Cart::BOTH) {
            $type = Cart::BOTH_WITHOUT_SHIPPING;
        }

        if ($with_shipping || $type == Cart::ONLY_DISCOUNTS) {
            if (is_null($products) && is_null($id_carrier)) {
                $shipping_fees = $this->getTotalShippingCost(null, (bool)$with_taxes);
            } else {
                $shipping_fees = $this->getPackageShippingCost((int)$id_carrier, (bool)$with_taxes, null, $products);
            }
        } else {
            $shipping_fees = 0;
        }

        if ($type == Cart::ONLY_SHIPPING) {
            return $shipping_fees;
        }

        if ($type == Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING) {
            $type = Cart::ONLY_PRODUCTS;
        }

        $param_product = true;
        if (is_null($products)) {
            $param_product = false;
            $products = $this->getProducts();
        }

        if ($type == Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING) {
            foreach ($products as $key => $product) {
                if ($product['is_virtual']) {
                    unset($products[$key]);
                }
            }
            $type = Cart::ONLY_PRODUCTS;
        }

        $order_total = 0;
        if (Tax::excludeTaxeOption()) {
            $with_taxes = false;
        }

        $products_total = array();
        $ecotax_total = 0;

        $total_tax = 0; // AGG
		$wtxc = 0; // AGG

        foreach ($products as $product) {
            // products refer to the cart details

            if ($virtual_context->shop->id != $product['id_shop']) {
                $virtual_context->shop = new Shop((int)$product['id_shop']);
            }

            if ($ps_tax_address_type == 'id_address_invoice') {
                $id_address = (int)$this->id_address_invoice;
            } else {
                $id_address = (int)$product['id_address_delivery'];
            } // Get delivery address of the product from the cart
            if (!$address_factory->addressExists($id_address)) {
                $id_address = null;
            }

            // The $null variable below is not used,
            // but it is necessary to pass it to getProductPrice because
            // it expects a reference.
            $null = null;
            $price = $price_calculator->getProductPrice(
                (int)$product['id_product'],
                $with_taxes,
                (int)$product['id_product_attribute'],
                6,
                null,
                false,
                true,
                $product['cart_quantity'],
                false,
                (int)$this->id_customer ? (int)$this->id_customer : null,
                (int)$this->id,
                $id_address,
                $null,
                $ps_use_ecotax,
                true,
                $virtual_context
            );

            $address = $address_factory->findOrCreate($id_address, true);

            if ($with_taxes) {
                $id_tax_rules_group = Product::getIdTaxRulesGroupByIdProduct((int)$product['id_product'], $virtual_context);
                $tax_calculator = TaxManagerFactory::getManager($address, $id_tax_rules_group)->getTaxCalculator();
            } else {
                $id_tax_rules_group = 0;
            }

            if (in_array($ps_round_type, array(Order::ROUND_ITEM, Order::ROUND_LINE))) {
                if (!isset($products_total[$id_tax_rules_group])) {
                    $products_total[$id_tax_rules_group] = 0;
                }
            } elseif (!isset($products_total[$id_tax_rules_group.'_'.$id_address])) {
                $products_total[$id_tax_rules_group.'_'.$id_address] = 0;
            }

            switch ($ps_round_type) {
                case Order::ROUND_TOTAL:
                    $products_total[$id_tax_rules_group.'_'.$id_address] += $price * (int)$product['cart_quantity'];
                    break;

                case Order::ROUND_LINE:
                    $product_price = $price * $product['cart_quantity'];
                    $products_total[$id_tax_rules_group] += Tools::ps_round($product_price, $compute_precision);
                    break;

                case Order::ROUND_ITEM:
                default:
                    $product_price = /*$with_taxes ? $tax_calculator->addTaxes($price) : */$price;
                    $products_total[$id_tax_rules_group] += Tools::ps_round($product_price, $compute_precision) * (int)$product['cart_quantity'];
                    break;
            }
        }

        foreach ($products_total as $key => $price) {
            $order_total += $price;
        }

        $order_total_products = $order_total;

        if ($type == Cart::ONLY_DISCOUNTS) {
            $order_total = 0;
        }

        // Wrapping Fees
        $wrapping_fees = 0;

        $commissions = 0; // AGG

        // With PS_ATCP_SHIPWRAP on the gift wrapping cost computation calls getOrderTotal with $type === Cart::ONLY_PRODUCTS, so the flag below prevents an infinite recursion.
        $include_gift_wrapping = (!$configuration->get('PS_ATCP_SHIPWRAP') || $type !== Cart::ONLY_PRODUCTS);

        if ($this->gift && $include_gift_wrapping) {
            $wrapping_fees = Tools::convertPrice(Tools::ps_round($this->getGiftWrappingPrice($with_taxes), $compute_precision), Currency::getCurrencyInstance((int)$this->id_currency));
        }
        if ($type == Cart::ONLY_WRAPPING) {
            return $wrapping_fees;
        }

        $order_total_discount = 0;
        $order_shipping_discount = 0;
        if (!in_array($type, array(Cart::ONLY_SHIPPING, Cart::ONLY_PRODUCTS)) && CartRule::isFeatureActive()) {
            // First, retrieve the cart rules associated to this "getOrderTotal"
            if ($with_shipping || $type == Cart::ONLY_DISCOUNTS) {
                $cart_rules = $this->getCartRules(CartRule::FILTER_ACTION_ALL);
            } else {
                $cart_rules = $this->getCartRules(CartRule::FILTER_ACTION_REDUCTION);
                // Cart Rules array are merged manually in order to avoid doubles
                foreach ($this->getCartRules(CartRule::FILTER_ACTION_GIFT) as $tmp_cart_rule) {
                    $flag = false;
                    foreach ($cart_rules as $cart_rule) {
                        if ($tmp_cart_rule['id_cart_rule'] == $cart_rule['id_cart_rule']) {
                            $flag = true;
                        }
                    }
                    if (!$flag) {
                        $cart_rules[] = $tmp_cart_rule;
                    }
                }
            }

            $id_address_delivery = 0;
            if (isset($products[0])) {
                $id_address_delivery = (is_null($products) ? $this->id_address_delivery : $products[0]['id_address_delivery']);
            }
            $package = array('id_carrier' => $id_carrier, 'id_address' => $id_address_delivery, 'products' => $products);

            // Then, calculate the contextual value for each one
            $flag = false;
            foreach ($cart_rules as $cart_rule) {
                // If the cart rule offers free shipping, add the shipping cost
                if (($with_shipping || $type == Cart::ONLY_DISCOUNTS) && $cart_rule['obj']->free_shipping && !$flag) {
                    $order_shipping_discount = (float)Tools::ps_round($cart_rule['obj']->getContextualValue($with_taxes, $virtual_context, CartRule::FILTER_ACTION_SHIPPING, ($param_product ? $package : null), $use_cache), $compute_precision);
                    $flag = true;
                }

                // If the cart rule is a free gift, then add the free gift value only if the gift is in this package
                if ((int)$cart_rule['obj']->gift_product) {
                    $in_order = false;
                    if (is_null($products)) {
                        $in_order = true;
                    } else {
                        foreach ($products as $product) {
                            if ($cart_rule['obj']->gift_product == $product['id_product'] && $cart_rule['obj']->gift_product_attribute == $product['id_product_attribute']) {
                                $in_order = true;
                            }
                        }
                    }

                    if ($in_order) {
                        $order_total_discount += $cart_rule['obj']->getContextualValue($with_taxes, $virtual_context, CartRule::FILTER_ACTION_GIFT, $package, $use_cache);
                    }
                }

                // If the cart rule offers a reduction, the amount is prorated (with the products in the package)
                if ($cart_rule['obj']->reduction_percent > 0 || $cart_rule['obj']->reduction_amount > 0) {
                    $order_total_discount += Tools::ps_round($cart_rule['obj']->getContextualValue($with_taxes, $virtual_context, CartRule::FILTER_ACTION_REDUCTION, $package, $use_cache), $compute_precision);
                }
            }
            $order_total_discount = min(Tools::ps_round($order_total_discount, 2), (float)$order_total_products) + (float)$order_shipping_discount;
            $order_total -= $order_total_discount;
        }

        if ($type == Cart::BOTH) {
            $order_total += $shipping_fees + $wrapping_fees;
        }

        if ($order_total < 0 && $type != Cart::ONLY_DISCOUNTS) {
            return 0;
        }

        if ($type == Cart::ONLY_DISCOUNTS) {
            return $order_total_discount;
        }

        return Tools::ps_round((float)$order_total, $compute_precision);

        // ------------------ VECCHIOOOOOOOOOOOOOOOOOOOOOO ------------------
        
        // AGG
        if($type == Cart::SPLIT_PAYMENT)
            $order_total_split_payment = $order_total;
        
        // MOD
        if ($with_taxes) {
            $order_total = $order_total + $total_tax;
        }
        
        // AGG
        // controllare l'ordine delle cose, se setto var negli if e devo spostare qualcosa
        if($type == Cart::SPLIT_PAYMENT) {
            $order_total = $order_total_split_payment;
            $tax_rate_spl = Db::getInstance()->getValue('SELECT rate FROM '._DB_PREFIX_.'tax WHERE id_tax = 1');
            $tax_rate_split = (100+$tax_rate_spl)/100;
            $shipping_fees = $shipping_fees / $tax_rate_split;
            $wrapping_fees = $wrapping_fees / $tax_rate_split;
            // prima era in un altro if:
            $order_total += $shipping_fees + $wrapping_fees;
        }

        if ($type == Cart::ONLY_COMMISSIONS) 
            return $commissions;
    }

	public function checkQuantities($return_product = false)
	{
		//$product['quantity'] è la quantità attualmente nel carrello ;-)
		foreach ($this->getProducts() AS $product) {
		
			$quantita_disponibile = Db::getInstance()->getValue('
				SELECT quantity 
				FROM '._DB_PREFIX_.'product 
				WHERE id_product = '.$product['id_product'].'
			');
		
			if ($product['quantity'] <= $quantita_disponibile) {}
			else {
				if ($product['quantity'] > $quantita_disponibile) {
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	* Return useful informations for cart
	*
	* @return array Cart details
	*/
	function getSummaryDetails($id_lang = NULL, $refresh = false)
	{		
		$context = Context::getContext();
		$delivery = new Address((int)($this->id_address_delivery));
		$invoice = new Address((int)($this->id_address_invoice));

		// New layout system with personalization fields
		$formattedAddresses['invoice'] = AddressFormat::getFormattedLayoutData($invoice);
		$formattedAddresses['delivery'] = AddressFormat::getFormattedLayoutData($delivery);

		$total_tax = $this->getOrderTotal() - $this->getOrderTotal(false);

		if ($total_tax < 0)
			$total_tax = 0;

		$total_free_ship = 0;
		if ($free_ship = Tools::convertPrice((float)(Configuration::get('PS_SHIPPING_FREE_PRICE')), new Currency((int)($this->id_currency)))) {
			$discounts = $this->getDiscounts();
			$total_free_ship =  $free_ship - ($this->getOrderTotal(true, Cart::ONLY_PRODUCTS) + $this->getOrderTotal(true, Cart::ONLY_DISCOUNTS));
			foreach ($discounts as $discount)
				if ($discount['id_discount_type'] == 3) {
					$total_free_ship = 0;
					break;
				}
		}
		
		return array(
			'delivery' => $delivery,
			'delivery_state' => State::getNameById($delivery->id_state),
			'invoice' => $invoice,
			'invoice_state' => State::getNameById($invoice->id_state),
			'formattedAddresses' => $formattedAddresses,
			'carrier' => new Carrier((int)($this->id_carrier), $context->language->id),
			'products' => $this->getProducts(false),
			'discounts' => $this->getDiscounts(false, true),
			'is_virtual_cart' => (int)$this->isVirtualCart(),
			'total_discounts' => $this->getOrderTotal(true, Cart::ONLY_DISCOUNTS),
			'total_discounts_tax_exc' => $this->getOrderTotal(false, Cart::ONLY_DISCOUNTS),
			'total_wrapping' => $this->getOrderTotal(true, Cart::ONLY_WRAPPING),
			'total_wrapping_tax_exc' => $this->getOrderTotal(false, Cart::ONLY_WRAPPING),
			'total_commissions' => $this->getOrderTotal(true, Cart::ONLY_COMMISSIONS),
			'total_commissions_tax_exc' => $this->getOrderTotal(false, Cart::ONLY_COMMISSIONS),
			'total_shipping' => $this->getOrderShippingCost(),
			'total_shipping_tax_exc' => $this->getOrderShippingCost(NULL, false),
			'total_products_wt' => $this->getOrderTotal(true, Cart::ONLY_PRODUCTS),
			'total_products' => $this->getOrderTotal(false, Cart::ONLY_PRODUCTS),
			'total_price' => $this->getOrderTotal(),
			'total_tax' => $total_tax,
			'total_price_without_tax' => $this->getOrderTotal(false),
			'free_ship' => $total_free_ship
		);
	}
	
	/**
	 * Update product quantity
	 *
	 * @param integer $quantity Quantity to add (or substract)
	 * @param integer $id_product Product ID
	 * @param integer $id_product_attribute Attribute ID if needed
	 * @param string $operator Indicate if quantity must be increased or decreased
	 */
	public function updateQty($quantity, $id_product, $id_product_attribute = NULL, $id_customization = false, 
		$operator = 'up', $id_address_delivery = 0, Shop $shop = NULL, $auto_add_cart_rule = true)
	{
		$context = Context::getContext();

		$cart_by_ezdirect = Db::getInstance()->getValue("
			SELECT id_cart 
			FROM carrelli_creati 
			WHERE id_cart = ".$this->id."
		");
		
		$cart_by_ezdirect = Db::getInstance()->getValue("
			SELECT id_cart 
			FROM "._DB_PREFIX_."cart 
			WHERE id_cart = ".$this->id." 
				AND (id_employee != 0 OR created_by != 0 OR in_carico_a != 0)
		");
		
		if($cart_by_ezdirect > 0) {
			
			$id_carrier_default = (int)(Configuration::get('PS_CARRIER_DEFAULT'));
			$context->carrier->id = $id_carrier_default;
			
			$last_cart = Db::getInstance()->getValue('
				SELECT id_cart 
				FROM '._DB_PREFIX_.'cart 
				ORDER BY id_cart DESC 
				LIMIT 1
			');

			$last_cart++;
			
			Db::getInstance()->executeS("
				INSERT INTO cart (id_cart, id_lang, id_carrier, preventivo, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, date_add, date_upd, rif_prev, name, riferimento, esigenze, risorse, note, note_private, validita, consegna, transport, payment, attachment, created_by, id_employee, in_carico_a, notifiche, email_preparate, numero_notifiche, data_ultima_notifica, id_persona, provvisorio, visualizzato) 
				SELECT ".$last_cart.", id_lang, 0, 0, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 1 FROM "._DB_PREFIX_."cart WHERE id_cart = ".$this->id."
			");
			
			$new_id = $last_cart;
			
			Db::getInstance()->executeS("
				UPDATE "._DB_PREFIX_."cart 
				SET id_carrier = '".$id_carrier_default."', date_add = '".date('Y-m-d H:i:s')."' 
				WHERE id_cart = '".$new_id."'
			");
			
			$context->cart->id = $new_id;
			
			$this->id = $new_id;
		}
		
		$this_price = Db::getInstance()->getRow('
			SELECT price, free 
			FROM '._DB_PREFIX_.'cart_product 
			WHERE id_cart = '.$this->id.' 
				AND id_product = '.$id_product
		);	
			
		if($this_price['free'] == 1)
			die();
		
		$product = new Product((int)$id_product, false, (int)Configuration::get('PS_LANG_DEFAULT'));

		/* If we have a product combination, the minimal quantity is set with the one of this combination */
		if (!empty($id_product_attribute))
			$minimalQuantity = (int)Attribute::getAttributeMinimalQty((int)$id_product_attribute);
		else
			$minimalQuantity = (int)$product->minimal_quantity;

		if (!Validate::isLoadedObject($product))
			die(Tools::displayError());
		if (isset(self::$_nbProducts[$this->id]))
			unset(self::$_nbProducts[$this->id]);
		if (isset(self::$_totalWeight[$this->id]))
			unset(self::$_totalWeight[$this->id]);
		if ((int)$quantity <= 0)
			return $this->deleteProduct((int)$id_product, (int)$id_product_attribute, (int)$id_customization);
		else if (!$product->available_for_order OR Configuration::get('PS_CATALOG_MODE'))
			return false;
		else {
			/* Check if the product is already in the cart */
			$result = $this->containsProduct((int)$id_product, (int)$id_product_attribute, (int)$id_customization);

			/* Update quantity if product already exist */
			if ($result) {
				if ($operator == 'up') {
					$result2 = Db::getInstance()->getRow('
						SELECT '.(!empty($id_product_attribute) ? 'pa' : 'p').'.`quantity`, p.`out_of_stock`
						FROM `'._DB_PREFIX_.'product` p
						'.(!empty($id_product_attribute) ? 'LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON p.`id_product` = pa.`id_product`' : '').'
						WHERE p.`id_product` = '.(int)($id_product).
						(!empty($id_product_attribute) ? ' AND `id_product_attribute` = '.(int)$id_product_attribute : ''));
					$productQty = (int)$result2['quantity'];
					$newQty = (int)$result['quantity'] + (int)$quantity;
					$qty = '+ '.(int)$quantity;

					if (!Product::isAvailableWhenOutOfStock((int)$result2['out_of_stock']))
						if ($newQty > $productQty)
							return false;
				}
				elseif ($operator == 'down') {
					$qty = '- '.(int)$quantity;
					$newQty = (int)$result['quantity'] - (int)$quantity;
					if ($newQty < $minimalQuantity AND $minimalQuantity > 1)
						return -1;
				}
				else
					return false;

				/* Delete product from cart */
				if ($newQty <= 0) {
					$codice_omaggio = Db::getInstance()->getValue("SELECT id_product FROM "._DB_PREFIX_."product WHERE reference = 'OMAGGIO'");
					//CONTROLLO PER BUNDLE
					$eBundle = Db::getInstance()->getValue("SELECT bundle FROM '"._DB_PREFIX_."cart_product WHERE id_product = ".$id_product." AND id_cart = ".$this->id.""); 

					if($eBundle > 0 && ($eBundle != 88888878 && $eBundle != 88888877 && $eBundle != 999999)) {
								
						Db::getInstance()->executeS("UPDATE "._DB_PREFIX_."cart_product SET bundle = 0, price = 0, free = 0 WHERE id_cart = ".$this->id.""); 
						$this->deleteProduct($codice_omaggio, 0, 0, 0);
								
					}			
					//FINE CONTROLLO PER BUNDLE
				
					//CONTROLLO PER COUPON CHEAPNET
					/*
					$conto_iniziale = Db::getInstance()->getValue("SELECT COUNT(usato) FROM "._DB_PREFIX_."cheapnetcoupons WHERE usato = 0");
					if($conto_iniziale == 0) {
					}
					else {
						
						$prodotti_con_coupon = Db::getInstance()->getValue("SELECT value FROM "._DB_PREFIX_."configuration WHERE name = 'PS_CHEAPNET_COUPONS'");
		
						$prodotti = explode("-", $prodotti_con_coupon);

						$prodotti_da_6 = explode(";", $prodotti[0]);
						$prodotti_da_30 = explode(";", $prodotti[1]);
						
						if(in_array($id_product, $prodotti_da_6)) {
							$this->deleteProduct(31110, 0, 0);
						}
						else {
						}
						
						if(in_array($id_product, $prodotti_da_30)) {
						
							$this->deleteProduct(31109, 0, 0);
						}
						else {
						}
					}
					*/
					// FINE CONTROLLO PER COUPON CHEAPNET
					return $this->deleteProduct((int)$id_product, (int)$id_product_attribute, (int)$id_customization);
				}
				else if ($newQty < $minimalQuantity) {
					return -1;
				}
				else {
					Db::getInstance()->Execute('
					UPDATE `'._DB_PREFIX_.'cart_product`
					SET `quantity` = `quantity` '.$qty.', `date_add` = NOW()
					WHERE `id_product` = '.(int)$id_product.
					(!empty($id_product_attribute) ? ' AND `id_product_attribute` = '.(int)$id_product_attribute : '').'
					AND `id_cart` = '.(int)$this->id.'
					LIMIT 1');
				}
			}

			/* Add product to the cart */
			else {
				$result2 = Db::getInstance()->getRow('
				SELECT '.(!empty($id_product_attribute) ? 'pa' : 'p').'.`quantity`, p.`out_of_stock`
				FROM `'._DB_PREFIX_.'product` p
				'.(!empty($id_product_attribute) ? 'LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON p.`id_product` = pa.`id_product`' : '').'
				WHERE p.`id_product` = '.(int)$id_product.
				(!empty($id_product_attribute) ? ' AND `id_product_attribute` = '.(int)$id_product_attribute : ''));

				/*if (!Product::isAvailableWhenOutOfStock((int)$result2['out_of_stock']))
					if ((int)$quantity > $result2['quantity'])
						return false;*/

				if ((int)$quantity < $minimalQuantity)
					return -1;

				if (!Db::getInstance()->AutoExecute(_DB_PREFIX_.'cart_product', array('id_product' => (int)$id_product,
				'id_product_attribute' => (int)$id_product_attribute, 'id_cart' => (int)$this->id,
				'quantity' => (int)$quantity, 'date_add' => date('Y-m-d H:i:s')), 'INSERT'))
					return false;
			}
		}
		// refresh cache of self::_products
		$this->_products = $this->getProducts(true);
		$this->update(true);

		if ($product->customizable)
			return $this->_updateCustomizationQuantity((int)$quantity, (int)$id_customization, (int)$id_product, (int)$id_product_attribute, $operator);
		else
			return true;
	}
	

	public function getOrderTotal($withTaxes = true, $type = Cart::BOTH, $user_login = false)
	{
		ini_set('memory_limit', '-1');
		if (!$this->id)
			return 0;
		$type = (int)($type);
		if (!in_array($type, array(
				Cart::ONLY_PRODUCTS,
				Cart::ONLY_DISCOUNTS,
				Cart::BOTH,
				Cart::BOTH_WITHOUT_SHIPPING,
				Cart::ONLY_SHIPPING,
				Cart::ONLY_WRAPPING,
				Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING,
				Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING,
				Cart::SPLIT_PAYMENT,
				Cart::ONLY_COMMISSIONS,
			)
		))
			die(Tools::displayError());

		// no shipping cost if is a cart with only virtuals products
		$virtual = $this->isVirtualCart();
		if ($virtual AND $type == Cart::ONLY_SHIPPING)
			return 0;
		if ($virtual AND $type == Cart::BOTH)
			$type = Cart::BOTH_WITHOUT_SHIPPING;

		if ($type == Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING)
			$type = Cart::ONLY_PRODUCTS;

		$products = $this->getProducts();
		
		if ($type == Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING) {
			foreach ($products as $key => $product)
				if (ProductDownload::getIdFromIdProduct($product['id_product']))
					unset($products[$key]);
			$type = Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING;
		}
		
		$order_total = 0;
		$total_tax = 0;
		
		$wtxc = 0;
		
		if (Tax::excludeTaxeOption())
			$withTaxes = false;
		foreach ($products AS $product) {
			if ($this->_taxCalculationMethod == PS_TAX_EXC) {
				$row['cart_free'] = $product['cart_free'];
				// Here taxes are computed only once the quantity has been applied to the product price
				if($product['cart_price'] == 0 && $product['cart_free'] == 0) {
					$id_group = Customer::getDefaultGroupId($this->id_customer);
					if($id_group == 0) {
						$id_group = 1;
					}
					
					$quantity_now = Db::getInstance()->getValue("SELECT quantity FROM "._DB_PREFIX_."cart_product WHERE id_product = ".$product['id_product']." AND id_cart = ".$this->id."");
				
					$price = Db::getInstance()->getValue("SELECT price FROM "._DB_PREFIX_."cart_product WHERE id_product = ".$product['id_product']." AND id_cart = ".$this->id."");
					
					if($price == 0) {
						$price = Product::trovaMigliorPrezzo($product['id_product'],$id_group,$quantity_now,'y',$user_login);
					}
					
					/*$price = Product::getPriceStatic((int)$product['id_product'], false, (int)$product['id_product_attribute'], 2, NULL, false, true, $product['cart_quantity'], false, (int)$this->id_customer ? (int)$this->id_customer : NULL, (int)$this->id, ($this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}));*/
				} 
				else {
					$price = $product['price'];
				}
				$total_ecotax = $product['ecotax'] * (int)$product['cart_quantity'];
				$total_price = $price * (int)$product['cart_quantity'];

				if ($withTaxes) {
					$total_price =  Tools::ps_round(($total_price - $total_ecotax) * (1 + (float)(Tax::getProductTaxRate((int)$product['id_product'], (int)$this->{Configuration::get('PS_TAX_ADDRESS_TYPE')})) / 100), 2);
					//$total_ecotax = $total_ecotax * (1 + Tax::getProductEcotaxRate((int)$this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}) / 100);
					//$total_price = Tools::ps_round($total_price + $total_ecotax, 2);
				}
			}
			else {
				if($product['cart_price'] == 0 && $product['cart_free'] == 0) {
					
					$id_group = Customer::getDefaultGroupId($this->id_customer);
					if($id_group == 0) {
						$id_group = 1;
					}
					
					$quantity_now = Db::getInstance()->getValue("
						SELECT quantity 
						FROM "._DB_PREFIX_."cart_product 
						WHERE id_product = ".$product['id_product']." 
							AND id_cart = ".$this->id."
					");
				
					$price = Db::getInstance()->getValue("
						SELECT price 
						FROM "._DB_PREFIX_."cart_product 
						WHERE id_product = ".$product['id_product']." 
							AND id_cart = ".$this->id."
					");
					
					if($price == 0) {
						$price = Product::trovaMigliorPrezzo($product['id_product'],$id_group,$quantity_now,'y',$user_login);
					}
					/*$price = Product::getPriceStatic((int)($product['id_product']), true, (int)($product['id_product_attribute']), 2, NULL, false, true, $product['cart_quantity'], false, ((int)($this->id_customer) ? (int)($this->id_customer) : NULL), (int)($this->id), ((int)($this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}) ? (int)($this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}) : NULL));*/
				}
				else {
					$price = $product['price'];
				}
				$total_price = Tools::ps_round($price, 2) * (int)($product['cart_quantity']);
				
				if (!$withTaxes)
					$total_price = Tools::ps_round($total_price / (1 + ((float)(Tax::getProductTaxRate((int)$product['id_product'], (int)$this->{Configuration::get('PS_TAX_ADDRESS_TYPE')})) / 100)), 2);
			}
			
			$total_tax +=  0;
			
			$order_total += $total_price;
			
			$txc = Tax::getProductTaxRate((int)$product['id_product'], (int)$this->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
			
			if($txc > 0)
				$wtxc = 1;
		}
		
		$order_total_products = $order_total;
		
		if (!in_array($type, array(Cart::BOTH_WITHOUT_SHIPPING, Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING, Cart::					ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING))) {
			if($wtxc == 1)
				$shipping_fees = $this->getOrderShippingCost(null, (int)($withTaxes));
			else
				$shipping_fees = $this->getOrderShippingCost(null, 0);
		}
		else
			$shipping_fees = 0;
		
		if ($type == Cart::ONLY_DISCOUNTS)
			$order_total = 0;
		// Wrapping Fees
		$wrapping_fees = 0;
		$commissions = 0;
		if ($this->gift) {
			$wrapping_fees = (float)(Configuration::get('PS_GIFT_WRAPPING_PRICE'));
			if ($withTaxes) {
				$wrapping_fees_tax = new Tax((int)(Configuration::get('PS_GIFT_WRAPPING_TAX')));
				$wrapping_fees *= 1 + (((float)($wrapping_fees_tax->rate) / 100));
			}
			$wrapping_fees = Tools::convertPrice(Tools::ps_round($wrapping_fees, 2), Currency::getCurrencyInstance((int)($this->id_currency)));
		}
		if ($type != Cart::ONLY_PRODUCTS) {
			$discounts = array();
			/* Firstly get all discounts, looking for a free shipping one (in order to substract shipping fees to the total amount) */
			if ($discountIds = $this->getDiscounts(true)) {
				foreach ($discountIds AS $id_discount) {
					$discount = new Discount((int)($id_discount['id_discount']));
					if (Validate::isLoadedObject($discount)) {
						$discounts[] = $discount;
						if ($discount->id_discount_type == 3)
							foreach($products AS $product) {
								$categories = Discount::getCategories($discount->id);
								if(!empty($categories)) {
									if (count($categories) AND Product::idIsOnCategoryId($product['id_product'], $categories)) {
										if ($type == Cart::ONLY_DISCOUNTS)
											$order_total -= $shipping_fees;
										$shipping_fees = 0;
										break;
									}
								}
								else {
									$prodotticoncoupon = Discount::getProducts($discount->id);
									$index_products = array();
									foreach ($prodotticoncoupon AS $k => $row) {
										$index_products[] = $row['id_product'];
									}
									foreach ($products AS $product) {
										if (in_array($product['id_product'], $index_products)) {
											if ($type == Cart::ONLY_DISCOUNTS)
											$order_total -= $shipping_fees;
										$shipping_fees = 0;
										break;
										}
									}
								}
							}
					}
				}
				/* Secondly applying all vouchers to the correct amount */
				$shrunk = false;
				foreach ($discounts AS $discount)
					if ($discount->id_discount_type != 3) {
						$order_total -= Tools::ps_round((float)($discount->getValue(sizeof($discounts), $order_total_products, $shipping_fees, $this->id, (int)($withTaxes))), 2);
						if ($discount->id_discount_type == 2)
							if (in_array($discount->behavior_not_exhausted, array(1,2)))
								$shrunk = true;
					}

				$order_total_discount = 0;
				if ($shrunk AND $order_total < (-$wrapping_fees - $order_total_products - $shipping_fees))
					$order_total_discount = -$wrapping_fees - $order_total_products - $shipping_fees;
				else
					$order_total_discount = $order_total;
			}
		}
		
		if($type == Cart::SPLIT_PAYMENT)
			$order_total_split_payment = $order_total;
		
		if ($withTaxes) {
			$order_total = $order_total + $total_tax;  
		}
		
		if($type == Cart::SPLIT_PAYMENT) {
			$order_total = $order_total_split_payment;
			$tax_rate_spl = Db::getInstance()->getValue('SELECT rate FROM '._DB_PREFIX_.'tax WHERE id_tax = 1');
			$tax_rate_split = (100+$tax_rate_spl)/100;
			$shipping_fees = $shipping_fees / $tax_rate_split;
			$wrapping_fees = $wrapping_fees / $tax_rate_split;
		}
		if ($type == Cart::ONLY_COMMISSIONS) return $commissions;
		if ($type == Cart::ONLY_SHIPPING) return $shipping_fees;
		if ($type == Cart::ONLY_WRAPPING) return $wrapping_fees;
		if ($type == Cart::SPLIT_PAYMENT) $order_total += $shipping_fees + $wrapping_fees;
		if ($type == Cart::BOTH) $order_total += $shipping_fees + $wrapping_fees;
		if ($order_total < 0 AND $type != Cart::ONLY_DISCOUNTS) return 0;
		if ($type == Cart::ONLY_DISCOUNTS AND isset($order_total_discount))
			return Tools::ps_round((float)($order_total_discount), 2);
		return Tools::ps_round((float)($order_total), 2);
	}

	/**
	* Return shipping total
	*
	* @param integer $id_carrier Carrier ID (default : current carrier)
	* @return float Shipping total
	*/
	public function getOrderShippingCost($id_carrier = NULL, $useTax = true, Country $default_country = null, $product_list = null)
	{
		global $defaultCountry;

		if ($this->isVirtualCart())
			return 0;

		// Checking discounts in cart
		$products = $this->getProducts();
		$discounts = $this->getDiscounts(true);
		if ($discounts)
			foreach ($discounts AS $id_discount)
				if ($id_discount['id_discount_type'] == 3) {
					if ($id_discount['minimal'] > 0) {
						$total_cart = 0;

						$categories = Discount::getCategories((int)($id_discount['id_discount']));
						
						if(!empty($categories)) {
						
						if (sizeof($categories))
							foreach($products AS $product)
								if (Product::idIsOnCategoryId((int)($product['id_product']), $categories))
									$total_cart += $product['total_wt'];
						}
						else {
							$prodotticoncoupon = Discount::getProducts((int)($id_discount['id_discount']));
							$index_products = array();
							foreach ($prodotticoncoupon AS $k => $row) {
								$index_products[] = $row['id_product'];
							}
							foreach ($products AS $product) {
								if (in_array($product['id_product'], $index_products)) {
									$total_cart += $product['total_wt'];
								}
							}	
							
						}

						if ($total_cart >= $id_discount['minimal'])
							return 0;
					}
					else
						return 0;
				}

		// Order total in default currency without fees
		$order_total = $this->getOrderTotal(true, Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING);

		// Start with shipping cost at 0
		$shipping_cost = 0;

		// If no product added, return 0
		/*if ($order_total <= 0 AND !(int)(self::getNbProducts($this->id)))
			return $shipping_cost;
		*/
		// Get id zone
		if (isset($this->id_address_delivery)
			AND $this->id_address_delivery
			AND Customer::customerHasAddress($this->id_customer, $this->id_address_delivery))
			$id_zone = Address::getZoneById((int)($this->id_address_delivery));
		else {
			// This method can be called from the backend, and $defaultCountry won't be defined
			if (!Validate::isLoadedObject($defaultCountry))
				$defaultCountry = new Country(Configuration::get('PS_COUNTRY_DEFAULT'), Configuration::get('PS_LANG_DEFAULT'));
			$id_zone = (int)$defaultCountry->id_zone;
		}

		// If no carrier, select default one
		if (!$id_carrier)
			$id_carrier = $this->id_carrier;

		if ($id_carrier && !$this->isCarrierInRange($id_carrier, $id_zone))
			$id_carrier = '';

		if (empty($id_carrier) && (!$this->id_customer) && $this->isCarrierInRange(Configuration::get('PS_CARRIER_DEFAULT'), $id_zone))
				$id_carrier = (int)(Configuration::get('PS_CARRIER_DEFAULT'));

		if (empty($id_carrier)) {
			if ((int)($this->id_customer)) {
				$customer = new Customer((int)($this->id_customer));
				$result = Carrier::getCarriers((int)(Configuration::get('PS_LANG_DEFAULT')), true, false, (int)($id_zone), $customer->getGroups());
				unset($customer);
			}
			else
				$result = Carrier::getCarriers((int)(Configuration::get('PS_LANG_DEFAULT')), true, false, (int)($id_zone));

			foreach ($result AS $k => $row) {
				if ($row['id_carrier'] == Configuration::get('PS_CARRIER_DEFAULT'))
					continue;

				if (!isset(self::$_carriers[$row['id_carrier']]))
					self::$_carriers[$row['id_carrier']] = new Carrier((int)($row['id_carrier']));

				$carrier = self::$_carriers[$row['id_carrier']];

				// Get only carriers that are compliant with shipping method
				if (($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_WEIGHT AND $carrier->getMaxDeliveryPriceByWeight($id_zone) === false)
				OR ($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_PRICE AND $carrier->getMaxDeliveryPriceByPrice($id_zone) === false)) {
					unset($result[$k]);
					continue ;
				}

				// If out-of-range behavior carrier is set on "Desactivate carrier"
				if ($row['range_behavior']) {
					// Get only carriers that have a range compatible with cart
					if (($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_WEIGHT AND (!Carrier::checkDeliveryPriceByWeight($row['id_carrier'], $this->getTotalWeight(), $id_zone)))
					OR ($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_PRICE AND (!Carrier::checkDeliveryPriceByPrice($row['id_carrier'], $this->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING), $id_zone, (int)($this->id_currency))))) {
						unset($result[$k]);
						continue ;
					}
				}

				if ($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_WEIGHT)
					$shipping = $carrier->getDeliveryPriceByWeight($this->getTotalWeight(), $id_zone);
				else
					$shipping = $carrier->getDeliveryPriceByPrice($order_total, $id_zone, (int)($this->id_currency));

				if (!isset($minShippingPrice))
					$minShippingPrice = $shipping;

				if ($shipping <= $minShippingPrice && $shipping != 0)
				{
					$id_carrier = (int)($row['id_carrier']);
					$minShippingPrice = $shipping;
				}
			}
		}

		if (empty($id_carrier))
			$id_carrier = Configuration::get('PS_CARRIER_DEFAULT');

		if (!isset(self::$_carriers[$id_carrier]))
			self::$_carriers[$id_carrier] = new Carrier((int)($id_carrier), Configuration::get('PS_LANG_DEFAULT'));
		$carrier = new Carrier((int)($id_carrier), Configuration::get('PS_LANG_DEFAULT'));
		/*if (!Validate::isLoadedObject($carrier))
			die(Tools::displayError('Fatal error: "no default carrier"'));*/
		if (!$carrier->active)
			return $shipping_cost;
		
		$ex_rivenditore = Db::getInstance()->getValue('SELECT ex_rivenditore FROM '._DB_PREFIX_.'customer WHERE id_customer = '.$this->id_customer);
		$gruppo =  Db::getInstance()->getValue('SELECT id_default_group FROM '._DB_PREFIX_.'customer WHERE id_customer = '.$this->id_customer);
		$prodotti_carrello = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'cart_product WHERE id_cart = '.$this->id);
		$sconti_extra = false;
		
		foreach($prodotti_carrello as $prodotto) {
			if($prodotto['sconto_extra'] > 0)
			{
				$sconti_extra = true;
			}
		}
		// Free fees if free carrier
		if($ex_rivenditore == 0 && $gruppo != 3 && gruppo != 15 && gruppo != 22 && $sconti_extra == false) {
			if ($carrier->is_free == 1)
				return 0;
		}
		// Select carrier tax
		if ($useTax AND !Tax::excludeTaxeOption())
			 $carrierTax = Tax::getCarrierTaxRate((int)$carrier->id, (int)$this->{Configuration::get('PS_TAX_ADDRESS_TYPE')});

		$configuration = Configuration::getMultiple(array('PS_SHIPPING_FREE_PRICE', 'PS_SHIPPING_HANDLING', 'PS_SHIPPING_METHOD', 'PS_SHIPPING_FREE_WEIGHT'));
		// Free fees
		$free_fees_price = 0;
		if (isset($configuration['PS_SHIPPING_FREE_PRICE']))
			$free_fees_price = Tools::convertPrice((float)($configuration['PS_SHIPPING_FREE_PRICE']), Currency::getCurrencyInstance((int)($this->id_currency)));
		$orderTotalwithDiscounts = $this->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING);
		if ($orderTotalwithDiscounts >= (float)($free_fees_price) AND (float)($free_fees_price) > 0)
			return $shipping_cost;
		if (isset($configuration['PS_SHIPPING_FREE_WEIGHT']) AND $this->getTotalWeight() >= (float)($configuration['PS_SHIPPING_FREE_WEIGHT']) AND (float)($configuration['PS_SHIPPING_FREE_WEIGHT']) > 0)
			return $shipping_cost;

			// Get shipping cost using correct method
			if ($carrier->range_behavior) {
				// Get id zone
				if (
					isset($this->id_address_delivery)
					AND $this->id_address_delivery
					AND Customer::customerHasAddress($this->id_customer, $this->id_address_delivery)
				)
					$id_zone = Address::getZoneById((int)($this->id_address_delivery));
				else
					$id_zone = (int)$defaultCountry->id_zone;
				if (($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_WEIGHT AND (!Carrier::checkDeliveryPriceByWeight($carrier->id, $this->getTotalWeight(), $id_zone)))
						OR ($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_PRICE AND (!Carrier::checkDeliveryPriceByPrice($carrier->id, $this->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING), $id_zone, (int)($this->id_currency)))))
						$shipping_cost += 0;
				else {
						if ($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_WEIGHT)
							$shipping_cost += $carrier->getDeliveryPriceByWeight($this->getTotalWeight(), $id_zone);
						else // by price
							$shipping_cost += $carrier->getDeliveryPriceByPrice($order_total, $id_zone, (int)($this->id_currency));
				}
			}
			else {
				if ($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_WEIGHT)
					$shipping_cost += $carrier->getDeliveryPriceByWeight($this->getTotalWeight(), $id_zone);
				else
					$shipping_cost += $carrier->getDeliveryPriceByPrice($order_total, $id_zone, (int)($this->id_currency));

			}
		// Adding handling charges
		if (isset($configuration['PS_SHIPPING_HANDLING']) AND $carrier->shipping_handling)
			$shipping_cost += (float)($configuration['PS_SHIPPING_HANDLING']);

		$trasporto5percento = 0;
		// Additional Shipping Cost per product
		foreach($products AS $product)
		{
			$shipping_cost += $product['additional_shipping_cost'] * $product['cart_quantity'];
			
			if($product['id_tax_rules_group'] != 12)
			{
				$trasporto5percento = 1;
				break;
			}
			
		}
		$shipping_cost = Tools::convertPrice($shipping_cost, Currency::getCurrencyInstance((int)($this->id_currency)));

		//get external shipping cost from module
		if ($carrier->shipping_external) {
			$moduleName = $carrier->external_module_name;
			$module = Module::getInstanceByName($moduleName);

			if (Validate::isLoadedObject($module)) {
				if (array_key_exists('id_carrier', $module))
					$module->id_carrier = $carrier->id;
				if ($carrier->need_range)
					$shipping_cost = $module->getOrderShippingCost($this, $shipping_cost);
				else
					$shipping_cost = $module->getOrderShippingCostExternal($this);

				// Check if carrier is available
				if ($shipping_cost === false)
					return false;
			}
			else
				return false;
		}
		
		if($trasporto5percento == 0 && $useTax)
			$carrierTax = 5;
		
		// Apply tax
		if (isset($carrierTax) && $carrierTax != 0)
			$shipping_cost *= 1 + ($carrierTax / 100);
			
		
		
		$edited_transport = Db::getInstance()->getValue("SELECT transport FROM "._DB_PREFIX_."cart WHERE id_cart = ".$this->id."");
		$selected_carrier_bc = Db::getInstance()->getValue("SELECT id_carrier FROM "._DB_PREFIX_."cart WHERE id_cart = ".$this->id."");
		$transport_cost_r = explode(":",$edited_transport);
		$transport_cost = $transport_cost_r[1];
		
		if($transport_cost_r[0] == $selected_carrier_bc) {
			if($transport_cost != '' && $transport_cost != 0) {
				$shipping_cost = $transport_cost;
				
				if (isset($carrierTax) && $carrierTax != 0)
					$shipping_cost *= 1 + ($carrierTax / 100);
			}
			else {
			}
		}
		else {
		
		}
		return (float)(Tools::ps_round((float)($shipping_cost), 2));
	}

	public function getCartPDF($id_cart, $id_customer, $originale = 'n', $revisione = 0) 
	{
		$context = Context::getContext();
		
		if($context->customer->id > 0) {
			Db::getInstance()->execute('
				UPDATE '._DB_PREFIX_.'cart 
				SET visualizzato = 1 
				WHERE id_cart = '.$id_cart.'
			');

			Db::getInstance()->execute('
				UPDATE carrelli_creati 
				SET visualizzato = 1 
				WHERE id_cart = '.$id_cart.'
			');
			
			Customer::Storico($id_cart, 'C', 0, 26);
			
			$preventivo = Db::getInstance()->getValue('
				SELECT preventivo 
				FROM '._DB_PREFIX_.'cart 
				WHERE id_cart = '.$id_cart.'
			');

			if($preventivo == 1) {
				$cli = Db::getInstance()->getRow('
					SELECT * 
					FROM '._DB_PREFIX_.'customer 
					WHERE id_customer = '.$id_customer.'
				');

				$id_promemoria = Db::getInstance()->getValue('
					SELECT id_action 
					FROM action_thread 
					WHERE subject = "*** PROMEMORIA AUTOMATICO - TELEFONARE A '.($cli['is_company'] == 1 ? $cli['company'] : $cli['firstname']." ".$cli['lastname']).' *** "
				');

				Db::getInstance()->execute('
					UPDATE action_thread 
					SET status = "closed" 
					WHERE id_action = '.$id_promemoria.'
				');
			}			
		}
		
		$datiCliente = Db::getInstance()->getRow("
			SELECT c.*, c.tax_code as cf, c.vat_number as pi, a.*, s.iso_code AS provincia 
			FROM "._DB_PREFIX_."customer c 
			JOIN "._DB_PREFIX_."address a ON c.id_customer = a.id_customer 
			LEFT JOIN "._DB_PREFIX_."state s ON a.id_state = s.id_state 
			WHERE c.id_customer = ".$id_customer." 
				AND a.fatturazione = 1
				AND a.deleted = 0 
				AND a.active = 1
		");	
		
		$customer_group = Db::getInstance()->getValue('
			SELECT id_default_group 
			FROM '._DB_PREFIX_.'customer 
			WHERE id_customer = '.$id_customer
		);
		
		if($revisione > 0) {
			mysql_select_db(_DB_REV_NAME_);

			$datiCarrello = Db::getInstance()->getRow("
				SELECT * 
				FROM "._DB_PREFIX_."cart 
				WHERE id_cart = ".$id_cart." 
					AND id_customer = ".$id_customer." 
					AND id_revisione = ".$revisione."
			");	 

			$prodottiCarrello = Db::getInstance()->executeS("
				SELECT * 
				FROM "._DB_PREFIX_."cart_product 
				WHERE id_revisione = ".$revisione." 
				AND id_cart = ".$datiCarrello['id_cart']." 
				ORDER BY sort_order ASC
			");

			$tot_revisioni = Db::getInstance()->executeS('
				SELECT id_revisione 
				FROM '._DB_PREFIX_.'cart 
				WHERE id_cart = '.$datiCarrello['id_cart'].' 
				ORDER BY id_revisione ASC
			');

			$ord_rev = 0;
			foreach($tot_revisioni as $tot_rev) {
				if($revisione == $tot_rev['id_revisione'])
					$ord_revisione = $ord_rev;
					
				$ord_rev++;
			}
			mysql_select_db(_DB_NAME_);
		}
		else {
			$datiCarrello = Db::getInstance()->getRow("
				SELECT * 
				FROM "._DB_PREFIX_."cart 
				WHERE id_cart = ".$id_cart." 
					AND id_customer = ".$id_customer."
			");	
			
			$prodottiCarrello = Db::getInstance()->executeS("
				SELECT cp.*, p.reference,  p.id_manufacturer, pl.name as nome_prodotto, image.id_image 
				FROM "._DB_PREFIX_."cart_product cp 
				LEFT JOIN "._DB_PREFIX_."product p ON cp.id_product = p.id_product 
				LEFT JOIN (SELECT * FROM "._DB_PREFIX_."image WHERE cover = 1 group by id_product) image ON image.id_product = cp.id_product 
				LEFT JOIN "._DB_PREFIX_."product_lang pl ON cp.id_product = pl.id_product 
				WHERE pl.id_lang = ".$context->language->id." 
					AND cp.id_cart = ".$datiCarrello['id_cart']." 
				ORDER BY cp.sort_order ASC
			");

			if($originale == 'y' && $preventivo == 1) {
				$datiCarrello = Db::getInstance()->getRow("
					SELECT * 
					FROM carrelli_creati 
					WHERE id_cart = ".$id_cart." AND id_customer = ".$id_customer."
				");	 

				$prodottiCarrello = Db::getInstance()->executeS("
					SELECT cp.*, p.reference, p.id_manufacturer, pl.name as nome_prodotto, image.id_image 
					FROM carrelli_creati_prodotti cp 
					LEFT JOIN "._DB_PREFIX_."product p ON cp.id_product = p.id_product 
					LEFT JOIN (SELECT * FROM "._DB_PREFIX_."image WHERE cover = 1) image ON image.id_product = cp.id_product 
					LEFT JOIN "._DB_PREFIX_."product_lang pl ON cp.id_product = pl.id_product 
					WHERE pl.id_lang = ".$context->language->id." 
						AND cp.id_cart = ".$datiCarrello['id_cart']." 
					ORDER BY cp.sort_order ASC
				");
			}

			$ord_revisione = $datiCarrello['revisioni'];
		}
		
		$indirizzoSpedizione = Db::getInstance()->getRow("
			SELECT a.*, s.iso_code AS provincia 
			FROM "._DB_PREFIX_."address a 
			LEFT JOIN "._DB_PREFIX_."state s ON a.id_state = s.id_state 
			WHERE a.id_address = ".$datiCarrello['id_address_delivery']." 
		");	
		
		$id_country = Db::getInstance()->getValue("
			SELECT id_country 
			FROM "._DB_PREFIX_."address 
			WHERE id_address = ".$datiCarrello['id_address_delivery']."
		");
		
		$content = '<page footer="page">
		<page_header>
		</page_header> 
		<page_footer>
		</page_footer>
		<div id="contenitore" style="width:770px; height:1070px; font-family: Arial, Verdana, Sans-Serif; display:block; position:relative; margin:0 auto">

			<div style="width:770px">
				<img src="https://www.ezdirect.it/img/intestazione.jpg" alt="Ezdirect" title="Ezdirect"  style="display:block; margin:0px" />
				
			</div>
			<table style="width:770px; font-size:11px; display:block; margin:0 auto">
				<tr>
				<td style="width:385px">
				<strong style="font-size:16px">ezdirect srl - societ&agrave; con socio unico</strong><br />
				Via Nerino Garbuio snc - 54038 Montignoso (MS)<br />
				Tel: 0585 821163 - Fax: 0240708331<br />
				P.IVA-C.F. 01164670455<br />
				REA:118272 04-05-07 Reg.Imp.01164670455
				</td>
				<td style="width:385px">
				<strong style="font-size:16px">Banca d\'appoggio</strong><br />
				Banco Popolare, Ag. Montignoso<br />
				ABI: 05034 CAB: 69948 CIN:T<br />
				C/C 001454 SWIFT: BAPPIT21S44<br />
				IBAN: IT58T0503469948000000001454
				</td>
				</tr>
			</table>
			<br />
			<table style="width:770px; font-size:16px; display:block; margin:0 auto">
				<tr>
				<td style="width:385px">
				<strong>SPETT.LE</strong><br />
				<strong>'.($datiCliente['is_company'] == 1 ? $datiCliente['company'] : $datiCliente['firstname'].' '.$datiCliente['lastname']).'</strong><br />
				<strong>'.($datiCliente['is_company'] == 1 ? "PI: ".$datiCliente['pi']." - " : "").' CF: '.$datiCliente['cf'].'</strong>
				<br /><br />
				
				'.$datiCliente['address1'].'<br />'.$datiCliente['postcode'].' '.$datiCliente['city'].''.(empty($datiCliente['provincia']) ? '' :  ' ('.$datiCliente['provincia'].')').'
				</td>
				<td style="width:385px">
				'.($datiCarrello['id_address_delivery'] > 0 ? '<strong>DESTINAZIONE</strong><br />'.($indirizzoSpedizione['company'] == '' ? $indirizzoSpedizione['firstname'].' '.$indirizzoSpedizione['lastname'] : $indirizzoSpedizione['company']).'<br /><br /><br />'.$indirizzoSpedizione['address1'].'<br />'.$indirizzoSpedizione['postcode'].' '.$indirizzoSpedizione['city'].''.(empty($indirizzoSpedizione['provincia']) ? '' :  ' ('.$indirizzoSpedizione['provincia'].')').'' : '').'
				</td>
				</tr>
			</table>
			<h1 style="text-align:left; font-size:22px; font-weight:normal; display:block; width:210px; height:24px; background-color:#e6e6fa">
				<em>OFFERTA N. '.$datiCarrello['id_cart'].'-'.$ord_revisione.'</em>
			</h1>
			<table style="width:715px; border-top:1px solid black; border-bottom:1px solid black; border display:block; margin:0 auto">
				<tr style="font-size:9px">
				<td style="width:173px"><em>PARTITA IVA - CODICE FISCALE</em></td>
				<td style="width:180px"><em>DATA OFFERTA</em></td>
				<td style="width:181px"><em>VALIDITA\' OFFERTA</em></td>
				<td style="width:181px"><em>TEMPI DI CONSEGNA</em></td>
				
				</tr>
				<tr style="font-size:12px; font-weight:bold">
				<td style="width:173px">'.$datiCliente['pi'].' '.$datiCliente['cf'].'</td>
				<td style="width:180px">'.Tools::displayDate($datiCarrello['date_add'], $context->language->id, true).'</td>
				<td style="width:181px">'.($datiCarrello['validita'] != '0000-00-00 00:00:00' ? 'Fino al '.Tools::displayDate($datiCarrello['validita'], $context->language->id, true) : '-').'</td>
				<td style="width:181px">'.$datiCarrello['consegna'].'</td>
				</tr>
			</table>
			<table style="width:715px; border-bottom:1px solid black; display:block; margin:0 auto">
				<tr style="font-size:9px">
				<td style="width:359px"><em>OGGETTO</em></td>
				<td style="width:181px"><em>METODO DI PAGAMENTO</em></td>
				<td style="width:181px"><em></em></td>
				
				</tr>
				<tr style="font-size:12px; font-weight:bold">
				<td style="width:357px">'.$datiCarrello['name'].'</td>
				<td style="width:181px">'.$datiCarrello['payment'].'</td>
				<td style="width:181px"></td>
				</tr>
			</table>
			
			<br />';
			
			if(strip_tags($datiCarrello['premessa']) != '')
			{ 
				$content .= '<table cellspacing="0" style="width:700px; vertical-align:middle; margin:0 auto">
				<tr style="font-size:10px; height:30px; background-color:#ebebeb; width:646px; border-bottom:1px solid black; border-left:1px solid black;  border-right:1px solid black; color:#000000; font-weight:bold">
				
				<td  style="padding:2px; padding-top:12px; border-top:1px solid black; border-left:1px solid black; border-right:1px solid black; padding-bottom:12px; padding-left:45px; width:646px; font-size:15px ">PREMESSA</td>
				
				</tr>
				
				<tr style="font-size:10px; height:22px; width:646px;">
				<td style="font-size:12px; color:#000000; height:22px; width:646px; border:1px solid black ">'.$datiCarrello['premessa'].'</td></tr></table><br />';
			}	
			
			$importo = 0;
			$tax_regime = Db::getInstance()->getValue("
				SELECT tax_regime 
				FROM "._DB_PREFIX_."customer 
				WHERE id_customer = ".$id_customer."
			");

			if($tax_regime == 0) {
				$id_tax = 1;
			}
			else {
				$id_tax = $tax_regime;
			}

			$tax_rate = Db::getInstance()->getValue("
				SELECT rate 
				FROM "._DB_PREFIX_."tax 
				WHERE id_tax = ".$id_tax."
			");
			
			if($tax_regime == 1) {
				$tax_rate = 0;
			}
			
			$canoni = Db::getInstance()->getValue('
				SELECT count(id_product) 
				FROM '._DB_PREFIX_.'cart_product 
				WHERE id_cart = '.$id_cart.' 
					AND section = 1
			');

			$ezcloud = Db::getInstance()->getValue('
				SELECT * 
				FROM cart_ezcloud
				WHERE id_cart = '.$id_cart.'
			');
			
			if($canoni > 0)
				$sezione = 1;
			else
				$sezione = 0;
			
			if($ezcloud['interni'] > 0 || $ezcloud['linee'] > 0) {
				$risorse = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; RISORSE: ';
				if($ezcloud['interni'] > 0)
					$risorse .= $ezcloud['interni'].' INTERNI - ';
				if($ezcloud['linee'] > 0)
					$risorse .= $ezcloud['linee'].' LINEE';
			}
			
			if($canoni > 0) {
				$content .= '
				<table cellspacing="0" style="width:719px; vertical-align:middle; border-top:1px solid black;  border-left:1px solid black;  border-right:1px solid black;display:block; margin:0 auto">
					<tr style="font-size:10px; height:30px; background-color:#ebebeb; width:720px; border-bottom:1px solid black; border-left:1px solid black;  border-right:1px solid black; color:#000000; font-weight:bold">
					
					<td colspan="8" style="padding:2px; padding-top:12px; padding-bottom:12px; padding-left:45px; width:647px; ">SEZIONE '.($canoni > 0 ? '1: ' : '').'CANONI PERIODICI EZCLOUD'.$risorse.'</td>
					
					</tr>
					
					<tr style="font-size:10px; height:22px; width:720px; border:1px solid black; color:#656565; font-weight:bold">
					<td style="width:40px"></td>
					<td style="width:100px; padding-top:7px; padding-bottom:7px">CODICE ARTICOLO</td>
					<td style="width:296px">DESCRIZIONE ARTICOLO - ANNOTAZIONI</td>
					<td style="width:40px">UM</td>
					<td style="width:58px; text-align:right">INTERNI</td>
					<td style="width:58px; text-align:right">UNITARIO</td>
					<td style="width:58px; text-align:right">IMPORTO</td>
					<td style="width:41px; text-align:right">C.I.</td>
					</tr>
				</table>
				<table style="width:720px; border:1px solid black; display:block; margin:0 auto">';
				$totale_canoni = 0;
				$iva = 0;
				foreach ($prodottiCarrello as $prodotto) {
					if($prodotto['section'] == '1') {
						$tax_rate_p =  Tax::getProductTaxRate((int)$prodotto['id_product'], Configuration::get('PS_TAX_ADDRESS_TYPE'));
						
						$importoprodotto = $prodotto['price']*$prodotto['quantity'];
						
						$iva += $importoprodotto * ($tax_rate_p/100);
						
						$totale_canoni += $importoprodotto;
						$content .= '
						<tr style="font-size:10px;">
						<td style="width:40px"><img src="https://www.ezdirect.it/img/'.($prodotto['id_image'] > 0 ? 'p/'.$prodotto['id_product'].'-'.$prodotto['id_image'] : 'm/'.Db::getInstance()->getValue('SELECT id_manufacturer FROM '._DB_PREFIX_.'product WHERE id_product = '.$prodotto['id_product'])).'-small.jpg" alt="" style="width:30px;height:30px" /></td>
						<td style="width:100px">'.($revisione >= 0 ? Db::getInstance()->getValue('SELECT reference FROM '._DB_PREFIX_.'product WHERE id_product = '.$prodotto['id_product']) : $prodotto['reference']).'</td>
						<td style="width:295px">'.(empty($prodotto['name']) ? ($revisione >= 0 ? Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'product_lang WHERE id_lang = '.$context->language->id.' AND id_product = '.$prodotto['id_product']) : $prodotto['nome_prodotto']) : $prodotto['name']).'</td>
						<td style="width:40px;">N.</td>
						<td style="width:58px; text-align:right">'.$prodotto['quantity'].'</td>
						<td style="width:58px; text-align:right">'.number_format($prodotto['price'],2,",",".").'</td>
						<td style="width:58px; text-align:right">'.number_format($importoprodotto,2,",",".").'</td>
						<td style="width:42px; text-align:right">'.number_format($tax_rate_p,0,",",".").'</td>
						</tr>';
					}
				}

				$content .= '
				<tr style="font-size:10px; height:22px; width:720px; border:1px solid black; color:#000000; font-weight:bold">
					<td style="width:40px"></td>
					<td style="width:100px;"></td>
					<td style="width:296px">Totale</td>
					<td style="width:40px"></td>
					<td style="width:58px; text-align:right"></td>
					<td style="width:58px; text-align:right"></td>
					<td style="width:58px; text-align:right">'.number_format($totale_canoni,2,",",".").'</td>
					<td style="width:41px; text-align:right">'.number_format($tax_rate,0,",",".").'</td>
				</tr>
				';

				$content .= '</table><br />';
			}
			
			
			$content .= '
			<table cellspacing="0" style="width:719px; vertical-align:middle; border-top:1px solid black;  border-left:1px solid black;  border-right:1px solid black;display:block; margin:0 auto">
				<tr style="font-size:10px; height:30px; background-color:#ebebeb; width:720px; border-bottom:1px solid black; border-left:1px solid black;  border-right:1px solid black; color:#000000; font-weight:bold">
				
				<td colspan="8" style="padding:2px; padding-top:12px; padding-bottom:12px; padding-left:45px; width:647px; ">SEZIONE '.($canoni > 0 ? '2: ' : '').' PRODOTTI E SERVIZI IN C/VENDITA</td>
				
				</tr>
				
				<tr style="font-size:10px; height:22px; width:720px; border:1px solid black; color:#656565; font-weight:bold">
				<td style="width:40px"></td>
				<td style="width:100px; padding-top:7px; padding-bottom:7px">CODICE ARTICOLO</td>
				<td style="width:296px">DESCRIZIONE ARTICOLO - ANNOTAZIONI</td>
				<td style="width:40px">UM</td>
				<td style="width:58px; text-align:right">QUANTITA\'</td>
				<td style="width:58px; text-align:right">UNITARIO</td>
				<td style="width:58px; text-align:right">IMPORTO</td>
				<td style="width:41px; text-align:right">C.I.</td>
				</tr>
			</table>
			<table style="width:720px; border:1px solid black; display:block; margin:0 auto">';
			
			
			$totale_prodotti = 0;
			foreach ($prodottiCarrello as $prodotto) {
			
				if(!($prodotto['bundle'] == 999999 || $prodotto['bundle'] == 88888878)) {
					$importoprodotto = $prodotto['price']*$prodotto['quantity'];
					
					if($prodotto['section'] != '1') {
						$tax_rate_p =  Tax::getProductTaxRate((int)$prodotto['id_product'], Configuration::get('PS_TAX_ADDRESS_TYPE'));
						
						$iva += $importoprodotto * ($tax_rate_p/100);
						
						$totale_prodotti += $importoprodotto;
						$content .= '
						<tr style="font-size:10px;">
						<td style="width:40px"><img src="https://www.ezdirect.it/img/'.($prodotto['id_image'] > 0 ? 'p/'.$prodotto['id_product'].'-'.$prodotto['id_image'] : 'm/'.Db::getInstance()->getValue('SELECT id_manufacturer FROM '._DB_PREFIX_.'product WHERE id_product = '.$prodotto['id_product'])).'-small.jpg" alt="" style="width:30px;height:30px" /></td>
						<td style="width:100px">'.($revisione >= 0 ? Db::getInstance()->getValue('SELECT reference FROM '._DB_PREFIX_.'product WHERE id_product = '.$prodotto['id_product']) : $prodotto['reference']).'</td>
						<td style="width:295px">'.(empty($prodotto['name']) ? ($revisione >= 0 ? Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'product_lang WHERE id_lang = '.$context->language->id.' AND id_product = '.$prodotto['id_product']) : $prodotto['nome_prodotto']) : $prodotto['name']).'</td>
						<td style="width:40px;">N.</td>
						<td style="width:58px; text-align:right">'.$prodotto['quantity'].'</td>
						<td style="width:58px; text-align:right">'.number_format($prodotto['price'],2,",",".").'</td>
						<td style="width:58px; text-align:right">'.number_format($importoprodotto,2,",",".").'</td>
						<td style="width:42px; text-align:right">'.number_format($tax_rate_p,0,",",".").'</td>
						</tr>';
					}

					$importo = $importo + $importoprodotto;
				}
			}
			
			if($datiCarrello['id_carrier'] != 0) {
			
				$cart_ctrl = new Cart($datiCarrello['id_cart']);
				if($revisione > 0){
					mysql_select_db(_DB_REV_NAME_);
					$carrier_cart = Db::getInstance()->getValue("
						SELECT id_carrier 
						FROM "._DB_PREFIX_."cart 
						WHERE id_cart = ".$datiCarrello['id_cart']." AND id_revisione = ".$revisione."
					");
					
					$costo_trasporto_modificato_r = Db::getInstance()->getValue("
						SELECT transport 
						FROM "._DB_PREFIX_."cart 
						WHERE id_cart = ".$id_cart." AND id_revisione = ".$revisione."
					");
					
					mysql_select_db(_DB_NAME_);
				}
				else {
					$carrier_cart = Db::getInstance()->getValue("
						SELECT id_carrier
						FROM "._DB_PREFIX_."cart 
						WHERE id_cart = ".$datiCarrello['id_cart']."
					");
					$costo_trasporto_modificato_r = Db::getInstance()->getValue("
						SELECT transport 
						FROM "._DB_PREFIX_."cart 
						WHERE id_cart = ".$id_cart."
					");
				}
				$costo_trasporto_modificato_r = explode(":",$costo_trasporto_modificato_r);
				$costo_trasporto_modificato = $costo_trasporto_modificato_r[1];
				if($carrier_cart != 0) {
					if($costo_trasporto_modificato != "" && $costo_trasporto_modificato > 0) {
						$costo_spedizione = $costo_trasporto_modificato;
					}
					else {				
						$costo_spedizione = $cart_ctrl->getOrderShippingCost($datiCarrello['id_carrier'], false);
					}
				}
				else {
					$costo_spedizione = 0;
				}
				
				$content .= '
				<tr style="font-size:10px;">
				<td style="width:40px"></td>
				<td style="width:100px">TRASP</td>
				<td style="width:295px">Trasporti</td>
				<td style="width:40px;">N.</td>
				<td style="width:58px; text-align:right">1</td>
				<td style="width:58px; text-align:right">'.number_format($costo_spedizione,2,",",".").'</td>
				<td style="width:58px; text-align:right">'.number_format($costo_spedizione,2,",",".").'</td>
				<td style="width:42px; text-align:right">'.number_format($tax_rate,0,",",".").'</td>
				</tr>';
	
				$importo += $costo_spedizione; 
			
			}
			
			$id_country_sped = $indirizzoSpedizione['id_country'];
			
			if($id_country == 0)
				$id_country = 10;
			
			if($id_country_sped == 0)
				$id_country_sped = $id_country;
			
			if($id_country_sped == 10) {
				$iva = ($importo * $tax_rate)/100;
			}
			else {
				if($datiCliente['is_company'] == 1  || $datiCliente['is_company'] == 0 && $id_country_sped == 19)
					$iva = 0;
				else
					$iva = ($importo * $tax_rate)/100;
			}
			
			if($datiCliente['tax_regime'] == 1) {
				$iva = 0;
			}

			$carrellon = new Cart($id_cart);
			
			$iva = $carrellon->getOrderTotal() - $carrellon->getOrderTotal(false);
			
			$content .= '
				<tr style="font-size:10px; height:22px; width:720px; border:1px solid black; color:#000000;font-weight:bold">
					<td style="width:40px"></td>
					<td style="width:100px;"></td>
					<td style="width:296px; padding-top:14px; padding-bottom:14px; ">Totale</td>
					<td style="width:40px"></td>
					<td style="width:58px; text-align:right"></td>
					<td style="width:58px; text-align:right"></td>
					<td style="width:58px; padding-top:14px; padding-bottom:14px; text-align:right">'.number_format($totale_prodotti + $costo_spedizione,2,",",".").'</td>
					<td style="width:41px; padding-top:14px; padding-bottom:14px; text-align:right">'.number_format($tax_rate,0,",",".").'</td>
				</tr>';
			
			$content .= '
				</table>
				<br /><br />

				<table style="width:705px; text-align:right; border-top:1px solid black; border-bottom:1px solid black; display:block; margin:0 auto;">
					<tr style="font-size:9px">
					<td style="width:145px"><em>BOLLI</em></td>
					<td style="width:100px"><em>SPESE DI INCASSO</em></td>
					<td style="width:100px"><em>SPESE DI SPEDIZIONE</em></td>
					<td style="width:100px"><em>SPESE VARIE</em></td>
					<td style="width:100px"><em>SCONTO %</em></td>
					<td style="width:160px"><em>TOTALE MERCE SCONTATO</em></td>
					</tr>
					<tr style="font-size:11px; font-weight:bold">
					<td style="width:105px"></td>
					<td style="width:100px"></td>
					<td style="width:100px"></td>
					<td style="width:100px"></td>
					<td style="width:100px"></td>
					<td style="width:160px">'.number_format($importo,2,",",".").'</td>
					</tr>
				</table>

				<table style="width:705px; text-align:right; border-bottom:1px solid black; display:block; margin:0 auto; ">
					<tr style="font-size:9px">
					<td style="width:130px; text-align:left"><em>C.IVA</em></td>
					<td style="width:110px"><em>IMPONIBILE</em></td>
					<td style="width:200px"><em>% IVA - DESCRIZIONE IVA</em></td>
					<td style="width:110px"><em>IMPORTO IVA</em></td>
					<td style="width:160px"><em>TOTALE IMPONIBILE</em></td>
					</tr>
					<tr style="font-size:11px; font-weight:bold">
					<td style="width:130px; text-align:left">'.number_format($tax_rate,0,",",".").'</td>
					<td style="width:110px">'.number_format($importo,2,",",".").'</td>
					<td style="width:200px">'.number_format($tax_rate,0,",",".").'%</td>
					<td style="width:110px">'.number_format($iva,2,",",".").'<br /><br /><span style="font-size:9px; font-weight:normal"><em>(IVA dove applicabile)</em></span></td>
					<td style="width:160px">'.number_format($importo,2,",",".").'<br /><br />
					<span style="font-size:9px; font-weight:normal"><em>TOTALE IVA</em></span><br />
					'.number_format($iva,2,",","").'
					</td>
					</tr>
				</table>
					
				<table style="width:715px; text-align:right; display:block; margin:0 auto; ">
					<tr style="font-size:9px">
					<td style="width:215px; text-align:left"><em>A VISTA</em></td>
					<td style="width:272px"><em>IBAN PER BONIFICO BANCARIO</em></td>
					<td style="width:235px"><em>TOTALE DOCUMENTO</em></td>
					</tr>
					<tr style="font-size:11px; font-weight:bold">
					<td style="width:210px; text-align:left">'.number_format($importo+$iva,2,",",".").'</td>
					<td style="width:272px">IT58T0503469948000000001454</td>
					<td style="width:235px">'.number_format($importo+$iva,2,",",".").'</td>
					</tr>
				</table>
			';
			
			/*<strong style="font-size:16px">Offerta</strong><br />
			
			<table cellpadding="0" cellspacing="0" style="font-size:12px">
			<tr style="background-color:#f0f0f0">
			<td style="border:1px solid black; border-bottom:0px; padding:5px" colspan="3"><strong>Riepilogo offerta n. '.$datiCarrello['id_cart'].'</strong></td>
			</tr>
			<tr>
			<td style="border:1px solid black; width:230px; border-right:0px; padding:5px;">Offerta elaborata in data: '.Tools::displayDate($datiCarrello['date_add'], (int)($cookie->id_lang), true).'</td>
			<td style="border:1px solid black; width:230px; border-right:0px; padding:5px;">Validit&agrave; offerta: fino al '.Tools::displayDate($datiCarrello['validita'], (int)($cookie->id_lang), true).'</td>
			<td style="border:1px solid black; width:230px; padding:5px;">Tempi di consegna merce: '.$datiCarrello['consegna'].'</td>
			</tr>
			
			</table>
			<br />
			<strong style="font-size:16px">Prodotti</strong><br />
			
			<table cellpadding="0" cellspacing="0" style="border-collapse:collapse; font-size:12px">
			<tr style="background-color:#f0f0f0; border:1px solid black">
			<td style="padding:3px; width:140px; border-top:1px solid black; border-bottom:1px solid black;"><strong>Codice articolo</strong></td>
			<td style="padding:3px; width:395px; border-top:1px solid black; border-bottom:1px solid black;"><strong>Descrizione</strong></td>
			<td style="padding:3px; text-align:right; border-top:1px solid black; width:65px; border-bottom:1px solid black;"><strong>Prezzo</strong></td>
			<td style="padding:3px; text-align:right; border-top:1px solid black; width:30px; border-bottom:1px solid black;"><strong>Qt.</strong></td>
			<td style="padding:3px; text-align:right; border-top:1px solid black; width:65px; border-bottom:1px solid black;"><strong>Importo</strong></td>
			</tr>';
			
			$importo = 0;
			$tax_rate = Db::getInstance()->getValue("SELECT rate FROM "._DB_PREFIX_."tax WHERE id_tax = 1");
			
			foreach ($prodottiCarrello as $prodotto) {
			
				$importoprodotto = $prodotto['price']*$prodotto['quantity'];
			
				$content.='<tr style="border:1px solid black; border-bottom:1px">
				<td style="padding:3px; border-top:1px solid black; border-bottom:1px solid black;">'.$prodotto['reference'].'</td>
				<td style="padding:3px; border-top:1px solid black; border-bottom:1px solid black;">'.(empty($prodotto['name']) ? $prodotto['nome_prodotto'] : $prodotto['name']).'</td>
				<td style="padding:3px; border-top:1px solid black; border-bottom:1px solid black; text-align:right">'.number_format($prodotto['price'],2,",","").' &euro;</td>
				<td style="padding:3px; border-top:1px solid black; border-bottom:1px solid black; text-align:right;">'.$prodotto['quantity'].'</td>
				<td style="padding:3px; border-top:1px solid black; border-bottom:1px solid black; text-align:right;">'.number_format($importoprodotto,2,",","").' &euro;</td>
				</tr>';
				$importo = $importo + $importoprodotto;
			}
			
			$iva = ($importo * $tax_rate)/100;
			
			$content.= '</table>
			<br />
			<table cellpadding="0" cellspacing="0" style="border-collapse:collapse; width:770px; font-size:12px">
			<tr style="border:0px">
			<td style="padding:3px; width:140px"></td>
			<td style="padding:3px; width:395px;"><strong>TOT. PRODOTTI</strong></td>
			<td style="padding:3px; text-align:right; width:65px"></td>
			<td style="padding:3px; text-align:right; width:30px"></td>
			<td style="padding:3px; text-align:right; width:65px">'.number_format($importo,2,",","").' &euro;</td>
			</tr>
			<tr style="border:0px">
			<td style="padding:3px;"></td>
			<td style="padding:3px;"><strong>IVA</strong></td>
			<td style="padding:3px; text-align:right;"></td>
			<td style="padding:3px; text-align:right;"></td>
			<td style="padding:3px; text-align:right;">'.number_format($iva,2,",","").' &euro;</td>
			</tr>
			<tr style="border:0px">
			<td style="padding:3px;"></td>
			<td style="padding:3px;"><strong>TOT. PRODOTTI IVA INCLUSA</strong></td>
			<td style="padding:3px; text-align:right;"></td>
			<td style="padding:3px; text-align:right;"></td>
			<td style="padding:3px; text-align:right;">'.number_format($importo+$iva,2,",","").' &euro;</td>
			</tr>
			</table>
			
			<br />*/
		$content .= '	<br />
		<style type="text/css">
			#note p {
				margin-top:0px;
			}
		
		</style>
		<div id="note" style="border:0px; width:730px; padding:5px; font-size:11px">
			'.(empty($datiCarrello['esigenze']) ? '' : '<strong>Esigenze del cliente</strong><br />'. strip_tags($datiCarrello['esigenze']).'<br /><br />').'
			'.(empty($datiCarrello['risorse'])  ? '' : '<strong>Risorse</strong> <br />'. strip_tags($datiCarrello['risorse']).'<br /><br />').'
			
			<strong>Note</strong><br />
			'.(empty($datiCarrello['note']) ? ' - ' :  preg_replace("/<[\/]*div[^>]*>/i", "", $datiCarrello['note'])).'
			</div>
		
			
		';
		
		/*$noleggio = Db::getInstance()->getValue("
			SELECT id_cart 
			FROM "._DB_PREFIX_."noleggio 
			WHERE id_cart = ".$id_cart."
		");*/
		
		if($customer_group == 3 || $customer_group == 15)
			$noleggio = 0;
		
		if($noleggio > 0) {
			/*$datiNoleggio = Db::getInstance()->getRow("
				SELECT * 
				FROM "._DB_PREFIX_."noleggio 
				WHERE id_cart = ".$id_cart."
			");*/
			
			$rata = number_format(($importo*$datiNoleggio['parametri']/100),2);
			
			$content .= '
			<div id="proposta_di_noleggio" style="border:1px solid black; width:730px; padding:5px; font-size:11px; margin-top:10px">
			
			<strong>Proposta di noleggio (IVA esclusa)</strong>, valida per acquisti di apparecchiature hardware/elettronica su ordini di almeno 1.500 &euro; + iva <br />
			
			Contatta il tuo consulente Ezdirect al numero 0585 821163 per attivare la formula noleggio, da 18 a 60 mesi. La nostra guida sul noleggio: <strong style="color:blue; text-decoration:underline">https://www.ezdirect.it/guide/22-noleggio-centralini-telefonici-impianti-telefoni-voip-noleggiare</strong>. IMPORTO (IVA escl.) SPESE UNA TANTUM CADENZA RATA DURATA RISCATTO 13.586,00 100,00 Mensile 441,14 36 mesi 441,14<br /><br />
			<table style="width:686px; text-align:right;display:block; margin:0 auto; ">
			<tr style="font-size:9px">
			<td style="width:113px; text-align:left"></td>
			<td style="width:100px"><em>IMPORTO (IVA escl.)</em></td>
			<td style="width:110px"><em>SPESE UNA TANTUM</em></td>
			<td style="width:80px"><em>CADENZA</em></td>			
			<td style="width:80px"><em>RATA</em></td>				
			<td style="width:110px"><em>DURATA</em></td>
			<td style="width:100px"><em>RISCATTO</em></td>
			
			</tr>
			<tr style="font-size:11px; font-weight:bold">
			<td style="width:113px; text-align:left"></td>
			<td style="width:100px">'.number_format($importo,2,",",".").'</td>
			<td style="width:110px">'.number_format($datiNoleggio['spese_contratto'],2,",","").'</td>
			<td style="width:80px">'.($importo > 1500 && $importo < 2999 ? 'Trimestrale' : 'Mensile').'</td>
			<td style="width:80px">'.($importo > 1500 && $importo < 2999 ? number_format($rata*3,2,",","") : number_format($rata,2,",","")).'</td>
			<td style="width:110px">'.$datiNoleggio['mesi'].' mesi</td>
			<td style="width:100px">'.number_format($importo*$datiNoleggio['parametri']/100,2,",",".").'</td>
			
			</tr>
			</table></div>';
		}

		$content .= '</div></page>';
		
		return $content;
	
	}

	function getOrderShippingCostByTotalAndDelivery($id_carrier = NULL, $order_total, $address_delivery, $id_customer_group, $useTax = true)
	{		
		if($order_total == 0)
			$order_total = 1;
			
		global $defaultCountry;
		$tax_rate = Db::getInstance()->getValue("
			SELECT rate 
			FROM "._DB_PREFIX_."tax 
			WHERE id_tax = 1
		");

		$tax_rate = ($tax_rate/100)+1;
		
		$id_state = Db::getInstance()->getValue('
			SELECT id_state 
			FROM '._DB_PREFIX_.'address 
			WHERE id_address = '.$address_delivery.'
		');

		if($id_state == 0) {
			$id_country = Db::getInstance()->getValue('
				SELECT id_country 
				FROM '._DB_PREFIX_.'address 
				WHERE id_address = '.$address_delivery.'
			');

			$zone = Db::getInstance()->getValue('
				SELECT id_zone 
				FROM '._DB_PREFIX_.'country 
				WHERE id_country = '.$id_country.'
			');
		}
		else {
			$zone = Db::getInstance()->getValue('
				SELECT id_zone 
				FROM '._DB_PREFIX_.'state 
				WHERE id_state = '.$id_state
			);
		}
		
		/*if($id_customer_group != 3 && $id_customer_group != 12) {
					
			if($zone == 9) {
				if($order_total > (Configuration::get('FREE_SHIPPING_BOUNDARY')/$tax_rate)) {
					return 0;
				}
			}
			else {
			
			}
		}
		else {
		}*/
		
		if ($this->isVirtualCart())
			return 0;

		// Checking discounts in cart
		$products = $this->getProducts();
		$discounts = $this->getDiscounts(true);
		$this->id_address_delivery = $address_delivery;

		// Order total in default currency without fees
	
		// Start with shipping cost at 0
		$shipping_cost = 0;

		// If no product added, return 0
		if ($order_total <= 0 AND !(int)(self::getNbProducts($this->id)))
			return $shipping_cost;

		// Get id zone
		if (isset($this->id_address_delivery)
			AND $this->id_address_delivery
			AND Customer::customerHasAddress($this->id_customer, $this->id_address_delivery))
			$id_zone = Address::getZoneById((int)($this->id_address_delivery));
		else {
			// This method can be called from the backend, and $defaultCountry won't be defined
			if (!Validate::isLoadedObject($defaultCountry))
				$defaultCountry = new Country(Configuration::get('PS_COUNTRY_DEFAULT'), Configuration::get('PS_LANG_DEFAULT'));
			$id_zone = (int)$defaultCountry->id_zone;
		}

		// If no carrier, select default one
		if (!$id_carrier)
			$id_carrier = $this->id_carrier;

		if ($id_carrier && !$this->isCarrierInRange($id_carrier, $id_zone))
			$id_carrier = '';

		if (empty($id_carrier) && (!$this->id_customer) && $this->isCarrierInRange(Configuration::get('PS_CARRIER_DEFAULT'), $id_zone))
				$id_carrier = (int)(Configuration::get('PS_CARRIER_DEFAULT'));

		if (empty($id_carrier)) {
			if ((int)($this->id_customer))
			{
				$customer = new Customer((int)($this->id_customer));
				$result = Carrier::getCarriers((int)(Configuration::get('PS_LANG_DEFAULT')), true, false, (int)($id_zone), $customer->getGroups());
				unset($customer);
			}
			else
				$result = Carrier::getCarriers((int)(Configuration::get('PS_LANG_DEFAULT')), true, false, (int)($id_zone));

			foreach ($result AS $k => $row) {
				if ($row['id_carrier'] == Configuration::get('PS_CARRIER_DEFAULT'))
					continue;

				if (!isset(self::$_carriers[$row['id_carrier']]))
					self::$_carriers[$row['id_carrier']] = new Carrier((int)($row['id_carrier']));

				$carrier = self::$_carriers[$row['id_carrier']];

				// Get only carriers that are compliant with shipping method
				if (($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_WEIGHT AND $carrier->getMaxDeliveryPriceByWeight($id_zone) === false)
				OR ($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_PRICE AND $carrier->getMaxDeliveryPriceByPrice($id_zone) === false))
				{
					unset($result[$k]);
					continue ;
				}

				// If out-of-range behavior carrier is set on "Desactivate carrier"
				if ($row['range_behavior'])
				{
					// Get only carriers that have a range compatible with cart
					if (($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_WEIGHT AND (!Carrier::checkDeliveryPriceByWeight($row['id_carrier'], $this->getTotalWeight(), $id_zone)))
					OR ($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_PRICE AND (!Carrier::checkDeliveryPriceByPrice($row['id_carrier'], $this->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING), $id_zone, (int)($this->id_currency)))))
					{
						unset($result[$k]);
						continue ;
					}
				}

				if ($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_WEIGHT)
					$shipping = $carrier->getDeliveryPriceByWeight($this->getTotalWeight(), $id_zone);
				else
					$shipping = $carrier->getDeliveryPriceByPrice($order_total, $id_zone, (int)($this->id_currency));

				if (!isset($minShippingPrice))
					$minShippingPrice = $shipping;

				if ($shipping <= $minShippingPrice && $shipping != 0) {
					$id_carrier = (int)($row['id_carrier']);
					$minShippingPrice = $shipping;
				}
			}
		}

		if (empty($id_carrier))
			$id_carrier = Configuration::get('PS_CARRIER_DEFAULT');

		if (!isset(self::$_carriers[$id_carrier]))
			self::$_carriers[$id_carrier] = new Carrier((int)($id_carrier), Configuration::get('PS_LANG_DEFAULT'));
		$carrier = new Carrier((int)($id_carrier), Configuration::get('PS_LANG_DEFAULT'));
		
		/*if (!Validate::isLoadedObject($carrier))
			die(Tools::displayError('Fatal error: "no default carrier"'));*/
		if (!$carrier->active)
			return $shipping_cost;

		// Free fees if free carrier
		if ($carrier->is_free == 1)
			return 0;

		// Select carrier tax
		if ($useTax AND !Tax::excludeTaxeOption())
			 $carrierTax = Tax::getCarrierTaxRate((int)$carrier->id, (int)$this->{Configuration::get('PS_TAX_ADDRESS_TYPE')});

		$configuration = Configuration::getMultiple(array('PS_SHIPPING_FREE_PRICE', 'PS_SHIPPING_HANDLING', 'PS_SHIPPING_METHOD', 'PS_SHIPPING_FREE_WEIGHT'));
		// Free fees
		$free_fees_price = 0;
		if (isset($configuration['PS_SHIPPING_FREE_PRICE']))
			$free_fees_price = Tools::convertPrice((float)($configuration['PS_SHIPPING_FREE_PRICE']), Currency::getCurrencyInstance((int)($this->id_currency)));
		$orderTotalwithDiscounts = $this->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING);
		if ($orderTotalwithDiscounts >= (float)($free_fees_price) AND (float)($free_fees_price) > 0)
			return $shipping_cost;
		if (isset($configuration['PS_SHIPPING_FREE_WEIGHT']) AND $this->getTotalWeight() >= (float)($configuration['PS_SHIPPING_FREE_WEIGHT']) AND (float)($configuration['PS_SHIPPING_FREE_WEIGHT']) > 0)
			return $shipping_cost;

			// Get shipping cost using correct method
			if ($carrier->range_behavior) {
				// Get id zone
				if (isset($this->id_address_delivery) AND $this->id_address_delivery AND Customer::customerHasAddress($this->id_customer, $this->id_address_delivery))
					$id_zone = Address::getZoneById((int)($this->id_address_delivery));
				else
					$id_zone = (int)$defaultCountry->id_zone;
				if (($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_WEIGHT AND (!Carrier::checkDeliveryPriceByWeight($carrier->id, $this->getTotalWeight(), $id_zone)))
						OR ($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_PRICE AND (!Carrier::checkDeliveryPriceByPrice($carrier->id, $this->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING), $id_zone, (int)($this->id_currency)))))
						$shipping_cost += 0;
				else {
					if ($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_WEIGHT)
						$shipping_cost += $carrier->getDeliveryPriceByWeight($this->getTotalWeight(), $id_zone);
					else // by price
						$shipping_cost += $carrier->getDeliveryPriceByPrice($order_total, $id_zone, (int)($this->id_currency));
				}
			}
			else {
				if ($carrier->getShippingMethod() == Carrier::SHIPPING_METHOD_WEIGHT)
					$shipping_cost += $carrier->getDeliveryPriceByWeight($this->getTotalWeight(), $id_zone);
				else
					$shipping_cost += $carrier->getDeliveryPriceByPrice($order_total, $id_zone, (int)($this->id_currency));

			}
			
		// Adding handling charges
		if (isset($configuration['PS_SHIPPING_HANDLING']) AND $carrier->shipping_handling)
			$shipping_cost += (float)($configuration['PS_SHIPPING_HANDLING']);

		$trasporto5percento = 0;
		// Additional Shipping Cost per product
		foreach($products AS $product)
		{
			$shipping_cost += $product['additional_shipping_cost'] * $product['cart_quantity'];
			if($product['id_tax_rules_group'] != 12)
			{
				$trasporto5percento = 1;
				break;
			}
		}
		$shipping_cost = Tools::convertPrice($shipping_cost, 1);

		//get external shipping cost from module
		if ($carrier->shipping_external) {
			$moduleName = $carrier->external_module_name;
			$module = Module::getInstanceByName($moduleName);

			if (Validate::isLoadedObject($module)) {
				if (array_key_exists('id_carrier', $module))
					$module->id_carrier = $carrier->id;
				if ($carrier->need_range)
					$shipping_cost = $module->getOrderShippingCost($this, ($id_customer_group == 999999 ? '|||' : '').$shipping_cost);
				else
					$shipping_cost = $module->getOrderShippingCostExternal($this);

				// Check if carrier is available
				if ($shipping_cost === false)
					return false;
			}
			else
				return false;
		}
		
		if($trasporto5percento == 0 && $useTax)
			$carrierTax = 5;

		// Apply tax
		if (isset($carrierTax))
			$shipping_cost *= 1 + ($carrierTax / 100);
		
		return (float)(Tools::ps_round((float)($shipping_cost), 2));
	}
	
	public function update($nullValues = false)
	{
	
		if (isset(self::$_nbProducts[$this->id]))
			unset(self::$_nbProducts[$this->id]);
		if (isset(self::$_totalWeight[$this->id]))
			unset(self::$_totalWeight[$this->id]);
		$this->_products = NULL;
		$return = parent::update();

		$id_group = $context->customer->id ? $context->customer->id_default_group : _PS_DEFAULT_CUSTOMER_GROUP_;
		
		$products = Db::getInstance()->executeS('SELECT id_product, price, quantity, bundle FROM '._DB_PREFIX_.'cart_product WHERE id_cart = '.$this->id);
		
		$cart_by_ezdirect = Db::getInstance()->getValue("SELECT id_cart FROM carrelli_creati WHERE id_cart = ".$this->id."");
		$cart_by_ezdirect = Db::getInstance()->getValue("SELECT id_cart FROM "._DB_PREFIX_."cart WHERE id_cart = ".$this->id." AND (id_employee != 0 OR created_by != 0 OR in_carico_a != 0)");
		
		$id_employee = Db::getInstance()->getValue("SELECT id_employee FROM "._DB_PREFIX_."cart WHERE id_cart = ".$this->id."");
	
		foreach ($products as $product) {
			$idProduct = $product['id_product'];
			$quantity_now = $product['quantity'];
			$price = $product['price'];
			$dati_prodotto = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'product WHERE id_product = '.$idProduct);
			
			$wholesale_price = ($dati_prodotto['wholesale_price'] > 0 ? $dati_prodotto['wholesale_price'] : ($dati_prodotto['listino']*(100-$dati_prodotto['sconto_acquisto_1'])/100)*((100-$dati_prodotto['sconto_acquisto_2'])/100)*((100-$dati_prodotto['sconto_acquisto_3'])/100));
			
			$special_wholesale = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'specific_price_wholesale spw WHERE spw.to > NOW() AND (spw.pieces = "" OR spw.pieces > 0) AND spw.id_product = '.$idProduct);
			if($special_wholesale['wholesale_price'] > 0)
				$wholesale_price = $special_wholesale['wholesale_price'];
			
			$login_for_offer = Db::getInstance()->getValue('SELECT login_for_offer FROM '._DB_PREFIX_.'product WHERE id_product = '.$idProduct);
			
			if($price != 0) {
				
				$id_specific_price = Db::getInstance()->getValue("SELECT id_specific_price FROM "._DB_PREFIX_."cart_product WHERE id_product = ".$idProduct." AND id_cart = ".$this->id."");
				if($id_specific_price > 0) {	
					$sp = new SpecificPrice($id_specific_price);
					$wholesale_price_sp = $sp->wholesale_price;
					if($wholesale_price_sp > 0  && $wholesale_price > $wholesale_price_sp)
						$wholesale_price = $wholesale_price_sp;
				}
				
				$prezzo_prd = Product::trovaPrezzoSpeciale($idProduct, $id_group, $quantity_now);
				$special_price = Product::trovaMigliorPrezzo($idProduct,$id_group,$quantity_now);
				
				if($prezzo_prd > $special_price) {
					$prezzo_prd = $special_price;
				}
				
				if($product['bundle'] == 0) {	
					if(!($cart_by_ezdirect > 0 || $id_employee > 0))
						Db::getInstance()->executeS("
							UPDATE "._DB_PREFIX_."cart_product 
							SET price = ".$prezzo_prd." 
							WHERE id_product = ".$idProduct." 
							AND id_cart = ".$this->id."
					");
				}	
				
			}
			else {
				if(Product::checkPrezzoSpeciale($idProduct,$id_group,$quantity_now) == 'yes' ) {
					$prezzo_prd = Product::trovaPrezzoSpeciale($idProduct, $id_group, $quantity_now);
					$miglior_prezzo = Product::trovaMigliorPrezzo($idProduct,$id_group,$quantity_now);
					$prezzo_attuale = Db::getInstance()->getValue("
						SELECT price 
						FROM "._DB_PREFIX_."cart_product 
						WHERE id_product = ".$idProduct." 
							AND id_cart = ".$this->id."
					");
					
					if($prezzo_prd > $miglior_prezzo) {
						$prezzo_piu_basso = $miglior_prezzo;
					}
					else {
						if($login_for_offer == 0 || $context->customer->id > 0)
							$prezzo_piu_basso = $prezzo_prd;
					}

					if($prezzo_piu_basso > $prezzo_attuale) {
						$prezzo_piu_basso = $prezzo_attuale;
					}
					
					if($miglior_prezzo >= $prezzo_prd) {
						$id_specific_price = Product::checkPrezzoSpeciale($idProduct,$id_group,$quantity_now,1);
						//$wholesale_price_sp = Db::getInstance()->getValue('SELECT wholesale_price FROM '._DB_PREFIX_.'specific_price WHERE id_specific_price = '.$id_specific_price);
						if($wholesale_price_sp > 0 && $wholesale_price > $wholesale_price_sp)
							$wholesale_price = $wholesale_price_sp;
						
						if($cart_by_ezdirect > 0 || $id_employee > 0) {
						}
						else 
							Db::getInstance()->executeS("UPDATE "._DB_PREFIX_."cart_product SET price = ".$prezzo_prd.", id_specific_price = ".$id_specific_price." WHERE id_product = ".$idProduct." AND id_cart = ".$this->id."");
					}
				}
				else {
					/*$prezzo_prd = Product::trovaMigliorPrezzo($idProduct,$id_group,$quantity_now);
					if($cart_by_ezdirect > 0 || $id_employee > 0) {
					}
					else 
						Db::getInstance()->executeS("UPDATE cart_product SET price = ".$prezzo_prd." WHERE id_product = ".$idProduct." AND id_cart = ".$this->id."");*/
				}	
				
			}
			//verifico che sia carrello fatto da noi
			$id_employee_cart_row = Db::getInstance()->getRow('
				SELECT id_employee, in_carico_a 
				FROM '._DB_PREFIX_.'cart 
				WHERE id_cart = '.$this->id
			);

			$id_employee_cart = $id_employee_cart_row['id_employee']; 
			if($id_employee_cart == 0)
				Db::getInstance()->execute("
					UPDATE "._DB_PREFIX_."cart_product 
					SET prezzo_acquisto = '".$wholesale_price."' 
					WHERE id_product = ".$idProduct." 
						AND id_cart = ".$this->id."
				");
		}
		Module::hookExec('cart');
		return $return;
	}
	
	public function deleteProduct($id_product, $id_product_attribute = NULL, $id_customization = NULL, $id_address_delivery = 0, $auto_add_cart_rule = true)
	{
		$context = Context::getContext();

		$id_carrier_default = (int)(Configuration::get('PS_CARRIER_DEFAULT'));
		$context->carrier->id = $id_carrier_default;
			
		$cart_by_ezdirect = Db::getInstance()->getValue("
			SELECT id_cart 
			FROM carrelli_creati 
			WHERE id_cart = ".$this->id."
		");
		
		$cart_by_ezdirect = Db::getInstance()->getValue("
			SELECT id_cart 
			FROM "._DB_PREFIX_."cart 
			WHERE id_cart = ".$this->id." 
				AND (id_employee != 0 OR created_by != 0 OR in_carico_a != 0)
		");
		
		if($cart_by_ezdirect > 0) {
		
			$last_cart = Db::getInstance()->getValue('
				SELECT id_cart 
				FROM '._DB_PREFIX_.'cart 
				ORDER BY id_cart DESC 
				LIMIT 1
			');

			$last_cart++;
			
			Db::getInstance()->executeS("
				INSERT INTO cart (id_cart, id_lang, id_carrier, preventivo, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, date_add, date_upd, rif_prev, name, riferimento, esigenze, risorse, note, note_private, validita, consegna, transport, payment, attachment, created_by, id_employee, in_carico_a, notifiche, email_preparate, numero_notifiche, data_ultima_notifica, id_persona, provvisorio, visualizzato) 
				SELECT ".$last_cart.", id_lang, 0, 0, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', 0, '', 0, 0, 1 FROM "._DB_PREFIX_."cart WHERE id_cart = ".$this->id."
			");
			
			$new_id = $last_cart;
			
			/*Db::getInstance()->executeS("INSERT INTO cart_product (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, sconto_extra, prezzo_acquisto, no_acq, id_specific_price, sort_order, bundle, date_add) SELECT ".$new_id.", id_product, id_product_attribute, quantity, price, free, name, sc_qta, sconto_extra, prezzo_acquisto, no_acq, id_specific_price, sort_order, bundle, '".date('Y-m-d H:i:s')."' FROM cart_product WHERE id_cart = ".$this->id."");*/
			
			Db::getInstance()->executeS("
				UPDATE "._DB_PREFIX_."cart 
				SET id_carrier = '".$id_carrier_default."', date_add = '".date('Y-m-d H:i:s')."' 
				WHERE id_cart = '".$new_id."'
			");
			
			$context->cart->id = $new_id;
			$this->id = $new_id;
		
		}
		if (isset(self::$_nbProducts[$this->id]))
			unset(self::$_nbProducts[$this->id]);
		if (isset(self::$_totalWeight[$this->id]))
			unset(self::$_totalWeight[$this->id]);
		if ((int)($id_customization)) {
			$productTotalQuantity = (int)(Db::getInstance()->getValue('
				SELECT `quantity`
				FROM `'._DB_PREFIX_.'cart_product`
				WHERE `id_product` = '.(int)($id_product).' 
					AND `id_product_attribute` = '.(int)($id_product_attribute))
			);

			$customizationQuantity = (int)(Db::getInstance()->getValue('
				SELECT `quantity`
				FROM `'._DB_PREFIX_.'customization`
				WHERE `id_cart` = '.(int)($this->id).'
					AND `id_product` = '.(int)($id_product).'
					AND `id_product_attribute` = '.(int)($id_product_attribute))
			);

			if (!$this->_deleteCustomization((int)($id_customization), (int)($id_product), (int)($id_product_attribute)))
				return false;
			// refresh cache of self::_products
			$this->_products = $this->getProducts(true);
			return ($customizationQuantity == $productTotalQuantity AND $this->deleteProduct((int)($id_product), $id_product_attribute, NULL));
		}

		/* Get customization quantity */
		if (($result = Db::getInstance()->getRow('
					SELECT SUM(`quantity`) AS \'quantity\' 
					FROM `'._DB_PREFIX_.'customization` 
					WHERE `id_cart` = '.(int)($this->id).' 
							AND `id_product` = '.(int)($id_product).' 
							AND `id_product_attribute` = '.(int)($id_product_attribute))) === false)
		{
			return false;
		}
			
		/* If the product still possesses customization it does not have to be deleted */
		if (Db::getInstance()->NumRows() AND (int)($result['quantity']))
			return Db::getInstance()->Execute('
				UPDATE `'._DB_PREFIX_.'cart_product` 
				SET `quantity` = '.(int)($result['quantity']).' 
				WHERE `id_cart` = '.(int)($this->id).' 
					AND `id_product` = '.(int)($id_product)
					.($id_product_attribute != NULL ? ' AND `id_product_attribute` = '.(int)($id_product_attribute) : '')
			);

		/* Product deletion */
		if (Db::getInstance()->Execute('
			DELETE FROM `'._DB_PREFIX_.'cart_product` WHERE `id_product` = '.(int)($id_product).($id_product_attribute != NULL ? ' AND `id_product_attribute` = '.(int)($id_product_attribute) : '').' AND `id_cart` = '.(int)($this->id)))
		{
			// refresh cache of self::_products
			$this->_products = $this->getProducts(true);
			/* Update cart */
			return $this->update(true);
		}
		return false;
	}
	
	
	public static function getCustomerCarts($id_customer, $with_order = true)
	{
		$context = Context::getContext();

		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT *
			FROM '._DB_PREFIX_.'cart c
			WHERE c.`id_customer` = '.(int)($id_customer).'
			'.($context->employee->id_profile == 7 ? ' AND (id_employee = '.$context->employee->id.' OR in_carico_a = '.$context->employee->id.')' : '').' 
			ORDER BY c.`date_add` DESC');
		return $result;
	}
	
	public function getCartPDF_ezcloud_A($id_cart, $id_customer) 
	{
		$cliente = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'customer WHERE id_customer = '.$id_customer);
		$dati = Db::getInstance()->getRow('SELECT * FROM cart_ezcloud WHERE id_cart = '.$id_cart);
		$cliente_nome = ($cliente['is_company'] == 1 ? $cliente['company'] : $cliente['nome'].' '.$cliente['cognome']);
		$cart_products = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'cart_product WHERE id_cart = '.$id_cart);
		
		foreach($cart_products as $cp) {
			if($dati['tipo'] == 'basic') {
				if($cp['id_product'] == 367442) {
					$serv_code = Db::getInstance()->getValue('SELECT reference FROM '._DB_PREFIX_.'product WHERE id_product = '.$cp['id_product']);
					$serv_desc = Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'product_lang WHERE id_product = '.$cp['id_product'].' AND id_lang = 5');
					$serv_qtan = $cp['quantity'];
					$serv_impo = $cp['price'];
					$serv_scon = $cp['sconto_extra'];
					$serv_nett = $serv_impo * $serv_qtan;
					$serv_unit = ($cp['price'] * 100) / (100 - $cp['sconto_extra']);
					if($serv_scon < 0) {
						$serv_unit = $serv_impo;
						$serv_scon = 0;
					}
				}
				
				if($cp['id_product'] == 367642) {
					$conf_code = Db::getInstance()->getValue('SELECT reference FROM '._DB_PREFIX_.'product WHERE id_product = '.$cp['id_product']);
					$conf_desc = Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'product_lang WHERE id_product = '.$cp['id_product'].' AND id_lang = 5');
					$conf_qtan = $cp['quantity'];
					$conf_impo = $cp['price'];
					$conf_scon = $cp['sconto_extra'];
					$conf_nett = $conf_impo * $conf_qtan;
					$conf_unit = ($cp['price'] * 100) / (100 - $cp['sconto_extra']);
					if($conf_scon < 0) {
						$conf_unit = $conf_impo;
						$conf_scon = 0;
					}
				}
				
				if($cp['id_product'] == 367498) {
					$attv_code = Db::getInstance()->getValue('SELECT reference FROM '._DB_PREFIX_.'product WHERE id_product = '.$cp['id_product']);
					$attv_desc = Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'product_lang WHERE id_product = '.$cp['id_product'].' AND id_lang = 5');
					$attv_qtan = $cp['quantity'];
					$attv_impo = $cp['price'];
					$attv_scon = $cp['sconto_extra'];
					$attv_nett = $attv_impo * $attv_qtan;
					$attv_unit = ($cp['price'] * 100) / (100 - $cp['sconto_extra']);
					if($attv_scon < 0)
					{
						$attv_unit = $attv_impo;
						$attv_scon = 0;
					}
				}
			}
			
			else if($dati['tipo'] == 'business') {
				if($cp['id_product'] == 367491) {
					$serv_code = Db::getInstance()->getValue('
						SELECT reference 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$cp['id_product']
					);

					$serv_desc = Db::getInstance()->getValue('
						SELECT name 
						FROM '._DB_PREFIX_.'product_lang 
						WHERE id_product = '.$cp['id_product'].' 
							AND id_lang = 5
					');

					$serv_qtan = $cp['quantity'];
					$serv_impo = $cp['price'];
					$serv_scon = $cp['sconto_extra'];
					$serv_nett = $serv_impo * $serv_qtan;
					$serv_unit = ($cp['price'] * 100) / (100 - $cp['sconto_extra']);
					if($serv_scon < 0){
						$serv_unit = $serv_impo;
						$serv_scon = 0;
					}
				}
				
				if($cp['id_product'] == 367645) {
					$conf_code = Db::getInstance()->getValue('
						SELECT reference 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$cp['id_product']
					);

					$conf_desc = Db::getInstance()->getValue('
						SELECT name 
						FROM '._DB_PREFIX_.'product_lang 
						WHERE id_product = '.$cp['id_product'].' 
							AND id_lang = 5
					');

					$conf_qtan = $cp['quantity'];
					$conf_impo = $cp['price'];
					$conf_scon = $cp['sconto_extra'];
					$conf_nett = $conf_impo * $conf_qtan;
					$conf_unit = ($cp['price'] * 100) / (100 - $cp['sconto_extra']);
					if($conf_scon < 0) {
						$conf_unit = $conf_impo;
						$conf_scon = 0;
					}
				}
				
				if($cp['id_product'] == 367499) {
					$attv_code = Db::getInstance()->getValue('
						SELECT reference 
						FROM '._DB_PREFIX_.'product 
						WHERE id_product = '.$cp['id_product']
					);
					
					$attv_desc = Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'product_lang WHERE id_product = '.$cp['id_product'].' AND id_lang = 5');
					$attv_qtan = $cp['quantity'];
					$attv_impo = $cp['price'];
					$attv_scon = $cp['sconto_extra'];
					$attv_nett = $attv_impo * $attv_qtan;
					$attv_unit = ($cp['price'] * 100) / (100 - $cp['sconto_extra']);
					if($attv_scon < 0)
					{
						$attv_unit = $attv_impo;
						$attv_scon = 0;
					}
				}
			}
		}
		
		$content = '
		<page style="width:840px">
		<page_header>
		</page_header> 
		<page_footer>
		</page_footer><div id="container" style="position:relative; width:595pt; margin:0 auto">
		<div title="header" style="width:580pt">
		<table style="width:100%">
		<tr>
		<td style="text-align:left; width:50%"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAdEAAABKCAYAAADkFW6vAAAACXBIWXMAAHUwAAB1gQEg7e00AAAYoklEQVR4nO2db4wdV3mHT6gTxzSJV7El2ighbj6skUjw0kQt9gd8XYuChMOuZRQUWWHX+RAIQngtS1RCFN8VoUIoltciIolQyLWDBFiqtJa3og2EXH9ITFrSrAUf8Cohuyp/RGuXtRISCGrc+8z1Wc/enZk7f94z58zc80hXXq+v586dmXN+5/2d97xnzaUOyuOpKvNtpc615Y63uaHUcCPb/3m+pdSFBblz2Dah1IZNcsfzeDxd6C+ytu8+rBE9msdTNt+bVOqXZ+WONzKW7f1vLCl1bJ/c569br9TdTbnjeTyDDm30menui7blRdTjuQzRn6SAbrhVqVtGsv2fuRm5z4esIu7xeKL5r7mucJ45duV3fdpXuz2vFhbOq8XF/w3+vn37sNq0aUPwisOLqKe6uCBgLpyDx+O5AtMrZzqv+dMrf3/zlshpktOn51WrdUY99dSP1caN16l3vesGdf31a9XatVer73zn39XLL/+Peve7b1Sf+9zfqX37tqn169et+P9eRD3VhcYiydaJbO/HJjp7Uu7zsXK9iHo82aEtIpxEnhcWo99DrkGIpaU31P33H1dnzryi7r33b9T8/JeDiPMd77hqxfv+9Kf/UydO/EQ9/PAP1EMP/Yt64olxNTq6ZfnfvYh6qom3cj0eD/3AqWa3Lb55Mfm9ofaFgO7ceSSIOp977h/UbbdtjP1vV1/9Z2rv3r9VY2Mj6tvffkE98MBT6ne/+72amNgW/LsXUU81cUHAXDgHj2cQIcv2h9PpnaAeKxcBvf32m9SxUFLgjh2zqt3+9ar/un37X6rJyTs6IrpJfepTH1Tvec9fqA99aLoTtW5UjcawF1FPRfFWrsczWNDmGLjONuMt2zhCVu7x4z9WFy++qb72tT2r3oZgttt3r/jd0aM/U7t3P61effXejnBeHyQbPfTQqLr//mPqF7/4ihdRTwUh607SymWUmtXKZf5Fkp75Go/HcxksWwbNzHf2s2zjCA2SW63n1ac//cHAyk3D+PhwJxJ9Xi0uvh6IKHz+8x9WX/3qvwZJSV5EPdXDBQGzHQl7PHUHyzbItD3W753JbBlV6p1Dy39F+I4fj17bvbT01gpLd27ugpqZWVAjIxuCVxiSkLyIeqqJ7blIF5KaPJ66QvsmWUiqjfW077ffvqRuvPHPI9+6uPiampr6z+DnhYXXOqL6RzU9vS2IRntZt+6a4E8vop5qgZWbdT4kiZi1Y4nYFnGPp860JvLbtlH0tK/3v/8W9f3v/0zt2fPXq986slE9++yu4OeLF99SExPtjoj+VI2OblJDQ9csv++NN95Sv/rVUuf9t3gR9VQMb+V6PPUlzVKVLPRYuTA6OqIOH/6B+tjHtgTLV+JYv/4a1Wo1VKNxSu3YcUq99NKVRCRE+PXX/9D5t81eRD0Vw3YU6K1cj8ccJbTvycmd6utf/5H67nf/Q9133wcS/3tYSA8cOKOOHNmqLlz4vWo2TwUVjG644Vovop4K4YKVy/sf9xsfeTxGKEFEKdv3rW+Nq717nwgEkbWf69Zdraant0YeYsuWDWpu7uPBHOn8/G/Vrl2PqOuuW6u+9KWu7etF1FMdXLByPR6PGUqwcjVYubOzn1V79jwWRKRHjtyjtm69LfZQ1177tnruuTn1yCPPqo985L3B+zVeRD3VwbaV6/F4zFFy+6ZowiuvfEUdOHCi8/PD6qabhoKko7vuujX4+bXX/qBefvm/1QsvvKrOnfttUJz+scf2BgIcxouopxq4YOV6PB5zWBgka2uXyPLkybOq3T6nnnnm5+qqq64Uof/oR+9Q3/zmfep977s58hheRD3VwFu5Hk99KdHKjQIx/eQnPxC8smJdRKkOQVWIcKUIJnApsRQH9Q1haGhtUEWCUkxRFSU8FiBipNJIEDkuXH71RJBkpBIFkpV680h3xNjvgfdWbnnoGqXcR+5feF9Gavxy34Yb3WtYZmYxz5R+rvjzzaXuuSZlS+M46GeLc+Znff5VRberc+3uNeBaLP8+wa3R9w74c92QUpsb3bZo25WpcPsuXUQpoYRYttu/UWfPXsh1jNOnf7P888mTC8s/k47caNwUVNvnFV4cKwmCPzb2b0aOXQYMPmZm/l7ugDRiIkUaQhrLlffw0p0zOylsHVdq52R0p+yClUvdTsmGznfN2tBZhE5HWRS++0Rr9e/TbCvF77lvvGanugOiXU0zkT33XYt57wbLaQkLbO8xhrd37wEv2yKSBPeF64Boci3yRmz63oH+k3sI3Ec9MDIhQJw3z1YceiAgBe01jXvFQOIzM4U+qhQRRTRbrflAQKkCYQqOjajy2revWzi42bxzuWiwFHyPsJBXjagSVrnQRaEl1k1SH5PXzv3dTjkcmfIZkiBgWUna7DcPUSKWBB1p0Rqimp0jq39HB5enwDfXhEEQ/5fvVDQy5XvqAYvk9Y5CDwZOHOgK6t1NdyJULZy0Mcl1ybGft3ilDSKoFAChnWSwRBPhe+QdCOUh7TVj8F4QoyKKcDabP0m0Zk1y7Nh88Nq///bOedwlFpkiolWGKL0QjCqDqMhAJ/fM0e6I+2D7SgO2bfW4EAlLXoNwhSTs0EfHindwdPpFojkdqZTZ0a74/M7nHt7RFVMGA7YiU+0GSA2Ycp3DYjdCJZK7Z1omMpVuw1IIfDcjImpbPHthPzjsYyzMolEpVm7YQq4aWN65RZQGjnia7ugYRR5udIW0iH0VRcaEgwAXkpqkSg2GKyQxOOB+SkQ6WGJ5ohbb4tkL5/Hlke73KTMqdUE8e0FMH93ddYcQ07xIJw1JIbSHr6iIYttSsNcV8QzD/OvIyD8HG64WSUAa2CiUTvzEZHmNgY6dDl7KTtLkaTS2I2HJUoP6sxFQBioS93PXoeyCU9aALA9cE6LS8SfNZ3HjBGBf67lJF8Ed4jyzTkFoahyFgoiIEp0hnq5HaMyZkhBECae81u7AiSiNB/G0MUI+e7I7WpTEW7myAkpkz1xiFoi4XBYNDXO9RO2mMpBNTotIQ/vnOuTJJ/AimgzR59jY00YThiQhSkbw82SnDpyVi4DS2ZaR2BCHt3JlrVwE/AubZK4rx8sSnRB9fmPM7vOUFeaLvzgn74gwMCXCqxIkYGXNZK65lQuFRLTZfFFNTb0ociJlghAi/iyHycJARaFEK3QgVRglp8VbuXIRKJ3QgzPpxYXrKL1PZBnw/GO3Zo22Y4+3UL2BRBju4cF2+vfXPAqFXCJaFfs2icnJM2pubk//N4YYGBGVtPtcYtCtXLKepTpvEk3S2pxVsW/j4NwllnvUoV0xh50lE9uL6GoQUPZWy1sowRU4fyolpU0yGhgrVy95qHJDj2LQrVyQElDW1qX9LkQuLmWc5oVnIc98oIb7yBxrHSAyT5OtOwBWLmQS0TIEVJf0g37l/4rSap1T09PbUr13IKJQPQdaJwtXM+hWrhRE02mXO5gW0HBJv37l/4qCCOYV0ToJKOBopGEAolBILaKmBJQIaWJiuPPaHBkVIqSsO52e/ql48hKRaFqqLqJc476Q7OBapy3FoFu5EjCCT7se1JSAEgXHlabTNX9nm/IDwbztom4CClwLrnXZ9a6lsCGipgT00KE71eTkHYnLTSiOQOk+IinOQVJI05buq7KVyyCl1Wr0T6IKak3WwHaLwlu5MqSt5GNCQLmHn5hO/nzuMddZJ1BJDwhZkpJlPWwZAkqGNJs4hOendYF+kwPiX84lX4sBsXIhlYiSRCQpoLfeel0nsvtwpqIHvJdlKTt2zIqdBxDp9qtiNDd3foXNLAnRsKnlQVu2bEhXpYkGR/q6ScI7SJi23nrxVm5xKKiQ5jtID8Z4brCPswxCENODbbmlPJrzCx3hSPneoE1Nyn12GK4J1jLXJGlQoesQm1hK0+9aDEgUCn1FlGUsklEYHTtVg/IUOyCaQoAl50nTiCif225nWw6ThsnJ540VsqdecNr53iByMEXc7iy6UkueoudZoMPJGgVKFz8nisqza0xZUC9WRxV6a61wJSFdnL0fRGqSgzHu3cF2vmIHCCkdpqSgp91Bx2RyHoOZtJnCPHMMQIhUpSPifteCf+e5iUK6SlXc50RRJDkshkQRZS2l5DrQIgKqQdAoKl9lEG4KVJhI0NL2berlLFK7sPTSr5A3nQAds+TaxSiqGIWaOIdedDQT1yEjBFjaWJIPpjgX3s/6RymKCKiGgYGNKQoTVYiYU8+7Sw6DSO5lmSUWD7ajf4+4fuGv5D4HOzvus0oiVkT1WlAp6NyxFovupCK9rVnZmKzwxCAFAU1tk9PxsYZPGqLPtJVs6BQQU1N2sgsCZjupqRc6ZBKE+s0vapFNg3QhBYlt1TZuEjiRjPDsUK5SEu7XwXaxdarcRykR1ZWv8mC7bRkgVkSld2Ghc6+6ABbFZIUn9gjFvs00SCGLUToCxG7KWt2FBn7KwLnkSSKQTohwIakpTJYBTlqkhYNdQxzoHDPDoFR6akRCQCHLgETnLyCU+rUx9HMRpJPlwtv6WSJSRImW2D5MCpJyCu9heZmlpT+KHEeTtfRfHojqKXxvYv6TCB/xTLWEJUyQdCCccEAHnbc8Gtab9Ai+ilGoiXPQIOjSAiotHHTgbMouAecmyeZG8r9LD0qlBBTC4qdFct1Q9893Dq38uymkk+XC2/pZJFJEiZgkIQqVIsvaThdwyr4NI23j8kAX2XOQxuBF1JyVq+fUpJFODOMZkir2zrUsCxODUu6XZOH7L75kV3Rsty1DrBJRigpIRkzYjJI2rqSImlq2onHOvtXoRemSSHZ+EngrdyXSHTLoJRRSMBCT3L9TWkST1kVKD0qZFpEWPNtRWw2tXFglotJRqPQxJSM6U3O0Ttq3YaSjBzJxi44KpTu8KkahJs4BTHTIIP0cYTlKihEFAaRA4ONgMCGZBcxnSe0a4wo1tXJhhYhiPUovu3B5OUouG7QPRMrSlZU0eYpURCId7Ug0+LRr8NLigoC5YOXS2RhYGxe4GdKRBdmjZS7DyAJrLeOQXtMrNSfsErbblkFWiOj0tFwyURWQTiqivu+BA2dEj6kZHd0UzH8WXSIkXkiAubYspdCikK5g5K3cK9Ahm7DZOVcXy7qZIimpSHIwIW1pu0JNrVxYFlEKAFS1PmwesEWlIlHT+6seObI1qDEsgvSIUCLKodKNJFWMQk2cAwMcUx1ymRWVXCBuoIg4SA4mTLgGtqmxlQvLIlr1XUqyIhWFYt8y/2liyzYx+zaMC2LRiwvnZPscTFi5pjpk08UgXCNc97kX6efGoQhLDNttyzDLIooVOUhIrFuthH0bxgXLMgrJRsYoNWsjk44mWC+b9bpIR3YmbcFBi0LjniciLMllWVLtyTWknxfHovVARE1vfu0iRUS0UvZtGBdHhNLCXsUo1MQ5mExOkbbfXSfufqbdnLro51QZF/blNUwgooNm5RLl5Y3wTNq3zNNSoN9E1nCAC2LRi/QoNasdRlKTZDThQlJTnp1r0jKIVm7c/XSxPbmGC/vyGiYQ0XbbzHZcrpI3Cm215oPty0wsX6HwA/OfovZtGDo/6ZJkRa0n7DDJJQ15Eg5c6Ailz8FkR+Oj0CtIDr4k2pOLuNC+DHNZRH9t+zxKJauIYt8inqbWvB46dKdqNu80cuxlpDs/iew46SovLgiYC+dgcs5I2sJ0nbj76WJ7co0BsHJhDfOhkpEVERWWZF3AvmX+09Ten2wPV0YRfPGKQEUfZjoh6b0evZVrvqORrAIEj1+SPV5ZSA8mTNwz2jzRrS3hGQArFwIRlcTYfJ4FKm3f9lJmMe40fG9S9njeyu1iOnNROrKoKq61p144v/Bm9+FlOnrNq97izNTuLS60rxJYIz0fOjS0VvR4NqiFfduL5GLnomDjSp+PCwJW1XNIi7SFWeU5QOkylZLo7enCDgc/6/yDpDwE6mCD3hoNoeXnrCI7IFYuxG7KPajUxr4NI72vYhFYkzk7JX9cb+XWd52hi0gPAiVFGQHNe35RQnvw2ezHGRArF7yIhmCpDwJqau9PBNTUzjGJSM9jAY0ka+F5BPTYPvlz8VZu/nPwuIFUlI+ASg4Od+7PVxtb+tl2OPHKWRHV+4aWNceKfXv0qJkC/Pv33x5sX1YrsGoQxTQjRKK+2ab8psWaqgqYC+dQB4jieBXdCMEmtCfWTOed06aNMQcqXaM2T9GO4H4IryV2+N6KiyjRXNH5vqhyetSR1VEcwqrnXhuN7sba/FueKI/EqrGxp43Zt5Tukygx6CQnJruJCUkPeGDfNs0u0PdWbjWtXCxD7kWR8yaC+8bYymsZTqJhHk3PpfE7PstUIk1RyBWgLeVxVXrnQCV4cCbfvTGxJ27R58Qga6SzQxEjhDSPcCSV06NCkK4SFN7seipieg3rVH8v5h8poRf1PU3at8A1IKLWUbVJGEyUPtdKoz28o9uBIyIIKpxf6FrI0tuuReGt3PznkBXERxoGWPdM5/u/iE7U/Ho4iUb1KebB8xMWWQZktgQ2aE+N7vVI4/DwDBG9mtiDdfzJ/NfBxFpiBuwTrdW/ZxDF5/EnbcBCXd01JuxShClr+TrJ5SThqHJiYnOkgFJgYvfupwt/VhJlbkjeaOwq7bNWQVQnGdllIc+uF7aTHhhVS4qoyTJ/YUyICxY/G15nOX86TJZISViXDPL0QE9noqaBbFETGe8IKXkDDC4QBSLTcASGYJD5yjUwtZ8rGygUeZ5MVLViTTltRt+fKMs472CsIB0R3Sh+UISw0TgV2LpJhdSxUokGsW9N1KJ98slGR0SHI/8N0a4LWN2JUaijqeEiZG3sJkoNZp2vcaHofl5MiAeigXORtIG4HngwADIReSEcUZFOHGzSbXLZWDBHetRcHkEcWa9DL9LlRcOscBh6SNquzjCBnYv9KT0niJAyr9lsvhhEpOFOHvHE4jQxD6lJElCoU9H9vtZ5XUU0z9oxF2xUF84hL6bEQwsG6xT1GkV4c+ly5GVAODV5hIOBU9kCZxraUxEBBVvrZy3OcQeJRYiNqX0xEVPmMMPzmKZJI6Cm5kFtgGXdF1P2k03yWE4kOklSxaSmIvB9TYoHYmlSMHvJG3lxzbn2pqKusmHwQiJRUWxVcrKYvXtZRDcbE9EySZsNW6coFCs31dyzafvJBlnFg1Gy9BKAKiY1FYHvW5cB2a5D2dc6hyGJxUThkLIpauGGwTmwAf2bJQIRxdIdHx8uNRFGGsSEWrRpBKVOIpo6C9oF+4mRO3NeEhm73srNfw5FQTxMFM0oC57DtBmwSXAdyI6tcjRKMQXJhJxBjUSBJKCq2pxssk0Emma5TlW/YxyprFywbT8hep+Y7i6JkcBbueVbuRquPeJRxWhUz/tJzKExICSSPXGg+LHKhmeH6yD9/AQD2xLteLC8kcGyiFKogKo6+/a1LZ5ONrBv+2UA91KnKDS1lauxZT/p+RbJpSXeyrVboYgOOLxLSBUg6krKAM51zMlu9CW9rZ9JaI/cPxMJhzaSGC1aubCiYhHJOKyfrIKty1ZiRJ9ZqxTVSUQzF7SwYT+F552kIkFv5eY/BykYQGADVsHWZcCDaJiy/LgOCKnrkTnRJ23RZEECnsmyB+qWSwKuKvuHMIGrQkr0RcScpyLSwFq5GkbgdCaP7jZxOivp7bgkI0Fv5dqzcsPo++CqkHKNEIwiyUNpoF0dbMsXf5eE5CHE3nTpPAZXtH3Tlcp6P9MikbVzdYQ3NfVi2ecTC+LZbN6VuHSlH3WKQjNbuRo6Xkp6mez4omwzySjMW7n2BVSDkHKfTdRuzYsWT15l1Vvlcz4z03V6KEfoyrVAPBlElGmzBla/UO5DP8JlGy0RW4CeuUaiPUrxlbnGsxeShhBOiSLudRLRQtdDRxDUo5Rs7My1MNqNEhhv5do/B1NwLv+00H2ebM4N8mwgnJyPrWLl+vMRUlvXAmHhPHBLbFwH3CfTA/XwZ1kmcRcXIh1q4FJdqNU6F4iQifJ8YXQJO0SCP6UK5A+8ldsLQsqEfNHGruu20mjjxM1bufWzcnvRUwVEPQwYuOam5wi5DnSiXAueZVcqc3EeXAsGlCTTcT1MF5BgAME1sFlAPwztlA0pcChMWbt8Z9dFVIOYMg/JS+9KMjd3fvnnPOKktzbTL3YhoY6v9K4yGkT50qUHjBy7sujGrjs+vRtCUnSqa1TqzitNg+VzHr9U8GQL8I9z9j4bEBib379MuNfaSmXwRMKNLpied59J/czpLcz0yxXRjIP7rq8FAynqA9PG9P6neevM4viEr0VvkXpX4LxwKPiewUCinf07a7tWv/R9d2GgcJnM+4kiqN25uNVzk2T2JpF3z0+PYcIdnyZqJwYHRn2eCqE7vqioOehMl+L/r6t7fuYFkaP9xLWhftcjvC9q1dBiH0YPJKKo2L3/f/fukZ26600IAAAAAElFTkSuQmCC" name="Immagine1" align="left" hspace="12" width="174" height="26" border="0"/></td>
		<td style="text-align:right; width:50%">
		<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAVEAAAA8CAYAAADfa4lyAAAACXBIWXMAADqcAAA6mAEctlrUAAAgN0lEQVR4nO2dCbwdVX3H/0NeIiQxBDfKKpu4YQ2CgBUktEoLVogVRKtVxJaRgiVWwAK12iKGFv0ULAEeKNKCRNkScGEzkhCQfRcEUiACIRQFHiQQEpJ3+/++M5N3332z/c/MXZLM7/M579537yznzpz5nf9++hqNhtSoUaNGDT/0dbsDNWrUqLE2oybRGjVq1CiBmkRr1KixXmKDDTbYqNFo7Kpvj9T2x9HH9+vnZwdBcMuqVauWFzlOTaI1atRYrzBhwoQNVqxYcbC+na5tD23PaXsx+nofJdZPart1zJgxp48bN+6S5cuXr8o6Xk2iNWrUWG+gEuab9WWmvkKiN+rrMfp6k5Lmk9Emf6RtT/3/r/T1IiXbT6pkesTg4OAzacesSbRGjRrrBZQwt9SXK7XtrO1olUhnLl26dHXLZou13bnlllt+b8mSJaG+P0sJdfu+vr79Vb1/Kum4NYnWqFFjncekSZM2VIlyphLizvq6nxLi1VnbP/XUU8R+nq0q/UP6er1KojN1v0P09dXWbWsSrVGjxjqPl19++WNKoAeoNHp4TKBKkLvoZ8dEsfIbagu0NXSb+UqY5+l2L61evXqebvc5Jc//0e8O0HZx67FrEq1Ro8Y6jYkTJ/YpUR6n7SZVy8+PP1di3EZfPqXtqaih8r9Jt5umbapue5AS6arx48fPWrZs2d/q1ydMmDDhCiXkFc3Hr4REg+AcGHystl5Lf6I/Y7QN8r7RODzTy7bOIgy4Nxtpw6i+nbYttGEfmiTu2oD4WnGNUGFe0va/2jC4vyL9jdfa1DfG4AZSbOywzeu0rdD+rJ/3cn1GGDA+aRae6XvllWA3fd1VpcujVq5c2TyO4/dfU3K4iDdKopto+7kS7IH6uq1+tHDp0qWrdN8f6ft+3f9D+nrdiBN4/6AISqBv0hdYehdhcLuHcoOyx60I9OX12h7Xdoa2x7rbnQ4hDCDMjbXtoO1PtL1T2zu0jdc2TtyExyv3Px6QvHLfuGbYfTC4cz8h00V6zJ/r6w3a/q8SAgsDzvURbX8jjhiXS/64oU9v1Xaq7v8L7UevTdo12oUw2FT//ou2zbS9LMPjNQuMl5VKkDwLz+vrzS3fB9FrHN6EpPWCqu/LIxV/ZdO2t+lnf1DB9ItSNYkq/lLbcdo2qeBY7cLd2r7V7U60FU7afKO2qdo+qW1HcZLnG6T8ff6gOHvQ0wKRhsF39XWhkthg9m6ZQAr+sraPGvdjYEO6NYGuX/iAtr/32O8X2t6m7Xlx47cZ8fidoVInx4ZUt1Cp8536/2FKpk80bUuI04B+98HWE1RBon8qvU2giOxXSdNss84hDJidP6+N2DcGzOvbcBaO+fao7aXtTD3v2V5qfjgkACAZv8ujH9zLe/S8HrvWWCsRBmi70zz2hDhR07+Rs91bZFiD3gopVEn0+b6+vjWDLKtQUykSVVUeEXvbMsfoABZpu0KcaL9uIQwmiyPPg8SlrU1q+ja+64FUg+bjQYBI9jtoH45VQluZvlsisGvtLS6w2Yp54uy0NdYfoFHt67HfgLZrtB2o7c/E+QKag+Zjc8BXlCRn8UaJc1c88vr/CStXrvylONMBKaKbDg4OTtY2t/UkZSXRQ8XZ23oZt2i7v9E4vNv9qA5hQDjGEeI8iwQOj03YqiryTDsehE0w8tND6r3NToptloyQjYx9+J22X7fNyVWj9+CcSe8XZwu1ALs+BIo0+l/aDlbpElX8zqZtYsFgTNNnD+t2jyuJrlbCXGOu0v/3wHOvb89uPZE3iUYeeWaHN/oeowPAMcJFW3ceujDAu36KOOmzaqK0AtskZL5AILcicKr8u8VFB1hBtsnjHvvVWHuxlTh7vBV/0DYLu33feWN/vWrVqjuVCD81YcKE/qYQpXHR6/h4J+XN1+l2CCXvFeecXT5u3Lix+tln9f2dEydOvLH1RGUkUSSgrUrs3wngjb95nQltCoPd9e+/i1OFATNpt4kUb/lntW936YAdlc2RArzyPnbbX2n7vcd+NdZewDGMF+tYJ+7zPt689tprq8eMGTNDifDS5cuXH6YfnRVtc5u2o1TCnBfvpO8HdLujxUW2DAlfqt4fqp/vqe0Qwp1aT1SGRL8gvU+iSEi/6XYnSsOFLKE6483erumbbhNoDGxOGPBHzdIJ4LcQPWAlUe7jfbVDaT1CGDC+kQgxHVluPOSH7fKV+AMlwCuUHH+sr2cqoT6mxHiN/o/3fWbzjvo5JHlN1LCF7q3bnaOvcyZNmnRl0sm8SFRV+YniSkhhm+sFaSgJXEhCm1bkbdjTcPbPE7UdL70Tf9sKTDrvkTwSdbGhSNM+ky/OwVoKXb+AwPCR6L2FYzD5kJ65xoxH5tHYsWO/qiRJdMnVSqRHbL755v1PPvlkKjkrcX5BCfQ8faXG6JdfeOGFRE3LVxJldniz576dArMM4vza65UPA2w1xOAeK71LoAAb0o5D/e1vvJKxHTbU/cQ+6XIPF+ixl/p2sMZaCZzWf+6x3yKBSFu0FlXrn6YakxIjlZnOWrx48aeVHC8nHVS/XhJttplKq7vrZ/gcpmr7qe4TrlixYomkwEyiKoXyMBNWEzsGrA8EDgiyX3jYxuRs6wse1vu13bXWeuXDgN/wj5If41YU92pjsBBwzLXnPqJaY/vZX8pNihyLECtClrKywjDUf0LbBOPxcQ4+5Ne1GmslnAlrT3HPsgVMuGQmJWqgKpA+s/HGGx+8bNmygyPb52niNJx4gsbMhBf+Lm2fVen1EiXQzBA+H0mUUANUMl8CnKXEdobnvusTCF/6agXHwVaJbRgj+r06O4+UzIcHKzbXT5Q4DzF4k1O/dfat94lzRFnGDmEmP5XR2SY11lW4CA4y7vbx2PsRwfSTEbv84osvYvechZ1T2+6q4n9JX8mvJ5Rpnr4/Vz+jUHOWVrUGPiSKTWGyx36AsINFnvtWhkiaHip71elTy3CxjVWpUnIYkO+OBDq5xLlu1XbhUOtvDKRu1d8gZ/06Pecd4iTUQ2Q49MMCJtcsaRbpcz+P45JpVn1saBhg18fm9tboE16bpR7u0YC44Gze83A+1pZ8fWcrLgo3di3XwxV5KZqiyxhlkls9asK1IgzIZERDeVPU4mQQbItwAdeW7LMXEvZ+lwyve2TBg+IK5+RicHCQsT8vat4wkaiSD1ILEpJPjB+D73pxD3dXEDnEThBHTgyWQemcUyyukgRBIVn9TJLiV8OAbCDiQH0zwTjmD4SCK/2NBwrvxUAOAwo8UEjGJx2zuZhJEshug0StGgx27Uc8+jMSjkgIy9tJ3PiNU1iReBgDSdECkMgycZWtXHRAGPCA8uDfUEGfuM5/J26C4RxZTtr4u3FD53cFdfKOT92Evxbn9BuU/PHOObgO/Mb/lGEVtzhciuZUcdd2irhAecw8rWo51xbt4lbdBy3pZr2msWMSnkHb3dB4dvqPGt5RZ7JVEt1aXDEKHycH4vWNKn11xcMaSZ/cmOO7cf4mcKOZSNIkAx6qvTyPzcxKDNyJhpjNZrA0AjZriCVrbEAsSBLPRvsMSGSDTtzaqWesqri52CYtrtG1QuUoX4QBY/bD0fkpYjHFsDeEv3H0fm8Zjs99Qo97gb5eqtf5Hu++uVTE6cZ9GD/nFtwWB/DXxeWGW0DR4lNMe7j6DfweChIxWU7K3mHo2m4VNZw4j+gxZuj1PF/chPsBW5eHgBQ61yMNuRSsJIp05FtshAeumzGbSKE8TJCLdYarEvO1XaOTSattkr88pAeXODbS7cmeBAogrSUykuDpJyQWEyaN+0j+OqEkC3M88oAQKB8zAZLKTZ5FTpA2yajjeuLhrVLjgJgJOztQz/NNfb3crOaHAeTsq67+oOC2SN55ZNaKAcHxayGiMOD64mwm/tfHV8K1Y+L+jh4LByLOznd4HIdJvLj2VREKk6hKcoj508Q+qwEeAhxKv/LYtyqgZuA4Scoz7yRQXZKcJDxUSA2+CQyu3F9/43nfjkXAPsVAXiTDRPmwQJYubMRnlueBYIKwPmA8EL817eHscEhCSPRTjeezAtMAy0ZQiOVMYwgWUttBxvNBNiQc3Ja7pZtEOIdVYOA+zym0ZRigWVBLmBC8icbzNCOe4JhsTxNnUrCSPyAC5eUS/fCCRRLdXpwk5zPToGa+oESM8R6yqCrmMa50zvEfVZLOkoiwPzHDdTMxAFPG7TKy2GuMj4vzXvuASep7+nDd59uxCEigrtScs1H5SrStwCZnDVXBrkXx2+cK7xEGnIeIhs9J5+4zsbyovi9FpQHzJVIXqYBZbLLhPByXCXJ+we1Rh3c3HD8+BxEciatajkAYIEz9hziba5XX2trnGIz9m7pRqNtCohSNsFZSicEDFJdss1buSUPsqGEGxLPMw5MYo6jkzay2j3Q/u4qZ8o5RXnlXUJkCB1ZTSfx7LhNnOywHNwAfkipjMp1zg2tvJVEk4LmFPcRhQKzr17R9yHieqkCIGHG4RSYyJPNdjcfn3qDKZ65S2QRUeWtxIEw5C3K3CofWbsfxhPre7WzF+BlwIXxdQCESVRJCbP+0NFU7MQJbmI+Noyhmy3DGQRJQkT8m3b/hFENZlPA5E8wunsdEWiSMqVfjKCELSNRqf+eBeDh3K2dbRBpCKiqjUpYFTpxDhmx6+SYPpFAfiesBPfbvcrcKAzSuPTyOj03xipxjbyMU5PYLV2sXXIC9C9frOIoObKql+9yUGO0kL8jzYiWnrAvIg9xtVT72fI+Ei1ecLn4xofwenAw3lehXu4GKbSU37LLX5T4UYUBoEPG006X7EyTgGUH6S5/QXS0EzDZWyZyIiHxbqMOHxW4aYjKGiNJXgHAq/PfF2Vp7Bdx3ioV0zd9SlES3keFQj14DhvDUVEOVoieLsw9hYytqz8Vmy+zGIm1co0lSzo6LZ5vZO6nmJnF0m3oeF1Xmqsxg+m7CSYlMYFZn3qOS91A4uyJVreK1caxgPGD7I/IA0kNbIvoE27nvvcZv8L6cRfQI8N/Z49g4DotUyQJcc+vzyjN0S+q37nr/m5R31sV29iojZCjDuKjC45mQS6JKQnj5DimybReAN/THkq3KY4NlADJIIMY8uyjfEfiMQ4OZd5o41cvnwYqdIzgeUOVHxoa6AiN4kX3XRKIGQVlnUjsxVdwaXNaxE0cHJMNl+BwlLszIJ7ea8UAJNIgayYv7zX3nfkDM5FT73BOeFcxW2KeTEino91SxxarGuEOJYmHuVmHAsa2moUFxfoWswtoY8lmZ1cexjBRNAD9OVeogcB2miLsWO3ocrxloLV1L4AFFBjdhHB9ud0c8QQjO9UpOWR45JI0fWAszR9lZJ4mL5fOZQLBRsiqmyiSHp4VdIPkQYxevXmmVqM7Vo/fyekNcuzcY9yGI/9ocLysODdR4HxsoS0WcosdPCuBfpiTEPec+nOhxbAhmvKTfR8wP5GhbpTBsrHcX3BZb5U7G4xPVckdqNEYYcLyviJ9PBK0CIQK/xcAaR6GbUBj/54ibaH2BIFHUzNEWZJKDEgkv2EO7HVuZBtTkzGyWSPozLe2rvxsnwcnigrV9fjuxjcfrudON9E49IjPJN1qB353vZOgW3CJ6RHRYJXikretSvw0DUggPFTs5AzzK39QH+aXULXAKhQH3DUekTzB81kSIpGolOABJ5JOoIyZs0NZJn7KR96Qck2NBoG83HhNgLjlGr+mcUd+45bYf1eP/k5Cs4JdKDm7VYz3ruW8lyLvYqCYf70RHPICKcKkSVVLxAi8oeWIXQ2Uhv367nM2TgMTJOtff0H7lBYkT/M+1jdVRixSKlEb+fe+SqJuA8MpbSfR+fSieyfj+n8UuuaBa470/SY9dJK+aXH3CiXxINHnCdmSEQ8anLgE1J/IjFZyU+17jsdHQKB13e8r3JEnsbzwmIEQt1OudPiE6cF6SKogAso4V4ma7bs7KI1FsK3vnbJOGoUpF4sihas8pahMk9dOqDqgEioTArEiaoE8VIwY56x/9OCdSIAbOJB5SH1srpHBlzzqUHHBuWLPbkIjS413DgDA77PNW7YAVGr9uCMTmufAZsyRTYHtPMh1hY2VSsWoeqL/3Sl5xcSeFospbQwlR5RckqvIuxhf7sHVp6wFxEn8egQKuNXZpfp/1WaB6/T3GfSpHHokiwvt6KrFtIaY3CpzHCmwzGKgriY1UAmX5XmxsvpIHIRbfUvIstuKlA/nXVqdIDBxqXVVhMhEGhPm8zWNPipgke+VdbYEvipPgLcA8cKExkwXS8EksGZDRtQeaj+njRMFp8lCBtaWwt6LKW5/XxZKeXIE2ZpVsAc9m0aQA+su1jquAWSavGzNNMx1CKrkpsRBn9lHP4/LDfqTtX6P/q65gz4V+tewqnvobyRA6RtuR4hfChQf/e9rO1b5kRQiMhFPt8Pj7kigqjH9lo5F9YQkGyhsy8Cnv9lJkryqDeGkFK9JqSwII1CfL54eFcs1jOFs1D7WPjQ4VdjThuYw0TBAWE1FMKDhOitTHfE/ULOD5Icb4/lHfuHhWkhi2MB4TFfs0g50S8meCsdb3xd7aE8tnZ0mIVDuHSK2zA9sz+5+qxDLg37X2QgkUEpsh7mH3Ud2w45BmePWoikz5QJX3CbgGEByEV1VJQYqyUEeUwc/D+tBQnKNL/1viUZ2IMTVV7MkDSEPzUo7JX/Lhmegs43FR6jHTwYQfV+C3goyfpFx/VHns31bvNhrHrbl2XEd4TIQ7GI+P2enalCpcmLfos0UAitOG5xn2wY4bh5MVva88bzj/7jGcp21IJFElGGbO7aN/fUJvnlBi6ck0xKiuKFLNdPEz8gPMFN/W33in5/7YxZh9fST0OAmgfKX3MJgqLucbYP/aLWo4QAidWqjbzBOcZdnOnmZgR/dJ8SVOcV7KdyQkbB29LzoeG9HxHjT2g2MjnVvvDYQEgSRpRzxLVvMG/ZgnxaQtzCc+xWt4RtOkXKRmq00bu+pVMhxQnw0n9U8V+4TL8ecXKMHYEaRJoqSv7Ru9txIoEk1PpiFGNQAoN4e6abWtAW7eadrOVgIt4xmnH1ZjfQwGDk6scoVnw4AYPcwtSWrrFlFjHPyFUBwlDHiYsc/hKV6cMYDx5L7b2Buu6+16zNEE5KRQHCZxVlfR8Uj0xrzMNMZkoBK/37gPIJto/ihTSBjgpGzuvwVzxDnb0uGuD/21hiAxGWO+GU2iLjyNidQqOROdclXifUwGZhOWRLbGzWLKetS4T9swikSVaOJQjFiasKryzPwXlu9adYjiXT8jLsvFtwYAJchOU/KcU0GXUF987aE8pMsLOBrS4epAni7FKh5RsWefqBH+dYm4LKuk4/KbkGQt3nN+CDbLNK/85KifsVe76HhErbYtK+IkI6pp+UjSOFKSJlb6TXKA9X5jG763ACExGaNNWIUCNJnLUmqgMsFa8+MhZQi5mOQfBkj6CDNWZxvCw+zC5+kAkiRRnC1WSSIGg/BxJZpqnB4VICqDx9LDFI+1GskBg4O895n6u4rE6hUBA9jXeYMh/t1RfrZ9ITGXakokwsc8zk2fs6RQahRY1VZ+A3a/NA8x5ODjKcfxYI1gQIWlwpJVlces88tR9mMXdoSE6DPuyGMvEgONbd+6lAb9hPDS0khR47dO+S4N2G0J7yqqITGRE65mTa9lcry6wlq3pTGCRCOJjeIIvoVRB8TZtnoC+nuwbR0n/jm/8YJd31cCrXLdljKxs6iHpIqyxs9i055hQElAikh8xvPc2LtOyvj+ALEnKfAwZEmMqMFWtXJ1dExrlX/yw30yii5IKYiN9DlN/OKOCfXKngRcXU8KsPhElmTVnEC6tcbiospbAt+RQqcYzwEWSRULF1aIVkmUmz5V/JeoYPa8oEyHqoISKAtmkf/sq77j/Zuh5NmO4gYQepkEBKQlAs+/U3iPMMALP11c1IUPkLa+nroExvCaQdaF6Agkz1o5E4eXlYQgUQi0eH3JMEAyZ7K1EjbrWl2c8h1aHdKWT2ovUl2648SpwySH7Ju6TTK45mhCVyeWGnT2UB8nFdJtMUEjDAid9MnFR9olJrun6kW0kiiitTXWrBkY8zdSAkP9qjpLKfbKMkO+rOSW6FXUc+OpPEwcYWzucR5+A8vRnqLnsDolimJAyi3ryjU4NlpPCLtW2iqbqJHx+kaYM3xXJkAiOlXPk7XQIM6N7TO+TwIPHY6qLMnCh0S5Pmghk6VIPK1bVgQnm/X6cF1O1+syWqJz9lVMG4RK+TwLEFly7QWnUUwXR0RWQKJk+6WFyGH+8jE/wB275ZqYwgAtF23GR1AjQmR+N5YAycIaElXy4UZz46aUOB5OhfPEv7RbFmISpXIPD96oAaS/AWM4pcyQQq3qO2rlPG0nK3kWrdvoC9RwvK6+RRcAdity/A/QgYma3UwW/HbIhxCunUueh+tO2bhLU7dwweTTxM+O9pscJxnbWO3HjBMC87HtZ5NoGBALST7+FOM5ANWJrk/5jmcAhxiOIR9T0qHilmYmZpfxwkTCJIUWgqPKt7Ia12aWpK9dBQn6hM/hYd9L+7uv3s/RTkK3Hj3a0D+If2ghjrvRiQFdRrMkiipPOEuZB25rsT9IVjAof9b8gZInagH2LILffUOHeNjwsA7o8RigHNMy47EtZIIz4QEl4qx1ipBgUI+JqfT10sfYSfzseEWBU+30HAmDB4T0QEvKIdcLR11erC1Sno8TAVvq0frwPqd9H732jqu7iQ0Xp6OPTbFfWFUg/bpAopiSfNOmkQYpmnKguFhRxgnkgxOpjJZHjPFs7XfWqpg+kh59os9n6LU9X5yZBjKeLMOhTPuLf98xPbDmVvGFCzuEZhLF6+uTJ9tpoLquWXc7kqAxrn9bypXsY6b/kjipxGcQsQ/XEBMAS8imkyi2qDDADsjaSmVJtJ1ACjq5QKwlEt+2xmO/Gh0/L5jcZtscCUhyK73WpCDjrececZ+3i77zXdcKNfvEnLxtNCYmtzIrIkDuPhWU0oBET6ZdVrYbUnOZ5wgTBmUkITuIerOSx4uBkHNNBcepHEMkGhERal/ZKtOdwGMq5TU/eNwkCoiUvVGbib/NsBlF4xNJSEA9KWODbieuFGoKJNn7mhEGOE1Qia3XDinljgJhWjgRkNx96lmCncVvOY40EKN4ZAGJiOuCZFbV8uBVgInrJ5I9KWE+WVbBud4o9tVGs4CjLdn232X0Nb1CRD4G5U6C2ai1vBbE75Pn3A4g6XCj82qJAn7LzKiNke4v5xwDcsCuXUQCBUigFJe2qvLEKGY5qhwo9xcGF4nTkiZJZ69T67lQ4YlQsNQtaJeDtfV9EdDvy3Ny8ZmwKAB9qFfv2gO0kWu03+VTnduAmERRPVBteuEhzgJxaL+I/4ly/HEmvblrPRoJCGh2xnIgwyA9MAzIwT9CeseMgtSHHe5MQyUnHDjW2FC88nOkeClDMuAwfZRZcdYH8fNAWBfVuooWdQZcv0b0WqU0Sp8wI1DMeKrYnFYLo33Tgec7DFIXfqwAPBuYVCyaIyR6VXu6Ux59UYA9BFrGodQJMCAfVYJqLoRBn32X8GgHBqT4iowAyQB7Ls6bbhIppEZwN5lMtxVOKXVFewn8n+BxvhsKSxb9DdY+ouD1ueJX88AXXIi54iTQ2cYMMX4jGUdVqrQAQYK4VLSEfQz7oaIzaRdx0kG2OIaqHpP0G/MQFbk2Mez3mF77nvPKx0ASZZYkg6WTg9MKBjPq74KWzzHc++Q5twv3SNYqla1wZPVrJYgviMtl3yv6pl0qa+txkaowPZAgcc4QWdmAKm9d+QDJjAQGW+4z6/S4nH8CzH2TQdIQzxrN1waVdo6w2GC2JzsNTJCQxpEV9WlAnGOFAjqQMxqZRcJFy5hdsDgINn3GI6Ym3zXAmsfagLiaC5RcJKnBEmS/JNq3ZwGJMjCRJnxS0zoJbI1rqp5HQfWkjk3qWo9GApXjslHLIhdBf+NuJQiWYThVHJG2617EgxopCYkeJ8N3U1a+zIarHkRcsc/ke5k4ScqG/saZel5Ua+Jjq5w8m8mTmEziHLEJ+1cKwvEUBmeJq+Dks15Xc59cUL9LeHhNj8u4t1aawv5cbJJ0Kj3FoIlgmGY8T4y4/0i1M/SYP9Rj4rugwIslIoV46rmefegIIFECd3tZCgXckN+2qPIEHvsU0WgXGOhF1pRJhiNSUgS5H1TlwclX5X1BjYMsF4mTkHhInvIqYuLA5Mt6VNYQLTzD15dwEiA1E6YDkWIjLesM5fcPiCNPTDEQ32MV1aokDpZ+UvzbGgIWT3T0CQK9NyJQfu9BxmPFK00U/01Upg+DY6P/SF6xLvGzSFw6L3bkOLsQ80ORymHN4Hf3RAX7NMTq/H+LE7HjheV6Ac1pnkMre7Z8z0OIxMBDjFTT6VCSuH+cd0hFVZIvFwjswmbO0sHLb0XSIvkBezWExXWI1zXndXXUB5yC3LdmhwcPIKTJNeIBQiWaG7VnK1qXhrFDiBYhP8sk2wTRiPpMv4ifXeR9VmcCuUuvERINkRk4nKaIm3DImpkgw0tNoIpyb+Jrw+dcE8iE68N1WSBOy8EG+HylHmCnOv9E+4ppANs3JELsJ/cyfvZek2HVnX5xLVG9fyku+ePJlhx37j8OObKOVke/L+268x1OVxIa5pqXfYH8woB6AkzsXOe3yPD4i/0Qcf+5xvSTiZrwPUj70ZZJGrPIFdG2r6b0u/m5fyb6nT0NbiR54r7SSKfQSFhPCQP756P39L+b5B+vbFoNXAjN73UA84DHVfBRCSeLIwyIggcOUkIaGxBXcYqB58rVOVvnkuj9iqH11KsFweuk8OU9yM1oGAr2ZsOVQrsvkpZ4sIlTJeh/NxkmUUiW3/149BkPPw8mpIZNdmliEY6q0d94RPtJZhQZR9iQyeqDTJkAIZ34GkJ2ENDzGaXeMDEg3RYReOIsupXeGoezk5+n/b+kqf+Y0OKq9yxICSlyjRlzD2bYkOeII9GiySyrK1jvq+34f9MfPzuk2AKrAAAAAElFTkSuQmCC" name="Immagine2" hspace="12" width="160" height="27" border="0"/></td></tr></table>
		<br />
		</div>
		<table cellpadding="4" width="580pt" cellspacing="0">

			<tr>
				<td colspan="7" height="13" valign="top" bgcolor="#eeeeee" style="border: 1px solid #000000; padding: 0.1cm">
					
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 11pt"><b>ALLEGATO
					A &ndash; PROPOSTA DI ATTIVAZIONE CENTRALINO VIRTUALE EZCLOUD
					BASIC</b></font></font></p>
				</td>
			</tr>
			<tr>
				<td colspan="7" width="580pt" height="17" bgcolor="#ffffff" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0.1cm">
					<table style="width:580pt"><tr><td style="width:16.2%"><font face="Arial, sans-serif"><font size="2" style="font-size: 11pt"><b>Cliente
															</b> </font></font>   </td><td style="width:16.2%"><font face="Arial, sans-serif"><font size="2" style="font-size: 11pt">'.$cliente_nome.'</font></font></td><td style="width:16.2%">         <font face="Arial, sans-serif"><font size="2" style="font-size: 11pt"><b>    Codice   
							</b></font></font></td><td style="width:16.2%"><font face="Arial, sans-serif"><font size="2" style="font-size: 11pt">'.$cliente['id_customer'].'</font></font></td><td style="width:16.2%"><font face="Arial, sans-serif"><font size="2" style="font-size: 11pt"><b>Cod.
					Agente</b></font></font></td><td style="width:16.2%"></td></tr>
					<tr><td style="width:16.2%"><font face="Arial, sans-serif"><font size="2" style="font-size: 11pt"><b>Interni
															</b> </font></font>   </td><td style="width:16.2%"><font face="Arial, sans-serif"><font size="2" style="font-size: 11pt">'.$dati['interni'].'</font></font></td><td style="width:16.2%">         <font face="Arial, sans-serif"><font size="2" style="font-size: 11pt"><b>    Linee   
							</b></font></font></td><td style="width:16.2%"><font face="Arial, sans-serif"><font size="2" style="font-size: 11pt">'.$dati['linee'].'</font></font></td><td style="width:16.2%"><font face="Arial, sans-serif"><font size="2" style="font-size: 11pt"><b>Canali</b></font></font></td><td style="width:16.2%"><font face="Arial, sans-serif"><font size="2" style="font-size: 11pt">'.$dati['canali'].'</font></font></td></tr>
					<tr><td style="width:16.2%"><font face="Arial, sans-serif"><font size="2" style="font-size: 11pt"><b>Attivazione
															</b> </font></font>   </td><td style="width:16.2%"><font face="Arial, sans-serif"><font size="2" style="font-size: 11pt">'.($dati['attivazione'] == 1 ? 'S&igrave;' : '').'</font></font></td><td style="width:16.2%">         <font face="Arial, sans-serif"><font size="2" style="font-size: 11pt"><b>    Conf. iniziale   
							</b></font></font></td><td style="width:16.2%"><font face="Arial, sans-serif"><font size="2" style="font-size: 11pt">'.($dati['configurazione_iniziale'] == 1 ? 'S&igrave;' : '').'</font></font></td><td style="width:16.2%"><font face="Arial, sans-serif"><font size="2" style="font-size: 11pt"><b>Scadenza</b></font></font></td><td style="width:16.2%"></td></tr>
					</table>
					<br />

				</td>
			</tr>
			<tr>
				<td colspan="7" width="580pt" height="11" valign="top" bgcolor="#eeeeee" style="vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0.1cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"><b>Costo
					mensile servizio EzCloud</b></font></font></p>
				</td>
			</tr>
			<tr valign="top">
				<td height="11" bgcolor="#eeeeee" style="width:50pt; vertical-align:middle; line-height:11; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"><b>Codice</b></font></font></p>
				</td>
				<td height="11" bgcolor="#eeeeee" style="width:150pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"><b>Descrizione</b></font></font></p>
				</td>
				<td height="11" bgcolor="#eeeeee" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"><b>Quantità</b></font></font></p>
				</td>
				<td  height="11" bgcolor="#eeeeee" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"><b>Unitario</b></font></font></p>
				</td>
				<td  height="11" bgcolor="#eeeeee" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"><b></b></font></font></p>
				</td>
				<td height="11" bgcolor="#eeeeee" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"><b>Importo</b></font></font></p>
				</td>
				<td height="11" bgcolor="#eeeeee" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"><b>Netto</b></font></font></p>
				</td>
			</tr>
			'.($serv_code != '' ? '<tr valign="middle">
				<td height="11" bgcolor="#ffffff" style="width:20pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.$serv_code.'</font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:150pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.$serv_desc.'</font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="right"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.$serv_qtan.'</font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="right"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.number_format($serv_impo,2,",","").'</font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="right"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="right"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.number_format($serv_impo,2,",","").'</font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="right"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.number_format($serv_nett,2,",","").'</font></font></p>
				</td>
			</tr>' : '').'
			<tr>
				<td colspan="7" width="580pt" valign="top" bgcolor="#ffffff" style="height:20pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0.1cm">
					<br />
				</td>
			</tr>
			<tr>
				<td colspan="7" width="580pt" height="11" valign="top" bgcolor="#eeeeee" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0.1cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"><b>Terminali, dispositivi, applicazioni e servizi opzionali</b></font></font></p>
				</td>
			</tr>
			<tr valign="middle">
				<td height="11" bgcolor="#eeeeee" style="width:50pt; line-height:11; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"><b>Codice</b></font></font></p>
				</td>
				<td height="11" bgcolor="#eeeeee" style="width:150pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"><b>Descrizione</b></font></font></p>
				</td>
				<td height="11" bgcolor="#eeeeee" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"><b>Quantità</b></font></font></p>
				</td>
				<td  height="11" bgcolor="#eeeeee" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"><b>Unitario</b></font></font></p>
				</td>
				<td  height="11" bgcolor="#eeeeee" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"><b>Sconto</b></font></font></p>
				</td>
				<td height="11" bgcolor="#eeeeee" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"><b>Importo</b></font></font></p>
				</td>
				<td height="11" bgcolor="#eeeeee" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"><b>Netto</b></font></font></p>
				</td>
			</tr>
			'.($attv_code != '' ? '<tr valign="middle">
				<td height="11" bgcolor="#ffffff" style="width:20pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.$attv_code.'</font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:150pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.$attv_desc.'</font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="right"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.$attv_qtan.'</font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="right"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.number_format($attv_unit,2,",","").'</font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="right"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.number_format($attv_scon,2,",","").'</font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="right"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.number_format($attv_impo,2,",","").'</font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="right"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.number_format($attv_nett,2,",","").'</font></font></p>
				</td>
			</tr>' : '').($conf_code != '' ? '<tr valign="middle">
				<td height="11" bgcolor="#ffffff" style="width:20pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.$conf_code.'</font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:150pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.$conf_desc.'</font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="right"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.$conf_qtan.'</font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="right"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.number_format($conf_unit,2,",","").'</font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="right"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.number_format($conf_scon,2,",","").'</font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="right"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.number_format($conf_impo,2,",","").'</font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="right"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt">'.number_format($conf_nett,2,",","").'</font></font></p>
				</td>
			</tr>' : '').'<tr valign="middle">
				<td height="11" bgcolor="#ffffff" style="height:20pt; width:50pt; line-height:11; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:150pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td  height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td  height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
			</tr><tr valign="middle">
				<td height="11" bgcolor="#ffffff" style="height:20pt; width:50pt; line-height:11; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:150pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td  height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td  height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
			</tr><tr valign="middle">
				<td height="11" bgcolor="#ffffff" style="height:20pt; width:50pt; line-height:11; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:150pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td  height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td  height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
			</tr><tr valign="middle">
				<td height="11" bgcolor="#ffffff" style="height:20pt; width:50pt; line-height:11; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:150pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td  height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td  height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
			</tr><tr valign="middle">
				<td height="11" bgcolor="#ffffff" style="height:20pt; width:50pt; line-height:11; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:150pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td  height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td  height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
			</tr><tr valign="middle">
				<td height="11" bgcolor="#ffffff" style="height:20pt; width:50pt; line-height:11; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:150pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td  height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td  height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
			</tr><tr valign="middle">
				<td height="11" bgcolor="#ffffff" style="height:20pt; width:50pt; line-height:11; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:150pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td  height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td  height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
			</tr><tr valign="middle">
				<td height="11" bgcolor="#ffffff" style="height:20pt; width:50pt; line-height:11; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:150pt; vertical-align:middle; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td  height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td  height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
				<td height="11" bgcolor="#ffffff" style="width:30pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"></font></font></p>
				</td>
			</tr><tr>
				<td colspan="7" width="580pt" height="11" valign="top" bgcolor="#ffffff" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0.1cm">
					<p align="center"><font face="Arial, sans-serif"><font size="2" style="font-size: 9pt"><b>Servizi e risorse inclusi nel pacchetto basic</b></font></font></p>
				</td>
			</tr>
			<tr valign="top">
				<td colspan="2" style="width:260pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
					<p align="left"><font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Fino
					a 4 conversazioni contemporanee, fino a 2 linee VoIP SIP
					(espandibili) </font></font>
					<br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Fino
					a 4 dispositivi configurabili (espandibili)</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Chiamate
					interne ed esterne</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Attesa
					e ripresa linea</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Attesa
					con melodia musicale</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Trasferta
					delle chiamate tra i vari interni</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Paging
					- Chiamata generale e singola via amplificatore (cercapersone)</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Blocco
					chiamate in uscita</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">LCR
					(least cost routing)</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Conferenza
					a 3</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Deviazione
					chiamata (Non risp./occupato)</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">IVR
					con Orari personalizzabili giorno/settimana festivit&agrave;</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Prompts
					vocali di sistema in Italiano</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Messaggi
					vocali personalizzabili via telefono o file audio .wav 16 bit 8
					KHz</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Messaggio
					vocale mattino, pausa pranzo, pomeridiano, chiusura serale</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Messaggio
					weekend </font></font>
					<br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Messaggio
					per festivi&agrave;</font></font></p>
				</td>
				<td colspan="6" style="width:250pt; border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0.1cm">
					<p align="left"><font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Selezione
					derivati durante messaggio vocale</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">SIP
					forking (stesso interno su telefono fisso e smartphone)</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Voice
					Mail (segreteria telefonica per ogni interno)</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Riascolto
					messaggi segreteria </font></font>
					<br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Possibilit&agrave;
					di scaricare i messaggi vocali da pannello web</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Inclusione
					in conversazione in corso e inclusione &ldquo;suggeritore&rdquo;</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Non
					disturbare (DND)</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Rubrica
					web</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Sveglia
					- Promemoria</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Fax
					in E-Mail*</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">CDR
					(visualizz. traffico entrante/uscente da pannello web)</font></font><br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Accesso
					protetto al proprio centralino virtuale </font></font>
					<br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Utilizzo
					di softphone mobile iOS e Android </font></font>
					<br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Utilizzo
					di softphone desktop </font></font>
					<br />
					<font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">EzDial:
					Servizio selezione veloce (windows- Fanvil X , Snom D, Yealink)</font></font></p>
				</td>
			</tr><tr>
				<td colspan="7" width="580pt" valign="top" bgcolor="#ffffff" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0.1cm">
					<p align="center"><font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">Ezdirect
					pu&ograve; fornire linee e connettivit&agrave; adsl  in
					partnership con l&rsquo;operatore Italiano Voip Voice. Ezdirect
					attiva i servizi per nome e conto del Cliente.<br/>
		In questi
					casi, la fatturazione (e i pagamenti relativi) sono gestiti da
					VoipVoice. </font></font>
					<br /><font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">L&rsquo;attivazione
					di eventuali servizi di operatore non &egrave; inclusa nelle
					condizioni di contratto Ezdirect. Richiedono modulistica specifica
					fornita da Voip Voice.</font></font><br /><font face="Arial, sans-serif"><font size="1" style="font-size: 8pt">*
					Per la particolare configurazione delle linee IP e protocolli &ndash;
					standard, utilizzati nella gestione dei fax su IP, non pu&ograve;
					essere garantito al 100% questo servizio.</font></font></p>
				</td>
			</tr>
		</table>

		</div></page>';

		return $content;	
		
	}	
	
	public function openUploadedFile()
	{
		$filename = $_GET['filename'];
		
		if(strpos($filename, ":::")) {
			$parti = explode(":::", $filename);
			$nomecodificato = $parti[0];
			$nomevero = $parti[1];
		}
		else {
			$nomecodificato = $filename;
			$nomevero = $filename;
		}
		
		$extensions = array('.txt' => 'text/plain', '.rtf' => 'application/rtf', '.doc' => 'application/msword', '.docx'=> 'application/msword',
		'.pdf' => 'application/pdf', '.zip' => 'multipart/x-zip', '.png' => 'image/png', '.jpeg' => 'image/jpeg', '.gif' => 'image/gif', '.jpg' => 'image/jpeg');

		$extension = '';
		foreach ($extensions AS $key => $val)
			if (substr($nomevero, -4) == $key OR substr($nomevero, -5) == $key) {
				$extension = $val;
				break;
			}

		echo $nomecodificato."<br />";
		echo $nomevero;
		ob_end_clean();
		header('Content-Type: '.$extension);
		header('Content-Disposition:attachment;filename="'.$nomevero.'"');
		readfile(_PS_UPLOAD_DIR_.$nomecodificato);
		die;
	}
	
	public static function getCarriersForEditOrder($id_zone, $groups = null, $id_cart)
	{
		$context = Context::getContext();
		$cart = new Cart($id_cart);
		
		if (is_array($groups) && !empty($groups))
			$result = Carrier::getCarriers((int)$context->language->id, false, false, (int)$id_zone, $groups, Carrier::PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE);
		else
			$result = Carrier::getCarriers((int)$context->language->id, false, false, (int)$id_zone, array(1), Carrier::PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE);
		
		$resultsArray = array();

		foreach ($result as $k => $row)
		{
			$carrier = new Carrier((int)$row['id_carrier']);
			$shippingMethod = $carrier->getShippingMethod();
			if ($shippingMethod != Carrier::SHIPPING_METHOD_FREE)
			{
				if($row['active'] == 0 && $row['name'] != 'Trasporto gratuito') {
					unset($result[$k]);
					continue;
				}

				if($id_zone == 0) {
					$id_zone = 9;
				}
				// Get only carriers that are compliant with shipping method
				if (($shippingMethod == Carrier::SHIPPING_METHOD_WEIGHT && $carrier->getMaxDeliveryPriceByWeight($id_zone) === false)
					|| ($shippingMethod == Carrier::SHIPPING_METHOD_PRICE && $carrier->getMaxDeliveryPriceByPrice($id_zone) === false))
				{
					unset($result[$k]);
					continue;
				}

				// If out-of-range behavior carrier is set on "Desactivate carrier"
				if ($row['range_behavior'])
				{
					// Get id zone
					if (!$id_zone)
						$id_zone = Country::getIdZone(Country::getDefaultCountryId());

					// Get only carriers that have a range compatible with cart
					if (($shippingMethod == Carrier::SHIPPING_METHOD_WEIGHT && (!Carrier::checkDeliveryPriceByWeight($row['id_carrier'], $cart->getTotalWeight(), $id_zone)))
						|| ($shippingMethod == Carrier::SHIPPING_METHOD_PRICE
							&& (!Carrier::checkDeliveryPriceByPrice($row['id_carrier'], $cart->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING), $id_zone, $cart->id_currency))))
					{
						unset($result[$k]);
						continue;
					}
				}
			}
			
			$row['name'] = (strval($row['name']) != '0' ? $row['name'] : Configuration::get('PS_SHOP_NAME'));
			$row['price'] = ($shippingMethod == Carrier::SHIPPING_METHOD_FREE ? 0 : $cart->getOrderShippingCost((int)$row['id_carrier']));
			$row['price_tax_exc'] = ($shippingMethod == Carrier::SHIPPING_METHOD_FREE ? 0 : $cart->getOrderShippingCost((int)$row['id_carrier'], false));
			$row['img'] = file_exists(_PS_SHIP_IMG_DIR_.(int)($row['id_carrier']).'.jpg') ? _THEME_SHIP_DIR_.(int)($row['id_carrier']).'.jpg' : '';

			// If price is false, then the carrier is unavailable (carrier module)
			if ($row['price'] === false)
			{
				unset($result[$k]);
				continue;
			}

			$resultsArray[] = $row;
		}

		return $resultsArray;
	}
	
	public function getProductsOld($refresh = false, $id_product = false)
	{
		global $cookie;
		if (!$this->id)
			return array();
			
		$provvisorio = Db::getInstance()->getValue('SELECT provvisorio FROM '._DB_PREFIX_.'cart WHERE id_cart = '.(int)$this->id.'');
		if ($provvisorio == 1) {
			$cookie->id_cart = '';
		
		}
		
		// Product cache must be strictly compared to NULL, or else an empty cart will add dozens of queries
		if ($this->_products !== NULL AND !$refresh)
			return $this->_products;
		$sql = '
			SELECT cp.`id_product_attribute`, cp.`id_product`, cp.sort_order, cu.`id_customization`, cp.`quantity` AS cart_quantity, cp.`price` AS cart_price, cp.section, cp.prezzo_acquisto AS cart_wholesale_price, cp.no_acq AS no_acq, cp.`free` AS cart_free,  cp.`name` AS cart_name, cp.bundle AS cart_bundle, cu.`quantity` AS customization_quantity, pl.`name`,
			pl.`description_short`, pl.`available_now`, pl.`available_later`, p.`id_product`, p.id_tax_rules_group, p.`id_category_default`, p.`id_supplier`, p.`id_manufacturer`, p.`on_sale`, p.`ecotax`, p.`additional_shipping_cost`, p.`date_available`, p.`available_for_order`,
			p.`quantity`, p.`price`, p.`weight`, p.`width`, p.`height`, p.`depth`, p.`out_of_stock`, p.`active`, p.`date_add`, p.`date_upd`, IFNULL(pa.`minimal_quantity`, p.`minimal_quantity`) as minimal_quantity,
			t.`id_tax`, tl.`name` AS tax, t.`rate`, pa.`price` AS price_attribute, pa.`quantity` AS quantity_attribute,
			pa.`ecotax` AS ecotax_attr, pl.`link_rewrite`, cl.`link_rewrite` AS category, CONCAT(cp.`id_product`, cp.`id_product_attribute`) AS unique_id,
			IF (IFNULL(pa.`reference`, \'\') = \'\', p.`reference`, pa.`reference`) AS reference,
			IF (IFNULL(pa.`supplier_reference`, \'\') = \'\', p.`supplier_reference`, pa.`supplier_reference`) AS supplier_reference,
			(p.`weight`+ pa.`weight`) weight_attribute,
			IF (IFNULL(pa.`ean13`, \'\') = \'\', p.`ean13`, pa.`ean13`) AS ean13, IF (IFNULL(pa.`upc`, \'\') = \'\', p.`upc`, pa.`upc`) AS upc,
			pai.`id_image` pai_id_image, il.`legend` pai_legend
			FROM `'._DB_PREFIX_.'cart_product` cp
			LEFT JOIN `'._DB_PREFIX_.'product` p ON p.`id_product` = cp.`id_product`
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)$this->id_lang.')
			LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON (pa.`id_product_attribute` = cp.`id_product_attribute`)
			LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (p.`id_tax_rules_group` = tr.`id_tax_rules_group`
				AND tr.`id_country` = '.(int)Country::getDefaultCountryId().'
				AND tr.`id_state` = 0)
			LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
			LEFT JOIN `'._DB_PREFIX_.'tax_lang` tl ON (t.`id_tax` = tl.`id_tax` AND tl.`id_lang` = '.(int)$this->id_lang.')
			LEFT JOIN `'._DB_PREFIX_.'customization` cu ON (cp.`id_product` = cu.`id_product` AND cp.`id_product_attribute` = cu.`id_product_attribute` AND cu.`id_cart` = cp.`id_cart`)
			LEFT JOIN `'._DB_PREFIX_.'product_attribute_image` pai ON (pai.`id_product_attribute` = pa.`id_product_attribute`)
			LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (il.`id_image` = pai.`id_image` AND il.`id_lang` = '.(int)$this->id_lang.')
			LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (p.`id_category_default` = cl.`id_category` AND cl.`id_lang` = '.(int)$this->id_lang.')
			WHERE cp.`id_cart` = '.(int)$this->id.'
			'.($id_product ? ' AND cp.`id_product` = '.(int)$id_product : '').'
			-- AND (cp.bundle != 999999 AND cp.bundle != 88888878) --
			AND p.`id_product` IS NOT NULL
			GROUP BY unique_id
			ORDER BY cp.sort_order ASC, cp.date_add ASC
		';
		
		

		$result = Db::getInstance()->ExecuteS($sql);
		// Reset the cache before the following return, or else an empty cart will add dozens of queries

		$productsIds = array();
		$paIds = array();
		foreach ($result as $row) {
			$productsIds[] = $row['id_product'];
			$paIds[] = $row['id_product_attribute'];
		}
		// Thus you can avoid one query per product, because there will be only one query for all the products of the cart
		Product::cacheProductsFeatures($productsIds);
		self::cacheSomeAttributesLists($paIds, $this->id_lang);
		
		$this->_products = array();
		if (empty($result))
			return array();
		foreach ($result AS $row) {
			if (isset($row['ecotax_attr']) AND $row['ecotax_attr'] > 0)
				$row['ecotax'] = (float)($row['ecotax_attr']);
			$row['stock_quantity'] = (int)($row['quantity']);
			// for compatibility with 1.2 themes
			$row['quantity'] = (int)($row['cart_quantity']);
			if (isset($row['id_product_attribute']) AND (int)$row['id_product_attribute']) {
				$row['weight'] = $row['weight_attribute'];
				$row['stock_quantity'] = $row['quantity_attribute'];
			}
			
			if(empty($row['cart_name'])) {
				$row['name'] = $row['name'];
			}
			else {
				$row['name'] = $row['cart_name'];
			}
			
			if($row['section'] == '' || empty($row['section']))
				$row['section'] = 0;
			
			
			if ($this->_taxCalculationMethod == PS_TAX_EXC) {
				if($row['cart_price'] == 0 && $row['cart_free'] == 0) {
					
					$id_group = Customer::getDefaultGroupId($this->id_customer);
					if($id_group == 0) {
						$id_group = 1;
					}
					
					$quantity_now = Db::getInstance()->getValue("SELECT quantity FROM "._DB_PREFIX_."cart_product WHERE id_product = ".$row['id_product']." AND id_cart = ".$this->id."");
					
					$row['price'] = Db::getInstance()->getValue("SELECT price FROM "._DB_PREFIX_."cart_product WHERE id_product = ".$row['id_product']." AND id_cart = ".$this->id."");
					
					if($row['price'] == 0) {
						$row['price'] = Product::trovaMigliorPrezzo($row['id_product'],$id_group,$quantity_now);
					}
					//$row['price'] = Product::getPriceStatic((int)$row['id_product'], false, isset($row['id_product_attribute']) ? (int)($row['id_product_attribute']) : NULL, 2, NULL, false, true, (int)($row['cart_quantity']), false, ((int)($this->id_customer) ? (int)($this->id_customer) : NULL), (int)($this->id), ((int)($this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}) ? (int)($this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}) : NULL), $specificPriceOutput); // Here taxes are computed only once the quantity has been applied to the product price
				} 
				else if($row['cart_price'] > 0 && $row['cart_free'] == 0) {
					$row['price'] = $row['cart_price'];
				}
				
				else if ($row['cart_free'] == 1){
					$row['price'] = 0;
				}
				$row['price_wt'] = Product::getPriceStatic((int)$row['id_product'], true, isset($row['id_product_attribute']) ? (int)($row['id_product_attribute']) : NULL, 2, NULL, false, true, (int)($row['cart_quantity']), false, ((int)($this->id_customer) ? (int)($this->id_customer) : NULL), (int)($this->id), ((int)($this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}) ? (int)($this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}) : NULL));
				$tax_rate = Tax::getProductTaxRate((int)$row['id_product'], (int)($this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}));

				$row['total_wt'] = Tools::ps_round($row['price'] * (float)$row['cart_quantity'] * (1 + (float)($tax_rate) / 100), 2);
				$row['total'] = $row['price'] * (int)($row['cart_quantity']);
			}
			else {
				if($row['cart_price'] == 0 && $row['cart_free'] == 0) {
					$row['price'] = Product::getPriceStatic((int)$row['id_product'], false, (int)$row['id_product_attribute'], 6, NULL, false, true, $row['cart_quantity'], false, ((int)($this->id_customer) ? (int)($this->id_customer) : NULL), (int)($this->id), ((int)($this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}) ? (int)($this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}) : NULL), $specificPriceOutput);
				} else if($row['cart_price'] > 0 && $row['cart_free'] == 0) {
				
					$row['price'] = $row['cart_price'];
					
				}
				else if ($row['cart_free'] == 1)
				{
					$row['price'] = 0;
				}
				$row['price_wt'] = Product::getPriceStatic((int)$row['id_product'], true, (int)$row['id_product_attribute'], 2, NULL, false, true, $row['cart_quantity'], false, ((int)($this->id_customer) ? (int)($this->id_customer) : NULL), (int)($this->id), ((int)($this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}) ? (int)($this->{Configuration::get('PS_TAX_ADDRESS_TYPE')}) : NULL));

				/* In case when you use QuantityDiscount, getPriceStatic() can be return more of 2 decimals */
				$row['price_wt'] = Tools::ps_round($row['price_wt'], 2);
				$row['total_wt'] = $row['price_wt'] * (int)($row['cart_quantity']);
				$row['total'] = Tools::ps_round($row['price'] * (int)($row['cart_quantity']), 2);
			}

			if (!isset($row['pai_id_image']) OR $row['pai_id_image'] == 0) {
				$row2 = Db::getInstance()->getRow('
				SELECT i.`id_image`, il.`legend`
				FROM `'._DB_PREFIX_.'image` i
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$this->id_lang.')
				WHERE i.`id_product` = '.(int)$row['id_product'].' AND i.`cover` = 1');
				if (!$row2)
					$row2 = array('id_image' => false, 'legend' => false);
				else
					$row = array_merge($row, $row2);
			}
			else {
				$row['id_image'] = $row['pai_id_image'];
				$row['legend'] = $row['pai_legend'];
			}

			
			$row['reduction_applies'] = ($specificPriceOutput AND (float)$specificPriceOutput['reduction']);
			$row['id_image'] = Product::defineProductImage($row, $this->id_lang);
			$row['allow_oosp'] = Product::isAvailableWhenOutOfStock($row['out_of_stock']);
			$row['features'] = Product::getFeaturesStatic((int)$row['id_product']);
			$row['real_quantity'] = Db::getInstance()->getValue('SELECT quantity FROM '._DB_PREFIX_.'product WHERE id_product = '.$row['id_product'].'');
			
			$row['quantity_def'] = Db::getInstance()->getValue('SELECT quantity FROM '._DB_PREFIX_.'product WHERE id_product = '.$row['id_product'].'');
			
			$supplier = Db::getInstance()->getValue('SELECT id_supplier FROM '._DB_PREFIX_.'product WHERE id_product = '.$row['id_product']);
		
			switch($supplier) {
				case 11: $main_supplier = 'supplier_quantity '; break;
				case 8: $main_supplier = 'esprinet_quantity '; break;
				case 42: $main_supplier = 'itancia_quantity '; break;
				case 76: $main_supplier = 'asit_quantity '; break;
				case 95: $main_supplier = 'intracom_quantity '; break;
				case 1450: $main_supplier = 'attiva_quantity '; break;
				default: $main_supplier = 'stock_quantity'; break;
			}	
			
			$dispo_type = Db::getInstance()->getValue('SELECT dispo_type FROM '._DB_PREFIX_.'product WHERE id_product = '.$row['id_product']);
			switch($dispo_type)
			{
				case 0: $row['quantity_def'] = $row['quantity']; break;
				case 1: $row['quantity_def'] = $row['stock_quantity']; break;
				case 2: $row['quantity_def'] = Db::getInstance()->getValue('SELECT '.$main_supplier.' AS main_quantity FROM '._DB_PREFIX_.'product WHERE id_product = '.$row['id_product']) + $row['stock_quantity']; break;
				default: $row['quantity_def'] = $row['quantity']; break;
			}	 
			
			//$esprinet_blacklist = array(25,120,43,117,133,109,37,10);
		
			/*if(in_array($row['id_manufacturer'],$esprinet_blacklist))
				$row['quantity_def'] =  Db::getInstance()->getValue('SELECT supplier_quantity  AS main_quantity FROM product WHERE id_product = '.$row['id_product']);
			
			if($row['id_manufacturer'] == 17)
				$row['quantity_def'] = $row['stock_quantity'];*/
			
			$qta_ord_clienti = Product::calcolaImpegnato($row['id_product']);
			
			$row['quantity_def'] -= $qta_ord_clienti;
			//verificare perché $row['quantity'] rimane sempre a 1, real_quantity introdotta per risolvere questo problema
			if (array_key_exists($row['id_product_attribute'].'-'.$this->id_lang, self::$_attributesLists))
				$row = array_merge($row, self::$_attributesLists[$row['id_product_attribute'].'-'.$this->id_lang]);

			$this->_products[] = $row;
		}

		return $this->_products;
	}
    

}