{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}

	{* Stile della pagina *}
	{include file="./order_stile.tpl"}

	{* Javascript della pagina *}
	{include file="./script.tpl"} {* Di Prestashop *}
	{include file="./js/script.tpl"} {* Override *}

	{*include file="./js.tpl"*}

	{assign var="hook_invoice" value={hook h="displayInvoice" id_order=$order->id}}
	{if ($hook_invoice)}
	<div>{$hook_invoice}</div>
	{/if}

	{* Riassunto dati cliente *}
	{if $riassunto_on && !$not_mio_agente}
		<div class="row">
			{include file="./summary.tpl"}
		</div>
	{/if}

	{* <div class="panel kpi-container">
		<div class="row">
			<div class="col-xs-6 col-sm-3 box-stats color3" >
				<div class="kpi-content">
					<i class="icon-calendar-empty"></i>
					<span class="title">{l s='Date'}</span>
					<span class="value">{dateFormat date=$order->date_add full=false}</span>
				</div>
			</div>
			<div class="col-xs-6 col-sm-3 box-stats color4" >
				<div class="kpi-content">
					<i class="icon-money"></i>
					<span class="title">{l s='Total'}</span>
					<span class="value">{displayPrice price=$order->total_paid_tax_incl currency=$currency->id}</span>
				</div>
			</div>
			<div class="col-xs-6 col-sm-3 box-stats color2" >
				<div class="kpi-content">
					<i class="icon-comments"></i>
					<span class="title">{l s='Messages'}</span>
					<span class="value"><a href="{$link->getAdminLink('AdminCustomerThreads')|escape:'html':'UTF-8'}&amp;id_order={$order->id|intval}">{sizeof($customer_thread_message)}</a></span>
				</div>
			</div>
			<div class="col-xs-6 col-sm-3 box-stats color1" >
				<a href="#start_products">
					<div class="kpi-content">
						<i class="icon-book"></i>
						<span class="title">{l s='Products'}</span>
						<span class="value">{sizeof($products)}</span>
					</div>
				</a>
			</div>
		</div>
	</div> *}	

	{if isset($alert)}
		<div class="col-lg-12">
			<div class="row">
				<div class="alert alert-danger">{$alert}</div> {* Alert azioni in sospeso - note private - note nell'ordine *}
			</div>
		</div>
	{/if}

	{include file="./pulsanti.tpl"} {* Riga di pulsanti *}

	<div class="row">
		<input type="hidden" name="id_order" id="id_order" value="{$order->id}" />
		<input type="hidden" name="id_customer" id="id_customer" value="{$order->id_customer}" />
		<input type="hidden" name="id_address" id="id_address" value="{$order->id_address_delivery}" />
		
		{* Informazioni *}
		<div class="col-lg-12">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-user"></i> 
					Informazioni
					{if !$has_pa && $abilita_modifica}
					<div class="bottoni-tab">
						<a class="btn btn-default" id="inserisci_pa" onclick="modifica_pa()">
							<i class="icon-plus-sign"></i> Inserisci dati P.A.
						</a>
					</div>
					{/if}
				</div>

				<div class="form-row">

					<div class="row">
						{* <div class="form-group col-md-2">
							<label for="tipo_cliente">Tipo cliente</label>
							<p class="form-control-static">
								{if $customer->is_company}<i class="icon-building"></i> Azienda{else}<i class="icon-user"></i> Persona{/if}
							</p>
						</div>

						<div class="form-group col-md-4">
							<label for="ragione_sociale">Ragione sociale</label>
							<p class="form-control-static">
								{if ! isset($customer->company)}
									{$customer->firstname}
									{$customer->lastname}
								{else}
									{$customer->company}
									({$customer->firstname}
									{$customer->lastname})
								{/if}
							</p>
						</div> *}

						<div class="form-group col-md-2">
							<label for="data_ordine">Data ordine</label>
							<p class="form-control-static">
								{$order->date_add}
							</p>
						</div>

						<form name="form_rif_ordine" id="form_rif_ordine" action="ajax.php" method="post" onsubmit="saveRifOrdine(); return false;">
							<div class="form-group col-md-4">
								<label for="rif_ordine">Rif. Ordine</label>
								<p class="form-control-static">
									<div class="row">
										<div class="col-md-10">
											<div class="col-md-10">
												<input type="text" name="rif_ordine" id="rif_ordine" value="{$order->rif_ordine}" placeholder="Scrivi qui o seleziona un'opzione" />
											</div>
											<div class="col-md-2">
												<select class="form-control" onchange="document.getElementById('rif_ordine').value = this.value">
													<option id="rif_ordine_prima_opzione" value=""></option>
													<option value="NC">NC</option>
												</select>
											</div>
										</div>
										<div class="col-md-2">
											<input type="submit" id="submitRifOrdine" class="btn btn-default" value="Salva" />
											<div id="rif_ordine_feedback"></div>
										</div>
									</div>
								</p>
							</div>
						</form>

						{if $permessi_verifica or true} {* correggere: togliere or true dopo test ovunque *}
							<div class="form-group col-md-2">
								<label for="spring_{$order->id}">Verificato</label>
								<p class="form-control-static">
									<div class="row">
										<div class="col-md-1">
											<input type="checkbox" id="spring_{$order->id}" name="spring_{$order->id}" {if $order->spring == 1}checked="checked"{/if} onchange="updSpringOrder({$order->id}); return false;" />
										</div>
										{if $order->spring == 1 && $order->verificato_da > 0}
										<div class="col-md-11" id="verificato_da"> {* Correggere: far sparire da se spring passa da 1 a 0 e viceversa farlo riapparire *}
											da {$informazioni['verificato_da_nome']}
										</div>
										{/if}
									</div>
								</p>
							</div>

							<div class="form-group col-md-1">
								<label for="check_pi_{$order->id}">P.I.</label>
								<p class="form-control-static">
									<input type="checkbox" id="check_pi_{$order->id}" name="check_pi_{$order->id}" {if $order->check_pi == 1}checked="checked"{/if} onchange="updCheckPI({$order->id}); return false;" />
								</p>
							</div>
						{/if}

						{if $permessi_amz or true}
							<div class="form-group col-md-2">
								<label for="pag_amz_ric_{$order->id}">Pagamento amazon ricevuto</label>
								<p class="form-control-static">
									<input type="checkbox" id="pag_amz_ric_{$order->id}" name="pag_amz_ric_{$order->id}" {if $order->pag_amz_ric == 1}checked="checked"{/if} onchange="updPagAmzRic({$order->id}); return false;" />
								</p>
							</div>
						{/if}

						{if $permessi_esolver or true}
							<div class="form-group col-md-1">
								<label for="check_esolver_{$order->id}">eSolver</label>
								<p class="form-control-static">
									<input type="checkbox" id="check_esolver_{$order->id}" name="check_esolver_{$order->id}" {if $order->check_esolver == 1}checked="checked"{/if} onchange="updCheckEsolv({$order->id}); return false;" />
								</p>
							</div>
						{/if}

					</div>

					{if $informazioni['fatturato']}
					<div class="row thick rosso">
						<span>Questo ordine &egrave; stato fatturato (fattura n. <a href="" target="_blank">{$informazioni['id_fattura']}</a>)</span>
					</div>
					{/if}

				</div>
			</div>
		</div>
	</div>

	<div id="pa-view" {if !$has_pa}style="display:none"{/if}>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel">
					<div class="panel-heading">
						<i class="icon-legal"></i> 
						Dati P.A.
						{if $abilita_modifica}
						<div class="bottoni-tab">
							<a class="btn btn-default" onclick="modifica_pa()">
								<i class="icon-edit"></i> Modifica
							</a>
						</div>
						{/if}
					</div>

					<div class="form-row">
						<div class="row">
							
							<div class="form-group col-md-3">
								<label for="cig_string">CIG</label>
								<p id="cig_string" class="form-control-static">
									{$order->cig}
								</p>
							</div>

							<div class="form-group col-md-3">
								<label for="cup_string">CUP</label>
								<p id="cup_string" class="form-control-static">
									{$order->cup}
								</p>
							</div>

							<div class="form-group col-md-3">
								<label for="ipa_string">IPA</label> 
								<span class="label-tooltip text-primary" data-toggle="tooltip" title="Codice univoco ufficio per fatturazione elettronica">
									<i class="icon-question-sign"></i>
								</span>
								<p id="ipa_string" class="form-control-static">
									{$order->ipa}
								</p>
							</div>

							<div class="form-group col-md-3">
								<label for="data_ordine_mepa_string">Data ordine MEPA</label>
								<p id="data_ordine_mepa_string" class="form-control-static">
									{$order->data_ordine_mepa|date_format:'%d-%m-%Y'}
								</p>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="pa-edit" style="display: none;">
		<form name="form_modifica_pa" id="form_modifica_pa" method="post" action="ajax.php" onsubmit="saveDatiPA(); return false;">
			<div class="row">
				<div class="col-lg-12">
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-legal"></i> 
							Dati P.A. <span class="rosso">MODIFICA</span>
							<div class="bottoni-tab">
								<a class="btn btn-default" onclick="salva_pa()">
									<i class="icon-refresh"></i> Annulla
								</a>
								<input type="submit" id="submitDatiPA" class="btn btn-default" value="Salva" onclick="salva_pa()" />
							</div>
						</div>

						<div class="form-row">
							<div class="row">
								
								<div class="form-group col-md-3">
									<label for="cig">CIG</label>
									<p class="form-control-static">
										<input type="text" name="cig" id="cig" value="{$order->cig}" style="max-width: 70%;">
									</p>
								</div>

								<div class="form-group col-md-3">
									<label for="cup">CUP</label>
									<p class="form-control-static">
										<input type="text" name="cup" id="cup" value="{$order->cup}" style="max-width: 70%;">
									</p>
								</div>

								<div class="form-group col-md-3">
									<label for="ipa">IPA</label> 
									<span class="label-tooltip text-primary" data-toggle="tooltip" title="Codice univoco ufficio per fatturazione elettronica">
										<i class="icon-question-sign"></i>
									</span>
									<p class="form-control-static">
										<input type="text" name="ipa" id="ipa" value="{$order->ipa}" style="max-width: 70%;">
									</p>
								</div>

								<div class="form-group col-md-3">
									<label for="data_ordine_mepa">Data ordine MEPA</label>
									<p class="form-control-static">
										<input type="date" name="data_ordine_mepa" id="data_ordine_mepa" value="{$order->data_ordine_mepa|date_format:'%Y-%m-%d'}" style="max-width: 70%;">
									</p>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>

	<div class="row">
		{* Indirizzo Fatturazione *}
		<div class="col-md-6">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-user"></i> 
					Indirizzo fatturazione
				</div>

				<div class="form-row">

					{*if $has_pa*}
						<div class="row">
							{if $can_edit}
								<form class="form-horizontal hidden-print" method="post" action="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;vieworder&amp;id_order={$order->id|intval}">
									<div class="form-group">
										<div class="col-md-9">
											<select name="id_address">
												{foreach from=$customer_addresses item=address}
												<option value="{$address['id_address']}"
													{if $address['id_address'] == $order->id_address_invoice}
														selected="selected"
													{/if}>
													{$address['alias']} -
													{$address['address1']}
													{$address['postcode']}
													{$address['city']}
													{if !empty($address['state'])}
														{$address['state']}
													{/if},
													{$address['country']}
												</option>
												{/foreach}
											</select>
										</div>
										<div class="col-md-3">
											<button class="btn btn-default" type="submit" name="submitAddressInvoice"><i class="icon-refresh"></i> Modifica</button>
										</div>
									</div>
								</form>
							{/if}
						</div>
					{*else*}
						<div class="row">
							<div class="form-group">
								<div class="col-md-9">
								{* Correggere variabili e tasto modifica *}
									<input type="text" name="id_address" value="
										{$addresses.invoice->alias} -
										{$addresses.invoice->address1}
										{$addresses.invoice->postcode}
										{$addresses.invoice->city}
										{if !empty($addresses.invoice->state)}
											{$addresses.invoice->state}
										{/if},
										{$addresses.invoice->country}
									" disabled>
								</div>
								<div class="col-md-3">
									<button class="btn btn-default" type="submit" name="submitAddressInvoice"><i class="icon-refresh"></i> Modifica</button>
								</div>
							</div>
						</div>
					{*/if*}
				</div>

				<div class="form-row">
					<div class="row">
					</div>
				</div>

				<table>
					<tr class="tab_cus">
						<td class="tab_cus">Destinatario</td>
						<td class="tab_cus thick">{if $customer->is_company}{$addresses.invoice->company}{else}{$addresses.invoice->firstname} {$addresses.invoice->lastname}{/if}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus"></td>
						<td class="tab_cus"></td>
					</tr>
					{if $customer->is_company}
					<tr class="tab_cus">
						<td class="tab_cus">P.IVA</td>
						<td class="tab_cus">{$addresses.invoice->vat_number}</td>
					</tr>
					{/if}
					<tr class="tab_cus">
						<td class="tab_cus">CF</td>
						<td class="tab_cus {if !$indirizzo_fatturazione['cfcheck']}rosso{/if}" >{$addresses.invoice->cf} {if !$indirizzo_fatturazione['cfcheck']}<span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="Codice fiscale potenzialmente errato: controllare"><i class="icon-question-sign rosso"></i></span>{/if}</td>
					</tr>
					{if $customer->is_company}
					<tr class="tab_cus">
						<td class="tab_cus">Referente</td>
						<td class="tab_cus">{if Configuration::get('PS_B2B_ENABLE')}{$customer->company} - {/if}{$gender->name|escape:'html':'UTF-8'} {$customer->firstname} {$customer->lastname}</td>
					</tr>
					{/if}
					<tr class="tab_cus">
						<td class="tab_cus">Email</td>
						<td class="tab_cus">{$customer->email}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">PEC</td>
						<td class="tab_cus">{$customer->pec}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">SDI</td>
						<td class="tab_cus">{$customer->codice_univoco}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">Tel</td>
						<td class="tab_cus">{$addresses.invoice->phone}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">Cell</td>
						<td class="tab_cus">{$addresses.invoice->phone_mobile}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">Indirizzo</td>
						<td class="tab_cus">{$addresses.invoice->address1} {$addresses.invoice->address2}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">Località</td>
						<td class="tab_cus">{$addresses.invoice->suburb}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">CAP</td>
						<td class="tab_cus">{$addresses.invoice->postcode}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">Città</td>
						<td class="tab_cus">{$addresses.invoice->city}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">Provincia</td>
						<td class="tab_cus">{$addresses.invoiceState->name}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">Nazione</td>
						<td class="tab_cus">{$addresses.invoice->country}</td>
					</tr>
				</table>

			</div>
		</div>

		{* Indirizzo consegna *}
		<div class="col-md-6">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-truck"></i> 
					Indirizzo consegna
				</div>

				<div class="form-row">

					<div class="row">
						
						{if $can_edit}
							<form class="form-horizontal hidden-print" method="post" action="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;vieworder&amp;id_order={$order->id|intval}">
								<div class="form-group">
									<div class="col-md-9">
										<select name="id_address">
											{foreach from=$customer_addresses item=address}
											<option value="{$address['id_address']}"
												{if $address['id_address'] == $order->id_address_delivery}
												selected="selected"
												{/if}>
												{$address['alias']} -
												{$address['address1']}
												{$address['postcode']}
												{$address['city']}
												{if !empty($address['state'])}
													{$address['state']}
												{/if},
												{$address['country']}
											</option>
											{/foreach}
										</select>
									</div>
									<div class="col-md-3">
										<button class="btn btn-default" type="submit" name="submitAddressShipping"><i class="icon-refresh"></i> Modifica</button>
									</div>
								</div>
							</form>
						{/if}

					</div>
				</div>

				<div class="form-row">
					<div class="row">
					</div>
				</div>

				<table>
					<tr class="tab_cus">
						<td class="tab_cus">Destinatario</td>
						{*controllare che sia giusto l'if*}
						<td class="tab_cus thick">{if $addresses.delivery->company != ""}{$addresses.delivery->company}{else}{$addresses.delivery->firstname} {$addresses.delivery->lastname}{/if}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">C/O</td>
						<td class="tab_cus">{$addresses.delivery->c_o}</td>
					</tr>
					{*<tr class="tab_cus">
						<td class="tab_cus">P.IVA</td>
						<td class="tab_cus">{$addresses.delivery->vat_number}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">CF</td>
						<td class="tab_cus">{$addresses.delivery->cf}</td>
					</tr>*}
					<tr class="tab_cus">
						<td class="tab_cus">Referente</td>
						<td class="tab_cus">{if Configuration::get('PS_B2B_ENABLE')}{$customer->company} - {/if}{$gender->name|escape:'html':'UTF-8'} {$customer->firstname} {$customer->lastname}</td>
					</tr>
					{*<tr class="tab_cus">
						<td class="tab_cus">Email</td>
						<td class="tab_cus">{$customer->email}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">PEC</td>
						<td class="tab_cus">{$customer->pec}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">SDI</td>
						<td class="tab_cus">{$customer->codice_univoco}</td>
					</tr>*}
					<tr class="tab_cus">
						<td class="tab_cus">Tel</td>
						<td class="tab_cus">{$addresses.delivery->phone}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">Cell</td>
						<td class="tab_cus">{$addresses.delivery->phone_mobile}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">Indirizzo</td>
						<td class="tab_cus">{$addresses.delivery->address1} {$addresses.delivery->address2}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">Località</td>
						<td class="tab_cus">{$addresses.delivery->suburb}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">CAP</td>
						<td class="tab_cus">{$addresses.delivery->postcode}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">Città</td>
						<td class="tab_cus">{$addresses.delivery->city}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">Provincia</td>
						<td class="tab_cus">{$addresses.delivery->id_state}</td>
					</tr>
					<tr class="tab_cus">
						<td class="tab_cus">Nazione</td>
						<td class="tab_cus">{$addresses.delivery->country}</td>
					</tr>
				</table>

			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-info"></i> 
					Storico stato ordine ( pagamento / tecnico )
				</div>

				<div class="form-row">

					<!-- Change status form -->
					{if !$is_agente}
					<form action="{$currentIndex|escape:'html':'UTF-8'}&amp;vieworder&amp;token={$smarty.get.token}" method="post" class="form-horizontal well hidden-print">
						<div class="row">
							<div class="col-md-4">
								<select id="id_order_state" class="chosen form-control" name="id_order_state" onchange="onchangeStato(this);">
								{foreach from=$states item=state}
									{if $state['id_order_state'] == 35 && $mio_id != 2 && $mio_id != 6 && $mio_id != 22} {* Tutti tranne Barbara, Federico, Carolina *}
										{* Niente *}
									{else}
									<option value="{$state['id_order_state']|intval}"{if isset($currentState) && $state['id_order_state'] == $currentState->id} selected="selected" disabled="disabled"{/if}>{$state['name']|escape}</option>
									{/if}
								{/foreach}
								</select>
								<input type="hidden" name="id_order" value="{$order->id}" />
							</div>
							<div class="col-md-2" {if $currentState->id != 33}style="display:none"{/if}>
								<input type="text" name="acconto" id="acconto1" value="{$storico_stati['acconto']}" />
							</div>
							<div class="col-md-3" {if $currentState->id != 24}style="display:none"{/if}>
								<div class="col-md-4">
									In carico a:
								</div>
								<div class="col-md-8">
									<select name="impiegati_verifica" id="impiegati_verifica">
									{foreach $storico_stati['impiegati'] as $imp}
										<option name="{$imp['id_employee']}" value="{$imp['id_employee']}" {if $imp['id_employee'] == $storico_stati['incaricato_attuale']}selected="selected"{/if}>{$imp['name']}</option>
									{/foreach}
									</select>
								</div>
							</div>
							<div class="col-md-2" {if $currentState->id != 24}style="display:none"{/if}>
								<textarea name="verifica_msg" id="verifica_msg" readonly="readonly">{$storico_stati['msg_verifica']}</textarea>
							</div>
							{if $storico_stati['spedito']}
							<div class="col-md-2" id="imposta_tracking" style="" method="post">
								<div class="col-md-4">
									Numero tracking:
								</div>
								<div class="col-md-8">
									<input type="text" name="shipping_number" id="shipping_number" value="{$order->shipping_number}" {if $addresses.delivery->country == 'Italia'}maxlength="11"{/if} onchange="submitShippingNumber_ajax();" />
									<div id="shipping_number_div"></div>
								</div>
							</div>
							{/if}
							<div class="col-md-1">
								<button type="submit" name="submitState" class="btn btn-primary" {* onclick="saveRifOrdine();" *}>
									{l s='Update status'}
								</button>
							</div>
						</div>
						{if $storico_stati['ast_status'] != ''}
						<br />
						<div class="row">
							<p>{$storico_stati['ast_status']}</p>
						</div>
						{/if}
					</form>
					{/if}

					<div class="row">
						<div class="form-group col-md-12">
							<table class="table history-status row-margin-bottom">
								<tbody>
									<tr>
										<th>Data</th>
										{*<th></th>*}
										<th>Stato</th>
										<th>Stato tecnico</th>
										<th>Impiegato</th>
										<th>Azioni</th>
									</tr>
									{foreach from=$history item=row key=key}
										{if ($key == 0)}
											<tr>
												<td style="background-color:{$row['color']};color:{$row['text-color']}">{dateFormat date=$row['date_add'] full=true}</td>
												{*<td style="background-color:{$row['color']}"><img src="../img/os/{$row['id_order_state']|intval}.gif" width="16" height="16" alt="{$row['ostate_name']|stripslashes}" /></td>*}
												<td style="background-color:{$row['color']};color:{$row['text-color']}">{if $row['ostate_name'] != '--'}<img src="../img/os/{$row['id_order_state']|intval}.gif" width="16" height="16" alt="{$row['ostate_name']|stripslashes}" />{/if} {$row['ostate_name']|stripslashes}</td>
												<td style="background-color:{$row['color']};color:{$row['text-color']}">{if $row['stato_tecnico'] != '--'}<img src="../img/os/{$row['id_order_state']|intval}.gif" width="16" height="16" alt="{$row['ostate_name']|stripslashes}" />{/if} {$row['stato_tecnico']|stripslashes}</td>
												<td style="background-color:{$row['color']};color:{$row['text-color']}">{if $row['employee_lastname']}{$row['employee_firstname']|stripslashes} {$row['employee_lastname_substr']|stripslashes}{/if}</td>
												<td style="background-color:{$row['color']};color:{$row['text-color']}" class="text-right">
													{if $row['send_email']|intval}
														<a class="btn btn-default" href="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;vieworder&amp;id_order={$order->id|intval}&amp;sendStateEmail={$row['id_order_state']|intval}&amp;id_order_history={$row['id_order_history']|intval}" title="{l s='Resend this email to the customer'}">
															<i class="icon-mail-reply"></i>
															{l s='Resend email'}
														</a>
													{/if}
												</td>
											</tr>
										{else}
											<tr>
												<td>{dateFormat date=$row['date_add'] full=true}</td>
												{*<td><img src="../img/os/{$row['id_order_state']|intval}.gif" width="16" height="16" /></td>*}
												<td>{if $row['ostate_name'] != '--'}<img src="../img/os/{$row['id_order_state']|intval}.gif" width="16" height="16" alt="{$row['ostate_name']|stripslashes}" />{/if} {$row['ostate_name']|stripslashes}</td>
												<td>{if $row['stato_tecnico'] != '--'}<img src="../img/os/{$row['id_order_state']|intval}.gif" width="16" height="16" alt="{$row['ostate_name']|stripslashes}" />{/if} {$row['stato_tecnico']|stripslashes}</td>
												<td>{if $row['employee_lastname']}{$row['employee_firstname']|stripslashes} {$row['employee_lastname_substr']|stripslashes}{else}&nbsp;{/if}</td>
												<td class="text-right">
													{if $row['send_email']|intval}
														<a class="btn btn-default" href="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;vieworder&amp;id_order={$order->id|intval}&amp;sendStateEmail={$row['id_order_state']|intval}&amp;id_order_history={$row['id_order_history']|intval}" title="{l s='Resend this email to the customer'}">
															<i class="icon-mail-reply"></i>
															{l s='Resend email'}
														</a>
													{/if}
												</td>
											</tr>
										{/if}
									{/foreach}
								</tbody>
							</table>
						</div>
					</div>

				</div>

				{if !$is_agente}
				<hr />
				
				<form name="form_nofeedback" id="form_nofeedback" action="ajax.php" method="post" onsubmit="saveNoFeedback(); return false;">
					<div class="row">
						<div class="col-md-2">
							<input type="checkbox" name="no_feedback" id="no_feedback" value="{$order->no_feedback}" {if $order->no_feedback == 1}checked="checked"{/if}> <label>Abortisci feedback</label> 
							<span class="label-tooltip text-primary" data-toggle="tooltip" data-html="true" title="Spuntando questo flag impedirai l'invio della mail di richiesta feedback/recensione al cliente per qualsiasi piattaforma (eKomi, Amazon, nostro sito)">
								<i class="icon-question-sign"></i>
							</span>
						</div>
						<div class="col-md-1">
							<span class="pull-right thick">Causa</span> 
						</div>
						<div class="col-md-6">
							<input type="text" name="no_feedback_cause" id="no_feedback_cause" value="{$order->no_feedback_cause}">
						</div>
						<div class="col-md-2">
							<input type="submit" name="submitNoFeedback" id="submitNoFeedback" class="btn btn-default" value="Salva dati feedback">
							<div id="nofeedback_feedback"></div>
						</div>
					</div>
				</form>
				{/if}

			</div>
		</div>
	</div>

	<div class="row">
		{*
		<div class="col-lg-8">
			<div id="formAddPaymentPanel" class="panel">
				<div class="panel-heading">
					<i class="icon-money"></i>
					Pagamento <span class="badge">{$order->getOrderPayments()|@count}</span>
				</div>
				{if count($order->getOrderPayments()) > 0}
					<p class="alert alert-danger"{if round($orders_total_paid_tax_incl, 2) == round($total_paid, 2) || (isset($currentState) && $currentState->id == 6)} style="display: none;"{/if}>
						{l s='Warning'}
						<strong>{displayPrice price=$total_paid currency=$currency->id}</strong>
						{l s='paid instead of'}
						<strong class="total_paid">{displayPrice price=$orders_total_paid_tax_incl currency=$currency->id}</strong>
						{foreach $order->getBrother() as $brother_order}
							{if $brother_order@first}
								{if count($order->getBrother()) == 1}
									<br />{l s='This warning also concerns order '}
								{else}
									<br />{l s='This warning also concerns the next orders:'}
								{/if}
							{/if}
							<a href="{$current_index}&amp;vieworder&amp;id_order={$brother_order->id}&amp;token={$smarty.get.token|escape:'html':'UTF-8'}">
								#{'%06d'|sprintf:$brother_order->id}
							</a>
						{/foreach}
					</p>
				{/if}
				<form id="formAddPayment"  method="post" action="{$current_index}&amp;vieworder&amp;id_order={$order->id}&amp;token={$smarty.get.token|escape:'html':'UTF-8'}">
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='Date'}</span></th>
									<th><span class="title_box ">{l s='Payment method'}</span></th>
									<th><span class="title_box ">{l s='Transaction ID'}</span></th>
									<th><span class="title_box ">{l s='Amount'}</span></th>
									<th><span class="title_box ">{l s='Invoice'}</span></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								{foreach from=$order->getOrderPaymentCollection() item=payment}
								<tr>
									<td>{dateFormat date=$payment->date_add full=true}</td>
									<td>{$payment->payment_method|escape:'html':'UTF-8'}</td>
									<td>{$payment->transaction_id|escape:'html':'UTF-8'}</td>
									<td>{displayPrice price=$payment->amount currency=$payment->id_currency}</td>
									<td>
									{if $invoice = $payment->getOrderInvoice($order->id)}
										{$invoice->getInvoiceNumberFormatted($current_id_lang, $order->id_shop)}
									{else}
									{/if}
									</td>
									<td class="actions">
										<button class="btn btn-default open_payment_information">
											<i class="icon-search"></i>
											{l s='Details'}
										</button>
									</td>
								</tr>
								<tr class="payment_information" style="display: none;">
									<td colspan="5">
										<p>
											<b>{l s='Card Number'}</b>&nbsp;
											{if $payment->card_number}
												{$payment->card_number}
											{else}
												<i>{l s='Not defined'}</i>
											{/if}
										</p>
										<p>
											<b>{l s='Card Brand'}</b>&nbsp;
											{if $payment->card_brand}
												{$payment->card_brand}
											{else}
												<i>{l s='Not defined'}</i>
											{/if}
										</p>
										<p>
											<b>{l s='Card Expiration'}</b>&nbsp;
											{if $payment->card_expiration}
												{$payment->card_expiration}
											{else}
												<i>{l s='Not defined'}</i>
											{/if}
										</p>
										<p>
											<b>{l s='Card Holder'}</b>&nbsp;
											{if $payment->card_holder}
												{$payment->card_holder}
											{else}
												<i>{l s='Not defined'}</i>
											{/if}
										</p>
									</td>
								</tr>
								{foreachelse}
								<tr>
									<td class="list-empty hidden-print" colspan="6">
										<div class="list-empty-msg">
											<i class="icon-warning-sign list-empty-icon"></i>
											{l s='No payment methods are available'}
										</div>
									</td>
								</tr>
								{/foreach}
								<tr class="current-edit hidden-print">
									<td>
										<div class="input-group fixed-width-xl">
											<input type="text" name="payment_date" class="datepicker" value="{date('Y-m-d')}" />
											<div class="input-group-addon">
												<i class="icon-calendar-o"></i>
											</div>
										</div>
									</td>
									<td>
										<input name="payment_method" list="payment_method" class="payment_method">
										<datalist id="payment_method">
										{foreach from=$payment_methods item=payment_method}
											<option value="{$payment_method}">
										{/foreach}
										</datalist>
									</td>
									<td>
										<input type="text" name="payment_transaction_id" value="" class="form-control fixed-width-sm"/>
									</td>
									<td>
										<input type="text" name="payment_amount" value="" class="form-control fixed-width-sm pull-left" />
										<select name="payment_currency" class="payment_currency form-control fixed-width-xs pull-left">
											{foreach from=$currencies item=current_currency}
												<option value="{$current_currency['id_currency']}"{if $current_currency['id_currency'] == $currency->id} selected="selected"{/if}>{$current_currency['sign']}</option>
											{/foreach}
										</select>
									</td>
									<td>
										{if count($invoices_collection) > 0}
											<select name="payment_invoice" id="payment_invoice">
											{foreach from=$invoices_collection item=invoice}
												<option value="{$invoice->id}" selected="selected">{$invoice->getInvoiceNumberFormatted($current_id_lang, $order->id_shop)}</option>
											{/foreach}
											</select>
										{/if}
									</td>
									<td class="actions">
										<button class="btn btn-primary" type="submit" name="submitAddPayment">
											{l s='Add'}
										</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</form>
				{if (!$order->valid && sizeof($currencies) > 1)}
					<form class="form-horizontal well" method="post" action="{$currentIndex|escape:'html':'UTF-8'}&amp;vieworder&amp;id_order={$order->id}&amp;token={$smarty.get.token|escape:'html':'UTF-8'}">
						<div class="row">
							<label class="control-label col-lg-3">{l s='Change currency'}</label>
							<div class="col-lg-6">
								<select name="new_currency">
								{foreach from=$currencies item=currency_change}
									{if $currency_change['id_currency'] != $order->id_currency}
									<option value="{$currency_change['id_currency']}">{$currency_change['name']} - {$currency_change['sign']}</option>
									{/if}
								{/foreach}
								</select>
								<p class="help-block">{l s='Do not forget to update your exchange rate before making this change.'}</p>
							</div>
							<div class="col-lg-3">
								<button type="submit" class="btn btn-default" name="submitChangeCurrency"><i class="icon-refresh"></i> {l s='Change'}</button>
							</div>
						</div>
					</form>
				{/if}
			</div>
		</div>
		*}
		{*
		<div class="col-lg-8">
			<div id="formAddPaymentPanel" class="panel">
				<div class="panel-heading">
					<i class="icon-money"></i>
					Pagamento <span class="badge">{$order->getOrderPayments()|@count}</span>
				</div>

			</div>
		</div>
		*}

		<div class="col-lg-12">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-truck"></i> 
					Pagamento e Spedizione
				</div>

				<div class="form-row">

					<div class="row">
						<div class="form-group col-md-12">
							<label for="tipo_cliente">Metodo di pagamento</label>
							<p class="form-control-static">
								{$order->payment}
							</p>
						</div>
					</div>

					<div class="row">
						
						<div class="form-group col-md-3">
							<label for="tipo_cliente">Peso</label>
							<p class="form-control-static">
								{$order->weight}
							</p>
						</div>

						<div class="form-group col-md-3">
							<label for="ragione_sociale">Metodo di spedizione</label>
							<p class="form-control-static">
								{$pag_spedizione["metodo_spedizione"]}
							</p>
						</div>
					{*
					</div>
					<div class="row">
					*}
						<div class="form-group col-md-3">
							<label for="creazione_account">Consegna</label>
							<p class="form-control-static">
								{$pag_spedizione["consegna"]}
							</p>
						</div>

						<div class="form-group col-md-3">
							<label for="ordini_totali">Numero di tracking</label>
							<p class="form-control-static">
								{$order->shipping_number}
							</p>
							<p class="form-control-static">
								{if isset($carrier->url) AND $carrier->url != ""}<a href="{$carrier->url}">Traccia la spedizione</a>{/if}
							</p>
						</div>

					</div>

					{if $pag_spedizione['avviso_ddt']}
						{$pag_spedizione['avviso_ddt']}
					{/if}

					{if $pag_spedizione['ezcloud_contratti']}
						<input type="checkbox" name="check_contratti" id="check_contratti" onchange="salvaRicezioneContratto();" {if $pag_spedizione['check_contratti']}checked="checked"{/if} />
						&nbsp;&nbsp;&nbsp;<span id="check_contratti_span">{$pag_spedizione['check_contratti_string']}</span>
					{/if}

					<hr />
					{if $order->recyclable}
						<span class="label label-success"><i class="icon-check"></i> {l s='Recycled packaging'}</span>
					{else}
						<span class="label label-inactive"><i class="icon-remove"></i> {l s='Recycled packaging'}</span>
					{/if}

					{if $order->gift}
						<span class="label label-success"><i class="icon-check"></i> {l s='Gift wrapping'}</span>
					{else}
						<span class="label label-inactive"><i class="icon-remove"></i> {l s='Gift wrapping'}</span>
					{/if}
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	var stopKeyUp = false;
	</script>
	<div id="prodotti-view">
		<input type="hidden" name="gruppo_cliente" id="gruppo_cliente" value="{$customer->id_default_group}" />
		<input type="hidden" name="id_customer" id="id_customer" value="{$customer->id}" />
		<input type="hidden" name="id_order" id="id_order" value="{$order->id}" />
		<div class="row" id="prodotti" style="display:none">
			<div class="col-lg-12">
				<div class="panel">
					<div class="panel-heading">
						<i class="icon-shopping-cart"></i>
						Prodotti <span class="badge">{$products|@count}</span>
						<div class="bottoni-tab">
							<a class="btn btn-default" onclick="modifica_prodotti()">
								Modifica
							</a>
						</div>
					</div>



					{*
					<div id="refundForm">
						<a href="#" class="standard_refund"><img src="../img/admin/add.gif" alt="{l s='Process a standard refund'}" /> {l s='Process a standard refund'}</a>
						<a href="#" class="partial_refund"><img src="../img/admin/add.gif" alt="{l s='Process a partial refund'}" /> {l s='Process a partial refund'}</a>
					</div>
					*}

				
					<div class="row" id="tabella_costi">
						<div class="form-group col-md-12">
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<th><span class="title_box ">{*immagine*}</span></th>
											<th><span class="title_box ">Codice</span></th>					{* Col + grafico e GXP-1610 *}
											<th><span class="title_box ">Nome prodotto</span></th>			{* nome *}
											<th><span class="title_box ">Qt.</span></th>
											<th><span class="title_box ">DDT</span></th>					{* Checkbox *}
											<th><span class="title_box ">Unitario</span></th>
											<th><span class="title_box ">Sconto extra</span></th>
											<th><span class="title_box ">Acquisto</span></th>
											<th><span class="title_box ">Marginalità</span></th>
											<th><span class="title_box ">Qt. disp. all'ordine</span></th>
											<th><span class="title_box ">Rimanenze</span></th>
											<th><span class="title_box ">Kit?</span></th>
											<th><span class="title_box ">Totale *</span></th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										{*
										<tr>
											<td>Diritto di chiamata</td>
											<td id="diritto_chiamata_code"></td>
											<td>
												<select onchange="aggiorna_costo_unitario('diritto_chiamata')" name="diritto_chiamata" id="diritto_chiamata">
													<option value="">-- Seleziona --</option>
													<option value="EZDIRFIX">Fisso intervento < 20 km</option>
													<option value="EZDIRFIXF">Fisso intervento < 20 km festivo</option>
												</select>
											</td>
											
											<td><input onblur="aggiorna_costo_totale_riga('diritto_chiamata')" type="text" id="diritto_chiamata_qta" name="diritto_chiamata_qta"></td>
											<td><input onblur="aggiorna_costo_totale_riga('diritto_chiamata')" type="text" id="diritto_chiamata_unitario" name="diritto_chiamata_unitario"></td>
											<td><input onblur="aggiorna_costo_totale_riga('diritto_chiamata')" type="text" id="diritto_chiamata_sconto" name="diritto_chiamata_sconto"></td>
											<td><input type="text" id="diritto_chiamata_importo" name="diritto_chiamata_importo" readonly></td>
										</tr>
										*}

										

										{foreach $products AS $key => $product}
										{* Assign product price *}
										{if ($order->getTaxCalculationMethod() == $smarty.const.PS_TAX_EXC)}
											{assign var=prezzo_prodotto value=($product['unit_price_tax_excl'] + $product['ecotax'])}
										{else}
											{assign var=prezzo_prodotto value=$product['unit_price_tax_incl']}
										{/if}
										<tr>
											<td>{if isset($product.image) && $product.image->id}{$product.image_tag}{/if}</td>
											<td><img src="../img/admin/add.gif" alt="+" data-toggle="collapse" data-target="#dett_pr_{$product['product_id']|intval}" aria-expanded="false" aria-controls="collapse_ra"/> <a class="tooltip_prodotto" title="{$product['tooltip']}" href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}&amp;id_product={$product['product_id']|intval}&amp;updateproduct" target="_blank">{$product.product_reference}</a></td>
											<td>{$product['product_name']}</td>
											<td>{$product['quantity']} or {$product['product_quantity']}</td>
											
											<td style="text-align: center;"><input type="checkbox"{if $product["ddt"] == 1}checked{/if}></td>
											<td>{displayPrice price=$product.unit_price_tax_excl currency=$currency->id}</td>
											<td>{$product['sconto_extra']}</td>
											<td>{displayPrice price=$product['wholesale_price'] currency=$currency->id}</td>
											<td>{$product['margin']}</td>
											<td>{$product['current_stock']}</td>
											<td>{$product['rimanenze']}</td>
											<td>{* Kit? Si o No *}{if $product["kit"]}SI{else}NO{/if}</td>
											<td>{displayPrice price=($prezzo_prodotto * ($product['product_quantity'] - $product['customizationQuantityTotal'])) currency=$currency->id}</td>
											<td><a class="btn btn-default"><i class="icon-edit"></i></a><a class="btn btn-danger"><i class="icon-trash"></i></a></td>
										</tr>
										
										{* Collapsable *}
										<tr>
											<td colspan="13">
												<div class="collapse" id="dett_pr_{$product['product_id']|intval}">
													{for $i = 1 to $product['product_quantity']}
													<div class="row">
														<div class="col-md-3">
															Seriale: <input onkeyup="saveDatiTecnici({$product['id_order_detail']}, {$product['product_id']}, {$idt}); return false;" type="text" name="dt_seriale_{$product['id_order_detail']}[{$idt}]" value="{htmlspecialchars($dati_tecnici_attuali['seriale'])}" />
														</div>
														<div class="col-md-3">
															MAC: <input onkeyup="saveDatiTecnici({$product['id_order_detail']}, {$product['product_id']}, {$idt}); return false;" type="text" name="dt_mac_{$product['id_order_detail']}[{$idt}]" value="{htmlspecialchars($dati_tecnici_attuali['mac'])}" class="dt_mac_{$product['id_order_detail']}" />
														</div>
														<div class="col-md-3">
															IMEI: <input onkeyup="saveDatiTecnici({$product['id_order_detail']}, {$product['product_id']}, {$idt}); return false;" type="text" name="dt_imei_{$product['id_order_detail']}[{$idt}]" value="{htmlspecialchars($dati_tecnici_attuali['imei'])}" />
														</div>
														<div class="col-md-3">
															Note: <input onkeyup="saveDatiTecnici({$product['id_order_detail']}, {$product['product_id']}, {$idt}); return false;" type="text" name="dt_note_{$product['id_order_detail']}[{$idt}]" value="{htmlspecialchars($dati_tecnici_attuali['note'])}" size="50" />
														</div>
														<div class="col-md-3">
															<span id="dati_tecnici_feedback_{$product['id_order_detail']}"></span>
														</div>
													</div>
													{/for}
												</div>
											</td>
										</tr>
										
										{/foreach}
									</tbody>
									{if ($order->getTaxCalculationMethod() == $smarty.const.PS_TAX_EXC)}
										{assign var=order_product_price value=($order->total_products)}
										{assign var=order_discount_price value=$order->total_discounts_tax_excl}
										{assign var=order_wrapping_price value=$order->total_wrapping_tax_excl}
										{assign var=order_shipping_price value=$order->total_shipping_tax_excl}
									{else}
										{assign var=order_product_price value=$order->total_products_wt}
										{assign var=order_discount_price value=$order->total_discounts_tax_incl}
										{assign var=order_wrapping_price value=$order->total_wrapping_tax_incl}
										{assign var=order_shipping_price value=$order->total_shipping_tax_incl}
									{/if}
									{assign var=order_total_price value=$order->total_paid_tax_incl}
									<tfoot>
										<tr>
											<td></td>
											<td></td>
											<td>Prodotti</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td>{displayPrice price=$order_product_price currency=$currency->id}</td>
										</tr>
										<tr>
											<td></td>
											<td>TRASP</td>
											<td>Spedizione</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td>{displayPrice price=$order_shipping_price currency=$currency->id}</td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td>Commissioni</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td>0 € ?</td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td>Subtotale</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td>0 € ?</td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td>IVA</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td>{displayPrice price=($order->total_paid_tax_excl) currency=$currency->id}</td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td>Totale ordine</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td>{displayPrice price=$order_total_price currency=$currency->id}</td>
										</tr>
									</tfoot>
								</table>
							</div>
							<input type="hidden" id="num_varie" value="{$bdl['buoni']['num_varie']}">
							<br />
							{if $ordine_parziale == 1}<span class="thick">Prodotto in ordine parziale ancora da consegnare (la cella nella colonna a fianco della quantità indica il numero di articoli già consegnati)</span>{/if}
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	


	{* PRODOTTI MODIFICA - CORREGGERE: AGGIUNGERE REGOLA CON $abilita_modifica - SE FALSE DEVO FAR VEDERE IL PANEL VIEW O TUTTO READONLY *}

	<div id="prodotti-edit" {*style="display: none;"*}>
		<form method="post" action="">
		<div class="row" >
			<div class="col-lg-12">
				<div class="panel">
					<div class="panel-heading">
						<i class="icon-shopping-cart"></i>
						Prodotti <span class="rosso">modifica</span> <span class="badge">{$products|@count}</span>

						<div class="bottoni-tab">
							<a class="btn btn-default">
								<i class="icon-save"></i> Salva
							</a>
						</div>
					</div>
					{*
					<div id="refundForm">
						<a href="#" class="standard_refund"><img src="../img/admin/add.gif" alt="{l s='Process a standard refund'}" /> {l s='Process a standard refund'}</a>
						<a href="#" class="partial_refund"><img src="../img/admin/add.gif" alt="{l s='Process a partial refund'}" /> {l s='Process a partial refund'}</a>
					</div>
					*}

					<div class="row" id="seleziona_prodotto">
						<div class="form-row">
							<div class="form-group col-md-10">
								<label for="">Seleziona il prodotto da aggiungere</label>
							</div>
						</div>
					</div>

					<div class="row" id="aggiungi_prodotti">
						<script type="text/javascript" src="/js/jquery/jquery.autocomplete2.js"></script>
						
						<script type="text/javascript" src="/js/admin.js"></script>
						<input id="reset" type="button" value="Reset" class="button" onclick="document.getElementById('prodotti_online').checked = true; document.getElementById('prodotti_offline').checked = true; document.getElementById('prodotti_old').checked = true;  document.getElementById('prodotti_disponibili').checked = false; document.getElementById('auto_categoria').options[0].selected = true; $('#auto_categoria').select2();  document.getElementById('auto_marca').options[0].selected = true; $('#auto_marca').select2();  
			document.getElementById('auto_serie').options[0].selected = true; $('#auto_serie').select2();  
			document.getElementById('auto_fornitore').options[0].selected = true; $('#auto_fornitore').select2(); $('#prodlist').hide(); $('div.autocomplete_list').hide();" />
			
			<br />
			<input id="veditutti" type="button" value="Cerca" class="button" /> <input size="123" type="text" value="" id="product_autocomplete_input" /><br /><br />
			
			<script type="text/javascript">
					var formProduct = "";
					var products = new Array();
				</script>
				<script type="text/javascript">
			
			/*
				//setup before functions
				var typingTimer;                //timer identifier
				var doneTypingInterval = 3000;  //time in ms, 3 second for example
				var $ainput = $("#product_autocomplete_input");

				//on keyup, start the countdown
				$ainput.on("keyup", function () {
				  clearTimeout(typingTimer);
				  typingTimer = setTimeout(doneTyping, doneTypingInterval);
				});

				//on keydown, clear the countdown 
				$ainput.on("keydown", function () {
				  clearTimeout(typingTimer);
				});

				//user is "finished typing," do something
				function doneTyping () {
					lastKeyPressCode = event.keyCode;
					if(lastKeyPressCode != 37 && lastKeyPressCode != 38 && lastKeyPressCode != 39 && lastKeyPressCode != 40)
						$ainput.trigger("click");
				}
			*/
			
					var formProduct = "";
					var products = new Array();
				</script>
				
				<script type="text/javascript">
				{literal}
					$("body").on("click", function (event) {
						 if(event.target.tagName.toUpperCase() == "BODY"  || event.target.id == "auto_marca"   || event.target.id == "auto_serie" || event.target.id == "auto_categoria" || event.target.id == "auto_fornitore" ||  event.target.id == "prodotti_online"  || event.target.id == "prodotti_offline"   || event.target.id == "prodotti_old" || event.target.id == "prodotti_disponibili" || event.target.id == "product_autocomplete_input")
						 {
							event.stopPropagation();
							
						 }
						 else
						 {
							$('#prodlist').hide();
							$('div.autocomplete_list').hide();
						 }
					});	
					
						urlToCall = null;
					
					function getExcludeIds() {
						var ids = "";
						return ids;
					}
					
					function getOnline()
					{
						if(document.getElementById("prodotti_online").checked == true)
							return 1;
						else
							return 0;
					}
					
					function getOffline()
					{
						if(document.getElementById("prodotti_offline").checked == true)
							return 1;
						else
							return 0;
					}

					function getOld()
					{
						if(document.getElementById("prodotti_old").checked == true)
							return 2;
						else
							return 0;
					}
					
					function getDisponibili()
					{
						if(document.getElementById("prodotti_disponibili").checked == true)
							return 1;
						else
							return 0;
					}
					
					function getCopiaDa()
					{
							return 0;
					}
										
					function getSerie()
					{
						if(document.getElementById("auto_serie").value > 0)
							return document.getElementById("auto_serie").value;
						else
							return 0;
					}
					
					function getMarca()
					{
						if(document.getElementById("auto_marca").value > 0)
							return document.getElementById("auto_marca").value;
						else
							return 0;
					}
					
					function getFornitore()
					{
						if(document.getElementById("auto_fornitore").value > 0)
							return document.getElementById("auto_fornitore").value;
						else
							return 0;
					}
					
					function getCategoria()
					{
						if(document.getElementById("auto_categoria").value > 0)
							return document.getElementById("auto_categoria").value;
						else
							return 0;
					}
					
					function repopulateSeries(id_manufacturer)
					{
						$("#auto_serie").empty();
						$.ajax({
						  url:"ajax.php?repopulate_series=y",
						  type: "POST",
						  data: { id_manufacturer: id_manufacturer
						  },
						  success:function(resp){  
							var newOptions = $.parseJSON(resp);
							
							 $("#auto_serie").append($("<option></option>")
								 .attr("value", "0").text("Serie..."));
							
							$.each(newOptions, function(key,value) {
							  $("#auto_serie").append($("<option></option>")
								 .attr("value", value).text(key));
							});
							
							$("#auto_serie").select2({
								placeholder: "Serie..."
							});
						  },
						  error: function(xhr,stato,errori){
							 alert("Errore: impossibile trovare serie ");
						  }
						});
							
						
					}	
					
					function clearAutoComplete()
					{
						$("#veditutti").trigger("click");
						
					}	
					
					/* function autocomplete */
					$(function() {
						$('#product_autocomplete_input')
							.autocomplete('ajax_products_list2.php?excludeIds='+document.getElementById("excludeIds").value+'&id_cart=0', {
							
								minChars: 0,
								autoFill: false,
								max:5000,
								matchContains: true,
								matchSubset: 0,
								mustMatch:true,
								scroll:true,
								useCache:false,
								cacheLength:1,
								formatItem: function(item) {
							
											var color = "";
											if(item[24] == 1) {
												var color = "; color:red";
											}
											if(item[28] == 0) {
												return '</table><br /><div style="position:absolute; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:180px;float:left;margin-left:10px;">Codice</div></th><th style="width:630px;"><div style="width:350px; float:left;">Desc. prodotto<br /><br /></div></th><th style="width:60px; text-align:right"><div style="width:55px;float:left;">Prezzo</div></th><th style="width:45px; text-align:right"><div style="width:40px;float:left; text-align:right">Mag.</div></th><th style="width:45px; text-align:right"><div style="width:40px;float:left; text-align:right">INT</div></th><th style="width:45px; text-align:right"><div style="width:40px;float:left; text-align:right">ALN</div></th><th style="width:40px; text-align:right"><div style="width:40px;float:left; text-align:right">ITA</div></th><th style="width:40px; text-align:right"><div style="width:40px;float:left; text-align:right">Imp</div></th><th style="width:40px; text-align:right"><div style="width:40px;float:left; text-align:center">Tot</div></th></tr></div><table>'
												
												;
											}
											else {
												return '<tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="'+item[35]+'" />&nbsp;&nbsp;'+item[4]+'</td><td style="width:450px'+color+'">'+item[5]+'</td><td style="width:60px; text-align:right">'+item[23]+'</td><td style="width:40px; text-align:right">'+item[25]+'</td><td style="width:40px; text-align:right">'+item[33]+'</td><td style="width:40px; text-align:right">'+item[34]+'</td><td style="width:40px; text-align:right">'+item[38]+'</td><td style="width:40px; text-align:right">'+item[37]+'</td><td style="width:40px; text-align:right">'+item[22]+'</td></tr>'
												
												;
												}
											}
								
							}).result(addProduct_TR);
							
							$("#product_autocomplete_input").setOptions({
								extraParams: {excludeIds : getExcludeIds()}
							});		
						
					});
				
					$("#product_autocomplete_input").css('width','835px');
					
					
					$("#product_autocomplete_input").keypress(function(event){
								
								  var keycode = (event.keyCode ? event.keyCode : event.which);
								  if (keycode == "13") {
									  $("#veditutti").trigger("click"); 
									event.preventDefault();
									event.stopPropagation();    
								  }
								});
								
				
					
				</script>
				<script type="text/javascript">
				
				function addProduct_TR()
				{
				}
				
				$("#veditutti").click ( function (zEvent) {
					
					/*	$("body").trigger("click");
						$("body").trigger("click");
						
						
						
						$("#product_autocomplete_input").focus();
					//	$("#product_autocomplete_input").val("");
					//	$("#product_autocomplete_input").val(" ");
					*/
						var press = jQuery.Event("keypress");
						press.ctrlKey = true;
						if(document.getElementById('product_autocomplete_input').value == "")
						{
							$("#product_autocomplete_input").val(" ");
						}
						$("#product_autocomplete_input").trigger(press);
					
						$("#product_autocomplete_input").trigger("click");
						$("#product_autocomplete_input").trigger("click");
					//	$("#product_autocomplete_input").val("");
						
				} );
	
				{/literal}
				</script>
				
				<script type="text/javascript">
				{literal}
				var prodotti_nel_carrello = [0];
 
				function addProduct_TR(event, data, formatted)
				{
					if(data[6] == "*BUNDLE*") {
						var prodotti_bundle = data[7].split(";");
						for(i=0;i<((prodotti_bundle.length))-1;i++)
						{
							$.ajax({
							type: "GET",
							data: "id_cart=0&id_bundle="+data[1]+"&product_in_bundle="+prodotti_bundle[i],
							async: false,
							url: "ajax_products_list2.php",
							success: function(resp)
								{
									addProduct_TR("",resp.split("|"),"");
								},
								error: function(XMLHttpRequest, textStatus, errorThrown)
								{
									tooltip_content = "";
									alert("ERROR" + textStatus + " " + XMLHttpRequest.responseText);					
								}
														
							});
						}
						return false;
					
					}
					
					document.getElementById("product_autocomplete_input").value = ""; 
					var productId = data[1];
					
					$("#product_delete["+productId+"]").remove();
					
					var excludeIds = document.getElementById("excludeIds").value;
					var arrayescl = excludeIds.split(",");
					
					for(i=0;i<((arrayescl.length));i++)
					{
						if(arrayescl[i].trim() != "" && arrayescl[i].trim() == productId.trim())
						{
							alert("Prodotto già nel carrello");
							return false;
						}
					}
					excludeIds = excludeIds+","+productId+",";
					document.getElementById("excludeIds").value = excludeIds;
					$("#product_autocomplete_input").val("");
							
					$("#product_autocomplete_input").setOptions({
						extraParams: {excludeIds : document.getElementById("excludeIds").value}
					});	
				
					$("#product_autocomplete_input").focus();
					
					
					var productPrice = data[2];
					var productScQta = data[3];
					var productRef = data[4];
					var productName = data[5];
					var productQuantity = data[16];
					var tdImporto = data[17];
					var przUnitario = data[18];
					var deleteProd = data[19];
					var marg = data[20];
					var scontoExtra = data[21];
					var sc_qta_1 = data[6];
					var sc_qta_2 = data[7];
					var sc_qta_3 = data[8];
					var sc_riv_1 = data[9];
					var sc_riv_2 = data[10];
					var sc_riv_3 = data[26];
					var sc_qta_1_q = data[11];	
					var sc_qta_2_q = data[12];	
					var sc_qta_3_q = data[13];	
					var sc_riv_1_q = data[14];	
					var sc_riv_2_q = data[15];	
					var sc_riv_3_q = data[27];
					var acquisto = data[30];	
					var src_img = data[35];
					prodotti_nel_carrello.push(parseInt(productId));
					var varspec = data[24];
					var textVarSpec = "";
							
					
					$("#product_autocomplete_input").setOptions({
						extraParams: {excludeIds : document.getElementById("excludeIds").value}
					});	
					
					$("#product_autocomplete_input_interno").setOptions({
						extraParams: {excludeIds : document.getElementById("excludeIds").value}
					});
							
					if(varspec == 0) {
					}
					else {
						
						textVarSpec = "style=\'border:1px solid red\'"
					}
					
					$("<tr " + textVarSpec + " id='tr_"+productId.replace(/\s/g, "").replace(/^ss*/, "").replace(/ss*$/, "")+"'><td><img src='"+src_img+"' style='width:30px; height:30px;' alt='' /></td><td><a href='index.php?tab=AdminCatalogExFeatures&id_product="+productId.replace(/^ss*/, "").replace(/ss*$/, "")+"&updateproduct&token='.$tokenProducts.'' target='_blank'>" + productRef + "</a></td><td><input name='nuovo_name["+productId.replace(/ /g,'')+"]' size='31' type='text' value='"+ productName +"' /></td><td> " + productQuantity + "</td><td>" + productPrice + "<div style='float:right' id='controllo_vendita_"+productId.replace(/\s/g, "").replace(/ss*$/, "")+"'></div></td><td>" + scontoExtra + "</td><td>" + acquisto + "</td>" + marg + "<td style='text-align:center'> " + productScQta + "</td>" + tdImporto + "<td style='text-align:center'>" + deleteProd + "<input name='nuovo_prodotto["+productId.replace(/ /g,'')+"]' type='hidden' value='"+productId.replace(/^ss*/, "").replace(/ss*$/, "")+"' />" + sc_qta_1 + " " + sc_qta_2 + " " + sc_qta_3 + " " + sc_riv_1 + " " + sc_riv_2 + " " + sc_riv_3 + " " + sc_qta_1_q + " " + sc_qta_2_q + " " + sc_qta_3_q + " " + sc_riv_1_q + " " + sc_riv_2_q + " " + sc_riv_3_q + " " + przUnitario + "</td></tr>").appendTo("#tableProductsBody");
					
					productId = productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "");
					calcolaImporto(productId);
					
					var excludeIds = document.getElementById("excludeIds").value;
					excludeIds = excludeIds+productId+",";
					
					document.getElementById("excludeIds").value = excludeIds;
					$("#product_autocomplete_input").val("");
					
					
					$("#product_autocomplete_input").setOptions({
						extraParams: {excludeIds : document.getElementById("excludeIds").value}
					});	
					
					$("#product_autocomplete_input_interno").setOptions({
						extraParams: {excludeIds : document.getElementById("excludeIds").value}
					});
					
					$("body").trigger("click");			
					$("#product_autocomplete_input").trigger("click");
					$("#product_autocomplete_input").trigger("click");
					
					$("body").trigger("click");			
					$("#product_autocomplete_input_interno").trigger("click");
					$("#product_autocomplete_input_interno").trigger("click");
													
					function getExcludeIds() {
						var ids = "";
						ids += $('#excludeIds').val().replace(/-/g,',').replace(/,$/,'');
						return ids;
					}
					
				}

				{/literal}
			</script>
			
				
				
						{* ROW DI AGGIUNTA PRODOTTI ECC *}

						<div class="form-row">
							<div class="col-md-1">
								<input class="form-check-input" type="checkbox" id="prodotti_online" checked="checked" name="prodotti_online" value="option1">
								<label class="form-check-label" for="inlineCheckbox1">Online</label>
							</div>
							<div class="col-md-1">
								<input class="form-check-input" type="checkbox" id="prodotti_offline" name="prodotti_offline" value="option1">
								<label class="form-check-label" for="inlineCheckbox1">Offline</label>
							</div>
							<div class="col-md-1">
								<input class="form-check-input" type="checkbox" id="prodotti_old" name="prodotti_old" value="option1">
								<label class="form-check-label" for="inlineCheckbox1">Old</label>
							</div>
							<div class="col-md-1">
								<input class="form-check-input" type="checkbox" id="prodotti_disponibili" name="prodotti_disponibili" value="option1">
								<label class="form-check-label" for="inlineCheckbox1">{l s='Only av.'}</label> {* Solo disp. *}
							</div>

							<div class="col-md-2">
								<select id="auto_marca" class="form-control" name="brand">
									<option value="">{l s='Brand...'}</option>
									{foreach $bdl['buoni']['marche'] AS $key => $marche}
										<option value="{$marche['id']}">{$marche['name']}</option>
									{/foreach}
								</select>
							</div>
							<div class="col-md-2">
								<select id="auto_serie" class="form-control" name="serie" >
									<option value="">{l s='Series...'}</option>
									<option value="">Scegli prima un costruttore</option>
								</select>
							</div>
							<div class="col-md-2">
								<select id="auto_categoria" class="form-control" name="categorie">
									<option value="">{l s='Category...'}</option>
									{foreach $bdl['buoni']['categorie'] AS $key => $categorie}
										<option value="{$categorie['id']}">{$categorie['name']}</option>
									{/foreach}
								</select>
							</div>
							<div class="col-md-2">
								<select id="auto_fornitore" class="form-control" name="fornitori">
									<option value="">{l s='Supplier...'}</option>
									{foreach $bdl['buoni']['fornitori'] AS $key => $fornitori}
										<option value="{$fornitori['id']}">{$fornitori['name']}</option>
									{/foreach}
								</select>
							</div>
						</div>
					</div>

					<br />

					<div class="row" id="ricerca_testo">
						<div class="form-row">
							<div class="form-group col-md-1">
								<a class="btn btn-default">
									<i class="icon-search"></i> Cerca
								</a>
							</div>
							<div class="form-group col-md-11">
								<input type="hidden" value="" id="excludeIds" />
							</div>
						</div>
					</div>

					<hr />

					<div class="row" id="tabella_costi">
						<div class="form-group col-md-12">
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<th><span class="title_box ">{*immagine*}</span></th>
											<th><span class="title_box ">Codice</span></th>					{* Col + grafico e GXP-1610 *}
											<th><span class="title_box ">Nome prodotto</span></th>			{* nome *}
											<th><span class="title_box ">Qta</span></th>
											{*{if $ordine_parziale == 1}<th><span class="title_box ">Cons.</span></th>{/if}*}
											<th><span class="title_box ">Unitario</span></th>
											<th><span class="title_box ">Sconto extra %</span></th>
											<th><span class="title_box ">Acquisto</span></th>
											<th><span class="title_box ">Marginalità</span></th>
											<th><span class="title_box ">Sconto qt.</span></th>
											{*<th><span class="title_box ">Kit?</span></th>*}
											<th><span class="title_box ">Totale *</span></th>
											<th></th>
											<th></th>
										</tr>
									</thead>
									<tbody id="tableProductsBody">
										{*
										<tr>
											<td>Diritto di chiamata</td>
											<td id="diritto_chiamata_code"></td>
											<td>
												<select onchange="aggiorna_costo_unitario('diritto_chiamata')" name="diritto_chiamata" id="diritto_chiamata">
													<option value="">-- Seleziona --</option>
													<option value="EZDIRFIX">Fisso intervento < 20 km</option>
													<option value="EZDIRFIXF">Fisso intervento < 20 km festivo</option>
												</select>
											</td>
											
											<td><input onblur="aggiorna_costo_totale_riga('diritto_chiamata')" type="text" id="diritto_chiamata_qta" name="diritto_chiamata_qta"></td>
											<td><input onblur="aggiorna_costo_totale_riga('diritto_chiamata')" type="text" id="diritto_chiamata_unitario" name="diritto_chiamata_unitario"></td>
											<td><input onblur="aggiorna_costo_totale_riga('diritto_chiamata')" type="text" id="diritto_chiamata_sconto" name="diritto_chiamata_sconto"></td>
											<td><input type="text" id="diritto_chiamata_importo" name="diritto_chiamata_importo" readonly></td>
										</tr>
										*}

										{* MODIFICA PRODOTTI FOREACH *}

										{foreach $products AS $key => $product}
										{* Assign product price *}
										{if ($order->getTaxCalculationMethod() == $smarty.const.PS_TAX_EXC)}
											{assign var=prezzo_prodotto value=($product['unit_price_tax_excl'] + $product['ecotax'])}
										{else}
											{assign var=prezzo_prodotto value=$product['unit_price_tax_incl']}
										{/if}
										<tr id="tr_{$product['product_id']}">
											<td>{if isset($product.image) && $product.image->id}{$product.image_tag}{/if}</td>
											<td>
												<a class="tooltip_prodotto" title="{$product['tooltip']}" href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}&amp;id_product={$product['product_id']|intval}&amp;updateproduct">
													<img src="../img/admin/add.gif" alt="+" /> {$product.product_reference}
												</a>
											</td>
											<td><input type="text" name="product_name[{$product['id_order_detail']}]" id="nome_[{$product['product_id']}]" value="{$product['product_name']}"></td>
											<td><input type="text" name="product_quantity[{$product['id_order_detail']}]" value="{$product['product_quantity']}" id="product_quantity[{$product['product_id']}]" onkeyup="if (isArrowKey(event)) return; calcolaPrezzoScontoExtra({$product['product_id']}, 'sconto');  calcolaImporto({$product['product_id']});" style="max-width: 80%;">
											
											{if $ordine_parziale == 1}
											
											<input type="text" style="width:13px" value="'.{if ($product['cons'] != '1')}{if $product['cons'] == '0' || $product['cons'] == ''}{$quantity}{else if $product['cons'] == 'aa'}0{else}{$product['cons']|replace:"0":""}{/if}{/if}" name="qtforcons[{$product['product_id']}]" id="qtforcons[{$product['product_id']}]" onkeyup="{literal}if(this.value != 0) { document.getElementById('product_cons[{/literal}{$product['product_id']}{literal}]').checked = true; } else { document.getElementById('product_cons[{/literal}{$product['product_id']}{literal}]').checked = false; } delCons({/literal}{$product['product_id']}); " /><div id="cons_td_{$product['product_id']}"></div>
											
											{/if}
											
											</td>
											{*{if $ordine_parziale == 1}<td><input type="text" name="cons_" value="0" style="max-width: 80%;"></td>{/if}*}
											<td><input type="text" name="product_price[{$product['id_order_detail']}]" id="product_price[{$product['product_id']}]" value="{$product.unit_price_tax_excl|number_format:2:",":""}" onkeyup="calcolaPrezzoScontoExtra({$product['product_id']}, 'inverso'); calcolaImporto({$product['product_id']});">{*displayPrice price=$product.unit_price_tax_excl currency=$currency->id*}
											
											
											<div style="float:right" id="controllo_vendita_{$product['product_id']}"></div>
											 
											</td>
											<td><div class="row"><div class="col-md-12"><input type="text" name="sconto_extra[{$product['id_order_detail']}]" id="sconto_extra[{$product['product_id']}]" value="{$product['sconto_extra']}" onkeyup="if (isArrowKey(event)) return; calcolaPrezzoScontoExtra({$product['product_id']}, 'sconto'); calcolaImporto({$product['product_id']});"></div></div></td>
											<td><input type="text" name="wholesale_price[{$product['id_order_detail']}]" id="wholesale_price[{$product['product_id']}]" value="{$product['wholesale_price']}" onkeyup="javascript:document.getElementById('usa_sconti_quantita[{$product['product_id']}]').checked = false; calcolaPrezzoScontoExtra({$product['product_id']}, 'inverso'); calcolaImporto({$product['product_id']});" style="max-width: 80%;"></td>
											<td><span id="spanmarginalita[{$product['product_id']}]" name="spanmarginalita[{$product['id_order_detail']}]">{$product['margin']}</span></td>
											<td style="text-align: center;"><input type="checkbox" name="usa_sconti_quantita[{$product['id_order_detail']}]" id="usa_sconti_quantita[{$product['product_id']}]" onclick="ristabilisciPrezzo({$product['product_id']})"></td>
											{*<td>{if $product["kit"]}SI{else}NO{/if}</td>*}
											<td style="text-align:right" id="valoreImporto[{$product['product_id']}]"><span name="product_price_totale[{$product['id_order_detail']}]" id="product_price_totale[{$product['product_id']}]">{($prezzo_prodotto * ($product['product_quantity'] - $product['customizationQuantityTotal']))|string_format:"%.2f"}</span></td>
											
											<td><a class="btn btn-danger" onclick="togliImporto({$product['product_id']}); delProduct30({$product['product_id']});"><i class="icon-trash"></i></a></td>
											
											{* Campi nascosti *}

											<input type="hidden" class="prezzoacquisto" name="totaleacquisto[{$product['id_order_detail']}]" id="totaleacquisto[{$product['product_id']}]" value="{($product['wholesale_price'] * $product['product_quantity'])}" />

											<input type="hidden" class="importo" id="impImporto[{$product['product_id']}]" value="{$product['prezzo_unitario'] * $product['product_quantity']}">
											
											<input type="hidden" name="unitario[{$product['id_order_detail']}]" id="unitario[{$product['product_id']}]" value="{$product['prezzo_unitario']}" >
											
											<input type="hidden" name="sconto_acquisto_1[{$product['id_order_detail']}]" id="sconto_acquisto_1[{$product['product_id']}]" value="{$products['sc_acq_1']}" />
											<input type="hidden" name="sconto_acquisto_2[{$product['id_order_detail']}]" id="sconto_acquisto_2[{$product['product_id']}]" value="{$products['sc_acq_2']}" />
											<input type="hidden" name="sconto_acquisto_3[{$product['id_order_detail']}]" id="sconto_acquisto_3[{$product['product_id']}]" value="{$products['sc_acq_3']}" />
											
											<input type="hidden" id="sc_qta_1[{$product['product_id']}]" value="{$product['sc_qta_1']}">
											<input type="hidden" id="sc_qta_2[{$product['product_id']}]" value="{$product['sc_qta_2']}">
											<input type="hidden" id="sc_qta_3[{$product['product_id']}]" value="{$product['sc_qta_3']}">
											
											<input type="hidden" id="sc_riv_1[{$product['product_id']}]" value="{$product['sc_riv_1']}">
											<input type="hidden" id="sc_riv_2[{$product['product_id']}]" value="{$product['sc_riv_2']}">
											<input type="hidden" id="sc_riv_3[{$product['product_id']}]" value="{$product['sc_riv_3']}">
											
											<input type="hidden" id="sc_qta_1_q[{$product['product_id']}]" value="{$product['sc_qta_1_q']}">
											<input type="hidden" id="sc_qta_2_q[{$product['product_id']}]" value="{$product['sc_qta_2_q']}">
											<input type="hidden" id="sc_qta_3_q[{$product['product_id']}]" value="{$product['sc_qta_3_q']}">
											
											<input type="hidden" id="sc_riv_1_q[{$product['product_id']}]" value="{$product['sc_riv_1_q']}">
											<input type="hidden" id="sc_riv_2_q[{$product['product_id']}]" value="{$product['sc_riv_2_q']}">
											<input type="hidden" id="sc_riv_3_q[{$product['product_id']}]" value="{$product['sc_riv_3_q']}">
											
											<input type="hidden" id="oldQuantity[{$product['product_id']}]" value="{$product['quantity']}">
											<input type="hidden" id="oldPrice[{trim($product['product_id'])}]" value="{$product['product_price']}">
											

											{* <input name="Apply" type="submit" class="button" value="Aggiorna" />*}
											<input name="id_order" type="hidden" value="{$smarty.get.id_order}" />
											<input name="tax_rate" type="hidden" value="{$tax_rate}" />
											<input name="totaleprodotti" id="totaleprodotti" type="hidden" value="{$total_products}" />
											<input name="id_lang" id="id_lang" type="hidden" value="{$id_lang}" />

											{*
											<input name="product_quantity_old[{$product['id_order_detail']}]" type="hidden" value="{$product['product_quantity']}" />
											<input name="product_id[{$product['id_order_detail']}]" type="hidden" value="{$product['product_id']}" />
											*}
											
											{******************}
										</tr>
										
										
										
										{/foreach}
										
									</tbody>
									{if ($order->getTaxCalculationMethod() == $smarty.const.PS_TAX_EXC)}
										{assign var=order_product_price value=($order->total_products)}
										{assign var=order_discount_price value=$order->total_discounts_tax_excl}
										{assign var=order_wrapping_price value=$order->total_wrapping_tax_excl}
										{assign var=order_shipping_price value=$order->total_shipping_tax_excl}
									{else}
										{assign var=order_product_price value=$order->total_products_wt}
										{assign var=order_discount_price value=$order->total_discounts_tax_incl}
										{assign var=order_wrapping_price value=$order->total_wrapping_tax_incl}
										{assign var=order_shipping_price value=$order->total_shipping_tax_incl}
									{/if}
									{assign var=order_total_price value=$order->total_paid_tax_incl}
										<tfoot>
		<tr id="trasp_carrello">
		<td></td><td>TRASP</td><td>Trasporto (non modificare per calcolarlo in automatico)</td><td style="text-align:right">1</td>
		<input name="vecchia_spedizione" type="hidden"  value="{$order_shipping_price}" />
		<input type="hidden" id="trasporto_modificato" value="" />
		<td style="text-align:right" id="costo_trasporto"><input type="text" value="{$order_shipping_price|number_format:2:".":","}" size="7" onkeyup="document.getElementById(\'trasporto_modificato\').value = \'y\'; document.getElementById(\'importo_trasporto\').innerHTML=this.value; calcolaImportoConSpedizione('.$carrier_cart.');" name="transport" id="transport" style="text-align:right" /></td><td></td><td style="text-align:right"></td><td style="text-align:right"></td><td></td><td style="text-align:right" id="importo_trasporto">{displayPrice price=$order_shipping_price currency=$currency->id}</td><td></td><td></td></tr>
		
		
		
	
		<tr id="riga_commissioni">
		<td></td><td></td><td>Commissioni</td><td style="text-align:right">1</td>
		<td style="text-align:right" id="sconti_aggiuntivi">
		<input name="vecchie_commissioni" type="hidden"  value="" /> {* correggere: manca una funzione in onkeyup, perchè? anche in sconti_aggiuntivi; ci sono altri campi scritti male con php in mezzo qui; vedi 1.4*}
		<input type="text" value="0" size="7" onkeyup="document.getElementById('importo_commissioni').innerHTML=this.value;" name="total_commissions" id="total_commissions" style="text-align:right" /></td><td style="text-align:right"></td><td style="text-align:right"><td></td></td><td></td><td style="text-align:right" id="importo_commissioni"></td><td></td><td></td></tr>
		
		<tr id="sconti_extra">
		<td></td><td></td><td>Sconti</td><td style="text-align:right">1</td>
		<td style="text-align:right" id="sconti_aggiuntivi">
		<input name="vecchi_sconti" type="hidden"  value="" />
		<input type="text" value="0" size="7" onkeyup="" name="total_discounts" id="total_discounts" style="text-align:right" /></td><td style="text-align:right"></td><td style="text-align:right"><td></td></td><td></td><td style="text-align:right" id="importo_sconti"></td><td></td><td></td></tr>
		
		
		<tr><td></td><td style="width:130px"></td><td style="width:330px">Guadagno totale in euro: <span id="guadagnototale"></span> &euro;</td><td style="width:45px"></td><td style="width:45px"></td><td style="width:75px"></td><td style="width:45px"></td><td style="width:55px"><span id="marginalitatotale"></span></td><td style="width:25px"><strong>Imponibile</strong></td><td style="width:85px; text-align:right"><span id="spantotaleprodotti"> </span></td><td style="width:55px"></td><td></td></tr>
		
		<tr><td></td><td style="width:130px"></td><td style="width:310px"></td><td style="width:45px"></td><td style="width:75px"></td><td style="width:45px"></td><td style="width:75px"></td><td style="width:55px"></td><td style="width:25px"><strong>IVA incl.</strong></td><td style="width:85px; text-align:right"><span id="spantotaleprodottiiva">
		{displayPrice price=$order_total_price currency=$currency->id}
		</span></td><td style="width:10px"></td>
		<td style="width:10px"></td><!-- <td style="width:45px"></td> --></tr>
		</tfoot>	
		
								</table>
							</div>
							
							<input type="hidden" id="num_varie" value="{$bdl['buoni']['num_varie']}">
							<br />
							{if $ordine_parziale == 1}<span class="thick">Prodotto in ordine parziale ancora da consegnare (la cella nella colonna a fianco della quantità indica il numero di articoli già consegnati)</span>{/if}
						</div>
					</div>


					<div class="row">
						<div class="col-md-3">
							Metodo di pagamento:
							<select name="metodo_pagamento">
			
			<option value="bankwire" {if $order->payment == 'Bank wire' || $order->payment == 'Bankwire'}selected="selected"{/if}>Bonifico anticipato</option>
			<option value="gestpay" {if $order->payment == 'gestpay'}selected="selected"{/if}>Carta di credito (Gestpay)</option>
			<option value="Carta Amazon" {if ($order->payment =="Carta Amazon" || $order->payment == "Amazon")}selected='selected'{/if}>Carta Amazon</option>
			<option value="Carta ePrice" {if ($order->payment =="Carta ePrice" || $order->payment == "ePrice")}selected='selected'{/if}>Carta ePrice</option>
			<option value="paypal" {if $order->payment == 'paypal'}selected="selected"{/if}>PayPal</option> 
			<option value="cashondeliverywithfee" {if $order->payment == 'cashondeliverywithfee'}selected="selected"{/if}>Contrassegno</option>
			<option value="Bonifico 30 gg. fine mese" {if $order->payment == 'Bonifico 30 gg. fine mese'}selected="selected"{/if}>Bonifico 30 gg. fine mese</option>
			<option value="Bonifico 60 gg. fine mese"{if $order->payment == 'Bonifico 60 gg. fine mese'}selected="selected"{/if}>Bonifico 60 gg. fine mese</option>
			<option value="Bonifico 90 gg. fine mese"{if $order->payment == 'Bonifico 90 gg. fine mese'}selected="selected"{/if}>Bonifico 90 gg. fine mese</option>
			<option value="Bonifico 30 gg. 15 mese successivo"{if $order->payment == 'Bonifico 30 gg. 15 mese successivo'}selected="selected"{/if}>Bonifico 30 gg. 15 mese successivo</option>
			<option value="R.B. 30 GG. D.F. F.M." {if $order->payment == "R.B. 30 GG. D.F. F.M."}selected='selected'{/if}>R.B. 30 GG. D.F. F.M.</option>
			<option value="R.B. 60 GG. D.F. F.M." {if $order->payment =="R.B. 60 GG. D.F. F.M."}selected='selected'{/if}>R.B. 60 GG. D.F. F.M.</option>
			<option value="R.B. 90 GG. D.F. F.M." {if $order->payment =="R.B. 90 GG. D.F. F.M."}selected='selected'{/if}>R.B. 90 GG. D.F. F.M.</option>
			<option value="R.B. 30 GG. 5 mese successivo" {if $order->payment =="R.B. 30 GG. 5 mese successivo"}selected='selected'{/if}>R.B. 30 GG. 5 mese successivo</option>
			<option value="R.B. 30 GG. 10 mese successivo" {if $order->payment =="R.B. 30 GG. 10 mese successivo"}selected='selected'{/if}>R.B. 30 GG. 10 mese successivo</option>
			<option value="R.B. 60 GG. 5 mese successivo" {if $order->payment =="R.B. 60 GG. 5 mese successivo"}selected='selected'{/if}>R.B. 60 GG. 5 mese successivo</option>
			<option value="R.B. 60 GG. 10 mese successivo" {if $order->payment =="R.B. 60 GG. 10 mese successivo"}selected='selected'{/if}>R.B. 60 GG. 10 mese successivo</option>
			<option value="Renting"{if $order->payment == 'Renting'}selected="selected"{/if}>Renting</option>
			<option value="altro" selected="selected">Altro (vedi cella a fianco)</option>
			</select>
						</div>
						<div class="col-md-3">
							Altro metodo:
							<input type="text" name="altro_metodo" id="altro_metodo">
						</div>
						
					</div>

					<br />

					<div class="row">
						<div class="col-md-12">
							Seleziona metodo di spedizione:&nbsp;&nbsp;&nbsp;

							{$carriers_inputs}
						</div>
						
					</div>

					<br />

					Riprodurre CSV? &nbsp;
					<input type="checkbox" name="rip_csv">
					<input name="Apply" type="submit" value="Salva" />


				</div>
			</div>
		</form>
		</div>
	</div>

	{* PRODOTTI OLD DI PRESTASHOP 1.6 *}
	{hook h="displayAdminOrder" id_order=$order->id}
	<div class="row" id="start_products" style="display: none;">
		<div class="col-lg-12">
			<form class="container-command-top-spacing" action="{$current_index}&amp;vieworder&amp;token={$smarty.get.token|escape:'html':'UTF-8'}&amp;id_order={$order->id|intval}" method="post" onsubmit="return orderDeleteProduct('{l s='This product cannot be returned.'}', '{l s='Quantity to cancel is greater than quantity available.'}');">
				<input type="hidden" name="id_order" value="{$order->id}" />
				<div style="display: none">
					<input type="hidden" value="{$order->getWarehouseList()|implode}" id="warehouse_list" />
				</div>

				<div class="panel">
					<div class="panel-heading">
						<i class="icon-shopping-cart"></i>
						Prodotti <span class="badge">{$products|@count}</span>
					</div>
					<div id="refundForm">
					<!--
						<a href="#" class="standard_refund"><img src="../img/admin/add.gif" alt="{l s='Process a standard refund'}" /> {l s='Process a standard refund'}</a>
						<a href="#" class="partial_refund"><img src="../img/admin/add.gif" alt="{l s='Process a partial refund'}" /> {l s='Process a partial refund'}</a>
					-->
					</div>

					{capture "TaxMethod"}
						{if ($order->getTaxCalculationMethod() == $smarty.const.PS_TAX_EXC)}
							{l s='tax excluded.'}
						{else}
							{l s='tax included.'}
						{/if}
					{/capture}
					{if ($order->getTaxCalculationMethod() == $smarty.const.PS_TAX_EXC)}
						<input type="hidden" name="TaxMethod" value="0">
					{else}
						<input type="hidden" name="TaxMethod" value="1">
					{/if}
					<div class="table-responsive">
						<table class="table" id="orderProducts">
							<thead>
								<tr>
									<th></th>
									<th><span class="title_box ">{l s='Product'}</span></th>
									{if ($order->getTaxCalculationMethod() != $smarty.const.PS_TAX_EXC)}
									<th>
										<span class="title_box ">{l s='Unit Price'}</span>
										<small class="text-muted">{l s='tax excluded.'}</small>
									</th>
									{/if}
									<th>
										<span class="title_box ">{l s='Unit Price'}</span>
										<small class="text-muted">{$smarty.capture.TaxMethod}</small>
									</th>
									<th class="text-center"><span class="title_box ">{l s='Qty'}</span></th>
									{if $display_warehouse}<th><span class="title_box ">{l s='Warehouse'}</span></th>{/if}
									{if ($order->hasBeenPaid())}<th class="text-center"><span class="title_box ">{l s='Refunded'}</span></th>{/if}
									{if ($order->hasBeenDelivered() || $order->hasProductReturned())}
										<th class="text-center"><span class="title_box ">{l s='Returned'}</span></th>
									{/if}
									{if $stock_management}<th class="text-center"><span class="title_box ">{l s='Available quantity'}</span></th>{/if}
									<th>
										<span class="title_box ">{l s='Total'}</span>
										<small class="text-muted">{$smarty.capture.TaxMethod}</small>
									</th>
									<th style="display: none;" class="add_product_fields"></th>
									<th style="display: none;" class="edit_product_fields"></th>
									<th style="display: none;" class="standard_refund_fields">
										<i class="icon-minus-sign"></i>
										{if ($order->hasBeenDelivered() || $order->hasBeenShipped())}
											{l s='Return'}
										{elseif ($order->hasBeenPaid())}
											{l s='Refund'}
										{else}
											{l s='Cancel'}
										{/if}
									</th>
									<th style="display:none" class="partial_refund_fields">
										<span class="title_box ">{l s='Partial refund'}</span>
									</th>
									{if !$order->hasBeenDelivered()}
									<th></th>
									{/if}
								</tr>
							</thead>
							<tbody>
							{foreach from=$products item=product key=k}
								{include file='controllers/orders/_customized_data.tpl'}
								{include file='controllers/orders/_product_line.tpl'}
							{/foreach}
							{if $can_edit}
								{include file='controllers/orders/_new_product.tpl'}
							{/if}
							</tbody>
						</table>
					</div>

					{if $can_edit}
					<div class="row-margin-bottom row-margin-top order_action">
					{if !$order->hasBeenDelivered()}
						<button type="button" id="add_product" class="btn btn-default">
							<i class="icon-plus-sign"></i>
							{l s='Add a product'}
						</button>
					{/if}
						<button id="add_voucher" class="btn btn-default" type="button" >
							<i class="icon-ticket"></i>
							{l s='Add a new discount'}
						</button>
					</div>
					{/if}
					<div class="clear">&nbsp;</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="alert alert-warning">
								{l s='For this customer group, prices are displayed as: [1]%s[/1]' sprintf=[$smarty.capture.TaxMethod] tags=['<strong>']}
								{if !Configuration::get('PS_ORDER_RETURN')}
									<br/><strong>{l s='Merchandise returns are disabled'}</strong>
								{/if}
							</div>
						</div>
						<div class="col-xs-6">
							<div class="panel panel-vouchers" style="{if !sizeof($discounts)}display:none;{/if}">
								{if (sizeof($discounts) || $can_edit)}
								<div class="table-responsive">
									<table class="table">
										<thead>
											<tr>
												<th>
													<span class="title_box ">
														{l s='Discount name'}
													</span>
												</th>
												<th>
													<span class="title_box ">
														{l s='Value'}
													</span>
												</th>
												{if $can_edit}
												<th></th>
												{/if}
											</tr>
										</thead>
										<tbody>
											{foreach from=$discounts item=discount}
											<tr>
												<td>{$discount['name']}</td>
												<td>
												{if $discount['value'] != 0.00}
													-
												{/if}
												{displayPrice price=$discount['value'] currency=$currency->id}
												</td>
												{if $can_edit}
												<td>
													<a href="{$current_index}&amp;submitDeleteVoucher&amp;id_order_cart_rule={$discount['id_order_cart_rule']}&amp;id_order={$order->id}&amp;token={$smarty.get.token|escape:'html':'UTF-8'}">
														<i class="icon-minus-sign"></i>
														{l s='Delete voucher'}
													</a>
												</td>
												{/if}
											</tr>
											{/foreach}
										</tbody>
									</table>
								</div>
								<div class="current-edit" id="voucher_form" style="display:none;">
									{include file='controllers/orders/_discount_form.tpl'}
								</div>
								{/if}
							</div>
							<div class="panel panel-total">
								<div class="table-responsive">
									<table class="table">
										{if ($order->getTaxCalculationMethod() == $smarty.const.PS_TAX_EXC)}
											{assign var=order_product_price value=($order->total_products)}
											{assign var=order_discount_price value=$order->total_discounts_tax_excl}
											{assign var=order_wrapping_price value=$order->total_wrapping_tax_excl}
											{assign var=order_shipping_price value=$order->total_shipping_tax_excl}
										{else}
											{assign var=order_product_price value=$order->total_products_wt}
											{assign var=order_discount_price value=$order->total_discounts_tax_incl}
											{assign var=order_wrapping_price value=$order->total_wrapping_tax_incl}
											{assign var=order_shipping_price value=$order->total_shipping_tax_incl}
										{/if}
										<tr id="total_products">
											<td class="text-right">{l s='Products:'}</td>
											<td class="amount text-right nowrap">
												{displayPrice price=$order_product_price currency=$currency->id}
											</td>
											<td class="partial_refund_fields current-edit" style="display:none;"></td>
										</tr>
										<tr id="total_discounts" {if $order->total_discounts_tax_incl == 0}style="display: none;"{/if}>
											<td class="text-right">{l s='Discounts'}</td>
											<td class="amount text-right nowrap">
												-{displayPrice price=$order_discount_price currency=$currency->id}
											</td>
											<td class="partial_refund_fields current-edit" style="display:none;"></td>
										</tr>
										<tr id="total_wrapping" {if $order->total_wrapping_tax_incl == 0}style="display: none;"{/if}>
											<td class="text-right">{l s='Wrapping'}</td>
											<td class="amount text-right nowrap">
												{displayPrice price=$order_wrapping_price currency=$currency->id}
											</td>
											<td class="partial_refund_fields current-edit" style="display:none;"></td>
										</tr>
										<tr id="total_shipping">
											<td class="text-right">{l s='Shipping'}</td>
											<td class="amount text-right nowrap" >
												{displayPrice price=$order_shipping_price currency=$currency->id}
											</td>
											<td class="partial_refund_fields current-edit" style="display:none;">
												<div class="input-group">
													<div class="input-group-addon">
														{$currency->prefix}
														{$currency->suffix}
													</div>
													<input type="text" name="partialRefundShippingCost" value="0" />
												</div>
												<p class="help-block"><i class="icon-warning-sign"></i> {l s='(%s)' sprintf=$smarty.capture.TaxMethod}</p>
											</td>
										</tr>
										{if ($order->getTaxCalculationMethod() == $smarty.const.PS_TAX_EXC)}
			 							<tr id="total_taxes">
			 								<td class="text-right">{l s='Taxes'}</td>
			 								<td class="amount text-right nowrap" >{displayPrice price=($order->total_paid_tax_excl) currency=$currency->id}</td>
			 								<td class="partial_refund_fields current-edit" style="display:none;"></td>
			 							</tr>
			 							{/if}
										{assign var=order_total_price value=$order->total_paid_tax_incl}
										<tr id="total_order">
											<td class="text-right"><strong>{l s='Total'}</strong></td>
											<td class="amount text-right nowrap">
												<strong>{displayPrice price=$order_total_price currency=$currency->id}</strong>
											</td>
											<td class="partial_refund_fields current-edit" style="display:none;"></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div style="display: none;" class="standard_refund_fields form-horizontal panel">
						<div class="form-group">
							{if ($order->hasBeenDelivered() && Configuration::get('PS_ORDER_RETURN'))}
							<p class="checkbox">
								<label for="reinjectQuantities">
									<input type="checkbox" id="reinjectQuantities" name="reinjectQuantities" />
									{l s='Re-stock products'}
								</label>
							</p>
							{/if}
							{if ((!$order->hasBeenDelivered() && $order->hasBeenPaid()) || ($order->hasBeenDelivered() && Configuration::get('PS_ORDER_RETURN')))}
							<p class="checkbox">
								<label for="generateCreditSlip">
									<input type="checkbox" id="generateCreditSlip" name="generateCreditSlip" onclick="toggleShippingCost()" />
									{l s='Generate a credit slip'}
								</label>
							</p>
							<p class="checkbox">
								<label for="generateDiscount">
									<input type="checkbox" id="generateDiscount" name="generateDiscount" onclick="toggleShippingCost()" />
									{l s='Generate a voucher'}
								</label>
							</p>
							<p class="checkbox" id="spanShippingBack" style="display:none;">
								<label for="shippingBack">
									<input type="checkbox" id="shippingBack" name="shippingBack" />
									{l s='Repay shipping costs'}
								</label>
							</p>
							{if $order->total_discounts_tax_excl > 0 || $order->total_discounts_tax_incl > 0}
							<br/><p>{l s='This order has been partially paid by voucher. Choose the amount you want to refund:'}</p>
							<p class="radio">
								<label id="lab_refund_total_1" for="refund_total_1">
									<input type="radio" value="0" name="refund_total_voucher_off" id="refund_total_1" checked="checked" />
									{l s='Include amount of initial voucher: '}
								</label>
							</p>
							<p class="radio">
								<label id="lab_refund_total_2" for="refund_total_2">
									<input type="radio" value="1" name="refund_total_voucher_off" id="refund_total_2"/>
									{l s='Exclude amount of initial voucher: '}
								</label>
							</p>
							<div class="nowrap radio-inline">
								<label id="lab_refund_total_3" class="pull-left" for="refund_total_3">
									{l s='Amount of your choice: '}
									<input type="radio" value="2" name="refund_total_voucher_off" id="refund_total_3"/>
								</label>
								<div class="input-group col-lg-1 pull-left">
									<div class="input-group-addon">
										{$currency->prefix}
										{$currency->suffix}
									</div>
									<input type="text" class="input fixed-width-md" name="refund_total_voucher_choose" value="0"/>
								</div>
							</div>
							{/if}
						{/if}
						</div>
						{if (!$order->hasBeenDelivered() || ($order->hasBeenDelivered() && Configuration::get('PS_ORDER_RETURN')))}
						<div class="row">
							<input type="submit" name="cancelProduct" value="{if $order->hasBeenDelivered()}{l s='Return products'}{elseif $order->hasBeenPaid()}{l s='Refund products'}{else}{l s='Cancel products'}{/if}" class="btn btn-default" />
						</div>
						{/if}
					</div>
					<div style="display:none;" class="partial_refund_fields">
						<p class="checkbox">
							<label for="reinjectQuantitiesRefund">
								<input type="checkbox" id="reinjectQuantitiesRefund" name="reinjectQuantities" />
								{l s='Re-stock products'}
							</label>
						</p>
						<p class="checkbox">
							<label for="generateDiscountRefund">
								<input type="checkbox" id="generateDiscountRefund" name="generateDiscountRefund" onclick="toggleShippingCost()" />
								{l s='Generate a voucher'}
							</label>
						</p>
						{if $order->total_discounts_tax_excl > 0 || $order->total_discounts_tax_incl > 0}
						<p>{l s='This order has been partially paid by voucher. Choose the amount you want to refund:'}</p>
						<p class="radio">
							<label id="lab_refund_1" for="refund_1">
								<input type="radio" value="0" name="refund_voucher_off" id="refund_1" checked="checked" />
								{l s='Product(s) price: '}
							</label>
						</p>
						<p class="radio">
							<label id="lab_refund_2" for="refund_2">
								<input type="radio" value="1" name="refund_voucher_off" id="refund_2"/>
								{l s='Product(s) price, excluding amount of initial voucher: '}
							</label>
						</p>
						<div class="nowrap radio-inline">
								<label id="lab_refund_3" class="pull-left" for="refund_3">
									{l s='Amount of your choice: '}
									<input type="radio" value="2" name="refund_voucher_off" id="refund_3"/>
								</label>
								<div class="input-group col-lg-1 pull-left">
									<div class="input-group-addon">
										{$currency->prefix}
										{$currency->suffix}
									</div>
									<input type="text" class="input fixed-width-md" name="refund_voucher_choose" value="0"/>
								</div>
							</div>
						{/if}
						<br/>
						<button type="submit" name="partialRefund" class="btn btn-default">
							<i class="icon-check"></i> {l s='Partial refund'}
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	
	
	{* }
	<div class="row">
		<div class="col-lg-7">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-credit-card"></i>
					{l s='Order'}
					<span class="badge">{$order->reference}</span>
					<span class="badge">{l s="#"}{$order->id}</span>
					<div class="panel-heading-action">
						<div class="btn-group">
							<a class="btn btn-default{if !$previousOrder} disabled{/if}" href="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;vieworder&amp;id_order={$previousOrder|intval}">
								<i class="icon-backward"></i>
							</a>
							<a class="btn btn-default{if !$nextOrder} disabled{/if}" href="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;vieworder&amp;id_order={$nextOrder|intval}">
								<i class="icon-forward"></i>
							</a>
						</div>
					</div>
				</div>
				<!-- Orders Actions -->
				<div class="well hidden-print">
					<a class="btn btn-default" href="javascript:window.print()">
						<i class="icon-print"></i>
						{l s='Print order'}
					</a>
					&nbsp;
					{if Configuration::get('PS_INVOICE') && count($invoices_collection) && $order->invoice_number}
						<a data-selenium-id="view_invoice" class="btn btn-default _blank" href="{$link->getAdminLink('AdminPdf')|escape:'html':'UTF-8'}&amp;submitAction=generateInvoicePDF&amp;id_order={$order->id|intval}">
							<i class="icon-file"></i>
							{l s='View invoice'}
						</a>
					{else}
						<span class="span label label-inactive">
							<i class="icon-remove"></i>
							{l s='No invoice'}
						</span>
					{/if}
					&nbsp;
					{if $order->delivery_number}
						<a class="btn btn-default _blank"  href="{$link->getAdminLink('AdminPdf')|escape:'html':'UTF-8'}&amp;submitAction=generateDeliverySlipPDF&amp;id_order={$order->id|intval}">
							<i class="icon-truck"></i>
							{l s='View delivery slip'}
						</a>
					{else}
						<span class="span label label-inactive">
							<i class="icon-remove"></i>
							{l s='No delivery slip'}
						</span>
					{/if}
					&nbsp;
					{if Configuration::get('PS_ORDER_RETURN')}
						<a id="desc-order-standard_refund" class="btn btn-default" href="#refundForm">
							<i class="icon-exchange"></i>
							{if $order->hasBeenShipped()}
								{l s='Return products'}
							{elseif $order->hasBeenPaid()}
								{l s='Standard refund'}
							{else}
								{l s='Cancel products'}
							{/if}
						</a>
						&nbsp;
					{/if}
					{if $order->hasInvoice()}
						<a id="desc-order-partial_refund" class="btn btn-default" href="#refundForm">
							<i class="icon-exchange"></i>
							{l s='Partial refund'}
						</a>
					{/if}
				</div>
				<!-- Tab nav -->
				<ul class="nav nav-tabs" id="tabOrder">
					{$HOOK_TAB_ORDER}
					<li class="active">
						<a href="#status">
							<i class="icon-time"></i>
							{l s='Status'} <span class="badge">{$history|@count}</span>
						</a>
					</li>
					<li>
						<a href="#documents">
							<i class="icon-file-text"></i>
							{l s='Documents'} <span class="badge">{$order->getDocuments()|@count}</span>
						</a>
					</li>
				</ul>
				<!-- Tab content -->
				<div class="tab-content panel">
					{$HOOK_CONTENT_ORDER}
					<!-- Tab status -->
					<div class="tab-pane active" id="status">
						<h4 class="visible-print">{l s='Status'} <span class="badge">({$history|@count})</span></h4>
						<!-- History of status -->
						<div class="table-responsive">
							<table class="table history-status row-margin-bottom">
								<tbody>
									{foreach from=$history item=row key=key}
										{if ($key == 0)}
											<tr>
												<td style="background-color:{$row['color']}"><img src="../img/os/{$row['id_order_state']|intval}.gif" width="16" height="16" alt="{$row['ostate_name']|stripslashes}" /></td>
												<td style="background-color:{$row['color']};color:{$row['text-color']}">{$row['ostate_name']|stripslashes}</td>
												<td style="background-color:{$row['color']};color:{$row['text-color']}">{if $row['employee_lastname']}{$row['employee_firstname']|stripslashes} {$row['employee_lastname']|stripslashes}{/if}</td>
												<td style="background-color:{$row['color']};color:{$row['text-color']}">{dateFormat date=$row['date_add'] full=true}</td>
												<td style="background-color:{$row['color']};color:{$row['text-color']}" class="text-right">
													{if $row['send_email']|intval}
														<a class="btn btn-default" href="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;vieworder&amp;id_order={$order->id|intval}&amp;sendStateEmail={$row['id_order_state']|intval}&amp;id_order_history={$row['id_order_history']|intval}" title="{l s='Resend this email to the customer'}">
															<i class="icon-mail-reply"></i>
															{l s='Resend email'}
														</a>
													{/if}
												</td>
											</tr>
										{else}
											<tr>
												<td><img src="../img/os/{$row['id_order_state']|intval}.gif" width="16" height="16" /></td>
												<td>{$row['ostate_name']|stripslashes}</td>
												<td>{if $row['employee_lastname']}{$row['employee_firstname']|stripslashes} {$row['employee_lastname']|stripslashes}{else}&nbsp;{/if}</td>
												<td>{dateFormat date=$row['date_add'] full=true}</td>
												<td class="text-right">
													{if $row['send_email']|intval}
														<a class="btn btn-default" href="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;vieworder&amp;id_order={$order->id|intval}&amp;sendStateEmail={$row['id_order_state']|intval}&amp;id_order_history={$row['id_order_history']|intval}" title="{l s='Resend this email to the customer'}">
															<i class="icon-mail-reply"></i>
															{l s='Resend email'}
														</a>
													{/if}
												</td>
											</tr>
										{/if}
									{/foreach}
								</tbody>
							</table>
						</div>
						<!-- Change status form -->
						<form action="{$currentIndex|escape:'html':'UTF-8'}&amp;vieworder&amp;token={$smarty.get.token}" method="post" class="form-horizontal well hidden-print">
							<div class="row">
								<div class="col-lg-9">
									<select id="id_order_state" class="chosen form-control" name="id_order_state">
									{foreach from=$states item=state}
										<option value="{$state['id_order_state']|intval}"{if isset($currentState) && $state['id_order_state'] == $currentState->id} selected="selected" disabled="disabled"{/if}>{$state['name']|escape}</option>
									{/foreach}
									</select>
									<input type="hidden" name="id_order" value="{$order->id}" />
								</div>
								<div class="col-lg-3">
									<button type="submit" name="submitState" class="btn btn-primary">
										{l s='Update status'}
									</button>
								</div>
							</div>
						</form>
					</div>
					<!-- Tab documents -->
					<div class="tab-pane" id="documents">
						<h4 class="visible-print">{l s='Documents'} <span class="badge">({$order->getDocuments()|@count})</span></h4>

						{include file='controllers/orders/_documents.tpl'}
					</div>
				</div>
				<script>
					$('#tabOrder a').click(function (e) {
						e.preventDefault()
						$(this).tab('show')
					})
				</script>
				<hr />
				<!-- Tab nav -->
				<ul class="nav nav-tabs" id="myTab">
					{$HOOK_TAB_SHIP}
					<li class="active">
						<a href="#shipping">
							<i class="icon-truck "></i>
							{l s='Shipping'} <span class="badge">{$order->getShipping()|@count}</span>
						</a>
					</li>
					<li>
						<a href="#returns">
							<i class="icon-undo"></i>
							{l s='Merchandise Returns'} <span class="badge">{$order->getReturn()|@count}</span>
						</a>
					</li>
				</ul>
				<!-- Tab content -->
				<div class="tab-content panel">
				{$HOOK_CONTENT_SHIP}
					<!-- Tab shipping -->
					<div class="tab-pane active" id="shipping">
						<h4 class="visible-print">{l s='Shipping'} <span class="badge">({$order->getShipping()|@count})</span></h4>
						<!-- Shipping block -->
						{if !$order->isVirtual()}
						<div class="form-horizontal">
							{if $order->gift_message}
							<div class="form-group">
								<label class="control-label col-lg-3">{l s='Message'}</label>
								<div class="col-lg-9">
									<p class="form-control-static">{$order->gift_message|nl2br}</p>
								</div>
							</div>
							{/if}
							{include file='controllers/orders/_shipping.tpl'}
							{if $carrierModuleCall}
								{$carrierModuleCall}
							{/if}
							<hr />
							{if $order->recyclable}
								<span class="label label-success"><i class="icon-check"></i> {l s='Recycled packaging'}</span>
							{else}
								<span class="label label-inactive"><i class="icon-remove"></i> {l s='Recycled packaging'}</span>
							{/if}

							{if $order->gift}
								<span class="label label-success"><i class="icon-check"></i> {l s='Gift wrapping'}</span>
							{else}
								<span class="label label-inactive"><i class="icon-remove"></i> {l s='Gift wrapping'}</span>
							{/if}
						</div>
						{/if}
					</div>
					<!-- Tab returns -->
					<div class="tab-pane" id="returns">
						<h4 class="visible-print">{l s='Merchandise Returns'} <span class="badge">({$order->getReturn()|@count})</span></h4>
						{if !$order->isVirtual()}
						<!-- Return block -->
							{if $order->getReturn()|count > 0}
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<th><span class="title_box ">{l s='Date'}</span></th>
											<th><span class="title_box ">{l s='Type'}</span></th>
											<th><span class="title_box ">{l s='Carrier'}</span></th>
											<th><span class="title_box ">{l s='Tracking number'}</span></th>
										</tr>
									</thead>
									<tbody>
										{foreach from=$order->getReturn() item=line}
										<tr>
											<td>{$line.date_add}</td>
											<td>{l s=$line.type}</td>
											<td>{$line.state_name}</td>
											<td class="actions">
												<span class="shipping_number_show">{if isset($line.url) && isset($line.tracking_number)}<a href="{$line.url|replace:'@':$line.tracking_number|escape:'html':'UTF-8'}">{$line.tracking_number}</a>{elseif isset($line.tracking_number)}{$line.tracking_number}{/if}</span>
												{if $line.can_edit}
												<form method="post" action="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;vieworder&amp;id_order={$order->id|intval}&amp;id_order_invoice={if $line.id_order_invoice}{$line.id_order_invoice|intval}{else}0{/if}&amp;id_carrier={if $line.id_carrier}{$line.id_carrier|escape:'html':'UTF-8'}{else}0{/if}">
													<span class="shipping_number_edit" style="display:none;">
														<button type="button" name="tracking_number">
															{$line.tracking_number|htmlentities}
														</button>
														<button type="submit" class="btn btn-default" name="submitShippingNumber">
															{l s='Update'}
														</button>
													</span>
													<button href="#" class="edit_shipping_number_link">
														<i class="icon-pencil"></i>
														{l s='Edit'}
													</button>
													<button href="#" class="cancel_shipping_number_link" style="display: none;">
														<i class="icon-remove"></i>
														{l s='Cancel'}
													</button>
												</form>
												{/if}
											</td>
										</tr>
										{/foreach}
									</tbody>
								</table>
							</div>
							{else}
							<div class="list-empty hidden-print">
								<div class="list-empty-msg">
									<i class="icon-warning-sign list-empty-icon"></i>
									{l s='No merchandise returned yet'}
								</div>
							</div>
							{/if}
							{if $carrierModuleCall}
								{$carrierModuleCall}
							{/if}
						{/if}
					</div>
				</div>
				<script>
					$('#myTab a').click(function (e) {
						e.preventDefault()
						$(this).tab('show')
					})
				</script>
			</div>
			<!-- Payments block -->
			<div id="formAddPaymentPanel" class="panel">
				<div class="panel-heading">
					<i class="icon-money"></i>
					{l s="Payment"} <span class="badge">{$order->getOrderPayments()|@count}</span>
				</div>
				{if count($order->getOrderPayments()) > 0}
					<p class="alert alert-danger"{if round($orders_total_paid_tax_incl, 2) == round($total_paid, 2) || (isset($currentState) && $currentState->id == 6)} style="display: none;"{/if}>
						{l s='Warning'}
						<strong>{displayPrice price=$total_paid currency=$currency->id}</strong>
						{l s='paid instead of'}
						<strong class="total_paid">{displayPrice price=$orders_total_paid_tax_incl currency=$currency->id}</strong>
						{foreach $order->getBrother() as $brother_order}
							{if $brother_order@first}
								{if count($order->getBrother()) == 1}
									<br />{l s='This warning also concerns order '}
								{else}
									<br />{l s='This warning also concerns the next orders:'}
								{/if}
							{/if}
							<a href="{$current_index}&amp;vieworder&amp;id_order={$brother_order->id}&amp;token={$smarty.get.token|escape:'html':'UTF-8'}">
								#{'%06d'|sprintf:$brother_order->id}
							</a>
						{/foreach}
					</p>
				{/if}
				<form id="formAddPayment"  method="post" action="{$current_index}&amp;vieworder&amp;id_order={$order->id}&amp;token={$smarty.get.token|escape:'html':'UTF-8'}">
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th><span class="title_box ">{l s='Date'}</span></th>
									<th><span class="title_box ">{l s='Payment method'}</span></th>
									<th><span class="title_box ">{l s='Transaction ID'}</span></th>
									<th><span class="title_box ">{l s='Amount'}</span></th>
									<th><span class="title_box ">{l s='Invoice'}</span></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								{foreach from=$order->getOrderPaymentCollection() item=payment}
								<tr>
									<td>{dateFormat date=$payment->date_add full=true}</td>
									<td>{$payment->payment_method|escape:'html':'UTF-8'}</td>
									<td>{$payment->transaction_id|escape:'html':'UTF-8'}</td>
									<td>{displayPrice price=$payment->amount currency=$payment->id_currency}</td>
									<td>
									{if $invoice = $payment->getOrderInvoice($order->id)}
										{$invoice->getInvoiceNumberFormatted($current_id_lang, $order->id_shop)}
									{else}
									{/if}
									</td>
									<td class="actions">
										<button class="btn btn-default open_payment_information">
											<i class="icon-search"></i>
											{l s='Details'}
										</button>
									</td>
								</tr>
								<tr class="payment_information" style="display: none;">
									<td colspan="5">
										<p>
											<b>{l s='Card Number'}</b>&nbsp;
											{if $payment->card_number}
												{$payment->card_number}
											{else}
												<i>{l s='Not defined'}</i>
											{/if}
										</p>
										<p>
											<b>{l s='Card Brand'}</b>&nbsp;
											{if $payment->card_brand}
												{$payment->card_brand}
											{else}
												<i>{l s='Not defined'}</i>
											{/if}
										</p>
										<p>
											<b>{l s='Card Expiration'}</b>&nbsp;
											{if $payment->card_expiration}
												{$payment->card_expiration}
											{else}
												<i>{l s='Not defined'}</i>
											{/if}
										</p>
										<p>
											<b>{l s='Card Holder'}</b>&nbsp;
											{if $payment->card_holder}
												{$payment->card_holder}
											{else}
												<i>{l s='Not defined'}</i>
											{/if}
										</p>
									</td>
								</tr>
								{foreachelse}
								<tr>
									<td class="list-empty hidden-print" colspan="6">
										<div class="list-empty-msg">
											<i class="icon-warning-sign list-empty-icon"></i>
											{l s='No payment methods are available'}
										</div>
									</td>
								</tr>
								{/foreach}
								<tr class="current-edit hidden-print">
									<td>
										<div class="input-group fixed-width-xl">
											<input type="text" name="payment_date" class="datepicker" value="{date('Y-m-d')}" />
											<div class="input-group-addon">
												<i class="icon-calendar-o"></i>
											</div>
										</div>
									</td>
									<td>
										<input name="payment_method" list="payment_method" class="payment_method">
										<datalist id="payment_method">
										{foreach from=$payment_methods item=payment_method}
											<option value="{$payment_method}">
										{/foreach}
										</datalist>
									</td>
									<td>
										<input type="text" name="payment_transaction_id" value="" class="form-control fixed-width-sm"/>
									</td>
									<td>
										<input type="text" name="payment_amount" value="" class="form-control fixed-width-sm pull-left" />
										<select name="payment_currency" class="payment_currency form-control fixed-width-xs pull-left">
											{foreach from=$currencies item=current_currency}
												<option value="{$current_currency['id_currency']}"{if $current_currency['id_currency'] == $currency->id} selected="selected"{/if}>{$current_currency['sign']}</option>
											{/foreach}
										</select>
									</td>
									<td>
										{if count($invoices_collection) > 0}
											<select name="payment_invoice" id="payment_invoice">
											{foreach from=$invoices_collection item=invoice}
												<option value="{$invoice->id}" selected="selected">{$invoice->getInvoiceNumberFormatted($current_id_lang, $order->id_shop)}</option>
											{/foreach}
											</select>
										{/if}
									</td>
									<td class="actions">
										<button class="btn btn-primary" type="submit" name="submitAddPayment">
											{l s='Add'}
										</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</form>
				{if (!$order->valid && sizeof($currencies) > 1)}
					<form class="form-horizontal well" method="post" action="{$currentIndex|escape:'html':'UTF-8'}&amp;vieworder&amp;id_order={$order->id}&amp;token={$smarty.get.token|escape:'html':'UTF-8'}">
						<div class="row">
							<label class="control-label col-lg-3">{l s='Change currency'}</label>
							<div class="col-lg-6">
								<select name="new_currency">
								{foreach from=$currencies item=currency_change}
									{if $currency_change['id_currency'] != $order->id_currency}
									<option value="{$currency_change['id_currency']}">{$currency_change['name']} - {$currency_change['sign']}</option>
									{/if}
								{/foreach}
								</select>
								<p class="help-block">{l s='Do not forget to update your exchange rate before making this change.'}</p>
							</div>
							<div class="col-lg-3">
								<button type="submit" class="btn btn-default" name="submitChangeCurrency"><i class="icon-refresh"></i> {l s='Change'}</button>
							</div>
						</div>
					</form>
				{/if}
			</div>
			{hook h="displayAdminOrderLeft" id_order=$order->id}
		</div>
		<div class="col-lg-5">
			<!-- Customer informations -->
			<div class="panel">
				{if $customer->id}
					<div class="panel-heading">
						<i class="icon-user"></i>
						{l s='Customer'}
						<span class="badge">
							<a href="?tab=AdminCustomers&amp;id_customer={$customer->id}&amp;viewcustomer&amp;token={getAdminToken tab='AdminCustomers'}" target="_blank">
								{if Configuration::get('PS_B2B_ENABLE')}{$customer->company} - {/if}
								{$gender->name|escape:'html':'UTF-8'}
								{$customer->firstname}
								{$customer->lastname}
							</a>
						</span>
						<span class="badge">
							{l s='#'}{$customer->id}
						</span>
					</div>
					<div class="row">
						<div class="col-xs-6">
							{if ($customer->isGuest())}
								{l s='This order has been placed by a guest.'}
								{if (!Customer::customerExists($customer->email))}
									<form method="post" action="index.php?tab=AdminCustomers&amp;id_customer={$customer->id}&amp;id_order={$order->id|intval}&amp;token={getAdminToken tab='AdminCustomers'}">
										<input type="hidden" name="id_lang" value="{$order->id_lang}" />
										<input class="btn btn-default" type="submit" name="submitGuestToCustomer" value="{l s='Transform a guest into a customer'}" />
										<p class="help-block">{l s='This feature will generate a random password and send an email to the customer.'}</p>
									</form>
								{else}
									<div class="alert alert-warning">
										{l s='A registered customer account has already claimed this email address'}
									</div>
								{/if}
							{else}
								<dl class="well list-detail">
									<dt>{l s='Email'}</dt>
										<dd><a href="mailto:{$customer->email}"><i class="icon-envelope-o"></i> {$customer->email}</a></dd>
									<dt>{l s='Account registered'}</dt>
										<dd class="text-muted"><i class="icon-calendar-o"></i> {dateFormat date=$customer->date_add full=true}</dd>
									<dt>{l s='Valid orders placed'}</dt>
										<dd><span class="badge">{$customerStats['nb_orders']|intval}</span></dd>
									<dt>{l s='Total spent since registration'}</dt>
										<dd><span class="badge badge-success">{displayPrice price=Tools::ps_round(Tools::convertPrice($customerStats['total_orders'], $currency), 2) currency=$currency->id}</span></dd>
									{if Configuration::get('PS_B2B_ENABLE')}
										<dt>{l s='Siret'}</dt>
											<dd>{$customer->siret}</dd>
										<dt>{l s='APE'}</dt>
											<dd>{$customer->ape}</dd>
									{/if}
								</dl>
							{/if}
						</div>

						<div class="col-xs-6">
							<div class="form-group hidden-print">
								<a href="?tab=AdminCustomers&amp;id_customer={$customer->id}&amp;viewcustomer&amp;token={getAdminToken tab='AdminCustomers'}" target="_blank" class="btn btn-default btn-block">{l s='View full details...'}</a>
							</div>
							<div class="panel panel-sm">
								<div class="panel-heading">
									<i class="icon-eye-slash"></i>
									{l s='Private note'}
								</div>
								<form id="customer_note" class="form-horizontal" action="ajax.php" method="post" onsubmit="saveCustomerNote({$customer->id});return false;" >
									<div class="form-group">
										<div class="col-lg-12">
											<textarea name="note" id="noteContent" class="textarea-autosize" onkeyup="$(this).val().length > 0 ? $('#submitCustomerNote').removeAttr('disabled') : $('#submitCustomerNote').attr('disabled', 'disabled')">{$customer->note}</textarea>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<button type="submit" id="submitCustomerNote" class="btn btn-default pull-right" disabled="disabled">
												<i class="icon-save"></i>
												{l s='Save'}
											</button>
										</div>
									</div>
									<span id="note_feedback"></span>
								</form>
							</div>
						</div>
					</div>
				{/if}
				<!-- Tab nav -->
				<div class="row">
					<ul class="nav nav-tabs" id="tabAddresses">
						<li class="active">
							<a href="#addressShipping">
								<i class="icon-truck"></i>
								{l s='Shipping address'}
							</a>
						</li>
						<li>
							<a href="#addressInvoice">
								<i class="icon-file-text"></i>
								{l s='Invoice address'}
							</a>
						</li>
					</ul>
					<!-- Tab content -->
					<div class="tab-content panel">
						<!-- Tab status -->
						<div class="tab-pane  in active" id="addressShipping">
							<!-- Addresses -->
							<h4 class="visible-print">{l s='Shipping address'}</h4>
							{if !$order->isVirtual()}
							<!-- Shipping address -->
								{if $can_edit}
									<form class="form-horizontal hidden-print" method="post" action="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;vieworder&amp;id_order={$order->id|intval}">
										<div class="form-group">
											<div class="col-lg-9">
												<select id="id_address_select" name="id_address">
													{foreach from=$customer_addresses item=address}
													<option value="{$address['id_address']}"
														{if $address['id_address'] == $order->id_address_delivery}
															selected="selected"
														{/if}>
														{$address['alias']} -
														{$address['address1']}
														{$address['postcode']}
														{$address['city']}
														{if !empty($address['state'])}
															{$address['state']}
														{/if},
														{$address['country']}
													</option>
													{/foreach}
												</select>
											</div>
											<div class="col-lg-3">
												<button class="btn btn-default" type="submit" name="submitAddressShipping"><i class="icon-refresh"></i> {l s='Change'}</button>
											</div>
										</div>
									</form>
								{/if}
								<div class="well">
									<div class="row">
										<div class="col-sm-6">
											<a class="btn btn-default pull-right" href="?tab=AdminAddresses&amp;id_address={$addresses.delivery->id}&amp;addaddress&amp;realedit=1&amp;id_order={$order->id}&amp;address_type=1&amp;token={getAdminToken tab='AdminAddresses'}&amp;back={$smarty.server.REQUEST_URI|urlencode}">
												<i class="icon-pencil"></i>
												{l s='Edit'}
											</a>
											{displayAddressDetail address=$addresses.delivery newLine='<br />'}
											{if $addresses.delivery->other}
												<hr />{$addresses.delivery->other}<br />
											{/if}
										</div>
										<div class="col-sm-6 hidden-print">
											<div id="map-delivery-canvas" style="height: 190px"></div>
										</div>
									</div>
								</div>
							{/if}
						</div>
						<div class="tab-pane " id="addressInvoice">
							<!-- Invoice address -->
							<h4 class="visible-print">{l s='Invoice address'}</h4>
							{if $can_edit}
								<form class="form-horizontal hidden-print" method="post" action="{$link->getAdminLink('AdminOrders')|escape:'html':'UTF-8'}&amp;vieworder&amp;id_order={$order->id|intval}">
									<div class="form-group">
										<div class="col-lg-9">
											<select name="id_address" id="id_address">
												{foreach from=$customer_addresses item=address}
												<option value="{$address['id_address']}"
													{if $address['id_address'] == $order->id_address_invoice}
													selected="selected"
													{/if}>
													{$address['alias']} -
													{$address['address1']}
													{$address['postcode']}
													{$address['city']}
													{if !empty($address['state'])}
														{$address['state']}
													{/if},
													{$address['country']}
												</option>
												{/foreach}
											</select>
										</div>
										<div class="col-lg-3">
											<button class="btn btn-default" type="submit" name="submitAddressInvoice"><i class="icon-refresh"></i> {l s='Change'}</button>
										</div>
									</div>
								</form>
							{/if}
							<div class="well">
								<div class="row">
									<div class="col-sm-6">
										<a class="btn btn-default pull-right" href="?tab=AdminAddresses&amp;id_address={$addresses.invoice->id}&amp;addaddress&amp;realedit=1&amp;id_order={$order->id}&amp;address_type=2&amp;back={$smarty.server.REQUEST_URI|urlencode}&amp;token={getAdminToken tab='AdminAddresses'}">
											<i class="icon-pencil"></i>
											{l s='Edit'}
										</a>
										{displayAddressDetail address=$addresses.invoice newLine='<br />'}
										{if $addresses.invoice->other}
											<hr />{$addresses.invoice->other}<br />
										{/if}
									</div>
									<div class="col-sm-6 hidden-print">
										<div id="map-invoice-canvas" style="height: 190px"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<script>
					$('#tabAddresses a').click(function (e) {
						e.preventDefault()
						$(this).tab('show')
					})
				</script>
			</div>
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-envelope"></i> {l s='Messages'} <span class="badge">{sizeof($customer_thread_message)}</span>
				</div>
				{if (sizeof($messages))}
					<div class="panel panel-highlighted">
						<div class="message-item">
							{foreach from=$messages item=message}
								<div class="message-avatar">
									<div class="avatar-md">
										<i class="icon-user icon-2x"></i>
									</div>
								</div>
								<div class="message-body">

									<span class="message-date">&nbsp;<i class="icon-calendar"></i>
										{dateFormat date=$message['date_add']} -
									</span>
									<h4 class="message-item-heading">
										{if ($message['elastname']|escape:'html':'UTF-8')}{$message['efirstname']|escape:'html':'UTF-8'}
											{$message['elastname']|escape:'html':'UTF-8'}{else}{$message['cfirstname']|escape:'html':'UTF-8'} {$message['clastname']|escape:'html':'UTF-8'}
										{/if}
										{if ($message['private'] == 1)}
											<span class="badge badge-info">{l s='Private'}</span>
										{/if}
									</h4>
									<p class="message-item-text">
										{$message['message']|escape:'html':'UTF-8'|nl2br}
									</p>
								</div>
								{*if ($message['is_new_for_me'])}
									<a class="new_message" title="{l s='Mark this message as \'viewed\''}" href="{$smarty.server.REQUEST_URI}&amp;token={$smarty.get.token}&amp;messageReaded={$message['id_message']}">
										<i class="icon-ok"></i>
									</a>
								{/if*}
								{*}
							{/foreach}
						</div>
					</div>
				{/if}
				<div id="messages" class="well hidden-print">
					<form action="{$smarty.server.REQUEST_URI|escape:'html':'UTF-8'}&amp;token={$smarty.get.token|escape:'html':'UTF-8'}" method="post" onsubmit="if (getE('visibility').checked == true) return confirm('{l s='Do you want to send this message to the customer?'}');">
						<div id="message" class="form-horizontal">
							<div class="form-group">
								<label class="control-label col-lg-3">{l s='Choose a standard message'}</label>
								<div class="col-lg-9">
									<select class="chosen form-control" name="order_message" id="order_message" onchange="orderOverwriteMessage(this, '{l s='Do you want to overwrite your existing message?'}')">
										<option value="0" selected="selected">-</option>
										{foreach from=$orderMessages item=orderMessage}
										<option value="{$orderMessage['message']|escape:'html':'UTF-8'}">{$orderMessage['name']}</option>
										{/foreach}
									</select>
									<p class="help-block">
										<a href="{$link->getAdminLink('AdminOrderMessage')|escape:'html':'UTF-8'}">
											{l s='Configure predefined messages'}
											<i class="icon-external-link"></i>
										</a>
									</p>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-lg-3">{l s='Display to customer?'}</label>
								<div class="col-lg-9">
									<span class="switch prestashop-switch fixed-width-lg">
										<input type="radio" name="visibility" id="visibility_on" value="0" />
										<label for="visibility_on">
											{l s='Yes'}
										</label>
										<input type="radio" name="visibility" id="visibility_off" value="1" checked="checked" />
										<label for="visibility_off">
											{l s='No'}
										</label>
										<a class="slide-button btn"></a>
									</span>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-lg-3">{l s='Message'}</label>
								<div class="col-lg-9">
									<textarea id="txt_msg" class="textarea-autosize" name="message">{Tools::getValue('message')|escape:'html':'UTF-8'}</textarea>
									<p id="nbchars"></p>
								</div>
							</div>


							
							<button type="submit" id="submitMessage" class="btn btn-primary pull-right" name="submitMessage">
								{l s='Send message'}
							</button>
							<a class="btn btn-default" href="{$link->getAdminLink('AdminCustomerThreads')|escape:'html':'UTF-8'}&amp;id_order={$order->id|intval}">
								{l s='Show all messages'}
								<i class="icon-external-link"></i>
							</a>
						</div>
					</form>
				</div>
			</div>
			{hook h="displayAdminOrderRight" id_order=$order->id}
		</div>
	</div>
	{ *}

	{* Area carrello *}
	{if !$is_agente}
	<div class="row">
		<div class="col-lg-12">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-shopping-cart"></i>
					Area carrello
				</div>

				<div class="row">
					<div class="col-lg-12">
						<a href="{$link->getAdminLink('AdminCarts')}&amp;id_cart={$cart->id}&amp;viewcart" target="_blank">Carrello numero {$cart->id}</a>
						<br />
						<span class="thick">Quest'ordine arriva da un carrello creato da{$area_carrello['creato_da_nome']}</span>
						{if $cart->name != ''}
							<br /><span class="thick">Oggetto del carrello: {$cart->name}</span>
						{/if}
					</div>
				</div>

				<br />

				<form name="form_area_carrello" id="form_area_carrello" action="ajax.php" method="post" onsubmit="saveOrdineAreaCarrello(); return false;">
					<div class="row">
						<div class="col-lg-12">
							<label>Premessa:</label>
							<textarea id="premessa" name="premessa" class="autoload_rte">{$cart->premessa|escape:'html':'UTF-8'}</textarea>
						</div>
					</div>

					<br />

					<div class="row">
						<div class="col-lg-6">
							<label>Esigenze del cliente:</label>
							<textarea id="esigenze" name="esigenze" class="autoload_rte">{$cart->esigenze|escape:'html':'UTF-8'}</textarea>
						</div>
						<div class="col-lg-6">
							<label>Risorse:</label>
							<textarea id="risorse" name="risorse" class="autoload_rte">{$cart->risorse|escape:'html':'UTF-8'}</textarea>
						</div>
					</div>

					<br />

					<div class="row">
						<div class="col-lg-12">
							<label>Note:</label>
							<textarea id="note" name="note" class="autoload_rte">{$cart->note|escape:'html':'UTF-8'}</textarea>
						</div>
					</div>

					<br />

					{if $area_carrello['cart_old']}
					<div class="row">
						<div class="form-group col-md-12">
							<label for="note_private">Nota privata:</label>
							<p class="form-control-static">
								<textarea name="note_private" id="note_private">{$area_carrello['cart_nota_old']|escape:'htmlall'}</textarea>
							</p>
						</div>
					</div>
					<br />
					{/if}

					<div class="row">
						<button type="submit" name="submitAreaCarrello" id="submitAreaCarrello" class="btn btn-primary pull-left">
							Salva premessa, esigenze, risorse, note e nota privata
						</button>
						<div id="note_feedback"></div>
					</div>
				</form>

				<br />

				<div class="row">
					<div class="panel">
						<div class="panel-heading">
							<i class="icon-eye-close"></i> {l s='Private notes'}

							<div class="panel-heading-action">
								<a class="btn btn-default add-new">
									<i class="icon-plus-sign"></i>
									{l s='Add private note'}
								</a>
							</div>
						</div>
						<div class="form-group">
							<table class="table table-bordered tabella_note" id="noteTable">
								<thead>
									<tr>
										<th>{l s='Note'}</th>
										<th>{l s='Created by'}</th>
										<th>{l s='Last edit'}</th>
										<th>{l s='Actions'}</th>
									</tr>
								</thead>
								<tbody>
									<tr style="display:none"> {* Necessario per aggiungere la prima nota in js *}
										<td class="mod"></td>
										<td></td>
										<td></td>
										<td class="azioni_note">
											<a class="add" title="Add" data-toggle="tooltip"><i class="icon-save"></i></a>
											<a class="edit" title="Edit" data-toggle="tooltip"><i class="icon-edit"></i></a>
											<a class="delete" title="Delete" data-toggle="tooltip"><i class="icon-trash"></i></a>
										</td>
									</tr>
								{foreach $area_carrello['cart_note'] as $cart_nota}
									<tr dbid="{$cart_nota['id_note']}" id="tr_note_{$cart_nota['id_note']}">
										<td id="note_nota[{$cart_nota['id_note']}]" class="mod">{$cart_nota['note']}</td>
										<td id="note_id_employee[{$cart_nota['id_note']}]">{$cart_nota['creato_da']}</td>
										<td id="note_date_add[{$cart_nota['id_note']}]">{$cart_nota['date_upd']}</td>
										<td class="azioni_note">
											<a class="add" title="Add" data-toggle="tooltip"><i class="icon-save"></i></a>
											<a class="edit" title="Edit" data-toggle="tooltip"><i class="icon-edit"></i></a>
											<a class="delete" title="Delete" data-toggle="tooltip"><i class="icon-trash"></i></a>
										</td>
									</tr>
								{/foreach}
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>

		</div>
	</div>
	{/if}

	{if isset($tinymce) && $tinymce}
	<script type="text/javascript">
		var iso = '{$iso|addslashes}';
		var pathCSS = '{$smarty.const._THEME_CSSDIR|addslashes}';
		var ad = '{$ad|addslashes}';
	
		$(document).ready(function(){
			{block name="autoload_tinyMCE"}
				tinySetup({
					editor_selector :"autoload_rte"
				});
			{/block}
		});
	</script>
	{/if}

	{if !$is_agente}
	<div class="row">
		<div class="col-lg-6">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-envelope"></i> {l s='Messages'} <span class="badge">{sizeof($customer_thread_message)}</span>
				</div>
				{if (sizeof($messages))}
					<div class="panel panel-highlighted">
						<div class="message-item">
							{foreach from=$messages item=message}
								<div class="message-avatar">
									<div class="avatar-md">
										<i class="icon-user icon-2x"></i>
									</div>
								</div>
								<div class="message-body">

									<span class="message-date">&nbsp;<i class="icon-calendar"></i>
										{dateFormat date=$message['date_add']} -
									</span>
									<h4 class="message-item-heading">
										{if ($message['elastname']|escape:'html':'UTF-8')}{$message['efirstname']|escape:'html':'UTF-8'}
											{$message['elastname']|escape:'html':'UTF-8'}{else}{$message['cfirstname']|escape:'html':'UTF-8'} {$message['clastname']|escape:'html':'UTF-8'}
										{/if}
										{if ($message['private'] == 1)}
											<span class="badge badge-info">{l s='Private'}</span>
										{/if}
									</h4>
									<p class="message-item-text">
										{$message['message']|escape:'html':'UTF-8'|nl2br}
									</p>
								</div>
								{*if ($message['is_new_for_me'])}
									<a class="new_message" title="{l s='Mark this message as \'viewed\''}" href="{$smarty.server.REQUEST_URI}&amp;token={$smarty.get.token}&amp;messageReaded={$message['id_message']}">
										<i class="icon-ok"></i>
									</a>
								{/if*}
								
							{/foreach}
						</div>
					</div>
				{/if}
				<div id="messages" class="well hidden-print">
					<form action="{$smarty.server.REQUEST_URI|escape:'html':'UTF-8'}&amp;token={$smarty.get.token|escape:'html':'UTF-8'}" method="post" onsubmit="if (getE('visibility').checked == true) return confirm('{l s='Do you want to send this message to the customer?'}');">
						<div id="message" class="form-horizontal">
							<div class="form-group">
								<label class="control-label col-lg-3">{l s='Choose a standard message'}</label>
								<div class="col-lg-9">
									<select class="chosen form-control" name="order_message" id="order_message" onchange="orderOverwriteMessage(this, '{l s='Do you want to overwrite your existing message?'}')">
										<option value="0" selected="selected">-</option>
										{foreach from=$orderMessages item=orderMessage}
										<option value="{$orderMessage['message']|escape:'html':'UTF-8'}">{$orderMessage['name']}</option>
										{/foreach}
									</select>
									<p class="help-block">
										<a href="{$link->getAdminLink('AdminOrderMessage')|escape:'html':'UTF-8'}">
											{l s='Configure predefined messages'}
											<i class="icon-external-link"></i>
										</a>
									</p>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-lg-3">{l s='Display to customer?'}</label>
								<div class="col-lg-9">
									<span class="switch prestashop-switch fixed-width-lg">
										<input type="radio" name="visibility" id="visibility_on" value="0" />
										<label for="visibility_on">
											{l s='Yes'}
										</label>
										<input type="radio" name="visibility" id="visibility_off" value="1" checked="checked" />
										<label for="visibility_off">
											{l s='No'}
										</label>
										<a class="slide-button btn"></a>
									</span>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-lg-3">{l s='Message'}</label>
								<div class="col-lg-9">
									<textarea id="txt_msg" class="textarea-autosize" name="message">{Tools::getValue('message')|escape:'html':'UTF-8'}</textarea>
									<p id="nbchars"></p>
								</div>
							</div>


							<input type="hidden" name="id_order" value="{$order->id}" />
							<input type="hidden" name="id_customer" value="{$order->id_customer}" />
							<button type="submit" id="submitMessage" class="btn btn-primary pull-right" name="submitMessage">
								{l s='Send message'}
							</button>
							<a class="btn btn-default" href="{$link->getAdminLink('AdminCustomerThreads')|escape:'html':'UTF-8'}&amp;id_order={$order->id|intval}">
								{l s='Show all messages'}
								<i class="icon-external-link"></i>
							</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	{* </div>

	<div class="row"> *}
		<div class="col-lg-6">
			<!-- Sources block -->
			{if (sizeof($sources))}
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-globe"></i>
					{l s='Sources'} <span class="badge">{$sources|@count}</span>
				</div>
				<ul {if sizeof($sources) > 3}style="height: 200px; overflow-y: scroll;"{/if}>
				{foreach from=$sources item=source}
					<li>
						{dateFormat date=$source['date_add'] full=true}<br />
						<b>{l s='From'}</b>{if $source['http_referer'] != ''}<a href="{$source['http_referer']}">{parse_url($source['http_referer'], $smarty.const.PHP_URL_HOST)|regex_replace:'/^www./':''}</a>{else}-{/if}<br />
						<b>{l s='To'}</b> <a href="http://{$source['request_uri']}">{$source['request_uri']|truncate:100:'...'}</a><br />
						{if $source['keywords']}<b>{l s='Keywords'}</b> {$source['keywords']}<br />{/if}<br />
					</li>
				{/foreach}
				</ul>
			</div>
			{/if}

			<!-- linked orders block -->
			{if count($order->getBrother()) > 0}
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-cart"></i>
					{l s='Linked orders'}
				</div>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>
									{l s='Order no. '}
								</th>
								<th>
									{l s='Status'}
								</th>
								<th>
									{l s='Amount'}
								</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							{foreach $order->getBrother() as $brother_order}
							<tr>
								<td>
									<a href="{$current_index}&amp;vieworder&amp;id_order={$brother_order->id}&amp;token={$smarty.get.token|escape:'html':'UTF-8'}">#{$brother_order->id}</a>
								</td>
								<td>
									{$brother_order->getCurrentOrderState()->name[$current_id_lang]}
								</td>
								<td>
									{displayPrice price=$brother_order->total_paid_tax_incl currency=$currency->id}
								</td>
								<td>
									<a href="{$current_index}&amp;vieworder&amp;id_order={$brother_order->id}&amp;token={$smarty.get.token|escape:'html':'UTF-8'}">
										<i class="icon-eye-open"></i>
										{l s='See the order'}
									</a>
								</td>
							</tr>
							{/foreach}
						</tbody>
					</table>
				</div>
			</div>
			{/if}
		</div>
	</div>

	{* Storico *}
	<div class="row">
		<div class="col-lg-12">
			{if (sizeof($storico))}
			<div class="panel">
				<div class="panel-heading">
					Storico <span class="badge">{$storico|@count}</span>
				</div>
				<ul {if sizeof($storico) > 3}style="height: 200px; overflow-y: scroll;"{/if}>
				{foreach from=$storico item=storia}
					<li>
						{dateFormat date=$storia['data_attivita'] full=true}<br />
						<b>{$storia['impiegato']}</b><br />
						{$storia['desc_attivita']}<br />
					</li>
				{/foreach}
				</ul>
			</div>
			{/if}
		</div>
	</div>
	{/if}

	<script type="text/javascript">
		var geocoder = new google.maps.Geocoder();
		var delivery_map, invoice_map;

		$(document).ready(function()
		{
			$(".textarea-autosize").autosize();

			geocoder.geocode({
				address: '{$addresses.delivery->address1|@addcslashes:'\''},{$addresses.delivery->postcode|@addcslashes:'\''},{$addresses.delivery->city|@addcslashes:'\''}{if isset($addresses.deliveryState->name) && $addresses.delivery->id_state},{$addresses.deliveryState->name|@addcslashes:'\''}{/if},{$addresses.delivery->country|@addcslashes:'\''}'
				}, function(results, status) {
				if (status === google.maps.GeocoderStatus.OK)
				{
					delivery_map = new google.maps.Map(document.getElementById('map-delivery-canvas'), {
						zoom: 10,
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						center: results[0].geometry.location
					});
					var delivery_marker = new google.maps.Marker({
						map: delivery_map,
						position: results[0].geometry.location,
						url: 'http://maps.google.com?q={$addresses.delivery->address1|urlencode},{$addresses.delivery->postcode|urlencode},{$addresses.delivery->city|urlencode}{if isset($addresses.deliveryState->name) && $addresses.delivery->id_state},{$addresses.deliveryState->name|urlencode}{/if},{$addresses.delivery->country|urlencode}'
					});
					google.maps.event.addListener(delivery_marker, 'click', function() {
						window.open(delivery_marker.url);
					});
				}
			});

			geocoder.geocode({
				address: '{$addresses.invoice->address1|@addcslashes:'\''},{$addresses.invoice->postcode|@addcslashes:'\''},{$addresses.invoice->city|@addcslashes:'\''}{if isset($addresses.deliveryState->name) && $addresses.invoice->id_state},{$addresses.deliveryState->name|@addcslashes:'\''}{/if},{$addresses.invoice->country|@addcslashes:'\''}'
				}, function(results, status) {
				if (status === google.maps.GeocoderStatus.OK)
				{
					invoice_map = new google.maps.Map(document.getElementById('map-invoice-canvas'), {
						zoom: 10,
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						center: results[0].geometry.location
					});
					invoice_marker = new google.maps.Marker({
						map: invoice_map,
						position: results[0].geometry.location,
						url: 'http://maps.google.com?q={$addresses.invoice->address1|urlencode},{$addresses.invoice->postcode|urlencode},{$addresses.invoice->city|urlencode}{if isset($addresses.deliveryState->name) && $addresses.invoice->id_state},{$addresses.deliveryState->name|urlencode}{/if},{$addresses.invoice->country|urlencode}'
					});
					google.maps.event.addListener(invoice_marker, 'click', function() {
						window.open(invoice_marker.url);
					});
				}
			});

			$('.datepicker').datetimepicker({
				prevText: '',
				nextText: '',
				dateFormat: 'yy-mm-dd',
				// Define a custom regional settings in order to use PrestaShop translation tools
				currentText: '{l s='Now' js=1}',
				closeText: '{l s='Done' js=1}',
				ampm: false,
				amNames: ['AM', 'A'],
				pmNames: ['PM', 'P'],
				timeFormat: 'hh:mm:ss tt',
				timeSuffix: '',
				timeOnlyTitle: '{l s='Choose Time' js=1}',
				timeText: '{l s='Time' js=1}',
				hourText: '{l s='Hour' js=1}',
				minuteText: '{l s='Minute' js=1}'
			});
		});

		// Fix wrong maps center when map is hidden
		$('#tabAddresses').click(function(){
		if (delivery_map) {
			x = delivery_map.getZoom();
			c = delivery_map.getCenter();
			google.maps.event.trigger(delivery_map, 'resize');
			delivery_map.setZoom(x);
			delivery_map.setCenter(c);
		}
      
		if (invoice_map) {
			x = invoice_map.getZoom();
			c = invoice_map.getCenter();
			google.maps.event.trigger(invoice_map, 'resize');
			invoice_map.setZoom(x);
			invoice_map.setCenter(c);
		  }
		});
	</script>

{/block}
