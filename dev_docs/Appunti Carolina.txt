APPUNTI CAROLINA

- PER DUMP DI MOLTI DATI IN PHPMYADMIN SCARICARE:
	https://www.ozerov.de/bigdump/you-need/

- Ultime attività: si trovano in ezadmin/index.php 1.4 (ancora da vedere)

- PER RISOLVERE ERRORE PER CUI L'ULTIMO ELEMENTO DI UN FOREACH CON RIFERIMENTO NON VIENE VISUALIZZATO, AGGIUNGERE DOPO:
	unset($row); // breaks the reference with the last element of the foreach loop

- MODULI: Se blocklayered 1.6 non funziona: Change blocklayered folder permissions to 755 and all file permissions to 644.
https://stackoverflow.com/questions/39664407/prestashop-blocklayered-show-wrong-stock-available-products

- Nel front, se gli URL con filtri fanno redirect automatico a pagina senza filtri, bisogna ricostruire indice URL. 
	Andare in Moduli -> dal blocco ricerca scegliere "blocco di navigazione a strati" (o anche "layered" che è la stessa cosa), cliccare su configura, cliccare su "Costruire indice url", aspettare un minuto che finisce.

- Modificare l'ordinamento di una list:
usare $this->_orderBy e $this->_orderWay nella funzione renderList del controller (sovrascrivono l'ordinamento per ID)
	Esempio: $this->_orderBy = 'a.lastname'; $this->_orderWay = 'ASC';

- Modificare l'ordinamento di default in un controller:
definire le variabili protected $_defaultOrderBy e $_defaultOrderWay subito dopo l'apertura della classe (prima delle funzioni)
	Esempio: protected $_defaultOrderBy = 'c!data_fine'; protected $_defaultOrderWay = 'DESC';

- non usare getRow come return delle funzioni (errore 500), ma solo executeS
- usare _PS_USE_SQL_SLAVE solo per SELECT e non immediatamente dopo operazioni di scrittura
- executeS() andrebbe usata solo con SELECT; per INSERT, UPDATE, DELETE usare execute()
- Non usare NumRows() (verrà deprecata); fare anzi SELECT COUNT(*)
- Insert_ID(): returns the ID created during the latest INSERT query
- DB 1.6 BEST PRACTICES: https://doc.prestashop.com/pages/viewpage.action?pageId=51184692

- Per creare una list secondaria in un controller, vedi AdminGroupsController (lista di customers)

- Usare literal nel js inserito nel tpl se si usano le parentesi graffe per una corretta interpretazione da parte di smarty

- addJS() non funziona in __costruct()

$this->fields_list:
- ['position'] => 'position'   // if set to position the field will display arrows and be drag and dropable, which will update position in db
- 'filter_type' => 'string'    // invece che avere 'campo' = value nella where, ci sarà LIKE '%value%'
- 'remove_onclick' => true     // rimuove azione onclick sulla riga


- Se in un controller la query principale di una list ha group by / having / limit ma voglio calcolare nel panel-header il numero totale di risultati uso:
	$this->_use_found_rows = true;

- Aggiunti in defines.inc.php: 
	- _PS_BO_DEFAULT_THEME_DIR_ = percorso del tema BO default (ezdirect/themes/default/)
	- _PS_BO_DEFAULT_THEME_JS_DIR_ = percorso della cartella js del tema BO default (ezdirect/themes/default/js)

- Creata classe BDL

Alcune modifiche database da controllare:
- ps_order_detail:
	aggiunto wholesale_price (ma c'era già original_wholesale_price, lo sostituisce o no?)
	product_quantity, product_price, product_reference e product_supplier_reference devono diventare key

- ps_orders:
	data_ordine_mepa: messo default null perchè nuova versione mysql non permette il formato '0000-00-00'; andrebbe fatto anche con tutte le altre date 

------------------------------------------------------------------------------------------------------------------

Ricordarsi di controllare che non ci siano modifiche nei miei file locali in htdocs che non sono state riportate in github; per ora userò direttamente la seconda cartella.

------------------------------------------------------------------------------------------------------------------

Modificato nel db 1.6 (ma non 1.4): 

- ps_address_format riga 10 (Italia): 
firstname lastname
company
c_o
vat_number
address1
address2
suburb
Country:name
postcode
city
State:name
phone
phone_mobile
fax

- customer_amministrazione:
	- blacklist varchar -> int

------------------------------------------------------------------------------------------------------------------

ps_customer: cambia l'ordine di alcuni campi (quindi nell'import/inserimento specificare sempre il nome del campo); risk diventa id_risk; company varchar(400);

creata tabella customer_amministrazione; alcune delle tabelle aggiunte da noi non hanno prefisso ps_

- Aggiungere chiavi e indici! 
-> Per ora aggiunto index su customer_thread.id_employee

ps_cart: aggiunti preventivo, convertito e visualizzato (trasformati da int a tinyint, a parte visualizzato); il campo convertito non esisteva nel db 1.4, che controlla invece se il carrello si trova in ps_order (aggiunto per comodità ma ridondante, visto che comunque la query seleziona anche id_order); 06/11: aggiunti created_by, in_carico_a, name (ordine diverso dalla 1.4); 12/11: aggiunti tutti i campi mancanti tranne le date (errore sql, mancano: 
	`validita` datetime NOT NULL,
	`data_ultima_notifica` datetime NOT NULL,
	`data_ordine_mepa` date NOT NULL,
  	`competenza_dal` date NOT NULL,
  	`competenza_al` date NOT NULL
)

ps_employee: valore di default non valido per last_connection_date ???

ps_configuration: PS_B2B_ENABLE = 1
ps_configuration: PS_COUNTRY_DEFAULT = 10    // Italia

I campi 1.4:
- ps_product_download.date_deposit
- ps_product_download.physically_filename
nella 1.6 si chiamano date_add e filename

- Per permettere di aggiungere date con value '0000-00-00' scrivere prima dell'operazione in sql: 
	SET SQL_MODE='ALLOW_INVALID_DATES';

------------------------------------------------------------------------------------------------------------------

- Il type "date" usa Tools::displayDate($data) cioè Tools::displayDate($data, null, false) -> senza orario (o meglio, come definito in ps_lang.date_format_lite)
- Il type "datetime" usa Tools::displayDate($data, null, true) -> con orario (ps_lang.date_format_full)


Stampare messaggi (1.6):
- Error (rosso): 
	1. $this->errors[] = 'MESSAGGIO';
	2. $this->errors[] = Tools::displayError('MESSAGGIO');
- Warning (arancione): 
	1. $this->warnings[] = 'MESSAGGIO';
	2. $this->displayWarning('MESSAGGIO');
- Success (verde): 
	1. $this->confirmations[] = 'MESSAGGIO';
- Information (blu):
	1. $this->informations[] = 'MESSAGGIO';
	2. $this->displayInformation('MESSAGGIO');

------------------------------------------------------------------------------------------------------------------
IN THEMES/EZDIRECT (FRONT OFFICE):

function cercaCitta() {
	var this_cap = $('#cap').val();
	$.ajax({
	  url:"https://www.ezdirect.it/cap.php?cercacitta=y",
	  type: "POST",
	  data: { 
	  cap: this_cap
	  },
	  success:function(r){
		 if(r != '')
		 {
			$( "#citta" ).html( r );
			console.log(r);
			cercaProvincia();
		 }	
	  },
	  error: function(xhr,stato,errori){
	 alert("Errore:"+xhr.status);
	  }
	});
}

JS DA IMPLEMENTARE DOVE NECESSARIO:
// Funzione per mantenere header tabella visibile in alto dopo scroll
<script type='text/javascript'>
	$(document).ready(function() {
		function moveScroll(){
			var scroll = $(window).scrollTop();
			var anchor_top = $('#tablePrezzi').offset().top;
			var anchor_bottom = $('#bottom_anchor').offset().top;
			if (scroll>anchor_top && scroll<anchor_bottom) {
			clone_table = $('#clone');
			if(clone_table.length == 0){
				clone_table = $('#tablePrezzi').clone();
				clone_table.attr('id', 'clone');
				clone_table.css({position:'fixed',
							'pointer-events': 'none',
							top:0});
				clone_table.width($('#tablePrezzi').width());
				$('#table-container').append(clone_table);
				$('#clone').css({visibility:'hidden'});
				$('#clone thead').css({'visibility':'visible','pointer-events':'auto'});
			}
			} else {
			$('#clone').remove();
			}
		}
		$(window).scroll(moveScroll); 
	});
</script>

----------------------------------------------------------------------------------------------

Tab 1.4 inutilizzate da tenere (riportare nella 1.6 i file):
Orders -> messaggi ordine (AdminOrderMessage), ordini non processati, ordini non conclusi
Customers -> controllo pagamenti
Products -> coupon cheapnet, mappatura immagine (AdminScenes), movimenti magazzino (AdminStockMvt)

----------------------------------------------------------------------------------------------

Tabelle inutilizzate: cosa fare?
amazon_verifica_pagamenti -> controllare se e dove viene utilizzata