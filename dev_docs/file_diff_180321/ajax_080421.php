<?php
/*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_ADMIN_DIR_')) {
    define('_PS_ADMIN_DIR_', getcwd());
}
include(_PS_ADMIN_DIR_.'/../config/config.inc.php');

/* Getting cookie or logout */
require_once(_PS_ADMIN_DIR_.'/init.php');

$context = Context::getContext();

if (Tools::isSubmit('ajaxReferrers')) {
    require(_PS_CONTROLLER_DIR_.'admin/AdminReferrersController.php');
}

if (Tools::getValue('page') == 'prestastore' and @fsockopen('addons.prestashop.com', 80, $errno, $errst, 3)) {
    readfile('http://addons.prestashop.com/adminmodules.php?lang='.$context->language->iso_code);
}

if (Tools::isSubmit('getAvailableFields') and Tools::isSubmit('entity')) {
    $jsonArray = array();
    $import = new AdminImportController();

    $fields = $import->getAvailableFields(true);
    foreach ($fields as $field) {
        $jsonArray[] = '{"field":"'.addslashes($field).'"}';
    }
    die('['.implode(',', $jsonArray).']');
}

if (Tools::isSubmit('ajaxProductPackItems')) {
    $jsonArray = array();
    $products = Db::getInstance()->executeS('
	SELECT p.`id_product`, pl.`name`
	FROM `'._DB_PREFIX_.'product` p
	NATURAL LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
	WHERE pl.`id_lang` = '.(int)(Tools::getValue('id_lang')).'
	'.Shop::addSqlRestrictionOnLang('pl').'
	AND NOT EXISTS (SELECT 1 FROM `'._DB_PREFIX_.'pack` WHERE `id_product_pack` = p.`id_product`)
	AND p.`id_product` != '.(int)(Tools::getValue('id_product')));

    foreach ($products as $packItem) {
        $jsonArray[] = '{"value": "'.(int)($packItem['id_product']).'-'.addslashes($packItem['name']).'", "text":"'.(int)($packItem['id_product']).' - '.addslashes($packItem['name']).'"}';
    }
    die('['.implode(',', $jsonArray).']');
}

if (Tools::isSubmit('getChildrenCategories') && Tools::isSubmit('id_category_parent')) {
    $children_categories = Category::getChildrenWithNbSelectedSubCat(Tools::getValue('id_category_parent'), Tools::getValue('selectedCat'), Context::getContext()->language->id, null, Tools::getValue('use_shop_context'));
    die(Tools::jsonEncode($children_categories));
}

if (Tools::isSubmit('getNotifications')) {
    $notification = new Notification;
    die(Tools::jsonEncode($notification->getLastElements()));
}

if (Tools::isSubmit('updateElementEmployee') && Tools::getValue('updateElementEmployeeType')) {
    $notification = new Notification;
    die($notification->updateEmployeeLastElement(Tools::getValue('updateElementEmployeeType')));
}

if (Tools::isSubmit('searchCategory')) {
    $q = Tools::getValue('q');
    $limit = Tools::getValue('limit');
    $results = Db::getInstance()->executeS('SELECT c.`id_category`, cl.`name`
		FROM `'._DB_PREFIX_.'category` c
		LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (c.`id_category` = cl.`id_category`'.Shop::addSqlRestrictionOnLang('cl').')
		WHERE cl.`id_lang` = '.(int)$context->language->id.' AND c.`level_depth` <> 0
		AND cl.`name` LIKE \'%'.pSQL($q).'%\'
		GROUP BY c.id_category
		ORDER BY c.`position`
		LIMIT '.(int)$limit);
    if ($results) {
        foreach ($results as $result) {
            echo trim($result['name']).'|'.(int)$result['id_category']."\n";
        }
    }
}

if (Tools::isSubmit('getParentCategoriesId') && $id_category = Tools::getValue('id_category')) {
    $category = new Category((int)$id_category);
    $results = Db::getInstance()->executeS('SELECT `id_category` FROM `'._DB_PREFIX_.'category` c WHERE c.`nleft` < '.(int)$category->nleft.' AND c.`nright` > '.(int)$category->nright.'');
    $output = array();
    foreach ($results as $result) {
        $output[] = $result;
    }

    die(Tools::jsonEncode($output));
}

if (Tools::isSubmit('getZones')) {
    $html = '<select id="zone_to_affect" name="zone_to_affect">';
    foreach (Zone::getZones() as $z) {
        $html .= '<option value="'.$z['id_zone'].'">'.$z['name'].'</option>';
    }
    $html .= '</select>';
    $array = array('hasError' => false, 'errors' => '', 'data' => $html);
    die(Tools::jsonEncode($array));
}

if (Tools::isSubmit('getEmailHTML') && $email = Tools::getValue('email')) {
    $email_html = AdminTranslationsController::getEmailHTML($email);
    die($email_html);
}

/** \/ OVERRIDE TESTATI \/ **/

if (Tools::isSubmit('searchCustomerById')) {
    $id_customer = Tools::getValue('id_customer');
    if( str_replace(' ', '', $id_customer) != "" ){
        $customer = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'customer WHERE id_customer = '.Tools::getValue('id_customer'));
        if(!$customer)
            die('Nessun cliente trovato');
        else
        {
            if($customer['is_company'] == 1)
                die($customer['company']);
            else
                die($customer['firstname'].' '.$customer['lastname']);
        }
    } else {
        die('');
    }
    
}

if (Tools::isSubmit('aggiornaPeriodoForecast'))
{
    // Non decommentare, chiedere a Federico
	/*Db::getInstance()->executeS('DELETE FROM forecast WHERE id_product = '.Tools::getValue('id_product'));
	Db::getInstance()->executeS('INSERT INTO forecast (id_product, periodo) VALUES ("'.Tools::getValue('id_product').'", "'.Tools::getValue('periodo').'")');*/
	
	Db::getInstance()->executeS('DELETE FROM forecast WHERE id_product = 11111');
	Db::getInstance()->executeS('INSERT INTO forecast (id_product, periodo) VALUES ("11111", "'.Tools::getValue('periodo').'")');
	
	die('ok');
}


/** \/ OVERRIDE NON TESTATI \/ **/

if (Tools::getIsset('changeDDT')){
	$ddt = Tools::getValue('changeDDT');
	
	Db::getInstance()->executeS('UPDATE '._DB_PREFIX_.'order_detail SET ddt = "'.$ddt.'" WHERE product_id = '.$_POST['id_product'].' AND id_order = '.$_POST['id_order']);
	die('ok');
}

if (Tools::getIsset('readMail'))
{
	$connection = mysqli_connect($_POST['database_server'], $_POST['database_user'], $_POST['database_pass']);
	mysqli_select_db($connection,$_POST['database']);
	
	$result = mysqli_query($connection,'SELECT * FROM mail WHERE id = '.$_POST['id']);
	$email = mysqli_fetch_array($result);
	//$email = Db::getInstance()->getRow('SELECT * FROM mail WHERE id = '.$_POST['id']); // eliminare

	$allegati_string = '';
	
	if($email['numero_allegati'] > 0)
	{
		$allegati = unserialize($email['allegati']);
		foreach ($allegati as $allegato)
		{
			$allegati_html .= '<img src="../img/admin/import.gif" alt="Allegato" title="Allegato" /> <a target="_blank" href="http://uploads:asdf6Hsrbxh!64@www.ezdirectftp.it/um/'.$email['id'].'/'.$allegato.'">'.$allegato.'</a> - ';
			
		}

		$allegati_string = '<tr><th>Allegati</th><td>'.$allegati_html.'</td></tr>';
	}

	$email_table = '<table class="table" style="width:100%">';
	$email_table .= '<tr><th style="width:100px">Da</th><td>'.htmlentities($email['da_intestazione']).' <a href="mailto:'.$email['da'].'"><img src="../img/admin/outlook.gif" alt="Invia una mail" title="Invia una mail" /></a> </td></tr>';
	$email_table .= '<tr><th>A</th><td>'.htmlentities($email['a_intestazione']).'</td></tr>';
	$email_table .= '<tr><th>Oggetto</th><td>'.$email['oggetto'].'</td></tr>';
	$email_table .= '<tr><th>Data</th><td>'.date('d/m/Y H:i:s', strtotime($email['data'])).'</td></tr>';
	$email_table .= $allegati_string;
	$email_table .= '<tr><td colspan="2"><div style="max-width:600px">'.str_replace('<style', '<style scoped ',$email['messaggio']).'</div></td></tr>';
	$email_table .= '</table><script type="text/javascript" src="https://www.ezdirect.it/js/jquery/scoper.min.js"></script>';
		
	$mailHTML = '';
	$mailHTML .= utf8_encode('<br /><br />'.$email_table);

	die($mailHTML);
}

if(Tools::getIsset('cancellanota_customer') && Tools::getValue('cancellanota_customer') == 'y') {
	Db::getInstance()->executeS("DELETE FROM customer_note WHERE id_note = ".Tools::getValue('id_note'));
}

if(Tools::getIsset('creanota_customer') && Tools::getValue('creanota_customer') == 'y') {
	Db::getInstance()->execute("
                                INSERT INTO customer_note (id_note, id_customer, id_employee, note, date_add, date_upd)
                                VALUES (
                                    NULL,
                                    '".Tools::getValue('id_customer')."',
                                    '".$context->employee->id."',
                                    '".addslashes(Tools::getValue('nota'))."',
                                    '".date("Y-m-d H:i:s")."',
                                    '".date("Y-m-d H:i:s")."'
                                )
                            ");

	print Db::getInstance()->Insert_ID();
}

if(Tools::getIsset('modificanota_customer') && Tools::getValue('modificanota_customer') == 'y') {
	Db::getInstance()->execute("
                                UPDATE customer_note 
                                SET note = '".addslashes(Tools::getValue('nota'))."', date_upd = '".date("Y-m-d H:i:s")."', id_employee = '".$context->employee->id."' 
                                WHERE id_note = ".Tools::getValue('id_nota')
							); 
}


if (Tools::isSubmit('aggiorna_dati_cliente'))
{
	if(Tools::getValue('tipo') == 'agente') {
		$total = Db::getInstance()->getValue('SELECT count(id_customer) FROM customer_amministrazione WHERE id_customer = '.Tools::getValue('cliente'));
		
		if($total == 0) 
			Db::getInstance()->execute('INSERT INTO customer_amministrazione (id_customer, agente) VALUES ('.Tools::getValue('cliente').', '.Tools::getValue('valore').')');
		else
			Db::getInstance()->execute('UPDATE customer_amministrazione SET agente = '.Tools::getValue('valore').' WHERE id_customer = '.Tools::getValue('cliente'));
		
		die('Agente');
	}
	
	if(Tools::getValue('tipo') == 'tecnico') {
		$total = Db::getInstance()->getValue('SELECT count(id_customer) FROM customer_amministrazione WHERE id_customer = '.Tools::getValue('cliente'));
		
		if($total == 0) 
			Db::getInstance()->execute('INSERT INTO customer_amministrazione (id_customer, tecnico) VALUES ('.Tools::getValue('cliente').', '.Tools::getValue('valore').')');
		else
			Db::getInstance()->execute('UPDATE customer_amministrazione SET tecnico = '.Tools::getValue('valore').' WHERE id_customer = '.Tools::getValue('cliente'));
			
		die('Tecnico');
	}
	
	if(Tools::getValue('tipo') == 'referente') {
		Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'customer SET referente = '.Tools::getValue('valore').' WHERE id_customer = '.Tools::getValue('cliente'));
		
		die('Referente');
	}
}

/** \/ OVERRIDE DI FEDERICO \/ */


if (Tools::isSubmit('submitOre'))
{
	$dati_stringa = Tools::getValue('dati_da_salvare');
	
	$dati_stringa = preg_replace('/undefined/', '0', $dati_stringa);
	
	$dati = explode(':::',$dati_stringa);
	foreach($dati as $dato_stringa)
	{
		$dato = explode('|',$dato_stringa);
		
		$id_employee = $dato[0];
		$month = $dato[1];
		$year = $dato[2];
		$exists = Db::getInstance()->getValue('SELECT count(valori_ore) FROM ore WHERE id_employee = '.$id_employee.' AND month = '.$month.' AND year = '.$year.'');
		if($exists > 0)
			Db::getInstance()->executeS('UPDATE ore SET valori_ore = "'.$dato[3].'" WHERE id_employee = '.$id_employee.' AND month = '.$month.' AND year = '.$year.'');
		else
			Db::getInstance()->executeS('INSERT INTO ore (id_employee, month, year, valori_ore) VALUES ('.$id_employee.', '.$month.', '.$year.', "'.$dato[3].'")');
		
	}
	
	
	die('ok');
}


if(Tools::getIsset('esporta_ore_excel')) { 
	ini_set("memory_limit","892M");
	set_time_limit(3600);
	require_once 'esportazione-catalogo/Classes/PHPExcel.php';
	$objPHPExcel = new PHPExcel();
				
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', Tools::getValue('month_name_ore_xls').' '.Tools::getValue('year_ore_xls'));
	$employees = Db::getInstance()->ExecuteS('
				SELECT `id_employee`, CONCAT(`firstname`, \' \', `lastname`) AS "name", lastname, firstname
				FROM `'._DB_PREFIX_.'employee`
				WHERE id_employee != 6 AND id_employee != 11 AND id_employee != 15  AND id_employee != 16 AND   id_employee != 20 AND id_employee != 3 AND id_employee != 18  AND id_employee != 21 AND id_employee != 23 AND id_employee != 25
			ORDER BY (CASE id_employee WHEN 1 THEN 0 WHEN 2 THEN 1 WHEN 7 THEN 2 WHEN 4 THEN 3 WHEN 3 THEN 4 WHEN 5 THEN 6 WHEN 13 THEN 7 WHEN 12 THEN 8 WHEN 14 THEN 9 WHEN 17 THEN 10 WHEN 18 THEN 11 WHEN 19 THEN 12 WHEN 22 THEN 13 WHEN 24 THEN 14 WHEN 26 THEN 15 END)');
	$num = 2;			
	
	$days_number = cal_days_in_month(CAL_GREGORIAN, Tools::getValue('month_ore_xls'), Tools::getValue('year_ore_xls'));
	$last_letter = '';
	
	if($days_number == 28)
	{
		$last_letter = 'AC';
		$columns = createColumnsArray($last_letter);
	}	
	else if($days_number == 29)
	{
		$last_letter = 'AD';
		$columns = createColumnsArray($last_letter);
	}
	else if($days_number == 30)
	{
		$last_letter = 'AE';
		$columns = createColumnsArray($last_letter);	
	}
	else 
	{
		$last_letter = 'AF';
		$columns = createColumnsArray($last_letter);	
	}
	foreach($employees as $employee)
	{
		if($employee['id_employee'] == 6 || (Tools::getValue('month_ore_xls') > 9 && Tools::getValue('year_ore_xls') >= 2018 && ($employee['id_employee'] == 3 || $employee['id_employee'] == 18)))
		{
		}
		else
		{
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue("A$num", $employee['lastname'].' '.$employee['firstname']);
			
			$valori_ore = Db::getInstance()->getValue('SELECT valori_ore FROM ore WHERE id_employee = '.$employee['id_employee'].' AND month = '.Tools::getValue('month_ore_xls').' AND year = '.Tools::getValue('year_ore_xls').'');
							
			$valori = explode("*",$valori_ore);
			$valori_i = 0;
			foreach(array_slice($columns, 1) as $column)
			{
				if($valori[$valori_i] == 0 && is_numeric($valori[$valori_i]))
					$valori[$valori_i] = '';
				
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue($column.$num, $valori[$valori_i]);
				$objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth(5);
				
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue($column."1", $valori_i+1);
				
				$objPHPExcel->getActiveSheet()->getStyle($column.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
				$valori_i++;
			}
		
			$num++;
		}
	}
				
	$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(30);
	
	$objPHPExcel->getActiveSheet()->getStyle("A")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				
	$objPHPExcel->getActiveSheet()->setTitle('Ore Ezdirect '.Tools::getValue('month_name_ore_xls').' '.Tools::getValue('year_ore_xls'));

	$objPHPExcel->getActiveSheet()->getStyle('A1:'.$last_letter.'1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	

	$objPHPExcel->getActiveSheet()->getStyle('A1:'.$last_letter.'1')->getFill()->getStartColor()->setRGB('FFFF00');	

	$objPHPExcel->getActiveSheet()->getStyle("A1:".$last_letter."$num")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				
	$objPHPExcel->getActiveSheet()->getStyle('A1:'.$last_letter.'1')->getFont()->setBold(true);
				
	$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
	$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
	PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

	$data = date("Ymd");

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->setPreCalculateFormulas(false);
			
	$filename = 'ore-ezdirect-'.Tools::getValue('month_name_ore_xls').'-'.Tools::getValue('year_ore_xls').'.xls';
	ob_end_clean();
	header('Content-type: application/ms-excel');
	header('Content-Disposition: attachment; filename='.$filename);
	$objWriter->save('php://output');
	return true;	
}