<?php
/*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class BoOrder extends PaymentModule
{
    public $active = 1;
    public $name = 'bo_order';

    public function __construct()
    {
        $this->displayName = $this->l('Back office order');
    }
}

/**
 * @property Order $object
 */
class AdminOrdersControllerCore extends AdminController
{
    public $toolbar_title;

    protected $statuses_array = array();

    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'order';
        $this->className = 'Order';
        $this->lang = false;
        $this->addRowAction('view');
        $this->addRowAction('delete');
        $this->explicitSelect = true;
        $this->allow_export = true;
        $this->deleted = false;
        $this->context = Context::getContext();

        // Override
		$modalita_esolver = Db::getInstance()->getValue('SELECT value FROM '._DB_PREFIX_.'configuration WHERE name = "MODALITA_ESOLVER"');

        $this->_select = '
            a.id_currency,
            a.id_order AS id_pdf,
            CONCAT(LEFT(c.`firstname`, 1), \'. \', c.`lastname`) AS `customer`,
            osl.`name` AS `osname`,
            os.`color`,
            IF((SELECT so.id_order FROM `'._DB_PREFIX_.'orders` so WHERE so.id_customer = a.id_customer AND so.id_order < a.id_order LIMIT 1) > 0, 0, 1) as new,
            country_lang.name as cname,
            IF(a.valid, 1, 0) badge_success
        ';

        // Override
        $this->_select .= '
            ,

            (CASE WHEN a.id_cart IN (SELECT id_cart FROM `'._DB_PREFIX_.'cart` WHERE name LIKE "%AMAZON PRIME%") THEN "Prime"
			ELSE a.payment
			END
            ) AS paymentz,

            (CASE c.is_company
			WHEN 0
			THEN CONCAT(c.firstname," ",c.lastname)
			WHEN 1
			THEN c.company
			ELSE
			CONCAT(c.firstname," ",c.lastname)
			END
			) AS cliente,

            (CASE WHEN ca.preventivo = 0
			THEN "--"
			WHEN ca.preventivo = ""
			THEN "--"
			WHEN ca.preventivo IS NULL
			THEN "--"
			ELSE
			a.id_cart
			END
            ) AS `preventivo`
        ';

        $this->_join = '
            LEFT JOIN `'._DB_PREFIX_.'customer` c ON (c.`id_customer` = a.`id_customer`)
            LEFT JOIN `'._DB_PREFIX_.'address` address ON address.id_address = a.id_address_delivery
            LEFT JOIN `'._DB_PREFIX_.'country` country ON address.id_country = country.id_country
            LEFT JOIN `'._DB_PREFIX_.'country_lang` country_lang ON (country.`id_country` = country_lang.`id_country` AND country_lang.`id_lang` = '.(int)$this->context->language->id.')
            LEFT JOIN `'._DB_PREFIX_.'order_state` os ON (os.`id_order_state` = a.`current_state`)
            LEFT JOIN `'._DB_PREFIX_.'order_state_lang` osl ON (os.`id_order_state` = osl.`id_order_state` AND osl.`id_lang` = '.(int)$this->context->language->id.')
        ';

        /* OVERRIDE */

        $this->_join .= '
            LEFT JOIN `'._DB_PREFIX_.'cart` ca ON (ca.`id_cart` = a.`id_cart`)
        ';

        // Serve per i filtri di ricerca
        $this->_join .= '
            LEFT JOIN `'._DB_PREFIX_.'order_history` oh ON (oh.`id_order` = a.`id_order`)
        ';

        $this->_group = 'GROUP BY a.id_order';

        /* FINE OVERRIDE */

        $this->_orderBy = 'id_order';
        $this->_orderWay = 'DESC';
        $this->_use_found_rows = true;

        $statuses = OrderState::getOrderStates((int)$this->context->language->id);
        foreach ($statuses as $status) {
            $this->statuses_array[$status['id_order_state']] = $status['name'];
        }

        $this->fields_list = array(
            'id_order' => array(
                'title' => $this->l('ID'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs'
            ), // togliere reference; esisteva nella 1.4?
            /*'reference' => array(
                'title' => $this->l('Reference')
            ),*/
            'preventivo' => array(
                'title' => $this->l('N. PREV.'),
                'tmpTableFilter' => true,
            ),
            'new' => array(
                'title' => $this->l('New client'),
                'align' => 'text-center',
                'type' => 'bool',
                'tmpTableFilter' => true,
                'orderby' => false,
                'callback' => 'printNewCustomer'
            ),
            /*'customer' => array(
                'title' => $this->l('Customer'),
                'havingFilter' => true,
            ),*/
            'cliente' => array(
                'title' => $this->l('Customer'),
                //'havingFilter' => true,
                'tmpTableFilter' => true,
            ),
            'id_default_group' => array(
                'title' => $this->l('Group'),
            ),
        );

        /*if (Configuration::get('PS_B2B_ENABLE')) {
            $this->fields_list = array_merge($this->fields_list, array(
                'company' => array(
                    'title' => $this->l('Company'),
                    'filter_key' => 'c!company'
                ),
            ));
        }*/

        $this->fields_list = array_merge($this->fields_list, array(
            // correggere valore
            'total_paid_tax_excl' => array(
                'title' => $this->l('Net Total'),
                'align' => 'text-right',
                'type' => 'price',
                'currency' => true,
                'callback' => 'setOrderCurrency',
                //'badge_success' => true
            ),
            'total_paid_tax_incl' => array(
                'title' => $this->l('Total'),
                'align' => 'text-right',
                'type' => 'price',
                'currency' => true,
                'callback' => 'setOrderCurrency',
                //'badge_success' => true
            ),/*
            'payment' => array(
                'title' => $this->l('Payment')
            ),*/
            'paymentz' => array(
                'title' => $this->l('Payment'),
                'tmpTableFilter' => true,
            ),
            'osname' => array(
                'title' => $this->l('Status'),
                'type' => 'select',
                'color' => 'color',
                'list' => $this->statuses_array,
                'filter_key' => 'os!id_order_state',
                'filter_type' => 'int',
                'order_key' => 'osname'
            ),
            'date_add' => array(
                'title' => $this->l('Date'),
                'align' => 'text-right',
                'type' => 'datetime',
                'filter_key' => 'a!date_add'
            ),/*
            'id_pdf' => array(
                'title' => $this->l('PDF'),
                'align' => 'text-center',
                'callback' => 'printPDFIcons',
                'orderby' => false,
                'search' => false,
                'remove_onclick' => true
            ),*/
        ));

        /* OVERRIDE */

        $permessi_pdf = (($this->context->employee->id != 2 && $this->context->employee->id != 7 && $this->context->employee->id != 12 && $this->context->employee->id != 14 && $this->context->employee->id != 19) ? true : false); // false: Barbara, Matteo, Lorenzo, Valentina, Leila
        $permessi_pag_amz = (($this->context->employee->id == 2 || $this->context->employee->id == 14) ? true : false); // true: Barbara, Valentina
        $permessi_esolver = (($this->context->employee->id == 2 || $this->context->employee->id == 14) ? true : false); // true: Barbara, Valentina
        $permessi_pi = (($this->context->employee->id == 2 || $this->context->employee->id == 7 || $this->context->employee->id == 14 || $this->context->employee->id == 19) ? true : false); // true: Barbara, Matteo, Valentina, Leila

        if($permessi_pdf) {
            $this->fields_list = array_merge($this->fields_list, array(
                'id_pdf' => array(
                    'title' => $this->l('PDF'),
                    'align' => 'text-center',
                    'callback' => 'printPDFIcons',
                    'orderby' => false,
                    'search' => false,
                    'remove_onclick' => true
                )
            ));
        }
        // CHECKBOX CON AZIONE IN AJAX, COME FARE?
        /*
		else { // eliminare?
            $this->fields_list = array_merge($this->fields_list, array(
                'spring' => array(
                    'title' => $this->l('Verificato'),
                    'align' => 'text-center',
                    'orderby' => false,
                    'search' => false,
                )
            ));
        } 

        // CHECKBOX CON AZIONE IN AJAX, COME FARE?
        if($permessi_pag_amz) {
            $this->fields_list = array_merge($this->fields_list, array(
                'pag_amz_ric' => array(
                    'title' => $this->l('Pag.Amz.'),
                    'align' => 'text-center',
                    'orderby' => false,
                    'search' => false,
                )
            ));
        }

        // CHECKBOX CON AZIONE IN AJAX, COME FARE?
        if($permessi_pi) {
            $this->fields_list = array_merge($this->fields_list, array(
                'check_pi' => array(
                    'title' => $this->l('P.I.'),
                    'align' => 'text-center',
                    'orderby' => false,
                    'search' => false,
                )
            ));
        }

        // CHECKBOX CON AZIONE IN AJAX, COME FARE?
        if($permessi_esolver) {
            $this->fields_list = array_merge($this->fields_list, array(
                'check_esolver' => array(
                    'title' => $this->l('eSolv.'),
                    'align' => 'text-center',
                    'orderby' => false,
                    'search' => false,
                )
            ));
        }

        // vedi colori righe e checkbox in displayListContent 1.4

        */

        // CORREGGERE: fare anzi in renderList? Perchè uso cookie?
        // Ricerca per prodotto
        if(Tools::getIsset('cercaordineprodotto')) {
			//$cookie->_whereorder = 'AND a.id_order IN (SELECT id_order FROM order_detail WHERE product_id = "'.$_POST['cercaordineprodotto'].'") AND oh.`id_order_history` = (SELECT MAX(`id_order_history`) FROM `'._DB_PREFIX_.'order_history` moh WHERE moh.`id_order` = a.`id_order` AND (moh.id_order_state != 27 AND moh.id_order_state != 28) GROUP BY moh.`id_order`)';
			//$cookie->_cercaordineprodotto = $_POST['cercaordineprodotto'];
		}

		if(Tools::getIsset('cercaordineprodotto') || Tools::getIsset('cercatestoprodottoordine') || Tools::getIsset('online')  || Tools::getIsset('offline')  || Tools::getIsset('old') || Tools::getIsset('auto_marca') || Tools::getIsset('auto_serie') || Tools::getIsset('auto_categoria') || Tools::getIsset('auto_fornitore')) {
            // vedi AdminOrders 1.4; non vedo il form di ricerca online-offline-etc, è stato tolto?
        }

        // DA CORREGGERE!!!
        /*
        if(isset($cookie->_whereorder))
			$this->_where = $cookie->_whereorder;
		else
			$this->_where = 'AND ca.riferimento NOT LIKE "%BDL%" AND oh.`id_order_history` = (SELECT MAX(`id_order_history`) FROM `'._DB_PREFIX_.'order_history` moh WHERE moh.`id_order` = a.`id_order` AND ( moh.id_order_state != 27 AND moh.id_order_state != 28) GROUP BY moh.`id_order`)';
		
        // Filtra ordini

        // TOLTO (... && Tools::getValue('...') == 'yes')

        // Ordini aperti
        // 1.4: href a Ordini Non Processati -> trasformare in filtro
        if(Tools::getIsset('aperti'))
		    $this->_where .= 'AND oh.`id_order_history` = (SELECT MAX(`id_order_history`) FROM `'._DB_PREFIX_.'order_history` moh WHERE moh.`id_order` = a.`id_order` AND moh.id_order NOT IN (SELECT id_order FROM `'._DB_PREFIX_.'order_history` moh WHERE moh.id_order_state = 4 OR moh.id_order_state = 5 OR moh.id_order_state = 6 OR moh.id_order_state = 7 OR moh.id_order_state = 8 OR moh.id_order_state = 14 OR moh.id_order_state = 16 OR moh.id_order_state = 17 OR moh.id_order_state = 20  OR moh.id_order_state = 35) GROUP BY moh.`id_order`)';
        
        // Ordini aperti EXCEL: NON FUNZIONA NELLA 1.4! Da vedere / eliminare!
        // pulsante con href="ordini_non_evasi_xls.php"

        // Ordini verificati (?) (non c'entra niente con codice_spring di customer)
        else if(Tools::getIsset('spring') && Tools::getValue('spring') == 'yes')
			$this->_where .= 'AND a.spring = 0';
		
		// Ordini con assistenza
        else if(Tools::getIsset('astec'))
			$this->_where .= 'AND a.id_order IN (SELECT id_order FROM '._DB_PREFIX_.'order_detail WHERE product_reference LIKE "%inst%" OR product_reference LIKE "%astec%"  OR product_reference LIKE "%teleg%")';
		
        // Ordini con assistenza CON TODO APERTO
		else if(Tools::getIsset('astec_aperti'))
			$this->_where .= 'AND (a.id_order) IN (SELECT substr(riferimento,2) FROM action_thread WHERE subject LIKE "%*** ACQUISTO ORDINE CON ASSISTENZA TECNICA ***%" AND status != "closed")';
		
        // Ordini validi
		else if(Tools::getIsset('valid'))
			$this->_where .= 'AND (a.id_order IN (SELECT id_order FROM '._DB_PREFIX_.'order_history WHERE id_order_state = 4 OR id_order_state = 5 OR id_order_state = 15 OR id_order_state = 14 OR id_order_state = 16 OR id_order_state = 20))';
		
        // Eliminare?
		// else if(Tools::getIsset('spring')')
			// $this->_where .= 'AND a.spring = 0';

        // Uso il reset default della list
		if(Tools::getIsset('submitResetorder')) {
			$cookie->_whereorder = "";
			$cookie->_cercaordineprodotto = "";
			$cookie->_cercatestoprodottoordine = "";
			$this->_where = 'AND oh.`id_order_history` = (SELECT MAX(`id_order_history`) FROM `'._DB_PREFIX_.'order_history` moh WHERE moh.`id_order` = a.`id_order` GROUP BY moh.`id_order`)';
			unset($cookie->_whereorder);
		}

        // esportaexcel: eliminato!
        
        */

        /* FINE OVERRIDE */
        
        if (Country::isCurrentlyUsed('country', true)) {
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT DISTINCT c.id_country, cl.`name`
			FROM `'._DB_PREFIX_.'orders` o
			'.Shop::addSqlAssociation('orders', 'o').'
			INNER JOIN `'._DB_PREFIX_.'address` a ON a.id_address = o.id_address_delivery
			INNER JOIN `'._DB_PREFIX_.'country` c ON a.id_country = c.id_country
			INNER JOIN `'._DB_PREFIX_.'country_lang` cl ON (c.`id_country` = cl.`id_country` AND cl.`id_lang` = '.(int)$this->context->language->id.')
			ORDER BY cl.name ASC');

            $country_array = array();
            foreach ($result as $row) {
                $country_array[$row['id_country']] = $row['name'];
            }

            $part1 = array_slice($this->fields_list, 0, 3);
            $part2 = array_slice($this->fields_list, 3);
            // Nascosta la colonna Delivery
            /*$part1['cname'] = array(
                'title' => $this->l('Delivery'),
                'type' => 'select',
                'list' => $country_array,
                'filter_key' => 'country!id_country',
                'filter_type' => 'int',
                'order_key' => 'cname'
            );*/
            $this->fields_list = array_merge($part1, $part2);
        }

        $this->shopLinkType = 'shop';
        $this->shopShareDatas = Shop::SHARE_ORDER;

        if (Tools::isSubmit('id_order')) {
            // Save context (in order to apply cart rule)
            $order = new Order((int)Tools::getValue('id_order'));
            $this->context->cart = new Cart($order->id_cart);
            $this->context->customer = new Customer($order->id_customer);
        }

        // CORREGGERE: controllare che funzioni bene e non serva DisplayDeleteLink
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?'),
                'icon' => 'icon-trash'
            ),
            'updateOrderStatus' => array('text' => $this->l('Change Order Status'), 'icon' => 'icon-refresh')
        );

        parent::__construct();
    }

    public static function setOrderCurrency($echo, $tr)
    {
        $order = new Order($tr['id_order']);
        return Tools::displayPrice($echo, (int)$order->id_currency);
    }

    public function initPageHeaderToolbar()
    {
        parent::initPageHeaderToolbar();

        if (empty($this->display)) {
            $this->page_header_toolbar_btn['new_order'] = array(
                'href' => self::$currentIndex.'&addorder&token='.$this->token,
                'desc' => $this->l('Add new order', null, null, false),
                'icon' => 'process-icon-new'
            );
        }

        if ($this->display == 'add') {
            unset($this->page_header_toolbar_btn['save']);
        }

        if (Context::getContext()->shop->getContext() != Shop::CONTEXT_SHOP && isset($this->page_header_toolbar_btn['new_order'])
            && Shop::isFeatureActive()) {
            unset($this->page_header_toolbar_btn['new_order']);
        }
    }

    public function renderForm()
    {
        if (Context::getContext()->shop->getContext() != Shop::CONTEXT_SHOP && Shop::isFeatureActive()) {
            $this->errors[] = $this->l('You have to select a shop before creating new orders.');
        }

        $id_cart = (int)Tools::getValue('id_cart');
        $cart = new Cart((int)$id_cart);
        if ($id_cart && !Validate::isLoadedObject($cart)) {
            $this->errors[] = $this->l('This cart does not exists');
        }
        if ($id_cart && Validate::isLoadedObject($cart) && !$cart->id_customer) {
            $this->errors[] = $this->l('The cart must have a customer');
        }
        if (count($this->errors)) {
            return false;
        }

        parent::renderForm();
        unset($this->toolbar_btn['save']);
        $this->addJqueryPlugin(array('autocomplete', 'fancybox', 'typewatch'));

        $defaults_order_state = array('cheque' => (int)Configuration::get('PS_OS_CHEQUE'),
                                                'bankwire' => (int)Configuration::get('PS_OS_BANKWIRE'),
                                                'cashondelivery' => Configuration::get('PS_OS_COD_VALIDATION') ? (int)Configuration::get('PS_OS_COD_VALIDATION') : (int)Configuration::get('PS_OS_PREPARATION'),
                                                'other' => (int)Configuration::get('PS_OS_PAYMENT'));
        $payment_modules = array();
        foreach (PaymentModule::getInstalledPaymentModules() as $p_module) {
            $payment_modules[] = Module::getInstanceById((int)$p_module['id_module']);
        }

        $this->context->smarty->assign(array(
            'recyclable_pack' => (int)Configuration::get('PS_RECYCLABLE_PACK'),
            'gift_wrapping' => (int)Configuration::get('PS_GIFT_WRAPPING'),
            'cart' => $cart,
            'currencies' => Currency::getCurrenciesByIdShop(Context::getContext()->shop->id),
            'langs' => Language::getLanguages(true, Context::getContext()->shop->id),
            'payment_modules' => $payment_modules,
            'order_states' => OrderState::getOrderStates((int)Context::getContext()->language->id),
            'defaults_order_state' => $defaults_order_state,
            'show_toolbar' => $this->show_toolbar,
            'toolbar_btn' => $this->toolbar_btn,
            'toolbar_scroll' => $this->toolbar_scroll,
            'PS_CATALOG_MODE' => Configuration::get('PS_CATALOG_MODE'),
            'title' => array($this->l('Orders'), $this->l('Create order'))

        ));
        $this->content .= $this->createTemplate('form.tpl')->fetch();
    }

    public function initToolbar()
    {
        if ($this->display == 'view') {
            /** @var Order $order */
            $order = $this->loadObject();
            $customer = $this->context->customer;

            if (!Validate::isLoadedObject($order)) {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminOrders'));
            }

            // Override
            $company_or_name = ($customer->is_company ? $customer->company : $customer->firstname.' '.$customer->lastname);
            $this->toolbar_title[] = sprintf($this->l('Order %1$s - %2$s'), $order->id, $company_or_name.' (#'.$customer->id.')');
            
            //$this->toolbar_title[] = sprintf($this->l('Order %1$s from %2$s %3$s'), $order->reference, $customer->firstname, $customer->lastname);
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);

            if ($order->hasBeenShipped()) {
                $type = $this->l('Return products');
            } elseif ($order->hasBeenPaid()) {
                $type = $this->l('Standard refund');
            } else {
                $type = $this->l('Cancel products');
            }

            if (!$order->hasBeenShipped() && !$this->lite_display) {
                $this->toolbar_btn['new'] = array(
                    'short' => 'Create',
                    'href' => '#',
                    'desc' => $this->l('Add a product'),
                    'class' => 'add_product'
                );
            }

            if (Configuration::get('PS_ORDER_RETURN') && !$this->lite_display) {
                $this->toolbar_btn['standard_refund'] = array(
                    'short' => 'Create',
                    'href' => '',
                    'desc' => $type,
                    'class' => 'process-icon-standardRefund'
                );
            }

            if ($order->hasInvoice() && !$this->lite_display) {
                $this->toolbar_btn['partial_refund'] = array(
                    'short' => 'Create',
                    'href' => '',
                    'desc' => $this->l('Partial refund'),
                    'class' => 'process-icon-partialRefund'
                );
            }
        }
        $res = parent::initToolbar();
        if (Context::getContext()->shop->getContext() != Shop::CONTEXT_SHOP && isset($this->toolbar_btn['new']) && Shop::isFeatureActive()) {
            unset($this->toolbar_btn['new']);
        }
        return $res;
    }

    public function setMedia()
    {
        parent::setMedia();

        $this->addJqueryUI('ui.datepicker');
        $this->addJS(_PS_JS_DIR_.'vendor/d3.v3.min.js');
        $api_key = (Configuration::get('PS_API_KEY')) ? 'key=' . Configuration::get('PS_API_KEY') . '&' : '';
        $protocol = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) ? 'https' : 'http';
        $this->addJS($protocol . '://maps.google.com/maps/api/js?' . $api_key);

        if ($this->tabAccess['edit'] == 1 && $this->display == 'view') {
            $this->addJS(_PS_JS_DIR_.'admin/orders.js');
            $this->addJS(_PS_JS_DIR_.'tools.js');
            $this->addJqueryPlugin('autocomplete');
        }
    }

    public function printPDFIcons($id_order, $tr)
    {
        static $valid_order_state = array();

        $order = new Order($id_order);
        if (!Validate::isLoadedObject($order)) {
            return '';
        }

        if (!isset($valid_order_state[$order->current_state])) {
            $valid_order_state[$order->current_state] = Validate::isLoadedObject($order->getCurrentOrderState());
        }

        if (!$valid_order_state[$order->current_state]) {
            return '';
        }

        $this->context->smarty->assign(array(
            'order' => $order,
            'tr' => $tr
        ));

        return $this->createTemplate('_print_pdf_icon.tpl')->fetch();
    }

    public function printNewCustomer($id_order, $tr)
    {
        return ($tr['new'] ? $this->l('Yes') : $this->l('No'));
    }

    public function processBulkUpdateOrderStatus()
    {
        if (Tools::isSubmit('submitUpdateOrderStatus')
            && ($id_order_state = (int)Tools::getValue('id_order_state'))) {
            if ($this->tabAccess['edit'] !== '1') {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            } else {
                $order_state = new OrderState($id_order_state);

                if (!Validate::isLoadedObject($order_state)) {
                    $this->errors[] = sprintf(Tools::displayError('Order status #%d cannot be loaded'), $id_order_state);
                } else {
                    foreach (Tools::getValue('orderBox') as $id_order) {
                        $order = new Order((int)$id_order);
                        if (!Validate::isLoadedObject($order)) {
                            $this->errors[] = sprintf(Tools::displayError('Order #%d cannot be loaded'), $id_order);
                        } else {
                            $current_order_state = $order->getCurrentOrderState();
                            if ($current_order_state->id == $order_state->id) {
                                $this->errors[] = $this->displayWarning(sprintf('Order #%d has already been assigned this status.', $id_order));
                            } else {
                                $history = new OrderHistory();
                                $history->id_order = $order->id;
                                $history->id_employee = (int)$this->context->employee->id;

                                $use_existings_payment = !$order->hasInvoice();
                                $history->changeIdOrderState((int)$order_state->id, $order, $use_existings_payment);

                                $carrier = new Carrier($order->id_carrier, $order->id_lang);
                                $templateVars = array();
                                if ($history->id_order_state == Configuration::get('PS_OS_SHIPPING') && $order->shipping_number) {
                                    $templateVars = array('{followup}' => str_replace('@', $order->shipping_number, $carrier->url));
                                }

                                if ($history->addWithemail(true, $templateVars)) {
                                    if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')) {
                                        foreach ($order->getProducts() as $product) {
                                            if (StockAvailable::dependsOnStock($product['product_id'])) {
                                                StockAvailable::synchronize($product['product_id'], (int)$product['id_shop']);
                                            }
                                        }
                                    }
                                } else {
                                    $this->errors[] = sprintf(Tools::displayError('Cannot change status for order #%d.'), $id_order);
                                }
                            }
                        }
                    }
                }
            }
            if (!count($this->errors)) {
                Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.$this->token);
            }
        }
    }

    public function renderList()
    {
        if (Tools::isSubmit('submitBulkupdateOrderStatus'.$this->table)) {
            if (Tools::getIsset('cancel')) {
                Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
            }

            $this->tpl_list_vars['updateOrderStatus_mode'] = true;
            $this->tpl_list_vars['order_statuses'] = $this->statuses_array;
            $this->tpl_list_vars['REQUEST_URI'] = $_SERVER['REQUEST_URI'];
            $this->tpl_list_vars['POST'] = $_POST;
        }

        /* Override */

		$prodotti = Db::getInstance()->executeS('
            SELECT DISTINCT od.product_reference as reference, od.product_id as id, od.product_name as name
            FROM '._DB_PREFIX_.'order_detail od 
            WHERE od.product_name != ""
            GROUP BY od.product_reference, od.product_id, od.product_name
            ORDER BY od.product_name ASC
        ');

        $this->tpl_list_vars['prodotti'] = $prodotti;

        $this->addjQueryPlugin(array(
            'select2'
        ));

        $this->addJS(_PS_JS_DIR_.'jquery/plugins/select2/select2_locale_'.$this->context->language->iso_code.'.js');
        
        // PROVA TOTALE, CORREGGERE
        $totale_ordini = Tools::displayPrice($this->getTotal(), $this->context->currency->id);
        $this->tpl_list_vars['totale_ordini'] = $totale_ordini;
        
        /* Fine override */

        return parent::renderList();
    }

    // Override: correggere!
    private function getTotal()
	{
		$total = 0;
		foreach($this->_list AS $item)
			if ($item['id_currency'] == Configuration::get('PS_CURRENCY_DEFAULT'))
				$total += (float)($item['total_paid']);
			else
			{
				$currency = new Currency((int)($item['id_currency']));
				$total += Tools::ps_round((float)($item['total_paid']) / (float)($currency->conversion_rate), 2);
			}
		return $total;
	}

    public function postProcess()
    {
        // If id_order is sent, we instanciate a new Order object
        if (Tools::isSubmit('id_order') && Tools::getValue('id_order') > 0) {
            $order = new Order(Tools::getValue('id_order'));
            if (!Validate::isLoadedObject($order)) {
                $this->errors[] = Tools::displayError('The order cannot be found within your database.');
            }
            ShopUrl::cacheMainDomainForShop((int)$order->id_shop);
        }

        /* Update shipping number */
        if (Tools::isSubmit('submitShippingNumber') && isset($order)) {
            if ($this->tabAccess['edit'] === '1') {
                $order_carrier = new OrderCarrier(Tools::getValue('id_order_carrier'));
                if (!Validate::isLoadedObject($order_carrier)) {
                    $this->errors[] = Tools::displayError('The order carrier ID is invalid.');
                } elseif (!Validate::isTrackingNumber(Tools::getValue('tracking_number'))) {
                    $this->errors[] = Tools::displayError('The tracking number is incorrect.');
                } else {
                    // update shipping number
                    // Keep these two following lines for backward compatibility, remove on 1.6 version
                    $order->shipping_number = Tools::getValue('tracking_number');
                    $order->update();

                    // Update order_carrier
                    $order_carrier->tracking_number = pSQL(Tools::getValue('tracking_number'));
                    if ($order_carrier->update()) {
                        // Send mail to customer
                        $customer = new Customer((int)$order->id_customer);
                        $carrier = new Carrier((int)$order->id_carrier, $order->id_lang);
                        if (!Validate::isLoadedObject($customer)) {
                            throw new PrestaShopException('Can\'t load Customer object');
                        }
                        if (!Validate::isLoadedObject($carrier)) {
                            throw new PrestaShopException('Can\'t load Carrier object');
                        }
                        $templateVars = array(
                            '{followup}' => str_replace('@', $order->shipping_number, $carrier->url),
                            '{firstname}' => $customer->firstname,
                            '{lastname}' => $customer->lastname,
                            '{id_order}' => $order->id,
                            '{shipping_number}' => $order->shipping_number,
                            '{order_name}' => $order->getUniqReference()
                        );
                        if (@Mail::Send((int)$order->id_lang, 'in_transit', Mail::l('Package in transit', (int)$order->id_lang), $templateVars,
                            $customer->email, $customer->firstname.' '.$customer->lastname, null, null, null, null,
                            _PS_MAIL_DIR_, true, (int)$order->id_shop)) {
                            Hook::exec('actionAdminOrdersTrackingNumberUpdate', array('order' => $order, 'customer' => $customer, 'carrier' => $carrier), null, false, true, false, $order->id_shop);
                            Tools::redirectAdmin(self::$currentIndex.'&id_order='.$order->id.'&vieworder&conf=4&token='.$this->token);
                        } else {
                            $this->errors[] = Tools::displayError('An error occurred while sending an email to the customer.');
                        }
                    } else {
                        $this->errors[] = Tools::displayError('The order carrier cannot be updated.');
                    }
                }
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        }

        /* Change order status, add a new entry in order history and send an e-mail to the customer if needed */
        elseif (Tools::isSubmit('submitState') && isset($order)) {
            if ($this->tabAccess['edit'] === '1') {
                $order_state = new OrderState(Tools::getValue('id_order_state'));

                if (!Validate::isLoadedObject($order_state)) {
                    $this->errors[] = Tools::displayError('The new order status is invalid.');
                } else {
                    $current_order_state = $order->getCurrentOrderState();
                    if ($current_order_state->id != $order_state->id) {
                        // Create new OrderHistory
                        $history = new OrderHistory();
                        $history->id_order = $order->id;
                        $history->id_employee = (int)$this->context->employee->id;

                        $use_existings_payment = false;
                        if (!$order->hasInvoice()) {
                            $use_existings_payment = true;
                        }
                        $history->changeIdOrderState((int)$order_state->id, $order, $use_existings_payment);

                        $carrier = new Carrier($order->id_carrier, $order->id_lang);
                        $templateVars = array();
                        if ($history->id_order_state == Configuration::get('PS_OS_SHIPPING') && $order->shipping_number) {
                            $templateVars = array('{followup}' => str_replace('@', $order->shipping_number, $carrier->url));
                        }

                        // Save all changes
                        if ($history->addWithemail(true, $templateVars)) {
                            // synchronizes quantities if needed..
                            if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')) {
                                foreach ($order->getProducts() as $product) {
                                    if (StockAvailable::dependsOnStock($product['product_id'])) {
                                        StockAvailable::synchronize($product['product_id'], (int)$product['id_shop']);
                                    }
                                }
                            }

                            Tools::redirectAdmin(self::$currentIndex.'&id_order='.(int)$order->id.'&vieworder&token='.$this->token);
                        }
                        $this->errors[] = Tools::displayError('An error occurred while changing order status, or we were unable to send an email to the customer.');
                    } else {
                        $this->errors[] = Tools::displayError('The order has already been assigned this status.');
                    }
                }
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        }

        /* Add a new message for the current order and send an e-mail to the customer if needed */
        elseif (Tools::isSubmit('submitMessage') && isset($order)) {
            if ($this->tabAccess['edit'] === '1') {
                $customer = new Customer(Tools::getValue('id_customer'));
                if (!Validate::isLoadedObject($customer)) {
                    $this->errors[] = Tools::displayError('The customer is invalid.');
                } elseif (!Tools::getValue('message')) {
                    $this->errors[] = Tools::displayError('The message cannot be blank.');
                } else {
                    /* Get message rules and and check fields validity */
                    $rules = call_user_func(array('Message', 'getValidationRules'), 'Message');
                    foreach ($rules['required'] as $field) {
                        if (($value = Tools::getValue($field)) == false && (string)$value != '0') {
                            if (!Tools::getValue('id_'.$this->table) || $field != 'passwd') {
                                $this->errors[] = sprintf(Tools::displayError('field %s is required.'), $field);
                            }
                        }
                    }
                    foreach ($rules['size'] as $field => $maxLength) {
                        if (Tools::getValue($field) && Tools::strlen(Tools::getValue($field)) > $maxLength) {
                            $this->errors[] = sprintf(Tools::displayError('field %1$s is too long (%2$d chars max).'), $field, $maxLength);
                        }
                    }
                    foreach ($rules['validate'] as $field => $function) {
                        if (Tools::getValue($field)) {
                            if (!Validate::$function(htmlentities(Tools::getValue($field), ENT_COMPAT, 'UTF-8'))) {
                                $this->errors[] = sprintf(Tools::displayError('field %s is invalid.'), $field);
                            }
                        }
                    }

                    if (!count($this->errors)) {
                        //check if a thread already exist
                        $id_customer_thread = CustomerThread::getIdCustomerThreadByEmailAndIdOrder($customer->email, $order->id);
                        if (!$id_customer_thread) {
                            $customer_thread = new CustomerThread();
                            $customer_thread->id_contact = 0;
                            $customer_thread->id_customer = (int)$order->id_customer;
                            $customer_thread->id_shop = (int)$this->context->shop->id;
                            $customer_thread->id_order = (int)$order->id;
                            $customer_thread->id_lang = (int)$this->context->language->id;
                            $customer_thread->email = $customer->email;
                            $customer_thread->status = 'open';
                            $customer_thread->token = Tools::passwdGen(12);
                            $customer_thread->add();
                        } else {
                            $customer_thread = new CustomerThread((int)$id_customer_thread);
                        }

                        $customer_message = new CustomerMessage();
                        $customer_message->id_customer_thread = $customer_thread->id;
                        $customer_message->id_employee = (int)$this->context->employee->id;
                        $customer_message->message = Tools::getValue('message');
                        $customer_message->private = Tools::getValue('visibility');

                        if (!$customer_message->add()) {
                            $this->errors[] = Tools::displayError('An error occurred while saving the message.');
                        } elseif ($customer_message->private) {
                            Tools::redirectAdmin(self::$currentIndex.'&id_order='.(int)$order->id.'&vieworder&conf=11&token='.$this->token);
                        } else {
                            $message = $customer_message->message;
                            if (Configuration::get('PS_MAIL_TYPE', null, null, $order->id_shop) != Mail::TYPE_TEXT) {
                                $message = Tools::nl2br($customer_message->message);
                            }

                            $varsTpl = array(
                                '{lastname}' => $customer->lastname,
                                '{firstname}' => $customer->firstname,
                                '{id_order}' => $order->id,
                                '{order_name}' => $order->getUniqReference(),
                                '{message}' => $message
                            );
                            if (@Mail::Send((int)$order->id_lang, 'order_merchant_comment',
                                Mail::l('New message regarding your order', (int)$order->id_lang), $varsTpl, $customer->email,
                                $customer->firstname.' '.$customer->lastname, null, null, null, null, _PS_MAIL_DIR_, true, (int)$order->id_shop)) {
                                Tools::redirectAdmin(self::$currentIndex.'&id_order='.$order->id.'&vieworder&conf=11'.'&token='.$this->token);
                            }
                        }
                        $this->errors[] = Tools::displayError('An error occurred while sending an email to the customer.');
                    }
                }
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to delete this.');
            }
        }

        /* Partial refund from order */
        elseif (Tools::isSubmit('partialRefund') && isset($order)) {
            if ($this->tabAccess['edit'] == '1') {
                if (Tools::isSubmit('partialRefundProduct') && ($refunds = Tools::getValue('partialRefundProduct')) && is_array($refunds)) {
                    $amount = 0;
                    $order_detail_list = array();
                    $full_quantity_list = array();
                    foreach ($refunds as $id_order_detail => $amount_detail) {
                        $quantity = Tools::getValue('partialRefundProductQuantity');
                        if (!$quantity[$id_order_detail]) {
                            continue;
                        }

                        $full_quantity_list[$id_order_detail] = (int)$quantity[$id_order_detail];

                        $order_detail_list[$id_order_detail] = array(
                            'quantity' => (int)$quantity[$id_order_detail],
                            'id_order_detail' => (int)$id_order_detail
                        );

                        $order_detail = new OrderDetail((int)$id_order_detail);
                        if (empty($amount_detail)) {
                            $order_detail_list[$id_order_detail]['unit_price'] = (!Tools::getValue('TaxMethod') ? $order_detail->unit_price_tax_excl : $order_detail->unit_price_tax_incl);
                            $order_detail_list[$id_order_detail]['amount'] = $order_detail->unit_price_tax_incl * $order_detail_list[$id_order_detail]['quantity'];
                        } else {
                            $order_detail_list[$id_order_detail]['amount'] = (float)str_replace(',', '.', $amount_detail);
                            $order_detail_list[$id_order_detail]['unit_price'] = $order_detail_list[$id_order_detail]['amount'] / $order_detail_list[$id_order_detail]['quantity'];
                        }
                        $amount += $order_detail_list[$id_order_detail]['amount'];
                        if (!$order->hasBeenDelivered() || ($order->hasBeenDelivered() && Tools::isSubmit('reinjectQuantities')) && $order_detail_list[$id_order_detail]['quantity'] > 0) {
                            $this->reinjectQuantity($order_detail, $order_detail_list[$id_order_detail]['quantity']);
                        }
                    }

                    $shipping_cost_amount = (float)str_replace(',', '.', Tools::getValue('partialRefundShippingCost')) ? (float)str_replace(',', '.', Tools::getValue('partialRefundShippingCost')) : false;

                    if ($amount == 0 && $shipping_cost_amount == 0) {
                        if (!empty($refunds)) {
                            $this->errors[] = Tools::displayError('Please enter a quantity to proceed with your refund.');
                        } else {
                            $this->errors[] = Tools::displayError('Please enter an amount to proceed with your refund.');
                        }
                        return false;
                    }

                    $choosen = false;
                    $voucher = 0;

                    if ((int)Tools::getValue('refund_voucher_off') == 1) {
                        $amount -= $voucher = (float)Tools::getValue('order_discount_price');
                    } elseif ((int)Tools::getValue('refund_voucher_off') == 2) {
                        $choosen = true;
                        $amount = $voucher = (float)Tools::getValue('refund_voucher_choose');
                    }

                    if ($shipping_cost_amount > 0) {
                        if (!Tools::getValue('TaxMethod')) {
                            $tax = new Tax();
                            $tax->rate = $order->carrier_tax_rate;
                            $tax_calculator = new TaxCalculator(array($tax));
                            $amount += $tax_calculator->addTaxes($shipping_cost_amount);
                        } else {
                            $amount += $shipping_cost_amount;
                        }
                    }

                    $order_carrier = new OrderCarrier((int)$order->getIdOrderCarrier());
                    if (Validate::isLoadedObject($order_carrier)) {
                        $order_carrier->weight = (float)$order->getTotalWeight();
                        if ($order_carrier->update()) {
                            $order->weight = sprintf("%.3f ".Configuration::get('PS_WEIGHT_UNIT'), $order_carrier->weight);
                        }
                    }

                    if ($amount >= 0) {
                        if (!OrderSlip::create($order, $order_detail_list, $shipping_cost_amount, $voucher, $choosen,
                            (Tools::getValue('TaxMethod') ? false : true))) {
                            $this->errors[] = Tools::displayError('You cannot generate a partial credit slip.');
                        } else {
                            Hook::exec('actionOrderSlipAdd', array('order' => $order, 'productList' => $order_detail_list, 'qtyList' => $full_quantity_list), null, false, true, false, $order->id_shop);
                            $customer = new Customer((int)($order->id_customer));
                            $params['{lastname}'] = $customer->lastname;
                            $params['{firstname}'] = $customer->firstname;
                            $params['{id_order}'] = $order->id;
                            $params['{order_name}'] = $order->getUniqReference();
                            @Mail::Send(
                                (int)$order->id_lang,
                                'credit_slip',
                                Mail::l('New credit slip regarding your order', (int)$order->id_lang),
                                $params,
                                $customer->email,
                                $customer->firstname.' '.$customer->lastname,
                                null,
                                null,
                                null,
                                null,
                                _PS_MAIL_DIR_,
                                true,
                                (int)$order->id_shop
                            );
                        }

                        foreach ($order_detail_list as &$product) {
                            $order_detail = new OrderDetail((int)$product['id_order_detail']);
                            if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')) {
                                StockAvailable::synchronize($order_detail->product_id);
                            }
                        }

                        // Generate voucher
                        if (Tools::isSubmit('generateDiscountRefund') && !count($this->errors) && $amount > 0) {
                            $cart_rule = new CartRule();
                            $cart_rule->description = sprintf($this->l('Credit slip for order #%d'), $order->id);
                            $language_ids = Language::getIDs(false);
                            foreach ($language_ids as $id_lang) {
                                // Define a temporary name
                                $cart_rule->name[$id_lang] = sprintf('V0C%1$dO%2$d', $order->id_customer, $order->id);
                            }

                            // Define a temporary code
                            $cart_rule->code = sprintf('V0C%1$dO%2$d', $order->id_customer, $order->id);
                            $cart_rule->quantity = 1;
                            $cart_rule->quantity_per_user = 1;

                            // Specific to the customer
                            $cart_rule->id_customer = $order->id_customer;
                            $now = time();
                            $cart_rule->date_from = date('Y-m-d H:i:s', $now);
                            $cart_rule->date_to = date('Y-m-d H:i:s', strtotime('+1 year'));
                            $cart_rule->partial_use = 1;
                            $cart_rule->active = 1;

                            $cart_rule->reduction_amount = $amount;
                            $cart_rule->reduction_tax = $order->getTaxCalculationMethod() != PS_TAX_EXC;
                            $cart_rule->minimum_amount_currency = $order->id_currency;
                            $cart_rule->reduction_currency = $order->id_currency;

                            if (!$cart_rule->add()) {
                                $this->errors[] = Tools::displayError('You cannot generate a voucher.');
                            } else {
                                // Update the voucher code and name
                                foreach ($language_ids as $id_lang) {
                                    $cart_rule->name[$id_lang] = sprintf('V%1$dC%2$dO%3$d', $cart_rule->id, $order->id_customer, $order->id);
                                }
                                $cart_rule->code = sprintf('V%1$dC%2$dO%3$d', $cart_rule->id, $order->id_customer, $order->id);

                                if (!$cart_rule->update()) {
                                    $this->errors[] = Tools::displayError('You cannot generate a voucher.');
                                } else {
                                    $currency = $this->context->currency;
                                    $customer = new Customer((int)($order->id_customer));
                                    $params['{lastname}'] = $customer->lastname;
                                    $params['{firstname}'] = $customer->firstname;
                                    $params['{id_order}'] = $order->id;
                                    $params['{order_name}'] = $order->getUniqReference();
                                    $params['{voucher_amount}'] = Tools::displayPrice($cart_rule->reduction_amount, $currency, false);
                                    $params['{voucher_num}'] = $cart_rule->code;
                                    @Mail::Send((int)$order->id_lang, 'voucher', sprintf(Mail::l('New voucher for your order #%s', (int)$order->id_lang), $order->reference),
                                        $params, $customer->email, $customer->firstname.' '.$customer->lastname, null, null, null,
                                        null, _PS_MAIL_DIR_, true, (int)$order->id_shop);
                                }
                            }
                        }
                    } else {
                        if (!empty($refunds)) {
                            $this->errors[] = Tools::displayError('Please enter a quantity to proceed with your refund.');
                        } else {
                            $this->errors[] = Tools::displayError('Please enter an amount to proceed with your refund.');
                        }
                    }

                    // Redirect if no errors
                    if (!count($this->errors)) {
                        Tools::redirectAdmin(self::$currentIndex.'&id_order='.$order->id.'&vieworder&conf=30&token='.$this->token);
                    }
                } else {
                    $this->errors[] = Tools::displayError('The partial refund data is incorrect.');
                }
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to delete this.');
            }
        }

        /* Cancel product from order */
        elseif (Tools::isSubmit('cancelProduct') && isset($order)) {
            if ($this->tabAccess['delete'] === '1') {
                if (!Tools::isSubmit('id_order_detail') && !Tools::isSubmit('id_customization')) {
                    $this->errors[] = Tools::displayError('You must select a product.');
                } elseif (!Tools::isSubmit('cancelQuantity') && !Tools::isSubmit('cancelCustomizationQuantity')) {
                    $this->errors[] = Tools::displayError('You must enter a quantity.');
                } else {
                    $productList = Tools::getValue('id_order_detail');
                    if ($productList) {
                        $productList = array_map('intval', $productList);
                    }

                    $customizationList = Tools::getValue('id_customization');
                    if ($customizationList) {
                        $customizationList = array_map('intval', $customizationList);
                    }

                    $qtyList = Tools::getValue('cancelQuantity');
                    if ($qtyList) {
                        $qtyList = array_map('intval', $qtyList);
                    }

                    $customizationQtyList = Tools::getValue('cancelCustomizationQuantity');
                    if ($customizationQtyList) {
                        $customizationQtyList = array_map('intval', $customizationQtyList);
                    }

                    $full_product_list = $productList;
                    $full_quantity_list = $qtyList;

                    if ($customizationList) {
                        foreach ($customizationList as $key => $id_order_detail) {
                            $full_product_list[(int)$id_order_detail] = $id_order_detail;
                            if (isset($customizationQtyList[$key])) {
                                $full_quantity_list[(int)$id_order_detail] += $customizationQtyList[$key];
                            }
                        }
                    }

                    if ($productList || $customizationList) {
                        if ($productList) {
                            $id_cart = Cart::getCartIdByOrderId($order->id);
                            $customization_quantities = Customization::countQuantityByCart($id_cart);

                            foreach ($productList as $key => $id_order_detail) {
                                $qtyCancelProduct = abs($qtyList[$key]);
                                if (!$qtyCancelProduct) {
                                    $this->errors[] = Tools::displayError('No quantity has been selected for this product.');
                                }

                                $order_detail = new OrderDetail($id_order_detail);
                                $customization_quantity = 0;
                                if (array_key_exists($order_detail->product_id, $customization_quantities) && array_key_exists($order_detail->product_attribute_id, $customization_quantities[$order_detail->product_id])) {
                                    $customization_quantity = (int)$customization_quantities[$order_detail->product_id][$order_detail->product_attribute_id];
                                }

                                if (($order_detail->product_quantity - $customization_quantity - $order_detail->product_quantity_refunded - $order_detail->product_quantity_return) < $qtyCancelProduct) {
                                    $this->errors[] = Tools::displayError('An invalid quantity was selected for this product.');
                                }
                            }
                        }
                        if ($customizationList) {
                            $customization_quantities = Customization::retrieveQuantitiesFromIds(array_keys($customizationList));

                            foreach ($customizationList as $id_customization => $id_order_detail) {
                                $qtyCancelProduct = abs($customizationQtyList[$id_customization]);
                                $customization_quantity = $customization_quantities[$id_customization];

                                if (!$qtyCancelProduct) {
                                    $this->errors[] = Tools::displayError('No quantity has been selected for this product.');
                                }

                                if ($qtyCancelProduct > ($customization_quantity['quantity'] - ($customization_quantity['quantity_refunded'] + $customization_quantity['quantity_returned']))) {
                                    $this->errors[] = Tools::displayError('An invalid quantity was selected for this product.');
                                }
                            }
                        }

                        if (!count($this->errors) && $productList) {
                            foreach ($productList as $key => $id_order_detail) {
                                $qty_cancel_product = abs($qtyList[$key]);
                                $order_detail = new OrderDetail((int)($id_order_detail));

                                if (!$order->hasBeenDelivered() || ($order->hasBeenDelivered() && Tools::isSubmit('reinjectQuantities')) && $qty_cancel_product > 0) {
                                    $this->reinjectQuantity($order_detail, $qty_cancel_product);
                                }

                                // Delete product
                                $order_detail = new OrderDetail((int)$id_order_detail);
                                if (!$order->deleteProduct($order, $order_detail, $qty_cancel_product)) {
                                    $this->errors[] = Tools::displayError('An error occurred while attempting to delete the product.').' <span class="bold">'.$order_detail->product_name.'</span>';
                                }
                                // Update weight SUM
                                $order_carrier = new OrderCarrier((int)$order->getIdOrderCarrier());
                                if (Validate::isLoadedObject($order_carrier)) {
                                    $order_carrier->weight = (float)$order->getTotalWeight();
                                    if ($order_carrier->update()) {
                                        $order->weight = sprintf("%.3f ".Configuration::get('PS_WEIGHT_UNIT'), $order_carrier->weight);
                                    }
                                }

                                if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && StockAvailable::dependsOnStock($order_detail->product_id)) {
                                    StockAvailable::synchronize($order_detail->product_id);
                                }
                                Hook::exec('actionProductCancel', array('order' => $order, 'id_order_detail' => (int)$id_order_detail), null, false, true, false, $order->id_shop);
                            }
                        }
                        if (!count($this->errors) && $customizationList) {
                            foreach ($customizationList as $id_customization => $id_order_detail) {
                                $order_detail = new OrderDetail((int)($id_order_detail));
                                $qtyCancelProduct = abs($customizationQtyList[$id_customization]);
                                if (!$order->deleteCustomization($id_customization, $qtyCancelProduct, $order_detail)) {
                                    $this->errors[] = Tools::displayError('An error occurred while attempting to delete product customization.').' '.$id_customization;
                                }
                            }
                        }
                        // E-mail params
                        if ((Tools::isSubmit('generateCreditSlip') || Tools::isSubmit('generateDiscount')) && !count($this->errors)) {
                            $customer = new Customer((int)($order->id_customer));
                            $params['{lastname}'] = $customer->lastname;
                            $params['{firstname}'] = $customer->firstname;
                            $params['{id_order}'] = $order->id;
                            $params['{order_name}'] = $order->getUniqReference();
                        }

                        // Generate credit slip
                        if (Tools::isSubmit('generateCreditSlip') && !count($this->errors)) {
                            $product_list = array();
                            $amount = $order_detail->unit_price_tax_incl * $full_quantity_list[$id_order_detail];

                            $choosen = false;
                            if ((int)Tools::getValue('refund_total_voucher_off') == 1) {
                                $amount -= $voucher = (float)Tools::getValue('order_discount_price');
                            } elseif ((int)Tools::getValue('refund_total_voucher_off') == 2) {
                                $choosen = true;
                                $amount = $voucher = (float)Tools::getValue('refund_total_voucher_choose');
                            }
                            foreach ($full_product_list as $id_order_detail) {
                                $order_detail = new OrderDetail((int)$id_order_detail);
                                $product_list[$id_order_detail] = array(
                                    'id_order_detail' => $id_order_detail,
                                    'quantity' => $full_quantity_list[$id_order_detail],
                                    'unit_price' => $order_detail->unit_price_tax_excl,
                                    'amount' => isset($amount) ? $amount : $order_detail->unit_price_tax_incl * $full_quantity_list[$id_order_detail],
                                );
                            }

                            $shipping = Tools::isSubmit('shippingBack') ? null : false;

                            if (!OrderSlip::create($order, $product_list, $shipping, $voucher, $choosen)) {
                                $this->errors[] = Tools::displayError('A credit slip cannot be generated. ');
                            } else {
                                Hook::exec('actionOrderSlipAdd', array('order' => $order, 'productList' => $full_product_list, 'qtyList' => $full_quantity_list), null, false, true, false, $order->id_shop);
                                @Mail::Send(
                                    (int)$order->id_lang,
                                    'credit_slip',
                                    Mail::l('New credit slip regarding your order', (int)$order->id_lang),
                                    $params,
                                    $customer->email,
                                    $customer->firstname.' '.$customer->lastname,
                                    null,
                                    null,
                                    null,
                                    null,
                                    _PS_MAIL_DIR_,
                                    true,
                                    (int)$order->id_shop
                                );
                            }
                        }

                        // Generate voucher
                        if (Tools::isSubmit('generateDiscount') && !count($this->errors)) {
                            $cartrule = new CartRule();
                            $language_ids = Language::getIDs((bool)$order);
                            $cartrule->description = sprintf($this->l('Credit card slip for order #%d'), $order->id);
                            foreach ($language_ids as $id_lang) {
                                // Define a temporary name
                                $cartrule->name[$id_lang] = 'V0C'.(int)($order->id_customer).'O'.(int)($order->id);
                            }
                            // Define a temporary code
                            $cartrule->code = 'V0C'.(int)($order->id_customer).'O'.(int)($order->id);

                            $cartrule->quantity = 1;
                            $cartrule->quantity_per_user = 1;
                            // Specific to the customer
                            $cartrule->id_customer = $order->id_customer;
                            $now = time();
                            $cartrule->date_from = date('Y-m-d H:i:s', $now);
                            $cartrule->date_to = date('Y-m-d H:i:s', $now + (3600 * 24 * 365.25)); /* 1 year */
                            $cartrule->active = 1;

                            $products = $order->getProducts(false, $full_product_list, $full_quantity_list);

                            $total = 0;
                            foreach ($products as $product) {
                                $total += $product['unit_price_tax_incl'] * $product['product_quantity'];
                            }

                            if (Tools::isSubmit('shippingBack')) {
                                $total += $order->total_shipping;
                            }

                            if ((int)Tools::getValue('refund_total_voucher_off') == 1) {
                                $total -= (float)Tools::getValue('order_discount_price');
                            } elseif ((int)Tools::getValue('refund_total_voucher_off') == 2) {
                                $total = (float)Tools::getValue('refund_total_voucher_choose');
                            }

                            $cartrule->reduction_amount = $total;
                            $cartrule->reduction_tax = true;
                            $cartrule->minimum_amount_currency = $order->id_currency;
                            $cartrule->reduction_currency = $order->id_currency;

                            if (!$cartrule->add()) {
                                $this->errors[] = Tools::displayError('You cannot generate a voucher.');
                            } else {
                                // Update the voucher code and name
                                foreach ($language_ids as $id_lang) {
                                    $cartrule->name[$id_lang] = 'V'.(int)($cartrule->id).'C'.(int)($order->id_customer).'O'.$order->id;
                                }
                                $cartrule->code = 'V'.(int)($cartrule->id).'C'.(int)($order->id_customer).'O'.$order->id;
                                if (!$cartrule->update()) {
                                    $this->errors[] = Tools::displayError('You cannot generate a voucher.');
                                } else {
                                    $currency = $this->context->currency;
                                    $params['{voucher_amount}'] = Tools::displayPrice($cartrule->reduction_amount, $currency, false);
                                    $params['{voucher_num}'] = $cartrule->code;
                                    @Mail::Send((int)$order->id_lang, 'voucher', sprintf(Mail::l('New voucher for your order #%s', (int)$order->id_lang), $order->reference),
                                    $params, $customer->email, $customer->firstname.' '.$customer->lastname, null, null, null,
                                    null, _PS_MAIL_DIR_, true, (int)$order->id_shop);
                                }
                            }
                        }
                    } else {
                        $this->errors[] = Tools::displayError('No product or quantity has been selected.');
                    }

                    // Redirect if no errors
                    if (!count($this->errors)) {
                        Tools::redirectAdmin(self::$currentIndex.'&id_order='.$order->id.'&vieworder&conf=31&token='.$this->token);
                    }
                }
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to delete this.');
            }
        } elseif (Tools::isSubmit('messageReaded')) {
            Message::markAsReaded(Tools::getValue('messageReaded'), $this->context->employee->id);
        } elseif (Tools::isSubmit('submitAddPayment') && isset($order)) {
            if ($this->tabAccess['edit'] === '1') {
                $amount = str_replace(',', '.', Tools::getValue('payment_amount'));
                $currency = new Currency(Tools::getValue('payment_currency'));
                $order_has_invoice = $order->hasInvoice();
                if ($order_has_invoice) {
                    $order_invoice = new OrderInvoice(Tools::getValue('payment_invoice'));
                } else {
                    $order_invoice = null;
                }

                if (!Validate::isLoadedObject($order)) {
                    $this->errors[] = Tools::displayError('The order cannot be found');
                } elseif (!Validate::isNegativePrice($amount) || !(float)$amount) {
                    $this->errors[] = Tools::displayError('The amount is invalid.');
                } elseif (!Validate::isGenericName(Tools::getValue('payment_method'))) {
                    $this->errors[] = Tools::displayError('The selected payment method is invalid.');
                } elseif (!Validate::isString(Tools::getValue('payment_transaction_id'))) {
                    $this->errors[] = Tools::displayError('The transaction ID is invalid.');
                } elseif (!Validate::isLoadedObject($currency)) {
                    $this->errors[] = Tools::displayError('The selected currency is invalid.');
                } elseif ($order_has_invoice && !Validate::isLoadedObject($order_invoice)) {
                    $this->errors[] = Tools::displayError('The invoice is invalid.');
                } elseif (!Validate::isDate(Tools::getValue('payment_date'))) {
                    $this->errors[] = Tools::displayError('The date is invalid');
                } else {
                    if (!$order->addOrderPayment($amount, Tools::getValue('payment_method'), Tools::getValue('payment_transaction_id'), $currency, Tools::getValue('payment_date'), $order_invoice)) {
                        $this->errors[] = Tools::displayError('An error occurred during payment.');
                    } else {
                        Tools::redirectAdmin(self::$currentIndex.'&id_order='.$order->id.'&vieworder&conf=4&token='.$this->token);
                    }
                }
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        } elseif (Tools::isSubmit('submitEditNote')) {
            $note = Tools::getValue('note');
            $order_invoice = new OrderInvoice((int)Tools::getValue('id_order_invoice'));
            if (Validate::isLoadedObject($order_invoice) && Validate::isCleanHtml($note)) {
                if ($this->tabAccess['edit'] === '1') {
                    $order_invoice->note = $note;
                    if ($order_invoice->save()) {
                        Tools::redirectAdmin(self::$currentIndex.'&id_order='.$order_invoice->id_order.'&vieworder&conf=4&token='.$this->token);
                    } else {
                        $this->errors[] = Tools::displayError('The invoice note was not saved.');
                    }
                } else {
                    $this->errors[] = Tools::displayError('You do not have permission to edit this.');
                }
            } else {
                $this->errors[] = Tools::displayError('The invoice for edit note was unable to load. ');
            }
        } elseif (Tools::isSubmit('submitAddOrder') && ($id_cart = Tools::getValue('id_cart')) &&
            ($module_name = Tools::getValue('payment_module_name')) &&
            ($id_order_state = Tools::getValue('id_order_state')) && Validate::isModuleName($module_name)) {
            if ($this->tabAccess['edit'] === '1') {
                if (!Configuration::get('PS_CATALOG_MODE')) {
                    $payment_module = Module::getInstanceByName($module_name);
                } else {
                    $payment_module = new BoOrder();
                }

                $cart = new Cart((int)$id_cart);
                Context::getContext()->currency = new Currency((int)$cart->id_currency);
                Context::getContext()->customer = new Customer((int)$cart->id_customer);

                $bad_delivery = false;
                if (($bad_delivery = (bool)!Address::isCountryActiveById((int)$cart->id_address_delivery))
                    || !Address::isCountryActiveById((int)$cart->id_address_invoice)) {
                    if ($bad_delivery) {
                        $this->errors[] = Tools::displayError('This delivery address country is not active.');
                    } else {
                        $this->errors[] = Tools::displayError('This invoice address country is not active.');
                    }
                } else {
                    $employee = new Employee((int)Context::getContext()->cookie->id_employee);
                    $payment_module->validateOrder(
                        (int)$cart->id, (int)$id_order_state,
                        $cart->getOrderTotal(true, Cart::BOTH), $payment_module->displayName, $this->l('Manual order -- Employee:').' '.
                        substr($employee->firstname, 0, 1).'. '.$employee->lastname, array(), null, false, $cart->secure_key
                    );
                    if ($payment_module->currentOrder) {
                        Tools::redirectAdmin(self::$currentIndex.'&id_order='.$payment_module->currentOrder.'&vieworder'.'&token='.$this->token);
                    }
                }
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to add this.');
            }
        } elseif ((Tools::isSubmit('submitAddressShipping') || Tools::isSubmit('submitAddressInvoice')) && isset($order)) {
            if ($this->tabAccess['edit'] === '1') {
                $address = new Address(Tools::getValue('id_address'));
                if (Validate::isLoadedObject($address)) {
                    // Update the address on order
                    if (Tools::isSubmit('submitAddressShipping')) {
                        $order->id_address_delivery = $address->id;
                    } elseif (Tools::isSubmit('submitAddressInvoice')) {
                        $order->id_address_invoice = $address->id;
                    }
                    $order->update();
                    Tools::redirectAdmin(self::$currentIndex.'&id_order='.$order->id.'&vieworder&conf=4&token='.$this->token);
                } else {
                    $this->errors[] = Tools::displayError('This address can\'t be loaded');
                }
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        } elseif (Tools::isSubmit('submitChangeCurrency') && isset($order)) {
            if ($this->tabAccess['edit'] === '1') {
                if (Tools::getValue('new_currency') != $order->id_currency && !$order->valid) {
                    $old_currency = new Currency($order->id_currency);
                    $currency = new Currency(Tools::getValue('new_currency'));
                    if (!Validate::isLoadedObject($currency)) {
                        throw new PrestaShopException('Can\'t load Currency object');
                    }

                    // Update order detail amount
                    foreach ($order->getOrderDetailList() as $row) {
                        $order_detail = new OrderDetail($row['id_order_detail']);
                        $fields = array(
                            'ecotax',
                            'product_price',
                            'reduction_amount',
                            'total_shipping_price_tax_excl',
                            'total_shipping_price_tax_incl',
                            'total_price_tax_incl',
                            'total_price_tax_excl',
                            'product_quantity_discount',
                            'purchase_supplier_price',
                            'reduction_amount',
                            'reduction_amount_tax_incl',
                            'reduction_amount_tax_excl',
                            'unit_price_tax_incl',
                            'unit_price_tax_excl',
                            'original_product_price'

                        );
                        foreach ($fields as $field) {
                            $order_detail->{$field} = Tools::convertPriceFull($order_detail->{$field}, $old_currency, $currency);
                        }

                        $order_detail->update();
                        $order_detail->updateTaxAmount($order);
                    }

                    $id_order_carrier = (int)$order->getIdOrderCarrier();
                    if ($id_order_carrier) {
                        $order_carrier = $order_carrier = new OrderCarrier((int)$order->getIdOrderCarrier());
                        $order_carrier->shipping_cost_tax_excl = (float)Tools::convertPriceFull($order_carrier->shipping_cost_tax_excl, $old_currency, $currency);
                        $order_carrier->shipping_cost_tax_incl = (float)Tools::convertPriceFull($order_carrier->shipping_cost_tax_incl, $old_currency, $currency);
                        $order_carrier->update();
                    }

                    // Update order && order_invoice amount
                    $fields = array(
                        'total_discounts',
                        'total_discounts_tax_incl',
                        'total_discounts_tax_excl',
                        'total_discount_tax_excl',
                        'total_discount_tax_incl',
                        'total_paid',
                        'total_paid_tax_incl',
                        'total_paid_tax_excl',
                        'total_paid_real',
                        'total_products',
                        'total_products_wt',
                        'total_shipping',
                        'total_shipping_tax_incl',
                        'total_shipping_tax_excl',
                        'total_wrapping',
                        'total_wrapping_tax_incl',
                        'total_wrapping_tax_excl',
                    );

                    $invoices = $order->getInvoicesCollection();
                    if ($invoices) {
                        foreach ($invoices as $invoice) {
                            foreach ($fields as $field) {
                                if (isset($invoice->$field)) {
                                    $invoice->{$field} = Tools::convertPriceFull($invoice->{$field}, $old_currency, $currency);
                                }
                            }
                            $invoice->save();
                        }
                    }

                    foreach ($fields as $field) {
                        if (isset($order->$field)) {
                            $order->{$field} = Tools::convertPriceFull($order->{$field}, $old_currency, $currency);
                        }
                    }

                    // Update currency in order
                    $order->id_currency = $currency->id;
                    // Update exchange rate
                    $order->conversion_rate = (float)$currency->conversion_rate;
                    $order->update();
                } else {
                    $this->errors[] = Tools::displayError('You cannot change the currency.');
                }
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        } elseif (Tools::isSubmit('submitGenerateInvoice') && isset($order)) {
            if (!Configuration::get('PS_INVOICE', null, null, $order->id_shop)) {
                $this->errors[] = Tools::displayError('Invoice management has been disabled.');
            } elseif ($order->hasInvoice()) {
                $this->errors[] = Tools::displayError('This order already has an invoice.');
            } else {
                $order->setInvoice(true);
                Tools::redirectAdmin(self::$currentIndex.'&id_order='.$order->id.'&vieworder&conf=4&token='.$this->token);
            }
        } elseif (Tools::isSubmit('submitDeleteVoucher') && isset($order)) {
            if ($this->tabAccess['edit'] === '1') {
                $order_cart_rule = new OrderCartRule(Tools::getValue('id_order_cart_rule'));
                if (Validate::isLoadedObject($order_cart_rule) && $order_cart_rule->id_order == $order->id) {
                    if ($order_cart_rule->id_order_invoice) {
                        $order_invoice = new OrderInvoice($order_cart_rule->id_order_invoice);
                        if (!Validate::isLoadedObject($order_invoice)) {
                            throw new PrestaShopException('Can\'t load Order Invoice object');
                        }

                        // Update amounts of Order Invoice
                        $order_invoice->total_discount_tax_excl -= $order_cart_rule->value_tax_excl;
                        $order_invoice->total_discount_tax_incl -= $order_cart_rule->value;

                        $order_invoice->total_paid_tax_excl += $order_cart_rule->value_tax_excl;
                        $order_invoice->total_paid_tax_incl += $order_cart_rule->value;

                        // Update Order Invoice
                        $order_invoice->update();
                    }

                    // Update amounts of order
                    $order->total_discounts -= $order_cart_rule->value;
                    $order->total_discounts_tax_incl -= $order_cart_rule->value;
                    $order->total_discounts_tax_excl -= $order_cart_rule->value_tax_excl;

                    $order->total_paid += $order_cart_rule->value;
                    $order->total_paid_tax_incl += $order_cart_rule->value;
                    $order->total_paid_tax_excl += $order_cart_rule->value_tax_excl;

                    // Delete Order Cart Rule and update Order
                    $order_cart_rule->delete();
                    $order->update();
                    Tools::redirectAdmin(self::$currentIndex.'&id_order='.$order->id.'&vieworder&conf=4&token='.$this->token);
                } else {
                    $this->errors[] = Tools::displayError('You cannot edit this cart rule.');
                }
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        } elseif (Tools::isSubmit('submitNewVoucher') && isset($order)) {
            if ($this->tabAccess['edit'] === '1') {
                if (!Tools::getValue('discount_name')) {
                    $this->errors[] = Tools::displayError('You must specify a name in order to create a new discount.');
                } elseif ((float)Tools::getValue('discount_value') <= 0) {
                    $this->errors[] = Tools::displayError('The discount value is invalid.');
                } else {
                    if ($order->hasInvoice()) {
                        // If the discount is for only one invoice
                        if (!Tools::isSubmit('discount_all_invoices')) {
                            $order_invoice = new OrderInvoice(Tools::getValue('discount_invoice'));
                            if (!Validate::isLoadedObject($order_invoice)) {
                                throw new PrestaShopException('Can\'t load Order Invoice object');
                            }
                        }
                    }

                    $cart_rules = array();
                    $discount_value = (float)str_replace(',', '.', Tools::getValue('discount_value'));
                    switch (Tools::getValue('discount_type')) {
                        // Percent type
                        case 1:
                            if ($discount_value < 100) {
                                if (isset($order_invoice)) {
                                    $cart_rules[$order_invoice->id]['value_tax_incl'] = Tools::ps_round($order_invoice->total_paid_tax_incl * $discount_value / 100, 2);
                                    $cart_rules[$order_invoice->id]['value_tax_excl'] = Tools::ps_round($order_invoice->total_paid_tax_excl * $discount_value / 100, 2);

                                    // Update OrderInvoice
                                    $this->applyDiscountOnInvoice($order_invoice, $cart_rules[$order_invoice->id]['value_tax_incl'], $cart_rules[$order_invoice->id]['value_tax_excl']);
                                } elseif ($order->hasInvoice()) {
                                    $order_invoices_collection = $order->getInvoicesCollection();
                                    foreach ($order_invoices_collection as $order_invoice) {
                                        /** @var OrderInvoice $order_invoice */
                                        $cart_rules[$order_invoice->id]['value_tax_incl'] = Tools::ps_round($order_invoice->total_paid_tax_incl * $discount_value / 100, 2);
                                        $cart_rules[$order_invoice->id]['value_tax_excl'] = Tools::ps_round($order_invoice->total_paid_tax_excl * $discount_value / 100, 2);

                                        // Update OrderInvoice
                                        $this->applyDiscountOnInvoice($order_invoice, $cart_rules[$order_invoice->id]['value_tax_incl'], $cart_rules[$order_invoice->id]['value_tax_excl']);
                                    }
                                } else {
                                    $cart_rules[0]['value_tax_incl'] = Tools::ps_round($order->total_paid_tax_incl * $discount_value / 100, 2);
                                    $cart_rules[0]['value_tax_excl'] = Tools::ps_round($order->total_paid_tax_excl * $discount_value / 100, 2);
                                }
                            } else {
                                $this->errors[] = Tools::displayError('The discount value is invalid.');
                            }
                            break;
                        // Amount type
                        case 2:
                            if (isset($order_invoice)) {
                                if ($discount_value > $order_invoice->total_paid_tax_incl) {
                                    $this->errors[] = Tools::displayError('The discount value is greater than the order invoice total.');
                                } else {
                                    $cart_rules[$order_invoice->id]['value_tax_incl'] = Tools::ps_round($discount_value, 2);
                                    $cart_rules[$order_invoice->id]['value_tax_excl'] = Tools::ps_round($discount_value / (1 + ($order->getTaxesAverageUsed() / 100)), 2);

                                    // Update OrderInvoice
                                    $this->applyDiscountOnInvoice($order_invoice, $cart_rules[$order_invoice->id]['value_tax_incl'], $cart_rules[$order_invoice->id]['value_tax_excl']);
                                }
                            } elseif ($order->hasInvoice()) {
                                $order_invoices_collection = $order->getInvoicesCollection();
                                foreach ($order_invoices_collection as $order_invoice) {
                                    /** @var OrderInvoice $order_invoice */
                                    if ($discount_value > $order_invoice->total_paid_tax_incl) {
                                        $this->errors[] = Tools::displayError('The discount value is greater than the order invoice total.').$order_invoice->getInvoiceNumberFormatted(Context::getContext()->language->id, (int)$order->id_shop).')';
                                    } else {
                                        $cart_rules[$order_invoice->id]['value_tax_incl'] = Tools::ps_round($discount_value, 2);
                                        $cart_rules[$order_invoice->id]['value_tax_excl'] = Tools::ps_round($discount_value / (1 + ($order->getTaxesAverageUsed() / 100)), 2);

                                        // Update OrderInvoice
                                        $this->applyDiscountOnInvoice($order_invoice, $cart_rules[$order_invoice->id]['value_tax_incl'], $cart_rules[$order_invoice->id]['value_tax_excl']);
                                    }
                                }
                            } else {
                                if ($discount_value > $order->total_paid_tax_incl) {
                                    $this->errors[] = Tools::displayError('The discount value is greater than the order total.');
                                } else {
                                    $cart_rules[0]['value_tax_incl'] = Tools::ps_round($discount_value, 2);
                                    $cart_rules[0]['value_tax_excl'] = Tools::ps_round($discount_value / (1 + ($order->getTaxesAverageUsed() / 100)), 2);
                                }
                            }
                            break;
                        // Free shipping type
                        case 3:
                            if (isset($order_invoice)) {
                                if ($order_invoice->total_shipping_tax_incl > 0) {
                                    $cart_rules[$order_invoice->id]['value_tax_incl'] = $order_invoice->total_shipping_tax_incl;
                                    $cart_rules[$order_invoice->id]['value_tax_excl'] = $order_invoice->total_shipping_tax_excl;

                                    // Update OrderInvoice
                                    $this->applyDiscountOnInvoice($order_invoice, $cart_rules[$order_invoice->id]['value_tax_incl'], $cart_rules[$order_invoice->id]['value_tax_excl']);
                                }
                            } elseif ($order->hasInvoice()) {
                                $order_invoices_collection = $order->getInvoicesCollection();
                                foreach ($order_invoices_collection as $order_invoice) {
                                    /** @var OrderInvoice $order_invoice */
                                    if ($order_invoice->total_shipping_tax_incl <= 0) {
                                        continue;
                                    }
                                    $cart_rules[$order_invoice->id]['value_tax_incl'] = $order_invoice->total_shipping_tax_incl;
                                    $cart_rules[$order_invoice->id]['value_tax_excl'] = $order_invoice->total_shipping_tax_excl;

                                    // Update OrderInvoice
                                    $this->applyDiscountOnInvoice($order_invoice, $cart_rules[$order_invoice->id]['value_tax_incl'], $cart_rules[$order_invoice->id]['value_tax_excl']);
                                }
                            } else {
                                $cart_rules[0]['value_tax_incl'] = $order->total_shipping_tax_incl;
                                $cart_rules[0]['value_tax_excl'] = $order->total_shipping_tax_excl;
                            }
                            break;
                        default:
                            $this->errors[] = Tools::displayError('The discount type is invalid.');
                    }

                    $res = true;
                    foreach ($cart_rules as &$cart_rule) {
                        $cartRuleObj = new CartRule();
                        $cartRuleObj->date_from = date('Y-m-d H:i:s', strtotime('-1 hour', strtotime($order->date_add)));
                        $cartRuleObj->date_to = date('Y-m-d H:i:s', strtotime('+1 hour'));
                        $cartRuleObj->name[Configuration::get('PS_LANG_DEFAULT')] = Tools::getValue('discount_name');
                        $cartRuleObj->quantity = 0;
                        $cartRuleObj->quantity_per_user = 1;
                        if (Tools::getValue('discount_type') == 1) {
                            $cartRuleObj->reduction_percent = $discount_value;
                        } elseif (Tools::getValue('discount_type') == 2) {
                            $cartRuleObj->reduction_amount = $cart_rule['value_tax_excl'];
                        } elseif (Tools::getValue('discount_type') == 3) {
                            $cartRuleObj->free_shipping = 1;
                        }
                        $cartRuleObj->active = 0;
                        if ($res = $cartRuleObj->add()) {
                            $cart_rule['id'] = $cartRuleObj->id;
                            $cart_rule['free_shipping'] = $cartRuleObj->free_shipping;
                        } else {
                            break;
                        }
                    }

                    if ($res) {
                        foreach ($cart_rules as $id_order_invoice => $cart_rule) {
                            // Create OrderCartRule
                            $order_cart_rule = new OrderCartRule();
                            $order_cart_rule->id_order = $order->id;
                            $order_cart_rule->id_cart_rule = $cart_rule['id'];
                            $order_cart_rule->id_order_invoice = $id_order_invoice;
                            $order_cart_rule->name = Tools::getValue('discount_name');
                            $order_cart_rule->value = $cart_rule['value_tax_incl'];
                            $order_cart_rule->value_tax_excl = $cart_rule['value_tax_excl'];
                            $order_cart_rule->free_shipping = $cart_rule['free_shipping'];
                            $res &= $order_cart_rule->add();

                            $order->total_discounts += $order_cart_rule->value;
                            $order->total_discounts_tax_incl += $order_cart_rule->value;
                            $order->total_discounts_tax_excl += $order_cart_rule->value_tax_excl;
                            $order->total_paid -= $order_cart_rule->value;
                            $order->total_paid_tax_incl -= $order_cart_rule->value;
                            $order->total_paid_tax_excl -= $order_cart_rule->value_tax_excl;
                        }

                        // Update Order
                        $res &= $order->update();
                    }

                    if ($res) {
                        Tools::redirectAdmin(self::$currentIndex.'&id_order='.$order->id.'&vieworder&conf=4&token='.$this->token);
                    } else {
                        $this->errors[] = Tools::displayError('An error occurred during the OrderCartRule creation');
                    }
                }
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        } elseif (Tools::isSubmit('sendStateEmail') && Tools::getValue('sendStateEmail') > 0 && Tools::getValue('id_order') > 0) {
            if ($this->tabAccess['edit'] === '1') {
                $order_state = new OrderState((int)Tools::getValue('sendStateEmail'));

                if (!Validate::isLoadedObject($order_state)) {
                    $this->errors[] = Tools::displayError('An error occurred while loading order status.');
                } else {
                    $history = new OrderHistory((int)Tools::getValue('id_order_history'));

                    $carrier = new Carrier($order->id_carrier, $order->id_lang);
                    $templateVars = array();
                    if ($order_state->id == Configuration::get('PS_OS_SHIPPING') && $order->shipping_number) {
                        $templateVars = array('{followup}' => str_replace('@', $order->shipping_number, $carrier->url));
                    }

                    if ($history->sendEmail($order, $templateVars)) {
                        Tools::redirectAdmin(self::$currentIndex.'&id_order='.$order->id.'&vieworder&conf=10&token='.$this->token);
                    } else {
                        $this->errors[] = Tools::displayError('An error occurred while sending the e-mail to the customer.');
                    }
                }
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        }

        parent::postProcess();
    }

    public function renderKpis()
    {
        $time = time();
        $kpis = array();

        /* The data generation is located in AdminStatsControllerCore */

        $helper = new HelperKpi();
        $helper->id = 'box-conversion-rate';
        $helper->icon = 'icon-sort-by-attributes-alt';
        //$helper->chart = true;
        $helper->color = 'color1';
        $helper->title = $this->l('Conversion Rate', null, null, false);
        $helper->subtitle = $this->l('30 days', null, null, false);
        if (ConfigurationKPI::get('CONVERSION_RATE') !== false) {
            $helper->value = ConfigurationKPI::get('CONVERSION_RATE');
        }
        if (ConfigurationKPI::get('CONVERSION_RATE_CHART') !== false) {
            $helper->data = ConfigurationKPI::get('CONVERSION_RATE_CHART');
        }
        $helper->source = $this->context->link->getAdminLink('AdminStats').'&ajax=1&action=getKpi&kpi=conversion_rate';
        $helper->refresh = (bool)(ConfigurationKPI::get('CONVERSION_RATE_EXPIRE') < $time);
        $kpis[] = $helper->generate();

        $helper = new HelperKpi();
        $helper->id = 'box-carts';
        $helper->icon = 'icon-shopping-cart';
        $helper->color = 'color2';
        $helper->title = $this->l('Abandoned Carts', null, null, false);
        $helper->subtitle = $this->l('Today', null, null, false);
        $helper->href = $this->context->link->getAdminLink('AdminCarts').'&action=filterOnlyAbandonedCarts';
        if (ConfigurationKPI::get('ABANDONED_CARTS') !== false) {
            $helper->value = ConfigurationKPI::get('ABANDONED_CARTS');
        }
        $helper->source = $this->context->link->getAdminLink('AdminStats').'&ajax=1&action=getKpi&kpi=abandoned_cart';
        $helper->refresh = (bool)(ConfigurationKPI::get('ABANDONED_CARTS_EXPIRE') < $time);
        $kpis[] = $helper->generate();

        $helper = new HelperKpi();
        $helper->id = 'box-average-order';
        $helper->icon = 'icon-money';
        $helper->color = 'color3';
        $helper->title = $this->l('Average Order Value', null, null, false);
        $helper->subtitle = $this->l('30 days', null, null, false);
        if (ConfigurationKPI::get('AVG_ORDER_VALUE') !== false) {
            $helper->value = sprintf($this->l('%s tax excl.'), ConfigurationKPI::get('AVG_ORDER_VALUE'));
        }
        $helper->source = $this->context->link->getAdminLink('AdminStats').'&ajax=1&action=getKpi&kpi=average_order_value';
        $helper->refresh = (bool)(ConfigurationKPI::get('AVG_ORDER_VALUE_EXPIRE') < $time);
        $kpis[] = $helper->generate();

        $helper = new HelperKpi();
        $helper->id = 'box-net-profit-visit';
        $helper->icon = 'icon-user';
        $helper->color = 'color4';
        $helper->title = $this->l('Net Profit per Visit', null, null, false);
        $helper->subtitle = $this->l('30 days', null, null, false);
        if (ConfigurationKPI::get('NETPROFIT_VISIT') !== false) {
            $helper->value = ConfigurationKPI::get('NETPROFIT_VISIT');
        }
        $helper->source = $this->context->link->getAdminLink('AdminStats').'&ajax=1&action=getKpi&kpi=netprofit_visit';
        $helper->refresh = (bool)(ConfigurationKPI::get('NETPROFIT_VISIT_EXPIRE') < $time);
        $kpis[] = $helper->generate();

        $helper = new HelperKpiRow();
        $helper->kpis = $kpis;
        return $helper->generate();
    }

    public function renderView()
    {
        $order = new Order(Tools::getValue('id_order'));
        if (!Validate::isLoadedObject($order)) {
            $this->errors[] = Tools::displayError('The order cannot be found within your database.');
        }

        $customer = new Customer($order->id_customer);
        $carrier = new Carrier($order->id_carrier);
        $products = $this->getProductsModificata($order);
        $currency = new Currency((int)$order->id_currency);
        // Carrier module call
        $carrier_module_call = null;
        if ($carrier->is_module) {
            $module = Module::getInstanceByName($carrier->external_module_name);
            if (method_exists($module, 'displayInfoByCart')) {
                $carrier_module_call = call_user_func(array($module, 'displayInfoByCart'), $order->id_cart);
            }
        }

        // Retrieve addresses information
        $addressInvoice = new Address($order->id_address_invoice, $this->context->language->id);
        if (Validate::isLoadedObject($addressInvoice) && $addressInvoice->id_state) {
            $invoiceState = new State((int)$addressInvoice->id_state);
        }

        if ($order->id_address_invoice == $order->id_address_delivery) {
            $addressDelivery = $addressInvoice;
            if (isset($invoiceState)) {
                $deliveryState = $invoiceState;
            }
        } else {
            $addressDelivery = new Address($order->id_address_delivery, $this->context->language->id);
            if (Validate::isLoadedObject($addressDelivery) && $addressDelivery->id_state) {
                $deliveryState = new State((int)($addressDelivery->id_state));
            }
        }

        $this->toolbar_title = sprintf($this->l('Order #%1$d (%2$s) - %3$s %4$s'), $order->id, $order->reference, $customer->firstname, $customer->lastname);
        if (Shop::isFeatureActive()) {
            $shop = new Shop((int)$order->id_shop);
            $this->toolbar_title .= ' - '.sprintf($this->l('Shop: %s'), $shop->name);
        }

        // gets warehouses to ship products, if and only if advanced stock management is activated
        $warehouse_list = null;

        $order_details = $order->getOrderDetailList();
        foreach ($order_details as $order_detail) {
            $product = new Product($order_detail['product_id']);

            if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')
                && $product->advanced_stock_management) {
                $warehouses = Warehouse::getWarehousesByProductId($order_detail['product_id'], $order_detail['product_attribute_id']);
                foreach ($warehouses as $warehouse) {
                    if (!isset($warehouse_list[$warehouse['id_warehouse']])) {
                        $warehouse_list[$warehouse['id_warehouse']] = $warehouse;
                    }
                }
            }
        }

        $payment_methods = array();
        foreach (PaymentModule::getInstalledPaymentModules() as $payment) {
            $module = Module::getInstanceByName($payment['name']);
            if (Validate::isLoadedObject($module) && $module->active) {
                $payment_methods[] = $module->displayName;
            }
        }

        // display warning if there are products out of stock
        $display_out_of_stock_warning = false;
        $current_order_state = $order->getCurrentOrderState();
        if (Configuration::get('PS_STOCK_MANAGEMENT') && (!Validate::isLoadedObject($current_order_state) || ($current_order_state->delivery != 1 && $current_order_state->shipped != 1))) {
            $display_out_of_stock_warning = true;
        }

        // products current stock (from stock_available)
        foreach ($products as &$product) {
            // Get total customized quantity for current product
            $customized_product_quantity = 0;

            if (is_array($product['customizedDatas'])) {
                foreach ($product['customizedDatas'] as $customizationPerAddress) {
                    foreach ($customizationPerAddress as $customizationId => $customization) {
                        $customized_product_quantity += (int)$customization['quantity'];
                    }
                }
            }

            $product['customized_product_quantity'] = $customized_product_quantity;
            $product['current_stock'] = StockAvailable::getQuantityAvailableByProduct($product['product_id'], $product['product_attribute_id'], $product['id_shop']);
            $resume = OrderSlip::getProductSlipResume($product['id_order_detail']);
            $product['quantity_refundable'] = $product['product_quantity'] - $resume['product_quantity'];
            $product['amount_refundable'] = $product['total_price_tax_excl'] - $resume['amount_tax_excl'];
            $product['amount_refundable_tax_incl'] = $product['total_price_tax_incl'] - $resume['amount_tax_incl'];
            $product['amount_refund'] = Tools::displayPrice($resume['amount_tax_incl'], $currency);
            $product['refund_history'] = OrderSlip::getProductSlipDetail($product['id_order_detail']);
            $product['return_history'] = OrderReturn::getProductReturnDetail($product['id_order_detail']);

            // if the current stock requires a warning
            if ($product['current_stock'] <= 0 && $display_out_of_stock_warning) {
                $this->displayWarning($this->l('This product is out of stock: ').' '.$product['product_name']);
            }
            if ($product['id_warehouse'] != 0) {
                $warehouse = new Warehouse((int)$product['id_warehouse']);
                $product['warehouse_name'] = $warehouse->name;
                $warehouse_location = WarehouseProductLocation::getProductLocation($product['product_id'], $product['product_attribute_id'], $product['id_warehouse']);
                if (!empty($warehouse_location)) {
                    $product['warehouse_location'] = $warehouse_location;
                } else {
                    $product['warehouse_location'] = false;
                }
            } else {
                $product['warehouse_name'] = '--';
                $product['warehouse_location'] = false;
            }
        }

        $gender = new Gender((int)$customer->id_gender, $this->context->language->id);

        $history = $order->getHistory($this->context->language->id);

        foreach ($history as &$order_state) {
            $order_state['text-color'] = Tools::getBrightness($order_state['color']) < 128 ? 'white' : 'black';
        }

        /* OVERRIDE */

        $is_agente = ($this->context->employee->id_profile == 7 ? true : false); // true: l'employee loggato è un agente
        $not_mio_agente = (($this->context->employee->id_profile == 7 && $agente_c != $this->context->employee->id) ? true : false); // true: l'employee loggato è un agente e non è l'agente assegnato al cliente
        $permessi_verifica = (($this->context->employee->id == 2 || $this->context->employee->id == 7 || $this->context->employee->id == 12 || $this->context->employee->id == 14 || $this->context->employee->id == 19 || $this->context->employee->id == 21) ? true : false);
        // $is_pa = ($customer->id_default_group == 5 ? true : false); // true: il cliente ha profilo P.A.

        $riassunto_on = true; // false: non carica la tab riassuntiva dei dati del cliente
        $attivita_recenti_on = false; // false: non carica la tab riassuntiva delle ultime attività

        $id_gruppo = Customer::getDefaultGroupId($customer->id);
        $gruppo = new Group($id_gruppo);
        $profilo = $gruppo->name[$this->context->language->id];

        /* INIZIO Riassunto dati cliente */
        // CORREGGERE! Aggiungere $numero_ordini
        if($riassunto_on && !$not_mio_agente){

            $company_or_name = ($customer->is_company ? $customer->company : $customer->firstname.' '.$customer->lastname);
            $codici = $customer->id.' / '.$customer->codice_esolver;

            $acquisti_ultimo_anno = Db::getInstance()->getValue('
                SELECT count(id_order) 
                FROM '._DB_PREFIX_.'orders 
                WHERE id_customer = '.$customer->id.' 
                    AND date_add BETWEEN date_sub(curdate(),interval 365 day) 
                    AND date_sub(curdate(),interval -1 day)
            ');
                
            $preventivi_ultimo_anno = Db::getInstance()->getValue('
                SELECT count(id_cart)  
                FROM '._DB_PREFIX_.'cart 
                WHERE id_customer = '.$customer->id.' 
                    AND date_add BETWEEN date_sub(curdate(),interval 365 day) 
                    AND date_sub(curdate(),interval -1 day)
            ');
            
            $ric_prev_ultimo_anno = Db::getInstance()->getValue('
                SELECT count(id_thread)    
                FROM form_prevendita_thread 
                WHERE id_customer = '.$customer->id.' 
                    AND date_add BETWEEN date_sub(curdate(),interval 365 day) 
                    AND date_sub(curdate(),interval -1 day)
            ');
            
            $todo_ultimo_anno = Db::getInstance()->getValue('
                SELECT count(id_action)    
                FROM action_thread 
                WHERE id_customer = '.$customer->id.' 
                    AND date_add BETWEEN date_sub(curdate(),interval 365 day) 
                    AND date_sub(curdate(),interval -1 day)
            ');
            
            $ticket_ultimo_anno = Db::getInstance()->getValue('
                SELECT count(id_customer_thread)   
                FROM '._DB_PREFIX_.'customer_thread 
                WHERE id_customer = '.$customer->id.' 
                    AND date_add BETWEEN date_sub(curdate(),interval 365 day) 
                    AND date_sub(curdate(),interval -1 day)
            ');

            if($acquisti_ultimo_anno >= 3){
                $tipo_profilo = 'Fedele';
                $tipo_profilo_title = 'Almeno tre acquisti nell\'anno';
            }
            else if ($acquisti_ultimo_anno > 0 && $acquisti_ultimo_anno < 3){
                $tipo_profilo = 'Attivo';
                $tipo_profilo_title = 'Almeno un acquisto nell\'anno';
            }  
            else
            {
                if($preventivi_ultimo_anno > 0){
                    $tipo_profilo = 'Prospect';
                    $tipo_profilo_title = 'Non ha ancora acquistato quest\'anno, ma ha un preventivo o un carrello';
                }
                else{
                    if($ric_prev_ultimo_anno > 0 || $todo_ultimo_anno > 0 || $ticket_ultimo_anno > 0){
                        $tipo_profilo = 'Lead';
                        $tipo_profilo_title = 'Non ha acquistato né chiesto preventivi quest\'anno, ma ci ha contattato o ci sono attivit&agrave; in corso';
                    }
                    else{
                        $tipo_profilo = 'Inattivo';
                        $tipo_profilo_title = 'Non acquista da almeno un anno';
                    }  
                }
            }
            
            //$profilo = $groups[0]['name'];
            $tipo_cliente = ($customer->is_company ? 'Azienda' : 'Privato');
            switch ($customer->active) {
                case 0:
                    $status = 'Disattivato';
                    break;
                case 1:
                    $status = 'Attivo';
                    break;
                case 2:
                    $status = 'Cessata attività';
                    break;
                
                default:
                    $status = 'Valore non valido';
                    break;
            }

            $referenti = Db::getInstance()->executeS('
                SELECT id_persona as id, CONCAT(firstname, " ", lastname) as name 
                FROM persone 
                WHERE id_customer = '.$customer->id.'
            ');

            $referente_c = Db::getInstance()->getValue('
                SELECT referente 
                FROM '._DB_PREFIX_.'customer 
                WHERE id_customer = '.$customer->id
            );
			
			if(!$referente_c || $referente_c == 0) 
			{
				$referente_c = Db::getInstance()->getValue('
                    SELECT id_persona as id
                    FROM persone 
                    WHERE id_customer = '.$customer->id.' 
                        AND firstname = "'.$customer->firstname.'" 
                        AND lastname = "'.$customer->lastname.'"
                ');
                // UPDATE DISABILITATO PER ORA
                /* executeS() andrebbe usata solo con SELECT; 
                per INSERT, UPDATE, DELETE usare execute() oppure insert(), update(), delete() */

				/*Db::getInstance()->executeS('
                    UPDATE '._DB_PREFIX_.'customer 
                    SET referente = '.$referente_c.' 
                    WHERE id_customer = '.$customer->id.'
                ');*/
            }

            /* correggere: migliorabile con le modifiche db fatte? */
            // Unknown se non ci sono indirizzi di fatturazione attivi; gestire il caso o non può mai succedere?
            $provincia = Db::getInstance()->getValue('SELECT id_state FROM '._DB_PREFIX_.'address WHERE fatturazione = 1 AND deleted = 0 AND active = 1 AND id_customer = '.$customer->id); 
            if(!$provincia)
                $prov_text = 'Unknown';
            else
                $prov_text = Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'state WHERE id_state = '.$provincia);
            
            $nazione = Db::getInstance()->getValue('SELECT id_country FROM '._DB_PREFIX_.'address WHERE fatturazione = 1 AND deleted = 0 AND active = 1 AND id_customer = '.$customer->id);
            if(!$nazione)
                $naz_text = 'Unknown';
            else  
                $naz_text = Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'country WHERE id_country = '.$nazione);
            
            $zona = ($prov_text == 'Unknown' ? $naz_text : $naz_text.'-'.$prov_text);
            
            $ordini_totali = Db::getInstance()->getValue('
                SELECT SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) tot 
                FROM '._DB_PREFIX_.'order_detail 
                JOIN '._DB_PREFIX_.'orders ON orders.id_order = order_detail.id_order 
                LEFT JOIN '._DB_PREFIX_.'order_history oh ON oh.id_order_state = 6 AND oh.id_order = orders.id_order 
                WHERE oh.id_order_state IS NULL  
                    AND (orders.valid = 1 OR 
                        (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment"))
                    ) 
                    AND orders.id_customer = '.$customer->id.'
            ');
            if(!$ordini_totali)
                $ordini_totali = 0;
			
			$ordini_ultimi_12_mesi = Db::getInstance()->getValue('
                SELECT SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) tot 
                FROM '._DB_PREFIX_.'order_detail 
                JOIN '._DB_PREFIX_.'orders ON orders.id_order = order_detail.id_order 
                LEFT JOIN '._DB_PREFIX_.'order_history oh ON oh.id_order_state =6 AND oh.id_order = orders.id_order 
                WHERE oh.id_order_state IS NULL  
                    AND (orders.valid = 1 OR 
                        (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment"))
                    ) 
                    AND orders.id_customer = '.$customer->id.' 
                    AND orders.date_add BETWEEN date_sub(curdate(),interval 365 day) AND date_sub(curdate(),interval 0 day)
            ');
            if(!$ordini_ultimi_12_mesi)
                $ordini_ultimi_12_mesi = 0;
			
			$ordini_ultimi_6_mesi = Db::getInstance()->getValue('
                SELECT SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) tot 
                FROM '._DB_PREFIX_.'order_detail 
                JOIN '._DB_PREFIX_.'orders ON orders.id_order = order_detail.id_order 
                LEFT JOIN '._DB_PREFIX_.'order_history oh ON oh.id_order_state =6 AND oh.id_order = orders.id_order 
                WHERE oh.id_order_state IS NULL  
                    AND (orders.valid = 1 OR 
                        (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment"))
                    ) 
                    AND orders.id_customer = '.$customer->id.' 
                    AND orders.date_add BETWEEN date_sub(curdate(),interval 180 day) AND date_sub(curdate(),interval 0 day)
            ');
            if(!$ordini_ultimi_6_mesi)
                $ordini_ultimi_6_mesi = 0;

            $data_ultimo_acquisto = Db::getInstance()->getValue('
                SELECT data_fattura 
                FROM fattura 
                WHERE id_customer = '.$customer->id.' 
                ORDER BY data_fattura DESC
            ');

            $impiegati = Db::getInstance()->executeS('SELECT id_employee as id, CONCAT(firstname," ",LEFT(lastname, 1),".") AS name FROM '._DB_PREFIX_.'employee WHERE active = 1 ORDER BY firstname ASC, lastname ASC');
            //$tecnici = Db::getInstance()->executeS('SELECT id_employee as id, CONCAT(firstname," ",LEFT(lastname, 1),".") AS name FROM '._DB_PREFIX_.'employee WHERE id_employee = 4 OR id_employee = 5 OR id_employee = 12 OR id_employee = 17 ORDER BY firstname ASC, lastname ASC');
            // correggere: togliere id_employee = 6 finiti gli employee; la riga sopra è quella giusta
            $tecnici = Db::getInstance()->executeS('SELECT id_employee as id, CONCAT(firstname," ",LEFT(lastname, 1),".") AS name FROM '._DB_PREFIX_.'employee WHERE id_employee = 4 OR id_employee = 5 OR id_employee = 6 ORDER BY firstname ASC, lastname ASC');

            $query_amministrazione = '
                SELECT *
                FROM customer_amministrazione
                WHERE id_customer = '.$customer->id.'
            ';
            $amministrazione_c = Db::getInstance()->getRow($query_amministrazione);

            $query_agente = '
                SELECT id_employee as id, CONCAT(firstname, " ", LEFT(lastname,1), ".") as name
                FROM '._DB_PREFIX_.'employee
                WHERE id_employee = '.$amministrazione_c['agente'].'
            ';
            $agente_c = Db::getInstance()->getRow($query_agente);

            /* // Manca nome agente
            if(!$agente_c || $agente_c == 0) {
                // Se il cliente non ha un agente, considero l'ultimo impiegato che ha fatto azioni su di lui
                // correggere query: perchè doppio select???
                $agente_c = Db::getInstance()->getValue('SELECT id_employee FROM (SELECT id_thread, id_employee FROM form_prevendita_thread WHERE id_customer = '.$customer->id.') au ORDER BY id_thread DESC');
                if(!$agente_c || $agente_c == 0) {	
                    if($customer->id_default_group == 3 || $customer->id_default_group == 5 || $customer->id_default_group == 15 || $customer->id_default_group == 8 || $customer->id_default_group == 19)
                        $agente_c = 1; // Ezio
                    else
                        $agente_c = Db::getInstance()->getValue('SELECT id_employee FROM (SELECT * FROM cart WHERE id_customer = '.$customer->id.') au ORDER BY id_cart DESC');
                }	
            }
            */

            $query_tecnico = '
                SELECT id_employee as id, CONCAT(firstname, " ", LEFT(lastname,1), ".") as name
                FROM '._DB_PREFIX_.'employee
                WHERE id_employee = '.$amministrazione_c['tecnico'].'
            ';
            $tecnico_c = Db::getInstance()->getRow($query_tecnico);
            // Assegnare un tecnico default (quello con + azioni sul cliente) -> basta visualizzarlo, o devo fare update nel db di tutti i clienti?
            // Forse meglio fare update del singolo cliente nel momento in cui entro nell'anagrafica e lo trovo vuoto
            // $tecnico_default = ...; 
            
            /* // Manca nome tecnico            
            if(!$tecnico_c || $tecnico_c == 0) {
                // Se il cliente non ha un tecnico, considero l'ultimo impiegato che ha fatto azioni su di lui
                $tecnico_c = Db::getInstance()->getValue('SELECT id_employee FROM (SELECT * FROM '._DB_PREFIX_.'customer_thread WHERE id_customer = '.$customer->id.') au ORDER BY id_customer_thread DESC');
                if(!$tecnico_c || $tecnico_c == 0) 
                    $tecnico_c = Db::getInstance()->getValue('SELECT action_to FROM (SELECT * FROM action_thread WHERE id_customer = '.$customer->id.') au ORDER BY id_action DESC');
            }
            */


            // RIGA 3

            $r3_visite = Db::getInstance()->getValue('
                SELECT COUNT(conn.id_connections) as Visite
                FROM '._DB_PREFIX_.'connections conn 
                LEFT JOIN '._DB_PREFIX_.'guest g ON conn.id_guest = g.id_guest 
                LEFT JOIN '._DB_PREFIX_.'customer c ON g.id_customer = c.id_customer 
                WHERE g.id_customer = '.$customer->id.'
            ');
            
            $r3_pagine = Db::getInstance()->getValue('
                SELECT
                    COUNT(a.id_connections) as Pagine
                FROM (
                    SELECT DISTINCT conn.id_connections, cp.id_page 
                    FROM '._DB_PREFIX_.'connections conn 
                    LEFT JOIN '._DB_PREFIX_.'guest g ON conn.id_guest = g.id_guest 
                    LEFT JOIN '._DB_PREFIX_.'customer c ON g.id_customer = c.id_customer 
                    LEFT JOIN '._DB_PREFIX_.'connections_page cp ON cp.id_connections = conn.id_connections 
                    WHERE g.id_customer = '.$customer->id.'
                ) a
            ');
            
            $r3_prodotti = Db::getInstance()->executeS('
                SELECT pl.name AS Nome, CONCAT(pr.reference," (", IF(LEFT(c.date_add,10) = CURRENT_DATE(), "Oggi", IF(YEAR(c.date_add) = YEAR(CURRENT_DATE()), DATE_FORMAT(LEFT(c.date_add,10), "%d/%m"), DATE_FORMAT(LEFT(c.date_add,10), "%d/%m/%Y"))),")") AS Codice
                FROM '._DB_PREFIX_.'connections c 
                LEFT JOIN '._DB_PREFIX_.'guest g ON c.id_guest = g.id_guest 
                LEFT JOIN '._DB_PREFIX_.'customer cu ON g.id_customer = cu.id_customer 
                LEFT JOIN '._DB_PREFIX_.'connections_page cp ON cp.id_connections = c.id_connections 
                LEFT JOIN '._DB_PREFIX_.'page p ON p.id_page = cp.id_page 
                LEFT JOIN '._DB_PREFIX_.'page_type pt ON pt.id_page_type = p.id_page_type
                LEFT JOIN '._DB_PREFIX_.'product pr ON pr.id_product = p.id_object
                LEFT JOIN '._DB_PREFIX_.'product_lang pl ON pr.id_product = pl.id_product
                WHERE g.id_customer = '.$customer->id.'
                    AND pt.name = "product.php"
                ORDER BY c.date_add DESC
                LIMIT 1
            ');
            
            // TUTTO IL RESTO FUORI DAL FOREACH PERCHE' LA QUERY RESTITUISCE UNA SOLA ROW
            foreach($r3_prodotti as $r_prodotto){
                $r3_prodotti_c = $r_prodotto['Codice'];
                $r3_prodotti_n = $r_prodotto['Nome'];
            }

            $r3_ch_ricevuta = Db::getInstance()->executeS('
                SELECT LEFT(datetime,16) as data, extfield1, extfield2 
                FROM telefonate 
                WHERE id_customer = '.$customer->id.' 
                    AND calltype = "Inbound" ORDER BY datetime DESC LIMIT 1
            ');
            
            if(count($r3_ch_ricevuta)>0){
                foreach($r3_ch_ricevuta as $ricevuta){
                    $ric_date = $ricevuta['data'];
                    $ric_da = $ricevuta['extfield2'];
                }
            }
            
            $ric_date1 = new DateTime($ric_date);
            
            if($ric_date1->format('Y-m-d') == date("Y-m-d"))
                $ric_date = 'Oggi '.$ric_date1->format('H:i');
            else if($ric_date1->format('Y') == date("Y"))
                $ric_date = $ric_date1->format('d/m');
            else
                $ric_date = $ric_date1->format('d/m/Y');
            // TROVA RECORD CHE NON ESISTONO??????
            $r3_ch_inviata = Db::getInstance()->executeS('
                SELECT LEFT(datetime,16) as data, extfield1, extfield2 
                FROM telefonate
                WHERE id_customer = '.$customer->id.' 
                    AND calltype = "Outbound" ORDER BY datetime DESC LIMIT 1
            ');
            
            if(count($r3_ch_inviata)>0){
                foreach($r3_ch_inviata as $inviata){
                    $inv_date = $inviata['data'];
                    $inv_da = $inviata['extfield1'];
                }
            }
            
            $inv_date1 = new DateTime($inv_date);
            
            if($inv_date1->format('Y-m-d') == date("Y-m-d"))
                $inv_date = 'Oggi '.$inv_date1->format('H:i');
            else if($inv_date1->format('Y') == date("Y"))
                $inv_date = $inv_date1->format('d/m');
            else
                $inv_date = $inv_date1->format('d/m/Y');
            // NEL TPL STAMPA ORA DEL REFRESH PAGINA??????
            $r3_azione = Db::getInstance()->executeS('
                SELECT a.tipo, a.tecnico, a.data 
                FROM (
                    (SELECT "TO DO" as tipo, act.id_action as id, e.firstname as tecnico, act.date_upd as data FROM action_thread act JOIN '._DB_PREFIX_.'employee e on act.action_to = e.id_employee WHERE act.id_customer = '.$customer->id.' and (e.id_employee = 4 OR e.id_employee = 5 OR e.id_employee = 12 OR e.id_employee = 17) ORDER BY act.date_upd DESC)
                    UNION
                    (SELECT "Preventivo" as tipo, ft.id_thread, e.firstname, ft.date_upd FROM form_prevendita_thread ft JOIN '._DB_PREFIX_.'employee e on ft.id_employee = e.id_employee WHERE ft.id_customer = '.$customer->id.' and (e.id_employee = 4 OR e.id_employee = 5 OR e.id_employee = 12 OR e.id_employee = 17) order by ft.date_upd desc)
                    UNION
                    (SELECT "Ticket" as tipo, cm.id_customer_thread, e.firstname, cm.date_add FROM '._DB_PREFIX_.'`customer_thread` ct inner JOIN '._DB_PREFIX_.'customer_message cm on ct.id_customer_thread = cm.id_customer_thread inner join employee e on cm.id_employee = e.id_employee where cm.id_employee!=0 and ct.id_customer = '.$customer->id.' and (e.id_employee = 4 OR e.id_employee = 5 OR e.id_employee = 12 OR e.id_employee = 17) group by cm.id_customer_thread order by cm.date_add desc)
                    UNION
                    (SELECT "Carrello creato" as tipo, c.id_cart, e.firstname, c.date_add FROM '._DB_PREFIX_.'cart c JOIN '._DB_PREFIX_.'employee e on c.created_by = e.id_employee WHERE c.id_customer = '.$customer->id.' and (e.id_employee = 4 OR e.id_employee = 5 OR e.id_employee = 12 OR e.id_employee = 17) ORDER BY c.date_add desc)
                    UNION
                    (SELECT "Carrello aggiornato" as tipo, c.id_cart, e.firstname, c.date_upd FROM '._DB_PREFIX_.'cart c JOIN '._DB_PREFIX_.'employee e on c.id_employee = e.id_employee WHERE c.created_by = 0 and c.id_customer = '.$customer->id.' and (e.id_employee = 4 OR e.id_employee = 5 OR e.id_employee = 12 OR e.id_employee = 17) ORDER BY c.date_upd desc)
                    ) a
                ORDER BY data desc
                LIMIT 1
            ');
            
            // TUTTO IL RESTO FUORI DAL FOREACH PERCHE' LA QUERY RESTITUISCE UNA SOLA ROW
            foreach($r3_azione as $ultima_azione){
                $r3_azione_tipo = $ultima_azione['tipo'];
                $r3_tecnico = $ultima_azione['tecnico'];
                $r3_data_azione = $ultima_azione['data'];
            }
            
            $azione_date1 = new DateTime($r3_data_azione);
            
            if($azione_date1->format('Y-m-d') == date("Y-m-d"))
                $r3_data_azione = 'Oggi '.$azione_date1->format('H:i');
            else if($azione_date1->format('Y') == date("Y"))
                $r3_data_azione = $azione_date1->format('d/m');
            else
                $r3_data_azione = $azione_date1->format('d/m/Y');
            
            $ultimo_prod = (count($r3_prodotti)>0 ?$r3_prodotti_c : "Nessuno");
            $ultimo_prod_title = $r3_prodotti_n;
            $ultima_ch_ric = (count($r3_ch_ricevuta)>0 ? $ric_da." (".$ric_date.")" : "-");
            $ultima_ch_inv = (count($r3_ch_inviata)>0 ? $inv_da." (".$inv_date.")" : "-");
            $ultima_azione = (count($r3_azione)>0 ? $r3_tecnico." (".$r3_data_azione.")" : "-");

            // Colore delle label del riassunto: #268ccd

            $riassunto = array(
                // Riga 1
                'cliente' => $company_or_name,
                'codici' => $codici,
                'profilo' => $profilo,
                'tipo_profilo' => $tipo_profilo,
                'tipo_profilo_title' => $tipo_profilo_title,
                'tipo_cliente' => $tipo_cliente,
                'status' => $status,
                'registrazione' => Tools::displayDate($customer->date_add, null, true),
                // Riga 2
                'referenti' => $referenti,
                'referente' => $referente_c,
                'zona' => $zona,
                'ord_6' => Tools::displayPrice($ordini_ultimi_6_mesi, $this->context->currency->id),
                'ord_12' => Tools::displayPrice($ordini_ultimi_12_mesi, $this->context->currency->id),
                'ord_tot' => Tools::displayPrice($ordini_totali, $this->context->currency->id),
                'ultimo_acq' => $data_ultimo_acquisto,
                'impiegati' => $impiegati,
                'agente' => $agente_c,
                'tecnici' => $tecnici,
                'tecnico' => $tecnico_c,
                // Riga 3
                'visite' => $r3_visite,
                'pagine_viste' => $r3_pagine,
                'ultimo_prod' => $ultimo_prod,
                'ultimo_prod_title' => $ultimo_prod_title,
                'ultima_ch_ric' => $ultima_ch_ric,
                'ultima_ch_inv' => $ultima_ch_inv,
                'ultima_azione' => $ultima_azione,
            );

        }
        /* FINE Riassunto dati cliente */


        /* INIZIO Panel Informazioni */

        // tipo_cliente, ragione_sociale, data_ordine: presi da $customer e $order

        // $rif_ordine = ?

        $id_fattura = Db::getInstance()->getValue('SELECT id_fattura FROM '._DB_PREFIX_.'fattura FORCE INDEX (PRIMARY) WHERE rif_ordine = '.$order->id);
		
        if($id_fattura > 0) {
            $fatturato = true;
            $tipo_fattura = Db::getInstance()->getValue('SELECT tipo FROM '._DB_PREFIX_.'fattura WHERE id_fattura = '.$id_fattura);
        }
        else
            $fatturato = false;
		
        // nel tpl, se $fatturato = true:
		// <span style="color:red"><strong>Questo ordine &egrave; stato fatturato (fattura n. <a href="...inserire link..." target="_blank">'.$id_fattura.'</a>)</span></td></tr>
        if($permessi_verifica)
        {
            // js
            
            // spring? va tenuto? => Sì
            $spring_v = Db::getInstance()->getValue('SELECT spring FROM '._DB_PREFIX_.'orders WHERE id_order = '.$order->id);
            $verificato_da = Db::getInstance()->getValue('SELECT verificato_da FROM '._DB_PREFIX_.'orders WHERE id_order = '.$order->id);
            
            if($verificato_da > 0)
                $verificato_da_nome = Db::getInstance()->getValue('SELECT concat(firstname," ",lastname) from '._DB_PREFIX_.'employee where id_employee = '.$verificato_da);
            
            // tpl:
            // Verificato? checkbox name="spring_'.$order->id.'" id="spring_'.$order->id.'" '.($spring_v == 1 ? 'checked="checked"' : '').' onchange="updSpringOrder('.$order->id.'); return false;" /> 
            // ($spring_v == 1 && $verificato_da > 0 ? 'da '.$verificato_da_nome : '')

            // js function updCheckPI(id)
            
            $check_pi_v = Db::getInstance()->getValue('SELECT check_pi FROM '._DB_PREFIX_.'orders WHERE id_order = '.$order->id);
            
            // tpl:
            // P.I.? checkbox name="check_pi_'.$order->id.'" id="check_pi_'.$order->id.'" '.($check_pi_v == 1 ? 'checked="checked"' : '').' onchange="updCheckPI('.$order->id.'); return false;" />
        }

        // $permessi_pag_amz è in construct, devo ridefinirlo anche qui?
        if($order->payment == 'Carta Amazon' && $permessi_pag_amz)
        {
            // js function updPagAmzRic
            
            $pag_amz_ric_v = Db::getInstance()->getValue('SELECT pag_amz_ric FROM '._DB_PREFIX_.'orders WHERE id_order = '.$order->id);
            
            // tpl.
            // Pagamento amazon ricevuto? checkbox name="pag_amz_ric_'.$order->id.'" id="pag_amz_ric_'.$order->id.'" '.($pag_amz_ric_v == 1 ? 'checked="checked"' : '').' onchange="updPagAmzRic('.$order->id.'); return false;" />
        }

        // CORREGGERE: nascondere a Barbara (2)?
        if($permessi_esolver)
        {
            // js function updCheckEsolv

			$check_esolver_v = Db::getInstance()->getValue('SELECT check_esolver FROM '._DB_PREFIX_.'orders WHERE id_order = '.$order->id);

            // tpl:
			//eSolver? checkbox name="check_esolver_'.$order->id.'" id="check_esolver_'.$order->id.'" '.($check_esolver_v == 1 ? 'checked="checked"' : '').' onchange="updCheckEsolv('.$order->id.'); return false;" />
        }

        $informazioni = array(
            'rif_ordine' => $rif_ordine,
            'spring_v' => $spring_v,
            'verificato_da' => $verificato_da,
            'verificato_da_nome' => $verificato_da_nome,
            'check_pi_v' => $check_pi_v,
            'pag_amz_ric_v' => $pag_amz_ric_v,
            'check_esolver_v' => $check_esolver_v,
            'fatturato' => $fatturato,
            'id_fattura' => $id_fattura,
            'tipo_fattura' => $tipo_fattura,
        );

        /* FINE Panel Informazioni */


        /* INSERIRE PANEL ULTIME ATTIVITA'? Vedi $ultime_attivita_ord 1.4 */


        /* INIZIO Panel Indirizzo Fatturazione */

        // se false, tpl: cf in rosso con avviso in un'icona con title e alt = "Codice fiscale potenzialmente errato: controllare"
        $cfcheck = Customer::ControllaCF2($customer->tax_code);

        // CORREGGERE TPL: se privato non vedo p.iva e referente, destinatario: firstname e lastname; se azienda, destinatario: company

        // vedi js function modificaIndirizzoSpedizione

        // Scelta indirizzo di fatturazione attiva solo se il cliente è una P.A.
        if($customer->id_default_group == 5){
            // CORREGGERE: si può spostare in una funzione nella classe Address
            $indirizzi_fatt = Db::getInstance()->executeS("
                SELECT * 
                FROM "._DB_PREFIX_."address 
                WHERE id_customer = ".$customer->id." 
                    AND fatturazione = 1 
                    AND deleted = 0 
                    AND active = 1 
                ORDER BY id_address ASC
            ");
        }

        // TPL: aggiungere link a modifica dati dell'indirizzo di fatturazione; togliere select

        $indirizzo_fatturazione = array(
            'indirizzi' => $indirizzi_fatt,
            'indirizzo' => $addressInvoice,
            'cfcheck' => $cfcheck,
        );
        /* FINE Panel Indirizzo Fatturazione */


        /* INIZIO Panel Indirizzo Consegna */

        // CORREGGERE: si può spostare in una funzione nella classe Customer (+ func che prende solo ind con fatt = 0?)
		$indirizzi_cons = Db::getInstance()->executeS("
            SELECT * 
            FROM "._DB_PREFIX_."address 
            WHERE id_customer = ".$customer->id." 
                AND deleted = 0 
                AND active = 1 
            ORDER BY id_address ASC
        ");

        // vedi js function modificaIndirizzoSpedizione

        // destinatario: se esiste $addressDelivery->company quello, altrimenti nome
        // referente: se privato, se esiste $addressDelivery->company stampo nome altrimenti niente; se azienda, nome

        // tpl: checkbox csv (name="ind_csv" id="ind_csv")
        // tasti modifica dati indirizzo e gmaps
		// tasto gmaps: <a id="cons_map" href="http://maps.google.com/maps?f=q&hl='.$currentLanguage->iso_code.'&geocode=&q='.$addressDelivery->address1.' '.$addressDelivery->postcode.' '.$addressDelivery->city.($addressDelivery->id_state ? ' '.$deliveryState->name: '').'" target="_blank"><img src="../img/admin/google.gif" alt="" class="middle" /></a>

        $indirizzo_consegna = array(
            'indirizzi' => $indirizzi_cons,
        );
        /* FINE Panel Indirizzo Consegna */


        /* INIZIO Panel Dati P.A. */

        $has_pa = false;
        
        //if($is_pa){
            $dati_pa =  Db::getInstance()->getRow('
                SELECT cig, cup, ipa, data_ordine_mepa 
                FROM '._DB_PREFIX_.'orders 
                WHERE id_order = '.$order->id
            );
            
            if($dati_pa['cig'] != '' || $dati_pa['cup'] != '' || $dati_pa['ipa'] != '' || $dati_pa['data_ordine_mepa'] != '0000-00-00')
                $has_pa = true;

            $dati_pa['data_ordine_mepa'] = (($dati_pa['data_ordine_mepa'] == '' || $dati_pa['data_ordine_mepa'] == '0000-00-00') ? '' : date("d-m-Y", strtotime($dati_pa['data_ordine_mepa'])));
        //}
        /* FINE Panel Dati P.A. */


        /* INIZIO Panel Storico stato ordine */

        if(!$is_agente){ // Form cambio stato attuale e tracking
            // da fare, vedi 1.4
        }

        // tpl: aggiungere colonna stato tecnico

        // CORREGGERE: spostare sopra o eliminare doppioni di $history?
        foreach ($history as &$row) {

            // SOLO PER LA PRIMA RIGA?
            // Stato (?????)
            if($row['id_order_state'] == 24 || $row['id_order_state'] == 25 || $row['id_order_state'] == 27 || $row['id_order_state'] == 28)
                $row['ostate_name'] = '--';

            if($row['id_order_state'] == 33) {
                $acconto = Db::getInstance()->getValue('
                    SELECT desc_attivita 
                    FROM storico_attivita
                    WHERE tipo_attivita = "OH" 
                        AND id_attivita = '.$row['id_order_history']
                );
                
                $acconto = str_replace('Incassato acconto di ', '', $acconto);
                
                if($acconto != '')
                    $row['ostate_name'] = $row['ostate_name'].' di '.$acconto.' euro';
            }

            // Stato tecnico (?????) -> CORREGGERE: chiedere a Federico come funziona
            if($row['id_order_state'] != 24 && $row['id_order_state'] != 25 && $row['id_order_state'] != 27 && $row['id_order_state'] != 28)
                $row['stato_tecnico'] = '--';
            // FINE PRIMA RIGA?

            $current_state_history = Db::getInstance()->getValue('SELECT max(id_order_history) FROM '._DB_PREFIX_.'order_history WHERE id_order_state != 24 AND id_order_state != 25 AND id_order_state != 27 AND id_order_state !=28 AND id_order = '.$order->id);
			$current_state = Db::getInstance()->getValue('SELECT id_order_state FROM '._DB_PREFIX_.'order_history WHERE id_order_history = '.$current_state_history);
			
            // PER OGNI RIGA DI HISTORY - STATI PRECEDENTI
            if($row['id_order_state'] == 24 || $row['id_order_state'] == 25 || $row['id_order_state'] == 27 || $row['id_order_state'] == 28) {	
                $row['ostate_name'] = '--';
                $row['stato_tecnico'] = $row['ostate_name']; // fare stripslashes nel tpl
            }
            else {
                if($row['id_order_state'] == 33) {
                    $acconto = Db::getInstance()->getValue('
                        SELECT desc_attivita 
                        FROM storico_attivita
                        WHERE tipo_attivita = "OH" 
                            AND id_attivita = '.$row['id_order_history']
                    );

                    $acconto = str_replace('Incassato acconto di ', '', $acconto);
                
                    if($acconto != '')
                        $row['ostate_name'] = $row['ostate_name'].' di '.$acconto.' euro';
                }
            
                $row['stato_tecnico'] = '--';
            }
        }

        if(!$is_agente){ // Form feedback
            $no_feedback = Db::getInstance()->getValue('SELECT no_feedback FROM '._DB_PREFIX_.'orders WHERE id_order = '.$order->id);
            // if no_feedback = 1 => checked
            $causa = Db::getInstance()->getValue('SELECT no_feedback_cause FROM '._DB_PREFIX_.'orders WHERE id_order = '.$order->id);

            // js da controllare:
            /*
            <script type="text/javascript">function saveNoFeedback()
            {	
                $("#nofeedback_feedback").html("<img src=\"../img/loader.gif\" />").show();
                if($("#no_feedback").is(":checked"))
                    var no_feedback = 1;
                else
                    var no_feedback = 0;
                
                console.log(no_feedback);
                var no_feedback_cause = $("#no_feedback_cause").val();
                console.log(no_feedback_cause);
                
                $.post("ajax.php", {submitNoFeedback:1,id_customer:'.(int)$customer->id.',id_order:"'.$order->id.'",no_feedback:no_feedback,no_feedback_cause:no_feedback_cause}, function (r) {
                    $("#nofeedback_feedback").html("").hide();
                    if (r == "ok")
                    {
                        $("#nofeedback_feedback").html("<b style=\"color:green\">'.addslashes(AdminOrders::lx('Dati salvati')).'</b>").fadeIn(400);
                    }
                    else if (r == "error:validation")
                        $("#nofeedback_feedback").html("<b style=\"color:red\">'.addslashes(AdminOrders::lx('Errore: impossibile salvare dati')).'</b>").fadeIn(400);
                    else if (r == "error:update")
                        $("#nofeedback_feedback").html("<b style=\"color:red\">'.addslashes(AdminOrders::lx('Errore: impossibile salvare dati')).'</b>").fadeIn(400);
                    $("#nofeedback_feedback").fadeOut(3000);
                });
            }
            </script>
            */
        }

        $storico_stati = array(
            'no_feedback' => $no_feedback,
            'causa' => $causa,
        );

        /* FINE Panel Storico stato ordine */


        /* INIZIO Panel Pagamento e spedizione */

        // tpl
        // metodo di pagamento: $order->payment
        // peso: $order->weight
        // Numero di tracking: $order->shipping_number

        // Metodo di spedizione:
        $metodo_spedizione = ($carrier->name == '0' ? Configuration::get('PS_SHOP_NAME') : $carrier->name);

        // Consegna
        $tempi_di_consegna = Db::getInstance()->getValue('SELECT consegna FROM '._DB_PREFIX_.'cart WHERE id_cart = '.$order->id_cart);
        
        $preventivo = Db::getInstance()->getValue('SELECT preventivo FROM '._DB_PREFIX_.'cart WHERE id_cart = '.$order->id_cart);
        if($preventivo == 1 && $tempi_di_consegna == '')
            $tempi_di_consegna = '2-5 gg.';

		// Traccia la spedizione
        $tracking_href = (!empty($carrier->url) ? str_replace('@', $order->shipping_number, $carrier->url) : '');

        $pag_spedizione = array(
            'metodo_spedizione' => $metodo_spedizione,
            'consegna' => $tempi_di_consegna,
            'tracking_href' => $tracking_href,
        );

        /* FINE Panel Pagamento e spedizione */


        /* INIZIO Panel Prodotti */
        foreach ($products as &$product) {
            $product['tooltip'] = Product::showProductTooltip($product['id_product']);
        }
        /* FINE Panel Prodotti */


        /* INIZIO Panel Area carrello */
        /* FINE Panel Area carrello */

        /* CORREGGERE: spostare nel posto giusto */
        // Flag "Riprodurre CSV", input name="si_csv"
        if(Tools::getIsset('si_csv'))
			Product::RecuperaCSV($order->id);

        /* FINE OVERRIDE */

        // Smarty assign
        $this->tpl_view_vars = array(
            'order' => $order,
            'cart' => new Cart($order->id_cart),
            'customer' => $customer,
            'gender' => $gender,
            'customer_addresses' => $customer->getAddresses($this->context->language->id),
            'addresses' => array(
                'delivery' => $addressDelivery,
                'deliveryState' => isset($deliveryState) ? $deliveryState : null,
                'invoice' => $addressInvoice,
                'invoiceState' => isset($invoiceState) ? $invoiceState : null
            ),
            'customerStats' => $customer->getStats(),
            'products' => $products,
            'discounts' => $order->getCartRules(),
            'orders_total_paid_tax_incl' => $order->getOrdersTotalPaid(), // Get the sum of total_paid_tax_incl of the order with similar reference
            'total_paid' => $order->getTotalPaid(),
            'returns' => OrderReturn::getOrdersReturn($order->id_customer, $order->id),
            'customer_thread_message' => CustomerThread::getCustomerMessages($order->id_customer, null, $order->id),
            'orderMessages' => OrderMessage::getOrderMessages($order->id_lang),
            'messages' => Message::getMessagesByOrderId($order->id, true),
            'carrier' => new Carrier($order->id_carrier),
            'history' => $history,
            'states' => OrderState::getOrderStates($this->context->language->id),
            'warehouse_list' => $warehouse_list,
            'sources' => ConnectionsSource::getOrderSources($order->id),
            'currentState' => $order->getCurrentOrderState(),
            'currency' => new Currency($order->id_currency),
            'currencies' => Currency::getCurrenciesByIdShop($order->id_shop),
            'previousOrder' => $order->getPreviousOrderId(),
            'nextOrder' => $order->getNextOrderId(),
            'current_index' => self::$currentIndex,
            'carrierModuleCall' => $carrier_module_call,
            'iso_code_lang' => $this->context->language->iso_code,
            'id_lang' => $this->context->language->id,
            'can_edit' => ($this->tabAccess['edit'] == 1),
            'current_id_lang' => $this->context->language->id,
            'invoices_collection' => $order->getInvoicesCollection(),
            'not_paid_invoices_collection' => $order->getNotPaidInvoicesCollection(),
            'payment_methods' => $payment_methods,
            'invoice_management_active' => Configuration::get('PS_INVOICE', null, null, $order->id_shop),
            'display_warehouse' => (int)Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT'),
            'HOOK_CONTENT_ORDER' => Hook::exec('displayAdminOrderContentOrder', array(
                'order' => $order,
                'products' => $products,
                'customer' => $customer)
            ),
            'HOOK_CONTENT_SHIP' => Hook::exec('displayAdminOrderContentShip', array(
                'order' => $order,
                'products' => $products,
                'customer' => $customer)
            ),
            'HOOK_TAB_ORDER' => Hook::exec('displayAdminOrderTabOrder', array(
                'order' => $order,
                'products' => $products,
                'customer' => $customer)
            ),
            'HOOK_TAB_SHIP' => Hook::exec('displayAdminOrderTabShip', array(
                'order' => $order,
                'products' => $products,
                'customer' => $customer)
            ),
            /* INIZIO override! */
            'is_agente' => $is_agente,
            'not_mio_agente' => $not_mio_agente,
            'permessi_verifica' => $permessi_verifica,
            'has_pa' => $has_pa,
            'riassunto_on' => $riassunto_on,
            'riassunto' => $riassunto,
            'indirizzo_fatturazione' => $indirizzo_fatturazione,
            'indirizzo_consegna' => $indirizzo_consegna,
            'dati_pa' => $dati_pa,
            'storico_stati' => $storico_stati,
            'pag_spedizione' => $pag_spedizione,
            /* FINE override! */
        );

        /* OVERRIDE */

        // autoload rich text editor (tiny mce)
        $this->tpl_view_vars['tinymce'] = true;
        $iso = $this->context->language->iso_code;
        $this->tpl_view_vars['iso'] = file_exists(_PS_CORE_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en';
        $this->tpl_view_vars['path_css'] = _THEME_CSS_DIR_;
        $this->tpl_view_vars['ad'] = __PS_BASE_URI__.basename(_PS_ADMIN_DIR_);

        $this->addJS(array(
            _PS_JS_DIR_.'tiny_mce/tiny_mce.js',
            _PS_JS_DIR_.'admin/tinymce.inc.js',
            _PS_BO_DEFAULT_THEME_JS_DIR_.'tooltip_prodotto.js'
        ));

        /* FINE OVERRIDE */

        return parent::renderView();
    }

    public function ajaxProcessSearchProducts()
    {
        Context::getContext()->customer = new Customer((int)Tools::getValue('id_customer'));
        $currency = new Currency((int)Tools::getValue('id_currency'));
        if ($products = Product::searchByName((int)$this->context->language->id, pSQL(Tools::getValue('product_search')))) {
            foreach ($products as &$product) {
                // Formatted price
                $product['formatted_price'] = Tools::displayPrice(Tools::convertPrice($product['price_tax_incl'], $currency), $currency);
                // Concret price
                $product['price_tax_incl'] = Tools::ps_round(Tools::convertPrice($product['price_tax_incl'], $currency), 2);
                $product['price_tax_excl'] = Tools::ps_round(Tools::convertPrice($product['price_tax_excl'], $currency), 2);
                $productObj = new Product((int)$product['id_product'], false, (int)$this->context->language->id);
                $combinations = array();
                $attributes = $productObj->getAttributesGroups((int)$this->context->language->id);

                // Tax rate for this customer
                if (Tools::isSubmit('id_address')) {
                    $product['tax_rate'] = $productObj->getTaxesRate(new Address(Tools::getValue('id_address')));
                }

                $product['warehouse_list'] = array();

                foreach ($attributes as $attribute) {
                    if (!isset($combinations[$attribute['id_product_attribute']]['attributes'])) {
                        $combinations[$attribute['id_product_attribute']]['attributes'] = '';
                    }
                    $combinations[$attribute['id_product_attribute']]['attributes'] .= $attribute['attribute_name'].' - ';
                    $combinations[$attribute['id_product_attribute']]['id_product_attribute'] = $attribute['id_product_attribute'];
                    $combinations[$attribute['id_product_attribute']]['default_on'] = $attribute['default_on'];
                    if (!isset($combinations[$attribute['id_product_attribute']]['price'])) {
                        $price_tax_incl = Product::getPriceStatic((int)$product['id_product'], true, $attribute['id_product_attribute']);
                        $price_tax_excl = Product::getPriceStatic((int)$product['id_product'], false, $attribute['id_product_attribute']);
                        $combinations[$attribute['id_product_attribute']]['price_tax_incl'] = Tools::ps_round(Tools::convertPrice($price_tax_incl, $currency), 2);
                        $combinations[$attribute['id_product_attribute']]['price_tax_excl'] = Tools::ps_round(Tools::convertPrice($price_tax_excl, $currency), 2);
                        $combinations[$attribute['id_product_attribute']]['formatted_price'] = Tools::displayPrice(Tools::convertPrice($price_tax_excl, $currency), $currency);
                    }
                    if (!isset($combinations[$attribute['id_product_attribute']]['qty_in_stock'])) {
                        $combinations[$attribute['id_product_attribute']]['qty_in_stock'] = StockAvailable::getQuantityAvailableByProduct((int)$product['id_product'], $attribute['id_product_attribute'], (int)$this->context->shop->id);
                    }

                    if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && (int)$product['advanced_stock_management'] == 1) {
                        $product['warehouse_list'][$attribute['id_product_attribute']] = Warehouse::getProductWarehouseList($product['id_product'], $attribute['id_product_attribute']);
                    } else {
                        $product['warehouse_list'][$attribute['id_product_attribute']] = array();
                    }

                    $product['stock'][$attribute['id_product_attribute']] = Product::getRealQuantity($product['id_product'], $attribute['id_product_attribute']);
                }

                if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && (int)$product['advanced_stock_management'] == 1) {
                    $product['warehouse_list'][0] = Warehouse::getProductWarehouseList($product['id_product']);
                } else {
                    $product['warehouse_list'][0] = array();
                }

                $product['stock'][0] = StockAvailable::getQuantityAvailableByProduct((int)$product['id_product'], 0, (int)$this->context->shop->id);

                foreach ($combinations as &$combination) {
                    $combination['attributes'] = rtrim($combination['attributes'], ' - ');
                }
                $product['combinations'] = $combinations;

                if ($product['customizable']) {
                    $product_instance = new Product((int)$product['id_product']);
                    $product['customization_fields'] = $product_instance->getCustomizationFields($this->context->language->id);
                }
            }

            $to_return = array(
                'products' => $products,
                'found' => true
            );
        } else {
            $to_return = array('found' => false);
        }

        $this->content = Tools::jsonEncode($to_return);
    }

    public function ajaxProcessSendMailValidateOrder()
    {
        if ($this->tabAccess['edit'] === '1') {
            $cart = new Cart((int)Tools::getValue('id_cart'));
            if (Validate::isLoadedObject($cart)) {
                $customer = new Customer((int)$cart->id_customer);
                if (Validate::isLoadedObject($customer)) {
                    $mailVars = array(
                        '{order_link}' => Context::getContext()->link->getPageLink('order', false, (int)$cart->id_lang, 'step=3&recover_cart='.(int)$cart->id.'&token_cart='.md5(_COOKIE_KEY_.'recover_cart_'.(int)$cart->id)),
                        '{firstname}' => $customer->firstname,
                        '{lastname}' => $customer->lastname
                    );
                    if (Mail::Send((int)$cart->id_lang, 'backoffice_order', Mail::l('Process the payment of your order', (int)$cart->id_lang), $mailVars, $customer->email,
                            $customer->firstname.' '.$customer->lastname, null, null, null, null, _PS_MAIL_DIR_, true, $cart->id_shop)) {
                        die(Tools::jsonEncode(array('errors' => false, 'result' => $this->l('The email was sent to your customer.'))));
                    }
                }
            }
            $this->content = Tools::jsonEncode(array('errors' => true, 'result' => $this->l('Error in sending the email to your customer.')));
        }
    }

    public function ajaxProcessAddProductOnOrder()
    {
        // Load object
        $order = new Order((int)Tools::getValue('id_order'));
        if (!Validate::isLoadedObject($order)) {
            die(Tools::jsonEncode(array(
                'result' => false,
                'error' => Tools::displayError('The order object cannot be loaded.')
            )));
        }

        $old_cart_rules = Context::getContext()->cart->getCartRules();

        if ($order->hasBeenShipped()) {
            die(Tools::jsonEncode(array(
                'result' => false,
                'error' => Tools::displayError('You cannot add products to delivered orders. ')
            )));
        }

        $product_informations = $_POST['add_product'];
        if (isset($_POST['add_invoice'])) {
            $invoice_informations = $_POST['add_invoice'];
        } else {
            $invoice_informations = array();
        }
        $product = new Product($product_informations['product_id'], false, $order->id_lang);
        if (!Validate::isLoadedObject($product)) {
            die(Tools::jsonEncode(array(
                'result' => false,
                'error' => Tools::displayError('The product object cannot be loaded.')
            )));
        }

        if (isset($product_informations['product_attribute_id']) && $product_informations['product_attribute_id']) {
            $combination = new Combination($product_informations['product_attribute_id']);
            if (!Validate::isLoadedObject($combination)) {
                die(Tools::jsonEncode(array(
                'result' => false,
                'error' => Tools::displayError('The combination object cannot be loaded.')
            )));
            }
        }

        // Total method
        $total_method = Cart::BOTH_WITHOUT_SHIPPING;

        // Create new cart
        $cart = new Cart();
        $cart->id_shop_group = $order->id_shop_group;
        $cart->id_shop = $order->id_shop;
        $cart->id_customer = $order->id_customer;
        $cart->id_carrier = $order->id_carrier;
        $cart->id_address_delivery = $order->id_address_delivery;
        $cart->id_address_invoice = $order->id_address_invoice;
        $cart->id_currency = $order->id_currency;
        $cart->id_lang = $order->id_lang;
        $cart->secure_key = $order->secure_key;

        // Save new cart
        $cart->add();

        // Save context (in order to apply cart rule)
        $this->context->cart = $cart;
        $this->context->customer = new Customer($order->id_customer);

        // always add taxes even if there are not displayed to the customer
        $use_taxes = true;

        $initial_product_price_tax_incl = Product::getPriceStatic($product->id, $use_taxes, isset($combination) ? $combination->id : null, 2, null, false, true, 1,
            false, $order->id_customer, $cart->id, $order->{Configuration::get('PS_TAX_ADDRESS_TYPE', null, null, $order->id_shop)});

        // Creating specific price if needed
        if ($product_informations['product_price_tax_incl'] != $initial_product_price_tax_incl) {
            $specific_price = new SpecificPrice();
            $specific_price->id_shop = 0;
            $specific_price->id_shop_group = 0;
            $specific_price->id_currency = 0;
            $specific_price->id_country = 0;
            $specific_price->id_group = 0;
            $specific_price->id_customer = $order->id_customer;
            $specific_price->id_product = $product->id;
            if (isset($combination)) {
                $specific_price->id_product_attribute = $combination->id;
            } else {
                $specific_price->id_product_attribute = 0;
            }
            $specific_price->price = $product_informations['product_price_tax_excl'];
            $specific_price->from_quantity = 1;
            $specific_price->reduction = 0;
            $specific_price->reduction_type = 'amount';
            $specific_price->reduction_tax = 0;
            $specific_price->from = '0000-00-00 00:00:00';
            $specific_price->to = '0000-00-00 00:00:00';
            $specific_price->add();
        }

        // Add product to cart
        $update_quantity = $cart->updateQty($product_informations['product_quantity'], $product->id, isset($product_informations['product_attribute_id']) ? $product_informations['product_attribute_id'] : null,
            isset($combination) ? $combination->id : null, 'up', 0, new Shop($cart->id_shop));

        if ($update_quantity < 0) {
            // If product has attribute, minimal quantity is set with minimal quantity of attribute
            $minimal_quantity = ($product_informations['product_attribute_id']) ? Attribute::getAttributeMinimalQty($product_informations['product_attribute_id']) : $product->minimal_quantity;
            die(Tools::jsonEncode(array('error' => sprintf(Tools::displayError('You must add %d minimum quantity', false), $minimal_quantity))));
        } elseif (!$update_quantity) {
            die(Tools::jsonEncode(array('error' => Tools::displayError('You already have the maximum quantity available for this product.', false))));
        }

        // If order is valid, we can create a new invoice or edit an existing invoice
        if ($order->hasInvoice()) {
            $order_invoice = new OrderInvoice($product_informations['invoice']);
            // Create new invoice
            if ($order_invoice->id == 0) {
                // If we create a new invoice, we calculate shipping cost
                $total_method = Cart::BOTH;
                // Create Cart rule in order to make free shipping
                if (isset($invoice_informations['free_shipping']) && $invoice_informations['free_shipping']) {
                    $cart_rule = new CartRule();
                    $cart_rule->id_customer = $order->id_customer;
                    $cart_rule->name = array(
                        Configuration::get('PS_LANG_DEFAULT') => $this->l('[Generated] CartRule for Free Shipping')
                    );
                    $cart_rule->date_from = date('Y-m-d H:i:s', time());
                    $cart_rule->date_to = date('Y-m-d H:i:s', time() + 24 * 3600);
                    $cart_rule->quantity = 1;
                    $cart_rule->quantity_per_user = 1;
                    $cart_rule->minimum_amount_currency = $order->id_currency;
                    $cart_rule->reduction_currency = $order->id_currency;
                    $cart_rule->free_shipping = true;
                    $cart_rule->active = 1;
                    $cart_rule->add();

                    // Add cart rule to cart and in order
                    $cart->addCartRule($cart_rule->id);
                    $values = array(
                        'tax_incl' => $cart_rule->getContextualValue(true),
                        'tax_excl' => $cart_rule->getContextualValue(false)
                    );
                    $order->addCartRule($cart_rule->id, $cart_rule->name[Configuration::get('PS_LANG_DEFAULT')], $values);
                }

                $order_invoice->id_order = $order->id;
                if ($order_invoice->number) {
                    Configuration::updateValue('PS_INVOICE_START_NUMBER', false, false, null, $order->id_shop);
                } else {
                    $order_invoice->number = Order::getLastInvoiceNumber() + 1;
                }

                $invoice_address = new Address((int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE', null, null, $order->id_shop)});
                $carrier = new Carrier((int)$order->id_carrier);
                $tax_calculator = $carrier->getTaxCalculator($invoice_address);

                $order_invoice->total_paid_tax_excl = Tools::ps_round((float)$cart->getOrderTotal(false, $total_method), 2);
                $order_invoice->total_paid_tax_incl = Tools::ps_round((float)$cart->getOrderTotal($use_taxes, $total_method), 2);
                $order_invoice->total_products = (float)$cart->getOrderTotal(false, Cart::ONLY_PRODUCTS);
                $order_invoice->total_products_wt = (float)$cart->getOrderTotal($use_taxes, Cart::ONLY_PRODUCTS);
                $order_invoice->total_shipping_tax_excl = (float)$cart->getTotalShippingCost(null, false);
                $order_invoice->total_shipping_tax_incl = (float)$cart->getTotalShippingCost();

                $order_invoice->total_wrapping_tax_excl = abs($cart->getOrderTotal(false, Cart::ONLY_WRAPPING));
                $order_invoice->total_wrapping_tax_incl = abs($cart->getOrderTotal($use_taxes, Cart::ONLY_WRAPPING));
                $order_invoice->shipping_tax_computation_method = (int)$tax_calculator->computation_method;

                // Update current order field, only shipping because other field is updated later
                $order->total_shipping += $order_invoice->total_shipping_tax_incl;
                $order->total_shipping_tax_excl += $order_invoice->total_shipping_tax_excl;
                $order->total_shipping_tax_incl += ($use_taxes) ? $order_invoice->total_shipping_tax_incl : $order_invoice->total_shipping_tax_excl;

                $order->total_wrapping += abs($cart->getOrderTotal($use_taxes, Cart::ONLY_WRAPPING));
                $order->total_wrapping_tax_excl += abs($cart->getOrderTotal(false, Cart::ONLY_WRAPPING));
                $order->total_wrapping_tax_incl += abs($cart->getOrderTotal($use_taxes, Cart::ONLY_WRAPPING));
                $order_invoice->add();

                $order_invoice->saveCarrierTaxCalculator($tax_calculator->getTaxesAmount($order_invoice->total_shipping_tax_excl));

                $order_carrier = new OrderCarrier();
                $order_carrier->id_order = (int)$order->id;
                $order_carrier->id_carrier = (int)$order->id_carrier;
                $order_carrier->id_order_invoice = (int)$order_invoice->id;
                $order_carrier->weight = (float)$cart->getTotalWeight();
                $order_carrier->shipping_cost_tax_excl = (float)$order_invoice->total_shipping_tax_excl;
                $order_carrier->shipping_cost_tax_incl = ($use_taxes) ? (float)$order_invoice->total_shipping_tax_incl : (float)$order_invoice->total_shipping_tax_excl;
                $order_carrier->add();
            }
            // Update current invoice
            else {
                $order_invoice->total_paid_tax_excl += Tools::ps_round((float)($cart->getOrderTotal(false, $total_method)), 2);
                $order_invoice->total_paid_tax_incl += Tools::ps_round((float)($cart->getOrderTotal($use_taxes, $total_method)), 2);
                $order_invoice->total_products += (float)$cart->getOrderTotal(false, Cart::ONLY_PRODUCTS);
                $order_invoice->total_products_wt += (float)$cart->getOrderTotal($use_taxes, Cart::ONLY_PRODUCTS);
                $order_invoice->update();
            }
        }

        // Create Order detail information
        $order_detail = new OrderDetail();
        $order_detail->createList($order, $cart, $order->getCurrentOrderState(), $cart->getProducts(), (isset($order_invoice) ? $order_invoice->id : 0), $use_taxes, (int)Tools::getValue('add_product_warehouse'));

        // update totals amount of order
        $order->total_products += (float)$cart->getOrderTotal(false, Cart::ONLY_PRODUCTS);
        $order->total_products_wt += (float)$cart->getOrderTotal($use_taxes, Cart::ONLY_PRODUCTS);

        $order->total_paid += Tools::ps_round((float)($cart->getOrderTotal(true, $total_method)), 2);
        $order->total_paid_tax_excl += Tools::ps_round((float)($cart->getOrderTotal(false, $total_method)), 2);
        $order->total_paid_tax_incl += Tools::ps_round((float)($cart->getOrderTotal($use_taxes, $total_method)), 2);

        if (isset($order_invoice) && Validate::isLoadedObject($order_invoice)) {
            $order->total_shipping = $order_invoice->total_shipping_tax_incl;
            $order->total_shipping_tax_incl = $order_invoice->total_shipping_tax_incl;
            $order->total_shipping_tax_excl = $order_invoice->total_shipping_tax_excl;
        }
        
        // discount
        $order->total_discounts += (float)abs($cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS));
        $order->total_discounts_tax_excl += (float)abs($cart->getOrderTotal(false, Cart::ONLY_DISCOUNTS));
        $order->total_discounts_tax_incl += (float)abs($cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS));

        // Save changes of order
        $order->update();

        // Update weight SUM
        $order_carrier = new OrderCarrier((int)$order->getIdOrderCarrier());
        if (Validate::isLoadedObject($order_carrier)) {
            $order_carrier->weight = (float)$order->getTotalWeight();
            if ($order_carrier->update()) {
                $order->weight = sprintf("%.3f ".Configuration::get('PS_WEIGHT_UNIT'), $order_carrier->weight);
            }
        }

        // Update Tax lines
        $order_detail->updateTaxAmount($order);

        // Delete specific price if exists
        if (isset($specific_price)) {
            $specific_price->delete();
        }

        $products = $this->getProductsModificata($order);

        // Get the last product
        $product = end($products);
        $product['current_stock'] = StockAvailable::getQuantityAvailableByProduct($product['product_id'], $product['product_attribute_id'], $product['id_shop']);
        $resume = OrderSlip::getProductSlipResume((int)$product['id_order_detail']);
        $product['quantity_refundable'] = $product['product_quantity'] - $resume['product_quantity'];
        $product['amount_refundable'] = $product['total_price_tax_excl'] - $resume['amount_tax_excl'];
        $product['amount_refund'] = Tools::displayPrice($resume['amount_tax_incl']);
        $product['return_history'] = OrderReturn::getProductReturnDetail((int)$product['id_order_detail']);
        $product['refund_history'] = OrderSlip::getProductSlipDetail((int)$product['id_order_detail']);
        if ($product['id_warehouse'] != 0) {
            $warehouse = new Warehouse((int)$product['id_warehouse']);
            $product['warehouse_name'] = $warehouse->name;
            $warehouse_location = WarehouseProductLocation::getProductLocation($product['product_id'], $product['product_attribute_id'], $product['id_warehouse']);
            if (!empty($warehouse_location)) {
                $product['warehouse_location'] = $warehouse_location;
            } else {
                $product['warehouse_location'] = false;
            }
        } else {
            $product['warehouse_name'] = '--';
            $product['warehouse_location'] = false;
        }

        // Get invoices collection
        $invoice_collection = $order->getInvoicesCollection();

        $invoice_array = array();
        foreach ($invoice_collection as $invoice) {
            /** @var OrderInvoice $invoice */
            $invoice->name = $invoice->getInvoiceNumberFormatted(Context::getContext()->language->id, (int)$order->id_shop);
            $invoice_array[] = $invoice;
        }

        // Assign to smarty informations in order to show the new product line
        $this->context->smarty->assign(array(
            'product' => $product,
            'order' => $order,
            'currency' => new Currency($order->id_currency),
            'can_edit' => $this->tabAccess['edit'],
            'invoices_collection' => $invoice_collection,
            'current_id_lang' => Context::getContext()->language->id,
            'link' => Context::getContext()->link,
            'current_index' => self::$currentIndex,
            'display_warehouse' => (int)Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')
        ));

        $this->sendChangedNotification($order);
        $new_cart_rules = Context::getContext()->cart->getCartRules();
        sort($old_cart_rules);
        sort($new_cart_rules);
        $result = array_diff($new_cart_rules, $old_cart_rules);
        $refresh = false;

        $res = true;
        foreach ($result as $cart_rule) {
            $refresh = true;
            // Create OrderCartRule
            $rule = new CartRule($cart_rule['id_cart_rule']);
            $values = array(
                    'tax_incl' => $rule->getContextualValue(true),
                    'tax_excl' => $rule->getContextualValue(false)
                    );
            $order_cart_rule = new OrderCartRule();
            $order_cart_rule->id_order = $order->id;
            $order_cart_rule->id_cart_rule = $cart_rule['id_cart_rule'];
            $order_cart_rule->id_order_invoice = $order_invoice->id;
            $order_cart_rule->name = $cart_rule['name'];
            $order_cart_rule->value = $values['tax_incl'];
            $order_cart_rule->value_tax_excl = $values['tax_excl'];
            $res &= $order_cart_rule->add();

            $order->total_discounts += $order_cart_rule->value;
            $order->total_discounts_tax_incl += $order_cart_rule->value;
            $order->total_discounts_tax_excl += $order_cart_rule->value_tax_excl;
            $order->total_paid -= $order_cart_rule->value;
            $order->total_paid_tax_incl -= $order_cart_rule->value;
            $order->total_paid_tax_excl -= $order_cart_rule->value_tax_excl;
        }

        // Update Order
        $res &= $order->update();


        die(Tools::jsonEncode(array(
            'result' => true,
            'view' => $this->createTemplate('_product_line.tpl')->fetch(),
            'can_edit' => $this->tabAccess['add'],
            'order' => $order,
            'invoices' => $invoice_array,
            'documents_html' => $this->createTemplate('_documents.tpl')->fetch(),
            'shipping_html' => $this->createTemplate('_shipping.tpl')->fetch(),
            'discount_form_html' => $this->createTemplate('_discount_form.tpl')->fetch(),
            'refresh' => $refresh
        )));
    }

    public function sendChangedNotification(Order $order = null)
    {
        if (is_null($order)) {
            $order = new Order(Tools::getValue('id_order'));
        }

        Hook::exec('actionOrderEdited', array('order' => $order));
    }

    public function ajaxProcessLoadProductInformation()
    {
        $order_detail = new OrderDetail(Tools::getValue('id_order_detail'));
        if (!Validate::isLoadedObject($order_detail)) {
            die(Tools::jsonEncode(array(
                'result' => false,
                'error' => Tools::displayError('The OrderDetail object cannot be loaded.')
            )));
        }

        $product = new Product($order_detail->product_id);
        if (!Validate::isLoadedObject($product)) {
            die(Tools::jsonEncode(array(
                'result' => false,
                'error' => Tools::displayError('The product object cannot be loaded.')
            )));
        }

        $address = new Address(Tools::getValue('id_address'));
        if (!Validate::isLoadedObject($address)) {
            die(Tools::jsonEncode(array(
                'result' => false,
                'error' => Tools::displayError('The address object cannot be loaded.')
            )));
        }

        die(Tools::jsonEncode(array(
            'result' => true,
            'product' => $product,
            'tax_rate' => $product->getTaxesRate($address),
            'price_tax_incl' => Product::getPriceStatic($product->id, true, $order_detail->product_attribute_id, 2),
            'price_tax_excl' => Product::getPriceStatic($product->id, false, $order_detail->product_attribute_id, 2),
            'reduction_percent' => $order_detail->reduction_percent
        )));
    }

    public function ajaxProcessEditProductOnOrder()
    {
        // Return value
        $res = true;

        $order = new Order((int)Tools::getValue('id_order'));
        $order_detail = new OrderDetail((int)Tools::getValue('product_id_order_detail'));
        if (Tools::isSubmit('product_invoice')) {
            $order_invoice = new OrderInvoice((int)Tools::getValue('product_invoice'));
        }

        // If multiple product_quantity, the order details concern a product customized
        $product_quantity = 0;
        if (is_array(Tools::getValue('product_quantity'))) {
            foreach (Tools::getValue('product_quantity') as $id_customization => $qty) {
                // Update quantity of each customization
                Db::getInstance()->update('customization', array('quantity' => (int)$qty), 'id_customization = ' . (int)$id_customization);
                // Calculate the real quantity of the product
                $product_quantity += $qty;
            }
        } else {
            $product_quantity = Tools::getValue('product_quantity');
        }

        $this->checkStockAvailable($order_detail, ($product_quantity - $order_detail->product_quantity));

        // Check fields validity
        $this->doEditProductValidation($order_detail, $order, isset($order_invoice) ? $order_invoice : null);

        // If multiple product_quantity, the order details concern a product customized
        $product_quantity = 0;
        if (is_array(Tools::getValue('product_quantity'))) {
            foreach (Tools::getValue('product_quantity') as $id_customization => $qty) {
                // Update quantity of each customization
                Db::getInstance()->update('customization', array('quantity' => (int)$qty), 'id_customization = '.(int)$id_customization);
                // Calculate the real quantity of the product
                $product_quantity += $qty;
            }
        } else {
            $product_quantity = Tools::getValue('product_quantity');
        }

        $product_price_tax_incl = Tools::ps_round(Tools::getValue('product_price_tax_incl'), 2);
        $product_price_tax_excl = Tools::ps_round(Tools::getValue('product_price_tax_excl'), 2);
        $total_products_tax_incl = $product_price_tax_incl * $product_quantity;
        $total_products_tax_excl = $product_price_tax_excl * $product_quantity;

        // Calculate differences of price (Before / After)
        $diff_price_tax_incl = $total_products_tax_incl - $order_detail->total_price_tax_incl;
        $diff_price_tax_excl = $total_products_tax_excl - $order_detail->total_price_tax_excl;

        // Apply change on OrderInvoice
        if (isset($order_invoice)) {
            // If OrderInvoice to use is different, we update the old invoice and new invoice
            if ($order_detail->id_order_invoice != $order_invoice->id) {
                $old_order_invoice = new OrderInvoice($order_detail->id_order_invoice);
                // We remove cost of products
                $old_order_invoice->total_products -= $order_detail->total_price_tax_excl;
                $old_order_invoice->total_products_wt -= $order_detail->total_price_tax_incl;

                $old_order_invoice->total_paid_tax_excl -= $order_detail->total_price_tax_excl;
                $old_order_invoice->total_paid_tax_incl -= $order_detail->total_price_tax_incl;

                $res &= $old_order_invoice->update();

                $order_invoice->total_products += $order_detail->total_price_tax_excl;
                $order_invoice->total_products_wt += $order_detail->total_price_tax_incl;

                $order_invoice->total_paid_tax_excl += $order_detail->total_price_tax_excl;
                $order_invoice->total_paid_tax_incl += $order_detail->total_price_tax_incl;

                $order_detail->id_order_invoice = $order_invoice->id;
            }
        }

        if ($diff_price_tax_incl != 0 && $diff_price_tax_excl != 0) {
            $order_detail->unit_price_tax_excl = $product_price_tax_excl;
            $order_detail->unit_price_tax_incl = $product_price_tax_incl;

            $order_detail->total_price_tax_incl += $diff_price_tax_incl;
            $order_detail->total_price_tax_excl += $diff_price_tax_excl;

            if (isset($order_invoice)) {
                // Apply changes on OrderInvoice
                $order_invoice->total_products += $diff_price_tax_excl;
                $order_invoice->total_products_wt += $diff_price_tax_incl;

                $order_invoice->total_paid_tax_excl += $diff_price_tax_excl;
                $order_invoice->total_paid_tax_incl += $diff_price_tax_incl;
            }

            // Apply changes on Order
            $order = new Order($order_detail->id_order);
            $order->total_products += $diff_price_tax_excl;
            $order->total_products_wt += $diff_price_tax_incl;

            $order->total_paid += $diff_price_tax_incl;
            $order->total_paid_tax_excl += $diff_price_tax_excl;
            $order->total_paid_tax_incl += $diff_price_tax_incl;

            $res &= $order->update();
        }

        $old_quantity = $order_detail->product_quantity;

        $order_detail->product_quantity = $product_quantity;
        $order_detail->reduction_percent = 0;

        // update taxes
        $res &= $order_detail->updateTaxAmount($order);

        // Save order detail
        $res &= $order_detail->update();

        // Update weight SUM
        $order_carrier = new OrderCarrier((int)$order->getIdOrderCarrier());
        if (Validate::isLoadedObject($order_carrier)) {
            $order_carrier->weight = (float)$order->getTotalWeight();
            $res &= $order_carrier->update();
            if ($res) {
                $order->weight = sprintf("%.3f ".Configuration::get('PS_WEIGHT_UNIT'), $order_carrier->weight);
            }
        }

        // Save order invoice
        if (isset($order_invoice)) {
            $res &= $order_invoice->update();
        }

        // Update product available quantity
        StockAvailable::updateQuantity($order_detail->product_id, $order_detail->product_attribute_id, ($old_quantity - $order_detail->product_quantity), $order->id_shop);

        $products = $this->getProductsModificata($order);
        // Get the last product
        $product = $products[$order_detail->id];
        $product['current_stock'] = StockAvailable::getQuantityAvailableByProduct($product['product_id'], $product['product_attribute_id'], $product['id_shop']);
        $resume = OrderSlip::getProductSlipResume($order_detail->id);
        $product['quantity_refundable'] = $product['product_quantity'] - $resume['product_quantity'];
        $product['amount_refundable'] = $product['total_price_tax_excl'] - $resume['amount_tax_excl'];
        $product['amount_refund'] = Tools::displayPrice($resume['amount_tax_incl']);
        $product['refund_history'] = OrderSlip::getProductSlipDetail($order_detail->id);
        if ($product['id_warehouse'] != 0) {
            $warehouse = new Warehouse((int)$product['id_warehouse']);
            $product['warehouse_name'] = $warehouse->name;
            $warehouse_location = WarehouseProductLocation::getProductLocation($product['product_id'], $product['product_attribute_id'], $product['id_warehouse']);
            if (!empty($warehouse_location)) {
                $product['warehouse_location'] = $warehouse_location;
            } else {
                $product['warehouse_location'] = false;
            }
        } else {
            $product['warehouse_name'] = '--';
            $product['warehouse_location'] = false;
        }

        // Get invoices collection
        $invoice_collection = $order->getInvoicesCollection();

        $invoice_array = array();
        foreach ($invoice_collection as $invoice) {
            /** @var OrderInvoice $invoice */
            $invoice->name = $invoice->getInvoiceNumberFormatted(Context::getContext()->language->id, (int)$order->id_shop);
            $invoice_array[] = $invoice;
        }

        // Assign to smarty informations in order to show the new product line
        $this->context->smarty->assign(array(
            'product' => $product,
            'order' => $order,
            'currency' => new Currency($order->id_currency),
            'can_edit' => $this->tabAccess['edit'],
            'invoices_collection' => $invoice_collection,
            'current_id_lang' => Context::getContext()->language->id,
            'link' => Context::getContext()->link,
            'current_index' => self::$currentIndex,
            'display_warehouse' => (int)Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')
        ));

        if (!$res) {
            die(Tools::jsonEncode(array(
                'result' => $res,
                'error' => Tools::displayError('An error occurred while editing the product line.')
            )));
        }


        if (is_array(Tools::getValue('product_quantity'))) {
            $view = $this->createTemplate('_customized_data.tpl')->fetch();
        } else {
            $view = $this->createTemplate('_product_line.tpl')->fetch();
        }

        $this->sendChangedNotification($order);

        die(Tools::jsonEncode(array(
            'result' => $res,
            'view' => $view,
            'can_edit' => $this->tabAccess['add'],
            'invoices_collection' => $invoice_collection,
            'order' => $order,
            'invoices' => $invoice_array,
            'documents_html' => $this->createTemplate('_documents.tpl')->fetch(),
            'shipping_html' => $this->createTemplate('_shipping.tpl')->fetch(),
            'customized_product' => is_array(Tools::getValue('product_quantity'))
        )));
    }

    public function ajaxProcessDeleteProductLine()
    {
        $res = true;

        $order_detail = new OrderDetail((int)Tools::getValue('id_order_detail'));
        $order = new Order((int)Tools::getValue('id_order'));

        $this->doDeleteProductLineValidation($order_detail, $order);

        // Update OrderInvoice of this OrderDetail
        if ($order_detail->id_order_invoice != 0) {
            $order_invoice = new OrderInvoice($order_detail->id_order_invoice);
            $order_invoice->total_paid_tax_excl -= $order_detail->total_price_tax_excl;
            $order_invoice->total_paid_tax_incl -= $order_detail->total_price_tax_incl;
            $order_invoice->total_products -= $order_detail->total_price_tax_excl;
            $order_invoice->total_products_wt -= $order_detail->total_price_tax_incl;
            $res &= $order_invoice->update();
        }

        // Update Order
        $order->total_paid -= $order_detail->total_price_tax_incl;
        $order->total_paid_tax_incl -= $order_detail->total_price_tax_incl;
        $order->total_paid_tax_excl -= $order_detail->total_price_tax_excl;
        $order->total_products -= $order_detail->total_price_tax_excl;
        $order->total_products_wt -= $order_detail->total_price_tax_incl;

        $res &= $order->update();

        // Reinject quantity in stock
        $this->reinjectQuantity($order_detail, $order_detail->product_quantity, true);

        // Update weight SUM
        $order_carrier = new OrderCarrier((int)$order->getIdOrderCarrier());
        if (Validate::isLoadedObject($order_carrier)) {
            $order_carrier->weight = (float)$order->getTotalWeight();
            $res &= $order_carrier->update();
            if ($res) {
                $order->weight = sprintf("%.3f ".Configuration::get('PS_WEIGHT_UNIT'), $order_carrier->weight);
            }
        }

        if (!$res) {
            die(Tools::jsonEncode(array(
                'result' => $res,
                'error' => Tools::displayError('An error occurred while attempting to delete the product line.')
            )));
        }

        // Get invoices collection
        $invoice_collection = $order->getInvoicesCollection();

        $invoice_array = array();
        foreach ($invoice_collection as $invoice) {
            /** @var OrderInvoice $invoice */
            $invoice->name = $invoice->getInvoiceNumberFormatted(Context::getContext()->language->id, (int)$order->id_shop);
            $invoice_array[] = $invoice;
        }

        // Assign to smarty informations in order to show the new product line
        $this->context->smarty->assign(array(
            'order' => $order,
            'currency' => new Currency($order->id_currency),
            'invoices_collection' => $invoice_collection,
            'current_id_lang' => Context::getContext()->language->id,
            'link' => Context::getContext()->link,
            'current_index' => self::$currentIndex
        ));

        $this->sendChangedNotification($order);

        die(Tools::jsonEncode(array(
            'result' => $res,
            'order' => $order,
            'invoices' => $invoice_array,
            'documents_html' => $this->createTemplate('_documents.tpl')->fetch(),
            'shipping_html' => $this->createTemplate('_shipping.tpl')->fetch()
        )));
    }

    protected function doEditProductValidation(OrderDetail $order_detail, Order $order, OrderInvoice $order_invoice = null)
    {
        if (!Validate::isLoadedObject($order_detail)) {
            die(Tools::jsonEncode(array(
                'result' => false,
                'error' => Tools::displayError('The Order Detail object could not be loaded.')
            )));
        }

        if (!empty($order_invoice) && !Validate::isLoadedObject($order_invoice)) {
            die(Tools::jsonEncode(array(
                'result' => false,
                'error' => Tools::displayError('The invoice object cannot be loaded.')
            )));
        }

        if (!Validate::isLoadedObject($order)) {
            die(Tools::jsonEncode(array(
                'result' => false,
                'error' => Tools::displayError('The order object cannot be loaded.')
            )));
        }

        if ($order_detail->id_order != $order->id) {
            die(Tools::jsonEncode(array(
                'result' => false,
                'error' => Tools::displayError('You cannot edit the order detail for this order.')
            )));
        }

        // We can't edit a delivered order
        if ($order->hasBeenDelivered()) {
            die(Tools::jsonEncode(array(
                'result' => false,
                'error' => Tools::displayError('You cannot edit a delivered order.')
            )));
        }

        if (!empty($order_invoice) && $order_invoice->id_order != Tools::getValue('id_order')) {
            die(Tools::jsonEncode(array(
                'result' => false,
                'error' => Tools::displayError('You cannot use this invoice for the order')
            )));
        }

        // Clean price
        $product_price_tax_incl = str_replace(',', '.', Tools::getValue('product_price_tax_incl'));
        $product_price_tax_excl = str_replace(',', '.', Tools::getValue('product_price_tax_excl'));

        if (!Validate::isPrice($product_price_tax_incl) || !Validate::isPrice($product_price_tax_excl)) {
            die(Tools::jsonEncode(array(
                'result' => false,
                'error' => Tools::displayError('Invalid price')
            )));
        }

        if (!is_array(Tools::getValue('product_quantity')) && !Validate::isUnsignedInt(Tools::getValue('product_quantity'))) {
            die(Tools::jsonEncode(array(
                'result' => false,
                'error' => Tools::displayError('Invalid quantity')
            )));
        } elseif (is_array(Tools::getValue('product_quantity'))) {
            foreach (Tools::getValue('product_quantity') as $qty) {
                if (!Validate::isUnsignedInt($qty)) {
                    die(Tools::jsonEncode(array(
                        'result' => false,
                        'error' => Tools::displayError('Invalid quantity')
                    )));
                }
            }
        }
    }

    protected function doDeleteProductLineValidation(OrderDetail $order_detail, Order $order)
    {
        if (!Validate::isLoadedObject($order_detail)) {
            die(Tools::jsonEncode(array(
                'result' => false,
                'error' => Tools::displayError('The Order Detail object could not be loaded.')
            )));
        }

        if (!Validate::isLoadedObject($order)) {
            die(Tools::jsonEncode(array(
                'result' => false,
                'error' => Tools::displayError('The order object cannot be loaded.')
            )));
        }

        if ($order_detail->id_order != $order->id) {
            die(Tools::jsonEncode(array(
                'result' => false,
                'error' => Tools::displayError('You cannot delete the order detail.')
            )));
        }

        // We can't edit a delivered order
        if ($order->hasBeenDelivered()) {
            die(Tools::jsonEncode(array(
                'result' => false,
                'error' => Tools::displayError('You cannot edit a delivered order.')
            )));
        }
    }

    /**
     * @param $order_detail
     * @param $add_quantity
     */
    protected function checkStockAvailable($order_detail, $add_quantity)
    {
        if ($add_quantity > 0) {
            $StockAvailable = StockAvailable::getQuantityAvailableByProduct($order_detail->product_id, $order_detail->product_attribute_id, $order_detail->id_shop);
            $product = new Product($order_detail->product_id, true, null, $order_detail->id_shop);
            if (!Validate::isLoadedObject($product)) {
                die(Tools::jsonEncode(array(
                    'result' => false,
                    'error' => Tools::displayError('The Product object could not be loaded.')
                )));
            } else {
                if (($StockAvailable < $add_quantity) && (!$product->isAvailableWhenOutOfStock((int)$product->out_of_stock))) {
                    die(Tools::jsonEncode(array(
                        'result' => false,
                        'error' => Tools::displayError('This product is no longer in stock with those attributes ')
                    )));

                }
            }
        }
    }

    /**
     * @param Order $order
     * @return array
     */
    protected function getProducts($order)
    {
        $products = $order->getProducts();

        foreach ($products as &$product) {
            if ($product['image'] != null) {
                $name = 'product_mini_'.(int)$product['product_id'].(isset($product['product_attribute_id']) ? '_'.(int)$product['product_attribute_id'] : '').'.jpg';
                // generate image cache, only for back office
                $product['image_tag'] = ImageManager::thumbnail(_PS_IMG_DIR_.'p/'.$product['image']->getExistingImgPath().'.jpg', $name, 45, 'jpg');
                if (file_exists(_PS_TMP_IMG_DIR_.$name)) {
                    $product['image_size'] = getimagesize(_PS_TMP_IMG_DIR_.$name);
                } else {
                    $product['image_size'] = false;
                }
            }
        }

        ksort($products);

        return $products;
    }

    // Override di getProducts: aggiunti i dati da visualizzare nella tabella Prodotti
    protected function getProductsModificata($order)
    {
        $products = $order->getProducts();

        foreach ($products as &$product) {
            if ($product['image'] != null) {
                $name = 'product_mini_'.(int)$product['product_id'].(isset($product['product_attribute_id']) ? '_'.(int)$product['product_attribute_id'] : '').'.jpg';
                // generate image cache, only for back office
                $product['image_tag'] = ImageManager::thumbnail(_PS_IMG_DIR_.'p/'.$product['image']->getExistingImgPath().'.jpg', $name, 45, 'jpg');
                if (file_exists(_PS_TMP_IMG_DIR_.$name)) {
                    $product['image_size'] = getimagesize(_PS_TMP_IMG_DIR_.$name);
                } else {
                    $product['image_size'] = false;
                }
            }
            
            /* OVERRIDE */
            
            // aggiungi permessi impiegati e altre regole!

            $product['tooltip'] = Product::showProductTooltip($product['product_id']);
            // quantity
            // cons -> da vedere
            // $product['ddt']

            if ($order->getTaxCalculationMethod() == PS_TAX_EXC) // CORREGGERE!!!!!
                $product_price = $product['product_price'] + $product['ecotax'];
            else
                $product_price = $product['product_price_wt'];

            if($product['wholesale_price'] > 0)
                $wholesale_price = $product['wholesale_price'];
            else if($product['no_acq'] == 1){
                $wholesale_price = 0;
                $product['wholesale_price'] = 0;
            }
            //else // eliminare? Non penso serva più facendo join con product nella funzione nella classe
            
            // tpl: se $is_agente, non vedo acquisto e marginalità
            if(!$is_agente){
                $margin = (($product_price - $wholesale_price) * 100) / $product_price;
                $product['margin'] = number_format($margin, 2, ",", ""); // %
            }

            // Tpl: ($customer->id_default_group == 3 ? 'Sc.rv?' : 'Sc.qt?')

            // tpl: se $unitario > 0 => stampo sconto extra sennò td vuoto
            $sconto_extra = Db::getInstance()->getValue('
                SELECT sconto_extra 
                FROM '._DB_PREFIX_.'cart_product 
                WHERE id_product = '.$product['product_id'].' 
                    AND id_cart = '.$order->id_cart
            );

            $product['prezzo_unitario'] = $product_price;
            $product['sconto_extra'] = number_format($sconto_extra, 2, ",", ""); // %
            $product['wholesale_price'] = number_format($wholesale_price, 2, ",", "");
            //$product['margin']: calcolato sopra

            // qt disp all'ordine (controllare anche ['current_stock'])
			//$product['qt_disp'] = ($order->id >= 23171 ? '<td>'.($virtual <= 0 ? (int)$product['product_quantity_in_stock'] : '').'</td>' : '')

            //$product['rimanenze'] = ($virtual <= 0 ? (int)$stock['quantity'] : '');
            $product['kit'] = Product::controllaSeFaParteDiUnBundle($order->id, $product['product_id']);

            // totale:
            // Tools::displayPrice(Tools::ps_round($product_price, 2) * ((int)($product['product_quantity']) - $product['customizationQuantityTotal']), $currency, false)

            /* FINE OVERRIDE */
        }

        ksort($products);

        return $products;
    }

    /**
     * @param OrderDetail $order_detail
     * @param int $qty_cancel_product
     * @param bool $delete
     */
    protected function reinjectQuantity($order_detail, $qty_cancel_product, $delete = false)
    {
        // Reinject product
        $reinjectable_quantity = (int)$order_detail->product_quantity - (int)$order_detail->product_quantity_reinjected;
        $quantity_to_reinject = $qty_cancel_product > $reinjectable_quantity ? $reinjectable_quantity : $qty_cancel_product;
        // @since 1.5.0 : Advanced Stock Management
        $product_to_inject = new Product($order_detail->product_id, false, (int)$this->context->language->id, (int)$order_detail->id_shop);

        $product = new Product($order_detail->product_id, false, (int)$this->context->language->id, (int)$order_detail->id_shop);

        if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && $product->advanced_stock_management && $order_detail->id_warehouse != 0) {
            $manager = StockManagerFactory::getManager();
            $movements = StockMvt::getNegativeStockMvts(
                                $order_detail->id_order,
                                $order_detail->product_id,
                                $order_detail->product_attribute_id,
                                $quantity_to_reinject
                            );
            $left_to_reinject = $quantity_to_reinject;
            foreach ($movements as $movement) {
                if ($left_to_reinject > $movement['physical_quantity']) {
                    $quantity_to_reinject = $movement['physical_quantity'];
                }

                $left_to_reinject -= $quantity_to_reinject;
                if (Pack::isPack((int)$product->id)) {
                    // Gets items
                        if ($product->pack_stock_type == 1 || $product->pack_stock_type == 2 || ($product->pack_stock_type == 3 && Configuration::get('PS_PACK_STOCK_TYPE') > 0)) {
                            $products_pack = Pack::getItems((int)$product->id, (int)Configuration::get('PS_LANG_DEFAULT'));
                            // Foreach item
                            foreach ($products_pack as $product_pack) {
                                if ($product_pack->advanced_stock_management == 1) {
                                    $manager->addProduct(
                                        $product_pack->id,
                                        $product_pack->id_pack_product_attribute,
                                        new Warehouse($movement['id_warehouse']),
                                        $product_pack->pack_quantity * $quantity_to_reinject,
                                        null,
                                        $movement['price_te'],
                                        true
                                    );
                                }
                            }
                        }
                    if ($product->pack_stock_type == 0 || $product->pack_stock_type == 2 ||
                            ($product->pack_stock_type == 3 && (Configuration::get('PS_PACK_STOCK_TYPE') == 0 || Configuration::get('PS_PACK_STOCK_TYPE') == 2))) {
                        $manager->addProduct(
                                $order_detail->product_id,
                                $order_detail->product_attribute_id,
                                new Warehouse($movement['id_warehouse']),
                                $quantity_to_reinject,
                                null,
                                $movement['price_te'],
                                true
                            );
                    }
                } else {
                    $manager->addProduct(
                            $order_detail->product_id,
                            $order_detail->product_attribute_id,
                            new Warehouse($movement['id_warehouse']),
                            $quantity_to_reinject,
                            null,
                            $movement['price_te'],
                            true
                        );
                }
            }

            $id_product = $order_detail->product_id;
            if ($delete) {
                $order_detail->delete();
            }
            StockAvailable::synchronize($id_product);
        } elseif ($order_detail->id_warehouse == 0) {
            StockAvailable::updateQuantity(
                    $order_detail->product_id,
                    $order_detail->product_attribute_id,
                    $quantity_to_reinject,
                    $order_detail->id_shop
                );

            if ($delete) {
                $order_detail->delete();
            }
        } else {
            $this->errors[] = Tools::displayError('This product cannot be re-stocked.');
        }
    }

    /**
     * @param OrderInvoice $order_invoice
     * @param float $value_tax_incl
     * @param float $value_tax_excl
     */
    protected function applyDiscountOnInvoice($order_invoice, $value_tax_incl, $value_tax_excl)
    {
        // Update OrderInvoice
        $order_invoice->total_discount_tax_incl += $value_tax_incl;
        $order_invoice->total_discount_tax_excl += $value_tax_excl;
        $order_invoice->total_paid_tax_incl -= $value_tax_incl;
        $order_invoice->total_paid_tax_excl -= $value_tax_excl;
        $order_invoice->update();
    }

    public function ajaxProcessChangePaymentMethod()
    {
        $customer = new Customer(Tools::getValue('id_customer'));
        $modules = Module::getAuthorizedModules($customer->id_default_group);
        $authorized_modules = array();

        if (!Validate::isLoadedObject($customer) || !is_array($modules)) {
            die(Tools::jsonEncode(array('result' => false)));
        }

        foreach ($modules as $module) {
            $authorized_modules[] = (int)$module['id_module'];
        }

        $payment_modules = array();

        foreach (PaymentModule::getInstalledPaymentModules() as $p_module) {
            if (in_array((int)$p_module['id_module'], $authorized_modules)) {
                $payment_modules[] = Module::getInstanceById((int)$p_module['id_module']);
            }
        }

        $this->context->smarty->assign(array(
            'payment_modules' => $payment_modules,
        ));

        die(Tools::jsonEncode(array(
            'result' => true,
            'view' => $this->createTemplate('_select_payment.tpl')->fetch(),
        )));
    }
}
