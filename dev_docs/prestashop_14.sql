SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `ezdirect_presta` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `ezdirect_presta`;

CREATE TABLE `access` (
  `id_profile` int(10) UNSIGNED NOT NULL,
  `id_tab` int(10) UNSIGNED NOT NULL,
  `view` int(11) NOT NULL,
  `add` int(11) NOT NULL,
  `edit` int(11) NOT NULL,
  `delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `accessory` (
  `id_product_1` int(10) UNSIGNED NOT NULL,
  `id_product_2` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `action_message` (
  `id_action_message` int(11) NOT NULL,
  `id_action` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `action_m_from` int(11) NOT NULL,
  `action_m_to` int(11) NOT NULL,
  `in_carico` int(11) NOT NULL,
  `action_message` text NOT NULL,
  `file_name` varchar(300) NOT NULL,
  `telefono_cliente` varchar(64) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `action_message_employee` (
  `id_action_message_employee` int(11) NOT NULL,
  `id_action` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `action_m_from` int(11) NOT NULL,
  `action_m_to` int(11) NOT NULL,
  `in_carico` int(11) NOT NULL,
  `action_message_employee` text NOT NULL,
  `file_name` varchar(300) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `action_thread` (
  `id_action` int(11) NOT NULL,
  `action_type` varchar(200) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `subject` varchar(300) NOT NULL,
  `action_from` int(11) NOT NULL,
  `action_to` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `riferimento` varchar(50) NOT NULL,
  `action_date` datetime NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `note` text NOT NULL,
  `promemoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `action_thread_employee` (
  `id_action` int(11) NOT NULL,
  `action_type` varchar(200) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `subject` varchar(300) NOT NULL,
  `action_from` int(11) NOT NULL,
  `action_to` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `riferimento` varchar(50) NOT NULL,
  `action_date` datetime NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `note` text NOT NULL,
  `promemoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `address` (
  `id_address` int(10) UNSIGNED NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_state` int(10) UNSIGNED DEFAULT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_manufacturer` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_supplier` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `alias` varchar(80) NOT NULL,
  `company` varchar(400) DEFAULT NULL,
  `lastname` varchar(32) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `address1` varchar(128) NOT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `postcode` varchar(12) DEFAULT NULL,
  `suburb` varchar(200) NOT NULL,
  `c_o` varchar(200) NOT NULL,
  `city` varchar(64) NOT NULL,
  `other` text,
  `phone` varchar(32) NOT NULL,
  `phone_mobile` varchar(16) DEFAULT NULL,
  `fax` varchar(32) NOT NULL,
  `vat_number` varchar(32) DEFAULT NULL,
  `tax_code` varchar(32) NOT NULL,
  `company_tax_code` varchar(32) NOT NULL,
  `dni` varchar(16) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `fatturazione` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `address_book` (
  `address_book_id` int(11) NOT NULL,
  `customers_id` int(11) NOT NULL DEFAULT '0',
  `entry_is_company` char(1) NOT NULL DEFAULT '1',
  `entry_gender` char(1) NOT NULL DEFAULT '',
  `entry_company` varchar(32) DEFAULT NULL,
  `entry_cf` varchar(16) NOT NULL DEFAULT '',
  `entry_company_cf` varchar(16) NOT NULL DEFAULT '',
  `entry_piva` varchar(11) NOT NULL DEFAULT '',
  `entry_firstname` varchar(32) NOT NULL DEFAULT '',
  `entry_lastname` varchar(32) NOT NULL DEFAULT '',
  `entry_street_address` varchar(64) NOT NULL DEFAULT '',
  `entry_suburb` varchar(32) DEFAULT NULL,
  `entry_postcode` varchar(10) NOT NULL DEFAULT '',
  `entry_city` varchar(32) NOT NULL DEFAULT '',
  `entry_state` varchar(32) DEFAULT NULL,
  `entry_country_id` int(11) NOT NULL DEFAULT '0',
  `entry_zone_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `address_format` (
  `id_country` int(10) UNSIGNED NOT NULL,
  `format` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `agente` (
  `id_agente` int(11) NOT NULL,
  `company` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `id_state` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `profilo` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pec` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `phone_mobile` varchar(100) NOT NULL,
  `tax_code` varchar(50) NOT NULL,
  `vat_number` varchar(50) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `alias` (
  `id_alias` int(10) UNSIGNED NOT NULL,
  `alias` varchar(255) NOT NULL,
  `search` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `allnet_prices` (
  `reference` varchar(300) NOT NULL,
  `wholesale_price` decimal(10,2) NOT NULL,
  `listino` decimal(10,2) NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `alternative` (
  `id_product_1` int(11) NOT NULL,
  `id_product_2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `amazon_customers` (
  `id_customer` int(11) NOT NULL,
  `id_amazon` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_orders` (
  `id_order` int(11) NOT NULL,
  `id_order_amazon` varchar(100) NOT NULL,
  `data_ordine` varchar(50) NOT NULL,
  `tipo` varchar(10) NOT NULL,
  `marketplace` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_products` (
  `id_product` int(11) NOT NULL,
  `it` int(11) NOT NULL,
  `fr` int(11) NOT NULL,
  `uk` int(11) NOT NULL,
  `es` int(11) NOT NULL,
  `de` int(11) NOT NULL,
  `price` decimal(10,6) NOT NULL,
  `qt_prime` int(11) NOT NULL,
  `price_prime` float NOT NULL,
  `qt_ez` int(11) NOT NULL,
  `qt_supplier` int(11) NOT NULL,
  `last_price` decimal(10,6) NOT NULL,
  `qt_last` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `amazon_verifica_pagamenti` (
  `id_pagamento` varchar(50) NOT NULL,
  `verificato` int(11) NOT NULL,
  `data_verifica` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `attachment` (
  `id_attachment` int(10) UNSIGNED NOT NULL,
  `file` varchar(40) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `mime` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `attachment_lang` (
  `id_attachment` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `attribute` (
  `id_attribute` int(10) UNSIGNED NOT NULL,
  `id_attribute_group` int(10) UNSIGNED NOT NULL,
  `color` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `attribute_group` (
  `id_attribute_group` int(10) UNSIGNED NOT NULL,
  `is_color_group` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `attribute_group_lang` (
  `id_attribute_group` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `public_name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `attribute_impact` (
  `id_attribute_impact` int(10) UNSIGNED NOT NULL,
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_attribute` int(11) UNSIGNED NOT NULL,
  `weight` float NOT NULL,
  `price` decimal(17,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `attribute_lang` (
  `id_attribute` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `bdl` (
  `id_bdl` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_address` int(11) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `data_richiesta` datetime NOT NULL,
  `richiesto_da` int(11) NOT NULL,
  `data_effettuato` datetime NOT NULL,
  `effettuato_da` int(11) NOT NULL,
  `impianto_ubicazione` text NOT NULL,
  `contratto_assistenza` int(11) NOT NULL,
  `manutenzione_ordinaria` varchar(100) NOT NULL,
  `manutenzione_straordinaria` varchar(100) NOT NULL,
  `motivo_chiamata` text NOT NULL,
  `guasto` text NOT NULL,
  `causa_guasto` text NOT NULL,
  `descrizione` text NOT NULL,
  `intervento_svolto` text NOT NULL,
  `prodotti` text NOT NULL,
  `dettaglio_costi` text NOT NULL,
  `importo` decimal(10,2) NOT NULL,
  `note` text NOT NULL,
  `riferimento` varchar(50) NOT NULL,
  `note_private` text NOT NULL,
  `rif_ordine` int(11) NOT NULL,
  `pagato` int(11) NOT NULL,
  `fatturato` int(11) NOT NULL,
  `invio_controllo` int(11) NOT NULL,
  `invio_contabilita` int(11) NOT NULL,
  `nessun_addebito` int(11) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `bundle` (
  `id_bundle` int(11) NOT NULL,
  `bundle_name` varchar(200) NOT NULL,
  `bundle_ref` varchar(200) NOT NULL,
  `ean13` varchar(15) NOT NULL,
  `asin` varchar(15) NOT NULL,
  `sku_amazon` varchar(20) NOT NULL,
  `fnsku` varchar(15) NOT NULL,
  `father` int(11) NOT NULL,
  `id_category_default` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `bundle_products` text NOT NULL,
  `bundle_prices` varchar(400) NOT NULL,
  `priceCLI` decimal(10,2) NOT NULL,
  `priceRIV` decimal(10,2) NOT NULL,
  `estensione_garanzia` int(11) NOT NULL,
  `supporto_da_remoto` int(11) NOT NULL,
  `telegestione` int(11) NOT NULL,
  `comparaprezzi_check` int(11) NOT NULL,
  `trovaprezzi` int(11) NOT NULL,
  `google_shopping` int(11) NOT NULL,
  `amazon` varchar(11) NOT NULL,
  `active` int(11) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `bundle_image` (
  `id_image` int(11) NOT NULL,
  `id_bundle` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `bundle_image_lang` (
  `id_image` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `legend` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `bundle_lang` (
  `id_bundle` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `description_amazon` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `campi_esolver` (
  `id_product` int(11) NOT NULL,
  `tipo_anagrafica` int(11) NOT NULL,
  `modello` int(11) NOT NULL,
  `ean_descrizione` varchar(50) NOT NULL,
  `ean_tipo` varchar(20) NOT NULL,
  `ean_quantita` int(11) NOT NULL,
  `intrastat` varchar(20) NOT NULL,
  `name_short` varchar(50) NOT NULL,
  `altro_esolver` varchar(150) NOT NULL,
  `tempo_approvvigionamento` varchar(50) NOT NULL,
  `codice_barre_fornitore` varchar(50) NOT NULL,
  `pezzi_in_confezione` int(11) NOT NULL,
  `nr` int(11) NOT NULL,
  `pz` int(11) NOT NULL,
  `commissione_amazon` int(11) NOT NULL,
  `commissione_eprice` int(11) NOT NULL,
  `note_2` text NOT NULL,
  `premio_target` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `cap` (
  `id` int(11) NOT NULL,
  `cod_istat` varchar(6) NOT NULL,
  `comune` varchar(200) NOT NULL,
  `prefisso` varchar(6) NOT NULL,
  `cap` varchar(9) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `carrelli_creati` (
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `preventivo` int(11) NOT NULL,
  `id_address_delivery` int(10) UNSIGNED NOT NULL,
  `id_address_invoice` int(10) UNSIGNED NOT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_guest` int(10) UNSIGNED NOT NULL,
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `recyclable` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `gift` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `gift_message` text,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `rif_prev` varchar(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `riferimento` varchar(10) NOT NULL,
  `premessa` text NOT NULL,
  `esigenze` text NOT NULL,
  `risorse` text NOT NULL,
  `note` text NOT NULL,
  `note_private` text NOT NULL,
  `validita` datetime NOT NULL,
  `consegna` varchar(300) NOT NULL,
  `transport` varchar(30) NOT NULL,
  `payment` varchar(150) NOT NULL,
  `total_products` decimal(10,2) NOT NULL,
  `attachment` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `in_carico_a` int(11) NOT NULL,
  `notifiche` text NOT NULL,
  `telegestione` text NOT NULL,
  `revisioni` int(11) NOT NULL,
  `email_preparate` varchar(350) NOT NULL,
  `numero_notifiche` int(11) NOT NULL,
  `data_ultima_notifica` datetime NOT NULL,
  `id_persona` int(11) NOT NULL,
  `provvisorio` int(11) NOT NULL,
  `visualizzato` int(11) NOT NULL,
  `prezzi_carrello` int(11) NOT NULL,
  `no_addebito_commissioni` int(11) NOT NULL,
  `rif_ordine` varchar(70) NOT NULL,
  `cig` varchar(40) NOT NULL,
  `cup` varchar(40) NOT NULL,
  `ipa` varchar(40) NOT NULL,
  `data_ordine_mepa` date NOT NULL,
  `template` varchar(200) NOT NULL,
  `competenza_dal` date NOT NULL,
  `competenza_al` date NOT NULL,
  `cadenza` varchar(50) NOT NULL,
  `scadenza` varchar(50) NOT NULL,
  `decorrenza` varchar(50) NOT NULL,
  `esito` int(11) NOT NULL,
  `causa` varchar(150) NOT NULL,
  `concorrente` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `carrelli_creati_prodotti` (
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `price` decimal(10,2) NOT NULL,
  `free` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `sc_qta` int(11) NOT NULL,
  `sconto_extra` decimal(10,2) NOT NULL,
  `prezzo_acquisto` decimal(10,2) NOT NULL,
  `no_acq` int(11) NOT NULL,
  `id_specific_price` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `bundle` int(11) NOT NULL,
  `section` varchar(10) NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `carrier` (
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_tax_rules_group` int(10) UNSIGNED DEFAULT '0',
  `name` varchar(64) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `shipping_handling` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `range_behavior` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_module` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_free` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `shipping_external` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `need_range` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `external_module_name` varchar(64) DEFAULT NULL,
  `shipping_method` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `carrier_group` (
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `carrier_lang` (
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `delay` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `carrier_zone` (
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_zone` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cart` (
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `preventivo` int(11) NOT NULL,
  `id_address_delivery` int(10) UNSIGNED NOT NULL,
  `id_address_invoice` int(10) UNSIGNED NOT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_guest` int(10) UNSIGNED NOT NULL,
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `recyclable` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `gift` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `gift_message` text,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `rif_prev` varchar(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `riferimento` varchar(10) NOT NULL,
  `premessa` text NOT NULL,
  `esigenze` text NOT NULL,
  `risorse` text NOT NULL,
  `note` text NOT NULL,
  `note_private` text NOT NULL,
  `validita` datetime NOT NULL,
  `consegna` varchar(300) NOT NULL,
  `transport` varchar(30) NOT NULL,
  `payment` varchar(150) NOT NULL,
  `total_products` decimal(10,2) NOT NULL,
  `attachment` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `in_carico_a` int(11) NOT NULL,
  `notifiche` text NOT NULL,
  `telegestione` text NOT NULL,
  `revisioni` int(11) NOT NULL,
  `email_preparate` varchar(350) NOT NULL,
  `numero_notifiche` int(11) NOT NULL,
  `data_ultima_notifica` datetime NOT NULL,
  `id_persona` int(11) NOT NULL,
  `provvisorio` int(11) NOT NULL,
  `visualizzato` int(11) NOT NULL,
  `prezzi_carrello` int(11) NOT NULL,
  `no_addebito_commissioni` int(11) NOT NULL,
  `rif_ordine` varchar(70) NOT NULL,
  `cig` varchar(40) NOT NULL,
  `cup` varchar(40) NOT NULL,
  `ipa` varchar(40) NOT NULL,
  `data_ordine_mepa` date NOT NULL,
  `template` varchar(200) NOT NULL,
  `competenza_dal` date NOT NULL,
  `competenza_al` date NOT NULL,
  `cadenza` varchar(50) NOT NULL,
  `scadenza` varchar(50) NOT NULL,
  `decorrenza` varchar(50) NOT NULL,
  `esito` int(11) NOT NULL,
  `causa` varchar(150) NOT NULL,
  `concorrente` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cart_discount` (
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_discount` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cart_ezcloud` (
  `id_cart` int(11) NOT NULL,
  `periodicita` varchar(10) NOT NULL,
  `canone` varchar(50) NOT NULL,
  `interni` int(11) NOT NULL,
  `linee` int(11) NOT NULL,
  `canali` int(11) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `tipo_altro` varchar(50) NOT NULL,
  `rinnovo_automatico` int(11) NOT NULL,
  `configurazione_iniziale` int(11) NOT NULL,
  `attivazione` int(11) NOT NULL,
  `agente` int(11) NOT NULL,
  `allegato_a` int(11) NOT NULL,
  `check_contratti` text NOT NULL,
  `data_fatturazione_ezcloud` date NOT NULL,
  `canone_mensile` decimal(10,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cart_note` (
  `id_note` int(10) UNSIGNED NOT NULL,
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_employee` int(10) UNSIGNED NOT NULL,
  `note` text,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cart_product` (
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `price` decimal(10,2) NOT NULL,
  `free` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `sc_qta` int(11) NOT NULL,
  `sconto_extra` decimal(10,2) NOT NULL,
  `prezzo_acquisto` decimal(10,2) NOT NULL,
  `no_acq` int(11) NOT NULL,
  `id_specific_price` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `bundle` int(11) NOT NULL,
  `section` varchar(10) NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cart_product_revisioni` (
  `id_revisione` int(10) UNSIGNED NOT NULL,
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `price` decimal(10,2) NOT NULL,
  `free` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `sc_qta` int(11) NOT NULL,
  `sconto_extra` decimal(10,2) NOT NULL,
  `prezzo_acquisto` decimal(10,2) NOT NULL,
  `no_acq` int(11) NOT NULL,
  `id_specific_price` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `bundle` int(11) NOT NULL,
  `section` varchar(50) NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cart_revisioni` (
  `id_revisione` int(10) UNSIGNED NOT NULL,
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `preventivo` int(11) NOT NULL,
  `id_address_delivery` int(10) UNSIGNED NOT NULL,
  `id_address_invoice` int(10) UNSIGNED NOT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_guest` int(10) UNSIGNED NOT NULL,
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `recyclable` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `gift` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `gift_message` text,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `rif_prev` varchar(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `riferimento` varchar(10) NOT NULL,
  `premessa` text NOT NULL,
  `esigenze` text NOT NULL,
  `risorse` text NOT NULL,
  `note` text NOT NULL,
  `note_private` text NOT NULL,
  `validita` datetime NOT NULL,
  `consegna` varchar(300) NOT NULL,
  `transport` varchar(30) NOT NULL,
  `payment` varchar(150) NOT NULL,
  `total_products` decimal(10,2) NOT NULL,
  `attachment` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `in_carico_a` int(11) NOT NULL,
  `notifiche` text NOT NULL,
  `telegestione` text NOT NULL,
  `revisioni` int(11) NOT NULL,
  `email_preparate` varchar(350) NOT NULL,
  `numero_notifiche` int(11) NOT NULL,
  `data_ultima_notifica` datetime NOT NULL,
  `id_persona` int(11) NOT NULL,
  `provvisorio` int(11) NOT NULL,
  `visualizzato` int(11) NOT NULL,
  `prezzi_carrello` int(11) NOT NULL,
  `rif_ordine` varchar(70) NOT NULL,
  `cig` varchar(40) NOT NULL,
  `cup` varchar(40) NOT NULL,
  `ipa` varchar(40) NOT NULL,
  `data_ordine_mepa` date NOT NULL,
  `template` varchar(100) NOT NULL,
  `competenza_dal` date NOT NULL,
  `competenza_al` date NOT NULL,
  `cadenza` varchar(50) NOT NULL,
  `scadenza` varchar(50) NOT NULL,
  `decorrenza` varchar(50) NOT NULL,
  `esito` int(11) NOT NULL,
  `causa` varchar(150) NOT NULL,
  `concorrente` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `category` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_parent` int(10) UNSIGNED NOT NULL,
  `level_depth` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `nleft` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `nright` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `comparaprezzi_check` int(11) NOT NULL,
  `kelkoo` int(11) NOT NULL,
  `trovaprezzi` int(11) NOT NULL,
  `pangora` int(11) NOT NULL,
  `ciao` int(11) NOT NULL,
  `google_shopping` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `category_group` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `category_lang` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text,
  `link_rewrite` varchar(128) NOT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `breadcrumb` varchar(300) NOT NULL,
  `seo_keyword` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `category_product` (
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `category_product_gst` (
  `id_category` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `chatban` (
  `banid` int(11) NOT NULL,
  `dtmcreated` datetime DEFAULT '0000-00-00 00:00:00',
  `dtmtill` datetime DEFAULT '0000-00-00 00:00:00',
  `address` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `blockedCount` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chatconfig` (
  `id` int(11) NOT NULL,
  `vckey` varchar(255) DEFAULT NULL,
  `vcvalue` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chatgroup` (
  `groupid` int(11) NOT NULL,
  `vcemail` varchar(64) DEFAULT NULL,
  `vclocalname` varchar(64) NOT NULL,
  `vccommonname` varchar(64) NOT NULL,
  `vclocaldescription` varchar(1024) NOT NULL,
  `vccommondescription` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chatgroupoperator` (
  `groupid` int(11) NOT NULL,
  `operatorid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chatmessage` (
  `messageid` int(11) NOT NULL,
  `threadid` int(11) NOT NULL,
  `ikind` int(11) NOT NULL,
  `agentId` int(11) NOT NULL DEFAULT '0',
  `tmessage` text NOT NULL,
  `dtmcreated` datetime DEFAULT '0000-00-00 00:00:00',
  `tname` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chatoperator` (
  `operatorid` int(11) NOT NULL,
  `vclogin` varchar(64) NOT NULL,
  `vcpassword` varchar(64) NOT NULL,
  `vclocalename` varchar(64) NOT NULL,
  `vccommonname` varchar(64) NOT NULL,
  `vcemail` varchar(64) DEFAULT NULL,
  `dtmlastvisited` datetime DEFAULT '0000-00-00 00:00:00',
  `istatus` int(11) DEFAULT '0',
  `vcavatar` varchar(255) DEFAULT NULL,
  `vcjabbername` varchar(255) DEFAULT NULL,
  `iperm` int(11) DEFAULT '65535',
  `dtmrestore` datetime DEFAULT '0000-00-00 00:00:00',
  `vcrestoretoken` varchar(64) DEFAULT NULL,
  `code` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chatresponses` (
  `id` int(11) NOT NULL,
  `locale` varchar(8) DEFAULT NULL,
  `groupid` int(11) DEFAULT NULL,
  `vcvalue` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chatrevision` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chatthread` (
  `threadid` int(11) NOT NULL,
  `userName` varchar(64) NOT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `agentName` varchar(64) DEFAULT NULL,
  `agentId` int(11) NOT NULL DEFAULT '0',
  `dtmcreated` datetime DEFAULT '0000-00-00 00:00:00',
  `dtmmodified` datetime DEFAULT '0000-00-00 00:00:00',
  `dtmchatstarted` int(11) NOT NULL,
  `dtmclosed` int(11) NOT NULL,
  `lrevision` int(11) NOT NULL DEFAULT '0',
  `istate` int(11) NOT NULL DEFAULT '0',
  `invitationstate` int(11) NOT NULL,
  `ltoken` int(11) NOT NULL,
  `remote` varchar(255) DEFAULT NULL,
  `referer` text,
  `nextagent` int(11) NOT NULL DEFAULT '0',
  `locale` varchar(8) DEFAULT NULL,
  `lastpinguser` datetime DEFAULT '0000-00-00 00:00:00',
  `lastpingagent` datetime DEFAULT '0000-00-00 00:00:00',
  `userTyping` int(11) DEFAULT '0',
  `agentTyping` int(11) DEFAULT '0',
  `shownmessageid` int(11) NOT NULL DEFAULT '0',
  `userAgent` varchar(255) DEFAULT NULL,
  `messageCount` varchar(16) DEFAULT NULL,
  `groupid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_availableupdate` (
  `id` int(11) NOT NULL,
  `target` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `url` text,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_ban` (
  `banid` int(11) NOT NULL,
  `dtmcreated` int(11) NOT NULL DEFAULT '0',
  `dtmtill` int(11) NOT NULL DEFAULT '0',
  `address` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_cannedmessage` (
  `id` int(11) NOT NULL,
  `locale` varchar(8) DEFAULT NULL,
  `groupid` int(11) DEFAULT NULL,
  `vctitle` varchar(100) NOT NULL DEFAULT '',
  `vcvalue` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_config` (
  `id` int(11) NOT NULL,
  `vckey` varchar(255) DEFAULT NULL,
  `vcvalue` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_locale` (
  `localeid` int(11) NOT NULL,
  `code` varchar(5) NOT NULL,
  `name` varchar(128) NOT NULL DEFAULT '',
  `enabled` tinyint(4) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  `time_locale` varchar(128) NOT NULL DEFAULT 'en_US',
  `date_format` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_mailtemplate` (
  `templateid` int(11) NOT NULL,
  `locale` varchar(5) NOT NULL,
  `name` varchar(256) NOT NULL,
  `subject` varchar(1024) NOT NULL,
  `body` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_message` (
  `messageid` int(11) NOT NULL,
  `threadid` int(11) NOT NULL,
  `ikind` int(11) NOT NULL,
  `agentid` int(11) NOT NULL DEFAULT '0',
  `tmessage` text NOT NULL,
  `plugin` varchar(256) NOT NULL DEFAULT '',
  `data` text,
  `dtmcreated` int(11) NOT NULL DEFAULT '0',
  `tname` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_operator2` (
  `operatorid` int(11) NOT NULL,
  `vclogin` varchar(64) NOT NULL,
  `vcpassword` varchar(64) NOT NULL,
  `vclocalename` varchar(64) NOT NULL,
  `vccommonname` varchar(64) NOT NULL,
  `vcemail` varchar(64) DEFAULT NULL,
  `dtmlastvisited` int(11) NOT NULL DEFAULT '0',
  `istatus` int(11) DEFAULT '0',
  `idisabled` int(11) DEFAULT '0',
  `vcavatar` varchar(255) DEFAULT NULL,
  `iperm` int(11) DEFAULT '0',
  `dtmrestore` int(11) NOT NULL DEFAULT '0',
  `vcrestoretoken` varchar(64) DEFAULT NULL,
  `code` varchar(64) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_operatorstatistics` (
  `statid` int(11) NOT NULL,
  `date` int(11) NOT NULL DEFAULT '0',
  `operatorid` int(11) NOT NULL,
  `threads` int(11) NOT NULL DEFAULT '0',
  `messages` int(11) NOT NULL DEFAULT '0',
  `averagelength` float(10,1) NOT NULL DEFAULT '0.0',
  `sentinvitations` int(11) NOT NULL DEFAULT '0',
  `acceptedinvitations` int(11) NOT NULL DEFAULT '0',
  `rejectedinvitations` int(11) NOT NULL DEFAULT '0',
  `ignoredinvitations` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_operatortoopgroup` (
  `groupid` int(11) NOT NULL,
  `operatorid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_opgroup` (
  `groupid` int(11) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `vcemail` varchar(64) DEFAULT NULL,
  `vclocalname` varchar(64) NOT NULL,
  `vccommonname` varchar(64) NOT NULL,
  `vclocaldescription` varchar(1024) NOT NULL,
  `vccommondescription` varchar(1024) NOT NULL,
  `iweight` int(11) NOT NULL DEFAULT '0',
  `vctitle` varchar(255) DEFAULT '',
  `vcchattitle` varchar(255) DEFAULT '',
  `vclogo` varchar(255) DEFAULT '',
  `vchosturl` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_plugin` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `installed` tinyint(4) NOT NULL DEFAULT '0',
  `enabled` tinyint(4) NOT NULL DEFAULT '0',
  `initialized` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_requestbuffer` (
  `requestid` int(11) NOT NULL,
  `requestkey` char(32) NOT NULL,
  `request` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_requestcallback` (
  `callbackid` int(11) NOT NULL,
  `token` varchar(64) NOT NULL DEFAULT '',
  `func` varchar(64) NOT NULL,
  `arguments` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_revision` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_sitevisitor` (
  `visitorid` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `username` varchar(64) DEFAULT NULL,
  `firsttime` int(11) NOT NULL DEFAULT '0',
  `lasttime` int(11) NOT NULL DEFAULT '0',
  `entry` text NOT NULL,
  `details` text NOT NULL,
  `invitations` int(11) NOT NULL DEFAULT '0',
  `chats` int(11) NOT NULL DEFAULT '0',
  `threadid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_thread2` (
  `threadid` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `agentname` varchar(64) DEFAULT NULL,
  `agentid` int(11) NOT NULL DEFAULT '0',
  `dtmcreated` int(11) NOT NULL DEFAULT '0',
  `dtmchatstarted` int(11) NOT NULL DEFAULT '0',
  `dtmmodified` int(11) NOT NULL DEFAULT '0',
  `dtmclosed` int(11) NOT NULL DEFAULT '0',
  `lrevision` int(11) NOT NULL DEFAULT '0',
  `istate` int(11) NOT NULL DEFAULT '0',
  `invitationstate` int(11) NOT NULL DEFAULT '0',
  `ltoken` int(11) NOT NULL,
  `remote` varchar(255) DEFAULT NULL,
  `referer` text,
  `nextagent` int(11) NOT NULL DEFAULT '0',
  `locale` varchar(8) DEFAULT NULL,
  `lastpinguser` int(11) NOT NULL DEFAULT '0',
  `lastpingagent` int(11) NOT NULL DEFAULT '0',
  `usertyping` int(11) DEFAULT '0',
  `agenttyping` int(11) DEFAULT '0',
  `shownmessageid` int(11) NOT NULL DEFAULT '0',
  `useragent` varchar(255) DEFAULT NULL,
  `messagecount` varchar(16) DEFAULT NULL,
  `groupid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_threadstatistics` (
  `statid` int(11) NOT NULL,
  `date` int(11) NOT NULL DEFAULT '0',
  `threads` int(11) NOT NULL DEFAULT '0',
  `missedthreads` int(11) NOT NULL DEFAULT '0',
  `sentinvitations` int(11) NOT NULL DEFAULT '0',
  `acceptedinvitations` int(11) NOT NULL DEFAULT '0',
  `rejectedinvitations` int(11) NOT NULL DEFAULT '0',
  `ignoredinvitations` int(11) NOT NULL DEFAULT '0',
  `operatormessages` int(11) NOT NULL DEFAULT '0',
  `usermessages` int(11) NOT NULL DEFAULT '0',
  `averagewaitingtime` float(10,1) NOT NULL DEFAULT '0.0',
  `averagechattime` float(10,1) NOT NULL DEFAULT '0.0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_translation` (
  `translationid` int(11) NOT NULL,
  `locale` varchar(5) NOT NULL,
  `context` varchar(256) NOT NULL DEFAULT '',
  `source` text CHARACTER SET utf8 COLLATE utf8_bin,
  `translation` text,
  `hash` char(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_visitedpage` (
  `pageid` int(11) NOT NULL,
  `address` varchar(1024) DEFAULT NULL,
  `visittime` int(11) NOT NULL DEFAULT '0',
  `visitorid` int(11) DEFAULT NULL,
  `calculated` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `chat_visitedpagestatistics` (
  `pageid` int(11) NOT NULL,
  `date` int(11) NOT NULL DEFAULT '0',
  `address` varchar(1024) DEFAULT NULL,
  `visits` int(11) NOT NULL DEFAULT '0',
  `chats` int(11) NOT NULL DEFAULT '0',
  `sentinvitations` int(11) NOT NULL DEFAULT '0',
  `acceptedinvitations` int(11) NOT NULL DEFAULT '0',
  `rejectedinvitations` int(11) NOT NULL DEFAULT '0',
  `ignoredinvitations` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cheapnetcoupons` (
  `codice_cheapnet` varchar(100) NOT NULL,
  `tipo` varchar(5) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `usato` int(11) NOT NULL,
  `data_assegnazione` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `cms` (
  `id_cms` int(10) UNSIGNED NOT NULL,
  `id_cms_category` int(10) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cms_block` (
  `id_cms_block` int(10) UNSIGNED NOT NULL,
  `id_cms_category` int(10) UNSIGNED NOT NULL,
  `name` varchar(40) NOT NULL,
  `location` tinyint(1) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `display_store` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cms_block_lang` (
  `id_cms_block` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(40) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cms_block_page` (
  `id_cms_block_page` int(10) UNSIGNED NOT NULL,
  `id_cms_block` int(10) UNSIGNED NOT NULL,
  `id_cms` int(10) UNSIGNED NOT NULL,
  `is_category` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cms_category` (
  `id_cms_category` int(10) UNSIGNED NOT NULL,
  `id_parent` int(10) UNSIGNED NOT NULL,
  `level_depth` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `position` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cms_category_lang` (
  `id_cms_category` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text,
  `link_rewrite` varchar(128) NOT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cms_lang` (
  `id_cms` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `meta_title` varchar(128) NOT NULL,
  `meta_description` varchar(355) DEFAULT NULL,
  `meta_keywords` varchar(355) DEFAULT NULL,
  `content` longtext,
  `link_rewrite` varchar(128) NOT NULL,
  `seo_keyword` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `commissioni_accessori` (
  `id_group` varchar(200) NOT NULL,
  `data_pagamento` datetime NOT NULL,
  `tipo` varchar(120) NOT NULL,
  `totale_commissioni` decimal(10,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `commissioni_marketplace` (
  `id_order` varchar(110) NOT NULL,
  `id_cart` varchar(110) NOT NULL,
  `id_order_marketplace` varchar(100) NOT NULL,
  `seller_sku` varchar(100) NOT NULL,
  `id_product` int(10) NOT NULL,
  `amount` decimal(10,6) NOT NULL,
  `quantity` int(5) NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `approved` int(11) NOT NULL,
  `commissioni` decimal(10,2) NOT NULL,
  `commissioni_trasporti` decimal(10,2) NOT NULL,
  `commissioni_prime` decimal(10,2) NOT NULL,
  `tax` decimal(10,3) NOT NULL,
  `date_add` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `compare` (
  `id_compare` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `compare_product` (
  `id_compare` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `comuni` (
  `id` int(11) NOT NULL,
  `cod_regione` varchar(9) DEFAULT NULL,
  `cod_provincia` varchar(9) DEFAULT NULL,
  `cod_comune` varchar(9) DEFAULT NULL,
  `cod_istat` varchar(9) DEFAULT NULL,
  `comune` varchar(35) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `configuration` (
  `id_configuration` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` text,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `configuration_lang` (
  `id_configuration` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `value` text,
  `date_upd` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `connections` (
  `id_connections` int(10) UNSIGNED NOT NULL,
  `id_guest` int(10) UNSIGNED NOT NULL,
  `id_page` int(10) UNSIGNED NOT NULL,
  `ip_address` bigint(20) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `connections_cache` (
  `id_connections` int(10) UNSIGNED NOT NULL,
  `id_guest` int(10) UNSIGNED NOT NULL,
  `id_page` int(10) UNSIGNED NOT NULL,
  `ip_address` bigint(20) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `connections_page` (
  `id_connections` int(10) UNSIGNED NOT NULL,
  `id_page` int(10) UNSIGNED NOT NULL,
  `time_start` datetime NOT NULL,
  `time_end` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `connections_page_cache` (
  `id_connections` int(10) UNSIGNED NOT NULL,
  `id_page` int(10) UNSIGNED NOT NULL,
  `time_start` datetime NOT NULL,
  `time_end` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `connections_source` (
  `id_connections_source` int(10) UNSIGNED NOT NULL,
  `id_connections` int(10) UNSIGNED NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL,
  `request_uri` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `contact` (
  `id_contact` int(10) UNSIGNED NOT NULL,
  `email` varchar(128) NOT NULL,
  `customer_service` tinyint(1) NOT NULL DEFAULT '0',
  `position` tinyint(2) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `contact_lang` (
  `id_contact` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `contratto_assistenza` (
  `id_contratto` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `codice_spring` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `tipo` int(11) NOT NULL,
  `descrizione` text NOT NULL,
  `data_registrazione` datetime NOT NULL,
  `data_inizio` datetime NOT NULL,
  `data_fine` datetime NOT NULL,
  `competenza_dal` date NOT NULL,
  `competenza_al` date NOT NULL,
  `cadenza` varchar(50) NOT NULL,
  `indirizzo` varchar(400) NOT NULL,
  `rif_ordine` varchar(50) NOT NULL,
  `rif_fattura` varchar(100) NOT NULL,
  `rif_noleggio` varchar(100) NOT NULL,
  `cig` varchar(100) NOT NULL,
  `cup` varchar(100) NOT NULL,
  `univoco` varchar(100) NOT NULL,
  `prezzo` decimal(10,2) NOT NULL,
  `periodicita` int(11) NOT NULL,
  `pagamento` varchar(200) NOT NULL,
  `blocco_amministrativo` int(11) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `note` text NOT NULL,
  `pagato` int(11) NOT NULL,
  `fatturato` int(11) NOT NULL,
  `invio_contabilita` int(11) NOT NULL,
  `note_private` text NOT NULL,
  `storico` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `contratto_assistenza_prodotti` (
  `id_contratto` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `codice_prodotto` varchar(200) NOT NULL,
  `descrizione_prodotto` text NOT NULL,
  `quantita` int(11) NOT NULL,
  `prezzo` decimal(10,2) NOT NULL,
  `seriale` varchar(100) NOT NULL,
  `note` text NOT NULL,
  `indirizzo` text NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `country` (
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_zone` int(10) UNSIGNED NOT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `iso_code` varchar(3) NOT NULL,
  `call_prefix` int(10) NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `contains_states` tinyint(1) NOT NULL DEFAULT '0',
  `need_identification_number` tinyint(1) NOT NULL DEFAULT '0',
  `need_zip_code` tinyint(1) NOT NULL DEFAULT '1',
  `zip_code_format` varchar(12) NOT NULL DEFAULT '',
  `display_tax_label` tinyint(1) NOT NULL,
  `id_country_importerosc` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `country_lang` (
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `county` (
  `id_county` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `id_state` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `county_zip_code` (
  `id_county` int(11) NOT NULL,
  `from_zip_code` int(11) NOT NULL,
  `to_zip_code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `currency` (
  `id_currency` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL,
  `iso_code` varchar(3) NOT NULL DEFAULT '0',
  `iso_code_num` varchar(3) NOT NULL DEFAULT '0',
  `sign` varchar(8) NOT NULL,
  `blank` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `format` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `decimals` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `conversion_rate` decimal(13,6) NOT NULL,
  `last_conversion_rate` decimal(13,6) NOT NULL,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `id_currency_importerosc` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `customer` (
  `id_customer` int(10) UNSIGNED NOT NULL,
  `codice_spring` varchar(20) NOT NULL,
  `codice_esolver` int(11) NOT NULL,
  `id_gender` int(10) UNSIGNED NOT NULL,
  `id_default_group` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `is_company` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `pec` varchar(128) NOT NULL,
  `passwd` varchar(32) NOT NULL,
  `last_passwd_gen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `birthday` date DEFAULT NULL,
  `telefono_principale` varchar(100) NOT NULL,
  `cellulare_principale` varchar(100) NOT NULL,
  `referente` int(11) NOT NULL,
  `tax_code` varchar(50) NOT NULL,
  `vat_number` varchar(20) NOT NULL,
  `company` varchar(400) NOT NULL,
  `employees_number` varchar(20) NOT NULL,
  `tax_regime` int(11) NOT NULL,
  `newsletter` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ip_registration_newsletter` varchar(15) DEFAULT NULL,
  `newsletter_date_add` datetime DEFAULT NULL,
  `optin` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `order_notifications` int(11) NOT NULL,
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `note` text,
  `keyword` text NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `is_guest` tinyint(1) NOT NULL DEFAULT '0',
  `supplier` int(11) NOT NULL,
  `cartella_documenti` varchar(250) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `ex_rivenditore` int(11) NOT NULL,
  `risk` int(11) NOT NULL,
  `codice_univoco` varchar(150) NOT NULL,
  `ipa` varchar(50) NOT NULL,
  `settore` varchar(150) NOT NULL,
  `categoria` varchar(150) NOT NULL,
  `canale` varchar(150) NOT NULL,
  `ultimo_contatto` varchar(50) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `consenso_1` int(11) NOT NULL,
  `consenso_2` int(11) NOT NULL,
  `consenso_3` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `pw_gen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `customer_amministrazione` (
  `id_customer` int(11) NOT NULL,
  `tipo_soggetto` int(11) NOT NULL,
  `codice_fornitore` varchar(50) NOT NULL,
  `pagamento` varchar(150) NOT NULL,
  `pagamento_2` varchar(150) NOT NULL,
  `fido` varchar(150) NOT NULL,
  `assicurato` varchar(100) NOT NULL,
  `data_fido` date NOT NULL,
  `no_addebito_commissioni` int(11) NOT NULL,
  `zona` int(11) NOT NULL,
  `trasporto_a_mezzo` int(11) NOT NULL,
  `consegna` int(11) NOT NULL,
  `vettore` int(11) NOT NULL,
  `id_corriere` varchar(150) NOT NULL,
  `trasporto_gratis_da` varchar(150) NOT NULL,
  `fermo_deposito` varchar(150) NOT NULL,
  `trasporto_assicurato` int(11) NOT NULL,
  `note_consegna` varchar(150) NOT NULL,
  `iban` varchar(150) NOT NULL,
  `swift` varchar(150) NOT NULL,
  `paypal` varchar(150) NOT NULL,
  `iva_agevolata` varchar(150) NOT NULL,
  `esportatore_abituale` int(11) NOT NULL,
  `sconto_extra` varchar(150) NOT NULL,
  `per_ordini_da` varchar(150) NOT NULL,
  `fino_a` datetime NOT NULL,
  `data_rinnovo` varchar(40) NOT NULL,
  `data_revoca` varchar(40) NOT NULL,
  `motivo_revoca` text NOT NULL,
  `rebate` varchar(150) NOT NULL,
  `fatturato_annuo` varchar(150) NOT NULL,
  `blacklist` varchar(150) NOT NULL,
  `agente` varchar(150) NOT NULL,
  `tecnico` int(11) NOT NULL,
  `installatore` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `customer_group` (
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `customer_message` (
  `id_customer_message` int(10) UNSIGNED NOT NULL,
  `id_customer_thread` int(11) DEFAULT NULL,
  `id_employee` int(10) UNSIGNED DEFAULT NULL,
  `in_carico` int(11) NOT NULL,
  `email` text NOT NULL,
  `message` text NOT NULL,
  `note_private` text NOT NULL,
  `file_name` varchar(500) DEFAULT NULL,
  `ip_address` int(11) DEFAULT NULL,
  `user_agent` varchar(128) DEFAULT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `customer_note` (
  `id_note` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_employee` int(10) UNSIGNED NOT NULL,
  `note` text,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `customer_thread` (
  `id_customer_thread` int(11) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_contact` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED DEFAULT NULL,
  `id_employee` int(11) NOT NULL,
  `subject` varchar(300) NOT NULL,
  `priorita` varchar(20) NOT NULL,
  `id_order` int(10) UNSIGNED DEFAULT NULL,
  `id_product` int(10) UNSIGNED DEFAULT NULL,
  `status` enum('open','closed','pending1','pending2') NOT NULL DEFAULT 'open',
  `email` varchar(128) NOT NULL,
  `riferimento` varchar(50) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `token` varchar(12) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `note` text NOT NULL,
  `motivo_chiamata` text NOT NULL,
  `descrizione` varchar(200) NOT NULL,
  `guasto` varchar(200) NOT NULL,
  `causa_guasto` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `customization` (
  `id_customization` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) NOT NULL,
  `quantity` int(10) NOT NULL,
  `quantity_refunded` int(11) NOT NULL DEFAULT '0',
  `quantity_returned` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `customization_field` (
  `id_customization_field` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `type` tinyint(1) NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `customization_field_lang` (
  `id_customization_field` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `customized_data` (
  `id_customization` int(10) UNSIGNED NOT NULL,
  `type` tinyint(1) NOT NULL,
  `index` int(3) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `date_planner` (
  `id` int(11) NOT NULL,
  `tipo` varchar(200) NOT NULL,
  `data` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `date_range` (
  `id_date_range` int(10) UNSIGNED NOT NULL,
  `time_start` datetime NOT NULL,
  `time_end` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `dati_tecnici_prodotti` (
  `id_order_detail` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `riga` int(11) NOT NULL,
  `seriale` varchar(300) NOT NULL,
  `mac` varchar(300) NOT NULL,
  `imei` varchar(300) NOT NULL,
  `note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ddt` (
  `ddt` int(11) NOT NULL,
  `ddt_date` datetime NOT NULL,
  `id_order` int(11) NOT NULL,
  `products` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `delivery` (
  `id_delivery` int(10) UNSIGNED NOT NULL,
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_range_price` int(10) UNSIGNED DEFAULT NULL,
  `id_range_weight` int(10) UNSIGNED DEFAULT NULL,
  `id_zone` int(10) UNSIGNED NOT NULL,
  `price` decimal(20,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `discount` (
  `id_discount` int(10) UNSIGNED NOT NULL,
  `id_discount_type` int(10) UNSIGNED NOT NULL,
  `cat_or_prod` int(11) NOT NULL,
  `behavior_not_exhausted` tinyint(3) DEFAULT '1',
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_currency` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `value` decimal(17,2) NOT NULL DEFAULT '0.00',
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `quantity_per_user` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `cumulable` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `cumulable_reduction` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_from` datetime NOT NULL,
  `date_to` datetime NOT NULL,
  `minimal` decimal(17,2) DEFAULT NULL,
  `include_tax` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `cart_display` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `discount_category` (
  `id_category` int(11) UNSIGNED NOT NULL,
  `id_discount` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `discount_group` (
  `id_discount` int(11) NOT NULL,
  `id_group` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `discount_lang` (
  `id_discount` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `discount_manufacturer` (
  `id_discount` int(11) NOT NULL,
  `id_manufacturer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `discount_product` (
  `id_product` int(11) NOT NULL,
  `id_discount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `discount_type` (
  `id_discount_type` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `discount_type_lang` (
  `id_discount_type` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `editorial` (
  `id_editorial` int(10) UNSIGNED NOT NULL,
  `body_home_logo_link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `editorial_lang` (
  `id_editorial` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `body_title` varchar(255) NOT NULL,
  `body_subheading` varchar(255) NOT NULL,
  `body_paragraph` text NOT NULL,
  `body_logo_subheading` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `employee` (
  `id_employee` int(10) UNSIGNED NOT NULL,
  `id_profile` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastname` varchar(32) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `passwd` varchar(32) NOT NULL,
  `interno` varchar(5) NOT NULL,
  `last_passwd_gen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stats_date_from` date DEFAULT NULL,
  `stats_date_to` date DEFAULT NULL,
  `stats_date_from_compare` varchar(40) NOT NULL,
  `stats_date_to_compare` varchar(40) NOT NULL,
  `bo_color` varchar(32) DEFAULT NULL,
  `bo_theme` varchar(32) DEFAULT NULL,
  `bo_uimode` enum('hover','click') DEFAULT 'click',
  `bo_show_screencast` tinyint(1) NOT NULL DEFAULT '1',
  `logged` int(1) NOT NULL,
  `last_activity` datetime NOT NULL,
  `last_position` varchar(150) NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `eprice_customers` (
  `id_customer` int(11) NOT NULL,
  `id_eprice` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `eprice_orders` (
  `id_order` int(11) NOT NULL,
  `id_order_eprice` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `evento_yeastar_21102014` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `cognome` varchar(100) NOT NULL,
  `azienda` varchar(200) NOT NULL,
  `partita_iva` varchar(20) NOT NULL,
  `ruolo` varchar(100) NOT NULL,
  `settore` varchar(50) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `ex_rivenditori` (
  `id_customer` int(11) NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ezcloud` (
  `id_ezcloud` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `linee` int(11) NOT NULL,
  `interni` int(11) NOT NULL,
  `prezzo` decimal(10,6) NOT NULL,
  `active` int(11) NOT NULL,
  `avvio_mese` int(11) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ezcloud_valutazione` (
  `id_modulo` int(11) NOT NULL,
  `id_ezcloud` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `servizio` int(11) NOT NULL,
  `funzionalita` int(11) NOT NULL,
  `supporto` int(11) NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ezdial_licenze` (
  `id_licenza` int(11) NOT NULL,
  `licenza` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ezdial_mac_address` (
  `id_mac_address` int(11) NOT NULL,
  `mac_address` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ezdial_mac_licenze` (
  `id_mac_address` int(11) NOT NULL,
  `id_licenza` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `fattura` (
  `id_riga` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `codice_cliente` varchar(100) NOT NULL,
  `id_fattura` varchar(100) NOT NULL,
  `id_storico` varchar(100) NOT NULL,
  `tipo` varchar(25) NOT NULL,
  `data_fattura` datetime NOT NULL,
  `cod_articolo` varchar(200) NOT NULL,
  `desc_articolo` varchar(200) NOT NULL,
  `qt_ord` int(11) NOT NULL,
  `qt_sped` int(11) NOT NULL,
  `prezzo` decimal(13,6) NOT NULL,
  `importo_netto` decimal(13,6) NOT NULL,
  `sconto1` decimal(13,6) NOT NULL,
  `sconto2` decimal(13,6) NOT NULL,
  `sconto3` decimal(13,6) NOT NULL,
  `totale_fattura` decimal(15,6) NOT NULL,
  `rif_ordine` varchar(200) NOT NULL,
  `rif_vs_ordine` varchar(100) NOT NULL,
  `col_note` varchar(200) NOT NULL,
  `tipo_fattura` varchar(100) NOT NULL,
  `causale_trasporto` varchar(100) NOT NULL,
  `desc_porto` varchar(100) NOT NULL,
  `desc_aspetto_beni` varchar(150) NOT NULL,
  `trasp_mezzo` varchar(100) NOT NULL,
  `codice_vettore` varchar(50) NOT NULL,
  `dati_vettore` varchar(400) NOT NULL,
  `num_colli` int(11) NOT NULL,
  `peso` decimal(13,6) NOT NULL,
  `iva` decimal(15,6) NOT NULL,
  `imponibile` decimal(15,6) NOT NULL,
  `desc_fattura` varchar(200) NOT NULL,
  `dati_fatturazione` text NOT NULL,
  `dati_spedizione` text NOT NULL,
  `banca_appoggio` text NOT NULL,
  `iva_percent` decimal(13,6) NOT NULL,
  `num_riga` int(11) NOT NULL,
  `pagamento` varchar(150) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_manufacturer` int(11) NOT NULL,
  `data_odv` date NOT NULL,
  `ddt` int(11) NOT NULL,
  `data_ddt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `fatture_acquisto` (
  `id_riga` int(11) NOT NULL,
  `gruppo_documento` varchar(5) NOT NULL,
  `classe_documento` int(11) NOT NULL,
  `data_registrazione` date NOT NULL,
  `numero_registrazione` int(11) NOT NULL,
  `codice_cliente` int(11) NOT NULL,
  `totale_documento` decimal(12,6) NOT NULL,
  `totale_iva` decimal(12,6) NOT NULL,
  `descrizione` varchar(50) NOT NULL,
  `numero_riga` decimal(12,6) NOT NULL,
  `codice_articolo` varchar(100) NOT NULL,
  `descrizione_articolo` varchar(100) NOT NULL,
  `quantita` decimal(12,6) NOT NULL,
  `prezzo` decimal(12,6) NOT NULL,
  `importo` decimal(12,6) NOT NULL,
  `sconto_1` decimal(12,6) NOT NULL,
  `sconto_2` decimal(12,6) NOT NULL,
  `sconto_3` decimal(12,6) NOT NULL,
  `totale` decimal(12,6) NOT NULL,
  `imponibile` decimal(12,6) NOT NULL,
  `codice_tipo_fattura` int(11) NOT NULL,
  `codice_pagamento` varchar(10) NOT NULL,
  `causale_trasporto` varchar(10) NOT NULL,
  `porto` varchar(10) NOT NULL,
  `aspetto_beni` varchar(10) NOT NULL,
  `trasporto_a_mezzo` varchar(10) NOT NULL,
  `appoggio_bancari` varchar(10) NOT NULL,
  `codice_spedizione` int(11) NOT NULL,
  `tipo_spedizione` int(11) NOT NULL,
  `tipo_anagrafica` int(11) NOT NULL,
  `codice_anagrafica` int(11) NOT NULL,
  `totale_colli` int(11) NOT NULL,
  `peso_netto` decimal(12,6) NOT NULL,
  `peso_lordo` decimal(12,6) NOT NULL,
  `iva` decimal(12,6) NOT NULL,
  `valuta_totale` decimal(12,6) NOT NULL,
  `valuta_imponibile` decimal(12,6) NOT NULL,
  `valuta_iva` decimal(12,6) NOT NULL,
  `partita_iva` varchar(20) NOT NULL,
  `codice_fiscale` varchar(20) NOT NULL,
  `cognome` varchar(100) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `codice_iva` int(11) NOT NULL,
  `aliquota` decimal(12,6) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_manufacturer` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `tipo_documento` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `feature` (
  `id_feature` int(10) UNSIGNED NOT NULL,
  `position` int(10) DEFAULT NULL,
  `id_group` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `feature_group` (
  `id_group` int(11) NOT NULL,
  `position` int(11) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=5461 DEFAULT CHARSET=utf8;

CREATE TABLE `feature_group_lang` (
  `id_group` int(11) DEFAULT NULL,
  `id_lang` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=2730 DEFAULT CHARSET=utf8;

CREATE TABLE `feature_lang` (
  `id_feature` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `feature_product` (
  `id_feature` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_feature_value` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `feature_value` (
  `id_feature_value` int(10) UNSIGNED NOT NULL,
  `id_feature` int(10) UNSIGNED NOT NULL,
  `custom` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `feature_value_lang` (
  `id_feature_value` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ff` (
  `nm` int(11) NOT NULL,
  `an` int(11) NOT NULL,
  `rd` int(11) NOT NULL,
  `tp` varchar(2) NOT NULL,
  `dt` datetime NOT NULL,
  `ct` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `fixed_ck` (
  `id_ck` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `forecast` (
  `id_product` int(11) NOT NULL,
  `periodo` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `form_prevendita_message` (
  `id_message` int(11) NOT NULL,
  `id_thread` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `in_carico` int(11) NOT NULL,
  `email` text NOT NULL,
  `file_name` varchar(300) NOT NULL,
  `message` text NOT NULL,
  `note_private` text NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `form_prevendita_thread` (
  `id_thread` int(11) NOT NULL,
  `tipo_richiesta` varchar(200) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `subject` varchar(300) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `tax_code` varchar(300) NOT NULL,
  `vat_number` varchar(300) NOT NULL,
  `firstname` varchar(300) NOT NULL,
  `lastname` varchar(300) NOT NULL,
  `company` varchar(300) NOT NULL,
  `address1` varchar(300) NOT NULL,
  `postcode` varchar(300) NOT NULL,
  `state` varchar(300) NOT NULL,
  `country` varchar(300) NOT NULL,
  `phone` varchar(300) NOT NULL,
  `email` varchar(300) NOT NULL,
  `riferimento` varchar(50) NOT NULL,
  `token` varchar(300) NOT NULL,
  `status` varchar(300) NOT NULL,
  `category` varchar(300) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `note` text NOT NULL,
  `source` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `gestione_link` (
  `id_regola` int(11) NOT NULL,
  `keyword` varchar(500) NOT NULL,
  `url` varchar(500) NOT NULL,
  `active` int(11) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `gestpay_currencies_map` (
  `id_prestashop` int(10) UNSIGNED NOT NULL,
  `id_gestpay` int(10) UNSIGNED NOT NULL,
  `currency_name` varchar(10) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `gestpay_languages_map` (
  `code_prestashop` char(2) NOT NULL,
  `id_gestpay` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `group` (
  `id_group` int(10) UNSIGNED NOT NULL,
  `reduction` decimal(17,2) NOT NULL DEFAULT '0.00',
  `price_display_method` tinyint(4) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `id_group_importerosc` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `group_lang` (
  `id_group` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `group_reduction` (
  `id_group_reduction` mediumint(8) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  `id_category` int(10) UNSIGNED NOT NULL,
  `reduction` decimal(4,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `guest` (
  `id_guest` int(10) UNSIGNED NOT NULL,
  `id_operating_system` int(10) UNSIGNED DEFAULT NULL,
  `id_web_browser` int(10) UNSIGNED DEFAULT NULL,
  `id_customer` int(10) UNSIGNED DEFAULT NULL,
  `javascript` tinyint(1) DEFAULT '0',
  `screen_resolution_x` smallint(5) UNSIGNED DEFAULT NULL,
  `screen_resolution_y` smallint(5) UNSIGNED DEFAULT NULL,
  `screen_color` tinyint(3) UNSIGNED DEFAULT NULL,
  `sun_java` tinyint(1) DEFAULT NULL,
  `adobe_flash` tinyint(1) DEFAULT NULL,
  `adobe_director` tinyint(1) DEFAULT NULL,
  `apple_quicktime` tinyint(1) DEFAULT NULL,
  `real_player` tinyint(1) DEFAULT NULL,
  `windows_media` tinyint(1) DEFAULT NULL,
  `accept_language` varchar(8) DEFAULT NULL,
  `agent` varchar(400) NOT NULL,
  `agent_check` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `help_access` (
  `id_help_access` int(11) NOT NULL,
  `label` varchar(45) NOT NULL,
  `version` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `hook` (
  `id_hook` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text,
  `position` tinyint(1) NOT NULL DEFAULT '1',
  `live_edit` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `hook_module` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_hook` int(10) UNSIGNED NOT NULL,
  `position` tinyint(2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `hook_module_exceptions` (
  `id_hook_module_exceptions` int(10) UNSIGNED NOT NULL,
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_hook` int(10) UNSIGNED NOT NULL,
  `file_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `image` (
  `id_image` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `position` smallint(2) UNSIGNED NOT NULL DEFAULT '0',
  `cover` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `image_lang` (
  `id_image` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `legend` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `image_type` (
  `id_image_type` int(10) UNSIGNED NOT NULL,
  `name` varchar(16) NOT NULL,
  `width` int(10) UNSIGNED NOT NULL,
  `height` int(10) UNSIGNED NOT NULL,
  `products` tinyint(1) NOT NULL DEFAULT '1',
  `categories` tinyint(1) NOT NULL DEFAULT '1',
  `manufacturers` tinyint(1) NOT NULL DEFAULT '1',
  `suppliers` tinyint(1) NOT NULL DEFAULT '1',
  `scenes` tinyint(1) NOT NULL DEFAULT '1',
  `stores` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `immagini_provvisoria` (
  `idimage` int(11) NOT NULL,
  `idprod` int(11) NOT NULL,
  `cover` int(11) NOT NULL,
  `nomeprod` varchar(300) NOT NULL,
  `codice` varchar(300) NOT NULL,
  `dimensioniOrig` varchar(300) NOT NULL,
  `pesoOrig` decimal(10,2) NOT NULL,
  `pesoTrec` decimal(10,2) NOT NULL,
  `pesoSeic` decimal(10,2) NOT NULL,
  `pesoList` decimal(10,2) NOT NULL,
  `pesoPic` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `import_match` (
  `id_import_match` int(10) NOT NULL,
  `name` varchar(32) NOT NULL,
  `match` text NOT NULL,
  `skip` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `lang` (
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL,
  `active` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `iso_code` char(2) NOT NULL,
  `language_code` char(5) NOT NULL,
  `date_format_lite` char(32) NOT NULL DEFAULT 'Y-m-d',
  `date_format_full` char(32) NOT NULL DEFAULT 'Y-m-d H:i:s',
  `is_rtl` tinyint(1) NOT NULL DEFAULT '0',
  `id_lang_importerosc` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `layered_category` (
  `id_layered_category` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL,
  `id_category` int(10) UNSIGNED NOT NULL,
  `id_value` int(10) UNSIGNED DEFAULT '0',
  `type` enum('category','id_feature','id_attribute_group','quantity','condition','manufacturer','weight','price') NOT NULL,
  `position` int(10) UNSIGNED NOT NULL,
  `filter_type` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `filter_show_limit` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `layered_filter` (
  `id_layered_filter` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `filters` text,
  `n_categories` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `layered_filter_shop` (
  `id_layered_filter` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `layered_friendly_url` (
  `id_layered_friendly_url` int(11) NOT NULL,
  `url_key` varchar(32) NOT NULL,
  `data` varchar(200) NOT NULL,
  `id_lang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `layered_indexable_attribute_group` (
  `id_attribute_group` int(11) NOT NULL,
  `indexable` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `layered_indexable_attribute_group_lang_value` (
  `id_attribute_group` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(20) DEFAULT NULL,
  `meta_title` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `layered_indexable_attribute_lang_value` (
  `id_attribute` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(20) DEFAULT NULL,
  `meta_title` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `layered_indexable_feature` (
  `id_feature` int(11) NOT NULL,
  `indexable` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `layered_indexable_feature_lang_value` (
  `id_feature` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(20) NOT NULL,
  `meta_title` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `layered_indexable_feature_value_lang_value` (
  `id_feature_value` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(20) DEFAULT NULL,
  `meta_title` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `layered_price_index` (
  `id_product` int(11) NOT NULL,
  `id_currency` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  `price_min` int(11) NOT NULL,
  `price_max` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `layered_product_attribute` (
  `id_attribute` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_attribute_group` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `location_coords` (
  `id_location_coords` int(10) UNSIGNED NOT NULL,
  `x` int(4) NOT NULL,
  `y` int(4) NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `log` (
  `id_log` int(10) UNSIGNED NOT NULL,
  `severity` tinyint(1) NOT NULL,
  `error_code` int(11) DEFAULT NULL,
  `message` text NOT NULL,
  `object_type` varchar(32) DEFAULT NULL,
  `object_id` int(10) UNSIGNED DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `loyalty` (
  `id_loyalty` int(10) UNSIGNED NOT NULL,
  `id_loyalty_state` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED DEFAULT NULL,
  `id_discount` int(10) UNSIGNED DEFAULT NULL,
  `points` int(11) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `loyalty_history` (
  `id_loyalty_history` int(10) UNSIGNED NOT NULL,
  `id_loyalty` int(10) UNSIGNED DEFAULT NULL,
  `id_loyalty_state` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `points` int(11) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `loyalty_state` (
  `id_loyalty_state` int(10) UNSIGNED NOT NULL,
  `id_order_state` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `loyalty_state_lang` (
  `id_loyalty_state` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `mailalert_customer_oos` (
  `id_customer` int(10) UNSIGNED NOT NULL,
  `customer_email` varchar(128) NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `mailing_history` (
  `id_campaign` int(5) NOT NULL,
  `subject` varchar(255) NOT NULL DEFAULT '',
  `date` varchar(10) NOT NULL,
  `time` varchar(8) NOT NULL,
  `num_sent` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `mailing_import` (
  `ID` int(5) NOT NULL,
  `email` varchar(128) NOT NULL DEFAULT '',
  `lastname` varchar(32) DEFAULT '',
  `firstname` varchar(32) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `mailing_sent` (
  `id_campaign` varchar(5) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `date` varchar(20) NOT NULL,
  `dateReceived` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `mailing_track` (
  `ID` int(5) NOT NULL,
  `ipAddress` varchar(12) NOT NULL DEFAULT '',
  `id_campaign` varchar(5) NOT NULL DEFAULT '0',
  `subject` varchar(255) NOT NULL DEFAULT '0',
  `postDate` varchar(10) NOT NULL DEFAULT '',
  `postTime` varchar(8) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `manufacturer` (
  `id_manufacturer` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `other_suppliers` varchar(300) NOT NULL,
  `margin_min_cli` decimal(10,2) NOT NULL,
  `margin_min_riv` decimal(10,2) NOT NULL,
  `margin_login_for_offer` decimal(10,2) NOT NULL,
  `margin_min_mepa` decimal(10,2) NOT NULL,
  `amazon_it` decimal(10,2) NOT NULL,
  `amazon_fr` decimal(10,2) NOT NULL,
  `amazon_uk` decimal(10,2) NOT NULL,
  `amazon_es` decimal(10,2) NOT NULL,
  `amazon_de` decimal(10,2) NOT NULL,
  `amazon_nl` decimal(10,2) NOT NULL,
  `abilita_banda` int(11) NOT NULL,
  `website` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `manufacturer_lang` (
  `id_manufacturer` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `description` text,
  `short_description` varchar(254) DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `seo_keyword` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `memcached_servers` (
  `id_memcached_server` int(11) UNSIGNED NOT NULL,
  `ip` varchar(254) NOT NULL,
  `port` int(11) UNSIGNED NOT NULL,
  `weight` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `message` (
  `id_message` int(10) UNSIGNED NOT NULL,
  `id_cart` int(10) UNSIGNED DEFAULT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_employee` int(10) UNSIGNED DEFAULT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `message` text NOT NULL,
  `private` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `message_readed` (
  `id_message` int(10) UNSIGNED NOT NULL,
  `id_employee` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `meta` (
  `id_meta` int(10) UNSIGNED NOT NULL,
  `page` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `meta_lang` (
  `id_meta` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `title` varchar(128) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `url_rewrite` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `module` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `module_country` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `module_currency` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_currency` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `module_group` (
  `id_module` int(10) UNSIGNED NOT NULL,
  `id_group` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `movimenti_speciali` (
  `id_movement` int(11) NOT NULL,
  `tipo` varchar(2) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `prima` int(11) NOT NULL,
  `dopo` int(11) NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `newsletter` (
  `id` int(6) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_company` int(11) NOT NULL,
  `firstname` varchar(120) NOT NULL,
  `lastname` varchar(120) NOT NULL,
  `company` varchar(120) NOT NULL,
  `phone` varchar(120) NOT NULL,
  `newsletter_date_add` datetime DEFAULT NULL,
  `ip_registration_newsletter` varchar(15) NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL,
  `consenso_1` int(11) NOT NULL,
  `consenso_2` int(11) NOT NULL,
  `consenso_3` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `noleggio` (
  `id_noleggio` int(11) NOT NULL,
  `id_cart` int(11) NOT NULL,
  `mesi` int(11) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `spese_contratto` decimal(10,2) NOT NULL,
  `parametri` decimal(10,3) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `note_attivita` (
  `id_note` int(10) UNSIGNED NOT NULL,
  `id_attivita` int(10) UNSIGNED NOT NULL,
  `tipo_attivita` varchar(10) DEFAULT NULL,
  `id_employee` int(10) UNSIGNED NOT NULL,
  `note` text,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `note_di_credito` (
  `id_riga` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `codice_cliente` varchar(100) NOT NULL,
  `id_fattura` varchar(100) NOT NULL,
  `id_storico` varchar(100) NOT NULL,
  `tipo` varchar(25) NOT NULL,
  `data_fattura` datetime NOT NULL,
  `cod_articolo` varchar(200) NOT NULL,
  `desc_articolo` varchar(200) NOT NULL,
  `qt_ord` int(11) NOT NULL,
  `qt_sped` int(11) NOT NULL,
  `prezzo` decimal(13,6) NOT NULL,
  `importo_netto` decimal(13,6) NOT NULL,
  `sconto1` decimal(13,6) NOT NULL,
  `sconto2` decimal(13,6) NOT NULL,
  `sconto3` decimal(13,6) NOT NULL,
  `totale_fattura` decimal(15,6) NOT NULL,
  `rif_ordine` varchar(200) NOT NULL,
  `rif_vs_ordine` varchar(100) NOT NULL,
  `col_note` varchar(200) NOT NULL,
  `tipo_fattura` varchar(100) NOT NULL,
  `causale_trasporto` varchar(100) NOT NULL,
  `desc_porto` varchar(100) NOT NULL,
  `desc_aspetto_beni` varchar(150) NOT NULL,
  `trasp_mezzo` varchar(100) NOT NULL,
  `codice_vettore` varchar(50) NOT NULL,
  `dati_vettore` varchar(400) NOT NULL,
  `num_colli` int(11) NOT NULL,
  `peso` decimal(13,6) NOT NULL,
  `iva` decimal(15,6) NOT NULL,
  `imponibile` decimal(15,6) NOT NULL,
  `desc_fattura` varchar(200) NOT NULL,
  `dati_fatturazione` text NOT NULL,
  `dati_spedizione` text NOT NULL,
  `banca_appoggio` text NOT NULL,
  `iva_percent` decimal(13,6) NOT NULL,
  `num_riga` int(11) NOT NULL,
  `pagamento` varchar(150) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_manufacturer` int(11) NOT NULL,
  `data_odv` date NOT NULL,
  `ddt` int(11) NOT NULL,
  `data_ddt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `operating_system` (
  `id_operating_system` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `orders` (
  `id_order` int(10) UNSIGNED NOT NULL,
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_cart` int(10) UNSIGNED NOT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `id_address_delivery` int(10) UNSIGNED NOT NULL,
  `id_address_invoice` int(10) UNSIGNED NOT NULL,
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `payment` varchar(255) NOT NULL,
  `conversion_rate` decimal(13,6) NOT NULL DEFAULT '1.000000',
  `module` varchar(255) DEFAULT NULL,
  `recyclable` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `gift` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `gift_message` text,
  `shipping_number` varchar(32) DEFAULT NULL,
  `total_discounts` decimal(17,2) NOT NULL DEFAULT '0.00',
  `total_paid` decimal(17,2) NOT NULL DEFAULT '0.00',
  `total_paid_real` decimal(17,2) NOT NULL DEFAULT '0.00',
  `total_products` decimal(17,2) NOT NULL DEFAULT '0.00',
  `total_products_wt` decimal(17,2) NOT NULL DEFAULT '0.00',
  `total_shipping` decimal(17,2) NOT NULL DEFAULT '0.00',
  `carrier_tax_rate` decimal(10,3) NOT NULL DEFAULT '0.000',
  `acconto` decimal(17,2) NOT NULL,
  `tax_regime` int(11) NOT NULL,
  `total_wrapping` decimal(17,2) NOT NULL DEFAULT '0.00',
  `total_commissions` decimal(10,2) NOT NULL,
  `invoice_number` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `delivery_number` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `invoice_date` datetime NOT NULL,
  `delivery_date` datetime NOT NULL,
  `valid` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `csv` int(11) NOT NULL,
  `analytics` int(11) NOT NULL,
  `ddt` int(11) NOT NULL,
  `spring` int(11) NOT NULL,
  `check_pi` int(11) NOT NULL,
  `check_esolver` int(11) NOT NULL,
  `pag_amz_ric` int(11) NOT NULL,
  `verificato` int(11) NOT NULL,
  `verificato_da` varchar(2) NOT NULL,
  `rif_ordine` varchar(70) NOT NULL,
  `cig` varchar(40) NOT NULL,
  `cup` varchar(40) NOT NULL,
  `ipa` varchar(40) NOT NULL,
  `data_ordine_mepa` date NOT NULL,
  `no_feedback` int(11) NOT NULL,
  `no_feedback_cause` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `orders_controllo_differiti` (
  `id_order` int(11) NOT NULL,
  `pagato` int(11) NOT NULL,
  `insoluto` int(11) NOT NULL,
  `pagamento_in_corso` int(11) NOT NULL,
  `altro` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `order_detail` (
  `id_order_detail` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `product_attribute_id` int(10) UNSIGNED DEFAULT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_quantity_in_stock` int(10) NOT NULL DEFAULT '0',
  `product_quantity_refunded` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_quantity_return` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_quantity_reinjected` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `wholesale_price` decimal(10,2) NOT NULL,
  `no_acq` int(11) NOT NULL,
  `reduction_percent` decimal(10,2) NOT NULL DEFAULT '0.00',
  `reduction_amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `group_reduction` decimal(10,2) NOT NULL DEFAULT '0.00',
  `product_quantity_discount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `product_ean13` varchar(13) DEFAULT NULL,
  `product_upc` varchar(12) DEFAULT NULL,
  `product_reference` varchar(32) DEFAULT NULL,
  `product_supplier_reference` varchar(32) DEFAULT NULL,
  `product_weight` float NOT NULL,
  `tax_name` varchar(40) NOT NULL,
  `tax_rate` decimal(10,3) NOT NULL DEFAULT '0.000',
  `ecotax` decimal(21,6) NOT NULL DEFAULT '0.000000',
  `ecotax_tax_rate` decimal(5,3) NOT NULL DEFAULT '0.000',
  `discount_quantity_applied` tinyint(1) NOT NULL DEFAULT '0',
  `download_hash` varchar(255) DEFAULT NULL,
  `download_nb` int(10) UNSIGNED DEFAULT '0',
  `download_deadline` datetime DEFAULT '0000-00-00 00:00:00',
  `cons` varchar(50) NOT NULL,
  `ddt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `order_discount` (
  `id_order_discount` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `id_discount` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL,
  `value` decimal(17,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `order_history` (
  `id_order_history` int(10) UNSIGNED NOT NULL,
  `id_employee` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `id_order_state` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `order_message` (
  `id_order_message` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `order_message_lang` (
  `id_order_message` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `order_processati` (
  `id_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `order_return` (
  `id_order_return` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `state` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `question` text NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `order_return_detail` (
  `id_order_return` int(10) UNSIGNED NOT NULL,
  `id_order_detail` int(10) UNSIGNED NOT NULL,
  `id_customization` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `product_quantity` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `order_return_state` (
  `id_order_return_state` int(10) UNSIGNED NOT NULL,
  `color` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `order_return_state_lang` (
  `id_order_return_state` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `order_slip` (
  `id_order_slip` int(10) UNSIGNED NOT NULL,
  `conversion_rate` decimal(13,6) NOT NULL DEFAULT '1.000000',
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `shipping_cost` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `order_slip_detail` (
  `id_order_slip` int(10) UNSIGNED NOT NULL,
  `id_order_detail` int(10) UNSIGNED NOT NULL,
  `product_quantity` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `order_state` (
  `id_order_state` int(10) UNSIGNED NOT NULL,
  `invoice` tinyint(1) UNSIGNED DEFAULT '0',
  `send_email` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `color` varchar(32) DEFAULT NULL,
  `unremovable` tinyint(1) UNSIGNED NOT NULL,
  `hidden` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `logable` tinyint(1) NOT NULL DEFAULT '0',
  `delivery` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `order_state_lang` (
  `id_order_state` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `template` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `order_tax` (
  `id_order` int(11) NOT NULL,
  `tax_name` varchar(40) NOT NULL,
  `tax_rate` decimal(6,3) NOT NULL,
  `amount` decimal(20,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ordini_acquisto` (
  `id_riga` int(11) NOT NULL,
  `gruppo_documento` varchar(5) NOT NULL,
  `classe_documento` int(11) NOT NULL,
  `data_registrazione` datetime NOT NULL,
  `numero_registrazione` int(11) NOT NULL,
  `codice_cliente` int(11) NOT NULL,
  `totale_documento` decimal(12,6) NOT NULL,
  `totale_iva` decimal(12,6) NOT NULL,
  `descrizione` varchar(50) NOT NULL,
  `numero_riga` decimal(12,6) NOT NULL,
  `codice_articolo` varchar(100) NOT NULL,
  `descrizione_articolo` varchar(100) NOT NULL,
  `quantita` decimal(12,6) NOT NULL,
  `prezzo` decimal(12,6) NOT NULL,
  `importo` decimal(12,6) NOT NULL,
  `sconto_1` decimal(12,6) NOT NULL,
  `sconto_2` decimal(12,6) NOT NULL,
  `sconto_3` decimal(12,6) NOT NULL,
  `totale` decimal(12,6) NOT NULL,
  `imponibile` decimal(12,6) NOT NULL,
  `codice_tipo_fattura` int(11) NOT NULL,
  `codice_pagamento` varchar(10) NOT NULL,
  `causale_trasporto` varchar(10) NOT NULL,
  `porto` varchar(10) NOT NULL,
  `aspetto_beni` varchar(10) NOT NULL,
  `trasporto_a_mezzo` varchar(10) NOT NULL,
  `appoggio_bancari` varchar(10) NOT NULL,
  `codice_spedizione` int(11) NOT NULL,
  `tipo_spedizione` int(11) NOT NULL,
  `tipo_anagrafica` int(11) NOT NULL,
  `codice_anagrafica` int(11) NOT NULL,
  `totale_colli` int(11) NOT NULL,
  `peso_netto` decimal(12,6) NOT NULL,
  `peso_lordo` decimal(12,6) NOT NULL,
  `iva` decimal(12,6) NOT NULL,
  `valuta_totale` decimal(12,6) NOT NULL,
  `valuta_imponibile` decimal(12,6) NOT NULL,
  `valuta_iva` decimal(12,6) NOT NULL,
  `partita_iva` varchar(20) NOT NULL,
  `codice_fiscale` varchar(20) NOT NULL,
  `cognome` varchar(100) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `codice_iva` int(11) NOT NULL,
  `aliquota` decimal(12,6) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_manufacturer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `ore` (
  `id_employee` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `valori_ore` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `page` (
  `id_page` int(10) UNSIGNED NOT NULL,
  `id_page_type` int(10) UNSIGNED NOT NULL,
  `id_object` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `pagenotfound` (
  `id_pagenotfound` int(10) UNSIGNED NOT NULL,
  `request_uri` varchar(256) NOT NULL,
  `http_referer` varchar(256) NOT NULL,
  `ip_address` varchar(200) NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `page_type` (
  `id_page_type` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `page_viewed` (
  `id_page` int(10) UNSIGNED NOT NULL,
  `id_date_range` int(10) UNSIGNED NOT NULL,
  `counter` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `payment_cc` (
  `id_payment_cc` int(11) NOT NULL,
  `id_order` int(10) UNSIGNED DEFAULT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `transaction_id` varchar(254) DEFAULT NULL,
  `card_number` varchar(254) DEFAULT NULL,
  `card_brand` varchar(254) DEFAULT NULL,
  `card_expiration` char(7) DEFAULT NULL,
  `card_holder` varchar(254) DEFAULT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `paypal_order` (
  `id_order` int(10) UNSIGNED NOT NULL,
  `id_transaction` varchar(255) NOT NULL,
  `payment_method` int(10) UNSIGNED NOT NULL,
  `payment_status` varchar(255) NOT NULL,
  `capture` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `persone` (
  `id_persona` int(11) NOT NULL,
  `firstname` varchar(300) NOT NULL,
  `lastname` varchar(300) NOT NULL,
  `role` varchar(400) NOT NULL,
  `email` varchar(400) NOT NULL,
  `email_aziendale` varchar(400) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `phone_2` varchar(100) NOT NULL,
  `phone_mobile` varchar(100) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `tax_code` varchar(50) NOT NULL,
  `vat_number` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `precompilato` (
  `id_precompilato` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `oggetto` varchar(200) NOT NULL,
  `testo` text NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_supplier` int(10) UNSIGNED DEFAULT NULL,
  `id_manufacturer` int(10) UNSIGNED DEFAULT NULL,
  `id_tax_rules_group` int(10) UNSIGNED NOT NULL,
  `id_category_default` int(10) UNSIGNED DEFAULT NULL,
  `id_category_gst` int(11) NOT NULL,
  `id_color_default` int(10) UNSIGNED DEFAULT NULL,
  `on_sale` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `online_only` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ean13` varchar(13) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `cod_fornitore_1` varchar(150) NOT NULL,
  `cod_fornitore_2` varchar(150) NOT NULL,
  `asin` varchar(200) NOT NULL,
  `sku_amazon` varchar(50) NOT NULL,
  `fnsku` varchar(50) NOT NULL,
  `ecotax` decimal(17,2) NOT NULL DEFAULT '0.00',
  `dispo_type` int(11) NOT NULL,
  `quantity` int(10) NOT NULL DEFAULT '0',
  `minimal_quantity` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `price` decimal(20,2) NOT NULL DEFAULT '0.00',
  `listino` decimal(20,2) NOT NULL,
  `wholesale_price` decimal(20,2) NOT NULL DEFAULT '0.00',
  `unity` varchar(255) DEFAULT NULL,
  `unit_price_ratio` decimal(20,2) NOT NULL DEFAULT '0.00',
  `additional_shipping_cost` decimal(20,2) NOT NULL DEFAULT '0.00',
  `margin_min_cli` decimal(10,2) NOT NULL,
  `margin_min_riv` decimal(10,2) NOT NULL,
  `margin_login_for_offer` decimal(10,2) NOT NULL,
  `map` decimal(10,2) NOT NULL,
  `reference` varchar(36) DEFAULT NULL,
  `supplier_reference` varchar(32) DEFAULT NULL,
  `location` varchar(64) DEFAULT NULL,
  `width` float NOT NULL DEFAULT '0',
  `height` float NOT NULL DEFAULT '0',
  `depth` float NOT NULL DEFAULT '0',
  `weight` float NOT NULL DEFAULT '0',
  `out_of_stock` int(10) UNSIGNED NOT NULL DEFAULT '2',
  `quantity_discount` tinyint(1) DEFAULT '0',
  `customizable` tinyint(2) NOT NULL DEFAULT '0',
  `uploadable_files` tinyint(4) NOT NULL DEFAULT '0',
  `text_fields` tinyint(4) NOT NULL DEFAULT '0',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `available_for_order` tinyint(1) NOT NULL DEFAULT '1',
  `condition` enum('new','used','refurbished') NOT NULL DEFAULT 'new',
  `show_price` tinyint(1) NOT NULL DEFAULT '1',
  `indexed` tinyint(1) NOT NULL DEFAULT '0',
  `cache_is_pack` tinyint(1) NOT NULL DEFAULT '0',
  `cache_has_attachments` tinyint(1) NOT NULL DEFAULT '0',
  `cache_default_attribute` int(10) UNSIGNED DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `other_suppliers` varchar(300) NOT NULL,
  `comparaprezzi_check` int(11) NOT NULL,
  `scontolistinovendita` decimal(20,2) NOT NULL,
  `sconto_acquisto_1` decimal(20,2) NOT NULL,
  `sconto_acquisto_2` decimal(20,2) NOT NULL,
  `sconto_acquisto_3` decimal(20,2) NOT NULL,
  `serial` varchar(50) NOT NULL,
  `scorta_minima` int(11) NOT NULL,
  `scorta_minima_amazon` int(11) NOT NULL,
  `no_amazon` varchar(150) NOT NULL,
  `stock_quantity` int(11) NOT NULL,
  `arrivo_quantity` int(11) NOT NULL,
  `supplier_quantity` int(11) NOT NULL,
  `esprinet_quantity` int(11) NOT NULL,
  `attiva_quantity` int(11) NOT NULL,
  `itancia_quantity` int(11) NOT NULL,
  `techdata_quantity` int(11) NOT NULL,
  `asit_quantity` int(11) NOT NULL,
  `amazon_quantity` int(11) NOT NULL,
  `arrivo_esprinet_quantity` int(11) NOT NULL,
  `arrivo_attiva_quantity` int(11) NOT NULL,
  `arrivo_techdata_quantity` int(11) NOT NULL,
  `ordinato_quantity` int(11) NOT NULL,
  `impegnato_quantity` int(11) NOT NULL,
  `impegnato_amazon` int(11) NOT NULL,
  `trovaprezzi` int(11) NOT NULL,
  `google_shopping` int(11) NOT NULL,
  `amazon` varchar(20) NOT NULL,
  `amazon_free_shipping` int(11) NOT NULL,
  `amazon_it` decimal(10,2) NOT NULL,
  `amazon_fr` decimal(10,2) NOT NULL,
  `amazon_uk` decimal(10,2) NOT NULL,
  `amazon_es` decimal(10,2) NOT NULL,
  `amazon_de` decimal(10,2) NOT NULL,
  `amazon_nl` decimal(10,2) NOT NULL,
  `eprice` int(11) NOT NULL,
  `date_available` date DEFAULT NULL,
  `date_available_msg` text NOT NULL,
  `ottieni_miglior_prezzo_flag` int(11) NOT NULL,
  `data_arrivo` date NOT NULL,
  `data_arrivo_ordinato` varchar(300) NOT NULL,
  `data_arrivo_esprinet` date NOT NULL,
  `data_arrivo_techdata` date NOT NULL,
  `intracom_quantity` int(11) NOT NULL,
  `arrivo_intracom_quantity` int(11) NOT NULL,
  `data_arrivo_intracom` date NOT NULL,
  `canonical` int(11) NOT NULL,
  `redirect_category` int(11) NOT NULL,
  `note` text NOT NULL,
  `note_fornitore` text NOT NULL,
  `fuori_produzione` int(11) NOT NULL,
  `data_uscita_produzione` date NOT NULL,
  `acquisto_in_dollari` int(11) NOT NULL,
  `costo_dollari` decimal(10,2) NOT NULL,
  `blocco_prezzi` int(11) NOT NULL,
  `login_for_offer` int(11) NOT NULL,
  `noindex` int(11) NOT NULL,
  `prodotto_con_canone` int(11) NOT NULL,
  `qt_esolver` int(11) NOT NULL,
  `ord_esolver` int(11) NOT NULL,
  `provvigione` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `products_to_categories` (
  `products_id` int(11) NOT NULL DEFAULT '0',
  `categories_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `product_attachment` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_attachment` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_attribute` (
  `id_product_attribute` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `reference` varchar(32) DEFAULT NULL,
  `supplier_reference` varchar(32) DEFAULT NULL,
  `location` varchar(64) DEFAULT NULL,
  `ean13` varchar(13) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `ecotax` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `quantity` int(10) NOT NULL DEFAULT '0',
  `weight` float NOT NULL DEFAULT '0',
  `unit_price_impact` decimal(17,2) NOT NULL DEFAULT '0.00',
  `default_on` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `minimal_quantity` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_attribute_combination` (
  `id_attribute` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_attribute_image` (
  `id_product_attribute` int(10) UNSIGNED NOT NULL,
  `id_image` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_comment` (
  `id_product_comment` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_guest` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(64) DEFAULT NULL,
  `content` text NOT NULL,
  `pros` varchar(300) NOT NULL,
  `cons` varchar(300) NOT NULL,
  `customer_name` varchar(250) NOT NULL,
  `nickname` varchar(64) DEFAULT NULL,
  `grade` float UNSIGNED NOT NULL,
  `replica` text NOT NULL,
  `validate` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `date_add` datetime NOT NULL,
  `customer_role` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_comment_criterion` (
  `id_product_comment_criterion` int(10) UNSIGNED NOT NULL,
  `id_product_comment_criterion_type` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_comment_criterion_category` (
  `id_product_comment_criterion` int(10) UNSIGNED NOT NULL,
  `id_category` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_comment_criterion_lang` (
  `id_product_comment_criterion` int(11) UNSIGNED NOT NULL,
  `id_lang` int(11) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_comment_criterion_product` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_product_comment_criterion` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_comment_grade` (
  `id_product_comment` int(10) UNSIGNED NOT NULL,
  `id_product_comment_criterion` int(10) UNSIGNED NOT NULL,
  `grade` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_country_tax` (
  `id_product` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_tax` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_download` (
  `id_product_download` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `display_filename` varchar(255) DEFAULT NULL,
  `physically_filename` varchar(255) DEFAULT NULL,
  `date_deposit` datetime NOT NULL,
  `date_expiration` datetime DEFAULT NULL,
  `nb_days_accessible` int(10) UNSIGNED DEFAULT NULL,
  `nb_downloadable` int(10) UNSIGNED DEFAULT '1',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_esolver` (
  `id_product` int(11) NOT NULL,
  `rebate_1` decimal(10,2) NOT NULL,
  `rebate_2` decimal(10,2) NOT NULL,
  `rebate_3` decimal(10,2) NOT NULL,
  `wholesale_price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `product_group_reduction_cache` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  `reduction` decimal(4,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_lang` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `description` text,
  `description_short` text,
  `description_amazon` text NOT NULL,
  `punti_di_forza` text NOT NULL,
  `link_rewrite` varchar(128) NOT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `cat_homepage` varchar(100) NOT NULL,
  `desc_homepage` text NOT NULL,
  `available_now` varchar(255) DEFAULT NULL,
  `available_later` varchar(255) DEFAULT NULL,
  `seo_keyword` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_listini` (
  `id_product` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `listino` decimal(10,2) NOT NULL,
  `sconto_acquisto_1` decimal(10,2) NOT NULL,
  `sconto_acquisto_2` decimal(10,2) NOT NULL,
  `sconto_acquisto_3` decimal(10,2) NOT NULL,
  `wholesale_price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `product_magazzino` (
  `id_product` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `allnet` int(11) NOT NULL,
  `eds` int(11) NOT NULL,
  `itancia` int(11) NOT NULL,
  `asit` int(11) NOT NULL,
  `techdata` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_other` (
  `id_product` int(11) NOT NULL,
  `aggiornato` varchar(5) NOT NULL,
  `data_disponibilita` datetime NOT NULL,
  `fornitore` varchar(300) NOT NULL,
  `directel` varchar(200) NOT NULL,
  `redirect` varchar(200) NOT NULL,
  `wordpress_1` varchar(200) NOT NULL,
  `wordpress_2` varchar(200) NOT NULL,
  `wordpress_3` varchar(200) NOT NULL,
  `canonical` varchar(200) NOT NULL,
  `prova_gratuita` varchar(5) NOT NULL,
  `trasporto_gratuito` varchar(5) NOT NULL,
  `mostra_spese_spedizione` varchar(5) NOT NULL,
  `garanzia_3_anni` varchar(5) NOT NULL,
  `assistenza_3_anni` varchar(5) NOT NULL,
  `assistenza_base_top_gold` varchar(30) NOT NULL,
  `sconto_rivenditore_1` decimal(10,6) NOT NULL,
  `sconto_rivenditore_1_max` decimal(10,6) NOT NULL,
  `sconto_rivenditore_1_min` decimal(10,6) NOT NULL,
  `sconto_rivenditore_2` decimal(10,6) NOT NULL,
  `sconto_rivenditore_2_max` decimal(10,6) NOT NULL,
  `sconto_rivenditore_2_min` decimal(10,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `product_sale` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `sale_nbr` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `date_upd` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_tag` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_tag` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `product_video` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_product` int(11) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `profile` (
  `id_profile` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `profile_lang` (
  `id_lang` int(10) UNSIGNED NOT NULL,
  `id_profile` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `province` (
  `id` int(11) NOT NULL,
  `cod_regione` varchar(14) DEFAULT NULL,
  `cod_provincia` varchar(16) DEFAULT NULL,
  `provincia` varchar(28) DEFAULT NULL,
  `sigla` varchar(21) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `quick_access` (
  `id_quick_access` int(10) UNSIGNED NOT NULL,
  `new_window` tinyint(1) NOT NULL DEFAULT '0',
  `link` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `quick_access_lang` (
  `id_quick_access` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `range_price` (
  `id_range_price` int(10) UNSIGNED NOT NULL,
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `delimiter1` decimal(20,6) NOT NULL,
  `delimiter2` decimal(20,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `range_weight` (
  `id_range_weight` int(10) UNSIGNED NOT NULL,
  `id_carrier` int(10) UNSIGNED NOT NULL,
  `delimiter1` decimal(20,6) NOT NULL,
  `delimiter2` decimal(20,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `referrer` (
  `id_referrer` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `passwd` varchar(32) DEFAULT NULL,
  `http_referer_regexp` varchar(64) DEFAULT NULL,
  `http_referer_like` varchar(64) DEFAULT NULL,
  `request_uri_regexp` varchar(64) DEFAULT NULL,
  `request_uri_like` varchar(64) DEFAULT NULL,
  `http_referer_regexp_not` varchar(64) DEFAULT NULL,
  `http_referer_like_not` varchar(64) DEFAULT NULL,
  `request_uri_regexp_not` varchar(64) DEFAULT NULL,
  `request_uri_like_not` varchar(64) DEFAULT NULL,
  `base_fee` decimal(5,2) NOT NULL DEFAULT '0.00',
  `percent_fee` decimal(5,2) NOT NULL DEFAULT '0.00',
  `click_fee` decimal(5,2) NOT NULL DEFAULT '0.00',
  `cache_visitors` int(11) DEFAULT NULL,
  `cache_visits` int(11) DEFAULT NULL,
  `cache_pages` int(11) DEFAULT NULL,
  `cache_registrations` int(11) DEFAULT NULL,
  `cache_orders` int(11) DEFAULT NULL,
  `cache_sales` decimal(17,2) DEFAULT NULL,
  `cache_reg_rate` decimal(5,4) DEFAULT NULL,
  `cache_order_rate` decimal(5,4) DEFAULT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `referrer_cache` (
  `id_connections_source` int(11) UNSIGNED NOT NULL,
  `id_referrer` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `required_field` (
  `id_required_field` int(11) NOT NULL,
  `object_name` varchar(32) NOT NULL,
  `field_name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `rimborsi` (
  `id_order` int(11) NOT NULL,
  `product_id` varchar(100) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `product_price` decimal(10,6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `rma` (
  `id_customer_thread` int(11) NOT NULL,
  `codice_rma` varchar(5) NOT NULL,
  `rma_product` varchar(300) NOT NULL,
  `qta` int(11) NOT NULL,
  `seriale` varchar(100) NOT NULL,
  `rma_type` varchar(100) NOT NULL,
  `rma_shipping` varchar(100) NOT NULL,
  `arrivoezcli` int(11) NOT NULL,
  `arrivocli` datetime NOT NULL,
  `a_carico` int(11) NOT NULL,
  `corriere` int(11) NOT NULL,
  `richiesto_rma` int(11) NOT NULL,
  `il_fornitore` varchar(100) NOT NULL,
  `data_richiesta` datetime NOT NULL,
  `codice_rma_fornitore` varchar(100) NOT NULL,
  `arrivoezfornitore` datetime NOT NULL,
  `sostituitoriparato` int(11) NOT NULL,
  `id_address` int(11) NOT NULL,
  `attivita_svolta` varchar(150) NOT NULL,
  `altra_attivita` varchar(300) NOT NULL,
  `costo_materiale` decimal(10,2) NOT NULL,
  `costo_manodopera` decimal(10,2) NOT NULL,
  `trasporti_qta` int(11) NOT NULL,
  `manodopera_qta` decimal(10,2) NOT NULL,
  `totale_trasporti` decimal(10,2) NOT NULL,
  `in_garanzia` int(11) NOT NULL,
  `test` int(11) NOT NULL,
  `primo_test` int(11) NOT NULL,
  `test_in_carico_a` int(11) NOT NULL,
  `problematica_test` text NOT NULL,
  `urgente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `scadenze` (
  `id_scadenza` int(11) NOT NULL,
  `scadenza` varchar(500) NOT NULL,
  `tipo` varchar(100) NOT NULL,
  `decorrenza` date NOT NULL,
  `referente` varchar(100) NOT NULL,
  `importo` varchar(100) NOT NULL,
  `esito` varchar(100) NOT NULL,
  `note` text NOT NULL,
  `data_scadenza` datetime NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `scene` (
  `id_scene` int(10) UNSIGNED NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `scene_category` (
  `id_scene` int(10) UNSIGNED NOT NULL,
  `id_category` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `scene_lang` (
  `id_scene` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `scene_products` (
  `id_scene` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `x_axis` int(4) NOT NULL,
  `y_axis` int(4) NOT NULL,
  `zone_width` int(3) NOT NULL,
  `zone_height` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `search_engine` (
  `id_search_engine` int(10) UNSIGNED NOT NULL,
  `server` varchar(64) NOT NULL,
  `getvar` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `search_index` (
  `id_product` int(11) UNSIGNED NOT NULL,
  `id_word` int(11) UNSIGNED NOT NULL,
  `weight` smallint(4) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `search_word` (
  `id_word` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `word` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sekeyword` (
  `id_sekeyword` int(10) UNSIGNED NOT NULL,
  `keyword` varchar(256) NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sendin_newsletter` (
  `id` int(6) NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_shop_group` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `email` varchar(255) NOT NULL,
  `newsletter_date_add` datetime DEFAULT NULL,
  `ip_registration_newsletter` varchar(15) NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `specific_price` (
  `id_specific_price` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_shop` tinyint(3) UNSIGNED NOT NULL,
  `id_currency` int(10) UNSIGNED NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_group` int(10) UNSIGNED NOT NULL,
  `price` decimal(20,2) NOT NULL,
  `from_quantity` mediumint(8) UNSIGNED NOT NULL,
  `reduction` decimal(20,4) NOT NULL,
  `reduction_type` enum('amount','percentage') NOT NULL,
  `wholesale_price` decimal(10,2) NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  `specific_price_name` varchar(400) NOT NULL,
  `abilita_banda` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `specific_price_pieces` (
  `id_specific_price` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `pieces` int(11) NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `specific_price_priority` (
  `id_specific_price_priority` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `priority` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `specific_price_wholesale` (
  `id_specific_price` int(11) NOT NULL,
  `id_product` varchar(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `reduction_1` decimal(10,2) NOT NULL,
  `reduction_2` decimal(10,2) NOT NULL,
  `reduction_3` decimal(10,2) NOT NULL,
  `wholesale_price` decimal(10,2) NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  `pieces` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `state` (
  `id_state` int(10) UNSIGNED NOT NULL,
  `id_country` int(11) UNSIGNED NOT NULL,
  `id_zone` int(11) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `iso_code` char(4) NOT NULL,
  `tax_behavior` smallint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `id_state_importerosc` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `statssearch` (
  `id_statssearch` int(10) UNSIGNED NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `results` int(6) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `stock_mvt` (
  `id_stock_mvt` int(11) UNSIGNED NOT NULL,
  `id_product` int(11) UNSIGNED DEFAULT NULL,
  `id_product_attribute` int(11) UNSIGNED DEFAULT NULL,
  `id_order` int(11) UNSIGNED DEFAULT NULL,
  `id_stock_mvt_reason` int(11) UNSIGNED NOT NULL,
  `id_employee` int(11) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `stock_mvt_reason` (
  `id_stock_mvt_reason` int(11) NOT NULL,
  `sign` tinyint(1) NOT NULL DEFAULT '1',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `stock_mvt_reason_lang` (
  `id_stock_mvt_reason` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `store` (
  `id_store` int(10) UNSIGNED NOT NULL,
  `id_country` int(10) UNSIGNED NOT NULL,
  `id_state` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `address1` varchar(128) NOT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `city` varchar(64) NOT NULL,
  `postcode` varchar(12) NOT NULL,
  `latitude` decimal(11,8) DEFAULT NULL,
  `longitude` decimal(11,8) DEFAULT NULL,
  `hours` varchar(254) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `fax` varchar(16) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `note` text,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `storico_attivita` (
  `id_storico` int(11) NOT NULL,
  `id_attivita` int(11) NOT NULL,
  `tipo_attivita` varchar(10) NOT NULL,
  `tipo_num` int(11) NOT NULL,
  `data_attivita` datetime NOT NULL,
  `id_employee` int(11) NOT NULL,
  `desc_attivita` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `storico_prezzi` (
  `id_product` int(11) NOT NULL,
  `storico` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `subdomain` (
  `id_subdomain` int(10) UNSIGNED NOT NULL,
  `name` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `supplier` (
  `id_supplier` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `supplier_lang` (
  `id_supplier` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `description` text,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tab` (
  `id_tab` int(10) UNSIGNED NOT NULL,
  `id_parent` int(11) NOT NULL,
  `class_name` varchar(64) NOT NULL,
  `module` varchar(64) DEFAULT NULL,
  `position` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tab_lang` (
  `id_tab` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tag` (
  `id_tag` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tax` (
  `id_tax` int(10) UNSIGNED NOT NULL,
  `rate` decimal(10,3) NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tax_lang` (
  `id_tax` int(10) UNSIGNED NOT NULL,
  `id_lang` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tax_rule` (
  `id_tax_rule` int(11) NOT NULL,
  `id_tax_rules_group` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_state` int(11) NOT NULL,
  `id_county` int(11) NOT NULL,
  `id_tax` int(11) NOT NULL,
  `state_behavior` int(11) NOT NULL,
  `county_behavior` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tax_rules_group` (
  `id_tax_rules_group` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `telefonate` (
  `id_telefonata` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `clid` varchar(150) NOT NULL,
  `src` varchar(100) NOT NULL,
  `dst` varchar(100) NOT NULL,
  `srctrunk` varchar(100) NOT NULL,
  `lastapp` varchar(100) NOT NULL,
  `duration` int(11) NOT NULL,
  `billable` int(11) NOT NULL,
  `disposition` varchar(100) NOT NULL,
  `calltype` varchar(50) NOT NULL,
  `uniqueid` varchar(150) NOT NULL,
  `recordfile` varchar(200) NOT NULL,
  `recordpath` varchar(200) NOT NULL,
  `extfield1` varchar(100) NOT NULL,
  `extfield2` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `ticket_cc` (
  `msg` int(11) NOT NULL,
  `type` varchar(2) NOT NULL,
  `cc` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `ticket_convertiti` (
  `old_id` varchar(100) NOT NULL,
  `new_id` varchar(100) NOT NULL,
  `t_from` varchar(100) NOT NULL,
  `t_to` varchar(100) NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `ticket_promemoria` (
  `msg` int(11) NOT NULL,
  `tipo_att` varchar(20) NOT NULL,
  `ora` datetime NOT NULL,
  `conto` int(11) NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `timezone` (
  `id_timezone` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `trgt` (
  `id` int(11) NOT NULL,
  `descrizione` varchar(100) NOT NULL,
  `anno` varchar(4) NOT NULL,
  `configurazione` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `valutazione` (
  `id_modulo` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `supporto` int(11) NOT NULL,
  `ricevutosupporto` int(11) NOT NULL,
  `completezza` int(11) NOT NULL,
  `consegna` int(11) NOT NULL,
  `catalogo` int(11) NOT NULL,
  `tornare` int(11) NOT NULL,
  `motivotornare` text NOT NULL,
  `news` varchar(50) NOT NULL,
  `suggerimento` text NOT NULL,
  `prodotti` varchar(400) NOT NULL,
  `posizione` varchar(200) NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `webservice_account` (
  `id_webservice_account` int(11) NOT NULL,
  `key` varchar(32) NOT NULL,
  `description` text,
  `class_name` varchar(50) NOT NULL DEFAULT 'WebserviceRequest',
  `is_module` tinyint(2) NOT NULL DEFAULT '0',
  `module_name` varchar(50) DEFAULT NULL,
  `active` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `webservice_permission` (
  `id_webservice_permission` int(11) NOT NULL,
  `resource` varchar(50) NOT NULL,
  `method` enum('GET','POST','PUT','DELETE','HEAD') NOT NULL,
  `id_webservice_account` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `web_browser` (
  `id_web_browser` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `wishlist` (
  `id_wishlist` int(10) UNSIGNED NOT NULL,
  `id_customer` int(10) UNSIGNED NOT NULL,
  `token` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `counter` int(10) UNSIGNED DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `wishlist_email` (
  `id_wishlist` int(10) UNSIGNED NOT NULL,
  `email` varchar(128) NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `wishlist_product` (
  `id_wishlist_product` int(10) NOT NULL,
  `id_wishlist` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED NOT NULL,
  `id_product_attribute` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `priority` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `wishlist_product_cart` (
  `id_wishlist_product` int(10) UNSIGNED NOT NULL,
  `id_cart` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `date_add` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `zone` (
  `id_zone` int(10) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `id_zone_importerosc` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `access`
  ADD PRIMARY KEY (`id_profile`,`id_tab`);

ALTER TABLE `accessory`
  ADD PRIMARY KEY (`id_product_1`,`id_product_2`);

ALTER TABLE `action_message`
  ADD PRIMARY KEY (`id_action_message`),
  ADD KEY `id_action_message` (`id_action_message`,`id_action`,`id_customer`,`action_m_from`,`action_m_to`),
  ADD KEY `date_add` (`date_add`,`date_upd`);

ALTER TABLE `action_message_employee`
  ADD PRIMARY KEY (`id_action_message_employee`),
  ADD KEY `id_action_message` (`id_action_message_employee`,`id_action`,`id_employee`,`action_m_from`,`action_m_to`),
  ADD KEY `date_add` (`date_add`,`date_upd`);

ALTER TABLE `action_thread`
  ADD PRIMARY KEY (`id_action`),
  ADD KEY `action_type` (`action_type`,`id_customer`,`status`,`date_add`,`date_upd`),
  ADD KEY `id_action` (`id_action`),
  ADD KEY `action_to` (`action_to`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `date_add` (`date_add`);

ALTER TABLE `action_thread_employee`
  ADD PRIMARY KEY (`id_action`),
  ADD KEY `action_type` (`action_type`,`id_employee`,`status`,`date_add`,`date_upd`),
  ADD KEY `id_action` (`id_action`),
  ADD KEY `action_to` (`action_to`),
  ADD KEY `id_employee` (`id_employee`),
  ADD KEY `date_add` (`date_add`);

ALTER TABLE `address`
  ADD PRIMARY KEY (`id_address`),
  ADD KEY `address_customer` (`id_customer`),
  ADD KEY `id_country` (`id_country`),
  ADD KEY `id_state` (`id_state`),
  ADD KEY `id_manufacturer` (`id_manufacturer`),
  ADD KEY `id_supplier` (`id_supplier`);

ALTER TABLE `address_book`
  ADD PRIMARY KEY (`address_book_id`),
  ADD KEY `idx_address_book_customers_id` (`customers_id`),
  ADD KEY `entry_state` (`entry_state`),
  ADD KEY `entry_company` (`entry_company`),
  ADD KEY `entry_city` (`entry_city`);

ALTER TABLE `address_format`
  ADD PRIMARY KEY (`id_country`);

ALTER TABLE `agente`
  ADD PRIMARY KEY (`id_agente`);

ALTER TABLE `alias`
  ADD PRIMARY KEY (`id_alias`),
  ADD UNIQUE KEY `alias` (`alias`);

ALTER TABLE `amazon_verifica_pagamenti`
  ADD PRIMARY KEY (`id_pagamento`);

ALTER TABLE `attachment`
  ADD PRIMARY KEY (`id_attachment`);

ALTER TABLE `attachment_lang`
  ADD PRIMARY KEY (`id_attachment`,`id_lang`);

ALTER TABLE `attribute`
  ADD PRIMARY KEY (`id_attribute`),
  ADD KEY `attribute_group` (`id_attribute_group`);

ALTER TABLE `attribute_group`
  ADD PRIMARY KEY (`id_attribute_group`);

ALTER TABLE `attribute_group_lang`
  ADD PRIMARY KEY (`id_attribute_group`,`id_lang`);

ALTER TABLE `attribute_impact`
  ADD PRIMARY KEY (`id_attribute_impact`),
  ADD UNIQUE KEY `id_product` (`id_product`,`id_attribute`);

ALTER TABLE `attribute_lang`
  ADD PRIMARY KEY (`id_attribute`,`id_lang`),
  ADD KEY `id_lang` (`id_lang`,`name`);

ALTER TABLE `bdl`
  ADD PRIMARY KEY (`id_bdl`);

ALTER TABLE `bundle`
  ADD PRIMARY KEY (`id_bundle`);

ALTER TABLE `bundle_image`
  ADD PRIMARY KEY (`id_image`);

ALTER TABLE `bundle_lang`
  ADD PRIMARY KEY (`id_bundle`,`id_lang`);

ALTER TABLE `campi_esolver`
  ADD PRIMARY KEY (`id_product`);

ALTER TABLE `cap`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `carrelli_creati`
  ADD PRIMARY KEY (`id_cart`),
  ADD KEY `cart_customer` (`id_customer`),
  ADD KEY `id_address_delivery` (`id_address_delivery`),
  ADD KEY `id_address_invoice` (`id_address_invoice`),
  ADD KEY `id_carrier` (`id_carrier`),
  ADD KEY `id_lang` (`id_lang`),
  ADD KEY `id_currency` (`id_currency`),
  ADD KEY `id_guest` (`id_guest`);

ALTER TABLE `carrelli_creati_prodotti`
  ADD PRIMARY KEY (`id_cart`,`id_product`,`id_product_attribute`),
  ADD KEY `id_product_attribute` (`id_product_attribute`);

ALTER TABLE `carrier`
  ADD PRIMARY KEY (`id_carrier`),
  ADD KEY `deleted` (`deleted`,`active`),
  ADD KEY `id_tax_rules_group` (`id_tax_rules_group`);

ALTER TABLE `carrier_group`
  ADD PRIMARY KEY (`id_carrier`,`id_group`);

ALTER TABLE `carrier_lang`
  ADD PRIMARY KEY (`id_carrier`,`id_lang`);

ALTER TABLE `carrier_zone`
  ADD PRIMARY KEY (`id_carrier`,`id_zone`);

ALTER TABLE `cart`
  ADD PRIMARY KEY (`id_cart`),
  ADD KEY `cart_customer` (`id_customer`),
  ADD KEY `id_address_delivery` (`id_address_delivery`),
  ADD KEY `id_address_invoice` (`id_address_invoice`),
  ADD KEY `id_carrier` (`id_carrier`),
  ADD KEY `id_lang` (`id_lang`),
  ADD KEY `id_currency` (`id_currency`),
  ADD KEY `id_guest` (`id_guest`),
  ADD KEY `idx_revisioni` (`revisioni`),
  ADD KEY `preventivo` (`preventivo`),
  ADD KEY `date_add` (`date_add`),
  ADD KEY `date_upd` (`date_upd`),
  ADD KEY `id_cart` (`id_cart`);

ALTER TABLE `cart_discount`
  ADD PRIMARY KEY (`id_cart`,`id_discount`),
  ADD KEY `id_discount` (`id_discount`);

ALTER TABLE `cart_ezcloud`
  ADD PRIMARY KEY (`id_cart`);

ALTER TABLE `cart_note`
  ADD PRIMARY KEY (`id_note`);

ALTER TABLE `cart_product`
  ADD PRIMARY KEY (`id_cart`,`id_product`,`id_product_attribute`),
  ADD KEY `id_product_attribute` (`id_product_attribute`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_cart` (`id_cart`),
  ADD KEY `id_cart_id_product` (`id_cart`,`id_product`);

ALTER TABLE `cart_product_revisioni`
  ADD PRIMARY KEY (`id_revisione`,`id_cart`,`id_product`,`id_product_attribute`),
  ADD KEY `id_product_attribute` (`id_product_attribute`);

ALTER TABLE `cart_revisioni`
  ADD PRIMARY KEY (`id_revisione`),
  ADD KEY `cart_customer` (`id_customer`),
  ADD KEY `id_address_delivery` (`id_address_delivery`),
  ADD KEY `id_address_invoice` (`id_address_invoice`),
  ADD KEY `id_carrier` (`id_carrier`),
  ADD KEY `id_lang` (`id_lang`),
  ADD KEY `id_currency` (`id_currency`),
  ADD KEY `id_guest` (`id_guest`);

ALTER TABLE `category`
  ADD PRIMARY KEY (`id_category`),
  ADD KEY `category_parent` (`id_parent`),
  ADD KEY `nleftright` (`nleft`,`nright`);

ALTER TABLE `category_group`
  ADD PRIMARY KEY (`id_category`,`id_group`),
  ADD KEY `id_category` (`id_category`),
  ADD KEY `id_group` (`id_group`);

ALTER TABLE `category_lang`
  ADD PRIMARY KEY (`id_category`,`id_lang`),
  ADD KEY `category_name` (`name`);

ALTER TABLE `category_product`
  ADD UNIQUE KEY `category_product_index` (`id_category`,`id_product`),
  ADD KEY `id_product` (`id_product`);

ALTER TABLE `chatban`
  ADD PRIMARY KEY (`banid`);

ALTER TABLE `chatconfig`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `chatgroup`
  ADD PRIMARY KEY (`groupid`);

ALTER TABLE `chatmessage`
  ADD PRIMARY KEY (`messageid`);

ALTER TABLE `chatoperator`
  ADD PRIMARY KEY (`operatorid`);

ALTER TABLE `chatresponses`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `chatthread`
  ADD PRIMARY KEY (`threadid`);

ALTER TABLE `chat_availableupdate`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `target` (`target`);

ALTER TABLE `chat_ban`
  ADD PRIMARY KEY (`banid`);

ALTER TABLE `chat_cannedmessage`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `chat_config`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `chat_locale`
  ADD PRIMARY KEY (`localeid`),
  ADD UNIQUE KEY `code` (`code`);

ALTER TABLE `chat_mailtemplate`
  ADD PRIMARY KEY (`templateid`);

ALTER TABLE `chat_message`
  ADD PRIMARY KEY (`messageid`),
  ADD KEY `idx_agentid` (`agentid`);

ALTER TABLE `chat_operator2`
  ADD PRIMARY KEY (`operatorid`);

ALTER TABLE `chat_operatorstatistics`
  ADD PRIMARY KEY (`statid`),
  ADD KEY `operatorid` (`operatorid`);

ALTER TABLE `chat_operatortoopgroup`
  ADD KEY `groupid` (`groupid`),
  ADD KEY `operatorid` (`operatorid`);

ALTER TABLE `chat_opgroup`
  ADD PRIMARY KEY (`groupid`),
  ADD KEY `parent` (`parent`);

ALTER TABLE `chat_plugin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

ALTER TABLE `chat_requestbuffer`
  ADD PRIMARY KEY (`requestid`),
  ADD KEY `requestkey` (`requestkey`);

ALTER TABLE `chat_requestcallback`
  ADD PRIMARY KEY (`callbackid`),
  ADD KEY `token` (`token`);

ALTER TABLE `chat_revision`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `chat_sitevisitor`
  ADD PRIMARY KEY (`visitorid`),
  ADD KEY `threadid` (`threadid`);

ALTER TABLE `chat_thread2`
  ADD PRIMARY KEY (`threadid`);

ALTER TABLE `chat_threadstatistics`
  ADD PRIMARY KEY (`statid`);

ALTER TABLE `chat_translation`
  ADD PRIMARY KEY (`translationid`),
  ADD UNIQUE KEY `hash` (`hash`);

ALTER TABLE `chat_visitedpage`
  ADD PRIMARY KEY (`pageid`),
  ADD KEY `visitorid` (`visitorid`);

ALTER TABLE `chat_visitedpagestatistics`
  ADD PRIMARY KEY (`pageid`);

ALTER TABLE `cms`
  ADD PRIMARY KEY (`id_cms`);

ALTER TABLE `cms_block`
  ADD PRIMARY KEY (`id_cms_block`);

ALTER TABLE `cms_block_lang`
  ADD PRIMARY KEY (`id_cms_block`,`id_lang`);

ALTER TABLE `cms_block_page`
  ADD PRIMARY KEY (`id_cms_block_page`);

ALTER TABLE `cms_category`
  ADD PRIMARY KEY (`id_cms_category`),
  ADD KEY `category_parent` (`id_parent`);

ALTER TABLE `cms_category_lang`
  ADD UNIQUE KEY `category_lang_index` (`id_cms_category`,`id_lang`),
  ADD KEY `category_name` (`name`);

ALTER TABLE `cms_lang`
  ADD PRIMARY KEY (`id_cms`,`id_lang`);

ALTER TABLE `commissioni_marketplace`
  ADD PRIMARY KEY (`id_order`,`id_cart`,`seller_sku`),
  ADD KEY `seller_sku` (`seller_sku`);

ALTER TABLE `compare`
  ADD PRIMARY KEY (`id_compare`);

ALTER TABLE `compare_product`
  ADD PRIMARY KEY (`id_compare`,`id_product`);

ALTER TABLE `comuni`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `configuration`
  ADD PRIMARY KEY (`id_configuration`),
  ADD UNIQUE KEY `name` (`name`);

ALTER TABLE `configuration_lang`
  ADD PRIMARY KEY (`id_configuration`,`id_lang`);

ALTER TABLE `connections`
  ADD PRIMARY KEY (`id_connections`),
  ADD KEY `id_guest` (`id_guest`),
  ADD KEY `date_add` (`date_add`),
  ADD KEY `id_page` (`id_page`),
  ADD KEY `id_connections` (`id_connections`,`id_guest`);

ALTER TABLE `connections_cache`
  ADD PRIMARY KEY (`id_connections`),
  ADD KEY `id_guest` (`id_guest`),
  ADD KEY `date_add` (`date_add`),
  ADD KEY `id_page` (`id_page`),
  ADD KEY `id_connections` (`id_connections`,`id_guest`);

ALTER TABLE `connections_page`
  ADD PRIMARY KEY (`id_connections`,`id_page`,`time_start`);

ALTER TABLE `connections_page_cache`
  ADD PRIMARY KEY (`id_connections`,`id_page`,`time_start`);

ALTER TABLE `connections_source`
  ADD PRIMARY KEY (`id_connections_source`),
  ADD KEY `connections` (`id_connections`),
  ADD KEY `orderby` (`date_add`),
  ADD KEY `http_referer` (`http_referer`),
  ADD KEY `request_uri` (`request_uri`);

ALTER TABLE `contact`
  ADD PRIMARY KEY (`id_contact`);

ALTER TABLE `contact_lang`
  ADD PRIMARY KEY (`id_contact`,`id_lang`);

ALTER TABLE `contratto_assistenza`
  ADD PRIMARY KEY (`id_contratto`);

ALTER TABLE `country`
  ADD PRIMARY KEY (`id_country`),
  ADD KEY `country_iso_code` (`iso_code`),
  ADD KEY `country_` (`id_zone`);

ALTER TABLE `country_lang`
  ADD PRIMARY KEY (`id_country`,`id_lang`);

ALTER TABLE `county`
  ADD PRIMARY KEY (`id_county`);

ALTER TABLE `county_zip_code`
  ADD PRIMARY KEY (`id_county`,`from_zip_code`,`to_zip_code`);

ALTER TABLE `currency`
  ADD PRIMARY KEY (`id_currency`);

ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`),
  ADD KEY `customer_email` (`email`),
  ADD KEY `customer_login` (`email`,`passwd`),
  ADD KEY `id_customer_passwd` (`id_customer`,`passwd`),
  ADD KEY `id_gender` (`id_gender`);

ALTER TABLE `customer_amministrazione`
  ADD KEY `id_customer` (`id_customer`,`tipo_soggetto`,`agente`,`tecnico`);

ALTER TABLE `customer_group`
  ADD PRIMARY KEY (`id_customer`,`id_group`),
  ADD KEY `customer_login` (`id_group`),
  ADD KEY `id_customer` (`id_customer`);

ALTER TABLE `customer_message`
  ADD PRIMARY KEY (`id_customer_message`),
  ADD KEY `id_customer_thread` (`id_customer_thread`),
  ADD KEY `id_employee` (`id_employee`);

ALTER TABLE `customer_note`
  ADD PRIMARY KEY (`id_note`);

ALTER TABLE `customer_thread`
  ADD PRIMARY KEY (`id_customer_thread`),
  ADD KEY `id_customer_thread` (`id_customer_thread`,`id_contact`,`id_customer`,`id_employee`,`date_add`,`date_upd`),
  ADD KEY `id_contact` (`id_contact`);

ALTER TABLE `customization`
  ADD PRIMARY KEY (`id_customization`,`id_cart`,`id_product`),
  ADD KEY `id_product_attribute` (`id_product_attribute`);

ALTER TABLE `customization_field`
  ADD PRIMARY KEY (`id_customization_field`),
  ADD KEY `id_product` (`id_product`);

ALTER TABLE `customization_field_lang`
  ADD PRIMARY KEY (`id_customization_field`,`id_lang`);

ALTER TABLE `customized_data`
  ADD PRIMARY KEY (`id_customization`,`type`,`index`);

ALTER TABLE `date_range`
  ADD PRIMARY KEY (`id_date_range`);

ALTER TABLE `dati_tecnici_prodotti`
  ADD PRIMARY KEY (`id_order_detail`,`id_product`,`riga`);

ALTER TABLE `delivery`
  ADD PRIMARY KEY (`id_delivery`),
  ADD KEY `id_zone` (`id_zone`),
  ADD KEY `id_carrier` (`id_carrier`,`id_zone`),
  ADD KEY `id_range_price` (`id_range_price`),
  ADD KEY `id_range_weight` (`id_range_weight`);

ALTER TABLE `discount`
  ADD PRIMARY KEY (`id_discount`),
  ADD KEY `discount_name` (`name`),
  ADD KEY `discount_customer` (`id_customer`),
  ADD KEY `id_discount_type` (`id_discount_type`);

ALTER TABLE `discount_category`
  ADD PRIMARY KEY (`id_category`,`id_discount`),
  ADD KEY `discount` (`id_discount`);

ALTER TABLE `discount_lang`
  ADD PRIMARY KEY (`id_discount`,`id_lang`);

ALTER TABLE `discount_type`
  ADD PRIMARY KEY (`id_discount_type`);

ALTER TABLE `discount_type_lang`
  ADD PRIMARY KEY (`id_discount_type`,`id_lang`);

ALTER TABLE `editorial`
  ADD PRIMARY KEY (`id_editorial`);

ALTER TABLE `editorial_lang`
  ADD PRIMARY KEY (`id_editorial`,`id_lang`);

ALTER TABLE `employee`
  ADD PRIMARY KEY (`id_employee`),
  ADD KEY `employee_login` (`email`,`passwd`),
  ADD KEY `id_employee_passwd` (`id_employee`,`passwd`),
  ADD KEY `id_profile` (`id_profile`),
  ADD KEY `idx_firstname` (`firstname`),
  ADD KEY `idx_lastname` (`lastname`),
  ADD KEY `last_activity` (`last_activity`),
  ADD KEY `id_lang` (`id_lang`),
  ADD KEY `id_employee` (`id_employee`);

ALTER TABLE `evento_yeastar_21102014`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `ex_rivenditori`
  ADD PRIMARY KEY (`id_customer`);

ALTER TABLE `ezcloud`
  ADD PRIMARY KEY (`id_ezcloud`);

ALTER TABLE `ezcloud_valutazione`
  ADD PRIMARY KEY (`id_modulo`);

ALTER TABLE `ezdial_licenze`
  ADD PRIMARY KEY (`id_licenza`);

ALTER TABLE `ezdial_mac_address`
  ADD PRIMARY KEY (`id_mac_address`);

ALTER TABLE `fattura`
  ADD PRIMARY KEY (`id_riga`),
  ADD KEY `id_customer` (`id_customer`,`codice_cliente`,`id_fattura`,`tipo`,`data_fattura`),
  ADD KEY `id_fattura` (`id_fattura`),
  ADD KEY `rif_ordine` (`rif_ordine`),
  ADD KEY `id_riga` (`id_riga`),
  ADD KEY `rif_vs_ordine` (`rif_vs_ordine`);

ALTER TABLE `fatture_acquisto`
  ADD PRIMARY KEY (`id_riga`);

ALTER TABLE `feature`
  ADD PRIMARY KEY (`id_feature`);

ALTER TABLE `feature_group`
  ADD PRIMARY KEY (`id_group`);

ALTER TABLE `feature_lang`
  ADD PRIMARY KEY (`id_feature`,`id_lang`);

ALTER TABLE `feature_product`
  ADD PRIMARY KEY (`id_feature`,`id_product`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_feature` (`id_feature`,`id_product`) USING BTREE;

ALTER TABLE `feature_value`
  ADD PRIMARY KEY (`id_feature_value`),
  ADD KEY `feature` (`id_feature`);

ALTER TABLE `feature_value_lang`
  ADD PRIMARY KEY (`id_feature_value`,`id_lang`);

ALTER TABLE `ff`
  ADD PRIMARY KEY (`nm`,`an`);

ALTER TABLE `fixed_ck`
  ADD PRIMARY KEY (`id_ck`);

ALTER TABLE `form_prevendita_message`
  ADD PRIMARY KEY (`id_message`);

ALTER TABLE `form_prevendita_thread`
  ADD PRIMARY KEY (`id_thread`),
  ADD KEY `tipo_richiesta` (`tipo_richiesta`,`id_customer`,`id_employee`,`date_add`,`date_upd`);

ALTER TABLE `gestione_link`
  ADD PRIMARY KEY (`id_regola`);

ALTER TABLE `gestpay_currencies_map`
  ADD PRIMARY KEY (`id_prestashop`),
  ADD UNIQUE KEY `id_gestpay` (`id_gestpay`);

ALTER TABLE `gestpay_languages_map`
  ADD PRIMARY KEY (`code_prestashop`),
  ADD UNIQUE KEY `id_gestpay` (`id_gestpay`);

ALTER TABLE `group`
  ADD PRIMARY KEY (`id_group`);

ALTER TABLE `group_lang`
  ADD PRIMARY KEY (`id_group`,`id_lang`);

ALTER TABLE `group_reduction`
  ADD PRIMARY KEY (`id_group_reduction`),
  ADD UNIQUE KEY `id_group` (`id_group`,`id_category`);

ALTER TABLE `guest`
  ADD PRIMARY KEY (`id_guest`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_operating_system` (`id_operating_system`),
  ADD KEY `id_web_browser` (`id_web_browser`),
  ADD KEY `id_guest` (`id_guest`);

ALTER TABLE `help_access`
  ADD PRIMARY KEY (`id_help_access`),
  ADD UNIQUE KEY `label` (`label`);

ALTER TABLE `hook`
  ADD PRIMARY KEY (`id_hook`),
  ADD UNIQUE KEY `hook_name` (`name`);

ALTER TABLE `hook_module`
  ADD PRIMARY KEY (`id_module`,`id_hook`),
  ADD KEY `id_hook` (`id_hook`),
  ADD KEY `id_module` (`id_module`);

ALTER TABLE `hook_module_exceptions`
  ADD PRIMARY KEY (`id_hook_module_exceptions`),
  ADD KEY `id_module` (`id_module`),
  ADD KEY `id_hook` (`id_hook`);

ALTER TABLE `image`
  ADD PRIMARY KEY (`id_image`),
  ADD KEY `image_product` (`id_product`),
  ADD KEY `id_product_cover` (`id_product`,`cover`);

ALTER TABLE `image_lang`
  ADD PRIMARY KEY (`id_image`,`id_lang`),
  ADD KEY `id_image` (`id_image`);

ALTER TABLE `image_type`
  ADD PRIMARY KEY (`id_image_type`),
  ADD KEY `image_type_name` (`name`);

ALTER TABLE `import_match`
  ADD PRIMARY KEY (`id_import_match`);

ALTER TABLE `lang`
  ADD PRIMARY KEY (`id_lang`),
  ADD KEY `lang_iso_code` (`iso_code`);

ALTER TABLE `layered_category`
  ADD PRIMARY KEY (`id_layered_category`),
  ADD KEY `id_category` (`id_category`,`type`);

ALTER TABLE `layered_filter`
  ADD PRIMARY KEY (`id_layered_filter`);

ALTER TABLE `layered_filter_shop`
  ADD PRIMARY KEY (`id_layered_filter`,`id_shop`),
  ADD KEY `id_shop` (`id_shop`);

ALTER TABLE `layered_friendly_url`
  ADD PRIMARY KEY (`id_layered_friendly_url`),
  ADD KEY `id_lang` (`id_lang`),
  ADD KEY `url_key` (`url_key`(5));

ALTER TABLE `layered_indexable_attribute_group`
  ADD PRIMARY KEY (`id_attribute_group`);

ALTER TABLE `layered_indexable_attribute_group_lang_value`
  ADD PRIMARY KEY (`id_attribute_group`,`id_lang`);

ALTER TABLE `layered_indexable_attribute_lang_value`
  ADD PRIMARY KEY (`id_attribute`,`id_lang`);

ALTER TABLE `layered_indexable_feature`
  ADD PRIMARY KEY (`id_feature`);

ALTER TABLE `layered_indexable_feature_lang_value`
  ADD PRIMARY KEY (`id_feature`,`id_lang`);

ALTER TABLE `layered_indexable_feature_value_lang_value`
  ADD PRIMARY KEY (`id_feature_value`,`id_lang`);

ALTER TABLE `layered_price_index`
  ADD PRIMARY KEY (`id_product`,`id_currency`,`id_shop`),
  ADD KEY `id_currency` (`id_currency`),
  ADD KEY `price_min` (`price_min`),
  ADD KEY `price_max` (`price_max`);

ALTER TABLE `layered_product_attribute`
  ADD KEY `id_attribute` (`id_attribute`);

ALTER TABLE `location_coords`
  ADD PRIMARY KEY (`id_location_coords`);

ALTER TABLE `log`
  ADD PRIMARY KEY (`id_log`);

ALTER TABLE `loyalty`
  ADD PRIMARY KEY (`id_loyalty`),
  ADD KEY `index_loyalty_loyalty_state` (`id_loyalty_state`),
  ADD KEY `index_loyalty_order` (`id_order`),
  ADD KEY `index_loyalty_discount` (`id_discount`),
  ADD KEY `index_loyalty_customer` (`id_customer`);

ALTER TABLE `loyalty_history`
  ADD PRIMARY KEY (`id_loyalty_history`),
  ADD KEY `index_loyalty_history_loyalty` (`id_loyalty`),
  ADD KEY `index_loyalty_history_loyalty_state` (`id_loyalty_state`);

ALTER TABLE `loyalty_state`
  ADD PRIMARY KEY (`id_loyalty_state`),
  ADD KEY `index_loyalty_state_order_state` (`id_order_state`);

ALTER TABLE `loyalty_state_lang`
  ADD UNIQUE KEY `index_unique_loyalty_state_lang` (`id_loyalty_state`,`id_lang`);

ALTER TABLE `mailalert_customer_oos`
  ADD PRIMARY KEY (`id_customer`,`customer_email`,`id_product`,`id_product_attribute`);

ALTER TABLE `mailing_history`
  ADD PRIMARY KEY (`id_campaign`);

ALTER TABLE `mailing_import`
  ADD PRIMARY KEY (`ID`);

ALTER TABLE `mailing_sent`
  ADD KEY `email` (`email`);

ALTER TABLE `mailing_track`
  ADD PRIMARY KEY (`ID`);

ALTER TABLE `manufacturer`
  ADD PRIMARY KEY (`id_manufacturer`),
  ADD KEY `id_manufacturer` (`id_manufacturer`);

ALTER TABLE `manufacturer_lang`
  ADD PRIMARY KEY (`id_manufacturer`,`id_lang`);

ALTER TABLE `memcached_servers`
  ADD PRIMARY KEY (`id_memcached_server`);

ALTER TABLE `message`
  ADD PRIMARY KEY (`id_message`),
  ADD KEY `message_order` (`id_order`),
  ADD KEY `id_cart` (`id_cart`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_employee` (`id_employee`);

ALTER TABLE `message_readed`
  ADD PRIMARY KEY (`id_message`,`id_employee`);

ALTER TABLE `meta`
  ADD PRIMARY KEY (`id_meta`),
  ADD KEY `meta_name` (`page`);

ALTER TABLE `meta_lang`
  ADD PRIMARY KEY (`id_meta`,`id_lang`);

ALTER TABLE `module`
  ADD PRIMARY KEY (`id_module`),
  ADD KEY `name` (`name`);

ALTER TABLE `module_country`
  ADD PRIMARY KEY (`id_module`,`id_country`);

ALTER TABLE `module_currency`
  ADD PRIMARY KEY (`id_module`,`id_currency`),
  ADD KEY `id_module` (`id_module`);

ALTER TABLE `module_group`
  ADD PRIMARY KEY (`id_module`,`id_group`);

ALTER TABLE `movimenti_speciali`
  ADD PRIMARY KEY (`id_movement`);

ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `noleggio`
  ADD PRIMARY KEY (`id_noleggio`);

ALTER TABLE `note_attivita`
  ADD PRIMARY KEY (`id_note`);

ALTER TABLE `note_di_credito`
  ADD PRIMARY KEY (`id_riga`),
  ADD KEY `id_customer` (`id_customer`,`codice_cliente`,`id_fattura`,`tipo`,`data_fattura`),
  ADD KEY `tipo_id_fattura` (`tipo`,`id_fattura`),
  ADD KEY `id_fattura` (`id_fattura`),
  ADD KEY `rif_ordine` (`rif_ordine`),
  ADD KEY `id_riga` (`id_riga`),
  ADD KEY `rif_vs_ordine` (`rif_vs_ordine`);

ALTER TABLE `operating_system`
  ADD PRIMARY KEY (`id_operating_system`);

ALTER TABLE `orders`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_cart` (`id_cart`),
  ADD KEY `invoice_number` (`invoice_number`),
  ADD KEY `id_carrier` (`id_carrier`),
  ADD KEY `id_lang` (`id_lang`),
  ADD KEY `id_currency` (`id_currency`),
  ADD KEY `id_address_delivery` (`id_address_delivery`),
  ADD KEY `id_address_invoice` (`id_address_invoice`),
  ADD KEY `date_add` (`date_add`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `date_upd` (`date_upd`);

ALTER TABLE `orders_controllo_differiti`
  ADD PRIMARY KEY (`id_order`);

ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id_order_detail`),
  ADD KEY `order_detail_order` (`id_order`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_attribute_id` (`product_attribute_id`),
  ADD KEY `id_order_id_order_detail` (`id_order`,`id_order_detail`),
  ADD KEY `category_id` (`category_id`,`manufacturer_id`,`product_price`,`product_reference`,`product_supplier_reference`),
  ADD KEY `product_id_quantity` (`product_id`,`product_quantity`),
  ADD KEY `product_reference` (`product_reference`);

ALTER TABLE `order_discount`
  ADD PRIMARY KEY (`id_order_discount`),
  ADD KEY `order_discount_order` (`id_order`),
  ADD KEY `id_discount` (`id_discount`);

ALTER TABLE `order_history`
  ADD PRIMARY KEY (`id_order_history`),
  ADD KEY `order_history_order` (`id_order`),
  ADD KEY `id_employee` (`id_employee`),
  ADD KEY `id_order_state` (`id_order_state`),
  ADD KEY `id_order_history` (`id_order_history`),
  ADD KEY `id_order` (`id_order`);

ALTER TABLE `order_message`
  ADD PRIMARY KEY (`id_order_message`);

ALTER TABLE `order_message_lang`
  ADD PRIMARY KEY (`id_order_message`,`id_lang`);

ALTER TABLE `order_return`
  ADD PRIMARY KEY (`id_order_return`),
  ADD KEY `order_return_customer` (`id_customer`),
  ADD KEY `id_order` (`id_order`);

ALTER TABLE `order_return_detail`
  ADD PRIMARY KEY (`id_order_return`,`id_order_detail`,`id_customization`);

ALTER TABLE `order_return_state`
  ADD PRIMARY KEY (`id_order_return_state`);

ALTER TABLE `order_return_state_lang`
  ADD PRIMARY KEY (`id_order_return_state`,`id_lang`);

ALTER TABLE `order_slip`
  ADD PRIMARY KEY (`id_order_slip`),
  ADD KEY `order_slip_customer` (`id_customer`),
  ADD KEY `id_order` (`id_order`);

ALTER TABLE `order_slip_detail`
  ADD PRIMARY KEY (`id_order_slip`,`id_order_detail`);

ALTER TABLE `order_state`
  ADD PRIMARY KEY (`id_order_state`);

ALTER TABLE `order_state_lang`
  ADD PRIMARY KEY (`id_order_state`,`id_lang`);

ALTER TABLE `ordini_acquisto`
  ADD PRIMARY KEY (`id_riga`);

ALTER TABLE `page`
  ADD PRIMARY KEY (`id_page`),
  ADD KEY `id_page_type` (`id_page_type`),
  ADD KEY `id_object` (`id_object`);

ALTER TABLE `pagenotfound`
  ADD PRIMARY KEY (`id_pagenotfound`);

ALTER TABLE `page_type`
  ADD PRIMARY KEY (`id_page_type`),
  ADD KEY `name` (`name`);

ALTER TABLE `page_viewed`
  ADD PRIMARY KEY (`id_page`,`id_date_range`);

ALTER TABLE `payment_cc`
  ADD PRIMARY KEY (`id_payment_cc`),
  ADD KEY `id_order` (`id_order`);

ALTER TABLE `paypal_order`
  ADD PRIMARY KEY (`id_order`);

ALTER TABLE `persone`
  ADD PRIMARY KEY (`id_persona`),
  ADD KEY `id_persona` (`id_persona`,`email`);

ALTER TABLE `precompilato`
  ADD PRIMARY KEY (`id_precompilato`);

ALTER TABLE `product`
  ADD PRIMARY KEY (`id_product`),
  ADD KEY `product_supplier` (`id_supplier`),
  ADD KEY `product_manufacturer` (`id_manufacturer`),
  ADD KEY `id_category_default` (`id_category_default`),
  ADD KEY `id_color_default` (`id_color_default`),
  ADD KEY `date_add` (`date_add`),
  ADD KEY `price` (`price`),
  ADD KEY `price_id_product` (`id_product`,`price`),
  ADD KEY `intracom_quantity_quantity` (`intracom_quantity`,`quantity`),
  ADD KEY `id_product_price` (`id_product`,`price`),
  ADD KEY `reference` (`reference`),
  ADD KEY `supplier_reference` (`supplier_reference`),
  ADD KEY `upc` (`upc`);

ALTER TABLE `products_to_categories`
  ADD PRIMARY KEY (`products_id`,`categories_id`),
  ADD KEY `ptc_catidx` (`categories_id`);

ALTER TABLE `product_attachment`
  ADD PRIMARY KEY (`id_product`,`id_attachment`);

ALTER TABLE `product_attribute`
  ADD PRIMARY KEY (`id_product_attribute`),
  ADD KEY `product_attribute_product` (`id_product`),
  ADD KEY `reference` (`reference`),
  ADD KEY `supplier_reference` (`supplier_reference`),
  ADD KEY `product_default` (`id_product`,`default_on`),
  ADD KEY `id_product_id_product_attribute` (`id_product_attribute`,`id_product`);

ALTER TABLE `product_attribute_combination`
  ADD PRIMARY KEY (`id_attribute`,`id_product_attribute`),
  ADD KEY `id_product_attribute` (`id_product_attribute`);

ALTER TABLE `product_attribute_image`
  ADD PRIMARY KEY (`id_product_attribute`,`id_image`),
  ADD KEY `id_image` (`id_image`);

ALTER TABLE `product_comment`
  ADD PRIMARY KEY (`id_product_comment`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_customer` (`id_customer`),
  ADD KEY `id_guest` (`id_guest`);

ALTER TABLE `product_comment_criterion`
  ADD PRIMARY KEY (`id_product_comment_criterion`);

ALTER TABLE `product_comment_criterion_category`
  ADD PRIMARY KEY (`id_product_comment_criterion`,`id_category`),
  ADD KEY `id_category` (`id_category`);

ALTER TABLE `product_comment_criterion_lang`
  ADD PRIMARY KEY (`id_product_comment_criterion`,`id_lang`);

ALTER TABLE `product_comment_criterion_product`
  ADD PRIMARY KEY (`id_product`,`id_product_comment_criterion`),
  ADD KEY `id_product_comment_criterion` (`id_product_comment_criterion`);

ALTER TABLE `product_comment_grade`
  ADD PRIMARY KEY (`id_product_comment`,`id_product_comment_criterion`),
  ADD KEY `id_product_comment_criterion` (`id_product_comment_criterion`);

ALTER TABLE `product_country_tax`
  ADD PRIMARY KEY (`id_product`,`id_country`);

ALTER TABLE `product_download`
  ADD PRIMARY KEY (`id_product_download`),
  ADD KEY `product_active` (`id_product`,`active`);

ALTER TABLE `product_esolver`
  ADD PRIMARY KEY (`id_product`);

ALTER TABLE `product_group_reduction_cache`
  ADD PRIMARY KEY (`id_product`,`id_group`);

ALTER TABLE `product_lang`
  ADD PRIMARY KEY (`id_product`,`id_lang`),
  ADD KEY `id_lang` (`id_lang`),
  ADD KEY `name` (`name`);
ALTER TABLE `product_lang` ADD FULLTEXT KEY `name_2` (`name`);

ALTER TABLE `product_listini`
  ADD PRIMARY KEY (`id_product`,`id_supplier`);

ALTER TABLE `product_sale`
  ADD PRIMARY KEY (`id_product`);

ALTER TABLE `product_tag`
  ADD PRIMARY KEY (`id_product`,`id_tag`),
  ADD KEY `id_tag` (`id_tag`);

ALTER TABLE `product_video`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `profile`
  ADD PRIMARY KEY (`id_profile`);

ALTER TABLE `profile_lang`
  ADD PRIMARY KEY (`id_profile`,`id_lang`);

ALTER TABLE `province`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `quick_access`
  ADD PRIMARY KEY (`id_quick_access`);

ALTER TABLE `quick_access_lang`
  ADD PRIMARY KEY (`id_quick_access`,`id_lang`);

ALTER TABLE `range_price`
  ADD PRIMARY KEY (`id_range_price`),
  ADD UNIQUE KEY `id_carrier` (`id_carrier`,`delimiter1`,`delimiter2`);

ALTER TABLE `range_weight`
  ADD PRIMARY KEY (`id_range_weight`),
  ADD UNIQUE KEY `id_carrier` (`id_carrier`,`delimiter1`,`delimiter2`);

ALTER TABLE `referrer`
  ADD PRIMARY KEY (`id_referrer`);

ALTER TABLE `referrer_cache`
  ADD PRIMARY KEY (`id_connections_source`,`id_referrer`);

ALTER TABLE `required_field`
  ADD PRIMARY KEY (`id_required_field`),
  ADD KEY `object_name` (`object_name`);

ALTER TABLE `rma`
  ADD PRIMARY KEY (`id_customer_thread`);

ALTER TABLE `scadenze`
  ADD PRIMARY KEY (`id_scadenza`);

ALTER TABLE `scene`
  ADD PRIMARY KEY (`id_scene`);

ALTER TABLE `scene_category`
  ADD PRIMARY KEY (`id_scene`,`id_category`);

ALTER TABLE `scene_lang`
  ADD PRIMARY KEY (`id_scene`,`id_lang`);

ALTER TABLE `scene_products`
  ADD PRIMARY KEY (`id_scene`,`id_product`,`x_axis`,`y_axis`);

ALTER TABLE `search_engine`
  ADD PRIMARY KEY (`id_search_engine`);

ALTER TABLE `search_index`
  ADD PRIMARY KEY (`id_word`,`id_product`);

ALTER TABLE `search_word`
  ADD PRIMARY KEY (`id_word`),
  ADD UNIQUE KEY `id_lang` (`id_lang`,`word`);

ALTER TABLE `sekeyword`
  ADD PRIMARY KEY (`id_sekeyword`);

ALTER TABLE `sendin_newsletter`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `specific_price`
  ADD PRIMARY KEY (`id_specific_price`),
  ADD KEY `id_product` (`id_product`,`id_shop`,`id_currency`,`id_country`,`id_group`,`from_quantity`,`from`,`to`);

ALTER TABLE `specific_price_priority`
  ADD PRIMARY KEY (`id_specific_price_priority`,`id_product`),
  ADD UNIQUE KEY `id_product` (`id_product`);

ALTER TABLE `specific_price_wholesale`
  ADD PRIMARY KEY (`id_specific_price`);

ALTER TABLE `state`
  ADD PRIMARY KEY (`id_state`),
  ADD KEY `id_country` (`id_country`),
  ADD KEY `id_zone` (`id_zone`);

ALTER TABLE `statssearch`
  ADD PRIMARY KEY (`id_statssearch`);

ALTER TABLE `stock_mvt`
  ADD PRIMARY KEY (`id_stock_mvt`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_product_attribute` (`id_product_attribute`),
  ADD KEY `id_stock_mvt_reason` (`id_stock_mvt_reason`);

ALTER TABLE `stock_mvt_reason`
  ADD PRIMARY KEY (`id_stock_mvt_reason`);

ALTER TABLE `stock_mvt_reason_lang`
  ADD PRIMARY KEY (`id_stock_mvt_reason`,`id_lang`);

ALTER TABLE `store`
  ADD PRIMARY KEY (`id_store`);

ALTER TABLE `storico_attivita`
  ADD PRIMARY KEY (`id_storico`),
  ADD KEY `id_attivita` (`id_attivita`,`tipo_attivita`,`tipo_num`,`data_attivita`),
  ADD KEY `id_attivita_data_attivita` (`id_attivita`,`data_attivita`),
  ADD KEY `data_attivita` (`data_attivita`),
  ADD KEY `dataattivita` (`data_attivita`),
  ADD KEY `statt` (`id_attivita`,`data_attivita`);

ALTER TABLE `subdomain`
  ADD PRIMARY KEY (`id_subdomain`);

ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

ALTER TABLE `supplier_lang`
  ADD PRIMARY KEY (`id_supplier`,`id_lang`);

ALTER TABLE `tab`
  ADD PRIMARY KEY (`id_tab`),
  ADD KEY `class_name` (`class_name`),
  ADD KEY `id_parent` (`id_parent`);

ALTER TABLE `tab_lang`
  ADD PRIMARY KEY (`id_tab`,`id_lang`);

ALTER TABLE `tag`
  ADD PRIMARY KEY (`id_tag`),
  ADD KEY `tag_name` (`name`),
  ADD KEY `id_lang` (`id_lang`);

ALTER TABLE `tax`
  ADD PRIMARY KEY (`id_tax`);

ALTER TABLE `tax_lang`
  ADD PRIMARY KEY (`id_tax`,`id_lang`);

ALTER TABLE `tax_rule`
  ADD PRIMARY KEY (`id_tax_rule`),
  ADD UNIQUE KEY `tax_rule` (`id_tax_rules_group`,`id_country`,`id_state`,`id_county`),
  ADD KEY `id_tax_rules_group` (`id_tax_rules_group`),
  ADD KEY `id_tax` (`id_tax`);

ALTER TABLE `tax_rules_group`
  ADD PRIMARY KEY (`id_tax_rules_group`);

ALTER TABLE `telefonate`
  ADD PRIMARY KEY (`id_telefonata`);

ALTER TABLE `ticket_promemoria`
  ADD PRIMARY KEY (`msg`,`tipo_att`,`ora`);

ALTER TABLE `timezone`
  ADD PRIMARY KEY (`id_timezone`);

ALTER TABLE `trgt`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `valutazione`
  ADD PRIMARY KEY (`id_modulo`);

ALTER TABLE `webservice_account`
  ADD PRIMARY KEY (`id_webservice_account`),
  ADD KEY `key` (`key`);

ALTER TABLE `webservice_permission`
  ADD PRIMARY KEY (`id_webservice_permission`),
  ADD UNIQUE KEY `resource_2` (`resource`,`method`,`id_webservice_account`),
  ADD KEY `resource` (`resource`),
  ADD KEY `method` (`method`),
  ADD KEY `id_webservice_account` (`id_webservice_account`);

ALTER TABLE `web_browser`
  ADD PRIMARY KEY (`id_web_browser`);

ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id_wishlist`);

ALTER TABLE `wishlist_product`
  ADD PRIMARY KEY (`id_wishlist_product`);

ALTER TABLE `zone`
  ADD PRIMARY KEY (`id_zone`);


ALTER TABLE `action_message`
  MODIFY `id_action_message` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `action_message_employee`
  MODIFY `id_action_message_employee` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `action_thread`
  MODIFY `id_action` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `action_thread_employee`
  MODIFY `id_action` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `address`
  MODIFY `id_address` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `address_book`
  MODIFY `address_book_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `agente`
  MODIFY `id_agente` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `alias`
  MODIFY `id_alias` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `attachment`
  MODIFY `id_attachment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `attachment_lang`
  MODIFY `id_attachment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `attribute`
  MODIFY `id_attribute` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `attribute_group`
  MODIFY `id_attribute_group` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `attribute_impact`
  MODIFY `id_attribute_impact` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `bdl`
  MODIFY `id_bdl` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `bundle`
  MODIFY `id_bundle` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `bundle_image`
  MODIFY `id_image` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `cap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `carrelli_creati`
  MODIFY `id_cart` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `carrier`
  MODIFY `id_carrier` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `cart`
  MODIFY `id_cart` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `cart_note`
  MODIFY `id_note` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `cart_revisioni`
  MODIFY `id_revisione` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `category`
  MODIFY `id_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `chatban`
  MODIFY `banid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chatconfig`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chatgroup`
  MODIFY `groupid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chatmessage`
  MODIFY `messageid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chatoperator`
  MODIFY `operatorid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chatresponses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chatthread`
  MODIFY `threadid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chat_availableupdate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chat_ban`
  MODIFY `banid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chat_cannedmessage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chat_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chat_locale`
  MODIFY `localeid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chat_mailtemplate`
  MODIFY `templateid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chat_message`
  MODIFY `messageid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chat_operator2`
  MODIFY `operatorid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chat_operatorstatistics`
  MODIFY `statid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chat_opgroup`
  MODIFY `groupid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chat_plugin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chat_requestbuffer`
  MODIFY `requestid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chat_requestcallback`
  MODIFY `callbackid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chat_sitevisitor`
  MODIFY `visitorid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chat_thread2`
  MODIFY `threadid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chat_threadstatistics`
  MODIFY `statid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chat_translation`
  MODIFY `translationid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chat_visitedpage`
  MODIFY `pageid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `chat_visitedpagestatistics`
  MODIFY `pageid` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `cms`
  MODIFY `id_cms` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `cms_block`
  MODIFY `id_cms_block` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `cms_block_page`
  MODIFY `id_cms_block_page` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `cms_category`
  MODIFY `id_cms_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `compare`
  MODIFY `id_compare` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `comuni`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `configuration`
  MODIFY `id_configuration` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `connections`
  MODIFY `id_connections` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `connections_cache`
  MODIFY `id_connections` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `connections_source`
  MODIFY `id_connections_source` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `contact`
  MODIFY `id_contact` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `country`
  MODIFY `id_country` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `county`
  MODIFY `id_county` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `currency`
  MODIFY `id_currency` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `customer`
  MODIFY `id_customer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `customer_message`
  MODIFY `id_customer_message` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `customer_note`
  MODIFY `id_note` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `customer_thread`
  MODIFY `id_customer_thread` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `customization`
  MODIFY `id_customization` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `customization_field`
  MODIFY `id_customization_field` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `date_range`
  MODIFY `id_date_range` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `delivery`
  MODIFY `id_delivery` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `discount`
  MODIFY `id_discount` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `discount_type`
  MODIFY `id_discount_type` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `editorial`
  MODIFY `id_editorial` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `employee`
  MODIFY `id_employee` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `evento_yeastar_21102014`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `ezcloud`
  MODIFY `id_ezcloud` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `ezcloud_valutazione`
  MODIFY `id_modulo` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `ezdial_licenze`
  MODIFY `id_licenza` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `ezdial_mac_address`
  MODIFY `id_mac_address` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `fattura`
  MODIFY `id_riga` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `fatture_acquisto`
  MODIFY `id_riga` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `feature`
  MODIFY `id_feature` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `feature_group`
  MODIFY `id_group` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `feature_value`
  MODIFY `id_feature_value` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `fixed_ck`
  MODIFY `id_ck` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `form_prevendita_message`
  MODIFY `id_message` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `form_prevendita_thread`
  MODIFY `id_thread` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `gestione_link`
  MODIFY `id_regola` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `group`
  MODIFY `id_group` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `group_reduction`
  MODIFY `id_group_reduction` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `guest`
  MODIFY `id_guest` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `help_access`
  MODIFY `id_help_access` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `hook`
  MODIFY `id_hook` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `hook_module_exceptions`
  MODIFY `id_hook_module_exceptions` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `image`
  MODIFY `id_image` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `image_type`
  MODIFY `id_image_type` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `import_match`
  MODIFY `id_import_match` int(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `lang`
  MODIFY `id_lang` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `layered_category`
  MODIFY `id_layered_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `layered_filter`
  MODIFY `id_layered_filter` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `layered_friendly_url`
  MODIFY `id_layered_friendly_url` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `location_coords`
  MODIFY `id_location_coords` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `log`
  MODIFY `id_log` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `loyalty`
  MODIFY `id_loyalty` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `loyalty_history`
  MODIFY `id_loyalty_history` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `loyalty_state`
  MODIFY `id_loyalty_state` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `loyalty_state_lang`
  MODIFY `id_loyalty_state` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `mailing_import`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT;

ALTER TABLE `mailing_track`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT;

ALTER TABLE `manufacturer`
  MODIFY `id_manufacturer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `memcached_servers`
  MODIFY `id_memcached_server` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `message`
  MODIFY `id_message` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `meta`
  MODIFY `id_meta` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `module`
  MODIFY `id_module` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `movimenti_speciali`
  MODIFY `id_movement` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `newsletter`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

ALTER TABLE `noleggio`
  MODIFY `id_noleggio` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `note_attivita`
  MODIFY `id_note` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `note_di_credito`
  MODIFY `id_riga` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `operating_system`
  MODIFY `id_operating_system` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `orders`
  MODIFY `id_order` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `order_detail`
  MODIFY `id_order_detail` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `order_discount`
  MODIFY `id_order_discount` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `order_history`
  MODIFY `id_order_history` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `order_message`
  MODIFY `id_order_message` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `order_return`
  MODIFY `id_order_return` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `order_return_state`
  MODIFY `id_order_return_state` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `order_slip`
  MODIFY `id_order_slip` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `order_state`
  MODIFY `id_order_state` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `ordini_acquisto`
  MODIFY `id_riga` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `page`
  MODIFY `id_page` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `pagenotfound`
  MODIFY `id_pagenotfound` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `page_type`
  MODIFY `id_page_type` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `payment_cc`
  MODIFY `id_payment_cc` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `persone`
  MODIFY `id_persona` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `precompilato`
  MODIFY `id_precompilato` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `product`
  MODIFY `id_product` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `product_attribute`
  MODIFY `id_product_attribute` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `product_comment`
  MODIFY `id_product_comment` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `product_comment_criterion`
  MODIFY `id_product_comment_criterion` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `product_download`
  MODIFY `id_product_download` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `product_video`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `profile`
  MODIFY `id_profile` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `province`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `quick_access`
  MODIFY `id_quick_access` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `range_price`
  MODIFY `id_range_price` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `range_weight`
  MODIFY `id_range_weight` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `referrer`
  MODIFY `id_referrer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `required_field`
  MODIFY `id_required_field` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `scadenze`
  MODIFY `id_scadenza` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `scene`
  MODIFY `id_scene` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `search_engine`
  MODIFY `id_search_engine` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `search_word`
  MODIFY `id_word` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `sekeyword`
  MODIFY `id_sekeyword` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `sendin_newsletter`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

ALTER TABLE `specific_price`
  MODIFY `id_specific_price` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `specific_price_priority`
  MODIFY `id_specific_price_priority` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `specific_price_wholesale`
  MODIFY `id_specific_price` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `state`
  MODIFY `id_state` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `statssearch`
  MODIFY `id_statssearch` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `stock_mvt`
  MODIFY `id_stock_mvt` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `stock_mvt_reason`
  MODIFY `id_stock_mvt_reason` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `store`
  MODIFY `id_store` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `storico_attivita`
  MODIFY `id_storico` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `subdomain`
  MODIFY `id_subdomain` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `supplier`
  MODIFY `id_supplier` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `tab`
  MODIFY `id_tab` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `tag`
  MODIFY `id_tag` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `tax`
  MODIFY `id_tax` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `tax_rule`
  MODIFY `id_tax_rule` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `tax_rules_group`
  MODIFY `id_tax_rules_group` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `telefonate`
  MODIFY `id_telefonata` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `timezone`
  MODIFY `id_timezone` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `trgt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `valutazione`
  MODIFY `id_modulo` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `webservice_account`
  MODIFY `id_webservice_account` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `webservice_permission`
  MODIFY `id_webservice_permission` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `web_browser`
  MODIFY `id_web_browser` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `wishlist`
  MODIFY `id_wishlist` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `wishlist_product`
  MODIFY `id_wishlist_product` int(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `zone`
  MODIFY `id_zone` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
