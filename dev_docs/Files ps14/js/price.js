/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14009 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

function getTax()
{
	if (noTax)
		return 0;
	var selectedTax = document.getElementById('id_tax_rules_group');
	var taxId = selectedTax.options[selectedTax.selectedIndex].value;
	return taxesArray[taxId];
}

function getEcotaxTaxIncluded()
{
	return ($('#ecotax').length && $('#ecotax').val()  != '') ? parseFloat($('#ecotax').val()) : 0;
}

function getEcotaxTaxExcluded()
{
	return getEcotaxTaxIncluded() / (1 + ecotaxTaxRate);
}

function formatPrice(price)
{
	var fixedToSix = (Math.round(price * 1000000) / 1000000);
	return (Math.round(fixedToSix) == fixedToSix + 0.000001 ? fixedToSix + 0.000001 : fixedToSix);
}

function calcMarginalita()
{
	var tax = getTax();
	var priceListino = parseFloat(document.getElementById('wholesale_price').value.replace(/,/g, '.'));
	
	var priceTE = parseFloat(document.getElementById('priceTE').value.replace(/,/g, '.'));
	
	if(priceListino == "" || isNaN(priceListino))
		priceListino = 0;
		
	if(priceTE == "" || isNaN(priceTE))
		priceTE = 0;
		
	var newPrice = (((priceTE - priceListino)*100) / priceTE);
	
	document.getElementById('marginalita').value = (isNaN(newPrice) == true || newPrice < 0) ? '' :
		ps_round(newPrice, 2);

	calcReduction();
	document.getElementById('marginalita').value = parseFloat(document.getElementById('marginalita').value) + getEcotaxTaxIncluded();

}

function aggiornaScontisticaAmazon()
{
	
	var priceTE = parseFloat(document.getElementById('priceTE').value.replace(/,/g, '.'));
	
	var migliorPrezzo = parseFloat(document.getElementById('miglior_prezzo_appoggio').value.replace(/,/g, '.'));
	
	if(!migliorPrezzo || migliorPrezzo == 0)
		migliorPrezzo = priceTE;
	
	if(priceTE < migliorPrezzo)
		document.getElementById('miglior_prezzo').value = priceTE;
	else
		document.getElementById('miglior_prezzo').value = migliorPrezzo;
	
	//document.getElementById('miglior_prezzo').value = priceTE;
	
	calcPrezzoSconto('sconto_amazon_it', 'prezzo_amazon_it', 'miglior_prezzo');
	calcPrezzoSconto('sconto_amazon_fr', 'prezzo_amazon_fr', 'miglior_prezzo');
	calcPrezzoSconto('sconto_amazon_es', 'prezzo_amazon_es', 'miglior_prezzo');
	calcPrezzoSconto('sconto_amazon_de', 'prezzo_amazon_de', 'miglior_prezzo');
	calcPrezzoSconto('sconto_amazon_uk', 'prezzo_amazon_uk', 'miglior_prezzo');
	
	calcMarginalitaSconto('sconto_amazon_it', 'margin_amazon_it', 'miglior_prezzo');
	calcMarginalitaSconto('sconto_amazon_fr', 'margin_amazon_fr', 'miglior_prezzo');
	calcMarginalitaSconto('sconto_amazon_es', 'margin_amazon_es', 'miglior_prezzo');
	calcMarginalitaSconto('sconto_amazon_de', 'margin_amazon_de', 'miglior_prezzo');
	calcMarginalitaSconto('sconto_amazon_uk', 'margin_amazon_uk', 'miglior_prezzo');
	
}

function calcMarginalitaSconto(sc_riv, margin_riv, list_type)
{
	var tax = getTax();
	var priceListino = parseFloat(document.getElementById('wholesale_price').value.replace(/,/g, '.'));
	
	var migliorPrezzo = parseFloat(document.getElementById('miglior_prezzo_acquisto').value.replace(/,/g, '.'));
	
	
	/*if(priceListino == "" || !Number(priceListino))
		priceListino = 0;
	
	if(migliorPrezzo != 0 && migliorPrezzo < priceListino)
		priceListino = migliorPrezzo;
	*/

	var sconto = parseFloat(document.getElementById(sc_riv).value.replace(/,/g, '.'));
	
	if(sconto == "" || !Number(sconto))
		sconto = 0;
		
	var priceTE = parseFloat(document.getElementById(list_type).value.replace(/,/g, '.'));
	
	if(priceTE == "" || !Number(priceTE))
		priceTE = 0;
		
	priceTE = priceTE - ((priceTE * sconto) / 100);
	var newPrice = (((priceTE - priceListino)*100) / priceTE);
	
	document.getElementById(margin_riv).value = (!Number(newPrice) == true || newPrice < 0) ? '' :
		ps_round(newPrice, 2);

	calcReduction();
	document.getElementById(margin_riv).value = parseFloat(document.getElementById(margin_riv).value) + getEcotaxTaxIncluded();

}

function calcMarginalitaSconto2(sc_riv, margin_riv, list_type)
{
	var tax = getTax();
	var priceListino = parseFloat(document.getElementById('wholesale_price').value.replace(/,/g, '.'));
	
	var migliorPrezzo = parseFloat(document.getElementById('miglior_prezzo_acquisto').value.replace(/,/g, '.'));
	
	
	if(priceListino == "" || !Number(priceListino))
		priceListino = 0;
	
	if(migliorPrezzo != 0 && migliorPrezzo < priceListino)
		priceListino = migliorPrezzo;
	

	var sconto = parseFloat(document.getElementById(sc_riv).value.replace(/,/g, '.'));
	
	if(sconto == "" || !Number(sconto))
		sconto = 0;
		
	var priceTE = parseFloat(document.getElementById(list_type).value.replace(/,/g, '.'));
	
	if(priceTE == "" || !Number(priceTE))
		priceTE = 0;
		
	priceTE = priceTE - ((priceTE * sconto) / 100);
	var newPrice = (((priceTE - priceListino)*100) / priceTE);
	
	document.getElementById(margin_riv).value = (!Number(newPrice) == true || newPrice < 0) ? '' :
		ps_round(newPrice, 2);

	calcReduction();
	document.getElementById(margin_riv).value = parseFloat(document.getElementById(margin_riv).value) + getEcotaxTaxIncluded();

}

function calcPrezzoSconto(sc_riv, prezzo_riv, list_type)
{
	var tax = getTax();
	var sconto = parseFloat(document.getElementById(sc_riv).value.replace(/,/g, '.'));
	
	if(sconto == "" || !Number(sconto))
		sconto = 0;
		
	var priceTE = parseFloat(document.getElementById(list_type).value.replace(/,/g, '.'));
	
	if(priceTE == "" || !Number(priceTE))
		priceTE = 0;
		
	var newPrice = priceTE = priceTE - ((priceTE * sconto) / 100);
	
	document.getElementById(prezzo_riv).value = (!Number(newPrice) == true ) ? 0 :
		ps_round(newPrice, 2);

	calcReduction();
	document.getElementById(prezzo_riv).value = parseFloat(document.getElementById(prezzo_riv).value) + getEcotaxTaxIncluded();

}

function calcPrezzoFromMargine(acquisto, margine, dest)
{
	var acquisto = parseFloat(document.getElementById(acquisto).value.replace(/,/g, '.'));
	var margine = parseFloat(document.getElementById(margine).value.replace(/,/g, '.'));
	
	var finale = (acquisto * 100) / (100 - margine);
	
	document.getElementById(dest).value = (!Number(finale) == true ) ? 0 :
		ps_round(finale, 2);

}

function calcPrezzoScontoHTML(sconto, prezzo, visualizzazione)
{
	var sconto = parseFloat(document.getElementById(sconto).value.replace(/,/g, '.'));
	
	if(sconto == "" || isNaN(sconto))
		sconto = 0;
		
	var prezzo = parseFloat(document.getElementById(prezzo).value.replace(/,/g, '.'));
	
	if(prezzo == "" || isNaN(prezzo))
		prezzo = 0;
		
	var nuovoPrezzo = prezzo - ((prezzo * sconto) / 100);

	document.getElementById(visualizzazione).innerHTML = nuovoPrezzo;

}

function calcSconto(partenza,fine,sconto)
{

	var partenza = parseFloat(document.getElementById(partenza).value.replace(/,/g, '.'));
	
	if(partenza == "" || !Number(partenza))
		partenza = 0;
		
	var fine = parseFloat(document.getElementById(fine).value.replace(/,/g, '.'));
	
	if(fine == "" || !Number(fine))
		fine = 0;
		
	var newPrice = ((partenza - fine)*100)/partenza;
	
	
	document.getElementById(sconto).value = (!Number(newPrice) == true ) ? 0 :
		ps_round(newPrice, 2);

	document.getElementById(sconto).innerHTML = parseFloat(document.getElementById(sconto).value);
}

function calcScontoAcquisto1()
{

	var acq1 = parseFloat(document.getElementById('sconto_acquisto_1').value.replace(/,/g, '.'));
	
	if(acq1 == "" || isNaN(acq1))
		acq1 = 0;
		
	var listino = parseFloat(document.getElementById('listino').value.replace(/,/g, '.'));
	
	if(listino == "" || isNaN(listino))
		listino = 0;
		
	var newPrice = (listino * (100 - acq1) / 100);
	
	document.getElementById('wholesale_price').value = (isNaN(newPrice) == true || newPrice < 0) ? '' :
		ps_round(newPrice, 2);

	document.getElementById('wholesale_price').innerHTML = parseFloat(document.getElementById('wholesale_price').value);
}

function calcScontoAcquisto2()
{

	var acq1 = parseFloat(document.getElementById('sconto_acquisto_1').value.replace(/,/g, '.'));
	
	if(acq1 == "" || isNaN(acq1))
		acq1 = 0;
		
	var acq2 = parseFloat(document.getElementById('sconto_acquisto_2').value.replace(/,/g, '.'));
	
	if(acq2 == "" || isNaN(acq2))
		acq2 = 0;
		
	var listino = parseFloat(document.getElementById('listino').value.replace(/,/g, '.'));
	
	if(listino == "" || isNaN(listino))
		listino = 0;
		
	var newPrice = (listino * (100 - acq1) / 100) * ((100 - acq2)/100);
	
	document.getElementById('wholesale_price').value = (isNaN(newPrice) == true || newPrice < 0) ? '' :
		ps_round(newPrice, 2);

	document.getElementById('wholesale_price').innerHTML = parseFloat(document.getElementById('wholesale_price').value);
}

function calcScontoAcquisto3()
{

	var acq1 = parseFloat(document.getElementById('sconto_acquisto_1').value.replace(/,/g, '.'));
	var acq2 = parseFloat(document.getElementById('sconto_acquisto_2').value.replace(/,/g, '.'));
	var acq3 = parseFloat(document.getElementById('sconto_acquisto_3').value.replace(/,/g, '.'));
	
	if(acq1 == "" || isNaN(acq1))
		acq1 = 0;
		
	if(acq2 == "" || isNaN(acq2))
		acq2 = 0;
		
	if(acq3 == "" || isNaN(acq3))
		acq3 = 0;
		
	var listino = parseFloat(document.getElementById('listino').value.replace(/,/g, '.'));
	
	if(listino == "" || isNaN(listino))
		listino = 0;
		
	var newPrice = (listino * (100 - acq1) / 100) * ((100 - acq2)/100) * ((100 - acq3)/100);
	
	document.getElementById('wholesale_price_esolver').value = (isNaN(newPrice) == true || newPrice < 0) ? '' :
		ps_round(newPrice, 2);

	document.getElementById('wholesale_price_esolver').innerHTML = parseFloat(document.getElementById('wholesale_price_esolver').value);
}

function calcScontoAcquisto3_esolver()
{

	var acq1 = parseFloat(document.getElementById('sconto_acquisto_1_esolver').value.replace(/,/g, '.'));
	var acq2 = parseFloat(document.getElementById('sconto_acquisto_2_esolver').value.replace(/,/g, '.'));
	var acq3 = parseFloat(document.getElementById('sconto_acquisto_3_esolver').value.replace(/,/g, '.'));
	
	if(acq1 == "" || isNaN(acq1))
		acq1 = 0;
		
	if(acq2 == "" || isNaN(acq2))
		acq2 = 0;
		
	if(acq3 == "" || isNaN(acq3))
		acq3 = 0;
		
	var listino = parseFloat(document.getElementById('listino_esolver').value.replace(/,/g, '.'));
	
	if(listino == "" || isNaN(listino))
		listino = 0;
		
	var newPrice = (listino * (100 - acq1) / 100) * ((100 - acq2)/100) * ((100 - acq3)/100);
	
	document.getElementById('wholesale_price_esolver').value = (isNaN(newPrice) == true || newPrice < 0) ? '' :
		ps_round(newPrice, 2);

	document.getElementById('wholesale_price_esolver').innerHTML = parseFloat(document.getElementById('wholesale_price_esolver').value);
}

function calcRebate()
{
	var acq1 = parseFloat(document.getElementById('rebate_1').value.replace(/,/g, '.'));
	var acq2 = parseFloat(document.getElementById('rebate_2').value.replace(/,/g, '.'));
	var acq3 = parseFloat(document.getElementById('rebate_3').value.replace(/,/g, '.'));
	
	if(acq1 == "" || isNaN(acq1))
		acq1 = 0;
		
	if(acq2 == "" || isNaN(acq2))
		acq2 = 0;
		
	if(acq3 == "" || isNaN(acq3))
		acq3 = 0;
		
	var listino = parseFloat(document.getElementById('wholesale_price_esolver').value.replace(/,/g, '.'));
	
	if(listino == "" || isNaN(listino))
		listino = 0;
		
	var newPrice = (listino * (100 - acq1) / 100) * ((100 - acq2)/100) * ((100 - acq3)/100);
	
	document.getElementById('wholesale_price').value = (isNaN(newPrice) == true || newPrice < 0) ? '' :
		ps_round(newPrice, 2);

	document.getElementById('wholesale_price').innerHTML = parseFloat(document.getElementById('wholesale_price').value);
}

function calcScontiAcquisto(acq1,acq2,acq3,listino,acquisto)
{
	var acq1 = parseFloat(document.getElementById(acq1).value.replace(/,/g, '.'));
	var acq2 = parseFloat(document.getElementById(acq2).value.replace(/,/g, '.'));
	var acq3 = parseFloat(document.getElementById(acq3).value.replace(/,/g, '.'));
	
	if(acq1 == "" || isNaN(acq1))
		acq1 = 0;
		
	if(acq2 == "" || isNaN(acq2))
		acq2 = 0;
		
	if(acq3 == "" || isNaN(acq3))
		acq3 = 0;
		
	var listino = parseFloat(document.getElementById(listino).value.replace(/,/g, '.'));
	
	if(listino == "" || isNaN(listino))
		listino = 0;
		
	var newPrice = (listino * (100 - acq1) / 100) * ((100 - acq2)/100) * ((100 - acq3)/100);
	
	document.getElementById(acquisto).value = (isNaN(newPrice) == true || newPrice < 0) ? '' :
		ps_round(newPrice, 2);

	document.getElementById(acquisto).innerHTML = parseFloat(document.getElementById(acquisto).value);
}

function calcPriceTI()
{
	try {
	var tax = getTax();
	var priceTE = parseFloat(document.getElementById('priceTE').value.replace(/,/g, '.'));
	
	if(priceTE == "" || isNaN(priceTE))
		priceTE = 0;
		
	var newPrice = priceTE * ((tax / 100) + 1);
	document.getElementById('priceTI').value = (isNaN(newPrice) == true || newPrice < 0) ? '' :
		ps_round(newPrice, 2);
	document.getElementById('finalPrice').innerHTML = (isNaN(newPrice) == true || newPrice < 0) ? '' :
		ps_round(newPrice, 2).toFixed(2);
	document.getElementById('finalPriceWithoutTax').innerHTML = (isNaN(priceTE) == true || priceTE < 0) ? '' :
		(ps_round(priceTE, 2) + getEcotaxTaxExcluded()).toFixed(2);
	calcReduction();
	document.getElementById('priceTI').value = parseFloat(document.getElementById('priceTI').value) + getEcotaxTaxIncluded();
	document.getElementById('finalPrice').innerHTML = parseFloat(document.getElementById('priceTI').value);
	}
	catch(e) {
	}
}

function calcPriceTE()
{
	var tax = getTax();
	var priceTI = parseFloat(document.getElementById('priceTI').value.replace(/,/g, '.'));
	
	if(priceTI == "" || isNaN(priceTI))
		priceTI = 0;
		
	var newPrice = ps_round(priceTI - getEcotaxTaxIncluded(), 2) / ((tax / 100) + 1);
	document.getElementById('priceTE').value =	(isNaN(newPrice) == true || newPrice < 0) ? '' :
		ps_round(newPrice.toFixed(6), 6);
	document.getElementById('finalPrice').innerHTML = (isNaN(newPrice) == true || newPrice < 0) ? '' :
		ps_round(priceTI.toFixed(2), 2);
	document.getElementById('finalPriceWithoutTax').innerHTML = (isNaN(newPrice) == true || newPrice < 0) ? '' :
		ps_round(newPrice.toFixed(2), 2) + getEcotaxTaxExcluded();
	calcReduction();
}

function calcImpactPriceTI()
{
	var tax = getTax();
	var priceTE = parseFloat(document.getElementById('attribute_price').value.replace(/,/g, '.'));
	var newPrice = priceTE * ((tax / 100) + 1);
	$('#attribute_priceTI').val((isNaN(newPrice) == true || newPrice < 0) ? '' :ps_round(newPrice.toFixed(6), 6));
	var total = ps_round((parseFloat($('#attribute_priceTI').val())*parseInt($('#attribute_price_impact').val())+parseFloat($('#finalPrice').html())), 2);
	if (isNaN(total) || total < 0)
		$('#attribute_new_total_price').html('0.00');
	else
		$('#attribute_new_total_price').html(total);
}

function calcImpactPriceTE()
{
	var tax = getTax();
	var priceTI = parseFloat(document.getElementById('attribute_priceTI').value.replace(/,/g, '.'));
	priceTI = (isNaN(priceTI)) ? 0 : ps_round(priceTI);
	var newPrice = ps_round(priceTI, 2) / ((tax / 100) + 1);
	$('#attribute_price').val((isNaN(newPrice) == true || newPrice < 0) ? '' :ps_round(newPrice.toFixed(6), 6));
	var total = ps_round((parseFloat($('#attribute_priceTI').val())*parseInt($('#attribute_price_impact').val())+parseFloat($('#finalPrice').html())), 2);
	if (isNaN(total) || total < 0)
		$('#attribute_new_total_price').html('0.00');
	else
		$('#attribute_new_total_price').html(total);
}

function calcReduction()
{
	if (parseFloat($('#reduction_price').val()) > 0)
		reductionPrice();
	else if (parseFloat($('#reduction_percent').val()) > 0)
		reductionPercent();
}

function reductionPrice()
{
	var tax = getTax();
	var price    = document.getElementById('priceTI');
	var priceWhithoutTaxes = document.getElementById('priceTE');
	var newprice = document.getElementById('finalPrice');
	var newpriceWithoutTax = document.getElementById('finalPriceWithoutTax');
	var curPrice = price.value;

	document.getElementById('reduction_percent').value = 0;
	if (isInReductionPeriod())
	{
		var rprice = document.getElementById('reduction_price');
		if (parseFloat(curPrice) <= parseFloat(rprice.value))
			rprice.value = curPrice;
		if (parseFloat(rprice.value) < 0 || isNaN(parseFloat(curPrice)))
			rprice.value = 0;
		curPrice = curPrice - rprice.value;
	}

	newprice.innerHTML = (ps_round(parseFloat(curPrice),2) + getEcotaxTaxIncluded()).toFixed(2);
	var rpriceWithoutTaxes = ps_round(rprice.value / ((tax / 100) + 1), 2);
	newpriceWithoutTax.innerHTML = ps_round(priceWhithoutTaxes.value - rpriceWithoutTaxes,2).toFixed(2);
}

function reductionPercent()
{
	var tax = getTax();
	var price    = document.getElementById('priceTI');
	var newprice = document.getElementById('finalPrice');
	var newpriceWithoutTax = document.getElementById('finalPriceWithoutTax');
	var curPrice = price.value;

	document.getElementById('reduction_price').value = 0;
	if (isInReductionPeriod())
	{
		var newprice = document.getElementById('finalPrice');
		var rpercent = document.getElementById('reduction_percent');

		if (parseFloat(rpercent.value) >= 100)
			rpercent.value = 100;
		if (parseFloat(rpercent.value) < 0)
			rpercent.value = 0;
		curPrice = price.value * (1 - (rpercent.value / 100));
	}

	newprice.innerHTML = (ps_round(parseFloat(curPrice),2) + getEcotaxTaxIncluded()).toFixed(2);
	newpriceWithoutTax.innerHTML = ps_round(parseFloat(ps_round(curPrice, 2) / ((tax / 100) + 1)),2).toFixed(2);
}

function isInReductionPeriod()
{
	var start  = document.getElementById('reduction_from').value;
	var end    = document.getElementById('reduction_to').value;

	if (start == end && start != "" && start != "0000-00-00 00:00:00") return true;

	var sdate  = new Date(start.replace(/-/g,'/'));
	var edate  = new Date(end.replace(/-/g,'/'));
	var today  = new Date();

	return (sdate <= today && edate >= today);
}

function decimalTruncate(source, decimals)
{
	if (typeof(decimals) == 'undefined')
		decimals = 6;
	source = source.toString();
	var pos = source.indexOf('.');
	return parseFloat(source.substr(0, pos + decimals + 1));
}

function unitPriceWithTax(type)
{
	var tax = getTax();
	var priceWithTax = parseFloat(document.getElementById(type+'_price').value.replace(/,/g, '.'));
	var newPrice = priceWithTax * ((tax / 100) + 1);
	$('#'+type+'_price_with_tax').html((isNaN(newPrice) == true || newPrice < 0) ? '0.00' : ps_round(newPrice, 2).toFixed(2));
}

function unitySecond()
{
	$('#unity_second').html($('#unity').val());
	if ($('#unity').get(0).value.length > 0)
	{
		$('#unity_third').html($('#unity').val());
		$('#tr_unit_impact').show();
	}
	else
		$('#tr_unit_impact').hide();
}

function changeCurrencySpecificPrice(index)
{
	var id_currency = $('#spm_currency_' + index).val();
	if (currencies[id_currency]["format"] == 2 || currencies[id_currency]["format"] == 4)
	{
		$('#spm_currency_sign_pre_' + index).html('');
		$('#spm_currency_sign_post_' + index).html(' ' + currencies[id_currency]["sign"]);
	}
	else if (currencies[id_currency]["format"] == 1 || currencies[id_currency]["format"] == 3)
	{
		$('#spm_currency_sign_post_' + index).html('');
		$('#spm_currency_sign_pre_' + index).html(currencies[id_currency]["sign"] + ' ');
	}
}

function checkIfEmpty(y) 
{
    var x;
    x = y.value;
    if (x == "") {
        alert("Campo obbligatorio, inserisci un valore");
		window.setTimeout(function () { 
			y.focus(); 
		}, 0); 
    };
}

function validate_form_product()
{
	if(document.getElementById('listino_esolver').value == '') {
		alert("Il campo listino eSolver è obbligatorio");
		document.getElementById('listino_esolver').scrollIntoView();
		return false;
	}
	else if(document.getElementById('sconto_acquisto_1').value == '') {
		alert("Il campo sconto acquisto 1 è obbligatorio");
		document.getElementById('sconto_acquisto_1').scrollIntoView();
		return false;
	}
	else if(document.getElementById('sconto_acquisto_2').value == '') {
		alert("Il campo sconto acquisto 2 è obbligatorio");
		document.getElementById('sconto_acquisto_2').scrollIntoView();
		return false;
	}
	else if(document.getElementById('sconto_acquisto_3').value == '') {
		alert("Il campo sconto acquisto 3 è obbligatorio");
		document.getElementById('sconto_acquisto_3').scrollIntoView();
		return false;
	}
	else if(document.getElementById('wholesale_price_esolver').value == '') {
		alert("Il campo netto eSolver è obbligatorio");
		document.getElementById('wholesale_price_esolver').scrollIntoView();
		return false;
	}
	else if(document.getElementById('scontolistinovendita').value == '') {
		alert("Il campo sconto clienti è obbligatorio");
		document.getElementById('scontolistinovendita').scrollIntoView();
		return false;
	}
	else if(document.getElementById('priceTE').value == '') {
		alert("Il campo prezzo di vendita è obbligatorio");
		document.getElementById('priceTE').scrollIntoView();
		return false;
	}
	else if(document.getElementById('rebate_1').value == '') {
		alert("Il campo rebate 1 è obbligatorio");
		document.getElementById('rebate_1').scrollIntoView();
		return false;
	}
	else if(document.getElementById('rebate_2').value == '') {
		alert("Il campo rebate 2 è obbligatorio");
		document.getElementById('rebate_2').scrollIntoView();
		return false;
	}
	else if(document.getElementById('rebate_3').value == '') {
		alert("Il campo rebate 3 è obbligatorio");
		document.getElementById('rebate_3').scrollIntoView();
		return false;
	}
	else if(document.getElementById('wholesale_price').value == '') {
		alert("Il campo netto sales è obbligatorio");
		document.getElementById('wholesale_price').scrollIntoView();
		return false;
	}
	else if(document.getElementById('sconto_quantita_1').value == '') {
		alert("Il campo sconto quantita 1 è obbligatorio");
		document.getElementById('sconto_quantita_1').scrollIntoView();
		return false;
	}
	else if(document.getElementById('prezzo_qta_1').value == '') {
		alert("Il campo prezzo quantita 1 è obbligatorio");
		document.getElementById('prezzo_qta_1').scrollIntoView();
		return false;
	}
	else if(document.getElementById('sconto_quantita_2').value == '') {
		alert("Il campo sconto quantita 2 è obbligatorio");
		document.getElementById('sconto_quantita_2').scrollIntoView();
		return false;
	}
	else if(document.getElementById('prezzo_qta_2').value == '') {
		alert("Il campo prezzo quantita 2 è obbligatorio");
		document.getElementById('prezzo_qta_2').scrollIntoView();
		return false;
	}
	else if(document.getElementById('sconto_quantita_1').value == '') {
		alert("Il campo sconto quantita 1 è obbligatorio");
		document.getElementById('sconto_quantita_1').scrollIntoView();
		return false;
	}
	else if(document.getElementById('prezzo_qta_3').value == '') {
		alert("Il campo prezzo quantita 3 è obbligatorio");
		document.getElementById('prezzo_qta_3').scrollIntoView();
		return false;
	}
	else if(document.getElementById('sconto_rivenditore_1').value == '') {
		alert("Il campo sconto rivenditore 1 è obbligatorio");
		document.getElementById('sconto_rivenditore_1').scrollIntoView();
		return false;
	}
	else if(document.getElementById('prezzo_riv_1').value == '') {
		alert("Il campo prezzo rivenditore 1 è obbligatorio");
		document.getElementById('prezzo_riv_1').scrollIntoView();
		return false;
	}
	else if(document.getElementById('sconto_rivenditore_2').value == '') {
		alert("Il campo sconto rivenditore 2 è obbligatorio");
		document.getElementById('sconto_rivenditore_2').scrollIntoView();
		return false;
	}
	else if(document.getElementById('prezzo_riv_2').value == '') {
		alert("Il campo prezzo rivenditore 2 è obbligatorio");
		document.getElementById('prezzo_riv_2').scrollIntoView();
		return false;
	}
	else if(document.getElementById('sconto_rivenditore_3').value == '') {
		alert("Il campo sconto distributore è obbligatorio");
		document.getElementById('sconto_rivenditore_3').scrollIntoView();
		return false;
	}
	else if(document.getElementById('prezzo_riv_3').value == '') {
		alert("Il campo prezzo distributore è obbligatorio");
		document.getElementById('prezzo_riv_3').scrollIntoView();
		return false;
	}
	else
	{
		return true;	
	}
}