<?php
/**
 * This is file for override core prestashop Product class
 * Should be copied to /override/classes directory of prestashop installation location
 */
 
 class Product extends ProductCore
 {
    /** @var date_available */
    public $date_available = '1946-01-01';
	public $data_arrivo = '1946-01-01';
	public $data_arrivo_esprinet = '1946-01-01';
	
	public $description_amazon;
	
	public $stock_quantity;
	public $arrivo_quantity;
	public $supplier_quantity;
	public $esprinet_quantity;
	public $itancia_quantity;
	public $arrivo_esprinet_quantity;
	public $attiva_quantity;
	public $arrivo_attiva_quantity;
	public $ordinato_quantity;
	public $impegnato_quantity;
	public $intracom_quantity;
	public $arrivo_intracom_quantity;
	public $data_arrivo_intracom;
	public $asin;
	public $amazon_free_shipping;
	public $canonical;
	public $redirect_category;
	public $note;
	public $note_fornitore;
	public $scorta_minima;
	public $scorta_minima_amazon;
	public $fuori_produzione = false;
	public $acquisto_in_dollari = false;
	public $margin_min_cli;
	public $margin_min_riv;
	public $blocco_prezzi;
	public $login_for_offer;
	public $margin_login_for_offer;
	public $costo_dollari;
	public $dispo_type;
	public $noindex;
	public $prodotto_con_canone;
	public $date_available_msg;
	public $ottieni_miglior_prezzo_flag;
	
	protected $fieldsRequired = array('quantity', 'price', 'reference');
	
	protected $fieldsSize = array('reference' => 40, 'supplier_reference' => 40, 'location' => 64, 'ean13' => 13, 'upc' => 12, 'unity' => 10);
	
	protected $fieldsValidate = array(
		'id_tax_rules_group' => 'isUnsignedId',
		'id_manufacturer' => 'isUnsignedId',
		'id_supplier' => 'isUnsignedId',
		'id_category_default' => 'isUnsignedId',
		'id_color_default' => 'isUnsignedInt', /* unsigned integer because its value could be 0 if the feature is disabled */
		'minimal_quantity' => 'isUnsignedInt',
		'price' => 'isPrice',
		'listino' => 'isPrice',
		'additional_shipping_cost' => 'isPrice',
		'wholesale_price' => 'isPrice',
		'on_sale' => 'isBool',
		'online_only' => 'isBool',
		'fuori_produzione' => 'isBool',
		'acquisto_in_dollari' => 'isBool',
		'ecotax' => 'isPrice',
		'unit_price' => 'isPrice',
		'unity' => 'isString',
		'reference' => 'isReference',
		'supplier_reference' => 'isReference',
		'location' => 'isReference',
		'width' => 'isUnsignedFloat',
		'height' => 'isUnsignedFloat',
		'depth' => 'isUnsignedFloat',
		'weight' => 'isUnsignedFloat',
		'out_of_stock' => 'isUnsignedInt',
		'quantity_discount' => 'isBool',
		'customizable' => 'isUnsignedInt',
		'uploadable_files' => 'isUnsignedInt',
		'text_fields' => 'isUnsignedInt',
		'active' => 'isUnsignedId',
		'available_for_order' => 'isBool',
		'condition' => 'isGenericName',
		'show_price' => 'isBool',
		'amazon_free_shipping' => 'isBool',
		'ean13' => 'isEan13',
		'indexed' => 'isBool',
		'cache_is_pack' => 'isBool',
		'cache_has_attachments' => 'isBool'
	);
	
	protected $fieldsValidateLang = array(
		'meta_description' => 'isGenericName', 'meta_keywords' => 'isGenericName', 'cat_homepage' => 'isString', 'desc_homepage' => 'isString',
		'meta_title' => 'isGenericName', 'link_rewrite' => 'isLinkRewrite', 'name' => 'isCatalogName',
		'description' => 'isString', 'description_short' => 'isString', 'description_amazon' => 'isString',  'available_now' => 'isGenericName', 'available_later' => 'IsGenericName');
	
	public function getFields()
	{
		//parent::validateFields();
		if (isset($this->id))
			$fields['id_product'] = (int)($this->id);
		$fields['id_tax_rules_group'] = (int)($this->id_tax_rules_group);
		$fields['id_manufacturer'] = (int)($this->id_manufacturer);
		$fields['id_supplier'] = (int)($this->id_supplier);
		$fields['id_category_default'] = (int)($this->id_category_default);
		$fields['id_color_default'] = (int)($this->id_color_default);
		$fields['quantity'] = (int)($this->quantity);
		$fields['stock_quantity'] = (int)($this->stock_quantity);
		$fields['supplier_quantity'] = (int)($this->supplier_quantity);
		$fields['esprinet_quantity'] = (int)($this->esprinet_quantity);
		$fields['attiva_quantity'] = (int)($this->attiva_quantity);
		$fields['itancia_quantity'] = (int)($this->itancia_quantity);
		$fields['arrivo_quantity'] = (int)($this->arrivo_quantity);
		$fields['arrivo_esprinet_quantity'] = (int)($this->arrivo_esprinet_quantity);
		$fields['arrivo_attiva_quantity'] = (int)($this->arrivo_attiva_quantity);
		$fields['intracom_quantity'] = (int)($this->intracom_quantity);
		$fields['arrivo_intracom_quantity'] = (int)($this->arrivo_intracom_quantity);
		$fields['data_arrivo_intracom'] = (int)($this->data_arrivo_intracom);
		
		$fields['ordinato_quantity'] = (int)($this->ordinato_quantity);
		$fields['impegnato_quantity'] = (int)($this->impegnato_quantity);
		$fields['scorta_minima'] = (int)($this->scorta_minima);
		$fields['scorta_minima_amazon'] = (int)($this->scorta_minima_amazon);
		$fields['asin'] = pSQL($this->asin);
		$fields['minimal_quantity'] = (int)($this->minimal_quantity);
		$fields['price'] = (float)($this->price);
		$fields['listino'] = pSQL(str_replace(',','.',$this->listino));
		$fields['canonical'] = pSQL($this->canonical);
		$fields['redirect_category'] = pSQL($this->redirect_category);
		$fields['note'] = pSQL($this->note);
		$fields['note_fornitore'] = pSQL($this->note_fornitore);
		$fields['fuori_produzione'] = pSQL($this->fuori_produzione);
		$fields['acquisto_in_dollari'] = pSQL($this->acquisto_in_dollari);
		$fields['amazon_free_shipping'] = pSQL($this->amazon_free_shipping);
		$fields['scontolistinovendita'] = pSQL($this->scontolistinovendita);
		$fields['sconto_acquisto_1'] = pSQL($this->sconto_acquisto_1);
		$fields['sconto_acquisto_2'] = pSQL($this->sconto_acquisto_2);
		$fields['sconto_acquisto_3'] = pSQL($this->sconto_acquisto_3);

		$fields['additional_shipping_cost'] = (float)($this->additional_shipping_cost);
		$fields['wholesale_price'] = (float)($this->wholesale_price);
		$fields['on_sale'] = (int)($this->on_sale);
		$fields['online_only'] = (int)($this->online_only);
		$fields['ecotax'] = (float)($this->ecotax);
		$fields['unity'] = pSQL($this->unity);
		$fields['unit_price_ratio'] = (float)($this->unit_price > 0 ? $this->price / $this->unit_price : 0);
		$fields['ean13'] = pSQL($this->ean13);
		$fields['upc'] = pSQL($this->upc);
		$fields['reference'] = pSQL($this->reference);
		$fields['supplier_reference'] = pSQL($this->supplier_reference);
		$fields['location'] = pSQL($this->location);
		$fields['width'] = (float)($this->width);
		$fields['height'] = (float)($this->height);
		$fields['depth'] = (float)($this->depth);
		$fields['weight'] = (float)($this->weight);
		$fields['out_of_stock'] = pSQL($this->out_of_stock);
		$fields['quantity_discount'] = (int)($this->quantity_discount);
		$fields['customizable'] = (int)($this->customizable);
		$fields['uploadable_files'] = (int)($this->uploadable_files);
		$fields['text_fields'] = (int)($this->text_fields);
		$fields['active'] = (int)($this->active);
		$fields['available_for_order'] = (int)($this->available_for_order);
		$fields['condition'] = pSQL($this->condition);
		$fields['show_price'] = (int)($this->show_price);
		$fields['indexed'] = 0; // Reset indexation every times
		$fields['cache_is_pack'] = (int)($this->cache_is_pack);
		$fields['cache_has_attachments'] = (int)($this->cache_has_attachments);
		$fields['cache_default_attribute'] = (int)($this->cache_default_attribute);
		$fields['date_add'] = pSQL($this->date_add);
		$fields['date_upd'] = pSQL($this->date_upd);
		$fields['date_available'] = pSQL(date("Y-m-d H:i:s",strtotime($this->date_available)));
		$fields['data_arrivo'] = pSQL(date("Y-m-d H:i:s",strtotime($this->data_arrivo)));
		$fields['data_arrivo_esprinet'] = pSQL(date("Y-m-d H:i:s",strtotime($this->data_arrivo_esprinet)));
		
		$fields['margin_min_cli'] = pSQL(str_replace(',','.',$this->margin_min_cli));
		$fields['margin_login_for_offer'] = pSQL(str_replace(',','.',$this->margin_login_for_offer));
		$fields['costo_dollari'] = pSQL(str_replace(',','.',$this->costo_dollari));
		$fields['margin_min_riv'] = pSQL(str_replace(',','.',$this->margin_min_riv));
		$fields['blocco_prezzi'] = pSQL($this->blocco_prezzi);
		$fields['login_for_offer'] = pSQL($this->login_for_offer);
		$fields['noindex'] = pSQL($this->noindex);
		$fields['prodotto_con_canone'] = pSQL($this->prodotto_con_canone);
		$fields['dispo_type'] = pSQL($this->dispo_type);
		$fields['date_available_msg'] = pSQL($this->date_available_msg);
		$fields['ottieni_miglior_prezzo_flag'] = pSQL($this->ottieni_miglior_prezzo_flag);
		return $fields;
	}
 
	public function getTranslationsFieldsChild()
	{
		self::validateFieldsLang();

		$fieldsArray = array('meta_description', 'meta_keywords', 'meta_title', 'link_rewrite', 'cat_homepage', 'desc_homepage', 'name', 'available_now', 'available_later');
		$fields = array();
		$languages = Language::getLanguages(false);
		$defaultLanguage = Configuration::get('PS_LANG_DEFAULT');
		foreach ($languages as $language) {
			$fields[$language['id_lang']]['id_lang'] = $language['id_lang'];
			$fields[$language['id_lang']][$this->identifier] = (int)($this->id);
			$fields[$language['id_lang']]['desc_homepage'] = (isset($this->desc_homepage[$language['id_lang']])) ? pSQL($this->desc_homepage[$language['id_lang']], true) : '';
			$fields[$language['id_lang']]['description'] = (isset($this->description[$language['id_lang']])) ? pSQL($this->description[$language['id_lang']], true) : '';
			$fields[$language['id_lang']]['description_short'] = (isset($this->description_short[$language['id_lang']])) ? pSQL($this->description_short[$language['id_lang']], true) : '';
			$fields[$language['id_lang']]['description_amazon'] = (isset($this->description_amazon[$language['id_lang']])) ? pSQL($this->description_amazon[$language['id_lang']], true) : '';
			foreach ($fieldsArray as $field) {
				if (!Validate::isTableOrIdentifier($field))
					die(Tools::displayError());

				/* Check fields validity */
				if (isset($this->{$field}[$language['id_lang']]) AND !empty($this->{$field}[$language['id_lang']]))
					$fields[$language['id_lang']][$field] = pSQL($this->{$field}[$language['id_lang']]);
				elseif (in_array($field, $this->fieldsRequiredLang)) {
					if ($this->{$field} != '')
						$fields[$language['id_lang']][$field] = pSQL($this->{$field}[$defaultLanguage]);
				}
				else
					$fields[$language['id_lang']][$field] = '';
			}
		}
		return $fields;
	}
	
	public static function getFreeShipping($id_product)
	{
	
		$query = "
			SELECT * 
			FROM configuration 
			WHERE name = 'FREE_SHIPPING_INCLUDE_PRD'
		";

		$row = Db::getInstance()->getRow($query);
		
		$array_trasporti = unserialize($row['value']);
		
		foreach ($array_trasporti as $el) {
			if($id_product == $el) {
				$vero = 1;
				break;
			}
			else {
				$vero = 0;
			}
		}

		if($vero == 1) {
			return true;
		}
		else {
			return false;
		}
	}
 
 	public static function getGaranzia($id_lang, $id_product)
	{
		$query = ('
			SELECT fvl.value
			FROM '._DB_PREFIX_.'feature_product pf
			LEFT JOIN '._DB_PREFIX_.'feature_value_lang fvl ON (fvl.id_feature_value = pf.id_feature_value AND fvl.id_lang = '.(int)$id_lang.')
			WHERE pf.id_product = '.(int)$id_product.' 
				AND pf.id_feature = 449
		');
			
		$row = $row = Db::getInstance()->getRow($query);
	
		return $row['value'];
	}
 
  	public static function getConfezione($id_lang, $id_product)
	{
		$query = ('
			SELECT fvl.value
			FROM '._DB_PREFIX_.'feature_product pf
			LEFT JOIN '._DB_PREFIX_.'feature_value_lang fvl ON (fvl.id_feature_value = pf.id_feature_value AND fvl.id_lang = '.(int)$id_lang.')
			WHERE pf.id_product = '.(int)$id_product.' 
				AND pf.id_feature = 450
		');
		
		$result = mysql_query($query);
		$row = Db::getInstance()->getRow($query);
	
		return $row['value'];
	}
 
 	public static function duplicateAttachments($id_product_old, $id_product_new)
	{
		$return = true;
		$result = Db::getInstance()->ExecuteS('
		SELECT *
		FROM `'._DB_PREFIX_.'product_attachment`
		WHERE `id_product` = '.(int)($id_product_old));

		// Prepare data of table product_attachment
		foreach ($result as $row) {
			$data = array(
				'id_product' => (int)($id_product_new),
				'id_attachment' => (int)($row['id_attachment'])
			);
			$return &= Db::getInstance()->AutoExecute(_DB_PREFIX_.'product_attachment', $data, 'INSERT');
		}
		// Duplicate product attachement
		return $return;
	}

	/**
     * Get all features, which has values for product
     * 
     * @param int $id_lang Language on which we should return labels and values of features
     * @param bool $withGroups whether we should return features with groups or not
     * @return array of features. @see parent::getFrontFeatures()
     */ 
    public function getFrontFeatures($id_lang, $withGroups=true)
	{
        if (!$withGroups)
            return parent::getFrontFeatures($id_lang);
        $id_product = $this->id;
        //if (!array_key_exists($this->id.'-'.$id_lang, self::$_frontFeaturesCache))
		//{

		self::$_frontFeaturesCache[$this->id.'-'.$id_lang] = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT 
	            fl.name AS feature_name, 
	            value, 
	            pf.id_feature, 
	            fgl.id_group, 
	            fgl.name AS feature_group_name,
	            fg.position AS group_position,
	            f.position AS feature_position
			FROM '._DB_PREFIX_.'feature_product pf
	        LEFT JOIN '._DB_PREFIX_.'feature f ON pf.id_feature=f.id_feature
	        LEFT JOIN '._DB_PREFIX_.'feature_group fg ON f.id_group=fg.id_group
	        LEFT JOIN '._DB_PREFIX_.'feature_group_lang fgl ON (fg.id_group=fgl.id_group AND fgl.id_lang = '.(int)$id_lang.')
			LEFT JOIN '._DB_PREFIX_.'feature_lang fl ON (fl.id_feature = pf.id_feature AND fl.id_lang = '.(int)$id_lang.')
			LEFT JOIN '._DB_PREFIX_.'feature_value_lang fvl ON (fvl.id_feature_value = pf.id_feature_value AND fvl.id_lang = '.(int)$id_lang.')
			WHERE pf.id_product = '.intval($this->id).' 
			ORDER BY fg.position ASC, fl.name ASC');
			
		//}
        
        $result = array();
        foreach (self::$_frontFeaturesCache[$id_product.'-'.$id_lang] as $frecord) {
            if (!$frecord['id_group'] || !$frecord['id_feature'])
                continue;
            if (!array_key_exists($frecord["id_group"], $result))
                $result[$frecord["id_group"]] = array(
					"name" => $frecord['feature_group_name'], 
					"position" => $frecord["group_position"],
					'features' => array()
				);
            $result[$frecord['id_group']]['features'][$frecord['id_feature']] = array(
				"name" => $frecord['feature_name'], 
				"position" => $frecord["feature_position"],
				'value'=>$frecord["value"]
			);
        }
        
       /* uasort($result, array($this, 'sortFeaturesGroupsByPositionCallback'));
        foreach ($result as $group_id=>&$group_data) {
			uasort($group_data["features"], array($this, 'sortFeaturesByPositionCallback'));
		}*/
        return $result;
	}
	
	public static function getProductProperties($id_lang, $row, $id_bundle = 0)
	{
		// Product::getDefaultAttribute is only called if id_product_attribute is missing from the SQL query at the origin of it: consider adding it in order to avoid unnecessary queries
		$row['allow_oosp'] = Product::isAvailableWhenOutOfStock($row['out_of_stock']);
		if ((!isset($row['id_product_attribute']) OR !$row['id_product_attribute'])
			AND ((isset($row['cache_default_attribute']) AND ($ipa_default = $row['cache_default_attribute']) !== NULL)
				OR ($ipa_default = Product::getDefaultAttribute($row['id_product'], !$row['allow_oosp'])))
		)
			$row['id_product_attribute'] = $ipa_default;
		if (!isset($row['id_product_attribute']))
			$row['id_product_attribute'] = 0;

		// Tax
		$usetax = Tax::excludeTaxeOption();

		$cacheKey = $row['id_product'].'-'.$row['id_product_attribute'].'-'.$id_lang.'-'.(int)($usetax);
		if (array_key_exists($cacheKey, self::$producPropertiesCache))
		{
			// If product is in a pack, we want to add pack_quantity to the cached data returned
			if (isset($row['pack_quantity']))
			{
				$pack_product = self::$producPropertiesCache[$cacheKey];
				$pack_product['pack_quantity'] = $row['pack_quantity'];
				return $pack_product;
			}
			return self::$producPropertiesCache[$cacheKey];
		}
		
		if(isset($row['id_bundle_layered']) && $row['id_bundle_layered'] > 0)
			$row['bundle_layered'] = Bundle::getBundleForLayered($row['id_bundle_layered']);
		

		// Datas
		$link = new Link();
		$row['category'] = Category::getLinkRewrite((int)$row['id_category_default'], (int)($id_lang));
		$catn = new Category($row['id_category_default']);
		$row['category_name'] = $catn->getName($id_lang);
		
		$row['attribute_price'] = (isset($row['id_product_attribute']) AND $row['id_product_attribute']) ? (float)(Product::getProductAttributePrice($row['id_product_attribute'])) : 0;
		
		$row['virtual'] = ProductDownload::getIdFromIdProduct($row['id_product']);
		
		if (strpos($row['id_product'],'b') !== false) {
			
			$father = Db::getInstance()->getValue('SELECT father FROM bundle WHERE id_bundle = '.str_replace('b','',$row['id_product']).'');
			$father = new Product($father);
			if($father->fuori_produzione == 1)
				$row['fuori_produzione'] = 1;
			/*if(array_shift(explode('?', basename($_SERVER['PHP_SELF']))) == 'category.php' || array_shift(explode('?', basename($_SERVER['PHP_SELF']))) == 'blocklayered-ajax.php')
			{
				$father = Db::getInstance()->getRow('SELECT pl.link_rewrite lnk, pl.id_lang, cl.link_rewrite AS category, p.id_product AS prd, supplier_reference FROM product p JOIN product_lang pl ON p.id_product = pl.id_product JOIN category_lang cl ON p.id_category_default = cl.id_category WHERE cl.id_lang = '.$id_lang.' AND pl.id_lang = '.$id_lang.' AND p.id_product = (SELECT father FROM bundle WHERE id_bundle = '.(str_replace('b','',$row['id_product'])).')'); 
				$row['link'] = $link->getProductLink($father['prd'], $father['lnk'], $father['category'], $father['supplier_reference']);
				$row['link'] .= '#bundles?bundles-selection=selected';
			}
			else {*/
				$row['link'] = $link->getBundleLink(str_replace('b','',$row['id_product']));
			/*}*/
			$row['id_image'] = $row['id_image']."-".str_replace('b','',$row['id_product']);
			
			$row['price_tax_exc'] = Bundle::getBundlePrice(str_replace('b','',$row['id_product']), 1);
			$row['price_tax_exc'] = Tools::ps_round($row['price_tax_exc'], 2);
			$row['price'] = Bundle::getBundlePrice(str_replace('b','',$row['id_product']), 1);
			$row['price_without_reduction'] = Bundle::getBundlePrice(str_replace('b','',$row['id_product']), 1);
		}
		else {
			$row['link'] = $link->getProductLink((int)$row['id_product'], $row['link_rewrite'], $row['category'], $row['supplier_reference']);
			$row['price_tax_exc'] = Product::getPriceStatic((int)$row['id_product'], false, ((isset($row['id_product_attribute']) AND !empty($row['id_product_attribute'])) ? (int)($row['id_product_attribute']) : NULL), (self::$_taxCalculationMethod == PS_TAX_EXC ? 2 : 6));
			if(!$row['id_image'] || $row['id_image'] == '') 
				$row['id_image'] = Db::getInstance()->getValue('SELECT id_image FROM image WHERE id_product = '.$row['id_product'].' AND cover = 1');
			$row['id_image'] = $row['id_product']."-".$row['id_image'];
			
			$secondary_image = Db::getInstance()->getValue('SELECT id_image FROM image WHERE id_product = '.$row['id_product'].' AND cover = 0 ORDER BY position ASC');
			
			if($secondary_image > 0)
				$row['secondary_image'] =  $row['id_product']."-".$secondary_image;
			else
				$row['secondary_image'] = '';
			
			if (self::$_taxCalculationMethod == PS_TAX_EXC)
			{
				
				$row['price_tax_exc'] = Tools::ps_round($row['price_tax_exc'], 2);
				$row['price'] = Product::getPriceStatic((int)$row['id_product'], true, ((isset($row['id_product_attribute']) AND !empty($row['id_product_attribute'])) ? (int)($row['id_product_attribute']) : 	NULL), 6);
				$row['price_without_reduction'] = Product::getPriceStatic((int)$row['id_product'], false, ((isset($row['id_product_attribute']) AND !empty($row['id_product_attribute'])) ? (int)($row['id_product_attribute']) : NULL), 2, NULL, false, false);
			}
			else
			{
				$row['price'] = Tools::ps_round(Product::getPriceStatic((int)$row['id_product'], true, ((isset($row['id_product_attribute']) AND !empty($row['id_product_attribute'])) ? (int)($row['id_product_attribute']) : NULL), 2), 2);
				$row['price_without_reduction'] = Product::getPriceStatic((int)$row['id_product'], true, ((isset($row['id_product_attribute']) AND !empty($row['id_product_attribute'])) ? (int)($row['id_product_attribute']) : NULL), 6, NULL, false, false);
			}
		}
		$row['reduction'] = Product::getPriceStatic((int)($row['id_product']), (bool)$usetax, (int)($row['id_product_attribute']), 6, NULL, true, true, 1, true, NULL, NULL, NULL, $specific_prices);
		$row['specific_prices'] = $specific_prices;

		if ($row['id_product_attribute'])
		{
			$row['quantity_all_versions'] = $row['quantity'];
			$row['quantity'] = Product::getQuantity((int)$row['id_product'], $row['id_product_attribute'], isset($row['cache_is_pack']) ? $row['cache_is_pack'] : NULL);
		}
		
		$row['features'] = Product::getFrontFeaturesStatic((int)$id_lang, $row['id_product']);
		$row['attachments'] = ((!isset($row['cache_has_attachments']) OR $row['cache_has_attachments']) ? Product::getAttachmentsStatic((int)($id_lang), $row['id_product']) : array());

		// Pack management
		$row['pack'] = (!isset($row['cache_is_pack']) ? Pack::isPack($row['id_product']) : (int)$row['cache_is_pack']);
		$row['packItems'] = $row['pack'] ? Pack::getItemTable($row['id_product'], $id_lang) : array();
		$row['nopackprice'] = $row['pack'] ? Pack::noPackPrice($row['id_product']) : 0;
		if($id_bundle > 0) {
		
			$row['quantity_in_bundle'] = Product::getQuantityInBundle($row['id_product'], $id_bundle);
		}
		else {
		
		}
		
		if (strpos($row['id_product'],'b') !== false) {
			$father = Db::getInstance()->getValue('SELECT father FROM bundle WHERE id_bundle = '.(str_replace('b','',$row['id_product'])));
		}
		
		if($row['stock_quantity'] < 0)
			$row['stock_quantity'] = 0;
		
		if($row['supplier_quantity'] < 0)
			$row['supplier_quantity'] = 0;
		
		if($row['asit_quantity'] < 0)
			$row['asit_quantity'] = 0;
		
		if($row['esprinet_quantity'] < 0)
			$row['esprinet_quantity'] = 0;
		
		if($row['itancia_quantity'] < 0)
			$row['itancia_quantity'] = 0;
		
		if($row['intracom_quantity'] < 0)
			$row['intracom_quantity'] = 0;
		
		if($row['attiva_quantity'] < 0)
			$row['attiva_quantity'] = 0;
		
		$supplier = Db::getInstance()->getValue('SELECT id_supplier FROM product WHERE id_product = '.(strpos($row['id_product'],'b') !== false ? $father : $row['id_product']));
		
		switch($supplier)
		{
			case 11: $main_supplier = 'supplier_quantity '; break;
			case 38: $main_supplier = 'esprinet_quantity '; break;
			case 42: $main_supplier = 'itancia_quantity '; break;
			case 76: $main_supplier = 'asit_quantity '; break;
			case 95: $main_supplier = 'intracom_quantity '; break;
			case 1450: $main_supplier = 'attiva_quantity '; break;
			default: $main_supplier = '0 '; break;
		}	
		
		$dispo_type = Db::getInstance()->getValue('SELECT dispo_type FROM product WHERE id_product = '.(strpos($row['id_product'],'b') !== false ? $father : $row['id_product']));
		switch($dispo_type)
		{
			case 0: $row['quantity'] = $row['quantity']; break;
			case 1: $row['quantity'] = $row['stock_quantity']; break;
			case 2: $row['quantity'] = Db::getInstance()->getValue('SELECT '.$main_supplier.' AS main_quantity FROM product WHERE id_product = '.(strpos($row['id_product'],'b') !== false ? $father : $row['id_product'])) + $row['stock_quantity']; break;
			default: $row['quantity'] = $row['quantity']; break;
		}	
		//applico regola switch a tutti i prodotti... nuove disposizioni di Ezio
		//$row['quantity'] = Db::getInstance()->getValue('SELECT '.$main_supplier.' AS main_quantity FROM product WHERE id_product = '.(strpos($row['id_product'],'b') !== false ? $father : $row['id_product'])); 
		
		if($row['quantity'] < 0)
			$row['quantity'] = 0;
		
		//$row['quantity'] += $row['stock_quantity'];
		
		
		//$esprinet_blacklist = array(25,120,43,117,133,109,37,10);
		
		
		
		
		/*if(in_array($row['id_manufacturer'],$esprinet_blacklist))
			$row['quantity'] =  Db::getInstance()->getValue('SELECT supplier_quantity + stock_quantity AS main_quantity FROM product WHERE id_product = '.(strpos($row['id_product'],'b') !== false ? $father : $row['id_product']));
		*/
		if($row['id_manufacturer'] == 27)
		{
			$row['itancia_quantity'] = 0;
			$row['quantity'] = $row['stock_quantity'] - $row['itancia_quantity'] + $row['asit_quantity'];
		}
		
		/*$qta_ord_clienti = Db::getInstance()->getValue('SELECT sum(product_quantity - (REPLACE(cons,"0_",""))) FROM order_detail od JOIN orders o ON o.id_order = od.id_order 
		JOIN cart c ON o.id_cart = c.id_cart JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE c.name NOT LIKE "%prime%" AND moh.id_order_state != 18 AND moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE  od.product_id = '.(strpos($row['id_product'],'b') !== false ? $father : $row['id_product']).' AND os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state != 20 AND os.id_order_state != 26 AND os.id_order_state != 29 AND os.id_order_state != 31 AND os.id_order_state != 32 GROUP BY od.product_id');*/
		
		
						
		if(strpos($row['id_product'],'b') !== false)
			$qta_ord_clienti = Product::calcolaImpegnato($father);
		else
			$qta_ord_clienti = Product::calcolaImpegnato($row['id_product']);

		if(!$qta_ord_clienti)
			$qta_ord_clienti = 0;
		
		$row['qta_ord_clienti'] = $qta_ord_clienti;
		
		$row['real_quantity'] = $row['quantity'] - $qta_ord_clienti;
	
		if ($row['pack'] AND !Pack::isInStock($row['id_product']))
			$row['quantity'] =  0;
		
		if($row['id_manufacturer'] == 132 || $row['id_manufacturer'] == 133 || $row['id_manufacturer'] == 37 || $row['id_manufacturer'] == 10 || $row['id_manufacturer'] == 27 || $row['id_manufacturer'] == 105 || $row['id_manufacturer'] == 117 || $row['id_manufacturer'] == 25 || $row['id_manufacturer'] == 120 || $row['id_manufacturer'] == 43 || $row['id_manufacturer'] == 109 || $row['id_manufacturer'] == 179 || $row['id_manufacturer'] == 184 || $row['id_manufacturer'] == 172 || $row['id_manufacturer'] == 108 || $row['id_manufacturer'] == 182 || $row['id_manufacturer'] == 113 || $row['id_manufacturer'] == 116 || $row['id_manufacturer'] == 181 || $row['id_manufacturer'] == 191)
			$row['migliorprezzo_link'] = 1;
		else
			$row['migliorprezzo_link'] = 0;
		
		
		/*if(Product::checkPrezzoSpeciale($row['id_product'],1,999999) == 'yes')
			$row['migliorprezzo_link'] = 1;
		else
			$row['migliorprezzo_link'] = 0;*/
		
		$averages = Product::getAveragesByProduct($row['id_product'], (int)$id_lang);
		$row['total_reviews'] = (int)(Product::getCommentNumber((int)$row['id_product']));
		$averageTotal = 0;
		foreach ($averages AS $average)
			$averageTotal += (float)($average);
		
		$averageTotal = count($averages) ? ($averageTotal / count($averages)) : 0;
		
		$row['averageTotal'] = $averageTotal;

		self::$producPropertiesCache[$cacheKey] = $row;
		return self::$producPropertiesCache[$cacheKey];
	}
	
	public static function getProductsProperties($id_lang, $query_result, $id_bundle = 0)
	{
		$resultsArray = array();
		if (is_array($query_result)){
			foreach ($query_result AS $row) {
				if ($row2 = Product::getProductProperties($id_lang, $row, $id_bundle))
					$resultsArray[] = $row2;
			}
		}
		return $resultsArray;
	}
	
	public static function getUrlRewriteInformations($id_product)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT pl.`id_lang`, pl.`link_rewrite`, p.`ean13`, p.`supplier_reference`, cl.`link_rewrite` AS category_rewrite
			FROM `'._DB_PREFIX_.'product` p
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product`)
			LEFT JOIN `'._DB_PREFIX_.'lang` l ON (pl.`id_lang` = l.`id_lang`)
			LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (cl.`id_category` = p.`id_category_default` AND cl.`id_lang` = pl.`id_lang`)
			WHERE p.`id_product` = '.(int)$id_product. '
				AND l.`active` = 1
		');
	}
	
	public function sortFeaturesByPositionCallback($a, $b)
	{
		return intval($a["position"]) < intval($b["position"]) ? -1 : 1;
	}
	
	public function sortFeaturesGroupsByPositionCallback($a, $b)
	{
		return intval($a["position"]) < intval($b["position"]) ? -1 : 1;
	}
	
	public static function searchByName($id_lang, $query, $extra = '', $opzioni = '')
	{
		$opzioni = unserialize($opzioni);
		
		//OR (MATCH(pl.name) AGAINST ("+'.str_replace(' ',' +',$query).'" IN BOOLEAN MODE))
		$result = Db::getInstance()->executeS('
			SELECT SQL_CACHE p.`id_product`, pl.`name`, p.`active`, p.`reference`, p.`listino`, p.`id_category_default`, p.id_supplier, p.ean13, p.asin, p.`stock_quantity`, p.`ordinato_quantity`, p.`arrivo_quantity`, p.`supplier_quantity`, p.`esprinet_quantity`, p.`attiva_quantity`, p.amazon_quantity, p.itancia_quantity, p.asit_quantity, p.impegnato_quantity, p.id_manufacturer, p.`date_add`, p.`date_upd`, m.`name` AS manufacturer_name ,
			(CASE p.id_supplier WHEN 11 THEN supplier_quantity WHEN 38 THEN esprinet_quantity WHEN 1450 THEN attiva_quantity WHEN 42 THEN itancia_quantity WHEN 95 THEN intracom_quantity WHEN 76 THEN asit_quantity ELSE 0 END) first_supplier,
			(CASE WHEN p.other_suppliers LIKE "%11%" THEN supplier_quantity WHEN p.other_suppliers LIKE "%38%" THEN esprinet_quantity WHEN p.other_suppliers LIKE "%1450%" THEN attiva_quantity WHEN p.other_suppliers LIKE "%42%" THEN itancia_quantity WHEN p.other_suppliers LIKE "%95%" THEN intracom_quantity WHEN p.other_suppliers LIKE "%76%" THEN asit_quantity ELSE 0 END) second_supplier,
			(CASE p.id_supplier WHEN 11 THEN "Allnet" WHEN 38 THEN "Esprinet" WHEN 42 THEN "Itancia" WHEN 95 THEN "Intracom" WHEN 76 THEN "Asit" ELSE "N.D." END) first_supplier_name,
			(CASE WHEN p.other_suppliers LIKE "%11%" THEN "Allnet" WHEN p.other_suppliers LIKE "%38%" THEN "Esprinet" WHEN p.other_suppliers LIKE "%42%" THEN "Itancia" WHEN p.other_suppliers LIKE "%95%" THEN "Intracom" WHEN p.other_suppliers LIKE "%76%" THEN "Asit" ELSE "N.D." END) second_supplier_name,
			(CASE p.id_supplier WHEN 11 THEN arrivo_quantity WHEN 38 THEN arrivo_esprinet_quantity WHEN 1450 THEN arrivo_attiva_quantity WHEN 42 THEN 0 WHEN 95 THEN arrivo_intracom_quantity WHEN 76 THEN 0 ELSE 0 END) arrivo_supplier,
			cap.prev
			FROM `'._DB_PREFIX_.'product` p
			LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON p.`id_product` = cp.`id_product`
			
			LEFT JOIN (SELECT sum(quantity) prev, cp.id_product FROM cart_product cp JOIN cart c ON c.id_cart = cp.id_cart WHERE c.preventivo = 1 AND c.date_upd >= ( CURDATE() - INTERVAL 15 DAY ) GROUP BY cp.id_product) cap ON cap.id_product = p.id_product
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')
			LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON m.`id_manufacturer` = p.`id_manufacturer`
			
			WHERE   '.($query != '' ? '(p.id_product LIKE \'%'.pSQL($query).'%\' OR pl.`name` LIKE \'%'.pSQL($query).'%\' OR p.`reference` LIKE \'%'.pSQL($query).'%\' OR p.`fnsku` LIKE \'%'.pSQL($query).'%\' OR p.`supplier_reference` LIKE \'%'.pSQL($query).'%\' OR p.`ean13` LIKE \'%'.pSQL($query).'%\'  OR p.`upc` LIKE \'%'.pSQL($query).'%\' OR p.`asin` LIKE \'%'.pSQL($query).'%\'  OR m.`name` LIKE \'%'.pSQL($query).'%\'
			OR REPLACE(pl.name," ","") LIKE \'%'.pSQL($query).'%\'
			OR REPLACE(p.`reference`," ","") LIKE \'%'.pSQL($query).'%\'
			OR REPLACE(p.`supplier_reference`," ","") LIKE \'%'.pSQL($query).'%\'
			
			OR REPLACE(pl.name," ","") LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\'
			OR REPLACE(p.`reference`," ","") LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\'
			OR REPLACE(p.`supplier_reference`," ","") LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\'
		
			OR (pl.name) LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\'
			OR (p.`reference`) LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\'
			OR (p.`supplier_reference`) LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\'
			
			'.($opzioni['note'] == 'on' ? 'OR REPLACE(p.`note`," ","") LIKE \'%'.pSQL($query).'%\' OR REPLACE(p.`note_fornitore`," ","") LIKE \'%'.pSQL($query).'%\'' : '').'
			
			) ' : 'p.id_product > 0 ').'
			
			AND (
			'.($opzioni['online'] == 'on' ? 'p.active = 1' : 'p.active = 0').'
			'.($opzioni['offline'] == 'on' ? 'OR p.active = 0' : '').'
			'.($opzioni['old'] == 'on' ? 'OR p.active = 2' : '').')
			
			'.($opzioni['disponibili'] == 'on' ? 'AND p.quantity > 0' : '').'
			'.($opzioni['marca'] != 0 ? 'AND p.id_manufacturer = '.$opzioni['marca'] : '').'
			'.($opzioni['serie'] != 0 ? 'AND p.id_product IN (SELECT p.id_product FROM feature_value fv JOIN feature_product fp ON fp.id_feature_value = fv.id_feature_value JOIN product p ON fp.id_product = p.id_product WHERE fv.id_feature_value = '.$opzioni['serie'].')' : '').'
			'.($opzioni['fornitore'] != 0 ? 'AND p.id_supplier = '.$opzioni['fornitore'] : '').'
			GROUP BY `id_product`
			'.(isset($_GET['orderby']) ? 'ORDER BY '.$_GET['orderby'].(isset($_GET['orderway']) ? ' '.$_GET['orderway'] : 'ASC') : 'ORDER BY pl.`name` ASC')
		);

		if (!$result)
			return false;

		$resultsArray = array();
		foreach ($result AS $row)
		{
			$row['price'] = Product::getPriceStatic($row['id_product'], false, NULL, 2);
			$row['wholesale_price'] = Product::getCurrentWholesalePrice($row['id_product']);
			$row['priceii'] = Product::getPriceStatic($row['id_product']);
			$row['quantity'] = Product::getQuantity($row['id_product']);
			$resultsArray[] = $row;
		}
		return $resultsArray;
	}
	
	public static function getNewProducts($id_lang, $pageNumber = 0, $nbProducts = 10, $count = false, $orderBy = NULL, $orderWay = NULL)
	{
		if ($pageNumber < 0) $pageNumber = 0;
		if ($nbProducts < 1) $nbProducts = 10;
		if (empty($orderBy) || $orderBy == 'position') $orderBy = 'date_add';
		if (empty($orderWay)) $orderWay = 'DESC';
		if ($orderBy == 'id_product' OR $orderBy == 'price' OR $orderBy == 'date_add')
			$orderByPrefix = 'p';
		elseif ($orderBy == 'name')
			$orderByPrefix = 'pl';
		if (!Validate::isOrderBy($orderBy) OR !Validate::isOrderWay($orderWay))
			die(Tools::displayError());

		$id_category = $_GET['cat'];
		
		$groups = FrontController::getCurrentCustomerGroups();
		$sqlGroups = (count($groups) ? 'IN ('.implode(',', $groups).')' : '= 1');

		if ($count) 
		{
			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
				SELECT COUNT(*) AS nb
				FROM (SELECT p.* FROM `'._DB_PREFIX_.'product` p
				WHERE `active` = 1
				AND DATEDIFF(p.`date_add`, DATE_SUB(NOW(), INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY)) > 0
				AND p.`id_product` IN (
					SELECT cp.`id_product`
					FROM `'._DB_PREFIX_.'category_group` cg
					LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON (cp.`id_category` = cg.`id_category`)
					WHERE cg.`id_group` '.$sqlGroups.' LIMIT 15) t
				)
			');

			return (int)($result['nb']);
		}

		// override
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT p.*, pl.`description`, pl.`description_short`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`,  pl.`cat_homepage`,  pl.`desc_homepage`, pl.`meta_title`, pl.`name`, p.`ean13`, p.`upc`,
				i.`id_image`, il.`legend`, t.`rate`, m.`name` AS manufacturer_name,
				(p.`price` * ((100 + (t.`rate`))/100)) AS orderprice, pa.id_product_attribute
			FROM `'._DB_PREFIX_.'product` p
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')
			LEFT OUTER JOIN `'._DB_PREFIX_.'product_attribute` pa ON (p.`id_product` = pa.`id_product` AND `default_on` = 1)
			LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
			LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)($id_lang).')
			LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON cp.`id_product` = p.`id_product`
			LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (p.`id_tax_rules_group` = tr.`id_tax_rules_group`
			   AND tr.`id_country` = '.(int)Country::getDefaultCountryId().'
			   AND tr.`id_state` = 0)
		    LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
			LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
			WHERE p.`active` = 1'.($id_category > 0 ? ' AND cp.`id_category` = '.$id_category : '').'
			AND p.id_category_default != 246 AND p.id_category_default != 256
			AND p.`id_product` IN (
				SELECT cp.`id_product`
				FROM `'._DB_PREFIX_.'category_group` cg
				LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON (cp.`id_category` = cg.`id_category`)
				WHERE cg.`id_group` '.$sqlGroups.'
			)
			GROUP BY p.id_product
			
			ORDER BY p.id_product DESC, '.(isset($orderByPrefix) ? pSQL($orderByPrefix).'.' : '').'`'.pSQL($orderBy).'` '.pSQL($orderWay).'
			
			LIMIT 15
		');

		if ($orderBy == 'price')
			Tools::orderbyPrice($result, $orderWay);
		if (!$result)
			return false;

		$productsIds = array();
		foreach ($result as $row)
			$productsIds[] = $row['id_product'];
		// Thus you can avoid one query per product, because there will be only one query for all the products of the cart
		Product::cacheFrontFeatures($productsIds, $id_lang);

		return Product::getProductsProperties((int)$id_lang, $result);
	}
	
	public static function getNewProductsImgOnly($id_lang, $pageNumber = 0, $nbProducts = 10, $count = false, $orderBy = NULL, $orderWay = NULL)
	{
		if ($pageNumber < 0) $pageNumber = 0;
		if ($nbProducts < 1) $nbProducts = 10;
		if (empty($orderBy) || $orderBy == 'position') $orderBy = 'date_add';
		if (empty($orderWay)) $orderWay = 'DESC';
		if ($orderBy == 'id_product' OR $orderBy == 'price' OR $orderBy == 'date_add')
			$orderByPrefix = 'p';
		elseif ($orderBy == 'name')
			$orderByPrefix = 'pl';
		if (!Validate::isOrderBy($orderBy) OR !Validate::isOrderWay($orderWay))
			die(Tools::displayError());

			$id_category = $_GET['cat'];
		$groups = FrontController::getCurrentCustomerGroups();
	
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
		SELECT p.*, pl.`description`, pl.`description_short`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`,  pl.`cat_homepage`,  pl.`desc_homepage`, pl.`meta_title`, pl.`name`, p.`ean13`, p.`upc`,
			i.`id_image`, il.`legend`
		FROM `'._DB_PREFIX_.'product` p
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')
		LEFT OUTER JOIN `'._DB_PREFIX_.'product_attribute` pa ON (p.`id_product` = pa.`id_product` AND `default_on` = 1)
		JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
		LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)($id_lang).')
		WHERE p.`active` = 1'.($id_category > 0 ? ' AND cp.`id_category` = '.$id_category : '').'
		AND p.id_category_default != 246 AND p.id_category_default != 256
		
		GROUP BY p.id_product
		
		ORDER BY (CASE WHEN p.price = 0 THEN 1 ELSE 0 END), p.id_product DESC, '.(isset($orderByPrefix) ? pSQL($orderByPrefix).'.' : '').'`'.pSQL($orderBy).'` '.pSQL($orderWay).'
		
		LIMIT 4');

		if ($orderBy == 'price')
			Tools::orderbyPrice($result, $orderWay);
		if (!$result)
			return false;

		$productsIds = array();
		foreach ($result as $row)
			$productsIds[] = $row['id_product'];
		// Thus you can avoid one query per product, because there will be only one query for all the products of the cart
		Product::cacheFrontFeatures($productsIds, $id_lang);

		return Product::getProductsProperties((int)$id_lang, $result);
	}
	
	public function duplicateComparaprezzi($id_product_old, $id_product_new)
	{
		$query = ('
		SELECT id_category_gst, trovaprezzi, google_shopping, eprice, amazon, comparaprezzi_check
		FROM `'._DB_PREFIX_.'product`
		WHERE `id_product` = '.(int)($id_product_old));
		
		$row = Db::getInstance()->getRow($query);
		Db::getInstance()->executeS("UPDATE product SET id_category_gst = $row[id_category_gst], eprice = $row[eprice], amazon = '".$row['amazon']."', trovaprezzi = $row[trovaprezzi], google_shopping = $row[google_shopping], comparaprezzi_check = $row[comparaprezzi_check] WHERE id_product = $id_product_new");
		
	}
	
	/**
	* Price calculation / Get product price
	*
	* @param integer $id_shop Shop id
	* @param integer $id_product Product id
	* @param integer $id_product_attribute Product attribute id
	* @param integer $id_country Country id
	* @param integer $id_state State id
	* @param integer $id_currency Currency id
	* @param integer $id_group Group id
	* @param integer $quantity Quantity Required for Specific prices : quantity discount application
	* @param boolean $use_tax with (1) or without (0) tax
	* @param integer $decimals Number of decimals returned
	* @param boolean $only_reduc Returns only the reduction amount
	* @param boolean $use_reduc Set if the returned amount will include reduction
	* @param boolean $with_ecotax insert ecotax in price output.
	* @param variable_reference $specific_price_output If a specific price applies regarding the previous parameters, this variable is filled with the corresponding SpecificPrice object
	* @return float Product price
	**/

	public static function priceCalculation($id_shop, $id_product, $id_product_attribute, $id_country, $id_state, $id_county, $id_currency, $id_group, $quantity, $use_tax, $decimals, $only_reduc, $use_reduc, $with_ecotax, &$specific_price, $use_groupReduction, $user_login = false)
	{
		global $cookie;
		$decimals = 2;
		// Caching
		if ($id_product_attribute === NULL)
			$product_attribute_label = 'NULL';
		else
			$product_attribute_label = ($id_product_attribute === false ? 'false' : $id_product_attribute);
		$cacheId = $id_product.'-'.$id_shop.'-'.$id_currency.'-'.$id_country.'-'.$id_state.'-'.$id_county.'-'.$id_group.'-'.$quantity.'-'.$product_attribute_label.'-'.($use_tax?'1':'0').'-'.$decimals.'-'.($only_reduc?'1':'0').'-'.($use_reduc?'1':'0').'-'.$with_ecotax;

		// reference parameter is filled before any returns
		
		$login_for_offer = Db::getInstance()->getValue('SELECT login_for_offer FROM product WHERE id_product = '.$id_product);
		$wholesale_price = Db::getInstance()->getValue('SELECT wholesale_price FROM product WHERE id_product = '.$id_product);
		$margin_login_for_offer = Db::getInstance()->getValue('SELECT margin_login_for_offer FROM product WHERE id_product = '.$id_product);
		$prezzo_login_for_offer = (($wholesale_price * 100) / (100 - $margin_login_for_offer));	
		
		// REGOLA DEL PREZZO PIU BASSO PER I RIVENDITORI
		if($id_group != 3 && $id_group != 10 && $id_group != 12 && $id_group != 22) {
			$id_group = 1;
			$specific_price = SpecificPrice::getSpecificPrice((int)($id_product), $id_shop, $id_currency, $id_country, $id_group, $quantity);
		}
		else if($id_group == 3 || $id_group == 10 || $id_group == 12 || $id_group == 22) {
		
			$specific_priceRIV = SpecificPrice::getSpecificPrice((int)($id_product), $id_shop, $id_currency, $id_country, $id_group, $quantity);
			$prezzotestRIV = $specific_priceRIV['price']-($specific_priceRIV['price']*$specific_priceRIV['reduction']);
			
			
			$trepercentoriv2 = 'n';
			
			if($id_group == 22)
			{	
				$testriv2 = Db::getInstance()->getValue('SELECT id_specific_price FROM specific_price WHERE specific_price_name = "sc_riv_2" AND id_product = '.$id_product);
				if($testriv2 > 0)
				{	
					$specific_priceRIV = SpecificPrice::getSpecificPrice((int)($id_product), $id_shop, $id_currency, $id_country, 22, $quantity);
					$prezzotestRIV = $specific_priceRIV['price']-($specific_priceRIV['price']*$specific_priceRIV['reduction']);
				}	
				else
				{	
					$specific_priceRIV = SpecificPrice::getSpecificPrice((int)($id_product), $id_shop, $id_currency, $id_country, 3, $quantity);
					$prezzotestRIV = $specific_priceRIV['price']-($specific_priceRIV['price']*$specific_priceRIV['reduction']);
					//SE GRUPPO RIVENDITORI2, AGGIUNGO IL 3%
					
					if($prezzotestRIV > 0)
						$trepercentoriv2 = 'y';
					
					$prezzotestRIV = $prezzotestRIV + ($prezzotestRIV/100 * 3);
					
				}	
			}
			
			$specific_priceCLI = SpecificPrice::getSpecificPrice((int)($id_product), $id_shop, $id_currency, $id_country, 1, 9999);
			
			$prezzotestCLI = $specific_priceCLI['price']-($specific_priceCLI['price']*$specific_priceCLI['reduction']);
		
			
			//SE IL PREZZO RIVENDITORE SCONTATO E' PIU ALTO DEL PREZZO QUANTITA CLIENTE, PRENDO (PER IL RIVENDITORE) IL PREZZO QTA CLIENTE PIU BASSO
			if($prezzotestRIV > $prezzotestCLI) {
				$specific_price = SpecificPrice::getSpecificPrice((int)($id_product), $id_shop, $id_currency, $id_country, 1, 99999);
			}
			//ALTRIMENTI MI COMPORTO NORMALMENTE: PRENDO IL PREZZO RIVENDITORE
			else {
				$specific_price = SpecificPrice::getSpecificPrice((int)($id_product), $id_shop, $id_currency, $id_country, $id_group, $quantity);
				
				// SE GRUPPO RIVENDITORI 2, PRENDO PREZZI RIVENDITORI
				if($id_group == 22)
				{
					$testriv2 = Db::getInstance()->getValue('SELECT id_specific_price FROM specific_price WHERE specific_price_name = "sc_riv_2" AND id_product = '.$id_product);
					
					if($testriv2 > 0)
						$specific_price = SpecificPrice::getSpecificPrice((int)($id_product), $id_shop, $id_currency, $id_country, 22, $quantity);
					else
					{
						
						$specific_price = SpecificPrice::getSpecificPrice((int)($id_product), $id_shop, $id_currency, $id_country, 3, $quantity);
						
						$prezzotestRIV = $specific_price['price']-($specific_price['price']*$specific_price['reduction']);
						//SE GRUPPO RIVENDITORI2, AGGIUNGO IL 3%
						
						if($prezzotestRIV > 0)
							$trepercentoriv2 = 'y';
					
					}	
			
				}	
			}
			
			//SE NON ESISTE UNO SCONTO RIVENDITORE MA ESISTE UNO SCONTO QUANTITA, PRENDO (PER IL RIVENDITORE) LO SCONTO QUANTITA CLIENTE PIU BASSO
			if(!$prezzotestRIV) {
				$specific_price = SpecificPrice::getSpecificPrice((int)($id_product), $id_shop, $id_currency, $id_country, 1, 99999);
			}
			
			//SE NON ESISTE UNO SCONTO QUANTITA, PRENDO OVVIAMENTE LO SCONTO RIVENDITORE
			if(!$prezzotestCLI) {
				$specific_price = SpecificPrice::getSpecificPrice((int)($id_product), $id_shop, $id_currency, $id_country, $id_group, $quantity);
			
				// SE GRUPPO RIVENDITORI 2, PRENDO PREZZI RIVENDITORI
				if($id_group == 22)
				{
					$testriv2 = Db::getInstance()->getValue('SELECT id_specific_price FROM specific_price WHERE specific_price_name = "sc_riv_2" AND id_product = '.$id_product);
					
					if($testriv2 > 0)
						$specific_price = SpecificPrice::getSpecificPrice((int)($id_product), $id_shop, $id_currency, $id_country, 22, $quantity);
					else
					{
						$specific_price = SpecificPrice::getSpecificPrice((int)($id_product), $id_shop, $id_currency, $id_country, 3, $quantity);
						
						$prezzotestRIV = $specific_price['price']-($specific_price['price']*$specific_price['reduction']);
						//SE GRUPPO RIVENDITORI2, AGGIUNGO IL 3%
						
						if($prezzotestRIV > 0)
							$trepercentoriv2 = 'y';
						
					}	
				}
			}
		
			$prezzofinalequery = ("SELECT price FROM product WHERE id_product = $id_product");
			$rowprezzofinale = Db::getInstance()->getRow($prezzofinalequery);
			
			$prezzofinale = $rowprezzofinale['price'];
			
			//SE NON ESISTONO SCONTI QUANTITA, E IL PREZZO RIVENDITORE RISULTA PIU ALTO RISPETTO AL PREZZO DI VENDITA WEB, PRENDO IL PREZZO DI VENDITA WEB
			if(!$prezzotestCLI && $prezzotestRIV > $prezzofinale) {
			$specific_price = '';
			}
			
			
	
			//SE ESISTE UN PREZZO SPECIALE PER IL CLIENTE E QUESTO RISULTA PIU ALTO DEL PREZZO SCONTATO RIVENDITORE, APPLICO ANCHE AL RIVENDITORE IL PREZZO SPECIALE
			
		
			
			$rann = mysql_query("SELECT * FROM specific_price WHERE id_product = ".$id_product." AND specific_price.to != '0000-00-00 00:00:00' AND specific_price.to > '".date("Y-m-d H:i:s")."' AND (id_group = 1 OR id_group = 0) AND specific_price.specific_price_name = ''"); //con questa query seleziono il prezzo speciale del cliente dando per scontato che al momento ce ne sar? sempre uno
			if(mysql_num_rows($rann) > 0  && ($login_for_offer == 0 || $user_login == true || $cookie->isLogged())) {
			$rannr = mysql_fetch_array($rann);
			$specific_price_fin = $rannr['price']-($rannr['price']*$rannr['reduction']);
			
				//VERIFICO CHE CI SIA ANCORA DISPONIBILITA
				
				$pz_tot = Db::getInstance()->getRow("SELECT stock, pieces FROM specific_price_pieces WHERE id_specific_price = ".$rannr['id_specific_price']."");
				$pz = $pz_tot['stock'];
				$pz_orig = $pz_tot['pieces'];
								
				if(($pz_orig != '') && $pz_orig == 0)
				{
				}
				else
				{
					if($prezzotestRIV > $specific_price_fin) {
						$specific_price = SpecificPrice::getSpecificPrice((int)($id_product), $id_shop, $id_currency, $id_country, 1, $quantity);
					}
				}
			}
			else {
			}

			//SE IL PREZZO RIVENDITORE RISULTA PIU ALTO RISPETTO AL PREZZO DI VENDITA LOGIN FOR OFFER, PRENDO IL PREZZO DI VENDITA LOGIN FOR OFFER
			if($prezzotestRIV > $prezzo_login_for_offer && $login_for_offer == 1 && ($user_login == true || $cookie->isLogged())) {
			$specific_price = '';
			}
			
		}
		// FINE REGOLA DEL PREZZO PIU BASSO PER I RIVENDITORI
		
		
		//SE ESISTE UN PREZZO SPECIALE ED E' PIU ALTO DEL PREZZO UNITARIO, ALLORA APPLICO IL PREZZO UNITARIO
		$unitario = Db::getInstance()->getValue("SELECT price FROM product WHERE id_product = ".$id_product);
		
	
		
				
		if($unitario < Product::trovaPrezzoSpeciale($id_product, $id_group, 1)) {
			$specific_price = '';
		}
		
		
		//CONTROLLLL
		if($specific_price['price'] == 0 && $specific_price['name'] != '') {
			$specific_price = '';
		}
		
		// SE UNITARIO PIU ALTO DEL LOGIN FOR OFFER, PRENDO LOGIN FOR OFFER
		if($unitario> $prezzo_login_for_offer && $login_for_offer == 1 && ($user_login == true || $cookie->isLogged()))
			$unitario = $prezzo_login_for_offer;
		
		if (isset(self::$_prices[$cacheId]))
			return self::$_prices[$cacheId];
		
		
		/*if($login_for_offer == 1 && !$cookie->isLogged())
			$specific_price = '';
		*/
		
		// fetch price & attribute price
		$cacheId2 = $id_product.'-'.$id_product_attribute;
		if (!isset(self::$_pricesLevel2[$cacheId2]))
			self::$_pricesLevel2[$cacheId2] = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT p.`price`,
			'.($id_product_attribute ? 'pa.`price`' : 'IFNULL((SELECT pa.price FROM `'._DB_PREFIX_.'product_attribute` pa WHERE id_product = '.(int)$id_product.' AND default_on = 1), 0)').' AS attribute_price,
			p.`ecotax`
			'.($id_product_attribute ? ', pa.`ecotax` AS attribute_ecotax' : '').'
			FROM `'._DB_PREFIX_.'product` p
			'.($id_product_attribute ? 'LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON pa.`id_product_attribute` = '.(int)($id_product_attribute) : '').'
			WHERE p.`id_product` = '.(int)$id_product);
		$result = self::$_pricesLevel2[$cacheId2];
		if($result['price'] > $prezzo_login_for_offer && ( !$specific_price['price'] || $specific_price['price'] == 0 || ($specific_price['price'] > 0 && ( $prezzo_login_for_offer < ($specific_price['price'] - $specific_price['amount'])  && $prezzo_login_for_offer < ($specific_price['price'] - ($specific_price['price'] * $specific_price['reduction']))	) ))
		&& $login_for_offer == 1 && ($user_login == true ||$cookie->isLogged())) {	
			$specific_price['price'] = $prezzo_login_for_offer;
			$specific_price['reduction'] = 0;
			$specific_price['amount'] = 0;
			$price = $prezzo_login_for_offer;
			
		}
	    if (!$specific_price || $specific_price['price'] == 0) {	
			  $price = (float)$result['price'];
				
		}	  
	    else {
			$pzs = Db::getInstance()->getValue("SELECT pieces FROM specific_price_pieces WHERE id_specific_price = ".$specific_price['id_specific_price']." AND specific_price_name = ''");
			
			if($pzs == 0 && $pzs != '' && $specific_price['specific_price_name'] == '')
				$price = (float)$result['price'];
			else
				$price = (float)$specific_price['price'];
			
		}
	
		// convert only if the specific price is in the default currency (id_currency = 0)
		if (!$specific_price OR !($specific_price['price'] > 0 AND $specific_price['id_currency']))
			$price = Tools::convertPrice($price, $id_currency);

		// Attribute price
		$attribute_price = Tools::convertPrice(array_key_exists('attribute_price', $result) ? (float)($result['attribute_price']) : 0, $id_currency);
		if ($id_product_attribute !== false) // If you want the default combination, please use NULL value instead
			$price += $attribute_price;

		// TaxRate calculation
		$tax_rate = Tax::getProductTaxRateViaRules((int)$id_product, (int)$id_country, (int)$id_state, (int)$id_county);
		if ($tax_rate === false)
			$tax_rate = 0;

		

		// Reduction
		$reduc = 0;
		if (($only_reduc OR $use_reduc) AND $specific_price)
		{
			if ($specific_price['reduction_type'] == 'amount')
			{
				$reduction_amount = $specific_price['reduction'];

				if (!$specific_price['id_currency'])
					$reduction_amount = Tools::convertPrice($reduction_amount, $id_currency);
				$reduc = Tools::ps_round(!$use_tax ? $reduction_amount / (1 + $tax_rate / 100) : $reduction_amount, $decimals);
			}
			else
				$reduc = Tools::ps_round($price * $specific_price['reduction'], $decimals);
		}

		if ($only_reduc)
			return $reduc;
		if ($use_reduc)
		{	
			$price -= $reduc;
			if($id_group == 22 && $trepercentoriv2 == 'y')
			{	
				$price = $price + ($price / 100 * 3);
				
				if($price > Product::trovaMigliorPrezzo($id_product,1,99999))
					$price = Product::trovaMigliorPrezzo($id_product,1,99999);
			
				if($specific_price_fin && $price > $specific_price_fin)
					$price = $specific_price_fin;
			}
		}
		

		// Group reduction
		if ($use_groupReduction)
		{
			if ($reductionFromCategory = (float)(GroupReduction::getValueForProduct($id_product, $id_group)))
				$price -= $price * $reductionFromCategory;
			else // apply group reduction if there is no group reduction for this category
				$price *= ((100 - Group::getReductionByIdGroup($id_group)) / 100);
		}

		// Add Tax
		if ($use_tax)
			$price = $price * (1 + ($tax_rate / 100));
		//$price = Tools::ps_round($price, $decimals);
		
		$price = Tools::ps_round($price, $decimals);
		// Eco Tax
		if (($result['ecotax'] OR isset($result['attribute_ecotax'])) AND $with_ecotax)
		{
			$ecotax = $result['ecotax'];
			if (isset($result['attribute_ecotax']) && $result['attribute_ecotax'] > 0)
				$ecotax = $result['attribute_ecotax'];

			if ($id_currency)
				$ecotax = Tools::convertPrice($ecotax, $id_currency);
			if ($use_tax)
			{
				$taxRate = TaxRulesGroup::getTaxesRate((int)Configuration::get('PS_ECOTAX_TAX_RULES_GROUP_ID'), (int)$id_country, (int)$id_state, (int)$id_county);
				$price += $ecotax * (1 + ($taxRate / 100));
			}
			else
				$price += $ecotax;
		}
		$price = Tools::ps_round($price, $decimals);
		if ($price < 0)
			$price = 0;

		self::$_prices[$cacheId] = $price;
		return self::$_prices[$cacheId];
	}

	
	//funzione per aggiungere i tag in automatico 
	public static function addTagAuto($id_prodotto, $id_lang) 
	{
		$tags = array();

		$tagq = Db::getInstance()->executeS("SELECT name FROM tag WHERE id_lang = $id_lang") ;
		foreach($tagq as $rowtag) {
			$tags[] = $rowtag['name'];
		}

		echo "<br /><br /><br /><br /><br />";
		
		//Cerco il mio prodotto
		$query = "SELECT product.id_product AS id, product_lang.name AS prodotto, product.reference AS codice, category_lang.name AS categoria, manufacturer.name AS produttore, category.id_category FROM product JOIN product_lang ON product.id_product = product_lang.id_product JOIN manufacturer ON product.id_manufacturer = manufacturer.id_manufacturer JOIN category_product ON product_lang.id_product = category_product.id_product JOIN category_lang ON category_product.id_category = category_lang.id_category JOIN category ON category_product.id_category = category.id_category WHERE product.id_product = $id_prodotto AND product_lang.id_lang = $id_lang";

		$rows = Db::getInstance()->executeS($query);

		foreach($rows as $row) {
			$id_prodotto = (int)$id_prodotto;
			
			//Scompongo il nome del prodotto
			$arrprodotto = explode(" ",$row['prodotto']);
			$stringa = "";

			//Per ogni parola del prodotto, controllo che non sia gi? nei tag
			foreach($arrprodotto as $elemento) {


				if (strlen($elemento) > 3 && !is_numeric($elemento)) {
				
					//Se la parola ? gi? nei tag non faccio niente
					if(in_array(ucwords($elemento), $tags)) {


					}
					
					//Altrimenti la inserisco ex novo nella tabella dei tag
					else {
						Db::getInstance()->executeS("REPLACE INTO tag (id_tag, id_lang, name) VALUES ('NULL', 5, '".addslashes(ucwords($elemento))."')") ;

						$tags[] = ucwords($elemento);

						$last_id = mysql_insert_id() ;

					}

					//Seleziono quindi l'elemento della tabella tag che corrisponde alla parola che sto percorrendo nel ciclo foreach
					$qx = ("SELECT id_tag FROM tag WHERE name =  '".addslashes(ucwords($elemento))."'");
					$rowx = Db::getInstance()->getRow($qx);
					
					//... e quindi la associo come tag del prodotto
					Db::getInstance()->executeS("INSERT IGNORE INTO product_tag (id_product, id_tag) VALUES ('".$id_prodotto."', '".$rowx['id_tag']."')") ;


					//Semplicemente per visualizzare
					$stringa.= $elemento." - ";
				}
				else { }


			}

			//Controllo che il produttore non sia gi? nei tag
			if(in_array(ucwords($row['produttore']), $tags)) {


			}
			
			//Se non c'?, lo aggiungo alla tabella tag nel database
			else {
				Db::getInstance()->executeS("REPLACE INTO tag (id_tag, id_lang, name) VALUES ('NULL', 5, '".addslashes(ucwords($row['produttore']))."')") ;
		
				$tags[] = ucwords($row['produttore']);

			}

			//Seleziono quindi l'elemento della tabella tag che corrisponde al produttore
			$qx = ("SELECT id_tag FROM tag WHERE name =  '".addslashes(ucwords($row['produttore']))."'") ;
			$rowx = Db::getInstance()->getRow($qx) ;
			
			//... e quindi lo associo come tag del prodotto
			Db::getInstance()->executeS("INSERT IGNORE INTO product_tag (id_product, id_tag) VALUES ($id_prodotto, $rowx[id_tag])") ;


			//da questa riga fino al prossimo commento, faccio la stessa cosa per la categoria
			if(in_array(ucwords($row['categoria']), $tags)) {


			}
			else {
				
				$active_category = Db::getInstance()->getValue('SELECT active FROM category WHERE id_category = '.$row['category']);
				if($active_category == 1) 
				{	
					Db::getInstance()->executeS("REPLACE INTO tag (id_tag, id_lang, name) VALUES ('NULL', 5, '".addslashes(ucwords($row['categoria']))."')") ;

					$tags[] = ucwords($row['categoria']);
				}

			}
			$qx = ("SELECT id_tag FROM tag WHERE name =  '".addslashes(ucwords($row['categoria']))."'") ;
			$rowx = Db::getInstance()->getRow($qx);
			Db::getInstance()->executeS("INSERT IGNORE INTO product_tag (id_product, id_tag) VALUES ($id_prodotto, $rowx[id_tag])") ;


			//Aggiungo categoria, codice e produttore alla stringa
			$stringa.= " - ".$row['categoria']." - ".$row['produttore']." - ".$row['codice']." - ";

			$array_delle_specifiche = array(778,779,781,783,786,785,789,788,776,775,429,793,791,811,795,799,797,798,801,802,805,804,807);
		
			foreach($array_delle_specifiche as $specifica) {
			
				//Cerco tutti i prodotti che hanno la specifica "Ottimizzata per"
				$query4 = "SELECT feature_value_lang.value FROM feature_value_lang JOIN feature_product ON feature_value_lang.id_feature_value = feature_product.id_feature_value JOIN feature_value ON feature_value.id_feature_value = feature_value_lang.id_feature_value WHERE feature_product.id_product = $id_prodotto AND feature_value_lang.id_lang = $id_lang AND feature_value.id_feature = 431";
				
				$row4 = Db::getInstance()->getRow($query4);

				//Se il valore della specifica ? gi? presente nei tag, non faccio niente
				if(in_array(ucwords($row4['value']), $tags)) {
			

				}
			
				//Altrimenti lo inserisco nella tabella dei tag
				else {
					Db::getInstance()->executeS("REPLACE INTO tag (id_tag, id_lang, name) VALUES ('NULL', 5, '".addslashes(ucwords($row4['value']))."')") ;

					$tags[] = ucwords($row4['value']);

				}
			
				//Seleziono quindi il tag corrispondente al valore della specifica "Ottimizzata per"
				$qx = ("SELECT id_tag FROM tag WHERE name =  '".addslashes(ucwords($row4['value']))."'") ;
				$rowx = Db::getInstance()->getRow($qx);
			
				//... e lo associo al prodotto
				Db::getInstance()->executeS("INSERT IGNORE INTO product_tag (id_product, id_tag) VALUES ($id_prodotto, $rowx[id_tag])") ;

				//$stringa.= " - ".$row4['value'];

			}
			
			//Seleziono il valore della specifica Compatibilit? Skype
			$query6 = "SELECT feature_value_lang.value FROM feature_value_lang JOIN feature_product ON feature_value_lang.id_feature_value = feature_product.id_feature_value JOIN feature_value ON feature_value.id_feature_value = feature_value_lang.id_feature_value WHERE feature_product.id_product = $id_prodotto AND feature_value_lang.id_lang = $id_lang AND feature_value.id_feature = 771";
			
			$row6 = Db::getInstance()->getRow($query6);

			//Se il valore ? "S?", allora creo una variabile di tipo stringa chiamandola "Compatibile Skype" 
			if($row6['value'] == 's?') {
				$vv = "Compatibile Skype";
		
				//Se quest'ultima è già nel db non faccio niente
				if(in_array("Compatibile Skype", $tags)) {
				}
				//Altrimenti la inserisco nella tabella tag
				else {
					Db::getInstance()->executeS("REPLACE INTO tag (id_tag, id_lang, name) VALUES ('NULL', 5, 'Compatibile Skype')") ;
					$tags[] = "Compatibile Skype";
				}
				
				//Quindi cerco il tag che si chiama "Compatibile Skype" e gli associo il prodotto
				$qx = ("
					SELECT id_tag 
					FROM tag 
					WHERE name =  'Compatibile Skype'
				");
				$rowx = Db::getInstance()->getRow($qx);
				Db::getInstance()->executeS("INSERT IGNORE INTO product_tag (id_product, id_tag) VALUES ($id_prodotto, $rowx[id_tag])");



			}
			
			//Se il valore non c'? lascio la stringa vuota
			else {
			$vv = "";
			}


			$stringa.= " - ".$vv;


			//Ripeto la stessa operazione per i prodotti compatibili con Mac
			$query7 = "SELECT feature_value_lang.value FROM feature_value_lang JOIN feature_product ON feature_value_lang.id_feature_value = feature_product.id_feature_value JOIN feature_value ON feature_value.id_feature_value = feature_value_lang.id_feature_value WHERE feature_product.id_product = $id_prodotto AND feature_value_lang.id_lang = $id_lang AND feature_value.id_feature = 763";
			
			$row7 = Db::getInstance()->getRow($query7);

			if($row7['value'] == 's?') {
				$xx = "Compatibile Mac";

				if(in_array("Compatibile Mac", $tags)) {


				}
				else {
					Db::getInstance()->executeS("REPLACE INTO tag (id_tag, id_lang, name) VALUES ('NULL', 5, 'Compatibile Mac')") ;

					$tags[] = "Compatibile Mac";

				}
				$qx = ("SELECT id_tag FROM tag WHERE name =  'Compatibile Mac'") ;
				$rowx = Db::getInstance()->getRow($qx);
				Db::getInstance()->executeS("INSERT IGNORE INTO product_tag (id_product, id_tag) VALUES ($id_prodotto, $rowx[id_tag])");
			}
			else {
				$xx = "";
			}

			$stringa.= " - ".$xx;
			echo $stringa;
			echo "<br />";

		}

		//E per finire cancello tutti i tag vuoti e tutte le associazioni tra prodotti e tag vuoti.
		mysql_query("
			DELETE product_tag.*, tag.* 
			FROM tag 
			JOIN product_tag ON tag.id_tag = product_tag.id_tag 
			WHERE tag.name = ''
		");
	}
	
	public function addFeaturesToDB($id_feature, $id_value, $cust = 0, $val = '')
	{
		if($id_feature == 771 && $id_value = 3436) {
			mysql_query("REPLACE INTO category_product (id_product, id_category) VALUES ($this->id, 103)");
		}
	
		if ($cust)
		{
			$id_value = Db::getInstance()->getValue('SELECT fvl.id_feature_value FROM feature_value_lang fvl JOIN feature_value fv ON fvl.id_feature_value = fv.id_feature_value WHERE value = "'.$val.'"');
			
			if(!$id_value || $id_value == 0)
			{
				$row = array('id_feature' => (int)($id_feature), 'custom' => 0);
				Db::getInstance()->autoExecute(_DB_PREFIX_.'feature_value', $row, 'INSERT');
				$id_value = Db::getInstance()->Insert_ID();
			}
		}
		$row = array('id_feature' => (int)($id_feature), 'id_product' => (int)($this->id), 'id_feature_value' => (int)($id_value));
		Db::getInstance()->autoExecute(_DB_PREFIX_.'feature_product', $row, 'INSERT');
		if ($id_value)
			return ($id_value);
	}
	
	/**
	* Get product accessories (only names)
	*
	* @param integer $id_lang Language id
	* @param integer $id_product Product id
	* @return array Product accessories
	*/
	public static function getAccessoriesLight($id_lang, $id_product)
	{
		return Db::getInstance()->ExecuteS('
			SELECT p.`id_product`, p.`reference`, pl.`name`
			FROM `'._DB_PREFIX_.'accessory`
			LEFT JOIN `'._DB_PREFIX_.'product` p ON (p.`id_product`= `id_product_2`)
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')
			WHERE p.price > 0 
				AND p.active = 1 
				AND `id_product_1` = '.(int)($id_product)
		);
	}
	
	/**
	* Get product accessories (only names)
	*
	* @param integer $id_lang Language id
	* @param integer $id_product Product id
	* @return array Product accessories
	*/
	public static function getAccessoriesLightAll($id_lang, $id_product)
	{
		return Db::getInstance()->ExecuteS('
			SELECT id_product_2 `id_product`, p.`reference`, pl.`name`
			FROM `'._DB_PREFIX_.'accessory`
			LEFT JOIN `'._DB_PREFIX_.'product` p ON (p.`id_product`= `id_product_2`)
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')
			WHERE id_product_2 != 0 
				AND `id_product_1` = '.(int)($id_product)
		);
	}
	
	public static function getAlternativesLightAll($id_lang, $id_product)
	{
		return Db::getInstance()->ExecuteS('
			SELECT id_product_2 `id_product`, p.`reference`, pl.`name`
			FROM `'._DB_PREFIX_.'alternative`
			LEFT JOIN `'._DB_PREFIX_.'product` p ON (p.`id_product`= `id_product_2`)
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')
			WHERE id_product_2 != 0 
				AND `id_product_1` = '.(int)($id_product)
		);
	}

	/**
	* Get product accessories
	*
	* @param integer $id_lang Language id
	* @return array Product accessories
	*/
	public function getAccessories($id_lang, $active = true)
	{
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT p.*, pl.`description`, pl.`description_short`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`cat_homepage`, pl.`desc_homepage`, pl.`meta_title`, pl.`name`, p.`ean13`, p.`upc`,
			i.`id_image`, il.`legend`, t.`rate`, m.`name` as manufacturer_name, cl.`name` AS category_default, DATEDIFF(p.`date_add`, DATE_SUB(NOW(),
			INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY)) > 0 AS new
			FROM `'._DB_PREFIX_.'accessory`
			LEFT JOIN `'._DB_PREFIX_.'product` p ON p.`id_product` = `id_product_2`
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')
			LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (p.`id_category_default` = cl.`id_category` AND cl.`id_lang` = '.(int)($id_lang).')
			LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
			LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)($id_lang).')
			LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (p.`id_manufacturer`= m.`id_manufacturer`)
			LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (p.`id_tax_rules_group` = tr.`id_tax_rules_group`
														AND tr.`id_country` = '.(int)Country::getDefaultCountryId().'
														AND tr.`id_state` = 0)
			LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
			WHERE p.price > 0 
				AND `id_product_1` = '.(int)($this->id).'
				'.($active ? 'AND p.`active` = 1' : 'AND p.`active` = 1')
		);
		
		if (!$result)
			return false;

		return $this->getProductsProperties($id_lang, $result);
	}
	
		/**
	 * check if a product can be deleted (in a non-valid order)
	 * @return boolean 
	 */
	protected function isDeletable()
	{
		$is_deletable = Db::getInstance()->getValue('
		SELECT COUNT(`product_id`) as `nb_product`
		FROM `'._DB_PREFIX_.'order_detail` od
		LEFT JOIN `'._DB_PREFIX_.'orders` o
			ON (o.`id_order` = od.`id_order`)
		WHERE `valid` = 0
		AND o.id_order NOT IN (SELECT id_order FROM order_history WHERE id_order_state = 6 OR id_order_state = 8)
		AND `product_id` = '.(int)$this->id.'
		');

		return !(bool)$is_deletable;
	}
	
	public function getBundles($id_product)
	{
		//Tutti i bundle attivi, anche quelli eventualmente con prezzi a zero
		$bundles = Db::getInstance()->executeS("SELECT b.*, bi.id_image FROM bundle b JOIN bundle_image bi ON b.id_bundle = bi.id_bundle WHERE b.active = 1 AND b.father = ".$id_product." ORDER BY b.sort_order ASC");
		
		//CONTROLLO per evitare bundle a prezzo zero
		foreach($bundles as $bundle)
			Product::getBundlesPrices($bundle['id_bundle']);
	
		//Riprendo i bundle veramente attivi
		$bundles = Db::getInstance()->executeS("SELECT b.*, bi.id_image FROM bundle b JOIN bundle_image bi ON b.id_bundle = bi.id_bundle WHERE b.active = 1 AND b.father = ".$id_product." ORDER BY b.sort_order ASC");
		
		return $bundles;

	}
	
	public function getBundlesDisplay($id_bundle)
	{
		global $cookie;

		$bundle = Db::getInstance()->getRow("SELECT * FROM bundle WHERE id_bundle = ".$id_bundle." AND active = 1");
		
		Product::getBundlesPrices($id_bundle);
		
		$bundle_products_row = explode(":", $bundle['bundle_prices']);
		$bundle_string = "";
		$bundle_string .= "<td style='position:relative; vertical-align:middle'>";
		$i_n = 0;
		foreach($bundle_products_row as $bundle_row) {
						
			$idprz = explode(";", $bundle_row);
					
						
			$nomeprod = Db::getInstance()->getRow("SELECT i.id_image, pl.id_product, pl.name, pl.link_rewrite, p.reference, p.ean13, p.id_category_default, p.active FROM product_lang pl LEFT JOIN image i ON pl.id_product = i.id_product JOIN product p ON pl.id_product = p.id_product WHERE pl.id_product = ".$idprz[0]." AND pl.id_lang = ". $cookie->id_lang." AND i.cover = 1");
						
			if(empty($nomeprod)) {
			}
			else {
				if($i_n == 6) 
				{
					$bundle_string .= "</td></tr><td style='position:relative; vertical-align:middle'>";
				
				}
				$bundle_string .= ($i_n == 0 || $i_n == 6 ? "" : "<img style='border:0px' src='"._PS_IMG_."/p_bundle.png' alt='' title='' width='25' height='32' /></td><td style='position:relative; vertical-align:middle'>");
			
				$bundle_string .= "<span class='quantity-bundle'><strong>".$idprz[1]."x</strong></span>";
				
				$customer_group = $cookie->id_default_group;
				if($customer_group == 3  || $customer_group == 10 || $customer_group == 12) {
				
					if((float)(str_replace(",",".",$idprz[3])) == '100.00') {
						$bundle_string .= "<span class='omaggio-bundle'><strong>Omaggio</strong></span>";
					}
					else {
					}
				}
				else if($customer_group == 1) {
				
					if((float)(str_replace(",",".",$idprz[2])) == '100.00') {
						$bundle_string .= "<span class='omaggio-bundle'><strong>Omaggio</strong></span>";
					}
					else {
					}
				}
				else {
					if((float)(str_replace(",",".",$idprz[2])) == '100.00') {
						$bundle_string .= "<span class='omaggio-bundle'><strong>Omaggio</strong></span>";
					}
					else {
					}
				}
				$id_lang = $cookie->id_lang;
				$category_rewrite = Category::getLinkRewrite($nomeprod['id_category_default'], $id_lang);
				$link = new Link();
				$product_link = $link->getProductLink($nomeprod['id_product'], $nomeprod['link_rewrite'], $category_rewrite, $nomeprod['ean13']);
				if($nomeprod['active'] == 0)
					$product_link = '#';
		
				$bundle_image = Db::getInstance()->getValue("SELECT id_image FROM bundle_image WHERE id_bundle = ".$id_bundle."");
				
				$bundle_string .= "<a href='".$product_link."' target='_blank'>
				
				<img class='product_img_link' style='border:0px' src='https://www.ezdirect.it/img/p/".$nomeprod['id_product']."-".$nomeprod['id_image']."-80x80.jpg' alt='".$nomeprod['name']."' title='".$nomeprod['name']."' width='60' height='60' /></a></td><td style='position:relative; vertical-align:middle'>";
				
			}
						
			$i_n++;
		}
					
		$bundle_string .= "</td>";

			
		$bundle_string .= "</tr>";
		
		return $bundle_string;

	}
	
	public function getQuantityInBundle($id_product, $id_bundle)
	{
		$bundle_prices = Db::getInstance()->getValue("
			SELECT bundle_prices 
			FROM bundle 
			WHERE id_bundle = ".$id_bundle."
		");

		$dati = explode(":", $bundle_prices);
	
		foreach($dati as $data_col) {
			$dati_singoli = explode(";", $data_col);

			if($dati_singoli[0] == $id_product) {
				$quantity_in_bundle = $dati_singoli[1];
				return $quantity_in_bundle;
			}
		}
		
		return 0;		
	}
	
	public function isProductInBundle($id_product) 
	{
		$father = Db::getInstance()->getValue("SELECT count(father) FROM bundle WHERE father = ".$id_product."");

		if($father > 1) {
			return true;				
		}
		else {		
			$bundles = Db::getInstance()->executeS("SELECT bundle_products FROM bundle");
			
			foreach($bundles as $bundle) {

				$prodotti = explode(";",$bundle['bundle_products']);
				foreach ($prodotti as $prodotto) {
					if($prodotto == $id_product)						
						return true;						
				}					
			}	
		}
	
		return false;
	}
		
	public function getHighestQuantity($id_product) {
		
		$qts = Db::getInstance()->executeS("
			SELECT from_quantity 
			FROM specific_price 
			WHERE id_product = ".$id_product." 
				AND (specific_price_name = 'sc_qta_1' OR specific_price_name = 'sc_qta_2' OR specific_price_name = 'sc_qta_3')
		");
		
		$highest_quantity = 1;
		
		foreach($qts as $qt) {
		
			if($qt['from_quantity'] > $highest_quantity) {
				$highest_quantity = $qt['from_quantity'];
			}
		}

		return $highest_quantity;
	}
	
	public function getBundlesPrices($id_bundle)
	{
		global $cookie;

		$bundle = Db::getInstance()->getRow("SELECT * FROM bundle WHERE id_bundle = ".$id_bundle."");
		
		$prezzi_bundle = array();
		$prezzi_bundle_riv = array();
		
		$prezzi_mostrati = array();
		$prezzi_normale = array();
		$prezzi_normale_riv = array();	
		
		$prezzi_cliente_qta = array();
		$prezzi_riv_qta = array();
		
		$sc_qts_cli = array();
	
		$highest_qta_cli = 1;
		
		$bundle_products_row = explode(":", $bundle['bundle_prices']);
					
		$i_n = 0;

		foreach($bundle_products_row as $bundle_row) {
						
			$idprz = explode(";", $bundle_row);
			
			$i_n++;
			
			$prezzo_normale_cliente = Db::getInstance()->getValue("
				SELECT scontolistinovendita
				 FROM product 
				 WHERE id_product = ".$idprz[0]."
			");
	
			if($prezzo_normale_cliente == 100 && !empty($prezzo_normale_cliente))
				Db::getInstance()->executeS('
					UPDATE bundle 
					SET active = 0 
					WHERE id_bundle = '.$id_bundle
			);

			$prezzi_normale[] = $prezzo_normale_cliente*$idprz[1];
			$prezzi_normale_riv[] = Product::calcolaPrezzoRivenditore($idprz[0])*$idprz[1];

			$prezzo_bundle_cliente = Product::trovaMigliorPrezzo($idprz[0],1,$idprz[1]);
			$prezzo_bundle_cliente_qta = Product::trovaMigliorPrezzo($idprz[0],1,99999);
			
			if(Product::checkPrezzoSpeciale($idprz[0],1,$idprz[1]) == 'yes' && str_replace(",",".",$idprz[2]) != '100.00') {
				$speciale = Product::trovaPrezzoSpeciale($idprz[0],1,$idprz[1]);
				
				if($speciale < $prezzo_bundle_cliente) {
					$prezzo_bundle_cliente = $speciale;
				}

				$prezzo_bundle_cliente = round($prezzo_bundle_cliente - (($prezzo_bundle_cliente * (str_replace(",",".",$idprz[2])))/100),2);
			}
			else {
				$prezzo_bundle_cliente = round($prezzo_bundle_cliente - (($prezzo_bundle_cliente * (str_replace(",",".",$idprz[2])))/100),2);
			}
			
			if($prezzo_bundle_cliente < $prezzo_bundle_cliente_qta) {
				$prezzo_bundle_cliente_qta = $prezzo_bundle_cliente;
				$sc_qts_cli[] = 1;
			}
			else {
				if($idprz[1] > 0)
					$sc_qts_cli[] = round((Product::getHighestQuantity($idprz[0])/$idprz[1]),0);
				else
					$sc_qts_cli[] = 0;
			}
			
			$prezzo_bundle_riv = Product::trovaMigliorPrezzo($idprz[0],3,$idprz[1]);
			$prezzo_bundle_riv_qta = Product::trovaMigliorPrezzo($idprz[0],3,99999);

			if(Product::checkPrezzoSpeciale($idprz[0],3,$idprz[1]) == 'yes' && str_replace(",",".",$idprz[2]) != '100.00') {
				$prezzo_bundle_riv = round($prezzo_bundle_riv - (($prezzo_bundle_riv * (str_replace(",",".",$idprz[3])))/100),2);		
			}
			else {
				$prezzo_bundle_riv = round($prezzo_bundle_riv - (($prezzo_bundle_riv * (str_replace(",",".",$idprz[3])))/100),2);
			}
			
			if($prezzo_bundle_riv < $prezzo_bundle_riv_qta) {
				$prezzo_bundle_riv_qta = $prezzo_bundle_riv_cliente;
			}
			
			$prezzi_bundle[] = (float)(str_replace(",",".",$prezzo_bundle_cliente))*$idprz[1];
			$prezzi_bundle_riv[] = (float)(str_replace(",",".",$prezzo_bundle_riv))*$idprz[1];
			
			$prezzi_cliente_qta[] = (float)(str_replace(",",".",$prezzo_bundle_cliente_qta))*$idprz[1];
			$prezzi_bundle_riv_qta[] = (float)(str_replace(",",".",$prezzo_bundle_riv_qta))*$idprz[1];
		}		
		
		$prezzo_tot = 0;

		foreach($prezzi_bundle as $prezzo_bundle) {
			$prz_u = (float)(str_replace(",",".",$prezzo_bundle))." - ";
			$prezzo_tot += $prz_u;
		}
						
		$prezzo_tot_riv = 0;

		foreach($prezzi_bundle_riv as $prezzo_bundle_riv) {
			$prz_u_riv = (float)(str_replace(",",".",$prezzo_bundle_riv))." - ";
			$prezzo_tot_riv += $prz_u_riv;
		}
			
		$prezzo_normale_tot = 0;
		$prezzo_normale_riv_tot = 0;
		
		foreach($prezzi_normale as $prz_n) {
			$prezzo_normale_tot += $prz_n;
		}
		
		foreach($prezzi_normale_riv as $prz_n_riv) {
			$prezzo_normale_riv_tot += $prz_n_riv;
		}
		
		$prezzo_cliente_qta = 0;
		$prezzo_riv_qta = 0;
		
		foreach($prezzi_cliente_qta as $prz_q) {
			$prezzo_cliente_qta += $prz_q;
		}
		
		foreach($prezzi_riv_qta as $prz_q_riv) {
			$prezzo_riv_qta += $prz_q_riv;
		}
		
		if(($prezzo_tot_riv > $prezzo_tot)||($prezzo_tot_riv > $prezzo_cliente_qta)) {
			$prezzo_tot_riv = $prezzo_tot;
		}

		/*if($prezzo_tot_riv > $prezzo_cliente_qta) {
			$prezzo_tot_riv = $prezzo_tot;
		}*/
		
		Db::getInstance()->executeS("
			UPDATE bundle 
			SET priceCLI = ".$prezzo_tot.", priceRIV = ".$prezzo_tot_riv." 
			WHERE id_bundle = ".$id_bundle."
		");
		
		$group = Customer::getDefaultGroupId($cookie->id_customer);
		$prezzo_finito = ($group == 3 || $group == 10 || $group == 12 ? $prezzo_tot_riv : $prezzo_tot);		
		$prezzo_normale = ($group == 3 || $group == 10 || $group == 12 ? $prezzo_normale_riv_tot : $prezzo_normale_tot);		
		$prezzo_sc_qta = ($group == 3 || $group == 10 || $group == 12 ? ($prezzo_tot_riv > $prezzo_cliente_qta ? $prezzo_cliente_qta : $prezzo_tot_riv) : $prezzo_cliente_qta);	

		foreach($sc_qts_cli as $qt) {
			
			if($qt > $highest_qta_cli) {
				$highest_qta_cli = $qt;
			}
		}		
		
		$category_father = Db::getInstance()->getValue("
			SELECT id_category_default 
			FROM product 
			WHERE id_product = ".$bundle['father']."
		");

		$macro_category = Category::getMacroCategory($category_father);
		
		$prezzi_mostrati[] = round($prezzo_finito,2);
		$prezzi_mostrati[] = round($prezzo_normale,2);

		if($macro_category == 23) {
			$prezzi_mostrati[] = round($prezzo_finito,2);
		}
		else {
			$prezzi_mostrati[] = round($prezzo_sc_qta,2);
		}

		$prezzi_mostrati[] = $highest_qta_cli;
		
		return $prezzi_mostrati;
	}
	
	
	public function calcolaPrezzoRivenditore($id_product) {
	
		$qtas = Db::getInstance()->executeS("
			SELECT price, reduction, specific_price_name 
			FROM specific_price 
			WHERE (specific_price_name = 'sc_qta_1' OR specific_price_name = 'sc_qta_2' OR specific_price_name = 'sc_qta_3' OR specific_price_name = 'sc_riv_1' OR specific_price_name = 'sc_riv_2' OR specific_price_name = 'sc_riv_3') 
				AND id_product = '".$id_product."'
		");
		
		foreach($qtas as $qta) {
			if($qta['specific_price_name'] == 'sc_qta_1')
				$sc_qta_1_r = $qta;
			else if($qta['specific_price_name'] == 'sc_qta_2')
				$sc_qta_2_r = $qta;
			else if($qta['specific_price_name'] == 'sc_qta_3')
				$sc_qta_3_r = $qta;
			else if($qta['specific_price_name'] == 'sc_riv_1')
				$sc_riv_1_r = $qta;
			else if($qta['specific_price_name'] == 'sc_riv_2')
				$sc_riv_2_r = $qta;
			else if($qta['specific_price_name'] == 'sc_riv_3')
				$sc_riv_3_r = $qta;
		}
					
		$prezzoriv = Db::getInstance()->getValue("SELECT price FROM product WHERE id_product = ".$id_product."");
		$confronto_prezzi = array($sc_riv_1, $sc_qta_1, $sc_qta_2, $sc_qta_3);

		foreach($confronto_prezzi as $prz) {
			if($prz != 0 && $prz < $prezzoriv) {
				$prezzoriv = $prz;
			}
		}
	
		return $prezzoriv;
	}
	
	function trovaMigliorPrezzoAcquisto($id_product, $id_group, $quantity = 0, $abilita_speciale = 'y') {
		
		$wholesale = Db::getInstance()->getValue("
			SELECT wholesale_price 
			FROM product 
			WHERE id_product = '".$id_product."'
		");

		$listino = Db::getInstance()->getValue("
			SELECT listino 
			FROM product 
			WHERE id_product = '".$id_product."'
		");

		$special = Db::getInstance()->getRow("
			SELECT * 
			FROM specific_price_wholesale spw 
			WHERE spw.id_product = '".$id_product."' 
				AND spw.from < '".date('Y-m-d H:i:s')."' 
				AND spw.to > '".date('Y-m-d H:i:s')."' 
				AND (spw.pieces = '' OR spw.pieces > 0)
		");
		
		if($special['wholesale_price'] && $special['wholesale_price'] < $wholesale && ($special['wholesale_price'] != 0 || ($special['wholesale_price'] == 0 && $special['reduction_1'] == 100)))
			return $special['wholesale_price'];
		else
			return $wholesale;
	}	
	
	
	function trovaMigliorPrezzo($id_product, $id_group, $quantity = 0, $abilita_speciale = 'y', $user_login = false) {
		
		global $cookie;
	
		$unitario = Db::getInstance()->getValue("SELECT price FROM product WHERE id_product = '".$id_product."'");
		$speciale = Product::trovaPrezzoSpeciale($id_product, $id_group, $quantity);
		$acquisto = Db::getInstance()->getValue("SELECT wholesale_price FROM product WHERE id_product = '".$id_product."'");
		$login_for_offer = Db::getInstance()->getValue("SELECT login_for_offer FROM product WHERE id_product = '".$id_product."'");
		$margin_login_for_offer = Db::getInstance()->getValue("SELECT margin_login_for_offer FROM product WHERE id_product = '".$id_product."'");
		
		$prezzo_login_for_offer = (($acquisto * 100) / (100 - $margin_login_for_offer));
		
		if($abilita_speciale == 'y') {
			if($speciale != 0) {
				if($unitario > $speciale) {
					$unitario = $speciale;
				}
			}
		}
		
		$qtas = Db::getInstance()->executeS("SELECT price, reduction, specific_price_name, from_quantity FROM specific_price WHERE (specific_price_name = 'sc_qta_1' OR specific_price_name = 'sc_qta_2' OR specific_price_name = 'sc_qta_3' OR specific_price_name = 'sc_riv_1' OR specific_price_name = 'sc_riv_2' OR specific_price_name = 'sc_riv_3') AND id_product = '".$id_product."'");
		
		foreach($qtas as $qta) {
			if($qta['specific_price_name'] == 'sc_qta_1'){	
				$sc_qta_1_r = $qta;
				$sc_qta_1_q = $qta['from_quantity'];
			}
			else if($qta['specific_price_name'] == 'sc_qta_2'){
				$sc_qta_2_r = $qta;
				$sc_qta_2_q = $qta['from_quantity'];
			}
			else if($qta['specific_price_name'] == 'sc_qta_3'){
				$sc_qta_3_r = $qta;
				$sc_qta_3_q = $qta['from_quantity'];
			}
			else if($qta['specific_price_name'] == 'sc_riv_1'){
				$sc_riv_1_r = $qta;
			}
			else if($qta['specific_price_name'] == 'sc_riv_2'){
				$sc_riv_2_r = $qta;
			}
			else if($qta['specific_price_name'] == 'sc_riv_3'){
				$sc_riv_3_r = $qta;
			}
		}
			
		$sc_qta_1 = $sc_qta_1_r['price'] - ($sc_qta_1_r['price'] * $sc_qta_1_r['reduction']);
		$sc_qta_2 = $sc_qta_2_r['price'] - ($sc_qta_2_r['price'] * $sc_qta_2_r['reduction']);
		$sc_qta_3 = $sc_qta_3_r['price'] - ($sc_qta_3_r['price'] * $sc_qta_3_r['reduction']);
		$sc_riv_1 = $sc_riv_1_r['price'] - ($sc_riv_1_r['price'] * $sc_riv_1_r['reduction']);
		$sc_riv_2 = $sc_riv_2_r['price'] - ($sc_riv_2_r['price'] * $sc_riv_2_r['reduction']);
		$sc_riv_3 = $sc_riv_3_r['price'] - ($sc_riv_3_r['price'] * $sc_riv_3_r['reduction']);
		
		if($login_for_offer == 1 && ($user_login == true || $cookie->isLogged()))
			$unitario = $prezzo_login_for_offer;

		if($id_group == 3  || $id_group == 10  || ($id_group == 22 && $sc_riv_2 == 0)) {

			$prezzo_migliore = $unitario;
			$confronto_prezzi = array($sc_riv_1, $sc_qta_1, $sc_qta_2, $sc_qta_3);

			foreach($confronto_prezzi as $prz) {
				if($prz != 0 && $prz < $prezzo_migliore) {

					$prezzo_migliore = $prz;
					if($id_group == 22 && $sc_riv_2 == 0) {	
						$prezzo_migliore = $prezzo_migliore + ($prezzo_migliore / 100 * 3);
						if($prezzo_migliore > Product::trovaMigliorPrezzo($id_product, 1, 999999))
							$prezzo_migliore = Product::trovaMigliorPrezzo($id_product, 1, 999999);
					}
				}
			}
			
			$speciale_riv = Product::trovaPrezzoSpeciale($id_product, 1, $quantity);
		
			if($speciale != 0) {
				if($prezzo_migliore > $speciale) {
					$prezzo_migliore = $speciale;
				}
			}
		}
		else if($id_group == 22 && $sc_riv_2 != 0) {

			$prezzo_migliore = $unitario;
			$confronto_prezzi = array($sc_riv_2, $sc_qta_1, $sc_qta_2, $sc_qta_3);

			foreach($confronto_prezzi as $prz) {
				if($prz != 0 && $prz < $prezzo_migliore) {
					$prezzo_migliore = $prz;
				}
			}
			$speciale_riv = Product::trovaPrezzoSpeciale($id_product, 1, $quantity);
		
			if($speciale != 0) {
				if($prezzo_migliore > $speciale) {
					$prezzo_migliore = $speciale;
				}
			}
		}
		else if($id_group == 12) {

			$prezzo_migliore = $unitario;
			$confronto_prezzi = array($sc_riv_1, $sc_riv_3, $sc_qta_1, $sc_qta_2, $sc_qta_3);

			foreach($confronto_prezzi as $prz) {
				if($prz != 0 && $prz < $prezzo_migliore) {
					$prezzo_migliore = $prz;
				}
			}

			$speciale_riv = Product::trovaPrezzoSpeciale($id_product, 1, $quantity);
		
			if($speciale != 0) {
				if($prezzo_migliore > $speciale) {
				
					$prezzo_migliore = $speciale;
				}
			}
		}
		else {
			if($sc_qta_1 != 0) {
				switch ($quantity) {
					case $quantity < $sc_qta_1_q: 
						$prezzo_migliore = $unitario; 
						break;
					case $quantity >= $sc_qta_1_q && $quantity < $sc_qta_2_q: 
						$prezzo_migliore = $sc_qta_1; 
						break;
					case $quantity >= $sc_qta_2_q && $quantity < $sc_qta_3_q: 
						($sc_qta_2 != 0 ? $prezzo_migliore = $sc_qta_2 :  $prezzo_migliore = $sc_qta_1); 
						break;
					case $quantity >= $sc_qta_3_q: 
						($sc_qta_3 != 0 ? $prezzo_migliore = $sc_qta_3 : ($sc_qta_2 != 0 ? $prezzo_migliore = $sc_qta_2 :  $prezzo_migliore = $sc_qta_1)); 
						break;
					default: 
						$prezzo_migliore = $unitario; 
						break;
				}
			}
			else {
				$prezzo_migliore = $unitario;
			}
		}

		$prezzo_migliore = round($prezzo_migliore,2);
		return $prezzo_migliore;
	}
	
	function trovaPrezzoSpeciale($id_product, $id_group, $quantity = 0) {
	
		global $cookie;
		
		$id_customer = (isset($cookie->id_customer) AND $cookie->id_customer) ? (int)($cookie->id_customer) : 0;
		$id_country = (int)($id_customer ? Customer::getCurrentCountry($id_customer) : Configuration::get('PS_COUNTRY_DEFAULT'));
				
		$today = date("Y-m-d H:i:s");
		$righe = Db::getInstance()->executeS("
			SELECT * 
			FROM specific_price sp 
			WHERE sp.id_product = '".$id_product."' 
				AND sp.specific_price_name = ''
				 AND sp.from <= '".$today."' 
				 AND sp.to >= '".$today."'
		");

		foreach($righe as $riga) {
			if($riga['price'] == 0) {
				$riga['price'] = Db::getInstance()->getValue("SELECT price FROM product WHERE id_product = ".$id_product."");
			}
			
			if($riga['id_group'] = $id_group) {
			
				if($riga['reduction_type'] == 'amount') {
					$prezzo_da_ritornare = $riga['price'];
				}
				else if($riga['reduction_type'] == 'percentage') {
					$prezzo_da_ritornare = $riga['price']-($riga['price']*$riga['reduction']);
				}
				
				//VERIFICO CHE CI SIA ANCORA DISPONIBILITA
				$pz_tot = Db::getInstance()->getRow("
					SELECT stock, pieces 
					FROM specific_price_pieces 
					WHERE id_specific_price = ".$riga['id_specific_price']."
				");

				$pz = $pz_tot['stock'];
				$pz_orig = $pz_tot['pieces'];
								
				if($pz_orig != '' && $pz_orig == 0) {
					$prezzo_da_ritornare = Db::getInstance()->getValue('
						SELECT price 
						FROM product 
						WHERE id_product = '.$id_product
					);
				}
				
				//VERIFICO NAZIONE
				$sp_country = Db::getInstance()->getValue("
					SELECT id_country 
					FROM specific_price 
					WHERE id_specific_price = ".$riga['id_specific_price']."
				");
				
				if($sp_country != 0 && $sp_country != $id_country) {
					$prezzo_da_ritornare = Db::getInstance()->getValue('SELECT price FROM product WHERE id_product = '.$id_product);
				}

				return $prezzo_da_ritornare;
			}
		}
		
		return 0;
	}
	
	function trovaIdPrezzoSpeciale($id_product, $tipo = 'y')
	{
		$today = date('Y-m-d H:i:s');
		$riga = Db::getInstance()->getRow("
			SELECT * 
			FROM specific_price sp 
			WHERE sp.id_product = '".$id_product."' 
				AND sp.specific_price_name = '' 
				AND sp.from < '".$today."' 
				AND sp.to > '".$today."' 
			ORDER BY id_specific_price DESC
		");
	
		//VERIFICO NAZIONE
		/*
		$sp_country = Db::getInstance()->getValue("SELECT id_country FROM specific_price WHERE id_specific_price = ".$riga['id_specific_price']."");
				
		if($sp_country != 0 && $sp_country != $id_country)
		{
			return 0;
		}
		*/
		
		//VERIFICO CHE CI SIA ANCORA DISPONIBILITA
		$pz_tot = Db::getInstance()->getRow("
			SELECT stock, pieces 
			FROM specific_price_pieces 
			WHERE id_specific_price = ".$riga['id_specific_price']."
		");

		$pz = $pz_tot['stock'];
		$pz_orig = $pz_tot['pieces'];
								
		if($pz_orig != '' && $pz == 0){
			if($tipo == 'y')
				return 0;
		}
		
		if($riga['id_specific_price'] > 0)
			return $riga['id_specific_price'];
		else
			return 0;
	}
	
	// type 0 -> ritorna yes o no; type 1 -> ritorna il numero del prezzo speciale o 0
	function checkPrezzoSpeciale($id_product, $id_group, $quantity = 0, $type = 0) {
		
		$today = date("Y-m-d H:i:s");
		$righe = Db::getInstance()->executeS("
			SELECT * 
			FROM specific_price sp 
			WHERE sp.id_product = '".$id_product."' 
				AND sp.specific_price_name = '' 
				AND sp.from < '".$today."' 
				AND sp.to > '".$today."'
		");

		foreach($righe as $riga) {
			
			if($riga['price'] == 0) {
				$riga['price'] = Db::getInstance()->getValue("SELECT price FROM product WHERE id_product = ".$id_product."");
			}

			if($riga['id_group'] = $id_group || $riga['id_group'] == 0) {
			
				if($riga['reduction_type'] == 'amount') {
					$prezzo_da_ritornare = $riga['price'];
				}
				else if($riga['reduction_type'] == 'percentage') {
					$prezzo_da_ritornare = $riga['price']*$riga['reduction'];
				}
				
				$pz_tot = Db::getInstance()->getRow("
					SELECT stock, pieces 
					FROM specific_price_pieces 
					WHERE id_specific_price = ".$riga['id_specific_price']."
				");

				$pz = $pz_tot['stock'];
				$pz_orig = $pz_tot['pieces'];
				
				if($type == 1) {
					return $riga['id_specific_price'];
				}
				else {
					return "yes";
				}
			}
		}
		
		if($type == 1) {
			return 0;
		}
		else {
			return "no";
		}
	}
	
	function controllaSeFaParteDiUnBundle($id_order, $id_product) {
	
		$bundle = Db::getInstance()->getValue("SELECT cp.bundle FROM cart_product cp JOIN cart c ON cp.id_cart = c.id_cart JOIN orders o ON o.id_cart = c.id_cart WHERE o.id_order = ".$id_order." AND cp.id_product = ".$id_product."");

		if($bundle > 0) {
			return "<strong>S&igrave;</strong>";
		}
		else {
			return "No";
		}
	}
	
	public static function getPricesDrop($id_lang, $pageNumber = 0, $nbProducts = 10, $count = false, $orderBy = NULL, $orderWay = NULL, $beginning = false, $ending = false)
	{
		if (!Validate::isBool($count))
			die(Tools::displayError());

		if ($pageNumber < 0) $pageNumber = 0;
		if ($nbProducts < 1) $nbProducts = 10;
		
		if (empty($orderWay)) $orderWay = 'DESC';
		
		if (empty($orderBy) || $orderBy == 'position') { $orderBy = '(CASE WHEN p.id_manufacturer = 105 THEN 0 WHEN p.id_manufacturer = 132 THEN 1 WHEN p.id_manufacturer = 133 THEN 2 WHEN p.id_manufacturer = 37 THEN 3 WHEN p.id_manufacturer = 10 THEN 4 WHEN p.id_manufacturer = 25 THEN 5 WHEN p.id_manufacturer = 37 THEN 6 ELSE 7 END)'; $orderWay = ''; }
		
		if(Tools::getValue('orderby') != '')
			$orderBy = Tools::getValue('orderby');
		
		if(Tools::getValue('orderway') != '')
			$orderWay = Tools::getValue('orderway');
		
		if($orderBy == 'disponibili' || $orderBy == 'venduti')
			$orderBy = 'quantity';
		
		if ($orderBy == 'id_product' OR $orderBy == 'quantity' OR $orderBy == 'price' OR $orderBy == 'date_add')
			$orderByPrefix = 'p';
		elseif ($orderBy == 'name')
			$orderByPrefix = 'pl';
		
		if (Tools::getValue('orderby') == 'venduti')
			$orderByPrefix = 'ps';
		
		$currentDate = date('Y-m-d H:i:s');
		$ids_product = self::_getProductIdByDate((!$beginning ? $currentDate : $beginning), (!$ending ? $currentDate : $ending));

		$groups = FrontController::getCurrentCustomerGroups();
		$sqlGroups = (count($groups) ? 'IN ('.implode(',', $groups).')' : '= 1');
		
		
		$sql = '
			SELECT SQL_CACHE p.*, pl.`description`, pl.`description_short`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`cat_homepage`, pl.`desc_homepage`,  pl.`meta_title`,
		pl.`name`, p.`ean13`, p.`upc`, i.`id_image`, il.`legend`, t.`rate`, m.`name` AS manufacturer_name,
		DATEDIFF(p.`date_add`, DATE_SUB(NOW(), INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY)) > 0 AS new
			FROM `'._DB_PREFIX_.'product` p
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')
			LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
			LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)($id_lang).')
			LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (p.`id_tax_rules_group` = tr.`id_tax_rules_group`
													AND tr.`id_country` = '.(int)Country::getDefaultCountryId().'
													AND tr.`id_state` = 0)
			LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
			LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
			LEFT JOIN '._DB_PREFIX_.'product_sale ps ON (ps.id_product = p.id_product)
			WHERE 1 
				AND p.id_category_default != 246 
				AND p.id_category_default != 256 
				'.(Tools::getIsset('category') ? ' AND 
				(
				(SELECT 
					(CASE WHEN (c4.id_category = 0 OR c4.id_category = 1 OR c4.id_category IS NULL) THEN 
					(CASE WHEN (c3.id_category = 0 OR c3.id_category = 1  OR c3.id_category IS NULL) THEN 
					(CASE WHEN (c2.id_category = 0 OR c2.id_category = 1  OR c2.id_category IS NULL) THEN 
					c1.id_category 
					ELSE c2.id_category END) 
					ELSE c3.id_category END) 
					ELSE c4.id_category END) 
				FROM category AS c1 
				LEFT JOIN category AS c2 ON( c1.id_parent = c2.id_category ) 
				LEFT JOIN category AS c3 ON( c2.id_parent = c3.id_category ) 
				LEFT JOIN category AS c4 ON( c3.id_parent = c4.id_category ) 
				WHERE c1.id_category = p.id_category_default)
				) = 
				'.Category::getMacroCategory(Tools::getValue('category')) : '').'
				AND p.`active` = 1
				AND p.price > 0
				AND p.`show_price` = 1
				AND p.`id_product` IN (
					SELECT id_product 
					FROM specific_price sp
					WHERE sp.specific_price_name = "" 
						AND sp.from < "'.date("Y-m-d H:i:s").'" 
						AND sp.to > "'.date("Y-m-d H:i:s").'"
					)
				AND p.`id_product` IN (
					SELECT cp.`id_product`
					FROM `'._DB_PREFIX_.'category_group` cg
					LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON (cp.`id_category` = cg.`id_category`)
					WHERE cg.`id_group` '.$sqlGroups.'
				)
			ORDER BY '.(isset($orderByPrefix) ? pSQL($orderByPrefix).'.' : ' ').''.pSQL($orderBy).''.' '.pSQL($orderWay).'
			LIMIT '.(int)($pageNumber * $nbProducts).', '.(int)($nbProducts)
		;
		
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);

		$i = 1;
		foreach($result as $key => $res) {
			$rquery = ("
				SELECT * 
				FROM specific_price 
				WHERE id_product = ".$res['id_product']." 
					AND specific_price.to != '0000-00-00 00:00:00' 
					AND specific_price.from < '".date("Y-m-d H:i:s")."' 
					AND specific_price.to > '".date("Y-m-d H:i:s")."' 
			");

			$rann = mysql_query($rquery);
			if(mysql_num_rows($rann) > 0) {
				$rowann = Db::getInstance()->getRow($rquery);
				
				$pz_tot = Db::getInstance()->getRow("
					SELECT stock, pieces 
					FROM specific_price_pieces 
					WHERE id_specific_price = ".$rowann['id_specific_price']."
				");

				$pz = $pz_tot['stock'];
				$pz_orig = $pz_tot['pieces'];
				
				if(($pz_orig != '') && $pz_orig == 0) {
					 unset($result[$key]);		
				}		
				else {
					$i++;
				}
			}
		}
		
		if ($count) {
			$sql = '
				SELECT COUNT(DISTINCT p.`id_product`) AS nb
				FROM `'._DB_PREFIX_.'product` p
				WHERE p.`active` = 1
					AND p.`show_price` = 1
					AND p.`id_product` IN (
						SELECT id_product 
						FROM specific_price sp 
						WHERE sp.specific_price_name = "" 
							AND sp.from < "'.date("Y-m-d H:i:s").'" 
							AND sp.to > "'.date("Y-m-d H:i:s").' 
							AND sp.id_specific_price NOT IN (
								SELECT id_specific_price 
								FROM specific_price_pieces 
								WHERE pieces = 0
							)
						")
					AND p.`id_product` IN (
						SELECT cp.`id_product`
						FROM `'._DB_PREFIX_.'category_group` cg
						LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON (cp.`id_category` = cg.`id_category`)
						WHERE cg.`id_group` '.$sqlGroups.'
					)
			';
			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
			return (int)($result['nb']);
		}
		
		$i = 0;
		
		if ($orderBy == 'price')
			Tools::orderbyPrice($result,$orderWay);
		if (!$result)
			return false;
		return Product::getProductsProperties($id_lang, $result);
	}
	
	public static function getPricesDropCategories() {
		
			$categories_i = Db::getInstance()->executeS('
				SELECT c.id_category 
				FROM category c 
				JOIN product p ON c.id_category = p.id_category_default 
				JOIN specific_price sp ON sp.id_product = p.id_product 
				WHERE c.id_category != 246 
					AND c.id_category != 256 
					AND sp.specific_price_name = "" 
					AND sp.from < "'.date("Y-m-d H:i:s").'" 
					AND sp.to > "'.date("Y-m-d H:i:s").'" 
				GROUP BY c.id_category
			');

			$categories = array();
			foreach($categories_i as $category_i) {
				$category = Category::getMacroCategory($category_i['id_category']);
				if(!(in_array($category, $categories)))
					$categories[] = $category;
			}

			return $categories;
	}
			
	public function checkCouponLogo($id_product) {
		
		$coupons_i = Db::getInstance()->executeS("
			SELECT d.id_discount 
			FROM discount_product dp 
			JOIN discount d ON dp.id_discount = d.id_discount 
			WHERE d.logo = 1 
				AND d.date_to > NOW() 
				AND dp.id_product = ".$id_product."
		");
		
		if(count($coupons_i) > 0) {
			return 1;
		}
		else {
			return 0;
		}
		
	}
		
	public static function getCommentNumber($id_product)
	{
		if (!Validate::isUnsignedId($id_product))
			die(Tools::displayError());

		$validate = (int)(Configuration::get('PRODUCT_COMMENTS_MODERATE'));

		if (($result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT COUNT(`id_product_comment`) AS "nbr"
			FROM `'._DB_PREFIX_.'product_comment` pc
			WHERE `id_product` = '.(int)($id_product).($validate == '1' ? ' AND `validate` = 1' : ''))
		) === false)
			return false;

		return (int)($result['nbr']);
	}
	
	public static function getAveragesByProduct($id_product, $id_lang)
	{
		/* Get all grades */
		$grades = Product::getGradeByProduct((int)($id_product), (int)($id_lang));
		$total = Product::getGradedCommentNumber((int)($id_product));
		if (!sizeof($grades) OR (!$total))
			return array();

		/* Addition grades for each criterion */
		$criterionsGradeTotal = array();
		for ($i = 0; $i < count($grades); ++$i)
			if (array_key_exists($grades[$i]['id_product_comment_criterion'], $criterionsGradeTotal) === false)
				$criterionsGradeTotal[$grades[$i]['id_product_comment_criterion']] = (int)($grades[$i]['grade']);
			else
				$criterionsGradeTotal[$grades[$i]['id_product_comment_criterion']] += (int)($grades[$i]['grade']);

		/* Finally compute the averages */
		$averages = array();
		foreach ($criterionsGradeTotal AS $key => $criterionGradeTotal)
			$averages[(int)($key)] = (int)($total) ? ((int)($criterionGradeTotal) / (int)($total)) : 0;
		return $averages;
	}
	
	public static function getGradeByProduct($id_product, $id_lang)
	{
		if (!Validate::isUnsignedId($id_product) ||
			!Validate::isUnsignedId($id_lang))
			die(Tools::displayError());
		$validate = Configuration::get('PRODUCT_COMMENTS_MODERATE');

		return (Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT pc.`id_product_comment`, pcg.`grade`, pccl.`name`, pcc.`id_product_comment_criterion`
			FROM `'._DB_PREFIX_.'product_comment` pc
			LEFT JOIN `'._DB_PREFIX_.'product_comment_grade` pcg ON (pcg.`id_product_comment` = pc.`id_product_comment`)
			LEFT JOIN `'._DB_PREFIX_.'product_comment_criterion` pcc ON (pcc.`id_product_comment_criterion` = pcg.`id_product_comment_criterion`)
			LEFT JOIN `'._DB_PREFIX_.'product_comment_criterion_lang` pccl ON (pccl.`id_product_comment_criterion` = pcg.`id_product_comment_criterion`)
			WHERE pc.`id_product` = '.(int)($id_product).'
				AND pccl.`id_lang` = '.(int)($id_lang).
				($validate == '1' ? ' AND pc.`validate` = 1' : '')
			)
		);
	}
	
	public static function getGradedCommentNumber($id_product)
	{
		if (!Validate::isUnsignedId($id_product))
			die(Tools::displayError());
		$validate = (int)(Configuration::get('PRODUCT_COMMENTS_MODERATE'));

		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT COUNT(pc.`id_product`) AS nbr
			FROM `'._DB_PREFIX_.'product_comment` pc
			WHERE `id_product` = '.(int)($id_product)
				.($validate == '1' ? ' AND `validate` = 1' : '')
				.'AND `grade` > 0'
		);

		return (int)($result['nbr']);
	}
	
	public static function getPriceStatic($id_product, $usetax = true, $id_product_attribute = NULL, $decimals = 6, $divisor = NULL, $only_reduc = false, $usereduc = true, $quantity = 1, $forceAssociatedTax = false, $id_customer = NULL, $id_cart = NULL, $id_address = NULL, &$specificPriceOutput = NULL, $with_ecotax = true, $use_groupReduction = true, $user_login = false)
	{
		global $cookie, $cart;
		$cur_cart = $cart;

		if (isset($divisor))
			Tools::displayParameterAsDeprecated('divisor');

		if (!Validate::isBool($usetax) OR !Validate::isUnsignedId($id_product))
			die(Tools::displayError());
		// Initializations
		if (!$id_customer)
			$id_customer = ((Validate::isCookie($cookie) AND isset($cookie->id_customer) AND $cookie->id_customer) ? (int)($cookie->id_customer) : NULL);
		if(isset($cookie->id_customer))
			$id_group = $id_customer ? $cookie->id_default_group : _PS_DEFAULT_CUSTOMER_GROUP_;
		else
			$id_group = $id_customer ? (int)(Customer::getDefaultGroupId($id_customer)) : _PS_DEFAULT_CUSTOMER_GROUP_;
		
		if (!is_object($cur_cart) OR (Validate::isUnsignedInt($id_cart) AND $id_cart)) {
			/*
			* When a user (e.g., guest, customer, Google...) is on PrestaShop, he has already its cart as the global (see /init.php)
			* When a non-user calls directly this method (e.g., payment module...) is on PrestaShop, he does not have already it BUT knows the cart ID
			*/
			if (!$id_cart AND !Validate::isCookie($cookie))
				die(Tools::displayError());
			$cur_cart = $id_cart ? new Cart((int)($id_cart)) : new Cart((int)($cookie->id_cart));
		}

		$cart_quantity = 0;
		if ((int)($id_cart)) {
			$condition = '';
			$cache_name = (int)($id_cart).'_'.(int)($id_product);
			
			if(Configuration::get('PS_QTY_DISCOUNT_ON_COMBINATION')) {
				$cache_name = (int)($id_cart).'_'.(int)($id_product).'_'.(int)($id_product_attribute);
				$condition = ' AND `id_product_attribute` = '.(int)($id_product_attribute);
			}
				
			if (!isset(self::$_cart_quantity[$cache_name]) OR self::$_cart_quantity[$cache_name] !=  (int)($quantity)) {
				self::$_cart_quantity[$cache_name] = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
					SELECT SUM(`quantity`)
					FROM `'._DB_PREFIX_.'cart_product`
					WHERE `id_product` = '.(int)($id_product).' 
						AND `id_cart` = '.(int)($id_cart).' '.$condition					
				);
				
				$cart_quantity = self::$_cart_quantity[$cache_name];
			}
		}

		$quantity = ($id_cart AND $cart_quantity) ? $cart_quantity : $quantity;
		$id_currency = (int)(Validate::isLoadedObject($cur_cart) ? $cur_cart->id_currency : ((isset($cookie->id_currency) AND (int)($cookie->id_currency)) ? $cookie->id_currency : Configuration::get('PS_CURRENCY_DEFAULT')));

		// retrieve address informations
		$id_country = (int)Country::getDefaultCountryId();
		$id_state = 0;
		$id_county = 0;

		if (!$id_address)
			$id_address = $cur_cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};

		if ($id_address) {
			$address_infos = Address::getCountryAndState($id_address);
			if ($address_infos['id_country']) {
				$id_country = (int)($address_infos['id_country']);
				$id_state = (int)($address_infos['id_state']);
				$postcode = (int)$address_infos['postcode'];

				$id_county = (int)County::getIdCountyByZipCode($id_state, $postcode);
			}
		}
		elseif (isset($cookie->id_country)) {
			// fetch address from cookie
			$id_country = (int)$cookie->id_country;
			$id_state = (int)$cookie->id_state;
			$postcode = (int)$cookie->postcode;

			$id_county = (int)County::getIdCountyByZipCode($id_state, $postcode);
		}

		if (Tax::excludeTaxeOption())
			$usetax = false;

		if ($usetax != false AND !empty($address_infos['vat_number']) AND $address_infos['id_country'] != Configuration::get('VATNUMBER_COUNTRY') AND Configuration::get('VATNUMBER_MANAGEMENT'))
			$usetax = false;

		$id_shop = (int)(Shop::getCurrentShop());

		return Product::priceCalculation($id_shop, $id_product, $id_product_attribute, $id_country,  $id_state, $id_county, $id_currency, $id_group, $quantity, $usetax, $decimals, $only_reduc,
		$usereduc, $with_ecotax, $specificPriceOutput, $use_groupReduction, $user_login);
	}
	
	public function RecuperaCSV($id_order)
	{
		$query = "
			SELECT DISTINCT
				customer.id_customer AS identificatore_univoco_cliente,
				customer.firstname AS nome_cliente,
				customer.lastname AS cognome_cliente,
				customer.is_company AS e_azienda,
				customer.pec AS pec,
				customer.codice_univoco AS codice_univoco,
				orders.id_order AS codice_ordine,
				orders.date_add AS data_ordine,
				orders.payment AS pagamento,
				orders.payment AS pagamento_e,
				orders.total_commissions AS commissioni,
				orders.total_paid AS importo,
				customer.vat_number AS partita_iva_cliente,
				customer.company AS ragione_sociale,
				customer.tax_code AS codice_fiscale,
				address1.address1 AS indirizzo,
				address1.postcode AS cap,
				address1.city AS localita,
				state1.iso_code AS provincia,
				state1.name AS provincia_esteso,
				address1.phone AS telefono_1,
				address1.phone_mobile AS telefono_2,
				address1.fax AS fax,
				customer.email AS indirizzo_email,
				address2.company AS spedizione_ragione_sociale,
				address2.firstname AS nome_spedizione,
				address2.lastname AS cognome_spedizione,
				address2.address1 AS spedizione_indirizzo,
				address2.postcode AS spedizione_cap,
				address2.city AS spedizione_citta,
				state2.iso_code AS spedizione_provincia,
				country.iso_code AS codice_nazione,
				country2.iso_code AS spedizione_nazione,
				orders.date_add AS data_ordine,
				order_detail.product_reference AS codice_articolo,
				order_detail.product_supplier_reference AS codice_sku,
				order_detail.product_id AS id_product,
				order_detail.product_name AS descrizione_articolo,
				order_detail.product_quantity AS quantita_ordinata,
				order_detail.wholesale_price AS prezzo_acquisto,
				orders.total_shipping AS spedizione,
				orders.carrier_tax_rate AS iva_spedizione,
				orders.id_carrier AS corriere,
				orders.id_cart AS id_cart,
				customer.id_default_group AS gruppo_cliente,
				order_detail.product_price AS prezzo_unitario
			FROM order_detail
			JOIN orders ON (orders.id_order = order_detail.id_order)
			JOIN customer ON (orders.id_customer = customer.id_customer)
			JOIN address AS address1 ON (orders.id_address_invoice = address1.id_address)
			JOIN address AS address2 ON (orders.id_address_delivery = address2.id_address)
			LEFT JOIN country ON (address1.id_country = country.id_country)
			LEFT JOIN state AS state1 ON (address1.id_state = state1.id_state)
			LEFT JOIN state AS state2 ON (address2.id_state = state2.id_state)
			LEFT JOIN country AS country2 ON (address2.id_country = country2.id_country)
			WHERE orders.id_order = $id_order
		";
			
		$rows = Db::getInstance()->executeS($query);
		
		$riga_ordine = '';
		$riga_cli = '';
		$applica_fedelta = false;
		$tipo_ordine = 1;
		
		/*
		tipo ordine 0: da confermare
		tipo ordine 1: confermato
		tipo ordine 2: prenotato
		*/
		
		foreach($rows as $row) {
			$identificatore_univoco_cliente = $row["id_order"]; //1 
			$identificatore_univoco_cliente = $row["identificatore_univoco_cliente"]; //1 
			$codice_ordine = $row["codice_ordine"]; //2
			$partita_iva_cliente = $row["partita_iva_cliente"]; //3 
			$codice_fiscale = $row["codice_fiscale"]; //4 
			$ragione_sociale = $row["ragione_sociale"]; //5 
			$indirizzo = $row["indirizzo"]; //6 
			$cap = $row["cap"]; //7 
			$localita = $row["localita"]; //8 
			$provincia = $row["provincia"]; //9 
			$telefono_1 = $row["telefono_1"]; //10 
			$telefono_2 = ""; //11
			$fax = $row["fax"]; //12 
			$indirizzo_email= $row["indirizzo_email"]; //13 
			$codice_del_listino = ""; //14 
			$codice_pagamento = ""; //15 
			if($row["spedizione_ragione_sociale"] != '') {
				$spedizione_ragione_sociale = $row["spedizione_ragione_sociale"]; //16 
			}
			else {
				$spedizione_ragione_sociale = $row["nome_spedizione"]." ".$row["cognome_spedizione"]; //16 
			}
			$spedizione_indirizzo = $row["spedizione_indirizzo"]; //17 
			$spedizione_cap = $row["spedizione_cap"]; //18 
			$spedizione_provincia = $row["spedizione_provincia"]; //19
			$data_ordine = $row["data_ordine"]; //20 
			$numero_ordine = $row["codice_ordine"]; //21 
			$codice_articolo = $row["codice_articolo"]; //22 
			$codice_articolo_del_produttore = ""; //23
			$descrizione_articolo = $row["descrizione_articolo"]; //24
			$quantita_ordinata = $row["quantita_ordinata"]; //25
			
			$sconto_1 = ""; //27
			$sconto_2 = ""; //28
			$sconto_3 = ""; //29
			$codice_iva_vendite = ""; //30
			$codice_deposito = ""; //31 
			$codice_del_listino = ""; //32 
			$codice_aspetto_dei_beni = ""; //33 
			$codice_porto = ""; //34
			$codice_trasporto_a_mezzo = ""; //35
			$codice_vettore = ""; //36
			$riga_omaggio = ""; //37
			$pagamento = $row['pagamento'];
			$id_product = $row["id_product"];  
			$gruppo_cliente = $row["gruppo_cliente"]; 

			if(strtolower($ragione_sociale) == 'geometra' || strtolower($ragione_sociale) == 'geom' || strtolower($ragione_sociale) == 'avv' || strtolower($ragione_sociale) == 'avvocato' || strtolower($ragione_sociale) == 'dott' || strtolower($ragione_sociale) == 'dottore' || strtolower($ragione_sociale) == 'commercialista' || strtolower($ragione_sociale) == 'notaio' || strtolower($ragione_sociale) == 'studio medico' || strtolower($ragione_sociale) == 'studio legale' || strtolower($ragione_sociale) == 'studio dentistico' || strtolower($ragione_sociale) == 'studio notarile' || strtolower($ragione_sociale) == 'studio tecnico' || strtolower($ragione_sociale) == 'studio associato') {
				$ragione_sociale.= " ".$row['nome_cliente']." ".$row['cognome_cliente'];
			}

			if(strtolower($ragione_sociale) == 'geometra' || strtolower($ragione_sociale) == 'geom' || strtolower($ragione_sociale) == 'avv' || strtolower($ragione_sociale) == 'avvocato' || strtolower($ragione_sociale) == 'dott' || strtolower($ragione_sociale) == 'dottore' || strtolower($ragione_sociale) == 'commercialista' || strtolower($ragione_sociale) == 'notaio' || strtolower($ragione_sociale) == 'studio medico' || strtolower($ragione_sociale) == 'studio legale' || strtolower($ragione_sociale) == 'studio dentistico' || strtolower($ragione_sociale) == 'studio notarile' || strtolower($ragione_sociale) == 'studio tecnico' || strtolower($ragione_sociale) == 'studio associato') {
				$spedizione_ragione_sociale.= " ".$row['nome_spedizione']." ".$row['cognome_spedizione'];
			}	

			$queryprice = ("SELECT product_price, reduction_percent, reduction_amount FROM order_detail WHERE product_id = $id_product AND id_order = ".$id_order."");
			$rowprice = Db::getInstance()->getRow($queryprice);

			$numpri = mysql_num_rows($queryprice);
			
			if($rowprice['reduction_amount'] > 0)
				$prezzo_unitario = ($rowprice['product_price']-$rowprice['reduction_amount']);
			else {	
				$sconto = ($rowprice['product_price']*$rowprice['reduction_percent'])/100;
				$prezzo_unitario = ($rowprice['product_price']-$sconto);
			}
			
			$prezzo_unitario = number_format($prezzo_unitario, 2, ',', '');
			$prezzo_unitario_e = $prezzo_unitario;
			$prezzo_unitario = "€".$prezzo_unitario;

			if($row['e_azienda'] == 1) {
				$ragsoc = $row['ragione_sociale'];
			}
			else {
				$ragsoc = $row['nome_cliente']." ".$row['cognome_cliente'];
			}

			if($row["spedizione_ragione_sociale"] != "") {
				if($row['e_azienda'] == 1) {
					if((strtolower($row['nome_cliente']." ".$row['cognome_cliente'])) != (strtolower($row['nome_spedizione']." ".$row['cognome_spedizione']))) {
						$ragsoc2 = $row['nome_spedizione']." ".$row['cognome_spedizione'];
					}
					else {
						$ragsoc2 = "";
					}
				} 
				else {
					$ragsoc2 = $row['nome_spedizione']." ".$row['cognome_spedizione'];
				}
			}

			/*$iva_spedizione = (int)($row['iva_spedizione']);
			$iva_spedizione = (string)($iva_spedizione);
			$iva_spedizione = "1.".$iva_spedizione;
			$iva_spedizione = (double)($iva_spedizione);
			*/
			$iva_spedizione = (int)($row['iva_spedizione']);
			$iva_spedizione = (100 + $iva_spedizione) / 100;
			$spedizione_citta = $row["spedizione_citta"]; //18 
			$spedizione = ($row['spedizione'] / $iva_spedizione);
			$spedizione = number_format($spedizione, 2, ',', '');
			

			$arraydata1 = explode(" ",$data_ordine);
			$arraydata2 = explode("-",$arraydata1[0]);

			$data_nuova = $arraydata2[1]."/".$arraydata2[2]."/".$arraydata2[0];
			$data_nuova_e = $arraydata2[2]."/".$arraydata2[1]."/".$arraydata2[0];
			// esolver
			
			$is_verifica = Db::getInstance()->getValue('SELECT id_order_history FROM order_history WHERE id_order_state = 24 AND id_order = '.$id_order.' AND id_order NOT IN (SELECT id_order FROM order_history WHERE id_order = '.$id_order.' AND id_order_state = 25)');
		
			if($is_verifica > 0 && $is_verifica != '')
				$stato_conferma = 0;
			
			$check_verifica = Db::getInstance()->getValue('SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT max( id_order_history )FROM (SELECT * FROM order_history WHERE id_order_state = 24 OR id_order_state =25) s WHERE s.id_order = '.$id_order.') ORDER BY date_add DESC');
			
			$stato_attuale = Db::getInstance()->getValue('SELECT id_order_state FROM order_history WHERE id_order = '.$id_order.' ORDER BY date_add DESC');
			
			if($stato_attuale == 30)
				$magazzino = 30;
			else
				$magazzino = 1;
			
			$check_prime = Db::getInstance()->getValue('SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT max( id_order_history )FROM (SELECT * FROM order_history WHERE id_order_state = 30) s WHERE s.id_order = '.$id_order.') ORDER BY date_add DESC');
			
			if($check_prime > 0)
			{	
				$casella_prime = 'PRIME';
				$magazzino = 30;
			}	
			else
				$casella_prime = '';
			
			// sotto: != 24
			//if($check_verifica != 24)
			//{
				if($pagamento == "Pagamento alla consegna (COD)") {
					$pagamento_e = "X002"; 
					$stato_conferma = 2;
				}

				else if($pagamento == "Bonifico Bancario" || $pagamento == "Bonifico Anticipato" || $pagamento == "Bonifico anticipato" || $pagamento == "Bonifico bancario" || $pagamento == "Bonifico") {
					$pagamento_e = "BB369"; 
					$stato_conferma = 1;

					// aggiungere verificare che sia dopo stato 10
					$is_conf = Db::getInstance()->getValue('SELECT id_order_history FROM order_history WHERE id_order = '.$id_order.' AND id_order_state = 18');

					if($is_conf > 0)
						$stato_conferma = 2;
					
					if($stato_attuale == 10)
						$stato_conferma = 1;

				}
				else if($pagamento == "Bonifico 30 gg. fine mese") {
					$pagamento_e = "BB430"; 
					$stato_conferma = 2;
				}

				else if($pagamento == "Bonifico 60 gg. fine mese") {
					$pagamento_e = "BB431"; 
					$stato_conferma = 2;
				}
				
				else if($pagamento == "Bonifico 90 gg. fine mese") {
					$pagamento_e = "BB432"; 
					$stato_conferma = 2;
				}
				else if($pagamento == "Bonifico 30 gg. 15 mese successivo") {
					$pagamento_e = "BB463"; 
					$stato_conferma = 2;
				}
				
				else if($pagamento == "R.B. 30 GG. D.F. F.M.") {
					$pagamento_e = "RB230"; 
					$stato_conferma = 2;
				}
				
				else if($pagamento == "R.B. 60 GG. D.F. F.M.") {
					$pagamento_e = "RB231";
					$stato_conferma = 2;
				}
				
				else if($pagamento == "R.B. 90 GG. D.F. F.M.") {
					$pagamento_e = "RB232";
					$stato_conferma = 2;
				}
				
				else if($pagamento == "R.B. 30 GG. 5 mese successivo") {
					$pagamento_e = "RB253"; 
					$stato_conferma = 2;
				}
				else if($pagamento == "R.B. 30 GG. 10 mese successivo") {
					$pagamento_e = "RB250"; 
					$stato_conferma = 2;
				}
				else if($pagamento == "R.B. 60 GG. 5 mese successivo") {
					$pagamento_e = "RB258"; 
					$stato_conferma = 2;
				}
				else if($pagamento == "R.B. 60 GG. 10 mese successivo") {
					$pagamento_e = "RB249"; 
					$stato_conferma = 2;
				}
				
				else if($pagamento == "Contrassegno") {
					$pagamento_e = "X002";
					$stato_conferma = 2;
				}

				else if($pagamento == "Contrassegno" || $pagamento == "Contanti alla consegna (non si accettano assegni)") {
					$pagamento_e = "X002";
					$stato_conferma = 2;
				}
				
				else if($pagamento == "Carta Amazon") {
					$pagamento_e = "X011";
					$stato_conferma = 2;
				}
				
				else if($pagamento == "Carta ePrice") {
					$pagamento_e = "X012";
					$stato_conferma = 2;
				}
				
				else if($pagamento == "PayPal" || $pagamento == "Paypal") {
					$pagamento_e = "X013";
					$stato_conferma = 2;
				}
				
				else if($pagamento == "GestPay") {
					// gestpay
					$pagamento_e = "X007";
					$stato_conferma = 2;
				}
				else
				{
					$pagamento_e = "BB369";
					$stato_conferma = 2;
					
				}
			//}
			
			
			if($check_verifica == 24)
				$stato_conferma_ver = 0;
			else if($check_verifica == 25)
				$stato_conferma_ver = 1;
			else
				$stato_conferma_ver = 1;
			
			if($stato_conferma_ver == 0)
				$stato_conferma = 0;
			
			
			if($row['codice_articolo'] == 'CHEAP-300-O' || $row['codice_articolo'] == 'CHEAP-1500-O' || $prezzo_unitario == "€"."0,00") {
			$prezzo_unitario = "€"."1,00";
			}

		
			if($codice_articolo == 'OMAGGIO') {
		
				$queryprice = ("SELECT product_name FROM order_detail WHERE product_id = $id_product AND id_order = ".$id_order."");
				$rowprice = Db::getInstance()->getRow($queryprice);
				
				$riga_nome_omaggio = $rowprice['product_name'];
				
				$elenco_prodotti = explode(":", $riga_nome_omaggio);
				
				$qtprod = explode(";", $elenco_prodotti[1]);
			
				
				foreach($qtprod as $prodotto) {
					if(!($prodotto == '')) {
						$qtnpr = explode("x ",$prodotto);
						$id_art = Db::getInstance()->getValue("
							SELECT id_product 
							FROM product_lang 
							WHERE id_lang = 5 
								AND name = '".$qtnpr[1]."'
						");

						$cd_art = Db::getInstance()->getValue("
							SELECT reference 
							FROM product 
							WHERE id_product = '".$id_art."'
						");
						
						$riga_ordine .= '"'.$row['identificatore_univoco_cliente'].'";"'.$row['partita_iva_cliente'].'";"'.$row['codice_fiscale'].'";"'.$row['identificatore_univoco_cliente'].'";"";"'.$row['codice_ordine'].'";"'.$data_nuova.'";"'.$pagamento.'";"";"'.$spedizione_ragione_sociale.'";"'.$ragsoc2.'";"'.$row['spedizione_indirizzo'].'";"'.$row['spedizione_cap'].'";"'.$row['spedizione_citta'].'";"'.$row['spedizione_provincia'].'";"";"";"";"";"";"";"";"";"";"";"";"'.$row['codice_ordine'].'";"'.$cd_art.'";"";"";"'.trim($qtnpr[0]).'";"";"";"";"";"";"";"";"'."€"."1,00".'";"";"";"";"";"";"";"";"";"0"';
						$riga_ordine .= "\n";
						
						/* esolver */
						$riga_ord_prod_e .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||4|'.$cd_art.'|'.$qtnpr[1].'|'.$qtnpr[0].'|0,00|||||'.$stato_conferma.'|'.$magazzino.'|||||EOR';
						$riga_ord_prod_e .= "\n";
						/* fine esolver */
					}
				}
			}		
			else {	
			
				$riga_ordine .= '"'.$row['identificatore_univoco_cliente'].'";"'.$row['partita_iva_cliente'].'";"'.$row['codice_fiscale'].'";"'.$row['identificatore_univoco_cliente'].'";"";"'.$row['codice_ordine'].'";"'.$data_nuova.'";"'.$pagamento.'";"";"'.$spedizione_ragione_sociale.'";"'.$ragsoc2.'";"'.$row['spedizione_indirizzo'].'";"'.$row['spedizione_cap'].'";"'.$row['spedizione_citta'].'";"'.$row['spedizione_provincia'].'";"";"";"";"";"";"";"";"";"";"";"";"'.$row['codice_ordine'].'";"'.$row['codice_articolo'].'";"";"";"'.$row['quantita_ordinata'].'";"";"";"";"";"";"";"";"'.$prezzo_unitario.'";"";"";"";"";"";"";"";"";"0"';
				/*esolver */
				
				$category_product = Db::getInstance()->getValue('SELECT id_category_default FROM product WHERE id_product = '.$row['id_product']);
				if(($category_product == 119 || $category_product == 248 || $category_product == 249 || $category_product == 250 || $category_product == 258 || $category_product == 259)) {
					$tipo_articolo = 85;
					
				}
				else {
					$tipo_ordine = 0;
					$tipo_articolo = 1;
				}
				
				/*if($stato_attuale == 30)
					$tipo_ordine = 3;
				*/
				
				if($row['codice_articolo'] == 'CHEAP-300-O' || $row['codice_articolo'] == 'CHEAP-600-O') {
					
					$riga_ord_prod_e .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||'.$tipo_articolo.'|'.$row['codice_articolo'].'|'.$row['descrizione_articolo'].'|'.$row['quantita_ordinata'].'|'.$prezzo_unitario_e.'|||||'.$stato_conferma.'|'.$magazzino.'|||||EOR';
				}
				else if($row['codice_articolo'] == 'TRASP') {
					$riga_ord_prod_e .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||40|TRASP|Trasporto|1|'.$prezzo_unitario_e.'|||||'.$stato_conferma.'|'.$magazzino.'|||||EOR';
				}
				else {
					
					$riga_ord_prod_e .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||'.$tipo_articolo.'|'.$row['codice_articolo'].'|'.$row['descrizione_articolo'].'|'.$row['quantita_ordinata'].'|'.$prezzo_unitario_e.'|||||'.$stato_conferma.'|'.$magazzino.'|||||EOR';
				}
					
				$riga_ord_prod_e .= "\n";
				
				
				/* fine esolver */
			}

			$riga_ordine .= "\n";

			$riga_cli .= '"'.$row['identificatore_univoco_cliente'].'";"'.$row['partita_iva_cliente'].'";"'.$row['codice_fiscale'].'";"";"";"";"'.$ragione_sociale.'";"";"'.$row['indirizzo'].'";"'.$row['cap'].'";"'.$row['localita'].'";"'.$row['provincia'].'";"";"'.$row['telefono_1'].'";"'.$row['telefono_2'].'";"'.$row['fax'].'";"'.$row['indirizzo_email'].'";"";"'.$row['codice_nazione'].'";"'.$row['cognome_cliente'].'";"'.$row['nome_cliente'].'";"";"";"";"";"";"";"";"";"'.$pagamento.'";"";"";"";"";"";"";"";"";"";"";"";"";"";"'.$ragione_sociale.'";"";"'.$row['spedizione_indirizzo'].'";"'.$row['spedizione_cap'].'";"'.$row['spedizione_citta'].'";"'.$row['spedizione_provincia'].'";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"1"';
			
			$riga_cli .= "\n";
			
			$riga_cli .= '"'.$row['identificatore_univoco_cliente'].'";"'.$row['partita_iva_cliente'].'";"'.$row['codice_fiscale'].'";"";"";"";"'.$spedizione_ragione_sociale.'";"";"'.$row['spedizione_indirizzo'].'";"'.$row['spedizione_cap'].'";"";"'.$row['spedizione_provincia'].'";"";"";"";"";"'.$row['indirizzo_email'].'";"";"'.$row['codice_nazione'].'";"";"";"";"";"";"";"";"";"";"";"'.$pagamento.'";"";"";"";"";"";"";"";"";"";"";"";"";"";"'.$spedizione_ragione_sociale.'";"";"'.$row['spedizione_indirizzo'].'";"'.$row['spedizione_cap'].'";"'.$row['spedizione_citta'].'";"'.$row['spedizione_provincia'].'";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"1"';

			$riga_cli .= "\n";
			
			/* esolver */
			
			if($row['e_azienda'] == 1) {
				
			}
			else {
				$ragione_sociale = $row['nome_cliente']." ".$row['cognome_cliente'];
			}

			if(strpos(strtolower($ragione_sociale), 'srl') !== false || strpos(strtolower($ragione_sociale), 's.r.l') !== false || strpos(strtolower($ragione_sociale), 's.p.a') !== false || strpos(strtolower($ragione_sociale), ' spa') !== false || strpos(strtolower($ragione_sociale), ' spa ') !== false)
				$tipo_soggetto = 1;
			else if(strpos(strtolower($ragione_sociale), ' sas') !== false || strpos(strtolower($ragione_sociale), ' sas ') !== false || strpos(strtolower($ragione_sociale), 's.a.s') !== false || strpos(strtolower($ragione_sociale), 's.n.c') !== false || strpos(strtolower($ragione_sociale), 'snc') !== false)
				$tipo_soggetto = 2;
			else if($row['partita_iva_cliente'] != $row['codice_fiscale'])
				$tipo_soggetto = 3;
			else
				$tipo_soggetto = 4;
			
			
			$gr_d = Db::getInstance()->getValue('
				SELECT id_default_group 
				FROM customer 
				WHERE id_customer = '.$identificatore_univoco_cliente
			);
			
			if($gr_d == 16)
				$tipo_soggetto = 4;
			
			$ragione_sociale_e = explode(';;',wordwrap($ragione_sociale, 30, ";;", true));
			
			$ragione_sociale_prima_parte = $ragione_sociale_e[0];
			$ragione_sociale_seconda_parte = $ragione_sociale_e[1];
			
			
			$spedizione_ragione_sociale_e = explode(';;',wordwrap($spedizione_ragione_sociale, 30, ";;", true));
			
			$spedizione_ragione_sociale_prima_parte = $spedizione_ragione_sociale_e[0];
			$spedizione_ragione_sociale_seconda_parte = $spedizione_ragione_sociale_e[1];
			
			$indirizzo_e = explode(';;',wordwrap($row['indirizzo'], 35, ";;", true));
			
			$indirizzo_prima_parte = $indirizzo_e[0];
			$indirizzo_seconda_parte = $indirizzo_e[1];
			
			if($row['codice_nazione'] == 'IT')
				$codice_iva_agevolata = '411';
			else
				$codice_iva_agevolata = '412';
			
			if($tipo_soggetto == 3 && $codice_iva_agevolata == '411')
				$stato_nascita = 'IT';
			else
				$stato_nascita = '';
			
			$size_ordini = Db::getInstance()->executeS('
				SELECT id_customer 
				FROM orders 
				WHERE id_customer = '.$row['identificatore_univoco_cliente']
			);

			if(sizeof($size_ordini) == 1)
				$nuovo = '*';
			
			$pagamento_cli = Db::getInstance()->getValue('
				SELECT pagamento 
				FROM customer_amministrazione 
				WHERE id_customer = '.$identificatore_univoco_cliente
			);		
			
			if($pagamento_cli == '' || !isset($pagamento_cli))
				$pagamento_cli = $pagamento_e;
			
			// esolver
			if($pagamento_cli == "Pagamento alla consegna (COD)") {
				$pagamento_cli_e = "X002"; 
			}

			else if($pagamento_cli == "Bonifico Bancario" || $pagamento_cli == "Bonifico bancario" || $pagamento_cli == "Bonifico") {
				$pagamento_cli_e = "BB369"; 
			}
			else if($pagamento_cli == "Bonifico 30 gg. fine mese") {
				$pagamento_cli_e = "BB430"; 
			}

			else if($pagamento_cli == "Bonifico 60 gg. fine mese") {
				$pagamento_cli_e = "BB431"; 
			}
			
			else if($pagamento_cli == "Bonifico 90 gg. fine mese") {
				$pagamento_cli_e = "BB432"; 
			}
			else if($pagamento_cli == "Bonifico 30 gg. 15 mese successivo") {
				$pagamento_cli_e = "BB463"; 
			}
			else if($pagamento_cli == "R.B. 30 GG. D.F. F.M.") {
				$pagamento_cli_e = "RB230"; 
			}
			
			else if($pagamento_cli == "R.B. 60 GG. D.F. F.M.") {
				$pagamento_cli_e = "RB231"; 
			}
			
			else if($pagamento_cli == "R.B. 90 GG. D.F. F.M.") {
				$pagamento_cli_e = "RB232"; 
			}
			
			else if($pagamento_cli == "R.B. 30 GG. 5 mese successivo") {
				$pagamento_cli_e = "RB253"; 
			}
			else if($pagamento_cli == "R.B. 30 GG. 10 mese successivo") {
				$pagamento_cli_e = "RB250"; 
			}
			else if($pagamento_cli == "R.B. 60 GG. 5 mese successivo") {
				$pagamento_cli_e = "RB258"; 
			}
			else if($pagamento_cli == "R.B. 60 GG. 10 mese successivo") {
				$pagamento_cli_e = "RB249"; 
			}
			
			else if($pagamento_cli == "Contrassegno") {
				$pagamento_cli_e = "X002"; 
			}

			else if($pagamento_cli == "Contrassegno") {
				$pagamento_cli_e = "X002"; 
			}
			
			else if($pagamento_cli == "Carta Amazon") {
				$pagamento_cli_e = "X011"; 
			}
			
			else if($pagamento_cli == "Carta ePrice") {
				$pagamento_cli_e = "X012"; 
			}
			
			else if($pagamento_cli == "PayPal") {
				$pagamento_cli_e = "X013"; 
			}
			
			else if($pagamento_cli == "GestPay") {
				// gestpay
				$pagamento_cli_e = "X007"; 
			}
			else
			{
				$pagamento_cli_e = "BB369"; 
				
			}
			//fine esolver
			
		
			/*$riga_cli_e = 'GEN|'.$row['identificatore_univoco_cliente'].'|'.$row['partita_iva_cliente'].'|'.$row['codice_fiscale'].'|'.($row['partita_iva_cliente'] != '' ? 0 : 1).'||'.($row['gruppo_cliente'] == 5 ? '1' : '0').'||'.($row['gruppo_cliente'] == 5 ? '4' : $tipo_soggetto).'|'.$ragione_sociale_prima_parte.'|'.$ragione_sociale_seconda_parte.'|'.$indirizzo_prima_parte.'|'.$indirizzo_seconda_parte.'|'.$row['cap'].'|'.$row['localita'].'||'.$row['provincia'].'|'.$row['codice_nazione'].'|'.$row['telefono_1'].'|'.$row['telefono_2'].'|'.$row['fax'].'|'.$row['indirizzo_email'].'|'.$row['nome_cliente'].' '.$row['cognome_cliente'].'|'.$row['cognome_cliente'].'|'.$row['nome_cliente'].'|'.'|'.'|'.'|'.'|'.$stato_nascita.'|'.$codice_iva_agevolata.'|'.$row['codice_nazione'].$row['provincia'].'|'.$row['codice_nazione'].'-'.$row['provincia_esteso'].'|||'.$nuovo.'|'.$pagamento_cli_e.'|'."\n";
			*/
			
			// 1.1.2019 campo 7: pa = 1 , codice univoco = 2, estero = 0, 3 pec, 4 altro canale
			// campo 8: pa (1) = codice univoco ufficio, altri (2) = cod. destinatario sdi
			$fte = 2;
			
			if($row['gruppo_cliente'] == 5)
				$fte = 1;
			else {
				if($row['codice_univoco'] != '')
					$fte = 2;
				else if($row['pec'] != '')
					$fte = 2;
			}
			
			if($tipo_ordine == 0)
				$estero_fte = 711;
			
			if($row['codice_nazione'] != 'IT') {
				$fte = 0;
				
				if($tipo_ordine == 0)
					$estero_fte = 717;
			}
			
			if($tipo_ordine == 1)
				$estero_fte = 710;
			
			$zero_o_uno = 0;
			
			if($row['partita_iva_cliente'] != '')
				$zero_o_uno = 0;
			else
				$zero_o_uno = 1;
			
			if($row['gruppo_cliente'] == 5) {
				$zero_o_uno = 0;
				$tipo_soggetto = 4;
			}

			$row['cognome_cliente'] = '';
			$row['nome_cliente'] = '';
			
			$riga_cli_e = 'GEN|'.$row['identificatore_univoco_cliente'].'|'.$row['partita_iva_cliente'].'|'.$row['codice_fiscale'].'|'.$zero_o_uno.'||'.$fte.'|'.($row['codice_univoco'] != '' ? $row['codice_univoco'] : '').'|'.$tipo_soggetto.'|'.$ragione_sociale_prima_parte.'|'.$ragione_sociale_seconda_parte.'|'.$indirizzo_prima_parte.'|'.$indirizzo_seconda_parte.'|'.$row['cap'].'|'.$row['localita'].'||'.$row['provincia'].'|'.$row['codice_nazione'].'|'.$row['telefono_1'].'|'.$row['telefono_2'].'|'.$row['fax'].'|'.$row['indirizzo_email'].'|'.$row['nome_cliente'].' '.$row['cognome_cliente'].'|'.$row['cognome_cliente'].'|'.$row['nome_cliente'].'|'.'|'.'|'.'|'.'|'.$stato_nascita.'|'.$codice_iva_agevolata.'|'.$row['codice_nazione'].$row['provincia'].'|'.$row['codice_nazione'].'-'.$row['provincia_esteso'].'|||'.$nuovo.'|'.$pagamento_cli_e.'||'.$row['pec'].''."\n";
				
			if($row['spedizione_nazione'] == "IT")
				$codice_esterno_fornitore = 33153; // GLS
			else
				$codice_esterno_fornitore = 33153; // GLS
				
			//$codice_esterno_fornitore = 35432; // TNT
					
			$corriere_nome = Db::getInstance()->getValue('
				SELECT name 
				FROM carrier 
				WHERE id_carrier = '.$row['corriere']
			);
			
			if(strpos($corriere_nome, 'BRT') !== false)
				$codice_esterno_fornitore = 39140;
			
			if(strpos($corriere_nome, 'Ritiro') !== false)
				$codice_trasporto_a_mezzo = '2';
			else
				$codice_trasporto_a_mezzo = '3';
			
			if($codice_trasporto_a_mezzo == '2')
				$codice_esterno_fornitore = '';
			
			$nuovo = '';
			
			$cart_subject = Db::getInstance()->getValue('
				SELECT riferimento 
				FROM cart 
				WHERE id_cart = '.$row['id_cart']
			);

			$cart_subject_2 = Db::getInstance()->getValue('
				SELECT name 
				FROM cart 
				WHERE id_cart = '.$row['id_cart']
			);

			if(substr($cart_subject,0,3) == 'BDL') {
				$bdl_dati = Db::getInstance()->getRow('
					SELECT * 
					FROM bdl 
					WHERE id_bdl = '.str_replace('BDL','',$cart_subject)
				);

				$bdl_ticket = Customer::trovaSigla(substr($bdl_dati['riferimento'],1), substr($bdl_dati['riferimento'],0,1));
				$data_ticket_r = Db::getInstance()->getValue('
					SELECT date_add 
					FROM customer_thread 
					WHERE id_customer_thread = '.substr($bdl_dati['riferimento'],1)
				);
				
				if($data_ticket_r == '')
					$data_ticket = $data_nuova_e;
				else {
					$data_ticket_r = explode(' ',$data_ticket_r);
					$data_ticket_r = explode('-',$data_ticket_r[0]);
					$data_ticket = $data_ticket_r[2].'/'.$data_ticket_r[1].'/'.$data_ticket_r[0];
				}	
				
				$riga_ord_bdl = '';
				
				if($bdl_dati['descrizione'] != '')
					$riga_ord_bdl .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||20|||||||||||||Descrizione: '.(substr($bdl_dati['descrizione'],0,5) == 'Altro' ? substr($bdl_dati['descrizione'],5,strlen($bdl_dati['descrizione'])) : $bdl_dati['descrizione']).'||EOR'."\n";
				
				if($bdl_dati['motivo_chiamata'] != '')
					$riga_ord_bdl .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||20|||||||||||||Motivo chiamata: '.$bdl_dati['motivo_chiamata'].'||EOR'."\n";
				
				if($bdl_dati['guasto'] != '' && $bdl_dati['guasto'] != 'Nessuna')
					$riga_ord_bdl .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||20|||||||||||||Guasto: '.(substr($bdl_dati['guasto'],0,5) == 'Altro' ? substr($bdl_dati['guasto'],5,strlen($bdl_dati['guasto'])) : $bdl_dati['guasto']).'||EOR'."\n";
				
				if($bdl_dati['causa_guasto'] != '' && $bdl_dati['causa_guasto'] != 'Nessuno')
					$riga_ord_bdl .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||20|||||||||||||Causa guasto: '.(substr($bdl_dati['causa_guasto'],0,5) == 'Altro' ? substr($bdl_dati['causa_guasto'],5,strlen($bdl_dati['causa_guasto'])) : $bdl_dati['causa_guasto']).'||EOR'."\n";

			}
			else if(substr($cart_subject_2,0,9) == 'Contratto') {
				$contratto_dati = Db::getInstance()->getRow('
					SELECT * 
					FROM contratto_assistenza 
					WHERE id_contratto = '.str_replace('Contratto N. ','',$cart_subject_2)
				);
				
				$competenza = 'Competenza: dal '.date('d/m/Y',strtotime($contratto_dati['data_inizio'])).' al '.date('d/m/Y',strtotime($contratto_dati['data_fine'])).'';
				
				$riga_ord_bdl = '';
				
				$riga_ord_bdl .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||20|||||||||||||'.$cart_subject_2.'||EOR'."\n";
				
				$riga_ord_bdl .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||20|||||||||||||'.$competenza.'||EOR'."\n";	
			}
			else {
				$riga_ord_bdl = '';
				$bdl_ticket = '';
				$data_ticket = '';
			}
			
			/*if($row['spedizione_nazione'] != 'IT')
				$tipo_ordine = 4;
			*/
			
			// ultimo campo: se estero 717, sennò non mandarlo
			
			$riga_ord_e = 'TES|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'||||'.$pagamento_e.'|'.number_format($row['importo'],2,",","").'|'.$tipo_ordine.'||'.$casella_prime.'|||'.$spedizione_ragione_sociale_prima_parte.'|'.$spedizione_ragione_sociale_seconda_parte.'|'.$row['spedizione_indirizzo'].'|'.$row['spedizione_cap'].'|'.$row['spedizione_citta'].'||'.$row['spedizione_provincia'].'|'.$row['spedizione_nazione'].'||1|VENDITA|1|FRANCO ASSEGNATO|1|SCATOLA|'.$codice_trasporto_a_mezzo.'|'.($codice_trasporto_a_mezzo == 1 ? 'VETTORE' : 'PROPRIO').'|'.$codice_esterno_fornitore.'|'.$trasporto.'|||||||||||||'.$stato_conferma.'|'.$magazzino.'|'.$data_ticket.'|'.$bdl_ticket.'||'.$estero_fte.'|EOR'."\n";
			
			$riga_ord_e .= $riga_ord_prod_e;
			
			//COMMDIL
			//COMMISSIONI PER PAGAMENTO DILAZIONATO 
			
			
			if($row['commissioni'] > 0) {	
				if($pagamento == "GestPay")
					$riga_ord_e .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||40|COMMIS|COMMISSIONE PAGAMENTO CARTA|1|'.number_format($row['commissioni'],2,",","").'|||||'.$stato_conferma.'|'.$magazzino.'|||||EOR'."\n";
				else
					$riga_ord_e .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||40|COMMDIL|COMMISSIONI PER PAGAMENTO DILAZIONATO|1|'.number_format($row['commissioni'],2,",","").'|||||'.$stato_conferma.'|'.$magazzino.'|||||EOR'."\n";
			}
			if($spedizione > 0) {	
				$riga_ord_e .= 'RIG|'.$data_nuova_e.'|'.$row['codice_ordine'].'|'.$row['identificatore_univoco_cliente'].'|||||||||||||||||||||||||||||||||40|TRASP|Trasporto|1|'.$spedizione.'|||||'.$stato_conferma.'|'.$magazzino.'|||||EOR';
			
				$riga_ord_e .= "\n";
			
			
			}
			$riga_ord_e .= $riga_ord_bdl;
			/* fine esolver */
	
		}
			
		$riga_ordine .= '"'.$identificatore_univoco_cliente.'";"'.$partita_iva_cliente.'";"'.$codice_fiscale.'";"'.$identificatore_univoco_cliente.'";"";"'.$codice_ordine.'";"'.$data_nuova.'";"'.$pagamento.'";"";"'.$spedizione_ragione_sociale.'";"";"'.$spedizione_indirizzo.'";"'.$spedizione_cap.'";"'.$spedizione_citta.'";"'.$spedizione_provincia.'";"";"";"";"";"";"";"";"";"";"";"";"'.$codice_ordine.'";"TRASP";"";"";"1";"";"";"";"";"";"";"";"'.$spedizione.'";"";"";"";"";"";"";"";"";"0"';
		
		$ccod = $codice_ordine;
		
		
		$file_csv = "ORD.$ccod.csv";
		$file_csv2 = "CLI.$ccod.csv";

		global $cookie;

		$log_recuperi=fopen("/var/www/vhosts/ezdirect.it/httpdocs/import/log-recuperi-csv.txt","a+");
		$riga_log = $cookie->id_employee." - ".$ccod." - ".date("Y-m-d H:i:s")."\n";
		fwrite($log_recuperi,$riga_log);

		/* esolver */
		
		$file_csv_e = "ORD.$ccod.csv";
		$file_csv2_e = "CLI.$ccod.csv";
				
		$file_csv3_e_prov = "ORD".$ccod."-PROV.csv";
		$file_csv4_e_prov = "CLI".$ccod."-PROV.csv";	

		$file_csv3_e = "ORD.".$ccod.".csv";
		$file_csv4_e = "CLI.".$ccod.".csv";		
		
		if(!is_dir("/var/www/vhosts/ezdirect.it/httpdocs/csv-ordini-nuovi-esolver")) {
		mkdir("/var/www/vhosts/ezdirect.it/httpdocs/csv-ordini-nuovi-esolver");
		}
		else {
		}

		$file_csv3_e_prov_stream=fopen("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/".$file_csv3_e_prov,"a+");
		fwrite($file_csv3_e_prov_stream,$riga_ord_e);
		fclose("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/".$file_csv3_e_prov);
		
		$file_csv4_e_prov_stream=fopen("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/".$file_csv4_e_prov,"a+");
		fwrite($file_csv4_e_prov_stream,$riga_cli_e);
		fclose("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/".$file_csv4_e_prov);
		
		
		$file_csv3_e_prov_stream=fopen("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolvertest/".$file_csv3_e_prov,"a+");
		fwrite($file_csv3_e_prov_stream,$riga_ord_e);
		fclose("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolvertest/".$file_csv3_e_prov);
		
		$file_csv4_e_prov_stream=fopen("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolvertest/".$file_csv4_e_prov,"a+");
		fwrite($file_csv4_e_prov_stream,$riga_cli_e);
		fclose("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolvertest/".$file_csv4_e_prov);
		
		
		$data = file_get_contents("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/".$file_csv3_e_prov);
		$data = mb_convert_encoding($data, 'ISO-8859-1');
		file_put_contents("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/".$file_csv3_e, $data);
		
		$data_2 = file_get_contents("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/".$file_csv4_e_prov);
		$data_2 = mb_convert_encoding($data_2, 'ISO-8859-1');
		file_put_contents("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/".$file_csv4_e, $data_2);
		
		unlink("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/".$file_csv3_e_prov);
		unlink("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/".$file_csv4_e_prov);
		
		unlink("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/ORD..csv");
		unlink("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/CLI..csv");
		
		Db::getInstance()->executeS("UPDATE orders SET csv = 1 WHERE id_order = ".$ccod."");
		
		Product::anagraficheArticoliEsolver($ccod);
		/* fine esolver */
	}
	
	public function showProductTooltip($id_product)
	{
		global $cookie;
		$products = Db::getInstance()->getRow('
			SELECT product.id_product, id_manufacturer, reference, wholesale_price AS acquisto, sconto_acquisto_1 AS sc_acq_1, sconto_acquisto_2 AS sc_acq_2, sconto_acquisto_3 AS sc_acq_3, listino AS listino, price AS vendita, stock_quantity AS magazzino, supplier_quantity AS allnet, intracom_quantity AS intracom, itancia_quantity AS itancia, asit_quantity AS asit, amazon_quantity AS amazon_qt, esprinet_quantity as esprinet, arrivo_esprinet_quantity as qt_arrivo_esprinet, attiva_quantity as attiva, arrivo_attiva_quantity as qt_arrivo_attiva, ordinato_quantity AS qt_ordinato, impegnato_quantity AS qt_impegnato, quantity AS qt_tot, arrivo_quantity AS qt_arrivo, arrivo_intracom_quantity AS qt_arrivo_intracom, data_arrivo, data_arrivo_intracom, data_arrivo_esprinet, date_available, id_image 
			FROM product 
			LEFT JOIN (
				SELECT * 
				FROM image 
				WHERE cover = 1
			) image ON product.id_product = image.id_product 
			WHERE product.id_product = '.$id_product.'
		');
		
		$arrivo_ordinato = Db::getInstance()->getValue('SELECT data_arrivo_ordinato FROM product WHERE id_product = '.$id_product);
							
		if($arrivo_ordinato == '')
			$date_arrivo_ordinato_riga = '<tr><td colspan=\'3\'>Nessun ordine in arrivo</td></tr>';
		else
		{
			$arrivo_ordinato = unserialize($arrivo_ordinato);
			
			foreach($arrivo_ordinato as $ao)
				$date_arrivo_ordinato_riga .= '<tr><td style=\'text-align:right;\'>'.$ao['ordine'].'</td><td style=\'text-align:right;\'>'.round($ao['quantita'],0).'</td><td>'.Db::getInstance()->getValue('SELECT company FROM customer WHERE codice_esolver = \''.$ao['fornitore'].'\'').'</td><td>'.date('d/m/Y',strtotime($ao['data'])).'</td></tr>';
		}
		
		$qta_ord_clienti = Product::calcolaImpegnato($id_product);
						
		if(!$qta_ord_clienti || $qta_ord_clienti == '')
			$qta_ord_clienti = 0;
		
		$qtas = Db::getInstance()->executeS("SELECT price, reduction, specific_price_name FROM specific_price WHERE (specific_price_name = 'sc_qta_1' OR specific_price_name = 'sc_qta_2' OR specific_price_name = 'sc_qta_3' OR specific_price_name = 'sc_riv_1' OR specific_price_name = 'sc_riv_2' OR specific_price_name = 'sc_riv_3') AND id_product = '".$id_product."'");
		
		$scorta_minima = Db::getInstance()->getValue('
			SELECT scorta_minima 
			FROM product 
			WHERE id_product = '.$id_product
		);
							
		$scorta_minima = (int)$scorta_minima;
		
		$img_scorta = '';
		if($sc_att < $scorta_minima)
			$img_scorta = "<img style='border:2px solid red' src='../img/admin/error2.png' alt='Sotto scorta' title='Sotto scorta'>";
		else {
			if($products['qt_ordinato'] > 0 && $sc_att > $scorta_minima)
				$img_scorta = "<img style='border:2px solid green' src='../img/admin/delivery.gif' alt='In arrivo' title='In arrivo'>";
			else
				$img_scorta = '';
		}

		foreach($qtas as $qta) {
			if($qta['specific_price_name'] == 'sc_qta_1')
				$sc_qta_1_r = $qta;
			else if($qta['specific_price_name'] == 'sc_qta_2')
				$sc_qta_2_r = $qta;
			else if($qta['specific_price_name'] == 'sc_qta_3')
				$sc_qta_3_r = $qta;
			else if($qta['specific_price_name'] == 'sc_riv_1')
				$sc_riv_1_r = $qta;
			else if($qta['specific_price_name'] == 'sc_riv_2')
				$sc_riv_2_r = $qta;
			else if($qta['specific_price_name'] == 'sc_riv_3')
				$sc_riv_3_r = $qta;
		}
		
		$id_supplier = Db::getInstance()->getValue("
			SELECT id_supplier 
			FROM product 
			WHERE id_product = '".$id_product."'
		");
		
		$spec_price = Db::getInstance()->getValue("
			SELECT price 
			FROM specific_price sp 
			WHERE sp.specific_price_name = '' 
				AND sp.to > '".date('Y-m-d H:i:s')."' 
				AND sp.id_product = '".$id_product."'
		");
		
		$spec_whole = Db::getInstance()->getValue("
			SELECT wholesale_price 
			FROM specific_price_wholesale sp 
			WHERE sp.to > '".date('Y-m-d H:i:s')."' 
				AND sp.id_product = '".$id_product."'
		");
		
		$sc_qta_1 = $sc_qta_1_r['price'] - ($sc_qta_1_r['price'] * $sc_qta_1_r['reduction']);
		$sc_qta_2 = $sc_qta_2_r['price'] - ($sc_qta_2_r['price'] * $sc_qta_2_r['reduction']);
		$sc_qta_3 = $sc_qta_3_r['price'] - ($sc_qta_3_r['price'] * $sc_qta_3_r['reduction']);
		$sc_riv_1 = $sc_riv_1_r['price'] - ($sc_riv_1_r['price'] * $sc_riv_1_r['reduction']);
		$sc_riv_2 = $sc_riv_2_r['price'] - ($sc_riv_2_r['price'] * $sc_riv_2_r['reduction']);
		$sc_riv_3 = $sc_riv_3_r['price'] - ($sc_riv_3_r['price'] * $sc_riv_3_r['reduction']);
		
		$supplier = Db::getInstance()->getValue('SELECT name FROM supplier WHERE id_supplier = '.$id_supplier);
		
		$tooltip = 
		'<div style=\'color:#000; z-index:99999999; width:970px !important; height:260px\'><h2 style=\'display:block; float:left\'>'.$products['reference'].'</h2><div style=\'margin-left:50px; float:left\'> '.str_replace('|','',str_replace('"',"''",Db::getInstance()->getValue('SELECT name FROM product_lang WHERE id_lang = 5 AND id_product = '.$products['id_product']))).'<!-- <img src=\'https://www.ezdirect.it/img/'.($products['id_image'] > 0 ? 'p/'.$products['id_product'].'-'.$products['id_image'] : 'm/'.$products['id_manufacturer']).'-small.jpg\' /> --> </div><div style=\'float:left; margin-top:2px; margin-left:50px\'>'.($cookie->profile == 7 ? '' : 'Fornitore: <strong>'.$supplier.'</strong>').'</div><div class=\'clear\'></div><div style=\'float:left\'></div><div style=\'float:left\'><strong>Prezzi</strong><br /><style type=\'text/css\'>table.tooltip_table tr td, table.tooltip_table tr th { text-align:right }</style><table class=\'tooltip_table table\' style=\'width:670px; float:left\'><tr><th>Listino</th><th>'.($cookie->profile == 7 ? '' : 'Acq.').'</th><th>Vnd. Web</th><th>Qta 1</th><th>Qta2</th><th>Qta 3</th><th>Riv.</th><th>Spec. Vnd.</th><th>'.($cookie->profile == 7 ? '' : 'Spec. Acq.').'</th></tr><tr><td>'.Tools::displayPrice($products['listino']).'</td><td>'.($cookie->profile == 7 ? '' : ($cookie->id_employee == 1 || $cookie->id_employee == 2  || $cookie->id_employee == 3 || $cookie->id_employee == 6 || $cookie->id_employee == 15 || $cookie->id_employee == 7 ? Tools::displayPrice(($products['acquisto'] > 0 ? $products['acquisto'] : ($products['listino']*(100-$products['sc_acq_1'])/100)*((100-$products['sc_acq_2'])/100)*((100-$products['sc_acq_3'])/100))).'' : 'N.D.')).'</td><td>'.Tools::displayPrice($products['vendita']).'</td><td>'.($sc_qta_1 > 0 ? Tools::displayPrice($sc_qta_1) : '--').'</td><td>'.($sc_qta_2 > 0 ? Tools::displayPrice($sc_qta_2) : '--').'</td><td>'.($sc_qta_3> 0 ? Tools::displayPrice($sc_qta_3) : '--').'</td><td>'.($sc_riv_1 > 0 ? Tools::displayPrice($sc_riv_1) : '--').'</td><td>'.($spec_price > 0 ? '<font style=\'color:red\'>'.Tools::displayPrice($spec_price).'</font>' : '--').'</td><td>'.($cookie->profile == 7 ? '' : ($spec_whole > 0 ? '<font style=\'color:blue\'>'.Tools::displayPrice($spec_whole).'</font>' : '--')).'</td></tr></table><br /><script>         $(document).ready(function() {          $(\'.span-reference-2\').tooltipster({interactive: true});         });        </script>          <table class=\'table tooltip_table\'  cellpadding=\'0\' cellspacing=\'0\' style=\'width:650px; border:1px solid #000; float:left; width:70%\'><tr><th style=\'background: #ebebeb; text-align:center; border-bottom:1px solid #000\' colspan=\'8\'>Disponibilit&agrave;</th></tr>        <tr>        <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right; width:150px\'>Magazzino EZ</th>        <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right\'>'.$products['magazzino'].'</th>        <th style=\'padding:0px; border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right; width:300px\'>                  <table style=\'width:100%; border-collapse:collapse; margin-top:0px; height:26px; margin-bottom:0px;border-bottom:0px\'><tr><th style=\'border-right: 1px solid #bbb; border-bottom:0px solid #000; text-align:right;\'>Netto<img class=\'img_control\' src=\'https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif\' alt=\'Netto = Magazzino EZ - Qta ordinata dai clienti (sito)\' title=\'Netto = Magazzino EZ - Qta ordinata dai clienti (sito)\' /></th>         <th style=\'border-right: 1px solid #bbb; border-bottom:0px solid #000;text-align:right\'>'.($products['magazzino'] - $qta_ord_clienti).'</th>                  <th style=\'border-right: 0px solid #bbb; border-bottom:0px solid #000;text-align:right\'>Qta ordinata dai clienti (sito)</th></tr></table>        </th>        <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right\'>'.$qta_ord_clienti.'</span>                <div class=\'tooltip_templates\' style=\'display:none\'>        <span id=\'qta_ord_clienti_tooltip\'><table class=\'table\' style=\'width:250px\'><tr><th style=\'color:#000 !important\'>Ordine</th><th style=\'color:#000 !important\'>Stato</th><th style=\'color:#000 !important\'>Quantit&agrave;</th></tr></table></span>        </div>        </th>        <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right; width:300px\'>Qta impegnata ord. caricati su gestionale (eSolver)</th>        <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right\'>'.$products['qt_impegnato'].'</th>        </tr>                <tr>        <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right\'>Mag. Amazon</th>        <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right\'>'.$products['amazon_qt'].'</th>        <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right\' >Ordinato al fornitore</th>                <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right\'>'.$products['qt_ordinato'].'</th>        <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right\' colspan=\'1\'>Mag. EZ + Ordinato al fornitore - Impegnato sito</th>        <th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right\' >'.($products['magazzino'] + $products['qt_ordinato'] - $qta_ord_clienti).'</th>                        </tr>                  '.($cookie->profile == 7 ? '' : '              <tr>        <'.($id_supplier == 11 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Allnet</'.($id_supplier == 11 ? 'th' : 'td').'>        <'.($id_supplier == 11 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['allnet'].'</'.($id_supplier == 11 ? 'th' : 'td').'>        <'.($id_supplier == 11 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>In arrivo Allnet</'.($id_supplier == 11 ? 'th' : 'td').'>        <'.($id_supplier == 11 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['qt_arrivo'].'</'.($id_supplier == 11 ? 'th' : 'td').'>        <'.($id_supplier == 11 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Data arr. Allnet</'.($id_supplier == 11 ? 'th' : 'td').'>        <'.($id_supplier == 11 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.(($from = $products['data_arrivo'] AND $from != '1946-01-01') ? ($from == '0000-00-00' || $from == '1970-01-01' ? 'ND' : date('d-m-y',strtotime($from))) : date('d-m-y')).'</'.($id_supplier == 11 ? 'th' : 'td').'>                </tr> <tr>        <'.($id_supplier == 38 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Esprinet</'.($id_supplier == 38 ? 'th' : 'td').'>        <'.($id_supplier == 38 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['esprinet'].'</'.($id_supplier == 38 ? 'th' : 'td').'>        <'.($id_supplier == 38 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>In arrivo Esprinet</'.($id_supplier == 38 ? 'th' : 'td').'>        <'.($id_supplier == 38 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['qt_arrivo_esprinet'].'</'.($id_supplier == 38 ? 'th' : 'td').'>        <'.($id_supplier == 38 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Data arr. Esprinet</'.($id_supplier == 38 ? 'th' : 'td').'>        <'.($id_supplier == 38 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.(($from = $products['data_arrivo_esprinet'] AND $from != '1946-01-01') ? ($from == '0000-00-00' || $from == '1970-01-01' ? 'ND' : date('d-m-y',strtotime($from))) : date('d-m-y')).'</'.($id_supplier == 38 ? 'th' : 'td').'>                </tr>  <tr>        <'.($id_supplier == 1450 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Attiva</'.($id_supplier == 1450 ? 'th' : 'td').'>        <'.($id_supplier == 1450 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['attiva'].'</'.($id_supplier == 1450 ? 'th' : 'td').'>        <'.($id_supplier == 1450 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>In arrivo Attiva</'.($id_supplier == 1450 ? 'th' : 'td').'>        <'.($id_supplier == 1450 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['qt_arrivo_attiva'].'</'.($id_supplier == 1450 ? 'th' : 'td').'>        <'.($id_supplier == 1450 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Data arr. Attiva</'.($id_supplier == 1450 ? 'th' : 'td').'>        <'.($id_supplier == 1450 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.(($from = $products['data_arrivo_attiva'] AND $from != '1946-01-01') ? ($from == '0000-00-00' || $from == '1970-01-01' ? 'ND' : date('d-m-y',strtotime($from))) : date('d-m-y')).'</'.($id_supplier == 1450 ? 'th' : 'td').'>                </tr>              <tr>        <'.($id_supplier == 42 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Itancia</'.($id_supplier == 42 ? 'th' : 'td').'>        <'.($id_supplier == 42 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['itancia'].'</'.($id_supplier == 42 ? 'th' : 'td').'>        <'.($id_supplier == 42 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>In arrivo Itancia</'.($id_supplier == 42 ? 'th' : 'td').'>        <'.($id_supplier == 42 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>0</'.($id_supplier == 42 ? 'th' : 'td').'>        <'.($id_supplier == 42 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Data arr. Itancia:</'.($id_supplier == 42 ? 'th' : 'td').'>        <'.($id_supplier == 42 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>ND</'.($id_supplier == 42 ? 'th' : 'td').'>                </tr>                <tr>        <'.($id_supplier == 95 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Intracom</'.($id_supplier == 95 ? 'th' : 'td').'>        <'.($id_supplier == 95 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['intracom'].'</'.($id_supplier == 95 ? 'th' : 'td').'>        <'.($id_supplier == 95 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>In arrivo Intracom</'.($id_supplier == 95 ? 'th' : 'td').'>        <'.($id_supplier == 95 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['qt_arrivo_intracom'].'</'.($id_supplier == 95 ? 'th' : 'td').'>        <'.($id_supplier == 95 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>Data arr. Intracom:</'.($id_supplier == 95 ? 'th' : 'td').'>        <'.($id_supplier == 95 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.(($from_intracom = $products['data_arrivo_intracom'] AND $from_intracom != '1946-01-01') ? ($from_intracom == '0000-00-00' || $from_intracom == '1970-01-01' ? 'ND' : date('d-m-y',strtotime($from_intracom))) : date('d-m-y')).'</'.($id_supplier == 95 ? 'th' : 'td').'>                </tr>                <tr>        <'.($id_supplier == 76 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>ASIT</'.($id_supplier == 76 ? 'th' : 'td').'>        <'.($id_supplier == 76 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'>'.$products['asit'].'</'.($id_supplier == 76 ? 'th' : 'td').'>        <'.($id_supplier == 76 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'></'.($id_supplier == 76 ? 'th' : 'td').'>        <'.($id_supplier == 76 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'></'.($id_supplier == 76 ? 'th' : 'td').'>        <'.($id_supplier == 76 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'></'.($id_supplier == 76 ? 'th' : 'td').'>        <'.($id_supplier == 76 ? 'th' : 'td').' style=\'border-right: 1px solid #bbb; text-align:right\'></'.($id_supplier == 76 ? 'th' : 'td').'></tr> <tr> <td style=\'background: #ebebeb;border-right: 1px solid #000; border-top: 1px solid #000; text-align:right\'><strong>Totale tutti i magazzini (escl. Amazon)</strong></td> <td style=\'background: #ebebeb;border-right: 1px solid #000; border-top: 1px solid #000; text-align:right\'>'.$products['qt_tot'].'</td><td style=\'background: #ebebeb;border-right: 1px solid #000; border-top: 1px solid #000; text-align:right; width:170px\'><strong>Tot. magazzini + Ordinato a fornitore - Impegnato sito</strong></td>        <td style=\'background: #ebebeb;text-align:right; border-right: 1px solid #000; border-top: 1px solid #000; \'>'.(($products['qt_tot'])+$products['qt_ordinato']-$products['qt_impegnato']).'</td><td style=\'background: #ebebeb;border-right: 1px solid #000; border-top: 1px solid #000; text-align:right; \'><strong>'.$img_scorta.' Scorta minima</strong></td> <td style=\'background: #ebebeb;text-align:right; border-top: 1px solid #000; \'>  '.$scorta_minima.'</td>  </tr>').'</table> 		<table class=\'table\'  cellpadding=\'0\' cellspacing=\'0\' style=\'float:left; border:1px solid #000; float:left; width:24%; margin-left:5%\'><tr><th style=\'background: #ebebeb; text-align:center; border-bottom:1px solid #000\' colspan=\'8\'>Date qta in arrivo</th></tr> 							<tr> 							<th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right;\'>ID Ord.</th> 							<th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right;\'>Qt.</th> 							<th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right; width:150px\'>Fornitore</th> 							<th style=\'border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right\'>Data</th> 							</tr> 							 							'.$date_arrivo_ordinato_riga.' 							 							</table> 		 		</div><div style=\'clear:both\'></div></div>';
		
		return $tooltip;
		
	}
	
	public function getCurrentWholesalePrice($id_product)
	{
		$special_wholesale = Db::getInstance()->getValue("
			SELECT wholesale_price 
			FROM specific_price_wholesale sp 
			WHERE sp.to > '".date('Y-m-d H:i:s')."' 
				AND sp.id_product = '".$id_product."'
		");
		
		$normal_wholesale = Db::getInstance()->getValue("
			SELECT wholesale_price 
			FROM product 
			WHERE id_product = '".$id_product."'
		");
		
		if($special_wholesale > 0)
			return $special_wholesale;
		else
			return $normal_wholesale;
		
	}
	
	public function anagraficheArticoliEsolver($id_order = 0)
	{
		$file_csv_prov = "EZ-ARTICOLI-PROV.csv";
		$file_csv = "EZ-ARTICOLI.csv";
		$file_csv_listino = "EZ-LISTINI.csv";
		$file_csv_vendita = "EZ-VENDITA.csv";
		$string_products = '';
		
		if($id_order == 0)
			$string_products = '';
		else {
			$string_products .= 'AND ((';
			$products = Db::getInstance()->executeS('
				SELECT product_id 
				FROM order_detail 
				WHERE id_order = '.$id_order
			);

			foreach($products as $product) {
				$string_products .= 'p.id_product = '.$product['product_id'].' OR ';
			}		

			$string_products .= 'p.id_product = 9999999999999) OR (p.active = 1 AND p.date_add > DATE_ADD(NOW(), INTERVAL -5 DAY)))';
		}	
		
		if(!filemtime("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/$file_csv_listino"))
			$data_ultima_modifica = date('Y-m-d 00:00:00');
		else
			$data_ultima_modifica = date('Y-m-d H:i:s', filemtime("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/$file_csv_listino"));

		$prima_riga = "Tipo record|Tipo anagrafica|Modello|Codice articolo|Codice esterno|Cod. Produttore|Cod. EAN|Cod. EAN descrizione|Cod. EAN quantità|Cod. EAN tipo|Descrizione|Descrizione estesa|Stato|Usato|Codice marca|Descrizione marca|Codice famiglia|Descrizione famiglia|Peso KG|CAcquisti Est.|CVendite Est.|Cod. Fornitore Esterno Abituale|Codice fornitore esterno|Codifica dell'articolo c/o fornitore|Tempo di approvvigionamento|Codice a barre c/o fornitore|Descr. Articolo|Note 1|Note 1|EOR\n";
		
		$prima_riga = "";
		
		$righe = '';
		$righe_listino = '';
		$righe_vendita = '';
		
		if($id_order == 0) {
			$prodotti = Db::getInstance()->executeS('
				SELECT p.*, pe.wholesale_price as pa_esolver, pl.name as nome_prodotto, m.name as costruttore, cl.name as categoria, s.name as fornitore 
				FROM product p 
				LEFT JOIN product_lang pl ON p.id_product = pl.id_product 
				LEFT JOIN product_esolver pe ON p.id_product = pe.id_product 
				LEFT JOIN manufacturer m ON p.id_manufacturer = m.id_manufacturer 
				LEFT JOIN supplier s ON p.id_supplier = s.id_supplier 
				LEFT JOIN category_lang cl ON p.id_category_default = cl.id_category
				WHERE ((p.active >= 0 AND p.date_upd > "'.date('Y-m-d', strtotime('-5 days', strtotime(date('Y-m-d')))).'")
					OR p.id_product IN (
						SELECT id_product 
						FROM storico_prezzi 
						WHERE date > "'.date('Y-m-d', strtotime('-5 days', strtotime(date('Y-m-d')))).'"
					)
				) 
					AND pl.id_lang = 5 
				GROUP BY p.id_product
			');
		}
		else {
			$prodotti = Db::getInstance()->executeS('
				SELECT p.*, pe.wholesale_price as pa_esolver, pl.name as nome_prodotto, m.name as costruttore, cl.name as categoria, s.name as fornitore 
				FROM product p 
				LEFT JOIN product_lang pl ON p.id_product = pl.id_product 
				LEFT JOIN product_esolver pe ON p.id_product = pe.id_product 
				LEFT JOIN manufacturer m ON p.id_manufacturer = m.id_manufacturer 
				LEFT JOIN supplier s ON p.id_supplier = s.id_supplier 
				LEFT JOIN category_lang cl ON p.id_category_gst = cl.id_category 
				WHERE p.reference != "" 
					AND pl.id_lang = 5 '.$string_products.' 
				GROUP BY p.id_product
			');
		}

		foreach($prodotti as $p) {
			$virtual = ProductDownload::getIdFromIdProduct($p['id_product']);

			if(($p['id_category_default'] == 119 || $p['id_category_default'] == 248 || $p['id_category_default'] == 249 || $p['id_category_default'] == 250)) {	
				$tipo_anagrafica = 4;
				$modello = 4;
			}
			else {	
				$tipo_anagrafica = 1;
				$modello = 1;
			}
			
			if($p['id_supplier'] == 11)
				$fornitore_abituale = $p['cod_fornitore_1'];
			else
				$fornitore_abituale = $p['cod_fornitore_2'];
			
			if($p['upc'] != '' && ($p['id_supplier'] != 11 && $p['id_supplier'] != 8))
				$fornitore_abituale = $p['upc'];
			
			$data_inserimento = date('dmY', strtotime($p['date_add']));
			$data_aggiornamento = date('dmY', strtotime($p['date_upd']));
			
			$codice_marca = substr(strtoupper($p['costruttore']),0,3).sprintf("%'.03d", $p['id_manufacturer']);
			$codice_categoria = substr(strtoupper($p['categoria']),0,3).sprintf("%'.03d", $p['id_category_gst']);
			$codice_fornitore = substr(strtoupper($p['fornitore']),0,2).sprintf("%'.03d", $p['id_supplier']);
			
			if($p['id_category_gst'] == 0 || $p['id_category_gst'] == '') {	
				$codice_categoria = '';
				$p['categoria'] = '';
			}
			$fornitore = 0;
			
			switch($p['id_supplier'])
			{
				case 67: $fornitore = 36501; break; 
				case 3: $fornitore = 29645; break; 
				case 4: $fornitore = 36383; break; 
				case 8: $fornitore = 35420; break; 
				case 10: $fornitore = 36462; break; 
				case 11: $fornitore = 36448; break; 
				case 12: $fornitore = 39403; break; 
				case 13: $fornitore = 36561; break; 
				case 14: $fornitore = 36559; break; 
				case 31: $fornitore = 42585; break; 
				case 32: $fornitore = 38692; break; 
				case 33: $fornitore = 41822; break; 
				case 34: $fornitore = 36847; break; 
				case 35: $fornitore = 45936; break; 
				case 36: $fornitore = 39731; break; 
				case 37: $fornitore = 35184; break; 
				case 38: $fornitore = 36541; break; 
				case 39: $fornitore = 50971; break; 
				case 40: $fornitore = 34696; break; 
				case 41: $fornitore = 33790; break; 
				case 42: $fornitore = 35732; break; 
				case 43: $fornitore = 29350; break; 
				case 44: $fornitore = 37916; break; 
				case 45: $fornitore = 55321; break; 
				case 47: $fornitore = 36373; break; 
				case 48: $fornitore = 35401; break; 
				case 49: $fornitore = 36471; break; 
				case 50: $fornitore = 30647; break; 
				case 52: $fornitore = 34316; break; 
				case 53: $fornitore = 38045; break; 
				case 54: $fornitore = 35521; break; 
				case 55: $fornitore = 36459; break; 
				case 56: $fornitore = 45255; break; 
				case 57: $fornitore = 52060; break; 
				case 58: $fornitore = 42121; break; 
				case 60: $fornitore = 55322; break; 
				case 61: $fornitore = 36307; break; 
				case 66: $fornitore = 55323; break; 
				case 68: $fornitore = 42583; break; 
				case 70: $fornitore = 53695; break; 
				case 76: $fornitore = 58350; break; 
				case 1458: $fornitore = 46215; break; 
				default: $fornitore = ''; break;
			}
	
			$codice_fornitore = 'A.451'.$codice_fornitore;
	
			if($p['pa_esolver'] > 0)
				$acquisto = $p['pa_esolver'];
			else
				$acquisto = $p['wholesale_price'];
			
			$serie = Db::getInstance()->getValue('
				SELECT value 
				FROM product p 
				JOIN feature_product fp ON p.id_product = fp.id_product 
				JOIN feature_value_lang fvl ON fp.id_feature_value = fvl.id_feature_value 
				WHERE fvl.id_lang = 5
					AND fp.id_feature = 917 
					AND p.id_product = '.$p['id_product']
			);
			
			$tipo = Db::getInstance()->getValue("
				SELECT value 
				FROM product p 
				JOIN feature_product fp ON p.id_product = fp.id_product 
				JOIN feature_value_lang fvl ON fp.id_feature_value = fvl.id_feature_value 
				JOIN feature_lang fl ON fp.id_feature = fl.ID_FEATURE 
				WHERE fvl.id_lang = 5 
					AND fl.id_lang = 5 
					AND (fl.name = 'Tipo' OR fl.name = 'Tipo radioguide' OR fl.name = 'Tipo (domotica)' OR fl.name = 'Tipo (videoconferenza)')
					AND  p.id_product = ".$p['id_product']
			);
			//if(($p['id_category_default'] == 119 || $p['id_category_default'] == 248 || $p['id_category_default'] == 249))
			//{	

			$current_wholesale = Db::getInstance()->getRow("
				SELECT * 
				FROM specific_price_wholesale sp 
				WHERE sp.to > '".date('Y-m-d H:i:s')."' 
					AND sp.id_product = '".$p['id_product']."'
			");
			
			if($current_wholesale['wholesale_price'] > 0) {
				$p['sconto_acquisto_1'] = $current_wholesale['reduction_1'];
				$p['sconto_acquisto_2'] = $current_wholesale['reduction_2'];
				$p['sconto_acquisto_3'] = $current_wholesale['reduction_3'];
			}
			
			$righe .= "GEN|".$tipo_anagrafica."|".$modello."|".strtoupper(str_replace(array("\n\r", "\n", "\r", "\r\n"), "", $p['reference']))."|".$p['id_product']."|".strtoupper(str_replace(array("\n\r", "\n", "\r", "\r\n"), "", $p['supplier_reference']))."|".$p['ean13']."||||".substr(str_replace(array("\n\r", "\n", "\r", "\r\n"), "", $p['nome_prodotto']),0,50)."|".str_replace(array("\n\r", "\n", "\r", "\r\n"), "",$p['nome_prodotto'])."|".($p['fuori_produzione'] == 1 ? "2" : "0")."|".($p['condition'] == "new" ? "0" : "1")."|".$codice_marca."|".$p['costruttore']."|".$codice_categoria."|".$p['categoria']."|".number_format(($p['weight']/1000),3,",",".")."|||".$fornitore."|".$fornitore."|".$fornitore_abituale."||||".str_replace("\r\n"," ",$p['note'])."|".str_replace("\r\n"," ",$p['note_fornitore'])."|EOR"; $righe.="\n";
			
			
			if($p['costo_dollari'] > 0)
				$righe_listino .= $p['reference']."|".$p['id_product']."|".substr(str_replace(array("\n\r", "\n", "\r", "\r\n"), "", $p['nome_prodotto']),0,50)."|".$fornitore."|".number_format($p['costo_dollari'],2,",","")."|0|0|0||||USD|EOR";
			else
				$righe_listino .= $p['reference']."|".$p['id_product']."|".substr(str_replace(array("\n\r", "\n", "\r", "\r\n"), "", $p['nome_prodotto']),0,50)."|".$fornitore."|".number_format($p['listino'],2,",","")."|".number_format($p['sconto_acquisto_1'],2,",","")."|".number_format($p['sconto_acquisto_2'],2,",","")."|".number_format($p['sconto_acquisto_3'],2,",","")."||||EUR|EOR";
				
			$righe_listino.="\n";	
			
			if($spedizione > 0)
				$righe_vendita .= $p['supplier_reference']."|".$p['id_product']."|".substr(str_replace(array("\n\r", "\n", "\r", "\r\n"), "", $p['nome_prodotto']),0,50)."||".number_format($p['listino'],2,",","")."|0||||||EOR";
			
			$righe_vendita.="\n";			

			$listini_fornitori = Db::getInstance()->executeS('
				SELECT s.name as fornitore, pli.id_supplier, pli.listino, pli.sconto_acquisto_1, pli.sconto_acquisto_2, pli.sconto_acquisto_3, pli.wholesale_price 
				FROM product_listini pli 
				JOIN product p ON p.id_product = pli.id_product 
				JOIN supplier s ON pli.id_supplier = s.id_supplier 
				WHERE p.id_supplier != pli.id_supplier 
					AND p.id_product = '.$p['id_product']
			);
			
			foreach($listini_fornitori as $f) {
				switch($f['id_supplier'])
				{
					case 8: $fornitore_f = 35420; break; 
					case 11: $fornitore_f = 36448; break; 
					case 38: $fornitore_f = 36541; break; 
					case 42: $fornitore_f = 35732; break;
					case 76: $fornitore_f = 58350; break; 
				case 1458: $fornitore_f = 46215; break; 
					default: $fornitore_f = ''; break;
				}
				
				$current_wholesale_s = Db::getInstance()->getRow("
					SELECT * 
					FROM specific_price_wholesale sp 
					WHERE sp.to > '".date('Y-m-d H:i:s')."' 
						AND sp.id_product = '".$p['id_product']."' 
						AND sp.id_supplier = ".$f['id_supplier']
				);
		
				if($current_wholesale_s['wholesale_price'] > 0) {
					$f['sconto_acquisto_1'] = $current_wholesale_s['reduction_1'];
					$f['sconto_acquisto_2'] = $current_wholesale_s['reduction_2'];
					$f['sconto_acquisto_3'] = $current_wholesale_s['reduction_3'];
				}
		
				$righe_listino .= $p['supplier_reference']."|".$p['id_product']."|".substr(str_replace(array("\n\r", "\n", "\r", "\r\n"), "", $p['nome_prodotto']),0,50)."|".$fornitore_f."|".number_format($f['listino'],2,",",".")."|".number_format($f['sconto_acquisto_1'],2,",",".")."|".number_format($f['sconto_acquisto_2'],2,",",".")."|".number_format($f['sconto_acquisto_3'],2,",",".")."||||EOR";
				$righe_listino.="\n";	
			}				
			//}
		}	

		$file_csv_prov=fopen("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/$file_csv_prov","a+");
		@fwrite($file_csv_prov,$prima_riga.$righe);
		@fclose("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/$file_csv_prov");
		
		$data = file_get_contents("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/EZ-ARTICOLI-PROV.csv");
		$data = mb_convert_encoding($data, 'ISO-8859-1');
		file_put_contents("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/EZ-ARTICOLI.csv", $data);

		unlink("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/EZ-ARTICOLI-PROV.csv");
		
		$file_csv_listino=fopen("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/$file_csv_listino","a+");
		@fwrite($file_csv_listino,$righe_listino);
		@fclose("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/$file_csv_listino");
				
		$file_csv_vendita=fopen("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/$file_csv_vendita","a+");
		@fwrite($file_csv_vendita,$righe_vendita);
		@fclose("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/$file_csv_vendita");		
	}
	
	public function RecuperaBDL($id_bdl)
	{
		$dati_bdl = Db::getInstance()->getRow('SELECT * FROM bdl WHERE id_bdl = '.$id_bdl);
		
		$customer = new Customer($dati_bdl['id_customer']);
		
		$identificatore_univoco_cliente = $customer->id;
		$partita_iva_cliente = $customer->vat_number;
		$codice_fiscale =  $customer->tax_code;
		
		$data_ordine = date("d/m/Y");
		$pagamento_e = Db::getInstance()->getValue('SELECT pagamento FROM customer_amministrazione WHERE id_customer = '.$customer->id); //pag def cli
		$pagamento = Db::getInstance()->getValue('SELECT pagamento FROM customer_amministrazione WHERE id_customer = '.$customer->id); //pag def cli
		$spedizione_ragione_sociale = '--';
		$ragione_sociale = $customer->company;
		$address_cli = new Address(Db::getInstance()->getValue('SELECT id_address FROM address WHERE id_customer = '.$customer->id.' AND active = 1 AND deleted = 0 AND fatturazione = 1'));
			
		$indirizzo = $address_cli->address1;
		$cap = $address_cli->postcode;
		$citta = $address_cli->city;
		$localita = $address_cli->suburb;
		$provincia = Db::getInstance()->getValue('SELECT iso_code FROM state WHERE id_state = '.$address_cli->id_state);
		$telefono1 = $address_cli->phone;
		$telefono2 = $address_cli->phone_mobile;
		$fax = $address_cli->fax;
		$indirizzo_email = $customer->email;
		$cognome_cliente = $customer->lastname;
		$nome_cliente = $customer->firstname;
		$spedizione_indirizzo = '--';
		$spedizione_cap = '--';
		$spedizione_citta = '--';
		$spedizione_provincia = '--';
		$spedizione_nazione = 'IT';
		$corriere = '';
		
		$codice_ordine = '1'.$id_bdl;
			
		$riga_ordine = '';
		
		/*esolver */
		$riga_ord_prod_e = '';
		/* fine esolver */
		
		// esolver
		if($pagamento == "Pagamento alla consegna (COD)") {
			$pagamento_e = "X002"; 
		}

		else if($pagamento == "Bonifico Bancario" || $pagamento == "Bonifico bancario" || $pagamento == "Bonifico") {
			$pagamento_e = "BB369"; 
		}
		else if($pagamento == "Bonifico 30 gg. fine mese") {
			$pagamento_e = "BB430"; 
		}

		else if($pagamento == "Bonifico 60 gg. fine mese") {
			$pagamento_e = "BB431"; 
		}
		
		else if($pagamento == "Bonifico 90 gg. fine mese") {
			$pagamento_e = "BB432"; 
		}
		else if($pagamento == "Bonifico 30 gg. 15 mese successivo") {
			$pagamento_e = "BB463"; 
		}
		else if($pagamento == "R.B. 30 GG. D.F. F.M.") {
			$pagamento_e = "RB230"; 
		}
		
		else if($pagamento == "R.B. 60 GG. D.F. F.M.") {
			$pagamento_e = "RB231"; 
		}
		
		else if($pagamento == "R.B. 90 GG. D.F. F.M.") {
			$pagamento_e = "RB232"; 
		}
		
		else if($pagamento_cli == "R.B. 30 GG. 5 mese successivo") {
					$pagamento_e = "RB253"; 
		}
		else if($pagamento_cli == "R.B. 30 GG. 10 mese successivo") {
			$pagamento_e = "RB250"; 
		}
		else if($pagamento_cli == "R.B. 60 GG. 5 mese successivo") {
			$pagamento_e = "RB258"; 
		}
		else if($pagamento_cli == "R.B. 60 GG. 10 mese successivo") {
			$pagamento_e = "RB249"; 
		}
		
		else if($pagamento == "Contrassegno") {
			$pagamento_e = "X002"; 
		}

		else if($pagamento == "Contrassegno") {
			$pagamento_e = "X002"; 
		}
		
		else if($pagamento == "Carta Amazon") {
			$pagamento_e = "X011"; 
		}
		
		else if($pagamento == "Carta ePrice") {
			$pagamento_e = "X012"; 
		}
		
		else if($pagamento == "PayPal") {
			$pagamento_e = "X013"; 
		}
		
		else if($pagamento == "GestPay") {
			// gestpay
			$pagamento_e = "X007"; 
		}
		else {
			$pagamento_e = "BB369"; 
			
		}
			//fine esolver
			
		$riga_cli = '';
		if($pagamento == "Pagamento alla consegna (COD)") {
			$pagamento = "Contrassegno."; 
		}

		else if($pagamento == "Bonifico Bancario") {
			$pagamento = "Bonifico Bancario"; 
		}

		else if($pagamento == "Contrassegno") {
			$pagamento = "Contrassegno."; 
		}
		
		else if($pagamento == "Bonifico 30 gg. fine mese") {
			$pagamento = "BB30"; 
		}
		
		else if($pagamento == "Bonifico 60 gg. fine mese" || $pagamento == "Bonifico 90 gg. fine mese" ) {
			$pagamento = "posticipato"; 
		}
		
		else if($pagamento == "Bonifico 30 gg. 15 mese successivo") {
			$pagamento = "BB463"; 
		}
		
		else if($pagamento == "R.B. 60 GG. D.F. F.M." || $pagamento == "R.B. 90 GG. D.F. F.M." ) {
			$pagamento = "60RB"; 
		}
		
		else if($pagamento == "R.B. 30 GG. D.F. F.M.") {
			$pagamento = "30RB"; 
		}

		else {
			$pagamento = "<p><strong>CARTA DI CREDITO:</st"; 
		}
			
			
		$riga_cli .= '"'.$identificatore_univoco_cliente.'";"'.$partita_iva_cliente.'";"'.$codice_fiscale.'";"";"";"";"'.$ragione_sociale.'";"";"'.$indirizzo.'";"'.$cap.'";"'.$citta.'";"'.$provincia.'";"";"'.$telefono1.'";"'.$telefono2.'";"'.$fax.'";"'.$indirizzo_email.'";"";"";"'.$cognome_cliente.'";"'.$nome_cliente.'";"";"";"";"";"";"";"";"";"'.$pagamento.'";"";"";"";"";"";"";"";"";"";"";"";"";"";"'.$ragione_sociale.'";"";"'.$spedizione_indirizzo.'";"'.$spedizione_cap.'";"'.$spedizione_citta.'";"'.$spedizione_provincia.'";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"1"';

		$riga_cli .= "\n";
		
		$riga_cli .= '"'.$identificatore_univoco_cliente.'";"'.$partita_iva_cliente.'";"'.$codice_fiscale.'";"";"";"";"'.$spedizione_ragione_sociale.'";"";"'.$spedizione_indirizzo.'";"'.$spedizione_cap.'";"";"'.$spedizione_provincia.'";"";"";"";"";"'.$indirizzo_email.'";"";"";"";"";"";"";"";"";"";"";"";"";"'.$pagamento.'";"";"";"";"";"";"";"";"";"";"";"";"";"";"'.$spedizione_ragione_sociale.'";"";"'.$spedizione_indirizzo.'";"'.$spedizione_cap.'";"'.$spedizione_citta.'";"'.$spedizione_provincia.'";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"";"1"';

		$riga_cli .= "\n";
	
		$dettaglio_bdl = array();
		
		/*esolver */
		$data_nuova_e = date('d/m/Y', strtotime($dati_bdl['date_add']));
		/* fine esolver */
		
		$note_bdl = 'BDL '.$id_bdl.' del '.date("d/m/Y", strtotime(Db::getInstance()->getValue('SELECT data_effettuato FROM bdl WHERE id_bdl = '.$id_bdl)));
	
		$riga_ordine .= '"'.$identificatore_univoco_cliente.'";"'.$partita_iva_cliente.'";"'.$codice_fiscale.'";"'.$identificatore_univoco_cliente.'";"";"'.$codice_ordine.'";"'.$data_ordine.'";"'.$pagamento.'";"";"'.$spedizione_ragione_sociale.'";"'.$ragione_sociale.'";"'.$spedizione_indirizzo.'";"'.$spedizione_cap.'";"'.$spedizione_citta.'";"'.$spedizione_provincia.'";"";"";"";"";"";"";"";"";"";"";"";"'.$codice_ordine.'";"RIFBDL";"";"";"1";"";"";"";"";"";"";"";"'."€"."1,00".'";"";"";"";"";"";"";"'.$note_bdl.'";"";"0"';
		$riga_ordine .= "\n";
		
		$ccod = $codice_ordine;
		$file_csv = "ORD.$ccod.csv";
		$file_csv2 = "CLI.$ccod.csv";

		/*esolver */
		if($customer->is_company == 1) {
	
		}
		else {
			$ragione_sociale = $nome_cliente." ".$cognome_cliente;
		}
		
		$ragione_sociale_e = explode(';;',wordwrap($ragione_sociale, 30, ";;", true));

		$ragione_sociale_prima_parte = $ragione_sociale_e[0];
		$ragione_sociale_seconda_parte = $ragione_sociale_e[1];
		
		$indirizzo_e = explode(';;',wordwrap($indirizzo, 35, ";;", true));
		
		$indirizzo_prima_parte = $indirizzo_e[0];
		$indirizzo_seconda_parte = $indirizzo_e[1];
		
		if($spedizione_nazione == "IT")
			$codice_esterno_fornitore = 33153; // GLS
		else
			$codice_esterno_fornitore = 33153; // GLS

		//	$codice_esterno_fornitore = 35432; // TNT
		
		$corriere_nome = Db::getInstance()->getValue('SELECT name FROM carrier WHERE id_carrier = '.$corriere);
		
		if(strpos($corriere_nome, 'Ritiro') !== false)
			$codice_trasporto_a_mezzo = '2';
		else
			$codice_trasporto_a_mezzo = '1';
		
		if($codice_trasporto_a_mezzo == '2')
			$codice_esterno_fornitore = '';

		$riga_ord_e = 'TES|'.$data_nuova_e.'|'.$codice_ordine.'|'.$identificatore_univoco_cliente.'||||'.$pagamento_e.'|'.number_format($totale_bdl_u,2,",","").'|1|||||'.$ragione_sociale_prima_parte.'|'.$ragione_sociale_seconda_parte.'|'.$spedizione_indirizzo.'|'.$spedizione_cap.'|'.$spedizione_citta.'||'.$spedizione_provincia.'|'.$spedizione_nazione.'||1||1||1||1||33153|0||||||||||||1||EOR'."";
		
		$riga_ord_e .= $riga_ord_prod_e;
		
		if(strpos(strtolower($ragione_sociale), 'srl') !== false || strpos(strtolower($ragione_sociale), 's.r.l') !== false || strpos(strtolower($ragione_sociale), 's.p.a') !== false || strpos(strtolower($ragione_sociale), ' spa ') !== false)
			$tipo_soggetto = 1;
		else if(strpos(strtolower($ragione_sociale), ' sas ') !== false || strpos(strtolower($ragione_sociale), 's.a.s') !== false || strpos(strtolower($ragione_sociale), 's.n.c') !== false || strpos(strtolower($ragione_sociale), 'snc') !== false)
			$tipo_soggetto = 2;
		else if($partita_iva_cliente != $codice_fiscale)
			$tipo_soggetto = 3;
		else
			$tipo_soggetto = 4;

		$codice_nazione = Db::getInstance()->getValue('SELECT iso_code FROM country WHERE id_country = '.$address_cli->id_country);
		$provincia_esteso = Db::getInstance()->getValue('SELECT name FROM state WHERE id_state = '.$address_cli->id_state);
		$gruppo_cliente = $customer->id_default_group;
		
		if($row['codice_nazione'] == 'IT')
			$codice_iva_agevolata = '411';
		else
			$codice_iva_agevolata = '412';
		
		if($tipo_soggetto == 3 && $codice_iva_agevolata == '411')
			$stato_nascita = 'IT';
		else
			$stato_nascita = '';
		
		$size_ordini = Db::getInstance()->executeS('
			SELECT id_customer 
			FROM orders 
			WHERE id_customer = '.$identificatore_univoco_cliente
		);

		if(sizeof($size_ordini) == 1)
			$nuovo = '*';
		
		$pagamento_cli = Db::getInstance()->getValue('
			SELECT pagamento 
			FROM customer_amministrazione 
			WHERE id_customer = '.$identificatore_univoco_cliente
		);
				
		if($pagamento_cli != '' || !isset($pagamento_cli))
			$pagamento_cli = $pagamento_e;
		
		// esolver
		if($pagamento_cli == "Pagamento alla consegna (COD)") {
			$pagamento_cli_e = "X002"; 
		}

		else if($pagamento_cli == "Bonifico Bancario" || $pagamento_cli == "Bonifico bancario" || $pagamento_cli == "Bonifico") {
			$pagamento_cli_e = "BB369"; 
		}
		else if($pagamento_cli == "Bonifico 30 gg. fine mese") {
			$pagamento_cli_e = "BB430"; 
		}

		else if($pagamento_cli == "Bonifico 60 gg. fine mese") {
			$pagamento_cli_e = "BB431"; 
		}
		
		else if($pagamento_cli == "Bonifico 90 gg. fine mese") {
			$pagamento_cli_e = "BB432"; 
		}
		
		else if($pagamento_cli == "Bonifico 30 gg. 15 mese successivo") {
			$pagamento_cli_e = "BB463"; 
		}
		
		else if($pagamento_cli == "R.B. 30 GG. D.F. F.M.") {
			$pagamento_cli_e = "RB230"; 
		}
		
		else if($pagamento_cli == "R.B. 60 GG. D.F. F.M.") {
			$pagamento_cli_e = "RB231"; 
		}
		
		else if($pagamento_cli == "R.B. 90 GG. D.F. F.M.") {
			$pagamento_cli_e = "RB232"; 
		}
		
		else if($pagamento_cli == "R.B. 30 GG. 5 mese successivo") {
			$pagamento_cli_e = "RB253"; 
		}
		else if($pagamento_cli == "R.B. 30 GG. 10 mese successivo") {
			$pagamento_cli_e = "RB250"; 
		}
		else if($pagamento_cli == "R.B. 60 GG. 5 mese successivo") {
			$pagamento_cli_e = "RB258"; 
		}
		else if($pagamento_cli == "R.B. 60 GG. 10 mese successivo") {
			$pagamento_cli_e = "RB249"; 
		}
		else if($pagamento_cli == "Contrassegno") {
			$pagamento_cli_e = "X002"; 
		}

		else if($pagamento_cli == "Contrassegno") {
			$pagamento_cli_e = "X002"; 
		}
		
		else if($pagamento_cli == "Carta Amazon") {
			$pagamento_cli_e = "X011"; 
		}
		
		else if($pagamento_cli == "Carta ePrice") {
			$pagamento_cli_e = "X012"; 
		}
		
		else if($pagamento_cli == "PayPal") {
			$pagamento_cli_e = "X013"; 
		}
		
		else if($pagamento_cli == "GestPay") {
			// gestpay
			$pagamento_cli_e = "X007"; 
		}
		else
		{
			$pagamento_cli_e = "BB369"; 
			
		}
		//fine esolver
		
		$riga_cli_e = 'GEN|'.$identificatore_univoco_cliente.'|'.$partita_iva_cliente.'|'.$codice_fiscale.'|'.($partita_iva_cliente != '' ? 0 : 1).'||'.($gruppo_cliente == 5 ? '1' : '0').'||'.$tipo_soggetto.'|'.$ragione_sociale_prima_parte.'|'.$ragione_sociale_seconda_parte.'|'.$indirizzo_prima_parte.'|'.$indirizzo_seconda_parte.'|'.$cap.'|'.$citta.'||'.$provincia.'|'.$codice_nazione.'|'.$telefono1.'|'.$telefono2.'|'.$fax.'|'.$indirizzo_email.'|'.$nome_cliente.' '.$cognome_cliente.'|'.$cognome_cliente.'|'.$nome_cliente.'|'.'|'.'|'.'|'.'|'.$stato_nascita.'|'.$codice_iva_agevolata.'|'.$codice_nazione.$provincia.'|'.$codice_nazione.'-'.$provincia_esteso.'|||'.$nuovo.'|'.$pagamento_cli_e.'|'."\n";
		
		
		$ccod = $codice_ordine;
		$file_csv = "BDL.$ccod.csv";
		$file_csv2 = "CLI.$ccod.csv";
		
		if(!$customer->id) {
		}
		else {
		$file_csv=fopen("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/$file_csv","a+");
		@fwrite($file_csv,$riga_ord_e);

			$file_csv2=fopen("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/esolver/$file_csv2","a+");
		@fwrite($file_csv2,$riga_cli_e);
		}
		
		/*fine esolver */
	}
	
	public function BDLtoCart($id_bdl)
	{
		$dati_bdl = Db::getInstance()->getRow('
			SELECT * 
			FROM bdl 
			WHERE id_bdl = '.$id_bdl
		);
	
		$dettaglio_bdl = array();
		
		$dettaglio_costi = unserialize($dati_bdl['dettaglio_costi']);
		
		$dettaglio_bdl['chilometri_percorsi_tipo'] = str_replace(",",".",$dettaglio_costi['chilometri_percorsi_tipo']);
		$dettaglio_bdl['chilometri_percorsi_qta'] = str_replace(",",".",$dettaglio_costi['chilometri_percorsi_qta']);
		$dettaglio_bdl['chilometri_percorsi_sconto'] = str_replace(",",".",$dettaglio_costi['chilometri_percorsi_sconto']);
		$dettaglio_bdl['chilometri_percorsi_unitario'] = str_replace(",",".",$dettaglio_costi['chilometri_percorsi_unitario']);
		$prezzo_chilometri_percorsi = $dettaglio_bdl['chilometri_percorsi_unitario']-(($dettaglio_bdl['chilometri_percorsi_unitario']*$dettaglio_bdl['chilometri_percorsi_sconto'])/100);
		$prezzo_chilometri_percorsi = str_replace(".",",",$prezzo_chilometri_percorsi);
		
		if($dettaglio_bdl['chilometri_percorsi_unitario'] > 0) {
			if($dettaglio_costi['chilometri_percorsi_tipo'] == 'EZKMR') {
				$riga_ordine .= '"'.$identificatore_univoco_cliente.'";"'.$partita_iva_cliente.'";"'.$codice_fiscale.'";"'.$identificatore_univoco_cliente.'";"";"'.$codice_ordine.'";"'.$data_ordine.'";"'.$pagamento.'";"";"'.$spedizione_ragione_sociale.'";"'.$ragione_sociale.'";"'.$spedizione_indirizzo.'";"'.$spedizione_cap.'";"'.$spedizione_citta.'";"'.$spedizione_provincia.'";"";"";"";"";"";"";"";"";"";"";"";"'.$codice_ordine.'";"'.$dettaglio_costi['chilometri_percorsi_tipo'].'";"";"";"1";"";"";"";"";"";"";"";"'."€"."".number_format($dettaglio_bdl['chilometri_percorsi_unitario']*$dettaglio_bdl['chilometri_percorsi_qta'],2,",","")."".'";"'.number_format($dettaglio_costi['chilometri_percorsi_sconto'],2,",","").'";"";"";"";"";"";"";"";"0"';
				$riga_ordine .= "\n";
				
				/*esolver */
				$riga_ord_prod_e .= 'RIG|'.$data_nuova_e.'|'.$codice_ordine.'|'.$identificatore_univoco_cliente.'|||||||||||||||||||||||||||||||||85|EZKMR|Rimborso km oltre diritto fisso|'.number_format($dettaglio_bdl['chilometri_percorsi_qta'],2,",","").'|'.number_format($prezzo_chilometri_percorsi,2,",","").'|||||1|||EOR';
				
				$riga_ord_prod_e .= "\n";
				/*fine esolver */
			}
			else {
				$riga_ordine .= '"'.$identificatore_univoco_cliente.'";"'.$partita_iva_cliente.'";"'.$codice_fiscale.'";"'.$identificatore_univoco_cliente.'";"";"'.$codice_ordine.'";"'.$data_ordine.'";"'.$pagamento.'";"";"'.$spedizione_ragione_sociale.'";"'.$ragione_sociale.'";"'.$spedizione_indirizzo.'";"'.$spedizione_cap.'";"'.$spedizione_citta.'";"'.$spedizione_provincia.'";"";"";"";"";"";"";"";"";"";"";"";"'.$codice_ordine.'";"'.$dettaglio_costi['chilometri_percorsi_tipo'].'";"";"";"'.$dettaglio_costi['chilometri_percorsi_qta'].'";"";"";"";"";"";"";"";"'."€"."".$dettaglio_costi['chilometri_percorsi_unitario']."".'";"'.$dettaglio_costi['chilometri_percorsi_sconto'].'";"";"";"";"";"";"";"";"0"';
				$riga_ordine .= "\n";
			
				/*esolver */
				$riga_ord_prod_e .= 'RIG|'.$data_nuova_e.'|'.$codice_ordine.'|'.$identificatore_univoco_cliente.'|||||||||||||||||||||||||||||||||85|EZZONAFIX|Chiamata fissa con partner di zona|'.number_format($dettaglio_bdl['chilometri_percorsi_qta'],2,",","").'|'.number_format($prezzo_chilometri_percorsi,2,",","").'|||||1|||EOR';
				
				$riga_ord_prod_e .= "\n";
				/*fine esolver */
			}
		}

		$dettaglio_bdl['diritto_chiamata_tipo'] = str_replace(",",".",$dettaglio_costi['diritto_chiamata_tipo']);
		$dettaglio_bdl['diritto_chiamata_qta'] = str_replace(",",".",$dettaglio_costi['diritto_chiamata_qta']);
		$dettaglio_bdl['diritto_chiamata_sconto'] = str_replace(",",".",$dettaglio_costi['diritto_chiamata_sconto']);
		$dettaglio_bdl['diritto_chiamata_unitario'] = str_replace(",",".",$dettaglio_costi['diritto_chiamata_unitario']);
		$prezzo_diritto_chiamata = $dettaglio_bdl['diritto_chiamata_unitario']-(($dettaglio_bdl['diritto_chiamata_unitario']*$dettaglio_bdl['diritto_chiamata_sconto'])/100);
		$prezzo_diritto_chiamata = str_replace(".",",",$prezzo_diritto_chiamata);
		
		if($dettaglio_bdl['diritto_chiamata_unitario'] > 0) {
			$riga_ordine .= '"'.$identificatore_univoco_cliente.'";"'.$partita_iva_cliente.'";"'.$codice_fiscale.'";"'.$identificatore_univoco_cliente.'";"";"'.$codice_ordine.'";"'.$data_ordine.'";"'.$pagamento.'";"";"'.$spedizione_ragione_sociale.'";"'.$ragione_sociale.'";"'.$spedizione_indirizzo.'";"'.$spedizione_cap.'";"'.$spedizione_citta.'";"'.$spedizione_provincia.'";"";"";"";"";"";"";"";"";"";"";"";"'.$codice_ordine.'";"'.$dettaglio_costi['diritto_chiamata_tipo'].'";"";"";"'.$dettaglio_costi['diritto_chiamata_qta'].'";"";"";"";"";"";"";"";"'."€"."".$dettaglio_costi['diritto_chiamata_unitario']."".'";"'.$dettaglio_costi['diritto_chiamata_sconto'].'";"";"";"";"";"";"";"";"0"';
			$riga_ordine .= "\n";
			
			/*esolver */
			$riga_ord_prod_e .= 'RIG|'.$data_nuova_e.'|'.$codice_ordine.'|'.$identificatore_univoco_cliente.'|||||||||||||||||||||||||||||||||85|'.$dettaglio_bdl['diritto_chiamata_tipo'].'|'.Db::getInstance()->getValue('SELECT name FROM product_lang WHERE id_product = (SELECT id_product FROM product WHERE reference = "'.$dettaglio_bdl['diritto_chiamata_tipo'].'")').'|'.number_format($dettaglio_bdl['diritto_chiamata_qta'],2,",","").'|'.number_format($prezzo_diritto_chiamata,2,",","").'|||||1|||EOR';
			$riga_ord_prod_e .= "\n";
			/*fine esolver */
		}
		
		$dettaglio_bdl['costo_manodopera_tipo'] = str_replace(",",".",$dettaglio_costi['costo_manodopera_tipo']);
		$dettaglio_bdl['costo_manodopera_qta'] = str_replace(",",".",$dettaglio_costi['costo_manodopera_qta']);
		$dettaglio_bdl['costo_manodopera_sconto'] = str_replace(",",".",$dettaglio_costi['costo_manodopera_sconto']);
		$dettaglio_bdl['costo_manodopera_unitario'] = str_replace(",",".",$dettaglio_costi['costo_manodopera_unitario']);
		$prezzo_costo_manodopera = $dettaglio_bdl['costo_manodopera_unitario']-(($dettaglio_bdl['costo_manodopera_unitario']*$dettaglio_bdl['costo_manodopera_sconto'])/100);
		$prezzo_costo_manodopera = str_replace(".",",",$prezzo_costo_manodopera);
		
		if($dettaglio_bdl['costo_manodopera_unitario']) {
			$riga_ordine .= '"'.$identificatore_univoco_cliente.'";"'.$partita_iva_cliente.'";"'.$codice_fiscale.'";"'.$identificatore_univoco_cliente.'";"";"'.$codice_ordine.'";"'.$data_ordine.'";"'.$pagamento.'";"";"'.$spedizione_ragione_sociale.'";"'.$ragione_sociale.'";"'.$spedizione_indirizzo.'";"'.$spedizione_cap.'";"'.$spedizione_citta.'";"'.$spedizione_provincia.'";"";"";"";"";"";"";"";"";"";"";"";"'.$codice_ordine.'";"'.$dettaglio_bdl['costo_manodopera_tipo'].'";"";"";"'.$dettaglio_costi['costo_manodopera_qta'].'";"";"";"";"";"";"";"";"'."€"."".$dettaglio_costi['costo_manodopera_unitario']."".'";"'.$dettaglio_costi['costo_manodopera_sconto'].'";"";"";"";"";"";"";"";"0"';
			$riga_ordine .= "\n";
			
			/*esolver */
			$riga_ord_prod_e .= 'RIG|'.$data_nuova_e.'|'.$codice_ordine.'|'.$identificatore_univoco_cliente.'|||||||||||||||||||||||||||||||||85|'.$dettaglio_bdl['costo_manodopera_tipo'].'|'.Db::getInstance()->getValue('SELECT name FROM product_lang WHERE id_product = (SELECT id_product FROM product WHERE reference = "'.$dettaglio_bdl['costo_manodopera_tipo'].'")').'|'.number_format($dettaglio_bdl['costo_manodopera_qta'],2,",","").'|'.number_format($prezzo_costo_manodopera,2,",","").'|||||1|||EOR';
			$riga_ord_prod_e .= "\n";
			/*fine esolver */
		}
		
		$dettaglio_bdl['num_varie'] = $dettaglio_costi['num_varie'];
		
		$num_varie = $dettaglio_costi['num_varie']; 
		
		$totale_bdl_u = 0;
		$totale_bdl_u += (($dettaglio_bdl['diritto_chiamata_unitario']-(($dettaglio_bdl['diritto_chiamata_unitario']*$dettaglio_bdl['diritto_chiamata_sconto'])/100))*$dettaglio_bdl['diritto_chiamata_qta'])+(($dettaglio_bdl['chilometri_percorsi_unitario']-(($dettaglio_bdl['chilometri_percorsi_unitario']*$dettaglio_bdl['chilometri_percorsi_sconto'])/100))*$dettaglio_bdl['chilometri_percorsi_qta'])+(($dettaglio_bdl['costo_manodopera_unitario']-(($dettaglio_bdl['costo_manodopera_unitario']*$dettaglio_bdl['costo_manodopera_sconto'])/100))*$dettaglio_bdl['costo_manodopera_qta']);
		
		
		for($i = 0; $i<=$dettaglio_costi['num_varie']; $i++) {
			
			if(empty($dettaglio_costi['ricambi_e_varie_'.$i.'_descrizione']) && $dettaglio_costi['ricambi_e_varie_'.$i.'_unitario'] == 0) {
			}
			else {
				$dettaglio_bdl['ricambi_e_varie_'.$i.'_qta'] = str_replace(",",".",$dettaglio_costi['ricambi_e_varie_'.$i.'_qta']);
				$dettaglio_bdl['ricambi_e_varie_'.$i.'_unitario'] = str_replace(",",".",$dettaglio_costi['ricambi_e_varie_'.$i.'_unitario']);
				$dettaglio_bdl['ricambi_e_varie_'.$i.'_sconto'] = str_replace(",",".",$dettaglio_costi['ricambi_e_varie_'.$i.'_sconto']);
				$dettaglio_bdl['ricambi_e_varie_'.$i.'_descrizione'] = str_replace(",",".",$dettaglio_costi['ricambi_e_varie_'.$i.'_descrizione']);
				$dettaglio_bdl['ricambi_e_varie_'.$i.'_tipo'] = $dettaglio_costi['ricambi_e_varie_'.$i.'_tipo'];
				$prezzo_ricambi_e_varie = $dettaglio_bdl['ricambi_e_varie_'.$i.'_unitario']-(($dettaglio_bdl['ricambi_e_varie_'.$i.'_unitario']*$dettaglio_bdl['ricambi_e_varie_'.$i.'_sconto'])/100);
				
				$totale_bdl_u += ($dettaglio_bdl['ricambi_e_varie_'.$i.'_unitario']-(($dettaglio_bdl['ricambi_e_varie_'.$i.'_unitario']*$dettaglio_bdl['ricambi_e_varie_'.$i.'_sconto'])/100))*$dettaglio_bdl['ricambi_e_varie_'.$i.'_qta'];
				
				$prezzo_ricambi_e_varie = str_replace(".",",",$prezzo_ricambi_e_varie);
				
				$riga_ordine .= '"'.$identificatore_univoco_cliente.'";"'.$partita_iva_cliente.'";"'.$codice_fiscale.'";"'.$identificatore_univoco_cliente.'";"";"'.$codice_ordine.'";"'.$data_ordine.'";"'.$pagamento.'";"";"'.$spedizione_ragione_sociale.'";"'.$ragione_sociale.'";"'.$spedizione_indirizzo.'";"'.$spedizione_cap.'";"'.$spedizione_citta.'";"'.$spedizione_provincia.'";"";"";"";"";"";"";"";"";"";"";"";"'.$codice_ordine.'";"'.$dettaglio_bdl['ricambi_e_varie_'.$i.'_tipo'].'";"";"";"'.$dettaglio_costi['ricambi_e_varie_'.$i.'_qta'].'";"";"";"";"";"";"";"";"'."€"."".$dettaglio_costi['ricambi_e_varie_'.$i.'_unitario']."".'";"'.$dettaglio_costi['ricambi_e_varie_'.$i.'_sconto'].'";"";"";"";"";"";"";"";"0"';
				$riga_ordine .= "\n";
				
				/*esolver */
				
				$virtual_varie = ProductDownload::getIdFromIdProduct(Db::getInstance()->getValue('SELECT id_product FROM product WHERE reference = "'.$dettaglio_bdl['ricambi_e_varie_'.$i.'_tipo'].'"'));	
				
				$category_varie = Db::getInstance()->getValue('SELECT id_category_default FROM product WHERE reference = "'.$dettaglio_bdl['ricambi_e_varie_'.$i.'_tipo'].'"');	
				if(($category_varie == 119 || $category_varie== 248 || $category_varie == 249))
					$tipo_varie = 85;
				else
					$tipo_varie = 1;
				
				$riga_ord_prod_e .= 'RIG|'.$data_nuova_e.'|'.$codice_ordine.'|'.$identificatore_univoco_cliente.'|||||||||||||||||||||||||||||||||85|'.$dettaglio_bdl['ricambi_e_varie_'.$i.'_tipo'].'|'.Db::getInstance()->getValue('SELECT name FROM product_lang WHERE id_product = (SELECT id_product FROM product WHERE reference = "'.$dettaglio_bdl['ricambi_e_varie_'.$i.'_tipo'].'")').'|'.number_format($dettaglio_bdl['ricambi_e_varie_'.$i.'_qta'],2,",","").'|'.number_format($prezzo_ricambi_e_varie,2,",","").'|||||1|||EOR';
				$riga_ord_prod_e .= "\n";
				/*fine esolver */
			}
		}
		
	}
	
	public static function getAccessoryById($accessoryId)
	{
		$is_product = Db::getInstance()->getValue('
			SELECT `name` 
			FROM `'._DB_PREFIX_.'product_lang` 
			WHERE `id_product` = '.(int)($accessoryId)
		);

		if($is_product == '') {
			$is_product = Db::getInstance()->getValue('
				SELECT `bundle_name` 
				FROM `'._DB_PREFIX_.'bundle` 
				WHERE `id_bundle` = '.(int)($accessoryId)
			);
		
			if($is_product != '') {	
				return Db::getInstance()->getRow('
					SELECT `id_bundle` id_product, `bundle_name` name 
					FROM `'._DB_PREFIX_.'bundle` 
					WHERE `id_bundle` = '.(int)($accessoryId)
				);
			}
			else
				return '';
		}
		else {
			return Db::getInstance()->getRow('
				SELECT `id_product`, `name` 
				FROM `'._DB_PREFIX_.'product_lang` 
				WHERE `id_product` = '.(int)($accessoryId)
			);
		}
	}

	public static function getAlternativeById($accessoryId)
	{
		$is_product = Db::getInstance()->getValue('
			SELECT `name` 
			FROM `'._DB_PREFIX_.'product_lang` 
			WHERE `id_product` = '.(int)($accessoryId)
		);
		
		if($is_product == '') {
			$is_product = Db::getInstance()->getValue('
				SELECT `bundle_name` 
				FROM `'._DB_PREFIX_.'bundle` 
				WHERE `id_bundle` = '.(int)($accessoryId)
			);
		
			if($is_product != '') {
				return Db::getInstance()->getRow('
					SELECT `id_bundle` id_product, `bundle_name` name 
					FROM `'._DB_PREFIX_.'bundle` 
					WHERE `id_bundle` = '.(int)($accessoryId)
				);
			}
			else
				return '';
		}
		else{
			return Db::getInstance()->getRow('
				SELECT `id_product`, `name` 
				FROM `'._DB_PREFIX_.'product_lang` 
				WHERE `id_product` = '.(int)($accessoryId)
			);
		}
	}
	
	public function changeAlternatives($accessories_id)
	{
		foreach ($accessories_id as $id_product_2)
			Db::getInstance()->AutoExecute(_DB_PREFIX_.'alternative', array('id_product_1' => (int)($this->id), 'id_product_2' => (int)($id_product_2)), 'INSERT');
	}
	
	public function deleteAlternatives()
	{
		return Db::getInstance()->Execute('
			DELETE FROM `'._DB_PREFIX_.'alternative` 
		WHERE `id_product_1` = '.(int)($this->id)
	);
	}
	
	public static function duplicateAlternatives($id_product_old, $id_product_new)
	{
		$return = true;

		$result = Db::getInstance()->ExecuteS('
			SELECT *
			FROM `'._DB_PREFIX_.'alternative`
			WHERE `id_product_1` = '.(int)($id_product_old)
		);

		foreach ($result as $row) {
			$data = array(
				'id_product_1' => (int)($id_product_new),
				'id_product_2' => (int)($row['id_product_2']));
			$return &= Db::getInstance()->AutoExecute(_DB_PREFIX_.'alternative', $data, 'INSERT');
		}

		return $return;
	}
	
	public function getTags($id_lang)
	{
		if (!($this->tags AND key_exists($id_lang, $this->tags)))
			return '';

		$result = '';

		foreach ($this->tags[$id_lang] AS $tagName) {
			if($tagName != '')
				$result .= $tagName.', ';
		}

		return rtrim($result, ', ');
	}
	
	public function calcolaImpegnato($id_product, $id_order = 0)
	{
		$qta_ord_clienti = Db::getInstance()->getValue('SELECT sum(product_quantity - (REPLACE(cons,"0_",""))) FROM order_detail od JOIN orders o ON o.id_order = od.id_order
		JOIN cart c ON c.id_cart = o.id_cart
		JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE '.($id_order > 0 ? 'moh.id_order != '.$id_order.' AND' : '').' moh.id_order_state != 18 AND moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE c.name NOT LIKE "%prime%" AND  od.product_id = '.$id_product.' AND os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 35 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state != 20 AND os.id_order_state != 26 AND os.id_order_state != 29 AND os.id_order_state != 31 AND os.id_order_state != 32 AND os.id_order_state !=34 GROUP BY od.product_id');
		
		return $qta_ord_clienti;
						
	}

}