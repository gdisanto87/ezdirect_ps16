<?php

/* FA PARTE DEL MODULO exfeatures */
/* La funzione getFeaturesAdvanced è stata modificata */

/**
 * @author dreamsoft
 * @package grouped features
 * 
 * This class extends FeatureCore and override method for fetching features fo compare
 */  
 
class Feature extends FeatureCore
{
    public static function getFeaturesForComparison($list_ids_product, $id_lang)
	{
		$features = parent::getFeaturesForComparison($list_ids_product, $id_lang);
        $fids = array();
        foreach ($features as $feature) {
            $fids[$feature['id_feature']] = 1; 
        }
        
        $records = Db::getInstance()->ExecuteS('
            SELECT 
				f.id_feature, 
				fg.id_group, 
				fgl.name,
				f.position AS feature_position,
				fg.position AS group_position
            FROM
            '._DB_PREFIX_.'feature f
            JOIN '._DB_PREFIX_.'feature_group fg ON f.id_group=fg.id_group
            JOIN '._DB_PREFIX_.'feature_group_lang fgl ON (fg.id_group = fgl.id_group AND fgl.id_lang='.intval($id_lang).')
            WHERE f.id_feature IN ('.implode(',', array_keys($fids)).')'
        );
        
        if (!$records) {
			return null;
		}
		$result = array();
        foreach ($records as $rec) {
            if (!array_key_exists($rec["id_group"], $result))
                $result[$rec["id_group"]] = array(
					"name" => $rec["name"], 
					"position" => $rec["group_position"],
					'features' => array()
				);
            foreach ($features as $feature) {
                if ($feature["id_feature"] == $rec["id_feature"]) {
					$feature["position"] = $rec["feature_position"];
                    $result[$rec["id_group"]]["features"][$feature["id_feature"]] = $feature;
				}
            }    
        }
        
		uasort($result, array("Feature", 'sortFeaturesGroupsByPositionCallback'));
        foreach ($result as $group_id=>&$group_data) {
			uasort($group_data["features"], array("Feature", 'sortFeaturesByPositionCallback'));
		}
		
        return $result;
	}
	
	static public function sortFeaturesByPositionCallback($a, $b)
	{
		return intval($a["position"]) < intval($b["position"]) ? -1 : 1;
	}
	
	/**
	 * Use for sorting of features by positions as callback
	 * @TODO: Features can be sorted over sql 'ORDER BY'
	 * @see http://ua.php.net/manual/en/function.uasort.php
	 *  
	 * @param array $a
	 * @param array $b
	 */
	static public function sortFeaturesGroupsByPositionCallback($a, $b)
	{
		return intval($a["position"]) < intval($b["position"]) ? -1 : 1;
	}
	
	// Correggere: Perchè non gli passo direttamente la categoria? Nella maggior parte dei casi la avrò già nel controller
	public static function getFeaturesAdvanced($id_lang, $prodotto)
	{
		$rowxf = Db::getInstance()->getRow("SELECT id_category_default FROM "._DB_PREFIX_."product WHERE id_product = ".$prodotto);
		$categoria = $rowxf['id_category_default'];
		
		// CENTRALINI
		if($categoria == '23' || $categoria == '154' || $categoria == '63' || $categoria == '235' || $categoria == '181' || $categoria == '205' || $categoria == '97' || $categoria == '94' || $categoria == '207' || $categoria == '206' || $categoria == '213' || $categoria == '153' || $categoria == '137' || $categoria == '227' || $categoria == '232' || $categoria == '138' || $categoria == '209' || $categoria == '158' || $categoria == '81' || $categoria == '231' || $categoria == '67' || $categoria == '151' || $categoria == '122' || $categoria == '199' || $categoria == '150' || $categoria == '187' || $categoria == '123' || $categoria == '152' || $categoria == '250' || $categoria == '252' || $categoria == '253' || $categoria == '255') 
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE f.id_feature = 597
				OR f.id_feature = 598
				OR f.id_feature = 599
				OR f.id_feature = 600
				OR f.id_feature = 601
				OR f.id_feature = 602
				OR f.id_feature = 603
				OR f.id_feature = 496
				OR f.id_feature = 604
				OR f.id_feature = 605
				OR f.id_feature = 606
				OR f.id_feature = 513
				OR f.id_feature = 607
				OR f.id_feature = 608
				OR f.id_feature = 609
				OR f.id_feature = 610
				OR f.id_feature = 611
				OR f.id_feature = 612
				OR f.id_feature = 613
				OR f.id_feature = 614
				OR f.id_feature = 576
				OR f.id_feature = 615
				OR f.id_feature = 616
				OR f.id_feature = 617
				OR f.id_feature = 618
				OR f.id_feature = 619
				OR f.id_feature = 620
				OR f.id_feature = 621
				OR f.id_feature = 622
				OR f.id_feature = 623
				OR f.id_feature = 624
				OR f.id_feature = 625
				OR f.id_feature = 626
				OR f.id_feature = 548
				OR f.id_feature = 627
				OR f.id_feature = 628
				OR f.id_feature = 485
				OR f.id_feature = 579
				OR f.id_feature = 630
				OR f.id_feature = 631
				OR f.id_feature = 632
				OR f.id_feature = 633
				OR f.id_feature = 634
				OR f.id_feature = 635
				OR f.id_feature = 636
				OR f.id_feature = 637
				OR f.id_feature = 638
				OR f.id_feature = 640
				OR f.id_feature = 639
				OR f.id_feature = 641
				OR f.id_feature = 642
				OR f.id_feature = 643
				OR f.id_feature = 644
				OR f.id_feature = 645
				OR f.id_feature = 646
				OR f.id_feature = 647
				OR f.id_feature = 648
				OR f.id_feature = 649
				OR f.id_feature = 650
				OR f.id_feature = 651
				OR f.id_feature = 562
				OR f.id_feature = 459
				OR f.id_feature = 652
				OR f.id_feature = 560
				OR f.id_feature = 559
				OR f.id_feature = 453
				OR f.id_feature = 653
				OR f.id_feature = 654
				OR f.id_feature = 655
				OR f.id_feature = 656
				OR f.id_feature = 657
				OR f.id_feature = 783
				OR f.id_feature = 784
				OR f.id_feature = 987
				OR f.id_feature = 988
				OR f.id_feature = 989
				OR f.id_feature = 449
				OR f.id_feature = 450
				OR f.id_feature = 773
				OR f.id_feature = 916
				OR f.id_feature = 917
				ORDER BY f.id_group, fl.name ASC
			');	
		}
		// CUFFIE
		else if($categoria == '106' || $categoria == '221' || $categoria == '192' || $categoria == '222' || $categoria == '202' || $categoria == '216' || $categoria == '32' || $categoria == '229' || $categoria == '69' || $categoria == '21' ||  $categoria == '33' || $categoria == '98' || $categoria == '223' || $categoria == '39' || $categoria == '85' || $categoria == '22' || $categoria == '37' || $categoria == '36' || $categoria == '50' || $categoria == '220' || $categoria == '201' || $categoria == '203' || $categoria == '211' || $categoria == '60'  || $categoria == '117' || $categoria == '38' || $categoria == '34' ||  $categoria == '186' ||  $categoria == '204' || $categoria == '193' || $categoria == '113' || $categoria == '183' || $categoria == '182' || $categoria == '184' || $categoria == '215')
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE f.id_feature = 433
				OR f.id_feature = 435
				OR f.id_feature = 428
				OR f.id_feature = 436
				OR f.id_feature = 441
				OR f.id_feature = 442
				OR f.id_feature = 762
				OR f.id_feature = 443
				OR f.id_feature = 444
				OR f.id_feature = 496
				OR f.id_feature = 448
				OR f.id_feature = 469
				OR f.id_feature = 470
				OR f.id_feature = 429
				OR f.id_feature = 432
				OR f.id_feature = 437
				OR f.id_feature = 438
				OR f.id_feature = 439
				OR f.id_feature = 460
				OR f.id_feature = 461
				OR f.id_feature = 462
				OR f.id_feature = 463
				OR f.id_feature = 464
				OR f.id_feature = 775
				OR f.id_feature = 445
				OR f.id_feature = 446
				OR f.id_feature = 478
				OR f.id_feature = 458
				OR f.id_feature = 466
				OR f.id_feature = 467
				OR f.id_feature = 471
				OR f.id_feature = 431
				OR f.id_feature = 440
				OR f.id_feature = 465
				OR f.id_feature = 468
				OR f.id_feature = 472
				OR f.id_feature = 473
				OR f.id_feature = 474
				OR f.id_feature = 476
				OR f.id_feature = 475
				OR f.id_feature = 455
				OR f.id_feature = 456
				OR f.id_feature = 457
				OR f.id_feature = 459
				OR f.id_feature = 453
				OR f.id_feature = 559
				OR f.id_feature = 449
				OR f.id_feature = 771
				OR f.id_feature = 836
				OR f.id_feature = 776
				OR f.id_feature = 428
				OR f.id_feature = 763
				OR f.id_feature = 450
				OR f.id_feature = 773
				OR f.id_feature = 889
				OR f.id_feature = 917
				ORDER BY f.id_group, fl.name ASC
			');
		}
		// AUDIOCONFERENZE
		else if($categoria == '75' || $categoria == '142' || $categoria == '226' || $categoria == '214' )
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE f.id_feature = 580
				OR f.id_feature = 436
					OR f.id_feature = 581 
				OR f.id_feature = 582
				OR f.id_feature = 583
				OR f.id_feature = 584
				OR f.id_feature = 585
				OR f.id_feature = 447
				OR f.id_feature = 586
				OR f.id_feature = 460
				OR f.id_feature = 523
				OR f.id_feature = 777
				OR f.id_feature = 496
				OR f.id_feature = 493
				OR f.id_feature = 587
				OR f.id_feature = 491
				OR f.id_feature = 588
				OR f.id_feature = 589
				OR f.id_feature = 590
				OR f.id_feature = 495
				OR f.id_feature = 596
				OR f.id_feature = 591
				OR f.id_feature = 592
				OR f.id_feature = 593
				OR f.id_feature = 594
				OR f.id_feature = 431
				OR f.id_feature = 595
				OR f.id_feature = 559
				OR f.id_feature = 453
				OR f.id_feature = 560
				OR f.id_feature = 480
				OR f.id_feature = 481
				OR f.id_feature = 575
				OR f.id_feature = 482
				OR f.id_feature = 483
				OR f.id_feature = 484
				OR f.id_feature = 487
				OR f.id_feature = 488
				OR f.id_feature = 489
				OR f.id_feature = 490
				OR f.id_feature = 527
				OR f.id_feature = 486
				OR f.id_feature = 778
				OR f.id_feature = 773
				OR f.id_feature = 772
				OR f.id_feature = 449
				OR f.id_feature = 450
				OR f.id_feature = 779
				OR f.id_feature = 771
				OR f.id_feature = 763
				OR f.id_feature = 917
				ORDER BY f.id_group, fl.name ASC
			');
		}
		// FISSI
		else if($categoria == '115' || $categoria == '86' || $categoria == '24' || $categoria == '196' || $categoria == '212' || $categoria == '129' || $categoria == '197'|| $categoria == '92'|| $categoria == '31'|| $categoria == '30'|| $categoria == '219'|| $categoria == '131'|| $categoria == '136'|| $categoria == '135'|| $categoria == '133'|| $categoria == '134'|| $categoria == '132'|| $categoria == '198')
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE f.id_feature = 500
				OR f.id_feature = 496
				OR f.id_feature = 504
				OR f.id_feature = 436
				OR f.id_feature = 550
				OR f.id_feature = 551
				OR f.id_feature = 555
				OR f.id_feature = 497
				OR f.id_feature = 518
				OR f.id_feature = 505
				OR f.id_feature = 519
				OR f.id_feature = 521
				OR f.id_feature = 460
				OR f.id_feature = 461
				OR f.id_feature = 462
				OR f.id_feature = 523
				OR f.id_feature = 515
				OR f.id_feature = 516
				OR f.id_feature = 556
				OR f.id_feature = 557
				OR f.id_feature = 558
				OR f.id_feature = 561
				OR f.id_feature = 578
				OR f.id_feature = 553
				OR f.id_feature = 513
				OR f.id_feature = 514
				OR f.id_feature = 517
				OR f.id_feature = 574
				OR f.id_feature = 498
				OR f.id_feature = 503
				OR f.id_feature = 506
				OR f.id_feature = 507
				OR f.id_feature = 522
				OR f.id_feature = 791
				OR f.id_feature = 493
				OR f.id_feature = 458
				OR f.id_feature = 495
				OR f.id_feature = 508
				OR f.id_feature = 491
				OR f.id_feature = 466
				OR f.id_feature = 531
				OR f.id_feature = 492
				OR f.id_feature = 480
				OR f.id_feature = 481
				OR f.id_feature = 575
				OR f.id_feature = 482
				OR f.id_feature = 483
				OR f.id_feature = 484
				OR f.id_feature = 487
				OR f.id_feature = 488
				OR f.id_feature = 489
				OR f.id_feature = 490
				OR f.id_feature = 527
				OR f.id_feature = 532
				OR f.id_feature = 534
				OR f.id_feature = 548
				OR f.id_feature = 485
				OR f.id_feature = 552
				OR f.id_feature = 501
				OR f.id_feature = 502
				OR f.id_feature = 511
				OR f.id_feature = 512
				OR f.id_feature = 520
				OR f.id_feature = 524
				OR f.id_feature = 554
				OR f.id_feature = 563
				OR f.id_feature = 564
				OR f.id_feature = 565
				OR f.id_feature = 566
				OR f.id_feature = 567
				OR f.id_feature = 568
				OR f.id_feature = 569
				OR f.id_feature = 570
				OR f.id_feature = 571
				OR f.id_feature = 572
				OR f.id_feature = 573
				OR f.id_feature = 579
				OR f.id_feature = 509
				OR f.id_feature = 536
				OR f.id_feature = 537
				OR f.id_feature = 538
				OR f.id_feature = 486
				OR f.id_feature = 499
				OR f.id_feature = 562
				OR f.id_feature = 771
				OR f.id_feature = 559
				OR f.id_feature = 560
				OR f.id_feature = 453
				OR f.id_feature = 793
				OR f.id_feature = 794
				OR f.id_feature = 773
				OR f.id_feature = 772
				OR f.id_feature = 449
				OR f.id_feature = 450
				OR f.id_feature = 431
				OR f.id_feature = 889
				OR f.id_feature = 917
				ORDER BY f.id_group, fl.name ASC
			');
		}
		// CORDLESS
		else if($categoria == '128' || $categoria == '189' || $categoria == '224' || $categoria == '195' )
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE f.id_feature = 529
				OR f.id_feature = 766
				OR f.id_feature = 530
				OR f.id_feature = 504
				OR f.id_feature = 436
				OR f.id_feature = 550
				OR f.id_feature = 551
				OR f.id_feature = 555
				OR f.id_feature = 460
				OR f.id_feature = 461
				OR f.id_feature = 462
				OR f.id_feature = 523
				OR f.id_feature = 543
				OR f.id_feature = 556
				OR f.id_feature = 557
				OR f.id_feature = 558
				OR f.id_feature = 561
				OR f.id_feature = 513
				OR f.id_feature = 514
				OR f.id_feature = 539
				OR f.id_feature = 540
				OR f.id_feature = 517
				OR f.id_feature = 574
				OR f.id_feature = 522
				OR f.id_feature = 542
				OR f.id_feature = 788
				OR f.id_feature = 525
				OR f.id_feature = 526
				OR f.id_feature = 491
				OR f.id_feature = 466
				OR f.id_feature = 531
				OR f.id_feature = 533
				OR f.id_feature = 458
				OR f.id_feature = 577
				OR f.id_feature = 544
				OR f.id_feature = 625
				OR f.id_feature = 482
				OR f.id_feature = 527
				OR f.id_feature = 576
				OR f.id_feature = 480
				OR f.id_feature = 481
				OR f.id_feature = 532
				OR f.id_feature = 534
				OR f.id_feature = 535
				OR f.id_feature = 546
				OR f.id_feature = 547
				OR f.id_feature = 548
				OR f.id_feature = 549
				OR f.id_feature = 524
				OR f.id_feature = 554
				OR f.id_feature = 563
				OR f.id_feature = 564
				OR f.id_feature = 565
				OR f.id_feature = 566
				OR f.id_feature = 567
				OR f.id_feature = 568
				OR f.id_feature = 569
				OR f.id_feature = 570
				OR f.id_feature = 571
				OR f.id_feature = 572
				OR f.id_feature = 573
				OR f.id_feature = 579
				OR f.id_feature = 509
				OR f.id_feature = 536
				OR f.id_feature = 537
				OR f.id_feature = 538
				OR f.id_feature = 528
				OR f.id_feature = 455
				OR f.id_feature = 456
				OR f.id_feature = 545
				OR f.id_feature = 562
				OR f.id_feature = 541
				OR f.id_feature = 559
				OR f.id_feature = 560
				OR f.id_feature = 453
				OR f.id_feature = 789
				OR f.id_feature = 790
				OR f.id_feature = 773
				OR f.id_feature = 772
				OR f.id_feature = 486
				OR f.id_feature = 449
				OR f.id_feature = 450
				OR f.id_feature = 836
				OR f.id_feature = 771
				OR f.id_feature = 889
				OR f.id_feature = 917
				ORDER BY f.id_group, fl.name ASC
			');
		}
		// AUDIOGUIDE
		else if($categoria == '144' || $categoria == '230' || $categoria == '190' || $categoria == '191')
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE f.id_feature = 735
				OR f.id_feature = 736
				OR f.id_feature = 441
				OR f.id_feature = 767
				OR f.id_feature = 460
				OR f.id_feature = 737
				OR f.id_feature = 495
				OR f.id_feature = 596
				OR f.id_feature = 738
				OR f.id_feature = 739
				OR f.id_feature = 716
				OR f.id_feature = 740
				OR f.id_feature = 741
				OR f.id_feature = 742
				OR f.id_feature = 743
				OR f.id_feature = 744
				OR f.id_feature = 734
				OR f.id_feature = 771
				OR f.id_feature = 745
				OR f.id_feature = 746
				OR f.id_feature = 747
				OR f.id_feature = 748
				OR f.id_feature = 749
				OR f.id_feature = 750
				OR f.id_feature = 559
				OR f.id_feature = 560
				OR f.id_feature = 453
				OR f.id_feature = 781
				OR f.id_feature = 782
				OR f.id_feature = 773
				OR f.id_feature = 772
				OR f.id_feature = 449
				OR f.id_feature = 450
				OR f.id_feature = 917
				ORDER BY f.id_group, fl.name ASC
			');
		}
		// RICETRASMITTENTI
		else if($categoria == '156')
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE f.id_feature = 518
				OR f.id_feature = 700
				OR f.id_feature = 701
				OR f.id_feature = 702
				OR f.id_feature = 550
				OR f.id_feature = 703
				OR f.id_feature = 460
				OR f.id_feature = 461
				OR f.id_feature = 513
				OR f.id_feature = 714
				OR f.id_feature = 595
				OR f.id_feature = 713
				OR f.id_feature = 495
				OR f.id_feature = 715
				OR f.id_feature = 716
				OR f.id_feature = 704
				OR f.id_feature = 705
				OR f.id_feature = 706
				OR f.id_feature = 707
				OR f.id_feature = 708
				OR f.id_feature = 709
				OR f.id_feature = 710
				OR f.id_feature = 771
				OR f.id_feature = 455
				OR f.id_feature = 456
				OR f.id_feature = 711
				OR f.id_feature = 712
				OR f.id_feature = 559
				OR f.id_feature = 453
				OR f.id_feature = 560
				OR f.id_feature = 807
				OR f.id_feature = 808
				OR f.id_feature = 773
				OR f.id_feature = 772
				OR f.id_feature = 449
				OR f.id_feature = 450
				OR f.id_feature = 917
				ORDER BY f.id_group, fl.name ASC
			');
		}
		// REGISTRATORI
		else if($categoria == '25')
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE f.id_feature = 718
				OR f.id_feature = 719
				OR f.id_feature = 720
				OR f.id_feature = 721
				OR f.id_feature = 595
				OR f.id_feature = 804
				OR f.id_feature = 401
				OR f.id_feature = 723
				OR f.id_feature = 724
				OR f.id_feature = 614
				OR f.id_feature = 725
				OR f.id_feature = 726
				OR f.id_feature = 727
				OR f.id_feature = 728
				OR f.id_feature = 480
				OR f.id_feature = 481
				OR f.id_feature = 751
				OR f.id_feature = 774
				OR f.id_feature = 692
				OR f.id_feature = 693
				OR f.id_feature = 694
				OR f.id_feature = 695
				OR f.id_feature = 696
				OR f.id_feature = 697
				OR f.id_feature = 698
				OR f.id_feature = 699
				OR f.id_feature = 647
				OR f.id_feature = 771
				OR f.id_feature = 559
				OR f.id_feature = 560
				OR f.id_feature = 453
				OR f.id_feature = 805
				OR f.id_feature = 806
				OR f.id_feature = 773
				OR f.id_feature = 772
				OR f.id_feature = 449
				OR f.id_feature = 450
				OR f.id_feature = 917
				ORDER BY f.id_group, fl.name ASC
			');
		}
		// GSM
		else if($categoria == '27' || $categoria == '217' || $categoria == '179' || $categoria == '178' || $categoria == '225' || $categoria == '180' )
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE f.id_feature = 460
				OR f.id_feature = 523
				OR f.id_feature = 504
				OR f.id_feature = 564
				OR f.id_feature = 797
				OR f.id_feature = 798
				OR f.id_feature = 680
				OR f.id_feature = 480
				OR f.id_feature = 682
				OR f.id_feature = 683
				OR f.id_feature = 615
				OR f.id_feature = 681
				OR f.id_feature = 482
				OR f.id_feature = 481
				OR f.id_feature = 486
				OR f.id_feature = 484
				OR f.id_feature = 491
				OR f.id_feature = 495
				OR f.id_feature = 533
				OR f.id_feature = 493
				OR f.id_feature = 684
				OR f.id_feature = 685
				OR f.id_feature = 686
				OR f.id_feature = 687
				OR f.id_feature = 717
				OR f.id_feature = 524
				OR f.id_feature = 688
				OR f.id_feature = 689
				OR f.id_feature = 690
				OR f.id_feature = 691
				OR f.id_feature = 559
				OR f.id_feature = 560
				OR f.id_feature = 453
				OR f.id_feature = 799
				OR f.id_feature = 800
				OR f.id_feature = 773
				OR f.id_feature = 772
				OR f.id_feature = 771
				OR f.id_feature = 449
				OR f.id_feature = 450
				OR f.id_feature = 535
				OR f.id_feature = 871
				OR f.id_feature = 468
				OR f.id_feature = 823
				OR f.id_feature = 565
				OR f.id_feature = 872
				OR f.id_feature = 873
				OR f.id_feature = 830
				OR f.id_feature = 874
				OR f.id_feature = 513
				OR f.id_feature = 875
				OR f.id_feature = 455
				OR f.id_feature = 456
				OR f.id_feature = 876
				OR f.id_feature = 889
				OR f.id_feature = 917
				OR f.id_feature = 921
				OR f.id_feature = 918
				OR f.id_feature = 919
				OR f.id_feature = 920
				OR f.id_feature = 562
				OR f.id_feature = 460
				OR f.id_feature = 923
				OR f.id_feature = 922
				OR f.id_feature = 874
				ORDER BY f.id_group, fl.name ASC
			');
		}
		// RIPETITORI GSM
		else if($categoria == '242')
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE 
				f.id_feature = 917
				OR f.id_feature = 799
				OR f.id_feature = 800
				OR f.id_feature = 921
				OR f.id_feature = 918
				OR f.id_feature = 919
				OR f.id_feature = 920
				OR f.id_feature = 562
				OR f.id_feature = 460
				OR f.id_feature = 923
				OR f.id_feature = 922
				OR f.id_feature = 449
				OR f.id_feature = 450
				ORDER BY f.id_group, fl.name ASC
			');
		}
		// GATEWAY VOIP
		else if($categoria == '46')
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE f.id_feature = 460
				OR f.id_feature = 684
				OR f.id_feature = 576
				OR f.id_feature = 617
				OR f.id_feature = 493
				OR f.id_feature = 725
				OR f.id_feature = 752
				OR f.id_feature = 682
				OR f.id_feature = 683
				OR f.id_feature = 760
				OR f.id_feature = 482
				OR f.id_feature = 483
				OR f.id_feature = 488
				OR f.id_feature = 753
				OR f.id_feature = 490
				OR f.id_feature = 754
				OR f.id_feature = 480
				OR f.id_feature = 755
				OR f.id_feature = 756
				OR f.id_feature = 548
				OR f.id_feature = 757
				OR f.id_feature = 646
				OR f.id_feature = 575
				OR f.id_feature = 758
				OR f.id_feature = 567
				OR f.id_feature = 759
				OR f.id_feature = 771
				OR f.id_feature = 486
				OR f.id_feature = 562
				OR f.id_feature = 654
				OR f.id_feature = 559
				OR f.id_feature = 560
				OR f.id_feature = 453
				OR f.id_feature = 795
				OR f.id_feature = 796
				OR f.id_feature = 773
				OR f.id_feature = 772
				OR f.id_feature = 449
				OR f.id_feature = 450
					OR f.id_feature = 917
				ORDER BY f.id_group, fl.name ASC
			');
		}
		// IP CAMERA
		else if($categoria == '147')
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE f.id_feature = 659
				OR f.id_feature = 660
				OR f.id_feature = 661
				OR f.id_feature = 662
				OR f.id_feature = 663
				OR f.id_feature = 768
				OR f.id_feature = 658
				OR f.id_feature = 665
				OR f.id_feature = 666
				OR f.id_feature = 667
				OR f.id_feature = 670
				OR f.id_feature = 675
				OR f.id_feature = 801
				OR f.id_feature = 676
				OR f.id_feature = 769
				OR f.id_feature = 483
				OR f.id_feature = 664
				OR f.id_feature = 668
				OR f.id_feature = 669
				OR f.id_feature = 674
				OR f.id_feature = 677
				OR f.id_feature = 678
				OR f.id_feature = 671
				OR f.id_feature = 771
				OR f.id_feature = 672
				OR f.id_feature = 673
				OR f.id_feature = 559
				OR f.id_feature = 560
				OR f.id_feature = 453
				OR f.id_feature = 802
				OR f.id_feature = 803
				OR f.id_feature = 773
				OR f.id_feature = 908
				OR f.id_feature = 904
				OR f.id_feature = 907
				OR f.id_feature = 906
				OR f.id_feature = 905
				OR f.id_feature = 909
				OR f.id_feature = 772
				OR f.id_feature = 887
				OR f.id_feature = 449
				OR f.id_feature = 450
				OR f.id_feature = 809
				OR f.id_feature = 913
				OR f.id_feature = 914
				OR f.id_feature = 915
				OR f.id_feature = 917
				ORDER BY f.id_group, fl.name ASC
			');
		}
		// CITOFONI
		else if($categoria == '208')
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE f.id_feature = 500
				OR f.id_feature = 460
				OR f.id_feature = 461
				OR f.id_feature = 578
				OR f.id_feature = 595
				OR f.id_feature = 498
				OR f.id_feature = 506
				OR f.id_feature = 522
				OR f.id_feature = 785
				OR f.id_feature = 729
				OR f.id_feature = 614
				OR f.id_feature = 480
				OR f.id_feature = 481
				OR f.id_feature = 482
				OR f.id_feature = 483
				OR f.id_feature = 487
				OR f.id_feature = 488
				OR f.id_feature = 554
				OR f.id_feature = 771
				OR f.id_feature = 486
				OR f.id_feature = 562
				OR f.id_feature = 730
				OR f.id_feature = 731
				OR f.id_feature = 732
				OR f.id_feature = 733
				OR f.id_feature = 559
				OR f.id_feature = 560
				OR f.id_feature = 453
				OR f.id_feature = 786
				OR f.id_feature = 787
				OR f.id_feature = 773
				OR f.id_feature = 772
				OR f.id_feature = 881
				OR f.id_feature = 976
				OR f.id_feature = 977
				OR f.id_feature = 978
				OR f.id_feature = 979
				OR f.id_feature = 980
				OR f.id_feature = 915
				OR f.id_feature = 917
				OR f.id_feature = 449
				OR f.id_feature = 450
				OR f.id_feature = 889
				ORDER BY f.id_group, fl.name ASC
			');
		}
		// CUFFIE ANTIRUMORE
		else if($categoria == '241')
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE f.id_feature = 449
				OR f.id_feature = 450
				OR f.id_feature = 811
				OR f.id_feature = 773
				OR f.id_feature = 812
				OR f.id_feature = 813
				OR f.id_feature = 814
				OR f.id_feature = 815
				OR f.id_feature = 816
				OR f.id_feature = 817
				OR f.id_feature = 818
				OR f.id_feature = 491
				OR f.id_feature = 441
				OR f.id_feature = 460
				OR f.id_feature = 819
				OR f.id_feature = 820
				OR f.id_feature = 445
				OR f.id_feature = 821
				OR f.id_feature = 458
				OR f.id_feature = 822
				OR f.id_feature = 823
				OR f.id_feature = 824
				OR f.id_feature = 738
				OR f.id_feature = 716
				OR f.id_feature = 825
				OR f.id_feature = 826
				OR f.id_feature = 827
				OR f.id_feature = 828
				OR f.id_feature = 829
				OR f.id_feature = 704
				OR f.id_feature = 830
				OR f.id_feature = 831
				OR f.id_feature = 746
				OR f.id_feature = 832
				OR f.id_feature = 833
				OR f.id_feature = 834
				OR f.id_feature = 835
				OR f.id_feature = 559
				OR f.id_feature = 560
				OR f.id_feature = 453
				OR f.id_feature = 917
				OR f.id_feature = 449
				OR f.id_feature = 450
				ORDER BY f.id_group, fl.name ASC
			');
		}
		// DOMOTICA
		else if($categoria == '243')
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE f.id_feature = 839
				OR f.id_feature = 831
				OR f.id_feature = 832
				OR f.id_feature = 840
				OR f.id_feature = 841
				OR f.id_feature = 842
				OR f.id_feature = 843
				OR f.id_feature = 844
				OR f.id_feature = 845
				OR f.id_feature = 846
				OR f.id_feature = 847
				OR f.id_feature = 848
				OR f.id_feature = 849
				OR f.id_feature = 850
				OR f.id_feature = 851
				OR f.id_feature = 852
				OR f.id_feature = 853
				OR f.id_feature = 854
				OR f.id_feature = 855
				OR f.id_feature = 856
				OR f.id_feature = 857
				OR f.id_feature = 858
				OR f.id_feature = 859
				OR f.id_feature = 860
				OR f.id_feature = 861
				OR f.id_feature = 862
				OR f.id_feature = 863
				OR f.id_feature = 864
				OR f.id_feature = 865
				OR f.id_feature = 866
				OR f.id_feature = 867
				OR f.id_feature = 513
				OR f.id_feature = 868
				OR f.id_feature = 869
				OR f.id_feature = 450
				OR f.id_feature = 656
				OR f.id_feature = 673
				OR f.id_feature = 653
				OR f.id_feature = 654
				OR f.id_feature = 655
				OR f.id_feature = 453
				OR f.id_feature = 560
				OR f.id_feature = 559
				OR f.id_feature = 917
				OR f.id_feature = 449
				OR f.id_feature = 450
				ORDER BY f.id_group, fl.name ASC
			');
		}
		// VIDEOCONFERENZA
		else if($categoria == '76')
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE f.id_feature = 891
				OR f.id_feature = 659
				OR f.id_feature = 892
				OR f.id_feature = 893
				OR f.id_feature = 578
				OR f.id_feature = 665
				OR f.id_feature = 894
				OR f.id_feature = 895
				OR f.id_feature = 896
				OR f.id_feature = 897
				OR f.id_feature = 898
				OR f.id_feature = 899
				OR f.id_feature = 900
				OR f.id_feature = 901
				OR f.id_feature = 902
				OR f.id_feature = 903
				OR f.id_feature = 559
				OR f.id_feature = 560
				OR f.id_feature = 915
				OR f.id_feature = 981
				OR f.id_feature = 982
				OR f.id_feature = 983
				OR f.id_feature = 984
				OR f.id_feature = 986
				OR f.id_feature = 985
				OR f.id_feature = 990
				OR f.id_feature = 431
				OR f.id_feature = 453
				OR f.id_feature = 450
				OR f.id_feature = 449
				OR f.id_feature = 917
				ORDER BY f.id_group, fl.name ASC
			');
		}
		// NETWORKING
		else if($categoria == '210' || $categoria == '260' || $categoria == '261')
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE f.id_feature = 449
				OR f.id_feature = 559
				OR f.id_feature = 450
				OR f.id_feature = 560
				OR f.id_feature = 562
				OR f.id_feature = 924
				OR f.id_feature = 925
				OR f.id_feature = 926
				OR f.id_feature = 927
				OR f.id_feature = 673
				OR f.id_feature = 928
				OR f.id_feature = 486
				OR f.id_feature = 930
				OR f.id_feature = 931
				OR f.id_feature = 932
				OR f.id_feature = 933
				OR f.id_feature = 934
				OR f.id_feature = 935
				OR f.id_feature = 936
				OR f.id_feature = 937
				OR f.id_feature = 938
				OR f.id_feature = 939
				OR f.id_feature = 716
				OR f.id_feature = 940
				OR f.id_feature = 941
				OR f.id_feature = 942
				OR f.id_feature = 943
				OR f.id_feature = 944
				OR f.id_feature = 483
				OR f.id_feature = 484
				OR f.id_feature = 945
				OR f.id_feature = 946
				OR f.id_feature = 449
						OR f.id_feature = 450
				ORDER BY (CASE WHEN f.id_group = 46 THEN 0 ELSE 1 END), f.id_group ASC, fl.name ASC
			');
		}
		// Monitor
		else if($categoria == '233')
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE f.id_feature = 462
				OR f.id_feature = 947
				OR f.id_feature = 948
				OR f.id_feature = 561
				OR f.id_feature = 946
				OR f.id_feature = 949
				OR f.id_feature = 896
				OR f.id_feature = 899
				OR f.id_feature = 950
				OR f.id_feature = 951
				OR f.id_feature = 952
				OR f.id_feature = 953
				OR f.id_feature = 954
				OR f.id_feature = 955
				OR f.id_feature = 956
				OR f.id_feature = 957
				OR f.id_feature = 562
				OR f.id_feature = 565
				OR f.id_feature = 450
				OR f.id_feature = 560
				OR f.id_feature = 959
				OR f.id_feature = 960
				OR f.id_feature = 559
				OR f.id_feature = 449
						OR f.id_feature = 450
				OR f.id_feature = 958
				ORDER BY f.id_group, fl.name ASC
			');
		}
		// Cablaggio
		else if($categoria == '245')
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE f.id_feature = 961
				OR f.id_feature = 962
				OR f.id_feature = 963
				OR f.id_feature = 964
				OR f.id_feature = 965
				OR f.id_feature = 966
				OR f.id_feature = 967
				OR f.id_feature = 968
				OR f.id_feature = 969
				OR f.id_feature = 970
				OR f.id_feature = 971
				OR f.id_feature = 559
				OR f.id_feature = 449
						OR f.id_feature = 450
				ORDER BY f.id_group, fl.name ASC
			');
		}
		// DPI
		else if($categoria == '266')
		{
			return Db::getInstance()->ExecuteS('
				SELECT f.id_feature AS id_feature, f.position AS position, f.id_group AS id_group, fl.id_lang AS id_lang, fl.name AS name, fg.name AS group_name
				FROM `feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'feature_group_lang` fg ON (f.`id_group` = fg.`id_group` AND fg.`id_lang` = '.(int)($id_lang).')
				WHERE f.id_feature = 975
				OR f.id_feature = 974
				OR f.id_feature = 973
				OR f.id_feature = 972
				OR f.id_feature = 453
				OR f.id_feature = 450
				OR f.id_feature = 449
				ORDER BY f.id_group, fl.name ASC
			');
		}
		// Tutto
		else 
		{
			return Db::getInstance()->ExecuteS('
				SELECT *
				FROM `'._DB_PREFIX_.'feature` f
				LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.`id_feature` = fl.`id_feature` AND fl.`id_lang` = '.(int)($id_lang).')
				ORDER BY fl.`name` ASC
			');
		}
	}
}
