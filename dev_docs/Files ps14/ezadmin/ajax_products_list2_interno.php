<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

define('_PS_ADMIN_DIR_', getcwd());
define('PS_ADMIN_DIR', _PS_ADMIN_DIR_); // Retro-compatibility

include(PS_ADMIN_DIR.'/../config/config.inc.php');
/* Getting cookie or logout */
require_once(dirname(__FILE__).'/init.php');

if(isset($_POST['edit_bundle_sort_order'])) {

	Db::getInstance()->executeS("UPDATE bundle SET sort_order = ".$_POST['sort_order']." WHERE id_bundle = ".$_POST['id_bundle']."");
		
}

if(Tools::getValue('cancellaprodotto')) {

	/*Db::getInstance()->executes("DELETE FROM cart_product WHERE id_product = ".$_POST['id_product']." AND id_cart = ".$_POST['id_cart']."");
	Db::getInstance()->executes("DELETE FROM carrelli_creati_prodotti WHERE id_product = ".$_POST['id_product']." AND id_cart = ".$_POST['id_cart']."");
*/
}

else if(Tools::getValue('aggiungiescludi')) {

	$excludeIds.= $_POST['id_product'].",";

}

else {



	$query = Tools::getValue('q', false);
	if (!$query OR $query == '' OR strlen($query) < 1)
		die();

	/*
	 * In the SQL request the "q" param is used entirely to match result in database.
	 * In this way if string:"(ref : #ref_pattern#)" is displayed on the return list,
	 * they are no return values just because string:"(ref : #ref_pattern#)"
	 * is not write in the name field of the product.
	 * So the ref pattern will be cut for the search request.
	 */
	if ($pos = strpos($query, ' (ref:'))
		$query = substr($query, 0, $pos);

	$excludeIds = Tools::getValue('excludeIds', false);
	if ($excludeIds && $excludeIds != 'NaN')
		$excludeIds = implode(',', array_map('intval', explode(',', $excludeIds)));
	else
		$excludeIds = '';

	// Excluding downloadable products from packs because download from pack is not supported
	$excludeVirtuals = (bool)Tools::getValue('excludeVirtuals', false);

	$items = Db::getInstance()->ExecuteS('
	SELECT p.`id_product`, p.id_category_default, `reference`, p.price, pl.name, p.listino as listino, p.quantity as qt_tot, p.stock_quantity as spring, p.wholesale_price as acquisto, p.stock_quantity as magazzino, p.supplier_quantity as allnet, p.esprinet_quantity as esprinet, p.itancia_quantity as itancia, p.sconto_acquisto_1 as sc_acq_1, p.sconto_acquisto_2 as sc_acq_2, p.sconto_acquisto_3 as sc_acq_3
	FROM `'._DB_PREFIX_.'product` p
	LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.id_product = p.id_product)
	WHERE (p.id_category_default = 246 or p.id_category_default = 256) AND (pl.name LIKE \'%'.pSQL($query).'%\' OR p.reference LIKE \'%'.pSQL($query).'%\' OR REPLACE(pl.name," ","") LIKE \'%'.pSQL($query).'%\' OR REPLACE(p.reference," ","") LIKE \'%'.pSQL($query).'%\'
	OR pl.name LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\' OR p.reference LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\') AND pl.id_lang = '.(int)($cookie->id_lang).
	(!empty($excludeIds) ? ' AND p.id_product NOT IN ('.$excludeIds.') ' : ' ').
	($excludeVirtuals ? 'AND p.id_product NOT IN (SELECT pd.id_product FROM `'._DB_PREFIX_.'product_download` pd WHERE (pd.id_product = p.id_product))' : '').' ORDER BY reference ASC');
	
	$i = 0;

	if ($items)
		foreach ($items AS $item) {
		
		$cart = new Cart(Tools::getValue('id_cart'));		
		
		$customer = Db::getInstance()->getValue("SELECT id_customer FROM cart WHERE id_cart = ".Tools::getValue('id_cart')."");
		$group = Db::getInstance()->getValue("SELECT id_default_group FROM customer WHERE id_customer = ".$customer."");


		if($group == 3) {
			$ctrl_sc_qt = Db::getInstance()->getValue("SELECT count(id_product) FROM specific_price WHERE id_product = ".$item['id_product']." AND (specific_price_name = 'sc_riv_1' OR specific_price_name = 'sc_riv_2')");
								
			if($ctrl_sc_qt > 0) {
				$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$item['id_product'].']" name="usa_sconti_quantita['.$item['id_product'].']" type="checkbox" checked="checked" onclick="ristabilisciPrezzo('.$item['id_product'].'); calcolaImporto('.$item['id_product'].'); " />';
			}
			else {
				$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$item['id_product'].']" name="usa_sconti_quantita['.$item['id_product'].']" type="hidden" value="0" />';
			}
		}
		
		else if($group == 12) {
			$ctrl_sc_qt = Db::getInstance()->getValue("SELECT count(id_product) FROM specific_price WHERE id_product = ".$item['id_product']." AND (specific_price_name = 'sc_riv_1' OR specific_price_name = 'sc_riv_2'  OR specific_price_name = 'sc_riv_3')");
								
			if($ctrl_sc_qt > 0) {
				$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$item['id_product'].']" name="usa_sconti_quantita['.$item['id_product'].']" type="checkbox" checked="checked" onclick="ristabilisciPrezzo('.$item['id_product'].'); calcolaImporto('.$item['id_product'].'); " />';
			}
			else {
				$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$item['id_product'].']" name="usa_sconti_quantita['.$item['id_product'].']" type="hidden" value="0" />';
			}
		}	
							
		else {
			$ctrl_sc_qt = Db::getInstance()->getValue("SELECT count(id_product) FROM specific_price WHERE id_product = ".$item['id_product']." AND (specific_price_name = 'sc_qta_1' OR specific_price_name = 'sc_qta_2' OR specific_price_name = 'sc_qta_3')");
								
			if($ctrl_sc_qt > 0) {
				$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$item['id_product'].']" name="usa_sconti_quantita['.$item['id_product'].']" type="checkbox" checked="checked" onclick="ristabilisciPrezzo('.$item['id_product'].'); calcolaImporto('.$item['id_product'].'); " />';
			}
			else {
				$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$item['id_product'].']" name="usa_sconti_quantita['.$item['id_product'].']" type="hidden" value="0" />';
			}
		}
		
			if(strlen($item['name'] > 100)) {
				$name = substr($item['name'],0,100)."... ";
			}
			else {
				$name = $item['name'];
			}
			
			$sc_qta_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_1' AND id_product = '".$item['id_product']."'");
			$sc_qta_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_2' AND id_product = '".$item['id_product']."'");
			$sc_qta_3_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_3' AND id_product = '".$item['id_product']."'");	
			$sc_riv_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_1' AND id_product = '".$item['id_product']."'");	
			$sc_riv_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_2' AND id_product = '".$item['id_product']."'");
			$sc_riv_3_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_3' AND id_product = '".$item['id_product']."'");
			
			$sc_qta_1 = $sc_qta_1_r['price'] - ($sc_qta_1_r['price'] * $sc_qta_1_r['reduction']);
			$sc_qta_2 = $sc_qta_2_r['price'] - ($sc_qta_2_r['price'] * $sc_qta_2_r['reduction']);
			$sc_qta_3 = $sc_qta_3_r['price'] - ($sc_qta_3_r['price'] * $sc_qta_3_r['reduction']);
			$sc_riv_1 = $sc_riv_1_r['price'] - ($sc_riv_1_r['price'] * $sc_riv_1_r['reduction']);
			$sc_riv_2 = $sc_riv_2_r['price'] - ($sc_riv_2_r['price'] * $sc_riv_2_r['reduction']);
			$sc_riv_3 = $sc_riv_2_r['price'] - ($sc_riv_3_r['price'] * $sc_riv_3_r['reduction']);
			
			$unitario = Db::getInstance()->getValue("SELECT price FROM product WHERE id_product = '".$item['id_product']."'");
			
			$sc_qta_1_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_1' AND id_product = '".$item['id_product']."'");
			$sc_qta_2_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_2' AND id_product = '".$item['id_product']."'");
			$sc_qta_3_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_3' AND id_product = '".$item['id_product']."'");
			
			
			$wholesale_price = ($item['acquisto'] > 0 ? $item['acquisto'] : ($item['listino']*(100-$item['sc_acq_1'])/100)*((100-$item['sc_acq_2'])/100)*((100-$item['sc_acq_3'])/100));
			$special_wholesale = Db::getInstance()->getRow('SELECT * FROM specific_price_wholesale spw WHERE spw.to > NOW() AND (spw.pieces = "" OR spw.pieces > 0) AND spw.id_product = '.$item['id_product']);
			if($special_wholesale['wholesale_price'] > 0)
				$wholesale_price = $special_wholesale['wholesale_price'];
			
			$prezzo_partenza = ($item['price'] == 0 && $item['free'] == 0 ? number_format(($customer->id_default_group == 3 ? $sc_riv_1 : ($customer->id_default_group == 12 ? $sc_riv_3 : $item['product_price'])), 2, ',', '') : number_format($item['price'], 2, ',', ''));
						
			$marginalita = ((($prezzo_partenza - $wholesale_price)*100)/$prezzo_partenza);
			
			$acquisto = '<input type="text" size="7" style="text-align:right"  onkeyup="javascript:document.getElementById(\'usa_sconti_quantita['.$item['id_product'].']\').checked = false; calcolaPrezzoScontoExtra('.$item['id_product'].', \'inverso\'); calcolaImporto('.$item['id_product'].');"  name="wholesale_price['.$item['id_product'].']" id="wholesale_price['.$item['id_product'].']" value="'.number_format(round($wholesale_price,2), 2, ',', '').'" />';
			
			$marg = '<td style="text-align:right"><input type="hidden" name="sconto_acquisto_1['.$item['id_product'].']" id="sconto_acquisto_1['.$item['id_product'].']" value="'.$item['sc_acq_1'].'" /><input type="hidden" name="sconto_acquisto_2['.$item['id_product'].']" id="sconto_acquisto_2['.$item['id_product'].']" value="'.$item['sc_acq_2'].'" /><input type="hidden" name="sconto_acquisto_3['.$item['id_product'].']" id="sconto_acquisto_3['.$item['id_product'].']" value="'.$item['sc_acq_3'].'" /><input type="hidden" class="prezzoacquisto" name="totaleacquisto['.$item['id_product'].']" id="totaleacquisto['.$item['id_product'].']" value="'.number_format(round($wholesale_price,2), 2, ',', '').'" /><span id="spanmarginalita['.$item['id_product'].']">'.number_format($marginalita, 2, ',', '').'%</span></td>';
			
			
			$sc_riv_1_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_1' AND id_product = '".$item['id_product']."'");
			$sc_riv_2_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_2' AND id_product = '".$item['id_product']."'");
			$sc_riv_3_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_3' AND id_product = '".$item['id_product']."'");
			
			if($group == 3) {
				$prezzounitario = $unitario;
				$confronto_prezzi = array($sc_riv_1, $sc_qta_1, $sc_qta_2, $sc_qta_3);
				foreach($confronto_prezzi as $prz) {
					if($prz != 0 && $prz < $prezzounitario) {
						$prezzounitario = $prz;
					}
				}
			}
			
			else if($group == 12) {
				$prezzounitario = $unitario;
				$confronto_prezzi = array($sc_riv_1, $sc_riv_3, $sc_qta_1, $sc_qta_2, $sc_qta_3);
				foreach($confronto_prezzi as $prz) {
					if($prz != 0 && $prz < $prezzounitario) {
						$prezzounitario = $prz;
					}
				}
			}
			
			else {
			
				$prezzounitario = $unitario;
			}
			
			$speciale = Product::trovaPrezzoSpeciale($item['id_product'], 1, 0);
			
			if($speciale < $prezzounitario && $speciale != 0) {
				$prezzounitario = $speciale;
				$unitario = $speciale;
				$varspec = 1;
				$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$item['id_product'].']" name="usa_sconti_quantita['.$item['id_product'].']" type="hidden" value="0" />';
			}
			else {
				$varspec = 0;
			}
			
			if(isset($_GET['id_cart'])) {
				$price = '<input size="7" style="text-align:right" onkeyup="javascript:document.getElementById(\'usa_sconti_quantita['.$item['id_product'].']\').checked = false; calcolaPrezzoScontoExtra('.$item['id_product'].', \'inverso\'); calcolaImporto('.$item['id_product'].');" name="nuovo_price['.$item['id_product'].']" id="product_price['.$item['id_product'].']" type="text" value="'.number_format($prezzounitario, 2, ',', '') .'" />';
			}
			else {
				$price = '<input size="7" style="text-align:right" onkeyup="calcolaImporto('.$item['id_product'].');" name="nuovo_price['.$item['id_product'].']" id="product_price['.$item['id_product'].']" type="text" value="'.number_format($prezzounitario, 2, ',', '') .'" />';
			}
			
			$unitario = '<input type="hidden" id="unitario['.$item['id_product'].']" value="'.$unitario.'" />';
			$sc_qta_1 = '<input type="hidden" id="sc_qta_1['.$item['id_product'].']" value="'.$sc_qta_1.'" />';
			$sc_qta_2 = '<input type="hidden" id="sc_qta_2['.$item['id_product'].']" value="'.$sc_qta_2.'" />';
			$sc_qta_3 = '<input type="hidden" id="sc_qta_3['.$item['id_product'].']" value="'.$sc_qta_3.'" />';
			$sc_riv_1 = '<input type="hidden" id="sc_riv_1['.$item['id_product'].']" value="'.$sc_riv_1.'" />';
			$sc_riv_2 = '<input type="hidden" id="sc_riv_2['.$item['id_product'].']" value="'.$sc_riv_2.'" />';
			$sc_riv_3 = '<input type="hidden" id="sc_riv_3['.$item['id_product'].']" value="'.$sc_riv_3.'" />';
			
			$sc_qta_1_q = '<input type="hidden" id="sc_qta_1_q['.$item['id_product'].']" value="'.$sc_qta_1_q.'" />';
			$sc_qta_2_q = '<input type="hidden" id="sc_qta_2_q['.$item['id_product'].']" value="'.$sc_qta_2_q.'" />';
			$sc_qta_3_q = '<input type="hidden" id="sc_qta_3_q['.$item['id_product'].']" value="'.$sc_qta_3_q.'" />';
			$sc_riv_1_q = '<input type="hidden" id="sc_riv_1_q['.$item['id_product'].']" value="'.$sc_riv_1_q.'" />';
			$sc_riv_2_q = '<input type="hidden" id="sc_riv_2_q['.$item['id_product'].']" value="'.$sc_riv_2_q.'" />';
			$sc_riv_3_q = '<input type="hidden" id="sc_riv_3_q['.$item['id_product'].']" value="'.$sc_riv_3_q.'" />';
			
			$quantity = "<input style='text-align:right' name='nuovo_quantity[".$item['id_product']."]' id='product_quantity[".$item['id_product']."]' size='1' type='text' value='1' onkeyup='calcolaImporto(".$item['id_product'].");' />";
			
			$importo = "<td style='text-align:right' id='valoreImporto[".$item['id_product']."]'>".number_format($item['price'], 2, ',', '')."</td>";
		
			$delete = '<a style="cursor:pointer" onclick="togliImporto('.$item['id_product'].'); delProduct30('.$item['id_product'].')"><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a>';
			$delete.="<input id='impImporto[".$item['id_product']."]' class='importo' type='hidden' value='0' />";
			
			$sconto_extra = '<input style="text-align:right" name="sconto_extra['.$item['id_product'].']" id="sconto_extra['.$item['id_product'].']" type="text" size="2" value="'.number_format($item['sconto_extra'],2,",","").'" onkeyup="if (isArrowKey(event)) return; calcolaPrezzoScontoExtra('.$item['id_product'].', \'sconto\'); calcolaImporto('.$item['id_product'].');" />';
			
			$reference = '<span style="cursor:pointer" class="span-reference" id="span-reference-'.$item['id_product'].'" title="<strong>Qta disponibile ALLNET</strong>: '.$item['allnet'].' pz.<br /><strong>Qta disponibile Esprinet</strong>: '.$item['esprinet'].' pz.<br /><strong>Qta disponibile ITANCIA</strong>: '.$item['itancia'].' pz.<br /><strong>Qta disponibile MAGAZZINO</strong>: '.$item['magazzino'].' pz.<br /><strong>Qta disponibile TOTALE</strong>: '.$item['qt_tot'].' pz.">'.$item['reference'].'</span>';
			
			echo $item['id_product']." ".$name.' '.$item['reference'].' | '.($item['id_product']).' | '.$price.' | '.$usa_sconti_quantita.' | '.$reference.' |'.$item['name'].'|'.$sc_qta_1.'|'.$sc_qta_2.'|'.$sc_qta_3.'|'.$sc_riv_1.'|'.$sc_riv_2.'|'.$sc_qta_1_q.'|'.$sc_qta_2_q.'|'.$sc_qta_3_q.'|'.$sc_riv_1_q.'|'.$sc_riv_2_q.'|'.$quantity.'|'.$importo.'|'.$unitario.'|'.$delete.'|'.$marg.'|'.$sconto_extra.'|'.$item['qt_tot'].'|'.number_format($prezzounitario, 2,",","")." &euro;".'|'.$varspec.'|'.$item['spring'].'|'.$sc_riv_3.'|'.$sc_riv_3_q.'|'.$i.'|'.$virtuali.'|'.$acquisto.'|'.$link_rewrite."\n";

			$i++;
		}
}