<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/



define('_PS_ADMIN_DIR_', getcwd());
define('PS_ADMIN_DIR', _PS_ADMIN_DIR_); // Retro-compatibility

include(PS_ADMIN_DIR.'/../config/config.inc.php');
/* Getting cookie or logout */
require_once(dirname(__FILE__).'/init.php');

$query = Tools::getValue('q', false);

$array_utils = array();
$array_utils[] = array('Preventivi' => 'https://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini');
$array_utils[] = array('Ti richiamiamo noi' => 'https://www.ezdirect.it/ti-richiamiamo-noi');
$array_utils[] = array('Il tuo account' => 'https://www.ezdirect.it/autenticazione');
$array_utils[] = array('Blog' => 'http://www.ezdirect.it/blog/');
$array_utils[] = array('Offerte speciali' => 'http://www.ezdirect.it/offerte-speciali');
$array_utils[] = array('Assistenza tecnica postvendita' => 'http://www.ezdirect.it/contattaci?step=tecnica');
$array_utils[] = array('Assistenza ordini' => 'http://www.ezdirect.it/contattaci?step=assistenza-ordini');
$array_utils[] = array('Assistenza contabilità' => 'http://www.ezdirect.it/contattaci?step=contabilita');
$array_utils[] = array('Rivenditori' => 'https://www.ezdirect.it/autenticazione?cliente=rivenditore');
$array_utils[] = array('Condizioni di vendita' => 'http://www.ezdirect.it/guide/3-termini-e-condizioni-acquisto-on-line-ezdirect');
$array_utils[] = array('Cashback Jabra' => 'http://www.it.jabra.com/campaigns/cashback');
$array_utils[] = array('Canale YouTube' => 'https://www.youtube.com/user/ezdirectit');
$array_utils[] = array('Centralino Virtuale' => 'https://www.ezdirect.it/centralino-virtuale/');

$cmss = Db::getInstance()->executeS('SELECT * FROM cms_lang WHERE id_lang = 5');
foreach ($cmss as $cms)
{
	$array_utils[] = array($cms['meta_title'] => 'http://www.ezdirect.it/guide/'.$cms['id_cms'].'-'.$cms['link_rewrite'].'.php');
}

foreach ($array_utils as $util)
{	
	foreach($util as $key => $value)
	{
		if (strpos(strtolower($key),strtolower($query)) !== false) 
		{
			echo $key.'|'.$value."\n";
		}
		
	}
}	