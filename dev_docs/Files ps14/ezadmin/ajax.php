<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

define('_PS_ADMIN_DIR_', getcwd());
define('PS_ADMIN_DIR', _PS_ADMIN_DIR_); // Retro-compatibility

include(PS_ADMIN_DIR.'/../config/config.inc.php');
/* Getting cookie or logout */
require_once(dirname(__FILE__).'/init.php');

if(isset($_GET['openAllegato']))
{

	$filename = $_GET['filename'];
		
		if(strpos($filename, ":::")) {
		
			$parti = explode(":::", $filename);
			$nomecodificato = $parti[0];
			$nomevero = $parti[1];
		
		}
		
		else {
		
			$nomecodificato = $filename;
			$nomevero = $filename;
		
		}
		
		$extensions = array('.txt' => 'text/plain', '.rtf' => 'application/rtf', '.doc' => 'application/msword', '.docx'=> 'application/msword',
		'.pdf' => 'application/pdf', '.zip' => 'multipart/x-zip', '.png' => 'image/png', '.jpeg' => 'image/jpeg', '.gif' => 'image/gif', '.jpg' => 'image/jpeg');

		$extension = '';
		foreach ($extensions AS $key => $val)
			if (substr($nomevero, -4) == $key OR substr($nomevero, -5) == $key)
			{
				$extension = $val;
				break;
			}
		
		ob_end_clean();
		header('Content-Type: '.$extension);
		header('Content-Disposition:attachment;filename="'.$nomevero.'"');
		readfile(_PS_UPLOAD_DIR_.$nomecodificato);
		die;
	
}

if(isset($_GET['getOrderPDF'])) {
			
			ob_clean();

			require_once('../classes/html2pdf/html2pdf.class.php');
			$order = new Order(Tools::getValue('id_order'));
			
			$content = Order::getOrderPDF($order->id, $order->id_customer);

			$html2pdf = new HTML2PDF('P','A4','it');
			$html2pdf->WriteHTML($content);
	
			$html2pdf->Output('ordine-n-'.$order->id.'.pdf', 'D'); 
		}
		
		if(isset($_GET['getDDT'])) {
			ob_clean();
			$order = new Order(Tools::getValue('id_order'));
			Fattura::getFattura($order->id, $order->id_customer, 'DDT');
		
		}
		
		if(isset($_GET['getFF'])) {
			ob_clean();
			$order = new Order(Tools::getValue('id_order'));
			Fattura::getFattura($order->id, $order->id_customer, 'ff');
		
		}
		
if(isset($_GET['getPDF'])) {
			
			require_once('../classes/html2pdf/html2pdf.class.php');
			
			$id_cst = Db::getInstance()->getValue("SELECT id_customer FROM cart WHERE id_cart = ".$_GET['id_cart']."");
			if(Tools::getIsset('revisione'))
				$content = Cart::getCartPDF($_GET['id_cart'], $id_cst, 'y', Tools::getValue('revisione'));
			else
				$content = Cart::getCartPDF($_GET['id_cart'], $id_cst, 'y');

			ob_clean(); ob_end_clean(); 
			$html2pdf = new HTML2PDF('P','A4','it');
			$html2pdf->WriteHTML($content);
			if(Tools::getValue('originale') == 'y') {
				$html2pdf->Output($id_cst.'-offerta-'.$_GET['id_cart'].'-originale.pdf', 'D'); 
			}
			else {
				if(Tools::getIsset('revisione'))
					$html2pdf->Output($id_cst.'-offerta-'.$_GET['id_cart'].'-revisione-'.$_GET['revisione'].'.pdf', 'D'); 
				else
					$html2pdf->Output($id_cst.'-offerta-'.$_GET['id_cart'].'.pdf', 'D'); 
			}
		}
		
		if(isset($_GET['getPDFezcloudA'])) {
			require_once('../classes/html2pdf/html2pdf.class.php');
			
			$id_cst = Db::getInstance()->getValue("SELECT id_customer FROM cart WHERE id_cart = ".$_GET['id_cart']."");

			$content = Cart::getCartPDF_ezcloud_A($_GET['id_cart'], $id_cst);

			ob_clean(); ob_end_clean(); 
			$html2pdf = new HTML2PDF('P','A4','it');
			$html2pdf->WriteHTML($content);
			$html2pdf->Output($id_cst.'-offerta-ezcloud-'.$_GET['id_cart'].'-allegato-A.pdf', 'D'); 
		}

		if (isset($_GET['filename'])) {
			$filename = $_GET['filename'];
						
			if(strpos($filename, ":::")) {
			
				$parti = explode(":::", $filename);
				$nomecodificato = $parti[0];
				$nomevero = $parti[1];
							
			}
			
			else {
				$nomecodificato = $filename;
				$nomevero = $filename;
			
			}
						
			if (file_exists(_PS_UPLOAD_DIR_.$nomecodificato)) {
				Cart::openUploadedFile();
			}
			else { }
		} 
		
if (isset($_GET['changeParentUrl']))
	echo '<script type="text/javascript">parent.parent.document.location.href = "'.addslashes(urldecode(Tools::getValue('changeParentUrl'))).'";</script>';
if (isset($_GET['installBoughtModule']))
{
	$file = false;
	while ($file === false OR file_exists(_PS_MODULE_DIR_.$file))
		$file = uniqid();
	$file = _PS_MODULE_DIR_.$file.'.zip';
	$sourceFile = 'http://addons.prestashop.com/iframe/getboughtfile.php?id_order_detail='.Tools::getValue('id_order_detail').'&token='.Tools::getValue('token');
	if (!copy($sourceFile, $file))
	{
		if (!($content = file_get_contents($sourceFile)))
			die(displayJavascriptAlert('Access denied: Please download your module directly from PrestaShop Addons website'));
		elseif (!file_put_contents($file, $content))
			die(displayJavascriptAlert('Local error: your module directory is not writable'));
	}
	$first6 = fread($fd = fopen($file, 'r'), 6);
	if (!strncmp($first6, 'Error:', 6))
	{
		$displayJavascriptAlert = displayJavascriptAlert(fread($fd, 1024));
		fclose($fd);
		unlink($file);
		die($displayJavascriptAlert);
	}
	fclose($fd);
	if (!Tools::ZipExtract($file, _PS_MODULE_DIR_))
	{
		unlink($file);
		die(displayJavascriptAlert('Cannot unzip file'));
	}
	unlink($file);
	die(displayJavascriptAlert('Module copied to disk'));
}

function displayJavascriptAlert($s){echo '<script type="text/javascript">alert(\''.addslashes($s).'\');</script>';}

if (isset($_GET['ajaxProductManufacturers']))
{
	$currentIndex = 'index.php?tab=AdminCatalog';
	$manufacturers = Manufacturer::getManufacturersAdmin();
	if ($manufacturers)
	{
		$jsonArray = array();
		foreach ($manufacturers AS $manufacturer)
		{
			$supplier = Manufacturer::getSupplierById($manufacturer['id_manufacturer']);
			$other_suppliers = Manufacturer::getOtherSuppliersById($manufacturer['id_manufacturer']);
			$margin_min_cli = Db::getInstance()->getValue('SELECT margin_min_cli FROM manufacturer WHERE id_manufacturer = '.$manufacturer['id_manufacturer']);
			$margin_min_riv = Db::getInstance()->getValue('SELECT margin_min_riv FROM manufacturer WHERE id_manufacturer = '.$manufacturer['id_manufacturer']);
			
			$jsonArray[] = '{"optionValue": "'.$manufacturer['id_manufacturer'].'", "margin_min_cli": "'.$margin_min_cli.'", "margin_min_riv": "'.$margin_min_riv.'", "datasupplier": "'.$supplier.'", "dataothersuppliers": "'.$other_suppliers.'", "optionDisplay": "'.htmlspecialchars(trim($manufacturer['name'])).'"}';
		}
		die('['.implode(',', $jsonArray).']');
	}
}
if (isset($_GET['ajaxReferrers']))
{
	require('tabs/AdminReferrers.php');
}

if(Tools::isSubmit('inoltra')) {
	$utenti = $_POST['utenti'];
	$utenti = explode(',',$utenti);
	$tipo_richiesta_p = $_POST['tipo'];
	$cc2 = '';
	$id_messaggio = $_POST['id_messaggio'];
	foreach($utenti as $conoscenza)
	{
		$mailconoscenza = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = $conoscenza");
							
		$tokenimp = Tools::getAdminToken("AdminCustomers"."2".$conoscenza);
		$employeemess = new Employee($_POST['azione_da']);
		
		$cc2 .= $conoscenza.";";
		if($_POST['tipo'] == 'preventivo' || $_POST['tipo'] == 'tirichiamiamonoi')
		{	
			$anno = date('Y');
			$anno = substr($anno, 2,2);
			$sigla = "P";
			$idrichiesta = $sigla.$anno.$_POST['id_thread'];
					
			$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$_POST['id_customer']."&viewcustomer&viewpreventivo&id_thread=".$_POST['id_thread']."&token=".$tokenimp.'&tab-container-1=7';
				
			$params4 = array(
			'{reply}' => "<strong>".$employeemess->firstname."</strong> ha inviato un ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." a un cliente e ti ha messo in copia conoscenza.
			<br /><br />
			L'ID del ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." &egrave;: <strong>".$idrichiesta."</strong><br /><br />
			<a href='".$linkimp."'>Clicca qui per aprire il ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')."</a>. ",
			'{firma}' => $firma,
			'{id_richiesta}' => "Id richiesta: <strong>".$idrichiesta."</strong>");
			$cc1 = Db::getInstance()->getValue('SELECT cc FROM ticket_cc WHERE msg = '.$id_messaggio);
			
			$cc = $cc1.';'.$cc2;
			Db::getInstance()->executeS("REPLACE INTO ticket_cc (msg, type, cc) VALUES (".$id_messaggio.", 'p', '".$cc."')");
					
			Mail::Send((int)$cookie->id_lang, 'senzagrafica', Mail::l(($tipo_richiesta_p == 'tirichiamiamonoi' ? 'Ti richiamiamo noi' : 'Preventivo').' utente in copia conoscenza', (int)$cookie->id_lang), 
			$params4, $mailconoscenza, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
			_PS_MAIL_DIR_, true);
		}
		
		
	}
	die('ok');
}

if (isset($_GET['ajaxProductSuppliers']))
{
	$currentIndex = 'index.php?tab=AdminCatalog';
	$suppliers = Supplier::getSuppliers();
	if ($suppliers)
	{
		$jsonArray = array();
		foreach ($suppliers AS $supplier)
			$jsonArray[] = '{"optionValue": "'.$supplier['id_supplier'].'", "optionDisplay": "'.htmlspecialchars(trim($supplier['name'])).'"}';
		die('['.implode(',', $jsonArray).']');
	}
}

if (isset($_GET['ajaxProductAccessories']))
{
	$currentIndex = 'index.php?tab=AdminCatalog';
	$jsonArray = array();

	$products = Db::getInstance()->ExecuteS('
	SELECT p.`id_product`, pl.`name`
	FROM `'._DB_PREFIX_.'product` p
	NATURAL LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
	WHERE pl.`id_lang` = '.(int)(Tools::getValue('id_lang')).'
	AND p.`id_product` != '.(int)(Tools::getValue('id_product')).'
	AND p.`id_product` NOT IN (
		SELECT a.`id_product_2`
		FROM `'._DB_PREFIX_.'accessory` a
		WHERE a.`id_product_1` = '.(int)(Tools::getValue('id_product')).')
	ORDER BY pl.`name`');

	foreach ($products AS $accessory)
		$jsonArray[] = '{"value: "'.(int)($accessory['id_product']).'-'.addslashes($accessory['name']).'", "text":"'.(int)($accessory['id_product']).' - '.addslashes($accessory['name']).'"}';
	die('['.implode(',', $jsonArray).']');
}

if (isset($_GET['ajaxDiscountCustomers']))
{
	global $cookie;

	$currentIndex = 'index.php?tab=AdminDiscounts';
	$jsonArray = array();
	$filter = Tools::getValue('filter');

	if (Validate::isBool_Id($filter))
		$filterArray = explode('_', $filter);

	$customers = Db::getInstance()->ExecuteS('
	SELECT `id_customer`, `email`, CONCAT(`lastname`, \' \', `firstname`) as name
	FROM `'._DB_PREFIX_.'customer`
	WHERE `deleted` = 0 AND is_guest = 0
	AND '.(Validate::isUnsignedInt($filter) ? '`id_customer` = '.(int)($filter) : '(`email` LIKE "%'.pSQL($filter).'%"
	'.((Validate::isBool_Id($filter) AND $filterArray[0] == 0) ? 'OR `id_customer` = '.(int)($filterArray[1]) : '').'
	'.(Validate::isUnsignedInt($filter) ? '`id_customer` = '.(int)($filter) : '').'
	OR CONCAT(`firstname`, \' \', `lastname`) LIKE "%'.pSQL($filter).'%"
	OR CONCAT(`lastname`, \' \', `firstname`) LIKE "%'.pSQL($filter).'%")').'
	ORDER BY CONCAT(`lastname`, \' \', `firstname`) ASC
	LIMIT 50');

	$groups = Db::getInstance()->ExecuteS('
	SELECT g.`id_group`, gl.`name`
	FROM `'._DB_PREFIX_.'group` g
	LEFT JOIN `'._DB_PREFIX_.'group_lang` AS gl ON (g.`id_group` = gl.`id_group` AND gl.`id_lang` = '.(int)($cookie->id_lang).')
	WHERE '.(Validate::isUnsignedInt($filter) ? 'g.`id_group` = '.(int)($filter) : 'gl.`name` LIKE "%'.pSQL($filter).'%"
	'.((Validate::isBool_Id($filter) AND $filterArray[0] == 1) ? 'OR g.`id_group` = '.(int)($filterArray[1]) : '')).'
	ORDER BY gl.`name` ASC
	LIMIT 50');

	$json = '{"customers" : ';
	foreach ($customers AS $customer)
		$jsonArray[] = '{"value":"0_'.(int)($customer['id_customer']).'", "text":"'.addslashes($customer['name']).' ('.addslashes($customer['email']).')"}';
	$json .= '['.implode(',', $jsonArray).'],
		"groups" : ';
	$jsonArray = array();
	foreach ($groups AS $group)
		$jsonArray[] = '{"value":"1_'.(int)($group['id_group']).'", "text":"'.addslashes($group['name']).'"}';
	$json .= '['.implode(',', $jsonArray).']}';
	die($json);
}

if (Tools::getValue('page') == 'prestastore' AND @fsockopen('addons.prestashop.com', 80, $errno, $errst, 3))
	readfile('http://addons.prestashop.com/adminmodules.php?lang='.Language::getIsoById($cookie->id_lang));
if (Tools::getValue('page') == 'themes'  AND @fsockopen('addons.prestashop.com', 80, $errno, $errst, 3))
	readfile('http://addons.prestashop.com/adminthemes.php?lang='.Language::getIsoById($cookie->id_lang));

if ($step = (int)(Tools::getValue('ajaxProductTab')))
{
	require_once(dirname(__FILE__).'/tabs/AdminCatalog.php');
	$catalog = new AdminCatalog();
	$admin = new AdminProducts();

	$languages = Language::getLanguages(false);
	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	$product = new Product((int)(Tools::getValue('id_product')));
	if (!Validate::isLoadedObject($product))
		die (Tools::displayError('Product cannot be loaded'));

	$switchArray = array(3 => 'displayFormPrices', 4 => 'displayFormAttributes', 5 => 'displayFormFeatures', 6 => 'displayFormCustomization', 7 => 'displayFormAttachments');
	$currentIndex = 'index.php?tab=AdminCatalog';
	if (key_exists($step, $switchArray))
		$admin->{$switchArray[$step]}($product, $languages, $defaultLanguage);
}

if (isset($_GET['getAvailableFields']) and isset($_GET['entity']))
{
	$currentIndex = 'index.php?tab=AdminImport';
	$jsonArray = array();
	require_once(dirname(__FILE__).'/tabs/AdminImport.php');
	$import = new AdminImport();

	$languages = Language::getLanguages(false);
	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	$fields = $import->getAvailableFields(true);
	foreach ($fields AS $field)
		$jsonArray[] = '{"field":"'.addslashes($field).'"}';
	die('['.implode(',', $jsonArray).']');
}

if (array_key_exists('ajaxModulesPositions', $_POST))
{
	$id_module = (int)(Tools::getValue('id_module'));
	$id_hook = (int)(Tools::getValue('id_hook'));
	$way = (int)(Tools::getValue('way'));
	$positions = Tools::getValue(strval($id_hook));
	$position = (is_array($positions)) ? array_search($id_hook.'_'.$id_module, $positions) : null;
	$module = Module::getInstanceById($id_module);
	if (Validate::isLoadedObject($module))
		if ($module->updatePosition($id_hook, $way, $position))
			die(true);
		else
			die('{"hasError" : true, "errors" : "Can not update module position"}');
	else
		die('{"hasError" : true, "errors" : "This module can not be loaded"}');
}

if (array_key_exists('ajaxCategoriesPositions', $_POST))
{
	$id_category_to_move = (int)(Tools::getValue('id_category_to_move'));
	$id_category_parent = (int)(Tools::getValue('id_category_parent'));
	$way = (int)(Tools::getValue('way'));
	$positions = Tools::getValue('category');
	if (is_array($positions))
		foreach ($positions AS $key => $value)
		{
			$pos = explode('_', $value);
			if ((isset($pos[1]) AND isset($pos[2])) AND ($pos[1] == $id_category_parent AND $pos[2] == $id_category_to_move))
			{
				$position = $key;
				break;
			}
		}
	$category = new Category($id_category_to_move);
	if (Validate::isLoadedObject($category))
	{
		if (isset($position) && $category->updatePosition($way, $position))
		{
			Category::regenerateEntireNtree();
			Module::hookExec('categoryUpdate');
			die(true);
		}
		else
			die('{"hasError" : true, errors : "Can not update categories position"}');
	}
	else
		die('{"hasError" : true, "errors" : "This category can not be loaded"}');
}

if (array_key_exists('ajaxCMSCategoriesPositions', $_POST))
{
	$id_cms_category_to_move = (int)(Tools::getValue('id_cms_category_to_move'));
	$id_cms_category_parent = (int)(Tools::getValue('id_cms_category_parent'));
	$way = (int)(Tools::getValue('way'));
	$positions = Tools::getValue('cms_category');
	if (is_array($positions))
		foreach ($positions AS $key => $value)
		{
			$pos = explode('_', $value);
			if ((isset($pos[1]) AND isset($pos[2])) AND ($pos[1] == $id_cms_category_parent AND $pos[2] == $id_cms_category_to_move))
			{
				$position = $key;
				break;
			}
		}
	$cms_category = new CMSCategory($id_cms_category_to_move);
	if (Validate::isLoadedObject($cms_category))
	{
		if (isset($position) && $cms_category->updatePosition($way, $position))
			die(true);
		else
			die('{"hasError" : true, "errors" : "Can not update cms categories position"}');
	}
	else
		die('{"hasError" : true, "errors" : "This cms category can not be loaded"}');
}

if (array_key_exists('ajaxCMSPositions', $_POST))
{
	$id_cms = (int)(Tools::getValue('id_cms'));
	$id_category = (int)(Tools::getValue('id_cms_category'));
	$way = (int)(Tools::getValue('way'));
	$positions = Tools::getValue('cms');
	if (is_array($positions))
		foreach ($positions AS $key => $value)
		{
			$pos = explode('_', $value);
			if ((isset($pos[1]) AND isset($pos[2])) AND ($pos[1] == $id_category AND $pos[2] == $id_cms))
			{
				$position = $key;
				break;
			}
		}
	$cms = new CMS($id_cms);
	if (Validate::isLoadedObject($cms))
	{
		if (isset($position) && $cms->updatePosition($way, $position))
			die(true);
		else
			die('{"hasError" : true, "errors" : "Can not update cms position"}');
	}
	else
		die('{"hasError" : true, "errors" : "This cms can not be loaded"}');
}

/* Modify product position in catalog */
if (array_key_exists('ajaxProductsPositions', $_POST))
{
	$way = (int)(Tools::getValue('way'));
	$id_product = (int)(Tools::getValue('id_product'));
	$id_category = (int)(Tools::getValue('id_category'));
	$positions = Tools::getValue('product');

	if (is_array($positions))
		foreach ($positions AS $position => $value)
		{
			// pos[1] = id_categ, pos[2] = id_product, pos[3]=old position
			$pos = explode('_', $value);

			if ((isset($pos[1]) AND isset($pos[2])) AND ($pos[1] == $id_category AND (int)$pos[2] === $id_product))
			{
				if ($product = new Product((int)$pos[2]))
					if (isset($position) && $product->updatePosition($way, $position))
						echo "ok position $position for product $pos[2]\r\n";
					else
						echo '{"hasError" : true, "errors" : "Can not update product '. $id_product . ' to position '.$position.' "}';
				else
					echo '{"hasError" : true, "errors" : "This product ('.$id_product.') can t be loaded"}';

				break;
			}
		}
}

if (isset($_GET['ajaxProductPackItems']))
{
	$jsonArray = array();
	$products = Db::getInstance()->ExecuteS('
	SELECT p.`id_product`, pl.`name`
	FROM `'._DB_PREFIX_.'product` p
	NATURAL LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
	WHERE pl.`id_lang` = '.(int)(Tools::getValue('id_lang')).'
	AND p.`id_product` NOT IN (SELECT DISTINCT id_product_pack FROM `'._DB_PREFIX_.'pack`)
	AND p.`id_product` != '.(int)(Tools::getValue('id_product')));

	foreach ($products AS $packItem)
		$jsonArray[] = '{"value": "'.(int)($packItem['id_product']).'-'.addslashes($packItem['name']).'", "text":"'.(int)($packItem['id_product']).' - '.addslashes($packItem['name']).'"}';
	die('['.implode(',', $jsonArray).']');
}

if (isset($_GET['ajaxStates']) AND isset($_GET['id_country']))
{
	$states = Db::getInstance()->ExecuteS('
	SELECT s.id_state, s.name
	FROM '._DB_PREFIX_.'state s
	LEFT JOIN '._DB_PREFIX_.'country c ON (s.`id_country` = c.`id_country`)
	WHERE s.id_country = '.(int)(Tools::getValue('id_country')).' AND s.active = 1 AND c.`contains_states` = 1
	ORDER BY s.`name` ASC');

	if (is_array($states) AND !empty($states))
	{
		$list = '';
		if (Tools::getValue('no_empty') != true)
			$list = '<option value="0">-----------</option>'."\n";

		foreach ($states AS $state)
			$list .= '<option value="'.(int)($state['id_state']).'"'.((isset($_GET['id_state']) AND $_GET['id_state'] == $state['id_state']) ? ' selected="selected"' : '').'>'.$state['name'].'</option>'."\n";
	}
	else
		$list = 'false';

	die($list);
}

if (Tools::isSubmit('submitCustomerNote') AND $id_customer = (int)Tools::getValue('id_customer'))
{
	$note = html_entity_decode(Tools::getValue('note'));
	if (!empty($note) AND !Validate::isCleanHtml($note))
		die ('error:validation');
	if (!Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'customer SET `note` = "'.pSQL($note, true).'" WHERE id_customer = '.(int)$id_customer.' LIMIT 1'))
		die ('error:update');
	die('ok');
}

if (Tools::isSubmit('aggiornaPeriodoForecast'))
{
	/*Db::getInstance()->executeS('DELETE FROM forecast WHERE id_product = '.Tools::getValue('id_product'));
	Db::getInstance()->executeS('INSERT INTO forecast (id_product, periodo) VALUES ("'.Tools::getValue('id_product').'", "'.Tools::getValue('periodo').'")');*/
	
	Db::getInstance()->executeS('DELETE FROM forecast WHERE id_product = 11111');
	Db::getInstance()->executeS('INSERT INTO forecast (id_product, periodo) VALUES ("11111", "'.Tools::getValue('periodo').'")');
	
	die('ok');
}


if (Tools::isSubmit('submitOrdineDatiTecnici'))
{

	$to_update = Db::getInstance()->getValue('SELECT riga FROM dati_tecnici_prodotti WHERE id_order_detail = '.$_POST['id_order_detail'].' AND id_product = '.$_POST['id_product'].' AND riga = '.$_POST['riga'].'');
	if($to_update > 0)
		Db::getInstance()->execute('UPDATE dati_tecnici_prodotti SET seriale = "'.$_POST['seriale'].'", mac = "'.$_POST['mac'].'", imei = "'.$_POST['imei'].'", note = "'.$_POST['note'].'" WHERE id_order_detail = '.$_POST['id_order_detail'].' AND id_product = '.$_POST['id_product'].' AND riga = '.$_POST['riga']);
	else
		Db::getInstance()->execute('INSERT INTO dati_tecnici_prodotti (id_order_detail, id_product, riga, seriale, mac, imei, note) VALUES ('.$_POST['id_order_detail'].', '.$_POST['id_product'].','.$_POST['riga'].',"'.$_POST['seriale'].'","'.$_POST['mac'].'","'.$_POST['imei'].'","'.$_POST['note'].'")');
	
	die ('ok');
	
}


if (Tools::isSubmit('load_template'))
{

	Db::getInstance()->execute('UPDATE cart SET premessa = "'.$_POST['premessa'].'", risorse = "'.$_POST['risorse'].'", esigenze = "'.$_POST['esigenze'].'" WHERE id_cart = "'.$_POST['id_cart'].'"');
	Db::getInstance()->execute('UPDATE carrelli_creati SET premessa = "'.$_POST['premessa'].'", risorse = "'.$_POST['risorse'].'", esigenze = "'.$_POST['esigenze'].'" WHERE id_cart = "'.$_POST['id_cart'].'"');
	$valori = Db::getInstance()->getRow('SELECT name, note, premessa, risorse, esigenze, note_private FROM cart WHERE id_cart = '.Tools::getValue('id_template'));
	$risposta = $valori['name']."|||".$valori['note']."|||".$valori['premessa']."|||".$valori['risorse']."|||".$valori['esigenze']."|||".$valori['note_private'];
	
	die($risposta);
}

if (Tools::isSubmit('load_stats'))
{
	
	include('../modules/statsforecast/'.$_POST['stats'].'.php');
	
}

if (Tools::isSubmit('repopulate_series'))
{
	$series_rep = array();

	if(Tools::getIsset('id_manufacturer') && Tools::getValue('id_manufacturer') > 0)
	{
		$series = Db::getInstance()->executeS('SELECT * FROM feature_value_lang fvl JOIN feature_value fv ON fvl.id_feature_value = fv.id_feature_value JOIN feature_product fp ON fp.id_feature_value = fv.id_feature_value JOIN product p ON fp.id_product = p.id_product WHERE id_manufacturer = '.$_POST['id_manufacturer'].' AND fvl.id_lang = 5 AND fv.id_feature = 917 GROUP BY fv.id_feature_value ORDER BY value ASC');
	}
	else
	{
		$series_rep['Scegli prima un costruttore...'] = 0;
	}
	
	foreach($series as $serie)
		$series_rep[$serie['value']] = $serie['id_feature_value'];
	
	die(json_encode($series_rep));
}



if (Tools::isSubmit('togli_allegati'))
{

	$ids = $_POST['id_allegati'];
	$risposta = '';
	
	foreach($ids as $allegato)
	{
		$name = Db::getInstance()->getValue('SELECT file FROM attachment WHERE id_attachment = '.$allegato);
		unlink('../download/'.$name);
		Db::getInstance()->execute('DELETE FROM attachment WHERE id_attachment = '.$allegato);
		Db::getInstance()->execute('DELETE FROM attachment_lang WHERE id_attachment = '.$allegato);
		Db::getInstance()->execute('DELETE FROM product_attachment WHERE id_attachment = '.$allegato);
	}
	die($risposta);
}

if(Tools::getIsset('cancellanota_customer') && Tools::getValue('cancellanota_customer') == 'y') {
			Db::getInstance()->executeS("DELETE FROM customer_note WHERE id_note = ".Tools::getValue('id_note')."");
		}

if(Tools::getIsset('cancellanota_cart') && Tools::getValue('cancellanota_cart') == 'y') {
			Db::getInstance()->executeS("DELETE FROM cart_note WHERE id_note = ".Tools::getValue('id_note')."");
		}
	

if(Tools::getIsset('cancellanota_attivita') && Tools::getValue('cancella_nota') == 'y') {
			Db::getInstance()->executeS("DELETE FROM note_attivita WHERE id_note = ".Tools::getValue('id_note')."");
		}	

if (Tools::isSubmit('cancella_allegato_carrello'))
{

	$nome_allegato = $_POST['nome'];
	
	$attachment = Db::getInstance()->getValue('SELECT attachment FROM cart WHERE id_cart = '.$_POST['id_cart']);
	$attachments = explode(';',$attachment);
	
	$attachment_new = '';
	
	foreach($attachments as $attachment)
	{
		if($attachment != $nome_allegato)
			$attachment_new .= $attachment.';';
	}
	
	Db::getInstance()->executeS('UPDATE cart SET attachment = "'.$attachment_new.'" WHERE id_cart = '.$_POST['id_cart']);
	Db::getInstance()->executeS('UPDATE carrelli_creati SET attachment = "'.$attachment_new.'" WHERE id_cart = '.$_POST['id_cart']);
	
	die ('ok');
	
}

if (Tools::isSubmit('salvaCAP'))
{
	Db::getInstance()->executeS('UPDATE cap SET cap = "'.Tools::getValue('cap').'", comune = "'.Tools::getValue('comune').'" WHERE cod_istat = "'.Tools::getValue('cod_istat').'"');
	
	Db::getInstance()->executeS('UPDATE comuni SET comune = "'.Tools::getValue('comune').'" WHERE cod_istat = "'.Tools::getValue('cod_istat').'"');
	
	die('ok');
}

if (Tools::isSubmit('aggiorna_dati_cliente'))
{
	if(Tools::getValue('tipo') == 'agente') {
	
		$total = Db::getInstance()->getValue('SELECT count(id_customer) FROM customer_amministrazione WHERE id_customer = '.Tools::getValue('cliente'));
		
		if($total == 0) 
			Db::getInstance()->executeS('INSERT INTO customer_amministrazione (id_customer, agente) VALUES ('.Tools::getValue('cliente').', '.Tools::getValue('valore').')');
		else
			Db::getInstance()->executeS('UPDATE customer_amministrazione SET agente = '.Tools::getValue('valore').' WHERE id_customer = '.Tools::getValue('cliente'));
		
		
		die('Agente');
	
	}
	
	if(Tools::getValue('tipo') == 'tecnico') {
	
		$total = Db::getInstance()->getValue('SELECT count(id_customer) FROM customer_amministrazione WHERE id_customer = '.Tools::getValue('cliente'));
		
		if($total == 0) 
			Db::getInstance()->executeS('INSERT INTO customer_amministrazione (id_customer, tecnico) VALUES ('.Tools::getValue('cliente').', '.Tools::getValue('valore').')');
		else
			Db::getInstance()->executeS('UPDATE customer_amministrazione SET tecnico = '.Tools::getValue('valore').' WHERE id_customer = '.Tools::getValue('cliente'));
			
		
		die('Tecnico');
	
	}
	
	if(Tools::getValue('tipo') == 'referente') {
	

		Db::getInstance()->executeS('UPDATE customer SET referente = '.Tools::getValue('valore').' WHERE id_customer = '.Tools::getValue('cliente'));
			
		
		die('Referente');
	
	}
}

if (Tools::isSubmit('submitOrdineNotaPrivata') AND $id_customer = (int)Tools::getValue('id_customer') AND $id_cart = (int)Tools::getValue('id_cart'))
{
	global $cookie;
	
	$note = html_entity_decode(Tools::getValue('note'));
	$notePrivate = html_entity_decode(Tools::getValue('notePrivate'));
	$esigenze = html_entity_decode(Tools::getValue('esigenze'));
	$risorse = html_entity_decode(Tools::getValue('risorse'));
	$premessa = html_entity_decode(Tools::getValue('premessa'));
	
	foreach ($_POST['note_nuova'] as $nota_r) {
		
		$nota = explode("***", $nota_r);
		
		$nota_key = str_replace('note_nota[','',(str_replace(']','',$nota[0])));
		
		if($nota_key > 0) {
			
			$testo_nota = Db::getInstance()->getValue('SELECT note FROM cart_note WHERE id_note = '.$nota_key.'');
			if($testo_nota != $_POST['note_nota'][$id_nota])
				Db::getInstance()->executeS("UPDATE cart_note SET note = '".htmlentities($nota[1],ENT_QUOTES)."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_note = ".$nota_key.""); 
		}
		
		else {
			Db::getInstance()->executeS("INSERT INTO cart_note
			(id_note,
			id_cart,
			id_employee,
			note,
			date_add,
			date_upd)
			
			VALUES (
			NULL,
			'".$id_cart."',
			'".$cookie->id_employee."',
			'".htmlentities($nota[1],ENT_QUOTES)."',
			'".date("Y-m-d H:i:s")."',
			'".date("Y-m-d H:i:s")."'
			)");
			
			
		}
	}
	
	if (!empty($note) AND !Validate::isCleanHtml($note))
		die ('error:validation');
	if (!Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'cart SET `note` = "'.pSQL($note, true).'", `note_private` = "'.pSQL($notePrivate, true).'",`esigenze` = "'.pSQL($esigenze, true).'",`premessa` = "'.pSQL($premessa, true).'",`risorse` = "'.pSQL($risorse, true).'" WHERE id_customer = '.(int)$id_customer.' AND id_cart = '.(int)$id_cart.'')) {
		die ('error:update');
	}
	else {
		 Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'carrelli_creati SET `note` = "'.pSQL($note, true).'", `note_private` = "'.pSQL($notePrivate, true).'",`premessa` = "'.pSQL($premessa, true).'",`esigenze` = "'.pSQL($esigenze, true).'",`risorse` = "'.pSQL($risorse, true).'" WHERE id_customer = '.(int)$id_customer.' AND id_cart = '.(int)$id_cart.'');
		die('ok');
	}
}

if (Tools::isSubmit('submitNoFeedback') AND $id_customer = (int)Tools::getValue('id_customer') AND $id_order = (int)Tools::getValue('id_order'))
{
	global $cookie;
	
	$no_feedback = (Tools::getValue('no_feedback'));
	$no_feedback_cause = html_entity_decode(Tools::getValue('no_feedback_cause'));
	
	if (!Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'orders SET `no_feedback` = "'.pSQL($no_feedback, true).'", `no_feedback_cause` = "'.pSQL($no_feedback_cause, true).'" WHERE id_customer = '.(int)$id_customer.' AND id_order = '.(int)$id_order.'')) {
		die ('error:update');
	}
	else {
		 Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'orders SET `no_feedback` = "'.pSQL($no_feedback, true).'", `no_feedback_cause` = "'.pSQL($no_feedback_cause, true).'" WHERE id_customer = '.(int)$id_customer.' AND id_order = '.(int)$id_order.'');
		die('ok');
	}
}

if (Tools::isSubmit('submitRifOrdine') AND $id_customer = (int)Tools::getValue('id_customer'))
{
	$rif_ordine = html_entity_decode(Tools::getValue('rif_ordine'));
	
	$id_order = Tools::getValue('id_order');
	$id_cart = Db::getInstance()->getValue('SELECT id_cart FROM orders WHERE id_order = '.$id_order);
	
	Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'orders SET `rif_ordine` = "'.pSQL($rif_ordine, true).'" WHERE id_customer = '.(int)$id_customer.' AND id_order = '.(int)$id_order.'');
	
	Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'cart SET `rif_ordine` = "'.pSQL($rif_ordine, true).'" WHERE id_customer = '.(int)$id_customer.' AND id_cart = '.(int)$id_cart.'');
	
	Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'carrelli_creati SET `rif_ordine` = "'.pSQL($rif_ordine, true).'" WHERE id_customer = '.(int)$id_customer.' AND id_cart = '.(int)$id_cart.'');
		
		
	die('ok');
	
}

if (Tools::isSubmit('submitEsolver'))
{
	
	Db::getInstance()->Execute("REPLACE INTO product_esolver VALUES ($object->id, '".$_POST['rebate_1']."', '".$_POST['rebate_2']."', '".$_POST['rebate_3']."', '".$_POST['wholesale_price_esolver']."')");
	
	Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'product SET `listino` = "'.$_POST['listino_esolver'].'", sconto_acquisto_1 = "'.$_POST['sconto_acquisto_1'].'", sconto_acquisto_2 = "'.$_POST['sconto_acquisto_2'].'", sconto_acquisto_3 = "'.$_POST['sconto_acquisto_3'].'" WHERE id_product = '.$_POST['id_product'].'');
		
		
	die('ok');
	
}


if (Tools::isSubmit('activeBundle'))
{
	Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'bundle SET active = '.$_POST['active'].' WHERE id_bundle = '.$_POST['id_bundle'].'');
		
	if($_POST['active'] == 0)	
		die(1);
	else
		die(0);
}

if (Tools::isSubmit('submitCreaRevisione') AND $id_cart = (int)Tools::getValue('id_cart'))
{
	$da_copiare = Db::getInstance()->getRow('SELECT * FROM cart WHERE id_cart = '.$id_cart.'');
	$data_rev = date('Y-m-d H:i:s')	;			
	mysql_select_db (_DB_REV_NAME_);
						 
	mysql_query('INSERT INTO cart (id_revisione, id_cart, id_carrier, id_lang, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, date_add, date_upd, rif_prev, name, riferimento, risorse, esigenze, note, note_private, preventivo, validita, consegna, in_carico_a, payment, total_products, attachment, created_by, id_employee, numero_notifiche, prezzi_carrello) VALUES (NULL, '.$id_cart.', "'.$da_copiare['id_carrier'].'", "'.$da_copiare['id_lang'].'","'.addslashes($da_copiare['id_address_delivery']).'","'.addslashes($da_copiare['id_address_invoice']).'",1,'.$da_copiare['id_customer'].',0,"",0,0,"","'.$data_rev.'", "'.$data_rev.'", "'.addslashes($da_copiare['rif_prev']).'", "'.addslashes($da_copiare['name']).'", "'.addslashes($da_copiare['riferimento']).'", "'.addslashes($da_copiare['risorse']).'","'.addslashes($da_copiare['esigenze']).'","'.addslashes($da_copiare['note']).'","'.addslashes($da_copiare['note_private']).'","'.addslashes($da_copiare['preventivo']).'","'.$da_copiare['validita'].'","'.addslashes($da_copiare['consegna']).'","'.addslashes($da_copiare['in_carico_a']).'","'.$da_copiare['payment'].'","'.$da_copiare['total_products'].'","'.$da_copiare['attachment'].'",'.$cookie->id_employee.',
	'.$cookie->id_employee.',0,'.$da_copiare['prezzi_carrello'].')');
			
	$totale = Db::getInstance()->getValue('SELECT count(id_revisione) FROM cart WHERE id_cart = '.$id_cart.'');		
			
	//mysql_select_db (_DB_NAME_);
	$tokenCarts = Tools::getAdminToken('AdminCarts'.(int)(Tab::getIdFromClassName('AdminCarts')).Tools::getValue('id_employee'));				
					
	$prodotti_da_copiare = Db::getInstance()->executeS('SELECT * FROM cart_product WHERE id_cart = '.trim($id_cart).'');
						
	//mysql_select_db (_DB_REV_NAME_);
	$res_rev = mysql_query('SELECT id_revisione FROM cart_revisioni ORDER BY id_revisione DESC LIMIT 1');
	$row = mysql_fetch_array($res_rev, MYSQL_ASSOC);
	$id_revisione = $row['id_revisione'];
	
						
	foreach($prodotti_da_copiare as $prodotto_da_copiare) 
		{
							
			mysql_query('INSERT INTO cart_product_revisioni (id_revisione, id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, sconto_extra, prezzo_acquisto, no_acq, sort_order, bundle, date_add) VALUES ('.$id_revisione.', '.$id_cart.', "'.$prodotto_da_copiare['id_product'].'", "'.$prodotto_da_copiare['id_product_attribute'].'", "'.$prodotto_da_copiare['quantity'].'", "'.$prodotto_da_copiare['price'].'","'.$prodotto_da_copiare['free'].'","'.addslashes($prodotto_da_copiare['name']).'","'.$prodotto_da_copiare['sc_qta'].'","'.$prodotto_da_copiare['sconto_extra'].'","'.$prodotto_da_copiare['wholesale_price'].'","'.$prodotto_da_copiare['no_acq'].'","'.$prodotto_da_copiare['sort_order'].'","'.$prodotto_da_copiare['bundle'].'","'.date('Y-m-d H:i:s').'")'); 
								
		}
	//mysql_select_db (_DB_NAME_);
	$impiegato = Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$cookie->id_employee.'');
	
	$revisioni = Db::getInstance()->getValue('SELECT revisioni FROM cart WHERE id_cart = '.trim($id_cart).'');
	$revisioni = $revisioni+1;
	Db::getInstance()->executeS('UPDATE cart SET revisioni = '.$revisioni.' WHERE id_cart = '.trim($id_cart).'');
	Db::getInstance()->executeS('UPDATE carrelli_creati SET revisioni = '.$revisioni.' WHERE id_cart = '.trim($id_cart).'');
	
	$option = '<option onclick="// window.open(\'index.php?tab=AdminCarts&id_cart='.$id_cart.'&viewcart&revisione='.$id_revisione.'&getPDF=y&token='.$tokenCarts.'\')" value="'.$id_revisione.'">'.$id_cart.'-'.($totale-1).' - '.Tools::displayDate($data_rev, $cookie->id_lang, true).' ('.$impiegato.')</option>
	<option value="totrevisioni" selected="selected">'.$id_cart.'-'.($totale).' - '.Tools::displayDate($data_rev, $cookie->id_lang, true).' ('.$impiegato.')</option>
	';
	
	
	die('ok;'.$totale.';'.$id_revisione.';'.$option);
}

if (Tools::isSubmit('submitRispostaCommento') AND $id_customer = (int)Tools::getValue('id_customer') AND $id_recensione = (int)Tools::getValue('id_recensione'))
{
	$risposta = html_entity_decode(Tools::getValue('rispostaContent'));
	if (!empty($risposta) AND !Validate::isCleanHtml($risposta))
		die ('error:validation');
	if (!Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'product_comment SET `replica` = "'.pSQL($risposta, true).'" WHERE id_customer = '.(int)$id_customer.' AND id_product_comment = '.(int)$id_recensione.'')) {
		die ('error:update');
	}
	else {
		die('ok');
	}
}

if (Tools::isSubmit('submitTrgt'))
{
	$obiettivi_anno = array();
	$obiettivi_anno['totale'] = Tools::getValue('totale');
	$obiettivi_anno['q1'] = Tools::getValue('q1');
	$obiettivi_anno['q2'] = Tools::getValue('q2');
	$obiettivi_anno['q3'] = Tools::getValue('q3');
	$obiettivi_anno['q4'] = Tools::getValue('q4');
	$obiettivi = serialize($obiettivi_anno);
	
	$obiettivi_attuali = Db::getInstance()->getRow('SELECT * FROM trgt WHERE anno = "'.Tools::getValue('anno').'" AND descrizione = "'.Tools::getValue('descrizione').'"');
	if(!empty($obiettivi_attuali['descrizione'])) {
		if (!Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'trgt SET `configurazione` = \''.$obiettivi.'\' WHERE anno = "'.Tools::getValue('anno').'" AND descrizione = "'.Tools::getValue('descrizione').'"'))
			die ('error:update');
		die('ok');
	
	}
	else 
	{
		if (!Db::getInstance()->Execute('INSERT INTO trgt (id, descrizione, anno, configurazione) VALUES (NULL, "'.Tools::getValue('descrizione').'", "'.Tools::getValue('anno').'", \''.$obiettivi.'\')'))
			die ('error:update');
		die('ok');
	
	}

}


if (Tools::isSubmit('getManufacturerQuarter'))
{
	$manufacturer = $_POST['id_manufacturer'];
	
	$costruttori = explode(";",$_POST['costruttori']);
	
	$home_manufacturers = array();
	
	foreach($costruttori as $cm)
	{
		if($cm != '')
			$home_manufacturers[] = $cm;
	}	

	$home_manufacturers = serialize($home_manufacturers);
	
	$home_manufacturers_id = Db::getInstance()->getValue('SELECT id_ck FROM fixed_ck WHERE id_employee = '.$cookie->id_employee.' AND name = "home_manufacturers"');
	if($home_manufacturers_id > 0)
		Db::getInstance()->execute('UPDATE fixed_ck SET value = "'.addslashes($home_manufacturers).'" WHERE id_employee = '.$cookie->id_employee.' AND name = "home_manufacturers"');
	else
		Db::getInstance()->execute('INSERT INTO fixed_ck VALUES (NULL, '.$cookie->id_employee.', "home_manufacturers", "'.addslashes($home_manufacturers).'")');
	

	$vendite_anno = (Db::getInstance()->getValue('
	SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) as total
	FROM '._DB_PREFIX_.'orders o JOIN customer c ON o.id_customer = c.id_customer LEFT JOIN order_history oh ON oh.id_order_state =6 AND oh.id_order = o.id_order
	LEFT JOIN order_detail od ON o.id_order = od.id_order WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire"  OR o.module = "other_payment"))) AND oh.id_order_state IS NULL	AND od.manufacturer_id = '.$manufacturer.' AND o.`date_add` BETWEEN "'.date('Y-01-01').' 00:00:00" AND "'.date('Y-12-31').' 23:59:59"')+$tot_bdl_anno);
	
	if($manufacturer == 105)
	{
		$tot_bdl_anno = Db::getInstance()->getValue('SELECT SUM(importo) FROM '._DB_PREFIX_.'bdl WHERE importo > 0 AND invio_contabilita > 0 AND rif_ordine = 0 AND date_add  BETWEEN "'.date('Y-01-01').' 00:00:00" AND "'.date('Y-12-31').' 23:59:59"');
		$vendite_anno += $tot_bdl_anno;
	}	
	$target_anno = Db::getInstance()->getValue('SELECT configurazione FROM trgt WHERE descrizione = "m-'.$manufacturer.'-'.date('Y').'" AND anno = "'.date('Y').'"');
	$target_anno = unserialize($target_anno);
	
	$target_percent = ($vendite_anno*100)/$target_anno['totale'];

	die(number_format($target_percent,2,",",""));
}

if (Tools::isSubmit('removeManufacturerFromCookie'))
{
	$costruttori = explode(";",$_POST['costruttori']);
	
	$home_manufacturers = array();
	
	foreach($costruttori as $cm)
	{
		if($cm != '')
			$home_manufacturers[] = $cm;
	}	

	$home_manufacturers = serialize($home_manufacturers);
	
	$home_manufacturers_id = Db::getInstance()->getValue('SELECT id_ck FROM fixed_ck WHERE id_employee = '.$cookie->id_employee.' AND name = "home_manufacturers"');
	
	if($home_manufacturers_id > 0)
		Db::getInstance()->execute('UPDATE fixed_ck SET value = "'.addslashes($home_manufacturers).'" WHERE id_employee = '.$cookie->id_employee.' AND name = "home_manufacturers"');
	else
		Db::getInstance()->execute('INSERT INTO fixed_ck VALUES (NULL, '.$cookie->id_employee.', "home_manufacturers", "'.addslashes($home_manufacturers).'")');

}	

if (Tools::isSubmit('nascondiMostraPlanner'))
{
	$mostra_planner_id = Db::getInstance()->getValue('SELECT id_ck FROM fixed_ck WHERE id_employee = '.$cookie->id_employee.' AND name = "hidden_planner"');
	
	if($mostra_planner_id > 0)
		Db::getInstance()->execute('UPDATE fixed_ck SET value = "'.addslashes($_POST['hidden_planner']).'" WHERE id_employee = '.$cookie->id_employee.' AND name = "hidden_planner"');
	else
		Db::getInstance()->execute('INSERT INTO fixed_ck VALUES (NULL, '.$cookie->id_employee.', "hidden_planner", "'.addslashes($_POST['hidden_planner']).'")');
}

if (Tools::isSubmit('nascondiMostraSeo'))
{
	if(Tools::getIsset('type') && Tools::getValue('type') == 2)
	{
		$mostra_seo_id = Db::getInstance()->getValue('SELECT id_ck FROM fixed_ck WHERE id_employee = '.$cookie->id_employee.' AND name = "hidden_seo_2"');
			
		if($mostra_seo_id > 0)
			Db::getInstance()->execute('UPDATE fixed_ck SET value = "'.addslashes($_POST['hidden_seo_2']).'" WHERE id_employee = '.$cookie->id_employee.' AND name = "hidden_seo_2"');
		else
			Db::getInstance()->execute('INSERT INTO fixed_ck VALUES (NULL, '.$cookie->id_employee.', "hidden_seo_2", "'.addslashes($_POST['hidden_seo_2']).'")');
	}
	else
	{
		$mostra_seo_id = Db::getInstance()->getValue('SELECT id_ck FROM fixed_ck WHERE id_employee = '.$cookie->id_employee.' AND name = "hidden_seo"');
			
		if($mostra_seo_id > 0)
			Db::getInstance()->execute('UPDATE fixed_ck SET value = "'.addslashes($_POST['hidden_seo']).'" WHERE id_employee = '.$cookie->id_employee.' AND name = "hidden_seo"');
		else
			Db::getInstance()->execute('INSERT INTO fixed_ck VALUES (NULL, '.$cookie->id_employee.', "hidden_seo", "'.addslashes($_POST['hidden_seo']).'")');
	}	
}

if (Tools::isSubmit('submitOre'))
{
	$dati_stringa = Tools::getValue('dati_da_salvare');
	
	$dati_stringa = preg_replace('/undefined/', '0', $dati_stringa);
	
	$dati = explode(':::',$dati_stringa);
	foreach($dati as $dato_stringa)
	{
		$dato = explode('|',$dato_stringa);
		
		$id_employee = $dato[0];
		$month = $dato[1];
		$year = $dato[2];
		$exists = Db::getInstance()->getValue('SELECT count(valori_ore) FROM ore WHERE id_employee = '.$id_employee.' AND month = '.$month.' AND year = '.$year.'');
		if($exists > 0)
			Db::getInstance()->executeS('UPDATE ore SET valori_ore = "'.$dato[3].'" WHERE id_employee = '.$id_employee.' AND month = '.$month.' AND year = '.$year.'');
		else
			Db::getInstance()->executeS('INSERT INTO ore (id_employee, month, year, valori_ore) VALUES ('.$id_employee.', '.$month.', '.$year.', "'.$dato[3].'")');
		
	}
	
	
	die('ok');
}


if (Tools::isSubmit('deleteTrgt'))
{
	
	if (!Db::getInstance()->Execute('DELETE FROM trgt WHERE descrizione = "'.Tools::getValue('descrizione').'"'))
		die ('error:update');
	die('ok');
	
}

if (Tools::getIsset('deletePromemoria'))
{
	
	Db::getInstance()->Execute('DELETE FROM ticket_promemoria WHERE msg = "'.Tools::getValue('msg').'" AND tipo_att = "'.Tools::getValue('tipo_att').'" AND ora = "'.Tools::getValue('ora').'"');
		
	die('ok');
	
}

if (Tools::isSubmit('submitTicketNote') AND $id_customer_thread = (int)Tools::getValue('id_customer_thread'))
{
	$ticket_note = html_entity_decode(Tools::getValue('ticket_note'));

	if (!Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'customer_thread SET `note` = "'.pSQL($ticket_note, true).'" WHERE id_customer_thread = '.(int)$id_customer_thread.' LIMIT 1'))
		die ('error:update');
	die('ok');
}

if (Tools::isSubmit('submitPreventivoNote') AND $id_thread = (int)Tools::getValue('id_thread'))
{
	$preventivo_note = html_entity_decode(Tools::getValue('preventivo_note'));

	if (!Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'form_prevendita_thread SET `note` = "'.pSQL($preventivo_note, true).'" WHERE id_thread = '.(int)$id_thread.' LIMIT 1'))
		die ('error:update');
	die('ok');
}

if (Tools::isSubmit('submitSubject') AND $id_thread = (int)Tools::getValue('id_thread'))
{
	$subject = html_entity_decode(Tools::getValue('subject'));
	
	if(Tools::getValue('tipo') == 'ticket') {
	if (!Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'customer_thread SET `subject` = "'.pSQL($subject, true).'" WHERE id_customer_thread = '.(int)$id_thread.' LIMIT 1'))
		die ('error:update');
	}
	else if(Tools::getValue('tipo') == 'commerciale') {
	if (!Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'form_prevendita_thread SET `subject` = "'.pSQL($subject, true).'" WHERE id_thread = '.(int)$id_thread.' LIMIT 1'))
		die ('error:update');
	}
	else if(Tools::getValue('tipo') == 'azione') {
	if (!Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'action_thread SET `subject` = "'.pSQL($subject, true).'" WHERE id_action = '.(int)$id_thread.' LIMIT 1'))
		die ('error:update');
	}
	
	
	die('ok');
}

if (Tools::isSubmit('searchCustomerById'))
{
	$customer = Db::getInstance()->getRow('SELECT * FROM customer WHERE id_customer = '.Tools::getValue('id_customer'));
	
	if(!$customer)
		die('Nessun cliente trovato');
	else
	{
		if($customer['is_company'] == 1)
			die($customer['company']);
		else
			die($customer['firstname'].' '.$customer['lastname']);
	}
	
}

if (Tools::isSubmit('SEOby'))
{
	$text = $_POST['text'];
	$keyword = strtolower($_POST['keyword']);
	$title = strtolower($_POST['title']);
	$meta_description = strtolower($_POST['meta_description']);
	$friendly_url = $_POST['url'];
	$urlified_keyword = $_POST['urlified_keyword'];
	$tipo = $_POST['tipo'];
	$text_plain = strip_tags($text);
	$htmlDoc = new DomDocument();
    $htmlDoc->loadhtml($text);
	$numero_link = 0;
	$numero_link_interni = 0;
	$numero_link_esterni = 0;
	$numero_link_assoluti = 0;
	$numero_h1 = 0;
	$numero_h2 = 0;
	$numero_h2_keyword = 0;
	$numero_immagini = 0;
	$host = 'ezdirect.it';
	$numero_parole = str_word_count($text_plain);
	$conto_keyword = substr_count(strtolower($text_plain),$keyword);
	$rapporto_keyword = (($conto_keyword * 100) / $numero_parole);
	$p = $htmlDoc->getElementsByTagName('p');
	$primo_paragrafo_elemento = $p[0];
	$primo_paragrafo = $primo_paragrafo_elemento->nodeValue;
	$keyword_length = strlen($keyword);
	$titoli_immagini = array();
	$alt_immagini = array();
	
	// CERCO I LINK
	foreach($htmlDoc->getElementsByTagName('a') as $link) {
		$numero_link++; 
		$url = trim($link->getAttribute('href'));
		$pos = strpos($url,$host);
		if( $pos === false ){ // NON TROVO URL SITO
			if( (substr($url, 0, 1) == '/') ||  (substr($url, 0, 1) == '#') )
            {
				// ASSOLUTO
				$numero_link_assoluti++;
			}
			else
			{
				// ESTERNO
				$numero_link_esterni++;
			}
		}
		else {
			if( $pos < 20 ){
				// INTERNO
				$numero_link_interni++;
			}
			else
			{
				// ESTERNO
				$numero_link_esterni++;
				
			}
		}
	}
	
	// CERCO GLI H1
	foreach($htmlDoc->getElementsByTagName('h1') as $h1) {
		$numero_h1++;
		
		if (strpos(strtolower($h1->nodeValue), $keyword) !== false)
			$findH1 = true;
		else
			$findH1 = false;

	}
	
	// CERCO GLI H2
	foreach($htmlDoc->getElementsByTagName('h2') as $h2) {
		$numero_h2++;
		
		if (strpos(strtolower($h2->nodeValue), $keyword) !== false)
			$numero_h2_keyword++;
	}
	
	// CERCO ATTRIBUTI ALT E TITLE
	foreach($htmlDoc->getElementsByTagName('img') as $img) {
		$numero_immagini++;
		$titoli_immagini[] = strtolower($img->getAttribute('alt'));
		$alt_immagini[] = strtolower($img->getAttribute('title'));

	}

	// REGOLA 1: PAROLE
	if($numero_parole < 300)
		$result .= '<img src="../img/admin/status_red.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:red">Le parole devono essere almeno 300. Attualmente ce ne sono '.$numero_parole.'</span><br /><br />';
	else
		$result .= '<img src="../img/admin/status_green.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:green">Ottimo, ci sono almeno 300 parole nel testo. Attualmente ce ne sono '.$numero_parole.'</span><br /><br />';
	
	// REGOLA 2: NUMERO LINK
	/*if($numero_link == 0)
		$result .= '<img src="../img/admin/status_red.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:red">Dovresti inserire almeno un link nel testo</span><br /><br />';
	else
		$result .= '<img src="../img/admin/status_green.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:green">Bene, nel testo ho trovato almeno un link: ce ne sono in tutto '.$numero_link.'</span><br /><br />';
	*/
	// REGOLA 3: NUMERO LINK INTERNI
	/*if($numero_link_interni == 0)
		$result .= '<img src="../img/admin/status_red.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:red">Dovresti inserire almeno un link interno nel testo</span><br /><br />';
	else
		$result .= '<img src="../img/admin/status_green.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:green">Bene, nel testo ho trovato almeno un link interno : ce ne sono in tutto '.$numero_link_interni.'</span><br /><br />';
	*/
	// REGOLA 4: NUMERO LINK ESTERNI
	/*if($numero_link_esterni == 0)
		$result .= '<img src="../img/admin/status_orange.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:orange">Potresti inserire almeno un link esterno nel testo</span><br /><br />';
	else
		$result .= '<img src="../img/admin/status_green.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:green">Bene, nel testo ho trovato almeno un link esterno : ce ne sono in tutto '.$numero_link_esterni.'</span><br /><br />';
	*/
	// REGOLA 5: NUMERO LINK RELATIVI
	if($numero_link_assoluti > 0)
	{
		$result .= '<img src="../img/admin/status_orange.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:orange">Ci sono '.$numero_link_assoluti.' link relativi nel sito. Cerca di trasformarli in link assoluti.</span><br /><br />';
	}
	
	// REGOLA 6: NUMERO H1

	if($tipo == 'product')
	{
		if($numero_h1 == 0)
			$result .= '<img src="../img/admin/status_red.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:red">Manca l\'h1 nel testo: inseriscilo!</span><br /><br />';
		else if($numero_h1 == 1)
			$result .= '<img src="../img/admin/status_green.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:green">Ottimo, ho trovato l\'h1 nel testo</span><br /><br />';
		else
			$result .= '<img src="../img/admin/status_red.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:red">Ci sono troppi h1 nel testo: ne ho trovati '.$numero_h1.'. Devi metterne solo uno</span><br /><br />';
	}
	
	// REGOLA 7: NUMERO H2
	if($numero_h2 == 0)
		$result .= '<img src="../img/admin/status_red.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:red">Dovresti inserire almeno un h2 nel testo</span><br /><br />';
	else
		$result .= '<img src="../img/admin/status_green.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:green">Bene, nel testo ho trovato almeno un h2: ce ne sono in tutto '.$numero_h2.'</span><br /><br />';
	
	// REGOLA 14: NUMERO IMMAGINI
		
	if($numero_immagini == 0)
		$result .= '<img src="../img/admin/status_red.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:red">Dovresti inserire almeno un\'immagine nel testo</span><br /><br />';
	else
		$result .= '<img src="../img/admin/status_green.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:green">Bene, nel testo ho trovato almeno un\'immagine: ce ne sono in tutto '.$numero_immagini.'</span><br /><br />';
	
	if($keyword != '')
	{
		// REGOLA 8: PRESENZA KEYWORD IN H1
		if($tipo == 'product')
		{
			if($findH1)
				$result .= '<img src="../img/admin/status_green.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:green">Bene, ho trovato la parola chiave nell\'h1</span><br /><br />';
			else
				$result .= '<img src="../img/admin/status_red.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:red">La parola chiave non &egrave; presente nell\'h1</span><br /><br />';
		}
		// REGOLA 9: PRESENZA KEYWORD IN H2
		if($numero_h2_keyword > 0)
			$result .= '<img src="../img/admin/status_green.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:green">Bene, ho trovato almeno una volta la parola chiave nell\'h2. In tutto compare '.$numero_h2_keyword.' volte</span><br /><br />';
		else
			$result .= '<img src="../img/admin/status_red.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:red">La parola chiave non &egrave; presente in nessuno degli h2</span><br /><br />';
		
		// REGOLA 10: KEYWORD DENSITY
		if($rapporto_keyword > 0 && $rapporto_keyword < 0.5)
			$result .= '<img src="../img/admin/status_orange.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:orange">La parola chiave compare troppe poche volte nel testo: la ricorrenza &egrave; dello '.number_format($rapporto_keyword,2,",","").'%. Prova a inserire la parola chiave altre volte.</span><br /><br />';
		else if ($rapporto_keyword > 0.5 && $rapporto_keyword < 2.5)
			$result .= '<img src="../img/admin/status_green.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:green">Bene, la ricorrenza della parola chiave &egrave; dello '.number_format($rapporto_keyword,2,",","").'%. In tutto compare '.$conto_keyword.' volte nel testo.</span><br /><br />';
		else if($rapporto_keyword > 2.5)
			$result .= '<img src="../img/admin/status_red.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:red">La parola chiave compare troppe volte nel testo e questo potrebbe essere penalizzante: la ricorrenza &egrave; dello '.number_format($rapporto_keyword,2,",","").'%. Prova a toglierla da alcune parti.</span><br /><br />';
		else if($rapporto_keyword <= 0)
			$result .= '<img src="../img/admin/status_red.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:red">La parola chiave non compare nel testo. Devi inserirla.</span><br /><br />';
		
		/*
		if($conto_keyword >= 1)
			$result .= '<img src="../img/admin/status_green.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:green">Bene, la keyword compare almeno una volta nel testo. In tutto compare '.$conto_keyword.' volte</span><br /><br />';
		else
			$result .= '<img src="../img/admin/status_red.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:red">La parola chiave non compare almeno tre volte nel testo. In tutto compare '.$conto_keyword.' volte</span><br /><br />';
		*/
		
		// REGOLA 11: KEYWORD IN META DESCRIPTION
		if (strpos(strtolower($meta_description), $keyword) !== false)
			$result .= '<img src="../img/admin/status_green.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:green">Bene, la parola chiave compare nella meta description.</span><br /><br />';
		else
			$result .= '<img src="../img/admin/status_red.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:red">Nella meta description manca la parola chiave</span><br /><br />';
		
		// REGOLA 12: KEYWORD IN PRIMO PARAGRAFO
		if (strpos(strtolower($primo_paragrafo), $keyword) !== false)
			$result .= '<img src="../img/admin/status_green.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:green">Bene, la parola chiave compare nel primo paragrafo.</span><br /><br />';
		else
			$result .= '<img src="../img/admin/status_red.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:red">La parola chiave non &egrave; presente nel primo paragrafo</span><br /><br />';
		
		if($tipo != 'produttore')
		{
			if (strpos(strtolower($friendly_url), $urlified_keyword) !== false)
				$result .= '<img src="../img/admin/status_green.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:green">Bene, la parola chiave compare nella friendly url.</span><br /><br />';
			else
				$result .= '<img src="../img/admin/status_red.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:red">La parola chiave non compare nella friendly url</span><br /><br />';
		}
		// REGOLA 13: KEYWORD IN TITLE E SUO POSIZIONAMENTO
		if(substr($title,0,$keyword_length) == $keyword)
			$result .= '<img src="../img/admin/status_green.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:green">Bene, la parola chiave compare subito all\'inizio del title tag.</span><br /><br />';
		else
		{
			if (strpos(strtolower($title), $keyword) !== false)
			$result .= '<img src="../img/admin/status_orange.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:orange">Il title tag contiene la parola chiave, ma non all\'inizio. Potresti pensare di spostarla.</span><br /><br />';
		else
			$result .= '<img src="../img/admin/status_red.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:red">La parola chiave non &egrave; presente nel title tag</span><br /><br />';
			
		}

		if($numero_immagini > 0)
		{
			$titoli_immagini = implode('*GLUE*',$titoli_immagini);
			$alt_immagini = implode('*GLUE*',$alt_immagini);
			
			if (strpos(strtolower($titoli_immagini), $keyword) !== false)
				$result .= '<img src="../img/admin/status_green.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:green">Bene, la parola chiave compare almeno una volta nei titoli delle immagini.</span><br /><br />';
			else
				$result .= '<img src="../img/admin/status_orange.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:orange">La parola chiave non &egrave; presente nei titoli delle immagini. Potresti inserirla</span><br /><br />';
			
			if (strpos(strtolower($alt_immagini), $keyword) !== false)
				$result .= '<img src="../img/admin/status_green.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:green">Bene, la parola chiave compare almeno una volta nelle descrizioni delle immagini.</span><br /><br />';
			else
				$result .= '<img src="../img/admin/status_orange.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:orange">La parola chiave non &egrave; presente nelle descrizioni delle immagini. Potresti inserirla</span><br /><br />';
		
		}
		
	}
	else
	{
		$result .= '<img src="../img/admin/status_orange.gif" alt="" title="" />&nbsp;&nbsp;<span style="color:orange">Non hai impostato una parola chiave. Impostala per avere ulteriori indicazioni.</span><br /><br />';
	}
	die ($result);
}


if (Tools::getValue('form_language_id'))
{
	if (!($cookie->employee_form_lang = (int)(Tools::getValue('form_language_id'))))
		die ('Error while updating cookie.');
	die ('Form language updated.');
}

if (Tools::getValue('submitPublishProduct'))
{
	global $cookie;

	if (Tools::getIsset('id_product'))
	{
		$id_product = (int)(Tools::getValue('id_product'));
		$id_tab_catalog = (int)(Tab::getIdFromClassName('AdminCatalog'));
		$token = Tools::getAdminToken('AdminCatalog'.(int)($id_tab_catalog).(int)($cookie->id_employee));
		$bo_product_url = dirname($_SERVER['PHP_SELF']).'/index.php?tab=AdminCatalog&id_product='.$id_product.'&updateproduct&token='.$token;

		if (Tools::getValue('redirect'))
			die($bo_product_url);

		$profileAccess = Profile::getProfileAccess((int)$cookie->profile, $id_tab_catalog);
		if ($profileAccess['edit'])
		{
			$product = new Product((int)(Tools::getValue('id_product')));
			if (!Validate::isLoadedObject($product))
				die('error: invalid id');

			$product->active = 1;

			if ($product->save())
				die($bo_product_url);
			else
				die('error: saving');

		} else {
			die('error: permissions');
		}
	}
	else
		die ('error: parameters');
}

if (Tools::getValue('submitPublishCMS'))
{
	global $cookie;

	if (Tools::getIsset('id_cms'))
	{
		$id_cms = (int)(Tools::getValue('id_cms'));
		$id_tab_cms = (int)(Tab::getIdFromClassName('AdminCMSContent'));
		$token = Tools::getAdminToken('AdminCMSContent'.(int)($id_tab_cms).(int)($cookie->id_employee));
		$bo_cms_url = dirname($_SERVER['PHP_SELF']).'/index.php?tab=AdminCMSContent&id_cms='.(int)$id_cms.'&updatecms&token='.$token;

		if (Tools::getValue('redirect'))
			die($bo_cms_url);

		$profileAccess = Profile::getProfileAccess((int)$cookie->profile, $id_tab_cms);
		if ($profileAccess['edit'])
		{
			$cms = new CMS((int)(Tools::getValue('id_cms')));
			if (!Validate::isLoadedObject($cms))
				die('error: invalid id');

			$cms->active = 1;

			if ($cms->save())
				die($bo_cms_url);
			else
				die('error: saving');

		} else {
			die('error: permissions');
		}
	}
	else
		die ('error: parameters');
}

if (Tools::isSubmit('submitTrackClickOnHelp'))
{
	$label = Tools::getValue('label');
	$version = Tools::getValue('version');

	if (!empty($label) && !empty($version))
		HelpAccess::trackClick($label, $version);
}

if (Tools::isSubmit('saveImportMatchs'))
{
   $match = implode('|', Tools::getValue('type_value'));
   Db::getInstance()->Execute('INSERT INTO  `'._DB_PREFIX_.'import_match` (
								`id_import_match` ,
								`name` ,
								`match`,
								`skip`
								)
								VALUES (
								NULL ,
								\''.pSQL(Tools::getValue('newImportMatchs')).'\',
								\''.pSQL($match).'\',
								\''.pSQL(Tools::getValue('skip')).'\'
								)');

	die('{"id" : "'.Db::getInstance()->Insert_ID().'"}');
}

if (Tools::isSubmit('deleteImportMatchs'))
{
   Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'import_match` WHERE `id_import_match` = '.(int)Tools::getValue('idImportMatchs'));
}

if (Tools::isSubmit('loadImportMatchs'))
{
   $return = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'import_match` WHERE `id_import_match` = '.(int)Tools::getValue('idImportMatchs'));
   die('{"id" : "'.$return[0]['id_import_match'].'", "matchs" : "'.$return[0]['match'].'", "skip" : "'.$return[0]['skip'].'"}');
}

if (Tools::isSubmit('toggleScreencast'))
{
	global $cookie;

	$employee = new Employee($cookie->id_employee);
	if (Validate::isLoadedObject($employee))
	{
		$employee->bo_show_screencast = !$employee->bo_show_screencast;
		$employee->update();
	}
}

if (Tools::isSubmit('ajaxAddZipCode') OR Tools::isSubmit('ajaxRemoveZipCode'))
{
	require_once(PS_ADMIN_DIR.'/tabs/AdminCounty.php');

	$zipcodes = Tools::getValue('zipcodes');
	$id_county = (int)Tools::getValue('id_county');

	$county = new County($id_county);
	if (!Validate::isLoadedObject($county))
		die('error');

	if (Tools::isSubmit('ajaxAddZipCode'))
	{
		if ($county->isZipCodeRangePresent($zipcodes))
			die('error:'.Tools::displayError('This Zip Code is already in use.'));
		if ($county->addZipCodes($zipcodes))
			die(AdminCounty::renderZipCodeList($county->getZipCodes()));
	}
	elseif (Tools::isSubmit('ajaxRemoveZipCode') AND $county->removeZipCodes($zipcodes))
			die(AdminCounty::renderZipCodeList($county->getZipCodes()));

	die('error');
}

if (Tools::isSubmit('closethis'))
{
	switch($_POST['tipo'])
	{
		case 'T': $table = 'customer_thread'; $id = 'id_customer_thread'; break;
		case 'P': $table = 'form_prevendita_thread'; $id = 'id_thread'; break;
		case 'A': $table = 'action_thread'; $id = 'id_action'; break;
		case 'L': $table = 'bdl'; $id = 'id_bdl'; break;
		default: $table = ''; $id = ''; break;
	}
	
	if(Db::getInstance()->execute('UPDATE '.$table.' SET status = "closed" WHERE '.$id.' = '.$_POST['id']))
		die('ok');
	else
		die('error');
}	

if (Tools::isSubmit('ordModificaIndirizzoSpedizione'))
{
	global $cookie;
	$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
	$headers  = 'MIME-Version: 1.0' . "\n";
	$headers .= 'Content-Type: text/html' ."\n";
	$headers .= 'From: "Ezdirect" <info@ezdirect.it>' . "\n";
						
	if($_POST['tipo'] == 'consegna')
	{	
		Db::getInstance()->execute('UPDATE orders SET id_address_delivery = '.$_POST['id_address'].' WHERE id_order = '.$_POST['id_order']);
		
		mail("barbara.giorgini@ezdirect.it", "Modificato indirizzo consegna ordine ".$_POST['id_order']."", "Attenzione modificato indirizzo di consegna dell'ordine in oggetto. <br /><br />".$messaggio, $headers);
		mail("matteo.delgiudice@ezdirect.it", "Modificato indirizzo consegna ordine ".$_POST['id_order']."", "Attenzione modificato indirizzo di consegna dell'ordine in oggetto. <br /><br />".$messaggio, $headers);
		mail("federico.giannini@mail.com", "Modificato indirizzo consegna ordine ".$_POST['id_order']."", "Attenzione modificato indirizzo di consegna dell'ordine in oggetto. <br /><br />".$messaggio, $headers);
	}	
	else
	{
		Db::getInstance()->execute('UPDATE orders SET id_address_invoice = '.$_POST['id_address'].' WHERE id_order = '.$_POST['id_order']);
		
		mail("barbara.giorgini@ezdirect.it", "Modificato indirizzo fatturazione ordine ".$_POST['id_order']."", "Attenzione modificato indirizzo di fatturazione dell'ordine in oggetto. <br /><br />".$messaggio, $headers);
		mail("matteo.delgiudice@ezdirect.it", "Modificato indirizzo fatturazione ordine ".$_POST['id_order']."", "Attenzione modificato indirizzo di fatturazione dell'ordine in oggetto. <br /><br />".$messaggio, $headers);
		mail("federico.giannini@mail.com", "Modificato indirizzo fatturazione ordine ".$_POST['id_order']."", "Attenzione modificato indirizzo di fatturazione dell'ordine in oggetto. <br /><br />".$messaggio, $headers);
		
	}
	
	
	
	
	$addressDelivery = new Address($_POST['id_address']);
	$deliveryState = new State($addressDelivery->id_state);
	$new_address = array();
	$new_address[] = '?tab=AdminCustomers&viewcart&id_customer='.$addressDelivery->id_customer.'&viewcustomer&id_address='.$addressDelivery->id.'&token='.$tokenCustomers.'&tab-container-1=2&newaddress&id_order='.$_POST['id_order'].'&addaddress&'.($addressDelivery->fatturazione == 1 ? 'fatturazione' : 'consegna').'=1&idclientee='.$addressDelivery->id_customer.''; //cons_mod
	$new_address[] = 'http://maps.google.com/maps?f=q&hl=it&geocode=&q='.$addressDelivery->address1.' '.$addressDelivery->postcode.' '.$addressDelivery->city.($addressDelivery->id_state ? ' '.$deliveryState->name: ''); //cons_map
	if($addressDelivery->company != '')
		$new_address[] = $addressDelivery->company; 
	else 
		$new_address[] = $addressDelivery->firstname." ".$addressDelivery->lastname;
	//cons_destinatario
	
	$new_address[] = $addressDelivery->firstname." ".$addressDelivery->lastname; // cons_referente
	$new_address[] = $addressDelivery->phone; // cons_tel
	$new_address[] = $addressDelivery->phone_mobile; //cons_cell
	$new_address[] = $addressDelivery->address1; //cons_indirizzo
	$new_address[] = $addressDelivery->postcode; //cons_cap
	$new_address[] = $addressDelivery->city; //cons_citta
	$new_address[] = $deliveryState->name; // cons_provincia
	$new_address[] = $addressDelivery->country; // cons_nazione
	
	if(Tools::getValue('ind_csv') == 'y')
		Product::RecuperaCSV($_POST['id_order']);
	
	die(Tools::jsonEncode($new_address));
}

if (Tools::isSubmit('helpAccess'))
{
	$item = Tools::getValue('item');
	$isoUser = Tools::getValue('isoUser');
	$country = Tools::getValue('country');
	$version = Tools::getValue('version');

	if (isset($item) AND isset($isoUser) AND isset($country))
		die(HelpAccess::displayHelp($item, $isoUser,  $country, $version));
	die();
}

if (Tools::isSubmit('getHookableList'))
{
	/* PrestaShop demo mode */
	if (_PS_MODE_DEMO_)
		die('{"hasError" : true, "errors" : ["Live Edit : This functionnality has been disabled"]}');
	/* PrestaShop demo mode*/
		
	if (!strlen(Tools::getValue('hooks_list')))
		die('{"hasError" : true, "errors" : ["Live Edit : no module on this page"]}');
	
	$modules_list = explode(',', Tools::getValue('modules_list'));
	$hooks_list = explode(',', Tools::getValue('hooks_list'));
	$hookableList = array();
	
	
	foreach ($modules_list as $module)
	{
		$moduleInstance = Module::getInstanceByName($module);
		foreach($hooks_list as $hook_name)
		{
			if (!array_key_exists($hook_name, $hookableList))
				$hookableList[$hook_name] = array();
			if ($moduleInstance->isHookableOn($hook_name))
				array_push($hookableList[$hook_name], $module);
		}

	}
	$hookableList['hasError'] = false;
	die(Tools::jsonEncode($hookableList));
}
if (Tools::getIsset('getCartDisplay'))
{
	$cart = new Cart(Tools::getValue('id_cart'));
	$currency = new Currency($cart->id_currency);
	$customer = new Customer($cart->id_customer);
	global $cookie;
	
	$tokenCarts = Tools::getAdminToken('AdminCarts'.(int)(Tab::getIdFromClassName('AdminCarts')).(int)($cookie->id_employee));
	
	$dati_carrello = Db::getInstance()->getRow('SELECT * FROM cart WHERE id_cart = '.$cart->id);
	$id_order = Db::getInstance()->getValue('SELECT id_order FROM orders WHERE id_cart = '.$cart->id);
	$total_price_wt = $cart->getOrderTotal(false);
	
	$cartDisplay .= '<script type="text/javascript">
		$(function() {
			$(".span-reference").tooltipster({ interactive: true, contentAsHTML: true  });
			$(".img_control").tooltipster({ interactive: true, contentAsHTML: true  });
		});
		</script>	
		<table class="table3" style="margin-top:20px">
		<th class="right" style="text-align:right;" colspan="2">
		<strong>N. Carrello </strong></th>
		<th class="right" style="text-align:right;">Data</th>
		<th class="right" style="">Prev.?</th>
		<th class="right" style="">Letto?</th>
		<th class="right" style="">Conv.?</th>
		<th class="right" style="text-align:right;">N.Ord.</th>
		<th class="right" style="text-align:right;">Totale</th>
		<th class="right" style="">Oggetto</th>
		<th class="right" style="">Creato da</th>
		<th class="right" style="">In carico</th>
		<th style="text-align:right">Azioni</th>
	</tr>
	<tr><td colspan="2" style="text-align:right"><strong><a style="color:#cc0000"href="index.php?tab=AdminCustomers&id_customer='.$cart->id_customer.'&viewcart&id_cart='.$cart->id.'&viewcustomer&token='. Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee).'&tab-container-1=4">'.$cart->id.'</a></span></td>
	
	<td style="text-align:right">'.Tools::displayDate($cart->date_add, (int)($cookie->id_lang), false).'</td>
	<td style="text-align:right">'.($dati_carrello['preventivo'] == 1 ?  "<img src='https://www.ezdirect.it/img/admin/enabled.gif' alt='Si' title='Si' />" : "<img src='https://www.ezdirect.it/img/admin/red_no.gif' alt='No' title='No' />").'</td>
	<td style="text-align:right" >'.($dati_carrello['visualizzato'] == 1 ?  "<img src='https://www.ezdirect.it/img/admin/enabled.gif' alt='Si' title='Si' />" : "<img src='https://www.ezdirect.it/img/admin/red_no.gif' alt='No' title='No' />").'</td>
	<td style="text-align:right" >'.($id_ordine == 1 ?  "<img src='https://www.ezdirect.it/img/admin/enabled.gif' alt='Si' title='Si' />" : "<img src='https://www.ezdirect.it/img/admin/red_no.gif' alt='No' title='No' />").'</td>
	<td style="text-align:right">'.($id_ordine > 0 ? $id_ordine : '--').'</td>
	
	
	<td style="text-align:right">'.Tools::displayPrice($total_price_wt).'</td>
	<td style="text-align:right"><span title="'.$dati_carrello['name'].'">'.substr($dati_carrello['name'],0,15).' '.(strlen($dati_carrello['name']) > 15 ? '...' : '').'</td>
	
	<td style="text-align:right">'.($dati_carrello['created_by'] == 0 ? 'Cliente' : Db::getInstance()->getValue("SELECT CONCAT(firstname,' ',LEFT(lastname, 1),'.')  FROM employee WHERE id_employee = ".$dati_carrello['created_by']."")).'</td>
	<td style="text-align:right">'.Db::getInstance()->getValue("SELECT CONCAT(firstname,' ',LEFT(lastname, 1),'.')  FROM employee WHERE id_employee = ".$dati_carrello['in_carico_a']."").'</td>
	<td align="right">	<a href="index.php?tab=AdminCarts&id_customer='.$cart->id_customer.'&id_cart='.$cart->id.'&deleteccart&token='.$tokenCarts.'" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }"><img src="../img/admin/delete.gif" /></a></td>
	</tr>
	';
	
	$cart_products = Db::getInstance()->executeS("select cp.*,t.rate as tax_rate,p.quantity as stock,p.id_tax_rules_group,p.price as vendita,p.price as product_price,p.listino as listino,p.wholesale_price as acquisto,p.sconto_acquisto_1 as sc_acq_1, p.sconto_acquisto_2 as sc_acq_2, p.sconto_acquisto_3 as sc_acq_3, p.quantity as qt_tot, p.stock_quantity as magazzino, p.supplier_quantity as allnet, p.esprinet_quantity as esprinet, p.itancia_quantity as itancia, p.ordinato_quantity as qt_ordinato, p.impegnato_quantity as qt_impegnato, p.arrivo_quantity as qt_arrivo, p.arrivo_esprinet_quantity as qt_arrivo_esprinet, p.reference as product_reference,pl.name as product_name,i.id_image from "._DB_PREFIX_."cart_product cp left join ". _DB_PREFIX_."product p on  cp.id_product=p.id_product left join (select * from image where cover = 1) i on i.id_product = p.id_product left join ". _DB_PREFIX_."product_lang pl  on  cp.id_product=pl.id_product 
	LEFT JOIN `"._DB_PREFIX_."tax_rule` tr ON (p.`id_tax_rules_group` = tr.`id_tax_rules_group`
	AND tr.`id_country` = ".(int)Country::getDefaultCountryId()."
	AND tr.`id_state` = 0)
	LEFT JOIN `"._DB_PREFIX_."tax` t ON (t.`id_tax` = tr.`id_tax`)
	WHERE pl.id_lang = 5 
	AND id_cart=".$cart->id." GROUP BY cp.id_product ORDER BY cp.sort_order ASC");
						
	$cartDisplay .= '<tr class="invisible-table-row subs sub_cart_'.$cart->id.'">
	<th style="width:20px">Riga</th>
	<th style="width: 100px; text-align:right">Codice</th>
	<th colspan="4" style="width: 290px">Nome prodotto</th>
	<th style="width: 35px; text-align: right">Qta</th>
	<th style="width: 95px; text-align: right">Unitario</th>
	<th style="width: 115px; text-align: right">'.($cookie->profile == 7 ? '' : 'Acquisto').'</th>
	<th style="width: 65px; text-align: right">'.($cookie->profile == 7 ? '' : 'Margine').'</th>
	<th style="width: 60px; text-align: right">Magaz.</th>
	<th style="width: 65px; text-align: right">Tot. riga</th>
	</tr>';
	$i = 1;
	
	foreach($cart_products as $product)
	{
		
		$product['product_reference'] = Db::getInstance()->getValue('SELECT reference FROM product WHERE id_product = '.$product['id_product']);
		$product['product_name'] = Db::getInstance()->getValue('SELECT name FROM product_lang WHERE id_lang = 5 AND id_product = '.$product['id_product']);	
							
		$prezzo_partenza = ($product['price'] == 0 && $product['free'] == 0 && $product['sc_qta'] == 1 ? Product::trovaMigliorPrezzo($product['id_product'],$customer->id_default_group,$product['quantity']) - (Product::trovaMigliorPrezzo($product['id_product'],$customer->id_default_group,$product['quantity']) * ($product['sconto_extra']/100)) : ($product['price'] == 0 && $product['free'] == 0 && $product['sc_qta'] == 0 ? Product::trovaMigliorPrezzo($product['id_product'],1,1) - ( Product::trovaMigliorPrezzo($product['id_product'],1,1) * ($product['sconto_extra'] / 100))  : $product['price']));
					  
		if($id_order && !Tools::getIsset('vedirevisione'))
		{
								
			if($prezzo_partenza == 0)
				$prezzo_partenza = Db::getInstance()->getValue('SELECT (price - ((price*sconto_extra)/100)) FROM cart_product WHERE id_product = '.$product['id_product'].' AND id_cart = '.$cart->id);
										
			if($prezzo_partenza == 0) 
			{	
				$is_product = Db::getInstance()->getValue('SELECT product_id FROM order_detail WHERE product_id = '.$product['id_product']);
									
				if($is_product > 0)
				{
					$prezzo_partenza = number_format(Db::getInstance()->getValue('SELECT (product_price - ((product_price*reduction_percent)/100)) price FROM order_detail WHERE product_id = '.$product['id_product'].' AND id_order = '.$id_order),2,',','');
					$productprice = $prezzo_partenza;
				}	
			}
		}
		$quantity = $product['quantity'];
		
		if($product['prezzo_acquisto'] > 0)
			$wholesale_price = $product['prezzo_acquisto'];
		else
			$wholesale_price = Db::getInstance()->getValue('SELECT wholesale_price FROM product WHERE id_product = '.$product['id_product']);
		
		$nome_prodotto = $product['name'];
		
		if($nome_prodotto == '')
			$nome_prodotto = $product['product_name'];
									
		$cartDisplay .= '<tr class="invisible-table-row subs sub_cart_'.$cart->id.'" style="'.(((($prezzo_partenza - $wholesale_price)*100)/$prezzo_partenza) < 10 && $prezzo_partenza > 0 ? 'background-color:#ffcccc"' : "").'">
		<td style="text-align:right">'.$i.'</td>
		<td align="right">
		<span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($product['id_product']).'"><a href="index.php?tab=AdminCatalog&id_product='.$product['id_product'].'&updateproduct&token='.$tokenCatalog.'">'.$product['product_reference'].'</a></span></td>
		<td colspan="4"><span class="productName" title="'.$nome_prodotto.'">'.substr($nome_prodotto,0,35).' '.(strlen($nome_prodotto) > 35 ? '...' : '').'</span></td>
		<td align="right" class="productQuantity" '.($quantity > 1 && $product['customizationQuantityTotal'] > 0 ? 'style="font-weight:700;font-size:1.1em;color:red"' : '').'>'.(int)$quantity.'</td>
		<td align="right">'.Tools::displayPrice($prezzo_partenza, $currency, false).'</td>
		
		<td align="right">'.($cookie->profile == 7 ? '' : Tools::displayPrice($wholesale_price, $currency, false)).'</td>
		<td align="right">'.($cookie->profile == 7 ? '' : number_format(((($prezzo_partenza - $wholesale_price)*100)/$prezzo_partenza),2,",","").'%').'</td>
		<td align="right" class="productQuantity">'.Db::getInstance()->getValue('SELECT stock_quantity FROM product WHERE id_product = '.$product['id_product']).'</td>
		<td align="right">'.Tools::displayPrice(Tools::ps_round($prezzo_partenza, 2) * ((int)($product['quantity'])), $currency, false).'</td>
		</tr>';
		$i++;
	}	
	
	if($id_order > 0)
		$costo_spedizione = Db::getInstance()->getValue('SELECT (total_shipping / ((carrier_tax_rate/100)+1)) spedizione FROM orders WHERE id_order = '.$id_order);
	else
	{
		$provincia_cliente = Db::getInstance()->getValue("SELECT id_state FROM address WHERE id_customer = ".$customer->id." AND active = 1 AND deleted = 0 AND fatturazione = 1");
			
		$nazione_cliente = Db::getInstance()->getValue("SELECT id_country FROM address WHERE id_customer = ".$customer->id." AND active = 1 AND deleted = 0 AND fatturazione = 1");
			
		if($provincia_cliente == 0) {
			$zona_cliente = Db::getInstance()->getValue("SELECT id_zone FROM country WHERE id_country = ".$nazione_cliente."");
		}
		else {
			$zona_cliente = Db::getInstance()->getValue("SELECT id_zone FROM state WHERE id_state = ".$nazione_cliente."");
		}
		
		$gruppi_per_spedizione = array();
		$gruppi_per_spedizione[] = $customer->id_default_group;
		
		

		$spedizione_default = Db::getInstance()->getValue('SELECT d.`price` FROM `delivery` d LEFT JOIN `range_price` r ON d.`id_range_price` = r.`id_range_price` WHERE d.`id_zone` = '.$zona_cliente.' AND 1 >= r.`delimiter1` AND 156 < r.`delimiter2` AND d.`id_carrier` = '.$default_carrier.' ORDER BY r.`delimiter1` ASC');
			
			
		$costo_trasporto_modificato_r = Db::getInstance()->getValue("SELECT transport FROM cart WHERE id_cart = ".$cart->id."");
		$costo_trasporto_modificato_r = split(":",$costo_trasporto_modificato_r);
		$costo_trasporto_modificato = $costo_trasporto_modificato_r[1];
		if($carrier_cart != 0) {
			if($costo_trasporto_modificato != "" && $costo_trasporto_modificato > 0) {
				$costo_spedizione = $costo_trasporto_modificato;
			}
			else {				
				$costo_spedizione = $cart->getOrderShippingCost($carrier_cart, false);
				$costo_spedizione = $cart->getOrderShippingCostByTotalAndDelivery($carrier_cart, $total_price_wte, $cart->id_address_delivery, $customer->id_default_group, false);
			}
		}
		else {
			$costo_spedizione = $cart->getOrderShippingCostByTotalAndDelivery($default_carrier, $total_price_wt, $cart->id_address_delivery, $customer->id_default_group, false);
		}
		
	}	
	
	$cartDisplay .= '<tr class="invisible-table-row subs sub_'.$cart->id.'">
	<td style="text-align:right">'.$i.'</td>
	<td align="right">TRASP</td><td colspan="4">Trasporto</td><td align="right">1</td>
	<td align="right">'.Tools::displayPrice(($costo_trasporto_modificato == '' || $costo_trasporto_modificato == 0 ? $costo_spedizione : $costo_trasporto_modificato), $currency, false).'</td>
	<td align="right"></td><td align="right" class="productQuantity"></td><td align="right"></td>
	<td align="right">'.Tools::displayPrice(($costo_trasporto_modificato == '' || $costo_trasporto_modificato == 0 ? $costo_spedizione : $costo_trasporto_modificato), $currency, false).'</td></tr>';
	
	$cartDisplay .= '
		<tr><th colspan="11" style="text-align:right; ">Importo</th>
		<th style="width:80px; text-align:right; ">'.Tools::displayPrice($total_price_wt).'</th>
	</tr>';
	
	$cartDisplay .= '</table><br /><br />';
	die($cartDisplay);
}

if (Tools::getIsset('getOrderDisplay'))
{
	$ord = new Order(Tools::getValue('id_order'));
	$currency = new Currency($ord->id_currency);
	global $cookie;
	
	$orderDisplay .= '<table class="table3" style="margin-top:20px">
		<th class="right" style="text-align:right;" colspan="2"><strong>N. Ordine </strong></th>
		<th class="right" style="text-align:right;">Data</th>
		<th class="right" style="text-align:right;">Carrello</th>
		<th style="text-align:right;">Pagamento</th>
		
		<th class="right" style="text-align:right;" colspan="2">Stato pagamento</th>
		<th class="right" style="text-align:right;" colspan="2">Stato tecnico</th>
		<th style="text-align:right;" colspan="2">Tracking</th>
		
		<th style="text-align:right;">Importo</th>
	</tr>
	<tr><td  colspan="2" style="text-align:right"><strong><a style="color:#cc0000"href="index.php?tab=AdminCustomers&id_customer='.$ord->id_customer.'&vieworder&id_order='.$ord->id.'&viewcustomer&token='. Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee).'&tab-container-1=4">'.$ord->id.'</a></span></td>
	
	<td style="text-align:right">'.Tools::displayDate($ord->date_add, (int)($cookie->id_lang), false).'</td>
	<td style="text-align:right">'.$ord->id_cart.'</td>
	<td style="text-align:right" ><span title="'.$ord->payment.'">'.substr($ord->payment,0,11).' '.(strlen($ord->payment) > 11 ? '...' : '').'</span></td>
	<td style="text-align:right" colspan="2"><span title="'.strip_tags(Order::getTechState($ord->id, date('Y-m-d H:i:s'), 'pay')).'">'.substr(Order::getTechState($ord->id, date('Y-m-d H:i:s'), 'pay'),0,46).' '.(strlen(Order::getTechState($ord->id, date('Y-m-d H:i:s'), 'pay')) > 46 ? '...' : '').'</span></td>
	<td style="text-align:right" colspan="2"><span title="'.strip_tags(Order::getTechState($ord->id, date('Y-m-d H:i:s'), 'pay')).'">'.substr(Order::getTechState($ord->id, date('Y-m-d H:i:s'), 'tech'),0,46).' '.(strlen(Order::getTechState($ord->id, date('Y-m-d H:i:s'), 'tech')) > 46 ? '...' : '').'</span></td>
	
	<td style="text-align:right" colspan="2">'.$ord->shipping_number.'</td>
	<td align="right">'.Tools::displayPrice(($ord->total_products+($ord->total_shipping/(($ord->carrier_tax_rate/100)+1))), $currency).'</td>
	';
	
	
	$orderDisplay .= '
	<script type="text/javascript">
	$(function() {
		$(".span-reference").tooltipster({ interactive: true, contentAsHTML: true  });
		$(".img_control").tooltipster({ interactive: true, contentAsHTML: true  });
	});
	</script>	
	<tr class="invisible-table-row subs sub_'.$ord->id.'">
	<th style="width:20px">Riga</th>
	<th style="width: 80px; text-align:right">Codice</th>
	<th style="width: 280px" colspan="3">Prodotto - Servizio</th>
	<th style="width: 35px; text-align: right">Qta</th>
	<th style="width: 105px; text-align: right">Unitario</th>
	<th style="width: 105px; text-align: right">'.($cookie->profile == 7 ? '' : 'Acquisto').'</th>
	<th style="width: 55px; text-align: right">'.($cookie->profile == 7 ? '' : 'Margine').'</th>
	<th style="width: 50px; text-align: right">Magaz.</th>
	<th style="width: 50px; text-align: right">Kit?</th>
	<th style="width: 85px; text-align: right">Tot. riga</th>
	
	</tr>';
	$products = $ord->getProducts();
	$i = 1;
	foreach ($products as $k => $product)
	{
		
		$product_price = $product['product_price'];
		
		$quantity = $product['product_quantity'] - $product['customizationQuantityTotal'];
		
		if($product['wholesale_price'] > 0)
			$wholesale_price = $product['wholesale_price'];
		else
		{
			if($product['no_acq'] == 1)
				$wholesale_price = 0;
			else
				$wholesale_price = Db::getInstance()->getValue('SELECT wholesale_price FROM product WHERE id_product = '.$product['product_id']);
		}		
		$orderDisplay .= '<tr class="invisible-table-row subs sub_'.$ord->id.'" style="'.(((($product_price - $wholesale_price)*100)/$product_price) < 10 && $product_price > 0 ? ' background-color:#ffcccc"' : "").'">
			<td style="text-align:right">'.$i.'</td>
			<td align="right">
			<span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($product['product_id']).'"><a href="index.php?tab=AdminCatalog&id_product='.$product['product_id'].'&updateproduct&token='.$tokenCatalog.'">'.$product['product_reference'].'</a></span></td>
			<td colspan="3"><span class="productName" title="'.$product['product_name'].'">'.substr($product['product_name'],0,30).' '.(strlen($product['product_name']) > 30 ? '...' : '').'</span></td>
			<td align="right" class="productQuantity" '.($quantity > 1 && $product['customizationQuantityTotal'] > 0 ? 'style="font-weight:700;font-size:1.1em;color:red"' : '').'>'.(int)$quantity.'</td>
			<td align="right">'.Tools::displayPrice($product_price, $currency, false).'</td>
			
			<td align="right">'.($cookie->profile == 7 ? '' : Tools::displayPrice($wholesale_price, $currency, false)).'</td>
			<td align="right">'.($cookie->profile == 7 ? '' : number_format(((($product_price - $wholesale_price)*100)/$product_price),2,",","").'%').'</td>
			<td align="right" class="productQuantity">'.Db::getInstance()->getValue('SELECT stock_quantity FROM product WHERE id_product = '.$product['product_id']).'</td>
			<td align="right">'.Product::controllaSeFaParteDiUnBundle($ord->id, $product['product_id']).'</td>
			<td align="right">'.Tools::displayPrice(Tools::ps_round($product_price, 2) * ((int)($product['product_quantity']) - $product['customizationQuantityTotal']), $currency, false).'</td>
			</tr>';
			$i++;
	}
	$orderDisplay .=  "";
	
	$ordershipping = ((($ord->total_shipping*100)/(100+$ord->carrier_tax_rate)));
	

	$orderDisplay .= '<tr class="invisible-table-row subs sub_'.$ord->id.'">
	<td style="text-align:right">'.$i.'</td>
	<td align="right">TRASP</td><td colspan="3">Trasporto</td><td align="right">1</td>
	<td align="right">'.Tools::displayPrice($ordershipping, $currency, false).'</td><td align="right"></td>
	<td align="right"></td><td align="right" class="productQuantity"></td><td align="right"></td>
	<td align="right">'.Tools::displayPrice($ordershipping, $currency, false).'</td></tr>';
	
	$orderDisplay .= '
		<tr><th colspan="11" style="text-align:right; ">Importo</th>
		<th style="width:80px; text-align:right; ">'.Tools::displayPrice(($ord->total_products+($ord->total_shipping/(($ord->carrier_tax_rate/100)+1))), $currency).'</th>
	</tr>';
	
	
	$orderDisplay .= '
	</table><br /><br />';
	
	if(Tools::getIsset('ultime') == 'y')
	{
		global $cookie;
		$carrello = Db::getInstance()->getRow('SELECT * FROM cart WHERE id_cart = '.$ord->id_cart);
		$in_carico = $carrello['in_carico'];
		
		$ahrefcosa = '<a href="index.php?tab=AdminCustomers&id_customer='.$ord->id_customer.'&viewcart&id_cart='.$carrello['id_cart'].'&viewcustomer&token='.Tools::getAdminToken('AdminCarts'.(int)(Tab::getIdFromClassName('AdminCarts')).(int)($cookie->id_employee)).'&tab-container-1=4">'; $creato_da = $carrello['created_by']; if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
		
		if(is_numeric($in_carico)) {
			$newinc = new Employee($in_carico);
			$in_carico = $newinc->firstname." ".substr($newinc->lastname, 0,1).".";
		}
		else
		{
			$in_carico = '--';
		}
		
		$numero_notifiche =  Db::getInstance()->getValue("SELECT numero_notifiche FROM cart WHERE id_cart = ".$ultima_cosa['id']);
		$status_s = ($carrello['preventivo'] == 1 ? '<img src="../img/admin/enabled.gif" alt="Letto" title="Letto" />Letto' : '<img src="../img/admin/red_no.gif" alt="Non letto" title="Non letto" />Non letto');
		
		if($carrello['numero_notifiche'] > 0)
			$status_s .= '&nbsp;&nbsp;&nbsp;<img src="../img/admin/email-sent.gif" alt="Carrello notificato al cliente" title="Carrello notificato al cliente" />';
		
		$status = $status_s;
		
		if($carrello['preventivo'] == 0)
			$status = '--';
		
		$cartID = new Cart((int)($carrello['id_cart']));
		$importo_uc = Tools::displayPrice($cartID->getOrderTotal(false),1);
		
		$orderDisplay .= "<table class='table'>
		<tr><th style='width:100px; max-width:100px'>Tipo 
			
			</th><th style='width:70px; max-width:70px'>ID</th><th style='width:70px; max-width:70px'>Creato da</th><th  style='width:70px; max-width:70px'>In carico a</th><th style='width:30px; max-width:30px'>Conv.</th><th style='max-width:150px'>Status</th><th style='max-width:70px'>Scaduto</th><th style='max-width:100px'>Oggetto</th><th style='width:70px; max-width:70px'>Totale</th><th>Data apertura</th><th>Ultima modifica</th></tr>
		
		<tr><td style='width:100px; max-width:100px'>".($carrello['preventivo'] == 1 ? 'C.Preventivo' : 'Carrello')." 
			</td>
			<td style='width:70px; max-width:70px'>".$carrello['id_cart'].'-'.$carrello['revisioni']."</td>
			<td style='width:70px; max-width:70px'>".$creato_da."</td><td style='width:70px; max-width:70px'>".$in_carico."</td>
			
			<td style='width:30px; max-width:30px'><img src='https://www.ezdirect.it/img/admin/enabled.gif' alt='Si' title='Si' /></td>
			
			<td style='max-width:150px'>".$status."</td>
			<td style='max-width:70px'></td>
			
			<td style='max-width:100px'>".$carrello['name']."</td>
			
			<td style='width:70px; max-width:70px'>".Tools::displayPrice($importo_uc, 1)."</td>
			
			<td>".Tools::displayDate($carrello['date_add'],5)."</td>
			<td>".Tools::displayDate($carrello['date_upd'],5)."</td></tr></table>";
		
	}
	
	die($orderDisplay);

}	

if (Tools::getIsset('readMail'))
{
	$connection = mysqli_connect($_POST['database_server'],$_POST['database_user'], $_POST['database_pass']);
	mysqli_select_db($connection,$_POST['database']);
	
	$result = mysqli_query($connection,'SELECT * FROM mail WHERE id = '.$_POST['id']);
	$email = mysqli_fetch_array($result);
	$allegati_string = '';
	
	if($email['numero_allegati'] > 0)
	{
		$allegati = unserialize($email['allegati']);
		foreach ($allegati as $allegato)
		{
			$allegati_html .= '<img src="../img/admin/import.gif" alt="Allegato" title="Allegato" /> <a target="_blank" href="http://uploads:asdf6Hsrbxh!64@www.ezdirectftp.it/um/'.$email['id'].'/'.$allegato.'">'.$allegato.'</a> - ';
			
		}
		
		$allegati_string = '<tr><th>Allegati</th><td>'.$allegati_html.'</td></tr>';
		
	}
	$email_table = '<table class="table" style="width:100%">';
	$email_table .= '<tr><th style="width:100px">Da</th><td>'.htmlentities($email['da_intestazione']).' <a href="mailto:'.$email['da'].'"><img src="../img/admin/outlook.gif" alt="Invia una mail" title="Invia una mail" /></a> </td></tr>';
	$email_table .= '<tr><th>A</th><td>'.htmlentities($email['a_intestazione']).'</td></tr>';
	$email_table .= '<tr><th>Oggetto</th><td>'.$email['oggetto'].'</td></tr>';
	$email_table .= '<tr><th>Data</th><td>'.date('d/m/Y H:i:s', strtotime($email['data'])).'</td></tr>';
	$email_table .= $allegati_string;
	$email_table .= '<tr><td colspan="2"><div style="max-width:600px">'.str_replace('<style', '<style scoped ',$email['messaggio']).'</div></td></tr>';
	$email_table .= '</table><script type="text/javascript" src="https://www.ezdirect.it/js/jquery/scoper.min.js"></script>';
		
	$mailHTML = '';
	$mailHTML .= utf8_encode('<br /><br />'.$email_table);
	die($mailHTML);
	
}	
if (Tools::isSubmit('getHookableModuleList'))
{
	/* PrestaShop demo mode */
	if (_PS_MODE_DEMO_)
		die('{"hasError" : true, "errors" : ["Live Edit : This functionnality has been disabled"]}');
	/* PrestaShop demo mode*/
	
	include('../init.php');
	$hook_name = Tools::getValue('hook');
	$hookableModulesList = array();
	$modules = Db::getInstance()->ExecuteS('SELECT id_module, name FROM `'._DB_PREFIX_.'module` ');
	foreach ($modules as $module)
	{
		if (file_exists(_PS_MODULE_DIR_.$module['name'].'/'.$module['name'].'.php'))
		{
			include_once(_PS_MODULE_DIR_.$module['name'].'/'.$module['name'].'.php');
			$mod = new $module['name']();
			if ($mod->isHookableOn($hook_name))
				$hookableModulesList[] = array('id' => (int)$mod->id, 'name' => $mod->displayName, 'display' => Module::hookExec($hook_name, array(), (int)$mod->id));
		}
	}
	die(Tools::jsonEncode($hookableModulesList));
}

	

if (Tools::isSubmit('saveHook'))
{
	/* PrestaShop demo mode */
	if (_PS_MODE_DEMO_)
		die('{"hasError" : true, "errors" : ["Live Edit : This functionnality has been disabled"]}');
	/* PrestaShop demo mode*/
	
	$hooks_list = explode(',', Tools::getValue('hooks_list'));
	foreach ($hooks_list as $hook)
	{
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'hook_module` WHERE `id_hook` = (SELECT id_hook FROM `'._DB_PREFIX_.'hook` WHERE `name` = \''.pSQL($hook).'\' LIMIT 0, 1)');
		$hookedModules = explode(',', Tools::getValue($hook));
		$i = 1;
		$value = '';
		$ids = array();
		foreach($hookedModules as $module)
		{
			$id = explode('_', $module);
			if (!in_array($id[1], $ids))
			{
				$ids[] = $id[1];
				$value .= '('.(int)$id[1].', (SELECT id_hook FROM `'._DB_PREFIX_.'hook` WHERE `name` = \''.pSQL($hook).'\' LIMIT 0, 1), '.(int)$i.'),';
			}
			$i ++;
		}
		$value = rtrim($value, ',');
		Db::getInstance()->Execute('INSERT INTO  `'._DB_PREFIX_.'hook_module` (`id_module`, `id_hook`, `position`) VALUES '.$value);

	}
	die('{"hasError" : false, "errors" : ""}');
}

if (Tools::isSubmit('getAdminHomeElement'))
{
	$result = array();

	$protocol = Tools::usingSecureMode() ? 'https' : 'http';
	$isoUser = Language::getIsoById(intval($cookie->id_lang));
	$isoCountry = Country::getIsoById(Configuration::get('PS_COUNTRY_DEFAULT'));
	$stream_context = @stream_context_create(array('http' => array('method'=> 'GET', 'timeout' => 5)));

	// SCREENCAST
	if (@fsockopen('www.prestashop.com', 80, $errno, $errst, 3))
		$result['screencast'] = 'OK';
	else
		$result['screencast'] = 'NOK';

	// PREACTIVATION
	$content = @file_get_contents($protocol.'://www.prestashop.com/partner/preactivation/preactivation-block.php?version=1.0&shop='.urlencode(Configuration::get('PS_SHOP_NAME')).'&protocol='.$protocol.'&url='.urlencode($_SERVER['HTTP_HOST']).'&iso_country='.$isoCountry.'&iso_lang='.Tools::strtolower($isoUser).'&id_lang='.(int)$cookie->id_lang.'&email='.urlencode(Configuration::get('PS_SHOP_EMAIL')).'&date_creation='._PS_CREATION_DATE_.'&v='._PS_VERSION_.'&security='.md5(Configuration::get('PS_SHOP_EMAIL')._COOKIE_IV_), false, $stream_context);
	if (!$content)
		$result['partner_preactivation'] = 'NOK';
	else
	{
		$content = explode('|', $content);
		if ($content[0] == 'OK' && Validate::isCleanHtml($content[2]) && Validate::isCleanHtml($content[1]))
		{
			$result['partner_preactivation'] = $content[2];
			$content[1] = explode('#%#', $content[1]);
			foreach ($content[1] as $partnerPopUp)
				if ($partnerPopUp)
				{
					$partnerPopUp = explode('%%', $partnerPopUp);
					if (!Configuration::get('PS_PREACTIVATION_'.strtoupper($partnerPopUp[0])))
					{
						$result['partner_preactivation'] .= $partnerPopUp[1];
						Configuration::updateValue('PS_PREACTIVATION_'.strtoupper($partnerPopUp[0]), 'TRUE');
					}
				}
		}
		else
			$result['partner_preactivation'] = 'NOK';
	}

	// PREACTIVATION PAYPAL WARNING
	$content = @file_get_contents('https://www.prestashop.com/partner/preactivation/preactivation-warnings.php?version=1.0&partner=paypal&iso_country='.Tools::strtolower(Country::getIsoById(Configuration::get('PS_COUNTRY_DEFAULT'))).'&iso_lang='.Tools::strtolower(Language::getIsoById(intval($cookie->id_lang))).'&id_lang='.(int)$cookie->id_lang.'&email='.urlencode(Configuration::get('PS_SHOP_EMAIL')).'&security='.md5(Configuration::get('PS_SHOP_EMAIL')._COOKIE_IV_), false, $stream_context);
	$content = explode('|', $content);
	if ($content[0] == 'OK' && Validate::isCleanHtml($content[1]))
		Configuration::updateValue('PS_PREACTIVATION_PAYPAL_WARNING', $content[1]);
	else
		Configuration::updateValue('PS_PREACTIVATION_PAYPAL_WARNING', '');

	// DISCOVER PRESTASHOP
	$content = @file_get_contents($protocol.'://www.prestashop.com/partner/prestashop/prestashop-link.php?iso_country='.$isoCountry.'&iso_lang='.Tools::strtolower($isoUser).'&id_lang='.(int)$cookie->id_lang, false, $stream_context);
	if (!$content)
		$result['discover_prestashop'] = 'NOK';
	else
	{
		$content = explode('|', $content);
		if ($content[0] == 'OK' && Validate::isCleanHtml($content[1]))
			$result['discover_prestashop'] = $content[1];
		else
			$result['discover_prestashop'] = 'NOK';

		if (@fsockopen('www.prestashop.com', 80, $errno, $errst, 3))
			$result['discover_prestashop'] .= '<iframe frameborder="no" style="margin: 0px; padding: 0px; width: 315px; height: 290px;" src="'.$protocol.'://www.prestashop.com/rss/news2.php?v='._PS_VERSION_.'&lang='.$isoUser.'"></iframe>';

		$content = @file_get_contents($protocol.'://www.prestashop.com/partner/paypal/paypal-tips.php?protocol='.$protocol.'&iso_country='.$isoCountry.'&iso_lang='.Tools::strtolower($isoUser).'&id_lang='.(int)$cookie->id_lang, false, $stream_context);
		$content = explode('|', $content);
		if ($content[0] == 'OK' && Validate::isCleanHtml($content[1]))
			$result['discover_prestashop'] .= $content[1];
	}

	die(Tools::jsonEncode($result));
}

if (Tools::isSubmit('getChildrenCategories') && Tools::getValue('id_category_parent'))
{
	$children_categories = Category::getChildrenWithNbSelectedSubCat(Tools::getValue('id_category_parent'), Tools::getValue('selectedCat'), $cookie->id_lang);
	die(Tools::jsonEncode($children_categories));
}

function createColumnsArray($end_column, $first_letters = '')
{
  $columns = array();
  $length = strlen($end_column);
  $letters = range('A', 'Z');

  // Iterate over 26 letters.
  foreach ($letters as $letter) {
      // Paste the $first_letters before the next.
      $column = $first_letters . $letter;

      // Add the column to the final array.
      $columns[] = $column;

      // If it was the end column that was added, return the columns.
      if ($column == $end_column)
          return $columns;
  }

  // Add the column children.
  foreach ($columns as $column) {
      // Don't itterate if the $end_column was already set in a previous itteration.
      // Stop iterating if you've reached the maximum character length.
      if (!in_array($end_column, $columns) && strlen($column) < $length) {
          $new_columns = createColumnsArray($end_column, $column);
          // Merge the new columns which were created with the final columns array.
          $columns = array_merge($columns, $new_columns);
      }
  }

  return $columns;
}

if(Tools::isSubmit('promemoria_postit'))
	{
		global $cookie;
		if($cookie->id_employee == '' || $cookie->id_employee == 0 || !$cookie->id_employee)
			return '';
		
		$postit = "";
		$azioni = Db::getInstance()->executeS("SELECT a.id_action, a.action_type, a.action_date, tp.ora, tp.tipo_att, c.* FROM `action_thread` a JOIN customer c ON a.id_customer = c.id_customer LEFT JOIN ticket_promemoria tp ON a.id_action = tp.msg WHERE (tp.tipo_att = 'a' AND a.status != 'closed' AND tp.status = 'open' AND a.action_to = ".$cookie->id_employee." AND DATE_FORMAT(tp.ora, '%Y-%m-%d %H:%i') < DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i')) OR (a.status != 'closed' AND a.action_to = ".$cookie->id_employee." AND a.promemoria = 0 AND DATE_FORMAT(a.action_date, '%Y-%m-%d %H:%i') - interval 30 minute < DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i')) OR (a.status != 'closed' AND a.action_to = ".$cookie->id_employee." AND a.promemoria = 2 AND DATE_FORMAT(a.action_date, '%Y-%m-%d %H:%i') - interval 15 minute < DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i'))");
		
		if(count($azioni) > 0)
		{
			$postit = "<input type='hidden' name='postitok' value='1' />";
			foreach($azioni as $azione)
			{	
			
				if($azione['tipo_att'] != '' && $azione['promemoria'] != 0)
				{
					Db::getInstance()->execute('UPDATE ticket_promemoria SET status = "pending" WHERE ora = "'.$azione['ora'].'" AND msg = "'.$azione['id_action'].'" AND tipo_att = "'.$azione['tipo_att'].'" AND status = "open"');
				}
				else
					Db::getInstance()->execute('UPDATE action_thread SET promemoria = 1 WHERE id_action = '.$azione['id_action']);
				
				if($azione['is_company'] == 1)
					$cliente = $azione['company'];
				else
					$cliente = $azione['firstname'].' '.$azione['lastname'];
				
				switch($azione['action_type']) {
						
					case 'Assistenza tecnica postvendita': $icona = '<img src="http://www.ezdirect.it/img/admin/icons/assistenza.gif" alt="'.$azione['action_type'].'" title="'.$azione['action_type'].'" />'; break;
					case 'Assistenza': $icona =  '<img src="http://www.ezdirect.it/img/admin/icons/assistenza.gif" alt="'.$azione['action_type'].'" title="'.$azione['action_type'].'" />'; break;
					case 'Contabilita': $icona =  '<img src="http://www.ezdirect.it/img/admin/icons/contabilita.gif" alt="'.$azione['action_type'].'" title="'.$azione['action_type'].'" />'; break;
					case 'Messaggi': $icona =  '<img src="http://www.ezdirect.it/img/admin/icons/messaggio.gif" alt="'.$azione['action_type'].'" title="'.$azione['action_type'].'" />'; break;
					case 'Ordini': $icona =  '<img src="http://www.ezdirect.it/img/admin/icons/ordini.gif" alt="'.$azione['action_type'].'" title="'.$azione['action_type'].'" />'; break;
					case 'Rivenditori': $icona =  '<img src="http://www.ezdirect.it/img/admin/icons/rivenditori.gif" alt="'.$azione['action_type'].'" title="'.$azione['action_type'].'" />'; break;
					case 'RMA': $icona =  '<img src="http://www.ezdirect.it/img/admin/icons/rma.gif" alt="'.$azione['action_type'].'" title="'.$azione['action_type'].'" />'; break;
					case 'Telefonata': $icona =  '<img src="http://www.ezdirect.it/img/admin/icons/telefonata.gif" alt="'.$azione['action_type'].'" title="'.$azione['action_type'].'" />'; break;
					case 'TODO Amazon': $icona =  '<img src="https://www.ezdirect.it/img/tmp/order_state_mini_22.gif" alt="'.$azione['action_type'].'" title="'.$azione['action_type'].'" />'; break;
					case 'TODO Social': $icona =  '<img src="http://www.ezdirect.it/img/admin/icons/face-todo.gif" alt="'.$azione['action_type'].'" title="'.$azione['action_type'].'" />'; break;
					case 'TODO Sendinblue': $icona =  '<img src="http://www.ezdirect.it/img/admin/icons/Sendinblue-logo.gif" alt="'.$azione['action_type'].'" title="'.$azione['action_type'].'" />'; break;
					case 'Attivita': $icona =  '<img src="http://www.ezdirect.it/img/admin/icons/attivita.gif" alt="'.$azione['action_type'].'" title="'.$azione['action_type'].'" />'; break;
					case 'Caso': $icona =  '<img src="http://www.ezdirect.it/img/admin/icons/caso.gif" alt="'.$azione['action_type'].'" title="'.$azione['action_type'].'" />'; break;
					case 'Intervento': $icona =  '<img src="http://www.ezdirect.it/img/admin/icons/intervento.gif" alt="'.$azione['action_type'].'" title="'.$azione['action_type'].'" />'; break;
					case 'preventivo': $icona =  '<img src="http://www.ezdirect.it/img/admin/icons/commerciale.gif" alt="'.$azione['action_type'].'" title="'.$azione['action_type'].'" />'; break;
					case 'Commerciale': $icona =  '<img src="http://www.ezdirect.it/img/admin/icons/commerciale.gif" alt="'.$azione['action_type'].'" title="'.$azione['action_type'].'" />'; break;
					case 'Richiesta_RMA': $icona =  '<img src="http://www.ezdirect.it/img/admin/icons/richiesta_rma.gif" alt="'.$azione['action_type'].'" title="'.$azione['action_type'].'" />'; break;
					case 'tirichiamiamonoi': $icona =  '<img src="http://www.ezdirect.it/img/admin/icons/tirichiamiamonoi.gif" alt="'.$azione['action_type'].'" title="'.$azione['action_type'].'" />'; break;
					default: $icona =  $azione['action_type']; break;
				
				}
				
				$minutes = round(abs(strtotime(date('Y-m-d H:i:s')) - strtotime($azione['action_date'])) / 60);
				
				$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
				$posizione_left = rand(0,1000);
				$posizione_top = rand(0,500);
				$postit .= '<div class="note_postit yellow" style="z-index:999999"><a href="index.php?tab=AdminCustomers&id_customer='.$azione['id_customer'].'&viewcustomer&token='.$tokenCustomers.'&id_action='.$azione['id_action'].'&close_todo">'.$cliente.'</a><br />Hai in scadenza un TODO<br /><br />Tipo: '.$azione['action_type'].' '.$icona.'<br />ID: TODO'.$azione['id_action'].'<br />Scadenza: '.Tools::displayDate($azione['action_date'], $cookie->id_lang, true).'<br />'.($azione['tipo_att'] != '' ? '<a href="index.php?tab=AdminCustomers&id_customer='.$azione['id_customer'].'&viewcustomer&viewaction&id_action='.$azione['id_action'].'&token='.$tokenCustomers.'&tab-container-1=10&close_todo">Clicca qui per aprire il todo</a><br /><br /><div style="text-align:left"><img src="../img/admin/time.gif" alt="Posticipa" title="Posticipa" />Posticipa: <a class="posticipa_link" href="javascript:void(0)" onclick="posticipa_todo('.$azione['id_action'].', \''.$azione['tipo_att'].'\', \''.$azione['ora'].'\', 15, $(this))">15 min</a> | <a class="posticipa_link" href="javascript:void(0)" onclick="posticipa_todo('.$azione['id_action'].', \''.$azione['tipo_att'].'\', \''.$azione['ora'].'\', 30, $(this))">30 min</a> | <a class="posticipa_link" href="javascript:void(0)" onclick="posticipa_todo('.$azione['id_action'].', \''.$azione['tipo_att'].'\', \''.$azione['ora'].'\', 60, $(this))">1 ora</a> | <a class="posticipa_link" href="javascript:void(0)" onclick="posticipa_todo('.$azione['id_action'].', \''.$azione['tipo_att'].'\', \''.$azione['ora'].'\', 180, $(this))">3 ore</a> <br/><a href="javascript:void(0)" class="postit_trash" id="'.$azione['id_action'].':::'.$azione['tipo_att'].':::'.$azione['ora'].'"><img src="../img/admin/disabled.gif" alt="Chiudi" title="Chiudi" /> Chiudi</a>' : '<div style="text-align:left"><a href="index.php?tab=AdminCustomers&id_customer='.$azione['id_customer'].'&viewcustomer&viewaction&id_action='.$azione['id_action'].'&token='.$tokenCustomers.'&tab-container-1=10&close_todo">Clicca qui per aprire il todo</a>'.($minutes > 15 ? '<br /><br /><img src="../img/admin/time.gif" alt="Posticipa" title="Posticipa" /><a class="posticipa_link" href="javascript:void(0)" onclick="posticipa_todo('.$azione['id_action'].', \'PROMEMORIA\', \''.$azione['ora'].'\', 15, $(this))">Avvisami 15 minuti prima della scadenza</a><br /><br /><a href="javascript:void(0)" class="postit_trash" id="PROMEMORIA"><img src="../img/admin/disabled.gif" alt="Chiudi" title="Chiudi" /> Chiudi</a>' : '').'<br /><br /><a href="javascript:void(0)" class="postit_trash" id="PROMEMORIA"><img src="../img/admin/disabled.gif" alt="Chiudi" title="Chiudi" /> Chiudi</a>').'</div></div><script type="text/javascript">$(".note_postit").css({position:"absolute", top: document.documentElement.scrollTop + "px", left:"70%"});</script></div>';
			}
			
			$postit .= '<script type="text/javascript" src="'._PS_JS_DIR_.'jquery/post-it.js"></script>';
		}
		else
			$postit = "";
		
		die($postit);
	}
	
if(Tools::getIsset('chiudi_postit') && Tools::getValue('chiudi_postit') == 'y') {
	if(Tools::getValue('id') == 'PROMEMORIA')
	{
		
	}
	else
	{
		$data = explode(':::',Tools::getValue('id'));
		Db::getInstance()->executeS("UPDATE ticket_promemoria SET status = 'closed' WHERE msg = ".$data[0]." AND tipo_att = '".$data[1]."' AND ora = '".$data[2]."'");
	}	
}

if(Tools::getIsset('posticipa_todo') && Tools::getValue('posticipa_todo') == 'y') {
	
	if($_POST['tipo_att'] == 'PROMEMORIA')
	{
		Db::getInstance()->executeS('UPDATE action_thread SET promemoria = 2 WHERE id_action = '.$_POST['id_action']);
	}
	else
	{	
		$ora = Db::getInstance()->getValue("SELECT ora FROM ticket_promemoria WHERE msg = ".$_POST['id_action']." AND tipo_att = '".$_POST['tipo_att']."'");
		if($ora < date('Y-m-d H:i:s'))
			$ora = "'".date('Y-m-d H:i:s')."'";
		else
			$ora = 'ora';
		Db::getInstance()->executeS("UPDATE ticket_promemoria SET status = 'open', ora = ".$ora." + INTERVAL ".$_POST['tipo']." MINUTE WHERE msg = ".$_POST['id_action']." AND tipo_att = '".$_POST['tipo_att']."' AND ora = '".$_POST['ora']."'");
	}	
}

if(Tools::getIsset('riapri_postit') && Tools::getValue('riapri_postit') == 'y') {
	global $cookie;
	$azioni = Db::getInstance()->executeS("SELECT a.id_action, a.action_type, a.action_date, tp.ora, tp.tipo_att, c.* FROM `action_thread` a JOIN customer c ON a.id_customer = c.id_customer JOIN ticket_promemoria tp ON a.id_action = tp.msg WHERE tp.status = 'pending' AND a.action_to = ".$cookie->id_employee."");
		if(count($azioni) > 0)
		{
			foreach($azioni as $azione)
			{	
				
				Db::getInstance()->executeS("UPDATE ticket_promemoria SET status = 'open' WHERE status = 'pending' AND msg = '".$azione['id_action']."' AND ora = '".$azione['ora']."'");
				
			}
		}
}

if(Tools::getIsset('changeStatusFromListing') && Tools::getValue('changeStatusFromListing') == 'y') {
	$type = Tools::getValue('type');
	global $cookie;
	
	if($type == 'ticket')
	{	
		Db::getInstance()->executeS("UPDATE customer_thread SET status = '".Tools::getValue('status')."' WHERE id_customer_thread = '".Tools::getValue('id_thread')."'");
		$sigla = 'T';
	}
	else if($type == 'to-do')
	{
		
		
		$subject = Db::getInstance()->getValue('SELECT subject FROM action_thread WHERE id_action = '.Tools::getValue('id_thread'));
		$current_action_status = Db::getInstance()->getValue('SELECT status FROM action_thread WHERE id_action = '.Tools::getValue('id_thread'));
		
		if(substr($subject,0,20) == '*** VERIFICA TECNICA')
		{	
			if(Tools::getValue('status') == 'closed'  && $current_action_status != 'closed')
			{
				Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.substr($subject, -6,5).', 25, "'.date('Y-m-d H:i:s').'")');
				
			}
			else if((Tools::getValue('status') == 'open' || Tools::getValue('status') == 'pending1')  && $current_action_status == 'closed')
			{
				Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.substr($subject, -6,5).', 24, "'.date('Y-m-d H:i:s').'")');
				
			}
			
		}
		Db::getInstance()->executeS("UPDATE action_thread SET status = '".Tools::getValue('status')."' WHERE id_action = '".Tools::getValue('id_thread')."'");		
		$sigla = 'A';
	}
	else if($type == 'to-do-employee')
	{
		Db::getInstance()->executeS("UPDATE action_thread_employee SET status = '".Tools::getValue('status')."' WHERE id_action = '".Tools::getValue('id_thread')."'");
		$sigla = 'AE';
	}
	else
	{	
		Db::getInstance()->executeS("UPDATE form_prevendita_thread SET status = '".Tools::getValue('status')."' WHERE id_thread = '".Tools::getValue('id_thread')."'");
		$sigla = 'P';
	}	
	
	$color = 'red';
	
	
	switch(Tools::getValue('status')) 
	{
		case 'open': $color = 'red'; Customer::Storico(Tools::getValue('id_thread'), $sigla, $cookie->id_employee, 3); break;
		case 'closed': $color = 'green'; Customer::Storico(Tools::getValue('id_thread'), $sigla, $cookie->id_employee, 5); break;
		default: $color = 'orange'; Customer::Storico(Tools::getValue('id_thread'), $sigla, $cookie->id_employee, 4); break;
	}
	die($color);
}

if(Tools::getIsset('changeInCaricoFromListing') && Tools::getValue('changeInCaricoFromListing') == 'y') {
	$type = Tools::getValue('type');
	global $cookie;
	
	if($type == 'ticket')
	{	
		Db::getInstance()->executeS("UPDATE customer_thread SET id_employee = '".Tools::getValue('in_carico')."' WHERE id_customer_thread = '".Tools::getValue('id_thread')."'");
		$sigla = 'T';
	}
	else if($type == 'to-do')
	{
		Db::getInstance()->executeS("UPDATE action_thread SET action_to = '".Tools::getValue('in_carico')."' WHERE id_action = '".Tools::getValue('id_thread')."'");
		$sigla = 'A';
	}
	else if($type == 'to-do-employee')
	{
		Db::getInstance()->executeS("UPDATE action_thread_employee SET action_to = '".Tools::getValue('in_carico')."' WHERE id_action = '".Tools::getValue('id_thread')."'");
		$sigla = 'AE';
	}
	else
	{	
		Db::getInstance()->executeS("UPDATE form_prevendita_thread SET id_employee = '".Tools::getValue('in_carico')."' WHERE id_thread = '".Tools::getValue('id_thread')."'");
		$sigla = 'P';
	}	
	
	Customer::Storico(Tools::getValue('id_thread'), $sigla, $cookie->id_employee, 2, Tools::getValue('in_carico')); 
	
	die('ok');
}

if (Tools::isSubmit('delcons'))
{
	$delcons = Tools::getValue('delcons');
	$cons = Db::getInstance()->getValue('SELECT cons FROM order_detail  WHERE product_id = '.$_POST['id_product'].' AND id_order = '.$_POST['id_order']);
	
	$cons_vecchio = $cons;
	$cons_vecchio = str_replace('0_','',$cons_vecchio);
	
	$cons = "0_".Tools::getValue('qtforcons');
	
	Db::getInstance()->executeS('UPDATE order_detail SET cons = "'.$cons.'" WHERE product_id = '.$_POST['id_product'].' AND id_order = '.$_POST['id_order']);
	
	$modalita_esolver = Db::getInstance()->getValue('SELECT value FROM configuration WHERE name = "MODALITA_ESOLVER"');
	
	if($modalita_esolver == 0)
	{
		$stock = Db::getInstance()->getRow('SELECT quantity, stock_quantity, itancia_quantity, esprinet_quantity, techdata_quantity, intracom_quantity, supplier_quantity FROM product WHERE id_product = '.$_POST['id_product']);
		
		$stock_quantity = $stock['stock_quantity'] + $cons_vecchio - Tools::getValue('qtforcons');
		
		$qtsito = ($stock['supplier_quantity'] + $stock_quantity + $stock['esprinet_quantity'] + $stock['itancia_quantity'] + $stock['techdata_quantity'] + $stock['intracom_quantity']);
								
		Db::getInstance()->executeS("UPDATE product SET stock_quantity = $stock_quantity, quantity = $qtsito WHERE id_product = ".$_POST['id_product']."");
								
	}
	
	die('ok');
}

if (Tools::isSubmit('salvaRicezioneContratto'))
{
	$id_employee = Tools::getValue('id_employee');
	$check_contratti = Tools::getValue('contratti_check');
	$id_cart = Tools::getValue('id_cart');
	
	if($check_contratti == '1')
	{
		$array_check_contratti = array('check' => '1', 'id_employee' => $id_employee, 'data' => date('d/m/Y H:i:s'));
		$array_check_contratti = serialize($array_check_contratti);
		$resp = 'Ricezione contratto confermata da '.Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$id_employee).' in data '.date('d/m/Y H:i:s');
		Db::getInstance()->executeS('UPDATE cart_ezcloud SET check_contratti = "'.addslashes($array_check_contratti).'" WHERE id_cart = '.$id_cart);
	}
	else
	{
		$resp = "<strong>Confermare la ricezione dell'allegato firmato dal cliente</strong>";
		Db::getInstance()->executeS('UPDATE cart_ezcloud SET check_contratti = "" WHERE id_cart = '.$id_cart);
	}
	die($resp);
}

if (Tools::getIsset('changeDDT'))
{
	$ddt = Tools::getValue('changeDDT');
	
	Db::getInstance()->executeS('UPDATE order_detail SET ddt = "'.$ddt.'" WHERE product_id = '.$_POST['id_product'].' AND id_order = '.$_POST['id_order']);
	die('ok');
}

if (Tools::isSubmit('updSpringOrder'))
{
	global $cookie;
	$updSpringOrder = Tools::getValue('updSpringOrder');
	$spring = Db::getInstance()->getValue('SELECT spring FROM orders  WHERE id_order = '.$_POST['id_order']);
	if($updSpringOrder == 'yes')
	{
		Db::getInstance()->executeS('UPDATE action_thread SET status = "closed" WHERE subject LIKE "%*** ORDINE N. '.$_POST['id_order'].' NON VERIFICATO ***%"');
		
		$spring = 1;
		$verificato_da = $cookie->id_employee;
	}
	else
	{
		Db::getInstance()->executeS('UPDATE action_thread SET status = "open" WHERE subject LIKE "%*** ORDINE N. '.$_POST['id_order'].' NON VERIFICATO ***%"');
		$spring = 0;
		$verificato_da = '';
	}
	Db::getInstance()->executeS('UPDATE orders SET spring = "'.$spring.'", verificato_da = "'.$verificato_da.'" WHERE id_order = '.$_POST['id_order']);
	die('ok');
}

if (Tools::isSubmit('updControlloPagamento'))
{
	global $cookie;
	$tipo = $_POST['tipo'];
	$id_order = $_POST['id_order'];
	$updControlloPagamento = Tools::getValue('updControlloPagamento');
	
	$exists = Db::getInstance()->getValue('SELECT id_order FROM orders_controllo_differiti WHERE id_order = '.$id_order);

	if(!$exists || $exists <= 0)
	{
		if($id_order != 0)
			Db::getInstance()->executeS('INSERT INTO orders_controllo_differiti (id_order) VALUES ('.$id_order.')');
	}
	
	if($updControlloPagamento == 'yes')
	{
		$upd = 1;
	}
	else
	{
		$upd = 0;
	}
	
	if($tipo == 'altro')
		Db::getInstance()->executeS('UPDATE orders_controllo_differiti SET altro = "'.addslashes($_POST['altro']).'" WHERE id_order = '.$id_order);

	else
		Db::getInstance()->executeS('UPDATE orders_controllo_differiti SET '.$tipo.' = '.$upd.' WHERE id_order = '.$id_order);

	die('ok');
}

if (Tools::isSubmit('updCheckPI'))
{
	$updCheckPI = Tools::getValue('updCheckPI');
	$spring = Db::getInstance()->getValue('SELECT check_pi FROM orders  WHERE id_order = '.$_POST['id_order']);
	if($updCheckPI == 'yes')
		$spring = 1;
	else
		$spring = 0;
	Db::getInstance()->executeS('UPDATE orders SET check_pi = "'.$spring.'" WHERE id_order = '.$_POST['id_order']);
	die('ok');
}

if (Tools::isSubmit('updPagAmzRic'))
{
	$updPagAmzRic = Tools::getValue('updPagAmzRic');
	$spring = Db::getInstance()->getValue('SELECT pag_amz_ric FROM orders  WHERE id_order = '.$_POST['id_order']);
	if($updPagAmzRic == 'yes')
		$spring = 1;
	else
		$spring = 0;
	Db::getInstance()->executeS('UPDATE orders SET pag_amz_ric = "'.$spring.'" WHERE id_order = '.$_POST['id_order']);
	die('ok');
}

if (Tools::isSubmit('updCheckEsolv'))
{
	$updCheckPI = Tools::getValue('updCheckEsolv');
	$spring = Db::getInstance()->getValue('SELECT check_esolver FROM orders  WHERE id_order = '.$_POST['id_order']);
	if($updCheckPI == 'yes')
		$spring = 1;
	else
		$spring = 0;
	Db::getInstance()->executeS('UPDATE orders SET check_esolver = "'.$spring.'" WHERE id_order = '.$_POST['id_order']);
	die('ok');
}

if (Tools::isSubmit('updAmazonPayment'))
{
	$updAmazonPayment = Tools::getValue('updAmazonPayment');
	$verificato = Db::getInstance()->getValue('SELECT verificato FROM amazon_verifica_pagamenti WHERE id_pagamento = '.$_POST['id_pagamento']);
	if($updAmazonPayment == 'yes')
		$verificato = 1;
	else
		$verificato = 0;
	Db::getInstance()->executeS('REPLACE INTO amazon_verifica_pagamenti (id_pagamento, verificato, data_verifica) VALUES ("'.$_POST['id_pagamento'].'", "'.$verificato.'", "'.date('Y-m-d H:i:s').'")');
	die('ok');
}

if (Tools::isSubmit('updVerificatoOrder'))
{
	global $cookie;
	$updVerificatoOrder = Tools::getValue('updVerificatoOrder');
	$spring = Db::getInstance()->getValue('SELECT verificato FROM orders  WHERE id_order = '.$_POST['id_order']);
	
	if($updVerificatoOrder == 'y')
	{
		$verificato = 1;
		$verificato_da = $cookie->id_employee;
	}
	else
	{
		$verificato = 0;
		$verificato_da = '';
	}
	
		
	Db::getInstance()->executeS('UPDATE orders SET verificato = "'.$verificato.'", verificato_da = "'.$verificato_da.'" WHERE id_order = '.$_POST['id_order']);
	die('ok');
}

if(Tools::getIsset('checkPI') && Tools::getValue('checkPI') == 'y') {
	$pi = Db::getInstance()->getValue('SELECT id_customer FROM customer WHERE vat_number = "'.$_GET['pi'].'"');
	if($pi > 0)
		echo 'yes';
}

if(Tools::getIsset('checkCF') && Tools::getValue('checkCF') == 'y') {
	$pi = Db::getInstance()->getValue('SELECT id_customer FROM customer WHERE tax_code = "'.$_GET['cf'].'"');
	if($pi > 0)
		echo 'yes';
}


if(Tools::getIsset('esporta_ore_excel')) { 
	ini_set("memory_limit","892M");
	set_time_limit(3600);
	require_once 'esportazione-catalogo/Classes/PHPExcel.php';
	$objPHPExcel = new PHPExcel();
				
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', Tools::getValue('month_name_ore_xls').' '.Tools::getValue('year_ore_xls'));
	$employees = Db::getInstance()->ExecuteS('
				SELECT `id_employee`, CONCAT(`firstname`, \' \', `lastname`) AS "name", lastname, firstname
				FROM `'._DB_PREFIX_.'employee`
				WHERE id_employee != 6 AND id_employee != 11 AND id_employee != 15  AND id_employee != 16 AND   id_employee != 20 AND id_employee != 3 AND id_employee != 18  AND id_employee != 21 AND id_employee != 23 AND id_employee != 25
			ORDER BY (CASE id_employee WHEN 1 THEN 0 WHEN 2 THEN 1 WHEN 7 THEN 2 WHEN 4 THEN 3 WHEN 3 THEN 4 WHEN 5 THEN 6 WHEN 13 THEN 7 WHEN 12 THEN 8 WHEN 14 THEN 9 WHEN 17 THEN 10 WHEN 18 THEN 11 WHEN 19 THEN 12 WHEN 22 THEN 13 WHEN 24 THEN 14 WHEN 26 THEN 15 END)');
	$num = 2;			
	
	$days_number = cal_days_in_month(CAL_GREGORIAN, Tools::getValue('month_ore_xls'), Tools::getValue('year_ore_xls'));
	$last_letter = '';
	
	if($days_number == 28)
	{
		$last_letter = 'AC';
		$columns = createColumnsArray($last_letter);
	}	
	else if($days_number == 29)
	{
		$last_letter = 'AD';
		$columns = createColumnsArray($last_letter);
	}
	else if($days_number == 30)
	{
		$last_letter = 'AE';
		$columns = createColumnsArray($last_letter);	
	}
	else 
	{
		$last_letter = 'AF';
		$columns = createColumnsArray($last_letter);	
	}
	foreach($employees as $employee)
	{
		if($employee['id_employee'] == 6 || (Tools::getValue('month_ore_xls') > 9 && Tools::getValue('year_ore_xls') >= 2018 && ($employee['id_employee'] == 3 || $employee['id_employee'] == 18)))
		{
		}
		else
		{
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue("A$num", $employee['lastname'].' '.$employee['firstname']);
			
			$valori_ore = Db::getInstance()->getValue('SELECT valori_ore FROM ore WHERE id_employee = '.$employee['id_employee'].' AND month = '.Tools::getValue('month_ore_xls').' AND year = '.Tools::getValue('year_ore_xls').'');
							
			$valori = explode("*",$valori_ore);
			$valori_i = 0;
			foreach(array_slice($columns, 1) as $column)
			{
				if($valori[$valori_i] == 0 && is_numeric($valori[$valori_i]))
					$valori[$valori_i] = '';
				
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue($column.$num, $valori[$valori_i]);
				$objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth(5);
				
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue($column."1", $valori_i+1);
				
				$objPHPExcel->getActiveSheet()->getStyle($column.$num)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
				$valori_i++;
			}
		
			$num++;
		}
	}
				
	$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(30);
	
	$objPHPExcel->getActiveSheet()->getStyle("A")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				
	$objPHPExcel->getActiveSheet()->setTitle('Ore Ezdirect '.Tools::getValue('month_name_ore_xls').' '.Tools::getValue('year_ore_xls'));

	$objPHPExcel->getActiveSheet()->getStyle('A1:'.$last_letter.'1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	

	$objPHPExcel->getActiveSheet()->getStyle('A1:'.$last_letter.'1')->getFill()->getStartColor()->setRGB('FFFF00');	

	$objPHPExcel->getActiveSheet()->getStyle("A1:".$last_letter."$num")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				
	$objPHPExcel->getActiveSheet()->getStyle('A1:'.$last_letter.'1')->getFont()->setBold(true);
				
	$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
	$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
	PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

	$data = date("Ymd");

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->setPreCalculateFormulas(false);
			
	$filename = 'ore-ezdirect-'.Tools::getValue('month_name_ore_xls').'-'.Tools::getValue('year_ore_xls').'.xls';
	ob_end_clean();
	header('Content-type: application/ms-excel');
	header('Content-Disposition: attachment; filename='.$filename);
	$objWriter->save('php://output');
	return true;	
}

if(Tools::getIsset('inviafatturamail'))
{
	global $cookie, $currentIndex;
	
	$params = array(
	'{msg}' => 'Gentile cliente, <a href="https://www.ezdirect.it/fattura.php?id_fattura='.Tools::getValue('id_fattura').'&id_customer='.Tools::getValue('id_customer').'">cliccando qui</a> puoi scaricare la tua fattura n. '.Tools::getValue('id_fattura').' in formato PDF.
	'.(Tools::getValue('note') != '' ? '<br /><br /><strong>Note aggiuntive</strong>: '.Tools::getValue('note') : '').'
	<br /><br />Cordiali saluti, <br />Lo staff Ezdirect');
	
	$mailfattura = Db::getInstance()->getValue('SELECT email FROM customer WHERE id_customer = '.Tools::getValue('id_customer').'');
			
	Mail::Send(5, 'msg_base', Mail::l('La tua fattura', 5), 
	$params, Tools::getValue('mail'), NULL, NULL, NULL, NULL, NULL, 
	_PS_MAIL_DIR_, true);
	
	$datiFatturaTipo = Db::getInstance()->getValue('SELECT tipo FROM fattura WHERE id_fattura = "'.Tools::getValue('id_fattura').'"');

	$log_mail=fopen("../import/log-mail.txt","a+");
	$riga_log = "FTA | Mail inviata a ".Tools::getValue('mail')." in data ".date("Y-m-d H:i:s")."\n";
	@fwrite($log_mail,$riga_log);
	
	$id_storico = Db::getInstance()->getValue('SELECT id_riga FROM fattura WHERE id_fattura = "'.Tools::getValue('id_fattura').'" ORDER BY id_riga ASC');
	
	Customer::Storico($id_storico, 'F', $cookie->id_employee, 'Ha inviato la fattura per mail a: '.Tools::getValue('mail') .' / '.(Tools::getValue('note') != '' ? '. Note: '.Tools::getValue('note') : ''));
	
	
}

if(Tools::getIsset('carica-pin-centralino'))
		{
			$fp = fopen($_FILES['file-di-testo']['tmp_name'], 'rb');
			$string = '';
			while ( ($line = fgets($fp)) !== false) {
			  $string .= "database put PINSET ".trim($line)." 6502\r\n";
			}
			
			$filename = "pin-centralino.txt";
				
			$newf = fopen($filename, 'w');
			fwrite($newf, $string);
			fclose($newf);
			header("Content-Description: File Transfer");
			header("Content-Length: ". filesize("$filename").";");
			header("Content-Disposition: attachment; filename=$filename");
			header("Content-Type: application/octet-stream; "); 
			header("Content-Transfer-Encoding: binary");
			readfile($filename);
			
		}

if(Tools::getIsset('esporta-target-excel')) { 
	ini_set("memory_limit","892M");
	set_time_limit(3600);
	require_once 'esportazione-catalogo/Classes/PHPExcel.php';
	$objPHPExcel = new PHPExcel();
				
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1','Obiettivo')
	->setCellValue('B1','Anno')
	->setCellValue('C1','Vnd Anno')
	->setCellValue('D1','% Anno')
	->setCellValue('E1','ML Anno')
	->setCellValue('F1','Q1')
	->setCellValue('G1','Vnd Q1')
	->setCellValue('H1','% Q1')
	->setCellValue('I1','ML Q1')
	->setCellValue('J1','Q2')
	->setCellValue('K1','Vnd Q2')
	->setCellValue('L1','% Q2')
	->setCellValue('M1','ML Q2')
	->setCellValue('N1','Q3')
	->setCellValue('O1','Vnd Q3')
	->setCellValue('P1','% Q3')
	->setCellValue('Q1','ML Q3')
	->setCellValue('R1','Q4')
	->setCellValue('S1','Vnd Q4')
	->setCellValue('T1','% Q4')
	->setCellValue('U1','ML Q4')
	->setCellValue('V1','QTOT')
	->setCellValue('W1','Vnd QTOT')
	->setCellValue('X1','% QTOT')
	->setCellValue('Y1','ML QTOT');
	$num = 2;	
	
	$anno_di_riferimento = Tools::getValue('anno');
	
	$query_totale = 'SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate)+IFNULL((SELECT SUM(importo) FROM bdl WHERE rif_ordine = 0 AND importo > 0 AND invio_contabilita > 0 AND year(`date_add`) = '.($anno_di_riferimento).'';

	$query_totale_2 = ' ),0) as total
	FROM `'._DB_PREFIX_.'orders` o
	LEFT JOIN order_history oh ON oh.id_order_state = 6 AND oh.id_order = o.id_order
	LEFT JOIN `'._DB_PREFIX_.'order_detail` od ON o.id_order = od.id_order
	WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire" OR o.module = "other_payment"))) AND oh.id_order_state IS NULL AND year( o.date_add ) = '.($anno_di_riferimento) .'';
	 
	$query_margine = 'SELECT SUM(((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) - ((CASE WHEN (od.wholesale_price = 0 AND od.no_acq = 0) THEN p.wholesale_price ELSE od.wholesale_price END)*od.product_quantity))+IFNULL(((SELECT SUM(importo) FROM bdl WHERE rif_ordine = 0 AND importo > 0 AND invio_contabilita > 0 AND year(`date_add`) = '.($anno_di_riferimento);
	
	$query_margine_2 = ' )*0.30),0) as margine,
	SUM((CASE WHEN (od.wholesale_price = 0 AND od.no_acq = 0) THEN p.wholesale_price ELSE od.wholesale_price END)*od.product_quantity) as tot_acquisti
	FROM `'._DB_PREFIX_.'orders` o
	LEFT JOIN `'._DB_PREFIX_.'order_detail` od ON o.id_order = od.id_order
	LEFT JOIN order_history oh ON oh.id_order_state = 6 AND oh.id_order = o.id_order
	LEFT JOIN `'._DB_PREFIX_.'product` p ON p.id_product = od.product_id
	LEFT JOIN `'._DB_PREFIX_.'customer` c ON o.id_customer = c.id_customer 
	WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire" OR o.module = "other_payment"))) AND oh.id_order_state IS NULL AND year( o.date_add ) = '.($anno_di_riferimento) .'';
	
	$where_totale = "";
	$where_margine = "";
	
	$query_totale_d = Db::getInstance()->getRow($query_totale.$query_totale_2.$where_totale);
	$query_margine_d = Db::getInstance()->getRow($query_margine.$query_margine_2.$where_totale);
	 
	$where_totale = "";
	$where_margine = "";
	
	$obiettivi_anno_q = Db::getInstance()->getRow('SELECT * FROM trgt WHERE anno = "'.$anno_di_riferimento.'" AND descrizione = "Anno"');
			
	$obiettivi_anno = unserialize($obiettivi_anno_q['configurazione']);
	
	$tot_t = number_format($query_totale_d['total'],2,",",".");
	$tot_d = number_format(((($query_totale_d['total']*100)/$obiettivi_anno['totale'])),2,",",".");
	$tot_m = number_format($query_margine_d['margine'],2,",",".");
	
	$query_totale_d = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
	$query_margine_d = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
	
	//q1
	$where_totale = ' AND ( month(o.date_add) = 1 OR  month(o.date_add) = 2 OR month(o.date_add) = 3)';
	$where_margine = ' AND ( month(o.date_add) = 1 OR  month(o.date_add) = 2 OR month(o.date_add) = 3)';
	$where_bdl = ' AND ( month(date_add) = 1 OR  month(date_add) = 2 OR month(date_add) = 3)';		
	$query_totale_q1 = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
	$query_margine_q1 = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
	
	//q2
	$where_totale = ' AND ( month(o.date_add) = 4 OR  month(o.date_add) = 5 OR month(o.date_add) = 6)';
	$where_margine = ' AND ( month(o.date_add) = 4 OR  month(o.date_add) = 5 OR month(o.date_add) = 6)';
	$where_bdl = ' AND ( month(date_add) = 4 OR  month(date_add) = 5 OR month(date_add) = 6)';		
	$query_totale_q2 = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
	$query_margine_q2 = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
	
	//q3
	$where_totale = ' AND ( month(o.date_add) = 7 OR  month(o.date_add) = 8 OR month(o.date_add) = 9)';
	$where_margine = ' AND ( month(o.date_add) = 7 OR  month(o.date_add) = 8 OR month(o.date_add) = 9)';
	$where_bdl = ' AND ( month(date_add) = 7 OR  month(date_add) = 8 OR month(date_add) = 9)';		
	$query_totale_q3 = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
	$query_margine_q3 = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
	
	//q4
	$where_totale = ' AND ( month(o.date_add) = 10 OR  month(o.date_add) = 11 OR month(o.date_add) = 12)';
	$where_margine = ' AND ( month(o.date_add) = 10 OR  month(o.date_add) = 11 OR month(o.date_add) = 12)';
	$where_bdl = ' AND ( month(date_add) = 10 OR  month(date_add) = 11 OR month(date_add) = 12)';		
	$query_totale_q4 = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
	$query_margine_q4 = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
	
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A'.$num,'Anno')
	->setCellValue('B'.$num,number_format($obiettivi_anno['totale'],2,",","."))
	->setCellValue('C'.$num,number_format($query_totale_d['total'],2,",","."))
	->setCellValue('D'.$num,number_format(((($query_totale_d['total']*100)/$obiettivi_anno['totale'])),2,",","."))
	->setCellValue('E'.$num,number_format($query_margine_d['margine'],2,",","."))
	->setCellValue('F'.$num,number_format($obiettivi_anno['q1'],2,",","."))
	->setCellValue('G'.$num,number_format($query_totale_q1['total'],2,",","."))
	->setCellValue('H'.$num,number_format(((($query_totale_q1['total']*100)/$obiettivi_anno['q1'])),2,",","."))
	->setCellValue('I'.$num,number_format($query_margine_q1['margine'],2,",","."))
	->setCellValue('J'.$num,number_format($obiettivi_anno['q2'],2,",","."))
	->setCellValue('K'.$num,number_format($query_totale_q2['total'],2,",","."))
	->setCellValue('L'.$num,number_format(((($query_totale_q2['total']*100)/$obiettivi_anno['q2'])),2,",","."))
	->setCellValue('M'.$num,number_format($query_margine_q2['margine'],2,",","."))
	->setCellValue('N'.$num,number_format($obiettivi_anno['q3'],2,",","."))
	->setCellValue('O'.$num,number_format($query_totale_q3['total'],2,",","."))
	->setCellValue('P'.$num,number_format(((($query_totale_q3['total']*100)/$obiettivi_anno['q3'])),2,",","."))
	->setCellValue('Q'.$num,number_format($query_margine_q3['margine'],2,",","."))
	->setCellValue('R'.$num,number_format($obiettivi_anno['q4'],2,",","."))
	->setCellValue('S'.$num,number_format($query_totale_q4['total'],2,",","."))
	->setCellValue('T'.$num,number_format(((($query_totale_q4['total']*100)/$obiettivi_anno['q4'])),2,",","."))
	->setCellValue('U'.$num,number_format($query_margine_q4['margine'],2,",","."))
	->setCellValue('V'.$num,number_format($obiettivi_anno['q1']+$obiettivi_anno['q2']+$obiettivi_anno['q3']+$obiettivi_anno['q4'],2,",","."))
	->setCellValue('W'.$num,number_format($query_totale_q1['total']+$query_totale_q2['total']+$query_totale_q3['total']+$query_totale_q4['total'],2,",","."))
	->setCellValue('X'.$num,number_format((((($query_totale_q1['total']+$query_totale_q2['total']+$query_totale_q3['total']+$query_totale_q4['total'])*100)/($obiettivi_anno['q1']+$obiettivi_anno['q2']+$obiettivi_anno['q3']+$obiettivi_anno['q4']))),2,",","."))
	->setCellValue('Y'.$num,number_format($query_margine_q1['margine']+$query_margine_q2['margine']+$query_margine_q3['margine']+$query_margine_q4['margine'],2,",","."));
			
	$costruttori_anno = Db::getInstance()->executeS('SELECT * FROM trgt WHERE anno = "'.$anno_di_riferimento.'" AND descrizione != "Anno"');
	$num++;
	
	foreach($costruttori_anno as $c) {
				
					
		$costruttore_array = $c['descrizione'];
		$costruttore_array = explode("-",$costruttore_array);
		$costruttore = $costruttore_array[1];
		$nome_costruttore = Db::getInstance()->getValue('SELECT name FROM manufacturer WHERE id_manufacturer = '.$costruttore.'');
		
		if($costruttore == 105)
			$where_bdl_c = '';
		else
			$where_bdl_c = ' AND id_bdl = "XXX"';
		
		
		$query_totale = 'SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate)+IFNULL((SELECT SUM(importo) FROM bdl WHERE rif_ordine = 0 AND importo > 0 AND invio_contabilita > 0 AND year(`date_add`) = '.($anno_di_riferimento).'';

		$query_totale_2 = ' ),0) as total
		FROM `'._DB_PREFIX_.'orders` o
		LEFT JOIN order_history oh ON oh.id_order_state = 6 AND oh.id_order = o.id_order
		LEFT JOIN `'._DB_PREFIX_.'order_detail` od ON o.id_order = od.id_order
		WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire" OR o.module = "other_payment"))) AND oh.id_order_state IS NULL AND year( o.date_add ) = '.($anno_di_riferimento) .'';
		 
		$query_margine = 'SELECT SUM(((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) - ((CASE WHEN (od.wholesale_price = 0 AND od.no_acq = 0) THEN p.wholesale_price ELSE od.wholesale_price END)*od.product_quantity))+IFNULL(((SELECT SUM(importo) FROM bdl WHERE rif_ordine = 0 AND importo > 0 AND invio_contabilita > 0 AND year(`date_add`) = '.($anno_di_riferimento);
		
		$query_margine_2 = ' )*0.30),0) as margine,
		SUM((CASE WHEN (od.wholesale_price = 0 AND od.no_acq = 0) THEN p.wholesale_price ELSE od.wholesale_price END)*od.product_quantity) as tot_acquisti
		FROM `'._DB_PREFIX_.'orders` o
		LEFT JOIN `'._DB_PREFIX_.'order_detail` od ON o.id_order = od.id_order
		LEFT JOIN order_history oh ON oh.id_order_state = 6 AND oh.id_order = o.id_order
		LEFT JOIN `'._DB_PREFIX_.'product` p ON p.id_product = od.product_id
		LEFT JOIN `'._DB_PREFIX_.'customer` c ON o.id_customer = c.id_customer 
		WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire" OR o.module = "other_payment"))) AND oh.id_order_state IS NULL AND year( o.date_add ) = '.($anno_di_riferimento) .'';
		
		$where_totale = ' AND od.manufacturer_id = '.$costruttore.' ';
		$where_margine = ' AND od.manufacturer_id = '.$costruttore.' ';
		
		$query_totale_d = Db::getInstance()->getRow($query_totale.$where_bdl_c.$query_totale_2.$where_totale);
					$query_margine_d = Db::getInstance()->getRow($query_margine.$where_bdl_c.$query_margine_2.$where_totale);
		 
		$where_totale = "";
		$where_margine = "";
		
		
		
		$obiettivi_anno_q = Db::getInstance()->getRow('SELECT * FROM trgt WHERE anno = "'.$anno_di_riferimento.'" AND descrizione = "m-'.$costruttore.'-'.$anno_di_riferimento.'"');
				
		$obiettivi_anno = unserialize($obiettivi_anno_q['configurazione']);
		
		$tot_t = number_format($query_totale_d['total'],2,",",".");
		$tot_d = number_format(((($query_totale_d['total']*100)/$obiettivi_anno['totale'])),2,",",".");
		$tot_m = number_format($query_margine_d['margine'],2,",",".");
		if($obiettivi_anno['totale'] == 0 && $obiettivi_anno['q1'] == 0 && $obiettivi_anno['q2'] == 0 && $obiettivi_anno['q3'] == 0 && $obiettivi_anno['q4'] == 0)
		{
					
		}
					
		else
		{
		
			//q1
			$where_totale = ' AND od.manufacturer_id = '.$costruttore.' AND ( month(o.date_add) = 1 OR  month(o.date_add) = 2 OR month(o.date_add) = 3)';
			$where_margine = ' AND od.manufacturer_id = '.$costruttore.' AND  ( month(o.date_add) = 1 OR  month(o.date_add) = 2 OR month(o.date_add) = 3)';
			$where_bdl = $where_bdl_c.' AND od.manufacturer_id = '.$costruttore.' AND  ( month(date_add) = 1 OR  month(date_add) = 2 OR month(date_add) = 3)';		
			$query_totale_q1 = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
			$query_margine_q1 = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
			
			//q2
			$where_totale = ' AND od.manufacturer_id = '.$costruttore.' AND  ( month(o.date_add) = 4 OR  month(o.date_add) = 5 OR month(o.date_add) = 6)';
			$where_margine = ' AND od.manufacturer_id = '.$costruttore.' AND  ( month(o.date_add) = 4 OR  month(o.date_add) = 5 OR month(o.date_add) = 6)';
			$where_bdl = $where_bdl_c.' AND ( month(date_add) = 4 OR  month(date_add) = 5 OR month(date_add) = 6)';		
			$query_totale_q2 = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
			$query_margine_q2 = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
			
			//q3
			$where_totale = ' AND od.manufacturer_id = '.$costruttore.' AND  ( month(o.date_add) = 7 OR  month(o.date_add) = 8 OR month(o.date_add) = 9)';
			$where_margine = ' AND od.manufacturer_id = '.$costruttore.' AND  ( month(o.date_add) = 7 OR  month(o.date_add) = 8 OR month(o.date_add) = 9)';
			$where_bdl = $where_bdl_c.' AND ( month(date_add) = 7 OR  month(date_add) = 8 OR month(date_add) = 9)';		
			$query_totale_q3 = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
			$query_margine_q3 = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
			
			//q4
			$where_totale = ' AND od.manufacturer_id = '.$costruttore.' AND  ( month(o.date_add) = 10 OR  month(o.date_add) = 11 OR month(o.date_add) = 12)';
			$where_margine = ' AND od.manufacturer_id = '.$costruttore.' AND  ( month(o.date_add) = 10 OR  month(o.date_add) = 11 OR month(o.date_add) = 12)';
			$where_bdl = $where_bdl_c.' AND ( month(date_add) = 10 OR  month(date_add) = 11 OR month(date_add) = 12)';		
			$query_totale_q4 = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
			$query_margine_q4 = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
			
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$num, $nome_costruttore)
			->setCellValue('B'.$num,number_format($obiettivi_anno['totale'],2,",","."))
			->setCellValue('C'.$num,number_format($query_totale_d['total'],2,",","."))
			->setCellValue('D'.$num,number_format(((($query_totale_d['total']*100)/$obiettivi_anno['totale'])),2,",","."))
			->setCellValue('E'.$num,number_format($query_margine_d['margine'],2,",","."))
			->setCellValue('F'.$num,number_format($obiettivi_anno['q1'],2,",","."))
			->setCellValue('G'.$num,number_format($query_totale_q1['total'],2,",","."))
			->setCellValue('H'.$num,number_format(((($query_totale_q1['total']*100)/$obiettivi_anno['q1'])),2,",","."))
			->setCellValue('I'.$num,number_format($query_margine_q1['margine'],2,",","."))
			->setCellValue('J'.$num,number_format($obiettivi_anno['q2'],2,",","."))
			->setCellValue('K'.$num,number_format($query_totale_q2['total'],2,",","."))
			->setCellValue('L'.$num,number_format(((($query_totale_q2['total']*100)/$obiettivi_anno['q2'])),2,",","."))
			->setCellValue('M'.$num,number_format($query_margine_q2['margine'],2,",","."))
			->setCellValue('N'.$num,number_format($obiettivi_anno['q3'],2,",","."))
			->setCellValue('O'.$num,number_format($query_totale_q3['total'],2,",","."))
			->setCellValue('P'.$num,number_format(((($query_totale_q3['total']*100)/$obiettivi_anno['q3'])),2,",","."))
			->setCellValue('Q'.$num,number_format($query_margine_q3['margine'],2,",","."))
			->setCellValue('R'.$num,number_format($obiettivi_anno['q4'],2,",","."))
			->setCellValue('S'.$num,number_format($query_totale_q4['total'],2,",","."))
			->setCellValue('T'.$num,number_format(((($query_totale_q4['total']*100)/$obiettivi_anno['q4'])),2,",","."))
			->setCellValue('U'.$num,number_format($query_margine_q4['margine'],2,",","."))
			->setCellValue('V'.$num,number_format($obiettivi_anno['q1']+$obiettivi_anno['q2']+$obiettivi_anno['q3']+$obiettivi_anno['q4'],2,",","."))
			->setCellValue('W'.$num,number_format($query_totale_q1['total']+$query_totale_q2['total']+$query_totale_q3['total']+$query_totale_q4['total'],2,",","."))
			->setCellValue('X'.$num,number_format((((($query_totale_q1['total']+$query_totale_q2['total']+$query_totale_q3['total']+$query_totale_q4['total'])*100)/($obiettivi_anno['q1']+$obiettivi_anno['q2']+$obiettivi_anno['q3']+$obiettivi_anno['q4']))),2,",","."))
			->setCellValue('Y'.$num,number_format($query_margine_q1['margine']+$query_margine_q2['margine']+$query_margine_q3['margine']+$query_margine_q4['margine'],2,",","."));
			$num++;
		}
	}
	
	$objPHPExcel->getActiveSheet()->getStyle("B2:Y$num")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				
	$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(30);
	
	$objPHPExcel->getActiveSheet()->getStyle("A")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				
	$objPHPExcel->getActiveSheet()->setTitle('Target '.$anno_di_riferimento);

	$objPHPExcel->getActiveSheet()->getStyle('A1:Y1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	

	$objPHPExcel->getActiveSheet()->getStyle('A1:Y1')->getFill()->getStartColor()->setRGB('FFFF00');	

	$objPHPExcel->getActiveSheet()->getStyle("A1:Y$num")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				
	$objPHPExcel->getActiveSheet()->getStyle('A1:Y1')->getFont()->setBold(true);
				
	$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
	$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
	PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

	$data = date("Ymd");

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->setPreCalculateFormulas(false);
			
	$filename = 'target-'.$anno_di_riferimento.'.xls';
	header('Content-type: application/ms-excel');
	header('Content-Disposition: attachment; filename='.$filename);
	ob_end_clean();
	$objWriter->save('php://output');
	return true;	
}

if (Tools::isSubmit('submitShippingNumber_ajax'))
{
	global $cookie; 
	
	$order = new Order(Tools::getValue('id_order'));
	
	$id_order = $order->id;
	
	$shipping_number = pSQL(Tools::getValue('shipping_number'));
	
	if($order->shipping_number == $shipping_number)
		die('NO');
	
	$order->shipping_number = $shipping_number;
	
	$order->update();
	
	if ($shipping_number)
	{
		global $_LANGMAIL;
		$customer = new Customer((int)($order->id_customer));
		$carrier = new Carrier((int)($order->id_carrier));
		if (!Validate::isLoadedObject($customer) OR !Validate::isLoadedObject($carrier))
			die(Tools::displayError());
		$templateVars = array(
			'{followup}' => str_replace('@', $order->shipping_number, $carrier->url),
			'{firstname}' => $customer->firstname,
			'{id_collo}' =>$order->shipping_number,
			'{lastname}' => $customer->lastname,
			'{id_order}' => (int)($order->id)
		);
		
		$id_country = Db::getInstance()->getValue("SELECT id_country FROM address WHERE id_address = ".$order->id_address_delivery."");
		
		Db::getInstance()->executeS('UPDATE action_thread SET status = "closed" WHERE subject LIKE "%*** PROMEMORIA ORDINE FATTURATO N. '.$order->id.'%"');
		
		if($order->id_lang == 0)
			$order->id_lang = 5;
		
		if($customer->order_notifications == 1) {
		}
		else {
	
			if($id_country == 10) {
				@Mail::Send((int)$order->id_lang, 'in_transit', Mail::l('Ordine spedito', (int)$order->id_lang), $templateVars,
				$customer->email, $customer->firstname.' '.$customer->lastname, NULL, NULL, NULL, NULL,
				_PS_MAIL_DIR_, true);
			}
			else
			{
				@Mail::Send((int)$order->id_lang, 'in_transit', Mail::l('Order sent', (int)$order->id_lang), $templateVars,
				$customer->email, $customer->firstname.' '.$customer->lastname, NULL, NULL, NULL, NULL,
				_PS_MAIL_DIR_, true);
				
			}
		}
		
	}
	
	$newOrderStatusId = Tools::getValue('id_order_state');
				
	$id_order_state_attuale = Db::getInstance()->getValue('SELECT id_order_state FROM order_history WHERE id_order = '.$id_order.' ORDER BY id_order_history DESC');
					
	$stato_attuale = Order::eSolverStatus($id_order, $id_order_state_attuale);
	$stato_nuovo = Order::eSolverStatus($id_order, $newOrderStatusId);
	
	if($newOrderStatusId == 4 || $newOrderStatusId == 5 || $newOrderStatusId == 14 || $newOrderStatusId == 15 || $newOrderStatusId == 16 || $newOrderStatusId == 20)
	{
		$history_prec = Db::getInstance()->getValue('SELECT id_order_history FROM order_history WHERE (id_order_state = 4 OR id_order_state = 5  OR id_order_state = 14  OR id_order_state = 15  OR id_order_state = 16  OR id_order_state = 20) AND id_order = '.$order->id);
		
		
		if($modalita_esolver == 0 && (!$history_prec || $history_prec < 0 || $history_prec == ''))
		{
			// SCARICO MAGAZZINO
			
			$prime = Db::getInstance()->getValue('SELECT id_cart FROM order WHERE id_order = '.$order->id.' AND id_cart IN (SELECT id_cart FROM cart WHERE name LIKE "%AMAZON PRIME%")');
			
			if($prime > 0)
			{
				
			}
			else
			{
				$products = $order->getProducts();
				foreach($products as $product)
				{
					$stock = Db::getInstance()->getRow('SELECT quantity, stock_quantity, itancia_quantity, esprinet_quantity, techdata_quantity, intracom_quantity, supplier_quantity FROM product WHERE id_product = '.$product['product_id']);
					
					if($newOrderStatusId != 15)
						$stock_quantity = $stock['stock_quantity'] -= $product['product_quantity'];
					else
					{
						$cons = Db::getInstance()->getValue('SELECT cons FROM order_detail WHERE id_order = '.$order->id.' AND product_id = '.$product['product_id']);
						$cons = str_replace('0_','',$cons);
						$qt_da_sottrarre = $cons;
						$stock_quantity = $stock['stock_quantity'] -= $qt_da_sottrarre;
					}
					
					$qtsito = ($stock['supplier_quantity'] + $stock_quantity + $stock['esprinet_quantity'] + $stock['itancia_quantity'] + $stock['techdata_quantity'] + $stock['intracom_quantity']);
					
					Db::getInstance()->executeS("UPDATE product SET stock_quantity = $stock_quantity, quantity = $qtsito WHERE id_product = ".$product['product_id']."");

				}
			}	
		}
	}	
					
	$history = new OrderHistory();
	$history->id_order = (int)$id_order;
	$history->id_employee = (int)($cookie->id_employee);
	$history->changeIdOrderState((int)($newOrderStatusId), (int)($id_order));
	$order = new Order((int)$order->id);
	
	$products = $order->getProducts();
	if($newOrderStatusId == 31)
	{
		$trasp_id = Db::getInstance()->getValue('SELECT product_id FROM rimborsi WHERE product_id = "TRASP" AND id_order = '.$order->id);
		if($trasp_id == 0 || $trasp_id == '' || !$trasp_id)
		{
			Db::getInstance()->executeS('INSERT INTO rimborsi (id_order, product_id, product_price, product_quantity) VALUES ("'.$order->id.'", "TRASP", "'.$order->total_shipping.'", "1")');
		}
	}
					
	foreach($products as $product)
	{
		if($newOrderStatusId == 31)
		{
			$product_id = Db::getInstance()->getValue('SELECT product_id FROM rimborsi WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
			if(!$product_id || $product_id <= 0 || $product_id == '')
			{
				
				Db::getInstance()->executeS('INSERT INTO rimborsi (id_order, product_id, product_quantity, product_price) VALUES ("'.$order->id.'", "'.$product['product_id'].'", "'.$product['product_quantity'].'", "'.($product['product_price']-($product['product_price']*$product['reduction_percent']/100)).'")');
			}
		}
	
		if ($newOrderStatusId != 15 && ($newOrderStatusId != 24 && $newOrderStatusId != 25 && $newOrderStatusId != 27 && $newOrderStatusId != 28))
			Db::getInstance()->executeS('UPDATE order_detail SET cons = "" WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
		else
		{
			$cons = Db::getInstance()->getValue('SELECT cons FROM order_detail WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
			if($cons == '' || empty($cons))
			{
				//$rif_fatt = Db::getInstance()->getValue('SELECT rif_vs_ordine FROM fattura WHERE rif_vs_ordine = '.$order->id);
				if($rif_fatt == $order->id && in_array(trim($product['product_reference']), $array_parziali))
					Db::getInstance()->executeS('UPDATE order_detail SET cons = "0_'.Db::getInstance()->getValue('SELECT SUM(qt_ord) FROM fattura WHERE cod_articolo = "'.trim($product['product_reference']).'" AND rif_ordine = '.$order->id.' GROUP BY cod_articolo').'" WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
				else if($rif_fatt == $order->id && !(in_array(trim($product['product_reference']), $array_parziali)))
					Db::getInstance()->executeS('UPDATE order_detail SET cons = "0_0" WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
				else if($rif_fatt != $order->id)
					Db::getInstance()->executeS('UPDATE order_detail SET cons = "0_0" WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
				else
					Db::getInstance()->executeS('UPDATE order_detail SET cons = "0_0" WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
			}	
		}
	}
					
		if($newOrderStatusId == 4)
		{
			
			Db::getInstance()->executeS('UPDATE action_thread SET status = "closed" WHERE subject LIKE "%PROMEMORIA ORDINE CON BONIFICO N. '.$order->id.'%" AND action_to = 7');
			
			$scadenza = Db::getInstance()->getValue('SELECT scadenza FROM cart WHERE id_cart = '.$order->id_cart);
			$cadenza = Db::getInstance()->getValue('SELECT cadenza FROM cart WHERE id_cart = '.$order->id_cart);
			$competenza_dal = Db::getInstance()->getValue('SELECT competenza_dal FROM cart WHERE id_cart = '.$order->id_cart);
			$competenza_al = Db::getInstance()->getValue('SELECT competenza_al FROM cart WHERE id_cart = '.$order->id_cart);
			$id_contratto_t = Db::getInstance()->getValue("SELECT id_contratto FROM contratto_assistenza WHERE rif_ordine = ".$order->id);
			if($cadenza != '' && $id_contratto_t <= 0)
			{
				if($scadenza != '' && strtotime($scadenza) > 0  && $scadenza != '0000-00-00' && $scadenza != '1970-01-01')
				{
					
				}
				else
				{
					$scadenza = date('Y-m-d',strtotime(date("Y-m-d H:i:s", mktime()) . " + 365 day"));
				}
				$tot_teleassistenza_row = Db::getInstance()->getRow("SELECT count(*) AS tot_assistenza, SUM((`product_price` - ((product_price * reduction_percent) / 100))) AS totale FROM order_detail WHERE id_order = ".$order->id." AND (product_reference LIKE '%teleg%') AND (product_reference NOT LIKE '%r1y%')");
				
				$tot_assistenza_row = Db::getInstance()->getRow("SELECT count(*) AS tot_assistenza, SUM((`product_price` - ((product_price * reduction_percent) / 100))) AS totale FROM order_detail WHERE id_order = ".$order->id." AND (product_reference LIKE '%astectop%')");
				
				if($tot_teleassistenza_row['tot_assistenza'] > 0)
					$tipo_contratto = 4;
				else 
					$tipo_contratto = 2;
				
				$dom = explode('-',$scadenza);
				
				$importo = $tot_assistenza_row['totale'] + $tot_teleassistenza_row['totale'];
				
				$data_scadenza = $dom[0].'-'.$dom[1].'-'.$dom[2].' 23:59:59';
				
				$decorrenza = Db::getInstance()->getValue('SELECT decorrenza FROM cart WHERE id_cart = '.$order->id_cart);
				
				if($decorrenza != '' && $decorrenza != '0000-00-00')
				{
					$dom2 = explode('-',$decorrenza);
					$data_decorrenza = $dom2[0].'-'.$dom2[1].'-'.$dom2[2].' 23:59:59';
				}
				else
				{
					$data_decorrenza = date('Y-m-d H:i:s');
				}
				
				$dom3 = explode('-',$competenza_dal);
				$dom4 = explode('-',$competenza_al);
				
				$data_competenza_dal = $dom3[0].'-'.$dom3[1].'-'.$dom3[2].'';
				$data_competenza_al = $dom4[0].'-'.$dom4[1].'-'.$dom4[2].'';
				
				$id_contratto = Db::getInstance()->getValue("SELECT id_contratto FROM contratto_assistenza ORDER BY id_contratto DESC");
				$id_contratto++;
		
				Db::getInstance()->executeS("INSERT INTO contratto_assistenza (id_contratto, id_customer, codice_spring, status, tipo, descrizione, data_registrazione, data_inizio, data_fine, cadenza, competenza_dal, competenza_al, cig, cup, univoco, indirizzo, prezzo, periodicita, pagamento, blocco_amministrativo, rif_fattura, rif_noleggio, rif_ordine, note_private, date_add, date_upd) VALUES (".$id_contratto.", '".$order->id_customer."', '".Db::getInstance()->getValue('SELECT codice_spring FROM customer WHERE id_customer = '.$order->id_customer)."', '0', '".$tipo_contratto."', '', '".$data_decorrenza."', '".$data_decorrenza."', '".$data_scadenza."', '".$cadenza."', '".$data_competenza_dal."', '".$data_competenza_al."', '', '', '', '".$order->id_address_invoice."', '".$importo."', '12', '".Db::getInstance()->getValue('SELECT payment FROM orders WHERE id_order = '.$order->id.'')."', '0', '', '', '".$order->id."', '', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
				
				$products_ctr = Db::getInstance()->executeS('SELECT * FROM order_detail WHERE id_order = '.$order->id);
				
				foreach($products_ctr as $product_ctr)
					Db::getInstance()->executeS("INSERT INTO contratto_assistenza_prodotti (id_contratto, id_product, codice_prodotto, descrizione_prodotto, quantita, prezzo, seriale, note, indirizzo, date_add, date_upd) VALUES ('".$id_contratto."', ".$product_ctr['product_id'].", '".$product_ctr['product_reference']."', '".$product_ctr['product_name']."', '".$product_ctr['product_quantity']."', '".$product_ctr['product_price']."', '', '', '".$order->id_address_invoice."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
				
				$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
				$action_thread++;
				$action_subject = "*** PROMEMORIA - VERIFICARE CONTRATTO N. ".$id_contratto." ";
				
				$id_employee = 14;
				$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').'1');

				Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd, promemoria) VALUES ('$action_thread', 'Attivita', '".$order->id_customer."', '".addslashes($action_subject)."', 0, ".$id_employee.", 'open', '', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."', 1)");   

				Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', '".$order->id_customer."', 99, '".$id_employee."', '".$id_employee."', '', '<strong>*** VERIFICARE CONTRATTO APPENA INSERITO ***</strong><br /><br />Appena inserito <a href=\"https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$order->id_customer."&viewcustomer&id_contratto=".$id_contratto."&token=".$tokenCustomers."&tab-container-1=13\" target=\"_blank\">contratto n. ".$id_contratto."</a>. Verificare', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  

			}
			
		}
		
		if($newOrderStatusId == 16 || $newOrderStatusId == 4)
		{
			$cart_name = Db::getInstance()->getValue('SELECT name FROM cart WHERE id_cart = '.$cart->id);
			if(substr($cart_name,0,18 == 'Rinnovo assistenza'))
			{
				
				$names = explode(' ',$cart_name);
				$id_contratto = $names[2];
				$anno = $names[4];
				$scadenza = date('Y-m-d',strtotime(date("Y-m-d H:i:s", strtotime(Db::getInstance()->getValue('SELECT data_fine FROM contratto_assistenza WHERE id_contratto = '.$id_contratto)) . " + 365 day")));
				Db::getInstance()->executeS('UPDATE contratto_assistenza SET data_fine = "'.$scadenza.'", status = 0 WHERE id_contratto = '.$id_contratto);
				
				$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
				$action_thread++;
				$action_subject = "*** PROMEMORIA - VERIFICARE CONTRATTO N. ".$id_contratto." ";
				
				$id_employee = 14;
				$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').'1');

				Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd, promemoria) VALUES ('$action_thread', 'Attivita', '".$order->id_customer."', '".addslashes($action_subject)."', 0, ".$id_employee.", 'open', '', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."', 1)");   

				Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', '".$order->id_customer."', 99, '".$id_employee."', '".$id_employee."', '', '<strong>*** VERIFICARE CONTRATTO APPENA RINNOVATO ***</strong><br /><br />Appena rinnovato <a href=\"https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$order->id_customer."&viewcustomer&id_contratto=".$id_contratto."&token=".$tokenCustomers."&tab-container-1=13\" target=\"_blank\">contratto n. ".$id_contratto."</a>. Verificare', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
			}
		}
		
		if($newOrderStatusId == 18)
		{
			
			Db::getInstance()->executeS('UPDATE action_thread SET status = "closed" WHERE subject LIKE "*** PROMEMORIA ORDINE CON BONIFICO N. '.$order->id.'%"');
			
			$cart_subject = Db::getInstance()->getValue('SELECT name FROM cart WHERE id_cart = '.$order->id_cart);
			
			if(strpos($cart_subject, 'Rinnovo') !== false)
			{
				$params_contratto = explode('anno',$cart_subject);
				
				$anno_contratto = $params_contratto[1];
				$anno_contratto = (int)$anno_contratto;
				$numero_contratto = preg_replace('/[^0-9]/', '', $params_contratto[0]);
				
				$data_scadenza_contratto = Db::getInstance()->getValue('SELECT data_fine FROM contratto_assistenza WHERE id_contratto = '.$numero_contratto);
				
				$data_scadenza_att = explode(' ',$data_scadenza_contratto);
				$data_scadenza_attuale = explode('-',$data_scadenza_att[0]);
				
				$nuova_data_scadenza = ($anno_contratto+1).'-'.$data_scadenza_attuale[1].'-'.$data_scadenza_attuale[2].' 23:59:59';
				
				
				Db::getInstance()->executeS('UPDATE contratto_assistenza SET status = 0, data_fine = "'.$nuova_data_scadenza.'" WHERE id_contratto = '.$numero_contratto);
				
			}
			
			$tot_assistenza = Db::getInstance()->getValue("SELECT count( * ) AS tot_assistenza FROM order_detail WHERE (product_reference LIKE '%astec%' OR product_reference LIKE '%inst%') AND id_order = ".$order->id);
			if($tot_assistenza > 0) 
			{
				$first_product = Db::getInstance()->getValue('SELECT product_name FROM order_detail WHERE id_order = '.$order->id);
				$action_subject = "*** ACCETTATO BONIFICO SU ORDINE N. ".$order->id." (".$first_product.")";
				
				$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
				$action_thread++;
			
				Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd) VALUES ('$action_thread', 'Attivita', '".$order->id_customer."', '".addslashes($action_subject)."', ".$cookie->id_employee.", 5, 'open', 'O".$order->id."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");   

				Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', ".$order->id_customer.", ".$cookie->id_employee.", 5, 5, '', '".addslashes($action_subject)."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
				
			}	
			
		}
		$cart_name = Db::getInstance()->getValue('SELECT name FROM cart WHERE id_cart = '.$order->id_cart);
		if($newOrderStatusId == 4 && (strpos($cart_name,'AMAZON ID') !== false))
		{
			$action_this = Db::getInstance()->getValue("SELECT id_action FROM action_thread WHERE subject LIKE '%*** CHISURA AMAZON SU ORDINE N. ".$order->id." %'");
			if($action_this > 0)
			{
				
			}
			else
			{
				$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
				$action_thread++;
				
				$action_subject = "*** CHISURA AMAZON SU ORDINE N. ".$order->id." ";
				$incaricato = 14;
				$messaggio_verifica = "Chiudere su Amazon ordine N. ".$order->id." ";
				
				Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd) VALUES ('$action_thread', 'TODO Amazon', '".$order->id_customer."', '".addslashes($action_subject)."', ".$cookie->id_employee.", ".$incaricato.", 'open', 'O".$order->id."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");   

				Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', ".$order->id_customer.", ".$cookie->id_employee.", ".$incaricato.", ".$incaricato.", '', '".addslashes($messaggio_verifica)."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
				
				//mando mail
				$tokenbarbara = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."2");
				$linkbarbara = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenbarbara.'';
				$tokenmatteo = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."7");
				$linkmatteo = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenmatteo.'';
				
				$tokenincarico = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders')).$incaricato);
				$linkincarico = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenincarico.'';
				$mail_incarico = Db::getInstance()->getValue('SELECT email FROM employee WHERE id_employee = '.$incaricato);
				
				$nome_incarico =  Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$incaricato);
				
				$testomail = "Verifica tecnica su ordine n. ".$order->id.". <br /><br />
				Messaggio: ".$messaggio_verifica."<br />
				Clicca sul link per entrare nell'ordine:<br />
				Barbara - <a href='".$linkbarbara."'>clicca qui</a><br />
				Matteo - <a href='".$linkmatteo."'>clicca qui</a><br />
				".($incaricato == 2 || $incaricato == 7 ? '' : $nome_incarico." - <a href='".$linkincarico."'>clicca qui</a><br />")."";
			}
		}
		
		if($newOrderStatusId == 24)
		{
			
			$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
			$action_thread++;
			
			$action_subject = "*** VERIFICA TECNICA SU ORDINE N. ".$order->id." ";
			
			$incaricato = Tools::getValue('impiegati_verifica');
			$messaggio_verifica = Tools::getValue('verifica_msg');
			
			$existing_action = Db::getInstance()->getValue('SELECT id_action FROM action_thread WHERE subject LIKE "%'.$action_subject.'%"');
			$existing_employee = Db::getInstance()->getValue('SELECT action_to FROM action_thread WHERE subject LIKE "%'.$action_subject.'%"');
			if($existing_action  > 0)
			{
				
				$action_thread = $existing_action;
				
				Db::getInstance()->executeS('UPDATE action_thread SET action_to = '.$incaricato.' WHERE id_action = '.$action_thread.'');
				
				Customer::Storico($action_thread, 'A', $cookie->id_employee, $incaricato);
				
				$mailimp = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".$incaricato."'");
							
				$tokenimp = Tools::getAdminToken("AdminCustomers"."2".$incaricato);
								
				$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewaction&id_action=".Tools::getValue('id_action')."&token=".$tokenimp.'&tab-container-1=10';
				
				$employeemess = new Employee($cookie->id_employee);
	
				$msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato l'azione numero ".$action_thread." su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirla</a>.";
								
				if($existing_employee != 0) {
									
					$mailimprev = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".$existing_employee."'");
					$nomeimprev = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = '".$existing_employee."'");
									
					$msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione dell'azione numero ".Tools::getValue('id_action')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect e l'ha passata a ".Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.Tools::getValue('assegnaimpiegatoa').'');
									
					$params = array(
					'{link}' => $linkimp,
					'{firma}' => '',
					'{msg}' => $msgimprev);
					
					if($existing_employee != $incaricato)
					{			
						Mail::Send(5, 'msg_base', Mail::l('Gestione azione revocata', 5), 
							$params, $mailimprev, NULL, NULL, NULL, NULL, NULL, 
						_PS_MAIL_DIR_, true);
					}		
				}
								
				else {
								
				}
								
				$params = array(
				'{link}' => $linkimp,
				'{firma}' => '',
				'{msg}' => $msgimp);
								
				if($incaricato != $cookie->id_employee) {
								
					Mail::Send(5, 'msg_base', Mail::l('Azione assegnata a te su Ezdirect', 5), 
						$params, $mailimp, NULL, NULL, NULL, NULL, NULL, 
						_PS_MAIL_DIR_, true);
												
				} else { }
			}
			else
			{
				Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd) VALUES ('$action_thread', 'Attivita', '".$order->id_customer."', '".addslashes($action_subject)."', ".$cookie->id_employee.", ".$incaricato.", 'open', 'O".$order->id."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");   
			}
			Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', ".$order->id_customer.", ".$cookie->id_employee.", ".$incaricato.", ".$incaricato.", '', '".addslashes($messaggio_verifica)."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
			
			//mando mail
			$tokenbarbara = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."2");
			$linkbarbara = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenbarbara.'';
			$tokenmatteo = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."7");
			$linkmatteo = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenmatteo.'';
			
			$tokenincarico = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders')).$incaricato);
			$linkincarico = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenincarico.'';
			$mail_incarico = Db::getInstance()->getValue('SELECT email FROM employee WHERE id_employee = '.$incaricato);
			
			$nome_incarico =  Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$incaricato);
			
			$testomail = "Verifica tecnica su ordine n. ".$order->id.". <br /><br />
			Messaggio: ".$messaggio_verifica."<br />
			Clicca sul link per entrare nell'ordine:<br />
			Barbara - <a href='".$linkbarbara."'>clicca qui</a><br />
			Matteo - <a href='".$linkmatteo."'>clicca qui</a><br />
			".($incaricato == 2 || $incaricato == 7 ? '' : $nome_incarico." - <a href='".$linkincarico."'>clicca qui</a><br />")."";
			
			
			$params = array('{reply}' => $testomail);
			Mail::Send(5, 'action', Mail::l('Ordine con verifica tecnica', 5), $params, "barbara.giorgini@ezdirect.it", NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true);
			Mail::Send(5, 'action', Mail::l('Ordine con verifica tecnica', 5), $params, "matteo.delgiudice@ezdirect.it", NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true);
			if($incaricato != 2 && $incaricato != 7)
				Mail::Send(5, 'action', Mail::l('Ordine con verifica tecnica', 5), $params, $mail_incarico, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true);

		}
		
		if($newOrderStatusId == 25)
		{
			
			//mando mail
			$tokenbarbara = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."2");
			$linkbarbara = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenbarbara.'';
			$tokenmatteo = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."7");
			$linkmatteo = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenmatteo.'';
			$actionverifica = Db::getInstance()->getValue('SELECT id_action FROM action_thread WHERE subject = "*** VERIFICA TECNICA SU ORDINE N. '.$order->id.' "');
			Db::getInstance()->getValue('UPDATE action_thread SET status = "closed" WHERE id_action = '.$actionverifica); 
			
			$messaggiotodo = Db::getInstance()->getValue('SELECT action_message FROM action_message WHERE id_action = '.$actionverifica.' ORDER BY id_action_message DESC');
			
			$testomail = "Verifica conclusa su ordine n. ".$order->id.". <br /><br />
			Ultimo messaggio del TODO associato alla verifica: ".$messaggiotodo."<br /><br />
			Clicca sul link per entrare nell'ordine:<br />
			Barbara - <a href='".$linkbarbara."'>clicca qui</a><br />
			Matteo - <a href='".$linkmatteo."'>clicca qui</a><br />";
			
			$params = array('{reply}' => $testomail);
			Mail::Send(5, 'action', Mail::l('Verifica conclusa su ordine '.$order->id, 5), $params, "barbara.giorgini@ezdirect.it", NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true);
			Mail::Send(5, 'action', Mail::l('Verifica conclusa su ordine '.$order->id, 5), $params, "matteo.delgiudice@ezdirect.it", NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true);
		}
		
		if($newOrderStatusId == 6)
		{
			$headers  = 'MIME-Version: 1.0' . "\n";
			$headers .= 'Content-Type: text/html' ."\n";
			$headers .= 'From: "Ezdirect" <info@ezdirect.it>' . "\n";

			$dati_cliente_od = Db::getInstance()->getRow('SELECT * FROM customer WHERE id_customer = '.$order->id_customer);

			if($dati_cliente_od['is_company'] == 1)
				$cli_od = $dati_cliente_od['company']. '(PI: '.$dati_cliente_od['vat_number'].')';
			else
				$cli_od = $dati_cliente_od['firstname'].' '.$dati_cliente_od['lastname']. '(CF: '.$dati_cliente_od['tax_code'].')';
			
			$messaggio_annullato = '<strong>Annullato ordine '.$id_order.'.<br /><br />Cliente: '.$cli_od.'<br /> Prodotti contenuti:</strong><br />
			';
			
			$products = $order->getProducts();
			foreach($products as $product)
				$messaggio_annullato .= $product['product_name'].' ('.$product['product_reference'].') - Qt: '.$product['product_quantity'].'<br />';
				
			$tokenMatteo = Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).'7');
			$tokenBarbara = Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).'2');
			
			$link_matteo = '<br /><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order='.$id_order.'&vieworder&token='.$tokenMatteo.'">Clicca qui per entrare nell\'ordine</a>';
			$link_barbara = '<br /><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order='.$id_order.'&vieworder&token='.$tokenBarbara.'">Clicca qui per entrare nell\'ordine</a>';
			
			
			//mail('posta@federicogiannini.com','ANNULLATO ORDINE '.$id_order,$messaggio_annullato.$link_barbara,$headers );
			mail('barbara.giorgini@ezdirect.it','ANNULLATO ORDINE '.$id_order,$messaggio_annullato.$link_barbara,$headers );
			mail('matteo.delgiudice@ezdirect.it','ANNULLATO ORDINE '.$id_order,$messaggio_annullato.$link_matteo,$headers );
		}
		
		
		
		$carrier = new Carrier((int)($order->id_carrier), (int)($order->id_lang));
		$templateVars = array();
		if ($history->id_order_state == Configuration::get('PS_OS_SHIPPING') AND $order->shipping_number)
			$templateVars = array('{followup}' => str_replace('@', $order->shipping_number, $carrier->url));
		elseif ($history->id_order_state == Configuration::get('PS_OS_CHEQUE'))
			$templateVars = array(
				'{cheque_name}' => (Configuration::get('CHEQUE_NAME') ? Configuration::get('CHEQUE_NAME') : ''),
				'{cheque_address_html}' => (Configuration::get('CHEQUE_ADDRESS') ? nl2br(Configuration::get('CHEQUE_ADDRESS')) : ''));
		elseif ($history->id_order_state == Configuration::get('PS_OS_BANKWIRE'))
			$templateVars = array(
				'{bankwire_owner}' => (Configuration::get('BANK_WIRE_OWNER') ? Configuration::get('BANK_WIRE_OWNER') : ''),
				'{bankwire_details}' => (Configuration::get('BANK_WIRE_DETAILS') ? nl2br(Configuration::get('BANK_WIRE_DETAILS')) : ''),
				'{bankwire_address}' => (Configuration::get('BANK_WIRE_ADDRESS') ? nl2br(Configuration::get('BANK_WIRE_ADDRESS')) : ''));
		
		if ($history->addWithemail(true, $templateVars))
		{
			
			if($stato_attuale != $stato_nuovo)
			{
				if( $newOrderStatusId == 4 || $newOrderStatusId == 5 || $newOrderStatusId == 6 || $newOrderStatusId == 7 || $newOrderStatusId == 8 || $newOrderStatusId == 14 || $newOrderStatusId == 15 || $newOrderStatusId == 16 || $newOrderStatusId == 20 || $newOrderStatusId == 29)
				{
					
				}
				else
				{
					Product::RecuperaCSV($id_order);
				}
			}
				
		}
		

		if($newOrderStatusId == 4 || $newOrderStatusId == 5 || $newOrderStatusId == 14 || $newOrderStatusId == 15 || $newOrderStatusId == 16 || $newOrderStatusId == 20)
		{
			$history_prec = Db::getInstance()->getValue('SELECT id_order_history FROM order_history WHERE (id_order_state = 4 OR id_order_state = 5  OR id_order_state = 14  OR id_order_state = 15  OR id_order_state = 16  OR id_order_state = 20) AND id_order = '.$order->id);
			
			
			if($modalita_esolver == 0 && (!$history_prec || $history_prec < 0 || $history_prec == ''))
			{
				// SCARICO MAGAZZINO
				
				$prime = Db::getInstance()->getValue('SELECT id_cart FROM orders WHERE id_order = '.$order->id.' AND id_cart IN (SELECT id_cart FROM cart WHERE name LIKE "%AMAZON PRIME%")');
				
				if($prime > 0)
				{
					
				}
				else
				{
					$products = $order->getProducts();
					foreach($products as $product)
					{
						$stock = Db::getInstance()->getRow('SELECT quantity, stock_quantity, itancia_quantity, esprinet_quantity, techdata_quantity, intracom_quantity, supplier_quantity FROM product WHERE id_product = '.$product['product_id']);
						
						if($newOrderStatusId != 15)
							$stock_quantity = $stock['stock_quantity'] -= $product['product_quantity'];
						else
						{
							$cons = Db::getInstance()->getValue('SELECT cons FROM order_detail WHERE id_order = '.$order->id.' AND product_id = '.$product['product_id']);
							$cons = str_replace('0_','',$cons);
							$qt_da_sottrarre = $cons;
							$stock_quantity = $stock['stock_quantity'] -= $qt_da_sottrarre;
						}
						
						$qtsito = ($stock['supplier_quantity'] + $stock_quantity + $stock['esprinet_quantity'] + $stock['itancia_quantity'] + $stock['techdata_quantity'] + $stock['intracom_quantity']);
						
						Db::getInstance()->executeS("UPDATE product SET stock_quantity = $stock_quantity, quantity = $qtsito WHERE id_product = ".$product['product_id']."");

					}
				}	
			}
		}	
					
					
				
			/*if(isset($_POST['prvcustomer'])) {
				$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
				Tools::redirectAdmin('index.php?tab=AdminCustomers&id_customer='.$_POST['numprvcustomer'].'viewcustomer&id_order='.Tools::getValue('id_order').'&vieworder&tab-container-1=4&token='.$tokenCustomers);
		
			
			}*/
	die('ok');	
}
		

if(Tools::getIsset('vedi_tutte_ultime_cose')) { 
	
	
			if(isset($_POST['tabcontainer1']) && $_POST['tabcontainer1'] == 1 || !isset($_POST['tabcontainer1'])) {

			$sql_ultime_cose = ("SELECT * FROM (SELECT id_thread as id, tipo_richiesta as tipo, tipo_richiesta as tipo_attivita, id_customer as cliente, id_employee as in_carico, status, (CASE WHEN((status = 'open' OR status = 'pending1') AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM form_prevendita_thread WHERE id_customer = ".$_POST['id_customer']." UNION 
			
				SELECT * FROM (SELECT id_customer_thread as id, ct.id_contact as tipo, cl.name as tipo_attivita, id_customer as cliente, id_employee as in_carico, status, (CASE WHEN((status = 'open' OR status = 'pending1') AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM customer_thread ct JOIN (SELECT * FROM contact_lang WHERE id_lang = 5) cl WHERE id_customer = ".$_POST['id_customer']." GROUP BY ct.id_customer_thread) ccc UNION 
				
				SELECT id_order as id, 'Ordine' as tipo, 'Ordine' as tipo_attivita, id_customer as cliente, '' as in_carico, (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) AS status, (CASE WHEN(((SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 4 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 5 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 14 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 15 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 16 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 20) AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM orders WHERE id_customer = ".$_POST['id_customer']." UNION 
				
				SELECT id_action as id, action_type as tipo, action_type as tipo_attivita, id_customer as cliente, action_to as in_carico, status, (CASE WHEN((status = 'open' OR status = 'pending1') AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM action_thread WHERE id_customer = ".$_POST['id_customer']." UNION 
				
				SELECT id_cart as id, 'Carrello' as tipo, 'Carrello' as tipo_attivita, id_customer as cliente, in_carico_a as in_carico, visualizzato AS status, '' as scaduto, date_add, date_upd FROM cart WHERE id_cart NOT IN (SELECT id_cart FROM orders) AND name LIKE '%amazon%' AND id_customer = ".$_POST['id_customer']." UNION
				
				SELECT id_cart as id, 'Carrello' as tipo, 'Carrello' as tipo_attivita, id_customer as cliente, in_carico_a as in_carico, visualizzato AS status, '' as scaduto, date_add, date_upd FROM carrelli_creati WHERE id_cart NOT IN (SELECT id_cart FROM orders) AND id_customer = ".$_POST['id_customer']." ORDER BY date_add DESC) x ORDER BY x.date_upd DESC ".(Tools::getIsset('ultime') && Tools::getValue('ultime') == 'vedi_tutte' ? '' : 'LIMIT 15')."");
				
				}
			else
			{
				$sql_ultime_cose = ("SELECT * FROM (
				
				SELECT id_order as id, 'Ordine' as tipo, 'Ordine' as tipo_attivita, id_customer as cliente, '' as in_carico, (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) AS status, (CASE WHEN(((SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 4 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 5 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 14 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 15 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 16 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 20) AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM orders WHERE id_customer = ".$_POST['id_customer']." UNION 
				
				
				SELECT id_cart as id, 'Carrello' as tipo, 'Carrello' as tipo_attivita, id_customer as cliente, in_carico_a as in_carico, visualizzato AS status, '' as scaduto, date_add, date_upd FROM carrelli_creati WHERE id_customer = ".$_POST['id_customer']." AND id_cart NOT IN (SELECT id_cart FROM orders) ORDER BY date_add DESC) x ORDER BY x.date_upd DESC ".(Tools::getIsset('ultime') && Tools::getValue('ultime') == 'vedi_tutte' ? '' : 'LIMIT 15')."");
				
			}

			$ultime_cose = Db::getInstance()->executeS($sql_ultime_cose);

			$result .=  "<thead><tr><th style='width:100px; max-width:100px'>Tipo 
			<a href='index.php?tab=AdminCustomers&viewcustomer&id_customer=".$_POST['id_customer']."&orderby=tipo_attivita&orderway=asc&token=".$_POST['token']."&tab-container-1=1'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
				<a href='index.php?tab=AdminCustomers&viewcustomer&id_customer=".$_POST['id_customer']."&orderby=tipo_attivita&orderway=desc&token=".$_POST['token']."&tab-container-1=1'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a>
			</th><th style='width:70px; max-width:70px'>ID</th><th style='width:70px; max-width:70px'>Creato da</th><th  style='width:70px; max-width:70px'>In carico a</th><th style='width:30px; max-width:30px'>Conv.</th><th style='max-width:150px'>Status</th><th style='max-width:100px'>Oggetto</th><th style='width:70px; max-width:70px'>Totale</th><th  data-sorter='shortDate' data-date-format='ddmmyyyy'>Data apertura</th><th data-sorter='shortDate' data-date-format='ddmmyyyy'>Ultima modifica</th></tr></thead><tbody>";
			
			foreach($ultime_cose as $ultima_cosa) {
				if ($ultima_cosa['tipo'] == 7){
					$ctrl_mex = Db::getInstance()->getValue("SELECT COUNT(id_customer_message) FROM customer_thread ct JOIN customer_message cm ON ct.id_customer_thread = cm.id_customer_thread WHERE cm.id_employee = 0 AND ct.id_customer_thread = ".$ultima_cosa['id']."");	
						
					if($ctrl_mex >= 1) {
						
					} else {
						$ultima_cosa['status'] = ''; $ultima_cosa['scaduto'] = '';
					}

				}
					
				if($i<5) {
					$includilo = 'yes';
				}
				else {
					if($ultima_cosa['scaduto'] == 'SCADUTO') {
						$includilo = 'yes';
					}
					else {
						$includilo = 'no';
					}
				
				}
				$conv = "--";
				if($includilo == 'yes') {
					$in_carico = $ultima_cosa['in_carico'];
					
					if(is_numeric($in_carico)) {
						$newinc = new Employee($in_carico);
						$in_carico = $newinc->firstname." ".substr($newinc->lastname, 0,1).".";
					}
					
					$tipo = $ultima_cosa['tipo'];
					if ($tipo == 4){
						$sigla = "T"; $tipo = "Ticket assistenza"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$_POST['token'].'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
					}
					else if ($tipo == 2){
						$sigla = "A"; $tipo = "Ticket contabilita"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$_POST['token'].'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
					} 
					else if ($tipo == 3){
						$sigla = "V"; $tipo = "Ticket rivenditori"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$_POST['token'].'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
					} 
					else if ($tipo == 7){
						$sigla = "M"; $tipo = "Messaggio"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewmessage&id_mex='.$ultima_cosa['id'].'&viewcustomer&token='.$_POST['token'].'&tab-container-1=7">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
						
						$ctrl_mex = Db::getInstance()->getValue("SELECT COUNT(id_customer_message) FROM customer_thread ct JOIN customer_message cm ON ct.id_customer_thread = cm.id_customer_thread WHERE cm.id_employee = 0 AND ct.id_customer_thread = ".$ultima_cosa['id']."");	
						
						if($ctrl_mex >= 1) {
							$status = ''; $scaduto = '';
						}
					}
					else if ($tipo == 8){
						$sigla = "D"; $tipo = "Ticket ordini"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$_POST['token'].'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
					} 
					else if ($tipo == 9){
						$sigla = "S"; $tipo = "RMA"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$_POST['token'].'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
					} 
					else if ($tipo == 'tirichiamiamonoi') {	
						$sigla = "R"; $tipo = "Ti richiamiamo noi"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewpreventivo&id_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$_POST['token'].'&tab-container-1=7">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM form_prevendita_message WHERE id_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
					}
					else if ($tipo == 'preventivo') {	
						$sigla = "P"; $tipo = "Preventivo (ric.)"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewpreventivo&id_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$_POST['token'].'&tab-container-1=7">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM form_prevendita_message WHERE id_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
					}
					else {
						if($tipo == 'Carrello') {
							$ultima_mod_imp = Db::getInstance()->getValue('SELECT date_upd FROM carrelli_creati WHERE id_cart = '.$ultima_cosa['id']);
							/*if($ultima_mod_imp == '' || !$ultima_mod_imp)
								$ultima_mod_imp = $ultima_cosa['date_upd'];*/
							
							$ultima_cosa['date_upd'] = $ultima_mod_imp;
			
							$revisioni = Db::getInstance()->getValue("SELECT revisioni FROM cart WHERE id_cart = ".$ultima_cosa['id']."");
							$ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcart&id_cart='.$ultima_cosa['id'].'&viewcustomer&token='.$_POST['token'].'&tab-container-1=4">'; $creato_da = Db::getInstance()->getValue("SELECT created_by FROM cart WHERE id_cart = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
							$in_carico = $ultima_cosa['in_carico'];
							
							
							$n_ord_c = Db::getInstance()->getValue("SELECT id_order FROM orders WHERE id_cart = ".$ultima_cosa['id']."");
							if($n_ord_c > 0) 
							{
								$conv = "<img src='https://www.ezdirect.it/img/admin/enabled.gif' alt='Si' title='Si' />";
							}
							else
							{
								$conv = "<img src='https://www.ezdirect.it/img/admin/red_no.gif' alt='No' title='No' />";
								
							}
					
							if(is_numeric($in_carico)) {
								$newinc = new Employee($in_carico);
								$in_carico = $newinc->firstname." ".substr($newinc->lastname, 0,1).".";
							}
						}
						else if ($tipo == 'Ordine') {
							
							$ord_fatt = Db::getInstance()->getRow('SELECT id_fattura, tipo FROM fattura FORCE INDEX (PRIMARY) WHERE rif_ordine = '.$ultima_cosa['id']);
		
							if($ord_fatt['id_fattura'] > 0)
								$link_fatt = '<a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.$_POST['token'].'&id_fattura='.$ord_fatt['id_fattura'].'&tab-container-1=5&viewfattura&tipo='.$ord_fatt['tipo'].'&updatefattura" target="_blank"><img src="../img/admin/enabled.gif" /> Fatt. </a>';
							else
								$link_fatt = '';
							
							$ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&vieworder&id_order='.$ultima_cosa['id'].'&viewcustomer&token='.$_POST['token'].'&tab-container-1=4">'; $creato_da = "";
						}
						else if ($tipo != 'Ordine' && $tipo != 'Carrello' && tipo != '') {
							$ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewaction&id_action='.$ultima_cosa['id'].'&viewcustomer&token='.$_POST['token'].'&tab-container-1=10">'; $creato_da = Db::getInstance()->getValue("SELECT action_from FROM action_thread WHERE id_action = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
						}
						else {
							$ahrefcosa = '<a href="#">';
						}
						
						$sigla = "";
					}
					
					$anno = substr($ultima_cosa['date_upd'],2,2);
					
					if($sigla != '') {
						$id_cosa = $sigla.$anno.$ultima_cosa['id'];
					}
					else {
						$id_cosa = $ultima_cosa['id'];
					}
					
					$status = $ultima_cosa['status']; 
					if($status == 'closed') {
						$status = '<img src="../img/admin/status_green.gif" alt="Chiuso" /> Chiuso';
					}
					else if($status == 'pending1' || $status == 'pending2') {
						$status = '<img src="../img/admin/status_orange.gif" alt="In lavorazione" />In lavorazione';
					}
					else if($status == 'open') {
						$status = '<img src="../img/admin/status_red.gif" alt="Aperto" />Aperto';
					}
					else if(is_numeric($status) && $tipo != 'Carrello') {
						
						if($status == 25 || $status == 24 || $status == 27 || $status == 28)
						{
							$status_lav = Db::getInstance()->getValue("SELECT name FROM order_state_lang WHERE id_lang = 5 AND id_order_state = ".$status."");
							
							$status = Db::getInstance()->getValue("SELECT name FROM order_state_lang WHERE id_lang = 5 AND id_order_state = ".Db::getInstance()->getValue('SELECT id_order_state FROM order_history WHERE id_order_history = '.Db::getInstance()->getValue('SELECT MAX(id_order_history) FROM order_history WHERE id_order = '.$ultima_cosa['id'].' AND id_order_state != 24 AND id_order_state != 25 AND id_order_state != 27 AND id_order_state != 28')));
							
							$status .= ' ('.$status_lav.')';
						}
						else
							$status = Db::getInstance()->getValue("SELECT name FROM order_state_lang WHERE id_lang = 5 AND id_order_state = ".$status."");
						
					}
					else if(is_numeric($status) && $tipo == 'Carrello') {
						$numero_notifiche =  Db::getInstance()->getValue("SELECT numero_notifiche FROM cart WHERE id_cart = ".$ultima_cosa['id']);
						$status_s = ($status == 1 ? '<img src="../img/admin/enabled.gif" alt="Letto" title="Letto" />Letto' : '<img src="../img/admin/red_no.gif" alt="Non letto" title="Non letto" />Non letto');
						
						if($numero_notifiche > 0)
							$status_s .= '&nbsp;&nbsp;&nbsp;<img src="../img/admin/email-sent.gif" alt="Carrello notificato al cliente" title="Carrello notificato al cliente" />';
						
						$status = $status_s;
						$preventivo = Db::getInstance()->getValue("SELECT preventivo FROM cart WHERE id_cart = ".$ultima_cosa['id']."");
						if($preventivo == 0)
							$status = '--';
						else
							$tipo = 'C.Preventivo';
					}
					
					if($tipo == 'Carrello' || $tipo == 'Preventivo' || $tipo == 'C.Preventivo') {
						$oggetto_uc = Db::getInstance()->getValue("SELECT name FROM cart WHERE id_cart = ".$ultima_cosa['id']."");
						$cartID = new Cart((int)($ultima_cosa['id']));
						$importo_uc = Tools::displayPrice($cartID->getOrderTotal(false),1);
						$is_order = Db::getInstance()->getValue("SELECT id_cart FROM orders WHERE id_cart = ".$ultima_cosa['id']."");
						if($is_order > 0) 
						{
							$rowOrder =  Db::getInstance()->getRow('SELECT * FROM orders WHERE id_cart = '.$ultima_cosa['id']);
							$importo_uc = Tools::displayPrice(($rowOrder['total_products']+($rowOrder['total_shipping']/(($rowOrder['carrier_tax_rate']/100)+1))), 1);
						}
						else
						{
							$importo_uc = Tools::displayPrice($cartID->getOrderTotal(false),1);
						}
						$id_cosa = $id_cosa."-".$revisioni;
					}
					else if($tipo == 'Ordine') {
						$rowOrder =  Db::getInstance()->getRow('SELECT * FROM orders WHERE id_order = '.$ultima_cosa['id']);
						$oggetto_uc = "";

						if($cookie->id_employee == 2)
							$importo_uc = Tools::displayPrice($rowOrder['total_paid_real']);
						else
							$importo_uc = Tools::displayPrice(($rowOrder['total_products']+($rowOrder['total_shipping']/(($rowOrder['carrier_tax_rate']/100)+1))), 1);
					}
					else {
						$oggetto_uc = "";
						$importo_uc = "";
					}
					
					
					$result .=  "<tr id='cart_riga_".$id_cosa."' class='riga'>";
					if($tipo == 'Carrello' || $tipo == 'C.Preventivo')
					{
					$result .=  '<script type="text/javascript">
						$(document).ready(function(){
							$("#espandi_cart_'.$id_cosa.', #td_cart_espandi_'.$id_cosa.'").click(function(){
								
								if($(this).attr("src") == ("../img/admin/add.gif")) {
									$(this).attr("src", "../img/admin/forbbiden.gif");
									$.ajax({
									  url:"ajax.php?getCartDisplay=y",
									  type: "POST",
									  data: {
									  id_cart: '.substr($id_cosa, 0, strpos($id_cosa, '-')).'
									  },
									  success:function(r){
										'.(count($ultime_cose) > 3 ? '$("#ultime-container").height($("#ultime-container").height()+250);' : '').'
										$("#cart_riga_show_'.$id_cosa.'_td").html(r);
										$("#cart_riga_show_'.$id_cosa.'").show();
										/*$(".riga").show();*/
										/*$("#orderTempDisplay").html("");*/
										/*$("#cartTempDisplay").html(r);*/
										/*$("#cart_riga_'.$id_cosa.'").toggle();*/
									  },
									  error: function(xhr,stato,errori){
										 alert("Errore durante l\'operazione:"+xhr.status);
									  }
									});
								}
								else {
									$(this).attr("src", "../img/admin/add.gif");
									$("#cart_riga_show_'.$id_cosa.'").hide();
									'.(count($ultime_cose) > 3 ? '$("#ultime-container").height(200);' : '').'
									/*$("#cartTempDisplay").html("");*/
								}
								return false;
							});
							
						});
						
						
					</script>';
					}
					else if($tipo == 'Ordine')
					{
						$result .=  '<script type="text/javascript">
						$(document).ready(function(){
							$("#espandi_cart_'.$id_cosa.', #td_cart_espandi_'.$id_cosa.'").click(function(){
								
								if($(this).attr("src") == ("../img/admin/add.gif")) {
									$(this).attr("src", "../img/admin/forbbiden.gif");
									$.ajax({
									  url:"ajax.php?getOrderDisplay=y&ultime=y",
									  type: "POST",
									  data: {
									  id_order: '.$id_cosa.'
									  },
									  success:function(r){
										'.(count($ultime_cose) > 3 ? '$("#ultime-container").height($("#ultime-container").height()+250);' : '').'
										$("#cart_riga_show_'.$id_cosa.'_td").html(r);
										$("#cart_riga_show_'.$id_cosa.'").show();
										/*$(".riga").show();*/
										/*$("#cartTempDisplay").html("");*/
										/*$("#orderTempDisplay").html(r);*/
										/*$("#cart_riga_'.$id_cosa.'").toggle();*/
									  },
									  error: function(xhr,stato,errori){
										 alert("Errore durante l\'operazione:"+xhr.status);
									  }
									});
								}
								else {
									$(this).attr("src", "../img/admin/add.gif");
									'.(count($ultime_cose) > 3 ? '$("#ultime-container").height(200);' : '').'
									$("#cart_riga_show_'.$id_cosa.'").hide();
									/*$("#orderTempDisplay").html("");*/
								}
								return false;
							});
							
						});
						
						
					</script>';
						
					}
					$result .=  "<td>".($tipo == 'Carrello' || $tipo == 'C.Preventivo' || $tipo == 'Ordine' ? '<img src="../img/admin/add.gif" alt="Espandi" style="cursor:pointer" title="Espandi" id="espandi_cart_'.$id_cosa.'" /> ' : '').$ahrefcosa.$tipo."</a> ".($tipo == 'Ordine' ? $link_fatt : '')."</td>";
					$result .=  "<td>".$ahrefcosa.$id_cosa."</a></td>";
					$result .=  "<td>".$ahrefcosa.$creato_da."</a></td>";
					$result .=  "<td>".$ahrefcosa.$in_carico."</a></td>";
					$result .=  "<td>".$conv."</td>";
					$result .=  "<td>".$ahrefcosa.$status."</a></td>";
					$result .=  "<td style='max-width:100px'>".$ahrefcosa.(strlen($oggetto_uc) > 13 ? substr($oggetto_uc,0,13).'...' : $oggetto_uc)."</a></td>";
					$result .=  "<td style='text-align:right'>".$ahrefcosa.$importo_uc."</a></td>";
					$result .=  "<td>".$ahrefcosa.Tools::DisplayDate($ultima_cosa['date_add'],$cookie->id_lang,false)."</a></td>";
					$result .=  "<td>".$ahrefcosa.Tools::DisplayDate($ultima_cosa['date_upd'],$cookie->id_lang,false)."</a></td>";
					$result .=  "</tr>";
					if($tipo == 'Carrello' || $tipo == 'C.Preventivo' || $tipo == 'Ordine')
					{
						$result .=  '
						<tr id="cart_riga_show_'.$id_cosa.'" class="invisible-table-row subs sub_'.$id_cosa.'" style="display:none"><td id="cart_riga_show_'.$id_cosa.'_td" colspan="11"></td></tr>
						';
					}
				}
				else {
				}
			}
		
			//$result .=  "</tbody></table></div><br /><div id='cartTempDisplay'></div><div id='orderTempDisplay'></div><p style='text-align:center'>".(Tools::getIsset('ultime') && Tools::getValue('ultime') == "vedi_tutte" ? "<a class='button' href='https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$customer->id."&viewcustomer&token=".$this->token."&tab-container-1=1'>Vedi solo ultime 15</a>" : "<a class='button' href='https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$customer->id."&viewcustomer&token=".$this->token."&tab-container-1=1&ultime=vedi_tutte'>Vedi tutte</a>")."</p><hr />";
			
			
			$result .=  "</tbody>";
			die($result);
}