<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14632 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class SpecialiAcquisto extends AdminTab
{
		public function display()
		{	
			global $cookie, $currentIndex;
			echo "<h1>Tutti i prezzi speciali d'acquisto</h1>";
			echo "<script type='text/javascript' src='../js/price.js'></script>";
			
			echo "Clicca sulla matita per apportare modifiche. Spunta il flag per cancellare. Tutte le modifiche e le cancellazioni saranno effettuate esclusivamente dopo la conferma cliccando sull'apposito pulsante. <strong>NB: se il prezzo &egrave; bordato in rosso, significa che &egrave; terminata la quantit&agrave; disponibile a prezzo speciale oppure che la quantit&grave; non &egrave; stata inserita, o ancora che l'offerta &egrave; scaduta.</strong>";
			
			if(isset($_POST['conferma_tutti']))
			{
			
				if(isset($_GET['deletesp'])) {
					
					Db::getInstance()->executeS("DELETE FROM specific_price_wholesale WHERE id_specific_price = '".$_GET['id_specific_price']."'");
					echo "<div class='conf'>Prezzo speciale d'acquisto cancellato con successo</div>";
				
				}
				if(isset($_GET['edit_price_conf'])) {

					for($i = 0; $i<count($_POST['sp_from']); $i++) {
						if($_POST['sp_from'][$i] != "01-01-1970") {
							$date_from_array = explode("-",$_POST['sp_from'][$i]);
							$date_from = $date_from_array[2]."-".$date_from_array[1]."-".$date_from_array[0]." 00:00:00";
						}
						else {
							$date_from = "0000-00-00 00:00:00";
						}
							
						if($_POST['sp_to'][$i] != "01-01-1970") {
							$date_to_array = explode("-",$_POST['sp_to'][$i]);
							$date_to = $date_to_array[2]."-".$date_to_array[1]."-".$date_to_array[0]." 23:59:59";
						}
						else {
							$date_to = "0000-00-00 00:00:00";
						}
							
							
						Db::getInstance()->executeS("UPDATE specific_price_wholesale sp SET sp.reduction_1 = '".str_replace(",",".",$_POST['reduction1'][$i])."', sp.reduction_2 = '".str_replace(",",".",$_POST['reduction2'][$i])."', sp.reduction_3 = '".str_replace(",",".",$_POST['reduction3'][$i])."', sp.wholesale_price = '".str_replace(",",".",$_POST['new_special_price'][$i])."', sp.from = '".$date_from."', sp.to = '".$date_to."' WHERE sp.id_specific_price = '".$_POST['id_specific_price'][$i]."'");
					}
						echo "<div class='conf'>Prezzo speciale d'acquisto modificato con successo</div>";
					
				}
				
				else if(isset($_GET['edit_price_conf_singolo'])) {
				
					if($_POST['sp_from'][0] != "01-01-1970") {
						$date_from_array = explode("-",$_POST['sp_from'][0]);
						$date_from = $date_from_array[2]."-".$date_from_array[1]."-".$date_from_array[0]." 00:00:00";
					}
					else {
						$date_from = "0000-00-00 00:00:00";
					}
						
					if($_POST['sp_to'][0] != "01-01-1970") {
						$date_to_array = explode("-",$_POST['sp_to'][0]);
						$date_to = $date_to_array[2]."-".$date_to_array[1]."-".$date_to_array[0]." 23:59:59";
					}
					else {
						$date_to = "0000-00-00 00:00:00";
					}
						
					Db::getInstance()->executeS("UPDATE specific_price_wholesale sp SET sp.reduction_1 = '".str_replace(",",".",$_POST['reduction1'][0])."', sp.reduction_2 = '".str_replace(",",".",$_POST['reduction2'][0])."', sp.reduction_3 = '".str_replace(",",".",$_POST['reduction3'][0])."', sp.wholesale_price = '".str_replace(",",".",$_POST['new_special_price'][0])."', sp.from = '".$date_from."', sp.to = '".$date_to."' WHERE sp.id_specific_price = '".$_POST['id_specific_price'][0]."'");
					
					echo "<div class='conf'>Prezzo speciale d'acquisto modificato con successo</div>";
					
				}
			}
			else if(isset($_POST['cancella']))
			{
				foreach($_POST['delete_sp'] as $del) 
				{
					if(isset($del))
						Db::getInstance()->executeS("DELETE FROM specific_price_wholesale WHERE id_specific_price = '".$del."'");
				}
				echo "<div class='conf'>Prezzi cancellati con successo</div>";
			
			}
			else if(isset($_POST['allunga']))
			{
				foreach($_POST['delete_sp'] as $del) 
					{
						if(isset($del))
						{
							$sp_to = Db::getInstance()->getValue("SELECT sp.to FROM specific_price_wholesale sp  WHERE sp.id_specific_price = '".$del."'");
							$new_date = (date("Y-m-t 23:59:59"));
							// $new_date = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s'). ' + 30 days'));
							Db::getInstance()->executeS("UPDATE specific_price_wholesale sp SET sp.to = '".$new_date."' WHERE sp.id_specific_price = '".$del."'");
							
						}
					}
				echo "<div class='conf'>Prezzi allungati con successo</div>";
			}
			else if(isset($_POST['allunga_successivo']))
			{
				foreach($_POST['delete_sp'] as $del) 
					{
						if(isset($del))
						{
							$sp_to = Db::getInstance()->getValue("SELECT sp.to FROM specific_price sp  WHERE sp.id_specific_price = '".$del."'");
							$new_date = date('Y-m-t 23:59:59', strtotime('first day of next month'));
							// $new_date = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s'). ' + 30 days'));
							Db::getInstance()->executeS("UPDATE specific_price_wholesale sp SET sp.to = '".$new_date."' WHERE sp.id_specific_price = '".$del."'");
							
						}
					}
				echo "<div class='conf'>Prezzi allungati con successo</div>";
			}
			
			$speciali = Db::getInstance()->executeS("SELECT sp.*, p.id_product AS id_prodotto, pl.name AS nome_prodotto, m.name AS costruttore, p.reference, p.stock_quantity FROM specific_price_wholesale sp JOIN product p ON sp.id_product = p.id_product JOIN product_lang pl ON pl.id_product = sp.id_product JOIN manufacturer m ON p.id_manufacturer = m.id_manufacturer WHERE pl.id_lang = 5 ORDER BY ".(isset($_GET['orderby']) ? $_GET['orderby'] : 'sp.id_specific_price')." ".(isset($_GET['orderway']) ? $_GET['orderway'] : 'ASC')."");
			
			include_once('functions.php');
			
			echo '
			<style type="text/css">
				tr.tr-red-border td {
				border:1px solid red;
				}
			</style>
			
			<script type="text/javascript">
				function activateEditForm(special_id, special_price, sp_from, sp_to) {
				
					
					document.getElementById("new_special_price_"+special_id).style.display = "none";
					document.getElementById("new_special_price_2_"+special_id).style.display = "block";
					
					document.getElementById("reduction1_"+special_id).style.display = "none";
					document.getElementById("reduction1_2_"+special_id).style.display = "block";
					
					document.getElementById("reduction2_"+special_id).style.display = "none";
					document.getElementById("reduction2_2_"+special_id).style.display = "block";
					
					document.getElementById("reduction3_"+special_id).style.display = "none";
					document.getElementById("reduction3_2_"+special_id).style.display = "block";
					
					document.getElementById("new_special_price_"+special_id).style.display = "none";
					document.getElementById("new_special_price_2_"+special_id).style.display = "block";
					
					document.getElementById("sp_from_"+special_id).style.display = "none";
					document.getElementById("sp_from_2_"+special_id).style.display = "block";
					
					document.getElementById("sp_to_"+special_id).style.display = "none";
					document.getElementById("sp_to_2_"+special_id).style.display = "block";
					
					document.getElementById("azioni_"+special_id).style.display = "none";
					document.getElementById("chiudilo_"+special_id).style.display = "block";
					document.getElementById("azioni_2_"+special_id).style.display = "block";
					
				}
				
				function chiudiForm(special_id, special_price, sp_from, sp_to) {
				
					
					document.getElementById("new_special_price_"+special_id).style.display = "block";
					document.getElementById("new_special_price_2_"+special_id).style.display = "none";
					
					document.getElementById("reduction1_"+special_id).style.display = "block";
					document.getElementById("reduction1_2_"+special_id).style.display = "none";
					
					document.getElementById("reduction2_"+special_id).style.display = "block";
					document.getElementById("reduction2_2_"+special_id).style.display = "none";
					
					document.getElementById("reduction3_"+special_id).style.display = "block";
					document.getElementById("reduction3_2_"+special_id).style.display = "none";
					
					document.getElementById("new_special_price_"+special_id).style.display = "block";
					document.getElementById("new_special_price_2_"+special_id).style.display = "none";
					
					document.getElementById("sp_from_"+special_id).style.display = "block";
					document.getElementById("sp_from_2_"+special_id).style.display = "none";
					
					document.getElementById("sp_to_"+special_id).style.display = "block";
					document.getElementById("sp_to_2_"+special_id).style.display = "none";
					
					document.getElementById("azioni_"+special_id).style.display = "block";
					document.getElementById("chiudilo_"+special_id).style.display = "none";
					document.getElementById("azioni_2_"+special_id).style.display = "none";
					
				}
				</script>';
			echo '<div id="apri_form"><form style="display:none" name="all_specials" action="index.php?tab=SpecialiAcquisto&edit_price_conf&token='.$this->token.(isset($_GET['orderby']) ? '&orderby='.$_GET['orderby'] : '').(isset($_GET['order_by']) ? '&order_by='.$_GET['order_by'] : '').(isset($_GET['orderway']) ? '&orderway='.$_GET['orderway'] : '').(isset($_GET['order_way']) ? '&order_way='.$_GET['order_way'] : '').'" method="post"></div>';
			echo "<script type='text/javascript'>
			$(document).ready(function() {
				function moveScroll(){
					var scroll = $(window).scrollTop();
					var anchor_top = $('#tablePrezzi').offset().top;
					var anchor_bottom = $('#bottom_anchor').offset().top;
					if (scroll>anchor_top && scroll<anchor_bottom) {
					clone_table = $('#clone');
					if(clone_table.length == 0){
						clone_table = $('#tablePrezzi').clone();
						clone_table.attr('id', 'clone');
						clone_table.css({position:'fixed',
								 'pointer-events': 'none',
								 top:0});
						clone_table.width($('#tablePrezzi').width());
						$('#table-container').append(clone_table);
						$('#clone').css({visibility:'hidden'});
						$('#clone thead').css({'visibility':'visible','pointer-events':'auto'});
					}
					} else {
					$('#clone').remove();
					}
				}
				$(window).scroll(moveScroll); 
			});
		</script>
		<div id='table-container'>
		<table class='table' id='tablePrezzi'>";
			echo "<thead>";
			echo "<tr>";
			echo "<th style='width:150px'>Cod. prodotto <a href='index.php?tab=SpecialiAcquisto&orderby=p.reference&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=SpecialiAcquisto&orderby=p.reference&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
			echo "<th style='width:290px'>Desc. prodotto <a href='index.php?tab=SpecialiAcquisto&orderby=nome_prodotto&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=SpecialiAcquisto&orderby=nome_prodotto&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
			echo "<th style='width:140px'>Costruttore <a href='index.php?tab=SpecialiAcquisto&orderby=costruttore&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=SpecialiAcquisto&orderby=costruttore&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
			echo "<th style='width:80px'>Mag. EZ<a href='index.php?tab=SpecialiAcquisto&orderby=p.stock_quantity&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=SpecialiAcquisto&orderby=p.stock_quantity&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
			
			echo "<th style='width:80px'>Sconto 1</th>";
			
			
			echo "<th style='width:80px'>Sconto 2</th>";
			echo "<th style='width:80px'>Sconto 3</th>";
			echo "<th style='width:80px'>Prezzo speciale <a href='index.php?tab=SpecialiAcquisto&order_by=prezzi_speciali&order_way=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=SpecialiAcquisto&order_by=prezzi_speciali&order_way=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
			echo "<th style='width:100px'>Partenza <a href='index.php?tab=SpecialiAcquisto&orderby=sp.from&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=SpecialiAcquisto&orderby=sp.from&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
			echo "<th style='width:100px'>Fine <a href='index.php?tab=SpecialiAcquisto&orderby=sp.to&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=SpecialiAcquisto&orderby=sp.to&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
			<th style='width:75px'>Azioni</th>";
			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
			
			$miglior_prezzo_cli = array();
			$miglior_prezzo_riv = array();
			$prezzi_speciali = array();
			
			$tokenProducts = Tools::getAdminToken('AdminCatalogExFeatures'.(int)Tab::getIdFromClassName('AdminCatalogExFeatures').(int)$cookie->id_employee);
			
			foreach ($speciali as $key => $row)
			{
				$prezzi_speciali[$key] = $row['wholesale_price'];
			}
			
			
			if(isset($_GET['order_by']) && $_GET['order_by'] == 'prezzi_speciali') {
				if(isset($_GET['order_way']) && $_GET['order_way'] == 'asc') {
					array_multisort($prezzi_speciali, SORT_ASC, $speciali);
				}
				else if(isset($_GET['order_way']) && $_GET['order_way'] == 'desc') {
					array_multisort($prezzi_speciali, SORT_DESC, $speciali);
				}
			}
			
			foreach ($speciali as $speciale) {
			
				$listino = Db::getInstance()->getValue('SELECT listino FROM product WHERE id_product = '.$speciale['id_product']);
								
				if($speciale['pieces'] == 0 && $speciale['pieces'] != '')
					$red = 'y';
				else
					$red = 'n';
	
				if($speciale['to'] < date('Y-m-d H:i:s'))
					$red = 'y';
						
				//includeDatepicker3(array('sp_from_'.$speciale['id_specific_price'].'_f', 'sp_to_'.$speciale['id_specific_price'].'_f'), true);
				echo "<tr ".($red == 'y' ? 'class="tr-red-border"' : '').">";
				echo "<td>".''."<span style='cursor:pointer' class='span-reference' title=\"".Product::showProductTooltip($speciale['id_prodotto'])."\"><a href='index.php?tab=AdminCatalogExFeatures&id_product=".$speciale['id_prodotto']."&updateproduct&token=".$tokenProducts."&tabs=5' target='_blank'>".$speciale['reference']."</a></span></td>";
				echo "<td id='nome_prodotto_".$speciale['id_specific_price']."' style='max-width:150px'>".str_replace(',',', ',str_replace(';',', ',$speciale['nome_prodotto']))."</td>";
				echo "<td>".$speciale['costruttore']."</td>";
				echo "<td>".$speciale['stock_quantity']."</td>";
			
				
				echo "<td style='text-align:right'>".
				
				(isset($_GET['editprice']) && $_GET['id_specific_price'] == $speciale['id_specific_price'] ? '<form action="index.php?tab=SpecialiAcquisto&id_specific_price='.$speciale['id_specific_price'].'&edit_price_conf&token='.$this->token.(isset($_GET['orderby']) ? '&orderby='.$_GET['orderby'] : '').(isset($_GET['order_by']) ? '&order_by='.$_GET['order_by'] : '').(isset($_GET['orderway']) ? '&orderway='.$_GET['orderway'] : '').(isset($_GET['order_way']) ? '&order_way='.$_GET['order_way'] : '').'" method="post"><input name="id_specific_price" value="'.$speciale['id_specific_price'].'" type="hidden" /> <input size="4" type="text" value="'.str_replace(".",",",$speciale['reduction_1']).'" name="reduction1" />' : "<div id='reduction1_".$speciale['id_specific_price']."' style='display:block'>".number_format($speciale['reduction_1'],2,",",""))."</div><div id='reduction1_2_".$speciale['id_specific_price']."' style='display:none'><input name='id_specific_price[]' value='".$speciale['id_specific_price']."' type='hidden' /> <input type='hidden' id='listino_input_".$speciale['id_specific_price']."' value='".$listino."' /><input type='text' value='".str_replace(".",",",$speciale['reduction_1'])."' size='5' name='reduction1[]' id='reduction1_input_".$speciale['id_specific_price']."' onkeyup='calcScontiAcquisto(\"reduction1_input_".$speciale['id_specific_price']."\", \"reduction2_input_".$speciale['id_specific_price']."\", \"reduction3_input_".$speciale['id_specific_price']."\", \"listino_input_".$speciale['id_specific_price']."\", \"new_special_price_input_".$speciale['id_specific_price']."\"); ' /></div></td>";
				
				echo "<td style='text-align:right'>".
				
				(isset($_GET['editprice']) && $_GET['id_specific_price'] == $speciale['id_specific_price'] ? ' <input size="4" type="text" value="'.str_replace(".",",",$speciale['reduction_2']).'" name="reduction2" />' : "<div id='reduction2_".$speciale['id_specific_price']."' style='display:block'>".number_format($speciale['reduction_2'],2,",",""))."</div><div id='reduction2_2_".$speciale['id_specific_price']."' style='display:none'><input type='text' value='".str_replace(".",",",$speciale['reduction_2'])."' size='5' name='reduction2[]' id='reduction2_input_".$speciale['id_specific_price']."' onkeyup='calcScontiAcquisto(\"reduction1_input_".$speciale['id_specific_price']."\", \"reduction2_input_".$speciale['id_specific_price']."\", \"reduction3_input_".$speciale['id_specific_price']."\", \"listino_input_".$speciale['id_specific_price']."\", \"new_special_price_input_".$speciale['id_specific_price']."\"); ' /></div></td>";
				
					echo "<td style='text-align:right'>".
				
				(isset($_GET['editprice']) && $_GET['id_specific_price'] == $speciale['id_specific_price'] ? ' <input size="4" type="text" value="'.str_replace(".",",",$speciale['reduction_3']).'" name="reduction3" />' : "<div id='reduction3_".$speciale['id_specific_price']."' style='display:block'>".number_format($speciale['reduction_3'],2,",",""))."</div><div id='reduction3_2_".$speciale['id_specific_price']."' style='display:none'><input type='text' value='".str_replace(".",",",$speciale['reduction_3'])."' size='5' name='reduction3[]' id='reduction3_input_".$speciale['id_specific_price']."' onkeyup='calcScontiAcquisto(\"reduction1_input_".$speciale['id_specific_price']."\", \"reduction2_input_".$speciale['id_specific_price']."\", \"reduction3_input_".$speciale['id_specific_price']."\", \"listino_input_".$speciale['id_specific_price']."\", \"new_special_price_input_".$speciale['id_specific_price']."\"); ' /></div></td>";
				
				$prezzo_speciale = $speciale['wholesale_price'];
			
				echo "<td style='text-align:right'>".
				
				(isset($_GET['editprice']) && $_GET['id_specific_price'] == $speciale['id_specific_price'] ? ' <input size="4" type="text" value="'.str_replace(".",",",$prezzo_speciale).'" name="new_special_price" />' : "<div id='new_special_price_".$speciale['id_specific_price']."' style='display:block'>".Tools::displayPrice($prezzo_speciale,1))."</div><div id='new_special_price_2_".$speciale['id_specific_price']."' style='display:none'><input type='text' value='".str_replace(".",",",$prezzo_speciale)."' size='5' name='new_special_price[]' id='new_special_price_input_".$speciale['id_specific_price']."' onchange='document.getElementById(\"reduction2_input_".$speciale['id_specific_price']."\").value = 0; document.getElementById(\"reduction3_input_".$speciale['id_specific_price']."\").value = 0; calcSconto(\"listino_input_".$speciale['id_specific_price']."\", \"new_special_price_input_".$speciale['id_specific_price']."\",\"reduction1_input_".$speciale['id_specific_price']."\");' /></div></td>";
				
				
				echo "<td><div id='sp_from_".$speciale['id_specific_price']."' style='display:block'>".(isset($_GET['editprice']) && $_GET['id_specific_price'] == $speciale['id_specific_price'] ? '<input type="text" id="sp_from" name="sp_from[]" value="'.date("d-m-Y",strtotime($speciale['from'])).'" />' : ($speciale['from'] == '0000-00-00 00:00:00' || $speciale['from'] == '1970-01-01 00:00:00' ? Tools::displayDate(date('Y-01-01 00:00:00'),5,false) : Tools::displayDate($speciale['from'],5,false)).'</div>'."<div id='sp_from_2_".$speciale['id_specific_price']."' style='display:none'>".'<input type="text" size="8" id="sp_from_'.$speciale['id_specific_price'].'_f" name="sp_from[]" value="'.date("d-m-Y",strtotime($speciale['from'])).'" />')."</div></td>";
				
				
				echo "<td><div id='sp_to_".$speciale['id_specific_price']."' style='display:block'>".(isset($_GET['editprice']) && $_GET['id_specific_price'] == $speciale['id_specific_price'] ? '<input type="text" id="sp_to" name="sp_to[]" value="'.date("d-m-Y",strtotime($speciale['to'])).'" />' : ($speciale['to'] == '0000-00-00 00:00:00' || $speciale['to'] == '1970-01-01 00:00:00' ? Tools::displayDate(date('Y-01-01 00:00:00'),5,false) : Tools::displayDate($speciale['to'],5,false)).'</div>'."<div id='sp_to_2_".$speciale['id_specific_price']."' style='display:none'>".'<input type="text" size="8" id="sp_to_'.$speciale['id_specific_price'].'_f" name="sp_to[]" value="'.date("d-m-Y",strtotime($speciale['to'])).'" />')."</div></td>";
				
				
				
				echo "<td><div id='azioni_2_".$speciale['id_specific_price']."' style='display:none'>".
				'<input type="submit" class="button" style="display:none" name="conferma_singolo['.$speciale['id_specific_price'].']" value="Conferma" /></div>
				
				
				<div id="chiudilo_'.$speciale['id_specific_price'].'" style="display:none"><a style="cursor:pointer" onclick="chiudiForm('.$speciale['id_specific_price'].', \''.str_replace(".",",",$prezzo_speciale).'\', \''.date("d-m-Y",strtotime($speciale['from'])).'\', \''.date("d-m-Y",strtotime($speciale['to'])).'\');"><img src="../img/admin/arrow-left.png" title="Chiudi campi" /></a></div>' . "<div id='azioni_".$speciale['id_specific_price']."' style='display:block'>".'
				
				<a style="cursor:pointer" onclick="activateEditForm('.$speciale['id_specific_price'].', \''.str_replace(".",",",$prezzo_speciale).'\', \''.date("d-m-Y",strtotime($speciale['from'])).'\', \''.date("d-m-Y",strtotime($speciale['to'])).'\');"><img src="../img/admin/edit.gif" /></a>
				
				<!-- <a href="index.php?tab=SpecialiAcquisto&id_specific_price='.$speciale['id_specific_price'].'&deletesp&token='.$this->token.(isset($_GET['orderby']) ? '&orderby='.$_GET['orderby'] : '').(isset($_GET['order_by']) ? '&order_by='.$_GET['order_by'] : '').(isset($_GET['orderway']) ? '&orderway='.$_GET['orderway'] : '').(isset($_GET['order_way']) ? '&order_way='.$_GET['order_way'] : '').'" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }"> --> <img src="../img/admin/delete.gif" /> <input type="checkbox" name="delete_sp[]" value="'.$speciale['id_specific_price'].'" /> <!-- </a> -->'.'
				
				</div></td>';
				echo "</tr>";
			}
			echo "</tbody>";
			echo '</table></div><div id="bottom_anchor">
			</div><!-- <div id="chiudi_form" style="display:none"> --> 
			
			<script type="text/javascript">
function select(a) {
    var theForm = document.all_specials;
    for (i=0; i<theForm.elements.length; i++) {
        if (theForm.elements[i].name=="delete_sp[]")
            theForm.elements[i].checked = a;
    }
}
</script>
			<a class="button" style="height:22px" href="javascript:select(1)">Seleziona tutti i prodotti</a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" type="submit" class="button" id="conferma_tutti" name="conferma_tutti" value="Conferma le modifiche" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit"  onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" class="button" id="cancella" name="cancella" value="CANCELLA i prezzi speciali flaggati" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit" class="button" id="allunga" name="allunga" value="Allunga fino a fine mese (da oggi) i prezzi speciali flaggati" />
			<input type="submit" class="button" id="allunga_successivo" name="allunga_successivo" value="Allunga fino a fine mese SUCCESSIVO (da oggi) i prezzi speciali flaggati" />
			
			</form> <!-- </div> -->';
			
					
			
		}
		
		
}

