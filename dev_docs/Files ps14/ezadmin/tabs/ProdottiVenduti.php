<?php

	include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

	class ProdottiVenduti extends AdminPreferences {
	
	public function bindDatepickerZ($id, $time)
	{
		if ($time)
			echo '
			var dateObj = new Date();
			var hours = dateObj.getHours();
			var mins = dateObj.getMinutes();
			var secs = dateObj.getSeconds();
			if (hours < 10) { hours = "0" + hours; }
			if (mins < 10) { mins = "0" + mins; }
			if (secs < 10) { secs = "0" + secs; }
			var time = " "+hours+":"+mins+":"+secs;';

		echo '
		$(function() {
			$("#'.Tools::htmlentitiesUTF8($id).'").datepicker({
				prevText:"",
				nextText:"",
				dateFormat:"dd-mm-yy"});
		});';
	}

	// id can be a identifier or an array of identifiers
	public function includeDatepickerZ($id, $time = false)
	{
		global $cookie;
		echo '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery-ui-1.8.10.custom.min.js"></script>';
		$iso = Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'lang WHERE `id_lang` = '.(int)$cookie->id_lang);
		if ($iso != 'en')
			echo '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/datepicker/ui/i18n/ui.datepicker-'.Tools::htmlentitiesUTF8($iso).'.js"></script>';
		echo '<script type="text/javascript">';
			if (is_array($id))
				foreach ($id as $id2)
					$this->bindDatepickerZ($id2, $time);
			else
				$this->bindDatepickerZ($id, $time);
		echo '</script>';
	}

	public	function datediff($tipo, $partenza, $fine)
    {
        switch ($tipo)
        {
            case "A" : $tipo = 365;
            break;
            case "M" : $tipo = (365 / 12);
            break;
            case "S" : $tipo = (365 / 52);
            break;
            case "G" : $tipo = 1;
            break;
        }
		$arrpulitopartenza = explode(" ", $partenza);
        $arr_partenza = explode("-", $arrpulitopartenza[0]);
        $partenza_gg = $arr_partenza[2];
        $partenza_mm = $arr_partenza[1];
        $partenza_aa = $arr_partenza[0];
		$arrpulitofine = explode(" ", $fine);
        $arr_fine = explode("-", $arrpulitofine[0]);
        $fine_gg = $arr_fine[2];
        $fine_mm = $arr_fine[1];
        $fine_aa = $arr_fine[0];
        $date_diff = mktime(12, 0, 0, $fine_mm, $fine_gg, $fine_aa) - mktime(12, 0, 0, $partenza_mm, $partenza_gg, $partenza_aa);
        $date_diff  = floor(($date_diff / 60 / 60 / 24) / $tipo);
        return $date_diff;
    }
	
		public function display()
		{
			

			if (get_magic_quotes_gpc()) {
				$process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
				while (list($key, $val) = each($process)) {
					foreach ($val as $k => $v) {
						unset($process[$key][$k]);
						if (is_array($v)) {
							$process[$key][stripslashes($k)] = $v;
							$process[] = &$process[$key][stripslashes($k)];
						} else {
							$process[$key][stripslashes($k)] = stripslashes($v);
						}
					}
				}
				unset($process);
			}


			global $cookie;
			echo "<h1>Statistiche sui prodotti venduti</h1>";

		
			if(isset($_POST['cercaiprodotti'])) { 
			
				$prodotti = "";
				$categorie_di_default = "";
				$macrocategorie = "";
				$macrocategorie_di_default = "";
				$categorie = "";
				$marchi = "";

				echo "<strong>Riepilogo - Statistiche su...</strong><br /><br />";
				
				if(isset($_POST['per_prodotto'])) {
					echo "<strong>Questi prodotti precisi:</strong><br /> ";
					
					
					foreach ($_POST['per_prodotto'] as $prodotto) {

						$nomeprodotto = Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_lang = 5 AND id_product = ".$prodotto."");
						
						$prodottiinclusi.= $nomeprodotto."; ";
						
						$strisciaprodotti.= "- ".$nomeprodotto."<br />";
	
						$prodotti.= "p.id_product = $prodotto OR ";
					
					}
				}
				
				else if(isset($_POST['prodottiinclusi']) && !empty($_POST['prodottiinclusi'])) {
				
					echo "<strong>Questi prodotti precisi:</strong><br /> ";
					$strisciaprodotti = $_POST['strisciaprodotti'];
					$prodottiinclusi = $_POST['prodottiinclusi'];
				
				
				}
				
				
				
				echo $strisciaprodotti;
				
				
				echo "<br />";
				
		
				
				if(isset($_POST['per_categoria'])) {
				
					echo "<strong>Prodotti in queste sottocategorie:</strong><br /> ";
			
			
					foreach ($_POST['per_categoria'] as $categoria) {
	
						$nomecat = Db::getInstance()->getValue("SELECT name FROM category_lang WHERE id_lang = 5 AND id_category = ".$categoria."");
						$strisciacat .= "- ".$nomecat."<br />";
						
					
						$categorieincluse.= $nomecat."; ";
						
						$categorie_di_default.= "p.id_category_default = $categoria OR ";
						$categorie.= "cp.id_category = $categoria OR ";
					}	
					
				}
				
				
				else if(isset($_POST['categorieincluse']) && !empty($_POST['categorieincluse'])) {
				
					echo "<strong>Prodotti in queste sottocategorie:</strong><br /> ";
					$strisciacat = $_POST['strisciacat'];
					$categorieincluse = $_POST['categorieincluse'];
				
				
				}
				
				echo $strisciacat;
				
				if(isset($_POST['per_macrocategoria'])) {
				
					echo "<strong>Prodotti in queste macrocategorie:</strong><br /> ";
			
			
					foreach ($_POST['per_macrocategoria'] as $macrocategoria) {
	
						$nomemacrocat = Db::getInstance()->getValue("SELECT name FROM category_lang WHERE id_lang = 5 AND id_category = ".$macrocategoria."");
						$strisciamacrocat .= "- ".$nomemacrocat."<br />";
						
						$children = Db::getInstance()->executeS("SELECT id_category FROM
						(SELECT t4.id_category
						FROM category AS t1
						LEFT JOIN category AS t2 ON t1.id_category = t2.id_parent
						LEFT JOIN category AS t3 ON t2.id_category = t3.id_parent
						LEFT JOIN category AS t4 ON t3.id_category = t3.id_parent
						WHERE t2.id_parent =$macrocategoria

						UNION 

						SELECT t3.id_category
						FROM category AS t1
						LEFT JOIN category AS t2 ON t1.id_category = t2.id_parent
						LEFT JOIN category AS t3 ON t2.id_category = t3.id_parent
						WHERE t2.id_parent =$macrocategoria

						UNION 

						SELECT t2.id_category
						FROM category AS t1
						LEFT JOIN category AS t2 ON t1.id_category = t2.id_parent
						WHERE t2.id_parent =$macrocategoria

						UNION 

						SELECT id_category FROM category WHERE id_category = $macrocategoria) ccc WHERE id_category IS NOT NULL ORDER BY id_category ASC

						");

						foreach ($children as $child) {
	
							$macrocategorie_di_default.= "p.id_category_default = ".$child['id_category']." OR ";
							$macrocategorie.= "cp.id_category = ".$child['id_category']." OR ";
						}
						
					
						$macrocategorieincluse.= $nomemacrocat."; ";
						
						
				
					}	
					
				}
				
				
				else if(isset($_POST['macrocategorieincluse']) && !empty($_POST['macrocategorieincluse'])) {
				
					echo "<strong>Prodotti in queste macrocategorie:</strong><br /> ";
					$strisciamacrocat = $_POST['strisciamacrocat'];
					$macrocategorieincluse = $_POST['macrocategorieincluse'];
				
				
				}
				
				echo $strisciamacrocat;
				
				if(isset($_POST['per_marchio'])) {
				
					echo "<strong>Prodotti in questi marchi:</strong><br /> ";
			
			
					foreach ($_POST['per_marchio'] as $marchio) {
	
						$nomemarchio = Db::getInstance()->getValue("SELECT name FROM manufacturer WHERE id_manufacturer = ".$marchio."");
						$strisciamarchi .= "- ".$nomemarchio."<br />";
						
					
						$marchiinclusi.= $nomemarchio."; ";
						$marchi.= "p.id_manufacturer = $marchio OR ";
						$marchi2.= "od.manufacturer_id = $marchio OR ";
					}	
					
				}
				
				else if(isset($_POST['marchiinclusi']) && !empty($_POST['marchiinclusi'])) {
				
					echo "<strong>Prodotti in questi marchi:</strong><br /> ";
					$strisciamarchi = $_POST['strisciamarchi'];
					$marchiinclusi = $_POST['marchiinclusi'];
				
				
				}
				
				$amazon_query = '';
				
				if(isset($_POST['AmazonEZ']) || isset($_POST['AmazonPrime']) || isset($_POST['AmazonItalia']) || isset($_POST['AmazonFrancia']) || isset($_POST['AmazonGermania']) || isset($_POST['AmazonSpagna']) || isset($_POST['AmazonRegnoUnito']))
				{
					
					$strisciaamazon = '';
					
					if(isset($_POST['AmazonEZ']) || isset($_POST['AmazonPrime']))
					{
						$amazon_query .= ' AND (ao.tipo = "xx" OR ';
						if(isset($_POST['AmazonEZ']))
							$amazon_query .= ' ao.tipo = "MFN" OR ';
						if(isset($_POST['AmazonPrime']))
							$amazon_query .= ' ao.tipo = "AFN" OR ';
						
						$amazon_query .= ' ao.tipo = "yy")';
					}
					
					if(isset($_POST['AmazonItalia']) || isset($_POST['AmazonFrancia']) || isset($_POST['AmazonGermania']) || isset($_POST['AmazonSpagna']) || isset($_POST['AmazonRegnoUnito']))
					{
						
						$amazon_query .= ' AND (ao.marketplace = "xx" OR ';
						if(isset($_POST['AmazonItalia']))
							$amazon_query .= ' ao.marketplace = "it" OR ';
						if(isset($_POST['AmazonFrancia']))
							$amazon_query .= ' ao.marketplace = "fr" OR ';
						if(isset($_POST['AmazonGermania']))
							$amazon_query .= ' ao.marketplace = "de" OR ';
						if(isset($_POST['AmazonSpagna']))
							$amazon_query .= ' ao.marketplace = "es" OR ';
						if(isset($_POST['AmazonRegnoUnito']))
							$amazon_query .= ' ao.marketplace = "uk" OR ';
						
						$amazon_query .= ' ao.marketplace = "yy")';
						
						
						
					}
						
					$strisciaamazon .= 	(isset($_POST['AmazonEZ']) ? 'Ez ' : '').' '.(isset($_POST['AmazonPrime']) ? 'Prime ' : '').' '.(isset($_POST['AmazonItalia']) ? 'Italia ' : '').' '.(isset($_POST['AmazonFrancia']) ? 'Francia ' : '').' '.(isset($_POST['AmazonSpagna']) ? 'Spagna ' : '').' '.(isset($_POST['AmazonGermania']) ? 'Germania ' : '').' '.(isset($_POST['AmazonRegnoUnito']) ? 'Regno Unito ' : '');
					
					$strisciaamazon = 'Amazon: '.$strisciaamazon;
					
					echo $strisciaamazon.'<br />';
				}
				else if(isset($_POST['strisciaamazon']) && !empty($_POST['strisciaamazon']))
				{
					echo "<strong>Amazon:</strong><br /> ";
					$strisciaamazon = $_POST['strisciaamazon'];
				}
				
				echo $strisciamarchi;
				
				if(isset($_POST['escludi-privati'])) {
				
					$escludi_privati = "AND c.is_company = 1";
					$striscia_escludi_privati = "<strong>Privati esclusi</strong>";
				}
				else {
					$striscia_escludi_privati = "<strong>Tutti i clienti</strong>";
				}
				
				if(isset($_POST['striscia_escludi_privati'])) {
				
					$striscia_escludi_privati = $_POST['striscia_escludi_privati'];
				}
			
				echo $striscia_escludi_privati."<br />";
				
				if(!empty($_POST['arco_dal'])) {
					
					$data_dal_a = explode("-", $_POST['arco_dal']);
					$data_dal = $data_dal_a[2]."-".$data_dal_a[1]."-".$data_dal_a[0]; 
					
					if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'ordinato') {
						$arco_dal = 'AND o.date_add > "'.$data_dal.' 00:00:00"';
					}
					else if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'fatturato') {
						$arco_dal = 'AND f.data_fattura > "'.$data_dal.' 00:00:00"';
					}
					else {
						$arco_dal = 'AND ca.date_add > "'.$data_dal.' 00:00:00"';
					}
					$striscia_arco_dal = "Da: <strong>$_POST[arco_dal]</strong> ";
					
				}
				else {
					$striscia_arco_dal = "Da: <strong>inizio della storia del sito</strong> ";
				
				}
				
				if(isset($_POST['striscia_arco_dal'])) {
				
					$striscia_arco_dal = $_POST['striscia_arco_dal'];
				}
			
				echo $striscia_arco_dal;
				
				if(!empty($_POST['arco_al'])) {
				
					$data_al_a = explode("-", $_POST['arco_al']);
					$data_al = $data_al_a[2]."-".$data_al_a[1]."-".$data_al_a[0]; 
					
					if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'ordinato') {
						$arco_al = 'AND o.date_add < "'.$data_al.' 00:00:00"';
					}
					else if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'fatturato') {
						$arco_al = 'AND f.data_fattura < "'.$data_al.' 00:00:00"';
					}
					else {
						$arco_al = 'AND ca.date_add < "'.$data_al.' 00:00:00"';
					}
					
					$striscia_arco_al = "a: <strong>$_POST[arco_al]</strong> ";
				}
				else {
					$striscia_arco_al = "a: <strong>oggi</strong> ";
				
				}
				
				if(isset($_POST['striscia_arco_al'])) {
				
					$striscia_arco_al = $_POST['striscia_arco_al'];
				}
			
				echo $striscia_arco_al."<br />";
				
				$anno_corrente = "2013";
				switch($_POST['cercaiprodotti']) {
				
					case 'referenceasc': $orderby = "ORDER BY reference ASC"; break;
					case 'referencedesc': $orderby = "ORDER BY reference DESC"; break;
					case 'prodottoasc': $orderby = "ORDER BY name ASC"; break;
					case 'prodottodesc': $orderby = "ORDER BY name DESC"; break;
					case 'totaleacqasc': $orderby = "ORDER BY totale_acq_euro ASC"; break;
					case 'totaleacqdesc': $orderby = "ORDER BY totale_acq_euro DESC"; break;
					case 'qtavendutoasc': $orderby = "ORDER BY quantita_venduto ASC"; break;
					case 'qtavendutodesc': $orderby = "ORDER BY quantita_venduto DESC"; break;
					case 'totaleacqannoasc': $orderby = ""; break;
					case 'totaleacqannodesc': $orderby = ""; break;
					
					default: $orderby = "ORDER BY totale_acq_euro DESC"; break;
					
				
				}
				
				if (isset($_POST['per_prodotto']) || isset($_POST['per_categoria']) || isset($_POST['per_marchio']) || isset($_POST['per_macrocategoria'])) { $where = "AND p.id_product IN (
					
						SELECT p.id_product FROM product p JOIN category_product cp ON p.id_product = cp.id_product
						WHERE (
						(
						$categorie_di_default
						p.id_category_default != 9999999999999999
						)
						
						";
						
						if(isset($_POST['per_prodotto'])) {
							$where.= "AND 
							(
							$prodotti
							p.id_product = 9999999999999999999999
							)";
						}
						if(isset($_POST['per_categoria'])) {
						
							$where .= "AND 
							(
							$categorie
							cp.id_category = 9999999999999999999999
							)";
						
						}
						
						if(isset($_POST['per_marchio'])) {
						
							$where .= "
							AND 
							(
							$marchi
							p.id_manufacturer = 9999999999999999999999
							)";
						}
						
						if(isset($_POST['per_macrocategoria'])) {
						
							$where .= "
							AND
							(
							$macrocategorie
							cp.id_category = 9999999999999999999999
							)";
						
						}
						
						/*
						OR 
						(
						$macrocategorie_di_default
						p.id_category_default = 9999999999999999
						)
						*/
						
					$where.= ")
					)"; 
					}
					
					
					
				if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'ordinato') {
				
					$from = 'FROM orders o
					JOIN customer c ON o.id_customer = c.id_customer
					JOIN order_detail od ON o.id_order = od.id_order
					LEFT JOIN product p ON od.product_id = p.id_product
					
					';
					
					if(isset($_POST['AmazonEZ']) || isset($_POST['AmazonPrime']) || isset($_POST['AmazonItalia']) || isset($_POST['AmazonFrancia']) || isset($_POST['AmazonGermania']) || isset($_POST['AmazonSpagna']) || isset($_POST['AmazonRegnoUnito']))
						$from .= '
						JOIN amazon_orders ao ON o.id_order = ao.id_order';
			
				}
				else if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'fatturato') {
				
					$from = 'FROM fattura f 
					JOIN customer c ON f.id_customer = c.id_customer
					LEFT JOIN product p ON f.cod_articolo = p.reference
					
					';
					
					if(isset($_POST['AmazonEZ']) || isset($_POST['AmazonPrime']) || isset($_POST['AmazonItalia']) || isset($_POST['AmazonFrancia']) || isset($_POST['AmazonGermania']) || isset($_POST['AmazonSpagna']) || isset($_POST['AmazonRegnoUnito']))
						$from .= '
						JOIN amazon_orders ao ON f.rif_ordine = ao.id_order';
					
				}
				else {
					$from = 'FROM cart ca
					JOIN customer c ON ca.id_customer = c.id_customer
					JOIN cart_product cp ON ca.id_cart = cp.id_cart
					JOIN product p ON cp.id_product = p.id_product
					JOIN product_lang pl ON p.id_product = pl.id_product
					
					';
					
					if(isset($_POST['AmazonEZ']) || isset($_POST['AmazonPrime']) || isset($_POST['AmazonItalia']) || isset($_POST['AmazonFrancia']) || isset($_POST['AmazonGermania']) || isset($_POST['AmazonSpagna']) || isset($_POST['AmazonRegnoUnito']))
						$from .= 'JOIN amazon_orders ao ON c.id_cart = ao.id_order';
				
				}
				
				if(isset($_POST['AmazonEZ']) || isset($_POST['AmazonPrime']) || isset($_POST['AmazonItalia']) || isset($_POST['AmazonFrancia']) || isset($_POST['AmazonGermania']) || isset($_POST['AmazonSpagna']) || isset($_POST['AmazonRegnoUnito']))
					$where .= $amazon_query;
				
				
				if(isset($_POST['querysql'])) {

					$sql = $_POST['querysql'];
					
				}
				else {
				
					if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'ordinato') {
						$sql = "SELECT * FROM (SELECT  od.product_reference, od.product_name, od.id_order_detail, od.product_id, od.product_price, od.reduction_percent, od.product_quantity, o.date_add, SUM(od.product_quantity) as quantita_venduto,
						SUM(((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) - ((CASE WHEN (od.wholesale_price = 0) THEN p.wholesale_price ELSE od.wholesale_price END)*od.product_quantity)) as margine,
						SUM(p.wholesale_price*od.product_quantity) as tot_acquisti,
						SUM((od.product_price-(od.product_price*od.reduction_percent/100))*od.product_quantity) totale_acq_euro
						$from
						
						WHERE (o.valid = 1 OR (o.valid = 0 AND o.module = \"bankwire\")
						AND o.id_order NOT IN (SELECT id_order FROM order_history WHERE id_order_state = 6  OR id_order_state = 35))
						$escludi_privati
						$arco_dal
						$arco_al
						$where 
						
						
						GROUP BY od.product_id
						
						) ct GROUP BY ct.id_order_detail
						";
					}
					else if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'fatturato') {
						$sql = "SELECT * FROM (SELECT f.desc_articolo, f.id_riga, f.prezzo, f.cod_articolo, f.qt_ord, f.data_fattura, SUM(f.qt_ord) as quantita_venduto, SUM(f.prezzo*f.qt_ord) totale_acq_euro
					$from
					
					WHERE 
					1
					
					$escludi_privati
					$arco_dal
					$arco_al
					$where 
					
					
					GROUP BY f.cod_articolo
					
					) ct GROUP BY ct.id_riga
					";
					}
					else {
						$sql = "SELECT * FROM (SELECT  p.reference, pl.name, p.id_product, cp.price, cp.quantity, ca.date_add, SUM(cp.quantity) as quantita_venduto, 0 AS totale_acq_euro
						$from
						
						WHERE pl.id_lang = 5 AND ca.preventivo = 1 
						
						$escludi_privati
						$arco_dal
						$arco_al
						$where 
						
						
						GROUP BY p.id_product
						
						) ct GROUP BY ct.id_product
						";
					}
					
					
				}
				
				//echo $sql; 
				
			
				echo "<br />";
				
				
				echo "<form method='post' action=''>";
				
				echo "<input type='submit' value='Esporta i dati in formato Excel' name='esportaexcel' class='button' />";
				
				echo "<input type='hidden' name='querysql' value='".(isset($_POST['querysql']) ? $_POST['querysql'] : $sql)."' />	
				<input type='hidden' name='query_from' value='".(isset($_POST['query_from']) ? $_POST['query_from'] : $from)."' />	
				<input type='hidden' name='query_where' value='".(isset($_POST['query_where']) ? $_POST['query_where'] : $where)."' />						
				<input type='hidden' name='prodottiinclusi' value='".$prodottiinclusi."' />		
				<input type='hidden' name='strisciaprodotti' value='".$strisciaprodotti."' />		
				<input type='hidden' name='categorieincluse' value='".$categorieincluse."' />	
				<input type='hidden' name='macrocategorieincluse' value='".$macrocategorieincluse."' />						
				<input type='hidden' name='strisciacat' value='".$strisciacat."' />	
				<input type='hidden' name='strisciaamazon' value='".$strisciaamazon."' />	
				<input type='hidden' name='marchiinclusi' value='".$marchiinclusi."' />		
				<input type='hidden' name='strisciamarchi' value='".$strisciamarchi."' />	
				<input type='hidden' name='striscia_escludi_privati' value='".$striscia_escludi_privati."' />				
				<input type='hidden' name='striscia_arco_dal' value='".$striscia_arco_dal."' />	
				<input type='hidden' name='striscia_arco_al' value='".$striscia_arco_al."' />	
				";
				if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'ordinato') {
					echo "<input type='hidden' name='su_tipo' value='ordinato' />";
				}
				else if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'fatturato') {
					echo "<input type='hidden' name='su_tipo' value='fatturato' />";
				}
				else {
					echo "<input type='hidden' name='su_tipo' value='preventivi' />";
				}
				echo "<br /><br />";
				
				$sql.= $orderby;
				
				
				$resultsprodotti = Db::getInstance()->ExecuteS(stripslashes($sql));
				
				$num = 0;
				$tot_acquisti = 0;
				
				if(Tools::getIsset('cercaiprodotti') && (Tools::getValue('cercaiprodotti') == 'totaleacqannoasc' || Tools::getValue('cercaiprodotti') == 'totaleacqannodesc')) { 
				
					$tot_anno = array();
					foreach ($resultsprodotti as $key => $row)
					{	
						$ultimo_anno = Db::getInstance()->getValue('SELECT SUM(o.total_paid) AS ultimo_anno FROM orders o WHERE (o.date_add > "'.$anno_corrente.'-01-01 00:00:00" AND o.date_add < "'.$anno_corrente.'-12-31 00:00:00" AND (o.valid = 1 OR ((o.valid = 0 (o.module = "bankwire" OR o.module = "other_payment"))
						AND o.id_order NOT IN (SELECT id_order FROM order_history WHERE id_order_state = 6 OR id_order_state = 35))) AND o.id_customer = '.$row['id_customer'].')');
						$tot_anno[$key] = $ultimo_anno;
					}
					
					if(Tools::getValue('cercaiprodotti') == 'totaleacqannodesc') {
						array_multisort($tot_anno, SORT_DESC, $resultsclienti);
					}
					else if(Tools::getValue('cercaiprodotti') == 'totaleacqannoasc') {
						array_multisort($tot_anno, SORT_ASC, $resultsclienti);
					}
				}
				
				foreach ($resultsprodotti as $row) {
					$num++;
					$tot_acquisti += $row['totale_acq_euro'];
				}
				
				echo "<input type='hidden' name='num' value='".$num."' />		
				<input type='hidden' name='tot_acquisti' value='".$tot_acquisti."' />
				<input type='hidden' name='data_dal' value='".$_POST['arco_dal']."' />
				<input type='hidden' name='data_al' value='".$_POST['arco_al']."' />
				";	
				
				echo "<table class='table'><tr><td>Totale prodotti trovati</td><td><strong>$num</strong></td></tr>";
				echo "<tr><td>Totale vendite</td><td><strong>".Tools::displayPrice($tot_acquisti,1)."</strong></td></tr></table><br />";
				echo "<table class='table'><thead class='persist-header'>";
				echo "<tr>";
				echo "<th style='width:30px'>N.</th>";
				echo "<th style='width:150px'>Codice prodotto
				<br />
				<input type='submit' name='cercaiprodotti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='referenceasc' /></a>   
				<input type='submit' name='cercaiprodotti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='referencedesc' /></a>   
				</th>";
				echo "<th style='width:450px'>Descrizione prodotto
				<br />
				<input type='submit' name='cercaiprodotti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='prodottoasc' /></a>   
				<input type='submit' name='cercaiprodotti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='prodottodesc' /></a> 
				</th>";
				echo "<th style='width:50px'>Qta venduti
				<br />
				<input type='submit' name='cercaiprodotti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='qtavendutoasc' /></a>   
				<input type='submit' name='cercaiprodotti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='qtavendutodesc' /></a>   
				</th>";
				
				if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'preventivi') {
				}
				else if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'ordinato') {
					echo "<th style='width:80px'>Totale ordinato in euro
					<br />
					<input type='submit' name='cercaiprodotti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='totaleacqasc' /></a>   
					<input type='submit' name='cercaiprodotti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='totaleacqdesc' /></a> 
					</th>
					<!-- <th style='width:80px'>ML</th><th>ML %</th> -->
					";
				}
				else {
					echo "<th style='width:80px'>Totale fatturato in euro
					<br />
					<input type='submit' name='cercaiprodotti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='totaleacqasc' /></a>   
					<input type='submit' name='cercaiprodotti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='totaleacqdesc' /></a> 
					</th>
					";
				}
				
				echo "</tr></thead><tbody>";
				
				$i = 1;
				
				foreach ($resultsprodotti as $row) {
					
					
					$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee));
					
					
					$tokenProdotti = Tools::getAdminToken('AdminCatalogExFeatures'.(int)Tab::getIdFromClassName('AdminCatalogExFeatures').(int)$cookie->id_employee);
					
					echo "<tr>";
					echo "<td style='width:30px'>".$i."</td>";
					echo "<td style='width:150px'><!-- <a href='index.php?tab=AdminCatalogExFeatures&id_product=".$row['id_product']."&updateproduct&token=".$tokenProdotti."' target='_blank'> -->".($_POST['su_tipo'] == 'ordinato' ? $row['product_name'] : $row['cod_articolo'])."<!-- </a> --></td>";
					echo "<td style='width:450px'>".($_POST['su_tipo'] == 'fatturato' ? ($row['cod_articolo'] == '' ? 'PRODOTTI SENZA CODICE' : $row['desc_articolo']) : $row['product_name'])."</td>";
					echo "<td style='width:50px; text-align:right'>".$row['quantita_venduto']."</td>";
					if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'preventivi') {
					}
					else {
						echo "<td style='text-align:right'>".Tools::displayPrice($row['totale_acq_euro'],1)."</td>";
					}
					
					if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'ordinato') {
						echo "<!-- <td style='text-align:right'>".Tools::displayPrice($row['margine'],1)."</td>";
						echo "<td style='text-align:right'>".number_format(((1-($row['tot_acquisti']/$row['totale_acq_euro']))*100),2,",","")."%</td> -->";
					}
					
					echo "</tr>";
					
					$i++;
				
				}
				
				echo "</tbody></table>";
				echo "</form>";
			}
			
			else if(isset($_POST['esportaexcel'])) { 
				ini_set("memory_limit","892M");
				set_time_limit(3600);
				require_once 'esportazione-catalogo/Classes/PHPExcel.php';
				$objPHPExcel = new PHPExcel();
				
				$resultsprodotti= Db::getInstance()->ExecuteS(stripslashes($_POST['querysql']));
				
				if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'preventivi') {
					$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'PRODOTTI VENDUTI')
					->setCellValue('A2', 'Prodotti selezionati: '.$_POST['prodottiinclusi'].'')
					->setCellValue('A3', 'Categorie: '.$_POST['macrocategorieincluse'].' '.$_POST['categorieincluse'].'')
					->setCellValue('A4', 'Marchi: '.$_POST['marchiinclusi'].'')
					->setCellValue('A5', $_POST['strisciaamazon'].'')
					->setCellValue('C1', 'Data esportazione: '.date('d/m/Y H:i:s').' - Periodo dal '.$_POST['data_dal'].' al '.$_POST['data_al'])
					->setCellValue('H1', 'Numero prodotti trovati: '.$_POST['num'].'')
					->setCellValue('H2', 'Totale fatturato: '.str_replace(".",",",$_POST['tot_acquisti']).' euro')
					->setCellValue('A6', 'N.')
					->setCellValue('B6', 'Cod. prod.')
					->setCellValue('C6', 'Desc. prod.')
					->setCellValue('D6', 'Qta')
					;
				}
				else {
					$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'PRODOTTI VENDUTI')
					->setCellValue('A2', 'Prodotti selezionati: '.$_POST['prodottiinclusi'].'')
					->setCellValue('A3', 'Categorie: '.$_POST['macrocategorieincluse'].' '.$_POST['categorieincluse'].'')
					->setCellValue('A4', 'Marchi: '.$_POST['marchiinclusi'].'')
					->setCellValue('A5', $_POST['strisciaamazon'].'')
					->setCellValue('C1', 'Data esportazione: '.date('d/m/Y H:i:s').' - Periodo dal '.$_POST['data_dal'].' al '.$_POST['data_al'])
					->setCellValue('H1', 'Numero prodotti trovati: '.$_POST['num'].'')
					->setCellValue('H2', 'Totale '.($_POST['su_tipo'] == 'fatturato' ? 'Fatturato' : 'Ordinato').': '.str_replace(".",",",$_POST['tot_acquisti']).' euro')
					->setCellValue('A6', 'N.')
					->setCellValue('B6', 'Cod. prod.')
					->setCellValue('C6', 'Desc. prod.')
					->setCellValue('D6', 'Qta')
					->setCellValue('E6', ''.($_POST['su_tipo'] == 'fatturato' ? 'Fatturato' : 'Ordinato').'')
					;
				}
				
				$k = 7;
				$i = 1;
				
				$objPHPExcel->getActiveSheet()->getStyle("I")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("J")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("K")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				
				
				foreach ($resultsprodotti as $row) {
					
			
				if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'preventivi') {
					$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue("A$k", $i)
						->setCellValue("B$k", $row['reference'])
						->setCellValue("C$k", $row['name'])
						->setCellValue("D$k", $row['quantita_venduto']);
				}
				
				else {
						
					$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue("A$k", $i)
						->setCellValue("B$k", ''.($_POST['su_tipo'] == 'fatturato' ? $row['cod_articolo'] : $row['product_reference']).'')
						->setCellValue("C$k", ''.($_POST['su_tipo'] == 'fatturato' ? ($row['cod_articolo'] == '' ? 'PRODOTTI SENZA CODICE' : $row['desc_articolo']) : $row['product_name']).'')
						->setCellValue("D$k", $row['quantita_venduto'])
						->setCellValue("E$k", $row['totale_acq_euro']);
				}		
						
						$objPHPExcel->getActiveSheet()->getStyle("B$k")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
						$objPHPExcel->getActiveSheet()->getStyle("D$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
						$i++;
						$k++;
					
				}
				
			
				$objPHPExcel->getActiveSheet()->setTitle('Clienti');

				$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	

				$objPHPExcel->getActiveSheet()->getStyle('A6:E6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
					
				$objPHPExcel->getActiveSheet()->getStyle('A6:E6')->getFill()->getStartColor()->setRGB('FFFF00');	

				$objPHPExcel->getActiveSheet()->getStyle("A6:E$k")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('A6:E6')->getFont()->setBold(true);

				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
				
				
				// Set active sheet index to the first sheet, so Excel opens this as the first sheet
				$objPHPExcel->setActiveSheetIndex(0);

				$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
				$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
				PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

				$data = date("Ymd");

				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->setPreCalculateFormulas(false);
				$objWriter->save(str_replace(".php", ".xls", "esportazione-catalogo/catalogo_xls/prodotti-venduti-$data.php"));

				$this->_html .= "<div style='visibility:hidden'><br /><br />FILE PRONTO PER IL DOWNLOAD: <a id='scaricaexcelstatistiche' href='http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-venduti-$data.xls'>CLICCA QUI PER SCARICARE</a>!</div>";

				header("Location: http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-venduti-$data.xls");
					
				
				
			
			}
			
			else {
			
			
				echo '<script type="text/javascript" src="../js/select2.js"></script>
				<script type="text/javascript">
				$(document).ready(function() { $("#per_prodotto").select2(); });
				
				$(document).ready(function() { $("#per_categoria").select2(); });
				
				$(document).ready(function() { $("#per_macrocategoria").select2(); });
				
				$(document).ready(function() { $("#per_marchio").select2(); });
				</script>
				
				
				<form name="cercaclienti" method="post" action="">
				
				Cerca dati su preciso prodotto:<br />
				<select multiple id="per_prodotto" name="per_prodotto[]" style="width:600px">
				<option name="0" value="0">--- Scegli un prodotto ---</option>
				';
				
				
				$results = Db::getInstance()->ExecuteS("SELECT product.id_product, product.reference, product.price, product_lang.name FROM product JOIN product_lang ON product.id_product = product_lang.id_product WHERE product_lang.id_lang = 5 ORDER BY name");
				
				foreach ($results as $row) {
				
					echo "<option name='$row[id_product]' value='$row[id_product]'"; 
					echo ">$row[id_product] - $row[name] ($row[reference]) - ".Tools::displayPrice($row['price'], 1, false)."</option>";
				
				}
					
				echo "</select><br /><br />";
				
				echo 'Cerca dati su prodotti di una macrocategoria:<br />
				<select multiple id="per_macrocategoria" name="per_macrocategoria[]" style="width:600px">
				<option name="0" value="0">--- Scegli una macrocategoria ---</option>
				';
				
				
				$resultscat = Db::getInstance()->ExecuteS("SELECT category.id_category, category_lang.name FROM category JOIN category_lang ON category.id_category = category_lang.id_category WHERE category_lang.id_lang = 5 AND category.id_parent = 1 ORDER BY name");
				
				foreach ($resultscat as $rowcat) {
				
					echo "<option name='$rowcat[id_category]' value='$rowcat[id_category]'"; 
					echo ">$rowcat[id_category] - $rowcat[name]</option>";
				
				}
					
				echo "</select><br /><br />";
				
				echo 'Cerca dati su prodotti di una sottocategoria:<br />
				<select multiple id="per_categoria" name="per_categoria[]" style="width:600px">
				<option name="0" value="0">--- Scegli una categoria ---</option>
				';
				
				
				$resultscat = Db::getInstance()->ExecuteS("SELECT category.id_category, category_lang.name FROM category JOIN category_lang ON category.id_category = category_lang.id_category WHERE category_lang.id_lang = 5 AND category.id_parent != 1 ORDER BY name");
				
				foreach ($resultscat as $rowcat) {
				
					echo "<option name='$rowcat[id_category]' value='$rowcat[id_category]'"; 
					echo ">$rowcat[id_category] - $rowcat[name]</option>";
				
				}
					
				echo "</select><br /><br />";
				
				
				
				echo 'Cerca dati su prodotti di un marchio:<br />
				<select multiple id="per_marchio" name="per_marchio[]" style="width:600px">
				<option name="0" value="0">--- Scegli un marchio ---</option>
				';
				
				
				$resultsman = Db::getInstance()->ExecuteS("SELECT id_manufacturer, name FROM manufacturer ORDER BY name");
				
				foreach ($resultsman as $rowman) {
				
					echo "<option name='$rowman[id_manufacturer]' value='$rowman[id_manufacturer]'"; 
					echo ">$rowman[id_manufacturer] - $rowman[name]</option>";
				
				}
					
				echo "</select><br /><br />";
				
				echo "<input type='checkbox' value='0' name='escludi-privati' /> Escludi privati<br /><br />";
				
				echo "Seleziona un arco temporale:<br /><br />";
				
				include_once('functions.php');
			
				$this->includeDatepickerZ(array('arco_dal', 'arco_al'), true);
				
				echo 'Dal <input type="text" id="arco_dal" name="arco_dal"  /> al <input type="text" id="arco_al" name="arco_al"  /><br /><br />';
				
				echo '<input type="radio" name="su_tipo" value="fatturato" checked="checked"> Sul fatturato<br />
				<input type="radio" name="su_tipo" value="ordinato"> Sull\'ordinato<br />
				<input type="radio" name="su_tipo" value="preventivi"> Sui preventivi<br /><br />
				
				<h2>Amazon</h2> 
				Flaggare le caselle sottostanti per limitare la ricerca agli ordini che provengono da Amazon. <br /><br />
				
				<strong>Canale</strong><br />
				<input type="checkbox" name="AmazonEZ" /> EZ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="AmazonPrime" /> Prime <br /><br />
				
				<strong>Marketplace</strong><br />
				
				<input type="checkbox" name="AmazonItalia" /> Italia&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="AmazonFrancia" /> Francia&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="AmazonGermania" /> Germania&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="AmazonSpagna" /> Spagna&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="AmazonRegnoUnito" /> Regno Unito&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br /><br />
				
				
				
				';
				echo "<input type='submit' name='cercaiprodotti' value='Avvia' class='button' /><br /><br />
				</form>";
			
			}
			
			
			
		}

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	