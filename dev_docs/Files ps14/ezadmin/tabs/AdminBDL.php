<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14632 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminBDL extends AdminTab
{
	public function __construct()
	{
		
		global $cookie;
			
	 	$this->table = 'bdl';
	 	$this->lang = false;
	 	$this->edit = false;
		if($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 1) {
			$this->delete = true;
		}
	 	$this->view = true;
		$this->requiredDatabase = true;

		$this->_select = '
		(CASE c.is_company
		WHEN 1
		THEN c.company
		ELSE
		CONCAT(c.firstname," ",c.lastname)
		END
		) cliente,
		
		(CASE WHEN a.rif_ordine > 0
		THEN a.rif_ordine
		ELSE "--"
		END
		) rif_ordine1,
		
		(
		CASE WHEN a.rif_ordine > 0
		THEN "closed"
		WHEN a.id_bdl < 384
		THEN "closed"
		WHEN a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 0
		THEN "pending1"
		WHEN a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 1
		THEN "closed"
		WHEN a.invio_controllo = 2 AND a.invio_contabilita = 2
		THEN "closed"
		WHEN a.rif_ordine = 0 AND a.invio_controllo = 0
		THEN "open"
		WHEN a.nessun_addebito = 1
		THEN "closed"
		ELSE "open"
		END
		) stato,

			(SELECT CONCAT(firstname," ",LEFT(lastname, 1),".")  FROM employee WHERE id_employee = a.effettuato_da) impiegato,
		
		(CASE c.is_company
		WHEN 1
		THEN c.vat_number
		WHEN 0
		THEN c.tax_code
		ELSE c.tax_code
		END
		) piva_cf';
		
		
		$this->_join = 'JOIN customer c ON a.id_customer = c.id_customer';
		
		$statusArray = array(
			'open' => $this->l('A'),
			'closed' => $this->l('C'),
			'pending1' => $this->l('L')
		);
		
		$imagesArray = array(
			'open' => 'status_red.gif',
			'closed' => 'status_green.gif',
			'pending1' => 'status_orange.gif'
		);
		
		$genders = array(1 => $this->l('M'), 2 => $this->l('F'), 9 => $this->l('?'));
 		$this->fieldsDisplay = array(
		'id_bdl' => array('title' => $this->l('ID'), 'align' => 'center', 'font-size' =>'12px', 'width' => 25, 'widthColumn' => 25,  'tmpTableFilter' => true),
		
		'cliente' => array('title' => $this->l('Cliente'), 'font-size' =>'14px',  'width' => 200,  'widthColumn' => 200, 'tmpTableFilter' => true,  'tmpTableFilter' => true),
		'piva_cf' => array('title' => $this->l('PI/CF'), 'font-size' =>'14px',  'width' => 70, 'widthColumn' => 90,  'tmpTableFilter' => true,  'tmpTableFilter' => true),
		'codice_spring' => array('title' => $this->l('Cod. Spring'), 'align' => 'left', 'font-size' =>'12px',  'width' => 25, 'widthColumn' => 60,  'tmpTableFilter' => true),
		
	//	'title' => array('title' => $this->l('Titolo rec.'), 'width' => 100, 'tmpTableFilter' => true),
	//	'name' => array('title' => $this->l('Prodotto'), 'width' => 100, 'tmpTableFilter' => true),
		'rif_ordine1' => array('title' => $this->l('Rif. Ordine'), 'width' => 30, 'align' => 'right', 'widthColumn' => 60,  'tmpTableFilter' => true),
		'importo' => array('title' => $this->l('Importo'), 'width' => 50, 'align' => 'right', 'prefix' => '<b>', 'suffix' => '</b>', 'price' => true, 'widthColumn' => 50,  'tmpTableFilter' => true),
		'pagato' => array('title' => $this->l('Pagato?'), 'width' => 25, 'align' => 'center', 'active' => 'pagato', 'type' => 'bool', 'orderby' => false, 'widthColumn' => 30,  'tmpTableFilter' => true),
		'stato' => array('title' => $this->l('Status'), 'width' =>30,  'widthColumn' => 30, 'type' => 'select', 'select' => $statusArray, 'icon' => $imagesArray, 'align' => 'center', 'filter_key' => 'stato', 'tmpTableFilter' => true, 'filter_type' => 'string'),
		'fatturato' => array('title' => $this->l('Fatturato?'), 'width' => 25,  'width' => 25,  'align' => 'center', 'active' => 'fatturato', 'type' => 'bool', 'orderby' => false, 'widthColumn' => 30,  'tmpTableFilter' => true),
	//	'invio_controllo' => array('title' => $this->l('Ctrl?'), 'width' => 25, 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false, 'widthColumn' => 30,  'tmpTableFilter' => true),
//'invio_contabilita' => array('title' => $this->l('Contabilita?'), 'width' => 25, 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false, 'widthColumn' => 30,  'tmpTableFilter' => true),
		'impiegato' => array('title' => $this->l('Fatto da'), 'width' => 50, 'widthColumn' => 50,  'tmpTableFilter' => true),
		'date_add' => array('title' => $this->l('Data ins.'), 'width' => 30,  'widthColumn' => 30, 'type' => 'date',  'tmpTableFilter' => true),

		//'connect' => array('title' => $this->l('Connection'), 'width' => 60, 'type' => 'datetime', 'search' => false)
		);
		
		if($cookie->id_employee == 2) {
			$this->_where = ' and a.fatturato = 0 AND a.invio_contabilita > 0 ';
		}

		$this->_where .= ' AND a.nessun_addebito = 0 ';
		
		$this->_where .= 'group by a.id_bdl';


		parent::__construct();
		
		if (isset($_GET['delete'.$this->table]) ) {
		
			Db::getInstance()->executeS('DELETE FROM bdl WHERE id_bdl = '.Tools::getValue('id_bdl'));
		
			Tools::redirectAdmin("index.php?tab=AdminBDL&conf=4&token=".$this->token);
		}
		
		if (isset($_GET['fatturato'.$this->table]) ) {
		
			$fatturato = Db::getInstance()->executeS('SELECT fatturato FROM bdl WHERE id_bdl = '.Tools::getValue('id_bdl'));
			if($fatturato == 0)
				Db::getInstance()->executeS('UPDATE bdl SET fatturato = 1  WHERE id_bdl = '.Tools::getValue('id_bdl'));
			else
				Db::getInstance()->executeS('UPDATE bdl SET fatturato = 1  WHERE id_bdl = '.Tools::getValue('id_bdl'));
				
			Tools::redirectAdmin("index.php?tab=AdminBDL&conf=4&token=".$this->token);
		}
		
		if (isset($_GET['pagato'.$this->table]) ) {
		
			$pagato = Db::getInstance()->executeS('SELECT pagato FROM bdl WHERE id_bdl = '.Tools::getValue('id_bdl'));
			if($pagato == 0)
				Db::getInstance()->executeS('UPDATE bdl SET pagato = 1  WHERE id_bdl = '.Tools::getValue('id_bdl'));
			else
				Db::getInstance()->executeS('UPDATE bdl SET pagato = 1  WHERE id_bdl = '.Tools::getValue('id_bdl'));
				
			Tools::redirectAdmin("index.php?tab=AdminBDL&conf=4&token=".$this->token);
		}
		
		if (isset($_GET['view'.$this->table]) || isset($_GET['statusbdl'])) {
				
				$id_customer = Db::getInstance()->getValue('SELECT id_customer FROM bdl WHERE id_bdl = '.Tools::getValue('id_bdl'));
				$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
				
				Tools::redirectAdmin("index.php?tab=AdminCustomers&id_customer=".$id_customer."&viewcustomer&edit_bdl=".Tools::getValue('id_bdl')."&token=".$tokenCustomers."&tab-container-1=14&backz=lista");
				
		}
	}

	
	public function display()
	{	
			global $cookie, $currentIndex;
			$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
			
			echo "<h1>Buoni di lavoro</h1>";
			
			echo '<div style="display:block; float:left"><a href="index.php?tab=AdminCustomers&id_bdl=vuoto&viewcustomer&id_customer=33208&getPDFBDL=tecnotel&token='.$tokenCustomers.'&tab-container-1=14" class="button">Stampa BDL vuoto Tecnotel</a><br /><br /><a href="index.php?tab=AdminCustomers&id_bdl=vuoto&viewcustomer&id_customer=33208&getPDFBDL=ezdirect&token='.$tokenCustomers.'&tab-container-1=14" class="button">Stampa BDL vuoto Ezdirect</a><br /></div>';
			
			
			echo "<div id='mostra_legenda' style='display:block; float:left; margin-left:15px; margin-top:-5px'>
			<table>
			<tr><td><strong>Pagato?</strong></td><td>  <img src='../img/admin/enabled.gif' alt='Spunta verde' title='Spunta verde' />: gi&agrave; pagato</td><td> -- : da pagare</td></tr>
			<tr><td><strong>Fatturato?</strong></td><td>  <img src='../img/admin/enabled.gif' alt='Spunta verde' title='Spunta verde' />: gi&agrave; fatturato </td><td>  -- : da fatturare</td></tr>
			</table>
			</div>
			<div style='display:block; float:left; margin-left:15px; margin-top:-5px'>
			<table>
			
			<tr><td><strong>Status</strong></td><td>  </td><td>  ROSSO: in lavorazione.<br />GIALLO: attivit&agrave; completata, in attesa di check amministrativo.<br />VERDE: BDL chiuso e verificato.</td></tr></table></div><div style='clear:both'></div>";
			
			if (isset($_GET['view'.$this->table]) || isset($_GET['statusbdl'])) {
				
				$id_customer = Db::getInstance()->getValue('SELECT id_customer FROM bdl WHERE id_bdl = '.Tools::getValue('id_bdl'));
				$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
				
				Tools::redirectAdmin("index.php?tab=AdminCustomers&id_customer=".$id_customer."&viewcustomer&edit_bdl=".Tools::getValue('id_bdl')."&token=".$tokenCustomers."&tab-container-1=14");
				
			}
			
			
			
		else
		{
			$this->getList((int)($cookie->id_lang), !Tools::getValue($this->table.'Orderby') ? 'date_add' : NULL, !Tools::getValue($this->table.'Orderway') ? 'DESC' : NULL);
			$currency = new Currency((int)(Configuration::get('PS_CURRENCY_DEFAULT')));
			$this->displayListHeader();
			$this->displayListContent();
		//	$this->displayListFooter();
			$this->displayListPagination($this->token);
		}
		
		
			
	}
		
	
			public function getList($id_lang, $orderBy = NULL, $orderWay = NULL, $start = 0, $limit = NULL)
	{
		global $cookie;

		/* Manage default params values */
		if (empty($limit))
			$limit = ((!isset($cookie->{$this->table.'_pagination'})) ? $this->_pagination[1] : $limit = $cookie->{$this->table.'_pagination'});

		if (!Validate::isTableOrIdentifier($this->table))
			die (Tools::displayError('Table name is invalid:').' "'.$this->table.'"');

		if (empty($orderBy))
			$orderBy = $cookie->__get($this->table.'Orderby') ? $cookie->__get($this->table.'Orderby') : $this->_defaultOrderBy;
		if (empty($orderWay))
			$orderWay = $cookie->__get($this->table.'Orderway') ? $cookie->__get($this->table.'Orderway') : 'ASC';

		$limit = (int)(Tools::getValue('pagination', $limit));
		$cookie->{$this->table.'_pagination'} = $limit;

		/* Check params validity */
		if (!Validate::isOrderBy($orderBy) OR !Validate::isOrderWay($orderWay)
			OR !is_numeric($start) OR !is_numeric($limit)
			OR !Validate::isUnsignedId($id_lang))
			die(Tools::displayError('get list params is not valid'));

		/* Determine offset from current page */
		if ((isset($_POST['submitFilter'.$this->table]) OR
		isset($_POST['submitFilter'.$this->table.'_x']) OR
		isset($_POST['submitFilter'.$this->table.'_y'])) AND
		!empty($_POST['submitFilter'.$this->table]) AND
		is_numeric($_POST['submitFilter'.$this->table]))
			$start = (int)($_POST['submitFilter'.$this->table] - 1) * $limit;

		/* Cache */
		$this->_lang = (int)($id_lang);
		$this->_orderBy = $orderBy;
		$this->_orderWay = Tools::strtoupper($orderWay);

		/* SQL table : orders, but class name is Order */
		$sqlTable = $this->table == 'order' ? 'orders' : $this->table;
		
		/* Query in order to get results with all fields */
		$sql = 'SELECT SQL_CALC_FOUND_ROWS
			'.($this->_tmpTableFilter ? ' * FROM (SELECT ' : '').'
			'.($this->lang ? 'b.*, ' : '').'a.*, c.firstname, c.lastname, c.company, c.codice_spring, c.vat_number, c.tax_code, c.is_company '.(isset($this->_select) ? ', '.$this->_select.' ' : '').'
			FROM `'._DB_PREFIX_.$sqlTable.'` a
			'.($this->lang ? 'LEFT JOIN `'._DB_PREFIX_.$this->table.'_lang` b ON (b.`'.$this->identifier.'` = a.`'.$this->identifier.'` AND b.`id_lang` = '.(int)($id_lang).')' : '').'
			'.(isset($this->_join) ? $this->_join.' ' : '').'
			WHERE 1 '.(isset($this->_where) ? $this->_where.' ' : '').($this->deleted ? 'AND a.`deleted` = 0 ' : '').(isset($this->_filter) ? $this->_filter : '').'
			'.(isset($this->_group) ? $this->_group.' ' : '').'
			'.((isset($this->_filterHaving) || isset($this->_having)) ? 'HAVING ' : '').(isset($this->_filterHaving) ? ltrim($this->_filterHaving, ' AND ') : '').(isset($this->_having) ? $this->_having.' ' : '').'
			ORDER BY '.(($orderBy == $this->identifier) ? 'a.' : '').'`'.pSQL($orderBy).'` '.pSQL($orderWay).
			($this->_tmpTableFilter ? ') tmpTable WHERE 1'.stripslashes($this->_tmpTableFilter).'' : '').'
			LIMIT '.(int)($start).','.(int)($limit);
		$this->_list = Db::getInstance()->ExecuteS($sql);

		$this->_listTotal = Db::getInstance()->getValue('SELECT FOUND_ROWS() AS `'._DB_PREFIX_.$this->table.'`');
	}
		
	protected function _displayEnableLink($token, $id, $value, $active,  $id_category = NULL, $id_product = NULL)
	{
		global $currentIndex, $cookie;

		if($value != 2)
		{
			echo ''.($cookie->id_employee == 2 || $cookie->id_employee == 6 ? '<a href="'.$currentIndex.'&'.$this->identifier.'='.$id.'&'.$active.$this->table.
			((int)$id_category AND (int)$id_product ? '&id_category='.$id_category : '').'&token='.($token!=NULL ? $token : $this->token).'" onclick=" var surec=window.confirm(\'Vuoi cambiare lo stato di questo BDL?\'); if (surec) { return true; } else { return false; event.stopPropagation(); }">' : '').'
			'.($value ? '<img src="../img/admin/enabled.gif"
			alt="'.($value ? $this->l('') : $this->l('')).'" title="'.($value ? $this->l('Enabled') : $this->l('Disabled')).'" />' : '--').' '.($cookie->id_employee == 2 || $cookie->id_employee == 6 ? '</a>' : '');
		}
	}
	
	public function displayListContent($token = NULL)
	{
		/* Display results in a table
		 *
		 * align  : determine value alignment
		 * prefix : displayed before value
		 * suffix : displayed after value
		 * image  : object image
		 * icon   : icon determined by values
		 * active : allow to toggle status
		 */

		global $currentIndex, $cookie;
		$currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));

		$id_category = 1; // default categ

		$irow = 0;
		if ($this->_list AND isset($this->fieldsDisplay['position']))
		{
			$positions = array_map(create_function('$elem', 'return (int)($elem[\'position\']);'), $this->_list);
			sort($positions);
		}
		if ($this->_list)
		{
			$isCms = false;
			if (preg_match('/cms/Ui', $this->identifier))
				$isCms = true;
			$keyToGet = 'id_'.($isCms ? 'cms_' : '').'category'.(in_array($this->identifier, array('id_category', 'id_cms_category')) ? '_parent' : '');
			foreach ($this->_list AS $tr)
			{
				$id = $tr[$this->identifier];
				echo '<tr'.(array_key_exists($this->identifier,$this->identifiersDnd) ? ' id="tr_'.(($id_category = (int)(Tools::getValue('id_'.($isCms ? 'cms_' : '').'category', '1'))) ? $id_category : '').'_'.$id.'_'.$tr['position'].'"' : '').($irow++ % 2 ? ' class="alt_row"' : '').' '.((isset($tr['color']) AND $this->colorOnBackground) ? 'style="background-color: '.$tr['color'].'"' : '').'>
							<td class="center">';
				if ($this->delete AND (!isset($this->_listSkipDelete) OR !in_array($id, $this->_listSkipDelete)))
					echo '<input type="checkbox" name="'.$this->table.'Box[]" value="'.$id.'" class="noborder" />';
				echo '</td>';
				foreach ($this->fieldsDisplay AS $key => $params)
				{
					$tmp = explode('!', $key);
					$key = isset($tmp[1]) ? $tmp[1] : $tmp[0];
					echo '
					<td '.(isset($params['position']) ? ' id="td_'.(isset($id_category) AND $id_category ? $id_category : 0).'_'.$id.'"' : '').' class="'.((!isset($this->noLink) OR !$this->noLink) ? 'pointer' : '').((isset($params['position']) AND $this->_orderBy == 'position')? ' dragHandle' : ''). (isset($params['align']) ? ' '.$params['align'] : '').'" ';
					if (!isset($params['position']) AND !isset($params['active']) AND (!isset($this->noLink) OR !$this->noLink))
						echo ' onclick="document.location = \''.$currentIndex.'&'.$this->identifier.'='.$id.($this->view? '&view' : '&update').$this->table.'&token='.($token!=NULL ? $token : $this->token).'\'">'.(isset($params['prefix']) ? $params['prefix'] : '');
					else
						echo '>';
					if (isset($params['active']) AND isset($tr[$key]))
						$this->_displayEnableLink($token, $id, $tr[$key], $params['active'], Tools::getValue('id_category'), Tools::getValue('id_product'));
					elseif (isset($params['activeVisu']) AND isset($tr[$key]))
						echo '<img src="../img/admin/'.($tr[$key] ? 'enabled.gif' : 'disabled.gif').'"
						alt="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" title="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" />';
					elseif (isset($params['position']))
					{
						if ($this->_orderBy == 'position' AND $this->_orderWay != 'DESC')
						{
							echo '<a'.(!($tr[$key] != $positions[sizeof($positions) - 1]) ? ' style="display: none;"' : '').' href="'.$currentIndex.
									'&'.$keyToGet.'='.(int)($id_category).'&'.$this->identifiersDnd[$this->identifier].'='.$id.'
									&way=1&position='.(int)($tr['position'] + 1).'&token='.($token!=NULL ? $token : $this->token).'">
									<img src="../img/admin/'.($this->_orderWay == 'ASC' ? 'down' : 'up').'.gif"
									alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>';

							echo '<a'.(!($tr[$key] != $positions[0]) ? ' style="display: none;"' : '').' href="'.$currentIndex.
									'&'.$keyToGet.'='.(int)($id_category).'&'.$this->identifiersDnd[$this->identifier].'='.$id.'
									&way=0&position='.(int)($tr['position'] - 1).'&token='.($token!=NULL ? $token : $this->token).'">
									<img src="../img/admin/'.($this->_orderWay == 'ASC' ? 'up' : 'down').'.gif"
									alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a>';						}
						else
							echo (int)($tr[$key] + 1);
					}
					elseif (isset($params['image']))
					{
						// item_id is the product id in a product image context, else it is the image id.
						$item_id = isset($params['image_id']) ? $tr[$params['image_id']] : $id;
						// If it's a product image
						if (isset($tr['id_image']))
						{
							$image = new Image((int)$tr['id_image']);
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$image->getExistingImgPath().'.'.$this->imageType;
						}else
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$item_id.(isset($tr['id_image']) ? '-'.(int)($tr['id_image']) : '').'.'.$this->imageType;

						echo cacheImage($path_to_image, $this->table.'_mini_'.$item_id.'.'.$this->imageType, 45, $this->imageType);
					}
					elseif (isset($params['icon']) AND (isset($params['icon'][$tr[$key]]) OR isset($params['icon']['default'])))
						echo '<img src="../img/admin/'.(isset($params['icon'][$tr[$key]]) ? $params['icon'][$tr[$key]] : $params['icon']['default'].'" alt="'.$tr[$key]).'" title="'.$tr[$key].'" />';
					elseif (isset($params['price']))
						echo Tools::displayPrice($tr[$key], (isset($params['currency']) ? Currency::getCurrencyInstance((int)($tr['id_currency'])) : $currency), false);
					elseif (isset($params['float']))
						echo rtrim(rtrim($tr[$key], '0'), '.');
					elseif (isset($params['type']) AND $params['type'] == 'date')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang);
					elseif (isset($params['type']) AND $params['type'] == 'datetime')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang, true);
					elseif (isset($tr[$key]))
					{
						$echo = ($key == 'price' ? round($tr[$key], 2) : isset($params['maxlength']) ? Tools::substr($tr[$key], 0, $params['maxlength']).'...' : $tr[$key]);
						echo isset($params['callback']) ? call_user_func_array(array($this->className, $params['callback']), array($echo, $tr)) : $echo;
					}
					else
						echo '--';
					
					if ($params['title'] == 'Status') {
						echo "&nbsp;&nbsp;&nbsp;";
						if ($tr[$key] == 'closed') 
							echo 'C';
						else if ($tr[$key] == 'pending1') 
							echo 'L';
						else if ($tr[$key] == 'open') 
							echo 'A';
					}
					
					
					echo (isset($params['suffix']) ? $params['suffix'] : '').
					'</td>';
					
						
				}

				if ($this->edit OR $this->delete OR ($this->view AND $this->view !== 'noActionColumn'))
				{
					echo '<td class="center" style="white-space: nowrap;">';
					if ($this->view)
						$this->_displayViewLink($token, $id);
					if ($this->edit)
						$this->_displayEditLink($token, $id);
					if ($this->delete AND (!isset($this->_listSkipDelete) OR !in_array($id, $this->_listSkipDelete)))
						$this->_displayDeleteLink($token, $id);
					if ($this->duplicate)
						$this->_displayDuplicate($token, $id);
					echo '</td>';
				}
				echo '</tr>';
			}
		}
	}
		
}

