<?php
 require("../config/settings.inc.php");
 require_once('../config/config.inc.php');
 require_once('init.php');

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Uprava objednávky</title>
<style type="text/css">
<!--
.Stile1 {
    font-size: large;
    font-weight: bold;
}
.Stile2 {font-size: small}
.style1 {
    text-align: right;
}
.style2 {
    text-align: left;
    margin-top: 15px;
    margin-bottom: 15px;
}
.style3 {
    text-align: center;
}
.style4 {
    font-weight: bold;
    margin-top: 10px;
    margin-bottom: 20px;
}
-->
</style>
</head>

<body>

<?php


$id_order_g = htmlspecialchars($_GET['id_order']);
$action_g = htmlspecialchars($_GET['action']); 
$id_product_g = htmlspecialchars($_GET['id_product']);
$id_lang_g = htmlspecialchars($_GET['id_lang']);


if ($id_order_g) 
    $_POST['id_order']=$id_order_g;
?>


<form name="order" method="post" action="order_edit.php?token=<?php echo $_GET['token'];?>">
  <label>Číslo obj.
  <input name="id_order" type="text" value="<?php echo $_POST['id_order'] ?>" size="10" maxlength="10" />
  </label>
  <input name="send" type="submit" value="Najít" />
  <br />
</form>

<?php


if ($_POST['id_order']) {
    if ($action_g=='add_product') {
    $query=" select p.*,pl.name,t.rate as tax_rate,tl.name as tax_name, sp.reduction from ". _DB_PREFIX_."product p join ". _DB_PREFIX_."product_lang pl on p.id_product=pl.id_product ";
    $query.=" left join ". _DB_PREFIX_."tax t on t.id_tax=p.id_tax_rules_group";
    $query.=" left join ". _DB_PREFIX_."tax_lang tl on t.id_tax=tl.id_tax";
    $query.= " LEFT JOIN ". _DB_PREFIX_."specific_price sp ON sp.id_product = p.id_product";
    $query.=" where p.id_product=".$id_product_g. " and pl.id_lang=".$id_lang_g;
    //echo $query; die();
    $res=dbquery($query);
    $products=mysql_fetch_array($res);

    if (is_null($products['tax_rate'])) $products['tax_rate']=0;

    $query="insert into ". _DB_PREFIX_."order_detail (id_order ,product_id ,product_name ,product_quantity ,product_quantity_in_stock ,product_price ,product_ean13 ,product_reference ,product_supplier_reference ,product_weight ,tax_name ,tax_rate, reduction_amount ) values  ";
    $query.="(".$id_order_g.",".$products['id_product'].",'".addslashes($products['name'])."',1,1,";
    $query.=$products['price'].",'".$products['ean13']."','".addslashes($products['reference'])."','".addslashes($products['supplier_reference'])."',".$products['weight'].",'".addslashes($products['tax_name'])."',".$products['tax_rate'].",".$products['reduction'].")";
    // echo $query;
    dbquery($query);
    update_total($id_order_g);
    }

    if ($_POST['order_total']) {
        $total=price($_POST['total_products']*(1+$_POST['tax_rate']/100) )+price($_POST['total_shipping'])+price($_POST['total_wrapping'])-price($_POST['total_discounts']);

        $query="update  ". _DB_PREFIX_."orders set ";
        $query.=" total_discounts=".price($_POST['total_discounts']);
        $query.=" ,total_wrapping=".price($_POST['total_wrapping']);
        $query.=" ,total_shipping=".price($_POST['total_shipping']);
        $query.=" ,total_paid_real=".$total;
        $query.=" ,total_paid=".$total;
        $query.=" where id_order=".$_POST['id_order'];
        $query.=" limit 1";
        dbquery($query);
        echo "<br/><b> Order Modified </b><br/>";

    }

    if ($_POST['Apply']) {

        //delete product
        if ($_POST['product_delete']) {
            foreach ($_POST['product_delete'] as $id_order_detail=>$value) {
            dbquery("delete from ". _DB_PREFIX_."order_detail where id_order_detail=".$id_order_detail);
        }
    }

        if ($_POST['product_price']) {
        foreach ($_POST['product_price'] as $id_order_detail=>$price_product) {
        $qty_difference=$_POST['product_quantity_old'][$id_order_detail]-$_POST['product_quantity'][$id_order_detail];
        $stock=max(0,$_POST['product_stock'][$id_order_detail]+$qty_difference);
        $name=addslashes($_POST['product_name'][$id_order_detail]);

        $price_product = str_replace(',', '.', $price_product);
        
        dbquery("update  ". _DB_PREFIX_."order_detail set product_price=".$price_product." where id_order_detail=".$id_order_detail);
        dbquery("update  ". _DB_PREFIX_."order_detail set product_quantity=".$_POST['product_quantity'][$id_order_detail].", product_quantity_in_stock=".$_POST['product_quantity'][$id_order_detail]." where id_order_detail=".$id_order_detail);
        dbquery("update  ". _DB_PREFIX_."order_detail set product_name='".$name."' where id_order_detail=".$id_order_detail);

        //servirebbe ad aggiornare lo stock, ma si dovrebbe vincolare ad uno stato. Attualmete lo disabilito
        dbquery("update  ". _DB_PREFIX_."product set quantity=".$stock." where id_product=".$_POST['product_id'][$id_order_detail]);

        $total_products+=$_POST['product_quantity'][$id_order_detail]*price($price_product);

        }

        $total_products=$total_products*(1+$_POST['tax_rate']/100);
        dbquery("update  ". _DB_PREFIX_."orders set total_products=".$total_products." where id_order=".$_POST['id_order']);
        update_total($_POST['id_order']);

        }
}

if ($_POST['id_order']) {
$query="select distinct o.*,a.*,p.tax_rate from ". _DB_PREFIX_."orders o,". _DB_PREFIX_."address a,". _DB_PREFIX_."order_detail p  where a.id_address=o.id_address_delivery and o.id_order=p.id_order and o.id_order=".$_POST['id_order'];

$res=dbquery($query);
if (mysql_num_rows($res)>0) {
$order=mysql_fetch_array($res);
$id_customer=$order['id_customer'];
$id_lang=$order['id_lang'];
$id_cart=$order['id_cart'];
$payment=$order['payment'];
$module=$order['module'];
$tax_rate=$order['tax_rate'];
$invoice_number=$order['invoice_number'];
$delivery_number=$order['delivery_number'];
$total_paid_real=$order['total_paid_real'];
$total_products=$order['total_products'];
$total_discounts=$order['total_discounts'];
$total_shipping=$order['total_shipping'];
$total_wrapping=$order['total_wrapping'];
$firstname=$order['firstname'];
$lastname=$order['lastname'];
$company=$order['company'];

}

} 
// echo "alert('Are you sure you want to give us the deed to your house?')"
?>



<p> 
        <table>
            <tr>
                <td>Číslo zákazníka: 
                </td>
                <td><?php echo $id_customer ?>
                </td>
            </tr>
            <tr>
                <td>Jméno:  
                </td>
                <td><?php echo $firstname." ".$lastname. " ".$company ?>
                </td>
            </tr>
        </table>
<table>
<tr>
<td>
<p class="style4">Detail objednávky</p>

<form name="order_total" method="post" action="order_edit.php?token=<?php echo $_GET['token'];?>">
    <table style="width: 100%">
        <tr>
            <td class="style1">
                Sleva: 
              </td>
              <td>
                <input name="total_discounts" type="text"  value="<?php echo $total_discounts ?>" />
              </td>
          </tr>
          <tr>
            <td class="style1">
                  Balné: 
              </td>
              <td>                
                  <input name="total_wrapping" type="text" value="<?php echo $total_wrapping ?>" />
              </td>
          </tr>
          <tr>
            <td class="style1">
                  Poštovné: 
              </td>
              <td>
                  <input name="total_shipping" type="text" value="<?php echo $total_shipping ?>" /> 
              
              </td>
          </tr>
          <tr>
            <td class="style1">
                  Číslo přepravy:           
              </td>
              <td>
                <input name="delivery_number" type="text"  value="<?php echo $delivery_number ?>" />
            </td>
        </tr>
    </table>
    <p class="style2">
          Cena za zboží: <?php echo $total_products ?>
              <input name="total_products" type="hidden"  value="<?php echo $total_products ?>" />
              <input name="tax_rate" type="hidden"  value="<?php echo $tax_rate ?>" />
      <br />
          Cena celkem: <?php echo $total_paid_real ?>
            <input name="total" type="hidden"  id="total_paid_real" value="<?php echo $total_paid_real ?>" />
            <input name="id_order" type="hidden" value="<?php echo $_POST['id_order'] ?>" />
      <br />
      </P>
      <p class="style3">
            <input type="submit" name="order_total"  value="Upravit" />
    </P>

</form>
</td></tr>
</table>
</p>


<p align="center" class="Stile1">
    <a href="add_product.php?id_order=<?php echo $_POST['id_order'] ?>&id_lang=<?php echo $id_lang ?>&token=<?php echo $_GET['token'];?>" target="_self">Přidat zboží</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="index.php?tab=AdminOrders&id_order=<?php echo $_POST['id_order'] ?>&vieworder&token=<?php echo $_GET['token'];?>" target="_self">Zavřít</a>
</p>

<form name="products" method="post" action="order_edit.php?token=<?php echo $_GET['token'];?>">

<table width="100%" border="1" bgcolor="#FFCCCC">
  <tr>
    <td>Číslo zboží</td>
    <td>Kód zboží</td>
    <td>Název</td>
    <td>Cena bez DPH</td>
    <td>DPH</td>
    <td>Cena s PDH</td>
    <td>Počet</td>
    <td>Celkem bez DPH</td>
    <td>Celkem s DPH</td>
    <td>Váha</td>
    <td>Smazat</td>
  </tr>

  <?php
$query="select o.*,p.quantity as stock from ". _DB_PREFIX_."order_detail o left join ". _DB_PREFIX_."product p  on  o.product_id=p.id_product";
$query.=" where id_order=".$_POST['id_order'];
$query.=" order by id_order_detail asc";
  $res1=dbquery($query);
if (mysql_num_rows($res1)>0) {

while ($products=mysql_fetch_array($res1)) {
$tax_rate=$products['tax_rate'];
  echo '<tr>';
  echo '  <td>'.$products['product_id'].'</td>';
  echo '  <td>'.$products['product_reference'].'</td>';
  echo '  <td><input name="product_name['.$products['id_order_detail'].']" type="text" value="'.$products['product_name'].'" /></td>';
  echo '  <td><input name="product_price['.$products['id_order_detail'].']" type="text" value="'.number_format($products['product_price'], 4, ',', '').'" /></td>';
  echo '  <td>'.$products['tax_rate'].'%</td>';
  echo '  <td>'.number_format($products['product_price']*(1+$products['tax_rate']/100),3, ',', '').'</td>';  
  echo '  <td><input name="product_quantity['.$products['id_order_detail'].']" type="text" value="'.$products['product_quantity'].'" /></td>';
  echo '  <td>'.number_format($products['product_price']*$products['product_quantity'],2, ',', '').'</td>';  
  echo '  <td>'.number_format($products['product_price']*$products['product_quantity']*(1+$products['tax_rate']/100),2, ',', '').'</td>';  
  echo '  <td>'.number_format($products['product_weight'],2, ',', '').'</td>';
  echo '  <td><input name="product_delete['.$products['id_order_detail'].']" type="checkbox" /></td>';
  echo '</tr> ';
  echo '  <input name="product_quantity_old['.$products['id_order_detail'].']" type="hidden" value="'.$products['product_quantity'].'" />';
  echo '  <input name="product_id['.$products['id_order_detail'].']" type="hidden" value="'.$products['product_id'].'" />';
  echo '  <input name="product_stock['.$products['id_order_detail'].']" type="hidden" value="'.$products['stock'].'" />';
    ?>
<?php
  }
  }
  ?>
</table>
  <p align="center">
  <input name="Apply" type="submit" value="Upravit" />
  <input name="id_order" type="hidden" value="<?php echo $_POST['id_order'] ?>" />
  <input name="tax_rate" type="hidden" value="<?php echo $tax_rate ?>" />
  <input name="id_lang" type="hidden" value="<?php echo $id_lang ?>" />
  </p>
</form>
</body>

</html>



<?php
}
function dbquery($query) {
$conn = mysql_connect(_DB_SERVER_, _DB_USER_, _DB_PASSWD_) or die ("Connessione non avvenuta");
mysql_select_db(_DB_NAME_, $conn) or die ("Selezione del db non avvenuta");
$query2 = "SET NAMES 'UTF8' ";
$res = @mysql_query($query2, $conn);
$res = @mysql_query($query, $conn); //or die ("Query non eseguita");
mysql_close($conn);
return $res;
}

function connessione() {
$conn = mysql_connect(_DB_SERVER_, _DB_USER_, _DB_PASSWD_) or die ("Connessione non avvenuta");
mysql_select_db(_DB_NAME_, $conn) or die ("Selezione del db non avvenuta");
}
function price($price) {
$price=str_replace(",",".",$price);
return $price;
}

function update_total($id_order) {
$query="select sum(product_price*product_quantity*(1+tax_rate/100)) as total_products,sum(product_price*product_quantity) as total_products_notax  from  ". _DB_PREFIX_."order_detail where id_order=".$id_order;
$res2=dbquery($query);
$products=mysql_fetch_array($res2);
$query="select * from  ". _DB_PREFIX_."orders where id_order=".$id_order;
$res3=dbquery($query);
$order=mysql_fetch_array($res3);
$total=price($products['total_products'])+price($order['total_shipping'])+price($order['total_wrapping'])-price($order['total_discounts']);
$query="update  ". _DB_PREFIX_."orders set ";
$query.=" total_discounts=".$order['total_discounts'];
$query.=" ,total_wrapping=".$order['total_wrapping'];
$query.=" ,total_shipping=".$order['total_shipping'];
$query.=" ,total_products=".$products['total_products_notax'];
$query.=" ,total_paid_real=".$total;
$query.=" ,total_paid=".$total;
$query.=" where id_order=".$id_order;
$query.=" limit 1";

dbquery($query);
}

?>
