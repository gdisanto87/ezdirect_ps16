<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14703 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/


class AdminRMAtodo extends AdminTab
{

	public function __construct()
	{
	 	global $cookie;
	
	 	$this->table = 'action_thread';
	 	$this->lang = false;
	 	$this->className = 'CustomerThread';
	 	$this->edit = false; 
	 	$this->view = noActionColumn; 
	 	$this->delete = false;
		
 		$this->_select = '
		a.id_action,
		a.action_to as id_employee,
		a.action_to as id_employeez,
		a.status as stato,
		re.codice_rma codice_rma, 
		re.rma_product prodotto_rma,
		re.richiesto_rma,
		re.codice_rma_fornitore,
		re.arrivocli,
		re.urgente,
		re.test,
		(CASE c.id_default_group
		WHEN 1
		THEN "Clienti web"
		WHEN 3
		THEN "Rivenditori"
		ELSE "Altro"
		END
		) AS gruppo_clienti,
	c.company,
	(CASE c.is_company
	WHEN 0
	THEN CONCAT(c.firstname," ",c.lastname)
	WHEN 1
	THEN c.company
	ELSE
	CONCAT(c.firstname," ",c.lastname)
	END
	)as customer, 
	(CASE c.is_company
	WHEN 1 
	THEN c.vat_number
	ELSE c.tax_code
	END) AS cfpi,
	a.date_upd AS data_act';
			
		
		
		$this->_group.= "";
		
		/*if(!empty($_POST['cercaticket']) || !$cookie->submitFiltercustomer_thread) {
			$cookie->customer_threadFilter_id_employee = '';
			$cookie->customer_threadFilter_stato = '';
			$cookie->submitFiltercustomer_thread = true;
		}
		else {

		}*/
	
		
		$this->_where = ' AND (a.subject LIKE "%RMA N.%") ';
		
	
		$this->_group .= 'GROUP BY a.id_action';
		
		
		
		$this->_join = '
		LEFT JOIN `'._DB_PREFIX_.'customer` c ON c.`id_customer` = a.`id_customer`
		JOIN `'._DB_PREFIX_.'rma` re ON re.`id_customer_thread` = SUBSTRING(a.riferimento,2)';
		
		
		$the_employees = Db::getInstance()->ExecuteS('
		SELECT `id_employee`, CONCAT(firstname," ",LEFT(lastname, 1),".") AS "name"
		FROM `'._DB_PREFIX_.'employee`
		WHERE `active` = 1
		ORDER BY `email`');
		
		$contactEmployees = array();
		foreach($the_employees as $employee) 
			$contactEmployees[$employee['id_employee']] = $employee['name'];
		
		$contactArray = array();
		$contacts = Contact::getContacts($cookie->id_lang);
		foreach ($contacts AS $contact)
			$contactArray[$contact['id_contact']] = $contact['name'];
			
		$languageArray = array();
		$languages = Language::getLanguages();
		foreach ($languages AS $language)
			$languageArray[$language['id_lang']] = $language['name'];
			
		$statusArray = array(
			'open' => $this->l('Open'),
			'closed' => $this->l('Closed'),
			'pending1' => $this->l('In lavorazione'),
			'pen' => $this->l('Aperto + In lavorazione')
		);
		
		$imagesArray = array(
			'open' => 'status_red.gif',
			'closed' => 'status_green.gif',
			'pending1' => 'status_orange.gif'
		);
		
		// IN CARICO A: nome invece di numero, mettere "employee"
		
		$this->fieldsDisplay = array(
			'id_action' => array('title' => $this->l('ID'), 'filter' => true, 'search' => true, 'width' => 25, 'tmpTableFilter' => true, 'filter_type' => 'string'),
				'stato' => array('title' => $this->l('Status'), 'width' =>80, 'type' => 'select', 'select' => $statusArray, 'icon' => $imagesArray, 'align' => 'center', 'filter_key' => 'stato', 'tmpTableFilter' => true, 'filter_type' => 'string'),
				
			'id_employeez' => array('title' => $this->l('In carico a'), 'width' => 75, 'type' => 'select', 'select' => $contactEmployees, 'filter_key' => 'id_employeez', 'tmpTableFilter' => true),
			
			'customer' => array('title' => $this->l('Customer'), 'width' => 130, 'filter_key' => 'customer', 'tmpTableFilter' => true),
			
			'arrivocli' => array('title' => $this->l('Data arrivo'), 'width' => 60, 'filter_key' => 'codice_rma', 'tmpTableFilter' => true),
			
			'test' => array('title' => $this->l('Testato'), 'width' => 25, 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false, 'widthColumn' => 30),
			'urgente' => array('title' => $this->l('Urgente'), 'width' => 25, 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false, 'widthColumn' => 30),
			
			
		);
		 
		
		 
		parent::__construct();
	}
	
	public function postProcess()
	{
		global $currentIndex, $cookie, $link;
		
		if (Tools::isSubmit('submitFilter'.$this->table) OR $cookie->{'submitFilter'.$this->table} !== false)
		{
			$_POST = array_merge($cookie->getFamily($this->table.'Filter_'), (isset($_POST) ? $_POST : array()));
			foreach ($_POST AS $key => $value)
			{
				/* Extracting filters from $_POST on key filter_ */
				if ($value != NULL AND !strncmp($key, $this->table.'Filter_', 7 + Tools::strlen($this->table)))
				{
					$key = Tools::substr($key, 7 + Tools::strlen($this->table));
					/* Table alias could be specified using a ! eg. alias!field */
					$tmpTab = explode('!', $key);
					$filter = count($tmpTab) > 1 ? $tmpTab[1] : $tmpTab[0];
					if ($field = $this->filterToField($key, $filter))
					{
						$type = (array_key_exists('filter_type', $field) ? $field['filter_type'] : (array_key_exists('type', $field) ? $field['type'] : false));
						if (($type == 'date' OR $type == 'datetime') AND is_string($value))
							$value = unserialize($value);
						$key = isset($tmpTab[1]) ? $tmpTab[0].'.`'.bqSQL($tmpTab[1]).'`' : '`'.bqSQL($tmpTab[0]).'`';
						if (array_key_exists('tmpTableFilter', $field))
							$sqlFilter = & $this->_tmpTableFilter;
						elseif (array_key_exists('havingFilter', $field))
							$sqlFilter = & $this->_filterHaving;
						else
							$sqlFilter = & $this->_filter;

						/* Only for date filtering (from, to) */
						if (is_array($value))
						{
							/*if (isset($value[0]) AND !empty($value[0]))
							{
								if (!Validate::isDate($value[0]))
									$this->_errors[] = Tools::displayError('\'from:\' date format is invalid (YYYY-MM-DD)');
								else
									$sqlFilter .= ' AND (date_add >= \''.pSQL(Tools::dateFrom($value[0])).'\'';
							}

							if (isset($value[1]) AND !empty($value[1]))
							{
								if (!Validate::isDate($value[1]))
									$this->_errors[] = Tools::displayError('\'to:\' date format is invalid (YYYY-MM-DD)');
								else
									$sqlFilter .= ' AND date_add <= \''.pSQL(Tools::dateTo($value[1])).'\'';
							}*/
						}
						else
						{
							$sqlFilter .= ' AND ';
							if ($type == 'int' OR $type == 'bool')
								$sqlFilter .= (($key == $this->identifier OR $key == '`'.$this->identifier.'`' OR $key == '`active`') ? 'a.' : '').pSQL($key).' = '.(int)($value).' ';
							elseif ($type == 'decimal')
								$sqlFilter .= (($key == $this->identifier OR $key == '`'.$this->identifier.'`') ? 'a.' : '').pSQL($key).' = '.(float)($value).' ';
							elseif ($type == 'select')
								$sqlFilter .= (($key == $this->identifier OR $key == '`'.$this->identifier.'`') ? 'a.' : '').pSQL($key).' = \''.pSQL($value).'\' ';
							else
								$sqlFilter .= (($key == $this->identifier OR $key == '`'.$this->identifier.'`') ? 'a.' : '').pSQL($key).' LIKE \'%'.pSQL($value).'%\' ';
						}
					}
				}
			}
		}
		
		if (isset($_POST['submitResetX'.$this->table]))
		{
			$filters = $cookie->getFamily($this->table.'Filter_');
			foreach ($filters AS $cookieKey => $filter)
				if (strncmp($cookieKey, $this->table.'Filter_', 7 + Tools::strlen($this->table)) == 0)
					{
						$key = substr($cookieKey, 7 + Tools::strlen($this->table));
						/* Table alias could be specified using a ! eg. alias!field */
						$tmpTab = explode('!', $key);
						$key = (count($tmpTab) > 1 ? $tmpTab[1] : $tmpTab[0]);
						if (array_key_exists($key, $this->fieldsDisplay))
							unset($cookie->$cookieKey);
					}
			if (isset($cookie->{'submitFilter'.$this->table}))
				unset($cookie->{'submitFilter'.$this->table});
			if (isset($cookie->{$this->table.'Orderby'}))
				unset($cookie->{$this->table.'Orderby'});
			if (isset($cookie->{$this->table.'Orderway'}))
				unset($cookie->{$this->table.'Orderway'});
			unset($_POST);
			
			$cookie->customer_threadFilter_id_employee = $cookie->id_employee;
			$cookie->customer_threadFilter_stato = 'pen';
			$cookie->customer_threadFilter_ticket = '';
			$cookie->customer_threadFilter_contact = '';
			$cookie->customer_threadFilter_customer = '';
			$cookie->customer_threadFilter_gruppo_clienti = '';
			$cookie->customer_threadFilter_cfpi = '';
			$cookie->customer_threadFilter_data_act = '';
			$cookie->submitFiltercustomer_thread = true;
			
			Tools::redirectAdmin($currentIndex.'&token='.$this->token);
		}
		
		if(isset($_GET['del']) && isset($_GET['typedel'])) {

			if(is_numeric($_GET['typedel']) || $_GET['typedel'] == 'messaggio' || $_GET['typedel'] == 'ticket') {
			
				Db::getInstance()->executeS("DELETE customer_thread, customer_message FROM customer_thread JOIN customer_message ON customer_message.id_customer_thread = customer_thread.id_customer_thread WHERE customer_thread.id_customer_thread = ".$_GET['id_customer_thread']."");
				
				if(Tools::getValue('typedel') == 9) {

					Db::getInstance()->executeS("DELETE FROM rma WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
				}
			
			}

			else if($_GET['typedel'] == 'preventivo' || $_GET['typedel'] == 'tirichiamiamonoi') {
			
				Db::getInstance()->executeS("DELETE form_prevendita_thread, form_prevendita_message  FROM form_prevendita_thread JOIN form_prevendita_message ON form_prevendita_message.id_thread = form_prevendita_thread.id_thread WHERE form_prevendita_thread.id_thread = ".$_GET['id_customer_thread']."");
					
			}
			
			else {
			
				Db::getInstance()->executeS("DELETE action_thread, action_message  FROM action_thread JOIN action_message ON action_message.id_action = action_thread.id_action WHERE action_thread.id_action = ".$_GET['id_customer_thread']."");
				Db::getInstance()->execute("DELETE FROM ticket_promemoria WHERE tipo = 'a' AND msg = ".$_GET['id_customer_thread']."");
			
			}
			
			if(Tools::getIsset('backtocustomers')) {
				Tools::redirectAdmin('index.php?tab=AdminCustomers&id_customer='.Tools::getValue('backtocustomers').'&viewcustomer&conf=1&token='.Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee).'&tab-container-1='.Tools::getValue('tab-container-1'));
			}
				
			else {
				Tools::redirectAdmin($currentIndex.'&conf=1&token='.$this->token);
			}
		}
		
		if(isset($_POST['multidelete'])) {
			
			foreach($_POST['customer_threadBox'] as $box) {
			
				$arrbox = explode(":", $box);
		
				if(is_numeric($arrbox[1])) {
				
					Db::getInstance()->executeS("DELETE customer_thread, customer_message FROM customer_thread JOIN customer_message ON customer_message.id_customer_thread = customer_thread.id_customer_thread WHERE customer_thread.id_customer_thread = ".$arrbox[0]."");
				
				}

				else if($arrbox[1] == 'preventivo' || $arrbox[1] == 'tirichiamiamonoi' ) {
				
					Db::getInstance()->executeS("DELETE form_prevendita_thread, form_prevendita_message  FROM form_prevendita_thread JOIN form_prevendita_message ON form_prevendita_message.id_thread = form_prevendita_thread.id_thread WHERE form_prevendita_thread.id_thread = ".$arrbox[0]."");
								
				}
				
				else {
				
					Db::getInstance()->executeS("DELETE action_thread, action_message  FROM action_thread JOIN action_message ON action_message.id_action = action_thread.id_action WHERE action_thread.id_action = ".$arrbox[0]."");
				
				}	
				
			}
			
			Tools::redirectAdmin($currentIndex.'&conf=1&token='.$this->token);
		}
		
		if(isset($_POST['cambiastatus'])) {

			if(Tools::getValue('tipo') == 'ticket') {
				
				Db::getInstance()->ExecuteS("UPDATE customer_thread SET status = '".Tools::getValue('cambiastatus')."' WHERE id_customer_thread = '".Tools::getValue('id_thread')."'");
			
			}
			
			else if(Tools::getValue('tipo') == 'preventivo' || Tools::getValue('tipo') == 'tirichiamiamonoi' ) {
			
				Db::getInstance()->ExecuteS("UPDATE form_prevendita_thread SET status = '".Tools::getValue('cambiastatus')."' WHERE id_thread = '".Tools::getValue('id_thread')."'");
				
				
			
			}
			
			else if(Tools::getValue('tipo') == 'to-do') {
			
				Db::getInstance()->ExecuteS("UPDATE action_thread SET status = '".Tools::getValue('cambiastatus')."' WHERE id_action = '".Tools::getValue('id_thread')."'");
				
				Customer::chiudiOrdineDaAzione(Tools::getValue('id_thread'));
			
			}
			
			if (Tools::getIsset($this->table.'Orderby') && Tools::getValue($this->table.'Orderby') != '' ) {
		
				Tools::redirectAdmin($currentIndex.'&customer_threadOrderby='.Tools::getValue('customer_threadOrderby').'&customer_threadOrderway='.Tools::getValue('customer_threadOrderway').(Tools::getValue('type') > 0 ? '&type='.Tools::getValue('type') : '').(Tools::getValue('date') != '' ? '&date='.Tools::getValue('date') : '').'&token='.$this->token.'&conf=4');
	
			}
			
			else {
			
				Tools::redirectAdmin($currentIndex.'&token='.$this->token.'&conf=4');
			
			}

		}
		
		if(isset($_POST['cambiaincarico'])) {
			
			Customer::cambiaIncaricoVeloce(Tools::getValue('tipo'), Tools::getValue('cambiaincarico'), Tools::getValue('id_thread'), Tools::getValue('precedente_incarico'), Tools::getValue('id_ticket_univoco'), Tools::getValue('id_customer'));
			
			if (Tools::getIsset($this->table.'Orderby') && Tools::getValue($this->table.'Orderby') != '' ) {
		
				Tools::redirectAdmin($currentIndex.'&customer_threadOrderby='.Tools::getValue('customer_threadOrderby').'&customer_threadOrderway='.Tools::getValue('customer_threadOrderway').(Tools::getValue('type') > 0 ? '&type='.Tools::getValue('type') : '').(Tools::getValue('date') != '' ? '&date='.Tools::getValue('date') : '').'&token='.$this->token.'&conf=4');
	
			}
			
			else {
			
				Tools::redirectAdmin($currentIndex.'&token='.$this->token.'&conf=4');
			
			}

		}
		
		

		
		if ($id_customer_thread = (int)Tools::getValue('id_customer_thread'))
		{
			if (($id_contact = (int)Tools::getValue('id_contact')))
				Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'customer_thread SET id_contact = '.(int)$id_contact.' WHERE id_customer_thread = '.(int)$id_customer_thread);
			if ($id_status = (int)Tools::getValue('setstatus'))
			{
				$statusArray = array(1 => 'open', 2 => 'closed', 3 => 'pending1', 4 => 'pending2');
				Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'customer_thread SET status = "'.$statusArray[$id_status].'" WHERE id_customer_thread = '.(int)$id_customer_thread.' LIMIT 1');
			}
			if (isset($_POST['id_employee_forward']))
			{
				// Todo: need to avoid doubles 
				$messages = Db::getInstance()->ExecuteS('
				SELECT ct.*, cm.*, cl.name subject, CONCAT(e.firstname, \' \', e.lastname) employee_name, CONCAT(c.firstname, \' \', c.lastname) customer_name, c.firstname
				FROM '._DB_PREFIX_.'customer_thread ct
				LEFT JOIN '._DB_PREFIX_.'customer_message cm ON (ct.id_customer_thread = cm.id_customer_thread)
				LEFT JOIN '._DB_PREFIX_.'contact_lang cl ON (cl.id_contact = ct.id_contact AND cl.id_lang = '.(int)$cookie->id_lang.')
				LEFT OUTER JOIN '._DB_PREFIX_.'employee e ON e.id_employee = cm.id_employee
				LEFT OUTER JOIN '._DB_PREFIX_.'customer c ON (c.email = ct.email)
				WHERE ct.id_customer_thread = '.(int)Tools::getValue('id_customer_thread').'
				ORDER BY cm.date_add DESC');
				$output = '';
				foreach ($messages AS $message)
					$output .= $this->displayMsg($message, true, (int)Tools::getValue('id_employee_forward'));
				
				$cm = new CustomerMessage();
				$cm->id_employee = (int)$cookie->id_employee;
				$cm->id_customer_thread = (int)Tools::getValue('id_customer_thread');
				$cm->ip_address = ip2long($_SERVER['REMOTE_ADDR']);
				$currentEmployee = new Employee($cookie->id_employee);
				if (($id_employee = (int)Tools::getValue('id_employee_forward')) AND ($employee = new Employee($id_employee)) AND Validate::isLoadedObject($employee))
				{
					$params = array(
					'{id_richiesta}' => "Id ticket: <strong>".$id_customer_thread."</strong>",
					'{messages}' => $output,
					'{employee}' => $currentEmployee->firstname.' '.$currentEmployee->lastname,
					'{comment}' => stripslashes($_POST['message_forward']));
					if (Mail::Send((int)($cookie->id_lang), 'forward_msg', Mail::l('Fwd: Customer message', (int)($cookie->id_lang)), $params,
						$employee->email, $employee->firstname.' '.$employee->lastname,
						$currentEmployee->email, $currentEmployee->firstname.' '.$currentEmployee->lastname,
						NULL, NULL, _PS_MAIL_DIR_, true))
					{
						$cm->message = $this->l('Message forwarded to').' '.$employee->firstname.' '.$employee->lastname."\n".$this->l('Comment:').' '.$_POST['message_forward'];
						$cm->add();
					}
				}
				elseif (($email = Tools::getValue('email')) AND Validate::isEmail($email))
				{
					$params = array(
					'{id_richiesta}' => "Id ticket: <strong>".$id_customer_thread."</strong>",
					'{messages}' => $output,
					'{employee}' => $currentEmployee->firstname.' '.$currentEmployee->lastname,
					'{comment}' => stripslashes($_POST['message_forward']));
					if (Mail::Send((int)($cookie->id_lang), 'forward_msg', Mail::l('Fwd: Customer message', (int)($cookie->id_lang)), $params,
						$email, NULL,
						$currentEmployee->email, $currentEmployee->firstname.' '.$currentEmployee->lastname,
						NULL, NULL, _PS_MAIL_DIR_, true))
					{
						$cm->message = $this->l('Message forwarded to').' '.$email."\n".$this->l('Comment:').' '.$_POST['message_forward'];
						$cm->add();
					}
				}
				else
					echo '<div class="alert error">'.Tools::displayError('Email invalid.').'</div>';
			}
			if (Tools::isSubmit('submitReply'))
			{
				$ct = new CustomerThread($id_customer_thread);
				$cm = new CustomerMessage();
				$cm->id_employee = (int)$cookie->id_employee;
				$cm->id_customer_thread = $ct->id;
				$cm->message = Tools::htmlentitiesutf8(nl2br2(Tools::getValue('reply_message')));
				$cm->ip_address = ip2long($_SERVER['REMOTE_ADDR']);
				if (isset($_FILES) AND !empty($_FILES['joinFile']['name']) AND $_FILES['joinFile']['error'] != 0)
					$this->_errors[] = Tools::displayError('An error occurred with the file upload.');
				elseif ($cm->add())
				{
					$fileAttachment = NULL;
					if (!empty($_FILES['joinFile']['name']))
					{
						$fileAttachment['content'] = file_get_contents($_FILES['joinFile']['tmp_name']);
						$fileAttachment['name'] = $_FILES['joinFile']['name'];
						$fileAttachment['mime'] = $_FILES['joinFile']['type'];
					}
					$params = array(
					'{id_richiesta}' => "Id ticket: <strong>".$ct->id."</strong>",
					'{reply}' => nl2br2(Tools::getValue('reply_message')),
					'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token);
					if (Mail::Send((int)$ct->id_lang, 'reply_msg', Mail::l('Messaggio da Ezdirect', (int)$ct->id_lang), 
						$params, Tools::getValue('msg_email'), NULL, NULL, NULL, $fileAttachment, NULL, 
						_PS_MAIL_DIR_, true))
					{
						$ct->status = 'closed';
						$ct->update();
					}
					Tools::redirectAdmin($currentIndex.'&id_customer_thread='.(int)$id_customer_thread.'&viewcustomer_thread&token='.Tools::getValue('token'));
				}
				else
					$this->_errors[] = Tools::displayError('An error occurred, your message was not sent. Please contact your system administrator.');
			}
		}

		return AdminRMAtodo::postProcessPCT();
	}
	
	public function display()
	{
		global $cookie;

		if (isset($_GET['filename']) AND file_exists(_PS_UPLOAD_DIR_.$_GET['filename']))
			self::openUploadedFile();
		elseif (isset($_GET['view'.$this->table]))
			$this->viewcustomer_thread();
		else
		{
			$this->getList((int)$cookie->id_lang, !Tools::getValue($this->table.'Orderby') ? 'date_add' : NULL, !Tools::getValue($this->table.'Orderway') ? 'DESC' : NULL);
			$this->displayListModificata();
		}
	}

	
	private function openUploadedFile()
	{
		$filename = $_GET['filename'];
		
		$extensions = array('.txt' => 'text/plain', '.rtf' => 'application/rtf', '.doc' => 'application/msword', '.docx'=> 'application/msword',
		'.pdf' => 'application/pdf', '.zip' => 'multipart/x-zip', '.png' => 'image/png', '.jpeg' => 'image/jpeg', '.gif' => 'image/gif', '.jpg' => 'image/jpeg');

		$extension = '';
		foreach ($extensions AS $key => $val)
			if (substr($filename, -4) == $key OR substr($filename, -5) == $key)
			{
				$extension = $val;
				break;
			}

		ob_end_clean();
		header('Content-Type: '.$extension);
		header('Content-Disposition:attachment;filename="'.$filename.'"');
		readfile(_PS_UPLOAD_DIR_.$filename);
		die;
	}
	private function displayMsg($message, $email = false, $id_employee = null)
	{
		global $cookie, $currentIndex;

		$customersToken = Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee));
		$contacts = Contact::getContacts($cookie->id_lang);
		
		if (!$email)
		{
			if (!empty($message['id_product']) AND empty($message['id_employee']))
				$id_order_product = Db::getInstance()->getValue('
				SELECT o.id_order
				FROM '._DB_PREFIX_.'orders o
				LEFT JOIN '._DB_PREFIX_.'order_detail od ON o.id_order = od.id_order
				WHERE o.id_customer = '.(int)$message['id_customer'].'
				AND od.product_id = '.(int)$message['id_product'].'
				ORDER BY o.date_add DESC');
				
				$prod_name = Db::getInstance()->getValue('
				SELECT pl.name
				FROM '._DB_PREFIX_.'orders o
				LEFT JOIN '._DB_PREFIX_.'order_detail od ON o.id_order = od.id_order
				JOIN product_lang pl ON od.product_id = pl.id_product
				WHERE o.id_customer = '.(int)$message['id_customer'].'
				AND pl.id_lang = 5
				AND od.product_id = '.(int)$message['id_product'].'
				ORDER BY o.date_add DESC');
			
			$output = '
			<fieldset style="'.(!empty($message['id_employee']) ? 'background: rgb(255,236,242);' : '').'width:600px;margin-top:10px">
				<legend '.(empty($message['id_employee']) ? '' : 'style="background:rgb(255,210,225)"').'>'.(
					!empty($message['employee_name'])
					? '<img src="../img/t/AdminCustomers.gif" alt="'.Configuration::get('PS_SHOP_NAME').'" /> '.Configuration::get('PS_SHOP_NAME').' - '.$message['employee_name']
					: '<img src="'.__PS_BASE_URI__.'img/admin/tab-customers.gif" alt="'.Configuration::get('PS_SHOP_NAME').'" /> '.(
						!empty($message['id_customer'])
						? '<a href="index.php?tab=AdminCustomers&id_customer='.(int)($message['id_customer']).'&viewcustomer&token='.$customersToken.'" title="'.$this->l('View customer').'">'.$message['customer_name'].'</a>'
						: $message['email']
					)
				).'</legend>
				<div style="font-size:11px">'.(
				
						(!empty($message['id_customer']) AND empty($message['id_employee']))
						? '<b>'.$this->l('Customer ID:').'</b> <a href="index.php?tab=AdminCustomers&id_customer='.(int)($message['id_customer']).'&viewcustomer&token='.$customersToken.'" title="'.$this->l('View customer').'">'.(int)($message['id_customer']).' <img src="../img/admin/search.gif" alt="'.$this->l('view').'" /></a><br />'
						: ''
					).(
				
						(!empty($message['phone']))
						? '<b>'.$this->l('Recapito telefonico:').'</b> '.($message['phone']).' <br />'
						: '<b>'.$this->l('Recapito telefonico:').'</b> <em>Non inserito</em> <br />'
					).'
					<b>'.$this->l('Sent on:').'</b> '.date("d/m/Y, H:i:s", strtotime($message['date_add'])).'<br />'.(
						empty($message['id_employee'])
						? '<b>'.$this->l('Browser:').'</b> '.strip_tags($message['user_agent']).'<br />'
						: ''
					).(
						(!empty($message['file_name']) AND file_exists(_PS_UPLOAD_DIR_.$message['file_name']))
						? '<b>'.$this->l('File attachment').'</b> <a href="index.php?tab=AdminCustomerThreads&id_customer_thread='.$message['id_customer_thread'].'&viewcustomer_thread&token='.Tools::getAdminToken('AdminCustomerThreads'.(int)(Tab::getIdFromClassName('AdminCustomerThreads')).(int)($cookie->id_employee)).'&filename='.$message['file_name'].'" title="'.$this->l('View file').'"><img src="../img/admin/search.gif" alt="'.$this->l('view').'" /></a><br />'
						: ''
					).(
						(!empty($message['id_order']) AND empty($message['id_employee']))
						? '<b>'.$this->l('Order #').'</b> <a href="index.php?tab=AdminCustomers&id_customer='.$message['id_customer'].'&viewcustomer&id_order='.(int)($message['id_order']).'&vieworder&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'&tab-container-1=4" title="'.$this->l('View order').'">'.(int)($message['id_order']).' <img src="../img/admin/search.gif" alt="'.$this->l('view').'" /></a><br />'
						: ''
					).(
						(!empty($message['id_product']) AND empty($message['id_employee']))
						? '<b>'.$this->l('Prodotto:').'</b> '.$prod_name.' <br />'
						: ''
					).'<br />
					<form action="'.Tools::htmlentitiesutf8($_SERVER['REQUEST_URI']).'" method="post">
						<b>'.$this->l('Subject:').'</b>
						<input type="hidden" name="id_customer_message" value="'.$message['id_customer_message'].'" />
						<select name="id_contact" onchange="this.form.submit();">';
			foreach ($contacts as $contact)
				$output .= '<option value="'.(int)$contact['id_contact'].'" '.($contact['id_contact'] == $message['id_contact'] ? 'selected="selected"' : '').'>'.Tools::htmlentitiesutf8($contact['name']).'</option>';
			$output .= '</select>
					</form>';
		}
		else
		{
			$output = '<div style="font-size:11px">
			'.($id_employee ? '<a href="'.Tools::getHttpHost(true).$currentIndex.'&token='.Tools::getAdminToken('AdminCustomerThreads'.(int)(Tab::getIdFromClassName('AdminCustomerThreads')).(int)($id_employee)).'&id_customer_thread='.(int)$message['id_customer_thread'].'&viewcustomer_thread">'.$this->l('View this thread').'</a><br />' : '').'
			<b>'.$this->l('Sent by:').'</b> '.(!empty($message['customer_name']) ? $message['customer_name'].' ('.$message['email'].')' : $message['email'])
			.((!empty($message['id_customer']) AND empty($message['employee_name'])) ? '<br /><b>'.$this->l('Customer ID:').'</b> '.(int)($message['id_customer']).'<br />' : '')
			.((!empty($message['id_order']) AND empty($message['employee_name'])) ? '<br /><b>'.$this->l('Order #').':</b> '.(int)($message['id_order']).'<br />' : '')
			.((!empty($message['id_product']) AND empty($message['employee_name'])) ? '<br /><b>'.$this->l('Product #').':</b> '.(int)($message['id_product']).'<br />' : '')
			.'<br /><b>'.$this->l('Subject:').'</b> '.$message['subject'];
		}
		
		$message['message'] = preg_replace('/(https?:\/\/[a-z0-9#%&_=\(\)\.\? \+\-@\/]{6,1000})([\s\n<])/Uui', '<a href="\1">\1</a>\2', html_entity_decode($message['message'], ENT_NOQUOTES, 'UTF-8'));
		$output .= '<br /><br />
			<b>'.$this->l('Thread ID:').'</b> '.(int)$message['id_customer_thread'].'<br />
			<b>'.$this->l('Message ID:').'</b> '.(int)$message['id_customer_message'].'<br />
			<b>'.$this->l('Message:').'</b><br />
			'.$message['message'].'
		</div>';
		
		if (!$email)
		{
			$iso = Language::getIsoById((int)($cookie->id_lang));
		$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
		$ad = dirname($_SERVER["PHP_SELF"]);
				$output .= '
			<script type="text/javascript">
			var iso = \''.$isoTinyMCE.'\' ;
			var pathCSS = \''._THEME_CSS_DIR_.'\' ;
			var ad = \''.$ad.'\' ;
			</script>
			
			
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce/tiny_mce.js"></script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>
			<script type="text/javascript">
					toggleVirtualProduct(getE(\'is_virtual_good\'));
					unitPriceWithTax(\'unit\');
			</script>';
			if (empty($message['employee_name']))
				$output .= '
				<p style="text-align:right">
					<button style="font-family: Verdana; font-size: 11px; font-weight:bold; height: 65px; width: 120px;" onclick="$(\'#reply_to_'.(int)($message['id_customer_message']).'\').show(500); $(this).hide();">
						<img src="'.__PS_BASE_URI__.'img/admin/contact.gif" alt="" style="margin-bottom: 5px;" /><br />'.$this->l('Reply to this message').'
					</button>
				</p>
				<div id="reply_to_'.(int)($message['id_customer_message']).'" style="display: none; margin-top: 20px;"">
					<form action="'.Tools::htmlentitiesutf8($_SERVER['REQUEST_URI']).'" method="post" enctype="multipart/form-data">
						<p>'.$this->l('Please type your reply below:').'</p>
						<textarea style="width: 450px; height: 175px;" class="rte" name="reply_message">'.str_replace('\r\n', "\n", Configuration::get('PS_CUSTOMER_SERVICE_SIGNATURE', $message['id_lang'])).'</textarea>
						<div style="width: 450px; text-align: right; font-style: italic; font-size: 9px; margin-top: 2px;">
							'.$this->l('Your reply will be sent to:').' '.$message['email'].'
						</div>
						<div style="width: 450px; margin-top: 0px;">
							<input type="file" name="joinFile"/>
						<div>
						<div style="width: 450px; text-align: center;">
							<input type="submit" class="button" name="submitReply" value="'.$this->l('Send my reply').'" style="margin-top:20px;" />
							<input type="hidden" name="id_customer_thread" value="'.(int)($message['id_customer_thread']).'" />
							<input type="hidden" name="msg_email" value="'.$message['email'].'" />
						</div>					
					</form>
				</div>';
			$output .= '
			</fieldset>';
		}
		
		return $output;
	}
	
	public function viewcustomer_thread()
	{
		global $cookie, $currentIndex;	
		
		if (!($thread = $this->loadObject()))
			return;
		$cookie->{'customer_threadFilter_cl!id_contact'} = $thread->id_contact;
		
		$employees = Db::getInstance()->ExecuteS('
		SELECT e.id_employee, e.firstname, e.lastname FROM '._DB_PREFIX_.'employee e
		WHERE e.active = 1 ORDER BY e.lastname ASC');

		echo '
		<h2>'.$this->l('Messages').'</h2>
		<form action="'.Tools::htmlentitiesUTF8($_SERVER['REQUEST_URI']).'" method="post" enctype="multipart/form-data">
			<p>
				<img src="../img/admin/msg-forward.png" alt="" style="vertical-align: middle;" /> '.$this->l('Forward this discussion to an employee:').' 
				<select name="id_employee_forward" style="vertical-align: middle;" onchange="
					if ($(this).val() >= 0)
						$(\'#message_forward\').show(400);
					else
						$(\'#message_forward\').hide(200);
					if ($(this).val() == 0)
						$(\'#message_forward_email\').show(200);
					else
						$(\'#message_forward_email\').hide(200);
				">
					<option value="-1">'.$this->l('-- Choose --').'</option>
					<option value="0">'.$this->l('Someone else').'</option>';
		foreach ($employees AS $employee)
			echo '	<option value="'.(int)($employee['id_employee']).'">'.substr($employee['firstname'], 0, 1).'. '.$employee['lastname'].'</option>';
		echo '	</select>
				<div id="message_forward_email" style="display:none">
					<b>'.$this->l('E-mail').'</b> <input type="text" name="email" />
				</div>
				<div id="message_forward" style="display:none;margin-bottom:10px">
					<textarea name="message_forward" style="width: 500px; height: 80px; margin-top: 15px;" onclick="if ($(this).val() == \''.addslashes($this->l('You can add a comment here.')).'\') { $(this).val(\'\'); }">'.$this->l('You can add a comment here.').'</textarea><br />
					<input type="Submit" name="submitForward" class="button" value="'.$this->l('Forward this discussion').'" style="margin-top: 10px;" />
				</div>
			</p>
		</form>
		<div class="clear">&nbsp;</div>';
		
		$messages = Db::getInstance()->ExecuteS('
		SELECT ct.*, cm.*, cl.name subject, CONCAT(e.firstname, \' \', e.lastname) employee_name, CONCAT(c.firstname, \' \', c.lastname) customer_name, c.firstname
		FROM '._DB_PREFIX_.'customer_thread ct
		LEFT JOIN '._DB_PREFIX_.'customer_message cm ON (ct.id_customer_thread = cm.id_customer_thread)
		LEFT JOIN '._DB_PREFIX_.'contact_lang cl ON (cl.id_contact = ct.id_contact AND cl.id_lang = '.(int)$cookie->id_lang.')
		LEFT JOIN '._DB_PREFIX_.'employee e ON e.id_employee = cm.id_employee
		LEFT JOIN '._DB_PREFIX_.'customer c ON (IFNULL(ct.id_customer, ct.email) = IFNULL(c.id_customer, c.email))
		WHERE ct.id_customer_thread = '.(int)Tools::getValue('id_customer_thread').'
		ORDER BY cm.date_add DESC');
	
		echo '<div style="float:right">';

		$nextThread = Db::getInstance()->getValue('
		SELECT id_customer_thread FROM '._DB_PREFIX_.'customer_thread ct
		WHERE ct.status = "open" AND ct.date_add = (
			SELECT date_add FROM '._DB_PREFIX_.'customer_message
			WHERE (id_employee IS NULL OR id_employee = 0) AND id_customer_thread = '.(int)$thread->id.'
			ORDER BY date_add DESC LIMIT 1
		)
		'.($cookie->{'customer_threadFilter_cl!id_contact'} ? 'AND ct.id_contact = '.(int)$cookie->{'customer_threadFilter_cl!id_contact'} : '').'
		'.($cookie->{'customer_threadFilter_l!id_lang'} ? 'AND ct.id_lang = '.(int)$cookie->{'customer_threadFilter_l!id_lang'} : '').
		' ORDER BY ct.date_add ASC');
				
		if ($nextThread)
			echo $this->displayButton('
			<a href="'.$currentIndex.'&id_customer_thread='.(int)$nextThread.'&viewcustomer_thread&token='.$this->token.'">
				<img src="../img/admin/next-msg.png" title="'.$this->l('Go to the oldest next unanswered message').'" style="margin-bottom: 10px;" />
				<br />'.$this->l('Answer to the next unanswered message in this category').' &gt;
			</a>');
		else
			echo $this->displayButton('
			<img src="../img/admin/msg-ok.png" title="'.$this->l('Go to the oldest next unanswered message').'" style="margin-bottom: 10px;" />
			<br />'.$this->l('The other messages in this category have been answered'));

		if ($thread->status != "closed")
			echo $this->displayButton('
			<a href="'.$currentIndex.'&viewcustomer_thread&setstatus=2&id_customer_thread='.(int)Tools::getValue('id_customer_thread').'&viewmsg&token='.$this->token.'">
				<img src="../img/admin/msg-ok.png" style="margin-bottom:10px" />
				<br />'.$this->l('Set this message as handled').'
			</a>');
			
		if ($thread->status != "pending1")
			echo $this->displayButton('
			<a href="'.$currentIndex.'&viewcustomer_thread&setstatus=3&id_customer_thread='.(int)Tools::getValue('id_customer_thread').'&viewmsg&token='.$this->token.'">
				<img src="../img/admin/msg-pending.png" style="margin-bottom:10px" />
				<br />'.$this->l('Declare this message').'<br />'.$this->l('as "pending 1"').'<br />'.$this->l('(will be answered later)').'
			</a>');
		else
			echo $this->displayButton('
			<a href="'.$currentIndex.'&viewcustomer_thread&setstatus=1&id_customer_thread='.(int)Tools::getValue('id_customer_thread').'&viewmsg&token='.$this->token.'">
				<img src="../img/admin/msg-is-pending.png" style="margin-bottom:10px" />
				<br />'.$this->l('Click here to disable pending status').'
			</a>');
			
		if ($thread->status != "pending2")
			echo $this->displayButton('
			<a href="'.$currentIndex.'&viewcustomer_thread&setstatus=4&id_customer_thread='.(int)Tools::getValue('id_customer_thread').'&viewmsg&token='.$this->token.'">
				<img src="../img/admin/msg-pending.png" style="margin-bottom:10px" />
				<br />'.$this->l('Declare this message').'<br />'.$this->l('as "pending 2"').'<br />'.$this->l('(will be answered later)').'
			</a>');
		else
			echo $this->displayButton('
			<a href="'.$currentIndex.'&viewcustomer_thread&setstatus=1&id_customer_thread='.(int)Tools::getValue('id_customer_thread').'&viewmsg&token='.$this->token.'">
				<img src="../img/admin/msg-is-pending.png" style="margin-bottom:10px" />
				<br />'.$this->l('Click here to disable pending status').'
			</a>');
			
		echo '</div>';
		
		if ($thread->id_customer)
		{
			$customer = new Customer($thread->id_customer);
			$products = $customer->getBoughtProducts();
			$orders = Order::getCustomerOrders($customer->id);
			
			echo '<div style="float:left;width:600px">';
			if ($orders AND sizeof($orders))
			{
				$totalOK = 0;
				$ordersOK = array();
				$tokenOrders = Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).(int)($cookie->id_employee));
				foreach ($orders as $order)
					if ($order['valid'])
					{
						$ordersOK[] = $order;
						$totalOK += $order['total_paid_real'];
					}
				if ($countOK = sizeof($ordersOK))
				{
					echo '<div style="float:left;margin-right:20px;">
					<h2>'.$this->l('Orders').'</h2>
					<table cellspacing="0" cellpadding="0" class="table float">
						<tr>
							<th class="center">'.$this->l('ID').'</th>
							<th class="center">'.$this->l('Date').'</th>
							<th class="center">'.$this->l('Products').'</th>
							<th class="center">'.$this->l('Total paid').'</th>
							<th class="center">'.$this->l('Payment').'</th>
							<th class="center">'.$this->l('State').'</th>
							<th class="center">'.$this->l('Actions').'</th>
						</tr>';
						$irow = 0;
					foreach ($ordersOK AS $order)
						echo '<tr '.($irow++ % 2 ? 'class="alt_row"' : '').' style="cursor: pointer" onclick="document.location = \'?tab=AdminOrders&id_order='.$order['id_order'].'&vieworder&token='.$tokenOrders.'\'">
						<td class="center">'.$order['id_order'].'</td>
							<td>'.date("d/m/Y, H:i:s", strtotime($order['date_add'])).'</td>
							<td align="right">'.$order['nb_products'].'</td>
							<td align="right">'.Tools::displayPrice($order['total_paid_real'], new Currency((int)($order['id_currency']))).'</td>
							<td>'.$order['payment'].'</td>
							<td>'.$order['order_state'].'</td>
							<td align="center"><a href="?tab=AdminOrders&id_order='.$order['id_order'].'&vieworder&token='.$tokenOrders.'"><img src="../img/admin/details.gif" /></a></td>
						</tr>';
					echo '</table>
					<h3 style="color:green;font-weight:700;margin-top:10px">'.$this->l('Validated Orders:').' '.$countOK.' '.$this->l('for').' '.Tools::displayPrice($totalOK, new Currency(Configuration::get('PS_CURRENCY_DEFAULT'))).'</h3>
					</div>';
				}
			}
			if ($products AND sizeof($products))
			{
				echo '<div style="float:left;margin-right:20px">
				<h2>'.$this->l('Products').'</h2>
				<table cellspacing="0" cellpadding="0" class="table">
					<tr>
						<th class="center">'.$this->l('Date').'</th>
						<th class="center">'.$this->l('ID').'</th>
						<th class="center">'.$this->l('Name').'</th>
						<th class="center">'.$this->l('Quantity').'</th>
						<th class="center">'.$this->l('Actions').'</th>
					</tr>';
				$irow = 0;
				$tokenOrders = Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).(int)($cookie->id_employee));
				foreach ($products AS $product)
					echo '
					<tr '.($irow++ % 2 ? 'class="alt_row"' : '').' style="cursor: pointer" onclick="document.location = \'?tab=AdminOrders&id_order='.$product['id_order'].'&vieworder&token='.$tokenOrders.'\'">
						<td>'.date("d/m/Y, H:i:s", strtotime($product['date_add'])).'</td>
						<td>'.$product['product_id'].'</td>
						<td>'.$product['product_name'].'</td>
						<td align="right">'.$product['product_quantity'].'</td>
						<td align="center"><a href="?tab=AdminOrders&id_order='.$product['id_order'].'&vieworder&token='.$tokenOrders.'"><img src="../img/admin/details.gif" /></a></td>
					</tr>';
				echo '</table></div>';
			}
			echo '</div>';
		}
		
		echo '<div style="float:left;margin-top:10px">';
		foreach ($messages AS $message)
			echo $this->displayMsg($message);
		echo '</div><div class="clear">&nbsp;</div>';
	}
	
	private function displayButton($content)
	{
		return '
		<div style="margin-bottom:10px;border:1px solid #005500;width:200px;height:130px;padding:10px;background:#EFE">
			<p style="text-align:center;font-size:15px;font-weight:bold">
				'.$content.'
			</p>
		</div>';
	}
	
		
		public function displayListModificata()
	{
		global $currentIndex;

		$this->displayTop();

		if ($this->edit AND (!isset($this->noAdd) OR !$this->noAdd))
			echo '<br /><a href="'.$currentIndex.'&add'.$this->table.'&token='.$this->token.'"><img src="../img/admin/add.gif" border="0" /> '.$this->l('Add new').'</a><br /><br />';
		/* Append when we get a syntax error in SQL query */
		if ($this->_list === false)
		{
			$this->displayWarning($this->l('Bad SQL query'));
		}
		$cookie->{$this->table.'_pagination'} = 100;
		/* Display list header (filtering, pagination and column names) */
		$this->displayListHeaderModificata();
		if (!sizeof($this->_list))
			echo '<tr><td class="center" colspan="'.(sizeof($this->fieldsDisplay) + 2).'">'.$this->l('No items found').'</td></tr>';

		/* Show the content of the table */
		$this->displayListContentModificata();

		/* Close list table and submit button */
		
		$this->isplayListPagination(Tools::getValue('token'));
		$this->displayListFooter();
		echo '<br /><a href="'.$currentIndex.'&token='.Tools::getValue('token').'">Lista completa ticket/azioni/messaggi</a>';
		
		$params = array(
			$this->l('Total threads') => $all = Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'customer_thread'),
			$this->l('Threads pending') => $pending = Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'customer_thread WHERE status LIKE "%pending%"'),
			$this->l('Total customer messages') => Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'customer_message WHERE id_employee = 0'),
			$this->l('Total employee messages') => Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'customer_message WHERE id_employee != 0'),
			$this->l('Threads unread') => $unread = Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'customer_thread WHERE status = "open"'),
			$this->l('Threads closed') => $all - ($unread + $pending));

		/*echo '<div style="padding 0px;border:1px solid #CFCFCF;width:280px;">
				<h3 class="button" style="margin:0;line-height:23px;height:23px;border:0;padding:0 5px;">'.$this->l('Customer service').' : '.$this->l('Statistics').'</h3>
				<table cellspacing="1" class="table" style="border-collapse:separate;width:280px;border:0">';
		$count = 0;
		foreach ($params as $key => $val)
			echo '<tr '.(++$count % 2 == 0 ? 'class="alt_row"' : '').'><td>'.$key.'</td><td>'.$val.'</td></tr>';
		echo '	</table>
			</div>';
		*/
	}
	
	
	public function displayListContentModificata($token = NULL)
	{
		/* Display results in a table
		 *
		 * align  : determine value alignment
		 * prefix : displayed before value
		 * suffix : displayed after value
		 * image  : object image
		 * icon   : icon determined by values
		 * active : allow to toggle status
		 */

		global $currentIndex, $cookie;
		$currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));

		$id_category = 1; // default categ
		echo "</form>";
		$irow = 0;
		if ($this->_list AND isset($this->fieldsDisplay['position']))
		{
			$positions = array_map(create_function('$elem', 'return (int)($elem[\'position\']);'), $this->_list);
			sort($positions);
		}
		if ($this->_list)
		{
			$isCms = false;
			if (preg_match('/cms/Ui', $this->identifier))
				$isCms = true;
			$keyToGet = 'id_'.($isCms ? 'cms_' : '').'category'.(in_array($this->identifier, array('id_category', 'id_cms_category')) ? '_parent' : '');
			foreach ($this->_list AS $tr)
			{
				$id = $tr[$this->identifier];
				$contact_type = $tr['id_contact'];
				echo '<tr id="'.$tr['id_customer_thread'].'" rel="'.$tr['id_contact'].'" '.($irow++ % 2 ? ' class="alt_row"' : 'class="odd_row"').' '.((isset($tr['color']) AND $this->colorOnBackground) ? 'style="background-color: '.$tr['color'].'"' : '').'>
							<td class="center">';
				if ($this->delete AND (!isset($this->_listSkipDelete) OR !in_array($id, $this->_listSkipDelete)))
					echo '<input type="checkbox" name="'.$this->table.'Box[]" value="'.$id.':'.$contact_type.'" class="noborder" />';
				echo '</td>';
				foreach ($this->fieldsDisplay AS $key => $params)
				{
					$tmp = explode('!', $key);
					$key = isset($tmp[1]) ? $tmp[1] : $tmp[0];
					echo '
					<td '.(isset($params['position']) ? ' id="td_'.(isset($id_category) AND $id_category ? $id_category : 0).'_'.$id.'"' : '').' class="'.((!isset($this->noLink) OR !$this->noLink) ? 'pointer' : '').((isset($params['position']) AND $this->_orderBy == 'position')? ' dragHandle' : ''). (isset($params['align']) ? ' '.$params['align'] : '').'" ';
					
					
					// MODIFICARE QUI
					$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee));
					$idcustct = Db::getInstance()->getValue("SELECT id_customer FROM customer_thread ct WHERE ct.id_customer_thread = ".$id."");
					
					if($params['title'] == 'Status' || $params['title'] == 'In carico a') {
						echo '>';
					}
					else {
					
					
						echo ' onclick="document.location = \'index.php?tab=AdminCustomers&id_customer='.$tr['id_customer'].'&viewcustomer&viewaction&id_action='.$tr['id_action'].'&token='.$tokenCustomers.'&tab-container-1=10\'">';
					}
					
					if($params['title'] == 'Data arrivo')
					{
						$tr[$key] = date('d/m/Y', strtotime($tr[$key]));
						if($tr[$key] == '01/01/1970')
							$tr[$key] = '--';
					}
					
					if (isset($params['active']) AND isset($tr[$key]))
						$this->_displayEnableLink($token, $id, $tr[$key], $params['active'], Tools::getValue('id_category'), Tools::getValue('id_product'));
					else if ($params['title'] == 'Azioni') {
						switch($tr[$key]) {
						
							case 'Assistenza tecnica postvendita': echo '<img src="http://www.ezdirect.it/img/admin/icons/assistenza.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Assistenza': echo '<img src="http://www.ezdirect.it/img/admin/icons/assistenza.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Contabilita': echo '<img src="http://www.ezdirect.it/img/admin/icons/contabilita.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Messaggi': echo '<img src="http://www.ezdirect.it/img/admin/icons/messaggio.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Ordini': echo '<img src="http://www.ezdirect.it/img/admin/icons/ordini.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Rivenditori': echo '<img src="http://www.ezdirect.it/img/admin/icons/rivenditori.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'RMA': echo '<img src="http://www.ezdirect.it/img/admin/icons/rma.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Telefonata': echo '<img src="http://www.ezdirect.it/img/admin/icons/telefonata.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'TODO Amazon': echo '<img src="https://www.ezdirect.it/img/tmp/order_state_mini_22.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Attivita': echo '<img src="http://www.ezdirect.it/img/admin/icons/attivita.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Caso': echo '<img src="http://www.ezdirect.it/img/admin/icons/caso.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Intervento': echo '<img src="http://www.ezdirect.it/img/admin/icons/intervento.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'preventivo': echo '<img src="http://www.ezdirect.it/img/admin/icons/commerciale.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Commerciale': echo '<img src="http://www.ezdirect.it/img/admin/icons/commerciale.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Visita': echo '<img src="http://www.ezdirect.it/img/admin/icons/visita.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Richiesta_RMA': echo '<img src="http://www.ezdirect.it/img/admin/icons/richiesta_rma.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'tirichiamiamonoi': echo '<img src="http://www.ezdirect.it/img/admin/icons/tirichiamiamonoi.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Richiamiamo': echo '<img src="http://www.ezdirect.it/img/admin/icons/tirichiamiamonoi.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							default: echo $tr[$key]; break;
						
						
						}
					}
					elseif (isset($params['activeVisu']) AND isset($tr[$key]))
						echo '<img src="../img/admin/'.($tr[$key] ? 'enabled.gif' : 'disabled.gif').'"
						alt="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" title="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" />';
					elseif (isset($params['position']))
					{
						if ($this->_orderBy == 'position' AND $this->_orderWay != 'DESC')
						{
							echo '<a'.(!($tr[$key] != $positions[sizeof($positions) - 1]) ? ' style="display: none;"' : '').' href="'.$currentIndex.
									'&'.$keyToGet.'='.(int)($id_category).'&'.$this->identifiersDnd[$this->identifier].'='.$id.'
									&way=1&position='.(int)($tr['position'] + 1).'&token='.($token!=NULL ? $token : $this->token).'">
									<img src="../img/admin/'.($this->_orderWay == 'ASC' ? 'down' : 'up').'.gif"
									alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>';

							echo '<a'.(!($tr[$key] != $positions[0]) ? ' style="display: none;"' : '').' href="'.$currentIndex.
									'&'.$keyToGet.'='.(int)($id_category).'&'.$this->identifiersDnd[$this->identifier].'='.$id.'
									&way=0&position='.(int)($tr['position'] - 1).'&token='.($token!=NULL ? $token : $this->token).'">
									<img src="../img/admin/'.($this->_orderWay == 'ASC' ? 'up' : 'down').'.gif"
									alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a>';						}
						else
							echo (int)($tr[$key] + 1);
					}
					elseif (isset($params['image']))
					{
						// item_id is the product id in a product image context, else it is the image id.
						$item_id = isset($params['image_id']) ? $tr[$params['image_id']] : $id;
						// If it's a product image
						if (isset($tr['id_image']))
						{
							$image = new Image((int)$tr['id_image']);
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$image->getExistingImgPath().'.'.$this->imageType;
						}else
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$item_id.(isset($tr['id_image']) ? '-'.(int)($tr['id_image']) : '').'.'.$this->imageType;

						echo cacheImage($path_to_image, $this->table.'_mini_'.$item_id.'.'.$this->imageType, 45, $this->imageType);
					}
					elseif (isset($params['icon']) AND (isset($params['icon'][$tr[$key]]) OR isset($params['icon']['default'])))
						echo '<img style="margin-top:5px; display:block; float:left" src="../img/admin/'.(isset($params['icon'][$tr[$key]]) ? $params['icon'][$tr[$key]] : $params['icon']['default'].'" alt="'.$tr[$key]).'" title="'.$tr[$key].'" />';
					elseif (isset($params['price']))
						echo Tools::displayPrice($tr[$key], (isset($params['currency']) ? Currency::getCurrencyInstance((int)($tr['id_currency'])) : $currency), false);
					elseif (isset($params['float']))
						echo rtrim(rtrim($tr[$key], '0'), '.');
					elseif (isset($params['type']) AND $params['type'] == 'date')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang);
					elseif (isset($params['type']) AND $params['type'] == 'datetime')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang, true);
					elseif (isset($tr[$key]))
					{
						if($params['title'] == 'In carico a') {
						}
						else {
							$echo = ($key == 'price' ? round($tr[$key], 2) : isset($params['maxlength']) ? Tools::substr($tr[$key], 0, $params['maxlength']).'...' : $tr[$key]);
							echo isset($params['callback']) ? call_user_func_array(array($this->className, $params['callback']), array($echo, $tr)) : $echo;
						}
					}
					else
						echo '--';

					echo (isset($params['suffix']) ? $params['suffix'] : '');
					
					
					
					if ($params['title'] == 'Status') {
					
						echo '
						<form style="margin-left:5px; display:block;float:left" name="cambiastato_'.$tr['id_action'].'" id="cambiastato_'.$tr['id_action'].'" action="" method="post" />
						<a id="'.$tr['id_action'].'"> </a>
						<input type="hidden" name="id_customer" value="'.$tr['id_customer'].'" />
						<input type="hidden" name="type" value="A" />
						<input type="hidden" name="customer_threadOrderby" value="'.Tools::getValue('customer_threadOrderby').'" />
						<input type="hidden" name="customer_threadOrderway" value="'.Tools::getValue('customer_threadOrderway').'" />
						<input type="hidden" name="id_thread" value="'.$tr['id_action'].'" />
						<input type="hidden" name="tipo" value="to-do" />
						<select name="cambiastatus" onchange="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { document.forms[\'cambiastato_'.$tr['id_action'].'\'].submit(); } else { this.value=\''.$tr[$key].'\'}">
						<option style="background-image:url(../img/admin/status_green.gif); background-repeat:no-repeat;
						background-position:bottom left;
						padding-left:30px;"  value="closed" '.($tr[$key] == 'closed' ? 'selected="selected"' : '').'>C</option>
						<option style="background-image:url(../img/admin/status_orange.gif); background-repeat:no-repeat;
						background-position:bottom left;
						padding-left:30px;"  value="pending1" '.($tr[$key] == 'pending1' ? 'selected="selected"' : '').'>L</option>
						<option style="background-image:url(../img/admin/status_red.gif); background-repeat:no-repeat;
						background-position:bottom left;
						padding-left:30px;"  value="open" '.($tr[$key] == 'open' ? 'selected="selected"' : '').'>A</option>
						</select>
						</form>
						';
					
					}
					
					if ($params['title'] == 'In carico a') {
						$tiporic = (substr($tr['ticket'],0,1) == 'P' ? 'preventivo' : (substr($tr['ticket'],0,1) == 'R' ? 'tirichiamiamonoi' : ($tr['ticket'] == 'TO DO' ? 'to-do' : 'ticket')));
						echo '
						<form style="margin-left:5px; display:block;float:left" name="cambiaincaricoa_'.$tr['id_action'].'" id="cambiaincaricoa_'.$tr['id_action'].'" action="" method="post" />
						<a id="'.$tr['id_action'].'"> </a>
						<input type="hidden" name="id_customer" value="'.$tr['id_customer'].'" />
						<input type="hidden" name="id_ticket_univoco" value="'.Customer::trovaSigla($tr['id_action'],'A').'" />
						<input type="hidden" name="type" value="A" />
						<input type="hidden" name="customer_threadOrderby" value="'.Tools::getValue('customer_threadOrderby').'" />
						<input type="hidden" name="customer_threadOrderway" value="'.Tools::getValue('customer_threadOrderway').'" />
						<input type="hidden" name="id_thread" value="'.$tr['id_action'].'" />
						<input type="hidden" name="precedente_incarico" value="'.$tr[$key].'" />
						<input type="hidden" name="tipo" value="to-do" />
						<select style="width:100px" name="cambiaincarico" onchange="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { document.forms[\'cambiaincaricoa_'.$tr['id_action'].'\'].submit(); } else { this.value=\''.$tr[$key].'\'}">
						<option value="0" '.($tr[$key] == '0' ? 'selected="selected"' : '').'>--Nessuno--</option>
						<option value="2" '.($tr[$key] == '2' ? 'selected="selected"' : '').'>Barbara G.</option>
						<option value="1" '.($tr[$key] == '1' ? 'selected="selected"' : '').'>Ezio D.</option>
						<option value="6" '.($tr[$key] == '6' ? 'selected="selected"' : '').'>Federico G.</option>
						<option value="17" '.($tr[$key] == '17' ? 'selected="selected"' : '').'>Leonardo B.</option>
						<option value="12" '.($tr[$key] == '12' ? 'selected="selected"' : '').'>Lorenzo M.</option>
						<option value="4" '.($tr[$key] == '4' ? 'selected="selected"' : '').'>Massimo B.</option>
						<option value="7" '.($tr[$key] == '7' ? 'selected="selected"' : '').'>Matteo D.</option>
						<option value="5" '.($tr[$key] == '5' ? 'selected="selected"' : '').'>Paolo D.</option>
						
						</select>
						</form>';
					
					}
					
					
					
					
					echo '</td>';
				}

				if ($this->edit OR $this->delete OR ($this->view AND $this->view !== 'noActionColumn'))
				{
					echo '<td class="center" style="white-space: nowrap;">';
					
					if ($this->edit)
						$this->_displayEditLink($token, $id);
					if ($this->delete AND (!isset($this->_listSkipDelete) OR !in_array($id, $this->_listSkipDelete)))
						$this->_displayDeleteLink($token, $id, $contact_type);
					if ($this->duplicate)
						$this->_displayDuplicate($token, $id);
					echo '</td>';
				}
				echo '</tr>';
			}
		}
	
	}
	
	public function displayListHeaderModificata($token = NULL)
	{
		
		global $currentIndex, $cookie;
		$contacts = Db::getInstance()->ExecuteS('
			SELECT cl.*, COUNT(*) as total, (
				SELECT id_customer_thread
				FROM '._DB_PREFIX_.'customer_thread ct2
				WHERE status = "open" AND ct.id_contact = ct2.id_contact
				ORDER BY date_add ASC
				LIMIT 1			
			) as id_customer_thread
			FROM '._DB_PREFIX_.'customer_thread ct
			LEFT JOIN '._DB_PREFIX_.'contact_lang cl ON (cl.id_contact = ct.id_contact AND cl.id_lang = '.$cookie->id_lang.')
			WHERE ct.status = "open"
			GROUP BY ct.id_contact HAVING COUNT(*) > 0');
		$categories = Db::getInstance()->ExecuteS('
			SELECT cl.*
			FROM '._DB_PREFIX_.'contact ct
			LEFT JOIN '._DB_PREFIX_.'contact_lang cl ON (cl.id_contact = ct.id_contact AND cl.id_lang = '.$cookie->id_lang.')
			WHERE ct.customer_service = 1');
		$dim = count($categories);

		echo '<div style="float:left;border:0;width:900px;">';
		/*foreach ($categories as $key => $val)
		{
			$totalThread = 0;
			$id_customer_thread = 0;
			foreach ($contacts as $tmp => $tmp2)
				if ($val['id_contact'] == $tmp2['id_contact'])
				{
					$totalThread = $tmp2['total'];
					$id_customer_thread = $tmp2['id_customer_thread'];
					break; 
				}
			echo '<div style="background-color:#EFEFEF;float:left;margin:0 10px 10px 0;width:'.($dim > 6 ? '200' : '300').'px;border:1px solid #CFCFCF" >
					<h3 style="overflow:hidden;line-height:25px;color:#812143;height:25px;margin:0;">&nbsp;'.$val['name'].'</h3>'.
					($dim > 6 ? '' : '<p style="overflow:hidden;line-height:15px;height:45px;margin:0;padding:0 5px;">'.$val['description'].'</p>').
					($totalThread == 0 ? '<h3 style="padding:0 5px;margin:0;height:23px;line-height:23px;background-color:#DEDEDE">'.$this->l('No new message').'</h3>' 
					: '<a href="'.$currentIndex.'&token='.Tools::getValue('token').'&type='.$val['id_contact'].'" style="padding:0 5px;display:block;height:23px;line-height:23px;border:0;" class="button">Vedi tutti</a>').'
				</div>';
		}
		
		
		echo '<div style="background-color:#EFEFEF;float:left;margin:0 10px 10px 0;width:'.($dim > 6 ? '200' : '300').'px;border:1px solid #CFCFCF" >
					<h3 style="overflow:hidden;line-height:25px;color:#812143;height:25px;margin:0;">&nbsp;Prevendita/preventivi</h3>'.
					($dim > 6 ? '' : '<p style="overflow:hidden;line-height:15px;height:45px;margin:0;padding:0 5px;">Messaggi prevendita/preventivi</p>').
					($totalThread == 0 ? '<h3 style="padding:0 5px;margin:0;height:23px;line-height:23px;background-color:#DEDEDE">'.$this->l('No new message').'</h3>' 
					: '<a href="'.$currentIndex.'&token='.Tools::getValue('token').'&type=preventivo" style="padding:0 5px;display:block;height:23px;line-height:23px;border:0;" class="button">Vedi tutti</a>').'
				</div>';
		*/	$tokenTickets = Tools::getAdminToken('AdminCustomerThreads'.(int)(Tab::getIdFromClassName('AdminCustomerThreads')).(int)($cookie->id_employee));
		
		
		
		echo '</div>';
		
		echo '<p class="clear">&nbsp;</p>';
		
		echo "
		<style type='text/css'>
	#hint{
		cursor:pointer;
	}
	.tooltip{
		margin:8px;
		text-align:left;
		padding:8px;
		border:1px solid blue;
		background-color:#ffffff;
		position: absolute;
		z-index: 2;
	}
</style>";
echo '
<script type="text/javascript">
	$(document).ready(function() {
		$("#filtro_ticket").keyup(function(){
			$(this).val($(this).val().toUpperCase());
		});
	});		
			</script>';
echo "<script type='text/javascript'>
 
$(document).ready(function() {
	var changeTooltipPosition = function(event) {
	  var tooltipX = event.pageX - 8;
	  var tooltipY = event.pageY + 8;
	  $('div.tooltip').css({top: tooltipY, left: tooltipX});
	};
	var tooltip_content = '';
	
 
	
	var showTooltip = function(event) {
	  $('div.tooltip').remove();
	  $.ajax({
	type: 'POST',
	data: 'id='+$(this).attr('id')+'&rel='+$(this).attr('rel'),
	async: false,
	url: 'customer_message_preview.php',
	success: function(resp)
		{
			tooltip_content = resp;						
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			tooltip_content = '';
			alert('ERROR');					
		}
								
	});
	 
	  $(\"<div class='tooltip'>\"+tooltip_content+\"</div>\")
            .appendTo('body');
	  changeTooltipPosition(event);
	};
	
	var hideTooltip = function() {
	   $('div.tooltip').remove();
	};
 
	$('tr.alt_row, tr.odd_row').bind({
	   mousemove : changeTooltipPosition,
	   mouseenter : showTooltip,
	   mouseleave: hideTooltip
	});
});
 
</script>
		
		<form method='post'>
		Cerca testo nel ticket: <input type='text' name='cercaticket' size='40' /> 
		<input type='hidden' value='data_act' name='customer_threadOrderby' />
		<input type='hidden' value='DESC' name='customer_threadOrderway' />
		<input type='submit' value='Cerca' name='vaicercaticket' class='button' />	<br />	
		</form>";
		
		$isCms = false;
		if (preg_match('/cms/Ui', $this->identifier))
			$isCms = true;
		$id_cat = Tools::getValue('id_'.($isCms ? 'cms_' : '').'category');

		if (!isset($token) OR empty($token))
			$token = $this->token;

		/* Determine total page number */
		$totalPages = ceil($this->_listTotal / Tools::getValue('pagination', (isset($cookie->{$this->table.'_pagination'}) ? $cookie->{$this->table.'_pagination'} : $this->_pagination[0])));
		if (!$totalPages) $totalPages = 1;

		echo '<a name="'.$this->table.'">&nbsp;</a>';
		echo '<form method="post" action="'.$currentIndex;
		if (Tools::getIsset($this->identifier))
			echo '&'.$this->identifier.'='.(int)(Tools::getValue($this->identifier));
		echo '&token='.$token.(isset($_GET['type']) ? '&type='.Tools::getValue('type').'' : '');
		
		if (Tools::getIsset($this->table.'Orderby') && Tools::getValue($this->table.'Orderby') != '' )
			echo '&'.$this->table.'Orderby='.urlencode($this->_orderBy).'&'.$this->table.'Orderway='.urlencode(strtolower($this->_orderWay));
		echo '" class="form">
		<input type="hidden" id="submitFilter'.$this->table.'" name="submitFilter'.$this->table.'" value="0">
		<table>
			<tr>
				<td style="vertical-align: bottom;">
					<span style="float: left;">';

		/* Determine current page number */
		$page = (int)(Tools::getValue('submitFilter'.$this->table));
		if (!$page) $page = 1;
		if ($page > 1)
			echo '
						<input type="image" src="../img/admin/list-prev2.gif" onclick="getE(\'submitFilter'.$this->table.'\').value=1"/>
						&nbsp; <input type="image" src="../img/admin/list-prev.gif" onclick="getE(\'submitFilter'.$this->table.'\').value='.($page - 1).'"/> ';
		echo $this->l('Page').' <b>'.$page.'</b> / '.$totalPages;
		if ($page < $totalPages)
			echo '
						<input type="image" src="../img/admin/list-next.gif" onclick="getE(\'submitFilter'.$this->table.'\').value='.($page + 1).'"/>
						 &nbsp;<input type="image" src="../img/admin/list-next2.gif" onclick="getE(\'submitFilter'.$this->table.'\').value='.$totalPages.'"/>';
		echo '			| '.$this->l('Display').'
						<select name="pagination">';
		/* Choose number of results per page */
		$selectedPagination = Tools::getValue('pagination', (isset($cookie->{$this->table.'_pagination'}) ? $cookie->{$this->table.'_pagination'} : NULL));
		foreach ($this->_pagination AS $value)
			echo '<option value="'.(int)($value).'"'.($selectedPagination == $value ? ' selected="selected"' : (($selectedPagination == NULL && $value == $this->_pagination[2]) ? ' selected="selected2"' : '')).'>'.(int)($value).'</option>';
		echo '
						</select>
						/ '.(int)($this->_listTotal).' '.$this->l('result(s)').'
					</span>
					<span style="float: right;">
						<input type="submit" name="submitResetX'.$this->table.'" value="'.$this->l('Reset').'" class="button" />
						<input type="submit" id="submitFilterButton_'.$this->table.'" name="submitFilter" value="'.$this->l('Filter').'" class="button" />
					</span>
					<span class="clear"></span>
				</td>
			</tr>
			<tr>
				<td>';
		
		/* Display column names and arrows for ordering (ASC, DESC) */
		if (array_key_exists($this->identifier,$this->identifiersDnd) AND $this->_orderBy == 'position')
		{
			echo '
			
			<script type="text/javascript" src="../js/jquery/jquery.tablednd_0_5.js"></script>
			<script type="text/javascript">
				var token = \''.($token!=NULL ? $token : $this->token).'\';
				var come_from = \''.$this->table.'\';
				var alternate = \''.($this->_orderWay == 'DESC' ? '1' : '0' ).'\';
			</script>
			<script type="text/javascript" src="../js/admin-dnd.js"></script>
			';
		}
		echo '<table'.(array_key_exists($this->identifier,$this->identifiersDnd) ? ' id="'.(((int)(Tools::getValue($this->identifiersDnd[$this->identifier], 1))) ? substr($this->identifier,3,strlen($this->identifier)) : '').'"' : '' ).' class="table'.((array_key_exists($this->identifier,$this->identifiersDnd) AND ($this->_orderBy != 'position 'AND $this->_orderWay != 'DESC')) ? ' tableDnD'  : '' ).'" cellpadding="0" cellspacing="0">
			<thead>
				<tr class="nodrag nodrop">
					<th>';
		if ($this->delete)
			echo '		<input type="checkbox" name="checkme" class="noborder" onclick="checkDelBoxes(this.form, \''.$this->table.'Box[]\', this.checked)" />';
		echo '		</th>';
		foreach ($this->fieldsDisplay AS $key => $params)
		{
			echo '	<th '.(isset($params['widthColumn']) ? 'style="width: '.$params['widthColumn'].'px"' : '').'>'.$params['title'];
			if (!isset($params['orderby']) OR $params['orderby'])
			{
				if(!Tools::getIsset('cercaticket'))
				{	
					// Cleaning links
					if (Tools::getValue($this->table.'Orderby') && Tools::getValue($this->table.'Orderway'))
						$currentIndex = preg_replace('/&'.$this->table.'Orderby=([a-z _]*)&'.$this->table.'Orderway=([a-z]*)/i', '', $currentIndex);
					echo '	<br />
							<a href="'.$currentIndex.'&'.$this->identifier.'='.(int)$id_cat.'&'.$this->table.'Orderby='.urlencode($key).'&'.$this->table.'Orderway=desc&submitFiltercustomer_thread=true&token='.$token.(isset($_GET['type']) ? '&type='.Tools::getValue('type').'' : '').'"><img border="0" src="../img/admin/down'.((isset($this->_orderBy) && ($key == $this->_orderBy) && ($this->_orderWay == 'DESC')) ? '_d' : '').'.gif" /></a>
							<a href="'.$currentIndex.'&'.$this->identifier.'='.(int)$id_cat.'&'.$this->table.'Orderby='.urlencode($key).'&'.$this->table.'Orderway=asc&submitFiltercustomer_thread=true&token='.$token.(isset($_GET['type']) ? '&type='.Tools::getValue('type').'' : '').'"><img border="0" src="../img/admin/up'.((isset($this->_orderBy) && ($key == $this->_orderBy) && ($this->_orderWay == 'ASC')) ? '_d' : '').'.gif" /></a>';
				}
			}
			echo '	</th>';
		}

		/* Check if object can be modified, deleted or detailed */
		if ($this->edit OR $this->delete OR ($this->view AND $this->view !== 'noActionColumn'))
			echo '	<th style="width: 52px">'.$this->l('Actions').'</th>';
		echo '	</tr>';
		
		if(!Tools::getIsset('cercaticket'))
		{
					
		echo '
				<tr class="nodrag nodrop" style="height: 35px;">
					<td class="center">';
		if ($this->delete)
			echo '		--';
		echo '		</td>';

		/* Javascript hack in order to catch ENTER keypress event */
		$keyPress = 'onkeypress="formSubmit(event, \'submitFilterButton_'.$this->table.'\');"';

		/* Filters (input, select, date or bool) */
		foreach ($this->fieldsDisplay AS $key => $params)
		{
			$width = (isset($params['width']) ? ' style="width: '.(int)($params['width']).'px;"' : '');
			echo '<td'.(isset($params['align']) ? ' class="'.$params['align'].'"' : '').'>';
			if (!isset($params['type']))
				$params['type'] = 'text';

			$value = Tools::getValue($this->table.'Filter_'.(array_key_exists('filter_key', $params) ? $params['filter_key'] : $key));
			if (isset($params['search']) AND !$params['search'])
			{
				echo '--</td>';
				continue;
			}
			switch ($params['type'])
			{
				case 'bool':
					echo '
					<select name="'.$this->table.'Filter_'.$key.'">
						<option value="">--</option>
						<option value="1"'.($value == 1 ? ' selected="selected"' : '').'>'.$this->l('Yes').'</option>
						<option value="0"'.(($value == 0 AND $value != '') ? ' selected="selected"' : '').'>'.$this->l('No').'</option>
					</select>';
					break;

				case 'date':
				case 'datetime':
					if (is_string($value))
						$value = unserialize($value);
					if (!Validate::isCleanHtml($value[0]) OR !Validate::isCleanHtml($value[1]))
						$value = '';
					$name = $this->table.'Filter_'.(isset($params['filter_key']) ? $params['filter_key'] : $key);
					$nameId = str_replace('!', '__', $name);
					includeDatepicker(array($nameId.'_0', $nameId.'_1'));
					echo $this->l('From').' <input type="text" id="'.$nameId.'_0" name="'.$name.'[0]" value="'.(isset($value[0]) ? $value[0] : '').'"'.$width.' '.$keyPress.' /><br />
					'.$this->l('To').' <input type="text" id="'.$nameId.'_1" name="'.$name.'[1]" value="'.(isset($value[1]) ? $value[1] : '').'"'.$width.' '.$keyPress.' />';
					break;

				case 'select':

					if (isset($params['filter_key']))
					{
						echo '<select onchange="$(\'#submitFilter'.$this->table.'\').focus();$(\'#submitFilter'.$this->table.'\').click();" name="'.$this->table.'Filter_'.$params['filter_key'].'" '.(isset($params['width']) ? 'style="width: '.$params['width'].'px"' : '').'>
								<option value=""'.(($value == 0 AND $value != '') ? ' selected="selected"' : '').'>--</option>';
						if (isset($params['select']) AND is_array($params['select']))
							foreach ($params['select'] AS $optionValue => $optionDisplay)
							{
								echo '<option value="'.$optionValue.'"'.((isset($_POST[$this->table.'Filter_'.$params['filter_key']]) AND Tools::getValue($this->table.'Filter_'.$params['filter_key']) == $optionValue AND Tools::getValue($this->table.'Filter_'.$params['filter_key']) != '') ? ' selected="selected"' : '').'>'.$optionDisplay.'</option>';
								}
						echo '</select>';
						break;
					}

				case 'text':
				default:
					if (!Validate::isCleanHtml($value))
							$value = '';
					echo '<input type="text" '.($key == 'ticket' ? 'id="filtro_ticket"' : '').' name="'.$this->table.'Filter_'.(isset($params['filter_key']) ? $params['filter_key'] : $key).'" value="'.htmlentities($value, ENT_COMPAT, 'UTF-8').'"'.$width.' '.$keyPress.' />';
			}
			echo '</td>';
		}

		if ($this->edit OR $this->delete OR ($this->view AND $this->view !== 'noActionColumn'))
			echo '<td class="center">--</td>';

		echo '</tr>';
		}
		
		echo '
			</thead>';
	}
	
	public function getList($id_lang, $orderBy = NULL, $orderWay = NULL, $start = 0, $limit = NULL)
	{
		global $cookie;

		/* Manage default params values */
		if (empty($limit))
			$limit = ((!isset($cookie->{$this->table.'_pagination'})) ? $this->_pagination[1] : $limit = $cookie->{$this->table.'_pagination'});

		if (!Validate::isTableOrIdentifier($this->table))
			die (Tools::displayError('Table name is invalid:').' "'.$this->table.'"');

		if (empty($orderBy))
			$orderBy = $cookie->__get($this->table.'Orderby') ? $cookie->__get($this->table.'Orderby') : $this->_defaultOrderBy;
		if (empty($orderWay))
			$orderWay = $cookie->__get($this->table.'Orderway') ? $cookie->__get($this->table.'Orderway') : 'ASC';

		$limit = (int)(Tools::getValue('pagination', $limit));
		$cookie->{$this->table.'_pagination'} = $limit;

		/* Check params validity */
		if (!Validate::isOrderBy($orderBy) OR !Validate::isOrderWay($orderWay)
			OR !is_numeric($start) OR !is_numeric($limit)
			OR !Validate::isUnsignedId($id_lang))
			die(Tools::displayError('get list params is not valid'));

		/* Determine offset from current page */
		if ((isset($_POST['submitFilter'.$this->table]) OR
		isset($_POST['submitFilter'.$this->table.'_x']) OR
		isset($_POST['submitFilter'.$this->table.'_y'])) AND
		!empty($_POST['submitFilter'.$this->table]) AND
		is_numeric($_POST['submitFilter'.$this->table]))
			$start = (int)($_POST['submitFilter'.$this->table] - 1) * $limit;

		/* Cache */
		$this->_lang = (int)($id_lang);
		$this->_orderBy = $orderBy;
		$this->_orderWay = Tools::strtoupper($orderWay);

		/* SQL table : orders, but class name is Order */
		$sqlTable = $this->table == 'order' ? 'orders' : $this->table;

		/* Query in order to get results with all fields */
		$sql = 'SELECT SQL_CALC_FOUND_ROWS
			'.($this->_tmpTableFilter || !isset($_GET['customer_threadOrderby']) ? ' * FROM (SELECT ' : '').'
			'.($this->lang ? 'b.*, ' : '').'a.id_customer, a.status, a.date_add, a.date_upd'.(isset($this->_select) ? ', '.$this->_select.' ' : '').'
			FROM `'._DB_PREFIX_.$sqlTable.'` a
			'.($this->lang ? 'LEFT JOIN `'._DB_PREFIX_.$this->table.'_lang` b ON (b.`'.$this->identifier.'` = a.`'.$this->identifier.'` AND b.`id_lang` = '.(int)($id_lang).')' : '').'
			'.(isset($this->_join) ? $this->_join.' ' : '').'
			WHERE 1 '.(isset($this->_where) ? $this->_where.' ' : '').($this->deleted ? 'AND a.`deleted` = 0 ' : '').(isset($this->_filter) ? $this->_filter : '').'
			'.(isset($this->_group) ? $this->_group.' ' : '').'
			'.((isset($this->_filterHaving) || isset($this->_having)) ? 'HAVING ' : '').(isset($this->_filterHaving) ? ltrim($this->_filterHaving, ' AND ') : '').(isset($this->_having) ? $this->_having.' ' : '').'
			ORDER BY '.(($orderBy == $this->identifier) ? 'a.' : '').'`'.pSQL($orderBy).'` '.pSQL($orderWay).
			($this->_tmpTableFilter  || !isset($_GET['customer_threadOrderby']) ? ') tmpTable WHERE 1 '.($this->_tmpTableFilter && !Tools::getIsset('cercaticket') ? '' : 'AND (stato = "open" OR stato = "pending1" OR stato = "pending2" OR stato = "closed" '.(Tools::getIsset('cercaticket') ? 'OR stato = "closed"' : '').') '.(Tools::getIsset('cercaticket') ? '' : '')).' '.($this->_tmpTableFilter && !Tools::getIsset('cercaticket') ? (Tools::getIsset('submitFilter') ? stripslashes($this->_tmpTableFilter) : stripslashes($this->_tmpTableFilter)) : '').' ORDER BY '.(Tools::getIsset($this->table.'Orderby') ? Tools::getValue($this->table.'Orderby').' '.Tools::getValue($this->table.'Orderway') : '(CASE WHEN stato = "open" THEN 3 WHEN stato = "pending1" THEN 2 WHEN stato = "pending2" THEN 1 WHEN stato = "closed" THEN 0 END) DESC, '.(Tools::getIsset('cercaticket') ? '' : '(CASE WHEN id_employee = '.$cookie->id_employee.' THEN 0 ELSE 1 END),').' data_act ASC') : '').'
			LIMIT '.(int)($start).','.(int)($limit);
			
		$this->_list = Db::getInstance()->ExecuteS($sql);
		
		
		$this->_listTotal = Db::getInstance()->getValue('SELECT FOUND_ROWS() AS `'._DB_PREFIX_.$this->table.'`');
	}
	
	protected function _displayDeleteLink($token = NULL, $id, $contact_type)
	{
		global $currentIndex;

		$_cacheLang['Delete'] = $this->l('Delete');
		$_cacheLang['DeleteItem'] = $this->l('Delete item #', __CLASS__, TRUE, FALSE);
		
		echo '
			<a href="'.$currentIndex.'&'.$this->identifier.'='.$id.'&del&typedel='.$contact_type.'&token='.($token!=NULL ? $token : $this->token).'" onclick="return confirm(\''.$_cacheLang['DeleteItem'].$id.' ?'.
					(!is_null($this->specificConfirmDelete) ? '\r'.$this->specificConfirmDelete : '').'\');">
			<img src="../img/admin/delete.gif" alt="'.$_cacheLang['Delete'].'" title="'.$_cacheLang['Delete'].'" /></a>';
	}
	
		/**
	 * Close list table and submit button
	 */
	public function displayListFooter($token = NULL)
	{
		echo '</table></div><div id="bottom_anchor">';
		if ($this->delete)
			echo '<p><input type="submit" class="button" name="multidelete" value="'.$this->l('Delete selection').'" onclick="return confirm(\''.$this->l('Delete selected items?', __CLASS__, TRUE, FALSE).'\');" /></p>';
		echo '
				</td>
			</tr>
		</table>
		<input type="hidden" name="token" value="'.($token ? $token : $this->token).'" />
		</form><script type="text/javascript" src="'._PS_JS_DIR_.'jquery/maintainscroll.jquery.js"></script>
</body>';
		if (isset($this->_includeTab) AND sizeof($this->_includeTab))
			echo '<br /><br />';
			
			echo '	';
	}
	
	
	public function postProcessPCT()
	{
		global $currentIndex, $cookie;
		if (!isset($this->table))
			return false;

		// set token
		$token = Tools::getValue('token') ? Tools::getValue('token') : $this->token;

		// Sub included tab postProcessing
		$this->includeSubTab('postProcess', array('status', 'submitAdd1', 'submitDel', 'delete', 'submitFilter', 'submitReset'));

		/* Delete object image */
		if (isset($_GET['deleteImage']))
		{
			if (Validate::isLoadedObject($object = $this->loadObject()))
				if (($object->deleteImage()))
					Tools::redirectAdmin($currentIndex.'&add'.$this->table.'&'.$this->identifier.'='.Tools::getValue($this->identifier).'&conf=7&token='.$token);
			$this->_errors[] = Tools::displayError('An error occurred during image deletion (cannot load object).');
		}

		/* Delete object */
		elseif (isset($_GET['delete'.$this->table]))
		{
			if ($this->tabAccess['delete'] === '1')
			{
				if (Validate::isLoadedObject($object = $this->loadObject()) AND isset($this->fieldImageSettings))
				{
					// check if request at least one object with noZeroObject
					if (isset($object->noZeroObject) AND sizeof(call_user_func(array($this->className, $object->noZeroObject))) <= 1)
						$this->_errors[] = Tools::displayError('You need at least one object.').' <b>'.$this->table.'</b><br />'.Tools::displayError('You cannot delete all of the items.');
					else
					{
						if ($this->deleted)
						{
							$object->deleteImage();
							$object->deleted = 1;
							if ($object->update())
								Tools::redirectAdmin($currentIndex.'&conf=1&token='.$token);
						}
						elseif ($object->delete())
							Tools::redirectAdmin($currentIndex.'&conf=1&token='.$token);
						$this->_errors[] = Tools::displayError('An error occurred during deletion.');
					}
				}
				else
					$this->_errors[] = Tools::displayError('An error occurred while deleting object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to delete here.');
		}

		/* Change object statuts (active, inactive) */
		elseif ((isset($_GET['status'.$this->table]) OR isset($_GET['status'])) AND Tools::getValue($this->identifier))
		{
			if ($this->tabAccess['edit'] === '1')
			{
				if (Validate::isLoadedObject($object = $this->loadObject()))
				{
					if ($object->toggleStatus())
						Tools::redirectAdmin($currentIndex.'&conf=5'.((($id_category = (int)(Tools::getValue('id_category'))) AND Tools::getValue('id_product')) ? '&id_category='.$id_category : '').'&token='.$token);
					else
						$this->_errors[] = Tools::displayError('An error occurred while updating status.');
				}
				else
					$this->_errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
		}
		/* Move an object */
		elseif (isset($_GET['position']))
		{
			if ($this->tabAccess['edit'] !== '1')
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
			elseif (!Validate::isLoadedObject($object = $this->loadObject()))
				$this->_errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			elseif (!$object->updatePosition((int)(Tools::getValue('way')), (int)(Tools::getValue('position'))))
				$this->_errors[] = Tools::displayError('Failed to update the position.');
			else
				Tools::redirectAdmin($currentIndex.'&'.$this->table.'Orderby=position&'.$this->table.'Orderway=asc&conf=5'.(($id_identifier = (int)(Tools::getValue($this->identifier))) ? ('&'.$this->identifier.'='.$id_identifier) : '').'&token='.$token);
		}
		/* Delete multiple objects */
		elseif (Tools::getValue('submitDel'.$this->table))
		{
			if ($this->tabAccess['delete'] === '1')
			{
				if (isset($_POST[$this->table.'Box']))
				{
					$object = new $this->className();
					if (isset($object->noZeroObject) AND
						// Check if all object will be deleted
						(sizeof(call_user_func(array($this->className, $object->noZeroObject))) <= 1 OR sizeof($_POST[$this->table.'Box']) == sizeof(call_user_func(array($this->className, $object->noZeroObject)))))
						$this->_errors[] = Tools::displayError('You need at least one object.').' <b>'.$this->table.'</b><br />'.Tools::displayError('You cannot delete all of the items.');
					else
					{
						$result = true;
						if ($this->deleted)
						{
							foreach(Tools::getValue($this->table.'Box') as $id)
							{
								$toDelete = new $this->className($id);
								$toDelete->deleted = 1;
								$result = $result AND $toDelete->update();
							}
						}
						else
							$result = $object->deleteSelection(Tools::getValue($this->table.'Box'));

						if ($result)
							Tools::redirectAdmin($currentIndex.'&conf=2&token='.$token);
						$this->_errors[] = Tools::displayError('An error occurred while deleting selection.');
					}
				}
				else
					$this->_errors[] = Tools::displayError('You must select at least one element to delete.');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to delete here.');
		}

		/* Create or update an object */
		elseif (Tools::getValue('submitAdd'.$this->table))
		{
			/* Checking fields validity */
			$this->validateRules();
			if (!sizeof($this->_errors))
			{
				$id = (int)(Tools::getValue($this->identifier));

				/* Object update */
				if (isset($id) AND !empty($id))
				{
					if ($this->tabAccess['edit'] === '1' OR ($this->table == 'employee' AND $cookie->id_employee == Tools::getValue('id_employee') AND Tools::isSubmit('updateemployee')))
					{
						$object = new $this->className($id);
						if (Validate::isLoadedObject($object))
						{
							/* Specific to objects which must not be deleted */
							if ($this->deleted AND $this->beforeDelete($object))
							{
								// Create new one with old objet values
								$objectNew = new $this->className($object->id);
								$objectNew->id = NULL;
								$objectNew->date_add = '';
								$objectNew->date_upd = '';

								// Update old object to deleted
								$object->deleted = 1;
								$object->update();

								// Update new object with post values
								$this->copyFromPost($objectNew, $this->table);
								$result = $objectNew->add();
								if (Validate::isLoadedObject($objectNew))
									$this->afterDelete($objectNew, $object->id);
							}
							else
							{
								$this->copyFromPost($object, $this->table);
								$result = $object->update();
								$this->afterUpdate($object);
							}
							if (!$result)
								$this->_errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.'</b> ('.Db::getInstance()->getMsgError().')';
							elseif ($this->postImage($object->id) AND !sizeof($this->_errors))
							{
								$parent_id = (int)(Tools::getValue('id_parent', 1));
								// Specific back redirect
								if ($back = Tools::getValue('back'))
									Tools::redirectAdmin(urldecode($back).'&conf=4');
								// Specific scene feature
								if (Tools::getValue('stay_here') == 'on' || Tools::getValue('stay_here') == 'true' || Tools::getValue('stay_here') == '1')
									Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$object->id.'&conf=4&updatescene&token='.$token);
								// Save and stay on same form
								if (Tools::isSubmit('submitAdd'.$this->table.'AndStay'))
									Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$object->id.'&conf=4&update'.$this->table.'&token='.$token);
								// Save and back to parent
								if (Tools::isSubmit('submitAdd'.$this->table.'AndBackToParent'))
									Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$parent_id.'&conf=4&token='.$token);
								// Default behavior (save and back)
								Tools::redirectAdmin($currentIndex.($parent_id ? '&'.$this->identifier.'='.$object->id : '').'&conf=4&token='.$token);
							}
						}
						else
							$this->_errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
					}
					else
						$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
				}

				/* Object creation */
				else
				{
					if ($this->tabAccess['add'] === '1')
					{
						$object = new $this->className();
						$this->copyFromPost($object, $this->table);
						if (!$object->add())
							$this->_errors[] = Tools::displayError('An error occurred while creating object.').' <b>'.$this->table.' ('.mysql_error().')</b>';
						elseif (($_POST[$this->identifier] = $object->id /* voluntary */) AND $this->postImage($object->id) AND !sizeof($this->_errors) AND $this->_redirect)
						{
							$parent_id = (int)(Tools::getValue('id_parent', 1));
							$this->afterAdd($object);
							// Save and stay on same form
							if (Tools::isSubmit('submitAdd'.$this->table.'AndStay'))
								Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$object->id.'&conf=3&update'.$this->table.'&token='.$token);
							// Save and back to parent
							if (Tools::isSubmit('submitAdd'.$this->table.'AndBackToParent'))
								Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$parent_id.'&conf=3&token='.$token);
							// Default behavior (save and back)
							Tools::redirectAdmin($currentIndex.($parent_id ? '&'.$this->identifier.'='.$object->id : '').'&conf=3&token='.$token);
						}
					}
					else
						$this->_errors[] = Tools::displayError('You do not have permission to add here.');
				}
			}
			$this->_errors = array_unique($this->_errors);
		}

		/* Cancel all filters for this tab */
		elseif (isset($_POST['submitReset'.$this->table]))
		{
			$filters = $cookie->getFamily($this->table.'Filter_');
			foreach ($filters AS $cookieKey => $filter)
				if (strncmp($cookieKey, $this->table.'Filter_', 7 + Tools::strlen($this->table)) == 0)
					{
						$key = substr($cookieKey, 7 + Tools::strlen($this->table));
						/* Table alias could be specified using a ! eg. alias!field */
						$tmpTab = explode('!', $key);
						$key = (count($tmpTab) > 1 ? $tmpTab[1] : $tmpTab[0]);
						if (array_key_exists($key, $this->fieldsDisplay))
							unset($cookie->$cookieKey);
					}
			if (isset($cookie->{'submitFilter'.$this->table}))
				unset($cookie->{'submitFilter'.$this->table});
			if (isset($cookie->{$this->table.'Orderby'}))
				unset($cookie->{$this->table.'Orderby'});
			if (isset($cookie->{$this->table.'Orderway'}))
				unset($cookie->{$this->table.'Orderway'});
			unset($_POST);
		}

		/* Submit options list */
		elseif (Tools::getValue('submitOptions'.$this->table))
		{
			$this->updateOptions($token);
		}

		/* Manage list filtering */
		elseif (Tools::isSubmit('submitFilter'.$this->table) OR $cookie->{'submitFilter'.$this->table} !== false)
		{
			$_POST = array_merge($cookie->getFamily($this->table.'Filter_'), (isset($_POST) ? $_POST : array()));
			foreach ($_POST AS $key => $value)
			{
				/* Extracting filters from $_POST on key filter_ */
				if ($value != NULL AND !strncmp($key, $this->table.'Filter_', 7 + Tools::strlen($this->table)))
				{
					$key = Tools::substr($key, 7 + Tools::strlen($this->table));
					/* Table alias could be specified using a ! eg. alias!field */
					$tmpTab = explode('!', $key);
					$filter = count($tmpTab) > 1 ? $tmpTab[1] : $tmpTab[0];
					if ($field = $this->filterToField($key, $filter))
					{
						$type = (array_key_exists('filter_type', $field) ? $field['filter_type'] : (array_key_exists('type', $field) ? $field['type'] : false));
						if (($type == 'date' OR $type == 'datetime') AND is_string($value))
							$value = unserialize($value);
						$key = isset($tmpTab[1]) ? $tmpTab[0].'.`'.bqSQL($tmpTab[1]).'`' : '`'.bqSQL($tmpTab[0]).'`';
						if (array_key_exists('tmpTableFilter', $field))
							$sqlFilter = & $this->_tmpTableFilter;
						elseif (array_key_exists('havingFilter', $field))
							$sqlFilter = & $this->_filterHaving;
						else
							$sqlFilter = & $this->_filter;

						/* Only for date filtering (from, to) */
						if (is_array($value))
						{
							if (isset($value[0]) AND !empty($value[0]))
							{
								if (!Validate::isDate($value[0]))
									$this->_errors[] = Tools::displayError('\'from:\' date format is invalid (YYYY-MM-DD)');
								else
									$sqlFilter .= ' AND (( '.$key.' >= \''.pSQL(Tools::dateFrom($value[0])).'\'';
							}

							if (isset($value[1]) AND !empty($value[1]))
							{
								if (!Validate::isDate($value[1]))
									$this->_errors[] = Tools::displayError('\'to:\' date format is invalid (YYYY-MM-DD)');
								else
									$sqlFilter .= ' AND '.$key.' <= \''.pSQL(Tools::dateTo($value[1])).'\') OR ( date_add >= \''.pSQL(Tools::dateFrom($value[0])).'\' AND date_add <= \''.pSQL(Tools::dateTo($value[1])).'\'))';
							}
						}
						else
						{
							$sqlFilter .= ' AND ';
							if ($type == 'int' OR $type == 'bool')
								$sqlFilter .= (($key == $this->identifier OR $key == '`'.$this->identifier.'`' OR $key == '`active`') ? 'a.' : '').pSQL($key).' = '.(int)($value).' ';
							elseif ($type == 'decimal')
								$sqlFilter .= (($key == $this->identifier OR $key == '`'.$this->identifier.'`') ? 'a.' : '').pSQL($key).' = '.(float)($value).' ';
							elseif ($type == 'select')
								$sqlFilter .= (($key == $this->identifier OR $key == '`'.$this->identifier.'`') ? 'a.' : '').pSQL($key).' = \''.pSQL($value).'\' ';
							else
								$sqlFilter .= (($key == $this->identifier OR $key == '`'.$this->identifier.'`') ? 'a.' : '').pSQL($key).' LIKE \'%'.pSQL($value).'%\' ';
						}
					}
				}
			}
		}
		elseif (Tools::isSubmit('submitFields') AND $this->requiredDatabase AND $this->tabAccess['add'] === '1' AND $this->tabAccess['delete'] === '1')
		{
			if (!is_array($fields = Tools::getValue('fieldsBox')))
				$fields = array();

			$object = new $this->className();
			if (!$object->addFieldsRequiredDatabase($fields))
				$this->_errors[] = Tools::displayError('Error in updating required fields');
			else
				Tools::redirectAdmin($currentIndex.'&conf=4&token='.$token);
		}
	}
	
	
}

