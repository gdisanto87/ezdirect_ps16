<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14846 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AggiornaPrezzi extends AdminTab
{
	
	
	
	public function display()
	{
		global $cookie, $currentIndex;
		
		
		
			if(Tools::getIsset('conferma-prezzi'))
			{
				foreach($_POST['id_product'] as $id_product=>$id_product_g)
				{
					if(isset($_POST['aggiorna'][$id_product]))
					{
						$id_da_inserire = $_POST['id_product'][$id_product];
						
						Db::getInstance()->executeS("DELETE FROM specific_price WHERE id_product = ".$id_da_inserire." AND specific_price_name = 'sc_qta_1'");
						Db::getInstance()->executeS("DELETE FROM specific_price WHERE id_product = ".$id_da_inserire." AND specific_price_name = 'sc_qta_2'");
						Db::getInstance()->executeS("DELETE FROM specific_price WHERE id_product = ".$id_da_inserire." AND specific_price_name = 'sc_qta_3'");
						Db::getInstance()->executeS("DELETE FROM specific_price WHERE id_product = ".$id_da_inserire." AND specific_price_name = 'sc_riv_1'");
						Db::getInstance()->executeS("DELETE FROM specific_price WHERE id_product = ".$id_da_inserire." AND specific_price_name = 'sc_riv_3'");
						
						$prezzoweb = floatval(str_replace(',', '.', $_POST['price'][$id_da_inserire]));
						$listino = floatval(str_replace(',', '.', $_POST['listino'][$id_da_inserire]));
						$sc_acq_1 = floatval(str_replace(',', '.', $_POST['sconto_acquisto_1'][$id_da_inserire]));
						$sc_acq_2 = floatval(str_replace(',', '.', $_POST['sconto_acquisto_2'][$id_da_inserire]));
						$sc_acq_3 = floatval(str_replace(',', '.', $_POST['sconto_acquisto_3'][$id_da_inserire]));
						$netto = floatval(str_replace(',', '.', $_POST['wholesale_price'][$id_da_inserire]));
						$sc_clienti = floatval(str_replace(',', '.', $_POST['scontolistinovendita'][$id_da_inserire]));
						$sc_riv_1 = floatval(str_replace(',', '.', $_POST['sconto_rivenditore_1'][$id_da_inserire]));
						$sc_riv_3 = floatval(str_replace(',', '.', $_POST['sconto_rivenditore_3'][$id_da_inserire]));
						
						$sc_quantita_1 = floatval(str_replace(',', '.', $_POST['sconto_quantita_1'][$id_da_inserire]));
						$sc_quantita_2 = floatval(str_replace(',', '.', $_POST['sconto_quantita_2'][$id_da_inserire]));
						$sc_quantita_3 = floatval(str_replace(',', '.', $_POST['sconto_quantita_3'][$id_da_inserire]));
						
	
						$product_insertion = ("UPDATE product 
						SET 
						id_tax_rules_group = 1,
						price = '".$prezzoweb."',
						listino = '".$listino."',
						wholesale_price = '".$netto."',
						sconto_acquisto_1 = '".$sc_acq_1."',
						sconto_acquisto_2 = '".$sc_acq_2."',
						sconto_acquisto_3 = '".$sc_acq_3."',
						scontolistinovendita = '".$sc_clienti."',
						date_upd = '".date('Y-m-d H:i:s')."'
						WHERE id_product = '".addslashes($id_da_inserire)."'

						");
						
						
						Db::getInstance()->executeS($product_insertion);
						
						if($sc_quantita_1 > 0) {
						
							Db::getInstance()->executeS("INSERT INTO specific_price (
							id_specific_price, 
							id_product, 
							id_shop,
							id_currency,
							id_country,
							id_group,
							price,
							from_quantity,
							reduction,
							reduction_type,
							specific_price_name)

							VALUES (
							'NULL',
							'".addslashes($id_da_inserire)."',
							0,
							0,
							0,
							1,
							'".$prezzoweb."',
							'".$_POST['quantita_1'][$id_da_inserire]."',
							'".($sc_quantita_1/100)."',
							'percentage',
							'sc_qta_1')

							");
						}
						if($sc_quantita_2 > 0) {
						
							Db::getInstance()->executeS("INSERT INTO specific_price (
							id_specific_price, 
							id_product, 
							id_shop,
							id_currency,
							id_country,
							id_group,
							price,
							from_quantity,
							reduction,
							reduction_type,
							specific_price_name)

							VALUES (
							'NULL',
							'".addslashes($id_da_inserire)."',
							0,
							0,
							0,
							1,
							'".$prezzoweb."',
							'".$_POST['quantita_2'][$id_da_inserire]."',
							'".($sc_quantita_2/100)."',
							'percentage',
							'sc_qta_2')

							");
						}
						
						if($sc_quantita_3 > 0) {
						
							Db::getInstance()->executeS("INSERT INTO specific_price (
							id_specific_price, 
							id_product, 
							id_shop,
							id_currency,
							id_country,
							id_group,
							price,
							from_quantity,
							reduction,
							reduction_type,
							specific_price_name)

							VALUES (
							'NULL',
							'".addslashes($id_da_inserire)."',
							0,
							0,
							0,
							1,
							'".$prezzoweb."',
							'".$_POST['quantita_3'][$id_da_inserire]."',
							'".($sc_quantita_3/100)."',
							'percentage',
							'sc_qta_3')

							");
						}
						
						if($sc_riv_1 > 0) {
						
							Db::getInstance()->executeS("INSERT INTO specific_price (
							id_specific_price, 
							id_product, 
							id_shop,
							id_currency,
							id_country,
							id_group,
							price,
							from_quantity,
							reduction,
							reduction_type,
							specific_price_name)

							VALUES (
							'NULL',
							'".addslashes($id_da_inserire)."',
							0,
							0,
							0,
							3,
							'".$listino."',
							'0',
							'".($sc_riv_1/100)."',
							'percentage',
							'sc_riv_1')

							");
						}

						if($sc_riv_3 > 0) {
						
							Db::getInstance()->executeS("INSERT INTO specific_price (
							id_specific_price, 
							id_product, 
							id_shop,
							id_currency,
							id_country,
							id_group,
							price,
							from_quantity,
							reduction,
							reduction_type,
							specific_price_name)

							VALUES (
							'NULL',
							'".addslashes($id_da_inserire)."',
							0,
							0,
							0,
							12,
							'".$listino."',
							'0',
							'".($sc_riv_3/100)."',
							'percentage',
							'sc_riv_3')

							");
						}
					}
				}
				
			}
			
			
			echo '<fieldset id="cambioprezzi">
				<legend>Aggiornamento prezzi</legend>
			';
			
			
			if(Tools::getIsset('seleziona_tipo')) 
			{
				if(Tools::getIsset('id_category')) {
				
				
					$macrocategoria = Tools::getValue('id_category');
					$macrocategorie = "";
	
					$nomemacrocat = Db::getInstance()->getValue("SELECT name FROM category_lang WHERE id_lang = 5 AND id_category = ".$macrocategoria."");
						
					$children = Db::getInstance()->executeS("SELECT id_category FROM
						(SELECT t4.id_category
						FROM category AS t1
						LEFT JOIN category AS t2 ON t1.id_category = t2.id_parent
						LEFT JOIN category AS t3 ON t2.id_category = t3.id_parent
						LEFT JOIN category AS t4 ON t3.id_category = t3.id_parent
						WHERE t2.id_parent =$macrocategoria

						UNION 

						SELECT t3.id_category
						FROM category AS t1
						LEFT JOIN category AS t2 ON t1.id_category = t2.id_parent
						LEFT JOIN category AS t3 ON t2.id_category = t3.id_parent
						WHERE t2.id_parent =$macrocategoria

						UNION 

						SELECT t2.id_category
						FROM category AS t1
						LEFT JOIN category AS t2 ON t1.id_category = t2.id_parent
						WHERE t2.id_parent =$macrocategoria

						UNION 

						SELECT id_category FROM category WHERE id_category = $macrocategoria) ccc WHERE id_category IS NOT NULL ORDER BY id_category ASC

					");

					foreach ($children as $child) {
	
						$macrocategorie.= "cp.id_category = ".$child['id_category']." OR ";
					}
						
				
				
					
				}
			
				echo '<form action="index.php?tab=AggiornaPrezzi&token='.$this->token.'&step=cambioprezzi&seleziona_tipo=y&id_category='.Tools::getValue('id_category').'&id_manufacturer='.Tools::getValue('id_manufacturer').'" id="cambioprezzi" method="post">';
				$prodotti = Db::getInstance()->executeS('SELECT p.*, pl.name FROM product p JOIN product_lang pl 
				ON pl.id_product = p.id_product WHERE ('.(Tools::getValue('id_category') == '' ? 'id_category_default > 0' : 'id_category_default = '.Tools::getValue('id_category').' OR p.id_product IN (SELECT id_product FROM category_product cp WHERE cp.id_category = '.Tools::getValue('id_category').' OR '.$macrocategorie.' cp.id_category = 999999999999999)').') AND '.(Tools::getValue('id_manufacturer') == '' ? 'id_manufacturer > 0' : 'id_manufacturer = '.Tools::getValue('id_manufacturer').'').' GROUP BY p.id_product');
				
				$tax_rules_groups = TaxRulesGroup::getTaxRulesGroups(true);
						$taxesRatesByGroup = TaxRulesGroup::getAssociatedTaxRatesByIdCountry(Country::getDefaultCountryId());
						$ecotaxTaxRate = Tax::getProductEcotaxRate();
						echo '<script type="text/javascript">';
						echo 'noTax = '.(Tax::excludeTaxeOption() ? 'true' : 'false'), ";\n";
						echo 'taxesArray = new Array ();'."\n";
						echo 'taxesArray[0] = 0', ";\n";

						foreach ($tax_rules_groups AS $tax_rules_group)
						{
							$tax_rate = (array_key_exists($tax_rules_group['id_tax_rules_group'], $taxesRatesByGroup) ?  $taxesRatesByGroup[$tax_rules_group['id_tax_rules_group']] : 0);
							echo 'taxesArray['.$tax_rules_group['id_tax_rules_group'].']='.$tax_rate."\n";
						}
						echo '
							ecotaxTaxRate = '.($ecotaxTaxRate / 100).';
						</script>';
				echo '
				<select style="display:none" onChange="unitPriceWithTax(\'unit\');" name="id_tax_rules_group" id="id_tax_rules_group" '.(Tax::excludeTaxeOption() ? 'disabled="disabled"' : '' ).'>
							';

							foreach ($tax_rules_groups AS $tax_rules_group)
								echo '<option value="'.$tax_rules_group['id_tax_rules_group'].'" '.(($this->getFieldValue($obj, 'id_tax_rules_group') == $tax_rules_group['id_tax_rules_group']) ? ' selected="selected"' : '').'>'.Tools::htmlentitiesUTF8($tax_rules_group['name']).'</option>';
					//echo ' <option value="0">'.$this->l('No Tax').'</option>';
					echo '</select>
					<link type="text/css" rel="stylesheet" href="fixedHeaderTable.css" />	
				<style type="text/css">
				
				#strumento_prezzi {
					border-collapse:collapse;
				}
				#cambioprezzi input {
					width:50px;
					text-align:right;
				}
				
				#strumento_prezzi tr td {
					overflow:scroll;
				}
				
				.thclass div {
					width:85px !important;
				}
				
				section.positioned {
				  position: absolute;
				  top:100px;
				  left:100px;
				  width:3000px;
				  box-shadow: 0 0 15px #333;
				}
				.container {
				  overflow-y: auto;
				  height: 400px;
				}
				#columnSelector.vertical label {
					 display: block;
					 /* inline-block; horizontal */
				 }

				.columnSelectorWrapper {
				  position: relative;
				  margin: 10px 0;
				}
				.columnSelector, .hidden {
				  display: none;
				}
				.columnSelectorButton {
				  background: #99bfe6;
				  border: #888 1px solid;
				  color: #111;
				  border-radius: 5px;
				  padding: 5px;
				}
				/* this wont work in IE8 & older */
				#colSelect1:checked + label {
					background: #5797d7;
					border-color: #555;
				}
				#colSelect1:checked ~ #columnSelector {
				  display: block;
				}
				.columnSelector {
				  position: absolute;
				  top: 30px;
				  padding: 10px;
				  background: #fff;
				  border: #99bfe6 1px solid;
				  border-radius: 5px;
				}
				.columnSelector label {
				  display: block;
				}
				.columnSelector label:nth-child(1) {
				  border-bottom: #99bfe6 solid 1px;
				  margin-bottom: 5px;
				}
				.columnSelector .disabled {
				  color: #ddd;
				}
				
				table {
					table-layout: fixed;
					
				}

				</style>
			
				<script type="text/javascript" src="jquery.fixedheadertable.min.js"></script>
				<script type="text/javascript" src="tablesorter/jquery.tablesorter.js"></script> 
				<script type="text/javascript" src="tablesorter/jquery.tablesorter.widgets.js"></script> 
				<script src="jquery.fixedTblHdrLftCol.js"></script>
				';  ?> <script type='text/javascript'>
			$(function() {
		
			

	$("table").tablesorter({
		theme: 'blue',
		widgets: ['zebra', 'columnSelector', 'cssStickyHeaders', 'resizable'],
		widgetOptions : {
			 resizable_addLastColumn : true,
			 resizable: true,
			  resizable_widths : [ '0px', '0px', '0px', '0px', '0px' ],
			// target the column selector markup
			columnSelector_container : $('#columnSelector'),
			// column status, true = display, false = hide
			// disable = do not display on list
			columnSelector_columns : {
				0: 'disable' /*,  disabled no allowed to unselect it */
				/* 1: false // start hidden */
			},
			// remember selected columns
			columnSelector_saveColumns: true,
	
			// container layout
			columnSelector_layout : '<label><input type="checkbox">{name}</label>',
			// data attribute containing column name to use in the selector container
			columnSelector_name  : 'data-selector-name',
	
			/* Responsive Media Query settings */
			// enable/disable mediaquery breakpoints
			columnSelector_mediaquery: true,
			// toggle checkbox name
			columnSelector_mediaqueryName: 'Auto: ',
			// breakpoints checkbox initial setting
			columnSelector_mediaqueryState: true,
			// responsive table hides columns with priority 1-6 at these breakpoints
			// see http://view.jquerymobile.com/1.3.2/dist/demos/widgets/table-column-toggle/#Applyingapresetbreakpoint
			// *** set to false to disable ***
			columnSelector_breakpoints : [ '20em', '30em', '40em', '50em', '60em', '70em' ],
			// data attribute containing column priority
			// duplicates how jQuery mobile uses priorities:
			// http://view.jquerymobile.com/1.3.2/dist/demos/widgets/table-column-toggle/
			columnSelector_priority : 'data-priority'

		}
	});

});

/* Column Selector/Responsive table widget (beta) for TableSorter 12/17/2013 (v2.14.6)
 * Requires tablesorter v2.8+ and jQuery 1.7+
 * by Justin Hallett & Rob Garrison
 */
/*jshint browser:true, jquery:true, unused:false */
/*global jQuery: false */
;(function($){
"use strict";

var ts = $.tablesorter,
namespace = '.tscolsel',
tsColSel = ts.columnSelector = {

	queryAll   : '@media only all { [columns] { display: none; } }',
	queryBreak : '@media screen and (min-width: [size]) { [columns] { display: table-cell; } }',

	init: function(table, c, wo) {
		var colSel;

		// unique table class name
		c.tableId = 'tablesorter' + new Date().getTime();
		c.$table.addClass( c.tableId );

		// build column selector/state array
		colSel = c.selector = { $container : $(wo.columnSelector_container) };
		tsColSel.setupSelector(table, c, wo);

		if (wo.columnSelector_mediaquery) {
			tsColSel.setupBreakpoints(c, wo);
		}

		if (colSel.$container.length) {
			colSel.$style = $('<style></style>').prop('disabled', true).appendTo('head');
			tsColSel.updateCols(c, wo);
		}

	},

	setupSelector: function(table, c, wo) {
		var name,
			colSel = c.selector,
			$container = colSel.$container,
			// get stored column states
			saved = wo.columnSelector_saveColumns && ts.storage ? ts.storage( table, 'tablesorter-columnSelector' ) : [];

		// initial states
		colSel.states = [];
		colSel.$column = [];
		colSel.$wrapper = [];
		colSel.$checkbox = [];
		// populate the selector container
		c.$table.children('thead').find('tr:first th', table).each(function() {
			var $this = $(this),
				// if no data-priority is assigned, default to 1, but don't remove it from the selector list
				priority = $this.attr(wo.columnSelector_priority) || 1,
				colId = $this.attr('data-column');

			// if this column not hidable at all
			// include getData check (includes "columnSelector-false" class, data attribute, etc)
			if ( isNaN(priority) && priority.length > 0 || ts.getData(this, c.headers[colId], 'columnSelector') == 'false' ||
				( wo.columnSelector_columns[colId] && wo.columnSelector_columns[colId] === 'disable') ) {
				return true; // goto next
			}

			// set default state
			colSel.states[colId] = saved && typeof(saved[colId]) !== 'undefined' ? saved[colId] : typeof(wo.columnSelector_columns[colId]) !== 'undefined' ? wo.columnSelector_columns[colId] : true;
			colSel.$column[colId] = $(this);

			// set default col title
			name = $this.attr(wo.columnSelector_name) || $this.text();

			if ($container.length) {
				colSel.$wrapper[colId] = $(wo.columnSelector_layout.replace(/\{name\}/g, name)).appendTo($container);
				colSel.$checkbox[colId] = colSel.$wrapper[colId]
					.find('input')
					.attr('data-column', colId)
					.prop('checked', colSel.states[colId])
					.bind('change', function(){
						colSel.states[colId] = this.checked;
						tsColSel.updateCols(c, wo);
					}).change();
			}
		});

	},

	setupBreakpoints: function(c, wo){
		var colSel = c.selector;

		// add responsive breakpoints
		if (wo.columnSelector_mediaquery) {
			// used by window resize function
			colSel.lastIndex = -1;
			wo.columnSelector_breakpoints.sort();
			colSel.$breakpoints = $('<style></style>').prop('disabled', true).appendTo('head');
			tsColSel.updateBreakpoints(c, wo);
			c.$table.unbind('updateAll' + namespace).bind('updateAll' + namespace, function(){
				tsColSel.updateBreakpoints(c, wo);
				tsColSel.updateCols(c, wo);
			});
		}

		if (colSel.$container.length) {
			// Add media queries toggle
			if (wo.columnSelector_mediaquery && wo.columnSelector_mediaquery) {
				$( wo.columnSelector_layout.replace(/\{name\}/g, wo.columnSelector_mediaqueryName) )
					.prependTo(colSel.$container)
					.find('input')
					.prop('checked', wo.columnSelector_mediaqueryState)
					.bind('change', function(){
						wo.columnSelector_mediaqueryState = this.checked;
						$.each( colSel.$checkbox, function(i, $cb){
							if ($cb) {
								$cb[0].disabled = wo.columnSelector_mediaqueryState;
								colSel.$wrapper[i].toggleClass('disabled', wo.columnSelector_mediaqueryState);
							}
						});
						tsColSel.updateBreakpoints(c, wo);
						tsColSel.updateCols(c, wo);
					}).change();
			}
			// Add a bind on update to re-run col setup
			c.$table.unbind('update' + namespace).bind('update' + namespace, function() {
				tsColSel.updateCols(c, wo);
			});
		}
	},


	updateBreakpoints: function(c, wo){
		var priority, column, breaks,
			colSel = c.selector,
			prefix = '.' + c.tableId,
			mediaAll = [],
			breakpts = '';
		if (wo.columnSelector_mediaquery && !wo.columnSelector_mediaqueryState) {
			colSel.$breakpoints.prop('disabled', true);
			colSel.$style.prop('disabled', false);
			return;
		}

		// only 6 breakpoints (same as jQuery Mobile)
		for (priority = 0; priority < 6; priority++){
			/*jshint loopfunc:true */
			breaks = [];
			c.$headers.filter('[' + wo.columnSelector_priority + '=' + (priority + 1) + ']').each(function(){
				column = parseInt($(this).attr('data-column'), 10) + 1;
				breaks.push(prefix + ' tr th:nth-child(' + column + ')');
				breaks.push(prefix + ' tr td:nth-child(' + column + ')');
			});
			if (breaks.length) {
				mediaAll = mediaAll.concat( breaks );
				breakpts += tsColSel.queryBreak
					.replace(/\[size\]/g, wo.columnSelector_breakpoints[priority])
					.replace(/\[columns\]/g, breaks.join(','));
			}
		}
		if (colSel.$style) { colSel.$style.prop('disabled', true); }
		colSel.$breakpoints.prop('disabled', false)
			.html( tsColSel.queryAll.replace(/\[columns\]/g, mediaAll.join(',')) + breakpts );

	},

	updateCols: function(c, wo) {
		if (wo.columnSelector_mediaquery && wo.columnSelector_mediaqueryState) {
			return;
		}
		var column,
			styles = [],
			prefix = '.' + c.tableId;
		c.selector.$container.find('input[data-column]').each(function(){
			if (!this.checked) {
				column = parseInt( $(this).attr('data-column'), 10 ) + 1;
				styles.push(prefix + ' tr th:nth-child(' + column + ')');
				styles.push(prefix + ' tr td:nth-child(' + column + ')');
			}
		});
		if (wo.columnSelector_mediaquery){
			c.selector.$breakpoints.prop('disabled', true);
		}
		if (c.selector.$style) {
			c.selector.$style.prop('disabled', false).html( styles.join(',') + ' { display: none; }' );
		}
		if (wo.columnSelector_saveColumns && ts.storage) {
			ts.storage( c.$table[0], 'tablesorter-columnSelector', c.selector.states );
		}
	}

};

ts.addWidget({
	id: "columnSelector",
	priority: 10,
	options: {
		// target the column selector markup
		columnSelector_container : null,
		// column status, true = display, false = hide
		// disable = do not display on list
		columnSelector_columns : [],
		// remember selected columns
		columnSelector_saveColumns: true,

		// container layout
		columnSelector_layout : '<label><input type="checkbox">{name}</label>',
		// data attribute containing column name to use in the selector container
		columnSelector_name  : 'data-selector-name',

		/* Responsive Media Query settings */
		// enable/disable mediaquery breakpoints
		columnSelector_mediaquery: true,
		// toggle checkbox name
		columnSelector_mediaqueryName: 'Auto: ',
		// breakpoints checkbox initial setting
		columnSelector_mediaqueryState: true,
		// responsive table hides columns with priority 1-6 at these breakpoints
		// see http://view.jquerymobile.com/1.3.2/dist/demos/widgets/table-column-toggle/#Applyingapresetbreakpoint
		// *** set to false to disable ***
		columnSelector_breakpoints : [ '20em', '30em', '40em', '50em', '60em', '70em' ],
		// data attribute containing column priority
		// duplicates how jQuery mobile uses priorities:
		// http://view.jquerymobile.com/1.3.2/dist/demos/widgets/table-column-toggle/
		columnSelector_priority : 'data-priority'

	},
	init: function(table, thisWidget, c, wo) {
		tsColSel.init(table, c, wo);
	},
	remove: function(table, c){
		var csel = c.selector;
		csel.$container.empty();
		csel.$style.remove();
		csel.$breakpoints.remove();
		c.$table.unbind('updateAll' + namespace + ',update' + namespace);
	}

});

})(jQuery);

/*! tablesorter CSS Sticky Headers widget - updated 12/14/2013 (v2.14.4)
* Requires a modern browser, tablesorter v2.8+
*/
/*global jQuery: false, unused:false */
;(function($){

    $.tablesorter.addWidget({
        id: "cssStickyHeaders",
        priority: 10,
        options: {
            cssStickyHeaders_offset   : 0,
            cssStickyHeaders_attachTo : null
        },
        init : function(table, thisWidget, c, wo) {
            var offset, bottom,
                $attach = $(wo.cssStickyHeaders_attachTo),
                namespace = '.cssstickyheader',
                $thead = c.$table.children('thead'),
				$tcol = $('.headcol'),
                $win = $attach.length ? $attach : $(window),
                left = 0;
           $( ".table_container, table" ).scroll(function() {
					left = 0,
                        right = $tcol.width(),
                        finalX = $('.table_container').scrollLeft();
                    // IE can only transform header cells - fixes #447 thanks to @gakreol!
					
                    $tcol.css({
                        "transform": finalX === 0 ? "" : "translate(" + finalX + "px, 0px)",
                        "-ms-transform": finalX === 0 ? "" : "translate(" + finalX + "px, 0px)",
                        "-webkit-transform": finalX === 0 ? "" : "translate(" + finalX + "px, 0px)"
                    });
					
                    var tableOffset = c.$table.offset(),
                        top = $attach.length ? $attach.offset().top : $win.scrollTop(),
                        bottom = c.$table.height() - $thead.height() - (c.$table.find('tfoot').height() || 0),
                        deltaY = top - tableOffset.top - 1 - (wo.cssStickyHeaders_offset || 0), // subtract out top border
                        finalY = (deltaY > 0 && deltaY <= bottom ? deltaY : 0);
                    // IE can only transform header cells - fixes #447 thanks to @gakreol!
                    $thead.children().children().css({
                        "transform": finalY === 0 ? "" : "translate(0px," + finalY + "px)",
                        "-ms-transform": finalY === 0 ? "" : "translate(0px," + finalY + "px)",
                        "-webkit-transform": finalY === 0 ? "" : "translate(0px," + finalY + "px)",
                    });
					
					
                });
        },
        remove: function(table, c, wo){
            var namespace = '.cssstickyheader';
            $(window).unbind('scroll resize '.split(' ').join(namespace + ' '));
            c.$table
                .unbind('update updateAll '.split(' ').join(namespace + ' '))
                .children('thead').css({ "transform": "translate(0px, 0px)" });
        }
    });
	
	

})(jQuery);


				
				</script>
				
				<script type="text/javascript" src="../js/price.js"></script>
				
				
				<?php
				echo '
				<div class="table_container" style="position:absolute; background-color:#ffffff; left:0px; top:0px; width:100%; height:650px; overflow:auto;"><table class="fancyTable" id="strumento_prezzi" 
				style="position:relative;">
				<thead>
				<tr>
				<th class="headcol"><div style="width:100px; display:block">Cod. EZ</div></th>
				<th><div style="width:100px; display:block">Cod. SKU</div></th>
				<th><div style="width:250px; display:block">Desc</div></th>
				<th><div style="width:150px; display:block">Costruttore</div></th>
				<th><div style="width:20px; display:block">Agg.?</div></th>
				<th class="thclass" style="background-color:#ebebeb"><div>Listino</div></th>
				<th class="thclass" style="background-color:#ffff00"><div>Sc. acq 1</div></th>
				<th class="thclass" style="background-color:#ffff00"><div>Sc. acq 2</div></th>
				<th class="thclass" style="background-color:#ffff00"><div>Sc. acq 3</div></th>
				<th class="thclass" style="background-color:#ffff00"><div>Netto acq.</div></th>
				<th class="thclass" style="background-color:#ebebeb"><div>Sc. cli</div></th>
				<th class="thclass" style="background-color:#ebebeb"><div>Prz. web</div></th>
				<th class="thclass" style="background-color:#ebebeb"><div>Margine %</div></th>
				<th class="thclass" style="background-color:#99cc99"><div>Sc. qta 1</div></th>
				<th class="thclass" style="background-color:#99cc99"><div>Qta 1</div></th>
				<th class="thclass" style="background-color:#99cc99"><div>Prz qta 1</div></th>
				<th class="thclass" style="background-color:#99cc99"><div>Marg qt 1</div></th>
				<th class="thclass" style="background-color:#99cc99"><div>Sc. qta 2</div></th>
				<th class="thclass" style="background-color:#99cc99"><div>Qta 2</div></th>
				<th class="thclass" style="background-color:#99cc99"><div>Prz qta 2</div></th>
				<th class="thclass" style="background-color:#99cc99"><div>Marg qt 2</div></th>
				<th class="thclass" style="background-color:#99cc99"><div>Sc. qta 3</div></th>
				<th class="thclass" style="background-color:#99cc99"><div>Qta 3</div></th>
				<th class="thclass" style="background-color:#99cc99"><div>Prz qta 3</div></th>
				<th class="thclass" style="background-color:#99cc99"><div>Marg qt 3</div></th>
				<th class="thclass" style="background-color:#ff99ff"><div>Sc. riv 1</div></th>
				<th class="thclass" style="background-color:#ff99ff"><div>Prz riv 1</div></th>
				<th class="thclass" style="background-color:#ff99ff"><div>Mrg riv 1</div></th>
				<th class="thclass" style="background-color:#ff99ff"><div>Sc. distr.</div></th>
				<th class="thclass" style="background-color:#ff99ff"><div>Prz distr.</div></th>
				<th class="thclass" style="background-color:#ff99ff"><div>Mrg distr.</div></th>
				</tr></thead>';
				
				foreach($prodotti as $product)
				{
					$sc_cli = $product['scontolistinovendita'];
					if($sc_cli <= 0)
						$sc_cli = 100-(($product['price']*100)/$product['listino']);
					
					$sc_qta_1 = Db::getInstance()->getValue("SELECT reduction FROM specific_price WHERE id_product = ".$product['id_product']." AND specific_price_name = 'sc_qta_1'");
					if($sc_qta_1 > 0) {		
						$qta_1 = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE id_product = ".$product['id_product']." AND specific_price_name = 'sc_qta_1'");
						$prz_sc_qta_1 = $product['price']-($product['price']*$sc_qta_1);
						$marg_qta_1 = ($product['price']-($product['price']*$sc_qta_1)-$product['wholesale_price'])/($product['price']-($product['price']*$sc_qta_1))*100;
					}
					else {
						$qta_1 = 0;
						$prz_sc_qta_1 = 0;
						$marg_qta_1 = 0;
					}
					
					$sc_qta_2 = Db::getInstance()->getValue("SELECT reduction FROM specific_price WHERE id_product = ".$product['id_product']." AND specific_price_name = 'sc_qta_2'");
					if($sc_qta_2 > 0) {		
						$qta_2 = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE id_product = ".$product['id_product']." AND specific_price_name = 'sc_qta_2'");
						$prz_sc_qta_2 = $product['price']-($product['price']*$sc_qta_2);
						$marg_qta_2 = ($product['price']-($product['price']*$sc_qta_2)-$product['wholesale_price'])/($product['price']-($product['price']*$sc_qta_2))*100;
					}
					else {
						$qta_2 = 0;
						$prz_sc_qta_2 = 0;
						$marg_qta_2 = 0;
					}
					
					$sc_qta_3 = Db::getInstance()->getValue("SELECT reduction FROM specific_price WHERE id_product = ".$product['id_product']." AND specific_price_name = 'sc_qta_3'");
					if($sc_qta_3 > 0) {		
						$qta_3 = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE id_product = ".$product['id_product']." AND specific_price_name = 'sc_qta_3'");
						$prz_sc_qta_3 = $product['price']-($product['price']*$sc_qta_3);
						$marg_qta_3 = ($product['price']-($product['price']*$sc_qta_3)-$product['wholesale_price'])/($product['price']-($product['price']*$sc_qta_3))*100;
					}
					else {
						$qta_3 = 0;
						$prz_sc_qta_3 = 0;
						$marg_qta_3 = 0;
					}
					
					$sc_riv_1 = Db::getInstance()->getValue("SELECT reduction FROM specific_price WHERE id_product = ".$product['id_product']." AND specific_price_name = 'sc_riv_1'");
					if($sc_riv_1 > 0) {		
						$prz_sc_riv_1 = $product['listino']-($product['listino']*$sc_riv_1);
						$marg_riv_1 = ($product['listino']-($product['listino']*$sc_riv_1)-$product['wholesale_price'])/($product['price']-($product['price']*$sc_riv_1))*100;
					}
					else {
						$prz_sc_riv_1 = 0;
						$marg_riv_1 = 0;
					}
					
					$sc_riv_2 = Db::getInstance()->getValue("SELECT reduction FROM specific_price WHERE id_product = ".$product['id_product']." AND specific_price_name = 'sc_riv_2'");
					if($sc_riv_2 > 0) {	
						$prz_sc_riv_2 = $product['listino']-($product['listino']*$sc_riv_2);
						$marg_riv_2 = ($product['listino']-($product['listino']*$sc_riv_2)-$product['wholesale_price'])/($product['price']-($product['price']*$sc_riv_2))*100;
					}
					else {
						$prz_sc_riv_2 = 0;
						$marg_riv_2 = 0;
					}
					
					$sc_riv_3 = Db::getInstance()->getValue("SELECT reduction FROM specific_price WHERE id_product = ".$product['id_product']." AND specific_price_name = 'sc_riv_3'");
					if($sc_riv_3 > 0) {	
						$prz_sc_riv_3 = $product['listino']-($product['listino']*$sc_riv_3);
						$marg_riv_3 = ($product['listino']-($product['listino']*$sc_riv_3)-$product['wholesale_price'])/($product['price']-($product['price']*$sc_riv_3))*100;
					}
					else {
						$prz_sc_riv_3 = 0;
						$marg_riv_3 = 0;
					}
					
					$marg = (($product['price']-$product['wholesale_price'])*100)/$product['price'];
				
					echo '
				<tr>
				<td class="headcol" style="width:100px">'.$product['reference'].'
				<script type="text/javascript">
					function calcMarginalita_'.$product['id_product'].'()
					{
						var tax = getTax();
						var priceListino = parseFloat(document.getElementById(\'wholesale_price['.$product['id_product'].']\').value.replace(/,/g, \'.\'));
						var priceTE = parseFloat(document.getElementById(\'priceTE['.$product['id_product'].']\').value.replace(/,/g, \'.\'));
						var newPrice = (((priceTE - priceListino)*100) / priceTE);
						
						document.getElementById(\'marginalita['.$product['id_product'].']\').value = (isNaN(newPrice) == true || newPrice < 0) ? \'\' :
							ps_round(newPrice, 2);

						calcReduction();
						document.getElementById(\'marginalita['.$product['id_product'].']\').value =
						parseFloat(document.getElementById(\'marginalita['.$product['id_product'].']\').value) + getEcotaxTaxIncluded();

					}

					function calcMarginalitaSconto_'.$product['id_product'].'(sc_riv, margin_riv, list_type)
					{
						var tax = getTax();
						var priceListino = parseFloat(document.getElementById(\'wholesale_price['.$product['id_product'].']\').value.replace(/,/g, \'.\'));
						var sconto = parseFloat(document.getElementById(sc_riv).value.replace(/,/g, \'.\'));
						var priceTE = parseFloat(document.getElementById(list_type).value.replace(/,/g, \'.\'));
						priceTE = priceTE - ((priceTE * sconto) / 100);
						var newPrice = (((priceTE - priceListino)*100) / priceTE);
						
						document.getElementById(margin_riv).value = (isNaN(newPrice) == true || newPrice < 0) ? \'\' :
							ps_round(newPrice, 2);

						calcReduction();
						document.getElementById(margin_riv).value = parseFloat(document.getElementById(margin_riv).value) + getEcotaxTaxIncluded();

					}
					
					function calcScontoAcquisto1_'.$product['id_product'].'()
					{
						
						var acq1 = parseFloat(document.getElementById(\'sconto_acquisto_1['.$product['id_product'].']\').value.replace(/,/g, \'.\'));
						var listino = parseFloat(document.getElementById(\'listino['.$product['id_product'].']\').value.replace(/,/g, \'.\'));
						var newPrice = (listino * (100 - acq1) / 100);
						
						document.getElementById(\'wholesale_price['.$product['id_product'].']\').value = (isNaN(newPrice) == true || newPrice < 0) ? \'\' :
							ps_round(newPrice, 2);

						document.getElementById(\'wholesale_price['.$product['id_product'].']\').innerHTML = parseFloat(document.getElementById(\'wholesale_price['.$product['id_product'].']\').value);
						
					}

					function calcScontoAcquisto2_'.$product['id_product'].'()
					{

						var acq1 = parseFloat(document.getElementById(\'sconto_acquisto_1['.$product['id_product'].']\').value.replace(/,/g, \'.\'));
						var acq2 = parseFloat(document.getElementById(\'sconto_acquisto_2['.$product['id_product'].']\').value.replace(/,/g, \'.\'));
						var listino = parseFloat(document.getElementById(\'listino['.$product['id_product'].']\').value.replace(/,/g, \'.\'));
						var newPrice = (listino * (100 - acq1) / 100) * ((100 - acq2)/100);
						
						document.getElementById(\'wholesale_price['.$product['id_product'].']\').value = (isNaN(newPrice) == true || newPrice < 0) ? \'\' :
							ps_round(newPrice, 2);

						document.getElementById(\'wholesale_price['.$product['id_product'].']\').innerHTML = parseFloat(document.getElementById(\'wholesale_price['.$product['id_product'].']\').value);
					}

					function calcScontoAcquisto3_'.$product['id_product'].'()
					{

						var acq1 = parseFloat(document.getElementById(\'sconto_acquisto_1['.$product['id_product'].']\').value.replace(/,/g, \'.\'));
						var acq2 = parseFloat(document.getElementById(\'sconto_acquisto_2['.$product['id_product'].']\').value.replace(/,/g, \'.\'));
						var acq3 = parseFloat(document.getElementById(\'sconto_acquisto_3['.$product['id_product'].']\').value.replace(/,/g, \'.\'));
						var listino = parseFloat(document.getElementById(\'listino['.$product['id_product'].']\').value.replace(/,/g, \'.\'));
						var newPrice = (listino * (100 - acq1) / 100) * ((100 - acq2)/100) * ((100 - acq3)/100);
						
						document.getElementById(\'wholesale_price['.$product['id_product'].']\').value = (isNaN(newPrice) == true || newPrice < 0) ? \'\' :
							ps_round(newPrice, 2);

						document.getElementById(\'wholesale_price['.$product['id_product'].']\').innerHTML = parseFloat(document.getElementById(\'wholesale_price['.$product['id_product'].']\').value);
					}
				
				</script>
				</td>
				<td style="width:100px">'.$product['supplier_reference'].'</td>
				<td style="width:250px"><a href="?tab=AdminCatalog&id_product='.$product['id_product'].'&updateproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.$product['name'].'</a></td>
				<td style="width:150px">'.Db::getInstance()->getValue('SELECT name FROM manufacturer WHERE id_manufacturer = '.$product['id_manufacturer']).'</td>
				
				<td style="width:20px"><input type="checkbox" name="aggiorna['.$product['id_product'].']" '.(isset($_POST['aggiorna'][$product['id_product']]) ? 'checked="checked"' : '').' value="1" />
				<input type="hidden" name="id_product['.$product['id_product'].']" value="'.$product['id_product'].'" />
				</td>
				
				<td style="background-color:#ebebeb"><div style="display:none">'.$product['listino'].'</div><input type="text" id="listino['.$product['id_product'].']" name="listino['.$product['id_product'].']" onchange="this.value = this.value.replace(/,/g, \'.\');" value="'.number_format($product['listino'],2,",","").'" onkeyup="
								calcPrezzoSconto(\'scontolistinovendita['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\', \'listino['.$product['id_product'].']\');
								calcScontoAcquisto1_'.$product['id_product'].'(); calcScontoAcquisto2_'.$product['id_product'].'(); calcScontoAcquisto3_'.$product['id_product'].'(); 
								calcMarginalita_'.$product['id_product'].'(); calcSconto(\'listino['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\', \'scontolistinovendita['.$product['id_product'].']\');
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_rivenditore_1['.$product['id_product'].']\', \'margin_riv_1['.$product['id_product'].']\', \'listino['.$product['id_product'].']\'); calcPrezzoSconto(\'sconto_rivenditore_1['.$product['id_product'].']\', \'prezzo_riv_1['.$product['id_product'].']\', \'listino['.$product['id_product'].']\');
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_rivenditore_2['.$product['id_product'].']\', \'margin_riv_2['.$product['id_product'].']\', \'listino['.$product['id_product'].']\'); calcPrezzoSconto(\'sconto_rivenditore_2['.$product['id_product'].']\', \'prezzo_riv_2['.$product['id_product'].']\', \'listino['.$product['id_product'].']\');
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_rivenditore_3['.$product['id_product'].']\', \'margin_riv_3['.$product['id_product'].']\', \'listino['.$product['id_product'].']\'); calcPrezzoSconto(\'sconto_rivenditore_3['.$product['id_product'].']\', \'prezzo_riv_3['.$product['id_product'].']\', \'listino['.$product['id_product'].']\');" /></td>
								
				<td style="background-color:#ffff00"><div style="display:none">'.$product['sconto_acquisto_1'].'</div><input type="text" id="sconto_acquisto_1['.$product['id_product'].']" name="sconto_acquisto_1['.$product['id_product'].']" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcScontoAcquisto1_'.$product['id_product'].'(); calcMarginalita_'.$product['id_product'].'(); 
							
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_rivenditore_1['.$product['id_product'].']\', \'margin_riv_1['.$product['id_product'].']\', \'listino['.$product['id_product'].']\'); 
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_rivenditore_2['.$product['id_product'].']\', \'margin_riv_2['.$product['id_product'].']\', \'listino['.$product['id_product'].']\');
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_rivenditore_3['.$product['id_product'].']\', \'margin_riv_3['.$product['id_product'].']\', \'listino['.$product['id_product'].']\');
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_1['.$product['id_product'].']\', \'margin_qta_1['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\'); 
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_2['.$product['id_product'].']\', \'margin_qta_2['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\'); 
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_3['.$product['id_product'].']\', \'margin_qta_3['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\')							
								" value="'.number_format($product['sconto_acquisto_1'],2,",","").'" />%</td>
				
				<td style="background-color:#ffff00"><div style="display:none">'.$product['sconto_acquisto_2'].'</div><input id="sconto_acquisto_2['.$product['id_product'].']" name="sconto_acquisto_2['.$product['id_product'].']"  onchange="this.value = this.value.replace(/,/g, \'.\');" type="text" value="'.number_format($product['sconto_acquisto_2'],2,",","").'" onkeyup="if (isArrowKey(event)) return; calcScontoAcquisto2_'.$product['id_product'].'(); calcMarginalita_'.$product['id_product'].'();
				calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_rivenditore_1['.$product['id_product'].']\', \'margin_riv_['.$product['id_product'].']1\', \'listino['.$product['id_product'].']\'); calcPrezzoSconto(\'sconto_rivenditore_1['.$product['id_product'].']\', \'prezzo_riv_1['.$product['id_product'].']\', \'listino['.$product['id_product'].']\');
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_rivenditore_2['.$product['id_product'].']\', \'margin_riv_2['.$product['id_product'].']\', \'listino['.$product['id_product'].']\'); 
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_rivenditore_3['.$product['id_product'].']\', \'margin_riv_3['.$product['id_product'].']\', \'listino['.$product['id_product'].']\'); 
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_1['.$product['id_product'].']\', \'margin_qta_1['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\'); 
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_2['.$product['id_product'].']\', \'margin_qta_2['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\'); 
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_3['.$product['id_product'].']\', \'margin_qta_3['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\')
								" />%</td>
				<td style="background-color:#ffff00"><div style="display:none">'.$product['sconto_acquisto_3'].'</div><input id="sconto_acquisto_3['.$product['id_product'].']" name="sconto_acquisto_3['.$product['id_product'].']" type="text" value="'.number_format($product['sconto_acquisto_3'],2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcScontoAcquisto3_'.$product['id_product'].'(); calcMarginalita_'.$product['id_product'].'();	calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_rivenditore_1['.$product['id_product'].']\', \'margin_riv_1['.$product['id_product'].']\', \'listino['.$product['id_product'].']\');
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_rivenditore_2['.$product['id_product'].']\', \'margin_riv_2['.$product['id_product'].']\', \'listino['.$product['id_product'].']\'); 
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_rivenditore_3['.$product['id_product'].']\', \'margin_riv_3['.$product['id_product'].']\', \'listino['.$product['id_product'].']\'); 
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_1['.$product['id_product'].']\', \'margin_qta_1['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\'); 
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_2['.$product['id_product'].']\', \'margin_qta_2['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\'); 
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_3['.$product['id_product'].']\', \'margin_qta_3['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\')

								" />%</td>
				<td style="background-color:#ffff00"><div style="display:none">'.$product['wholesale_price'].'</div><input id="wholesale_price['.$product['id_product'].']" name="wholesale_price['.$product['id_product'].']"  type="text" value="'.number_format($product['wholesale_price'],2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');
								calcMarginalita_'.$product['id_product'].'();
								calcSconto(\'listino['.$product['id_product'].']\', \'wholesale_price['.$product['id_product'].']\', \'sconto_acquisto_1['.$product['id_product'].']\');
								document.getElementById(\'sconto_acquisto_2['.$product['id_product'].']\').value = 0;
								document.getElementById(\'sconto_acquisto_3['.$product['id_product'].']\').value = 0;
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_rivenditore_1['.$product['id_product'].']\', \'margin_riv_1['.$product['id_product'].']\', \'listino['.$product['id_product'].']\');
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_rivenditore_2['.$product['id_product'].']\', \'margin_riv_2['.$product['id_product'].']\', \'listino['.$product['id_product'].']\'); 
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_rivenditore_3['.$product['id_product'].']\', \'margin_riv_3['.$product['id_product'].']\', \'listino['.$product['id_product'].']\'); 
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_1['.$product['id_product'].']\', \'margin_qta_1['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\'); 
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_2['.$product['id_product'].']\', \'margin_qta_2['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\'); 
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_3['.$product['id_product'].']\', \'margin_qta_3['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\')
								
								"  /></td>
				
				<td style="background-color:#ebebeb"><div style="display:none">'.$sc_cli.'</div><input id="scontolistinovendita['.$product['id_product'].']" name="scontolistinovendita['.$product['id_product'].']" type="text" value="'.number_format($sc_cli,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return;  calcPrezzoSconto(\'scontolistinovendita['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\', \'listino['.$product['id_product'].']\');
								calcMarginalita_'.$product['id_product'].'(); 
										calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_1['.$product['id_product'].']\', \'margin_qta_1['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\'); calcPrezzoSconto(\'sconto_quantita_1['.$product['id_product'].']\', \'prezzo_qta_1['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\');
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_2['.$product['id_product'].']\', \'margin_qta_2['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\'); calcPrezzoSconto(\'sconto_quantita_2['.$product['id_product'].']\', \'prezzo_qta_2['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\');
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_3['.$product['id_product'].']\', \'margin_qta_3['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\'); calcPrezzoSconto(\'sconto_quantita_3['.$product['id_product'].']\', \'prezzo_qta_3['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\');
								
								" />%</td>
				<td style="background-color:#ebebeb"><div style="display:none">'.$product['price'].'</div><input id="priceTE['.$product['id_product'].']" name="price['.$product['id_product'].']" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcMarginalita_'.$product['id_product'].'(); calcSconto(\'listino['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\', \'scontolistinovendita['.$product['id_product'].']\'); calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_1['.$product['id_product'].']\', \'margin_qta_1['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\'); calcPrezzoSconto(\'sconto_quantita_1['.$product['id_product'].']\', \'prezzo_qta_1['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\');
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_2['.$product['id_product'].']\', \'margin_qta_2['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\'); calcPrezzoSconto(\'sconto_quantita_2['.$product['id_product'].']\', \'prezzo_qta_2['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\');
								calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_3['.$product['id_product'].']\', \'margin_qta_3['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\'); calcPrezzoSconto(\'sconto_quantita_3['.$product['id_product'].']\', \'prezzo_qta_3['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\');
								
								" type="text" value="'.number_format($product['price'],2,",","").'" /></td>
								
				<td style="background-color:#ebebeb"><div style="display:none">'.$marg.'</div><input id="marginalita['.$product['id_product'].']" name="marginalita['.$product['id_product'].']" value="'.number_format($marg,2,",","").'"  type="text" />%</td>
				
				<td style="background-color:#99cc99"><div style="display:none">'.$sc_qta_1.'</div><input id="sconto_quantita_1['.$product['id_product'].']" name="sconto_quantita_1['.$product['id_product'].']" type="text" value="'.number_format($sc_qta_1*100,2,",","").'" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_1['.$product['id_product'].']\', \'margin_qta_1['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\'); calcPrezzoSconto(\'sconto_quantita_1['.$product['id_product'].']\', \'prezzo_qta_1['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\');" />%</td>
				
				<td style="background-color:#99cc99"><div style="display:none">'.$qta_1.'</div><input id="quantita_1['.$product['id_product'].']" name="quantita_1['.$product['id_product'].']" type="text" value="'.$qta_1.'" /></td>
				
				<td style="background-color:#99cc99"><div style="display:none">'.$prz_sc_qta_1.'</div><input id="prezzo_qta_1['.$product['id_product'].']" name="prezzo_qta_1['.$product['id_product'].']" type="text" value="'.number_format($prz_sc_qta_1,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'priceTE['.$product['id_product'].']\', \'prezzo_qta_1['.$product['id_product'].']\', \'sconto_quantita_1['.$product['id_product'].']\');" /></td>
				
				
				<td style="background-color:#99cc99"><div style="display:none">'.$marg_qta_1.'</div><input type="text" id="margin_qta_1['.$product['id_product'].']"  value="'.number_format($marg_qta_1,2,",","").'" name="margin_qta_1['.$product['id_product'].']" type="text" onchange="this.value = this.value.replace(/,/g, \'.\');" readonly="readonly" />%</td>
				
				<td style="background-color:#99cc99"><div style="display:none">'.$sc_qta_2.'</div><input id="sconto_quantita_2['.$product['id_product'].']" name="sconto_quantita_2['.$product['id_product'].']" type="text" value="'.number_format($sc_qta_2*100,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_2['.$product['id_product'].']\', \'margin_qta_2['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\'); calcPrezzoSconto(\'sconto_quantita_2['.$product['id_product'].']\', \'prezzo_qta_2['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\');" />%</td>
				
				<td style="background-color:#99cc99"><div style="display:none">'.$qta_2.'</div><input id="quantita_2['.$product['id_product'].']" name="quantita_2['.$product['id_product'].']" type="text" value="'.$qta_2.'" /></td>
				
				<td style="background-color:#99cc99"><div style="display:none">'.$prz_sc_qta_2.'</div><input type="text" id="prezzo_qta_2['.$product['id_product'].']" name="prezzo_qta_2['.$product['id_product'].']"  value="'.number_format($prz_sc_qta_2,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'priceTE['.$product['id_product'].']\', \'prezzo_qta_2['.$product['id_product'].']\', \'sconto_quantita_2['.$product['id_product'].']\');" /></td>
				
				<td style="background-color:#99cc99"><div style="display:none">'.$marg_qta_2.'</div><input type="text" id="margin_qta_2['.$product['id_product'].']" value="'.number_format($marg_qta_2,2,",","").'" name="margin_qta_2['.$product['id_product'].']" type="text" onchange="this.value = this.value.replace(/,/g, \'.\');" readonly="readonly" />%</td>
				
				<td style="background-color:#99cc99"><div style="display:none">'.$sc_qta_3.'</div><input id="sconto_quantita_3['.$product['id_product'].']" name="sconto_quantita_3['.$product['id_product'].']" type="text" value="'.number_format($sc_qta_3*100,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_quantita_3['.$product['id_product'].']\', \'margin_qta_3['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\'); calcPrezzoSconto(\'sconto_quantita_3['.$product['id_product'].']\', \'prezzo_qta_3['.$product['id_product'].']\', \'priceTE['.$product['id_product'].']\');" />%</td>
				
				<td style="background-color:#99cc99"><div style="display:none">'.$qta_3.'</div><input id="quantita_3['.$product['id_product'].']" name="quantita_3['.$product['id_product'].']" type="text" value="'.$qta_3.'" /></td>
				
				<td style="background-color:#99cc99"><div style="display:none">'.$prz_sc_qta_3.'</div><input id="prezzo_qta_3['.$product['id_product'].']" name="prezzo_qta_3['.$product['id_product'].']"  type="text" value="'.number_format($prz_sc_qta_3,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'priceTE['.$product['id_product'].']\', \'prezzo_qta_3['.$product['id_product'].']\', \'sconto_quantita_3['.$product['id_product'].']\');" /></td>
				
				<td style="background-color:#99cc99"><div style="display:none">'.$marg_qta_3.'</div><input id="margin_qta_3['.$product['id_product'].']" name="margin_qta_3['.$product['id_product'].']" readonly="readonly" type="text" value="'.number_format($marg_qta_3,2,",","").'" />%</td>
				
				
				<td style="background-color:#ff99ff"><div style="display:none">'.$sc_riv_1.'</div><input id="sconto_rivenditore_1['.$product['id_product'].']" name="sconto_rivenditore_1['.$product['id_product'].']" type="text" value="'.number_format($sc_riv_1*100,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_rivenditore_1['.$product['id_product'].']\', \'margin_riv_1['.$product['id_product'].']\', \'listino['.$product['id_product'].']\'); calcPrezzoSconto(\'sconto_rivenditore_1['.$product['id_product'].']\', \'prezzo_riv_1['.$product['id_product'].']\', \'listino['.$product['id_product'].']\');" />%</td>
				
				<td style="background-color:#ff99ff"><div style="display:none">'.$prz_sc_riv_1.'</div><input id="prezzo_riv_1['.$product['id_product'].']" name="prezzo_riv_1['.$product['id_product'].']"  type="text" value="'.number_format($prz_sc_riv_1,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'listino\', \'prezzo_riv_1['.$product['id_product'].']\', \'sconto_rivenditore_1['.$product['id_product'].']\');" /></td>
				
				<td style="background-color:#ff99ff"><div style="display:none">'.$marg_riv_1.'</div><input id="margin_riv_1['.$product['id_product'].']" name="margin_riv_1['.$product['id_product'].']" type="text" value="'.number_format($marg_riv_1,2,",","").'"  onchange="this.value = this.value.replace(/,/g, \'.\');" readonly="readonly" />%</td>
				
				
				<td style="background-color:#ff99ff"><div style="display:none">'.$sc_riv_3.'</div><input id="sconto_rivenditore_3['.$product['id_product'].']" name="sconto_rivenditore_3['.$product['id_product'].']" type="text" value="'.number_format($sc_riv_3*100,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto_'.$product['id_product'].'(\'sconto_rivenditore_3['.$product['id_product'].']\', \'margin_riv_3['.$product['id_product'].']\', \'listino['.$product['id_product'].']\'); calcPrezzoSconto(\'sconto_rivenditore_3['.$product['id_product'].']\', \'prezzo_riv_3['.$product['id_product'].']\', \'listino['.$product['id_product'].']\');" />%</td>
				
				<td style="background-color:#ff99ff"><div style="display:none">'.$prz_sc_riv_3.'</div><input id="prezzo_riv_3['.$product['id_product'].']" name="prezzo_riv_3['.$product['id_product'].']" type="text" value="'.number_format($prz_sc_riv_3,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');"  onkeyup="calcSconto(\'listino['.$product['id_product'].']\', \'prezzo_riv_3['.$product['id_product'].']\', \'sconto_rivenditore_3['.$product['id_product'].']\');" /></td>
				
				<td style="background-color:#ff99ff"><div style="display:none">'.$marg_riv_3.'</div><input type="text" id="margin_riv_3['.$product['id_product'].']" name="margin_riv_3['.$product['id_product'].']" value="'.number_format($marg_riv_3,2,",","").'"  onchange="this.value = this.value.replace(/,/g, \'.\');" readonly="readonly" />%</td>
				</tr>';
			
			
			
			
				}
				
				echo '</table></div><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><div class="columnSelectorWrapper vertical" style=" width:300px; z-index:9999999; top:0px">
					<input id="colSelect1" type="checkbox" class="hidden" />
					<label class="columnSelectorButton" for="colSelect1">Nascondi colonne</label>
					<div id="columnSelector" class="columnSelector"></div>
				</div>';
				
				echo '<input type="hidden" name="id_category" value="'.Tools::getValue('id_category').'" />';
				echo '<input type="hidden" name="id_manufacturer" value="'.Tools::getValue('id_manufacturer').'" />';
				echo '<input type="hidden" name="seleziona_tip" value="y" />';
				echo '<p style="text-align:center">';
				echo '<input type="submit" name="conferma-prezzi" value="Conferma modifiche" style="width:160px; height:23px; text-align:center; cursor:pointer" class="button" />';
				echo '<a class="button" style="text-decoration:none; cursor:pointer; color:#000; margin-left:10px;" href="'.$currentIndex.'&token='.($token ? $token : $this->token).''.'">Torna al menù di selezione</a>';
				echo '</p>';
				echo '</form>';
			
			}
			
			else {
				echo '<form method="post">';
				echo 'Seleziona macrocategoria:<br />
				<select id="id_category" name="id_category" style="width:600px">
				<option name="0" value="">--- Scegli una macrocategoria ---</option>
				';
				
				
				$resultscat = Db::getInstance()->ExecuteS("SELECT category.id_category, category_lang.name FROM category JOIN category_lang ON category.id_category = category_lang.id_category WHERE category_lang.id_lang = 5 AND category.id_parent = 1 ORDER BY name");
				
				foreach ($resultscat as $rowcat) {
				
					echo "<option name='$rowcat[id_category]' value='$rowcat[id_category]'"; 
					echo ">$rowcat[name]</option>";
				
				}
					
				echo "</select><br /><br />";
	
				echo 'Seleziona costruttore:<br />
				<select id="id_manufacturer" name="id_manufacturer" style="width:600px">
				<option name="0" value="">--- Scegli un costruttore ---</option>
				';
				
				
				$resultsman = Db::getInstance()->ExecuteS("SELECT id_manufacturer, name FROM manufacturer ORDER BY name");
				
				foreach ($resultsman as $rowman) {
				
					echo "<option name='$rowman[id_manufacturer]' value='$rowman[id_manufacturer]'"; 
					echo ">$rowman[name]</option>";
				
				}
					
				echo "</select><br /><br />";
			
				echo "<input type='submit' class='button' style='cursor:pointer' name='seleziona_tipo' value='Vedi listino prezzi' />";
				echo "</form>";
			
			
			
			
			
			}
			
			echo '</fieldset>';
			
			
			
		
		
	}
	

}
