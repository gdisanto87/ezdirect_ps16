<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

class ExportCatalog extends AdminPreferences
{


	public function display()
	{
		if(isset($_POST['vai-rapido'])) {
		 
		include("esportazione-catalogo/esportazione-rapida.php");
		 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";

		}
		else if(isset($_POST['vai-rapido-c'])) {
		 
		include("esportazione-catalogo/esportazione-rapida-costruttori.php");
		 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";

		}
		else	if(isset($_POST['vai']) && $_POST['vai'] == 'Esporta catalogo XLS') {
			 
			 if (isset($_POST['export']) && $_POST['export'] == 'cuffie') {
			
			include("esportazione-catalogo/esportazione-cuffie.php");
			 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";
			
			 }
			 
					 else if (isset($_POST['export']) && $_POST['export'] == 'tutto') {
			 
			include("esportazione-catalogo/tutto-il-catalogo.php");
			 
			 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";
			
			 }
			 
			 else if (isset($_POST['export']) && $_POST['export'] == 'cordless') {
			 
			include("esportazione-catalogo/esportazione-cordless.php");
			 
			 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";
			
			 }
			 
				 else if (isset($_POST['export']) && $_POST['export'] == 'audioconferenze') {
			 
			include("esportazione-catalogo/esportazione-audioconferenze.php");
			 
			 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";
			
			 }
			 
			  else if (isset($_POST['export']) && $_POST['export'] == 'videoconferenza') {
			 
			include("esportazione-catalogo/esportazione-videoconferenza.php");
			 
			 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";
			
			 }
			 
			 
				 else if (isset($_POST['export']) && $_POST['export'] == 'centralini') {
			 
			include("esportazione-catalogo/esportazione-centralini.php");
			 
			 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";
			
			 }

		 else if (isset($_POST['export']) && $_POST['export'] == 'fissi') {
			 
			include("esportazione-catalogo/esportazione-telefoni-fissi.php");
			 
			 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";
			
			 }
			 
			  else if (isset($_POST['export']) && $_POST['export'] == 'ricetrasmittenti') {
			 
			include("esportazione-catalogo/esportazione-ricetrasmittenti.php");
			 
			 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";
			
			 }
			 
					  else if (isset($_POST['export']) && $_POST['export'] == 'gsm') {
			 
			include("esportazione-catalogo/esportazione-gsm.php");
			 
			 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";
			
			 }
			 
					  else if (isset($_POST['export']) && $_POST['export'] == 'audioguide') {
			 
			include("esportazione-catalogo/esportazione-audioguide.php");
			 
			 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";
			
			 }
			 
				 
			  else if (isset($_POST['export']) && $_POST['export'] == 'registratori') {
			 
			include("esportazione-catalogo/esportazione-registratori.php");
			 
			 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";
			
			 }
			 
				  else if (isset($_POST['export']) && $_POST['export'] == 'gateway') {
			 
			include("esportazione-catalogo/esportazione-gateway.php");
			 
			 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";
			
			 }
			 
					  else if (isset($_POST['export']) && $_POST['export'] == 'varie') {
			 
			include("esportazione-catalogo/esportazione-varie.php");
			 
			 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";
			
			 }
			 
			   else if (isset($_POST['export']) && $_POST['export'] == 'listinointerno') {
			 
			include("esportazione-catalogo/esportazione-listino-interno.php");
			 
			 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";
			
			 }
			 
					  else if (isset($_POST['export']) && $_POST['export'] == 'citofoni') {
			 
			include("esportazione-catalogo/esportazione-citofoni.php");
			 
			 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";
			
			 }
			 
							  else if (isset($_POST['export']) && $_POST['export'] == 'ipcamera') {
			 
			include("esportazione-catalogo/esportazione-ipcamera.php");
			 
			 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";
			
			 }
			 
			 else if (isset($_POST['export']) && $_POST['export'] == 'antirumore') {
			 
				include("esportazione-catalogo/esportazione-antirumore.php");
			 
			 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";
			
			 }

			 
			  else if (isset($_POST['per-costruttore'])) {
			 
				include("esportazione-catalogo/esportazione-per-costruttore.php");
			 
			 echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";
			
			 }

		 else {
		 
		 echo "Questa categoria non � ancora pronta, <a href='index.php?tab=ExportCatalog&token=".$this->token."'>clicca qui per tornare indietro</a>";
		 
		 }
		 
		 
		 } else if(isset($_POST['vai']) == 'Importa il file') {
		 
		 
		 if($_FILES['filexls']['size'] != 0) {
			$path = $_SERVER["DOCUMENT_ROOT"];

			$tmp = $_FILES['filexls']['tmp_name']; //cartella temporanea 
			$filexls = $_FILES['filexls']['name'];
			$dir = "$path"."/ezadmin/importazione-catalogo/catalogo_xls/"; //percorso della cartella
			$dirfile = $dir.$filexls;
			move_uploaded_file($tmp, $dirfile);


			if($_POST['import'] == 'solo-prezzi') {
			include("aggiorna_solo_prezzi.php");
			}



			if($_POST['import'] == 'solo-prezzi') {
			include("importazione-catalogo/aggiorna_solo_prezzi.php");

			}
			else {
			if(substr($filexls, 0,15)  == 'audioconferenze') {
			include("importazione-catalogo/import-audioconferenze.php");
			}
			if(substr($filexls, 0,15)  == 'videoconferenza') {
			include("importazione-catalogo/import-videoconferenza.php");
			}
			else if(substr($filexls, 0,6) == 'cuffie') {
			include("importazione-catalogo/import-cuffie.php");
			}
			else if(substr($filexls, 0,5) == 'fissi') {
			include("importazione-catalogo/import-fissi.php");
			}
			else if(substr($filexls, 0,8) == 'cordless') {
			include("importazione-catalogo/import-cordless.php");
			}
			else if(substr($filexls, 0,10) == 'audioguide') {
			include("importazione-catalogo/import-audioguide.php");
			}
			else if(substr($filexls, 0,16) == 'ricetrasmittenti') {
			include("importazione-catalogo/import-ricetrasmittenti.php");
			}
			else if(substr($filexls, 0,12) == 'registratori') {
			include("importazione-catalogo/import-registratori.php");
			}
			else if(substr($filexls, 0,3) == 'gsm') {
			include("importazione-catalogo/import-gsm.php");
			}
			else if(substr($filexls, 0,7) == 'gateway') {
			include("importazione-catalogo/import-gateway.php");
			}
			else if(substr($filexls, 0,10) == 'centralini') {
			include("importazione-catalogo/import-centralini.php");
			}
			else if(substr($filexls, 0,8) == 'citofoni') {
			include("importazione-catalogo/import-citofoni.php");
			}
			else if(substr($filexls, 0,8) == 'ipcamera') {
			include("importazione-catalogo/import-ipcamera.php");
			}
			else if(substr($filexls, 0,10) == 'antirumore') {
			include("importazione-catalogo/import-antirumore.php");
			}
			else if(substr($filexls, 0,5) == 'varie') {
			include("importazione-catalogo/import-varie.php");

			} else {
			echo "File non riconosciuto";
			}

			}
			echo "<br /><br /><a href='index.php?tab=ExportCatalog&token=".$this->token."'>Torna indietro</a>";
					



			}
			/*
			else if (empty($_POST['import'])) {
			echo "Non hai selezionato la categoria da importare... !";
			echo $_POST['import'];

			}
			*/
		 
		 
		 
		 
		 
		 
		} else {


echo "




		 <br />
		 <script type='text/javascript'>

function message (t1, t2) {
	
	strong = document.createElement ('STRONG')
	strong.appendChild (document.createTextNode (t1))
	br = document.createElement ('BR')
	strong.appendChild (document.createTextNode (t2))
	document.getElementsByTagName ('BODY')[0].appendChild (strong)
}

</script>
		<div style='border:1px solid black; float:left; width:400px; padding:5px'>
		<strong>Esporta per categoria</strong><br /><br />
		    Seleziona una categoria da esportare:
			<table>
			<form name='esportazione' method='post'>
				<tr>
			<td><input type='radio' name='export' value='tutto' /></td><td><strong><u>Tutto il catalogo</u></strong></td>
			</tr>
			<tr>
			<tr>
			<td><input type='radio' name='export' value='cuffie' /></td><td><strong>Cuffie</strong> (tempo stimato: 2 minuti ca.)</td>
			</tr>
			<tr>
			<td><input type='radio' name='export' value='cordless' /></td><td><strong>Telefoni cordless</strong> (tempo stimato: 40 secondi ca.)</td>
			</tr>
			<tr>
			<td><input type='radio' name='export' value='fissi' /></td><td><strong>Telefoni fissi</strong> (tempo stimato: 1 minuto ca.)</td>
			</tr>
				<tr>
			<td><input type='radio' name='export' value='audioconferenze' /></td><td><strong>Audioconferenze</strong> (tempo stimato: 15 secondi ca.)</td>
			</tr>
			<tr>
			<td><input type='radio' name='export' value='videoconferenza' /></td><td><strong>Videoconferenza</strong> (tempo stimato: 15 secondi ca.)</td>
			</tr>
			<tr>
			<td><input type='radio' name='export' value='centralini' /></td><td><strong>Centralini</strong> (tempo stimato: 2 minuti e 30 secondi ca.)</td>
			</tr>
				<tr>
			<td><input type='radio' name='export' value='ricetrasmittenti' /></td><td><strong>Ricetrasmittenti</strong> (tempo stimato: 5 secondi ca.)</td>
			</tr>
		<tr>
			<td><input type='radio' name='export' value='audioguide' /></td><td><strong>Audioguide</strong> (tempo stimato: 5 secondi ca.)</td>
			</tr>
						<tr>
			<td><input type='radio' name='export' value='citofoni' /></td><td><strong>Citofoni</strong> (tempo stimato: 5 secondi ca.)</td>
			</tr>
			<tr>
			<td><input type='radio' name='export' value='gsm' /></td><td><strong>GSM/UMTS</strong> (tempo stimato: 10 secondi ca.)</td>
			</tr>
			<tr>
			<td><input type='radio' name='export' value='registratori' /></td><td><strong>Registratori</strong> (tempo stimato: 5 secondi ca.)</td>
			</tr>
				<tr>
			<td><input type='radio' name='export' value='gateway' /></td><td><strong>Gateway VoIP</strong> (tempo stimato: 5 secondi ca.)</td>
			</tr>
					<tr>
			<td><input type='radio' name='export' value='ipcamera' /></td><td><strong>IP Camera</strong> (tempo stimato: 5 secondi ca.)</td>
			</tr>
								<tr>
			<td><input type='radio' name='export' value='antirumore' /></td><td><strong>Cuffie antirumore</strong> (tempo stimato: 5 secondi ca.)</td>
			</tr>
			
				<tr>
			<td><input type='radio' name='export' value='varie' /></td><td><strong>VARIE</strong> (tempo stimato: 30 secondi ca.)</td>
			</tr>
			
				<tr>
			<td><input type='radio' name='export' value='listinointerno' /></td><td><strong>Listino interno</strong> (tempo stimato: 40 secondi ca.)</td>
			</tr>
			
			<tr>
			<td><br /><input type='checkbox' name='senza_seo' /></td><td><br />Spunta per esportare file SENZA campi SEO e descrizioni (file pi&ugrave; leggero)</td>
			</tr>
			
			<tr>
			<td><br /><input type='checkbox' name='senza_fp' /></td><td><br />Spunta per esportare file SENZA prodotti fuori produzione (file pi&ugrave; leggero)</td>
			</tr>
			
					<tr>
			<td></td><td><br /><br /><input type='submit' name='vai' value='Esporta catalogo XLS' />&nbsp;&nbsp;&nbsp;<input type='submit' name='vai-rapido' value='Esporta listino breve clienti' /></td>
			</form>
			
			</tr>
			</table>
		</div>	
			
		<div style='border:1px solid black; float:left; margin-left:15px; width:400px; padding:5px'>
		<strong>Esporta per costruttore</strong><br /><br />
		    Seleziona il costruttore
			<table>
			<form name='esportazione-costruttore' method='post'>
				<tr>
			<td><div style='height:300px; overflow:auto'>"; echo '
				
				
				';
				
				
				$resultsman = Db::getInstance()->ExecuteS("SELECT id_manufacturer, name FROM manufacturer ORDER BY name");
				
				echo "<input type='checkbox' name='per-costruttore[]' value='tutti' /"; 
					echo "> <strong><u>Tutti i costruttori</strong></u> <br /> <br />";
				foreach ($resultsman as $rowman) {
				
					echo "<input type='checkbox' name='per-costruttore[]' value='$rowman[id_manufacturer]' /"; 
					echo "> $rowman[name] <br /> ";
				
				}
					
					
				echo "</div>
			</td>
			</tr>
			
			
			<tr>
			<td><br /><input type='checkbox' name='senza_seo' /> Spunta per esportare file SENZA campi SEO e descrizioni
			<br />
			<input type='checkbox' name='senza_fp' /> Spunta per esportare file SENZA prodotti fuori produzione <br />
			<input type='checkbox' name='includi_inattivi' /> Includi prodotti inattivi <br />
			<input type='checkbox' name='includi_interno' /> Includi prodotti del listino interno <br />
			<input type='checkbox' name='includi_scaricabili' /> Includi prodotti scaricabili <br />
			
			</td><td></td>
			</tr>
			
					<tr>
			<td><br /><input type='submit' name='vai' value='Esporta catalogo XLS' />&nbsp;&nbsp;&nbsp;<input type='submit' name='vai-rapido-c' value='Esporta listino breve clienti' /></td><td></td>
			</form>
			
			</tr>
			</table>
		</div>	
			
			<div style='clear:both'></div>
			
			
			<br /><br /><br /><br />";
			
			/*
			echo "
			<strong>Copia prodotti correlati</strong>
			<br /><br />
			<form method='post'>
			<select name='prodotti-correlati1'>
			<option name='' value=''>-- Seleziona un prodotto -- </option>";
			$query_corr = "SELECT name FROM product_lang WHERE id_lang = 5";
			$res_corr = mysql_query($query_corr);
			while ($rowcorr = mysql_fetch_array($res_corr, MYSQL_ASSOC)) {
			echo "<option name='$rowcorr[name]' value='$rowcorr[name]'>$rowcorr[name]</option>";
			}
			
			echo "</select>";
			
			echo "<select name='prodotti-correlati2'>
			<option name='' value=''>-- Seleziona un prodotto -- </option>";
			$query_corr = "SELECT name FROM product_lang WHERE id_lang = 5";
			$res_corr = mysql_query($query_corr);
			while ($rowcorr = mysql_fetch_array($res_corr, MYSQL_ASSOC)) {
			echo "<option name='$rowcorr[name]' value='$rowcorr[name]'>$rowcorr[name]</option>";
			}
			
			echo "</select>";
					echo "<select name='prodotti-correlati3'>
			<option name='' value=''>-- Seleziona un prodotto -- </option>";
			$query_corr = "SELECT name FROM product_lang WHERE id_lang = 5";
			$res_corr = mysql_query($query_corr);
			while ($rowcorr = mysql_fetch_array($res_corr, MYSQL_ASSOC)) {
			echo "<option name='$rowcorr[name]' value='$rowcorr[name]'>$rowcorr[name]</option>";
			}
			
			echo "</select>";
			
					echo "<select name='prodotti-correlati4'>
			<option name='' value=''>-- Seleziona un prodotto -- </option>";
			$query_corr = "SELECT name FROM product_lang WHERE id_lang = 5";
			$res_corr = mysql_query($query_corr);
			while ($rowcorr = mysql_fetch_array($res_corr, MYSQL_ASSOC)) {
			echo "<option name='$rowcorr[name]' value='$rowcorr[name]'>$rowcorr[name]</option>";
			}
			
			echo "</select>";
			
					echo "<select name='prodotti-correlati5'>
			<option name='' value=''>-- Seleziona un prodotto -- </option>";
			$query_corr = "SELECT name FROM product_lang WHERE id_lang = 5";
			$res_corr = mysql_query($query_corr);
			while ($rowcorr = mysql_fetch_array($res_corr, MYSQL_ASSOC)) {
			echo "<option name='$rowcorr[name]' value='$rowcorr[name]'>$rowcorr[name]</option>";
			}
			
			echo "</select>";
			
					echo "<select name='prodotti-correlati6'>
			<option name='' value=''>-- Seleziona un prodotto -- </option>";
			$query_corr = "SELECT name FROM product_lang WHERE id_lang = 5";
			$res_corr = mysql_query($query_corr);
			while ($rowcorr = mysql_fetch_array($res_corr, MYSQL_ASSOC)) {
			echo "<option name='$rowcorr[name]' value='$rowcorr[name]'>$rowcorr[name]</option>";
			}
			
			echo "</select>";
			
					echo "<select name='prodotti-correlati7'>
			<option name='' value=''>-- Seleziona un prodotto -- </option>";
			$query_corr = "SELECT name FROM product_lang WHERE id_lang = 5";
			$res_corr = mysql_query($query_corr);
			while ($rowcorr = mysql_fetch_array($res_corr, MYSQL_ASSOC)) {
			echo "<option name='$rowcorr[name]' value='$rowcorr[name]'>$rowcorr[name]</option>";
			}
			
			echo "</select>";
			
					echo "<select name='prodotti-correlati8'>
			<option name='' value=''>-- Seleziona un prodotto -- </option>";
			$query_corr = "SELECT name FROM product_lang WHERE id_lang = 5";
			$res_corr = mysql_query($query_corr);
			while ($rowcorr = mysql_fetch_array($res_corr, MYSQL_ASSOC)) {
			echo "<option name='$rowcorr[name]' value='$rowcorr[name]'>$rowcorr[name]</option>";
			}
			
			echo "</select>";
			
					echo "<select name='prodotti-correlati9'>
			<option name='' value=''>-- Seleziona un prodotto -- </option>";
			$query_corr = "SELECT name FROM product_lang WHERE id_lang = 5";
			$res_corr = mysql_query($query_corr);
			while ($rowcorr = mysql_fetch_array($res_corr, MYSQL_ASSOC)) {
			echo "<option name='$rowcorr[name]' value='$rowcorr[name]'>$rowcorr[name]</option>";
			}
			
			echo "</select>";
			
					echo "<select name='prodotti-correlati10'>
			<option name='' value=''>-- Seleziona un prodotto -- </option>";
			$query_corr = "SELECT name FROM product_lang WHERE id_lang = 5";
			$res_corr = mysql_query($query_corr);
			while ($rowcorr = mysql_fetch_array($res_corr, MYSQL_ASSOC)) {
			echo "<option name='$rowcorr[name]' value='$rowcorr[name]'>$rowcorr[name]</option>";
			}
			
			echo "</select>";
			
			
			echo "<br /><br />
			<input type='submit' name='copiacorrelati' value='Clicca per visualizzare la stringa da copiare' />
				</form>
			
			<br /><br />";
			if(isset($_POST['copiacorrelati'])) {
			echo "Copia la stringa di testo seguente nella casella \"Prodotti correlati\" del foglio di Excel: <br /><br />";
			
			echo $_POST['prodotti-correlati1'].";".$_POST['prodotti-correlati2'].";".$_POST['prodotti-correlati3'].";".$_POST['prodotti-correlati4'].";".$_POST['prodotti-correlati5'].";".$_POST['prodotti-correlati6'].";".$_POST['prodotti-correlati7'].";".$_POST['prodotti-correlati8'];
			
			}
			else { }
			
		
		echo "</table>
			<br /><br />";
			
			*/
			
			echo "
			<strong>Importa file XLS e carica/aggiorna prodotti</strong>
			<br /><br />
			Attraverso questo form puoi importare un foglio di Excel per aggiornare i prodotti esistenti nel database o per inserirne di nuovi. Seleziona il file da importare, quindi clicca su 'Carica': il file sar&agrave; automaticamente importato, i prodotti nuovi saranno inseriti automaticamente e quelli gi&agrave; esistenti saranno aggiornati. A seconda delle dimensioni del file, la procedura potrebbe impiegare qualche minuto prima di essere terminata.<br /><br />
			<form name='uploadfile' method='post' enctype='multipart/form-data'>
			
			<input type='file' name='filexls' id='filexls' style='width:400px; '/>
			
			<br /><br />
			 Seleziona una categoria da importare:
		<table>
		
				
			
			
				<tr>
			<td> <input type='radio' name='import' value='solo-prezzi' id='solo-prezzi' /></td><td><strong>NON IMPORTARE, AGGIORNA SOLO I PREZZI*</strong><br /><br />
			* aggiorna: tutti i prezzi, categoria, costruttore, fornitore, codici SKU e eSolver, ASIN, EAN, condizione, peso, sconti, online/offline/old, data disponibilit&agrave;, trasporto gratuito, punti di forza, noindex, esportazione Amazon e Google Shopping, garanzia, confezione, messaggio prodotto disponibile, messaggio prodotto non disponibile.</td>
			</tr>
			<tr>
			<td></td><td>
			<br /><br />
			<input type='submit' name='vai' value='Importa il file' onclick='var fu1 = document.getElementById(\"filexls\").value; var ftype = fu1.split(\"-\"); ftype = ftype[0]; if(ftype != \"cuffie\" && ftype != \"centralini\" && ftype != \"fissi\" && ftype != \"ricetrasmittenti\" && ftype != \"registratori\" && ftype != \"ipcamera\" && ftype != \"gsm\" && ftype != \"gatewayvoip\" && ftype != \"cordless\" && ftype != \"citofoni\" && ftype != \"audioguide\" && ftype != \"audioconferenze\" && ftype != \"antirumore\" && ftype != \"videoconferenza\" && document.getElementById(\"solo-prezzi\").checked == false) { alert(\"Devi cliccare su AGGIORNA SOLO PREZZI per importare questo tipo di file\"); return false } else { }' />
			</form>
			</td>
			</tr>
			</table>";
			
			//echo "WORK IN PROGRESS in arrivo i file Excel 2.0... ";
			}
			
	}
	

}


