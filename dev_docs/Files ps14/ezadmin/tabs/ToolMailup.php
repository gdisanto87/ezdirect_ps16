<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14632 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class ToolMailup extends AdminTab
{
	
	public function bindDatepickerZ($id, $time)
	{
		if ($time)
			echo '
			var dateObj = new Date();
			var hours = dateObj.getHours();
			var mins = dateObj.getMinutes();
			var secs = dateObj.getSeconds();
			if (hours < 10) { hours = "0" + hours; }
			if (mins < 10) { mins = "0" + mins; }
			if (secs < 10) { secs = "0" + secs; }
			var time = " "+hours+":"+mins+":"+secs;';

		echo '
		$(function() {
			$("#'.Tools::htmlentitiesUTF8($id).'").datepicker({
				prevText:"",
				nextText:"",
				dateFormat:"dd-mm-yy"});
		});';
	}

	// id can be a identifier or an array of identifiers
	public function includeDatepickerZ($id, $time = false)
	{
		global $cookie;
		echo '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery-ui-1.8.10.custom.min.js"></script>';
		$iso = Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'lang WHERE `id_lang` = '.(int)$cookie->id_lang);
		if ($iso != 'en')
			echo '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/datepicker/ui/i18n/ui.datepicker-'.Tools::htmlentitiesUTF8($iso).'.js"></script>';
		echo '<script type="text/javascript">';
			if (is_array($id))
				foreach ($id as $id2)
					$this->bindDatepickerZ($id2, $time);
			else
				$this->bindDatepickerZ($id, $time);
		echo '</script>';
	}

	public function display() 
	{
		
		include_once('functions.php');
		
		global $currentIndex;

		echo '<h1>Esportazione Mailup</h1>';
		
		$this->includeDatepickerZ(array('arco_dal', 'arco_al'), true);
		
		if(Tools::getIsset('submitted'))
		{
			if(!empty($_POST['arco_dal'])) {
						
				$data_dal_a = explode("-", $_POST['arco_dal']);
				$data_dal = $data_dal_a[2]."-".$data_dal_a[1]."-".$data_dal_a[0]; 
				
			}
		
			else
			{
				$data_dal = date('Y-m-d 00:00:00',strtotime("-30 days"));
			}
			if(!empty($_POST['arco_al'])) {
						
				$data_al_a = explode("-", $_POST['arco_al']);
				$data_al = $data_al_a[2]."-".$data_al_a[1]."-".$data_al_a[0]; 
				
			}
			else
			{
				$data_al = date('Y-m-d 00:00:00',strtotime("now"));
			}
			
			if(Tools::isSubmit('sub') && Tools::getValue('sub') == 'Aziende TUTTI')
			{
				$query = "select c.firstname,c.lastname,c.company,c.email,a.address1,a.city,a.postcode,a.phone from customer c join address a on c.id_customer = a.id_customer where a.active = 1 and c.ex_rivenditore = 0 and a.deleted = 0 and a.fatturazione = 1 and c.is_company = 1 and (c.id_default_group != 15 and c.id_default_group != 3) and email not like '%amazon%' and c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
				
			}
			else if(Tools::isSubmit('sub') && Tools::getValue('sub') == 'Aziende GDPR')
			{
				$query = "select c.firstname,c.lastname,c.company,c.email,a.address1,a.city,a.postcode,a.phone from customer c join address a on c.id_customer = a.id_customer where a.active = 1 and c.ex_rivenditore = 0 and a.deleted = 0 and a.fatturazione = 1 and c.newsletter = 1 and c.is_company = 1 and (c.id_default_group != 15 and c.id_default_group != 3) and email not like '%amazon%' and c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
				
			}
			
			else if(Tools::isSubmit('sub') && Tools::getValue('sub') == 'Privati TUTTI')
			{
				$query = "select c.firstname,c.lastname,c.company,c.email,a.address1,a.city,a.postcode,a.phone from customer c join address a on c.id_customer = a.id_customer where a.active = 1 and a.deleted = 0 and a.fatturazione = 1 and c.is_company = 0 and email not like '%amazon%' and c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
				
			}
			else if(Tools::isSubmit('sub') && Tools::getValue('sub') == 'Privati GDPR')
			{
				$query = "select c.firstname,c.lastname,c.company,c.email,a.address1,a.city,a.postcode,a.phone from customer c join address a on c.id_customer = a.id_customer where a.active = 1 and a.deleted = 0 and a.fatturazione = 1 and c.newsletter = 1 and c.is_company = 0 and email not like '%amazon%' and c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
				
			}
			
			else if(Tools::isSubmit('sub') && Tools::getValue('sub') == 'Rivenditori 1 TUTTI')
			{
				$query = "select c.firstname,c.lastname,c.company,c.email,a.address1,a.city,a.postcode,a.phone from customer c join address a on c.id_customer = a.id_customer where a.active = 1 and a.deleted = 0 and a.fatturazione = 1 and c.is_company = 1 and (c.id_default_group = 3) and email not like '%amazon%' and c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
				
			}
			else if(Tools::isSubmit('sub') && Tools::getValue('sub') == 'Rivenditori 1 GDPR')
			{
				$query = "select c.firstname,c.lastname,c.company,c.email,a.address1,a.city,a.postcode,a.phone from customer c join address a on c.id_customer = a.id_customer where a.active = 1 and a.deleted = 0 and a.fatturazione = 1 and c.newsletter = 1 and c.is_company = 1 and (c.id_default_group = 3) and email not like '%amazon%' and c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
				
			}
			
			else if(Tools::isSubmit('sub') && Tools::getValue('sub') == 'Rich. Riv. TUTTI')
			{
				$query = "select c.firstname,c.lastname,c.company,c.email,a.address1,a.city,a.postcode,a.phone from customer c join address a on c.id_customer = a.id_customer where a.active = 1 and a.deleted = 0 and a.fatturazione = 1 and c.is_company = 1 and (c.id_default_group = 15) and email not like '%amazon%' and c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
				
			}
			else if(Tools::isSubmit('sub') && Tools::getValue('sub') == 'Rich. Riv. GDPR')
			{
				$query = "select c.firstname,c.lastname,c.company,c.email,a.address1,a.city,a.postcode,a.phone from customer c join address a on c.id_customer = a.id_customer where a.active = 1 and a.deleted = 0 and a.fatturazione = 1 and c.is_company = 1 and (c.id_default_group = 15 ) and c.newsletter = 1 and email not like '%amazon%' and c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
				
			}
			else if(Tools::isSubmit('sub') && Tools::getValue('sub') == 'Ex Rivenditori TUTTI')
			{
				$query = "select c.firstname,c.lastname,c.company,c.email,a.address1,a.city,a.postcode,a.phone from customer c join address a on c.id_customer = a.id_customer where a.active = 1 and a.deleted = 0 and a.fatturazione = 1 and c.is_company = 1 and c.ex_rivenditore = 1 and email not like '%amazon%' and c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
				
			}
			else if(Tools::isSubmit('sub') && Tools::getValue('sub') == 'Ex Rivenditori GDPR')
			{
				$query = "select c.firstname,c.lastname,c.company,c.email,a.address1,a.city,a.postcode,a.phone from customer c join address a on c.id_customer = a.id_customer where a.active = 1 and a.deleted = 0 and a.fatturazione = 1 and c.is_company = 1 and c.ex_rivenditore = 1 and email not like '%amazon%' and c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
				
			}
			
			$res = Db::getInstance()->executeS($query);
			
			$file = '';
			foreach($res as $row)
			{
				$file .= $row['firstname'].';'.$row['lastname'].';'.$row['company'].';'.$row['email'].';'.str_replace(';',',',$row['address1']).';'.$row['city'].';'.$row['postcode'].';'.$row['phone']."\n";
			}
			
			$data = date('d-m-Y',strtotime("now"));
			
			$file_csv_mailup_stream=fopen("/var/www/vhosts/ezdirect.it/httpdocs/ezadmin/esportazione-catalogo/catalogo_xls/".str_replace(' ','',Tools::getValue('sub'))."-$data-mailup.csv","w+");
			
				@fwrite($file_csv_mailup_stream,$file);
				
				@fclose("/var/www/vhosts/ezdirect.it/httpdocs/ezadmin/esportazione-catalogo/catalogo_xls/".str_replace(' ','',Tools::getValue('sub'))."-$data-mailup.csv");
		
				header("Location: http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/".str_replace(' ','',Tools::getValue('sub'))."-$data-mailup.csv");
				
		}		
				
		
		
		
		
		echo "<form method='post' action=''>";	
		
		echo 'Clicca sul file da prelevare. Puoi selezionare un periodo con gli appositi campi data. Se non selezioni il periodo, viene esportato il file degli ultimi 30 giorni. Se un indirizzo su Mailup &egrave; doppio, non viene esportato<br /><br />';
		
		echo 'Dal <input type="text" id="arco_dal" name="arco_dal"  /> al <input type="text" id="arco_al" name="arco_al"  /><br /><br />';
		
		echo '<input type="submit" value="Aziende TUTTI" name="sub" style="width:250px" class="button"/><br /><br />';
		echo '<input type="submit" value="Aziende GDPR" name="sub" style="width:250px" class="button"/><br /><br />';
		echo '<input type="submit" value="Privati TUTTI" name="sub" style="width:250px" class="button"/><br /><br />';
		echo '<input type="submit" value="Privati GDPR" name="sub" style="width:250px" class="button"/><br /><br />';
		echo '<input type="submit" value="Rivenditori 1 TUTTI" name="sub" style="width:250px" class="button"/><br /><br />';
		echo '<input type="submit" value="Rivenditori 1 GDPR" name="sub" style="width:250px" class="button"/><br /><br />';
		echo '<input type="submit" value="Rich. Riv. TUTTI" name="sub" style="width:250px" class="button"/><br /><br />';
		echo '<input type="submit" value="Rich. Riv. GDPR" name="sub" style="width:250px" class="button"/><br /><br />';
		echo '<input type="submit" value="Ex Rivenditori TUTTI" name="sub" style="width:250px" class="button"/><br /><br />';
		echo '<input type="submit" value="Ex Rivenditori GDPR" name="sub" style="width:250px" class="button"/><br /><br />';
		echo '<input type="hidden" value="y" name="submitted" />';
		echo '</form>';
	
	}
}

