<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/../classes/AdminTab.php');

class AdminCarts extends AdminTab
{
	public function __construct()
	{
	 	$this->table = 'carrelli_creati';
	 	$this->className = 'Cart';
		$this->lang = false;
	 	$this->edit = false;
	 	$this->view = true;
	 	$this->delete = true;

		$this->_select = '(CASE c.is_company WHEN 0 THEN CONCAT(LEFT(c.`firstname`, 1), \'. \', c.`lastname`)
		WHEN 1 THEN c.company
		ELSE CONCAT(LEFT(c.`firstname`, 1), \'. \', c.`lastname`)
		END
		)  `customer`, 
		(SELECT firstname FROM employee WHERE id_employee = a.created_by) `created`, 
		
		a.id_cart total, ca.name carrier, o.id_order ';
		$this->_join = 'LEFT JOIN '._DB_PREFIX_.'customer c ON (c.id_customer = a.id_customer)
		LEFT JOIN '._DB_PREFIX_.'currency cu ON (cu.id_currency = a.id_currency)
		LEFT JOIN '._DB_PREFIX_.'carrier ca ON (ca.id_carrier = a.id_carrier)
		LEFT JOIN '._DB_PREFIX_.'orders o ON (o.id_cart = a.id_cart) ';

 		$this->fieldsDisplay = array(
			'id_cart' => array('title' => AdminCarts::lx('ID'), 'align' => 'center', 'width' => 25),
			'id_order' => array('title' => AdminCarts::lx('ID Order'), 'align' => 'center', 'width' => 25),
			'customer' => array('title' => AdminCarts::lx('Customer'), 'width' => 80, 'filter_key' => 'customer', 'tmpTableFilter' => true),
			'total' => array('title' => AdminCarts::lx('Total'), 'callback' => 'getOrderTotalUsingTaxCalculationMethod', 'orderby' => false, 'search' => false, 'width' => 50, 'align' => 'right', 'prefix' => '<b>', 'suffix' => '</b>', 'currency' => true),
			'carrier' => array('title' => AdminCarts::lx('Carrier'), 'width' => 25, 'align' => 'center', 'callback' => 'replaceZeroByShopName', 'filter_key' => 'ca!name'),
			'created' => array('title' => AdminCarts::lx('Creato da'), 'width' => 55, 'filter_key' => 'created'),
			'date_add' => array('title' => AdminCarts::lx('Date'), 'width' => 90, 'align' => 'right', 'type' => 'datetime', 'filter_key' => 'a!date_add'));

		parent::__construct();
		
		if(isset($_GET['deleteccart'])) {
			Db::getInstance()->executeS("DELETE FROM carrelli_creati WHERE id_cart = ".$_GET['id_cart']."");
			Db::getInstance()->executeS("DELETE FROM carrelli_creati_prodotti WHERE id_cart = ".$_GET['id_cart']."");
		
			global $cookie;
			
			Tools::redirectAdmin("index.php?tab=AdminCustomers&id_customer=".$_GET['id_customer']."&viewcustomer&token=".Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee))."&tab-container-1=4");
			
		}
	}

	public function viewDetails()
	{
		global $currentIndex, $cookie;
		
		if(isset($_GET['getPDF'])) {
		
			require_once('../classes/html2pdf/html2pdf.class.php');
			
			$id_cst = Db::getInstance()->getValue("SELECT id_customer FROM cart WHERE id_cart = ".$_GET['id_cart']."");
			$content = Cart::getCartPDF($_GET['id_cart'], $id_cst);
			ob_clean();
			$html2pdf = new HTML2PDF('P','A4','it');
			$html2pdf->WriteHTML($content);
			$html2pdf->Output($id_cst.'-offerta-'.$_GET['id_cart'].'.pdf', 'D'); 
			
		}
		

		
		
		if(isset($_GET['createnew'])) {
			
				$getLastCart = Db::getInstance()->getValue("SELECT id_cart FROM cart ORDER BY id_cart DESC");
				$newCart = $getLastCart+1;
				$address_fatturazione = Db::getInstance()->getValue("SELECT id_address FROM address WHERE customer = ".$_GET['id_customer']." AND fatturazione = 1 AND deleted = 0 AND active = 1");
				
				Db::getInstance()->executeS("INSERT INTO cart (id_cart, id_carrier, id_lang, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, date_add, date_upd, created_by, id_employee) VALUES (
				'".$newCart."', 0, 5, '".$address_fatturazione."', '".$address_fatturazione."', 1, '".$_GET['id_customer']."', 0, '', 0,0,'', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."', ".$cookie->id_employee.", ".$cookie->id_employee.")");
				
				Db::getInstance()->executeS("INSERT INTO carrelli_creati (id_cart, id_carrier, id_lang, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, date_add, date_upd, created_by, id_employee) VALUES (
				'".$newCart."', 0, 5, '".$address_fatturazione."', '".$address_fatturazione."', 1, '".$_GET['id_customer']."', 0, '', 0,0,'', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."', ".$cookie->id_employee.", ".$cookie->id_employee.")");
				
				Tools::redirectAdmin($currentIndex.'&id_customer='.$_GET['id_customer'].'&viewcustomer&id_cart='.$newCart.'&conf=4&tab-container-1=4&viewcart'.'&token='.$this->token);
				
				
			
			}

		/*if (!($cart = AdminCarts::loadObject(true)))
			return;*/
		$cart = new Cart(Tools::getValue('id_cart'));		
			
		$customer = new Customer($cart->id_customer);
		$customerStats = $customer->getStats();
		$products = $cart->getProducts();
		$customizedDatas = Product::getAllCustomizedDatas((int)($cart->id));
		Product::addCustomizationPrice($products, $customizedDatas);
		$summary = $cart->getSummaryDetails();
		$discounts = $cart->getDiscounts();

		$currency = new Currency($cart->id_currency);
		$currentLanguage = new Language((int)($cookie->id_lang));
		
		$creato_da = Db::getInstance()->getValue("SELECT created_by FROM cart WHERE id_cart = ".$cart->id."");
		$creato_da_nome = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".$creato_da."");
		// display cart header
		echo '<h2>'.(($customer->id) ? ($customer->is_company == 1 ? $customer->company: $customer->firstname.' '.$customer->lastname) : AdminCarts::lx('Guest')).' - '.AdminCarts::lx('Cart #').sprintf('%06d', $cart->id).' '.AdminCarts::lx('creato il').' '.date("d/m/Y H:i:s", strtotime($cart->date_add)).' '.($creato_da == 0 ? 'dal cliente' : 'da '.$creato_da_nome).'</h2>';

		/* Display customer information */
		echo '
		<br />
		<form name="products" id="products" method="post" action="'.$currentIndex.'&id_cart='.$_GET['id_cart'].'&id_customer='.$customer->id.'&viewcustomer&viewcart&cartupdated&token='.$this->token.'#modifica-carrello">
		<div style="float: left;">
		<fieldset style="width: 400px">
			<legend><img src="../img/admin/tab-customers.gif" /> '.AdminCarts::lx('Customer information').'</legend>
			<span style="font-weight: bold; font-size: 14px;">';
			if ($customer->id) {
				echo '
			<a href="?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'"> '.($customer->is_company == 1 ? $customer->company: $customer->firstname.' '.$customer->lastname).'</a></span> ('.AdminCarts::lx('#').$customer->id.')<br />
			<strong>Gruppo</strong>: '.($customer->id_default_group == 3 ? 'Rivenditori' : 'Clienti web').'<br /><strong>Email</strong>: <a href="mailto:'.$customer->email.'">'.$customer->email.'</a><br />';
			
			$customer_phone = Db::getInstance()->getRow("SELECT	phone, phone_mobile FROM address WHERE id_customer = ".$customer->id." AND deleted = 0 AND fatturazione = 1 AND active = 1");
			
			if($customer_phone['phone'] == '') { $telefono_cliente = $customer_phone['phone_mobile']; } else { $telefono_cliente = $customer_phone['phone']; }
			
			echo '<strong>Telefono</strong>: '.$telefono_cliente.'<br />';
			
			/*
			
			'.AdminCarts::lx('Account registered:').' '.Tools::displayDate($customer->date_add, (int)($cookie->id_lang), true).'<br />
			'.AdminCarts::lx('Valid orders placed:').' <b>'.$customerStats['nb_orders'].'</b><br />
			'.AdminCarts::lx('Total paid since registration:').' <b>'.Tools::displayPrice($customerStats['total_orders'], $currency, false).'</b><br />';*/
			} else {
				echo AdminCarts::lx('Guest not registered').'</span>';
			}
		echo '</fieldset>';
		echo '
		</div>
		<div style="float: left; margin-left: 40px">';

		/* Display order information */
	
		$id_order = (int)(Order::getOrderByCartId($cart->id));
		$order = new Order($id_order);
		
		if ($order->getTaxCalculationMethod() == PS_TAX_EXC)
		{
		    $total_products = $summary['total_products'];
		    $total_discount = $summary['total_discounts_tax_exc'];
		    $total_wrapping = $summary['total_wrapping_tax_exc'];
		    $total_price = $summary['total_price_without_tax'];
   		    $total_shipping = $summary['total_shipping_tax_exc'];
		} else {
		    $total_products = $summary['total_products_wt'];
		    $total_discount = $summary['total_discounts'];
		    $total_wrapping = $summary['total_wrapping'];
		    $total_price = $summary['total_price'];
  		    $total_shipping = $summary['total_shipping'];
		}
		if ($order->id) {
			echo '
			<fieldset style="width: 400px">
				<legend><img src="../img/admin/cart.gif" /> '.AdminCarts::lx('Order information').'</legend>
				<span style="font-weight: bold; font-size: 14px;">';
				if ($order->id)
					echo '
				<a href="?tab=AdminOrders&id_order='.(int)($order->id).'&vieworder&token='.Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).(int)($cookie->id_employee)).'"> '.AdminCarts::lx('Order #').sprintf('%06d', $order->id).'</a></span>
				<br /><br />
				'.AdminCarts::lx('Made on:').' '.Tools::displayDate($order->date_add, (int)$cookie->id_lang, true).'<br /><br /><br /><br />';
				else
					echo AdminCarts::lx('No order created from this cart').'</span>';
			echo '</fieldset>';
			echo '
			</div>';
		}
		
		else {
			$validita = Db::getInstance()->getValue("SELECT validita FROM cart WHERE id_cart = ".$_GET['id_cart']."");
			$payment = Db::getInstance()->getValue("SELECT payment FROM cart WHERE id_cart = ".$_GET['id_cart']."");
			$name = Db::getInstance()->getValue("SELECT name FROM cart WHERE id_cart = ".$_GET['id_cart']."");
			$consegna = Db::getInstance()->getValue("SELECT consegna FROM cart WHERE id_cart = ".$_GET['id_cart']."");
			$note = Db::getInstance()->getValue("SELECT note FROM cart WHERE id_cart = ".$_GET['id_cart']."");
			include_once('functions.php');
		includeDatepicker(array('validita'), true);
			echo '
			<fieldset style="width: 400px">
				<legend><img src="../img/admin/cart.gif" /> '.AdminCarts::lx('Informazioni sul carrello').'</legend>
				
				
				<table>
				';
			$date_validita = date('Y-m-d', strtotime($Date. ' + 15 days'));
			$impiegato = Db::getInstance()->getValue("SELECT id_employee FROM cart WHERE id_cart = ".$_GET['id_cart']."");
			if($impiegato != 0) { $nome_impiegato = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".$impiegato.""); } else { }
			echo '<tr><td colspan="2"><strong>'.($impiegato == 0 ? 'Questo carrello &egrave; stato creato dal cliente' : 'Questo carrello &egrave; stato modificato l\'ultima volta in data '.date("d/m/Y H:i:s", strtotime($cart->date_upd)).' da '.$nome_impiegato.'').'</strong></td></tr>';
			echo '<tr><td>Nome offerta:</td><td> <input type="text" id="name" name="name" value="'.$name.'" /></td></tr>';
			echo '<tr><td>Offerta valida fino al:</td><td> <input type="text" id="validita" name="validita" value="'.((isset($validita) && $validita != '0000-00-00 00:00:00' && $validita != '1942-01-01') ? $validita : $date_validita).'" /></td></tr>';
			echo '<tr><td>Tempi di consegna:</td><td> <input type="text" id="consegna" name="consegna" value="'.(!empty($consegna) ? $consegna : "2-5 gg.").'" /></td></tr>';
			$tokenCarts = Tools::getAdminToken('AdminCarts'.(int)(Tab::getIdFromClassName('AdminCarts')).(int)($cookie->id_employee));
			echo '<tr><td><a href="index.php?tab=AdminCarts&id_cart='.$cart->id.'&viewcart&getPDF=y&token='.$tokenCarts.'"><strong>Ottieni PDF del carrello</strong></a></td><td></td></tr>';
			echo '</table></fieldset>';
			echo '
			</div>';
			
			
		}
		// List of products
		echo '
		<br style="clear:both;" />
			<fieldset style="margin-top:25px; width: 715px; ">
				<legend><img src="../img/admin/cart.gif" alt="'.AdminCarts::lx('Products').'" />'.AdminCarts::lx('Cart summary').'</legend>';
				
				
		if ($order->id) {
						
				echo '<div style="float:left;">
				<strong>Nome offerta:</strong> '.$name.'<br /><br />
				<strong>Validit&agrave; offerta:</strong> '.$validita.'<br /><br />
				<strong>Tempi di consegna:</strong> '.$consegna.'<br /><br />
				<strong>Note:</strong> '.$note.'<br /><br />
				
				
					<table style="width: 700px;" cellspacing="0" cellpadding="0" class="table" id="orderProducts">
						<tr>
							<th align="center" style="width: 60px">&nbsp;</th>
							<th>'.AdminCarts::lx('Product').'</th>
							<th style="width: 80px; text-align: center">'.AdminCarts::lx('UP').'</th>
							<th style="width: 20px; text-align: center">'.AdminCarts::lx('Qty').'</th>
							<th style="width: 30px; text-align: center">'.AdminCarts::lx('Stock').'</th>
							<th style="width: 90px; text-align: right; font-weight:bold;">'.AdminCarts::lx('Total').'</th>
						</tr>';
						$tokenCatalog = Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee));
						foreach ($products as $k => $product)
						{
                   			if ($order->getTaxCalculationMethod() == PS_TAX_EXC)
                			{
                			    $product_price = $product['price'];
                			    $product_total = $product['total'];
                			} else {
	            			    $product_price = $product['price_wt'];
                			    $product_total = $product['total_wt'];
                			}


							$image = array();
							if (isset($product['id_product_attribute']) AND (int)($product['id_product_attribute']))
								$image = Db::getInstance()->getRow('
								SELECT id_image
								FROM '._DB_PREFIX_.'product_attribute_image
								WHERE id_product_attribute = '.(int)($product['id_product_attribute']));
						 	if (!isset($image['id_image']))
								$image = Db::getInstance()->getRow('
								SELECT id_image
								FROM '._DB_PREFIX_.'image
								WHERE id_product = '.(int)($product['id_product']).' AND cover = 1');
						 	$stock = Db::getInstance()->getRow('
							SELECT '.($product['id_product_attribute'] ? 'pa' : 'p').'.quantity
							FROM '._DB_PREFIX_.'product p
							'.($product['id_product_attribute'] ? 'LEFT JOIN '._DB_PREFIX_.'product_attribute pa ON p.id_product = pa.id_product' : '').'
							WHERE p.id_product = '.(int)($product['id_product']).'
							'.($product['id_product_attribute'] ? 'AND pa.id_product_attribute = '.(int)($product['id_product_attribute']) : ''));
							/* Customization display */
							AdminCarts::displayCustomizedDatas($customizedDatas, $product, $currency, $image, $tokenCatalog, $stock);
							if ($product['cart_quantity'] > $product['customizationQuantityTotal'])
							{
								$imageProduct = new Image($image['id_image']);
								echo '
								<tr>
									<td align="center">'.(isset($image['id_image']) ? cacheImage(_PS_IMG_DIR_.'p/'.$imageProduct->getExistingImgPath().'.jpg',
									'product_mini_'.(int)($product['id_product']).(isset($product['id_product_attribute']) ? '_'.(int)($product['id_product_attribute']) : '').'.jpg', 45, 'jpg') : '--').'</td>
									<td><a href="index.php?tab=AdminCatalog&id_product='.$product['id_product'].'&updateproduct&token='.$tokenCatalog.'">
										<span class="productName">'.$product['name'].'</span><br />
										'.($product['reference'] ? AdminCarts::lx('Ref:').' '.$product['reference'] : '')
										.(($product['reference'] AND $product['supplier_reference']) ? ' / '.$product['supplier_reference'] : '')
										.'</a></td>
									<td align="center">'.Tools::displayPrice($product_price, $currency, false).'</td>
									<td align="center" class="productQuantity">'.((int)($product['cart_quantity']) - $product['customizationQuantityTotal']).'</td>
									<td align="center" class="productQuantity">'.(int)($stock['quantity']).'</td>
									<td align="right">'.Tools::displayPrice($product_total, $currency, false).'</td>
								</tr>';
							}
						}
					echo '
					<tr class="cart_total_product">
				<td colspan="5">'.AdminCarts::lx('Total products:').'</td>
				<td class="price bold right">'.Tools::displayPrice($total_products, $currency, false).'</td>
			</tr>';

			if ($summary['total_discounts'] != 0)
			echo '
			<tr class="cart_total_voucher">
				<td colspan="5">'.AdminCarts::lx('Total vouchers:').'</td>
				<td class="price-discount bold right">'.Tools::displayPrice($total_discount, $currency, false).'</td>
			</tr>';
			if ($summary['total_wrapping'] > 0)
			 echo '
			 <tr class="cart_total_voucher">
				<td colspan="5">'.AdminCarts::lx('Total gift-wrapping:').'</td>
				<td class="price-discount bold right">'.Tools::displayPrice($total_wrapping, $currency, false).'</td>
			</tr>';
			if ($cart->getOrderTotal(true, Cart::ONLY_SHIPPING) > 0)
			echo '
			<tr class="cart_total_delivery">
				<td colspan="5">'.AdminCarts::lx('Total shipping:').'</td>
				<td class="price bold right">'.Tools::displayPrice($total_shipping, $currency, false).'</td>
			</tr>';
			echo '
			<tr class="cart_total_price">
				<td colspan="5" class="bold">'.AdminCarts::lx('Total:').'</td>
				<td class="price bold right">'.Tools::displayPrice($total_price, $currency, false).'</td>
			</tr>
			</table>';

			if (sizeof($discounts))
			{
				echo '
			<table cellspacing="0" cellpadding="0" class="table" style="width:280px; margin:15px 0px 0px 420px;">
				<tr>
					<th><img src="../img/admin/coupon.gif" alt="'.AdminCarts::lx('Discounts').'" />'.AdminCarts::lx('Discount name').'</th>
					<th align="center" style="width: 100px">'.AdminCarts::lx('Value').'</th>
				</tr>';

				foreach ($discounts as $discount)
					echo '
				<tr>
					<td><a href="?tab=AdminDiscounts&id_discount='.$discount['id_discount'].'&updatediscount&token='.Tools::getAdminToken('AdminDiscounts'.(int)(Tab::getIdFromClassName('AdminDiscounts')).(int)($cookie->id_employee)).'">'.$discount['name'].'</a></td>
					<td align="center">- '.Tools::displayPrice($discount['value_real'], $currency, false).'</td>
				</tr>';
				echo '
			</table>';
			}
				echo '<div style="float:left; margin-top:15px;">'.
				AdminCarts::lx('According to the group of this customer, prices are printed:').' <b>'.($order->getTaxCalculationMethod() == PS_TAX_EXC ? AdminCarts::lx('tax excluded.') : AdminCarts::lx('tax included.')).'</b><br /><br />
				</div></div>';
				
				if ($order->id) {
						echo '<div style="text-align:center;"><br /><br /><strong>Non &egrave; possibile modificare questo carrello in quanto &egrave; gi&agrave; stato trasformato in un ordine</div>';
				}
				
				else {
				
				
					$tokenCarts = Tools::getAdminToken('AdminCarts'.(int)(Tab::getIdFromClassName('AdminCarts')).(int)($cookie->id_employee));
					echo '<div style="text-align:center;"><br /><br /><a class="button" href="index.php?tab=AdminCarts&id_cart='.$cart->id.'&viewcart&editcart&token='.$tokenCarts.'#modifica-carrello">Modifica carrello</a></div>';
				}
				
				// Cancel product
				echo '
				
				

			</fieldset>
		<div class="clear" style="height:20px;">&nbsp;</div>';
		}
		
		else {
		
		

			
				$ctrl_creato_nuovo = Db::getInstance()->getValue("SELECT count(id_cart) FROM carrelli_creati WHERE id_cart = ".$_GET['id_cart']."");
				if($ctrl_creato_nuovo == 0) {
					
					$query="select c.* from ". _DB_PREFIX_."cart c where c.id_cart=".$_GET['id_cart'];

					$res=mysql_query($query) or die;
					if (mysql_num_rows($res)>0) {
						$cart=mysql_fetch_array($res);
					
					Db::getInstance()->executeS("INSERT INTO carrelli_creati (id_cart, id_carrier, id_lang, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, date_add, date_upd, id_employee) VALUES (
					'".$cart['id_cart']."', '".$cart['id_carrier']."', '".$cart['id_lang']."', '".$cart['id_address_delivery']."', '".$cart['id_address_invoice']."', '".$cart['id_currency']."', '".$cart['id_customer']."', '".$cart['id_guest']."', '".$cart['secure_key']."', '".$cart['recyclable']."','".$cart['gift']."','".$cart['gift_message']."','".$cart['date_add']."', '".$cart['date_upd']."', ".$cookie->id_employee.")");
					}
					
					$query2="select * from ". _DB_PREFIX_."cart_product cp where cp.id_cart=".$_GET['id_cart'];
					$res2=mysql_query($query2) or die;
					if (mysql_num_rows($res2)>0) {
						while($cp=mysql_fetch_array($res2, MYSQL_ASSOC)) {
						
							Db::getInstance()->executeS("INSERT INTO carrelli_creati_prodotti (id_cart, id_product, id_product_attribute, quantity, price, free, date_add) VALUES (
							'".$cp['id_cart']."', '".$cp['id_product']."', '".$cp['id_product_attribute']."', '".$cp['quantity']."', '".$cp['price']."', '".$cp['free']."', '".$cp['date_add']."')");
							
						}
					}
					
					
				}
				else {
				
				
				}
			
			
		
			?>
			
			<?php
			
			if(isset($_GET['notifica_inviata'])) {
				echo "<script type='text/javascript'>alert('Notifica correttamente inviata');</script>";
			}
		
			echo '
			<script type="text/javascript" src="../js/jquery/jquery.tablednd_0_5.js"></script>
				<script type="text/javascript">
				$(document).ready(function() {
					$("#tableProducts").tableDnD();
				});
				</script>
				
			<script type="text/javascript">
			
			function ristabilisciPrezzo(id_product) {
			
				var numprice = document.getElementById(\'product_price[\'+id_product+\']\').value;
				document.getElementById(\'sconto_extra[\'+id_product+\']\').value = 0;
				var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
				var unitario = parseFloat(document.getElementById(\'unitario[\'+id_product+\']\').value);
				document.getElementById(\'product_price[\'+id_product+\']\').value=unitario;
				price=unitario;
			
			}
			
			
			
			function calcolaPrezzoScontoExtra(id_product, tipo) {
			
				var numunitario = document.getElementById(\'unitario[\'+id_product+\']\').value;
				var unitario = parseFloat(numunitario.replace(/\s/g, "").replace(",", "."));
				var quantity = parseFloat(document.getElementById(\'product_quantity[\'+id_product+\']\').value);
				var numsconto_extra = document.getElementById(\'sconto_extra[\'+id_product+\']\').value;
				var numprice = document.getElementById(\'product_price[\'+id_product+\']\').value;	
				var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
				var sconto_extra = parseFloat(numsconto_extra.replace(/\s/g, "").replace(",", "."));
				var sc_qta_1 = parseFloat(document.getElementById(\'sc_qta_1[\'+id_product+\']\').value);
				var sc_qta_2 = parseFloat(document.getElementById(\'sc_qta_2[\'+id_product+\']\').value);
				var sc_qta_3 = parseFloat(document.getElementById(\'sc_qta_3[\'+id_product+\']\').value);
				var sc_riv_1 = parseFloat(document.getElementById(\'sc_riv_1[\'+id_product+\']\').value);
				var sc_riv_2 = parseFloat(document.getElementById(\'sc_riv_2[\'+id_product+\']\').value);
				var sc_qta_1_q = parseFloat(document.getElementById(\'sc_qta_1_q[\'+id_product+\']\').value);
				var sc_qta_2_q = parseFloat(document.getElementById(\'sc_qta_2_q[\'+id_product+\']\').value);
				var sc_qta_3_q = parseFloat(document.getElementById(\'sc_qta_3_q[\'+id_product+\']\').value);
				var sc_riv_1_q = parseFloat(document.getElementById(\'sc_riv_1_q[\'+id_product+\']\').value);
				var sc_riv_2_q = parseFloat(document.getElementById(\'sc_riv_2_q[\'+id_product+\']\').value);	
				
				if (document.getElementById(\'usa_sconti_quantita[\'+id_product+\']\').checked == true) {
				';	
					if ($customer->id_default_group == 3) {
					
						echo '
						if(tipo == "sconto") {
							var finito = sc_riv_1 - ((sconto_extra*sc_riv_1)/100);
						}
						else if (tipo == "inverso") {
							var finito = ((sc_riv_1 - price)/sc_riv1)*100;
						}
						
						';
						
					}
						
					else {
						
					echo '
					if(quantity < sc_qta_1_q && sc_qta_1_q != 0) {
						if(tipo == "sconto") {
							var finito = unitario - ((sconto_extra*unitario)/100);
						}
						else if (tipo == "inverso") {
							var finito = ((unitario - price)/unitario)*100;
						}
					}
						
					else if((quantity >= sc_qta_1_q && quantity < sc_qta_2_q) || (quantity >= sc_qta_1_q && isNaN(parseFloat(sc_qta_2_q)))) {
						if(tipo == "sconto") {
							var finito = sc_qta_1 - ((sconto_extra*sc_qta_1)/100);	
						}
						else if (tipo == "inverso") {
							var finito = ((sc_qta_1 - price)/sc_qta_1)*100;
						}
					}
						
					else if((quantity >= sc_qta_2_q && quantity < sc_qta_3_q) || (quantity >= sc_qta_2_q && isNaN(parseFloat(sc_qta_3_q == 0)))) {
						if(tipo == "sconto") {
							var finito = sc_qta_2 - ((sconto_extra*sc_qta_2)/100);
						}
						else if (tipo == "inverso") {
							var finito = ((sc_qta_2 - price)/sc_qta_2)*100;
						}
					}
						
					else if(quantity >= sc_qta_3_q && sc_qta_2_q != 0 && sc_qta_3_q != 0 && sc_qta_1_q != 0) {
						if(tipo == "sconto") {
							var finito = sc_qta_3 - ((sconto_extra*sc_qta_3)/100);					
						}
						else if (tipo == "inverso") {
							var finito = ((sc_qta_3 - price)/sc_qta_3)*100;
						}
						
					}	';
						
					} echo '				
				}
				else {
					if (tipo == "sconto") {
						var finito = unitario - ((sconto_extra*unitario)/100);
					}
					else if (tipo == "inverso") {
						var finito = ((unitario - price)/unitario)*100;
					}
				}
				
				if(finito < 0) {
					finito = 0;
				}
				finito = finito.toFixed(2);
				finito = finito.toString();				
				finito = finito.replace(".",",");
				
				if (tipo == "sconto") {
					document.getElementById(\'product_price[\'+id_product+\']\').value=finito;
				}
				else if (tipo == "inverso") {
					document.getElementById(\'sconto_extra[\'+id_product+\']\').value=finito;
				}
			}
			
			function calcolaImportoConSpedizione(id_metodo) {
			
				
				var metodo_price = parseFloat(document.getElementById(\'metodo[\'+id_metodo+\']\').getAttribute("rel"));
				
				var totale = 0;
				var arrayImporti = document.getElementsByClassName("importo");
					for (var i = 0; i < arrayImporti.length; ++i) {
						var item = parseFloat(arrayImporti[i].value);  
						totale += item;
					}
					
				totale = totale+metodo_price;
				totale = totale.toFixed(2);
					
				document.getElementById(\'totaleprodotti\').value=totale;
						
				document.getElementById(\'spantotaleprodotti\').innerHTML=totale.replace(".",",");
			
			}

			function calcolaImporto(id_product) {
					var numprice = document.getElementById(\'product_price[\'+id_product+\']\').value;
					var numunitario = document.getElementById(\'unitario[\'+id_product+\']\').value;
					var numacquisto = document.getElementById(\'wholesale_price[\'+id_product+\']\').value;
					
					var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
								
					var quantity = parseFloat(document.getElementById(\'product_quantity[\'+id_product+\']\').value);

					var unitario = parseFloat(numunitario.replace(/\s/g, "").replace(",", "."));
					var acquisto = parseFloat(numacquisto.replace(/\s/g, "").replace(",", "."));
					
					var totaleacquisto = acquisto * quantity;
					
					document.getElementById(\'totaleacquisto[\'+id_product+\']\').value = totaleacquisto;

					var totale = parseFloat(document.getElementById(\'totaleprodotti\').value);
							
					var sc_qta_1 = parseFloat(document.getElementById(\'sc_qta_1[\'+id_product+\']\').value);
					var sc_qta_2 = parseFloat(document.getElementById(\'sc_qta_2[\'+id_product+\']\').value);
					var sc_qta_3 = parseFloat(document.getElementById(\'sc_qta_3[\'+id_product+\']\').value);
					var sc_riv_1 = parseFloat(document.getElementById(\'sc_riv_1[\'+id_product+\']\').value);
					var sc_riv_2 = parseFloat(document.getElementById(\'sc_riv_2[\'+id_product+\']\').value);
									
					var sc_qta_1_q = parseFloat(document.getElementById(\'sc_qta_1_q[\'+id_product+\']\').value);
					var sc_qta_2_q = parseFloat(document.getElementById(\'sc_qta_2_q[\'+id_product+\']\').value);
					var sc_qta_3_q = parseFloat(document.getElementById(\'sc_qta_3_q[\'+id_product+\']\').value);
					var sc_riv_1_q = parseFloat(document.getElementById(\'sc_riv_1_q[\'+id_product+\']\').value);
					var sc_riv_2_q = parseFloat(document.getElementById(\'sc_riv_2_q[\'+id_product+\']\').value);
					
					
					var importo = parseFloat(price*quantity);
					
					if (document.getElementById(\'usa_sconti_quantita[\'+id_product+\']\').checked == true) {
					
					
						';
						if ($customer->id_default_group == 3) {
						
						echo '
							if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value > 0) {
							} else {
								var prezzounitarionew = unitario;
								var prezziconfronto = new Array(sc_riv_1, sc_riv_2, sc_qta_1, sc_qta_2, sc_qta_3);
								for (var i = 0; i < prezziconfronto.length; ++i) {
									if(prezziconfronto[i] !=0 && prezziconfronto[i] < prezzounitarionew) { 
										prezzounitarionew = prezziconfronto[i];
									} else { }
								}
													
								var importo = parseFloat(prezzounitarionew*quantity);
								document.getElementById(\'product_price[\'+id_product+\']\').value=prezzounitarionew.toFixed(2);					
							}
						';
						
						}
						
						else {
						
						echo '
						if(quantity < sc_qta_1_q && sc_qta_1_q != 0) {
							
							if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value > 0) {
							} else {
								document.getElementById(\'product_price[\'+id_product+\']\').value=unitario.toFixed(2);;	
								var importo = parseFloat(price*quantity);
							}		
						}
						
						else if((quantity >= sc_qta_1_q && quantity < sc_qta_2_q) || (quantity >= sc_qta_1_q && isNaN(parseFloat(sc_qta_2_q)))) {
							if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value > 0) {
							} else {
								var importo = parseFloat(sc_qta_1*quantity);
								document.getElementById(\'product_price[\'+id_product+\']\').value=sc_qta_1.toFixed(2);;		
							}
						}
						
						else if((quantity >= sc_qta_2_q && quantity < sc_qta_3_q) || (quantity >= sc_qta_2_q && isNaN(parseFloat(sc_qta_3_q == 0)))) {
							if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value > 0) {
							} else {
								var importo = parseFloat(sc_qta_2*quantity);	
								document.getElementById(\'product_price[\'+id_product+\']\').value=sc_qta_2.toFixed(2);;	
							}
						}
						
						else if(quantity >= sc_qta_3_q && sc_qta_2_q != 0 && sc_qta_3_q != 0 && sc_qta_1_q != 0) {
							if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value > 0) {
							} else {
								var importo = parseFloat(sc_qta_3*quantity);	
								document.getElementById(\'product_price[\'+id_product+\']\').value=sc_qta_3.toFixed(2);;
							}
						}';
						
						} echo '
					}
					
					else if (document.getElementById(\'usa_sconti_quantita[\'+id_product+\']\').checked == false) {
						
						var importo = parseFloat(price*quantity);
					
					}
					document.getElementById(\'product_price[\'+id_product+\']\').value.replace(".",",");
					
					totale = 0;
					
					
					
					document.getElementById(\'impImporto[\'+id_product+\']\').value = importo;
					
					var arrayImporti = document.getElementsByClassName("importo");
					for (var i = 0; i < arrayImporti.length; ++i) {
						var item = parseFloat(arrayImporti[i].value);  
						totale += item;
					}
					
					var totaleacquisti = 0;
					
					var arrayAcquisti = document.getElementsByClassName("prezzoacquisto");
					for (var i = 0; i < arrayAcquisti.length; ++i) {
						arrayAcquisti[i].value = arrayAcquisti[i].value.replace(",",".");
						var item = parseFloat(arrayAcquisti[i].value); 					
						totaleacquisti += item;
					}
					
					
								
					var marginalitatotale = (((totale - totaleacquisti)*100) / totale);
		
					marginalitatotale = marginalitatotale.toFixed(2);
		
					document.getElementById(\'marginalitatotale\').innerHTML=marginalitatotale.replace(".",",") + "%";
					
					var guadagnototale = totale - totaleacquisti;
					
					guadagnototale = guadagnototale.toFixed(2);
					
					document.getElementById(\'guadagnototale\').innerHTML=guadagnototale.replace(".",",");
					
					importo = importo.toFixed(2);
					importo = importo.replace(".",",");
					
					
					document.getElementById(\'valoreImporto[\'+id_product+\']\').innerHTML=importo;
					
					for (var i = 0; i < document.forms.products.elements.metodo.length; i++) {
						var button = document.forms.products.elements.metodo[i];
						
						if (button.checked) {
							
							totale += parseFloat(button.getAttribute("rel"));
						}
						else {
						
						}
					}';
					
							if($customer->id_default_group == 1) {
				
					echo '	
						if(totale < 399) {
							var supplemento_contrassegno = 3.5;
						}
						else {
							var supplemento_contrassegno = 0;
							
						}';
					}
						
					else if($customer->id_default_group  == 3) {
							
						echo 'if(totale < 516) {
							var supplemento_contrassegno = 5;
						}
						else {
							var supplemento_contrassegno = ((totale/100)*1.5);
						
						}';
							
					}
						
					else {
						echo 'if(totale < 516) {
							var supplemento_contrassegno = 5;
						}
						else {
							var supplemento_contrassegno = ((totale/100)*1.5);
							
						}';
					
					}
					
					echo '
					
					supplemento_contrassegno = supplemento_contrassegno.toFixed(2);
									
					document.getElementById(\'supplemento_contrassegno\').innerHTML=supplemento_contrassegno.replace(".",",");				
					
					totale = totale.toFixed(2);
				
					document.getElementById(\'totaleprodotti\').value=totale;
					
					document.getElementById(\'spantotaleprodotti\').innerHTML=totale.replace(".",",");
					
					numprice = document.getElementById(\'product_price[\'+id_product+\']\').value;
					
					price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
					
					var marginalita = (((price - acquisto)*100) / price);
					
					marginalita = marginalita.toFixed(2);
				
					document.getElementById(\'spanmarginalita[\'+id_product+\']\').innerHTML=marginalita.replace(".",",")+"%";
					
			}
			
			function togliImporto(id_product) {
			
				var daTogliere = parseFloat(document.getElementById(\'impImporto[\'+id_product+\']\').value);
				
				
				var totale = parseFloat(document.getElementById(\'totaleprodotti\').value);
				
				';
				
							if($customer->id_default_group == 1) {
				
					echo '	
						if(totale < 399) {
							var supplemento_contrassegno = 3.5;
						}
						else {
							var supplemento_contrassegno = 0;
							
						}';
					}
						
					else if($customer->id_default_group  == 3) {
							
						echo 'if(totale < 516) {
							var supplemento_contrassegno = 5;
						}
						else {
							var supplemento_contrassegno = ((totale/100)*1.5);
						
						}';
							
					}
						
					else {
						echo 'if(totale < 516) {
							var supplemento_contrassegno = 5;
						}
						else {
							var supplemento_contrassegno = ((totale/100)*1.5);
							
						}';
					
					}
					
					echo '
					
					var numacquisto = document.getElementById(\'wholesale_price[\'+id_product+\']\').value;
			
					var quantity = parseFloat(document.getElementById(\'product_quantity[\'+id_product+\']\').value);

					var acquisto = parseFloat(numacquisto.replace(/\s/g, "").replace(",", "."));
					
					supplemento_contrassegno = supplemento_contrassegno.toFixed(2);
									
					document.getElementById(\'supplemento_contrassegno\').innerHTML=supplemento_contrassegno.replace(".",",");
					
					var totaleacquisti = 0;
					
					var arrayAcquisti = document.getElementsByClassName("prezzoacquisto");
					for (var i = 0; i < arrayAcquisti.length; ++i) {
						arrayAcquisti[i].value = arrayAcquisti[i].value.replace(",",".");
						var item = parseFloat(arrayAcquisti[i].value); 					
						totaleacquisti += item;
					}
					
					var togliAcquisto = (acquisto*quantity);
					
					totaleacquisti = totaleacquisti-togliAcquisto;
					
					totale = totale-daTogliere;
								
					var marginalitatotale = (((totale - totaleacquisti)*100) / totale);
		
					marginalitatotale = marginalitatotale.toFixed(2);
		
					document.getElementById(\'marginalitatotale\').innerHTML=marginalitatotale.replace(".",",") + "%";
					
					var guadagnototale = totale - totaleacquisti;
					
					guadagnototale = guadagnototale.toFixed(2);
					
					
					
					
					document.getElementById(\'guadagnototale\').innerHTML=guadagnototale.replace(".",",");
				
				totale = totale.toFixed(2);
				
				document.getElementById(\'totaleprodotti\').value=totale;
					
				document.getElementById(\'spantotaleprodotti\').innerHTML=totale.replace(".",",");
				
				
				
			}
			
			function delProduct30(id)
				{
					document.getElementById(\'tr_\'+id).innerHTML = "";
					$.ajax({
					  url:"ajax_products_list2.php?cancellaprodotto=y",
					  type: "POST",
					  data: { id_product: id,
					  id_cart: '.$_GET['id_cart'].'
					  },
					  success:function(){
						 
					  },
					  error: function(xhr,stato,errori){
						 alert("Errore nella cancellazione:"+xhr.status);
					  }
					});
					
					
				}
			
			</script>

';
$excludeIds = "";
	$array_prodotti_da_escludere = Db::getInstance()->executeS("select id_product FROM cart_product WHERE id_cart=".$_GET['id_cart']."");
			foreach($array_prodotti_da_escludere as $escl) {
			
				$excludeIds .= $escl['id_product'].",";
			
			}
echo '<input id="excludeIds" type="hidden" value="'.$excludeIds.'" />';			
			
 echo '
 	<script type="text/javascript" src="../themes/ezdirect/js/jquery.tooltip.js"></script>
			<style type="text/css">
			#tooltip {
				position: absolute;
				z-index: 3000;
				width:350px;
				border: 1px solid #111;
				background-color: #eee;
				padding: 5px;
				text-align:left;
			}
			#tooltip h3, #tooltip div { text-align:left; }
			</style>
			
			<script type="text/javascript">
				$(function() {
					$(".span-reference").tooltip({ showURL: false  });
				});
			</script>	
 
 
 <script type="text/javascript">
 var prodotti_nel_carrello = ['.$excludeIds.'0];
 
				function addProduct_TR(event, data, formatted)
				{
					
					var productId = data[1];
					var productPrice = data[2];
					var productScQta = data[3];
					var productRef = data[4];
					var productName = data[5];
					var productQuantity = data[16];
					var tdImporto = data[17];
					var przUnitario = data[18];
					var deleteProd = data[19];
					var marg = data[20];
					var scontoExtra = data[21];
					var sc_qta_1 = data[6];
					var sc_qta_2 = data[7];
					var sc_qta_3 = data[8];
					var sc_riv_1 = data[9];
					var sc_riv_2 = data[10];
					var sc_qta_1_q = data[11];	
					var sc_qta_2_q = data[12];	
					var sc_qta_3_q = data[13];	
					var sc_riv_1_q = data[14];	
					var sc_riv_2_q = data[15];	
					prodotti_nel_carrello.push(parseInt(productId));
				
					$("<tr id=\'tr_"+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"\'><td> " + productId + " </td><td> " + productRef + " </td><td><input name=\'nuovo_name["+ productId + "]\' size=\'55\' type=\'text\' value=\'"+ productName +"\' /></td><td> " + productQuantity + "</td><td>" + productPrice + "</td><td>" + scontoExtra + "</td>" + marg + "<td style=\'text-align:center\'> " + productScQta + "</td>" + tdImporto + "<td class=\'pointer dragHandle center\' style=\'background:url(../img/admin/upanddown.gif) no-repeat center;\'><input type=\'hidden\' name=\'sort_order["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' /></td><td style=\'text-align:center\'>" + deleteProd + "<input name=\'nuovo_prodotto["+ productId +"]\' type=\'hidden\' value=\'" + productId + "\' />" + sc_qta_1 + " " + sc_qta_2 + " " + sc_qta_3 + " " + sc_riv_1 + " " + sc_riv_2 + " " + sc_qta_1_q + " " + sc_qta_2_q + " " + sc_qta_3_q + " " + sc_riv_1_q + " " + sc_riv_2_q + " " + przUnitario + "</td></tr>").appendTo("#tableProductsBody");
					
					
					'; 
					//CONTROLLO PER COUPON CHEAPNET
								$conto_iniziale = Db::getInstance()->getValue("SELECT COUNT(usato) FROM cheapnetcoupons WHERE usato = 0");
								if($conto_iniziale == 0) {
								}
								else {
									$prodotti_con_coupon = Db::getInstance()->getValue("SELECT value FROM configuration WHERE name = 'PS_CHEAPNET_COUPONS'");
					
									$prodotti = explode("-", $prodotti_con_coupon);

									$prodotti_da_6 = str_replace(";",",",$prodotti[0]);
									$prodotti_da_30 = str_replace(";",",",$prodotti[1]);
									
									$conto_iniziale_6 = Db::getInstance()->getValue("SELECT COUNT(usato) FROM cheapnetcoupons WHERE tipo = 6 AND usato = 0");
									if($conto_iniziale_6 == 0) {
									}
									else {
										echo 'var prodotti_da_6 = ['.$prodotti_da_6.'0]; ';
									}
									
									$conto_iniziale_30 = Db::getInstance()->getValue("SELECT COUNT(usato) FROM cheapnetcoupons WHERE tipo = 30 AND usato = 0");
									if($conto_iniziale_30 == 0) {
										echo 'var prodotti_da_30 = [0]; ';
									}
									else {
										echo 'var prodotti_da_30 = ['.$prodotti_da_30.'0]; ';
									}
								}
								// FINE CONTROLLO PER COUPON CHEAPNET
					
					 echo '
						
						
						if($.inArray(parseInt(productId),prodotti_da_6) > -1){
							if($.inArray(31110,prodotti_nel_carrello) == -1){
								$(\'<tr style="cursor: move;" id="tr_31110">  <td>31110</td>  <td>CHEAP-300-O</td>  <td><input size="55" name="nuovo_name[31110]" value="OMAGGIO Traffico telefonico VoIP 300 minuti" type="text"></td>  <td><input style="text-align:right" name="nuovo_quantity[31110]" id="product_quantity[31110]" size="1" value="1" onkeyup="if (isArrowKey(event)) return; calcolaImporto(31110);" type="text"></td>  <td><input size="7" style="text-align:right" onkeyup="calcolaImporto(31110);" name="nuovo_price[31110]" id="product_price[31110]" value="0,00" type="text"></td>  <td></td>  <td style="text-align:right"></td>  <td style="text-align:center"><input id="usa_sconti_quantita[31110]" name="usa_sconti_quantita[31110]" value="0" type="hidden"> <input id="impImporto[31110]" class="importo" value="0" type="hidden"></td>  <td style="text-align:right" id="valoreImporto[31110]">0,00</td><td class="pointer dragHandle center" style="background:url(../img/admin/upanddown.gif) no-repeat center;"><input name="sort_order[31110]" type="hidden"></td>  <td style="text-align:center"><a style="cursor:pointer" onclick="togliImporto(31110); delProduct30(31110);"><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a></td><input name="nuovo_prodotto[31110]" value="31110" type="hidden"><input name="product_id[31110]" value="31110" type="hidden"><input id="wholesale_price[31110]" value="0.000000" type="hidden"><input id="unitario[31110]" value="0.000000" type="hidden"><input id="sc_qta_1[31110]" value="0" type="hidden"><input id="sc_qta_2[31110]" value="0" type="hidden"><input id="sc_qta_3[31110]" value="0" type="hidden"><input id="sc_riv_1[31110]" value="0" type="hidden"><input id="sc_riv_2[31110]" value="0" type="hidden"><input id="sc_qta_1_q[31110]" value="" type="hidden"><input id="sc_qta_2_q[31110]" value="" type="hidden"><input id="sc_qta_3_q[31110]" value="" type="hidden"><input id="sc_riv_1_q[31110]" value="" type="hidden"><input id="sc_riv_2_q[31110]" value="" type="hidden"><input id="oldQuantity[31110]" value="1" type="hidden"><input id="oldPrice[31110]" value="0.000000" type="hidden"></tr>\').appendTo("#tableProductsBody");
								prodotti_nel_carrello.push(31110);
							} else { }
						} 
						else if($.inArray(parseInt(productId),prodotti_da_30) > -1){
							if($.inArray(31109,prodotti_nel_carrello) == -1){
								$(\'<tr style="cursor: move;" id="tr_31109">  <td>31109</td>  <td>CHEAP-1500-O</td>  <td><input size="55" name="nuovo_name[31109]" value="OMAGGIO Traffico telefonico VoIP 1500 minuti" type="text"></td>  <td><input style="text-align:right" name="nuovo_quantity[31109]" id="product_quantity[31109]" size="1" value="1" onkeyup="if (isArrowKey(event)) return; calcolaImporto(31109);" type="text"></td>  <td><input size="7" style="text-align:right" onkeyup="calcolaImporto(31109);" name="nuovo_price[31109]" id="product_price[31109]" value="0,00" type="text"></td>  <td></td>  <td style="text-align:right"></td>  <td style="text-align:center"><input id="usa_sconti_quantita[31109]" name="usa_sconti_quantita[31109]" value="0" type="hidden"> <input id="impImporto[31109]" class="importo" value="0" type="hidden"></td>  <td style="text-align:right" id="valoreImporto[31109]">0,00</td><td class="pointer dragHandle center" style="background:url(../img/admin/upanddown.gif) no-repeat center;"><input name="sort_order[31109]" type="hidden"></td>  <td style="text-align:center"><a style="cursor:pointer" onclick="togliImporto(31109); delProduct30(31109);"><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a></td><input name="nuovo_prodotto[31109]" value="31109" type="hidden"><input name="product_id[31109]" value="31109" type="hidden"><input id="wholesale_price[31109]" value="0.000000" type="hidden"><input id="unitario[31109]" value="0.000000" type="hidden"><input id="sc_qta_1[31109]" value="0" type="hidden"><input id="sc_qta_2[31109]" value="0" type="hidden"><input id="sc_qta_3[31109]" value="0" type="hidden"><input id="sc_riv_1[31109]" value="0" type="hidden"><input id="sc_riv_2[31109]" value="0" type="hidden"><input id="sc_qta_1_q[31109]" value="" type="hidden"><input id="sc_qta_2_q[31109]" value="" type="hidden"><input id="sc_qta_3_q[31109]" value="" type="hidden"><input id="sc_riv_1_q[31109]" value="" type="hidden"><input id="sc_riv_2_q[31109]" value="" type="hidden"><input id="oldQuantity[31109]" value="1" type="hidden"><input id="oldPrice[31109]" value="0.000000" type="hidden"></tr>\').appendTo("#tableProductsBody");
								prodotti_nel_carrello.push(31109);
							}
						}
						else {
						}
					
					  
					$("#tableProducts").tableDnD();
					
					
			
					
					productId = productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "");
					
					$("#span-reference-"+productId+"").tooltip({ showURL: false  });
					
					calcolaImporto(productId);
				
					var excludeIds = document.getElementById("excludeIds").value;
					excludeIds = excludeIds+productId+",";
					document.getElementById("excludeIds").value = excludeIds;
				}

				
			</script>
			
			
			
			
			
			
			
			';
			
			
		
			
			
			echo '
			
			
			<input name="id_cart" type="hidden" value="'.$_GET['id_cart'].'" />
			Seleziona il prodotto da aggiungere: <input size="123" type="text" value="" id="product_autocomplete_input" /><br /><br />
			<script type="text/javascript">
					var formProduct = "";
					var products = new Array();
				</script>
				<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete.js"></script>
				
				
				<script type="text/javascript">
					urlToCall = null;
					
					/* function autocomplete */
					$(function() {
						$(\'#product_autocomplete_input\')
							.autocomplete(\'ajax_products_list2.php?id_cart='.$_GET['id_cart'].'\', {
								minChars: 1,
								autoFill: true,
								max:50,
								matchContains: true,
								mustMatch:true,
								scroll:true,
								cacheLength:0,
								formatItem: function(item) {
												return item[1]+\' - \'+item[0];
											}	
								
							}).result(addProduct_TR);
				
					});
					
					$("#product_autocomplete_input").keypress(function(event){
					
								  var keycode = (event.keyCode ? event.keyCode : event.which);
								  if (keycode == "13") {
									event.preventDefault();
									event.stopPropagation();    
								  }
								});
				</script>
		
				
				
			<table width="100%" class="table" id="tableProducts">
			<thead>
			  <tr>
				<th style="width:45px">ID</th>
				<th style="width:130px">Codice articolo</th>
				<th style="width:330px">Nome prodotto</th>
				<th style="width:45px">Qta</th>
				<th style="width:75px">Unitario</th>
				
				
				<th style="width:45px; text-align:center">Sconto<br />extra</th>
				<th style="width:55px">Marg</th>
				<th style="width:55px; text-align:center">'.($customer->id_default_group == 3 ? 'Sc. riv?' : 'Sc. qta?').'</th>
				<th style="width:85px">Importo</th>
				<th style="width:55px">Ordina</th>
				<th style="width:55px">Canc.</th>
			  </tr>
			  </thead>
			  <tbody id="tableProductsBody">';
			 
			$query="select cp.*,t.rate as tax_rate,p.quantity as stock,p.id_tax_rules_group,p.price as product_price,p.listino as listino,p.wholesale_price as acquisto,p.sconto_acquisto_1 as sc_acq_1, p.sconto_acquisto_2 as sc_acq_2, p.sconto_acquisto_3 as sc_acq_3, p.quantity as qt_tot, p.stock_quantity as magazzino, p.supplier_quantity as allnet, p.ordinato_quantity as qt_ordinato, p.reference as product_reference,pl.name as product_name from ". _DB_PREFIX_."cart_product cp left join ". _DB_PREFIX_."product p on  cp.id_product=p.id_product left join ". _DB_PREFIX_."product_lang pl  on  cp.id_product=pl.id_product 
			LEFT JOIN `"._DB_PREFIX_."tax_rule` tr ON (p.`id_tax_rules_group` = tr.`id_tax_rules_group`
			AND tr.`id_country` = ".(int)Country::getDefaultCountryId()."
			AND tr.`id_state` = 0)
			LEFT JOIN `"._DB_PREFIX_."tax` t ON (t.`id_tax` = tr.`id_tax`)
			
			WHERE pl.id_lang = 5 
			AND id_cart=".$_GET['id_cart']." ORDER BY cp.sort_order ASC";
			 $res1=mysql_query($query);
			if (mysql_num_rows($res1)>0) {
			$totale = 0;
				while ($products=mysql_fetch_array($res1)) {
					$tax_rate=$products['tax_rate'];
						$sc_qta_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_1' AND id_product = '".$products['id_product']."'");
						$sc_qta_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_2' AND id_product = '".$products['id_product']."'");
						$sc_qta_3_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_3' AND id_product = '".$products['id_product']."'");	
						$sc_riv_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_1' AND id_product = '".$products['id_product']."'");	
						$sc_riv_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_2' AND id_product = '".$products['id_product']."'");
						
						$sc_qta_1 = $sc_qta_1_r['price'] - ($sc_qta_1_r['price'] * $sc_qta_1_r['reduction']);
						$sc_qta_2 = $sc_qta_2_r['price'] - ($sc_qta_2_r['price'] * $sc_qta_2_r['reduction']);
						$sc_qta_3 = $sc_qta_3_r['price'] - ($sc_qta_3_r['price'] * $sc_qta_3_r['reduction']);
						$sc_riv_1 = $sc_riv_1_r['price'] - ($sc_riv_1_r['price'] * $sc_riv_1_r['reduction']);
						$sc_riv_2 = $sc_riv_2_r['price'] - ($sc_riv_2_r['price'] * $sc_riv_2_r['reduction']);
						
						$unitario = Db::getInstance()->getValue("SELECT price FROM product WHERE id_product = '".$products['id_product']."'");
						
						$sc_qta_1_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_1' AND id_product = '".$products['id_product']."'");
						$sc_qta_2_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_2' AND id_product = '".$products['id_product']."'");
						$sc_qta_3_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_3' AND id_product = '".$products['id_product']."'");
						
						$sc_riv_1_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_1' AND id_product = '".$products['id_product']."'");
						$sc_riv_2_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_2' AND id_product = '".$products['id_product']."'");
						 
						

						if($customer->id_default_group == 3) {
							$ctrl_sc_qt = Db::getInstance()->getValue("SELECT count(id_product) FROM specific_price WHERE id_product = ".$products['id_product']." AND (specific_price_name = 'sc_riv_1' OR specific_price_name = 'sc_riv_2')");
							
							if($ctrl_sc_qt > 0) {
								$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['id_product'].']" name="usa_sconti_quantita['.$products['id_product'].']" type="checkbox" '.($products['sc_qta'] == 0 ? '' : 'checked="checked"').' onclick="ristabilisciPrezzo('.$products['id_product'].'); calcolaImporto('.$products['id_product'].'); " />';
							}
							else {
								$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['id_product'].']" name="usa_sconti_quantita['.$products['id_product'].']" type="hidden" value="0" />';
							}
						}
						
						else {
							$ctrl_sc_qt = Db::getInstance()->getValue("SELECT count(id_product) FROM specific_price WHERE id_product = ".$products['id_product']." AND (specific_price_name = 'sc_qta_1' OR specific_price_name = 'sc_qta_2' OR specific_price_name = 'sc_qta_3')");
							
							if($ctrl_sc_qt > 0) {
								$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['id_product'].']" name="usa_sconti_quantita['.$products['id_product'].']" type="checkbox" '.($products['sc_qta'] == 0 ? '' : 'checked="checked"').' onclick="ristabilisciPrezzo('.$products['id_product'].'); calcolaImporto('.$products['id_product'].'); " />';
							}
							else {
								$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['id_product'].']" name="usa_sconti_quantita['.$products['id_product'].']" type="hidden" value="0" />';
							}
						}
					  
					  
						
					  echo '<tr id="tr_'.$products['id_product'].'">';
					  echo '  <td>'.$products['id_product'].' <script type="text/javascript">calcolaImporto('.$products['id_product'].');</script></td>';
					  echo '  <td><span style="cursor:pointer" class="span-reference" title="<strong>Qta disponibile ALLNET</strong>: '.$products['allnet'].' pz.
					  <br />
					  <strong>Qta disponibile MAGAZZINO</strong>: '.$products['magazzino'].' pz.<br />
					  <strong>Qta disponibile TOTALE</strong>: '.$products['qt_tot'].' pz. <br />
					  <strong>Qta ORDINATO</strong>: '.$products['qt_ordinato'].' pz. 
					  ">'.$products['product_reference'].'</span></td>';
					  echo '  <td><input size="55" name="product_name['.$products['id_product'].']" type="text" value="'.(empty($products['name']) ? $products['product_name'] : $products['name']).'" /></td>';
					  
					    echo '  <td><input style="text-align:right" name="product_quantity['.$products['id_product'].']" id="product_quantity['.$products['id_product'].']" type="text" size="1" value="'.$products['quantity'].'" onkeyup="if (isArrowKey(event)) return; calcolaImporto('.$products['id_product'].');" /></td>';
						
					  echo '  <td><input size="7" style="text-align:right" onkeyup="javascript:document.getElementById(\'usa_sconti_quantita['.$products['id_product'].']\').checked = false; calcolaPrezzoScontoExtra('.$products['id_product'].', \'inverso\'); calcolaImporto('.$products['id_product'].');" name="product_price['.$products['id_product'].']" id="product_price['.trim($products['id_product']).']" type="text" value="'.
					  ($products['price'] == 0 && $products['free'] == 0 ? number_format(($customer->id_default_group == 3 ? $sc_riv_1 : $products['product_price']), 2, ',', '') : number_format($products['price'], 2, ',', ''))
					 
					  .'" /></td>';
					  /*echo '  <td>'.$products['tax_rate'].'%</td>';
					  echo '  <td>'.number_format($products['product_price']*(1+$products['tax_rate']/100),3, ',', '').'</td>';  */
					
					  
					 
					   echo '  <td style="text-align:right">';
					  
					   
					   echo '<input style="text-align:right" name="sconto_extra['.$products['id_product'].']" id="sconto_extra['.$products['id_product'].']" type="text" size="2" value="'.number_format($products['sconto_extra'],2,",","").'" onkeyup="if (isArrowKey(event)) return; calcolaPrezzoScontoExtra('.$products['id_product'].', \'sconto\'); calcolaImporto('.$products['id_product'].');" />
					   </td>';
					    echo '  <td style="text-align:right">
						<input type="hidden" name="sconto_acquisto_1['.$products['id_product'].']" id="sconto_acquisto_1['.$products['id_product'].']" value="'.$products['sc_acq_1'].'" />
						<input type="hidden" name="sconto_acquisto_2['.$products['id_product'].']" id="sconto_acquisto_2['.$products['id_product'].']" value="'.$products['sc_acq_2'].'" />
						<input type="hidden" name="sconto_acquisto_3['.$products['id_product'].']" id="sconto_acquisto_3['.$products['id_product'].']" value="'.$products['sc_acq_3'].'" />
						';
						$wholesale_price = ($products['acquisto'] > 0 ? $products['acquisto'] : ($products['listino']*(100-$products['sc_acq_1'])/100)*((100-$products['sc_acq_2'])/100)*((100-$products['sc_acq_3'])/100));
						echo '<input type="hidden" name="wholesale_price['.$products['id_product'].']" id="wholesale_price['.$products['id_product'].']" value="'.number_format(round($wholesale_price,2), 2, ',', '').'" />
						
						<input type="hidden" class="prezzoacquisto" name="totaleacquisto['.$products['id_product'].']" id="totaleacquisto['.$products['id_product'].']" value="'.number_format(round($wholesale_price*$products['quantity'],2), 2, ',', '').'" />
						';
						
						$totaleacquisti += $wholesale_price*$products['quantity'];
						
						$prezzo_partenza = ($products['price'] == 0 && $products['free'] == 0 ? ($customer->id_default_group == 3 ? $sc_riv_1 : $products['product_price']) : $products['price']);
						
						$marginalita = ((($prezzo_partenza - $wholesale_price)*100)/$prezzo_partenza);
						
						echo '<span id="spanmarginalita['.$products['id_product'].']">'.number_format($marginalita, 2, ',', '').'%</span></td>';
					  echo '  <td style="text-align:center">'.$usa_sconti_quantita.' <input id="impImporto['.$products['id_product'].']" class="importo" type="hidden" value="'.($products['price'] == 0 && $products['free'] == 0 ? $products['product_price'] : $products['price'])*$products['quantity'].'" /></td>';
					  echo '  <td style="text-align:right" id="valoreImporto['.$products['id_product'].']">
					 
					  '.number_format(($products['price'] == 0 && $products['free'] == 0 ? $products['product_price'] : $products['price'])*$products['quantity'],2, ',', '').'
					  
					 
					  
					  </td>';  
					  
					   $totale += ($products['price'] == 0 && $products['free'] == 0 ? $products['product_price'] : $products['price'])*$products['quantity'];
					  /*echo '  <td>'.number_format($products['product_price']*$products['quantity']*(1+$products['tax_rate']/100),2, ',', '').'</td>';  */
					  
					  echo '<td class="pointer dragHandle center" style="background:url(../img/admin/upanddown.gif) no-repeat center;">
					  <input type="hidden" name="sort_order['.$products['id_product'].']" /></td>';
					  
					  echo '  <td style="text-align:center">
					  <a style="cursor:pointer" onclick="togliImporto('.$products['id_product'].'); delProduct30('.$products['id_product'].');"><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a>
					  </td>';
					  echo '  <input name="product_id['.$products['id_product'].']" type="hidden" value="'.$products['id_product'].'" />';
					  
						
						echo '<input type="hidden" id="unitario['.$products['id_product'].']" value="'.$unitario.'" />';
						echo '<input type="hidden" id="sc_qta_1['.$products['id_product'].']" value="'.$sc_qta_1.'" />';
						echo '<input type="hidden" id="sc_qta_2['.$products['id_product'].']" value="'.$sc_qta_2.'" />';
						echo '<input type="hidden" id="sc_qta_3['.$products['id_product'].']" value="'.$sc_qta_3.'" />';
						echo '<input type="hidden" id="sc_riv_1['.$products['id_product'].']" value="'.$sc_riv_1.'" />';
						echo '<input type="hidden" id="sc_riv_2['.$products['id_product'].']" value="'.$sc_riv_2.'" />';
		
						echo '<input type="hidden" id="sc_qta_1_q['.$products['id_product'].']" value="'.$sc_qta_1_q.'" />';
						echo '<input type="hidden" id="sc_qta_2_q['.$products['id_product'].']" value="'.$sc_qta_2_q.'" />';
						echo '<input type="hidden" id="sc_qta_3_q['.$products['id_product'].']" value="'.$sc_qta_3_q.'" />';
						echo '<input type="hidden" id="sc_riv_1_q['.$products['id_product'].']" value="'.$sc_riv_1_q.'" />';
						echo '<input type="hidden" id="sc_riv_2_q['.$products['id_product'].']" value="'.$sc_riv_2_q.'" />';
					    echo '<input type="hidden" id="oldQuantity['.$products['id_product'].']" value="'.$products['quantity'].'" />';
						echo '<input type="hidden" id="oldPrice['.trim($products['id_product']).']" value="'.
					  ($products['price'] == 0 && $products['free'] == 0 ? ($customer->id_default_group == 3 ? $sc_riv_1 : $products['product_price']) : $products['price']).'" />';
				   echo '</tr> ';
				}
			}
		
			$cart_ctrl = new Cart($_GET['id_cart']);
			$costo_spedizione = $cart_ctrl->getOrderShippingCost($carrier_cart, false);
			$marginalitatotale = ((($totale - $totaleacquisti)*100) / $totale);
			$guadagno = $totale - $totaleacquisti;
			
			
			$totale += $costo_spedizione; 
			
	/* Add TinyMCE */  
		        global $cookie;  
        $iso = Language::getIsoById((int)($cookie->id_lang));  
        $isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');  
        $ad = dirname($_SERVER["PHP_SELF"]);  
        echo '  
		</tbody>
		<tfoot>
		<tr><td style="width:45px"></td><td style="width:130px"></td><td style="width:330px">Guadagno totale in euro: <span id="guadagnototale">'.number_format($guadagno,2, ',', '').'</span> &euro;</td><td style="width:45px"></td><td style="width:75px"></td><td style="width:45px"></td><td style="width:55px"><span id="marginalitatotale">'.number_format($marginalitatotale,2, ',', '').'%</span></td><td style="width:55px"><strong>TOT</strong></td><td style="width:85px; text-align:right"><span id="spantotaleprodotti"> '.number_format($totale,2, ',', '').'</span></td><td style="width:55px"></td>
		<td style="width:55px"></td></tr>
		</tfoot>
		</table><table class="table">
		</table>';
		
		echo '
            <script type="text/javascript">  
            var iso = \''.$isoTinyMCE.'\' ;  
            var pathCSS = \''._THEME_CSS_DIR_.'\' ;  
            var ad = \''.$ad.'\' ;  
            </script>  
            <script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce/tiny_mce.js"></script>  
            <script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>';  
        /* End TinyMCE */ 
		
			$provincia_cliente = Db::getInstance()->getValue("SELECT id_state FROM address WHERE id_customer = ".$customer->id." AND active = 1 AND deleted = 0 AND fatturazione = 1");
			
			$nazione_cliente = Db::getInstance()->getValue("SELECT id_country FROM address WHERE id_customer = ".$customer->id." AND active = 1 AND deleted = 0 AND fatturazione = 1");
			
			if($provincia_cliente == 0) {
				$zona_cliente = Db::getInstance()->getValue("SELECT id_zone FROM country WHERE id_country = ".$nazione_cliente."");
			}
			else {
				$zona_cliente = Db::getInstance()->getValue("SELECT id_zone FROM state WHERE id_state = ".$nazione_cliente."");
			}
			
			$gruppi_per_spedizione = array();
			$gruppi_per_spedizione[] = $customer->id_default_group;
		
			$metodi_spedizione = AdminCarts::getCarriersForEditOrder($zona_cliente, $gruppi_per_spedizione, $_GET['id_cart']);
			$carrier_cart = Db::getInstance()->getValue("SELECT id_carrier FROM cart WHERE id_cart = ".$_GET['id_cart']."");
			
			if($payment != "" && $payment != "Bonifico" && $payment != "Carta" && $payment != "Paypal" && $payment != "Contrassegno" && $payment != "Bonifico 30 gg. fine mese" && $payment != "Bonifico 60 gg. fine mese" && $payment != "Bonifico 90 gg. fine mese") {
				$check_payment = 1;
			} else {
				$check_payment = 0;
			}
			
			echo "<br /><strong>Metodo di pagamento</strong>: ";
			echo "<select name='payment' id='payment'>
			<option value='' ".($payment == "" ? "selected='selected'" : "").">Nessun metodo - lascia scelta al cliente</option>
			<option value='Bonifico' ".($payment == "Bonifico" ? "selected='selected'" : "").">Bonifico</option>
			<option value='Carta' ".($payment == "Carta" ? "selected='selected'" : "").">Carta di credito (Gestpay)</option>
			<option value='Paypal' ".($payment == "Paypal" ? "selected='selected'" : "").">Paypal</option>
			<option value='Contrassegno' ".($payment == "Contrassegno" ? "selected='selected'" : "").">Contrassegno</option>
			<option value='Bonifico 30 gg. fine mese' ".($payment == "Bonifico 30 gg. fine mese" ? "selected='selected'" : "").">Bonifico 30 gg. fine mese</option>
			<option value='Bonifico 60 gg. fine mese' ".($payment == "Bonifico 60 gg. fine mese" ? "selected='selected'" : "").">Bonifico 60 gg. fine mese</option>
			<option value='Bonifico 90 gg. fine mese' ".($payment == "Bonifico 90 gg. fine mese" ? "selected='selected'" : "").">Bonifico 90 gg. fine mese</option>
			<option value='Altro' ".($check_payment == 1 ? "selected='selected'": "").">Altro</option>
			</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			echo "Altro metodo: <input type='text' name='altropagamento' id='altropagamento' value='".($check_payment == 1 ? $payment : "")."' /><br />";
			
			if($customer->id_default_group == 1) {
				
				if($totale < 399) {
					$supplemento_contrassegno = 3.5;
				}
				else {
					$supplemento_contrassegno = 0;
					
				}
			}
				
			else if($customer->id_default_group  == 3) {
					
				if($totale < 516) {
					$supplemento_contrassegno = 5;
				}
				else {
					$supplemento_contrassegno = (($totale/100)*1.5);
				
				}
					
			}
				
			else {
				if($totale < 516) {
					$supplemento_contrassegno = 5;
				}
				else {
					$supplemento_contrassegno = (($totale/100)*1.5);
					
				}
			
			}

			echo "Il pagamento in contrassegno prevede un supplemento di euro <span id='supplemento_contrassegno'>".number_format($supplemento_contrassegno,2,",","")."</span> iva esclusa
						
			<br /><br />
			";
			
			echo "<strong>Seleziona metodo di spedizione</strong> (se l'importo prevede consegna gratuita, il costo sar&agrave; ricalcolato automaticamente dopo aver fatto clic sul tasto Conferma):
			<table>
			<tr><td><input type='radio' name='metodo' id='metodo[0]' rel='0' value='0' onclick='javascript:calcolaImportoConSpedizione(0);' ".($carrier_cart == 0 ? 'checked="checked"' : '')." />Nessun metodo (il cliente sceglie da s&eacute; e gli saranno presentate le opzioni in automatico)</td></tr>
			";
			
			
		
			foreach ($metodi_spedizione as $metodo) {
			
				echo "<tr><td><input type='radio' id='metodo[".$metodo['id_carrier']."]' name='metodo' rel='".$metodo['price_tax_exc']."' value='".$metodo['id_carrier']."' ".($carrier_cart == $metodo['id_carrier'] ? "checked='checked'" : '')." onclick='javascript:calcolaImportoConSpedizione(".$metodo['id_carrier'].");' />".$metodo['name']." (".number_format($metodo['price_tax_exc'],2,",","")." &euro;)</td></tr>";
			
			}
			echo "</table>";
		
			
			echo '
			
		    <p align="center">
			<strong>Note</strong>: <textarea name="note" class="rte">'.$note.'</textarea><br />
			<input name="notifica_mail" id="notifica_mail" type="checkbox" />Spunta questo flag se vuoi inviare una notifica via mail al cliente <br /><br />
			  <input name="Apply" type="submit" class="button" value="Conferma" />
			  <input name="totaleprodotti" id="totaleprodotti" type="hidden" value="'.$totale.'" />
			  <input name="id_order" type="hidden" value="'. $_GET['id_order'] .'" />
			  <input name="tax_rate" type="hidden" value="'. $tax_rate .'" />
			  <input name="id_lang" type="hidden" value="'. $id_lang .'" />
			</p>';
			echo '</form></fieldset>
			<div class="clear" style="height:20px;">&nbsp;</div>';
	
		}
		
			if(isset($_GET['cartupdated'])) {
			
			$id_cart_g = htmlspecialchars($_GET['id_cart']);
			$action_g = ""; 
			$id_product_g = htmlspecialchars($_POST['id_product']);
			$id_lang_g = 5;


			if ($id_order_g) 
			$_POST['id_order']=$id_order_g;
			

			if ($_POST['id_cart']) {
				
				/*
					$query="update  ". _DB_PREFIX_."orders set ";
					$query.=" total_discounts=".$this->price($_POST['total_discounts']);
					$query.=" ,total_shipping=".$this->price($_POST['total_shipping']);
					$query.=" where id_order=".$_POST['id_order'];
					$query.=" limit 1";
					//echo $query;
					mysql_query($query) or die(mysql_error());
				*/
				

				if ($_POST['Apply']) {
					
					//delete product
					if ($_POST['product_delete']) {
						foreach ($_POST['product_delete'] as $id_product=>$value) {
							mysql_query("delete from ". _DB_PREFIX_."cart_product where id_product=".$id_product);
							mysql_query("delete from ". _DB_PREFIX_."carrelli_creati_prodotti where id_product=".$id_product);
						}
					}
					
					if(isset($_POST['notifica_mail'])) {
							
						$params = array('{msg}' => "Gentile cliente, <br /><br />
						Grazie per aver richiesto un'offerta tecnico-economica. 
						<br /><br />
						Per visualizzare e ordinare la nostra offerta, accedi al tuo profilo personale (\"area ordini\") e seleziona <a href='http://www.ezdirect.it/modules/mieofferte/offerte.php'>\"Le mie offerte\"</a>. Troverai uno o pi&ugrave; carrelli con il contenuto dettagliato.
						<br /><br />
						Il carrello predisposto non � un ordine, lo diventer&agrave; nel momento in cui deciderai di portare a termine il processo d'ordine seguendo le indicazioni che il carrello propone, alla stegua di un normalissimo ordine online. 
						<br /><br />
						Se hai bisogno di supporto contatta il nostro staff. Grazie per la tua attenzione!");
							
						Mail::Send(5, 'msg_base', Mail::l('La tua offerta su Ezdirect', 5), 
						$params, $customer->email, NULL, NULL, NULL, NULL, NULL, 
						_PS_MAIL_DIR_, true);					
					
					}
					
					if($_POST['altropagamento'] != '') {
						$pagamento = $_POST['altropagamento'];
					}
					else {
						$pagamento = $_POST['payment'];
					}
				
					mysql_query("update  ". _DB_PREFIX_."cart set date_upd = '".date("Y-m-d H:i:s")."', id_employee = ".$cookie->id_employee.", note='".$_POST['note']."', id_carrier='".$_POST['metodo']."', name='".$_POST['name']."', validita='".$_POST['validita']."',payment='".$pagamento."', consegna='".$_POST['consegna']."' where id_cart = ".$_POST['id_cart']);
					
					
					mysql_query("update  ". _DB_PREFIX_."carrelli_creati set date_upd = '".date("Y-m-d H:i:s")."', id_employee = ".$cookie->id_employee.", note='".$_POST['note']."', id_carrier='".$_POST['metodo']."', name='".$_POST['name']."', validita='".$_POST['validita']."',payment='".$pagamento."', consegna='".$_POST['consegna']."' where id_cart = ".$_POST['id_cart']);
					
					
					foreach ($_POST['nuovo_prodotto'] as $id_product_g=>$id_product) {
						
						if(isset($_POST['product_delete'][$id_product_g])) {
						}
						else {
							
						
							$price = str_replace(',', '.', $_POST['nuovo_price'][trim($id_product_g)]);
							$sconto_extra = str_replace(',', '.', $_POST['sconto_extra'][trim($id_product_g)]);	
	

							
							$query="insert into ". _DB_PREFIX_."cart_product (id_cart,id_product,id_product_attribute,quantity,price,free,name,sc_qta,sconto_extra,date_add) values  ";
							$query.="(".$_POST['id_cart'].",".$_POST['nuovo_prodotto'][$id_product_g].",'0','".$_POST['nuovo_quantity'][trim($id_product_g)]."',";
							$query.=$price.",'".($_POST['nuovo_price'][$id_product_g] == 0 ? '1' : '0')."','".$_POST['nuovo_name'][$id_product_g]."','".($_POST['usa_sconti_quantita'][$id_product_g] == 0 ? '1' : '0')."','".$sconto_extra."','".date("Y-m-d H:i:s")."')";
							
							$query2="insert into ". _DB_PREFIX_."carrelli_creati_prodotti (id_cart,id_product,id_product_attribute,quantity,price,free,name,sc_qta,sconto_extra,date_add) values  ";
							$query2.="(".$_POST['id_cart'].",".$_POST['nuovo_prodotto'][$id_product_g].",'0','".$_POST['nuovo_quantity'][trim($id_product_g)]."',";
							$query2.=$price.",'".($_POST['price'][$id_product_g] == 0 ? '1' : '0')."','".$_POST['nuovo_name'][$id_product_g]."','".($_POST['usa_sconti_quantita'][$id_product_g] == 0 ? '1' : '0')."','".$sconto_extra."','".date("Y-m-d H:i:s")."')";
							
							
							
							echo $query;
							mysql_query($query) or die(mysql_error());
							mysql_query($query2) or die(mysql_error());
						}
					}
					
					foreach ($_POST['product_name'] as $id_product=>$product_name) {
						$name = Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_lang = 5 AND id_product = ".$id_product."");
							
						if($name != $_POST['product_name'][$id_product]) {
							
								mysql_query("update  ". _DB_PREFIX_."cart_product set name='".addslashes($_POST['product_name'][$id_product])."' where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
								mysql_query("update  ". _DB_PREFIX_."carrelli_creati_prodotti set name='".addslashes($_POST['product_name'][$id_product])."' where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							} else { }
							
					}
					
					
					
					
					
					if ($_POST['product_price']) {
					
						foreach ($_POST['product_price'] as $id_product=>$price_product) {
						
							if(!empty($_POST['usa_sconti_quantita'][$id_product])) {
								$price_product = Product::getPriceStatic((int)$id_product, false, 0, 6, NULL, false, true, $_POST['product_quantity'][$id_product], false, (int)$customer->id ? (int)$customer->id : NULL, $_POST['id_cart']);
								mysql_query("update  ". _DB_PREFIX_."cart_product set sc_qta=1 where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							}
							
							else {
								$price_product = str_replace(',', '.', $_POST['product_price'][$id_product]);
								mysql_query("update  ". _DB_PREFIX_."cart_product set sc_qta=0 where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							}
							
							
							mysql_query("update  ". _DB_PREFIX_."cart_product set price=".$price_product." where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							mysql_query("update  ". _DB_PREFIX_."carrelli_creati_prodotti set price=".$price_product." where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							
							
							if($price_product == 0) {
							
								mysql_query("update  ". _DB_PREFIX_."cart_product set free=1 where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
								mysql_query("update  ". _DB_PREFIX_."carrelli_creati_prodotti set free=1 where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
								
							}
							
							
							
							
							
							mysql_query("update  ". _DB_PREFIX_."cart_product set quantity=".$_POST['product_quantity'][$id_product]." where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							mysql_query("update  ". _DB_PREFIX_."carrelli_creati_prodotti set quantity=".$_POST['product_quantity'][$id_product]." where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							
							$sconto_extra = str_replace(',', '.', $_POST['sconto_extra'][$id_product]);
							
							mysql_query("update  ". _DB_PREFIX_."cart_product set sconto_extra=".$sconto_extra." where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							mysql_query("update  ". _DB_PREFIX_."carrelli_creati_prodotti set sconto_extra=".$sconto_extra." where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
						}
						

						/*$total_products=$total_products*(1+$_POST['tax_rate']/100);
						mysql_query("update  ". _DB_PREFIX_."orders set total_products=".$total_products." where id_order=".$_POST['id_order']);
						$this->update_total($_POST['id_order']);*/

					}
					
					$i_ord = 0;
					foreach ($_POST['sort_order'] as $id_product_g=>$id_product) {
					
					
						mysql_query("update  ". _DB_PREFIX_."cart_product set sort_order=$i_ord where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product_g)."<br />";
						mysql_query("update  ". _DB_PREFIX_."carrelli_creati_prodotti set sort_order=$i_ord where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product_g);
						$i_ord++;
						
					
					}
					
				}


			
			} 
			// echo "alert('Are you sure you want to give us the deed to your house?')"
		
		
			Tools::redirectAdmin($currentIndex.'&id_cart='.$_POST['id_cart'].'&id_customer='.$customer->id.'&viewcustomer&conf=4&tab-container-1=4&viewcart'.'&token='.$this->token.(isset($_POST['notifica_mail']) ? '&notifica_inviata=y' : '') );
		
		
		
		}
		
		
	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

	private function displayCustomizedDatas(&$customizedDatas, &$product, &$currency, &$image, $tokenCatalog, &$stock)
	{
		if (!($order = AdminCarts::loadObject(true)))
			return;

		if (is_array($customizedDatas) AND isset($customizedDatas[(int)($product['id_product'])][(int)($product['id_product_attribute'])]))
		{
			if ($image = new Image($image['id_image']))
				echo '
					<tr>
						<td align="center">'.(isset($image->id_image) ? cacheImage(_PS_IMG_DIR_.'p/'.$image->getExistingImgPath().'.jpg',
						'product_mini_'.(int)($product['id_product']).(isset($product['id_product_attribute']) ? '_'.(int)($product['id_product_attribute']) : '').'.jpg', 45, 'jpg') : '--').'</td>
						<td><a href="index.php?tab=AdminCatalog&id_product='.$product['id_product'].'&updateproduct&token='.$tokenCatalog.'">
							<span class="productName">'.$product['name'].'</span>'.(isset($product['attributes']) ? '<br />'.$product['attributes'] : '').'<br />
							'.($product['reference'] ? AdminCarts::lx('Ref:').' '.$product['reference'] : '')
							.(($product['reference'] AND $product['supplier_reference']) ? ' / '.$product['supplier_reference'] : '')
							.'</a></td>
						<td align="center">'.Tools::displayPrice($product['price_wt'], $currency, false).'</td>
						<td align="center" class="productQuantity">'.$product['customizationQuantityTotal'].'</td>
						<td align="center" class="productQuantity">'.(int)($stock['quantity']).'</td>
						<td align="right">'.Tools::displayPrice($product['total_customization_wt'], $currency, false).'</td>
					</tr>';
			foreach ($customizedDatas[(int)($product['id_product'])][(int)($product['id_product_attribute'])] AS $customization)
			{
				echo '
				<tr>
					<td colspan="2">';
				foreach ($customization['datas'] AS $type => $datas)
					if ($type == _CUSTOMIZE_FILE_)
					{
						$i = 0;
						echo '<ul style="margin: 0; padding: 0; list-style-type: none;">';
						foreach ($datas AS $data)
							echo '<li style="display: inline; margin: 2px;">
									<a href="displayImage.php?img='.$data['value'].'&name='.(int)($order->id).'-file'.++$i.'" target="_blank"><img src="'._THEME_PROD_PIC_DIR_.$data['value'].'_small" alt="" /></a>
								</li>';
						echo '</ul>';
					}
					elseif ($type == _CUSTOMIZE_TEXTFIELD_)
					{
						$i = 0;
						echo '<ul style="margin-bottom: 4px; padding: 0; list-style-type: none;">';
						foreach ($datas AS $data)
							echo '<li>'.($data['name'] ? $data['name'] : AdminCarts::lx('Text #').++$i).AdminCarts::lx(':').' <b>'.$data['value'].'</b></li>';
						echo '</ul>';
					}
				echo '</td>
					<td align="center"></td>
					<td align="center" class="productQuantity">'.$customization['quantity'].'</td>
					<td align="center" class="productQuantity"></td>
					<td align="center"></td>
				</tr>';
			}
		}
	}

	public function display()
	{
		global $cookie;

		if (isset($_GET['view'.$this->table]))
			$this->viewDetails();
		else
		{
			$this->getList((int)($cookie->id_lang), !Tools::getValue($this->table.'Orderby') ? 'date_add' : NULL, !Tools::getValue($this->table.'Orderway') ? 'DESC' : NULL);
			$this->displayList();
		}
	}
	
	protected function _displayDeleteLink($token = NULL, $id)
	{
		global $currentIndex;
		
		foreach ($this->_list as $cart)
			if ($id == $cart['id_cart'])
				if ($cart['id_order'])
					return;
		
		$_cacheLang['Delete'] = AdminCarts::lx('Delete', __CLASS__, true, false);
		$_cacheLang['DeleteItem'] = AdminCarts::lx('Delete item #', __CLASS__, true, false).$id.' ?)';
		
		echo '
			<a href="'.$currentIndex.'&'.$this->identifier.'='.$id.'&delete'.$this->table.'&token='.($token != null ? $token : $this->token).'" onclick="return confirm(\''.$_cacheLang['DeleteItem'].'\');">
			<img src="../img/admin/delete.gif" alt="'.$_cacheLang['Delete'].'" title="'.$_cacheLang['Delete'].'" /></a>
		';
	}
	
	protected function lx($string, $class = 'AdminCarts', $addslashes = FALSE, $htmlentities = TRUE)
	{
		// if the class is extended by a module, use modules/[module_name]/xx.php lang file
		$currentClass = get_class($this);
		if (Module::getModuleNameFromClass($currentClass))
		{
			$string = str_replace('\'', '\\\'', $string);
			return Module::findTranslation(Module::$classInModule[$currentClass], $string, $currentClass);
		}
		global $_LANGADM;

		if ($class == __CLASS__)
				$class = 'AdminCarts';

		$key = md5(str_replace('\'', '\\\'', $string));
		$str = (key_exists(get_class($this).$key, $_LANGADM)) ? $_LANGADM[get_class($this).$key] : ((key_exists($class.$key, $_LANGADM)) ? $_LANGADM[$class.$key] : $string);
		$str = $htmlentities ? htmlentities($str, ENT_QUOTES, 'utf-8') : $str;
		return str_replace('"', '&quot;', ($addslashes ? addslashes($str) : stripslashes($str)));
	}
	
	protected function getCarriersForEditOrder($id_zone, $groups = null, $id_cart)
	{
		$cart = new Cart($id_cart);
		global $cookie; 
		
		if (is_array($groups) && !empty($groups))
			$result = Carrier::getCarriers((int)$cookie->id_lang, true, false, (int)$id_zone, $groups, Carrier::PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE);
		else
			$result = Carrier::getCarriers((int)$cookie->id_lang, true, false, (int)$id_zone, array(1), Carrier::PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE);
		$resultsArray = array();

		foreach ($result as $k => $row)
		{
			$carrier = new Carrier((int)$row['id_carrier']);
			$shippingMethod = $carrier->getShippingMethod();
			if ($shippingMethod != Carrier::SHIPPING_METHOD_FREE)
			{
				// Get only carriers that are compliant with shipping method
				if (($shippingMethod == Carrier::SHIPPING_METHOD_WEIGHT && $carrier->getMaxDeliveryPriceByWeight($id_zone) === false)
					|| ($shippingMethod == Carrier::SHIPPING_METHOD_PRICE && $carrier->getMaxDeliveryPriceByPrice($id_zone) === false))
				{
					unset($result[$k]);
					continue;
				}

				// If out-of-range behavior carrier is set on "Desactivate carrier"
				if ($row['range_behavior'])
				{
					// Get id zone
					if (!$id_zone)
						$id_zone = Country::getIdZone(Country::getDefaultCountryId());

					// Get only carriers that have a range compatible with cart
					if (($shippingMethod == Carrier::SHIPPING_METHOD_WEIGHT && (!Carrier::checkDeliveryPriceByWeight($row['id_carrier'], $cart->getTotalWeight(), $id_zone)))
						|| ($shippingMethod == Carrier::SHIPPING_METHOD_PRICE
							&& (!Carrier::checkDeliveryPriceByPrice($row['id_carrier'], $cart->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING), $id_zone, $cart->id_currency))))
					{
						unset($result[$k]);
						continue;
					}
				}
			}
			
			$row['name'] = (strval($row['name']) != '0' ? $row['name'] : Configuration::get('PS_SHOP_NAME'));
			$row['price'] = ($shippingMethod == Carrier::SHIPPING_METHOD_FREE ? 0 : $cart->getOrderShippingCost((int)$row['id_carrier']));
			$row['price_tax_exc'] = ($shippingMethod == Carrier::SHIPPING_METHOD_FREE ? 0 : $cart->getOrderShippingCost((int)$row['id_carrier'], false));
			$row['img'] = file_exists(_PS_SHIP_IMG_DIR_.(int)($row['id_carrier']).'.jpg') ? _THEME_SHIP_DIR_.(int)($row['id_carrier']).'.jpg' : '';

			// If price is false, then the carrier is unavailable (carrier module)
			if ($row['price'] === false)
			{
				unset($result[$k]);
				continue;
			}

			$resultsArray[] = $row;
		}
		return $resultsArray;
	}
}

