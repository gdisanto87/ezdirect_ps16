<?php

	include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

	class MarketingTelefonate extends AdminPreferences {
	
	public function bindDatepickerZ($id, $time)
	{
		if ($time)
			echo '
			var dateObj = new Date();
			var hours = dateObj.getHours();
			var mins = dateObj.getMinutes();
			var secs = dateObj.getSeconds();
			if (hours < 10) { hours = "0" + hours; }
			if (mins < 10) { mins = "0" + mins; }
			if (secs < 10) { secs = "0" + secs; }
			var time = " "+hours+":"+mins+":"+secs;';

		echo '
		$(function() {
			$("#'.Tools::htmlentitiesUTF8($id).'").datepicker({
				prevText:"",
				nextText:"",
				dateFormat:"dd-mm-yy"});
		});';
	}

	// id can be a identifier or an array of identifiers
	public function includeDatepickerZ($id, $time = false)
	{
		global $cookie;
		echo '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery-ui-1.8.10.custom.min.js"></script>';
		$iso = Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'lang WHERE `id_lang` = '.(int)$cookie->id_lang);
		if ($iso != 'en')
			echo '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/datepicker/ui/i18n/ui.datepicker-'.Tools::htmlentitiesUTF8($iso).'.js"></script>';
		echo '<script type="text/javascript">';
			if (is_array($id))
				foreach ($id as $id2)
					$this->bindDatepickerZ($id2, $time);
			else
				$this->bindDatepickerZ($id, $time);
		echo '</script>';
	}

	public	function datediff($tipo, $partenza, $fine)
    {
        switch ($tipo)
        {
            case "A" : $tipo = 365;
            break;
            case "M" : $tipo = (365 / 12);
            break;
            case "S" : $tipo = (365 / 52);
            break;
            case "G" : $tipo = 1;
            break;
        }
		$arrpulitopartenza = explode(" ", $partenza);
        $arr_partenza = explode("-", $arrpulitopartenza[0]);
        $partenza_gg = $arr_partenza[2];
        $partenza_mm = $arr_partenza[1];
        $partenza_aa = $arr_partenza[0];
		$arrpulitofine = explode(" ", $fine);
        $arr_fine = explode("-", $arrpulitofine[0]);
        $fine_gg = $arr_fine[2];
        $fine_mm = $arr_fine[1];
        $fine_aa = $arr_fine[0];
        $date_diff = mktime(12, 0, 0, $fine_mm, $fine_gg, $fine_aa) - mktime(12, 0, 0, $partenza_mm, $partenza_gg, $partenza_aa);
        $date_diff  = floor(($date_diff / 60 / 60 / 24) / $tipo);
        return $date_diff;
    }
	
		public function display()
		{
			global $cookie;
			
			
			if (get_magic_quotes_gpc()) {
				$process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
				while (list($key, $val) = each($process)) {
					foreach ($val as $k => $v) {
						unset($process[$key][$k]);
						if (is_array($v)) {
							$process[$key][stripslashes($k)] = $v;
							$process[] = &$process[$key][stripslashes($k)];
						} else {
							$process[$key][stripslashes($k)] = stripslashes($v);
						}
					}
				}
				unset($process);
			}


			global $cookie;
			echo "<h1>Analisi telefonate</h1>";

			echo "<p>Questo strumento ti consente di estrapolare liste di clienti che hanno fatto telefonate a Ezdirect.</p>";
			if(isset($_POST['cercaiclienti'])) { 
			
				$prodotti = "";
				$categorie_di_default = "";
				$macrocategorie = "";
				$macrocategorie_di_default = "";
				$categorie = "";
				$marchi = "";
				
				if(Tools::getValue('modalita') == 1)
				{
					$strisciamodalita = 'Ricerca eseguita sulle chiamate in uscita';		
					$modalita = 1;
				}	
				else if(Tools::getValue('modalita') == 2)
				{
					$strisciamodalita = 'Ricerca eseguita sia sulle chiamate in ingresso sia sulle chiamate in uscita';		
					$modalita = 2;
				}	
				else
				{
					$strisciamodalita = 'Ricerca eseguita sulle chiamate in ingresso';			
					$modalita = 0;
				}
				echo "<strong>Riepilogo</strong><br /><br />";
				
				if(!empty($_POST['arco_dal'])) {
					
					$data_dal_a = explode("-", $_POST['arco_dal']);
					$data_dal = $data_dal_a[2]."-".$data_dal_a[1]."-".$data_dal_a[0]; 
					
					if($modalita == 1 || $modalita == 2)
						$arco_dal = 'AND c.date_add > "'.$data_dal.' 00:00:00"';
					else
						$arco_dal = 'AND t.datetime > "'.$data_dal.' 00:00:00"';
					
					$striscia_arco_dal = "Da: <strong>$_POST[arco_dal]</strong> ";
					
				}
				else {
					$striscia_arco_dal = "Da: <strong>inizio della storia del sito</strong> ";
				
				}
				
				if(isset($_POST['striscia_arco_dal'])) {
				
					$striscia_arco_dal = $_POST['striscia_arco_dal'];
				}
			
				echo $striscia_arco_dal;
				
				if(!empty($_POST['arco_al'])) {
				
					$data_al_a = explode("-", $_POST['arco_al']);
					$data_al = $data_al_a[2]."-".$data_al_a[1]."-".$data_al_a[0]; 
					
					if($modalita == 1 || $modalita == 2)
						$arco_al = 'AND c.date_add < "'.$data_al.' 00:00:00"';
					else
						$arco_al = 'AND t.datetime < "'.$data_al.' 00:00:00"';
				
					$striscia_arco_al = "a: <strong>$_POST[arco_al]</strong> ";
				}
				else {
					$striscia_arco_al = "a: <strong>oggi</strong> ";
				
				}
				
				if(isset($_POST['striscia_arco_al'])) {
				
					$striscia_arco_al = $_POST['striscia_arco_al'];
				}
			
				echo $striscia_arco_al."<br />";
				
				//nuovi filtri
			
				$strisciatelefonate = '';
				$numero_telefonate = '';
				if(Tools::getValue('numero_telefonate') != 0)
				{
					switch(Tools::getValue('numero_telefonate'))
					{
						case '1_10': $numero_telefonate = ' AND numero_telefonate >= 1 AND numero_telefonate <= 10 '; $strisciatelefonate = ' che hanno effettuato da 1 a 10 telefonate'; break;
						case '11_30': $numero_telefonate = ' AND numero_telefonate >= 11 AND numero_telefonate <= 30 '; $strisciatelefonate = ' che hanno effettuato da 11 a 30 telefonate'; break;
						case '31_50': $numero_telefonate = ' AND numero_telefonate >= 31 AND numero_telefonate <= 50 '; $strisciatelefonate = ' che hanno effettuato da 31 a 50 telefonate'; break;
						case '50p': $numero_telefonate = ' AND numero_telefonate > 50 '; $strisciatelefonate = ' che hanno effettuato pi&ugrave; di 50 telefonate'; break;
					}
					
				}	
				
				$strisciadurata = '';
				$durata = '';
				
				if(Tools::getValue('durata') != 0)
				{
					switch(Tools::getValue('durata'))
					{
						case '1_15': $durata = ' AND durata >= 60 AND durata <= 900 '; $strisciadurata = ' che hanno effettuato chiamate per una durata complessiva compresa tra 1 e 15 minuti'; break;
						case '16_30': $durata = ' AND durata >= 901 AND durata <= 1800 '; $strisciadurata = ' che hanno effettuato chiamate per una durata complessiva compresa tra 16 e 30 minuti'; break;
						case '31_60': $durata = ' AND durata >= 1801 AND durata <= 3600 '; $strisciadurata = ' che hanno effettuato chiamate per una durata complessiva compresa tra 31 e 60 minuti'; break;
						case '60p': $durata = ' AND durata > 3600 '; $strisciadurata = ' che hanno effettuato chiamate per una durata complessiva superiore a un\'ora'; break;
					}
					
				}	
				
				$strisciareparto = '';
				$reparto = '';
				
				if(Tools::getValue('reparto') != 0)
				{
					switch(Tools::getValue('reparto'))
					{
						case '6700': $reparto = ' AND dst LIKE "%6700%" '; $strisciareparto = ' che hanno effettuato chiamate verso il reparto ordini'; break;
						case '6701': $reparto = ' AND (dst LIKE "%6701%" OR dst LIKE "%6706%") '; $strisciareparto = ' che hanno effettuato chiamate verso il reparto commerciale'; break;
						case '6702': $reparto = ' AND dst LIKE "%6702%" '; $strisciareparto = ' che hanno effettuato chiamate verso il reparto assistenza'; break;
						case '6703': $reparto = ' AND dst LIKE "%6703%" '; $strisciareparto = ' che hanno effettuato chiamate verso il reparto amministrazione'; break;
						case '6704': $reparto = ' AND (dst LIKE "%6704%" OR dst LIKE "%6706%") '; $strisciareparto = ' che hanno effettuato chiamate in entrata'; break;
					}
					
				}	
				
				$strisciainterno = '';
				$interno = '';
				
				if(Tools::getValue('interno') != 0)
				{
					if($modalita == 1)
						$interno = ' AND src LIKE "%'.Tools::getValue('interno').'%"';
					else if($modalita == 0)
						$interno = ' AND dst LIKE "%'.Tools::getValue('interno').'%"';
					else if($modalita == 2)
						$interno = ' AND (src LIKE "%'.Tools::getValue('interno').'%" OR dst LIKE "%'.Tools::getValue('interno').'%")';
					
					switch(Tools::getValue('interno'))
					{
						case '201': $strisciainterno .= 'chiamate su interno Ezio'; break;
						case '202': $strisciainterno .= 'chiamate su interno Barbara'; break;
						case '203': $strisciainterno .= 'chiamate su interno Paolo'; break;
						case '205': $strisciainterno .= 'chiamate su interno Massimo'; break;
						case '206': $strisciainterno .= 'chiamate su interno Magazzino'; break;
						case '207': $strisciainterno .= 'chiamate su interno Matteo'; break;
						case '208': $strisciainterno .= 'chiamate su interno Valentina'; break;
						case '209': $strisciainterno .= 'chiamate su interno Federico'; break;
						case '210': $strisciainterno .= 'chiamate su interno Sala Riunioni'; break;
						case '211': $strisciainterno .= 'chiamate su interno Ingresso'; break;
						case '212': $strisciainterno .= 'chiamate su interno Laboratorio'; break;
						case '214': $strisciainterno .= 'chiamate su interno Laboratorio 1'; break;
						case '215': $strisciainterno .= 'chiamate su interno Laboratorio 2'; break;
						case '216': $strisciainterno .= 'chiamate su interno Laboratorio 3'; break;
						case '217': $strisciainterno .= 'chiamate su interno Laboratorio 4'; break;
						case '218': $strisciainterno .= 'chiamate su interno Amministrazione'; break;
						case '219': $strisciainterno .= 'chiamate su interno Lorenzo'; break;
						
					}
					
				}	
				
				$strisciaprivati = '';
				$strisciagruppi = '';
				
				$gruppi = 'AND (';
				foreach($_POST['group'] as $key => $value)
				{	
					$gruppi .= 'c.id_default_group = '.$key.' OR ';
					$strisciagruppi .= ' '.Db::getInstance()->getValue('SELECT name FROM group_lang WHERE id_group = '.$key.' AND id_lang = 5').',';
				}	
				$gruppi	.= 'c.id_default_group = 99999999)';
				$strisciagruppi = 'In questi gruppi:'.substr($strisciagruppi, 0, -1);	
					
				if($_POST['escludi_privati'] == 1) {
				
					$strisciaprivati = "<strong>Privati esclusi</strong>";
				}
				else {
					$strisciaprivati = "<strong>Aziende e privati inclusi</strong>";
				}
				
				if(Tools::getValue('inattivita') > 0)
					$strisciainattivita = 'Che non acquistano da '.Tools::getValue('inattivita').' mesi';
				
				echo $strisciaprivati.'<br />'.$strisciagruppi.'<br />'.($strisciatelefonate != '' ? $strisciatelefonate.'<br />' : '').($strisciadurata != '' ? $strisciadurata.'<br />' : '').($strisciareparto != '' ? $strisciareparto.'<br />' : '').($strisciainterno != '' ? $strisciainterno.'<br />' : '').'<strong>'.$strisciamodalita.'</strong><br />';
				
				//fine nuovi filtri
				
				$anno_corrente = "2017";
				
				$where = $reparto.$interno;
				
				$from = 'FROM customer c JOIN telefonate t ON c.id_customer = t.id_customer JOIN address a ON c.id_customer = a.id_customer
						JOIN state s ON a.id_state = s.id_state';
				
				if($modalita == 1)
				{
					$where .= ' AND calltype = "Outbound" '; 

				}	
				else if($modalita == 0)
				{
					$where .= ' AND calltype = "Inbound" '; 
				}
				else 
				{
					$where .= ' AND (calltype = "Inbound" OR calltype = "Outbound") '; 
				}		
				
				
				if(isset($_POST['querysql'])) {

					$sql = $_POST['querysql'];
					
				}
			
				else {
				
						$sql = "SELECT * FROM (SELECT count(id_telefonata) numero_telefonate, sum(billable) durata, 
						IF(calltype LIKE '%inbound%', SUM(billable),NULL) AS durata_ricevute, IF(calltype LIKE '%outbound%', SUM(billable),NULL) AS durata_inviate,
						IF(calltype LIKE '%inbound%',count(id_telefonata),NULL) AS totale_ricevute, IF(calltype LIKE '%outbound%', count(id_telefonata),NULL) AS totale_inviate, 
						c.id_customer, c.firstname, c.lastname, gr.name gruppo, s.iso_code, c.email, c.id_default_group, c.is_company, c.company, a.phone, a.phone_mobile, a.fax, fat.totale_acquisti, fa.ultimo_anno,

						c.firstname AS nome_mailup, c.lastname AS cognome_mailup, c.company AS azienda_mailup, c.email AS email_mailup, REPLACE(a.address1,',',' ') AS indirizzo_mailup, a.city AS citta_mailup, a.postcode AS cap_mailup, a.phone AS telefono_mailup, a.phone_mobile AS cellulare_mailup,

						primo.data_fattura primo_acquisto, ultimo.data_fattura ultimo_acquisto, (CASE WHEN c.is_company = 0 THEN CONCAT(c.lastname,\" \",c.firstname) WHEN c.is_company = 1 THEN (CASE WHEN c.company IS NULL OR c.company = \"\" THEN CONCAT(c.lastname,\" \",c.firstname) ELSE c.company END) END) cliente
						$from
						LEFT JOIN (SELECT * FROM fattura GROUP BY id_customer ORDER BY data_fattura ASC ) primo ON primo.id_customer = c.id_customer
						LEFT JOIN (SELECT id_customer, MAX(data_fattura) data_fattura FROM fattura GROUP BY id_customer ORDER BY data_fattura DESC) ultimo ON ultimo.id_customer = c.id_customer
						
						LEFT JOIN (SELECT * FROM group_lang WHERE id_lang = 5) gr ON c.id_default_group = gr.id_group
						LEFT JOIN (SELECT SUM(fprv.totale_fattura) AS totale_acquisti, fprv.id_customer FROM (SELECT totale_fattura, id_customer FROM fattura GROUP BY id_fattura) fprv GROUP BY fprv.id_customer) fat ON fat.id_customer = c.id_customer
						LEFT JOIN (SELECT SUM(totale_fattura) AS ultimo_anno, id_customer FROM (SELECT totale_fattura, id_customer FROM fattura WHERE data_fattura > '".$anno_corrente."-01-01 00:00:00' AND data_fattura < '".$anno_corrente."-12-31 00:00:00' GROUP BY id_fattura) faprv GROUP BY faprv.id_customer) fa ON fa.id_customer = c.id_customer
						
						".(Tools::getIsset('prev_richiesto') ? 'JOIN (SELECT * FROM form_prevendita_thread WHERE tipo_richiesta = "preventivo") fp ON fp.id_customer = c.id_customer' : '')."
						".(Tools::getIsset('ric_richiesto') ? 'JOIN (SELECT * FROM form_prevendita_thread WHERE tipo_richiesta = "tirichiamiamonoi") fp2 ON fp2.id_customer = c.id_customer' : '')."
						".(Tools::getIsset('prev_ricevuto')  ? 'JOIN cart ca ON ca.id_customer = c.id_customer' : '')."
						
						WHERE a.fatturazione =1
						AND a.deleted =0
						AND a.active =1 
						".(Tools::getValue('escludi_privati') == 1 ? 'AND c.is_company = 1' : '')."
						".(Tools::getValue('inattivita') > 0 ? 'AND DATE_FORMAT(ultimo.data_fattura, "%Y-%m-%d %H:%i") < DATE_FORMAT(NOW() - INTERVAL '.Tools::getValue('inattivita').' MONTH, "%Y-%m-%d %H:%i")' : '')."
						
						$gruppi
						$arco_dal
						$arco_al
						$where 
						
						
						GROUP BY c.id_customer
						
						) cz WHERE cz.id_customer > 0 ".$numero_telefonate.$durata." GROUP BY cz.id_customer
						";
						
					
				}
		
				//echo $sql;
				
				$numeroclienti = Db::getInstance()->NumRows($sql);
		
		
				echo "<br />";
				
				
				echo "<form method='post' action=''>";
				/*echo "<strong>Esportazione Dati</strong> - opzioni<br />";
				echo "<strong>IMPORTANTE: non spuntare NESSUNA CASELLA se vuoi esportare la lista completa che trovi qua sotto.</strong><br /><br />";
				echo "<input type='checkbox' name='esportaaziende' /> Esporta aziende<br />";
				echo "<input type='checkbox' name='esportaprivati' /> Esporta privati<br />---<br />";
				echo "<input type='checkbox' name='esportaclienti' /> Esporta clienti<br />";
				echo "<input type='checkbox' name='esportarivenditori' /> Esporta rivenditori<br />--<br />";
				
				echo "<input type='checkbox' name='escludimarketplace' /> Escludi clienti marketplace (Amazon, ePrice)<br /><br />";
				*/
				echo "<!-- <input type='submit' value='Esporta i dati in formato Excel' name='esportaexcel' class='button' /> -->
				<input type='submit' value='Vedi solo le mail in formato TXT per invio' name='esportamailtxt' class='button' />
				<input type='submit' value='Esporta lista in formato CSV per Mailup' name='esportamailup' class='button' />
				<input type='hidden' name='querysql' value='".htmlentities((isset($_POST['querysql']) ? $_POST['querysql'] : $sql),ENT_QUOTES)."' />	
				<input type='hidden' name='query_from' value='".htmlentities((isset($_POST['query_from']) ? $_POST['query_from'] : $from),ENT_QUOTES)."' />	
				<input type='hidden' name='query_where' value='".htmlentities((isset($_POST['query_where']) ? $_POST['query_where'] : $where),ENT_QUOTES)."' />						
				<input type='hidden' name='prodottiinclusi' value='".$prodottiinclusi."' />		
				<input type='hidden' name='strisciaprodotti' value='".$strisciaprodotti."' />		
				<input type='hidden' name='categorieincluse' value='".$categorieincluse."' />	
				<input type='hidden' name='macrocategorieincluse' value='".$macrocategorieincluse."' />						
				<input type='hidden' name='strisciacat' value='".$strisciacat."' />	
				<input type='hidden' name='marchiinclusi' value='".$marchiinclusi."' />		
				<input type='hidden' name='strisciamarchi' value='".$strisciamarchi."' />	
				<input type='hidden' name='striscia_escludi_privati' value='".$striscia_escludi_privati."' />				
				<input type='hidden' name='striscia_arco_dal' value='".$striscia_arco_dal."' />	
				<input type='hidden' name='striscia_arco_al' value='".$striscia_arco_al."' />	
				<input type='hidden' name='strisciaprivati' value='".$strisciaprivati."' />	
				<input type='hidden' name='strisciainattivita' value='".$strisciainattivita."' />	
				<input type='hidden' name='strisciagruppi' value='".$strisciagruppi."' />	
				<input type='hidden' name='strisciaprevric' value='".$strisciaprevric."' />	
				<input type='hidden' name='strisciaprevriv' value='".$strisciaprevriv."' />	
				<input type='hidden' name='strisciaricric' value='".$strisciaricric."' />	
				";
				echo "<br /><br />";
				
				$sql.= $orderby;
			
				$resultsclienti = Db::getInstance()->ExecuteS(stripslashes($sql));
				$tot_acquisti = 0;
				
				$num = count($resultsclienti);
				foreach($resultsclienti as $r)
					$tot_acquisti += $r['totale_acquisti'];
					
				echo '<script type="text/javascript" src="../js/jquery/jquery.tablesorter.min.js"></script> 
				<script type="text/javascript" src="../js/jquery/jquery.tablesorter.widgets.js"></script> 
				<script type="text/javascript" src="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.js"></script> 
				<link rel="stylesheet" type="text/css" href="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.css" />';
				
				echo '
				<script type="text/javascript">
				var myTextExtraction = function(node) 
					{ 
						// extract data from markup and return it 
						return node.textContent.replace( /<.*?>/g, "" ); 
					}
					
				$(document).ready(function() 
					{ 
					var maxDigits = 3;
					
					$.tablesorter.addParser({
						id: "times",
						is: function (s) {
							return false;
						},
						format: function (s) {
							// prefix contains leading zeros that are tacked
							var prefix = new Array(maxDigits + 1).join("0"),
								// split time into blocks
								blocks = s.split(/\s*:\s*/),
								len = blocks.length,
								result = [];
							// add values in reverse, so if there is only one block
							// (e.g. "10"), then it would be the time in seconds
							while (len) {
								result.push((prefix + (blocks[--len] || 0)).slice(-maxDigits));
							}
							// reverse the results and join them
							return result.length ? result.reverse().join("") : s;
						},
						type: "text"
					});

				
					$.tablesorter.addParser({ 
						// set a unique id 
						id: "thousands",
						is: function(s) { 
							// return false so this parser is not auto detected 
							return false; 
						}, 
						format: function(s) {
							// format your data for normalization 
							return s.replace(/[\s.$£€]/g,"").replace(/,/g,".");
						}, 
						// set type, either numeric or text 
						type: "numeric" 
					}); 


						$("#table-clienti").tablesorter({  headers: {
						0: {sorter: false}, 9: {sorter: "times"}, 6: {	sorter:"thousands"	}, 7: {	sorter:"thousands"	}
				}, widthFixed: true, widgets: ["zebra"]});
							
						} 
					);
					</script>';
					
					echo "<script type='text/javascript'>
					$(document).ready(function() {
						function moveScroll(){
							var scroll = $(window).scrollTop();
							var anchor_top = $('#table-clienti').offset().top;
							var anchor_bottom = $('#bottom_anchor').offset().top;
							if (scroll>anchor_top && scroll<anchor_bottom) {
							clone_table = $('#clone');
							if(clone_table.length == 0){
								clone_table = $('#table-clienti').clone();
								clone_table.attr('id', 'clone');
								clone_table.css({position:'fixed',
										'pointer-events': 'none',
										top:0});
								clone_table.width($('#table-clienti').width());
								$('#table-container').append(clone_table);
								$('#clone').css({visibility:'hidden'});
								$('#clone thead').css({'visibility':'visible','pointer-events':'auto'});
							}
							} else {
							$('#clone').remove();
							}
						}
						$(window).scroll(moveScroll); 
					});
				</script>";
			
				$totale_chiamate = 0;
				$totale_durata = 0;
				$totale_ricevute = 0;
				$totale_inviate = 0;
				$durata_ricevute = 0;
				$durata_inviate = 0;
			
				echo "<input type='hidden' name='num' value='".$num."' />		
				<input type='hidden' name='tot_acquisti' value='".$tot_acquisti."' />";	
				echo "<table class='table'><tr><td>Totale clienti trovati</td><td><strong>$num</strong></td></tr>";
				echo "<tr><td>Totale acquisti</td><td><strong>".Tools::displayPrice($tot_acquisti,1)."</strong></td></tr></table><br />";
				echo "<div id='table-container'><table class='table tablesorter' id='table-clienti'><thead>";
				echo "<tr>";
				echo "<th style='width:30px'>N.</th>";
				echo "<th style='width:45px'>Id cliente 
				</th>";
				echo "<th style='width:150px'>Cliente
				</th>";
				echo "<th style='width:50px'>Tipo
				</th>";
				echo "<th style='width:50px'>Gruppo
				</th>";
			
				echo "<th style='width:30px'>Provincia
				</th>";
				
				
				echo "<th style='width:80px'>Totale acq. in euro
				</th>
				";
				
				echo "<th style='width:80px'>Totale acq. ".$anno_corrente."
				</th>
				";
				
				if($modalita == 2)
				{
					echo "<th style='width:80px'>Tot. ricevute
				
					</th>";
					
					echo "<th style='width:80px'>Dur. ricevute</th>
					
					";
					
					echo "<th style='width:80px'>Tot. inviate
				
					</th>";
					
					echo "<th style='width:80px'>Dur. inviate</th>
					
					";
				}
				else
				{
					echo "<th style='width:80px'>Tot.
					
					</th>";
					
					echo "<th style='width:80px'>Durata complessiva</th>
					
					";
				}
				//echo "<th>Mesi inattivit&agrave;</th>";
				
				echo "<th>Vai</th>";
				echo "</tr></thead><tbody>";
				
				$i = 1;
				
				foreach ($resultsclienti as $row) {
					
					if($row['totale_ricevute'] == '')
						$row['totale_ricevute'] = 0;
					
					if($row['totale_inviate'] == '')
						$row['totale_inviate'] = 0;
					
					if($row['is_company'] == 1) {
						$tipo = 'A';
					}
					else {
						$tipo = 'P';
					}
			
					$mesi_inattivita = $this->datediff("M", $row['ultimo_acquisto'], date("Y-m-d H:i:s"));
					
					
					$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee));
					
					
					
					
					echo "<tr>";
					echo "<td style='width:30px'>".$i."</td>";
					echo "<td>".$row['id_customer']."</td>";
					echo "<td>".$row['cliente']."</td>";
					echo "<td>".$tipo."</td>";
					echo "<td>".$row['gruppo']."</td>";
					echo "<td style='width:30px'>".$row['iso_code']."</td>";
					
					echo "<td style='text-align:right'>".Tools::displayPrice($row['totale_acquisti'],1)."</td>";
					
					echo "<td style='text-align:right'>".Tools::displayPrice($row['ultimo_anno'],1)."</td>";
					
					if($modalita == 2)
					{
						echo "<td style='text-align:right'>".$row['totale_ricevute']."</td>";
						echo "<td style='text-align:right'>".gmdate("H:i:s", $row['durata_ricevute'])."</td>";
						echo "<td style='text-align:right'>".$row['totale_inviate']."</td>";
						echo "<td style='text-align:right'>".gmdate("H:i:s", $row['durata_inviate'])."</td>";
					}
					else
					{
						echo "<td style='text-align:right'>".$row['numero_telefonate']."</td>";
						echo "<td style='text-align:right'>".gmdate("H:i:s", $row['durata'])."</td>";
					}
					//echo "<td>".$mesi_inattivita."</td>";
					
					echo "<td><a href='index.php?tab=AdminCustomers&id_customer=".$row['id_customer']."&viewcustomer&token=".$tokenCustomers."&tab-container-1=1' target='_blank'><img src='../img/admin/details.gif' alt='Apri cliente' title='Apri cliente' /></a></td>";
					
					$totale_chiamate += $row['numero_telefonate'];
					$totale_durata += $row['durata'];
					$totale_ricevute += $row['totale_ricevute'];
					$durata_ricevute += $row['durata_ricevute'];
					$totale_inviate += $row['totale_inviate'];
					$durata_inviate += $row['durata_inviate'];
					
					echo "</tr>";
					
					$i++;
				
				}
				
				echo "</tbody><tfoot><tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th>".($modalita == 2 ? '<th>'.$totale_ricevute.'</th><th>'.gmdate("H:i:s", $durata_ricevute).'</th><th>'.$totale_inviate.'</th><th>'.gmdate("H:i:s", $durata_inviate).'</th>' : "<th>".$totale_chiamate."</th><th>".gmdate("H:i:s", $totale_durata)."</th>")."<th></th></tr></tfoot></table></div><div id='bottom_anchor'>
				</div>";
				echo "</form>";
			}
			else if(isset($_POST['esportamailup'])) { 
				
				$resultsclienti = Db::getInstance()->ExecuteS(html_entity_decode($_POST['querysql']));
				$mails = '';
				foreach ($resultsclienti as $row) {
					if(isset($_POST['esportaaziende']) && $row['is_company'] == 0) {
					}
					else if(isset($_POST['esportaprivati']) && $row['is_company'] == 1) {
					}
					else if(isset($_POST['esportaclienti']) && ($row['id_default_group'] == 3 || $row['id_default_group'] != 15)) {
					}
					else if(isset($_POST['esportarivenditori']) && ($row['id_default_group'] != 3 && $row['id_default_group'] != 15)) {
					}
					else {
					
						if(isset($_POST['escludimarketplace']) && ($row['id_default_group'] == 16 || $row['id_default_group'] == 17)) {
						}
						else
						{
							$mails.= $row['nome_mailup'].";".$row['cognome_mailup'].";".$row['azienda_mailup'].";".$row['email_mailup'].";".$row['indirizzo_mailup'].";".$row['citta_mailup'].";".$row['cap_mailup'].";".$row['telefono_mailup'].";".$row['cellulare_mailup']."\n";
						}
					}	
				}
				
				$file_csv_mailup_stream=fopen("/var/www/vhosts/ezdirect.it/httpdocs/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-clienti-$data-mailup.csv","w+");
				@fwrite($file_csv_mailup_stream,$mails);
				@fclose("/var/www/vhosts/ezdirect.it/httpdocs/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-clienti-$data-mailup.csv");
		
				header("Location: http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-clienti-$data-mailup.csv");
			}
			
			else if(isset($_POST['esportamailtxt'])) { 
			
				$resultsclienti = Db::getInstance()->ExecuteS(html_entity_decode($_POST['querysql']));
				$mails = '';
				foreach ($resultsclienti as $row) {
					
					if(isset($_POST['esportaaziende']) && $row['is_company'] == 0) {
					}
					else if(isset($_POST['esportaprivati']) && $row['is_company'] == 1) {
					}
					else if(isset($_POST['esportaclienti']) && ($row['id_default_group'] == 3 || $row['id_default_group'] != 15)) {
					}
					else if(isset($_POST['esportamarketplace']) && ($row['id_default_group'] != 3 && $row['id_default_group'] != 15)) {
					}
					else {
					
						if(isset($_POST['escludirivenditori']) && ($row['id_default_group'] == 16 || $row['id_default_group'] == 17)) {
						}
						else
						{
							$mails.= $row['email']."; ";
						}
					}
				}
		
				echo $mails;
				
				/*$filename = "mail-clienti.txt";
				
				$newf = fopen($filename, 'w');
				fwrite($newf, $mails);
				fclose($newf);
				header("Content-Description: File Transfer");
				header("Content-Length: ". filesize("$filename").";");
				header("Content-Disposition: attachment; filename=$filename");
				header("Content-Type: application/octet-stream; "); 
				header("Content-Transfer-Encoding: binary");
				readfile($filename);*/

			}
			
			else if(isset($_POST['esportaexcel'])) { 
				ini_set("memory_limit","892M");
				set_time_limit(3600);
				require_once 'esportazione-catalogo/Classes/PHPExcel.php';
				$objPHPExcel = new PHPExcel();
				
				
				$resultsclienti = Db::getInstance()->ExecuteS(stripslashes($_POST['querysql']));
				
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'Filtri attivati: '.strip_tags($_POST['strisciaprivati']).' / '.$_POST['strisciagruppi'].' / '.($_POST['strisciainattivita'] != '' ? $_POST['strisciainattivita'].' / ' : '').($_POST['strisciaprevric'] != '' ? $_POST['strisciaprevric'].' / ' : '').($_POST['strisciaprevriv'] != '' ? $_POST['strisciaprevriv'].' / ' : '').$_POST['strisciaricric'].'')
					->setCellValue('A2', 'Prodotti: '.$_POST['prodottiinclusi'].'')
					->setCellValue('A3', 'Categorie: '.$_POST['macrocategorieincluse'].' '.$_POST['categorieincluse'].'')
					->setCellValue('A4', 'Marchi: '.$_POST['marchiinclusi'].'')
					->setCellValue('A5', 'Data esportazione: '.date('d/m/Y H:i:s').'')
					->setCellValue('B5', $_POST['strisciamodalita'])
					->setCellValue('A6', 'N.')
					->setCellValue('B6', 'Id cliente')
					->setCellValue('C6', 'Nome')
					->setCellValue('D6', 'Cognome')
					->setCellValue('E6', 'Azienda')
					->setCellValue('F6', 'Tipo')
					->setCellValue('G6', 'Gruppo')
					->setCellValue('H6', 'Email')
					->setCellValue('I6', 'Telefono')
					->setCellValue('J6', 'Cellulare')
					->setCellValue('K6', 'Fax')
					->setCellValue('L6', 'Provincia')
					->setCellValue('M6', 'Tot. acq. in euro')
					->setCellValue('N6', 'Tot. acq. '.date("Y").'')
					->setCellValue('O6', 'Data primo acquisto')
					->setCellValue('P6', 'Data ultimo acquisto')
					->setCellValue('Q6', 'Mesi inattivita')
					;
				$k = 7;
				$i = 1;
				
				$objPHPExcel->getActiveSheet()->getStyle("I")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("J")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("K")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$tot_acquisti = 0;
				$num = count($resultsclienti);
				foreach ($resultsclienti as $row) {
					$tot_acquisti += $row['totale_acquisti'];
					
					if($row['is_company'] == 1) {
						$tipo = 'Azienda';
					}
					else {
						$tipo = 'Privato';
					}
					
					
					
					if(isset($_POST['esportaaziende']) && $row['is_company'] == 0) {
					}
					else if(isset($_POST['esportaprivati']) && $row['is_company'] == 1) {
					}
					else if(isset($_POST['esportaclienti']) && ($row['id_default_group'] == 3 || $row['id_default_group'] != 15)) {
					}
					else if(isset($_POST['esportarivenditori']) && ($row['id_default_group'] != 3 && $row['id_default_group'] != 15)) {
					}
					else {
					
						if(isset($_POST['escludimarketplace']) && ($row['id_default_group'] == 16 || $row['id_default_group'] == 17)) {
						}
						else
						{
							$tot_anno[$key] = $ultimo_anno;
						
							$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue("A$k", $i)
							->setCellValue("B$k", $row['id_customer'])
							->setCellValue("C$k", $row['firstname'])
							->setCellValue("D$k", $row['lastname'])
							->setCellValue("E$k", $row['company'])
							->setCellValue("F$k", $tipo)
							->setCellValue("G$k", $row['gruppo'])
							->setCellValue("H$k", $row['email'])
							->setCellValue("I$k", $row['phone'])
							->setCellValue("J$k", $row['phome_mobile'])
							->setCellValue("K$k", $row['fax'])
							->setCellValue("L$k", $row['iso_code'])
							->setCellValue("M$k", str_replace(".",",",round($row['totale_acquisti'],2)))
							->setCellValue("N$k", str_replace(".",",",round($row['ultimo_anno'],2)))
							->setCellValue("O$k", date('d/m/Y', strtotime($row['primo_acquisto'])))
							->setCellValue("P$k", date('d/m/Y', strtotime($row['ultimo_acquisto'])))
							->setCellValue("Q$k", $mesi_inattivita)
							;
						
							$objPHPExcel->getActiveSheet()->getCell("I$k")->setValueExplicit($row['phone'], PHPExcel_Cell_DataType::TYPE_STRING);
							$objPHPExcel->getActiveSheet()->getCell("J$k")->setValueExplicit($row['phone_mobile'], PHPExcel_Cell_DataType::TYPE_STRING);
							$objPHPExcel->getActiveSheet()->getCell("K$k")->setValueExplicit($row['fax'], PHPExcel_Cell_DataType::TYPE_STRING);
							$objPHPExcel->getActiveSheet()->getStyle("M$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
							$objPHPExcel->getActiveSheet()->getStyle("N$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
							$i++;
							$k++;
						}
					}
				}
				
				$objPHPExcel->getActiveSheet()->setTitle('Clienti');

				$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	

				$objPHPExcel->getActiveSheet()->getStyle('A6:Q6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
					
				$objPHPExcel->getActiveSheet()->getStyle('A6:Q6')->getFill()->getStartColor()->setRGB('FFFF00');	

				$objPHPExcel->getActiveSheet()->getStyle("A6:Q$k")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('A6:Q6')->getFont()->setBold(true);

				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
				
				
				// Set active sheet index to the first sheet, so Excel opens this as the first sheet
				$objPHPExcel->setActiveSheetIndex(0);

				$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
				$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
				PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

				$data = date("Ymd");

				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->setPreCalculateFormulas(false);
				$objWriter->save(str_replace(".php", ".xls", "esportazione-catalogo/catalogo_xls/prodotti-clienti-$data.php"));

				$this->_html .= "<div style='visibility:hidden'><br /><br />FILE PRONTO PER IL DOWNLOAD: <a id='scaricaexcelstatistiche' href='http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-clienti-$data.xls'>CLICCA QUI PER SCARICARE</a>!</div>";

				header("Location: http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-clienti-$data.xls");
					
				
				
			
			}
			
			else {
			
			
				echo '<script type="text/javascript" src="../js/select2.js"></script>
				<script type="text/javascript">				
				$(document).ready(function() {
						$(".hint_tooltip").tooltip({ showURL: false  });
					});
				</script>
				
				<strong>Filtri di selezione</strong><br /><br />
				
				<form name="cercaclienti" method="post" action="">
				<table cellpadding="9">
				';
				
				
				echo "<tr><td>Privati?</td><td>
				<input type='radio' value='0' name='escludi_privati' checked='checked' /> Includi privati &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				<input type='radio' value='1' name='escludi_privati' /> Escludi privati</td><td></td></tr>
				";
				
				echo "<tr><td>Profilo cliente</td><td>";
				
				$gruppi_clienti = Db::getInstance()->executeS('SELECT id_group, name FROM group_lang WHERE id_lang = 5 AND (id_group != 11 AND id_group != 14 AND id_group != 4)');
				
				foreach($gruppi_clienti as $g)
					echo "<input type='checkbox' value='0' class='gruppi' name='group[".$g['id_group']."]' /> ".$g['name']." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ";
				
				echo " - <a href='javascript:void(0)' onclick='$(\".gruppi\").attr(\"checked\", true);'>Clicca per selezionarli tutti</a></td><td></td></tr>
				";
				
				echo "<tr><td>Numero telefonate</td><td>
				<select name='numero_telefonate' style='width:300px'>
				<option value='0'> Qualsiasi </option>
				<option value='1_10'>Da a 10</option>
				<option value='11_30'>Da 11 a 30</option>
				<option value='31_50'>Da 31 a 50</option>
				<option value='50p'>Pi&ugrave; di 50</option>
				</select>
				</td><td><span class='hint_tooltip' style=\"cursor:pointer;\" title=\"Per selezionare il numero complessivo di telefonate fatte dal cliente nel periodo selezionato. \" ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				";
				
				echo "<tr><td>Durata complessiva</td><td>
				<select name='durata' style='width:300px'>
				<option value='0'> Qualsiasi </option>
				<option value='1_15'>Da 1 a 15 minuti</option>
				<option value='16_30'>Da 16 a 30 minuti</option>
				<option value='31_60'>Da 31 a 60 minuti</option>
				<option value='60p'>Pi&ugrave; di un'ora</option>
				</select>
				</td><td><span class='hint_tooltip' style=\"cursor:pointer;\" title=\"Per selezionare la durata complessiva delle telefonate fatte dal cliente nel periodo selezionato. \" ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				";
				
				echo "<tr><td>Per reparto raggiunto</td><td>
				<select name='reparto' style='width:300px'>
				<option value='0'> Tutti </option>
				<option value='6700'>Ordini</option>
				<option value='6701'>Commerciale</option>
				<option value='6702'>Assistenza tecnica</option>
				<option value='6703'>Amministrazione</option>
				<option value='6704'>Chiamata in entrata</option>
				</select>
				</td><td><span class='hint_tooltip' style=\"cursor:pointer;\" title=\"Per selezionare tutte le chiamate da o verso un preciso reparto. \" ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				";
				
				echo "<tr><td>Per interno raggiunto</td><td>
				<select name='interno' style='width:300px'>
				<option value='0'> Tutti </option>
				<option value='201'>Ezio</option>
				<option value='202'>Barbara</option>
				<option value='203'>Paolo</option>
				<option value='205'>Massimo</option>
				<option value='206'>Magazzino</option>
				<option value='207'>Matteo</option>
				<option value='208'>Valentina</option>
				<option value='209'>Federico</option>
				<option value='210'>Sala Riunioni</option>
				<option value='211'>Ingresso</option>
				<option value='212'>Laboratorio</option>
				<option value='214'>Laboratorio 1</option>
				<option value='215'>Laboratorio 2</option>
				<option value='216'>Laboratorio 3</option>
				<option value='217'>Laboratorio 4</option>
				<option value='218'>Amministrazione</option>
				<option value='219'>Lorenzo</option>
				</select>
				</td><td><span class='hint_tooltip' style=\"cursor:pointer;\" title=\"Per selezionare tutte le chiamate da o verso un preciso interno. \" ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				";
				
				/*
				echo "<tr><td>Preventivo?</td><td>
				<input type='checkbox' value='0' name='prev_richiesto' /> Richiesto &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				<input type='checkbox' value='0' name='prev_ricevuto' /> Ricevuto</td><td><span class='hint_tooltip' style=\"cursor:pointer;\" title=\"Per selezionare clienti che hanno richiesto o ricevuto un preventivo.\" ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				";
								echo "<tr><td>Ti richiamiamo noi</td><td>
				<input type='checkbox' value='0' name='ric_richiesto' /> Richiesto</td><td><span class='hint_tooltip' style=\"cursor:pointer;\" title=\"Per selezionare clienti che hanno chiesto di essere richiamati.\" ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				";
				*/
				echo "<tr><td>Data</td><td>";
				
				include_once('functions.php');
			
				$this->includeDatepickerZ(array('arco_dal', 'arco_al'), true);
				
				echo 'Dal <input type="text" id="arco_dal" name="arco_dal"  /> al <input type="text" id="arco_al" name="arco_al"  /></td><td><span class="hint_tooltip" style="cursor:pointer;" title="Modalità acquisti: si considera la data di acquisto. Modalità no acquisto o modalità tutti: si considera la data di registrazione dell\'account." ><img src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Aiuto" /></span></td></tr>';
				
				echo "<tr><td>Modalit&agrave;</td><td>
				<input type='radio' value='0' name='modalita' checked='checked' /> In ingresso &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				
				<input type='radio' value='1' name='modalita' /> In uscita &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				<input type='radio' value='2' name='modalita' /> Entrambe
				";
				
				echo "<tr><td></td><td><input type='submit' name='cercaiclienti' value='Cerca clienti' class='button' /></td><td></td></tr>
				</table>
				</form>";
			
			}
			
			
			
		}

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	