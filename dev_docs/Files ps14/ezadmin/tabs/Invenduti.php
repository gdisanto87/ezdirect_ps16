<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14632 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

//00002730

class Invenduti extends AdminTab
{
		public function display()
		{	
			global $cookie, $currentIndex;
			echo "<h1>Prodotti invenduti (non movimentati da pi&ugrave; di 30 giorni)</h1>";
			
			$prodotti = Db::getInstance()->ExecuteS("SELECT p.id_product, p.reference, p.supplier_reference, pl.name, p.stock_quantity, m.name costruttore FROM product p JOIN manufacturer m ON p.id_manufacturer = m.id_manufacturer JOIN product_lang pl ON p.id_product = pl.id_product WHERE p.stock_quantity > 0 AND p.stock_quantity != 999999 AND pl.id_lang = 5 AND p.id_product NOT IN (SELECT product_id FROM order_detail od JOIN orders o ON od.id_order = o.id_order WHERE o.date_add BETWEEN date_sub(curdate(),interval 30 day) AND date_sub(curdate(),interval 0 day)) ORDER BY ".(Tools::getIsset('orderby') ? Tools::getValue('orderby') : 'm.name')." ".(Tools::getIsset('orderway') ? Tools::getValue('orderway') : 'ASC')."");
		
			echo "<script type='text/javascript'>
				$(document).ready(function() {
					function moveScroll(){
						var scroll = $(window).scrollTop();
						var anchor_top = $('#tablePrezzi').offset().top;
						var anchor_bottom = $('#bottom_anchor').offset().top;
						if (scroll>anchor_top && scroll<anchor_bottom) {
						clone_table = $('#clone');
						if(clone_table.length == 0){
							clone_table = $('#tablePrezzi').clone();
							clone_table.attr('id', 'clone');
							clone_table.css({position:'fixed',
									 'pointer-events': 'none',
									 top:0});
							clone_table.width($('#tablePrezzi').width());
							$('#table-container').append(clone_table);
							$('#clone').css({visibility:'hidden'});
							$('#clone thead').css({'visibility':'visible','pointer-events':'auto'});
						}
						} else {
						$('#clone').remove();
						}
					}
					$(window).scroll(moveScroll); 
				});
			</script>
			<div id='table-container'>";
		
			$lista_prodotti = "<table class='table' id='tablePrezzi'><thead><tr>
			<th style='width:260px'>Codice Spring <a href='index.php?tab=Invenduti&orderby=p.reference&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=Invenduti&orderby=p.reference&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
			
			<th style='width:260px'>Codice SKU <a href='index.php?tab=Invenduti&orderby=p.supplier_reference&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=Invenduti&orderby=p.supplier_reference&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
			
			<th style='width:150px'>Costruttore <a href='index.php?tab=Invenduti&orderby=m.name&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=Invenduti&orderby=m.name&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
			
			<th style='width:300px'>Descrizione <a href='index.php?tab=Invenduti&orderby=pl.name&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=Invenduti&orderby=pl.name&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
			
			<th style='width:50px'>Qt. <a href='index.php?tab=Invenduti&orderby=p.stock_quantity&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=Invenduti&orderby=p.stock_quantity&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
			
			<th style='width:100px'>Ultimo movimento <a href='index.php?tab=Invenduti&orderby_u=ultimomovimento&orderway_u=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=Invenduti&orderby_u=ultimomovimento&orderway_u=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th></tr></thead><tbody>";
			
			
			if(Tools::getIsset('orderby_u') && (Tools::getValue('orderby_u') == 'ultimomovimento')) { 
				$ultimi_movimenti = array();
				foreach ($prodotti as $key => $row)
				{
					$ultimi_movimenti[$key] = Db::getInstance()->getValue('SELECT date_add FROM orders o JOIN order_detail od ON o.id_order = od.id_order WHERE od.product_id = '.$row['id_product'].' ORDER BY o.id_order DESC');		
				}
				if(Tools::getValue('orderway_u') == 'desc') 
					array_multisort($ultimi_movimenti, SORT_DESC, $prodotti);
				else if(Tools::getValue('orderway_u') == 'asc') 
					array_multisort($ultimi_movimenti, SORT_ASC, $prodotti);
					
			}
			
			$tokenProducts = Tools::getAdminToken('AdminCatalogExFeatures'.(int)Tab::getIdFromClassName('AdminCatalogExFeatures').(int)$cookie->id_employee);
			
			foreach($prodotti as $prodotto) 
			{
				$lista_prodotti .= "<tr>
				
				<td style='text-align:left'><span style='cursor:pointer' class='span-reference' title=\"".Product::showProductTooltip($prodotto['id_product'])."\"><a href='index.php?tab=AdminCatalogExFeatures&id_product=".$prodotto['id_product']."&updateproduct&token=".$tokenProducts."' target='_blank'>".$prodotto['reference']."</a></span></td>
				
				<td style='text-align:left'>".$prodotto['supplier_reference']."</td><td>".$prodotto['costruttore']."</td><td style='text-align:left'>".preg_replace('/"/', "", $prodotto['name'])."</td><td style='text-align:right'>".$prodotto['stock_quantity']."</td><td>".Tools::displayDate(Db::getInstance()->getValue('SELECT date_add FROM orders o JOIN order_detail od ON o.id_order = od.id_order WHERE od.product_id = '.$prodotto['id_product'].' ORDER BY o.id_order DESC'),5)."</td></tr>";
			}
			
			$lista_prodotti .= "</tbody></table></div><div id='bottom_anchor'>
			</div>";
			
			echo $lista_prodotti;
			
		}
		
		
}

