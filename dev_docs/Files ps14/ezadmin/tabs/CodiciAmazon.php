<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

class CodiciAmazon extends AdminPreferences
{


	public function display()
	{
		global $cookie;
		
		echo "<h1>Logistica Amazon</h1>";
		echo '<a class="button" style="display:block; text-align:center; width:450px" onclick="window.onbeforeunload = null" href="qta-logistica-amazon.php">Scarica file di confronto logistica Amazon-magazzino eSolver</a><br /><br />';
	
		echo '<a class="button" style="display:block; text-align:center; width:450px" onclick="window.onbeforeunload = null" href="template-amazon-prime.php">Scarica
		file con quantit&agrave; da inviare a Logistica Amazon</a>';
		
		echo "
		<form name='esporta-amazon' method='post'>
		<!--	Marca il flag per scegliere i campi da inserire nel foglio di Excel:<br />
		<input type='checkbox' checked='checked' name='fnsku' readonly='readonly' /> FNSKU (obbligatorio)<br />
		<input type='checkbox' checked='checked' name='asin' readonly='readonly' /> ASIN (obbligatorio)<br />
		<input type='checkbox' checked='checked' name='sku' readonly='readonly' /> SKU (obbligatorio)<br />
		<input type='checkbox' checked='checked' name='nome' readonly='readonly' /> Nome prodotto (obbligatorio)<br />
		
		<input type='checkbox' checked='checked' name='costruttore' />Costruttore<br />
		<input type='checkbox' checked='checked' name='prezzo_web' />Prezzo web<br />
		<input type='checkbox' name='prezzo_amazon'  />Prezzo Amazon Italia<br />
		<input type='checkbox' checked='checked' name='qt_ez' />Qt. Magazzino EZ<br />
		<input type='checkbox' checked='checked' name='qt_aln'  />Qt. Allnet<br />
		<input type='checkbox' checked='checked' name='qt_eds'  />Qt. EDS<br />
		<input type='checkbox' checked='checked' name='ord_ez'  />Ordinato EZ<br />
		-->
		<br /><br />
		<!-- <input onclick='window.onbeforeunload = null' type='submit' name='submitamazon' value='Esporta lista' /> -->
		
		</form>";
		
	}

}


