<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

class RecuperaCsv extends AdminPreferences
{


	public function display()
	{
		
		global $cookie;
		if(isset($_POST['submitcsv']) && $_POST['submitcsv'] == 'Recupera CSV') {
		
			$id_order = Tools::getValue('ordine_da_recuperare');
			
			Product::RecuperaCSV($id_order);

			echo "Il file CSV &egrave; stato recuperato con successo. Nel caso in cui il CSV non sia presente sul server significa che l'ordine &egrave; stato cancellato o ha avuto dei problemi. Grazie.<br /><br />
			<a href='index.php?tab=RecuperaCsv&token=".$this->token."'>Torna indietro</a>";
		
		
		
		}
		else if(isset($_POST['submitbdl']) && $_POST['submitbdl'] == 'Recupera CSV') {
		
			$id_bdl = $_POST['bdl_da_recuperare'];
			
			Product::RecuperaBDL($id_bdl);

			echo "Il file CSV &egrave; stato recuperato con successo. Nel caso in cui il CSV non sia presente sul server significa che l'ordine &egrave; stato cancellato o ha avuto dei problemi. Grazie.<br /><br />
			<a href='index.php?tab=RecuperaCsv&token=".$this->token."'>Torna indietro</a>";
		
		
		
		}
		else {
		
			echo "<h1>Recupero di file CSV</h1>
			<form name='recupera-csv' method='post'>
			Inserisci nel box qua sotto il numero dell'ordine per il quale vuoi recuperare il CSV e quindi clicca sul pulsante:<br />
			
			<input type='text' name='ordine_da_recuperare' />
			<br /><br />
			<input type='submit' name='submitcsv' value='Recupera CSV' />
			
			</form>";
			
			if($cookie->id_employee == 6 || $cookie->id_employee == 22)
			{
				echo "<br /><br />
				<form name='recupera-bdl' method='post'>
				Recupero BDL:<br />
				
				<input type='text' name='bdl_da_recuperare' />
				<br /><br />
				<input type='submit' name='submitbdl' value='Recupera CSV' />
				
				</form>";
				
			}
		}
		
		
		
	}

}


