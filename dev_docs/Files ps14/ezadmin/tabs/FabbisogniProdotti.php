<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14632 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class FabbisogniProdotti extends AdminTab
{
		public function display()
		{	
			global $cookie, $currentIndex;
			echo "<h1>Allocato</h1>";
		
			$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
			$tokenOrders = Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee);
			$tokenProducts = Tools::getAdminToken('AdminProducts'.(int)Tab::getIdFromClassName('AdminProducts').(int)$cookie->id_employee);
			
			
			if(isset($_POST['per_prodotto'])) {
						
				foreach ($_POST['per_prodotto'] as $prodotto) {

					$nomeprodotto = Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_lang = 5 AND id_product = ".$prodotto."");
							
					$prodottiinclusi.= $nomeprodotto."; ";
							
					$strisciaprodotti.= "- ".$nomeprodotto."<br />";
		
					$prodotti.= "p.id_product = $prodotto OR ";
						
				}
						
					$cookie->sql_prodotti_verifica = "AND ($prodotti p.id_product =99999999999999999)";
			}
			else if(isset($_POST['per_costruttore'])) {
						
				foreach ($_POST['per_costruttore'] as $costruttore) {

					$nomecostruttore = Db::getInstance()->getValue("SELECT name FROM manufacturer WHERE id_manufacturer = ".$costruttore."");
		
					$costruttori.= "p.id_manufacturer = $costruttore OR ";
						
				}
						
					$cookie->sql_prodotti_verifica = "AND ($costruttori p.id_manufacturer =99999999999999999)";
			}
			else if(isset($_POST['per_categoria'])) {
						
				foreach ($_POST['per_categoria'] as $categoria) {

					$nomecategoria = Db::getInstance()->getValue("SELECT name FROM category_lang WHERE id_lang = 5 AND id_category =  ".$categoria."");
		
					$categorie.= "p.id_category_default = $categoria OR ";
						
				}
						
					$cookie->sql_prodotti_verifica = "AND ($categorie p.id_category_default =99999999999999999)";
			}
			else {
				if(!isset($cookie->sql_prodotti_verifica)) {
					$cookie->sql_prodotti_verifica = "";
				}
				else {
					
				}
			}
			
			if(isset($_POST['resetta'])) {
				$cookie->sql_prodotti_verifica = "";
			
			}
				
			
			
			
			echo '<script type="text/javascript" src="../js/select2.js"></script>
			<script type="text/javascript">
			$(document).ready(function() { $("#per_prodotto").select2(); });
			$(document).ready(function() { $("#per_costruttore").select2(); });
			$(document).ready(function() { $("#per_categoria").select2(); });
			</script>
			<table>
			<tr>
			<td>
			<form name="cercaordini" method="post" action="index.php?tab=FabbisogniProdotti&token='.$this->token.$o.'">
			
			Cerca in base al prodotto:
			</td>
			<td>
			<select multiple id="per_prodotto" name="per_prodotto[]" style="width:500px">
			<option name="0" value="0">--- Scegli un prodotto ---</option>
			';
				
				
			$results = Db::getInstance()->ExecuteS("SELECT product.id_product, product.reference, product_lang.name FROM product JOIN product_lang ON product.id_product = product_lang.id_product WHERE product_lang.id_lang = 5 ORDER BY name");
				
			foreach ($results as $row) {
				
				echo "<option name='$row[id_product]' value='$row[id_product]'"; 
				echo ">$row[id_product] - $row[name] ($row[reference])</option>";
				
			}
					
			echo "</select></td><td>";
			
			echo "&nbsp;<input type='submit' name='cercagliordini' value='Cerca ordini' class='button' />
				</form></td></tr>";
				
			echo '<tr><td><form name="cercaordini2" method="post" action="index.php?tab=FabbisogniProdotti&token='.$this->token.$o.'">
				
			Cerca in base al costruttore:</td>
			<td>
			<select multiple id="per_costruttore" name="per_costruttore[]" style="width:500px">
			<option name="0" value="0">--- Scegli un costruttore ---</option>
			';
				
				
			$results = Db::getInstance()->ExecuteS("SELECT * FROM manufacturer ORDER BY name");
				
			foreach ($results as $row) {
				
				echo "<option name='$row[id_manufacturer]' value='$row[id_manufacturer]'"; 
				echo ">$row[id_manufacturer] - $row[name]</option>";
			
			}
					
			echo "</select></td><td>";
			
			echo "&nbsp;<input type='submit' name='cercagliordini' value='Cerca ordini' class='button' /></td></tr>";
			
			echo '<tr><td><form name="cercaordini3" method="post" action="index.php?tab=FabbisogniProdotti&token='.$this->token.$o.'">
				
			Cerca in base alla categoria:</td>
			<td>
			<select multiple id="per_categoria" name="per_categoria[]" style="width:500px">
			<option name="0" value="0">--- Scegli una categoria ---</option>
			';
				
				
			$results = Db::getInstance()->ExecuteS("SELECT c.id_category, cl.name FROM category c JOIN category_lang cl ON c.id_category = cl.id_category WHERE cl.id_lang = 5 ORDER BY cl.name");
				
			foreach ($results as $row) {
				
				echo "<option name='$row[id_category]' value='$row[id_category]'"; 
				echo ">$row[id_category] - $row[name]</option>";
				
			}
					
			echo "</select></td><td>";
			
			echo "&nbsp;<input type='submit' name='cercagliordini' value='Cerca ordini' class='button' /></td></tr></table><br />";
			
			//SQL per raggruppamento ordine
			$queryorders = 'SELECT od.id_order_detail, o.id_order, oh.date_add AS ultima_modifica, o.payment, o.id_customer, o.verificato, p.id_product, p.reference, p.stock_quantity as magazzino, pl.name AS nome_prodotto, p.supplier_quantity as allnet, p.esprinet_quantity esprinet, p.itancia_quantity itancia, p.ordinato_quantity as qt_ordinato, p.quantity as totale, m.name as costruttore, od.product_quantity AS ordinati, o.date_add, os.id_order_state, 
			(CASE c.is_company
				WHEN 0
				THEN CONCAT(c.firstname," ",c.lastname)
				WHEN 1
				THEN c.company
				ELSE
				CONCAT(c.firstname," ",c.lastname)
				END
				)as cliente
			FROM product p 
			
			LEFT JOIN product_lang pl ON p.id_product = pl.id_product LEFT JOIN manufacturer m ON p.id_manufacturer = m.id_manufacturer LEFT JOIN order_detail od ON od.product_id = p.id_product JOIN orders o ON o.id_order = od.id_order JOIN cart ca ON o.id_cart = ca.id_cart JOIN customer c ON o.id_customer = c.id_customer JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE
			'.(Tools::getIsset('pagamento') && Tools::getValue('pagamento') == 'accettato' ? ' (o.payment = "paypal" OR o.payment = "gestpay" OR (o.payment = "bankwire" AND os.id_order_state = 18)) AND ' : '').'
			'.(Tools::getIsset('pagamento') && Tools::getValue('pagamento') == 'anticipato' ? ' os.id_order_state = 10 AND ' : '').'
			
			'.(Tools::getIsset('pagamento') && Tools::getValue('pagamento') == 'contrassegno' ? ' o.payment = "cashondeliverywithfee" AND ' : '').'

			'.(Tools::getIsset('pagamento') && Tools::getValue('pagamento') == 'differito' ? ' os.id_order_state = 21 AND ' : '').'
			
			'.(Tools::getIsset('parziali') && Tools::getValue('parziali') == 's' ? ' os.id_order_state = 15 AND ' : '').'
			
			'.(Tools::getIsset('prime') && Tools::getValue('prime') == 's' ? '  o.id_cart IN (SELECT id_cart FROM cart WHERE name LIKE "%AMAZON PRIME%") AND ' : '').'
			
			'.(Tools::getIsset('astec') && Tools::getValue('astec') == 'ss' ? '(od.product_reference LIKE "%astec%" OR od.product_reference LIKE "%teleg%" OR od.product_reference LIKE "%inst%")  AND ' : '').' 
			
			'.(Tools::getIsset('astec') && Tools::getValue('astec') == 'n' ? 'od.product_reference NOT LIKE "%astec%" AND ' : '').' os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state != 20  AND os.id_order_state != 26  AND os.id_order_state != 29 AND os.id_order_state != 30 AND os.id_order_state != 31 AND os.id_order_state != 35 AND ca.riferimento NOT LIKE "%BDL%" AND pl.id_lang = 5 '.$cookie->sql_prodotti_verifica.'';
			$orders = Db::getInstance()->executeS($queryorders.' ORDER BY o.id_order ASC');
			
			$complete_orders_check = array();
			$totale_ordini = array();
			
			foreach($orders as $order) {
				
				$fatt = Db::getInstance()->getValue('SELECT rif_ordine FROM fattura WHERE TRIM(cod_articolo) = "'.trim($order['reference']).'" AND rif_ordine = "'.$order['id_order'].'"');
				
				
				if($fatt > 0 && $order['id_order_state'] != 15)
				{
					
				}
				else
				{
					if($order['id_order_state'] == 15)
					{	
						
						if($fatt > 0)
						{
							$qt_fatt = Db::getInstance()->getValue('SELECT SUM(qt_sped) FROM fattura WHERE TRIM(cod_articolo) = "'.trim($order['reference']).'" AND rif_ordine = "'.$order['id_order'].'"');
					
							$order['ordinati'] -= $qt_fatt;
						}
						else
						{
							$qt_fatt = Db::getInstance()->getValue('SELECT (product_quantity - (REPLACE(cons,"0_",""))) FROM order_detail od JOIN orders o ON o.id_order = od.id_order WHERE od.product_reference = "'.trim($order['reference']).'" AND o.id_order = '.$order['id_order']);
						
							$order['ordinati'] = $qt_fatt;
						}
					}					
					if(!in_array($order['id_order'], $totale_ordini))
						$totale_ordini[] = $order['id_order'];
				
					if(!isset($complete_orders_check[$order['reference']])) {
						$complete_orders_check[$order['reference']] = $order['magazzino'];
							
					}
					else {
					}
					$disponibili_per_ordine = $complete_orders_check[$order['reference']] - $order['ordinati'];

					
					if($disponibili_per_ordine < 0 && $complete_orders_check[$order['reference']] > 0) {
						
						$disponibili_per_ordine = $complete_orders_check[$order['reference']];
						$da_ordinare = $order['ordinati']-$complete_orders_check[$order['reference']];
						$complete_orders_check[$order['reference']] = 0;
							
					}
					else if($disponibili_per_ordine < 0 && $complete_orders_check[$order['reference']] == 0) {
						$disponibili_per_ordine = 0;
						$da_ordinare = $order['ordinati'];
						$complete_orders_check[$order['reference']] = 0;
					}
					
					else if($disponibili_per_ordine >= 0 && $complete_orders_check[$order['reference']] >= 0) {
						$disponibili_per_ordine = $order['ordinati'];
						$da_ordinare = 0;
						$complete_orders_check[$order['reference']] = $complete_orders_check[$order['reference']]-$disponibili_per_ordine;
							
					}
					else {
							
					}
					if($disponibili_per_ordine >= 0 && $disponibili_per_ordine >= $order['ordinati'] && (!isset($complete_orders_check[$order['id_order']]) || $complete_orders_check[$order['id_order']] != 0 )) {
						
						//echo $order['id_order'].'***'.$order['reference'].'***'.$disponibili_per_ordine.'***'.$order['ordinati'].'A1<br />';
						
						$complete_orders_check[$order['id_order']] = 1;
						
					}
					else {
						//echo $order['id_order'].'***'.$order['reference'].'***'.$disponibili_per_ordine.'***'.$order['ordinati'].'B0<br />';
						$complete_orders_check[$order['id_order']] = 0;
						//echo $order['id_order'].'***'.$disponibili_per_ordine.'***'.$order['ordinati'].'<br />';
					}
				}	
			}
			
			$ordini_completi = array_count_values($complete_orders_check);
			
				if(!isset($_GET['pronti'])) {
				echo "<a href='index.php?tab=FabbisogniProdotti&token=".$this->token."&pronti=s".(Tools::getIsset('astec') ? '&astec='.Tools::getValue('astec') : '')."' class='button'>Visualizza solo ordini pronti per la spedizione (Totale: ".$ordini_completi['1'].")</a>";
			}
			else {
				echo "<a href='index.php?tab=FabbisogniProdotti".(Tools::getIsset('astec') ? '&astec='.Tools::getValue('astec') : '')."&token=".$this->token."' class='button'>Visualizza tutti gli ordini (Totale: ".count($totale_ordini).")</a>
			";
			}
			echo "&nbsp;&nbsp;&nbsp;";

			if(!isset($_GET['astec'])) {
				echo "<a href='index.php?tab=FabbisogniProdotti&astec=n".(Tools::getIsset('pronti') ? '&pronti='.Tools::getValue('pronti') : '')."&token=".$this->token."&pronti=s' class='button'>Non visualizzare assistenze tecniche</a>
				
				&nbsp;&nbsp;&nbsp;<a href='index.php?tab=FabbisogniProdotti&astec=ss".(Tools::getIsset('pronti') ? '&pronti='.Tools::getValue('pronti') : '')."&token=".$this->token."&pronti=s' class='button'>Visualizza solo assistenze tecniche</a>
				
				&nbsp;&nbsp;&nbsp;<a href='index.php?tab=FabbisogniProdotti".(Tools::getIsset('pronti') ? '&pronti='.Tools::getValue('pronti') : '')."".(Tools::getIsset('parziali') ? '&parziali='.Tools::getValue('parziali') : '')."&token=".$this->token."&parziali=s' class='button'>Solo parziali</a>
				
				&nbsp;&nbsp;&nbsp;<a href='index.php?tab=FabbisogniProdotti".(Tools::getIsset('pronti') ? '&pronti='.Tools::getValue('pronti') : '')."".(Tools::getIsset('prime') ? '&prime='.Tools::getValue('prime') : '')."&token=".$this->token."&prime=s' class='button'>Solo prime</a>
				<br />
			<br />";
			}
			else {
				echo "<a href='index.php?tab=FabbisogniProdotti".(Tools::getIsset('pronti') ? '&pronti='.Tools::getValue('pronti') : '')."&token=".$this->token."' class='button'>Visualizza anche assistenze tecniche</a>
			";
			}
			echo "";
		
			if($_GET['pronti'] == 's')
			{
			echo '';
			echo "<a href='index.php?tab=FabbisogniProdotti&token=".$this->token."&pronti=s&pagamento=accettato".(Tools::getIsset('astec') ? '&astec='.Tools::getValue('astec') : '')."' class='button'>Ordini con pagamento accettato</a>&nbsp;&nbsp;&nbsp;";
			
			echo "<a href='index.php?tab=FabbisogniProdotti&token=".$this->token."&pronti=s&pagamento=anticipato".(Tools::getIsset('astec') ? '&astec='.Tools::getValue('astec') : '')."' class='button'>Ordini con bonifico anticipato</a>&nbsp;&nbsp;&nbsp;";
			
			echo "<a href='index.php?tab=FabbisogniProdotti&token=".$this->token."&pronti=s&pagamento=contrassegno".(Tools::getIsset('astec') ? '&astec='.Tools::getValue('astec') : '')."' class='button'>Ordini con contrassegno</a>&nbsp;&nbsp;&nbsp;";
		
			echo "<a href='index.php?tab=FabbisogniProdotti&token=".$this->token."&pronti=s&pagamento=differito".(Tools::getIsset('astec') ? '&astec='.Tools::getValue('astec') : '')."' class='button'>Ordini con pagamento differito</a><br /><br />";
			}
			
			echo "
			<input type='submit' name='resetta' value='Resetta' class='button' style='margin-top:-2px; height:22px'  />
			</form><br /><br />NB: gli ordini marcati in <strong>grassetto</strong> sono quelli che aspettano al spedizione da pi&ugrave; di due giorni.<br /><br />";
			
			
			
			
			echo "
			<style type='text/css'>
			
			#tableVerificaProdotti td {
			color:#000;
			}
			

			</style>";
			
			$o = '';
			
			if(isset($_GET['pronti'])) {
				
				echo '<strong>ATTENZIONE: stai visualizzando SOLO gli ordini pronti</strong><br />';
				$o .= "&pronti=s";
			}	
			
			if(isset($_GET['parziali'])) {
				$o .= "&parziali=s";
			}
			
			if(isset($_GET['prime'])) {
				$o .= "&prime=s";
			}
			
			if(isset($_GET['astec'])) {
				$o .= "&astec=ss";
			}
			
			if(isset($_GET['pagamento'])) {
				$o .= "&pagamento=".Tools::getValue('pagamento');
			}
			
			echo "<script type='text/javascript' src='../js/jquery/jquery.fixheadertable.js'></script>
			<table class='table' id='tableVerificaProdotti'>
			<thead class='persist-header'>
			<tr>";
	
			echo "
			<th></th>
			<th style='width:90px'>Codice<br /><br /> <a href='index.php?tab=FabbisogniProdotti&orderby=p.reference&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=FabbisogniProdotti&orderby=p.reference&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
			
			<th>Fatt?</th>
			
			<th style='width:220px'>Prodotto<br /><br /> <a href='index.php?tab=FabbisogniProdotti&orderby=nome_prodotto&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=FabbisogniProdotti&orderby=nome_prodotto&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
			
			<th style='width:80px'>Costruttore<br /><br /> <a href='index.php?tab=FabbisogniProdotti&orderby=costruttore&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=FabbisogniProdotti&orderby=costruttore&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
			
			
			
			echo "<th style='width:20px'>Qta. ord.</th>";
			
			
				
			echo "<th style='width:160px'>Cliente<br /><a href='index.php?tab=FabbisogniProdotti&orderby=cliente&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
				<a href='index.php?tab=FabbisogniProdotti&orderby=cliente&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a>
				</th>";
			
			echo "<th style='width:40px'>ID <br />ordine<br /><a href='index.php?tab=FabbisogniProdotti&orderby=o.id_order&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
				<a href='index.php?tab=FabbisogniProdotti&orderby=o.id_order&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a>
				</th>";
				
			echo "<th style='width:40px'>Data ord.</th>";
			echo "<th style='width:40px'>Ultima mod.</th>";
			
			echo "<th style='width:80px'>Pagamento</th>";
				
			echo "<th style='width:80px'>Stato</th>";
				
			echo "<th style='width:20px'>Qt x cli.</th>";
				
			echo "<th style='width:40px'>Mag.<br /> EZ  <br /><a href='index.php?tab=FabbisogniProdotti&orderby=magazzino&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
				<a href='index.php?tab=FabbisogniProdotti&orderby=magazzino&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
			
			echo "<th style='width:20px'>Verif.</th>"; 
			
			echo "</tr><thead>";
			
			echo "<tbody>";
			
			$tokenCatalog = Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee));
			$matchingdisponibili = array();
			
			$orders = Db::getInstance()->executeS($queryorders.' ORDER BY '.(isset($_GET['orderby']) ? $_GET['orderby'] : 'o.id_order').' '.(isset($_GET['orderway']) ? $_GET['orderway'] : 'ASC').', o.id_order ASC');
			
			$n = 0;
			$id_order = 0;
			foreach ($orders as $order) {
				
				$go = '';
				
				$ver = Db::getInstance()->getValue('SELECT id_order FROM order_history WHERE id_order = '.$order['id_order'].' AND (id_order_state = 4 OR id_order_state = 5 OR id_order_state = 6  OR id_order_state = 35 OR id_order_state = 7 OR id_order_state = 8 OR id_order_state = 14 OR id_order_state = 16 OR id_order_state = 20 OR id_order_state = 26 OR id_order_state = 29 OR id_order_state = 31)');
	
					if($ver > 0)
						$go = 'n';
					
				$fatturato = 'n';
				$fatt = Db::getInstance()->getValue('SELECT rif_ordine FROM fattura WHERE TRIM(cod_articolo) = "'.trim($order['reference']).'" AND rif_ordine = "'.$order['id_order'].'"');
				if($fatt > 0 && $order['id_order_state'] != 15)
				{
					$fatturato = 'y';
				}
				else
				{ 
					
					
					
					if($order['id_order_state'] == 15)
					{	
						if($fatt > 0)
						{
							$fatturato = 'y';
							$qt_fatt = Db::getInstance()->getValue('SELECT SUM(qt_sped) FROM fattura WHERE TRIM(cod_articolo) = "'.trim($order['reference']).'" AND rif_ordine = "'.$order['id_order'].'"');
					
							$order['ordinati'] -= $qt_fatt;
						}
						else
						{
							$qt_fatt = Db::getInstance()->getValue('SELECT (product_quantity - (REPLACE(cons,"0_",""))) FROM order_detail od JOIN orders o ON o.id_order = od.id_order WHERE od.product_reference = "'.trim($order['reference']).'" AND o.id_order = '.$order['id_order']);
						
							$order['ordinati'] = $qt_fatt;
						}
							
						
					}
					
					
	
					
					if($order['ordinati'] > 0 && $go == '')
					{	
				
						if($order['id_order'] != $id_order)
						{
							$n++;
						}
						$id_order = $order['id_order'];
						
						if($complete_orders_check[$order['id_order']] == 1) {
							echo "<tr style='background-color:#d2f6d6'>";
						}
						else {
							echo "<tr ".(isset($_GET['pronti']) ? "style='display:none'" : "").">";
						}
						
				
						echo "
						<td style='text-align:right'>".$n."</td>
						<td>".'<span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($order['id_product']).'"><a href="index.php?tab=AdminCatalog&id_product='.$order['id_product'].'&updateproduct&token='.$tokenCatalog.'">'.$order['reference']."</a></span></td>
						
						<td>".($fatturato == 'y' ? '<img src="../img/admin/enabled.gif" alt="si" title="si" />' : '')."</td>
						
						<td>".$order['nome_prodotto']."</td>
						
						<td>".$order['costruttore']."</td>";

						if(!isset($matchingdisponibili[$order['reference']])) {
							$matchingdisponibili[$order['reference']] = $order['magazzino'];
								
						}
						else {
						}
						$disponibili_per_ordine = $matchingdisponibili[$order['reference']] - $order['ordinati'];
						if($disponibili_per_ordine < 0 && $matchingdisponibili[$order['reference']] > 0) {
							$disponibili_per_ordine = $matchingdisponibili[$order['reference']];
							$da_ordinare = $order['ordinati']-$matchingdisponibili[$order['reference']];
							$matchingdisponibili[$order['reference']] = 0;
						}
						else if($disponibili_per_ordine < 0 && $matchingdisponibili[$order['reference']] == 0) {
							$disponibili_per_ordine = 0;
							$da_ordinare = $order['ordinati'];
							$matchingdisponibili[$order['reference']] = 0;
						}
						else if($disponibili_per_ordine >= 0 && $matchingdisponibili[$order['reference']] >= 0) {
							$disponibili_per_ordine = $order['ordinati'];
							$da_ordinare = 0;
							$matchingdisponibili[$order['reference']] = $matchingdisponibili[$order['reference']]-$disponibili_per_ordine;
						}
						else {
							$disponibili_per_ordine = "XXX";
						}
							
						if(Tools::datediff('G', $order['date_add'],date('Y-m-d H:i:s')) > 2)
							$morethan2 = 1;
						else
							$morethan2 = 0;
							
						echo "<td style='text-align:right'>".$order['ordinati']." </td>";
						echo "<td>".$order['cliente']."</td>";
						echo '<td><a href="?tab=AdminCustomers&id_customer='.$order['id_customer'].'&viewcustomer&id_order='.$order['id_order'].'&vieworder&token='.$tokenCustomers.'&tab-container-1=4" target="_blank">'.($morethan2 == 1 ? '<strong>' : '').$order['id_order'].($morethan2 == 1 ? '</strong>' : '').'</a></td>';
						echo "<td>".date('d/m/y', strtotime($order['date_add']))."</td>";
						echo "<td>".date('d/m/y', strtotime($order['ultima_modifica']))."</td>";
						
						$ast = Db::getInstance()->getRow('SELECT am.id_action, am.id_action_message, a.status, am.action_message, a.action_to, a.date_upd FROM action_message am JOIN action_thread a ON am.id_action = a.id_action WHERE (a.status = "closed" OR a.status = "pending1" OR a.status = "pending2") AND (am.action_message LIKE "%<strong>*** ASSISTENZA TECNICA ***</strong><br /><br />%" AND am.action_message LIKE "%'.$order['id_order'].')%") GROUP BY a.id_action');
	
						$pagamento = $order['payment'];
						$ast_status = '';
						if($ast['id_action'] > 0)	
						{
							$employee = Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$ast['action_to']);
							if($ast['status'] == 'closed')
								$ast_status = '<em>Status assistenza tecnica</em>: <strong>chiuso</strong>';
							else if($ast['status'] == 'open')
								$ast_status = '<em>Status assistenza tecnica</em>: <strong>aperto</strong>';
							else
								$ast_status = '<em>Status assistenza tecnica</em>: <strong>in lavorazione</strong>';
							
							$ast_status .= '<br /><em>In carico a</em>: <strong>'.$employee.'</strong>';
							
							
						}

						echo "<td>".$pagamento."</td>";
						echo "<td>".Db::getInstance()->getValue('SELECT name FROM order_state_lang WHERE id_lang = 5 AND id_order_state = '.$order['id_order_state']).($ast_status != '' ? '<br />'.$ast_status : '')."</td>";
						echo "<td style='text-align:right'>".$disponibili_per_ordine."</td>";
						echo "<td style='text-align:right'>".$order['magazzino']."</td>";
						
						
						echo "<td style='width:20px'>
						
						<script type='text/javascript'>
							function checkVerificato(id_order, mode)
							{
								if(mode == 'on')
								{	
									$('input[name=\"order_'+id_order+'\"]').prop('checked', true);
									var updverificato = 'y';
								}
								else
								{
									$('input[name=\"order_'+id_order+'\"]').prop('checked', false);
									var updverificato = 'n';
								}
								
								$.ajax({
								  url:'ajax.php?updVerificatoOrder='+updverificato,
								  type: 'POST',
								  data: { id_order: id_order
								  },
								  success:function(r){
									alert('Salvato');
								  },
								  error: function(xhr,stato,errori){
									 alert('Errore durante l\'operazione:'+xhr.status);
								  }
								});
								
							}
						</script>
						
						
						<input id='id_order_detail_".$order['id_order_detail']."' name='order_".$order['id_order']."' type='checkbox' onclick='
						if(this.checked == true) { checkVerificato(".$order['id_order'].", \"on\"); } else { checkVerificato(".$order['id_order'].", \"off\"); }' ".($order['verificato'] == 1 ? "checked='checked'" : "")."/></td>";
						echo "</tr>";	
						
					}
				}
			}
				
				
			
			
			echo "</tbody>";
			echo "</table>";
		
			echo "<br /><form method='post' action=''>";
				
				
			echo "<input type='submit' value='Esporta i dati in formato Excel' name='esportaexcel' class='button' onclick='window.onbeforeunload = null' />
			<input type='hidden' name='querysql' value='".stripslashes($queryorders)."' />		
			</form>
			";
			echo "<br /><br />";
		
			if(isset($_POST['esportaexcel'])) { 
				ini_set("memory_limit","892M");
				set_time_limit(3600);
				require_once 'esportazione-catalogo/Classes/PHPExcel.php';
				$objPHPExcel = new PHPExcel();
				$results = Db::getInstance()->ExecuteS(stripslashes($_POST['querysql'])) or die(mysql_error());
				
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'Codice')
					->setCellValue('B1', 'Prodotto')
					->setCellValue('C1', 'Costruttore')
					->setCellValue('D1', 'Qta ord.')
					->setCellValue('E1', 'Cliente')
					->setCellValue('F1', 'ID ordine')
					->setCellValue('G1', 'Quanti per cliente')
					->setCellValue('H1', 'Mag. EZ')
				;
				$objPHPExcel->getActiveSheet()->getStyle("F")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				
				$objPHPExcel->getActiveSheet()->getStyle("A")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$i = 2;
				$matchingdisponibilixls = array();
				$complete_orders_check_xls = array();
				foreach ($results as $row) {
				
					if(!isset($complete_orders_check_xls[$row['reference']])) {
						$complete_orders_check_xls[$row['reference']] = $row['magazzino'];
								
					}
					else {
					}
					$disponibili_per_ordine = $complete_orders_check_xls[$row['reference']] - $row['ordinati'];
					if($disponibili_per_ordine < 0 && $complete_orders_check_xls[$row['reference']] > 0) {
						$disponibili_per_ordine = $complete_orders_check_xls[$row['reference']];
						$da_ordinare = $row['ordinati']-$complete_orders_check_xls[$row['reference']];
						$complete_orders_check_xls[$row['reference']] = 0;
								
					}
					else if($disponibili_per_ordine < 0 && $complete_orders_check_xls[$row['reference']] == 0) {
						$disponibili_per_ordine = 0;
						$da_ordinare = $row['ordinati'];
						$complete_orders_check_xls[$row['reference']] = 0;
					}
					else if($disponibili_per_ordine >= 0 && $complete_orders_check_xls[$row['reference']] >= 0) {
						$disponibili_per_ordine = $row['ordinati'];
						$da_ordinare = 0;
						$complete_orders_check_xls[$row['reference']] = $complete_orders_check_xls[$row['reference']]-$disponibili_per_ordine;
								
					}
					else {
								
					}
					if($disponibili_per_ordine > 0 && $disponibili_per_ordine >= $row['ordinati'] && (!isset($complete_orders_check_xls[$row['id_order']]) || $complete_orders_check_xls[$row['id_order']] != 0 )) {
						$complete_orders_check_xls[$row['id_order']] = 1;
					}
					else {
						$complete_orders_check_xls[$row['id_order']] = 0;
					}
							
					
				}
				
				foreach ($results as $row) {
					
					$fatt = Db::getInstance()->getValue('SELECT rif_ordine FROM fattura WHERE TRIM(cod_articolo) = "'.trim($row['reference']).'" AND rif_ordine = "'.$row['id_order'].'"');
					if($fatt > 0)
					{
					}
				
					else {
						
						if(!isset($matchingdisponibilixls[$row['reference']])) {
						$matchingdisponibilixls[$row['reference']] = $row['magazzino'];
								
						}
						else {
						}
						$disponibili_per_ordine = $matchingdisponibilixls[$row['reference']] - $row['ordinati'];
						if($disponibili_per_ordine < 0 && $matchingdisponibilixls[$row['reference']] > 0) {
							$disponibili_per_ordine = $matchingdisponibilixls[$row['reference']];
							$da_ordinare = $row['ordinati']-$matchingdisponibilixls[$row['reference']];
							$matchingdisponibilixls[$row['reference']] = 0;
						}
						else if($disponibili_per_ordine < 0 && $matchingdisponibilixls[$row['reference']] == 0) {
							$disponibili_per_ordine = 0;
							$da_ordinare = $row['ordinati'];
							$matchingdisponibilixls[$row['reference']] = 0;
						}
						else if($disponibili_per_ordine >= 0 && $matchingdisponibilixls[$row['reference']] >= 0) {
							$disponibili_per_ordine = $row['ordinati'];
							$da_ordinare = 0;
							$matchingdisponibilixls[$row['reference']] = $matchingdisponibilixls[$row['reference']]-$disponibili_per_ordine;
						}
						else {
							$disponibili_per_ordine = "XXX";
						}
					
						$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue("A$i", $row['reference'])
						->setCellValue("B$i", $row['nome_prodotto'])
						->setCellValue("C$i", $row['costruttore'])
						->setCellValue("D$i", $row['ordinati'])
						->setCellValue("E$i", $row['cliente'])
						->setCellValue("F$i", $row['id_order'])
						->setCellValue("G$i", $disponibili_per_ordine)
						->setCellValue("H$i", $row['magazzino'])
							;
						$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
						$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(70);
						$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
						$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
						$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
						$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
						$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
						$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
						
						
						
						if ($complete_orders_check_xls[$row['id_order']] == 1) {
							$objPHPExcel->getActiveSheet()->getStyle("A$i:H$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	

							$objPHPExcel->getActiveSheet()->getStyle("A$i:H$i")->getFill()->getStartColor()->setRGB('D2F6D6');	
						}
						else {
						
						}
						$i++;
					}
				}
				$objPHPExcel->getActiveSheet()->setTitle('Fabbisogni');

				$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	

				$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFill()->getStartColor()->setRGB('FFFF00');	

				$objPHPExcel->getActiveSheet()->getStyle("A1:H$i")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				
				$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true);
				
				$objPHPExcel->getActiveSheet()->getStyle("F2:F$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->getActiveSheet()->getStyle("G2:G$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->getActiveSheet()->getStyle("H2:H$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				
				$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
				$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
				PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

				$data = date("Ymd");

				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->setPreCalculateFormulas(false);
				$objWriter->save(str_replace(".php", ".xls", "esportazione-catalogo/catalogo_xls/fabbisogni-$data.php"));

				$this->_html .= "<div style='visibility:hidden'><br /><br />FILE PRONTO PER IL DOWNLOAD: <a id='scaricaexcelstatistiche' href='http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/fabbisogni-$data.xls' onclick='window.onbeforeunload = null'>CLICCA QUI PER SCARICARE</a>!</div>";

				header("Location: http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/fabbisogni-$data.xls");
					
			}
		}
		
		
}

