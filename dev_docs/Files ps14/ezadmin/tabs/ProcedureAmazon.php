<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license	http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class ProcedureAmazon extends AdminTab
{
	public function bindDatepickerZ($id, $time)
	{
		if ($time)
			echo '
			var dateObj = new Date();
			var hours = dateObj.getHours();
			var mins = dateObj.getMinutes();
			var secs = dateObj.getSeconds();
			if (hours < 10) { hours = "0" + hours; }
			if (mins < 10) { mins = "0" + mins; }
			if (secs < 10) { secs = "0" + secs; }
			var time = " "+hours+":"+mins+":"+secs;';

		echo '
		$(function() {
			$("#'.Tools::htmlentitiesUTF8($id).'").datepicker({
				prevText:"",
				nextText:"",
				dateFormat:"dd-mm-yy"});
		});';
	}

	// id can be a identifier or an array of identifiers
	public function includeDatepickerZ($id, $time = false)
	{
		global $cookie;
		echo '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery-ui-1.8.10.custom.min.js"></script>';
		$iso = Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'lang WHERE `id_lang` = '.(int)$cookie->id_lang);
		if ($iso != 'en')
			echo '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/datepicker/ui/i18n/ui.datepicker-'.Tools::htmlentitiesUTF8($iso).'.js"></script>';
		echo '<script type="text/javascript">';
			if (is_array($id))
				foreach ($id as $id2)
					$this->bindDatepickerZ($id2, $time);
			else
				$this->bindDatepickerZ($id, $time);
		echo '</script>';
	}

	
	public function display()
	{
		global $cookie, $currentIndex;
		
		$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
		$this->includeDatepickerZ(array('arco_dal', 'arco_al'), true);
		echo '<h1>Strumenti per Amazon</h1>
		
		<script type="text/javascript" src="https://www.ezdirect.it/js/sweetalert2.js"></script>
		<link rel="stylesheet" href="https://www.ezdirect.it/css/sweetalert2.css" />
			
		<script type="text/javascript">
		
		$(document).ready(function(){
			 $("#aggiornamento").on("click",function(e){
				 e.preventDefault();
				var valid = false;
				swal({
				  title: \'Sei sicuro/a?\',
				  text: \'La pagina si ricaricherà. La procedura durerà alcuni minuti.\',
				  type: \'warning\',
				  showCancelButton: true,
				  confirmButtonColor: \'#6A9944\',
				  cancelButtonColor: \'#d33\',
				  confirmButtonText: \'S&igrave;, voglio continuare!\'
				}).then(function () {
					window.location = $("#aggiornamento").attr("href");
				});
				
			});
			
			$("#prodotti").on("click",function(e){
				 e.preventDefault();
				var valid = false;
				swal({
				  title: \'Sei sicuro/a?\',
				  text: \'La pagina si ricaricherà. La procedura durerà alcuni minuti.\',
				  type: \'warning\',
				  showCancelButton: true,
				  confirmButtonColor: \'#6A9944\',
				  cancelButtonColor: \'#d33\',
				  confirmButtonText: \'S&igrave;, voglio continuare!\'
				}).then(function () {
					window.location = $("#prodotti").attr("href");
				});
				
			});
		});
		</script>
		In questa pagina hai a disposizione diversi strumenti per gestire Amazon. <strong>ATTENZIONE</strong>: Amazon consente di fare solo un numero limitato di operazioni in un\'ora. Se viene raggiunto il limite, le procedure mostreranno messaggi d\'errore.<br /><br />
		
		<a id="aggiornamento" style="display:block; width:350px" href="'.$currentIndex.'&token='.$this->token.'&submitfeed=y" class="button">Invia/Aggiorna prodotti, prezzi e quantit&agrave; (procedura unica)</a><br />
		
		<a id="ordini" style="display:block; width:350px" href="'.$currentIndex.'&token='.$this->token.'&listorders=y" class="button">Importazione ordini</a><br />
		
		<a class="button" style="display:block; width:350px" onclick="window.onbeforeunload = null" href="carrelli-amazon.php">Scarica
		file carrelli preordinati</a>
		<br />
		<a id="aggiornamento" style="display:block; width:350px" href="'.$currentIndex.'&token='.$this->token.'&submitfattura=y" class="button">Carica fattura</a><br />
		
		<!-- <a id="prodotti" style="display:block; width:350px" href="'.$currentIndex.'&token='.$this->token.'&amazonfeed=y" class="button">Aggiorna strumento disponibilit&agrave; dei marketplace</a><br /> -->';
		
		if(Tools::getIsset('submitfeed'))
		{
			echo 'Attendere il caricamento nel riquadro sottostante finch&egrave; non compaiono i messaggi di conferma. La procedura impiega tipicamente 2 minuti circa<br /><br />';
			echo '<iframe style="width:100%; height: 450px" src="procedure-amazon.php?prodotti=y&submitfrominst=y"></iframe>';
			
		}
		
		if(Tools::getIsset('submitfattura'))
		{
			echo '<iframe style="width:100%; height: 450px" src="https://www.ezdirect.it/docs/disponibilita/invio_fattura_amazon_post.php"></iframe>';
			
		}
		
		if(Tools::getIsset('amazonfeed'))
		{
			echo 'Attendere il caricamento nel riquadro sottostante finch&egrave; non compaiono i messaggi di conferma. La procedura impiega tipicamente 3-4 minuti circa<br /><br />';
			echo '<iframe style="width:100%; height: 450px" src="procedure-amazon.php?report=y&submitfrominst=y"></iframe>';
			
		}
		
		if(Tools::getIsset('listorders'))
		{
			if(!Tools::getIsset('importa'))
			{
				echo '<form method="post" action="'.$currentIndex.'&token='.$this->token.'&listorders=y">';
				echo "Seleziona l'intervallo temporale d'importazione degli ordini:<br /><br />";
				echo 'Dal <input type="text" id="arco_dal" name="arco_dal"  /> al <input type="text" id="arco_al" name="arco_al"  />';
				echo '<input type="submit" name="importa" value="Importa ordini" />';
				echo '</form>';
			}
			else
			{
				$data_dal_a = explode("-", $_POST['arco_dal']);
				$data_dal = $data_dal_a[2]."-".$data_dal_a[1]."-".$data_dal_a[0]; 
				
				$data_al_a = explode("-", $_POST['arco_al']);
				$data_al = $data_al_a[2]."-".$data_al_a[1]."-".$data_al_a[0]; 
				
				echo 'Attendere il caricamento nel riquadro sottostante finch&egrave; non compaiono i messaggi di conferma. La procedura impiega pi&ugrave; tempo a seconda della lunghezza del periodo scelto.<br /><br />';
				
				echo '<iframe style="width:100%; height: 450px" src="https://www.ezdirect.it/docs/disponibilita/MarketplaceWebServiceOrders/Samples/ListOrdersSample.php?ordini=y&submitfrominst=y&date_from='.$data_dal.'&date_to='.$data_al.'"></iframe>';
				
			}
		}
	}
}

