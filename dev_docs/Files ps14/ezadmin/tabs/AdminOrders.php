<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14632 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminOrders extends AdminTab
{
	public function __construct()
	{
		global $cookie;
		$modalita_esolver = Db::getInstance()->getValue('SELECT value FROM configuration WHERE name = "MODALITA_ESOLVER"');
	 	$this->table = 'order';
	 	$this->className = 'Order';
	 	$this->view = true;
		$this->delete = true;
		$this->colorOnBackground = true;
	 	$this->_select = '
			a.id_order AS id_pdf,
			
			(CASE c.is_company
			WHEN 0
			THEN CONCAT(c.firstname," ",c.lastname)
			WHEN 1
			THEN c.company
			ELSE
			CONCAT(c.firstname," ",c.lastname)
			END
			)as customer,

			(CASE WHEN ca.preventivo = 0
			THEN "--"
			WHEN ca.preventivo = ""
			THEN "--"
			WHEN ca.preventivo IS NULL
			THEN "--"
			ELSE
			a.id_cart
			END) AS `preventivo`,

			(CASE WHEN a.id_cart IN (SELECT id_cart FROM cart WHERE name LIKE "%AMAZON PRIME%") THEN "Prime"
			ELSE a.payment
			END) AS paymentz,
			
			c.`company` AS `company`,
			c.`id_default_group` AS `group`,
			osl.`name` AS `osname`,
			os.`color`,
			IF((SELECT COUNT(so.id_order) FROM `'._DB_PREFIX_.'orders` so WHERE so.id_customer = a.id_customer) > 1, 0, 1) as new,
			(SELECT COUNT(od.`id_order`) FROM `'._DB_PREFIX_.'order_detail` od WHERE od.`id_order` = a.`id_order` GROUP BY `id_order`) AS product_number
		';
	 	
		$this->_join = 'LEFT JOIN `'._DB_PREFIX_.'customer` c ON (c.`id_customer` = a.`id_customer`)
			LEFT JOIN `'._DB_PREFIX_.'order_history` oh ON (oh.`id_order` = a.`id_order`)
			LEFT JOIN `'._DB_PREFIX_.'cart` ca ON (ca.`id_cart` = a.`id_cart`)
			LEFT JOIN `'._DB_PREFIX_.'order_state` os ON (os.`id_order_state` = oh.`id_order_state`)
			LEFT JOIN `'._DB_PREFIX_.'order_state_lang` osl ON (os.`id_order_state` = osl.`id_order_state` AND osl.`id_lang` = '.(int)($cookie->id_lang).')
		';
		
		
		if(isset($_POST['cercaordineprodotto'])) {
			
			$cookie->_whereorder = 'AND a.id_order IN (SELECT id_order FROM order_detail WHERE product_id = "'.$_POST['cercaordineprodotto'].'") AND oh.`id_order_history` = (SELECT MAX(`id_order_history`) FROM `'._DB_PREFIX_.'order_history` moh WHERE moh.`id_order` = a.`id_order` AND (moh.id_order_state != 27 AND moh.id_order_state != 28) GROUP BY moh.`id_order`)';
			$cookie->_cercaordineprodotto = $_POST['cercaordineprodotto'];
		}
		
		if(Tools::getIsset('cercaordineprodotto') || Tools::getIsset('cercatestoprodottoordine') || Tools::getIsset('online')  || Tools::getIsset('offline')  || Tools::getIsset('old') || Tools::getIsset('auto_marca') || Tools::getIsset('auto_serie') || Tools::getIsset('auto_categoria') || Tools::getIsset('auto_fornitore')) {
			$cookie->_whereorder = '';
			
			$filters = $cookie->getFamily($this->table.'Filter_');
			foreach ($filters AS $cookieKey => $filter)
				if (strncmp($cookieKey, $this->table.'Filter_', 7 + Tools::strlen($this->table)) == 0)
				{
					$key = substr($cookieKey, 7 + Tools::strlen($this->table));
					
					$tmpTab = explode('!', $key);
					$key = (count($tmpTab) > 1 ? $tmpTab[1] : $tmpTab[0]);
					if (array_key_exists($key, $this->fieldsDisplay))
						unset($cookie->$cookieKey);
				}
			if (isset($cookie->{'submitFilter'.$this->table}))
				unset($cookie->{'submitFilter'.$this->table});
			if (isset($cookie->{$this->table.'Orderby'}))
				unset($cookie->{$this->table.'Orderby'});
			if (isset($cookie->{$this->table.'Orderway'}))
				unset($cookie->{$this->table.'Orderway'});
			//togliere tutta la parte sopra quando non andrà più bene la ricerca solo prodotti...
			
			if(Tools::getIsset('cercaordineprodotto'))
			{
				if(Tools::getValue('cercaordineprodotto') != 0)
				{
					
					$cookie->_whereorder = 'AND a.id_order IN (SELECT id_order FROM order_detail WHERE product_id = "'.$_POST['cercaordineprodotto'].'") AND oh.`id_order_history` = (SELECT MAX(`id_order_history`) FROM `'._DB_PREFIX_.'order_history` moh WHERE moh.`id_order` = a.`id_order` AND (moh.id_order_state != 27 AND moh.id_order_state != 28) GROUP BY moh.`id_order`)';
					$cookie->_cercaordineprodotto = $_POST['cercaordineprodotto'];
				}	
			}
			
			
			if(Tools::getIsset('cercatestoprodottoordine') && Tools::getValue('cercatestoprodottoordine') != '')
			{
				$cercatestoprodotto_sql = "";
				
				$cercatestoprodotto_sql .= ' AND a.id_order IN (SELECT id_order FROM order_detail od JOIN product p ON od.product_id = p.id_product JOIN product_lang pl ON pl.id_product = od.product_id WHERE pl.id_lang = 5 AND ( ';
				
				if(preg_match("/ or /", Tools::getValue('cercatestoprodottoordine'))) {
					
						$words = explode(' or ', Tools::getValue('cercatestoprodottoordine'));
				
						foreach ($words AS $key => $word) {
							if (!empty($word))
							{
								$cercatestoprodotto_sql .= 'p.reference LIKE "%'.$word.'%" OR p.supplier_reference LIKE "%'.$word.'%" OR pl.name LIKE  "%'.$word.'%" OR ';
							}
						}	
						
					
					$cercatestoprodotto_sql .= 'p.reference LIKE "%999999999%")';
				
					$ccercacarrelloprodotto = Tools::getValue('cercaordineprodotto');
				}
				else if(preg_match("/ and /", Tools::getValue('cercatestoprodottoordine'))) 
				{
					$words = explode(' and ', Tools::getValue('cercatestoprodottoordine'));
				
						foreach ($words AS $key => $word) {
							if (!empty($word))
							{
								$cercatestoprodotto_sql .= '(p.reference LIKE "%'.$word.'%" OR p.supplier_reference LIKE "%'.$word.'%" OR pl.name LIKE  "%'.$word.'%") AND ';
							}
						}	
						
						
				
					
					
					$cercatestoprodotto_sql .= 'p.reference != "")' ;
				
					$cercacarrelloprodotto = Tools::getValue('cercaordineprodotto');
					
				}
				
				else
				{
					$cercatestoprodotto_sql .= ' p.reference LIKE "%'.Tools::getValue('cercatestoprodottoordine').'%" OR p.supplier_reference LIKE "%'.Tools::getValue('cercatestoprodottoordine').'%" OR pl.name LIKE  "%'.Tools::getValue('cercatestoprodottoordine').'%") ';
				}
			
				$cercatestoprodotto_sql .= ') AND a.id_customer != 0 AND oh.`id_order_history` = (SELECT MAX(`id_order_history`) FROM `'._DB_PREFIX_.'order_history` moh WHERE moh.`id_order` = a.`id_order` AND (moh.id_order_state != 27 AND moh.id_order_state != 28) GROUP BY moh.`id_order`)';
				$cookie->_cercatestoprodottoordine = Tools::getValue('cercatestoprodottoordine');

				$cookie->_whereorder .= $cercatestoprodotto_sql;
				
			}
		}
		
		
		if(isset($cookie->_whereorder)) {
		
			$this->_where = $cookie->_whereorder;
		
		}
		else
		{
			$this->_where = 'AND ca.riferimento NOT LIKE "%BDL%" AND oh.`id_order_history` = (SELECT MAX(`id_order_history`) FROM `'._DB_PREFIX_.'order_history` moh WHERE moh.`id_order` = a.`id_order`  AND ( moh.id_order_state != 27 AND moh.id_order_state != 28) GROUP BY moh.`id_order`)';
		}
		
		if(Tools::getIsset('spring') && Tools::getValue('spring') == 'yes')
			$this->_where .= 'AND a.spring = 0';
		
		if(Tools::getIsset('astec') && Tools::getValue('astec') == 'yes')
			$this->_where .= 'AND a.id_order IN (SELECT id_order FROM order_detail WHERE product_reference LIKE "%inst%" OR product_reference LIKE "%astec%"  OR product_reference LIKE "%teleg%")';
		
		if(Tools::getIsset('astec_aperti') && Tools::getValue('astec_aperti') == 'yes')
			$this->_where .= 'AND (a.id_order) IN (SELECT substr(riferimento,2) FROM action_thread WHERE subject LIKE "%*** ACQUISTO ORDINE CON ASSISTENZA TECNICA ***%" AND status != "closed")';
		
		if(Tools::getIsset('valid') && Tools::getValue('valid') == 'yes')
			$this->_where .= 'AND (a.id_order IN (SELECT id_order FROM order_history WHERE id_order_state = 4 OR id_order_state = 5 OR id_order_state = 15 OR id_order_state = 14 OR id_order_state = 16 OR id_order_state = 20))';
		
		if(isset($_POST['submitResetorder'])) {
		
			$cookie->_whereorder = "";
			$cookie->_cercaordineprodotto = "";
			$cookie->_cercatestoprodottoordine = "";
			$this->_where = 'AND oh.`id_order_history` = (SELECT MAX(`id_order_history`) FROM `'._DB_PREFIX_.'order_history` moh WHERE moh.`id_order` = a.`id_order` GROUP BY moh.`id_order`)';
			unset($cookie->_whereorder);
		}
		
		$statesArray = array();
		$states = OrderState::getOrderStates((int)($cookie->id_lang));
		foreach ($states AS $state)
			$statesArray[$state['id_order_state']] = $state['name'];

 		$array_orders = array(
		'id_order' => array('title' => AdminOrders::lx('ID'), 'align' => 'center', 'width' => 40, 'widthColumn' => 50),
		'preventivo' => array('title' => AdminOrders::lx('N. PREV.'), 'align' => 'center', 'width' => 40, 'tmpTableFilter' => true, 'widthColumn' => 50),
		'new' => array('title' => AdminOrders::lx('New'), 'width' => 25, 'align' => 'center', 'type' => 'bool', 'filter_key' => 'new', 'tmpTableFilter' => true, 'icon' => array(0 => 'blank.gif', 1 => 'news-new.gif'), 'orderby' => false, 'widthColumn' => 35),
		'customer' => array('title' => AdminOrders::lx('Cliente'), 'widthColumn' => 200, 'width' => 120, 'filter_key' => 'customer', 'tmpTableFilter' => true),
		'group' => array('title' => AdminOrders::lx('Gruppo'), 'widthColumn' => 10, 'width' => 10, 'filter_key' => 'group', 'tmpTableFilter' => true),
		'total_products' => array('title' => AdminOrders::lx('T. netto'), 'width' => 40, 'align' => 'right', 'prefix' => '<b>', 'suffix' => '</b>', 'price' => true, 'currency' => true, 'widthColumn' => 50),
		
		'total_paid' => array('title' => AdminOrders::lx('Totale'), 'width' => 40, 'align' => 'right', 'prefix' => '<b>', 'suffix' => '</b>', 'price' => true, 'currency' => true, 'widthColumn' => 50),
		'paymentz' => array('title' => AdminOrders::lx('Pagamento'), 'width' => 100, 'widthColumn' => 110, 'tmpTableFilter' => true),
		'osname' => array('title' => AdminOrders::lx('Stato'), 'widthColumn' => 120, 'type' => 'select', 'select' => $statesArray, 'filter_key' => 'os!id_order_state', 'filter_type' => 'int', 'width' => 120),
		'date_add' => array('title' => AdminOrders::lx('Data'), 'width' => 35, 'align' => 'right', 'type' => 'datetime', 'filter_key' => 'a!date_add', 'widthColumn' => 55));
		
		if($cookie->id_employee != 7 && $cookie->id_employee != 14 && $cookie->id_employee != 2 && $cookie->id_employee != 3 && $cookie->id_employee != 19 && $cookie->id_employee != 12 && $cookie->id_employee != 21)
			$array_orders['id_pdf'] = array('title' => AdminOrders::lx('PDF'), 'callback' => 'printPDFIcons', 'orderby' => false, 'search' => false, 'widthColumn' => 30);
		else
		{
			$array_orders['spring'] = array('title' => AdminOrders::lx('Verificato'), 'align' => 'center', 'orderby' => false, 'search' => false, 'widthColumn' => 30);
		}
		
		if($cookie->id_employee == 2 || $cookie->id_employee == 14)
			$array_orders['pag_amz_ric'] = array('title' => AdminOrders::lx('Pag.Amz.'), 'align' => 'center', 'orderby' => false, 'search' => false, 'widthColumn' => 30);
		
		if($cookie->id_employee == 7 || $cookie->id_employee == 2 || $cookie->id_employee == 14 || $cookie->id_employee == 19)
			$array_orders['check_pi'] = array('title' => AdminOrders::lx('P.I.'), 'align' => 'center', 'orderby' => false, 'search' => false, 'widthColumn' => 30);
		
		if($cookie->id_employee == 14 || $cookie->id_employee == 2)
			$array_orders['check_esolver'] = array('title' => AdminOrders::lx('eSolv.'), 'align' => 'center', 'orderby' => false, 'search' => false, 'widthColumn' => 30);
		
		$this->fieldsDisplay = $array_orders;
		
		parent::__construct();
	}

	/**
	  * @global object $cookie Employee cookie necessary to keep trace of his/her actions
	  */
	public function postProcess()
	{
		global $currentIndex, $cookie;
		
		$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
		
		$modalita_esolver = Db::getInstance()->getValue('SELECT value FROM configuration WHERE name = "MODALITA_ESOLVER"');
		if(Tools::getIsset('vieworder') && Tools::getIsset('tab') && Tools::getValue('tab') == 'AdminOrders')
		{
			$id_customer = Db::getInstance()->getValue('SELECT id_customer FROM orders WHERE id_order = '.Tools::getValue('id_order'));
			/*Tools::redirectAdmin('index.php?tab=AdminCustomers&id_customer='.$id_customer.'&viewcustomer&tab-container-1=4&id_order='.Tools::getValue('id_order').'&vieworder&tab-container-1=4&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'');*/
		}
		
		if (isset($_GET['delete'.$this->table]))
		{
			
			
			
			$id_order_to_delete = Tools::getValue('id_order');
			
			Db::getInstance()->executeS('UPDATE action_thread SET status = "closed" WHERE subject LIKE "%PROMEMORIA ORDINE CON BONIFICO N. '.$id_order_to_delete.'%"');
			
			Db::getInstance()->executeS('UPDATE action_thread SET action_to = '.$incaricato.' WHERE subject = "*** VERIFICA TECNICA SU ORDINE N. '.$id_order_to_delete.'"');
			
			$importo_netto = Db::getInstance()->getValue('SELECT total_products FROM orders WHERE id_order = '.$id_order_to_delete);
			$id_customer_od = Db::getInstance()->getValue('SELECT id_customer FROM orders WHERE id_order = '.$id_order_to_delete);
			$dati_cliente_od = Db::getInstance()->getRow('SELECT * FROM customer WHERE id_customer = '.$id_customer_od);
			$importo_totale = Db::getInstance()->getValue('SELECT total_paid FROM orders WHERE id_order = '.$id_order_to_delete);

			if($dati_cliente_od['is_company'] == 1)
				$cli_od = $dati_cliente_od['company']. '(PI: '.$dati_cliente_od['vat_number'].')';
			else
				$cli_od = $dati_cliente_od['firstname'].' '.$dati_cliente_od['lastname']. '(CF: '.$dati_cliente_od['tax_code'].')';
			$headers  = 'MIME-Version: 1.0' . "\n";
			$headers .= 'Content-Type: text/html' ."\n";
			$headers .= 'From: "Ezdirect" <info@ezdirect.it>' . "\n";
			
			$messaggio_eliminato = '<strong>Eliminato ordine '.$id_order_to_delete.'. </strong>
			
			<br /><br />
			Cliente: '.$cli_od.'<br />
			Importo netto senza trasporto: '.$importo_netto.'<br />
			Importo totale IVA inclusa: '.$importo_totale.'<br />
			<br />
			';
			
			
			//mail('posta@federicogiannini.com','ELIMINATO ORDINE '.$id_order,$messaggio_eliminato,$headers );
			mail('barbara.giorgini@ezdirect.it','ELIMINATO ORDINE '.$id_order,$messaggio_eliminato,$headers );
			mail('matteo.delgiudice@ezdirect.it','ELIMINATO ORDINE '.$id_order,$messaggio_eliminato,$headers );
		}
		
		if(!isset($_GET['vieworder'])) {
		
			echo "Cerca ordine in base al codice prodotto: 
			<form action='' name='cercareordineprodotto' method='post'>";
			echo '<script type="text/javascript" src="../js/select2.js"></script>
					<script type="text/javascript">
					$(document).ready(function() { $("#cercaordineprodotto").select2(); });
					</script>';
			echo '<select id="cercaordineprodotto" name="cercaordineprodotto" style="width:600px">
					<option name="0" value="0">--- Scegli un prodotto ---</option>
					';
					
					
					$results = Db::getInstance()->ExecuteS("SELECT DISTINCT od.product_reference, od.product_id, od.product_name FROM order_detail od WHERE product_name != '' GROUP BY product_reference, product_id ORDER BY product_name ASC");
					
					foreach ($results as $row) {
					
						echo "<option name='$row[product_id]' value='$row[product_id]'"; 
						echo ">$row[product_name] ($row[product_reference]) - ID: $row[product_id]</option>";
					
					}
						
					echo "</select>&nbsp;&nbsp;&nbsp;";
			echo "
			Testo libero:
			<input type='text' value='".$cookie->_cercatestoprodottoordine."' name='cercatestoprodottoordine' />
			"; 
			
			echo "<input type='submit' value='Cerca' class='button' /></form>
			<br />";
			
			if(isset($cookie->_whereorder)) {
				$prodottocercato = Db::getInstance()->getRow('SELECT name, reference FROM product p JOIN product_lang pl ON p.id_product = pl.id_product WHERE pl.id_lang = '.$cookie->id_lang.' AND p.id_product = '.$cookie->_cercaordineprodotto);
				//echo "<strong>Stai vedendo la lista degli ordini che contengono: ".$prodottocercato['name']." (".$prodottocercato['reference'].")</strong>";
				echo "<br />";
			
			}
			
			echo '<a href="index.php?tab=AdminOrdersNonProcessati&token='.Tools::getAdminToken('AdminOrdersNonProcessati'.(int)Tab::getIdFromClassName('AdminOrdersNonProcessati').(int)$cookie->id_employee).'">Clicca qui per ordini APERTI</a> (<a href="ordini_non_evasi_xls.php"  onclick="window.onbeforeunload = null">scarica in formato Excel</a>) &nbsp;&nbsp;&nbsp;';
	
			if(Tools::getIsset('astec'))
				echo '<br ><strong>Stai guardando solo gli ordini con assistenza</strong>. <a href="index.php?tab=AdminOrders&token='.Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee).'">Clicca qui per visualizzare la lista di tutti gli ordini</a><br />';
			else
				echo ' |  &nbsp;&nbsp;&nbsp;<a href="index.php?tab=AdminOrders&astec=yes&token='.Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee).'">Clicca qui per ordini con assistenza</a> ';
			
			if(Tools::getIsset('astec_aperti'))
				echo '<br ><strong>Stai guardando solo gli ordini con assistenza CON TODO APERTO</strong>. <a href="index.php?tab=AdminOrders&token='.Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee).'">Clicca qui per visualizzare la lista di tutti gli ordini</a><br />';
			else
				echo ' |  &nbsp;&nbsp;&nbsp;<a href="index.php?tab=AdminOrders&astec_aperti=yes&token='.Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee).'">Clicca qui per ordini con assistenza CON TODO APERTO</a> ';
			
			if(Tools::getIsset('valid'))
				echo '<br ><strong>Stai guardando solo gli ordini validi</strong>. <a href="index.php?tab=AdminOrders&token='.Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee).'">Clicca qui per visualizzare la lista di tutti gli ordini</a><br />';
			else
				echo ' |  &nbsp;&nbsp;&nbsp;<a href="index.php?tab=AdminOrders&valid=yes&token='.Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee).'">Clicca qui per ordini validi</a><br />';
			
			if($cookie->id_employee == 7 || $cookie->id_employee == 2 || $cookie->id_employee == 3)
			{	
				if(Tools::getIsset('spring'))
					echo '<br ><strong>Stai guardando ordini non modificati per Spring</strong>. <a href="index.php?tab=AdminOrders&token='.Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee).'">Clicca qui per visualizzare la lista di tutti gli ordini</a><br />';
				else
					echo '<br /><a href="index.php?tab=AdminOrders&spring=yes&token='.Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee).'">Clicca qui per visualizzare la lista di tutti gli ordini non modificati su Spring</a><br />';
			}
		}
		$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
					
		if(isset($_GET['prvcustomer'])) {
			$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
			Tools::redirectAdmin('index.php?tab=AdminCustomers&id_customer='.$_GET['prvcustomer'].'&viewcustomer&id_order='.Tools::getValue('id_order').'&vieworder&tab-container-1=4&token='.$tokenCustomers);
		}
		
		/* Update shipping number */
		if (Tools::isSubmit('submitShippingNumber') AND ($id_order = (int)(Tools::getValue('id_order'))) AND Validate::isLoadedObject($order = new Order($id_order)))
		{
			if ($this->tabAccess['edit'] === '1')
			{
				/*if (!$order->hasBeenShipped() && !$order->hasBeenShippedPartially())
					die(Tools::displayError('The shipping number can only be set once the order has been shipped.'));
				*/
				$_GET['view'.$this->table] = true;
				$shipping_number = pSQL(Tools::getValue('shipping_number'));
				$order->shipping_number = $shipping_number;
				$order->update();
				if ($shipping_number)
				{
					global $_LANGMAIL;
					$customer = new Customer((int)($order->id_customer));
					$carrier = new Carrier((int)($order->id_carrier));
					if (!Validate::isLoadedObject($customer) OR !Validate::isLoadedObject($carrier))
						die(Tools::displayError());
					$templateVars = array(
						'{followup}' => str_replace('@', $order->shipping_number, $carrier->url),
						'{firstname}' => $customer->firstname,
						'{id_collo}' =>$order->shipping_number,
						'{lastname}' => $customer->lastname,
						'{id_order}' => (int)($order->id)
					);
					
					$id_country = Db::getInstance()->getValue("SELECT id_country FROM address WHERE id_address = ".$order->id_address_delivery."");
					
					if($order->id_lang == 0)
						$order->id_lang = 5;
					
					if($customer->order_notifications == 1) {
					}
					else {
				
						if($id_country == 10) {
							echo "X";
							@Mail::Send((int)$order->id_lang, 'in_transit', Mail::l('Ordine spedito', (int)$order->id_lang), $templateVars,
							$customer->email, $customer->firstname.' '.$customer->lastname, NULL, NULL, NULL, NULL,
							_PS_MAIL_DIR_, true);
						}
						else
						{
							@Mail::Send((int)$order->id_lang, 'in_transit', Mail::l('Order sent', (int)$order->id_lang), $templateVars,
							$customer->email, $customer->firstname.' '.$customer->lastname, NULL, NULL, NULL, NULL,
							_PS_MAIL_DIR_, true);
							
							/*@Mail::Send((int)$order->id_lang, 'in_transit_tnt', Mail::l('Order sent', (int)$order->id_lang), $templateVars,
							$customer->email, $customer->firstname.' '.$customer->lastname, NULL, NULL, NULL, NULL,
							_PS_MAIL_DIR_, true);*/
						}
					}
					if(isset($_POST['numprvcustomer'])) {
						$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
					
					}
					else { }
				}
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
		}

		/* Change order state, add a new entry in order history and send an e-mail to the customer if needed */
		if ((Tools::isSubmit('submitState') || (Tools::isSubmit('submitShippingNumber'))) AND ($id_order = (int)(Tools::getValue('id_order'))) AND Validate::isLoadedObject($order = new Order(Tools::getValue('id_order'))))
		{
			if ($this->tabAccess['edit'] === '1')
			{
				$_GET['view'.$this->table] = true;
				if (!$newOrderStatusId = (int)(Tools::getValue('id_order_state')))
					$this->_errors[] = Tools::displayError('Invalid new order status');
				else
				{
					$id_order_state_attuale = Db::getInstance()->getValue('SELECT id_order_state FROM order_history WHERE id_order = '.$id_order.' ORDER BY id_order_history DESC');
					
					$stato_attuale = Order::eSolverStatus($id_order, $id_order_state_attuale);
					$stato_nuovo = Order::eSolverStatus($id_order, $newOrderStatusId);
					
					$history = new OrderHistory();
					$history->id_order = (int)$id_order;
					$history->id_employee = (int)($cookie->id_employee);
					$history->changeIdOrderState((int)($newOrderStatusId), (int)($id_order));
					
					$order = new Order((int)$order->id);
					$products = $order->getProducts();
					if($newOrderStatusId == 31)
					{
						$trasp_id = Db::getInstance()->getValue('SELECT product_id FROM rimborsi WHERE product_id = "TRASP" AND id_order = '.$order->id);
						if($trasp_id == 0 || $trasp_id == '' || !$trasp_id)
						{
							Db::getInstance()->executeS('INSERT INTO rimborsi (id_order, product_id, product_price, product_quantity) VALUES ("'.$order->id.'", "TRASP", "'.$order->total_shipping.'", "1")');
						}
					}
					
					
					foreach($products as $product)
					{
						
						if($newOrderStatusId == 18)
						{
							Db::getInstance()->executeS('UPDATE orders SET acconto = 0 WHERE id_order = '.$order->id);
						}
						
						if($newOrderStatusId == 31)
						{
							$product_id = Db::getInstance()->getValue('SELECT product_id FROM rimborsi WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
							if(!$product_id || $product_id <= 0 || $product_id == '')
							{
								
								Db::getInstance()->executeS('INSERT INTO rimborsi (id_order, product_id, product_quantity, product_price) VALUES ("'.$order->id.'", "'.$product['product_id'].'", "'.$product['product_quantity'].'", "'.($product['product_price']-($product['product_price']*$product['reduction_percent']/100)).'")');
							}
						}
					
						if ($newOrderStatusId != 15 && ($newOrderStatusId != 24 && $newOrderStatusId != 25 && $newOrderStatusId != 27 && $newOrderStatusId != 28))
						{	
							// Db::getInstance()->executeS('UPDATE order_detail SET cons = "" WHERE product_id = '.$product['product_id'].' AND 	id_order = '.$order->id);
						}
						else
						{
							$cons = Db::getInstance()->getValue('SELECT cons FROM order_detail WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
							if($cons == '' || empty($cons))
							{
								//$rif_fatt = Db::getInstance()->getValue('SELECT rif_vs_ordine FROM fattura WHERE rif_vs_ordine = '.$order->id);
								if($rif_fatt == $order->id && in_array(trim($product['product_reference']), $array_parziali))
									Db::getInstance()->executeS('UPDATE order_detail SET cons = "0_'.Db::getInstance()->getValue('SELECT SUM(qt_ord) FROM fattura WHERE cod_articolo = "'.trim($product['product_reference']).'" AND rif_ordine = '.$order->id.' GROUP BY cod_articolo').'" WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
								else if($rif_fatt == $order->id && !(in_array(trim($product['product_reference']), $array_parziali)))
									Db::getInstance()->executeS('UPDATE order_detail SET cons = "0_0" WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
								else if($rif_fatt != $order->id)
									Db::getInstance()->executeS('UPDATE order_detail SET cons = "0_0" WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
								else
									Db::getInstance()->executeS('UPDATE order_detail SET cons = "0_0" WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id);
							}	
						}
					}
					
					if($newOrderStatusId == 16 || $newOrderStatusId == 4 || $newOrderStatusId == 2 || $newOrderStatusId == 20)
					{
						/*$fatturato_todo_thread = Db::getInstance()->getValue('SELECT id_action FROM action_message WHERE message LIKE "%Ricordarsi di chiudere ordine n. '.$order->id.'%"');
						
						Db::getInstance()->executeS('UPDATE action_thread SET status = "closed" WHERE id_action = '.$fatturato_todo_thread);*/
					}
					
					if($newOrderStatusId == 5 || $newOrderStatusId == 4 ||  $newOrderStatusId == 6 || $newOrderStatusId == 7 || $newOrderStatusId == 20 || $newOrderStatusId == 16 || $newOrderStatusId == 14)
					{
						Db::getInstance()->executeS('UPDATE action_thread SET status = "closed" WHERE subject LIKE "%*** PROMEMORIA ORDINE FATTURATO N. '.$order->id.'%"');
					}
					
					
					
					if($newOrderStatusId == 16 || $newOrderStatusId == 4 ||  $newOrderStatusId == 18 || $newOrderStatusId == 2 || $newOrderStatusId == 20)
					{
						
						
						
						Db::getInstance()->executeS('UPDATE action_thread SET status = "closed" WHERE subject LIKE "%PROMEMORIA ORDINE CON BONIFICO N. '.$order->id.'%" AND action_to = 7');
						
						$scadenza = Db::getInstance()->getValue('SELECT scadenza FROM cart WHERE id_cart = '.$order->id_cart);
						$cadenza = Db::getInstance()->getValue('SELECT cadenza FROM cart WHERE id_cart = '.$order->id_cart);
						$competenza_dal = Db::getInstance()->getValue('SELECT competenza_dal FROM cart WHERE id_cart = '.$order->id_cart);
						$competenza_al = Db::getInstance()->getValue('SELECT competenza_al FROM cart WHERE id_cart = '.$order->id_cart);
						$id_contratto_t = Db::getInstance()->getValue("SELECT id_contratto FROM contratto_assistenza WHERE rif_ordine = ".$order->id);
						
						$cart_name = Db::getInstance()->getValue('SELECT name FROM cart WHERE id_cart = '.$order->id_cart);
						
						if($cadenza != '' && $id_contratto_t <= 0 && substr($cart_name,0,18 != 'Rinnovo assistenza'))
						{
							if($scadenza != '' && strtotime($scadenza) > 0  && $scadenza != '0000-00-00' && $scadenza != '1970-01-01')
							{
								
							}
							else
							{
								$scadenza = date('Y-m-d',strtotime(date("Y-m-d H:i:s", mktime()) . " + 365 day"));
							}
							$tot_teleassistenza_row = Db::getInstance()->getRow("SELECT count(*) AS tot_assistenza, SUM((`product_price` - ((product_price * reduction_percent) / 100))) AS totale FROM order_detail WHERE id_order = ".$order->id." AND (product_reference LIKE '%teleg%') AND (product_reference NOT LIKE '%r1y%')");
							
							$tot_assistenza_row = Db::getInstance()->getRow("SELECT count(*) AS tot_assistenza, SUM((`product_price` - ((product_price * reduction_percent) / 100))) AS totale FROM order_detail WHERE id_order = ".$order->id." AND (product_reference LIKE '%astectop%')");
							
							if($tot_teleassistenza_row['tot_assistenza'] > 0)
								$tipo_contratto = 4;
							else 
								$tipo_contratto = 2;
							
							$dom = explode('-',$scadenza);
							
							$importo = $tot_assistenza_row['totale'] + $tot_teleassistenza_row['totale'];
							
							$data_scadenza = $dom[0].'-'.$dom[1].'-'.$dom[2].' 23:59:59';
							
							$decorrenza = Db::getInstance()->getValue('SELECT decorrenza FROM cart WHERE id_cart = '.$order->id_cart);
							
							if($decorrenza != '' && $decorrenza != '0000-00-00')
							{
								$dom2 = explode('-',$decorrenza);
								$data_decorrenza = $dom2[0].'-'.$dom2[1].'-'.$dom2[2].' 23:59:59';
							}
							else
							{
								$data_decorrenza = date('Y-m-d H:i:s');
							}
							
							$dom3 = explode('-',$competenza_dal);
							$dom4 = explode('-',$competenza_al);
							
							$data_competenza_dal = $dom3[0].'-'.$dom3[1].'-'.$dom3[2].'';
							$data_competenza_al = $dom4[0].'-'.$dom4[1].'-'.$dom4[2].'';
							
							$id_contratto = Db::getInstance()->getValue("SELECT id_contratto FROM contratto_assistenza ORDER BY id_contratto DESC");
							$id_contratto++;
					
							Db::getInstance()->executeS("INSERT INTO contratto_assistenza (id_contratto, id_customer, codice_spring, status, tipo, descrizione, data_registrazione, data_inizio, data_fine, cadenza, competenza_dal, competenza_al, cig, cup, univoco, indirizzo, prezzo, periodicita, pagamento, blocco_amministrativo, rif_fattura, rif_noleggio, rif_ordine, note_private, date_add, date_upd) VALUES (".$id_contratto.", '".$order->id_customer."', '".Db::getInstance()->getValue('SELECT codice_spring FROM customer WHERE id_customer = '.$order->id_customer)."', '0', '".$tipo_contratto."', '', '".$data_decorrenza."', '".$data_decorrenza."', '".$data_scadenza."', '".$cadenza."', '".$data_competenza_dal."', '".$data_competenza_al."', '', '', '', '".$order->id_address_invoice."', '".$importo."', '12', '".Db::getInstance()->getValue('SELECT payment FROM orders WHERE id_order = '.$order->id.'')."', '0', '', '', '".$order->id."', '', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
							
							$products_ctr = Db::getInstance()->executeS('SELECT * FROM order_detail WHERE id_order = '.$order->id);
							
							foreach($products_ctr as $product_ctr)
								Db::getInstance()->executeS("INSERT INTO contratto_assistenza_prodotti (id_contratto, id_product, codice_prodotto, descrizione_prodotto, quantita, prezzo, seriale, note, indirizzo, date_add, date_upd) VALUES ('".$id_contratto."', ".$product_ctr['product_id'].", '".$product_ctr['product_reference']."', '".$product_ctr['product_name']."', '".$product_ctr['product_quantity']."', '".$product_ctr['product_price']."', '', '', '".$order->id_address_invoice."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
							
							$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
							$action_thread++;
							$action_subject = "*** PROMEMORIA - VERIFICARE CONTRATTO N. ".$id_contratto." ";
							
							$id_employee = 14;
							$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').'1');

							Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd, promemoria) VALUES ('$action_thread', 'Attivita', '".$order->id_customer."', '".addslashes($action_subject)."', 0, ".$id_employee.", 'open', '', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."', 1)");   

							Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', '".$order->id_customer."', 99, '".$id_employee."', '".$id_employee."', '', '<strong>*** VERIFICARE CONTRATTO APPENA INSERITO ***</strong><br /><br />Appena inserito <a href=\"https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$order->id_customer."&viewcustomer&id_contratto=".$id_contratto."&token=".$tokenCustomers."&tab-container-1=13\" target=\"_blank\">contratto n. ".$id_contratto."</a>. Verificare', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  

						}
						
					}
					
					if($newOrderStatusId == 16 || $newOrderStatusId == 4 ||  $newOrderStatusId == 18 || $newOrderStatusId == 2 || $newOrderStatusId == 20)
					{
						$cart_name = Db::getInstance()->getValue('SELECT name FROM cart WHERE id_cart = '.$order->id_cart);
						
						
						if(substr($cart_name,0,18) == 'Rinnovo assistenza')
						{
							
							$tot_teleassistenza_row = Db::getInstance()->getRow("SELECT count(*) AS tot_assistenza, SUM((`product_price` - ((product_price * reduction_percent) / 100))) AS totale FROM order_detail WHERE id_order = ".$order->id." AND (product_reference LIKE '%teleg%') ");
							
							$tot_assistenza_row = Db::getInstance()->getRow("SELECT count(*) AS tot_assistenza, SUM((`product_price` - ((product_price * reduction_percent) / 100))) AS totale FROM order_detail WHERE id_order = ".$order->id." AND (product_reference LIKE '%astectop%')");
							
							if($tot_teleassistenza_row['tot_assistenza'] > 0)
								$tipo_contratto = 4;
							else 
								$tipo_contratto = 2;
							
							$dom = explode('-',$scadenza);
							
							$importo = $tot_assistenza_row['totale'] + $tot_teleassistenza_row['totale'];
							
							$names = explode(' ',$cart_name);
							$id_contratto = $names[2];
							$anno = $names[4];
							$scadenza = date('Y-m-d',strtotime(date("Y-m-d H:i:s", strtotime(Db::getInstance()->getValue('SELECT data_fine FROM contratto_assistenza WHERE id_contratto = '.$id_contratto)) . " + 365 day")));
							Db::getInstance()->executeS('UPDATE contratto_assistenza SET data_fine = "'.$scadenza.'", prezzo = "'.$importo.'", status = 0 WHERE id_contratto = '.$id_contratto);
							
							$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
							$action_thread++;
							$action_subject = "*** PROMEMORIA - VERIFICARE CONTRATTO N. ".$id_contratto." ";
							
							$id_employee = 14;
							$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').'1');

							Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd, promemoria) VALUES ('$action_thread', 'Attivita', '".$order->id_customer."', '".addslashes($action_subject)."', 0, ".$id_employee.", 'open', '', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."', 1)");   

							Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', '".$order->id_customer."', 99, '".$id_employee."', '".$id_employee."', '', '<strong>*** VERIFICARE CONTRATTO APPENA RINNOVATO ***</strong><br /><br />Appena rinnovato <a href=\"https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$order->id_customer."&viewcustomer&id_contratto=".$id_contratto."&token=".$tokenCustomers."&tab-container-1=13\" target=\"_blank\">contratto n. ".$id_contratto."</a>. Verificare', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
						}
					}
					
					if($newOrderStatusId == 18 || $newOrderStatusId == 2)
					{
						
						Db::getInstance()->executeS('UPDATE action_thread SET status = "closed" WHERE subject LIKE "*** PROMEMORIA ORDINE CON BONIFICO N. '.$order->id.'%"');
						
						$cart_subject = Db::getInstance()->getValue('SELECT name FROM cart WHERE id_cart = '.$order->id_cart);
						
						if(strpos($cart_subject, 'Rinnovo') !== false)
						{
							$params_contratto = explode('anno',$cart_subject);
							
							$anno_contratto = $params_contratto[1];
							$anno_contratto = (int)$anno_contratto;
							$numero_contratto = preg_replace('/[^0-9]/', '', $params_contratto[0]);
							
							$data_scadenza_contratto = Db::getInstance()->getValue('SELECT data_fine FROM contratto_assistenza WHERE id_contratto = '.$numero_contratto);
							
							$data_scadenza_att = explode(' ',$data_scadenza_contratto);
							$data_scadenza_attuale = explode('-',$data_scadenza_att[0]);
							
							$nuova_data_scadenza = ($anno_contratto+1).'-'.$data_scadenza_attuale[1].'-'.$data_scadenza_attuale[2].' 23:59:59';
							
							
							Db::getInstance()->executeS('UPDATE contratto_assistenza SET status = 0, data_fine = "'.$nuova_data_scadenza.'" WHERE id_contratto = '.$numero_contratto);
							
						}
						
						$tot_assistenza = Db::getInstance()->getValue("SELECT count( * ) AS tot_assistenza FROM order_detail WHERE (product_reference LIKE '%astec%' OR product_reference LIKE '%inst%') AND id_order = ".$order->id);
						if($tot_assistenza > 0) 
						{
							$first_product = Db::getInstance()->getValue('SELECT product_name FROM order_detail WHERE id_order = '.$order->id);
							$action_subject = "*** ACCETTATO BONIFICO SU ORDINE N. ".$order->id." (".$first_product.")";
							
							$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
							$action_thread++;
						
							Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd) VALUES ('$action_thread', 'Attivita', '".$order->id_customer."', '".addslashes($action_subject)."', ".$cookie->id_employee.", 5, 'open', 'O".$order->id."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");   

							Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', ".$order->id_customer.", ".$cookie->id_employee.", 5, 5, '', '".addslashes($action_subject)."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
							
						}	
						
					}
					$cart_name = Db::getInstance()->getValue('SELECT name FROM cart WHERE id_cart = '.$order->id_cart);
					if($newOrderStatusId == 4 && (strpos($cart_name,'AMAZON ID') !== false))
					{
						$action_this = Db::getInstance()->getValue("SELECT id_action FROM action_thread WHERE subject LIKE '%*** CHISURA AMAZON SU ORDINE N. ".$order->id." %'");
						if($action_this > 0)
						{
							
						}
						else
						{
							$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
							$action_thread++;
							
							$action_subject = "*** CHISURA AMAZON SU ORDINE N. ".$order->id." ";
							$incaricato = 14;
							$messaggio_verifica = "Chiudere su Amazon ordine N. ".$order->id." ";
							
							Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd) VALUES ('$action_thread', 'TODO Amazon', '".$order->id_customer."', '".addslashes($action_subject)."', ".$cookie->id_employee.", ".$incaricato.", 'open', 'O".$order->id."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");   

							Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', ".$order->id_customer.", ".$cookie->id_employee.", ".$incaricato.", ".$incaricato.", '', '".addslashes($messaggio_verifica)."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
							
							//mando mail
							$tokenbarbara = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."2");
							$linkbarbara = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenbarbara.'';
							$tokenmatteo = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."7");
							$linkmatteo = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenmatteo.'';
							
							$tokenincarico = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders')).$incaricato);
							$linkincarico = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenincarico.'';
							$mail_incarico = Db::getInstance()->getValue('SELECT email FROM employee WHERE id_employee = '.$incaricato);
							
							$nome_incarico =  Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$incaricato);
							
							$testomail = "Verifica tecnica su ordine n. ".$order->id.". <br /><br />
							Messaggio: ".$messaggio_verifica."<br />
							Clicca sul link per entrare nell'ordine:<br />
							Barbara - <a href='".$linkbarbara."'>clicca qui</a><br />
							Matteo - <a href='".$linkmatteo."'>clicca qui</a><br />
							".($incaricato == 2 || $incaricato == 7 ? '' : $nome_incarico." - <a href='".$linkincarico."'>clicca qui</a><br />")."";
						}
					}
					
					if($newOrderStatusId == 24)
					{
						
						$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
						$action_thread++;
						
						$action_subject = "*** VERIFICA TECNICA SU ORDINE N. ".$order->id." ";
						
						$incaricato = Tools::getValue('impiegati_verifica');
						$messaggio_verifica = Tools::getValue('verifica_msg');
						
						$existing_action = Db::getInstance()->getValue('SELECT id_action FROM action_thread WHERE subject LIKE "%'.$action_subject.'%"');
						$existing_employee = Db::getInstance()->getValue('SELECT action_to FROM action_thread WHERE subject LIKE "%'.$action_subject.'%"');
						if($existing_action  > 0)
						{
							
							$action_thread = $existing_action;
							
							Db::getInstance()->executeS('UPDATE action_thread SET action_to = '.$incaricato.' WHERE id_action = '.$action_thread.'');
							
							Customer::Storico($action_thread, 'A', $cookie->id_employee, $incaricato);
							
							$mailimp = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".$incaricato."'");
										
							$tokenimp = Tools::getAdminToken("AdminCustomers"."2".$incaricato);
											
							$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewaction&id_action=".Tools::getValue('id_action')."&token=".$tokenimp.'&tab-container-1=10';
							
							$employeemess = new Employee($cookie->id_employee);
				
							$msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato l'azione numero ".$action_thread." su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirla</a>.";
											
							if($existing_employee != 0) {
												
								$mailimprev = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".$existing_employee."'");
								$nomeimprev = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = '".$existing_employee."'");
												
								$msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione dell'azione numero ".Tools::getValue('id_action')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect e l'ha passata a ".Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.Tools::getValue('assegnaimpiegatoa').'');
												
								$params = array(
								'{link}' => $linkimp,
								'{firma}' => '',
								'{msg}' => $msgimprev);
								
								if($existing_employee != $incaricato)
								{			
									Mail::Send(5, 'msg_base', Mail::l('Gestione azione revocata', 5), 
										$params, $mailimprev, NULL, NULL, NULL, NULL, NULL, 
									_PS_MAIL_DIR_, true);
								}		
							}
											
							else {
											
							}
											
							$params = array(
							'{link}' => $linkimp,
							'{firma}' => '',
							'{msg}' => $msgimp);
											
							if($incaricato != $cookie->id_employee) {
											
								Mail::Send(5, 'msg_base', Mail::l('Azione assegnata a te su Ezdirect', 5), 
									$params, $mailimp, NULL, NULL, NULL, NULL, NULL, 
									_PS_MAIL_DIR_, true);
															
							} else { }
						}
						else
						{
							Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd) VALUES ('$action_thread', 'Attivita', '".$order->id_customer."', '".addslashes($action_subject)."', ".$cookie->id_employee.", ".$incaricato.", 'open', 'O".$order->id."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");   
						}
						Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', ".$order->id_customer.", ".$cookie->id_employee.", ".$incaricato.", ".$incaricato.", '', '".addslashes($messaggio_verifica)."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
						
						//mando mail
						$tokenbarbara = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."2");
						$linkbarbara = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenbarbara.'';
						$tokenmatteo = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."7");
						$linkmatteo = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenmatteo.'';
						
						$tokenincarico = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders')).$incaricato);
						$linkincarico = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenincarico.'';
						$mail_incarico = Db::getInstance()->getValue('SELECT email FROM employee WHERE id_employee = '.$incaricato);
						
						$nome_incarico =  Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$incaricato);
						
						$testomail = "Verifica tecnica su ordine n. ".$order->id.". <br /><br />
						Messaggio: ".$messaggio_verifica."<br />
						Clicca sul link per entrare nell'ordine:<br />
						Barbara - <a href='".$linkbarbara."'>clicca qui</a><br />
						Matteo - <a href='".$linkmatteo."'>clicca qui</a><br />
						".($incaricato == 2 || $incaricato == 7 ? '' : $nome_incarico." - <a href='".$linkincarico."'>clicca qui</a><br />")."";
						
						
						$params = array('{reply}' => $testomail);
						Mail::Send(5, 'action', Mail::l('Ordine con verifica tecnica', 5), $params, "barbara.giorgini@ezdirect.it", NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true);
						Mail::Send(5, 'action', Mail::l('Ordine con verifica tecnica', 5), $params, "matteo.delgiudice@ezdirect.it", NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true);
						if($incaricato != 2 && $incaricato != 7)
							Mail::Send(5, 'action', Mail::l('Ordine con verifica tecnica', 5), $params, $mail_incarico, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true);

					}
					
					if($newOrderStatusId == 25)
					{
						
						//mando mail
						$tokenbarbara = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."2");
						$linkbarbara = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenbarbara.'';
						$tokenmatteo = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."7");
						$linkmatteo = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenmatteo.'';
						$actionverifica = Db::getInstance()->getValue('SELECT id_action FROM action_thread WHERE subject = "*** VERIFICA TECNICA SU ORDINE N. '.$order->id.' "');
						Db::getInstance()->getValue('UPDATE action_thread SET status = "closed" WHERE id_action = '.$actionverifica); 
						
						$messaggiotodo = Db::getInstance()->getValue('SELECT action_message FROM action_message WHERE id_action = '.$actionverifica.' ORDER BY id_action_message DESC');
						
						$testomail = "Verifica conclusa su ordine n. ".$order->id.". <br /><br />
						Ultimo messaggio del TODO associato alla verifica: ".$messaggiotodo."<br /><br />
						Clicca sul link per entrare nell'ordine:<br />
						Barbara - <a href='".$linkbarbara."'>clicca qui</a><br />
						Matteo - <a href='".$linkmatteo."'>clicca qui</a><br />";
						
						$params = array('{reply}' => $testomail);
						Mail::Send(5, 'action', Mail::l('Verifica conclusa su ordine '.$order->id, 5), $params, "barbara.giorgini@ezdirect.it", NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true);
						Mail::Send(5, 'action', Mail::l('Verifica conclusa su ordine '.$order->id, 5), $params, "matteo.delgiudice@ezdirect.it", NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true);
					}
					
					if($newOrderStatusId == 6)
					{
						$headers  = 'MIME-Version: 1.0' . "\n";
						$headers .= 'Content-Type: text/html' ."\n";
						$headers .= 'From: "Ezdirect" <info@ezdirect.it>' . "\n";
			
						$dati_cliente_od = Db::getInstance()->getRow('SELECT * FROM customer WHERE id_customer = '.$order->id_customer);
			
						if($dati_cliente_od['is_company'] == 1)
							$cli_od = $dati_cliente_od['company']. '(PI: '.$dati_cliente_od['vat_number'].')';
						else
							$cli_od = $dati_cliente_od['firstname'].' '.$dati_cliente_od['lastname']. '(CF: '.$dati_cliente_od['tax_code'].')';
						
						$messaggio_annullato = '<strong>Annullato ordine '.$id_order.'.<br /><br />Cliente: '.$cli_od.'<br /> Prodotti contenuti:</strong><br />
						';
						
						$products = $order->getProducts();
						foreach($products as $product)
							$messaggio_annullato .= $product['product_name'].' ('.$product['product_reference'].') - Qt: '.$product['product_quantity'].'<br />';
							
						$tokenMatteo = Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).'7');
						$tokenBarbara = Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).'2');
						
						$link_matteo = '<br /><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order='.$id_order.'&vieworder&token='.$tokenMatteo.'">Clicca qui per entrare nell\'ordine</a>';
						$link_barbara = '<br /><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order='.$id_order.'&vieworder&token='.$tokenBarbara.'">Clicca qui per entrare nell\'ordine</a>';
						
						
						//mail('posta@federicogiannini.com','ANNULLATO ORDINE '.$id_order,$messaggio_annullato.$link_barbara,$headers );
						mail('barbara.giorgini@ezdirect.it','ANNULLATO ORDINE '.$id_order,$messaggio_annullato.$link_barbara,$headers );
						mail('matteo.delgiudice@ezdirect.it','ANNULLATO ORDINE '.$id_order,$messaggio_annullato.$link_matteo,$headers );
					}
					
					
					
					if($newOrderStatusId == 4 || $newOrderStatusId == 5 || $newOrderStatusId == 14 || $newOrderStatusId == 15 || $newOrderStatusId == 16 || $newOrderStatusId == 20)
					{
						$history_prec = Db::getInstance()->getValue('SELECT id_order_history FROM order_history WHERE (id_order_state = 4 OR id_order_state = 5  OR id_order_state = 14  OR id_order_state = 16  OR id_order_state = 20) AND id_order = '.$order->id);
						
						$history_parziali = Db::getInstance()->getValue('SELECT id_order_history FROM order_history WHERE (id_order_state = 15) AND id_order = '.$order->id);
						
						
						if($modalita_esolver == 0 && (!$history_prec || $history_prec < 0 || $history_prec == ''))
						{
							// SCARICO MAGAZZINO
							
							$prime = Db::getInstance()->getValue('SELECT id_cart FROM orders WHERE id_order = '.$order->id.' AND id_cart IN (SELECT id_cart FROM cart WHERE name LIKE "%AMAZON PRIME%")');
					
							if($prime > 0)
							{
								
							}
							else
							{
								
								$products = $order->getProducts();
								foreach($products as $product)
								{
									$stock = Db::getInstance()->getRow('SELECT quantity, stock_quantity, itancia_quantity, esprinet_quantity, attiva_quantity, techdata_quantity, intracom_quantity, supplier_quantity FROM product WHERE id_product = '.$product['product_id']);
									
									if($history_parziali > 0 && $newOrderStatusId != 15)
									{
										
										$cons = Db::getInstance()->getValue('SELECT cons FROM order_detail WHERE id_order = '.$order->id.' AND product_id = '.$product['product_id']);
										$cons = str_replace('0_','',$cons);
										$qt_da_sottrarre = $product['product_quantity'] - $cons;
										$stock_quantity = $stock['stock_quantity'] -= $qt_da_sottrarre;
										
									}
									else
									{
										if($newOrderStatusId != 15)
											$stock_quantity = $stock['stock_quantity'] -= $product['product_quantity'];
										else
										{
											$cons = Db::getInstance()->getValue('SELECT cons FROM order_detail WHERE id_order = '.$order->id.' AND product_id = '.$product['product_id']);
											$cons = str_replace('0_','',$cons);
											$qt_da_sottrarre = $cons;
											$stock_quantity = $stock['stock_quantity'] -= $qt_da_sottrarre;
										}
									}
									$qtsito = ($stock['supplier_quantity'] + $stock_quantity + $stock['esprinet_quantity'] + $stock['attiva_quantity'] + $stock['itancia_quantity'] + $stock['techdata_quantity'] + $stock['intracom_quantity']);
									
									Db::getInstance()->executeS("UPDATE product SET stock_quantity = $stock_quantity, quantity = $qtsito WHERE id_product = ".$product['product_id']."");

								}
							}	
						}
					}	
	
					if ($newOrderStatusId != 15 && ($newOrderStatusId != 24 && $newOrderStatusId != 25 && $newOrderStatusId != 27 && $newOrderStatusId != 28))
					{	
						Db::getInstance()->executeS('UPDATE order_detail SET cons = "" WHERE product_id = '.$product['product_id'].' AND 	id_order = '.$order->id);
					}
	
					$carrier = new Carrier((int)($order->id_carrier), (int)($order->id_lang));
					$templateVars = array();
					if ($history->id_order_state == Configuration::get('PS_OS_SHIPPING') AND $order->shipping_number)
						$templateVars = array('{followup}' => str_replace('@', $order->shipping_number, $carrier->url));
					elseif ($history->id_order_state == Configuration::get('PS_OS_CHEQUE'))
						$templateVars = array(
							'{cheque_name}' => (Configuration::get('CHEQUE_NAME') ? Configuration::get('CHEQUE_NAME') : ''),
							'{cheque_address_html}' => (Configuration::get('CHEQUE_ADDRESS') ? nl2br(Configuration::get('CHEQUE_ADDRESS')) : ''));
					elseif ($history->id_order_state == Configuration::get('PS_OS_BANKWIRE'))
						$templateVars = array(
							'{bankwire_owner}' => (Configuration::get('BANK_WIRE_OWNER') ? Configuration::get('BANK_WIRE_OWNER') : ''),
							'{bankwire_details}' => (Configuration::get('BANK_WIRE_DETAILS') ? nl2br(Configuration::get('BANK_WIRE_DETAILS')) : ''),
							'{bankwire_address}' => (Configuration::get('BANK_WIRE_ADDRESS') ? nl2br(Configuration::get('BANK_WIRE_ADDRESS')) : ''));
					
					if ($history->addWithemail(true, $templateVars))
					{
						
						$history_id = Db::getInstance()->getValue('SELECT id_order_history FROM order_history WHERE id_order = '.$id_order.' ORDER BY id_order_history DESC');
					
						if($newOrderStatusId == 33)
						{
							
							$acconto = str_replace(",",".",$_POST['acconto']);
							
							Customer::Storico($history_id, 'OH', $cookie->id_employee, 'Incassato acconto di '.number_format($acconto,2,",","."));
					
							Db::getInstance()->executeS('UPDATE orders SET acconto = '.$acconto.' WHERE id_order = '.$order->id);
							
						}
					
						
						if($stato_attuale != $stato_nuovo)
						{
							if( $newOrderStatusId == 4 || $newOrderStatusId == 5 || $newOrderStatusId == 6 || $newOrderStatusId == 7 || $newOrderStatusId == 8 || $newOrderStatusId == 14 || $newOrderStatusId == 15 || $newOrderStatusId == 16 || $newOrderStatusId == 20 || $newOrderStatusId == 29 || $newOrderStatusId == 35)
							{
								
							}
							else
							{
								Product::RecuperaCSV($id_order);
							}
						}
						$tokenOrders = Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').$cookie->id_employee);
						
						Tools::redirectAdmin($currentIndex.'&id_order='.$id_order.'&vieworder'.(isset($_POST['prvcustomer']) ? '&prvcustomer='.$_POST['numprvcustomer'] : '').'&token='.$tokenOrders);
		
					}
					
				
					$this->_errors[] = Tools::displayError('An error occurred while changing the status or was unable to send e-mail to the customer.');
				}
			}
			else {
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
			}
			
			/*if(isset($_POST['prvcustomer'])) {
				$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
				Tools::redirectAdmin('index.php?tab=AdminCustomers&id_customer='.$_POST['numprvcustomer'].'viewcustomer&id_order='.Tools::getValue('id_order').'&vieworder&tab-container-1=4&token='.$tokenCustomers);
		
			
			}*/
		}
		
		/* Add a new message for the current order and send an e-mail to the customer if needed */
		elseif (isset($_POST['submitMessage']))
		{
			$_GET['view'.$this->table] = true;
		 	if ($this->tabAccess['edit'] === '1')
			{
				if (!($id_order = (int)(Tools::getValue('id_order'))) OR !($id_customer = (int)(Tools::getValue('id_customer'))))
					$this->_errors[] = Tools::displayError('An error occurred before sending message');
				elseif (!Tools::getValue('message'))
					$this->_errors[] = Tools::displayError('Message cannot be blank');
				else
				{
					/* Get message rules and and check fields validity */
					$rules = call_user_func(array('Message', 'getValidationRules'), 'Message');
					foreach ($rules['required'] AS $field)
						if (($value = Tools::getValue($field)) == false AND (string)$value != '0')
							if (!Tools::getValue('id_'.$this->table) OR $field != 'passwd')
								$this->_errors[] = Tools::displayError('field').' <b>'.$field.'</b> '.Tools::displayError('is required.');
					foreach ($rules['size'] AS $field => $maxLength)
						if (Tools::getValue($field) AND Tools::strlen(Tools::getValue($field)) > $maxLength)
							$this->_errors[] = Tools::displayError('field').' <b>'.$field.'</b> '.Tools::displayError('is too long.').' ('.$maxLength.' '.Tools::displayError('chars max').')';
					foreach ($rules['validate'] AS $field => $function)
						if (Tools::getValue($field))
							if (!Validate::$function(htmlentities(Tools::getValue($field), ENT_COMPAT, 'UTF-8')))
								$this->_errors[] = Tools::displayError('field').' <b>'.$field.'</b> '.Tools::displayError('is invalid.');
					if (!sizeof($this->_errors))
					{
						$message = new Message();
						$message->id_employee = (int)($cookie->id_employee);
						$message->message = htmlentities(Tools::getValue('message'), ENT_COMPAT, 'UTF-8');
						$message->id_order = $id_order;
						$message->private = Tools::getValue('visibility');
						if (!$message->add())
							$this->_errors[] = Tools::displayError('An error occurred while sending message.');
						elseif ($message->private)
							Tools::redirectAdmin($currentIndex.'&id_order='.$id_order.'&vieworder&conf=11'.'&token='.$tokenCustomers);
						elseif (Validate::isLoadedObject($customer = new Customer($id_customer)))
						{
							$order = new Order((int)($message->id_order));
							if (Validate::isLoadedObject($order))
							{
								$varsTpl = array('{lastname}' => $customer->lastname, '{firstname}' => $customer->firstname, '{id_order}' => $message->id_order, '{message}' => (Configuration::get('PS_MAIL_TYPE') == 2 ? $message->message : nl2br2($message->message)));
								if (@Mail::Send((int)($order->id_lang), 'order_merchant_comment',
									Mail::l('New message regarding your order', (int)($order->id_lang)), $varsTpl, $customer->email,
									$customer->firstname.' '.$customer->lastname, NULL, NULL, NULL, NULL, _PS_MAIL_DIR_, true))
									Tools::redirectAdmin($currentIndex.'&id_order='.$id_order.'&vieworder&conf=11'.'&token='.$tokenCustomers);
							}
						}
						$this->_errors[] = Tools::displayError('An error occurred while sending e-mail to customer.');
					}
				}
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to delete here.');
		}

		/* Cancel product from order */
		elseif (Tools::isSubmit('cancelProduct') AND Validate::isLoadedObject($order = new Order((int)(Tools::getValue('id_order')))))
		{
			if ($this->tabAccess['delete'] === '1')
			{
				$productList = Tools::getValue('id_order_detail');
				$customizationList = Tools::getValue('id_customization');
				$qtyList = Tools::getValue('cancelQuantity');
				$customizationQtyList = Tools::getValue('cancelCustomizationQuantity');

				$full_product_list = $productList;
				$full_quantity_list = $qtyList;

				if ($customizationList)
				{
					foreach ($customizationList as $key => $id_order_detail)
					{
						$full_product_list[$id_order_detail] = $id_order_detail;
						$full_quantity_list[$id_order_detail] += $customizationQtyList[$key];
					}
				}

				if ($productList OR $customizationList)
				{
					if ($productList)
					{
						$id_cart = Cart::getCartIdByOrderId($order->id);
						$customization_quantities = Customization::countQuantityByCart($id_cart);

						foreach ($productList AS $key => $id_order_detail)
						{
							$qtyCancelProduct = abs($qtyList[$key]);
							if (!$qtyCancelProduct)
								$this->_errors[] = Tools::displayError('No quantity selected for product.');

							// check actionable quantity
							$order_detail = new OrderDetail($id_order_detail);
							$customization_quantity = 0;
							if (array_key_exists($order_detail->product_id, $customization_quantities) && array_key_exists($order_detail->product_attribute_id, $customization_quantities[$order_detail->product_id]))
								$customization_quantity =  (int) $customization_quantities[$order_detail->product_id][$order_detail->product_attribute_id];

							if (($order_detail->product_quantity - $customization_quantity - $order_detail->product_quantity_refunded - $order_detail->product_quantity_return) < $qtyCancelProduct)
								$this->_errors[] = Tools::displayError('Invalid quantity selected for product.');

						}
					}
					if ($customizationList)
					{
						$customization_quantities = Customization::retrieveQuantitiesFromIds(array_keys($customizationList));

						foreach ($customizationList AS $id_customization => $id_order_detail)
						{
							$qtyCancelProduct = abs($customizationQtyList[$id_customization]);
							$customization_quantity = $customization_quantities[$id_customization];

							if (!$qtyCancelProduct)
								$this->_errors[] = Tools::displayError('No quantity selected for product.');

							if ($qtyCancelProduct > ($customization_quantity['quantity'] - ($customization_quantity['quantity_refunded'] + $customization_quantity['quantity_returned'])))
								$this->_errors[] = Tools::displayError('Invalid quantity selected for product.');
						}
					}

					if (!sizeof($this->_errors) AND $productList)
						foreach ($productList AS $key => $id_order_detail)
						{
							$qtyCancelProduct = abs($qtyList[$key]);
							$orderDetail = new OrderDetail((int)($id_order_detail));

							// Reinject product
							if (!$order->hasBeenDelivered() OR ($order->hasBeenDelivered() AND Tools::isSubmit('reinjectQuantities')))
							{
								$reinjectableQuantity = (int)($orderDetail->product_quantity) - (int)($orderDetail->product_quantity_reinjected);
								$quantityToReinject = $qtyCancelProduct > $reinjectableQuantity ? $reinjectableQuantity : $qtyCancelProduct;
								if (!Product::reinjectQuantities($orderDetail, $quantityToReinject))
									$this->_errors[] = Tools::displayError('Cannot re-stock product').' <span class="bold">'.$orderDetail->product_name.'</span>';
								else
								{
									$updProductAttributeID = !empty($orderDetail->product_attribute_id) ? (int)($orderDetail->product_attribute_id) : NULL;
									$newProductQty = Product::getQuantity((int)($orderDetail->product_id), $updProductAttributeID);
									$product = get_object_vars(new Product((int)($orderDetail->product_id), false, (int)($cookie->id_lang)));
									if (!empty($orderDetail->product_attribute_id))
									{
										$updProduct['quantity_attribute'] = (int)($newProductQty);
										$product['quantity_attribute'] = $updProduct['quantity_attribute'];
									}
									else
									{
										$updProduct['stock_quantity'] = (int)($newProductQty);
										$product['stock_quantity'] = $updProduct['stock_quantity'];
									}
									Hook::updateQuantity($product, $order);
								}
							}

							// Delete product
							if (!$order->deleteProduct($order, $orderDetail, $qtyCancelProduct))
								$this->_errors[] = Tools::displayError('An error occurred during deletion of the product.').' <span class="bold">'.$orderDetail->product_name.'</span>';
							Module::hookExec('cancelProduct', array('order' => $order, 'id_order_detail' => $id_order_detail));
						}
					if (!sizeof($this->_errors) AND $customizationList)
						foreach ($customizationList AS $id_customization => $id_order_detail)
						{
							$orderDetail = new OrderDetail((int)($id_order_detail));
							$qtyCancelProduct = abs($customizationQtyList[$id_customization]);
							if (!$order->deleteCustomization($id_customization, $qtyCancelProduct, $orderDetail))
								$this->_errors[] = Tools::displayError('An error occurred during deletion of product customization.').' '.$id_customization;
						}
					// E-mail params
					if ((isset($_POST['generateCreditSlip']) OR isset($_POST['generateDiscount'])) AND !sizeof($this->_errors))
					{
						$customer = new Customer((int)($order->id_customer));
						$params['{lastname}'] = $customer->lastname;
						$params['{firstname}'] = $customer->firstname;
						$params['{id_order}'] = $order->id;
					}

					// Generate credit slip
					if (isset($_POST['generateCreditSlip']) AND !sizeof($this->_errors))
					{
						if (!OrderSlip::createOrderSlip($order, $full_product_list, $full_quantity_list, isset($_POST['shippingBack'])))
							$this->_errors[] = Tools::displayError('Cannot generate credit slip');
						else
						{
							Module::hookExec('orderSlip', array('order' => $order, 'productList' => $full_product_list, 'qtyList' => $full_quantity_list));
							@Mail::Send((int)$order->id_lang, 'credit_slip', Mail::l('New credit slip regarding your order', (int)$order->id_lang),
							$params, $customer->email, $customer->firstname.' '.$customer->lastname, NULL, NULL, NULL, NULL,
							_PS_MAIL_DIR_, true);
						}
					}

					// Generate voucher
					if (isset($_POST['generateDiscount']) AND !sizeof($this->_errors))
					{
						if (!$voucher = Discount::createOrderDiscount($order, $full_product_list, $full_quantity_list, AdminOrders::lx('Credit Slip concerning the order #'), isset($_POST['shippingBack'])))
							$this->_errors[] = Tools::displayError('Cannot generate voucher');
						else
						{
							$currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));
							$params['{voucher_amount}'] = Tools::displayPrice($voucher->value, $currency, false);
							$params['{voucher_num}'] = $voucher->name;
							@Mail::Send((int)$order->id_lang, 'voucher', Mail::l('New voucher regarding your order', (int)$order->id_lang),
							$params, $customer->email, $customer->firstname.' '.$customer->lastname, NULL, NULL, NULL,
							NULL, _PS_MAIL_DIR_, true);
						}
					}
				}
				else
					$this->_errors[] = Tools::displayError('No product or quantity selected.');

				// Redirect if no errors
				if (!sizeof($this->_errors))
					Tools::redirectAdmin($currentIndex.'&id_order='.$order->id.'&vieworder&conf=24&token='.$tokenCustomers);
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to delete here.');
		}
		elseif (isset($_GET['messageReaded']))
		{
			Message::markAsReaded((int)($_GET['messageReaded']), (int)($cookie->id_employee));
		}
		parent::postProcess();
	}

	private function displayCustomizedDatas(&$customizedDatas, &$product, &$currency, &$image, $tokenCatalog, $id_order_detail)
	{
		if (!($order = AdminOrders::loadObject()))
			return;

		if (is_array($customizedDatas) AND isset($customizedDatas[(int)($product['product_id'])][(int)($product['product_attribute_id'])]))
		{
			$imageObj = new Image($image['id_image']);
			echo '
			<tr>
				<td align="center">'.(isset($image['id_image']) ? cacheImage(_PS_IMG_DIR_.'p/'.$imageObj->getExistingImgPath().'.jpg',
				'product_mini_'.(int)($product['product_id']).(isset($product['product_attribute_id']) ? '_'.(int)($product['product_attribute_id']) : '').'.jpg', 45, 'jpg') : '--').'</td>
				<td><a href="index.php?tab=AdminCatalog&id_product='.$product['product_id'].'&updateproduct&token='.$tokenCatalog.'">
					<span class="productName">'.$product['product_name'].' - '.AdminOrders::lx('customized').'</span><br />
					'.($product['product_reference'] ? AdminOrders::lx('Ref:').' '.$product['product_reference'].'<br />' : '')
					.($product['product_supplier_reference'] ? AdminOrders::lx('Ref Supplier:').' '.$product['product_supplier_reference'] : '')
					.'</a></td>
				<td align="center">'.Tools::displayPrice($product['product_price_wt'], $currency, false).'</td>
				<td align="center" class="productQuantity">'.$product['customizationQuantityTotal'].'</td>
				'.($order->hasBeenPaid() ? '<td align="center" class="productQuantity">'.$product['customizationQuantityRefunded'].'</td>' : '').'
				'.($order->hasBeenDelivered() ? '<td align="center" class="productQuantity">'.$product['customizationQuantityReturned'].'</td>' : '').'
				<td align="center" class="productQuantity"> - </td>
				<td align="center">'.Tools::displayPrice(Tools::ps_round($order->getTaxCalculationMethod() == PS_TAX_EXC ? $product['product_price'] : $product['product_price_wt'], 2) * $product['customizationQuantityTotal'], $currency, false).'</td>
				<td align="center" class="cancelCheck">--</td>
			</tr>';
			foreach ($customizedDatas[(int)($product['product_id'])][(int)($product['product_attribute_id'])] AS $customizationId => $customization)
			{
				echo '
				<tr>
					<td colspan="2">';
				foreach ($customization['datas'] AS $type => $datas)
					if ($type == _CUSTOMIZE_FILE_)
					{
						$i = 0;
						echo '<ul style="margin: 4px 0px 4px 0px; padding: 0px; list-style-type: none;">';
						foreach ($datas AS $data)
							echo '<li style="display: inline; margin: 2px;">
									<a href="displayImage.php?img='.$data['value'].'&name='.(int)($order->id).'-file'.++$i.'" target="_blank"><img src="'._THEME_PROD_PIC_DIR_.$data['value'].'_small" alt="" /></a>
								</li>';
						echo '</ul>';
					}
					elseif ($type == _CUSTOMIZE_TEXTFIELD_)
					{
						$i = 0;
						echo '<ul style="margin: 0px 0px 4px 0px; padding: 0px 0px 0px 6px; list-style-type: none;">';
						foreach ($datas AS $data)
							echo '<li>'.($data['name'] ? $data['name'] : AdminOrders::lx('Text #').++$i).AdminOrders::lx(':').' '.$data['value'].'</li>';
						echo '</ul>';
					}
				echo '</td>
					<td align="center">-</td>
					<td align="center" class="productQuantity">'.$customization['quantity'].'</td>
					'.($order->hasBeenPaid() ? '<td align="center">'.$customization['quantity_refunded'].'</td>' : '').'
					'.($order->hasBeenDelivered() ? '<td align="center">'.$customization['quantity_returned'].'</td>' : '').'
					<td align="center">-</td>
					<td align="center">'.Tools::displayPrice(Tools::ps_round($order->getTaxCalculationMethod() == PS_TAX_EXC ? $product['product_price'] : $product['product_price_wt'], 2) * $customization['quantity'], $currency, false).'</td>
					<td align="center" class="cancelCheck">
						<input type="hidden" name="totalQtyReturn" id="totalQtyReturn" value="'.(int)($customization['quantity_returned']).'" />
						<input type="hidden" name="totalQty" id="totalQty" value="'.(int)($customization['quantity']).'" />
						<input type="hidden" name="productName" id="productName" value="'.$product['product_name'].'" />';
				if ((!$order->hasBeenDelivered() OR Configuration::get('PS_ORDER_RETURN')) AND (int)(($customization['quantity_returned']) < (int)($customization['quantity'])))
					echo '
						<input type="checkbox" name="id_customization['.$customizationId.']" id="id_customization['.$customizationId.']" value="'.$id_order_detail.'" onchange="setCancelQuantity(this, \''.$customizationId.'\', \''.(int)($customization['quantity'] - $customization['quantity_refunded']).'\')" '.(((int) ($customization['quantity_returned'] + $customization['quantity_refunded']) >= (int)($customization['quantity'])) ? 'disabled="disabled" ' : '').'/>';
				else
				echo '
					</td>
					<td class="cancelQuantity">';
				if ((int)($customization['quantity_returned'] + $customization['quantity_refunded']) >= (int)($customization['quantity']))
					echo '<input type="hidden" name="cancelCustomizationQuantity['.$customizationId.']" value="0" />';
				elseif (!$order->hasBeenDelivered() OR Configuration::get('PS_ORDER_RETURN'))
					echo '
						<input type="text" id="cancelQuantity_'.$customizationId.'" name="cancelCustomizationQuantity['.$customizationId.']" size="2" onclick="selectCheckbox(this);" value="" /> ';
				echo ($order->hasBeenDelivered() ? (int)($customization['quantity_returned']).'/'.((int)($customization['quantity']) - (int)($customization['quantity_refunded'])) : ($order->hasBeenPaid() ? (int)($customization['quantity_refunded']).'/'.(int)($customization['quantity']) : '')).'
					</td>';
				echo '
				</tr>';
			}
		}
	}

	private function getCancelledProductNumber(&$order, &$product)
	{
		$productQuantity = array_key_exists('customizationQuantityTotal', $product) ? $product['product_quantity'] - $product['customizationQuantityTotal'] : $product['product_quantity'];
		$productRefunded = $product['product_quantity_refunded'];
		$productReturned = $product['product_quantity_return'];
		$content = '0/'.$productQuantity;
		if ($order->hasBeenDelivered())
			$content = $productReturned.'/'.($productQuantity - $productRefunded);
		elseif ($order->hasBeenPaid())
			$content = $productRefunded.'/'.$productQuantity;
		return $content;
	}

	public function viewDetails()
	{

		global $currentIndex, $cookie, $link;
		$irow = 0;
		/*if (!($order = AdminOrders::loadObject()))
			return;*/
			
		$order = new Order(Tools::getValue('id_order'));	
		
		$customer = new Customer($order->id_customer);
		
		if(isset($_GET['getPDF'])) {
			ob_clean();

			require_once('../classes/html2pdf/html2pdf.class.php');
			
			
			$content = Order::getOrderPDF($order->id, $order->id_customer);

			$html2pdf = new HTML2PDF('P','A4','it');
			$html2pdf->WriteHTML($content);
	
			$html2pdf->Output('ordine-n-'.$order->id.'.pdf', 'D'); 
		
		}
		
		if(isset($_GET['getDDT'])) {
			ob_clean();
			
			Fattura::getFattura($order->id, $order->id_customer, 'DDT');
		
		}
		
		if(isset($_GET['getFF'])) {
			ob_clean();
			
			Fattura::getFattura($order->id, $order->id_customer, 'ff');
		
		}
		
		if(isset($_GET['printDDT']))
		{
			

			echo '<iframe id="iFramePdf" style="display:none;" src="http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order='.$order->id.'&vieworder&getDDT&token='.Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee).'"></iframe>';
		
			echo '
			<script type="text/javascript">
			$(window).load(function(){
				document.getElementById("iFramePdf").contentWindow.print();
			});
			</script>
			
			';
		}
		
		$customerStats = $customer->getStats();
		$addressInvoice = new Address($order->id_address_invoice, (int)($cookie->id_lang));
		if (Validate::isLoadedObject($addressInvoice) AND $addressInvoice->id_state)
			$invoiceState = new State((int)($addressInvoice->id_state));
		$addressDelivery = new Address($order->id_address_delivery, (int)($cookie->id_lang));
		if (Validate::isLoadedObject($addressDelivery) AND $addressDelivery->id_state)
			$deliveryState = new State((int)($addressDelivery->id_state));
		$carrier = new Carrier($order->id_carrier);
		$history = $order->getHistory($cookie->id_lang);
		$products = $order->getProducts();
		$customizedDatas = Product::getAllCustomizedDatas((int)($order->id_cart));
		Product::addCustomizationPrice($products, $customizedDatas);
		$discounts = $order->getDiscounts();
		$messages = Message::getMessagesByOrderId($order->id, true);
		$states = OrderState::getOrderStates((int)($cookie->id_lang));
		$currency = new Currency($order->id_currency);
		$currentLanguage = new Language((int)($cookie->id_lang));
		$currentState = OrderHistory::getLastOrderState($order->id);
		$sources = ConnectionsSource::getOrderSources($order->id);
		$storico = Db::getInstance()->executeS('SELECT * FROM storico_attivita WHERE id_attivita = '.$order->id.' AND tipo_attivita = "O" ORDER BY data_attivita DESC, desc_attivita DESC');
		$cart = Cart::getCartByOrderId($order->id);
		$ctrl_cart = Db::getInstance()->getValue("SELECT id_cart FROM carrelli_creati WHERE id_cart = ".$cart->id."");
		$note_private = Db::getInstance()->getValue("SELECT note_private FROM carrelli_creati WHERE id_cart = ".$cart->id."");
		$in_carico_a = Db::getInstance()->getValue("SELECT in_carico_a FROM cart WHERE id_cart = ".$cart->id."");
		$riferimento = Db::getInstance()->getValue("SELECT riferimento FROM cart WHERE id_cart = ".$cart->id."");
		$consegna = Db::getInstance()->getValue("SELECT consegna FROM cart WHERE id_cart = ".$cart->id."");
		$note = Db::getInstance()->getValue("SELECT note FROM carrelli_creati WHERE id_cart = ".$cart->id."");
		if($note == '')
		$note = Db::getInstance()->getValue("SELECT note FROM cart WHERE id_cart = ".$cart->id."");
		$esigenze = Db::getInstance()->getValue("SELECT esigenze FROM cart WHERE id_cart = ".$cart->id."");
		$premessa = Db::getInstance()->getValue("SELECT premessa FROM cart WHERE id_cart = ".$cart->id."");
		$risorse = Db::getInstance()->getValue("SELECT risorse FROM cart WHERE id_cart = ".$cart->id."");
		$note_private = Db::getInstance()->getValue("SELECT note_private FROM cart WHERE id_cart = ".$cart->id."");
		$attachment = Db::getInstance()->getValue("SELECT attachment FROM cart WHERE id_cart = ".$cart->id."");
		$id_persona = Db::getInstance()->getValue("SELECT id_persona FROM cart WHERE id_cart = ".$cart->id."");
		$impiegato = Db::getInstance()->getValue("SELECT id_employee FROM cart WHERE id_cart = ".$cart->id."");
		$visualizzato = Db::getInstance()->getValue("SELECT visualizzato FROM cart WHERE id_cart = ".$cart->id."");

		if ($prevOrder = Db::getInstance()->getValue('SELECT id_order FROM '._DB_PREFIX_.'orders WHERE id_order < '.(int)$order->id.' ORDER BY id_order DESC'))
			$prevOrder = '<a href="'.$currentIndex.'&token='.Tools::getValue('token').'&vieworder&id_order='.$prevOrder.'"><img style="width:24px;height:24px" src="../img/admin/arrow-left.png" /></a>';
		if ($nextOrder = Db::getInstance()->getValue('SELECT id_order FROM '._DB_PREFIX_.'orders WHERE id_order > '.(int)$order->id.' ORDER BY id_order ASC'))
			$nextOrder = '<a href="'.$currentIndex.'&token='.Tools::getValue('token').'&vieworder&id_order='.$nextOrder.'"><img style="width:24px;height:24px" src="../img/admin/arrow-right.png" /></a>';


		if ($order->total_paid != $order->total_paid_real)
			echo '<center><span class="warning" style="font-size: 16px">'.AdminOrders::lx('Attenzione: pagato ').' '.Tools::displayPrice($order->total_paid_real, $currency, false).' '.AdminOrders::lx(' invece di ').' '.Tools::displayPrice($order->total_paid, $currency, false).' </span></center><div class="clear"><br /><br /></div>';

		// display bar code if module enabled
		
		$hook = Module::hookExec('invoice', array('id_order' => $order->id));
		if ($hook !== false)
		{
			echo '<div style="float: right; margin: -40px 40px 10px 0;">';
			echo $hook;
			echo '</div><br class="clear" />';
		}
		$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
		
		$amazon = Db::getInstance()->getValue('SELECT id_order_amazon FROM amazon_orders WHERE id_order = '.$order->id);
		$eprice = Db::getInstance()->getValue('SELECT id_order_eprice FROM eprice_orders WHERE id_order = '.$order->id);
		
		$cart_name = Db::getInstance()->getValue('SELECT name FROM cart WHERE id_cart = '.$order->id_cart);
		
		if($amazon != '' && (strpos('AMAZON ID',$cart_name !== false)))
			$style = ';padding:3px; background-color:#000000; color:#ffffff';
		else if($eprice != '')
			$style = ';padding:3px; background-color:#ffff00; color:#000000';
		else
			$style = '';
		
		$ord_fatt = Db::getInstance()->getValue('SELECT id_fattura FROM fattura FORCE INDEX (PRIMARY) WHERE rif_ordine = '.$order->id);
		
		if($ord_fatt > 0)
			$link_fatt = 'https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&token='.$tokenCustomers.'&id_fattura='.$ord_fatt.'&tab-container-1=5&viewfattura&tipo='.Db::getInstance()->getValue('SELECT tipo FROM fattura WHERE id_fattura = '.$ord_fatt).'&updatefattura';
		
		// display order header
		echo '
		<div style="float:left" style="width:100%; ">';
		
		echo '<fieldset style="width: 97%">
			<legend><img src="../img/admin/charged_ok.gif" /> '.AdminOrders::lx('Informazioni ordine').'</legend>
			<table>
			<tr style="width:800px">
			<td style="width:300px"><em>ID Ordine</em></td>
			<td style="width:200px"><em>Cliente</em></td>
			<td style="width:100px"><em>Data Ordine</em></td>
			<td style="width:200px"><em>Rif. Ordine</em></td>
			</tr>
			<tr>
			<td>
				'.(isset($_GET['viewcustomer']) ? '' : '').'
				<strong style="font-size:20px">'.$order->id.'</strong>
				'.(isset($_GET['viewcustomer']) ? '' : '').' '.($amazon != '' ? 'Ordine Amazon n. '.$amazon : ($eprice != '' ? 'Ordine ePrice n. '.$eprice : '')).'</td>
			<td><strong style="font-size:14px"><a href="?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'"> '.($customer->is_company == 1 ? $customer->company : $customer->firstname.' '.$customer->lastname).'</a></span> ('.AdminOrders::lx('#').$customer->id.'</strong>)</td>
			<td><strong>'.date("d/m/Y, H:i:s", strtotime($order->date_add)).'</strong></td>
			<td>
			<div style="display:block; position:relative; height:25px;">
			<form action="ajax.php" method="post" onsubmit="saveRifOrdine(); return false;" id="form_rif_ordine">
			 <select style="position:absolute; top:1px; height:22px; width:135px; left:0px" onchange="this.nextElementSibling.value=this.value">
				<option id="rif_ordine_prima_opzione" value=""></option>
				<option value="NC">NC</option>
			</select>
			<input id="rif_ordine" style="position:absolute; top:2px; left:1px; border:0px; width:107px" name="rif_ordine" type="text" value="'.Db::getInstance()->getValue('SELECT rif_ordine FROM orders WHERE id_order = '.$order->id).'" />
			<input type="submit" id="submitRifOrdine" class="button" style="position:absolute; top:-2px; left:140px; "  value="Salva"  /><div  style="position:absolute; top:2px; left:200px; " id="rif_ordine_feedback"></div>
			
			
			
			<script type="text/javascript">
				function saveRifOrdine()
				{	
					tinyMCE.triggerSave();
					$("#rif_ordine_feedback").html("<img src=\"../img/loader.gif\" />").show();

					var rif_ordine = $("#rif_ordine").val();
					$.post("ajax.php", {submitRifOrdine:1,id_customer:'.(int)$customer->id.',id_order:'.$order->id.',rif_ordine:rif_ordine}, function (r) {
						$("#note_feedback").html("").hide();
						if (r == "ok")
						{
							$("#rif_ordine_feedback").html("<img src=\"..\/img\/admin\/ok.gif\" alt=\"OK\" title=\"OK\"  \/>").fadeIn(400);
						}
						else if (r == "error:validation")
							$("#rif_ordine_feedback").html("<b style=\"color:red\">Errore</b>").fadeIn(400);
						else if (r == "error:update")
							$("#rif_ordine_feedback").html("<b style=\"color:red\">Errore</b>").fadeIn(400);
						$("#rif_ordine_feedback").fadeOut(5000);
					});
				}
			</script>
			<div style="clear:both"></div>
			</form></div><div style="clear:both"></div>
			</td>
			</tr>
			'.($ord_fatt > 0 ? '<tr><td colspan="5"><span style="color:red"><strong>Questo ordine &egrave; stato fatturato (fattura n. <a href="'.$link_fatt.'" target="_blank">'.$ord_fatt.'</a>)</span></td></tr>' : '').'
			</table>
			</fieldset>
		';
		
		echo '
			<div style="width:900px; float:left"><br />
			<a href="index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&tab-container-1=1&token='.$tokenCustomers.'"><img src="../img/admin/employee.gif" alt="'.AdminOrders::lx('Apri scheda anagrafica').'" /> '.AdminOrders::lx('Apri scheda anagrafica').'</a> - 
			<a href="index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&viewmessage&aprinuovomessaggio&rif_ord='.$order->id.'&tab-container-1=7&token='.$tokenCustomers.'"><img src="../img/admin/email.gif" alt="'.AdminOrders::lx('Invia un messaggio al cliente').'" /> '.AdminOrders::lx('Invia un messaggio al cliente').'</a> - 
			
				'.((($currentState->invoice OR $order->invoice_number) AND count($products))
					? '<a href="ajax.php?tab=AdminOrders&id_order='.$order->id.'&vieworder&getOrderPDF&token='.Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee).'" onclick="window.onbeforeunload = null"><img src="../img/admin/charged_ok.gif" alt="'.AdminOrders::lx('Apri PDF').'" /> '.AdminOrders::lx('Apri PDF').'</a>'
					: '<img src="../img/admin/charged_ko.gif" alt="'.AdminOrders::lx('No riepilogo PDF').'" /> '.AdminOrders::lx('No riepilogo PDF')).' -
				<a href="javascript:window.print()"><img src="../img/admin/printer.gif" alt="'.AdminOrders::lx('Print order').'" title="'.AdminOrders::lx('Stampa').'" /> '.AdminOrders::lx('Stampa').'</a>
				
				'.($cookie->profile != 8 ? '<a href="index.php?tab=AdminOrders&id_order='.$order->id.'&deleteorder&token='.Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee).'" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" ><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /> Cancella ordine</a>' : '').'
				';
				
				if($cookie->id_employee == 7 || $cookie->id_employee == 6 || $cookie->id_employee == 22)
				{
					$nmft = Db::getInstance()->getValue('SELECT nm FROM ff WHERE rd = '.$order->id);
					
					echo '
					<a href="ajax.php?tab=AdminOrders&id_order='.$order->id.'&vieworder&getFF&token='.Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee).'" target="_blank"  onclick="window.onbeforeunload = null"><img src="../img/admin/details.gif" alt="FF" title="FF" />&nbsp;'.($nmft > 0 ? 'Apri' : 'Crea').' fattura pro forma (PDF) '.($nmft > 0 ? ' - '.str_pad($nmft, 10, '0', STR_PAD_LEFT).'-'.Db::getInstance()->getValue('SELECT an FROM ff WHERE rd = '.$order->id) : '').'</a> ';
						
				}
				
				echo '<br /><br />';
			
			if(Tools::getIsset('submitCollegaAttivita'))
			{
				$id_thread = substr(Tools::getValue('coll_partenza'),1);
				switch(substr(Tools::getValue('coll_partenza'), 0,1))
				{
					case 'T': Db::getInstance()->execute('UPDATE customer_thread SET riferimento = "'.Tools::getValue('coll_destinazione').'" WHERE id_customer_thread = '.$id_thread.''); break;
					case 'P': Db::getInstance()->execute('UPDATE form_prevendita_thread SET riferimento = "'.Tools::getValue('coll_destinazione').'" WHERE id_thread = '.$id_thread.''); break;
					case 'A': Db::getInstance()->execute('UPDATE action_thread SET riferimento = "'.Tools::getValue('coll_destinazione').'" WHERE id_action = '.$id_thread.''); break;
					default: ''; break;
				}
			}
		
			echo '
			<link rel="stylesheet" href="jquery.treeview.css" type="text/css" />


			<script type="text/javascript" src="jquery.treeview.js"></script>

			<script type="text/javascript">

			// build treeview after web page has been loaded

			$(document).ready(function(){

				$(".tree-menu").treeview();
				
				$(".tasti-apertura").mouseover(function() {
					$(this).children(".menu-apertura").show();
				}).mouseout(function() {
					$(this).children(".menu-apertura").hide();
				});
				
				$(".tasti-apertura-in").mouseover(function() {
					$(this).children(".menu-apertura-in").show();
				}).mouseout(function() {
					$(this).children(".menu-apertura-in").hide();
				});


			});

			</script>
		';
	
			echo '
			<div class="tasti-apertura">
			<a href="ajax.php?tab=AdminOrders&id_order='.$order->id.'&vieworder&getDDT&token='.Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee).'"  class="button" style="display:block"  onclick="window.onbeforeunload = null"><img src="../img/admin/pdf.gif" alt="DDT" title="DDT" />&nbsp;Apri DDT in formato PDF</a> <!-- | <a href="index.php?tab=AdminOrders&id_order='.$order->id.'&vieworder&printDDT&token='.Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee).'" class="button"><img src="../img/admin/invoice.gif" alt="DDT" title="DDT" />Stampa direttamente DDT</a> --> </div>
			
			<div class="tasti-apertura">
			<a class="button" style="display:block" href="#"><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" />&nbsp;&nbsp;&nbsp;Aggiungi nuova azione padre</a>
				<div class="menu-apertura">
					<ul class="dropdown">
						<li class="tasti-apertura-in"><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewticket&aprinuovoticket&token='.$tokenCustomers.'&tab-container-1=6"><img src="../img/admin/AdminTools.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;Ticket</a>
						<div class="menu-apertura-in" style="position:absolute; left:100px">
							<ul class="dropdown">
								<li><a style="color:#000" href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewticket&aprinuovoticket&token='.$tokenCustomers.'&tipo-ticket=2&tab-container-1=6"><img src="../img/admin/icons/contabilita.gif" alt="Contabilita" title="Contabilita" />&nbsp;&nbsp;&nbsp;Contabilit&agrave;</a></li>
								
								<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewticket&aprinuovoticket&token='.$tokenCustomers.'&tipo-ticket=8&tab-container-1=6"><img src="../img/admin/icons/ordini.gif" alt="Ordini" title="Ordini" />&nbsp;&nbsp;&nbsp;Ordini</a></li>
								
								<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewticket&aprinuovoticket&token='.$tokenCustomers.'&tipo-ticket=4&tab-container-1=6"><img src="../img/admin/icons/assistenza.gif" alt="Assistenza tecnica" title="Assistenza tecnica" />&nbsp;&nbsp;&nbsp;Assistenza tecnica</a></li>
								
								<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewticket&aprinuovoticket&token='.$tokenCustomers.'&tipo-ticket=9&tab-container-1=6"><img src="../img/admin/icons/rma.gif" alt="RMA" title="RMA" />&nbsp;&nbsp;&nbsp;RMA</a></li>
								
								<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewticket&aprinuovoticket&token='.$tokenCustomers.'&tipo-ticket=3&tab-container-1=6"><img src="../img/admin/icons/rivenditori.gif" alt="Rivenditori" title="Rivenditori" />&nbsp;&nbsp;&nbsp;Rivenditori</a></li>
								
							</ul>
							</div>
						</li>
						<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewmessage&aprinuovomessaggio&token='.$tokenCustomers.'&tab-container-1=7"><img src="../img/admin/email.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;Messaggio</a></li>
						<li class="tasti-apertura-in"><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewaction&aprinuovaazione&token='.$tokenCustomers.'&tab-container-1=10"><img src="../img/admin/return.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;To-do</a>
							<div class="menu-apertura-in" style="position:absolute; left:100px">
								<ul class="dropdown">
									<li><a style="color:#000" href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewaction&aprinuovaazione&token='.$tokenCustomers.'&tipo-todo=Attivita&tab-container-1=10"><img src="../img/admin/icons/attivita.gif" alt="Attivita" title="Attivita" />&nbsp;&nbsp;&nbsp;Attivit&agrave;</a></li>
									
									<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewaction&aprinuovaazione&token='.$tokenCustomers.'&tipo-todo=Telefonata&tab-container-1=10"><img src="../img/admin/icons/telefonata.gif" alt="Telefonata" title="Telefonata" />&nbsp;&nbsp;&nbsp;Telefonata</a></li>
									
									<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewaction&aprinuovaazione&token='.$tokenCustomers.'&tipo-todo=Visita&tab-container-1=10"><img src="../img/admin/icons/visita.gif" alt="Visita" title="Visita" />&nbsp;&nbsp;&nbsp;Visita</a></li>
									
									<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewaction&aprinuovaazione&token='.$tokenCustomers.'&tipo-todo=Caso&tab-container-1=10"><img src="../img/admin/icons/caso.gif" alt="Caso" title="Caso" />&nbsp;&nbsp;&nbsp;Caso</a></li>
									
									<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewaction&aprinuovaazione&token='.$tokenCustomers.'&tipo-todo=Intervento&tab-container-1=10"><img src="../img/admin/icons/intervento.gif" alt="Intervento" title="Intervento" />&nbsp;&nbsp;&nbsp;Intervento</a></li>
									
									<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewaction&aprinuovaazione&token='.$tokenCustomers.'&tipo-todo=Richiesta_RMA&tab-container-1=10"><img src="../img/admin/icons/richiesta_rma.gif" alt="Richiesta RMA" title="Richiesta RMA" />&nbsp;&nbsp;&nbsp;Richiesta RMA</a></li>
								</ul>
							</div>
						
						</li>
						<li class="tasti-apertura-in"><a href="index.php?tab=AdminCustomers&viewcart&editcart&createnew&provvisorio=1&id_customer='.$order->id_customer.'&viewcustomer&token='.$tokenCustomers.'&tab-container-1=4&preventivo=1#modifica-carrello" onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }" id="link-preventivo"><img src="../img/admin/charged_ok.gif" alt="Nuovo preventivo" title="Nuovo preventivo" /> Preventivo</a>
						';
							
							$orders_telegest = Db::getInstance()->getValue('SELECT count(id_order) FROM orders WHERE id_customer = '.$cart->id_customer);
							if($orders_telegest > 0)
							{
								$orders_telegest_a = Db::getInstance()->executeS('SELECT DISTINCT id_order FROM orders WHERE id_customer = '.$cart->id_customer);
								foreach($orders_telegest_a as $ot)
								{
									$orders_t_string .= ($otz % 9 == 0 ? '</tr><tr>' : '').'<td><input type="checkbox" class="orders_telegest_c" name="orders_telegest[]" value="'.$ot['id_order'].'" /> '.$ot['id_order'].'&nbsp;&nbsp;&nbsp;&nbsp;</td>';
									$otz++;
								}	
									
								echo '
									<div class="hider" id="hider_telegest_form_2" style="display:none"></div>
									<div class="popup_box" id="popup_telegest_form_2" style="display:none; width:50%; text-align:center; z-index:999999999"><form method="post" action="index.php?tab=AdminCustomers&viewcart&editcart&provvisorio=1&id_customer='.$cart->id_customer.'&amp;copy_to='.$cart->id_customer.'&copia_carrello_conferma=y&id_cart=84366&viewcustomer&token='.$tokenCustomers.'&tab-container-1=4&preventivo=1&rif_prev='.$rif.'#modifica-carrello"> Seleziona gli ordini su cui applicare la telegestione: <table><tr>'.$orders_t_string.'</tr></table><br />
									<input type="checkbox" onchange="$(\'.orders_telegest_c\').not(this).prop(\'checked\', this.checked);" />Seleziona tutti<br /><br />
									<input type="submit" class="button" value="Conferma" />
									
									<button type="button" class="button"  onclick="$(\'#hider_telegest_form\').hide(); $(\'#popup_telegest_form\').hide();">Chiudi</button>
									
									</form>
									</div>
									<div class="menu-apertura-in" style="position:absolute; left:100px">
										<ul class="dropdown">
											<li><a style="color:#000" onclick="$(\'#hider_telegest_form_2\').show(); $(\'#popup_telegest_form_2\').show(); "><img src="../img/admin/icons/attivita.gif" alt="Telegestione annuale" title="Telegestione annuale" />&nbsp;&nbsp;&nbsp;Telegestione annuale</a></li>
										</ul>
									</div>
								';
							}
							echo '
						</li>
						
						<li><a href="index.php?tab=AdminCustomers&viewcart&editcart&createnew&provvisorio=1&id_customer='.$order->id_customer.'&viewcustomer&token='.$tokenCustomers.'&tab-container-1=4&preventivo=2#modifica-carrello" onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }" id="link-ordine-manuale"><img src="../img/admin/cart.gif" alt="Nuovo ordine manuale" title="Nuovo ordine manuale" /> Ordine manuale</a></li>
						
						<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&nuovobdl&token='.$tokenCustomers.'&tab-container-1=14"><img src="../img/admin/prefs.gif" alt="Buono di lavoro" title="Buono di lavoro" />&nbsp;&nbsp;&nbsp;Buono di lavoro (BDL)</a></li>
						
					</ul>
				</div>
			</div>';
			
			if(Tools::getIsset('id_customer_thread') || Tools::getIsset('id_thread') || Tools::getIsset('id_mex') || Tools::getIsset('id_action') || Tools::getIsset('id_order') || Tools::getIsset('id_cart'))
			{
				if(Tools::getIsset('id_customer_thread'))
					$rif = 'T'.Tools::getValue('id_customer_thread');
				else if(Tools::getIsset('id_thread'))
					$rif = 'P'.Tools::getValue('id_thread');
				else if(Tools::getIsset('id_mex'))
					$rif = 'T'.Tools::getValue('id_mex');
				else if(Tools::getIsset('id_action'))
					$rif = 'A'.Tools::getValue('id_action');
				else if(Tools::getIsset('id_order'))
					$rif = 'O'.Tools::getValue('id_order');
				else if(Tools::getIsset('id_cart'))
					$rif = 'C'.Tools::getValue('id_cart');
				
				
				echo '
				<div class="tasti-apertura">
					<a style="display:block" class="button" href="#"><img src="../img/admin/tab-groups.gif" alt="Aggiungi figlia" title="Aggiungi figlia" />&nbsp;&nbsp;&nbsp;Aggiungi azione figlia</a>
					<div class="menu-apertura">
						<ul class="dropdown">
							<li class="tasti-apertura-in"><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewticket&aprinuovoticket&riferimento='.$rif.'&token='.$tokenCustomers.'&tab-container-1=6"><img src="../img/admin/AdminTools.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;Ticket</a>
								<div class="menu-apertura-in" style="position:absolute; left:100px">
									<ul class="dropdown">
										<li><a style="color:#000" href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewticket&aprinuovoticket&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo-ticket=2&tab-container-1=6"><img src="../img/admin/icons/contabilita.gif" alt="Contabilita" title="Contabilita" />&nbsp;&nbsp;&nbsp;Contabilit&agrave;</a></li>
										
										<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewticket&aprinuovoticket&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo-ticket=8&tab-container-1=6"><img src="../img/admin/icons/ordini.gif" alt="Ordini" title="Ordini" />&nbsp;&nbsp;&nbsp;Ordini</a></li>
										
										<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewticket&aprinuovoticket&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo-ticket=4&tab-container-1=6"><img src="../img/admin/icons/assistenza.gif" alt="Assistenza tecnica" title="Assistenza tecnica" />&nbsp;&nbsp;&nbsp;Assistenza tecnica</a></li>
										
										<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewticket&aprinuovoticket&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo-ticket=9&tab-container-1=6"><img src="../img/admin/icons/rma.gif" alt="RMA" title="RMA" />&nbsp;&nbsp;&nbsp;RMA</a></li>
										
										<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewticket&aprinuovoticket&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo-ticket=3&tab-container-1=6"><img src="../img/admin/icons/rivenditori.gif" alt="Rivenditori" title="Rivenditori" />&nbsp;&nbsp;&nbsp;Rivenditori</a></li>
										
									</ul>
								</div>
							</li>
							<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewmessage&aprinuovomessaggio&riferimento='.$rif.'&token='.$tokenCustomers.'&tab-container-1=7"><img src="../img/admin/email.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;Messaggio</a></li>
							<li class="tasti-apertura-in"><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$tokenCustomers.'&tab-container-1=10"><img src="../img/admin/return.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;To-do</a>
								<div class="menu-apertura-in" style="position:absolute; left:100px">
									<ul class="dropdown">
										<li><a style="color:#000" href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo-todo=Attivita&tab-container-1=10"><img src="../img/admin/icons/attivita.gif" alt="Attivita" title="Attivita" />&nbsp;&nbsp;&nbsp;Attivit&agrave;</a></li>
										
										<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo-todo=Telefonata&tab-container-1=10"><img src="../img/admin/icons/telefonata.gif" alt="Telefonata" title="Telefonata" />&nbsp;&nbsp;&nbsp;Telefonata</a></li>
										
										<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo-todo=Visita&tab-container-1=10"><img src="../img/admin/icons/visita.gif" alt="Visita" title="Visita" />&nbsp;&nbsp;&nbsp;Visita</a></li>
										
										<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo-todo=Caso&tab-container-1=10"><img src="../img/admin/icons/caso.gif" alt="Caso" title="Caso" />&nbsp;&nbsp;&nbsp;Caso</a></li>
										
										<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo-todo=Intervento&tab-container-1=10"><img src="../img/admin/icons/intervento.gif" alt="Intervento" title="Intervento" />&nbsp;&nbsp;&nbsp;Intervento</a></li>
										
										<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$tokenCustomers.'&tipo-todo=Richiesta_RMA&tab-container-1=10"><img src="../img/admin/icons/richiesta_rma.gif" alt="Richiesta RMA" title="Richiesta RMA" />&nbsp;&nbsp;&nbsp;Richiesta RMA</a></li>
									</ul>
								</div>
							</li>
							<li class="tasti-apertura-in"><a href="index.php?tab=AdminCustomers&viewcart&editcart&createnew&provvisorio=1&id_customer='.$order->id_customer.'&viewcustomer&token='.$tokenCustomers.'&tab-container-1=4&preventivo=1&rif_prev='.$rif.'#modifica-carrello" onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }" id="link-preventivo"><img src="../img/admin/charged_ok.gif" alt="Nuovo preventivo" title="Nuovo preventivo" /> Preventivo</a>
								';
							
							$orders_telegest = Db::getInstance()->getValue('SELECT count(id_order) FROM orders WHERE id_customer = '.$cart->id_customer);
							if($orders_telegest > 0)
							{
								$orders_telegest_a = Db::getInstance()->executeS('SELECT DISTINCT id_order FROM orders WHERE id_customer = '.$cart->id_customer);
								foreach($orders_telegest_a as $ot)
									$orders_t_stringb .= '<input type="checkbox" name="orders_telegest[]" value="'.$ot['id_order'].'" /> '.$ot['id_order'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
									
								echo '
									<div class="hider" id="hider_telegest_form" style="display:none"></div>
									<div class="popup_box" id="popup_telegest_form" style="display:none; width:50%; text-align:center; z-index:999999999"><form method="post" action="index.php?tab=AdminCustomers&viewcart&editcart&provvisorio=1&id_customer='.$cart->id_customer.'&amp;copy_to='.$cart->id_customer.'&copia_carrello_conferma=y&id_cart=84366&viewcustomer&token='.$tokenCustomers.'&tab-container-1=4&preventivo=1&rif_prev='.$rif.'#modifica-carrello"> Seleziona gli ordini su cui applicare la telegestione: '.$orders_t_stringb.'<br /><br />
									
									<input type="submit" class="button" value="Conferma" />
									
									<button type="button" class="button"  onclick="$(\'#hider_telegest_form\').hide(); $(\'#popup_telegest_form\').hide();">Chiudi</button>
									
									</form>
									</div>
									<div class="menu-apertura-in" style="position:absolute; left:100px">
										<ul class="dropdown">
											<li><a style="color:#000" onclick="$(\'#hider_telegest_form\').show(); $(\'#popup_telegest_form\').show(); "><img src="../img/admin/icons/attivita.gif" alt="Telegestione annuale" title="Telegestione annuale" />&nbsp;&nbsp;&nbsp;Telegestione annuale</a></li>
										</ul>
									</div>
								';
							}
							echo '
							</li> 
							
							<li><a href="index.php?tab=AdminCustomers&viewcart&editcart&createnew&provvisorio=1&id_customer='.$order->id_customer.'&viewcustomer&token='.$tokenCustomers.'&tab-container-1=4&preventivo=2&rif_prev='.$rif.'#modifica-carrello" onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }" id="link-ordine-manuale"><img src="../img/admin/cart.gif" alt="Nuovo ordine manuale" title="Nuovo ordine manuale" /> Nuovo ordine manuale</a></li> 
							
							<li><a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&nuovobdl&token='.$tokenCustomers.'&riferimento='.$rif.'&tab-container-1=14"><img src="../img/admin/prefs.gif" alt="Buono di lavoro" title="Buono di lavoro" />&nbsp;&nbsp;&nbsp;Buono di lavoro (BDL)</a></li>
							
						</ul>
					</div>
				</div>';
			}
			
			echo '
			&nbsp;&nbsp;&nbsp;<a id="invio_mail_outlook_t" href="#" onclick="window.location = \'mailto:\'+document.getElementById(\'selezione_outlook_t\').value" style="display:block;float:left;margin-right:0px" class="button" ><img src="../img/admin/outlook.gif" alt="Outlook" title="Outlook" />&nbsp;&nbsp;&nbsp;Invia mail con Outlook a: </a>
				<select name="selezione_outlook_t" id="selezione_outlook_t" onchange="window.location = \'mailto:\'+this.value" style="display:block;float:left;margin-right:10px; height:24px;width:150px" >
				<option value="">-- Seleziona mail --</option>
				';
				$le_mail = Db::getInstance()->executeS("SELECT id_persona, firstname, lastname, email FROM persone WHERE id_customer = ".$order->id_customer." AND email != ''");

				foreach ($le_mail as $la_mail) {
								
					echo "<option value='".$la_mail['email']."'>".$la_mail['email']."</option>";
							
				}
				
				echo '</select><br /><br />';
			
			if($cookie->id_employee == 7 || $cookie->id_employee == 2 || $cookie->id_employee == 3  || $cookie->id_employee == 14 || $cookie->id_employee == 19 || $cookie->id_employee == 12 || $cookie->id_employee == 21)
			{	
				echo '<script type="text/javascript">
				function updSpringOrder(id)
				{
					var updSpringOrder="no";
					if(document.getElementById("spring_"+id+"").checked == true)
						updSpringOrder="yes";
					else
						updSpringOrder="no";
					
					$.ajax({
					  url:"ajax.php?updSpringOrder="+updSpringOrder,
					  type: "POST",
					  data: { id_order: id
					  },
					  success:function(r){
						// alert(r);
					  },
					  error: function(xhr,stato,errori){
						 alert("Errore durante l\'operazione:"+xhr.status);
					  }
					});
					
					
				}</script>';
				
				$spring_v = Db::getInstance()->getValue('SELECT spring FROM orders WHERE id_order = '.$order->id);
				$verificato_da = Db::getInstance()->getValue('select verificato_da from orders where id_order = '.$order->id);
				
				echo '<br />Verificato? <input type="checkbox" name="spring_'.$order->id.'" id="spring_'.$order->id.'" '.($spring_v == 1 ? 'checked="checked"' : '').' onchange="updSpringOrder('.$order->id.'); return false;" /> '.($spring_v == 1 && $verificato_da > 0 ? 'da '.Db::getInstance()->getValue('SELECT concat(firstname," ",lastname) from employee where id_employee = '.$verificato_da) : '').'<br />';
			}
			
			if($cookie->id_employee == 7 || $cookie->id_employee == 2 || $cookie->id_employee == 14 || $cookie->id_employee == 19 || $cookie->id_employee == 12 || $cookie->id_employee == 21)
			{
				echo '<script type="text/javascript">
				function updCheckPI(id)
				{
					var updCheckPI="no";
					if(document.getElementById("check_pi_"+id+"").checked == true)
						updCheckPI="yes";
					else
						updCheckPI="no";
					
					$.ajax({
					  url:"ajax.php?updCheckPI="+updCheckPI,
					  type: "POST",
					  data: { id_order: id
					  },
					  success:function(r){
						// alert(r);
					  },
					  error: function(xhr,stato,errori){
						 alert("Errore durante l\'operazione:"+xhr.status);
					  }
					});
					
					
				}</script>';
				
				$check_pi_v = Db::getInstance()->getValue('SELECT check_pi FROM orders WHERE id_order = '.$order->id);
				
				echo '<br />P.I.? <input type="checkbox" name="check_pi_'.$order->id.'" id="check_pi_'.$order->id.'" '.($check_pi_v == 1 ? 'checked="checked"' : '').' onchange="updCheckPI('.$order->id.'); return false;" /><br />';
			}
			
			if($order->payment == 'Carta Amazon' && ($cookie->id_employee == 2 || $cookie->id_employee == 14))
			{
				echo '<script type="text/javascript">
				function updPagAmzRic(id)
				{
					var updPagAmzRic="no";
					if(document.getElementById("pag_amz_ric_"+id+"").checked == true)
						updPagAmzRic="yes";
					else
						updPagAmzRic="no";
					
					$.ajax({
					  url:"ajax.php?updPagAmzRic="+updPagAmzRic,
					  type: "POST",
					  data: { id_order: id
					  },
					  success:function(r){
						// alert(r);
					  },
					  error: function(xhr,stato,errori){
						 alert("Errore durante l\'operazione:"+xhr.status);
					  }
					});
					
					
				}</script>';
				
				$pag_amz_ric_v = Db::getInstance()->getValue('SELECT pag_amz_ric FROM orders WHERE id_order = '.$order->id);
				
				echo '<br />Pagamento amazon ricevuto? <input type="checkbox" name="pag_amz_ric_'.$order->id.'" id="pag_amz_ric_'.$order->id.'" '.($pag_amz_ric_v == 1 ? 'checked="checked"' : '').' onchange="updPagAmzRic('.$order->id.'); return false;" /><br />';
			}
		
			if($cookie->id_employee == 14)
			{
				echo '<script type="text/javascript">
				function updCheckEsolv(id)
				{
					var updCheckEsolv="no";
					if(document.getElementById("check_esolver_"+id+"").checked == true)
						updCheckEsolv="yes";
					else
						updCheckEsolv="no";
					
					$.ajax({
					  url:"ajax.php?updCheckEsolv="+updCheckEsolv,
					  type: "POST",
					  data: { id_order: id
					  },
					  success:function(r){
						// alert(r);
					  },
					  error: function(xhr,stato,errori){
						 alert("Errore durante l\'operazione:"+xhr.status);
					  }
					});
					
					
				}</script>';
				
				$check_esolver_v = Db::getInstance()->getValue('SELECT check_esolver FROM orders WHERE id_order = '.$order->id);
				
				echo '<br />eSolver? <input type="checkbox" name="check_esolver_'.$order->id.'" id="check_esolver_'.$order->id.'" '.($check_esolver_v == 1 ? 'checked="checked"' : '').' onchange="updCheckEsolv('.$order->id.'); return false;" /><br />';
			}
			
			echo '
			</div>
			<div class="clear">&nbsp;</div>';
			
			$num_prv = Db::getInstance()->getValue('SELECT count(*) FROM form_prevendita_thread WHERE status = "open" AND id_customer = '.$order->id_customer);
			$num_tkt = Db::getInstance()->getValue('SELECT count(*) FROM customer_thread ct	LEFT JOIN customer_message cm ON ct.id_customer_thread = cm.id_customer_thread
			WHERE (status = "open") AND (id_contact != 7 OR (cm.id_employee = 0 AND ct.id_contact = 7)	) AND id_customer = '.$order->id_customer);
			$num_act = Db::getInstance()->getValue('SELECT count(*) FROM action_thread WHERE status = "open" AND id_customer = '.$order->id_customer);

			if($num_prv+$num_tkt+$num_act > 0 || $note_private != '' || $note != '')
			{	
				echo '<div class="error">Attenzione: '.($num_prv+$num_tkt+$num_act > 0 != '' ? '<a href="index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&tab-container-1=1&token='.$tokenCustomers.'" target="_blank">ticket/azioni in sospeso</a> - ' : '').' 
				'.($note_private != '' ? 'note private in quest\'ordine - ' : '').' 
				'.($note != '' ? 'note in quest\'ordine - ' : '').' 
				</div>';
			}

			$ultime_attivita_ord = Db::getInstance()->executeS('SELECT sa.* FROM (SELECT * FROM action_thread WHERE status != "closed" AND riferimento LIKE "%O'.$order->id.'") ac JOIN (SELECT * from storico_attivita WHERE tipo_attivita = "A") sa ON ac.id_action = sa.id_attivita GROUP BY sa.id_attivita
			UNION
			SELECT sa.* FROM (SELECT * FROM customer_thread WHERE status != "closed" AND riferimento LIKE "%O'.$order->id.'") ac JOIN (SELECT * from storico_attivita WHERE tipo_attivita = "T") sa ON ac.id_customer_thread = sa.id_attivita  GROUP BY sa.id_attivita ');
							
			if(count($ultime_attivita_ord) > 0)
			{
				echo '<div style="height:200px; overflow-y:scroll"><table id="table-ultime-att" class="table tablesorter Xpaginated Xpaginated-5" style="width:100%"><thead><tr><th style="width:100px">Tipo attivit&agrave;</th><th style="width:100px">Id attivit&agrave;</th><th colspan="3" style="width:200px">Cliente</th><th style="width:100px" colspan="2">In carico a</th><th style="width:100px">Status</th><th style="width:100px" data-sorter="shortDate" data-date-format="ddmmyyyy">Data attivit&agrave;</th></tr></thead><tbody>';
				
				foreach($ultime_attivita_ord as $uai)
				{
					if($uai['tipo_attivita'] == 'T')
					{
						$id_contact = Db::getInstance()->getValue('SELECT id_contact FROM customer_thread WHERE id_customer_thread = '.$uai['id_attivita']);
						/*if($id_contact == 7)
							$uai['tipo_attivita'] = 'M';*/
					}	
					switch($uai['tipo_attivita'])
					{
						case 'A': $tipo_attivita_uai = 'Azione'; $table = 'action_thread'; $attivita_key = 'id_action'; $id_attivita = 'TODO'.$uai['id_attivita']; break;
						case 'P': $tipo_attivita_uai = 'Richiesta preventivo'; $table = 'form_prevendita_thread'; $attivita_key = 'id_thread';  $id_attivita = Customer::trovaSigla($uai['id_attivita'], 'P'); break;
						case 'C': $tipo_attivita_uai = 'Carrello'; $table = 'cart'; $attivita_key = 'id_cart'; $id_attivita = $uai['id_attivita']; break;
						case 'O': $tipo_attivita_uai = 'Ordine'; $table = 'orders'; $attivita_key = 'id_order'; $id_attivita = $uai['id_attivita']; break;
						case 'L': $tipo_attivita_uai = 'BDL'; $table = 'bdl'; $attivita_key = 'id_bdl';  $id_attivita = 'BDL'.$uai['id_attivita']; break;
						case 'T': $tipo_attivita_uai = 'Ticket'; $table = 'customer_thread'; $attivita_key = 'id_customer_thread';  $id_attivita = Customer::trovaSigla($uai['id_attivita'], 'T'); break;	
						case 'M': $tipo_attivita_uai = 'Messaggio'; $table = 'customer_thread'; $attivita_key = 'id_customer_thread';  $id_attivita = Customer::trovaSigla($uai['id_attivita'], 'M'); break;	
					}
					
					$id_customer = Db::getInstance()->getValue('SELECT id_customer FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']);
					
					$uai_link = 'index.php?tab=AdminCustomers&id_customer='.$id_customer.'&viewcustomer';
					$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
					
					$uai_employee_act = Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname,1),".") impiegato FROM employee WHERE id_employee = '.$uai['id_employee']);
					
					switch($uai['tipo_attivita'])
					{
						case 'A': $uai_link.= '&id_action='.$uai['id_attivita'].'&viewaction&token='.$tokenCustomers.'&tab-container-1=10'; $uai_status = Db::getInstance()->getValue('SELECT status FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); $in_carico_a = Db::getInstance()->getValue('SELECT action_to FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); break;
						case 'P': $uai_link.= '&id_thread='.$uai['id_attivita'].'&viewpreventivo&token='.$tokenCustomers.'&tab-container-1=7'; $uai_status = Db::getInstance()->getValue('SELECT status FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); $in_carico_a = Db::getInstance()->getValue('SELECT id_employee FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); break;
						case 'C': $uai_link.= '&id_cart='.$uai['id_attivita'].'&viewcart&token='.$tokenCustomers.'&tab-container-1=4'; $uai_status = Db::getInstance()->getValue('SELECT visualizzato FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); $in_carico_a = Db::getInstance()->getValue('SELECT in_carico_a FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); break;
						case 'O': $uai_link.= '&id_order='.$uai['id_attivita'].'&vieworder&token='.$tokenCustomers.'&tab-container-1=4'; $uai_status = Db::getInstance()->getValue('SELECT name FROM order_state_lang WHERE id_lang = 5 AND id_order_state = (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = '.$uai['id_attivita'].' GROUP BY moh.`id_order`))'); $in_carico_a = ''; break;
						case 'L': $uai_link.= '&edit_bdl='.$uai['id_attivita'].'&viewbdl&token='.$tokenCustomers.'&tab-container-1=14'; $uai_status = Db::getInstance()->getValue('SELECT (CASE WHEN a.rif_ordine > 0 THEN "closed" WHEN a.id_bdl < 384 THEN "closed" WHEN a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 0 THEN "pending1"	WHEN a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 1	THEN "closed" WHEN a.invio_controllo = 2 AND a.invio_contabilita = 2 THEN "closed" WHEN a.rif_ordine = 0 AND a.invio_controllo = 0 THEN "open" ELSE "open" END) stato  FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); $in_carico_a = Db::getInstance()->getValue('SELECT effettuato_da FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); break;
						case 'T': $uai_link.= '&id_customer_thread='.$uai['id_attivita'].'&viewticket&token='.$tokenCustomers.'&tab-container-1=6'; $uai_status = Db::getInstance()->getValue('SELECT status FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); $in_carico_a = Db::getInstance()->getValue('SELECT id_employee FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); break;
						case 'M': $uai_link.= '&id_mex='.$uai['id_attivita'].'&viewmessage&token='.$tokenCustomers.'&tab-container-1=7'; $uai_status = Db::getInstance()->getValue('SELECT status FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); $in_carico_a = Db::getInstance()->getValue('SELECT id_employee FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); break;	
						
					}	
					
					$uai_employee = Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname,1),".") impiegato FROM employee WHERE id_employee = '.$in_carico_a);
					
					switch($uai_status)
					{
						case 'open': $uai_status = '<img src="../img/admin/status_red.gif" alt="Aperto" />Aperto'; break;
						case 'closed': $uai_status = '<img src="../img/admin/status_green.gif" alt="Chiuso" /> Chiuso'; break;
						case 'pending1': $uai_status = '<img src="../img/admin/status_orange.gif" alt="In lavorazione" />In lavorazione'; break;
						case 'pending2': $uai_status = '<img src="../img/admin/status_orange.gif" alt="In lavorazione" />In lavorazione'; break;
						case 1: $uai_status = '<img src="../img/admin/enabled.gif" alt="Letto" title="Letto" />Letto'; break;
						case 0: $uai_status = '<img src="../img/admin/red_no.gif" alt="Non letto" title="Non letto" />Non letto'; break;
						case 999: $uai_status = 'Carrello Amazon convertito'; break;
						default: $uai_status; break;
						
					}	
					
					$dati_cliente = Db::getInstance()->getValue('SELECT (CASE WHEN is_company = 1 THEN company ELSE CONCAT(firstname," ",lastname) END) cliente FROM customer WHERE id_customer = '.$id_customer);
					
					if($uai['tipo_attivita'] == 'T')
					{
						$check_resp = 1;
						$id_contact = Db::getInstance()->getValue('SELECT id_contact FROM customer_thread WHERE id_customer_thread = '.$uai['id_attivita']);
						if($id_contact == 7)
						{
							$tipo_attivita_uai = 'Messaggio';
							$count_msg = Db::getInstance()->getValue('SELECT count(id_customer_message) FROM customer_message WHERE id_employee = 0 AND id_customer_thread = '.$uai['id_attivita']);
							if($count_msg == 0)
								$check_resp = 0;
						}	
					}
					
					if($uai['tipo_attivita'] == 'P')
					{
						$id_contact = Db::getInstance()->getValue('SELECT tipo_richiesta FROM form_prevendita_thread WHERE id_thread = '.$uai['id_attivita']);
						if($id_contact == 'tirichiamiamonoi')
							$tipo_attivita_uai = 'Ti richiamiamo noi';
					}
					
					if(substr($uai['desc_attivita'],0,22) == 'Ha salvato il carrello')
						$desc_attivita = 'Ha lavorato sul carrello';
					else
						$desc_attivita = $uai['desc_attivita'];
					
					if(is_numeric(substr($desc_attivita, -1)))
					{	
						$employee = filter_var($desc_attivita, FILTER_SANITIZE_NUMBER_INT);
						$name_employee = Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM employee WHERE id_employee = '.$employee);
						$desc_attivita = 'Ha messo l\'attivit&agrave; in carico a '.$name_employee;
					}
					
					echo '<tr '.($uaii%2 == 0 ? '' : 'style="background-color:#f5f5f5"').' class="riga"><td data-sort="'.$tipo_attivita_uai.'" id="td_espandix_'.$uai['id_attivita'].'"><img src="../img/admin/add.gif" alt="Espandi" style="cursor:pointer" title="Espandi" id="espandix_'.$uai['id_attivita'].'" />'.$tipo_attivita_uai.'
					';
					
					
					if($uai['tipo_attivita'] == 'C' || $uai['tipo_attivita'] == 'O')
					{
						echo '
					<script type="text/javascript">
						$(document).ready(function(){
							$("#espandix_'.$uai['id_attivita'].', #td_espandix_'.$uai['id_attivita'].'").click(function(){
								
								if($(this).attr("src") == ("../img/admin/add.gif")) {
									$(this).attr("src", "../img/admin/forbbiden.gif");
									$.ajax({
										url:"ajax.php?get'.($uai['tipo_attivita'] == 'O' ? 'Order' : 'Cart').'Display=y",
										type: "POST",
										data: {
										id_'.($uai['tipo_attivita'] == 'O' ? 'order' : 'cart').': '.$uai['id_attivita'].'
										},
										success:function(r){
										/* $(".riga").show(); */
										/*$("#orderTempDisplay").html(r);*/
										$("#cart_riga_show_'.$uai['id_attivita'].'_td").html(r);
										$("#cart_riga_show_'.$uai['id_attivita'].'").show();
													
										/* $("#riga_'.$uai['id_attivita'].'").toggle(); */
										},
										error: function(xhr,stato,errori){
											alert("Errore durante l\'operazione:"+xhr.status);
										}
									});
								}
								else {
									$(this).attr("src", "../img/admin/add.gif");
									$("#cart_riga_show_'.$uai['id_attivita'].'").hide();
									/*$("#orderTempDisplay").html("");*/
								}
								return false;
							});
							
						});
					</script>';
					}
					else
					{
						echo '<script type="text/javascript">
						$(document).ready(function(){
							$("#espandix_'.$uai['id_attivita'].', #td_espandix_'.$uai['id_attivita'].'").click(function(){
								$("tr.subx_'.$uai['id_attivita'].'").toggle();
								if($("tr.subx_'.$uai['id_attivita'].':visible").length) {
									$(this).attr("src", "../img/admin/forbbiden.gif");	
								}
								else {
									$(this).attr("src", "../img/admin/add.gif");
									$("tr.subx_'.$uai['id_attivita'].'").hide();
								}
								return false;
							});
							
						});
						</script>';
						
					}
					echo '
					</td>
					
					<td><a href="'.$uai_link.'">'.$id_attivita.'</a></td><td colspan="3"><a href="'.$uai_link.'">'.$dati_cliente.'</a></td><td colspan="2"><a href="'.$uai_link.'">'.$uai_employee.'</a></td><td><a href="'.$uai_link.'">'.($tipo_attivita_uai == 'Messaggio' && $check_resp == 0 ? '' : $uai_status).'</a></td><td><a href="'.$uai_link.'">'.Tools::displayDate($uai['data_attivita'],5,true).'</a></td></tr>';
					
					if($uai['tipo_attivita'] == 'O')
					{
						echo '
						<tr id="cart_riga_show_'.$uai['id_attivita'].'" class="invisible-table-row subs subx_'.$uai['id_attivita'].'" style="display:none '.($uaii%2 == 0 ? '' : '; background-color:#f5f5f5').'"><td id="cart_riga_show_'.$uai['id_attivita'].'_td" colspan="9"></td></tr>
						';
					}
					else if($uai['tipo_attivita'] == 'C')
					{
						
						echo '
						<tr id="cart_riga_show_'.$uai['id_attivita'].'" class="invisible-table-row subs subx_'.$uai['id_attivita'].'" style="display:none '.($uaii%2 == 0 ? '' : '; background-color:#f5f5f5').'"><td id="cart_riga_show_'.$uai['id_attivita'].'_td" colspan="9"></td></tr>
						';
					}	
					else
					{
						
						echo '
						<tr class="invisible-table-row subs subx_'.$uai['id_attivita'].'" style="display:none '.($uaii%2 == 0 ? '' : '; background-color:#f5f5f5').'"><td colspan="9">';
						
						if($uai['tipo_attivita'] == 'T')
						{
							$messaggit = Db::getInstance()->ExecuteS("SELECT * FROM customer_message cm WHERE cm.id_customer_thread = ".$uai['id_attivita']." ORDER BY date_add ASC");
							$msg_id = 0;
							foreach($messaggit as $messaggiot) {
							
								echo "<table style='width:100%; 
								border:1px solid #dfd5c3; 
								";
								if ($messaggiot['id_employee'] == 0) {
									echo 'background-color: #ffffff;';
								} else {
						
									echo 'background-color: #ffecf2;';
								}
								$employeezt = new Employee($messaggiot['id_employee']);
								echo "margin-bottom:15px'>";
								echo '<tr>
								<td style="colspan:2">
									<table>
							
									<td style="font-size:14px; width:230px">Da: <strong>';
									if ($messaggiot['id_employee'] == 0) {
										if($messaggiot['email'] == "" || is_numeric($messaggiot['email'])) {
											echo $customer->firstname." ".$customer->lastname; 
										}
										else {
											echo "Ticket aperto dallo staff";
										}
									} 
									else 
									{ 
										echo $employeezt->firstname." ".substr($employeezt->lastname,0,1).".";
										if($msg_id == 0)
												echo ' - ticket aperto dallo staff';
									} 
									echo '<strong></td>
									
									<td style="width:170px"><strong>Data: </strong>'.date("d-m-Y H:i:s",strtotime($messaggiot['date_add'])).'</td>
									<td style="width:180px"><strong>Allegati: </strong>';
									if(!empty($messaggiot['file_name'])) {
									
										$allegati = explode(";",$messaggiot['file_name']);
										$nall = 1;
										foreach($allegati as $allegato) {
											
											
											if(strpos($allegato, ":::")) {
								
												$parti = explode(":::", $allegato);
												$nomevero = $parti[1];
												
											}
								
											else {
												$nomevero = $allegato;
								
											}
								
											if($allegato == "") { } else {
												if($nall == 1) { } else { echo " - "; }
												
												if($customer->id == 2) {
													echo '<a href="https://ezdocs:WRY753MNR43ASD147g!@'.preg_replace('#^https?://#', '', rtrim($nomevero,'/')).'"><span style="color:red">'.substr( $nomevero, strrpos( $nomevero, '/' )+1 ).'</span></a>';
													$nall++;
												}
												else {
													echo '<a href="ajax.php?openAllegato=y&id_customer='.$customer->id.'&viewcustomer&viewticket&id_customer_thread='.$thread['id_customer_thread'].'&tab-container-1=6&token='.$tokenCustomers.'&filename='.$allegato.'"><span style="color:red">'.$nomevero.'</span></a>';
													$nall++;
												}
											}
										}
									}
									
									else { echo 'Nessun allegato'; }
									
									
									echo '</td>';
									
									
									
									echo '<td></td></tr></table>
								</td>
								</tr>
								<tr>
								<td style="colspan:2">
									<table>
									<tr>
									<td style="width:100px"><strong>Messaggio</strong></td>
									<td>'.htmlspecialchars_decode(preg_replace('/(<[^>]+) style=".*?"/i', '$1', preg_replace('/(<[^>]+) class=".*?"/i', '$1', $messaggiot['message']))).($messaggiot['id_employee'] == 0 ? '' : '
									').'
									</td>
									</tr>
									</table>
								</td>';
								echo '</tr>
								';
								
								if($messaggiot['note_private'] != '')
								{
									echo '<tr>
									<td style="colspan:2">
										<table>
										<tr>
										<td style="width:100px"><strong style="color:red">Nota privata</strong></td>
										<td style="color:red">'.htmlspecialchars_decode(preg_replace('/(<[^>]+) style=".*?"/i', '$1', preg_replace('/(<[^>]+) class=".*?"/i', '$1', $messaggiot['note_private']))).'
										</td>
										</tr>
										</table>
									</td>';
									echo '</tr>
									';
								}
								
								if($messaggiot['email'] == '') { 
									if($messaggiot['in_carico'] > 0) 
									{
										echo '<tr><td style="colspan:2">
										<table>
										<tr>
										
										<td style="width:100px"><strong>In carico a</strong></td>
										<td>'.Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$messaggiot['in_carico']).'</td>
										';
									}	
								} else {
								
									echo '<tr><td style="colspan:2">
									<table>
									<tr>
									<td><strong>Inviato a: </strong>'.(is_numeric($messaggiot['email']) ? Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$messaggiot['email']) : htmlspecialchars_decode($messaggiot['email'])).'</td>
									
									'.($messaggiot['in_carico'] > 0 ? '<td style="padding-left:20px"><strong>In carico a</strong>:
									'.($messaggiot['in_carico'] > 0 ? Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$messaggiot['in_carico'])  : '--').'</td>' : '').'
									
									';
								
								} 
								
								$cc = Db::getInstance()->getValue("SELECT cc FROM ticket_cc WHERE type = 't' AND msg = ".$messaggiot['id_customer_message']."");
									if(!empty($cc)) {
									
										echo '<td style="padding-left:20px">';
									
										$ccs = explode(";",$cc);
										$cc_str = "<strong>CC:</strong> ";
										
										foreach ($ccs as $conoscenza) {
										
											$imp = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".$conoscenza."");
											$cc_str .= $imp." ";
										}
										
										echo $cc_str;
										echo "</td>";
									
									}
									else {
									
										echo '<td style="padding-left:20px"><strong>CC:</strong> Nessuno</td>';
									
									}
								
								echo '</tr>
									</table></tr></table>';
								
								if($thread['id_contact'] == 9) {
								}
								else {
									echo '<strong>Ordine di riferimento</strong>: '.($thread['id_order'] != 0 ? $thread['id_order'] : '<em>Nessuno</em>').' - <strong>Prodotto di riferimento</strong>: '; if($thread['id_product'] != 0) { $prodrichiesta = Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_lang = 5 and id_product = ".$thread['id_product'].""); echo $prodrichiesta; } else { 	echo '<em>Nessuno</em>'; }
								}					
								/*
								if($thread['id_contact'] == 9) {
								
								echo '<br /><strong>Seriale</strong>: '.$dati_rma['seriale'].' - <strong>Codice RMA</strong>: '.$dati_rma['codice_rma'].' - <strong>Tipo RMA</strong>: '.$dati_rma['rma_type'].' - <strong>Spedizione</strong>: '.$dati_rma['rma_shipping'].' - <strong>Indirizzo</strong>: '.$indirizzo_rma['address1'].' - '.$indirizzo_rma['postcode'].' '.$indirizzo_rma['city'].' ('.$indirizzo_rma['iso_code'].')<br /><strong>Template RMA</strong>: <a href="http://www.ezdirect.it/rma/'.$dati_rma['codice_rma'].'.html" target="_blank">http://www.ezdirect.it/rma/'.$dati_rma['codice_rma'].'.html</a>';  
								
								}					
								*/
								
								echo '
								
								<br /><br />';
								$msg_id++;
							}
						}
						else if($uai['tipo_attivita'] == 'A')
						{
							
							$messaggia = Db::getInstance()->ExecuteS("SELECT * FROM action_message am WHERE am.id_action = ".$uai['id_attivita']."");
				
							foreach($messaggia as $messaggioa) {
							
								$employee_from = new Employee($messaggioa['action_m_from']);
								$employee_to = new Employee($messaggioa['action_m_to']);
								
								echo "<table style='width:100%; 
								border:1px solid #dfd5c3; background-color: #ffecf2;margin-bottom:15px'>";
								echo '<tr>
									<td style="colspan:2">
										<table>
								
										<td style="font-size:14px; width:250px">Da: <strong>'.$employee_from->firstname." ".substr($employee_from->lastname,0,1).".".'<strong></td>
										<td style="width:170px"><strong>Data: </strong>'.date("d-m-Y H:i:s",strtotime($messaggioa['date_add'])).'</td>
										<td style="width:180px"><strong>Allegato: </strong>';
										if(!empty($messaggioa['file_name'])) {
										
											$allegatia = explode(";",$messaggioa['file_name']);
											$nalla = 1;
											foreach($allegatia as $allegatoa) {
												if(strpos($allegatoa, ":::")) {
									
													$parti = explode(":::", $allegatoa);
													$nomeveroa = $parti[1];
													
												}
									
												else {
													$nomeveroa = $allegatoa;
									
												}
												if($allegatoa == "") { } else {
													if($nalla == 1) { } else { echo " - "; }
													echo '<a href="ajax.php?openAllegato=y&id_customer='.$customer->id.'&viewcustomer&viewaction&id_action='.$azione['id_action'].'&token='.$tokenCustomers.'&tab-container-1=10&filename='.$allegatoa.'"><span style="color:red">'.$nomeveroa.'</span></a>';
													$nallp++;
												}
											}
										
										}
										
										else { echo 'Nessun allegato'; }
										
										
										echo '</td>';
										
										
										
										
										echo '</tr></table>
										
									</td>
									</tr>
									<tr>
									<td style="colspan:2">
										<table>
										<tr>
										<td style="width:100px"><strong>Messaggio</strong></td>
										<td>'.htmlspecialchars_decode($messaggioa['action_message']).'
										
										</td>
										</tr>
										</table>
									</td>';
									echo '</tr>
									<tr><td style="colspan:2">
										<table>
										<tr>
										<td><strong>Inviato a: </strong>'.$employee_to->firstname.'</td>
										
										
										<td style="padding-left:20px"><strong>In carico a</strong></td>
										<td>'.($messaggioa['in_carico'] > 0 ? Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$messaggioa['in_carico']) : '--').'</td>
										
										'.($messaggioa['telefono_cliente'] != '' ? '<td style="padding-left:20px"><strong>Telefono cliente</td><td><a href="callto:'.$messaggioa['telefono_cliente'].'">'.$messaggioa['telefono_cliente'].'</a></td>' : '').'
										';
									$cc = Db::getInstance()->getValue("SELECT cc FROM ticket_cc WHERE type = 'a' AND msg = ".$messaggioa['id_action_message']."");
										if(!empty($cc)) {
										
											echo '<td style="padding-left:20px">';
										
											$ccs = explode(";",$cc);
											$cc_str = "<strong>CC:</strong> ";
											
											foreach ($ccs as $conoscenza) {
											
												$imp = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".$conoscenza."");
												$cc_str .= $imp." ";
											}
											
											echo $cc_str;
											echo "</td>";
										
										}
										else {
										
											echo '<td style="padding-left:20px"><strong>CC:</strong> Nessuno</td>';
										
										}
									
									echo '</tr>
										</table>
									
									</td></tr>
									</table><br />';
							}
							
						}
						echo '</td></tr>
						';
					}
					
					$uaii++;
				}
				
				echo '</tbody></table></div>';
			
			}

		/* Display customer information */
		if (Validate::isLoadedObject($customer))
		{		
			$rowcomp = Db::getInstance()->getRow("SELECT is_company FROM customer WHERE id_customer = $order->id_customer");
		
			/*$acquisti_totali = Db::getInstance()->getValue('SELECT SUM(imponibile) AS totale_acquisti FROM (SELECT * FROM fattura WHERE id_customer = '.$customer->id.' GROUP BY id_fattura) f');
			*/
		
			$ordini_totali = Db::getInstance()->getValue('SELECT SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) tot FROM order_detail JOIN orders ON orders.id_order = order_detail.id_order LEFT JOIN order_history oh ON oh.id_order_state =6 AND oh.id_order = orders.id_order WHERE oh.id_order_state IS NULL  AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment"))) AND orders.id_customer = '.$customer->id.'');
			
			if($rowcomp['is_company'] == 0) {
			
				echo '
				
				
				<fieldset style="width: 97%">
				<legend><img src="../img/admin/tab-customers.gif" /> '.AdminOrders::lx('Informazioni cliente').'</legend>
				<table>
				<tr style="width:800px">
				<td style="width:100px"><em>Tipo cliente</em></td>
				<td style="width:300px"><em>Nome e cognome</em></td>
				<td style="width:200px"><em>Data creazione account</em></td>
				<td style="width:100px"><em>Ordini totali</em></td>
				<td style="width:100px"><em>Ordinato totale</em></td>
				</tr>
				<td><strong>PRIVATO</strong></td>
				<td>
					
					<span style="font-weight: bold; font-size: 14px;"><a href="?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'"> '.$customer->firstname.' '.$customer->lastname.'</a></span> ('.AdminOrders::lx('#').$customer->id.')</td>
				
					';
					
					}
					
			else {		
				echo '<br />
				<fieldset style="width: 97%">
				<table>
				
				<legend><img src="../img/admin/tab-customers.gif" /> '.AdminOrders::lx('Informazioni cliente').'</legend>
				<table>
				<tr style="width:800px">
				<td style="width:100px"><em>Tipo cliente</em></td>
				<td style="width:300px"><em>Ragione sociale</em></td>
				<td style="width:200px"><em>Data creazione account</em></td>
				<td style="width:100px"><em>Ordini totali</em></td>
				<td style="width:100px"><em>Ordinato totale</em></td>
				</tr>
				<td><strong>AZIENDA</strong></td>
				<td>
					<span style="font-weight: bold; font-size: 14px;"><a href="?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'"> '.$customer->company.'</a></span> ('.AdminOrders::lx('#').$customer->id.')</td>';	
			}
				
			if ($customer->isGuest())
			{
				echo '
				'.AdminOrders::lx('This order has been placed by a').' <b>'.AdminOrders::lx('guest').'</b>';
				if (!Customer::customerExists($customer->email))
				{
					echo '<form method="POST" action="index.php?tab=AdminCustomers&id_customer='.(int)$customer->id.'&token='.Tools::getAdminTokenLite('AdminCustomers').'">
						<input type="hidden" name="id_lang" value="'.(int)$order->id_lang.'" />
						<p class="center"><input class="button" type="submit" name="submitGuestToCustomer" value="'.AdminOrders::lx('Transform to customer').'" /></p>
						'.AdminOrders::lx('This feature will generate a random password and send an e-mail to the customer').'
					</form>';
				}
				else
					echo '<div><b style="color:red;">'.AdminOrders::lx('A registered customer account exists with the same email address').'</b></div>';
			}
			else
			{
				echo '<td>'.Tools::displayDate($customer->date_add, (int)($cookie->id_lang), true).'</td>
				<td><b>'.$customerStats['nb_orders'].'</b></td>
				<td><b>'.Tools::displayPrice(Tools::ps_round(Tools::convertPrice($ordini_totali, $currency), 2), $currency, false).'</b></td>';
			}
			echo '</tr></table></fieldset>';
		}
		
		echo '	<script type="text/javascript">
				function modificaIndirizzoSpedizione(id, tipo)
					{
						if(document.getElementById(\'ind_csv\').checked == false)
							var ind_csv = "n";
						else
							var ind_csv = "y";
						$.ajax({
						  url:"ajax.php?ordModificaIndirizzoSpedizione=y",
						  type: "POST",
						  data: { id_address: id, ind_csv: ind_csv, id_order: "'.$order->id.'", tipo: tipo
						  },
						  success:function(r){
							var parsed = JSON.parse(r);
							
							if(tipo == "consegna")
							{	
								document.getElementById("cons_mod").href = parsed[0];
								document.getElementById("cons_map").href = parsed[1];
								document.getElementById("cons_destinatario").innerHTML = "<strong>"+parsed[2]+"</strong>";
								document.getElementById("cons_referente").innerHTML = parsed[3];
								document.getElementById("cons_tel").innerHTML = parsed[4];
								document.getElementById("cons_cell").innerHTML = parsed[5];
								document.getElementById("cons_indirizzo").innerHTML = parsed[6];
								document.getElementById("cons_suburb").innerHTML = parsed[7];
								document.getElementById("cons_c_o").innerHTML = parsed[8];
								document.getElementById("cons_cap").innerHTML = parsed[9];
								document.getElementById("cons_citta").innerHTML = parsed[10];
								document.getElementById("cons_provincia").innerHTML = parsed[11];
								document.getElementById("cons_nazione").innerHTML = parsed[12];
							}
							else
							{
								document.getElementById("inv_mod").href = parsed[0];
								document.getElementById("inv_tel").innerHTML = parsed[4];
								document.getElementById("inv_cell").innerHTML = parsed[5];
								document.getElementById("inv_indirizzo").innerHTML = parsed[6];
								document.getElementById("inv_suburb").innerHTML = parsed[7];
								document.getElementById("inv_c_o").innerHTML = parsed[8];
								document.getElementById("inv_cap").innerHTML = parsed[9];
								document.getElementById("inv_citta").innerHTML = parsed[10];
								document.getElementById("inv_provincia").innerHTML = parsed[11];
								document.getElementById("inv_nazione").innerHTML = parsed[12];
							}
						  },
						  error: function(xhr,stato,errori){
							 alert("Errore durante l\'operazione:"+xhr.status);
						  }
						});
						
						
					}
				</script>';
		
			/* Display adresses : delivery & invoice */
			
			if($rowcomp['is_company'] == 0) {
			
				$indirizzi_di_fatturazione = Db::getInstance()->executeS("SELECT * FROM address WHERE id_customer = ".$customer->id." AND fatturazione = 1 AND deleted = 0 AND active = 1 ORDER BY id_address ASC");
				
				echo '<div class="clear">&nbsp;</div>
				<div style="float: left; width:45%">
				<fieldset style="width: 100%">
				<legend><img src="../img/admin/invoice.gif" alt="'.AdminOrders::lx('Indirizzo fatturazione').'" />'.AdminOrders::lx('Indirizzo fatturazione').'</legend>
				<div style="float: right"><a id="inv_mod" target="_blank" href="?tab=AdminCustomers&viewcart&id_customer='.$customer->id.'&viewcustomer&id_address='.$addressInvoice->id.'&token='.$tokenCustomers.'&tab-container-1=2&newaddress&addaddress&id_order='.$order->id.'&fatturazione=1&idclientee='.$customer->id.'"><img src="../img/admin/edit.gif" /></a></div>
				<table>
					<tr><td style="width:100px"> </td><td><br />'.($customer->id_default_group != 5 ? '<!--' : '').'
					
					<select name="id_address_invoice" id="id_address_invoice" onchange="modificaIndirizzoSpedizione(this.value, \'fatturazione\');" style="width:100%"><option value="--">Seleziona indirizzo</option>';
				foreach ($indirizzi_di_fatturazione as $inv_a) 
				{
					echo '<option value="'.$inv_a['id_address'].'" '.($addressInvoice->id == $inv_a['id_address'] ? 'selected="selected"' : '').'>'.$inv_a['address1'].' - '.$inv_a['city'].'</option>';
				}
				echo '</select> '.($customer->id_default_group != 5 ? '-->' : '').'';
				
				$cfcheck = true;
				
				if(Customer::ControllaCF2($customer->tax_code) == false)
					$cfcheck = false;
						
				echo '</td></tr>
				<tr><td style="width:100px">Destinatario</td><td><strong>'.$customer->firstname.' '.$customer->lastname.'</strong></td></tr>
				<tr><td style="width:100px"></td><td id="inv_c_o">&nbsp;</td></tr>
				<tr><td style="'.($cfcheck == false ? 'border:3px solid red;' : '').'width:100px">CF</td><td> '.$customer->tax_code.'
				'.($cfcheck == false ? '<img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Codice fiscale potenzialmente errato: controllare" title="Codice fiscale potenzialmente errato: controllare" />' : '').'
				</td></tr>
				<tr><td style="width:100px">Email</td><td>'.$customer->email.'</td></tr>
				<tr><td style="width:100px">PEC</td><td>'.$customer->pec.'</td></tr>
				<tr><td style="width:100px">SDI</td><td>'.$customer->codice_univoco.'</td></tr>
				<tr><td style="width:100px">Tel</td><td id="inv_tel">'.$addressInvoice->phone.'</td></tr>
				<tr><td style="width:100px">Cell</td><td id="inv_cell">'.$addressInvoice->phone_mobile.'</td></tr>>
				<tr><td style="width:100px">Indirizzo</td><td id="inv_indirizzo">'.$addressInvoice->address1.'</td></tr>
				<tr><td style="width:100px">Localit&agrave;</td><td id="inv_suburb">'.$addressInvoice->suburb.'</td></tr>
				
				<tr><td style="width:100px">CAP</td><td id="inv_cap">'.$addressInvoice->postcode.'</td></tr>
				<tr><td style="width:100px">Citt&agrave;</td><td id="inv_citta">'.$addressInvoice->city.'</td></tr>
				<tr><td style="width:100px">Provincia</td><td id="inv_provincia">'.$invoiceState->name.'</td></tr>
				<tr><td style="width:100px">Nazione</td><td id="inv_nazione">'.$addressInvoice->country.'</td></tr>
				</table>

				</fieldset>
				</div>
				<div style="float: left; width:45%; margin-left: 7%">
		
				<fieldset style="width: 100%">
				<legend><img src="../img/admin/delivery.gif" alt="'.AdminOrders::lx('Indirizzo consegna').'" />'.AdminOrders::lx('Indirizzo consegna').'</legend>
				<div style="float: right">

				<a id="cons_mod" target="_blank" href="?tab=AdminCustomers&viewcart&id_customer='.$customer->id.'&viewcustomer&id_address='.$addressDelivery->id.'&token='.$tokenCustomers.'&tab-container-1=2&newaddress&id_order='.$order->id.'&addaddress&'.($addressDelivery->id == $addressInvoice->id ? 'fatturazione' : 'consegna').'=1&idclientee='.$customer->id.'" ><img src="../img/admin/edit.gif" /></a>
				
				<a id="cons_map" href="http://maps.google.com/maps?f=q&hl='.$currentLanguage->iso_code.'&geocode=&q='.$addressDelivery->address1.' '.$addressDelivery->postcode.' '.$addressDelivery->city.($addressDelivery->id_state ? ' '.$deliveryState->name: '').'" target="_blank"><img src="../img/admin/google.gif" alt="" class="middle" /></a>

				</div>';
				
				
				echo'
				<table>';
				$indirizzi_di_consegna = Db::getInstance()->executeS("SELECT * FROM address WHERE id_customer = ".$customer->id." AND deleted = 0 AND active = 1 ORDER BY id_address ASC");
				
				
				
				echo '
				<table>
				<tr><td style="width:100px">CSV? <input name="ind_csv" id="ind_csv" type="checkbox" checked="checked" '.($cookie->id_employee == 2 || $cookie->id_employee == 7 || $cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 14  ? 'style=""' : '').' /> </td><td> <select name="id_address_delivery" id="id_address_delivery" onchange="modificaIndirizzoSpedizione(this.value, \'consegna\');" style="width:70%">';
				foreach ($indirizzi_di_consegna as $del_a) 
				{
					echo '<option value="'.$del_a['id_address'].'" '.($addressDelivery->id == $del_a['id_address'] ? 'selected="selected"' : '').'>'.$del_a['address1'].' - '.$del_a['city'].'</option>';
				}
				echo '</select>';
						
				echo '</td></tr>';
				echo '				
				<tr><td style="width:100px">Destinatario</td><td id="cons_destinatario"><strong>'; if($addressDelivery->company != '') { echo $addressDelivery->company; } else { echo $addressDelivery->firstname." ".$addressDelivery->lastname; } echo '</strong></td></tr>
				<tr><td style="width:100px">C/O</td><td id="cons_c_o">'.$addressDelivery->c_o.'</td></tr>
				<tr>';
				if($addressDelivery->company == '') {
					echo "<td>&nbsp;</td><td id='cons_referente'></td></tr>";
				} else {
					echo '<td style="width:100px">Referente</td><td id="cons_referente">'.$addressDelivery->firstname.' '.$addressDelivery->lastname.'</td></tr>';
				}
				
				echo ' <tr><td style="width:100px">&nbsp;</td><td></td></tr>
				
				<tr><td style="width:100px">&nbsp;</td><td></td></tr>
				<tr><td style="width:100px">&nbsp;</td><td></td></tr>
				
				<tr><td style="width:100px">Tel</td><td id="cons_tel">'.$addressDelivery->phone.'</td></tr>
				<tr><td style="width:100px">Cell</td><td id="cons_cell">'.$addressDelivery->phone_mobile.'</td></tr>
				<tr><td style="width:100px">Indirizzo</td><td id="cons_indirizzo">'.$addressDelivery->address1.'</td></tr>
				<tr><td style="width:100px">Localit&agrave;</td><td id="cons_suburb">'.$addressDelivery->suburb.'</td></tr>
				
				<tr><td style="width:100px">CAP</td><td id="cons_cap">'.$addressDelivery->postcode.'</td></tr>
				<tr><td style="width:100px">Citt&agrave;</td><td id="cons_citta">'.$addressDelivery->city.'</td></tr>
				<tr><td style="width:100px">Provincia</td><td id="cons_provincia">('.$deliveryState->name.')</td></tr>
				<tr><td style="width:100px">Nazione</td><td id="cons_nazione">'.$addressDelivery->country.'</td></tr>
				</table>
				'
				
				.'</fieldset>
			
			
				</div>
				<div class="clear">&nbsp;</div>';
		
			}
		
			else {
		
				$indirizzi_di_fatturazione = Db::getInstance()->executeS("SELECT * FROM address WHERE id_customer = ".$customer->id." AND fatturazione = 1 AND deleted = 0 AND active = 1 ORDER BY id_address ASC");
				
				echo '<div class="clear">&nbsp;</div>

				<div style="width:45%; float: left">
				<fieldset style="width: 100%">
				<legend><img src="../img/admin/invoice.gif" alt="'.AdminOrders::lx('Indirizzo Fatturazione').'" />'.AdminOrders::lx('Indirizzo Fatturazione').'</legend>
				<div style="float: right"><!-- <a href="?tab=AdminAddresses&id_address='.$addressInvoice->id.'&addaddress&realedit=1&id_order='.$order->id.($addressDelivery->id == $addressInvoice->id ? '&address_type=2' : '').'&back='.urlencode($_SERVER['REQUEST_URI']).'&token='.Tools::getAdminToken('AdminAddresses'.(int)(Tab::getIdFromClassName('AdminAddresses')).(int)($cookie->id_employee)).'"><img src="../img/admin/edit.gif" /></a> -->
				<a target="_blank" href="?tab=AdminCustomers&viewcart&id_customer='.$customer->id.'&viewcustomer&id_address='.$addressInvoice->id.'&token='.$tokenCustomers.'&tab-container-1=2&newaddress&addaddress&id_order='.$order->id.'&fatturazione=1&idclientee='.$customer->id.'" id="inv_mod"><img src="../img/admin/edit.gif" /></a>
				</div>
				
				
				<table>
					<tr><td style="width:100px"> </td><td>'.($customer->id_default_group != 5 ? '<br /><!--' : '').' <select name="id_address_invoice" id="id_address_invoice" onchange="modificaIndirizzoSpedizione(this.value, \'fatturazione\');" style="width:90%"><option value="--">Seleziona indirizzo</option>';
				foreach ($indirizzi_di_fatturazione as $inv_a) 
				{
					echo '<option value="'.$inv_a['id_address'].'" '.($addressInvoice->id == $inv_a['id_address'] ? 'selected="selected"' : '').'>'.$inv_a['address1'].' - '.$inv_a['city'].'</option>';
				}
				echo '</select> '.($customer->id_default_group != 5 ? '-->' : '').'';
						
				$cfcheck = true;
				
				if(Customer::ControllaCF2($customer->tax_code) == false)
					$cfcheck = false;
				
				echo '</td></tr>
				<tr><td style="width:100px">Destinatario</td><td><strong>'.$customer->company.'</strong></td></tr>
				<tr><td style="width:100px"></td><td id="inv_c_o">&nbsp;</td></tr>
				<tr><td style="width:100px">P.IVA</td><td>'.$customer->vat_number.'</td></tr>
				<tr><td style="'.($cfcheck == false ? 'border:3px solid red;' : '').'width:100px">CF</td><td> '.$customer->tax_code.'
				'.($cfcheck == false ? '<img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Codice fiscale potenzialmente errato: controllare" title="Codice fiscale potenzialmente errato: controllare" />' : '').'
				</td></tr>
				<tr><td style="width:100px">Referente</td><td>'.$customer->firstname.' '.$customer->lastname.'</td></tr>
				<tr><td style="width:100px">Email</td><td>'.$customer->email.'</td></tr>
				<tr><td style="width:100px">PEC</td><td>'.$customer->pec.'</td></tr>
				<tr><td style="width:100px">SDI</td><td>'.$customer->codice_univoco.'</td></tr>
				<tr><td style="width:100px">Tel</td><td id="inv_tel">'.$addressInvoice->phone.'</td></tr>
				<tr><td style="width:100px">Cell</td><td id="inv_cell">'.$addressInvoice->phone_mobile.'</td></tr>
				<tr><td style="width:100px">Indirizzo</td><td id="inv_indirizzo">'.$addressInvoice->address1.'</td></tr>
				<tr><td style="width:100px">Localit&agrave;</td><td id="inv_suburb">'.$addressInvoice->suburb.'</td></tr>
				
				<tr><td style="width:100px">CAP</td><td id="inv_cap">'.$addressInvoice->postcode.'</td></tr>
				<tr><td style="width:100px">Citt&agrave;</td><td id="inv_citta">'.$addressInvoice->city.'</td></tr>
				<tr><td style="width:100px">Provincia</td><td id="inv_provincia">('.$invoiceState->name.')</td></tr>
				<tr><td style="width:100px">Nazione</td><td id="inv_nazione">'.$addressInvoice->country.'</td></tr>
				</table>

				</fieldset>
				</div>
				<div style="float: left; width:45%; margin-left: 7%">
				<fieldset style="width: 100%">
				<legend><img src="../img/admin/delivery.gif" alt="'.AdminOrders::lx('Indirizzo consegna').'" />'.AdminOrders::lx('Indirizzo consegna').'</legend>
				<div style="float: right">
				<!-- <a href="?tab=AdminAddresses&id_address='.$addressDelivery->id.'&addaddress&realedit=1&id_order='.$order->id.($addressDelivery->id == $addressInvoice->id ? '&address_type=1' : '').'&token='.Tools::getAdminToken('AdminAddresses'.(int)(Tab::getIdFromClassName('AdminAddresses')).(int)($cookie->id_employee)).'&back='.urlencode($_SERVER['REQUEST_URI']).'"><img src="../img/admin/edit.gif" /></a> -->
			
				<a id="cons_mod" target="_blank" href="?tab=AdminCustomers&viewcart&id_customer='.$customer->id.'&viewcustomer&id_address='.$addressDelivery->id.'&token='.$tokenCustomers.'&tab-container-1=2&newaddress&id_order='.$order->id.'&addaddress&'.($addressDelivery->id == $addressInvoice->id ? 'fatturazione' : 'consegna').'=1&idclientee='.$customer->id.'"><img src="../img/admin/edit.gif" /></a>
				
				<a id="cons_map" href="http://maps.google.com/maps?f=q&hl='.$currentLanguage->iso_code.'&geocode=&q='.$addressDelivery->address1.' '.$addressDelivery->postcode.' '.$addressDelivery->city.($addressDelivery->id_state ? ' '.$deliveryState->name: '').'" target="_blank"><img src="../img/admin/google.gif" alt="" class="middle" /></a>
	
				</div>';
				
				$indirizzi_di_consegna = Db::getInstance()->executeS("SELECT * FROM address WHERE id_customer = ".$customer->id." AND deleted = 0 AND active = 1 ORDER BY id_address ASC");
				
				echo '
				<table>
				<tr><td style="width:100px"> CSV? <input name="ind_csv" id="ind_csv" type="checkbox" checked="checked" /> </td><td><select name="id_address_delivery" id="id_address_delivery" onchange="modificaIndirizzoSpedizione(this.value, \'consegna\');" style="width:100%"><option value="--">Seleziona indirizzo</option>';
				foreach ($indirizzi_di_consegna as $del_a) 
				{
					echo '<option value="'.$del_a['id_address'].'" '.($addressDelivery->id == $del_a['id_address'] ? 'selected="selected"' : '').'>'.$del_a['address1'].' - '.$del_a['city'].'</option>';
				}
				echo '</select>';
						
				echo '</td></tr>
				<tr><td style="width:100px">Destinatario</td><td id="cons_destinatario"><strong>'; if($addressDelivery->company != '') { echo $addressDelivery->company; } else { echo $addressDelivery->firstname." ".$addressDelivery->lastname; } echo '</strong></td></tr>
				<tr><td style="width:100px">C/O</td><td id="cons_c_o">'.$addressDelivery->c_o.'</td></tr>
				<tr><td style="width:100px">&nbsp;</td><td></td></tr>
				<tr><td style="width:100px">&nbsp;</td><td></td></tr>
				<tr><td style="width:100px">Referente</td><td id="cons_referente">'.$addressDelivery->firstname.' '.$addressDelivery->lastname.'</td></tr>
				<tr><td style="width:100px">&nbsp;</td><td></td></tr>
				<tr><td style="width:100px">&nbsp;</td><td></td></tr>
				<tr><td style="width:100px">&nbsp;</td><td></td></tr>
				<tr><td style="width:100px">Tel</td><td id="cons_tel">'.$addressDelivery->phone.'</td></tr>
				<tr><td style="width:100px">Cell</td><td id="cons_cell">'.$addressDelivery->phone_mobile.'</td></tr>
				<tr><td style="width:100px">Indirizzo</td><td id="cons_indirizzo">'.$addressDelivery->address1.'</td></tr>
				<tr><td style="width:100px">Localit&agrave;</td><td id="cons_suburb">'.$addressDelivery->suburb.'</td></tr>
				
				<tr><td style="width:100px">CAP</td><td id="cons_cap">'.$addressDelivery->postcode.'</td></tr>
				<tr><td style="width:100px">Citt&agrave;</td><td id="cons_citta">'.$addressDelivery->city.'</td></tr>
				<tr><td style="width:100px">Provincia</td><td id="cons_provincia">('.$deliveryState->name.')</td></tr>
				<tr><td style="width:100px">Nazione</td><td id="cons_nazione">'.$addressDelivery->country.'</td></tr>
				</table>
				'
				
				.'</fieldset>
				</div>
				<div class="clear">&nbsp;</div>';
		
		
		
		
		
			}
	
		
				/* Display current status */
				
			/*	foreach($history as $key => $val)
				{
					if($val['id_order_state'] == 24)
						unset($history[$key]);
				}
				
				
			*/	
			
			$dati_pa =  Db::getInstance()->getRow('SELECT rif_ordine, cig, cup, ipa, data_ordine_mepa FROM orders WHERE id_order = '.$order->id.'');
			
			if($dati_pa['cig'] != '' || $dati_pa['cup'] != '' || $dati_pa['ipa'] != '' || $dati_pa['data_ordine_mepa'] != '0000-00-00')
			{	
				echo '
				<fieldset style="width:97%">
				<legend><img src="../img/admin/AdminEmployees.gif" /> '.AdminOrders::lx('Dati P.A.').'</legend>';
				
				
				echo '<table style="width:800px">
				<tr>
				<td style="width:200px">CIG</td>
				<td style="width:200px">CUP</td>
				<td style="width:200px">IPA <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Codice univoco ufficio per fatturazione elettronica" title="Codice univoco ufficio per fatturazione elettronica" /></td>
				<td style="width:200px">Data ordine Mepa</td>
				</tr>
				<tr>
				<td> <b>'.$dati_pa['cig'].'</b></td>
				<td> <b>'.$dati_pa['cup'].'</b></td>
				<td><b>'.$dati_pa['ipa'].'</b></td>';
				echo "<td><b>".($dati_pa['data_ordine_mepa'] != '0000-00-00' ? date("d-m-Y", strtotime($dati_pa['data_ordine_mepa'])) : '')."</b></td>"; 
				echo "</tr></table>";

				echo "</fieldset><br />";
			}
			
			$row = array_shift($history);
		echo '
		<fieldset style="width:97%">
			<legend><img src="../img/admin/information.png" /> '.AdminOrders::lx('Storico stato ordine (pagamento / tecnico)').'</legend>
			';
			
			if($cookie->profile == 7)
			{
				
			}
			else
			{
					echo '<table>';
				
				/* Display status form */
				echo '<tr>';
				
				$tokenOrders = Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).(int)($cookie->id_employee));
				
				$acconto = Db::getInstance()->getValue('SELECT acconto FROM orders WHERE id_order = '.$order->id);
				
				echo'	<td colspan="5" style="text-align:left"><form action="
				'.(isset($_GET['viewcustomer']) ? 'index.php?tab=AdminOrders&vieworders&token='.$tokenOrders.'' : $currentIndex.'&view'.$this->table.'&token='.$tokenCustomers).'
				
				" method="post" style="text-align:left;">
						Stato attuale: <select style="width:200px" name="id_order_state" id="id_order_state" onchange="
						
						if(this.value == 4 || this.value == 15) {
							 document.getElementById(\'imposta_tracking\').style.display = \'inline\'; 
							document.getElementById(\'submitState\').style.display = \'none\';			
						}
						else if(this.value == 24) { 
							document.getElementById(\'impiegati_verifica\').style.display = \'inline\';
							document.getElementById(\'submitState\').style.display = \'inline\'; 
							document.getElementById(\'imposta_tracking\').style.display = \'none\'; 				document.getElementById(\'verifica_msg\').style.display = \'inline\'; 
						} 
						else if(this.value == 33) { 
							document.getElementById(\'acconto1\').style.display = \'inline\';
						}
						else
						{
							document.getElementById(\'submitState\').style.display = \'inline\'; 
							document.getElementById(\'imposta_tracking\').style.display = \'none\'; 
							document.getElementById(\'impiegati_verifica\').style.display = \'none\'; 			
							document.getElementById(\'verifica_msg\').style.display = \'none\'; 
							document.getElementById(\'acconto1\').style.display = \'none\';
						}
						
						
						" 
						>';
				$currentStateTab = $order->getCurrentStateFull($cookie->id_lang);
				foreach ($states AS $state)
				{
					if($cookie->id_employee != 2 && $state['id_order_state'] == 35)
					{ 
					} 
					else
					{
						echo '<option value="'.$state['id_order_state'].'"'.(($state['id_order_state'] == $currentStateTab['id_order_state']) ? ' selected="selected"' : '').'>'.stripslashes($state['name']).'</option>';
					}	
				}	
				echo '
						</select>
						
						
						
						
						'.(isset($_GET['viewcustomer']) ? '<input type="hidden" name="prvcustomer" value="y" /><input type="hidden" name="numprvcustomer" value="'.$_GET['id_customer'].'" />' : '').'
						
						<input type="hidden" name="id_order" value="'.$order->id.'" />
						
						<input type="text" name="acconto" id="acconto1" value="'.($acconto > 0 ? number_format($acconto,2,".","") : 'Importo acconto?').'" '.($currentStateTab['id_order_state'] == 33 ? '' : 'style="display:none"').' />

					 '.($currentStateTab['id_order_state'] == 24 ? 'In carico a:' : '').' <select name="impiegati_verifica" id="impiegati_verifica" '.($currentStateTab['id_order_state'] == 24 ? '' : 'style="display:none"').'>
					';
					
					if($currentStateTab['id_order_state'] == 24)
						$incaricato_attuale = Db::getInstance()->getValue('SELECT action_to FROM action_thread WHERE subject = "*** VERIFICA TECNICA SU ORDINE N. '.$order->id.' " ORDER BY date_upd DESC');
					
					$impiegati = Employee::getEmployees();
					
					foreach($impiegati as $imp)
					{
						if($currentStateTab['id_order_state'] == 24)
						{
							echo '<option name="'.$imp['id_employee'].'" value="'.$imp['id_employee'].'" '.($imp['id_employee'] == $incaricato_attuale ? 'selected="selected"' : '').'>'.$imp['name'].'</option>';
							
							$actionverifica = Db::getInstance()->getValue('SELECT id_action FROM action_thread WHERE subject = "*** VERIFICA TECNICA SU ORDINE N. '.$order->id.' " ORDER BY date_upd DESC');
							$msg_verifica = Db::getInstance()->getValue('SELECT action_message FROM action_message WHERE id_action = '.$actionverifica.' ORDER BY date_add DESC');
								
						}
							
						else
							echo '<option name="'.$imp['id_employee'].'" value="'.$imp['id_employee'].'" '.($imp['id_employee'] == 1 ? 'selected="selected"' : '').'>'.$imp['name'].'</option>';
					}
					echo '
					</select>
					
					<textarea '.($currentStateTab['id_order_state'] == 24 ? 'readonly="readonly"' : '').' name="verifica_msg" id="verifica_msg" style="'.($currentStateTab['id_order_state'] == 24 ? 'display:inline' : 'display:none').'; width:350px; margin-top:3px; height:16px" '.($currentStateTab['id_order_state'] == 24 ? '' : 'onfocus="if(this.value == \'Messaggio...\') { this.value = \'\' }"').' onkeyup="auto_grow(this)">'.($currentStateTab['id_order_state'] == 24 ? $msg_verifica : 'Messaggio...').'</textarea>
					<script type="text/javascript">
						function auto_grow(element) {
								element.style.height = "5px";
								element.style.height = (element.scrollHeight)+"px";
							}
							
							$(document).ready(function() {
							
								$(".textarea_note").each(function () {
									this.style.height = ((this.scrollHeight)-4)+"px";
								});
								
								$(".textarea_note").on("change keyup keydown paste cut", "textarea", function () {
									$(this).height(0).height(this.scrollHeight);
								}).find("textarea").change();
													
							});
							
					</script>
						<input type="submit" name="submitState" id="submitState" onclick="<!-- saveOrdineNotaPrivata(); --> saveRifOrdine();"  style=" margin-top:-7px" value="'.AdminOrders::lx('Conferma').'" class="button" />
					<br />';
					
					$ast = Db::getInstance()->getRow('SELECT am.id_action, am.id_action_message, a.status, am.action_message, a.action_to, a.date_upd FROM action_message am JOIN action_thread a ON am.id_action = a.id_action WHERE (a.status = "closed" OR a.status = "pending1" OR a.status = "pending2") AND (am.action_message LIKE "%<strong>*** ASSISTENZA TECNICA ***</strong><br /><br />%" AND am.action_message LIKE "%'.$order->id.')%") GROUP BY a.id_action');
					
					if($ast['id_action'] > 0)	
					{
						$ast_status = '<strong>Ordine con <a href="index.php?tab=AdminCustomers&id_customer='.$order->id_customer.'&viewcustomer&viewaction&id_action='.$ast['id_action'].'&token='.$tokenCustomers.'&tab-container-1=10">todo assistenza tecnica</a></strong>. ';
						$employee_ast = Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$ast['action_to']);
						if($ast['status'] == 'closed')
							$ast_status .= '<em>Status</em>: <strong>chiuso</strong>';
						else if($ast['status'] == 'open')
							$ast_status .= '<em>Status</em>: <strong>aperto</strong>';
						else
							$ast_status .= '<em>Status</em>: <strong>in lavorazione</strong>';
						
						$ast_status .= ' - <em>In carico a</em>: <strong>'.$employee_ast.'</strong>';
						
						echo $ast_status;
					}
			
					echo '</td>';
							/* Display shipping number field */
					
					 echo '
						<td style="colspan:6" ><div id="imposta_tracking" method="post" style="'.($order->hasBeenShipped() || $order->hasBeenShippedPartially() ? '' : 'display:none; ').' margin-left:20px; margin-top:0px;">
							Numero tracking: <input type="text" name="shipping_number" '.($addressDelivery->country == 'Italia' ? 'maxlength="11"' : '').' id="shipping_number" onchange="submitShippingNumber_ajax();" value="'. $order->shipping_number.'" />
							<input type="hidden" name="id_order" value="'.$order->id.'" />
							<div id="shipping_number_div"></div>
							'.(isset($_GET['viewcustomer']) ? '<input type="hidden" name="prvcustomer" value="y" /><input type="hidden" name="numprvcustomer" value="'.$_GET['id_customer'].'" />' : '').'
							<!-- <input type="submit" name="submitShippingNumber" onclick=" saveRifOrdine();" value="'.AdminOrders::lx('Cambia e imposta numero tracking').'" class="button"  /> -->
							
							<script type="text/javascript">
							function submitShippingNumber_ajax()
							{	
								saveRifOrdine();
								var id_order_state = document.getElementById(\'id_order_state\').value;
								var shipping_number = document.getElementById(\'shipping_number\').value;
								$.ajax({url:"ajax.php?submitShippingNumber_ajax=y",
								  type: "POST",
								  data: {id_order:'.(int)$order->id.',id_order_state:id_order_state,shipping_number:shipping_number}, success:function (r) {
									
										console.log("OK");
										alert("Numero di tracking salvato. Una mail è stata inviata al cliente");
										$("#shipping_number_div").html("<b style=\"color:green\">Numero di tracking salvato. Una mail è stata inviata al cliente</b>").fadeIn(400);
									
								},
								  error: function(xhr,stato,errori){
								  }
								  });
							}
						</script>
						</div>';
					echo '</form>
				</td></tr></table>
				';
			}
			echo '
			<table cellspacing="0" cellpadding="0" class="table" style="width: 99.8%">
				<tr>
				<th style="width:20%">Data</th>
				<th style="width:25%">Stato </th>
				<th style="width:25%">Stato tecnico</th>
				<th style="width:30%">Impiegato</th>
				</tr>
				<tr>
					<th>'.Tools::displayDate($row['date_add'], (int)($cookie->id_lang), true).'</th>
					';
					if($row['id_order_state'] == 24 || $row['id_order_state'] == 25 || $row['id_order_state'] == 27 || $row['id_order_state'] == 28)
						echo '<th>--</th>';
					
					if($row['id_order_state'] == 33)
					{
						$acconto = Db::getInstance()->getValue('SELECT desc_attivita FROM storico_attivita WHERE tipo_attivita = "OH" AND id_attivita = '.$row['id_order_history']);
						
						$acconto = str_replace('Incassato acconto di ','',$acconto);
						
						if($acconto != '')
							echo '<th style="width:60px"><img src="../img/os/'.$row['id_order_state'].'.gif" /> '.stripslashes($row['ostate_name']).' '.'di '.$acconto.' euro</th>';
						else
							echo '<th style="width:60px"><img src="../img/os/'.$row['id_order_state'].'.gif" /> '.stripslashes($row['ostate_name']).'</th>';
					}
					else
						echo '<th style="width:60px"><img src="../img/os/'.$row['id_order_state'].'.gif" /> '.stripslashes($row['ostate_name']).'</th>';
					
					if($row['id_order_state'] != 24 && $row['id_order_state'] != 25 && $row['id_order_state'] != 27 && $row['id_order_state'] != 28)
						echo '<th>--</th>';
					
					echo '
					<th>'.((!empty($row['employee_lastname'])) ? '('.stripslashes($row['employee_firstname']).' '.stripslashes(Tools::substr($row['employee_lastname'], 0, 1)).'.)' : '').'</th>
				</tr>';
				
			$current_state_history = Db::getInstance()->getValue('SELECT max(id_order_history) FROM order_history WHERE id_order_state != 24 AND id_order_state != 25 AND id_order_state != 27 AND id_order_state !=28 AND id_order = '.$order->id);
			
			$current_state = Db::getInstance()->getValue('SELECT id_order_state FROM order_history WHERE id_order_history = '.$current_state_history);
			
			/* Display previous status */
			foreach ($history AS $row)
			{
				echo '
				<tr class="'.($irow++ % 2 ? 'alt_row' : '').'">
					<td>'.Tools::displayDate($row['date_add'], (int)($cookie->id_lang), true).'</td>';
					
					if($row['id_order_state'] == 24 || $row['id_order_state'] == 25 || $row['id_order_state'] == 27 || $row['id_order_state'] == 28)
					{	
						echo '<td>--</td>';
						echo '<td><img src="../img/os/'.$row['id_order_state'].'.gif" /> '.stripslashes($row['ostate_name']).'</td>';
					}
					
					if($row['id_order_state'] != 24 && $row['id_order_state'] != 25 && $row['id_order_state'] != 27 && $row['id_order_state'] != 28)
					{
						if($row['id_order_state'] == 33)
						{
							$acconto = Db::getInstance()->getValue('SELECT desc_attivita FROM storico_attivita WHERE tipo_attivita = "OH" AND id_attivita = '.$row['id_order_history']);
							
							$acconto = str_replace('Incassato acconto di ','',$acconto);
							
							if($acconto != '')
								echo '<td style="width:60px"><img src="../img/os/'.$row['id_order_state'].'.gif" /> '.stripslashes($row['ostate_name']).' '.'di '.$acconto.' euro</td>';
							else
								echo '<td><img src="../img/os/'.$row['id_order_state'].'.gif" /> '.stripslashes($row['ostate_name']).'</td>';
								
						}
						else
						{
							echo '<td><img src="../img/os/'.$row['id_order_state'].'.gif" /> '.stripslashes($row['ostate_name']).'</td>';
							
						}
					
						echo '<td>--</td>';
					
					}
					
					echo '
					<td>'.((!empty($row['employee_lastname'])) ? '('.stripslashes($row['employee_firstname']).' '.stripslashes(Tools::substr($row['employee_lastname'], 0, 1)).'.)' : '--').'</td>
				</tr>';
			}
			echo '</table>';
			
			if($cookie->profile == 7)
			{
				
			}
			else
			{
				echo '<br />
				<form action="ajax.php" method="post" onsubmit="saveNoFeedback(); return false;" id="form_nofeedback">
				<input type="checkbox" id="no_feedback" name="no_feedback" '.(Db::getInstance()->getValue('SELECT no_feedback FROM orders WHERE id_order = '.$order->id) == 1 ? 'checked="checked"' : '').' /> <strong>Abortisci feedback</strong> <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Spuntando questo flag impedirai l\'invio della mail di richiesta feedback/recensione al cliente per qualsiasi piattaforma (eKomi, Amazon, nostro sito)" title="Spuntando questo flag impedirai l\'invio della mail di richiesta feedback/recensione al cliente per qualsiasi piattaforma (eKomi, Amazon, nostro sito)" /> Causa: <input id="no_feedback_cause" type="text" name="no_feedback_cause" value="'.Db::getInstance()->getValue('SELECT no_feedback_cause FROM orders WHERE id_order = '.$order->id).'" style="width:450px" />&nbsp;&nbsp;&nbsp;<input type="submit" id="submitNoFeedback" class="button" value="Salva dati feedback"  /><div id="nofeedback_feedback"></div>';
				
				echo '<script type="text/javascript">
				function saveNoFeedback()
				{	
					
					$("#nofeedback_feedback").html("<img src=\"../img/loader.gif\" />").show();
					if($("#no_feedback").is(":checked"))
						var no_feedback = 1;
					else
						var no_feedback = 0;
					
					console.log(no_feedback);
					var no_feedback_cause = $("#no_feedback_cause").val();
					console.log(no_feedback_cause);
					
					$.post("ajax.php", {submitNoFeedback:1,id_customer:'.(int)$customer->id.',id_order:"'.$order->id.'",no_feedback:no_feedback,no_feedback_cause:no_feedback_cause}, function (r) {
						$("#nofeedback_feedback").html("").hide();
						if (r == "ok")
						{
							$("#nofeedback_feedback").html("<b style=\"color:green\">'.addslashes(AdminOrders::lx('Dati salvati')).'</b>").fadeIn(400);
						}
						else if (r == "error:validation")
							$("#nofeedback_feedback").html("<b style=\"color:red\">'.addslashes(AdminOrders::lx('Errore: impossibile salvare dati')).'</b>").fadeIn(400);
						else if (r == "error:update")
							$("#nofeedback_feedback").html("<b style=\"color:red\">'.addslashes(AdminOrders::lx('Errore: impossibile salvare dati')).'</b>").fadeIn(400);
						$("#nofeedback_feedback").fadeOut(3000);
					});
				}</script>';
			}	
			
		echo '</fieldset><br />';

			/* Carrier module */
			if ($carrier->is_module == 1)
			{
				$module = Module::getInstanceByName($carrier->external_module_name);
				if (method_exists($module, 'displayInfoByCart'))
					echo call_user_func(array($module, 'displayInfoByCart'), $order->id_cart);
			}
			

				/* Display shipping infos */
		echo '
		<fieldset style="width:97%">
			<legend><img src="../img/admin/delivery.gif" /> '.AdminOrders::lx('Pagamento e spedizione').'</legend>';
			
			$tempi_di_consegna = Db::getInstance()->getValue('SELECT consegna FROM cart WHERE id_cart = '.$order->id_cart);
				
				$preventivo = Db::getInstance()->getValue('SELECT preventivo FROM cart WHERE id_cart = '.$order->id_cart);
				
				if($preventivo == 1 && $tempi_di_consegna == '')
					$tempi_di_consegna = '2-5 gg.';
			
			echo '
					<div style="float:left">'.AdminOrders::lx('Metodo di pagamento').' <strong style="font-size:14px">'.Tools::substr($order->payment, 0, 32).' </strong></div><div style="clear:both"></div><br />';
			
			echo '<table style="width:800px"><tr><td style="width:200px">'.AdminOrders::lx('Peso:').'</td><td style="width:200px">'.AdminOrders::lx('Metodo spedizione:').'</td><td style="width:200px"><!-- '.AdminOrders::lx('Nota di consegna #').' --> Consegna</td><td style="width:200px">'.AdminOrders::lx('Numero tracking:').'</tr>
			<tr>
			<td> <b>'.number_format($order->getTotalWeight(), 2, ",","").' '.Configuration::get('PS_WEIGHT_UNIT').'</b></td>
			<td> <b>'.($carrier->name == '0' ? Configuration::get('PS_SHOP_NAME') : $carrier->name).'</b></td>
			<td>'.$tempi_di_consegna.'</td>';
			if ($order->shipping_number) {
				echo ' <td><b>'.$order->shipping_number.'</b> '.(!empty($carrier->url) ? '(<a href="'.str_replace('@', $order->shipping_number, $carrier->url).'" target="_blank">'.AdminOrders::lx('Track the shipment').'</a>)' : ''); echo "</td>";
		}
		else { echo "<td></td>"; }
		echo "</tr></table>";

		$rcb = Db::getInstance()->getValue('SELECT recyclable FROM cart WHERE id_cart = '.$order->id_cart);
		if($rcb == 99)
		echo '<br /><strong style="color:red">ATTENZIONE: emettere DDT senza prezzi</strong>';

		//ezcloud
		$ezcloud = Db::getInstance()->getRow('SELECT * FROM cart_ezcloud WHERE id_cart = '.$order->id_cart);

		if($ezcloud['id_cart'] > 0)
		{
			if($ezcloud['check_contratti'] == '')
			{
				echo '<br /><br /><input type="checkbox" name="check_contratti" id="check_contratti" onchange="salvaRicezioneContratto();" />&nbsp;&nbsp;&nbsp;<span id="check_contratti_span"><strong>Confermare la ricezione dell\'allegato firmato dal cliente</strong></span>';
				
			}
			else
			{
				$array_check_contratti = unserialize($ezcloud['check_contratti']);
				echo '<br /><br /><input type="checkbox" checked="checked" name="check_contratti" id="check_contratti" onchange="salvaRicezioneContratto();" />&nbsp;&nbsp;&nbsp;<span id="check_contratti_span">Ricezione contratto confermata da '.Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$array_check_contratti['id_employee']).' in data '.$array_check_contratti['data'].'</span>';
			}
			
			echo '<script type="text/javascript">
			function salvaRicezioneContratto()
			{
				if(document.getElementById("check_contratti").checked == true)
				{
					var contratti_check = 1;
				}
				else
				{
					var contratti_check = 0;
				}
				
				$.ajax({
				url:"ajax.php?salvaRicezioneContratto=y&id_cart='.$order->id_cart.'&id_employee='.$cookie->id_employee.'&contratti_check="+contratti_check,
				type: "POST",
				data: { 
				},
				success:function(r){
					// alert(r);
					$("#check_contratti_span").html(r);
				},
				error: function(xhr,stato,errori){
					alert("Errore durante l\'operazione:"+xhr.status);
				}
				});
			}
			</script>';
		}
		//fine ezcloud

		echo "</fieldset><br /><br />";
		
		if ($current_state == 15)
		{
			/*$array_parziali1 = Db::getInstance()->executeS('SELECT cod_articolo FROM fattura WHERE rif_vs_ordine = '.$order->id);
			$array_parziali = array();
			foreach($array_parziali1 as $ar)
				$array_parziali[] = trim($ar['cod_articolo']);*/
		}	
		

		// List of products
		echo '
		
			
		<a name="products"><br /></a>
		<form action="'.$currentIndex.'&submitCreditSlip&vieworder&token='.$tokenCustomers.'" method="post" onsubmit="return orderDeleteProduct(\''.AdminOrders::lx('Cannot return this product').'\', \''.AdminOrders::lx('Quantity to cancel is greater than quantity available').'\');">
			<input type="hidden" name="id_order" value="'.$order->id.'" />
			<script type="text/javascript">
			function delCons(id)
				{
					var delcons="no";
					if(document.getElementById("product_cons["+id+"]").checked == true)
						delcons="no";
					else
						delcons="yes";
					
					$.ajax({
					  url:"ajax.php?delcons="+delcons+"&qtforcons="+document.getElementById("qtforcons["+id+"]").value,
					  type: "POST",
					  data: { id_product: id,
					  id_order: '.$order->id.'
					  },
					  success:function(r){
						// alert(r);
						$("#cons_td_"+id).html("<img src=\"..\/img\/admin\/ok.gif\" alt=\"OK\" title=\"OK\"  \/>").fadeIn(100);
						$("#cons_td_"+id).html("<img src=\"..\/img\/admin\/ok.gif\" alt=\"OK\" title=\"OK\"  \/>").fadeOut(100);
					  },
					  error: function(xhr,stato,errori){
						 alert("Errore durante l\'operazione:"+xhr.status);
					  }
					});
					
					
				}
				
			function changeDDT(id)
				{
					if(document.getElementById("ddt["+id+"]").checked == true)
						ddt="0";
					else
						ddt="1";
					
					$.ajax({
					  url:"ajax.php?changeDDT="+ddt+"",
					  type: "POST",
					  data: { id_product: id,
					  id_order: '.$order->id.'
					  },
					  success:function(r){
						alert("Modifica avvenuta con successo");
					  },
					  error: function(xhr,stato,errori){
						 alert("Errore durante l\'operazione:"+xhr.status);
					  }
					});
					
					
				}	
				
			function saveDatiTecnici(id_order_detail, id_product,riga_max)
				{	
					for(riga=1; riga<=riga_max; riga++)
					{	
						$("#dati_tecnici_feedback_"+id_order_detail).html("<img src=\"../img/loader.gif\" />").show();
						var seriale = $(\'input[name="dt_seriale_\'+id_order_detail+\'[\'+riga+\']"]\').val();
						var mac = $(\'input[name="dt_mac_\'+id_order_detail+\'[\'+riga+\']"]\').val();
						var imei = $(\'input[name="dt_imei_\'+id_order_detail+\'[\'+riga+\']"]\').val();
						var note = $(\'input[name="dt_note_\'+id_order_detail+\'[\'+riga+\']"]\').val();
					
							$.post("ajax.php", {submitOrdineDatiTecnici:1,riga:riga,id_customer:'.(int)$customer->id.',id_order:'.$order->id.',seriale:seriale,mac:mac,imei:imei,note:note,id_order_detail:id_order_detail,id_product:id_product}, function (r) {
							$("#dati_tecnici_feedback_"+id_order_detail).html("").hide();
							if (r == "ok")
							{
								$("#dati_tecnici_feedback_"+id_order_detail).html("<b style=\"color:green\">'.addslashes(AdminOrders::lx('Dati tecnici salvati')).'</b>").fadeIn(400);
							}
							else if (r == "error:validation")
								$("#dati_tecnici_feedback_"+id_order_detail).html("<b style=\"color:red\">'.addslashes(AdminOrders::lx('Errore: dati tecnici non validi')).'</b>").fadeIn(400);
							else if (r == "error:update")
								$("#dati_tecnici_feedback_"+id_order_detail).html("<b style=\"color:red\">'.addslashes(AdminOrders::lx('Errore: dati tecnici non salvati')).'</b>").fadeIn(400);
							else
								$("#dati_tecnici_feedback_"+id_order_detail).html(r).fadeIn(400);
							
							// 
						
						});
					}
				}
				
			</script>
			<fieldset style="width: 97%; margin-top:-30px ">
				<legend><img src="../img/admin/cart.gif" alt="'.AdminOrders::lx('Prodotti').'" />'.AdminOrders::lx('Prodotti').'</legend>
				<div style="float:left;">
				
				
				
					<table style="width: 101%;" class="table" id="orderProducts">
						<tr>
							<th></th>
							<th style="width: 170px">Codice</th>
							<th style="width: 320px">Nome prodotto</th>
							<th style="width: 75px; text-align: center">Qta</th>
							'.(($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 14 || $cookie->id_employee == 11 || $cookie->id_employee == 7 || $cookie->id_employee == 3 || $cookie->id_employee == 2 || $cookie->id_employee == 19) && $current_state == 15 ? '<th style="width: 75px; text-align: center">Cons.</th>' : '').'
							'.($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 7 || $cookie->id_employee == 2  || $cookie->id_employee == 14  || $cookie->id_employee == 11 || $cookie->id_employee == 3 || $cookie->id_employee == 19 ? '<th>DDT</th>' : '').'
							
							<th style="width: 75px; text-align: right">Unitario</th>
							<th style="width: 45px; text-align: right">Sconto extra</th>
							<th style="width: 75px; text-align: right">'.($cookie->profile == 7 ? 'Premio' : 'Acquisto').'</th>
							<th style="width: 55px; text-align: right">'.($cookie->profile == 7 ? '' : 'Marginalit&agrave;').'</th>
							'.($order->id >= 23171 ? '<th style="width: 20px; text-align: center">'.AdminOrders::lx('Qt. disp. all\'ordine').'</th>' : '').'
							<!-- '.($order->hasBeenPaid() ? '<th style="width: 20px; text-align: center">'.AdminOrders::lx('Rest.').'</th>' : '').'
							'.($order->hasBeenDelivered() ? '<th style="width: 20px; text-align: center">'.AdminOrders::lx('Rmb.').'</th>' : '').' -->
							<th style="width: 30px; text-align: right">'.AdminOrders::lx('Rima- nenze').'</th>
							<th style="width: 30px; text-align: center">Kit?</th>
							<th style="min-width: 75px; text-align: right">'.AdminOrders::lx('Total').' <sup>*</sup></th>
							<!-- <th colspan="2" style="width: 90px;"><img src="../img/admin/delete.gif" alt="'.AdminOrders::lx('Products').'" /> '.(($order->hasBeenDelivered() || $order->hasBeenShipped()) ? AdminOrders::lx('Return') : ($order->hasBeenPaid() ? AdminOrders::lx('Refund') : AdminOrders::lx('Cancel'))).'</th> -->';
						echo '
						</tr>';
						$tokenCatalog = Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee));
						
						$margine_10 = 'no';
						$no_cons = 'no';
						$id_order_state_attuale = Db::getInstance()->getValue('SELECT id_order_state FROM order_history WHERE id_order = '.$order->id.' ORDER BY id_order_history DESC');
						
						foreach ($products as $k => $product)
						{
							
							if ($order->getTaxCalculationMethod() == PS_TAX_EXC)
								$product_price = $product['product_price'] + $product['ecotax'];
							else
								$product_price = $product['product_price_wt'];

							$image = array();
							if (isset($product['product_attribute_id']) AND (int)($product['product_attribute_id']))
								$image = Db::getInstance()->getRow('
								SELECT id_image
								FROM '._DB_PREFIX_.'product_attribute_image
								WHERE id_product_attribute = '.(int)($product['product_attribute_id']));
						 	if (!isset($image['id_image']) OR !$image['id_image'])
								$image = Db::getInstance()->getRow('
								SELECT id_image
								FROM '._DB_PREFIX_.'image
								WHERE id_product = '.(int)($product['product_id']).' AND cover = 1');
						 	$stock = Db::getInstance()->getRow('
							SELECT '.($product['product_attribute_id'] ? 'pa' : 'p').'.quantity
							FROM '._DB_PREFIX_.'product p
							'.($product['product_attribute_id'] ? 'LEFT JOIN '._DB_PREFIX_.'product_attribute pa ON p.id_product = pa.id_product' : '').'
							WHERE p.id_product = '.(int)($product['product_id']).'
							'.($product['product_attribute_id'] ? 'AND pa.id_product_attribute = '.(int)($product['product_attribute_id']) : ''));
							if (isset($image['id_image']))
							{
								$target = _PS_TMP_IMG_DIR_.'product_mini_'.(int)($product['product_id']).(isset($product['product_attribute_id']) ? '_'.(int)($product['product_attribute_id']) : '').'.jpg';
								if (file_exists($target))
									$products[$k]['image_size'] = getimagesize($target);
							}
							// Customization display
							//AdminOrders::displayCustomizedDatas($customizedDatas, $product, $currency, $image, $tokenCatalog, $k);

							// Normal display
							if ($product['product_quantity'] > $product['customizationQuantityTotal'])
							{
								$quantity = $product['product_quantity'] - $product['customizationQuantityTotal'];
								$imageObj = new Image($image['id_image']);
								
								//<tr'.((isset($image['id_image']) AND isset($products[$k]['image_size'])) ? ' height="'.($products[$k]['image_size'][1] + 7).'"' : '').'>
								
								if($product['wholesale_price'] > 0)
									$wholesale_price = $product['wholesale_price'];
								else if($product['no_acq'] == 1)
									$wholesale_price = 0;
								else
									$wholesale_price = Db::getInstance()->getValue('SELECT wholesale_price FROM product WHERE id_product = '.$product['product_id']);
								
								
								
								if (((($product_price - $wholesale_price)*100)/$product_price) < 10 && $product_price > 0) { $margine_10 = 'yes'; }
								if($current_state == 15)
								{	
								
									//$rif_fatt = Db::getInstance()->getValue('SELECT rif_vs_ordine FROM fattura WHERE rif_vs_ordine = '.$order->id);
									if(in_array(trim($product['product_reference']), $array_parziali))
										$no_cons = 'yes'; 
									else if ($product['cons'] == '')
										$no_cons = 'yes'; 
									else if (str_replace('0_','',$product['cons']) != $quantity)
										$no_cons = 'yes'; 
								}
									echo '<tr '.((((($product_price - $wholesale_price)*100)/$product_price) > 10 && $product_price > 0) || ($product_price == 0) ? '' : 'style="background-color:#ffcccc"').' '.($current_state == 15 ? ($product['cons'] == '')  ? '' : (str_replace('0_','',$product['cons']) == $quantity ? '' : 'style="background-color:#ffff00"') : '').'>	
									<td><img src="http://www.ezdirect.it/img/'.($product['id_image'] > 0 ? 'p/'.$product['product_id'].'-'.$product['id_image'] : 'm/'.Db::getInstance()->getValue('SELECT id_manufacturer FROM product WHERE id_product = '.$product['product_id'])).'-small.jpg" alt="" style="width:30px; height:30px" /></td>
									<td align="left"><img src="../img/admin/add.gif" alt="Espandi" style="cursor:pointer" title="Espandi" id="espandi_dati_tecnici_'.$product['id_order_detail'].'" />
									
									<script type="text/javascript">
										$(document).ready(function(){
											$("#espandi_dati_tecnici_'.$product['id_order_detail'].'").click(function(){
												$("tr.dati_tecnici_'.$product['id_order_detail'].'").toggle();
												if($("tr.dati_tecnici_'.$product['id_order_detail'].':visible").length) {
													$(this).attr("src", "../img/admin/forbbiden.gif");	
												}
												else {
													$(this).attr("src", "../img/admin/add.gif");
													$("tr.dati_tecnici_'.$product['id_order_detail'].'").hide();
												}
												return false;
											});
											
										});
									</script>
							
									'.($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 7 || $cookie->id_employee == 14 || $cookie->id_employee == 11 || $cookie->id_employee == 2 || $cookie->id_employee == 3 || $cookie->id_employee == 19 ? '<input type="checkbox" '.(($product['cons'] != '0' && $product['cons'] != '1') || ($product['cons'] == '')  ? 'checked="checked"' : '').' name="product_cons['.$product['product_id'].']" id="product_cons['.$product['product_id'].']" style="display:none" onchange="delCons('.$product['product_id'].');" />' : '').'<span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($product['product_id']).'"> <a href="index.php?tab=AdminCatalog&id_product='.$product['product_id'].'&updateproduct&token='.$tokenCatalog.'">'.$product['product_reference'].'</a></span></td>
										
									<td>
										<span class="productName">'.$product['product_name'].'</span></td>
										
									<td align="right" class="productQuantity" '.($quantity > 1 && $product['customizationQuantityTotal'] > 0 ? 'style="font-weight:700;font-size:1.1em;color:red"' : '').'>'.(int)$quantity.'</td>
									'.(($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 14 || $cookie->id_employee == 11 || $cookie->id_employee == 7 || $cookie->id_employee == 3 || $cookie->id_employee == 2 || $cookie->id_employee == 19) && $current_state == 15 ? '<td><input type="text" style="width:13px" value="'.($product['cons'] != '1' ? ($product['cons'] == '0' || $product['cons'] == '' ? $quantity : ($product['cons'] == 'aa' ? '0' : str_replace('0_','',$product['cons']))) : '0').'" name="qtforcons['.$product['product_id'].']" id="qtforcons['.$product['product_id'].']" onkeyup="if(this.value != 0) { document.getElementById(\'product_cons['.$product['product_id'].']\').checked = true; } else { document.getElementById(\'product_cons['.$product['product_id'].']\').checked = false; } delCons('.$product['product_id'].'); " /><div id="cons_td_'.$product['product_id'].'"></div></td>' : '').'';
									
									$unitario = Db::getInstance()->getValue("SELECT price FROM product WHERE id_product = '".$product['product_id']."'");
									
									$virtual = ProductDownload::getIdFromIdProduct($product['product_id']);
									
									echo ''.($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 7  || $cookie->id_employee == 14  || $cookie->id_employee == 11 || $cookie->id_employee == 2  || $cookie->id_employee == 3 || $cookie->id_employee == 19 ? '<td><input type="checkbox" name="ddt['.$product['product_id'].']" id="ddt['.$product['product_id'].']" '.($product['ddt'] == 0 ? 'checked="checked"' : '').' onchange="changeDDT('.$product['product_id'].')" /></td>' : '');
									
									if($id_order_state_attuale == 31)
										$originario = '<br /><span style="font-size:10px; color:red">('.number_format(Db::getInstance()->getValue('SELECT product_price FROM rimborsi WHERE product_id = '.$product['product_id'].' AND id_order = '.$order->id),2,",","").')</span>';
									else
										$originario = '';
									
									echo '
										
									<td align="right">'.Tools::displayPrice($product_price, $currency, false).''.$originario.'</td>
									<td align="right">'.($unitario > 0 ? number_format(Db::getInstance()->getValue('SELECT sconto_extra FROM cart_product WHERE id_product = '.$product['product_id'].' AND id_cart = '.$order->id_cart),2,",","").'%' : '').'</td>
									<td align="right">'.($cookie->profile == 7 ? '' : Tools::displayPrice($wholesale_price, $currency, false)).'</td>
									
									<td align="right">'.($cookie->profile == 7 ? '' : number_format(((($product_price - $wholesale_price)*100)/$product_price),2,",","").'%').'</td>
									
									
									
									'.($order->id >= 23171 ? '	<td align="right">'.($virtual <= 0 ? (int)$product['product_quantity_in_stock'] : '').'</td>' : '').'
								
									<!-- '.($order->hasBeenPaid() ? '<td align="right" class="productQuantity">'.(int)($product['product_quantity_refunded']).'</td>' : '').'
									'.($order->hasBeenDelivered() ? '<td align="right" class="productQuantity">'.(int)($product['product_quantity_return']).'</td>' : '').' -->
									<td align="right" class="productQuantity">'.($virtual <= 0 ? (int)$stock['quantity'] : '').'</td>
									
									<td>
									'.Product::controllaSeFaParteDiUnBundle($order->id, $product['product_id']).'
									
									</td>
									<td align="right">'.Tools::displayPrice(Tools::ps_round($product_price, 2) * ((int)($product['product_quantity']) - $product['customizationQuantityTotal']), $currency, false).'</td>
									
									<!-- <td align="center" class="cancelCheck">
										<input type="hidden" name="totalQtyReturn" id="totalQtyReturn" value="'.(int)($product['product_quantity_return']).'" />
										<input type="hidden" name="totalQty" id="totalQty" value="'.(int)($product['product_quantity']).'" />
										<input type="hidden" name="productName" id="productName" value="'.$product['product_name'].'" />';
								if ((!$order->hasBeenDelivered() OR Configuration::get('PS_ORDER_RETURN')) AND (int)($product['product_quantity_return']) < (int)($product['product_quantity']))
									echo '
										<input type="checkbox" name="id_order_detail['.$k.']" id="id_order_detail['.$k.']" value="'.$product['id_order_detail'].'" onchange="setCancelQuantity(this, '.(int)($product['id_order_detail']).', '.(int)($product['product_quantity_in_stock'] - $product['customizationQuantityTotal'] - $product['product_quantity_reinjected']).')" '.(((int)($product['product_quantity_return'] + $product['product_quantity_refunded']) >= (int)($product['product_quantity'])) ? 'disabled="disabled" ' : '').'/>';
								else
									echo '--';
								echo '
									</td> -->
									<!-- <td class="cancelQuantity">';
								if ((int)($product['product_quantity_return'] + $product['product_quantity_refunded']) >= (int)($product['product_quantity']))
									echo '<input type="hidden" name="cancelQuantity['.$k.']" value="0" />';
								elseif (!$order->hasBeenDelivered() OR Configuration::get('PS_ORDER_RETURN'))
									echo '
										<input type="text" id="cancelQuantity_'.(int)($product['id_order_detail']).'" name="cancelQuantity['.$k.']" size="2" onclick="selectCheckbox(this);" value="" /> ';
								echo AdminOrders::getCancelledProductNumber($order, $product).'
									</td> -->
								</tr>';
								
								for($idt = 1; $idt<=$product['product_quantity']; $idt++)
								{		
									$dati_tecnici_attuali = Db::getInstance()->getRow('SELECT * FROM dati_tecnici_prodotti WHERE id_order_detail = '.$product['id_order_detail'].' AND id_product = '.$product['product_id'].' AND riga = '.$idt.'');
									echo '<tr class="dati_tecnici_'.$product['id_order_detail'].'" style="display:none"><td colspan="13">
									Seriale: <input onkeyup="saveDatiTecnici('.$product['id_order_detail'].', '.$product['product_id'].', '.$idt.'); return false;" type="text" name="dt_seriale_'.$product['id_order_detail'].'['.$idt.']" value="'.htmlspecialchars($dati_tecnici_attuali['seriale']).'" />
									MAC: <input onkeyup="saveDatiTecnici('.$product['id_order_detail'].', '.$product['product_id'].', '.$idt.'); return false;" type="text" name="dt_mac_'.$product['id_order_detail'].'['.$idt.']" value="'.htmlspecialchars($dati_tecnici_attuali['mac']).'" class="dt_mac_'.$product['id_order_detail'].'" />
									IMEI: <input onkeyup="saveDatiTecnici('.$product['id_order_detail'].', '.$product['product_id'].', '.$idt.'); return false;" type="text" name="dt_imei_'.$product['id_order_detail'].'['.$idt.']" value="'.htmlspecialchars($dati_tecnici_attuali['imei']).'" />
									Note: <input onkeyup="saveDatiTecnici('.$product['id_order_detail'].', '.$product['product_id'].', '.$idt.'); return false;" type="text" name="dt_note_'.$product['id_order_detail'].'['.$idt.']" value="'.htmlspecialchars($dati_tecnici_attuali['note']).'" size="50" /><span id="dati_tecnici_feedback_'.$product['id_order_detail'].'"></span>
									</td></tr>';
								}
								
								echo '<!-- <tr class="dati_tecnici_'.$product['id_order_detail'].'" style="display:none"><td colspan="2">
									<button type="submit" class="button" name="submitDatiTecnici" onclick="saveDatiTecnici('.$product['id_order_detail'].', '.$product['product_id'].', '.($idt-1).'); return false;"><img src="../img/admin/save.gif" alt="Salva" title="Salva" />&nbsp;&nbsp;&nbsp;Salva</button>
									</td><td colspan="9" id="dati_tecnici_feedback_'.$product['id_order_detail'].'"></td></tr> -->';
							}
						}
						
						
					/* Display summary order */
		
		$tax_rate_query = ("SELECT tax_rate FROM order_tax WHERE id_order = ".$order->id."");
		$txrow = Db::getInstance()->getRow($tax_rate_query);
		$carrier_tax_rate = Db::getInstance()->getValue("SELECT carrier_tax_rate FROM orders WHERE id_order = ".$order->id."");
		
		$prodtaxes = Db::getInstance()->executeS('SELECT tax_rate FROM order_detail WHERE id_order = '.$order->id);
		$wtxc = 0;
		foreach($prodtaxes as $txc)
		{
			if($txc['tax_rate'] > 0)
				$wtxc = 1;
		}
		
		if($wtxc == 0)
			$carrier_tax_rate = 0;
		else
			$carrier_tax_rate = $_POST['tax_rate'];
		
	
		if(isset($txrow['tax_rate'])) {
			$tax = (int)($txrow['tax_rate']);
			if($tax == 3) {
				$tax = (string)"04";
			}
			$tax2 = (string)"1.".$tax;
			$tax3 = (float)$tax2;
			
		
			$id_country = Db::getInstance()->getValue("SELECT id_country FROM address WHERE id_address = ".$order->id_address_delivery."");
			
			if($id_country == 10) {
				Db::getInstance()->executeS("UPDATE orders SET carrier_tax_rate = ".$carrier_tax_rate." WHERE id_order = ".$order->id."");
			}
				
			else {
				
				if($customer->is_company == 1  || $customer->is_company == 0 && $id_country == 19) {
					$carrier_tax_rate = 0;
					Db::getInstance()->executeS("UPDATE orders SET carrier_tax_rate = ".$carrier_tax_rate." WHERE id_order = ".$order->id."");
				}
				
			}
			
			
			$ordersubtotal = $order->total_products;
		
			if($carrier_tax_rate > 0)
				$ordershipping = $order->total_shipping/$carrier_tax_rate;
			else
				$ordershipping = $order->total_shipping;
			
			$ordercommissions = $order->total_commissions;
			
			$discountIE = number_format($order->total_discounts, 2);
			$ordersubtotal = $ordersubtotal + ($ordershipping / ((100 + Db::getInstance()->getValue("SELECT carrier_tax_rate FROM orders WHERE id_order = ".$order->id."")) / 100) ) + $ordercommissions - $discountIE;
		
			$ordertotal = $order->total_paid;
			$taxes = $ordertotal - $ordersubtotal;
			Db::getInstance()->executeS("UPDATE orders SET total_shipping = ".$ordershipping." WHERE id_order = ".$order->id."");
			$ordershipping = ((($ordershipping*100)/(100+$order->carrier_tax_rate)));
			$ordercommissions = Tools::displayPrice($ordercommissions, $currency, false);
			$ordersubtotal = Tools::displayPrice($ordersubtotal, $currency, false);
			$ordershipping = Tools::displayPrice($ordershipping, $currency, false);
			$discountIE = Tools::displayPrice($discountIE, $currency, false);
			$taxes = Tools::displayPrice($taxes, $currency, false);
			$ordertotal = Tools::displayPrice($ordertotal, $currency, false);
			
		
		}
		else {
			
			$carrier_tax_rate = 0;
			Db::getInstance()->executeS("UPDATE orders SET carrier_tax_rate = ".$carrier_tax_rate." WHERE id_order = ".$order->id."");
			$ordersubtotal = $order->total_products;
			$ordershipping = ((($order->total_shipping*100)/(100+$carrier_tax_rate)));
			$ordersubtotal = $ordersubtotal = $ordersubtotal + ($ordershipping / ((100 + $carrier_tax_rate) / 100) ) ;
			Db::getInstance()->executeS("UPDATE orders SET total_shipping = ".$ordershipping." WHERE id_order = ".$order->id."");
			$ordertotal = $order->total_paid;
			$taxes = $ordertotal - $ordersubtotal;
			$ordercommissions = $order->total_commissions;
			
			$ordercommissions = Tools::displayPrice($ordercommissions, $currency, false);
			$ordersubtotal = Tools::displayPrice($ordersubtotal, $currency, false);
			$ordershipping = Tools::displayPrice($ordershipping, $currency, false);
			$taxes = Tools::displayPrice($taxes, $currency, false);
			$ordertotal = Tools::displayPrice($ordertotal, $currency, false);
		}
		
		
		
		if($order->payment == 'Contrassegno') {
			$spcr = AdminOrders::lx('Spedizione + contrassegno');
		}
		else {
			$spcr = AdminOrders::lx('Spedizione');
		}
		
		/*echo '<div>
				<table class="table" width="300px;" cellspacing="0" cellpadding="0">
				<tr><td width="150px;">'.AdminOrders::lx('Products').' i.e.</td><td align="right">'.Tools::displayPrice($order->getTotalProductsWithoutTaxes(), $currency, false).'</td></tr>
				
					
					'.($order->total_discounts > 0 ? '<tr><td>'.AdminOrders::lx('Discounts').'</td><td align="right">-'.$discountIE.'</td></tr>' : '').'
					'.($order->total_wrapping > 0 ? '<tr><td>'.AdminOrders::lx('Wrapping').'</td><td align="right">'.Tools::displayPrice($order->total_wrapping, $currency, false).'</td></tr>' : '').'
					<tr><td>'.$spcr.'</td><td align="right">'.$ordershipping.'</td></tr>
						<tr><td width="150px;">'.AdminOrders::lx('Subtotale').' </td><td align="right">'.$ordersubtotal.'</td></tr>
				
					
						<tr><td width="150px;">'.AdminOrders::lx('IVA').'</td><td align="right">'.$taxes.'</td></tr>
				
					
					
					<tr style="font-size: 20px"><td>'.AdminOrders::lx('Total').'</td><td align="right">'.$ordertotal.'</td></tr>
				</table>
			</div>
			<div style="float: left; margin-right: 0px; margin-left: 0px;">
				<span class="bold">'.AdminOrders::lx('Recycled package:').'</span>
				'.($order->recyclable ? '<img src="../img/admin/enabled.gif" />' : '<img src="../img/admin/disabled.gif" />').'
			</div>
			<div style="float: left; margin-right: 0px;">
				<span class="bold">'.AdminOrders::lx('Gift wrapping:').'</span>
				 '.($order->gift ? '<img src="../img/admin/enabled.gif" />
			</div>
			<div style="clear: left; padding-top: 2px;">
				'.(!empty($order->gift_message) ? '<div style="border: 1px dashed #999; padding: 5px; margin-top: 8px;"><b>'.AdminOrders::lx('Message:').'</b><br />'.nl2br2($order->gift_message).'</div>' : '') : '<img src="../img/admin/disabled.gif" />').'
			</div>
		</div>';*/
		
					if($id_order_state_attuale == 31)
					{
						$trasp_value_origin = Db::getInstance()->getValue('SELECT product_price FROM rimborsi WHERE product_id = "TRASP" AND id_order = '.$order->id);
						$trasp_value_origin = (($trasp_value_origin*100)/(100+$order->carrier_tax_rate));
						$trasp_originario = '<br /><span style="font-size:10px; color:red">('.number_format($trasp_value_origin,2,",","").')</span>';
					}
					else
						$trasp_originario = '';
									
					echo '
					<tr>
					<td></td>
					<td style="width: 130px"></td><td style="width: 370px">Prodotti</td>
					'.((($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 7 || $cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 14 || $cookie->id_employee == 11 || $cookie->id_employee == 7 || $cookie->id_employee == 3 || $cookie->id_employee == 2 || $cookie->id_employee == 19) && $current_state == 15) ? '<td></td>' : '').'
					<td style="width: 45px; text-align: center"></td><td style="width: 75px; text-align: center"></td><td></td>'.($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 7  || $cookie->id_employee ==  14 || $cookie->id_employee == 11 || $cookie->id_employee ==  2 || $cookie->id_employee ==  3  || $cookie->id_employee == 19 ? '<td></td>' : '').'
					<td style="width: 75px; text-align: center"></td><td style="width: 55px; text-align: center"></td>'.($order->id >= 23171 ? '<td style="width: 20px; text-align: center"></td>' : '').'<td style="width: 30px; text-align: center"></td><td style="width: 30px; text-align: center"> </td>
					<td style="width: 85px; text-align: right">'.Tools::displayPrice($order->getTotalProductsWithoutTaxes(), $currency, false).'<sup></sup></td>
					</tr>
					
					<tr>
					<td></td>
					<td style="width: 130px">TRASP</td><td style="width: 370px">Spedizione</td>
					'.((($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 7 || $cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 14 || $cookie->id_employee == 11 || $cookie->id_employee == 7 || $cookie->id_employee == 3 || $cookie->id_employee == 2 || $cookie->id_employee == 19) && $current_state == 15) ? '<td></td>' : '').'
					<td style="width: 45px; text-align: center"></td><td style="width: 75px; text-align: center"></td><td style="text-align:right">'.$trasp_originario.'</td>'.($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 7  || $cookie->id_employee ==  14 || $cookie->id_employee == 11 || $cookie->id_employee ==  2 || $cookie->id_employee ==  3  || $cookie->id_employee == 19 ? '<td></td>' : '').'
					<td style="width: 75px; text-align: center"></td><td style="width: 55px; text-align: center"></td>'.($order->id >= 23171 ? '<td style="width: 20px; text-align: center"></td>' : '').'<td style="width: 30px; text-align: center"></td><td style="width: 30px; text-align: center"> </td>
					<td style="width: 85px; text-align: right">'.$ordershipping.'<sup></sup></td>
					</tr>
					
					'.($order->total_discounts > 0 ? '<tr>
					<td></td>
					<td style="width: 130px"></td><td style="width: 370px">Sconti</td>
					'.((($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 7 || $cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 14 || $cookie->id_employee == 11 || $cookie->id_employee == 7 || $cookie->id_employee == 3 || $cookie->id_employee == 2 || $cookie->id_employee == 19) && $current_state == 15) ? '<td></td>' : '').'
					<td style="width: 45px; text-align: center"></td><td style="width: 75px; text-align: center"></td><td></td>'.($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 7  || $cookie->id_employee ==  14 || $cookie->id_employee == 11 || $cookie->id_employee ==  2 || $cookie->id_employee ==  3 || $cookie->id_employee == 19 ? '<td></td>' : '').'
					<td style="width: 75px; text-align: center"></td><td style="width: 55px; text-align: center"></td>'.($order->id >= 23171 ? '<td style="width: 20px; text-align: center"></td>' : '').'<td style="width: 30px; text-align: center"></td><td style="width: 30px; text-align: center"> </td>
					<td style="width: 85px; text-align: right">-'.$discountIE.'<sup></sup></td>
					</tr>' : '').'
					';
					if($ordercommissions != '')
					{
						echo '
						<tr>
						<td></td>
						<td style="width: 130px"></td><td style="width: 370px">Commissioni</td>
						'.((($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 7 || $cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 14 || $cookie->id_employee == 11 || $cookie->id_employee == 7 || $cookie->id_employee == 3 || $cookie->id_employee == 2 || $cookie->id_employee == 19) && $current_state == 15) ? '<td></td>' : '').'
						<td style="width: 45px; text-align: center"></td><td style="width: 75px; text-align: center"></td><td></td>'.($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 7  || $cookie->id_employee ==  14 || $cookie->id_employee == 11 || $cookie->id_employee ==  2 || $cookie->id_employee ==  3 || $cookie->id_employee == 19 ? '<td></td>' : '').'
						<td style="width: 75px; text-align: center"></td><td style="width: 55px; text-align: center"></td>'.($order->id >= 23171 ? '<td style="width: 20px; text-align: center"></td>' : '').'<td style="width: 30px; text-align: center"></td><td style="width: 30px; text-align: center"> </td>
						<td style="width: 85px; text-align: right">'.$ordercommissions.'<sup></sup></td>
						</tr>';
						
					}
					echo '
					<tr>
					<td></td>
					<td style="width: 130px"></td><td style="width: 370px">Subtotale</td>
					'.((($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 7 || $cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 14 || $cookie->id_employee == 11 || $cookie->id_employee == 7 || $cookie->id_employee == 3 || $cookie->id_employee == 2 || $cookie->id_employee == 19) && $current_state == 15) ? '<td></td>' : '').'
					<td style="width: 45px; text-align: center"></td><td style="width: 75px; text-align: center"></td><td></td>'.($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 7 || $cookie->id_employee ==  14 || $cookie->id_employee == 11 || $cookie->id_employee ==  2 || $cookie->id_employee ==  3  || $cookie->id_employee == 19 ? '<td></td>' : '').'
					<td style="width: 75px; text-align: center"></td><td style="width: 55px; text-align: center"></td>'.($order->id >= 23171 ? '<td style="width: 20px; text-align: center"></td>' : '').'<td style="width: 30px; text-align: center"></td><td style="width: 30px; text-align: center"> </td>
					<td style="width: 85px; text-align: right">'.$ordersubtotal.'<sup></sup></td>
					</tr>
					
					<tr>
					<td></td>
					<td style="width: 130px"></td><td style="width: 370px">IVA</td>
					'.((($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 7 || $cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 14 || $cookie->id_employee == 11 || $cookie->id_employee == 7 || $cookie->id_employee == 3 || $cookie->id_employee == 2 || $cookie->id_employee == 19) && $current_state == 15) ? '<td></td>' : '').'
					<td style="width: 45px; text-align: center"></td><td style="width: 75px; text-align: center"></td><td></td>'.($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 7  || $cookie->id_employee ==  14 || $cookie->id_employee == 11 || $cookie->id_employee ==  2 || $cookie->id_employee ==  3 || $cookie->id_employee == 19 ? '<td></td>' : '').'
					<td style="width: 75px; text-align: center"></td><td style="width: 55px; text-align: center"></td>'.($order->id >= 23171 ? '<td style="width: 20px; text-align: center"></td>' : '').'<td style="width: 30px; text-align: center"></td><td style="width: 30px; text-align: center"></td>
					<td style="width: 85px; text-align: right">'.$taxes.'<sup></sup></td>
					</tr>
					<tr>
					<td></td>
					<td style="width: 130px"></td><td style="width: 370px">Totale ordine</td>
					'.((($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 7 || $cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 14 || $cookie->id_employee == 11 || $cookie->id_employee == 7 || $cookie->id_employee == 3 || $cookie->id_employee == 2 || $cookie->id_employee == 19) && $current_state == 15) ? '<td></td>' : '').'
					<td style="width: 45px; text-align: center"></td><td style="width: 75px; text-align: center"></td><td></td>'.($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 7  || $cookie->id_employee ==  14 || $cookie->id_employee == 11 || $cookie->id_employee ==  2 || $cookie->id_employee ==  3 || $cookie->id_employee == 19 ? '<td></td>' : '').'
					<td style="width: 75px; text-align: center"></td><td style="width: 55px; text-align: center"></td>'.($order->id >= 23171 ? '<td style="width: 20px; text-align: center"></td>' : '').'<td style="width: 30px; text-align: center"></td><td style="width: 30px; text-align: center"> </td>
					<td style="width: 85px; text-align: right">'.$ordertotal.'<sup></sup></td>
					</tr>
					
					</table>';
					
					echo ''.($no_cons == 'yes' || $margine_10 == 'yes' ? '<br />Legenda righe - ' : '').' '.($no_cons == 'yes' ? '<span style="background-color:#ffff00">Prodotto in ordine parziale ancora da consegnare (la cella nella colonna a fianco della quantit&agrave; indica il numero di articoli gi&agrave; consegnati)</span>' : '').' '.($margine_10 == 'yes' ? ' - <span style="background-color:#ffcccc">Prodotto venduto con margine sotto al 10%</span>' : '').'<br />';
					
					if($id_order_state_attuale == 31)
					{
						echo '<br /><span style="color:red; font-size:10px">In rosso e tra parentesi, l\'importo originario del prodotto</span>';
						
					}
					
					
					/*
					<div style="float:left; width:280px; margin-top:15px;"><sup>*</sup> '.AdminOrders::lx('According to the group of this customer, prices are printed:').' '.($order->getTaxCalculationMethod() == PS_TAX_EXC ? AdminOrders::lx('tax excluded.') : AdminOrders::lx('tax included.')).(!Configuration::get('PS_ORDER_RETURN') ? '<br /><br />'.AdminOrders::lx('Merchandise returns are disabled') : '').'</div>';*/
					if (sizeof($discounts))
					{
						echo '
					<div style="float:left; width:280px; margin-top:15px;">
					<table cellspacing="0" cellpadding="0" class="table" style="width:90%;">
						<tr>
							<th><img src="../img/admin/coupon.gif" alt="'.AdminOrders::lx('Discounts').'" />'.AdminOrders::lx('Discount name').'</th>
							<th align="center" style="width: 100px">'.AdminOrders::lx('Value').'</th>
						</tr>';
						foreach ($discounts as $discount)
							echo '
						<tr>
							<td>'.$discount['name'].'</td>
							<td align="center">'.($discount['value'] != 0.00 ? '- ' : '').Tools::displayPrice($discount['value'], $currency, false).'</td>
						</tr>';
						echo '
					</table></div>';
					}
				echo '
				</div>';

				// Cancel product
				/*
				echo '
				<div style="clear:both; height:15px;">&nbsp;</div>
				<div style="float: right; width: 160px;">';
				if ($order->hasBeenDelivered() AND Configuration::get('PS_ORDER_RETURN'))
					echo '
					<input type="checkbox" id="reinjectQuantities" name="reinjectQuantities" class="button" />&nbsp;<label for="reinjectQuantities" style="float:none; font-weight:normal;">'.AdminOrders::lx('Re-stock products').'</label><br />';
				if ((!$order->hasBeenDelivered() AND $order->hasBeenPaid()) OR ($order->hasBeenDelivered() AND Configuration::get('PS_ORDER_RETURN')))
					echo '
					<input type="checkbox" id="generateCreditSlip" name="generateCreditSlip" class="button" onclick="toogleShippingCost(this)" />&nbsp;<label for="generateCreditSlip" style="float:none; font-weight:normal;">'.AdminOrders::lx('Generate a credit slip').'</label><br />
					<input type="checkbox" id="generateDiscount" name="generateDiscount" class="button" onclick="toogleShippingCost(this)" />&nbsp;<label for="generateDiscount" style="float:none; font-weight:normal;">'.AdminOrders::lx('Generate a voucher').'</label><br />
					<span id="spanShippingBack" style="display:none;"><input type="checkbox" id="shippingBack" name="shippingBack" class="button" />&nbsp;<label for="shippingBack" style="float:none; font-weight:normal;">'.AdminOrders::lx('Repay shipping costs').'</label><br /></span>';
				if (!$order->hasBeenDelivered() OR ($order->hasBeenDelivered() AND Configuration::get('PS_ORDER_RETURN')))
					echo '
					<div style="text-align:center; margin-top:5px;"><input type="submit" name="cancelProduct" value="'.($order->hasBeenDelivered() ? AdminOrders::lx('Return products') : ($order->hasBeenPaid() ? AdminOrders::lx('Refund products') : AdminOrders::lx('Cancel products'))).'" class="button" style="margin-top:8px;" /></div>';
				echo '
				</div>';*/
			echo '
			</fieldset>
		</form>
		<div class="clear" style="height:20px;">&nbsp;</div>';
		
		if($cookie->profile == 7)
		{
			
		}
		else
		{
			echo '
			<fieldset style="width: 97%">
		
				<legend><img src="../img/admin/details.gif" /> '.AdminOrders::lx('Area carrello').'</legend>
				<div style="float:left; width:45%">
				'.AdminOrders::lx('Carrello:').' '.(Tools::getValue('tab') == 'AdminCustomers' ? "<a href='index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&id_cart=".$cart->id."&viewcart&token=".$tokenCustomers."&tab-container-1=4'>" : '<a href="index.php?tab=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&tab-container-1=4&id_cart='.(int)($cart->id).'&viewcart'.'&token='.Tools::getAdminToken('AdminCarts'.(int)(Tab::getIdFromClassName('AdminCarts')).(int)($cookie->id_employee)).'">').''.AdminOrders::lx('Carrello numero ').($cart->id).'</a><br />';
				$creato_da = Db::getInstance()->getValue("SELECT created_by FROM cart WHERE id_cart = ".$cart->id."");
				
				if($impiegato != 0) { $nome_impiegato = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".$impiegato.""); } else { $nome_impiegato = 'Cliente'; }
				if($creato_da > 0) {
					$creato_da = Db::getInstance()->getValue("SELECT created_by FROM cart WHERE id_cart = ".$cart->id."");
					$creato_da_nome = " ".Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".$creato_da."");
					if($creato_da == 0) { $creato_da_nome = "l cliente e modificato da "; 
						$modificato_da = Db::getInstance()->getValue("SELECT id_employee FROM cart WHERE id_cart = ".$cart->id."");
						$modificato_da_nome = " ".Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".$modificato_da."");
						$creato_da_nome .= $modificato_da_nome;
					}
					echo "<strong>Quest'ordine arriva da un carrello creato da".$creato_da_nome.".</strong> ";
					
					$cart_name = Db::getInstance()->getValue('SELECT name FROM cart WHERE id_cart = '.$cart->id);
					if($cart_name != '')
					{
						
						echo '<br /><br /><strong>Oggetto del carrello</strong>: '.$cart_name;
						
					}
					$array_carrello_originale = Db::getInstance()->executeS("SELECT c.id_customer, cp.id_product, cp.quantity, cp.price FROM carrelli_creati c JOIN carrelli_creati_prodotti cp ON c.id_cart = cp.id_cart WHERE cp.id_cart = ".$cart->id." GROUP BY cp.id_product");
						
					$array_carrello = Db::getInstance()->executeS("SELECT c.id_customer, cp.id_product, cp.quantity, cp.price FROM cart c JOIN cart_product cp ON c.id_cart = cp.id_cart WHERE cp.id_cart = ".$cart->id." GROUP BY cp.id_product");
						
					/*if($array_carrello_originale === $array_carrello) {
						echo "Il cliente non ha modificato l'offerta originale.";
					}
					else {
						$tokenCarts = Tools::getAdminToken('AdminCarts'.(int)(Tab::getIdFromClassName('AdminCarts')).(int)($cookie->id_employee));
						echo "<br /><strong style='color:red'>ATTENZIONE!</strong> Il cliente ha modificato l'offerta originale. <a href='index.php?tab=AdminCustomers&id_customer=".$cart->id_customer."&viewcustomer&tab-container-1=4&id_cart=".(int)($cart->id)."&viewcart&token=".$tokenCarts."'>Clicca qui per vedere l'offerta originale.</a>";
					}*/
				}
				else {
					echo "<strong>Questo carrello &egrave; stato creato dal cliente.</strong>";
				}
				
			//if($ctrl_cart > 0) {
				echo '
					<script type="text/javascript">  
					var iso = \''.$isoTinyMCE.'\' ;  
					var pathCSS = \''._THEME_CSS_DIR_.'\' ;  
					var ad = \''.$ad.'\' ;  
					</script>  
					<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce3/tiny_mce.js"></script>  
					<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>';  
				/* End TinyMCE */ 
				  global $cookie;  
				$iso = Language::getIsoById((int)($cookie->id_lang));  
				$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');  
				$ad = dirname($_SERVER["PHP_SELF"]);
				echo '<p>
				<strong>Premessa</strong>: <textarea name="premessa" class="rte" id="premessa" style="width:890px; background-color:#f0ebd6; border:1px solid #cccccc; overflow-y:auto; height:150px">'.$premessa.'</textarea><br />
				
				<table style="margin-left:-3px"><tr><td><strong>Esigenze del cliente</strong>: 
				
				
				
				<textarea name="esigenze" class="rte" id="esigenze" style="width:435px; background-color:#f0ebd6; border:1px solid #cccccc; overflow-y:auto; height:150px">'.$esigenze.'</textarea></td>
				<td style="padding-left:20px">
				<strong>Risorse</strong><textarea name="risorse" class="rte" id="risorse" style="width:430px; background-color:#f0ebd6; border:1px solid #cccccc; overflow-y:auto; height:150px">'.$risorse.'</textarea></td></tr></table><br />
				
				
				<strong>Note</strong>: <textarea name="note" class="rte" id="note" style="width:890px; background-color:#f0ebd6; border:1px solid #cccccc; overflow-y:auto; height:150px">'.$note.'</textarea><br />
				';
						
			echo '<span class="button" onclick="$(\'#mostra_nota_privata\').slideToggle(); " style="cursor: pointer; display:block; color:red; font-size:14px; font-weight:bold; width:882px; border:1px solid #cccccc; text-align:center; background-color:#f0f0ee; text-decoration:none"><img src="../img/admin/arrow.gif" alt="'.AdminOrders::lx('Dettaglio note private').'" title="'.AdminOrders::lx('Dettaglio note private').'" style="float:left; margin-right:5px;"/>'.AdminOrders::lx('Clicca qui per inserire note private/riservate - Non visibili all\'utente.').'</span>';
			
			$cart_note = Db::getInstance()->executeS('SELECT * FROM cart_note WHERE id_cart = '.$order->id_cart.'');
			
			echo '
				
				<form action="ajax.php" method="post" onsubmit="saveOrdineNotaPrivata(); return false;" id="ordine_nota_privata">
				<p><div id="mostra_nota_privata" style="'.(strip_tags(str_replace(" ","",$note_private)) == '' && count($cart_note) == 0 ? 'display:none;' : '').' margin-top:-15px">';
						
				echo '
				<script type="text/javascript" src="autosize.js"></script>
				<script type="text/javascript">
					function add_nota_privata() {
						var possible = "0123456789";
						var randomid = "";
						for( var i=0; i < 11; i++ ) {
							randomid += possible.charAt(Math.floor(Math.random() * possible.length));
						}
								
						$("<tr id=\'tr_note_"+randomid+"\'><td><textarea class=\'textarea_note\' name=\'note_nota[]\' onkeyup=\'auto_grow(this)\' id=\'note_nota[]\' style=\'width:580px;height:16px\'></textarea><input type=\'hidden\' name=\'note_nuova[]\' value=\'1\' /></td><td><input type=\'text\' readonly=\'readonly\' name=\'note_id_employee[]\' value=\''.Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM employee WHERE id_employee = '.$cookie->id_employee).'\' /></td><td><input type=\'text\' readonly=\'readonly\' name=\'note_date_add[]\' value=\''.date('d/m/Y').'\' /></td><td><a href=\'javascript:void(0)\' onclick=\'togli_nota("+randomid+");\'><img src=\'../img/admin/delete.gif\' alt=\'Cancella\' title=\'Cancella\' style=\'cursor:pointer\' /></a></td></tr>").appendTo("#tableNoteBody");
						
						$(".textarea_note").each(function () {
							this.style.height = ((this.scrollHeight)-4)+"px";
						});
						
						$(".textarea_note").on("change keyup keydown paste cut", "textarea", function () {
							$(this).height(0).height(this.scrollHeight);
						}).find("textarea").change();
						
						autosize($(".textarea_note"));

					}
						
					function togli_nota(id) {
						document.getElementById(\'tr_note_\'+id).innerHTML = "";
					}
						
						
					function cancella_nota(id) {
						$.ajax({
							  url:"ajax.php?cancellanota_cart=y",
							  type: "POST",
							  data: { 
							  cancella_nota: \'y\',
							  id_note: id
							  },
							  success:function(){
								alert("Nota cancellata con successo"); 
							  },
							  error: function(xhr,stato,errori){
								 alert("Errore nella cancellazione:"+xhr.status);
							  }
						});
					
					}
					
					function auto_grow(element) {
					}

					
					
					</script>
					
					<table><thead>'.(count($cart_note) > 0 ? '<tr><th>Nota</th><th>Creata da</th><th>Ultima modifica</th><th></th></tr>' : '' ).'</thead><tbody id="tableNoteBody">
					';
					
					
					foreach($cart_note as $cart_nota)
					{
						echo '<tr id="tr_note_'.$cart_nota['id_note'].'">';
						echo '
						<td>
						<textarea class="textarea_note" name="note_nota['.$cart_nota['id_note'].']" id="note_nota['.$cart_nota['id_note'].']" style="width:580px; height:16px" onkeyup="auto_grow(this)">'.($cart_nota['note']).'</textarea><input type="hidden" name="note_nuova['.$cart_nota['id_note'].']" id="note_nuova['.$cart_nota['id_note'].']" value="0" /></td>
						<td><input type="text" readonly="readonly" name="note_id_employee['.$cart_nota['id_note'].']" value="'.Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM employee WHERE id_employee = '.$cart_nota['id_employee']).'" /></td>
						<td><input type="text" readonly="readonly" name="note_date_add['.$cart_nota['id_note'].']" value="'.Tools::displayDate($cart_nota['date_upd'], 5).'" /></td>';
						echo '<td><a href="javascript:void(0)" onclick="javascript:var surec=window.confirm(\'Sei sicuro?\'); if (surec) { togli_nota('.$cart_nota['id_note'].'); cancella_nota('.$cart_nota['id_note'].'); } else { return false; }; "><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" style="cursor:pointer" /></a></td>';
						echo'
						</tr>
						';
					}
					
					echo '</tbody></table><br />';
					
					echo '
					<a href="javascript:void(0)" onclick="javascript:add_nota_privata()";><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" /> Clicca per aggiungere una nota privata</a> <br /><br />';
					
					if($order->date_add < '2017-12-08')
					{			echo '
				<textarea name="note_private" class="rte" id="note_private" style="width:890px;height:100px">'.$note_private.'</textarea>';
					}
				
				echo '<br /></div>
				';
				
				echo '<input type="submit" id="submitOrdineNotaPrivata" class="button" value="'.AdminOrders::lx('Salva premessa, esigenze, risorse, note e note private').'"  />
				
				<br /><div id="note_feedback"></div></form>
				<script type="text/javascript">
					function saveOrdineNotaPrivata()
					{	
						tinyMCE.triggerSave();
						
						
						var values1 = $("textarea.textarea_note");
						
						var myArray = $.map(values1, function(element) {     
							return element.name+"***"+element.value;                             
						});
						
						console.log(myArray);

						$("#note_feedback").html("<img src=\"../img/loader.gif\" />").show();
						var notePrivateContent = $("#note_private").val();
						var noteContent = $("#note").val();
						var esigenze = $("#esigenze").val();
						var premessa = $("#premessa").val();
						var risorse = $("#risorse").val();
						$.post("ajax.php", {submitOrdineNotaPrivata:1,id_customer:'.(int)$customer->id.',id_cart:"'.$cart->id.'",note:noteContent,notePrivate:notePrivateContent,premessa:premessa,esigenze:esigenze,risorse:risorse,note_nuova:myArray}, function (r) {
							$("#note_feedback").html("").hide();
							if (r == "ok")
							{
								$("#note_feedback").html("<b style=\"color:green\">'.addslashes(AdminOrders::lx('Note salvate')).'</b>").fadeIn(400);
							}
							else if (r == "error:validation")
								$("#note_feedback").html("<b style=\"color:red\">'.addslashes(AdminOrders::lx('Errore: note non valide')).'</b>").fadeIn(400);
							else if (r == "error:update")
								$("#note_feedback").html("<b style=\"color:red\">'.addslashes(AdminOrders::lx('Errore: impossibile salvare note')).'</b>").fadeIn(400);
							$("#note_feedback").fadeOut(3000);
						});
					}
				</script>
				
				';
				
			//}
			
			//else {
			
			//}
				
			echo '
			
			<div style="clear:both">
			</fieldset>';
		
			echo '</div>
			<div class="clear">&nbsp;</div>';

			/* Display invoice information */
			/*
				echo '<fieldset style="width: 90%">';
				if (($currentState->invoice OR $order->invoice_number) AND count($products))
					echo '<legend><a href="pdf.php?id_order='.$order->id.'&pdf"><img src="../img/admin/charged_ok.gif" /> '.AdminOrders::lx('Invoice').'</a></legend>
						<a href="pdf.php?id_order='.$order->id.'&pdf">'.AdminOrders::lx('Invoice #').'<b>'.Configuration::get('PS_INVOICE_PREFIX', (int)($cookie->id_lang)).sprintf('%06d', $order->invoice_number).'</b></a>
						<br />'.AdminOrders::lx('Created on:').' '.Tools::displayDate($order->invoice_date, (int)$cookie->id_lang, true);
				else
					echo '<legend><img src="../img/admin/charged_ko.gif" />'.AdminOrders::lx('Invoice').'</legend>
						'.AdminOrders::lx('No invoice yet.');
				echo '</fieldset><br /><div class="clear">&nbsp;</div>';
			*/
			/* Display send a message to customer & returns/credit slip*/
			$returns = OrderReturn::getOrdersReturn($order->id_customer, $order->id);
			$slips = OrderSlip::getOrdersSlip($order->id_customer, $order->id);
			echo '
			<div style="width:97%">
				<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'&token='.$tokenCustomers.'" method="post" onsubmit="if (getE(\'visibility\').checked == true) return confirm(\''.AdminOrders::lx('Do you want to send this message to the customer?', __CLASS__, true, false).'\');">
				<fieldset style="width: 100%">
					<legend style="cursor: pointer;" onclick="$(\'#message\').slideToggle();$(\'#message_m\').slideToggle();return false"><img src="../img/admin/email_edit.gif" /> '.AdminOrders::lx('Nuovo messaggio').'</legend>
					<div id="message_m" style="display: '.(Tools::getValue('message') ? 'none' : 'block').'; overflow: auto; width: 400px;">
						<a href="index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&viewmessage&aprinuovomessaggio&rif_ord='.$order->id.'&tab-container-1=7&token='.$tokenCustomers.'"><img src="../img/admin/email.gif" alt="'.AdminOrders::lx('Invia un messaggio al cliente').'" /> '.AdminOrders::lx('Invia un messaggio al cliente').'</a>
					</div>
					<div id="message" style="display: '.(Tools::getValue('message') ? 'block' : 'none').'">
						<select name="order_message" id="order_message" onchange="orderOverwriteMessage(this, \''.AdminOrders::lx('Do you want to overwrite your existing message?').'\')">
							<option value="0" selected="selected">-- '.AdminOrders::lx('Choose a standard message').' --</option>';
			$orderMessages = OrderMessage::getOrderMessages((int)($order->id_lang));
			foreach ($orderMessages AS $orderMessage)
				echo '		<option value="'.htmlentities($orderMessage['message'], ENT_COMPAT, 'UTF-8').'">'.$orderMessage['name'].'</option>';
			echo '		</select><br /><br />
						<b>'.AdminOrders::lx('Display to consumer?').'</b>
						<input type="radio" name="visibility" id="visibility" value="0" /> '.AdminOrders::lx('Yes').'
						<input type="radio" name="visibility" value="1" checked="checked" /> '.AdminOrders::lx('No').'
						<p id="nbchars" style="display:inline;font-size:10px;color:#666;"></p><br /><br />
						<textarea id="txt_msg" name="message" cols="50" rows="8" onKeyUp="var length = document.getElementById(\'txt_msg\').value.length; if (length > 600) length = \'600+\'; document.getElementById(\'nbchars\').innerHTML = \''.AdminOrders::lx('600 chars max').' (\' + length + \')\';">'.htmlentities(Tools::getValue('message'), ENT_COMPAT, 'UTF-8').'</textarea><br /><br />
						<input type="hidden" name="id_order" value="'.(int)($order->id).'" />
						<input type="hidden" name="id_customer" value="'.(int)($order->id_customer).'" />
						<input type="submit" class="button" name="submitMessage" value="'.AdminOrders::lx('Send').'" />
					</div>
				</fieldset>
				</form><div class="clear">&nbsp;</div>';
			/* Display list of messages */
			if (sizeof($messages))
			{
				echo '
				
				<fieldset style="width: 100%">
				<legend><img src="../img/admin/email.gif" /> '.AdminOrders::lx('Messages').'</legend>';
				foreach ($messages as $message)
				{
					echo '<div style="overflow:auto; width:400px;" '.($message['is_new_for_me'] ?'class="new_message"':'').'>';
					if ($message['is_new_for_me'])
						echo '<a class="new_message" title="'.AdminOrders::lx('Mark this message as \'viewed\'').'" href="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'&token='.$tokenCustomers.'&messageReaded='.(int)($message['id_message']).'"><img src="../img/admin/enabled.gif" alt="" /></a>';
					echo AdminOrders::lx('At').' <i>'.Tools::displayDate($message['date_add'], (int)($cookie->id_lang), true);
					echo '</i> '.AdminOrders::lx('from').' <b>'.(($message['elastname']) ? ($message['efirstname'].' '.$message['elastname']) : ($message['cfirstname'].' '.$message['clastname'])).'</b>';
					echo ((int)($message['private']) == 1 ? '<span style="color:red; font-weight:bold;">'.AdminOrders::lx('Private:').'</span>' : '');
					echo '<p>'.nl2br2($message['message']).'</p>';
					echo '</div>';
					echo '<br />';
				}
				echo '<p class="info">'.AdminOrders::lx('When you read a message, please click on the green check.').'</p>';
				echo '</fieldset>';
			}
			echo '</div>';

			/* Display return product */
			/*
			echo '<div>
				<fieldset style="width: 97%">
					<legend><img src="../img/admin/return.gif" alt="'.AdminOrders::lx('Merchandise returns').'" />'.AdminOrders::lx('Merchandise returns').'</legend>';
			if (!sizeof($returns))
				echo AdminOrders::lx('No merchandise return for this order.');
			else
				foreach ($returns as $return)
				{
					$state = new OrderReturnState($return['state']);
					echo '('.Tools::displayDate($return['date_upd'], $cookie->id_lang).') :
					<b><a href="index.php?tab=AdminReturn&id_order_return='.$return['id_order_return'].'&updateorder_return&token='.Tools::getAdminToken('AdminReturn'.(int)(Tab::getIdFromClassName('AdminReturn')).(int)($cookie->id_employee)).'">'.AdminOrders::lx('#').sprintf('%06d', $return['id_order_return']).'</a></b> -
					'.$state->name[$cookie->id_lang].'<br />';
				}
			echo '</fieldset>';
			*/
			/* Display credit slip */
			/*
			echo '
					<br />
					<fieldset style="width: 97%;">
						<legend><img src="../img/admin/slip.gif" alt="'.AdminOrders::lx('Credit slip').'" />'.AdminOrders::lx('Credit slip').'</legend>';
			if (!sizeof($slips))
				echo AdminOrders::lx('No slip for this order.');
			else
				foreach ($slips as $slip)
					echo '('.Tools::displayDate($slip['date_upd'], $cookie->id_lang).') : <b><a href="pdf.php?id_order_slip='.$slip['id_order_slip'].'">'.AdminOrders::lx('#').sprintf('%06d', $slip['id_order_slip']).'</a></b><br />';
			echo '</fieldset>
			</div>';
			*/
			/* Display sources */
			if (sizeof($sources))
			{
				echo '<br />
				<fieldset style="width: 97%"><legend><img src="../img/admin/tab-stats.gif" /> '.AdminOrders::lx('Fonti').'</legend><ul '.(sizeof($sources) > 3 ? 'style="height: 200px; overflow-y: scroll; width: 90%"' : '').'>';
				foreach ($sources as $source)
					echo '<li>
							'.Tools::displayDate($source['date_add'], (int)($cookie->id_lang), true).'<br />
							<b>'.AdminOrders::lx('From:').'</b> <a href="'.$source['http_referer'].'">'.preg_replace('/^www./', '', parse_url($source['http_referer'], PHP_URL_HOST)).'</a><br />
							<b>'.AdminOrders::lx('To:').'</b> '.$source['request_uri'].'<br />
							'.($source['keywords'] ? '<b>'.AdminOrders::lx('Keywords:').'</b> '.$source['keywords'].'<br />' : '').'<br />
						</li>';
				echo '</ul></fieldset>';
			}
			
			if (sizeof($storico))
			{
				echo '<br />
				<fieldset style="width: 97%"><legend><img src="../img/admin/date.png" /> '.AdminOrders::lx('Storico').'</legend><ul '.(sizeof($storico) > 3 ? 'style="height: 200px; overflow-y: scroll; width: 90%"' : '').'>';
				foreach ($storico as $storia)
					echo '<li>
							'.Tools::displayDate($storia['data_attivita'], (int)($cookie->id_lang), true).'<br />
							<b>'.Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM employee WHERE id_employee = '.$storia['id_employee']).'</b> '.$storia['desc_attivita'].'<br />
						</li>';
				echo '</ul></fieldset>';
			}
			
			// display hook specified to this page : AdminOrder
			if (($hook = Module::hookExec('adminOrder', array('id_order' => $order->id))) !== false)
				echo $hook;

			echo '
			';
			
			
			echo '<div class="clear">&nbsp;</div>';
			
			$is_cart = Db::getInstance()->getValue('SELECT count(id_cart) FROM cart WHERE id_customer = '.$order->id_customer.' AND id_cart = '.Db::getInstance()->getValue('SELECT id_cart FROM orders WHERE id_order = '.Tools::getValue('id_order')).'');
			
			$is_fattura =  Db::getInstance()->getValue('SELECT id_fattura FROM fattura WHERE rif_vs_ordine = '.Tools::getValue('id_order').' OR rif_ordine = '.Tools::getValue('id_order'));
			
			//if($is_cart > 0 && $is_fattura <= 0)
			if($is_cart > 0)	
				echo '<div style="text-align:center; margin-top:5px;"><a class="button" href="'.$currentIndex.(isset($_GET['id_customer']) ? '&id_customer='.$_GET['id_customer'].'&viewcustomer&tab-container-1=4' : '').'&id_order='.$order->id.'&vieworder&editorder&token='.$tokenCustomers.'#modifica-ordine">Modifica ordine</a></div>';
		
		}
		
		
		if(isset($_GET['editorder'])) {
		
		
			echo "<a name='modifica-ordine'><h1>Modifica ordine</h1></a>
			";
			
			echo '<script type="text/javascript">
			function calcPrezzoScontoHTML(sconto, prezzo, quantita, visualizzazione1, visualizzazione2)
			{
				var sconto = parseFloat(document.getElementById(sconto).value.replace(/,/g, "."));
				var prezzo = parseFloat(document.getElementById(prezzo).value.replace(/,/g, "."));
				var quantita = parseFloat(document.getElementById(quantita).value.replace(/,/g, "."));
				var nuovoPrezzo = prezzo - ((prezzo * sconto) / 100);
				
				var nuovoPrezzoQ = nuovoPrezzo*quantita;
				nuovoPrezzo = nuovoPrezzo.toFixed(2);
				nuovoPrezzo = nuovoPrezzo.toString();				
				nuovoPrezzo = nuovoPrezzo.replace(".",",");
				
				nuovoPrezzoQ = nuovoPrezzoQ.toFixed(2);
				nuovoPrezzoQ = nuovoPrezzoQ.toString();				
				nuovoPrezzoQ = nuovoPrezzoQ.replace(".",",");
				document.getElementById(visualizzazione1).innerHTML = nuovoPrezzo;
				document.getElementById(visualizzazione2).innerHTML = nuovoPrezzoQ;
			}
			</script>';
		
			?>
			
			<?php
		
			if ($_GET['id_order']) {
				$query="select distinct o.*,a.*,p.tax_rate,image.id_image from ". _DB_PREFIX_."orders o,". _DB_PREFIX_."address a,". _DB_PREFIX_."order_detail p left join (select * from image where cover = 1) image on p.product_id = image.id_product where a.id_address=o.id_address_delivery and o.id_order=p.id_order and o.id_order=".$_GET['id_order'];

				$order = Db::getInstance()->getRow($query);
				if ($order['id_order']>0) {
					
					$id_customer=$order['id_customer'];
					$id_lang=$order['id_lang'];
					$id_cart=$order['id_cart'];
					$payment=$order['payment'];
					$module=$order['module'];
					$tax_rate=$order['tax_rate'];
					$invoice_number=$order['invoice_number'];
					$delivery_number=$order['delivery_number'];
					$total_paid_real=$order['total_paid_real'];
					$total_products=$order['total_products'];
					$total_discounts=$order['total_discounts'];
					$total_shipping=$order['total_shipping'];
					$total_commissions=$order['total_commissions'];
					$total_wrapping=$order['total_wrapping'];
					$firstname=$order['firstname'];
					$lastname=$order['lastname'];
					$company=$order['company'];
					$address_delivery=$order['id_address_delivery'];
					
				}

			}
				$rif_fatt = Db::getInstance()->getValue('SELECT rif_vs_ordine FROM fattura WHERE rif_vs_ordine = '.$_GET['id_order']);
			if($rif_fatt == '')
				$rif_fatt = Db::getInstance()->getValue('SELECT rif_ordine FROM fattura WHERE rif_ordine = '.$_GET['id_order']);
			
			
			if($rif_fatt > 0)
			{
				echo '<script type="text/javascript" src="https://www.ezdirect.it/js/sweetalert2.js"></script>
				<link rel="stylesheet" href="https://www.ezdirect.it/css/sweetalert2.css" />
				
				<script type="text/javascript">
				swal(
				  "ATTENZIONE!",
				  "Stai modificando un ordine gi&agrave; fatturato. Avvisare l\'ufficio amministrazione!",
				  "warning"
				)
				</script>
				';
				
			}
			
			echo '<form name="products" id="order_edit" method="post" action="'.$currentIndex.(isset($_GET['id_customer']) ? '&id_customer='.$_GET['id_customer'].'&viewcustomer&tab-container-1=4' : '').'&id_order='.$_GET['id_order'].'&vieworder&orderupdated&token='.$tokenCustomers.'#modifica-ordine">	
			';

			echo '
			<script type="text/javascript" src="../js/jquery/jquery.tablednd_0_5.js"></script>
				<script type="text/javascript">
				$(document).ready(function() {
					$("#tableProducts").tableDnD();
				});
				</script>
				
			<script type="text/javascript">
			
			function ristabilisciPrezzo(id_product) {
			
				var numprice = document.getElementById(\'product_price[\'+id_product+\']\').value;
				document.getElementById(\'sconto_extra[\'+id_product+\']\').value = 0;
				var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
				var unitario = parseFloat(document.getElementById(\'unitario[\'+id_product+\']\').value);
				unitario = unitario.toFixed(2);
				unitario = unitario.toString();				
				unitario = unitario.replace(".",",");
				document.getElementById(\'product_price[\'+id_product+\']\').value=unitario;
				price=unitario;
			
			}
			
			
			
			function calcolaPrezzoScontoExtra(id_product, tipo) {
			
				var numunitario = document.getElementById(\'unitario[\'+id_product+\']\').value;
			
				var unitario = parseFloat(numunitario.replace(/\s/g, "").replace(",", "."));
				
				if (unitario == 0) {
				unitario = 0.001;
				}
				else {
				}
				
				var quantity = parseFloat(document.getElementById(\'product_quantity[\'+id_product+\']\').value);
				var numsconto_extra = document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".");
				
				var numprice = document.getElementById(\'product_price[\'+id_product+\']\').value;	
				var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
				var sconto_extra = parseFloat(numsconto_extra);
				
				if(sconto_extra == null)
					sconto_extra = 0;
					
				var sc_qta_1 = parseFloat(document.getElementById(\'sc_qta_1[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
				var sc_qta_2 = parseFloat(document.getElementById(\'sc_qta_2[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
				var sc_qta_3 = parseFloat(document.getElementById(\'sc_qta_3[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
				var sc_riv_1 = parseFloat(document.getElementById(\'sc_riv_1[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
				var sc_riv_2 = parseFloat(document.getElementById(\'sc_riv_2[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
				var sc_riv_3 = parseFloat(document.getElementById(\'sc_riv_3[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
				var sc_qta_1_q = parseFloat(document.getElementById(\'sc_qta_1_q[\'+id_product+\']\').value);
				var sc_qta_2_q = parseFloat(document.getElementById(\'sc_qta_2_q[\'+id_product+\']\').value);
				var sc_qta_3_q = parseFloat(document.getElementById(\'sc_qta_3_q[\'+id_product+\']\').value);
				var sc_riv_1_q = parseFloat(document.getElementById(\'sc_riv_1_q[\'+id_product+\']\').value);
				var sc_riv_2_q = parseFloat(document.getElementById(\'sc_riv_2_q[\'+id_product+\']\').value);	
				var sc_riv_3_q = parseFloat(document.getElementById(\'sc_riv_3_q[\'+id_product+\']\').value);	
				var finito = unitario - ((sconto_extra*unitario)/100);
				
				if (document.getElementById(\'usa_sconti_quantita[\'+id_product+\']\').checked == true) {
				';	
					if ($customer->id_default_group == 3) {
					
						echo '
						if(sc_riv_1 > unitario)
							sc_riv_1 = unitario;
							
						if(tipo == "sconto") {
							var finito = sc_riv_1 - ((sconto_extra*sc_riv_1)/100);
						}
						else if (tipo == "inverso") {
							var finito = ((sc_riv_1 - price)/sc_riv_1)*100;
						}
						
						
						';
						
					}
					
					else if ($customer->id_default_group == 12) {
					
						echo '
						if(tipo == "sconto") {
							var finito = sc_riv_3 - ((sconto_extra*sc_riv_3)/100);
						}
						else if (tipo == "inverso") {
							var finito = ((sc_riv_3 - price)/sc_riv_3)*100;
						}
						
						
						';
						
					}
					
					if ($customer->id_default_group == 22) {
						
						echo '
						
						if(sc_riv_2 > 0)
						{
							if(sc_riv_2 > unitario)
								sc_riv_2 = unitario;
							
							if(tipo == "sconto") {
								var finito = sc_riv_2 - ((sconto_extra*sc_riv_2)/100);
							}
							else if (tipo == "inverso") {
								var finito = ((sc_riv_2 - price)/sc_riv_2)*100;
							}
						}
						else
						{
							sc_riv_1 = sc_riv_1 + (sc_riv_1 / 100 * 3);
							if(sc_riv_1 > unitario)
							sc_riv_1 = unitario;
						
							if(tipo == "sconto") {
								var finito = sc_riv_1 - ((sconto_extra*sc_riv_1)/100);
							}
							else if (tipo == "inverso") {
								var finito = ((sc_riv_1 - price)/sc_riv_1)*100;
							}
						}
						';
						
					}
						
						
					else {
						
					echo '
					if(quantity < sc_qta_1_q && sc_qta_1_q != 0) {
						if(tipo == "sconto") {
							var finito = unitario - ((sconto_extra*unitario)/100);
						}
						else if (tipo == "inverso") {
							var finito = ((unitario - price)/unitario)*100;
						}
					}
						
					else if((quantity >= sc_qta_1_q && quantity < sc_qta_2_q) || (quantity >= sc_qta_1_q && isNaN(parseFloat(sc_qta_2_q)))) {
						if(tipo == "sconto") {
							var finito = sc_qta_1 - ((sconto_extra*sc_qta_1)/100);	
						}
						else if (tipo == "inverso") {
							var finito = ((sc_qta_1 - price)/sc_qta_1)*100;
						}
					}
						
					else if((quantity >= sc_qta_2_q && quantity < sc_qta_3_q) || (quantity >= sc_qta_2_q && isNaN(parseFloat(sc_qta_3_q == 0)))) {
						if(tipo == "sconto") {
							var finito = sc_qta_2 - ((sconto_extra*sc_qta_2)/100);
						}
						else if (tipo == "inverso") {
							var finito = ((sc_qta_2 - price)/sc_qta_2)*100;
						}
					}
						
					else if(quantity >= sc_qta_3_q && sc_qta_2_q != 0 && sc_qta_3_q != 0 && sc_qta_1_q != 0) {
						if(tipo == "sconto") {
							var finito = sc_qta_3 - ((sconto_extra*sc_qta_3)/100);					
						}
						else if (tipo == "inverso") {
							var finito = ((sc_qta_3 - price)/sc_qta_3)*100;
						}
						
					}	';
						
					} echo '				
				}
				else {
					if (tipo == "sconto") {
						var finito = unitario - ((sconto_extra*unitario)/100);
					}
					else if (tipo == "inverso") {
						var finito = ((unitario - price)/unitario)*100;
					}
				}
				
		
				finito = finito.toFixed(2);
				finito = finito.toString();				
				finito = finito.replace(".",",");
				
				if (tipo == "sconto") {
					document.getElementById(\'product_price[\'+id_product+\']\').value=finito;
				}
				else if (tipo == "inverso") {
					document.getElementById(\'sconto_extra[\'+id_product+\']\').value=finito;
				}
			}
			
			function calcolaImportoConSpedizione(id_metodo) {
			
				if(document.getElementById(\'trasporto_modificato\').value == "y") {
					var metodo_price = parseFloat(document.getElementById(\'transport\').value.replace(/\s/g, "").replace(",", "."));
				}
				else {
					var metodo_price = parseFloat(document.getElementById(\'metodo[\'+id_metodo+\']\').getAttribute("rel"));
				}
				var totale = 0;
				var arrayImporti = document.getElementsByClassName("importo");
					for (var i = 0; i < arrayImporti.length; ++i) {
						var item = parseFloat(arrayImporti[i].value);  
						totale += item;
					}
				var sconti = parseFloat(document.getElementById(\'total_discounts\').value.replace(/\s/g, "").replace(",", "."));	
				var commissioni = parseFloat(document.getElementById(\'total_commissions\').value.replace(/\s/g, "").replace(",", "."));	
				totale = totale+metodo_price;
				totale = totale+commissioni;
				totale = totale-sconti;
				totale = totale.toFixed(2);
				
				var totalep = totale;
				
				document.getElementById(\'totaleprodotti\').value=totale;
						
				document.getElementById(\'spantotaleprodotti\').innerHTML=totale.replace(".",",");
				
				$.ajax({
					url:"ajax_products_list2.php?calcola_totale_con_iva=y",
					type: "POST",
					data: { address: "'.$address_delivery.'",
					customer : "'.$id_customer.'",
					totalep : totalep
					 },
					success:function(resp){
						totale_con_iva = resp;
						totale_con_iva = parseFloat(totale_con_iva);
						totale_con_iva = totale_con_iva.toFixed(2);
						document.getElementById(\'spantotaleprodottiiva\').innerHTML=totale_con_iva.replace(".",",");		
					},
					error: function(xhr,stato,errori){
						alert("Errore nella cancellazione:"+xhr.status);
					}
					
					
				});
			
			}

			function calcolaImporto(id_product) {
					var numprice = document.getElementById(\'product_price[\'+id_product+\']\').value;
					
					var numunitario = document.getElementById(\'unitario[\'+id_product+\']\').value;
					var numacquisto = document.getElementById(\'wholesale_price[\'+id_product+\']\').value;
					
					var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
								
					var quantity = parseFloat(document.getElementById(\'product_quantity[\'+id_product+\']\').value);

					var unitario = parseFloat(numunitario.replace(/\s/g, "").replace(",", "."));
					var acquisto = parseFloat(numacquisto.replace(/\s/g, "").replace(",", "."));
					
					var spedizione = parseFloat(document.getElementById(\'transport\').value.replace(/\s/g, "").replace(",", "."));
					
					var sconti = parseFloat(document.getElementById(\'total_discounts\').value.replace(/\s/g, "").replace(",", "."));
					
					var commissioni = parseFloat(document.getElementById(\'total_commissions\').value.replace(/\s/g, "").replace(",", "."));
					
					
					var totaleacquisto = acquisto * quantity;
					
					document.getElementById(\'totaleacquisto[\'+id_product+\']\').value = totaleacquisto;
					
					

					var totale = parseFloat(document.getElementById(\'totaleprodotti\').value);
					var sconti = parseFloat(document.getElementById(\'total_discounts\').value.replace(/\s/g, "").replace(",", "."));
					var commissioni = parseFloat(document.getElementById(\'total_commissions\').value.replace(/\s/g, "").replace(",", "."));

					var sc_qta_1 = parseFloat(document.getElementById(\'sc_qta_1[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
					var sc_qta_2 = parseFloat(document.getElementById(\'sc_qta_2[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
					var sc_qta_3 = parseFloat(document.getElementById(\'sc_qta_3[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
					var sc_riv_1 = parseFloat(document.getElementById(\'sc_riv_1[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
					var sc_riv_2 = parseFloat(document.getElementById(\'sc_riv_2[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
					var sc_riv_3 = parseFloat(document.getElementById(\'sc_riv_3[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));

					var sc_qta_1_q = parseFloat(document.getElementById(\'sc_qta_1_q[\'+id_product+\']\').value);
					var sc_qta_2_q = parseFloat(document.getElementById(\'sc_qta_2_q[\'+id_product+\']\').value);
					var sc_qta_3_q = parseFloat(document.getElementById(\'sc_qta_3_q[\'+id_product+\']\').value);
					var sc_riv_1_q = parseFloat(document.getElementById(\'sc_riv_1_q[\'+id_product+\']\').value);
					var sc_riv_2_q = parseFloat(document.getElementById(\'sc_riv_2_q[\'+id_product+\']\').value);
					var sc_riv_3_q = parseFloat(document.getElementById(\'sc_riv_3_q[\'+id_product+\']\').value);
					
					
					var importo = parseFloat(price*quantity);
					
					
					
					if (document.getElementById(\'usa_sconti_quantita[\'+id_product+\']\').checked == true) {
					
					
						';
						if ($customer->id_default_group == 3 || $customer->id_default_group == 12) {
						
						echo '
							if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".") > 0) {
							} else {
								var prezzounitarionew = unitario;
								var prezziconfronto = new Array(sc_riv_1, '.($customer->id_default_group == 12 ? 'sc_riv_3, ' : '').' sc_qta_1, sc_qta_2, sc_qta_3);
								for (var i = 0; i < prezziconfronto.length; ++i) {
									if(prezziconfronto[i] !=0 && prezziconfronto[i] < prezzounitarionew) { 
										prezzounitarionew = prezziconfronto[i];
										
									} else { }
								}	
								var importo = parseFloat(prezzounitarionew*quantity);
								document.getElementById(\'product_price[\'+id_product+\']\').value=prezzounitarionew.toFixed(2).replace(".",",");					
							}
						';
						
						}
						
						else if ($customer->id_default_group == 22) {
							
								echo '
								if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".") > 0) {
								} else {
									var prezzounitarionew = unitario;
									
									if(sc_riv_2 == 0)
									{
										sc_riv_1 = sc_riv_1 + (sc_riv_1 / 100 *3);
										var prezziconfronto = new Array(sc_riv_1, sc_qta_1, sc_qta_2, sc_qta_3);
										for (var i = 0; i < prezziconfronto.length; ++i) {
											if(prezziconfronto[i] !=0 && prezziconfronto[i] < prezzounitarionew) { 
												prezzounitarionew = prezziconfronto[i];
												
											} else { }
										}	
									}
									else
									{
										var prezziconfronto = new Array(sc_riv_2, sc_qta_1, sc_qta_2, sc_qta_3);
										for (var i = 0; i < prezziconfronto.length; ++i) {
											if(prezziconfronto[i] !=0 && prezziconfronto[i] < prezzounitarionew) { 
												prezzounitarionew = prezziconfronto[i];
												
											} else { }
										}	
									}
									if(prezzounitarionew > unitario)
										prezzounitarionew = unitario;
									
									var importo = parseFloat(prezzounitarionew*quantity);
									document.getElementById(\'product_price[\'+id_product+\']\').value=prezzounitarionew.toFixed(2).replace(".",",");					
								}
							';
							
							}
							
						else {
						
						echo '
						if(quantity < sc_qta_1_q && sc_qta_1_q != 0) {
							
							if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".") > 0) {
							} else {
								document.getElementById(\'product_price[\'+id_product+\']\').value=unitario.toFixed(2).replace(".",",");	
								var importo = parseFloat(price*quantity);
							}		
						}
						
						else if((quantity >= sc_qta_1_q && quantity < sc_qta_2_q) || (quantity >= sc_qta_1_q && isNaN(parseFloat(sc_qta_2_q)))) {
							if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".") > 0) {
							} else {
								var importo = parseFloat(sc_qta_1*quantity);
								document.getElementById(\'product_price[\'+id_product+\']\').value=sc_qta_1.toFixed(2).replace(".",",");	
							}
						}
						
						else if((quantity >= sc_qta_2_q && quantity < sc_qta_3_q) || (quantity >= sc_qta_2_q && isNaN(parseFloat(sc_qta_3_q == 0)))) {
							if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".") > 0) {
							} else {
								var importo = parseFloat(sc_qta_2*quantity);	
								document.getElementById(\'product_price[\'+id_product+\']\').value=sc_qta_2.toFixed(2).replace(".",",");	
							}
						}
						
						else if(quantity >= sc_qta_3_q && sc_qta_2_q != 0 && sc_qta_3_q != 0 && sc_qta_1_q != 0) {
							if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".") > 0) {
							} else {
								var importo = parseFloat(sc_qta_3*quantity);	
								document.getElementById(\'product_price[\'+id_product+\']\').value=sc_qta_3.toFixed(2).replace(".",",");
							}
						}';
						
						} echo '
					}
					
					else if (document.getElementById(\'usa_sconti_quantita[\'+id_product+\']\').checked == false) {
						
						var importo = parseFloat(price*quantity);
					
					}
					document.getElementById(\'product_price[\'+id_product+\']\').value.replace(".",",");
					
					totale = 0;
					
					
					
					document.getElementById(\'impImporto[\'+id_product+\']\').value = importo;
					
					var arrayImporti = document.getElementsByClassName("importo");
					for (var i = 0; i < arrayImporti.length; ++i) {
						var item = parseFloat(arrayImporti[i].value);  
						totale += item;
					}
					
					var totaleacquisti = 0;
					
					var arrayAcquisti = document.getElementsByClassName("prezzoacquisto");
					for (var i = 0; i < arrayAcquisti.length; ++i) {
						arrayAcquisti[i].value = arrayAcquisti[i].value.replace(",",".");
						var item = parseFloat(arrayAcquisti[i].value); 					
						totaleacquisti += item;
					}
					
					
					totale = totale+commissioni;
					totale = totale-sconti;
					
					var controllo_vendita = document.getElementById(\'controllo_vendita_\'+id_product+\'\');
					
					
					if(numunitario < price)
						controllo_vendita.innerHTML = "<img class=\'img_control\' src=\'../img/admin/error.png\' style=\'padding:0px; width:15px; float:right\' title=\'ATTENZIONE stai vendendo il prodotto a un prezzo maggiore rispetto al prezzo standard di "+document.getElementById(\'unitario[\'+id_product+\']\').value + "\' alt=\'ATTENZIONE stai vendendo il prodotto a un prezzo maggiore rispetto al prezzo standard di "+document.getElementById(\'unitario[\'+id_product+\']\').value + "\' />";
					else
						controllo_vendita.innerHTML = "";
					
								
					var marginalitatotale = (((totale - totaleacquisti)*100) / totale);
		
					marginalitatotale = marginalitatotale.toFixed(2);
		
					document.getElementById(\'marginalitatotale\').innerHTML=marginalitatotale.replace(".",",") + "%";
					
					var guadagnototale = totale - totaleacquisti;
					
					guadagnototale = guadagnototale.toFixed(2);
					
					document.getElementById(\'guadagnototale\').innerHTML=guadagnototale.replace(".",",");
					
					importo = importo.toFixed(2);
					importo = importo.replace(".",",");
					
					
					document.getElementById(\'valoreImporto[\'+id_product+\']\').innerHTML=importo;
					
					totale = totale+spedizione;			
					var totalep = totale;
					totale = totale.toFixed(2);
				
					document.getElementById(\'totaleprodotti\').value=totale;
					
					document.getElementById(\'spantotaleprodotti\').innerHTML=totale.replace(".",",");
					
					numprice = document.getElementById(\'product_price[\'+id_product+\']\').value;
					
					price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
					
					if(marginalita < 10 && price > 0)
						document.getElementById(\'tr_\'+id_product).style.backgroundColor=\'#ffcccc\';
					else
						document.getElementById(\'tr_\'+id_product).style.backgroundColor=\'#ffffff\';
					
					var marginalita = (((price - acquisto)*100) / price);
					
					marginalita = marginalita.toFixed(2);
				
					document.getElementById(\'spanmarginalita[\'+id_product+\']\').innerHTML=marginalita.replace(".",",")+"%";
					
					$.ajax({
						url:"ajax_products_list2.php?calcola_totale_con_iva=y",
						type: "POST",
						data: { address: "'.$address_delivery.'",
						customer : "'.$id_customer.'",
						totalep : totalep
						 },
						success:function(resp){
							totale_con_iva = resp;
							totale_con_iva = parseFloat(totale_con_iva);
							totale_con_iva = totale_con_iva.toFixed(2);
							document.getElementById(\'spantotaleprodottiiva\').innerHTML=totale_con_iva.replace(".",",");		
						},
						error: function(xhr,stato,errori){
							alert("Errore nella cancellazione:"+xhr.status);
						}
						
						
					});
					
			}
			
			function togliImporto(id_product) {
			
				var daTogliere = parseFloat(document.getElementById(\'impImporto[\'+id_product+\']\').value.replace(",", "."));
				
				
				var totale = parseFloat(document.getElementById(\'totaleprodotti\').value.replace(",", "."));
				
				
					var spedizione = parseFloat(document.getElementById(\'transport\').value.replace(/\s/g, "").replace(",", "."));
					
					var sconti = parseFloat(document.getElementById(\'total_discounts\').value.replace(/\s/g, "").replace(",", "."));
					var commissioni = parseFloat(document.getElementById(\'total_commissions\').value.replace(/\s/g, "").replace(",", "."));
					
					var numacquisto = document.getElementById(\'wholesale_price[\'+id_product+\']\').value;
			
					var quantity = parseFloat(document.getElementById(\'product_quantity[\'+id_product+\']\').value);

					var acquisto = parseFloat(numacquisto.replace(/\s/g, "").replace(",", "."));
					
				
					var totaleacquisti = 0;
					
					var arrayAcquisti = document.getElementsByClassName("prezzoacquisto");
					for (var i = 0; i < arrayAcquisti.length; ++i) {
						arrayAcquisti[i].value = arrayAcquisti[i].value.replace(",",".");
						var item = parseFloat(arrayAcquisti[i].value); 					
						totaleacquisti += item;
					}
					
					var togliAcquisto = (acquisto*quantity);
					
					totaleacquisti = totaleacquisti-togliAcquisto;
					
					totale = totale-daTogliere;
								
					var marginalitatotale = (((totale - totaleacquisti)*100) / totale);
		
					marginalitatotale = marginalitatotale.toFixed(2);
		
					document.getElementById(\'marginalitatotale\').innerHTML=marginalitatotale.replace(".",",") + "%";
					
					var guadagnototale = totale - totaleacquisti;
					
					guadagnototale = guadagnototale.toFixed(2);
					
					
					
					
					document.getElementById(\'guadagnototale\').innerHTML=guadagnototale.replace(".",",");
				
				totale = totale.toFixed(2);
				
				document.getElementById(\'totaleprodotti\').value=totale;
					
				document.getElementById(\'spantotaleprodotti\').innerHTML=totale.replace(".",",");
				
				
				
			}
			
			function deleteRow(rowid)  
			{   
				var row = document.getElementById(rowid);
				row.parentNode.removeChild(row);
			}
			
			function delProduct30(id)
				{
					deleteRow(\'tr_\'+id);
					$("#id_lang").after("<input name=\'product_delete["+id+"]\' id=\'product_delete["+id+"]\' type=\'hidden\' value=\'"+id+"\' />");
					$.ajax({
					  url:"ajax_products_list2.php?cancellaprodotto_daordine=y",
					  type: "POST",
					  data: { id_product: id,
					  id_order: '.$_GET['id_order'].'
					  },
					  success:function(){
						 	var excludeIds = document.getElementById("excludeIds").value;
							var da_cancellare = new RegExp(id, "g");
							
							excludeIds = excludeIds.replace(da_cancellare,""); 
							
							document.getElementById("excludeIds").value = excludeIds;
							$("#product_autocomplete_input").val("");
							$("#product_autocomplete_input").setOptions({
								extraParams: {excludeIds : document.getElementById("excludeIds").value}
							});	
							
					  },
					  error: function(xhr,stato,errori){
						 alert("Errore nella cancellazione:"+xhr.status);
					  }
					});
					
					
				}
				
				function delProduct30MSG(id)
				{
					document.getElementById(\'prodotti_cancellati\').value = document.getElementById(\'prodotti_cancellati\').value+","+id;
				
				}
			
			</script>

			';

			$cig = Db::getInstance()->getValue("SELECT cig FROM orders WHERE id_order = ".$_GET['id_order']);
			$cup = Db::getInstance()->getValue("SELECT cup FROM orders WHERE id_order = ".$_GET['id_order']);
			$ipa = Db::getInstance()->getValue("SELECT ipa FROM orders WHERE id_order = ".$_GET['id_order']);
			$data_ordine_mepa = Db::getInstance()->getValue("SELECT data_ordine_mepa FROM orders WHERE id_order = ".$_GET['id_order']);
						
			$tokenProducts = $tokenCarts = Tools::getAdminToken('AdminCatalogExFeatures'.(int)(Tab::getIdFromClassName('AdminCatalogExFeatures')).(int)($cookie->id_employee));
			$excludeIds = "";
			$array_prodotti_da_escludere = Db::getInstance()->executeS("select product_id FROM order_detail WHERE id_order=".$_GET['id_order']."");
			foreach($array_prodotti_da_escludere as $escl) {
			
				$excludeIds .= $escl['product_id'].",";
			
			}
			echo '<input id="excludeIds" name="excludeIds" type="hidden" value="'.$excludeIds.'" />';			
			echo '<input id="prodotti_cancellati" name="prodotti_cancellati" type="hidden" value="0" />';			
			echo ' <input name="total_products" type="hidden"  value="'.$total_products.'" />
							
									<input name="total" type="hidden"  id="total_paid_real" value="'.$total_paid_real.'" />
									<input name="id_order" type="hidden" value="'.$_GET['id_order'].'" />';
						
			echo '
				
						
			<script type="text/javascript">
			var prodotti_nel_carrello = ['.$excludeIds.'0];
 
				function addProduct_TR(event, data, formatted)
				{
					if(data[6] == "*BUNDLE*") {
						var prodotti_bundle = data[7].split(";");
						for(i=0;i<((prodotti_bundle.length))-1;i++)
						{
							$.ajax({
							type: "GET",
							data: "id_cart=0&id_bundle="+data[1]+"&product_in_bundle="+prodotti_bundle[i],
							async: false,
							url: "ajax_products_list2.php",
							success: function(resp)
								{
									addProduct_TR("",resp.split("|"),"");
								},
								error: function(XMLHttpRequest, textStatus, errorThrown)
								{
									tooltip_content = "";
									alert("ERROR" + textStatus + " " + XMLHttpRequest.responseText);					
								}
														
							});
						}
						return false;
					
					}
					
					document.getElementById("product_autocomplete_input").value = ""; 
					var productId = data[1];
					
					$("#product_delete["+productId+"]").remove();
					
					var excludeIds = document.getElementById("excludeIds").value;
					var arrayescl = excludeIds.split(",");
					
					for(i=0;i<((arrayescl.length));i++)
					{
						if(arrayescl[i].trim() != "" && arrayescl[i].trim() == productId.trim())
						{
							alert("Prodotto già nel carrello");
							return false;
						}
					}
					excludeIds = excludeIds+","+productId+",";
					document.getElementById("excludeIds").value = excludeIds;
					$("#product_autocomplete_input").val("");
							
					$("#product_autocomplete_input").setOptions({
						extraParams: {excludeIds : document.getElementById("excludeIds").value}
					});	
				
					$("#product_autocomplete_input").focus();
					
					
					var productPrice = data[2];
					var productScQta = data[3];
					var productRef = data[4];
					var productName = data[5];
					var productQuantity = data[16];
					var tdImporto = data[17];
					var przUnitario = data[18];
					var deleteProd = data[19];
					var marg = data[20];
					var scontoExtra = data[21];
					var sc_qta_1 = data[6];
					var sc_qta_2 = data[7];
					var sc_qta_3 = data[8];
					var sc_riv_1 = data[9];
					var sc_riv_2 = data[10];
					var sc_riv_3 = data[26];
					var sc_qta_1_q = data[11];	
					var sc_qta_2_q = data[12];	
					var sc_qta_3_q = data[13];	
					var sc_riv_1_q = data[14];	
					var sc_riv_2_q = data[15];	
					var sc_riv_3_q = data[27];
					var acquisto = data[30];	
					var src_img = data[35];
					prodotti_nel_carrello.push(parseInt(productId));
					var varspec = data[24];
					var textVarSpec = "";
							
					
					$("#product_autocomplete_input").setOptions({
						extraParams: {excludeIds : document.getElementById("excludeIds").value}
					});	
					
					$("#product_autocomplete_input_interno").setOptions({
						extraParams: {excludeIds : document.getElementById("excludeIds").value}
					});
							
					if(varspec == 0) {
					}
					else {
						
						textVarSpec = "style=\'border:1px solid red\'"
					}
					
					$("<tr " + textVarSpec + " id=\'tr_"+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"\'><td><img src=\'"+src_img+"\' style=\'width:30px; height:30px;\' alt=\'\' /></td><td><a href=\'index.php?tab=AdminCatalogExFeatures&id_product="+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"&updateproduct&token='.$tokenProducts.'\' target=\'_blank\'>" + productRef + "</a></td><td><input name=\'nuovo_name["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' size=\'31\' type=\'text\' value=\'"+ productName +"\' /></td><td> " + productQuantity + "</td><td>" + productPrice + "<div style=\'float:right\' id=\'controllo_vendita_"+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"\'></div></td><td>" + scontoExtra + "</td><td>" + acquisto + "</td>" + marg + "<td style=\'text-align:center\'> " + productScQta + "</td>" + tdImporto + "<td class=\'pointer dragHandle center\' style=\'background:url(../img/admin/up-and-down.gif) no-repeat center;\'><input type=\'hidden\' name=\'sort_order["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' /></td><td style=\'text-align:center\'>" + deleteProd + "<input name=\'nuovo_prodotto["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' type=\'hidden\' value=\'"+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"\' />" + sc_qta_1 + " " + sc_qta_2 + " " + sc_qta_3 + " " + sc_riv_1 + " " + sc_riv_2 + " " + sc_riv_3 + " " + sc_qta_1_q + " " + sc_qta_2_q + " " + sc_qta_3_q + " " + sc_riv_1_q + " " + sc_riv_2_q + " " + sc_riv_3_q + " " + przUnitario + "</td></tr>").appendTo("#tableProductsBody");
					
					
					$("#tableProducts").tableDnD();
					
					productId = productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "");
					calcolaImporto(productId);
					
					var excludeIds = document.getElementById("excludeIds").value;
					excludeIds = excludeIds+productId+",";
					
					document.getElementById("excludeIds").value = excludeIds;
					$("#product_autocomplete_input").val("");
					
					
					$("#product_autocomplete_input").setOptions({
						extraParams: {excludeIds : document.getElementById("excludeIds").value}
					});	
					
					$("#product_autocomplete_input_interno").setOptions({
						extraParams: {excludeIds : document.getElementById("excludeIds").value}
					});
					
					$("body").trigger("click");			
					$("#product_autocomplete_input").trigger("click");
					$("#product_autocomplete_input").trigger("click");
					
					$("body").trigger("click");			
					$("#product_autocomplete_input_interno").trigger("click");
					$("#product_autocomplete_input_interno").trigger("click");
													
					function getExcludeIds() {
						var ids = "";
						ids += $(\'#excludeIds\').val().replace(/\\-/g,\',\').replace(/\\,$/,\'\');
						return ids;
					}
					
				}

				
			</script>
			<table>';
				echo "<tr>";
			echo '<td style="width:70px">CIG</td><td><input type="text" id="cig" style="width:150px; margin-right:20px" name="cig" value="'.$cig.'" /></td>';
			echo '<td style="width:70px">CUP</td><td><input type="text" id="cup" style="width:130px; margin-right:20px" name="cup" value="'.$cup.'" /></td>';
			echo '<td style="width:70px">IPA <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Codice univoco ufficio per fatturazione elettronica" title="Codice univoco ufficio per fatturazione elettronica" /></td><td><input type="text" id="ipa" style="width:130px; margin-right:20px" name="ipa" value="'.$ipa.'" /></td>';
			
			echo '<td style="width:70px">Data MEPA </td><td><input type="text" id="data_ordine_mepa" style="width:130px; margin-right:20px" name="data_ordine_mepa" value="'.($data_ordine_mepa == '' || $data_ordine_mepa == '0000-00-00' ? '' : date("d-m-Y", strtotime($data_ordine_mepa))).'" /></td>';
			echo '</tr></table>';
			
			echo '
			
			<strong>Cerca:</strong>
			<input type="checkbox" checked="checked" id="prodotti_online" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Online &nbsp;&nbsp;
			<input type="checkbox" id="prodotti_offline" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Offline &nbsp;
			<input type="checkbox" id="prodotti_old" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Old &nbsp;&nbsp;&nbsp;
			<input type="checkbox" id="prodotti_disponibili" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Solo disp. &nbsp;&nbsp;&nbsp;
			
			<script type="text/javascript" src="../js/select2.js"></script>
					<script type="text/javascript">
					$(document).ready(function() { $("#auto_marca").select2(); $("#auto_serie").select2(); $("#auto_fornitore").select2();  $("#auto_categoria").select2(); });
					</script>
					
			<select id="auto_marca" name="auto_marca" onchange="repopulateSeries(this.value); clearAutoComplete();" style="width:130px">
			<option value="0">Marca...</option>
			';
			$marche = Db::getInstance()->executeS('SELECT * FROM manufacturer ORDER BY name ASC');
			foreach($marche as $marca)
				echo '<option value="'.$marca['id_manufacturer'].'">'.$marca['name'].'</option>';
			echo '
			</select>
			
					
			<select id="auto_serie" name="auto_serie" onchange="clearAutoComplete();" style="width:130px">
			<option value="0">Serie...</option>
			<option value="0">Scegli prima un costruttore</option>
			';
			echo '
			</select>
			
			<select id="auto_categoria" name="auto_categoria" onchange="clearAutoComplete();" style="width:130px">
			<option value="0">Categoria...</option>
			';
			$categorie = Db::getInstance()->executeS('SELECT * FROM category c JOIN category_lang cl ON c.id_category = cl.id_category WHERE cl.id_lang = 5 AND c.id_parent = 1 ORDER BY cl.name ASC');
			foreach($categorie as $categoria)
				echo '<option value="'.$categoria['id_category'].'">'.$categoria['name'].'</option>';
			echo '
			</select>
			
			<select id="auto_fornitore" name="auto_fornitore" onchange="clearAutoComplete(); " style="width:130px">
			<option value="0">Fornitore...</option>
			';
			
			$fornitori = Db::getInstance()->executeS('SELECT * FROM supplier WHERE id_supplier = 8 OR id_supplier = 11 OR id_supplier = 42 ORDER BY name ASC');
			foreach($fornitori as $fornitore)
				echo '<option value="'.$fornitore['id_supplier'].'">'.$fornitore['name'].'</option>';
			echo '
			</select>
			
			<input id="reset" type="button" value="Reset" class="button" onclick="document.getElementById(\'prodotti_online\').checked = true; document.getElementById(\'prodotti_offline\').checked = true; document.getElementById(\'prodotti_old\').checked = true;  document.getElementById(\'prodotti_disponibili\').checked = false; document.getElementById(\'auto_categoria\').options[0].selected = true; $(\'#auto_categoria\').select2();  document.getElementById(\'auto_marca\').options[0].selected = true; $(\'#auto_marca\').select2();  
			document.getElementById(\'auto_serie\').options[0].selected = true; $(\'#auto_serie\').select2();  
			document.getElementById(\'auto_fornitore\').options[0].selected = true; $(\'#auto_fornitore\').select2(); $(\'#prodlist\').hide(); $(\'div.autocomplete_list\').hide();" />
			
			<br />
			<input id="veditutti" type="button" value="Cerca" class="button" /> <input size="123" type="text" value="" id="product_autocomplete_input" /><br /><br />
			
			<script type="text/javascript">
					var formProduct = "";
					var products = new Array();
				</script>
				<script type="text/javascript">
			
			/*
				//setup before functions
				var typingTimer;                //timer identifier
				var doneTypingInterval = 3000;  //time in ms, 3 second for example
				var $ainput = $("#product_autocomplete_input");

				//on keyup, start the countdown
				$ainput.on("keyup", function () {
				  clearTimeout(typingTimer);
				  typingTimer = setTimeout(doneTyping, doneTypingInterval);
				});

				//on keydown, clear the countdown 
				$ainput.on("keydown", function () {
				  clearTimeout(typingTimer);
				});

				//user is "finished typing," do something
				function doneTyping () {
					lastKeyPressCode = event.keyCode;
					if(lastKeyPressCode != 37 && lastKeyPressCode != 38 && lastKeyPressCode != 39 && lastKeyPressCode != 40)
						$ainput.trigger("click");
				}
			*/
			
					var formProduct = "";
					var products = new Array();
				</script>
				
				<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete2.js"></script>
				
				
				<script type="text/javascript">
					$("body").on("click", function (event) {
						 if(event.target.tagName.toUpperCase() == "BODY"  || event.target.id == "auto_marca"   || event.target.id == "auto_serie" || event.target.id == "auto_categoria" || event.target.id == "auto_fornitore" || event.target.id == "veditutti" || event.target.id == "prodotti_online"  || event.target.id == "prodotti_offline"   || event.target.id == "prodotti_old" || event.target.id == "prodotti_disponibili" || event.target.id == "product_autocomplete_input")
						 {
							event.stopPropagation();
							
						 }
						 else
						 {
							$(\'#prodlist\').hide();
							$(\'div.autocomplete_list\').hide();
						 }
					});	
					
						urlToCall = null;
					
					function getExcludeIds() {
						var ids = "";
						ids += $(\'#excludeIds\').val().replace(/\\-/g,\',\').replace(/\\,$/,\'\');
						return ids;
					}
					
					function getOnline()
					{
						if(document.getElementById("prodotti_online").checked == true)
							return 1;
						else
							return 0;
					}
					
					function getOffline()
					{
						if(document.getElementById("prodotti_offline").checked == true)
							return 1;
						else
							return 0;
					}

					function getOld()
					{
						if(document.getElementById("prodotti_old").checked == true)
							return 2;
						else
							return 0;
					}
					
					function getDisponibili()
					{
						if(document.getElementById("prodotti_disponibili").checked == true)
							return 1;
						else
							return 0;
					}
					
					function getCopiaDa()
					{
							return 0;
					}
										
					function getSerie()
					{
						if(document.getElementById("auto_serie").value > 0)
							return document.getElementById("auto_serie").value;
						else
							return 0;
					}
					
					function getMarca()
					{
						if(document.getElementById("auto_marca").value > 0)
							return document.getElementById("auto_marca").value;
						else
							return 0;
					}
					
					function getFornitore()
					{
						if(document.getElementById("auto_fornitore").value > 0)
							return document.getElementById("auto_fornitore").value;
						else
							return 0;
					}
					
					function getCategoria()
					{
						if(document.getElementById("auto_categoria").value > 0)
							return document.getElementById("auto_categoria").value;
						else
							return 0;
					}
					
					function repopulateSeries(id_manufacturer)
					{
						$("#auto_serie").empty();
						$.ajax({
						  url:"ajax.php?repopulate_series=y",
						  type: "POST",
						  data: { id_manufacturer: id_manufacturer
						  },
						  success:function(resp){  
							var newOptions = $.parseJSON(resp);
							
							 $("#auto_serie").append($("<option></option>")
								 .attr("value", "0").text("Serie..."));
							
							$.each(newOptions, function(key,value) {
							  $("#auto_serie").append($("<option></option>")
								 .attr("value", value).text(key));
							});
							
							$("#auto_serie").select2({
								placeholder: "Serie..."
							});
						  },
						  error: function(xhr,stato,errori){
							 alert("Errore: impossibile trovare serie ");
						  }
						});
							
						
					}	
					
					function clearAutoComplete()
					{
						$("#veditutti").trigger("click");
						
					}	
					
					/* function autocomplete */
					$(function() {
						$(\'#product_autocomplete_input\')
							.autocomplete(\'ajax_products_list2.php?excludeIds=\'+document.getElementById("excludeIds").value+\'&id_cart='.$id_cart.'\', {
							
								minChars: 0,
								autoFill: false,
								max:5000,
								matchContains: true,
								matchSubset: 0,
								mustMatch:true,
								scroll:true,
								useCache:false,
								cacheLength:1,
								formatItem: function(item) {
							
											var color = "";
											if(item[24] == 1) {
												var color = "; color:red";
											}
											if(item[28] == 0) {
												return \'</table><br /><div style="position:absolute; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:180px;float:left;margin-left:10px;">Codice</div></th><th style="width:630px;"><div style="width:350px; float:left;">Desc. prodotto<br /><br /></div></th><th style="width:60px; text-align:right"><div style="width:55px;float:left;">Prezzo</div></th><th style="width:45px; text-align:right"><div style="width:40px;float:left; text-align:right">Mag.</div></th><th style="width:45px; text-align:right"><div style="width:40px;float:left; text-align:right">INT</div></th><th style="width:45px; text-align:right"><div style="width:40px;float:left; text-align:right">ALN</div></th><th style="width:40px; text-align:right"><div style="width:40px;float:left; text-align:right">ITA</div></th><th style="width:40px; text-align:right"><div style="width:40px;float:left; text-align:right">Imp</div></th><th style="width:40px; text-align:right"><div style="width:40px;float:left; text-align:center">Tot</div></th></tr></div><table>\'
												
												;
											}
											else {
												return \'<tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:450px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:40px; text-align:right">\'+item[25]+\'</td><td style="width:40px; text-align:right">\'+item[33]+\'</td><td style="width:40px; text-align:right">\'+item[34]+\'</td><td style="width:40px; text-align:right">\'+item[38]+\'</td><td style="width:40px; text-align:right">\'+item[37]+\'</td><td style="width:40px; text-align:right">\'+item[22]+\'</td></tr>\'
												
												;
												}
											}
								
							}).result(addProduct_TR);
							
							$("#product_autocomplete_input").setOptions({
								extraParams: {excludeIds : getExcludeIds()}
							});		
						
					});
				
					$("#product_autocomplete_input").css(\'width\',\'835px\');
					$("#product_autocomplete_input").keypress(function(event){
								
								  var keycode = (event.keyCode ? event.keyCode : event.which);
								  if (keycode == "13") {
									  $("#veditutti").trigger("click"); 
									event.preventDefault();
									event.stopPropagation();    
								  }
								});
								
				
					
				</script>
				<script type="text/javascript">
				
				$("#veditutti").click ( function (zEvent) {
					
					/*	$("body").trigger("click");
						$("body").trigger("click");
						
						
						
						$("#product_autocomplete_input").focus();
					//	$("#product_autocomplete_input").val("");
					//	$("#product_autocomplete_input").val(" ");
					*/
						var press = jQuery.Event("keypress");
						press.ctrlKey = true;
						if(document.getElementById(\'product_autocomplete_input\').value == "")
						{
							$("#product_autocomplete_input").val(" ");
						}
						$("#product_autocomplete_input").trigger(press);
					
						$("#product_autocomplete_input").trigger("click");
						$("#product_autocomplete_input").trigger("click");
					//	$("#product_autocomplete_input").val("");
						
				} );


				</script>
				KIT <input size="123" type="text" value="" id="product_autocomplete_input_kit" /><br /><br /> 
				<script type="text/javascript">
					var formProduct = "";
					var products = new Array();
				</script>
				<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete3.js"></script>
				<script type="text/javascript">
					urlToCall = null;
					
					/* function autocomplete */
					$(function() {
						$(\'#product_autocomplete_input_kit\')
							.autocomplete(\'ajax_products_list2.php?id_bundle=0&bundles=y&id_cart='.$id_cart.'\', {
								minChars: 0,
								autoFill: false,
								max:50,
								matchContains: true,
								matchSubset: 0,
								mustMatch:true,
								scroll:true,
								cacheLength:1,
								formatItem: function(item) {
							
											var color = "";
											if(item[24] == 1) {
												var color = "; color:red";
											}
											if(item[28] == 0) {
												return \'</table><div style="position:absolute; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:150px;float:left;margin-left:10px;">Codice</div></th><th style="width:570px;"><div style="width:550px; float:left;">Desc. kit<br /><br /></div></th><th style="width:60px; text-align:right"><div style="width:60px;float:left;">Prezzo</div></th><th style="width:50px; text-align:right"><div style="width:60px;float:left;">Qt. mag.</div></th><th style="width:50px; text-align:right"><div style="width:75px;float:left;">Qt. tot</div></th></tr></div><table><tr><td style="width:145px; text-align:left"><br /><br /><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:555px\'+color+\'"><br /><br />\'+item[5]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[23]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[25]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[22]+\'</td></tr></table>\'
												
												;
											}
											else {
												return \'<tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:550px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
												
												;
												}
											}
								
							}).result(addProduct_TR);
						
						
					});
				
					$("#product_autocomplete_input_kit").css(\'width\',\'905px\');
					$("#product_autocomplete_input_kit").keypress(function(event){
								
					  var keycode = (event.keyCode ? event.keyCode : event.which);
					  if (keycode == "13") {
						$("#product_autocomplete_input_kit").trigger("click");
						event.preventDefault();
						event.stopPropagation();    
					  }
					});
							

				$("#order_edit").on("keyup keypress", function(e) {
				  var keyCode = e.keyCode || e.which;
				  if (keyCode === 13) { 
					e.preventDefault();
					return false;
				  }
				});
					
				</script>

			<table width="100%" class="table"  id="tableProducts">
			<thead>
			  <tr>
				<th></th>
				<th style="width:130px">Codice + Dati</th>
				<th style="width:150px">Nome prodotto</th>
				<th style="width:45px">Qta</th>
				<th style="width:115px">Unitario</th>
				<th style="width:45px">Sconto extra</th>
				<th style="width:75px">Acquisto</th>
				<th style="width:55px">Marg.</th>
				<th style="width:15px">'.($customer->id_default_group == 3 ? 'Sc.rv?' : 'Sc.qt?').'</th>
				<th style="width:85px">Importo</th>
				<th style="width:10px"><img src="../img/admin/up-and-down.gif" alt="Ordina" title="Ordina" /></th>
				<th style="width:10px"><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></th>
				
			  </tr></thead><tbody  id="tableProductsBody">';
			$tax_rate=Db::getInstance()->getValue("SELECT tax_rate FROM order_tax WHERE id_order = ".$_GET['id_order']."");
			$query="select o.*,p.quantity as stock,p.listino as listino,o.wholesale_price as acquisto,p.wholesale_price as acquisto_o, p.sconto_acquisto_1 as sc_acq_1, p.sconto_acquisto_2 as sc_acq_2, p.sconto_acquisto_3 as sc_acq_3, p.quantity as qt_tot, p.stock_quantity as magazzino, p.supplier_quantity as allnet, p.esprinet_quantity esprinet,p.attiva_quantity attiva, p.itancia_quantity itancia, p.ordinato_quantity as qt_ordinato, p.reference as product_reference, id_image from ". _DB_PREFIX_."order_detail o left join ". _DB_PREFIX_."product p  on  o.product_id=p.id_product left join (select * from image where cover = 1) image on image.id_product = p.id_product ";
			$query.=" where id_order=".$_GET['id_order'];
			$query.=" order by id_order_detail asc";
			 $res1=Db::getInstance()->executeS($query);
			//if (mysql_num_rows($res1)>0) {

				foreach($res1 as $products) {
					
					
						$sc_qta_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_1' AND id_product = '".$products['product_id']."'");
						$sc_qta_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_2' AND id_product = '".$products['product_id']."'");
						$sc_qta_3_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_3' AND id_product = '".$products['product_id']."'");	
						$sc_riv_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_1' AND id_product = '".$products['product_id']."'");	
						$sc_riv_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_2' AND id_product = '".$products['product_id']."'");
						$sc_riv_3_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_3' AND id_product = '".$products['product_id']."'");
						
						$sc_qta_1 = $sc_qta_1_r['price'] - ($sc_qta_1_r['price'] * $sc_qta_1_r['reduction']);
						$sc_qta_2 = $sc_qta_2_r['price'] - ($sc_qta_2_r['price'] * $sc_qta_2_r['reduction']);
						$sc_qta_3 = $sc_qta_3_r['price'] - ($sc_qta_3_r['price'] * $sc_qta_3_r['reduction']);
						$sc_riv_1 = $sc_riv_1_r['price'] - ($sc_riv_1_r['price'] * $sc_riv_1_r['reduction']);
						$sc_riv_2 = $sc_riv_2_r['price'] - ($sc_riv_2_r['price'] * $sc_riv_2_r['reduction']);
						$sc_riv_3 = $sc_riv_2_r['price'] - ($sc_riv_3_r['price'] * $sc_riv_3_r['reduction']);
						
						$unitario = Db::getInstance()->getValue("SELECT price FROM product WHERE id_product = '".$products['product_id']."'");
						
						$sc_qta_1_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_1' AND id_product = '".$products['product_id']."'");
						$sc_qta_2_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_2' AND id_product = '".$products['product_id']."'");
						$sc_qta_3_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_3' AND id_product = '".$products['product_id']."'");
						
						$sc_riv_1_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_1' AND id_product = '".$products['product_id']."'");
						$sc_riv_2_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_2' AND id_product = '".$products['product_id']."'");
						$sc_riv_3_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_3' AND id_product = '".$products['product_id']."'");
						
						if($customer->id_default_group == 3 || $customer->id_default_group == 22) {
							$ctrl_sc_qt = Db::getInstance()->getValue("SELECT count(id_product) FROM specific_price WHERE id_product = ".$products['product_id']." AND (specific_price_name = 'sc_riv_1' OR specific_price_name = 'sc_riv_2')");
							
							if($ctrl_sc_qt > 0) {
								$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['product_id'].']" name="usa_sconti_quantita['.$products['product_id'].']" type="checkbox" '.($products['product_price'] == Product::TrovaMigliorPrezzo($products['product_id'], $customer->id_default_group, $products['product_quantity']) || $products['product_price']-($products['product_price']*$products['reduction_percent']/100) == Product::TrovaMigliorPrezzo($products['product_id'], $customer->id_default_group, $products['product_quantity']) ? 'checked="checked"' : "").' onclick="ristabilisciPrezzo('.$products['product_id'].'); calcolaImporto('.$products['product_id'].'); " />';
							}
							else {
								$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['product_id'].']" name="usa_sconti_quantita['.$products['product_id'].']" type="hidden" value="0" />';
							}
						}
						
						else if($customer->id_default_group == 12) {
							$ctrl_sc_qt = Db::getInstance()->getValue("SELECT count(id_product) FROM specific_price WHERE id_product = ".$products['product_id']." AND (specific_price_name = 'sc_riv_1' OR specific_price_name = 'sc_riv_2' OR specific_price_name = 'sc_riv_3')");
							
							if($ctrl_sc_qt > 0) {
								$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['product_id'].']" name="usa_sconti_quantita['.$products['product_id'].']" type="checkbox" '.($products['product_price'] == Product::TrovaMigliorPrezzo($products['product_id'], $customer->id_default_group, $products['product_quantity']) || $products['product_price']-($products['product_price']*$products['reduction_percent']/100) == Product::TrovaMigliorPrezzo($products['product_id'], $customer->id_default_group, $products['product_quantity']) ? 'checked="checked"' : "").' onclick="ristabilisciPrezzo('.$products['product_id'].'); calcolaImporto('.$products['product_id'].'); " />';
							}
							else {
								$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['product_id'].']" name="usa_sconti_quantita['.$products['product_id'].']" type="hidden" value="0" />';
							}
						}
						
						else {
							$ctrl_sc_qt = Db::getInstance()->getValue("SELECT count(id_product) FROM specific_price WHERE id_product = ".$products['product_id']." AND (specific_price_name = 'sc_qta_1' OR specific_price_name = 'sc_qta_2' OR specific_price_name = 'sc_qta_3')");
							
							if($ctrl_sc_qt > 0) {
								$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['product_id'].']" name="usa_sconti_quantita['.$products['product_id'].']" type="checkbox" '.($products['product_price'] == Product::TrovaMigliorPrezzo($products['product_id'], $customer->id_default_group, $products['product_quantity']) ? 'checked="checked"' : "").' onclick="ristabilisciPrezzo('.$products['product_id'].'); calcolaImporto('.$products['product_id'].'); " />';
							}
							else {
								$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['product_id'].']" name="usa_sconti_quantita['.$products['product_id'].']" type="hidden" value="0"  />';
							}
						}
					echo '<tr id="tr_'.$products['product_id'].'">';
					 
					echo '<td><img src="http://www.ezdirect.it/img/p/'.$products['product_id'].'-'.$products['id_image'].'-small.jpg" alt="" style="width:30px;height:30px" /><td><span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($products['product_id']).'"> <a href="index.php?tab=AdminCatalog&id_product='.$products['product_id'].'&updateproduct&token='.$tokenCatalog.'">'.$products['product_reference'].'</a></span></td>';
					 
					  echo '  <td><input size="37" name="product_name['.$products['id_order_detail'].']" type="text" value="'.$products['product_name'].'" /></td>';
					   echo '  <td><input size="3" style="text-align:right" id="product_quantity['.$products['product_id'].']" onkeyup="if (isArrowKey(event)) return; calcolaPrezzoScontoExtra('.$products['product_id'].', \'sconto\');  calcolaImporto('.$products['product_id'].');" name="product_quantity['.$products['id_order_detail'].']" type="text" value="'.$products['product_quantity'].'" /></td>';
					   
					  echo '  <td><input size="7" style="text-align:right" name="product_price['.$products['id_order_detail'].']" id="product_price['.$products['product_id'].']"  onkeyup="if(stopKeyUp == false) { calcolaPrezzoScontoExtra('.$products['product_id'].', \'inverso\'); calcolaImporto('.$products['product_id'].'); }" name="product_price['.$products['product_id'].']" id="product_price['.trim($products['product_id']).']" type="text" value="'.number_format(($products['reduction_amount'] > 0 ? $products['product_price'] - $products['reduction_amount'] : $products['product_price']-($products['product_price']*$products['reduction_percent']/100)), 2, ',', '').'" /> <div style="float:right" id="controllo_vendita_'.$products['product_id'].'">'.( (str_replace(",",".",($products['reduction_amount'] > 0 ? $products['product_price'] - $products['reduction_amount'] : $products['product_price']-($products['product_price']*$products['reduction_percent']/100))) > $unitario) && $unitario > 0 ? '<img class="img_control" src="../img/admin/error.png" style="width:15px; padding:0px; float:right" alt="ATTENZIONE stai vendendo il prodotto a un prezzo maggiore rispetto al prezzo standard di '.number_format($unitario,2,",","").'" title="ATTENZIONE stai vendendo il prodotto a un prezzo maggiore rispetto al prezzo standard di '.number_format($unitario,2,",","").'" />'  : '').'</div></td>';
					  
					  
					  echo '  <td><input size="2"  style="text-align:right'.($unitario == 0 ? ';display:none' : '').'" name="product_reduction['.$products['id_order_detail'].']" id="sconto_extra['.$products['product_id'].']" onkeyup="if (isArrowKey(event)) return; calcolaPrezzoScontoExtra('.$products['product_id'].', \'sconto\'); calcolaImporto('.$products['product_id'].');"   type="text" value="'.($products['product_price'] == Product::TrovaMigliorPrezzo($products['product_id'], $customer->id_default_group, $products['product_quantity']) || $products['product_price']-($products['product_price']*$products['reduction_percent']/100) == Product::TrovaMigliorPrezzo($products['product_id'], $customer->id_default_group, $products['product_quantity']) ? "0,00" : number_format((($unitario-($products['reduction_amount'] > 0 ?  $products['product_price'] - $products['reduction_amount'] : $products['product_price']))*100/$unitario), 2, ',', '')).'" /></td>';
					  
					  	$wholesale_price = ($products['acquisto'] > 0 ? $products['acquisto'] : ( $products['acquisto_o'] > 0 ? $products['acquisto_o'] : ($products['listino']*(100-$products['sc_acq_1'])/100)*((100-$products['sc_acq_2'])/100)*((100-$products['sc_acq_3'])/100)));
						
						if($products['no_acq'] == 1)
							$wholesale_price = 0;
						
					   echo '<td>'.($cookie->profile == 7 ? '' : '
					   <input type="text" size="7" class="wholesale_price_class" style="text-align:right" onkeyup="javascript:document.getElementById(\'usa_sconti_quantita['.$products['product_id'].']\').checked = false; calcolaPrezzoScontoExtra('.$products['product_id'].', \'inverso\'); calcolaImporto('.$products['product_id'].');"  name="wholesale_price['.$products['product_id'].']" id="wholesale_price['.$products['product_id'].']" value="'.number_format(round($wholesale_price,2), 2, ',', '').'" />').'
					  
					   </td>';
					  
					  
					  echo '  <td style="text-align:right">'.($cookie->profile == 7 ? '' : '
						<input type="hidden" name="sconto_acquisto_1['.$products['product_id'].']" id="sconto_acquisto_1['.$products['product_id'].']" value="'.$products['sc_acq_1'].'" />
						<input type="hidden" name="sconto_acquisto_2['.$products['product_id'].']" id="sconto_acquisto_2['.$products['product_id'].']" value="'.$products['sc_acq_2'].'" />
						<input type="hidden" name="sconto_acquisto_3['.$products['product_id'].']" id="sconto_acquisto_3['.$products['product_id'].']" value="'.$products['sc_acq_3'].'" />
						');
					echo '
						
						'.($cookie->profile == 7 ? '' : '<input type="hidden" class="prezzoacquisto" name="totaleacquisto['.$products['product_id'].']" id="totaleacquisto['.$products['product_id'].']" value="'.number_format(round($wholesale_price*$products['product_quantity'],2), 2, ',', '').'" />
						');
						
						$totaleacquisti += $wholesale_price*$products['product_quantity'];
						
						$prezzo_partenza = $products['product_price']-($products['product_price']*$products['reduction_percent']/100);
						
						$marginalita = ((($prezzo_partenza - $wholesale_price)*100)/$prezzo_partenza);
						
						echo ''.($cookie->profile == 7 ? '' : '<span id="spanmarginalita['.$products['product_id'].']">'.number_format($marginalita, 2, ',', '').'%</span>').'</td>';
					  echo '  <td style="text-align:center">'.$usa_sconti_quantita.' <input id="impImporto['.$products['product_id'].']" class="importo" type="hidden" value="'.(($products['reduction_amount'] > 0 ? $products['product_price'] - $products['reduction_amount'] : $products['product_price']-($products['product_price']*$products['reduction_percent'])/100))*$products['product_quantity'].'"  /></td>';
					 
					  echo '  <td  style="text-align:right" id="valoreImporto['.$products['product_id'].']"><span id="product_price_totale['.$products['id_order_detail'].']">'.number_format((($products['reduction_amount'] > 0 ? $products['product_price'] - $products['reduction_amount'] : $products['product_price']-($products['product_price']*$products['reduction_percent'])/100))*$products['product_quantity'],2, ',', '').'</span></td>';  
					   echo '<td class="pointer dragHandle center" style="background:url(../img/admin/up-and-down.gif) no-repeat center;">
					  <input type="hidden" name="sort_order['.$products['id_product'].']" /></td>';
					  echo '  <td style="text-align:center"> <a style="cursor:pointer" onclick="togliImporto('.$products['product_id'].'); delProduct30('.$products['product_id'].'); delProduct30MSG('.$products['product_id'].');"><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a>';
					  
					 
					  echo '<input type="hidden" id="unitario['.$products['product_id'].']" name="unitario['.$products['product_id'].']" value="'.$unitario.'" />';
						echo '<input type="hidden" id="sc_qta_1['.$products['product_id'].']" value="'.$sc_qta_1.'" />';
						echo '<input type="hidden" id="sc_qta_2['.$products['product_id'].']" value="'.$sc_qta_2.'" />';
						echo '<input type="hidden" id="sc_qta_3['.$products['product_id'].']" value="'.$sc_qta_3.'" />';
						echo '<input type="hidden" id="sc_riv_1['.$products['product_id'].']" value="'.$sc_riv_1.'" />';
						echo '<input type="hidden" id="sc_riv_2['.$products['product_id'].']" value="'.$sc_riv_2.'" />';
						echo '<input type="hidden" id="sc_riv_3['.$products['product_id'].']" value="'.$sc_riv_3.'" />';
						
						echo '<input type="hidden" id="sc_qta_1_q['.$products['product_id'].']" value="'.$sc_qta_1_q.'" />';
						echo '<input type="hidden" id="sc_qta_2_q['.$products['product_id'].']" value="'.$sc_qta_2_q.'" />';
						echo '<input type="hidden" id="sc_qta_3_q['.$products['product_id'].']" value="'.$sc_qta_3_q.'" />';
						echo '<input type="hidden" id="sc_riv_1_q['.$products['product_id'].']" value="'.$sc_riv_1_q.'" />';
						echo '<input type="hidden" id="sc_riv_2_q['.$products['product_id'].']" value="'.$sc_riv_2_q.'" />';
						echo '<input type="hidden" id="sc_riv_3_q['.$products['product_id'].']" value="'.$sc_riv_3_q.'" />';
						 echo '<input type="hidden" id="oldQuantity['.$products['product_id'].']" value="'.$products['quantity'].'" />';
						echo '<input type="hidden" id="oldPrice['.trim($products['product_id']).']" value="'.
					  ($products['product_price']).'" />';
					  
					  echo '  <input name="product_quantity_old['.$products['id_order_detail'].']" type="hidden" value="'.$products['product_quantity'].'" />';
					  echo '  <input name="product_id['.$products['id_order_detail'].']" type="hidden" value="'.$products['product_id'].'" />';
					  echo '  <input name="product_stock['.$products['id_order_detail'].']" type="hidden" value="'.$products['stock'].'" /></td>';
				   echo '</tr> ';
				}
			//}
			$totale = $total_products;
			$totale = $totale+$total_commissions;
			$totale = $totale-$total_discounts;
			
			$marginalitatotale = ((($totale - $totaleacquisti)*100) / $totale);
			$guadagno = $totale - $totaleacquisti;
			$totale_senza_spedizione = $totale;
			
			$costo_spedizione = ($total_shipping/((100+$carrier_tax_rate)/100));
			
			$totale += $costo_spedizione; 
			
			$tax_regime = Db::getInstance()->getValue("SELECT tax_regime FROM customer WHERE id_customer = ".$customer->id."");
			
			if(!$tax_regime)
				$tax_regime = 0;
			
			if($tax_regime == 0) {
				$id_tax = 1;
			}
			else {
				$id_tax = $tax_regime;
			}
			$tax_rate = Db::getInstance()->getValue("SELECT rate FROM tax WHERE id_tax = ".$id_tax."");
				
			if($tax_regime == 1) {
				$tax_rate = 0;
			}
			
			$id_country = Db::getInstance()->getValue("SELECT id_country FROM address WHERE id_address = ".Db::getInstance()->getValue('SELECT id_address_delivery FROM orders WHERE id_order = '.Tools::getValue('id_order'))."");
			
			if($id_country == 10) {
				
				
				$iva = ($totale * $tax_rate)/100;
				
			}
				
			else {
				
				if($customer->is_company == 1  || ($customer->is_company == 0 && $id_country == 19)) 
				{
					$tax_rate = 0;
					$iva = 0;
				}
				else
					$iva = ($totale * $tax_rate)/100;
			}
			
			
			if(!$id_country)
				$iva = ($totale * $tax_rate)/100;
				
			if($tax_regime == 1) {

				$iva = 0;
					
			}
			
			$carrier_cart = Db::getInstance()->getValue("SELECT id_carrier FROM orders WHERE id_order = ".$order['id_order']."");
			
			echo '
			</tbody>
			<tfoot>
			<tr id="trasp_carrello">
			<td></td><td>TRASP</td><td>Trasporto (non modificare per calcolarlo in automatico)</td><td style="text-align:right">1</td>
			<input name="vecchia_spedizione" type="hidden"  value="'.number_format($total_shipping/((100+$order['carrier_tax_rate'])/100),2).'" />
			<input type="hidden" id="trasporto_modificato" value="'.($costo_trasporto_modificato == '' ? 'n' : 'y').'" />
			<td style="text-align:right" id="costo_trasporto"><input type="text" value="'.number_format($total_shipping/((100+$tax_rate)/100),2,",","").'" size="7" onkeyup="document.getElementById(\'trasporto_modificato\').value = \'y\'; document.getElementById(\'importo_trasporto\').innerHTML=this.value; calcolaImportoConSpedizione('.$carrier_cart.');" name="transport" id="transport" style="text-align:right" /></td><td></td><td style="text-align:right"></td><td style="text-align:right"></td><td></td><td style="text-align:right" id="importo_trasporto">'.number_format($total_shipping/((100+$order['carrier_tax_rate'])/100),2,",","").'</td><td></td><td></td></tr>
			
			
			
			'.($total_commissions > 0 ? '
			<tr id="riga_commissioni">
			<td></td><td></td><td>Commissioni</td><td style="text-align:right">1</td>
			<td style="text-align:right" id="sconti_aggiuntivi">
			<input name="vecchie_commissioni" type="hidden"  value="'.$total_commissions.'" />
			<input type="text" value="'.number_format($total_commissions,2,",","").'" size="7" onkeyup="calcolaImportoConSpedizione('.$carrier_cart.'); document.getElementById(\'importo_commissioni\').innerHTML=this.value;" name="total_commissions" id="total_commissions" style="text-align:right" /></td><td style="text-align:right"></td><td style="text-align:right"><td></td></td><td></td><td style="text-align:right" id="importo_commissioni">'.number_format($total_commissions,2,",","").'</td><td></td><td></td></tr>' : '<input type="hidden" name="total_commissions" id="total_commissions" value="0" />').'
			
			<tr id="sconti_extra">
			<td></td><td></td><td>Sconti</td><td style="text-align:right">1</td>
			<td style="text-align:right" id="sconti_aggiuntivi">
			<input name="vecchi_sconti" type="hidden"  value="'.$total_discounts.'" />
			<input type="text" value="'.number_format($total_discounts,2,",","").'" size="7" onkeyup="calcolaImportoConSpedizione('.$carrier_cart.'); document.getElementById(\'importo_sconti\').innerHTML=this.value;" name="total_discounts" id="total_discounts" style="text-align:right" /></td><td style="text-align:right"></td><td style="text-align:right"><td></td></td><td></td><td style="text-align:right" id="importo_sconti">-'.number_format($total_discounts,2,",","").'</td><td></td><td></td></tr>
			
			
			<tr><td></td><td style="width:130px"></td><td style="width:330px">Guadagno totale in euro: <span id="guadagnototale">'.number_format($guadagno,2, ',', '').'</span> &euro;</td><td style="width:45px"></td><td style="width:45px"></td><td style="width:75px"></td><td style="width:45px"></td><td style="width:55px"><span id="marginalitatotale">'.number_format($marginalitatotale,2, ',', '').'%</span></td><td style="width:25px"><strong>Imponibile</strong></td><td style="width:85px; text-align:right"><span id="spantotaleprodotti"> '.number_format($totale-$total_discounts,2, ',', '').'</span></td><td style="width:55px"></td><td></td></tr>
			
			<tr><td></td><td style="width:130px"></td><td style="width:310px"></td><td style="width:45px"></td><td style="width:75px"></td><td style="width:45px"></td><td style="width:75px"></td><td style="width:55px"></td><td style="width:25px"><strong>IVA incl.</strong></td><td style="width:85px; text-align:right"><span id="spantotaleprodottiiva"> '.number_format($totale+$iva,2, ',', '').'</span></td><td style="width:10px"></td>
			<td style="width:10px"></td><!-- <td style="width:45px"></td> --></tr>
			</tfoot>	
			</table>
			';
			$bonifico_accettato = Db::getInstance()->getValue('SELECT id_order_history FROM order_history WHERE id_order = '.Tools::getValue('id_order').' AND (id_order_state = 2 OR id_order_state = 18)');
			
			if($payment != "" && $payment != "Bonifico" && $payment != "nessuno" && $payment != "Bonifico Bancario" && $payment != "Bonifico anticipato" && $payment != "Bonifico Anticipato" && $payment != "Carta" && $payment != "GestPay" && $payment != "Carta Amazon"  && $payment != "Amazon" && $payment != "Carta ePrice"  && $payment != "ePrice" && $payment != "Paypal" && $payment != "PayPal" && $payment != "Contrassegno" && $payment != "Bonifico 30 gg. fine mese" && $payment != "Bonifico 60 gg. fine mese" && $payment != "Bonifico 90 gg. fine mese"  && $payment != "Bonifico 30 gg. 15 mese successivo" && $payment != 'R.B. 30 GG. D.F. F.M.' && $payment != 'R.B. 60 GG. D.F. F.M.' && $payment != 'R.B. 90 GG. D.F. F.M.'  && $payment != "R.B. 30 GG. 5 mese successivo" && $payment != "R.B. 30 GG. 10 mese successivo" && $payment != "R.B. 60 GG. 5 mese successivo" && $payment != "R.B. 60 GG. 10 mese successivo" ) {
					$check_payment = 1;
				} else {
					$check_payment = 0;
				}
				
			echo'
			<br /><div id="metodo_di_pagamento"  '.($cookie->id_employee == 2 || $cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 14 ? '' : (($module == 'bankwire' && $bonifico_accettato > 0) || $module == 'gestpay' || $module == 'Carta Amazon' || $module == 'Amazon' || $module == 'carta' || $module == 'PayPal' || $module == 'paypal' ? 'style="display:none"' : '')).'><strong>Metodo di pagamento</strong>: 
			<select name="metodo_pagamento">
			
			<option value="bankwire" '.($module == 'bankwire' ? 'selected="selected"' : '').'>Bonifico anticipato</option>
			<option value="gestpay" '.($module == 'gestpay' ? 'selected="selected"' : '').'>Carta di credito (Gestpay)</option>
			<option value="Carta Amazon" '.($payment == "Carta Amazon" || $payment == "Amazon" ? "selected='selected'" : "").'>Carta Amazon</option>
			<option value="Carta ePrice" '.($payment == "Carta ePrice" || $payment == "ePrice" ? "selected='selected'" : "").'>Carta ePrice</option>
			<option value="paypal" '.($module == 'paypal' ? 'selected="selected"' : '').'>PayPal</option> 
			<option value="cashondeliverywithfee" '.($module == 'cashondeliverywithfee' ? 'selected="selected"' : '').'>Contrassegno</option>
			<option value="Bonifico 30 gg. fine mese" '.($module == 'other_payment' && $payment == 'Bonifico 30 gg. fine mese' ? 'selected="selected"' : '').'>Bonifico 30 gg. fine mese</option>
			<option value="Bonifico 60 gg. fine mese"'.($module == 'other_payment' && $payment == 'Bonifico 60 gg. fine mese' ? 'selected="selected"' : '').'>Bonifico 60 gg. fine mese</option>
			<option value="Bonifico 90 gg. fine mese"'.($module == 'other_payment' && $payment == 'Bonifico 90 gg. fine mese' ? 'selected="selected"' : '').'>Bonifico 90 gg. fine mese</option>
			<option value="Bonifico 30 gg. 15 mese successivo"'.($module == 'other_payment' && $payment == 'Bonifico 30 gg. 15 mese successivo' ? 'selected="selected"' : '').'>Bonifico 30 gg. 15 mese successivo</option>
			<option value="R.B. 30 GG. D.F. F.M." '.($payment == "R.B. 30 GG. D.F. F.M." ? "selected='selected'" : "").'>R.B. 30 GG. D.F. F.M.</option>
			<option value="R.B. 60 GG. D.F. F.M." '.($payment == "R.B. 60 GG. D.F. F.M." ? "selected='selected'" : "").'>R.B. 60 GG. D.F. F.M.</option>
			<option value="R.B. 90 GG. D.F. F.M." '.($payment == "R.B. 90 GG. D.F. F.M." ? "selected='selected'" : "").'>R.B. 90 GG. D.F. F.M.</option>
			<option value="R.B. 30 GG. 5 mese successivo" '.($payment == "R.B. 30 GG. 5 mese successivo" ? "selected='selected'" : "").'>R.B. 30 GG. 5 mese successivo</option>
			<option value="R.B. 30 GG. 10 mese successivo" '.($payment == "R.B. 30 GG. 10 mese successivo" ? "selected='selected'" : "").'>R.B. 30 GG. 10 mese successivo</option>
			<option value="R.B. 60 GG. 5 mese successivo" '.($payment == "R.B. 60 GG. 5 mese successivo" ? "selected='selected'" : "").'>R.B. 60 GG. 5 mese successivo</option>
			<option value="R.B. 60 GG. 10 mese successivo" '.($payment == "R.B. 60 GG. 10 mese successivo" ? "selected='selected'" : "").'>R.B. 60 GG. 10 mese successivo</option>
			<option value="Renting"'.($module == 'other_payment' && $payment == 'Renting' ? 'selected="selected"' : '').'>Renting</option>
			'.($check_payment == 1 ? '<option value="altro" selected="selected">Altro (vedi cella a fianco)</option>' : '').'
			</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			
			Altro metodo: <input type="text" name="other_payment"  value="'.($check_payment == 1 ? str_replace("'",htmlentities("'",ENT_QUOTES),$payment) : "").'" /><br /><br />	';
			echo '</div>';
			
			if($module == 'gestpay' || $module == 'Carta Amazon' || $payment == 'Bonifico 30 gg. fine mese' || $payment == 'Bonifico 60 gg. fine mese' || $payment == 'Bonifico 90 gg. fine mese' || $payment == 'Bonifico 30 gg. 15 mese successivo' || $payment == 'R.B. 30 GG. D.F. F.M.' || $payment == 'R.B. 60 GG. D.F. F.M.' || $payment == 'R.B. 90 GG. D.F. F.M.' || $payment == 'R.B. 30 GG. 5 mese successivo' || $payment == 'R.B. 30 GG. 10 mese successivo' || $payment == 'R.B. 60 GG. 5 mese successivo' || $payment == 'R.B. 60 GG. 10 mese successivo')
			{
				$ordz = Db::getInstance()->getRow('SELECT * FROM orders WHERE id_order = '.Tools::getValue('id_order'));
				
				$prezzi_carrello = Db::getInstance()->getValue('SELECT prezzi_carrello FROM cart WHERE id_cart = '.$ordz['id_cart']);
		
				$sconti_extra = Db::getInstance()->getValue('SELECT id_cart FROM cart_product WHERE id_cart = '.$ordz['id_cart'].' AND sconto_extra > 0');
				
				$ex_riv = Db::getInstance()->getValue("SELECT ex_rivenditore FROM customer WHERE id_customer = ".$ordz['id_customer']."");
		
				 if($gruppo == 3 || ($gruppo == 15  && $discountz == 1) || $gruppo == 11 || $gruppo == 12 || ($ex_riv == 1 && $discountz == 1) || $prezzi_carrello == 15 || $prezzi_carrello == 3)
				 { 
					$total_commissions = Db::getInstance()->getValue('SELECT total_commissions FROM orders WHERE id_order = '.Tools::getValue('id_order'));
					/*echo '
					<input type="checkbox" '.($total_commissions > 0 ? 'checked="checked"' : '').' name="addebito_commissioni" /> Addebita commissioni
					';*/
					echo '';
				 }
				
			}
			
			$provincia_cliente = Db::getInstance()->getValue("SELECT id_state FROM address WHERE id_address = ".$address_delivery."");
				
			$nazione_cliente = Db::getInstance()->getValue("SELECT id_country FROM address WHERE id_customer = ".$address_delivery."");
				
			if($provincia_cliente == 0) {
				$zona_cliente = Db::getInstance()->getValue("SELECT id_zone FROM country WHERE id_country = ".$nazione_cliente."");
			}
			else {
				$zona_cliente = Db::getInstance()->getValue("SELECT id_zone FROM state WHERE id_state = ".$provincia_cliente."");
			}
			
			$gruppi_per_spedizione = array();
			$gruppi_per_spedizione[] = $customer->id_default_group;
			
			$metodi_spedizione = Cart::getCarriersForEditOrder($zona_cliente, $gruppi_per_spedizione, $order['id_cart']);
			
			if(sizeof($metodi_spedizione) == 0)
				$metodi_spedizione = Cart::getCarriersForEditOrder(9, $gruppi_per_spedizione, $order['id_cart']);
			
			foreach($metodi_spedizione as $metodo) {
				if($metodo['name'] != 'Ritiro in sede Ezdirect' && $metodo['name'] != 'Trasporto gratuito') {
					$default_carrier = $metodo['id_carrier'];
				}
				else {
				}
			}
			
			echo "<strong>Seleziona metodo di spedizione</strong>:";
			
			$array_id_metodi = array();
			
			foreach ($metodi_spedizione as $metodo) {
				$array_id_metodi[] = $metodo['id_carrier'];
			}
			
			
			$i_metodi_spedizione = 0;
			foreach ($metodi_spedizione as $metodo) {
				$i_metodi_spedizione++;
				if ($costo_trasporto_modificato == '' || $costo_trasporto_modificato == 0) {
					$metodo['price_tax_exc'] = $metodo['price_tax_exc'];
				}
				else {
					if ($carrier_cart == $metodo['id_carrier']) {
						$metodo['price_tax_exc'] = $costo_trasporto_modificato;
					}
					else {
						$metodo['price_tax_exc'] = $metodo['price_tax_exc'];
					}
				}

				echo "<input type='radio' id='metodo[".$metodo['id_carrier']."]' ".(strpos($metodo['name'],'grat') !== false ? "class='gratis'"  : (strpos($metodo['name'],'itiro') !== false ? "class='ritiro'" : "class='a_pagamento'"))." name='metodo' rel='".$metodo['price_tax_exc']."' value='".$metodo['id_carrier']."' ".($carrier_cart == $metodo['id_carrier'] ? "checked='checked'" : ($carrier_cart == '' || $carrier_cart == 0 || !in_array($carrier_cart, $array_id_metodi)  ? ($metodo['name'] != 'Ritiro in sede Ezdirect' && $metodo['name'] != 'Trasporto gratuito' ? "checked='checked'" : "") : ""))." onclick='document.getElementById(\"transport\").value = \"".number_format($metodo['price_tax_exc'],2,",","")."\"; document.getElementById(\"importo_trasporto\").innerHTML = \"".number_format($metodo['price_tax_exc'],2,",","")."\"; calcolaImportoConSpedizione(".$metodo['id_carrier']."); ' />".$metodo['name']." (<span id='metodo_costo[".$metodo['id_carrier']."]'>".number_format($metodo['price_tax_exc'],2,",","")."</span> &euro;)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			
			}
			
			
			echo '
		    <p align="center">
			';
			
		
			
			if($rif_fatt > 0)
			{
				
			}
			else
			{
				echo '
			  Riprodurre CSV? <input name="si_csv" type="checkbox" checked="checked" /><br /><br />';
			  
			}
			echo '
			  <input name="Apply" type="submit" class="button" value="Aggiorna" />
			  <input name="id_order" type="hidden" value="'. $_GET['id_order'] .'" />
			  <input name="tax_rate" type="hidden" value="'. $tax_rate .'" />
			  <input name="totaleprodotti" id="totaleprodotti" type="hidden" value="'.$total_products.'" />
			  <input name="id_lang" id="id_lang" type="hidden" value="'. $id_lang .'" />
			</p></form>';

	
		
		}

		else if(isset($_GET['orderupdated'])) {
		
			
			$id_order_g = htmlspecialchars($_GET['id_order']);
			$action_g = ""; 
			$id_product_g = htmlspecialchars($_POST['id_product']);
			$id_lang_g = 5;

			if ($id_order_g) 
			$_POST['id_order']=$id_order_g;
			

			if ($_POST['id_order']) {
				if ($action_g=='add_product') {
					$query=" select p.*,pl.name,t.id_tax as tax_rate,tl.name as tax_name, sp.reduction from ". _DB_PREFIX_."product p join ". _DB_PREFIX_."product_lang pl on p.id_product=pl.id_product ";
					$query.=" left join ". _DB_PREFIX_."tax t on t.id_tax=p.id_tax_rules_group";
					$query.=" left join ". _DB_PREFIX_."tax_lang tl on t.id_tax=tl.id_tax";
					$query.= " LEFT JOIN ". _DB_PREFIX_."specific_price sp ON sp.id_product = p.id_product";
					$query.=" where p.id_product=".$id_product_g. " and pl.id_lang=".$id_lang_g;
					//echo $query; die();
					$res=mysql_query($query);
					$products=mysql_fetch_array($res);
					
					$tax_regime = Db::getInstance()->getValue("SELECT tax_regime FROM customer WHERE id_customer = ".$customer->id."");
					
					$products['tax_rate'] = Tax::getProductTaxRate((int)$products['id_product'], Configuration::get('PS_TAX_ADDRESS_TYPE'));
					
					if (is_null($products['tax_rate'])) $products['tax_rate']=0;

					$query="insert into ". _DB_PREFIX_."order_detail (id_order ,product_id ,product_name ,product_quantity,reduction_percent,product_quantity_in_stock ,product_price ,product_ean13 ,product_reference ,product_supplier_reference ,product_weight ,tax_name ,tax_rate, reduction_amount, cons ) values  ";
					$query.="(".$id_order_g.",".$products['id_product'].",'".addslashes($products['name'])."',1,'".addslashes( str_replace(',', '.', $products['reduction_percent']))."',1,";
					$query.=$products['price'].",'".$products['ean13']."','".addslashes($products['reference'])."','".addslashes($products['supplier_reference'])."',".$products['weight'].",'".addslashes($products['tax_name'])."','".$products['tax_rate'].",".$products['reduction'].", '0_0')";
					// echo $query;
					Db::getInstance()->executeS($query);
					AdminOrders::update_total($id_order_g);
				}
				
				$act_history = Db::getInstance()->getValue('SELECT MAX(id_order_history) FROM order_history WHERE id_order = '.$id_order_g.'');
				$status_attuale = Db::getInstance()->getValue('SELECT id_order_state FROM order_history WHERE id_order_history = '.$act_history.'');
				
				$module = Db::getInstance()->getValue('SELECT module FROM orders WHERE id_order = '.$id_order_g.'');
				
				$payment = Tools::getValue('metodo_pagamento');
				$other_payment = Tools::getValue('other_payment');
				
				if($payment != $module || !empty($other_payment))
				{	
					Customer::Storico(Tools::getValue('id_order'), 'O', $cookie->id_employee, 'Ha modificato il metodo di pagamento dell\'ordine');
			
					if($payment == 'bankwire' && empty($other_payment)) 
					{
						$payment = 'Bonifico Anticipato';
						$module = 'bankwire';
						
						if($status_attuale != 4 && $status_attuale != 5 && $status_attuale != 6 && $status_attuale != 7 && $status_attuale != 8 && $status_attuale != 14 && $status_attuale != 15 && $status_attuale != 16 && $status_attuale != 20 && $status_attuale != 24 && $status_attuale != 25 && $status_attuale != 26 && $status_attuale != 31)
							Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.$id_order_g.', 10, "'.date('Y-m-d H:i:s').'")');
					
					}
					else if($payment == 'gestpay' && empty($other_payment)) 
					{
						$payment = 'GestPay';
						$module = 'gestpay';
						if($status_attuale != 4 && $status_attuale != 5 && $status_attuale != 6 && $status_attuale != 7 && $status_attuale != 8 && $status_attuale != 14 && $status_attuale != 15 && $status_attuale != 16 && $status_attuale != 20 && $status_attuale != 24 && $status_attuale != 25 && $status_attuale != 26 && $status_attuale != 31)
							Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.$id_order_g.', 2, "'.date('Y-m-d H:i:s').'")');
					
					}
					else if($payment == 'paypal' && empty($other_payment)) 
					{
						$payment = 'Paypal';
						$module = 'paypal';
						if($status_attuale != 4 && $status_attuale != 5 && $status_attuale != 6 && $status_attuale != 7 && $status_attuale != 8 && $status_attuale != 14 && $status_attuale != 15 && $status_attuale != 16 && $status_attuale != 20 && $status_attuale != 24 && $status_attuale != 25 && $status_attuale != 26 && $status_attuale != 31)
							Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.$id_order_g.', 2, "'.date('Y-m-d H:i:s').'")');
					
					}
					else if($payment == 'Carta Amazon' && empty($other_payment)) 
					{
						$payment = 'Carta Amazon';
						$module = 'other_payment';
						if($status_attuale != 4 && $status_attuale != 5 && $status_attuale != 6 && $status_attuale != 7 && $status_attuale != 8 && $status_attuale != 14 && $status_attuale != 15 && $status_attuale != 16 && $status_attuale != 20 && $status_attuale != 24 && $status_attuale != 25 && $status_attuale != 26 && $status_attuale != 31)
							Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.$id_order_g.', 22, "'.date('Y-m-d H:i:s').'")');
					
					}
					else if($payment == 'cashondeliverywithfee' && empty($other_payment)) 
					{
						$payment = 'Contrassegno';
						$module = 'cashondeliverywithfee';
						if($status_attuale != 4 && $status_attuale != 5 && $status_attuale != 6 && $status_attuale != 7 && $status_attuale != 8 && $status_attuale != 14 && $status_attuale != 15 && $status_attuale != 16 && $status_attuale != 20 && $status_attuale != 24 && $status_attuale != 25 && $status_attuale != 26 && $status_attuale != 31)
							Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.$id_order_g.', 3, "'.date('Y-m-d H:i:s').'")');
					
					}
					/*else if($payment != 'bankwire' && $payment != 'gestpay' && $payment != 'paypal' && $payment != 'cashondeliverywithfee' && empty($other_payment)) 
					{
						$payment = $payment;
						$module = 'other_payment';
						if($status_attuale != 4 && $status_attuale != 5 && $status_attuale != 6 && $status_attuale != 7 && $status_attuale != 8 && $status_attuale != 14 && $status_attuale != 15 && $status_attuale != 16 && $status_attuale != 20 && $status_attuale != 22 && $status_attuale != 24 && $status_attuale != 25 && $status_attuale != 26 && $status_attuale != 31)
							Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.$id_order_g.', 3, "'.date('Y-m-d H:i:s').'")');
					
					}*/
					else if((trim($payment) == 'Bonifico 30 gg. fine mese' || trim($payment) == 'Bonifico 60 gg. fine mese' || trim($payment) == 'Bonifico 90 gg. fine mese' || trim($payment) == 'Bonifico 30 gg. 15 mese successivo' || trim($payment) == 'R.B. 30 GG. D.F. F.M.' || trim($payment) == 'R.B. 60 GG. D.F. F.M.' || trim($payment) == 'R.B. 90 GG. D.F. F.M.'    || trim($payment) == "R.B. 30 GG. 5 mese successivo" || trim($payment) == "R.B. 30 GG. 10 mese successivo" || trim($payment) == "R.B. 60 GG. 5 mese successivo" || trim($payment) == "Bonifico posticipato" || trim($payment) == "R.B. 60 GG. 10 mese successivo" || trim($payment) == 'Renting')  && empty($other_payment))
					{
						$payment = $payment;
						$module = 'other_payment';
						if($status_attuale != 4 && $status_attuale != 5 && $status_attuale != 6 && $status_attuale != 7 && $status_attuale != 8 && $status_attuale != 14 && $status_attuale != 15 && $status_attuale != 16 && $status_attuale != 20 && $status_attuale != 24 && $status_attuale != 25 && $status_attuale != 26 && $status_attuale != 31)
							Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.$id_order_g.', 21, "'.date('Y-m-d H:i:s').'")');
					
					}
					/*
					else if($payment == 'Carta Amazon' && empty($other_payment)) 
					{
						$payment = $payment;
						$module = 'other_payment';
						if($status_attuale != 4 && $status_attuale != 5 && $status_attuale != 6 && $status_attuale != 7 && $status_attuale != 8 && $status_attuale != 14 && $status_attuale != 15 && $status_attuale != 16 && $status_attuale != 20 && $status_attuale != 24 && $status_attuale != 25 && $status_attuale != 26 && $status_attuale != 31)
							Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.$id_order_g.', 22, "'.date('Y-m-d H:i:s').'")');
					}
					*/
					else if(!empty($other_payment))
					{
						$payment = $other_payment;
						$module = 'other_payment';
					}
					
					else 
					{
						$payment = Db::getInstance()->getValue('SELECT payment FROM orders WHERE id_order = '.Tools::getValue('id_order').'');
						$module =  Db::getInstance()->getValue('SELECT module FROM orders WHERE id_order = '.Tools::getValue('id_order').'');
					
					}
				}
				else
					$payment = Db::getInstance()->getValue('SELECT payment FROM orders WHERE id_order = '.Tools::getValue('id_order').'');
				
				$vecchio_ordine = Db::getInstance()->executeS('SELECT * FROM order_detail WHERE id_order = '.Tools::getValue('id_order').'');
				
				$vecchio_pagamento = Db::getInstance()->getValue('SELECT payment FROM orders WHERE id_order = '.Tools::getValue('id_order').'');
				$nuovo_pagamento = $payment;
				
				$vo_total = 0;
				$msg_vecchio_ordine = '<strong>Vecchio ordine</strong> (tipo pagamento: '.$vecchio_pagamento.')<br /><table cellpadding="0" cellspacing="0"><tr style="background-color: #db6906; text-transform: uppercase;"><th style="width:350px">Codice eSolver</th><th style="width:100px">Qt.</th><th style="width:70px">Unitario</th><th style="width:100px">Totale</th></tr>';
				foreach($vecchio_ordine as $vo)
				{
					
					$msg_vecchio_ordine .= '<tr><td style="border:1px solid #ebebeb">'.$vo['product_reference'].'</td>
					<td style="text-align:right; border:1px solid #ebebeb">'.$vo['product_quantity'].'</td>
					<td style="text-align:right; border:1px solid #ebebeb">'.Tools::displayPrice(($vo['product_price'] - (($vo['product_price']*$vo['reduction_percent']) / 100 )),1).'</td>
					<td style="text-align:right; border:1px solid #ebebeb">'.Tools::displayPrice((($vo['product_price'] - (($vo['product_price']*$vo['reduction_percent']) / 100 )) * $vo['product_quantity']),1).'</td>
					</tr>';
					
					$vo_total += (($vo['product_price'] - (($vo['product_price']*$vo['reduction_percent']) / 100 )) * $vo['product_quantity']);
					
				}
				
				$vo_total += $_POST['vecchia_spedizione'];
				
				$msg_vecchio_ordine .= '
				<tr><td style="border:1px solid #ebebeb">Trasporto</td><td style="border:1px solid #ebebeb"></td><td style="border:1px solid #ebebeb"></td><td style="border:1px solid #ebebeb; text-align:right">'.Tools::displayPrice($_POST['vecchia_spedizione'],1).'</td></tr>
				<tr><td style="border:1px solid #ebebeb">Totale</td><td style="border:1px solid #ebebeb"></td><td style="border:1px solid #ebebeb"></td><td style="border:1px solid #ebebeb; text-align:right">'.Tools::displayPrice($vo_total,1).'</td></tr></table><br /><br />';
				
				
				
				$vecchio_cig = Db::getInstance()->getValue('SELECT cig FROM orders WHERE id_order = '.Tools::getValue('id_order').'');
				$vecchio_cup = Db::getInstance()->getValue('SELECT cup FROM orders WHERE id_order = '.Tools::getValue('id_order').'');
				$vecchio_ipa = Db::getInstance()->getValue('SELECT ipa FROM orders WHERE id_order = '.Tools::getValue('id_order').'');
				$vecchia_dat = Db::getInstance()->getValue('SELECT data_ordine_mepa FROM orders WHERE id_order = '.Tools::getValue('id_order').'');
				
				$dom = explode('-',Tools::getValue('data_ordine_mepa'));
				
				$nuovo_cig = Tools::getValue('cig');
				$nuovo_cup = Tools::getValue('cup');
				$nuovo_ipa = Tools::getValue('ipa');
				$nuova_dat = $dom[2].'-'.$dom[1].'-'.$dom[0];
				
				$prodtaxes = Db::getInstance()->executeS('SELECT tax_rate FROM order_detail WHERE id_order = '.$_POST['id_order']);
				$wtxc = 0;
				foreach($prodtaxes as $txc)
				{
					if($txc['tax_rate'] > 0)
						$wtxc = 1;
				}
				
				if($wtxc == 0)
					$carrier_tax_rate = 0;
				else
					$carrier_tax_rate = $_POST['tax_rate'];
				
			
				if(Tools::getValue('data_ordine_mepa') == '')
					$nuova_dat = '0000-00-00';
				
					$query="update  ". _DB_PREFIX_."orders set cig = '".$nuovo_cig."', cup = '".$nuovo_cup."', ipa = '".$nuovo_ipa."', ".(Tools::getValue('data_ordine_mepa') == '0000-00-00' || Tools::getValue('data_ordine_mepa') == '' ? '' : "data_ordine_mepa = '".$nuova_dat."',")." payment = '".$payment."', id_carrier = '".$_POST['metodo']."', module = '".$module."', ";
					$query.=" total_discounts='".(AdminOrders::price($_POST['total_discounts']) == '' ? '0.00' : AdminOrders::price($_POST['total_discounts']));
					$query.="', total_commissions='".(AdminOrders::price($_POST['total_commissions']) == '' ? '0.00' : AdminOrders::price($_POST['total_commissions']));
					$query.="' ,carrier_tax_rate=".$carrier_tax_rate.", total_shipping='".(AdminOrders::price($_POST['transport']) == '' ? '0.00' : AdminOrders::price($_POST['transport'])+(AdminOrders::price($_POST['transport'])*$carrier_tax_rate/100));
					$query.="' where id_order=".$_POST['id_order'];
					$query.=" limit 1";
					//echo $query; die();
					Db::getInstance()->executeS($query);

				

				if ($_POST['Apply']) {
					$messaggio = "";
					
					if($vecchio_cig != $nuovo_cig) 
					{
						Customer::Storico(Tools::getValue('id_order'), 'O', $cookie->id_employee, 'Ha modificato il CIG dell\'ordine');
			
						$messaggio.= "<br /><br />Modificato CIG. Vecchio CIG: ".$vecchio_cig." / Nuovo CIG: ".$nuovo_cig."<br /><br />";
					}
					
					if($vecchio_cup != $nuovo_cup) 
					{
						Customer::Storico(Tools::getValue('id_order'), 'O', $cookie->id_employee, 'Ha modificato il CUP dell\'ordine');
						$messaggio.= "<br /><br />Modificato CUP. Vecchio CUP: ".$vecchio_cup." / Nuovo CUP: ".$nuovo_cup."<br /><br />";
					}
					
					if($vecchio_ipa != $nuovo_ipa) 
					{
						Customer::Storico(Tools::getValue('id_order'), 'O', $cookie->id_employee, 'Ha modificato l\'IPA dell\'ordine');
						$messaggio.= "<br /><br />Modificato IPA. Vecchio IPA: ".$vecchio_ipa." / Nuovo IPA: ".$nuovo_ipa."<br /><br />";
					}
					
					if($vecchia_dat != Tools::getValue('data_ordine_mepa') && Tools::getValue('data_ordine_mepa') != '' && Tools::getValue('data_ordine_mepa') != '0000-00-00') 
					{
						Customer::Storico(Tools::getValue('id_order'), 'O', $cookie->id_employee, 'Ha modificato data ordine mepa dell\'ordine');
						$messaggio.= "<br /><br />Modificato data ordine mepa. Vecchia data: ".date('d-m-Y',strtotime($vecchia_dat))." / Nuova data: ".Tools::displayDate($nuova_dat,$cookie->id_lang)."<br /><br />";
					}
					
					if($vecchio_pagamento != $nuovo_pagamento) 
					{
						$messaggio.= "<br /><br />Modificato metodo di pagamento. Vecchio metodo: ".$vecchio_pagamento." / Nuovo metodo: ".$payment."<br /><br />";
					}
					
					if($_POST['total_discounts'] != number_format($_POST['vecchi_sconti'],2,",","")) {
					
						$messaggio.= "<br /><br />Modificati sconti. Vecchi sconti: ".$_POST['vecchi_sconti']." / Nuovi sconti: ".$_POST['total_discounts']."<br /><br />";
					
					}
					
					if($_POST['total_commissions'] != number_format($_POST['vecchie_commissioni'],2,",","")) {
					
						$messaggio.= "<br /><br />Modificate commissioni. Vecchie commissioni: ".$_POST['vecchie_commissioni']." / Nuove commissioni: ".$_POST['total_commissions']."<br /><br />";
					
					}
					
					if($_POST['transport'] != number_format($_POST['vecchia_spedizione'],2,",","")) {
					
						Customer::Storico(Tools::getValue('id_order'), 'O', $cookie->id_employee, 'Ha modificato le spese di spedizione dell\'ordine');
						
						$messaggio.= "<br /><br />Modificate spese di spedizione. Vecchie spese di spedizione: ".$_POST['vecchia_spedizione']." / Nuove spese di spedizione: ".$_POST['transport']."<br /><br />";
					
					}
					
					//delete product
					
					$prodotti_cancellati = explode(",",$_POST['prodotti_cancellati']);
					$messaggio_cancellati = '';
					
					$frs_d_p = 0;
					foreach($prodotti_cancellati as $id_prodotto_cancellato) {
					
						if($id_prodotto_cancellato == 0) {
						}
						else {
							if($frs_d_p == 0) {
								$messaggio_cancellati .= "<br /><br />Prodotti cancellati: <br /><br />";
								$frs_d_p = 1;
							}
							else {
								
							}
							$nome = Db::getInstance()->getValue("SELECT product_name FROM order_detail WHERE product_id = ".$id_prodotto_cancellato."");
							$messaggio_cancellati .= $nome." (".Db::getInstance()->getValue('SELECT reference FROM product WHERE id_product = '.$id_prodotto_cancellato.'').") - ";
						}
					}
					$i_aggiunti = 0;
					$messaggio .= $messaggio_cancellati;
					if(sizeof($prodotti_cancellati) > 0)
					{
						Customer::Storico(Tools::getValue('id_order'), 'O', $cookie->id_employee, 'Ha cancellato prodotti:' .$messaggio_cancellati);
					}
					
					if ($_POST['product_delete']) {
						foreach ($_POST['product_delete'] as $id_product=>$value) {
							Db::getInstance()->executeS("delete from ". _DB_PREFIX_."order_detail where id_order = ".$_GET['id_order']." AND product_id=".$id_product);
						}
					}
					
					foreach ($_POST['nuovo_prodotto'] as $id_product_g=>$id_product) {
						
						
							$i_aggiunti++;
							
							if($i_aggiunti == 1) {
								$messaggio_aggiunti .= "<br /><br />Prodotti aggiunti:<br /><br />";
							}
							$qty_difference=(Db::getInstance()->getValue('SELECT quantity FROM product WHERE id_product = '.$id_product_g))-$_POST['quantity'][$id_product_g];
							$stock=max(0,$_POST['stock'][$id_product_g]+$qty_difference);
							$messaggio_aggiunti.= $_POST['nuovo_name'][$id_product_g]." - ".Db::getInstance()->getValue('SELECT reference FROM product WHERE id_product = '.$id_product_g.'')." (quantit&agrave;: ".$_POST['nuovo_quantity'][$id_product_g].", prezzo: ".number_format(str_replace(",",".",$_POST['nuovo_price'][$id_product_g]),2,",","")." euro) - ";
							$query="insert into ". _DB_PREFIX_."order_detail (id_order ,product_id ,product_name ,product_quantity ,reduction_percent,product_quantity_in_stock ,product_price ,product_ean13 ,product_reference ,product_supplier_reference, category_id, manufacturer_id, product_weight ,tax_name ,tax_rate, cons) values  ";
							$query.="(".$_GET['id_order'].",".$_POST['nuovo_prodotto'][$id_product_g].",'".addslashes($_POST['nuovo_name'][$id_product_g])."','".$_POST['nuovo_quantity'][$id_product_g]."','". ($_POST['unitario'][$id_product_g] == 0 ? 0 : str_replace(',', '.', $_POST['sconto_extra'][$id_product_g]))."','".Db::getInstance()->getValue('SELECT quantity FROM product WHERE id_product = '.$id_product_g)."',";
							
							
							$query.=
							
							AdminOrders::price((isset($_POST['usa_sconti_quantita'][$id_product_g]) && (Product::trovaPrezzoSpeciale($id_product_g, 1, 0) == 0 && $_POST['unitario'][$id_product_g] != 0) ? Product::trovaMigliorPrezzo($id_product_g, $customer->id_default_group,$_POST['nuovo_quantity'][$id_product_g]) : (Product::trovaPrezzoSpeciale($id_product_g, 1, 0) > 0 ? Product::trovaPrezzoSpeciale($id_product_g, 1, 0) : ($_POST['unitario'][$id_product_g] == 0 ? $_POST['nuovo_price'][$id_product_g] : $_POST['unitario'][$id_product_g])))).",
							
							'".$_POST['ean13'][$id_product_g]."','".Db::getInstance()->getValue("SELECT reference FROM product WHERE id_product = ".$id_product_g."")."','".Db::getInstance()->getValue("SELECT supplier_reference FROM product WHERE id_product = ".$id_product_g."")."','".Db::getInstance()->getValue("SELECT id_category_default FROM product WHERE id_product = ".$id_product_g."")."', '".Db::getInstance()->getValue("SELECT id_manufacturer FROM product WHERE id_product = ".$id_product_g."")."', '','".addslashes($_POST['tax_name'])."','".Tax::getProductTaxRate((int)$id_product_g, Configuration::get('PS_TAX_ADDRESS_TYPE'))."', '0_0')";
							
							//servirebbe ad aggiornare lo stock, ma si dovrebbe vincolare ad uno stato. Attualmete lo disabilito
								Db::getInstance()->executeS("update  ". _DB_PREFIX_."product set quantity=".$qty_difference." where id_product=".$_POST['nuovo_prodotto'][$id_product_g]);
								
							//echo $query;
							echo $query."*";
							
							Db::getInstance()->executeS($query);
							
							AdminOrders::update_total($id_order_g);
						
					}
					$messaggio .= $messaggio_aggiunti;
					if(sizeof($i_aggiunti) > 0)
					{
						Customer::Storico(Tools::getValue('id_order'), 'O', $cookie->id_employee, 'Ha aggiunto prodotti:' .$messaggio_aggiunti);
					}
					
					
					$i_modificati = 0;

					if ($_POST['product_price']) {
					
						foreach ($_POST['product_price'] as $id_order_detail=>$price_product) {
							
							$id_prodotto_modificato = Db::getInstance()->getValue("SELECT product_id FROM order_detail WHERE id_order_detail = ".$id_order_detail."");
							$qty_difference=$_POST['product_quantity_old'][$id_order_detail]-$_POST['product_quantity'][$id_order_detail];
							$stock=max(0,$_POST['product_stock'][$id_order_detail]+$qty_difference);
							$name=addslashes($_POST['product_name'][$id_order_detail]);
							$price_product = str_replace(',', '.', $price_product);

							$reduction_percent = str_replace(',', '.', $_POST['product_reduction'][$id_order_detail]);
							$wholesale_price = str_replace(',', '.', $_POST['wholesale_price'][$id_prodotto_modificato]);
							
						
							$vecchio_prezzo = Db::getInstance()->getValue("SELECT product_price FROM order_detail WHERE id_order_detail = ".$id_order_detail."");
							$vecchio_nome = Db::getInstance()->getValue("SELECT product_name FROM order_detail WHERE id_order_detail = ".$id_order_detail."");
							$vecchio_sconto = Db::getInstance()->getValue("SELECT reduction_percent FROM order_detail WHERE id_order_detail = ".$id_order_detail."");
							
							$vecchio_sconto_t = Db::getInstance()->getValue("SELECT reduction_amount FROM order_detail WHERE id_order_detail = ".$id_order_detail."");
							
							$vecchio_prezzo = $vecchio_prezzo - (($vecchio_prezzo*$vecchio_sconto)/100);
							
							if($vecchio_sconto_t != 0)
							{
								$vecchio_prezzo = $vecchio_prezzo - $vecchio_sconto_t;
							}
							
							$vecchia_quanti = Db::getInstance()->getValue("SELECT product_quantity FROM order_detail WHERE id_order_detail = ".$id_order_detail."");
							if(number_format($vecchio_prezzo,2,",","") != number_format($price_product,2,",","") || $vecchio_sconto != $reduction_percent || $vecchia_quanti!= $_POST['product_quantity'][$id_order_detail]) {
								$i_modificati++;
							}
							if($i_modificati == 1) {
								$messaggio .= "<br /><br />Prodotti modificati:<br /><br /> ";
								$i_modificati++;
							}
							
							if ($_POST['product_delete'][$id_order_detail]) {
							}
							else {
								if(number_format($vecchio_prezzo,2,",","") != number_format($price_product,2,",","")) {
								
									$messaggio .= $name." (".Db::getInstance()->getValue('SELECT reference FROM product WHERE id_product = '.$id_prodotto_modificato.'').") - Vecchio prezzo: ".number_format($vecchio_prezzo,2,",","")." / Nuovo prezzo: ".number_format($price_product,2,",","")."<br />"; 
								
								}
								if($vecchio_sconto != $reduction_percent) {
								
									//$messaggio .= $name." - Vecchio sconto: ".$vecchio_sconto." / Nuovo sconto: ".$reduction_percent."<br />"; 
								
								}
								
								if($vecchia_quanti!= $_POST['product_quantity'][$id_order_detail]) {
								
									$messaggio .= $name." - Vecchia quantita: ".$vecchia_quanti." / Nuova quantita: ".$_POST['product_quantity'][$id_order_detail]."<br />"; 
								
								}
							}
							
							Db::getInstance()->executeS("update  ". _DB_PREFIX_."order_detail set tax_rate=".Tax::getProductTaxRate((int)$id_prodotto_modificato, Configuration::get('PS_TAX_ADDRESS_TYPE'))." where id_order_detail=".$id_order_detail);
							
							Db::getInstance()->executeS("update  ". _DB_PREFIX_."order_detail set wholesale_price=".$wholesale_price." ".((int)$wholesale_price == 0 ? ', no_acq = 1' : ', no_acq = 0')." where id_order_detail=".$id_order_detail);
					
							if((number_format($vecchio_prezzo,2,",","") != number_format($price_product,2,",","")) || ($vecchia_quanti!= $_POST['product_quantity'][$id_order_detail]) || $vecchio_nome != $_POST['product_name'][$id_order_detail]) {
							
								Db::getInstance()->executeS("update  ". _DB_PREFIX_."order_detail set product_price=".$price_product.", reduction_amount=0 where id_order_detail=".$id_order_detail);
								
								Db::getInstance()->executeS("update  ". _DB_PREFIX_."order_detail set reduction_percent=0 where id_order_detail=".$id_order_detail);
								Db::getInstance()->executeS("update  ". _DB_PREFIX_."order_detail set product_quantity=".$_POST['product_quantity'][$id_order_detail].", product_quantity_in_stock=".$_POST['product_quantity'][$id_order_detail]." where id_order_detail=".$id_order_detail);
								Db::getInstance()->executeS("update  ". _DB_PREFIX_."order_detail set product_name='".$_POST['product_name'][$id_order_detail]."' where id_order_detail=".$id_order_detail);
							

								//servirebbe ad aggiornare lo stock, ma si dovrebbe vincolare ad uno stato. Attualmete lo disabilito
								//Db::getInstance()->executeS("update  ". _DB_PREFIX_."product set quantity=".$stock." where id_product=".$_POST['product_id'][$id_order_detail]);

								$total_products+=$_POST['product_quantity'][$id_order_detail]*(AdminOrders::price($price_product)-(AdminOrders::price($price_product)*($reduction_percent)/100));
							
							}
						}
					
						$total_products=$total_products*(1+$_POST['tax_rate']/100);
					}	
					
					if(sizeof($i_modificati) > 0)
					{
						Customer::Storico(Tools::getValue('id_order'), 'O', $cookie->id_employee, 'Ha modificato alcuni prodotti');
					}
					
					AdminOrders::update_total($_POST['id_order']);
						
					$nuovo_ordine = Db::getInstance()->executeS('SELECT * FROM order_detail WHERE id_order = '.Tools::getValue('id_order').'');
					$no_total = 0;
					$msg_nuovo_ordine = '<strong>Nuovo ordine</strong> (tipo pagamento: '.$nuovo_pagamento.')<br /><table cellpadding="0" cellspacing="0"><tr style="background-color: #db6906;; text-transform: uppercase;"><th style="width:350px">Codice eSolver</th><th style="width:100px">Qt.</th><th style="width:100px">Unitario</th><th style="width:70px">Totale</th></tr>';
					foreach($nuovo_ordine as $no)
					{
						
						$msg_nuovo_ordine .= '<tr><td style="border:1px solid #ebebeb">'.$no['product_reference'].'</td>
						<td style="text-align:right; border:1px solid #ebebeb">'.$no['product_quantity'].'</td>
						<td style="text-align:right; border:1px solid #ebebeb">'.Tools::displayPrice(($no['product_price'] - (($no['product_price']*$no['reduction_percent']) / 100 )),1).'</td>
						<td style="text-align:right; border:1px solid #ebebeb">'.Tools::displayPrice((($no['product_price'] - (($no['product_price']*$no['reduction_percent']) / 100 )) * $no['product_quantity']),1).'</td>
						</tr>';
						
						$no_total += (($no['product_price'] - (($no['product_price']*$no['reduction_percent']) / 100 )) * $no['product_quantity']);
						
					}
					$no_total += str_replace(",",".",$_POST['transport']);
					
					$msg_nuovo_ordine .= '
					<tr><td style="border:1px solid #ebebeb">Trasporto</td><td style="border:1px solid #ebebeb"></td><td style="border:1px solid #ebebeb"></td><td  style="border:1px solid #ebebeb; text-align:right">'.Tools::displayPrice(str_replace(",",".",$_POST['transport']),1).'</td></tr>
					<tr><td style="border:1px solid #ebebeb">Totale</td><td style="border:1px solid #ebebeb"></td><td style="border:1px solid #ebebeb"></td><td  style="border:1px solid #ebebeb; text-align:right">'.Tools::displayPrice($no_total,1).'</td></tr></table><br /><br />';
					
					$messaggio.= $msg_vecchio_ordine.$msg_nuovo_ordine;
					$messaggio .= '<strong>Differenza:</strong> '.Tools::displayPrice(($no_total-$vo_total),1);
					
					$messaggio = str_replace('€','',$messaggio);
					
					if($messaggio == "") {
					}
					else {
						$headers  = 'MIME-Version: 1.0' . "\n";
						$headers .= 'Content-Type: text/html' ."\n";
						$headers .= 'Charset: iso-8859-1' ."\n";
						$headers .= 'From: "Ezdirect" <info@ezdirect.it>' . "\n";
							
						
						mail("barbara.giorgini@ezdirect.it", "Modificato ordine ".$_POST['id_order']."", "Attenzione ordine modificato. <br /><br />".$messaggio, $headers);
						
						mail("valentina.vatteroni@ezdirect.it", "Modificato ordine ".$_POST['id_order']."", "Attenzione ordine modificato. <br /><br />".$messaggio, $headers);
							
						mail("matteo.delgiudice@ezdirect.it", "Modificato ordine ".$_POST['id_order']."", "Attenzione ordine modificato. <br /><br />".$messaggio, $headers);
						
						/*
						mail("federico.giannini@mail.com", "Modificato ordine ".$_POST['id_order']."", "Attenzione ordine modificato. <br /><br />".$messaggio, $headers);
						*/
						$action_ordine_modificato = Db::getInstance()->getValue("SELECT id_action FROM action_thread WHERE subject = '*** MODIFICATO ORDINE N. ".$order->id." '");

						if($action_ordine_modificato > 0) 
						{ 
							Db::getInstance()->ExecuteS("UPDATE action_thread SET status = 'open' WHERE id_action = '$action_thread', date_upd = '".date("Y-m-d H:i:s")."'");   
							
							Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', ".$order->id_customer.", ".$cookie->id_employee.", 7, 7, '', '".addslashes($action_subject.'<br /><br />'.addslashes($messaggio))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
						}
						else
						{	
							$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
							$action_thread++;
							
							$action_subject = "*** MODIFICATO ORDINE N. ".$order->id." ";
							
							/*Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd) VALUES ('$action_thread', 'Attivita', '".$order->id_customer."', '".addslashes($action_subject)."', ".$cookie->id_employee.", 7, 'open', 'O".$order->id."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");   

							Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', ".$order->id_customer.", ".$cookie->id_employee.", 7, 7, '', '".addslashes($action_subject.'<br /><br />'.addslashes($messaggio))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')"); */ 
							
						}
							
					}
					
					$cart_id = Db::getInstance()->getValue('SELECT id_cart FROM orders WHERE id_order = '.$_POST['id_order'].'');
					
					
					
					$cart_a = new Cart($cart_id);	
					$products_a = Db::getInstance()->executeS('SELECT * FROM order_detail WHERE id_order = '.$_POST['id_order'].'');
					
					
					foreach ($products_a AS $key => $product)
					{
						$tot_assistenza = Db::getInstance()->getValue("SELECT count(*) AS tot_assistenza FROM product WHERE id_product = ".$product['product_id']." AND (reference LIKE '%astec%' OR reference LIKE '%inst%')");
						if($tot_assistenza == 1) {
							
							$tokenmassimo = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."4");
							$linkmassimo = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenmassimo.'';
							
							$tokenlorenzo = Tools::getAdminToken("AdminOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."12");
							$linklorenzo = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$order->id."&vieworder&token=".$tokenlorenzo.'';
							
							$tokenpaolo = Tools::getAdminToken("AdminCustomerOrders".(int)(Tab::getIdFromClassName('AdminOrders'))."5");
							$linkpaolo = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminOrders&id_order=".$_POST['id_order']."&vieworder&token=".$tokenpaolo.'';
							$testomail = "Modificato un ordine con assistenza. L'id dell'ordine è ".$_POST['id_order'].". 
							".$messaggio."<br />
							Clicca sul link per entrare nell'ordine:<br />
							Massimo - <a href='".$linkmassimo."'>clicca qui</a><br />
							Paolo - <a href='".$linkpaolo."'>clicca qui</a><br />
							Lorenzo - <a href='".$linklorenzo."'>clicca qui</a><br />
							";
							$params = array('{reply}' => $testomail);
							Mail::Send(5, 'action', Mail::l('Ordine con assistenza modificato', 5), $params, "ordiniweb@ezdirect.it", NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true);
							
						}
						else {
							
						}
					
					}
					
					if(Tools::getIsset('si_csv'))
						Product::RecuperaCSV($_POST['id_order']);
				}



			}		
		
			Tools::redirectAdmin($currentIndex.'&id_order='.$_POST['id_order'].(isset($_GET['id_customer']) ? '&id_customer='.$_GET['id_customer'].'&viewcustomer&tab-container-1=4' : '').'&conf=4&vieworder'.'&token='.$tokenCustomers.'#modifica-ordine');
		
		
		
		}
		
		
		//echo '<br /><br /><a href="'.$currentIndex.'&token='.$this->token.'"><img src="../img/admin/arrow2.gif" /> '.AdminOrders::lx('Back to list').'</a><br />';
	}
	
	public function update_total($id_order) {
		
		$id_address_order = Db::getInstance()->getValue('SELECT id_address_delivery FROM orders WHERE id_order = '.$id_order);
		$delivery_country = Db::getInstance()->getValue('SELECT id_country FROM address WHERE id_address = '.$id_address_order);
		$id_customer_order = Db::getInstance()->getValue('SELECT id_customer FROM orders WHERE id_order = '.$id_order);
		$customer_order = new Customer($id_customer_order);
		
		if($delivery_country != 10 && $customer_order->is_company == 1 || ($delivery_country == 19 && $customer_order->is_company == 0))
		{
			Db::getInstance()->execute('UPDATE order_tax SET tax_name = "", tax_rate = 0, amount = 0 WHERE id_order = '.$id_order);
			Db::getInstance()->execute('UPDATE order_detail SET tax_name = "", tax_rate = 0 WHERE id_order = '.$id_order);
		}
				
				
		$query="select 
		truncate(sum((product_price-(CASE WHEN reduction_amount > 0 THEN (reduction_amount) ELSE ((product_price*reduction_percent)/100) END))*product_quantity),2) as total_products,
		
		(1+tax_rate/100) as tax_rate_p,(sum((product_price-(CASE WHEN reduction_amount > 0 THEN (reduction_amount) ELSE ((product_price*reduction_percent)/100) END))*product_quantity)) as total_products_notax  from  order_detail where id_order=".$id_order." group by id_order";
		
		$products=Db::getInstance()->getRow($query);
		$query="select * from  ". _DB_PREFIX_."orders where id_order=".$id_order;
		
		$order=Db::getInstance()->getRow($query);
		
		$gruppo = Db::getInstance()->getValue('SELECT id_default_group FROM customer WHERE id_customer = '.$order['id_customer']);
		$prezzi_carrello = Db::getInstance()->getValue('SELECT prezzi_carrello FROM cart WHERE id_cart = '.$order['id_cart']);
		
		$total=((round($products['total_products_notax'],2)*(round($products['tax_rate_p'],2)))+round($order['total_shipping'],2)+round($order['total_commissions'],2))-(round($order['total_discounts'],2)*(round($products['tax_rate_p'],2)));
		
		$payment_method_cart = $order['payment'];
		
		$sconti_extra = Db::getInstance()->getValue('SELECT id_cart FROM cart_product WHERE id_cart = '.$order['id_cart'].' AND sconto_extra > 0');
		$ex_riv = Db::getInstance()->getValue("SELECT ex_rivenditore FROM customer WHERE id_customer = ".$order['id_customer']."");
		if($sconti_extra > 0)
			$discountz = 1;
		else
			$discountz = 0;
		
		/*
		if($order['module'] == 'gestpay')
		{
			 if($gruppo == 3 || ($gruppo == 15  && $discountz == 1) || $gruppo == 11 || $gruppo == 12 || ($ex_riv == 1 && $discountz == 1) || $prezzi_carrello == 15 || $prezzi_carrello == 3)
				$commissioni = ($total / $products['tax_rate_p']) / 100;
			else
				$commissioni = 0;
		}
		else if(($payment_method_cart == 'Bonifico 30 gg. fine mese' || $payment_method_cart == 'Bonifico 60 gg. fine mese' || $payment_method_cart == 'Bonifico 90 gg. fine mese' || $payment_method_cart == 'R.B. 30 GG. D.F. F.M.' || $payment_method_cart == 'R.B. 60 GG. D.F. F.M.' || $payment_method_cart == 'R.B. 90 GG. D.F. F.M.' || $payment_method_cart == 'R.B. 30 GG. 5 mese successivo' || $payment_method_cart == 'R.B. 30 GG. 10 mese successivo' || $payment_method_cart == 'R.B. 60 GG. 5 mese successivo' || $payment_method_cart == 'R.B. 60 GG. 10 mese successivo') )
		{
			$commissioni = 0;
			
			if($tax_regime == 0 || $tax_regime == 4) {
				$id_tax = 1;
			}
			else {
				$id_tax = $tax_regime;
			}
			$tax_ratez = Db::getInstance()->getValue("SELECT rate FROM tax WHERE id_tax = ".$id_tax."");
			
			if($tax_regime == 1) {
				$tax_ratez = 0;
			}
			else {
			}
			if($gruppo == 3 || ($gruppo == 15  && $discountz == 1) || $gruppo == 11 || $gruppo == 12 || ($ex_riv == 1 && $discountz == 1) || $prezzi_carrello == 15 || $prezzi_carrello == 3)
			  {
				
				$commissioni = (($total  / $products['tax_rate_p']) / 100) * 1.5;
				$commissioni_ti = $commissioni + (($commissioni * $tax_ratez)/100);
			  }
			  else
			  {
				  $commissioni = 0;
				  $commissioni_ti = 0;
			  }
			  
			  $no_addebito_commissioni = Db::getInstance()->getValue("SELECT no_addebito_commissioni FROM cart WHERE id_cart = ".$order['id_cart']." AND created_by != 0");
  
			$no_addebito_commissioni_2 = Db::getInstance()->getValue("SELECT no_addebito_commissioni FROM customer_amministrazione WHERE id_customer = ".$order['id_customer']."");
  
			if($no_addebito_commissioni_2 == 1)
				$no_addebito_commissioni = 1;
  
		  if( $no_addebito_commissioni == 1)
		  {
			  $commissioni = 0;
			  $commissioni_ti = 0;
		  }
		  
		  if(!Tools::getIsset('addebito_commissioni'))
		  {
			  $commissioni = 0;
			  $commissioni_ti = 0;
		  }
		}
		else
		{
			$commissioni = 0;
		}
		*/
		
		$commissioni = $order['total_commissions'];
		
		$total=((round($products['total_products_notax'],2)*(round($products['tax_rate_p'],2)))+(round($commissioni,2)*(round($products['tax_rate_p'],2)))+round($order['total_shipping'],2))-(round($order['total_discounts'],2)*(round($products['tax_rate_p'],2)));
		
		$query="update  ". _DB_PREFIX_."orders set ";
		$query.=" total_discounts=".$order['total_discounts'];
		$query.=" ,total_commissions=".$order['total_commissions'];
		$query.=" ,total_wrapping=".$order['total_wrapping'];
		$query.=" ,total_shipping=".$order['total_shipping'];
		$query.=" ,total_products=".$products['total_products_notax'];
		$query.=" ,total_paid_real=".$total;
		$query.=" ,total_paid=".$total;
		$query.=" where id_order=".$id_order;
		$query.=" limit 1";
		
		Db::getInstance()->executeS($query);
	}
	
	public function price($price) {
		$price=str_replace(",",".",$price);
		return $price;
	}

	public function displayAddressDetail($addressDelivery)
	{
		// Allow to add specific rules
		$patternRules = array(
			'avoid' => array()
			//'avoid' => array('address2')
		);
		return AddressFormat::generateAddress($addressDelivery, $patternRules, '<br />');
	}

	public function display()
	{
		global $cookie;

		if (isset($_GET['view'.$this->table]))
			$this->viewDetails();
		else
		{
			echo '<script type="text/javascript">
			function updSpringOrder(id)
			{
				var updSpringOrder="no";
				if(document.getElementById("spring_"+id+"").checked == true)
					updSpringOrder="yes";
				else
					updSpringOrder="no";
				
				$.ajax({
				  url:"ajax.php?updSpringOrder="+updSpringOrder,
				  type: "POST",
				  data: { id_order: id
				  },
				  success:function(r){
					// alert(r);
				  },
				  error: function(xhr,stato,errori){
					 alert("Errore durante l\'operazione:"+xhr.status);
				  }
				});
				
				
			}</script>
			
			<script type="text/javascript">
			function updCheckPI(id)
			{
				var updCheckPI="no";
				if(document.getElementById("check_pi_"+id+"").checked == true)
					updCheckPI="yes";
				else
					updCheckPI="no";
				
				$.ajax({
				  url:"ajax.php?updCheckPI="+updCheckPI,
				  type: "POST",
				  data: { id_order: id
				  },
				  success:function(r){
					// alert(r);
				  },
				  error: function(xhr,stato,errori){
					 alert("Errore durante l\'operazione:"+xhr.status);
				  }
				});
				
				
			}
			
			function updPagAmzRic(id)
			{
				var updPagAmzRic="no";
				if(document.getElementById("pag_amz_ric_"+id+"").checked == true)
					updPagAmzRic="yes";
				else
					updPagAmzRic="no";
				
				$.ajax({
				  url:"ajax.php?updPagAmzRic="+updPagAmzRic,
				  type: "POST",
				  data: { id_order: id
				  },
				  success:function(r){
					// alert(r);
				  },
				  error: function(xhr,stato,errori){
					 alert("Errore durante l\'operazione:"+xhr.status);
				  }
				});
				
				
			}
			
			function updCheckEsolv(id)
				{
					var updCheckEsolv="no";
					if(document.getElementById("check_esolver_"+id+"").checked == true)
						updCheckEsolv="yes";
					else
						updCheckEsolv="no";
					
					$.ajax({
					  url:"ajax.php?updCheckEsolv="+updCheckEsolv,
					  type: "POST",
					  data: { id_order: id
					  },
					  success:function(r){
						// alert(r);
					  },
					  error: function(xhr,stato,errori){
						 alert("Errore durante l\'operazione:"+xhr.status);
					  }
					});
					
					
				}
			</script>
			
			';
				
			$this->getList((int)($cookie->id_lang), !Tools::getValue($this->table.'Orderby') ? 'date_add' : NULL, !Tools::getValue($this->table.'Orderway') ? 'DESC' : NULL);
			$currency = new Currency((int)(Configuration::get('PS_CURRENCY_DEFAULT')));
			$this->displayListHeader();
			$this->displayListContent();
			
			echo "</tbody></table>
			<div id='bottom_anchor'>
			</div>";
			
			echo '<h2 class="space" style="text-align:right; margin-right:44px;">'.AdminOrders::lx('Totale:').' '.Tools::displayPrice($this->getTotal(), $currency).'</h2>';
			
			$this->displayListPagination($this->token);
			echo '<p><a class="button" href="http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'&esportaexcel=y&startn='.($_POST['submitFilter'.$this->table] - 1) * ($_POST['submitFilter'.$this->table] == 0 ? 0 : ($cookie->{$this->table.'_pagination'})).'">Clicca qui per scaricare la lista in formato Excel</a></p>';
			
		}
		
		if(isset($cookie->_whereorder)) {
				$prodottocercato = Db::getInstance()->getRow('SELECT name, reference FROM product p JOIN product_lang pl ON p.id_product = pl.id_product WHERE pl.id_lang = '.$cookie->id_lang.' AND p.id_product = '.$cookie->_cercaordineprodotto);
				//echo "<strong>Stai vedendo la lista degli ordini che contengono: ".$prodottocercato['name']." (".$prodottocercato['reference'].")</strong>";
				echo "<br />";
			
			}
			
	}

	private function getTotal()
	{
		$total = 0;
		foreach($this->_list AS $item)
			if ($item['id_currency'] == Configuration::get('PS_CURRENCY_DEFAULT'))
				$total += (float)($item['total_paid']);
			else
			{
				$currency = new Currency((int)($item['id_currency']));
				$total += Tools::ps_round((float)($item['total_paid']) / (float)($currency->conversion_rate), 2);
			}
		return $total;
	}
	
	public function lx($string, $class = 'AdminOrders', $addslashes = FALSE, $htmlentities = TRUE)
	{
		// if the class is extended by a module, use modules/[module_name]/xx.php lang file
		$currentClass = 'AdminOrders';
		if (Module::getModuleNameFromClass($currentClass))
		{
			$string = str_replace('\'', '\\\'', $string);
			return Module::findTranslation(Module::$classInModule[$currentClass], $string, $currentClass);
		}
		global $_LANGADM;

		if ($class == __CLASS__)
				$class = 'AdminOrders';

		$key = md5(str_replace('\'', '\\\'', $string));
		$str = (key_exists('AdminOrders'.$key, $_LANGADM)) ? $_LANGADM[get_class($this).$key] : ((key_exists($class.$key, $_LANGADM)) ? $_LANGADM[$class.$key] : $string);
		$str = $htmlentities ? htmlentities($str, ENT_QUOTES, 'utf-8') : $str;
		return str_replace('"', '&quot;', ($addslashes ? addslashes($str) : stripslashes($str)));
	}
	
	public function displayListContent($token = NULL)
	{
		/* Display results in a table
		 *
		 * align  : determine value alignment
		 * prefix : displayed before value
		 * suffix : displayed after value
		 * image  : object image
		 * icon   : icon determined by values
		 * active : allow to toggle status
		 */

		global $currentIndex, $cookie;
		$currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));

		$id_category = 1; // default categ

		$irow = 0;
		if ($this->_list AND isset($this->fieldsDisplay['position']))
		{
			$positions = array_map(create_function('$elem', 'return (int)($elem[\'position\']);'), $this->_list);
			sort($positions);
		}
		if ($this->_list)
		{
			$isCms = false;
			if (preg_match('/cms/Ui', $this->identifier))
				$isCms = true;
			$keyToGet = 'id_'.($isCms ? 'cms_' : '').'category'.(in_array($this->identifier, array('id_category', 'id_cms_category')) ? '_parent' : '');
			$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
			
			foreach ($this->_list AS $tr)
			{
				$id = $tr[$this->identifier];
				$amazon_exists = Db::getInstance()->getValue("SELECT count(id_order) FROM amazon_orders WHERE id_order = '".$id."'");
				$eprice_exists = Db::getInstance()->getValue("SELECT count(id_order) FROM eprice_orders WHERE id_order = '".$id."'");
				
				$order = new Order($id);
				$cart_name = Db::getInstance()->getValue('SELECT name FROM cart WHERE id_cart = '.$order->id_cart);
		
				if($amazon_exists > 0 && (strpos('AMAZON ID',$cart_name !== false)))
					$tr['color'] = '#000000';
				else if($eprice_exists > 0)
					$tr['color'] = '#ffff00';
					
				echo '<tr'.(array_key_exists($this->identifier,$this->identifiersDnd) ? ' id="tr_'.(($id_category = (int)(Tools::getValue('id_'.($isCms ? 'cms_' : '').'category', '1'))) ? $id_category : '').'_'.$id.'_'.$tr['position'].'"' : '').($irow++ % 2 ? ' class="alt_row"' : '').' '.((isset($tr['color']) AND $this->colorOnBackground) ? 'style="background-color: '.$tr['color'].'"' : '').'>
							<td class="center">';
				if ($this->delete AND (!isset($this->_listSkipDelete) OR !in_array($id, $this->_listSkipDelete)))
					echo '<input type="checkbox" name="'.$this->table.'Box[]" value="'.$id.'" class="noborder" />';
				echo '</td>';
				foreach ($this->fieldsDisplay AS $key => $params)
				{
					$tmp = explode('!', $key);
					$key = isset($tmp[1]) ? $tmp[1] : $tmp[0];
					echo '
					<td '.($tr['color'] == '#000000' ? 'style="color:#ffffff"' : '').' '.(isset($params['position']) ? ' id="td_'.(isset($id_category) AND $id_category ? $id_category : 0).'_'.$id.'"' : '').' class="'.((!isset($this->noLink) OR !$this->noLink) ? 'pointer' : '').((isset($params['position']) AND $this->_orderBy == 'position')? ' dragHandle' : ''). (isset($params['align']) ? ' '.$params['align'] : '').'" ';
					if (!isset($params['position']) AND $key != 'spring'  AND $key != 'check_esolver'  AND $key != 'pag_amz_ric'  AND $key != 'check_pi' AND (!isset($this->noLink) OR !$this->noLink))
						echo ' onclick="document.location = \'index.php?tab=AdminCustomers&id_customer='.$tr['id_customer'].'&viewcustomer&'.$this->identifier.'='.$id.($this->view? '&view' : '&update').$this->table.'&tab-container-1=4&token='.$tokenCustomers.'\'">'.(isset($params['prefix']) ? $params['prefix'] : '');
					else
						echo '>';
					if (isset($params['active']) AND isset($tr[$key]))
						$this->_displayEnableLink($token, $id, $tr[$key], $params['active'], Tools::getValue('id_category'), Tools::getValue('id_product'));
					elseif (isset($params['activeVisu']) AND isset($tr[$key]))
						echo '<img src="../img/admin/'.($tr[$key] ? 'enabled.gif' : 'disabled.gif').'"
						alt="'.($tr[$key] ? AdminOrders::lx('Enabled') : AdminOrders::lx('Disabled')).'" title="'.($tr[$key] ? AdminOrders::lx('Enabled') : AdminOrders::lx('Disabled')).'" />';
					elseif (isset($params['position']))
					{
						if ($this->_orderBy == 'position' AND $this->_orderWay != 'DESC')
						{
							echo '<a'.(!($tr[$key] != $positions[sizeof($positions) - 1]) ? ' style="display: none;"' : '').' href="'.$currentIndex.
									'&'.$keyToGet.'='.(int)($id_category).'&'.$this->identifiersDnd[$this->identifier].'='.$id.'
									&way=1&position='.(int)($tr['position'] + 1).'&token='.($token!=NULL ? $token : $this->token).'">
									<img src="../img/admin/'.($this->_orderWay == 'ASC' ? 'down' : 'up').'.gif"
									alt="'.AdminOrders::lx('Down').'" title="'.AdminOrders::lx('Down').'" /></a>';

							echo '<a'.(!($tr[$key] != $positions[0]) ? ' style="display: none;"' : '').' href="'.$currentIndex.
									'&'.$keyToGet.'='.(int)($id_category).'&'.$this->identifiersDnd[$this->identifier].'='.$id.'
									&way=0&position='.(int)($tr['position'] - 1).'&token='.($token!=NULL ? $token : $this->token).'">
									<img src="../img/admin/'.($this->_orderWay == 'ASC' ? 'up' : 'down').'.gif"
									alt="'.AdminOrders::lx('Up').'" title="'.AdminOrders::lx('Up').'" /></a>';						}
						else
							echo (int)($tr[$key] + 1);
					}
					elseif (isset($params['image']))
					{
						// item_id is the product id in a product image context, else it is the image id.
						$item_id = isset($params['image_id']) ? $tr[$params['image_id']] : $id;
						// If it's a product image
						if (isset($tr['id_image']))
						{
							$image = new Image((int)$tr['id_image']);
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$image->getExistingImgPath().'.'.$this->imageType;
						}else
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$item_id.(isset($tr['id_image']) ? '-'.(int)($tr['id_image']) : '').'.'.$this->imageType;

						echo cacheImage($path_to_image, $this->table.'_mini_'.$item_id.'.'.$this->imageType, 45, $this->imageType);
					}
					elseif (isset($params['icon']) AND (isset($params['icon'][$tr[$key]]) OR isset($params['icon']['default'])))
						echo '<img src="../img/admin/'.(isset($params['icon'][$tr[$key]]) ? $params['icon'][$tr[$key]] : $params['icon']['default'].'" alt="'.$tr[$key]).'" title="'.$tr[$key].'" />';
					elseif (isset($params['price']))
						echo (number_format($tr[$key],2,",",""));
					elseif (isset($params['float']))
						echo rtrim(rtrim($tr[$key], '0'), '.');
					elseif (isset($params['type']) AND $params['type'] == 'date')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang);
					elseif (isset($params['type']) AND $params['type'] == 'datetime')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang, true);
					elseif (isset($tr[$key]))
					{
						$verificato_da = Db::getInstance()->getValue('select verificato_da from orders where id_order = '.$id);
						
						if($key == 'spring')
							echo '<input type="checkbox" name="spring_'.$id.'" id="spring_'.$id.'" '.($tr[$key] == 1 ? 'checked="checked"' : '').' onchange="updSpringOrder('.$id.'); return false;" />'.($verificato_da != '' ? Db::getInstance()->getValue('SELECT concat(LEFT(firstname,1),"",LEFT(lastname,1)) FROM employee WHERE id_employee = '.$verificato_da) : '');
						else if($key == 'check_pi')
							echo '<input type="checkbox" name="check_pi_'.$id.'" id="check_pi_'.$id.'" '.($tr[$key] == 1 ? 'checked="checked"' : '').' onchange="updCheckPI('.$id.'); return false;" />';
						else if($key == 'check_esolver')
							echo '<input type="checkbox" name="check_esolver_'.$id.'" id="check_esolver_'.$id.'" '.($tr[$key] == 1 ? 'checked="checked"' : '').' onchange="updCheckEsolv('.$id.'); return false;" />';
						else if($key == 'pag_amz_ric')
							echo '<input type="checkbox" name="pag_amz_ric_'.$id.'" id="pag_amz_ric_'.$id.'" '.($tr[$key] == 1 ? 'checked="checked"' : '').' onchange="updPagAmzRic('.$id.'); return false;" />';
						else
						{	
							$echo = ($key == 'price' ? round($tr[$key], 2) : isset($params['maxlength']) ? Tools::substr($tr[$key], 0, $params['maxlength']).'...' : $tr[$key]);
							echo isset($params['callback']) ? call_user_func_array(array($this->className, $params['callback']), array($echo, $tr)) : $echo;
						}
					}
					else
						echo '--';

					echo (isset($params['suffix']) ? $params['suffix'] : '').
					'</td>';
				}

				if ($this->edit OR $this->delete OR ($this->view AND $this->view !== 'noActionColumn'))
				{
					echo '<td class="center" style="white-space: nowrap;">';
					if ($this->view)
						$this->_displayViewLink($token, $id);
					if ($this->edit)
						$this->_displayEditLink($token, $id);
					if ($this->delete AND (!isset($this->_listSkipDelete) OR !in_array($id, $this->_listSkipDelete)))
						$this->_displayDeleteLink($token, $id);
					if ($this->duplicate)
						$this->_displayDuplicate($token, $id);
					echo '</td>';
				}
				echo '</tr>';
			}
		}
	}
	
	public function getList($id_lang, $orderBy = NULL, $orderWay = NULL, $start = 0, $limit = NULL)
	{
		global $cookie;

		/* Manage default params values */
		if (empty($limit))
			$limit = ((!isset($cookie->{$this->table.'_pagination'})) ? $this->_pagination[1] : $limit = $cookie->{$this->table.'_pagination'});

		if (!Validate::isTableOrIdentifier($this->table))
			die (Tools::displayError('Table name is invalid:').' "'.$this->table.'"');

		if (empty($orderBy))
			$orderBy = $cookie->__get($this->table.'Orderby') ? $cookie->__get($this->table.'Orderby') : $this->_defaultOrderBy;
		if (empty($orderWay))
			$orderWay = $cookie->__get($this->table.'Orderway') ? $cookie->__get($this->table.'Orderway') : 'ASC';

		$limit = (int)(Tools::getValue('pagination', $limit));
		$cookie->{$this->table.'_pagination'} = $limit;

		/* Check params validity */
		if (!Validate::isOrderBy($orderBy) OR !Validate::isOrderWay($orderWay)
			OR !is_numeric($start) OR !is_numeric($limit)
			OR !Validate::isUnsignedId($id_lang))
			die(Tools::displayError('get list params is not valid'));

		/* Determine offset from current page */
		if ((isset($_POST['submitFilter'.$this->table]) OR
		isset($_POST['submitFilter'.$this->table.'_x']) OR
		isset($_POST['submitFilter'.$this->table.'_y'])) AND
		!empty($_POST['submitFilter'.$this->table]) AND
		is_numeric($_POST['submitFilter'.$this->table]))
			$start = (int)($_POST['submitFilter'.$this->table] - 1) * $limit;

		/* Cache */
		$this->_lang = (int)($id_lang);
		$this->_orderBy = $orderBy;
		$this->_orderWay = Tools::strtoupper($orderWay);

		/* SQL table : orders, but class name is Order */
		$sqlTable = $this->table == 'order' ? 'orders' : $this->table;

		/* Query in order to get results with all fields */
		$sql = 'SELECT 
			'.($this->_tmpTableFilter ? ' * FROM (SELECT ' : '').'
			'.($this->lang ? 'b.*, ' : '').'a.*'.(isset($this->_select) ? ', '.$this->_select.' ' : '').'
			FROM `'._DB_PREFIX_.$sqlTable.'` a
			'.($this->lang ? 'LEFT JOIN `'._DB_PREFIX_.$this->table.'_lang` b ON (b.`'.$this->identifier.'` = a.`'.$this->identifier.'` AND b.`id_lang` = '.(int)($id_lang).')' : '').'
			'.(isset($this->_join) ? $this->_join.' ' : '').'
			WHERE 1 '.(isset($this->_where) ? $this->_where.' ' : '').($this->deleted ? 'AND a.`deleted` = 0 ' : '').(isset($this->_filter) ? $this->_filter : '').'
			'.(isset($this->_group) ? $this->_group.' ' : '').'
			'.((isset($this->_filterHaving) || isset($this->_having)) ? 'HAVING ' : '').(isset($this->_filterHaving) ? ltrim($this->_filterHaving, ' AND ') : '').(isset($this->_having) ? $this->_having.' ' : '').'
			ORDER BY '.(($orderBy == $this->identifier) ? 'a.' : '').'`'.pSQL($orderBy).'` '.pSQL($orderWay).
			($this->_tmpTableFilter ? ') tmpTable WHERE 1'.$this->_tmpTableFilter : '').
			($this->_tmpTableFilter ? 'ORDER BY '.(($orderBy == $this->identifier) ? 'a.' : '').'`'.pSQL($orderBy).'` '.pSQL($orderWay) : '');
			
			$sql2 = 'SELECT
			'.($this->_tmpTableFilter ? ' *, count(tmpTable.id_order) total FROM (SELECT ' : '').'
			'.($this->lang ? 'b.*, ' : '').'a.* '.($this->_tmpTableFilter ? ' ' : ', count(a.id_order) total').' '.(isset($this->_select) ? ', '.$this->_select.' ' : '').'
			FROM `'._DB_PREFIX_.$sqlTable.'` a
			'.($this->lang ? 'LEFT JOIN `'._DB_PREFIX_.$this->table.'_lang` b ON (b.`'.$this->identifier.'` = a.`'.$this->identifier.'` AND b.`id_lang` = '.(int)($id_lang).')' : '').'
			'.(isset($this->_join) ? $this->_join.' ' : '').'
			WHERE 1 '.(isset($this->_where) ? $this->_where.' ' : '').($this->deleted ? 'AND a.`deleted` = 0 ' : '').(isset($this->_filter) ? $this->_filter : '').'
			'.(isset($this->_group) ? $this->_group.' ' : '').'
			'.((isset($this->_filterHaving) || isset($this->_having)) ? 'HAVING ' : '').(isset($this->_filterHaving) ? ltrim($this->_filterHaving, ' AND ') : '').(isset($this->_having) ? $this->_having.' ' : '').'
			ORDER BY '.(($orderBy == $this->identifier) ? 'a.' : '').'`'.pSQL($orderBy).'` '.pSQL($orderWay).
			($this->_tmpTableFilter ? ') tmpTable WHERE 1'.$this->_tmpTableFilter : '').
			($this->_tmpTableFilter ? 'ORDER BY '.(($orderBy == $this->identifier) ? 'a.' : '').'`'.pSQL($orderBy).'` '.pSQL($orderWay) : '');
			
			
			$count = Db::getInstance()->getRow($sql2);
		$this->_list = Db::getInstance()->ExecuteS($sql.' LIMIT '.(int)($start).','.(int)($limit));
		$this->_listTotal = $count['total'];
		
		if(isset($_GET['esportaexcel'])) { 
		
			
			$startn = Tools::getValue('startn');
			
			$sqln = 'SELECT SQL_CALC_FOUND_ROWS
			'.($this->_tmpTableFilter ? ' * FROM (SELECT ' : '').'
			'.($this->lang ? 'b.*, ' : '').'a.*'.(isset($this->_select) ? ', '.$this->_select.' ' : '').'
			FROM `'._DB_PREFIX_.$sqlTable.'` a
			'.($this->lang ? 'LEFT JOIN `'._DB_PREFIX_.$this->table.'_lang` b ON (b.`'.$this->identifier.'` = a.`'.$this->identifier.'` AND b.`id_lang` = '.(int)($id_lang).')' : '').'
			'.(isset($this->_join) ? $this->_join.' ' : '').'
			WHERE 1 '.(isset($this->_where) ? $this->_where.' ' : '').($this->deleted ? 'AND a.`deleted` = 0 ' : '').(isset($this->_filter) ? $this->_filter : '').'
			'.(isset($this->_group) ? $this->_group.' ' : '').'
			'.((isset($this->_filterHaving) || isset($this->_having)) ? 'HAVING ' : '').(isset($this->_filterHaving) ? ltrim($this->_filterHaving, ' AND ') : '').(isset($this->_having) ? $this->_having.' ' : '').'
			ORDER BY '.(($orderBy == $this->identifier) ? 'a.' : '').'`'.pSQL($orderBy).'` '.pSQL($orderWay).
			($this->_tmpTableFilter ? ') tmpTable WHERE 1'.$this->_tmpTableFilter : '').
			($this->_tmpTableFilter ? 'ORDER BY '.(($orderBy == $this->identifier) ? 'a.' : '').'`'.pSQL($orderBy).'` '.pSQL($orderWay) : '').' LIMIT '.(int)($startn).','.($cookie->{$this->table.'_pagination'});
			$listn = Db::getInstance()->ExecuteS($sqln);
		
			ini_set("memory_limit","892M");
			set_time_limit(3600);
			require_once 'esportazione-catalogo/Classes/PHPExcel.php';
			$objPHPExcel = new PHPExcel();
			
			$resultsprodotti= Db::getInstance()->ExecuteS(stripslashes($_POST['querysql']));
			
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'ID')
			->setCellValue('B1', 'N. Prev.')
			->setCellValue('C1', 'Cliente')
			->setCellValue('D1', 'Gruppo')
			->setCellValue('E1', 'T. netto')
			->setCellValue('F1', 'Totale')
			->setCellValue('G1', 'Pagamento')
			->setCellValue('H1', 'Stato')
			->setCellValue('I1', 'Data')
			;
				
			$k = 2;

			foreach ($listn as $row) {
				
		
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue("A$k", $row['id_order'])
				->setCellValue("B$k", $row['preventivo'])
				->setCellValue("C$k", $row['customer'])
				->setCellValue("D$k", $row['group'])
				->setCellValue("E$k", number_format($row['total_products'],2,",",""))
				->setCellValue("F$k", number_format($row['total_paid'],2,",",""))
				->setCellValue("G$k", $row['payment'])
				->setCellValue("H$k", $row['osname'])
				->setCellValue("I$k", Tools::displayDate($row['date_add'],5));
		
		
				$objPHPExcel->getActiveSheet()->getStyle("E$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->getActiveSheet()->getStyle("F$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$i++;
				$k++;
			
			}
		
			$objPHPExcel->getActiveSheet()->setTitle('Clienti');

			$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
				
			$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->getStartColor()->setRGB('FFFF00');	

			$objPHPExcel->getActiveSheet()->getStyle("A1:I$k")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFont()->setBold(true);

			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
			
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);

			$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
			$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
			PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

			$data = date("Ymd");

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->setPreCalculateFormulas(false);
			$objWriter->save(str_replace(".php", ".xls", "esportazione-catalogo/catalogo_xls/ordini-lista-$data-$startn.php"));

			$this->_html .= "<div style='visibility:hidden'><br /><br />FILE PRONTO PER IL DOWNLOAD: <a id='scaricaexcelstatistiche' href='http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/ordini-lista-$data.xls'>CLICCA QUI PER SCARICARE</a>!</div>";

			header("Location: http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/ordini-lista-$data-$startn.xls");
				
			
			
		
		}

	}
	
}

