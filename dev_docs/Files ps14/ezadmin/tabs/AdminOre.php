<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14703 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminOre extends AdminTab
{

	public function display()
	{	
	
		global $cookie;
		
		if($cookie->id_employee != 6 && $cookie->id_employee != 22 && $cookie->id_employee != 2 && $cookie->id_employee != 1)
			die('Non hai accesso a questa sezione');
		
		echo '<h1>Ore impiegati</h1>';
		
		
		echo '
		<script src="handsontable.full.js"></script>
		<link rel="stylesheet" media="screen" href="handsontable.full.css">
		
		<style type="text/css">
		.rowHeader {
			width: 250px;
		}
		</style>
		<form method="post">
		<select name="month-ore" style="height:25px">
		<option value="1" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '1' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 1 ? 'selected="selected"' : '') ).'>Gennaio</option>
		<option value="2" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '2' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 2 ? 'selected="selected"' : '') ).'>Febbraio</option>
		<option value="3" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '3' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 3 ? 'selected="selected"' : '') ).'>Marzo</option>
		<option value="4" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '4' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 4 ? 'selected="selected"' : '') ).'>Aprile</option>
		<option value="5" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '5' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 5 ? 'selected="selected"' : '') ).'>Maggio</option>
		<option value="6" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '6' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 6 ? 'selected="selected"' : '') ).'>Giugno</option>
		<option value="7" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '7' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 7 ? 'selected="selected"' : '') ).'>Luglio</option>
		<option value="8" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '8' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 8 ? 'selected="selected"' : '') ).'>Agosto</option>
		<option value="9" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '9' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 9 ? 'selected="selected"' : '') ).'>Settembre</option>
		<option value="10" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '10' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 10 ? 'selected="selected"' : '') ).'>Ottobre</option>
		<option value="11" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '11' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 11 ? 'selected="selected"' : '') ).'>Novembre</option>
		<option value="12" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '12' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 12 ? 'selected="selected"' : '') ).'>Dicembre</option>
		
		
		</select>
		&nbsp;&nbsp;&nbsp;
		<select name="year-ore" style="height:25px">
		<option value="2014" '.(Tools::getIsset('year-ore') && Tools::getValue('year-ore') == '2014' ? 'selected="selected"' : '').'>2014</option>
		<option value="2015"  '.(Tools::getIsset('year-ore') && Tools::getValue('year-ore') == '2015' ? 'selected="selected"' : '').' >2015</option>
		<option value="2016"  '.(Tools::getIsset('year-ore') && Tools::getValue('year-ore') == '2016' ? 'selected="selected"' : '').' >2016</option>
		<option value="2017"  '.(Tools::getIsset('year-ore') && Tools::getValue('year-ore') == '2017' ? 'selected="selected"' : '').'>2017</option>
		
		<option value="2018"  '.(Tools::getIsset('year-ore') && Tools::getValue('year-ore') == '2018' ? 'selected="selected"' : '').' >2018</option>
		
		<option value="2019"  '.(Tools::getIsset('year-ore') && Tools::getValue('year-ore') == '2019' ? 'selected="selected"' : '').' '.(!Tools::getIsset('year-ore') ? 'selected="selected"' : '').'>2019</option>
		<option value="2020"  '.(Tools::getIsset('year-ore') && Tools::getValue('year-ore') == '2020' ? 'selected="selected"' : '').' '.(!Tools::getIsset('year-ore') ? 'selected="selected"' : '').'>2020</option>
		</select>
		&nbsp;&nbsp;&nbsp;
		<input type="submit" value="Seleziona" name="select-month-year" class="button" style="margin-top:-3px; cursor:pointer" />
		
		<br /><br />

		

		';
		
		
		if(Tools::getIsset('year-ore'))
			$year = Tools::getValue('year-ore');
		else
			$year = date("Y");
			
		if(Tools::getIsset('month-ore'))
			$month = Tools::getValue('month-ore');
		else
			$month = date("n");
			
		
		switch($month)
		{
			case 1: $month_name = 'Gennaio'; break; 
			case 2: $month_name = 'Febbraio'; break; 
			case 3: $month_name = 'Marzo'; break; 
			case 4: $month_name = 'Aprile'; break; 
			case 5: $month_name = 'Maggio'; break; 
			case 6: $month_name = 'Giugno'; break; 
			case 7: $month_name = 'Luglio'; break; 
			case 8: $month_name = 'Agosto'; break; 
			case 9: $month_name = 'Settembre'; break; 
			case 10: $month_name = 'Ottobre'; break; 
			case 11: $month_name = 'Novembre'; break; 
			case 12: $month_name = 'Dicembre'; break; 
			default: ''; break; 
		
		
		}
		
		$days_number = cal_days_in_month(CAL_GREGORIAN, $month, $year);
		$days_sequence = '';
		$days_sequence_empty = ',';
		
		
		
		for($i = 1; $i<=$days_number; $i++)
		{
			$days_sequence .= $i.",";
			$days_sequence_empty .= ",";
		}	
		$days_sequence = substr($days_sequence, 0, -1);
		
		echo '<script type="text/javascript">
		$(document).ready(function () {

		  var
			data = [';
			
			
			
			$employees = Db::getInstance()->ExecuteS('
			SELECT `id_employee`, CONCAT(`firstname`, \' \', `lastname`) AS "name", lastname, firstname
			FROM `'._DB_PREFIX_.'employee`
			WHERE id_employee != 6 AND id_employee != 11 AND id_employee != 15  AND id_employee != 16 AND   id_employee != 20 AND id_employee != 3 AND id_employee != 18  AND id_employee != 21 AND id_employee != 23 AND id_employee != 25
			ORDER BY (CASE id_employee WHEN 1 THEN 0 WHEN 2 THEN 1 WHEN 7 THEN 2 WHEN 4 THEN 3 WHEN 3 THEN 4 WHEN 5 THEN 6 WHEN 13 THEN 7 WHEN 12 THEN 8 WHEN 14 THEN 9 WHEN 17 THEN 10 WHEN 18 THEN 11 WHEN 19 THEN 12 WHEN 22 THEN 13 WHEN 24 THEN 14 END)');
			$rowHeaders = "";
			$rowImpiegati = "";
			
			foreach ($employees as $employee) {
				if($employee['id_employee'] == 6 || (($month > 9 && $year >= 2018 || $year >= 2019) && ($employee['id_employee'] == 3 || $employee['id_employee'] == 18)))
				{
				}
				else
				{
						$rowHeaders .= '"ID'.$employee['id_employee'].'",';
					
					$exists = Db::getInstance()->getValue('SELECT count(valori_ore) FROM ore WHERE id_employee = '.$employee['id_employee'].' AND month = '.$month.' AND year = '.$year.'');
					if($exists > 0)
					{
						$valori_ore = Db::getInstance()->getValue('SELECT valori_ore FROM ore WHERE id_employee = '.$employee['id_employee'].' AND month = '.$month.' AND year = '.$year.'');
						$valori_ore = preg_replace('/\*/', '","', $valori_ore);
						
						$valori_ore = '"'.$valori_ore.'"';
						$rowImpiegati .= preg_replace('/"0"/', '""','["'.$employee['lastname'].' '.$employee['firstname'].'",'.$valori_ore.',],');
					}
					
					else {
						$rowImpiegati .= '["'.$employee['lastname'].' '.$employee['firstname'].'"'.$days_sequence_empty.'],';
					}
				}
			
			}
			$rowHeaders = substr($rowHeaders, 0, -1);
			$rowImpiegati = substr($rowImpiegati, 0, -1);
			echo $rowImpiegati.'],';
			echo '
			container = document.getElementById("example"),
			hot;
			
			  
		 var
		$$ = function(id) {
		  return document.getElementById(id);
		},

		save = $$("save");
		  
		  hot = new Handsontable(container, {
			colWidths: [150, 27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27,27],
			data: data,
			minSpareRows: 0,
			colHeaders: ["'.$month_name.' '.$year.'", '.$days_sequence.'],
			//rowHeaders: ['.$rowHeaders.'],
			className: "htLeft",
			manualColumnResize: true,
			manualRowResize: true,
			afterLoadData: function(){
				var rowHeaders = container.querySelectorAll(".rowHeader");
					
					for (var j = 0; j < rowHeaders.length; j++) {
						rowHeaders[j].style.width = "200px";
					}
				
			}
		  });
		
			 Handsontable.Dom.addEvent(save, "click", function (e) {
				';
				$k = 0;
				
				$data_to_save = "";
				$data_to_save_all = "var data_to_save_all = ";
				
				foreach($employees as $employee)
				{
					if($employee['id_employee'] == 6 || ($month > 9 && $year >= 2018 && ($employee['id_employee'] == 3 || $employee['id_employee'] == 18)))
					{	
					}
					else
					{
						$data_to_save .= 'var data_to_save_'.$employee['id_employee'].' = "'.$employee['id_employee'].'|'.$month.'|'.$year.'|" + ';
						for($i = 1; $i<=$days_number; $i++)
							$data_to_save .= 'data['.$k.']['.$i.']+"*"+';
					
						$data_to_save = substr($data_to_save, 0, -5);
						$data_to_save .= "+\":::\";\n";
						$data_to_save_all .= 'data_to_save_'.$employee['id_employee'].' + ';
						$k++;
					}	
				}
				$data_to_save_all .= '"";';
				
				echo $data_to_save;
				echo $data_to_save_all;
				
			 echo '
				
				$("#save_dati").html("<img src=\"../img/loader.gif\" />").show();
				$.post("ajax.php", {submitOre:1,dati_da_salvare:data_to_save_all}, function (r) {
					
					if (r == "ok")
						$("#save_dati").html("<b style=\"color:green\">'.addslashes($this->l('Dati salvati correttamente')).'</b>").fadeIn(400);
					else
						$("#save_dati").html("<b style=\"color:red\">'.addslashes($this->l('Errore: dati non alidi')).'</b>").fadeIn(400);
					
				});
				
				 e.preventDefault(); 
			});
			
		  
		  function bindDumpButton() {
		  
			  Handsontable.Dom.addEvent(document.body, "click", function (e) {
				
				var element = e.target || e.srcElement;
				if (element.nodeName == "BUTTON" && element.name == "dump") {
				  var name = element.getAttribute("data-dump");
				  var instance = element.getAttribute("data-instance");
				  var hot = window[instance];
				  console.log("data of " + name, hot.getData());
				}
			  });
			}
		  bindDumpButton();

		});
		</script>';
		
		echo '


		<div class="handsontable" id="example"></div>
		
		';
		
		echo '
			<script type="text/javascript">
				function exportExcel()
				{	
					var month_ore_xls = $("#month_ore_xls").val();
					var year_ore_xls = $("#year_ore_xls").val();
					var month_name_ore_xls = $("#month_name_ore_xls").val();
					$.post({"ajax.php", {esporta_ore_excel:1,month_ore_xls:month_ore_xls,year_ore_xls:year_ore_xls,month_name_ore_xls:month_name_ore_xls}, success:function (r) {
						
					},
					error: function(xhr,stato,errori){
						alert("Errore durante l\'operazione:"+xhr.status);
					}
					});
					
					
				}
		</script>		
				';
				
		
		echo '
		
		<p><br />
		<form method="post">
		<input type="hidden" id="month-ore-xls" name="month-ore-xls" value="'.$month.'" />
		<input type="hidden" id="year-ore-xls" name="year-ore-xls" value="'.$year.'" />
		<input type="hidden" id="month-name-ore-xls" name="month-name-ore-xls" value="'.$month_name.'" />
				<button name="save" id="save" class="button" style="cursor:pointer">Salva dati</button>
			</form>
			
			<form method="post" action="ajax.php" onSubmit="exportExcel(); return false;">
			<input type="hidden" id="month_ore_xls" name="month_ore_xls" value="'.$month.'" />
		<input type="hidden" id="year_ore_xls" name="year_ore_xls" value="'.$year.'" />
		<input type="hidden" id="month_name_ore_xls" name="month_name_ore_xls" value="'.$month_name.'" />
				<input type="submit" name="esporta_ore_excel" id="esporta_ore_excel" class="button" value="Esporta dati in formato Excel" style="cursor:pointer; height:27px; margin-top:-4px" />
				<!-- <label><input name="autosave" id="autosave" checked="checked" autocomplete="off" type="checkbox"> Autosave</label> -->
			</p>
			</form>
			<div id="save_dati">
			</div>
		';
					
		
	}
	
	
	
}

