<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/../classes/AdminTab.php');

class AdminPrecompilati extends AdminTab
{	
	private $_category;

	public function __construct()
	{
	 	$this->table = 'precompilato';
	 	$this->className = 'Precompilato';
	 	$this->lang = false;
	 	$this->edit = true;
	 	$this->view = false;
	 	$this->delete = true;
		
		$this->fieldsDisplay = array(
			'id_precompilato' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 25),
			'oggetto' => array('title' => $this->l('Oggetto'), 'width' => 400),
			'date_add' => array('title' => $this->l('Data aggiunto'), 'width' => 100),
			'date_upd' => array('title' => $this->l('Data modifica'), 'width' => 100),
			'active' => array('title' => $this->l('Enabled'), 'width' => 25, 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false)
			);
			
		
		parent::__construct();
	}
	
	public function displayForm($isMainTab = true)
	{
		global $currentIndex, $cookie;
		parent::displayForm();
		
		$obj = $this->loadObject(true);
		
		echo '
		<form action="'.$currentIndex.'&submitAdd'.$this->table.'=1&token='.Tools::getAdminTokenLite('AdminPrecompilati').'" method="post" name="precompilato" id="precompilato">
		<input type="hidden" name="id_employee" value="'.$cookie->id_employee.'" />
		'.($obj->id ? '<input type="hidden" name="id_'.$this->table.'" value="'.$obj->id.'" />' : '').'
			'.$this->_displayDraftWarning($obj->active).'
			<fieldset><legend><img src="../img/admin/cms.gif" />'.$this->l('Precompilato').'</legend>';
			
		echo '
			<label>'.$this->l('Oggetto').' </label>
				<div class="margin-form">';
			echo '	<div id="oggettodiv" style="display: block; float: left;">
						<input size="110" type="text" id="oggetto" name="oggetto" value="'.htmlentities($obj->oggetto, ENT_COMPAT, 'UTF-8').'" /><sup> *</sup>
					</div>';
		echo '	</div><div class="clear space">&nbsp;</div>';
		
		
		// CONTENT
		echo '	<label>'.$this->l('Testo messaggio').' </label>
				<div class="margin-form">';
			echo '	<div id="messaggiodiv" style="display: block;float: left;">
						<textarea onkeypress="" class="rte" style="width:755px; height:500px"  id="testo" name="testo">'.htmlentities($obj->testo, ENT_COMPAT, 'UTF-8').'</textarea>
					</div>';
		echo '	</div>';
		
		
		// ACTIVE
		
		echo '<div class="clear space">&nbsp;</div>
				<label>'.$this->l('Attivo:').' </label>
				<div class="margin-form">
					<input type="radio" name="active" id="active_on" onclick="toggleDraftWarning(false);" value="1" '.($this->getFieldValue($obj, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_on"> <img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="active" id="active_off" onclick="toggleDraftWarning(true);" value="0" '.(!$this->getFieldValue($obj, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_off"> <img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label>
				</div>';
		// SUBMIT
		echo '	<div class="margin-form space">
					<input type="submit" value="'.$this->l('   Save   ').'" name="submitAdd'.$this->table.'" class="button" />
				</div>
				<div class="small"><sup>*</sup> '.$this->l('Required field').'</div>
			</fieldset><br />
			'.$this->_displayDraftWarning($obj->active).'
		</form>';
		// TinyMCE
		global $cookie;
		$iso = Language::getIsoById((int)($cookie->id_lang));
		$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
		$ad = dirname($_SERVER["PHP_SELF"]);
		echo '
			<script type="text/javascript">	
			var iso = \''.$isoTinyMCE.'\' ;
			var pathCSS = \''._THEME_CSS_DIR_.'\' ;
			var ad = \''.$ad.'\' ;
			</script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce3/tiny_mce.js"></script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>';
	}
	
	

	function postProcess()
	{
		global $cookie, $link, $currentIndex;
		
		if (Tools::isSubmit('deleteprecompilato'))
		{
			$precompilato = new Precompilato((int)(Tools::getValue('id_precompilato')));
			
			if (!$precompilato->delete())
				$this->_errors[] = Tools::displayError('An error occurred while deleting object.').' <b>'.$this->table.' ('.mysql_error().')</b>';
			else
				Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getAdminTokenLite('AdminPrecompilati'));
		}
		elseif (Tools::getValue('submitDel'.$this->table))
		{
			if ($this->tabAccess['delete'] === '1')
			{
				if (isset($_POST[$this->table.'Box']))
				{
					$precompilato = new Precompilato();
					$result = true;
					$result = $cms->deleteSelection(Tools::getValue($this->table.'Box'));
					if ($result)
					{
						Tools::redirectAdmin($currentIndex.'&conf=2&token='.Tools::getAdminTokenLite('AdminPrecompilati'));
					}
					$this->_errors[] = Tools::displayError('An error occurred while deleting selection.');

				}
				else
					$this->_errors[] = Tools::displayError('You must select at least one element to delete.');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to delete here.');
		}
		elseif (Tools::isSubmit('submitAddPrecompilati') OR Tools::isSubmit('submitAddPrecompilatiAndPreview'))
		{
			if (!sizeof($this->_errors))
			{
				if (!$id_precompilato = (int)(Tools::getValue('id_precompilato')))
				{
					$precompilato = new Precompilato();
					if (!$precompilato->add())
						$this->_errors[] = Tools::displayError('An error occurred while creating object.').' <b>'.$this->table.' ('.mysql_error().')</b>';
					elseif (Tools::isSubmit('submitAddPrecompilati'))
					{
						Db::getInstance()->executes("UPDATE precompilato SET oggetto = '".addslashes($_POST['oggetto'])."', testo = '".addslashes($_POST['testo'])."' WHERE id_precompilato = ".$precompilato->id."");
						
						Tools::redirectAdmin($currentIndex.'&conf=3&token='.Tools::getAdminTokenLite('AdminPrecompilati'));
					}
					else
						Tools::redirectAdmin($currentIndex.'&conf=3&token='.Tools::getAdminTokenLite('AdminPrecompilati'));
				}
				else
				{
					$precompilato = new Precompilato($id_precompilato);
					
					if (!$precompilato->update())
						$this->_errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.' ('.mysql_error().')</b>';
					elseif (Tools::isSubmit('submitAddPrecompilati'))
					{
						Db::getInstance()->executes("UPDATE precompilato SET oggetto = '".addslashes($_POST['oggetto'])."', testo = '".addslashes($_POST['testo'])."' WHERE id_precompilato = ".$precompilato->id."");
						
						Tools::redirectAdmin($currentIndex.'&conf=4&token='.Tools::getAdminTokenLite('AdminPrecompilati'));
					}
				}
			}
		}
		elseif (Tools::getValue('position'))
		{
			if ($this->tabAccess['edit'] !== '1')
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
			elseif (!Validate::isLoadedObject($object = $this->loadObject()))
				$this->_errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			elseif (!$object->updatePosition((int)(Tools::getValue('way')), (int)(Tools::getValue('position'))))
				$this->_errors[] = Tools::displayError('Failed to update the position.');
			else
				Tools::redirectAdmin($currentIndex.'&'.$this->table.'Orderby=position&'.$this->table.'Orderway=asc&conf=4'.(($id_category = (int)(Tools::getValue('id_cms_category'))) ? ('&id_cms_category='.$id_category) : '').'&token='.Tools::getAdminTokenLite('AdminPrecompilati'));
		}
		/* Change object statuts (active, inactive) */
		elseif (Tools::isSubmit('status') AND Tools::isSubmit($this->identifier))
		{
			if ($this->tabAccess['edit'] === '1')
			{
				if (Validate::isLoadedObject($object = $this->loadObject()))
				{
					if ($object->toggleStatus())
						Tools::redirectAdmin($currentIndex.'&conf=5'.((int)Tools::getValue('id_cms_category') ? '&id_cms_category='.(int)Tools::getValue('id_cms_category') : '').'&token='.Tools::getValue('token'));
					else
						$this->_errors[] = Tools::displayError('An error occurred while updating status.');
				}
				else
					$this->_errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
		}
		else
			parent::postProcess(true);
	}
	
	private function _displayDraftWarning($active)
	{
		return 
		'<div class="warn draft" style="'.($active ? 'display:none' : '').'">
			<p>
			<span style="float: left">
			<img src="../img/admin/warn2.png" />
			'.$this->l('Il messaggio sarà salvato come bozza').'
			</span>
			<input type="hidden" name="previewSubmitAddcmsAndPreview" id="previewSubmitAddcmsAndPreview" />
			<br class="clear" />
			</p>
		</div>';
	}
	
}


