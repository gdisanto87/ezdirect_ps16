<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/


class AdminSearch extends AdminTab
{
	public function	searchIP($query)
	{
		if (!ip2long(trim($query)))
			return;
			
		$this->_list['customers'] = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
		SELECT DISTINCT c.*
		FROM `'._DB_PREFIX_.'customer` c
		LEFT JOIN `'._DB_PREFIX_.'guest` g ON g.id_customer = c.id_customer
		LEFT JOIN `'._DB_PREFIX_.'connections` co ON g.id_guest = co.id_guest
		WHERE co.`ip_address` = \''.ip2long(trim($query)).'\'');
	}
	
	/**
	* Search a specific string in the products and categories
	*
	* @params string $query String to find in the catalog
	*/
	public function searchCatalog($query)
	{
		global $cookie;
		
		$array_opzioni = array();
		
		$array_opzioni['online'] = Tools::getValue('prodotti_online_src');
		$array_opzioni['offline'] = Tools::getValue('prodotti_offline_src');
		$array_opzioni['old'] = Tools::getValue('prodotti_old_src');
		$array_opzioni['disponibili'] = Tools::getValue('prodotti_disponibili_src');
		$array_opzioni['marca'] = Tools::getValue('auto_marca_src');
		$array_opzioni['serie'] = Tools::getValue('auto_serie_src');
		$array_opzioni['categoria'] = Tools::getValue('auto_categoria_src');
		$array_opzioni['fornitore'] = Tools::getValue('auto_fornitore_src');
		$array_opzioni['note'] = Tools::getValue('prodotti_note_src');
		
		$opzioni = serialize($array_opzioni);
		
		$this->_list['products'] = Product::searchByName((int)$cookie->id_lang, $query, '', $opzioni);
		
		if(Tools::getIsset('qta_prev'))
			$this->_list['products'] = Product::searchByName((int)$cookie->id_lang, $query, 'qta_prev',$opzioni);
		
		if(Tools::getIsset('old') && Tools::getValue('old') == 'yes')
		{
			$this->_list['products'] = Product::searchByName((int)$cookie->id_lang, $query, '', $opzioni);
		}
		if (!empty($this->_list['products']))
			for ($i = 0; $i < count($this->_list['products']); $i++)
				$this->_list['products'][$i]['nameh'] = str_ireplace($query, '<span class="highlight">'.Tools::htmlentitiesUTF8($query).'</span>', $this->_list['products'][$i]['name']);

		if(!empty($query))
			$this->_list['categories'] = Category::searchByName((int)$cookie->id_lang, $query);
	}
	
	public function searchBundles($query)
	{
	
		$this->_list['bundles'] = Bundle::searchByName((int)$cookie->id_lang, $query);
		
	}
	
	public function searchTickets($query)
	{
	
		global $cookie;
		
		$this->_list['tickets'] = Customer::searchTicketById($query);
		
	}
	
	public function searchOrders($query)
	{
	
		$this->_list['orders'] = Order::searchOrderById($query);
		
	}
	
	public function searchCarts($query)
	{
	
		$this->_list['carts'] = Cart::searchCartById($query);
		
	}
	
	
	public function searchBDL($query)
	{
	
		$this->_list['bdl'] = Customer::searchBDLById($query);
		
	}
	
	public function searchPersone($query)
	{
		$searchType = (int)Tools::getValue('bo_search_type');
		$this->_list['customers'] = Customer::searchByNameWithNotes($query);
	}

	/**
	* Search a specific name in the customers
	*
	* @params string $query String to find in the catalog
	*/
	public function searchCustomer($query)
	{
		$searchType = (int)Tools::getValue('bo_search_type');
		
		if(Tools::isSubmit('bo_search_submit_customers'))
			$this->_list['customers'] = Customer::searchByName($query);
		else
			$this->_list['customers'] = Customer::searchByName($query);
		
		if(Tools::getValue('note_persone'))
			$this->_list['customers'] = Customer::searchByNameWithNotes($query);
	}
	
	public function searchCustomerPI($query)
	{
		$searchType = (int)Tools::getValue('bo_search_type');
		
		if(Tools::isSubmit('bo_search_submit_customers'))
			$this->_list['customers'] = Customer::searchByPI($query);
		else
			$this->_list['customers'] = Customer::searchByPI($query);

	}

	function postProcess()
	{
		global $cookie;
		
		$query = trim(Tools::getValue('bo_query'));
		$query = (string)$query;
		$searchType = (int)Tools::getValue('bo_search_type');
		
		if(Tools::getIsset('note_persone'))
		{
			$searchType = 2;
		}
		
		if(Tools::isSubmit('bo_search_submit'))
			$b = '';
		else if(Tools::isSubmit('bo_search_submit_products'))
			$searchType = 1;
		else if(Tools::isSubmit('bo_search_submit_customers'))
			$searchType = 2;
		else if(Tools::isSubmit('bo_search_submit_orders'))
			$searchType = 3;
		else if(Tools::isSubmit('bo_search_submit_carts'))
			$searchType = 5;
		else if(Tools::isSubmit('bo_search_submit_persone'))
			$searchType = 7;
		
		if($cookie->profile == 7)
			$searchType = 8;
		
		echo '
		
		<script type="text/javascript" src="../js/jquery/jquery.tablesorter.min.js"></script> 
				<script type="text/javascript" src="../js/jquery/jquery.tablesorter.widgets.js"></script><script type="text/javascript" src="../js/jquery/jquery.tablesorter.cssStickyHeaders.js"></script> ';
		
		
		
		echo '
				<script type="text/javascript">
				var myTextExtraction = function(node) 
					{ 
						// extract data from markup and return it 
						return node.textContent.replace( /<.*?>/g, "" ); 
					}
					
				$(document).ready(function() 
					{ 
					
				
					$.tablesorter.addParser({ 
						// set a unique id 
						id: "thousands",
						is: function(s) { 
							// return false so this parser is not auto detected 
							return false; 
						}, 
						format: function(s) {
							// format your data for normalization 
							return s.replace(/[\s.$£€]/g,"").replace(/,/g,".");
						}, 
						// set type, either numeric or text 
						type: "numeric" 
					}); 


						$("#table-products").tablesorter({ widthFixed: true, widgets: ["zebra", "stickyHeaders"]});
						
					} 
				);
				</script>';
				
		/* Handle empty search field */
		
		if(!empty($query) || (empty($query) && (Tools::getValue('auto_marca_src') != 0 || Tools::getValue('auto_serie_src') != 0 || Tools::getValue('auto_categoria_src') != 0 || Tools::getValue('auto_fornitore_src') != 0)))
		{
			echo '<h2>'.$this->l('Search results').'</h2>';
			
			if (!$searchType and strlen($query) > 1)
			{
				global $_LANGADM;
				$tabs = array();
				$result = Db::getInstance()->ExecuteS('SELECT class_name, name FROM '._DB_PREFIX_.'tab t INNER JOIN '._DB_PREFIX_.'tab_lang tl ON t.id_tab = tl.id_tab AND tl.id_lang = '.(int)$cookie->id_lang);
				foreach ($result as $row)
					$tabs[$row['class_name']] = $row['name'];
				foreach (AdminTab::$tabParenting as $key => $value)
					$tabs[$key] = $tabs[$value];
				$matchingResults = array();

				foreach ($_LANGADM as $key => $value)
					if (stripos($value, $query) !== false)
					{
						$key = substr($key, 0, -32);
						if (in_array($key, array('AdminTab', 'index')))
							continue;
						if (!isset($matchingResults[$tabs[$key]]))
							$matchingResults[$tabs[$key]] = array();
						$matchingResults[$tabs[$key]][] = array('tab' => $key, 'value' => $value);
					}
				
				if (count($matchingResults))
				{
					arsort($matchingResults);
					echo '<h3>'.$this->l('Features matching your query:').' '.count($matchingResults).'</h3>
					<table class="table " cellpadding="0" cellspacing="0">';
					foreach ($matchingResults as $key => $tab)
					{
						for ($i = 0; isset($tab[$i]); ++$i)
							echo '<tr>
							<th>'.($i == 0 ? htmlentities($key, ENT_COMPAT, 'utf-8') : '&nbsp;').'</th>
							<td>
								<a href="?tab='.$tab[$i]['tab'].'&token='.Tools::getAdminTokenLite($tab[$i]['tab']).'">
									'.htmlentities(stripslashes($tab[$i]['value']), ENT_COMPAT, 'utf-8').'
								</a>
							</td>
						</tr>';
					}
					echo '</tbody></table><div class="clear">&nbsp;</div>';
				}
			}
			
			
			/* Product research */
			if (!$searchType OR $searchType == 1)
			{
				$this->fieldsDisplay['catalog'] = (array(
					'ID' => array('title' => $this->l('ID'), 'orderby' => 'p.id_product'),
					'image' => array('title' => $this->l('Photo')),
					'name' => array('title' => $this->l('Name'), 'orderby' => 'pl.name'),
					'reference' => array('title' => $this->l('Reference'), 'orderby' => 'p.reference'),
					'ean13' => array('title' => $this->l('EAN'), 'orderby' => 'p.ean13'),
					
					'price' => array('title' => $this->l('Base price'), 'orderby' => 'p.price'),

					'login_for_offer' => array('title' => $this->l('Login for offer')),

					'listino' => array('title' => $this->l('List price'), 'orderby' => 'p.listino'),
					'stock_quantity' => array('title' => $this->l('EZ_ '), 'title_description' => 'Quantita magazzino EZ', 'width' => 30,  'align' => 'right', 'orderby' => 'p.stock_quantity'),
					'ordinato_quantity' => array('title' => $this->l('OEZ'),  'width' => 30, 'title_description' => 'Quantita ordinato al fornitore (caricata su eSolver)', 'align' => 'right', 'orderby' => 'p.ordinato_quantity'),
					'amazon_quantity' => array('title' => $this->l('AMZ'),  'width' => 30, 'title_description' => 'Magazzino Amazon', 'align' => 'right', 'orderby' => 'p.amazon_quantity'),
					'first_supplier' => array('title' => $this->l('Forn.1'),  'title_description' => 'Quantita disponibile Fornitore 1', 'width' => 30,  'align' => 'right','orderby' => 'p.supplier_quantity'),
					'arrivo_supplier' => array('title' => $this->l('Arrivo'),  'title_description' => 'Quantita in arrivo Fornitore 1', 'width' => 30,  'align' => 'right','orderby' => 'p.arrivo_quantity'),
					'second_supplier' => array('title' => $this->l('Forn.2'),  'title_description' => 'Quantita disponibile Fornitore 2', 'width' => 30,  'align' => 'right','orderby' => 'p.itancia_quantity'),
					'impegnato_quantity' => array('title' => $this->l('Imp.Sito'), 'title_description' => 'Impegnato da sito (quantita ordinata da clienti su sito)', 'width' => 30,  'align' => 'right','orderby' => 'p.impegnato_quantity'),
					'prev' => array('title' => $this->l('Prev.'),  'width' => 30, 'title_description' => 'Quantita prodotto dentro preventivi aperti', 'align' => 'right','orderby' => 'cap.prev'),
					'status' => array('title' => $this->l('St.'),  'width' => 30, 'title_description' => 'Status', 'align' => 'right','orderby' => 'p.active'),
					'action' => array('title' => $this->l('Actions'))
				));

				/* Handle product ID */
				/*if ($searchType == 1 AND (int)$query AND Validate::isUnsignedInt((int)$query))
					if ($product = new Product((int)$query) AND Validate::isLoadedObject($product))
						Tools::redirectAdmin('index.php?tab=AdminCatalog&id_product='.(int)($product->id).'&id_category='.(int)($product->id_category_default).'&addproduct'.'&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)));
				*/	
				/* Normal catalog search */
				$this->searchCatalog($query);
			}
			
			/* Bundles research */
			if (!$searchType && !empty($query))
			{
				$this->fieldsDisplay['bundles'] = (array(
					'ID' => array('title' => $this->l('ID'), 'orderby' => 'p.id_product'),
					'image' => array('title' => $this->l('Photo')),
					'name' => array('title' => $this->l('Name'), 'orderby' => 'pl.name'),
					'reference' => array('title' => $this->l('Reference'), 'orderby' => 'p.reference'),
					'priceCLI' => array('title' => $this->l('Price CLI'), 'orderby' => 'p.price'),
					'priceRIV' => array('title' => $this->l('PriceRIV'), 'orderby' => 'p.listino'),
					'date_add' => array('title' => $this->l('Added on'), 'type' => 'datetime', 'orderby' => 'p.date_add'),
					'date_upd' => array('title' => $this->l('Updated on'), 'type' => 'datetime', 'orderby' => 'p.date_upd'),
					'status' => array('title' => $this->l('Status')),
					'action' => array('title' => $this->l('Actions'))
				));

				

				/* Normal catalog search */
				$this->searchBundles($query);
			}
			
			
			
			if (!$searchType && !empty($query) && ((strtolower(substr($query,0,4)) == 'todo') || (ctype_alpha((strtolower(substr($query,0,1)))) && is_numeric(strtolower(substr($query,1,-1))) )))
			{
				$this->fieldsDisplay['tickets'] = (array(
					'ticket' => array('title' => $this->l('ID')),
					'contact' =>  array('title' => $this->l('Tipo')),
					'customer' => array('title' => $this->l('Cliente')),
					'stato' => array('title' => $this->l('Status')),
					'data_act' => array('title' => $this->l('Aperto il')
				)));

				

				/* Normal catalog search */
				$this->searchTickets($query);
			}
			
			if (!$searchType && !empty($query) && (strtolower(substr($query,0,3)) == 'bdl'))
			{
				$this->fieldsDisplay['bdl'] = (array(
					'ticket' => array('title' => $this->l('ID'), 'orderby' => 'a.id_bdl'),
					'customer' => array('title' => $this->l('Cliente')),
					'stato' => array('title' => $this->l('Status')),
					'data_richiesta' => array('title' => $this->l('Richiesto il')
				)));

				
				$this->searchBDL($query);
			}
			
			/* Orders research */
			if (!$searchType && !empty($query))
			{
				if($cookie->id_employee == 2)
					$this->fieldsDisplay['orders'] = (array(
						'id_order' => array('title' => $this->l('ID'), 'orderby' => 'id_order'),
						'preventivo' => array('title' => $this->l('preventivo')),
						'customer' => array('title' => $this->l('Cliente'), 'orderby' => 'customer'),
						'esolver'  => array('title' => $this->l('eSolver'), 'orderby' => 'codice_esolver'),
						'gruppo' => array('title' => $this->l('Gruppo'), 'orderby' => 'gruppo'),
						'total_products' => array('title' => $this->l('Tot.'), 'orderby' => 'total_products'),
						'payment' => array('title' => $this->l('Pagamento'), 'orderby' => 'payment'),
						'osname' => array('title' => $this->l('Stato'), 'type' => 'datetime', 'orderby' => 'osname'),
						'date_add' => array('title' => $this->l('Data'), 'type' => 'datetime', 'orderby' => 'date_add')
					));
				else
					$this->fieldsDisplay['orders'] = (array(
						'id_order' => array('title' => $this->l('ID'), 'orderby' => 'id_order'),
						'preventivo' => array('title' => $this->l('preventivo')),
						'customer' => array('title' => $this->l('Cliente'), 'orderby' => 'customer'),
						'esolver'  => array('title' => $this->l('eSolver'), 'orderby' => 'codice_esolver'),
						'gruppo' => array('title' => $this->l('Gruppo'), 'orderby' => 'gruppo'),
						'total_products' => array('title' => $this->l('T.netto'), 'orderby' => 'total_products'),
						'payment' => array('title' => $this->l('Pagamento'), 'orderby' => 'payment'),
						'osname' => array('title' => $this->l('Stato'), 'type' => 'datetime', 'orderby' => 'osname'),
						'date_add' => array('title' => $this->l('Data'), 'type' => 'datetime', 'orderby' => 'date_add')
					));

				/* Normal catalog search */
				$this->searchOrders($query);
			}
			
			
			/* Carts research */
			if (!$searchType && !empty($query))
			{
				$this->fieldsDisplay['carts'] = (array(
					'id_cart' => array('title' => $this->l('ID'), 'orderby' => 'id_order'),
					'prev' => array('title' => $this->l('Prev.?')),
					'conv' => array('title' => $this->l('Conv.?')),
					'letto' => array('title' => $this->l('Letto?')),
					'id_order' => array('title' => $this->l('N.ORD.')),
					'customer' => array('title' => $this->l('Cliente'), 'orderby' => 'customer'),
					'name' => array('title' => $this->l('Oggetto'), 'orderby' => 'name'),
					'created_by' => array('title' => $this->l('Creato da'), 'orderby' => 'created_by'),
					'in_carico' => array('title' => $this->l('In carico a'), 'orderby' => 'in_carico'),
					'date_add' => array('title' => $this->l('Data'), 'type' => 'datetime', 'orderby' => 'date_add')
				));

				
				$this->searchCarts($query);
			}
			

			/* Customer */
			if (!empty($query) && (!$searchType OR $searchType == 2  OR $searchType == 8 OR $searchType == 6 OR $searchType == 7))
			{
				$this->fieldsDisplay['customers'] = (array(
					'ID' => array('title' => $this->l('ID')),
					
					'codice_esolver' => array('title' => $this->l('eSolver')),
					
					'company' => array('title' => $this->l('Company')),
					'lastname' => array('title' => $this->l('Lastname')),
					'firstname' => array('title' => $this->l('Firstname')),
					'group_name' => array('title' => $this->l('Gruppo')),
					'iso_code' => array('title' => $this->l('Prov.')),
					'tax_code' => array('title' => $this->l('PI/CF')),
					'phone' => array('title' => $this->l('Tel.')),
					/*'phone_mobile' => array('title' => $this->l('Cell.'), 'orderby' => 'a.phone_mobile'),*/
					'e-mail' => array('title' => $this->l('e-mail')),
					'status' => array('title' => $this->l('Status')),
					//'register_date' => array('title' => $this->l('Register date'), 'orderby' => 'c.date_add'),
					'actions' => array('title' => $this->l('Actions'))
				));

				if (!$searchType OR $searchType == 2)
				{
					/* Handle customer ID */
					/*if ($searchType AND (int)$query AND Validate::isUnsignedInt((int)$query))
						if ($customer = new Customer((int)$query) AND Validate::isLoadedObject($customer))
							Tools::redirectAdmin('index.php?tab=AdminCustomers&id_customer='.(int)($customer->id).'&viewcustomer'.'&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)));
*/
					/* Normal customer search */
					$this->searchCustomer($query);
				}
				
				if ($searchType == 8)
				{
					/* Handle customer ID */
					/*if ($searchType AND (int)$query AND Validate::isUnsignedInt((int)$query))
						if ($customer = new Customer((int)$query) AND Validate::isLoadedObject($customer))
							Tools::redirectAdmin('index.php?tab=AdminCustomers&id_customer='.(int)($customer->id).'&viewcustomer'.'&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)));
*/
					/* Normal customer search */
					$this->searchCustomerPI($query);
				}
				
				if ($searchType == 7)
				{
					$this->searchPersone($query);
				}
				
				if ($searchType == 6)
					$this->searchIP($query);
			}

			/* Order */
			if ($searchType == 3 && !empty($query))
			{
				if(strlen($query) > 10 && substr_count($query, '-') > 1)
				{
					$id_cart_new = Db::getInstance()->getValue('SELECT id_order FROM orders WHERE id_cart IN (SELECT id_cart FROM cart WHERE name LIKE "%'.$query.'%")');
					$order = new Order($id_cart_new);
						
					Tools::redirectAdmin('index.php?tab=AdminCustomers&viewcustomer&id_customer='.$order->id_customer.'&id_order='.(int)($order->id).'&vieworder'.'&tab-container-1=4&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)));
				}	
				
				
				if ((int)$query AND Validate::isUnsignedInt((int)$query) AND $order = new Order((int)$query) AND Validate::isLoadedObject($order))
				{
					Tools::redirectAdmin('index.php?tab=AdminCustomers&viewcustomer&id_customer='.$order->id_customer.'&id_order='.(int)($order->id).'&vieworder'.'&tab-container-1=4&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)));
				}	
				$this->_errors[] = Tools::displayError('No order found with this ID:').' '.Tools::htmlentitiesUTF8($query);
			}
			
			/* Invoices */
			if ($searchType == 4 && !empty($query))
			{
				if ((int)$query AND Validate::isUnsignedInt((int)$query) AND $invoice = Order::getInvoice((int)$query))
					Tools::redirectAdmin('pdf.php?id_order='.(int)($invoice['id_order']).'&pdf');
				$this->_errors[] = Tools::displayError('No invoice found with this ID:').' '.Tools::htmlentitiesUTF8($query);
			}

			/* Cart */
			if ($searchType == 5 && !empty($query))
			{
				if(strlen($query) > 10 && substr_count($query, '-') > 1)
					{
						$id_cart_new = Db::getInstance()->getValue('SELECT id_cart FROM cart WHERE name LIKE "%'.$query.'%"');
						$cart = new Cart($id_cart_new);
						
						Tools::redirectAdmin('index.php?tab=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&tab-container-1=4&id_cart='.(int)($cart->id).'&viewcart'.'&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)));
					}
					
					
				if ((int)$query AND Validate::isUnsignedInt((int)$query) AND $cart = new Cart((int)$query) AND Validate::isLoadedObject($cart))
				{
					Tools::redirectAdmin('index.php?tab=AdminCustomers&id_customer='.$cart->id_customer.'&viewcustomer&tab-container-1=4&id_cart='.(int)($cart->id).'&viewcart'.'&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)));
					
				}	
				else
				{
					$this->fieldsDisplay['carts'] = (array(
					'id_cart' => array('title' => $this->l('ID'), 'orderby' => 'id_order'),
					'prev' => array('title' => $this->l('Prev.?')),
					'conv' => array('title' => $this->l('Conv.?')),
					'letto' => array('title' => $this->l('Letto?')),
					'id_order' => array('title' => $this->l('N.ORD.')),
					'customer' => array('title' => $this->l('Cliente'), 'orderby' => 'customer'),
					'name' => array('title' => $this->l('Oggetto'), 'orderby' => 'name'),
					'created_by' => array('title' => $this->l('Creato da'), 'orderby' => 'created_by'),
					'in_carico' => array('title' => $this->l('In carico a'), 'orderby' => 'in_carico'),
					'date_add' => array('title' => $this->l('Data'), 'type' => 'datetime', 'orderby' => 'date_add')
					));

				
					$this->_list['carts'] = Cart::searchCartById($query);
				}
			}
			
			/* IP */
			// 6 - but it is included in the customer block
		}
		else
			$this->_errors[] = Tools::displayError('Please fill in search form first.');
	}

	public function display()
	{
		global $cookie;
		$currentIndex = 'index.php';
		$currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));
		$query = trim(Tools::getValue('bo_query'));
		$query = (string)$query;
		$nbCategories = $nbProducts = $nbCustomers = $nbBundles = $nbTickets = $nbBDL = $nbOrders = $nbCarts = 0;
		
		/* Display categories if any has been matching */
		if (isset($this->_list['categories']) AND $nbCategories = sizeof($this->_list['categories']))
		{
			echo '<h3>'.$nbCategories.' '.($nbCategories > 1 ? $this->l('categories found with') : $this->l('category found with')).' <b>"'.Tools::htmlentitiesUTF8($query).'"</b></h3>';
			echo '<table cellspacing="0" cellpadding="0" class="table ">';
			$irow = 0;
			foreach ($this->_list['categories'] AS $k => $category)
				echo '<tr class="'.($irow++ % 2 ? 'alt_row' : '').'"><td>'.rtrim(getPath($currentIndex.'?tab=AdminCatalog', $category['id_category'], '', $query), ' >').'</td></tr>';
			echo '</tbody></table>
			<div class="clear">&nbsp;</div>';
		}
		
		/* Display tickets if any has been matching */
		if (isset($this->_list['tickets']) AND !empty($this->_list['tickets']) AND $nbTickets = sizeof($this->_list['tickets']))
		{
			echo '<h3>'.$nbTickets.' '.($nbTickets > 1 ? $this->l('ticket / azioni / to do trovati ') : $this->l('ticket / azioni / to do trovati ')).' <b>"'.Tools::htmlentitiesUTF8($query).'"</b></h3>
			<table class="table " cellpadding="0" cellspacing="0"><thead><tr>
				';
			
			foreach ($this->fieldsDisplay['tickets'] AS $field)
				echo '<th style="'.(isset($field['width']) ? 'width: '.$field['width'].';' : '').' '.(isset($field['align']) ? 'text-align: '.$field['align'].';' : '').'">'.$field['title'].' '.($field['orderby'] != '' ? '<a href="index.php?tab=AdminSearch&bo_query='.Tools::getValue('bo_query').'&token='.$this->token.'&orderby='.$field['orderby'].'&orderway=asc"><img src="../img/admin/up.gif" alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a><a href="index.php?tab=AdminSearch&bo_query='.Tools::getValue('bo_query').'&token='.$this->token.'&orderby='.$field['orderby'].'&orderway=desc"><img src="../img/admin/down.gif" alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>' : '').'
				</th>';
				
			echo '</tr></thead><tbody>';	
			foreach ($this->_list['tickets'] AS $k => $ticket)
			{
				if(is_numeric($ticket['id_contact']) && $ticket['id_contact'] != 7)		
					$linkticket = $currentIndex.'?tab=AdminCustomers&id_customer='.$ticket['id_customer'].'&viewcustomer&viewticket&id_customer_thread='.$ticket['id_customer_thread'].'&tab-container-1=6&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee));
				else if($ticket['id_contact'] == 'preventivo' || $ticket['id_contact'] == 'tirichiamiamonoi')
					$linkticket = $currentIndex.'?tab=AdminCustomers&id_customer='.$ticket['id_customer'].'&viewcustomer&viewpreventivo&id_thread='.$ticket['id_customer_thread'].'&tab-container-1=7&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee));
				else if(is_numeric($ticket['id_contact']) && $ticket['id_contact'] == 7)		
					$linkticket = $currentIndex.'?tab=AdminCustomers&id_customer='.$ticket['id_customer'].'&viewcustomer&viewmessage&id_mex='.$ticket['id_customer_thread'].'&tab-container-1=7&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee));	
				else	
					$linkticket = $currentIndex.'?tab=AdminCustomers&id_customer='.$ticket['id_customer'].'&viewcustomer&viewaction&id_action='.$ticket['id_customer_thread'].'&tab-container-1=10&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee));
				
				
				echo '
				<tr>
					<td><a href="'.$linkticket.'">'.$ticket['ticket'].'</a></td>
					<td><a href="'.$linkticket.'">'.$ticket['contact'].'</a></td>
					<td><a href="'.$linkticket.'">'.$ticket['customer'].'</a></td>
					<td><a href="'.$linkticket.'">'.($ticket['status'] == 'open' ? '<img src="../img/admin/status_red.gif" alt="Open" title="Open" /> A' : ($ticket['status'] == 'pending1' ? '<img src="../img/admin/status_orange.gif" alt="Pending" title="Pending" /> L' : '<img src="../img/admin/status_green.gif" alt="Closed" title="Closed" /> C')) .'</a></td>
					<td><a href="'.$linkticket.'">'.Tools::displayDate($ticket['data_act'],5).'</a></td>
					</td>
				</tr>';
			}
			echo '</tbody></table>
			<div class="clear">&nbsp;</div>';
		}
		
		
		/* Display orders if any has been matching */
		if (isset($this->_list['orders']) AND !empty($this->_list['orders']) AND $nbOrders = sizeof($this->_list['orders']))
		{
			echo '<h3>'.$nbOrders.' '.($nbOrders > 1 ? $this->l('orders found with') : $this->l('orders found with')).' <b>"'.Tools::htmlentitiesUTF8($query).'"</b></h3>
			<table class="table " cellpadding="0" cellspacing="0"><thead><tr>
				';
			
			foreach ($this->fieldsDisplay['orders'] AS $field)
				echo '<th style="'.(isset($field['width']) ? 'width: '.$field['width'].';' : '').' '.(isset($field['align']) ? 'text-align: '.$field['align'].';' : '').'">'.$field['title'].' '.($field['orderby'] != '' ? '<a href="index.php?tab=AdminSearch&bo_query='.Tools::getValue('bo_query').'&token='.$this->token.'&orderby='.$field['orderby'].'&orderway=asc"><img src="../img/admin/up.gif" alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a><a href="index.php?tab=AdminSearch&bo_query='.Tools::getValue('bo_query').'&token='.$this->token.'&orderby='.$field['orderby'].'&orderway=desc"><img src="../img/admin/down.gif" alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>' : '').'
				</th>';
				
			echo '</tr></thead><tbody>';	
			foreach ($this->_list['orders'] AS $k => $order)
			{
				
				$linkorder = $currentIndex.'?tab=AdminCustomers&viewcustomer&id_customer='.$order['id_customer'].'&id_order='.$order['id_order'].'&vieworder&tab-container-1=4&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee));
				
				echo '
				<tr>
					<td><a href="'.$linkorder.'">'.$order['id_order'].'</a></td>
					<td><a href="'.$linkorder.'">'.$order['preventivo'].'</a></td>
					<td><a href="'.$linkorder.'">'.$order['customer'].'</a></td>
					<td><a href="'.$linkorder.'">'.$order['codice_esolver'].'</a></td>
					<td><a href="'.$linkorder.'">'.$order['gruppo'].'</a></td>
					<td><a href="'.$linkorder.'">'.($cookie->id_employee == 2 ? Tools::displayPrice($order['total_paid_real'],1)  : Tools::displayPrice($order['total_products'],1)).'</a></td>
					<td><a href="'.$linkorder.'">'.$order['payment'].'</a></td>
					<td><a href="'.$linkorder.'">'.$order['osname'].'</a></td>
					<td><a href="'.$linkorder.'">'.Tools::displayDate($order['date_add'],5).'</a></td>
					</td>
				</tr>';
			}
			echo '</tbody></table>
			<div class="clear">&nbsp;</div>';
		}
		
		
		/* Display orders if any has been matching */
		if (isset($this->_list['carts']) AND !empty($this->_list['carts']) AND $nbCarts = sizeof($this->_list['carts']))
		{
			
			
			echo '<h3>'.$nbCarts.' '.($nbCarts > 1 ? $this->l('carts found with') : $this->l('carts found with')).' <b>"'.Tools::htmlentitiesUTF8($query).'"</b></h3>
			<table class="table " cellpadding="0" cellspacing="0"><thead><tr>
				';
			
			foreach ($this->fieldsDisplay['carts'] AS $field)
				echo '<th style="'.(isset($field['width']) ? 'width: '.$field['width'].';' : '').' '.(isset($field['align']) ? 'text-align: '.$field['align'].';' : '').'">'.$field['title'].' '.($field['cartby'] != '' ? '<a href="index.php?tab=AdminSearch&bo_query='.Tools::getValue('bo_query').'&token='.$this->token.'&cartby='.$field['cartby'].'&cartway=asc"><img src="../img/admin/up.gif" alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a><a href="index.php?tab=AdminSearch&bo_query='.Tools::getValue('bo_query').'&token='.$this->token.'&orderby='.$field['cartby'].'&orderway=desc"><img src="../img/admin/down.gif" alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>' : '').'
				</th>';
				
			echo '</tr></thead><tbody>';	
			foreach ($this->_list['carts'] AS $k => $cart)
			{
				
				$linkcart = $currentIndex.'?tab=AdminCustomers&viewcustomer&id_customer='.$cart['id_customer'].'&id_cart='.$cart['id_cart'].'&viewcart&tab-container-1=4&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee));
				
				echo '
				<tr>
					<td><a href="'.$linkcart.'">'.$cart['id_cart'].'</a></td>
					<td><a href="'.$linkcart.'">'.($cart['prev'] == 0 ? '<img src="../img/admin/red_no.gif" alt="No" title="No" />' : '<img src="../img/admin/enabled.gif" alt="Si" title="Si" />').'</a></td>
					<td><a href="'.$linkcart.'">'.($cart['conv'] == 0 ? '<img src="../img/admin/red_no.gif" alt="No" title="No" />' : '<img src="../img/admin/enabled.gif" alt="Si" title="Si" />').'</a></td>
					<td><a href="'.$linkcart.'">'.($cart['prev'] == 0 ? '--' : ($cart['visualizzato'] == 0 ? '<img src="../img/admin/red_no.gif" alt="No" title="No" />' : '<img src="../img/admin/enabled.gif" alt="Si" title="Si" />')).'</a></td>
					<td><a href="'.$linkcart.'">'.$cart['id_order'].'</a></td>
					<td><a href="'.$linkcart.'">'.$cart['customer'].'</a></td>
					<td><a href="'.$linkcart.'">'.$cart['name'].'</a></td>
					<td><a href="'.$linkcart.'">'.$cart['created_by'].'</a></td>
					<td><a href="'.$linkcart.'">'.$cart['in_carico'].'</a></td>
					<td><a href="'.$linkcart.'">'.Tools::displayDate($cart['date_add'],5).'</a></td>
					</td>
				</tr>';
			}
			echo '</tbody></table>
			<div class="clear">&nbsp;</div>';
		}
		
		
		/* Display BDLs if any has been matching */
		if (isset($this->_list['bdl']) AND !empty($this->_list['bdl']) AND $nbBDL = sizeof($this->_list['bdl']))
		{
			echo '<h3>'.$nbBDL.' '.($nbNDL > 1 ? $this->l('BDL found with') : $this->l('BDL found with')).' <b>"'.Tools::htmlentitiesUTF8($query).'"</b></h3>
			<table class="table " cellpadding="0" cellspacing="0"><thead><tr>
				';
			
			foreach ($this->fieldsDisplay['bdl'] AS $field)
				echo '<th style="'.(isset($field['width']) ? 'width: '.$field['width'].';' : '').' '.(isset($field['align']) ? 'text-align: '.$field['align'].';' : '').'">'.$field['title'].' '.($field['orderby'] != '' ? '<a href="index.php?tab=AdminSearch&bo_query='.Tools::getValue('bo_query').'&token='.$this->token.'&orderby='.$field['orderby'].'&orderway=asc"><img src="../img/admin/up.gif" alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a><a href="index.php?tab=AdminSearch&bo_query='.Tools::getValue('bo_query').'&token='.$this->token.'&orderby='.$field['orderby'].'&orderway=desc"><img src="../img/admin/down.gif" alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>' : '').'
				</th>';
				
			echo '</tr></thead><tbody>';
			foreach ($this->_list['bdl'] AS $k => $bdl)
			{
					$linkbdl = $currentIndex.'?tab=AdminCustomers&id_customer='.$bdl['id_customer'].'&viewcustomer&edit_bdl='.$bdl['id_bdl'].'&tab-container-1=14&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee));
			
				echo '
				<tr>
					<td><a href="'.$linkbdl.'">'.$bdl['ticket'].'</a></td>
					<td><a href="'.$linkbdl.'">'.$bdl['customer'].'</a></td>
					<td><a href="'.$linkbdl.'">'.($bdl['status'] == 'open' ? '<img src="../img/admin/status_red.gif" alt="Open" title="Open" /> A' : ($bdl['status'] == 'pending1' ? '<img src="../img/admin/status_orange.gif" alt="Pending" title="Pending" /> L' : '<img src="../img/admin/status_green.gif" alt="Closed" title="Closed" /> C')) .'</a></td>
					<td><a href="'.$linkbdl.'">'.$bdl['employee'].'</a></td>
					<td><a href="'.$linkbdl.'">'.Tools::displayDate($bdl['data_richiesta'],5).'</a></td>
					</td>
				</tr>';
			}
			echo '</tbody></table>
			<div class="clear">&nbsp;</div>';
		}
		
		if (isset($_GET['status']))
		{
			$object = new Product();
			$object->id = Tools::getValue('id_product');
			$object->toggleStatus();
			echo '<script type="text/javascript">
			$(document).ready(function(){
				$("#bo_search_submit").trigger("click");
			});
			</script>';
			//Tools::redirectAdmin($currentIndex.'?tab=AdminSearch&bo_query='.trim(Tools::getValue('bo_query')).'&token='.Tools::getAdminToken('AdminSearch'.(int)(Tab::getIdFromClassName('AdminSearch')).(int)($cookie->id_employee)));
		}

		/* Display products if any has been matching */
		if (isset($this->_list['products']) AND !empty($this->_list['products']) AND $nbProducts = sizeof($this->_list['products']))
		{
			$margine_10 = 'no';
			foreach ($this->_list['products'] AS $k => $product)
			{
				if ($product['price'] > 0 && (($product['price'] - $product['wholesale_price'])*100)/$product['price'] < 10)
					$margine_10 = 'yes';
			}	
			
			echo '<h3>'.$nbProducts.' '.($nbProducts > 1 ? $this->l('products found with') : $this->l('product found with')).' <b>"'.Tools::htmlentitiesUTF8($query).'"</b> 
			'.($margine_10 == 'yes' ? '(<span style="background-color:#ffcccc">Righe in rosa: prodotto in vendita con margine sotto il 10%</span>)' : '').'</h3><br /><br />
			Nota: passare con puntatore su quantit&agrave; per visualizzare fornitore<br /><br />
			<!-- <a href="'.$currentIndex.'?tab=AdminSearch&bo_query='.$query.'&token='.$this->token.'&old=yes">Clicca qui se vuoi cercare anche nei prodotti OLD</a>
			 | 
			 <a href="'.$currentIndex.'?tab=AdminSearch&bo_query='.$query.'&token='.$this->token.'&qta_prev=yes">Clicca qui se vuoi cercare anche le quantit&agrave; nei preventivi degli ultimi quindici giorni</a> <br /><br /> -->
			 
			<div class="table-container">
			
			<table class="table" cellpadding="0" cellspacing="0" id="table-products"><thead><tr>
				';
			foreach ($this->fieldsDisplay['catalog'] AS $field)
			{
				if($field['title'] != 'Prev.' || ($field['title'] == 'Prev.'))
					echo '<th style="'.(isset($field['width']) ? 'max-width: '.$field['width'].';' : '').' '.(isset($field['align']) ? 'text-align: '.$field['align'].';' : '').'">'.($field['title_description'] != '' ? '<span class="title-reference" title="'.$field['title_description'].'">' : '').$field['title'].''.($field['title_description'] != '' ? '</span>' : '').' <br /></th>';
				
			}
				
			echo '</tr></thead><tbody>';
			foreach ($this->_list['products'] AS $k => $product)
			{
			
			$imageproduct = Image::getCoverz((int)($cookie->id_lang), $product['id_product']);
	
			$image_obj = new Image($imageproduct[0]['id_image']);
			$img_path = 'p/'.$image_obj->getExistingImgPath();
		
			if($img_path == 'p/')
				$img_path = 'm/'.$product['id_manufacturer'];
			
			$qta_ord_clienti = Db::getInstance()->getValue('SELECT sum(product_quantity - (REPLACE(cons,"0_",""))) FROM order_detail od JOIN orders o ON o.id_order = od.id_order 
			JOIN cart c ON c.id_cart = o.id_cart
						JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE  moh.id_order_state != 18 AND moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE c.name NOT LIKE "%prime%" AND od.product_id = '.$product['id_product'].' AND os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state != 20 AND os.id_order_state != 26 AND os.id_order_state != 31 AND os.id_order_state != 32 AND os.id_order_state != 35 GROUP BY od.product_id');
			
			if(!$qta_ord_clienti) 
				$qta_ord_clienti = 0;

			$login_for_offer = Db::getInstance()->getValue('SELECT login_for_offer FROM product WHERE id_product = '.$product['id_product']);
			$margin_login_for_offer = Db::getInstance()->getValue('SELECT margin_login_for_offer FROM product WHERE id_product = '.$product['id_product']);

			if($login_for_offer == 1)
				$prezzo_login_for_offer = (($product['wholesale_price'] * 100) / (100 - $margin_login_for_offer));
			
				$marginalita = (($product['price'] - $product['wholesale_price'])*100)/$product['price'];
				echo '
				<tr '.($marginalita < 10 && $product['price'] > 0 ? 'style="background-color:#ffcccc"' : '').'>
					<td><span class="span-reference" title="'.Product::showProductTooltip($product['id_product']).'">'.$product['id_product'].'</span></span></td>
					<td align="center"><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$product['id_product'].'&id_category='.(int)($product['id_category_default']).'&addproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'"><img class="span-reference" width="45" height="45" src="https://www.ezdirect.it/img/'.$img_path.'-small.jpg'.((int)(Tools::getValue('image_updated')) === (int)($image['id_image']) ? '?date='.time() : '').'"
								alt="'.htmlentities(stripslashes($image['legend']), ENT_COMPAT, 'UTF-8').'" title="'.Product::showProductTooltip($product['id_product']).'" /></a></span></td>
					<td style="max-width:200px"><span class="span-reference" title="'.Product::showProductTooltip($product['id_product']).'"><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$product['id_product'].'&id_category='.(int)($product['id_category_default']).'&addproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.stripslashes($product['nameh']).'</a></span></td>
					<td><span class="span-reference" title="'.Product::showProductTooltip($product['id_product']).'"><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$product['id_product'].'&id_category='.(int)($product['id_category_default']).'&addproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.$product['reference'].'</a></span></td>
					
					<td><span class="span-reference" title="'.Product::showProductTooltip($product['id_product']).'"><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$product['id_product'].'&id_category='.(int)($product['id_category_default']).'&addproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.$product['ean13'].'</a></span></td>
				
					<td align="right"><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$product['id_product'].'&id_category='.(int)($product['id_category_default']).'&addproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.number_format($product['price'], 2, ",","").'</a></td>

					<td align="right"><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$product['id_product'].'&id_category='.(int)($product['id_category_default']).'&addproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.(($login_for_offer == 1) ? number_format($prezzo_login_for_offer, 2, ",","") : 'ND').'</a></td>
				
					<td align="right"><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$product['id_product'].'&id_category='.(int)($product['id_category_default']).'&addproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.number_format($product['listino'], 2, ",","").'</a></td>
					
					<td  align="right"><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$product['id_product'].'&id_category='.(int)($product['id_category_default']).'&addproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.$product['stock_quantity'].'</a></td>
					
					<td align="right"><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$product['id_product'].'&id_category='.(int)($product['id_category_default']).'&addproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.$product['ordinato_quantity'].'</a></td>

					<td align="right"><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$product['id_product'].'&id_category='.(int)($product['id_category_default']).'&addproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.$product['amazon_quantity'].'</a></td>
					
					<td align="right"><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$product['id_product'].'&id_category='.(int)($product['id_category_default']).'&addproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'"><span class="span-reference" title="'.$product['first_supplier_name'].'">'.$product['first_supplier'].'</span></a></td>
						
					<td align="right"><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$product['id_product'].'&id_category='.(int)($product['id_category_default']).'&addproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.$product['arrivo_supplier'].'</a></td>
			
									
					<td align="right"><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$product['id_product'].'&id_category='.(int)($product['id_category_default']).'&addproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'"><span class="span-reference" title="'.$product['second_supplier_name'].'">'.$product['second_supplier'].'</span></a></td>
					
					
					<td align="right"><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$product['id_product'].'&id_category='.(int)($product['id_category_default']).'&addproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.$qta_ord_clienti.'</a></td>
					
					<td align="right"><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$product['id_product'].'&id_category='.(int)($product['id_category_default']).'&addproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.($product['prev'] == '' ? '0' : $product['prev']).'</a></td>
						
					<td align="right"><a href="'.$currentIndex.'?tab=AdminSearch&id_product='.$product['id_product'].'&id_category='.(int)($product['id_category_default']).'&status&bo_query='.trim(Tools::getValue('bo_query')).'&token='.Tools::getAdminToken('AdminSearch'.(int)(Tab::getIdFromClassName('AdminSearch')).(int)($cookie->id_employee)).'">
					<img src="../img/admin/'.($product['active'] == 1 ? 'enabled.gif' : ($product['active'] == 0 ? 'forbbiden.gif' : 'time.gif')).'" alt="" /></a></td>
					<td align="right">
						<a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$product['id_product'].'&id_category='.(int)($product['id_category_default']).'&addproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">
						<img src="../img/admin/edit.gif" alt="'.$this->l('Modify this product').'" /></a>&nbsp;
						<a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$product['id_product'].'&id_category='.(int)($product['id_category_default']).'&duplicateproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'"
							onclick="return confirm(\''.$this->l('Do you want to duplicate this product?', __CLASS__, true, false).' ('.addslashes($product['name']).')\');">
						<img src="../img/admin/duplicate.png" alt="'.$this->l('Duplicate').'" /></a>
					</td>
				</span></tr>';
			}
			echo '</tbody></table></div><div class="bottom_anchor"></div>
			<div class="clear">&nbsp;</div>';
		}
		
		/* Display bundles if any has been matching */
		if (isset($this->_list['bundles']) AND !empty($this->_list['bundles']) AND $nbBundles = sizeof($this->_list['bundles']))
		{
			echo '<h3>'.$nbBundles.' '.($nbBundles > 1 ? $this->l('bundles found with') : $this->l('bundle found with')).' <b>"'.Tools::htmlentitiesUTF8($query).'"</b></h3>
			<table class="table " cellpadding="0" cellspacing="0"><thead><tr>
				';
			foreach ($this->fieldsDisplay['bundles'] AS $field)
				echo '<th style="'.(isset($field['width']) ? 'width: '.$field['width'].';' : '').' '.(isset($field['align']) ? 'text-align: '.$field['align'].';' : '').'">'.$field['title'].' '.($field['orderby'] != '' ? '<a href="index.php?tab=AdminSearch&bo_query='.Tools::getValue('bo_query').'&token='.$this->token.'&orderby='.$field['orderby'].'&orderway=asc"><img src="../img/admin/up.gif" alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a><a href="index.php?tab=AdminSearch&bo_query='.Tools::getValue('bo_query').'&token='.$this->token.'&orderby='.$field['orderby'].'&orderway=desc"><img src="../img/admin/down.gif" alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>' : '').'
				</th>';
				
			echo '</tr></thead><tbody>';
			foreach ($this->_list['bundles'] AS $k => $bundle)
			{
			
				Bundle::getBundlePrice($bundle['id_bundle'],1);
				$bundle_cat = Db::getInstance()->getValue("SELECT id_category_default FROM product WHERE id_product = ".$bundle['father']."");
				echo '
				<tr>
					<td>'.$bundle['id_bundle'].'</td>
					<td align="center"><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$bundle['father'].'&id_category='.(int)($bundle_cat).'&newbundle&id_bundle='.$bundle['id_bundle'].'&updateproduct&updatebundle&tabs=3&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'"><img  width="45" height="45" src="https://www.ezdirect.it/img/b/'.$bundle['id_image']."-".$bundle['id_bundle'].'-small.jpg" title="'.htmlentities(stripslashes($bundle['legend']), ENT_COMPAT, 'UTF-8').'" /></a></td>
						<td><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$bundle['father'].'&id_category='.(int)($bundle_cat).'&newbundle&id_bundle='.$bundle['id_bundle'].'&updateproduct&updatebundle&tabs=3&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.stripslashes($bundle['bundle_name']).'</a></td>
					<td><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$bundle['father'].'&id_category='.(int)($bundle_cat).'&newbundle&id_bundle='.$bundle['id_bundle'].'&updateproduct&updatebundle&tabs=3&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.$bundle['bundle_ref'].'</a></td>
				
					<td><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$bundle['father'].'&id_category='.(int)($bundle_cat).'&newbundle&id_bundle='.$bundle['id_bundle'].'&updateproduct&updatebundle&tabs=3&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.number_format($bundle['priceCLI'], 2,",","").'</a></td>
				
					<td><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$bundle['father'].'&id_category='.(int)($bundle_cat).'&newbundle&id_bundle='.$bundle['id_bundle'].'&updateproduct&updatebundle&tabs=3&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.number_format($bundle['priceRIV'], 2,",","").'</a></td>
						<td align="center"><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$bundle['father'].'&id_category='.(int)($bundle_cat).'&newbundle&id_bundle='.$bundle['id_bundle'].'&updateproduct&updatebundle&tabs=3&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.date("d/m/Y, H:i:s", strtotime($bundle['date_add'])).'</a></td>
							<td align="center"><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$bundle['father'].'&id_category='.(int)($bundle_cat).'&newbundle&id_bundle='.$bundle['id_bundle'].'&updateproduct&updatebundle&tabs=3&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.date("d/m/Y, H:i:s", strtotime($bundle['date_upd'])).'</a></td>
				
					<td align="center"><a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$bundle['father'].'&id_category='.(int)($bundle_cat).'&newbundle&id_bundle='.$bundle['id_bundle'].'&updateproduct&updatebundle&tabs=3&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">
					<img src="../img/admin/'.($bundle['active'] ? 'enabled.gif' : 'forbbiden.gif').'" alt="" /></a></td>
					<td>
						<a href="'.$currentIndex.'?tab=AdminCatalog&id_product='.$bundle['father'].'&id_category='.(int)($bundle_cat).'&newbundle&id_bundle='.$bundle['id_bundle'].'&updateproduct&updatebundle&tabs=3&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">
						<img src="../img/admin/edit.gif" alt="'.$this->l('Modify this product').'" /></a>&nbsp;
						
					</td>
				</tr>';
			}
			echo '</tbody></table>
			<div class="clear">&nbsp;</div>';
		}

		/* Display customers if any has been matching */
		if (isset($this->_list['customers']) AND !empty($this->_list['customers']) AND $nbCustomers = sizeof($this->_list['customers']))
		{
			
			echo '<script type="text/javascript">
					var myTextExtraction = function(node) 
						{ 
							// extract data from markup and return it 
							return node.textContent.replace( /<.*?>/g, "" ); 
						}
						
					$(document).ready(function() 
						{ 
						
							$.tablesorter.addParser({ 
								// set a unique id 
								id: "myParser", 
								is: function(s) { 
								  // return false so this parser is not auto detected 
								  return false; 
								}, 
								format: function(s, table, cell, cellIndex) { 
								  // get data attributes from $(cell).attr("data-sort");
								  // check specific column using cellIndex
								  return $(cell).attr("data-sort");
								}, 
								// set type, either numeric or text 
								type: "text" 
							  }); 
			  
							$("#searchforcustomers").tablesorter({  widthFixed: false, headers: {
								
								8 : { sorter: false }, 9 : { sorter: false }, 10 : { sorter: false }, 11 : { sorter: false }
				}, cssChildRow: "invisible-table-row", widthFixed: true, widgets: ["zebra", "cssStickyHeaders", "columns"]});
							
						} 
					);
			</script>';
			
			echo '<h3>'.$nbCustomers.' '.($nbCustomers > 1 ? $this->l('customers') : $this->l('customer')).' '.$this->l('found with').' <b>"'.Tools::htmlentitiesUTF8($query).'"</b></h3>
			
			'.($cookie->profile == 7 ? '' : '<a href="'.$currentIndex.'?tab=AdminSearch&bo_query='.$query.'&token='.$this->token.'&note_persone=yes">Clicca qui se vuoi cercare anche nelle note e nelle persone</a> (ricerca più lenta)<br /><br />').'
			
			<table cellspacing="0" id="searchforcustomers" cellpadding="0" class="table tablesorter"><thead><tr>
				';
			foreach ($this->fieldsDisplay['customers'] AS $field)
				echo '<th style="'.(isset($field['width']) ? 'width: '.$field['width'].';' : '').' '.(isset($field['align']) ? 'text-align: '.$field['align'].';' : '').'">'.$field['title'].' '.($field['orderby'] != '' ? '<a href="index.php?tab=AdminSearch&bo_query='.Tools::getValue('bo_query').'&token='.$this->token.'&orderby='.$field['orderby'].'&orderway=asc"><img src="../img/admin/up.gif" alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a><a href="index.php?tab=AdminSearch&bo_query='.Tools::getValue('bo_query').'&token='.$this->token.'&orderby='.$field['orderby'].'&orderway=desc"><img src="../img/admin/down.gif" alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>' : '').'
				</th>';
				
			echo '</tr></thead><tbody>';
			$irow = 0;
			foreach ($this->_list['customers'] AS $k => $customer)
			{
				$imgGender = $customer['id_gender'] == 1 ? '<img src="../img/admin/male.gif" alt="'.$this->l('Male').'" />' : ($customer['id_gender'] == 2 ? '<img src="../img/admin/female.gif" alt="'.$this->l('Female').'" />' : '');
				echo '
				<tr class="'.($irow++ % 2 ? 'alt_row' : '').'">
					<td style="width:15px"><strong><a href="'.$currentIndex.'?tab=AdminCustomers&id_customer='.$customer['id_customer'].'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'">'.$customer['id_customer'].'</a></strong></td>
					<td class="center" style="width:15px"><a href="'.$currentIndex.'?tab=AdminCustomers&id_customer='.$customer['id_customer'].'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'">'.$customer['codice_esolver'].'</a></td>
					<td style="width:350px"><a href="'.$currentIndex.'?tab=AdminCustomers&id_customer='.$customer['id_customer'].'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'">'.$customer['company'].'</a></td>
					<td style="width:55px"><a href="'.$currentIndex.'?tab=AdminCustomers&id_customer='.$customer['id_customer'].'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'">'.$customer['lastname'].'</a></td>
					<td style="width:75px"><a href="'.$currentIndex.'?tab=AdminCustomers&id_customer='.$customer['id_customer'].'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'">'.$customer['firstname'].'</a></td>
					<td style="width:75px"><a href="'.$currentIndex.'?tab=AdminCustomers&id_customer='.$customer['id_customer'].'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'">'.$customer['group_name'].'</a></td>
					<td><a href="'.$currentIndex.'?tab=AdminCustomers&id_customer='.$customer['id_customer'].'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'">'.$customer['iso_code'].'</a></td>
					<td><a href="'.$currentIndex.'?tab=AdminCustomers&id_customer='.$customer['id_customer'].'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'">'.($customer['is_company'] == 0 ? $customer['tax_code'] : $customer['vat_number']).'</a></td>
					 <td style="text-align:center"><a onclick="window.onbeforeunload = null" href="callto:'.$customer['phone'].'"><img src="https://www.ezdirect.it/img/admin/phone.gif" title="'.$customer['phone'].'" /></a></td>
					<!-- <td>'.$customer['phone_mobile'].'</td> -->
					<td style="text-align:center"><a href="mailto:'.stripslashes($customer['email']).'"> <img src="../img/admin/email_edit.gif" title="'.stripslashes($customer['email']).'" alt="'.$this->l('Write to this customer').'" /></a></td>
					<td class="center"><img src="../img/admin/'.($customer['active'] ? 'enabled.gif' : 'forbbiden.gif').'" alt="" /></td>
					<!-- <td>'.date("d/m/Y, H:i:s", strtotime($customer['date_add'])).'</td> -->
				
					
					<td  style="text-align:center">
						<a href="'.$currentIndex.'?tab=AdminCustomers&id_customer='.$customer['id_customer'].'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'">
						<img src="../img/admin/details.gif" alt="'.$this->l('View orders').'" /></a>
					
						<a href="'.$currentIndex.'?tab=AdminCustomers&id_customer='.$customer['id_customer'].'&deletecustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'" onclick="return confirm(\''.$this->l('Are you sure?', __CLASS__, true, false).'\');">
						<img src="../img/admin/delete.gif" alt="'.$this->l('Delete this customer').'" /></a>
					</td>
				</tr>';
			}
			echo '</tbody></table>
			<div class="clear">&nbsp;</div>';
		}
			
		/* Display error if nothing has been matching */
		if (!$nbCategories AND !$nbProducts AND !$nbCustomers AND !$nbBundles)
		{
			echo '<h3>'.$this->l('Nothing found for').' "'.Tools::htmlentitiesUTF8($query).'"</h3>';
		
			echo 'Prodotti: <a href="'.$currentIndex.'?tab=AdminSearch&bo_query='.$query.'&token='.$this->token.'&old=yes">Clicca qui se vuoi cercare anche nei prodotti OLD</a><br /><br />';
			echo 'Clienti: <a href="'.$currentIndex.'?tab=AdminSearch&bo_query='.$query.'&token='.$this->token.'&note_persone=yes">Clicca qui se vuoi cercare anche nelle note e nelle persone</a> (ricerca più lenta)<br /><br />';
		}
	}
}
