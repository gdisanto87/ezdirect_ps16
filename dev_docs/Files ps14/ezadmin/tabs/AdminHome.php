<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminHome extends AdminTab
{
	public function postProcess()
	{
	}

	private function _displayOptimizationTips()
	{
		$rewrite = 0;
		if (Configuration::get('PS_REWRITING_SETTINGS'))
		{
			$rewrite = 2;
			if (!file_exists(dirname(__FILE__).'/../../.htaccess'))
				$rewrite = 1;
			else
			{
				$stat = stat(dirname(__FILE__).'/../../.htaccess');
				if (strtotime(Db::getInstance()->getValue('SELECT date_upd FROM '._DB_PREFIX_.'configuration WHERE name = "PS_REWRITING_SETTINGS"')) > $stat['mtime'])
					$rewrite = 0;
			}
		}

		$htaccessAfterUpdate = 2;
		$htaccessOptimized = (Configuration::get('PS_HTACCESS_CACHE_CONTROL') ? 2 : 0);
		if (!file_exists(dirname(__FILE__).'/../../.htaccess'))
		{
			if (Configuration::get('PS_HTACCESS_CACHE_CONTROL'))
				$htaccessOptimized = 1;
		}
		else
		{
			$stat = stat(dirname(__FILE__).'/../../.htaccess');
			$dateUpdHtaccess = Db::getInstance()->getValue('SELECT date_upd FROM '._DB_PREFIX_.'configuration WHERE name = "PS_HTACCESS_CACHE_CONTROL"');
			if (Configuration::get('PS_HTACCESS_CACHE_CONTROL') AND strtotime($dateUpdHtaccess) > $stat['mtime'])
				$htaccessOptimized = 1;

			$dateUpdate = Configuration::get('PS_LAST_SHOP_UPDATE');
			if ($dateUpdate AND strtotime($dateUpdate) > $stat['mtime'])
				$htaccessAfterUpdate = 0;
		}
		$indexRebuiltAfterUpdate = 0;
		$needRebuild=Configuration::get('PS_NEED_REBUILD_INDEX');
		if ($needRebuild !='0');
			$indexRebuiltAfterUpdate = 2;

		$smartyOptimized = 0;
		if (!Configuration::get('PS_SMARTY_FORCE_COMPILE'))
			++$smartyOptimized;
		if (Configuration::get('PS_SMARTY_CACHE'))
			++$smartyOptimized;

		$cccOptimized = Configuration::get('PS_CSS_THEME_CACHE')
		+ Configuration::get('PS_JS_THEME_CACHE')
		+ Configuration::get('PS_HTML_THEME_COMPRESSION')
		+ Configuration::get('PS_JS_HTML_THEME_COMPRESSION');
		if ($cccOptimized == 4)
			$cccOptimized = 2;
		else
			$cccOptimized = 1;

		$shopEnabled = (Configuration::get('PS_SHOP_ENABLE') ? 2 : 1);

		$lights = array(
		0 => array('image'=>'error2.png','color'=>'#fbe8e8'),
		1 => array('image'=>'warn2.png','color'=>'#fffac6'),
		2 => array('image'=>'ok2.png','color'=>'#dffad3'));

		
		
		
		if ($rewrite + $htaccessOptimized + $smartyOptimized + $cccOptimized + $shopEnabled + $htaccessAfterUpdate + $indexRebuiltAfterUpdate != 14)
		{
			echo '
			<div class="admin-box1">
				<h5>'.$this->l('A good beginning...')
				.'
					<span style="float:right">
						<a id="optimizationTipsFold"'.
						(Configuration::get('PS_HIDE_OPTIMIZATION_TIPS')
						?'" href="#"><img alt="v" style="padding-top:0px; padding-right: 5px;" src="../img/admin/down-white.gif" /></a>':'href="?hideOptimizationTips" >
						<img alt="X" style="padding-top:0px; padding-right: 5px;" src="../img/admin/close-white.png" />
						</a>').'</span></h5>';
			echo '
			<script type="text/javascript">
			$(document).ready(function(){
				$("#optimizationTipsFold").click(function(e){
					$("#list-optimization-tips").toggle(function(){
						if ($("#optimizationTipsFold").children("img").attr("src") == "../img/admin/down-white.gif")
							$("#optimizationTipsFold").children("img").attr("src","../img/admin/close-white.png");
						else
							$("#optimizationTipsFold").children("img").attr("src","../img/admin/down-white.gif");
					});
				})
			});
						</script>
			';
			echo '<ul id="list-optimization-tips" class="admin-home-box-list" '
				.(Configuration::get('PS_HIDE_OPTIMIZATION_TIPS')?'style="display:none"':'').'>
				<li style="background-color:'.$lights[$rewrite]['color'].'">
				<img src="../img/admin/'.$lights[$rewrite]['image'].'" class="pico" />
					<a href="index.php?tab=AdminGenerator&token='.Tools::getAdminTokenLite('AdminGenerator').'">'.$this->l('URL rewriting').'</a>
				</li>
				<li style="background-color:'.$lights[$htaccessOptimized]['color'].'">
				<img src="../img/admin/'.$lights[$htaccessOptimized]['image'].'" class="pico" />
				<a href="index.php?tab=AdminGenerator&token='.Tools::getAdminTokenLite('AdminGenerator').'">'.$this->l('Browser cache & compression').'</a>
				</li>
				<li style="background-color:'.$lights[$smartyOptimized]['color'].'">
				<img src="../img/admin/'.$lights[$smartyOptimized]['image'].'" class="pico" />
				<a href="index.php?tab=AdminPerformance&token='.Tools::getAdminTokenLite('AdminPerformance').'">'.$this->l('Smarty optimization').'</a></li>
				<li style="background-color:'.$lights[$cccOptimized]['color'].'">
				<img src="../img/admin/'.$lights[$cccOptimized]['image'].'" class="pico" />
				<a href="index.php?tab=AdminPerformance&token='.Tools::getAdminTokenLite('AdminPerformance').'">'.$this->l('Combine, Compress & Cache').'</a></li>
				<li style="background-color:'.$lights[$shopEnabled]['color'].'">
				<img src="../img/admin/'.$lights[$shopEnabled]['image'].'" class="pico" />
				<a href="index.php?tab=AdminPreferences&token='.Tools::getAdminTokenLite('AdminPreferences').'">'.$this->l('Shop enabled').'</a></li>
				<li style="background-color:'.$lights[$indexRebuiltAfterUpdate]['color'].'">
					<img src="../img/admin/'.$lights[$indexRebuiltAfterUpdate]['image'].'" class="pico" />
		<a href="index.php?tab=AdminSearchConf&token='.Tools::getAdminTokenLite('AdminSearchConf').'">'.$this->l('index rebuilt after update').'</a></li>
				<li style="background-color:'.$lights[$htaccessAfterUpdate]['color'].'">
					<img src="../img/admin/'.$lights[$htaccessAfterUpdate]['image'].'" class="pico" />
		<a href="index.php?tab=AdminGenerator&token='.Tools::getAdminTokenLite('AdminGenerator').'">'.$this->l('.htaccess up-to-date').'</a></li>
					</ul>
			</div>';
		}
	}

	public function display()
	{
		global $cookie;
		global $currentIndex;
		
		if($cookie->profile == 7)
		{
			// niente
		}
		else
			$this->warnDomainName();
		
		if(Tools::getIsset('source') && Tools::getValue('source') == 'sugarCRM')
		{
			$numero = Tools::getValue('query_string');
			
			if(strlen($numero) > 4)
			{	
				$cliente_numero = Db::getInstance()->getValue('SELECT id_customer FROM address WHERE phone LIKE "%'.$numero.'%" OR phone_mobile LIKE "%'.$numero.'%"');

				if($cliente_numero > 0)
					header('Location: index.php?tab=AdminCustomers&id_customer='.$cliente_numero.'&viewcustomer&tab-container-1=4&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)));
				else
					header('Location: index.php?tab=RegistraAnagrafica&token='.Tools::getAdminToken('RegistraAnagrafica'.(int)(Tab::getIdFromClassName('RegistraAnagrafica')).(int)($cookie->id_employee)));
			}
			else
				header('Location: index.php');
		}
		
		if($cookie->homeStats == '')
		{
			$homeStats = array();
			$homeStats['period'] = date('M');
			$homeStats['year'] = date('Y');
			$homeStats = serialize($homeStats);
			$cookie->homeStats = $homeStats;
		}
		
		if(Tools::getIsset('vai-home'))
		{
			$homeStats = array();
			$homeStats['period'] = Tools::getValue('select-month');
			$homeStats['year'] = Tools::getValue('select-year');
			$homeStats = serialize($homeStats);
			$cookie->homeStats = $homeStats;
			
		}	
		
		
		if(Tools::getIsset('reset-home'))
		{
			$homeStats = array();
			$homeStats['period'] = date('M');
			$homeStats['year'] = date('Y');
			$homeStats = serialize($homeStats);
			$cookie->homeStats = $homeStats;
		}	
		
		$homeStats = unserialize($cookie->homeStats);
		
		$tab = get_class();
		$protocol = Tools::usingSecureMode()?'https':'http';
		$isoDefault = Language::getIsoById(intval(Configuration::get('PS_LANG_DEFAULT')));
		$isoUser = Language::getIsoById(intval($cookie->id_lang));
		$isoCountry = Country::getIsoById(Configuration::get('PS_COUNTRY_DEFAULT'));
		$currency = new Currency((int)(Configuration::get('PS_CURRENCY_DEFAULT')));
		$employee = new Employee($cookie->id_employee);
		
		$tokenStats = Tools::getAdminToken('AdminStats'.(int)(Tab::getIdFromClassName('AdminStats')).(int)($cookie->id_employee));

		echo '<div>
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<h1 style="display:block;float:left">'.$this->l('Dashboard').'</h1>';
		if($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 1 || $cookie->id_employee == 7)
		{
			$mese_attuale = date('M');
			switch($homeStats['period']) {
				case 'Jan': $mese = 'Gennaio'; $mese_inizio = '01'; $mese_fine = '01'; $td = cal_days_in_month(CAL_GREGORIAN, 1, $homeStats['year']); break;		
				case 'Feb': $mese = 'Febbraio'; $mese_inizio = '02'; $mese_fine = '02'; $td = cal_days_in_month(CAL_GREGORIAN, 2, $homeStats['year']); break;
				case 'Mar': $mese = 'Marzo'; $mese_inizio = '03'; $mese_fine = '03'; $td = cal_days_in_month(CAL_GREGORIAN, 3, $homeStats['year']); break;
				case 'Apr': $mese = 'Aprile'; $mese_inizio = '04'; $mese_fine = '04'; $td = cal_days_in_month(CAL_GREGORIAN, 4, $homeStats['year']); break;
				case 'May': $mese = 'Maggio'; $mese_inizio = '05'; $mese_fine = '05'; $td = cal_days_in_month(CAL_GREGORIAN, 5, $homeStats['year']); break;
				case 'Jun': $mese = 'Giugno'; $mese_inizio = '06'; $mese_fine = '06'; $td = cal_days_in_month(CAL_GREGORIAN, 6, $homeStats['year']); break;
				case 'Jul': $mese = 'Luglio'; $mese_inizio = '07'; $mese_fine = '07'; $td = cal_days_in_month(CAL_GREGORIAN, 7, $homeStats['year']); break;
				case 'Aug': $mese = 'Agosto'; $mese_inizio = '08'; $mese_fine = '08'; $td = cal_days_in_month(CAL_GREGORIAN, 8, $homeStats['year']); break;
				case 'Sep': $mese = 'Settembre'; $mese_inizio = '09'; $mese_fine = '09'; $td = cal_days_in_month(CAL_GREGORIAN, 9, $homeStats['year']); break;
				case 'Oct': $mese = 'Ottobre'; $mese_inizio = '10'; $mese_fine = '10'; $td = cal_days_in_month(CAL_GREGORIAN, 10, $homeStats['year']); break;
				case 'Nov': $mese = 'Novembre'; $mese_inizio = '11'; $mese_fine = '11'; $td = cal_days_in_month(CAL_GREGORIAN, 11, $homeStats['year']); break;
				case 'Dec': $mese = 'Dicembre'; $mese_inizio = '12'; $mese_fine = '12'; $td = cal_days_in_month(CAL_GREGORIAN, 12, $homeStats['year']); break;
				case 'q1': $mese = 'Q1: Gennaio - Marzo'; $mese_inizio = '01'; $mese_fine = '03'; $td = '31'; break;
				case 'q2': $mese = 'Q2: Aprile - Giugno'; $mese_inizio = '04'; $mese_fine = '06'; $td = '30'; break;
				case 'q3': $mese = 'Q3: Luglio - Settembre'; $mese_inizio = '07'; $mese_fine = '09'; $td = '30'; break;
				case 'q4': $mese = 'Q4: Ottobre - Dicembre'; $mese_inizio = '10'; $mese_fine = '12'; $td = '31'; break;
				case 'anno': $mese = 'Intero anno'; $mese_inizio = '01'; $mese_fine = '12'; $td = '31'; break;
				default: $mese = $mese_attuale; break;
			}
			
			echo '<div style="float:left; margin-left:30px"><form method="post" action="index.php?tab=AdminHome&token='.$this->token.'"> Seleziona il periodo:'; 
			echo '<select name="select-month" id="select-month" style="height:28px; margin-right:10px">
				<option value="Jan" '.($homeStats['period'] == 'Jan' ? 'selected="selected"' : '').'>Gennaio</option>
				<option value="Feb" '.($homeStats['period'] == 'Feb' ? 'selected="selected"' : '').'>Febbraio</option>
				<option value="Mar" '.($homeStats['period'] == 'Mar' ? 'selected="selected"' : '').'>Marzo</option>
				<option value="Apr" '.($homeStats['period'] == 'Apr' ? 'selected="selected"' : '').'>Aprile</option>
				<option value="May" '.($homeStats['period'] == 'May' ? 'selected="selected"' : '').'>Maggio</option>
				<option value="Jun" '.($homeStats['period'] == 'Jun' ? 'selected="selected"' : '').'>Giugno</option>
				<option value="Jul" '.($homeStats['period'] == 'Jul' ? 'selected="selected"' : '').'>Luglio</option>
				<option value="Aug" '.($homeStats['period'] == 'Aug' ? 'selected="selected"' : '').'>Agosto</option>
				<option value="Sep" '.($homeStats['period'] == 'Sep' ? 'selected="selected"' : '').'>Settembre</option>
				<option value="Oct" '.($homeStats['period'] == 'Oct' ? 'selected="selected"' : '').'>Ottobre</option>
				<option value="Nov" '.($homeStats['period'] == 'Nov' ? 'selected="selected"' : '').'>Novembre</option>
				<option value="Dec" '.($homeStats['period'] == 'Dec' ? 'selected="selected"' : '').'>Dicembre</option>
				<option value="q1" '.($homeStats['period'] == 'q1' ? 'selected="selected"' : '').'>Quarter 1</option>
				<option value="q2" '.($homeStats['period'] == 'q2' ? 'selected="selected"' : '').'>Quarter 2</option>
				<option value="q3" '.($homeStats['period'] == 'q3' ? 'selected="selected"' : '').'>Quarter 3</option>
				<option value="q4" '.($homeStats['period'] == 'q4' ? 'selected="selected"' : '').'>Quarter 4</option>
				<option value="anno" '.($homeStats['period'] == 'anno' ? 'selected="selected"' : '').'>Intero anno</option>
			</select>';
			
			echo '<select name="select-year" id="select-year"  style="height:28px;">
			';
			for($i = 2010; $i<=date("Y"); $i++)
				echo '<option value="'.$i.'" '.($homeStats['year'] == $i ? 'selected="selected"' : '').'>'.$i.'</option>';
			echo '
			</select>
			
			<input type="submit" name="vai-home" value="Vai" class="button" style="margin-left:10px; margin-top:-3px; height:28px" />
			
			<input type="submit" name="reset-home" value="Resetta" class="button" style="margin-left:10px; margin-top:-3px; height:28px" />
			</form>
			</div>';
		}	
		echo '
		<div class="clear"></div>
		<hr style="background-color: #812143;color: #812143;" />
		<br />
		';
		
		if($cookie->id_employee == 6 || $cookie->id_employee == 22)
		{
			$modalita_esolver = Db::getInstance()->getValue('SELECT value FROM configuration WHERE name = "MODALITA_ESOLVER"');
			
			if(Tools::getIsset('modalita'))
			{
				if(Tools::getValue('modalita') == 1)
					Db::getInstance()->executeS('UPDATE configuration SET value = 0 WHERE name = "MODALITA_ESOLVER"');
				else
				{
					Db::getInstance()->executeS('UPDATE configuration SET value = 1 WHERE name = "MODALITA_ESOLVER"');
					Db::getInstance()->executeS('UPDATE product SET qt_esolver = stock_quantity WHERE 1');
					Db::getInstance()->executeS('UPDATE product SET ord_esolver = ordinato_quantity WHERE 1');
				}
				header("Refresh:0");
			}
			
			echo '<form method="post" id="form_cambia_modalita" action="https://www.ezdirect.it/ezadmin/index.php?tab=AdminHome&token='.$this->token.'&conf=3">
			<input name="modalita" type="hidden" value="'.$modalita_esolver.'" />
			Il sito &egrave; attualmente configurato in modalit&agrave; '.($modalita_esolver == 1 ? 'eSolver' : 'CRM').' per il carico/scarico magazzino. <input type="button" class="button" id="cambia_modalita" name="cambia_modalita" value="Passa alla modalit&agrave; '.($modalita_esolver == 1 ? 'CRM' : 'eSolver').'" /><br /><br />
			</form>
			
			<script type="text/javascript" src="https://www.ezdirect.it/js/sweetalert2.js"></script>
			<link rel="stylesheet" href="https://www.ezdirect.it/css/sweetalert2.css" />
			<script type="text/javascript">
			
			$(document).ready(function(){
				 $("#cambia_modalita").on("click",function(e){
					var valid = false;
					swal({
					  title: \'Sei sicuro/a?\',
					  text: \'Questa operazione cambia la modalità di carico/scarico magazzino\',
					  type: \'warning\',
					  showCancelButton: true,
					  confirmButtonColor: \'#6A9944\',
					  cancelButtonColor: \'#d33\',
					  confirmButtonText: \'S&igrave;, voglio continuare!\'
					}).then(function () {
						$("#form_cambia_modalita").submit();
						console.log("OK");
					});
					
				});
			});
			</script>
			';
			
		}
		
		if($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 1 || $cookie->id_employee == 7)
		{
			
			echo '<div style="width:65%; float:left">';
			
			echo '<h2 style="text-align:center">Statistiche del periodo ('.$mese.') - Categorie</h2>';
			
			
			$cat_month_stats = Db::getInstance()->ExecuteS('
			SELECT 
			SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) as orderSum, SUM(od.product_quantity) AS orderQty, cl.name, AVG((od.`product_price` - ((od.product_price * od.reduction_percent) / 100))) as priceAvg,
			SUM(((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) - ((CASE WHEN (od.wholesale_price = 0 AND od.no_acq = 0) THEN p.wholesale_price ELSE od.wholesale_price END)*od.product_quantity)) as margine,
			SUM((CASE WHEN (od.wholesale_price = 0 AND od.no_acq = 0) THEN p.wholesale_price ELSE od.wholesale_price END)*od.product_quantity) as tot_acquisti,
			(CASE WHEN ((SELECT (CASE WHEN (c4.id_category = 0 OR c4.id_category = 1 OR c4.id_category IS NULL) THEN 
					(CASE WHEN (c3.id_category = 0 OR c3.id_category = 1  OR c3.id_category IS NULL) THEN 
						(CASE WHEN (c2.id_category = 0 OR c2.id_category = 1  OR c2.id_category IS NULL) THEN 
							c1.id_category 
							ELSE c2.id_category END) 
					ELSE c3.id_category END) 
				ELSE c4.id_category END) FROM category AS c1 LEFT JOIN category AS c2 ON( c1.id_parent = c2.id_category ) LEFT JOIN category AS c3 ON( c2.id_parent = c3.id_category ) LEFT JOIN category AS c4 ON( c3.id_parent = c4.id_category ) WHERE c1.id_category = p.id_category_default)) IS NULL THEN p.id_category_default ELSE ((SELECT (CASE WHEN (c4.id_category = 0 OR c4.id_category = 1 OR c4.id_category IS NULL) THEN 
					(CASE WHEN (c3.id_category = 0 OR c3.id_category = 1  OR c3.id_category IS NULL) THEN 
						(CASE WHEN (c2.id_category = 0 OR c2.id_category = 1  OR c2.id_category IS NULL) THEN 
							c1.id_category 
							ELSE c2.id_category END) 
					ELSE c3.id_category END) 
				ELSE c4.id_category END) FROM category AS c1 LEFT JOIN category AS c2 ON( c1.id_parent = c2.id_category ) LEFT JOIN category AS c3 ON( c2.id_parent = c3.id_category ) LEFT JOIN category AS c4 ON( c3.id_parent = c4.id_category ) WHERE c1.id_category = p.id_category_default)) END) as macro_category
			FROM `'._DB_PREFIX_.'orders` o
			LEFT JOIN `'._DB_PREFIX_.'order_detail` od ON o.id_order = od.id_order
			LEFT JOIN `'._DB_PREFIX_.'product` p ON p.id_product = od.product_id
			LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (((SELECT (CASE WHEN (c4.id_category = 0 OR c4.id_category = 1 OR c4.id_category IS NULL) THEN 
					(CASE WHEN (c3.id_category = 0 OR c3.id_category = 1  OR c3.id_category IS NULL) THEN 
						(CASE WHEN (c2.id_category = 0 OR c2.id_category = 1  OR c2.id_category IS NULL) THEN 
							c1.id_category 
							ELSE c2.id_category END) 
					ELSE c3.id_category END) 
				ELSE c4.id_category END) FROM category AS c1 LEFT JOIN category AS c2 ON( c1.id_parent = c2.id_category ) LEFT JOIN category AS c3 ON( c2.id_parent = c3.id_category ) LEFT JOIN category AS c4 ON( c3.id_parent = c4.id_category ) WHERE c1.id_category = p.id_category_default)) = cl.id_category AND cl.id_lang = '.(int)($cookie->id_lang).')
			WHERE (o.valid = 1 OR (o.valid = 0 AND ((o.module = "bankwire"  OR o.module = "other_payment"))))
			  AND o.id_order NOT IN (SELECT id_order FROM order_history WHERE id_order_state = 6  OR id_order_state = 35)
			AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-'.$mese_inizio.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$mese_fine.'-'.$td).' 23:59:59"
			GROUP BY macro_category
			ORDER BY (CASE WHEN macro_category = 119 THEN 4 WHEN macro_category = 249 THEN 3 WHEN macro_category = 248 THEN 2 WHEN macro_category = 240 OR macro_category = 246 THEN 1 ELSE 0 END), orderSum DESC
			');
			
			$tot_bdl = Db::getInstance()->getValue('SELECT SUM(importo) FROM '._DB_PREFIX_.'bdl WHERE importo > 0 AND invio_contabilita > 0 AND rif_ordine = 0 AND date_add  BETWEEN "'.date($homeStats['year'].'-'.$mese_inizio.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$mese_fine.'-'.$td).' 23:59:59"');
			
			$tot_bdl_anno = Db::getInstance()->getValue('SELECT SUM(importo) FROM '._DB_PREFIX_.'bdl WHERE importo > 0 AND invio_contabilita > 0 AND rif_ordine = 0 AND date_add  BETWEEN "'.date($homeStats['year'].'-01-01').' 00:00:00" AND "'.date($homeStats['year'].'-12-31').' 23:59:59"');
			
			$category_count_chart = 0;
			$category_data_chart = '';
			$other_category_tot = 0;
			
			foreach ($cat_month_stats as $catrow) 
			{
				if($category_count_chart < 8)
				{
					$category_data_chart .= '["'.$catrow['name'].'", '.number_format($catrow['orderSum'],2,".","").'],';
					$category_count_chart++;
				}	
				else
				{
					$other_category_tot += number_format($catrow['orderSum'],2,".","");	
					
				}	
	
			}	
			
			$other_category_tot += $tot_bdl;
			
			echo ' <script type="text/javascript">

			  // Load the Visualization API and the corechart package.
			  google.charts.load("current", {"packages":["corechart"]});

			  // Set a callback to run when the Google Visualization API is loaded.
			  google.charts.setOnLoadCallback(drawChart);

			  // Callback that creates and populates a data table,
			  // instantiates the pie chart, passes in the data and
			  // draws it.
			  function drawChart() {

				// Create the data table.
				var data = new google.visualization.DataTable();
				data.addColumn("string", "Topping");
				data.addColumn("number", "Slices");
				data.addRows([
				  '.$category_data_chart.'
				  ["Altre", '.$other_category_tot.']
				]);

				// Set chart options
				var options = {"title":"Grafico categorie",
				is3D: true,
				legend: "labeled",
				pieSliceText: "none",
				chartArea:{left:20,top:0,width:"97%",height:"97%"},
				"width":600,
				"height":300};

				var chart = new google.visualization.PieChart(document.getElementById("chart_categorie_div"));

				  chart.draw(data, options);
				  

			  }
			</script>
			<div id="chart_categorie_div"></div>';
			
			echo '<h2 style="text-align:center">Statistiche del mese ('.$mese.') - Costruttori</h2>';
			$man_month_stats = Db::getInstance()->executeS('SELECT
		
			(CASE WHEN m.id_manufacturer = 105 THEN SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate)+(IFNULL((SELECT SUM(importo) FROM bdl WHERE rif_ordine = 0 AND importo > 0 AND invio_contabilita > 0 AND bdl.`date_add` BETWEEN "'.date($homeStats['year'].'-'.$mese_inizio.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$mese_fine.'-'.$td).' 23:59:59"),0)) ELSE SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) END) as orderSum2,

			(CASE WHEN m.id_manufacturer = 105 THEN SUM(od.product_quantity)+(SELECT COUNT(*) FROM bdl WHERE rif_ordine = 0 AND importo > 0 AND invio_contabilita > 0 AND bdl.`date_add` BETWEEN "'.date($homeStats['year'].'-'.$mese_inizio.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$mese_fine.'-'.$td).' 23:59:59") ELSE SUM(od.product_quantity) END) as orderQty2,

			SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) as orderSum, SUM(od.product_quantity) AS orderQty, m.name, AVG((od.`product_price` - ((od.product_price * od.reduction_percent) / 100))) as priceAvg,
			SUM(((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) - ((CASE WHEN (od.wholesale_price = 0 AND od.no_acq = 0) THEN p.wholesale_price ELSE od.wholesale_price END)*od.product_quantity)) as margine,
			SUM((CASE WHEN (od.wholesale_price = 0 AND od.no_acq = 0) THEN p.wholesale_price ELSE od.wholesale_price END)*od.product_quantity) as tot_acquisti,
			m.id_manufacturer as manufacturer
			FROM `orders` o
			LEFT JOIN `order_detail` od ON o.id_order = od.id_order
			LEFT JOIN order_history oh ON oh.id_order_state =6 AND oh.id_order = o.id_order
			LEFT JOIN `product` p ON p.id_product = od.product_id
			LEFT JOIN `manufacturer` m ON (p.id_manufacturer = m.id_manufacturer)
			WHERE (o.valid = 1 OR (o.valid = 0 AND ((o.module = "bankwire"  OR o.module = "other_payment"))))
			 AND oh.id_order_state IS NULL
			AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-'.$mese_inizio.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$mese_fine.'-'.$td).' 23:59:59"
			GROUP BY m.id_manufacturer ORDER BY orderSum2 DESC');
			
			$manufacturer_count_chart = 0;
			$manufacturer_data_chart = '';
			$other_manufacturer_tot = 0;
			
			foreach ($man_month_stats as $manrow) {
				
				if($manufacturer_count_chart < 12)
				{
					if($manrow['manufacturer'] == 105)
						$manufacturer_data_chart .= '["'.$manrow['name'].'", '.(number_format($manrow['orderSum']+$tot_bdl,2,".","")).'],';
					else
						$manufacturer_data_chart .= '["'.$manrow['name'].'", '.number_format($manrow['orderSum'],2,".","").'],';
					$manufacturer_count_chart++;
				}	
				else
				{
					if($manrow['manufacturer'] == 105)
						$other_manufacturer_tot += number_format(($manrow['orderSum']+$this->t13),2,".","");
					else
						$other_manufacturer_tot += number_format($manrow['orderSum'],2,".","");	
					
				}
			}	
			
			echo '<script type="text/javascript">

				 // Load the Visualization API and the corechart package.

				google.charts.setOnLoadCallback(drawChart2);
			  // Callback that creates and populates a data table,
			  // instantiates the pie chart, passes in the data and
			  // draws it.
			  function drawChart2() {

				// Create the data table.
				var data = new google.visualization.DataTable();
				data.addColumn("string", "Topping");
				data.addColumn("number", "Slices");
				data.addRows([
				  '.$manufacturer_data_chart.'
				  ["Altri", '.$other_manufacturer_tot.']
				]);

				// Set chart options
				var options = {"title":"Grafico costruttori",
				is3D: true,
				legend: "labeled",
				pieSliceText: "none",
				chartArea:{left:20,top:0,width:"97%",height:"97%"},
				"width":600,
				"height":300};

				// Instantiate and draw our chart, passing in some options.
				var chart = new google.visualization.PieChart(document.getElementById("chart_costruttori_div"));
				chart.draw(data, options);
			  }
			</script>';
			echo '<div id="chart_costruttori_div"></div>';
			
			$stats_ticket_todo = Db::getInstance()->getRow(
			"select 
			
			(SELECT AVG(TIMESTAMPDIFF(MINUTE, date_add, date_upd)/60.0 - 12*(TIMESTAMPDIFF(DAY,'2000-01-03',date_upd)-TIMESTAMPDIFF(DAY,'2000-01-03',date_add)) - 24*(TIMESTAMPDIFF(WEEK, '2000-01-03',date_upd)-TIMESTAMPDIFF(WEEK,'2000-01-03',date_add))) FROM customer_thread WHERE id_contact != 7 AND date_add BETWEEN '".date($homeStats['year'].'-'.$mese_inizio.'-01')." 00:00:00' AND '".date($homeStats['year'].'-'.$mese_fine.'-'.$td)." 23:59:59' AND status = 'closed') as hourstkt,
			(SELECT AVG(TIMESTAMPDIFF(MINUTE, date_add, date_upd)/60.0 - 12*(TIMESTAMPDIFF(DAY,'2000-01-03',date_upd)-TIMESTAMPDIFF(DAY,'2000-01-03',date_add)) - 24*(TIMESTAMPDIFF(WEEK, '2000-01-03',date_upd)-TIMESTAMPDIFF(WEEK,'2000-01-03',date_add))) FROM action_thread WHERE date_add BETWEEN '".date($homeStats['year'].'-'.$mese_inizio.'-01')." 00:00:00' AND '".date($homeStats['year'].'-'.$mese_fine.'-'.$td)." 23:59:59' AND status = 'closed') as hourstodo,
			
			(SELECT AVG(TIMESTAMPDIFF(MINUTE, date_add, date_upd)/60.0 - 12*(TIMESTAMPDIFF(DAY,'2000-01-03',date_upd)-TIMESTAMPDIFF(DAY,'2000-01-03',date_add)) - 24*(TIMESTAMPDIFF(WEEK, '2000-01-03',date_upd)-TIMESTAMPDIFF(WEEK,'2000-01-03',date_add))) FROM form_prevendita_thread WHERE date_add BETWEEN '".date($homeStats['year'].'-'.$mese_inizio.'-01')." 00:00:00' AND '".date($homeStats['year'].'-'.$mese_fine.'-'.$td)." 23:59:59' AND status = 'closed') as hoursprv,
			
			(SELECT AVG(MINUTE(TIMEDIFF(date_upd, date_add))) FROM action_thread WHERE date_add BETWEEN '".date($homeStats['year'].'-'.$mese_inizio.'-01')." 00:00:00' AND '".date($homeStats['year'].'-'.$mese_fine.'-'.$td)." 23:59:59' AND status = 'closed') as minutestodo");
			
			echo '<br /><br />
			
			<table>
			<tr>
			<td style="width:320px">
			<a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminStats&token='.$tokenStats.'&module=statsbestproducts"  style="font-size:20px" target="_blank">  I migliori prodotti</a>
			</td>
			<td>
			<a href="https://www.ezdirect.it/ezadmin/index.php?tab=Invenduti&token='.Tools::getAdminToken('Invenduti'.(int)(Tab::getIdFromClassName('Invenduti')).(int)($cookie->id_employee)).'" style="font-size:20px">Prodotti invenduti</a>
			</td>
			</tr>
			<tr>
			<td>
			<a href="#"  style="font-size:20px" target="_blank">Migliori sorgenti</a>
			</td>
			<td>
			<a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminStats&token='.$tokenStats.'&module=statstrackvisits" style="font-size:20px"> Visitatori online</a>
			</td>
			</tr>
			<tr>
			<td>
			<a href="https://www.ezdirect.it/ezadmin/index.php?tab=Speciali&token='.Tools::getAdminToken('Speciali'.(int)(Tab::getIdFromClassName('Speciali')).(int)($cookie->id_employee)).'" style="font-size:20px" target="_blank"> Prezzi speciali vendita</a>
			</td>
			<td>
			<a href="https://www.ezdirect.it/ezadmin/index.php?tab=SpecialiAcquisto&token='.Tools::getAdminToken('SpecialiAcquisto'.(int)(Tab::getIdFromClassName('SpecialiAcquisto')).(int)($cookie->id_employee)).'" style="font-size:20px" target="_blank"> Prezzi speciali acquisto</a>
			</td>
			</tr>
			<tr>
			<td>
			<a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomerThreads&token='.Tools::getAdminToken('AdminCustomerThreads'.(int)(Tab::getIdFromClassName('AdminCustomerThreads')).(int)($cookie->id_employee)).'" style="font-size:20px" target="_blank">Planner</a>
			</td>
			<td>
			<a href="#" style="font-size:20px">Formazione</a>
			</td>
			</tr>
			</table>
			';
			
			$target_anno = Db::getInstance()->getValue('SELECT configurazione FROM trgt WHERE descrizione = "Anno" AND anno = "'.date('Y').'"');
			$target_anno = unserialize($target_anno);
			
			$vendite_mese = Db::getInstance()->getValue('
			SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) as total
			FROM '._DB_PREFIX_.'orders o JOIN customer c ON o.id_customer = c.id_customer LEFT JOIN order_history oh ON oh.id_order_state =6 AND oh.id_order = o.id_order
			LEFT JOIN order_detail od ON o.id_order = od.id_order WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire"  OR o.module = "other_payment"))) AND oh.id_order_state IS NULL	AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-'.$mese_inizio.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$mese_fine.'-'.$td).' 23:59:59"')+$tot_bdl;
			
			switch($homeStats['period'])
			{
				case 'Jan': case 'Feb': case 'Mar': $quarter_first_month = '01'; $quarter_last_month = '03'; $target_quarter = 'q1'; break;
				case 'Apr': case 'May': case 'Jun': $quarter_first_month = '04'; $quarter_last_month = '06'; $target_quarter = 'q2'; break;
				case 'Jul': case 'Aug': case 'Sep': $quarter_first_month = '07'; $quarter_last_month = '09'; $target_quarter = 'q3'; break;
				case 'Oct': case 'Nov': case 'Dec': $quarter_first_month = '10'; $quarter_last_month = '12'; $target_quarter = 'q4'; break;
				case 'q1': case 'q2': case 'q3': case 'q4': $quarter_first_month = $mese_inizio; $quarter_last_month = $mese_fine; $target_quarter = $homeStats['period']; break;
				case 'anno': $quarter_first_month = '01';  $quarter_last_month = '12'; $target_quarter = 'anno'; break;
				default: $quarter_first_month = '01'; $quarter_last_month = '03'; $target_quarter = 'q1'; break;
			}
			
			$tot_bdl_quarter = Db::getInstance()->getValue('SELECT SUM(importo) FROM '._DB_PREFIX_.'bdl WHERE importo > 0 AND invio_contabilita > 0 AND rif_ordine = 0 AND date_add  BETWEEN "'.date($homeStats['year'].'-'.$quarter_first_month.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$quarter_last_month.'-'.$td).' 23:59:59"');
			
			$vendite_quarter = Db::getInstance()->getValue('
			SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) as total
			FROM '._DB_PREFIX_.'orders o JOIN customer c ON o.id_customer = c.id_customer LEFT JOIN order_history oh ON oh.id_order_state =6 AND oh.id_order = o.id_order
			LEFT JOIN order_detail od ON o.id_order = od.id_order WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire"  OR o.module = "other_payment"))) AND oh.id_order_state IS NULL	AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-'.$quarter_first_month.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$quarter_last_month.'-'.$td).' 23:59:59"')+$tot_bdl_quarter;
			
			$vendite_anno = (Db::getInstance()->getValue('
			SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) as total
			FROM '._DB_PREFIX_.'orders o JOIN customer c ON o.id_customer = c.id_customer LEFT JOIN order_history oh ON oh.id_order_state =6 AND oh.id_order = o.id_order
			LEFT JOIN order_detail od ON o.id_order = od.id_order WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire"  OR o.module = "other_payment"))) AND oh.id_order_state IS NULL	AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-01-01').' 00:00:00" AND "'.date($homeStats['year'].'-12-31').' 23:59:59"')+$tot_bdl_anno);
			
			$servizi_mese = Db::getInstance()->getValue('
			SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) as total
			FROM '._DB_PREFIX_.'orders o JOIN customer c ON o.id_customer = c.id_customer LEFT JOIN order_history oh ON oh.id_order_state =6 AND oh.id_order = o.id_order
			LEFT JOIN order_detail od ON o.id_order = od.id_order WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire"  OR o.module = "other_payment"))) AND oh.id_order_state IS NULL	AND (od.category_id = 248 OR od.category_id = 249 OR od.category_id = 119)	AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-'.$mese_inizio.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$mese_fine.'-'.$td).' 23:59:59"')+$tot_bdl;
			
			$servizi_anno = (Db::getInstance()->getValue('
			SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) as total
			FROM '._DB_PREFIX_.'orders o JOIN customer c ON o.id_customer = c.id_customer LEFT JOIN order_history oh ON oh.id_order_state =6 AND oh.id_order = o.id_order
			LEFT JOIN order_detail od ON o.id_order = od.id_order WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire"  OR o.module = "other_payment"))) AND oh.id_order_state IS NULL	AND (od.category_id = 248 OR od.category_id = 249 OR od.category_id = 119) AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-01-01').' 00:00:00" AND "'.date($homeStats['year'].'-12-31').' 23:59:59"')+$tot_bdl_anno);
			
			$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
			
			echo '</div><div style="width:30%; margin-left:4%; float:left">
			<h2>Statistiche in breve</h2>
			
			<table class="table" style="width:280px">
			<tr><th colspan="2">Su base periodo - <a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminStats&token='.Tools::getAdminToken('AdminStats'.(int)(Tab::getIdFromClassName('AdminStats')).(int)($cookie->id_employee)).'">vai a stats</a></th></tr>
			<tr><td>Ricavi</td><td style="text-align:right">'.Tools::displayPrice($vendite_mese,1).'</td></tr>
			
			<tr><td>di cui Amazon</td><td style="text-align:right">'.Tools::displayPrice(Db::getInstance()->getValue('SELECT sum(total_products) FROM orders o JOIN commissioni_marketplace cm ON o.id_order = cm.id_order WHERE cm.tipo = "Amazon" AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-'.$mese_inizio.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$mese_fine.'-'.$td).' 23:59:59"'),1).'</td></tr>
			
			<tr><td>di cui Servizi</td><td style="text-align:right">'.Tools::displayPrice($servizi_mese,1).'</td></tr>
			
			<tr><td>Ordini</td><td style="text-align:right">'.Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'orders o LEFT JOIN order_history oh ON oh.id_order_state =6 AND oh.id_order = o.id_order WHERE (o.valid = 1 OR (o.valid = 0 AND ((o.module = "bankwire"  OR o.module = "other_payment"))))  AND oh.id_order_state IS NULL AND o.date_add BETWEEN "'.date($homeStats['year'].'-'.$mese_inizio.'-01').' 00:00:00" AND "'.date($homeStats['year'].'-'.$mese_fine.'-'.$td).' 23:59:59"').'</td></tr>
			<tr><td>Target quarter % ('.number_format($target_anno[$target_quarter],2,",",".").')</td><td style="text-align:right">'.number_format((($vendite_quarter*100)/$target_anno[$target_quarter]),2,",","").'%</td></tr>
			</table>
			
			<br />
			<table class="table" style="width:280px">
			<tr><th colspan="2">Su base anno - <a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminStats&token='.Tools::getAdminToken('AdminStats'.(int)(Tab::getIdFromClassName('AdminStats')).(int)($cookie->id_employee)).'">vai a stats</a></th></tr>
			<tr><td>Ricavi</td><td style="text-align:right">'.Tools::displayPrice($vendite_anno,1).'</td></tr>
			<tr><td>di cui Amazon</td><td style="text-align:right">'.Tools::displayPrice(Db::getInstance()->getValue('SELECT sum(total_products) FROM orders o JOIN commissioni_marketplace cm ON o.id_order = cm.id_order WHERE cm.tipo = "Amazon" AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-01-01').' 00:00:00" AND "'.date($homeStats['year'].'-12-31').' 23:59:59"'),1).'</td></tr>
			<tr><td>di cui Servizi</td><td style="text-align:right">'.Tools::displayPrice($servizi_anno,1).'</td></tr>
			<tr><td>Ordini</td><td style="text-align:right">'.Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'orders o LEFT JOIN order_history oh ON oh.id_order_state =6 AND oh.id_order = o.id_order WHERE (o.valid = 1 OR (o.valid = 0 AND ((o.module = "bankwire"  OR o.module = "other_payment"))))  AND oh.id_order_state IS NULL AND o.date_add BETWEEN "'.date($homeStats['year'].'-01-01').' 00:00:00" AND "'.date($homeStats['year'].'-12-31').' 23:59:59"').'</td></tr>
			<tr><td>Target anno % ('.number_format($target_anno['totale'],2,",",".").')</td><td style="text-align:right">'.number_format((($vendite_anno*100)/$target_anno['totale']),2,",","").'%</td></tr>
			</table>
			
			<br />
			<table class="table" style="width:280px">
			<tr><th colspan="2">Clienti (totale)</th></tr>
			
			<tr><td><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&filter=1&token='.$tokenCustomers.'">Business</a></td><td style="text-align:right">'.Db::getInstance()->getValue('SELECT count(id_customer) FROM customer WHERE id_default_group = 1').'</td></tr>
			
			<tr><td><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&filter=8&token='.$tokenCustomers.'">Top Business</a></td><td style="text-align:right">'.Db::getInstance()->getValue('SELECT count(id_customer) FROM customer WHERE id_default_group = 8').'</td></tr>
			
			<tr><td><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&filter=3&token='.$tokenCustomers.'">Rivenditori 1</a></td><td style="text-align:right">'.Db::getInstance()->getValue('SELECT count(id_customer) FROM customer WHERE id_default_group = 3').'</td></tr>
			<tr><td><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&filter=3&token='.$tokenCustomers.'">Rivenditori 2</a></td><td style="text-align:right">'.Db::getInstance()->getValue('SELECT count(id_customer) FROM customer WHERE id_default_group = 22').'</td></tr>
			<tr><td><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&filter=15&token='.$tokenCustomers.'">Rich. riv</a></td><td style="text-align:right">'.Db::getInstance()->getValue('SELECT count(id_customer) FROM customer WHERE id_default_group = 15').'</td></tr>
			<tr><td><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&filter=15&token='.$tokenCustomers.'">Call center</a></td><td style="text-align:right">'.Db::getInstance()->getValue('SELECT count(id_customer) FROM customer WHERE id_default_group = 4').'</td></tr>
			
			<tr><td><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&filter=5&token='.$tokenCustomers.'">PA</a></td><td style="text-align:right">'.Db::getInstance()->getValue('SELECT count(id_customer) FROM customer WHERE id_default_group = 5').'</td></tr>
			
			<tr><td><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&filter=16&token='.$tokenCustomers.'">Amazon</a></td><td style="text-align:right">'.Db::getInstance()->getValue('SELECT count(id_customer) FROM customer WHERE id_default_group = 16').'</td></tr>
			<tr><td><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&filter=17&token='.$tokenCustomers.'">ePrice</a></td><td style="text-align:right">'.Db::getInstance()->getValue('SELECT count(id_customer) FROM customer WHERE id_default_group = 17').'</td></tr>
			<tr><td><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&filter=18&token='.$tokenCustomers.'">Hotel</a></td><td style="text-align:right">'.Db::getInstance()->getValue('SELECT count(id_customer) FROM customer WHERE id_default_group = 18').'</td></tr>
			<tr><td><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&filter=18&token='.$tokenCustomers.'">Top Hotel</a></td><td style="text-align:right">'.Db::getInstance()->getValue('SELECT count(id_customer) FROM customer WHERE id_default_group = 19 AND id_default_group != 12').'</td></tr>
			<tr><td><strong>Totale aziende</strong></td><td style="text-align:right"><strong>'.Db::getInstance()->getValue('SELECT count(id_customer) FROM customer WHERE id_default_group != 2 AND id_default_group != 11 AND id_default_group != 12').'</strong></td></tr>
			<tr><td><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&filter=2&token='.$tokenCustomers.'">Privati</a></td><td style="text-align:right">'.Db::getInstance()->getValue('SELECT count(id_customer) FROM customer WHERE id_default_group = 2').'</td></tr>
			<tr><td><strong>Totale anagrafiche clienti</strong></td><td style="text-align:right"><strong>'.Db::getInstance()->getValue('SELECT count(id_customer) FROM customer WHERE id_default_group != 11 AND id_default_group != 12').'</strong></td></tr>
			<tr><td><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&filter=11&token='.$tokenCustomers.'">Fornitori</a></td><td style="text-align:right">'.Db::getInstance()->getValue('SELECT count(id_customer) FROM customer WHERE id_default_group = 11').'</td></tr>
			<tr><td><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&filter=12&token='.$tokenCustomers.'">Distributori</a></td><td style="text-align:right">'.Db::getInstance()->getValue('SELECT count(id_customer) FROM customer WHERE id_default_group = 12').'</td></tr>
			<tr><td><strong>Totale anagrafiche</strong></td><td style="text-align:right"><strong>'.Db::getInstance()->getValue('SELECT count(id_customer) FROM customer  ').'</strong></td></tr>
			</table>
			<br />
			
			<table class="table" style="width:280px">
			<tr><th colspan="2">Efficienza (questo periodo: '.$mese.')</th></tr>
			<tr><td>Chiusura ticket</td><td style="text-align:right">'.number_format($stats_ticket_todo['hourstkt'],0).' ore lav.</td></tr>
			<tr><td>Chiusura preventivo</td><td style="text-align:right">'.number_format($stats_ticket_todo['hoursprv'],0).' ore lav.</td></tr>
			<tr><td>Chiusura todo</td><td style="text-align:right">'.number_format($stats_ticket_todo['hourstodo'],0).' ore lav.</td></tr>
			</table>
			
			<script type="text/javascript">
			
			function getManufacturerTarget(id_manufacturer, id_element)
			{
				var costruttori = "";
				var arrayCostruttori = document.getElementsByClassName("select-costruttori");
				for (var i = 0; i < arrayCostruttori.length; ++i) {
					var item = (arrayCostruttori[i].value);  
					costruttori += item+";";
				}

				$.ajax({
					url:"ajax.php?getManufacturerQuarter=y",
					type: "POST",
					data: {
						costruttori: costruttori,
						getManufacturerQuarter: "y",
						id_manufacturer: id_manufacturer
					},
					success:function(resp){
						document.getElementById(id_element).innerHTML = resp + "%";
					},
					error: function(xhr,stato,errori){
						return "Errore!";
					}
				});
				
			}
			
			function accoda(){
				var possible = "0123456789";
				var randomid = "";
				for( var i=0; i < 11; i++ ) {
					randomid += possible.charAt(Math.floor(Math.random() * possible.length));
				}
						
				$(\'<tr id="tr_manufacturer_\'+randomid+\'"><td><select class="select-costruttori" id="choose-manufacturer-\'+randomid+\'" onchange="getManufacturerTarget(this.value, \\\'m-choose-manufacturer-\'+randomid+\'\\\')"><option value="0">-- Seleziona costruttore --</option>';	$tutti_i_costruttori = Db::getInstance()->executeS('SELECT * FROM manufacturer ORDER BY name ASC');foreach($tutti_i_costruttori as $c) { echo '<option value="'.$c['id_manufacturer'].'">'.$c['name'].'</option>'; } echo '</select><a href="javascript:void(0)" onclick="javascript:rimuovi(\'+randomid+\')";><img src="../img/admin/delete.gif" alt="Togli" title="Togli" /> </a></td><td style="text-align:right" id="m-choose-manufacturer-\'+randomid+\'"></td></tr>\').appendTo("#table_target");
				
			}

			function rimuovi(id){
				document.getElementById(\'tr_manufacturer_\'+id).innerHTML = "";
				
				var costruttori = "";
				var arrayCostruttori = document.getElementsByClassName("select-costruttori");
				for (var i = 0; i < arrayCostruttori.length; ++i) {
					var item = (arrayCostruttori[i].value);  
					costruttori += item+";";
				}
				
				$.ajax({
					url:"ajax.php?removeManufacturerFromCookie=y",
					type: "POST",
					data: { 
						costruttori: costruttori
					},
					success:function(resp){
						alert("Costruttore rimosso con successo");
					},
					error: function(xhr,stato,errori){
						return "Errore!";
					}
				});
				
			}
						
			</script>
			
			<br />
			<table class="table" style="width:280px" id="table_target">
			<tr><th colspan="3">Target anno - <a href="javascript:void(0)" onclick="javascript:accoda()";>Clicca per aggiungere costr. <img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" /> </a></th></tr>
			
			
			';
			
			
			$home_manufacturers = unserialize(Db::getInstance()->getValue('SELECT value FROM fixed_ck WHERE id_employee = '.$cookie->id_employee.' AND name="home_manufacturers"'));
			
			foreach($home_manufacturers as $hm)
			{
				/*$vendite_anno_hm = (Db::getInstance()->getValue('
				SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) as total
				FROM '._DB_PREFIX_.'orders o JOIN customer c ON o.id_customer = c.id_customer LEFT JOIN order_history oh ON oh.id_order_state =6 AND oh.id_order = o.id_order
				LEFT JOIN order_detail od ON o.id_order = od.id_order WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire"  OR o.module = "other_payment"))) AND oh.id_order_state IS NULL	AND od.manufacturer_id = '.$hm.' AND o.`date_add` BETWEEN "'.date('Y-01-01').' 00:00:00" AND "'.date('Y-12-31').' 23:59:59"'));*/
				
				$vendite_anno_hm = (Db::getInstance()->getValue('
				SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) as total
				FROM '._DB_PREFIX_.'orders o JOIN customer c ON o.id_customer = c.id_customer LEFT JOIN order_history oh ON oh.id_order_state =6 AND oh.id_order = o.id_order
				LEFT JOIN order_detail od ON o.id_order = od.id_order WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire"  OR o.module = "other_payment"))) AND oh.id_order_state IS NULL	AND od.manufacturer_id = '.$hm.' AND o.`date_add` BETWEEN "'.date($homeStats['year'].'-01-01').' 00:00:00" AND "'.date($homeStats['year'].'-12-31').' 23:59:59"'));
				
				
				if($hm == 105)
				{
					/*$tot_bdl_anno_hm = Db::getInstance()->getValue('SELECT SUM(importo) FROM '._DB_PREFIX_.'bdl WHERE importo > 0 AND invio_contabilita > 0 AND rif_ordine = 0 AND date_add  BETWEEN "'.date('Y-01-01').' 00:00:00" AND "'.date('Y-12-31').' 23:59:59"');*/
					
					$tot_bdl_anno_hm = Db::getInstance()->getValue('SELECT SUM(importo) FROM '._DB_PREFIX_.'bdl WHERE importo > 0 AND invio_contabilita > 0 AND rif_ordine = 0 AND date_add BETWEEN "'.date($homeStats['year'].'-01-01').' 00:00:00" AND "'.date($homeStats['year'].'-12-31').' 23:59:59"');
					$vendite_anno_hm += $tot_bdl_anno_hm;
				}	
			
				$target_anno_hm = Db::getInstance()->getValue('SELECT configurazione FROM trgt WHERE descrizione = "m-'.$hm.'-'.$homeStats['year'].'" AND anno = "'.$homeStats['year'].'"');
				$target_anno_hm = unserialize($target_anno_hm);
				
				$target_percent_hm = ($vendite_anno_hm*100)/($target_anno_hm['q1']+$target_anno_hm['q2']+$target_anno_hm['q3']+$target_anno_hm['q4']);
				
				echo '
				<tr id="tr_manufacturer_'.$hm.'"><td>
				<select class="select-costruttori" id="choose-manufacturer-'.$hm.'" onchange="getManufacturerTarget(this.value, \'m-choose-manufacturer-'.$hm.'\')"><option value="0">-- Seleziona costruttore --</option>
				';
				$tutti_i_costruttori = Db::getInstance()->executeS('SELECT * FROM manufacturer ORDER BY name ASC');
				foreach($tutti_i_costruttori as $c)
					echo '<option value="'.$c['id_manufacturer'].'" '.($hm == $c['id_manufacturer'] ? 'selected="selected"' : '').'>'.$c['name'].'</option>';
				
				echo '
				</select> <br />Target: '.number_format($target_anno_hm['q1']+$target_anno_hm['q2']+$target_anno_hm['q3']+$target_anno_hm['q4'],2,",",".").'</td><td> <a href="javascript:void(0)" onclick="javascript:rimuovi('.$hm.')";><img src="../img/admin/delete.gif" alt="Togli" title="Togli" /> </a>
				</td><td style="text-align:right" id="m-choose-manufacturer-'.$hm.'">'.number_format($target_percent_hm,2,",","").'%</td></tr>';
			}	
			
			echo '
			</table>
			
			
			</div>';
			
			
			echo '<div class="clear"></div>';
			
		}
		else
		{	
			if($cookie->profile == 7)
			{
				
				echo '<h1>Area Agente</h1>';
				
			}
			else
			{	
				$tokenTickets = Tools::getAdminToken('AdminCustomerThreads'.(int)(Tab::getIdFromClassName('AdminCustomerThreads')).(int)($cookie->id_employee));
				
				echo "<h2>Servizio clienti</h2>";
				echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."&type=2'><img src='../img/ticket-a-120.jpg' alt='Ticket amministrativi' title='Ticket amministrativi' /></a>";
				echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."&type=8'><img src='../img/ticket-d-120.jpg' alt='Ticket ordini' title='Ticket ordini' /></a>";
				echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."&type=4'><img src='../img/ticket-t-120.jpg' alt='Ticket assistenza' title='Ticket assistenza' /></a>";
				echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."&type=preventivo'><img src='../img/ticket-p-120.jpg' alt='Ticket commerciale' title='Ticket commerciale' /></a>";
				echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."&type=3'><img src='../img/ticket-v-120.jpg' alt='Ticket rivenditori' title='Ticket rivenditori' /></a>";
				echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."'><img src='../img/ticket-tutti-120.jpg' alt='Tutti i ticket' title='Tutti i ticket' /></a>";
				echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."&data=oggi'><img src='../img/attivita-oggi-120.gif' alt='Attivita di oggi' title='Attivita di oggi' /></a>";
				
				$tokenCatalog = Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee));

				$ultime_attivita_impiegato_group = Db::getInstance()->executeS('SELECT DISTINCT id_attivita FROM storico_attivita WHERE  id_employee = '.$cookie->id_employee.' ORDER BY data_attivita DESC LIMIT 60');
				
				$ultime_att_gr = '(';
				foreach($ultime_attivita_impiegato_group as $u)
					$ultime_att_gr .= $u['id_attivita'].',';

				$ultime_att_gr .= '9999999999999)';		
				$ultime_attivita_impiegato = Db::getInstance()->executeS('SELECT * FROM storico_attivita WHERE tipo_attivita != "AE" AND id_attivita IN '.$ultime_att_gr.' GROUP BY id_attivita ORDER BY data_attivita DESC ');
				
				// classe "ultime-att" per paginazione in testa
				
				echo '<h2 id="ultime_attivita" style="display:block;float:left">Ultime attivit&agrave;</h2><div style="clear:both"></div><table class="table paginated paginated-20" style="width:100%"><tr><th>Tipo attivit&agrave;</th><th>Id attivit&agrave;</th><th colspan="4">Cliente</th><th>In carico a</th><th>Status</th><th>Data attivit&agrave;</th></tr><tbody>';
				$uaii = 0;
				
				foreach($ultime_attivita_impiegato as $uai)
				{
					if($uai['tipo_attivita'] == 'T')
					{
						$id_contact = Db::getInstance()->getValue('SELECT id_contact FROM customer_thread WHERE id_customer_thread = '.$uai['id_attivita']);
						if($id_contact == 7)
							$uai['tipo_attivita'] = 'M';
					}	
					switch($uai['tipo_attivita'])
					{
						case 'A': $tipo_attivita_uai = 'Azione'; $table = 'action_thread'; $attivita_key = 'id_action'; $id_attivita = 'TODO'.$uai['id_attivita']; break;
						case 'P': $tipo_attivita_uai = 'Richiesta preventivo'; $table = 'form_prevendita_thread'; $attivita_key = 'id_thread';  $id_attivita = Customer::trovaSigla($uai['id_attivita'], 'P'); break;
						case 'C': $tipo_attivita_uai = 'Carrello'; $table = 'cart'; $attivita_key = 'id_cart'; $id_attivita = $uai['id_attivita']; break;
						case 'O': $tipo_attivita_uai = 'Ordine'; $table = 'orders'; $attivita_key = 'id_order'; $id_attivita = $uai['id_attivita']; break;
						case 'L': $tipo_attivita_uai = 'BDL'; $table = 'bdl'; $attivita_key = 'id_bdl';  $id_attivita = 'BDL'.$uai['id_attivita']; break;
						case 'T': $tipo_attivita_uai = 'Ticket'; $table = 'customer_thread'; $attivita_key = 'id_customer_thread';  $id_attivita = Customer::trovaSigla($uai['id_attivita'], 'T'); break;	
						case 'M': $tipo_attivita_uai = 'Messaggio'; $table = 'customer_thread'; $attivita_key = 'id_customer_thread';  $id_attivita = Customer::trovaSigla($uai['id_attivita'], 'M'); break;	
					}
					
					$id_customer = Db::getInstance()->getValue('SELECT id_customer FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']);
					
					$uai_link = 'index.php?tab=AdminCustomers&id_customer='.$id_customer.'&viewcustomer';
					$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
					
					$uai_employee_act = Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname,1),".") impiegato FROM employee WHERE id_employee = '.$uai['id_employee']);
					
					switch($uai['tipo_attivita'])
					{
						case 'A': $uai_link.= '&id_action='.$uai['id_attivita'].'&viewaction&token='.$tokenCustomers.'&tab-container-1=10'; $uai_status = Db::getInstance()->getValue('SELECT status FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); $in_carico_a = Db::getInstance()->getValue('SELECT action_to FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); break;
						case 'P': $uai_link.= '&id_thread='.$uai['id_attivita'].'&viewpreventivo&token='.$tokenCustomers.'&tab-container-1=7'; $uai_status = Db::getInstance()->getValue('SELECT status FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); $in_carico_a = Db::getInstance()->getValue('SELECT id_employee FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); break;
						case 'C': $uai_link.= '&id_cart='.$uai['id_attivita'].'&viewcart&token='.$tokenCustomers.'&tab-container-1=4'; $uai_status = Db::getInstance()->getValue('SELECT visualizzato FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); $in_carico_a = Db::getInstance()->getValue('SELECT in_carico_a FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); break;
						case 'O': $uai_link.= '&id_order='.$uai['id_attivita'].'&vieworder&token='.$tokenCustomers.'&tab-container-1=4'; $uai_status = Db::getInstance()->getValue('SELECT name FROM order_state_lang WHERE id_lang = 5 AND id_order_state = (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = '.$uai['id_attivita'].' GROUP BY moh.`id_order`))'); $in_carico_a = ''; break;
						case 'L': $uai_link.= '&edit_bdl='.$uai['id_attivita'].'&viewbdl&token='.$tokenCustomers.'&tab-container-1=14'; $uai_status = Db::getInstance()->getValue('SELECT (CASE WHEN a.rif_ordine > 0 THEN "closed" WHEN a.id_bdl < 384 THEN "closed" WHEN a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 0 THEN "pending1"	WHEN a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 1	THEN "closed" WHEN a.invio_controllo = 2 AND a.invio_contabilita = 2 THEN "closed" WHEN a.rif_ordine = 0 AND a.invio_controllo = 0 THEN "open" ELSE "open" END) stato  FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); $in_carico_a = Db::getInstance()->getValue('SELECT effettuato_da FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); break;
						case 'T': $uai_link.= '&id_customer_thread='.$uai['id_attivita'].'&viewticket&token='.$tokenCustomers.'&tab-container-1=6'; $uai_status = Db::getInstance()->getValue('SELECT status FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); $in_carico_a = Db::getInstance()->getValue('SELECT id_employee FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); break;
						case 'M': $uai_link.= '&id_mex='.$uai['id_attivita'].'&viewmessage&token='.$tokenCustomers.'&tab-container-1=7'; $uai_status = Db::getInstance()->getValue('SELECT status FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); $in_carico_a = Db::getInstance()->getValue('SELECT id_employee FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); break;	
						
					}	
					
					$uai_employee = Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname,1),".") AS impiegato FROM employee WHERE id_employee = '.$in_carico_a);
					
					switch($uai_status)
					{
						case 'open': $uai_status = '<img src="../img/admin/status_red.gif" alt="Aperto" />Aperto'; break;
						case 'closed': $uai_status = '<img src="../img/admin/status_green.gif" alt="Chiuso" /> Chiuso'; break;
						case 'pending1': $uai_status = '<img src="../img/admin/status_orange.gif" alt="In lavorazione" />In lavorazione'; break;
						case 'pending2': $uai_status = '<img src="../img/admin/status_orange.gif" alt="In lavorazione" />In lavorazione'; break;
						case 1: $uai_status = '<img src="../img/admin/enabled.gif" alt="Letto" title="Letto" />Letto'; break;
						case 0: $uai_status = '<img src="../img/admin/red_no.gif" alt="Non letto" title="Non letto" />Non letto'; break;
						case 999: $uai_status = 'Carrello Amazon convertito'; break;
						default: $uai_status; break;
						
					}	
					
					$dati_cliente = Db::getInstance()->getValue('SELECT (CASE WHEN is_company = 1 THEN company ELSE CONCAT(firstname," ",lastname) END) AS cliente FROM customer WHERE id_customer = '.$id_customer);
					
					if($uai['tipo_attivita'] == 'T')
					{
						$check_resp = 1;
						$id_contact = Db::getInstance()->getValue('SELECT id_contact FROM customer_thread WHERE id_customer_thread = '.$uai['id_attivita']);
						if($id_contact == 7)
						{
							$tipo_attivita_uai = 'Messaggio';
							$count_msg = Db::getInstance()->getValue('SELECT count(id_customer_message) FROM customer_message WHERE id_employee = 0 AND id_customer_thread = '.$uai['id_attivita']);
							if($count_msg == 0)
								$check_resp = 0;
						}	
					}
					
					if($uai['tipo_attivita'] == 'P')
					{
						$id_contact = Db::getInstance()->getValue('SELECT tipo_richiesta FROM form_prevendita_thread WHERE id_thread = '.$uai['id_attivita']);
						if($id_contact == 'tirichiamiamonoi')
							$tipo_attivita_uai = 'Ti richiamiamo noi';
					}
					
					if(substr($uai['desc_attivita'],0,22) == 'Ha salvato il carrello')
						$desc_attivita = 'Ha lavorato sul carrello';
					else
						$desc_attivita = $uai['desc_attivita'];
					
					if(is_numeric(substr($desc_attivita, -1)))
					{	
						$employee = filter_var($desc_attivita, FILTER_SANITIZE_NUMBER_INT);
						$name_employee = Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM employee WHERE id_employee = '.$employee);
						$desc_attivita = 'Ha messo l\'attivit&agrave; in carico a '.$name_employee;
					}
					
					echo '<tr '.($uaii%2 == 0 ? '' : 'style="background-color:#f5f5f5"').' class="riga"><td id="td_espandi_'.$uai['id_attivita'].'"><img src="../img/admin/add.gif" alt="Espandi" style="cursor:pointer" title="Espandi" id="espandi_'.$uai['id_attivita'].'" />'.$tipo_attivita_uai.'
					';
					
					
					if($uai['tipo_attivita'] == 'C' || $uai['tipo_attivita'] == 'O')
					{
						echo '
						<script type="text/javascript">
						$(document).ready(function(){
							$("#espandi_'.$uai['id_attivita'].', #td_espandi_'.$uai['id_attivita'].'").click(function(){
								
								if($(this).attr("src") == ("../img/admin/add.gif")) {
									$(this).attr("src", "../img/admin/forbbiden.gif");
									$.ajax({
									url:"ajax.php?get'.($uai['tipo_attivita'] == 'O' ? 'Order' : 'Cart').'Display=y",
									type: "POST",
									data: {
									id_'.($uai['tipo_attivita'] == 'O' ? 'order' : 'cart').': '.$uai['id_attivita'].'
									},
									success:function(r){
										/* $(".riga").show(); */
										/*$("#orderTempDisplay").html(r);*/
										$("#cart_riga_show_'.$uai['id_attivita'].'_td").html(r);
										$("#cart_riga_show_'.$uai['id_attivita'].'").show();
													
										/* $("#riga_'.$uai['id_attivita'].'").toggle(); */
									},
									error: function(xhr,stato,errori){
										alert("Errore durante l\'operazione:"+xhr.status);
									}
									});
								}
								else {
									$(this).attr("src", "../img/admin/add.gif");
									$("#cart_riga_show_'.$uai['id_attivita'].'").hide();
									/*$("#orderTempDisplay").html("");*/
								}
								return false;
							});
							
						});
					</script>';
					}
					else
					{
						echo '<script type="text/javascript">
						$(document).ready(function(){
							$("#espandi_'.$uai['id_attivita'].', #td_espandi_'.$uai['id_attivita'].'").click(function(){
								$("tr.sub_'.$uai['id_attivita'].'").toggle();
								if($("tr.sub_'.$uai['id_attivita'].':visible").length) {
									$(this).attr("src", "../img/admin/forbbiden.gif");	
								}
								else {
									$(this).attr("src", "../img/admin/add.gif");
									$("tr.sub_'.$uai['id_attivita'].'").hide();
								}
								return false;
							});
							
						});
						</script>';
						
					}
					echo '
					</td>
					
					<td><a href="'.$uai_link.'">'.$id_attivita.'</a></td><td colspan="4"><a href="'.$uai_link.'">'.$dati_cliente.'</a></td><td><a href="'.$uai_link.'">'.$uai_employee.'</a></td><td><a href="'.$uai_link.'">'.($tipo_attivita_uai == 'Messaggio' && $check_resp == 0 ? '' : $uai_status).'</a></td><td><a href="'.$uai_link.'">'.Tools::displayDate($uai['data_attivita'],5,true).'</a></td></tr>';
					
					if($uai['tipo_attivita'] == 'O')
					{
						echo '
						<tr id="cart_riga_show_'.$uai['id_attivita'].'" class="subs sub_'.$uai['id_attivita'].'" style="display:none '.($uaii%2 == 0 ? '' : '; background-color:#f5f5f5"').'"><td id="cart_riga_show_'.$uai['id_attivita'].'_td" colspan="9"></td></tr>
						';
					}
					else if($uai['tipo_attivita'] == 'C')
					{
						
						echo '
						<tr id="cart_riga_show_'.$uai['id_attivita'].'" class="subs sub_'.$uai['id_attivita'].'" style="display:none '.($uaii%2 == 0 ? '' : '; background-color:#f5f5f5"').'"><td id="cart_riga_show_'.$uai['id_attivita'].'_td" colspan="9"></td></tr>
						';
					}	
					else
					{
						
						echo '
						<tr class="subs sub_'.$uai['id_attivita'].'" style="display:none '.($uaii%2 == 0 ? '' : '; background-color:#f5f5f5"').'"><td colspan="9">'.$uai_employee_act.' '.$desc_attivita.'</td></tr>
						';
					}
					
					$uaii++;
				}
				
				echo '</tbody></table><br /><div id="orderTempDisplay"></div>';
				
			

				echo Module::hookExec('backOfficeHome');
			}	
		}
	}
}


