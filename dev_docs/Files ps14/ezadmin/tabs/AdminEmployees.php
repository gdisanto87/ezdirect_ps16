<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminEmployees extends AdminTab
{
 	/** @var array profiles list */
	private $profilesArray = array();

	public function __construct()
	{
	 	global $cookie;

	 	$this->table = 'employee';
	 	$this->className = 'Employee';
	 	$this->lang = false;
	 	$this->edit = true;
	 	$this->delete = true;
 		$this->_select = 'pl.`name` AS profile';
		$this->_join = 'LEFT JOIN `'._DB_PREFIX_.'profile` p ON a.`id_profile` = p.`id_profile`
		LEFT JOIN `'._DB_PREFIX_.'profile_lang` pl ON (pl.`id_profile` = p.`id_profile` AND pl.`id_lang` = '.(int)($cookie->id_lang).')';

		$profiles = Profile::getProfiles((int)($cookie->id_lang));
		if (!$profiles)
			$this->_errors[] = Tools::displayError('No profile');
		else
			foreach ($profiles AS $profile)
				$this->profilesArray[$profile['name']] = $profile['name'];

		$this->fieldsDisplay = array(
		'id_employee' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 25),
		'lastname' => array('title' => $this->l('Last name'), 'width' => 130),
		'firstname' => array('title' => $this->l('First name'), 'width' => 130),
		'email' => array('title' => $this->l('E-mail address'), 'width' => 180),
		'profile' => array('title' => $this->l('Profile'), 'width' => 90, 'type' => 'select', 'select' => $this->profilesArray, 'filter_key' => 'pl!name'),
		'active' => array('title' => $this->l('Can log in'), 'align' => 'center', 'active' => 'status', 'type' => 'bool'));

		$this->optionTitle = $this->l('Employees options');
		$this->_fieldsOptions = array(
			'PS_PASSWD_TIME_BACK' => array('title' => $this->l('Password regenerate:'), 'desc' => $this->l('Security minimum time to wait to regenerate a new password'), 'cast' => 'intval', 'size' => 5, 'type' => 'text', 'suffix' => ' '.$this->l('minutes')),
			'PS_BO_ALLOW_EMPLOYEE_FORM_LANG' => array('title' => $this->l('Memorize form language:'), 'desc' => $this->l('Allow employees to save their own default form language'), 'cast' => 'intval', 'type' => 'select', 'identifier' => 'value', 'list' => array(
				'0' => array('value' => 0, 'name' => $this->l('No')),
				'1' => array('value' => 1, 'name' => $this->l('Yes'))
			))
		);

		parent::__construct();
	}

	protected function _childValidation()
	{
		if (!($obj = $this->loadObject(true)))
			return false;
		$email = $this->getFieldValue($obj, 'email');
		if (!Validate::isEmail($email))
	 		$this->_errors[] = Tools::displayError('Invalid e-mail');
		elseif (Employee::employeeExists($email) AND !Tools::getValue('id_employee'))
			$this->_errors[] = Tools::displayError('An account already exists for this e-mail address:').' '.$email;
	}

	public function displayForm($isMainTab = true)
	{
		global $currentIndex, $cookie, $employee;
		parent::displayForm();

		if (!($obj = $this->loadObject(true)))
			return;

		$profiles = Profile::getProfiles((int)$cookie->id_lang);
		// If the current employee is not an Admin, don't make it possible to select the Admin profile
		if ($employee->id_profile != 1)
			foreach ($profiles as $i => $profile)
				if ($profile['id_profile'] == 1)
				{
					unset($profiles[$i]);
					break;
				}
		echo '
		<script type="text/javascript" src="yetii.js"></script>
		';
		
		$c_emp = new Employee(Tools::getValue('id_employee'));
		
		echo '<p style="font-size:16px; margin-top:-10px">
			<strong>Dipendente:</strong> '.$c_emp->firstname.' '.$c_emp->lastname.'</p>
			<p>';
		
		
		echo '<script type="text/javascript" src="'._PS_JS_DIR_.'jquery/jquery-colorpicker.js"></script>
		 	 <script type="text/javascript">
				var employeePage = true;
		 	 </script>

		<div id="tab-container-1">
			<ul id="tab-container-2-nav" style="margin-left:30px; margin-top:-27px; width:80%">
			<li '.($_GET['tab-container-1'] == 1 || !isset($_GET['tab-container-1']) ? 'class="activeli" ' : '').' style="margin-left:-35px"><a href="'.$currentIndex.'&id_employee='.Tools::getValue('id_employee').'&updateemployee&token='.$this->token.'&tab-container-1=1"><strong>Ultime attivit&agrave;</strong></a></li>
			<li '.($_GET['tab-container-1'] == 2 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_employee='.Tools::getValue('id_employee').'&updateemployee&token='.$this->token.'&tab-container-1=2"><strong>Opzioni</strong></a></li>
			</ul>';
			
			echo '<ul id="tab-container-1-nav" style="list-style-type:none; display:none">
			<li style="margin-left:-35px"><a href="'.$currentIndex.'&id_employee='.$customer->id.'&viewemployee&token='.$this->token.'#attivita"><strong>Attivit&agrave;</strong></a></li>
			<li><a href="'.$currentIndex.'&id_employee='.$customer->id.'&viewemployee&token='.$this->token.'#opzioni"><strong>Opzioni</strong></a></li></ul>';
			
			echo '<div class="tab1" id="attivita">';
			
				echo '<br /><br /><h2><a>'.$this->l('Attività recenti').' </a></h2>';
			
			$ultime_cose = Db::getInstance()->executeS("SELECT id_thread as id, tipo_richiesta as tipo, id_customer as cliente, id_employee as in_carico, status, (CASE WHEN((status = 'open' OR status = 'pending1') AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM form_prevendita_thread WHERE id_employee = ".Tools::getValue('id_employee')." UNION 
			SELECT id_customer_thread as id, id_contact as tipo, id_customer as cliente, id_employee as in_carico, status, (CASE WHEN((status = 'open' OR status = 'pending1') AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM customer_thread WHERE id_employee = ".Tools::getValue('id_employee')." UNION 
			
			SELECT id_action as id, action_type as tipo, id_customer as cliente, action_to as in_carico, status, (CASE WHEN((status = 'open' OR status = 'pending1') AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM action_thread WHERE action_to = ".Tools::getValue('id_employee')." UNION 
			SELECT id_cart as id, 'Carrello' as tipo, id_customer as cliente, in_carico_a as in_carico, '' AS status, '' as scaduto, date_add, date_upd FROM cart WHERE in_carico_a = ".Tools::getValue('id_employee')." ORDER BY date_add DESC");
			
			//SELECT id_cart as id, 'Carrello' as tipo, id_customer as cliente, in_carico_a as in_carico, '' AS status, '' as scaduto, date_add, date_upd FROM cart WHERE id_cart NOT IN (SELECT id_cart FROM orders WHERE id_customer = ".$_GET['id_customer'].") AND id_customer = ".$_GET['id_customer']." ORDER BY date_add DESC");
			
			$date_cose = array();
			foreach ($ultime_cose as $key => $row)
			{
				$date_cose[$key] = $row['date_upd'];
			}
			array_multisort($date_cose, SORT_DESC, $ultime_cose);
			
			$customersIndex = 'index.php?tab=AdminCustomers';
			
			echo "<table class='table'>
			<tr><th>Tipo 
			<a href='index.php?tab=AdminEmployees&id_employee=".Tools::getValue('id_employee')."&updateemployee&orderby=tipo&orderway=asc&token=".$this->token."&tab-container-1=1'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
				<a href='index.php?tab=AdminEmployees&id_employee=".Tools::getValue('id_employee')."&updateemployee&orderby=tipo&orderway=desc&token=".$this->token."&tab-container-1=1'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a>
			</th><th>ID</th><th>Cliente</th><th>Tipo cliente</th><th>Creato da</th><!-- <th>In carico a</th> --><th>Conv.</th><th>Status</th><th>Scaduto</th><th>Oggetto</th><th>Totale</th><th>Data apertura</th><th>Ultima modifica</th></tr>";
			
			for($i=0;$i<count($ultime_cose);$i++) {
				
				$customer = new Customer($ultime_cose[$i]['cliente']);
				$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
				
				if ($ultime_cose[$i]['tipo'] == 7){
					$ctrl_mex = Db::getInstance()->getValue("SELECT COUNT(id_customer_message) FROM customer_thread ct JOIN customer_message cm ON ct.id_customer_thread = cm.id_customer_thread WHERE cm.id_employee = 0 AND ct.id_customer_thread = ".$ultime_cose[$i]['id']."");	
						
					if($ctrl_mex >= 1) {
						
					} else {
						$ultime_cose[$i]['status'] = ''; 
						$ultime_cose[$i]['scaduto'] = '';
					}

				}
					
				if($i<5) {
					$includilo = 'yes';
				}
				else {
					if($ultime_cose[$i]['scaduto'] == 'SCADUTO') {
						$includilo = 'yes';
					}
					else {
						$includilo = 'no';
					}
				
				}
				$conv = "--";
				if($includilo == 'yes') {
					$in_carico = $ultime_cose[$i]['in_carico'];
					
					if(is_numeric($in_carico)) {
						$newinc = new Employee($in_carico);
						$in_carico = $newinc->firstname;
					}
					
					$tipo = $ultime_cose[$i]['tipo'];
					if ($tipo == 4){
						$sigla = "T"; $tipo = "Ticket assistenza"; $ahrefcosa = '<a href="'.$customersIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$tokenCustomers.'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
					}
					else if ($tipo == 2){
						$sigla = "A"; $tipo = "Ticket contabilita"; $ahrefcosa = '<a href="'.$customersIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$tokenCustomers.'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
					} 
					else if ($tipo == 3){
						$sigla = "V"; $tipo = "Ticket rivenditori"; $ahrefcosa = '<a href="'.$customersIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$tokenCustomers.'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
					} 
					else if ($tipo == 7){
						$sigla = "M"; $tipo = "Messaggio"; $ahrefcosa = '<a href="'.$customersIndex.'&id_customer='.$customer->id.'&viewmessage&id_mex='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$tokenCustomers.'&tab-container-1=7">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
						
						$ctrl_mex = Db::getInstance()->getValue("SELECT COUNT(id_customer_message) FROM customer_thread ct JOIN customer_message cm ON ct.id_customer_thread = cm.id_customer_thread WHERE cm.id_employee = 0 AND ct.id_customer_thread = ".$ultime_cose[$i]['id']."");	
						
						if($ctrl_mex >= 1) {
							$status = ''; $scaduto = '';
						}
					}
					else if ($tipo == 8){
						$sigla = "D"; $tipo = "Ticket ordini"; $ahrefcosa = '<a href="'.$customersIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$tokenCustomers.'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
					} 
					else if ($tipo == 9){
						$sigla = "S"; $tipo = "RMA"; $ahrefcosa = '<a href="'.$customersIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$tokenCustomers.'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
					} 
					else if ($tipo == 'tirichiamiamonoi') {	
						$sigla = "R"; $tipo = "Ti richiamiamo noi"; $ahrefcosa = '<a href="'.$customersIndex.'&id_customer='.$customer->id.'&viewpreventivo&id_thread='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$tokenCustomers.'&tab-container-1=7">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM form_prevendita_message WHERE id_thread = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
					}
					else if ($tipo == 'preventivo') {	
						$sigla = "P"; $tipo = "Preventivo"; $ahrefcosa = '<a href="'.$customersIndex.'&id_customer='.$customer->id.'&viewpreventivo&id_thread='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$tokenCustomers.'&tab-container-1=7">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM form_prevendita_message WHERE id_thread = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
					}
					else {
						if($tipo == 'Carrello') {
							$ahrefcosa = '<a href="'.$customersIndex.'&id_customer='.$customer->id.'&viewcart&id_cart='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$tokenCustomers.'&tab-container-1=4">'; $creato_da = Db::getInstance()->getValue("SELECT created_by FROM cart WHERE id_cart = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
							$in_carico = $ultime_cose[$i]['in_carico'];
							
							$n_ord_c = Db::getInstance()->getValue("SELECT id_order FROM orders WHERE id_cart = ".$ultime_cose[$i]['id']."");
							if($n_ord_c > 0) 
							{
								$conv = "<img src='http://www.ezdirect.it/img/admin/enabled.gif' alt='Si' title='Si' />";
							}
							else
							{
								$conv = "<img src='http://www.ezdirect.it/img/admin/red_no.gif' alt='No' title='No' />";
								
							}
					
							if(is_numeric($in_carico)) {
								$newinc = new Employee($in_carico);
								$in_carico = $newinc->firstname;
							}
						}
						else if ($tipo == 'Ordine') {
							$ahrefcosa = '<a href="'.$customersIndex.'&id_customer='.$customer->id.'&vieworder&id_order='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$tokenCustomers.'&tab-container-1=4">'; $creato_da = "";
						}
						else if ($tipo != 'Ordine' && $tipo != 'Carrello' && tipo != '') {
							$sigla = 'TODO'; $ahrefcosa = '<a href="'.$customersIndex.'&id_customer='.$customer->id.'&viewaction&id_action='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$tokenCustomers.'&tab-container-1=10">'; $creato_da = Db::getInstance()->getValue("SELECT action_from FROM action_thread WHERE id_action = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
						}
						else {
							$ahrefcosa = '<a href="#">';
						}
						
						$sigla = "";
					}
					
					$anno = substr($ultime_cose[$i]['date_upd'],2,2);
					
					if($sigla != '') {
						$id_cosa = $sigla.$anno.$ultime_cose[$i]['id'];
					}
					else {
						$id_cosa = $ultime_cose[$i]['id'];
					}
					
					$status = $ultime_cose[$i]['status']; 
					if($status == 'closed') {
						$status = '<img src="../img/admin/status_green.gif" alt="Chiuso" /> Chiuso';
					}
					else if($status == 'pending1' || $status == 'pending2') {
						$status = '<img src="../img/admin/status_orange.gif" alt="In lavorazione" />In lavorazione';
					}
					else if($status == 'open') {
						$status = '<img src="../img/admin/status_red.gif" alt="Aperto" />Aperto';
					}
					else if(is_numeric($status)) {
						$status = Db::getInstance()->getValue("SELECT name FROM order_state_lang WHERE id_lang = 5 AND id_order_state = ".$status."");
					}
					
					if($tipo == 'Carrello') {
						$oggetto_uc = Db::getInstance()->getValue("SELECT name FROM cart WHERE id_cart = ".$ultime_cose[$i]['id']."");
						$cartID = new Cart((int)($ultime_cose[$i]['id']));
						$summaryD = $cartID->getSummaryDetails();
						$importo_uc = Tools::displayPrice($summaryD['total_products'],1);
					}
					else {
						$oggetto_uc = "--";
						$importo_uc = "--";
					}
					
					
					echo "<tr>";
					echo "<td>".$ahrefcosa.$tipo."</a></td>";
					echo "<td>".$ahrefcosa.$id_cosa."</a></td>";
					
					$is_company = Db::getInstance()->getValue('SELECT is_company FROM customer WHERE id_customer = '.$ultime_cose[$i]['cliente'].'');
					if($is_company == 1)
					{
						$group = Db::getInstance()->getValue('SELECT id_default_group FROM customer WHERE id_customer = '.$ultime_cose[$i]['cliente'].'');
						
						if($group == 3)
							$tipo_cliente = 'Rivenditore';
						else
							$tipo_cliente = 'Azienda';
							
						$nome_cliente = Db::getInstance()->getValue('SELECT company FROM customer WHERE id_customer = '.$ultime_cose[$i]['cliente'].'');
					}
					else 
					{
						$nome_cliente = Db::getInstance()->getRow('SELECT firstname, lastname FROM customer WHERE id_customer = '.$ultime_cose[$i]['cliente'].'');
						$nome_cliente = $nome_cliente['firstname']." ".$nome_cliente['lastname'];
						$tipo_cliente = 'Privato';
					
					}
					echo "<td>".$ahrefcosa.$nome_cliente."</a></td>";
					echo "<td>".$ahrefcosa.$tipo_cliente."</a></td>";
					echo "<td>".$ahrefcosa.$creato_da."</a></td>";
					echo "<!-- <td>".$ahrefcosa.$in_carico."</a></td> -->";
					echo "<td>".$conv."</td>";
					echo "<td>".$ahrefcosa.$status."</a></td>";
					echo "<td>".$ahrefcosa.$ultime_cose[$i]['scaduto']."</a></td>";
					echo "<td>".$ahrefcosa.$oggetto_uc."</a></td>";
					echo "<td style='text-align:right'>".$ahrefcosa.$importo_uc."</a></td>";
					echo "<td>".$ahrefcosa.Tools::DisplayDate($ultime_cose[$i]['date_add'],$cookie->id_lang,false)."</a></td>";
					echo "<td>".$ahrefcosa.Tools::DisplayDate($ultime_cose[$i]['date_upd'],$cookie->id_lang,false)."</a></td>";
					echo "</tr>";
				}
				else {
				}
			}
		
			echo "</table><br />";
			
	
			
			echo '</div>';
			
			
			echo '<div class="tab1" id="opzioni"><br /><br /><h2>Opzioni dipendente</h2>
		<form action="'.$currentIndex.'&submitAdd'.$this->table.'=1&token='.$this->token.((int)$this->tabAccess['view'] ? '' : '&updateemployee&id_employee='.(int)$obj->id).'" method="post" enctype="multipart/form-data" autocomplete="off">
		'.($obj->id ? '<input type="hidden" name="id_'.$this->table.'" value="'.$obj->id.'" />' : '').'
		'.((int)$this->tabAccess['view'] ? '' : '<input type="hidden" name="back" value="'.$currentIndex.'&token='.$this->token.'&updateemployee&id_employee='.(int)$obj->id.'" />').'
			<fieldset class="width3"><legend><img src="../img/admin/nav-user.gif" />'.$this->l('Dipendente').'</legend>
				<label>'.$this->l('Last name:').' </label>
				<div class="margin-form">
					<input type="text" size="33" name="lastname" value="'.htmlentities($this->getFieldValue($obj, 'lastname'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup>
				</div>
				<label>'.$this->l('First name:').' </label>
				<div class="margin-form">
					<input type="text" size="33" name="firstname" value="'.htmlentities($this->getFieldValue($obj, 'firstname'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup>
				</div>
				<label>'.$this->l('Password:').' </label>
				<div class="margin-form">
					<input type="password" size="33" name="passwd" value="" /> <sup>*</sup>
					<p>'.($obj->id ? $this->l('Leave blank if you do not want to change your password') : $this->l('Min. 8 characters; use only letters, numbers or').' -_').'</p>
				</div>
				<label>'.$this->l('E-mail address:').' </label>
				<div class="margin-form">
					<input type="text" size="33" name="email" value="'.htmlentities($this->getFieldValue($obj, 'email'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup>
				</div><div class="clear">&nbsp;</div>
				<label>'.$this->l('Back office color:').' </label>
				<div class="margin-form">';
				// Note : width= fix Firefox 4 display bug related to colorpicker librarie
				echo '<input type="color" width="50px" data-hex="true" class="color mColorPickerInput" name="bo_color" value="'.htmlentities($this->getFieldValue($obj, 'bo_color'), ENT_COMPAT, 'UTF-8').'" />
					<p>'.$this->l('Back office background will be displayed in this color. HTML colors only (e.g.,').' "lightblue", "#CC6600")</p>
				</div><div class="clear">&nbsp;</div>
				<label>'.$this->l('Language:').' </label>
				<div class="margin-form">
					<select name="id_lang">';
		foreach (Language::getLanguages() as $lang)
			echo '		<option value="'.(int)$lang['id_lang'].'" '.($this->getFieldValue($obj, 'id_lang') == $lang['id_lang'] ? 'selected="selected"' : '').'>'.Tools::htmlentitiesUTF8($lang['name']).'</option>';
		echo '		</select> <sup>*</sup>
				</div><div class="clear">&nbsp;</div>
				<label>'.$this->l('Theme:').' </label>
				<div class="margin-form">
					<select name="bo_theme">';
		$path = dirname(__FILE__).'/../themes/';
		foreach (scandir($path) as $theme)
			if ($theme[0] != '.' AND is_dir($path.$theme) AND file_exists($path.$theme.'/admin.css'))
				echo '	<option value="'.Tools::htmlentitiesUTF8($theme).'" '.($this->getFieldValue($obj, 'bo_theme') == $theme ? 'selected="selected"' : '').'>'.Tools::htmlentitiesUTF8($theme).'</option>';
		echo '		</select> <sup>*</sup>
				</div>';
		if ((int)$this->tabAccess['edit'])
		{
			echo '<div class="clear">&nbsp;</div>
				<label>'.$this->l('UI mode:').' </label>
				<div class="margin-form">
					<input type="radio" name="bo_uimode" id="uimode_on" value="hover" '.($this->getFieldValue($obj, 'bo_uimode') == 'hover' ? 'checked="checked" ' : '').'/>
					<label class="t" for="uimode_on">'.$this->l('Hover on tabs').'</label>
					<input type="radio" name="bo_uimode" id="uimode_off" value="click" '.($this->getFieldValue($obj, 'bo_uimode') == 'click' ? 'checked="checked" ' : '').'/>
					<label class="t" for="uimode_off">'.$this->l('Click on tabs').'</label>
				</div><div class="clear">&nbsp;</div>
				<label>'.$this->l('Show screencast:').' </label>
				<div class="margin-form">
					<input type="radio" name="bo_show_screencast" id="bo_show_screencast_on" value="1" '.($this->getFieldValue($obj, 'bo_show_screencast') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_on"> <img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="bo_show_screencast" id="bo_show_screencast_off" value="0" '.(!$this->getFieldValue($obj, 'bo_show_screencast') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_off"> <img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label>
					<p>'.$this->l('Show the welcome video on the dashbord of the back office').'</p>
				</div>
				<label>'.$this->l('Status:').' </label>
				<div class="margin-form">
					<input type="radio" name="active" id="active_on" value="1" '.($this->getFieldValue($obj, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_on"> <img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="active" id="active_off" value="0" '.(!$this->getFieldValue($obj, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_off"> <img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label>
					<p>'.$this->l('Allow or disallow this employee to log into this Back Office').'</p>
				</div>
				<label>'.$this->l('Profile:').' </label>
				<div class="margin-form">
					<select name="id_profile">
						<option value="">'.$this->l('-- Choose --').'</option>';
						foreach ($profiles AS $profile)
						 	echo '<option value="'.$profile['id_profile'].'"'.($profile['id_profile'] === $this->getFieldValue($obj, 'id_profile') ? ' selected="selected"' : '').'>'.$profile['name'].'</option>';
				echo '</select> <sup>*</sup>
				</div>';
		}
		echo '<div class="clear">&nbsp;</div>
				<center>
					<input type="submit" value="'.$this->l('   Save   ').'" name="submitAdd'.$this->table.'" class="button" />
				</center><div class="clear">&nbsp;</div>
				<div class="small"><sup>*</sup> '.$this->l('Required field').'</div>
			</fieldset>
		</form></div>
		
		</div> <!-- fine tab container 1 -->
		<script type="text/javascript">
		var tabber1 = new Yetii({
		id: "tab-container-1",
		tabclass: "tab1",
		});
		</script>';
	}

	public function postProcess()
	{
		global $cookie, $employee;

		/* PrestaShop demo mode */
		if (_PS_MODE_DEMO_)
		{
			$this->_errors[] = Tools::displayError('This functionnality has been disabled.');
			return;
		}
		/* PrestaShop demo mode*/
	
		if (Tools::isSubmit('deleteemployee') OR Tools::isSubmit('status') OR Tools::isSubmit('statusemployee'))
		{			
			if ($cookie->id_employee == Tools::getValue('id_employee'))
			{
				$this->_errors[] = Tools::displayError('You cannot disable or delete your own account.');
				return false;
			}

			$edited_employee = new Employee(Tools::getValue('id_employee'));
			if ($edited_employee->isLastAdmin())
			{
					$this->_errors[] = Tools::displayError('You cannot disable or delete the last administrator account.');
					return false;
			}
		}
		elseif (Tools::isSubmit('submitAddemployee'))
		{
			$edited_employee = new Employee((int)Tools::getValue('id_employee'));

			// Employee is changing its own profile
			if (!(int)$this->tabAccess['edit'])
				$_POST['id_profile'] = $_GET['id_profile'] = $edited_employee->id_profile;
			// Employee tries to change profile to Admin without being Admin himself
			elseif ($_POST['id_profile'] == 1 && $employee->id_profile != 1)
			{
				$this->_errors[] = Tools::displayError('Only an Administrator can give the Administrator profile.');
				return false;
			}

			if ($edited_employee->isLastAdmin())
			{
				if  (Tools::getValue('id_profile') != (int)_PS_ADMIN_PROFILE_)
				{
					$this->_errors[] = Tools::displayError('You should have at least one employee in the administrator group.');
					return false;
				}

				if (Tools::getvalue('active') == 0)
				{
					$this->_errors[] = Tools::displayError('You cannot disable or delete the last administrator account.');
					return false;
				}
			}
		}
		elseif (Tools::isSubmit('submitDelemployee'))
		{
			if (in_array($cookie->id_employee, $_POST['employeeBox']))
			{
				$this->_errors[] = Tools::displayError('You cannot disable or delete your own account.');
				return false;
			}
			if (in_array(Employee::getLastAdmin(true), $_POST['employeeBox']))
			{
				$this->_errors[] = Tools::displayError('You cannot disable or delete the last administrator account.');
				return false;
			}
		}
		return parent::postProcess();
	}
}
