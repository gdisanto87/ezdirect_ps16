<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14632 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminComments extends AdminTab
{
		public function __construct()
	{
	 	$this->table = 'product_comment';
	 	$this->lang = false;
	 	$this->edit = false;
		$this->delete = true;
	 	$this->view = true;
		$this->requiredDatabase = true;

		$this->_select = '
		(CASE c.is_company
		WHEN 1
		THEN c.company
		ELSE
		CONCAT(c.firstname," ",c.lastname)
		END
		) cliente,
		
		(CASE c.is_company
		WHEN 1
		THEN c.vat_number
		WHEN 0
		THEN c.tax_code
		ELSE c.tax_code
		END
		) piva_cf';
		
		$this->_join = 'JOIN customer c ON a.id_customer = c.id_customer JOIN product p ON a.id_product = p.id_product JOIN product_lang pl ON a.id_product = pl.id_product';
		
		$genders = array(1 => $this->l('M'), 2 => $this->l('F'), 9 => $this->l('?'));
 		$this->fieldsDisplay = array(
		'id_product_comment' => array('title' => $this->l('ID'), 'align' => 'center', 'font-size' =>'12px', 'width' => 25),
		
		'cliente' => array('title' => $this->l('Cliente'), 'font-size' =>'14px',  'width' => 80, 'tmpTableFilter' => true),
		'piva_cf' => array('title' => $this->l('PI/CF'), 'font-size' =>'14px',  'width' => 80, 'tmpTableFilter' => true),
		
		'title' => array('title' => $this->l('Titolo rec.'), 'width' => 100, 'tmpTableFilter' => true),
		'name' => array('title' => $this->l('Prodotto'), 'width' => 100, 'tmpTableFilter' => true),
		
		'grade' => array('title' => $this->l('Voto'), 'width' => 20, 'tmpTableFilter' => true),
		
		'content' => array('title' => $this->l('Commento'), 'width' => 100, 'tmpTableFilter' => true),
		'reference' => array('title' => $this->l('Cod. prod.'), 'width' => 60, 'align' => 'center', 'tmpTableFilter' => true),
		'date_add' => array('title' => $this->l('Data rec.'), 'width' => 30, 'type' => 'date',  'tmpTableFilter' => true),

		//'connect' => array('title' => $this->l('Connection'), 'width' => 60, 'type' => 'datetime', 'search' => false)
		);
		
		$this->_where = 'group by a.id_product_comment';


		parent::__construct();
	}
	

	
		public function display()
		{	
			global $cookie, $currentIndex;
			echo "<h1>Recensioni</h1>";
			
		
			if (isset($_GET['view'.$this->table])) {
			$this->view_product_comment();
			}
			
			else if (isset($_GET['delete'])) {
			
				Db::getInstance()->executeS("DELETE FROM product_comment WHERE id_product_comment = ".Tools::getValue('id_product_comment')."");
				Tools::redirectAdmin($currentIndex.'&token='.$this->token.'&conf=4');
			}
			
			else if(isset($_GET['approve'])) {
			
			
				Db::getInstance()->executeS("UPDATE product_comment SET validate = 1 WHERE id_product_comment = ".Tools::getValue('id_product_comment')."");
				Tools::redirectAdmin($currentIndex.(Tools::getIsset('viewproduct_comment') ? '&id_product_comment='.Tools::getValue('id_product_comment').'&viewproduct_comment' : '').'&token='.$this->token.'&conf=4');
			
			
			}
			
			else if(isset($_GET['disapprove'])) {
			
				Db::getInstance()->executeS("UPDATE product_comment SET validate = 0 WHERE id_product_comment = ".Tools::getValue('id_product_comment')."");
				Tools::redirectAdmin($currentIndex.(Tools::getIsset('viewproduct_comment') ? '&id_product_comment='.Tools::getValue('id_product_comment').'&viewproduct_comment' : '').'&token='.$this->token.'&conf=4');
			
			
			}
			
		else
		{
			if (Tools::isSubmit('abilitaselezionati')) {
			
				foreach($_POST['product_commentBox'] as $val) {
				
					Db::getInstance()->executeS("UPDATE product_comment SET validate = 1 WHERE id_product_comment = ".$val."");
				
				}
				Tools::redirectAdmin($currentIndex.'&token='.($token ? $token : $this->token));
			}
			
			if (Tools::isSubmit('disabilitaselezionati')) {
				
				foreach($_POST['product_commentBox'] as $val) {
				
					Db::getInstance()->executeS("UPDATE product_comment SET validate = 0 WHERE id_product_comment = ".$val."");
				
				}
				Tools::redirectAdmin($currentIndex.'&token='.($token ? $token : $this->token));
			}
		
			$this->getList((int)($cookie->id_lang), !Tools::getValue($this->table.'Orderby') ? 'date_add' : NULL, !Tools::getValue($this->table.'Orderway') ? 'DESC' : NULL);
			$currency = new Currency((int)(Configuration::get('PS_CURRENCY_DEFAULT')));
			$this->displayListHeader();
			$this->displayListContent();
		//	$this->displayListFooter();
			$this->displayListPagination($this->token);
			
			echo "<input type='submit' class='button' value='Approva selezionati' name='abilitaselezionati' /><br /><br />";
			echo "<input type='submit' class='button' value='Disapprova selezionati' name='disabilitaselezionati' />";
		}
		
			/*
				echo '<span style="float: left;">
						<input src="../img/admin/list-prev2.gif" onclick="getE(\'submitFiltercustomer\').value=1" type="image">
						&nbsp; <input src="../img/admin/list-prev.gif" onclick="getE(\'submitFiltercustomer\').value=1" type="image"> Pagina <b>2</b> / 544
						<input src="../img/admin/list-next.gif" onclick="getE(\'submitFiltercustomer\').value=3" type="image">
						 &nbsp;<input src="../img/admin/list-next2.gif" onclick="getE(\'submitFiltercustomer\').value=544" type="image">			| Mostrata
						<select name="pagination"><option value="20">20</option><option value="50" selected="selected">50</option><option value="100">100</option><option value="300">300</option>
						</select>
						/ 27183 risultato/i
					</span>
					<span style="float: right;">
						<input name="submitResetcustomer" value="Resetta" class="button" type="submit">
						<input id="submitFilterButton_customer" name="submitFilter" value="Filtra" class="button" type="submit">
					</span>
					<span class="clear"></span>';
					
				$recensioni = Db::getInstance()->executeS("SELECT c.firstname, c.lastname, c.company, c.codice_spring, c.vat_number, c.tax_code, c.is_company, p.reference, pl.name, pc.* FROM product_comment pc JOIN customer c ON pc.id_customer = c.id_customer JOIN product p ON pc.id_product = p.id_product JOIN product_lang pl ON pc.id_product = pl.id_product WHERE pl.id_lang = ".$cookie->id_lang." ORDER BY pc.date_add DESC");
				
				echo "<table class='table'>
				<thead>
				<tr><th>ID rec.</th><th>Cliente</th><th>Spring</th><th>PI/CF</th><th>Titolo rec.</th><th>Prodotto</th><th>Cod. prod.</th><th>Data rec.</th><th>Azioni</th>
				</thead><tbody>";
				
				foreach($recensioni as $rec) {
			
						
					echo '<tr onclick="document.location = \'index.php?tab=AdminComments&id_product_comment='.$rec['id_product_comment'].'&token='.$this->token.'\'" style="cursor:pointer">';
					
					echo "<td>".$rec['id_product_comment']."</td>";
					echo "<td>".($rec['is_company'] == 1 ? $rec['company'] : $rec['firstname']." ".$rec['lastname'])."</td>";
					echo "<td>".$rec['codice_spring']."</td>";
					echo "<td>".($rec['is_company'] == 1 ? $rec['vat_number'] : $rec['tax_code'])."</td>";	
					echo "<td>".$rec['title']."</td>";
					echo "<td>".$rec['name']."</td>";
					echo "<td>".$rec['reference']."</td>";
					echo "<td>".Tools::displayDate($rec['date_add'], $cookie->id_lang)."</td>";
					echo "<td>
						<a href='".$currentIndex."&id_product_comment=".$rec['id_product_comment']."&token=".$this->token."'><img src='../img/admin/details.gif' alt='Vedi' /></a>
						".($rec['validate'] == 0 ? "<a href='".$currentIndex."&id_product_comment=".$rec['id_product_comment']."&token=".$this->token."&approve'><img src='../img/admin/enabled.gif' alt='Abilita' /></a>" : "<a href='".$currentIndex."&id_product_comment=".$rec['id_product_comment']."&token=".$this->token."&disapprove'><img src='../img/admin/forbbiden.gif' alt='Disabilita' /></a>")."
						<a href='".$currentIndex."&id_product_comment=".$rec['id_product_comment']."&token=".$this->token."&delete'><img src='../img/admin/delete.gif' alt='Cancella' /></a>
					</td>";
					echo "</tr>";
					
					
					
				}
					
				echo "</tbody></table>";
			*/
		}
		
	public function view_product_comment()
	{
	
		global $currentIndex, $cookie;
		
		if (isset($_GET['delete'])) {
			
				Db::getInstance()->executeS("DELETE FROM product_comment WHERE id_product_comment = ".Tools::getValue('id_product_comment')."");
				Tools::redirectAdmin($currentIndex.'&token='.$this->token.'&conf=4');
			}
			
			else if(isset($_GET['approve'])) {
			
			
				Db::getInstance()->executeS("UPDATE product_comment SET validate = 1 WHERE id_product_comment = ".Tools::getValue('id_product_comment')."");
				Tools::redirectAdmin($currentIndex.(Tools::getIsset('viewproduct_comment') ? '&id_product_comment='.Tools::getValue('id_product_comment').'&viewproduct_comment' : '').'&token='.$this->token.'&conf=4');
			
			
			}
			
			else if(isset($_GET['disapprove'])) {
			
				Db::getInstance()->executeS("UPDATE product_comment SET validate = 0 WHERE id_product_comment = ".Tools::getValue('id_product_comment')."");
				Tools::redirectAdmin($currentIndex.(Tools::getIsset('viewproduct_comment') ? '&id_product_comment='.Tools::getValue('id_product_comment').'&viewproduct_comment' : '').'&token='.$this->token.'&conf=4');
			
			
			}
			
			$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
			$tokenCatalogExFeatures = Tools::getAdminToken('AdminCatalogExFeatures'.(int)Tab::getIdFromClassName('AdminCatalogExFeatures').(int)$cookie->id_employee);
		
		$rec = Db::getInstance()->getRow("SELECT c.firstname, c.lastname, c.company, c.codice_spring, c.vat_number, c.tax_code, c.is_company, p.reference, pl.name, pc.* FROM product_comment pc JOIN customer c ON pc.id_customer = c.id_customer JOIN product p ON pc.id_product = p.id_product JOIN product_lang pl ON pc.id_product = pl.id_product WHERE pl.id_lang = ".$cookie->id_lang." AND pc.id_product_comment = ".Tools::getValue('id_product_comment')." ORDER BY pc.date_add DESC");
		
		echo "<h2>Recensione n. ".$rec['id_product_comment']." di ".($rec['is_company'] == 1 ? $rec['company'] : $rec['firstname']." ".$rec['lastname'])." aggiunta in data ".Tools::displayDate($rec['date_add'], $cookie->id_lang)."</h2>";
	
		echo "<div style='float:left'>";
		echo "<table class='table' style='width:600px'><thead>
				<tr><th style='width:200px'>Cliente</th><th style='width:50px'>Codice Spring</th><th style='width:50px'>PI/CF</th><th>Nickname</th><th>Ruolo in azienda</th></tr>
				</thead><tbody>";
		echo "<tr>";
		echo "<td><a target='_blank' href='index.php?tab=AdminCustomers&id_customer=".$rec['id_customer'].'&viewcustomer&tab-container-1=1&token='.$tokenCustomers."'>".($rec['is_company'] == 1 ? $rec['company'] : $rec['firstname']." ".$rec['lastname'])."</a></td>";
					echo "<td>".$rec['codice_spring']."</td>";
					echo "<td>".($rec['is_company'] == 1 ? $rec['vat_number'] : $rec['tax_code'])."</td>";	
		echo "<td>".$rec['nickname']."</td>";
		echo "<td>".$rec['customer_role']."</td>";
		echo "</tr></tbody></table>";
		echo "<br />";
		echo "<table class='table' style='width:600px'>";
		echo "<tr><td style='width:100px; background-color:#f4e8cc'><strong>Prodotto</strong></td><td><a href='index.php?tab=AdminCatalogExFeatures&id_product=".$rec['id_product'].'&updateproduct&token='.$tokenCatalogExFeatures."'>".$rec['name']." (".$rec['reference'].")</a></td></tr>";
		echo "<tr><td style='width:100px; background-color:#f4e8cc'><strong>Titolo</strong></td><td>".$rec['title']."</td></tr>";
		echo "<tr><td style='width:100px; background-color:#f4e8cc; vertical-align:top'><strong>Testo</strong></td><td>".$rec['content']."</td></tr>";			
		echo "</table>";
		echo "<br /><br />";
		echo '<table class="table" style="width:600px">
		<thead><tr>
		<th>Rispondi al cliente</th></tr></thead>
		<tr><td>
		<form action="ajax.php" method="post" onsubmit="saveRispostaCommento(); return false;" id="rispondi_commento">
			<textarea name="risposta_commento" class="rte" id="risposta_commento" style="width:550px;height:100px">'.$rec['replica'].'</textarea><br /><input type="submit" id="submitRispostaCommento" class="button" value="'.$this->l('Salva risposta al cliente').'"  />
			
			<br /><div id="note_feedback"></div></form>
			<script type="text/javascript">
				function saveRispostaCommento()
				{	
					$("#risposta_feedback").html("<img src=\"../img/loader.gif\" />").show();
					var rispostaContent = $("#risposta_commento").val();
					
					$.post("ajax.php", {submitRispostaCommento:1,id_customer:'.(int)$rec['id_customer'].',id_recensione:'.$rec['id_product_comment'].',rispostaContent:rispostaContent}, function (r) {
						$("#risposta_feedback").html("").hide();
						if (r == "ok")
						{
							$("#risposta_feedback").html("<b style=\"color:green\">'.addslashes($this->l('Risposta salvata')).'</b>").fadeIn(400);
						}
						else if (r == "error:validation")
							$("#risposta_feedback").html("<b style=\"color:red\">'.addslashes($this->l('Errore: risposta non valida')).'</b>").fadeIn(400);
						else if (r == "error:update")
							$("#risposta_feedback").html("<b style=\"color:red\">'.addslashes($this->l('Errore: impossibile salvare risposta')).'</b>").fadeIn(400);
						$("#note_feedback").fadeOut(3000);
					});
				}
			</script><br /><br />
			<div id="risposta_feedback"></div>
			</td></tr></table>';
		echo "</div>";
		
		echo "<div style='float:left; margin-left:20px'>";
		
		$voti = Db::getInstance()->executeS("SELECT pccl.name, pcg.grade FROM product_comment_grade pcg JOIN product_comment_criterion_lang pccl ON pcg.id_product_comment_criterion = pccl.id_product_comment_criterion WHERE pccl.id_lang = ".$cookie->id_lang." AND pcg.id_product_comment = ".Tools::getValue('id_product_comment')."");
		echo "<table class='table' style='width:300px'>";
		foreach($voti as $voto) {
		
			
			echo "<tr><td style='width:230px; background-color:#f4e8cc'><strong>".$voto['name']."</strong></td><td>".$this->displayStars($voto['grade'])."</td></tr>";		
			
		
		
		}
		echo "</table>";
		
		echo "</div>";
		echo "<div style='clear:both'></div>";
		
		echo "<br />".($rec['validate'] == 0 ? "<a href='".$currentIndex."&id_product_comment=".$rec['id_product_comment']."&viewproduct_comment&token=".$this->token."&approve'><img src='../img/admin/enabled.gif' alt='Abilita' /> Approva recensione</a>" : "<a href='".$currentIndex."&id_product_comment=".$rec['id_product_comment']."&viewproduct_comment&token=".$this->token."&disapprove'><img src='../img/admin/forbbiden.gif' alt='Disabilita' /> Disapprova recensione </a>")."
						| <a href='".$currentIndex."&id_product_comment=".$rec['id_product_comment']."&viewproduct_comment&token=".$this->token."&delete'><img src='../img/admin/delete.gif' alt='Cancella' onclick='javascript: var surec=window.confirm(\"Sei sicuro?\"); if (surec) { return true; } else { return false; }' /> Cancella recensione</a> | <a href='".$currentIndex."&token=".$this->token."'>Torna alla lista delle recensioni</a>";
	}
	
		public function displayStars($voto) {
		
			switch($voto) {
				case 1: return '<img src="../img/1_n.gif" alt="1" title="1" />'; break;
				case 2: return '<img src="../img/2_n.gif" alt="2" title="2" />'; break;
				case 3: return '<img src="../img/3_n.gif" alt="3" title="3" />'; break;
				case 4: return '<img src="../img/4_n.gif" alt="4" title="4" />'; break;
				case 5: return '<img src="../img/5_n.gif" alt="5" title="5" />'; break;
				
			}
		}
		
			public function getList($id_lang, $orderBy = NULL, $orderWay = NULL, $start = 0, $limit = NULL)
	{
		global $cookie;

		/* Manage default params values */
		if (empty($limit))
			$limit = ((!isset($cookie->{$this->table.'_pagination'})) ? $this->_pagination[1] : $limit = $cookie->{$this->table.'_pagination'});

		if (!Validate::isTableOrIdentifier($this->table))
			die (Tools::displayError('Table name is invalid:').' "'.$this->table.'"');

		if (empty($orderBy))
			$orderBy = $cookie->__get($this->table.'Orderby') ? $cookie->__get($this->table.'Orderby') : $this->_defaultOrderBy;
		if (empty($orderWay))
			$orderWay = $cookie->__get($this->table.'Orderway') ? $cookie->__get($this->table.'Orderway') : 'ASC';

		$limit = (int)(Tools::getValue('pagination', $limit));
		$cookie->{$this->table.'_pagination'} = $limit;

		/* Check params validity */
		if (!Validate::isOrderBy($orderBy) OR !Validate::isOrderWay($orderWay)
			OR !is_numeric($start) OR !is_numeric($limit)
			OR !Validate::isUnsignedId($id_lang))
			die(Tools::displayError('get list params is not valid'));

		/* Determine offset from current page */
		if ((isset($_POST['submitFilter'.$this->table]) OR
		isset($_POST['submitFilter'.$this->table.'_x']) OR
		isset($_POST['submitFilter'.$this->table.'_y'])) AND
		!empty($_POST['submitFilter'.$this->table]) AND
		is_numeric($_POST['submitFilter'.$this->table]))
			$start = (int)($_POST['submitFilter'.$this->table] - 1) * $limit;

		/* Cache */
		$this->_lang = (int)($id_lang);
		$this->_orderBy = $orderBy;
		$this->_orderWay = Tools::strtoupper($orderWay);

		/* SQL table : orders, but class name is Order */
		$sqlTable = $this->table == 'order' ? 'orders' : $this->table;
		
		/* Query in order to get results with all fields */
		$sql = 'SELECT SQL_CALC_FOUND_ROWS
			'.($this->_tmpTableFilter ? ' * FROM (SELECT ' : '').'
			'.($this->lang ? 'b.*, ' : '').'a.*, c.firstname, c.lastname, c.company, c.codice_spring, c.vat_number, c.tax_code, c.is_company, p.reference, pl.name'.(isset($this->_select) ? ', '.$this->_select.' ' : '').'
			FROM `'._DB_PREFIX_.$sqlTable.'` a
			'.($this->lang ? 'LEFT JOIN `'._DB_PREFIX_.$this->table.'_lang` b ON (b.`'.$this->identifier.'` = a.`'.$this->identifier.'` AND b.`id_lang` = '.(int)($id_lang).')' : '').'
			'.(isset($this->_join) ? $this->_join.' ' : '').'
			WHERE 1 '.(isset($this->_where) ? $this->_where.' ' : '').($this->deleted ? 'AND a.`deleted` = 0 ' : '').(isset($this->_filter) ? $this->_filter : '').'
			'.(isset($this->_group) ? $this->_group.' ' : '').'
			'.((isset($this->_filterHaving) || isset($this->_having)) ? 'HAVING ' : '').(isset($this->_filterHaving) ? ltrim($this->_filterHaving, ' AND ') : '').(isset($this->_having) ? $this->_having.' ' : '').'
			ORDER BY '.(($orderBy == $this->identifier) ? 'a.' : '').'`'.pSQL($orderBy).'` '.pSQL($orderWay).
			($this->_tmpTableFilter ? ') tmpTable WHERE 1'.stripslashes($this->_tmpTableFilter).'' : '').'
			LIMIT '.(int)($start).','.(int)($limit);
		$this->_list = Db::getInstance()->ExecuteS($sql);

		$this->_listTotal = Db::getInstance()->getValue('SELECT FOUND_ROWS() AS `'._DB_PREFIX_.$this->table.'`');
	}
	
	
	
	protected function _displayViewLink($token = NULL, $id)
	{
		global $currentIndex;

		$_cacheLang['View'] = $this->l('View');

		echo '
			<a href="'.$currentIndex.'&'.$this->identifier.'='.$id.'&view'.$this->table.'&token='.($token!=NULL ? $token : $this->token).'">
			<img src="../img/admin/details.gif" alt="'.$_cacheLang['View'].'" title="'.$_cacheLang['View'].'" /></a>';
			
			$validate = Db::getInstance()->getValue("SELECT validate FROM product_comment WHERE id_product_comment = ".$id."");
			
			echo "".($validate == 0 ? "<a href='".$currentIndex."&id_product_comment=".$id."&token=".$this->token."&approve'><img src='../img/admin/forbbiden.gif' alt='Abilita' /></a>" : "<a href='".$currentIndex."&id_product_comment=".$id."&token=".$this->token."&disapprove'><img src='../img/admin/enabled.gif' alt='Disabilita' /></a>")."
						<a href='".$currentIndex."&id_product_comment=".$id."&token=".$this->token."&delete'><img src='../img/admin/delete.gif' alt='Cancella'  onclick='javascript: var surec=window.confirm(\"Sei sicuro?\"); if (surec) { return true; } else { return false; }' /></a>";
	}
		
}

