<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14846 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminOldProducts extends AdminTab
{
	
	public function display()
	{
		global $cookie, $currentIndex;
		
		echo "<h1>Prodotti OLD</h1>";
		echo 'Vecchi prodotti non pi&ugrave; presenti sul sito (non in vendita, non si possono cercare). ';
		
		$prodotti = Db::getInstance()->executeS('SELECT p.id_product, reference, supplier_reference, ean13, asin, pl.name, amazon, stock_quantity, m.name costruttore FROM product p JOIN product_lang pl ON p.id_product = pl.id_product LEFT JOIN manufacturer m ON m.id_manufacturer = p.id_manufacturer WHERE p.active = 2 AND pl.id_lang = 5 GROUP BY p.id_product ORDER BY '.(isset($_GET['orderby']) ? $_GET['orderby'] : 'p.reference').' '.(isset($_GET['orderway']) ? $_GET['orderway'] : 'ASC').' ');
		
		echo "
		<style type='text/css'>
		#tableVerificaProdotti {
		}
		
		#tableVerificaProdotti tbody {
			
		}

		#tableVerificaProdotti td {
			color:#000;
		}
		
		
		</style>
		
		<script type='text/javascript'>
			$(document).ready(function() {
				function moveScroll(){
					var scroll = $(window).scrollTop();
					var anchor_top = $('#tableVerificaProdotti').offset().top;
					var anchor_bottom = $('#bottom_anchor').offset().top;
					if (scroll>anchor_top && scroll<anchor_bottom) {
					clone_table = $('#clone');
					if(clone_table.length == 0){
						clone_table = $('#tableVerificaProdotti').clone();
						clone_table.attr('id', 'clone');
						clone_table.css({position:'fixed',
								 'pointer-events': 'none',
								 top:0});
						clone_table.width($('#tableVerificaProdotti').width());
						$('#table-container').append(clone_table);
						$('#clone').css({visibility:'hidden'});
						$('#clone thead').css({'visibility':'visible','pointer-events':'auto'});
					}
					} else {
					$('#clone').remove();
					}
				}
				$(window).scroll(moveScroll); 
			});
		</script>
		<div id='table-container'>
		
		<table class='table' id='tableVerificaProdotti'>
		<thead>
		<tr>";
		
		echo "

		<th style='width:90px'>Codice<a href='index.php?tab=AdminOldProducts&orderby=p.reference&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
		<a href='index.php?tab=AdminOldProducts&orderby=p.reference&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
		<th style='width:90px'>Codice SKU<a href='index.php?tab=AdminOldProducts&orderby=p.supplier_reference&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
		<a href='index.php?tab=AdminOldProducts&orderby=p.supplier_reference&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
		
		<th style='width:90px'>EAN<a href='index.php?tab=AdminOldProducts&orderby=p.ean13&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
		<a href='index.php?tab=AdminOldProducts&orderby=p.ean13&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
		
		<th style='width:90px'>Costruttore<a href='index.php?tab=AdminOldProducts&orderby=costruttore&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
		<a href='index.php?tab=AdminOldProducts&orderby=costruttore&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
		
		<th style='width:160px'>Prodotto <a href='index.php?tab=AdminOldProducts&orderby=pl.name&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
		<a href='index.php?tab=AdminOldProducts&orderby=pl.name&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
		
		<th style='width:50px'>Mag. EZ<a href='index.php?tab=AdminOldProducts&orderby=stock_quantity&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
		<a href='index.php?tab=AdminOldProducts&orderby=stock_quantity&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
		
		<th>Azioni</th>
		</tr>
		";
		
		echo "</tr></thead><tbody>";
		
		$tokenCatalog = Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee));
		
		foreach($prodotti as $p)
		{
			echo '<tr>
			<td style="font-size:11px"><span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($p['id_product']).'"><a href="index.php?tab=AdminCatalog&id_product='.$p['id_product'].'&updateproduct&token='.$tokenCatalog.'">'.$p['reference'].'</a></span></td>
			
			<td style="font-size:11px">'.$p['supplier_reference'].'</td>
			
			<td style="font-size:11px">'.$p['ean13'].'</td>
			
			<td style="font-size:11px">'.$p['costruttore'].'</td>
			
			<td>'.$p['name'].'</td>
			<td style="text-align:right">'.$p['stock_quantity'].'</td>
			
			<td></td>
			</tr>';
		}
		
		
		
		echo "</tbody></table></div><div id='bottom_anchor'>
			</div>";
			
	}
	

}
