<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

class ImgControl extends AdminPreferences
{


	public function display()
	{
	global $cookie;
	
		echo "<h1>Controllo peso immagini</h1>";
		
		mysql_query("DROP TABLE immagini_provvisoria");
		
		$result_prov = mysql_query("
			CREATE TABLE `immagini_provvisoria` (
				`idimage` INT( 11 ) NOT NULL ,
				`idprod` INT( 11 ) NOT NULL ,
				`cover` INT( 11 ) NOT NULL ,
				`nomeprod` VARCHAR( 300 ) NOT NULL ,
				`codice` VARCHAR( 300 ) NOT NULL ,
				`dimensioniOrig` VARCHAR( 300 ) NOT NULL ,
				`pesoOrig` DECIMAL (10, 2) NOT NULL ,
				`pesoTrec` DECIMAL (10, 2) NOT NULL ,
				`pesoSeic` DECIMAL (10, 2) NOT NULL ,
				`pesoList` DECIMAL (10, 2) NOT NULL ,
				`pesoPic` DECIMAL (10, 2) NOT NULL
			) 
		");
		
		$tokenProducts = Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee));
		
		$result = Db::getInstance()->executeS("SELECT image.id_image AS idimage, image.cover AS cover, image.id_product AS idproduct, product.reference AS codice, product_lang.name AS nomeprod FROM image JOIN product ON image.id_product = product.id_product JOIN product_lang ON image.id_product = product_lang.id_product WHERE product_lang.id_lang = 5");

		echo "<table>";
		echo "<tr>";
		echo "<th>ID immagine<br />
		<a href='index.php?tab=ImgControl&sort=idimgasc&token=".$this->token."'><img src='".$_SERVER['name']."/img/admin/up.gif' alt='Su' title='Su' /></a>   
		<a href='index.php?tab=ImgControl&sort=idimgdesc&token=".$this->token."'><img src='".$_SERVER['name']."/img/admin/down.gif' alt='Giu' title='Giu' /></a>
		</th>";
		echo "<th>Nome prodotto<br />
		<a href='index.php?tab=ImgControl&sort=nomeprodasc&token=".$this->token."'><img src='".$_SERVER['name']."/img/admin/up.gif' alt='Su' title='Su' /></a>   
		<a href='index.php?tab=ImgControl&sort=nomeproddesc&token=".$this->token."'><img src='".$_SERVER['name']."/img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
		echo "<th>Codice prodotto<br />
		<a href='index.php?tab=ImgControl&sort=codiceasc&token=".$this->token."'><img src='".$_SERVER['name']."/img/admin/up.gif' alt='Su' title='Su' /></a>   
		<a href='index.php?tab=ImgControl&sort=codicedesc&token=".$this->token."'><img src='".$_SERVER['name']."/img/admin/down.gif' alt='Giu' title='Giu' /></a>
		</th>";
		echo "<th>Default</th>";
		echo "<th>Anteprima img</th>";
		echo "<th>Dimensioni originale<br />
		<a href='index.php?tab=ImgControl&sort=dimasc&token=".$this->token."'><img src='".$_SERVER['name']."/img/admin/up.gif' alt='Su' title='Su' /></a>   
		<a href='index.php?tab=ImgControl&sort=dimdesc&token=".$this->token."'><img src='".$_SERVER['name']."/img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
		echo "<th>Peso orig.<br />
		<a href='index.php?tab=ImgControl&sort=pesorigasc&token=".$this->token."'><img src='".$_SERVER['name']."/img/admin/up.gif' alt='Su' title='Su' /></a>   
		<a href='index.php?tab=ImgControl&sort=pesorigdesc&token=".$this->token."'><img src='".$_SERVER['name']."/img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
		echo "<th>Peso 300x300<br />
		<a href='index.php?tab=ImgControl&sort=trecasc&token=".$this->token."'><img src='".$_SERVER['name']."/img/admin/up.gif' alt='Su' title='Su' /></a>   
		<a href='index.php?tab=ImgControl&sort=trecdesc&token=".$this->token."'><img src='".$_SERVER['name']."/img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
		echo "<th>Peso 600x600<br />
		<a href='index.php?tab=ImgControl&sort=seicasc&token=".$this->token."'><img src='".$_SERVER['name']."/img/admin/up.gif' alt='Su' title='Su' /></a>   
		<a href='index.php?tab=ImgControl&sort=seicdesc&token=".$this->token."'><img src='".$_SERVER['name']."/img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
		echo "<th>Peso 110x110<br />
		<a href='index.php?tab=ImgControl&sort=listasc&token=".$this->token."'><img src='".$_SERVER['name']."/img/admin/up.gif' alt='Su' title='Su' /></a>   
		<a href='index.php?tab=ImgControl&sort=listdesc&token=".$this->token."'><img src='".$_SERVER['name']."/img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
		echo "<th>Peso 45x45<br />
		<a href='index.php?tab=ImgControl&sort=picasc&token=".$this->token."'><img src='".$_SERVER['name']."/img/admin/up.gif' alt='Su' title='Su' /></a>   
		<a href='index.php?tab=ImgControl&sort=picdesc&token=".$this->token."'><img src='".$_SERVER['name']."/img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
		echo "</tr>";
		
		
		foreach($result as $row)
		{
		list($imwidth, $imheight, $imtype, $imattr) = getimagesize("".$_SERVER['name'].""._THEME_PROD_DIR_.$row['idproduct']."-".$row['idimage'].".jpg"); 
		$fileimg_size_normal = (filesize($_SERVER['DOCUMENT_ROOT']._THEME_PROD_DIR_.$row['idproduct']."-".$row['idimage'].".jpg")/1024); 
		$fileimg_size_large = (filesize($_SERVER['DOCUMENT_ROOT']._THEME_PROD_DIR_.$row['idproduct']."-".$row['idimage']."-large.jpg")/1024); 
		$fileimg_size_box = (filesize($_SERVER['DOCUMENT_ROOT']._THEME_PROD_DIR_.$row['idproduct']."-".$row['idimage']."-thickbox.jpg")/1024); 
		$fileimg_size_small = (filesize($_SERVER['DOCUMENT_ROOT']._THEME_PROD_DIR_.$row['idproduct']."-".$row['idimage']."-small.jpg")/1024); 
		$fileimg_size_medium = (filesize($_SERVER['DOCUMENT_ROOT']._THEME_PROD_DIR_.$row['idproduct']."-".$row['idimage']."-medium.jpg")/1024); 
								
		

		
		mysql_query("INSERT INTO immagini_provvisoria (idimage, idprod, cover, nomeprod, codice, dimensioniOrig, pesoOrig, pesoTrec, pesoSeic, pesoList, pesoPic) VALUES ('".$row['idimage']."', '".$row['idproduct']."', '".$row['cover']."', '".$row['nomeprod']."', '".$row['codice']."', '".$imwidth." x ".$imheight."', '".$fileimg_size_normal."', '".$fileimg_size_large."', '".$fileimg_size_box."', '".$fileimg_size_medium."', '".$fileimg_size_small."')");  
		
		}
		
		if(isset($_GET['sort']) && $_GET['sort'] == 'idimgasc') {
		$orderby = "idimage ASC";
		}
		else if(isset($_GET['sort']) && $_GET['sort'] == 'idimgdesc') {
		$orderby = "idimage DESC";
		}
		else if(isset($_GET['sort']) && $_GET['sort'] == 'nomeprodasc') {
		$orderby = "nomeprod ASC";
		}
		else if(isset($_GET['sort']) && $_GET['sort'] == 'nomeproddesc') {
		$orderby = "nomeprod DESC";
		}
			else if(isset($_GET['sort']) && $_GET['sort'] == 'codiceasc') {
		$orderby = "codice ASC";
		}
			else if(isset($_GET['sort']) && $_GET['sort'] == 'codicedesc') {
		$orderby = "codice DESC";
		}
			else if(isset($_GET['sort']) && $_GET['sort'] == 'dimasc') {
		$orderby = "dimensioniOrig ASC";
		}
			else if(isset($_GET['sort']) && $_GET['sort'] == 'dimdesc') {
		$orderby = "dimensioniOrig DESC";
		}
			else if(isset($_GET['sort']) && $_GET['sort'] == 'pesorigasc') {
		$orderby = "pesoOrig ASC";
		}
			else if(isset($_GET['sort']) && $_GET['sort'] == 'pesorigdesc') {
		$orderby = "pesoOrig DESC";
		}
					else if(isset($_GET['sort']) && $_GET['sort'] == 'trecasc') {
		$orderby = "pesoTrec ASC";
		}
			else if(isset($_GET['sort']) && $_GET['sort'] == 'trecdesc') {
		$orderby = "pesoTrec DESC";
		}
					else if(isset($_GET['sort']) && $_GET['sort'] == 'seicasc') {
		$orderby = "pesoSeic ASC";
		}
			else if(isset($_GET['sort']) && $_GET['sort'] == 'seicdesc') {
		$orderby = "pesoSeic DESC";
		}
					else if(isset($_GET['sort']) && $_GET['sort'] == 'listasc') {
		$orderby = "pesoList ASC";
		}
			else if(isset($_GET['sort']) && $_GET['sort'] == 'listdesc') {
		$orderby = "pesoList DESC";
		}
					else if(isset($_GET['sort']) && $_GET['sort'] == 'picasc') {
		$orderby = "pesoPic ASC";
		}
			else if(isset($_GET['sort']) && $_GET['sort'] == 'picdesc') {
		$orderby = "pesoPic DESC";
		}
		
		
		else { $orderby = "idprod DESC"; }
		
		
		$result2 = Db::getInstance()->executeS("SELECT * FROM immagini_provvisoria ORDER BY $orderby");
		
		foreach($result2 as $row2)
		{
		
						echo "<tr>";
		echo "<td>".$row2['idimage']."</td>";
		echo '<td><a href="index.php?tab=AdminCatalog&updateproduct&id_product='.$row2["idprod"].'&token='.$tokenProducts.'" target="_blank">'.$row2["nomeprod"].'</a></td>';
		echo "<td>".$row2['codice']."</td>";
		echo "<td>"; if ($row2['cover'] == 1) { echo "<strong>S&igrave;</strong>"; } else { echo "No"; } 
		echo "<td><a href='index.php?tab=AdminCatalog&updateproduct&id_product=".$row2['idprod']."&token=".$tokenProducts."' target='_blank'><img src='".$_SERVER['name'].""._THEME_PROD_DIR_.$row2['idprod']."-".$row2['idimage']."-small.jpg' /></a></td>";
		echo "<td>".$row2['dimensioniOrig']." pixel</td>";
		echo '<td>'.$row2['pesoOrig'].' kb</td>
		<td><strong>'.$row2['pesoTrec'].' kb</strong></td>
		<td>'.$row2['pesoSeic'].' kb</td>
		<td>'.$row2['pesoList'].' kb</td>
		<td>'.$row2['pesoPic'].' kb</td>';
		echo "</tr>";
		
		}
		
		
		
		
		
		echo "</table>";
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

	}
	

}


