<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

class EditOrders extends AdminPreferences
{


	public function display()
	{
	
	if (isset($_GET['id_order'])) $id_order = $_GET['id_order'];
else if (isset($_POST['id_order'])) $id_order = $_POST['id_order'];
else $id_order = "";
if (isset($_GET['id_lang'])) $id_lang = $_GET['id_lang'];
else if (isset($_POST['id_lang'])) $id_lang = $_POST['id_lang'];
else $id_lang = "";
$id_lang = strval(intval($id_lang));
if (!isset($_GET['attribute'])) $_GET['attribute'] = "";
else
$_GET['attribute'] = strval(intval($_GET['attribute']));

$query = "SET NAMES 'utf8'";
$result = mysql_query($query);

$query=" select cu.name, cu.id_currency,cu.conversion_rate from ". _DB_PREFIX_."configuration cf, ". _DB_PREFIX_."currency cu";
$query.=" WHERE cf.name='PS_CURRENCY_DEFAULT' AND cf.value=cu.id_currency";
$res=mysql_query($query);
$row=mysql_fetch_array($res);
$cur_name = $row['name'];
$cur_rate = $row['conversion_rate'];
$id_currency = $row['id_currency'];
	
	echo '
<h1>Modifica ordini</h1>
<form name="order" method="post">
	<label for="order_number">Order number:</label><input name="id_order" type="text" value="'.$id_order.'" size="10" maxlength="10" />

	<input name="send" type="submit" value="Find order" />
</form>
</td><td>';

if ($id_order != "") {
$query="select a.id_country, a.id_state, s.name AS sname, c.name AS cname, cu.id_currency, cu.name AS currname, cu.conversion_rate AS currrate from ". _DB_PREFIX_."orders o left join ". _DB_PREFIX_."address a on o.id_address_delivery=a.id_address";
$query .= " left join ". _DB_PREFIX_."country_lang c on a.id_country=c.id_country AND c.id_lang='".$id_lang."'";
$query .= " left join ". _DB_PREFIX_."state s on a.id_country=s.id_country  AND a.id_state=s.id_state";
$query .= " left join ". _DB_PREFIX_."currency cu on cu.id_currency=o.id_currency";
$query.=" WHERE o.id_order ='".mysql_real_escape_string($id_order)."'";
$res=mysql_query($query);
$row=mysql_fetch_array($res);
$id_country = $row['id_country'];
$id_state = $row['id_state'];
echo "Tax country=".$row['cname'];
if ($id_state != 0)
  echo " AND state=".$row['sname'];
$order_currency = $row['id_currency'];
$order_currname = $row['currname'];
$conversion_rate = $row['currrate'] / $cur_rate;

if ((isset($_GET['action'])) &&($_GET['action']=='add_product')) {
$query=" select p.*,pl.name,pl.id_lang,l.iso_code,t.rate as tax_rate,tl.name as tax_name from ". _DB_PREFIX_."product p left join ". _DB_PREFIX_."product_lang pl on p.id_product=pl.id_product AND pl.id_lang='".$id_lang."'";
$query.=" left join ". _DB_PREFIX_."lang l on pl.id_lang=l.id_lang";
$query.=" left join ". _DB_PREFIX_."tax_rule tr on tr.id_tax_rules_group=p.id_tax_rules_group AND tr.id_country='".$id_country."'  AND tr.id_state='".$id_state."'";
$query.=" left join ". _DB_PREFIX_."tax t on t.id_tax=tr.id_tax";
$query.=" left join ". _DB_PREFIX_."tax_lang tl on t.id_tax=tl.id_tax AND tl.id_lang='".$id_lang."'";
$query.=" WHERE p.id_product='".$_GET['id_product']."' ";

$res=mysql_query($query);
//echo $query."<p>".mysql_num_rows($res)." RESULTS";
if(mysql_num_rows($res) == 0) 		/* Debugging stuff */
{ echo "<h1>Product not found</h1>";
  echo "QUERY = ".$query."<br/>";
  $query2="select * from product WHERE id_product=".$_GET['id_product'];
  $res2=mysql_query($query2);
  if(mysql_num_rows($res2) == 0)
  { echo " product ".$_GET['id_product']." is not in database<br>".$query2;
    return;
  }  
  $prod=mysql_fetch_array($res2);
  echo "product in category ".$prod['id_category_default']."<br>";
  $query3="select * from product p, product_lang pl WHERE p.id_product=".$_GET['id_product']." AND p.id_product=pl.id_product AND pl.id_lang='".$id_lang."'";
  $res3=mysql_query($query3);
  if(mysql_num_rows($res3) == 0)
  { echo " product ".$_GET['id_product']." is not in language database<br>".$query3;
    return;
  } 
}
$products=mysql_fetch_array($res);
echo "; group=".$products['id_tax_rules_group']."; perc=".$products['tax_rate']."<br>";
$name = $products['name'];
$price = $products['price'];
$weight = $products['weight'];
$quantity = $products['quantity'];
$attribute = '0';

if (is_null($products['tax_rate'])) $products['tax_rate']=0;

if($_GET['attribute']!='')
{ $price = $price+$_GET['attprice'];
  $weight = $weight+$_GET['attweight'];
  $attribute = $_GET['attribute'];
  $gquery = "SELECT public_name,l.name,pa.quantity FROM ". _DB_PREFIX_."product_attribute_combination c LEFT JOIN "._DB_PREFIX_."attribute a on c.id_attribute=a.id_attribute";
  $gquery .= " LEFT JOIN ". _DB_PREFIX_."attribute_group_lang g on a.id_attribute_group=g.id_attribute_group AND g.id_lang='".$id_lang."'";
  $gquery .= " LEFT JOIN ". _DB_PREFIX_."attribute_lang l on a.id_attribute=l.id_attribute AND l.id_lang='".$id_lang."'";
  $gquery .= " LEFT JOIN ". _DB_PREFIX_."product_attribute pa on pa.id_product_attribute=c.id_product_attribute";
  $gquery .= " WHERE c.id_product_attribute='".$_GET['attribute']."'";
  $gres = mysql_query($gquery);
  $grow=mysql_fetch_array($gres);
  $atquantity = $grow["quantity"];
  $name .= " - ".$grow['public_name'].": ".$grow['name'];
  while ($grow=mysql_fetch_array($gres))  /* products with multiple attributes */
      $name .= ", ".$grow['public_name'].": ".$grow['name'];
}

if($quantity > 0)
{ mysql_query("update ". _DB_PREFIX_."product set quantity=".($quantity-1)." where id_product=".$products['id_product']);
  if(($_GET['attribute']!='') && ($atquantity >0))
    mysql_query("update ". _DB_PREFIX_."product_attribute set quantity=".($atquantity-1)." where id_product_attribute=".$_GET['attribute']);
}

$query="insert into ". _DB_PREFIX_."order_detail (id_order ,product_id ,product_attribute_id, product_name ,product_quantity ,product_quantity_in_stock ,product_price ,product_ean13 ,product_reference ,product_supplier_reference ,product_weight ,tax_name ,tax_rate ) values  ";
$query.="(".mysql_real_escape_string($id_order).",".$products['id_product'].",'".addslashes($attribute)."','".addslashes($name)."',1,1,";
$query.=($price*$conversion_rate).",'".$products['ean13']."','".addslashes($products['reference'])."','".addslashes($products['supplier_reference'])."',".$weight.",'".addslashes($products['tax_name'])."',".$products['tax_rate'].")";
// echo $query;
mysql_query($query);
update_total($id_order);

}

$carrierseg = "";
if (isset($_POST['id_carrier'])) 
   $carrierseg = " ,id_carrier=".$_POST['id_carrier'];

if (isset($_POST['order_total'])) {
$total=price($_POST['total_products']*(1+$_POST['tax_rate']/100) )+price($_POST['total_shipping'])+price($_POST['total_wrapping'])-price($_POST['total_discounts']);

$query="update  ". _DB_PREFIX_."orders set ";
$query.=" total_discounts=".price($_POST['total_discounts']);
$query.=" ,total_wrapping=".price($_POST['total_wrapping']);
$query.=" ,total_shipping=".price($_POST['total_shipping']);
$query.=" ,delivery_number=".price($_POST['delivery_number']);
$query.=$carrierseg;
$query.=" ,total_paid_real=".$total;
$query.=" ,total_paid=".$total;
$query.=" where id_order=".mysql_real_escape_string($id_order);
$query.=" limit 1";
mysql_query($query);
echo "<br/><b> Order Modified </b><br/>";

}

if (isset($_POST['Apply'])) {

//delete product
if (isset($_POST['product_delete'])) {
  foreach ($_POST['product_delete'] as $id_order_detail=>$value) {
    mysql_query("delete from ". _DB_PREFIX_."order_detail where id_order_detail=".$id_order_detail);
  }
}

$total_products = 0;
if ($_POST['product_price']) {
foreach ($_POST['product_price'] as $id_order_detail=>$price_product) {
  $qty_difference=$_POST['product_quantity_old'][$id_order_detail]-$_POST['product_quantity'][$id_order_detail];
  $stock=max(0,$_POST['product_stock'][$id_order_detail]+$qty_difference);
  $name=$_POST['product_name'][$id_order_detail];
  $attribute = $_POST['product_attribute'][$id_order_detail];

  $query = "update  ". _DB_PREFIX_."order_detail set product_price='".$price_product."'";
  $query .= ", product_quantity='".$_POST['product_quantity'][$id_order_detail]."', product_quantity_in_stock='".$_POST['product_quantity'][$id_order_detail]."'";
  $query .= ", product_name='".mysql_real_escape_string($name)."' where id_order_detail=".$id_order_detail;
  mysql_query($query);

  //servirebbe ad aggiornare lo stock, ma si dovrebbe vincolare ad uno stato. Attualmete lo disabilito
  mysql_query("update  ". _DB_PREFIX_."product set quantity=".$stock." where id_product=".$_POST['product_id'][$id_order_detail]);
  if(($attribute != 0) && ($qty_difference != 0))
  { $res = mysql_query("SELECT quantity from ". _DB_PREFIX_."product_attribute WHERE id_product_attribute='".mysql_real_escape_string($attribute)."'");
    $row=mysql_fetch_array($res);
    $atquantity = $row["quantity"];
    $atstock=max(0,$row["quantity"]+$qty_difference);
    mysql_query("update  ". _DB_PREFIX_."product_attribute set quantity=".$atstock." where id_product_attribute='".mysql_real_escape_string($attribute)."'");
  }

  $total_products+=$_POST['product_quantity'][$id_order_detail]*price($price_product);
}
update_total($id_order);

}




}
if ($id_order) {
$query="select distinct o.*,a.*,p.tax_rate from ". _DB_PREFIX_."orders o LEFT JOIN "._DB_PREFIX_."address a ON a.id_address=o.id_address_delivery";
$query .= " LEFT JOIN ". _DB_PREFIX_."order_detail p ON o.id_order=p.id_order where o.id_order=".mysql_real_escape_string($id_order);

$res=mysql_query($query);
if (mysql_num_rows($res)>0) {
$order=mysql_fetch_array($res);
$id_customer=$order['id_customer'];
$id_lang=$order['id_lang'];
$id_cart=$order['id_cart'];
$payment=$order['payment'];
$module=$order['module'];
$tax_rate=$order['tax_rate'];
$invoice_number=$order['invoice_number'];
$delivery_number=$order['delivery_number'];
$total_paid_real=$order['total_paid_real'];
$total_products=$order['total_products'];
$total_discounts=$order['total_discounts'];
$total_shipping=$order['total_shipping'];
$total_wrapping=$order['total_wrapping'];
$firstname=$order['firstname'];
$lastname=$order['lastname'];
$company=$order['company'];
$carrier = $order['id_carrier'];
}

} 


	echo '<label for="costumer_id">Costumer ID:</label><span>'.$id_customer.'</span>
	<label for="costumer_name">Costumer Name:</label><span>'.$firstname." ".$lastname. " ".$company.'</span>
</td>
</tr></table>

<form name="order_total" method="post" style="padding-top: 20px;width: 620px;">
<!-- hidden value --> <input type=hidden name=id_lang value="'.$id_lang.'">

	<label for="carrier">Carrier:</label>
	<select name="id_carrier">';
		$query=" select * from ". _DB_PREFIX_."carrier WHERE deleted='0'";
			$res=mysql_query($query);
			while ($carrierrow=mysql_fetch_array($res)) {
			  $selected='';
			  if ($carrierrow['id_carrier']==$carrier) $selected=' selected="selected" ';
			  echo '<option  value="'.$carrierrow['id_carrier'].'" '.$selected.'>'.$carrierrow['name'].'</option>';
			}
		echo '
	</select>
	
	<label for="total_shipping">Shipping:</label><input name="total_shipping" type="text" value="'. $total_shipping .'" />
	<label for="total_discounts">Discounts:</label><input name="total_discounts" type="text"  value="'. $total_discounts .'" />
	<label for="total_wrapping">Wrapping:</label><input name="total_wrapping" type="text" value="'. $total_wrapping .'" />
	<label for="delivery_number">Delivery no.:</label><input name="delivery_number" type="text"  value="'. $delivery_number .'" />
	<label for="subtotal">Subtotal (tax excl.):</label><span>'. $total_products .'</span>
	<label for="total">Total (tax incl.):</label><span>'. $total_paid_real." &nbsp; ".$order_currname .'</span>

	<!-- hidden value -->  <input name="total_products" type="hidden"  value="'. $total_products .'" />
	<!-- hidden value -->  <input name="tax_rate" type="hidden"  value="'. $tax_rate .'" />
	<!-- hidden value -->  <input name="total" type="hidden"  id="total_paid_real" value="'. $total_paid_real .'" />
	<!-- hidden value -->  <input name="id_order" type="hidden" value="'. $id_order .'" />
	
	<input type="submit" name="order_total"  value="Modify Order" />
</form>

<br style="clear:both; height:40px;display:block;" />';


if ((isset($_GET['action'])) &&($_GET['action']=='add_product') && ($conversion_rate != 1))
 echo "Currency converted: Product price: ".$price." ".$cur_name." was converted into ".($price*$conversion_rate)." ".$order_currname;

echo'

<form name="products" method="post" onSubmit="return checkPrices();">
<table width="100%" ><tr><td width="100%" align=right>
<a style="height:20px; background:#000; color:#FFF; border-radius:3px; padding:5px 10px; text-decoration:none; margin:20px 0"href="add_product.php?id_order='.$id_order.'&id_lang'.$id_lang.'" target="_self">Add new product</a>
</td></tr>
<tr><td>
<table width="100%" border="1" bgcolor="#FFCCCC" style="margin-top:10px;">
  <tr>
    <td>product id</td>
    <td>attrib</td>
    <td>Product Reference</td>
    <td>Product Name</td>
    <td>Price tax ex</td>
    <td>Tax</td>
    <td>Price with tax</td>
    <td>Qty</td>
    <td>Total no tax</td>
    <td>Total  tax inc.</td>
    <td>Weight</td>
    <td>Delete</td>
  </tr>
';

$query="select o.*,p.quantity as stock, o.product_attribute_id from ". _DB_PREFIX_."order_detail o left join ". _DB_PREFIX_."product p  on  o.product_id=p.id_product";
$query.=" where id_order=".mysql_real_escape_string($id_order);
$query.=" order by id_order_detail asc";
  $res1=mysql_query($query);
if (mysql_num_rows($res1)>0) {

while ($products=mysql_fetch_array($res1)) {
$tax_rate=$products['tax_rate'];
  echo '<tr>';
  echo '  <td>'.$products['product_id'].'</td>';
  echo '  <td>'.$products['product_attribute_id'].'</td>';
  echo '  <td>'.$products['product_reference'].'</td>';
  echo '  <td><input name="product_name['.$products['id_order_detail'].']" type="text" value="'.$products['product_name'].'" /></td>';
  echo '  <td><input name="product_price['.$products['id_order_detail'].']" class="price" type="text" value="'.number_format($products['product_price'], 4, '.', '').'" /></td>';
  echo '  <td>'.$products['tax_rate'].'%</td>';
  echo '  <td>'.number_format($products['product_price']*(1+$products['tax_rate']/100),3, '.', '').'</td>';  
  echo '  <td><input name="product_quantity['.$products['id_order_detail'].']" type="text" value="'.$products['product_quantity'].'" /></td>';
  echo '  <td>'.number_format($products['product_price']*$products['product_quantity'],2, '.', '').'</td>';  
  echo '  <td>'.number_format($products['product_price']*$products['product_quantity']*(1+$products['tax_rate']/100),2, '.', '').'</td>';  
  echo '  <td>'.number_format($products['product_weight'],2, '.', '').'</td>';
  echo '  <td><input name="product_delete['.$products['id_order_detail'].']" type="checkbox" />';
  echo '  <input name="product_quantity_old['.$products['id_order_detail'].']" type="hidden" value="'.$products['product_quantity'].'" />';
  echo '  <input name="product_id['.$products['id_order_detail'].']" type="hidden" value="'.$products['product_id'].'" />';
  echo '  <input name="product_attribute['.$products['id_order_detail'].']" type="hidden" value="'.$products['product_attribute_id'].'" />';
  echo '  <input name="product_stock['.$products['id_order_detail'].']" type="hidden" value="'.$products['stock'].'" />';
  echo '</td></tr> ';
   
  }
  }
  echo '
</table>
</td></tr>
<tr><td width="100%" align=center>
  <input name="Apply" type="submit" value="Modify order" />
  <input name="id_order" type="hidden" value="'.$id_order .'" />
  <input name="tax_rate" type="hidden" value="'.$tax_rate .'" />
  <input name="id_lang" type="hidden" value="'.$id_lang.'" />
</td><tr>
<tr><td>
Limitations:<br/>
 - this program will not recalculate your shipping costs if you change carrier or add or remove products. You should do that manually.<br/>
 - ecotax is ignored.<br/>
 - group discounts are not processed<br/>
 - stock management is partly supported: deleted lines are not processed and having multiple lines with the same product may give problems<br/>
</td></tr>
</table>
</form>
<hr style="border: 1px dotted #CCCCCC;" />
<table width="100%"><tr><td width="25%"><a href=cat_edit.php>Category Edit</a></td>
<td width="25%"><a href=product_edit.php>Product Edit</a></td>
<td width="25%">PS version: '. _PS_VERSION_ .'</td>
<td width="25%"><a href=logout1.php>logout</a></td>
</tr></table>';


}


function price($price) {
$price=str_replace(",",".",$price);
return $price;
}

function update_total($id_order) {
$query="select sum(product_price*product_quantity*(1+tax_rate/100)) as total_products,sum(product_price*product_quantity) as total_products_notax  from  ". _DB_PREFIX_."order_detail where id_order=".$id_order;
$res2=mysql_query($query);
$products=mysql_fetch_array($res2);
if($products['total_products']=="")
  $products['total_products'] = $products['total_products_notax'] = 0; /* no products present */
$query="select * from  ". _DB_PREFIX_."orders where id_order=".mysql_real_escape_string($id_order);
$res3=mysql_query($query);
$order=mysql_fetch_array($res3);
$total=price($products['total_products'])+price($order['total_shipping'])+price($order['total_wrapping'])-price($order['total_discounts']);
$query="update  ". _DB_PREFIX_."orders set ";
$query.=" total_discounts=".$order['total_discounts'];
$query.=" ,total_wrapping=".$order['total_wrapping'];
$query.=" ,total_shipping=".$order['total_shipping'];
$query.=" ,total_products=".$products['total_products_notax'];
$query.=" ,total_products_wt=".$products['total_products'];
$query.=" ,total_paid_real=".$total;
$query.=" ,total_paid=".$total;
$query.=" where id_order=".mysql_real_escape_string($id_order);
$query.=" limit 1";

mysql_query($query);
}
		
		
		
		
		
		
		}
	

}


