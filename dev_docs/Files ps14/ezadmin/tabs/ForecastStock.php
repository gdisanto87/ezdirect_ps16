<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14632 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

class ForecastStock extends AdminPreferences {
	
	public function display()
	{
		global $cookie;
		//ini_set('display_errors', 'on');
		echo '<h1>Forecast Stock</h1>';
		
		$forecast_registrato = Db::getInstance()->getValue('SELECT periodo FROM forecast WHERE id_product = 11111');
		
		
		
		$tokenCatalog = Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee));
		
		if(Tools::getIsset('vai-forecast') || Tools::getIsset('esportaexcel'))
		{
			echo 'Periodo forecast: <input type="text" size="3" id="periodo_tutti" onkeyup="calcolaPeriodoTutti()" name="periodo_tutti" style="text-align:right" value="'.round($forecast_registrato,2).'" />';
			
			if(Tools::getIsset('esportaexcel'))
			{	
				ini_set("memory_limit","892M");
				set_time_limit(3600);
				require_once 'esportazione-catalogo/Classes/PHPExcel.php';
				$objPHPExcel = new PHPExcel();
			}	
				
			foreach ($_POST['per_marchio'] as $marchio)
			{
				if($marchio == 'tutto')
					$marchi.= "p.id_manufacturer > 0 OR ";
				else
					$marchi.= "p.id_manufacturer = $marchio OR ";
			}

			foreach ($_POST['per_fornitore'] as $fornitore)
			{
				if($fornitore == 'tutti_fornitori')
					$fornitori.= "p.id_supplier > 0 OR ";
				else
					$fornitori.= "p.id_supplier = $fornitore OR ";
			}
			
			if(empty($_POST['per_marchio']))
				$marchi.= "p.id_manufacturer > 0 OR ";
			
			if(empty($_POST['per_fornitore']))
				$fornitori.= "p.id_supplier > 0 OR ";
			
			if(Tools::getIsset('vai-forecast'))
			{
				//$query = 'SELECT p.*, pl.name as nome_prodotto, m.name as costruttore, s.name as fornitore FROM product p JOIN order_detail od ON p.id_product = od.product_id JOIN product_lang pl ON p.id_product = pl.id_product JOIN orders o on od.id_order = o.id_order JOIN manufacturer m on p.id_manufacturer = m.id_manufacturer JOIN supplier s ON s.id_supplier = p.id_supplier WHERE pl.id_lang = 5 AND o.date_add > DATE_SUB(curdate(), INTERVAL 1 YEAR) AND ('.$marchi.' p.id_manufacturer = 9999999999) AND ('.$fornitori.' p.id_supplier = 999999999) AND p.quantity < 99999 GROUP BY p.id_product';
				
				$query = 'SELECT p.*, pl.name as nome_prodotto, m.name as costruttore, s.name as fornitore FROM product p JOIN product_lang pl ON p.id_product = pl.id_product JOIN manufacturer m on p.id_manufacturer = m.id_manufacturer JOIN supplier s ON s.id_supplier = p.id_supplier WHERE pl.id_lang = 5 AND ('.$marchi.' p.id_manufacturer = 9999999999) AND ('.$fornitori.' p.id_supplier = 999999999) AND p.quantity < 99999 GROUP BY p.id_product';
			}
			else
				$query = Tools::getValue('query');
			
			$products = Db::getInstance()->executeS($query);
			
			//echo 'Nelle caselle col venduto il numero indica il venduto nel periodo, tra parentesi invece &egrave; indicata la media del periodo';
			
			echo '
		
			<script type="text/javascript" src="../js/jquery/jquery.tablesorter.min.js"></script> 
			<script type="text/javascript" src="../js/jquery/jquery.tablesorter.widgets.js"></script><script type="text/javascript" src="../js/jquery/jquery.tablesorter.cssStickyHeaders.js"></script> ';
			
			echo '
			<script type="text/javascript">
				var myTextExtraction = function(node) 
				{ 
					// extract data from markup and return it 
					return node.textContent.replace( /<.*?>/g, "" ); 
				}
				
				$(document).ready(function() 
				{ 
				
			
				$.tablesorter.addParser({ 
					// set a unique id 
					id: "thousands",
					is: function(s) { 
						// return false so this parser is not auto detected 
						return false; 
					}, 
					format: function(s) {
						// format your data for normalization 
						return s.replace(/[\s.$£€]/g,"").replace(/,/g,".");
					}, 
					// set type, either numeric or text 
					type: "numeric" 
				}); 


					$("#prodotti_table").tablesorter({ widthFixed: true, headers: {
							11 : { sorter: "thousands" },
							15 : { sorter: "thousands" },
				}, 
				widgets: ["zebra", "stickyHeaders"]});
					
				} 
			);
			
			function calcolaPeriodoTutti()
			{
				var periodo_tutti = parseFloat(document.getElementById("periodo_tutti").value);
				
				var arrayIds = document.getElementsByClassName("id_productClass");
				for (var i = 0; i < arrayIds.length; ++i) {
					var id = parseFloat(arrayIds[i].value);  
					document.getElementById("periodo_"+id).value = periodo_tutti;
					calcolaPeriodo(id);
				}
						
			}
			
			
			function calcolaPeriodo(id)
			{
				var ordine = parseFloat(document.getElementById("ordine_per_periodo_"+id).value);
				var periodo = parseFloat(document.getElementById("periodo_"+id).value);
				var netto = parseFloat(document.getElementById("netto_"+id).value);
				var ordinato = parseFloat(document.getElementById("ordinato_"+id).value);
				if(ordine > 3)
					ordine = Math.round(ordine);
				
				$.ajax({
				  url:"ajax.php?aggiornaPeriodoForecast=y",
				  type: "POST",
				  data: { id_product: id,
				  periodo: periodo
				  },
				  success:function(r){
					// alert(r);
				  },
				  error: function(xhr,stato,errori){
					 alert("Errore durante l\'operazione:"+xhr.status);
				  }
				});
					
				
				var calcolo = ordine * periodo;
				var ord_tot = (netto + ordinato) - calcolo;
				
				if(calcolo > 3)
					calcolo = calcolo.toFixed(0);
				else
				{
					calcolo = calcolo.toFixed(2);
					calcolo = calcolo.replace(".",",");
				}
				
				if(ord_tot > 0)
					ord_tot = 0;
				
				
				var ord_tot = 0-ord_tot;
				
				
				if(ord_tot > 3)
					ord_tot = ord_tot.toFixed(0);
				else
				{
					ord_tot = ord_tot.toFixed(2);
					ord_tot = ord_tot.replace(".",",");
				}
				
				document.getElementById("calcolo_"+id).innerHTML = calcolo;
				document.getElementById("ord_tot_"+id).innerHTML = ord_tot;
			}
			
			</script>';
			
			echo "<table class='table tablesorter' id='prodotti_table'><thead><tr><th>Codice</th><th>Descrizione</th><th>Costruttore</th><th>Fornitore</th><th>Mese</Th><th class='corner-cut corner-red' title='2 Mesi'><span class='span-reference' title='2 Mesi'>2 m.</span></th><th class='corner-cut corner-red' title='3 Mesi'>3 m.</th><th class='corner-cut corner-red' title='6 Mesi'>6 m.</th><th class='corner-cut corner-red' title='12 Mesi'>12 m.</th><th>Ordine 1 mese</th><th style='display:none'>Periodo</th><th>Forecast periodo</th><th class='corner-cut corner-red' title='Disponibilita netta = Magazzino EZ - Impegnato'>Netto disp.</th><th class='corner-cut corner-red' title='Ordinato a fornitore'>Ord.</th><th class='corner-cut corner-red' title='Ordinato al fornitore + magazzino EZ netto - Impegnato'>Ord. + netto</th><th>Ord. per periodo</th></tr></thead>";
			$i = 2;
			foreach($products as $p)
			{
				$venduto30 = Db::getInstance()->getValue('SELECT sum(product_quantity) from orders o join order_detail od on o.id_order = od.id_order where od.product_reference = "'.$p['reference'].'" and o.date_add BETWEEN NOW() - INTERVAL 30 DAY AND NOW()'); 
				$venduto60 = Db::getInstance()->getValue('SELECT sum(product_quantity) from orders o join order_detail od on o.id_order = od.id_order where od.product_reference = "'.$p['reference'].'" and o.date_add BETWEEN NOW() - INTERVAL 60 DAY AND NOW()'); 
				$venduto90 = Db::getInstance()->getValue('SELECT sum(product_quantity) from orders o join order_detail od on o.id_order = od.id_order where od.product_reference = "'.$p['reference'].'" and o.date_add BETWEEN NOW() - INTERVAL 90 DAY AND NOW()'); 
				$venduto180 = Db::getInstance()->getValue('SELECT sum(product_quantity) from orders o join order_detail od on o.id_order = od.id_order where od.product_reference = "'.$p['reference'].'" and o.date_add BETWEEN NOW() - INTERVAL 180 DAY AND NOW()'); 
				$venduto365 = Db::getInstance()->getValue('SELECT sum(product_quantity) from orders o join order_detail od on o.id_order = od.id_order where od.product_reference = "'.$p['reference'].'" and o.date_add BETWEEN NOW() - INTERVAL 365 DAY AND NOW()'); 
				
				$qta_ord_clienti = Db::getInstance()->getValue('SELECT sum(product_quantity - (REPLACE(cons,"0_",""))) FROM order_detail od JOIN orders o ON o.id_order = od.id_order
				JOIN cart c ON c.id_cart = o.id_cart
				JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE moh.id_order_state != 18 AND moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE c.name NOT LIKE "%prime%" AND  od.product_id = '.$p['id_product'].' AND os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state != 20 AND os.id_order_state != 26 AND os.id_order_state != 29 AND os.id_order_state != 31  AND os.id_order_state != 32 AND os.id_order_state != 35 GROUP BY od.product_id');
				
				$netto = $p['stock_quantity'] - $qta_ord_clienti;
				
				$media30 = $venduto30/1;
				$media60 = $venduto60/2;
				$media90 = $venduto90/3;
				$media180 = $venduto180/6;
				$media365 = $venduto365/6;
				$media_tot = ($media30 + $media60 + $media90 + $media180)/4;
				
				$media30 = (round($media30,2));
				$media60 = (round($media60,2));
				$media90 = (round($media90,2));
				$media180 = (round($media180,2));
				$media365 = (round($media365,2));
				
				$previsione_raw = $media_tot - ($media_tot * 0.1);
				
				/*/if($previsione_raw > 3)
				{	
					$previsione = round($previsione_raw,0);
					$previsione_raw = round($previsione_raw,0);
				}	
				else*/
					//$previsione = number_format(round($previsione_raw, 2),2,",","");
					
				$previsione = round($previsione_raw, 0, PHP_ROUND_HALF_UP);
				
				$forecast_registrato = round($forecast_registrato,0);
				if($forecast_registrato > 0)
				{
					$calcolo = $previsione_raw * $forecast_registrato;
					$ord_tot = $netto + $p['ordinato_quantity'] - $calcolo;
					
					if($ord_tot > 0)
						$ord_tot = 0;
					
					$ord_tot = 0-$ord_tot;
				
			
				}
				else
				{
					$calcolo = '';
					$ord_tot = '';
				}
				
				if($calcolo > 3)
					$calcolo_round = round($calcolo,0);
				else
					$calcolo_round = number_format($calcolo,2,",",".");

				$ord_round = round($ord_tot, 0, PHP_ROUND_HALF_UP);
				
				echo '<tr>';
						
				echo "<td>".'<span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($p['id_product']).'"><a href="index.php?tab=AdminCatalog&id_product='.$p['id_product'].'&updateproduct&token='.$tokenCatalog.'">'.$p['reference']."</a></span><input type='hidden' name='id_product[".$p['id_product']."]' class='id_productClass' value='".$p['id_product']."' /></td>";
				
				echo "<td>".$p['nome_prodotto']."</td><td>".$p['costruttore']."</td><td>".$p['fornitore']."</td>";
				
				echo '<td style="text-align:right">'.($venduto30 > 0 ? $venduto30 : 0).' </td>
				<td style="text-align:right">'.($venduto60 > 0 ? $venduto60 : 0).' </td>
				<td style="text-align:right">'.($venduto90 > 0 ? $venduto90 : 0).' </td>
				<td style="text-align:right">'.($venduto180 > 0 ? $venduto180 : 0).' </td>
				<td style="text-align:right">'.($venduto365 > 0 ? $venduto365 : 0).' </td>
				<td style="text-align:right"><input type="hidden" value="'.$previsione_raw.'" id="ordine_per_periodo_'.$p['id_product'].'" /><strong>'.$previsione.'</strong></td>
				<td style="text-align:right; display:none"><input type="text" size="3" id="periodo_'.$p['id_product'].'" onkeyup="calcolaPeriodo('.$p['id_product'].')" name="periodo['.$p['id_product'].']" style="text-align:right" value="'.$forecast_registrato.'" /></td>
				<td style="text-align:right" id="calcolo_'.$p['id_product'].'">'.($calcolo_round).'</td>
				<td style="text-align:right"><input type="hidden" value="'.$netto.'" id="netto_'.$p['id_product'].'" /> '.$netto.'</td>
				<td style="text-align:right"><input type="hidden" value="'.$p['ordinato_quantity'].'" id="ordinato_'.$p['id_product'].'" /> '.$p['ordinato_quantity'].'</td>
				<td style="text-align:right">'.($netto + $p['ordinato_quantity']).'</td>
				<td style="text-align:right" id="ord_tot_'.$p['id_product'].'">'.$ord_round.'</td>
				';
				
				echo '</tr>';
				
				if(Tools::getIsset('esportaexcel'))
				{
					$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue("A$i", $p['reference'])
					->setCellValue("B$i", $p['nome_prodotto'])
					->setCellValue("C$i", $p['costruttore'])
					->setCellValue("D$i", $p['fornitore'])
					->setCellValue("E$i", ($venduto30 > 0 ? $venduto30 : 0))
					->setCellValue("F$i", ($venduto60 > 0 ? $venduto60 : 0))
					->setCellValue("G$i", ($venduto90 > 0 ? $venduto90 : 0))
					->setCellValue("H$i", ($venduto180 > 0 ? $venduto180 : 0))
					->setCellValue("I$i", ($venduto365 > 0 ? $venduto365 : 0))
					->setCellValue("J$i", $previsione)
					->setCellValue("K$i", $forecast_registrato)
					->setCellValue("L$i", $calcolo)
					->setCellValue("M$i", $netto)
					->setCellValue("N$i", $ord_tot)
					;
				}
				$i++;
			}
			
			echo '</table>';
			
			if(Tools::getIsset('esportaexcel'))
			{
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'Codice')
				->setCellValue('B1', 'Descrizione')
				->setCellValue('C1', 'Costruttore')
				->setCellValue('D1', 'Fornitore')
				->setCellValue('E1', 'Mese')
				->setCellValue('F1', '2 mesi')
				->setCellValue('G1', '3 mesi')
				->setCellValue('H1', '6 mesi')
				->setCellValue('I1', '12 mesi')
				->setCellValue('J1', 'Ordine 1 mese')
				->setCellValue('K1', 'Periodo')
				->setCellValue('L1', 'Forecast periodo')
				->setCellValue('M1', 'Netto disp.')
				->setCellValue('N1', 'Ord. per periodo')
				;
				$objPHPExcel->getActiveSheet()->getStyle("B")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(70);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
				
				$objPHPExcel->getActiveSheet()->setTitle('Forecast');

				$objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	

				$objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getFill()->getStartColor()->setRGB('FFFF00');	

				$objPHPExcel->getActiveSheet()->getStyle("A1:N$i")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				
				$objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getFont()->setBold(true);
				
				$objPHPExcel->getActiveSheet()->getStyle("E2:E$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->getActiveSheet()->getStyle("F2:F$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->getActiveSheet()->getStyle("G2:G$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->getActiveSheet()->getStyle("H2:H$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->getActiveSheet()->getStyle("I2:I$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->getActiveSheet()->getStyle("J2:J$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->getActiveSheet()->getStyle("K2:K$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->getActiveSheet()->getStyle("L2:L$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->getActiveSheet()->getStyle("M2:M$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$objPHPExcel->getActiveSheet()->getStyle("N2:N$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
				$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
				PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

				$data = date("Ymd");

				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->setPreCalculateFormulas(false);
				$objWriter->save(str_replace(".php", ".xls", "esportazione-catalogo/catalogo_xls/forecast-prodotti-$data.php"));

				$this->_html .= "<div style='visibility:hidden'><br /><br />FILE PRONTO PER IL DOWNLOAD: <a id='scaricaexcelstatistiche' href='http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/forecast-prodotti-$data.xls' onclick='window.onbeforeunload = null'>CLICCA QUI PER SCARICARE</a>!</div>";

				header("Location: http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/forecast-prodotti-$data.xls");
			}
			echo "<br /><form method='post' action=''>";
				
				
			echo "	
			<input type='hidden' name='query' value=\"".$query."\" />		
			<input type='submit' value='Esporta i dati in formato Excel' name='esportaexcel' class='button'  onclick='window.onbeforeunload = null' />
			</form>
			";
			
		}
		else
		{
			echo '
			<script type="text/javascript">
			$(document).ready(function() {  $("#per_marchio").select2(); });
			$(document).ready(function() { $("#per_fornitore").val(""); $("#per_fornitore").select2();  });
			</script>
			
			
			<form name="forecast" method="post" action="">';
			
			
			echo 'Cerca in base al marchio:<br />
			<select multiple id="per_marchio" name="per_marchio[]" style="width:600px">
			<option name="0" value="0">--- Scegli un marchio ---</option>
			<option name="tutto" value="tutto">Tutti i marchi</option>
			';
			
			
			$resultsman = Db::getInstance()->ExecuteS("SELECT id_manufacturer, name FROM manufacturer ORDER BY name");
			
			foreach ($resultsman as $rowman) {
			
				echo "<option name='$rowman[id_manufacturer]' value='$rowman[id_manufacturer]'"; 
				echo ">$rowman[id_manufacturer] - $rowman[name]</option>";
				
			}
				
			echo "</select><br /><br />";
			
			echo 'Seleziona i fornitori:<br />
			<select multiple id="per_fornitore" name="per_fornitore[]" style="width:600px" class="fornitori">
			<option name="tutti_fornitori" value="tutto">Tutti i fornitori</option>
			';
			
			
			$resultssup = Db::getInstance()->ExecuteS("SELECT id_supplier, name FROM supplier WHERE name != '' ORDER BY name");
			
			foreach ($resultssup as $rowsup) {
			
				echo "<option name='$rowsup[id_supplier]' value='$rowsup[id_supplier]'"; 
				echo ">$rowsup[id_supplier] - $rowsup[name]</option>";
				
			}
				
			echo "</select><br /><br />";
			

			echo "<input type='submit' name='vai-forecast' value='Cerca' class='button' />
			</form>";
		}
	}

}
	
	
	
	
	
	
	
	
	
	
	
	
	
	