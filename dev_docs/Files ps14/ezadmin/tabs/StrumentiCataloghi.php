<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

class StrumentiCataloghi extends AdminPreferences
{


	public function display()
	{
		
		global $currentIndex, $cookie;
			
			if(Tools::getIsset('function'))
			{
				if(Tools::getValue('function') == 'usciti_di_produzione_allnet')
				{
					if(isset($_POST['del']))
					{
						foreach($_POST['delete_sp'] as $del) 
						{
							if(isset($del))
							{
								$prod = new Product($del);
								$prod->delete();
							}	
						}
						echo "<div class='conf'>Prodotto cancellati con successo</div>";
					
					}
					else if(isset($_POST['old']))
					{
						foreach($_POST['delete_sp'] as $del) 
						{
							if(isset($del))
							{
								Db::getInstance()->executeS('UPDATE product SET active = 2 WHERE id_product = '.$del);
							}	
						}
						echo "<div class='conf'>Prodotti messi in old con successo</div>";
					
					}
					else if(isset($_POST['off']))
					{
						foreach($_POST['delete_sp'] as $del) 
						{
							if(isset($del))
							{
								Db::getInstance()->executeS('UPDATE product SET active = 0 WHERE id_product = '.$del);
							}	
						}
						echo "<div class='conf'>Prodotti messi offline con successo</div>";
					
					}
					
					echo '<h1>Catalogo Allnet - Usciti di produzione</h1>';
					
					echo 'Da questa pagina puoi vedere quali prodotti inseriti su Ezdirect sono usciti di produzione dal catalogo Allnet. Criteri: vengono mostrati prodotti offline e online (NO old), che non hanno il flag "fuori produzione" e che non compaiono nel file di Allnet.<br /><br />';
					
					echo '<strong>ULTIMO AGGIORNAMENTO: '.date('d/m/Y H:i:s', filemtime("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/allnet/ezdirectNEW.csv")).'</strong><br /><br />';
					
					ini_set('display_errors', 'off');
					ini_set("memory_limit","892M");
					set_time_limit(36000);


					function in_array_r($needle, $haystack, $strict = false) {
						foreach ($haystack as $item) {
							if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
								return true;
							}
						}

						return false;
					}


					$id = 1;
					$dispzero = 0;
					$array = array();
					$array_vecchi = array();
					$i = 0;

					ini_set("memory_limit","892M");
					set_time_limit(3600);
					
					echo '<script type="text/javascript" src="../js/jquery/jquery.tablesorter.min.js"></script> 
					<script type="text/javascript" src="../js/jquery/jquery.tablesorter.widgets.js"></script> 
					<script type="text/javascript" src="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.js"></script> 
					<link rel="stylesheet" type="text/css" href="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.css" /> 
					<script type="text/javascript">
					$(document).ready(function() 
					{ 
					
						var myTextExtraction = function(node) 
						{ 
							// extract data from markup and return it 
							return node.textContent.replace( /<.*?>/g, "" ); 
						}
						
						$.tablesorter.addParser({ 
							// set a unique id 
							id: "thousands",
							is: function(s) { 
								// return false so this parser is not auto detected 
								return false; 
							}, 
							format: function(s) {
								// format your data for normalization 
								return s.replace(/[\s.$£€]/g,"").replace(/,/g,".");
							}, 
							// set type, either numeric or text 
							type: "numeric" 
						}); 

						$("#usciti_allnet").tablesorter({  headers: {
						8: {sorter: "shortDate", dateFormat: "ddmmyyyy"}, 5: {	sorter:"thousands"	}, 6: {	sorter:"thousands"	}, 7: {	sorter:"thousands"	}
						},  widgets: ["zebra"]});
						
					});
					</script>';
					
					echo '<form method="post" action="'.$currentIndex.'&token='.$this->token.'&function=usciti_di_produzione_allnet"><table class="table" id="usciti_allnet"><thead><tr><th>Codice Ezdirect</th><th>Codice SKU</th><th>Descrizione</th><th>Costruttore</th><th>Listino</th><th>Acquisto</th><th>Vendita</th><th>Att.?</th><th>Data usc.</th><th></th></tr></thead><tbody>';

					/*$array_skus = Db::getInstance()->executeS('SELECT reference, supplier_reference FROM product JOIN manufacturer ON product.id_manufacturer = manufacturer.id_manufacturer WHERE product.active < 2 AND product.fuori_produzione = 0 AND product.supplier_reference != "" AND manufacturer.supplier = 11');
					
					
					$array_sku = array();
					foreach($array_skus as $sku)
					{
						//$array_sku[] = trim(utf8_encode(strtolower($sku['reference'])));
						$array_vecchi[] = trim(utf8_encode(strtolower($sku['supplier_reference'])));

					}	

						
					$costruttori = Db::getInstance()->executeS('SELECT name FROM manufacturer WHERE supplier = 11');
					$array_costruttori = array();
					foreach($costruttori as $costruttore)
					{
						$nome_costruttore = $costruttore['name'];
						switch($nome_costruttore)
						{
							case 'Mitel Aastra': $n = 'Mitel'; break;
							case 'AVM Fritz!': $n = 'AVM'; break;
							case 'Cisco': $n = 'Cisco SMB'; break;
							case 'Deliberant': $n = 'DELIBERANT'; break;
							case 'Flir': $n = 'FLIR'; break;
							case 'Grandstream': $n = 'Grandstream Audio'; break;
							case 'Incom Unidata': $n = 'Incom - Unidata Comunication Sys'; break;
							case 'Jabra': $n = 'Jabra GN'; break;
							case 'Netgear': $n = 'NetGear'; break;
							case 'Polycom': $n = 'Polycom Audio'; break;
							case 'Portech': $n = 'PORTECH'; break;
							case 'Teltonika': $n = 'TELTONIKA'; break;
							case 'Ubiquiti Unifi': $n = 'UBIQUITI'; break;
							case 'Yealink': $n = 'YEALINK'; break;
							default: $n = $nome_costruttore; break;
						}	
						
						
						$array_costruttori[] = $n;
					}
					$array_costruttori[] = 'Grandstream';
					$array_costruttori[] = 'Grandstream VideoSorveglianza';
					$array_costruttori[] = 'Cisco Systems';
					$array_costruttori[] = 'Flir';
					$array_costruttori[] = 'Polycom Videoconferenza';
					$array_costruttori[] = 'Incom';
					$array_costruttori[] = 'Mitel SIP';
					$array_costruttori[] = 'Netgear';
					$array_costruttori[] = 'Cloud TC';
					$array_costruttori[] = 'Jabra GN';
					$array_costruttori[] = 'Polycom';
					$array_costruttori[] = 'Cisco';
					$array_costruttori[] = 'LG';
					$array_costruttori[] = 'vTech';
					$array_costruttori[] = 'YAMAHA';
					$array_costruttori[] = 'Yamaha';
					$array_costruttori[] = 'Revolabs Yamaha';

					if (($handle = fopen("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/ezdirect.csv", "r")) !== FALSE) {
					
						
						while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
							if(in_array($data[2], $array_costruttori))
							{
								$array[] = trim(utf8_encode((strtolower($data[1]))));
							}
						}
					}
					*/
	
					$id = 2;
					
					$array_vecchi = Db::getInstance()->executeS('SELECT SQL_CACHE p.id_product, p.active, p.reference, p.supplier_reference, p.listino, p.price, p.wholesale_price, pl.name as nome, m.name as costruttore, p.date_add, p.data_uscita_produzione FROM product p JOIN product_lang pl ON p.id_product = pl.id_product JOIN manufacturer m ON p.id_manufacturer = m.id_manufacturer WHERE pl.id_lang = 5 AND p.fuori_produzione = 0 AND p.id_supplier = 11 AND p.data_uscita_produzione > "0000-00-00"');
					
					
					foreach($array_vecchi as $dati)
					{
						//if( !in_array($prodotto, $array))
						//{
							
							$tokenProducts = Tools::getAdminToken('AdminCatalogExFeatures'.(int)Tab::getIdFromClassName('AdminCatalogExFeatures').(int)$cookie->id_employee);
							
							
							echo '<tr><td><span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($dati["id_product"]).'"><a href="index.php?tab=AdminCatalogExFeatures&id_product='.$dati["id_product"].'&updateproduct&token='.$tokenProducts.'" target="_blank">'.$dati['reference'].'</td><td>'.$dati['supplier_reference'].'</td><td>'.$dati['nome'].'</td><td>'.$dati['costruttore'].'</td><td style="text-align:right">'.number_format($dati['listino'],2,",","").'</td><td style="text-align:right">'.number_format($dati['wholesale_price'],2,",","").'</td><td style="text-align:right">'.number_format($dati['price'],2,",","").'</td><td>'.($dati['active']).'</td><td>'.date('d/m/Y',strtotime($dati['data_uscita_produzione'])).'</td><td><input type="checkbox" name="delete_sp[]" value="'.$dati['id_product'].'" /> </td></tr>';
							;
							
							$id++;
						//}
					}
					
					echo '</tbody></table><br /><br />
					<input class="button" type="submit" name="off" value="Metti offline i prodotti selezionati" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" />
					<input class="button" type="submit" name="old" value="Metti in old i prodotti selezionati" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" />
					<input class="button" type="submit" name="del" value="Cancella i prodotti selezionati" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" />';
					
					
				}
				
				else if(Tools::getValue('function') == 'usciti_di_produzione_intracom')
				{
					if(isset($_POST['del']))
					{
						foreach($_POST['delete_sp'] as $del) 
						{
							if(isset($del))
							{
								$prod = new Product($del);
								$prod->delete();
							}	
						}
						echo "<div class='conf'>Prodotti cancellati con successo</div>";
					
					}
					else if(isset($_POST['old']))
					{
						foreach($_POST['delete_sp'] as $del) 
						{
							if(isset($del))
							{
								Db::getInstance()->executeS('UPDATE product SET active = 2 WHERE id_product = '.$del);
							}	
						}
						echo "<div class='conf'>Prodotti messi in old con successo</div>";
					
					}
					else if(isset($_POST['off']))
					{
						foreach($_POST['delete_sp'] as $del) 
						{
							if(isset($del))
							{
								Db::getInstance()->executeS('UPDATE product SET active = 0 WHERE id_product = '.$del);
							}	
						}
						echo "<div class='conf'>Prodotti messi offline con successo</div>";
					
					}
					
					echo '<h1>Catalogo Intracom - Usciti di produzione</h1>';
					
					echo 'Da questa pagina puoi vedere quali prodotti inseriti su Ezdirect sono usciti di produzione dal catalogo Intracom. Criteri: vengono mostrati prodotti offline e online (NO old), che non hanno il flag "fuori produzione" e che non compaiono nel file di Intracom.<br /><br />';
					
					echo '<strong>ULTIMO AGGIORNAMENTO: '.date('d/m/Y H:i:s', filemtime("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/ezdirect-intracom.csv")).'</strong><br /><br />';
					
					ini_set('display_errors', 'off');
					ini_set("memory_limit","892M");
					set_time_limit(36000);


					$id = 1;
					$dispzero = 0;
					$array = array();
					$array_vecchi = array();
					$i = 0;

					ini_set("memory_limit","892M");
					set_time_limit(3600);
					
					echo '<script type="text/javascript" src="../js/jquery/jquery.tablesorter.min.js"></script> 
					<script type="text/javascript" src="../js/jquery/jquery.tablesorter.widgets.js"></script> 
					<script type="text/javascript" src="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.js"></script> 
					<link rel="stylesheet" type="text/css" href="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.css" /> 
					<script type="text/javascript">
					$(document).ready(function() 
					{ 
					
						var myTextExtraction = function(node) 
						{ 
							// extract data from markup and return it 
							return node.textContent.replace( /<.*?>/g, "" ); 
						}
						
						$.tablesorter.addParser({ 
							// set a unique id 
							id: "thousands",
							is: function(s) { 
								// return false so this parser is not auto detected 
								return false; 
							}, 
							format: function(s) {
								// format your data for normalization 
								return s.replace(/[\s.$£€]/g,"").replace(/,/g,".");
							}, 
							// set type, either numeric or text 
							type: "numeric" 
						}); 

						$("#usciti_allnet").tablesorter({  headers: {
						8: {sorter: "shortDate", dateFormat: "ddmmyyyy"}, 5: {	sorter:"thousands"	}, 6: {	sorter:"thousands"	}, 7: {	sorter:"thousands"	}
						},  widgets: ["zebra"]});
						
					});
					</script>';
					
					echo '<form method="post" action="'.$currentIndex.'&token='.$this->token.'&function=usciti_di_produzione_intracom"><table class="table" id="usciti_intracom"><thead><tr><th>Codice Ezdirect</th><th>Codice SKU</th><th>Descrizione</th><th>Costruttore</th><th>Listino</th><th>Acquisto</th><th>Vendita</th><th>Att.?</th><th>Data usc.</th><th></th></tr></thead><tbody>';

					$array_skus = Db::getInstance()->executeS('SELECT reference, supplier_reference FROM product JOIN manufacturer ON product.id_manufacturer = manufacturer.id_manufacturer WHERE product.active < 1 AND product.fuori_produzione = 0 AND product.supplier_reference != "" AND manufacturer.supplier = 95');
					
					
					/*$array_sku = array();
					foreach($array_skus as $sku)
					{
						//$array_sku[] = trim(utf8_encode(strtolower($sku['reference'])));
						$array_vecchi[] = trim(utf8_encode(strtolower($sku['supplier_reference'])));

					}	

						
					$costruttori = Db::getInstance()->executeS('SELECT name FROM manufacturer WHERE supplier = 95');
					$array_costruttori = array();
					foreach($costruttori as $costruttore)
					{
						$nome_costruttore = $costruttore['name'];
						switch($nome_costruttore)
						{
							case 'Techly Np': $n = 'Techly'; break;
							case 'Techly': $n = 'Techly'; break;
							case 'Techly Professional': $n = 'Techly'; break;
							case 'Intellinet': $n = 'Intellinet'; break;
							default: $n = $nome_costruttore; break;
						}	
						
						
						$array_costruttori[] = $n;
						$array_costruttori[] = 'Techly Np';
						$array_costruttori[] = 'Techly';
						$array_costruttori[] = 'Techly Professional';
					}
				

					if (($handle = fopen("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/ezdirect-intracom.csv", "r")) !== FALSE) {
					
						
						while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
							if(in_array($data[8], $array_costruttori))
							{
								$array[] = trim(utf8_encode((strtolower($data[0]))));
							}
						}
					}
	
					*/
					$id = 2;
					
					$array_vecchi = Db::getInstance()->executeS('SELECT SQL_CACHE p.id_product, p.active, p.reference, p.supplier_reference, p.listino, p.price, p.wholesale_price, pl.name as nome, m.name as costruttore, p.date_add, p.data_uscita_produzione FROM product p JOIN product_lang pl ON p.id_product = pl.id_product JOIN manufacturer m ON p.id_manufacturer = m.id_manufacturer WHERE pl.id_lang = 5 AND p.id_supplier = 95 AND p.fuori_produzione = 0 AND p.data_uscita_produzione > "0000-00-00"');
					
					foreach($array_vecchi as $dati)
					{
						//if( !in_array($prodotto, $array))
						//{
								
							$tokenProducts = Tools::getAdminToken('AdminCatalogExFeatures'.(int)Tab::getIdFromClassName('AdminCatalogExFeatures').(int)$cookie->id_employee);
				
							echo '<tr><td><span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($dati["id_product"]).'"><a href="index.php?tab=AdminCatalogExFeatures&id_product='.$dati["id_product"].'&updateproduct&token='.$tokenProducts.'" target="_blank">'.$dati['reference'].'</td><td>'.$dati['supplier_reference'].'</td><td>'.$dati['nome'].'</td><td>'.$dati['costruttore'].'</td><td style="text-align:right">'.number_format($dati['listino'],2,",","").'</td><td style="text-align:right">'.number_format($dati['wholesale_price'],2,",","").'</td><td style="text-align:right">'.number_format($dati['price'],2,",","").'</td><td>'.($dati['active']).'</td><td>'.date('d/m/Y',strtotime($dati['data_uscita_produzione'])).'</td><td><input type="checkbox" name="delete_sp[]" value="'.$dati['id_product'].'" /> </td></tr>';
							;
							
							$id++;
						//}
					}
					
					echo '</tbody></table>
					<br /><br />
					<input class="button" type="submit" name="off" value="Metti offline i prodotti selezionati" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" />
					<input class="button" type="submit" name="old" value="Metti in old i prodotti selezionati" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" />
					<input class="button" type="submit" name="del" value="Cancella i prodotti selezionati" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" />
					
					</form>';

				}
				
				else if(Tools::getValue('function') == 'nuovi_prodotti_allnet')
				{
					echo '<h1>Catalogo Allnet - Ultimi 100 prodotti inseriti</h1>';
					
					echo 'Da questa pagina puoi vedere quali sono gli ultimi 100 prodotti inseriti dal catalogo Allnet, con relativa data di inserimento.<br /><br />';
					
					echo '<strong>ULTIMO AGGIORNAMENTO: '.date('d/m/Y H:i:s', filemtime("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/allnet/ezdirectNEW.csv")).'</strong><br /><br />
					
					<a class="button" style="display:block; text-align:center; width:450px" href="https://ezdirectftp:nhfy57HGr32@www.ezdirect.it/docs/disponibilita/pronti-da-caricare.xls"><strong>ALLNET: scarica file con prodotti pronti da caricare</strong></a><br /><br />';
					
					
					
					echo '<script type="text/javascript" src="../js/jquery/jquery.tablesorter.min.js"></script> 
					<script type="text/javascript" src="../js/jquery/jquery.tablesorter.widgets.js"></script> 
					<script type="text/javascript" src="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.js"></script> 
					<link rel="stylesheet" type="text/css" href="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.css" /> 
					<script type="text/javascript">
					$(document).ready(function() 
					{ 
					
						var myTextExtraction = function(node) 
						{ 
							// extract data from markup and return it 
							return node.textContent.replace( /<.*?>/g, "" ); 
						}
						
						$.tablesorter.addParser({ 
							// set a unique id 
							id: "thousands",
							is: function(s) { 
								// return false so this parser is not auto detected 
								return false; 
							}, 
							format: function(s) {
								// format your data for normalization 
								return s.replace(/[\s.$£€]/g,"").replace(/,/g,".");
							}, 
							// set type, either numeric or text 
							type: "numeric" 
						}); 

						$("#usciti_allnet").tablesorter({  headers: {
						8: {sorter: "shortDate", dateFormat: "ddmmyyyy"}, 5: {	sorter:"thousands"	}, 6: {	sorter:"thousands"	}, 7: {	sorter:"thousands"	}
						},  widgets: ["zebra"]});
						
					});
					</script>';
					
					echo '<table class="table" id="usciti_allnet"><thead><tr><th>Codice Ezdirect</th><th>Codice SKU</th><th>Descrizione</th><th>Costruttore</th><th>Listino</th><th>Acquisto</th><th>Vendita</th><th>Att.?</th><th>Data ins.</th></tr></thead><tbody>';

					$array_nuovi = Db::getInstance()->executeS('SELECT p.id_product, p.active, p.reference, p.supplier_reference, p.listino, p.price, p.wholesale_price, pl.name as nome, m.name as costruttore, p.date_add FROM product p JOIN product_lang pl ON p.id_product = pl.id_product JOIN manufacturer m ON p.id_manufacturer = m.id_manufacturer  WHERE p.active < 1 AND p.id_supplier = 11 GROUP BY p.id_product ORDER BY p.date_add DESC LIMIT 100');
					
					foreach($array_nuovi as $dati)
					{
						
						$tokenProducts = Tools::getAdminToken('AdminCatalogExFeatures'.(int)Tab::getIdFromClassName('AdminCatalogExFeatures').(int)$cookie->id_employee);
						
						Db::getInstance()->executeS('UPDATE product SET supplier_quantity = 0, arrivo_quantity = 0 WHERE id_product = '.$id_product);
						
						echo '<tr><td><span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($dati["id_product"]).'"><a href="index.php?tab=AdminCatalogExFeatures&id_product='.$dati["id_product"].'&updateproduct&token='.$tokenProducts.'" target="_blank">'.$dati['reference'].'</td><td>'.$dati['supplier_reference'].'</td><td>'.$dati['nome'].'</td><td>'.$dati['costruttore'].'</td><td style="text-align:right">'.number_format($dati['listino'],2,",","").'</td><td style="text-align:right">'.number_format($dati['wholesale_price'],2,",","").'</td><td style="text-align:right">'.number_format($dati['price'],2,",","").'</td><td>'.($dati['active']).'</td><td>'.date('d/m/Y',strtotime($dati['date_add'])).'</td></tr>';
						;
							
							
					}
					
					echo '</tbody></table>';

				}
				
				else if(Tools::getValue('function') == 'nuovi_prodotti_intracom')
				{
					echo '<h1>Catalogo Intracom - Ultimi 100 prodotti inseriti</h1>';
					
					echo 'Da questa pagina puoi vedere quali sono gli ultimi 100 prodotti inseriti dal catalogo Intracom, con relativa data di inserimento.<br /><br />';
					
					echo '<strong>ULTIMO AGGIORNAMENTO: '.date('d/m/Y H:i:s', filemtime("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/ezdirect-intracom.csv")).'</strong><br /><br />';
					
					echo '<script type="text/javascript" src="../js/jquery/jquery.tablesorter.min.js"></script> 
					<script type="text/javascript" src="../js/jquery/jquery.tablesorter.widgets.js"></script> 
					<script type="text/javascript" src="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.js"></script> 
					<link rel="stylesheet" type="text/css" href="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.css" /> 
					<script type="text/javascript">
					$(document).ready(function() 
					{ 
					
						var myTextExtraction = function(node) 
						{ 
							// extract data from markup and return it 
							return node.textContent.replace( /<.*?>/g, "" ); 
						}
						
						$.tablesorter.addParser({ 
							// set a unique id 
							id: "thousands",
							is: function(s) { 
								// return false so this parser is not auto detected 
								return false; 
							}, 
							format: function(s) {
								// format your data for normalization 
								return s.replace(/[\s.$£€]/g,"").replace(/,/g,".");
							}, 
							// set type, either numeric or text 
							type: "numeric" 
						}); 

						$("#usciti_allnet").tablesorter({  headers: {
						8: {sorter: "shortDate", dateFormat: "ddmmyyyy"}, 5: {	sorter:"thousands"	}, 6: {	sorter:"thousands"	}, 7: {	sorter:"thousands"	}
						},  widgets: ["zebra"]});
						
					});
					</script>';
					
					echo '<table class="table" id="usciti_allnet"><thead><tr><th>Codice Ezdirect</th><th>Codice SKU</th><th>Descrizione</th><th>Costruttore</th><th>Listino</th><th>Acquisto</th><th>Vendita</th><th>Att.?</th><th>Data ins.</th></tr></thead><tbody>';

					$array_nuovi = Db::getInstance()->executeS('SELECT p.id_product, p.active, p.reference, p.supplier_reference, p.listino, p.price, p.wholesale_price, pl.name as nome, m.name as costruttore, p.date_add FROM product p JOIN product_lang pl ON p.id_product = pl.id_product JOIN manufacturer m ON p.id_manufacturer = m.id_manufacturer  WHERE p.active < 2 AND p.id_supplier = 95 GROUP BY p.id_product ORDER BY p.date_add DESC LIMIT 100');
					
					foreach($array_nuovi as $dati)
					{
						
						$tokenProducts = Tools::getAdminToken('AdminCatalogExFeatures'.(int)Tab::getIdFromClassName('AdminCatalogExFeatures').(int)$cookie->id_employee);
						
						Db::getInstance()->executeS('UPDATE product SET supplier_quantity = 0, arrivo_quantity = 0 WHERE id_product = '.$id_product);
						
						echo '<tr><td><span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($dati["id_product"]).'"><a href="index.php?tab=AdminCatalogExFeatures&id_product='.$dati["id_product"].'&updateproduct&token='.$tokenProducts.'" target="_blank">'.$dati['reference'].'</td><td>'.$dati['supplier_reference'].'</td><td>'.$dati['nome'].'</td><td>'.$dati['costruttore'].'</td><td style="text-align:right">'.number_format($dati['listino'],2,",","").'</td><td style="text-align:right">'.number_format($dati['wholesale_price'],2,",","").'</td><td style="text-align:right">'.number_format($dati['price'],2,",","").'</td><td>'.($dati['active']).'</td><td>'.date('d/m/Y',strtotime($dati['date_add'])).'</td></tr>';
						;
							
							
					}
					
					echo '</tbody></table>';

				}
				
				else if(Tools::getIsset('zzz'))
				{
					
				}
				
			}
			else
			{
				echo "<h1>Strumenti cataloghi fornitori</h1>";
					echo 'Funzioni per lavoro sui cataloghi fornitori: da questa pagina puoi selezionare diversi strumenti per ottenere informazioni sui prodotti <br /><br />
					
					<a class="button" style="display:block; text-align:center; width:450px" href="https://ezdirectftp:nhfy57HGr32@www.ezdirect.it/docs/disponibilita/pronti-da-caricare.xls"><strong>ALLNET: scarica file con prodotti pronti da caricare</strong></a><br /><br />
					
					<a class="button" style="display:block; text-align:center; width:450px" href="'.$currentIndex.'&token='.$this->token.'&function=usciti_di_produzione_allnet">ALLNET: vedi prodotti usciti di produzione</a><br /><br />';
				
					echo '<a class="button" style="display:block; text-align:center; width:450px" href="'.$currentIndex.'&token='.$this->token.'&function=nuovi_prodotti_allnet">ALLNET: vedi ultimi 100 prodotti inseriti</a><br /><br />';
					
					echo '<a class="button" style="display:block; text-align:center; width:450px" href="'.$currentIndex.'&token='.$this->token.'&function=usciti_di_produzione_intracom">INTRACOM: vedi prodotti usciti di produzione</a><br /><br />';
				
					echo '<a class="button" style="display:block; text-align:center; width:450px" href="'.$currentIndex.'&token='.$this->token.'&function=nuovi_prodotti_intracom">INTRACOM: vedi ultimi 100 prodotti inseriti</a><br /><br />';
					
					echo '... oppure puoi inserire in questa cella un codice, un nome o la parte di un codice o un nome di prodotto per ottenere informazioni sullo stato presso il fornitore. <br /><br />
					<form method="post">
					
					</form>
					';
				
			}
		
		
	}

}


