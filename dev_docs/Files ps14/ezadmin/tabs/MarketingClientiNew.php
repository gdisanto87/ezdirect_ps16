<?php

	include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

	class MarketingClientiNew extends AdminPreferences {
	
	public function bindDatepickerZ($id, $time)
	{
		if ($time)
			echo '
			var dateObj = new Date();
			var hours = dateObj.getHours();
			var mins = dateObj.getMinutes();
			var secs = dateObj.getSeconds();
			if (hours < 10) { hours = "0" + hours; }
			if (mins < 10) { mins = "0" + mins; }
			if (secs < 10) { secs = "0" + secs; }
			var time = " "+hours+":"+mins+":"+secs;';

		echo '
		$(function() {
			$("#'.Tools::htmlentitiesUTF8($id).'").datepicker({
				prevText:"",
				nextText:"",
				dateFormat:"dd-mm-yy"});
		});';
	}

	// id can be a identifier or an array of identifiers
	public function includeDatepickerZ($id, $time = false)
	{
		global $cookie;
		echo '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery-ui-1.8.10.custom.min.js"></script>';
		$iso = Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'lang WHERE `id_lang` = '.(int)$cookie->id_lang);
		if ($iso != 'en')
			echo '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/datepicker/ui/i18n/ui.datepicker-'.Tools::htmlentitiesUTF8($iso).'.js"></script>';
		echo '<script type="text/javascript">';
			if (is_array($id))
				foreach ($id as $id2)
					$this->bindDatepickerZ($id2, $time);
			else
				$this->bindDatepickerZ($id, $time);
		echo '</script>';
	}

	public	function datediff($tipo, $partenza, $fine)
    {
        switch ($tipo)
        {
            case "A" : $tipo = 365;
            break;
            case "M" : $tipo = (365 / 12);
            break;
            case "S" : $tipo = (365 / 52);
            break;
            case "G" : $tipo = 1;
            break;
        }
		$arrpulitopartenza = explode(" ", $partenza);
        $arr_partenza = explode("-", $arrpulitopartenza[0]);
        $partenza_gg = $arr_partenza[2];
        $partenza_mm = $arr_partenza[1];
        $partenza_aa = $arr_partenza[0];
		$arrpulitofine = explode(" ", $fine);
        $arr_fine = explode("-", $arrpulitofine[0]);
        $fine_gg = $arr_fine[2];
        $fine_mm = $arr_fine[1];
        $fine_aa = $arr_fine[0];
        $date_diff = mktime(12, 0, 0, $fine_mm, $fine_gg, $fine_aa) - mktime(12, 0, 0, $partenza_mm, $partenza_gg, $partenza_aa);
        $date_diff  = floor(($date_diff / 60 / 60 / 24) / $tipo);
        return $date_diff;
    }
	
		public function display()
		{
			global $cookie;
			
			/*if($cookie->id_employee != 6)
				die('Errore! Non hai i permessi per usare questo strumento');
			*/
			if (get_magic_quotes_gpc()) {
				$process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
				while (list($key, $val) = each($process)) {
					foreach ($val as $k => $v) {
						unset($process[$key][$k]);
						if (is_array($v)) {
							$process[$key][stripslashes($k)] = $v;
							$process[] = &$process[$key][stripslashes($k)];
						} else {
							$process[$key][stripslashes($k)] = stripslashes($v);
						}
					}
				}
				unset($process);
			}


			global $cookie;
			echo "<h1>Marketing su fatturato</h1>";

			echo "<p>Questo strumento ti consente di estrapolare liste di clienti (per invii mail, per statistiche... ) sulla base del fatturato. Puoi profilare i clienti secondo diversi filtri creando delle liste ad hoc. I dati sono esportabili in Excel ed &egrave; possibile anche esportare liste gi&agrave; formattate per l'inserimento su Mailup.</p>";
			
						
			$anno_corrente = date("Y");
			
			if(!empty($_POST['arco_dal'])) {
				$data_dal_a = explode("-", $_POST['arco_dal']);
				$data_dal = $data_dal_a[2]."-".$data_dal_a[1]."-".$data_dal_a[0]; 
			}
			//else // PERCHE' ENTRA SEMPRE NELL'ELSE??? SBAGLIA EXPORT
				//$data_dal = '2020-01-01';
			
			//if(isset($_POST['striscia_arco_dal']))
				//$striscia_arco_dal = $_POST['striscia_arco_dal'];
			
			if(!empty($_POST['arco_al'])) {	
				$data_al_a = explode("-", $_POST['arco_al']);
				$data_al = $data_al_a[2]."-".$data_al_a[1]."-".$data_al_a[0]; 
			}
			//else // PERCHE' ENTRA SEMPRE NELL'ELSE??? SBAGLIA EXPORT
				//$data_al = '2020-12-31';
			
			if(isset($_POST['cercaiclienti'])) { 
			
				$prodotti = "";
				$categorie_di_default = "";
				$macrocategorie = "";
				$macrocategorie_di_default = "";
				$categorie = "";
				$marchi = "";
				
				if(Tools::getValue('modalita') == 1)
				{
					$strisciamodalita = 'Ricerca eseguita sia tra clienti che hanno acquistato, sia tra clienti che non hanno acquistato';		
					$modalita = 1;
				}	
				else if(Tools::getValue('modalita') == 2)
				{
					$strisciamodalita = 'Ricerca eseguita tra i clienti che non hanno mai acquistato';		
					$modalita = 2;
				}	
				else
				{
					$strisciamodalita = 'Ricerca eseguita solo tra clienti che hanno acquistato';			
					$modalita = 0;
				}
				echo "<strong>Riepilogo</strong><br /><br />";
				
				if(isset($_POST['per_prodotto'])) {
					echo "<strong>Clienti che hanno acquistato questi prodotti precisi:</strong><br /> ";
					
					
					foreach ($_POST['per_prodotto'] as $prodotto) {

						$id_product = Db::getInstance()->getValue("SELECT id_product FROM product WHERE reference = '".$prodotto."'");
						$ref_prod = $prodotto;
						$nomeprodotto = Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_lang = 5 AND id_product = ".$id_product."");
						if(!$nomeprodotto || $nomeprodotto == '')
							$nomeprodotto = Db::getInstance()->getValue("SELECT desc_articolo FROM fattura WHERE cod_articolo = '".$prodotto."'");
						
						$prodottiinclusi.= $nomeprodotto."; ";
						
						$strisciaprodotti.= "- ".$nomeprodotto."<br />";
	
						$prodotti.= "f.cod_articolo = '$prodotto' OR ";
					
					}
				}
				
				else if(isset($_POST['prodottiinclusi']) && !empty($_POST['prodottiinclusi'])) {
				
					echo "<strong>Clienti che hanno acquistato questi prodotti precisi:</strong><br /> ";
					$strisciaprodotti = $_POST['strisciaprodotti'];
					$prodottiinclusi = $_POST['prodottiinclusi'];
				
				
				}
				
				if($strisciaprodotti != '')
				{
					echo $strisciaprodotti;
					echo "<br />";
				}
				
				if(isset($_POST['per_categoria'])) {
				
					echo "<strong>Clienti che hanno acquistato prodotti in queste sottocategorie:</strong><br /> ";
			
			
					foreach ($_POST['per_categoria'] as $categoria) {
	
						$nomecat = Db::getInstance()->getValue("SELECT name FROM category_lang WHERE id_lang = 5 AND id_category = ".$categoria."");
						$strisciacat .= "- ".$nomecat."<br />";
						
					
						$categorieincluse.= $nomecat."; ";
						
						$categorie_di_default.= "f.id_category = $categoria OR ";
						
					}	
					$categorie = $categorie_di_default;
				}
				
				
				else if(isset($_POST['categorieincluse']) && !empty($_POST['categorieincluse'])) {
				
					echo "<strong>Clienti che hanno acquistato prodotti in queste sottocategorie:</strong><br /> ";
					$strisciacat = $_POST['strisciacat'];
					$categorieincluse = $_POST['categorieincluse'];
				
				
				}
				
				if($strisciacat != '')
				{
					echo $strisciacat;
				}
				
				if(isset($_POST['per_macrocategoria'])) {
				
					echo "<strong>Clienti che hanno acquistato prodotti in queste macrocategorie:</strong><br /> ";
			
			
					foreach ($_POST['per_macrocategoria'] as $macrocategoria) {
	
						$nomemacrocat = Db::getInstance()->getValue("SELECT name FROM category_lang WHERE id_lang = 5 AND id_category = ".$macrocategoria."");
						$strisciamacrocat .= "- ".$nomemacrocat."<br />";
						
						$children = Db::getInstance()->executeS("SELECT id_category FROM
						(SELECT t4.id_category
						FROM category AS t1
						LEFT JOIN category AS t2 ON t1.id_category = t2.id_parent
						LEFT JOIN category AS t3 ON t2.id_category = t3.id_parent
						LEFT JOIN category AS t4 ON t3.id_category = t3.id_parent
						WHERE t2.id_parent =$macrocategoria

						UNION 

						SELECT t3.id_category
						FROM category AS t1
						LEFT JOIN category AS t2 ON t1.id_category = t2.id_parent
						LEFT JOIN category AS t3 ON t2.id_category = t3.id_parent
						WHERE t2.id_parent =$macrocategoria

						UNION 

						SELECT t2.id_category
						FROM category AS t1
						LEFT JOIN category AS t2 ON t1.id_category = t2.id_parent
						WHERE t2.id_parent =$macrocategoria

						UNION 

						SELECT id_category FROM category WHERE id_category = $macrocategoria) ccc WHERE id_category IS NOT NULL ORDER BY id_category ASC

						");

						foreach ($children as $child) {
	
							$macrocategorie_di_default.= "f.id_category = ".$child['id_category']." OR ";
						}
						
						$macrocategorie = $macrocategorie_di_default;
						$macrocategorieincluse.= $nomemacrocat."; ";
						
						
				
					}	
					
				}
				
				
				else if(isset($_POST['macrocategorieincluse']) && !empty($_POST['macrocategorieincluse'])) {
				
					echo "<strong>Clienti che hanno acquistato prodotti in queste macrocategorie:</strong><br /> ";
					$strisciamacrocat = $_POST['strisciamacrocat'];
					$macrocategorieincluse = $_POST['macrocategorieincluse'];
				
				
				}
				
				if($strisciamacrocat != '')
					echo $strisciamacrocat;
				
				if(isset($_POST['per_marchio'])) {
				
					echo "<strong>Clienti che hanno acquistato prodotti in questi marchi:</strong><br /> ";
			
			
					foreach ($_POST['per_marchio'] as $marchio) {
	
						$nomemarchio = Db::getInstance()->getValue("SELECT name FROM manufacturer WHERE id_manufacturer = ".$marchio."");
						$strisciamarchi .= "- ".$nomemarchio."<br />";
						
					
						$marchiinclusi.= $nomemarchio."; ";
						$marchi.= "f.id_manufacturer = $marchio OR ";
					}	
					
				}
				
				else if(isset($_POST['marchiinclusi']) && !empty($_POST['marchiinclusi'])) {
				
					echo "<strong>Prodotti in questi marchi:</strong><br /> ";
					$strisciamarchi = $_POST['strisciamarchi'];
					$marchiinclusi = $_POST['marchiinclusi'];
				
				
				}
				
				if($strisciamarchi != '')
					echo $strisciamarchi;
				
				
			
				echo $striscia_escludi_privati."<br />";
				
				if(!empty($_POST['arco_dal'])) {
					
					$data_dal_a = explode("-", $_POST['arco_dal']);
					$data_dal = $data_dal_a[2]."-".$data_dal_a[1]."-".$data_dal_a[0]; 
					
					if($modalita == 1 || $modalita == 2)
						$arco_dal = 'AND c.date_add > "'.$data_dal.' 00:00:00"';
					else
						$arco_dal = 'AND f.data_fattura > "'.$data_dal.' 00:00:00"';
					
					$arco_dal_clienti = 'AND date_add > "'.$data_dal.' 00:00:00"';
					
					$striscia_arco_dal = "Da: <strong>$_POST[arco_dal]</strong> ";
					
				}
				else {
					$striscia_arco_dal = "Da: <strong>inizio della storia del sito</strong> ";
				
				}

				if(isset($_POST['striscia_arco_dal'])) {
				
					$striscia_arco_dal = $_POST['striscia_arco_dal'];
					$arco_dal_clienti = '';
				}
			
				echo $striscia_arco_dal;
				
				if(!empty($_POST['arco_al'])) {
				
					$data_al_a = explode("-", $_POST['arco_al']);
					$data_al = $data_al_a[2]."-".$data_al_a[1]."-".$data_al_a[0]; 
					
					if($modalita == 1 || $modalita == 2)
						$arco_al = 'AND c.date_add < "'.$data_al.' 00:00:00"';
					else
						$arco_al = 'AND f.data_fattura < "'.$data_al.' 00:00:00"';
					
					$arco_al_clienti = 'AND date_add < "'.$data_al.' 00:00:00"';
				
					$striscia_arco_al = "a: <strong>$_POST[arco_al]</strong> ";
				}
				else {
					$striscia_arco_al = "a: <strong>oggi</strong> ";
					$arco_al_clienti = '';
				}
				
				if(isset($_POST['striscia_arco_al'])) {
				
					$striscia_arco_al = $_POST['striscia_arco_al'];
				}
			
				echo $striscia_arco_al."<br />";
				
				//nuovi filtri
				
				$strisciaprivati = '';
				$strisciainattivita = '';
				$strisciatipocliente = '';
				$strisciazonacliente = '';
				$strisciagruppi = '';
				$strisciaprevric = '';
				$strisciaprevriv = '';
				$strisciaricric = '';
				$strisciakeyword = '';
				
				$gruppi = 'AND (';
				foreach($_POST['group'] as $key => $value)
				{	
					$gruppi .= 'c.id_default_group = '.$key.' OR ';
					$strisciagruppi .= ' '.Db::getInstance()->getValue('SELECT name FROM group_lang WHERE id_group = '.$key.' AND id_lang = 5').',';
				}	
				$gruppi	.= 'c.id_default_group = 99999999)';
				$strisciagruppi = 'In questi gruppi:'.substr($strisciagruppi, 0, -1);	
					
				if($_POST['escludi_privati'] == 1) {
				
					$strisciaprivati = "<strong>Privati esclusi</strong>";
				}
				else {
					$strisciaprivati = "<strong>Aziende e privati inclusi</strong>";
				}
				
				if(Tools::getValue('inattivita') > 0)
					$strisciainattivita = 'Che non acquistano da '.Tools::getValue('inattivita').' mesi';
				
				if(Tools::getValue('tipo_cliente') != '')
					$strisciatipocliente = 'Tipo cliente: '.Tools::getValue('tipo_cliente').'';
								
				if(Tools::getValue('zona_cliente') != '')
					$strisciazonacliente = 'Zona cliente: '.Tools::getValue('zona_cliente').'';
				
				if(Tools::getIsset('prev_richiesto'))
					$strisciaprevric = 'Che hanno richiesto un preventivo';
				
				if(Tools::getIsset('prev_ricevuto'))
					$strisciaprevriv = 'Che hanno ricevuto un preventivo';
				
				if(Tools::getIsset('ric_richiesto'))
					$strisciaricric = 'Che hanno richiesto un ti richiamiamo noi';
				
				if(Tools::getIsset('1piuacquisti'))
					$strisciaricric = 'Che hanno fatto pi&ugrave; di un acquisto';
				
				
				if(Tools::getValue('keyword') != '')
					$strisciakeyword = 'Che includono la keyword <strong>'.Tools::getValue('keyword').'</strong>';
				
				echo $strisciaprivati.'<br />'.$strisciagruppi.'<br />'.($strisciainattivita != '' ? $strisciainattivita.'<br />' : '').($strisciatipocliente != '' ? $strisciatipocliente.'<br />' : '').($strisciazonacliente != '' ? $strisciazonacliente.'<br />' : '').($strisciaprevric != '' ? $strisciaprevric.'<br />' : '').($strisciaprevriv != '' ? $strisciaprevriv.'<br />' : '').$strisciaricric.'<br />'.$strisciakeyword.'<br /><strong>'.$strisciamodalita.'</strong><br />';
				
				//fine nuovi filtri
				
				
					$where = "
					
						
						";
						
						if(isset($_POST['per_prodotto'])) {
							$where.= "AND 
							(
							$prodotti
							f.cod_articolo = '9999999999999999999999'
							)";
						}
						if(isset($_POST['per_categoria'])) {
						
							$where .= "AND 
							(
							$categorie
							f.id_category = 9999999999999999999999
							)";
						
						}
						
						if(isset($_POST['per_marchio'])) {
						
							$where .= "
							AND 
							(
							$marchi
							f.id_manufacturer = 9999999999999999999999
							)";
						}
						
						if(isset($_POST['per_macrocategoria'])) {
						
							$where .= "
							AND
							(
							$macrocategorie
							f.id_category = 9999999999999999999999
							)";
						
						}
						
				
				
				if($modalita == 1 || $modalita == 2)
				{
					$from = 'FROM customer c
						LEFT JOIN address a ON c.id_customer = a.id_customer
						LEFT JOIN state s ON a.id_state = s.id_state
					';
					
				}	
				else
				{
					$from = 'FROM fattura f
						JOIN customer c ON f.id_customer = c.id_customer
						JOIN address a ON c.id_customer = a.id_customer
						JOIN state s ON a.id_state = s.id_state
					';
				}
				
				
				
				if(isset($_POST['querysql'])) {

					$sql = $_POST['querysql'];
					
				}
			
				else {
				
						$sql = "SELECT * FROM (SELECT c.id_customer, c.firstname, c.lastname, gr.name gruppo, s.iso_code, c.email, c.id_default_group, c.is_company, c.company, a.phone, a.phone_mobile, a.fax, fat.totale_acquisti, fat.quantita_totale, fa.ultimo_anno,

						c.firstname AS nome_mailup, c.lastname AS cognome_mailup, c.company AS azienda_mailup, c.email AS email_mailup, REPLACE(a.address1,',',' ') AS indirizzo_mailup, a.city AS citta_mailup, a.postcode AS cap_mailup, a.phone AS telefono_mailup, a.phone_mobile AS cellulare_mailup,

						primo.data_fattura AS primo_acquisto, ultimo.data_fattura AS ultimo_acquisto, (CASE WHEN c.is_company = 0 THEN CONCAT(c.lastname,\" \",c.firstname) WHEN c.is_company = 1 THEN (CASE WHEN c.company IS NULL OR c.company = \"\" THEN CONCAT(c.lastname,\" \",c.firstname) ELSE c.company END) END) cliente
						$from
						LEFT JOIN (SELECT * FROM fattura GROUP BY id_customer ORDER BY data_fattura ASC ) primo ON primo.id_customer = c.id_customer
						LEFT JOIN (SELECT id_customer, MAX(date_add) data_fattura FROM orders GROUP BY id_customer ORDER BY date_add DESC) ultimo ON ultimo.id_customer = c.id_customer
						
						LEFT JOIN (SELECT * FROM group_lang WHERE id_lang = 5) gr ON c.id_default_group = gr.id_group
						LEFT JOIN (SELECT SUM(fprv.totale_fattura) AS totale_acquisti, sum(fprv.quant_tot) AS quantita_totale, fprv.id_customer FROM (SELECT totale_fattura, sum(qt_ord) quant_tot, id_customer FROM fattura GROUP BY id_fattura) fprv GROUP BY fprv.id_customer) fat ON fat.id_customer = c.id_customer
						
						LEFT JOIN (SELECT SUM(fprv.totale_fattura) AS totale_acquisti, sum(fprv.quant_tot) AS quantita_totale, fprv.id_customer FROM (SELECT totale_fattura, sum(qt_ord) quant_tot, id_customer FROM fattura f WHERE id_riga > 0 $where GROUP BY id_fattura) fprv GROUP BY fprv.id_customer) fat_corrente ON fat_corrente.id_customer = c.id_customer
						
						LEFT JOIN (SELECT SUM(totale_fattura) AS ultimo_anno, id_customer FROM (SELECT totale_fattura, id_customer FROM fattura WHERE data_fattura > '".$anno_corrente."-01-01 00:00:00' AND data_fattura < '".$anno_corrente."-12-31 00:00:00' GROUP BY id_fattura) faprv GROUP BY faprv.id_customer) fa ON fa.id_customer = c.id_customer
						
						".(Tools::getIsset('prev_richiesto') ? 'JOIN (SELECT * FROM form_prevendita_thread WHERE tipo_richiesta = "preventivo") fp ON fp.id_customer = c.id_customer' : '')."
						".(Tools::getIsset('ric_richiesto') ? 'JOIN (SELECT * FROM form_prevendita_thread WHERE tipo_richiesta = "tirichiamiamonoi") fp2 ON fp2.id_customer = c.id_customer' : '')."
						
						".(Tools::getIsset('1piuacquisti') ? 'JOIN (SELECT * FROM orders WHERE id_customer > 0 '.$arco_dal_clienti.' '.$arco_al_clienti.' group by id_customer having count(id_customer) > 2 ) 1piu ON 1piu.id_customer = c.id_customer' : '')."
						
						".(Tools::getIsset('prev_ricevuto')  ? 'JOIN cart ca ON ca.id_customer = c.id_customer' : '')."
						
						WHERE a.fatturazione =1
						".(Tools::getValue('quantita') > 0 ? 'AND fat_corrente.quantita_totale > '.Tools::getValue('quantita') : '')."
						".(Tools::getValue('importo') > 0 ? 'AND fat_corrente.totale_acquisti > '.Tools::getValue('importo') : '')."
						AND a.deleted =0
						AND a.active =1 
						
						".(Tools::getValue('zona_cliente') == 'nord' ? "AND (s.id_state = 124 or s.id_state =  126 or s.id_state =  129 or s.id_state =  133 or s.id_state =  135 or s.id_state =  136 or s.id_state =  137 or s.id_state =  138 or s.id_state =  139 or s.id_state =  149 or s.id_state =  151 or s.id_state =  153 or s.id_state =  156 or s.id_state =  159 or s.id_state =  161 or s.id_state =  162 or s.id_state =  164 or s.id_state =  167 or s.id_state =  170 or s.id_state =  172 or s.id_state =  175 or s.id_state =  180 or s.id_state =  181 or s.id_state =  182 or s.id_state =  184 or s.id_state =  189 or s.id_state =  191 or s.id_state =  192 or s.id_state =  196 or s.id_state =  199 or s.id_state =  203 or s.id_state =  205 or s.id_state =  207 or s.id_state =  209 or s.id_state =  212 or s.id_state =  215 or s.id_state =  219 or s.id_state =  221 or s.id_state =  222 or s.id_state =  223 or s.id_state =  224 or s.id_state =  225 or s.id_state =  226 or s.id_state =  227 or s.id_state =  228 or s.id_state =  229 or s.id_state =  231)" : '')."
						
						".(Tools::getValue('zona_cliente') == 'centro' ? "AND (s.id_state = 125 or s.id_state =  127 or s.id_state =  128 or s.id_state =  155 or s.id_state =  157 or s.id_state =  160 or s.id_state =  163 or s.id_state =  168 or s.id_state =  171 or s.id_state =  173 or s.id_state =  174 or s.id_state =  176 or s.id_state =  193 or s.id_state =  194 or s.id_state =  197 or s.id_state =  198 or s.id_state =  201 or s.id_state =  206 or s.id_state =  208 or s.id_state =  213 or s.id_state =  218 or s.id_state =  232)" : '')."
						
						".(Tools::getValue('zona_cliente') == 'sud' ? "AND (s.id_state = 123 or s.id_state =  130 or s.id_state =  131 or s.id_state =  132 or s.id_state =  134 or s.id_state =  140 or s.id_state =  141 or s.id_state =  142 or s.id_state =  143 or s.id_state =  144 or s.id_state =  145 or s.id_state =  146 or s.id_state =  147 or s.id_state =  148 or s.id_state =  150 or s.id_state =  152 or s.id_state =  154 or s.id_state =  158 or s.id_state =  165 or s.id_state =  166 or s.id_state =  169 or s.id_state =  177 or s.id_state =  178 or s.id_state =  179 or s.id_state =  183 or s.id_state =  185 or s.id_state =  186 or s.id_state =  187 or s.id_state =  188 or s.id_state =  190 or s.id_state =  195 or s.id_state =  200 or s.id_state =  202 or s.id_state =  204 or s.id_state =  210 or s.id_state =  211 or s.id_state =  214 or s.id_state =  216 or s.id_state =  217 or s.id_state =  220 or s.id_state =  230)" : '')."
						
						".(Tools::getValue('zona_cliente') == 'nd' ? "and s.id_state != 124 and s.id_state !=  126 and s.id_state !=  129 and s.id_state !=  133 and s.id_state !=  135 and s.id_state !=  136 and s.id_state !=  137 and s.id_state !=  138 and s.id_state !=  139 and s.id_state !=  149 and s.id_state !=  151 and s.id_state !=  153 and s.id_state !=  156 and s.id_state !=  159 and s.id_state !=  161 and s.id_state !=  162 and s.id_state !=  164 and s.id_state !=  167 and s.id_state !=  170 and s.id_state !=  172 and s.id_state !=  175 and s.id_state !=  180 and s.id_state !=  181 and s.id_state !=  182 and s.id_state !=  184 and s.id_state !=  189 and s.id_state !=  191 and s.id_state !=  192 and s.id_state !=  196 and s.id_state !=  199 and s.id_state !=  203 and s.id_state !=  205 and s.id_state !=  207 and s.id_state !=  209 and s.id_state !=  212 and s.id_state !=  215 and s.id_state !=  219 and s.id_state !=  221 and s.id_state !=  222 and s.id_state !=  223 and s.id_state !=  224 and s.id_state !=  225 and s.id_state !=  226 and s.id_state !=  227 and s.id_state !=  228 and s.id_state !=  229 and s.id_state !=  231 and s.id_state != 125 and s.id_state !=  127 and s.id_state !=  128 and s.id_state !=  155 and s.id_state !=  157 and s.id_state !=  160 and s.id_state !=  163 and s.id_state !=  168 and s.id_state !=  171 and s.id_state !=  173 and s.id_state !=  174 and s.id_state !=  176 and s.id_state !=  193 and s.id_state !=  194 and s.id_state !=  197 and s.id_state !=  198 and s.id_state !=  201 and s.id_state !=  206 and s.id_state !=  208 and s.id_state !=  213 and s.id_state !=  218 and s.id_state !=  232 and s.id_state != 123 and s.id_state !=  130 and s.id_state !=  131 and s.id_state !=  132 and s.id_state !=  134 and s.id_state !=  140 and s.id_state !=  141 and s.id_state !=  142 and s.id_state !=  143 and s.id_state !=  144 and s.id_state !=  145 and s.id_state !=  146 and s.id_state !=  147 and s.id_state !=  148 and s.id_state !=  150 and s.id_state !=  152 and s.id_state !=  154 and s.id_state !=  158 and s.id_state !=  165 and s.id_state !=  166 and s.id_state !=  169 and s.id_state !=  177 and s.id_state !=  178 and s.id_state !=  179 and s.id_state !=  183 and s.id_state !=  185 and s.id_state !=  186 and s.id_state !=  187 and s.id_state !=  188 and s.id_state !=  190 and s.id_state !=  195 and s.id_state !=  200 and s.id_state !=  202 and s.id_state !=  204 and s.id_state !=  210 and s.id_state !=  211 and s.id_state !=  214 and s.id_state !=  216 and s.id_state !=  217 and s.id_state !=  220 and s.id_state !=  230" : '')."
						
						".(Tools::getValue('tipo_cliente') == 'attivo' ? "AND c.id_customer IN (SELECT id_customer FROM orders WHERE date_add BETWEEN date_sub(curdate(),interval 365 day) AND  date_sub(curdate(),interval 0 day) GROUP BY id_customer HAVING(count(id_order)) > 1 AND HAVING(count(id_order)) < 3)" : '')."
						
						".(Tools::getValue('tipo_cliente') == 'fedele' ? "AND c.id_customer IN (SELECT id_customer FROM orders WHERE date_add BETWEEN date_sub(curdate(),interval 365 day) AND  date_sub(curdate(),interval 0 day) GROUP BY id_customer HAVING(count(id_order)) > 3)" : '')."
						
						".(Tools::getValue('tipo_cliente') == 'prospect' ? "AND c.id_customer NOT IN (SELECT id_customer FROM orders WHERE date_add BETWEEN date_sub(curdate(),interval 365 day) AND  date_sub(curdate(),interval 0 day)) AND c.id_customer IN (SELECT id_customer FROM cart  WHERE date_add BETWEEN date_sub(curdate(),interval 365 day) AND  date_sub(curdate(),interval 0 day))" : '')."
						
						".(Tools::getValue('tipo_cliente') == 'lead' ? "AND c.id_customer NOT IN (SELECT id_customer FROM orders WHERE date_add BETWEEN date_sub(curdate(),interval 365 day) AND  date_sub(curdate(),interval 0 day)) AND c.id_customer NOT IN (SELECT id_customer FROM cart  WHERE date_add BETWEEN date_sub(curdate(),interval 365 day) AND  date_sub(curdate(),interval 0 day)) AND (c.id_customer IN (SELECT id_customer FROM action_thread WHERE date_add BETWEEN date_sub(curdate(),interval 365 day) AND  date_sub(curdate(),interval 0 day)) OR c.id_customer IN (SELECT id_customer FROM customer_thread WHERE date_add BETWEEN date_sub(curdate(),interval 365 day) AND  date_sub(curdate(),interval 0 day)) OR c.id_customer IN (SELECT id_customer FROM form_prevendita_thread WHERE date_add BETWEEN date_sub(curdate(),interval 365 day) AND  date_sub(curdate(),interval 0 day)))" : '')."
						
						".(Tools::getValue('tipo_cliente') == 'inattivo' ? "AND c.id_customer NOT IN (SELECT id_customer FROM orders WHERE date_add BETWEEN date_sub(curdate(),interval 365 day) AND  date_sub(curdate(),interval 0 day)) " : '')."
						
						".(Tools::getValue('keyword') != '' ? "AND c.keyword LIKE '%".Tools::getValue('keyword')."%' " : '')."
						".($modalita == 2 ? 'AND fat.totale_acquisti IS NULL ' : '')."
						".(Tools::getValue('escludi_privati') == 1 ? 'AND c.is_company = 1' : '')."
						".(Tools::getValue('inattivita') > 0 ? 'AND DATE_FORMAT(ultimo.data_fattura, "%Y-%m-%d %H:%i") < DATE_FORMAT(NOW() - INTERVAL '.Tools::getValue('inattivita').' MONTH, "%Y-%m-%d %H:%i")' : '')."
						
						$gruppi
						$arco_dal
						$arco_al
						$where 
						
						
						".(($modalita == 1 || $modalita == 2) ? 'GROUP BY c.id_customer' : 'GROUP BY f.id_riga')."
						
						) cz GROUP BY cz.id_customer
						";
					
				}
			
				echo "<br />";
				
				echo "<form method='post' action=''>";
				/*	echo "<strong>Esportazione Dati</strong> - opzioni<br />";
				echo "<strong>IMPORTANTE: non spuntare NESSUNA CASELLA se vuoi esportare la lista completa che trovi qua sotto.</strong><br /><br />";
				echo "<input type='checkbox' name='esportaaziende' /> Esporta aziende<br />";
				echo "<input type='checkbox' name='esportaprivati' /> Esporta privati<br />---<br />";
				echo "<input type='checkbox' name='esportaclienti' /> Esporta clienti<br />";
				echo "<input type='checkbox' name='esportarivenditori' /> Esporta rivenditori<br />--<br />";
				
				echo "<input type='checkbox' name='escludimarketplace' /> Escludi clienti marketplace (Amazon, ePrice)<br /><br />";
				*/
				echo "<input type='submit' value='Esporta i dati in formato Excel' name='esportaexcel' class='button' />
				<input type='submit' value='Vedi solo le mail in formato TXT per invio' name='esportamailtxt' class='button' />
				<input type='submit' value='Esporta lista in formato CSV per Mailup' name='esportamailup' class='button' />
				<input type='hidden' name='querysql' value='".htmlentities((isset($_POST['querysql']) ? $_POST['querysql'] : $sql),ENT_QUOTES)."' />	
				<input type='hidden' name='query_from' value='".htmlentities((isset($_POST['query_from']) ? $_POST['query_from'] : $from),ENT_QUOTES)."' />	
				<input type='hidden' name='query_where' value='".htmlentities((isset($_POST['query_where']) ? $_POST['query_where'] : $where),ENT_QUOTES)."' />						
				<input type='hidden' name='prodottiinclusi' value='".$prodottiinclusi."' />		
				<input type='hidden' name='strisciaprodotti' value='".$strisciaprodotti."' />		
				<input type='hidden' name='categorieincluse' value='".$categorieincluse."' />	
				<input type='hidden' name='macrocategorieincluse' value='".$macrocategorieincluse."' />						
				<input type='hidden' name='strisciacat' value='".$strisciacat."' />	
				<input type='hidden' name='marchiinclusi' value='".$marchiinclusi."' />		
				<input type='hidden' name='strisciamarchi' value='".$strisciamarchi."' />	
				<input type='hidden' name='striscia_escludi_privati' value='".$striscia_escludi_privati."' />				
				<input type='hidden' name='striscia_arco_dal' value='".$striscia_arco_dal."' />	
				<input type='hidden' name='striscia_arco_al' value='".$striscia_arco_al."' />	
				<input type='hidden' name='strisciaprivati' value='".$strisciaprivati."' />	
				<input type='hidden' name='strisciainattivita' value='".$strisciainattivita."' />	
				<input type='hidden' name='strisciatipocliente' value='".$strisciatipocliente."' />	
				<input type='hidden' name='strisciazonacliente' value='".$strisciazonacliente."' />	
				<input type='hidden' name='strisciagruppi' value='".$strisciagruppi."' />	
				<input type='hidden' name='strisciaprevric' value='".$strisciaprevric."' />	
				<input type='hidden' name='strisciaprevriv' value='".$strisciaprevriv."' />	
				<input type='hidden' name='strisciaricric' value='".$strisciaricric."' />	
				";
				echo "<br /><br />";
				
				$sql.= $orderby;

				$resultsclienti = Db::getInstance()->ExecuteS(stripslashes($sql));
				
				$tot_acquisti = 0;
				$num = count($resultsclienti);
				
				foreach($resultsclienti as $r){
					
					if(!empty($data_dal) && !empty($data_al))
						$ordini_periodo_r = Db::getInstance()->getValue('
							SELECT 
								SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) AS tot 
							FROM order_detail 
							JOIN orders 
								ON orders.id_order = order_detail.id_order 
							LEFT JOIN order_history oh 
								ON oh.id_order_state = 6 AND oh.id_order = orders.id_order 
							WHERE oh.id_order_state IS NULL 
								AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment")))
								AND orders.id_customer = '.$r['id_customer'].'
								AND orders.date_add BETWEEN "'.$data_dal.'" AND "'.$data_al.'"
						');
					else if(!empty($data_dal) && empty($data_al))
						$ordini_periodo_r = Db::getInstance()->getValue('
							SELECT 
								SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) AS tot 
							FROM order_detail 
							JOIN orders 
								ON orders.id_order = order_detail.id_order 
							LEFT JOIN order_history oh 
								ON oh.id_order_state = 6 AND oh.id_order = orders.id_order 
							WHERE oh.id_order_state IS NULL 
								AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment")))
								AND orders.id_customer = '.$r['id_customer'].'
								AND orders.date_add > "'.$data_dal.'"
						');
					else if(empty($data_dal) && !empty($data_al))
						$ordini_periodo_r = Db::getInstance()->getValue('
							SELECT 
								SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) AS tot 
							FROM order_detail 
							JOIN orders 
								ON orders.id_order = order_detail.id_order 
							LEFT JOIN order_history oh 
								ON oh.id_order_state = 6 AND oh.id_order = orders.id_order 
							WHERE oh.id_order_state IS NULL 
								AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment")))
								AND orders.id_customer = '.$r['id_customer'].'
								AND orders.date_add < "'.$data_al.'"
						');
					else if(empty($data_dal) && empty($data_al))
						$ordini_periodo_r = Db::getInstance()->getValue('
							SELECT 
								SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) AS tot 
							FROM order_detail 
							JOIN orders 
								ON orders.id_order = order_detail.id_order 
							LEFT JOIN order_history oh 
								ON oh.id_order_state = 6 AND oh.id_order = orders.id_order 
							WHERE oh.id_order_state IS NULL 
								AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment")))
								AND orders.id_customer = '.$r['id_customer'].'
						');
						
					//$tot_acquisti += $r['totale_acquisti'];
					if(Tools::getValue('modalita') != 1 && Tools::getValue('modalita') != 2 && $ordini_periodo_r == 0)
						$num -= 1; // ESCLUDO I CLIENTI CHE NON HANNO COMPRATO NEL PERIODO
					else
						$tot_acquisti += $ordini_periodo_r;
				}
					
				echo '<script type="text/javascript" src="../js/jquery/jquery.tablesorter.min.js"></script> 
				<script type="text/javascript" src="../js/jquery/jquery.tablesorter.widgets.js"></script> 
				<script type="text/javascript" src="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.js"></script> 
				<link rel="stylesheet" type="text/css" href="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.css" />';
				
				echo '
				<script type="text/javascript">
				var myTextExtraction = function(node) 
					{ 
						// extract data from markup and return it 
						return node.textContent.replace( /<.*?>/g, "" ); 
					}
					
				$(document).ready(function() 
					{ 
					
				
					$.tablesorter.addParser({ 
						// set a unique id 
						id: "thousands",
						is: function(s) { 
							// return false so this parser is not auto detected 
							return false; 
						}, 
						format: function(s) {
							// format your data for normalization 
							return s.replace(/[\s.$£€]/g,"").replace(/,/g,".");
						}, 
						// set type, either numeric or text 
						type: "numeric" 
					}); 


						$("#table-clienti").tablesorter({  headers: {
						0: {sorter: false}, 8: {sorter: "shortDate", dateFormat: "ddmmyyyy"},  9: {sorter: "shortDate", dateFormat: "ddmmyyyy"}, 6: {	sorter:"thousands"	}, 7: {	sorter:"thousands"	}
				}, widthFixed: true, widgets: ["zebra", "stickyHeaders"]});
						
					} 
				);
				</script>';
				
				/* Colonne tabella */
			
				echo "<input type='hidden' name='num' value='".$num."' />		
				<input type='hidden' name='tot_acquisti' value='".$tot_acquisti."' />";	
				echo "<table class='table'><tr><td>Totale clienti trovati</td><td><strong>$num</strong></td></tr>";
				echo "<tr><td>Totale acquisti nel periodo</td><td><strong>".Tools::displayPrice($tot_acquisti,1)."</strong></td></tr></table><br />";
				echo "<div id='table-container'><table class='table tablesorter' id='table-clienti'><thead>";
				echo "<tr>";
				echo "<th style='width:30px'>N.</th>";
				echo "<th style='width:45px'>Id cliente 
				</th>";
				echo "<th style='width:150px'>Cliente
				</th>";
				echo "<th style='width:50px'>Tipo
				</th>";
				echo "<th style='width:50px'>Gruppo
				</th>";
			
				echo "<th style='width:30px'>Provincia
				</th>";
				
				echo "<th style='width:80px'>Acq. nel periodo 
				</th>
				";
				
				echo "<th style='width:80px'>Totale acq. 
				</th>
				";
				
				echo "<th style='width:80px'>Totale acq. ".$anno_corrente."
				</th>
				";
				
				echo "<th style='width:80px'>Data primo acquisto 
				
				</th>";
				
				echo "<th style='width:80px'>Data ultimo acquisto</th>
				
				";
				
				echo "<th>Mesi inattivit&agrave;</th>";
				
				echo "<th>Vai</th>";
				echo "</tr></thead><tbody>";
				
				$i = 1;
				
				foreach ($resultsclienti as $row) {
					
					
					if($row['is_company'] == 1) {
						$tipo = 'A';
					}
					else {
						$tipo = 'P';
					}
					
					
					$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee));
					
					/* I valori da qui in poi non si basano più sul fatturato */
					
					/* Totale acquisti nel periodo */
					if(!empty($data_dal) && !empty($data_al))
						$ordini_periodo = Db::getInstance()->getValue('
							SELECT 
								SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) AS tot 
							FROM order_detail 
							JOIN orders 
								ON orders.id_order = order_detail.id_order 
							LEFT JOIN order_history oh 
								ON oh.id_order_state = 6 AND oh.id_order = orders.id_order 
							WHERE oh.id_order_state IS NULL 
								AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment")))
								AND orders.id_customer = '.$row['id_customer'].'
								AND orders.date_add BETWEEN "'.$data_dal.'" AND "'.$data_al.'"
						');
					else if(!empty($data_dal) && empty($data_al))
						$ordini_periodo = Db::getInstance()->getValue('
							SELECT 
								SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) AS tot 
							FROM order_detail 
							JOIN orders 
								ON orders.id_order = order_detail.id_order 
							LEFT JOIN order_history oh 
								ON oh.id_order_state = 6 AND oh.id_order = orders.id_order 
							WHERE oh.id_order_state IS NULL 
								AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment")))
								AND orders.id_customer = '.$row['id_customer'].'
								AND orders.date_add > "'.$data_dal.'"
						');
					else if(empty($data_dal) && !empty($data_al))
						$ordini_periodo = Db::getInstance()->getValue('
							SELECT 
								SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) AS tot 
							FROM order_detail 
							JOIN orders 
								ON orders.id_order = order_detail.id_order 
							LEFT JOIN order_history oh 
								ON oh.id_order_state = 6 AND oh.id_order = orders.id_order 
							WHERE oh.id_order_state IS NULL 
								AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment")))
								AND orders.id_customer = '.$row['id_customer'].'
								AND orders.date_add < "'.$data_al.'"
						');
					else if(empty($data_dal) && empty($data_al))
						$ordini_periodo = Db::getInstance()->getValue('
							SELECT 
								SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) AS tot 
							FROM order_detail 
							JOIN orders 
								ON orders.id_order = order_detail.id_order 
							LEFT JOIN order_history oh 
								ON oh.id_order_state = 6 AND oh.id_order = orders.id_order 
							WHERE oh.id_order_state IS NULL
								AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment")))
								AND orders.id_customer = '.$row['id_customer'].'
						');
					
					/* Sostituisce $row['totale_acquisti'] */
					$ordini_totali = Db::getInstance()->getValue('
						SELECT 
							SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) AS tot
						FROM order_detail 
						JOIN orders 
							ON orders.id_order = order_detail.id_order 
						LEFT JOIN order_history oh 
							ON oh.id_order_state = 6 AND oh.id_order = orders.id_order 
						WHERE oh.id_order_state IS NULL 
							AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment")))
							AND orders.id_customer = '.$row['id_customer'].'
					');
					
					/* Sostituisce $row['ultimo_anno'] */
					$ordini_ultimo_anno = Db::getInstance()->getValue('
						SELECT 
							SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) AS tot 
						FROM order_detail 
						JOIN orders 
							ON orders.id_order = order_detail.id_order 
						LEFT JOIN order_history oh 
							ON oh.id_order_state = 6 AND oh.id_order = orders.id_order 
						WHERE oh.id_order_state IS NULL 
							AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment")))
							AND orders.id_customer = '.$row['id_customer'].'
							AND YEAR(orders.date_add) = '.$anno_corrente.'
					');
					
					/* Sostituisce $row['primo_acquisto'] */
					$data_primo = Db::getInstance()->getValue('
						SELECT 
							orders.date_add AS primo
						FROM order_detail 
						JOIN orders 
							ON orders.id_order = order_detail.id_order 
						LEFT JOIN order_history oh 
							ON oh.id_order_state = 6 AND oh.id_order = orders.id_order 
						WHERE oh.id_order_state IS NULL 
							AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment")))
							AND orders.id_customer = '.$row['id_customer'].'
						ORDER BY orders.date_add ASC
					');
					
					/* Sostituisce $row['ultimo_acquisto'] */
					$data_ultimo = Db::getInstance()->getValue('
						SELECT 
							orders.date_add AS ultimo
						FROM order_detail 
						JOIN orders 
							ON orders.id_order = order_detail.id_order 
						LEFT JOIN order_history oh 
							ON oh.id_order_state = 6 AND oh.id_order = orders.id_order 
						WHERE oh.id_order_state IS NULL 
							AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment")))
							AND orders.id_customer = '.$row['id_customer'].'
						ORDER BY orders.date_add DESC
					');
					
					$mesi_inattivita = $this->datediff("M", $data_ultimo, date("Y-m-d H:i:s"));
					
					
					/* Righe tabella */
					
					if(Tools::getValue('modalita') != 1 && Tools::getValue('modalita') != 2){
						if($ordini_totali != 0){ 
							// STAMPO SOLO CHI HA FATTO ACQUISTI
							echo "<tr>";
							echo "<td style='width:30px'>".$i."</td>";
							echo "<td>".$row['id_customer']."</td>";
							echo "<td>".$row['cliente']."</td>";
							echo "<td>".$tipo."</td>";
							echo "<td>".$row['gruppo']."</td>";
							echo "<td style='width:30px'>".$row['iso_code']."</td>";
							
							echo "<td style='text-align:right'>".Tools::displayPrice($ordini_periodo,1)."</td>";
							
							//echo "<td style='text-align:right'>".Tools::displayPrice($row['totale_acquisti'],1)."</td>";
							echo "<td style='text-align:right'>".Tools::displayPrice($ordini_totali,1)."</td>";

							// echo "<td style='text-align:right'>".Tools::displayPrice($row['ultimo_anno'],1)."</td>";
							echo "<td style='text-align:right'>".Tools::displayPrice($ordini_ultimo_anno,1)."</td>";
							
							//echo "<td>".($row['primo_acquisto'] ? date('d/m/Y', strtotime($row['primo_acquisto'])) : '--')."</td>";
							//echo "<td>".($row['ultimo_acquisto'] ? date('d/m/Y', strtotime($row['ultimo_acquisto'])) : '--')."</td>";
							echo "<td>".($data_primo ? date('d/m/Y', strtotime($data_primo)) : '--')."</td>";
							echo "<td>".($data_ultimo ? date('d/m/Y', strtotime($data_ultimo)) : '--')."</td>";
							
							echo "<td>".$mesi_inattivita."</td>";
							
							echo "<td><a href='index.php?tab=AdminCustomers&id_customer=".$row['id_customer']."&viewcustomer&token=".$tokenCustomers."&tab-container-1=1' target='_blank'><img src='../img/admin/details.gif' alt='Apri cliente' title='Apri cliente' /></a></td>";

							echo "</tr>";
							
							$i++;
						}
					}
					else if(Tools::getValue('modalita') == 1){
						// STAMPO TUTTO
						echo "<tr>";
						echo "<td style='width:30px'>".$i."</td>";
						echo "<td>".$row['id_customer']."</td>";
						echo "<td>".$row['cliente']."</td>";
						echo "<td>".$tipo."</td>";
						echo "<td>".$row['gruppo']."</td>";
						echo "<td style='width:30px'>".$row['iso_code']."</td>";
						
						echo "<td style='text-align:right'>".Tools::displayPrice($ordini_periodo,1)."</td>";
						
						//echo "<td style='text-align:right'>".Tools::displayPrice($row['totale_acquisti'],1)."</td>";
						echo "<td style='text-align:right'>".Tools::displayPrice($ordini_totali,1)."</td>";

						// echo "<td style='text-align:right'>".Tools::displayPrice($row['ultimo_anno'],1)."</td>";
						echo "<td style='text-align:right'>".Tools::displayPrice($ordini_ultimo_anno,1)."</td>";
						
						//echo "<td>".($row['primo_acquisto'] ? date('d/m/Y', strtotime($row['primo_acquisto'])) : '--')."</td>";
						//echo "<td>".($row['ultimo_acquisto'] ? date('d/m/Y', strtotime($row['ultimo_acquisto'])) : '--')."</td>";
						echo "<td>".($data_primo ? date('d/m/Y', strtotime($data_primo)) : '--')."</td>";
						echo "<td>".($data_ultimo ? date('d/m/Y', strtotime($data_ultimo)) : '--')."</td>";
						
						echo "<td>".$mesi_inattivita."</td>";
						
						echo "<td><a href='index.php?tab=AdminCustomers&id_customer=".$row['id_customer']."&viewcustomer&token=".$tokenCustomers."&tab-container-1=1' target='_blank'><img src='../img/admin/details.gif' alt='Apri cliente' title='Apri cliente' /></a></td>";

						echo "</tr>";
						
						$i++;
					}
					else if(Tools::getValue('modalita') == 2){
						if($ordini_totali == 0){ 
							// STAMPO SOLO CHI NON HA MAI FATTO ACQUISTI
							echo "<tr>";
							echo "<td style='width:30px'>".$i."</td>";
							echo "<td>".$row['id_customer']."</td>";
							echo "<td>".$row['cliente']."</td>";
							echo "<td>".$tipo."</td>";
							echo "<td>".$row['gruppo']."</td>";
							echo "<td style='width:30px'>".$row['iso_code']."</td>";
							
							echo "<td style='text-align:right'>".Tools::displayPrice($ordini_periodo,1)."</td>";
							
							//echo "<td style='text-align:right'>".Tools::displayPrice($row['totale_acquisti'],1)."</td>";
							echo "<td style='text-align:right'>".Tools::displayPrice($ordini_totali,1)."</td>";

							// echo "<td style='text-align:right'>".Tools::displayPrice($row['ultimo_anno'],1)."</td>";
							echo "<td style='text-align:right'>".Tools::displayPrice($ordini_ultimo_anno,1)."</td>";
							
							//echo "<td>".($row['primo_acquisto'] ? date('d/m/Y', strtotime($row['primo_acquisto'])) : '--')."</td>";
							//echo "<td>".($row['ultimo_acquisto'] ? date('d/m/Y', strtotime($row['ultimo_acquisto'])) : '--')."</td>";
							echo "<td>".($data_primo ? date('d/m/Y', strtotime($data_primo)) : '--')."</td>";
							echo "<td>".($data_ultimo ? date('d/m/Y', strtotime($data_ultimo)) : '--')."</td>";
							
							echo "<td>".$mesi_inattivita."</td>";
							
							echo "<td><a href='index.php?tab=AdminCustomers&id_customer=".$row['id_customer']."&viewcustomer&token=".$tokenCustomers."&tab-container-1=1' target='_blank'><img src='../img/admin/details.gif' alt='Apri cliente' title='Apri cliente' /></a></td>";

							echo "</tr>";
							
							$i++;
						}
					}

				
				}
				
				echo "</tbody></table></div><div id='bottom_anchor'>
				</div>";
				echo "</form>";
			}
			else if(isset($_POST['esportamailup'])) { 
				
				$resultsclienti = Db::getInstance()->ExecuteS(html_entity_decode($_POST['querysql']));
				$mails = '';
				foreach ($resultsclienti as $row) {
					if(isset($_POST['esportaaziende']) && $row['is_company'] == 0) {
					}
					else if(isset($_POST['esportaprivati']) && $row['is_company'] == 1) {
					}
					else if(isset($_POST['esportaclienti']) && ($row['id_default_group'] == 3 || $row['id_default_group'] != 15)) {
					}
					else if(isset($_POST['esportarivenditori']) && ($row['id_default_group'] != 3 && $row['id_default_group'] != 15)) {
					}
					else {
					
						if(isset($_POST['escludimarketplace']) && ($row['id_default_group'] == 16 || $row['id_default_group'] == 17)) {
						}
						else
						{
							$mails.= $row['nome_mailup'].";".$row['cognome_mailup'].";".$row['azienda_mailup'].";".$row['email_mailup'].";".$row['indirizzo_mailup'].";".$row['citta_mailup'].";".$row['cap_mailup'].";".$row['telefono_mailup'].";".$row['cellulare_mailup']."\n";
						}
					}	
				}
				
				$file_csv_mailup_stream=fopen("/var/www/vhosts/ezdirect.it/httpdocs/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-clienti-$data-mailup.csv","w+");
				@fwrite($file_csv_mailup_stream,$mails);
				@fclose("/var/www/vhosts/ezdirect.it/httpdocs/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-clienti-$data-mailup.csv");
		
				header("Location: http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-clienti-$data-mailup.csv");
			}
			else if(isset($_POST['esportamailtxt'])) { 
			
				$resultsclienti = Db::getInstance()->ExecuteS(html_entity_decode($_POST['querysql']));
				$mails = '';
				foreach ($resultsclienti as $row) {
					
					if(isset($_POST['esportaaziende']) && $row['is_company'] == 0) {
					}
					else if(isset($_POST['esportaprivati']) && $row['is_company'] == 1) {
					}
					else if(isset($_POST['esportaclienti']) && ($row['id_default_group'] == 3 || $row['id_default_group'] != 15)) {
					}
					else if(isset($_POST['esportamarketplace']) && ($row['id_default_group'] != 3 && $row['id_default_group'] != 15)) {
					}
					else {
					
						if(isset($_POST['escludirivenditori']) && ($row['id_default_group'] == 16 || $row['id_default_group'] == 17)) {
						}
						else
						{
							$mails.= $row['email']."; ";
						}
					}
				}
		
				echo $mails;
				
				/*$filename = "mail-clienti.txt";
				
				$newf = fopen($filename, 'w');
				fwrite($newf, $mails);
				fclose($newf);
				header("Content-Description: File Transfer");
				header("Content-Length: ". filesize("$filename").";");
				header("Content-Disposition: attachment; filename=$filename");
				header("Content-Type: application/octet-stream; "); 
				header("Content-Transfer-Encoding: binary");
				readfile($filename);*/

			}
			else if(isset($_POST['esportaexcel'])) { 
				ini_set("memory_limit","892M");
				set_time_limit(3600);
				require_once 'esportazione-catalogo/Classes/PHPExcel.php';
				$objPHPExcel = new PHPExcel();
				
				/*if(!empty($_POST['arco_dal'])) {
					$data_dal_a = explode("-", $_POST['arco_dal']);
					$data_dal = $data_dal_a[2]."-".$data_dal_a[1]."-".$data_dal_a[0]; 
				}

				if(!empty($_POST['arco_al'])) {	
					$data_al_a = explode("-", $_POST['arco_al']);
					$data_al = $data_al_a[2]."-".$data_al_a[1]."-".$data_al_a[0]; 
				}*/

				if(isset($_POST['querysql']))
					$sql = $_POST['querysql'];
				//else
					//$sql = '';
				
				$sql.= $orderby;

				$resultsclienti = Db::getInstance()->ExecuteS(stripslashes($sql));
				
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'Filtri attivati: '.strip_tags($_POST['strisciaprivati']).' / '.$_POST['strisciagruppi'].' / '.($_POST['strisciainattivita'] != '' ? $_POST['strisciainattivita'].' / ' : '').($_POST['strisciaprevric'] != '' ? $_POST['strisciaprevric'].' / ' : '').($_POST['strisciaprevriv'] != '' ? $_POST['strisciaprevriv'].' / ' : '').$_POST['strisciaricric'].'')
					->setCellValue('A2', 'Prodotti: '.$_POST['prodottiinclusi'].'')
					->setCellValue('A3', 'Categorie: '.$_POST['macrocategorieincluse'].' '.$_POST['categorieincluse'].'')
					->setCellValue('A4', 'Marchi: '.$_POST['marchiinclusi'].'')
					->setCellValue('A5', 'Periodo dal '.$data_dal.' al '.$data_al.'. Data esportazione: '.date('d/m/Y H:i:s').'')
					->setCellValue('B5', $_POST['strisciamodalita'])
					->setCellValue('A6', 'N.')
					->setCellValue('B6', 'Id cliente')
					->setCellValue('C6', 'Nome')
					->setCellValue('D6', 'Cognome')
					->setCellValue('E6', 'Azienda')
					->setCellValue('F6', 'Tipo')
					->setCellValue('G6', 'Gruppo')
					->setCellValue('H6', 'Email')
					->setCellValue('I6', 'Telefono')
					->setCellValue('J6', 'Cellulare')
					->setCellValue('K6', 'Fax')
					->setCellValue('L6', 'Provincia')
					->setCellValue('M6', 'Acq. nel periodo')
					->setCellValue('N6', 'Tot. acq. in euro')
					->setCellValue('O6', 'Tot. acq. '.$anno_corrente.'')
					->setCellValue('P6', 'Data primo acquisto')
					->setCellValue('Q6', 'Data ultimo acquisto')
					->setCellValue('R6', 'Mesi inattivita')
					;
				$k = 7;
				$i = 1;
				
				$objPHPExcel->getActiveSheet()->getStyle("I")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("J")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("K")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);


				foreach ($resultsclienti as $row) {
					
					/* Totale acquisti nel periodo */
					$ordini_periodo = Db::getInstance()->getValue('
						SELECT 
							SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) AS tot 
						FROM order_detail 
						JOIN orders 
							ON orders.id_order = order_detail.id_order 
						LEFT JOIN order_history oh 
							ON oh.id_order_state = 6 AND oh.id_order = orders.id_order 
						WHERE oh.id_order_state IS NULL 
							AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment")))
							AND orders.id_customer = '.$row['id_customer'].'
							AND orders.date_add BETWEEN "'.$data_dal.'" AND "'.$data_al.'"
					');
					
					/* Sostituisce $row['totale_acquisti'] */
					$ordini_totali = Db::getInstance()->getValue('
						SELECT 
							SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) AS tot
						FROM order_detail 
						JOIN orders 
							ON orders.id_order = order_detail.id_order 
						LEFT JOIN order_history oh 
							ON oh.id_order_state = 6 AND oh.id_order = orders.id_order 
						WHERE oh.id_order_state IS NULL 
							AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment")))
							AND orders.id_customer = '.$row['id_customer'].'
					');
					
					/* Sostituisce $row['ultimo_anno'] */
					$ordini_ultimo_anno = Db::getInstance()->getValue('
						SELECT 
							SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) AS tot 
						FROM order_detail 
						JOIN orders 
							ON orders.id_order = order_detail.id_order 
						LEFT JOIN order_history oh 
							ON oh.id_order_state = 6 AND oh.id_order = orders.id_order 
						WHERE oh.id_order_state IS NULL 
							AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment")))
							AND orders.id_customer = '.$row['id_customer'].'
							AND YEAR(orders.date_add) = '.$anno_corrente.'
					');
					
					/* Sostituisce $row['primo_acquisto'] */
					$data_primo = Db::getInstance()->getValue('
						SELECT 
							orders.date_add AS primo
						FROM order_detail 
						JOIN orders 
							ON orders.id_order = order_detail.id_order 
						LEFT JOIN order_history oh 
							ON oh.id_order_state = 6 AND oh.id_order = orders.id_order 
						WHERE oh.id_order_state IS NULL 
							AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment")))
							AND orders.id_customer = '.$row['id_customer'].'
						ORDER BY orders.date_add ASC
					');
					
					/* Sostituisce $row['ultimo_acquisto'] */
					$data_ultimo = Db::getInstance()->getValue('
						SELECT 
							orders.date_add AS ultimo
						FROM order_detail 
						JOIN orders 
							ON orders.id_order = order_detail.id_order 
						LEFT JOIN order_history oh 
							ON oh.id_order_state = 6 AND oh.id_order = orders.id_order 
						WHERE oh.id_order_state IS NULL 
							AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment")))
							AND orders.id_customer = '.$row['id_customer'].'
						ORDER BY orders.date_add DESC
					');
					
					$mesi_inattivita = $this->datediff("M", $data_ultimo, date("Y-m-d H:i:s"));
					
					if($row['is_company'] == 1) {
						$tipo = 'Azienda';
					}
					else {
						$tipo = 'Privato';
					}
						
					
					if(isset($_POST['esportaaziende']) && $row['is_company'] == 0) {
					}
					else if(isset($_POST['esportaprivati']) && $row['is_company'] == 1) {
					}
					else if(isset($_POST['esportaclienti']) && ($row['id_default_group'] == 3 || $row['id_default_group'] != 15)) {
					}
					else if(isset($_POST['esportarivenditori']) && ($row['id_default_group'] != 3 && $row['id_default_group'] != 15)) {
					}
					else {
					
						if(isset($_POST['escludimarketplace']) && ($row['id_default_group'] == 16 || $row['id_default_group'] == 17)) {
						}
						else
						{
							//$tot_anno[$key] = $ultimo_anno;
						
							$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue("A$k", $i)
							->setCellValue("B$k", $row['id_customer'])
							->setCellValue("C$k", $row['firstname'])
							->setCellValue("D$k", $row['lastname'])
							->setCellValue("E$k", $row['company'])
							->setCellValue("F$k", $tipo)
							->setCellValue("G$k", $row['gruppo'])
							->setCellValue("H$k", $row['email'])
							->setCellValue("I$k", $row['phone'])
							->setCellValue("J$k", $row['phome_mobile'])
							->setCellValue("K$k", $row['fax'])
							->setCellValue("L$k", $row['iso_code'])
							->setCellValue("M$k", str_replace(".",",",round($ordini_periodo,2)))
							->setCellValue("N$k", str_replace(".",",",round($ordini_totali,2)))
							->setCellValue("O$k", str_replace(".",",",round($ordini_ultimo_anno,2)))
							->setCellValue("P$k", date('d/m/Y', strtotime($data_primo)))
							->setCellValue("Q$k", date('d/m/Y', strtotime($data_ultimo)))
							->setCellValue("R$k", $mesi_inattivita)
							;
						
							$objPHPExcel->getActiveSheet()->getCell("I$k")->setValueExplicit($row['phone'], PHPExcel_Cell_DataType::TYPE_STRING);
							$objPHPExcel->getActiveSheet()->getCell("J$k")->setValueExplicit($row['phone_mobile'], PHPExcel_Cell_DataType::TYPE_STRING);
							$objPHPExcel->getActiveSheet()->getCell("K$k")->setValueExplicit($row['fax'], PHPExcel_Cell_DataType::TYPE_STRING);
							$objPHPExcel->getActiveSheet()->getStyle("M$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
							$objPHPExcel->getActiveSheet()->getStyle("N$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
							$objPHPExcel->getActiveSheet()->getStyle("O$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
							$i++;
							$k++;
						}
					}
				}
				
				$objPHPExcel->getActiveSheet()->setTitle('Clienti');

				$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	

				$objPHPExcel->getActiveSheet()->getStyle('A6:R6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
					
				$objPHPExcel->getActiveSheet()->getStyle('A6:R6')->getFill()->getStartColor()->setRGB('FFFF00');	

				$objPHPExcel->getActiveSheet()->getStyle("A6:R$k")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('A6:R6')->getFont()->setBold(true);

				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
				
				
				// Set active sheet index to the first sheet, so Excel opens this as the first sheet
				$objPHPExcel->setActiveSheetIndex(0);

				$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
				$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
				PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

				$data = date("Ymd");

				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->setPreCalculateFormulas(false);
				$objWriter->save(str_replace(".php", ".xls", "esportazione-catalogo/catalogo_xls/prodotti-clienti-$data.php"));

				$this->_html .= "<div style='visibility:hidden'><br /><br />FILE PRONTO PER IL DOWNLOAD: <a id='scaricaexcelstatistiche' href='http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-clienti-$data.xls'>CLICCA QUI PER SCARICARE</a>!</div>";

				header("Location: http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-clienti-$data.xls");
					
				
				
			
			}
			else {
			
				echo '<script type="text/javascript" src="../js/select2.js"></script>
				<script type="text/javascript">
				$(document).ready(function() { $("#per_prodotto").select2(); });
				
				$(document).ready(function() { $("#per_categoria").select2(); });
				
				$(document).ready(function() { $("#per_macrocategoria").select2(); });
				
				$(document).ready(function() { $("#per_marchio").select2(); });
				
				$(document).ready(function() {
						$(".hint_tooltip").tooltip({ showURL: false  });
					});
				</script>
				
				<strong>Filtri di selezione</strong><br /><br />
				
				<form name="cercaclienti" method="post" action="">
				<table cellpadding="9">
				
				<tr><td>Prodotto/i acquistato/i</td><td>
				<select multiple id="per_prodotto" name="per_prodotto[]" style="width:600px">
				<option name="0" value="0">--- Scegli un prodotto ---</option>
				';
				
				
				$results = Db::getInstance()->ExecuteS("SELECT cod_articolo, desc_articolo FROM fattura WHERE cod_articolo != '' GROUP BY cod_articolo ORDER BY cod_articolo");
				
				foreach ($results as $row) {
				
					echo "<option name='$row[cod_articolo]' value='$row[cod_articolo]'"; 
					echo ">$row[cod_articolo] - $row[desc_articolo]</option>";
				
				}
					
				echo "</select></td><td><span class='hint_tooltip' style=\"cursor:pointer;\" title=\"Per selezionare clienti che hanno acquistato uno o più prodotti precisi. Esempio: tutti i clienti che hanno acquistato la cuffia Ez Mono e la cuffia Ez Duo.\" ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>";
				
				echo "<tr><td>Quantit&agrave;</td><td>
				<input type='text' value='' name='quantita' /> </td><td></td></tr>
				";
				
				echo "<tr><td>Importo</td><td>
				<input type='text' value='' name='importo' /> </td><td></td></tr>
				";
				
				echo "<tr><td>Nel periodo</td><td>";
				
				include_once('functions.php');
			
				$this->includeDatepickerZ(array('arco_dal', 'arco_al'), true);
				
				echo 'Dal <input type="text" id="arco_dal" name="arco_dal"  /> al <input type="text" id="arco_al" name="arco_al"  /></td><td><span class="hint_tooltip" style="cursor:pointer;" title="Modalità acquisti: si considera la data di acquisto. Modalità no acquisto o modalità tutti: si considera la data di registrazione dell\'account." ><img src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Aiuto" /></span></td></tr>';
				
				
				
				echo '<tr><td>Macrocategoria/e acquistata/e</td><td>
				<select multiple id="per_macrocategoria" name="per_macrocategoria[]" style="width:600px">
				<option name="0" value="0">--- Scegli una macrocategoria ---</option>
				';
				
				
				$resultscat = Db::getInstance()->ExecuteS("SELECT category.id_category, category_lang.name FROM category JOIN category_lang ON category.id_category = category_lang.id_category WHERE category_lang.id_lang = 5 AND category.id_parent = 1 ORDER BY name");
				
				foreach ($resultscat as $rowcat) {
				
					echo "<option name='$rowcat[id_category]' value='$rowcat[id_category]'"; 
					echo ">$rowcat[id_category] - $rowcat[name]</option>";
				
				}
					
				echo "</select></td><td><span class='hint_tooltip' style=\"cursor:pointer;\" title=\"Per selezionare clienti che hanno acquistato in una o più macrocategorie. Esempio: clienti che hanno acquistato cuffie e centralini.\" ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>";
				
				echo '<tr><td>Sottocategoria/e acquistata/e</td><td>
				<select multiple id="per_categoria" name="per_categoria[]" style="width:600px">
				<option name="0" value="0">--- Scegli una categoria ---</option>
				';
				
				
				$resultscat = Db::getInstance()->ExecuteS("SELECT category.id_category, category_lang.name FROM category JOIN category_lang ON category.id_category = category_lang.id_category WHERE category_lang.id_lang = 5 AND category.id_parent != 1 ORDER BY name");
				
				foreach ($resultscat as $rowcat) {
				
					echo "<option name='$rowcat[id_category]' value='$rowcat[id_category]'"; 
					echo ">$rowcat[id_category] - $rowcat[name]</option>";
				
				}
				
				echo "</select></td><td><span class='hint_tooltip' style=\"cursor:pointer;\" title=\"Per selezionare clienti che hanno acquistato in una o più sottocategorie. Esempio: clienti che hanno acquistato cuffie per ufficio e cuffie per call center.\" ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>";
				
				
				
				echo '<tr><td>Marca/marche acquistata/e</td><td>
				<select multiple id="per_marchio" name="per_marchio[]" style="width:600px">
				<option name="0" value="0">--- Scegli un marchio ---</option>
				';
				
				
				$resultsman = Db::getInstance()->ExecuteS("SELECT id_manufacturer, name FROM manufacturer ORDER BY name");
				
				foreach ($resultsman as $rowman) {
				
					echo "<option name='$rowman[id_manufacturer]' value='$rowman[id_manufacturer]'"; 
					echo ">$rowman[id_manufacturer] - $rowman[name]</option>";
				
				}
					
				echo "</select></td><td><span class='hint_tooltip' style=\"cursor:pointer;\" title=\"Per cercare clienti che hanno acquistato marche precise. Esempio: clienti che hanno acquistato Ezdirect, Jabra e Plantronics\" ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>";
				
				echo "<tr><td>Privati?</td><td>
				<input type='radio' value='0' name='escludi_privati' checked='checked' /> Includi privati &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				<input type='radio' value='1' name='escludi_privati' /> Escludi privati</td><td></td></tr>
				";
				
				echo "<tr><td>Profilo cliente</td><td>";
				
				$gruppi_clienti = Db::getInstance()->executeS('SELECT id_group, name FROM group_lang WHERE id_lang = 5 AND (id_group != 11 AND id_group != 14 AND id_group != 4)');
				
				foreach($gruppi_clienti as $g)
					echo "<input type='checkbox' value='0' class='gruppi' name='group[".$g['id_group']."]' /> ".$g['name']." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ";
				
				echo " - <a href='javascript:void(0)' onclick='$(\".gruppi\").attr(\"checked\", true);'>Clicca per selezionarli tutti</a></td><td></td></tr>
				";
				
				echo "<tr><td>Inattivit&agrave;</td><td>
				<select name='inattivita'>
				<option value='0'> -- </option>
				<option value='6'> 6 mesi </option>
				<option value='12'> 12 mesi </option>
				<option value='18'> 18 mesi </option>
				<option value='24'> 24 mesi </option>
				<option value='48'> 48 mesi </option>
				<option value='60'> 60 mesi </option>
				<option value='60p'> Più di 60 mesi </option>
				</select>
				</td><td><span class='hint_tooltip' style=\"cursor:pointer;\" title=\"Per selezionare clienti che non acquistano dal periodo scelto col menù a tendina. Lasciare vuoto (--) per ignorare questo parametro. \" ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				";
				
				echo "<tr><td>Tipo cliente</td><td>
				<select name='tipo_cliente'>
				<option value=''> -- </option>
				<option value='attivo'> Attivo </option>
				<option value='fedele'> Fedele </option>
				<option value='lead'> Lead </option>
				<option value='prospect'> Prospect </option>
				<option value='inattivo'> Inattivo </option>
				</select>
				</td><td><span class='hint_tooltip' style=\"cursor:pointer;\" title=\"ATTIVO: Almeno un acquisto nell\'anno<br />FEDELE: almeno tre acquisti nell\'anno<br />PROSPECT: non ha ancora acquistato quest\'anno, ma ha un preventivo o un carrello<br />LEAD: non ha acquistato né chiesto preventivi quest\'anno, ma ci ha contattato o ci sono attivit&agrave; in corso<br />INATTIVO: non acquista da almeno un anno\" ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				";
				
				echo "<tr><td>Zona</td><td>
				<select name='zona_cliente'>
				<option value=''> -- </option>
				<option value='nord'> Nord </option>
				<option value='centro'> Centro </option>
				<option value='sud'> Sud </option>
				<option value='nd'> Estero / Non disponibile </option>
				</select>
				</td><td><span class='hint_tooltip' style=\"cursor:pointer;\" title=\"ATTIVO: Almeno un acquisto nell\'anno<br />FEDELE: almeno tre acquisti nell\'anno<br />PROSPECT: non ha ancora acquistato quest\'anno, ma ha un preventivo o un carrello<br />LEAD: non ha acquistato né chiesto preventivi quest\'anno, ma ci ha contattato o ci sono attivit&agrave; in corso<br />INATTIVO: non acquista da almeno un anno\" ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				";
				
				
				echo "<tr><td>Preventivo?</td><td>
				<input type='checkbox' value='0' name='prev_richiesto' /> Richiesto &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				<input type='checkbox' value='0' name='prev_ricevuto' /> Ricevuto</td><td><span class='hint_tooltip' style=\"cursor:pointer;\" title=\"Per selezionare clienti che hanno richiesto o ricevuto un preventivo.\" ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				";
				
				echo "<tr><td>Ti richiamiamo noi</td><td>
				<input type='checkbox' value='0' name='ric_richiesto' /> Richiesto</td><td><span class='hint_tooltip' style=\"cursor:pointer;\" title=\"Per selezionare clienti che hanno chiesto di essere richiamati.\" ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				";
				
				echo "<tr><td>Clienti che hanno fatto 1+ acquisti</td><td>
				<input type='checkbox' value='0' name='1piuacquisti' /> </td><td></td></tr>
				";
				
				echo "<tr><td>Keyword</td><td>
				<input type='text' value='' name='keyword' /> </td><td></td></tr>
				";
				
				
				echo "<tr><td>Modalit&agrave;</td><td>
				<input type='radio' value='0' name='modalita' checked='checked' onclick=\"$('.select2-container').show();\"/> Solo clienti che hanno fatto acquisti &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				
				<input type='radio' value='2' name='modalita' onclick=\" $('select').each(function () { $(this).select2('val', ''); }); $('.select2-container').hide(); $('#per_prodotto').hide(); $('#per_').hide(); $('#per_categoria').hide(); $('#per_marchio').hide();\" /> Solo clienti che NON hanno fatto acquisti  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				
				<input type='radio' value='1' name='modalita' onclick=\" $('select').each(function () { $(this).select2('val', ''); }); $('.select2-container').hide(); $('#per_prodotto').hide(); $('#per_').hide(); $('#per_categoria').hide(); $('#per_marchio').hide();\" /> Tutti</td><td><span class='hint_tooltip' style='cursor:pointer;' title='In modalità no acquisti e modalità tutti, verranno eliminati i filtri per prodotto, categoria e marchio perché non compatibili' ><img src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Aiuto' /></span></td></tr>
				";
				
				echo "<tr><td></td><td><input type='submit' name='cercaiclienti' value='Cerca clienti' class='button' /></td><td></td></tr>
				</table>
				</form>";
			
			}
		}

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	