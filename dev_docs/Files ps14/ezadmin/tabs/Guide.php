<?php

	include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

	class Guide extends AdminPreferences {


		public function display()
		{
			echo "<h1>Guide all'uso del portale</h1>";

			echo "<em>Fare clic sui singoli articoli per leggere le guide.</em><br /><br />";
			
			echo "<h2>CLIENTI</h2>";
			echo "<a href='guide/connessionecomecliente.pdf' target='_blank'><strong>Come connettersi come cliente (e fare azioni sul profilo del cliente)</strong></a><br />";
			echo "<a href='guide/registrareanagrafica.pdf' target='_blank'><strong>Come registrare una nuova anagrafica</strong></a><br />";
			echo "<a href='guide/modificareanagrafica.pdf' target='_blank'><strong>Come modificare l'anagrafica di un cliente</strong></a><br />";
			echo "<a href='guide/aggiungerenotaprivata.pdf' target='_blank'><strong>Come aggiungere una nota privata nell'anagrafica di un cliente</strong></a><br />";
			echo "<a href='guide/aggiungereindirizzocliente.pdf' target='_blank'><strong>Come aggiungere o modificare un indirizzo a un cliente</strong></a><br /><br />";
			
			echo "<h2>ORDINI, CARRELLI, OFFERTE</h2>";
			echo "<a href='guide/crearecarrello.pdf' target='_blank'><strong>Come creare un nuovo carrello per un cliente registrato</strong></a><br />";
			echo "<a href='guide/mostrarecarrelloalcliente.pdf' target='_blank'><strong>Come mostrare un carrello a un cliente registrato</strong></a><br />";
			//echo "<a href='guide/modificarecarrello.pdf' target='_blank'><strong>Come modificare un carrello di un cliente registrato</strong></a><br />";
			//echo "<a href='guide/cancellarecarrello.pdf' target='_blank'><strong>Come cancellare un carrello</strong></a><br />";
			//echo "<a href='guide/trasformazionecarrello.pdf' target='_blank'><strong>Come trasformare un carrello in un ordine</strong></a><br />";
			echo "<a href='guide/recuperocsv.pdf' target='_blank'><strong>Come recuperare il CSV di un ordine</strong></a><br />";
			//echo "<a href='guide/modificareordine.pdf' target='_blank'><strong>Come modificare un ordine</strong></a><br />";
			echo "<a href='guide/crearecoupon.pdf' target='_blank'><strong>Come creare un codice coupon</strong></a><br />";
			echo "<a href='guide/impostare-trasporto-gratuito.pdf' target='_blank'><strong>Come impostare trasporto gratis per ordini a partire da un certo totale</strong></a><br />";
			echo "<a href='guide/inserirecodicetracking.pdf' target='_blank'><strong>Come inserire il codice di tracking per la spedizione di un ordine</strong></a><br /><br />";

			echo "<h2>CATALOGO</h2>";
			echo "<a href='guide/come-aggiungere-un-filtro.pdf' target='_blank'><strong>Come aggiungere un filtro</strong></a><br />";
			echo "<a href='guide/aggiungereallegatosupiuprodotti.pdf' target='_blank'><strong>Come aggiungere un allegato su pi&ugrave; prodotti</strong></a><br /><br />";

			echo "<h2>ALTRO</h2>";
			echo "<a href='guide/bannerhome.pdf' target='_blank'><strong>Come creare, modificare ed eliminare i banner della home page</strong></a><br />";
			echo "<a href='guide/manuale-chat.pdf' target='_blank'><strong>Come utilizzare la chat di supporto per gli utenti</strong></a><br />";
			echo "<a href='guide/backupdatabase.pdf' target='_blank'><strong>Come fare il backup del database del sito</strong></a><br />";
			echo "<a href='guide/guidacms.jpg' target='_blank'><strong>Come scrivere e pubblicare un articolo CMS</strong></a><br /><br />";
			echo "<a href='guide/guidaamazon.pdf' target='_blank'><strong>Guida ad Amazon</strong></a><br /><br />";
			
		}

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	