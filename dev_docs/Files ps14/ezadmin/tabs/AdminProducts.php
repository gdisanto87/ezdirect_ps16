<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14703 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
if($cookie->id_employee == 6 || $cookie->id_employee == 22)
	ini_set('display_errors', 'on');

ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');

include_once(PS_ADMIN_DIR.'/tabs/AdminProfiles.php');

class AdminProducts extends AdminTab
{
	protected $maxImageSize = NULL;
	protected $maxFileSize  = NULL;

	private $_category;

	public function __construct()
	{
		global $currentIndex;

		
		$this->table = 'product';
		$this->className = 'Product';
		$this->lang = true;
		$this->edit = true;
	 	$this->delete = true;
		$this->view = false;
		$this->duplicate = true;
		$this->imageType = 'jpg';
		$this->maxImageSize = (Configuration::get('PS_LIMIT_UPLOAD_IMAGE_VALUE') * 1000000);
		$this->maxFileSize = (Configuration::get('PS_LIMIT_UPLOAD_FILE_VALUE') * 1000000);

		$this->fieldsDisplay = array(
			'id_product' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 40, 'widthColumn' => 40),
			'image' => array('title' => $this->l('Photo'), 'align' => 'center', 'image' => 'p', 'width' => 40, 'widthColumn' => 40, 'orderby' => false, 'filter' => false, 'search' => false),
			'name' => array('title' => $this->l('Name'), 'width' => 150, 'widthColumn' => 155, 'filter_key' => 'b!name'),
			'category' => array('title' => $this->l('Cat.'), 'width' => 100, 'widthColumn' => 120, 'filter_key' => 'category'),
			'reference' => array('title' => $this->l('Reference'), 'align' => 'left',  'widthColumn' => 100,  'width' => 20),
			'price' => array('title' => $this->l('Base price'), 'width' => 50, 'price' => true,  'widthColumn' => 55, 'align' => 'right', 'filter_key' => 'a!price'),
			'listino' => array('title' => $this->l('List price'), 'width' => 50, 'price' => true,  'widthColumn' => 55, 'align' => 'right', 'havingFilter' => true, 'orderby' => false),
			'wholesale_price' => array('title' => $this->l('Acq.'), 'width' => 50, 'price' => true,  'widthColumn' => 55, 'align' => 'right', 'havingFilter' => true, 'orderby' => false),
			'stock_quantity' => array('title' => $this->l(' EZ_'), 'width' => 10, 'align' => 'right',  'widthColumn' => 10, 'havingFilter' => true, 'orderby' => true),
			'ordinato_quantity' => array('title' => $this->l('OEZ'), 'width' => 10, 'align' => 'right',  'widthColumn' => 10, 'havingFilter' => true, 'orderby' => true),
			'supplier_quantity' => array('title' => $this->l(' AN_ '), 'width' => 10, 'align' => 'right',  'widthColumn' => 10, 'havingFilter' => true, 'orderby' => true),
			'arrivo_quantity' => array('title' => $this->l('AAN'), 'width' => 10, 'align' => 'right',  'widthColumn' => 10, 'havingFilter' => true, 'orderby' => true),
			//'date_add' => array('title' => $this->l('Added on'), 'align' => 'center', 'type' => 'datetime', 'width' => 20),
			//'date_upd' => array('title' => $this->l('Updated on'), 'align' => 'center', 'type' => 'datetime', 'width' => 20),
			'condition' => array('title' => $this->l('Cond.'), 'width' => 20, 'align' => 'right',  'widthColumn' => 10, 'havingFilter' => true, 'orderby' => true),
			'position' => array('title' => $this->l('Pos.'), 'width' => 20,'filter_key' => 'cp!position',  'widthColumn' => 20, 'align' => 'center', 'position' => 'position'),
			'a!active' => array('title' => $this->l('Stato'), 'active' => 'status', 'filter_key' => 'a!active',  'widthColumn' => 40, 'align' => 'center', 'type' => 'bool', 'orderby' => false));
		
		/* Join categories table */
		
		$this->_select = 'cp.`position`, i.`id_image`, (a.`price` * ((100 + (t.`rate`))/100)) AS price_final, cl.name AS category';
		
		$this->_category = AdminCatalog::getCurrentCategory();
		$this->_join = '
		LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = a.`id_product` AND i.`cover` = 1)
		LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON (cp.`id_product` = a.`id_product`)
		LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (cl.`id_category` = a.`id_category_default`)
		LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (a.`id_tax_rules_group` = tr.`id_tax_rules_group` AND tr.`id_country` = '.(int)Country::getDefaultCountryId().' AND tr.`id_state` = 0)
		LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)';
		
		$this->_where = ' AND a.active != 2 ';
		
		if($_GET['id_category'] == 9999999999) { 
		$this->_filter = 'AND cp.`id_category` > 0';
		}
		else {
		$this->_filter = 'AND cp.`id_category` = '.(int)($this->_category->id);
		}
		
		
		if($_GET['id_category'] == 9999999999) { 
		$this->_group = ' GROUP BY a.id_product';
		}
		
		$this->_group = ' GROUP BY a.id_product';
		
		parent::__construct();
	}

	private function _cleanMetaKeywords($keywords)
	{
		if (!empty($keywords) && $keywords != '')
		{
			$out = array();
			$words = explode(',', $keywords);
			foreach($words as $word_item)
			{
				$word_item = trim($word_item);
				if (!empty($word_item) && $word_item != '')
					$out[] = $word_item;
			}
			return ((count($out) > 0) ? implode(', ', $out) : '');
		}
		else
			return '';
	}

	protected function copyFromPost(&$object, $table)
	{
		parent::copyFromPost($object, $table);

		if (get_class($object) != 'Product')
			return;

		/* Additional fields */
		$languages = Language::getLanguages(false);
		foreach ($languages as $language)
			if (isset($_POST['meta_keywords_'.$language['id_lang']]))
			{
				$_POST['meta_keywords_'.$language['id_lang']] = $this->_cleanMetaKeywords(Tools::strtolower($_POST['meta_keywords_'.$language['id_lang']])); // preg_replace('/ *,? +,* /', ',', strtolower($_POST['meta_keywords_'.$language['id_lang']]));
				$object->meta_keywords[$language['id_lang']] = $_POST['meta_keywords_'.$language['id_lang']];
			}

			
				$_POST['width'] = empty($_POST['width']) ? '0' : str_replace(',', '.', $_POST['width']);
		$_POST['height'] = empty($_POST['height']) ? '0' : str_replace(',', '.', $_POST['height']);
		$_POST['depth'] = empty($_POST['depth']) ? '0' : str_replace(',', '.', $_POST['depth']);
		$_POST['weight'] = empty($_POST['weight']) ? '0' : str_replace(',', '.', $_POST['weight']);
		
		
		if ($_POST['unit_price'] != NULL)
			$object->unit_price = str_replace(',', '.', $_POST['unit_price']);
		if (array_key_exists('ecotax', $_POST) && $_POST['ecotax'] != NULL)
			$object->ecotax = str_replace(',', '.', $_POST['ecotax']);
		$object->available_for_order = (int)(Tools::isSubmit('available_for_order'));
		$object->show_price = $object->available_for_order ? 1 : (int)(Tools::isSubmit('show_price'));
		$object->on_sale = Tools::isSubmit('on_sale');
		$object->online_only = Tools::isSubmit('online_only');
		$object->fuori_produzione = Tools::isSubmit('fuori_produzione');
		$object->acquisto_in_dollari = Tools::isSubmit('acquisto_in_dollari');
		$object->blocco_prezzi = Tools::isSubmit('blocco_prezzi');
		$object->login_for_offer = Tools::isSubmit('login_for_offer');
		$object->noindex = Tools::isSubmit('noindex');
		$object->prodotto_con_canone = Tools::isSubmit('prodotto_con_canone');
		$object->ottieni_miglior_prezzo_flag = Tools::isSubmit('ottieni_miglior_prezzo_flag');
	}

	public function getList($id_lang, $orderBy = NULL, $orderWay = NULL, $start = 0, $limit = NULL)
	{
		global $cookie;

		$orderByPriceFinal = (empty($orderBy) ? ($cookie->__get($this->table.'Orderby') ? $cookie->__get($this->table.'Orderby') : 'id_'.$this->table) : $orderBy);
		$orderWayPriceFinal = (empty($orderWay) ? ($cookie->__get($this->table.'Orderway') ? $cookie->__get($this->table.'Orderby') : 'ASC') : $orderWay);
		if ($orderByPriceFinal == 'price_final')
		{
			$orderBy = 'id_'.$this->table;
			$orderWay = 'ASC';
		}
		parent::getList($id_lang, $orderBy, $orderWay, $start, $limit);

		/* update product quantity with attributes ...*/
		if ($this->_list)
		{
			$nb = count ($this->_list);
			for ($i = 0; $i < $nb; $i++)
				Attribute::updateQtyProduct($this->_list[$i]);
			/* update product final price */
			for ($i = 0; $i < $nb; $i++)
				$this->_list[$i]['price_tmp'] = Product::getPriceStatic($this->_list[$i]['id_product'], true, NULL, 6, NULL, false, true, 1, true);
		}

		if ($orderByPriceFinal == 'price_final')
		{
			if (strtolower($orderWayPriceFinal) == 'desc')
				uasort($this->_list, 'cmpPriceDesc');
			else
				uasort($this->_list, 'cmpPriceAsc');
		}
		for ($i = 0; $this->_list AND $i < $nb; $i++)
		{
			$this->_list[$i]['price_final'] = $this->_list[$i]['price_tmp'];
			unset($this->_list[$i]['price_tmp']);
		}
	}

	public function deleteVirtualProduct()
	{
		if (!($id_product_download = ProductDownload::getIdFromIdProduct((int)Tools::getValue('id_product'))) && !Tools::getValue('file'))
			return false;

		// case 1: product has been saved and product download entry has been created
		if ($id_product_download)
		{
			$productDownload = new ProductDownload((int)($id_product_download));
			return $productDownload->delete(true);
		}
		// case 2: product has not been created yet
		else
		{
			$file = Tools::getValue('file');
			if (file_exists(_PS_DOWNLOAD_DIR_.$file))
				return unlink(_PS_DOWNLOAD_DIR_.$file);
		}
	}

	/**
	 * postProcess handle every checks before saving products information
	 *
	 * @param mixed $token
	 * @return void
	 */
	public function postProcess($token = null)
	{
		global $cookie, $currentIndex;
		
		$this->_languages = Db::getInstance()->executeS('SELECT * FROM lang');
		
		$specificPrices = SpecificPrice::getByProductId((int)(Tools::getValue('id_product')));
	
		$specificPriceWholesale = Db::getInstance()->executeS('SELECT spw.*, "wholesale" as reduction_type FROM specific_price_wholesale spw WHERE id_product = '.Tools::getValue('id_product'));
		
		foreach($specificPriceWholesale as $sp)
			$specificPrices[] = $sp;
			
		foreach($specificPrices as $specificPrice) {
			
			$idprzspc = $specificPrice['id_specific_price'];
			$nomebottoneprzspc = "submitPricesModifiche_$idprzspc";
		   
			
			if (Tools::isSubmit($nomebottoneprzspc))
			{
			
				$hiddenidprzspc = $_POST["id_specific_price_$idprzspc"];
				$fromquantityprzspc = $_POST["sp_from_quantity_$idprzspc"];
			 
				$currencyprzspc = $_POST["sp_id_currency_$idprzspc"];
				$countryprzspc = $_POST["sp_id_country_$idprzspc"];
				$groupprzspc = $_POST["sp_id_group_$idprzspc"];
				$wholesalepricespc = str_replace(",",".",$_POST["sp_wholesale_price_$idprzspc"]);
				$priceprzspc = str_replace(",",".",$_POST["sp_price_$idprzspc"]);
				$reductionprzspc = 0; // $_POST["sp_reduction_$idprzspc"]/100;			
				$fromprzspc = date("Y-m-d 23:59:59", strtotime($_POST["sp_from_$idprzspc"]));
				if($_POST["sp_from_$idprzspc"] == '0000-00-00 00:00:00' || $_POST["sp_from_$idprzspc"] == '' || $_POST["sp_from_$idprzspc"] == '1970-01-01 01:00:00')
					$fromprzspc =  date('Y-m-d H:i:s');
					
				$toprzspc = date("Y-m-d 23:59:59", strtotime($_POST["sp_to_$idprzspc"]));
				if($_POST["sp_to_$idprzspc"] == '0000-00-00 00:00:00' || $_POST["sp_to_$idprzspc"] == '' || $_POST["sp_to_$idprzspc"] == '1970-01-01 01:00:00')
					$toprzspc =  date('Y-m-t 23:59:59');
				
				$piecesspc =  $_POST["sp_pieces_$idprzspc"];
				
				if($_POST["sp_reduction_type_$idprzspc"] != 'wholesale')
				{	
					Db::getInstance()->executeS("UPDATE `specific_price` SET `from_quantity` = '$fromquantityprzspc', 
					`id_currency` = '$currencyprzspc',
					`id_group` = '$groupprzspc',
					`id_country` = '$countryprzspc',
					`price` = '$priceprzspc',
					`reduction` = '$reductionprzspc',
					`wholesale_price` = '$wholesalepricespc',
					`from` = '$fromprzspc',
					`to` = '$toprzspc'
					WHERE `id_specific_price` = '$hiddenidprzspc'");
					
					if($piecesspc == '')
					{
						Db::getInstance()->getValue("DELETE FROM specific_price_pieces WHERE id_specific_price = ".$hiddenidprzspc."");
						
						Tools::redirectAdmin($currentIndex.'&id_product='.(int)(Tools::getValue($this->identifier)).'&id_category='.(int)(Tools::getValue('id_category')).'&addproduct&conf=4&tabs=2&token='.($token ? $token : $this->token));
					
					}
					else
					{
					//if($piecesspc > 0) {
						$stockspc = Db::getInstance()->getValue("SELECT stock FROM specific_price_pieces WHERE id_specific_price = ".$hiddenidprzspc."");
						$stockspc = $piecesspc;
						$ctpcs = Db::getInstance()->getValue("SELECT count(id_specific_price) FROM specific_price_pieces WHERE `id_specific_price` = '$hiddenidprzspc'");

						if($ctpcs > 0) {
							Db::getInstance()->executeS("UPDATE `specific_price_pieces` SET `pieces` = '$piecesspc', `stock` = '$piecesspc' WHERE `id_specific_price` = '$hiddenidprzspc'");
						}
						else {
							Db::getInstance()->executeS("INSERT INTO `specific_price_pieces` (id_specific_price, id_product, pieces, stock) VALUES ('$hiddenidprzspc', ".Tools::getValue('id_product').", '$piecesspc', '$piecesspc')");
						}
					//}
						Tools::redirectAdmin($currentIndex.'&id_product='.(int)(Tools::getValue($this->identifier)).'&id_category='.(int)(Tools::getValue('id_category')).'&addproduct&conf=4&tabs=2&token='.($token ? $token : $this->token));
					}
				}
				else
				{
					$red1przspc =  $_POST["sp_reductionw1_$idprzspc"];
					$red2przspc =  $_POST["sp_reductionw2_$idprzspc"];
					$red3przspc =  $_POST["sp_reductionw3_$idprzspc"];
					$idsupplier =  $_POST["sp_supplier_$idprzspc"];
					
					Db::getInstance()->executeS("UPDATE `specific_price_wholesale` SET 
					`price` = '$priceprzspc',
					`id_supplier` = '$idsupplier',
					`reduction_1` = '$red1przspc',
					`reduction_2` = '$red2przspc',
					`reduction_3` = '$red3przspc',
					`wholesale_price` = '$wholesalepricespc',
					`pieces` = '$piecesspc',
					`from` = '$fromprzspc',
					`to` = '$toprzspc'
					WHERE `id_specific_price` = '$hiddenidprzspc'");
					Tools::redirectAdmin($currentIndex.'&id_product='.(int)(Tools::getValue($this->identifier)).'&id_category='.(int)(Tools::getValue('id_category')).'&addproduct&conf=4&tabs=5&token='.($token ? $token : $this->token));
				}	
				
				
		
		
			}
			
		}
		
		

		// Add a new product
		if (Tools::isSubmit('submitAddproduct') || Tools::isSubmit('submitAddproductAndStay') ||  Tools::isSubmit('submitAddProductAndPreview'))
		{
		
			
			
			if($_POST['reference'] == '') { 
			}
			else {
				$rowdoppio = Db::getInstance()->getRow("SELECT id_product, reference FROM product WHERE reference = '".$_POST['reference']."' AND active != 2");
			
			
					if($rowdoppio['reference'] === $_POST['reference']) {
			
						if($rowdoppio['id_product'] == $_GET['id_product']) {
						}
						else {
						$this->_errors[] = Tools::displayError('Esiste già un prodotto nel database associato al codice di riferimento che hai inserito. <a href=\'#reference\' onclick=\'document.getElementById("reference").scrollIntoView(); document.getElementById("reference").focus(); document.getElementById("reference").style.border = "3px solid red";\'>Clicca qui per andare all\'errore</a>');
						}
					}
					
				
			}
			
			
			if($_POST['supplier_reference'] == '') { 
			}
			else {
				$rowdoppio = Db::getInstance()->getRow("SELECT id_product, supplier_reference FROM product WHERE supplier_reference = '".$_POST['supplier_reference']."' AND  active != 2");
				
				
				
				if($rowdoppio['supplier_reference'] === $_POST['supplier_reference']) {
				
				
					if($rowdoppio['id_product'] == $_GET['id_product']) {
					}
					else {
						$this->_errors[] = Tools::displayError('Esiste già un prodotto nel database associato al codice SKU che hai inserito. <a href=\'#supplier_reference\' onclick=\'document.getElementById("supplier_reference").scrollIntoView(); document.getElementById("supplier_reference").focus(); document.getElementById("supplier_reference").style.border = "3px solid red";\'>Clicca qui per andare all\'errore</a>');
					}
				}
				
				
			}
			
			if($_POST['ean13'] == '') { 
			}
			else {

			if(strlen($_POST['ean13']) != 13)
			{
			$this->_errors[] = Tools::displayError('Il codice EAN deve essere di 13 caratteri. <a href=\'#ean13\' onclick=\'document.getElementById("ean13").scrollIntoView(); document.getElementById("ean13").focus(); document.getElementById("ean13").style.border = "3px solid red";\'>Clicca qui per andare all\'errore</a>');
			}
				$rowdoppio = Db::getInstance()->getRow("SELECT id_product, ean13 FROM product WHERE ean13 = '".$_POST['ean13']."' AND active != 2");
				
				
				if($rowdoppio['ean13'] === $_POST['ean13']) {
				
				
					/*if($rowdoppio['id_product'] == $_GET['id_product']) {
					}
					else {
						$this->_errors[] = Tools::displayError('Esiste già un prodotto nel database associato al codice EAN che hai inserito.');
					}*/
				}
				
				
			}
			
			if($_POST['asin'] == '') { 
			}
			else {
				$rowdoppio = Db::getInstance()->executes("SELECT id_product, asin FROM product WHERE asin = '".$_POST['asin']."' AND active != 2");
				
				if($rowdoppio['asin'] == $_POST['asin']) {
				
				
					if($rowdoppio['id_product'] == $_GET['id_product']) {
					}
					else {
						$this->_errors[] = Tools::displayError('Esiste già un prodotto nel database associato al codice ASIN che hai inserito. <a href=\'#asin\' onclick=\'document.getElementById("asin").scrollIntoView(); document.getElementById("asin").focus(); document.getElementById("asin").style.border = "3px solid red";\'>Clicca qui per andare all\'errore</a>');
					}
				}
				
				
			}
			
			Db::getInstance()->executeS('UPDATE order_detail SET category_id = '.Tools::getValue('id_category_default').' WHERE product_id = '.Tools::getValue('id_product'));
			
			if($_POST['active'] == 0 || $_POST['price'] == 0) {
				if(isset($_GET['id_product'])) {
					Db::getInstance()->executeS("UPDATE bundle SET active = 0 WHERE father = ".$_GET['id_product']);
								
					$child_bundles = Db::getInstance()->executeS("SELECT * FROM bundle WHERE bundle_products LIKE '%".$_GET['id_product']."%'");
					foreach($child_bundles as $child) {
								
						$products_bundle = explode(";",$child['bundle_products']);
									
						foreach ($products_bundle as $product_bundle) {
							if($product_bundle == $_GET['id_product']) {
								echo $child['id_bundle']."<br />";
								Db::getInstance()->executeS("UPDATE bundle SET active = 0 WHERE id_bundle = ".$child['id_bundle']);
							}
						}
					}
				}
			
			}
			else {
				if(isset($_GET['id_product'])) {
					Db::getInstance()->executeS("UPDATE bundle SET active = 1 WHERE father = ".$_GET['id_product']);
								
					$child_bundles = Db::getInstance()->executeS("SELECT * FROM bundle WHERE bundle_products LIKE '%".$_GET['id_product']."%'");
					foreach($child_bundles as $child) {
									
						$products_bundle = explode(";",$child['bundle_products']);
									
						foreach ($products_bundle as $product_bundle) {	
							if($product_bundle == "") {
							}
							else {
								
								$child_p = new Product($product_bundle);
								if($child_p->price == 0) {
										
									Db::getInstance()->executeS("UPDATE bundle SET active = 0 WHERE id_bundle = ".$child['id_bundle']);
								}
								else {
								}
							}
						}
						
					}
				}
			}
			
			
			if ((Tools::getValue('id_product') && $this->tabAccess['edit'] === '1') || ($this->tabAccess['add'] === '1' && !Tools::isSubmit('id_product'))) {
				
				$reference = Db::getInstance()->getValue('SELECT reference FROM product WHERE id_product = '.$_GET['id_product']);
			
				if($reference != $_POST['reference'] && $reference != '')
				{
					$headers  = 'MIME-Version: 1.0' . "\n";
					$headers .= 'Content-Type: text/html' ."\n";
					$headers .= 'From: "Ezdirect" <info@ezdirect.it>' . "\n";
					mail('barbara.giorgini@ezdirect.it','Cambio codice eSolver su '.$reference, 'Attenzione, cambio di codice eSolver. <br />
					Codice vecchio: '.$reference.' / Codice nuovo: <strong>'.$_POST['reference'].'</strong>', $headers);
				}
			
				$this->submitAddproduct($token);
				}
			else {
				$this->_errors[] = Tools::displayError('You do not have permission to add here.');
				}
				
				
				
				
		}

		/* Delete a product in the download folder */
		if (Tools::getValue('deleteVirtualProduct'))
		{
			if ($this->tabAccess['delete'] === '1')
				$this->deleteVirtualProduct();
			else
				$this->_errors[] = Tools::displayError('You do not have permission to delete here.');
		}
		
		

		/* Update attachments */
		elseif (Tools::isSubmit('submitAddAttachments'))
		{
		
			//$this->salvatutto();
			if ($this->tabAccess['add'] === '1')
			{

				$languages = Language::getLanguages(false);
				$is_attachment_name_valid = false;
				foreach ($languages as $language)
				{
					$attachment_name_lang = Tools::getValue('attachment_name_'.(int)($language['id_lang']));
					if (strlen($attachment_name_lang ) > 0)
						$is_attachment_name_valid = true;

					if (!Validate::isGenericName(Tools::getValue('attachment_name_'.(int)($language['id_lang']))))
						$this->_errors[] = Tools::displayError('Ci sono caratteri non validi nel nome del file allegato');
					elseif (Tools::strlen(Tools::getValue('attachment_name_'.(int)($language['id_lang']))) > 32)
						$this->_errors[] = Tools::displayError('Il nome dell\'allegato &egrave; troppo lungo (max 32 caratteri)');
					if (!Validate::isCleanHtml(Tools::getValue('attachment_description_'.(int)($language['id_lang']))))
						$this->_errors[] = Tools::displayError('Ci sono caratteri non validi nella descrizione del file allegato');
				}
				if (!$is_attachment_name_valid) {
				
					$currentIndex.= "&nomeallegatononvalido";
					
					Tools::redirectAdmin($currentIndex.'&id_product='.(int)(Tools::getValue($this->identifier)).'&id_category='.(int)(Tools::getValue('id_category')).'&addproduct&tabs=6&token='.($token ? $token : $this->token));
					
								
				
				}

				if (empty($this->_errors))
				{
					if (isset($_FILES['attachment_file']) && is_uploaded_file($_FILES['attachment_file']['tmp_name']))
					{
						if ($_FILES['attachment_file']['size'] > (Configuration::get('PS_ATTACHMENT_MAXIMUM_SIZE') * 1024 * 1024))
							$this->_errors[] = $this->l('File allegato troppo grande, dimensione massima consentita: ').' '.(Configuration::get('PS_ATTACHMENT_MAXIMUM_SIZE') * 1024).' '.$this->l('kb').'. '.$this->l('File size you\'re trying to upload is:').number_format(($_FILES['attachment_file']['size']/1024), 2, '.', '').$this->l('kb');
						else
						{
							do $uniqid = sha1(microtime());	while (file_exists(_PS_DOWNLOAD_DIR_.$uniqid));
							
							
							
							if (!copy($_FILES['attachment_file']['tmp_name'], _PS_DOWNLOAD_DIR_.$uniqid))
								$this->_errors[] = $this->l('File copy failed');
							@unlink($_FILES['attachment_file']['tmp_name']);
						}
					}
					elseif ((int)$_FILES['attachment_file']['error'] === 1)
					{
						/*$max_upload = (int)ini_get('upload_max_filesize');
						$max_post = (int)ini_get('post_max_size');
						$upload_mb = min($max_upload, $max_post);
						$this->_errors[] = $this->l('the File').' <b>'.$_FILES['attachment_file']['name'].'</b> '.$this->l('exceeds the size allowed by the server, this limit is set to').' <b>'.$upload_mb.$this->l('Mb').'</b>';*/
					}

					if (empty($this->_errors) && isset($uniqid))
					{
						
						$attachment = new Attachment();
						foreach ($languages AS $language)
						{
							if (isset($_POST['attachment_name_'.(int)($language['id_lang'])]))
								$attachment->name[(int)($language['id_lang'])] = pSQL($_POST['attachment_name_'.(int)($language['id_lang'])]);
							if (isset($_POST['attachment_description_'.(int)($language['id_lang'])]))
								$attachment->description[(int)($language['id_lang'])] = pSQL($_POST['attachment_description_'.(int)($language['id_lang'])]);
						}
						$attachment->file = $uniqid;
						$attachment->mime = $_FILES['attachment_file']['type'];
						$attachment->file_name = pSQL($_FILES['attachment_file']['name']);
						if (empty($attachment->mime) OR Tools::strlen($attachment->mime) > 128)
							$this->_errors[] = Tools::displayError('Invalid file extension');
						if (!Validate::isGenericName($attachment->file_name))
							$this->_errors[] = Tools::displayError('Ci sono caratteri non validi ');
						if (Tools::strlen($attachment->file_name) > 128)
							$this->_errors[] = Tools::displayError('File name too long');
						if (!sizeof($this->_errors))
						{
							$attachment->add();
							$id_attachment = Db::getInstance()->getValue('SELECT id_attachment FROM attachment ORDER BY id_attachment DESC');
							
							Db::getInstance()->executeS("INSERT INTO product_attachment (id_product, id_attachment) VALUES ($_GET[id_product], $id_attachment)");
							
							$array_degli_allegati = array();
							
							
							$rowalleg = Db::getInstance()->getRow("SELECT id_attachment FROM product_attachment WHERE id_product = $_GET[id_product]");
								
							$array_degli_allegati[] = $rowalleg['id_attachment'];
							
							
							$array_degli_allegati[] = $id_attachment;
							
							//Attachment::attachToProduct($_GET['id_product'], $array_degli_allegati);
							
							Tools::redirectAdmin($currentIndex.'&id_product='.(int)(Tools::getValue($this->identifier)).'&id_category='.(int)(Tools::getValue('id_category')).'&updateproduct&id_product='.Tools::getValue('id_product').'&conf=4&tabs=6&token='.($token ? $token : $this->token));
						}
						else
							$this->_errors[] = Tools::displayError('Invalid file');
					}
				}
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to add here.');
			
			
		}
		elseif (Tools::isSubmit('submitAttachments'))
		{
			if ($this->tabAccess['edit'] === '1')
				if ($id = (int)(Tools::getValue($this->identifier)))
					if (Attachment::attachToProduct($id, $_POST['attachments']))
						Tools::redirectAdmin($currentIndex.'&id_product='.(int)$id.(isset($_POST['id_category']) ? '&id_category='.(int)$_POST['id_category'] : '').'&conf=4&add'.$this->table.'&tabs=6&token='.($token ? $token : $this->token));
		}
		elseif (Tools::isSubmit('submitnewbundle')) {
			
			$bundle_products = "";
			$bundle_prices = "";
			
			foreach ($_POST['nuovo_prodotto'] as $id_product_g=>$id_product) {
				$bundle_products.= trim($_POST['nuovo_prodotto'][$id_product_g]).";";
				$bundle_prices.= trim($_POST['nuovo_prodotto'][$id_product_g]).";".$_POST['nuovo_quantity'][trim($id_product_g)].";".$_POST['nuovo_sconto'][trim($id_product_g)].";".$_POST['nuovo_scontoriv'][trim($id_product_g)].":";
			}
			
			$amazon_array = array();
			if(isset($_POST['bundle_amazon_it'])) 
				$amazon_array[] = 5;
			if(isset($_POST['bundle_amazon_fr'])) 
				$amazon_array[] = 2;	
			if(isset($_POST['bundle_amazon_es'])) 
				$amazon_array[] = 3;	
			if(isset($_POST['bundle_amazon_uk'])) 
				$amazon_array[] = 1;	
			if(isset($_POST['bundle_amazon_de'])) 
				$amazon_array[] = 4;
			if(isset($_POST['bundle_amazon_nl'])) 
				$amazon_array[] = 6;
			if(isset($_POST['bundle_amazon_se'])) 
				$amazon_array[] = 7;
			if(isset($_POST['bundle_amazon_pl'])) 
				$amazon_array[] = 8;
			
			$amazon_string = implode(";",$amazon_array);
		
			Db::getInstance()->executeS("INSERT INTO bundle (id_bundle, bundle_name, bundle_ref, ean13, father, id_category_default, bundle_products, bundle_prices, amazon, estensione_garanzia, supporto_da_remoto, telegestione, active, date_add, date_upd) VALUES ('NULL', '".$_POST['bundle_name_5']."', '".$_POST['bundle_ref']."', '".$_POST['bundle_ean13']."', '".$_GET['id_product']."', '".$_POST['id_category_default']."', '".$bundle_products."', '".$bundle_prices."', '".$amazon_string."', ".$_POST['estensione_garanzia'].", ".$_POST['supporto_da_remoto'].", ".$_POST['telegestione'].", ".$_POST['bundle_active'].", '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
			
			$new_bundle_id = Db::getInstance()->Insert_ID();
			
			foreach($this->_languages as $language)
			{
				Db::getInstance()->executeS('INSERT INTO bundle_lang (id_bundle, id_lang, name, description_amazon) VALUES ('.$new_bundle_id.', '.$language['id_lang'].', "'.$_POST['bundle_name_'.$language['id_lang']].'", "'.$_POST['bundle_description_amazon_'.$language['id_lang']].'")');
			}
			
			foreach($_POST['categoryBox'] as $catbox) 
			{
				if(isset($catbox) && $catbox != $_POST['id_category_default'])
					Db::getInstance()->executeS("INSERT INTO category_bundle (id_category, id_bundle) VALUES (".$catbox.", ".$new_bundle_id.")");
			}
			
			Bundle::salvaCategoria($new_bundle_id);
			
			if(isset($_POST['trovaprezzi_add_bundle'])) {
				Db::getInstance()->executeS("UPDATE bundle SET trovaprezzi = 1 WHERE id_bundle = $new_bundle_id");
			}
			else {
				Db::getInstance()->executeS("UPDATE bundle SET trovaprezzi = 0 WHERE id_bundle = $new_bundle_id");
			}
			
			if(isset($_POST['google_shopping_add_bundle'])) {
				Db::getInstance()->executeS("UPDATE bundle SET google_shopping = 1 WHERE id_bundle = $new_bundle_id");
			}
			else {
				Db::getInstance()->executeS("UPDATE bundle SET google_shopping = 0 WHERE id_bundle = $new_bundle_id");
			}
			
			if(isset($_POST['tutti_add_bundle'])) {
				Db::getInstance()->executeS("UPDATE bundle SET comparaprezzi_check = 1 WHERE id_bundle = $new_bundle_id");
			}
			else {
				Db::getInstance()->executeS("UPDATE bundle SET comparaprezzi_check = 0 WHERE id_bundle = $new_bundle_id");
			}
			
			if(isset($_POST['trovaprezzi_add_bundle']) && isset($_POST['google_shopping_add_bundle'])) {
				Db::getInstance()->executeS("UPDATE bundle SET comparaprezzi_check = 1 WHERE id_bundle = $new_bundle_id");
			}
			else {
				Db::getInstance()->executeS("UPDATE bundle SET comparaprezzi_check = 0 WHERE id_bundle = $new_bundle_id");
			}	

			$this->addBundleImage($new_bundle_id, $_POST['id_image_bundle']);
			Tools::redirectAdmin($currentIndex.'&id_product='.$_GET['id_product'].'&updateproduct&conf=4&tabs=3&token='.($token ? $token : $this->token));
						
		}
		elseif (isset($_GET['deletebundle'])) {

			Db::getInstance()->execute("DELETE FROM bundle WHERE id_bundle = $_GET[id_bundle]");
			Db::getInstance()->execute("DELETE FROM bundle_lang WHERE id_bundle = $_GET[id_bundle]");
			Db::getInstance()->execute("
				DELETE bundle_image, bundle_image_lang
				FROM bundle_image
				INNER JOIN bundle_image_lang ON bundle_image_lang.id_image = bundle_image.id_image
				WHERE bundle_image.id_bundle = ".$_GET['id_bundle']."
			");
			
			Tools::redirectAdmin($currentIndex.'&id_product='.$_GET['id_product'].'&updateproduct&conf=4&tabs=3&token='.($token ? $token : $this->token));
		}
		elseif (Tools::isSubmit('submitupdatedbundle')) {
			
			$bundle_products = "";
			$bundle_prices = "";
			
			foreach ($_POST['nuovo_prodotto'] as $id_product_g=>$id_product) {
				$bundle_products.= trim($_POST['nuovo_prodotto'][$id_product_g]).";";
				$bundle_prices.= trim($_POST['nuovo_prodotto'][$id_product_g]).";".$_POST['nuovo_quantity'][trim($id_product_g)].";".$_POST['nuovo_sconto'][trim($id_product_g)].";".$_POST['nuovo_scontoriv'][trim($id_product_g)].":";
			}
			
			$amazon_array = array();
			if(isset($_POST['bundle_amazon_it'])) 
				$amazon_array[] = 5;
			if(isset($_POST['bundle_amazon_fr'])) 
				$amazon_array[] = 2;	
			if(isset($_POST['bundle_amazon_es'])) 
				$amazon_array[] = 3;	
			if(isset($_POST['bundle_amazon_uk'])) 
				$amazon_array[] = 1;	
			if(isset($_POST['bundle_amazon_de'])) 
				$amazon_array[] = 4;
			if(isset($_POST['bundle_amazon_nl'])) 
				$amazon_array[] = 6;
			if(isset($_POST['bundle_amazon_se'])) 
				$amazon_array[] = 7;
			if(isset($_POST['bundle_amazon_pl'])) 
				$amazon_array[] = 8;
			
			$amazon_string = implode(";",$amazon_array);
			
			Db::getInstance()->executeS("UPDATE bundle SET date_upd = '".date("Y-m-d H:i:s")."', bundle_name = '".$_POST['bundle_name_5']."', bundle_ref = '".$_POST['bundle_ref']."', ean13 = '".$_POST['bundle_ean13']."', amazon = '".$amazon_string."', asin = '".$_POST['bundle_asin']."', id_category_default = '".$_POST['id_category_default']."', bundle_products = '".$bundle_products."', bundle_prices = '".$bundle_prices."', estensione_garanzia = '".$_POST['estensione_garanzia']."', supporto_da_remoto = '".$_POST['supporto_da_remoto']."', telegestione = '".$_POST['telegestione']."', active = '".$_POST['bundle_active']."' WHERE id_bundle = '".$_POST['id_bundle']."'");
			
			foreach($this->_languages as $language)
			{
				Db::getInstance()->executeS('REPLACE INTO bundle_lang (id_bundle, id_lang, name, description_amazon) VALUES ('.$_POST['id_bundle'].', '.$language['id_lang'].', "'.$_POST['bundle_name_'.$language['id_lang']].'", "'.$_POST['bundle_description_amazon_'.$language['id_lang']].'")');
			}
			
			Db::getInstance()->executeS("DELETE FROM category_bundle WHERE id_bundle = ".$_POST['id_bundle']."");
			
			foreach($_POST['categoryBox'] as $catbox) 
			{
				if(isset($catbox) && $_POST['id_category_default'])
					Db::getInstance()->executeS("INSERT INTO category_bundle (id_category, id_bundle) VALUES (".$catbox.", ".$_POST['id_bundle'].")");
			}
			
			Bundle::salvaCategoria($_POST['id_bundle']);
			
			$this->addBundleImage($_POST['id_bundle'], $_POST['id_image_bundle']);
			
			if(isset($_POST['trovaprezzi_add_bundle'])) {
				Db::getInstance()->executeS("UPDATE bundle SET trovaprezzi = 1 WHERE id_bundle = ".$_POST['id_bundle']."");
			}
			else {
				Db::getInstance()->executeS("UPDATE bundle SET trovaprezzi = 0 WHERE id_bundle = ".$_POST['id_bundle']."");
			}
					
			if(isset($_POST['google_shopping_add_bundle'])) {
				Db::getInstance()->executeS("UPDATE bundle SET google_shopping = 1 WHERE id_bundle = ".$_POST['id_bundle']."");
			}
			else {
				Db::getInstance()->executeS("UPDATE bundle SET google_shopping = 0 WHERE id_bundle = ".$_POST['id_bundle']."");
			}
			
			if(isset($_POST['tutti_add_bundle'])) {
				Db::getInstance()->executeS("UPDATE bundle SET comparaprezzi_check = 1 WHERE id_bundle = ".$_POST['id_bundle']."");
			}
			else {
				Db::getInstance()->executeS("UPDATE bundle SET comparaprezzi_check = 0 WHERE id_bundle = ".$_POST['id_bundle']."");
			}
			
			if(isset($_POST['trovaprezzi_add_bundle']) && isset($_POST['google_shopping_add_bundle'])) {
				Db::getInstance()->executeS("UPDATE bundle SET comparaprezzi_check = 1 WHERE id_bundle = ".$_POST['id_bundle']."");
			}
			else {
				Db::getInstance()->executeS("UPDATE bundle SET comparaprezzi_check = 0 WHERE id_bundle = ".$_POST['id_bundle']."");
			}

			Tools::redirectAdmin($currentIndex.'&id_product='.$_GET['id_product'].'&updateproduct&conf=4&tabs=3&token='.($token ? $token : $this->token));
			
		}
		/* Product duplication */
		elseif (isset($_GET['duplicate'.$this->table]))
		{
			if($cookie->id_employee == 6 || $cookie->id_employee == 22)
				ini_set('display_errors', 'on');

			if ($this->tabAccess['add'] === '1')
			{
				if (Validate::isLoadedObject($product = new Product((int)(Tools::getValue('id_product')))))
				{
					$id_product_old = $product->id;
					$reference_old = $product->reference;
					unset($product->id);
					unset($product->id_product);
					$product->indexed = 0;
					$product->active = 0;
					$product->price = 0;
					$product->listino = 0;
					$product->wholesale_price = 0;
					$product->sconto_acquisto_1 = 0;
					$product->sconto_acquisto_2 = 0;
					$product->sconto_acquisto_3 = 0;
					$product->scontolistinovendita = 0;
					$product->reference = "";
					$product->supplier_reference = '';
					$product->ean13 = '';
					$product->description_amazon = '';
					$product->asin = '';
					$product->upc = '';
					$product->quantity = 0;
					$product->stock_quantity = 0;
					$product->arrivo_quantity = 0;
					$product->supplier_quantity = 0;
					$product->esprinet_quantity = 0;
					$product->attiva_quantity = 0;
					$product->itancia_quantity = 0;
					$product->scorta_minima = 0;
					$product->scorta_minima_amazon = 0;
					$product->ordinato_quantity = 0;
					$product->impegnato_quantity = 0;
					if ($product->add()
					AND Category::duplicateProductCategories($id_product_old, $product->id)
					AND ($combinationImages = Product::duplicateAttributes($id_product_old, $product->id)) !== false
					AND GroupReduction::duplicateReduction($id_product_old, $product->id)
					AND Product::duplicateAccessories($id_product_old, $product->id)
					AND Product::duplicateAlternatives($id_product_old, $product->id)
					AND Product::duplicateAttachments($id_product_old, $product->id)
					AND Product::duplicateFeatures($id_product_old, $product->id)
				//	AND Product::duplicateSpecificPrices($id_product_old, $product->id)
					AND Pack::duplicate($id_product_old, $product->id)
					AND Product::duplicateCustomizationFields($id_product_old, $product->id)
					AND Product::duplicateDownload($id_product_old, $product->id))
					{
					
						Product::duplicateComparaprezzi($id_product_old, $product->id);
			
			
						if ($product->hasAttributes())
							Product::updateDefaultAttribute($product->id);

						if (!Tools::getValue('noimage') AND !Image::duplicateProductImages($id_product_old, $product->id, $combinationImages))
							$this->_errors[] = Tools::displayError('An error occurred while copying images.');
						else
						{
							Hook::addProduct($product);
							Search::indexation(false, $product->id);
							Tools::redirectAdmin($currentIndex.'&id_product='.(!empty($product->id)?$product->id:'1').'&updateproduct&token='.($token ? $token : $this->token));
						}
					}
					else
						$this->_errors[] = Tools::displayError('An error occurred while creating object.');
				}
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to add here.');
		}
		/* Change object statuts (active, inactive) */
		elseif (isset($_GET['status']) AND Tools::getValue($this->identifier))
		{
			if ($this->tabAccess['edit'] === '1')
			{
				if (Validate::isLoadedObject($object = $this->loadObject()))
				{
					if ($object->toggleStatus())
						Tools::redirectAdmin($currentIndex.'&conf=5'.((($id_category = (!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1')) AND Tools::getValue('id_product')) ? '&id_category='.$id_category : '').'&token='.$token);
					else
						$this->_errors[] = Tools::displayError('An error occurred while updating status.');
				}
				else
					$this->_errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
		}
		/* Delete object */
		elseif (isset($_GET['delete'.$this->table]))
		{
			if ($this->tabAccess['delete'] === '1')
			{
				if (Validate::isLoadedObject($object = $this->loadObject()) AND isset($this->fieldImageSettings))
				{
					// check if request at least one object with noZeroObject
					if (isset($object->noZeroObject) AND sizeof($taxes = call_user_func(array($this->className, $object->noZeroObject))) <= 1)
						$this->_errors[] = Tools::displayError('You need at least one object.').' <b>'.$this->table.'</b><br />'.Tools::displayError('You cannot delete all of the items.');
					else
					{
						$id_category = Tools::getValue('id_category');
						$category_url = empty($id_category) ? '' : '&id_category='.$id_category;
						Db::getInstance()->executeS("DELETE FROM bundle WHERE father = ".$object->id);
							
							$child_bundles = Db::getInstance()->executeS("SELECT * FROM bundle WHERE bundle_products LIKE '%".$object->id."%'");
							foreach($child_bundles as $child) {
							
								$products_bundle = explode(";",$child['bundle_products']);
								
								foreach ($products_bundle as $product_bundle) {
									if($product_bundle == $object->id) {
										echo $child['id_bundle']."<br />";
										Db::getInstance()->executeS("UPDATE bundle SET active = 0 WHERE id_bundle = ".$child['id_bundle']);
									}
								}
							}
							
						if ($this->deleted)
						{
										
							$object->deleteImages();
							$object->deleted = 1;
							if ($object->update())
								Tools::redirectAdmin($currentIndex.'&conf=1&token='.($token ? $token : $this->token).$category_url);
						}
						
							
						elseif ($object->delete())
							Tools::redirectAdmin($currentIndex.'&conf=4&token='.($token ? $token : $this->token).$category_url);
						//$this->_errors[] = Tools::displayError('An error occurred during deletion: this product is contained in an order.');
						Tools::redirectAdmin($currentIndex.'&conf=4&token='.($token ? $token : $this->token).$category_url);
					}
				}
				else
					$this->_errors[] = Tools::displayError('An error occurred while deleting object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to delete here.');
		}

		/* Delete multiple objects */
		elseif (Tools::getValue('submitDel'.$this->table))
		{
			if ($this->tabAccess['delete'] === '1')
			{
				if (isset($_POST[$this->table.'Box']))
				{
					$object = new $this->className();

					if (isset($object->noZeroObject) AND
						// Check if all object will be deleted
						(sizeof(call_user_func(array($this->className, $object->noZeroObject))) <= 1 OR sizeof($_POST[$this->table.'Box']) == sizeof(call_user_func(array($this->className, $object->noZeroObject)))))
						$this->_errors[] = Tools::displayError('You need at least one object.').' <b>'.$this->table.'</b><br />'.Tools::displayError('You cannot delete all of the items.');
					else
					{
						$result = true;
						if ($this->deleted)
						{
							foreach(Tools::getValue($this->table.'Box') as $id)
							{
								$toDelete = new $this->className($id);
								$toDelete->deleted = 1;
								$result = $result AND $toDelete->update();
							}
						}
						else
							$result = $object->deleteSelection(Tools::getValue($this->table.'Box'));

						if ($result)
						{
							$id_category = Tools::getValue('id_category');
							$category_url = empty($id_category) ? '' : '&id_category='.$id_category;

							Tools::redirectAdmin($currentIndex.'&conf=2&token='.$token.$category_url);
						}
						$this->_errors[] = Tools::displayError('An error occurred while deleting selection.');
					}
				}
				else
					$this->_errors[] = Tools::displayError('You must select at least one element to delete.');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to delete here.');
		}

		/* Product images management */
		elseif (($id_image = (int)(Tools::getValue('id_image'))) AND Validate::isUnsignedId($id_image) AND Validate::isLoadedObject($image = new Image($id_image)))
		{
			/* PrestaShop demo mode */
			if (_PS_MODE_DEMO_)
			{
				$this->_errors[] = Tools::displayError('This functionnality has been disabled.');
				return;
			}
			/* PrestaShop demo mode*/

			if ($this->tabAccess['edit'] === '1')
			{
				/* Delete product image */
				if (isset($_GET['deleteImage']))
				{
					$image->delete();

					if (!Image::getCover($image->id_product))
					{
						$first_img = Db::getInstance()->getRow('
						SELECT `id_image` FROM `'._DB_PREFIX_.'image`
						WHERE `id_product` = '.(int)($image->id_product));
						Db::getInstance()->Execute('
						UPDATE `'._DB_PREFIX_.'image`
						SET `cover` = 1
						WHERE `id_image` = '.(int)($first_img['id_image']));
					}
					@unlink(_PS_TMP_IMG_DIR_.'/product_'.$image->id_product.'.jpg');
					@unlink(_PS_TMP_IMG_DIR_.'/product_mini_'.$image->id_product.'.jpg');
					Tools::redirectAdmin($currentIndex.'&id_product='.$image->id_product.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&add'.$this->table.'&tabs=1'.'&token='.($token ? $token : $this->token));
				}

				/* Update product image/legend */
				elseif (isset($_GET['editImage']))
				{
					if ($image->cover)
						$_POST['cover'] = 1;
					$languages = Language::getLanguages(false);
					foreach ($languages as $language)
						if (isset($image->legend[$language['id_lang']]))
							$_POST['legend_'.$language['id_lang']] = $image->legend[$language['id_lang']];
					$_POST['id_image'] = $image->id;
					$this->displayForm();
				}

				/* Choose product cover image */
				elseif (isset($_GET['coverImage']))
				{
					Image::deleteCover($image->id_product);
					$image->cover = 1;
					if (!$image->update())
						$this->_errors[] = Tools::displayError('Cannot change the product cover');
					else
					{
						$productId = (int)(Tools::getValue('id_product'));
						@unlink(_PS_TMP_IMG_DIR_.'/product_'.$productId.'.jpg');
						@unlink(_PS_TMP_IMG_DIR_.'/product_mini_'.$productId.'.jpg');
						Tools::redirectAdmin($currentIndex.'&id_product='.$image->id_product.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&addproduct&tabs=1'.'&token='.($token ? $token : $this->token));
					}
				}

				/* Choose product image position */
				elseif (isset($_GET['imgPosition']) AND isset($_GET['imgDirection']))
				{
					$image->positionImage((int)(Tools::getValue('imgPosition')), (int)(Tools::getValue('imgDirection')));
					Tools::redirectAdmin($currentIndex.'&id_product='.$image->id_product.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&add'.$this->table.'&tabs=1&token='.($token ? $token : $this->token));
				}
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
		}

		/* Product attributes management */
		elseif (Tools::isSubmit('submitProductAttribute'))
		{
			if (Validate::isLoadedObject($product = new Product((int)(Tools::getValue('id_product')))))
			{
				if (!isset($_POST['attribute_price']) OR $_POST['attribute_price'] == NULL)
					$this->_errors[] = Tools::displayError('Attribute price required.');
				if (!isset($_POST['attribute_combinaison_list']) OR !sizeof($_POST['attribute_combinaison_list']))
					$this->_errors[] = Tools::displayError('You must add at least one attribute.');

				if (!sizeof($this->_errors))
				{
					if (!isset($_POST['attribute_wholesale_price'])) $_POST['attribute_wholesale_price'] = 0;
					if (!isset($_POST['attribute_price_impact'])) $_POST['attribute_price_impact'] = 0;
					if (!isset($_POST['attribute_weight_impact'])) $_POST['attribute_weight_impact'] = 0;
					if (!isset($_POST['attribute_ecotax'])) $_POST['attribute_ecotax'] = 0;
					if (Tools::getValue('attribute_default'))
						$product->deleteDefaultAttributes();
					// Change existing one
					if ($id_product_attribute = (int)(Tools::getValue('id_product_attribute')))
					{
						if ($this->tabAccess['edit'] === '1')
						{
							if ($product->productAttributeExists($_POST['attribute_combinaison_list'], $id_product_attribute))
								$this->_errors[] = Tools::displayError('This attribute already exists.');
							else
							{
								$product->updateProductAttribute($id_product_attribute,
								Tools::getValue('attribute_wholesale_price'),
								Tools::getValue('attribute_price') * Tools::getValue('attribute_price_impact'),
								Tools::getValue('attribute_weight') * Tools::getValue('attribute_weight_impact'),
								Tools::getValue('attribute_unity') * Tools::getValue('attribute_unit_impact'),
								Tools::getValue('attribute_ecotax'),
								false,
								Tools::getValue('id_image_attr'),
								Tools::getValue('attribute_reference'),
								Tools::getValue('attribute_supplier_reference'),
								Tools::getValue('attribute_ean13'),
								Tools::getValue('attribute_default'),
								Tools::getValue('attribute_location'),
								Tools::getValue('attribute_upc'),
								Tools::getValue('attribute_minimal_quantity'));
								if ($id_reason = (int)Tools::getValue('id_mvt_reason') AND (int)Tools::getValue('attribute_mvt_quantity') > 0 AND $id_reason > 0)
								{
									$reason = new StockMvtReason((int)$id_reason);
									$qty = Tools::getValue('attribute_mvt_quantity') * $reason->sign;
									if (!$product->addStockMvt($qty, $id_reason, (int)$id_product_attribute, NULL, $cookie->id_employee))
										$this->_errors[] = Tools::displayError('An error occurred while updating qty.');
								}
								Hook::updateProductAttribute((int)$id_product_attribute);
							}
						}
						else
							$this->_errors[] = Tools::displayError('You do not have permission to add here.');
					}
					// Add new
					else
					{
						if ($this->tabAccess['add'] === '1')
						{
							if ($product->productAttributeExists($_POST['attribute_combinaison_list']))
								$this->_errors[] = Tools::displayError('This combination already exists.');
							else
							{
								$id_product_attribute = $product->addCombinationEntity(Tools::getValue('attribute_wholesale_price'),
								Tools::getValue('attribute_price') * Tools::getValue('attribute_price_impact'), Tools::getValue('attribute_weight') * Tools::getValue('attribute_weight_impact'),
								Tools::getValue('attribute_unity') * Tools::getValue('attribute_unit_impact'),
								Tools::getValue('attribute_ecotax'), Tools::getValue('attribute_quantity'),	Tools::getValue('id_image_attr'), Tools::getValue('attribute_reference'),
								Tools::getValue('attribute_supplier_reference'), Tools::getValue('attribute_ean13'), Tools::getValue('attribute_default'), Tools::getValue('attribute_location'), Tools::getValue('attribute_upc'), Tools::getValue('attribute_minimal_quantity'));
							}
						}
						else
							$this->_errors[] = Tools::displayError('You do not have permission to').'<hr>'.Tools::displayError('Edit here.');
					}
					if (!sizeof($this->_errors))
					{
						$product->addAttributeCombinaison($id_product_attribute, Tools::getValue('attribute_combinaison_list'));
						$product->checkDefaultAttributes();
					}
					if (!sizeof($this->_errors))
					{
						if (!$product->cache_default_attribute)
							Product::updateDefaultAttribute($product->id);
						Tools::redirectAdmin($currentIndex.'&id_product='.$product->id.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&add'.$this->table.'&tabs=3&token='.($token ? $token : $this->token));
					}
				}
			}
		}
		elseif (Tools::isSubmit('deleteProductAttribute'))
		{
			if ($this->tabAccess['delete'] === '1')
			{
				if (($id_product = (int)(Tools::getValue('id_product'))) AND Validate::isUnsignedId($id_product) AND Validate::isLoadedObject($product = new Product($id_product)))
				{
					$product->deleteAttributeCombinaison((int)(Tools::getValue('id_product_attribute')));
					$product->checkDefaultAttributes();
					$product->updateQuantityProductWithAttributeQuantity();
					if (!$product->hasAttributes())
					{
						$product->cache_default_attribute = 0;
						$product->update();
					}else
						Product::updateDefaultAttribute($id_product);

					Tools::redirectAdmin($currentIndex.'&add'.$this->table.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&tabs=3&id_product='.$product->id.'&token='.($token ? $token : $this->token));
				}
				else
					$this->_errors[] = Tools::displayError('Cannot delete attribute');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to delete here.');
		}
		elseif (Tools::isSubmit('deleteAllProductAttributes'))
		{
			if ($this->tabAccess['delete'] === '1')
			{
				if (($id_product = (int)(Tools::getValue('id_product'))) AND Validate::isUnsignedId($id_product) AND Validate::isLoadedObject($product = new Product($id_product)))
				{
					$product->deleteProductAttributes();
					$product->updateQuantityProductWithAttributeQuantity();
					if ($product->cache_default_attribute)
					{
						$product->cache_default_attribute = 0;
						$product->update();
					}
					Tools::redirectAdmin($currentIndex.'&add'.$this->table.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&tabs=3&id_product='.$product->id.'&token='.($token ? $token : $this->token));
				}
				else
					$this->_errors[] = Tools::displayError('Cannot delete attributes');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to delete here.');
		}
		elseif (Tools::isSubmit('defaultProductAttribute'))
		{
			if (Validate::isLoadedObject($product = new Product((int)(Tools::getValue('id_product')))))
			{
				$product->deleteDefaultAttributes();
				$product->setDefaultAttribute((int)(Tools::getValue('id_product_attribute')));
				Tools::redirectAdmin($currentIndex.'&add'.$this->table.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&tabs=3&id_product='.$product->id.'&token='.($token ? $token : $this->token));
			}
			else
				$this->_errors[] = Tools::displayError('Cannot make default attribute');
		}

		/* Product features management */
		elseif (Tools::isSubmit('submitProductFeature'))
		{
			if ($this->tabAccess['edit'] === '1')
			{
				if (Validate::isLoadedObject($product = new Product((int)(Tools::getValue('id_product')))))
				{
					// delete all objects
					$product->deleteFeatures();

					// add new objects
					$languages = Language::getLanguages(false);
					foreach ($_POST AS $key => $val)
					{
						if (preg_match('/^feature_([0-9]+)_value/i', $key, $match))
						{
							if ($val)
								$product->addFeaturesToDB($match[1], $val);
							else
							{
								if ($default_value = $this->checkFeatures($languages, $match[1]))
								{
									$id_value = $product->addFeaturesToDB($match[1], 0, 1, (int)$language['id_lang']);
									foreach ($languages AS $language)
									{
										if ($cust = Tools::getValue('custom_'.$match[1].'_'.(int)$language['id_lang']))
											$product->addFeaturesCustomToDB($id_value, (int)$language['id_lang'], $cust);
										else
											$product->addFeaturesCustomToDB($id_value, (int)$language['id_lang'], $default_value);
									}
								}
							}
						}
					}
					if (!sizeof($this->_errors))
						Tools::redirectAdmin($currentIndex.'&id_product='.(int)$product->id.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&add'.$this->table.'&tabs=4&conf=4&token='.($token ? $token : $this->token));
				}
				else
					$this->_errors[] = Tools::displayError('Product must be created before adding features.');
			}
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
		}
		/* Product specific prices management */
		elseif (Tools::isSubmit('submitPricesModification'))
		{
			$_POST['tabs'] = 5;
			if ($this->tabAccess['edit'] === '1')
			{
				$id_specific_prices = Tools::getValue('spm_id_specific_price');
				$id_shops = Tools::getValue('spm_id_shop');
				$id_currencies = Tools::getValue('spm_id_currency');
				$id_countries = Tools::getValue('spm_id_country');
				$id_groups = Tools::getValue('spm_id_group');
				$prices = str_replace(",",".",Tools::getValue('spm_price'));
				$from_quantities = Tools::getValue('spm_from_quantity');
				$reductions = Tools::getValue('spm_reduction');
				$reduction_types = Tools::getValue('spm_reduction_type');
				$froms = Tools::getValue('spm_from');
				
				if($froms == '' || $froms == '0000-00-00 00:00:00' || $froms == '1970-01-01 01:00:00')
					$froms = date('Y-m-d H:i:s');
				
				$tos = Tools::getValue('spm_to');
				
				if($tos == '' || $tos == '0000-00-00 00:00:00' || $tos == '1970-01-01 01:00:00')
					$tos = date('Y-m-t 23:59:59');
				
				foreach ($id_specific_prices as $key => $id_specific_price)
					if ($this->_validateSpecificPrice($id_shops[$key], $id_currencies[$key], $id_countries[$key], $id_groups[$key], $prices[$key], $from_quantities[$key], $reductions[$key], $reduction_types[$key], $froms[$key], $tos[$key]))
					{
						$specificPrice = new SpecificPrice((int)($id_specific_price));
						$specificPrice->id_shop = (int)($id_shops[$key]);
						$specificPrice->id_currency = (int)($id_currencies[$key]);
						$specificPrice->id_country = (int)($id_countries[$key]);
						$specificPrice->id_group = (int)($id_groups[$key]);
						$specificPrice->price = (float)($prices[$key]);
						$specificPrice->from_quantity = (int)($from_quantities[$key]);
						$specificPrice->reduction = (float)($reduction_types[$key] == 'percentage' ? ($reductions[$key] / 100) : $reductions[$key]);
						$specificPrice->reduction_type = !$reductions[$key] ? 'amount' : $reduction_types[$key];
						$specificPrice->from = !$froms[$key] ? date('Y-m-d H:i:s') : $froms[$key];
						$specificPrice->to = !$tos[$key] ?  date('Y-m-t 23:59:59') : str_replace('00:00:00', '23:59:59', $tos[$key]);
						
						$specificPriceToD = explode(' ',$specificPrice->to);
						$specificPrice->to = $specificPriceToD[0].' 23:59:59';
						
						if (!$specificPrice->update())
							$this->_errors = Tools::displayError('An error occurred while updating the specific price.');
					}
				if (!sizeof($this->_errors))
					Tools::redirectAdmin($currentIndex.'&id_product='.(int)(Tools::getValue('id_product')).'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&update'.$this->table.'&tabs=2&token='.($token ? $token : $this->token));
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to add here.');
		}
		elseif (Tools::isSubmit('submitPriceAddition'))
		{
			if(Tools::getValue('submitPriceAddition') == 'Aggiungi prezzo acquisto speciale')
			{
				if(str_replace(",",".",Tools::getValue('sp_wholesale_price')) == 0)
					$this->_errors[] = Tools::displayError('Devi specificare un prezzo d\'acquisto');
				
				if (!sizeof($this->_errors))
				{	
					$sp_fromw = Tools::getValue('sp_fromw');
					if($sp_fromw == '' || $sp_fromw == '0000-00-00 00:00:00' || $sp_fromw == '1970-01-01 01:00:00')
						$sp_fromw = date('Y-m-d H:i:s');
					
					$sp_tow = Tools::getValue('sp_tow');
					if($sp_tow == '' || $sp_tow == '0000-00-00 00:00:00' || $sp_tow == '1970-01-01 01:00:00')
						$sp_tow = date('Y-m-t 23:59:59');
					
					$ins_price = Db::getInstance()->getValue('SELECT price FROM product WHERE id_product = '.Tools::getValue('id_product'));
					Db::getInstance()->execute('INSERT INTO specific_price_wholesale VALUES (NULL, '.Tools::getValue('id_product').', "'.Tools::getValue('sp_supplier_w').'", "'.$ins_price.'", "'.str_replace(",",".",Tools::getValue('sp_reductionw1')).'", "'.str_replace(",",".",Tools::getValue('sp_reductionw2')).'", "'.str_replace(",",".",Tools::getValue('sp_reductionw3')).'", "'.str_replace(",",".",Tools::getValue('sp_wholesale_pricew')).'", "'.date("Y-m-d H:i:s", strtotime($sp_fromw)).'", "'.date("Y-m-d H:i:s", strtotime($sp_tow)).'", "'.Tools::getValue('sp_piecesw').'")');
					

					Tools::redirectAdmin($currentIndex.'&id_product='.(int)(Tools::getValue('id_product')).'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&update'.$this->table.'&tabs=5&conf=4&token='.($token ? $token : $this->token));
				}
			}
			else if (Tools::getValue('submitPriceAddition') == 'Modifica prezzo acquisto speciale')
			{
				$id_specific_price = Tools::getValue('id_specific_price_acq');
				if(str_replace(",",".",Tools::getValue('sp_wholesale_price')) == 0)
					$this->_errors[] = Tools::displayError('Devi specificare un prezzo d\'acquisto');
				
				if (!sizeof($this->_errors))
				{	
					$sp_fromw = Tools::getValue('sp_fromw');
					if($sp_fromw == '' || $sp_fromw == '0000-00-00 00:00:00' || $sp_fromw == '1970-01-01 01:00:00')
						$sp_fromw = date('Y-m-d H:i:s');
					
					$sp_tow = Tools::getValue('sp_tow');
					if($sp_tow == '' || $sp_tow == '0000-00-00 00:00:00' || $sp_tow == '1970-01-01 01:00:00')
						$sp_tow = date('Y-m-t 23:59:59');
					
					$sp_supplier = Tools::getValue('sp_supplier_w');
					
					$ins_price = Db::getInstance()->getValue('SELECT price FROM product WHERE id_product = '.Tools::getValue('id_product'));
					
					Db::getInstance()->execute('UPDATE specific_price_wholesale spw SET spw.id_supplier = "'.$sp_supplier.'", spw.price = "'.$ins_price.'", spw.reduction_1 = "'.str_replace(",",".",Tools::getValue('sp_reductionw1')).'", spw.reduction_2 = "'.str_replace(",",".",Tools::getValue('sp_reductionw2')).'", spw.reduction_3 = "'.str_replace(",",".",Tools::getValue('sp_reductionw3')).'", spw.wholesale_price = "'.str_replace(",",".",Tools::getValue('sp_wholesale_pricew')).'", spw.from = "'.date("Y-m-d H:i:s", strtotime($sp_fromw)).'", spw.to = "'.date("Y-m-d H:i:s", strtotime($sp_tow)).'", spw.pieces = "'.Tools::getValue('sp_piecesw').'" WHERE id_specific_price = '.$id_specific_price.'');

					Tools::redirectAdmin($currentIndex.'&id_product='.(int)(Tools::getValue('id_product')).'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&update'.$this->table.'&tabs=5&conf=4&token='.($token ? $token : $this->token));
				}
				
			}	
			
			else
			{
				if ($this->tabAccess['add'] === '1')
				{
					$id_product = (int)(Tools::getValue('id_product'));
					$id_shop = Tools::getValue('sp_id_shop');
					$id_currency = Tools::getValue('sp_id_currency');
					$id_country = Tools::getValue('sp_id_country');
					$wholesale_price_sp = str_replace(",",".",Tools::getValue('sp_wholesale_price'));
					$id_group = Tools::getValue('sp_id_group');
					$price = str_replace(",",".",Tools::getValue('sp_price'));
					$from_quantity = Tools::getValue('sp_from_quantity');
					$reduction = (float)(str_replace(",",".",Tools::getValue('sp_reduction')));
					$reduction = $reduction/100;
					$reduction_type = 'amount';
					$from = date("Y-m-d H:i:s", strtotime(Tools::getValue('sp_from')));
					if($from == '0000-00-00 00:00:00' || $from == '' || $from == '1970-01-01 01:00:00')
						$from =  date('Y-m-d H:i:s');
					$to = date("Y-m-d H:i:s", strtotime(Tools::getValue('sp_to')));
					
					if($to == '0000-00-00 00:00:00' || $to == '' || $to == '1970-01-01 01:00:00')
						$to =  date('Y-m-t 23:59:59');
					
						
					if ($this->_validateSpecificPrice($id_shop, $id_currency, $id_country, $id_group, $price, $from_quantity, $reduction, $reduction_type, $from, $to))
					{
						if(Tools::getIsset('id_specific_price_vnd'))
							$specificPrice = new SpecificPrice(Tools::getValue('id_specific_price_vnd'));
						else
							$specificPrice = new SpecificPrice();
						
						$specificPrice->id_product = $id_product;
						$specificPrice->id_shop = (int)($id_shop);
						$specificPrice->id_currency = (int)($id_currency);
						$specificPrice->id_country = (int)($id_country);
						$specificPrice->id_group = (int)($id_group);
						$specificPrice->price = (float)($price);
						$specificPrice->from_quantity = (int)($from_quantity);
						$specificPrice->reduction = 0;
						$specificPrice->reduction_type = $reduction_type;
						$specificPrice->wholesale_price = $wholesale_price_sp;
						$specificPrice->from = !$from ?  date('Y-m-d H:i:s') : $from;
						$specificPrice->to = !$to ?  date('Y-m-t 23:59:59') : $to;
					
						$specificPriceToD = explode(' ',$specificPrice->to);
						$specificPrice->to = $specificPriceToD[0].' 23:59:59';
						
						if(Tools::getIsset('id_specific_price_vnd'))
						{
							if (!$specificPrice->update()) {
								$this->_errors = Tools::displayError('An error occurred while updating the specific price.');
							}
						}	
						else {
							if (!$specificPrice->add()) {
								$this->_errors = Tools::displayError('An error occurred while updating the specific price.');
							}
						}
						
						if(!$this->_errors) 
						{	
					
							if(Tools::getIsset('id_specific_price_vnd'))
							{	
								if(Tools::getIsset('abilita_banda'))
									Db::getInstance()->executeS('UPDATE specific_price SET abilita_banda = 1 WHERE id_specific_price = '.Tools::getValue('id_specific_price_vnd'));
								else
									Db::getInstance()->executeS('UPDATE specific_price SET abilita_banda = 0 WHERE id_specific_price = '.Tools::getValue('id_specific_price_vnd'));
							}
							else
							{
								if(Tools::getIsset('abilita_banda'))
									Db::getInstance()->executeS('UPDATE specific_price SET abilita_banda = 1 WHERE id_specific_price = '.Db::getInstance()->Insert_ID());
								else
									Db::getInstance()->executeS('UPDATE specific_price SET abilita_banda = 0 WHERE id_specific_price = '.Db::getInstance()->Insert_ID());
							}
							if(Tools::getIsset('sp_pieces') && Tools::getValue('sp_pieces') >= 0 &&  Tools::getValue('sp_pieces') != '') {

								if(Tools::getIsset('id_specific_price_vnd'))
								{	
									
									$id_pieces = Db::getInstance()->getValue('SELECT id_specific_price FROM specific_price_pieces WHERE id_specific_price = '.Tools::getValue('id_specific_price_vnd'));
									
									
									if($id_pieces == Tools::getValue('id_specific_price_vnd'))
										Db::getInstance()->executeS("UPDATE specific_price_pieces SET pieces = ".Tools::getValue('sp_pieces').", stock = ".Tools::getValue('sp_pieces')." WHERE id_specific_price = ".Tools::getValue('id_specific_price_vnd')."");
									else
										Db::getInstance()->executeS("INSERT INTO specific_price_pieces (id_specific_price, id_product, pieces, stock) VALUES (".Tools::getValue('id_specific_price_vnd').", ".$id_product.", ".Tools::getValue('sp_pieces').", ".Tools::getValue('sp_pieces').")");
									
									
								}
								else
								{
									Db::getInstance()->executeS("INSERT INTO specific_price_pieces (id_specific_price, id_product, pieces, stock) VALUES (".Db::getInstance()->Insert_ID().", ".$id_product.", ".Tools::getValue('sp_pieces').", ".Tools::getValue('sp_pieces').")");
									
								}
							}
							else
							{
								Db::getInstance()->executeS('DELETE FROM specific_price_pieces WHERE id_specific_price = '.Tools::getValue('id_specific_price_vnd'));

							}
							Tools::redirectAdmin($currentIndex.(Tools::getValue('id_category') ? '&id_category='.Tools::getValue('id_category') : '').'&id_product='.$id_product.'&add'.$this->table.'&tabs=2&conf=3&token='.($token ? $token : $this->token));
						}
					}
					
				}
				else
					$this->_errors[] = Tools::displayError('You do not have permission to add here.');
			}
		}
		elseif (Tools::isSubmit('deleteSpecificPrice'))
		{
			if ($this->tabAccess['delete'] === '1')
			{
				if (!($obj = $this->loadObject()))
					return;
				if (!$id_specific_price = Tools::getValue('id_specific_price') OR !Validate::isUnsignedId($id_specific_price))
					$this->_errors[] = Tools::displayError('Invalid specific price ID');
				else
				{
					$specificPrice = new SpecificPrice((int)($id_specific_price));
					if (!$specificPrice->delete())
						$this->_errors[] = Tools::displayError('An error occurred while deleting the specific price');
					else
						Tools::redirectAdmin($currentIndex.(Tools::getValue('id_category') ? '&id_category='.Tools::getValue('id_category') : '').'&id_product='.$obj->id.'&add'.$this->table.'&tabs=2&conf=1&token='.($token ? $token : $this->token));
				}
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to delete here.');
		}
		elseif (Tools::isSubmit('deleteSpecificPriceWholesale'))
		{
			if (!($obj = $this->loadObject()))
				return;
		
			$id_specific_price = Tools::getValue('id_specific_price');
			if (!Db::getInstance()->execute('DELETE FROM specific_price_wholesale WHERE id_specific_price = '.$id_specific_price))
				$this->_errors[] = Tools::displayError('An error occurred while deleting the specific price');
			else
				Tools::redirectAdmin($currentIndex.(Tools::getValue('id_category') ? '&id_category='.Tools::getValue('id_category') : '').'&id_product='.$obj->id.'&add'.$this->table.'&tabs=5&conf=1&token='.($token ? $token : $this->token));
			
		}
		elseif (Tools::isSubmit('submitSpecificPricePriorities'))
		{
			if (!($obj = $this->loadObject()))
				return;
			if (!$priorities = Tools::getValue('specificPricePriority'))
				$this->_errors[] = Tools::displayError('Please specify priorities');
			elseif (Tools::isSubmit('specificPricePriorityToAll'))
			{
				if (!SpecificPrice::setPriorities($priorities))
					$this->_errors[] = Tools::displayError('An error occurred while updating priorities.');
				else
					Tools::redirectAdmin($currentIndex.'&id_product='.$obj->id.'&add'.$this->table.'&tabs=2&conf=4&token='.($token ? $token : $this->token));
			}
			elseif (!SpecificPrice::setSpecificPriority((int)($obj->id), $priorities))
				$this->_errors[] = Tools::displayError('An error occurred while setting priorities.');
			else
				Tools::redirectAdmin($currentIndex.(Tools::getValue('id_category') ? '&id_category='.Tools::getValue('id_category') : '').'&id_product='.$obj->id.'&add'.$this->table.'&tabs=2&conf=4&token='.($token ? $token : $this->token));
		}
		/* Customization management */
		elseif (Tools::isSubmit('submitCustomizationConfiguration'))
		{
			if ($this->tabAccess['edit'] === '1')
			{
				if (Validate::isLoadedObject($product = new Product((int)(Tools::getValue('id_product')))))
				{
					if (!$product->createLabels((int)($_POST['uploadable_files']) - (int)($product->uploadable_files), (int)($_POST['text_fields']) - (int)($product->text_fields)))
						$this->_errors[] = Tools::displayError('An error occurred while creating customization fields.');
					if (!sizeof($this->_errors) AND !$product->updateLabels())
						$this->_errors[] = Tools::displayError('An error occurred while updating customization.');
					$product->uploadable_files = (int)($_POST['uploadable_files']);
					$product->text_fields = (int)($_POST['text_fields']);
					$product->customizable = ((int)($_POST['uploadable_files']) > 0 OR (int)($_POST['text_fields']) > 0) ? 1 : 0;
					if (!sizeof($this->_errors) AND !$product->update())
						$this->_errors[] = Tools::displayError('An error occurred while updating customization configuration.');
					if (!sizeof($this->_errors))
						Tools::redirectAdmin($currentIndex.'&id_product='.$product->id.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&add'.$this->table.'&tabs=5&token='.($token ? $token : $this->token));
				}
				else
					$this->_errors[] = Tools::displayError('Product must be created before adding customization possibilities.');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
		}
		elseif (Tools::isSubmit('submitProductCustomization'))
		{
			if ($this->tabAccess['edit'] === '1')
			{
				if (Validate::isLoadedObject($product = new Product((int)(Tools::getValue('id_product')))))
				{
					foreach ($_POST AS $field => $value)
						if (strncmp($field, 'label_', 6) == 0 AND !Validate::isLabel($value))
							$this->_errors[] = Tools::displayError('Label fields are invalid');
					if (!sizeof($this->_errors) AND !$product->updateLabels())
						$this->_errors[] = Tools::displayError('An error occurred while updating customization.');
					if (!sizeof($this->_errors))
						Tools::redirectAdmin($currentIndex.'&id_product='.$product->id.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&add'.$this->table.'&tabs=5&token='.($token ? $token : $this->token));
				}
				else
					$this->_errors[] = Tools::displayError('Product must be created before adding customization possibilities.');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
		}
		elseif (isset($_GET['position']))
		{
			if ($this->tabAccess['edit'] !== '1')
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
			elseif (!Validate::isLoadedObject($object = $this->loadObject()))
				$this->_errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			if (!$object->updatePosition((int)(Tools::getValue('way')), (int)(Tools::getValue('position'))))
				$this->_errors[] = Tools::displayError('Failed to update the position.');
			else
				Tools::redirectAdmin($currentIndex.'&'.$this->table.'Orderby=position&'.$this->table.'Orderway=asc&conf=5'.(($id_category = (!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1')) ? ('&id_category='.$id_category) : '').'&token='.Tools::getAdminTokenLite('AdminCatalog'));
		}
		
		
		else if(Tools::isSubmit('salvatutto')) {

			
			$this->salvatutto();
		}
		
		elseif(Tools::isSubmit('accoda_esolver'))
		{
			ini_set('display_errors', 'off');
			ini_set("memory_limit","892M");
			Db::getInstance()->executeS('UPDATE product SET date_upd = "'.date('Y-m-d H:i:s').'" WHERE id_product = '.Tools::getValue('id_product').'');
			
			Product::anagraficheArticoliEsolver();
			
			Tools::redirectAdmin($currentIndex.(Tools::getValue('id_category') ? '&id_category='.Tools::getValue('id_category') : '').'&id_product='.Tools::getValue('id_product').'&update'.$this->table.'&tabs=7&conf=4&token='.($token ? $token : $this->token));
		}
		
		
		else
			parent::postProcess(true);
	}

	protected function _validateSpecificPrice($id_shop, $id_currency, $id_country, $id_group, $price, $from_quantity, $reduction, $reduction_type, $from, $to)
	{
		if (!Validate::isUnsignedId($id_shop) OR !Validate::isUnsignedId($id_currency) OR !Validate::isUnsignedId($id_country) OR !Validate::isUnsignedId($id_group))
			$this->_errors[] = Tools::displayError('Wrong ID\'s');
		elseif ((empty($price) AND empty($reduction)) OR (!empty($price) AND !Validate::isPrice($price)) OR (!empty($reduction) AND !Validate::isPrice($reduction)))
			$this->_errors[] = Tools::displayError('Invalid price/reduction amount');
		elseif (!Validate::isUnsignedInt($from_quantity))
			$this->_errors[] = Tools::displayError('Invalid quantity');
		/*elseif ($reduction AND !Validate::isReductionType($reduction_type))
			$this->_errors[] = Tools::displayError('Please select a reduction type (amount or percentage)');*/
		elseif ($from AND $to AND (!Validate::isDateFormat($from) OR !Validate::isDateFormat($to)))
			$this->_errors[] = Tools::displayError('Wrong from/to date');
		else
			return true;
		return false;
	}

	// Checking customs feature
	private function checkFeatures($languages, $feature_id)
	{
		$rules = call_user_func(array('FeatureValue', 'getValidationRules'), 'FeatureValue');
		$feature = Feature::getFeature(Configuration::get('PS_LANG_DEFAULT'), $feature_id);
		$val = 0;
		foreach ($languages AS $language)
			if ($val = Tools::getValue('custom_'.$feature_id.'_'.$language['id_lang']))
			{
				$currentLanguage = new Language($language['id_lang']);
				if (!call_user_func(array('Validate', $rules['validateLang']['value']), $val))
					$this->_errors[] = Tools::displayError('Valid name required for feature.').' <b>'.$feature['name'].'</b> '.Tools::displayError('in').' '.$currentLanguage->name;
				if (sizeof($this->_errors))
					return (0);
				// Getting default language
				if ($language['id_lang'] == Configuration::get('PS_LANG_DEFAULT'))
					return ($val);
			}
		return (0);
	}


	/**
	 * Add or update a product image
	 *
	 * @param object $product Product object to add image
	 */
	public function addProductImage($product, $method = 'auto')
	{
		/* Updating an existing product image */
		if ($id_image = ((int)(Tools::getValue('id_image'))))
		{
			$image = new Image($id_image);
			if (!Validate::isLoadedObject($image))
				$this->_errors[] = Tools::displayError('An error occurred while loading object image.');
			else
			{
				if (($cover = Tools::getValue('cover')) == 1)
					Image::deleteCover($product->id);
				$image->cover = $cover;
				$this->validateRules('Image');
				$this->copyFromPost($image, 'image');
				if (sizeof($this->_errors) OR !$image->update())
					$this->_errors[] = Tools::displayError('An error occurred while updating image.');
				elseif (isset($_FILES['image_product']['tmp_name']) AND $_FILES['image_product']['tmp_name'] != NULL)
					$this->copyImage($product->id, $image->id, $method);
			}
		}

		/* Adding a new product image */
		elseif (isset($_FILES['image_product']['name']) && $_FILES['image_product']['name'] != NULL )
		{

			if ($error = checkImageUploadError($_FILES['image_product']))
				$this->_errors[] = $error;

			if (!sizeof($this->_errors) AND isset($_FILES['image_product']['tmp_name']) AND $_FILES['image_product']['tmp_name'] != NULL)
			{
				if (!Validate::isLoadedObject($product))
					$this->_errors[] = Tools::displayError('Cannot add image because product add failed.');
				elseif (substr($_FILES['image_product']['name'], -4) == '.zip')
					return $this->uploadImageZip($product);
				else
				{
					$image = new Image();
					$image->id_product = (int)($product->id);
					$_POST['id_product'] = $image->id_product;
					$image->position = Image::getHighestPosition($product->id) + 1;
					if (($cover = Tools::getValue('cover')) == 1)
						Image::deleteCover($product->id);
					$image->cover = !$cover ? !sizeof($product->getImages(Configuration::get('PS_LANG_DEFAULT'))) : true;
					$this->validateRules('Image', 'image');
					$this->copyFromPost($image, 'image');
					if (!sizeof($this->_errors))
					{
						if (!$image->add())
							$this->_errors[] = Tools::displayError('Error while creating additional image');
						else
							$this->copyImage($product->id, $image->id, $method);
						$id_image = $image->id;
					}
				}
			}
		}
		if (isset($image) AND Validate::isLoadedObject($image) AND !file_exists(_PS_PROD_IMG_DIR_.$image->getExistingImgPath().'.'.$image->image_format))
			$image->delete();
		if (sizeof($this->_errors))
			return false;
		@unlink(_PS_TMP_IMG_DIR_.'/product_'.$product->id.'.jpg');
		@unlink(_PS_TMP_IMG_DIR_.'/product_mini_'.$product->id.'.jpg');
		return ((isset($id_image) AND is_int($id_image) AND $id_image) ? $id_image : true);
	}

	public function uploadImageZip($product)
	{
		// Move the ZIP file to the img/tmp directory
		if (!$zipfile = tempnam(_PS_TMP_IMG_DIR_, 'PS') OR !move_uploaded_file($_FILES['image_product']['tmp_name'], $zipfile))
		{
			$this->_errors[] = Tools::displayError('An error occurred during the ZIP file upload.');
			return false;
		}

		// Unzip the file to a subdirectory
		$subdir = _PS_TMP_IMG_DIR_.uniqid().'/';

		try
		{
			if (!Tools::ZipExtract($zipfile, $subdir))
				throw new Exception(Tools::displayError('An error occurred while unzipping your file.'));

			$types = array('.gif' => 'image/gif', '.jpeg' => 'image/jpeg', '.jpg' => 'image/jpg', '.png' => 'image/png');
			$_POST['id_product'] = (int)$product->id;
			$imagesTypes = ImageType::getImagesTypes('products');
			$highestPosition = Image::getHighestPosition($product->id);
			foreach (scandir($subdir) as $file)
			{
				if ($file[0] == '.')
					continue;

				// Create image object
				$image = new Image();
				$image->id_product = (int)$product->id;
				$image->position = ++$highestPosition;
				$image->cover = ($highestPosition == 1 ? true : false);

				// Call automated copy function
				$this->validateRules('Image', 'image');
				$this->copyFromPost($image, 'image');

				if (sizeof($this->_errors))
					throw new Exception('');

				if (!$image->add())
					throw new Exception(Tools::displayError('Error while creating additional image'));

				if (filesize($subdir.$file) > $this->maxImageSize)
				{
					$image->delete();
					throw new Exception(Tools::displayError('Image is too large').' ('.(filesize($subdir.$file) / 1000).Tools::displayError('kB').'). '.Tools::displayError('Maximum allowed:').' '.($this->maxImageSize / 1000).Tools::displayError('kB'));
				}

				$ext = (substr($file, -4) == 'jpeg') ? '.jpeg' : substr($file, -4);
				$type = (isset($types[$ext]) ? $types[$ext] : '');
				if (!isPicture(array('tmp_name' => $subdir.$file, 'type' => $type)))
				{
					$image->delete();
					throw new Exception(Tools::displayError('Image format not recognized, allowed formats are: .gif, .jpg, .png'));
				}

				if (!$new_path = $image->getPathForCreation())
					throw new Exception(Tools::displayError('An error occurred during new folder creation'));

				if (!imageResize($subdir.$file, $new_path.'.'.$image->image_format))
				{
					$image->delete();
					throw new Exception(Tools::displayError('An error occurred while resizing image.'));
				}

				foreach ($imagesTypes AS $k => $imageType)
					if (!imageResize($image->getPathForCreation().'.jpg', $image->getPathForCreation().'-'.stripslashes($imageType['name']).'.jpg', $imageType['width'], $imageType['height']))
					{
						$image->delete();
						throw new Exception(Tools::displayError('An error occurred while copying image.').' '.stripslashes($imageType['name']));
					}

				Module::hookExec('watermark', array('id_image' => $image->id, 'id_product' => $image->id_product));
			}
		}
		catch (Exception $e)
		{
			if ($error = $e->getMessage());
				$this->_errors[] = $error;
			Tools::deleteDirectory($subdir);
			return false;
		}

		Tools::deleteDirectory($subdir);
		return true;
	}

	/**
	 * Copy a product image
	 *
	 * @param integer $id_product Product Id for product image filename
	 * @param integer $id_image Image Id for product image filename
	 */
	public function copyImage($id_product, $id_image, $method = 'auto')
	{
		if (!isset($_FILES['image_product']['tmp_name']))
			return false;
		if ($error = checkImage($_FILES['image_product'], $this->maxImageSize))
			$this->_errors[] = $error;
		else
		{
			$image = new Image($id_image);

			if (!$new_path = $image->getPathForCreation())
				$this->_errors[] = Tools::displayError('An error occurred during new folder creation');
			if (!$tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS') OR !move_uploaded_file($_FILES['image_product']['tmp_name'], $tmpName))
				$this->_errors[] = Tools::displayError('An error occurred during the image upload');
			elseif (!imageResize($tmpName, $new_path.'.'.$image->image_format))
				$this->_errors[] = Tools::displayError('An error occurred while copying image.');
			elseif ($method == 'auto')
			{
				$imagesTypes = ImageType::getImagesTypes('products');
				foreach ($imagesTypes AS $k => $imageType)
					if (!imageResize($tmpName, $new_path.'-'.stripslashes($imageType['name']).'.'.$image->image_format, $imageType['width'], $imageType['height'], $image->image_format))
						$this->_errors[] = Tools::displayError('An error occurred while copying image:').' '.stripslashes($imageType['name']);
			}

			@unlink($tmpName);
			Module::hookExec('watermark', array('id_image' => $id_image, 'id_product' => $id_product));
		}
	}

	/**
	 * Add or update a product
	 *
	 * @global string $currentIndex Current URL in order to keep current Tab
	 */
	public function submitAddProduct($token = NULL)
	{
	


	
	
		global $cookie, $currentIndex, $link;

		$className = 'Product';
		$rules = call_user_func(array($this->className, 'getValidationRules'), $this->className);
		$defaultLanguage = new Language((int)(Configuration::get('PS_LANG_DEFAULT')));
		$languages = Language::getLanguages(false);

		/* Check required fields */
		foreach ($rules['required'] AS $field)
			if (($value = Tools::getValue($field)) == false AND $value != '0')
			{
				if(Tools::getIsset('sku_provvisorio') && $field == 'reference')
				{
				}
				else
				{
					if (Tools::getValue('id_'.$this->table) AND $field == 'passwd')
					continue;
					$this->_errors[] = $this->l('Il campo').' <b>'.call_user_func(array($className, 'displayFieldName'), $field, $className).'</b> '.$this->l(' &grave; obbligatorio. <a href=\'#'.$field.'\' onclick=\'document.getElementById("'.$field.'").style.border = "3px solid red"; document.getElementById("'.$field.'").scrollIntoView(); document.getElementById("'.$field.'").focus(); \'>Clicca qui per andare all\'errore</a>');
				}
			}

		/* Check multilingual required fields */
		foreach ($rules['requiredLang'] AS $fieldLang)
			if (!Tools::getValue($fieldLang.'_'.$defaultLanguage->id))
				$this->_errors[] = $this->l('the field').' <b>'.call_user_func(array($className, 'displayFieldName'), $fieldLang, $className).'</b> '.('&egrave; obbligatorio. <a href=\'#'.$fieldLang.'_'.$defaultLanguage->id.'\' onclick=\' document.getElementById("'.$fieldLang.'_'.$defaultLanguage->id.'").focus(); document.getElementById("'.$fieldLang.'_'.$defaultLanguage->id.'").style.border = "3px solid red"; document.getElementById("'.$fieldLang.'_'.$defaultLanguage->id.'").scrollIntoView();\'>Clicca qui per andare all\'errore</a>');

		/* Check fields sizes */
		foreach ($rules['size'] AS $field => $maxLength)
			if ($value = Tools::getValue($field) AND Tools::strlen($value) > $maxLength)
				$this->_errors[] = $this->l('Il campo').' <b>'.call_user_func(array($className, 'displayFieldName'), $field, $className).'</b> '.('&egrave; troppo lungo').' ('.$maxLength.' '.$this->l('chars max').'). <a href=\'#'.$field.'\' onclick=\'document.getElementById("'.$field.'").style.border = "3px solid red"; document.getElementById("'.$field.'").scrollIntoView(); document.getElementById("'.$field.'").focus(); \'>Clicca qui per andare all\'errore</a>';

		if (isset($_POST['description_short']))
		{
			$saveShort = $_POST['description_short'];
			$_POST['description_short'] = strip_tags($_POST['description_short']);
		}

		/* Check description short size without html */
		$limit = (int)Configuration::get('PS_PRODUCT_SHORT_DESC_LIMIT');
		if ($limit <= 0) $limit = 400;
		foreach ($languages AS $language)
			if ($value = Tools::getValue('description_short_'.$language['id_lang']))
				if (Tools::strlen(strip_tags($value)) > $limit)
					$this->_errors[] = 'Il campo descrizione breve &egrave; troppo lungo. Max '.$limit.' caratteri (ora ce ne sono '.Tools::strlen(strip_tags($value)).'). <a href=\'#description_short_'.$language['id_lang'].'\' onclick=\'document.getElementById("description_short_'.$language['id_lang'].'").style.border = "3px solid red"; document.getElementById("description_short_'.$language['id_lang'].'").scrollIntoView(); document.getElementById("description_short_'.$language['id_lang'].'").focus(); \'>Clicca qui per andare all\'errore</a>';
		/* Check multilingual fields sizes */
		foreach ($rules['sizeLang'] AS $fieldLang => $maxLength)
			foreach ($languages AS $language)
				if ($value = Tools::getValue($fieldLang.'_'.$language['id_lang']) AND Tools::strlen($value) > $maxLength)
					$this->_errors[] = $this->l('the field').' <b>'.call_user_func(array($className, 'displayFieldName'), $fieldLang, $className).' ('.$language['name'].')</b> '.$this->l('is too long').' ('.$maxLength.' '.$this->l('chars max').')';
		if (isset($_POST['description_short']))
			$_POST['description_short'] = $saveShort;

		/* Check fields validity */
		foreach ($rules['validate'] AS $field => $function)
			if ($value = Tools::getValue($field))
				if (!Validate::$function($value))
					$this->_errors[] = $this->l('Il campo').' <b>'.call_user_func(array($className, 'displayFieldName'), $field, $className).'</b> '.$this->l('ha caratteri non validi').'<a href=\'#'.$field.'\' onclick=\'document.getElementById("'.$field.'").style.border = "3px solid red"; document.getElementById("'.$field.'").scrollIntoView(); document.getElementById("'.$field.'").focus(); \'>Clicca qui per andare all\'errore</a>';

		/* Check multilingual fields validity */
		foreach ($rules['validateLang'] AS $fieldLang => $function)
			foreach ($languages AS $language)
				if ($value = Tools::getValue($fieldLang.'_'.$language['id_lang']))
					if (!Validate::$function($value))
						$this->_errors[] = $this->l('Il campo ').' <b>'.call_user_func(array($className, 'displayFieldName'), $fieldLang, $className).' ('.$language['name'].')</b> '.$this->l('ha caratteri non validi.').'<a href=\'#'.$fieldLang.'_'.$language['id_lang'].'\' onclick=\'document.getElementById("'.$fieldLang.'_'.$language['id_lang'].'").style.border = "3px solid red"; document.getElementById("'.$fieldLang.'_'.$language['id_lang'].'").scrollIntoView(); document.getElementById("'.$fieldLang.'_'.$language['id_lang'].'").focus(); \'>Clicca qui per andare all\'errore</a>';

		/* Categories */
		$productCats = '';
		if (!Tools::isSubmit('categoryBox') OR !sizeof(Tools::getValue('categoryBox')))
			$this->_errors[] = $this->l('product must be in at least one Category');

		if (!is_array(Tools::getValue('categoryBox')) OR !in_array(Tools::getValue('id_category_default'), Tools::getValue('categoryBox')))
			$this->_errors[] = $this->l('product must be in the default category');

		/* Tags */
		foreach ($languages AS $language)
			if ($value = Tools::getValue('tags_'.$language['id_lang']))
				if (!Validate::isTagsListModificata($value))
					$this->_errors[] = $this->l('Tags list').' ('.$language['name'].') '.$this->l('is invalid');

		if (!sizeof($this->_errors))
		{
			$id = (int)(Tools::getValue('id_'.$this->table));
			$tagError = true;

			/* Update an existing product */
			if (isset($id) AND !empty($id))
			{
				$object = new $this->className($id);
				if (Validate::isLoadedObject($object))
				{
					$this->_removeTaxFromEcotax();
					$this->copyFromPost($object, $this->table);
					if ($object->update())
					{
						if ($id_reason = (int)Tools::getValue('id_mvt_reason') AND (int)Tools::getValue('mvt_quantity') > 0 AND $id_reason > 0)
						{
							$reason = new StockMvtReason((int)$id_reason);
							$qty = Tools::getValue('mvt_quantity') * $reason->sign;
							if (!$object->addStockMvt($qty, (int)$id_reason, NULL, NULL, (int)$cookie->id_employee))
								$this->_errors[] = Tools::displayError('An error occurred while updating qty.');
						}
						$this->updateAccessories($object);
						$this->updateAlternatives($object);
						$this->updateDownloadProduct($object);
						$this->aggiornaCoseNonPredefinite($object);
						//creazione tag automatica
						

						if (!$this->updatePackItems($object))
							$this->_errors[] = Tools::displayError('An error occurred while adding products to the pack.');
						elseif (!$object->updateCategories($_POST['categoryBox'], true))
							$this->_errors[] = Tools::displayError('An error occurred while linking object.').' <b>'.$this->table.'</b> '.Tools::displayError('To categories');
						elseif (!$this->updateTags($languages, $object))
							$this->_errors[] = Tools::displayError('An error occurred while adding tags.');
						elseif ($id_image = $this->addProductImage($object, Tools::getValue('resizer')))
						{
							$currentIndex .= '&image_updated='.(int)Tools::getValue('id_image');
							Hook::updateProduct($object);
							Search::indexation(false, $object->id);
							if (Tools::getValue('resizer') == 'man' && isset($id_image) AND is_int($id_image) AND $id_image)
								Tools::redirectAdmin($currentIndex.'&id_product='.$object->id.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&edit='.strval(Tools::getValue('productCreated')).'&id_image='.$id_image.'&imageresize&toconf=4&submitAddAndStay='.((Tools::isSubmit('submitAdd'.$this->table.'AndStay') OR Tools::getValue('productCreated') == 'on') ? 'on' : 'off').'&token='.(($token ? $token : $this->token)));

							// Save and preview
							if (Tools::isSubmit('submitAddProductAndPreview'))
							{
								$preview_url = ($link->getProductLink($this->getFieldValue($object, 'id'), $this->getFieldValue($object, 'link_rewrite', (int)($cookie->id_lang)), Category::getLinkRewrite($this->getFieldValue($object, 'id_category_default'), (int)($cookie->id_lang))));
								if (!$object->active)
								{
									$admin_dir = dirname($_SERVER['PHP_SELF']);
									$admin_dir = substr($admin_dir, strrpos($admin_dir,'/') + 1);
									$token = Tools::encrypt('PreviewProduct'.$object->id);
									if (strpos($preview_url, '?') === false)
										$preview_url .= '?';
									else
										$preview_url .= '&';
									$preview_url .= 'adtoken='.$token.'&ad='.$admin_dir;
								}
								
								Tools::redirectAdmin($preview_url);
							} elseif (Tools::isSubmit('submitAdd'.$this->table.'AndStay') OR (Tools::isSubmit('salvatutto')) OR  ($id_image AND $id_image !== true)) // Save and stay on same form
							// Save and stay on same form
							if (Tools::isSubmit('submitAdd'.$this->table.'AndStay') ||(Tools::isSubmit('salvatutto')))
								Tools::redirectAdmin($currentIndex.'&id_product='.$object->id.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&addproduct&conf=4&tabs='.(int)(Tools::getValue('tabs')).'&token='.($token ? $token : $this->token));

							// Default behavior (save and back)
							Tools::redirectAdmin($currentIndex.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&conf=4&token='.($token ? $token : $this->token).'&onredirigeici');
							
						}
						
					}
					else
						$this->_errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.'</b> ('.Db::getInstance()->getMsgError().')';
				}
				else
					$this->_errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.'</b> ('.Tools::displayError('Cannot load object').')';
			}

			/* Add a new product */
			else
			{
				$object = new $this->className();
				$this->_removeTaxFromEcotax();
				$this->copyFromPost($object, $this->table);
				if ($object->add())
				{
					$this->updateAccessories($object);
					$this->updateAlternatives($object);
					if (!$this->updatePackItems($object))
						$this->_errors[] = Tools::displayError('An error occurred while adding products to the pack.');
					$this->updateDownloadProduct($object);
					if (!sizeof($this->_errors))
					{
						if (!$object->updateCategories($_POST['categoryBox']))
							$this->_errors[] = Tools::displayError('An error occurred while linking object.').' <b>'.$this->table.'</b> '.Tools::displayError('To categories');
						elseif (!$this->updateTags($languages, $object))
							$this->_errors[] = Tools::displayError('An error occurred while adding tags.');
						elseif ($id_image = $this->addProductImage($object))
						{
							Hook::addProduct($object);
							Search::indexation(false, $object->id);

							// Save and preview
							if (Tools::isSubmit('submitAddProductAndPreview'))
							{
								$preview_url = ($link->getProductLink($this->getFieldValue($object, 'id'), $this->getFieldValue($object, 'link_rewrite', (int)($cookie->id_lang)), Category::getLinkRewrite($this->getFieldValue($object, 'id_category_default'), (int)($cookie->id_lang))));
								if (!$object->active)
								{
									
									$admin_dir = dirname($_SERVER['PHP_SELF']);
									$admin_dir = substr($admin_dir, strrpos($admin_dir,'/') + 1);
									$token = Tools::encrypt('PreviewProduct'.$object->id);
									$preview_url .= '&adtoken='.$token.'&ad='.$admin_dir;
								}

								Tools::redirectAdmin($preview_url);
							}
							$this->aggiornaCoseNonPredefinite($object);
							
							
							if (Tools::getValue('resizer') == 'man' && isset($id_image) AND is_int($id_image) AND $id_image)
								Tools::redirectAdmin($currentIndex.'&id_product='.$object->id.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&id_image='.$id_image.'&imageresize&toconf=3&submitAddAndStay='.(Tools::isSubmit('submitAdd'.$this->table.'AndStay') ? 'on' : 'off').'&token='.(($token ? $token : $this->token)));
							// Save and stay on same form
							if (Tools::isSubmit('submitAdd'.$this->table.'AndStay'))
								Tools::redirectAdmin($currentIndex.'&id_product='.$object->id.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&addproduct&conf=3&tabs='.(int)(Tools::getValue('tabs')).'&token='.($token ? $token : $this->token));
							// Default behavior (save and back)
							Tools::redirectAdmin($currentIndex.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&conf=3&token='.($token ? $token : $this->token));
						}
					}
					else
						$object->delete();
				}
				else
					$this->_errors[] = Tools::displayError('An error occurred while creating object.').' <b>'.$this->table.'</b>';
			}
		}

	}

	private function _removeTaxFromEcotax()
	{
		$ecotaxTaxRate = Tax::getProductEcotaxRate();
		if ($ecotax = Tools::getValue('ecotax'))
			$_POST['ecotax'] = Tools::ps_round(Tools::getValue('ecotax') / (1 + $ecotaxTaxRate / 100), 6);
	}

	private function _applyTaxToEcotax($product)
	{
		$ecotaxTaxRate = Tax::getProductEcotaxRate();
		if ($product->ecotax)
			$product->ecotax = Tools::ps_round($product->ecotax * (1 + $ecotaxTaxRate / 100), 2);
	}

	/**
	 * Update product download
	 *
	 * @param object $product Product
	 */
	public function updateDownloadProduct($product)
	{
		/* add or update a virtual product */
		if (Tools::getValue('is_virtual_good') == 'true')
		{
			/*if (!Tools::getValue('virtual_product_name'))
			{
				$this->_errors[] = $this->l('the field').' <b>'.$this->l('display filename').'</b> '.$this->l('is required');
				return false;
			}*/
			if (Tools::getValue('virtual_product_nb_days') === false)
			{
				$this->_errors[] = $this->l('the field').' <b>'.$this->l('number of days').'</b> '.$this->l('is required');
				return false;
			}
			if (Tools::getValue('virtual_product_expiration_date') AND !Validate::isDate(Tools::getValue('virtual_product_expiration_date')))
			{
				$this->_errors[] = $this->l('the field').' <b>'.$this->l('expiration date').'</b> '.$this->l('is not valid');
				return false;
			}
			// The oos behavior MUST be "Deny orders" for virtual products
			if (Tools::getValue('out_of_stock') != 0)
			{
				$this->_errors[] = $this->l('The "when out of stock" behavior selection must be "deny order" for virtual products');
				return false;
			}

			$download = new ProductDownload(Tools::getValue('virtual_product_id'));
			$download->id_product          = $product->id;
			$download->display_filename    = Tools::getValue('virtual_product_name');
			$download->physically_filename = Tools::getValue('virtual_product_filename') ? Tools::getValue('virtual_product_filename') : ProductDownload::getNewFilename();
			$download->date_deposit        = date('Y-m-d H:i:s');
			$download->date_expiration     = Tools::getValue('virtual_product_expiration_date') ? Tools::getValue('virtual_product_expiration_date').' 23:59:59' : '';
			$download->nb_days_accessible  = Tools::getValue('virtual_product_nb_days');
			$download->nb_downloadable     = Tools::getValue('virtual_product_nb_downloable');
			$download->active              = 1;

			if ($download->save())
				return true;
		}
		else
		{
			Db::getInstance()->executeS('DELETE FROM product_download WHERE id_product = '.$product->id);
			/* unactive download product if checkbox not checked */
			if ($id_product_download = ProductDownload::getIdFromIdProduct($product->id))
			{
				/*$productDownload = new ProductDownload($id_product_download);
				$productDownload->display_filename = '';
				$productDownload->date_expiration = date('Y-m-d H:i:s', time()-1);
				$productDownload->active = 0;
				
				return $productDownload->save();*/
				$productDownload->delete();
			}
		}
		return false;
	}

	/**
	 * Update product accessories
	 *
	 * @param object $product Product
	 */
	public function updateAccessories($product)
	{
		$product->deleteAccessories();
		if ($accessories = Tools::getValue('inputAccessories'))
		{
			$accessories = str_replace(" ","",$accessories);
			echo $accessories."<br />";
			$accessories_id = array_unique(explode('-', $accessories));
			if (sizeof($accessories_id))
			{
				$product->changeAccessories($accessories_id);
			}
		}
	}
	
	public function updateAlternatives($product)
	{
		$product->deleteAlternatives();
		if ($accessories = Tools::getValue('inputAlternatives'))
		{
			$accessories = str_replace(" ","",$accessories);
			echo $accessories."<br />";
			$accessories_id = array_unique(explode('-', $accessories));
			if (sizeof($accessories_id))
			{
				$product->changeAlternatives($accessories_id);
			}
		}
	}
	
	/**
	 * Update product tags
	 *
	 * @param array Languages
	 * @param object $product Product
	 * @return boolean Update result
	 */
	public function updateTags($languages, $product)
	{
		$tagError = true;
		/* Reset all tags for THIS product */
		if (!Db::getInstance()->Execute('
		DELETE FROM `'._DB_PREFIX_.'product_tag`
		WHERE `id_product` = '.(int)($product->id)))
			return false;
		/* Assign tags to this product */
		
		if(!Tools::getValue('tags_5')) {
		Product::addTagAuto($product->id, 5);
		}
		else {
			$value = Tools::getValue('tags_5');
			Tag::addTags(5, (int)($product->id), $value);
			
		}
		
		return $tagError;
	}

	public function display($token = NULL)
	{
		global $currentIndex, $cookie;

		if (($id_category = (int)Tools::getValue('id_category')))
			$currentIndex .= '&id_category='.$id_category;
		$this->getList((int)($cookie->id_lang), !$cookie->__get($this->table.'Orderby') ? 'position' : NULL, !$cookie->__get($this->table.'Orderway') ? 'ASC' : NULL);
		$id_category = (Tools::getValue('id_category',1));
		if (!$id_category)
			$id_category = 1;
		echo '<h3>'.(!$this->_listTotal ? ($this->l('No products found')) : ($this->_listTotal.' '.($this->_listTotal > 1 ? $this->l('products') : $this->l('product')))).' '.
		$this->l('in category').' "'.stripslashes($this->_category->getName()).'"</h3>';
		
			echo '<a class="button" href="'.$currentIndex.'&id_category=9999999999&token='.($token!=NULL ? $token : $this->token).'">'.$this->l('Vedi tutti i prodotti del sito').'</a><br /><br />';
			
		if ($this->tabAccess['add'] === '1')
			echo '<a class="button" href="'.$currentIndex.'&id_category='.$id_category.'&add'.$this->table.'&token='.($token!=NULL ? $token : $this->token).'"> '.$this->l('Add a new product').'</a>';
			
		
		echo '<div style="margin:10px;">';
		$this->displayList($token);
		echo '</div>';
	}

	/**
	 * displayList show ordered list of current category
	 *
	 * @param mixed $token
	 * @return void
	 */
	public function displayList($token = NULL)
	{
		global $currentIndex;
		echo "<strong>EZ_</strong>: prodotti disponibili a magazzino Ezdirect<br />
		<strong>OEZ</strong>: prodotti ordinati al fornitore<br />
		<strong>AN_</strong>: prodotti disponibili su Allnet<br />
		<strong>AAN</strong>: prodotti in arrivo su Allnet
		";
		
		if (Tools::isSubmit('toglilink')) {
			
			foreach($_POST['productBox'] as $unlink) {
			
				Db::getInstance()->executeS("DELETE FROM category_product WHERE id_category = 1 AND id_product = ".$unlink."");
			
				//Db::getInstance()->executeS("DELETE FROM category_product WHERE id_category = ". $this->_category->id." AND id_product = ".$unlink."");
				
			
			}
		Tools::redirectAdmin($currentIndex.'&token='.($token ? $token : $this->token));
		}
		/* Display list header (filtering, pagination and column names) */
		$this->displayListHeader($token);
		if (!sizeof($this->_list))
			echo '<tr><td class="center" colspan="'.(sizeof($this->fieldsDisplay) + 2).'">'.$this->l('No items found').'</td></tr>';

		/* Show the content of the table */
		$this->displayListContent($token);
		$this->displayListPagination($token);
		
		if($this->_category->id == 1) 
		{
			echo "<input type='submit' class='button' value='Togli prodotti selezionati dalla home page' name='toglilink' />";
		
		}
		else
		{
			// echo "<input type='submit' class='button' value='Togli link (elimina i prodotti dalle categorie in cui è linkato, ma non dalla categoria principale)' name='toglilink' />";
			
		}
		
		if($this->_category->id != 1) 
		{
			echo '<p><input type="submit" class="button" name="submitDel'.$this->table.'" value="'.$this->l('Delete selection').'" onclick="return confirm(\''.$this->l('Delete selected items?', __CLASS__, TRUE, FALSE).'\');" /></p>';
		}	
		/* Close list table and submit button */
		$this->displayListFooter($token);
		
			
		
		
		
		
	}

	/**
	 * Build a categories tree
	 *
	 * @param array $indexedCategories Array with categories where product is indexed (in order to check checkbox)
	 * @param array $categories Categories to list
	 * @param array $current Current category
	 * @param integer $id_category Current category id
	 */
	public static function recurseCategoryForInclude($id_obj, $indexedCategories, $categories, $current, $id_category = 1, $id_category_default = NULL, $has_suite = array())
	{
		global $done;
		static $irow;

		if (!isset($done[$current['infos']['id_parent']]))
			$done[$current['infos']['id_parent']] = 0;
		$done[$current['infos']['id_parent']] += 1;

		$todo = sizeof($categories[$current['infos']['id_parent']]);
		$doneC = $done[$current['infos']['id_parent']];

		$level = $current['infos']['level_depth'] + 1;

		echo '
		<tr class="'.($irow++ % 2 ? 'alt_row' : '').'">
			<td>
				<input type="checkbox" name="categoryBox[]" class="categoryBox'.($id_category_default == $id_category ? ' id_category_default' : '').'" id="categoryBox_'.$id_category.'" value="'.$id_category.'"'.((in_array($id_category, $indexedCategories) OR ((int)(Tools::getValue('id_category')) == $id_category AND !(int)($id_obj))) ? ' checked="checked"' : '').' />
			</td>
			<td>
				'.$id_category.'
			</td>
			<td>';
			for ($i = 2; $i < $level; $i++)
				echo '<img src="../img/admin/lvl_'.$has_suite[$i - 2].'.gif" alt="" />';
			echo '<img src="../img/admin/'.($level == 1 ? 'lv1.gif' : 'lv2_'.($todo == $doneC ? 'f' : 'b').'.gif').'" alt="" /> &nbsp;
			<label for="categoryBox_'.$id_category.'" class="t">'.stripslashes($current['infos']['name']).'</label></td>
		</tr>';

		if ($level > 1)
			$has_suite[] = ($todo == $doneC ? 0 : 1);
		if (isset($categories[$id_category]))
			foreach ($categories[$id_category] AS $key => $row)
				if ($key != 'infos')
					self::recurseCategoryForInclude($id_obj, $indexedCategories, $categories, $categories[$id_category][$key], $key, $id_category_default, $has_suite);
	}

	public function displayErrors()
	{
		if ($this->includeSubTab('displayErrors'))
			;
		elseif ($nbErrors = sizeof($this->_errors))
		{
			echo '<div class="error">
				<img src="../img/admin/error2.png" />
				'.$nbErrors.' '.($nbErrors > 1 ? $this->l('errors') : $this->l('error')).'
				<ol>';
			foreach ($this->_errors AS $error)
				echo '<li>'.$error.'</li>';
			echo '
				</ol>
			</div>';
		}
	}

	private function _displayDraftWarning($active)
	{
		return '<div class="warn draft" style="'.($active ? 'display:none' : '').'">
				<p>
				<span style="float: left">
				<img src="../img/admin/warn2.png" />
				'.$this->l('Your product will be saved as draft').'
				</span>
				<span style="float:right"><a href="#" class="button" style="display: block" onclick="submitAddProductAndPreview()" >'.$this->l('Save and preview').'</a></span>
				<input type="hidden" name="fakeSubmitAddProductAndPreview" id="fakeSubmitAddProductAndPreview" />
				<br class="clear" />
				</p>
	 		</div>';
	}

	public function displayForm($isMainTab = true)
	{
		global $currentIndex, $link, $cookie;
		parent::displayForm();

		if ($id_category_back = (int)(Tools::getValue('id_category')))
			$currentIndex .= '&id_category='.$id_category_back;

		if (!($obj = $this->loadObject(true)))
			return;
		$currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));

		if ($obj->id)
			$currentIndex .= '&id_product='.$obj->id;

		if(isset($_GET['nomeallegatononvalido'])) {
		
			echo "<pre class='error'><strong>ERRORE</strong>: NOME ALLEGATO NON VALIDO O NON INSERITO</pre><br /><br />";
		
		}
		
		else {
			
		}
		
		echo '
		<h3>'.$this->l('Current product:').' <span id="current_product" style="font-weight: normal;">'.$this->l('no name').'</span></h3>';
		
		$scadenze_speciali_vendita = Db::getInstance()->executeS("SELECT * FROM specific_price WHERE id_product = ".Tools::getValue('id_product')." AND specific_price.to != '0000-00-00 00:00:00' AND specific_price.to < '".date('Y-m-d H:i:s')."'");

		
		foreach($scadenze_speciali_vendita as $scadenza_vendita)
		{
			echo "<div class='error'>ATTENZIONE: scaduto in data ".date("d/m/Y", strtotime($scadenza_vendita['to']))." prezzo speciale VENDITA  di euro ".($scadenza_vendita['reduction_type'] == 'amount' ? str_replace(".",",",($scadenza_vendita['price'] - $scadenza_vendita['reduction'])) : str_replace(".",",",($scadenza_vendita['price'] - ($scadenza_vendita['price']*$scadenza_vendita['reduction'])))).". Consulta il tab OFFERTE SPECIALI per informazioni.</div>";
		}	
		
		$scadenze_speciali_acquisto = Db::getInstance()->executeS("SELECT * FROM specific_price_wholesale WHERE id_product = ".Tools::getValue('id_product')." AND specific_price_wholesale.to != '0000-00-00 00:00:00' AND specific_price_wholesale.to < '".date('Y-m-d H:i:s')."'");

		foreach($scadenze_speciali_acquisto as $scadenza_acquisto)
		{
			echo "<div class='error'>ATTENZIONE: scaduto in data ".date("d/m/Y", strtotime($scadenza_acquisto['to']))." prezzo speciale ACQUISTO di euro ".str_replace(".",",",$scadenza_acquisto['wholesale_price']).". Consulta il tab OFFERTE SPECIALI per informazioni.</div>";
		}	
		
		echo '
		<script type="text/javascript">
			var pos_select = '.(($tab = Tools::getValue('tabs')) ? $tab : '0').';
			'.$this->initCombinationImagesJS().'
			$(document).ready(function(){
				$(\'#id_mvt_reason\').change(function(){
					updateMvtStatus($(this).val());
				});
				updateMvtStatus($(this).val());
			});
			function updateMvtStatus(id_mvt_reason)
			{
				if (id_mvt_reason == -1)
					return $(\'#mvt_sign\').hide();
				if ($(\'#id_mvt_reason option:selected\').attr(\'rel\') == -1)
					$(\'#mvt_sign\').html(\'<img src="../img/admin/arrow_down.png" /> '.$this->l('Decrease your stock').'\');
				else
					$(\'#mvt_sign\').html(\'<img src="../img/admin/arrow_up.png" /> '.$this->l('Increase your stock').'\');
				$(\'#mvt_sign\').show();
			}
		</script>
		<script src="../js/tabpane.js" type="text/javascript"></script>
		
		<link type="text/css" rel="stylesheet" href="../css/tabpane.css" />
		
		';
		
		
		echo '<form action="'.$currentIndex.'&token='.Tools::getValue('token').'" method="post" enctype="multipart/form-data" name="product" id="product" onsubmit="return validate_form_product()">
		<script type="text/javascript" src="messi.js"></script>
		<script type="text/javascript">
		
		try {
		$(document).ready(function () {
			
			warn = false;
			
			$("#product").submit(function () {
				warn=false;
			});
			
			$("#product").change( function(){
				warn=true;
			 });
	
	 
		$(window).bind(\'beforeunload\', function(){ 
			if (warn) {
		
				var salvamodifiche = window.confirm("Ci sono modifiche non salvate. Clicca su OK per uscire e salvare, clicca su Annulla per uscire senza salvare."); 
			
				if (salvamodifiche) {
			
					$("#salvatutto").trigger("click");
					alert("Modifiche salvate correttamente.");
				
				} else {

					//niente
				}

		
		
			}
   
		});
	});
	} 

	catch(e) {
	}


	
</script>
			'.$this->_displayDraftWarning($obj->active).'

			<input type="hidden" name="tabs" id="tabs" value="0" />
			<input type="hidden" name="id_category" value="'.(($id_category = Tools::getValue('id_category')) ? (int)($id_category) : '0').'">
			<div class="tab-pane" id="tabPane1">';
				/* Tabs */
		$this->displayFormInformations($obj, $currency);
		$this->displayFormImages($obj, $this->token);
		$this->displayFormPrices($obj, $this->token);
		$this->displayFormBundles($obj, $this->token);
		$countAttributes = (int)Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'product_attribute WHERE id_product = '.(int)$obj->id);
		$countAttachments = (int)Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'product_attachment WHERE id_product = '.(int)$obj->id);
		if ($obj->id)
			echo '';
			
			
			if($cookie->id_employee == 6 || $cookie->id_employee == 22)
				echo '<div class="tab-page" id="step4"><h4 class="tab"><a onclick="">10. Opzioni</a></h4></div>';
			
			echo '<div class="tab-page" id="step5"><h4 class="tab"><a onclick="">5. '.$this->l('Features').'</a></h4></div>';
			echo '<div class="tab-page" id="step8"><h4 class="tab"><a onclick="">6. '.$this->l('Prezzi acquisto speciali').'</a></h4>';
			
			$this->_displaySpecificPriceAdditionForm($defaultCurrency, $shops, $currencies, $countries, $groups, 'acq');
			$this->_displaySpecificPriceModificationForm($defaultCurrency, $shops, $currencies, $countries, $groups, 'acq');
			
			
			
			
			
			
			
			echo '</div>';
			
			
			/*echo '<div class="tab-page" id="step6"><h4 class="tab"><a onclick="">6. '.$this->l('Customization').'</a></h4></div>';*/
		$this->displayFormAttachments($obj, $this->token, $cookie->id_lang);	
			
			$this->displayFormEsolver($obj, $this->token, $cookie->id_lang);	
			
			$this->displayFormStorico($obj, $this->token, $cookie->id_lang);	
			
		echo '<script type="text/javascript">
					var toload = new Array();
					toload[3] = true;
					toload[4] = true;
					toload[5] = true;
					toload[6] = true;
					toload[7] = true;
					function loadTab(id) {';
		if ($obj->id)
		{
			echo '	if (toload[id]) {
							toload[id] = false;
							$.ajax({
								url: "'.dirname($currentIndex).'/ajax.php",
								data: {
									ajaxProductTab: id, id_product: '.$obj->id.',
									token: \''.Tools::getValue('token').'\',
									id_category: '.(int)(Tools::getValue('id_category')).'
									'.(Tools::getIsset('idspecificprice') ? ', idspecificprice: '.Tools::getValue('idspecificprice') : '').'
								},
								cache: false,
								type: \'POST\',
								success: function(rep) {
									$("#step" + id).html(rep);var languages = new Array();
									if (id == 3)
										populate_attrs();
									if (id == 7)
									{
										$(\'#addAttachment\').click(function() {
											return !$(\'#selectAttachment1 option:selected\').remove().appendTo(\'#selectAttachment2\');
										});
										$(\'#removeAttachment\').click(function() {
											return !$(\'#selectAttachment2 option:selected\').remove().appendTo(\'#selectAttachment1\');
										});
										$(\'#product\').submit(function() {
											$(\'#selectAttachment1 option\').each(function(i) {
												$(this).attr("selected", "selected");
											});
										});
									}
								}
							})
						}';
		}
		echo '	}
				</script>
			</div>
			<div class="clear"></div>
			<input type="hidden" name="id_product_attribute" id="id_product_attribute" value="0" />
			<br />'.$this->_displayDraftWarning($obj->active).'
			
			
			<input type="submit" class="button" value="SALVA TUTTO (descrizioni, prezzi, specifiche, eSolver)" name="salvatutto" id="salvatutto" />
			
		</form>';

		if (Tools::getValue('id_category') > 1)
		{
			$productIndex = preg_replace('/(&id_product=[0-9]*)/', '', $currentIndex);
			echo '<br /><br />
			<a href="'.$productIndex.($this->token ? '&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)) : '').'">
				<img src="../img/admin/arrow2.gif" /> '.$this->l('Back to the category').'
			</a><br />';
		}
	}

	function displayFormPrices($obj, $languages)
	{
		global $cookie, $currentIndex;
		
		echo '
		<div class="tab-page" id="step3">
				<h4 class="tab">3. Offerte speciali</h4>';

		if ($obj->id)
		{
			$shops = Shop::getShops();
			$currencies = Currency::getCurrencies();
			$countries = Country::getCountries((int)($cookie->id_lang));
			$groups = Group::getGroups((int)($cookie->id_lang));
			$defaultCurrency = new Currency((int)(Configuration::get('PS_CURRENCY_DEFAULT')));
			
			$this->_displaySpecificPriceAdditionForm($defaultCurrency, $shops, $currencies, $countries, $groups);
			
			$this->_displaySpecificPriceModificationForm($defaultCurrency, $shops, $currencies, $countries, $groups);
		}
		else
			echo '<b>'.$this->l('You must save this product before adding specific prices').'.</b>';
			
		echo '</div>';
	}

	private function _getFinalPrice($specificPrice, $productPrice, $taxRate)
	{
		$price = Tools::ps_round((float)($specificPrice['price']) ? $specificPrice['price'] : $productPrice, 2);
		if (!(float)($specificPrice['reduction']))
			return (float)($specificPrice['price']);
		return ($specificPrice['reduction_type'] == 'amount') ? ($price - $specificPrice['reduction'] / (1 + $taxRate / 100)) : ($price - $price * $specificPrice['reduction']);
	}

	protected function _displaySpecificPriceModificationForm($defaultCurrency, $shops, $currencies, $countries, $groups, $type = 'vnd')
	{
		
		global $currentIndex;
		
		if (!($obj = $this->loadObject()))
			return;
		
		if($type == 'vnd')
			$specificPrices = Db::getInstance()->executeS('SELECT * FROM specific_price WHERE id_product = '.$obj->id.' AND specific_price_name = ""');
		else
			$specificPrices = Db::getInstance()->executeS('SELECT * FROM specific_price_wholesale WHERE id_product = '.$obj->id.'');
		
		$shops = Shop::getShops();
		$currencies = Currency::getCurrencies();
		$countries = Country::getCountries((int)($cookie->id_lang));
		$groups = Group::getGroups((int)($cookie->id_lang));
		$defaultCurrency = new Currency((int)(Configuration::get('PS_CURRENCY_DEFAULT')));
			
		foreach($specificPrices as $specificPrice)
			$this->_displaySpecificPriceAdditionForm($defaultCurrency, $shops, $currencies, $countries, $groups, $type, $specificPrice['id_specific_price']);
		
		/*
		$specificPricePriorities = SpecificPrice::getPriority((int)($obj->id));
		$default_country = new Country((int)Configuration::get('PS_COUNTRY_DEFAULT'));
		$sp_string = array();
		
		$specificPriceWholesale = Db::getInstance()->executeS('SELECT spw.*, "wholesale" as reduction_type FROM specific_price_wholesale spw WHERE id_product = '.$obj->id);
		
		foreach($specificPriceWholesale as $sp)
			$specificPrices[] = $sp;

		$taxRate = TaxRulesGroup::getTaxesRate($obj->id_tax_rules_group, Configuration::get('PS_COUNTRY_DEFAULT'), 0, 0);

		$tmp = array();
		foreach ($shops as $shop)
			$tmp[$shop['id_shop']] = $shop;
		$shops = $tmp;

		$tmp = array();
		foreach ($currencies as $currency)
			$tmp[$currency['id_currency']] = $currency;
		$currencies = $tmp;

		$tmp = array();
		foreach ($countries as $country)
			$tmp[$country['id_country']] = $country;
		$countries = $tmp;

		$tmp = array();
		foreach ($groups as $group)
			$tmp[$group['id_group']] = $group;
		$groups = $tmp;

		if($type == 'vnd')
		{	
		echo '
		<h4 style="font-size:16px; text-align:center">Prezzi speciali VENDITA</h4>';
		}
		
		echo '
		<script type="text/javascript">
		function calcolaMargine2(vnd,acq,marg) 
			{
				var numprice = document.getElementById(vnd).value;
				if(numprice == "")
					numprice = 0;
					
				var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
			
				var numacquisto = document.getElementById(acq).value;
				if(numacquisto == "")
					numacquisto = 0;
					
				var acquisto = parseFloat(numacquisto.replace(/\s/g, "").replace(",", "."));
				
				var marginalitatotale = (((price - acquisto)*100) / price);
			
				marginalitatotale = marginalitatotale.toFixed(2);
				
				var guadagno = price-acquisto;
				guadagno = guadagno.toFixed(2);
			
				document.getElementById(marg).innerHTML=marginalitatotale.replace(".",",") + "%";
			}		
			</script>
		<strong>(NB: per non avere scadenza/quantit&agrave; disponibili, inserire spazio vuoto nel campo quantit&agrave;)</strong><br />';
		
		$table_sp_head = '
		<table style="text-align: center;width:100%" class="table" cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<th class="cell border" style="display:none">Tipo</th>
					<th class="cell border" style="width: 12%; display:none">'.$this->l('Currency').'</th>
					<th class="cell border" style="width: 11%; display:none">'.$this->l('Country').'</th>
					<th class="cell border" style="text-align:center; width: 10%;">'.$this->l('Group').'</th>
					<th class="cell border" style="text-align:center; width: 14%;">'.$this->l('Prezzo spec.').'</th>
					<!-- <th class="cell border" style="text-align:center; width: 14%;">'.$this->l('Reduction').'</th> -->
					<th class="cell border" style="text-align:center; width: 13%;">'.$this->l('Da').'</th>
					<th class="cell border" style="text-align:center; width: 13%;">'.$this->l('A').'</th>
					<th class="cell border" style="text-align:center; width: 15%;">'.$this->l('Pz. a prezzo speciale').'</th>
					<th class="cell border" style="text-align:center; width: 7%;">'.$this->l('MAG').'</th>
					<th class="cell border" style="text-align:center; width: 12%;">'.$this->l('Acq.').'</th>
					<!-- <th class="cell border" style="text-align:center; width: 15%;">'.$this->l('Disp.').'</th> -->
					<th class="cell border" style="text-align:center; width: 10%;">'.$this->l('From (quantity)').'</th>
					<th class="cell border" style="text-align:center; width: 16%;">'.$this->l('Vnd. standard').'</th>
					<th class="cell border" style="text-align:center; width: 15%;">'.$this->l('Marg.').'</th>
					<th class="cell border" style="text-align:center; width: 2%;">'.$this->l('Action').'</th>
				</tr>
			</thead>
			<tbody>';
		
		$table_sp_head_w = '
		<table style="text-align: center;width:100%" class="table" cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<th class="cell border" style="text-align:center; width: 14%;">'.$this->l('Prezzo').' '.($default_country->display_tax_label ? $this->l('(i.e.)') : '').'</th>
					<th class="cell border" style="text-align:center; width: 14%;">'.$this->l('Listino').'</th>
					<th class="cell border" style="text-align:center; width: 12%;">'.$this->l('Sc. 1').'</th>
					<th class="cell border" style="text-align:center; width: 12%;">'.$this->l('Sc. 2').'</th>
					<th class="cell border" style="text-align:center; width: 12%;">'.$this->l('Sc. 3').'</th>
					<th class="cell border" style="text-align:center; width: 13%;">'.$this->l('Da').'</th>
					<th class="cell border" style="text-align:center; width: 13%;">'.$this->l('A').'</th>
					<th class="cell border" style="text-align:center; width: 15%;">'.$this->l('Pz. a prezzo speciale').'</th>
					<th class="cell border" style="text-align:center; width: 7%;">'.$this->l('MAG').'</th>
					<th class="cell border" style="text-align:center; width: 12%;">'.$this->l('Acq.').'</th>
					<th class="cell border" style="text-align:center; width: 15%;">'.$this->l('Marg.').'</th>
					<th class="cell border" style="text-align:center; width: 2%;">'.$this->l('Action').'</th>
				</tr>
			</thead>
			<tbody>';
			
		if($type == 'vnd')		
			echo $table_sp_head;
		
		if (!is_array($specificPrices) OR !sizeof($specificPrices))
			echo '
				<tr>
					<td colspan="9">'.$this->l('No specific prices').'</td>
				</tr>';
		else
		{
			$i = 0;
			foreach ($specificPrices as $specificPrice)
			{	
				$sp_string[$specificPrice['id_specific_price']] = '';
				if($specificPrice['specific_price_name'] != 'sc_qta_1' && $specificPrice['specific_price_name'] != 'sc_qta_2' && $specificPrice['specific_price_name'] != 'sc_qta_3' && $specificPrice['specific_price_name'] != 'sc_riv_1' && $specificPrice['specific_price_name'] != 'sc_riv_2'  && $specificPrice['specific_price_name'] != 'sc_riv_3') {
					$current_specific_currency = $currencies[($specificPrice['id_currency'] ? $specificPrice['id_currency'] : $defaultCurrency->id)];
					if ($specificPrice['reduction_type'] == 'percentage')
						$reduction = ($specificPrice['reduction'] * 100).' %';
					else
						$reduction = Tools::displayPrice(Tools::ps_round($specificPrice['reduction'], 2), $current_specific_currency);

					if ($specificPrice['from'] == '0000-00-00 00:00:00' AND $specificPrice['to'] == '0000-00-00 00:00:00')
						$period = $this->l('Unlimited');
					else
						$period = $this->l('From').' '.($specificPrice['from'] != '0000-00-00 00:00:00' ? $specificPrice['from'] : '0000-00-00 00:00:00').'<br />'.$this->l('To').' '.($specificPrice['to'] != '0000-00-00 00:00:00' ? $specificPrice['to'] : '0000-00-00 00:00:00');
					$sp_string[$specificPrice['id_specific_price']] .= '
					<tr '.($i%2 ? 'class="alt_row"' : '').'>';
					$sp_string[$specificPrice['id_specific_price']] .= '
					<input type="hidden" value="'.$specificPrice['id_specific_price'].'" name="id_specific_price_'.$specificPrice['id_specific_price'].'" />
					<td class="cell border" style="display:none">'.$specificPrice['specific_price_name'].'</td>
						
						
						<td class="cell border" style="display:none">';
						
					if($specificPrice['reduction_type'] != 'wholesale')
					{
						$sp_string[$specificPrice['id_specific_price']] .= '<select style="width:70px" name="sp_id_currency_'.$specificPrice['id_specific_price'].'">
						<option value="0"'; if($specificPrice['id_currency'] == 0) { $sp_string[$specificPrice['id_specific_price']] .= ' selected="selected" '; } else { }
						$sp_string[$specificPrice['id_specific_price']] .= '>'.$this->l('All currencies').'</option>';
						$currencies = Currency::getCurrencies();
						foreach ($currencies as $currency) {
							$sp_string[$specificPrice['id_specific_price']] .= '<option value="'.(int)($currency['id_currency']).'"'; 
							if ($specificPrice['id_currency'] == (int)($currency['id_currency'])) { $sp_string[$specificPrice['id_specific_price']] .= ' selected="selected" '; } else { }
							$sp_string[$specificPrice['id_specific_price']] .= '>'.Tools::htmlentitiesUTF8($currency['name']).'</option>';
						}
						$sp_string[$specificPrice['id_specific_price']] .= '
						</select>';
						
						
						$sp_string[$specificPrice['id_specific_price']] .= '</td>
						
						
						
						<td class="cell border" style="display:none">
						<select style="width:110px" name="sp_id_country_'.$specificPrice['id_specific_price'].'">
						<option value="0"'; if($specificPrice['id_country'] == 0) { $sp_string[$specificPrice['id_specific_price']] .= ' selected="selected" '; } else { }
							$sp_string[$specificPrice['id_specific_price']] .= '>'.$this->l('All countries').'</option>';
						$countries = Country::getCountries(5);
						foreach ($countries as $country) {
							$sp_string[$specificPrice['id_specific_price']] .= '<option value="'.(int)($country['id_country']).'"'; 
							if ($specificPrice['id_country'] == (int)($country['id_country'])) { $sp_string[$specificPrice['id_specific_price']] .= ' selected="selected" '; } else { }
							$sp_string[$specificPrice['id_specific_price']] .= '>'.Tools::htmlentitiesUTF8($country['name']).'</option>';
						}
						$sp_string[$specificPrice['id_specific_price']] .= '
						</select>';
						
						
						
						$sp_string[$specificPrice['id_specific_price']] .= '</td>
						<td class="cell border">
						
						<select style="width:90px" name="sp_id_group_'.$specificPrice['id_specific_price'].'">
						<option value="0"'; if($specificPrice['id_group'] == 0) { $sp_string[$specificPrice['id_specific_price']] .= ' selected="selected" '; } else { }
						$sp_string[$specificPrice['id_specific_price']] .= '>'.$this->l('All groups').'</option>';
						$groups = Group::getGroups(5);
						foreach ($groups as $group) {
							$sp_string[$specificPrice['id_specific_price']] .= '<option value="'.(int)($group['id_group']).'"'; 
							if ($specificPrice['id_group'] == (int)($group['id_group'])) { $sp_string[$specificPrice['id_specific_price']] .= ' selected="selected" '; } else { }
							$sp_string[$specificPrice['id_specific_price']] .= '>'.Tools::htmlentitiesUTF8($group['name']).'</option>';
						}
						$sp_string[$specificPrice['id_specific_price']] .= '
						</select>';
					}
					else
					{
						
					}
						
						if($specificPrice['wholesale_price'] > 0)
							$sp_wholesale_price = $specificPrice['wholesale_price'];
						else
							$sp_wholesale_price = $obj->wholesale_price;
						
						
						includeDatepicker3(array('sp_from_'.$specificPrice['id_specific_price'], 'sp_to_'.$specificPrice['id_specific_price']), true);
						
						$color_spec = '';
						
						if($specificPrice['reduction_type'] == 'wholesale')
						{
							$speciale_o = Product::trovaPrezzoSpeciale($obj->id, 1, 0);
							
							if($speciale_o != 0 && $speciale_o < $specificPrice['price'])
							{
								$specificPrice['price'] = $speciale_o;
								$color_spec = 'red';
							}	
						}
						
						$sp_string[$specificPrice['id_specific_price']] .= '</td>
						
						<td class="cell border"><input readonly="readonly" type="text" onkeyup="calcolaMargine2(\'sp_price_'.$specificPrice['id_specific_price'].'\',\'sp_wholesale_price_'.$specificPrice['id_specific_price'].'\',\'sp_margin_'.$specificPrice['id_specific_price'].'\')" name="sp_price_'.$specificPrice['id_specific_price'].'" '.($specificPrice['reduction_type'] != 'wholesale' ? '' : 'readonly="readonly"').' id="sp_price_'.$specificPrice['id_specific_price'].'" value="'.number_format($specificPrice['price'], 2,",","").'" size="7" style="text-align:right '.($color_spec != '' ? ';color:'.$color_spec : '').' " /> €</td>';
						
						if($specificPrice['reduction_type'] != 'wholesale')
						{
							
							// $sp_string[$specificPrice['id_specific_price']] .= '<td class="cell border"><input type="text" name="sp_reduction_'.$specificPrice['id_specific_price'].'" id="sp_reduction_'.$specificPrice['id_specific_price'].'" value="'.number_format(($specificPrice['reduction'] * 100), 2,",","").'" size="2" style="text-align:right" /> %</td>';
						
						}
						else
						{
							$sp_string[$specificPrice['id_specific_price']] .= '<td class="cell border"><input type="text" readonly="readonly" value="'.number_format(($obj->listino), 2,",","").'"  size="7" style="text-align:right" /> </td>';
							
							$sp_string[$specificPrice['id_specific_price']] .= '<td class="cell border"><input type="text"  readonly="readonly" name="sp_reductionw1_'.$specificPrice['id_specific_price'].'" id="sp_reductionw1_'.$specificPrice['id_specific_price'].'" onkeyup="calcScontiAcquisto(\'sp_reductionw1_'.$specificPrice['id_specific_price'].'\',\'sp_reductionw2_'.$specificPrice['id_specific_price'].'\', \'sp_reductionw3_'.$specificPrice['id_specific_price'].'\', \'sp_listino_'.$specificPrice['id_specific_price'].'\', \'sp_wholesale_price_'.$specificPrice['id_specific_price'].'\'); calcolaMargine2(\'sp_price_'.$specificPrice['id_specific_price'].'\',\'sp_wholesale_price_'.$specificPrice['id_specific_price'].'\',\'sp_margin_'.$specificPrice['id_specific_price'].'\')" value="'.number_format(($specificPrice['reduction_1']), 2,",","").'" size="4" style="text-align:right" /> %</td>';
							
							$sp_string[$specificPrice['id_specific_price']] .= '<td class="cell border"><input type="text"  readonly="readonly" name="sp_reductionw2_'.$specificPrice['id_specific_price'].'" id="sp_reductionw2_'.$specificPrice['id_specific_price'].'" onkeyup="calcScontiAcquisto(\'sp_reductionw1_'.$specificPrice['id_specific_price'].'\',\'sp_reductionw2_'.$specificPrice['id_specific_price'].'\', \'sp_reductionw3_'.$specificPrice['id_specific_price'].'\', \'sp_listino_'.$specificPrice['id_specific_price'].'\', \'sp_wholesale_price_'.$specificPrice['id_specific_price'].'\');  calcolaMargine2(\'sp_price_'.$specificPrice['id_specific_price'].'\',\'sp_wholesale_price_'.$specificPrice['id_specific_price'].'\',\'sp_margin_'.$specificPrice['id_specific_price'].'\')" value="'.number_format(($specificPrice['reduction_2']), 2,",","").'" size="4" style="text-align:right" /> %</td>';
							
							$sp_string[$specificPrice['id_specific_price']] .= '<td class="cell border"><input type="text"  readonly="readonly" name="sp_reductionw3_'.$specificPrice['id_specific_price'].'" id="sp_reductionw3_'.$specificPrice['id_specific_price'].'" onkeyup="calcScontiAcquisto(\'sp_reductionw1_'.$specificPrice['id_specific_price'].'\',\'sp_reductionw2_'.$specificPrice['id_specific_price'].'\', \'sp_reductionw3_'.$specificPrice['id_specific_price'].'\', \'sp_listino_'.$specificPrice['id_specific_price'].'\', \'sp_wholesale_price_'.$specificPrice['id_specific_price'].'\');  calcolaMargine2(\'sp_price_'.$specificPrice['id_specific_price'].'\',\'sp_wholesale_price_'.$specificPrice['id_specific_price'].'\',\'sp_margin_'.$specificPrice['id_specific_price'].'\')" value="'.number_format(($specificPrice['reduction_3']), 2,",","").'" size="4" style="text-align:right" /> %</td>';
							
						}
						
						$sp_string[$specificPrice['id_specific_price']] .= '<td class="cell border"><input type="text"  readonly="readonly" id="sp_from_'.$specificPrice['id_specific_price'].'" name="sp_from_'.$specificPrice['id_specific_price'].'" value="'.date("d-m-Y",strtotime(($specificPrice['from'] == '0000-00-00 00:00:00' || $specificPrice['from'] == '1970-01-01 01:00:00' ? date('Y-01-01 00:00:00') : $specificPrice['from']))).'" size="11" style="text-align:right" /></td>
						
							<td class="cell border">
							<input type="hidden" value="'.$obj->listino.'" id="sp_listino_'.$specificPrice['id_specific_price'].'" />
							<input type="text" id="sp_to_'.$specificPrice['id_specific_price'].'"  readonly="readonly" name="sp_to_'.$specificPrice['id_specific_price'].'" value="'.date("d-m-Y",strtotime($specificPrice['to'])).'" size="11"  style="text-align:right" /></td>
						
						<td class="cell border"><input type="text" style="text-align:right"  readonly="readonly" name="sp_pieces_'.$specificPrice['id_specific_price'].'" value="'.Db::getInstance()->getValue("SELECT pieces FROM specific_price_".($specificPrice['reduction_type'] != 'wholesale' ? 'pieces' : 'wholesale')." WHERE id_specific_price = ".$specificPrice['id_specific_price']."").'" size="3" /></td>
						
						<td class="cell border">'.$obj->stock_quantity.'</td>';
						
						$color_spec_w = '';
						
						if($specificPrice['reduction_type'] != 'wholesale')
						{
							$speciale_ow = Db::getInstance()->getValue("SELECT wholesale_price FROM specific_price_wholesale spw WHERE id_product = ".$obj->id." AND spw.to != '0000-00-00 00:00:00' AND spw.to > '".date("Y-m-d H:i:s")."' ");
							
							if($speciale_ow != 0 && $speciale_ow < $specificPrice['price'])
							{
								$sp_wholesale_price = $speciale_ow;
								$color_spec_w = 'blue';
							}	
						}
						
						$sp_string[$specificPrice['id_specific_price']] .= '
						
						<td class="cell border"><input  readonly="readonly" type="text" onkeyup="
						'.($specificPrice['reduction_type'] != 'wholesale' ? '' : "document.getElementById('sp_reductionw2_".$specificPrice['id_specific_price']."').value = 0; document.getElementById('sp_reductionw3_".$specificPrice['id_specific_price']."').value = 0; calcSconto('sp_listino_".$specificPrice['id_specific_price']."', 'sp_wholesale_price_".$specificPrice['id_specific_price']."', 'sp_reductionw1_".$specificPrice['id_specific_price']."');").' calcolaMargine2(\'sp_price_'.$specificPrice['id_specific_price'].'\',\'sp_wholesale_price_'.$specificPrice['id_specific_price'].'\',\'sp_margin_'.$specificPrice['id_specific_price'].'\')" name="sp_wholesale_price_'.$specificPrice['id_specific_price'].'" id="sp_wholesale_price_'.$specificPrice['id_specific_price'].'" value="'.number_format(($sp_wholesale_price), 2,",","").'" size="6" style="text-align:right '.($color_spec_w != '' ? ';color:'.$color_spec_w : '').' " /></td>
						
						<!-- <td class="cell border">'.Db::getInstance()->getValue("SELECT stock FROM specific_price_pieces WHERE id_specific_price = ".$specificPrice['id_specific_price']."").'</td> -->';
						
						if($specificPrice['reduction_type'] != 'wholesale')
						{
							
							$sp_string[$specificPrice['id_specific_price']] .= '<td><input  readonly="readonly" type="text" name="sp_from_quantity_'.$specificPrice['id_specific_price'].'" value="'.$specificPrice['from_quantity'].'" size="3" style="text-align:right" /></td>';
						
						}
						
						else
						{
							
						}
						
						if($specificPrice['reduction_type'] != 'wholesale')
						{
						$sp_string[$specificPrice['id_specific_price']] .= '<td class="cell border"><b>'.Tools::displayPrice(Tools::ps_round((float)($obj->price), 2), $current_specific_currency).'</b></td>
						
						<td class="cell border"><b id="sp_margin_'.$specificPrice['id_specific_price'].'">'.number_format(((($this->_getFinalPrice($specificPrice, (float)($obj->price), $taxRate))-$sp_wholesale_price)*100)/($this->_getFinalPrice($specificPrice, (float)($obj->price), $taxRate)),2,",","").'%</b></td>';
						}
						else
						{
							$sp_string[$specificPrice['id_specific_price']] .= '<td class="cell border"><b id="sp_margin_'.$specificPrice['id_specific_price'].'">'.number_format((($specificPrice['price']-$sp_wholesale_price)*100)/$specificPrice['price'],2,",","").'%</b></td>';
							
						}
						
						
						$sp_string[$specificPrice['id_specific_price']] .= '<td class="cell border">
						<input type="hidden" id="sp_reduction_type_'.$specificPrice['id_specific_price'].'" name="sp_reduction_type_'.$specificPrice['id_specific_price'].'" value="'.$specificPrice['reduction_type'].'" />
						<a href="'.$currentIndex.(Tools::getValue('id_category') ? '&id_category='.(int)Tools::getValue('id_category') : '').'&id_product='.(int)(Tools::getValue('id_product')).'&updateproduct&deleteSpecificPrice'.($specificPrice['reduction_type'] != 'wholesale' ? '' : 'Wholesale').'&id_specific_price='.(int)($specificPrice['id_specific_price']).'&token='.Tools::getValue('token').'" onclick="javascript: var sure=window.confirm(\'Sei sicuro/a di cancellare il prezzo speciale?\'); if (sure) { return true; } else { return false; }"><img src="../img/admin/delete.gif" alt="'.$this->l('Delete').'" /></a>
						<a href="'.$currentIndex.(Tools::getValue('id_category') ? '&id_category='.(int)Tools::getValue('id_category') : '').'&id_product='.(int)(Tools::getValue('id_product')).'&updateproduct&updateSpecificPrice&tabs='.($specificPrice['reduction_type'] != 'wholesale' ? '2' : '5').'&type='.($specificPrice['reduction_type'] != 'wholesale' ? 'vnd' : 'acq').'&idspecificprice='.(int)($specificPrice['id_specific_price']).'&token='.Tools::getValue('token').'"><img src="../img/admin/edit.gif" alt="'.$this->l('Edit').'" /></a>
					
						</td>
						
					</tr>';
					$i++;
				}
			}
		}
		$table_sp_footer .='
			</tbody>
		</table>';
		
		if($type == 'vnd')
		{	
			foreach ($specificPrices as $specificPrice)
			{
				if($specificPrice['reduction_type'] != 'wholesale')
				echo $sp_string[$specificPrice['id_specific_price']];
			}
			
			echo $table_sp_footer;
		}
		else
		{
			echo '<hr /><h4 style="font-size:16px; text-align:center">Prezzi speciali ACQUISTO</h4>';
			
			echo $table_sp_head_w;
			foreach ($specificPrices as $specificPrice)
			{
				if($specificPrice['reduction_type'] == 'wholesale')
				echo $sp_string[$specificPrice['id_specific_price']];
			}
			
			echo $table_sp_footer;
		}
		

		echo '
		<script type="text/javascript">
			var currencies = new Array();
			currencies[0] = new Array();
			currencies[0]["sign"] = "'.$defaultCurrency->sign.'";
			currencies[0]["format"] = "'.$defaultCurrency->format.'";
			';
			foreach ($currencies as $currency)
			{
				echo '
				currencies['.$currency['id_currency'].'] = new Array();
				currencies['.$currency['id_currency'].']["sign"] = "'.$currency['sign'].'";
				currencies['.$currency['id_currency'].']["format"] = '.$currency['format'].';
				';
			}
			echo '
		</script>
		';
/*
		echo '
		<hr />
		<h4>'.$this->l('Priorities management').'</h4>

		<div class="hint clear" style="display:block;">
				'.$this->l('Sometimes one customer could fit in multiple rules, priorities allows you to define which rule to apply.').'
		</div>
	<br />
		<label>'.$this->l('Priorities:').'</label>
		<div class="margin-form">
			<input type="hidden" name="specificPricePriority[]" value="id_shop" />
			<select name="specificPricePriority[]">
				<option value="id_currency"'.($specificPricePriorities[1] == 'id_currency' ? ' selected="selected"' : '').'>'.$this->l('Currency').'</option>
				<option value="id_country"'.($specificPricePriorities[1] == 'id_country' ? ' selected="selected"' : '').'>'.$this->l('Country').'</option>
				<option value="id_group"'.($specificPricePriorities[1] == 'id_group' ? ' selected="selected"' : '').'>'.$this->l('Group').'</option>
			</select>
			&gt;
			<select name="specificPricePriority[]">
				<option value="id_currency"'.($specificPricePriorities[2] == 'id_currency' ? ' selected="selected"' : '').'>'.$this->l('Currency').'</option>
				<option value="id_country"'.($specificPricePriorities[2] == 'id_country' ? ' selected="selected"' : '').'>'.$this->l('Country').'</option>
				<option value="id_group"'.($specificPricePriorities[2] == 'id_group' ? ' selected="selected"' : '').'>'.$this->l('Group').'</option>
			</select>
			&gt;
			<select name="specificPricePriority[]">
				<option value="id_currency"'.($specificPricePriorities[3] == 'id_currency' ? ' selected="selected"' : '').'>'.$this->l('Currency').'</option>
				<option value="id_country"'.($specificPricePriorities[3] == 'id_country' ? ' selected="selected"' : '').'>'.$this->l('Country').'</option>
				<option value="id_group"'.($specificPricePriorities[3] == 'id_group' ? ' selected="selected"' : '').'>'.$this->l('Group').'</option>
			</select>
		</div>

		<div class="margin-form">
			<input type="checkbox" name="specificPricePriorityToAll" id="specificPricePriorityToAll" /> <label class="t" for="specificPricePriorityToAll">'.$this->l('Apply to all products').'</label>
		</div>

		<div class="margin-form">
			<input class="button" type="submit" name="submitSpecificPricePriorities" value="'.$this->l('Apply').'" />
		</div>
		';*/
	}

	public function _displaySpecificPriceAdditionForm($defaultCurrency, $shops, $currencies, $countries, $groups, $type = 'vnd', $id_specific_price = 0)
	{
		global $cookie;
		global $currentIndex;
		if(Tools::getIsset('idspecificprice'))
			$id_specific_price = Tools::getValue('idspecificprice');
		
		
		if (!($product = $this->loadObject()))
			return;

		if($id_specific_price > 0)
		{
			if($type == 'vnd')
				$specificPrice = Db::getInstance()->getRow('SELECT * FROM specific_price WHERE id_specific_price = '.$id_specific_price);
			else
				$specificPrice = Db::getInstance()->getRow('SELECT * FROM specific_price_wholesale WHERE id_specific_price = '.$id_specific_price);
		}
		
		$id_specific_price = $specificPrice['id_specific_price'];
		
		$default_country = new Country((int)Configuration::get('PS_COUNTRY_DEFAULT'));
		$idrand = rand(0,100);
		
		if($id_specific_price == 0)
			echo '<a href="#" onclick="$(\'#add_specific_price_'.$idrand.'\').slideToggle(); return false;"><img src="../img/admin/add.gif" alt="" />'.($type == 'vnd'  ? $this->l('AGGIUNGI OFFERTA SPECIALE') : $this->l('AGGIUNGI PREZZO ACQUISTO SPECIALE') ).'</a>';
		
		echo '

		<div id="add_specific_price_'.$idrand.'" style="'.($id_specific_price == 0 ? 'display: none;' : '').'">
			
			<input type="hidden" name="sp_id_shop" value="0" />
			<br />
			<table style="width:700px">
			';
		if($id_specific_price > 0 && $type == 'acq')
			$fornitore_standard = Db::getInstance()->getValue('SELECT id_supplier FROM supplier WHERE id_supplier = '.$product->id_supplier);
		else if($id_specific_price == 0 && $type == 'acq')
			$fornitore_standard = Db::getInstance()->getValue('SELECT id_supplier FROM supplier WHERE id_supplier = '.$product->id_supplier);
		else
			$fornitore_standard = Db::getInstance()->getValue('SELECT name FROM supplier WHERE id_supplier = '.$product->id_supplier);
		
			if($type == 'vnd')
			{
				echo '
				
				<tr><td colspan="4">'.$this->l('For:').'
					<select name="sp_id_currency" id="spm_currency_0_'.$idrand.'" onchange="changeCurrencySpecificPrice(0);">
						<option value="0">'.$this->l('All currencies').'</option>';
					foreach ($currencies as $currency)
						echo '<option value="'.(int)($currency['id_currency']).'" '.((int)($currency['id_currency']) == 1 ? 'selected="selected"' : '').'>'.Tools::htmlentitiesUTF8($currency['name']).'</option>';
					echo '
					</select>
					&gt;
					<select name="sp_id_country" style="width:200px">
						<option value="0">'.$this->l('All countries').'</option>';
					foreach ($countries as $country)
						echo '<option '.((int)($country['id_country']) == 10 ? 'selected="selected"' : '').' value="'.(int)($country['id_country']).'">'.Tools::htmlentitiesUTF8($country['name']).'</option>';
					echo '
					</select>
					&gt;
					<select name="sp_id_group">
						<option value="0">'.$this->l('All groups').'</option>';
					foreach ($groups as $group)
						echo '	<option '.((int)($group['id_group']) == 1 ? 'selected="selected"' : '').' value="'.(int)($group['id_group']).'">'.Tools::htmlentitiesUTF8($group['name']).'</option>';
					echo '
					</select>
				';
				
			
				echo '
			</td><td>Fornitore</td><td colspan="3">'.($type == 'vnd' ? '<input type="text" size="11" readonly="readonly" value="'.$fornitore_standard.'" />' : '<select name="sp_supplier_w">'.$supplier_select.'</select>').'</td></tr>';
			}
			
			
			
			echo '
			<tr><td colspan="8"><br /></td></tr>
			<tr><td>
			'.$this->l('Available from:').'</td><td>
				<input type="text" name="sp_from'.($type == 'acq' ? 'w' : '').'" value="'.($id_specific_price > 0 ? ( date('d-m-Y',strtotime($specificPrice['from'])) == '00-00-0000' ||  date('d-m-Y',strtotime($specificPrice['from'])) == '01-01-1970' || $specificPrice['from'] == '' ? date('01-m-Y') : date('d-m-Y',strtotime($specificPrice['from']))) : '').'" style="text-align: center" size="11" id="sp_from_'.$idrand.'" /></td>
				<td><span style="font-weight:bold; color:#000000; font-size:12px"> '.$this->l('to').'</span></td>
				<td>
				<input type="text" name="sp_to'.($type == 'acq' ? 'w' : '').'" value="'.($id_specific_price > 0 ? ( date('d-m-Y',strtotime($specificPrice['to'])) == '00-00-0000' ||  date('d-m-Y',strtotime($specificPrice['to'])) == '01-01-1970' || $specificPrice['to'] == '' ? date('01-m-Y') : date('d-m-Y',strtotime($specificPrice['to']))) : '').'" style="text-align: center" id="sp_to_'.$idrand.'" size="11" />
				</td>
				<td>
			';
			
			if($type == 'vnd')
			{	
			echo '	
			'.$this->l('Qta min.').'';
			
			
			echo '
				'.($type == 'vnd' ? '<input type="text" name="sp_from_quantity" value="'.($id_specific_price > 0 ? $specificPrice['from_quantity'] : '1').'" size="3" />' : '').'
				
				
				
			';
			
			}
			else
			{
				$supplier_select = '';
				$suppliers = Db::getInstance()->executeS('SELECT * FROM supplier ORDER BY name ASC');
				
				foreach($suppliers as $supplier)
					$supplier_select .= '<option value="'.$supplier['id_supplier'].'" '.($id_specific_price > 0 ? ($supplier['id_supplier'] == $specificPrice['id_supplier'] ? 'selected="selected"' : '') : ($supplier['id_supplier'] == $fornitore_standard ? 'selected="selected"' : '')).'>'.$supplier['name'].'</option>';
				
				echo 'Forn.: <select name="sp_supplier_w" id="sp_supplier_w" style="width:80px">'.$supplier_select.'</select>';
				
			}	
			
			
			if($id_specific_price > 0)
				$pieces = Db::getInstance()->getValue('SELECT pieces FROM specific_price_pieces WHERE id_specific_price = '.$id_specific_price);
			
			echo '</td>
			<td>
			'.$this->l('Pz. in off.').'</td><td>
				<input type="text" name="sp_pieces'.($type == 'acq' ? 'w' : '').'" value="'.($id_specific_price > 0 ? ($type == 'acq' ? $specificPrice['pieces'] : $pieces) : '').'" size="3" />  <!-- (NB: per non avere scadenza/quantit&agrave; disponibili, inserire spazio vuoto nel campo quantit&agrave;) -->
			</td></tr>
			
			
			<tr><td colspan="8"><br /></td></tr>
			<tr><td>
			'.$this->l('Vnd. standard:');
			echo '
			
				</td><td>
				<input type="text" readonly="readonly" id="sp_vnd_standard_'.$idrand.'" value="'.number_format($product->price,2,",","").'" size="11" />
				<span id="spm_currency_sign_post_0_'.$idrand.'" style="font-weight:bold; color:#000000; font-size:12px">'.($defaultCurrency->format == 2 ? ' '.$defaultCurrency->sign : '').'</span></td><td>
				
				Listino</td><td>
				
				<input type="text" size="11" readonly="readonly" id="sp_current_ht_price_'.$idrand.'" value="'.Tools::displayPrice((float)$product->listino, $defaultCurrency).'" /></td><td>';
		
			echo '			
			'.$this->l('Acq. standard').'</td><td colspan="3" style="text-align:right">
				
				<input type="text" readonly="readonly" value="'.(str_replace('.',',',$product->wholesale_price)).'" size="11" />
				<span id="spm_currency_sign_post_0_'.$idrand.'" style="font-weight:bold; color:#000000; font-size:12px">'.($defaultCurrency->format == 2 ? ' '.$defaultCurrency->sign : '').'</span>
			</td></tr>
			';
			
			if($type == 'acq')
			{
			echo '
			<tr><td colspan="8"><br /></td></tr>
			<tr>
			<td>
			'.$this->l('Sconto 1:').'</td><td>
				<input type="text" name="sp_reductionw1" size="11" value="'.($id_specific_price > 0 ? number_format($specificPrice['reduction_1'],2,",","") : number_format($product->sconto_acquisto_1,2,",","")).'" id="sp_reductionw1_'.$idrand.'"  onkeyup="calcSconti_'.$idrand.'(); calcolaMargine_'.$idrand.'()"; />%</td>
			
			<td>
			'.$this->l('Sconto 2:').'</td><td>
				<input type="text" name="sp_reductionw2" size="11" value="'.($id_specific_price > 0 ? number_format($specificPrice['reduction_2'],2,",","") : number_format($product->sconto_acquisto_2,2,",","")).'" id="sp_reductionw2_'.$idrand.'"  onkeyup="calcSconti_'.$idrand.'(); calcolaMargine_'.$idrand.'()"; />%</td>
				
			<td>
			'.$this->l('Sconto 3:').'</td><td colspan="3" style="text-align:right">
				<input type="text" name="sp_reductionw3" size="11" value="'.($id_specific_price > 0 ? number_format($specificPrice['reduction_3'],2,",","") : number_format($product->sconto_acquisto_3,2,",","")).'" id="sp_reductionw3_'.$idrand.'"  onkeyup="calcSconti_'.$idrand.'(); calcolaMargine_'.$idrand.'()"; />%</td>
			
			</tr>
			';
			
			}
			
			echo '
			
			
			<tr><td colspan="8"><br /></td></tr>
			<tr>
			
			<td>
			';
			
			$speciale_ow_row = Db::getInstance()->getRow("SELECT id_specific_price, wholesale_price FROM specific_price_wholesale spw WHERE id_product = ".Tools::getValue('id_product')." AND spw.to != '0000-00-00 00:00:00' AND spw.to > '".date("Y-m-d H:i:s")."' ");
							
			$speciale_ow = $speciale_ow_row['wholesale_price'];
			$speciale_ow_pieces = $speciale_ow_row['pieces'];
			if($speciale_ow >= 0 && $speciale_ow_row['id_specific_price'] > 0)
			{
				$sp_wholesale_price = $speciale_ow;
				$color_spec_w = 'blue';
			}
			else
			{
				$sp_wholesale_price = $product->wholesale_price;
			}
			
			if($type == 'acq' && $id_specific_price > 0)
				$sp_wholesale_price = $specificPrice['wholesale_price'];
			
			$fornitore_standard_name = Db::getInstance()->getValue('SELECT name FROM supplier WHERE id_supplier = '.$fornitore_standard);

			echo '				
				'.($type == 'acq' ? 'Fornitore stand.</td><td><input type="text" size="11" readonly="readonly" value="'.$fornitore_standard_name.'" /></td>' : 'Sconto</td><td><input type="text" id="sp_reduction_'.$idrand.'" name="sp_reduction" size="11"  value="'.($id_specific_price > 0 ? number_format(((($product->price-$specificPrice['price'])*100)/$product->price),2,",","") : 0).'" onkeyup="calcPrezzoSconto(\'sp_reduction_'.$idrand.'\', \'sp_price_'.$idrand.'\', \'sp_vnd_standard_'.$idrand.'\'); calcolaMargine_'.$idrand.'()";") /></td>').'
			
			<td>
			'.$this->l('Prezzo speciale');
			echo '
			
			
				</td><td><span id="spm_currency_sign_pre_0_'.$idrand.'" style="font-weight:bold; color:#000000; font-size:12px">'.($defaultCurrency->format == 1 ? ' '.$defaultCurrency->sign : '').'</span>
				<input type="text" name="sp_price'.($type == 'acq' ? 'w' : '').'" id="sp_price_'.$idrand.'" '.($type == 'acq' ? 'readonly="readonly"' : '').' onkeyup="calcSconto(\'sp_vnd_standard_'.$idrand.'\', \'sp_price_'.$idrand.'\', \'sp_reduction_'.$idrand.'\'); calcolaMargine_'.$idrand.'()"; value="'.(
				$type == 'vnd' && $id_specific_price > 0 ? number_format($specificPrice['price'],2,",","") : ($type == 'acq' ? (Product::trovaPrezzoSpeciale($product->id, 1, 1) > 0 ? number_format(Product::trovaPrezzoSpeciale($product->id, 1, 1),2,",","") : '--' ) : number_format($product->price,2,",",""))).'" size="11" style="color:red" />
				<span id="spm_currency_sign_post_0_'.$idrand.'" style="font-weight:bold; color:#000000; color: font-size:12px">'.($defaultCurrency->format == 2 ? ' '.$defaultCurrency->sign : '').'</span></td><td>
				<!-- <div class="hint clear" style="display:block;">
					'.$this->l('You can set this value at 0 in order to apply the default price').'
				</div> -->';
				
				$s_sc_qta_1 = ($product->price)-($product->price*($this->getSconti('sc_qta_1')));
				$s_sc_qta_2 = ($product->price)-($product->price*($this->getSconti('sc_qta_2')));
				$s_sc_qta_3 = ($product->price)-($product->price*($this->getSconti('sc_qta_3')));
				$s_sc_riv_1 = ($product->listino)-($product->listino*($this->getSconti('sc_riv_1')));
				$s_sc_riv_2 = ($product->listino)-($product->listino*($this->getSconti('sc_riv_2')));
				$s_sc_riv_3 = ($product->listino)-($product->listino*($this->getSconti('sc_riv_3')));
				
				echo '

					'.($type == 'vnd' ? '<input type="hidden" name="sp_reduction_type" value="amount" />' : '<input type="hidden" name="sp_reduction_type" value="acq" />').'
					
			';
		
			$qta_ord_clienti = Db::getInstance()->getValue('SELECT sum(product_quantity - (REPLACE(cons,"0_",""))) FROM order_detail od JOIN orders o ON o.id_order = od.id_order 
			JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE moh.id_order_state != 27 AND moh.id_order_state != 18 AND  moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE  od.product_id = '.$product->id.' AND os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state != 20 AND os.id_order_state != 26 AND os.id_order_state != 32 AND os.id_order_state != 35 GROUP BY od.product_id');

			$qta_ord_clienti = Db::getInstance()->getValue('SELECT sum(product_quantity - (REPLACE(cons,"0_",""))) FROM order_detail od JOIN orders o ON o.id_order = od.id_order
						JOIN cart c ON c.id_cart = o.id_cart
						JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE moh.id_order_state != 27 AND moh.id_order_state != 18 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE c.name NOT LIKE "%prime%" AND  od.product_id = '.$product->id.' AND os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state != 20 AND os.id_order_state != 26 AND os.id_order_state != 29 AND os.id_order_state != 31 AND os.id_order_state != 32 AND os.id_order_state != 35 GROUP BY od.product_id');
						
				if(!$qta_ord_clienti || $qta_ord_clienti == '')
					$qta_ord_clienti = 0;		
				
				$ordini_impegnati = Db::getInstance()->executeS('SELECT o.id_order, os.id_order_state, o.id_customer, sum(product_quantity - (REPLACE(cons,"0_",""))) qt FROM order_detail od JOIN orders o ON o.id_order = od.id_order
						JOIN cart c ON c.id_cart = o.id_cart
						JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE moh.id_order_state != 18 AND moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE c.name NOT LIKE "%prime%" AND  od.product_id = '.$obj->id.' AND os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state != 20 AND os.id_order_state != 26 AND os.id_order_state != 29 AND os.id_order_state != 31 AND os.id_order_state != 35 GROUP BY o.id_order');
						
						foreach($ordini_impegnati as $oo)
						{
							if($oo['qt'] == 0)
							{
								
							}
							else
								$ordini_impegnati_html .= '<tr><td><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer='.$oo['id_customer'].'&viewcustomer&id_order='.$oo['id_order'].'&vieworder&tab-container-1=4&token='.$tokenCustomers.'" target="_blank">'.$oo['id_order'].'</a></td><td>'.Db::getInstance()->getValue('SELECT name FROM order_state_lang WHERE id_lang = 5 AND id_order_state = '.$oo['id_order_state']).'</td><td>'.$oo['qt'].'</td></tr>';
						}
						
						
			echo '			
			'.$this->l('Prezzo acquisto').' '.($type == 'acq' ? 'spec.' : ($speciale_ow != 0 ? 'spec.' : '')).'</td><td colspan="3" style="text-align:right">
				<input type="hidden" id="sp_listino_'.$idrand.'" value="'.$product->listino.'" />
				
				<input type="text"  '.($type == 'acq' ? '' : 'readonly="readonly"').'  name="sp_wholesale_price'.($type == 'acq' ? 'w' : '').'" id="sp_wholesale_price_'.$idrand.'" onkeyup="'.($type == 'acq' ? "calcSconto('sp_listino_".$idrand."', 'sp_wholesale_price_".$idrand."', 'sp_reductionw1_".$idrand."'); document.getElementById('sp_reductionw2_".$idrand."').value = 0; document.getElementById('sp_reductionw3_".$idrand."').value = 0;  calcolaMargine_".$idrand."()" : 'calcolaMargine_'.$idrand.'()";').'" value="'.($type == 'acq' ? (str_replace('.',',',$sp_wholesale_price)) : ($sp_wholesale_price < 0 ? '--' : (str_replace('.',',',$sp_wholesale_price))) ).'" size="11" style="'.($color_spec_w != '' ? 'color:'.$color_spec_w : '').'" />
				<span id="spm_currency_sign_post_0_'.$idrand.'" style="font-weight:bold; color:#000000; font-size:12px">'.($defaultCurrency->format == 2 ? ' '.$defaultCurrency->sign : '').'</span>
			</td></tr>';
			
			if($type == 'acq')
			{
			}
			else
			{
				$abilita_banda = Db::getInstance()->getValue('SELECT abilita_banda FROM specific_price WHERE id_specific_price = '.$id_specific_price);
				echo '<tr><td colspan="8"><br /></td></tr>
				<tr><td>
				'.$this->l('Abilita banda');
				echo '
				
					</td><td>
					<input name="abilita_banda" id="abilita_banda" type="checkbox" '.($abilita_banda == 1 ? 'checked="checked"' : '').' /> <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" title="Abilita una banda con scritto \'Offerta speciale\' in listing e scheda prodotto" /></td><td>
					
					</td><td>
					
					</td><td>';
			
				echo '</td><td colspan="3" style="text-align:right">
					
					
				</td></tr>
				
				';
			}
			
			echo '
			<tr><td colspan="8">
			'.($type == 'vnd' ?  ($id_specific_price > 0 ? '<br />Attenzione, col prezzo speciale che stai impostando avrai margine del <strong><span id="sp_marg_'.$idrand.'">'.number_format(((($specificPrice['price']-$sp_wholesale_price)*100)/$specificPrice['price']),2,",","").'%</span></strong> e guadagno di <strong><span id="sp_guadagno_'.$idrand.'">'.number_format($specificPrice['price']-$sp_wholesale_price,2,",","").'&euro;</span></strong><br />' : '<br />Attenzione, col prezzo speciale che stai impostando avrai margine del <strong><span id="sp_marg_'.$idrand.'">'.number_format(((($product->price-$sp_wholesale_price)*100)/$product->price),2,",","").'%</span></strong> e guadagno di <strong><span id="sp_guadagno_'.$idrand.'">'.number_format($product->price-$sp_wholesale_price,2,",","").'&euro;</span></strong><br />') .'
			'.($speciale_ow != 0 ? 'Su questa offerta &egrave; presente un prezzo speciale d\'acquisto di <strong>'.str_replace('.',',',$sp_wholesale_price).'</strong> &euro; <br />' : '').'
			Con i prezzi di listino, il margine &egrave; del '.number_format(((($product->price-$product->wholesale_price)*100)/$product->price),2,",","").'% e il guadagno &egrave; di '.number_format($product->price-$product->wholesale_price,2,",","").'&euro;' : '
			<br />Attenzione, col prezzo d\'acquisto che stai impostando hai questi margini:<br />
			'.(Product::trovaPrezzoSpeciale($product->id, 1, 1) == 0 ? '<div style="display:none">' : '').'
			
			'.($id_specific_price > 0 ? 'Sul prezzo speciale, margine del <strong><span id="sp_marg_'.$idrand.'">'.number_format((((Product::trovaPrezzoSpeciale($product->id, 1, 1)-$sp_wholesale_price)*100)/Product::trovaPrezzoSpeciale($product->id, 1, 1)),2,",","").'%</span></strong> e guadagno di <strong><span id="sp_guadagno_'.$idrand.'">'.number_format(Product::trovaPrezzoSpeciale($product->id, 1, 1)-$sp_wholesale_price,2,",","").'&euro;</span></strong><br />
			'.(Product::trovaPrezzoSpeciale($product->id, 1, 1) == 0 ? '</div>' : '').'
			Sul prezzo di vendita standard, margine del <strong><span id="sp_marg_standard_'.$idrand.'">'.number_format(((($product->price-$sp_wholesale_price)*100)/$product->price),2,",","").'%</span></strong> e guadagno di <strong><span id="sp_guadagno_standard_'.$idrand.'">'.number_format($product->price-$sp_wholesale_price,2,",","").'&euro;</span></strong><br />' : 'Sul prezzo speciale, margine del <strong><span id="sp_marg_'.$idrand.'">'.number_format((((Product::trovaPrezzoSpeciale($product->id, 1, 1)-$sp_wholesale_price)*100)/Product::trovaPrezzoSpeciale($product->id, 1, 1)),2,",","").'%</span></strong> e guadagno di <strong><span id="sp_guadagno_'.$idrand.'">'.number_format(Product::trovaPrezzoSpeciale($product->id, 1, 1)-$sp_wholesale_price,2,",","").'&euro;</span></strong><br />Sul prezzo di vendita standard, margine del <strong><span id="sp_marg_standard_'.$idrand.'">'.number_format(((($product->price-$product->wholesale_price)*100)/$product->price),2,",","").'%</span></strong> e guadagno di <strong><span id="sp_guadagno_standard_'.$idrand.'">'.number_format($product->price-$product->wholesale_price,2,",","").'&euro;</span></strong><br />').'
			'.(Product::trovaPrezzoSpeciale($product->id, 1, 1) == 0 ? '</div>' : '').'
			
			Con i prezzi di listino, il margine &egrave; del '.number_format(((($product->price-$product->wholesale_price)*100)/$product->price),2,",","").'% e il guadagno &egrave; di '.number_format($product->price-$product->wholesale_price,2,",","").'&euro;').'
			</strong></td></tr>
			</table>
				<div style="float:left;margin-left:0px">
				<br />
				<table class="table" style="float:left">
				<tr><th style="color:#000">Listino</th><th style="color:#000">Vnd</th><th style="color:#000">Sc.qta 1</th><th style="color:#000">Sc.qta 2</th>
				<th style="color:#000">Sc.qta 3</th><th style="color:#000">Sc.riv.1</th><th style="color:#000">Sc.riv.2</th><th style="color:#000">Sc.distr.</th>
				</tr>
				<tr><td style="text-align:right">'.number_format($product->listino,2,",","").'</td>
				<td style="text-align:right">'.number_format($product->price,2,",","").'</td>
				<td style="text-align:right">'.($s_sc_qta_1 == $product->price ? '--' : number_format($s_sc_qta_1,2,",","")).'</td>
				<td style="text-align:right">'.($s_sc_qta_2 == $product->price ? '--' : number_format($s_sc_qta_2,2,",","")).'</td>
				<td style="text-align:right">'.($s_sc_qta_3 == $product->price ? '--' : number_format($s_sc_qta_3,2,",","")).'</td>
				<td style="text-align:right">'.($s_sc_riv_1 == $product->listino ? '--' : number_format($s_sc_riv_1,2,",","")).'</td>
				<td style="text-align:right">'.($s_sc_riv_2 == $product->listino ? '--' : number_format($s_sc_riv_2,2,",","")).'</td>
				<td style="text-align:right">'.($s_sc_riv_3 == $product->listino ? '--' : number_format($s_sc_riv_3,2,",","")).'</td>
				</tr>
				</table>
				</div><div style="clear:both"></div><br /><br />
				';
				
				$obj = $this->loadObject();
				$qty = Attribute::getAttributeQty($this->getFieldValue($obj, 'id_product'));
		if ($qty === false) {
			if (Validate::isLoadedObject($obj))
				$qty = $this->getFieldValue($obj, 'stock_quantity');
			else
				$qty = 1;
			$qty_state = '';
		}
		else
			$has_attribute = true;
				$scorta_minima = Db::getInstance()->getValue('SELECT scorta_minima FROM product WHERE id_product = '.$obj->id);
							
							$scorta_minima = (int)$scorta_minima;
							
							
							$sc_att = $this->getFieldValue($obj, 'stock_quantity')-$this->getFieldValue($obj, 'impegnato_quantity')+$this->getFieldValue($obj, 'ordinato_quantity');
							$sc_att = (int)$sc_att;
							
							$img_scorta = '';
							if($sc_att < $scorta_minima)
								$img_scorta = "<img style='border:2px solid red' src='../img/admin/error2.png' alt='Sotto scorta' title='Sotto scorta'>";
							
							
				echo '
				 <script>
								$(document).ready(function() {
									$(".span-reference-2").tooltipster({interactive: true});
								});
							</script>
	
							<table class="table"  cellpadding="0" cellspacing="0" style="float:left; border:1px solid #000; float:left; width:70%"><tr><th style="background: #ebebeb; text-align:center; border-bottom:1px solid #000" colspan="8">Disponibilit&agrave;</th></tr>
							<tr>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right; width:150px">'.$this->l('Magazzino EZ').'</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right">'.($obj->id ? $qty : 0).'</th>
							<th style="padding:0px; border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right; width:300px">
								
								<table style="width:100%; border-collapse:collapse; margin-top:0px; height:26px; margin-bottom:0px;border-bottom:0px"><tr><th style="border-right: 1px solid #bbb; border-bottom:0px solid #000; text-align:right;">'.$this->l('Netto').' <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Netto = Magazzino EZ - Qta ordinata dai clienti (sito)" title="Netto = Magazzino EZ - Qta ordinata dai clienti (sito)" /></th>
								<th style="border-right: 1px solid #bbb; border-bottom:0px solid #000;text-align:right">'.($qty - $qta_ord_clienti).'</th>
								
								<th style="border-right: 0px solid #bbb; border-bottom:0px solid #000;text-align:right">'.$this->l('Qta ordinata dai clienti (sito)').'</th></tr></table>
							</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right"><span style="cursor:pointer" class="span-reference-2" data-tooltip-content="#qta_ord_clienti_tooltip"><a href="javascript:void()">'.$qta_ord_clienti.'</a></span>
							
							<div class="tooltip_templates" style="display:none">
							<span id="qta_ord_clienti_tooltip"><table class=\'table\' style=\'width:250px\'><tr><th style="color:#000 !important">Ordine</th><th style="color:#000 !important">Stato</th><th style="color:#000 !important">Quantit&agrave;</th></tr>'.$ordini_impegnati_html.'</table></span>
							</div>
							</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right; width:300px">'.$this->l('Qta impegnata ord. caricati su gestionale (eSolver)').'</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'impegnato_quantity') : 0).'</th>
							</tr>
							
							<tr>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right">Mag. Amazon</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right">'.Db::getInstance()->getValue('SELECT amazon_quantity FROM product WHERE id_product = '.$obj->id).'</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right" >'.$this->l('Ordinato al fornitore').'</th>
							
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'ordinato_quantity') : 0).'</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right" colspan="1">Mag. EZ + Ordinato al fornitore - Impegnato sito</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right" >'.($qty + $this->getFieldValue($obj, 'ordinato_quantity') - $qta_ord_clienti).'</th>
							
							
							</tr>
							
							
							
							<tr>
							<'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Allnet').'</'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'supplier_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('In arrivo Allnet').'</'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'arrivo_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Data arr. Allnet').':</'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.(($from = $this->getFieldValue($obj, 'data_arrivo')) ? ($from == '0000-00-00' || $from == '1970-01-01' || $from == '1946-01-01' ? 'ND' : date("d-m-y",strtotime($from))) : date('d-m-y')).'</'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').'>
							
							</tr>
							<tr>
							<'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Esprinet').'</'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'esprinet_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('In arrivo Esprinet').'</'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'arrivo_esprinet_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Data arr. Esprinet').':</'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.(($from = $this->getFieldValue($obj, 'data_arrivo_esprinet')) ? ($from == '0000-00-00' || $from == '1970-01-01' || $from == '1946-01-01' ? 'ND' : date("d-m-y",strtotime($from))) : date('d-m-y')).'</'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').'>
							
							</tr>
							<tr>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Attiva').'</'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'attiva_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('In arrivo Attiva').'</'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'arrivo_attiva_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Data arr. Attiva').':</'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right"></'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').'>
							
							</tr>
							<tr>
							<'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Itancia').'</'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'itancia_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('In arrivo Itancia').'</'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">0</'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Data arr. Itancia').':</'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">ND</'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').'>
							
							</tr>
							
							<tr>
							<'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Intracom').'</'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'intracom_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('In arrivo Intracom').'</'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'arrivo_intracom_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Data arr. Intracom').':</'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.(($from_intracom = $this->getFieldValue($obj, 'data_arrivo_intracom')) ? ($from_intracom == '0000-00-00' || $from_intracom == '1970-01-01' || $from_intracom == '1946-01-01' ? 'ND' : date("d-m-y",strtotime($from_intracom))) : date('d-m-y')).'</'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').'>
							
							</tr>
							
							<tr>
							<'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('ASIT').'</'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? Db::getInstance()->getValue('SELECT asit_quantity FROM product WHERE id_product = '.$obj->id) : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right"></'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right"></'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right"></'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right"></'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').'>
							
							</tr>
							
							<tr>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Ingram').'</'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'ingram_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('In arrivo Ingram').'</'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'arrivo_ingram_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Data arr. Ingram').':</'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.(($from = $this->getFieldValue($obj, 'data_arrivo_ingram')) ? ($from == '0000-00-00' || $from == '1970-01-01' || $from == '1946-01-01' ? 'ND' : date("d-m-y",strtotime($from))) : date('d-m-y')).'</'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').'>
							
							</tr>
							
							<tr>
							
							<td style="background: #ebebeb;border-right: 1px solid #000; border-top: 1px solid #000; text-align:right"><strong>Totale tutti i magazzini (escl. Amazon)</strong></td>
							<td style="background: #ebebeb;border-right: 1px solid #000; border-top: 1px solid #000; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'quantity') : 0).'</td>
							<td style="background: #ebebeb;border-right: 1px solid #000; border-top: 1px solid #000; text-align:right; width:170px"><strong>Tot. magazzini + Ordinato a fornitore - Impegnato sito</strong></td>
							<td style="background: #ebebeb;text-align:right; border-right: 1px solid #000; border-top: 1px solid #000; ">'.($obj->id ? ($this->getFieldValue($obj, 'quantity'))+$this->getFieldValue($obj, 'ordinato_quantity')-$this->getFieldValue($obj, 'impegnato_quantity') : 0).'</td>
							';
							
							echo '
							<td style="background: #ebebeb;border-right: 1px solid #000; border-top: 1px solid #000; text-align:right; "><strong>'.$img_scorta.' Scorta minima</strong></td>
							<td style="background: #ebebeb;text-align:right; border-top: 1px solid #000; ">
							
							'.$scorta_minima.'</td>
							';
							
							$arrivo_ordinato = Db::getInstance()->getValue('SELECT data_arrivo_ordinato FROM product WHERE id_product = '.$obj->id);
							
							if($arrivo_ordinato == '')
								$date_arrivo_ordinato_riga = '<tr><td colspan="3">Nessun ordine in arrivo</td></tr>';
							else
							{
								$arrivo_ordinato = unserialize($arrivo_ordinato);
								
								foreach($arrivo_ordinato as $ao)
									$date_arrivo_ordinato_riga .= '<tr><td style="text-align:right;">'.$ao['ordine'].'</td><td style="text-align:right;">'.round($ao['quantita'],0).'</td><td>'.Db::getInstance()->getValue('SELECT company FROM customer WHERE codice_esolver = "'.$ao['fornitore'].'"').'</td><td>'.date('d/m/Y',strtotime($ao['data'])).'</td></tr>';
							}
							
							echo '
							</tr>
							
							</table> 
							
							<table class="table"  cellpadding="0" cellspacing="0" style="float:left; border:1px solid #000; float:left; width:24%; margin-left:2%"><tr><th style="background: #ebebeb; text-align:center; border-bottom:1px solid #000" colspan="8">Date qta in arrivo</th></tr>
							<tr>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right;">ID Ord.</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right;">Qt.</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right; width:150px">Fornitore</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right">Data</th>
							</tr>
							
							'.$date_arrivo_ordinato_riga.'
							
							</table>
							
							
							<div style="clear:both"></div>';
				
				echo '
				<div style="clear:both"></div>
			<script type="text/javascript">
			function calcolaMargine_'.$idrand.'() 
			{
				var numprice = document.getElementById(\'sp_price_'.$idrand.'\').value;
				var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
				
				if(numprice == "")
					numprice = 0;
				
				var numacquisto = document.getElementById(\'sp_wholesale_price_'.$idrand.'\').value;
				
				if(numacquisto == "")
					acquisto = 0;
					
				var acquisto = parseFloat(numacquisto.replace(/\s/g, "").replace(",", "."));
				
				var marginalitatotale = (((price - acquisto)*100) / price);
			
				marginalitatotale = marginalitatotale.toFixed(2);
				
				var guadagno = price-acquisto;
				guadagno = guadagno.toFixed(2);
			
				document.getElementById(\'sp_marg_'.$idrand.'\').innerHTML=marginalitatotale.replace(".",",") + "%";
				document.getElementById(\'sp_guadagno_'.$idrand.'\').innerHTML=guadagno.replace(".",",") + " €";
				
				'.($type == 'acq' ? 
				
				'
					var numprice_standard = document.getElementById(\'sp_vnd_standard_'.$idrand.'\').value;
					var price_standard = parseFloat(numprice_standard.replace(/\s/g, "").replace(",", "."));
	
					var marginalitatotale_standard = (((price_standard - acquisto)*100) / price_standard);
					marginalitatotale_standard = marginalitatotale_standard.toFixed(2);
					
					var guadagno_standard = price_standard-acquisto;
					guadagno_standard = guadagno_standard.toFixed(2);
				
					document.getElementById(\'sp_marg_standard_'.$idrand.'\').innerHTML=marginalitatotale_standard.replace(".",",") + "%";
					document.getElementById(\'sp_guadagno_standard_'.$idrand.'\').innerHTML=guadagno_standard.replace(".",",") + " €";
				
				
				'
				
				
				: '').'
			}		
			
			function calcSconti_'.$idrand.'() 
			{
				var acq1 = parseFloat(document.getElementById("sp_reductionw1_'.$idrand.'").value.replace(/,/g, "."));
				if (acq1 == "" || isNaN(acq1))
					acq1 = 0;
					
				var acq2 = parseFloat(document.getElementById("sp_reductionw2_'.$idrand.'").value.replace(/,/g, "."));
				if (acq2 == "" || isNaN(acq2))
					acq2 = 0;
					
				var acq3 = parseFloat(document.getElementById("sp_reductionw3_'.$idrand.'").value.replace(/,/g, "."));
				
				if (acq3 == "" || isNaN(acq3))
					acq3 = 0;
					
				var listino = parseFloat(document.getElementById("sp_listino_'.$idrand.'").value.replace(/,/g, "."));
				
				if (listino == "" || isNaN(listino))
					listino = 0;
				var newPrice = (listino * (100 - acq1) / 100) * ((100 - acq2)/100) * ((100 - acq3)/100);
				
				document.getElementById("sp_wholesale_price_'.$idrand.'").value = (isNaN(newPrice) == true || newPrice < 0) ? "" :
				ps_round(newPrice, 2);		
				
			}	
			</script><br />
			<div class="margin-form">
				'.($id_specific_price > 0 ? '<input type="hidden" name="id_specific_price_'.$type.'" value="'.$id_specific_price.'" /><input type="hidden" name="specific_price_type" value="'.$type.'" />' : '').'
				<input type="submit" style="height:22px" name="submitPriceAddition" value="'.($id_specific_price > 0 ? $this->l('Edit') : $this->l('Add')).' '.($type == 'vnd' ? 'offerta' : 'prezzo acquisto speciale').'" class="button" />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				'.($id_specific_price > 0 ? '<a class="button" style="font-size:12px; height:22px" href="'.$currentIndex.(Tools::getValue('id_category') ? '&id_category='.(int)Tools::getValue('id_category') : '').'&id_product='.(int)(Tools::getValue('id_product')).'&updateproduct&deleteSpecificPrice'.($type != 'acq' ? '' : 'Wholesale').'&id_specific_price='.(int)($specificPrice['id_specific_price']).'&token='.Tools::getValue('token').'" onclick="javascript: var sure=window.confirm(\'Sei sicuro/a di cancellare il prezzo speciale?\'); if (sure) { return true; } else { return false; }">'.$this->l('Cancella prezzo speciale').'</a>' : '').'
			</div>
		</div>
		<hr />';
		include_once('functions.php');
		includeDatepicker3(array('sp_from_'.$idrand, 'sp_to_'.$idrand, 'date_available'), true);
		includeDatepicker3(array('sp_from_'.$idrand, 'sp_to_'.$idrand, 'data_arrivo'), true);
	}

	private function _getCustomizationFieldIds($labels, $alreadyGenerated, $obj)
	{
		$customizableFieldIds = array();
		if (isset($labels[_CUSTOMIZE_FILE_]))
			foreach ($labels[_CUSTOMIZE_FILE_] AS $id_customization_field => $label)
				$customizableFieldIds[] = 'label_'._CUSTOMIZE_FILE_.'_'.(int)($id_customization_field);
		if (isset($labels[_CUSTOMIZE_TEXTFIELD_]))
			foreach ($labels[_CUSTOMIZE_TEXTFIELD_] AS $id_customization_field => $label)
				$customizableFieldIds[] = 'label_'._CUSTOMIZE_TEXTFIELD_.'_'.(int)($id_customization_field);
		$j = 0;
		for ($i = $alreadyGenerated[_CUSTOMIZE_FILE_]; $i < (int)($this->getFieldValue($obj, 'uploadable_files')); $i++)
			$customizableFieldIds[] = 'newLabel_'._CUSTOMIZE_FILE_.'_'.$j++;
		$j = 0;
		for ($i = $alreadyGenerated[_CUSTOMIZE_TEXTFIELD_]; $i < (int)($this->getFieldValue($obj, 'text_fields')); $i++)
			$customizableFieldIds[] = 'newLabel_'._CUSTOMIZE_TEXTFIELD_.'_'.$j++;
		return implode('¤', $customizableFieldIds);
	}

	private function _displayLabelField(&$label, $languages, $defaultLanguage, $type, $fieldIds, $id_customization_field)
	{
		$fieldsName = 'label_'.$type.'_'.(int)($id_customization_field);
		$fieldsContainerName = 'labelContainer_'.$type.'_'.(int)($id_customization_field);
		echo '<div id="'.$fieldsContainerName.'" class="translatable clear" style="line-height: 18px">';
		foreach ($languages as $language)
		{
			$fieldName = 'label_'.$type.'_'.(int)($id_customization_field).'_'.(int)($language['id_lang']);
			$text = (isset($label[(int)($language['id_lang'])])) ? $label[(int)($language['id_lang'])]['name'] : '';
			echo '<div class="lang_'.$language['id_lang'].'" id="'.$fieldName.'" style="display: '.((int)($language['id_lang']) == (int)($defaultLanguage) ? 'block' : 'none').'; clear: left; float: left; padding-bottom: 4px;">
						<div style="margin-right: 6px; float:left; text-align:right;">#'.(int)($id_customization_field).'</div><input type="text" name="'.$fieldName.'" value="'.htmlentities($text, ENT_COMPAT, 'UTF-8').'" style="float: left" />
					</div>';
		}

		$required = (isset($label[(int)($language['id_lang'])])) ? $label[(int)($language['id_lang'])]['required'] : false;
		echo '</div>
				<div style="margin: 3px 0 0 3px; font-size: 11px">
					<input type="checkbox" name="require_'.$type.'_'.(int)($id_customization_field).'" id="require_'.$type.'_'.(int)($id_customization_field).'" value="1" '.($required ? 'checked="checked"' : '').' style="float: left; margin: 0 4px"/><label for="require_'.$type.'_'.(int)($id_customization_field).'" style="float: none; font-weight: normal;"> '.$this->l('required').'</label>
				</div>';
	}

	private function _displayLabelFields(&$obj, &$labels, $languages, $defaultLanguage, $type)
	{
		$type = (int)($type);
		$labelGenerated = array(_CUSTOMIZE_FILE_ => (isset($labels[_CUSTOMIZE_FILE_]) ? count($labels[_CUSTOMIZE_FILE_]) : 0), _CUSTOMIZE_TEXTFIELD_ => (isset($labels[_CUSTOMIZE_TEXTFIELD_]) ? count($labels[_CUSTOMIZE_TEXTFIELD_]) : 0));

		$fieldIds = $this->_getCustomizationFieldIds($labels, $labelGenerated, $obj);
		if (isset($labels[$type]))
			foreach ($labels[$type] AS $id_customization_field => $label)
				$this->_displayLabelField($label, $languages, $defaultLanguage, $type, $fieldIds, (int)($id_customization_field));
	}

	function displayFormCustomization($obj, $languages)
	{
		parent::displayForm();
		$labels = $obj->getCustomizationFields();
		$defaultIso = Language::getIsoById($defaultLanguage);

		$hasFileLabels = (int)($this->getFieldValue($obj, 'uploadable_files'));
		$hasTextLabels = (int)($this->getFieldValue($obj, 'text_fields'));

		echo '
			<table cellpadding="5">
				<tr>
					<td colspan="2"><b>'.$this->l('Add or modify customizable properties').'</b></td>
				</tr>
			</table>
			<hr style="width:100%;" /><br />
			<table cellpadding="5" style="width:100%">
				<tr>
					<td style="width:150px;text-align:right;padding-right:10px;font-weight:bold;vertical-align:top;" valign="top">'.$this->l('File fields:').'</td>
					<td style="padding-bottom:5px;">
						<input type="text" name="uploadable_files" id="uploadable_files" size="4" value="'.((int)($this->getFieldValue($obj, 'uploadable_files')) ? (int)($this->getFieldValue($obj, 'uploadable_files')) : '0').'" />
						<p>'.$this->l('Number of upload file fields displayed').'</p>
					</td>
				</tr>
				<tr>
					<td style="width:150px;text-align:right;padding-right:10px;font-weight:bold;vertical-align:top;" valign="top">'.$this->l('Text fields:').'</td>
					<td style="padding-bottom:5px;">
						<input type="text" name="text_fields" id="text_fields" size="4" value="'.((int)($this->getFieldValue($obj, 'text_fields')) ? (int)($this->getFieldValue($obj, 'text_fields')) : '0').'" />
						<p>'.$this->l('Number of text fields displayed').'</p>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align:center;">
						<input type="submit" name="submitCustomizationConfiguration" value="'.$this->l('Update settings').'" class="button" onclick="this.form.action += \'&addproduct&tabs=5\';" />
					</td>
				</tr>';

				if ($hasFileLabels)
				{
					echo '
				<tr><td colspan="2"><hr style="width:100%;" /></td></tr>
				<tr>
					<td style="width:150px" valign="top">'.$this->l('Files fields:').'</td>
					<td>';
					$this->_displayLabelFields($obj, $labels, $languages, $defaultLanguage, _CUSTOMIZE_FILE_);
					echo '
					</td>
				</tr>';
				}

				if ($hasTextLabels)
				{
					echo '
				<tr><td colspan="2"><hr style="width:100%;" /></td></tr>
				<tr>
					<td style="width:150px" valign="top">'.$this->l('Text fields:').'</td>
					<td>';
					$this->_displayLabelFields($obj, $labels, $languages, $defaultLanguage, _CUSTOMIZE_TEXTFIELD_);
					echo '
					</td>
				</tr>';
				}

				echo '
				<tr>
					<td colspan="2" style="text-align:center;">';
				if ($hasFileLabels OR $hasTextLabels)
					echo '<input type="submit" name="submitProductCustomization" id="submitProductCustomization" value="'.$this->l('Save labels').'" class="button" onclick="this.form.action += \'&addproduct&tabs=5\';" style="margin-top: 9px" />';
				echo '
					</td>
				</tr>
			</table>';
	}

	function displayFormAttachments($obj, $languages, $defaultLanguage)
	{
		global $currentIndex, $cookie;
		if (!($obj = $this->loadObject(true)))
			return;
		$languages = Language::getLanguages(false);
		$attach1 = Attachment::getAttachments($cookie->id_lang, $obj->id, true);
		$attach2 = Attachment::getAttachments($cookie->id_lang, $obj->id, false);
		
		$tokenAttachments = Tools::getAdminToken('AdminAttachments'.(int)(Tab::getIdFromClassName('AdminAttachments')).(int)($cookie->id_employee));
		
		$countAttachments = (int)Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'product_attachment WHERE id_product = '.(int)$obj->id);

				echo '<div class="tab-page" id="step7"><h4 class="tab"><a onclick="">7. '.$this->l('Allegati').' ('.$countAttachments.')</a></h4>
		'.($obj->id ? '<input type="hidden" name="id_'.$this->table.'" value="'.$obj->id.'" />' : '').'
			<fieldset><legend><img src="../img/t/AdminAttachments.gif" />'.$this->l('Attachment').'</legend>';
					
			
		echo'		<label>'.$this->l('Filename:').'</label>
				<div class="margin-form">';
		foreach ($languages as $language)
			echo '	<div id="attachment_name_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
						<input size="33" maxlength="32" type="text" onkeyup="maxlengthinputf(this,32,\'conta_attachment_name_'.$language['id_lang'].'\');" name="attachment_name_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'attachment_name', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" /><sup> *</sup>
					<span id="conta_attachment_name_'.$language['id_lang'].'"></span> - Max 32 caratteri</div>';
		$this->displayFlags($languages, $defaultLanguage, 'attachment_name¤attachment_description', 'attachment_name');
		echo '	</div>
				<div class="clear">&nbsp;</div>
				<label>'.$this->l('Description:').' </label>
				<div class="margin-form">';
		foreach ($languages as $language)
			echo '	<div id="attachment_description_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
						<textarea name="attachment_description_'.$language['id_lang'].'">'.htmlentities($this->getFieldValue($obj, 'attachment_description', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'</textarea>
					</div>';
		$this->displayFlags($languages, $defaultLanguage, 'attachment_name¤attachment_description', 'attachment_description');
		echo '	</div>
				<div class="clear">&nbsp;</div>
				<label>'.$this->l('File').'</label>
				<div class="margin-form">
					<p><input type="file" name="attachment_file" /></p>
					<p>'.$this->l('Upload file from your computer').'</p>
				</div>
				<div class="clear">&nbsp;</div>
				<div class="margin-form">
					<input type="submit" value="'.$this->l('Add a new attachment file').'" name="submitAddAttachments" class="button" />
				</div>
				<div class="small"><sup>*</sup> '.$this->l('Required field').'</div>
			</fieldset>
		<div class="clear">&nbsp;</div>
		<table>
			<tr>
				<td>
					<p>'.$this->l('Attachments for this product:').' (fare doppio clic sugli allegati nella colonna di sinistra per aprirli. Fare clic col tasto destro per aprire la maschera di modifica dell\'allegato.)</p>
					
<script type="text/javascript">
$(document).ready(function() { $("#selectAttachment2").select2(); });

$("#selectAttachment1").bind("contextmenu",function(e){
   window.open("https://www.ezdirect.it/ezadmin/index.php?tab=AdminAttachments&id_attachment="+this.options[this.selectedIndex].value+"&updateattachment&token='.$tokenAttachments.'","_blank");
   return false;
}); 

document.getElementById("selectAttachment1").ondblclick = function(){
	window.open("http://www.ezdirect.it/attachment.php?id_attachment="+this.options[this.selectedIndex].value,"_blank");
};

function togli_allegati(id) {
	$.ajax({
		  url:"ajax.php?togli_allegati=y",
		  type: "POST",
		  data: { 
		  togli_allegati: \'y\',
		  id_allegati: id
		  },
		  success:function(resp){
		  },
		  error: function(xhr,stato,errori){
			 alert("Errore nella cancellazione:"+xhr.status);
		  }
	});

}
</script>
					<select multiple id="selectAttachment1" name="attachments[]" style="width:300px;height:160px;">';
			foreach ($attach1 AS $attach)
				echo '<option selected="selected" value="'.$attach['id_attachment'].'">'.$attach['name'].'</option>';
			echo '	</select><br /><br />
					<a href="#" id="addAttachment" style="text-align:center;display:block;border:1px solid #aaa;text-decoration:none;background-color:#fafafa;color:#123456;margin:2px;padding:2px">
						'.$this->l('Remove').' &gt;&gt;
					</a>
					<br /><br />
					<a href="javascript:void(0)" id="removeAttachmentDB" onclick="var surec = window.confirm(\'Sei sicuro/a?\'); if (surec) { togli_allegati($(\'#selectAttachment1\').val()); return !$(\'#selectAttachment1 option:selected\').remove().appendTo(\'#selectAttachment2\'); } else { return false; };" style="text-align:center;display:block;border:1px solid #aaa;text-decoration:none;background-color:#fafafa;color:#123456;margin:2px;padding:2px">
						'.$this->l('Elimina e cancella dal database').' &gt;&gt;
					</a>
				</td>
				<td style="padding-left:20px;">
					<p>'.$this->l('Available attachments:').'</p>
					<select multiple id="selectAttachment2" style="width:300px;height:160px;">';
			foreach ($attach2 AS $attach)
				echo '<option value="'.$attach['id_attachment'].'">'.$attach['name'].'</option>';
			echo '	</select><br /><br />
					<a href="#" id="removeAttachment" style="text-align:center;display:block;border:1px solid #aaa;text-decoration:none;background-color:#fafafa;color:#123456;margin:2px;padding:2px">
						&lt;&lt; '.$this->l('Add').'
					</a>
				</div>
				</td>
			</tr>
		</table>
		<div class="clear">&nbsp;</div>
		<input type="submit" name="submitAttachments" id="submitAttachments" value="'.$this->l('Update attachments').'" class="button" />
		</div>
		';
	}
	
	function displayFormEsolver($obj, $languages)
	{
		
		echo '<div class="tab-page" id="step8"><h4 class="tab"><a onclick="">8. '.$this->l('eSolver').'</a></h4>';
		
			$product = Db::getInstance()->getRow("select   id_category_default, tipo_anagrafica, cl.name categoria, s.name fornitore, m.name costruttore, modello,reference, product.id_product, supplier_reference, cod_fornitore_1, ean13, ean_descrizione, ean_quantita, ean_tipo, intrastat, name_short, product_lang.name, fuori_produzione, product.condition, product.date_add, product.date_upd,'' as manufacturer_code, product.id_manufacturer,'' as category_code, id_category_gst,weight, product.id_supplier, note_fornitore,
	'' as codice_fornitore, altro_esolver, note, tempo_approvvigionamento, codice_barre_fornitore,note, note_2,stock_quantity,  amazon_quantity, ordinato_quantity, impegnato_quantity, '' as ordinato_a_fornitore, supplier_quantity, esprinet_quantity, attiva_quantity, ingram_quantity, itancia_quantity, intracom_quantity, asit_quantity, arrivo_quantity, arrivo_esprinet_quantity, arrivo_attiva_quantity, arrivo_ingram_quantity, '' as arrivo_itancia_quantity, data_arrivo, data_arrivo_esprinet, '' as data_arrivo_itancia, data_arrivo_attiva, listino, product.sconto_acquisto_1, product.sconto_acquisto_2, product.sconto_acquisto_3, product.wholesale_price, acquisto_in_dollari, scorta_minima, scorta_minima_amazon, blocco_prezzi, login_for_offer, scontolistinovendita, price, '' as trasporto_gratuito, date_available, '' as data_arrivo_fornitore, '' as sconto_rivenditore_1, '' as sconto_rivenditore_2, rebate_1,'Rebate 1' as descrizione_rebate_1, rebate_2,'Rebate 2' as descrizione_rebate_2, rebate_3,
	'Rebate 3' as descrizione_rebate_3, premio_target,'Premio' as descrizione_premio,pezzi_in_confezione, nr, pz, '' as contenuto_confezione, '' as garanzia, asin, commissione_amazon, commissione_eprice from product 
	left join campi_esolver on product.id_product = campi_esolver.id_product
	left join product_lang on product.id_product = product_lang.id_product
	left join product_esolver on product.id_product = product_esolver.id_product
	left join category_lang cl on product.id_category_gst = cl.id_category
	left join manufacturer m on product.id_manufacturer = m.id_manufacturer
	left join supplier s on product.id_supplier = s.id_supplier
	where product_lang.id_lang = 5 and cl.id_lang = 5 AND product.id_product = ".$obj->id."");
	
		//CAMPI DA RIEMPIRE
		if($product['id_manufacturer'] == 150 && ($product['id_category_default'] == 119 || $product['id_category_default'] == 248 || $product['id_category_default'] == 249))
		{	
			$product['tipo_anagrafica'] = 4;
			$product['modello'] = 2;
		}
		else
		{	
			$product['tipo_anagrafica'] = 1;
			$product['modello'] = 1;
		}	
		
		$virtual = ProductDownload::getIdFromIdProduct($p['id_product']);
		if($virtual)
			$product['downloadable'] = 'S&igrave;';
		else
			$product['downloadable'] = 'No';
		
		$product['manufacturer_code'] = substr(strtoupper($product['costruttore']),0,3).sprintf("%'.03d", $product['id_manufacturer']);
		$product['category_code'] = substr(strtoupper($product['categoria']),0,3).sprintf("%'.03d", $product['id_category_gst']);
		$product['codice_fornitore'] = substr(strtoupper($product['fornitore']),0,2).sprintf("%'.03d", $product['id_supplier']);
		
		$product['tipo'] = Db::getInstance()->getValue('SELECT value FROM feature_value_lang fvl JOIN feature_product fp ON fvl.id_feature_value = fp.id_feature_value JOIN feature_lang fl ON fl.id_feature = fp.id_feature WHERE fvl.id_lang = 5 AND fl.name LIKE "Tipo" AND fp.id_product = '.$obj->id.' GROUP BY fvl.id_feature_value');
		
		$product['serie'] = Db::getInstance()->getValue('SELECT value FROM feature_value_lang fvl JOIN feature_product fp ON fvl.id_feature_value = fp.id_feature_value WHERE fvl.id_lang = 5 AND fp.id_feature = 917 AND fp.id_product = '.$obj->id.' GROUP BY fvl.id_feature_value');
		
		$product['confezione'] = Db::getInstance()->getValue('SELECT value FROM feature_value_lang fvl JOIN feature_product fp ON fvl.id_feature_value = fp.id_feature_value WHERE fvl.id_lang = 5 AND fp.id_feature = 450 AND fp.id_product = '.$obj->id.' GROUP BY fvl.id_feature_value');
		
		$product['garanzia'] = Db::getInstance()->getValue('SELECT value FROM feature_value_lang fvl JOIN feature_product fp ON fvl.id_feature_value = fp.id_feature_value WHERE fvl.id_lang = 5 AND fp.id_feature = 449 AND fp.id_product = '.$obj->id.' GROUP BY fvl.id_feature_value');
		
		$product['ordinato'] = Db::getInstance()->getValue('SELECT sum(product_quantity - (REPLACE(cons,"0_",""))) FROM order_detail od JOIN orders o ON o.id_order = od.id_order JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE  od.product_id = '.$obj->id.' AND os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state != 20 AND os.id_order_state != 35 AND os.id_order_state != 26 GROUP BY od.product_id');
		
		$listini_fornitori = Db::getInstance()->executeS('SELECT s.name as fornitore, pli.listino, pli.sconto_acquisto_1, pli.sconto_acquisto_2, pli.sconto_acquisto_3, pli.wholesale_price FROM product_listini pli JOIN product p ON p.id_product = pli.id_product JOIN supplier s ON pli.id_supplier = s.id_supplier WHERE p.id_supplier != pli.id_supplier AND p.id_product = '.$obj->id);
						
		if(!$product['ordinato'])
			$product['ordinato'] = 0;
		
		$row = Db::getInstance()->getRow("SELECT value FROM configuration WHERE id_configuration = 240");

		
		$trasp = unserialize($row['value']);

		$au = $obj->id;
		
		foreach($trasp as $el) {
		if ($au == $el) {
		$uu = 1;
		break;
		}
		else {
		$uu = 0;
		}
		}

		if($au == "") {
		$uu = 0;
		}
		
		$product['sconto_rivenditore_1'] = Db::getInstance()->getValue('SELECT reduction FROM specific_price WHERE specific_price_name = "sc_riv_1" AND id_product = '.$obj->id);
		$product['sconto_rivenditore_2'] = Db::getInstance()->getValue('SELECT reduction FROM specific_price WHERE specific_price_name = "sc_riv_2" AND id_product = '.$obj->id);
		
		$product['sconto_rivenditore_1'] *= 100;
		$product['sconto_rivenditore_2'] *= 100;
 		//FINE CAMPI DA RIEMPIRE
		
		echo '<fieldset><legend>Sezione articolo</legend><table cellpadding="5" cellspacing="5">
		<tr><td style="width:150px">ID CRM</td><td colspan="7"><input type="text" size="130" name="id_crm" readonly="readonly" value="'.$obj->id.'" /></td></tr>
		
		<tr><td style="width:150px">Tipo anagrafica</td><td colspan="7"><input type="text" size="130" name="tipo_anagrafica" readonly="readonly" value="'.$product['tipo_anagrafica'].'" /></td></tr>
		<tr><td>Modello</td><td colspan="7"><input type="text" size="130" name="modello" readonly="readonly" value="'.$product['modello'].'" /></td></tr>
		<tr><td>Produttore</td><td colspan="7"><input type="text" size="130" value="'.$product['costruttore'].'"  readonly="readonly" /></td></tr>
		
		<tr><td>Codice articolo  EZ</td><td colspan="7"><input type="text" size="130" value="'.$product['reference'].'" readonly="readonly" /></td></tr>
		<tr><td>Codice ID Database</td><td colspan="7"><input type="text" size="130" value="'.$product['id_product'].'" readonly="readonly" /></td></tr>
		<tr><td>Cod. Produttore (SKU)</td><td colspan="7"><input type="text" size="130"  value="'.$product['supplier_reference'].'" readonly="readonly" /></td></tr>
		<tr><td>Codifica fornitore 1</td><td colspan="7"><input type="text" size="130" value="'.$product['cod_fornitore_1'].'" readonly="readonly" /></td></tr>
		<tr><td>Cod. EAN</td><td><input type="text" size="11" value="'.$product['ean13'].'" readonly="readonly" /></td>
		<td>EAN descrizione</td><td><input type="text" size="11" name="ean_descrizione" value="'.$product['ean_descrizione'].'" /></td>
		<td>EAN quantità</td><td><input type="text" size="6" name="ean_quantita" value="'.$product['ean_quantita'].'" /></td>
		<td>EAN tipo</td><td><input type="text" size="6" name="ean_tipo" value="'.$product['ean_tipo'].'" /></td></tr>
		
		<tr><td>Codice Instrastat</td><td colspan="7"><input type="text" size="130" name="intrastat" value="'.$product['intrastat'].'" /></td></tr>
		<tr><td>Descr. Ez (50 car.)</td><td colspan="7"><input type="text" size="130" name="name_short" value="'.substr($product['name'],0,50).'" readonly="readonly" /></td></tr>
		<tr><td>Descr. estesa Ez</td><td colspan="7"><input type="text" size="130" value="'.$product['name'].'" readonly="readonly" /></td></tr>
		
		<tr><td>In produzione?</td><td>
		<input type="text" size="12" value="'.($product['fuori_produzione'] == 0 ? 'In produzione' : 'Fuori produzione').'" readonly="readonly" />
		</td><td>Usato (sì-no)</td><td><input type="text" size="12" value="'.($product['condition'] == 'new' ? 'No' : 'S&igrave;').'" readonly="readonly" />
		</td><td></td><td></td></tr>
		
		<tr><td>Data inserimento</td><td><input type="text" size="12" value="'.Tools::displayDate($product['date_add'],5).'" readonly="readonly"/>
		</td><td>Data ultima modifica</td><td><input type="text" size="12" value="'.Tools::displayDate($product['date_upd'],5).'" readonly="readonly"/>
		</td><td>Data validit&agrave;</td><td><input type="text" size="12" value=""  readonly="readonly" /></td><td></td></tr>
		
		<!-- <tr><td>Codice del produttore</td><td><input type="text" size="12"  value="'.$product['manufacturer_code'].'" readonly="readonly"/> -->
		
		<tr><td>Codice della famiglia</td><td><input type="text" size="12" value="'.$product['category_code'].'" readonly="readonly"/></td>
		<td>Descrizione famiglia</td><td><input type="text" size="12" value="'.$product['categoria'].'" readonly="readonly" />
		</td>
		<td></td><td></td></tr>
		
		<tr><td>Prodotto scaricabile</td><td><input type="text" size="12" value="'.$product['downloadable'].'"  readonly="readonly" /></td>
		<td>Codice prod scaricabile</td><td><input type="text" size="12" value="'.$product['codice_prodotto_scaricabile'].'" readonly="readonly" /></td><td></td><td></td></tr>
		
		<tr><td>Peso package</td><td><input type="text" size="12" value="'.$product['weight'].'"  readonly="readonly" /></td>
		<td>Peso netto</td><td><input type="text" size="12" value=""  readonly="readonly" /></td>
		<td></td><td></td></tr>
		
		<tr><td>Nome fornitore primario</td><td><input type="text" size="12" value="'.$product['fornitore'].'" readonly="readonly" /></td>
		<td>Note fornitore</td><td><input type="text" size="12" value="'.$product['note_fornitore'].'" readonly="readonly" /></td>
		<td>Codice del fornitore esterno</td><td><input type="text" size="12" value="'.$product['codice_fornitore'].'" readonly="readonly" />
		</td>	<td></td><td></td></tr>
		
		<tr><td>Tipo</td><td colspan="7"><input type="text" size="130"  value="'.$product['tipo'].'" readonly="readonly" /></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
		<tr><td>Serie</td><td colspan="7"><input type="text" size="130"  value="'.$product['serie'].'" readonly="readonly" /></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
		<tr><td>Altro</td><td colspan="7"><input type="text" size="130" name="altro_esolver" value="'.$product['altro_esolver'].'" /></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
		<tr><td>Note</td><td colspan="7"><input type="text" size="130" value="'.$product['note'].'" readonly="readonly" /></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
		<tr><td>Tempo di approvvigionamento</td><td colspan="7"><input type="text" size="130" name="tempo_approvvigionamento" value="'.$product['tempo_approvvigionamento'].'" /></td><td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
		<tr><td>Codice a barre c/o fornitore</td><td colspan="7"><input type="text" size="130" name="codice_barre_fornitore" value="'.$product['codice_barre_fornitore'].'" /></td><td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
		<tr><td>Note 1</td><td colspan="7"><input type="text" size="130" value="'.$product['note'].'" readonly="readonly" /></td><td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
		<tr><td>Note 2</td><td  colspan="7"><input type="text" size="130" name="note_2" value="'.$product['note_2'].'" /></td><td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
		
		<tr><td>Pz in confezione</td><td colspan="7"><input type="text" size="130" name="pezzi_in_confezione" value="'.$product['pezzi_in_confezione'].'" /></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
		<!-- <tr><td>Nr</td><td><input type="text" size="130" name="nr" value="'.$product['nr'].'" /> -->
		<!-- <tr><td>Pz</td><td><input type="text" size="130" name="pz" value="'.$product['pz'].'" /> -->
	
		<tr><td>Contenuto confezione</td><td colspan="7"><input type="text" size="130" value="'.$product['confezione'].'" readonly="readonly" /></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
		<tr><td>Garanzia</td><td colspan="7"><input type="text" size="130" value="'.$product['garanzia'].'" readonly="readonly" /></td><td></td><td></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
		</table></fieldset><br />';
		
		echo '<fieldset><legend>Sezione magazzino Ezdirect e fornitori</legend>
		<table cellpadding="5" cellspacing="5">
		<tr><td style="width:150px">Magazzino EZ</td><td><input type="text" size="10"  value="'.$product['stock_quantity'].'" readonly="readonly" style="text-align:right" /></td>
		<td></td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><td style="width:150px">Magazzino Amazon</td><td><input type="text" size="10"  value="'.$product['amazon_quantity'].'" readonly="readonly" style="text-align:right" /></td>
		<td></td><td></td><td></td><td></td><td></td><td></td></tr>
		<tr><td>Scorta minima</td><td><input type="text" size="10" value="'.$product['scorta_minima'].'" readonly="readonly" style="text-align:right" /></td>
		<tr><td>Scorta minima Amazon</td><td><input type="text" size="10" value="'.$product['scorta_minima_amazon'].'" readonly="readonly" style="text-align:right" /></td>
		<td></td><td></td><td></td><td></td><td></td><td></td></tr>
		<tr><td>Ord da clienti</td><td><input type="text" size="10" value="'.$product['ordinato'].'" readonly="readonly" style="text-align:right" /></td>
		<td></td><td></td><td></td><td></td><td></td><td></td></tr>
		<tr><td>Imp Ord. Su gestionale</td><td><input type="text" size="10" value="'.$product['impegnato_quantity'].'" readonly="readonly" style="text-align:right" /></td>
		<td></td><td></td><td></td><td></td><td></td><td></td></tr>
		<tr><td>Ordinato a fornitore 1</td><td><input type="text" size="10" value="'.$product['ordinato_quantity'].'" readonly="readonly" style="text-align:right" /></td>
		<td></td><td></td><td></td><td></td><td></td><td></td></tr>
		
		<tr><td>Magazzino Allnet</td><td><input type="text" size="10"  value="'.$product['supplier_quantity'].'" readonly="readonly" style="text-align:right" /></td>
		<td>In arrivo Allnet</td><td><input type="text" size="10" value="'.$product['arrivo_quantity'].'" readonly="readonly"  style="text-align:right"/></td>
		<td>Data arrivo allnet</td><td><input type="text" size="10" value="'.(($product['data_arrivo']) ? ($product['data_arrivo'] == '0000-00-00' || $product['data_arrivo'] == '1970-01-01' || $product['data_arrivo'] == '1946-01-01' ? 'ND' : date("d/m/Y",strtotime($product['data_arrivo']))) : date('d/m/Y')).'" readonly="readonly"  style="text-align:right"/></td></tr>
		
		<tr><td>Magazzino Esprinet</td><td><input type="text" size="10"  value="'.$product['esprinet_quantity'].'" readonly="readonly" style="text-align:right" /></td>
		<td>In arrivo Esprinet</td><td><input type="text" size="10" value="'.$product['arrivo_esprinet_quantity'].'" readonly="readonly"  style="text-align:right"/></td>
		<td>Data arrivo Esprinet</td><td><input type="text" size="10" value="'.(($product['data_arrivo_esprinet']) ? ($product['data_arrivo_esprinet'] == '0000-00-00' || $product['data_arrivo_esprinet'] == '1970-01-01' || $product['data_arrivo_esprinet'] == '1946-01-01' ? 'ND' : date("d/m/Y",strtotime($product['data_arrivo_esprinet']))) : date('d/m/Y')).'" readonly="readonly"  style="text-align:right"/></td></tr>
		
		<tr><td>Magazzino Attiva</td><td><input type="text" size="10"  value="'.$product['attiva_quantity'].'" readonly="readonly" style="text-align:right" /></td>
		<td>In arrivo Attiva</td><td><input type="text" size="10" value="'.$product['arrivo_attiva_quantity'].'" readonly="readonly"  style="text-align:right"/></td>
		<td>Data arrivo Attiva</td><td><input type="text" size="10" value="'.(($product['data_arrivo_attiva']) ? ($product['data_arrivo_attiva'] == '0000-00-00' || $product['data_arrivo_attiva'] == '1970-01-01' || $product['data_arrivo_attiva'] == '1946-01-01' ? 'ND' : date("d/m/Y",strtotime($product['data_arrivo_attiva']))) : date('d/m/Y')).'" readonly="readonly"  style="text-align:right"/></td></tr>
		
		<tr><td>Magazzino Ingram</td><td><input type="text" size="10"  value="'.$product['ingram_quantity'].'" readonly="readonly" style="text-align:right" /></td>
		<td>In arrivo Ingram</td><td><input type="text" size="10" value="'.$product['arrivo_ingram_quantity'].'" readonly="readonly"  style="text-align:right"/></td>
		<td>Data arrivo Ingram</td><td><input type="text" size="10" value="'.(($product['data_arrivo_ingram']) ? ($product['data_arrivo_ingram'] == '0000-00-00' || $product['data_arrivo_ingram'] == '1970-01-01' || $product['data_arrivo_ingram'] == '1946-01-01' ? 'ND' : date("d/m/Y",strtotime($product['data_arrivo_ingram']))) : date('d/m/Y')).'" readonly="readonly"  style="text-align:right"/></td></tr>
		
		<tr><td>Magazzino Itancia</td><td><input type="text" size="10"  value="'.$product['itancia_quantity'].'" readonly="readonly"  style="text-align:right"/>
		</td><td>In arrivo Itancia</td><td><input type="text" size="10"  value="'.$product['arrivo_itancia_quantity'].'" readonly="readonly"  style="text-align:right"/></td><td>Data arrivo Itancia</td><td><input type="text" size="10"  value="'.$product['data_arrivo_itancia'].'" readonly="readonly" style="text-align:right" /></td></tr>
		
		<tr><td>Magazzino Intracom</td><td><input type="text" size="10"  value="'.$product['intracom_quantity'].'" readonly="readonly"  style="text-align:right"/>
		</td><td>In arrivo Intracom</td><td><input type="text" size="10"  value="'.$product['arrivo_intracom_quantity'].'" readonly="readonly"  style="text-align:right"/></td><td>Data arrivo Intracom</td><td><input type="text" size="10"  value="'.$product['data_arrivo_intracom'].'" readonly="readonly" style="text-align:right" /></td></tr>
		
		<tr><td>Magazzino ASIT</td><td><input type="text" size="10"  value="'.$product['asit_quantity'].'" readonly="readonly"  style="text-align:right"/>
		</td><td></td><td></td><td></td><td></td></tr>
		
		<tr><td>Disponibile da</td><td><input type="text" size="10" value="'.(($product['date_available'] != '1946-01-01') ? ($product['date_available'] == '0000-00-00' || $product['date_available'] == '1970-01-01' ? 'ND' : date("d/m/Y",strtotime($product['date_available']))) : date('d/m/Y')).'" readonly="readonly" style="text-align:right" /></td>
		<td></td><td></td><td></td><td></td><td></td><td></td></tr>
		</table>		
		</fieldset><br />';
		
		echo '<fieldset><legend>Sezione listini</legend>
		
		<table cellpadding="5" cellspacing="5">
		<tr><td style="width:150px">Listino costruttore</td><td><input type="text" size="8" value="'.number_format($product['listino'],2,",","").'" readonly="readonly" style="text-align:right" /></td>
		<td></td><td></td><td></td><td></td><td></td><td></td></tr>
		
		<tr><td>Sconto 1 '.$product['fornitore'].'</td><td><input type="text" size="8"  value="'.number_format($product['sconto_acquisto_1'],2,",","").' %"  readonly="readonly" style="text-align:right" /></td>
		<td>Sconto 2 '.$product['fornitore'].'</td><td><input type="text" size="8" value="'.number_format($product['sconto_acquisto_2'],2,",","").' %" readonly="readonly" style="text-align:right" /></td>
		<td>Sconto 3 '.$product['fornitore'].'</td><td><input type="text" size="8" value="'.number_format($product['sconto_acquisto_3'],2,",","").' %" readonly="readonly" style="text-align:right" /></td>
		<td>Netto acq.  '.$product['fornitore'].'</td><td><input type="text" size="8" value="'.number_format($product['wholesale_price'],2,",","").'"  readonly="readonly" style="text-align:right" /></td></tr>';
		
		foreach($listini_fornitori as $pli)
		{
			echo '<tr><td>Sconto 1 '.$pli['fornitore'].'</td><td><input type="text" size="8"  value="'.number_format($pli['sconto_acquisto_1'],2,",","").' %"  readonly="readonly" style="text-align:right" /></td>
			<td>Sconto 2 '.$pli['fornitore'].'</td><td><input type="text" size="8" value="'.number_format($pli['sconto_acquisto_2'],2,",","").' %" readonly="readonly" style="text-align:right" /></td>
			<td>Sconto 3 '.$pli['fornitore'].'</td><td><input type="text" size="8" value="'.number_format($pli['sconto_acquisto_3'],2,",","").' %" readonly="readonly" style="text-align:right" /></td>
			<td>Netto acq.  '.$pli['fornitore'].'</td><td><input type="text" size="8" value="'.number_format($pli['wholesale_price'],2,",","").'"  readonly="readonly" style="text-align:right" /></td></tr>';
			
		}
		
		echo '		
		<tr><td>Acquisto in dollari</td><td><input type="text" size="8"  value="'.($product['acquisto_in_dollari'] == 1 ? 'S&igrave;' : 'No').'"  readonly="readonly" style="text-align:right" /></td>
		<td></td><td></td><td></td><td></td><td></td><td></td></tr>
		
		<tr><td>Blocco prezzi</td><td><input type="text" size="8" value="'.($product['blocco_prezzi'] == 0 ? 'No' : 'S&igrave').'" readonly="readonly" style="text-align:right" /></td>
		<td></td><td></td><td></td><td></td><td></td><td></td></tr>
		
		<tr><td>Login for offer</td><td><input type="text" size="8" value="'.($product['login_for_offer'] == 0 ? 'No' : 'S&igrave').'" readonly="readonly" style="text-align:right" /></td>
		<td></td><td></td><td></td><td></td><td></td><td></td></tr>
		
		<tr><td>Sconto cliente</td><td><input type="text" size="8"  value="'.number_format($product['scontolistinovendita'],2,",","").' %" readonly="readonly" style="text-align:right" /></td>
		<td></td><td></td><td></td><td></td><td></td><td></td></tr>
		
		<tr><td>Prezzo vendita</td><td><input type="text" size="8" value="'.number_format($product['price'],2,",","").'" readonly="readonly" style="text-align:right" /></td>
		<td></td><td></td><td></td><td></td><td></td><td></td></tr>
		<tr><td>Trasporto gratuito</td><td><input type="text" size="8" value="'.($product['trasporto_gratuito'] == 1 ? 'S&igrave;' : 'No').'" readonly="readonly" style="text-align:right" /></td>
		<td></td><td></td><td></td><td></td><td></td><td></td></tr>
		
		
		<tr><td>SC riv1</td><td><input type="text" size="8"  value="'.number_format($product['sconto_rivenditore_1'],2,",","").' %" readonly="readonly" style="text-align:right" /></td><td>Sc riv 2</td><td><input type="text" size="8" value="'.number_format($product['sconto_rivenditore_2'],2,",","").' %" readonly="readonly" style="text-align:right" /></td><td></td><td></td></tr>
		
		<tr><td>Rebate 1</td><td><input type="text" size="8" value="'.number_format($product['rebate_1'],2,",","").' %" readonly="readonly" style="text-align:right" /></td><td>Descrizione rebate 1</td><td><input type="text" size="8" name="descrizione_rebate_1" value="'.$product['descrizione_rebate_1'].'" readonly="readonly"style="text-align:right" /></td><td></td><td></td></tr>
		<tr><td>Rebate 2</td><td><input type="text" size="8" value="'.number_format($product['rebate_2'],2,",","").' %" readonly="readonly" style="text-align:right" /></td><td>Descrizione rebate 2</td><td><input type="text" size="8" name="descrizione_rebate_2" value="'.$product['descrizione_rebate_2'].'" readonly="readonly"style="text-align:right" /></td><td></td><td></td></tr>
		<tr><td>Rebate 3</td><td><input type="text" size="8" value="'.number_format($product['rebate_3'],2,",","").' %" readonly="readonly" style="text-align:right" /></td><td>Descrizione rebate 3</td><td><input type="text" size="8" name="descrizione_rebate_3" value="'.$product['descrizione_rebate_3'].'" readonly="readonly"style="text-align:right" /></td><td></td><td></td></tr>
		<tr><td>Premio target</td><td><input type="text" size="8" name="premio_target" value="'.$product['premio_target'].'" style="text-align:right" /></td>
		<tr><td>Descrizione premio</td><td><input type="text" size="8" name="descrizione_premio" value="'.$product['descrizione_premio'].'" readonly="readonly"style="text-align:right" /></td><td></td><td></td></tr>
		
		</table>
		
		
		</fieldset><br />';
		
		echo '<fieldset><legend>Sezione marketplace</legend>
		<table>
		<tr><td style="width:150px">ASIN</td><td><input type="text" size="110" value="'.$product['asin'].'"  readonly="readonly" />
		<tr><td>Commissione amazon</td><td><input type="text" size="110" name="commissione_amazon" value="'.$product['commissione_amazon'].'" />
		<tr><td>Commissione Eprice</td><td><input type="text" size="110" name="commissione_eprice" value="'.$product['commissione_eprice'].'" />
		</table></fieldset>';
		
		echo '<p style="text-align:center"><input type="submit" class="button" name="accoda_esolver" value="Accoda a CSV eSolver e scrivi file" />';
		
		
		echo '</div>';
	}
	
	function displayFormStorico($obj, $languages)
	{
		
		echo '<div class="tab-page" id="step9"><h4 class="tab"><a onclick="">9. '.$this->l('Storico').'</a></h4>';
		
		$storico_prezzi = Db::getInstance()->executeS('SELECT * FROM storico_prezzi WHERE id_product = '.$obj->id.' ORDER BY date DESC');
		if(count($storico_prezzi) == 0)
			echo 'Nessuna informazione da mostrare';
		else
		{
			echo '<table class="table">
			<tr><th>Data</th><th>Listino</th><th>Acquisto</th><th>Vendita</th><tr>';
			echo '<tr><td>Attuale</td><td style="text-align:right">'.number_format($obj->listino,2,",","").'</td><td style="text-align:right">'.number_format($obj->wholesale_price,2,",","").'</td><td style="text-align:right">'.number_format($obj->price,2,",","").'</td></tr>';
			
			foreach ($storico_prezzi as $storico)
				echo '<tr><td>'.(Tools::displayDate($storico['date'],5)).'</td>'.str_replace('<td','<td style="text-align:right"',str_replace('.',',',$storico['storico'])).'</tr>';
				
			echo '</table>';
		}
		
		echo '</div>';
	}

	function displayFormInformations($obj, $currency)
	{
		parent::displayForm(false);
		global $currentIndex, $cookie, $link;

		$default_country = new Country((int)Configuration::get('PS_COUNTRY_DEFAULT'));
		$iso = Language::getIsoById((int)($cookie->id_lang));
		$has_attribute = false;
		$qty_state = 'readonly';
		$qty = Attribute::getAttributeQty($this->getFieldValue($obj, 'id_product'));
		if ($qty === false) {
			if (Validate::isLoadedObject($obj))
				$qty = $this->getFieldValue($obj, 'stock_quantity');
			else
				$qty = 1;
			$qty_state = '';
		}
		else
			$has_attribute = true;
		$cover = Product::getCover($obj->id);
		$this->_applyTaxToEcotax($obj);
		
		$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);

		echo '
		<div class="tab-page" id="step1">
			<h4 class="tab"><a '.(isset($_GET['id_bundle']) ? 'onclick="location.href=\''.$currentIndex.'&id_product='.$obj->id.'&updateproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'\';"' : '').'>1. '.$this->l('Info.').'</a></h4>
			<script type="text/javascript">
				$(document).ready(function() {
					updateCurrentText();
					updateFriendlyURL();
					$.ajax({
						url: "'.dirname($currentIndex).'/ajax.php",
						cache: false,
						dataType: "json",
						data: "ajaxProductManufacturers=1",
						success: function(j) {
							var options = $("select#id_manufacturer").html();
							if (j)
							for (var i = 0; i < j.length; i++)
								options += \'<option value="\' + j[i].optionValue + \'" data-supplier="\' + j[i].datasupplier + \'" data-margin-cli="\' + j[i].margin_min_cli + \'" data-margin-riv="\' + j[i].margin_min_riv + \'" data-othersuppliers="\' + j[i].dataothersuppliers + \'">\' + j[i].optionDisplay + \'</option>\';
							$("select#id_manufacturer").html(options);
						},
						error: function(XMLHttpRequest, textStatus, errorThrown)
						{
						}

					});
					$.ajax({
						url: "'.dirname($currentIndex).'/ajax.php",
						cache: false,
						dataType: "json",
						data: "ajaxProductSuppliers=1",
						success: function(j) {
							var options = $("select#id_supplier").html();
							if (j)
							for (var i = 0; i < j.length; i++)
								options += \'<option value="\' + j[i].optionValue + \'">\' + j[i].optionDisplay + \'</option>\';
							$("select#id_supplier").html(options);
						},
						error: function(XMLHttpRequest, textStatus, errorThrown)
						{
							
						}

					});
					if ($(\'#available_for_order\').is(\':checked\')){
						$(\'#show_price\').attr(\'checked\', \'checked\');
						$(\'#show_price\').attr(\'disabled\', \'disabled\');
					}
					else {
						$(\'#show_price\').attr(\'disabled\', \'\');
					}
				});
			</script>
			<b>'.$this->l('Product global information').'</b>&nbsp;-&nbsp;';
		$preview_url = '';
		if (isset($obj->id))
		{
			$preview_url = ($link->getProductLink($this->getFieldValue($obj, 'id'), $this->getFieldValue($obj, 'link_rewrite', $this->_defaultFormLanguage), Category::getLinkRewrite($this->getFieldValue($obj, 'id_category_default'), (int)($cookie->id_lang))));
			if (!$obj->active)
			{
				$admin_dir = dirname($_SERVER['PHP_SELF']);
				$admin_dir = substr($admin_dir, strrpos($admin_dir,'/') + 1);
				$token = Tools::encrypt('PreviewProduct'.$obj->id);

				$preview_url .= $obj->active ? '' : '&adtoken='.$token.'&ad='.$admin_dir;
			}
			
			$is_in_bundle = Product::isProductInBundle($obj->id);

			echo '
			
				<a href="index.php?tab=AdminCatalog&id_product='.$obj->id.'&duplicateproduct&token='.$this->token.'" style="float:right;"
			onclick="return confirm(\''.$this->l('Are you sure?', __CLASS__, true, false).'\');">
			<img src="../img/admin/duplicate.png" alt="'.$this->l('Duplicate').'" title="'.$this->l('Duplicate').'" /> '.$this->l('Duplicate').'</a>
			
			';
			
			if($cookie->id_employee == 1 || $cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 7)
			{	
				if($is_in_bundle == true) {
				
						echo '<a href="index.php?tab=AdminCatalog&id_product='.$obj->id.'&deleteproduct&token='.$this->token.'" style="float:right;"
				onclick="return confirm(\''.$this->l('ATTENZIONE!!!!!!!! Questo prodotto fa parte di uno o piu\' bundle!!!!! Sei proprio sicuro di volerlo cancellare???', __CLASS__, true, false).'\');">
				'.($obj->active == 1 ? '' : '<img src="../img/admin/delete.gif" alt="'.$this->l('Delete this product').'" title="'.$this->l('Delete this product').'" /> '.$this->l('Delete this product').'</a>');
				
				}
				
				else {
				
				
					echo '<a href="index.php?tab=AdminCatalog&id_product='.$obj->id.'&deleteproduct&token='.$this->token.'" style="float:right;"
				onclick="return confirm(\''.$this->l('Are you sure?', __CLASS__, true, false).'\');">
				<img src="../img/admin/delete.gif" alt="'.$this->l('Delete this product').'" title="'.$this->l('Delete this product').'" /> '.$this->l('Delete this product').'</a>';
				
				}
			}
			echo '<a href="'.$preview_url.'" target="_blank"><img src="../img/admin/details.gif" alt="'.$this->l('View product in shop').'" title="'.$this->l('View product in shop').'" /> '.$this->l('View product in shop').'</a>';

			if (file_exists(_PS_MODULE_DIR_.'statsproduct/statsproduct.php'))
				echo '&nbsp;-&nbsp;<a href="index.php?tab=AdminStats&module=statsproduct&id_product='.$obj->id.'&token='.Tools::getAdminToken('AdminStats'.(int)(Tab::getIdFromClassName('AdminStats')).(int)($cookie->id_employee)).'"><img src="../modules/statsproduct/logo.gif" alt="'.$this->l('View product sales').'" title="'.$this->l('View product sales').'" /> '.$this->l('View product sales').'</a>';
		}
		echo '
			<hr class="clear"/>
			<br />
			<style type="text/css">
			/* .col-left { width:100px; } */
			</style>
				<table cellpadding="5" style="width: 50%; float: left; margin-right: 20px; border-right: 1px solid #E0D0B1;">
					<tr>
						<td class="col-left">'.$this->l('Name:').'</td>
						<td style="padding-bottom:5px;" class="translatable">';
		foreach ($this->_languages as $language)
			echo '		<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
								<input size="43" type="text" id="name_'.$language['id_lang'].'" name="name_'.$language['id_lang'].'"
								value="'.stripslashes(htmlspecialchars($this->getFieldValue($obj, 'name', $language['id_lang']))).'"'.((!$obj->id) ? ' onkeyup="if (isArrowKey(event)) return; attivaSEO(document.getElementById(\'description_5\').value); copy2friendlyURL();"' : '').' onkeyup="attivaSEO(document.getElementById(\'description_5\').value); maxlengthinputf(this,55,\'conta_name_'.$language['id_lang'].'\'); if (isArrowKey(event)) return; updateCurrentText();" onchange="updateCurrentText();" /><sup> *</sup><br />
								<!-- <span class="hint" name="help_box">'.$this->l('Invalid characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span> -->
								
								<div style="font-size:11px">Lunghezza: <span style="color:red" id="conta_name_'.$language['id_lang'].'">'.strlen($this->getFieldValue($obj, 'name', $language['id_lang'])).'</span> - <strong>Tenersi entro i 55 caratteri</strong></div>
							</div>
							
							';
		echo '		</td>
					</tr>
					<tr>
						<td class="col-left">'.$this->l('Codice eSolver:').'</td>
						<td style="padding-bottom:5px;">
							<input required size="55" type="text" id="reference" name="reference" value="'.($this->getFieldValue($obj, 'reference') == '--COPIA--' ? '' : htmlentities($this->getFieldValue($obj, 'reference'), ENT_COMPAT, 'UTF-8')).'" style="width: 160px; margin-right: 14px;" '.($this->getFieldValue($obj, 'reference') != '' ? '' : '').' /> <sup> *</sup>
				
				 '.($this->getFieldValue($obj, 'reference') != '' ? '<!-- <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Codice eSolver bloccato per evitare problemi in amministrazione. Per cambiare il codice creare un nuovo prodotto o contattare Federico/Barbara" title="Codice eSolver bloccato per evitare problemi in amministrazione. Per cambiare il codice creare un nuovo prodotto o contattare Federico/Barbara" /> -->' : '
							 <input name="btnAvailable" type="button" class="bottone" id="btnAvailable" 
			  onclick=\'$("#checkid").html("Attendi..."); $.get("checkreference.php",{ cmd: "checkref", id_product: "'.$_GET['id_product'].'", reference: $("#reference").val() } ,function(data){  $("#checkid").html(data); });\'
			  value="Verifica" /> ').'
			   <span style="font: bold 12px; " id="checkid" ></span> 
						</td>
					</tr>
					
			   
					<tr>
						<td class="col-left">'.$this->l('Codice SKU:').'</td>
						<td style="padding-bottom:5px;">
							<input size="55" type="text" name="supplier_reference" id="supplier_reference" value="'.htmlentities($this->getFieldValue($obj, 'supplier_reference'), ENT_COMPAT, 'UTF-8').'" style="width: 160px; margin-right: 14px;" />
							 <input name="btnAvailable" type="button" class="bottone" id="btnAvailable" 
			  onclick=\'$("#checkidsup").html("Attendi..."); $.get("checkreference.php",{ cmd: "checksupref", id_product: "'.$_GET['id_product'].'", supplier_reference: $("#supplier_reference").val() } ,function(data){  $("#checkidsup").html(data); });\'
			  value="Verifica" /> (<input type="checkbox" name="sku_provvisorio" '.($obj->reference == '' ? 'checked="checked"' : '').' /> Prov. )
			   <span style="font: bold 12px; " id="checkidsup" ></span>
						</td>
					</tr>
								
							
					
					<tr>
						<td class="col-left">'.$this->l('EAN13 or JAN:').'</td>
						<td style="padding-bottom:5px;">
							<input size="55" maxlength="13" pattern="[0-9]{13}"
        title="Questo campo deve essere obbligatoriamente di 13 caratteri" type="text" id="ean13" name="ean13" value="'.htmlentities($this->getFieldValue($obj, 'ean13'), ENT_COMPAT, 'UTF-8').'" style="width: 130px; margin-right: 5px;" /> <span class="small">'.$this->l('(Europe, Japan)').'</span>
						</td>
					</tr>
					<tr>
						<td class="col-left">'.$this->l('Rif. Fornitore 1:').'</td>
						<td style="padding-bottom:5px;">
							<input size="55" maxlength="12" type="text" name="upc" value="'.htmlentities($this->getFieldValue($obj, 'upc'), ENT_COMPAT, 'UTF-8').'" style="width: 130px; margin-right: 5px;" /> <span class="small"><!-- '.$this->l('(US, Canada)').' --></span>
						</td>
					</tr>
					<tr>
						<td class="col-left">'.$this->l('Rif. Fornitore 2:').'</td>
						<td style="padding-bottom:5px;">
							<input size="55" maxlength="12" type="text" name="rif_fornitore_2" value="'.htmlentities($this->getFieldValue($obj, 'rif_fornitore_2'), ENT_COMPAT, 'UTF-8').'" style="width: 130px; margin-right: 5px;" /> <span class="small"><!-- '.$this->l('(US, Canada)').' --></span>
						</td>
					</tr>
					<tr>
						<td class="col-left">'.$this->l('ASIN').'<br /><br /</td>
						<td style="padding-bottom:5px;">
						<input type="text" style="width:80px" value="'.htmlentities($this->getFieldValue($obj, 'asin'), ENT_COMPAT, 'UTF-8').'" name="asin" id="asin" />
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$this->l('Trasp. gratis Amazon.it?').'&nbsp;&nbsp;&nbsp;<input type="checkbox" name="amazon_free_shipping" id="amazon_free_shipping" value="1"  '.($this->getFieldValue($obj, 'amazon_free_shipping') == 1 ? 'checked="checked"' : '').'" />
						</td>
					</tr>
					<tr>
						<td class="col-left">'.$this->l('Dimensioni (cm)').'</td>
						<td style="padding-bottom:5px;">
							<input size="45" maxlength="6" name="width" type="text" value="'.htmlentities($this->getFieldValue($obj, 'width'), ENT_COMPAT, 'UTF-8').'" style="width: 50px; margin-right: 5px; text-align:right"  onKeyUp="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, \'.\');" /> Larg.
							
							<input size="45" maxlength="6" name="height" type="text" value="'.htmlentities($this->getFieldValue($obj, 'height'), ENT_COMPAT, 'UTF-8').'" style="width: 50px; margin-right: 5px; text-align:right"  onKeyUp="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, \'.\');" /> Lung.
							
							<input size="45" maxlength="6" name="depth" type="text" value="'.htmlentities($this->getFieldValue($obj, 'depth'), ENT_COMPAT, 'UTF-8').'" style="width: 50px; margin-right: 5px; text-align:right"  onKeyUp="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, \'.\');" /> Alt.
						</td>
					</tr>
					
					<tr>
						<td class="col-left">'.$this->l('Peso:').'</td>
						<td style="padding-bottom:5px;">
							<input size="55" maxlength="6" name="weight" type="text" value="'.htmlentities($this->getFieldValue($obj, 'weight'), ENT_COMPAT, 'UTF-8').'" style="width: 80px; margin-right: 5px; text-align:right"  onKeyUp="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, \'.\');" /> '.Configuration::get('PS_WEIGHT_UNIT').'
							
							&nbsp;&nbsp;&nbsp;&nbsp;'.$this->l('Scorta min. EZ / Amz').' <input size="55" maxlength="6" name="scorta_minima" type="text" value="'.htmlentities($this->getFieldValue($obj, 'scorta_minima'), ENT_COMPAT, 'UTF-8').'" style="width: 12px; margin-right: 2px;"  onKeyUp="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, \'.\');" />
							/ 
							 <input size="55" maxlength="6" name="scorta_minima_amazon" type="text" value="'.htmlentities($this->getFieldValue($obj, 'scorta_minima_amazon'), ENT_COMPAT, 'UTF-8').'" style="width: 12px; margin-right: 2px;"  onKeyUp="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, \'.\');" />
						</td>
					</tr>
					
					<tr>
						<td class="col-left">'.$this->l('Fuori produzione?').'&nbsp;&nbsp;&nbsp; </td>
						<td style="padding-bottom:5px;">
							
							 <input type="checkbox" name="fuori_produzione" id="fuori_produzione" value="1" '.($this->getFieldValue($obj, 'fuori_produzione') == 1 ? 'checked="checked"' : '').'" />
							 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							'.$this->l('Acquisto in dollari?').'&nbsp;&nbsp;&nbsp; <input type="checkbox" name="acquisto_in_dollari" id="acquisto_in_dollari" value="1" '.($this->getFieldValue($obj, 'acquisto_in_dollari') == 1 ? 'checked="checked"' : '').'" /> 
						</td> 
					</tr>
					
					<tr>
						<td class="col-left">'.$this->l('Blocco prezzi?*').'</td>
						<td style="padding-bottom:5px;">
							<input type="checkbox" name="blocco_prezzi" id="blocco_prezzi" value="1" '.($this->getFieldValue($obj, 'blocco_prezzi') == 1 ? 'checked="checked"' : '').'" /> 
							* Con questa opzione saranno disattivati gli aggiornamenti automatici dei prezzi per questo prodotto
						</td>
					</tr>
					<tr>
						<td class="col-left">'.$this->l('Login for offer?*').'</td>
						<td style="padding-bottom:5px;">
							<input type="checkbox" name="login_for_offer" id="login_for_offer" value="1" '.($this->getFieldValue($obj, 'login_for_offer') == 1 ? 'checked="checked"' : '').'" /> 
							* Con questa opzione il cliente vede il prezzo login for offer
						</td>
					</tr>
					<tr>
						<td colspan="2">'.$this->l('Data inserimento:').' <strong>'.Tools::displayDate($this->getFieldValue($obj, 'date_add'), $cookie->id_lang).'</strong>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$this->l('Ultimo aggiornamento: ').' <strong>'.Tools::displayDate($this->getFieldValue($obj, 'date_upd'), $cookie->id_lang).'</strong>
						</td>
						
					</tr>
			<tr>
			<td colspan="2">
			';
				
		
		$padrecanonical = Db::getInstance()->executeS("SELECT id_product, id_category_default, reference FROM product WHERE canonical = ".$obj->id."");
		$figlicanonical = array();
		foreach($padrecanonical as $padre) {
			$figlicanonical[] = $padre['reference'];
		}
		if(count($figlicanonical) > 0) {
			echo "<strong style='color:red'>ATTENZIONE</strong>: questo prodotto è il canonical di altri prodotti. Questi sono i codici dei prodotti:<br /> ";
			foreach ($padrecanonical as $figlio) { echo "<a href='index.php?tab=AdminCatalog&id_product=".$figlio['id_product']."&id_category=".$figlio['id_category_default']."&addproduct&token=".$this->token."' target='_blank' style='text-decoration:underline'>".$figlio['reference']."</a> ; "; }
			echo "<br /><strong>FARE ATTENZIONE se si vuole cancellare questo prodotto, il canonical non esisterebbe pi&ugrave;<br /><br />";
		}
		
		
		
		echo '</td></tr>
				</table>
				<table cellpadding="5" style="width: 45%; float: left; margin-left: 10px;">
					<tr>
						<td style="vertical-align:top;text-align:left;padding-right:10px;font-weight:bold;">'.$this->l('Status:').'</td>
						<td style="padding-bottom:5px;">
							<input style="float:left;" onclick="toggleDraftWarning(false);showOptions(true);" type="radio" name="active" id="active_on" value="1" '.($this->getFieldValue($obj, 'active') == 1 ? 'checked="checked" ' : '').'/>
							<label for="active_on" class="t"><img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" style="float:left; padding:0px 5px 0px 5px;" />'.$this->l('Enabled').'</label>
							<br class="clear" />
							<input style="float:left;" onclick="toggleDraftWarning(true);showOptions(false);"  type="radio" name="active" id="active_off" value="0" '.($this->getFieldValue($obj, 'active') == 0 ? 'checked="checked" ' : '').'/>
							<label for="active_off" class="t"><img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" style="float:left; padding:0px 5px 0px 5px" />'.$this->l('Disabled').'</label>
							<br class="clear" />
							<input style="float:left;" onclick=""  type="radio" name="active" id="active_old" value="2" '.($this->getFieldValue($obj, 'active') == 2 ? 'checked="checked" ' : '').'/> 
							<label for="active_old" class="t"><img src="../img/admin/time.gif" alt="'.$this->l('Old').'" title="'.$this->l('Old').'" style="float:left; padding:0px 5px 0px 5px" />'.$this->l('Old'),'</label>
						</td>
					</tr>
					';
					
					
					echo '<tr id="product_options" style="display:none">
						<td style="vertical-align:top;text-align:left;padding-right:10px;font-weight:bold;">'.$this->l('Options:').'</td>
						<td style="padding-bottom:5px;">
							<input style="float: left;" type="checkbox" name="available_for_order" id="available_for_order" value="1" '.($this->getFieldValue($obj, 'available_for_order') ? 'checked="checked" ' : '').' onclick="if ($(this).is(\':checked\')){$(\'#show_price\').attr(\'checked\', \'checked\');$(\'#show_price\').attr(\'disabled\', \'disabled\');}else{$(\'#show_price\').attr(\'disabled\', \'\');}"/>
							<label for="available_for_order" class="t"><img src="../img/admin/products.gif" alt="'.$this->l('available for order').'" title="'.$this->l('available for order').'" style="float:left; padding:0px 5px 0px 5px" />'.$this->l('available for order').'</label>
							<br class="clear" />
							<input style="float: left;" type="checkbox" name="show_price" id="show_price" value="1" '.($this->getFieldValue($obj, 'show_price') ? 'checked="checked" ' : '').' />
							<label for="show_price" class="t"><img src="../img/admin/gold.gif" alt="'.$this->l('display price').'" title="'.$this->l('show price').'" style="float:left; padding:0px 5px 0px 5px" />'.$this->l('show price').'</label>
							<br class="clear" />
							<input style="float: left;" type="checkbox" name="online_only" id="online_only" value="1" '.($this->getFieldValue($obj, 'online_only') ? 'checked="checked" ' : '').' />
							<label for="online_only" class="t"><img src="../img/admin/basket_error.png" alt="'.$this->l('online only').'" title="'.$this->l('online only').'" style="float:left; padding:0px 5px 0px 5px" />'.$this->l('online only (not sold in store)').'</label>
						</td>
					</tr>';
					
					echo '
					<tr>
						<td style="vertical-align:top;text-align:left;padding-right:10px;font-weight:bold;">'.$this->l('Condizione:').'</td>
						<td style="padding-bottom:5px;">
							<select name="condition" id="condition">
								<option value="new" '.($obj->condition == 'new' ? 'selected="selected"' : '').'>'.$this->l('New').'</option>
								<option value="used" '.($obj->condition == 'used' ? 'selected="selected"' : '').'>'.$this->l('Used').'</option>
								<option value="refurbished" '.($obj->condition == 'refurbished' ? 'selected="selected"' : '').'>'.$this->l('Refurbished').'</option>
							</select>
						</td>
					</tr>
					<tr>
						<td style="vertical-align:top;text-align:left;padding-right:10px;font-weight:bold;">'.$this->l('Tipo disponibilità').'</td><td style="padding-bottom:5px;">
  <select name="dispo_type" id="dispo_type">
  <option value="0" '.($this->getFieldValue($obj, 'dispo_type') == 0 ? 'selected="selected"' : '').'>Tutto</option>
  <option value="1" '.($this->getFieldValue($obj, 'dispo_type') == 1 ? 'selected="selected"' : '').'>Solo magazzino EZ</option>
  <option value="2" '.($this->getFieldValue($obj, 'dispo_type') == 2 ? 'selected="selected"' : '').'>Fornitore principale + Mag. EZ</option>
	</select>
</td></tr>
					<tr>
						<td style="vertical-align:top;text-align:left;padding-right:10px;font-weight:bold;">'.$this->l('Available from:').'</td><td>
  <input type="text" size="10" id="date_available" name="date_available" value="'.(($from = $this->getFieldValue($obj, 'date_available') AND $from != '1946-01-01') ? date("d-m-Y",strtotime($from)) : date('d-m-Y')).'" />
    &nbsp; &nbsp; &nbsp; <strong>'.$this->l('Data arrivo Allnet:').'</strong> <input size="10" type="text" id="data_arrivo" name="data_arrivo" value="'.(($from = $this->getFieldValue($obj, 'data_arrivo')) ? ($from == '0000-00-00' || $from == '1970-01-01' || $from == '1946-01-01' ? '' : date("d-m-Y",strtotime($from))) : date('d-m-Y')).'" /> 

</td></tr>



<tr>
						<td style="vertical-align:top;text-align:left;padding-right:10px;font-weight:bold;">Togli \'Ottieni miglior prezzo\'</td><td>
 <input type="checkbox" name="ottieni_miglior_prezzo_flag" id="ottieni_miglior_prezzo_flag" '.($this->getFieldValue($obj, 'ottieni_miglior_prezzo_flag') == 1 ? 'checked="checked"' : '').'" /> 

</td></tr>



<td style="vertical-align:top;text-align:left;padding-right:10px;font-weight:bold;">'.$this->l('Manufacturer:').'</td>
						<td style="padding-bottom:5px;">
						<script type="text/javascript">
							function getSupplierByManufacturer() {
								var supplier = $("option:selected", "#id_manufacturer").attr("data-supplier");
								document.getElementById("id_supplier").value = supplier;
								var other_suppliers = $("option:selected", "#id_manufacturer").attr("data-othersuppliers");
								var other_suppliers_array = other_suppliers.split(";");
								for(var z=0; z<other_suppliers_array.length;z++){
									item = other_suppliers_array[z];
									if(item.length != 0)
										add_riga_selected(item);
								}
							}
							
							function getMargins()
							{
								var margin_min_cli = $("option:selected", "#id_manufacturer").attr("data-margin-cli");
								var margin_min_riv = $("option:selected", "#id_manufacturer").attr("data-margin-riv");
								
								document.getElementById("margin_min_cli").value = margin_min_cli;
								document.getElementById("margin_min_riv").value = margin_min_riv;
							}	

						
						</script>

							<select name="id_manufacturer" id="id_manufacturer" onchange="getSupplierByManufacturer(); getMargins()">
								<option value="0">-- '.$this->l('Choose (optional)').' --</option>';
		if ($id_manufacturer = $this->getFieldValue($obj, 'id_manufacturer'))
			echo '				<option value="'.$id_manufacturer.'" selected="selected"  data-margin-cli="'.Db::getInstance()->getValue('SELECT margin_min_cli FROM manufacturer WHERE id_manufacturer = '.$id_manufacturer).'" data-margin-riv="'.Db::getInstance()->getValue('SELECT margin_min_riv FROM manufacturer WHERE id_manufacturer = '.$id_manufacturer).'" data-supplier="'.Manufacturer::getSupplierById($id_manufacturer).'" data-othersuppliers="'.Manufacturer::getOtherSuppliersById($id_manufacturer).'">'.Manufacturer::getNameById($id_manufacturer).'</option>
								<option disabled="disabled">----------</option>';
		echo '
							</select>&nbsp;&nbsp;&nbsp;<a href="?tab=AdminManufacturers&addmanufacturer&token='.Tools::getAdminToken('AdminManufacturers'.(int)(Tab::getIdFromClassName('AdminManufacturers')).(int)($cookie->id_employee)).'" onclick="return confirm(\''.$this->l('Are you sure you want to delete product information entered?', __CLASS__, true, false).'\');"><img src="../img/admin/add.gif" alt="'.$this->l('Create').'" title="'.$this->l('Create').'" /> <b>'.$this->l('Create').'</b></a>
						</td>
					</tr>
					<tr>
						<td style="vertical-align:top;text-align:left;padding-right:10px;font-weight:bold;">'.$this->l('Supplier:').'</td>
						<td style="padding-bottom:5px;">
							<select name="id_supplier" id="id_supplier">
								<option value="0">-- '.$this->l('Choose (optional)').' --</option>';
		if ($id_supplier = $this->getFieldValue($obj, 'id_supplier'))
			echo '				<option value="'.$id_supplier.'" selected="selected">'.Supplier::getNameById($id_supplier).'</option>
								<option disabled="disabled">----------</option>';
		echo '
							</select>&nbsp;&nbsp;&nbsp;<a href="?tab=AdminSuppliers&addsupplier&token='.Tools::getAdminToken('AdminSuppliers'.(int)(Tab::getIdFromClassName('AdminSuppliers')).(int)($cookie->id_employee)).'" onclick="return confirm(\''.$this->l('Are you sure you want to delete entered product information?', __CLASS__, true, false).'\');"><img src="../img/admin/add.gif" alt="'.$this->l('Create').'" title="'.$this->l('Create').'" /> <b>'.$this->l('Create').'</b></a>
						</td>
					</tr>';
					
					echo '<tr>
						<td style="vertical-align:top;text-align:left;padding-right:10px;font-weight:bold;">'.$this->l('Altri fornitori:').'</td>
						<td style="padding-bottom:5px;">
						<script type="text/javascript">
						function add_riga() {
				
							$("<div><select name=\'other_suppliers[]\'><option name=\'0\' value=\'0\'>-- Scegli (opzionale) --</option>';
							$resultssup = Db::getInstance()->ExecuteS("SELECT id_supplier, name FROM supplier ORDER BY name");
							foreach ($resultssup as $rowsup) {
				
								echo "<option name='$rowsup[id_supplier]' value='$rowsup[id_supplier]'"; 
								echo ">$rowsup[name]</option>";
							
							}
							
							echo '</select><br /><br /></div>").appendTo("#selezione-fornitori");

						}
						
						function add_riga_selected(id_supplier) {
				
							$("<div><select name=\'other_suppliers[]\'><option name=\'0\' value=\'0\'>-- Scegli (opzionale) --</option>';
							$resultssup = Db::getInstance()->ExecuteS("SELECT id_supplier, name FROM supplier ORDER BY name");
							foreach ($resultssup as $rowsup) {
				
								echo "<option name='$rowsup[id_supplier]' value='$rowsup[id_supplier]'\" + (id_supplier == ".$rowsup['id_supplier']." ? \"selected='selected'\" : \"\") +\""; 
								echo ">$rowsup[name]</option>";
							
							}
							
							echo '</select><br /><br /></div>").appendTo("#selezione-fornitori");

						}
						
						function rimuovi_riga() {
							$("#selezione-fornitori").children().last().remove();
						}
			
						</script>
						<div id="selezione-fornitori">';
						
						$other_suppliers = Db::getInstance()->getValue('SELECT other_suppliers FROM product WHERE id_product = '. $this->getFieldValue($obj, 'id').'');
					
						$other_suppliers = unserialize($other_suppliers);
						
						foreach ($other_suppliers as $supplier)
						{
						
							echo '
							<div><select name="other_suppliers[]">
							<option name="0" value="0">-- Scegli (opzionale) --</option>
							';
							
							
							$resultssup = Db::getInstance()->ExecuteS("SELECT id_supplier, name FROM supplier ORDER BY name");
							
							foreach ($resultssup as $rowsup) {
							
								echo "<option name='$rowsup[id_supplier]' ".($rowsup['id_supplier'] == $supplier ? "selected='selected'" : '')." value='$rowsup[id_supplier]'"; 
								echo ">$rowsup[name]</option>";
							
							}
				
					
							echo '</select>';
							echo '<br /><br /></div>';
				
						}
				echo '</div>
				
				
				<a href="javascript:void(0)" onclick="javascript:add_riga(); return false;"><img src="../img/admin/add.gif" alt="'.$this->l('Add').'" title="'.$this->l('Add').'" /> <b>'.$this->l('Aggiungi forn.').'</b></a>
				<a href="javascript:void(0)" onclick="javascript:rimuovi_riga(); return false;"><img src="../img/admin/forbbiden.gif" alt="'.$this->l('Remove').'" title="'.$this->l('Remove').'" /> <b>'.$this->l('Rimuovi forn.').'</b></a>
				<input type="hidden" name="other_suppliers_2" id="other_suppliers_2" /><!-- serve per ordinamento --><br />
						</td>
					</tr>';
					
					echo '<tr>
						<td style="vertical-align:top;text-align:left;padding-right:10px;font-weight:bold;">'.$this->l('Note forn.:').'</td>
						<td style="padding-bottom:5px;">
							<textarea name="note_fornitore" style="width: 230px; margin-right: 5px;">'.htmlentities($this->getFieldValue($obj, 'note_fornitore'), ENT_COMPAT, 'UTF-8').'</textarea>
						</td>
					</tr>'; 
					/*
 * Form for add a virtual product like software, mp3, etc...
 */
	$productDownload = new ProductDownload();
	if ($id_product_download = $productDownload->getIdFromIdProduct($this->getFieldValue($obj, 'id')))
		$productDownload = new ProductDownload($id_product_download);

?>
	<script type="text/javascript">
	// <![CDATA[
		ThickboxI18nImage = '<?php echo $this->l('Image') ?>';
		ThickboxI18nOf = '<?php echo $this->l('of') ?>';
		ThickboxI18nClose = '<?php echo $this->l('Close') ?>';
		ThickboxI18nOrEscKey = '<?php echo $this->l('(or "Esc")') ?>';
		ThickboxI18nNext = '<?php echo $this->l('Next >') ?>';
		ThickboxI18nPrev = '<?php echo $this->l('< Previous') ?>';
		tb_pathToImage = '../img/loadingAnimation.gif';
	//]]>
	</script>
	<script type="text/javascript" src="<?php echo _PS_JS_DIR_ ?>jquery/thickbox-modified.js"></script>
	<script type="text/javascript" src="<?php echo _PS_JS_DIR_ ?>jquery/ajaxfileupload.js"></script>
	<script type="text/javascript" src="<?php echo _PS_JS_DIR_ ?>date.js"></script>
	<style type="text/css">
		<!--
		@import url(<?php echo _PS_CSS_DIR_?>thickbox.css);
		-->
	</style>
	<script type="text/javascript">
	//<![CDATA[
	function toggleVirtualProduct(elt)
	{
		if (elt.checked)
		{
			$('#virtual_good').show('slow');
			$('#virtual_good_more').show('slow');
			getE('out_of_stock_1').checked = 'checked';
			getE('out_of_stock_2').disabled = 'disabled';
			getE('out_of_stock_3').disabled = 'disabled';
			getE('label_out_of_stock_2').setAttribute('for', '');
			getE('label_out_of_stock_3').setAttribute('for', '');
		}
		else
		{
			$('#virtual_good').hide('slow');
			$('#virtual_good_more').hide('slow');
			getE('out_of_stock_2').disabled = false;
			getE('out_of_stock_3').disabled = false;
			getE('label_out_of_stock_2').setAttribute('for', 'out_of_stock_2');
			getE('label_out_of_stock_3').setAttribute('for', 'out_of_stock_3');
		}
	}

	function uploadFile()
	{
		$.ajaxFileUpload (
			{
				url:'./uploadProductFile.php',
				secureuri:false,
				fileElementId:'virtual_product_file',
				dataType: 'xml',

				success: function (data, status)
				{
					data = data.getElementsByTagName('return')[0];
					var result = data.getAttribute("result");
					var msg = data.getAttribute("msg");
					var fileName = data.getAttribute("filename");

					if (result == "error")
					{
						$("#upload-confirmation").html('<p>error: ' + msg + '</p>');
					}
					else
					{
						$('#virtual_product_file').remove();
						$('#virtual_product_file_label').hide();
						$('#file_missing').hide();
						new_href = $('#delete_downloadable_product').attr('href').replace('%26deleteVirtualProduct%3Dtrue', '%26file%3D'+msg+'%26deleteVirtualProduct%3Dtrue');
						$('#delete_downloadable_product').attr('href', new_href);
						$('#delete_downloadable_product').show();
						$('#virtual_product_name').attr('value', fileName);
						$('#upload-confirmation').html(
							'<a class="link" href="get-file-admin.php?file='+msg+'&filename='+fileName+'"><?php echo $this->l('The file') ?>&nbsp;"' + fileName + '"&nbsp;<?php echo $this->l('has successfully been uploaded') ?></a>' +
							'<input type="hidden" id="virtual_product_filename" name="virtual_product_filename" value="' + msg + '" />');
					}
				}
			}
		);
	}

	//]]>
	</script>

<tr>
						<td colspan="2">
 <input type="checkbox" name="prodotto_con_canone" id="prodotto_con_canone" <?php echo ($this->getFieldValue($obj, 'prodotto_con_canone') == 1 ? 'checked="checked"' : ''); ?> /><strong> Servizio con canone ricorrente?</strong></td>

</tr>

	<tr>
		<td colspan="2">
			<p><input type="checkbox" id="is_virtual_good" name="is_virtual_good" value="true" onclick="toggleVirtualProduct(this);" <?php if (($productDownload->id OR Tools::getValue('is_virtual_good')=='true') AND $productDownload->active) echo 'checked="checked"' ?> />
			<label for="is_virtual_good" class="t bold" style="color: black;"><?php echo $this->l('Is this a downloadable product?') ?></label></p>
			<div id="virtual_good" <?php if (!$productDownload->id OR !$productDownload->active) echo 'style="display:none;"' ?> >
	<?php if (!ProductDownload::checkWritableDir()): ?>
		<p class="alert">
			<?php echo $this->l('Your download repository is not writable.'); ?><br/>
			<?php echo realpath(_PS_DOWNLOAD_DIR_); ?>
		</p>
	<?php else: ?>
			<?php if ($productDownload->id) echo '<input type="hidden" id="virtual_product_id" name="virtual_product_id" value="'.$productDownload->id.'" />' ?>
				<p class="block">
	<?php if (!$productDownload->checkFile()): ?>

			<!--	<div style="padding:5px;width:50%;float:left;margin-right:20px;border-right:1px solid #E0D0B1"> -->
			<div>
		<?php /* if ($productDownload->id): ?>
					<p class="alert" id="file_missing">
						<?php echo $this->l('This product is missing') ?>:<br/>
						<?php echo realpath(_PS_DOWNLOAD_DIR_) .'/'. $productDownload->physically_filename ?>
					</p>
		<?php endif; ?> */ ?>
					<p style="display:none"><?php
						$max_upload = (int)ini_get('upload_max_filesize');
						$max_post = (int)ini_get('post_max_size');
						$upload_mb = min($max_upload, $max_post);
					echo $this->l('Your server\'s maximum upload file size is') . ':&nbsp;'.$upload_mb.$this->l('Mb') ?></p>
					<?php if (!strval(Tools::getValue('virtual_product_filename'))): ?>
					<label style="display:none" id="virtual_product_file_label" for="virtual_product_file" class="t"><?php echo $this->l('Upload a file') ?></label>
					<p style="display:none"><input  style="display:none" type="file" id="virtual_product_file" name="virtual_product_file" onchange="uploadFile();" /></p>
					<?php endif; ?>
					<div id="upload-confirmation" style="display:none">
					<?php if ($up_filename = strval(Tools::getValue('virtual_product_filename'))): ?>
						<input type="hidden" id="virtual_product_filename" name="virtual_product_filename" value="<?php echo $up_filename ?>" />
					<?php endif; ?>
					</div>
					<a style="display:none" id="delete_downloadable_product" style="display:none;" href="confirm.php?height=200&amp;width=300&amp;modal=true&amp;referer=<?php echo rawurlencode($_SERVER['REQUEST_URI'].'&deleteVirtualProduct=true') ?>" class="thickbox red" title="<?php echo $this->l('Delete this file') ?>"><?php echo $this->l('Delete this file') ?></a>
	<?php else: ?>
					<input type="hidden" id="virtual_product_filename" name="virtual_product_filename" value="<?php echo $productDownload->physically_filename ?>" />
					<?php echo $this->l('This is the link').':&nbsp;'.$productDownload->getHtmlLink(false, true) ?>
					<a style="display:none" href="confirm.php?height=200&amp;width=300&amp;modal=true&amp;referer=<?php echo rawurlencode($_SERVER['REQUEST_URI'].'&deleteVirtualProduct=true') ?>" class="thickbox red" title="<?php echo $this->l('Delete this file') ?>"><?php echo $this->l('Delete this file') ?></a>
	<?php endif; // check if file exists ?>
				</p>
				
					<label for="virtual_product_name" class="t"><?php echo $this->l('Filename') ?></label>
					<input type="text" id="virtual_product_name" name="virtual_product_name" style="width:200px" value="<?php echo (Tools::getIsset('virtual_product_name') ? Tools::getValue('virtual_product_name') : $productDownload->id > 0 ? $productDownload->display_filename : $obj->reference) ?>" />
					<span class="hint" name="help_box" style="display:none;"><?php echo $this->l('The full filename with its extension (e.g., Book.pdf)') ?></span>
				

				</div>
				<div style="display:none" id="virtual_good_more" style="<?php if (!$productDownload->id OR !$productDownload->active) echo 'display:none;' ?>padding:5px;width:40%;float:left;margin-left:10px">

				<p class="block" style="display:none">
					<label for="virtual_product_nb_downloable" class="t"><?php echo $this->l('Number of downloads') ?></label>
					<input type="text" id="virtual_product_nb_downloable" name="virtual_product_nb_downloable" value="<?php echo $productDownload->id > 0 ? $productDownload->nb_downloadable : htmlentities(Tools::getValue('virtual_product_nb_downloable'), ENT_COMPAT, 'UTF-8') ?>" class="" size="6" />
					<span class="hint" name="help_box" style="display:none"><?php echo $this->l('Number of authorized downloads per customer') ?></span>
				</p>
				<p class="block" style="display:none">
					<label for="virtual_product_expiration_date" class="t"><?php echo $this->l('Expiration date') ?></label>
					<input type="text" id="virtual_product_expiration_date" name="virtual_product_expiration_date" value="<?php echo ($productDownload->id > 0) ? ((!empty($productDownload->date_expiration) AND $productDownload->date_expiration != '0000-00-00 00:00:00') ? date('Y-m-d', strtotime($productDownload->date_expiration))
: '' ) : htmlentities(Tools::getValue('virtual_product_expiration_date'), ENT_COMPAT, 'UTF-8') ?>" size="11" maxlength="10" autocomplete="off" /> <?php echo $this->l('Format: YYYY-MM-DD'); ?>
					<span class="hint" name="help_box" style="display:none"><?php echo $this->l('No expiration date if you leave this blank'); ?></span>
				</p>
				<p class="block" style="display:none">
					<label for="virtual_product_nb_days" class="t"><?php echo $this->l('Number of days') ?></label>
					<input type="text" id="virtual_product_nb_days" name="virtual_product_nb_days" value="<?php echo $productDownload->id > 0 ? $productDownload->nb_days_accessible : htmlentities(Tools::getValue('virtual_product_nb_days'), ENT_COMPAT, 'UTF-8') ?>" class="" size="4" /><sup> *</sup>
					<span class="hint" name="help_box" style="display:none"><?php echo $this->l('How many days this file can be accessed by customers') ?> - <em>(<?php echo $this->l('set to zero for unlimited access'); ?>)</em></span>
				</p>
				</div>
	<?php endif; // check if download directory is writable ?>
			</div>
		</td>
	</tr>
	</table>
	<script type="text/javascript">
		if ($('#is_virtual_good').attr('checked'))
		{
			$('#virtual_good').show('slow');
			$('#virtual_good_more').show('slow');
		}
	</script>
					<?php 
					
					include_once('functions.php');
		includeDatepicker3(array('date_available'), true);
		includeDatepicker3(array('data_arrivo'), true);
					
					$rowconfezione=Db::getInstance()->getRow("SELECT feature_value_lang.value FROM feature_value_lang JOIN feature_value ON feature_value.id_feature_value = feature_value_lang.id_feature_value JOIN feature_product ON feature_value.id_feature_value = feature_product.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.id_feature = 450 AND feature_product.id_product = $obj->id");
					
					
					$rowgaranzia=Db::getInstance()->getRow("SELECT feature_value_lang.id_feature_value FROM feature_value_lang JOIN feature_value ON feature_value.id_feature_value = feature_value_lang.id_feature_value JOIN feature_product ON feature_value.id_feature_value = feature_product.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.id_feature = 449 AND feature_product.id_product = $obj->id");
					
					/*echo '
					<tr>
						<td style="vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;">Confezione</td>
						<td style="padding-bottom:5px;">
							<input size="55" type="text" name="confezione" value="'.$rowconfezione['value'].'" />
						</td>
					</tr>';
					
					echo '
					<tr>
						<td style="vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;">Garanzia costruttore</td>
						<td style="padding-bottom:5px;">
						<select name="garanzia">';
						
					$selectgaranzia = mysql_query("SELECT * FROM feature_value_lang JOIN feature_value ON feature_value.id_feature_value = feature_value_lang.id_feature_value WHERE feature_value_lang.id_lang = 5 AND feature_value.id_feature = 449 AND custom = 0");
					
					while($rowselectgaranzia = mysql_fetch_array($selectgaranzia, MYSQL_ASSOC)) {
					echo "<option name='$rowselectgaranzia[value]' value='$rowselectgaranzia[id_feature_value]'"; if($rowselectgaranzia['id_feature_value'] == $rowgaranzia['id_feature_value']) { echo "selected = 'selected'"; } else { } echo ">$rowselectgaranzia[value]</option>";

					}
						
						
						echo '</select>
						</td>
					</tr>';*/
					
					
					echo '
				</table>
				<div class="clear"></div>
				';
					
					$prezzi_esolver = Db::getInstance()->getRow('SELECT * FROM product_esolver WHERE id_product = '.$obj->id);
					
					if(!$prezzi_esolver)
					{
						$prezzi_esolver['rebate_1'] = 0;
						$prezzi_esolver['rebate_2'] = 0;
						$prezzi_esolver['rebate_3'] = 0;
						$prezzi_esolver['wholesale_price'] = $obj->wholesale_price;
					}

						
					$prezzi_esolver['margin_login_for_offer'] = Db::getInstance()->getValue('SELECT margin_login_for_offer FROM product  WHERE id_product = '.$obj->id);
					
					echo '
					<script type="text/javascript">
					function calcolaScontoRivenditore2()
					{
						var numprice_riv_1 = document.getElementById(\'prezzo_riv_1\').value;
						var price_riv_1 = parseFloat(numprice_riv_1.replace(/\s/g, "").replace(",", "."));
						
						var listino = document.getElementById(\'listino_esolver\').value;
						var price_listino = parseFloat(listino.replace(/\s/g, "").replace(",", "."));
						
						var prezzo_riv_2 = price_riv_1 + ((price_riv_1/100 )*3);
						
						if(prezzo_riv_2 > price_listino)
							prezzo_riv_2 = price_listino;
						
						document.getElementById(\'prezzo_riv_2\').value=prezzo_riv_2;
						
						calcSconto(\'listino\', \'prezzo_riv_2\', \'sconto_rivenditore_2\'); 
						
						var sconto_riv_2 = document.getElementById(\'sconto_rivenditore_2\').value;
						var sco_riv_2 = parseFloat(sconto_riv_2.replace(/\s/g, "").replace(",", "."));
						calcMarginalitaSconto(\'sconto_rivenditore_2\', \'margin_riv_2\', \'listino\'); 
						
					}
					</script>
					<div style="border:1px solid black; background-color:#ffffff; margin-top:10px; padding-left:5px; padding-top:10px; margin-bottom:20px; ">
					<div style="float:left; margin-top:0px"><h2>eSolver</h2></div><div style="float:left; margin-left:20px">
						<table cellspacing="0" cellpadding="0" style="float:left"><tr style="background-color:#ebebeb">
							<td class="col-left" style="width:130px">'.$this->l('Listino').'
								'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input required class="esolver_input" size="6"  maxlength="14" id="listino_esolver" name="listino_esolver" type="text" style="text-align:right" value="'.number_format(htmlentities($this->getFieldValue($obj, 'listino'), ENT_COMPAT, 'UTF-8'),2,'.','').'" onblur="" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="
								document.getElementById(\'listino\').value = this.value.replace(/,/g, \'.\');
								calcPrezzoSconto(\'scontolistinovendita\', \'priceTE\', \'listino\');
								calcScontoAcquisto1(); calcScontoAcquisto2(); calcScontoAcquisto3(); 
								calcMarginalita(); calcSconto(\'listino\', \'priceTE\', \'scontolistinovendita\');
								calcMarginalitaSconto(\'sconto_rivenditore_1\', \'margin_riv_1\', \'listino\'); calcPrezzoSconto(\'sconto_rivenditore_1\', \'prezzo_riv_1\', \'listino\');
								calcMarginalitaSconto(\'sconto_rivenditore_2\', \'margin_riv_2\', \'listino\'); calcPrezzoSconto(\'sconto_rivenditore_2\', \'prezzo_riv_2\', \'listino\');
								calcMarginalitaSconto(\'sconto_rivenditore_3\', \'margin_riv_3\', \'listino\'); calcPrezzoSconto(\'sconto_rivenditore_3\', \'prezzo_riv_3\', \'listino\');
								aggiornaScontisticaAmazon(); calcolaScontoRivenditore2();
								calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_1\', \'prezzo_qta_1\', \'priceTE\');
								calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_2\', \'prezzo_qta_2\', \'priceTE\');
								calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_3\', \'prezzo_qta_3\', \'priceTE\');
								calcPrezzoFromMargine(\'wholesale_price\', \'margin_login_for_offer\', \'prezzo_login_for_offer\');
								
								" />'.($currency->format % 2 == 0 ? ' '.$currency->sign : '').'
							<input type="hidden" value="'.$this->getFieldValue($obj, 'listino').'" name="vecchio_listino" />	
							</td>
						
							
						</tr></table>';
						echo '
						<table  cellspacing="0" cellpadding="0" style="float:left"><tr style="background-color:yellow">
						<td class="col-left" style="width:120px">'.$this->l('Sc.Acq.1').'
								'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input class="esolver_input" style="text-align:right"  size="2" maxlength="14" id="sconto_acquisto_1" name="sconto_acquisto_1" type="text" value="'.number_format(htmlentities($this->getFieldValue($obj, 'sconto_acquisto_1'), ENT_COMPAT, 'UTF-8'),2,'.','').'" required onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; if (isArrowKey(event)) return; calcScontoAcquisto3(); calcRebate(); calcMarginalita(); 
							
								calcMarginalitaSconto(\'sconto_rivenditore_1\', \'margin_riv_1\', \'listino\'); 
								calcMarginalitaSconto(\'sconto_rivenditore_2\', \'margin_riv_2\', \'listino\');
								calcMarginalitaSconto(\'sconto_rivenditore_3\', \'margin_riv_3\', \'listino\');
								calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); 
								calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); 
								calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\');
								aggiornaScontisticaAmazon();						
								calcPrezzoFromMargine(\'wholesale_price\', \'margin_login_for_offer\', \'prezzo_login_for_offer\');
								
								" />%
							
							</td>
							
									
						
								<td class="col-left" style="width:120px">'.$this->l('Sc.Acq.2').'
								'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input class="esolver_input" style="text-align:right" size="2" maxlength="14" id="sconto_acquisto_2" name="sconto_acquisto_2" required type="text" value="'.number_format(htmlentities($this->getFieldValue($obj, 'sconto_acquisto_2'), ENT_COMPAT, 'UTF-8'),2,'.','').'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; if (isArrowKey(event)) return; calcScontoAcquisto3(); calcRebate(); calcMarginalita(); 
							
								calcMarginalitaSconto(\'sconto_rivenditore_1\', \'margin_riv_1\', \'listino\'); 
								calcMarginalitaSconto(\'sconto_rivenditore_2\', \'margin_riv_2\', \'listino\');
								calcMarginalitaSconto(\'sconto_rivenditore_3\', \'margin_riv_3\', \'listino\');
								calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); 
								calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); 
								calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\');
								aggiornaScontisticaAmazon();	
								calcPrezzoFromMargine(\'wholesale_price\', \'margin_login_for_offer\', \'prezzo_login_for_offer\');
								
								" />%
							
							</td>
					
								<td class="col-left" style="width:120px">'.$this->l('Sc.Acq.3').'
								'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input class="esolver_input" style="text-align:right" size="2" maxlength="14" id="sconto_acquisto_3" name="sconto_acquisto_3" required type="text" value="'.number_format(htmlentities($this->getFieldValue($obj, 'sconto_acquisto_3'), ENT_COMPAT, 'UTF-8'),2,'.','').'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; if (isArrowKey(event)) return; calcScontoAcquisto3(); calcRebate(); calcMarginalita(); 
								
								calcMarginalitaSconto(\'sconto_rivenditore_1\', \'margin_riv_1\', \'listino\'); 
								calcMarginalitaSconto(\'sconto_rivenditore_2\', \'margin_riv_2\', \'listino\');
								calcMarginalitaSconto(\'sconto_rivenditore_3\', \'margin_riv_3\', \'listino\');
								calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); 
								calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); 
								calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\');
								aggiornaScontisticaAmazon();
								calcPrezzoFromMargine(\'wholesale_price\', \'margin_login_for_offer\', \'prezzo_login_for_offer\');

								"  />%
							
							</td>
						
							<td class="col-left" style="width:160px">'.$this->l('Acquisto netto').'
								'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input class="esolver_input" style="text-align:right" size="6" maxlength="14" id="wholesale_price_esolver" name="wholesale_price_esolver" required type="text" value="'.(Tools::getIsset('wholesale_price_esolver') ? Tools::getValue('wholesale_price_esolver') : number_format($prezzi_esolver['wholesale_price'],2,'.','')).'" onchange="this.value = this.value.replace(/,/g, \'.\');
								
								calcSconto(\'listino\', \'wholesale_price_esolver\', \'sconto_acquisto_1\');
								document.getElementById(\'sconto_acquisto_2\').value = 0;
								document.getElementById(\'sconto_acquisto_3\').value = 0;
								calcRebate();
calcMarginalita();
								calcMarginalitaSconto(\'sconto_rivenditore_1\', \'margin_riv_1\', \'listino\');
								calcMarginalitaSconto(\'sconto_rivenditore_2\', \'margin_riv_2\', \'listino\'); 
								calcMarginalitaSconto(\'sconto_rivenditore_3\', \'margin_riv_3\', \'listino\'); 
								calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); 
								calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); 
								calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\');
								aggiornaScontisticaAmazon();
								calcPrezzoFromMargine(\'wholesale_price\', \'margin_login_for_offer\', \'prezzo_login_for_offer\');
								
								" />'.($currency->format % 2 == 0 ? ' '.$currency->sign : '').'
								
							</td>';
						
						/*
						<td style="width:130px"  style="padding-bottom:0px;">
								'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="8" maxlength="14" id="wholesale_price" name="wholesale_price" type="text" value="'.number_format(($this->getFieldValue($obj, 'listino')*((100-$this->getFieldValue($obj, 'sconto_acquisto_1'))/100)*((100-$this->getFieldValue($obj, 'sconto_acquisto_2'))/100)*((100-$this->getFieldValue($obj, 'sconto_acquisto_3'))/100)),2,'.','').'" onkeyup="if (isArrowKey(event)) return; calcMarginalita(); " onchange="this.value = this.value.replace(/,/g, \'.\');" />'.($currency->format % 2 == 0 ? ' '.$currency->sign : '').'*/
								
								
								
								echo '
							</td>
						</tr></table>';
						
						if($cookie->id_employee == 2|| $cookie->id_employee == 1 || $cookie->id_employee == 6 || $cookie->id_employee == 22)
						
						echo '
						<table  cellspacing="0" cellpadding="0" style="float:left; margin-left:40px"><tr style="background-color:yellow">
						<td class="col-left" style="width:90px">'.$this->l('In $').' <input class="esolver_input" style="text-align:right"  size="6" maxlength="14" id="costo_dollari" name="costo_dollari" type="text" value="'.number_format(htmlentities($this->getFieldValue($obj, 'costo_dollari'), ENT_COMPAT, 'UTF-8'),2,'.','').'" onchange="this.value = this.value.replace(/,/g, \'.\');"  />
						</td></tr></table>
						
						
						';
						
						
					echo '</div>
						<div style="float:left; margin-left:5px; margin-top:-3px"><!-- <input type="button" class="button" id="esolver_edit" value="Modifica" onclick=" $(\'.esolver_input\').each(function () {
        if (this.readOnly == true) {
			document.getElementById(\'esolver_edit\').value = \'Blocca\';
			document.getElementById(\'esolver_save\').style.visibility = \'visible\';
			this.style.backgroundColor = \'#ffffff\';
            this.readOnly = false;
        } else {
				document.getElementById(\'esolver_edit\').value = \'Modifica\';
				document.getElementById(\'esolver_save\').style.visibility = \'hidden\';
				this.style.backgroundColor = \'#f0ebd6\';
            this.readOnly = true;
        }
    });" />&nbsp;&nbsp;&nbsp;<input type="button" class="button" style="visibility:hidden" id="esolver_save" value="Salva" onclick="salvaEsolver()" /> --></div>
	<div id="esolver_ok" style="float:left"></div><div style="clear:both"></div>
	<script type="text/javascript">
		function salvaEsolver()
		{
			var listino_esolver = document.getElementById("listino_esolver").value;
			var sconto_acquisto_1 = document.getElementById("sconto_acquisto_1").value;
			var sconto_acquisto_2 = document.getElementById("sconto_acquisto_2").value;
			var sconto_acquisto_3 = document.getElementById("sconto_acquisto_3").value;
			var rebate_1 = document.getElementById("rebate_1").value;
			var rebate_2 = document.getElementById("rebate_2").value;
			var rebate_3 = document.getElementById("rebate_3").value;
			var wholesale_price_esolver = document.getElementById("wholesale_price_esolver").value;
			
			$.ajax({
			  url:"ajax.php?submitEsolver=ok",
			  type: "POST",
			  data: { listino_esolver: listino_esolver, sconto_acquisto_1: sconto_acquisto_1,  sconto_acquisto_2: sconto_acquisto_2,  sconto_acquisto_3: sconto_acquisto_3, rebate_1: rebate_1, rebate_2: rebate_2, rebate_3: rebate_3, wholesale_price_esolver: wholesale_price_esolver, 
			  id_product: "'.$obj->id.'"
			  },
			  success:function(r){
				// alert(r);
				$("#esolver_ok").html("<img src=\"..\/img\/admin\/ok.gif\" alt=\"OK\" title=\"OK\"  \/> OK").fadeIn(100);
				$("#esolver_ok").html("<img src=\"..\/img\/admin\/ok.gif\" alt=\"OK\" title=\"OK\"  \/> OK").fadeOut(10000);
			  },
			  error: function(xhr,stato,errori){
				 alert("Errore durante l\'operazione:"+xhr.status);
			  }
			});
			
			
		}
	</script>
	
					</div>';
?>
	<?php
		echo '
		<script type="text/javascript" src="../js/price.js"></script>
		<script type="text/javascript">
			var newLabel = \''.$this->l('New label').'\';
			var choose_language = \''.$this->l('Choose language:').'\';
			var required = \''.$this->l('required').'\';
			var customizationUploadableFileNumber = '.(int)($this->getFieldValue($obj, 'uploadable_files')).';
			var customizationTextFieldNumber = '.(int)($this->getFieldValue($obj, 'text_fields')).';
			var uploadableFileLabel = 0;
			var textFieldLabel = 0;
		</script>';
		
		if ((int)Configuration::get('PS_STOCK_MANAGEMENT'))
				{

					if (!$has_attribute)
					{
						/*if ($obj->id)
						{*/
						
						$qta_ord_clienti = Db::getInstance()->getValue('SELECT sum(product_quantity - (REPLACE(cons,"0_",""))) FROM order_detail od JOIN orders o ON o.id_order = od.id_order
						JOIN cart c ON c.id_cart = o.id_cart
						JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order_state != 18 AND  moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE c.name NOT LIKE "%prime%" AND  od.product_id = '.$obj->id.' AND os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 35 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state != 20 AND os.id_order_state != 26 AND os.id_order_state != 29 AND os.id_order_state != 31 AND os.id_order_state != 32 AND os.id_order_state !=34 GROUP BY od.product_id');
						
						$ordini_impegnati = Db::getInstance()->executeS('SELECT o.id_order, os.id_order_state, o.id_customer, sum(product_quantity - (REPLACE(cons,"0_",""))) qt FROM order_detail od JOIN orders o ON o.id_order = od.id_order
						JOIN cart c ON c.id_cart = o.id_cart
						JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE moh.id_order_state != 18 AND moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE c.name NOT LIKE "%prime%" AND  od.product_id = '.$obj->id.' AND os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state != 20 AND os.id_order_state != 26 AND os.id_order_state != 29 AND os.id_order_state != 31  AND os.id_order_state != 32 AND os.id_order_state != 35 GROUP BY o.id_order');
						
						foreach($ordini_impegnati as $oo)
						{
							if($oo['qt'] == 0)
							{
								
							}
							else
								$ordini_impegnati_html .= '<tr><td><a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer='.$oo['id_customer'].'&viewcustomer&id_order='.$oo['id_order'].'&vieworder&tab-container-1=4&token='.$tokenCustomers.'" target="_blank">'.$oo['id_order'].'</a></td><td>'.Db::getInstance()->getValue('SELECT name FROM order_state_lang WHERE id_lang = 5 AND id_order_state = '.$oo['id_order_state']).'</td><td>'.$oo['qt'].'</td></tr>';
						}
						
						
						
						$venduto30 = Db::getInstance()->getValue('SELECT sum(product_quantity) from orders o join order_detail od on o.id_order = od.id_order join cart c on o.id_cart = c.id_cart where od.product_reference = "'.$obj->reference.'" and o.date_add BETWEEN NOW() - INTERVAL 30 DAY AND NOW()'); 
						$venduto60 = Db::getInstance()->getValue('SELECT sum(product_quantity) from orders o join order_detail od on o.id_order = od.id_order join cart c on o.id_cart = c.id_cart where od.product_reference = "'.$obj->reference.'" and o.date_add BETWEEN NOW() - INTERVAL 60 DAY AND NOW()'); 
						$venduto90 = Db::getInstance()->getValue('SELECT sum(product_quantity) from orders o join order_detail od on o.id_order = od.id_order join cart c on o.id_cart = c.id_cart where od.product_reference = "'.$obj->reference.'" and o.date_add BETWEEN NOW() - INTERVAL 90 DAY AND NOW()'); 
						$venduto120 = Db::getInstance()->getValue('SELECT sum(product_quantity) from orders o join order_detail od on o.id_order = od.id_order join cart c on o.id_cart = c.id_cart where od.product_reference = "'.$obj->reference.'" and o.date_add BETWEEN NOW() - INTERVAL 120 DAY AND NOW()'); 
						$venduto150 = Db::getInstance()->getValue('SELECT sum(product_quantity) from orders o join order_detail od on o.id_order = od.id_order join cart c on o.id_cart = c.id_cart where od.product_reference = "'.$obj->reference.'" and o.date_add BETWEEN NOW() - INTERVAL 150 DAY AND NOW()'); 
						$venduto180 = Db::getInstance()->getValue('SELECT sum(product_quantity) from orders o join order_detail od on o.id_order = od.id_order join cart c on o.id_cart = c.id_cart where od.product_reference = "'.$obj->reference.'" and o.date_add BETWEEN NOW() - INTERVAL 180 DAY AND NOW()'); 
						
						
						
						if(!$qta_ord_clienti)
							$qta_ord_clienti = 0;
						
						
							$scorta_minima = Db::getInstance()->getValue('SELECT scorta_minima FROM product WHERE id_product = '.$obj->id);
							
							$scorta_minima = (int)$scorta_minima;
							
							
							$sc_att = $this->getFieldValue($obj, 'stock_quantity')-$this->getFieldValue($obj, 'impegnato_quantity')+$this->getFieldValue($obj, 'ordinato_quantity');
							$sc_att = (int)$sc_att;
							
							$img_scorta = '';
							if($sc_att < $scorta_minima)
								$img_scorta = "<img style='border:2px solid red' src='../img/admin/error2.png' alt='Sotto scorta' title='Sotto scorta'>";
							else
							{
								if($this->getFieldValue($obj, 'ordinato_quantity') > 0 && $sc_att > $scorta_minima)
									$img_scorta = "<img style='border:2px solid green' src='../img/admin/delivery.gif' alt='In arrivo' title='In arrivo'>";
								else
									$img_scorta = '';
							}
							
							if($scorta_minima == 0)
								$img_scorta = '';
							
							
							$date_arrivo_ordinato = Db::getInstance()->getValue('SELECT data_arrivo_ordinato FROM product WHERE id_product = '.$obj->id);
							
							$new_date_arrivo_ordinato = '';
							$date_arrivo_ordinato = explode(';',$date_arrivo_ordinato);
							foreach($date_arrivo_ordinato as $dao)
							{
								if($dao != '')
									$new_date_arrivo_ordinato .= date('d-m-Y',strtotime($dao)).'<br />';
							}
							echo '
							<script>
								$(document).ready(function() {
									$(".span-reference-2").tooltipster({interactive: true});
								});
							</script>
				
							<tr>
						
							<td class="col-left">
							
							<table class="table"  cellpadding="0" cellspacing="0" style="border:1px solid #000; float:left; width:70%"><tr><th style="background: #ebebeb; text-align:center; border-bottom:1px solid #000" colspan="8">Disponibilit&agrave;</th></tr>
							<tr>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right; width:150px">'.$this->l('Magazzino EZ').'</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right">'.($obj->id ? $qty : 0).'</th>
							<th style="padding:0px; border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right; width:300px">
								
								<table style="width:100%; border-collapse:collapse; margin-top:0px; height:26px; margin-bottom:0px;border-bottom:0px"><tr><th style="border-right: 1px solid #bbb; border-bottom:0px solid #000; text-align:right;">'.$this->l('Netto').' <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Netto = Magazzino EZ - Qta ordinata dai clienti (sito)" title="Netto = Magazzino EZ - Qta ordinata dai clienti (sito)" /></th>
								<th style="border-right: 1px solid #bbb; border-bottom:0px solid #000;text-align:right">'.($qty - $qta_ord_clienti).'</th>
								
								<th style="border-right: 0px solid #bbb; border-bottom:0px solid #000;text-align:right">'.$this->l('Qta ordinata dai clienti (sito)').'</th></tr></table>
							</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right"><span style="cursor:pointer" class="span-reference-2" data-tooltip-content="#qta_ord_clienti_tooltip"><a href="javascript:void()">'.$qta_ord_clienti.'</a></span>
							
							<div class="tooltip_templates" style="display:none">
							<span id="qta_ord_clienti_tooltip"><table class=\'table\' style=\'width:250px\'><tr><th style="color:#000 !important">Ordine</th><th style="color:#000 !important">Stato</th><th style="color:#000 !important">Quantit&agrave;</th></tr>'.$ordini_impegnati_html.'</table></span>
							</div>
							</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right; width:300px">'.$this->l('Qta impegnata ord. caricati su gestionale (eSolver)').'</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'impegnato_quantity') : 0).'</th>
							</tr>
							
							<tr>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right">Mag. Amazon</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right">'.Db::getInstance()->getValue('SELECT amazon_quantity FROM product WHERE id_product = '.$obj->id).'</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right" >'.$this->l('Ordinato al fornitore').'</th>
							
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'ordinato_quantity') : 0).'</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right" colspan="1">Mag. EZ + Ordinato al fornitore - Impegnato sito</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right" >'.($qty + $this->getFieldValue($obj, 'ordinato_quantity') - $qta_ord_clienti).'</th>
							
							
							</tr>
							
							
							
							<tr>
							<'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Allnet').'</'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'supplier_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('In arrivo Allnet').'</'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'arrivo_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Data arr. Allnet').':</'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.(($from = $this->getFieldValue($obj, 'data_arrivo')) ? ($from == '0000-00-00' || $from == '1970-01-01' || $from == '1946-01-01' ? 'ND' : date("d-m-y",strtotime($from))) : date('d-m-y')).'</'.($this->getFieldValue($obj, 'id_supplier') == 11 ? 'th' : 'td').'>
							
							</tr>
							<tr>
							<'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Esprinet').'</'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'esprinet_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('In arrivo Esprinet').'</'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'arrivo_esprinet_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Data arr. Esprinet').':</'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.(($from = $this->getFieldValue($obj, 'data_arrivo_esprinet')) ? ($from == '0000-00-00' || $from == '1970-01-01' || $from == '1946-01-01' ? 'ND' : date("d-m-y",strtotime($from))) : date('d-m-y')).'</'.($this->getFieldValue($obj, 'id_supplier') == 38 ? 'th' : 'td').'>
							
							</tr>
							
							<tr>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Attiva').'</'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'attiva_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('In arrivo Attiva').'</'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'arrivo_attiva_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Data arr. Attiva').':</'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right"></'.($this->getFieldValue($obj, 'id_supplier') == 1450 ? 'th' : 'td').'>
							
							</tr>
							
							<tr>
							<'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Itancia').'</'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'itancia_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('In arrivo Itancia').'</'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">0</'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Data arr. Itancia').':</'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">ND</'.($this->getFieldValue($obj, 'id_supplier') == 42 ? 'th' : 'td').'>
							
							</tr>
							
							<tr>
							<'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Intracom').'</'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'intracom_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('In arrivo Intracom').'</'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'arrivo_intracom_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Data arr. Intracom').':</'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.(($from_intracom = $this->getFieldValue($obj, 'data_arrivo_intracom')) ? ($from_intracom == '0000-00-00' || $from_intracom == '1970-01-01' || $from_intracom == '1946-01-01' ? 'ND' : date("d-m-y",strtotime($from_intracom))) : date('d-m-y')).'</'.($this->getFieldValue($obj, 'id_supplier') == 95 ? 'th' : 'td').'>
							
							</tr>
							
							<tr>
							<'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('ASIT').'</'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? Db::getInstance()->getValue('SELECT asit_quantity FROM product WHERE id_product = '.$obj->id) : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right"></'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right"></'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right"></'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right"></'.($this->getFieldValue($obj, 'id_supplier') == 76 ? 'th' : 'td').'>
							
							</tr>
							
							<tr>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Ingram').'</'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'ingram_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('In arrivo Ingram').'</'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'arrivo_ingram_quantity') : 0).'</'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.$this->l('Data arr. Ingram').':</'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').'>
							<'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').' style="border-right: 1px solid #bbb; text-align:right">'.(($from = $this->getFieldValue($obj, 'data_arrivo_ingram')) ? ($from == '0000-00-00' || $from == '1970-01-01' || $from == '1946-01-01' ? 'ND' : date("d-m-y",strtotime($from))) : date('d-m-y')).'</'.($this->getFieldValue($obj, 'id_supplier') == 1461 ? 'th' : 'td').'>
							
							</tr>
							
							
							
							<tr>
							
							<td style="background: #ebebeb;border-right: 1px solid #000; border-top: 1px solid #000; text-align:right"><strong>Totale tutti i magazzini (escl. Amazon)</strong></td>
							<td style="background: #ebebeb;border-right: 1px solid #000; border-top: 1px solid #000; text-align:right">'.($obj->id ? $this->getFieldValue($obj, 'quantity') : 0).'</td>
							<td style="background: #ebebeb;border-right: 1px solid #000; border-top: 1px solid #000; text-align:right; width:170px"><strong>Tot. magazzini + Ordinato a fornitore - Impegnato sito</strong></td>
							<td style="background: #ebebeb;text-align:right; border-right: 1px solid #000; border-top: 1px solid #000; ">'.($obj->id ? ($this->getFieldValue($obj, 'quantity'))+$this->getFieldValue($obj, 'ordinato_quantity')-$this->getFieldValue($obj, 'impegnato_quantity') : 0).'</td>
							';
							
							echo '
							<td style="background: #ebebeb;border-right: 1px solid #000; border-top: 1px solid #000; text-align:right; "><strong>'.$img_scorta.' Scorta minima</strong></td>
							<td style="background: #ebebeb;text-align:right; border-top: 1px solid #000; ">
							
							'.$scorta_minima.'</td>
							';
							
							
							$arrivo_ordinato = Db::getInstance()->getValue('SELECT data_arrivo_ordinato FROM product WHERE id_product = '.$obj->id);
							
							if($arrivo_ordinato == '')
								$date_arrivo_ordinato_riga = '<tr><td colspan="3">Nessun ordine in arrivo</td></tr>';
							else
							{
								$arrivo_ordinato = unserialize($arrivo_ordinato);
								
								foreach($arrivo_ordinato as $ao)
									$date_arrivo_ordinato_riga .= '<tr><td style="text-align:right;">'.$ao['ordine'].'</td><td style="text-align:right;">'.round($ao['quantita'],0).'</td><td>'.Db::getInstance()->getValue('SELECT company FROM customer WHERE codice_esolver = "'.$ao['fornitore'].'"').'</td><td>'.date('d/m/Y',strtotime($ao['data'])).'</td></tr>';
							}
							
							echo '
							</tr>
							
							</table> 
							
							<table class="table"  cellpadding="0" cellspacing="0" style="float:left; border:1px solid #000; float:left; width:24%; margin-left:2%"><tr><th style="background: #ebebeb; text-align:center; border-bottom:1px solid #000" colspan="8">Date qta in arrivo</th></tr>
							<tr>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right;">ID Ord.</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right;">Qt.</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000; text-align:right; width:150px">Fornitore</th>
							<th style="border-right: 1px solid #bbb; border-bottom:1px solid #000;text-align:right">Data</th>
							</tr>
							
							'.$date_arrivo_ordinato_riga.'
							
							
							</table> <div style="clear:both"></div>';
							
							/*
							
							<strong>Quantit&agrave;</strong>
							<table>
							<tr><td>'.$this->l('Magazzino').'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><b>'.($obj->id ? $qty : 0).'</b></td></tr>
							<tr><td>'.$this->l('Allnet').'</td><td><b>'.($obj->id ? $this->getFieldValue($obj, 'supplier_quantity') : 0).'</b></td></tr>
							<tr><td>'.$this->l('Totale').'</td><td><b>'.($obj->id ? $this->getFieldValue($obj, 'quantity') : 0).'</b></td></tr>
							<tr><td>'.$this->l('Ordinato').'</td><td><b>'.($obj->id ? $this->getFieldValue($obj, 'ordinato_quantity') : 0).'</b></td></tr>
							<tr><td>'.$this->l('In arrivo da Allnet').'</td><td><b>'.($obj->id ? $this->getFieldValue($obj, 'arrivo_quantity') : 0).'</b></td></tr>
							<tr><td>'.$this->l('Impegnato').'</td><td><b>'.($obj->id ? $this->getFieldValue($obj, 'impegnato_quantity') : 0).'</b></td></tr>
							</table>
							</td><td>
							&nbsp;&nbsp;&nbsp;</tr></table></td>*/
							
							echo '
								<td style="padding-bottom:5px;">';
								/*	<select id="id_mvt_reason" name="id_mvt_reason">
										<option value="-1">--</option>';
							$reasons = StockMvtReason::getStockMvtReasons((int)$cookie->id_lang);

							foreach ($reasons AS $reason)
								echo '<option rel="'.$reason['sign'].'" value="'.$reason['id_stock_mvt_reason'].'" '.(Configuration::get('PS_STOCK_MVT_REASON_DEFAULT') == $reason['id_stock_mvt_reason'] ? 'selected="selected"' : '').'>'.$reason['name'].'</option>';
							echo '</select>*/
									
							echo	'
							
							</td>
							</tr>
							';
						/*}
						else {
							echo '<tr><td class="col-left">'.$this->l('Initial stock:').'</td>
									<td style="padding-bottom:5px;">
										<input size="3" maxlength="10" name="stock_quantity" type="text" value="100" />
									</td>';
						}*/
					}

					if ($obj->id)
						echo '
							<tr>
							</tr>
						';
					if ($has_attribute)
						echo '<tr>
								<td class="col-left">&nbsp;</td>
								<td>
									<div class="hint clear" style="display: block;width: 70%;">'.$this->l('You used combinations, for this reason you cannot edit your stock quantity here, but in the Combinations tab').'</div>
								</td>
							</tr>';
				}
				else
				{
					echo '<tr>
							<td colspan="2">'.$this->l('The stock management is disabled').'</td>
						</tr>';

					echo  '
						<tr>
							<td class="col-left">'.$this->l('Minimum quantity:').'</td>
							<td style="padding-bottom:5px;">
								<input size="3" maxlength="10" name="minimal_quantity" id="minimal_quantity" type="text" value="'.($this->getFieldValue($obj, 'minimal_quantity') ? $this->getFieldValue($obj, 'minimal_quantity') : 1).'" />
								<p>'.$this->l('The minimum quantity to buy this product (set to 1 to disable this feature)').'</p>
							</td>
						</tr>';
					
				}
				echo '	';
	?>
	
<div id="prezzi_tabelle" />
<style type="text/css">
#prezzi_tabelle input {
	text-align:right;
}
</style>
<?php echo '<br />
<table class="table"  cellpadding="0" cellspacing="0" style="border:1px solid #000; float:left; width:100%"><tr>
								<th style="border-right: 1px solid #bbb; text-align:right">Venduto 30 gg</th><td style="width:30px; border-right: 1px solid #bbb; text-align:right">'.($venduto30 > 0 ? $venduto30 : 0).'</td>
								<th style="border-right: 1px solid #bbb; text-align:right">Venduto 60 gg</th><td style="width:30px; border-right: 1px solid #bbb; text-align:right">'.($venduto60 > 0 ? $venduto60 : 0).'</td>
								<th style="border-right: 1px solid #bbb; text-align:right">Venduto 90 gg</th><td style="width:30px; border-right: 1px solid #bbb; text-align:right">'.($venduto90 > 0 ? $venduto90 : 0).'</td>
								<th style="border-right: 1px solid #bbb; text-align:right">Venduto 120 gg</th><td style="width:30px; border-right: 1px solid #bbb; text-align:right">'.($venduto120 > 0 ? $venduto120 : 0).'</td>
								<th style="border-right: 1px solid #bbb; text-align:right">Venduto 150 gg</th><td style="width:30px; border-right: 1px solid #bbb; text-align:right">'.($venduto150 > 0 ? $venduto150 : 0).'</td>
								<th style="border-right: 1px solid #bbb; text-align:right">Venduto 180 gg</th><td style="width:30px; border-right: 1px solid #bbb; text-align:right">'.($venduto180 > 0 ? $venduto180 : 0).'</td>
								</tr>
								</table><br /><br />'; ?>
<?php

//$rann = mysql_query("SELECT * FROM specific_price WHERE id_product = ".$_GET['id_product']." AND specific_price.to != '0000-00-00 00:00:00' AND specific_price.to > '".date("Y-m-d H:i:s")."' ");

$rann = ("SELECT * FROM specific_price WHERE id_product = ".$_GET['id_product']." AND specific_price.to != '0000-00-00 00:00:00' ");
	
	//REGOLA PREZZO SPECIALE E SCONTIQTA
	
		$rowann = Db::getInstance()->getRow($rann);
		if($rowann['id_specific_price'] > 0)
		{
		$rowannpieces = Db::getInstance()->getValue('SELECT pieces FROM specific_price_pieces WHERE id_specific_price = '.$rowann['id_specific_price']);
		
		echo '<br /><table class="table"  cellpadding="0" cellspacing="0" style="border:1px solid #000; color:red; float:left; width:100%"><tr>
										<th style="width:220px; border-right: 1px solid #bbb; text-align:right">Prezzo speciale vendita</th><td style="width:70px; border-right: 1px solid #bbb; text-align:right">'.(($rowann['reduction_type'] == 'amount' ? str_replace(".",",",($rowann['price'] - $rowann['reduction'])) : str_replace(".",",",($rowann['price'] - ($rowann['price']*$rowann['reduction']))))).' &euro;</td>
								<th style="width:115px; border-right: 1px solid #bbb; text-align:right">Margine <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Sul miglior prezzo acquisto disponibile" title="Sul miglior prezzo acquisto disponibile" /></th><td style="width:70px; border-right: 1px solid #bbb; text-align:right">'.number_format(((($rowann['price'] - Product::trovaMigliorPrezzoAcquisto($obj->id, 1,99999))*100)/$rowann['price']),2,",","").'%</td>
								<th style="width:120px; border-right: 1px solid #bbb; text-align:right">Qta. in promo</th><td style="width:70px; border-right: 1px solid #bbb; text-align:right">'.($rowannpieces != '' ? $rowannpieces : 'illimitati').'</td>
								<th style="border-right: 1px solid #bbb; text-align:right">Scad. data</th><td style="width:80px; border-right: 1px solid #bbb; text-align:right">'.date("d/m/Y", strtotime($rowann['to'])).'</td>
								</tr>
								</table><div style="clear:both"></div>';
								
		
		if($rowannpieces != '' && $rowannpieces <= 0)
			$occhio_vendita = 1;
		else
			$occhio_vendita = 0;

	}
	else { }
	
	
//$rannw = mysql_query("SELECT * FROM specific_price_wholesale spw WHERE id_product = ".$_GET['id_product']." AND spw.to != '0000-00-00 00:00:00' AND spw.to > '".date("Y-m-d H:i:s")."' ");

$rannw = ("SELECT * FROM specific_price_wholesale spw WHERE id_product = ".$_GET['id_product']." AND spw.to != '0000-00-00 00:00:00' ");
	
	//REGOLA PREZZO SPECIALE E SCONTIQTA
	$rowannw = Db::getInstance()->getRow($rannw);
		if($rowannw['id_specific_price'] > 0)
		{
		
		echo '<br /><table class="table"  cellpadding="0" cellspacing="0" style="border:1px solid #000; color:blue; float:left; width:100%"><tr>
										<th style="width:220px; border-right: 1px solid #bbb; text-align:right">Prezzo speciale acquisto</th><td style="width:70px; border-right: 1px solid #bbb; text-align:right">'.str_replace(".",",",($rowannw['wholesale_price'])).' &euro;</td>
								<th style="width:115px; border-right: 1px solid #bbb; text-align:right">Margine <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Rispetto al miglior prezzo di vendita disponibile" title="Rispetto al miglior prezzo vendita disponibile" /></th><td style="width:70px; border-right: 1px solid #bbb; text-align:right">'.number_format((((Product::trovaMigliorPrezzo($obj->id, 1,1) - $rowannw['wholesale_price'])*100)/Product::trovaMigliorPrezzo($obj->id, 1,1)),2,",","").'%</td>
								<th style="width:120px; border-right: 1px solid #bbb; text-align:right">Qta. in promo</th><td style="width:70px; border-right: 1px solid #bbb; text-align:right">'.($rowannw['pieces'] != '' ? $rowannw['pieces'] : 'illimitati').'</td>
								<th style="border-right: 1px solid #bbb; text-align:right">Scad. data</th><td style="width:80px; border-right: 1px solid #bbb; text-align:right">'.date("d/m/Y", strtotime($rowannw['to'])).'</td>
								</tr>
								</table><div style="clear:both"></div>';
								
		
		if($rowannw['pieces'] != '' && $rowannw['pieces'] <= 0)
			$occhio_acquisto = 1;
		else
			$occhio_acquisto = 0;
	}
	
	if($rowann['price'] > $obj->price)
	{
		echo '<script type="text/javascript">alert("ATTENZIONE! Prezzo web MINORE del prezzo speciale")</script><span style="text-align:center; display:block; width:800px; background-color:#ffcccc"><strong style="font-size:16px">!!! ATTENZIONE !!! Prezzo web MINORE del prezzo speciale.</strong></span><br /><br />';
	
	}
	
	if($obj->id > 0)
	{	
		if((((Product::getPriceStatic($obj->id, false, NULL, 2) - Product::getCurrentWholesalePrice($obj->id))*100)/Product::getPriceStatic($obj->id, false, NULL, 2)) < 10 && Product::getPriceStatic($obj->id, false, NULL, 2) > 0)
		{
			echo '<span style="text-align:center; display:block; width:800px; background-color:#ffcccc"><strong style="font-size:16px">!!! ATTENZIONE !!! Prodotto in vendita con margine sotto il 10%</strong></span><br />';
			
			
		}	
	}
	
					echo '
					<div style="border:1px solid black; background-color:#ffffff; margin-top:10px; padding-left:5px; padding-top:10px; padding-bottom:10px">
					<div style="float:left; margin-top:10px"><h2>Sales</h2></div><div style="float:left; margin-left:20px">
						<table cellspacing="0" cellpadding="0"><tr style="background-color:#ebebeb">
							<td class="col-left" style="width:200px; text-align:right">'.$this->l('Listino').'
								'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="14" readonly="readonly" id="listino" name="listino" type="text" value="'.number_format(htmlentities($this->getFieldValue($obj, 'listino'), ENT_COMPAT, 'UTF-8'),2,'.','').'" onchange="this.value = this.value.replace(/,/g, \'.\'); document.getElementById(\'listino_esolver\').value = this.value.replace(/,/g, \'.\');" onkeyup="
								
								document.getElementById(\'listino_esolver\').value = this.value.replace(/,/g, \'.\');
								calcPrezzoSconto(\'scontolistinovendita\', \'priceTE\', \'listino\');
								calcScontoAcquisto1(); calcScontoAcquisto2(); calcScontoAcquisto3(); calcRebate();
								calcMarginalita(); calcSconto(\'listino\', \'priceTE\', \'scontolistinovendita\');
								calcMarginalitaSconto(\'sconto_rivenditore_1\', \'margin_riv_1\', \'listino\'); calcPrezzoSconto(\'sconto_rivenditore_1\', \'prezzo_riv_1\', \'listino\');
								calcMarginalitaSconto(\'sconto_rivenditore_2\', \'margin_riv_2\', \'listino\'); calcPrezzoSconto(\'sconto_rivenditore_2\', \'prezzo_riv_2\', \'listino\');
								calcMarginalitaSconto(\'sconto_rivenditore_3\', \'margin_riv_3\', \'listino\'); calcPrezzoSconto(\'sconto_rivenditore_3\', \'prezzo_riv_3\', \'listino\');
								calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_1\', \'prezzo_qta_1\', \'priceTE\');
								calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_2\', \'prezzo_qta_2\', \'priceTE\');
								calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_3\', \'prezzo_qta_3\', \'priceTE\');
								calcPrezzoFromMargine(\'wholesale_price\', \'margin_login_for_offer\', \'prezzo_login_for_offer\');
								
								" />'.($currency->format % 2 == 0 ? ' '.$currency->sign : '').'
								
							</td>
							<td class="col-left" style="width:200px; text-align:right">'.$this->l('Sconto clienti:').'
								'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="14" id="scontolistinovendita" name="scontolistinovendita" type="text" value="'.number_format(htmlentities($this->getFieldValue($obj, 'scontolistinovendita'), ENT_COMPAT, 'UTF-8'),2,'.','').'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return;  calcPrezzoSconto(\'scontolistinovendita\', \'priceTE\', \'listino\');
								calcMarginalita(); 
										calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_1\', \'prezzo_qta_1\', \'priceTE\');
								calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_2\', \'prezzo_qta_2\', \'priceTE\');
								calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_3\', \'prezzo_qta_3\', \'priceTE\');
								aggiornaScontisticaAmazon();
								calcPrezzoFromMargine(\'wholesale_price\', \'margin_login_for_offer\', \'prezzo_login_for_offer\');
								
								" />%
							
							</td>
							
							
							
							
							<td class="col-left" style="width:200px; text-align:right">'.$this->l('Prezzo di vendita:').'
								'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="14" id="priceTE" name="price" type="text" value="'.number_format(htmlentities($this->getFieldValue($obj, 'price'), ENT_COMPAT, 'UTF-8'),2,'.','').'" onchange="this.value = this.value.replace(/,/g, \'.\');" required onkeyup="calcMarginalita(); calcSconto(\'listino\', \'priceTE\', \'scontolistinovendita\'); calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_1\', \'prezzo_qta_1\', \'priceTE\');
								calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_2\', \'prezzo_qta_2\', \'priceTE\');
								calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_3\', \'prezzo_qta_3\', \'priceTE\');
								aggiornaScontisticaAmazon();
								calcPrezzoFromMargine(\'wholesale_price\', \'margin_login_for_offer\', \'prezzo_login_for_offer\');
								
								" />'.($currency->format % 2 == 0 ? ' '.$currency->sign : '').'
								
							</td>
								
								<td class="col-left" style="width:200px; text-align:right">'.$this->l('Marginalità:').'
								'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="14" id="marginalita" name="marginalita" type="text" onchange="this.value = this.value.replace(/,/g, \'.\');" value="'. number_format((($this->getFieldValue($obj, 'price')-$this->getFieldValue($obj, 'wholesale_price'))*100)/$this->getFieldValue($obj, 'price'),2,'.','') .'" onkeyup="if (isArrowKey(event)) return; calcPriceTI();" />%
								
							</td>
							
								
								
							</td>
							
						</tr></table><br />';

	

						echo '
						<table  cellspacing="0" cellpadding="0"><tr style="background-color:yellow">
						<td class="col-left" style="width:200px; text-align:right">'.$this->l('Rebate 1:').'
								'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="14" id="rebate_1" name="rebate_1" type="text" value="'.(Tools::getIsset('rebate_1') ? Tools::getValue('rebate_1') : number_format($prezzi_esolver['rebate_1'],2,'.','')).'" onchange="this.value = this.value.replace(/,/g, \'.\');" required onkeyup="if (isArrowKey(event)) return; calcRebate(); calcMarginalita(); 
							
								calcMarginalitaSconto(\'sconto_rivenditore_1\', \'margin_riv_1\', \'listino\'); 
								calcMarginalitaSconto(\'sconto_rivenditore_2\', \'margin_riv_2\', \'listino\');
								calcMarginalitaSconto(\'sconto_rivenditore_3\', \'margin_riv_3\', \'listino\');
								calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); 
								calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); 
								calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\');
								aggiornaScontisticaAmazon();	
calcPrezzoFromMargine(\'wholesale_price\', \'margin_login_for_offer\', \'prezzo_login_for_offer\');
								
								" />%
							
							</td>
							
									
						
								<td class="col-left" style="width:200px; text-align:right">'.$this->l('Rebate 2:').'
								'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="14" id="rebate_2" name="rebate_2" type="text" value="'.(Tools::getIsset('rebate_2') ? Tools::getValue('rebate_2') : number_format($prezzi_esolver['rebate_2'],2,'.','')).'" onchange="this.value = this.value.replace(/,/g, \'.\');" required onkeyup="if (isArrowKey(event)) return; calcRebate(); calcMarginalita();
								calcMarginalitaSconto(\'sconto_rivenditore_1\', \'margin_riv_1\', \'listino\'); calcPrezzoSconto(\'sconto_rivenditore_1\', \'prezzo_riv_1\', \'listino\');
								calcMarginalitaSconto(\'sconto_rivenditore_2\', \'margin_riv_2\', \'listino\'); 
								calcMarginalitaSconto(\'sconto_rivenditore_3\', \'margin_riv_3\', \'listino\'); 
								calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); 
								calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); 
								calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\');
								aggiornaScontisticaAmazon();
								calcPrezzoFromMargine(\'wholesale_price\', \'margin_login_for_offer\', \'prezzo_login_for_offer\');
								
								" />%
							
							</td>
					
								<td class="col-left" style="width:200px; text-align:right">'.$this->l('Rebate 3:').'
								'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="14" id="rebate_3" name="rebate_3" type="text" value="'.(Tools::getIsset('rebate_3') ? Tools::getValue('rebate_3') : number_format($prezzi_esolver['rebate_3'],2,'.','')).'" onchange="this.value = this.value.replace(/,/g, \'.\');" required onkeyup="if (isArrowKey(event)) return; calcRebate(); calcMarginalita();	calcMarginalitaSconto(\'sconto_rivenditore_1\', \'margin_riv_1\', \'listino\');
								calcMarginalitaSconto(\'sconto_rivenditore_2\', \'margin_riv_2\', \'listino\'); 
								calcMarginalitaSconto(\'sconto_rivenditore_3\', \'margin_riv_3\', \'listino\'); 
								calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); 
								calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); 
								calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\');
								calcPrezzoFromMargine(\'wholesale_price\', \'margin_login_for_offer\', \'prezzo_login_for_offer\');
								aggiornaScontisticaAmazon();

								"  />%
							
							</td>
						
							<td class="col-left" style="width:200px; text-align:right">'.$this->l('Acquisto netto:').'
								'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="14" id="wholesale_price" name="wholesale_price" type="text" value="'.number_format(htmlentities($this->getFieldValue($obj, 'wholesale_price'), ENT_COMPAT, 'UTF-8'),2,'.','').'" readonly="readonly" onclick="alert(\'Per cambiare il prezzo di acquisto, usare la casella nella sezione &quot;eSolver&quot;\')" required onchange="this.value = this.value.replace(/,/g, \'.\');
								calcMarginalita();
								calcSconto(\'wholesale_price_esolver\', \'wholesale_price\', \'rebate_1\');
								document.getElementById(\'rebate_2\').value = 0;
								document.getElementById(\'rebate_3\').value = 0;
								calcMarginalitaSconto(\'sconto_rivenditore_1\', \'margin_riv_1\', \'listino\');
								calcMarginalitaSconto(\'sconto_rivenditore_2\', \'margin_riv_2\', \'listino\'); 
								calcMarginalitaSconto(\'sconto_rivenditore_3\', \'margin_riv_3\', \'listino\'); 
								calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); 
								calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); 
								calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\');
								aggiornaScontisticaAmazon();
								calcPrezzoFromMargine(\'wholesale_price\', \'margin_login_for_offer\', \'prezzo_login_for_offer\');
								
								" />'.($currency->format % 2 == 0 ? ' '.$currency->sign : '').'
								
							</td>';
						
						/*
						<td style="width:130px"  style="padding-bottom:0px;">
								'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="8" maxlength="14" id="wholesale_price" name="wholesale_price" type="text" value="'.number_format(($this->getFieldValue($obj, 'listino')*((100-$this->getFieldValue($obj, 'sconto_acquisto_1'))/100)*((100-$this->getFieldValue($obj, 'sconto_acquisto_2'))/100)*((100-$this->getFieldValue($obj, 'sconto_acquisto_3'))/100)),2,'.','').'" onkeyup="if (isArrowKey(event)) return; calcMarginalita(); " onchange="this.value = this.value.replace(/,/g, \'.\');" />'.($currency->format % 2 == 0 ? ' '.$currency->sign : '').'*/
								
								
								
								echo '
							</td>
						</tr></table><br />';
						
						$miglior_prezzo = Product::trovaMigliorPrezzo($obj->id,1,1);
						$miglior_prezzo_acquisto = Product::trovaMigliorPrezzoAcquisto($obj->id,1,1);
						
						$prezzo_login_for_offer = (($this->getFieldValue($obj, 'wholesale_price') * 100) / (100 - $prezzi_esolver['margin_login_for_offer']));
						
						if($prezzi_esolver['margin_login_for_offer'] == 0)
							$prezzo_login_for_offer = 'ND';
						
						
						echo '
						<table  cellspacing="0" cellpadding="0"><tr style="background-color:#99a9e8">
						<td class="col-left" style="width:400px; text-align:right">'.$this->l('Margine login for offer:').'
								'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="14" id="margin_login_for_offer" name="margin_login_for_offer" type="text" value="'.(Tools::getIsset('margin_login_for_offer') ? Tools::getValue('margin_login_for_offer') : number_format($prezzi_esolver['margin_login_for_offer'],2,'.','')).'" onchange="this.value = this.value.replace(/,/g, \'.\');" required onkeyup="if (isArrowKey(event)) return; 
								calcPrezzoFromMargine(\'wholesale_price\', \'margin_login_for_offer\', \'prezzo_login_for_offer\'); 
								" />%
							
							</td>
							
									
						
								<td class="col-left" style="width:400px; text-align:right">'.$this->l('Prezzo login for offer:').'
								'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="14" id="prezzo_login_for_offer" name="prezzo_login_for_offer" type="text" value="'.Tools::displayPrice($prezzo_login_for_offer,1).'" readonly="readonly" onchange="this.value = this.value.replace(/,/g, \'.\');" required
								" />
							
							</td>
						</tr></table>';
						
					echo '</div>
						<div style="clear:both"></div>
					</div>';
					
						
					echo '<br />
					<strong>Sconti quantità</strong> <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="I margini sono calcolati sui prezzi di listino" title="I margini sono calcolati sui prezzi di listino" /><table cellspacing="0" cellpadding="0" style="width:100%; background-color:#99cc99">
					<tr >
					
							<td class="col-left" style="width:200px">'.$this->l('Quantita:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2"  value="'.(Tools::getIsset('quantita_1') ? Tools::getValue('quantita_1') : htmlentities($this->getQtSconti('sc_qta_1'))).'" maxlength="14" id="quantita_1" name="quantita_1" type="text" onchange="this.value = this.value.replace(/,/g, \'.\');" />
						
						</td>	

					<td class="col-left" style="width:200px">'.$this->l('Sc. qta 1:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="14" id="sconto_quantita_1" name="sconto_quantita_1" type="text" value="'.(Tools::getIsset('sconto_quantita_1') ? Tools::getValue('sconto_quantita_1') : htmlentities($this->getSconti('sc_qta_1')*100)).'" required onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_1\', \'prezzo_qta_1\', \'priceTE\');" />%
						
						</td>
						
							<td class="col-left" style="width:200px">'.$this->l('Prezzo:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="14" id="prezzo_qta_1" name="prezzo_qta_1"
value="'.(Tools::getIsset('prezzo_qta_1') ? Tools::getValue('prezzo_qta_1') : round(($this->getFieldValue($obj, 'price')-($this->getFieldValue($obj, 'price')*$this->getSconti('sc_qta_1'))),2)).'" 								type="text" required onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'priceTE\', \'prezzo_qta_1\', \'sconto_quantita_1\'); calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\');" />€
						
						</td>	

						
					<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('Marginalità:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="14" id="margin_qta_1" 
							
								value="'.(Tools::getIsset('margin_qta_1') ? Tools::getValue('margin_qta_1') : number_format(((($this->getFieldValue($obj, 'price')-($this->getFieldValue($obj, 'price')*$this->getSconti('sc_qta_1')))-$this->getFieldValue($obj, 'wholesale_price'))*100)/($this->getFieldValue($obj, 'price')-($this->getFieldValue($obj, 'price')*$this->getSconti('sc_qta_1'))),2,'.','')).'" 
							
							name="margin_qta_1" type="text" onchange="this.value = this.value.replace(/,/g, \'.\');"  />%
						
						</td>	
						
						
					</tr>	
					<tr>	
							<td class="col-left" style="width:200px">'.$this->l('Quantita:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="14"  value="'.(Tools::getIsset('quantita_2') ? Tools::getValue('quantita_2') : htmlentities($this->getQtSconti('sc_qta_2'))).'" id="quantita_2" name="quantita_2" type="text" onchange="this.value = this.value.replace(/,/g, \'.\');" />
						
						</td>	
					
				
							<td class="col-left" style="width:200px">'.$this->l('Sc. qta 2:').'
								'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="14" id="sconto_quantita_2" name="sconto_quantita_2" type="text" value="'.(Tools::getIsset('sconto_quantita_2') ? Tools::getValue('sconto_quantita_2') : htmlentities($this->getSconti('sc_qta_2')*100)).'" required onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_2\', \'prezzo_qta_2\', \'priceTE\');" />%
						
						</td>
						
						
					
							<td class="col-left" style="width:200px">'.$this->l('Prezzo:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="14" id="prezzo_qta_2" name="prezzo_qta_2" 
							value="'.(Tools::getIsset('prezzo_qta_2') ? Tools::getValue('prezzo_qta_2') : round(($this->getFieldValue($obj, 'price')-($this->getFieldValue($obj, 'price')*$this->getSconti('sc_qta_2'))),2)).'" 	type="text" required onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'priceTE\', \'prezzo_qta_2\', \'sconto_quantita_2\'); calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); " />€
						
						</td>	
							
					
					<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('Marginalità:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="14" id="margin_qta_2"
	value="'.(Tools::getIsset('margin_qta_2') ? Tools::getValue('margin_qta_2') : number_format(((($this->getFieldValue($obj, 'price')-($this->getFieldValue($obj, 'price')*$this->getSconti('sc_qta_2')))-$this->getFieldValue($obj, 'wholesale_price'))*100)/($this->getFieldValue($obj, 'price')-($this->getFieldValue($obj, 'price')*$this->getSconti('sc_qta_2'))),2,'.','')).'" 

							name="margin_qta_2" type="text" onchange="this.value = this.value.replace(/,/g, \'.\');" />%
						
						</td>	
							</tr>	
					<tr>		<td class="col-left" style="width:200px">'.$this->l('Quantita:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="14" id="quantita_3" name="quantita_3" type="text" 
							 value="'.(Tools::getIsset('quantita_3') ? Tools::getValue('quantita_3') : htmlentities($this->getQtSconti('sc_qta_3'))).'"
							
							
							  onchange="this.value = this.value.replace(/,/g, \'.\');" />
						
						</td>	

					
							<td class="col-left" style="width:200px">'.$this->l('Sc. qta 3:').'
								'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="14" id="sconto_quantita_3"  name="sconto_quantita_3" type="text" value="'.(Tools::getIsset('sconto_quantita_3') ? Tools::getValue('sconto_quantita_3') : htmlentities($this->getSconti('sc_qta_3')*100)).'"  required onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_3\', \'prezzo_qta_3\', \'priceTE\');" />%
						
						</td>
					
							<td class="col-left" style="width:200px">'.$this->l('Prezzo:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="14" id="prezzo_qta_3" name="prezzo_qta_3" 
							value="'.(Tools::getIsset('prezzo_qta_3') ? Tools::getValue('prezzo_qta_3') : round(($this->getFieldValue($obj, 'price')-($this->getFieldValue($obj, 'price')*$this->getSconti('sc_qta_3'))),2)).'" 	
							type="text" required onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'priceTE\', \'prezzo_qta_3\', \'sconto_quantita_3\'); calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\'); " />€
						
						</td>	
					
						
					<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('Marginalità:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="14" id="margin_qta_3" 
									value="'.(Tools::getIsset('margin_qta_3') ? Tools::getValue('margin_qta_3') : number_format(((($this->getFieldValue($obj, 'price')-($this->getFieldValue($obj, 'price')*$this->getSconti('sc_qta_3')))-$this->getFieldValue($obj, 'wholesale_price'))*100)/($this->getFieldValue($obj, 'price')-($this->getFieldValue($obj, 'price')*$this->getSconti('sc_qta_3'))),2,'.','')).'" 
							
							name="margin_qta_3" type="text" onchange="this.value = this.value.replace(/,/g, \'.\');" />%
						
						</td>	
					</tr></table>';
					
					
					echo '<br />';
					echo '
					<div style="float:left">
					<strong>Sconti rivenditori</strong> <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="I margini sono calcolati sui prezzi di listino. Lo sconto rivenditore 2 è calcolato elevando del 3% il prezzo finale del rivenditore 1" title="I margini sono calcolati sui prezzi di listino. Lo sconto rivenditore 2 è calcolato elevando del 3% il prezzo finale del rivenditore 1" /><table cellspacing="0" cellpadding="0" style="background-color:#ff99ff">
					<tr >
					<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('Sc. rivend. 1:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="14" id="sconto_rivenditore_1" name="sconto_rivenditore_1" type="text" value="'.(Tools::getIsset('sconto_rivenditore_1') ? Tools::getValue('sconto_rivenditore_1') : htmlentities($this->getSconti('sc_riv_1')*100)).'" required onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto(\'sconto_rivenditore_1\', \'margin_riv_1\', \'listino\'); 
							calcPrezzoSconto(\'sconto_rivenditore_1\', \'prezzo_riv_1\', \'listino\'); calcolaScontoRivenditore2(); " />%
						
						</td>
						
							<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('Prezzo:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="11" id="prezzo_riv_1" name="prezzo_riv_1" 
			value="'.(Tools::getIsset('prezzo_riv_1') ? Tools::getValue('prezzo_riv_1') : number_format($this->getFieldValue($obj, 'listino')-($this->getFieldValue($obj, 'listino')*($this->getSconti('sc_riv_1'))),2,",","")).'" 				
							type="text" required onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'listino\', \'prezzo_riv_1\', \'sconto_rivenditore_1\'); calcolaScontoRivenditore2(); calcMarginalitaSconto(\'sconto_rivenditore_1\', \'margin_riv_1\', \'listino\'); " />€
						
						</td>	
							
					
					<td class="col-left" style="width:160px; padding-bottom:5px;">'.$this->l('Marginalità:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="11" id="margin_riv_1" name="margin_riv_1" type="text"
							value="'.(Tools::getIsset('margin_riv_1') ? Tools::getValue('margin_riv_1') : number_format(((($this->getFieldValue($obj, 'listino')-($this->getFieldValue($obj, 'listino')*($this->getSconti('sc_riv_1'))))-$this->getFieldValue($obj, 'wholesale_price'))*100)/($this->getFieldValue($obj, 'listino')-($this->getFieldValue($obj, 'listino')*$this->getSconti('sc_riv_1'))),2,'.','')).'" 
							onchange="this.value = this.value.replace(/,/g, \'.\');" />%
						 	
						
						
						
						</td>	
						
						
					</tr>

					<tr>	
				
							<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('Sc. rivend. 2:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="11" readonly="readonly" id="sconto_rivenditore_2" name="sconto_rivenditore_2" type="text" value="'.(Tools::getIsset('sconto_rivenditore_2') ? Tools::getValue('sconto_rivenditore_2') : htmlentities($this->getSconti('sc_riv_2')*100)).'" required onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto(\'sconto_rivenditore_2\', \'margin_riv_2\', \'listino\'); calcPrezzoSconto(\'sconto_rivenditore_2\', \'prezzo_riv_2\', \'listino\');" />%
						
						</td>
					
							<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('Prezzo:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="11" readonly="readonly" id="prezzo_riv_2" 
							value="'.(Tools::getIsset('prezzo_riv_2') ? Tools::getValue('prezzo_riv_2') : number_format($this->getFieldValue($obj, 'listino')-($this->getFieldValue($obj, 'listino')*($this->getSconti('sc_riv_2'))),2,",","")).'" 	
							name="prezzo_riv_2" type="text" required onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'listino\', \'prezzo_riv_2\', \'sconto_rivenditore_2\'); calcMarginalitaSconto(\'sconto_rivenditore_2\', \'margin_riv_2\', \'listino\'); " />€
						
						</td>	
					
						
					<td class="col-left" style="width:160px; padding-bottom:5px;">'.$this->l('Marginalità:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="11" id="margin_riv_2" 
								value="'.(Tools::getIsset('margin_riv_2') ? Tools::getValue('margin_riv_2') : number_format(((($this->getFieldValue($obj, 'listino')-($this->getFieldValue($obj, 'listino')*$this->getSconti('sc_riv_2')))-$this->getFieldValue($obj, 'wholesale_price'))*100)/($this->getFieldValue($obj, 'listino')-($this->getFieldValue($obj, 'listino')*$this->getSconti('sc_riv_2'))),2,'.','')).'" 
							
							name="margin_riv_2" type="text" onchange="this.value = this.value.replace(/,/g, \'.\');" />%
						
						</td>	
							</tr>	
					<tr>	
					
							<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('Sc. distrib.:').'&nbsp;&nbsp;&nbsp;
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="11" id="sconto_rivenditore_3" name="sconto_rivenditore_3" type="text" value="'.(Tools::getIsset('sconto_rivenditore_3') ? Tools::getValue('sconto_rivenditore_3') : htmlentities($this->getSconti('sc_riv_3')*100)).'"  required onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto(\'sconto_rivenditore_3\', \'margin_riv_3\', \'listino\'); calcPrezzoSconto(\'sconto_rivenditore_3\', \'prezzo_riv_3\', \'listino\');"  />%
						
						</td>
			
							<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('Prezzo:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="11" id="prezzo_riv_3" name="prezzo_riv_3"
							value="'.(Tools::getIsset('prezzo_riv_3') ? Tools::getValue('prezzo_riv_3') : number_format($this->getFieldValue($obj, 'listino')-($this->getFieldValue($obj, 'listino')*($this->getSconti('sc_riv_3'))),2,",","")).'" 	
							type="text"  required onchange="this.value = this.value.replace(/,/g, \'.\');"  onkeyup="calcSconto(\'listino\', \'prezzo_riv_3\', \'sconto_rivenditore_3\'); calcMarginalitaSconto(\'sconto_rivenditore_3\', \'margin_riv_3\', \'listino\'); "/>€
						
						</td>	
							
					
					<td class="col-left" style="width:160px; padding-bottom:5px;">'.$this->l('Marginalità:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="11" id="margin_riv_3" name="margin_riv_3" 
								value="'.(Tools::getIsset('margin_riv_3') ? Tools::getValue('margin_riv_3') : number_format(((($this->getFieldValue($obj, 'listino')-($this->getFieldValue($obj, 'listino')*$this->getSconti('sc_riv_3')))-$this->getFieldValue($obj, 'wholesale_price'))*100)/($this->getFieldValue($obj, 'listino')-($this->getFieldValue($obj, 'listino')*$this->getSconti('sc_riv_3'))),2,'.','')).'" 
							
							type="text" onchange="this.value = this.value.replace(/,/g, \'.\');" />%
						
						</td>	
					</tr></table>';
					
					
					
					$map_price = Db::getInstance()->getValue('SELECT map FROM product WHERE id_product = '.$obj->id);
					
					$miglior_margine = $miglior_prezzo - $miglior_prezzo_acquisto;
					
					
					$marginalita_migliore_percentuale = ((($miglior_prezzo-$miglior_prezzo_acquisto)*100)/$miglior_prezzo);
					
					if($marginalita_migliore_percentuale > 20)
						$margine_provvigione = $miglior_margine / 100 * 30;
					else
						$margine_provvigione = $miglior_margine / 100 * 25;
					
					$provvigione = $margine_provvigione * 100 / $miglior_prezzo;
					
					$provvigione_personalizzata = Db::getInstance()->getValue('SELECT provvigione FROM product WHERE id_product = '.$obj->id);
					
				echo '</div>
				
				<div style="float:left; margin-left:20px">
				
				<strong>Margini minimi</strong> <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Margini minimi clienti e rivenditori per questo prodotto" title="Margini minimi clienti e rivenditori per questo prodotto" />
				<table cellspacing="0" cellpadding="0"><tr style="background-color:#ffcc99">
						<td class="col-left" style="width:260px">'.$this->l('Marg. min. CLIENTI').'</td>
						<td>
							<input size="5" maxlength="14" id="margin_min_cli" name="margin_min_cli" type="text" value="'.number_format(htmlentities($this->getFieldValue($obj, 'margin_min_cli'), ENT_COMPAT, 'UTF-8'),2,'.','').'" />%
							
						</td></tr>
						<tr style="background-color:#ffcc99">
						<td class="col-left" style="width:260px">'.$this->l('Marg. min. RIVENDITORI').'</td>
						<td>
							<input size="5" maxlength="14" id="margin_min_riv" name="margin_min_riv" type="text" value="'.number_format(htmlentities($this->getFieldValue($obj, 'margin_min_riv'), ENT_COMPAT, 'UTF-8'),2,'.','').'" />%
						
						</td>
						
					</tr>
					'.($map_price > 0 ? '
					<tr style="background-color:none">
						<td class="col-left" style="width:155px"><br /></td>
						<td>
							
						</td>
						
					</tr>
					
					<tr style="background-color:#f9932e;">
						<td class="col-left" style="width:260px">'.$this->l('Prezzo pubblicabile MAP').'</td>
						<td>
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="5" maxlength="14" readonly="readonly" id="map_price" name="map_price" type="text" value="'.number_format($map_price,2,'.','').'" /> 
						
						</td>
						
					</tr>' : '').'
					
					</table>
					<br /><br />
					<table cellspacing="0" cellpadding="0"><tr style="background-color:#8cd9f070">
						<td class="col-left" style="width:260px">'.$this->l('Premio Agente Standard').' <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Il premio agente corrisponde al 30% del miglior margine" title="Il premio agente corrisponde al 30% del miglior margine se questo è superiore al 20%, altrimenti il premio è del 25%" /></td>
						<td>
							<input size="5" maxlength="14" readonly="readonly" id="provvigione_standard" name="provvigione_standard" type="text" value="'.number_format($provvigione,2,'.','').'" />%
							
						</td></tr>
						<tr style="background-color:#8cd9f070">
						<td class="col-left" style="width:260px">'.$this->l('Premio Agente Personalizzato').'</td>
						<td>
							<input size="5" maxlength="14" id="provvigione" name="provvigione" type="text" value="'.($provvigione_personalizzata == 0 ? '' : number_format($provvigione_personalizzata,2,'.','')).'" />%
							
						</td></tr>
						
					</table>
				</div>';
					
			echo '<div style="clear:both"></div></div>';		
			
					
					$scontistica_amazon = Db::getInstance()->getRow('SELECT amazon_fr, amazon_uk, amazon_es, amazon_de, amazon_nl, amazon_se, amazon_it, amazon_pl FROM product WHERE id_product = '.$obj->id);
					
					

			echo '<br />';
					echo '
					<div style="float:left">
					<script type="text/javascript">
					function apriAmazon()
					{
						$(\'#amazon_es_row\').show();
						$(\'#amazon_se_row\').show();
						$(\'#amazon_pl_row\').show();
						$(\'#amazon_de_row\').show();
						$(\'#amazon_nl_row\').show();
						$(\'#amazon_it_row\').show();
						$(\'#amazon_uk_row\').show();
						$(\'#chiudi_amazon\').show();
						$(\'#apri_amazon\').hide();
					}
					
					function chiudiAmazon()
					{
						$(\'#amazon_es_row\').hide();
						$(\'#amazon_de_row\').hide();
						$(\'#amazon_it_row\').show();
						$(\'#amazon_uk_row\').hide();
						$(\'#chiudi_amazon\').hide();
						$(\'#amazon_nl_row\').hide();
						$(\'#amazon_se_row\').hide();
						$(\'#amazon_pl_row\').hide();
						$(\'#apri_amazon\').show();
					}
					</script>
					<strong>Scontistica Amazon</strong> <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Se ci sono prezzi speciali, i margini sono calcolati sui prezzi speciali, altrimenti i margini sono calcolati sui prezzi di listino" title="Se ci sono prezzi speciali, i margini sono calcolati sui prezzi speciali, altrimenti i margini sono calcolati sui prezzi di listino" /><table cellspacing="0" cellpadding="0" style="background-color:#000000; color:#ffffff">
					
					<a href="javascript:void(0)" onclick="chiudiAmazon()"><img src="../img/admin/forbbiden.gif" alt="Chiudi Amazon" title="Chiudi Amazon" id="chiudi_amazon" '.($scontistica_amazon['amazon_de'] != 0 || $scontistica_amazon['amazon_es'] != 0 || $scontistica_amazon['amazon_uk'] != 0 || $scontistica_amazon['amazon_it'] != 0 || $scontistica_amazon['amazon_nl'] != 0 || $scontistica_amazon['amazon_se'] != 0 ? '' : 'style="display:none"').'/></a>
					
					<a href="javascript:void(0)" onclick="apriAmazon()"><img src="../img/admin/add.gif" id="apri_amazon" alt="Apri Amazon" title="Apri Amazon" '.($scontistica_amazon['amazon_de'] != 0 || $scontistica_amazon['amazon_es'] != 0 || $scontistica_amazon['amazon_uk'] != 0 || $scontistica_amazon['amazon_it'] != 0 || $scontistica_amazon['amazon_nl'] != 0 || $scontistica_amazon['amazon_se'] != 0 || $scontistica_amazon['amazon_pl'] != 0 ? 'style="display:none"' : '').' /></a>
					<tr >
					<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('FR').'&nbsp;&nbsp;&nbsp;
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="14" style="text-align:right" id="sconto_amazon_fr" name="sconto_amazon_fr" type="text" value="'.(Tools::getIsset('sconto_amazon_fr') ? Tools::getValue('sconto_amazon_fr') : $scontistica_amazon['amazon_fr']).'"  onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto2(\'sconto_amazon_fr\', \'margin_amazon_fr\', \'miglior_prezzo\'); calcPrezzoSconto(\'sconto_amazon_fr\', \'prezzo_amazon_fr\', \'miglior_prezzo\'); if(document.getElementById(\'margin_amazon_fr\').value < 12.50) { document.getElementById(\'margine_amazon_fr_alert\').innerHTML = \'<span class=\&quot;span-reference\&quot; title=\&quot;Margine inferiore al 12,50%\&quot;><img src=&quot;../img/admin/error.png\&quot; alt=\&quot;Margine inferiore al 12,50%\&quot; style=\&quot;height:18px\&quot; /></span>\'; } else { document.getElementById(\'margine_amazon_fr_alert\').innerHTML = \'\'; }" />%
						
						</td>
						
							<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('Prezzo:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="11" style="text-align:right" id="prezzo_amazon_fr" name="prezzo_amazon_fr" 
			value="'.(Tools::getIsset('prezzo_amazon_fr') ? Tools::getValue('prezzo_amazon_fr') : number_format($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_fr']/100)),2,",","")).'" 				
							type="text"  onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'miglior_prezzo\', \'prezzo_amazon_fr\', \'sconto_amazon_fr\'); calcMarginalitaSconto2(\'sconto_amazon_fr\', \'margin_amazon_fr\', \'miglior_prezzo\'); if(document.getElementById(\'margin_amazon_fr\').value < 12.50) { document.getElementById(\'margine_amazon_fr_alert\').innerHTML = \'<span class=\&quot;span-reference\&quot; title=\&quot;Margine inferiore al 12,50%\&quot;><img src=&quot;../img/admin/error.png\&quot; alt=\&quot;Margine inferiore al 12,50%\&quot; style=\&quot;height:18px\&quot; /></span>\'; } else { document.getElementById(\'margine_amazon_fr_alert\').innerHTML = \'\'; }" />€
						
						</td>	
							
					
					<td class="col-left" style="width:160px; padding-bottom:5px;">'.$this->l('Marginalità:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="11" id="margin_amazon_fr" name="margin_amazon_fr" type="text"
							value="'.(Tools::getIsset('margin_amazon_fr') ? Tools::getValue('margin_amazon_fr') : number_format(((($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_fr']/100)))-$miglior_prezzo_acquisto)*100)/($miglior_prezzo-($miglior_prezzo*$scontistica_amazon['amazon_fr']/100)),2,'.','')).'" 
							onchange="this.value = this.value.replace(/,/g, \'.\');" />%
						 	
							<div id="margine_amazon_fr_alert" style="float:right; margin-right:3px">
								'.(((($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_fr']/100)))-$miglior_prezzo_acquisto)*100)/($miglior_prezzo-($miglior_prezzo*$scontistica_amazon['amazon_fr']/100)) < 12.50 ? '<span class="span-reference" title="Margine inferiore al 12,50%"><img src="../img/admin/error.png" alt="Margine inferiore al 12,50%" style="height:18px" /></span>' : '').'
							</div>
						
						</td>	
						
						
					</tr>

					
					
					
					<tr id="amazon_de_row" style="'.($scontistica_amazon['amazon_de'] != 0 || $scontistica_amazon['amazon_es'] != 0 || $scontistica_amazon['amazon_uk'] != 0 || $scontistica_amazon['amazon_it'] != 0 || $scontistica_amazon['amazon_se'] != 0 || $scontistica_amazon['amazon_pl'] != 0 ? '' : 'display:none').'">
					<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('DE').'&nbsp;&nbsp;&nbsp;
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="14" style="text-align:right" id="sconto_amazon_de" name="sconto_amazon_de" type="text" value="'.(Tools::getIsset('sconto_amazon_de') ? Tools::getValue('sconto_amazon_de') : $scontistica_amazon['amazon_de']).'"  onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto2(\'sconto_amazon_de\', \'margin_amazon_de\', \'miglior_prezzo\'); calcPrezzoSconto(\'sconto_amazon_de\', \'prezzo_amazon_de\', \'miglior_prezzo\'); if(document.getElementById(\'margin_amazon_de\').value < 12.50) { document.getElementById(\'margine_amazon_de_alert\').innerHTML = \'<span class=\&quot;span-reference\&quot; title=\&quot;Margine inferiore al 12,50%\&quot;><img src=&quot;../img/admin/error.png\&quot; alt=\&quot;Margine inferiore al 12,50%\&quot; style=\&quot;height:18px\&quot; /></span>\'; } else { document.getElementById(\'margine_amazon_de_alert\').innerHTML = \'\'; }" />%
						
						</td>
						
							<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('Prezzo:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="11" style="text-align:right" id="prezzo_amazon_de" name="prezzo_amazon_de" 
			value="'.(Tools::getIsset('prezzo_amazon_de') ? Tools::getValue('prezzo_amazon_de') : number_format($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_de']/100)),2,",","")).'" 				
							type="text"  onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'miglior_prezzo\', \'prezzo_amazon_de\', \'sconto_amazon_de\'); calcMarginalitaSconto2(\'sconto_amazon_de\', \'margin_amazon_de\', \'miglior_prezzo\'); if(document.getElementById(\'margin_amazon_de\').value < 12.50) { document.getElementById(\'margine_amazon_de_alert\').innerHTML = \'<span class=\&quot;span-reference\&quot; title=\&quot;Margine inferiore al 12,50%\&quot;><img src=&quot;../img/admin/error.png\&quot; alt=\&quot;Margine inferiore al 12,50%\&quot; style=\&quot;height:18px\&quot; /></span>\'; } else { document.getElementById(\'margine_amazon_de_alert\').innerHTML = \'\'; }" />€
						
						</td>	
							
					
					<td class="col-left" style="width:160px; padding-bottom:5px;">'.$this->l('Marginalità:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="11" id="margin_amazon_de" name="margin_amazon_de" type="text"
							value="'.(Tools::getIsset('margin_amazon_de') ? Tools::getValue('margin_amazon_de') : number_format(((($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_de']/100)))-$miglior_prezzo_acquisto)*100)/($miglior_prezzo-($miglior_prezzo*$scontistica_amazon['amazon_de']/100)),2,'.','')).'" 
							onchange="this.value = this.value.replace(/,/g, \'.\');" />%
						 	
							<div id="margine_amazon_de_alert" style="float:right; margin-right:3px">
								'.(((($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_de']/100)))-$miglior_prezzo_acquisto)*100)/($miglior_prezzo-($miglior_prezzo*$scontistica_amazon['amazon_de']/100)) < 12.50 ? '<span class="span-reference" title="Margine inferiore al 12,50%"><img src="../img/admin/error.png" alt="Margine inferiore al 12,50%" style="height:18px" /></span>' : '').'
							</div>
						
						
						</td>	
						
						
					</tr>
					
					<tr id="amazon_nl_row" style="'.($scontistica_amazon['amazon_de'] != 0 || $scontistica_amazon['amazon_es'] != 0 || $scontistica_amazon['amazon_uk'] != 0 || $scontistica_amazon['amazon_it'] || $scontistica_amazon['amazon_nl'] != 0 || $scontistica_amazon['amazon_se'] != 0 || $scontistica_amazon['amazon_pl'] != 0 ? '' : 'display:none').'">
					<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('NL').'&nbsp;&nbsp;&nbsp;
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="14" style="text-align:right" id="sconto_amazon_nl" name="sconto_amazon_nl" type="text" value="'.(Tools::getIsset('sconto_amazon_nl') ? Tools::getValue('sconto_amazon_nl') : $scontistica_amazon['amazon_nl']).'"  onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto2(\'sconto_amazon_nl\', \'margin_amazon_nl\', \'miglior_prezzo\'); calcPrezzoSconto(\'sconto_amazon_nl\', \'prezzo_amazon_nl\', \'miglior_prezzo\'); if(document.getElementById(\'margin_amazon_nl\').value < 12.50) { document.getElementById(\'margine_amazon_nl_alert\').innerHTML = \'<span class=\&quot;span-reference\&quot; title=\&quot;Margine inferiore al 12,50%\&quot;><img src=&quot;../img/admin/error.png\&quot; alt=\&quot;Margine inferiore al 12,50%\&quot; style=\&quot;height:18px\&quot; /></span>\'; } else { document.getElementById(\'margine_amazon_nl_alert\').innerHTML = \'\'; }" />%
						
						</td>
						
							<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('Prezzo:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="11" style="text-align:right" id="prezzo_amazon_nl" name="prezzo_amazon_nl" 
			value="'.(Tools::getIsset('prezzo_amazon_nl') ? Tools::getValue('prezzo_amazon_nl') : number_format($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_nl']/100)),2,",","")).'" 				
							type="text"  onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'miglior_prezzo\', \'prezzo_amazon_nl\', \'sconto_amazon_nl\'); calcMarginalitaSconto2(\'sconto_amazon_nl\', \'margin_amazon_nl\', \'miglior_prezzo\'); if(document.getElementById(\'margin_amazon_nl\').value < 12.50) { document.getElementById(\'margine_amazon_nl_alert\').innerHTML = \'<span class=\&quot;span-reference\&quot; title=\&quot;Margine inferiore al 12,50%\&quot;><img src=&quot;../img/admin/error.png\&quot; alt=\&quot;Margine inferiore al 12,50%\&quot; style=\&quot;height:18px\&quot; /></span>\'; } else { document.getElementById(\'margine_amazon_nl_alert\').innerHTML = \'\'; }" />€
						
						</td>	
							
					
					<td class="col-left" style="width:160px; padding-bottom:5px;">'.$this->l('Marginalità:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="11" id="margin_amazon_nl" name="margin_amazon_nl" type="text"
							value="'.(Tools::getIsset('margin_amazon_nl') ? Tools::getValue('margin_amazon_nl') : number_format(((($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_nl']/100)))-$miglior_prezzo_acquisto)*100)/($miglior_prezzo-($miglior_prezzo*$scontistica_amazon['amazon_nl']/100)),2,'.','')).'" 
							onchange="this.value = this.value.replace(/,/g, \'.\');" />%
						 	
							<div id="margine_amazon_nl_alert" style="float:right; margin-right:3px">
								'.(((($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_nl']/100)))-$miglior_prezzo_acquisto)*100)/($miglior_prezzo-($miglior_prezzo*$scontistica_amazon['amazon_nl']/100)) < 12.50 ? '<span class="span-reference" title="Margine inferiore al 12,50%"><img src="../img/admin/error.png" alt="Margine inferiore al 12,50%" style="height:18px" /></span>' : '').'
							</div>
						
						
						</td>	
						
						
					</tr>
					
					<tr id="amazon_se_row" style="'.($scontistica_amazon['amazon_de'] != 0 || $scontistica_amazon['amazon_es'] != 0 || $scontistica_amazon['amazon_uk'] != 0 || $scontistica_amazon['amazon_it'] || $scontistica_amazon['amazon_nl'] != 0 ? '' : 'display:none').'">
					<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('SE').'&nbsp;&nbsp;&nbsp;
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="14" style="text-align:right" id="sconto_amazon_se" name="sconto_amazon_se" type="text" value="'.(Tools::getIsset('sconto_amazon_se') ? Tools::getValue('sconto_amazon_se') : $scontistica_amazon['amazon_se']).'"  onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto2(\'sconto_amazon_se\', \'margin_amazon_se\', \'miglior_prezzo\'); calcPrezzoSconto(\'sconto_amazon_se\', \'prezzo_amazon_se\', \'miglior_prezzo\'); if(document.getElementById(\'margin_amazon_se\').value < 12.50) { document.getElementById(\'margine_amazon_se_alert\').innerHTML = \'<span class=\&quot;span-reference\&quot; title=\&quot;Margine inferiore al 12,50%\&quot;><img src=&quot;../img/admin/error.png\&quot; alt=\&quot;Margine inferiore al 12,50%\&quot; style=\&quot;height:18px\&quot; /></span>\'; } else { document.getElementById(\'margine_amazon_se_alert\').innerHTML = \'\'; }" />%
						
						</td>
						
							<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('Prezzo:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="11" style="text-align:right" id="prezzo_amazon_se" name="prezzo_amazon_se" 
			value="'.(Tools::getIsset('prezzo_amazon_se') ? Tools::getValue('prezzo_amazon_se') : number_format($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_se']/100)),2,",","")).'" 				
							type="text"  onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'miglior_prezzo\', \'prezzo_amazon_se\', \'sconto_amazon_se\'); calcMarginalitaSconto2(\'sconto_amazon_se\', \'margin_amazon_se\', \'miglior_prezzo\'); if(document.getElementById(\'margin_amazon_se\').value < 12.50) { document.getElementById(\'margine_amazon_se_alert\').innerHTML = \'<span class=\&quot;span-reference\&quot; title=\&quot;Margine inferiore al 12,50%\&quot;><img src=&quot;../img/admin/error.png\&quot; alt=\&quot;Margine inferiore al 12,50%\&quot; style=\&quot;height:18px\&quot; /></span>\'; } else { document.getElementById(\'margine_amazon_se_alert\').innerHTML = \'\'; }" />€
						
						</td>	
							
					
					<td class="col-left" style="width:160px; padding-bottom:5px;">'.$this->l('Marginalità:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="11" id="margin_amazon_se" name="margin_amazon_se" type="text"
							value="'.(Tools::getIsset('margin_amazon_se') ? Tools::getValue('margin_amazon_se') : number_format(((($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_se']/100)))-$miglior_prezzo_acquisto)*100)/($miglior_prezzo-($miglior_prezzo*$scontistica_amazon['amazon_se']/100)),2,'.','')).'" 
							onchange="this.value = this.value.replace(/,/g, \'.\');" />%
						 	
							<div id="margine_amazon_se_alert" style="float:right; margin-right:3px">
								'.(((($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_se']/100)))-$miglior_prezzo_acquisto)*100)/($miglior_prezzo-($miglior_prezzo*$scontistica_amazon['amazon_se']/100)) < 12.50 ? '<span class="span-reference" title="Margine inferiore al 12,50%"><img src="../img/admin/error.png" alt="Margine inferiore al 12,50%" style="height:18px" /></span>' : '').'
							</div>
						
						
						</td>	
						
						
					</tr>
					
					<tr id="amazon_pl_row" style="'.($scontistica_amazon['amazon_de'] != 0 || $scontistica_amazon['amazon_es'] != 0 || $scontistica_amazon['amazon_uk'] != 0 || $scontistica_amazon['amazon_it'] || $scontistica_amazon['amazon_nl'] != 0  || $scontistica_amazon['amazon_se'] != 0 ? '' : 'display:none').'">
					<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('PL').'&nbsp;&nbsp;&nbsp;
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="14" style="text-align:right" id="sconto_amazon_pl" name="sconto_amazon_pl" type="text" value="'.(Tools::getIsset('sconto_amazon_pl') ? Tools::getValue('sconto_amazon_pl') : $scontistica_amazon['amazon_pl']).'"  onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto2(\'sconto_amazon_pl\', \'margin_amazon_pl\', \'miglior_prezzo\'); calcPrezzoSconto(\'sconto_amazon_pl\', \'prezzo_amazon_pl\', \'miglior_prezzo\'); if(document.getElementById(\'margin_amazon_pl\').value < 12.50) { document.getElementById(\'margine_amazon_pl_alert\').innerHTML = \'<span class=\&quot;span-reference\&quot; title=\&quot;Margine inferiore al 12,50%\&quot;><img src=&quot;../img/admin/error.png\&quot; alt=\&quot;Margine inferiore al 12,50%\&quot; style=\&quot;height:18px\&quot; /></span>\'; } else { document.getElementById(\'margine_amazon_pl_alert\').innerHTML = \'\'; }" />%
						
						</td>
						
							<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('Prezzo:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="11" style="text-align:right" id="prezzo_amazon_pl" name="prezzo_amazon_pl" 
			value="'.(Tools::getIsset('prezzo_amazon_pl') ? Tools::getValue('prezzo_amazon_pl') : number_format($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_pl']/100)),2,",","")).'" 				
							type="text"  onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'miglior_prezzo\', \'prezzo_amazon_pl\', \'sconto_amazon_pl\'); calcMarginalitaSconto2(\'sconto_amazon_pl\', \'margin_amazon_pl\', \'miglior_prezzo\'); if(document.getElementById(\'margin_amazon_pl\').value < 12.50) { document.getElementById(\'margine_amazon_pl_alert\').innerHTML = \'<span class=\&quot;span-reference\&quot; title=\&quot;Margine inferiore al 12,50%\&quot;><img src=&quot;../img/admin/error.png\&quot; alt=\&quot;Margine inferiore al 12,50%\&quot; style=\&quot;height:18px\&quot; /></span>\'; } else { document.getElementById(\'margine_amazon_pl_alert\').innerHTML = \'\'; }" />€
						
						</td>	
							
					
					<td class="col-left" style="width:160px; padding-bottom:5px;">'.$this->l('Marginalità:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="11" id="margin_amazon_pl" name="margin_amazon_pl" type="text"
							value="'.(Tools::getIsset('margin_amazon_pl') ? Tools::getValue('margin_amazon_pl') : number_format(((($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_pl']/100)))-$miglior_prezzo_acquisto)*100)/($miglior_prezzo-($miglior_prezzo*$scontistica_amazon['amazon_pl']/100)),2,'.','')).'" 
							onchange="this.value = this.value.replace(/,/g, \'.\');" />%
						 	
							<div id="margine_amazon_pl_alert" style="float:right; margin-right:3px">
								'.(((($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_pl']/100)))-$miglior_prezzo_acquisto)*100)/($miglior_prezzo-($miglior_prezzo*$scontistica_amazon['amazon_pl']/100)) < 12.50 ? '<span class="span-reference" title="Margine inferiore al 12,50%"><img src="../img/admin/error.png" alt="Margine inferiore al 12,50%" style="height:18px" /></span>' : '').'
							</div>
						
						
						</td>	
						
						
					</tr>
					
					
					
					
					<tr id="amazon_es_row" style="'.($scontistica_amazon['amazon_de'] != 0 || $scontistica_amazon['amazon_es'] != 0 || $scontistica_amazon['amazon_uk'] != 0 || $scontistica_amazon['amazon_it'] != 0 || $scontistica_amazon['amazon_nl'] != 0 ? '' : 'display:none').'">
					<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('ES').'&nbsp;&nbsp;&nbsp;
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="14" style="text-align:right" id="sconto_amazon_es" name="sconto_amazon_es" type="text" value="'.(Tools::getIsset('sconto_amazon_es') ? Tools::getValue('sconto_amazon_es') : $scontistica_amazon['amazon_es']).'"  onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto2(\'sconto_amazon_es\', \'margin_amazon_es\', \'miglior_prezzo\'); calcPrezzoSconto(\'sconto_amazon_es\', \'prezzo_amazon_es\', \'miglior_prezzo\'); if(document.getElementById(\'margin_amazon_es\').value < 12.50) { document.getElementById(\'margine_amazon_es_alert\').innerHTML = \'<span class=\&quot;span-reference\&quot; title=\&quot;Margine inferiore al 12,50%\&quot;><img src=&quot;../img/admin/error.png\&quot; alt=\&quot;Margine inferiore al 12,50%\&quot; style=\&quot;height:18px\&quot; /></span>\'; } else { document.getElementById(\'margine_amazon_es_alert\').innerHTML = \'\'; }" />%
						
						</td>
						
							<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('Prezzo:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="11" style="text-align:right" id="prezzo_amazon_es" name="prezzo_amazon_es" 
			value="'.(Tools::getIsset('prezzo_amazon_es') ? Tools::getValue('prezzo_amazon_es') : number_format($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_es']/100)),2,",","")).'" 				
							type="text"  onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'miglior_prezzo\', \'prezzo_amazon_es\', \'sconto_amazon_es\'); calcMarginalitaSconto2(\'sconto_amazon_es\', \'margin_amazon_es\', \'miglior_prezzo\');  if(document.getElementById(\'margin_amazon_es\').value < 12.50) { document.getElementById(\'margine_amazon_es_alert\').innerHTML = \'<span class=\&quot;span-reference\&quot; title=\&quot;Margine inferiore al 12,50%\&quot;><img src=&quot;../img/admin/error.png\&quot; alt=\&quot;Margine inferiore al 12,50%\&quot; style=\&quot;height:18px\&quot; /></span>\'; } else { document.getElementById(\'margine_amazon_es_alert\').innerHTML = \'\'; }" />€
						
						</td>	
							
					
					<td class="col-left" style="width:160px; padding-bottom:5px;">'.$this->l('Marginalità:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="11" id="margin_amazon_es" name="margin_amazon_es" type="text"
							value="'.(Tools::getIsset('margin_amazon_es') ? Tools::getValue('margin_amazon_es') : number_format(((($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_es']/100)))-$miglior_prezzo_acquisto)*100)/($miglior_prezzo-($miglior_prezzo*$scontistica_amazon['amazon_es']/100)),2,'.','')).'" 
							onchange="this.value = this.value.replace(/,/g, \'.\');" />%
						 	
							<div id="margine_amazon_es_alert" style="float:right; margin-right:3px">
								'.(((($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_es']/100)))-$miglior_prezzo_acquisto)*100)/($miglior_prezzo-($miglior_prezzo*$scontistica_amazon['amazon_es']/100)) < 12.50 ? '<span class="span-reference" title="Margine inferiore al 12,50%"><img src="../img/admin/error.png" alt="Margine inferiore al 12,50%" style="height:18px" /></span>' : '').'
							</div>
						
						
						</td>	
						
						
					</tr>
					
					<tr id="amazon_it_row" style="'.($scontistica_amazon['amazon_de'] != 0 || $scontistica_amazon['amazon_it'] != 0 || $scontistica_amazon['amazon_uk'] != 0 || $scontistica_amazon['amazon_it'] != 0 || $scontistica_amazon['amazon_nl'] != 0  || $scontistica_amazon['amazon_se'] != 0 || $scontistica_amazon['amazon_pl'] != 0 ? '' : 'display:none').'">
					<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('IT').'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="14" style="text-align:right" id="sconto_amazon_it" name="sconto_amazon_it" type="text" value="'.(Tools::getIsset('sconto_amazon_it') ? Tools::getValue('sconto_amazon_it') : $scontistica_amazon['amazon_it']).'"  onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto2(\'sconto_amazon_it\', \'margin_amazon_it\', \'miglior_prezzo\'); calcPrezzoSconto(\'sconto_amazon_it\', \'prezzo_amazon_it\', \'miglior_prezzo\');  if(document.getElementById(\'margin_amazon_it\').value < 12.50) { document.getElementById(\'margine_amazon_it_alert\').innerHTML = \'<span class=\&quot;span-reference\&quot; title=\&quot;Margine inferiore al 12,50%\&quot;><img src=&quot;../img/admin/error.png\&quot; alt=\&quot;Margine inferiore al 12,50%\&quot; style=\&quot;height:18px\&quot; /></span>\'; } else { document.getElementById(\'margine_amazon_it_alert\').innerHTML = \'\'; }" />%
						
						</td>
						
							<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('Prezzo:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="11" style="text-align:right" id="prezzo_amazon_it" name="prezzo_amazon_it" 
			value="'.(Tools::getIsset('prezzo_amazon_it') ? Tools::getValue('prezzo_amazon_it') : number_format($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_it']/100)),2,",","")).'" 				
							type="text"  onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'miglior_prezzo\', \'prezzo_amazon_it\', \'sconto_amazon_it\'); calcMarginalitaSconto2(\'sconto_amazon_it\', \'margin_amazon_it\', \'miglior_prezzo\');  if(document.getElementById(\'margin_amazon_it\').value < 12.50) { document.getElementById(\'margine_amazon_it_alert\').innerHTML = \'<span class=\&quot;span-reference\&quot; title=\&quot;Margine inferiore al 12,50%\&quot;><img src=&quot;../img/admin/error.png\&quot; alt=\&quot;Margine inferiore al 12,50%\&quot; style=\&quot;height:18px\&quot; /></span>\'; } else { document.getElementById(\'margine_amazon_it_alert\').innerHTML = \'\'; }" />€
						
						</td>	
							
					
					<td class="col-left" style="width:160px; padding-bottom:5px;">'.$this->l('Marginalità:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="11" id="margin_amazon_it" name="margin_amazon_it" type="text"
							value="'.(Tools::getIsset('margin_amazon_it') ? Tools::getValue('margin_amazon_it') : number_format(((($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_it']/100)))-$miglior_prezzo_acquisto)*100)/($miglior_prezzo-($miglior_prezzo*$scontistica_amazon['amazon_it']/100)),2,'.','')).'" 
							onchange="this.value = this.value.replace(/,/g, \'.\'); " />%
						 	
						
							<div id="margine_amazon_it_alert" style="float:right; margin-right:3px">
								'.(((($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_it']/100)))-$miglior_prezzo_acquisto)*100)/($miglior_prezzo-($miglior_prezzo*$scontistica_amazon['amazon_it']/100)) < 12.50 ? '<span class="span-reference" title="Margine inferiore al 12,50%"><img src="../img/admin/error.png" alt="Margine inferiore al 12,50%" style="height:18px" /></span>' : '').'
							</div>
						
						</td>	
						
						
					</tr>
					
					<tr id="amazon_uk_row" style="'.($scontistica_amazon['amazon_de'] != 0 || $scontistica_amazon['amazon_es'] != 0 || $scontistica_amazon['amazon_uk'] != 0 || $scontistica_amazon['amazon_it'] != 0 || $scontistica_amazon['amazon_nl'] != 0 || $scontistica_amazon['amazon_se'] != 0 || $scontistica_amazon['amazon_pl'] != 0 ? '' : 'display:none').'">
					<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('UK').'&nbsp;&nbsp;&nbsp;
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="14" style="text-align:right" id="sconto_amazon_uk" name="sconto_amazon_uk" type="text" value="'.(Tools::getIsset('sconto_amazon_uk') ? Tools::getValue('sconto_amazon_uk') : $scontistica_amazon['amazon_uk']).'"  onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto2(\'sconto_amazon_uk\', \'margin_amazon_uk\', \'miglior_prezzo\'); calcPrezzoSconto(\'sconto_amazon_uk\', \'prezzo_amazon_uk\', \'miglior_prezzo\');  if(document.getElementById(\'margin_amazon_uk\').value < 12.50) { document.getElementById(\'margine_amazon_uk_alert\').innerHTML = \'<span class=\&quot;span-reference\&quot; title=\&quot;Margine inferiore al 12,50%\&quot;><img src=&quot;../img/admin/error.png\&quot; alt=\&quot;Margine inferiore al 12,50%\&quot; style=\&quot;height:18px\&quot; /></span>\'; } else { document.getElementById(\'margine_amazon_uk_alert\').innerHTML = \'\'; }" />%
						
						</td>
						
							<td class="col-left" style="width:200px; padding-bottom:5px;">'.$this->l('Prezzo:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="6" maxlength="11" style="text-align:right" id="prezzo_amazon_uk" name="prezzo_amazon_uk" 
			value="'.(Tools::getIsset('prezzo_amazon_uk') ? Tools::getValue('prezzo_amazon_uk') : number_format($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_uk']/100)),2,",","")).'" 				
							type="text"  onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'miglior_prezzo\', \'prezzo_amazon_uk\', \'sconto_amazon_uk\'); calcMarginalitaSconto2(\'sconto_amazon_uk\', \'margin_amazon_uk\', \'miglior_prezzo\'); if(document.getElementById(\'margin_amazon_uk\').value < 12.50) { document.getElementById(\'margine_amazon_uk_alert\').innerHTML = \'<span class=\&quot;span-reference\&quot; title=\&quot;Margine inferiore al 12,50%\&quot;><img src=&quot;../img/admin/error.png\&quot; alt=\&quot;Margine inferiore al 12,50%\&quot; style=\&quot;height:18px\&quot; /></span>\'; } else { document.getElementById(\'margine_amazon_uk_alert\').innerHTML = \'\'; }
							" />€
						
						</td>	
							
					
					<td class="col-left" style="width:160px; padding-bottom:5px;">'.$this->l('Marginalità:').'
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="2" maxlength="11" id="margin_amazon_uk" name="margin_amazon_uk" type="text"
							value="'.(Tools::getIsset('margin_amazon_uk') ? Tools::getValue('margin_amazon_uk') : number_format(((($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_uk']/100)))-$miglior_prezzo_acquisto)*100)/($miglior_prezzo-($miglior_prezzo*$scontistica_amazon['amazon_uk']/100)),2,'.','')).'" 
							onchange="this.value = this.value.replace(/,/g, \'.\');" />%
						 	
						
							<div id="margine_amazon_uk_alert" style="float:right; margin-right:3px">
								'.(((($miglior_prezzo-($miglior_prezzo*($scontistica_amazon['amazon_uk']/100)))-$miglior_prezzo_acquisto)*100)/($miglior_prezzo-($miglior_prezzo*$scontistica_amazon['amazon_uk']/100)) < 12.50 ? '<span class="span-reference" title="Margine inferiore al 12,50%"><img src="../img/admin/error.png" alt="Margine inferiore al 12,50%" style="height:18px" /></span>' : '').'
							</div>
						
						</td>	
						
						
					</tr>
					
					</table></div>
					
					<div style="float:left; margin-left:20px">
					<strong>Miglior prezzo attuale (clienti web, per 1 pz.)</strong><br />
					<span style="font-size:18px">'.Tools::displayPrice($miglior_prezzo,1).'</span>
					'.($occhio_vendita == 1 ? '<br /><strong style="color:red">ATTENZIONE<br /> c\'&egrave; un prezzo vendita speciale<br /> ma siamo sotto scorta. <br />I margini sono calcolati sui prezzi di listino.</strong>' : '').'
					<input type="hidden" name="miglior_prezzo" id="miglior_prezzo" value="'.$miglior_prezzo.'" />
					
					<input type="hidden" name="miglior_prezzo_appoggio" id="miglior_prezzo_appoggio" value="'.$miglior_prezzo.'" />
					<br /><br />
					
					<strong>Miglior prezzo acquisto</strong><br />
					<span style="font-size:18px">'.Tools::displayPrice($miglior_prezzo_acquisto,1).'  - Marginalit&agrave;: '.number_format($marginalita_migliore_percentuale,2,",","").'%</span>
					'.($occhio_acquisto == 1 ? '<br /><strong style="color:red">ATTENZIONE<br /> c\'&egrave; un prezzo acquisto speciale<br /> ma siamo sotto scorta. <br />I margini sono calcolati sui prezzi di listino.</strong>' : '').'
					<input type="hidden" name="miglior_prezzo_acquisto" id="miglior_prezzo_acquisto" value="'.$miglior_prezzo_acquisto.'" />
					</div>';
					//  in value sopra rimetterci $miglior_prezzo_acquisto
				echo '
				
				<div style="clear:both"></div>
				
				<br /><br />';
				
					
					echo '<table style="float:left">.
							<tr><td>Qta al 08/01/2020:</td><td> <input size="3" type="text" name="qt_08" readonly="readonly" value="'.Db::getInstance()->getValue('SELECT qt_esolver FROM product WHERE id_product = '.$obj->id).'" />
							<input type="hidden" name="supplier_quantity" value="'.($obj->id ? $this->getFieldValue($obj, 'supplier_quantity') : 0).'" /></td></tr>
							<tr><td>Mod. qta mag.:</td><td> <input size="3" type="text" name="stock_quantity" value="'.($obj->id ? $this->getFieldValue($obj, 'stock_quantity') : 0).'" />
							<input type="hidden" name="supplier_quantity" value="'.($obj->id ? $this->getFieldValue($obj, 'supplier_quantity') : 0).'" /></td></tr>
							
							<!-- <tr><td>'.$this->l('Qta min.:').'</td><td>	<input size="3" maxlength="10" name="minimal_quantity" id="minimal_quantity" type="text" value="'.($obj->id ? ($this->getFieldValue($obj, 'minimal_quantity') ? $this->getFieldValue($obj, 'minimal_quantity') : 1) : 0).'" /></td></tr> -->
							</table>
							<div style="clear:both"></div>
					<table><tr>
						
					</tr>';
					$tax_rules_groups = TaxRulesGroup::getTaxRulesGroups(true);
					$taxesRatesByGroup = TaxRulesGroup::getAssociatedTaxRatesByIdCountry(Country::getDefaultCountryId());
					$ecotaxTaxRate = Tax::getProductEcotaxRate();
					echo '<script type="text/javascript">';
					echo 'noTax = '.(Tax::excludeTaxeOption() ? 'true' : 'false'), ";\n";
					echo 'taxesArray = new Array ();'."\n";
					echo 'taxesArray[0] = 0', ";\n";

					foreach ($tax_rules_groups AS $tax_rules_group)
					{
						$tax_rate = (array_key_exists($tax_rules_group['id_tax_rules_group'], $taxesRatesByGroup) ?  $taxesRatesByGroup[$tax_rules_group['id_tax_rules_group']] : 0);
						echo 'taxesArray['.$tax_rules_group['id_tax_rules_group'].']='.$tax_rate."\n";
					}
					echo '
						ecotaxTaxRate = '.($ecotaxTaxRate / 100).';
					</script>';
					echo '
					<tr >
						<td class="col-left">'.$this->l('Tax rule:').'</td>
						<td style="padding-bottom:5px;">
					<span '.(Tax::excludeTaxeOption() ? 'style="display:none;"' : '' ).'>
					 <select onChange="javascript:calcPriceTI(); unitPriceWithTax(\'unit\');" name="id_tax_rules_group" id="id_tax_rules_group" '.(Tax::excludeTaxeOption() ? 'disabled="disabled"' : '' ).'>
						';

						foreach ($tax_rules_groups AS $tax_rules_group)
							echo '<option value="'.$tax_rules_group['id_tax_rules_group'].'" '.(($this->getFieldValue($obj, 'id_tax_rules_group') == $tax_rules_group['id_tax_rules_group']) ? ' selected="selected"' : '').'>'.Tools::htmlentitiesUTF8($tax_rules_group['name']).'</option>';
				//echo ' <option value="0">'.$this->l('No Tax').'</option>';
				echo '</select>

				<a href="?tab=AdminTaxRulesGroup&addtax_rules_group&token='.Tools::getAdminToken('AdminTaxRulesGroup'.(int)(Tab::getIdFromClassName('AdminTaxRulesGroup')).(int)($cookie->id_employee)).'&id_product='.(int)$obj->id.'" onclick="return confirm(\''.$this->l('Are you sure you want to delete entered product information?', __CLASS__, true, false).'\');"><img src="../img/admin/add.gif" alt="'.$this->l('Create').'" title="'.$this->l('Create').'" /> <b>'.$this->l('Create').'</b></a></span>
				';
				if (Tax::excludeTaxeOption())
				{
					echo '<span style="margin-left:10px; color:red;">'.$this->l('Taxes are currently disabled').'</span> (<b><a href="index.php?tab=AdminTaxes&token='.Tools::getAdminToken('AdminTaxes'.(int)(Tab::getIdFromClassName('AdminTaxes')).(int)($cookie->id_employee)).'">'.$this->l('Tax options').'</a></b>)';
					echo '<input type="hidden" value="'.(int)($this->getFieldValue($obj, 'id_tax_rules_group')).'" name="id_tax_rules_group" />';
				}


				echo '</td>
					</tr>
				';
				if (Configuration::get('PS_USE_ECOTAX'))
					echo '
					<tr>
						<td class="col-left">'.$this->l('Eco-tax (tax incl.):').'</td>
						<td style="padding-bottom:5px;">
							'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input size="11" maxlength="14" id="ecotax" name="ecotax" type="text" value="'.$this->getFieldValue($obj, 'ecotax').'" onkeyup="if (isArrowKey(event))return; calcPriceTE(); this.value = this.value.replace(/,/g, \'.\'); if (parseInt(this.value) > getE(\'priceTE\').value) this.value = getE(\'priceTE\').value; if (isNaN(this.value)) this.value = 0;" />'.($currency->format % 2 == 0 ? ' '.$currency->sign : '').'
							<span style="margin-left:10px">('.$this->l('already included in price').')</span>
						</td>
					</tr>';

				if ($default_country->display_tax_label)
				{
					 /*echo '
						<tr '.(Tax::excludeTaxeOption() ? 'style="display:none"' : '' ).'>
							<td class="col-left">'.$this->l('Retail price with tax:').'</td>
							<td style="padding-bottom:5px;">
								'.($currency->format % 2 != 0 ? ' '.$currency->sign : '').' <input size="11" maxlength="14" id="priceTI" type="text" value="" onchange="noComma(\'priceTI\');" onkeyup="if (isArrowKey(event)) return;  calcPriceTE();" />'.($currency->format % 2 == 0 ? ' '.$currency->sign : '').'
							</td>
						</tr>';*/
				} else {
					echo '<input size="11" maxlength="14" id="priceTI" type="hidden" value="" onchange="noComma(\'priceTI\');" onkeyup="if (isArrowKey(event)) return;  calcPriceTE();" />';
				}
				echo '
					<tr id="tr_unit_price" style="display:none">
						<td class="col-left">'.$this->l('Unit price without tax:').'</td>
						<td style="padding-bottom:5px;">
							'.($currency->format % 2 != 0 ? ' '.$currency->sign : '').' <input size="11" maxlength="14" id="unit_price" name="unit_price" type="text" value="'.($this->getFieldValue($obj, 'unit_price_ratio') != 0 ? Tools::ps_round($this->getFieldValue($obj, 'price') / $this->getFieldValue($obj, 'unit_price_ratio'), 2) : 0).'" onkeyup="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, \'.\'); unitPriceWithTax(\'unit\');"/>'.($currency->format % 2 == 0 ? ' '.$currency->sign : '').' '.$this->l('per').' <input size="6" maxlength="10" id="unity" name="unity" type="text" value="'.(Validate::isCleanHtml($this->getFieldValue($obj, 'unity')) ? htmlentities($this->getFieldValue($obj, 'unity'), ENT_QUOTES, 'UTF-8') : '').'" onkeyup="if (isArrowKey(event)) return ;unitySecond();" onchange="unitySecond();"/>'.
							(Configuration::get('PS_TAX') && $default_country->display_tax_label ? '<span style="margin-left:15px">'.$this->l('or').' '.($currency->format % 2 != 0 ? ' '.$currency->sign : '').'<span id="unit_price_with_tax">0.00</span>'.($currency->format % 2 == 0 ? ' '.$currency->sign : '').' '.$this->l('per').' <span id="unity_second">'.(Validate::isCleanHtml($this->getFieldValue($obj, 'unity')) ? htmlentities($this->getFieldValue($obj, 'unity'), ENT_QUOTES, 'UTF-8') : '').'</span> '.$this->l('with tax') : '').'</span>
							<p>'.$this->l('Eg. $15 per Lb').'</p>
						</td>
					</tr>
					<tr style="display:none">
						<td class="col-left">&nbsp;</td>
						<td style="padding-bottom:5px;">
							<input type="checkbox" name="on_sale" id="on_sale" style="padding-top: 5px;" '.($this->getFieldValue($obj, 'on_sale') ? 'checked="checked"' : '').'value="1" />&nbsp;<label for="on_sale" class="t">'.$this->l('Display "on sale" icon on product page and text on product listing').'</label>
						</td>
					</tr>
					<tr>
						
					</tr>
					<tr style="display:none">
						<td class="col-left">&nbsp;</td>
						<td>
							<div class="hint clear" style="display: block;width: 70%;">'.$this->l('You can define many discounts and specific price rules in the Prices tab').'</div>
						</td>
					</tr>';

					$languages = Language::getLanguages(false);

					
				

				echo '
					<tr><td colspan="2" style="padding-bottom:5px;"><hr style="width:100%;" /></td></tr>
					</table>
					<table>
					<tr>
						<td style="width:300px">'.$this->l('Additional shipping cost:').'</td>
						<td style="padding-bottom:5px;">
							<input type="text" name="additional_shipping_cost" value="'.Tools::safeOutput($this->getFieldValue($obj, 'additional_shipping_cost')).'" />'.($currency->format % 2 == 0 ? ' '.$currency->sign : '');
							if ($default_country->display_tax_label)
								echo ' ('.$this->l('tax excl.').')';

					echo '<p>'.$this->l('Carrier tax will be applied.').'</p>
						</td>
					</tr>
					';
							
		$row = Db::getInstance()->getRow("SELECT value FROM configuration WHERE id_configuration = 240");

$trasp = unserialize($row['value']);

$au = $_GET['id_product'];



foreach($trasp as $el) {
if ($au == $el) {
$uu = 1;
break;
}
else {
$uu = 0;
}
}

if($au == "") {
$uu = 0;
}
							echo '
							<tr>
								<td class="col-left">Forzare trasporto gratuito</td>
						<td style="padding-bottom:5px;">
						<input type="hidden" name="check_trasporto_gratuito" />
							<input type="checkbox" name="trasporto_gratuito_forzato" '.(isset($_POST['check_trasporto_gratuito']) ? (Tools::getIsset('trasporto_gratuito_forzato') ? "checked='checked'" : '') : ($uu == 1 ? 'checked="checked"' : '')).' id="trasporto_gratuito_forzato" />'; 
						echo '

					
						</td>
						
					</tr>
					
					
					<tr>
					
					';
					


					
					
						echo '<td class="col-left">'.$this->l('Displayed text when in-stock:').'</td>
						<td style="padding-bottom:5px;" class="translatable">';
		foreach ($this->_languages as $language) {
		
		$messdisponib = stripslashes(htmlentities($this->getFieldValue($obj, 'available_now', $language['id_lang']), ENT_COMPAT, 'UTF-8'));
		
		
			if (isset($messdisponib)) {
$msg_disp = stripslashes(htmlentities($this->getFieldValue($obj, 'available_now', $language['id_lang']), ENT_COMPAT, 'UTF-8'));
}
else {
$msg_disp = "Disponibile";
}					


		$messnondisponib = stripslashes(htmlentities($this->getFieldValue($obj, 'available_later', $language['id_lang']), ENT_COMPAT, 'UTF-8'));
		
		
			if (isset($messnondisponib)) {
$msg_n_disp = stripslashes(htmlentities($this->getFieldValue($obj, 'available_later', $language['id_lang']), ENT_COMPAT, 'UTF-8'));
}
else {
$msg_n_disp = "Ordinabile, consegna entro 5 giorni lavorativi";
}		



	
			echo '		<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
								<input size="30" type="text" id="available_now_'.$language['id_lang'].'" name="available_now_'.$language['id_lang'].'"
								value="'.$msg_disp.'" />
								<span class="hint" name="help_box">'.$this->l('Forbidden characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
							</div>';
							}
		echo '			</td>
					</tr>
					<tr>
						<td class="col-left">'.$this->l('Displayed text when allowed to be back-ordered:').'</td>
						<td style="padding-bottom:5px;" class="translatable">';
		foreach ($this->_languages as $language)
			echo '		<div  class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
								<input size="30" type="text" id="available_later_'.$language['id_lang'].'" name="available_later_'.$language['id_lang'].'"
								value="'.stripslashes(htmlentities($this->getFieldValue($obj, 'available_later', $language['id_lang']), ENT_COMPAT, 'UTF-8')).'" />
								<span class="hint" name="help_box">'.$this->l('Forbidden characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
							</div>';
			echo '	</td>
					</tr>

					<script type="text/javascript">
						calcPriceTI();
					</script>

					<tr>
	<td style="vertical-align:top;text-align:left;padding-right:10px;font-weight:bold;">Messaggio disponibilit&agrave;</td>
	<td>
		<textarea name="date_available_msg" style="width: 230px; margin-right: 5px;">'.htmlentities($this->getFieldValue($obj, 'date_available_msg'), ENT_COMPAT, 'UTF-8').'</textarea>
	</td>
</tr>

					<tr>
						<td class="col-left">'.$this->l('When out of stock:').'</td>
						<td style="padding-bottom:5px;">
							<input type="radio" name="out_of_stock" id="out_of_stock_1" value="0" '.((int)($this->getFieldValue($obj, 'out_of_stock')) == 0 ? 'checked="checked"' : '').'/> <label for="out_of_stock_1" class="t" id="label_out_of_stock_1">'.$this->l('Deny orders').'</label>
							<br /><input type="radio" name="out_of_stock" id="out_of_stock_2" value="1" '.($this->getFieldValue($obj, 'out_of_stock') == 1 ? 'checked="checked"' : '').'/> <label for="out_of_stock_2" class="t" id="label_out_of_stock_2">'.$this->l('Allow orders').'</label>
							<br /><input type="radio" name="out_of_stock" id="out_of_stock_3" value="2" '.($this->getFieldValue($obj, 'out_of_stock') == 2 ? 'checked="checked"' : '').'/> <label for="out_of_stock_3" class="t" id="label_out_of_stock_3">'.$this->l('Default:').' <i>'.$this->l(((int)(Configuration::get('PS_ORDER_OUT_OF_STOCK')) ? 'Allow orders' : 'Deny orders')).'</i> ('.$this->l('as set in').' <a href="index.php?tab=AdminPPreferences&token='.Tools::getAdminToken('AdminPPreferences'.(int)(Tab::getIdFromClassName('AdminPPreferences')).(int)($cookie->id_employee)).'"  onclick="return confirm(\''.$this->l('Are you sure you want to delete entered product information?', __CLASS__, true, false).'\');">'.$this->l('Preferences').'</a>)</label>
						</td>
					</tr>

					</table>
					<table>
					<tr>
						<td colspan="5" style="padding-bottom:5px;">
							<hr style="width:100%;" />
						</td>
					</tr>
					<tr>
						<td style=""><label for="id_category_default" class="t">'.$this->l('Default category:').'</label></td>
						<td style="">
						<div id="no_default_category" style="color: red;font-weight: bold;display: none;">'.$this->l('Please check a category in order to select the default category.').'</div>';
						
						if(Tools::getIsset('id_bundle'))
						{
						}
						
						else {
						
							echo '
							<script type="text/javascript">
								var post_selected_cat;
							</script>';
							$default_category = Tools::getValue('id_category', 1);
							
							
							if (!$obj->id)
							{
								$selectedCat = Category::getCategoryInformations(Tools::getValue('categoryBox', array($default_category)), $this->_defaultFormLanguage);
								echo '
								<script type="text/javascript">
									post_selected_cat = \''.implode(',', array_keys($selectedCat)).'\';
								</script>';
							}
							else
							{
								if (Tools::isSubmit('categoryBox'))
									$selectedCat = Category::getCategoryInformations(Tools::getValue('categoryBox', array($default_category)), $this->_defaultFormLanguage);
								else
									$selectedCat = Product::getProductCategoriesFull($obj->id, $this->_defaultFormLanguage);
							}
							
							$id_category_gst = Db::getInstance()->getValue('SELECT id_category_gst FROM product WHERE id_product = '.$obj->id.'');
							
							$categories_gst = Db::getInstance()->executeS('SELECT * FROM category_lang WHERE id_lang = 5 ORDER BY name ASC');

							echo '<select id="id_category_default" name="id_category_default">';
							foreach($selectedCat AS $cat)
								echo '<option value="'.$cat['id_category'].'" '.(Tools::getIsset('id_category_default') && Tools::getValue('id_category_default') == $cat['id_category'] ? 'selected' : ($obj->id_category_default == $cat['id_category'] ? 'selected' : '')).'>'.$cat['name'].'</option>';
							echo '</select>
							</td><td style="width:230px"></td>
							<td><label for="id_category_gst" class="t">'.$this->l('Categoria gestionale:').'</label></td>
							<td>
							<div id="no_default_category" style="color: red;font-weight: bold;display: none;">'.$this->l('Please check a category in order to select the default category.').'</div>
							
							<script type="text/javascript">
								var post_selected_cat;
							</script>
								<select id="id_category_gst" name="id_category_gst">';
									echo '<option value="0" '.(Tools::getIsset('id_category_gst') && Tools::getValue('id_category_gst') == $cat['id_category'] ? 'selected' : ($id_category_gst == $cat['id_category'] ? 'selected' : '')).'>-- Seleziona una categoria --</option>';
									
								foreach($categories_gst AS $cat)
									echo '<option value="'.$cat['id_category'].'" '.(Tools::getIsset('id_category_gst') && Tools::getValue('id_category_gst') == $cat['id_category'] ? 'selected' : ($id_category_gst == $cat['id_category'] ? 'selected' : '')).'>'.$cat['name'].'</option>';
								echo '</select> 
							</td>
						</tr>
						<tr id="tr_categories">
							<td colspan="2" style="vertical-align:top">
							';
						// Translations are not automatic for the moment ;)
						$trads = array(
							 'Home' => $this->l('Home'),
							 'selected' => $this->l('selected'),
							 'Collapse All' => $this->l('Collapse All'),
							 'Expand All' => $this->l('Expand All'),
							 'Check All' => $this->l('Check All'),
							 'Uncheck All'  => $this->l('Uncheck All')
						);
						echo Helper::renderAdminCategorieTree($trads, $selectedCat, 'categoryBox').'';
						
						
						echo '</td><td></td>
						<td colspan="2" style="vertical-align:top">
							';
							
							// Translations are not automatic for the moment ;)
						$trads2 = array(
							 'Home' => $this->l('Home'),
							 'selected' => $this->l('selected'),
							 'Collapse All' => $this->l('Collapse All'),
							 'Expand All' => $this->l('Expand All'),
							 'Check All' => $this->l('Check All'),
							 'Uncheck All'  => $this->l('Uncheck All')
						);
						
						//	echo $this->renderAdminGSTCategorieTree($trads2, $selectedCat, 'categoryGSTBox').'';
					}	
						
						echo '</td>
					</tr>
					<tr><td colspan="4" style="padding-bottom:5px;"><hr style="width:100%;" /></td></tr>
					</table>
					<table>
					<tr><td colspan="2">
						<span onclick="$(\'#seo\').slideToggle();" style="cursor: pointer"><img src="../img/admin/arrow.gif" alt="'.$this->l('SEO').'" title="'.$this->l('SEO').'" style="float:left; margin-right:5px;"/>'.$this->l('Click here to improve product\'s rank in search engines (SEO)').'</span><br />
						<div id="seo" style="padding-top: 15px;">
						<script type="text/javascript">
function maxlengthinputf(area,max,id_campo){
    var conta = area.value.length;
	
    if(id_campo!=null){
	
        document.getElementById(id_campo).innerHTML=conta;
		
    }
    if(conta < 0){

        area.value = area.value.substring(0,max);
        if(id_campo!=null){
            document.getElementById(id_campo).innerHTML = \'0\';
        }
    }
}
</script>

							<table>
								<tr>
									<td class="col-left">'.$this->l('Meta title:').'</td>
									<td class="translatable">';
		foreach ($this->_languages AS $language)
			echo '					<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
	
											<input onkeyup="attivaSEO(document.getElementById(\'description_5\').value); maxlengthinputf(this,80,\'conta_title_'.$language['id_lang'].'\')" size="110" type="text" id="meta_title_'.$language['id_lang'].'" name="meta_title_'.$language['id_lang'].'"
											value="'.htmlentities($this->getFieldValue($obj, 'meta_title', $language['id_lang']), ENT_COMPAT, 'UTF-8').'" />
											<span id="conta_title_'.$language['id_lang'].'">'.strlen($this->getFieldValue($obj, 'meta_title', $language['id_lang'])).'</span> (Ottimale: tra i 50 e i 70)
											<span class="hint" name="help_box">'.$this->l('Forbidden characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
										</div>';
		echo '						<p class="clear">'.$this->l('Product page title; leave blank to use product name').'</p>
									</td>
								</tr>
								<tr>
									<td class="col-left">'.$this->l('Meta description:').'</td>
									<td class="translatable">';
		foreach ($this->_languages as $language)
			echo '					<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
											<input onkeyup="attivaSEO(document.getElementById(\'description_5\').value); maxlengthinputf(this,350,\'conta_meta_desc_'.$language['id_lang'].'\')" size="110" type="text" id="meta_description_'.$language['id_lang'].'" name="meta_description_'.$language['id_lang'].'"
											value="'.htmlentities($this->getFieldValue($obj, 'meta_description', $language['id_lang']), ENT_COMPAT, 'UTF-8').'" />
											<span id="conta_meta_desc_'.$language['id_lang'].'">'.strlen($this->getFieldValue($obj, 'meta_description', $language['id_lang'])).'</span> (Ottimale: tra i 110 e i 300)
											<span class="hint" name="help_box">'.$this->l('Forbidden characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
										</div>';
		echo '						<p class="clear">'.$this->l('A single sentence for HTML header').'</p>
									</td>
								</tr>
								<tr>
									<td class="col-left">'.$this->l('Meta keywords:').'</td>
									<td class="translatable">';
		foreach ($this->_languages as $language)
			echo '					<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
											<input onkeyup="maxlengthinputf(this,250,\'conta_keywords_'.$language['id_lang'].'\')" size="110" type="text" id="meta_keywords_'.$language['id_lang'].'" name="meta_keywords_'.$language['id_lang'].'"
											value="'.htmlentities($this->getFieldValue($obj, 'meta_keywords', $language['id_lang']), ENT_COMPAT, 'UTF-8').'" />
											<span id="conta_keywords_'.$language['id_lang'].'">'.strlen($this->getFieldValue($obj, 'meta_keywords', $language['id_lang'])).'</span> (Ottimale: 5-10 parole, 200 c.)
											<span class="hint" name="help_box">'.$this->l('Forbidden characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
										</div>';
		echo '						<p class="clear">'.$this->l('Keywords for HTML header, separated by a comma').'</p>
									</td>
								</tr>';
								
					echo '
					 <script type="text/javascript" src="../js/select2.js"></script>
					 <script type="text/javascript">
$(document).ready(function() { $("#canonical").select2(); });
</script>
					<tr><td>Canonical-redir-EOL: <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Se il prodotto e\' online, il canonical mantiene il suo uso normale. Quando il prodotto viene messo offline, il canonical fa da redirect (la pagina del prodotto offline viene reindirizzata su quella del prodotto indicato nel canonical)" title="Se il prodotto e\' online, il canonical mantiene il suo uso normale. Quando il prodotto viene messo offline, il canonical fa da redirect (la pagina del prodotto offline viene reindirizzata su quella del prodotto indicato nel canonical)" /> <br /><br /></td><td><select id="canonical" name="canonical" style="width:600px">';
					if(!isset($obj->canonical) || $obj->canonical == 0) {
			echo "<option name='0' value='0' selected='selected'>--- Scegli un prodotto ---</option>"; 
			
			} else { 
			
			echo "<option name='0' value='0'>--- Scegli un prodotto ---</option>";
			}
			
		$query_corr = "SELECT product.id_product, product.reference, product_lang.name FROM product JOIN product_lang ON product.id_product = product_lang.id_product WHERE product_lang.id_lang = 5 AND product.active = 1";
			$res_corr = Db::getInstance()->executeS($query_corr);
			
			foreach($res_corr as $rowcorr)
				echo "<option name='$rowcorr[id_product]' value='$rowcorr[id_product]'".(Tools::getIsset('canonical') && Tools::getValue('canonical') == $rowcorr['id_product'] ? "selected='selected'" : ($obj->canonical != 0 && $obj->canonical == $rowcorr['id_product'] ? " selected='selected' " : '')). ">$rowcorr[id_product] - $rowcorr[name] ($rowcorr[reference])</option>";
									
								
		echo '</select><br /><br />';
	
		
		
		echo '</td></tr>';		

echo '
					<tr><td>Categoria per redirect: <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Se il prodotto viene messo offline, il redirect punta alla categoria qui selezionata" title="Se il prodotto viene messo offline, il redirect punta alla categoria qui selezionata" />
					<br /><br /></td><td><select id="redirect_category" name="redirect_category" style="width:600px">';
					if(!isset($obj->redirect_category) || $obj->redirect_category == 0) {
			echo "<option name='0' value='0' selected='selected'>--- Scegli una categoria ---</option>"; 
			
			} else { 
			
			echo "<option name='0' value='0'>--- Scegli una categoria ---</option>";
			}
			
			$res_cats = Db::getInstance()->executeS('SELECT * FROM category c JOIN category_lang cl ON c.id_category = cl.id_category WHERE cl.id_lang = 5 AND c.active = 1 ORDER BY cl.name ASC');
			foreach($res_cats as $rowcats)
				echo "<option name='$rowcats[id_category]' value='$rowcats[id_category]'".(Tools::getIsset('redirect_category') && Tools::getValue('redirect_category') == $rowcorr['id_product'] ? "selected='selected'" : ($obj->redirect_category != 0 && $obj->redirect_category == $rowcats['id_category'] ? " selected='selected' " : '')). ">$rowcats[name]</option>";
									
								
		echo '</select><br /><br />';
	
		
		
		echo '</td></tr>';						
								
							echo '<tr>
						<td class="col-left"><strong style="color:red">'.$this->l('Non indicizzare*').'</strong></td>
						<td style="padding-bottom:5px;">
							<input type="checkbox" name="noindex" id="noindex" '.($this->getFieldValue($obj, 'noindex') == 1 ? 'checked="checked"' : '').'" /> 
							<strong style="color:red">* Con questa opzione il prodotto viene messo in modalit&agrave; noindex e nofollow e non viene indicizzato su Google</strong><br /><br />
						</td>
					</tr>	';
								
			echo '<tr>
									<td class="col-left">'.$this->l('Home page category:').'</td>
									<td class="translatable">';
									
					
									
		
      // HOME PAGE INIZIO
		foreach ($this->_languages as $language)
			echo '					<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
											<input size="55" type="text" id="cat_homepage_'.$language['id_lang'].'" name="cat_homepage_'.$language['id_lang'].'"
											value="'.htmlentities($this->getFieldValue($obj, 'cat_homepage', $language['id_lang']), ENT_COMPAT, 'UTF-8').'" />
											
											
											<span class="hint" name="help_box">'.$this->l('Forbidden characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
										</div>';
		echo '						<p class="clear">'.$this->l('Home page category').'</p>
									</td>
								</tr>
								<tr>
									<td class="col-left">'.$this->l('Home page description:').'</td>
									<td class="translatable">';


		foreach ($this->_languages as $language)
			echo '					<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
									
											<input size="55" onkeyup="maxlengthinputf(this,110,\'conta_desc_homepage_'.$language['id_lang'].'\')" maxlength="110" type="text" id="desc_homepage_'.$language['id_lang'].'" name="desc_homepage_'.$language['id_lang'].'"
											value="'.htmlentities($this->getFieldValue($obj, 'desc_homepage', $language['id_lang']), ENT_COMPAT, 'UTF-8').'" />
											<span id="conta_desc_homepage_'.$language['id_lang'].'">'.strlen($this->getFieldValue($obj, 'desc_homepage', $language['id_lang'])).'</span>
											<span class="hint" name="help_box">'.$this->l('Forbidden characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
										</div>';
		echo '						<p class="clear">'.$this->l('Home page description, max 110 chars').'</p>
									</td>
								</tr>
								<tr>
									<td class="col-left">'.$this->l('Friendly url:').'</td>
									<td class="translatable">';
				
			// HOME PAGE FINE	
			
		foreach ($this->_languages as $language)
		{
			echo '					<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
											<input size="55" type="text" id="link_rewrite_'.$language['id_lang'].'" name="link_rewrite_'.$language['id_lang'].'" pattern="^((?!(-p-)|(-m-)|(-c-)|(-pr-)|(-pi-)).)*$"
											value="'.htmlentities($this->getFieldValue($obj, 'link_rewrite', $language['id_lang']), ENT_COMPAT, 'UTF-8').'" onchange="updateFriendlyURL();" /><sup> *</sup>
											<span class="hint" name="help_box">'.$this->l('Only letters and the "less" character are allowed').'<span class="hint-pointer">&nbsp;</span></span>
										</div>';
		}
		echo '						<p class="clear" style="padding:10px 0 0 0">'.'<a style="cursor:pointer" class="button" onmousedown="updateFriendlyURLByName();">'.$this->l('Generate').'</a>&nbsp;'.$this->l('Friendly-url from product\'s name.').'<br /><br />';
		echo '						'.$this->l('Product link will look like this:').' '.(Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://').$_SERVER['SERVER_NAME'].'/<b>id_product</b>-<span id="friendly-url"></span>.html</p>
									</td>
								</tr>';
								
								
								
								
								
		// URL SE IL PRODOTTO E' FUORI PRODUZIONE
		/*				
		echo '						
								<tr>
									<td class="col-left">'.$this->l('URL se fuori produzione:').'</td>
									<td class="translatable">';	
			
		foreach ($this->_languages as $language)
		{
			echo '					<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
											<input size="55" type="text" id="link_rewrite_fp_'.$language['id_lang'].'" name="link_rewrite_fp_'.$language['id_lang'].'" pattern="^((?!(-p-)|(-m-)|(-c-)|(-pr-)|(-pi-)).)*$"
											value="'.htmlentities($this->getFieldValue($obj, 'link_rewrite_fp', $language['id_lang']), ENT_COMPAT, 'UTF-8').'" onchange="updatefpURL();" /><sup> *</sup>
											<span class="hint" name="help_box">'.$this->l('Only letters and the "less" character are allowed').'<span class="hint-pointer">&nbsp;</span></span>
										</div>';
		}
		echo '
									</td>
								</tr>';
		*/						
		// FINE URL FUORI PRODUZIONE						
								
							
							
							
		
		echo '</td></tr></table>
						</div>
					</td></tr>
					<tr><td colspan="2" style="padding-bottom:5px;"><hr style="width:100%;" /></td></tr>
					<tr>
						<td class="col-left">'.$this->l('Short description:').'<br /><br /><i>('.$this->l('appears in the product lists and on the top of the product page').')</i></td>
						<td style="padding-bottom:5px;" class="translatable">';
		foreach ($this->_languages as $language)
			echo '		<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').';float: left;">
								<textarea onkeypress="changes=true;" cols="100" rows="10" id="description_short_'.$language['id_lang'].'" name="description_short_'.$language['id_lang'].'">'.htmlentities(stripslashes($this->getFieldValue($obj, 'description_short', $language['id_lang'])), ENT_COMPAT, 'UTF-8').'</textarea>
							</div>';
		echo '		</td>
					</tr>
					<tr><td colspan="2" style="padding-bottom:5px;"><hr style="width:100%;" /></td></tr>
					<tr>
						<td class="col-left">'.$this->l('Descrizione Amazon').'<br /><br /></td>
						<td style="padding-bottom:5px;" class="translatable">';
		foreach ($this->_languages as $language)
			echo '		<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').';float: left;">
								<textarea onkeypress="changes=true;" style="width:755px" cols="100" rows="10" class="editable rte" id="description_amazon_'.$language['id_lang'].'" name="description_amazon_'.$language['id_lang'].'">'.(htmlentities(stripslashes($this->getFieldValue($obj, 'description_amazon', $language['id_lang'])), ENT_COMPAT, 'UTF-8') == '' ? (stripslashes(($this->getFieldValue($obj, 'description_short', $language['id_lang'])))) : (stripslashes(($this->getFieldValue($obj, 'description_amazon', $language['id_lang']))))).'</textarea>
							</div>';
							
							
							
		echo '		</div></td>
					</tr>
					
					<tr>
						<td class="col-left">'.$this->l('Motivo mancata pubblicazione su Amazon').'<br /><br /></td>
						<td style="padding-bottom:5px;" ><input type="text" style="width:90%" id="no_amazon" name="no_amazon" value="'.htmlentities(Db::getInstance()->getValue('SELECT no_amazon FROM product WHERE id_product = '.$obj->id.''), ENT_COMPAT, 'UTF-8').'" />
							';
							
							
							
		echo '		</td>
					</tr>
					
					<tr><td colspan="2">
					
					
					
					<p style="width:900px; background-color:#000000; cursor:pointer"  onclick="$(\'#amazon\').slideToggle();" ><span style="cursor: pointer; color:#ffffff; background-color:#000000; width:100%"><img src="../img/admin/arrow.gif" alt="'.$this->l('Amazon').'" title="'.$this->l('Amazon').'" style="float:left; margin-right:5px;"/>'.$this->l('Clicca qui per aprire punti di forza Amazon').'</span></p><br />
					
					
					<div id="amazon">
					SKU Logistica Amazon: <input type="text" name="sku_amazon" readonly="readonly" value="'.Db::getInstance()->getValue('SELECT sku_amazon FROM product WHERE id_product = '.$obj->id).'" />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					FNSKU: <input type="text" name="fnsku" readonly="readonly" value="'.Db::getInstance()->getValue('SELECT fnsku FROM product WHERE id_product = '.$obj->id).'" /><br /><br />
					<table>
					
					
					
					<tr><td class="col-left">'.$this->l('Punti di forza').'<br /><br /</td>
							<td style="padding-bottom:5px;" class="translatable">';
					foreach ($this->_languages as $language) {
						$punti_di_forza = Db::getInstance()->getValue('SELECT punti_di_forza FROM product_lang WHERE id_lang = '.$language['id_lang'].' AND id_product = '.$obj->id);
							$punti_di_forza = explode(':::::', $punti_di_forza);
						echo '
						
						<div class="lang_'.$language['id_lang'].'"  style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').';float: left;">
							
							1: <input type="text" size="144" value="'.$punti_di_forza[0].'" name="punti_di_forza_0_'.$language['id_lang'].'" /><br /><br />
							2: <input type="text" size="144" value="'.$punti_di_forza[1].'" name="punti_di_forza_1_'.$language['id_lang'].'" /><br /><br />
							3: <input type="text" size="144" value="'.$punti_di_forza[2].'" name="punti_di_forza_2_'.$language['id_lang'].'" /><br /><br />
							4: <input type="text" size="144" value="'.$punti_di_forza[3].'" name="punti_di_forza_3_'.$language['id_lang'].'" /><br /><br />
							5: <input type="text" size="144" value="'.$punti_di_forza[4].'" name="punti_di_forza_4_'.$language['id_lang'].'" /><br />
							
						
					</div>';
				}	
					echo '
					</td></tr>
					</table>
					</div></td></tr>
					<tr><td colspan="2" style="padding-bottom:5px;"><hr style="width:100%;" /></td></tr>
					
					<tr>
						<td class="col-left">'.$this->l('Description:').'<br /><br /><i>('.$this->l('appears in the body of the product page').')</i></td>
						<td style="padding-bottom:5px;" class="translatable">';
		foreach ($this->_languages as $language)
			echo '		<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').';float: left;">
								<textarea onkeypress="changes=true; attivaSEO(document.getElementById(\'description_5\').value);" class="editable rte" style="width:755px; height:500px" id="description_'.$language['id_lang'].'" name="description_'.$language['id_lang'].'">'.htmlentities(stripslashes($this->getFieldValue($obj, 'description', $language['id_lang'])), ENT_COMPAT, 'UTF-8').'</textarea>
							</div>';
		echo '	</td>
		
					</tr>';
					
					$iso = Language::getIsoById((int)($cookie->id_lang));
					$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
					$ad = dirname($_SERVER["PHP_SELF"]);
						echo '
					<script type="text/javascript">
					var iso = \''.$isoTinyMCE.'\' ;
					var ad = \''.$ad.'\' ;
					</script>
					
					
					<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce3/tiny_mce.js"></script>
					<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>';
					echo '<tr><td>SEO (solo per italiano)
					<br />
					Parola chiave:
					<input id="parola_chiave" name="parola_chiave" type="text" onkeyup="attivaSEO(document.getElementById(\'description_5\').value)" value="'.Db::getInstance()->getValue('SELECT seo_keyword FROM product_lang WHERE id_lang = 5 AND id_product = '.Tools::getValue('id_product')).'" /><br /><br />
				
					</td>';
					
					echo '<td>';
					echo "
					<script type='text/javascript'>
					$(document).ready(function(){
						$('#toggleseo').click(function() {
							$('#seoby').slideToggle('fast', function() {
								if ($('#seoby').is(':hidden')) {
									var hidden_seo = 1;
								} else {
									var hidden_seo = 0;
								}
								
								$.ajax({
									type: 'POST',
									async: true,
									url: 'ajax.php?nascondiMostraSeo',
									data: {hidden_seo: hidden_seo, nascondiMostraSeo: 'y'},
									success: function(data) {
									},
									error: function(xhr,stato,errori){
									}
								});
							});
						});
					});
					</script>";
					
					$hidden_seo = (Db::getInstance()->getValue('SELECT value FROM fixed_ck WHERE id_employee = '.$cookie->id_employee.' AND name="hidden_seo"'));
					
					echo '
						<a href="javascript:void(0)" id="toggleseo">Clicca per aprire/chiudere SEO</a><br /><br />
					<div id="seoby" style="'.($hidden_seo == 0 ? "display:block" : 'display:none').'">
					</div>
					<script type="text/javascript">
						
						function attivaSEO(testo) 
						{
							var keyword = document.getElementById("parola_chiave").value;
							var title = document.getElementById("meta_title_5").value;
							var meta_description = document.getElementById("meta_description_5").value;
							var url = document.getElementById("link_rewrite_5").value;
							var urlified_keyword = str2url(document.getElementById("parola_chiave").value, "UTF-8");
							
							$.ajax({
							  url:"ajax.php?SEOby=y",
							  type: "POST",
							  data: { text: testo,
							  keyword: keyword,
							  title: title,
							  meta_description: meta_description,
							  url: url,
							  urlified_keyword: urlified_keyword,
							  tipo: "product"
							  },
							  success:function(r){
									console.log("SEO SUCCESS");
									$("#seoby").html(r);
							  },
							  error: function(xhr,stato,errori){
								 console.log("Errore nella cancellazione:"+xhr.status);
							  }
							});
						}
						
						var temp = document.getElementById("description_5").value;
						
						attivaSEO(temp);
						
					 </script>
					 
					 </td></tr>';
				
				
				
				
					
			echo '<tr><td class="col-left">Video:</td>
<td>';			

$rowvideos = Db::getInstance()->getRow("SELECT * FROM product_video WHERE id_product = $_GET[id_product]");

$videoc = str_replace('embed/', 'watch?v=', $rowvideos['content']);
echo "
<input type='checkbox' name='cancellavideo[]' value='$rowvideos[id]' /> Spunta per cancellare - <a href='$videoc' target='_blank'>".$videoc."</a><br /><br />";


					
					
					
					echo 'Video 1: <input type="text" name="video1" size="100" value="'.(Tools::getIsset('video1') ? Tools::getValue('video1') : '').'" /><br /><br />
					Video 2: <input type="text" name="video2" size="100" value="'.(Tools::getIsset('video2') ? Tools::getValue('video2') : '').'" /><br /><br />
				
					
</td>				
					
					
					
					
					
					
					</tr>';
					
					
				echo '<tr><td class="col-left">Comparaprezzi:</td>
<td>';			
$querycompara = "SELECT trovaprezzi, google_shopping, amazon, eprice, comparaprezzi_check FROM product WHERE id_product = $_GET[id_product]";
$rescompara = mysql_query($querycompara);
$numcompara = Db::getInstance()->getValue("SELECT count(amazon) FROM product WHERE id_product = $_GET[id_product]");


echo "
<script type='text/javascript'>
function checkUncheckSome(controller,theElements) {
	
     var formElements = theElements.split(',');
	 var theController = document.getElementById(controller);
	 for(var z=0; z<formElements.length;z++){
	  theItem = document.getElementById(formElements[z]);
	  if(theItem.type && theItem.type=='checkbox'){

	    	theItem.checked=theController.checked;

	  } else {
	  	  theInputs = theItem.getElementsByTagName('input');
	  for(var y=0; y<theInputs.length; y++){
	  if(theInputs[y].type == 'checkbox' && theInputs[y].id != theController.id){
	     theInputs[y].checked = theController.checked;
	    }
	  }
	  }
    }
}
</script>";
if($numcompara == 0) {


echo "
<fieldset id='comparaprezzifields'> 
<input type='hidden' name='tutti_i_comparaprezzi' />
<input type='checkbox' name='trovaprezzi_add' checked='checked' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('trovaprezzi_add') ? "checked='checked'" : '') : '')." /> Trovaprezzi<br />
<input type='checkbox' name='google_shopping_add' checked='checked'  ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('google_shopping_add') ? "checked='checked'" : '') : '')." /> Google Shopping<br />
<input type='checkbox' name='amazon_it' checked='checked'  ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('amazon_it') ? "checked='checked'" : '') : '')." />Amazon.it
<input type='checkbox' name='amazon_fr' checked='checked' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('amazon_fr') ? "checked='checked'" : '') : '')." />Amazon.fr
<input type='checkbox' name='amazon_es' checked='checked' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('amazon_es') ? "checked='checked'" : '') : '')." />Amazon.es
<input type='checkbox' name='amazon_uk' checked='checked' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('amazon_uk') ? "checked='checked'" : '') : '')." />Amazon.co.uk
<input type='checkbox' name='amazon_de' checked='checked' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('amazon_de') ? "checked='checked'" : '') : '')." />Amazon.de
<input type='checkbox' name='amazon_nl' checked='checked' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('amazon_nl') ? "checked='checked'" : '') : '')." />Amazon.nl
<input type='checkbox' name='amazon_se' checked='checked' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('amazon_se') ? "checked='checked'" : '') : '')." />Amazon.se
<input type='checkbox' name='amazon_pl' checked='checked' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('amazon_pl') ? "checked='checked'" : '') : '')." />Amazon.pl
<br />
<input type='checkbox' name='eprice_add' checked='checked' /> ePrice<br /><br />


<input type='checkbox' onclick=\"checkUncheckSome('tutti_add','comparaprezzifields')\" id='tutti_add' name='tutti_add' /> Tutti<br /></fieldset>




<br /><br />

";

} else {
$querycompara = "SELECT trovaprezzi, google_shopping, amazon, eprice, comparaprezzi_check FROM product WHERE id_product = $_GET[id_product]";
$rowscompara = Db::getInstance()->executeS($querycompara);
foreach($rowscompara as $rowcompara) {
echo "
<fieldset id='comparaprezzifields'> 
<input type='hidden' name='tutti_i_comparaprezzi' />

<input type='checkbox' name='trovaprezzi_add' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('trovaprezzi_add') ? "checked='checked'" : '') : (($rowcompara['trovaprezzi'] == 1) ? "checked='checked'" : ''))." /> Trovaprezzi<br />

<input type='checkbox' name='google_shopping_add' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('google_shopping_add') ? "checked='checked'" : '') : (($rowcompara['google_shopping'] == 1) ? "checked='checked'" : ''))." /> Google Shopping<br />";

$amazon_check = explode(";",$rowcompara['amazon']);

	$actives = Db::getInstance()->getRow('SELECT * FROM amazon_products WHERE id_product = '.Tools::getValue('id_product'));
	
	echo "<input type='checkbox' name='amazon_it' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('amazon_it') ? "checked='checked'" : '') : (in_array("5",$amazon_check) ? "checked='checked'" : ''))." /> ".($actives['it'] > 0 ? '<strong style="color:#008000">Amazon.it</strong>' : 'Amazon.it')." &nbsp;&nbsp;&nbsp;";

	echo "<input type='checkbox' name='amazon_fr' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('amazon_fr') ? "checked='checked'" : '') : (in_array("2",$amazon_check) ? "checked='checked'" : ''))." />".($actives['fr'] > 0 ? '<strong style="color:#008000">Amazon.fr</strong>' : 'Amazon.fr')." &nbsp;&nbsp;&nbsp; ";

	echo "<input type='checkbox' name='amazon_es' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('amazon_es') ? "checked='checked'" : '') : (in_array("3",$amazon_check) ? "checked='checked'" : ''))." />".($actives['es'] > 0 ? '<strong style="color:#008000">Amazon.es</strong>' : 'Amazon.es')." &nbsp;&nbsp;&nbsp; ";

	echo "<input type='checkbox' name='amazon_uk' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('amazon_uk') ? "checked='checked'" : '') : (in_array("1",$amazon_check) ? "checked='checked'" : ''))." />".($actives['uk'] > 0 ? '<strong style="color:#008000">Amazon.co.uk</strong>' : 'Amazon.co.uk')." &nbsp;&nbsp;&nbsp;";

	echo "<input type='checkbox' name='amazon_de' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('amazon_de') ? "checked='checked'" : '') : (in_array("4",$amazon_check) ? "checked='checked'" : ''))." />".($actives['de'] > 0 ? '<strong style="color:#008000">Amazon.de</strong>' : 'Amazon.de')." &nbsp;&nbsp;&nbsp;";
	
	echo "<input type='checkbox' name='amazon_nl' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('amazon_nl') ? "checked='checked'" : '') : (in_array("6",$amazon_check) ? "checked='checked'" : ''))." />".($actives['nl'] > 0 ? '<strong style="color:#008000">Amazon.nl</strong>' : 'Amazon.nl')." &nbsp;&nbsp;&nbsp;";
	
	echo "<input type='checkbox' name='amazon_se' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('amazon_se') ? "checked='checked'" : '') : (in_array("7",$amazon_check) ? "checked='checked'" : ''))." />".($actives['se'] > 0 ? '<strong style="color:#008000">Amazon.se</strong>' : 'Amazon.se')." &nbsp;&nbsp;&nbsp; ";
	
	echo "<input type='checkbox' name='amazon_pl' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('amazon_pl') ? "checked='checked'" : '') : (in_array("8",$amazon_check) ? "checked='checked'" : ''))." />".($actives['pl'] > 0 ? '<strong style="color:#008000">Amazon.pl</strong>' : 'Amazon.pl')." &nbsp;&nbsp;&nbsp; ";

echo "<br />
<input type='checkbox' name='eprice_add' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('eprice_add') ? "checked='checked'" : '') : (($rowcompara['eprice'] == 1) ? "checked='checked'" : ''))." /> ePrice<br /><br />

<input type='checkbox' onclick=\"checkUncheckSome('tutti_add','comparaprezzifields')\" id='tutti_add' name='tutti_add' /> Tutti<br /></fieldset>




<br /><br />

";

}

}
					
					echo '
</td>				
					
					
					
					
					
					
					</tr>';

					
					
				echo '
					<tr>
						<td class="col-left">'.$this->l('Tags:').'</td>
						<td style="padding-bottom:5px;" class="translatable">';
				if ($obj->id)
					$obj->tags = Tag::getProductTags((int)$obj->id);
				foreach ($this->_languages as $language)
				{
					echo '<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
							<input size="55" type="text" id="tags_'.$language['id_lang'].'" name="tags_'.$language['id_lang'].'"
							value="'.htmlentities(Tools::getValue('tags_'.$language['id_lang'], $obj->getTags($language['id_lang'], true)), ENT_COMPAT, 'UTF-8').'" />
							<span class="hint" name="help_box">'.$this->l('Forbidden characters:').' !<>;?=+#"&deg;{}_$%<span class="hint-pointer">&nbsp;</span></span>
						  </div>';
				}
				echo '	<p class="clear">'.$this->l('Tags separated by commas (e.g., dvd, dvd player, hifi)').'</p>
						</td>
					</tr>';
				$accessories = Product::getAccessoriesLightAll((int)($cookie->id_lang), $obj->id);
				$alternatives = Product::getAlternativesLightAll((int)($cookie->id_lang), $obj->id);

				if ($postAccessories = Tools::getValue('inputAccessories'))
				{
					$postAccessoriesTab = explode('-', Tools::getValue('inputAccessories'));
					foreach ($postAccessoriesTab AS $accessoryId)
						if (!$this->haveThisAccessory($accessoryId, $accessories) AND $accessory = Product::getAccessoryById($accessoryId))
							$accessories[] = $accessory;
				}
				
				if ($postAlternatives = Tools::getValue('inputAlternatives'))
				{
					$postAlternativesTab = explode('-', Tools::getValue('inputAlternatives'));
					foreach ($postAlternativesTab AS $alternativeId)
						if (!$this->haveThisAlternative($alternativeId, $alternatives) AND $alternative = Product::getAlternativeById($alternativeId))
							$alternatives[] = $alternative;
				}
				

					echo '
					<tr>
						<td class="col-left">'.$this->l('Accessories:').'<br /><br /><i>'.$this->l('(Do not forget to Save the product afterward)').'</i></td>
						<td style="padding-bottom:5px;">
							<div id="divAccessories">';
					foreach ($accessories as $accessory)
					{
						if($accessory['name'] == '')
						{
							$accessory = Db::getInstance()->getRow('SELECT `id_bundle` id_product, bundle_ref reference, `bundle_name` name FROM `bundle` WHERE `id_bundle` = '.$accessory['id_product']);
						}
						
						echo htmlentities($accessory['name'], ENT_COMPAT, 'UTF-8').(!empty($accessory['reference']) ? ' ('.$accessory['reference'].')' : '').' <span onclick="delAccessory('.$accessory['id_product'].');" style="cursor: pointer;"><img src="../img/admin/delete.gif" class="middle" alt="" /></span><br />';
					}	
					echo '</div>
							<input type="hidden" name="inputAccessories" id="inputAccessories" value="';
					foreach ($accessories as $accessory)
						echo (int)$accessory['id_product'].'-';
					echo '" />
							<input type="hidden" name="nameAccessories" id="nameAccessories" value="';
					foreach ($accessories as $accessory)
						echo htmlentities($accessory['name'], ENT_COMPAT, 'UTF-8').'¤';

					echo '" />
							<script type="text/javascript">
								var formProduct = "";
								var accessories = new Array();
							</script>
						<script type="text/javascript">
						
					
					function clearAutoComplete()
					{
						$("#veditutti").trigger("click");
						
					}	
					
			
			/*
				//setup before functions
				var typingTimer;                //timer identifier
				var doneTypingInterval = 3000;  //time in ms, 3 second for example
				var $ainput = $("#product_autocomplete_input");

				//on keyup, start the countdown
				$ainput.on("keyup", function () {
				  clearTimeout(typingTimer);
				  typingTimer = setTimeout(doneTyping, doneTypingInterval);
				});

				//on keydown, clear the countdown 
				$ainput.on("keydown", function () {
				  clearTimeout(typingTimer);
				});

				//user is "finished typing," do something
				function doneTyping () {
					lastKeyPressCode = event.keyCode;
					if(lastKeyPressCode != 37 && lastKeyPressCode != 38 && lastKeyPressCode != 39 && lastKeyPressCode != 40)
						$ainput.trigger("click");
				}
			*/
			
					var formProduct = "";
					var products = new Array();
				</script>
							<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
							<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete2.js"></script>
							<div id="ajax_choose_product" style="padding:6px; padding-top:2px; width:800px;">
								<p class="clear">'.$this->l('Begin typing the first letters of the product name, then select the product from the drop-down list:').'</p>
								<input type="checkbox" checked="checked" id="prodotti_online" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Online &nbsp;&nbsp;
			<input type="checkbox" id="prodotti_offline" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Offline &nbsp;
<input type="checkbox" id="prodotti_old" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Old &nbsp;&nbsp;
			<input type="checkbox" id="prodotti_disponibili" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Solo disp. &nbsp;&nbsp;&nbsp;
			
			<script type="text/javascript" src="../js/select2.js"></script>
					<script type="text/javascript">
					$(document).ready(function() { $("#auto_marca").select2(); $("#auto_serie").select2(); $("#auto_fornitore").select2();  $("#auto_categoria").select2(); });
					</script>
					
			<select id="auto_marca" name="auto_marca" onchange="repopulateSeries(this.value); clearAutoComplete();" style="width:100px">
			<option value="0">Marca...</option>
			';
			$marche = Db::getInstance()->executeS('SELECT * FROM manufacturer ORDER BY name ASC');
			foreach($marche as $marca)
				echo '<option value="'.$marca['id_manufacturer'].'">'.$marca['name'].'</option>';
			echo '
			</select>
			
					
			<select id="auto_serie" name="auto_serie" onchange="clearAutoComplete();" style="width:100px">
			<option value="0">Serie...</option>
			<option value="0">Scegli prima un costruttore</option>
			';
			echo '
			</select>
			
			<select id="auto_categoria" name="auto_categoria" onchange="clearAutoComplete();" style="width:100px">
			<option value="0">Categoria...</option>
			';
			$categorie = Db::getInstance()->executeS('SELECT * FROM category c JOIN category_lang cl ON c.id_category = cl.id_category WHERE cl.id_lang = 5 AND c.id_parent = 1 ORDER BY cl.name ASC');
			foreach($categorie as $categoria)
				echo '<option value="'.$categoria['id_category'].'">'.$categoria['name'].'</option>';
			echo '
			</select>
			
			<select id="auto_fornitore" name="auto_fornitore" onchange="clearAutoComplete(); " style="width:100px">
			<option value="0">Fornitore...</option>
			';
			
			$fornitori = Db::getInstance()->executeS('SELECT * FROM supplier WHERE id_supplier = 8 OR id_supplier = 11 OR id_supplier = 42 ORDER BY name ASC');
			foreach($fornitori as $fornitore)
				echo '<option value="'.$fornitore['id_supplier'].'">'.$fornitore['name'].'</option>';
			echo '
			</select>
			
			<input id="reset" type="button" value="Reset" class="button" onclick="document.getElementById(\'prodotti_online\').checked = true; document.getElementById(\'prodotti_offline\').checked = true; document.getElementById(\'prodotti_old\').checked = true; document.getElementById(\'prodotti_disponibili\').checked = false; document.getElementById(\'auto_categoria\').options[0].selected = true; $(\'#auto_categoria\').select2();  document.getElementById(\'auto_marca\').options[0].selected = true; $(\'#auto_marca\').select2();  
			document.getElementById(\'auto_serie\').options[0].selected = true; $(\'#auto_serie\').select2();  
			document.getElementById(\'auto_fornitore\').options[0].selected = true; $(\'#auto_fornitore\').select2(); $(\'#prodlist\').hide(); $(\'div.autocomplete_list\').hide();" />
			<br />
			<input type="checkbox" name="copia_da" id="copia_da" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }"  /> Spunta prima di cercare se vuoi copiare accessori dal prodotto selezionato, lascia bianco se vuoi caricarlo come accessorio
			<br />
			<input id="veditutti" type="button" value="Cerca" class="button" style="display:none" /> <input size="123" type="text" value="" id="product_autocomplete_input" style="margin-top:3px" /> 
								<img onclick="$(this).prev().search();" style="cursor: pointer;" src="../img/admin/add.gif" alt="'.$this->l('Add an accessory').'" title="'.$this->l('Add an accessory').'" />
							</div>
							<script type="text/javascript">
							
							function getAccessorieIds()
								{
									var ids = '. $obj->id.'+\',\';
									ids += $(\'#inputAccessories\').val().replace(/\\-/g,\',\').replace(/\\,$/,\'\');
									ids = ids.replace(/\,$/,\'\');

									return ids;
								}
								
								urlToCall = null;
								/* function autocomplete */
								
								function getOnline()
					{
						if(document.getElementById("prodotti_online").checked == true)
							return 1;
						else
							return 0;
					}
					
					function getOffline()
					{
						if(document.getElementById("prodotti_offline").checked == true)
							return 1;
						else
							return 0;
					}

function getOld()
					{
						if(document.getElementById("prodotti_old").checked == true)
							return 1;
						else
							return 0;
					}
					
					function getDisponibili()
					{
						if(document.getElementById("prodotti_disponibili").checked == true)
							return 1;
						else
							return 0;
					}
					
					function getSerie()
					{
						if(document.getElementById("auto_serie").value > 0)
							return document.getElementById("auto_serie").value;
						else
							return 0;
					}
					
					function getMarca()
					{
						if(document.getElementById("auto_marca").value > 0)
							return document.getElementById("auto_marca").value;
						else
							return 0;
					}
					
					function getFornitore()
					{
						if(document.getElementById("auto_fornitore").value > 0)
							return document.getElementById("auto_fornitore").value;
						else
							return 0;
					}
					
					function getCategoria()
					{
						if(document.getElementById("auto_categoria").value > 0)
							return document.getElementById("auto_categoria").value;
						else
							return 0;
					}
					
					function getCopiaDa()
					{
						console.log("COPIA ACCESSORI");
						if(document.getElementById("copia_da").checked == true)
							return 1;
						else
							return 0;
					}
					
					function repopulateSeries(id_manufacturer)
					{
						$("#auto_serie").empty();
						$.ajax({
						  url:"ajax.php?repopulate_series=y",
						  type: "POST",
						  data: { id_manufacturer: id_manufacturer
						  },
						  success:function(resp){  
							var newOptions = $.parseJSON(resp);
							
							 $("#auto_serie").append($("<option></option>")
								 .attr("value", "0").text("Serie..."));
							
							$.each(newOptions, function(key,value) {
							  $("#auto_serie").append($("<option></option>")
								 .attr("value", value).text(key));
							});
							
							$("#auto_serie").select2({
								placeholder: "Serie..."
							});
						  },
						  error: function(xhr,stato,errori){
						  }
						});
							
						
					}	
							
					$("body").on("click", function (event) {
						 if(event.target.tagName.toUpperCase() == "BODY"  || event.target.id == "auto_marca"   || event.target.id == "auto_serie" || event.target.id == "auto_categoria" || event.target.id == "auto_fornitore" || event.target.id == "veditutti" || event.target.id == "prodotti_online"  || event.target.id == "prodotti_offline" || event.target.id == "prodotti_old"  || event.target.id == "prodotti_disponibili" || event.target.id == "product_autocomplete_input")
						 {
							event.stopPropagation();
							
						 }
						 else
						 {
							$(\'#prodlist\').hide();
							$(\'div.autocomplete_list\').hide();
						 }
					});	
					
								$(function() {
						$(\'#product_autocomplete_input\')
							.autocomplete(\'ajax_products_list2.php?id_customer=0\', {
								minChars: 0,
								autoFill: false,
								max:5000,
								matchContains: true,
								matchSubset: 0,
								mustMatch:true,
								scroll:true,
								useCache:false,
								cacheLength:1,
								formatItem: function(item) {
							
											var color = "";
											if(item[24] == 1) {
												var color = "; color:red";
											}
											
											if(item[39] == 1)
											{
												var cornice = "; border:1px solid green";
											}
											else
											{
												var cornice = "; border:1px solid red";
											}
											if(item[28] == 0) {
												return \'</table><br /><div onclick="return false;" style="position:absolute; z-index:99997; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:180px;float:left;margin-left:10px;">Codice</div></th><th style="width:630px;"><div style="width:200px; float:left;">Desc. prodotto<br /><br /></div></th><th style="width:60px; text-align:right"><div style="width:45px;float:left;">Prezzo</div></th><th style="width:35px; text-align:right"><div style="width:30px;float:left; text-align:right">EZ</div></th><th style="width:35px; text-align:right"><div style="width:40px;float:left; text-align:right">ESP</div></th><th style="width:35px; text-align:right"><div style="width:30px;float:left; text-align:right">INT</div></th><th style="width:35px; text-align:right"><div style="width:30px;float:left; text-align:right">ALN</div></th><th style="width:30px; text-align:right"><div style="width:30px;float:left; text-align:right">ITA</div></th><th style="width:30px; text-align:right"><div style="width:30px;float:left; text-align:right">ASI</div></th><th style="width:30px; text-align:right"><div style="width:30px;float:left; text-align:right">Imp</div></th><th style="width:30px; text-align:right"><div style="width:30px;float:left; text-align:center">Tot</div></th></tr></div><table>\'
											
											;
											}
											else {
												return \'<tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:350px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:40px; text-align:right">\'+item[25]+\'</td><td style="width:40px; text-align:right">\'+item[33]+\'</td><td style="width:40px; text-align:right">\'+item[49]+\'</td><td style="width:40px; text-align:right">\'+item[34]+\'</td><td style="width:40px; text-align:right">\'+item[38]+\'</td><td style="width:40px; text-align:right">\'+item[50]+\'</td><td style="width:40px; text-align:right">\'+item[37]+\'</td><td style="width:40px; text-align:right">\'+item[22]+\'</td></tr>\'
												
												;
												}
											}
								
							}).result(addAccessory);
							
							/*$("#product_autocomplete_input").setOptions({
								extraParams: {excludeIds : getAccessorieIds()}
							});*/		
						
					});
					
					
				
					$("#product_autocomplete_input").css(\'width\',\'705px\');
					$("#product_autocomplete_input").keypress(function(event){
								
								  var keycode = (event.keyCode ? event.keyCode : event.which);
								  if (keycode == "13") {
									  $("#veditutti").trigger("click");
									event.preventDefault();
									event.stopPropagation();    
								  }
								});
					
					
							</script><script type="text/javascript">
				
				$("#veditutti").click ( function (zEvent) {
					
					/*	$("body").trigger("click");
						$("body").trigger("click");
						
						
						
						$("#product_autocomplete_input").focus();
					//	$("#product_autocomplete_input").val("");
					//	$("#product_autocomplete_input").val(" ");
					*/
						var press = jQuery.Event("keypress");
						press.ctrlKey = true;
						if(document.getElementById(\'product_autocomplete_input\').value == "")
						{
							$("#product_autocomplete_input").val(" ");
						}
						$("#product_autocomplete_input").trigger(press);
					
						$("#product_autocomplete_input").trigger("click");
						$("#product_autocomplete_input").trigger("click");
					//	$("#product_autocomplete_input").val("");
						
				} );


				</script>
						</td>
					</tr>';
					
					echo '<tr><td></td><td><br /><br /><br /></td></tr>';
					
					echo '
					<tr>
						<td class="col-left">'.$this->l('Prodotti alternativi:').'<br /><br /><i>'.$this->l('(Do not forget to Save the product afterward)').'</i></td>
						<td style="padding-bottom:5px;">
							<div id="divAlternatives">';
					foreach ($alternatives as $alternative)
					{
						if($alternative['name'] == '')
						{
							$alternative = Db::getInstance()->getRow('SELECT `id_bundle` id_product, bundle_ref reference, `bundle_name` name FROM `bundle` WHERE `id_bundle` = '.$alternative['id_product']);
						}
						
						echo htmlentities($alternative['name'], ENT_COMPAT, 'UTF-8').(!empty($alternative['reference']) ? ' ('.$alternative['reference'].')' : '').' <span onclick="delAlternative('.$alternative['id_product'].');" style="cursor: pointer;"><img src="../img/admin/delete.gif" class="middle" alt="" /></span><br />';
					}	
					echo '</div>
							<input type="hidden" name="inputAlternatives" id="inputAlternatives" value="';
					foreach ($alternatives as $alternative)
						echo (int)$alternative['id_product'].'-';
					echo '" />
							<input type="hidden" name="nameAlternatives" id="nameAlternatives" value="';
					foreach ($alternatives as $alternative)
						echo htmlentities($alternative['name'], ENT_COMPAT, 'UTF-8').'¤';

					echo '" />
							<script type="text/javascript">
								var formProduct = "";
								var alternatives = new Array();
							</script>
						<script type="text/javascript">
						
					
					function clearAutoComplete()
					{
						$("#veditutti2").trigger("click");
						
					}	
					
			
			/*
				//setup before functions
				var typingTimer;                //timer identifier
				var doneTypingInterval = 3000;  //time in ms, 3 second for example
				var $ainput = $("#product_autocomplete_input2");

				//on keyup, start the countdown
				$ainput.on("keyup", function () {
				  clearTimeout(typingTimer);
				  typingTimer = setTimeout(doneTyping, doneTypingInterval);
				});

				//on keydown, clear the countdown 
				$ainput.on("keydown", function () {
				  clearTimeout(typingTimer);
				});

				//user is "finished typing," do something
				function doneTyping () {
					lastKeyPressCode = event.keyCode;
					if(lastKeyPressCode != 37 && lastKeyPressCode != 38 && lastKeyPressCode != 39 && lastKeyPressCode != 40)
						$ainput.trigger("click");
				}
			*/
			
					var formProduct = "";
					var products = new Array();
				</script>
							<div id="ajax_choose_product2" style="padding:6px; padding-top:2px; width:800px;">
								<p class="clear">'.$this->l('Begin typing the first letters of the product name, then select the product from the drop-down list:').'</p>
								<input type="checkbox" checked="checked" id="prodotti_online2" onclick="if(document.getElementById(\'product_autocomplete_input2\').value != \'\') { clearAutoComplete(); }" />Online &nbsp;&nbsp;
			<input type="checkbox" id="prodotti_offline2" onclick="if(document.getElementById(\'product_autocomplete_input2\').value != \'\') { clearAutoComplete(); }" />Offline &nbsp;
<input type="checkbox" id="prodotti_old2" onclick="if(document.getElementById(\'product_autocomplete_input2\').value != \'\') { clearAutoComplete(); }" />Old &nbsp;&nbsp;
			<input type="checkbox" id="prodotti_disponibili2" onclick="if(document.getElementById(\'product_autocomplete_input2\').value != \'\') { clearAutoComplete(); }" />Solo disp. &nbsp;&nbsp;&nbsp;
			
			<script type="text/javascript" src="../js/select2.js"></script>
					<script type="text/javascript">
					$(document).ready(function() { $("#auto_marca2").select2(); $("#auto_serie2").select2(); $("#auto_fornitore2").select2();  $("#auto_categoria2").select2(); });
					</script>
					
			<select id="auto_marca2" name="auto_marca" onchange="repopulateSeries2(this.value); clearAutoComplete();" style="width:100px">
			<option value="0">Marca...</option>
			';
			$marche = Db::getInstance()->executeS('SELECT * FROM manufacturer ORDER BY name ASC');
			foreach($marche as $marca)
				echo '<option value="'.$marca['id_manufacturer'].'">'.$marca['name'].'</option>';
			echo '
			</select>
			
					
			<select id="auto_serie2" name="auto_serie" onchange="clearAutoComplete();" style="width:100px">
			<option value="0">Serie...</option>
			<option value="0">Scegli prima un costruttore</option>
			';
			echo '
			</select>
			
			<select id="auto_categoria2" name="auto_categoria" onchange="clearAutoComplete();" style="width:100px">
			<option value="0">Categoria...</option>
			';
			$categorie = Db::getInstance()->executeS('SELECT * FROM category c JOIN category_lang cl ON c.id_category = cl.id_category WHERE cl.id_lang = 5 AND c.id_parent = 1 ORDER BY cl.name ASC');
			foreach($categorie as $categoria)
				echo '<option value="'.$categoria['id_category'].'">'.$categoria['name'].'</option>';
			echo '
			</select>
			
			<select id="auto_fornitore2" name="auto_fornitore" onchange="clearAutoComplete(); " style="width:100px">
			<option value="0">Fornitore...</option>
			';
			
			$fornitori = Db::getInstance()->executeS('SELECT * FROM supplier WHERE id_supplier = 8 OR id_supplier = 11 OR id_supplier = 42 ORDER BY name ASC');
			foreach($fornitori as $fornitore)
				echo '<option value="'.$fornitore['id_supplier'].'">'.$fornitore['name'].'</option>';
			echo '
			</select>
			
			<input id="reset2" type="button" value="Reset" class="button" onclick="document.getElementById(\'prodotti_online2\').checked = true; document.getElementById(\'prodotti_offline2\').checked = true; document.getElementById(\'prodotti_old2\').checked = true; document.getElementById(\'prodotti_disponibili2\').checked = false; document.getElementById(\'auto_categoria2\').options[0].selected = true; $(\'#auto_categoria2\').select2();  document.getElementById(\'auto_marca2\').options[0].selected = true; $(\'#auto_marca2\').select2();  
			document.getElementById(\'auto_serie2\').options[0].selected = true; $(\'#auto_serie2\').select2();  
			document.getElementById(\'auto_fornitore2\').options[0].selected = true; $(\'#auto_fornitore2\').select2(); $(\'#prodlist2\').hide(); $(\'div.autocomplete_list\').hide();" />
			<br />
			<input type="checkbox" name="copia_da2" id="copia_da2" onclick="if(document.getElementById(\'product_autocomplete_input2\').value != \'\') { clearAutoComplete(); }"  /> Spunta prima di cercare se vuoi copiare alternative dal prodotto selezionato, lascia bianco se vuoi caricarlo come alternativa
			<br />
			<input id="veditutti2" type="button" value="Cerca" class="button" style="display:none" /> <input size="123" type="text" value="" id="product_autocomplete_input2" style="margin-top:3px" /> 
								<img onclick="$(this).prev().search();" style="cursor: pointer;" src="../img/admin/add.gif" alt="'.$this->l('Add an alternative').'" title="'.$this->l('Add an alternative').'" />
							</div>
							<script type="text/javascript">
							
							function getAlternativeIds()
								{
									var ids = '. $obj->id.'+\',\';
									ids += $(\'#inputAlternatives\').val().replace(/\\-/g,\',\').replace(/\\,$/,\'\');
									ids = ids.replace(/\,$/,\'\');

									return ids;
								}
								
								urlToCall = null;
								/* function autocomplete */
								
								function getOnline()
					{
						if(document.getElementById("prodotti_online2").checked == true)
							return 1;
						else
							return 0;
					}
					
					function getOffline()
					{
						if(document.getElementById("prodotti_offline2").checked == true)
							return 1;
						else
							return 0;
					}

					function getOld()
					{
						if(document.getElementById("prodotti_old2").checked == true)
							return 1;
						else
							return 0;
					}
					
					function getDisponibili()
					{
						if(document.getElementById("prodotti_disponibili2").checked == true)
							return 1;
						else
							return 0;
					}
					
					function getSerie()
					{
						if(document.getElementById("auto_serie2").value > 0)
							return document.getElementById("auto_serie2").value;
						else
							return 0;
					}
					
					function getMarca()
					{
						if(document.getElementById("auto_marca2").value > 0)
							return document.getElementById("auto_marca2").value;
						else
							return 0;
					}
					
					function getFornitore()
					{
						if(document.getElementById("auto_fornitore2").value > 0)
							return document.getElementById("auto_fornitore2").value;
						else
							return 0;
					}
					
					function getCategoria()
					{
						if(document.getElementById("auto_categoria2").value > 0)
							return document.getElementById("auto_categoria2").value;
						else
							return 0;
					}
					
					function getCopiaDa2()
					{
						console.log("COPIA ACCESSORI");
						if(document.getElementById("copia_da2").checked == true)
							return 1;
						else
							return 0;
					}
					
					function repopulateSeries2(id_manufacturer)
					{
						$("#auto_serie2").empty();
						$.ajax({
						  url:"ajax.php?repopulate_series=y",
						  type: "POST",
						  data: { id_manufacturer: id_manufacturer
						  },
						  success:function(resp){  
							var newOptions = $.parseJSON(resp);
							
							 $("#auto_serie2").append($("<option></option>")
								 .attr("value", "0").text("Serie..."));
							
							$.each(newOptions, function(key,value) {
							  $("#auto_serie2").append($("<option></option>")
								 .attr("value", value).text(key));
							});
							
							$("#auto_serie2").select2({
								placeholder: "Serie..."
							});
						  },
						  error: function(xhr,stato,errori){
						  }
						});
							
						
					}	
							
					$("body").on("click", function (event) {
						 if(event.target.tagName.toUpperCase() == "BODY"  || event.target.id == "auto_marca2"   || event.target.id == "auto_serie2" || event.target.id == "auto_categoria2" || event.target.id == "auto_fornitore2" || event.target.id == "veditutti2" || event.target.id == "prodotti_online2"  || event.target.id == "prodotti_offline2" || event.target.id == "prodotti_old2"  || event.target.id == "prodotti_disponibili2" || event.target.id == "product_autocomplete_input2")
						 {
							event.stopPropagation();
							
						 }
						 else
						 {
							$(\'#prodlist2\').hide();
							$(\'div.autocomplete_list2\').hide();
						 }
					});	
					
								$(function() {
						$(\'#product_autocomplete_input2\')
							.autocomplete(\'ajax_products_list2.php?id_customer=0\', {
								minChars: 0,
								autoFill: false,
								max:5000,
								matchContains: true,
								matchSubset: 0,
								mustMatch:true,
								scroll:true,
								useCache:false,
								cacheLength:1,
								formatItem: function(item) {
							
											var color = "";
											if(item[24] == 1) {
												var color = "; color:red";
											}
											
											if(item[39] == 1)
											{
												var cornice = "; border:1px solid green";
											}
											else
											{
												var cornice = "; border:1px solid red";
											}
											if(item[28] == 0) {
												return \'</table><br /><div onclick="return false;" style="position:absolute; z-index:99997; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:180px;float:left;margin-left:10px;">Codice</div></th><th style="width:630px;"><div style="width:200px; float:left;">Desc. prodotto<br /><br /></div></th><th style="width:60px; text-align:right"><div style="width:45px;float:left;">Prezzo</div></th><th style="width:35px; text-align:right"><div style="width:30px;float:left; text-align:right">EZ</div></th><th style="width:35px; text-align:right"><div style="width:40px;float:left; text-align:right">ESP</div></th><th style="width:35px; text-align:right"><div style="width:30px;float:left; text-align:right">INT</div></th><th style="width:35px; text-align:right"><div style="width:30px;float:left; text-align:right">ALN</div></th><th style="width:30px; text-align:right"><div style="width:30px;float:left; text-align:right">ITA</div></th><th style="width:30px; text-align:right"><div style="width:30px;float:left; text-align:right">ASI</div></th><th style="width:30px; text-align:right"><div style="width:30px;float:left; text-align:right">Imp</div></th><th style="width:30px; text-align:right"><div style="width:30px;float:left; text-align:center">Tot</div></th></tr></div><table>\'
												
												;
											}
											else {
												return \'<tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:350px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:40px; text-align:right">\'+item[25]+\'</td><td style="width:40px; text-align:right">\'+item[33]+\'</td><td style="width:40px; text-align:right">\'+item[49]+\'</td><td style="width:40px; text-align:right">\'+item[34]+\'</td><td style="width:40px; text-align:right">\'+item[38]+\'</td><td style="width:40px; text-align:right">\'+item[50]+\'</td><td style="width:40px; text-align:right">\'+item[37]+\'</td><td style="width:40px; text-align:right">\'+item[22]+\'</td></tr>\'
												
												;
												}
											}
								
							}).result(addAlternative);
							
							/*$("#product_autocomplete_input2").setOptions({
								extraParams: {excludeIds : getAlternativeIds()}
							});*/		
						
					});
					
					
				
					$("#product_autocomplete_input2").css(\'width\',\'705px\');
					$("#product_autocomplete_input2").keypress(function(event){
								
								  var keycode = (event.keyCode ? event.keyCode : event.which);
								  if (keycode == "13") {
									  $("#veditutti2").trigger("click");
									event.preventDefault();
									event.stopPropagation();    
								  }
								});
					
					
							</script><script type="text/javascript">
				
				$("#veditutti2").click ( function (zEvent) {
					
					/*	$("body").trigger("click");
						$("body").trigger("click");
						
						
						
						$("#product_autocomplete_input2").focus();
					//	$("#product_autocomplete_input2").val("");
					//	$("#product_autocomplete_input2").val(" ");
					*/
						var press = jQuery.Event("keypress");
						press.ctrlKey = true;
						if(document.getElementById(\'product_autocomplete_input2\').value == "")
						{
							$("#product_autocomplete_input2").val(" ");
						}
						$("#product_autocomplete_input2").trigger(press);
					
						$("#product_autocomplete_input2").trigger("click");
						$("#product_autocomplete_input2").trigger("click");
					//	$("#product_autocomplete_input2").val("");
						
				} );


				</script>
						</td>
					</tr>';
					
					echo '<tr><td class="col-left">Note</td>
<td>';			

		
					echo '<textarea rows="6" cols="100" name="note" id="note" class="editable">'.(Tools::getIsset('note') ? Tools::getValue('note') : $obj->note).'</textarea>
					
					
					
					
					
					
					</tr>';
					
					echo '<tr><td colspan="2" style="padding-bottom:10px;"><hr style="width:100%;" /></td></tr>
					<tr>
						<td colspan="2" style="text-align:center;">';
						/*	<input type="submit" value="'.$this->l('Save').'" name="submitAdd'.$this->table.'" class="button" />
							&nbsp;*/ echo '<input type="submit" id="submitaddproductandstay" value="'.$this->l('Save and stay').'" name="submitAdd'.$this->table.'AndStay" class="button" /></td>';
							echo '
					</tr>';
					
				echo '</table>
			<br />
			</div>';
			// TinyMCE
		global $cookie;
		$iso = Language::getIsoById((int)($cookie->id_lang));
		$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
		$ad = dirname($_SERVER["PHP_SELF"]);
		echo '
			<script type="text/javascript">
			var iso = \''.$isoTinyMCE.'\' ;
			var pathCSS = \''._THEME_CSS_DIR_.'\' ;
			var ad = \''.$ad.'\' ;
			</script>
			
			
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce3/tiny_mce.js"></script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>
			';
			
			/*echo '
			<script src="../js/tinymce4.4.3/tinymce.min.js"></script>
             <script>tinymce.init({ selector:"textarea.editable", 
			  language_url : "../js/tiny_mce/auz/it.js",
			 
			 browser_spellcheck: true,
			 language: "it",
			 color_picker_callback: function(callback, value) {
    callback("#FF00FF");
  },
  insert_toolbar: "quickimage quicktable",
   min_width: 700,
   plugins: "link image code responsivefilemanager ",
    external_filemanager_path:"respfilemanager/",
   filemanager_title:"Responsive Filemanager" ,
			 menubar: "file edit view format table tools",
			 external_plugins: { "filemanager" : "https://www.ezdirect.it/ezadmin/respfilemanager/plugin.min.js"},
			content_css : "tiny-mce-editor.css",
			 resize: "both",
			 toolbar: "undo redo code | styleselect | bold italic | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | bullist numlist | formatselect link image responsivefilemanager"
			 });</script>';
			 
			 echo '
			<script type="text/javascript" src="../js/JavaScriptSpellCheck/include.js" ></script>

<script type="text/javascript">$Spelling.UserInterfaceTranslation = "it"; $Spelling.DefaultDictionary = "Italiano";</script>

';*/
			/*<script type="text/javascript" src="../js/tiny_mce4.11/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
	theme: "modern",
	language:"it",
	document_base_url : ad,
	spellchecker_rpc_url: "rpc.php",
	
	content_css: "tiny-mce-editor.css",
	plugins : ["advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"],

	 toolbar: "link unlink insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | image spellchecker | fontselect fontsizeselect | print preview media fullpage | forecolor backcolor emoticons", 
	 spellchecker_languages:"Italian=it",
		file_browser_callback : "ajaxfilemanager",
    width: 755,
    height: 300
 });
 
 function ajaxfilemanager(field_name, url, type, win) {
		var ajaxfilemanagerurl = "http://localhost/prestashop/ezadmin/ajaxfilemanager/ajaxfilemanager.php";
		switch (type) {
			case "image":
				break;
			case "media":
				break;
			case "flash": 
				break;
			case "file":
				break;
			default:
				return false;
	}
    tinymce.activeEditor.windowManager.open({
        url: ajaxfilemanagerurl,
        width: 782,
        height: 440,
        inline : "yes",
        close_previous : "no"
    },{
        window : win,
        input : field_name
    });
 }	
</script>*/

			echo '<script type="text/javascript">
					toggleVirtualProduct(getE(\'is_virtual_good\'));
					unitPriceWithTax(\'unit\');
			</script>';
		$categoryBox = Tools::getValue('categoryBox', array());

	}

	function displayFormImages($obj, $token = NULL)
	{
		global $cookie, $currentIndex, $attributeJs, $images;

		$countImages = (int)Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'image WHERE id_product = '.(int)$obj->id);
		echo '
		<div class="tab-page" id="step2">
				<h4 class="tab"><a onclick="">2. '.$this->l('Images').' ('.$countImages.')</a></h4>
				<table cellpadding="5">
				<tr>
					<td><b>'.(Tools::getValue('id_image')?$this->l('Edit this product image'):$this->l('Add a new image to this product')).'</b></td>
				</tr>
				</table>
				<hr style="width: 100%;" /><br />
				<table cellpadding="5" style="width:100%">
					<tr>
						<td class="col-left">'.$this->l('File:').'</td>
						<td style="padding-bottom:5px;">
							<input type="file" id="image_product" name="image_product" />
							<p>
								'.$this->l('Format:').' JPG, GIF, PNG. '.$this->l('Filesize:').' '.($this->maxImageSize / 1000).''.$this->l('Kb max.').'
								<br />'.$this->l('You can also upload a ZIP file containing several images. Thumbnails will be resized automatically.').'
							</p>
						</td>
					</tr>
					<tr>
						<td class="col-left">'.$this->l('Caption:').'</td>
						<td style="padding-bottom:5px;" class="translatable">';
						foreach ($this->_languages as $language)
						{
							if (!Tools::getValue('legend_'.$language['id_lang']))
								$legend = $this->getFieldValue($obj, 'name', $language['id_lang']);
							else
								$legend = Tools::getValue('legend_'.$language['id_lang']);
							echo '
							<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float:left; width:371px;">
								<input size="55" type="text" id="legend_'.$language['id_lang'].'" name="legend_'.$language['id_lang'].'" value="'.stripslashes(htmlentities($legend, ENT_COMPAT, 'UTF-8')).'" maxlength="128" />
								<sup> *</sup>
								<span class="hint" name="help_box">'.$this->l('Forbidden characters:').' <>;=#{}<br />'.$this->l('Forbidden characters will be automatically erased.').'<span class="hint-pointer">&nbsp;</span></span>
							</div>';
						}
						echo '
							<p class="clear">'.$this->l('Short description of the image').'</p>
						</td>
					</tr>
					<tr>
						<td class="col-left">'.$this->l('Cover:').'</td>
						<td style="padding-bottom:5px;">
							<input type="checkbox" size="40" name="cover" id="cover_on" class="checkbox"'.((isset($_POST['cover']) AND (int)($_POST['cover'])) ? ' checked="checked"' : '').' value="1" /><label class="t" for="cover_on"> '.$this->l('Use as product cover?').'</label>
							<p>'.$this->l('If you want to select this image as a product cover').'</p>
						</td>
					</tr>
					'; /* DEPRECATED FEATURE
					<tr>
						<td class="col-left">'.$this->l('Thumbnails resize method:').'</td>
						<td style="padding-bottom:5px;">
							<select name="resizer">
								<option value="auto"'.(Tools::getValue('resizer', 'auto') == 'auto' ? ' selected="selected"' : '').'>'.$this->l('Automatic').'</option>
								<option value="man"'.(Tools::getValue('resizer', 'auto') == 'man' ? ' selected="selected"' : '').'>'.$this->l('Manual').'</option>
							</select>
							<p>'.$this->l('Method you want to use to generate resized thumbnails').'</p>
						</td>
					</tr>*/
					echo '
					<tr>
						<td colspan="2" style="text-align:center;">';
						echo '<input type="hidden" name="resizer" value="auto" />';
					$images = Image::getImages((int)($cookie->id_lang), $obj->id);
					$imagesTotal = Image::getImagesTotal($obj->id);

							if (isset($obj->id) AND sizeof($images))
							{
								echo '<input type="submit" value="'.$this->l('   Save image   ').'" name="submitAdd'.$this->table.'AndStay" class="button" />';
								echo '<input type="hidden" value="on" name="productCreated" /><br /><br />';
							}
							echo (Tools::getValue('id_image') ? '<input type="hidden" name="id_image" value="'.(int)(Tools::getValue('id_image')).'" />' : '').'
						</td>
					</tr>
					<tr><td colspan="2" style="padding-bottom:10px;"><hr style="width:100%;" /></td></tr>';
					if (!sizeof($images) OR !isset($obj->id))
						echo '<tr>
						<td colspan="2" style="text-align:center;">
							<input type="hidden" value="off" name="productCreated" />
							'.(Tools::isSubmit('id_category') ? '<input type="submit" value="'.$this->l('Save').'" name="submitAdd'.$this->table.'" class="button" />' : '').'
							&nbsp;<input type="submit" value="'.$this->l('Save and stay').'" name="submitAdd'.$this->table.'AndStay" class="button" /></td>
					</tr>';
					else
					{
						echo '
						<tr>
							<td colspan="2">
							<script type="text/javascript" src="../js/jquery/jquery.tablednd_0_5.js"></script>
							<script type="text/javascript">
							$(document).ready(function() {
									$("#tableImages").tableDnD({
									onDrop: function(table, row) {
										var arrayValoriImmagine = document.getElementsByClassName("immagine_ordine");
										for (var i = 0; i < arrayValoriImmagine.length; ++i) {
											var item = parseFloat(arrayValoriImmagine[i].value);  
											$.ajax({
												type: "POST",
												async: false,
												url: "ajax_products_list2.php",
												data: "edit_image_sort_order=1&id_image="+item+"&sort_order=" + i,
												success: function()
												{

												},
									error: function(xhr, status, error) {
 
}
											});
										}
									},
									dragHandle: ".dragHandle"
								});
								
							
								$("#tableImages").hover(function() {
									$(this.cells[1]).addClass(\'showDragHandle\');
								}, function() {
									  $(this.cells[1]).removeClass(\'showDragHandle\');
								});
							});
							</script>
							<table cellspacing="0" cellpadding="0" id="tableImages" class="table">
								<tr>
									<th style="width: 100px;">'.$this->l('Image').'</th>
									<th>&nbsp;</th>
									<th>'.$this->l('Position').'</th>
									<th>ID</th>
									<th>'.$this->l('Cover').'</th>
									<th>'.$this->l('Original height x width').'</th>
									<th>'.$this->l('Original size').'</th>
									<th>'.$this->l('Large size').'</th>
									<th>'.$this->l('Box size').'</th>
									<th>'.$this->l('Small size').'</th>
									<th>'.$this->l('Medium size').'</th>
									<th>'.$this->l('Action').'</th>
								</tr>';

						foreach ($images AS $k => $image)
						{
							$image_obj = new Image($image['id_image']);
							$img_path = $image_obj->getExistingImgPath();
							echo  $this->_positionJS().'
							<tr>
								<td style="padding: 4px;"><a href="'._THEME_PROD_DIR_.$img_path.'.jpg" target="_blank">
								<img src="'._THEME_PROD_DIR_.$img_path.'-small.jpg'.((int)(Tools::getValue('image_updated')) === (int)($image['id_image']) ? '?date='.time() : '').'"
								alt="'.htmlentities(stripslashes($image['legend']), ENT_COMPAT, 'UTF-8').'" title="'.htmlentities(stripslashes($image['legend']), ENT_COMPAT, 'UTF-8').'" /></a></td>
								<td class="center">'.(int)($image['position']).'</td>
								<td class="pointer dragHandle center">';
								echo	'<img src="../img/admin/up-and-down.gif"
									alt="'.$this->l('Up').'" title="'.$this->l('Up').'" />';
							/*		
							if ($image['position'] == 1)
							{
								echo '<span>[ <img src="../img/admin/up.gif" alt="" border="0"> ]</span>';
								if ($image['position'] == $imagesTotal)
									echo '<span>[ <img src="../img/admin/down_d.gif" alt="" border="0"> ]</span>';
								else
									echo '<span>[ <a onclick="return hideLink();" href="'.$currentIndex.'&id_image='.$image['id_image'].'&imgPosition='.$image['position'].'&imgDirection=0&token='.($token ? $token : $this->token).'"><img src="../img/admin/down.gif" alt="" border="0"></a> ]</span>';
							}
							elseif ($image['position'] == $imagesTotal)
								echo '
									<span>[ <a onclick="return hideLink();" href="'.$currentIndex.'&id_image='.$image['id_image'].'&imgPosition='.$image['position'].'&imgDirection=1&token='.($token ? $token : $this->token).'"><img src="../img/admin/up.gif" alt="" border="0"></a> ]</span>
									<span>[ <img src="../img/admin/down_d.gif" alt="" border="0"> ]</span>';
							else
								echo '
									<span>[ <a onclick="return hideLink();" href="'.$currentIndex.'&id_image='.$image['id_image'].'&imgPosition='.$image['position'].'&imgDirection=1&token='.($token ? $token : $this->token).'"><img src="../img/admin/up.gif" alt="" border="0"></a> ]</span>
									<span>[ <a onclick="return hideLink();" href="'.$currentIndex.'&id_image='.$image['id_image'].'&imgPosition='.$image['position'].'&imgDirection=0&token='.($token ? $token : $this->token).'"><img src="../img/admin/down.gif" alt="" border="0"></a> ]</span>';*/
							echo '
								</td>
								<td class="center">'.$image['id_image'].'<input type="hidden" class="immagine_ordine" value="'.$image['id_image'].'" /></td>
								<td class="center" style="text-align:center"><a href="'.$currentIndex.'&id_image='.$image['id_image'].'&coverImage&token='.($token ? $token : $this->token).'"><img src="../img/admin/'.($image['cover'] ? 'enabled.gif' : 'forbbiden.gif').'" alt="" /></a></td>
								
								<td>'; list($imwidth, $imheight, $imtype, $imattr) = getimagesize("http://www.ezdirect.it"._THEME_PROD_DIR_.$img_path.".jpg"); 
								
								$fileimg_size_normal = number_format((filesize($_SERVER['DOCUMENT_ROOT']._THEME_PROD_DIR_.$img_path.".jpg")/1024), 2, ",", " "); 
								$fileimg_size_large = number_format((filesize($_SERVER['DOCUMENT_ROOT']._THEME_PROD_DIR_.$img_path."-large.jpg")/1024), 2, ",", " "); 
								$fileimg_size_box = number_format((filesize($_SERVER['DOCUMENT_ROOT']._THEME_PROD_DIR_.$img_path."-thickbox.jpg")/1024), 2, ",", " "); 
								$fileimg_size_small = number_format((filesize($_SERVER['DOCUMENT_ROOT']._THEME_PROD_DIR_.$img_path."-small.jpg")/1024), 2, ",", " "); 
								$fileimg_size_medium = number_format((filesize($_SERVER['DOCUMENT_ROOT']._THEME_PROD_DIR_.$img_path."-medium.jpg")/1024), 2, ",", " "); 
								
								
								echo $imwidth." x ".$imheight; echo ' pixel</td>
								<td>'.$fileimg_size_normal.' kb</td>
								<td><strong>'.$fileimg_size_large.' kb</strong></td>
								<td>'.$fileimg_size_box.' kb</td>
								<td>'.$fileimg_size_small.' kb</td>
								<td>'.$fileimg_size_medium.' kb</td>
								<td class="center">
									<a href="'.$currentIndex.'&id_image='.$image['id_image'].'&editImage&tabs=1&token='.($token ? $token : $this->token).'"><img src="../img/admin/edit.gif" alt="'.$this->l('Modify this image').'" title="'.$this->l('Modify this image').'" /></a>
									<a href="'.$currentIndex.'&id_image='.$image['id_image'].'&deleteImage&tabs=1&token='.($token ? $token : $this->token).'" onclick="return confirm(\''.$this->l('Are you sure?', __CLASS__, true, false).'\');"><img src="../img/admin/delete.gif" alt="'.$this->l('Delete this image').'" title="'.$this->l('Delete this image').'" /></a>
								</td>
							</tr>';
						}
					}

			echo '
							</table>
						</td>
					</tr>
				</table>
			</div>';
			echo '
			<script type="text/javascript" src="../js/attributesBack.js"></script>
			<script type="text/javascript">
				var attrs = new Array();
				var modifyattributegroup = \''.addslashes(html_entity_decode($this->l('Modify this attribute combination'), ENT_COMPAT, 'UTF-8')).'\';
				attrs[0] = new Array(0, \'---\');';

			$attributes = Attribute::getAttributes((int)($cookie->id_lang), true);
			$attributeJs = array();

			foreach ($attributes AS $k => $attribute)
				$attributeJs[$attribute['id_attribute_group']][$attribute['id_attribute']] = $attribute['name'];

			foreach ($attributeJs AS $idgrp => $group)
			{
				echo '
				attrs['.$idgrp.'] = new Array(0, \'---\' ';
				foreach ($group AS $idattr => $attrname)
					echo ', '.$idattr.', \''.addslashes(($attrname)).'\'';
				echo ');';
			}
			echo '
			</script>';
	}
	
	function displayFormBundles($obj, $token = NULL)
	{
		global $cookie, $currentIndex;

		echo '
		<div class="tab-page" id="step_bundles">
				<h4 class="tab"><a '.(isset($_GET['newbundle']) ? 'onclick="location.href=\''.$currentIndex.'&id_product='.$obj->id.'&updateproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'&submitAddAndStay=on&tabs=3\';"' : '').'>4. Kit</a></h4>';
		
		if(isset($_GET['newbundle'])) {
		
			if(isset($_GET['updatebundle'])) {
				echo "<h3>Modifica bundle</h3>";
				$bundle = Db::getInstance()->getRow("SELECT * FROM bundle WHERE id_bundle = ".$_GET['id_bundle']."");
				echo "<input type='hidden' name='id_bundle' value='".$bundle['id_bundle']."' size='60' />";	
			}
			else {
			echo "<h3>Crea nuovo kit</h3>";
			}
			echo "
			Codice kit: <input type='text' name='bundle_ref' value='".(isset($_GET['updatebundle']) ? $bundle['bundle_ref'] : '' )."' size='30' /><br /><br />";
			echo "
			EAN kit: <input type='text' name='bundle_ean13' value='".(isset($_GET['updatebundle']) ? $bundle['ean13'] : '' )."' size='30' /><br /><br />";
			echo "ASIN kit: <input type='text' name='bundle_asin' value='".(isset($_GET['updatebundle']) ? $bundle['asin'] : '' )."' size='30' /><br /><br />";
			echo '
			<strong>Status</strong>: <input type="radio" name="bundle_active" id="active_on" value="1" '.(isset($_GET['updatebundle']) ? ($bundle['active'] == 1 ? 'checked="checked" ' : '') : 'checked="checked"').'/>
					<label class="t" for="active_on"><img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="bundle_active" id="active_off" value="0" '.(isset($_GET['updatebundle']) ? ($bundle['active'] == 0 ? 'checked="checked" ' : '') : '').'/>
					<label class="t" for="active_off"><img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label>
					<br /><br />';
					
			echo '<div >';
			foreach ($this->_languages as $language)
			{
				$bundle_name = Db::getInstance()->getValue('SELECT name FROM bundle_lang WHERE id_bundle = '.$_GET['id_bundle'].' AND id_lang = '.$language['id_lang']);
				echo '<div name="bundle_name_div_'.$language['id_lang'].'" id="bundle_name_div_'.$language['id_lang'].'" class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'block').'; float: left;">';
				echo "
				Nome kit: <input type='text' name='bundle_name_".$language['id_lang']."' value='".(isset($_GET['updatebundle']) ? ($bundle_name != '' ? $bundle_name : $bundle['bundle_name']) : '' )."' size='60' /> <img src='../img/l/".$language['id_lang'].".jpg' /><br />";
				echo '</div><br /><br />';
			}
			
			// echo '<div style="display:none">'.$this->displayFlags($this->_languages, $this->_defaultFormLanguage, 'bundle_name_div', 'bundle_name_div').'</div>';
			
			echo '</div>';
			
			echo '<div >';
			foreach ($this->_languages as $language)
			{
				$bundle_description_amazon = Db::getInstance()->getValue('SELECT description_amazon FROM bundle_lang WHERE id_bundle = '.$_GET['id_bundle'].' AND id_lang = '.$language['id_lang']);
				echo '<div name="bundle_description_amazon_div_'.$language['id_lang'].'" id="bundle_description_amazon_div_'.$language['id_lang'].'" class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'block').'; float: left;">';
				echo "
				Descrizione kit per Amazon <img src='../img/l/".$language['id_lang'].".jpg' />: <textarea class='rte' name='bundle_description_amazon_".$language['id_lang']."'>".(isset($_GET['updatebundle']) ? ($bundle_description_amazon != '' ? $bundle_description_amazon : '') : '' )."</textarea><br />";
				echo '</div><br /><br />';
			}
			echo '</div>';
			echo '<div style="clear:both"></div>';		
					
			echo '
			<script type="text/javascript" src="../js/jquery/jquery.tablednd_0_5.js"></script>
					<script type="text/javascript">
					$(document).ready(function() {
						$("#tableProducts").tableDnD();
					});
					</script>
					<script type="text/javascript">
					
					
				
				
				
					function calcolaImporto(id_product) {
						
						var totale = 0;
						var totaleriv = 0;
						var quantity = parseFloat(document.getElementById(\'product_quantity[\'+id_product+\']\').value);
						var numprice = document.getElementById(\'product_price[\'+id_product+\']\').value;
						
						if (quantity == "" || isNaN(quantity))
							quantity = 0;
							
						if (numprice == "")
							numprice = 0;
					
						var numpriceriv = document.getElementById(\'product_priceriv[\'+id_product+\']\').value;
						var numunitario = document.getElementById(\'unitario[\'+id_product+\']\').value;
						var numacquisto = document.getElementById(\'wholesale_price[\'+id_product+\']\').value;
						
						if (numpriceriv == "")
							numpriceriv = 0;
							
						if (numunitario == "")
							numunitario = 0;
							
						if (numacquisto == "")
							numacquisto = 0;
							
						var unitario = parseFloat(numunitario.replace(/\s/g, "").replace(",", "."));
						var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
						
						var priceriv = parseFloat(numpriceriv.replace(/\s/g, "").replace(",", "."));
						var acquisto = parseFloat(numacquisto.replace(/\s/g, "").replace(",", "."));
						
						var numscontocli = document.getElementById(\'product_sconto[\'+id_product+\']\').value;
						var numscontoriv = document.getElementById(\'product_scontoriv[\'+id_product+\']\').value;
						
						if (numscontocli == "")
							numscontocli = 0;
							
						if (numscontoriv == "")
							numscontoriv = 0;
						
						var scontocli = parseFloat(numscontocli.replace(/\s/g, "").replace(",", "."));
						var scontoriv = parseFloat(numscontoriv.replace(/\s/g, "").replace(",", "."));
						
						var sc_qta_1 = parseFloat(document.getElementById(\'sc_qta_1[\'+id_product+\']\').value);
						var sc_qta_2 = parseFloat(document.getElementById(\'sc_qta_2[\'+id_product+\']\').value);
						var sc_qta_3 = parseFloat(document.getElementById(\'sc_qta_3[\'+id_product+\']\').value);
						var sc_riv_1 = parseFloat(document.getElementById(\'sc_riv_1[\'+id_product+\']\').value);
						var sc_riv_2 = parseFloat(document.getElementById(\'sc_riv_2[\'+id_product+\']\').value);
										
						var sc_qta_1_q = parseFloat(document.getElementById(\'sc_qta_1_q[\'+id_product+\']\').value);
						var sc_qta_2_q = parseFloat(document.getElementById(\'sc_qta_2_q[\'+id_product+\']\').value);
						var sc_qta_3_q = parseFloat(document.getElementById(\'sc_qta_3_q[\'+id_product+\']\').value);
						var sc_riv_1_q = parseFloat(document.getElementById(\'sc_riv_1_q[\'+id_product+\']\').value);
						var sc_riv_2_q = parseFloat(document.getElementById(\'sc_riv_2_q[\'+id_product+\']\').value);
						
						if (sc_qta_1 == "" || isNaN(sc_qta_1))
							sc_qta_1 = 0;
							
						if (sc_qta_2 == "" || isNaN(sc_qta_2))
							sc_qta_2 = 0;

						if (sc_qta_3 == "" || isNaN(sc_qta_3))
							sc_qta_3 = 0;	
						
						if (sc_riv_1 == "" || isNaN(sc_riv_1))
							sc_riv_1 = 0;
							
						if (sc_riv_2 == "" || isNaN(sc_riv_2))
							sc_riv_2 = 0;

							var prezzounitarionew = unitario;
							
							var prezziconfronto = new Array(sc_riv_1, sc_riv_2, sc_qta_1, sc_qta_2, sc_qta_3);
							for (var i = 0; i < prezziconfronto.length; ++i) {
								if(prezziconfronto[i] !=0 && prezziconfronto[i] < prezzounitarionew) { 
									prezzounitarionew = prezziconfronto[i];
								
								} else { }
							}
													
							
							
							var prezzofinitoriv = prezzounitarionew - ((prezzounitarionew*scontoriv)/100);
							var importoriv = parseFloat(prezzofinitoriv.toFixed(2)*quantity);
							document.getElementById(\'product_priceriv[\'+id_product+\']\').value=prezzofinitoriv;
							document.getElementById(\'prz_vis_riv[\'+id_product+\']\').innerHTML=prezzounitarionew.toFixed(2).replace(".",",");
							document.getElementById(\'prz_nel_bundle_riv[\'+id_product+\']\').innerHTML=prezzofinitoriv.toFixed(2).replace(".",",");
						
						if(quantity < sc_qta_1_q && sc_qta_1_q != 0) {
							
							var prezzofinitocli = unitario - ((unitario*scontocli)/100);
							document.getElementById(\'prz_vis_cli[\'+id_product+\']\').innerHTML=unitario.toFixed(2).replace(".",",");	
									
						}
						
						else if((quantity >= sc_qta_1_q && quantity < sc_qta_2_q) || (quantity >= sc_qta_1_q && isNaN(parseFloat(sc_qta_2_q)))) {
					
							var prezzofinitocli = sc_qta_1 - ((sc_qta_1*scontocli)/100);
							document.getElementById(\'prz_vis_cli[\'+id_product+\']\').innerHTML=sc_qta_1.toFixed(2).replace(".",",");	
						}
						
						else if((quantity >= sc_qta_2_q && quantity < sc_qta_3_q) || (quantity >= sc_qta_2_q && isNaN(parseFloat(sc_qta_3_q == 0)))) {
						
							var prezzofinitocli = sc_qta_2 - ((sc_qta_2*scontocli)/100);
							document.getElementById(\'prz_vis_cli[\'+id_product+\']\').innerHTML=sc_qta_2.toFixed(2).replace(".",",");	
							
						}
						
						else if(quantity >= sc_qta_3_q && sc_qta_2_q != 0 && sc_qta_3_q != 0 && sc_qta_1_q != 0) {

							var prezzofinitocli = sc_qta_3 - ((sc_qta_3*scontocli)/100);
							document.getElementById(\'prz_vis_cli[\'+id_product+\']\').innerHTML=sc_qta_3.toFixed(2).replace(".",",");
							
						} else {
							var prezzofinitocli = unitario - ((unitario*scontocli)/100);
							document.getElementById(\'prz_vis_cli[\'+id_product+\']\').innerHTML=unitario.toFixed(2).replace(".",",");	
						}
						
						document.getElementById(\'product_price[\'+id_product+\']\').value=prezzofinitocli;	
						var importo = parseFloat(prezzofinitocli.toFixed(2)*quantity);
						
						document.getElementById(\'prz_nel_bundle_cli[\'+id_product+\']\').innerHTML=prezzofinitocli.toFixed(2).replace(".",",");
						
						document.getElementById(\'impImporto[\'+id_product+\']\').value = importo;
						document.getElementById(\'impImportoRiv[\'+id_product+\']\').value = importoriv;
						
						var marginalita = (((prezzofinitocli - acquisto)*100) / prezzofinitocli);
							marginalita = marginalita.toFixed(2);
							
							var marginalitariv = (((prezzofinitoriv - acquisto)*100) / prezzofinitoriv);
							marginalitariv = marginalitariv.toFixed(2);
						
							document.getElementById(\'spanmarginalita[\'+id_product+\']\').innerHTML=marginalita.replace(".",",")+"%";
							document.getElementById(\'spanmarginalitariv[\'+id_product+\']\').innerHTML=marginalitariv.replace(".",",")+"%";
					
						var arrayImporti = document.getElementsByClassName("importo");
						for (var i = 0; i < arrayImporti.length; ++i) {
							var item = parseFloat(arrayImporti[i].value);
							totale += item;
						}
						
						var arrayImportiriv = document.getElementsByClassName("importoriv");
						for (var i = 0; i < arrayImportiriv.length; ++i) {
							var item = parseFloat(arrayImportiriv[i].value);  
							totaleriv += item;
						}

						var totaleacquisti = 0;
					
						var arrayAcquisti = document.getElementsByClassName("prezzoacquisto");
						for (var i = 0; i < arrayAcquisti.length; ++i) {
							arrayAcquisti[i].value = arrayAcquisti[i].value.replace(",",".");
							var item = parseFloat(arrayAcquisti[i].value); 					
							totaleacquisti += item;
						}
			
						var marginalitatotale = (((totale - totaleacquisti)*100) / totale);
						
						marginalitatotale = marginalitatotale.toFixed(2);
		
						document.getElementById(\'marginalitatotale\').innerHTML=marginalitatotale.replace(".",",") + "%";
						
						document.getElementById(\'totaleprodotti\').value = totale;
						totale = totale.toFixed(2);
						document.getElementById(\'totaleprodottiriv\').value = totaleriv;
						totaleriv = totaleriv.toFixed(2);
						
					
												
						document.getElementById(\'spantotaleprodottiriv\').innerHTML=totaleriv.replace(".",",");
						document.getElementById(\'spantotaleprodotti\').innerHTML=totale.replace(".",",");
						
				}
				
				function togliImporto(id_product) {
				
					var daTogliere = parseFloat(document.getElementById(\'product_price[\'+id_product+\']\').value.replace(",", "."));
					var daTogliereriv = parseFloat(document.getElementById(\'product_priceriv[\'+id_product+\']\').value.replace(",", "."));
					
					var totale = parseFloat(document.getElementById(\'totaleprodotti\').value);
					
					var totaleriv = parseFloat(document.getElementById(\'totaleprodottiriv\').value);
					
					totale = totale-daTogliere;
					
					totale = totale.toFixed(2);
					
					totaleriv = totaleriv-daTogliereriv;
					
					totaleriv = totaleriv.toFixed(2);
					
					document.getElementById(\'totaleprodotti\').value=totale;
						
					document.getElementById(\'spantotaleprodotti\').innerHTML=totale.replace(".",",");
					
					document.getElementById(\'totaleprodottiriv\').value=totaleriv;
						
					document.getElementById(\'spantotaleprodottiriv\').innerHTML=totaleriv.replace(".",",");
					
				}
				
				function addProduct_TR(event, data, formatted)
					{
						
						var productId = data[1];
						var productPrice = data[2];
						var productScQta = data[3];
						var productRef = data[4];
						var productName = data[5];
						var productQuantity = data[16];
						var tdImporto = data[17];
						var przUnitario = data[18];
						var deleteProd = data[19];
						var marg = data[20];
						var scontoExtra = data[21];
						var sc_qta_1 = data[6];
						var sc_qta_2 = data[7];
						var sc_qta_3 = data[8];
						var sc_riv_1 = data[9];
						var sc_riv_2 = data[10];
						var sc_qta_1_q = data[11];	
						var sc_qta_2_q = data[12];	
						var sc_qta_3_q = data[13];	
						var sc_riv_1_q = data[14];	
						var sc_riv_2_q = data[15];	
						var priceriv = data[41];
						var baseprice = data[42];
						var basepriceriv = data[43];
						var margriv = data[44];
						var varspec = data[45];
						var textVarSpec = "";
						if(sc_riv_1 == "COPIADA") {
							console.log("COPIA ACCESSORI OK");
							var prodotti_bundle = sc_riv_2.split(";");
							for(i=0;i<((prodotti_bundle.length))-1;i++)
							{
								$.ajax({
								type: "GET",
								data: "id_product="+data[1]+"&stop_accessori=1",
								async: false,
								url: "ajax_products_list2.php",
								success: function(resp)
									{
										addProduct_TR("",resp.split("|"),"");
									},
									error: function(XMLHttpRequest, textStatus, errorThrown)
									{
										tooltip_content = "";				
									}
															
								});
							}
							return false;
						
						}
						else
							console.log("COPIA ACCESSORI KO");
						
						if(varspec == 0) {
						}
						else {
							textVarSpec = "style=\'color:red\'";
						}
						$("<tr id=\'tr_"+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"\'><td> " + productId + " </td><td><a href=\'index.php?tab=AdminCatalogExFeatures&id_product="+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"&updateproduct&token='.$this->token.'\' target=\'_blank\'>" + productRef + "</a></td><td>"+ productName +"</td><td>" + productQuantity + " " + przUnitario + " " + sc_qta_1 +" " + sc_qta_2 +" " + sc_qta_3 +" " + sc_riv_1 +" " + sc_riv_2 +" " + sc_qta_1_q +" " + sc_qta_2_q +" " + sc_qta_3_q +" " + sc_riv_1_q +" " + sc_riv_2_q +"</td><td style=\'text-align:right\'><table style=\'width:100%\'><tr><td>Prz. base</td><td><span id=\'prz_vis_cli["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' " + textVarSpec + ">" + baseprice + "</span> <span " + textVarSpec + "></span></td></tr><tr><td>Sconto %</td><td><input type=\'text\' style=\'text-align:right\' size=\'4\' id=\'product_sconto["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' name=\'nuovo_sconto["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' size=\'4\' value=\'0,00\' onkeyup=\'javascript: calcolaImporto("+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+");\' /></td></tr><tr><td>Prz. nel kit</td><td><span id=\'prz_nel_bundle_cli["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\'>" + baseprice + "</span></td></tr><td>Marg.</td>" + marg + "</tr></table></td><td style=\'text-align:right\'><table style=\'width:100%\'><tr><td>Prz. base</td><td><span id=\'prz_vis_riv["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\'>" + basepriceriv + "</span> </td></tr><tr><td>Sconto %</td><td><input type=\'text\' style=\'text-align:right\' size=\'4\' id=\'product_scontoriv["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' name=\'nuovo_scontoriv["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' value=\'0,00\' onkeyup=\'javascript: calcolaImporto("+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+");\' /></td></tr><tr><td>Prz. nel bundle</td><td><span id=\'prz_nel_bundle_riv["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\'>" + basepriceriv + "</span> </td></tr><td>Marg.</td><td>" + margriv + "</td></tr></table></td><td style=\'text-align:center\'>" + deleteProd + "<input name=\'nuovo_prodotto["+ productId +"]\' type=\'hidden\' value=\'" + productId + "\' /></td></tr>").appendTo("#tableProducts");
							
				
						
						productId = productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "");
				
						calcolaImporto(productId);
						
						$("#tableProducts").tableDnD();
						
						$("#span-reference-"+productId+"").tooltip({ showURL: false  });
						
						$("body").trigger("click");			
						$("#product_autocomplete_input_bundle").trigger("click");
						$("#product_autocomplete_input_bundle").trigger("click");
								
					}
					
					function delProduct30(id)
					{
						document.getElementById(\'tr_\'+id).innerHTML = "";
						
						
					}
				</script>	';
				if(isset($_GET['updatebundle'])) {
				
					$id_image_bundle = Db::getInstance()->getValue("SELECT id_image FROM bundle_image WHERE id_bundle = ".$bundle['id_bundle']."");
					$image_legend = Db::getInstance()->getValue("SELECT legend FROM bundle_image_lang WHERE id_lang = 5 AND id_image = ".$id_image_bundle."");
					echo "<input type='hidden' name='id_image_bundle' value='".$id_image_bundle."' />";
				
				}
				else { 
					echo "<input type='hidden' name='id_image_bundle' value='0' />";
				
				}
				
				echo '<hr style="width: 100%;" /><br />
				<table cellpadding="5" style="width:100%">
				<tr>
				<td>
					<table cellpadding="5" style="width:100%">
						<tr>
							<td class="col-left">'.$this->l('Immagine per kit:').'</td>
							<td style="padding-bottom:5px;">
								<input type="file" id="image_bundle" name="image_bundle" />
								<p>
									'.$this->l('Formato:').' JPG, GIF, PNG. '.$this->l('Dimensioni:').' '.($this->maxImageSize / 1000).''.$this->l('Kb max.').'<br />
								
								</p>
							</td>
						</tr>
						<tr>
						<td>Descrizione immagine</td>
						<td><input type="text" name="image_legend" size="50" value="'.(isset($_GET['updatebundle']) ? htmlentities($image_legend) : '' ).'" /></td>
						</tr>
					</table>
				</td>
				<td>';
				if(isset($_GET['updatebundle']) && $id_image_bundle > 0) {
					echo '<table cellpadding="5" style="width:100%">
						<tr>
							<td class="col-left" style="vertical-align:middle">'.$this->l('Immagine attuale:').'<br /><span style="font-size:10px">Per sostituire l\'immagine basta caricarne una nuova</td>
							<td style="padding-bottom:5px;" style="vertical-align:middle">
								<img src="'._PS_IMG_.'b/'.$id_image_bundle."-".$bundle['id_bundle'].'-medium.jpg" alt="" title="" />
							</td>
						</tr>
						
					</table>';
				
				}
				else {
				}
				echo '</td>
				</tr>
				</table>
				<hr style="width: 100%;" /><br />	
			<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />				
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete_bundle.js"></script>
								<div id="ajax_choose_product" style="padding:6px; padding-top:2px; width:800px;">
									
									<input type="checkbox" checked="checked" id="prodotti_online_bundle" onclick="if(document.getElementById(\'product_autocomplete_input_bundle\').value != \'\') { clearAutoComplete(); }" />Online &nbsp;&nbsp;
				<input type="checkbox" id="prodotti_offline_bundle" onclick="if(document.getElementById(\'product_autocomplete_input_bundle\').value != \'\') { clearAutoComplete(); }" />Offline &nbsp;
	<input type="checkbox" id="prodotti_old_bundle" onclick="if(document.getElementById(\'product_autocomplete_input_bundle\').value != \'\') { clearAutoComplete(); }" />Old &nbsp;&nbsp;
				<input type="checkbox" id="prodotti_disponibili_bundle" onclick="if(document.getElementById(\'product_autocomplete_input_bundle\').value != \'\') { clearAutoComplete(); }" />Solo disp. &nbsp;&nbsp;&nbsp;
				
				<script type="text/javascript" src="../js/select2.js"></script>
						<script type="text/javascript">
						$(document).ready(function() { $("#auto_marca_bundle").select2(); $("#auto_serie_bundle").select2(); $("#auto_fornitore_bundle").select2();  $("#auto_categoria_bundle").select2(); });
						</script>
						
				<select id="auto_marca_bundle" name="auto_marca" onchange="repopulateSeries(this.value); clearAutoComplete();" style="width:100px">
				<option value="0">Marca...</option>
				';
				$marche = Db::getInstance()->executeS('SELECT * FROM manufacturer ORDER BY name ASC');
				foreach($marche as $marca)
					echo '<option value="'.$marca['id_manufacturer'].'">'.$marca['name'].'</option>';
				echo '
				</select>
				
						
				<select id="auto_serie_bundle" name="auto_serie" onchange="clearAutoComplete();" style="width:100px">
				<option value="0">Serie...</option>
				<option value="0">Scegli prima un costruttore</option>
				';
				echo '
				</select>
				
				<select id="auto_categoria_bundle" name="auto_categoria" onchange="clearAutoComplete();" style="width:100px">
				<option value="0">Categoria...</option>
				';
				$categorie = Db::getInstance()->executeS('SELECT * FROM category c JOIN category_lang cl ON c.id_category = cl.id_category WHERE cl.id_lang = 5 AND c.id_parent = 1 ORDER BY cl.name ASC');
				foreach($categorie as $categoria)
					echo '<option value="'.$categoria['id_category'].'">'.$categoria['name'].'</option>';
				echo '
				</select>
				
				<select id="auto_fornitore_bundle" name="auto_fornitore" onchange="clearAutoComplete(); " style="width:100px">
				<option value="0">Fornitore...</option>
				';
				
				$fornitori = Db::getInstance()->executeS('SELECT * FROM supplier WHERE id_supplier = 8 OR id_supplier = 11 OR id_supplier = 42 ORDER BY name ASC');
				foreach($fornitori as $fornitore)
					echo '<option value="'.$fornitore['id_supplier'].'">'.$fornitore['name'].'</option>';
				echo '
				</select>
				
				<input id="reset" type="button" value="Reset" class="button" onclick="document.getElementById(\'prodotti_online_bundle\').checked = true; document.getElementById(\'prodotti_offline_bundle\').checked = true; document.getElementById(\'prodotti_old_bundle\').checked = true; document.getElementById(\'prodotti_disponibili_bundle\').checked = false; document.getElementById(\'auto_categoria_bundle\').options[0].selected = true; $(\'#auto_categoria_bundle\').select2();  document.getElementById(\'auto_marca_bundle\').options[0].selected = true; $(\'#auto_marca_bundle\').select2();  
				document.getElementById(\'auto_serie_bundle\').options[0].selected = true; $(\'#auto_seri_bundlee\').select2();  
				document.getElementById(\'auto_fornitore_bundle\').options[0].selected = true; $(\'#auto_fornitore_bundle\').select2(); $(\'#prodlist\').hide(); $(\'div.autocomplete_list\').hide();" />
				
				<br />
				<input id="veditutti_bundle" type="button" value="Cerca" class="button" style="display:none" />
				
			Seleziona il prodotto da aggiungere: <input size="123" type="text" value="" id="product_autocomplete_input_bundle" />
			<script type="text/javascript">
						var formProduct = "";
						var products = new Array();
					</script>
					<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
					
					<script type="text/javascript">
								
									
									urlToCall = null;
									/* function autocomplete */
									
						function getOnline_bundle()
						{
							if(document.getElementById("prodotti_online_bundle").checked == true)
								return 1;
							else
								return 0;
						}
						
						function getOld_bundle()
						{
							if(document.getElementById("prodotti_old_bundle").checked == true)
								return 1;
							else
								return 0;
						}
						
						function getOffline_bundle()
						{
							if(document.getElementById("prodotti_offline_bundle").checked == true)
								return 1;
							else
								return 0;
						}

	function getOld()
						{
							if(document.getElementById("prodotti_old_bundle").checked == true)
								return 1;
							else
								return 0;
						}
						
						function getDisponibili_bundle()
						{
							if(document.getElementById("prodotti_disponibili_bundle").checked == true)
								return 1;
							else
								return 0;
						}
						
						function getCopiaDa_bundle()
						{
								return 0;
						}
						
						function getSerie_bundle()
						{
							if(document.getElementById("auto_serie_bundle").value > 0)
								return document.getElementById("auto_serie_bundle").value;
							else
								return 0;
						}
						
						function getMarca_bundle()
						{
							if(document.getElementById("auto_marca_bundle").value > 0)
								return document.getElementById("auto_marca_bundle").value;
							else
								return 0;
						}
						
						function getFornitore_bundle()
						{
							if(document.getElementById("auto_fornitore_bundle").value > 0)
								return document.getElementById("auto_fornitore_bundle").value;
							else
								return 0;
						}
						
						function getCategoria_bundle()
						{
							if(document.getElementById("auto_categoria_bundle").value > 0)
								return document.getElementById("auto_categoria_bundle").value;
							else
								return 0;
						}
						
						function repopulateSeries(id_manufacturer)
						{
							$("#auto_serie_bundle").empty();
							$.ajax({
							  url:"ajax.php?repopulate_series=y",
							  type: "POST",
							  data: { id_manufacturer: id_manufacturer
							  },
							  success:function(resp){  
								var newOptions = $.parseJSON(resp);
								
								 $("#auto_serie_bundle").append($("<option></option>")
									 .attr("value", "0").text("Serie..."));
								
								$.each(newOptions, function(key,value) {
								  $("#auto_serie_bundle").append($("<option></option>")
									 .attr("value", value).text(key));
								});
								
								$("#auto_serie_bundle").select2({
									placeholder: "Serie..."
								});
							  },
							  error: function(xhr,stato,errori){
							  }
							});
								
							
						}	
									
								function clearAutoComplete()
						{
							$("#veditutti_bundle").trigger("click");
							
						}		
									
					</script>
			<br /><br />
				<script type="text/javascript">
						var formProduct = "";
						var products = new Array();
					</script>
					<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
					
					
					<script type="text/javascript">
					
					
					
						urlToCall = null;
						
						/* function autocomplete */
						$(function() {
							$(\'#product_autocomplete_input_bundle\')
								.autocomplete(\'ajax_products_list2.php?get_bdls=y&products_and_bundles=y&id_customer='.Tools::getValue('id_customer').'\', {
									minChars: 0,
									autoFill: false,
									max:500,
									matchContains: true,
									mustMatch:true,
									scroll:false,
									cacheLength:0,
									formatItem: function(item) {
								
												var color = "";
												if(item[24] == 1) {
													var color = "; color:red";
												}
												if(item[28] == 0) {
													return \'</table><br /><div onclick="return false;" style="position:absolute; z-index:99997; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:180px;float:left;margin-left:10px;">Codice</div></th><th style="width:630px;"><div style="width:200px; float:left;">Desc. prodotto<br /><br /></div></th><th style="width:60px; text-align:right"><div style="width:45px;float:left;">Prezzo</div></th><th style="width:35px; text-align:right"><div style="width:30px;float:left; text-align:right">EZ</div></th><th style="width:35px; text-align:right"><div style="width:40px;float:left; text-align:right">ESP</div></th><th style="width:35px; text-align:right"><div style="width:30px;float:left; text-align:right">INT</div></th><th style="width:35px; text-align:right"><div style="width:30px;float:left; text-align:right">ALN</div></th><th style="width:30px; text-align:right"><div style="width:30px;float:left; text-align:right">ITA</div></th><th style="width:30px; text-align:right"><div style="width:30px;float:left; text-align:right">ASI</div></th><th style="width:30px; text-align:right"><div style="width:30px;float:left; text-align:right">Imp</div></th><th style="width:30px; text-align:right"><div style="width:30px;float:left; text-align:center">Tot</div></th></tr></div><table>\'
													
													;
												}
												else {
													return \'<tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:350px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:40px; text-align:right">\'+item[25]+\'</td><td style="width:40px; text-align:right">\'+item[33]+\'</td><td style="width:40px; text-align:right">\'+item[49]+\'</td><td style="width:40px; text-align:right">\'+item[34]+\'</td><td style="width:40px; text-align:right">\'+item[38]+\'</td><td style="width:40px; text-align:right">\'+item[50]+\'</td><td style="width:40px; text-align:right">\'+item[37]+\'</td><td style="width:40px; text-align:right">\'+item[22]+\'</td></tr>\'
													
													;
													}
												}
									
								}).result(addProduct_TR);
					
						});
						
						$("#product_autocomplete_input_bundle").keypress(function(event){
						
									  var keycode = (event.keyCode ? event.keyCode : event.which);
									  if (keycode == "13") {
										event.preventDefault();
										event.stopPropagation();    
									  }
									});
					</script>
					
			<table width="100%" class="table" id="tableProducts">
				<thead><tr>
				
					<th style="width:45px">ID</th>
					<th style="width:130px">Codice articolo</th>
					<th style="width:370px">Nome prodotto</th>
					<th style="width:30px; text-align:right">Qt.</th>
					<th style="width:195px; text-align:right">Prezzo CLI</th>
					<th style="width:195px; text-align:right">Prezzo RIV</th>
					<th style="width:55px">Canc.</th>
				
				</tr></thead><tbody>';
				
				if(isset($_GET['updatebundle'])) {

					$bundle_prices = $bundle['bundle_prices'];
					
					$bundle_products = explode(":", $bundle_prices);
					$totale = 0;
					$totaleriv = 0;
					
					foreach($bundle_products as $bundle_product) {
					
						if($bundle_product == "") { } else {
							$bundle_prod = explode(";", $bundle_product);
							$prezzo_prd_cli = Product::trovaMigliorPrezzo($bundle_prod[0],1,$bundle_prod[1]);
							$prezzo_base_cli = number_format($prezzo_prd_cli, 2,",","");
							$prezzo_prd_cli = $prezzo_prd_cli - (($prezzo_prd_cli * (str_replace(",",".",$bundle_prod[2])))/100);
							$prezzo_prd_cli = number_format($prezzo_prd_cli, 2,",","");
							
							$prezzo_prd_riv = Product::trovaMigliorPrezzo($bundle_prod[0],3,$bundle_prod[1]);
							$prezzo_base_riv = number_format($prezzo_prd_riv, 2,",","");
							$prezzo_prd_riv = $prezzo_prd_riv - (($prezzo_prd_riv * (str_replace(",",".",$bundle_prod[3])))/100);
							$prezzo_prd_riv = number_format($prezzo_prd_riv, 2,",","");
							
							echo '
							<tr id="tr_'.$bundle_prod[0].'">
								<td>'.$bundle_prod[0].'</td>
								<td><a href="index.php?tab=AdminCatalog&id_product='.$bundle_prod[0].'&updateproduct&token='.$this->token.'" target="_blank" style="cursor:pointer"><span class="span-reference" title="'.Product::showProductTooltip($bundle_prod[0]).'">'.Db::getInstance()->getValue("SELECT reference FROM product WHERE id_product = ".$bundle_prod[0]."").'</span></a></td>
								
								<td>'.Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_lang = 5 AND id_product = ".$bundle_prod[0]."").'</td>
								
								<td><input style="text-align:right" name="nuovo_quantity['.$bundle_prod[0].']]" id="product_quantity['.$bundle_prod[0].']" type="text" size="3" value="'.$bundle_prod[1].'" onkeyup="if (isArrowKey(event)) return; calcolaImporto('.$bundle_prod[0].');" /></td>';
								$sc_acq_1_b = Db::getInstance()->getValue("SELECT sconto_acquisto_1 FROM product WHERE id_product = ".$bundle_prod[0]."");
								
								$sc_acq_2_b = Db::getInstance()->getValue("SELECT sconto_acquisto_2 FROM product WHERE id_product = ".$bundle_prod[0]."");
								$sc_acq_3_b = Db::getInstance()->getValue("SELECT sconto_acquisto_3 FROM product WHERE id_product = ".$bundle_prod[0]."");
								$listino_b = Db::getInstance()->getValue("SELECT listino FROM product WHERE id_product = ".$bundle_prod[0]."");
								$wholesale_b = Db::getInstance()->getValue("SELECT wholesale_price FROM product WHERE id_product = ".$bundle_prod[0]."");
								$wholesale_price = ($wholesale_b > 0 ? $wholesale_b : ($listino_b*(100-$sc_acq_1_b)/100)*((100-$sc_acq_2_b)/100)*((100-$sc_acq_3_b)/100));
								
								$wholesale_price = Product::trovaMigliorPrezzoAcquisto($bundle_prod[0], 1, 1, 'y');
								
								echo '<input type="hidden" name="wholesale_price['.$bundle_prod[0].']" id="wholesale_price['.$bundle_prod[0].']" value="'.number_format(round($wholesale_price,2), 2, ',', '').'" />';
									
								$sc_qta_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_1' AND id_product = '".$bundle_prod[0]."'");
								$sc_qta_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_2' AND id_product = '".$bundle_prod[0]."'");
								$sc_qta_3_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_3' AND id_product = '".$bundle_prod[0]."'");	
								$sc_riv_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_1' AND id_product = '".$bundle_prod[0]."'");	
								$sc_riv_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_2' AND id_product = '".$bundle_prod[0]."'");
								$sc_riv_3_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_3' AND id_product = '".$bundle_prod[0]."'");
										
								$sc_qta_1 = $sc_qta_1_r['price'] - ($sc_qta_1_r['price'] * $sc_qta_1_r['reduction']);
								$sc_qta_2 = $sc_qta_2_r['price'] - ($sc_qta_2_r['price'] * $sc_qta_2_r['reduction']);
								$sc_qta_3 = $sc_qta_3_r['price'] - ($sc_qta_3_r['price'] * $sc_qta_3_r['reduction']);
								$sc_riv_1 = $sc_riv_1_r['price'] - ($sc_riv_1_r['price'] * $sc_riv_1_r['reduction']);
								$sc_riv_2 = $sc_riv_2_r['price'] - ($sc_riv_2_r['price'] * $sc_riv_2_r['reduction']);
								$sc_riv_3 = $sc_riv_3_r['price'] - ($sc_riv_3_r['price'] * $sc_riv_3_r['reduction']);
								
								$speciale = Product::trovaPrezzoSpeciale($bundle_prod[0], 1, 0);
								$unitario = Db::getInstance()->getValue("SELECT price FROM product WHERE id_product = '".$bundle_prod[0]."'");
								
								if($speciale < $unitario && $speciale != 0) {
									$unitario = $speciale;
								}
										
								
										
								$sc_qta_1_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_1' AND id_product = '".$bundle_prod[0]."'");
								$sc_qta_2_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_2' AND id_product = '".$bundle_prod[0]."'");
								$sc_qta_3_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_3' AND id_product = '".$bundle_prod[0]."'");
										
								$sc_riv_1_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_1' AND id_product = '".$bundle_prod[0]."'");
								$sc_riv_2_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_2' AND id_product = '".$bundle_prod[0]."'");	
								$sc_riv_3_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_3' AND id_product = '".$bundle_prod[0]."'");	
									
								echo '<input type="hidden" id="unitario['.$bundle_prod[0].']" value="'.$unitario.'" />';
								echo '<input type="hidden" id="sc_qta_1['.$bundle_prod[0].']" value="'.$sc_qta_1.'" />';
								echo '<input type="hidden" id="sc_qta_2['.$bundle_prod[0].']" value="'.$sc_qta_2.'" />';
								echo '<input type="hidden" id="sc_qta_3['.$bundle_prod[0].']" value="'.$sc_qta_3.'" />';
								echo '<input type="hidden" id="sc_riv_1['.$bundle_prod[0].']" value="'.$sc_riv_1.'" />';
								echo '<input type="hidden" id="sc_riv_2['.$bundle_prod[0].']" value="'.$sc_riv_2.'" />';
								echo '<input type="hidden" id="sc_riv_3['.$bundle_prod[0].']" value="'.$sc_riv_3.'" />';
				
								echo '<input type="hidden" id="sc_qta_1_q['.$bundle_prod[0].']" value="'.$sc_qta_1_q.'" />';
								echo '<input type="hidden" id="sc_qta_2_q['.$bundle_prod[0].']" value="'.$sc_qta_2_q.'" />';
								echo '<input type="hidden" id="sc_qta_3_q['.$bundle_prod[0].']" value="'.$sc_qta_3_q.'" />';
								echo '<input type="hidden" id="sc_riv_1_q['.$bundle_prod[0].']" value="'.$sc_riv_1_q.'" />';
								echo '<input type="hidden" id="sc_riv_2_q['.$bundle_prod[0].']" value="'.$sc_riv_2_q.'" />';	
								echo '<input type="hidden" id="sc_riv_3_q['.$bundle_prod[0].']" value="'.$sc_riv_3_q.'" />';	
									
								$totaleacquisti += $wholesale_price*1;
								$prezzo_partenza = (float)str_replace(",",".",$prezzo_prd_cli);
								$priceclib = (float)str_replace(",",".",$prezzo_prd_cli)*$bundle_prod[1];
								$prezzoriv = (float)str_replace(",",".",$prezzo_prd_riv)*$bundle_prod[1];
										
								$marginalita = ((($prezzo_partenza - $wholesale_price)*100)/$prezzo_partenza);
								$marginalitariv = ((($prezzoriv - $wholesale_price)*100)/$prezzoriv);
										
								
								echo '<td style="text-align:right">';
								echo '<table style="width:100%">
								<tr>
								<td>Prz. base</td>
								<td>
								<span '.(Product::checkPrezzoSpeciale($bundle_prod[0],1,1) == 'yes' ? 'style="color:red"' : '').' id="prz_vis_cli['.$bundle_prod[0].']">'.$prezzo_base_cli.'</span><span '.(Product::checkPrezzoSpeciale($bundle_prod[0],1,1) == 'yes' ? 'style="color:red"' : '').'> &euro; </span></td>
								</tr>
								
								<tr>
								<td>Sconto %</td>
								<td>
								<input type="hidden" id="product_price['.$bundle_prod[0].']" name="nuovo_price['.$bundle_prod[0].']" value="'.$prezzo_prd_cli.'" />
								
								<input type="text" style="text-align:right" id="product_sconto['.$bundle_prod[0].']" size="4" name="nuovo_sconto['.$bundle_prod[0].']" value="'.$bundle_prod[2].'" onkeyup="javascript: calcolaImporto('.$bundle_prod[0].');"  />
								</td></tr>
								
								<tr>
								<td>Prz. nel kit</td>
								<td>
								<span id="prz_nel_bundle_cli['.$bundle_prod[0].']">'.$prezzo_prd_cli.'</span> &euro; </td>
								</tr>
								<tr>
								<td>Marg.</td>
								<td>
								<span id="spanmarginalita['.$bundle_prod[0].']">'.number_format($marginalita, 2, ',', '').'%</span>
								<input id="impImporto['.$bundle_prod[0].']" class="importo" type="hidden" value="'.$priceclib.'" />
								</td>
								</tr>
								</table>
								
								</td>
								
								<td style="text-align:right">
								<table style="width:100%">
								<tr>
								<td>Prz. base</td>
								<td>
								<span id="prz_vis_riv['.$bundle_prod[0].']">'.$prezzo_base_riv.'</span> &euro; </td>
								</tr>
								
								<tr>
								<td>Sconto %</td>
								<td>
								<input type="hidden" id="product_priceriv['.$bundle_prod[0].']" name="nuovo_priceriv['.$bundle_prod[0].']" value="'.$prezzo_prd_riv.'" />
								
								<input type="text" style="text-align:right" size="4" id="product_scontoriv['.$bundle_prod[0].']" name="nuovo_scontoriv['.$bundle_prod[0].']" value="'.$bundle_prod[3].'" onkeyup="javascript: calcolaImporto('.$bundle_prod[0].');"  />
								
								</td></tr>
								<tr>
								<td>Prz. nel kit</td>
								<td>
								<span id="prz_nel_bundle_riv['.$bundle_prod[0].']">'.$prezzo_prd_riv.'</span> &euro; </td>
								</tr>
								<tr>
								<td>Marg.</td>
								<td><span id="spanmarginalitariv['.$bundle_prod[0].']">'.number_format($marginalitariv, 2, ',', '').'%</span>
								<input id="impImportoRiv['.$bundle_prod[0].']" class="importoriv" type="hidden" value="'.$prezzoriv.'" />
								</td>
								</tr>
								
								</table>
								
								</td>
								
										
								<td style="text-align:center">
								<input name="nuovo_prodotto['.$bundle_prod[0].']" type="hidden" value="'.$bundle_prod[0].'" />
								'.($bundle_prod[0] == $bundle['father'] ? '<strong>PADRE</strong>' : '<a style="cursor:pointer" onclick="togliImporto('.$bundle_prod[0].'); delProduct30('.$bundle_prod[0].')"><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a>').'
								</td>
							</tr>';
							$totale += $priceclib;
							$totaleriv += $prezzoriv;
						}
					}
				}
				
				else {
				
					$sc_qta_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_1' AND id_product = '".$obj->id."'"); $sc_qta_1 = $sc_qta_1_r['price'] - ($sc_qta_1_r['price'] * $sc_qta_1_r['reduction']);
					$sc_qta_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_2' AND id_product = '".$obj->id."'"); $sc_qta_2 = $sc_qta_2_r['price'] - ($sc_qta_2_r['price'] * $sc_qta_2_r['reduction']);
					$sc_qta_3_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_3' AND id_product = '".$obj->id."'");	$sc_qta_3 = $sc_qta_3_r['price'] - ($sc_qta_3_r['price'] * $sc_qta_3_r['reduction']);
					$sc_riv_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_1' AND id_product = '".$obj->id."'");	$sc_riv_1 = $sc_riv_1_r['price'] - ($sc_riv_1_r['price'] * $sc_riv_1_r['reduction']);	
					$sc_riv_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_2' AND id_product = '".$obj->id."'");	$sc_riv_2 = $sc_riv_2_r['price'] - ($sc_riv_2_r['price'] * $sc_riv_2_r['reduction']);
					$sc_riv_3_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_3' AND id_product = '".$obj->id."'");	$sc_riv_3 = $sc_riv_3_r['price'] - ($sc_riv_3_r['price'] * $sc_riv_3_r['reduction']);
					
					$prezzoriv = Product::trovaMigliorPrezzo($this->getFieldValue($obj, 'id'),3);
					
					echo '
					<tr id="tr_'.$obj->id.'">
						<td>'.stripslashes(htmlspecialchars($this->getFieldValue($obj, 'id'))).'</td>
						<td><span  class="span-reference" title="'.Product::showProductTooltip(($this->getFieldValue($obj, 'id'))).'"><a href="index.php?tab=AdminCatalog&id_product='.$this->getFieldValue($obj, 'id').'&updateproduct&token='.$this->token.'" target="_blank" style="cursor:pointer">'.stripslashes(htmlspecialchars($this->getFieldValue($obj, 'reference'))).'</a></span></td>
						<td>'.stripslashes(htmlspecialchars($this->getFieldValue($obj, 'name', 5))).'</td>
						
						<td><input style="text-align:right" name="nuovo_quantity['.trim($obj->id).']]" id="product_quantity['.trim($obj->id).']" type="text" size="1" value="1" onkeyup="if (isArrowKey(event)) return; calcolaImporto('.trim($obj->id).');" />
					
						<input type="hidden" name="sconto_acquisto_1['.$obj->id.']" id="sconto_acquisto_1['.$obj->id.']" value="'.$obj->sconto_acquisto_1.'" />
						<input type="hidden" name="sconto_acquisto_2['.$obj->id.']" id="sconto_acquisto_2['.$obj->id.']" value="'.$obj->sconto_acquisto_2.'" />
						<input type="hidden" name="sconto_acquisto_3['.$obj->id.']" id="sconto_acquisto_3['.$obj->id.']" value="'.$obj->sconto_acquisto_3.'" />';
						$wholesale_price = ($obj->wholesale_price > 0 ? $obj->wholesale_price : ($obj->listino*(100-$obj->sconto_acquisto_1)/100)*((100-$obj->sconto_acquisto_2)/100)*((100-$obj->sconto_acquisto_3)/100));
						
						$wholesale_price = Product::trovaMigliorPrezzoAcquisto($obj->id, 1, 1, 'y');
						
						echo '<input type="hidden" name="wholesale_price['.$obj->id.']" id="wholesale_price['.$obj->id.']" value="'.number_format(round($wholesale_price,2), 2, ',', '').'" />';
						$sc_qta_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_1' AND id_product = '".$obj->id."'");
						$sc_qta_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_2' AND id_product = '".$obj->id."'");
						$sc_qta_3_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_3' AND id_product = '".$obj->id."'");	
						$sc_riv_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_1' AND id_product = '".$obj->id."'");	
						$sc_riv_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_2' AND id_product = '".$obj->id."'");
						$sc_riv_3_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_3' AND id_product = '".$obj->id."'");
										
						$sc_qta_1 = $sc_qta_1_r['price'] - ($sc_qta_1_r['price'] * $sc_qta_1_r['reduction']);
						$sc_qta_2 = $sc_qta_2_r['price'] - ($sc_qta_2_r['price'] * $sc_qta_2_r['reduction']);
						$sc_qta_3 = $sc_qta_3_r['price'] - ($sc_qta_3_r['price'] * $sc_qta_3_r['reduction']);
						$sc_riv_1 = $sc_riv_1_r['price'] - ($sc_riv_1_r['price'] * $sc_riv_1_r['reduction']);
						$sc_riv_2 = $sc_riv_2_r['price'] - ($sc_riv_2_r['price'] * $sc_riv_2_r['reduction']);
						$sc_riv_3 = $sc_riv_3_r['price'] - ($sc_riv_3_r['price'] * $sc_riv_3_r['reduction']);
										
						$unitario = Db::getInstance()->getValue("SELECT price FROM product WHERE id_product = '".$obj->id."'");
						
						$speciale = Product::trovaPrezzoSpeciale($obj->id, 1, 0);
			
						if($speciale < $unitario && $speciale != 0) {
					
							$unitario = $speciale;
			
						}
						else {
							
						}
										
						$sc_qta_1_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_1' AND id_product = '".$obj->id."'");
						$sc_qta_2_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_2' AND id_product = '".$obj->id."'");
						$sc_qta_3_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_3' AND id_product = '".$obj->id."'");
										
						$sc_riv_1_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_1' AND id_product = '".$obj->id."'");
						$sc_riv_2_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_2' AND id_product = '".$obj->id."'");	
						$sc_riv_3_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_3' AND id_product = '".$obj->id."'");	
									
						echo '<input type="hidden" id="unitario['.$obj->id.']" value="'.$unitario.'" />';
						echo '<input type="hidden" id="sc_qta_1['.$obj->id.']" value="'.$sc_qta_1.'" />';
						echo '<input type="hidden" id="sc_qta_2['.$obj->id.']" value="'.$sc_qta_2.'" />';
						echo '<input type="hidden" id="sc_qta_3['.$obj->id.']" value="'.$sc_qta_3.'" />';
						echo '<input type="hidden" id="sc_riv_1['.$obj->id.']" value="'.$sc_riv_1.'" />';
						echo '<input type="hidden" id="sc_riv_2['.$obj->id.']" value="'.$sc_riv_2.'" />';
						echo '<input type="hidden" id="sc_riv_3['.$obj->id.']" value="'.$sc_riv_3.'" />';
				
						echo '<input type="hidden" id="sc_qta_1_q['.$obj->id.']" value="'.$sc_qta_1_q.'" />';
						echo '<input type="hidden" id="sc_qta_2_q['.$obj->id.']" value="'.$sc_qta_2_q.'" />';
						echo '<input type="hidden" id="sc_qta_3_q['.$obj->id.']" value="'.$sc_qta_3_q.'" />';
						echo '<input type="hidden" id="sc_riv_1_q['.$obj->id.']" value="'.$sc_riv_1_q.'" />';
						echo '<input type="hidden" id="sc_riv_2_q['.$obj->id.']" value="'.$sc_riv_2_q.'" />';	
						echo '<input type="hidden" id="sc_riv_3_q['.$obj->id.']" value="'.$sc_riv_3_q.'" />';	
							
						$totaleacquisti += $wholesale_price*1;
						$prezzo_partenza = (Product::checkPrezzoSpeciale($this->getFieldValue($obj, 'id'),1,1) == 'yes' ? number_format(Product::trovaPrezzoSpeciale($obj->id,1,1),2,",","") : number_format($obj->price,2,",",""));
						$priceclib = (Product::checkPrezzoSpeciale($this->getFieldValue($obj, 'id'),1,1) == 'yes' ? number_format(Product::trovaPrezzoSpeciale($obj->id,1,1),2,",","") : number_format($obj->price,2,",",""));
								
						$marginalita = ((($prezzo_partenza - $wholesale_price)*100)/$prezzo_partenza);
						$marginalitariv = ((($prezzoriv - $wholesale_price)*100)/$prezzoriv);
						
						echo '</td>';
						
						echo '<td style="text-align:right">';
								echo '<table style="width:100%">
								<tr>
								<td>Prz. base</td>
								<td>
								<span '.(Product::checkPrezzoSpeciale($this->getFieldValue($obj, 'id'),1,1) == 'yes' ? 'style="color:red"' : '').' id="prz_vis_cli['.$obj->id.']">'.(Product::checkPrezzoSpeciale($this->getFieldValue($obj, 'id'),1,1) == 'yes' ? number_format(Product::trovaPrezzoSpeciale($obj->id,1,1),2,",","") : number_format($obj->price,2,",","")).'</span><span '.(Product::checkPrezzoSpeciale($this->getFieldValue($obj, 'id'),1,1) == 'yes' ? 'style="color:red"' : '').'"> &euro;</span></td>
								</tr>
								
								<tr>
								<td>Sconto %</td>
								<td>
								<input type="hidden" id="product_price['.$obj->id.']" name="nuovo_price['.$obj->id.']" value="'.(Product::checkPrezzoSpeciale($this->getFieldValue($obj, 'id'),1,1) == 'yes' ? Product::trovaPrezzoSpeciale($obj->id,1,1) : $obj->price).'" />
								
								<input type="text" style="text-align:right" size="4" id="product_sconto['.$obj->id.']" name="nuovo_sconto['.$obj->id.']" value="0,00" onkeyup="javascript: calcolaImporto('.$obj->id.');"  />
								</td></tr>
								
								<tr>
								<td>Prz. nel kit</td>
								<td>
								<span id="prz_nel_bundle_cli['.$obj->id.']">'.(Product::checkPrezzoSpeciale($this->getFieldValue($obj, 'id'),1,1) == 'yes' ? number_format(Product::trovaPrezzoSpeciale($obj->id,1,1),2,",","") : number_format($obj->price,2,",","")).'</span> &euro; </td>
								</tr>
								<tr>
								<td>Marg.</td>
								<td>
								<span id="spanmarginalita['.$obj->id.']">'.number_format($marginalita, 2, ',', '').'%</span>
								<input id="impImporto['.$obj->id.']" class="importo" type="hidden" value="'.$priceclib.'" />
								</td>
								</tr>
								</table>
								
								</td>
								
								<td style="text-align:right">
								<table style="width:100%">
								<tr>
								<td>Prz. base</td>
								<td>
								<span id="prz_vis_riv['.$obj->id.']">'.number_format($prezzoriv,2,",","").'</span> &euro; </td>
								</tr>
								
								<tr>
								<td>Sconto %</td>
								<td>
								<input type="hidden" id="product_priceriv['.$obj->id.']" name="nuovo_priceriv['.$obj->id.']" value="'.$prezzoriv.'" />
								
								<input type="text" style="text-align:right" size="4" id="product_scontoriv['.$obj->id.']" name="nuovo_scontoriv['.$obj->id.']" value="0,00" onkeyup="javascript: calcolaImporto('.$obj->id.');"  />
								
								</td></tr>
								<tr>
								<td>Prz. nel kit</td>
								<td>
								<span id="prz_nel_bundle_riv['.$obj->id.']">'.number_format($prezzoriv, 2, ',', '').'</span> &euro; </td>
								</tr>
								<tr>
								<td>Marg.</td>
								<td><span id="spanmarginalitariv['.$obj->id.']">'.number_format($marginalitariv, 2, ',', '').'%</span>
								<input id="impImportoRiv['.$obj->id.']" class="importoriv" type="hidden" value="'.$prezzoriv.'" />
								</td>
								</tr>
								
								</table>
								
								</td>';
			
						
								
								echo '
						<td style="text-align:center"><strong>PADRE</strong>
						<input name="nuovo_prodotto['.$obj->id.']" type="hidden" value="'.$obj->id.'" />
						</td>
					</tr>';
					$totale = $priceclib;
					$totaleriv = $prezzoriv;
				}
			
			echo '</tbody>';
			echo '<tfoot><tr><td></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td style="text-align:right">';
		
			$marginalitatotale = ((($totale - $totaleacquisti)*100) / $totale);
			echo "<input type='hidden' value='".$totale."' name='totaleprodotti' id='totaleprodotti' />
			<input type='hidden' value='".$totaleriv."' name='totaleprodottiriv' id='totaleprodottiriv' />
			TOT. kit CLI: <span id='spantotaleprodotti'>".number_format($totale,2, ',', '')."</span> &euro;<br />
			<span style='visibility:hidden' id='marginalitatotale'>".number_format($marginalitatotale,2, ',', '')."%</span>
			</td><td style='text-align:right'>
			TOT. kit RIV: <span id='spantotaleprodottiriv'>".number_format($totaleriv,2, ',', '')."</span>  &euro;<br />
			
			<span style='visibility:hidden' id='marginalitatotaleriv'>".number_format($marginalitatotaleriv,2, ',', '')."%</span>
			</td><td style='text-align:right'>
			
			</td></tr></tfoot>";
			
			echo '</table>
			';
			$rowcomparabundle = Db::getInstance()->getRow("SELECT trovaprezzi, google_shopping, comparaprezzi_check FROM bundle WHERE id_bundle = $_GET[id_bundle]");
				echo '
				<strong>Estensione garanzia</strong><br />';
				echo '<script type="text/javascript" src="../js/select2.js"></script>
					<script type="text/javascript">
					$(document).ready(function() { $("#estensione_garanzia").select2(); });
					</script>';
				echo '<select id="estensione_garanzia" name="estensione_garanzia" style="width:600px">
					<option name="0" value="0">--- Scegli un prodotto ---</option>
					';
					
					
					$results = Db::getInstance()->ExecuteS("SELECT DISTINCT p.id_product, p.reference, p.price, pl.name FROM product p LEFT JOIN product_lang pl ON p.id_product = pl.id_product WHERE (pl.name LIKE '%Estensione%') AND pl.id_lang = 5 GROUP BY p.id_product");
					
					foreach ($results as $row) {
					
						echo "<option name='$row[reference]' ".($bundle['estensione_garanzia'] == $row['id_product'] ? "selected='selected'" : "")." value='$row[id_product]'"; 
						echo ">$row[name] ($row[reference])  - ".Tools::displayPrice($row['price'], 1, false)."</option>";
					
					}
						
					echo "</select>&nbsp;&nbsp;&nbsp;<br /><br />
					";
					
							echo '
				<strong>Supporto da remoto</strong><br />';
				echo '
					<script type="text/javascript">
					$(document).ready(function() { $("#supporto_da_remoto").select2(); });
					</script>';
			echo '<select id="supporto_da_remoto" name="supporto_da_remoto" style="width:600px">
					<option name="0" value="0">--- Scegli un prodotto ---</option>
					';
					
					
					$results = Db::getInstance()->ExecuteS("SELECT DISTINCT p.id_product, p.reference, p.price, pl.name FROM product p LEFT JOIN product_lang pl ON p.id_product = pl.id_product WHERE (pl.name LIKE '%assistenza%') AND pl.id_lang = 5 GROUP BY p.id_product");
					
					foreach ($results as $row) {
					
						echo "<option name='$row[reference]' ".($bundle['supporto_da_remoto'] == $row['id_product'] ? "selected='selected'" : "")." value='$row[id_product]'"; 
						echo ">$row[name] ($row[reference])  - ".Tools::displayPrice($row['price'], 1, false)."</option>";
					
					}
						
					echo "</select>&nbsp;&nbsp;&nbsp;<br /><br />
					";
					
					echo '
				<strong>Telegestione</strong><br />';
				echo '
					<script type="text/javascript">
					$(document).ready(function() { $("#telegestione").select2(); });
					</script>';
			echo '<select id="telegestione" name="telegestione" style="width:600px">
					<option name="0" value="0">--- Scegli un prodotto ---</option>
					';
					
					
					$results = Db::getInstance()->ExecuteS("SELECT DISTINCT p.id_product, p.reference, p.price, pl.name FROM product p LEFT JOIN product_lang pl ON p.id_product = pl.id_product WHERE (pl.name LIKE '%teleassist%') AND pl.id_lang = 5 GROUP BY p.id_product");
					
					foreach ($results as $row) {
					
						echo "<option name='$row[reference]' ".($bundle['telegestione'] == $row['id_product'] ? "selected='selected'" : "")." value='$row[id_product]'"; 
						echo ">$row[name] ($row[reference])  - ".Tools::displayPrice($row['price'], 1, false)."</option>";
					
					}
						
					echo "</select>&nbsp;&nbsp;&nbsp;<br /><br />
					";
					
					if(Tools::getIsset('id_bundle')) {
						echo "<strong>Categoria/e del kit</strong>";
						$default_category = $bundle['id_category_default'];
						$selected_categories = array();
						
						$bundle_categories = Db::getInstance()->executeS("SELECT id_category FROM category_bundle WHERE id_bundle = ".$bundle['id_bundle']."");
						foreach($bundle_categories as $bdlcat)
							$selected_categories[] = $bdlcat['id_category'];
						
						$selected_categories[] = $default_category;
								
								$selectedCat = Category::getCategoryInformations($selected_categories, 5);
								echo '
								<script type="text/javascript">
									post_selected_cat = \''.implode(',', array_keys($selectedCat)).'\';
								</script>';
								

								echo '<select id="id_category_default" name="id_category_default">';
								foreach($selectedCat AS $cat)
									echo '<option value="'.$cat['id_category'].'" '.($bundle['id_category_default'] == $cat['id_category'] ? 'selected' : '').'>'.$cat['name'].'</option>';
								echo '</select>
								
							
								';
							// Translations are not automatic for the moment ;)
							$trads = array(
								 'Home' => $this->l('Home'),
								 'selected' => $this->l('selected'),
								 'Collapse All' => $this->l('Collapse All'),
								 'Expand All' => $this->l('Expand All'),
								 'Check All' => $this->l('Check All'),
								 'Uncheck All'  => $this->l('Uncheck All')
							);
							echo Helper::renderAdminCategorieTree($trads, $selectedCat).'
								';
					}
				echo "
				<strong>Seleziona i comparaprezzi su cui esportare il bundle:</strong>
				";
				if(!isset($_GET['updatebundle'])) {
					echo "
					<fieldset id='comparaprezzifields_bundle' style='border:0px'> 
					<input type='checkbox' name='trovaprezzi_add_bundle' /> Trovaprezzi<br />
					<input type='checkbox' name='google_shopping_add_bundle' /> Google Shopping<br /><br />
					
					<input type='checkbox' name='bundle_amazon_it' />Amazon.it
					<input type='checkbox' name='bundle_amazon_fr' />Amazon.fr
					<input type='checkbox' name='bundle_amazon_es' />Amazon.es
					<input type='checkbox' name='bundle_amazon_uk' />Amazon.co.uk
					<input type='checkbox' name='bundle_amazon_de' />Amazon.de
					<input type='checkbox' name='bundle_amazon_nl' />Amazon.nl
					<input type='checkbox' name='bundle_amazon_se' />Amazon.se
					<input type='checkbox' name='bundle_amazon_pl' />Amazon.pl
					<br /><br />
					<input type='checkbox' onclick=\"checkUncheckSome('tutti_add_bundle','comparaprezzifields_bundle')\" id='tutti_add_bundle' name='tutti_add_bundle' /> Tutti<br /></fieldset>
					</fieldset>
					";

				} else {

					$amazon_check = explode(";",Db::getInstance()->getValue('SELECT amazon FROM bundle WHERE id_bundle = '.$_GET['id_bundle']));
					
					echo "
					<fieldset id='comparaprezzifields_bundle' style='border:0px'> 
					
					<input type='checkbox' name='trovaprezzi_add_bundle'"; if($rowcomparabundle['trovaprezzi'] == 1) { echo "checked='checked'"; } else if($rowcomparabundle['trovaprezzi'] == 0)  { }
					else { echo "checked='checked'"; } echo " /> Trovaprezzi<br />
					<input type='checkbox' name='google_shopping_add_bundle'"; if($rowcomparabundle['google_shopping'] == 1) { echo "checked='checked'"; } else if($rowcomparabundle['google_shopping'] == 0)  { }
					else { echo "checked='checked'"; } echo " /> Google Shopping<br /><br />

					<input type='checkbox' name='bundle_amazon_it' ".(in_array("5",$amazon_check) ? "checked='checked'" : '')." />Amazon.it
					<input type='checkbox' name='bundle_amazon_fr' ".(in_array("2",$amazon_check) ? "checked='checked'" : '')." />Amazon.fr
					<input type='checkbox' name='bundle_amazon_es' ".(in_array("3",$amazon_check) ? "checked='checked'" : '')." />Amazon.es
					<input type='checkbox' name='bundle_amazon_uk' ".(in_array("1",$amazon_check) ? "checked='checked'" : '')." />Amazon.co.uk
					<input type='checkbox' name='bundle_amazon_de' ".(in_array("4",$amazon_check) ? "checked='checked'" : '')." />Amazon.de
					<input type='checkbox' name='bundle_amazon_nl' ".(in_array("6",$amazon_check) ? "checked='checked'" : '')." />Amazon.nl
					<input type='checkbox' name='bundle_amazon_se' ".(in_array("7",$amazon_check) ? "checked='checked'" : '')." />Amazon.se
					<input type='checkbox' name='bundle_amazon_pl' ".(in_array("8",$amazon_check) ? "checked='checked'" : '')." />Amazon.pl
					<br /><br />
					<input type='checkbox' onclick=\"checkUncheckSome('tutti_add_bundle','comparaprezzifields_bundle')\" id='tutti_add_bundle' name='tutti_add_bundle' /> Tutti<br /></fieldset>
					</fieldset>



					<br /><br />

					";

				

				}
			
			echo "<br />
			<input type='hidden' name='id_product' value='".$obj->id_product."' />
			<input type='hidden' name='id_tabs' value='3' />
			<p style='text-align:center'>";
			
			if(isset($_GET['updatebundle'])) {
				echo "<input type='submit' class='button' name='submitupdatedbundle' value='Salva kit' />&nbsp;&nbsp;&nbsp;";
			} else {
				echo "<input type='submit' class='button' name='submitnewbundle' value='Salva kit' />&nbsp;&nbsp;&nbsp;";
			}
			
			echo '<a href="'.$currentIndex.'&id_product='.$obj->id.'&updateproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'&submitAddAndStay=on&tabs=3" class="button">Torna alla lista dei kit di questo prodotto</a>
			
			</p></div>';
		} 
		
		else {
		
		
			echo "
		
			<h3>Kit esistenti per questo prodotto</h3>";
					
			$bundles = Db::getInstance()->executeS("SELECT * FROM bundle WHERE father = ".$_GET['id_product']." ORDER BY sort_order ASC");
			$bundles_current = array();
			
			foreach($bundles as $bundle) {
			
				$bundle_products = explode(";", $bundle['bundle_products']);
				
				if(in_array($obj->id, $bundle_products)) {
				
					$bundles_current[] = $bundle;
				
				}
			}
			
			if(!empty($bundles_current)) {
				
				
			
				echo '
				<script type="text/javascript">
				function activeBundle(id_bundle, active)
				{
					var id_bundle = id_bundle;
					var active = active;
					
					$.ajax({
					  url:"ajax.php?activeBundle=ok",
					  type: "POST",
					  data: { id_bundle: id_bundle, active:active, 
					  id_product: "'.$obj->id.'"
					  },
					  success:function(r){
						if(r == 0)
						{
							$("#active_bundle_"+id_bundle).html("<img src=\"../img/admin/disabled.gif\" alt=\"Non attivo\" title=\"Non Attivo\" style=\"cursor:pointer\" onclick=\"activeBundle("+id_bundle+",1)\" />").fadeIn(100);
						}
						else if(r == 1)
						{
							$("#active_bundle_"+id_bundle).html("<img src=\"../img/admin/enabled.gif\" alt=\"Attivo\" title=\"Attivo\" style=\"cursor:pointer\" onclick=\"activeBundle("+id_bundle+",0)\" />").fadeIn(100);
						}
					  },
					  error: function(xhr,stato,errori){
						 alert("Errore durante l\'operazione:"+xhr.status);
					  }
					});
					
					
				}
				</script>
				
				
				<table width="100%" class="table" id="tableBundles">
				<thead>
				<tr>
					<th style="width:45px">ID</th>
					<th style="width:45px">Immagine</th>
					<th style="width:90px">Codice</th>
					<th style="width:150px">Descrizione</th>
					<th style="width:450px">Prodotti</th>
					<th style="width:30px; text-align:right">Status</th>
					<th style="width:75px; text-align:right">Prezzo CLI</th>
					<th style="width:75px; text-align:right">Prezzo RIV</th>
					<th style="width:55px">Azioni</th>
				</tr>
				</thead>
				<tbody id="tableBundlesBody">
				';	
				
				foreach($bundles_current as $bundle) {
					$prezzi_bundle = array();
					$prezzi_bundle_riv = array();
					
					echo "<tr><td><input type='hidden' class='id_bundle_td' value='".$bundle['id_bundle']."' />".$bundle['id_bundle']."</td>";
					$id_image_bundle = Db::getInstance()->getValue("SELECT id_image FROM bundle_image WHERE id_bundle = ".$bundle['id_bundle']."");
						
						echo "<td>";
						if($id_image_bundle > 0) {
						
						echo '<img src="'._PS_IMG_.'b/'.$id_image_bundle."-".$bundle['id_bundle'].'-small.jpg" alt="" title="" />';
						
						}
						
						else {
						
						echo '<p style="text-align:center">-</p>';
						
						}
						
						echo "</td>";
						
					echo "<td>".$bundle['bundle_ref']."</td>";
					echo "<td>".$bundle['bundle_name']."</td>";
					$bundle_products_row = explode(":", $bundle['bundle_prices']);
					
					echo "<td style='vertical-align:center'>";
					$i_n = 0;
					foreach($bundle_products_row as $bundle_row) {
						
						$idprz = explode(";", $bundle_row);
					
						
						$nomeprod = Db::getInstance()->getRow("SELECT i.id_image, pl.id_product, pl.name, p.reference FROM product_lang pl LEFT JOIN image i ON pl.id_product = i.id_product JOIN product p ON pl.id_product = p.id_product WHERE pl.id_product = ".$idprz[0]." AND pl.id_lang = 5 AND i.cover = 1");
						
						if(empty($nomeprod)) {
						}
						else {
							echo ($i_n == 0 ? '' : "<div style='float:left; position:relative; text-align:center'><br /> + </div>");
							echo "<div style='float:left; position:relative; text-align:center; margin-right:5px; margin-left:5px;'>";
							echo "<div style='position:relative;display:block;margin:0 auto; width:50px'><div style='position:absolute; background-color:white;border:1px solid black; color:black; width:25px; left:5px'><strong>".$idprz[1]."x</strong></div>";
							echo "<img style='border:1px solid black;' src='http://www.ezdirect.it/img/p/".$nomeprod['id_product']."-".$nomeprod['id_image']."-80x80.jpg' alt='".$nomeprod['name']."' title='".$nomeprod['name']."' width='50' height='50' /></div>";
							echo "<br />".$nomeprod['reference']."</div>";
						}
						

						
						$i_n++;
						
						$prezzo_prd_cli = Product::trovaMigliorPrezzo($idprz[0],1,$idprz[1]);
						$prezzo_prd_cli = round($prezzo_prd_cli - (($prezzo_prd_cli * (str_replace(",",".",$idprz[2])))/100),2);
						
						
						$prezzo_prd_riv = Product::trovaMigliorPrezzo($idprz[0],3,$idprz[1]);
						$prezzo_prd_riv = round($prezzo_prd_riv - (($prezzo_prd_riv * (str_replace(",",".",$idprz[3])))/100),2);
						
						
						$prezzi_bundle[] = (float)($prezzo_prd_cli)*$idprz[1];
						$prezzi_bundle_riv[] = (float)($prezzo_prd_riv)*$idprz[1];
					}
					
					
					echo "</td>";
					echo "<td>".($bundle['active'] == 1 ? '<span id="active_bundle_'.$bundle['id_bundle'].'"><img src="../img/admin/enabled.gif" alt="Attivo" title="Attivo" style="cursor:pointer" onclick="activeBundle('.$bundle['id_bundle'].',0)" /></span>' : '<span id="active_bundle_'.$bundle['id_bundle'].'"><img src="../img/admin/disabled.gif" alt="Non Attivo" title="Non Attivo" style="cursor:pointer" onclick="activeBundle('.$bundle['id_bundle'].',1)" /></span>')."</td>";
					echo "<td style='text-align:right'>";
					$prezzo_tot = 0;
						foreach($prezzi_bundle as $prezzo_bundle) {
							
							$prz_u = (float)(str_replace(",",".",$prezzo_bundle))." - ";
							$prezzo_tot += $prz_u;
						}
						
					echo number_format($prezzo_tot,2,",","")." &euro;";	
					echo "</td>";
					echo "<td style='text-align:right'>";
					$prezzo_tot_riv = 0;
						foreach($prezzi_bundle_riv as $prezzo_bundle_riv) {
							
							$prz_u_riv = (float)(str_replace(",",".",$prezzo_bundle_riv))." - ";
							$prezzo_tot_riv += $prz_u_riv;
						}
						
					echo number_format($prezzo_tot_riv,2,",","")." &euro;";	
					echo "</td>";
					
					echo "<td>";
					echo '<a style="cursor:pointer" href="'.$currentIndex.'&id_product='.$obj->id.'&updateproduct&newbundle&id_bundle='.$bundle['id_bundle'].'&updatebundle&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'&tabs=3"><img src="../img/admin/edit.gif" /></a>
					
					<a style="cursor:pointer" href="'.$currentIndex.'&id_product='.$obj->id.'&updateproduct&id_bundle='.$bundle['id_bundle'].'&deletebundle&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'&tabs=3"  onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }"><img src="../img/admin/delete.gif" /></a>';
					echo "</td>";
					echo "</tr>";
				}
				
				echo "</tbody></table>";
				
				echo '<script type="text/javascript" src="../js/jquery/jquery.tablednd_0_5.js"></script>
				<script type="text/javascript">
				$(document).ready(function() {
					$("#tableBundles").tableDnD({
					
					onDrop: function() {
					
						var arrayValoriOrdine = document.getElementsByClassName("id_bundle_td");
						for (var i = 0; i < arrayValoriOrdine.length; ++i) {
							var item = parseFloat(arrayValoriOrdine[i].value);  
							$.ajax({
								type: "POST",
								async: false,
								url: "ajax_products_list2.php",
								data: "edit_bundle_sort_order=1&id_bundle="+item+"&sort_order=" + i,
								success: function()
								{
								},
					
							});
						}
					
						
					},
					
					
					});
					
				});
				</script>';
			}
			
			else {
				echo 'Nessun kit ancora creato per questo prodotto.<br /><br />';

			}
			
			echo '<p style="text-align:center"><br /><a href="'.$currentIndex.'&id_product='.$obj->id.'&updateproduct&newbundle&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'&tabs=3" class="button">Crea nuovo bundle</a></p>';
		}
		
		
		
		echo '</div>';
	}
		

	public function initCombinationImagesJS()
	{
		global $cookie;

		if (!($obj = $this->loadObject(true)))
			return;

		$content = 'var combination_images = new Array();';
		if (!$allCombinationImages = $obj->getCombinationImages((int)($cookie->id_lang)))
			return $content;
		foreach ($allCombinationImages AS $id_product_attribute => $combinationImages)
		{
			$i = 0;
			$content .= 'combination_images['.(int)($id_product_attribute).'] = new Array();';
			foreach ($combinationImages AS $combinationImage)
				$content .= 'combination_images['.(int)($id_product_attribute).']['.$i++.'] = '.(int)($combinationImage['id_image']).';';
		}
		return $content;
	}

	function displayFormAttributes($obj, $languages)
	{
		global $currentIndex, $cookie;

		$attributeJs = array();
		$attributes = Attribute::getAttributes((int)($cookie->id_lang), true);
		foreach ($attributes AS $k => $attribute)
			$attributeJs[$attribute['id_attribute_group']][$attribute['id_attribute']] = $attribute['name'];
		$currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));
		$attributes_groups = AttributeGroup::getAttributesGroups((int)($cookie->id_lang));
		$default_country = new Country((int)Configuration::get('PS_COUNTRY_DEFAULT'));

		$images = Image::getImages((int)($cookie->id_lang), $obj->id);
		if ($obj->id)
			{
				echo '
			<script type="text/javascript">
				$(document).ready(function(){
					$(\'#id_mvt_reason\').change(function(){
						updateMvtStatus($(this).val());
					});
					updateMvtStatus($(this).val());
				});
			</script>
			<table cellpadding="5">
				<tr>
					<td colspan="2"><b>'.$this->l('Add or modify combinations for this product').'</b> -
					&nbsp;<a href="index.php?tab=AdminCatalog&id_product='.$obj->id.'&id_category='.(int)(Tools::getValue('id_category')).'&attributegenerator&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'" onclick="return confirm(\''.$this->l('Are you sure you want to delete entered product information?', __CLASS__, true, false).'\');"><img src="../img/admin/appearance.gif" alt="combinations_generator" class="middle" title="'.$this->l('Product combinations generator').'" />&nbsp;'.$this->l('Product combinations generator').'</a>
					</td>
				</tr>
			</table>
			<hr style="width:100%;" /><br />
			<table cellpadding="5" style="width:100%">
			<tr>
			  <td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;" valign="top">'.$this->l('Group:').'</td>
			  <td style="padding-bottom:5px;"><select name="attribute_group" id="attribute_group" style="width: 200px;" onchange="populate_attrs();">';
				if (isset($attributes_groups))
					foreach ($attributes_groups AS $k => $attribute_group)
						if (isset($attributeJs[$attribute_group['id_attribute_group']]))
							echo '
							<option value="'.$attribute_group['id_attribute_group'].'">
							'.htmlentities(stripslashes($attribute_group['name']), ENT_COMPAT, 'UTF-8').'&nbsp;&nbsp;</option>';
				echo '
				</select></td>
		  </tr>
		  <tr>
			  <td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;" valign="top">'.$this->l('Attribute:').'</td>
			  <td style="padding-bottom:5px;"><select name="attribute" id="attribute" style="width: 200px;">
			  <option value="0">---</option>
			  </select>
			  <script type="text/javascript" language="javascript">populate_attrs();</script>
			  </td>
		  </tr>
		  <tr>
			  <td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;" valign="top">
			  <input style="width: 140px; margin-bottom: 10px;" type="button" value="'.$this->l('Add').'" class="button" onclick="add_attr();"/><br />
			  <input style="width: 140px;" type="button" value="'.$this->l('Delete').'" class="button" onclick="del_attr()"/></td>
			  <td align="left">
				  <select id="product_att_list" name="attribute_combinaison_list[]" multiple="multiple" size="4" style="width: 320px;"></select>
				</td>
		  </tr>
		  <tr><td colspan="2"><hr style="width:100%;" /></td></tr>
		  <tr>
			  <td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;">'.$this->l('Reference:').'</td>
			  <td style="padding-bottom:5px;">
				<input size="55" type="text" id="attribute_reference" name="attribute_reference" value="" style="width: 130px; margin-right: 44px;" />
				'.$this->l('EAN13:').'<input size="55" maxlength="13" type="text" id="attribute_ean13" name="attribute_ean13" value="" style="width: 110px; margin-left: 10px; margin-right: 44px;" />
				'.$this->l('UPC:').'<input size="55" maxlength="12" type="text" id="attribute_upc" name="attribute_upc" value="" style="width: 110px; margin-left: 10px;" />
				<span class="hint" name="help_box">'.$this->l('Special characters allowed:').' .-_#<span class="hint-pointer">&nbsp;</span></span>
			  </td>
		  </tr>
		  <tr>
			  <td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;">'.$this->l('Supplier Reference:').'</td>
			  <td style="padding-bottom:5px;">
				<input size="55" type="text" id="attribute_supplier_reference" name="attribute_supplier_reference" value="" style="width: 130px; margin-right: 44px;" />
				'.$this->l('Location:').'<input size="55" type="text" id="attribute_location" name="attribute_location" value="" style="width: 101px; margin-left: 10px;" />
				<span class="hint" name="help_box">'.$this->l('Special characters allowed:').' .-_#<span class="hint-pointer">&nbsp;</span></span>
			  </td>
		  </tr>
		  <tr><td colspan="2"><hr style="width:100%;" /></td></tr>
		  <tr>
			  <td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;">'.$this->l('Wholesale price:').'</td>
			  <td style="padding-bottom:5px;">'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input type="text" size="6"  name="attribute_wholesale_price" id="attribute_wholesale_price" value="0.00" onKeyUp="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, \'.\');" />'.($currency->format % 2 == 0 ? ' '.$currency->sign : '').' ('.$this->l('overrides Wholesale price on Information tab').')</td>
		  </tr>
		  <tr>
			  <td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;">'.$this->l('Impact on price:').'</td>
			  <td colspan="2" style="padding-bottom:5px;">
				<select name="attribute_price_impact" id="attribute_price_impact" style="width: 140px;" onchange="check_impact(); calcImpactPriceTI();">
				  <option value="0">'.$this->l('None').'</option>
				  <option value="1">'.$this->l('Increase').'</option>
				  <option value="-1">'.$this->l('Reduction').'</option>
				</select>
				<span id="span_impact">&nbsp;&nbsp;'.$this->l('of').'&nbsp;&nbsp;'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'
					<input type="text" size="6" name="attribute_price" id="attribute_price" value="0.00" onKeyUp="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, \'.\'); calcImpactPriceTI();"/>'.($currency->format % 2 == 0 ? ' '.$currency->sign : '');
					if ($default_country->display_tax_label)
					{
						echo ' '.$this->l('(tax excl.)').'<span '.(Tax::excludeTaxeOption() ? 'style="display:none"' : '' ).'> '.$this->l('or').' '.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'
							<input type="text" size="6" name="attribute_priceTI" id="attribute_priceTI" value="0.00" onKeyUp="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, \'.\'); calcImpactPriceTE();"/>'.($currency->format % 2 == 0 ? ' '.$currency->sign : '').' '.$this->l('(tax incl.)').'</span> '.$this->l('final product price will be set to').' '.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<span id="attribute_new_total_price">0.00</span>'.($currency->format % 2 == 0 ? $currency->sign.' ' : '');
					}
			echo '
				</span>
			</td>
		  </tr>
		  <tr>
			  <td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;">'.$this->l('Impact on weight:').'</td>
			  <td colspan="2" style="padding-bottom:5px;"><select name="attribute_weight_impact" id="attribute_weight_impact" style="width: 140px;" onchange="check_weight_impact();">
			  <option value="0">'.$this->l('None').'</option>
			  <option value="1">'.$this->l('Increase').'</option>
			  <option value="-1">'.$this->l('Reduction').'</option>
			  </select>
			  <span id="span_weight_impact">&nbsp;&nbsp;'.$this->l('of').'&nbsp;&nbsp;
				<input type="text" size="6" name="attribute_weight" id="attribute_weight" value="0.00" onKeyUp="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, \'.\');" /> '.Configuration::get('PS_WEIGHT_UNIT').'</span></td>
		  </tr>
		  <tr id="tr_unit_impact">
			  <td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;">'.$this->l('Impact on unit price :').'</td>
			  <td colspan="2" style="padding-bottom:5px;"><select name="attribute_unit_impact" id="attribute_unit_impact" style="width: 140px;" onchange="check_unit_impact();">
			  <option value="0">'.$this->l('None').'</option>
			  <option value="1">'.$this->l('Increase').'</option>
			  <option value="-1">'.$this->l('Reduction').'</option>
			  </select>
			  <span id="span_unit_impact">&nbsp;&nbsp;'.$this->l('of').'&nbsp;&nbsp;'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'
				<input type="text" size="6" name="attribute_unity" id="attribute_unity" value="0.00" onKeyUp="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, \'.\');" />'.($currency->format % 2 == 0 ? ' '.$currency->sign : '').' / <span id="unity_third">'.$this->getFieldValue($obj, 'unity').'</span>
			</span></td>
		  </tr>';
		if (Configuration::get('PS_USE_ECOTAX'))
			echo'
				  <tr>
					  <td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;">'.$this->l('Eco-tax:').'</td>
					  <td style="padding-bottom:5px;">'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').'<input type="text" size="3" name="attribute_ecotax" id="attribute_ecotax" value="0.00" onKeyUp="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, \'.\');" />'.($currency->format % 2 == 0 ? ' '.$currency->sign : '').' ('.$this->l('overrides Eco-tax on Information tab').')</td>
				  </tr>';

		echo'
		  <tr id="initial_stock_attribute">
				<td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;" class="col-left">'.$this->l('Initial stock:').'</td>
				<td><input type="text" name="attribute_quantity" size="3" maxlength="10" value="0"/></td>
		  </tr>
		  </tr>
			<tr id="stock_mvt_attribute" style="display:none;">
				<td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;" class="col-left">'.$this->l('Stock movement:').'</td>
				<td style="padding-bottom:5px;">
					<select id="id_mvt_reason" name="id_mvt_reason">
						<option value="-1">--</option>';
			$reasons = StockMvtReason::getStockMvtReasons((int)$cookie->id_lang);
			foreach ($reasons AS $reason)
				echo '<option rel="'.$reason['sign'].'" value="'.$reason['id_stock_mvt_reason'].'" '.(Configuration::get('PS_STOCK_MVT_REASON_DEFAULT') == $reason['id_stock_mvt_reason'] ? 'selected="selected"' : '').'>'.$reason['name'].'</option>';
			echo '</select>
					<input type="text" name="attribute_mvt_quantity" size="3" maxlength="10" value="0"/>&nbsp;&nbsp;
					<span style="display:none;" id="mvt_sign"></span>
					<br />
					<div class="hint clear" style="display: block;width: 70%;">'.$this->l('Choose the reason and enter the quantity that you want to increase or decrease in your stock').'</div>
				</td>
			</tr>
			<tr>
			<td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;" class="col-left">'.$this->l('Minimum quantity:').'</td>
				<td style="padding-bottom:5px;">
					<input size="3" maxlength="10" name="attribute_minimal_quantity" id="attribute_minimal_quantity" type="text" value="'.($this->getFieldValue($obj, 'attribute_minimal_quantity') ? $this->getFieldValue($obj, 'attribute_minimal_quantity') : 1).'" />
					<p>'.$this->l('The minimum quantity to buy this product (set to 1 to disable this feature)').'</p>
				</td>
			</tr>
		  <tr style="display:none;" id="attr_qty_stock">
			  <td style="width:150px">'.$this->l('Quantity in stock:').'</td>
			  <td style="padding-bottom:5px;"><b><span style="display:none;" id="attribute_quantity"></span></b></td>
		  </tr>
		  <tr><td colspan="2"><hr style="width:100%;" /></td></tr>
		  <tr>
			  <td style="width:150px">'.$this->l('Image:').'</td>
			  <td style="padding-bottom:5px;">
				<ul id="id_image_attr">';
			$i = 0;
			$imageType = ImageType::getByNameNType('small', 'products');
			$imageWidth = (isset($imageType['width']) ? (int)($imageType['width']) : 64) + 25;
			foreach ($images AS $image)
			{
				$imageObj = new Image($image['id_image']);
				echo '<li style="float: left; width: '.$imageWidth.'px;"><input type="checkbox" name="id_image_attr[]" value="'.(int)($image['id_image']).'" id="id_image_attr_'.(int)($image['id_image']).'" />
				<label for="id_image_attr_'.(int)($image['id_image']).'" style="float: none;"><img src="'._THEME_PROD_DIR_.$imageObj->getExistingImgPath().'-small.jpg" alt="'.htmlentities(stripslashes($image['legend']), ENT_COMPAT, 'UTF-8').'" title="'.htmlentities(stripslashes($image['legend']), ENT_COMPAT, 'UTF-8').'" /></label></li>';
				++$i;
			}
			echo '</ul>
				<img id="pic" alt="" title="" style="display: none; width: 100px; height: 100px; float: left; border: 1px dashed #BBB; margin-left: 20px;" />
			  </td>
		  </tr>
			<tr>
			  <td style="width:150px">'.$this->l('Default:').'<br /><br /></td>
			  <td style="padding-bottom:5px;">
				<input type="checkbox" name="attribute_default" id="attribute_default" value="1" />&nbsp;'.$this->l('Make this the default combination for this product').'<br /><br />
			  </td>
		  </tr>
		  <tr>
			  <td style="width:150px">&nbsp;</td>
			  <td style="padding-bottom:5px;">
				<span style="float: left;"><input type="submit" name="submitProductAttribute" id="submitProductAttribute" value="'.$this->l('Add this combination').'" class="button" onclick="attr_selectall(); this.form.action += \'&addproduct&tabs=3\';" /> </span>
				<span id="ResetSpan" style="float: left; margin-left: 8px; display: none;">
				  <input type="reset" name="ResetBtn" id="ResetBtn" onclick="init_elems(); getE(\'submitProductAttribute\').value = \''.$this->l('Add this attributes group', __CLASS__, true).'\';
				  getE(\'id_product_attribute\').value = 0; $(\'#ResetSpan\').slideToggle();" class="button" value="'.$this->l('Cancel modification').'" /></span><span class="clear"></span>
			  </td>
		  </tr>
		  <tr><td colspan="2"><hr style="width:100%;" /></td></tr>
		  <tr>
			  <td colspan="2">
					<br />
					<table border="0" cellpadding="0" cellspacing="0" class="table">
						<tr>
							<th>'.$this->l('Attributes').'</th>
							<th>'.$this->l('Impact').'</th>
							<th>'.$this->l('Weight').'</th>
							<th>'.$this->l('Reference').'</th>
							<th>'.$this->l('EAN13').'</th>
							<th>'.$this->l('UPC').'</th>
							<th class="center">'.$this->l('Quantity').'</th>
							<th class="center">'.$this->l('Actions').'</th>
						</tr>';
			if ($obj->id)
			{
				/* Build attributes combinaisons */
				$combinaisons = $obj->getAttributeCombinaisons((int)($cookie->id_lang));
				$groups = array();
				if (is_array($combinaisons))
				{
					$combinationImages = $obj->getCombinationImages((int)($cookie->id_lang));
					foreach ($combinaisons AS $k => $combinaison)
					{
						$combArray[$combinaison['id_product_attribute']]['wholesale_price'] = $combinaison['wholesale_price'];
						$combArray[$combinaison['id_product_attribute']]['price'] = $combinaison['price'];
						$combArray[$combinaison['id_product_attribute']]['weight'] = $combinaison['weight'];
						$combArray[$combinaison['id_product_attribute']]['unit_impact'] = $combinaison['unit_price_impact'];
						$combArray[$combinaison['id_product_attribute']]['reference'] = $combinaison['reference'];
						$combArray[$combinaison['id_product_attribute']]['supplier_reference'] = $combinaison['supplier_reference'];
						$combArray[$combinaison['id_product_attribute']]['ean13'] = $combinaison['ean13'];
						$combArray[$combinaison['id_product_attribute']]['upc'] = $combinaison['upc'];
						$combArray[$combinaison['id_product_attribute']]['attribute_minimal_quantity'] = $combinaison['minimal_quantity'];
						$combArray[$combinaison['id_product_attribute']]['location'] = $combinaison['location'];
						$combArray[$combinaison['id_product_attribute']]['quantity'] = $combinaison['quantity'];
						$combArray[$combinaison['id_product_attribute']]['id_image'] = isset($combinationImages[$combinaison['id_product_attribute']][0]['id_image']) ? $combinationImages[$combinaison['id_product_attribute']][0]['id_image'] : 0;
						$combArray[$combinaison['id_product_attribute']]['default_on'] = $combinaison['default_on'];
						$combArray[$combinaison['id_product_attribute']]['ecotax'] = $combinaison['ecotax'];
						$combArray[$combinaison['id_product_attribute']]['attributes'][] = array($combinaison['group_name'], $combinaison['attribute_name'], $combinaison['id_attribute']);
						if ($combinaison['is_color_group'])
							$groups[$combinaison['id_attribute_group']] = $combinaison['group_name'];
					}
				}
				$irow = 0;
				if (isset($combArray))
				{
					foreach ($combArray AS $id_product_attribute => $product_attribute)
					{
						$list = '';
						$jsList = '';

						/* In order to keep the same attributes order */
						asort($product_attribute['attributes']);

						foreach ($product_attribute['attributes'] AS $attribute)
						{
							$list .= addslashes(htmlspecialchars($attribute[0])).' - '.addslashes(htmlspecialchars($attribute[1])).', ';
							$jsList .= '\''.addslashes(htmlspecialchars($attribute[0])).' : '.addslashes(htmlspecialchars($attribute[1])).'\', \''.$attribute[2].'\', ';
						}
						$list = rtrim($list, ', ');
						$jsList = rtrim($jsList, ', ');
						$attrImage = $product_attribute['id_image'] ? new Image($product_attribute['id_image']) : false;
						echo '
						<tr'.($irow++ % 2 ? ' class="alt_row"' : '').($product_attribute['default_on'] ? ' style="background-color:#D1EAEF"' : '').'>
							<td>'.stripslashes($list).'</td>
							<td class="right">'.($currency->format % 2 != 0 ? $currency->sign.' ' : '').$product_attribute['price'].($currency->format % 2 == 0 ? ' '.$currency->sign : '').'</td>
							<td class="right">'.$product_attribute['weight'].Configuration::get('PS_WEIGHT_UNIT').'</td>
							<td class="right">'.$product_attribute['reference'].'</td>
							<td class="right">'.$product_attribute['ean13'].'</td>
							<td class="right">'.$product_attribute['upc'].'</td>
							<td class="center">'.$product_attribute['quantity'].'</td>
							<td class="center">
							<a style="cursor: pointer;">
							<img src="../img/admin/edit.gif" alt="'.$this->l('Modify this combination').'"
							onclick="javascript:fillCombinaison(\''.$product_attribute['wholesale_price'].'\', \''.$product_attribute['price'].'\', \''.$product_attribute['weight'].'\', \''.$product_attribute['unit_impact'].'\', \''.$product_attribute['reference'].'\', \''.$product_attribute['supplier_reference'].'\', \''.$product_attribute['ean13'].'\',
							\''.$product_attribute['quantity'].'\', \''.($attrImage ? $attrImage->id : 0).'\', Array('.$jsList.'), \''.$id_product_attribute.'\', \''.$product_attribute['default_on'].'\', \''.$product_attribute['ecotax'].'\', \''.$product_attribute['location'].'\', \''.$product_attribute['upc'].'\', \''.$product_attribute['attribute_minimal_quantity'].'\'); calcImpactPriceTI();" /></a>&nbsp;
							'.(!$product_attribute['default_on'] ? '<a href="'.$currentIndex.'&defaultProductAttribute&id_product_attribute='.$id_product_attribute.'&id_product='.$obj->id.'&'.(Tools::isSubmit('id_category') ? 'id_category='.(int)(Tools::getValue('id_category')).'&' : '&').'token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">
							<img src="../img/admin/asterisk.gif" alt="'.$this->l('Make this the default combination').'" title="'.$this->l('Make this combination the default one').'"></a>' : '').'
							<a href="'.$currentIndex.'&deleteProductAttribute&id_product_attribute='.$id_product_attribute.'&id_product='.$obj->id.'&'.(Tools::isSubmit('id_category') ? 'id_category='.(int)(Tools::getValue('id_category')).'&' : '&').'token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'" onclick="return confirm(\''.$this->l('Are you sure?', __CLASS__, true, false).'\');">
							<img src="../img/admin/delete.gif" alt="'.$this->l('Delete this combination').'" /></a></td>
						</tr>';
					}
					echo '<tr><td colspan="7" align="center"><a href="'.$currentIndex.'&deleteAllProductAttributes&id_product='.$obj->id.'&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'" onclick="return confirm(\''.$this->l('Are you sure?', __CLASS__, true, false).'\');"><img src="../img/admin/delete.gif" alt="'.$this->l('Delete this combination').'" /> '.$this->l('Delete all combinations').'</a></td></tr>';
				}
				else
					echo '<tr><td colspan="7" align="center"><i>'.$this->l('No combination yet').'.</i></td></tr>';
			}
			echo '
						</table>
						<br />'.$this->l('The row in blue is the default combination.').'
						<br />
						'.$this->l('A default combination must be designated for each product.').'
						</td>
						</tr>
					</table>
					<script type="text/javascript">
						var impact = getE(\'attribute_price_impact\');
						var impact2 = getE(\'attribute_weight_impact\');

						var s_attr_group = document.getElementById(\'span_new_group\');
						var s_attr_name = document.getElementById(\'span_new_attr\');
						var s_impact = document.getElementById(\'span_impact\');
						var s_impact2 = document.getElementById(\'span_weight_impact\');

						init_elems();
					</script>
					<hr style="width:100%;" />
					<table cellpadding="5">
						<tr>
							<td class="col-left"><b>'.$this->l('Color picker:').'</b></td>
							<td style="padding-bottom:5px;">
								<select name="id_color_default">
								<option value="0">'.$this->l('Do not display').'</option>';
								foreach ($attributes_groups AS $k => $attribute_group)
									if (isset($groups[$attribute_group['id_attribute_group']]))
										echo '<option value="'.(int)($attribute_group['id_attribute_group']).'"
												'.((int)($attribute_group['id_attribute_group']) == (int)($obj->id_color_default) ? 'selected="selected"' : '').'>'
												.htmlentities(stripslashes($attribute_group['name']), ENT_COMPAT, 'UTF-8').
											'</option>';
								echo '
								</select>
								&nbsp;&nbsp;<input type="submit" value="'.$this->l('OK').'" name="submitAdd'.$this->table.'AndStay" class="button" />
								&nbsp;&nbsp;&nbsp;&nbsp;<a href="index.php?tab=AdminAttributesGroups&token='.Tools::getAdminToken('AdminAttributesGroups'.(int)(Tab::getIdFromClassName('AdminAttributesGroups')).(int)($cookie->id_employee)).'" onclick="return confirm(\''.$this->l('Are you sure you want to delete entered product information?', __CLASS__, true, false).'\');"><img src="../img/admin/asterisk.gif" alt="" /> '.$this->l('Color attribute management').'</a>
								<p >'.$this->l('Activate the color choice by selecting a color attribute group.').'</p>
							</td>
						</tr>
					</table>';
				}
				else
					echo '<b>'.$this->l('You must save this product before adding combinations').'.</b>';
	}

	function displayFormFeatures($obj)
	{
		global $cookie, $currentIndex;
		parent::displayForm();

		if ($obj->id)
		{
			$prodid = $obj->id;
			$feature = Feature::getFeaturesAdvanced((int)($cookie->id_lang), $prodid);
			$ctab = '';
			foreach ($feature AS $tab)
				$ctab .= 'ccustom_'.$tab['id_feature'].'¤';
			$ctab = rtrim($ctab, '¤');

			echo '
			<table cellpadding="5">
				<tr>
					<td colspan="2">
						<b>'.$this->l('Assign features to this product:').'</b><br />
						<ul style="margin: 10px 0 0 20px;">
							<li>'.$this->l('You can specify a value for each relevant feature regarding this product, empty fields will not be displayed.').'</li>
							<li>'.$this->l('You can either set a specific value, or select among existing pre-defined values you added previously.').'</li>
						</ul>
					</td>
				</tr>
			</table>
			<hr style="width:100%;" /><br />';
			// Header
			$nb_feature = Feature::nbFeatures((int)($cookie->id_lang));
			echo '
			<table border="0" cellpadding="0" cellspacing="0" class="table" style="width:900px;">
				<tr>
					<th style="width:300px">'.$this->l('Feature').'</td>
					<th>'.$this->l('Group').'</td>
					<th>'.$this->l('Pre-defined value').'</td>
					<th><u>'.$this->l('or').'</u> '.$this->l('Customized value').'</td>
				</tr>';
			if (!$nb_feature)
				echo '<tr><td colspan="3" style="text-align:center;">'.$this->l('No features defined').'</td></tr>';
			echo '</table>';

			// Listing
			if ($nb_feature)
			{
				echo '
				<table cellpadding="5" style="width: 900px; margin-top: 10px">
				<input type="hidden" name="inviaFeatures" />
				';
				
				foreach ($feature AS $tab_features)
				{
					
					$current_item = false;
					$custom = true;
					foreach ($obj->getFeatures() as $tab_products)
						if ($tab_products['id_feature'] == $tab_features['id_feature'])
							$current_item = $tab_products['id_feature_value'];

					$featureValues = FeatureValue::getFeatureValuesWithLang((int)$cookie->id_lang, (int)$tab_features['id_feature']);
					
					if($tab_features['id_feature'] == '924' && ($current_item == '12847' || $current_item == '12894'))
					{
						echo '<script type="text/javascript">
						$(document).ready(function(){
							$(\'#features_925\').hide(); $(\'#features_926\').hide(); $(\'#features_927\').hide(); $(\'#features_928\').hide(); $(\'#features_486\').hide(); $(\'#features_930\').hide(); $(\'#features_931\').hide(); $(\'#features_932\').hide(); $(\'#features_933\').hide(); $(\'#features_934\').hide(); $(\'#features_935\').hide(); $(\'#features_936\').hide(); $(\'#features_937\').hide(); $(\'#features_938\').show(); $(\'#features_939\').show(); $(\'#features_716\').show(); $(\'#features_940\').show(); $(\'#features_941\').show(); $(\'#features_942\').show(); $(\'#features_483\').show(); $(\'#features_484\').show(); $(\'#features_943\').show(); $(\'#features_944\').show(); $(\'#features_945\').show(); $(\'#features_946\').show();
						});
						</script>';
					}
					else if($tab_features['id_feature'] == '924' && ($current_item == '12848'))
					{
						echo '<script type="text/javascript">
						$(document).ready(function(){
							$(\'#features_925\').show(); $(\'#features_926\').show(); $(\'#features_927\').show(); $(\'#features_928\').show(); $(\'#features_486\').show(); $(\'#features_930\').show(); $(\'#features_931\').show(); $(\'#features_932\').show(); $(\'#features_933\').show(); $(\'#features_934\').show(); $(\'#features_935\').show(); $(\'#features_936\').show(); $(\'#features_937\').show(); $(\'#features_938\').hide(); $(\'#features_939\').hide(); $(\'#features_716\').hide(); $(\'#features_940\').hide(); $(\'#features_941\').hide(); $(\'#features_942\').hide(); $(\'#features_483\').hide(); $(\'#features_484\').hide(); $(\'#features_943\').hide(); $(\'#features_944\').hide(); $(\'#features_945\').hide(); $(\'#features_946\').hide();
						});
						</script>';
					}

					echo '
					<tr id="features_'.$tab_features['id_feature'].'">
						<td style="width:300px">'.$tab_features['name'].'</td>
						<td>'; 

						
						echo "<strong>".$tab_features['group_name']."</strong>"; 
						
						
						echo '</td>
						
						
						<td>';

					if (sizeof($featureValues))
					{
						echo '
							<select style="width:160px; margin-right:5px" id="feature_'.$tab_features['id_feature'].'_value" name="feature_'.$tab_features['id_feature'].'_value"
								onchange="$(\'.custom_'.$tab_features['id_feature'].'_\').val(\'\'); '.($tab_features['id_feature'] == '924' ? 'if (this.value == \'12847\' || this.value == \'12894\' ) {$(\'#features_925\').hide(); $(\'#features_926\').hide(); $(\'#features_927\').hide(); $(\'#features_928\').hide(); $(\'#features_486\').hide(); $(\'#features_930\').hide(); $(\'#features_931\').hide(); $(\'#features_932\').hide(); $(\'#features_933\').hide(); $(\'#features_934\').hide(); $(\'#features_935\').hide(); $(\'#features_936\').hide(); $(\'#features_937\').hide(); $(\'#features_938\').show(); $(\'#features_939\').show(); $(\'#features_716\').show(); $(\'#features_940\').show(); $(\'#features_941\').show(); $(\'#features_942\').show(); $(\'#features_483\').show(); $(\'#features_484\').show(); $(\'#features_943\').show(); $(\'#features_944\').show(); $(\'#features_945\').show(); $(\'#features_946\').show(); } else if(this.value == \'12848\') { $(\'#features_925\').show(); $(\'#features_926\').show(); $(\'#features_927\').show(); $(\'#features_928\').show(); $(\'#features_486\').show(); $(\'#features_930\').show(); $(\'#features_931\').show(); $(\'#features_932\').show(); $(\'#features_933\').show(); $(\'#features_934\').show(); $(\'#features_935\').show(); $(\'#features_936\').show(); $(\'#features_937\').show(); $(\'#features_938\').hide(); $(\'#features_939\').hide(); $(\'#features_716\').hide(); $(\'#features_940\').hide(); $(\'#features_941\').hide(); $(\'#features_942\').hide(); $(\'#features_483\').hide(); $(\'#features_484\').hide(); $(\'#features_943\').hide(); $(\'#features_944\').hide(); $(\'#features_945\').hide(); $(\'#features_946\').hide(); } else { $(\'#features_925\').show(); $(\'#features_926\').show(); $(\'#features_927\').show(); $(\'#features_928\').show(); $(\'#features_486\').show(); $(\'#features_930\').show(); $(\'#features_931\').show(); $(\'#features_932\').show(); $(\'#features_933\').show(); $(\'#features_934\').show(); $(\'#features_935\').show(); $(\'#features_936\').show(); $(\'#features_937\').show(); $(\'#features_938\').show(); $(\'#features_939\').show(); $(\'#features_716\').show(); $(\'#features_940\').show(); $(\'#features_941\').show(); $(\'#features_942\').show(); $(\'#features_483\').show(); $(\'#features_484\').show(); $(\'#features_943\').show(); $(\'#features_944\').show(); $(\'#features_945\').show(); $(\'#features_946\').show(); }' : '').'">
								';
					$df_feature = false;
						foreach ($featureValues AS $value)
						{
							
							if($tab_features['id_feature'] == 917)
							{
								$present = Db::getInstance()->executeS('SELECT id_feature_value FROM feature_product fp JOIN product p ON fp.id_product = p.id_product WHERE id_feature_value = '.$value['id_feature_value'].' AND p.id_manufacturer = '.$obj->id_manufacturer);
								
								if(sizeof($present))
								{
									echo '<option value="'.$value['id_feature_value'].'"'.(($current_item == $value['id_feature_value']) ? ' selected="selected"' : '').'>'.($value['value']).'&nbsp;</option>';
									
									if($current_item == $value['id_feature_value'])
									$df_feature = true;
								}	
							}
							else
							{
								if ($current_item == $value['id_feature_value'])
									$custom = false;
								echo '<option value="'.$value['id_feature_value'].'"'.(($current_item == $value['id_feature_value']) ? ' selected="selected"' : '').'>'.($value['value']).'&nbsp;</option>';
								
								if($current_item == $value['id_feature_value'])
								$df_feature = true;
							
							}
							
						}
						
						if($df_feature == true) { 
						$custom = false;
						echo '<option value="0">---</option>';
						}
						else 
						{
						echo '<option value="0" selected="selected">---</option>';
						}

						echo '</select>
						
						<a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminExFeatures&id_feature='.$tab_features['id_feature'].'&id_group='.Db::getInstance()->getValue('SELECT id_group FROM feature WHERE id_feature = '.$tab_features['id_feature']).'&token='.Tools::getAdminToken('AdminExFeatures'.(int)(Tab::getIdFromClassName('AdminExFeatures')).(int)($cookie->id_employee)).'#features_values_'.$tab_features['id_feature'].'" target="_blank"><img src="../img/admin/add.gif" alt="Clicca qui per aggiungere un valore al menu a tendina" title="Clicca qui per aggiungere un valore al menu a tendina" /></a>';
					}
					else
						echo '<input type="hidden" name="feature_'.$tab_features['id_feature'].'_value" value="0" /><span style="font-size: 10px; color: #666;">'.$this->l('N/A').' - <a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminExFeatures&id_feature='.$tab_features['id_feature'].'&id_group='.Db::getInstance()->getValue('SELECT id_group FROM feature WHERE id_feature = '.$tab_features['id_feature']).'&token='.Tools::getAdminToken('AdminExFeatures'.(int)(Tab::getIdFromClassName('AdminExFeatures')).(int)($cookie->id_employee)).'#features_values_'.$tab_features['id_feature'].'" target="_blank" style="color: #666; text-decoration: underline;">'.$this->l('Add pre-defined values first').'</a></span>';

					echo '
						</td>
						<td style="class="translatable">';
					$tab_customs = ($custom ? FeatureValue::getFeatureValueLang($current_item) : array());
					
				/*	$varcustom = 0;
					$idspecifica = $tab_features['id_feature'];
					$row_mano = Db::getInstance()->getRow("SELECT custom FROM feature_value WHERE id_feature = $idspecifica");
					
						if($row_mano['custom'] == 0) {
							$varcustom = 0;
						}
						else {
						$varcustom = 1;
						break;
						}
					*/
					
					//if(isset($varcustom) && $varcustom == 1) {
					
						foreach ($this->_languages as $language)
							echo '
								<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
								
									
									<textarea class="custom_'.$tab_features['id_feature'].'_" name="custom_'.$tab_features['id_feature'].'_'.$language['id_lang'].'" cols="40" rows="1" style="height:15px"
										onkeyup="if (isArrowKey(event)) return ;$(\'#feature_'.$tab_features['id_feature'].'_value\').val(0);" >'.htmlentities(Tools::getValue('custom_'.$tab_features['id_feature'].'_'.$language['id_lang'], FeatureValue::selectLang($tab_customs, $language['id_lang'])), ENT_COMPAT, 'UTF-8').'</textarea>
								</div>';
							echo '
							</td>
						</tr>';
					//}
					
					//else {
					
					/*echo '<span title="Per cortesia se vuoi inserire un nuovo valore, inseriscilo nel menù a tendina. Clicca su Catalogo, poi su Caratteristiche, quindi su '.$tab_features['group_name'].' e cerca la specifica '.$tab_features['name'].'. Per aggiungere un nuovo valore, clicca su Add new value e riempi il form.">Istruzioni - passare sopra col mouse</span>';
					*/
					//}
					
					
				}
				echo '
				<tr>
					<td style="height: 50px; text-align: center;" colspan="3">';
					
					/*<input type="submit" name="submitProductFeature" id="submitProductFeature" value="'.$this->l('Save modifications').'" class="button" />*/echo '</td>
				</tr>';
			}
			echo '</table>
			<hr style="width:100%;" />
			<div style="text-align:center;">
				<a href="index.php?tab=AdminFeatures&addfeature&token='.Tools::getAdminToken('AdminFeatures'.(int)(Tab::getIdFromClassName('AdminFeatures')).(int)($cookie->id_employee)).'" onclick="return confirm(\''.$this->l('You will lose all modifications not saved, you may want to save modifications first?', __CLASS__, true, false).'\');"><img src="../img/admin/add.gif" alt="new_features" title="'.$this->l('Add a new feature').'" />&nbsp;'.$this->l('Add a new feature').'</a>
			</div>';
		}
		else
			echo '<b>'.$this->l('You must save this product before adding features').'.</b>';
	}

	public function haveThisAccessory($accessoryId, $accessories)
	{
		foreach ($accessories AS $accessory)
			if ((int)($accessory['id_product']) == (int)($accessoryId))
				return true;
		return false;
	}

	public function haveThisAlternative($accessoryId, $accessories)
	{
		foreach ($accessories AS $accessory)
			if ((int)($accessory['id_product']) == (int)($accessoryId))
				return true;
		return false;
	}
	
	private function displayPack(Product $obj)
	{
		global $currentIndex, $cookie;

		$boolPack = (($obj->id AND Pack::isPack($obj->id)) OR Tools::getValue('ppack')) ? true : false;
		$packItems = $boolPack ? Pack::getItems($obj->id, $cookie->id_lang) : array();

		echo '
		<tr>
			<td>
				<input type="checkbox" name="ppack" id="ppack" value="1"'.($boolPack ? ' checked="checked"' : '').' onclick="$(\'#ppackdiv\').slideToggle();" />
				<label class="t" for="ppack">'.$this->l('Pack').'</label>
			</td>
			<td>
				<div id="ppackdiv" '.($boolPack ? '' : ' style="display: none;"').'>
					<div id="divPackItems">';
		foreach ($packItems as $packItem)
			echo $packItem->pack_quantity.' x '.$packItem->name.'<span onclick="delPackItem('.$packItem->id.');" style="cursor: pointer;"><img src="../img/admin/delete.gif" /></span><br />';
		echo '		</div>
					<input type="hidden" name="inputPackItems" id="inputPackItems" value="';
					if (Tools::getValue('inputPackItems'))
						echo Tools::getValue('inputPackItems');
					else
						foreach ($packItems as $packItem)
							echo $packItem->pack_quantity.'x'.$packItem->id.'-';
					echo '" />
					<input type="hidden" name="namePackItems" id="namePackItems" value="';
					if (Tools::getValue('namePackItems'))
						echo Tools::getValue('namePackItems');
					else
					foreach ($packItems as $packItem)
						echo $packItem->pack_quantity.' x '.$packItem->name.'¤';
					echo '" />
					<input type="hidden" size="2" id="curPackItemId" />

					<p class="clear">'.$this->l('Begin typing the first letters of the product name, then select the product from the drop-down list:').'
					<br />'.$this->l('You cannot add downloadable products to a pack.').'</p>
					<input type="text" size="25" id="curPackItemName" />
					<input type="text" name="curPackItemQty" id="curPackItemQty" value="1" size="1" />
					<script language="javascript">
					'.$this->addPackItem().'
					'.$this->delPackItem().'

					</script>
					<span onclick="addPackItem();" style="cursor: pointer;"><img src="../img/admin/add.gif" alt="'.$this->l('Add an item to the pack').'" title="'.$this->l('Add an item to the pack').'" /></span>
				</td>
			</div>
		</tr>';
		// param multipleSeparator:'||' ajouté à cause de bug dans lib autocomplete
		echo '<script type="text/javascript">
								urlToCall = null;
								/* function autocomplete */
								$(function() {
									$(\'#curPackItemName\')
										.autocomplete(\'ajax_products_list.php\', {
											delay: 100,
											minChars: 1,
											autoFill: false,
											max:20,
											matchContains: true,
											mustMatch:true,
											scroll:false,
											cacheLength:0,
											multipleSeparator:\'||\',
											formatItem: function(item) {
												return item[1]+\' - \'+item[0];
											}
										}).result(function(event, item){
											$(\'#curPackItemId\').val(item[1]);
										});
										$(\'#curPackItemName\').setOptions({
											extraParams: {excludeIds : getSelectedIds(), excludeVirtuals : 1}
										});

								});


								function getSelectedIds()
								{
									// input lines QTY x ID-
									var ids = '. $obj->id.'+\',\';
									ids += $(\'#inputPackItems\').val().replace(/\\d+x/g, \'\').replace(/\-/g,\',\');
									ids = ids.replace(/\,$/,\'\');

									return ids;

								}

								function getAccessorieIds()
								{
									var ids = '. $obj->id.'+\',\';
									ids += $(\'#inputAccessories\').val().replace(/\\-/g,\',\').replace(/\\,$/,\'\');
									ids = ids.replace(/\,$/,\'\');

									return ids;
								}
								
								function getAlternativeIds()
								{
									var ids = '. $obj->id.'+\',\';
									ids += $(\'#inputAlternatives\').val().replace(/\\-/g,\',\').replace(/\\,$/,\'\');
									ids = ids.replace(/\,$/,\'\');

									return ids;
								}

			</script>';

	}

	private function addPackItem()
	{
		return '
			function addPackItem()
			{
				if ($(\'#curPackItemId\').val() == \'\' || $(\'#curPackItemName\').val() == \'\')
				{
					alert(\''.$this->l('Thanks to select at least one product.').'\');
					return false;
				}
				else if ($(\'#curPackItemId\').val() == \'\' || $(\'#curPackItemQty\').val() == \'\')
				{
					alert(\''.$this->l('Thanks to set a quantity to add a product.').'\');
					return false;
				}

				var lineDisplay = $(\'#curPackItemQty\').val()+ \'x \' +$(\'#curPackItemName\').val();

				var divContent = $(\'#divPackItems\').html();
				divContent += lineDisplay;
				divContent += \'<span onclick="delPackItem(\' + $(\'#curPackItemId\').val() + \');" style="cursor: pointer;"><img src="../img/admin/delete.gif" /></span><br />\';

				// QTYxID-QTYxID
				var line = $(\'#curPackItemQty\').val()+ \'x\' +$(\'#curPackItemId\').val();


				$(\'#inputPackItems\').val($(\'#inputPackItems\').val() + line  + \'-\');
				$(\'#divPackItems\').html(divContent);
				$(\'#namePackItems\').val($(\'#namePackItems\').val() + lineDisplay + \'Â¤\');

				$(\'#curPackItemId\').val(\'\');
				$(\'#curPackItemName\').val(\'\');

				$(\'#curPackItemName\').setOptions({
					extraParams: {excludeIds :  getSelectedIds()}
				});
			}
		';
	}

	private function delPackItem()
	{
		return '
		function delPackItem(id)
		{
			var reg = new RegExp(\'-\', \'g\');
			var regx = new RegExp(\'x\', \'g\');

			var div = getE(\'divPackItems\');
			var input = getE(\'inputPackItems\');
			var name = getE(\'namePackItems\');
			var select = getE(\'curPackItemId\');
			var select_quantity = getE(\'curPackItemQty\');

			var inputCut = input.value.split(reg);
			var nameCut = name.value.split(new RegExp(\'¤\', \'g\'));

			input.value = \'\';
			name.value = \'\';
			div.innerHTML = \'\';

			for (var i = 0; i < inputCut.length; ++i)
				if (inputCut[i])
				{
					var inputQty = inputCut[i].split(regx);
					if (inputQty[1] != id)
					{
						input.value += inputCut[i] + \'-\';
						name.value += nameCut[i] + \'¤\';
						div.innerHTML += nameCut[i] + \' <span onclick="delPackItem(\' + inputQty[1] + \');" style="cursor: pointer;"><img src="../img/admin/delete.gif" /></span><br />\';
					}
				}

			$(\'#curPackItemName\').setOptions({
				extraParams: {excludeIds :  getSelectedIds()}
			});
		}';
	}

	private function _positionJS()
	{

		return '<script type="text/javascript">

				function hideLink()
				{
					$(".position-cell span").hide();
					$(".position-cell").append("<img src=\"'._PS_IMG_.'loader.gif\" alt=\"\" />");

				}
				</script>';
	}

	public function updatePackItems($product)
	{
		Pack::deleteItems($product->id);

		// lines format: QTY x ID-QTY x ID
		if (Tools::getValue('ppack') AND $items = Tools::getValue('inputPackItems') AND sizeof($lines = array_unique(explode('-', $items))))
		{
			foreach($lines as $line)
			{
				// line format QTY x ID
				list($qty, $item_id) = explode('x', $line);
				if ($qty > 0 && isset($item_id))
				{
					if (!Pack::addItem((int)($product->id), (int)($item_id), (int)($qty)))
						return false;
				}
			}
		}
		return true;
	}

	public function getL($key)
	{
		$trad = array(
			'Default category:' => $this->l('Default category:'),
			'Catalog:' => $this->l('Catalog:'),
			'Consider changing the default category.' => $this->l('Consider changing the default category.'),
			'ID' => $this->l('ID'),
			'Name' => $this->l('Name'),
			'Mark all checkbox(es) of categories in which product is to appear' => $this->l('Mark all checkbox(es) of categories in which product is to appear')
		);
		return $trad[$key];
	}
	
	public function getSconti($name)
	{
	
		
		$row = Db::getInstance()->getRow("SELECT * FROM specific_price WHERE id_product = $_GET[id_product] AND specific_price_name = '$name'");
		
		$sconto  = $row['reduction'];
		
		if($name == 'sc_riv_2' && !$row)
		{
			$name = 'sc_riv_1';
			$row = Db::getInstance()->getRow("SELECT * FROM specific_price WHERE id_product = $_GET[id_product] AND specific_price_name = '$name'");
			$due = 'y';
		}
		
		if($due == 'y')
		{
			$finito = $row['price'] - ($row['price'] / 100 * ($row['reduction']*100));
			
			$finito = $finito + ($finito / 100 * 3);
			
			$sconto = ($row['price'] - $finito) * 100 / $row['price'];
			
			$sconto = $sconto / 100;
			
		}

	
	
		return $sconto;
	
	}
	
	
		public function getQtSconti($name)
	{
	

		$row = Db::getInstance()->getRow("SELECT * FROM specific_price WHERE id_product = $_GET[id_product] AND specific_price_name = '$name'");
		
		
		$qt_sconto  = $row['from_quantity'];

		
		
		return $qt_sconto;
	
	}
	
	
	
	public function updSconti($sconto, $qta, $group, $specific_price_name, $product, $listino) {
	
		Db::getInstance()->executeS("REPLACE INTO specific_price (id_product, id_group, price, from_quantity, reduction, reduction_type, specific_price_name) VALUES ($product, $group, $listino, $qta, $sconto, 'percentage', '$specific_price_name')");
	
	}
	
	
	public function aggiornaCoseNonPredefinite($object) {
	
		
		//controllo compatibilita skype e se c'è linko il prodotto 
		

		/*
		if(!empty($_POST['confezione'])) {
		
		$trovaidquery = mysql_query("SELECT id_feature_value FROM feature_value ORDER BY id_feature_value DESC");
		
		
		mysql_query("DELETE FROM feature_product WHERE id_product = $object->id AND id_feature = 450");
		mysql_query("INSERT INTO feature_product (id_product, id_feature, id_feature_value) VALUES ($object->id, 450, $trovaid)");
		mysql_query("INSERT INTO feature_value (id_feature_value, id_feature, custom) VALUES ($trovaid, 450, 1)");
		mysql_query("INSERT INTO feature_value_lang (id_feature_value, id_lang, value) VALUES ($trovaid, 5, '$_POST[confezione]')");

		}
		else {
		mysql_query("DELETE FROM feature_product WHERE id_product = $object->id AND id_feature = 450");
		}
		*/
		/*
		if(!empty($_POST['garanzia'])) {
		
		mysql_query("DELETE FROM feature_product WHERE id_product = $object->id AND id_feature = 449");
		mysql_query("INSERT INTO feature_product (id_product, id_feature, id_feature_value) VALUES ($object->id, 449, $_POST[garanzia])");
		mysql_query("INSERT INTO feature_value (id_feature_value, id_feature, custom) VALUES ($_POST[garanzia], 449, 0)");
		
		}
		else {
		mysql_query("DELETE FROM feature_product WHERE id_product = $object->id AND id_feature = 450");
		}
		*/
		
		$spw_wholesale = Db::getInstance()->getValue('SELECT wholesale_price FROM specific_price_wholesale WHERE id_product = '.$object->id);
		
		if($spw_wholesale > 0)
		{
			
			if($object->listino != Tools::getValue('vecchio_listino'))
			{
				$spw = Db::getInstance()->executeS('SELECT * FROM specific_price_wholesale WHERE id_product = '.$object->id);
				foreach($spw as $s)
				{
					$new_spw = ($object->listino * ((100 - $s['reduction_1'])/100) * ((100 - $s['reduction_2'])/100) * ((100 - $s['reduction_3'])/100));
					Db::getInstance()->executeS('UPDATE specific_price_wholesale SET wholesale_price = '.$new_spw.' WHERE id_specific_price = '.$s['id_specific_price']);
					
				}
			}
		}
		
		$stock_quantity = Tools::getValue('stock_quantity');
		$supplier_quantity = Db::getInstance()->getValue('SELECT supplier_quantity FROM product  WHERE id_product = '.$object->id);
		$itancia_quantity = Db::getInstance()->getValue('SELECT itancia_quantity FROM product  WHERE id_product = '.$object->id);
		$esprinet_quantity = Db::getInstance()->getValue('SELECT esprinet_quantity FROM product  WHERE id_product = '.$object->id);
		$attiva_quantity = Db::getInstance()->getValue('SELECT attiva_quantity FROM product  WHERE id_product = '.$object->id);
		$ingram_quantity = Db::getInstance()->getValue('SELECT ingram_quantity FROM product  WHERE id_product = '.$object->id);
		$asit_quantity = Db::getInstance()->getValue('SELECT asit_quantity FROM product  WHERE id_product = '.$object->id);
		$intracom_quantity = Db::getInstance()->getValue('SELECT intracom_quantity FROM product  WHERE id_product = '.$object->id);
		$total_quantity = $stock_quantity + $supplier_quantity + $esprinet_quantity + $attiva_quantity + $ingram_quantity + $itancia_quantity + $asit_quantity + $intracom_quantity;

		
		Db::getInstance()->executes("UPDATE product SET stock_quantity = ".Tools::getValue('stock_quantity')." WHERE id_product = $object->id");
		Db::getInstance()->executes("UPDATE product SET quantity = ".$total_quantity." WHERE id_product = $object->id");

		Db::getInstance()->executes("UPDATE product SET id_category_gst = ".Tools::getValue('id_category_gst')." WHERE id_product = $object->id");
		
		if(Tools::getValue('provvigione') == '')
			Db::getInstance()->executes("UPDATE product SET provvigione = 0 WHERE id_product = $object->id");
		else
			Db::getInstance()->executes("UPDATE product SET provvigione = '".str_replace(',','.',Tools::getValue('provvigione'))."' WHERE id_product = $object->id");
		
		// amazon
		
		Db::getInstance()->executes("UPDATE product SET no_amazon = '".addslashes($_POST['no_amazon'])."', fnsku = '".$_POST['fnsku']."', amazon_free_shipping = '".(isset($_POST['amazon_free_shipping']) ? 1 : 0)."', sku_amazon = '".$_POST['sku_amazon']."', amazon_it = ".str_replace(',','.',$_POST['sconto_amazon_it']).", amazon_fr = ".str_replace(',','.',$_POST['sconto_amazon_fr']).", amazon_uk = ".str_replace(',','.',$_POST['sconto_amazon_uk']).", amazon_es = ".str_replace(',','.',$_POST['sconto_amazon_es']).", amazon_de = ".str_replace(',','.',$_POST['sconto_amazon_de']).", amazon_nl = ".str_replace(',','.',$_POST['sconto_amazon_nl']).", amazon_se = ".str_replace(',','.',$_POST['sconto_amazon_se']).", amazon_pl = ".str_replace(',','.',$_POST['sconto_amazon_pl'])." WHERE id_product = $object->id");
		
		// fine amazon
		
		Db::getInstance()->executes("UPDATE product_lang SET seo_keyword = '".$_POST['parola_chiave']."' WHERE id_lang = 5 AND id_product = ".$object->id."");
		
		Db::getInstance()->executes("REPLACE INTO product_esolver VALUES ($object->id, '".$_POST['rebate_1']."', '".$_POST['rebate_2']."', '".$_POST['rebate_3']."', '".$_POST['wholesale_price_esolver']."')");
		
		Db::getInstance()->executes("REPLACE INTO campi_esolver VALUES ($object->id, '".$_POST['tipo_anagrafica']."', '".$_POST['modello']."', '".addslashes($_POST['ean_descrizione'])."', '".addslashes($_POST['ean_tipo'])."', '".$_POST['ean_quantita']."', '".addslashes($_POST['intrastat'])."', '".addslashes($_POST['name_short'])."', '".addslashes($_POST['altro_esolver'])."', '".addslashes($_POST['tempo_approvvigionamento'])."', '".$_POST['codice_barre_fornitore']."', '".$_POST['pezzi_in_confezione']."', '".$_POST['nr']."', '".$_POST['pz']."', '".$_POST['commissione_amazon']."', '".$_POST['commissione_eprice']."', '".addslashes($_POST['note_2'])."', '".addslashes($_POST['premio_target'])."')");
		
		
		if(isset($_POST['cancellavideo'])) {
			
				foreach($_POST['cancellavideo'] as $cancvid) {
				
				Db::getInstance()->executes("DELETE FROM product_video WHERE id = $cancvid");
				}
				}
				
				if($_POST['video1'] != '') {
				
				$video1 = str_replace("youtu.be/", "www.youtube.com/embed/", $_POST['video1']);
				$video1 = preg_replace('/\&.*/', '', $video1);

			
			Db::getInstance()->executes("INSERT INTO product_video (id, id_product, content) VALUES (NULL, $object->id, '$video1')");
			
			}
					if($_POST['video2'] != '') {
					$video2 = str_replace("youtu.be", "www.youtube.com/embed/", $_POST['video2']);
			$video2 = preg_replace('/\&.*/', '', $video2);
			
			Db::getInstance()->executes("INSERT INTO product_video (id, id_product, content) VALUES (NULL, $object->id, '$video2')");
			
			}		if($_POST['video3'] != '') {
			$video3 = str_replace("youtu.be", "www.youtube.com/embed/", $_POST['video3']);
			$video3 = preg_replace('/\&.*/', '', $video3);
			
			Db::getInstance()->executes("INSERT INTO product_video (id, id_product, content) VALUES (NULL, $object->id, '$video3')");
			
			}
				
				
			
				$suppliers = array();
				foreach ($_POST['other_suppliers'] as $supplier) {
					$suppliers[] = $supplier;

				}
				$suppliers = serialize($suppliers);
				Db::getInstance()->executeS("UPDATE product SET other_suppliers = '".$suppliers."' WHERE id_product = ".$object->id);
				
			
			if(isset($_POST['sconto_rivenditore_1'])) {
			
			$_POST['sconto_rivenditore_1'] = $_POST['sconto_rivenditore_1']/100;
			
			Db::getInstance()->executes("DELETE FROM specific_price WHERE id_product = $object->id AND specific_price_name = 'sc_riv_1'");
			
			$this->updSconti("$_POST[sconto_rivenditore_1]", 0, 3, "sc_riv_1", $object->id, $object->listino);
			}
			
			if($_POST['sconto_rivenditore_1'] == 0) {
			
			$this->updSconti("$_POST[sconto_rivenditore_1]", 0, 3, "sc_riv_1", $object->id, $object->listino);
			Db::getInstance()->executes("DELETE FROM specific_price WHERE id_product = $object->id AND specific_price_name = 'sc_riv_1'");
			
			}
			
			
			
				if(isset($_POST['sconto_rivenditore_2'])) {
				
				$_POST['sconto_rivenditore_2'] = $_POST['sconto_rivenditore_2']/100;
				
			Db::getInstance()->executes("DELETE FROM specific_price WHERE id_product = $object->id AND specific_price_name = 'sc_riv_2'");
			
			//$this->updSconti("$_POST[sconto_rivenditore_2]", 0, 22, "sc_riv_2", $object->id, $object->listino);
			}
			
			
					if($_POST['sconto_rivenditore_2'] == 0) {
			
			$this->updSconti("$_POST[sconto_rivenditore_2]", 0, 22, "sc_riv_2", $object->id, $object->listino);
			Db::getInstance()->executes("DELETE FROM specific_price WHERE id_product = $object->id AND specific_price_name = 'sc_riv_2'");
			
			}
			
			
				if(isset($_POST['sconto_rivenditore_3'])) {
				
				$_POST['sconto_rivenditore_3'] = $_POST['sconto_rivenditore_3']/100;
				
			Db::getInstance()->executes("DELETE FROM specific_price WHERE id_product = $object->id AND specific_price_name = 'sc_riv_3'");
			
			$this->updSconti("$_POST[sconto_rivenditore_3]", 0, 12, "sc_riv_3", $object->id, $object->listino);
			}
			
			
					if($_POST['sconto_rivenditore_3'] == 0) {
			
			$this->updSconti("$_POST[sconto_rivenditore_3]", 0, 12, "sc_riv_3", $object->id, $object->listino);
			Db::getInstance()->executes("DELETE FROM specific_price WHERE id_product = $object->id AND specific_price_name = 'sc_riv_3'");
			
			}
			
			
				if(isset($_POST['sconto_quantita_1'])) {
				
					$_POST['sconto_quantita_1'] = $_POST['sconto_quantita_1'] / 100;
		
				
			Db::getInstance()->executes("DELETE FROM specific_price WHERE id_product = $object->id AND specific_price_name = 'sc_qta_1'");
			
			$this->updSconti("$_POST[sconto_quantita_1]", "$_POST[quantita_1]", 1, "sc_qta_1", $object->id, $object->price);
			}
			
			
					if($_POST['sconto_quantita_1'] == 0 || $_POST['quantita_1'] == 0) {
			
		
			
			$this->updSconti($scq1, $_POST['quantita_1'], 1, "sc_qta_1", $object->id, $object->price);
			Db::getInstance()->executes("DELETE FROM specific_price WHERE id_product = $object->id AND specific_price_name = 'sc_qta_1'");
			
			}
			
				
				if(isset($_POST['sconto_quantita_2'])) {
				
				$_POST['sconto_quantita_2'] = $_POST['sconto_quantita_2']/100;
			
			Db::getInstance()->executes("DELETE FROM specific_price WHERE id_product = $object->id AND specific_price_name = 'sc_qta_2'");
			$this->updSconti("$_POST[sconto_quantita_2]", "$_POST[quantita_2]", 1, "sc_qta_2", $object->id, $object->price);
			}
			
							if($_POST['sconto_quantita_2'] == 0 || $_POST['quantita_2'] == 0) {
			
			$this->updSconti("$_POST[sconto_quantita_2]", "$_POST[quantita_2]", 1, "sc_qta_2", $object->id, $object->price);
			Db::getInstance()->executes("DELETE FROM specific_price WHERE id_product = $object->id AND specific_price_name = 'sc_qta_2'");
			
			}
			
				if(isset($_POST['sconto_quantita_3'])) {
				
				$_POST['sconto_quantita_3'] = $_POST['sconto_quantita_3']/100;
			Db::getInstance()->executes("DELETE FROM specific_price WHERE id_product = $object->id AND specific_price_name = 'sc_qta_3'");
			
			$this->updSconti("$_POST[sconto_quantita_3]", "$_POST[quantita_3]", 1, "sc_qta_3", $object->id, $object->price);
			}
			
							if($_POST['sconto_quantita_3'] == 0 || $_POST['quantita_3'] == 0) {
			
			$this->updSconti("$_POST[sconto_quantita_3]", "$_POST[quantita_3]", 1, "sc_qta_3", $object->id, $object->price);
			Db::getInstance()->executes("DELETE FROM specific_price WHERE id_product = $object->id AND specific_price_name = 'sc_qta_3'");
			
			}
			
				if(isset($_POST['trovaprezzi_add'])) {
			Db::getInstance()->executeS("UPDATE product SET trovaprezzi = 1 WHERE id_product = $object->id");
			}
				else {
			Db::getInstance()->executeS("UPDATE product SET trovaprezzi = 0 WHERE id_product = $object->id");
			}
			
					
					if(isset($_POST['google_shopping_add'])) {
			Db::getInstance()->executeS("UPDATE product SET google_shopping = 1 WHERE id_product = $object->id");
			}
				else {
			Db::getInstance()->executeS("UPDATE product SET google_shopping = 0 WHERE id_product = $object->id");
			}
			
			if(isset($_POST['eprice_add'])) {
			Db::getInstance()->executeS("UPDATE product SET eprice = 1 WHERE id_product = $object->id");
			}
				else {
			Db::getInstance()->executeS("UPDATE product SET eprice = 0 WHERE id_product = $object->id");
			}
			
			$amazon_array = array();
			if(isset($_POST['amazon_it'])) 
				$amazon_array[] = 5;
			if(isset($_POST['amazon_fr'])) 
				$amazon_array[] = 2;	
			if(isset($_POST['amazon_es'])) 
				$amazon_array[] = 3;	
			if(isset($_POST['amazon_uk'])) 
				$amazon_array[] = 1;	
			if(isset($_POST['amazon_de'])) 
				$amazon_array[] = 4;
			if(isset($_POST['amazon_nl'])) 
				$amazon_array[] = 6;
			if(isset($_POST['amazon_se'])) 
				$amazon_array[] = 7;
			if(isset($_POST['amazon_se'])) 
				$amazon_array[] = 8;
			
			$amazon_string = implode(";",$amazon_array);
			
			Db::getInstance()->executeS("UPDATE product SET amazon = '".$amazon_string."' WHERE id_product = $object->id");
			if($amazon_string != '')
				Db::getInstance()->executeS("REPLACE INTO amazon_products (id_product) VALUES (".$object->id.")");
			
					if(isset($_POST['tutti_add'])) {
			Db::getInstance()->executeS("UPDATE product SET comparaprezzi_check = 1 WHERE id_product = $object->id");
			}
				else {
			Db::getInstance()->executeS("UPDATE product SET comparaprezzi_check = 0 WHERE id_product = $object->id");
			}
			
			if(isset($_POST['trovaprezzi_add']) && isset($_POST['google_shopping_add'])) {
			Db::getInstance()->executeS("UPDATE product SET comparaprezzi_check = 1 WHERE id_product = $object->id");
			}
			else {
			Db::getInstance()->executeS("UPDATE product SET comparaprezzi_check = 0 WHERE id_product = $object->id");
			}
			$languages = Language::getLanguages(false);
			foreach ($languages as $language)
			{
				$punti_di_forza = Tools::getValue('punti_di_forza_0_'.$language['id_lang']).':::::'.Tools::getValue('punti_di_forza_1_'.$language['id_lang']).':::::'.Tools::getValue('punti_di_forza_2_'.$language['id_lang']).':::::'.Tools::getValue('punti_di_forza_3_'.$language['id_lang']).':::::'.Tools::getValue('punti_di_forza_4_'.$language['id_lang']);
				
			
				Db::getInstance()->executeS('UPDATE product_lang SET punti_di_forza = "'.htmlentities(stripslashes($punti_di_forza), ENT_COMPAT, 'UTF-8').'" WHERE id_lang = '.$language['id_lang'].' AND id_product = '.$object->id);
			}
			
		/*if(isset($object->nuovo)) {
			Db::getInstance()->executeS("UPDATE product SET comparaprezzi_check = 1, kelkoo = 1, trovaprezzi = 1, pangora = 1, ciao = 1, google_shopping = 1 WHERE id_product = $object->id");

		}
		else { }*/
	
		if(isset($_POST['trasporto_gratuito_forzato'])) {

			$row = Db::getInstance()->getRow("SELECT value FROM configuration WHERE id_configuration = 240");

			$trasp = unserialize($row['value']);

			$au = $object->id;

			foreach($trasp as $el) {
			if ($au == $el) {
			$var_a = 1;
			break;
			}
			else {
			$var_a = 0;
			}
			}

			if($var_a == 0) {
			$trasp[] = $au;
			}
			else {
			}

			$rowtrasportogratuito = serialize($trasp);


			Db::getInstance()->executes("REPLACE INTO configuration (id_configuration, name, value) VALUES (240, 'FREE_SHIPPING_INCLUDE_PRD', '$rowtrasportogratuito')");

		}
		
		else if(!isset($_POST['trasporto_gratuito_forzato'])) 
		{

			$row = Db::getInstance()->getRow("SELECT value FROM configuration WHERE id_configuration = 240");

			$trasp = unserialize($row['value']);

			$au = $object->id;


			foreach($trasp as $el) {
			if ($au == $el) {
			$var_a = 1;
			break;
			}
			else {
			$var_a = 0;
			}
			}


			if($var_a == 1) {
			$key = array_search($au, $trasp);
				unset($trasp[$key]);

			}
			else {
			}


			$rowtrasportogratuito = serialize($trasp);
	
			Db::getInstance()->executes("REPLACE INTO configuration (id_configuration, name, value) VALUES (240, 'FREE_SHIPPING_INCLUDE_PRD', '$rowtrasportogratuito')");
	
		
		}
		
		// regola prodotto attivo senza dati
		
		$nome_prod = Db::getInstance()->getRow('SELECT name, description FROM product_lang WHERE id_lang = 5 AND id_product = '.Tools::getValue('id_product'));
		$code_prod = Db::getInstance()->getValue('SELECT supplier_reference FROM product WHERE id_product = '.Tools::getValue('id_product'));
		
		
	}
	
		
			
			
			function salvatutto() {
				$specificPrices = SpecificPrice::getByProductId((int)(Tools::getValue('id_product')));
				
				/*foreach($_POST['attachments'] as $f) {
				
				echo $f."<br />";
				
				}
				
				die();*/
				
				if(empty($_POST['attachments'])) {
				}
				else {
					Attachment::attachToProduct(Tools::getValue('id_product'), $_POST['attachments']);
				}
				
				Db::getInstance()->executeS('UPDATE order_detail SET category_id = '.Tools::getValue('id_category_default').' WHERE product_id = '.Tools::getValue('id_product'));
				
				foreach($specificPrices as $specificPrice) {
			
					$idprzspc = $specificPrice['id_specific_price'];
					$nomebottoneprzspc = "submitPricesModifiche_$idprzspc";

					$hiddenidprzspc = $_POST["id_specific_price_$idprzspc"];
					$fromquantityprzspc = $_POST["sp_from_quantity_$idprzspc"];
			 
					$currencyprzspc = $_POST["sp_id_currency_$idprzspc"];
					$wholesalepricespc = str_replace(",",".",$_POST["sp_wholesale_price_$idprzspc"]);
					$countryprzspc = $_POST["sp_id_country_$idprzspc"];
					$groupprzspc = $_POST["sp_id_group_$idprzspc"];
					$priceprzspc = str_replace(",",".",$_POST["sp_price_$idprzspc"]);
					$reductionprzspc = 0; // $_POST["sp_reduction_$idprzspc"]/100;
					$fromprzspc = date("Y-m-d 23:59:59", strtotime($_POST["sp_from_$idprzspc"]));
					$toprzspc = date("Y-m-d 23:59:59", strtotime($_POST["sp_to_$idprzspc"]));
					
					Db::getInstance()->executes("UPDATE `specific_price` SET `from_quantity` = '$fromquantityprzspc', 
					`id_currency` = '$currencyprzspc',
					`id_group` = '$groupprzspc',
					`id_country` = '$countryprzspc',
					`price` = '$priceprzspc',
					`wholesale_price` = '$wholesalepricespc',
					`reduction` = '$reductionprzspc',
					`from` = '$fromprzspc',
					`to` = '$toprzspc'
					WHERE `id_specific_price` = '$hiddenidprzspc'");
					
					$piecesspc =  $_POST["sp_pieces_$idprzspc"];
					
					if($piecesspc == '')
					{
						Db::getInstance()->getValue("DELETE FROM specific_price_pieces WHERE id_specific_price = ".$hiddenidprzspc."");
					
					}
					else
					{
						if($piecesspc >= 0) {
							$ctpcs = Db::getInstance()->getValue("SELECT count(id_specific_price) FROM specific_price_pieces WHERE `id_specific_price` = '$hiddenidprzspc'");
							if($ctpcs > 0) {
								$stockspc = Db::getInstance()->getValue("SELECT stock FROM specific_price_pieces WHERE id_specific_price = ".$hiddenidprzspc."");
								$stockspc = $piecesspc;
								
								Db::getInstance()->executeS("UPDATE `specific_price_pieces` SET `pieces` = '$piecesspc',`stock` = '$piecesspc' WHERE `id_specific_price` = '$hiddenidprzspc'");
							}
							else {
								Db::getInstance()->executeS("INSERT INTO `specific_price_pieces` (id_specific_price, id_product, pieces, stock) VALUES ('$hiddenidprzspc', ".Tools::getValue('id_product').", '$piecesspc', '$piecesspc')");
							}
						}
					}
					/*Tools::redirectAdmin($currentIndex.'&id_product='.(int)(Tools::getValue($this->identifier)).'&id_category='.(int)(Tools::getValue('id_category')).'&addproduct&conf=4&tabs=2&token='.($token ? $token : $this->token));
					*/
		
			
				}
				
				if (isset($_FILES['attachment_file']))
		{
		
			//$this->salvatutto();
			if ($this->tabAccess['add'] === '1')
			{

				$languages = Language::getLanguages(false);
				$is_attachment_name_valid = false;
				foreach ($languages as $language)
				{
					$attachment_name_lang = Tools::getValue('attachment_name_'.(int)($language['id_lang']));
					if (strlen($attachment_name_lang ) > 0)
						$is_attachment_name_valid = true;

					if (!Validate::isGenericName(Tools::getValue('attachment_name_'.(int)($language['id_lang']))))
						$this->_errors[] = Tools::displayError('Il nome del file allegato ha caratteri non validi');
					elseif (Tools::strlen(Tools::getValue('attachment_name_'.(int)($language['id_lang']))) > 32)
						$this->_errors[] = Tools::displayError('Nome del file allegato troppo lungo (max 32 caratteri)');
					if (!Validate::isCleanHtml(Tools::getValue('attachment_description_'.(int)($language['id_lang']))))
						$this->_errors[] = Tools::displayError('La descrizione del file allegato ha caratteri non validi');
				}
				if (!$is_attachment_name_valid) {
				
					$currentIndex.= "&nomeallegatononvalido";
					
					/*Tools::redirectAdmin($currentIndex.'&id_product='.(int)(Tools::getValue($this->identifier)).'&id_category='.(int)(Tools::getValue('id_category')).'&addproduct&tabs=6&token='.($token ? $token : $this->token));*/
					
								
				
				}

				if (empty($this->_errors))
				{
					if (isset($_FILES['attachment_file']) && is_uploaded_file($_FILES['attachment_file']['tmp_name']))
					{
						if ($_FILES['attachment_file']['size'] > (Configuration::get('PS_ATTACHMENT_MAXIMUM_SIZE') * 1024 * 1024))
							$this->_errors[] = $this->l('File too large, maximum size allowed:').' '.(Configuration::get('PS_ATTACHMENT_MAXIMUM_SIZE') * 1024).' '.$this->l('kb').'. '.$this->l('File size you\'re trying to upload is:').number_format(($_FILES['attachment_file']['size']/1024), 2, '.', '').$this->l('kb');
						else
						{
							do $uniqid = sha1(microtime());	while (file_exists(_PS_DOWNLOAD_DIR_.$uniqid));
							if (!copy($_FILES['attachment_file']['tmp_name'], _PS_DOWNLOAD_DIR_.$uniqid))
								$this->_errors[] = $this->l('File copy failed');
							@unlink($_FILES['attachment_file']['tmp_name']);
						}
					}
					elseif ((int)$_FILES['attachment_file']['error'] === 1)
					{
						$max_upload = (int)ini_get('upload_max_filesize');
						$max_post = (int)ini_get('post_max_size');
						$upload_mb = min($max_upload, $max_post);
						$this->_errors[] = $this->l('the File').' <b>'.$_FILES['attachment_file']['name'].'</b> '.$this->l('exceeds the size allowed by the server, this limit is set to').' <b>'.$upload_mb.$this->l('Mb').'</b>';
					}

					if (empty($this->_errors) && isset($uniqid))
					{
						
						$attachment = new Attachment();
						foreach ($languages AS $language)
						{
							if (isset($_POST['attachment_name_'.(int)($language['id_lang'])]))
								$attachment->name[(int)($language['id_lang'])] = pSQL($_POST['attachment_name_'.(int)($language['id_lang'])]);
							if (isset($_POST['attachment_description_'.(int)($language['id_lang'])]))
								$attachment->description[(int)($language['id_lang'])] = pSQL($_POST['attachment_description_'.(int)($language['id_lang'])]);
						}
						$attachment->file = $uniqid;
						$attachment->mime = $_FILES['attachment_file']['type'];
						$attachment->file_name = pSQL($_FILES['attachment_file']['name']);
						if (empty($attachment->mime) OR Tools::strlen($attachment->mime) > 128)
							$this->_errors[] = Tools::displayError('Invalid file extension');
						if (!Validate::isGenericName($attachment->file_name))
							$this->_errors[] = Tools::displayError('Invalid file name');
						if (Tools::strlen($attachment->file_name) > 128)
							$this->_errors[] = Tools::displayError('File name too long');
						if (!sizeof($this->_errors))
						{
							$attachment->add();
							$id_attachment = Db::getInstance()->getValue('SELECT id_attachment FROM attachment ORDER BY id_attachment DESC');
							
							Db::getInstance()->executes("INSERT INTO product_attachment (id_product, id_attachment) VALUES ($_GET[id_product], $id_attachment)");
							
							$array_degli_allegati = array();
							
							
							$rowalleg = Db::getInstance()->getRow("SELECT id_attachment FROM product_attachment WHERE id_product = $_GET[id_product]");
								
							$array_degli_allegati[] = $rowalleg['id_attachment'];
							
							
							
							
							$array_degli_allegati[] = $id_attachment;
							
							Attachment::attachToProduct($_GET['id_product'], $array_degli_allegati);
							
							/*Tools::redirectAdmin($currentIndex.'&id_product='.(int)(Tools::getValue($this->identifier)).'&id_category='.(int)(Tools::getValue('id_category')).'&addproduct&conf=4&tabs=6&token='.($token ? $token : $this->token));*/
						}
						else
							$this->_errors[] = Tools::displayError('Invalid file');
					}
				}
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to add here.');
			
			
		}
		
				if($_POST['reference'] == '') { 
			
				}
				else {
					$rowdoppio = Db::getInstance()->getRow("SELECT id_product, reference FROM product WHERE reference = '".$_POST['reference']."' AND active != 2");
			
					
						if($rowdoppio['reference'] == $_POST['reference']) {
			
							if($rowdoppio['id_product'] == $_GET['id_product']) {
					
							}
							else {
								$this->_errors[] = Tools::displayError('Esiste già un prodotto nel database associato al codice di riferimento che hai inserito. <a href=\'#\' onclick=\'document.getElementById("reference").scrollIntoView(); document.getElementById("reference").focus(); document.getElementById("reference").style.border = "3px solid red";\'>Clicca qui per andare all\'errore</a>');
							}
						}
			
					
				}
			
			
				if($_POST['supplier_reference'] == '') { }
			
				else {
					$rowdoppio = Db::getInstance()->getRow("SELECT id_product, supplier_reference FROM product WHERE active != 2");
			
			
						if($rowdoppio['supplier_reference'] === $_POST['supplier_reference']) {
			
			
							if($rowdoppio['id_product'] == $_GET['id_product']) {
							}
							else {
							$this->_errors[] = Tools::displayError('Esiste già un prodotto nel database associato al codice SKU che hai inserito. <a href=\'#supplier_reference\' onclick=\'document.getElementById("supplier_reference").scrollIntoView(); document.getElementById("supplier_reference").focus(); document.getElementById("supplier_reference").style.border = "3px solid red";\'>Clicca qui per andare all\'errore</a>');
							}
						}
			
					
				}
				
				if($_POST['active'] == 0 || $_POST['price'] == 0) {
					if(isset($_GET['id_product'])) {
						Db::getInstance()->executeS("UPDATE bundle SET active = 0 WHERE father = ".$_GET['id_product']);
									
						$child_bundles = Db::getInstance()->executeS("SELECT * FROM bundle WHERE bundle_products LIKE '%".$_GET['id_product']."%'");
						foreach($child_bundles as $child) {
									
							$products_bundle = explode(";",$child['bundle_products']);
										
							foreach ($products_bundle as $product_bundle) {
								if($product_bundle == $_GET['id_product']) {
									echo $child['id_bundle']."<br />";
									Db::getInstance()->executeS("UPDATE bundle SET active = 0 WHERE id_bundle = ".$child['id_bundle']);
								}
							}
						}
					}
				}
				else {
					if(isset($_GET['id_product'])) {
						Db::getInstance()->executeS("UPDATE bundle SET active = 1 WHERE father = ".$_GET['id_product']);
									
						$child_bundles = Db::getInstance()->executeS("SELECT * FROM bundle WHERE bundle_products LIKE '%".$_GET['id_product']."%'");
						foreach($child_bundles as $child) {
									
							$products_bundle = explode(";",$child['bundle_products']);
										
							foreach ($products_bundle as $product_bundle) {
								if($product_bundle == $_GET['id_product']) {
									echo $child['id_bundle']."<br />";
									Db::getInstance()->executeS("UPDATE bundle SET active = 1 WHERE id_bundle = ".$child['id_bundle']);
								}
							}
						}
					}
				}
			

				if (isset($_POST['inviaFeatures'])) {
			
					if (Validate::isLoadedObject($product = new Product((int)(Tools::getValue('id_product')))))
					{
						// delete all objects
						$product->deleteFeatures();

						// add new objects
						$languages = Language::getLanguages(false);
						foreach ($_POST AS $key => $val)
						{
							if (preg_match('/^feature_([0-9]+)_value/i', $key, $match))
							{
								if ($val)
									$product->addFeaturesToDB($match[1], $val);
								else
								{
									if ($default_value = $this->checkFeatures($languages, $match[1]))
									{
										$id_value = $product->addFeaturesToDB($match[1], 0, 1,  Tools::getValue('custom_'.$match[1].'_'.(int)$language['id_lang']));
										foreach ($languages AS $language)
										{
											if ($cust = Tools::getValue('custom_'.$match[1].'_'.(int)$language['id_lang']))
												$product->addFeaturesCustomToDB($id_value, (int)$language['id_lang'], $cust);
											else
												$product->addFeaturesCustomToDB($id_value, (int)$language['id_lang'], $default_value);
										}
									}
								}
							}
						}
					}
				
				}
		
		//$this->submitAddproduct($token);	
		global $cookie, $currentIndex, $link;		
		$className = 'Product';
		$rules = call_user_func(array($this->className, 'getValidationRules'), $this->className);
		$defaultLanguage = new Language((int)(Configuration::get('PS_LANG_DEFAULT')));
		$languages = Language::getLanguages(false);

		/* Check required fields */
		foreach ($rules['required'] AS $field)
			if (($value = Tools::getValue($field)) == false AND $value != '0')
			{
				if(Tools::getIsset('sku_provvisorio') && $field == 'reference')
				{
				}
				else
				{
					if (Tools::getValue('id_'.$this->table) AND $field == 'passwd')
						continue;
					$this->_errors[] = $this->l('the field').' <b>'.call_user_func(array($className, 'displayFieldName'), $field, $className).'</b> '.$this->l('is required');
				}
			}

		/* Check multilingual required fields */
		foreach ($rules['requiredLang'] AS $fieldLang)
			if (!Tools::getValue($fieldLang.'_'.$defaultLanguage->id))
				$this->_errors[] = $this->l('the field').' <b>'.call_user_func(array($className, 'displayFieldName'), $fieldLang, $className).'</b> '.$this->l('is required at least in').' '.$defaultLanguage->name;

		/* Check fields sizes */
		foreach ($rules['size'] AS $field => $maxLength)
			if ($value = Tools::getValue($field) AND Tools::strlen($value) > $maxLength)
				$this->_errors[] = $this->l('the field').' <b>'.call_user_func(array($className, 'displayFieldName'), $field, $className).'</b> '.$this->l('is too long').' ('.$maxLength.' '.$this->l('chars max').')';

		if (isset($_POST['description_short']))
		{
			$saveShort = $_POST['description_short'];
			$_POST['description_short'] = strip_tags($_POST['description_short']);
		}

		/* Check description short size without html */
		$limit = (int)Configuration::get('PS_PRODUCT_SHORT_DESC_LIMIT');
		if ($limit <= 0) $limit = 400;
		foreach ($languages AS $language)
			if ($value = Tools::getValue('description_short_'.$language['id_lang']))
				if (Tools::strlen(strip_tags($value)) > $limit)
					$this->_errors[] = $this->l('the field').' <b>'.call_user_func(array($className, 'displayFieldName'), 'description_short').' ('.$language['name'].')</b> '.$this->l('is too long').' : '.$limit.' '.$this->l('chars max').' ('.$this->l('count now').' '.Tools::strlen(strip_tags($value)).')';
		/* Check multilingual fields sizes */
		foreach ($rules['sizeLang'] AS $fieldLang => $maxLength)
			foreach ($languages AS $language)
				if ($value = Tools::getValue($fieldLang.'_'.$language['id_lang']) AND Tools::strlen($value) > $maxLength)
					$this->_errors[] = $this->l('the field').' <b>'.call_user_func(array($className, 'displayFieldName'), $fieldLang, $className).' ('.$language['name'].')</b> '.$this->l('is too long').' ('.$maxLength.' '.$this->l('chars max').')';
		if (isset($_POST['description_short']))
			$_POST['description_short'] = $saveShort;

		/* Check fields validity */
		foreach ($rules['validate'] AS $field => $function)
			if ($value = Tools::getValue($field))
				if (!Validate::$function($value))
					$this->_errors[] = $this->l('the field').' <b>'.call_user_func(array($className, 'displayFieldName'), $field, $className).'</b> '.$this->l('is invalid');

		/* Check multilingual fields validity */
		foreach ($rules['validateLang'] AS $fieldLang => $function)
			foreach ($languages AS $language)
				if ($value = Tools::getValue($fieldLang.'_'.$language['id_lang']))
					if (!Validate::$function($value))
						$this->_errors[] = $this->l('the field').' <b>'.call_user_func(array($className, 'displayFieldName'), $fieldLang, $className).' ('.$language['name'].')</b> '.$this->l('is invalid');

		/* Categories */
		$productCats = '';
		if (!Tools::isSubmit('categoryBox') OR !sizeof(Tools::getValue('categoryBox')))
			$this->_errors[] = $this->l('product must be in at least one Category');

		if (!is_array(Tools::getValue('categoryBox')) OR !in_array(Tools::getValue('id_category_default'), Tools::getValue('categoryBox')))
			$this->_errors[] = $this->l('product must be in the default category');

		/* Tags */
		foreach ($languages AS $language)
			if ($value = Tools::getValue('tags_'.$language['id_lang']))
				if (!Validate::isTagsListModificata($value))
					$this->_errors[] = $this->l('Tags list').' ('.$language['name'].') '.$this->l('is invalid');

		if (!sizeof($this->_errors))
		{
			$id = (int)(Tools::getValue('id_'.$this->table));
			$tagError = true;

			/* Update an existing product */
			if (isset($id) AND !empty($id))
			{
				$object = new $this->className($id);
				if (Validate::isLoadedObject($object))
				{
					$this->_removeTaxFromEcotax();
					$this->copyFromPost($object, $this->table);
					if ($object->update())
					{
						if ($id_reason = (int)Tools::getValue('id_mvt_reason') AND (int)Tools::getValue('mvt_quantity') > 0 AND $id_reason > 0)
						{
							$reason = new StockMvtReason((int)$id_reason);
							$qty = Tools::getValue('mvt_quantity') * $reason->sign;
							if (!$object->addStockMvt($qty, (int)$id_reason, NULL, NULL, (int)$cookie->id_employee))
								$this->_errors[] = Tools::displayError('An error occurred while updating qty.');
						}
						$this->updateAccessories($object);
						$this->updateAlternatives($object);
						$this->updateDownloadProduct($object);
						$this->aggiornaCoseNonPredefinite($object);
						//creazione tag automatica
						

						if (!$this->updatePackItems($object))
							$this->_errors[] = Tools::displayError('An error occurred while adding products to the pack.');
						elseif (!$object->updateCategories($_POST['categoryBox'], true))
							$this->_errors[] = Tools::displayError('An error occurred while linking object.').' <b>'.$this->table.'</b> '.Tools::displayError('To categories');
						elseif (!$this->updateTags($languages, $object))
							$this->_errors[] = Tools::displayError('An error occurred while adding tags.');
						elseif ($id_image = $this->addProductImage($object, Tools::getValue('resizer')))
						{
							$currentIndex .= '&image_updated='.(int)Tools::getValue('id_image');
							Hook::updateProduct($object);
							Search::indexation(false, $object->id);
							if (Tools::getValue('resizer') == 'man' && isset($id_image) AND is_int($id_image) AND $id_image)
								Tools::redirectAdmin($currentIndex.'&id_product='.$object->id.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&edit='.strval(Tools::getValue('productCreated')).'&id_image='.$id_image.'&imageresize&toconf=4&submitAddAndStay='.((Tools::isSubmit('submitAdd'.$this->table.'AndStay') OR Tools::getValue('productCreated') == 'on') ? 'on' : 'off').'&token='.(($token ? $token : $this->token)));

							// Save and preview
							if (Tools::isSubmit('submitAddProductAndPreview'))
							{
								$preview_url = ($link->getProductLink($this->getFieldValue($object, 'id'), $this->getFieldValue($object, 'link_rewrite', (int)($cookie->id_lang)), Category::getLinkRewrite($this->getFieldValue($object, 'id_category_default'), (int)($cookie->id_lang))));
								if (!$object->active)
								{
									$admin_dir = dirname($_SERVER['PHP_SELF']);
									$admin_dir = substr($admin_dir, strrpos($admin_dir,'/') + 1);
									$token = Tools::encrypt('PreviewProduct'.$object->id);
									if (strpos($preview_url, '?') === false)
										$preview_url .= '?';
									else
										$preview_url .= '&';
									$preview_url .= 'adtoken='.$token.'&ad='.$admin_dir;
								}
								
								Tools::redirectAdmin($preview_url);
							} elseif (Tools::isSubmit('submitAdd'.$this->table.'AndStay') OR (Tools::isSubmit('salvatutto')) OR  ($id_image AND $id_image !== true)) // Save and stay on same form
							// Save and stay on same form
							if (Tools::isSubmit('submitAdd'.$this->table.'AndStay') ||(Tools::isSubmit('salvatutto')))
								Tools::redirectAdmin($currentIndex.'&id_product='.$object->id.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&addproduct&conf=4&tabs='.(int)(Tools::getValue('tabs')).'&token='.($token ? $token : $this->token));

							// Default behavior (save and back)
							Tools::redirectAdmin($currentIndex.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&conf=4&token='.($token ? $token : $this->token).'&onredirigeici');
							
						}
						
					}
					else
						$this->_errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.'</b> ('.Db::getInstance()->getMsgError().')';
				}
				else
					$this->_errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.'</b> ('.Tools::displayError('Cannot load object').')';
			}

			/* Add a new product */
			else
			{
				$object = new $this->className();
				$this->_removeTaxFromEcotax();
				$this->copyFromPost($object, $this->table);
				if ($object->add())
				{
					$this->updateAccessories($object);
					$this->updateAlternatives($object);
					if (!$this->updatePackItems($object))
						$this->_errors[] = Tools::displayError('An error occurred while adding products to the pack.');
					$this->updateDownloadProduct($object);
					if (!sizeof($this->_errors))
					{
						if (!$object->updateCategories($_POST['categoryBox']))
							$this->_errors[] = Tools::displayError('An error occurred while linking object.').' <b>'.$this->table.'</b> '.Tools::displayError('To categories');
						elseif (!$this->updateTags($languages, $object))
							$this->_errors[] = Tools::displayError('An error occurred while adding tags.');
						elseif ($id_image = $this->addProductImage($object))
						{
							Hook::addProduct($object);
							Search::indexation(false, $object->id);

							// Save and preview
							if (Tools::isSubmit('submitAddProductAndPreview'))
							{
								$preview_url = ($link->getProductLink($this->getFieldValue($object, 'id'), $this->getFieldValue($object, 'link_rewrite', (int)($cookie->id_lang)), Category::getLinkRewrite($this->getFieldValue($object, 'id_category_default'), (int)($cookie->id_lang))));
								if (!$object->active)
								{
									$admin_dir = dirname($_SERVER['PHP_SELF']);
									$admin_dir = substr($admin_dir, strrpos($admin_dir,'/') + 1);
									$token = Tools::encrypt('PreviewProduct'.$object->id);
									$preview_url .= '&adtoken='.$token.'&ad='.$admin_dir;
								}

								Tools::redirectAdmin($preview_url);
							}
							$this->aggiornaCoseNonPredefinite($object);
							
							
							if (Tools::getValue('resizer') == 'man' && isset($id_image) AND is_int($id_image) AND $id_image)
								Tools::redirectAdmin($currentIndex.'&id_product='.$object->id.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&id_image='.$id_image.'&imageresize&toconf=3&submitAddAndStay='.(Tools::isSubmit('submitAdd'.$this->table.'AndStay') ? 'on' : 'off').'&token='.(($token ? $token : $this->token)));
							// Save and stay on same form
							if (Tools::isSubmit('submitAdd'.$this->table.'AndStay'))
								Tools::redirectAdmin($currentIndex.'&id_product='.$object->id.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&addproduct&conf=3&tabs='.(int)(Tools::getValue('tabs')).'&token='.($token ? $token : $this->token));
							// Default behavior (save and back)
							Tools::redirectAdmin($currentIndex.'&id_category='.(!empty($_REQUEST['id_category'])?$_REQUEST['id_category']:'1').'&conf=3&token='.($token ? $token : $this->token));
						}
					}
					else
						$object->delete();
				}
				else
					$this->_errors[] = Tools::displayError('An error occurred while creating object.').' <b>'.$this->table.'</b>';
			}
		}
			}
	

	
	public function displayListContent($token = NULL)
	{
		/* Display results in a table
		 *
		 * align  : determine value alignment
		 * prefix : displayed before value
		 * suffix : displayed after value
		 * image  : object image
		 * icon   : icon determined by values
		 * active : allow to toggle status
		 */

		global $currentIndex, $cookie;
		$currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));

		$id_category = 1; // default categ

		$irow = 0;
		if ($this->_list AND isset($this->fieldsDisplay['position']))
		{
			$positions = array_map(create_function('$elem', 'return (int)($elem[\'position\']);'), $this->_list);
			sort($positions);
		}
		if ($this->_list)
		{
			$isCms = false;
			if (preg_match('/cms/Ui', $this->identifier))
				$isCms = true;
			$keyToGet = 'id_'.($isCms ? 'cms_' : '').'category'.(in_array($this->identifier, array('id_category', 'id_cms_category')) ? '_parent' : '');
			foreach ($this->_list AS $tr)
			{
				$id = $tr[$this->identifier];
				echo '<tr '.(array_key_exists($this->identifier,$this->identifiersDnd) ? ' id="tr_'.(($id_category = (int)(Tools::getValue('id_'.($isCms ? 'cms_' : '').'category', '1'))) ? $id_category : '').'_'.$id.'_'.$tr['position'].'"' : '').($irow++ % 2 ? ' class="alt_row"' : '').' '.((isset($tr['color']) AND $this->colorOnBackground) ? 'style="background-color: '.$tr['color'].'"' : '').'>
							<td class="center">';
			
					echo '<input type="checkbox" name="'.$this->table.'Box[]" value="'.$id.'" class="noborder" />';
				echo '</td>';
				foreach ($this->fieldsDisplay AS $key => $params)
				{
					$tmp = explode('!', $key);
					$key = isset($tmp[1]) ? $tmp[1] : $tmp[0];
					echo '
					<td '.(isset($params['widthColumn']) ? 'style="'.($tr['id_category_default'] != $id_category ? 'font-style:italic; color:blue;' : '').' width: '.$params['widthColumn'].'px"' : '').' '.(isset($params['position']) ? ' id="td_'.(isset($id_category) AND $id_category ? $id_category : 0).'_'.$id.'"' : '').' class="'.((!isset($this->noLink) OR !$this->noLink) ? 'pointer' : '').((isset($params['position']) AND $this->_orderBy == 'position')? ' dragHandle' : ''). (isset($params['align']) ? ' '.$params['align'] : '').'" ';
					if (!isset($params['position']) AND (!isset($this->noLink) OR !$this->noLink))
						echo ' onclick="document.location = \''.$currentIndex.'&'.$this->identifier.'='.$id.($this->view? '&view' : '&update').$this->table.'&token='.($token!=NULL ? $token : $this->token).'\'">'.(isset($params['prefix']) ? $params['prefix'] : '');
					else
						echo '>';
					if (isset($params['active']) AND isset($tr[$key]))
						$this->_displayEnableLink($token, $id, $tr[$key], $params['active'], Tools::getValue('id_category'), Tools::getValue('id_product'));
					elseif (isset($params['activeVisu']) AND isset($tr[$key]))
						echo '<img src="../img/admin/'.($tr[$key] ? 'enabled.gif' : 'disabled.gif').'"
						alt="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" title="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" />';
					elseif (isset($params['position']))
					{
						if ($this->_orderBy == 'position' AND $this->_orderWay != 'DESC')
						{
							echo	'<img src="../img/admin/up-and-down.gif"
									alt="'.$this->l('Up').'" title="'.$this->l('Up').'" />';					}
						else
							echo (int)($tr[$key] + 1);
					}
					elseif (isset($params['image']))
					{
						// item_id is the product id in a product image context, else it is the image id.
						$item_id = isset($params['image_id']) ? $tr[$params['image_id']] : $id;
						// If it's a product image
						if (isset($tr['id_image']))
						{
							$image = new Image((int)$tr['id_image']);
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$image->getExistingImgPath().'.'.$this->imageType;
						}else
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$item_id.(isset($tr['id_image']) ? '-'.(int)($tr['id_image']) : '').'.'.$this->imageType;

						echo '<img src="http://www.ezdirect.it/img/'.($tr['id_image'] > 0 ? 'p/'.$item_id.'-'.$tr['id_image'] : 'm/'.$tr['id_manufacturer']).'-medium.jpg" style="width:60px; height:60px" alt="" />';
					}
					elseif (isset($params['icon']) AND (isset($params['icon'][$tr[$key]]) OR isset($params['icon']['default'])))
						echo '<img src="../img/admin/'.(isset($params['icon'][$tr[$key]]) ? $params['icon'][$tr[$key]] : $params['icon']['default'].'" alt="'.$tr[$key]).'" title="'.$tr[$key].'" />';
					elseif (isset($params['price']))
						echo (number_format($tr[$key],2,",",""));
					elseif (isset($params['float']))
						echo rtrim(rtrim($tr[$key], '0'), '.');
					elseif (isset($params['type']) AND $params['type'] == 'date')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang);
					elseif (isset($params['type']) AND $params['type'] == 'datetime')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang, true);
					elseif (isset($tr[$key]))
					{
						$echo = ($key == 'price' ? round($tr[$key], 2) : isset($params['maxlength']) ? Tools::substr($tr[$key], 0, $params['maxlength']).'...' : $tr[$key]);
						echo isset($params['callback']) ? call_user_func_array(array($this->className, $params['callback']), array($echo, $tr)) : $echo;
					}
					else
						echo '--';

					echo (isset($params['suffix']) ? $params['suffix'] : '').
					'</td>';
				}

				if ($this->edit OR $this->delete OR ($this->view AND $this->view !== 'noActionColumn'))
				{
					echo '<td class="center" style="white-space: nowrap;">';
					if ($this->view)
						$this->_displayViewLink($token, $id);
					if ($this->edit)
						$this->_displayEditLink($token, $id);
					if ($this->delete AND (!isset($this->_listSkipDelete) OR !in_array($id, $this->_listSkipDelete)))
						$this->_displayDeleteLink($token, $id);
					if ($this->duplicate)
						$this->_displayDuplicate($token, $id);
					echo '</td>';
				}
				echo '</tr>';
			}
		}
	}
	
		/**
	 * Add or update a bundle image
	 *
	 * @param object $product Product object to add image
	 */
	public function addBundleImage($id_bundle, $id_image)
	{
		/* Updating an existing product image */
		if ($id_image > 0)
		{
			Db::getInstance()->executeS("UPDATE bundle_image_lang SET legend = '".addslashes($_POST['image_legend'])."' WHERE id_image = ".$id_image."");
				
			$this->copyFromPost($image, 'image');
			if (sizeof($this->_errors)) {
					$this->_errors[] = Tools::displayError('An error occurred while updating image.');
			}
			else if (isset($_FILES['image_bundle']['tmp_name']) AND $_FILES['image_bundle']['tmp_name'] != NULL) {
					$imagesTypes = ImageType::getImagesTypes('products');
						foreach ($imagesTypes AS $k => $imageType) {
							@unlink(_PS_IMG_DIR_.'b/'.$id_image.'-'.$id_bundle.'-'.stripslashes($imageType['name']).'.'.'jpg', $imageType['width'], $imageType['height'], 'jpg');
						
						}
					$this->copyBundleImage($id_bundle, $id_image);
			}
		}

		/* Adding a new bundle image */
		else if (isset($_FILES['image_bundle']['name']) && $_FILES['image_bundle']['name'] != NULL )
		{
			
			$id_image = Db::getInstance()->getValue("SELECT id_image FROM bundle_image ORDER BY id_image DESC");
			$id_image = $id_image+1;
			Db::getInstance()->executeS("INSERT INTO bundle_image (id_image, id_bundle) VALUES (".$id_image.", ".$id_bundle.")");
			Db::getInstance()->executeS("INSERT INTO bundle_image_lang (id_image, id_lang, legend) VALUES (".$id_image.", 5, '".addslashes($_POST['image_legend'])."')");
			
			if ($error = checkImageUploadError($_FILES['image_bundle'])) {
				$this->_errors[] = $error;
			}
			if (!sizeof($this->_errors) AND isset($_FILES['image_bundle']['tmp_name']) AND $_FILES['image_bundle']['tmp_name'] != NULL)
			{
			
				if (!sizeof($this->_errors))
				{
					
					$this->copyBundleImage($id_bundle, $id_image);
					
				}
				
			}
		}
		/*if (isset($image) AND Validate::isLoadedObject($image) AND !file_exists(_PS_PROD_IMG_DIR_.$image->getExistingImgPath().'.'.$image->image_format))
			$image->delete();
		if (sizeof($this->_errors))
			return false;
		@unlink(_PS_TMP_IMG_DIR_.'/product_'.$product->id.'.jpg');
		@unlink(_PS_TMP_IMG_DIR_.'/product_mini_'.$product->id.'.jpg');
		return ((isset($id_image) AND is_int($id_image) AND $id_image) ? $id_image : true);*/
	}
	
	public function copyBundleImage($id_bundle, $id_image, $method = 'auto')
	{
		if (!isset($_FILES['image_bundle']['tmp_name']))
			return false;
		if ($error = checkImage($_FILES['image_bundle'], $this->maxImageSize))
			$this->_errors[] = $error;
		else
		{
			$path = _PS_IMG_DIR_.'b/'.$id_image."-".$id_bundle;
			
			if (!$new_path = $path)
				$this->_errors[] = Tools::displayError('An error occurred during new folder creation');
			if (!$tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS') OR !move_uploaded_file($_FILES['image_bundle']['tmp_name'], $tmpName))
				$this->_errors[] = Tools::displayError('An error occurred during the image upload');
			elseif (!imageResize($tmpName, $new_path.'.'.'jpg'))
				$this->_errors[] = Tools::displayError('An error occurred while copying image.');
			elseif ($method == 'auto')
			{
				$imagesTypes = ImageType::getImagesTypes('products');
				foreach ($imagesTypes AS $k => $imageType)
					if (!imageResize($tmpName, $new_path.'-'.stripslashes($imageType['name']).'.'.'jpg', $imageType['width'], $imageType['height'], 'jpg'))
						$this->_errors[] = Tools::displayError('An error occurred while copying image:').' '.stripslashes($imageType['name']);
			}

			@unlink($tmpName);
			Module::hookExec('watermark', array('id_image' => $id_image, 'id_product' => $id_product));
		}
	}
	
	public static function renderAdminGSTCategorieTree($trads, $selected_cat = array(), $input_name = 'categoryBox', $use_radio = false)
	{
		if (!$use_radio)
			$input_name = $input_name.'[]';
		
		$html = '';
		/*$html = '
		

		<script src="../js/admin-categories-gst-tree.js" type="text/javascript"></script>
		<script type="text/javascript">
			var inputName = "'.$input_name.'";
		';
		if (sizeof($selected_cat) > 0)
		{
			if (isset($selected_cat[0]))
				$html .= 'var selectedCat = "'.implode(',', $selected_cat).'"';
			else
				$html .= 'var selectedCat = "'.implode(',', array_keys($selected_cat)).'"';
		} else {
			$html .= 'var selectedCat = ""';
		}
		$html .= '
			var selectedLabel = \''.$trads['selected'].'\';
			var home = \''.$trads['Home'].'\';
			var use_radio = '.(int)$use_radio.';
		</script>
		';
		
		$html .= '
		<div style="background-color:#F4E6C9; width:99%;padding:5px 0 5px 5px;">
			<a href="#" id="collapse_all" >'.$trads['Collapse All'].'</a>
			 - <a href="#" id="expand_all" >'.$trads['Expand All'].'</a>
			'.(!$use_radio ? '
			 - <a href="#" id="check_all" >'.$trads['Check All'].'</a>
			 - <a href="#" id="uncheck_all" >'.$trads['Uncheck All'].'</a>
			' : '').'
		</div>
		';
		
		$home_is_selected = false;
		foreach($selected_cat AS $cat)
		{
			if (is_array($cat))
			{
				if  ($cat['id_category'] != 1)
					$html .= '<input type="hidden" name="'.$input_name.'" value="'.$cat['id_category'].'" >';
				else
					$home_is_selected = true;
			}
			else
			{
				if  ($cat != 1)
					$html .= '<input type="hidden" name="'.$input_name.'" value="'.$cat.'" >';
				else
					$home_is_selected = true;
			}
		}
		$html .= '
			<ul id="categories-gst-treeview" class="filetree">
				<li id="1" class="hasChildren">
					<span class="folder">  '.$trads['Home'].'</span>
					<ul>
						<li><span class="placeholder">&nbsp;</span></li>	
				  </ul>
				</li>
			</ul>';*/
			
			

		return $html;
	}
}

