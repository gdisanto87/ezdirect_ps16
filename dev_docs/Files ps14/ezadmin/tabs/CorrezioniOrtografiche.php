<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

class CorrezioniOrtografiche extends AdminPreferences
{


	public function display()
	{
		global $currentIndex;
		
		if(Tools::getIsset('confermalog')) {
			ob_end_clean();
			header('Content-type: text/csv');
			header("Content-Type: application/force-download");
			header('Content-Disposition: attachment; filename="correzioni.csv"');
			header("Connection: close");
			print Tools::getValue('log_file');
			exit();
		}

		else if(Tools::getIsset('submit_correzioni')) {
		
			$da_correggere = Tools::getValue('da_correggere');
			$correzione = Tools::getValue('correzione');
			$log_file = "Correzioni effettuate\n".$correzione." al posto di ".$da_correggere.". Correzioni effettuate in data ".date('d/m/Y')." alle ore ".date('H:i:s.')."\nDettaglio correzioni eseguite:\n\n\nNumero occorrenze;Tipo dato;Prod./Cat./art.;Codice prodotto\n";
			$matches = array();
			$products = Db::getInstance()->executeS("SELECT * FROM product_lang WHERE id_lang = 5");

			foreach($products as $product) {
				
				$cod_prodotto = Db::getInstance()->getValue("SELECT reference FROM product WHERE id_product = ".$product['id_product']."");
				if(preg_match('/ '.$da_correggere.' /', $product['description_short'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $product["description_short"],$matches).";descrizione breve;".$product["name"].";".$cod_prodotto."\n";
					Db::getInstance()->executeS("UPDATE product_lang SET description_short = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $product['description_short']))."' WHERE id_product = ".$product['id_product']."");
				
				}
				
				if(preg_match('/ '.$da_correggere.' /', $product['description'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $product["description"],$matches)." ;descrizione;".$product["name"].";".$cod_prodotto."\n";
					Db::getInstance()->executeS("UPDATE product_lang SET description = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $product['description']))."' WHERE id_product = ".$product['id_product']."");
				
				}
				
				if(preg_match('/ '.$da_correggere.' /', $product['meta_description'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $product["meta_description"],$matches)." ;meta description;".$product["name"].";".$cod_prodotto."\n";
					Db::getInstance()->executeS("UPDATE product_lang SET meta_description = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $product['meta_description']))."' WHERE id_product = ".$product['id_product']."");
				
				}
				
				
				if(preg_match('/ '.$da_correggere.' /', $product['meta_title'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $product["meta_title"],$matches)." ;title tag;".$product["name"].";".$cod_prodotto."\n";
					Db::getInstance()->executeS("UPDATE product_lang SET meta_title = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $product['meta_title']))."' WHERE id_product = ".$product['id_product']."");
				
				}
				
				if(preg_match('/ '.$da_correggere.' /', $product['title'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $product["title"],$matches)." ;titolo;".$product["name"].";".$cod_prodotto."\n";
					Db::getInstance()->executeS("UPDATE product_lang SET title = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $product['title']))."' WHERE id_product = ".$product['id_product']."");
				
				}
				
				if(preg_match('/ '.$da_correggere.' /', $product['name'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $product["name"],$matches)." ;nome;".$product["name"].";".$cod_prodotto."\n";
					Db::getInstance()->executeS("UPDATE product_lang SET name = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $product['name']))."' WHERE id_product = ".$product['id_product']."");
				
				}
				
				if(preg_match('/ '.$da_correggere.' /', $product['meta_keywords'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $product["meta_keywords"],$matches).";keywords;".$product["name"].";".$cod_prodotto."\n";
					Db::getInstance()->executeS("UPDATE product_lang SET meta_keywords = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $product['meta_keywords']))."' WHERE id_product = ".$product['id_product']."");
				
				}
				

			}
			
			$log_file .= "\n";

			$categories = Db::getInstance()->executeS("SELECT * FROM category_lang WHERE id_lang = 5");

			foreach($categories as $category) {

				
				if(preg_match('/ '.$da_correggere.' /', $category['meta_description'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $category["meta_description"],$matches)." ;meta description;".$category["name"]."\n";
					Db::getInstance()->executeS("UPDATE category_lang SET meta_description = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $category['meta_description']))."' WHERE id_category = ".$category['id_category']."");
				
				}
				
				if(preg_match('/ '.$da_correggere.' /', $category['description'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $category["description"],$matches)." ;descrizione;".$category["name"]."\n";
					Db::getInstance()->executeS("UPDATE category_lang SET description = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $category['description']))."' WHERE id_category = ".$category['id_category']."");
				
				}
				
				if(preg_match('/ '.$da_correggere.' /', $category['meta_title'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $category["meta_title"],$matches)." ;title tag;".$category["name"]."\n";
					Db::getInstance()->executeS("UPDATE category_lang SET meta_title = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $category['meta_title']))."' WHERE id_category = ".$category['id_category']."");
				
				}
				
				if(preg_match('/ '.$da_correggere.' /', $category['title'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $category["title"],$matches)." ;titolo;".$category["name"]."\n";
					Db::getInstance()->executeS("UPDATE category_lang SET title = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $category['title']))."' WHERE id_category = ".$category['id_category']."");
				
				}
				
				if(preg_match('/ '.$da_correggere.' /', $category['name'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $category["name"],$matches)." ;nome;".$category["name"]."\n";
					Db::getInstance()->executeS("UPDATE category_lang SET name = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $category['name']))."' WHERE id_category = ".$category['id_category']."");
				
				}
				
				if(preg_match('/ '.$da_correggere.' /', $category['meta_keywords'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $category["meta_keywords"],$matches)." ;keywords;".$category["name"]."\n";
					Db::getInstance()->executeS("UPDATE category_lang SET meta_keywords = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $category['meta_keywords']))."' WHERE id_category = ".$category['id_category']."");
				
				}
				

			}
			
			$log_file .= "\n";

			$cmss = Db::getInstance()->executeS("SELECT * FROM cms_lang WHERE id_lang = 5");

			foreach($cmss as $cms) {

				
				if(preg_match('/ '.$da_correggere.' /', $cms['meta_description'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $cms["meta_description"],$matches)." ; meta description;".$cms["meta_title"]."\n";
					Db::getInstance()->executeS("UPDATE cms_lang SET meta_description = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $cms['meta_description']))."' WHERE id_cms = ".$cms['id_cms']."");
				
				}
				
				if(preg_match('/ '.$da_correggere.' /', $cms['content'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $cms["content"],$matches)." ;contenuto;".$cms["meta_title"]."\n";
					Db::getInstance()->executeS("UPDATE cms_lang SET content = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $cms['content']))."' WHERE id_cms = ".$cms['id_cms']."");
				
				}
				
				if(preg_match('/ '.$da_correggere.' /', $cms['meta_title'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $cms["meta_title"],$matches)." ; title tag;".$cms["meta_title"]."\n";
					Db::getInstance()->executeS("UPDATE cms_lang SET meta_title = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $cms['meta_title']))."' WHERE id_cms = ".$cms['id_cms']."");
				
				}
				
				
				if(preg_match('/ '.$da_correggere.' /', $cms['meta_keywords'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $cms["meta_keywords"],$matches)." ; title tag;".$cms["meta_title"]."\n";
					Db::getInstance()->executeS("UPDATE cms_lang SET meta_keywords = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $cms['meta_keywords']))."' WHERE id_cms = ".$cms['id_cms']."");
				
				}
				

			}
			
			$log_file .= "\n";
			
			$bundles = Db::getInstance()->executeS("SELECT * FROM bundle");

			foreach($bundles as $bundle) {

				if(preg_match('/ '.$da_correggere.' /', $bundle['bundle_name'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $bundle["bundle_name"],$matches).";nome;".$bundle["bundle_name"].";".$bundle["bundle_ref"]."\n";
					Db::getInstance()->executeS("UPDATE bundle SET bundle_name = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $bundle['bundle_name']))."' WHERE id_bundle = ".$bundle['id_bundle']."");
				
				}
				
			}

			$log_file = htmlentities($log_file, ENT_QUOTES);
			
			echo "<div class='conf'>";
			echo "Correzioni eseguite!</div>";
			echo '&Egrave; stato creato un file log con il dettaglio delle correzioni eseguite. <br /><br />
			<form action="'.$currentIndex.'&token='.$this->token.'&filename=correzioni.txt" method="post">
			<input type="hidden" name="log_file" value="'.$log_file.'" />
			<input type="submit" class="button" name="confermalog" value="Clicca qui se vuoi scaricare il file delle correzioni" /><br /><br />
			<a href="'.$currentIndex.'&token='.$this->token.'" class="button">Clicca se vuoi fare una nuova correzione</a>.
			
			';
		
		}
		
		else {
		
			echo '<h1>Correzioni ortografiche</h1>';
			echo 'Con questo form &egrave; possibile fare correzioni ortografiche su tutto il sito (prodotti, categorie, guide). Per ragioni;sicurezza &egrave; possibile sostituire <strong>solo parole intere</strong> e non parti;parola. Inserisci la parola DA CORREGGERE nel primo campo e la CORREZIONE nel secondo campo.<br /><br />';
			
			echo '<form action="'.$currentIndex.'&token='.$this->token.'" method="post">';
			echo '<table>';
			echo '<thead><tr><th></th><th></th></tr></thead><tbody>';
			echo '<tr><td>Parola da correggere</td><td><input type="text" size="30" name="da_correggere" /></td></tr>';
			echo '<tr><td>Correzione</td><td><input type="text" size="30" name="correzione" /></td></tr>';
			echo '<tr><td><input type="submit" name="submit_correzioni" value="Clicca per correggere" class="button" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" /></td><td></td></tr>';
			echo '</tbody></table>';
			echo '</form>';
		}
		
		
		
	}

}


