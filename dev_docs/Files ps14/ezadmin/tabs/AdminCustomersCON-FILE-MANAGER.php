<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14516 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminCustomers extends AdminTab
{
	public function __construct()
	{
	 	$this->table = 'customer';
	 	$this->className = 'Customer';
	 	$this->lang = false;
	 	$this->edit = false;
	 	$this->view = true;
	 	$this->delete = true;
		$this->deleted = true;
		$this->requiredDatabase = true;

		$this->_select = '
		CONCAT(a.firstname," ",a.lastname) customer,
		
		(CASE a.is_company
		WHEN 1
		THEN a.vat_number
		WHEN 0
		THEN a.tax_code
		ELSE a.tax_code
		END
		) piva_cf,
		
		(SELECT iso_code FROM state WHERE id_state = (SELECT id_state FROM address WHERE id_customer = a.id_customer AND fatturazione = 1 AND active = 1 AND deleted = 0 LIMIT 1)) provincia,
		
		
		(YEAR(CURRENT_DATE)-YEAR(`birthday`)) - (RIGHT(CURRENT_DATE, 5)<RIGHT(`birthday`, 5)) as age, (
			SELECT c.date_add FROM '._DB_PREFIX_.'guest g
			LEFT JOIN '._DB_PREFIX_.'connections c ON c.id_guest = g.id_guest
			WHERE g.id_customer = a.id_customer
			ORDER BY c.date_add DESC
			LIMIT 1
		) as connect';
		$genders = array(1 => $this->l('M'), 2 => $this->l('F'), 9 => $this->l('?'));
 		$this->fieldsDisplay = array(
		'id_customer' => array('title' => $this->l('ID'), 'align' => 'center', 'font-size' =>'12px', 'width' => 25),
		'codice_spring' => array('title' => $this->l('Cod. Spring'), 'align' => 'center', 'font-size' =>'12px',  'width' => 25),
		'company' => array('title' => $this->l('Company'), 'font-size' =>'14px',  'width' => 80),

		'customer' => array('title' => $this->l('Persona'), 'width' => 70, 'filter_key' => 'customer', 'tmpTableFilter' => true),
		'id_default_group' => array('title' => $this->l('Group'), 'width' => 30),
		'supplier' => array('title' => $this->l('Forn.'), 'width' => 25, 'align' => 'center', 'supplier' => 'status', 'type' => 'bool', 'orderby' => false),
		'provincia' => array('title' => $this->l('Prov.'), 'width' => 30, 'filter_key' => 'provincia', 'tmpTableFilter' => true),
		'piva_cf' => array('title' => $this->l('P.I. / CF'), 'width' => 40, 'filter_key' => 'piva_cf', 'tmpTableFilter' => true),
		'email' => array('title' => $this->l('E-mail address'), 'width' => 120, 'maxlength' => 19),
		
		'active' => array('title' => $this->l('Enabled'), 'width' => 25, 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false),
		
		'date_add' => array('title' => $this->l('Registration'), 'width' => 30, 'type' => 'date', 'align' => 'right'),
		//'connect' => array('title' => $this->l('Connection'), 'width' => 60, 'type' => 'datetime', 'search' => false)
		);

		$this->optionTitle = $this->l('Customers options');
		$this->_fieldsOptions = array(
			'PS_PASSWD_TIME_FRONT' => array('title' => $this->l('Regenerate password:'), 'desc' => $this->l('Security minimum time to wait to regenerate the password'),'validation' => 'isUnsignedInt', 'cast' => 'intval', 'size' => 5, 'type' => 'text', 'suffix' => ' '.$this->l('minutes'))
		);

		parent::__construct();
	}

	public function postProcess()
	{
		global $currentIndex;
		
		
		
		if(isset($_GET['updatecustomer']) && isset($_GET['conf']) && $_GET['conf'] == 4) {
			Tools::redirectAdmin($currentIndex.'&id_customer='.$_GET['id_customer'].'&viewcustomer&token='.$this->token.'&tab-container-1=1');
		}
		
		if(isset($_POST['submitAddcustomer'])) {
			
			if($_POST['tax_exempt'] == 1) {
				Db::getInstance()->executeS("UPDATE customer SET tax_exempt = 1 WHERE id_customer = ".$_POST['id_customer']."");
			}
			
			else {
				Db::getInstance()->executeS("UPDATE customer SET tax_exempt = 0 WHERE id_customer = ".$_POST['id_customer']."");
			}
			
			if($_POST['supplier'] == 1) {
				Db::getInstance()->executeS("UPDATE customer SET supplier = 1 WHERE id_customer = ".$_POST['id_customer']."");
			}
			
			else {
				Db::getInstance()->executeS("UPDATE customer SET supplier = 0 WHERE id_customer = ".$_POST['id_customer']."");
			}
			
			Db::getInstance()->executeS("UPDATE customer SET keyword = '".Tools::getValue('keyword')."' WHERE id_customer = ".$_POST['id_customer']."");
		
		}

		if (Tools::isSubmit('submitDel'.$this->table) OR Tools::isSubmit('delete'.$this->table))
		{
			$deleteForm = '
			<form action="'.htmlentities($_SERVER['REQUEST_URI']).'" method="post">
				<fieldset><legend>'.$this->l('How do you want to delete your customer(s)?').'</legend>
					'.$this->l('You have two ways to delete a customer, please choose what you want to do.').'
					<p>
						<input type="radio" name="deleteMode" value="real" id="deleteMode_real" />
						<label for="deleteMode_real" style="float:none">'.$this->l('I want to delete my customer(s) for real, all data will be removed from the database. A customer with the same e-mail address will be able to register again.').'</label>
					</p>
					<p>
						<input type="radio" name="deleteMode" value="deleted" id="deleteMode_deleted" />
						<label for="deleteMode_deleted" style="float:none">'.$this->l('I don\'t want my customer(s) to register again. The customer(s) will be removed from this list but all data will be kept in the database.').'</label>
					</p>';
			foreach ($_POST as $key => $value)
				if (is_array($value))
					foreach ($value as $val)
						$deleteForm .= '<input type="hidden" name="'.htmlentities($key).'[]" value="'.htmlentities($val).'" />';
				else
					$deleteForm .= '<input type="hidden" name="'.htmlentities($key).'" value="'.htmlentities($value).'" />';
			$deleteForm .= '	<br /><input type="submit" class="button" value="'.$this->l('   Delete   ').'" />
				</fieldset>
			</form>
			<div class="clear">&nbsp;</div>';
		}

		if (Tools::getValue('submitAdd'.$this->table))
		{
				
		 	$groupList = Tools::getValue('groupBox');

		 	/* Checking fields validity */
			$this->validateRules();
			if (!sizeof($this->_errors))
			{
				$id = (int)(Tools::getValue('id_'.$this->table));
				if (isset($id) AND !empty($id))
				{
					if ($this->tabAccess['edit'] !== '1')
						$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
					else
					{
						$object = new $this->className($id);
						if (Validate::isLoadedObject($object))
						{
							//AGGIORNO IL CODICE SPRING
							$codice_spring = Tools::getValue('codice_spring');
							
							mysql_query("UPDATE customer SET codice_spring = '$codice_spring' WHERE id_customer = $id");
							
							
							//AGGIORNO I NUMERI DI TELEFONO
							$telefono = Tools::getValue('telefono');
							$cellulare = Tools::getValue('cellulare');
							$fax = Tools::getValue('fax');
							$idindirizzo = Tools::getValue('idindirizzo');
			
								mysql_query("UPDATE address SET phone = '$telefono' WHERE address.deleted != 1 AND address.id_address = $idindirizzo AND address.fatturazione = 1");
							
					
								mysql_query("UPDATE address SET phone_mobile = '$cellulare' WHERE address.deleted != 1 AND address.id_address = $idindirizzo AND address.fatturazione = 1");
					
								mysql_query("UPDATE address SET fax = '$fax' WHERE address.deleted != 1 AND address.id_address = $idindirizzo AND address.fatturazione = 1");
						
									//MANDO LA MAIL PER ABILITAZIONE RIVENDITORI
									
									$resdefgroup = mysql_query("SELECT id_default_group FROM customer WHERE id_customer = $_GET[id_customer]");
									$rowdefgroup = mysql_fetch_array($resdefgroup);
									
									$cliente_id_default_group = $rowdefgroup['id_default_group'];
									
									$headers_nostri  = 'MIME-Version: 1.0' . "\n";
									$headers_nostri .= 'Content-Type: text/html' ."\n";
									$headers_nostri .= 'From: Ezdirect <info@ezdirect.it>' . "\n";
									$message = "<html>
									<head>
									<title>Abilitazione rivenditore</title>
									</head>
									<body>
									<table style='font-family: Verdana,sans-serif; font-size: 11px; color: #374953; width: 550px;'>
									<tr>
									<td align='left'><a title='Ezdirect' href='http://www.ezdirect.it'><img style='border: none;' src='http://www.ezdirect.it/img/logo.jpg' alt='Ezdirect' /></a></td>
									</tr>
									<tr>
									<td style='background-color: #db6909; color: #fff; font-size: 12px; font-weight: bold; padding: 0.5em 1em;' align='left'>Sei stato abilitato come rivenditore</td>
									</tr>
									<td>
									Grazie per la registrazione. Abbiamo abilitato il tuo account ai prezzi riservati.
<br /><br />
<strong>Avvisi:</strong>
Ti comunichiamo che il minimo ordinabile &egrave; di 150,00 euro iva esclusa
Alcuni prodotti possono essere visualizzati a prezzo netto, (senza sconti, per quantità richiedi quotazione)
 <br /><br />
<strong>Per comunicare con Ezdirect e richiedere assistenza:</strong><br /><br />

<strong>Per richiedere supporto nella configurazione di sistemi e/o sui prodotti utilizza i moduli predisposti o chiama 0585821163</strong><br />
Per richiedere assistenza tecnica <strong>postvendita</strong><br /> <a href='http://www.ezdirect.it/contattaci?step=tecnica' target='_blank'>clicca su questo link</a>
 <br /><br />
Per richiedere assistenza amministrativa e contabilità <a href='http://www.ezdirect.it/contattaci?step=contabilita' target='_blank'>clicca su questo link</a><br />
Per richiedere informazioni sullo stato degli ordini <a href='http://www.ezdirect.it/contattaci?step=ordini' target='_blank'>clicca su questo link</a><br /><br />
 
Grazie per la collaborazione
 <br /><br />
Staff Ezdirect
									</table>
									</body>
									</html>";									
									
								if($object->id_default_group == 1 && Tools::getValue('id_default_group') == 3) {
								mail($object->email, "Ezdirect - Abilitazione rivenditore", $message, $headers_nostri);
								}		
								
							$customer_email = strval(Tools::getValue('email'));

							// check if e-mail already used
							if ($customer_email != $object->email)
							{
								$customer = new Customer();
								$customer->getByEmail($customer_email);
								if ($customer->id)
									$this->_errors[] = Tools::displayError('An account already exists for this e-mail address:').' '.$customer_email;
							}
							
							Db::getInstance()->executeS("DELETE FROM customer_group WHERE id_customer = ".Tools::getValue('id_customer')."");
							Db::getInstance()->executeS("INSERT INTO customer_group (id_customer, id_group) VALUES (".Tools::getValue('id_customer').", ".Tools::getValue('id_default_group').")");
							
							
							
							/*

							if (!is_array($groupList) OR sizeof($groupList) == 0)
								$this->_errors[] = Tools::displayError('Customer must be in at least one group.');
							else
								if (!in_array(Tools::getValue('id_default_group'), $groupList))
									$this->_errors[] = Tools::displayError('Default customer group must be selected in group box.');
									
												

							// Updating customer's group
							if (!sizeof($this->_errors))
							{
								$object->cleanGroups();
								if (is_array($groupList) AND sizeof($groupList) > 0)
									$object->addGroups($groupList);
							}
							*/
						}
						else
							$this->_errors[] = Tools::displayError('An error occurred while loading object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
					}
				
				}
				else
				{
					if ($this->tabAccess['add'] === '1')
					{
						$object = new $this->className();
						$this->copyFromPost($object, $this->table);
						if (!$object->add())
							$this->_errors[] = Tools::displayError('An error occurred while creating object.').' <b>'.$this->table.' ('.mysql_error().')</b>';
						elseif (($_POST[$this->identifier] = $object->id /* voluntary */) AND $this->postImage($object->id) AND !sizeof($this->_errors) AND $this->_redirect)
						{
							// Add Associated groups
							$group_list = Tools::getValue('groupBox');
							if (is_array($group_list) && sizeof($group_list) > 0)
								$object->addGroups($group_list, true);
							$parent_id = (int)(Tools::getValue('id_parent', 1));
							// Save and stay on same form
							if (Tools::isSubmit('submitAdd'.$this->table.'AndStay'))
								Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$object->id.'&conf=3&update'.$this->table.'&token='.$this->token);
								
							// Save and back to parent
							if (Tools::isSubmit('submitAdd'.$this->table.'AndBackToParent'))
								Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$parent_id.'&conf=3&token='.$this->token);
							// Default behavior (save and back)
							Tools::redirectAdmin($currentIndex.($parent_id ? '&'.$this->identifier.'='.$object->id : '').'&conf=3&token='.$this->token);
						}
					}
					else
						$this->_errors[] = Tools::displayError('You do not have permission to add here.');
				}
			}
		}
		elseif (Tools::isSubmit('delete'.$this->table) AND $this->tabAccess['delete'] === '1')
		{
			switch (Tools::getValue('deleteMode'))
			{
				case 'real':
					$this->deleted = false;
					Db::getInstance()->executeS("DELETE FROM persone WHERE id_customer = ".Tools::getValue('id_customer')."");
					Discount::deleteByIdCustomer((int)(Tools::getValue('id_customer')));
					break;
				case 'deleted':
					$this->deleted = true;
					break;
				default:
					echo $deleteForm;
					if (isset($_POST['delete'.$this->table]))
						unset($_POST['delete'.$this->table]);
					if (isset($_GET['delete'.$this->table]))
						unset($_GET['delete'.$this->table]);
					break;
			}
		}
		elseif (Tools::isSubmit('submitDel'.$this->table) AND $this->tabAccess['delete'] === '1')
		{
			switch (Tools::getValue('deleteMode'))
			{
				case 'real':
					$this->deleted = false;
					foreach (Tools::getValue('customerBox') as $id_customer)
						Discount::deleteByIdCustomer((int)($id_customer));
					break;
				case 'deleted':
					$this->deleted = true;
					break;
				default:
					echo $deleteForm;
					if (isset($_POST['submitDel'.$this->table]))
						unset($_POST['submitDel'.$this->table]);
					if (isset($_GET['submitDel'.$this->table]))
						unset($_GET['submitDel'.$this->table]);
					break;
			}
		}
		elseif (Tools::isSubmit('submitGuestToCustomer') AND Tools::getValue('id_customer'))
		{
			if ($this->tabAccess['edit'] === '1')
			{
				$customer = new Customer((int)Tools::getValue('id_customer'));
				if (!Validate::isLoadedObject($customer))
					$this->_errors[] = Tools::displayError('This customer does not exist.');
				if (Customer::customerExists($customer->email))
					$this->_errors[] = Tools::displayError('This customer already exist as non-guest.');
				elseif ($customer->transformToCustomer(Tools::getValue('id_lang', Configuration::get('PS_LANG_DEFAULT'))))
					Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$customer->id.'&conf=3&token='.$this->token);
				else
					$this->_errors[] = Tools::displayError('An error occurred while updating customer.');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
		}elseif (Tools::isSubmit('changeNewsletterVal') AND Tools::getValue('id_customer'))
		{
			$id_customer = (int)Tools::getValue('id_customer');
			$customer = new Customer($id_customer);
			if (!Validate::isLoadedObject($customer))
				$this->_errors[] = Tools::displayError('An error occurred while updating customer.');
			$update = Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'customer` SET newsletter = '.($customer->newsletter ? 0 : 1).' WHERE `id_customer` = '.(int)($customer->id));
			if (!$update)
				$this->_errors[] = Tools::displayError('An error occurred while updating customer.');
			Tools::redirectAdmin($currentIndex.'&token='.$this->token);

		}elseif (Tools::isSubmit('changeOptinVal') AND Tools::getValue('id_customer'))
		{
			$id_customer = (int)Tools::getValue('id_customer');
			$customer = new Customer($id_customer);
			if (!Validate::isLoadedObject($customer))
				$this->_errors[] = Tools::displayError('An error occurred while updating customer.');
			$update = Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'customer` SET optin = '.($customer->optin ? 0 : 1).' WHERE `id_customer` = '.(int)($customer->id));
			if (!$update)
				$this->_errors[] = Tools::displayError('An error occurred while updating customer.');
			Tools::redirectAdmin($currentIndex.'&token='.$this->token);
		}
		
	
					
		return parent::postProcess();
	}

	public function viewcustomer()
	{
		ini_set('meemory_limit', '96M');
		ini_set('post_max_size', '64M');
		ini_set('upload_max_filesize', '64M');
		global $currentIndex, $cookie, $link;
		$tokenAddresses = Tools::getAdminToken('AdminAddresses'.(int)(Tab::getIdFromClassName('AdminAddresses')).(int)($cookie->id_employee));
		$irow = 0;
		$configurations = Configuration::getMultiple(array('PS_LANG_DEFAULT', 'PS_CURRENCY_DEFAULT'));
		$defaultLanguage = (int)($configurations['PS_LANG_DEFAULT']);
		$defaultCurrency = (int)($configurations['PS_CURRENCY_DEFAULT']);
		if (!($customer = $this->loadObject()))
			return;
		$customerStats = $customer->getStats();
		$addresses_fatt = $customer->getAddressesFatturazione($defaultLanguage);
		$addresses_cons = $customer->getAddressesConsegna($defaultLanguage);
		$products = $customer->getBoughtProducts();
		$discounts = Discount::getCustomerDiscounts($defaultLanguage, (int)$customer->id, false, false);
		$orders = Order::getCustomerOrders((int)$customer->id, true);
		$carts = Cart::getCustomerCarts((int)$customer->id);
		$groups = $customer->getGroups();
		$messages = CustomerThread::getCustomerMessages((int)$customer->id);
		$referrers = Referrer::getReferrers((int)$customer->id);
		if ($totalCustomer = Db::getInstance()->getValue('SELECT SUM(total_paid_real) FROM '._DB_PREFIX_.'orders WHERE id_customer = '.$customer->id.' AND valid = 1'))
		{
			Db::getInstance()->getValue('SELECT SQL_CALC_FOUND_ROWS COUNT(*) FROM '._DB_PREFIX_.'orders o WHERE o.id_order NOT IN (SELECT id_order FROM order_history WHERE id_order_state = 6) AND (o.valid = 1 OR (o.valid = 0 AND o.module = "bankwire") GROUP BY id_customer HAVING SUM(total_paid_real) > '.$totalCustomer);
			$countBetterCustomers = (int)Db::getInstance()->getValue('SELECT FOUND_ROWS()') + 1;
		}
		else
			$countBetterCustomers = '-';
			
			if($customer->company != '') { $azienda = $customer->company." -"; } else { $azienda = ""; }
			if($customer->vat_number != '') { $piva = "- <strong>Partita IVA</strong>:". $customer->vat_number.""; } else { $piva = ""; }
			
			
			
			$nome = $customer->firstname;
			$cognome = $customer->lastname;
			$cf = $customer->tax_code;
			$is_supplier = Db::getInstance()->getValue("SELECT supplier FROM customer WHERE id_customer = ".Tools::getValue('id_customer')."");
			
			$preventivi = Db::getInstance()->ExecuteS("SELECT * FROM form_prevendita_thread WHERE id_customer = ".$customer->id." ORDER BY id_thread DESC");
			$ticket = Db::getInstance()->ExecuteS("SELECT * FROM customer_thread WHERE id_contact != 7 AND id_customer = ".$customer->id." ORDER BY id_customer_thread DESC");
			$messaggi = Db::getInstance()->ExecuteS("SELECT * FROM customer_thread WHERE id_contact = 7 AND id_customer = ".$customer->id." ORDER BY id_customer_thread DESC");
			$azioni = Db::getInstance()->ExecuteS("SELECT * FROM action_thread WHERE id_customer = ".$customer->id." ORDER BY id_action DESC");
			$fatture = Db::getInstance()->ExecuteS("SELECT * FROM fattura WHERE id_customer = ".$customer->id." GROUP BY id_fattura");
			$persone = Db::getInstance()->ExecuteS("SELECT * FROM persone WHERE id_customer = ".$customer->id." GROUP BY id_persona");
			$contratti = Db::getInstance()->ExecuteS("SELECT * FROM contratto_assistenza WHERE id_customer = ".$customer->id." GROUP BY id_contratto");
			$bdl = Db::getInstance()->ExecuteS("SELECT * FROM bdl WHERE id_customer = ".$customer->id." GROUP BY id_bdl");
			
			$azioni_cliente = array_merge($preventivi, $messaggi);
			
			$preventiviaperti = Db::getInstance()->ExecuteS("SELECT * FROM form_prevendita_thread WHERE id_customer = ".$customer->id." AND status != 'closed' ORDER BY id_thread DESC");
			$messaggiaperti = Db::getInstance()->ExecuteS("SELECT ct.id_customer_thread FROM customer_message cm JOIN customer_thread ct ON cm.id_customer_thread = ct.id_customer_thread WHERE ct.id_contact = 7 AND cm.id_employee = 0 AND (status = 'open' OR status = 'pending1') AND ct.id_customer = ".$customer->id." GROUP BY ct.id_customer_thread");
			
			$ticketaperti = Db::getInstance()->ExecuteS("SELECT * FROM customer_thread WHERE id_contact != 7 AND id_customer = ".$customer->id." AND status != 'closed' ORDER BY id_customer_thread DESC");
			$azioniaperti = Db::getInstance()->ExecuteS("SELECT * FROM action_thread WHERE id_customer = ".$customer->id." AND status != 'closed' ORDER BY id_action DESC");
			
			$azioni_cliente_aperte = array_merge($preventiviaperti, $messaggiaperti);
			$codice_spring = Db::getInstance()->getValue("SELECT codice_spring FROM customer WHERE id_customer = ".Tools::getValue('id_customer')."");
			
			$filelist_count = array();
			$cartella_documenti = Db::getInstance()->getValue("SELECT cartella_documenti FROM customer WHERE id_customer = ".Tools::getValue('id_customer')."");

			if(empty($cartella_documenti)) {
				$dirs_count = 0;
				$docs_count = 0;
			}
			else {
				$directory_base = '../documenti-clienti/'.$cartella_documenti;
				
				$entire_dir = $directory_base;
				
				if(substr($entire_dir,0,12) != '../documenti') {
					echo "ERRORE"; die();
					
				}
					
				if ($handle = opendir($entire_dir)) {
					
					while ($dir = readdir($handle)) {
						
						if(is_dir($entire_dir.'/'.$dir) && $dir != "." && $dir != "..") { $dirlist_count[]=$dir; }
						if(is_file($entire_dir.'/'.$dir)) { $filelist_count[]=$dir; }
						
					}
				}
				
				$dirs_count = count($dirlist_count);
				$docs_count = count($filelist_count);
			}
			echo "<style type='text/css'>
			#hint{
				cursor:pointer;
			}
			.tooltip{
				margin:8px;
				text-align:left;
				padding:8px;
				border:1px solid blue;
				background-color:#ffffff;
				position: absolute;
				z-index: 2;
			}
			</style>";
			echo "<script type='text/javascript'>
 
$(document).ready(function() {
	var changeTooltipPosition = function(event) {
	  var tooltipX = event.pageX - 8;
	  var tooltipY = event.pageY + 8;
	  $('div.tooltip').css({top: tooltipY, left: tooltipX});
	};
	var tooltip_content = '';
	
 
	
	var showTooltip = function(event) {
	  $('div.tooltip').remove();
	  $.ajax({
	type: 'POST',
	data: 'id='+$(this).attr('id')+'&rel='+$(this).attr('rel'),
	async: false,
	url: 'customer_message_preview.php',
	success: function(resp)
		{
			tooltip_content = resp;						
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			tooltip_content = '';
			alert('ERROR');					
		}
								
	});
	 
	  $(\"<div class='tooltip'>\"+tooltip_content+\"</div>\")
            .appendTo('body');
	  changeTooltipPosition(event);
	};
	
	var hideTooltip = function() {
	   $('div.tooltip').remove();
	};
 
	$('tr.actions_ticket_tr').bind({
	   mousemove : changeTooltipPosition,
	   mouseenter : showTooltip,
	   mouseleave: hideTooltip
	});
});
 
</script>";
			
			echo '
			<script type="text/javascript" src="yetii.js"></script>
			
			<p style="font-size:16px; margin-top:-10px">
			<strong>Cliente:</strong> '.$azienda.' <strong>Nome e cognome</strong>: '.$nome.' '.$cognome.'</p>
			<p>
			<strong>Codice cliente</strong>: '.$customer->id.'
			- <strong>Codice Spring</strong>: '.($codice_spring == '' ? 'Non presente' : $codice_spring).'
			- <strong>CF</strong>: '.$cf.' '.$piva.'
			- <strong>Tipo</strong>: '.($customer->is_company == 1? 'Azienda' : 'Privato').'
			- <strong>Profilo</strong>: '.($customer->id_default_group == 3? 'Rivenditore' : ($customer->id_default_group == 12 ? 'Distributore' :  'Cliente web')).'
			- <strong>Data registrazione</strong>: '.Tools::displayDate($customer->date_add, $cookie->id_lang, false).'
			</p>
			
			<div id="tab-container-1">
			<ul id="tab-container-2-nav">
			<li '.($_GET['tab-container-1'] == 1 || !isset($_GET['tab-container-1']) ? 'class="activeli" ' : '').' style="margin-left:-35px"><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=1"><strong>Anagr.</strong></a></li>
			<li '.($_GET['tab-container-1'] == 2 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=2"><strong>Indirizzi</strong></a></li>
			'.($customer->is_company == 1 ? '<li '.($_GET['tab-container-1'] == 11 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=11"><strong>Pers. ('.sizeof($persone).')</strong></a></li>' : '').'
			<li '.($_GET['tab-container-1'] == 3 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=3"><strong>Amm.</strong></a></li>
			<li '.($_GET['tab-container-1'] == 4 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4"><strong>Ord.	('.sizeof($orders).') Car.	('.sizeof($carts).')</strong></a></li>
			<li '.($_GET['tab-container-1'] == 5 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=5"><strong>Fatt. ('.sizeof($fatture).')</strong></a></li>
			<li '.($_GET['tab-container-1'] == 6 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=6"><strong>Ticket ('.(sizeof($ticketaperti) > 0 ? '<span style="color:red">'.sizeof($ticketaperti).'</span>' : sizeof($ticketaperti)).' / '.sizeof($ticket).')</strong></a></li>
			<li '.($_GET['tab-container-1'] == 7 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=7"><strong>Az. cliente ('.(sizeof($azioni_cliente_aperte) > 0 ? '<span style="color:red">'.sizeof($azioni_cliente_aperte).'</span>' : sizeof($azioni_cliente_aperte)).' / '.sizeof($azioni_cliente).')</strong></a></li>
			
			
			<li '.($_GET['tab-container-1'] == 10 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=10"><strong>TO DO ('.(sizeof($azioniaperti) > 0 ? '<span style="color:red">'.sizeof($azioniaperti).'</span>' : sizeof($azioniaperti)).' / '.sizeof($azioni).')</strong></a></li>
			
			<li '.($_GET['tab-container-1'] == 8 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=8"><strong>Stat</strong></a></li>
			
			<li '.($_GET['tab-container-1'] == 12 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=12"><strong>Doc ('.$dirs_count."-".$docs_count.')</strong></a></li>
		
			
			<li '.($_GET['tab-container-1'] == 13 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=13"><strong>Contratti ('.sizeof($contratti).')</strong></a></li>
			
			<li '.($_GET['tab-container-1'] == 14 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=14"><strong>BDL ('.sizeof($bdl).')</strong></a></li>
			</ul>
			
			
			
			<ul id="tab-container-1-nav" style="list-style-type:none; display:none">
			<li style="margin-left:-35px"><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#anagrafica"><strong>Anagrafica</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#addresses"><strong>Indirizzi</strong></a></li>
			'.($customer->is_company == 1 ? '<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#persone-customer"><strong>Persone</strong></a></li>' : '').'
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#amministrazione"><strong>Amministrazione</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#orders-customer"><strong>Ordini</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#fatture-customer"><strong>Fatture</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#messages-customer"><strong>Ticket</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#preventivi-customer"><strong>Preventivi</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#stats"><strong>Stats</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#azioni-customer"><strong>Azioni</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#documenti-customer"><strong>Docs</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#contratti-customer"><strong>Contratti</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#bdl-customer"><strong>BDL</strong></a></li>
			
			</ul>
			<br /><br />';	
			
			
			echo "
			<script type='text/javascript'>

function openKCFinder(textarea) {
   window.KCFinder = {
        callBackMultiple: function(files) {
            window.KCFinder = null;
            textarea.value = '';
			var list = document.getElementById('list-ticket').innerHTML;	
			list = '';
            for (var i = 0; i < files.length; i++) {
                textarea.value += files[i] + \":::\";
				list += '<a href=\"http://localhost'+files[i]+'\" target=\"_blank\">http://localhost'+files[i] + \"</a><br />\";
			}
			document.getElementById('list-ticket').innerHTML = list;
        }
    };
    window.open('kcfinder/browse.php?lang=it&type=files&dir=files', 'kcfinder_multiple',
        'status=0, toolbar=0, location=0, menubar=0, directories=0, ' +
        'resizable=1, scrollbars=0, width=800, height=600'
    );
}

</script>";
			
			
			if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 1 || !isset($_GET['tab-container-1'])) {
			
			echo '<div class="tab1" id="anagrafica">';
			
			echo '<h2><a>'.$this->l('Attività recenti del cliente').' </a></h2>';
			
			$ultime_cose = Db::getInstance()->executeS("SELECT id_thread as id, tipo_richiesta as tipo, id_customer as cliente, id_employee as in_carico, status, (CASE WHEN((status = 'open' OR status = 'pending1') AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM form_prevendita_thread WHERE id_customer = ".$_GET['id_customer']." UNION 
			SELECT id_customer_thread as id, id_contact as tipo, id_customer as cliente, id_employee as in_carico, status, (CASE WHEN((status = 'open' OR status = 'pending1') AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM customer_thread WHERE id_customer = ".$_GET['id_customer']." UNION 
			SELECT id_order as id, 'Ordine' as tipo, id_customer as cliente, '' as in_carico, (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) AS status, (CASE WHEN(((SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 4 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 5 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 14 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 15 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 16 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 20) AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM orders WHERE id_customer = ".$_GET['id_customer']." UNION 
			SELECT id_action as id, action_type as tipo, id_customer as cliente, action_to as in_carico, status, (CASE WHEN((status = 'open' OR status = 'pending1') AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM action_thread WHERE id_customer = ".$_GET['id_customer']." UNION 
			SELECT id_cart as id, 'Carrello' as tipo, id_customer as cliente, in_carico_a as in_carico, '' AS status, '' as scaduto, date_add, date_upd FROM cart WHERE id_customer = ".$_GET['id_customer']." ORDER BY date_add DESC");
			
			//SELECT id_cart as id, 'Carrello' as tipo, id_customer as cliente, in_carico_a as in_carico, '' AS status, '' as scaduto, date_add, date_upd FROM cart WHERE id_cart NOT IN (SELECT id_cart FROM orders WHERE id_customer = ".$_GET['id_customer'].") AND id_customer = ".$_GET['id_customer']." ORDER BY date_add DESC");
			
			$date_cose = array();
			foreach ($ultime_cose as $key => $row)
			{
				$date_cose[$key] = $row['date_upd'];
			}
			array_multisort($date_cose, SORT_DESC, $ultime_cose);
			
			echo "<table class='table'>
			<tr><th>Tipo 
			<a href='index.php?tab=AdminCustomers&viewcustomer=".$_GET['id_customer']."&orderby=tipo&orderway=asc&token=".$this->token."&tab-container-1=1'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
				<a href='index.php?tab=AdminCustomers&viewcustomer=".$_GET['id_customer']."orderby=tipo&orderway=desc&token=".$this->token."&tab-container-1=1'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a>
			</th><th>ID</th><th>Creato da</th><th>In carico a</th><th>Conv.</th><th>Status</th><th>Scaduto</th><th>Oggetto</th><th>Importo</th><th>Data apertura</th><th>Ultima modifica</th></tr>";
			
			for($i=0;$i<count($ultime_cose);$i++) {
				if ($ultime_cose[$i]['tipo'] == 7){
					$ctrl_mex = Db::getInstance()->getValue("SELECT COUNT(id_customer_message) FROM customer_thread ct JOIN customer_message cm ON ct.id_customer_thread = cm.id_customer_thread WHERE cm.id_employee = 0 AND ct.id_customer_thread = ".$ultime_cose[$i]['id']."");	
						
					if($ctrl_mex >= 1) {
						
					} else {
						$ultime_cose[$i]['status'] = ''; $ultime_cose[$i]['scaduto'] = '';
					}

				}
					
				if($i<5) {
					$includilo = 'yes';
				}
				else {
					if($ultime_cose[$i]['scaduto'] == 'SCADUTO') {
						$includilo = 'yes';
					}
					else {
						$includilo = 'no';
					}
				
				}
				$conv = "--";
				if($includilo == 'yes') {
					$in_carico = $ultime_cose[$i]['in_carico'];
					
					if(is_numeric($in_carico)) {
						$newinc = new Employee($in_carico);
						$in_carico = $newinc->firstname;
					}
					
					$tipo = $ultime_cose[$i]['tipo'];
					if ($tipo == 4){
						$sigla = "T"; $tipo = "Ticket assistenza"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
					}
					else if ($tipo == 2){
						$sigla = "A"; $tipo = "Ticket contabilita"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
					} 
					else if ($tipo == 3){
						$sigla = "V"; $tipo = "Ticket rivenditori"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
					} 
					else if ($tipo == 7){
						$sigla = "M"; $tipo = "Messaggio"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewmessage&id_mex='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=7">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
						
						$ctrl_mex = Db::getInstance()->getValue("SELECT COUNT(id_customer_message) FROM customer_thread ct JOIN customer_message cm ON ct.id_customer_thread = cm.id_customer_thread WHERE cm.id_employee = 0 AND ct.id_customer_thread = ".$ultime_cose[$i]['id']."");	
						
						if($ctrl_mex >= 1) {
							$status = ''; $scaduto = '';
						}
					}
					else if ($tipo == 8){
						$sigla = "D"; $tipo = "Ticket ordini"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
					} 
					else if ($tipo == 9){
						$sigla = "S"; $tipo = "RMA"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
					} 
					else if ($tipo == 'tirichiamiamonoi') {	
						$sigla = "R"; $tipo = "Ti richiamiamo noi"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewpreventivo&id_thread='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=7">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM form_prevendita_message WHERE id_thread = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
					}
					else if ($tipo == 'preventivo') {	
						$sigla = "P"; $tipo = "Preventivo"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewpreventivo&id_thread='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=7">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM form_prevendita_message WHERE id_thread = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
					}
					else {
						if($tipo == 'Carrello') {
							$ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcart&id_cart='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=4">'; $creato_da = Db::getInstance()->getValue("SELECT created_by FROM cart WHERE id_cart = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
							$in_carico = $ultime_cose[$i]['in_carico'];
							
							$n_ord_c = Db::getInstance()->getValue("SELECT id_order FROM orders WHERE id_cart = ".$ultime_cose[$i]['id']."");
							if($n_ord_c > 0) 
							{
								$conv = "<img src='http://www.ezdirect.it/img/admin/enabled.gif' alt='Si' title='Si' />";
							}
							else
							{
								$conv = "<img src='http://www.ezdirect.it/img/admin/red_no.gif' alt='No' title='No' />";
								
							}
					
							if(is_numeric($in_carico)) {
								$newinc = new Employee($in_carico);
								$in_carico = $newinc->firstname;
							}
						}
						else if ($tipo == 'Ordine') {
							$ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&vieworder&id_order='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=4">'; $creato_da = "";
						}
						else if ($tipo != 'Ordine' && $tipo != 'Carrello' && tipo != '') {
							$ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewaction&id_action='.$ultime_cose[$i]['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=10">'; $creato_da = Db::getInstance()->getValue("SELECT action_from FROM action_thread WHERE id_action = ".$ultime_cose[$i]['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname; }
						}
						else {
							$ahrefcosa = '<a href="#">';
						}
						
						$sigla = "";
					}
					
					$anno = substr($ultime_cose[$i]['date_upd'],2,2);
					
					if($sigla != '') {
						$id_cosa = $sigla.$anno.$ultime_cose[$i]['id'];
					}
					else {
						$id_cosa = $ultime_cose[$i]['id'];
					}
					
					$status = $ultime_cose[$i]['status']; 
					if($status == 'closed') {
						$status = '<img src="../img/admin/status_green.gif" alt="Chiuso" /> Chiuso';
					}
					else if($status == 'pending1' || $status == 'pending2') {
						$status = '<img src="../img/admin/status_orange.gif" alt="In lavorazione" />In lavorazione';
					}
					else if($status == 'open') {
						$status = '<img src="../img/admin/status_red.gif" alt="Aperto" />Aperto';
					}
					else if(is_numeric($status)) {
						$status = Db::getInstance()->getValue("SELECT name FROM order_state_lang WHERE id_lang = 5 AND id_order_state = ".$status."");
					}
					
					if($tipo == 'Carrello') {
						$oggetto_uc = Db::getInstance()->getValue("SELECT name FROM cart WHERE id_cart = ".$ultime_cose[$i]['id']."");
						$cartID = new Cart((int)($ultime_cose[$i]['id']));
						$summaryD = $cartID->getSummaryDetails();
						$importo_uc = Tools::displayPrice($summaryD['total_products'],1);
					}
					else {
						$oggetto_uc = "--";
						$importo_uc = "--";
					}
					
					
					echo "<tr>";
					echo "<td>".$ahrefcosa.$tipo."</a></td>";
					echo "<td>".$ahrefcosa.$id_cosa."</a></td>";
					echo "<td>".$ahrefcosa.$creato_da."</a></td>";
					echo "<td>".$ahrefcosa.$in_carico."</a></td>";
					echo "<td>".$conv."</td>";
					echo "<td>".$ahrefcosa.$status."</a></td>";
					echo "<td>".$ahrefcosa.$ultime_cose[$i]['scaduto']."</a></td>";
					echo "<td>".$ahrefcosa.$oggetto_uc."</a></td>";
					echo "<td style='text-align:right'>".$ahrefcosa.$importo_uc."</a></td>";
					echo "<td>".$ahrefcosa.Tools::DisplayDate($ultime_cose[$i]['date_add'],$cookie->id_lang,false)."</a></td>";
					echo "<td>".$ahrefcosa.Tools::DisplayDate($ultime_cose[$i]['date_upd'],$cookie->id_lang,false)."</a></td>";
					echo "</tr>";
				}
				else {
				}
			}
		
			echo "</table><br />";
			
			echo '<h2><a>'.$this->l('Anagrafica').' </a></h2>';
			$this->displayForm();
	
		
		echo '<br />
			
		
		</fieldset>
		';
		
		
		echo '</div>
		';
		}
		else { }
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 2) {

		echo '<div class="tab1" id="addresses"><h2><a>'.$this->l('Indirizzo di fatturazione').' </a></h2>';
		if (sizeof($addresses_fatt))
		{
			echo '
			<table cellspacing="0" cellpadding="0" class="table" style="font-size:12px">
				<tr>
					<th style="width:100px; font-size:13px">'.$this->l('Company').'</th>
					<th style="width:150px; font-size:13px">'.$this->l('Name').'</th>
					<th style="width:150px; font-size:13px">'.$this->l('Address').'</th>
					<th style="width:50px; font-size:13px">'.$this->l('CAP').'</th>
					<th style="width:100px; font-size:13px">'.$this->l('Citta').'</th>
					<th style="width:100px; font-size:13px">'.$this->l('Nazione').'</th>
					<th style="width:100px; font-size:13px">'.$this->l('Phone number(s)').'</th>
					<th style="width:50px; font-size:13px">'.$this->l('Tipo').'</th>
					<th style="width:50px; font-size:13px">'.$this->l('Actions').'</th>
				</tr>';
			
			foreach ($addresses_fatt AS $address) {
			
			if($address['fatturazione'] == 1) { $tipoind = "<strong>Fatturazione</strong>"; } else { $tipoind = "Consegna"; }
				echo '
				<tr '.($irow++ % 2 ? 'class="alt_row"' : '').'>
					<td style="font-size:13px"><b>'.($address['company'] ? $address['company'] : '--').'</b></td>
					<td style="font-size:13px">'.$address['firstname'].' '.$address['lastname'].'</td>
					<td style="font-size:13px">'.$address['address1'].($address['address2'] ? ' '.$address['address2'] : '').'</td>
					<td style="font-size:13px"> '.$address['postcode'].'</td>
					<td style="font-size:13px">'.$address['city'].'</td>
					<td style="font-size:13px">'.$address['country'].'</td>
					<td style="font-size:13px">'.($address['phone'] ? ($address['phone'].($address['phone_mobile'] ? '<br />'.$address['phone_mobile'] : '')) : ($address['phone_mobile'] ? $address['phone_mobile'] : '--')).'</td>
					<td>'.$tipoind.'</td>
					<td align="center">
						<a style="cursor:pointer" onclick="window.open(\'?tab=AdminAddresses&id_address='.$address['id_address'].'&addaddress&back='.urlencode($_SERVER['REQUEST_URI']).'&token='.$tokenAddresses.'\', \'blank\', \'height=600,width=1200,modal=yes,alwaysRaised=yes,scrollbars=yes\');"><img src="../img/admin/edit.gif" /></a>
						<a href="?tab=AdminAddresses&id_address='.$address['id_address'].'&deleteaddress&cancellind=1&back='.urlencode($_SERVER['REQUEST_URI']).'&token='.$tokenAddresses.'"><img src="../img/admin/delete.gif" /></a>
					</td>
				</tr>';
				}
			echo '
			</table>';
			echo '<br /><a style="cursor:pointer" onclick="window.open(\'?tab=AdminAddresses&addaddress&fatturazione=1&idclientee='.$customer->id.'&back='.urlencode($_SERVER['REQUEST_URI']).'&token='.$tokenAddresses.'\', \'blank\', \'height=600,width=1200,modal=yes,alwaysRaised=yes,scrollbars=yes\');" class="button">Aggiungi un indirizzo di fatturazione</a>';
			
		}
		else {
			echo $customer->firstname.' '.$customer->lastname.' '.$this->l('has not registered any addresses yet').'.<br />';
			echo '<br /><a style="cursor:pointer" onclick="window.open(\'?tab=AdminAddresses&addaddress&fatturazione=1&idclientee='.$customer->id.'&back='.urlencode($_SERVER['REQUEST_URI']).'&token='.$tokenAddresses.'\', \'blank\', \'height=600,width=1200,modal=yes,alwaysRaised=yes,scrollbars=yes\');" class="button">Aggiungi un indirizzo di fatturazione</a>';
		}	
		echo '<br /><br /><br />';	
			
			
			echo '
		<h2><a name="addresses">'.$this->l('Indirizzi di consegna').' ('.sizeof($addresses_cons).')</a></h2>';
		if (sizeof($addresses_cons))
		{
			echo '
			<table cellspacing="0" cellpadding="0" class="table" style="font-size:12px">
				<tr>
					<th style="width:100px; font-size:13px">'.$this->l('Company').'</th>
					<th style="width:150px; font-size:13px">'.$this->l('Name').'</th>
					<th style="width:150px; font-size:13px">'.$this->l('Address').'</th>
					<th style="width:50px; font-size:13px">'.$this->l('CAP').'</th>
					<th style="width:100px; font-size:13px">'.$this->l('Citta').'</th>
					<th style="width:100px; font-size:13px">'.$this->l('Nazione').'</th>
					<th style="width:100px; font-size:13px">'.$this->l('Phone number(s)').'</th>
					<th style="width:50px; font-size:13px">'.$this->l('Tipo').'</th>
					<th style="width:50px; font-size:13px">'.$this->l('Actions').'</th>
				</tr>';
			$tokenAddresses = Tools::getAdminToken('AdminAddresses'.(int)(Tab::getIdFromClassName('AdminAddresses')).(int)($cookie->id_employee));
			foreach ($addresses_cons AS $address) {
			
			if($address['fatturazione'] == 1) { $tipoind = "<strong>Fatturazione</strong>"; } else { $tipoind = "Consegna"; }
				echo '
				<tr '.($irow++ % 2 ? 'class="alt_row"' : '').'>
					<td style="font-size:13px"><b>'.($address['company'] ? $address['company'] : '--').'</b></td>
					<td style="font-size:13px">'.$address['firstname'].' '.$address['lastname'].'</td>
					<td style="font-size:13px">'.$address['address1'].($address['address2'] ? ' '.$address['address2'] : '').'</td>
					<td style="font-size:13px"> '.$address['postcode'].'</td>
					<td style="font-size:13px">'.$address['city'].'</td>
					<td style="font-size:13px">'.$address['country'].'</td>
					<td style="font-size:13px">'.($address['phone'] ? ($address['phone'].($address['phone_mobile'] ? '<br />'.$address['phone_mobile'] : '')) : ($address['phone_mobile'] ? $address['phone_mobile'] : '--')).'</td>
					<td>'.$tipoind.'</td>
					<td align="center">
						<a style="cursor:pointer" onclick="window.open(\'?tab=AdminAddresses&id_address='.$address['id_address'].'&addaddress&back='.urlencode($_SERVER['REQUEST_URI']).'&token='.$tokenAddresses.'\', \'blank\', \'height=600,width=1200,modal=yes,alwaysRaised=yes,scrollbars=yes\');"><img src="../img/admin/edit.gif" /></a>
						<a href="?tab=AdminAddresses&id_address='.$address['id_address'].'&deleteaddress&cancellind=1&back='.urlencode($_SERVER['REQUEST_URI']).'&token='.$tokenAddresses.'"><img src="../img/admin/delete.gif" /></a>
					</td>
				</tr>';
				}
			echo '
			</table>';
			
		}
		else {
			echo $customer->firstname.' '.$customer->lastname.' '.$this->l('has not registered any addresses yet').'.<br />';
		}	
		echo '<br /><a style="cursor:pointer" onclick="window.open(\'?tab=AdminAddresses&addaddress&idclientee='.$customer->id.'&back='.urlencode($_SERVER['REQUEST_URI']).'&token='.$tokenAddresses.'\', \'blank\', \'height=600,width=1200,modal=yes,alwaysRaised=yes,scrollbars=yes\');" class="button">Aggiungi un indirizzo di consegna</a><br /><br /><br />';
			

		

		echo '</div>
		<div class="clear"></div>';
		
		}
		else {
		}
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 3) {
		echo '<div class="tab1" id="amministrazione"><h2><a>'.$this->l('Amministrazione').' </a></h2>';
		
		// INIZIO AMMINISTRAZIONE //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		

		
		if(isset($_GET['submitAmministrazione'])) {
		
			$count_amm = Db::getInstance()->getValue("SELECT count(id_customer) total FROM customer_amministrazione WHERE id_customer = ".Tools::getValue('id_customer')."");
			
			if($count_amm == 0) {
				Db::getInstance()->executeS("INSERT INTO customer_amministrazione (id_customer, tipo_soggetto, codice_fornitore, pagamento, pagamento_2, fido, zona, trasporto_a_mezzo, consegna, vettore, id_corriere, trasporto_gratis_da, fermo_deposito, trasporto_assicurato, note_consegna, iban, swift, paypal, iva_agevolata, esportatore_abituale, sconto_extra, per_ordini_da, fino_a, rebate, fatturato_annuo, blacklist, agente, installatore) VALUES ('".Tools::getValue('id_customer')."', '".Tools::getValue('tipo_soggetto')."', '".Tools::getValue('codice_fornitore')."', '".Tools::getValue('pagamento')."', '".Tools::getValue('pagamento_2')."', '".Tools::getValue('fido')."', '".Tools::getValue('zona')."', '".Tools::getValue('trasporto_a_mezzo')."', '".Tools::getValue('consegna')."', '".Tools::getValue('vettore')."', '".Tools::getValue('id_corriere')."', '".Tools::getValue('trasporto_gratis_da')."', '".Tools::getValue('fermo_deposito')."', '".Tools::getValue('trasporto_assicurato')."', '".Tools::getValue('note_consegna')."', '".Tools::getValue('iban')."', '".Tools::getValue('swift')."', '".Tools::getValue('paypal')."', '".Tools::getValue('iva_agevolata')."', '".Tools::getValue('esportatore_abituale')."', '".Tools::getValue('sconto_extra')."', '".Tools::getValue('per_ordini_da')."', '".Tools::getValue('fino_a')."', '".Tools::getValue('rebate')."', '".Tools::getValue('fatturato_annuo')."', '".Tools::getValue('blacklist')."', '".Tools::getValue('agente')."', '".Tools::getValue('installatore')."')");
			}
			else {
				Db::getInstance()->executeS("UPDATE customer_amministrazione SET tipo_soggetto = '$_POST[tipo_soggetto]', codice_fornitore = '$_POST[codice_fornitore]', pagamento = '$_POST[pagamento]', pagamento_2 = '$_POST[pagamento_2]', fido = '$_POST[fido]', zona = '$_POST[zona]', trasporto_a_mezzo = '$_POST[trasporto_a_mezzo]', consegna = '$_POST[consegna]', vettore = '$_POST[vettore]', id_corriere = '$_POST[id_corriere]', trasporto_gratis_da = '$_POST[trasporto_gratis_da]', fermo_deposito = '$_POST[fermo_deposito]', trasporto_assicurato = '$_POST[trasporto_assicurato]', note_consegna = '$_POST[note_consegna]', iban = '$_POST[iban]', swift = '$_POST[swift]', paypal = '$_POST[paypal]', iva_agevolata = '$_POST[iva_agevolata]', esportatore_abituale = '$_POST[esportare_abituale]', sconto_extra = '$_POST[sconto_extra]', per_ordini_da = '$_POST[per_ordini_da]', fino_a = '$_POST[fino_a]', rebate = '$_POST[rebate]', fatturato_annuo = '$_POST[fatturato_annuo]', blacklist = '$_POST[blacklist]', agente = '$_POST[agente]', installatore = '$_POST[installatore]' WHERE id_customer = '$_POST[id_customer]'");
			}
		}
		
		$sqlamm = mysql_query("SELECT * FROM customer_amministrazione WHERE id_customer = $_GET[id_customer]");
		$rowamm = mysql_fetch_array($sqlamm, MYSQL_ASSOC);
		
		if(!isset($_GET['modificheabilitate_amm'])) {
		echo "
		<script type='text/javascript'>
		$(document).ready(function(){
			$('#modificaamministrazione :input').attr('readonly', 'readonly');
			
			$('#modificaamministrazione :checkbox, #modificaamministrazione :radio').click(function(e) {
				e.preventDefault();
			});
			
			$('#modificaamministrazione select').attr('disabled', 'disabled');
		});
		// end
		</script>";
		echo '<form id="modificaamministrazione" action="'.$currentIndex.'&id_customer='.$_GET['id_customer'].'&viewcustomer&modificheabilitate_amm&token='.$this->token.'&tab-container-1=3" method="post" autocomplete="off">';
		}
		else {
		echo '
		<form id="modificaamministrazione" action="'.$currentIndex.'&id_customer='.$_GET['id_customer'].'&submitAmministrazione&viewcustomer&token='.$this->token.'&tab-container-1=3" method="post" autocomplete="off">';
		}
		
		
	echo '
			<fieldset><legend><img src="../img/admin/tab-customers.gif" />'.$this->l('Amministrazione').'</legend>';
	
			//	if ($this->getFieldValue($obj, 'is_company') == 1) {
				
				echo '<div class="anagrafica-cliente3">
				'.$this->l('Codice web:').'<br />
			
					<input type="text" size="25" name="id_customer" value="'.htmlentities(Tools::getValore($rowamm, 'id_customer'), ENT_COMPAT, 'UTF-8').'" readonly="readonly" /> 
					</div>
				';

				
				
				
			echo '<div class="anagrafica-cliente3">
				'.$this->l('Tipo soggetto:').'<br />
			<select name="tipo_soggetto">';
			echo '<option value="3"'.(Tools::getValore($rowamm, 'tipo_soggetto') == 3 ? ' selected="selected"' : '').'>Persona fisica</option>';
			echo '<option value="1"'.(Tools::getValore($rowamm, 'tipo_soggetto') == 1 ? ' selected="selected"' : '').'>Societ&agrave; di capitali</option>';
			echo '<option value="2"'.(Tools::getValore($rowamm, 'tipo_soggetto') == 2 ? ' selected="selected"' : '').'>Societ&agrave; di persone</option>';
			echo '<option value="4"'.(Tools::getValore($rowamm, 'tipo_soggetto') == 4 ? ' selected="selected"' : '').'>Altro</option>';
			echo '
					</select>
					</div>
				';
				
				echo '<div class="anagrafica-cliente3">
				'.$this->l('Codice fornitore:').'<br />
			
					<input type="text" size="25" name="codice_fornitore" value="'.htmlentities(Tools::getValore($rowamm, 'codice_fornitore'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>
				';
				
				
				echo "<div class='clear'></div>";
				
				
				
				
								echo '<div class="anagrafica-cliente3">
				'.$this->l('Pagamento:').'<br />
			<select name="pagamento">';
			echo '<option value="Bonifico Bancario"'.(Tools::getValore($rowamm, 'pagamento') == 'Bonifico Bancario' ? ' selected="selected"' : '').'>Bonifico bancario</option>';
			echo '<option value="GestPay"'.(Tools::getValore($rowamm, 'pagamento') == 'GestPay' ? ' selected="selected"' : '').'>Carta di credito (gestpay)</option>';
			echo '<option value="PayPal"'.(Tools::getValore($rowamm, 'pagamento') == 'PayPal' ? ' selected="selected"' : '').'>Paypal</option>';
			echo '<option value="Contrassegno"'.(Tools::getValore($rowamm, 'pagamento') == 'Contrassegno' ? ' selected="selected"' : '').'>Contrassegno</option>';
			echo "<option value='Bonifico 30 gg. fine mese' ".(Tools::getValore($rowamm, 'pagamento') == 'Bonifico 30 gg. fine mese' ? "selected='selected'" : "").">Bonifico 30 gg. fine mese</option>
			<option value='Bonifico 60 gg. fine mese' ".(Tools::getValore($rowamm, 'pagamento') == 'Bonifico 60 gg. fine mese' ? "selected='selected'" : "").">Bonifico 60 gg. fine mese</option>
			<option value='Bonifico 90 gg. fine mese' ".(Tools::getValore($rowamm, 'pagamento') == 'Bonifico 90 gg. fine mese' ? "selected='selected'" : "").">Bonifico 90 gg. fine mese</option>
			<option value='R.B. 30 GG. D.F. F.M.' ".(Tools::getValore($rowamm, 'pagamento') == 'R.B. 30 GG. D.F. F.M.' ? "selected='selected'" : "").">R.B. 30 GG. D.F. F.M.</option>
			<option value='R.B. 60 GG. D.F. F.M.' ".(Tools::getValore($rowamm, 'pagamento') == 'R.B. 60 GG. D.F. F.M.' ? "selected='selected'" : "").">R.B. 60 GG. D.F. F.M.</option>
			<option value='R.B. 90 GG. D.F. F.M.' ".(Tools::getValore($rowamm, 'pagamento') == 'R.B. 90 GG. D.F. F.M.' ? "selected='selected'" : "").">R.B. 90 GG. D.F. F.M.</option>
		";
			echo '
					</select>
					</div>
				';

							echo '<div class="anagrafica-cliente3">
				'.$this->l('Pagamento 2:').'<br />
			
					<input type="text" size="25" name="pagamento_2" value="'.htmlentities(Tools::getValore($rowamm, 'pagamento_2'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>
				';	

							echo '<div class="anagrafica-cliente3">
				'.$this->l('Fido:').'<br />
			
					<input type="text" size="25" name="fido" value="'.Tools::getValore($rowamm, 'fido').'" /> &euro;
					<input type="hidden" name="zona" value="" />
					</div>
				';				
				
											/*echo '<div class="anagrafica-cliente3">
				'.$this->l('Zona:').'<br />
			
					<select name="zona" onchange="checkDefaultGroup(this.value);">';
					
					$zone = State::getStatesByIdCountry(10);
					
				foreach ($zone as $zona)
					echo '<option value="'.(int)($zona['id_state']).'"'.($zona['id_state'] == $rowamm['zona'] ? ' selected="selected"' : '').'>'.htmlentities($zona['name'], ENT_NOQUOTES, 'utf-8').'</option>';
				echo '
					</select>
					
					</div>
				';*/
				
				
				
				echo "<div class='clear'></div>";
				
				
								echo '<div class="anagrafica-cliente3">
				'.$this->l('Trasporto a mezzo:').'<br />
			<select name="trasporto_a_mezzo">';
			echo '<option value="3"'.(Tools::getValore($rowamm, 'trasporto_a_mezzo') == '3' ? ' selected="selected"' : '').'>Vettore</option>';
			echo '<option value="1"'.(Tools::getValore($rowamm, 'trasporto_a_mezzo') == '1' ? ' selected="selected"' : '').'>Mittente</option>';
			echo '<option value="2"'.(Tools::getValore($rowamm, 'trasporto_a_mezzo') == '2' ? ' selected="selected"' : '').'>Destinatario</option>';
			echo '<option value="4"'.(Tools::getValore($rowamm, 'trasporto_a_mezzo') == '4' ? ' selected="selected"' : '').'>Ritiro in sede</option>';
			echo '
					</select>
					</div>
				';
				
				
				
								echo '<div class="anagrafica-cliente3">
				'.$this->l('Consegna (porto):').'<br />
			<select name="consegna">';
			echo '<option value="3"'.(Tools::getValore($rowamm, 'consegna') == 3 ? ' selected="selected"' : '').'>Franco Add. Fatt.</option>';
			echo '<option value="1"'.(Tools::getValore($rowamm, 'consegna') == 1 ? ' selected="selected"' : '').'>Franco</option>';
			echo '<option value="2"'.(Tools::getValore($rowamm, 'consegna') == 2 ? ' selected="selected"' : '').'>Assegnato</option>';
			
			echo '
					</select>
					</div>
				';
											echo '<div class="anagrafica-cliente3">
				'.$this->l('Vettore:').'<br />
			<select name="vettore">';
			echo '<option value="1043"'.(Tools::getValore($rowamm, 'vettore') == 1043 ? ' selected="selected"' : '').'>GLS National Express s.r.l.</option>';
			echo '<option value="1062"'.(Tools::getValore($rowamm, 'vettore') == 1062 ? ' selected="selected"' : '').'>UPS Italia s.r.l.</option>';
			echo '<option value="1245"'.(Tools::getValore($rowamm, 'vettore') == 1245 ? ' selected="selected"' : '').'>TNT Global Express spa</option>';
			echo '<option value="1269"'.(Tools::getValore($rowamm, 'vettore') == 1269 ? ' selected="selected"' : '').'>SDA Trasporti</option>';
			echo '<option value="1271"'.(Tools::getValore($rowamm, 'vettore') == 1271 ? ' selected="selected"' : '').'>DHL Trasporti</option>';
			echo '<option value="1752"'.(Tools::getValore($rowamm, 'vettore') == 1752 ? ' selected="selected"' : '').'>Ascoli Trasporti</option>';
			echo '<option value="1753"'.(Tools::getValore($rowamm, 'vettore') == 1753 ? ' selected="selected"' : '').'>Poste Trasporti</option>';
			echo '
					</select>
					</div>
				';
					
					echo '<div class="anagrafica-cliente3">
				'.$this->l('Id corriere cliente:').'<br />
			
					<input type="text" size="25" name="id_corriere" value="'.htmlentities(Tools::getValore($rowamm, 'id_corriere'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>
				';			
				
				
				echo "<div class='clear'></div>";
				
			echo '<div class="anagrafica-cliente3">
				'.$this->l('Trasporto gratis da:').'<br />
			
					<input type="text" size="25" name="trasporto_gratis_da" value="'.Tools::getValore($rowamm, 'trasporto_gratis_da').'" /> &euro;
					</div>
				';			
			
				echo '<div class="anagrafica-cliente3">
				'.$this->l('Fermo deposito:').'<br />
			
					<input type="text" size="25" name="fermo_deposito" value="'.htmlentities(Tools::getValore($rowamm, 'fermo_deposito'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>
				';	

		echo '<div class="anagrafica-cliente3">Trasporto assicurato<br />'; 
				
				echo '<input type="radio" name="trasporto_assicurato" value="1" '; if (Tools::getValore($rowamm, 'trasporto_assicurato') == 1) { echo 'checked="checked"'; } else { echo ''; } echo' /><strong>S&igrave;</strong><br />
		<input type="radio" name="trasporto_assicurato" value="0"'; if (Tools::getValore($rowamm, 'trasporto_assicurato') == 0) { echo 'checked="checked"'; } else { echo ''; } echo' /><strong>No</strong>
			';
			echo '</div>';
			
						echo '<div class="anagrafica-cliente3">
				'.$this->l('Note consegna:').'<br />
			
					<input type="text" size="25" name="note_consegna" value="'.htmlentities(Tools::getValore($rowamm, 'note_consegna'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>
				';	
			

				echo "<div class='clear'></div><hr />";
			
			
				echo '<div class="anagrafica-cliente3">
				'.$this->l('IBAN:').'<br />
			
					<input type="text" size="25" name="iban" value="'.htmlentities(Tools::getValore($rowamm, 'iban'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>
				';	
					
				echo '<div class="anagrafica-cliente3">
				'.$this->l('Swift:').'<br />
			
					<input type="text" size="25" name="swift" value="'.htmlentities(Tools::getValore($rowamm, 'swift'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>
				';	
			
				
				
				
				echo '<div class="anagrafica-cliente3">
				'.$this->l('Paypal:').'<br />
			
					<input type="text" size="25" name="paypal" value="'.htmlentities(Tools::getValore($rowamm, 'paypal'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>
				';	
				
				echo "<div class='clear'></div>";
			

								echo '<div class="anagrafica-cliente3">
				'.$this->l('IVA agevolata:').'<br />
			
					<input type="text" size="25" name="iva_agevolata" value="'.htmlentities(Tools::getValore($rowamm, 'iva_agevolata'), ENT_COMPAT, 'UTF-8').'" /> %
					</div>
				';	
			echo '<div class="anagrafica-cliente3">Esportatore abituale<br />'; 
				
				echo '<input type="radio" name="esportatore_abituale" value="1" '; if (Tools::getValore($rowamm, 'esportatore_abituale') == 1) { echo 'checked="checked"'; } else { echo ''; } echo' /><strong>S&igrave;</strong><br />
		<input type="radio" name="esportatore_abituale" value="0"'; if (Tools::getValore($rowamm, 'esportatore_abituale') == 0) { echo 'checked="checked"'; } else { echo ''; } echo' /><strong>No</strong>
			';
			echo '</div>';
			
			
					echo "<div class='clear'></div>";

								echo '<div class="anagrafica-cliente3">
				'.$this->l('Sconto extra:').'<br />
			
					<input type="text" size="25" name="sconto_extra" value="'.Tools::getValore($rowamm, 'sconto_extra').'" /> %
					</div>		';			
									echo '<div class="anagrafica-cliente3">
				'.$this->l('Per ordini da:').'<br />
			
					<input type="text" size="25" name="per_ordini_da" value="'.Tools::getValore($rowamm, 'per_ordini_da').'" /> &euro;
					</div>		';	
								echo '<div class="anagrafica-cliente3">
				'.$this->l('Fino a (data):').'<br />
			
					<input type="text" size="25" name="fino_a" value="'.htmlentities(Tools::getValore($rowamm, 'fino_a'), ENT_COMPAT, 'UTF-8').'" />
					</div>		';		
					
					echo "<div class='clear'></div>";
	
									echo '<div class="anagrafica-cliente3">
				'.$this->l('Rebate:').'<br />
			
					<input type="text" size="25" name="rebate" value="'.Tools::getValore($rowamm, 'rebate').'" /> %
					</div>		';		
					
													echo '<div class="anagrafica-cliente3">
				'.$this->l('Fatturato annuo:').'<br />
			
					<input type="text" size="25" name="fatturato_annuo" value="'.Tools::getValore($rowamm, 'fatturato_annuo').'" /> &euro;
					</div>		';		
					
					echo '<div class="anagrafica-cliente3">Blacklist<br />'; 
				
				echo '<input type="radio" name="blacklist" value="1" '; if (Tools::getValore($rowamm, 'blacklist') == 1) { echo 'checked="checked"'; } else { echo ''; } echo' /><strong>S&igrave;</strong><br />
		<input type="radio" name="blacklist" value="0"'; if (Tools::getValore($rowamm, 'blacklist') == 0) { echo 'checked="checked"'; } else { echo ''; } echo' /><strong>No</strong>
			';
			echo '</div>';
					echo "<div class='clear'></div>";
				
				
						echo '<div class="anagrafica-cliente3">
				'.$this->l('Agente:').'<br />
			
					<select name="agente">';
					
					$agenti = Employee::getEmployees();
					
				foreach ($agenti as $agente)
					echo '<option value="'.(int)($agente['id_employee']).'"'.($agente['id_employee'] == Tools::getValore($rowamm, 'agente') ? ' selected="selected"' : '').'>'.htmlentities($agente['name'], ENT_NOQUOTES, 'utf-8').'</option>';
				echo '
					</select>
					
					</div>
				';
				
													echo '<div class="anagrafica-cliente3">
				'.$this->l('Installatore:').'<br />
			
					<input type="text" size="25" name="installatore" value="'.htmlentities(Tools::getValore($rowamm, 'installatore'), ENT_QUOTES, 'utf-8').'" />
					</div>		';		
				
				
				
				
				
				echo "<div class='clear'></div>";
				
				echo '<div class="margin-form">';
				
				
				if(!isset($_GET['modificheabilitate_amm'])) {
				echo '<input type="submit" value="'.$this->l('Abilita modifiche').'" name="abilitamodifiche" class="button" />';
				}
				else {
				echo '<input type="submit" value="'.$this->l('   Save   ').'" name="submitAmministrazione" class="button" />';
				}
				
				
				echo '</div>
				<div class="small"><sup>*</sup> '.$this->l('Required field').'</div>
			</fieldset>
		</form>';
		
		// FINE AMMINISTRAZIONE ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		
		echo '</div>';
		} else { }
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 4) {
		echo '<div class="tab1" id="orders-customer">';
		
		if(!isset($_GET['vieworder']) && !isset($_GET['viewcart'])) {
		
		echo '<h2><a>'.$this->l('Orders').' ('.sizeof($orders).')</a></h2>';
			if ($orders AND sizeof($orders))
			{
				$totalOK = 0;
				$totalNettoOK = 0;
				$ordersOK = array();
				$ordersKO = array();
				$tokenOrders = Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee);
				foreach ($orders AS $order)
				if ($order['valid'] || $order['module'] == 'bankwire')
				{
					$ordersOK[] = $order;
					$totalNettoOK += $order['total_products'];
					$totalOK += $order['total_paid_real'];
				}
				else
					$ordersKO[] = $order;
				$orderHead = '
				<table cellspacing="0" cellpadding="0" class="table float">
					<tr>
						<th class="center" style="width:50px">'.$this->l('ID').'</th>
						<th class="center" style="width:150px">'.$this->l('Date').'</th>
						<th style="text-align:right; width:50px">'.$this->l('Products').'</th>
						<th style="text-align:right; width:150px">'.$this->l('Totale netto').'</th>
						<th style="text-align:right; width:150px">'.$this->l('Total paid').'</th>
						<th class="center" style="width:250px">'.$this->l('Payment').'</th>
						<th class="center" style="width:250px">'.$this->l('State').'</th>
						<th class="center" style="width:50px">'.$this->l('Actions').'</th>
					</tr>';
					$orderFoot = '</table>';
					if ($countOK = sizeof($ordersOK))
					{
						echo '<strong style="display:block; margin-top:20px; color:green;font-weight:700">'.$this->l('Valid orders:').' '.$countOK.' '.$this->l('for').' '.Tools::displayPrice($totalNettoOK, new Currency($defaultCurrency)).'</strong>'.$orderHead;
						foreach ($ordersOK AS $order)
							echo '<tr '.($irow++ % 2 ? 'class="alt_row"' : '').' style="cursor: pointer" onclick="document.location = \'?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&id_order='.$order['id_order'].'&vieworder&token='.$this->token.'&tab-container-1=4\';">
						
							<td class="center">'.$order['id_order'].'</td>
								<td>'.Tools::displayDate($order['date_add'], (int)($cookie->id_lang), false).'</td>
								<td align="right">'.$order['nb_products'].'</td>
								<td align="right">'.Tools::displayPrice($order['total_products'], new Currency((int)($order['id_currency']))).'</td>
								<td align="right">'.Tools::displayPrice($order['total_paid_real'], new Currency((int)($order['id_currency']))).'</td>
								<td>'.$order['payment'].'</td>
								<td>'.$order['order_state'].'</td>
								<td align="center"><a href="?tab=AdminOrders&id_order='.$order['id_order'].'&vieworder&token='.$tokenOrders.'"><img src="../img/admin/details.gif" /></a></td>
							</tr>';
						echo $orderFoot.'';
					}
					if ($countKO = sizeof($ordersKO))
					{
						echo '<div class="clear"></div><strong style="display:block; margin-top:20px; color:red;font-weight:700">'.$this->l('Invalid orders:').' '.$countKO.'</strong>'.$orderHead;
						foreach ($ordersKO AS $order)
							echo '
							<tr '.($irow++ % 2 ? 'class="alt_row"' : '').' style="cursor: pointer"
							onclick="document.location = \'?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&id_order='.$order['id_order'].'&vieworder&token='.$this->token.'&tab-container-1=4\';">
								<td class="center">'.$order['id_order'].'</td>
								<td>'.Tools::displayDate($order['date_add'], (int)($cookie->id_lang), false).'</td>
								<td align="right">'.$order['nb_products'].'</td>
								<td align="right">'.Tools::displayPrice($order['total_products'], new Currency((int)($order['id_currency']))).'</td>
								<td align="right">'.Tools::displayPrice($order['total_paid_real'], new Currency((int)($order['id_currency']))).'</td>
								<td>'.$order['payment'].'</td>
								<td>'.$order['order_state'].'</td>
								<td align="center"><a href="?tab=AdminOrders&id_order='.$order['id_order'].'&vieworder&token='.$tokenOrders.'"><img src="../img/admin/details.gif" /></a></td>
							</tr>';
						echo $orderFoot.'';
					}
			}
			else
				echo $customer->firstname.' '.$customer->lastname.' '.$this->l('has not placed any orders yet');

			
			
		
				echo '<div class="clear"></div>';
				
			
				
				
				echo '<br /><h2><a>'.$this->l('Carts').' ('.sizeof($carts).')</a></h2>';
				$tokenCarts = Tools::getAdminToken('AdminCarts'.(int)(Tab::getIdFromClassName('AdminCarts')).(int)($cookie->id_employee));
				
			
				if ($carts AND sizeof($carts))
				{
					echo '
					<table cellspacing="0" cellpadding="0" class="table">
						<tr>
							<th class="center">'.$this->l('ID').'</th>
							<th class="center">'.$this->l('Convertito').'</th>
							<th class="center">'.$this->l('N.Ordine').'</th>
							<th class="center">'.$this->l('Oggetto').'</th>
							<th class="center">'.$this->l('Total').'</th>
							<th class="center">'.$this->l('In carico a').'</th>
							<th class="center">'.$this->l('Date').'</th>
							<th class="center">'.$this->l('Actions').'</th>
						</tr>';
					
					foreach ($carts AS $cart)
					{
						$cartI = new Cart((int)($cart['id_cart']));
						$summary = $cartI->getSummaryDetails();
						$currency = new Currency((int)($cart['id_currency']));
						$carrier = new Carrier((int)($cart['id_carrier']));
						$n_ordine = Db::getInstance()->getValue("SELECT id_order FROM orders WHERE id_cart = ".$cart['id_cart']."");
						if($n_ordine > 0) 
						{
							$convertito = "<img src='http://www.ezdirect.it/img/admin/enabled.gif' alt='Si' title='Si' />";
						}
						else
						{
							$convertito = "<img src='http://www.ezdirect.it/img/admin/red_no.gif' alt='No' title='No' />";
							$n_ordine = "--";
						}
						$nome_carrello = Db::getInstance()->getValue("SELECT name FROM cart WHERE id_cart = ".$cart['id_cart']."");
						$creato_da = Db::getInstance()->getValue("SELECT in_carico_a FROM cart WHERE id_cart = ".$cart['id_cart']."");
						$creato_da_nome = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".$creato_da."");
						if($creato_da == 0) 
						{
							$creato_da_nome = "--";
						}
						echo '
						<tr '.($irow++ % 2 ? 'class="alt_row"' : '').' style="cursor:pointer" >
							<td class="center" onclick="document.location = \'?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&id_cart='.$cart['id_cart'].'&viewcart&token='.$this->token.'&tab-container-1=4\';">'.$cart['id_cart'].'</td>
							<td>'.$convertito.'</td>
							<td>'.$n_ordine.'</td>
							
							<td onclick="document.location = \'?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&id_cart='.$cart['id_cart'].'&viewcart&token='.$this->token.'&tab-container-1=4\';">'.$nome_carrello.'</td>
							<td align="right" onclick="document.location = \'?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&id_cart='.$cart['id_cart'].'&viewcart&token='.$this->token.'&tab-container-1=4\';">'.Tools::displayPrice($summary['total_products'], $currency).'</td>
							<td onclick="document.location = \'?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&id_cart='.$cart['id_cart'].'&viewcart&token='.$this->token.'&tab-container-1=4\';">'.$creato_da_nome.'</td>
							<td onclick="document.location = \'?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&id_cart='.$cart['id_cart'].'&viewcart&token='.$this->token.'&tab-container-1=4\';">'.Tools::displayDate($cart['date_add'], (int)($cookie->id_lang), false).'</td>
							
							<td align="center"><a href="index.php?tab=AdminCarts&id_customer='.$customer->id.'&id_cart='.$cart['id_cart'].'&deleteccart&token='.$tokenCarts.'" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }"><img src="../img/admin/delete.gif" /></a></td>
						</tr>';
					}
					echo '
					</table><br />
					
					';
				}
				else
					echo $this->l('No cart available').'.';
				echo '';
				
			if($cookie->id_employee != 1  && $cookie->id_employee != 2 && $cookie->id_employee != 6 && $cookie->id_employee != 7) {
			}
			else {
			/*echo '

			
				<a href="index.php?tab=AdminCustomers&viewcart&editcart&createnew&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4#modifica-carrello" class="button" onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }">Crea un nuovo carrello</a>';*/
			}
			echo '
			
			<a href="index.php?tab=AdminCustomers&viewcart&editcart&createnew&provvisorio=1&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4#modifica-carrello" class="button" onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }">Crea un nuovo carrello PROVVISORIO</a>
			';
			
			
			
			
			echo '
					<br /><br /><h2>'.$this->l('Discounts').' ('.sizeof($discounts).')</h2>';
			if (sizeof($discounts))
			{
				echo '
				<table cellspacing="0" cellpadding="0" class="table">
					<tr>
						<th>'.$this->l('ID').'</th>
						<th>'.$this->l('Code').'</th>
						<th>'.$this->l('Type').'</th>
						<th>'.$this->l('Value').'</th>
						<th>'.$this->l('Qty available').'</th>
						<th>'.$this->l('Status').'</th>
						<th>'.$this->l('Actions').'</th>
					</tr>';
				$tokenDiscounts = Tools::getAdminToken('AdminDiscounts'.(int)(Tab::getIdFromClassName('AdminDiscounts')).(int)($cookie->id_employee));
				foreach ($discounts AS $discount)
				{
					echo '
					<tr '.($irow++ % 2 ? 'class="alt_row"' : '').'>
						<td align="center">'.$discount['id_discount'].'</td>
						<td>'.$discount['name'].'</td>
						<td>'.$discount['type'].'</td>
						<td align="right">'.$discount['value'].'</td>
						<td align="center">'.$discount['quantity_for_user'].'</td>
						<td align="center"><img src="../img/admin/'.($discount['active'] ? 'enabled.gif' : 'disabled.gif').'" alt="'.$this->l('Status').'" title="'.$this->l('Status').'" /></td>
						<td align="center">
							<a style="cursor:pointer" onclick="window.open(\'?tab=AdminDiscounts&id_discount='.$discount['id_discount'].'&adddiscount&token='.$tokenDiscounts.'\', \'blank\', \'height=600,width=1200,modal=yes,alwaysRaised=yes,scrollbars=yes\');"><img src="../img/admin/edit.gif" /></a>
							<a style="cursor:pointer" onclick="window.open(\'?tab=AdminDiscounts&id_discount='.$discount['id_discount'].'&deletediscount&token='.$tokenDiscounts.'\', \'blank\', \'height=600,width=1200,modal=yes,alwaysRaised=yes,scrollbars=yes\');"><img src="../img/admin/delete.gif" /></a>
						</td>
					</tr>';
				}
				echo '
				</table>';
				
				
			

			}
			else
				echo $customer->firstname.' '.$customer->lastname.' '.$this->l('has no discount vouchers').'.';
				
			
			// display hook specified to this page : AdminCustomers
			if (($hook = Module::hookExec('adminCustomers', array('id_customer' => $customer->id))) !== false)
				echo '<br /><br />'.$hook.'';
			echo '<br /><br />
			<iframe style="display:none; margin-left:-10px" src="" id="ordini-frame" width="920" height="800"></iframe>'
			
			;
		}
		
		else if(isset($_GET['vieworder'])) {
			include("AdminOrders.php");
			
			AdminOrders::viewDetails();
			
			echo '<br /><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4" class="button">Torna alla lista degli ordini</a>';
		
		}
		
		else if(isset($_GET['viewcart'])) {
		
			include("AdminCarts.php");
			
			AdminCarts::viewDetails();
			
			echo '<br /><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4" class="button">Torna alla lista degli ordini</a>';
		
		}
		
		
		//FINE ORDINI
		echo '</div>';
		}
		else { }
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 5) {
		// INIZIO FATTURE
		
			$fatture = Db::getInstance()->executeS("SELECT * FROM fattura WHERE id_customer = ".Tools::getValue('id_customer')." GROUP BY id_fattura ORDER BY data_fattura DESC");
			
		echo '
		<div class="tab1" id="fatture-customer"><h2><a>'.$this->l('Fatture').' ('.sizeof($fatture).')</a></h2>';
	
		
			
			if(!isset($_GET['viewfattura'])) {
			
			echo '
				<table cellspacing="0" cellpadding="0" class="table">
					<tr>
						<th>'.$this->l('ID Fattura').'</th>
						<th>'.$this->l('ID Storico').'</th>
						<th>'.$this->l('Rif. ordine').'</th>
						<th>'.$this->l('Data fattura').'</th>
						<th>'.$this->l('Importo fattura').'</th>
					</tr>';
			
			foreach($fatture as $fattura) {
			
				$datafattura = date("d/m/Y", strtotime($fattura['data_fattura']));
				echo '<tr style="cursor:pointer" onclick="document.location = \'index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&id_fattura='.$fattura['id_fattura'].'&tab-container-1=5&viewfattura&updatefattura\';">
				<td>'.$fattura['id_fattura'].'</td>
				<td>'.$fattura['id_storico'].'</td>
				<td>'.$fattura['rif_ordine'].'</td>
				<td>'.$datafattura.'</td>
				<td>'.Tools::displayPrice($fattura['totale_fattura'], $currency).'</td>
				</tr>
				';
			}
			
			echo '</table>';
			
				
				
			} else {
		
				include("AdminFatture.php");
				
				AdminFatture::postProcess();
				
				echo '<br /><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4" class="button">Torna alla lista degli ordini</a>';
		
			}
		
		
		
		
		
		
		
		echo '</div>';
		}
		else { }
		//FINE FATTURE
		
		
		$employeemess = new Employee($cookie->id_employee);
		// INIZIO TICKET
		$gentilecliente = "Gentile cliente, <br />";
		
		$firma = '<br />
							Cordiali saluti / Best regards<br /><br />
							'.$employeemess->firstname." ".$employeemess->lastname.'<br /><br />
							Ezdirect srl <a href="http://www.ezdirect.it">www.ezdirect.it</a><br /><br />

							Tel +39 0585821163 '.$interno.'<br />
							Fax +39 0585821286<br />
						
							<br />
							Prima di stampare pensa all\'ambiente - Think about the environment before printing
							<p style="font-size:10px">
							Ai sensi del Decreto Legislativo n. 196/2003, si precisa che le informazioni contenute in questo messaggio e negli eventuali allegati sono riservate e per uso esclusivo del destinatario. Persone diverse dallo stesso non possono copiare o distribuire il messaggio a terzi. Chiunque riceva questo messaggio per errore, &egrave; pregato di distruggerlo e di informare immediatamente info@ezdirect.it<br /><br />
							
							This message is for the designated recipient only and may contain privileged or confidential information. If you have received it in error, please notify the sender immediately and delete the original. Any other use of the email by you is
							prohibited.
							
							</p>';
							
		$threads = Db::getInstance()->ExecuteS("SELECT * FROM customer_thread WHERE id_contact != 7 AND id_customer = ".$customer->id." ORDER BY id_customer_thread DESC");
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 6) {
		echo '<div class="tab1" id="messages-customer">';
		
		if(isset($_GET['id_customer_thread'])) {
		
			$thread = Db::getInstance()->getRow("SELECT * FROM customer_thread ct WHERE ct.id_customer_thread = ".$_GET['id_customer_thread']."");
	
				$incmess = new Employee($thread['id_employee']);
				
				$tiporichiesta = Db::getInstance()->getValue("SELECT name FROM contact_lang WHERE id_lang = 5 and id_contact = ".$thread['id_contact']."");
				
				$annoticket = substr($thread['date_add'],2,2);
					if($thread['id_contact'] == 2) { $siglat = 'A'; } else if($thread['id_contact'] == 4) { $siglat = 'T'; } else if($thread['id_contact'] == 3) { $siglat = 'V'; } else if($thread['id_contact'] == 6) { $siglat = 'R'; } else if($thread['id_contact'] == 8) { $siglat = 'D'; } else if($thread['id_contact'] == 9) { $siglat = 'S'; }
					$idticketunivoco = $siglat.$annoticket.$thread['id_customer_thread'];
				
				
						switch($thread['status']) {
							case 'open': $status_thread = "<img src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; break;
							case 'closed': $status_thread = "<img src='../img/admin/status_green.gif' alt='Chiuso' title='Chiuso' />"; break;
							case 'pending1': $status_thread = "<img src='../img/admin/status_orange.gif' alt='In lavorazione' title='In lavorazione' />"; break;
							default: $status_thread = "<img src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; break;
						}
				
				$dataultimo = Db::getInstance()->getValue("SELECT date_add FROM customer_message WHERE id_customer_thread = ".$thread['id_customer_thread']." ORDER BY date_add DESC");
				
				$oggetto = Db::getInstance()->getValue("SELECT subject FROM customer_thread WHERE id_customer_thread = ".$thread['id_customer_thread']." ORDER BY date_add ASC");
				
				if($oggetto == '') {
					$oggetto = Db::getInstance()->getValue("SELECT message FROM customer_message WHERE id_customer_thread = ".$thread['id_customer_thread']." ORDER BY date_add ASC");
				} 
				else {
				
				}
				
			echo "<h2 style='float:left; margin-right:10px'>".($thread['id_contact'] == 9 ? 'RMA n.' : 'Ticket n.')." ".$idticketunivoco."</h2>";
		
		}
	
		echo '<a class="button" style="display:block;float:left;margin-right:10px" href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewticket&aprinuovoticket&token='.$this->token.'&tab-container-1=6">Apri ticket per conto del cliente</a>
		
		&nbsp;&nbsp;&nbsp;<a style="display:block;float:left;margin-right:10px" class="button" href="http://www.ezdirect.it" target="_blank">Collegati al sito</a>
		<br /><br />
		<div style="clear:both"></div>
		';
		
		$impiegati_ez = Db::getInstance()->ExecuteS("SELECT id_employee, firstname, lastname FROM employee");
		
		$employeemess = new Employee($cookie->id_employee);
		
		
		switch($cookie->id_employee) {
		case 1: $interno = "(201)";
		case 2: $interno = "(202)";
		case 3: $interno = "(204)";
		case 4: $interno = "(205)";
		case 5: $interno = "(203)";
		case 6: $interno = "";
		case 7: $interno = "(207)";
		default: $interno = "";
		}

		
		if(isset($_GET['error']) && $_GET['error'] == '1tk') {
			
			echo '<div style="width:100%; height:30px; border:1px solid red; color:red">Il messaggio era vuoto e non &egrave; stato inviato, ma sono stati aggiornati i campi status e in carico a.</div>';
		}
		
		
		if(isset($_POST['cambiastatus_listing'])) {

				
			Db::getInstance()->ExecuteS("UPDATE customer_thread SET status = '".Tools::getValue('cambiastatus_listing')."' WHERE id_customer_thread = '".Tools::getValue('id_customer_thread')."'");
			
			Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&token='.$this->token.'&conf=4&tab-container-1=6');
			
		}
		
		if(isset($_POST['cambiaincarico'])) {

			
			Customer::cambiaIncaricoVeloce(Tools::getValue('tipo'), Tools::getValue('cambiaincarico'), Tools::getValue('id_thread'), Tools::getValue('precedente_incarico'), Tools::getValue('id_ticket_univoco'), Tools::getValue('id_customer'));
			
			Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&token='.$this->token.'&conf=4&tab-container-1=6');
			
		}
		
		if(isset($_POST['submitTicket'])) {

				
			Db::getInstance()->ExecuteS("UPDATE customer_thread SET subject = '".addslashes(Tools::getValue('ticket_subject'))."', note = '".addslashes(Tools::getValue('ticket_note'))."', id_contact = '".Tools::getValue('id_contact')."', status = '".Tools::getValue('cambiastatus')."' WHERE id_customer_thread = '".Tools::getValue('id_customer_thread')."'");
			
			
			if(Tools::getValue('id_contact') == 9) {
			
				Db::getInstance()->ExecuteS("UPDATE rma SET id_address = '".addslashes(Tools::getValue('indirizzo_rma'))."', rma_type = '".addslashes(Tools::getValue('rma_type'))."', rma_shipping = '".addslashes(Tools::getValue('rma_shipping'))."', qta = '".addslashes(Tools::getValue('qta'))."', seriale = '".addslashes(Tools::getValue('seriale'))."', in_garanzia = '".(Tools::getIsset('in_garanzia') ? 1 : 0)."', attivita_svolta = '".addslashes(Tools::getValue('attivita_svolta'))."', altra_attivita = '".addslashes(Tools::getValue('altra_attivita'))."', costo_materiale = '".str_replace(",", ".", addslashes(Tools::getValue('costo_materiale')))."', costo_manodopera = '".str_replace(",", ".", addslashes(Tools::getValue('costo_manodopera')))."', trasporti_qta = '".addslashes(Tools::getValue('trasporti_qta'))."', manodopera_qta = '".str_replace(",", ".", addslashes(Tools::getValue('manodopera_qta')))."', totale_trasporti = '".str_replace(",", ".", addslashes(Tools::getValue('totale_trasporti')))."' WHERE id_customer_thread = '".Tools::getValue('id_customer_thread')."'");
			}
					
			$impiegato_attuale = Db::getInstance()->getValue("SELECT id_employee FROM customer_thread WHERE id_customer_thread = '".Tools::getValue('id_customer_thread')."'");
			
			if($impiegato_attuale != Tools::getValue('assegnaimpiegato')) {
					
				Db::getInstance()->ExecuteS("UPDATE customer_thread SET id_employee = '".Tools::getValue('assegnaimpiegato')."' WHERE id_customer_thread = '".Tools::getValue('id_customer_thread')."'");
					
				$mailimp = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".Tools::getValue('assegnaimpiegato')."'");
						
				$tokenimp = Tools::getAdminToken("AdminCustomers"."2".Tools::getValue('assegnaimpiegato'));
						
				$linkimp = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewticket&id_customer_thread=".Tools::getValue('id_customer_thread')."&token=".$tokenimp.'&tab-container-1=6';
						
				$msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato il ticket numero ".Tools::getValue('id_ticket_univoco')." su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirlo</a>.";
						
				if($thread['id_employee'] != 0) {
							
					$mailimprev = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".$thread['id_employee']."'");
					$nomeimprev = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = '".Tools::getValue('assegnaimpiegato')."'");
							
					$msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione del ticket numero ".Tools::getValue('id_ticket_univoco')." su Ezdirect e l'ha passata a ".$nomeimprev.".";
							
					$params = array(
					'{msg}' => $msgimprev);
							
					Mail::Send(5, 'msg_base', Mail::l('Gestione ticket revocata', 5), 
						$params, $mailimprev, NULL, NULL, NULL, NULL, NULL, 
						_PS_MAIL_DIR_, true);
						
				}
						
				else {
						
				}
						
						
				$params = array(
				'{msg}' => $msgimp);
						
				if($_POST['assegnaimpiegato'] != 0) {
						
					Mail::Send(5, 'msg_base', Mail::l('Ticket assegnato a te su Ezdirect', 5), 
						$params, $mailimp, NULL, NULL, NULL, NULL, NULL, 
						_PS_MAIL_DIR_, true);
									
				} else { }
				
			}
					
			Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&viewticket&id_customer_thread='.Tools::getValue('id_customer_thread').'&token='.$this->token.'&conf=4&tab-container-1=6');
		}
			

		if(isset($_GET['viewticket'])) {
		
				
			

			if(isset($_GET['id_customer_thread'])) {			
				echo '<div id="tab-container-ticket" style="margin-top:16px">
				
				
				
				<ul id="tab-container-ticket-nav" style="list-style-type:none; margin-top:-5px; margin-left:-20px">
				<li><a href="#ticket-1"><strong>Generale</strong></a></li>
				<li><a href="#ticket-2"><strong>Cronologia</strong></a></li>
				</ul>';
			
		
				if(!isset($_GET['aprinuovoticket'])) {
			
					if (isset($_GET['filename'])) {
						$filename = $_GET['filename'];
						
						if(strpos($filename, ":::")) {
			
							$parti = explode(":::", $filename);
							$nomecodificato = $parti[0];
							$nomevero = $parti[1];
							
						}
			
						else {
							$nomecodificato = $filename;
							$nomevero = $filename;
			
						}
						
						if (file_exists(_PS_UPLOAD_DIR_.$nomecodificato)) {
							self::openUploadedFile();
						}
						else { }
					} 
					else { }
				
				echo '
				
				<div class="yetii-ticket" id="ticket-1">
				<fieldset style="width:95%">';
				
				
				echo '
				<form id="modificaticket" action="'.$currentIndex.'&id_customer='.$_GET['id_customer'].'&submitTicket&viewcustomer&token='.$this->token.'&tab-container-1=6" method="post" autocomplete="off">';
				
				
				
				echo '
				
				<div class="anagrafica-cliente" style="width:800px; margin-bottom:10px">
				<strong>Oggetto</strong><br />
				
					<textarea name="ticket_subject" id="ticket_subjectContent" style="width:690px;height:40px;display:block;float:left">'.strip_tags(htmlspecialchars_decode($oggetto)).'</textarea>
					<br /><br />
				</div>

				
				<div class="clear"></div>
			
				
				<div class="anagrafica-cliente">
				ID Ticket<br />
				<span class="tab-span">'.$idticketunivoco.'</span><br />
				</div>
				<div class="anagrafica-cliente" style="width:250px">
				In carico a<br />';
					echo '
				<input type="hidden" name="id_ticket_univoco" value="'.$idticketunivoco.'" />
				<select style="width:250px" name="assegnaimpiegato">';
				
				echo '<option value="0"'.($thread['id_employee'] == 0 ? 'selected="selected"' : '').' >Da assegnare</option>';
				
				foreach ($impiegati_ez as $impez) {
				
					echo "<option value='".$impez['id_employee']."' ".($thread['id_employee'] == $impez['id_employee'] ? 'selected="selected"' : '')." >".$impez['firstname']."</option>";
				
				}

				echo '</select>
		
				</div>
				
					<div class="anagrafica-cliente">
				Data apertura<br />
				<span class="tab-span">'.Tools::displayDate($thread['date_add'], (int)($cookie->id_lang), true).'</span><br />
				</div>
			
				<div class="clear"></div>
				
				
				
				<div class="clear"></div>
				<div class="anagrafica-cliente">
				Status<br />
				'.$status_thread.' <input type="hidden" name="id_customer" value="'.Tools::getValue('id_customer').'" />
				<input type="hidden" name="id_customer_thread" value="'.Tools::getValue('id_customer_thread').'" />

				<select name="cambiastatus">
				<option value="closed" '.($thread['status'] == 'closed' ? 'selected="selected"' : '').'>Chiuso</option>
				<option value="pending1" '.($thread['status'] == 'pending1' ? 'selected="selected"' : '').'>In lavorazione</option>
				<option value="open" '.($thread['status'] == 'open' ? 'selected="selected"' : '').'>Aperto</option>
				</select><br />
				</div>
				
				<div class="anagrafica-cliente" style="width:250px">
				Tipo ticket<br />
				';
				
				if($thread['id_contact'] == 9) {
					echo '<span class="tab-span" style="width:240px">RMA</span><br />';
				}
				
				else {
				
				echo '
				<select style="width:250px" name="id_contact" id="id_contact">
										<option '.($thread['id_contact'] == 4? 'selected="selected"' : '').' value="4">Assistenza tecnica</option>
										<option '.($thread['id_contact'] == 2? 'selected="selected"' : '').' value="2">Assistenza amministrativa - contabilit&agrave;</option>
										<option '.($thread['id_contact'] == 8? 'selected="selected"' : '').' value="8">Assistenza amministrativa - ordini</option>
										<option '.($thread['id_contact'] == 3? 'selected="selected"' : '').' value="3">Rivenditori</option>
										<option '.($thread['id_contact'] == 9? 'selected="selected"' : '').' value="9">RMA</option>
										</select>
						';				
				}
				echo '</div>
				
				
					<div class="anagrafica-cliente">
				Data ultima com.<br />
				<span class="tab-span">'.Tools::displayDate($dataultimo, (int)($cookie->id_lang), true).'</span><br />
				</div>';
			
				
				
				
				if($thread['id_contact'] == 9) {
					$dati_rma = Db::getInstance()->getRow('SELECT * FROM rma WHERE id_customer_thread = '.$thread['id_customer_thread'].'');
					
					$dati_fattura = Db::getInstance()->getRow('SELECT * FROM fattura WHERE id_riga = '.$thread['id_order'].'');
					echo '<div class="anagrafica-cliente" style="width:100px">
					Numero fattura<br />
					<span class="tab-span" style="width:100px">'.($thread['id_order'] != 0 ? $dati_fattura['id_fattura'] : '<em>Nessuno</em>').'</span><br />
					</div>';
					
					echo '<div class="anagrafica-cliente" style="width:100px">
					Data fattura<br />
					<span class="tab-span" style="width:100px">'.($thread['id_order'] != 0 ? Tools::displayDate($dati_fattura['data_fattura'], $cookie->id_lang) : '<em>--</em>').'</span><br />
					</div>';
					
					echo '<div class="anagrafica-cliente" style="width:100px">
					Codice RMA<br />
					<span class="tab-span" style="width:100px">'.($dati_rma['codice_rma']).'</span><br />
					</div>';
					
					echo '<div class="anagrafica-cliente" style="width:130px">
					Codice seriale prodotto<br />
					<input type="text" name="seriale" value="'.($dati_rma['seriale']).'"  style="width:120px" /><br />
					</div>';
					
					echo '<div class="anagrafica-cliente" style="width:70px">
					Qta prodotto<br />
					<input type="text" name="qta" value="'.($dati_rma['qta']).'" style="width:70px" /><br />
					</div>';
					
					echo '<div class="anagrafica-cliente" style="width:70px">
					In garanzia?<br />
					<input type="checkbox" name="in_garanzia" '.($dati_rma['in_garanzia'] == 1 ? 'checked="checked"' : '').' /><br />
					</div>';
				
				}
				else {
				
				}
				
				echo '
				<div class="clear"></div>
				<div class="anagrafica-cliente" style="width:720px">
				Prodotto di riferimento<br />
				<span class="tab-span" style="width:720px">'; 
				if($thread['id_product'] != 0) 
				{ 
					$prodrichiesta = Db::getInstance()->getRow("SELECT reference, name FROM product JOIN product_lang ON product.id_product = product_lang.id_product WHERE id_lang = 5 and product.id_product = ".$thread['id_product'].""); 
					echo $prodrichiesta['reference']." - ".$prodrichiesta['name']; 
					
					
				} 
				else if ($thread['id_contact'] == 9 && $thread['id_product'] == 0) {
					echo $dati_rma['rma_product'];
				}
				else { 
					echo '<em>Nessuno</em>';
				} 
				echo '</span><br />
				</div>

				
				<div class="clear"></div>';
		
				if($thread['id_contact'] == 9) {
				
				
				
				echo '<div class="anagrafica-cliente" style="width:100px">
				Tipo RMA<br />
				<select name="rma_type" style="width:110px">
				<option name="Reso" '.($dati_rma['rma_type'] == 'Reso' ? 'selected="selected"' : '').'>Reso</option>
				<option name="Riparazione"  '.($dati_rma['rma_type'] == 'Riparazione' ? 'selected="selected"' : '').'>Riparazione</option>
				</select><br />
				</div>';
				
				echo '<div class="anagrafica-cliente" style="width:100px">
				Spedizione<br />
				<select name="rma_shipping" style="width:110px">
				<option name="Corriere Ezdirect" '.($dati_rma['rma_shipping'] == 'Corriere Ezdirect' ? 'selected="selected"' : '').'>Corriere Ezdirect</option>
				<option name="Corriere Cliente"  '.($dati_rma['rma_shipping'] == 'Corriere Cliente' ? 'selected="selected"' : '').'>Corriere Cliente</option>
				</select><br />
				</div>';
				$indirizzi_rma = Db::getInstance()->executeS('SELECT * FROM address a JOIN state s ON a.id_state = s.id_state WHERE a.active = 1 AND a.deleted = 0 AND a.id_customer='.$customer->id.''); 
				
				echo '<div class="anagrafica-cliente" style="width:480px">
				Indirizzo spedizione reso<br />
				<select name="indirizzo_rma" style="width:490px">';
				foreach ($indirizzi_rma as $indirizzo_rma) {
				
				echo '<option value="'.$indirizzo_rma['id_address'].'" '.($indirizzo_rma['id_address'] == $dati_rma['id_address'] ? 'selected="selected"' : '').'>'.$indirizzo_rma['address1'].' - '.$indirizzo_rma['postcode'].' '.$indirizzo_rma['city'].' ('.$indirizzo_rma['iso_code'].')</option>';
				
				}
				echo '</select>
				
				<br /><br />
				</div>';
				
				
				echo '<div class="clear"></div>';
				
				
				echo '<div class="anagrafica-cliente">
				Attivit&agrave; svolta<br />
				<select name="attivita_svolta" id="attivita_svolta" style="width:230px">
										<option '.($dati_rma['attivita_svolta'] == 0 || $dati_rma['attivita_svolta'] == '' ? 'selected="selected"' : '').' value="0">Nessuna</option>
										<option '.($dati_rma['attivita_svolta'] == 1? 'selected="selected"' : '').' value="1">Sostituito con nuovo</option>
										<option '.($dati_rma['attivita_svolta'] == 2? 'selected="selected"' : '').' value="2">Riparato</option>
										<option '.($dati_rma['attivita_svolta'] == 3? 'selected="selected"' : '').' value="3">Inviato al centro assistenza</option>
										<option '.($dati_rma['attivita_svolta'] == 4? 'selected="selected"' : '').' value="4">Altro</option>
										</select>
				</div>
				<div class="anagrafica-cliente" style="width:480px">
				Se in "Attivit&agrave; svolta" hai selezionato "altro", specificalo qua:
				<input type="text" name="altra_attivita" style="width:480px" value="'.$dati_rma['altra_attivita'].'" />
				</div>
				<div class="clear"></div>
				
				';
				
				
				echo '<div class="anagrafica-cliente" style="width:380px;"><br />
				Dettaglio costi
				<script type="text/javascript">
				function CalcTotRMA () {
				
					var num_costo_materiale = document.getElementById(\'costo_materiale\').value;
					var costo_materiale = parseFloat(num_costo_materiale.replace(/\s/g, "").replace(",", "."));
					if(isNaN(costo_materiale)) { costo_materiale = 0; }
					
					var num_costo_manodopera = document.getElementById(\'costo_manodopera\').value;
					var costo_manodopera = parseFloat(num_costo_manodopera.replace(/\s/g, "").replace(",", "."));
					if(isNaN(costo_manodopera)) { costo_manodopera = 0; }
					
					var num_totale_trasporti = document.getElementById(\'totale_trasporti\').value;
					var totale_trasporti = parseFloat(num_totale_trasporti.replace(/\s/g, "").replace(",", "."));
					if(isNaN(totale_trasporti)) { totale_trasporti = 0; }
					
					
					var totale_RMA = costo_manodopera + costo_materiale + totale_trasporti;
					
					
					document.getElementById(\'totale_RMA\').innerHTML = totale_RMA.toFixed(2).replace(\'.\',\',\');
					
				}
				</script>
				<table style="font-size:12px">
				<tr><td style="width:150px">Costo materiale</td><td></td><td><input type="text" onkeyup="CalcTotRMA();" name="costo_materiale" id="costo_materiale" style="width:60px; text-align:right" value="'.str_replace(".",",",$dati_rma['costo_materiale']).'" /> &euro;</td></tr>
				<tr><td>Costo manodopera</td><td><select name="manodopera_qta" id="manodopera_qta" style="width:60px" onchange="document.getElementById(\'costo_manodopera\').value = document.getElementById(\'manodopera_qta\').value*40; CalcTotRMA();">
				<option value="0" '.($dati_rma['manodopera_qta'] == 0 ? 'selected="selected"' : '').'>Scegli</option>
				<option value="0.5" '.($dati_rma['manodopera_qta'] == 0.5 ? 'selected="selected"' : '').'>0,5</option>
				<option value="1" '.($dati_rma['manodopera_qta'] == 1 ? 'selected="selected"' : '').'>1</option>
				<option value="1.5" '.($dati_rma['manodopera_qta'] == 1.5 ? 'selected="selected"' : '').'>1,5</option>
				<option value="2" '.($dati_rma['manodopera_qta'] == 2 ? 'selected="selected"' : '').'>2</option>
				<option value="2.5" '.($dati_rma['manodopera_qta'] == 2.5 ? 'selected="selected"' : '').'>2,5</option>
				<option value="3" '.($dati_rma['manodopera_qta'] == 3 ? 'selected="selected"' : '').'>3</option>
				<option value="3.5" '.($dati_rma['manodopera_qta'] == 3.5 ? 'selected="selected"' : '').'>3,5</option>
				<option value="4" '.($dati_rma['manodopera_qta'] == 4 ? 'selected="selected"' : '').'>4</option>
				<option value="4.5" '.($dati_rma['manodopera_qta'] == 4.5 ? 'selected="selected"' : '').'>4,5</option>
				<option value="5" '.($dati_rma['manodopera_qta'] == 5 ? 'selected="selected"' : '').'>5</option>
				<option value="5.5" '.($dati_rma['manodopera_qta'] == 5.5 ? 'selected="selected"' : '').'>5,5</option>
				<option value="6" '.($dati_rma['manodopera_qta'] == 6 ? 'selected="selected"' : '').'>6</option>
				<option value="6.5" '.($dati_rma['manodopera_qta'] == 6.5 ? 'selected="selected"' : '').'>6,5</option>
				<option value="7" '.($dati_rma['manodopera_qta'] == 7 ? 'selected="selected"' : '').'>7</option>
				</select> ore
				</td><td><input type="text" name="costo_manodopera" id="costo_manodopera" value="'.str_replace(".",",",$dati_rma['costo_manodopera']).'" style="width:60px; text-align:right" onkeyup="CalcTotRMA();" /> &euro;</td></tr>
				<tr><td>Trasporti qta</td><td><input type="text" name="trasporti_qta" id="trasporti_qta" value="'.str_replace(".",",",$dati_rma['trasporti_qta']).'" onkeyup="document.getElementById(\'totale_trasporti\').value = (document.getElementById(\'trasporti_qta\').value*9.90).toFixed(2).replace(\'.\',\',\'); CalcTotRMA();"  style="width:60px; text-align:right" /></td><td><input type="text" name="totale_trasporti" id="totale_trasporti" value="'.str_replace(".",",",$dati_rma['totale_trasporti']).'" style="width:60px; text-align:right" onkeyup="CalcTotRMA();" /> &euro;</td></tr>
				<tr><td>Totale</td><td></td><td><span class="tab-span" id="totale_RMA" style="width:63px; margin-right:3px; text-align:right; float:left">'.number_format(($dati_rma['costo_materiale']+$dati_rma['costo_manodopera']+$dati_rma['totale_trasporti']),2,",","").'</span> &euro;</tr>
				</table>
				</div>';
				
				echo '<div class="anagrafica-cliente" style="width:325px"><br />Template RMA
				<span class="tab-span" style="width:325px"><a href="http://www.ezdirect.it/rma/'.$dati_rma['codice_rma'].'.html" target="_blank">http://www.ezdirect.it/rma/'.$dati_rma['codice_rma'].'.html</a></span></div>';
				
				echo '<div class="clear"></div>';
				
				}
				
				
		
			
				
			
				echo '<strong>Nota interna visibile solo agli impiegati</strong><br />
					<textarea name="ticket_note" id="ticketnoteContent" style="width:80%;height:100px">'.Tools::htmlentitiesUTF8($thread['note']).'</textarea><br />
					';
	
				echo '<br />';
				
				
				echo '<input type="submit" value="'.$this->l('   Save   ').'" name="submitTicket" class="button" />';
				
		echo '</form>
		</fieldset><br /><br /></div>';
			}
			
			if(isset($_GET['id_customer_thread'])) {
				echo '<div class="yetii-ticket" id="ticket-2">';
			}	
				$messaggit = Db::getInstance()->ExecuteS("SELECT * FROM customer_message cm WHERE cm.id_customer_thread = ".$_GET['id_customer_thread']."");
				
				foreach($messaggit as $messaggiot) {
				
					echo "<table style='width:100%; 
					border:1px solid #dfd5c3; 
					";
					if ($messaggiot['id_employee'] == 0) {
						echo 'background-color: #ffffff;';
					} else {
			
						echo 'background-color: #ffecf2;';
					}
					$employeezt = new Employee($messaggiot['id_employee']);
					echo "margin-bottom:15px'>";
					echo '<tr>
					<td style="colspan:2">
						<table>
				
						<td style="font-size:14px; width:230px">Da: <strong>';if ($messaggiot['id_employee'] == 0) {
						if($messaggiot['email'] == "") {
							echo $customer->firstname." ".$customer->lastname; 
						}
						else {
							echo "Ticket aperto dallo staff";
						}
						} else { echo $employeezt->firstname." ".$employeezt->lastname; } echo '<strong></td>
						
						<td style="width:170px"><strong>Data: </strong>'.$messaggiot['date_add'].'</td>
						<td style="width:180px"><strong>Allegati: </strong>';
						if(!empty($messaggiot['file_name'])) {
						
							$allegati = explode(";",$messaggiot['file_name']);
							$nall = 1;
							foreach($allegati as $allegato) {
								
								
								if(strpos($allegato, ":::")) {
					
									$parti = explode(":::", $allegato);
									$nomevero = $parti[1];
									
								}
					
								else {
									$nomevero = $allegato;
					
								}
					
								if($allegato == "") { } else {
									if($nall == 1) { } else { echo " - "; }
									echo '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewticket&id_customer_thread='.$thread['id_customer_thread'].'&tab-container-1=6&token='.$this->token.'&filename='.$allegato.'"><span style="color:red">'.$nomevero.'</span></a>';
									$nall++;
								}
							}
						}
						
						else { echo 'Nessun allegato'; }
						
						
						echo '</td>';
						
						$cc = Db::getInstance()->getValue("SELECT cc FROM ticket_cc WHERE type = 't' AND msg = ".$messaggiot['id_customer_message']."");
						if(!empty($cc)) {
						
							echo '<td>';
						
							$ccs = explode(";",$cc);
							$cc_str = "<strong>CC:</strong> ";
							
							foreach ($ccs as $conoscenza) {
							
								$imp = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".$conoscenza."");
								$cc_str .= $imp." ";
							}
							
							echo $cc_str;
							echo "</td>";
						
						}
						else {
						
							echo '<td><strong>CC:</strong> Nessuno</td>';
						
						}
						
						echo '</tr></table>
					</td>
					</tr>
					<tr>
					<td style="colspan:2">
						<table>
						<tr>
						<td style="width:100px"><strong>Messaggio</strong></td>
						<td>'.htmlspecialchars_decode($messaggiot['message']).($messaggiot['id_employee'] == 0 ? '' : '
						<form method="post">
						<input type="hidden" name="modifica_messaggio_ticket" value="'.htmlentities($messaggiot['message'], ENT_QUOTES | ENT_SUBSTITUTE, "UTF-8").'" />
						<input type="submit" name="modifica_messaggio_submit" value="Modifica" class="button" />
						</form>').'
						</td>
						</tr>
						</table>
					</td>';
					echo '</tr>
					';
					
					if($messaggiot['email'] == '') { 
					
					} else {
					
					echo '<tr><td style="colspan:2">
						<table>
						<tr>
						<td style="width:100px"><strong>Inviato a</strong></td>
						<td>'.htmlspecialchars_decode($messaggiot['email']).'</td>
						</tr>
						</table>
					</td></tr>';
					
					} 
					
					echo '</table>';
					
					if($thread['id_contact'] == 9) {
					}
					else {
						echo '<strong>Ordine di riferimento</strong>: '.($thread['id_order'] != 0 ? $thread['id_order'] : '<em>Nessuno</em>').' - <strong>Prodotto di riferimento</strong>: '; if($thread['id_product'] != 0) { $prodrichiesta = Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_lang = 5 and id_product = ".$thread['id_product'].""); echo $prodrichiesta; } else { 	echo '<em>Nessuno</em>'; }
					}					
					/*
					if($thread['id_contact'] == 9) {
					
					echo '<br /><strong>Seriale</strong>: '.$dati_rma['seriale'].' - <strong>Codice RMA</strong>: '.$dati_rma['codice_rma'].' - <strong>Tipo RMA</strong>: '.$dati_rma['rma_type'].' - <strong>Spedizione</strong>: '.$dati_rma['rma_shipping'].' - <strong>Indirizzo</strong>: '.$indirizzo_rma['address1'].' - '.$indirizzo_rma['postcode'].' '.$indirizzo_rma['city'].' ('.$indirizzo_rma['iso_code'].')<br /><strong>Template RMA</strong>: <a href="http://www.ezdirect.it/rma/'.$dati_rma['codice_rma'].'.html" target="_blank">http://www.ezdirect.it/rma/'.$dati_rma['codice_rma'].'.html</a>';  
					
					}					
					*/
					
					echo '
					
					<br /><br />';
				}
			
			}
			
			else {
			
			}
			
			if(isset($_GET['aprinuovoticket'])) {
			
				echo '<h2>Apri ticket per conto del cliente</h2>';
				echo '<strong style="color:red">ATTENZIONE!!!!!!</strong>: quando si apre un ticket per il cliente, il testo del ticket viene notificato anche al cliente tramite mail.<br /><br />';
			
			}
		
			else {
			
				echo '<h2 id="rispondi-cliente-ticket">Rispondi al cliente</h2>';
				if(isset($_POST['modifica_messaggio_submit'])) {
					echo '<script type="text/javascript">
						 window.location = "#rispondi-cliente-ticket";
					</script>';
				}
			}

			$iso = Language::getIsoById((int)($cookie->id_lang));
			$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
			$ad = dirname($_SERVER["PHP_SELF"]);
				echo '
			<script type="text/javascript">
			var iso = \''.$isoTinyMCE.'\' ;
			var pathCSS = \''._THEME_CSS_DIR_.'\' ;
			var ad = \''.$ad.'\' ;
			</script>
			
			
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce3/tiny_mce.js"></script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>
			<script type="text/javascript">
				$(document).ready(function () {
				
dataChanged = 0;     // global variable flags unsaved changes      

function bindForChange(){    
     $(\'input,checkbox,text,textarea,radio,select\').bind(\'change\',function(event) { dataChanged = 1});
	 $(\'#messaggiom\').bind(\'input propertychange\', function() { dataChanged = 1 });
     $(\':reset,:submit\').bind(\'click\',function(event) { dataChanged = 0 });
}


function askConfirm(){  
    if (dataChanged){ 
        return "You have some unsaved changes.  Press OK to continue without saving." 
    }
}

window.onbeforeunload = askConfirm;
window.onload = bindForChange;
});
			</script>
			<script type="text/javascript">
					toggleVirtualProduct(getE(\'is_virtual_good\'));
					unitPriceWithTax(\'unit\');
			</script>';
			$employeemess = new Employee($cookie->id_employee);
			
			echo'	<form id="submit_ticket" name="submit_ticket" action="'.$currentIndex.'&id_customer='.$obj->id.'&viewcustomer&token='.$this->token.'&tab-container-1=6" method="post" class="std" enctype="multipart/form-data">';
			
			

			echo'	<fieldset>
						<table>';
						if(isset($_GET['aprinuovoticket'])) {
			
							echo '<input type="hidden" name="nuovoticket" value="1" />
							<tr><td>Tipo ticket</td>
										<td>
										<select name="id_contact" id="id_contact">
										<option value="4">Assistenza tecnica</option>
										<option value="2">Assistenza amministrativa - contabilit&agrave;</option>
										<option value="8">Assistenza amministrativa - ordini</option>
										<option value="9">RMA</option>
										<option value="3">Rivenditori</option>
										</select>
										</td>
										</tr>

							';
				
			
						}
		
						else {
								echo '<script type="text/javascript">
			
								$(document).ready(function () {
									
									$("#submit_ticket").submit(function () {
										d = document.getElementById("status").value;
										
										if(d != "'.$thread['status'].'") {
											
										}
										
										else {
											var salvamodifiche = window.confirm("STATUS: OK per procedere, ANNULLA per cambiare."); 
									
												if (salvamodifiche) {
		$(\'#waiting1\').waiting({ 
		elements: 10, 
		auto: true 
		});
		var overlay = jQuery(\'<div id="overlay"></div>\');
		var attendere = jQuery(\'<div id="attendere">ATTENDERE PREGO</div>\');
		overlay.appendTo(document.body);
		attendere.appendTo(document.body);
													$("#submitMessage").trigger("click");
												
												} else {
													 return false;
														
													//niente
												}
										}
									});
									
									
								});
					
							</script>';
							echo '<input type="hidden" value="1" name="isAnswer" />';
							echo '<input type="hidden" value="'.$thread['id_contact'].'" name="id_contact">';
						}	
							echo '<tr>
								<td>
								';
								
							$customer_phone = Db::getInstance()->getRow("SELECT	phone, phone_mobile FROM address WHERE id_customer = ".$customer->id." AND deleted = 0 AND fatturazione = 1 AND active = 1");
							
							if($customer_phone['phone'] == '') { $telefono_cliente = $customer_phone['phone_mobile']; } else { $telefono_cliente = $customer_phone['phone']; }
								
								echo '
								<input type="hidden" name="id_customer" value="'.$customer->id.'" />
								
								<input type="hidden" value="'.$thread['id_customer_thread'].'" name="id_customer_thread">
								</td>
							</tr>
					
							<tr><td>Email<br /></td><td>';
							
							
							if($customer->is_company == 1) { echo '<div style="float:left">';
							
							$persone = Db::getInstance()->executeS("SELECT firstname, lastname, email FROM persone WHERE id_customer = ".Tools::getValue('id_customer')." AND email != ''");
							
							echo '
							<select style="width:160px" name="seleziona-persona" onchange="javascript:document.getElementById(\'customer_email_msg\').value = (this.value);" >';
							echo "<option value=''>-- Seleziona persona --</option>";
							
							foreach ($persone as $persona) {
							
								echo "<option value='".$persona['email']."'>".$persona['firstname']." ".$persona['lastname']."</option>";
							
							}
							
							echo "</select></div>";
							} else { }
							
								echo '<div style="float:left; '.($customer->is_company == 1? 'margin-left:20px' : '').'">
							<input type="text" size="40" name="customer_email_msg" id="customer_email_msg" value="'.(!$thread['email'] ? $customer->email : $thread['email']).'" /><br />
							<span style="font-size:11px">In caso di più indirizzi mail, separare con punto e virgola (;)</span></div>';
							
			
							
							echo '<div style="float:left; margin-left:20px"> Tel. Cliente: <input type="text" readonly="readonly" value="'.$telefono_cliente.'" /></div></td></tr>
							';
							
							
								
							echo '
							<tr><td>Stato ticket</td>
							<td>
							<div style="float:left">
							<select name="status" id="status">
							<option value="open" '.($thread['status'] == 'open' ? 'selected="selected"' : '').'>Aperto   </option>
							<option value="pending1" '.($thread['status'] == 'pending1' ? 'selected="selected"' : '').'>In lavorazione   </option>
							<option value="closed" '.($thread['status'] == 'closed' ? 'selected="selected"' : '').'>Chiuso   </option>
							
							</select>
							</div><div style="float:left; margin-left:20px">
							In carico a';
					echo '
				<script type="text/javascript">
								function changeInCaricoA()
								{';
								
								
								foreach ($impiegati_ez as $impez) {
									echo '
									
									firstVar = document.getElementById("in_carico_a").value;
									
									if (document.getElementById("in_carico_a").value == "'.$impez['id_employee'].'") {
									
									
									document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').disabled = true;
									document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').checked = false;
									
									} else { document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').disabled = false; }
								';
								}
							echo '
								}
								</script>';
								
								echo '
				<select style="width:250px" name="in_carico_a" id="in_carico_a" onChange = "changeInCaricoA();">';
	
				foreach ($impiegati_ez as $impez) {
				
					if(isset($_GET['aprinuovoticket'])) {
						echo "<option value='".$impez['id_employee']."' ".($cookie->id_employee == $impez['id_employee'] ? "selected='selected'" : '')." >".$impez['firstname']."</option>";
					}
					
					else {
					
						echo "<option value='".$impez['id_employee']."' ".($thread['id_employee'] == 0 ? ($cookie->id_employee == $impez['id_employee'] ? "selected='selected'" : '') : ($thread['id_employee'] == $impez['id_employee'] ? "selected='selected'" : ''))." >".$impez['firstname']."</option>";
					}
				}

				echo '</select><br /><br /></td>
							</tr>
							 <tr><td>Conoscenza</td>
							<td>
							';
							
							foreach ($impiegati_ez as $impez) {
		
									echo "<input type='checkbox' name='conoscenza[]' id='conoscenza_".$impez['id_employee']."' value='".$impez['id_employee']."' /> ".$impez['firstname']."  ";
							
							}
							
							
							
							echo '</td>
							</tr>
							<tr><td><br /></td></tr>
							
							<tr><td>Allega file</td>
							<td>
							<input name="MAX_FILE_SIZE" value="20000000" type="hidden">
							<script type="text/javascript" src="jquery.MultiFile.js"></script>';
							if($customer->id == 2) {
								
								$_SESSION['KCFINDER'] = array();
								$_SESSION['KCFINDER']['disabled'] = false;
								$_SESSION['KCFINDER']['uploadURL'] = '../../documenti-clienti/';
								$_SESSION['fold_type'] = "federico-giannini-2"; 
								echo '
								<textarea name="joinFile[]" class="multi" id="joinFile" onclick="openKCFinder(this)"></textarea>';
								
							}
							
							else {
							
								echo '
								<input name="joinFile[]" class="multi" id="joinFile" type="file" onchange="handleFileSelect(this.files)">';
							
							}
							echo 'Anteprime: <span id="list-ticket"></span>
							</td>
							</tr>
							<tr><td>Messaggio</td>
							<td> 
							<br />
							
							<textarea id="message" class="rte" name="message" rows="15" cols="20" style="width:760px;height:220px">
							'.(isset($_POST['modifica_messaggio_submit']) ? stripslashes($_POST['modifica_messaggio_ticket']) : '').'
							
							
							
							</textarea>
							
							
							
							
							</td>
							</tr>';
							
							if(isset($_GET['aprinuovoticket'])) {
							
							echo '<tr>
							<td>Nota interna (solo per impiegati)</td>
							<td><textarea id="note" name="note" rows="5" cols="145"></textarea>
							</tr>';
							
							}
							
							echo '<tr><td></td>
							<td><input name="submitMessage" id="submitMessage" value="Invia" class="button_invio" type="submit"></td>
				
							</tr>
						</table>
					</fieldset>
				</form>';			

			
					
					if(isset($_GET['id_customer_thread'])) {
				echo '</div></div>';	
		
				}
				
			echo '<br /><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=6" class="button">Torna alla lista dei ticket</a>';
		
		}
		
		else {
		
			
			
			if (sizeof($threads))
			{
				echo '
				<table class="table"  cellspacing="0" cellpadding="0">
					<tr>
						<th class="center">Tipo richiesta</th>
						<th class="center">ID ticket</th>
						<th class="center" style="width:80px">'.$this->l('Status').'</th>
						<th style="text-align:left">In carico</th>
						<th style="text-align:center">Q.ta com.</th>
						<th class="center">Ultima com.</th>
						<th class="center">'.$this->l('Aperto il').'</th>
						<th class="center">Modificato il</th>
						<th class="center">Azioni</th>
						
						
					</tr>';
				foreach ($threads AS $thread) {
					$tiporichiesta = Db::getInstance()->getValue("SELECT name FROM contact_lang WHERE id_lang = 5 AND id_contact = ".$thread['id_contact']."");
					$id_incmess = $thread['id_employee'];
					$incmess = new Employee($id_incmess);
						
						
			$annoticket = substr($thread['date_add'],2,2);
				if($thread['id_contact'] == 2) { $siglat = 'A'; } else if($thread['id_contact'] == 4) { $siglat = 'T'; } else if($thread['id_contact'] == 3) { $siglat = 'V'; } else if($thread['id_contact'] == 6) { $siglat = 'R'; } else if($thread['id_contact'] == 8) { $siglat = 'D'; } else if($thread['id_contact'] == 9) { $siglat = 'S'; }
				$idticketunivoco = $siglat.$annoticket.$thread['id_customer_thread'];
				
					switch($thread['status']) {
						case 'open': $status_thread = "<img style='display:block; margin-top:5px; float:left' src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; 
						break;
						case 'closed': $status_thread = "<img style='display:block; margin-top:5px; float:left' src='../img/admin/status_green.gif' alt='Chiuso' title='Chiuso' />"; 
						break;
						case 'pending1': $status_thread = "<img style='display:block; margin-top:5px; float:left' src='../img/admin/status_orange.gif' alt='In lavorazione' title='In lavorazione' />"; 
						break;
						default: $status_thread = "<img style='display:block; margin-top:5px; float:left' src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; break;
					}
					echo '<tr id="'.$thread['id_customer_thread'].'" class="actions_ticket_tr" rel="'.$thread['id_contact'].'">';
						echo '<td>'.$tiporichiesta.'</td>
						<td>'.$idticketunivoco.'</td>
						<td style="vertical-align:center">'.$status_thread.'
						&nbsp;&nbsp;&nbsp;
						<form style="margin-left:5px; display:block;float:left" name="cambiastato_'.$thread['id_customer_thread'].'" id="cambiastato_'.$thread['id_customer_thread'].'" method="post" />
						<input type="hidden" name="id_customer" value="'.$thread['id_customer'].'" />
						<input type="hidden" name="id_customer_thread" value="'.$thread['id_customer_thread'].'" />
						<input type="hidden" name="back" value="1" />
						<select name="cambiastatus_listing" onchange="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { document.forms[\'cambiastato_'.$thread['id_customer_thread'].'\'].submit(); } else { this.value=\''.$thread['status'].'\'}">
						<option style="background-image:url(../img/admin/status_green.gif); background-repeat:no-repeat;
						background-position:bottom left;
						padding-left:30px;"  value="closed" '.($thread['status'] == 'closed' ? 'selected="selected"' : '').'>C</option>
						<option style="background-image:url(../img/admin/status_orange.gif); background-repeat:no-repeat;
						background-position:bottom left;
						padding-left:30px;"  value="pending1" '.($thread['status'] == 'pending1' ? 'selected="selected"' : '').'>L</option>
						<option style="background-image:url(../img/admin/status_red.gif); background-repeat:no-repeat;
						background-position:bottom left;
						padding-left:30px;"  value="open" '.($thread['status'] == 'open' ? 'selected="selected"' : '').'>A</option>
						</select>
						</form>
	
						</td><td><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewticket&id_customer_thread='.$thread['id_customer_thread'].'&token='.$this->token.'&tab-container-1=6">';
						
						
							echo '
							<form style="margin-left:5px; display:block;float:left" name="cambiaincaricoa_'.$thread['id_customer_thread'].'" id="cambiaincaricoa_'.$thread['id_customer_thread'].'" action="" method="post" />
							<a id="'.$thread['id_customer_thread'].'"> </a>
							<input type="hidden" name="id_customer" value="'.$thread['id_customer'].'" />
							<input type="hidden" name="id_ticket_univoco" value="'.Customer::trovaSigla($thread['id_customer_thread'],'ticket').'" />
							<input type="hidden" name="back" value="1" />
							<input type="hidden" name="id_thread" value="'.$thread['id_customer_thread'].'" />
							<input type="hidden" name="precedente_incarico" value="'.$thread['id_employee'].'" />
							<input type="hidden" name="tipo" value="ticket" />
							<select style="width:100px" name="cambiaincarico" onchange="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { document.forms[\'cambiaincaricoa_'.$thread['id_customer_thread'].'\'].submit(); } else { this.value=\''.$thread['id_employee'].'\'}">
							<option value="0" '.($thread['id_employee'] == '0' ? 'selected="selected"' : '').'>--Nessuno--</option>
							<option value="1" '.($thread['id_employee'] == '1' ? 'selected="selected"' : '').'>Ezio</option>
							<option value="2" '.($thread['id_employee'] == '2' ? 'selected="selected"' : '').'>Barbara</option>
							<option value="5" '.($thread['id_employee'] == '5' ? 'selected="selected"' : '').'>Paolo</option>
							<option value="4" '.($thread['id_employee'] == '4' ? 'selected="selected"' : '').'>Massimo</option>
							<option value="3" '.($thread['id_employee'] == '3' ? 'selected="selected"' : '').'>Riccardo</option>
							<option value="6" '.($thread['id_employee'] == '6' ? 'selected="selected"' : '').'>Federico</option>
							<option value="7" '.($thread['id_employee'] == '7' ? 'selected="selected"' : '').'>Matteo</option>
							</select>
							</form>'; 
						
						
						
						
						
						echo '</a></td>';
						
						
						$ultimomessthr = Db::getInstance()->getValue("SELECT message FROM customer_message WHERE id_customer_thread = ".$thread['id_customer_thread']." ORDER BY id_customer_message DESC");
							$nummessthr = Db::getInstance()->getValue("SELECT COUNT(id_customer_message) FROM customer_message WHERE id_customer_thread = ".$thread['id_customer_thread']."");
							$dataultimomessthr = Db::getInstance()->getValue("SELECT date_add FROM customer_message WHERE id_customer_thread = ".$thread['id_customer_thread']." ORDER BY id_customer_message DESC");
						
						echo '</td><td style="text-align:center"><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewticket&id_customer_thread='.$thread['id_customer_thread'].'&token='.$this->token.'&tab-container-1=6">'.$nummessthr.'</a></td>';
							echo '<td><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewticket&id_customer_thread='.$thread['id_customer_thread'].'&token='.$this->token.'&tab-container-1=6">'.substr(strip_tags(html_entity_decode($ultimomessthr, ENT_NOQUOTES, 'UTF-8')), 0, 75).'...</a></td>';
							echo '<td><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewticket&id_customer_thread='.$thread['id_customer_thread'].'&token='.$this->token.'&tab-container-1=6">'.Tools::displayDate($thread['date_add'], (int)($cookie->id_lang), false).'</a></td>';
						
							echo '<td><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewticket&id_customer_thread='.$thread['id_customer_thread'].'&token='.$this->token.'&tab-container-1=6">'.Tools::displayDate($dataultimomessthr, (int)($cookie->id_lang), false).'</a></td>';
							
							echo '<td><a href="index.php?tab=AdminCustomerThreads&id_customer_thread='.$thread['id_customer_thread'].'&del&typedel='.$thread['id_contact'].'&token='.Tools::getAdminToken('AdminCustomerThreads'.(int)Tab::getIdFromClassName('AdminCustomerThreads').(int)$cookie->id_employee).'&backtocustomers='.$customer->id.'&tab-container-1=6"><img src="../img/admin/delete.gif" alt="Cancella" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" /></a></td>';
							
							
						
							
					echo '</tr>';
				}
				
				echo '</table>';
			}
			else
				echo $customer->firstname.' '.$customer->lastname.' '.$this->l('has never contacted you.');

			
			
			$id_orders = array();
			$id_products = array();
			
			$query_orders_mx = mysql_query("SELECT id_order FROM orders WHERE id_customer = ".$customer->id."");
			while($rowqordmx = mysql_fetch_array($query_orders_mx, MYSQL_ASSOC)) {
				$id_orders[] = $rowqordmx['id_order'];
			}

			$query_products_mx = mysql_query("SELECT DISTINCT product_id FROM order_detail JOIN orders ON orders.id_order = order_detail.id_order WHERE id_customer = ".$customer->id."");
			while($rowqprodmx = mysql_fetch_array($query_products_mx, MYSQL_ASSOC)) {
				$id_products[] = $rowqprodmx['product_id'];
			}	
		
		
		
			//echo '<br /><h2>Apri un nuovo ticket</h2>';
			

			
		
		}
		
		echo '</div>';
		}
		else { }
		
		if (Tools::isSubmit('submitMessage')) {
			
			
			if(Tools::getValue('isMail') != 1) {
			
				if(Tools::getValue('message') == '') {
				
					Db::getInstance()->executeS("UPDATE customer_thread SET status = '".Tools::getValue('status')."', id_employee = '".Tools::getValue('in_carico_a')."' WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
					
					$this->assegna_incarico(Tools::getValue('in_carico_a'), Tools::getValue('id_customer_thread'), 'ticket');
					
					Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&error=1tk&token='.$this->token.'&tab-container-1=6');
								
				
				}
			}
			
			else {
				if(Tools::getValue('messaggio') == '') {
				
					Db::getInstance()->executeS("UPDATE customer_thread SET status = '".Tools::getValue('statusm')."', id_employee = '".Tools::getValue('in_carico_a_m')."' WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
					
					$this->assegna_incarico(Tools::getValue('in_carico_a_m'), Tools::getValue('id_customer_thread'), 'messaggio');

					Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&error=1ms&token='.$this->token.'&tab-container-1=7');
								
				
				}
			
			}
	
				if(Tools::getValue('isAnswer') == 1) {
					
						$primaqueryct = ("SELECT id_customer_thread, token FROM "._DB_PREFIX_."customer_thread WHERE id_customer = $_POST[id_customer] AND id_customer_thread = $_POST[id_customer_thread]");
		
						$resprimaqueryct = mysql_query($primaqueryct);
						$primarowct = mysql_fetch_array($resprimaqueryct, MYSQL_ASSOC);
						$id_customer_thread = $primarowct['id_customer_thread'];
						echo $id_customer_thread;
						$newtoken = $primarowct['token'];
						$subj = Mail::l('An answer to your message is available', (int)$ct->id_lang);
						if(Tools::getValue('isMail') != 1) {
							$queryinserimentomsg = ("UPDATE "._DB_PREFIX_."customer_thread SET id_employee = '".Tools::getValue('in_carico_a')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = ".$id_customer_thread."");
							mysql_query($queryinserimentomsg) or die(mysql_error());
							
						
						}
						
						else {
						
							$queryinserimentomsg = ("UPDATE "._DB_PREFIX_."customer_thread SET status = '".Tools::getValue('statusm')."', id_employee = '".Tools::getValue('in_carico_a_m')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = ".$id_customer_thread."");
							mysql_query($queryinserimentomsg) or die(mysql_error());
						}
					}
					
					else {
						$queryct = mysql_query("SELECT id_customer_thread FROM "._DB_PREFIX_."customer_thread ORDER BY id_customer_thread DESC LIMIT 1");
						$rowct = mysql_fetch_array($queryct, MYSQL_ASSOC);
						$id_customer_thread = $rowct['id_customer_thread']+1;
						$newtoken = Tools::passwdGen(12);
						$subj = "Messaggio da parte di Ezdirect";
						
						$cstmr = Db::getInstance()->getRow("SELECT company, firstname, lastname, is_company FROM customer WHERE id_customer = ".(int)($customer->id)."");
						
						switch(Tools::getValue('id_contact')) {
						case 2: $titolomessaggio = "Ticket per amministrazione - contabilita"; break;
						case 3: $titolomessaggio = "Ticket rivenditori"; break;
						case 4: $titolomessaggio = "Ticket per assistenza"; break;
						case 5: $titolomessaggio = "Ticket servizio clienti e ufficio vendite"; break;
						case 6: $titolomessaggio = "Ticket ti richiamiamo noi"; break;
						case 7: $titolomessaggio = "Messaggio semplice"; break;
						case 8: $titolomessaggio = "Ticket per amministrazione - ordini"; break;
						default: $titolomessaggio = "Messaggio da modulo di contatto"; break;
						
						}
						
						if($cstmr['is_company'] == 1) { $cstmr_r = $cstmr['company']; } else { $cstmr_r = $cstmr['firstname']." ".$cstmr['lastname']; }
						
						$subject = "*** RICHIESTA TICKET da ".$cstmr_r." - Tipo: ".$titolomessaggio." ***";
						
						if(Tools::getValue('isMail') == 1) {
						
						$subject = "*** MESSAGGIO da ".$cstmr_r." ***";
						
							$queryinserimentomsg = ("INSERT INTO "._DB_PREFIX_."customer_thread (id_customer_thread, id_lang, id_contact, id_customer, id_employee, subject, id_order, id_product, status, email, token, date_add, date_upd, note) VALUES ('".$id_customer_thread."', '".$cookie->id_lang."',  '7', '".Tools::getValue('id_customer')."', '".$cookie->id_employee."', '".addslashes($subject)."', '$_POST[id_order]', '$_POST[id_product]', 'open', '$_POST[customer_email_msg]', '".$newtoken."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."', '".addslashes(Tools::getValue('note'))."')");
						}
						else {
						
							$queryinserimentomsg = ("INSERT INTO "._DB_PREFIX_."customer_thread (id_customer_thread, id_lang, id_contact, id_customer, id_employee, subject, id_order, id_product, status, email, token, date_add, date_upd, note) VALUES ('".$id_customer_thread."', '".$cookie->id_lang."',  '".Tools::getValue('id_contact')."', '".Tools::getValue('id_customer')."', '".Tools::getValue('in_carico_a')."', '".addslashes($subject)."', '$_POST[id_order]', '$_POST[id_product]', 'open', '$_POST[customer_email_msg]', '".$newtoken."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."', '".addslashes(Tools::getValue('note'))."')");
						}
						echo $queryinserimentomsg;
						
						mysql_query($queryinserimentomsg) or die(mysql_error());
					}
					

					$ct = new CustomerThread($id_customer_thread);
					$cm = new CustomerMessage();
					$getstatus = Tools::getValue('status');
					if(!empty($getstatus)) {
					
					Db::getInstance()->ExecuteS("UPDATE customer_thread SET status = '".Tools::getValue('status')."' WHERE id_customer_thread = ".$id_customer_thread."");
					
					//$ct->status = Tools::getValue('status');
					}
					if(Tools::getValue('isMail') == 1) {
						$cm->id_employee = (int)$cookie->id_employee;
					}
					else {
						if(Tools::getValue('isAnswer') == 1) {
						
						$cm->id_employee = (int)$cookie->id_employee;
						}
						
						else {
						$cm->id_employee = 0;
						}
					}
					$cm->id_customer_thread = $ct->id;
					if(Tools::getValue('isMail') == 1) {
						$cm->message = Tools::htmlentitiesutf8(Tools::getValue('messaggio'));
					}
					else {
						$cm->message = Tools::htmlentitiesutf8(Tools::getValue('message'));
					}
					$cm->ip_address = ip2long($_SERVER['REMOTE_ADDR']);
		
					
				
					$files=$_FILES["joinFile"];

					$files=array();
					$fdata=$_FILES['joinFile'];
					if(is_array($fdata['name'])){
						for($i=0;$i<count($fdata['name']);++$i){
							$files[]=array(
								'name'    =>$fdata['name'][$i],
								'tmp_name'=>$fdata['tmp_name'][$i],
								'type' => $fdata['type'][$i],
								'size' => $fdata['size'][$i],
								'error' => $fdata['error'][$i],
							 
							);
						}
					}
					else $files[]=$fdata;
										
					$attachments = array();
					
					
					foreach($files as $file) {
					
						if (isset($files) AND !empty($file['name']) AND $file['error'] != 0)
							$this->_errors[] = Tools::displayError('An error occurred with the file upload.');
							
							if (!empty($file['name']))
								{
									$extension = array('.txt', '.rtf', '.doc', '.docx', '.pdf', '.zip', '.png', '.jpeg', '.gif', '.jpg');
									$filename = md5(uniqid().substr($file['name'], -5));
									$fileAttachment['content'] = file_get_contents($file['tmp_name']);
									$fileAttachment['name'] = $file['name'];
									$fileAttachment['mime'] = $file['type'];
								}
								
								if (isset($filename) AND rename($file['tmp_name'], _PS_MODULE_DIR_.'../upload/'.$filename)) {
									$filename = $filename.":::".$file['name'];
									$file_name .= $filename.";";
							}
							
							$attachments[] = $fileAttachment;
							
					}
					$cm->file_name = $file_name;	
					
					
					if ($cm->add())
					{
					
						
						
						$idmessaggio_ticket = mysql_insert_id();
						
						Db::getInstance()->ExecuteS("UPDATE customer_message SET email = '".Tools::getValue('customer_email_msg')."' WHERE id_customer_message = ".$idmessaggio_ticket."");
				
						$tickettipo = $ct->id_contact;

						if(Tools::getValue('isMail') == 1) {
							$sigla = "M";
						}
						else {
							if ($tickettipo == 4){
								$sigla = "T";
							}
							else if ($tickettipo == 2){
								$sigla = "A";
							} 
							else if ($tickettipo == 3){
								$sigla = "V";
							} 
							else if ($tickettipo == 6){
								$sigla = "R";
							}
							else if ($tickettipo == 8){
								$sigla = "D";
							} 
							else if ($tickettipo == 9){
								$sigla = "S";
							} 
						}
						
						$anno = $ct->date_add;
						
						$anno = substr($anno,2,2);
						
						$id_thread_ticket = $ct->id;
						
						$id_ticket = $sigla.$anno.$id_thread_ticket;
						$numerounivoco_ticket = $sigla.$anno.$id_thread_ticket.$idmessaggio_ticket;
						
						$primo_ticket = Db::getInstance()->getValue("SELECT id_customer_message FROM customer_message WHERE id_customer_thread = $id_thread_ticket ORDER BY id_customer_message ASC");
						
						if(!$primo_ticket) {
									
							$primo_ticket = $idmessaggio_ticket;
								
						}
								
						else {
								
						}
						
						$cfpicr = Db::getInstance()->getValue("SELECT id_customer FROM customer WHERE id_customer = $ct->id_customer");
						$addretc = Db::getInstance()->getValue("SELECT id_address FROM address WHERE fatturazione = 1 AND active = 1 AND deleted = 0 AND id_customer = $ct->id_customer");
						
						$in_carico_a =  Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".Tools::getValue('in_carico_a')."");
						
						$clixcsv = new Customer($cfpicr);
						$addxcsv = new Address($addretc);
						
						$provinciaxcsv = Db::getInstance()->getRow("SELECT iso_code FROM state WHERE id_state = $addxcsv->id_state");
						
						$primo_ticket = $sigla.$anno.$primo_ticket;
						$messaggioclientepercsv = Tools::getValue('message');
						$messaggioclientepercsv = strip_tags($messaggioclientepercsv);
						$messaggioclientepercsv = str_replace(";", ",", $messaggioclientepercsv);
						
						$messaggioclientepercsv = str_replace(chr(10), " ", $messaggioclientepercsv); //remove carriage returns
						$messaggioclientepercsv = str_replace(chr(13), " ", $messaggioclientepercsv); //remove carriage returns	
						
						$messaggioclientepercsv2 = explode("Cordiali saluti / Best regards", $messaggioclientepercsv);
						
						$messaggioclientepercsv = $messaggioclientepercsv2[0];
						
						$employeeincaricato = new Employee($cookie->id_employee);
						$employeeincaricato2 = new Employee(Tools::getValue('in_carico_a'));
						
						if(Tools::getValue('isMail') == 1) {
							$ref = rand(0, 99999); $ref2 = rand(0, 99999);
							
								$file_csv=fopen("../cms/quotazioni/messaggi.csv","a+");
								if(Tools::getValue('isAnswer') == 1) {
									$riga_richiesta = "".date("d/m/Y H:i:s")." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;;;;;;;".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;Accettata;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;R;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								else {
									$riga_richiesta = "".date("d/m/Y H:i:s")." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$clixcsv->firstname.";".$clixcsv->lastname.";".$addxcsv->address1.";".$addxcsv->postcode.";".$addxcsv->city.";".$provincia.";".$addxcsv->phone.";".$addxcsv->fax.";".$clixcsv->email.";".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;Accettata;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;D;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								@fwrite($file_csv,$riga_richiesta);
								@fclose("../cms/quotazioni/messaggi.csv","a+");	
								
						} else 
						{
							if ($tickettipo == 4){
								$ref = rand(0, 99999); $ref2 = rand(0, 99999);
						
								$file_csv=fopen("../cms/assistenza_no_contratto_csv/csv-assistenza-output.csv","a+");
								
								if(Tools::getValue('isAnswer') == 1) {
									$riga_richiesta = "".date('H:i:s d F Y')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;;;;;;;;;;;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;R;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								else {
									$riga_richiesta = "".date('H:i:s d F Y')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;".$clixcsv->firstname.";".$clixcsv->lastname.";;".$clixcsv->vat_number.";".$clixcsv->tax_code.";".$addxcsv->address1.";".$addxcsv->city.";".$addxcsv->postcode.";".$provincia.";".$clixcsv->email.";".$addxcsv->phone.";".$addxcsv->fax.";".$addxcsv->phone_mobile.";;;;;;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;D;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								
								
								@fwrite($file_csv,$riga_richiesta);
								@fclose("../cms/assistenza_no_contratto_csv/csv-assistenza-output.csv");	
							}
							else if ($tickettipo == 2){
								$ref = rand(0, 99999); $ref2 = rand(0, 99999);
							
								$file_csv=fopen("../cms/amministrativa_ordini_csv/csv-EZamm-contabilita-output.csv","a+");
								if(Tools::getValue('isAnswer') == 1) {
									$riga_richiesta = "".date('H:i:s d L a')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;;;;;;;".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;Accettata;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;R;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								else {
									$riga_richiesta = "".date('H:i:s d L a')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$clixcsv->firstname.";".$clixcsv->lastname.";".$addxcsv->address1.";".$addxcsv->postcode.";".$addxcsv->city.";".$provincia.";".$addxcsv->phone.";".$addxcsv->fax.";".$clixcsv->email.";".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;Accettata;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;D;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								@fwrite($file_csv,$riga_richiesta);
								@fclose("../cms/amministrativa_ordini_csv/csv-EZamm-contabilita-output.csv");	
							} 
							else if ($tickettipo == 8){
								$ref = rand(0, 99999); $ref2 = rand(0, 99999);
							
								$file_csv=fopen("../cms/amministrativa_ordini_csv/csv-EZamm-ordini-output.csv","a+");
								if(Tools::getValue('isAnswer') == 1) {
									$riga_richiesta = "".date('H:i:s d L a')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;;;;;;;".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;Accettata;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;R;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								else {
									$riga_richiesta = "".date('H:i:s d L a')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$clixcsv->firstname.";".$clixcsv->lastname.";".$addxcsv->address1.";".$addxcsv->postcode.";".$addxcsv->city.";".$provincia.";".$addxcsv->phone.";".$addxcsv->fax.";".$clixcsv->email.";".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;Accettata;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;D;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								@fwrite($file_csv,$riga_richiesta);
								@fclose("../cms/amministrativa_ordini_csv/csv-EZamm-ordini-output.csv");	
							} 
							else if ($tickettipo == 3){
								$ref = rand(0, 99999); $ref2 = rand(0, 99999);
		
								$data_quotazione = date("d/m/Y H:i:s");

								$file_csv=fopen("../cms/quotazioni/rivenditori.csv","a+");
								if(Tools::getValue('isAnswer') == 1) {
									$riga_richiesta = ";".$data_quotazione.";".date('Ymd')."-".$ref."-".$ref2.";;".$clixcsv->vat_number.";".$clixcsv->tax_code.";".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;;0;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;R;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								else {
									$riga_richiesta = ";".$data_quotazione.";".date('Ymd')."-".$ref."-".$ref2.";;".$clixcsv->vat_number.";".$clixcsv->tax_code.";".$clixcsv->firstname.";".$clixcsv->lastname.";;;0;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;D;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								@fwrite($file_csv,$riga_richiesta);
								@fclose("../cms/quotazioni/rivenditori.csv");	
							} 
							else if ($tickettipo == 6){
								$ref = rand(0, 99999); $ref2 = rand(0, 99999);
						
							
						
								$file_csv=fopen("../cms/callmeback/csv-EZcall-output.csv","a+");
							
								if(Tools::getValue('isAnswer') == 1) {
									$riga_richiesta = "".date('H:i:s d F Y')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;;;;;;;;;".$cfpicr['vat_number'].";".$cfpicr['tax_code'].";".$messaggioclientepercsv.";;Accettata;$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;R;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								else {
								}
								@fwrite($file_csv,$riga_richiesta);
								@fclose("../cms/callmeback/csv-EZcall-output.csv");	
							}
						
						}
						
						if(Tools::getValue('isMail') == 1) {
						
							if(Tools::getValue('isAnswer') == 1) {
								$params = array(
								'{reply}' => (Tools::getIsset('mail_solo_testo') ? strip_tags($gentilecliente.Tools::getValue('messaggio')) : $gentilecliente.Tools::getValue('messaggio')),
'{msg}' => (Tools::getIsset('mail_solo_testo') ? strip_tags($gentilecliente.Tools::getValue('messaggio')) : $gentilecliente.Tools::getValue('messaggio')),
								'{firma}' => $firma,
								'{id_richiesta}' => "Id richiesta: <strong>".$id_ticket."</strong>",
								'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token);
							}
							
							else {
							
									$params = array(
							'{reply}' => (Tools::getIsset('mail_solo_testo') ? strip_tags($gentilecliente.Tools::getValue('messaggio')) : $gentilecliente.Tools::getValue('messaggio')),
'{msg}' => (Tools::getIsset('mail_solo_testo') ? strip_tags($gentilecliente.Tools::getValue('messaggio')) : $gentilecliente.Tools::getValue('messaggio')),
							'{firma}' => $firma,
							'{id_richiesta}' => "Id richiesta: <strong>".$id_ticket."</strong>",
							'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token);
							
							}
						}
						
						else {
						
							if(Tools::getValue('isAnswer') == 1) {
						
								$params = array(
								'{reply}' => $gentilecliente.(Tools::getValue('message')),
								'{firma}' => $firma,
								'{id_richiesta}' => "Id ticket: <strong>".$id_ticket."</strong>",
								'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token);
						
							}
							
							else {
							
								$params = array(
								'{reply}' => $gentilecliente."&egrave; stato aperto un ticket dal nostro operatore ".$employeeincaricato->firstname." ".$employeeincaricato->lastname.". Il testo del ticket:<br /><br />".(Tools::getValue('message')),
								'{firma}' => $firma,
								'{id_richiesta}' => "Id ticket: <strong>".$id_ticket."</strong>",
								'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token);
								$tokenimp = Tools::getAdminToken("AdminCustomers"."2"."1");
								$linkimp = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewticket&id_customer_thread=".$ct->id."&token=".$tokenimp.'&tab-container-1=6';	
								
								$params2 = array(
								'{reply}' => "<strong>".$employeeincaricato->firstname." ".$employeeincaricato->lastname."</strong> ha aperto un ticket per conto di un cliente.<br /><br />
								Il ticket &egrave; in carico a: <strong>".$in_carico_a."</strong><br /><br />
								L'id del ticket &egrave;: <strong>".$id_ticket."</strong><br /><br />
								<a href='".$linkimp."'>Per rispondere clicca qui</a>. Di seguito il testo del ticket:<br /><br />".(Tools::getValue('message'))."
								");
								
								$mailcontatto = Db::getInstance()->getValue('SELECT email FROM contact WHERE id_contact = '.$ct->id_contact.'');
								
								if($is_supplier == 1) {
							
									Mail::Send(5, 'risposta_fornitore', Mail::l('Aperto ticket su Ezdirect', 5), $params2, $mailcontatto, NULL, 'info@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true); 
								
								
								}
								else {
							
									Mail::Send(5, 'action', Mail::l('Aperto ticket per conto del cliente', 5), 
									$params2, $mailcontatto, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
									_PS_MAIL_DIR_, true);
								}
								
							}	
							
							$this->assegna_incarico(Tools::getValue('in_carico_a'), $ct->id, 'ticket');
							
						
						}
						
						
						
						$in_carico_a =  Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".Tools::getValue('in_carico_a')."");
						if(empty($in_carico_a)) { $in_carico_a = "Nessuno"; }
				
						if(Tools::getValue('isMail') == 1) {
				
						
							$mails_message = explode(";", Tools::getValue('customer_email_msg'));
						
							foreach ($mails_message as $mail) {
								$mail = trim($mail);
								Customer::findEmailPersona($mail, Tools::getValue('id_customer'));

if($is_supplier == 1) {
								if (Mail::Send(5, 'risposta_fornitore', Mail::l('Messaggio da Ezdirect', 5), 
									$params, $mail, NULL, 'info@ezdirect.it', 'Ezdirect', $attachments, NULL, 
									_PS_MAIL_DIR_, true))
								{
									
									//$ct->status = 'closed';
									//$ct->update();
									/*$mail_scrivente = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = ".$cookie->id_employee."");
									
									Mail::Send(5, 'msg_base', Mail::l('Copia messaggio inviato al cliente', 5), 
									$params, $mail_scrivente, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
									_PS_MAIL_DIR_, true);*/
									
									$log_mail=fopen("../import/log-mail.txt","a+");
									$riga_log = "MSG | Mail inviata a ".$mail." in data ".date("Y-m-d H:i:s")."\n";
									@fwrite($log_mail,$riga_log);
								}
}
else {

								if (Mail::Send(5, 'simple_msg', Mail::l('Messaggio da Ezdirect', 5), 
									$params, $mail, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
									_PS_MAIL_DIR_, true))
								{
									
									//$ct->status = 'closed';
									//$ct->update();
									/*$mail_scrivente = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = ".$cookie->id_employee."");
									
									Mail::Send(5, 'simple_msg', Mail::l('Copia messaggio inviato al cliente', 5), 
									$params, $mail_scrivente, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
									_PS_MAIL_DIR_, true);*/
									
									$log_mail=fopen("../import/log-mail.txt","a+");
									$riga_log = "MSG | Mail inviata a ".$mail." in data ".date("Y-m-d H:i:s")."\n";
									@fwrite($log_mail,$riga_log);
								}
}
							}
							
							
							$cc = "";
							
								
								
							foreach($_POST['conoscenza'] as $conoscenza) {

								$tokenimp = Tools::getAdminToken("AdminCustomers"."2".$conoscenza);
								$linkimp = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewmessage&id_mex=".$ct->id."&token=".$tokenimp.'&tab-container-1=7';	
								
								$params4 = array(
								'{reply}' => "<strong>".$employeemess->firstname."</strong> ha inviato una comunicazione al cliente e ti ha messo in copia conoscenza. <br /><br />
								Id messaggio: <strong>".$id_ticket."</strong><br /><br />
								Il messaggio &egrave; in carico a: <strong>".$in_carico_a."</strong><br /><br />							
								<a href='".$linkimp."'>Per aprire il messaggio fai clic qua</a>.",
							
								'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token);
								
								$cc .= $conoscenza.";";
						
								$mailconoscenza = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = $conoscenza");
								
								Mail::Send(5, 'senzagrafica', Mail::l('Messaggio cliente in copia conoscenza', 5), 
								$params4, $mailconoscenza, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
								_PS_MAIL_DIR_, true);
							
							
							}
							
							if(!empty($_POST['conoscenza'])) {
							
								Db::getInstance()->executeS("INSERT INTO ticket_cc (msg, type, cc) VALUES (".$idmessaggio_ticket.", 't', '".$cc."')"); 
							
							
							}
							
							
							
						}
						
						else {
							$mails_ticket = explode(";", Tools::getValue('customer_email_msg'));
							
							foreach ($mails_ticket as $mail) {
								$mail = trim($mail);
								Customer::findEmailPersona($mail, Tools::getValue('id_customer'));
								if($is_supplier == 1) {
									if (Mail::Send(5, 'risposta_fornitore', Mail::l('Messaggio da Ezdirect', 5), 
										$params, $mail, NULL, 'info@ezdirect.it', 'Ezdirect', $attachments, NULL, 
										_PS_MAIL_DIR_, true))
									{
										
										//$ct->status = 'closed';
										//$ct->update();
										/*$mail_scrivente = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = ".$cookie->id_employee."");
										
										Mail::Send(5, 'msg_base', Mail::l('Copia messaggio inviato al cliente', 5), 
										$params, $mail_scrivente, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
										_PS_MAIL_DIR_, true);*/
										
										$log_mail=fopen("../import/log-mail.txt","a+");
										$riga_log = "MSG | Mail inviata a ".$mail." in data ".date("Y-m-d H:i:s")."\n";
										@fwrite($log_mail,$riga_log);
									}
								}	
								else {
								
									if (Mail::Send((int)$ct->id_lang, 'reply_msg', Mail::l('Messaggio da Ezdirect', (int)$ct->id_lang), 
										$params, $mail, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
										_PS_MAIL_DIR_, true))
									{
										
										//$ct->status = 'closed';
										//$ct->update();
										/*
										$mail_scrivente = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = ".$cookie->id_employee."");
									
										Mail::Send(5, 'simple_msg', Mail::l('Copia ticket inviato al cliente', 5), 
										$params, $mail_scrivente, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
										_PS_MAIL_DIR_, true);
										*/
										
										$log_mail=fopen("../import/log-mail.txt","a+");
										$riga_log = "TKT | Mail inviata a ".$mail." in data ".date("Y-m-d H:i:s")."\n";
										@fwrite($log_mail,$riga_log);
									}
								}
							}
							
							
							$cc = "";
							
							foreach($_POST['conoscenza'] as $conoscenza) {
								
								$tokenimp = Tools::getAdminToken("AdminCustomers"."2".$conoscenza);
								$linkimp = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewticket&id_customer_thread=".$ct->id."&token=".$tokenimp.'&tab-container-1=6';	
								
								$params4 = array(
								'{reply}' => "<strong>".$employeemess->firstname."</strong> ha inviato una comunicazione di un ticket al cliente e ti ha messo in copia conoscenza. <br /><br />
								Id ticket: <strong>".$id_ticket."</strong><br /><br />
								Il ticket &egrave; in carico a: <strong>".$in_carico_a."</strong><br /><br />							
								<a href='".$linkimp."'>Per aprire il ticket fai clic qua</a>.",
							
								'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token);
								
								$cc .= $conoscenza.";";
								$mailconoscenza = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = $conoscenza");
								
								Mail::Send((int)$ct->id_lang, 'senzagrafica', Mail::l('Ticket utente in copia conoscenza', (int)$ct->id_lang), 
								$params4, $mailconoscenza, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
								_PS_MAIL_DIR_, true);
							
							
							}
							if(!empty($_POST['conoscenza'])) {
							
								Db::getInstance()->executeS("INSERT INTO ticket_cc (msg, type, cc) VALUES (".$idmessaggio_ticket.", 't', '".$cc."')"); 
							
							
							}
						}

						if(Tools::getValue('isMail') == 1) {
							Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&token='.$this->token.'&conf=4&tab-container-1=7');
						}
						else {
							Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&viewticket&id_customer_thread='.$ct->id.'&messagesent&token='.$this->token.'&conf=4&tab-container-1=6');
						}
					}
					else
						$this->_errors[] = Tools::displayError('An error occurred, your message was not sent. Please contact your system administrator.');
				
			}
			
			
				else {
				
					if (isset($_GET['messagesent'])) {
				
					echo 'Ticket aperto con successo!<br /> <a class="button" href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=6"><strong>Clicca qui per aprire un nuovo ticket</strong></a>';
					
					}
				
					else {

					
			}
		}
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 7) {
		
		
		// PREVENTIVI!!!!!!
		
		$preventivi = Db::getInstance()->ExecuteS("SELECT * FROM form_prevendita_thread WHERE id_customer = ".$customer->id." ORDER BY id_thread DESC");
		
		$messaggi = Db::getInstance()->ExecuteS("SELECT id_customer_thread AS id_thread, id_contact AS tipo_richiesta, id_customer, subject, id_employee, '' AS tax_code, '' AS vat_number, '' AS firstname, '' AS lastname, '' AS company, '' AS address1, '' AS postcode, '' AS state, '' AS country, phone, email, token, status, '' AS category, date_add, date_upd, note FROM customer_thread WHERE id_contact = 7 AND id_customer = ".$customer->id." ORDER BY id_customer_thread DESC");
		
		$azioni_cliente = array_merge($preventivi, $messaggi);
		
		$date_azioni = array();
		foreach ($azioni_cliente as $key => $row)
		{
			$date_azioni[$key] = $row['date_upd'];
		}
		array_multisort($date_azioni, SORT_DESC, $azioni_cliente);
		
		echo '<div class="tab1" id="preventivi-customer">';
		$impiegati_ezp = Db::getInstance()->ExecuteS("SELECT id_employee, firstname, lastname FROM employee");
		
			if(isset($_GET['id_thread'])) {
			
			$tipo_richiesta_p = Db::getInstance()->getValue("SELECT tipo_richiesta FROM form_prevendita_thread ft WHERE ft.id_thread = ".$_GET['id_thread']."");
			
					$preventivo = Db::getInstance()->getRow("SELECT * FROM form_prevendita_thread ft WHERE ft.id_thread = ".$_GET['id_thread']."");
			$incaricato = new Employee($preventivo['id_employee']);
			$dataultimo = Db::getInstance()->getValue("SELECT date_add FROM form_prevendita_message WHERE id_thread = ".$_GET['id_thread']." ORDER BY date_add DESC");
			
			$oggetto = Db::getInstance()->getValue("SELECT subject FROM form_prevendita_thread WHERE id_thread = ".$_GET['id_thread']."");
				
				if($oggetto == '') {
					$oggetto = Db::getInstance()->getValue("SELECT message FROM form_prevendita_message WHERE id_thread = ".$_GET['id_thread']." ORDER BY date_add ASC");
			
				} 
				else {
				
				}
			
					
					
				switch($preventivo['status']) {
						case 'open': $status_preventivo = "<img src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; break;
						case 'closed': $status_preventivo = "<img src='../img/admin/status_green.gif' alt='Chiuso' title='Chiuso' />"; break;
						case 'pending1': $status_preventivo = "<img src='../img/admin/status_orange.gif' alt='In lavorazione' title='In lavorazione' />"; break;
						default: $status_preventivo = "<img src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; break;
					}
			$annoprev = substr($preventivo['date_add'],2,2);
			if($preventivo['tipo_richiesta'] == 'preventivo') { $sigla = 'P'; } else if($preventivo['tipo_richiesta'] == 'tirichiamiamonoi') { $sigla = 'R'; }
			$idprevunivoco = $sigla.$annoprev.$preventivo['id_thread'];
			
			
			
		
		
			echo '<h2 style="display:block;float:left;margin-right:10px" >'.($tipo_richiesta_p == 'tirichiamiamonoi' ? 'Ti richiamiamo noi n.' : 'Preventivo n.').' '.$idprevunivoco.'</h2>';
			}
			
		echo '
		
		<a style="display:block;float:left;margin-right:10px" class="button" href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewmessage&aprinuovomessaggio&token='.$this->token.'&tab-container-1=7">Invia messaggio al cliente</a>
		
		&nbsp;&nbsp;&nbsp;<a class="button" style="display:block;float:left" href="http://www.ezdirect.it" target="_blank">Collegati al sito</a>
		<div class="clear:both"></div>
		<br /><br />';
		
		if(isset($_GET['error']) && $_GET['error'] == '1pv') {
			
			echo '<div style="width:100%; height:30px; border:1px solid red; color:red"> Il messaggio era vuoto e non &egrave; stato inviato, ma sono stati aggiornati i campi status e in carico a.</div>';
		}
		
		if(isset($_POST['cambiastatusp_listing'])) {

			if(Tools::getValue('tipo_richiesta') == 7) {
				Db::getInstance()->ExecuteS("UPDATE customer_thread SET status = '".Tools::getValue('cambiastatusp_listing')."' WHERE id_customer_thread = '".Tools::getValue('id_thread')."'");
				
			}
			else {
				
				Db::getInstance()->ExecuteS("UPDATE form_prevendita_thread SET status = '".Tools::getValue('cambiastatusp_listing')."' WHERE id_thread = '".Tools::getValue('id_thread')."'");
			}
			Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&token='.$this->token.'&conf=4&tab-container-1=7');
			
		}
		
		if(isset($_POST['cambiaincaricop'])) {

			
			Customer::cambiaIncaricoVeloce(Tools::getValue('tipo'), Tools::getValue('cambiaincaricop'), Tools::getValue('id_thread'), Tools::getValue('precedente_incarico'), Tools::getValue('id_ticket_univoco'), Tools::getValue('id_customer'));
	
			
			Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&token='.$this->token.'&conf=4&tab-container-1=7');
			
		}
		
		if(isset($_GET['id_mex'])) {		
			$mex = Db::getInstance()->getRow("SELECT * FROM customer_thread ct WHERE ct.id_customer_thread = ".$_GET['id_mex']."");
				
				$id_incmex = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$mex['id_customer_thread']."");
				
				$incmex = new Employee($id_incmex);
				
				
				$annomex = substr($mex['date_add'],2,2);
				$siglam = "M";
					$idmexunivoco = $siglam.$annomex.$mex['id_customer_thread'];
				
				
						switch($mex['status']) {
							case 'open': $status_mex = "<img src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; break;
							case 'closed': $status_mex = "<img src='../img/admin/status_green.gif' alt='Chiuso' title='Chiuso' />"; break;
							case 'pending1': $status_mex = "<img src='../img/admin/status_orange.gif' alt='In lavorazione' title='In lavorazione' />"; break;
							default: $status_mex = "<img src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; break;
						}				
		
			echo '<h2 style="display:block;float:left;margin-right:10px" >Messaggio n. '.$idmexunivoco.'</h2><div style="clear:both"></div>';
			
		}	
		
		
		if(isset($_GET['error']) && $_GET['error'] == '1ms') {
			
			echo '<div style="width:100%; height:30px; border:1px solid red; color:red">Il messaggio era vuoto e non &egrave; stato inviato, ma sono stati aggiornati i campi status e in carico a.</div>';
		}
		
		
		if(isset($_POST['submitMex'])) {

				
			Db::getInstance()->ExecuteS("UPDATE customer_thread SET subject = '".addslashes(Tools::getValue('mex_subject'))."', id_employee = '".addslashes(Tools::getValue('assegnaimpiegatom'))."', status = '".addslashes(Tools::getValue('cambiastatusm'))."', note = '".addslashes(Tools::getValue('mex_note'))."' WHERE id_customer_thread = '".Tools::getValue('id_mex')."'");
			
			$impiegato_attuale = Db::getInstance()->getValue("SELECT id_employee FROM customer_thread WHERE id_customer_thread = '".Tools::getValue('id_mex')."'");
			
			if($impiegato_attuale != Tools::getValue('assegnaimpiegatom')) {
									
				$mailimp = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".Tools::getValue('assegnaimpiegatom')."'");
								
				$tokenimp = Tools::getAdminToken("AdminCustomers"."2".Tools::getValue('assegnaimpiegatom'));
								
				$linkimp = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewmessage&id_mex=".Tools::getValue('id_mex')."&token=".$tokenimp.'&tab-container-1=7';

				$msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato un messaggio su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirlo</a>.";
								
				if($mex['id_employee'] != 0) {
									
					$mailimprev = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".$mex['id_employee']."'");
					$nomeimprev = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = '".Tools::getValue('assegnaimpiegatom')."'");
									
					$msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione di un messaggio su Ezdirect e l'ha passata a ".$nomeimprev.".";
									
					$params = array(
					'{link}' => $linkimp,
					'{firma}' => '',
					'{msg}' => $msgimprev);
									
					Mail::Send(5, 'msg_base', Mail::l('Gestione messaggio revocata', 5), 
						$params, $mailimprev, NULL, NULL, NULL, NULL, NULL, 
					_PS_MAIL_DIR_, true);
								
				}
								
				else {
								
				}
								
				$params = array(
				'{link}' => $linkimp,
				'{firma}' => '',
				'{msg}' => $msgimp);
								
				if($_POST['assegnaimpiegatom'] != 0) {
								
					Mail::Send(5, 'msg_base', Mail::l('Messaggio assegnato a te su Ezdirect', 5), 
						$params, $mailimp, NULL, NULL, NULL, NULL, NULL, 
						_PS_MAIL_DIR_, true);
												
				} else { }
			}

			else {
			}
			
			Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&viewmessage&id_mex='.Tools::getValue('id_mex').'&token='.$this->token.'&conf=4&tab-container-1=7');
		}
		
		//INVIO E VISUALIZZAZIONE MESSAGGIO
		if(isset($_GET['viewmessage'])) {
		
			if(!isset($_GET['aprinuovomessaggio'])) {
		
				if (isset($_GET['filename'])) {
						$filename = $_GET['filename'];
						
						if(strpos($filename, ":::")) {
			
							$parti = explode(":::", $filename);
							$nomecodificato = $parti[0];
							$nomevero = $parti[1];
							
						}
			
						else {
							$nomecodificato = $filename;
							$nomevero = $filename;
			
						}
						
						if (file_exists(_PS_UPLOAD_DIR_.$nomecodificato)) {
							self::openUploadedFile();
						}
						else { }
					} 
					else { }
		
			if(isset($_GET['id_mex'])) {	

				if($oggetto == '') {
					$oggetto = Db::getInstance()->getValue("SELECT message FROM customer_message WHERE id_customer_thread = ".$_GET['id_mex']." ORDER BY date_add ASC");
					
				} 
				else {
						
				}
				echo '<div id="tab-container-mex" style="margin-top:16px">
				
				
				
				<ul id="tab-container-mex-nav" style="list-style-type:none; margin-top:-5px; margin-left:-20px">
				<li><a href="#mex-1"><strong>Generale</strong></a></li>
				<li><a href="#mex-2"><strong>Cronologia</strong></a></li>
				</ul>';
		
				echo '
				
				<div class="yetii-mex" id="mex-1">
				<fieldset style="width:95%">';
				
				
				echo '
				<form id="modificamex" action="'.$currentIndex.'&id_customer='.$_GET['id_customer'].'&submitMex&viewcustomer&token='.$this->token.'&tab-container-1=7" method="post" autocomplete="off">';
				
				
				
				echo '<div class="anagrafica-cliente" style="width:800px; margin-bottom:10px">
				<strong>Oggetto</strong><br />
					
					<input type="hidden" value="'.Tools::getValue('id_mex').'" name="id_mex" />
					<textarea name="mex_subject" id="mex_subjectContent" style="width:690px;height:40px;display:block;float:left">'.strip_tags(htmlspecialchars_decode($oggetto)).'</textarea>
					<br />
		
				<br /><br />
				</div>

				<div class="clear">
				<div class="anagrafica-cliente">
				ID Messaggio<br />
				<span class="tab-span">'.$idmexunivoco.'</span><br />
				</div>
				<div class="anagrafica-cliente" style="width:250px">
				In carico a<br />';
					echo '
				
				<select style="width:250px" name="assegnaimpiegatom">';
				$impiegati_ez = Db::getInstance()->ExecuteS("SELECT id_employee, firstname, lastname FROM employee");
		
				echo '<option value="0"'.($mex['id_employee'] == 0 ? 'selected="selected"' : '').' >Da assegnare</option>';
				
				foreach ($impiegati_ez as $impez) {
				
					echo "<option value='".$impez['id_employee']."' ".($mex['id_employee'] == $impez['id_employee'] ? 'selected="selected"' : '')." >".$impez['firstname']."</option>";
				
				}

				echo '</select>
		
				</div>
				
				<div class="anagrafica-cliente">
				Data apertura<br />
				<span class="tab-span">'.Tools::displayDate($mex['date_add'], (int)($cookie->id_lang), true).'</span><br />
				</div>
				
					
					<div class="clear"></div>';
					
				switch($mex['status']) {
						case 'open': $status_mex = "<img style='display:block; margin-top:5px; float:left' src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; 
						break;
						case 'closed': $status_mex = "<img style='display:block; margin-top:5px; float:left' src='../img/admin/status_green.gif' alt='Chiuso' title='Chiuso' />"; 
						break;
						case 'pending1': $status_mex = "<img style='display:block; margin-top:5px; float:left' src='../img/admin/status_orange.gif' alt='In lavorazione' title='In lavorazione' />"; 
						break;
						default: $status_mex = "<img style='display:block; margin-top:5px; float:left' src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; break;
					}
					
					
				$ctrl_mex = Db::getInstance()->getValue("SELECT COUNT(id_customer_message) FROM customer_thread ct JOIN customer_message cm ON ct.id_customer_thread = cm.id_customer_thread WHERE cm.id_employee = 0 AND ct.id_customer_thread = ".$mex['id_customer_thread']."");	
					
				echo '<div class="anagrafica-cliente">';
				$dataultimo = Db::getInstance()->getValue("SELECT date_add FROM customer_message WHERE id_customer_thread = ".$_GET['id_mex']." ORDER BY date_add DESC");
				if($ctrl_mex >= 1) {
				echo 'Status<br />
				'.$status_mex.' <input type="hidden" name="id_customer" value="'.Tools::getValue('id_customer').'" />
				
				
				<select name="cambiastatusm">
				<option value="closed" '.($mex['status'] == 'closed' ? 'selected="selected"' : '').'>Chiuso</option>
				<option value="pending1" '.($mex['status'] == 'pending1' ? 'selected="selected"' : '').'>In lavorazione</option>
				<option value="open" '.($mex['status'] == 'open' ? 'selected="selected"' : '').'>Aperto</option>
				</select><br />'; } else { echo "<br />"; }
				echo '</div>
				
				<div class="anagrafica-cliente" style="width:250px">
				Tipo attivit&agrave;<br />
				<span class="tab-span">Messaggio</span><br />
				
				</div>
				
				<div class="anagrafica-cliente">
				Data ultima com.<br />
				<span class="tab-span">'.Tools::displayDate($dataultimo, (int)($cookie->id_lang), true).'</span><br />
				</div>
				
				<div class="clear"></div>';

				
				echo '<br />
				<strong>Nota interna visibile solo agli impiegati</strong><br />
							
								<textarea name="mex_note" id="mexnoteContent" style="width:80%;height:100px">'.Tools::htmlentitiesUTF8($mex['note']).'</textarea><br /><br />';
						
				
						echo '<input type="submit" value="'.$this->l('   Save   ').'" name="submitMex" class="button" />';
						
				echo '</form>
				</fieldset><br /><br /></div>';
		
		
			}
	
			echo '<div class="yetii-mex" id="mex-2">';
				
	
				$messaggixt = Db::getInstance()->ExecuteS("SELECT * FROM customer_message cm WHERE cm.id_customer_thread = ".$_GET['id_mex']."");
				
				foreach($messaggixt as $messaggioxt) {
				
					echo "<table style='width:100%; 
					border:1px solid #dfd5c3; 
					";
					if ($messaggioxt['id_employee'] == 0) {
						echo 'background-color: #ffffff;';
					} else {
			
						echo 'background-color: #ffecf2;';
					}
					$employeezxt = new Employee($messaggioxt['id_employee']);
					echo "margin-bottom:15px'>";
					echo '<tr>
					<td style="colspan:2">
						<table>
				
						<td style="font-size:14px; width:250px">Da: <strong>';if ($messaggioxt['id_employee'] == 0) { echo $customer->firstname." ".$customer->lastname; } else { echo $employeezxt->firstname." ".$employeezxt->lastname; } echo '<strong></td>
						
						<td style="width:170px"><strong>Data: </strong>'.$messaggioxt['date_add'].'</td>
						<td style="width:180px"><strong>Allegato: </strong>';
						if(!empty($messaggioxt['file_name'])) {
						
							$allegatim = explode(";",$messaggioxt['file_name']);
							$nallm = 1;
							foreach($allegatim as $allegatom) {
								if(strpos($allegatom, ":::")) {
					
									$parti = explode(":::", $allegatom);
									$nomeverom = $parti[1];
									
								}
					
								else {
									$nomeverom = $allegatom;
					
								}
								if($allegatom == "") { } else {
									if($nallm == 1) { } else { echo " - "; }
									echo '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewmessage&id_mex='.$mex['id_customer_thread'].'&token='.$this->token.'&tab-container-1=7&filename='.$allegatom.'"><span style="color:red">'.$nomeverom.'</span></a>';
									$nallm++;
								}
							}
						
						}
						
						else { echo 'Nessun allegato'; }
						
						
						echo '</td>';
						
						$cc = Db::getInstance()->getValue("SELECT cc FROM ticket_cc WHERE type = 't' AND msg = ".$messaggioxt['id_customer_message']."");
						if(!empty($cc)) {
						
							echo '<td>';
						
							$ccs = explode(";",$cc);
							$cc_str = "<strong>CC:</strong> ";
							
							foreach ($ccs as $conoscenza) {
							
								$imp = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".$conoscenza."");
								$cc_str .= $imp." ";
							}
							
							echo $cc_str;
							echo "</td>";
						
						}
						else {
						
							echo '<td><strong>CC:</strong> Nessuno</td>';
						
						}
						
						echo '</tr></table>
					</td>
					</tr>
					<tr>
					<td style="colspan:2">
						<table>
						<tr>
						<td style="width:100px"><strong>Messaggio</strong></td>
						<td>'.htmlspecialchars_decode($messaggioxt['message']).($messaggioxt['id_employee'] == 0 ? '' : '<form method="post">
						<input type="hidden" name="modifica_messaggio_messaggio" value="'.htmlentities($messaggioxt['message'], ENT_QUOTES | ENT_SUBSTITUTE, "UTF-8").'" />
						<input type="submit" name="modifica_messaggio_submit" value="Modifica" class="button" />
						</form>').'</td>
						</tr>
						</table>
					</td>';
					echo '</tr>';
						if($messaggioxt['email'] == '') { } else {
					
					echo '<tr><td style="colspan:2">
						<table>
						<tr>
						<td style="width:100px"><strong>Inviato a</strong></td>
						<td>'.htmlspecialchars_decode($messaggioxt['email']).'</td>
						</tr>
						</table>
					</td></tr>';
					
					}
					
					echo '</table>';
				}
			
			}
			
			else {
			
			}
			
			if(isset($_GET['aprinuovomessaggio'])) {
			
				echo '<h2>Invia messaggio al cliente</h2>';
			
			
			}
		
			else {
			
				echo '<h2 id="rispondi-cliente-messaggio">Rispondi al cliente</h2>';
				if(isset($_POST['modifica_messaggio_submit'])) {
				echo '<script type="text/javascript">
					 window.location = "#rispondi-cliente-messaggio";
				</script>';
				}
			}
		
					$iso = Language::getIsoById((int)($cookie->id_lang));
			$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
			$ad = dirname($_SERVER["PHP_SELF"]);
				echo '
			<script type="text/javascript">
			var iso = \''.$isoTinyMCE.'\' ;
			var pathCSS = \''._THEME_CSS_DIR_.'\' ;
			var ad = \''.$ad.'\' ;
			</script>
			
			
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce3/tiny_mce.js"></script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>
			<script type="text/javascript">
				$(document).ready(function () {
				
dataChanged = 0;     // global variable flags unsaved changes      

function bindForChange(){    
     $(\'input,checkbox,text,textarea,radio,select\').bind(\'change\',function(event) { dataChanged = 1});
	 $(\'#messaggio\').bind(\'input propertychange\', function() { dataChanged = 1 });
     $(\':reset,:submit\').bind(\'click\',function(event) { dataChanged = 0 });
}


function askConfirm(){  
    if (dataChanged){ 
        return "You have some unsaved changes.  Press OK to continue without saving." 
    }
}

window.onbeforeunload = askConfirm;
window.onload = bindForChange;
});
			</script>
			<script type="text/javascript">
					toggleVirtualProduct(getE(\'is_virtual_good\'));
					unitPriceWithTax(\'unit\');
			</script>';
			
					echo'	<form action="'.$currentIndex.'&id_customer='.$obj->id.'&viewcustomer&token='.$this->token.'&tab-container-1=7" method="post" class="std" enctype="multipart/form-data">
					<fieldset style="width:80%">
					<input type="hidden" name="isMail" value="1">
						<table>';
						
						$customer_phone = Db::getInstance()->getRow("SELECT	phone, phone_mobile FROM address WHERE id_customer = ".$customer->id." AND deleted = 0 AND fatturazione = 1 AND active = 1");
							
							if($customer_phone['phone'] == '') { $telefono_cliente = $customer_phone['phone_mobile']; } else { $telefono_cliente = $customer_phone['phone']; }
						
						if(isset($_GET['aprinuovomessaggio'])) {
			
							echo '<input type="hidden" name="nuovomessaggio" value="1" />
							

							';
				
			
						}
		
						else {
							echo '<input type="hidden" value="1" name="isAnswer" />';
						}	
						echo '	<tr>
								<td style="width:170px">
								
								<input type="hidden" name="id_customer" value="'.$customer->id.'" />
								</td>
							</tr>
							
							<tr><td>Email</td><td>';
							
							
							if($customer->is_company == 1) { echo '<div style="float:left">';
							
							$persone = Db::getInstance()->executeS("SELECT id_persona, firstname, lastname, email FROM persone WHERE id_customer = ".Tools::getValue('id_customer')." AND email != ''");
							
							echo '
							<select name="seleziona-persona" onchange="javascript:document.getElementById(\'customer_email_msg_m\').value = (this.value);">';
							echo "<option value=''>-- Seleziona persona --</option>";
							
							foreach ($persone as $persona) {
							
								echo "<option ".($persona['id_persona'] == $_GET['id_persona'] ? "selected='selected'" : "")."  value='".$persona['email']."'>".$persona['firstname']." ".$persona['lastname']."</option>";
							
							}
							
							echo "</select></div>";
							
							} else { }
							
							
							if(isset($_GET['id_persona'])) {
								$mail_persona = Db::getInstance()->getValue("SELECT email FROM persone WHERE id_persona = ".$_GET['id_persona']);
							}
							else {
								$mail_persona = $customer->email;
							}
							
							
							echo '<div style="float:left; '.($customer->is_company == 1? 'margin-left:20px' : '').'"><input type="text" size="40" name="customer_email_msg" id="customer_email_msg_m" value="'.(!$mex['email'] ? $mail_persona : $mex['email']).'" /><br />
							<span style="font-size:11px">In caso di più indirizzi mail, separare con punto e virgola (;)</span></div><div style="float:left; margin-left:20px"> Tel. cliente: <input type="text" readonly="readonly" value="'.$telefono_cliente.'" /></div></td></tr>
					
						
							<input type="hidden" value="'.$mex['id_customer_thread'].'" name="id_customer_thread">';
							
						
							
						echo ' 
							<tr><td>Stato messaggio</td>
							<td>
							<div style="float:left">
							<script type="text/javascript">
								function changeInCaricoAM()
								{';
								
								
								foreach ($impiegati_ez as $impez) {
									echo '
									
									firstVar = document.getElementById("in_carico_a_m").value;
									
									if (document.getElementById("in_carico_a_m").value == "'.$impez['id_employee'].'") {
									
									
									document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').disabled = true;
									document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').checked = false;
									
									} else { document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').disabled = false; }
								';
								}
							echo '
								}
								</script>
							<select name="statusm" id="statusm">
							<option value="open" '.($mex['status'] == 'open' ? 'selected="selected"' : '').'>Aperto   </option>
							<option value="pending1" '.($mex['status'] == 'pending1' ? 'selected="selected"' : '').'>In lavorazione   </option>
							<option value="closed" '.($mex['status'] == 'closed' ? 'selected="selected"' : '').'>Chiuso   </option>
							
							</select>
							</div><div style="float:left; margin-left:20px">
							In carico a';
					echo '
				
				<select style="width:250px" id="in_carico_a_m" name="in_carico_a_m" onChange="changeInCaricoAM()">';
				$impiegati_ez = Db::getInstance()->ExecuteS("SELECT id_employee, firstname, lastname FROM employee");
				foreach ($impiegati_ez as $impez) {
				
					if(isset($_GET['aprinuovomessaggio'])) {
						echo "<option value='".$impez['id_employee']."' ".($cookie->id_employee == $impez['id_employee'] ? 'selected="selected"' : '')." >".$impez['firstname']."</option>";
					}
					else {
						echo "<option value='".$impez['id_employee']."' ".($mex['id_employee'] == $impez['id_employee'] ? 'selected="selected"' : '')." >".$impez['firstname']."</option>";
					}
				}

				echo '</select><br /><br /></td>
							</tr><tr><td>Conoscenza</td>
							<td>
							';
							$impiegati_ez = Db::getInstance()->ExecuteS("SELECT id_employee, firstname, lastname FROM employee");
		
							foreach ($impiegati_ez as $impez) {
		
									echo "<input type='checkbox' id='conoscenza_".$impez['id_employee']."' name='conoscenza[]' value='".$impez['id_employee']."' /> ".$impez['firstname']."  ";
							
							}
							echo '</td>
							<tr><td><br /></td></tr>
							<tr><td>Allega file</td>
							<td>
							<input name="MAX_FILE_SIZE" value="20000000" type="hidden">
							<script type="text/javascript" src="jquery.MultiFile.js"></script>
							<input name="joinFile[]" class="multi" id="joinFile" type="file" onchange="handleFileSelect(this.files)">
							<output id="list-tkt">Anteprime: </output>
								<script type="text/javascript">
							function handleFileSelect(files) {
							
								// Loop through the FileList and render image files as thumbnails.
								 for (var i = 0; i < files.length; i++) {
									var f = files[i];
									var name = files[i].name;
          
									var reader = new FileReader();  
									reader.onload = function(e) {  
									  // Render thumbnail.
									  var span = document.createElement("span");
									  span.innerHTML = ["<a href=\'",e.target.result,"\' target=\'_blank\'>",name,"</a> - "].join("");
									  document.getElementById("list-tkt").insertBefore(span, null);
									};

								  // Read in the image file as a data URL.
								  reader.readAsDataURL(f);
								}
							  }
							 
							</script>
							</td>
							</tr>
							<tr><td>Messaggio</td>
							<td>
							
							<textarea id="messaggio" class="rte" name="messaggio" rows="15" cols="20" style="width:760px;height:220px">'.(isset($_POST['modifica_messaggio_submit']) ? stripslashes($_POST['modifica_messaggio_messaggio']) : '').'</textarea>
							
							</td>
							</tr>';
							if(isset($_GET['aprinuovomessaggio'])) {
							
							echo '<tr>
							<td>Nota interna (solo per impiegati)</td>
							<td><textarea id="note" name="note" rows="5" cols="145"></textarea>
							</tr>';
							
							}
							
							
							echo '<tr><td></td>
							<td>
							<input name="mail_solo_testo" type="checkbox" />Invia come mail di solo testo (senza HTML)<br />
							<input name="submitMessage" id="submitMessage" value="Invia" class="button_invio"  type="submit"></td>
				
							</tr>
						</table>
					</fieldset>
				</form>';
				if(isset($_GET['id_mex'])) {
			echo "</div></div>";
			} else { }
				
				
		} // FINE INVIO E VISUALIZZAZIONE MESSAGGIO
		
		if(isset($_POST['submitPreventivo'])) {

			$tipo_richiesta_p = Db::getInstance()->getValue("SELECT tipo_richiesta FROM form_prevendita_thread WHERE id_thread = '".Tools::getValue('id_thread')."'");
			Db::getInstance()->ExecuteS("UPDATE form_prevendita_thread SET subject = '".addslashes(Tools::getValue('preventivo_subject'))."', note = '".addslashes(Tools::getValue('preventivo_note'))."', status = '".Tools::getValue('cambiastatusp')."' WHERE id_thread = '".Tools::getValue('id_thread')."'");

			$impiegato_attuale = Db::getInstance()->getValue("SELECT id_employee FROM form_prevendita_thread WHERE id_thread = '".Tools::getValue('id_thread')."'");
			
			if($impiegato_attuale != Tools::getValue('assegnaimpiegatop')) {
	
				Db::getInstance()->ExecuteS("UPDATE form_prevendita_thread SET id_employee = '".Tools::getValue('assegnaimpiegatop')."' WHERE id_thread = '".Tools::getValue('id_thread')."'");
						
				$mailimp = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".Tools::getValue('assegnaimpiegatop')."'");
						
				$tokenimp = Tools::getAdminToken("AdminCustomers"."2".Tools::getValue('assegnaimpiegatop'));
						
				$linkimp = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewpreventivo&id_thread=".Tools::getValue('id_thread')."&token=".$tokenimp.'&tab-container-1=7';

				$msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato il preventivo ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." numero ".Tools::getValue('id_preventivo_univoco')." su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirlo</a>.";
						
				if($preventivo['id_employee'] != 0) {
							
					$mailimprev = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".$preventivo['id_employee']."'");
					$nomeimprev = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = '".Tools::getValue('assegnaimpiegatop')."'");
							
					$msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione del ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." numero ".Tools::getValue('id_preventivo_univoco')." su Ezdirect e l'ha passata a ".$nomeimprev.".";
							
					$params = array(
					'{msg}' => $msgimprev);
							
					Mail::Send(5, 'msg_base', Mail::l('Gestione '.($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo').' revocata', 5), 
										$params, $mailimprev, NULL, NULL, NULL, NULL, NULL, 
										_PS_MAIL_DIR_, true);
						
				}
						
				else {
						
				}
						
				$params = array(
				'{msg}' => $msgimp);
						
				if($_POST['assegnaimpiegatop'] != 0) {
						
					Mail::Send(5, 'msg_base', Mail::l(''.($tipo_richiesta_p == 'tirichiamiamonoi' ? 'Ti richiamiamo noi' : 'Preventivo').' assegnato a te su Ezdirect', 5), 
										$params, $mailimp, NULL, NULL, NULL, NULL, NULL, 
										_PS_MAIL_DIR_, true);
										
				} else { }
						
				
			}
			
			else {
			
			}
			
			Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&viewpreventivo&id_thread='.Tools::getValue('id_thread').'&token='.$this->token.'&conf=4&tab-container-1=7');
		}
				
		
		if(!isset($_GET['viewpreventivo']) && !isset($_GET['id_mex'])) {
		
		
	
		
		
			if (sizeof($azioni_cliente))
			{
				
				echo '
				<table cellspacing="0" cellpadding="0" class="table">
					<tr>
						<th class="center">Tipo richiesta</th>
						<th class="center">'.$this->l('ID azione').'</th>
						<th class="center" style="width:80px">'.$this->l('Status').'</th>
						<th style="text-align:left">In carico</th>
						<th style="text-align:center">Q.ta com.</th>
						<th class="center">Ultima com.</th>
						<th class="center">'.$this->l('Aperto il').'</th>
						<th class="center">Modificato il</th>
						<th>Azioni</th>
						
					</tr>';
				foreach ($azioni_cliente AS $preventivo) {
				
				$annoprev = substr($preventivo['date_add'],2,2);
				if($preventivo['tipo_richiesta'] == 'preventivo') { $sigla = 'P'; } else if($preventivo['tipo_richiesta'] == 'tirichiamiamonoi') { $sigla = 'R'; }
				else if($preventivo['tipo_richiesta'] == 7) { $sigla = 'M'; }
				$idprevunivoco = $sigla.$annoprev.$preventivo['id_thread'];
				
					if($preventivo['tipo_richiesta'] == 7) {
					$ctrl_mex = Db::getInstance()->getValue("SELECT COUNT(id_customer_message) FROM customer_thread ct JOIN customer_message cm ON ct.id_customer_thread = cm.id_customer_thread WHERE cm.id_employee = 0 AND ct.id_customer_thread = ".$preventivo['id_thread']."");	
				}
				else {
					$ctrl_mex = 1;
				}
				
					switch($preventivo['status']) {
						case 'open': $status_preventivo = "<img style='margin-top:5px; display:block; float:left' src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; break;
						case 'closed': $status_preventivo = "<img style='margin-top:5px; display:block; float:left' src='../img/admin/status_green.gif' alt='Chiuso' title='Chiuso' />"; break;
						case 'pending1': $status_preventivo = "<img style='margin-top:5px; display:block; float:left' src='../img/admin/status_orange.gif' alt='In lavorazione' title='In lavorazione' />"; break;
						default: $status_preventivo = "<img style='margin-top:5px; display:block; float:left' src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; break;
					}
				
					$incaricato = new Employee($preventivo['id_employee']);
				
					if($preventivo['tipo_richiesta'] == 7) {
					echo '<tr id="'.$preventivo['id_thread'].'" class="actions_ticket_tr" rel="'.$preventivo['tipo_richiesta'].'">';
						echo '<td>Messaggio</td>';
					}
					else {			
						echo '<tr id="'.$preventivo['id_thread'].'" class="actions_ticket_tr" rel="preventivo">';
						echo '<td>'.($preventivo['tipo_richiesta'] == 'preventivo' ? 'Preventivo' : 'Richiamiamo').'</td>';
					}	
					
					echo'	<td>'.$idprevunivoco.'</td>
						<td>'.($ctrl_mex >= 1 ? $status_preventivo.'
						
						&nbsp;&nbsp;&nbsp;<form style="margin-left:5px; display:block;float:left" name="cambiastatop_'.$preventivo['id_thread'].'" id="cambiastatop_'.$preventivo['id_thread'].'" method="post" />
						
						<input type="hidden" name="id_customer" value="'.$preventivo['id_customer'].'" />
						<input type="hidden" name="id_thread" value="'.$preventivo['id_thread'].'" />
						<input type="hidden" name="tipo_richiesta" value="'.$preventivo['tipo_richiesta'].'" />
						<input type="hidden" name="back" value="1" />
						<select name="cambiastatusp_listing" onchange="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { document.forms[\'cambiastatop_'.$preventivo['id_thread'].'\'].submit(); } else { this.value=\''.$preventivo['status'].'\'}">
						<option style="background-image:url(../img/admin/status_green.gif); background-repeat:no-repeat;
						background-position:bottom left;
						padding-left:30px;"  value="closed" '.($preventivo['status'] == 'closed' ? 'selected="selected"' : '').'>C</option>
						<option style="background-image:url(../img/admin/status_orange.gif); background-repeat:no-repeat;
						background-position:bottom left;
						padding-left:30px;"  value="pending1" '.($preventivo['status'] == 'pending1' ? 'selected="selected"' : '').'>L</option>
						<option style="background-image:url(../img/admin/status_red.gif); background-repeat:no-repeat;
						background-position:bottom left;
						padding-left:30px;"  value="open" '.($preventivo['status'] == 'open' ? 'selected="selected"' : '').'>A</option>
						</select>
						</form>' : '').'</td><td>';
					
					
						if($preventivo['tipo_richiesta'] == 7) {
							$tiporic = 'ticket';
						}
						else if($preventivo['tipo_richiesta'] == 'preventivo') {
							$tiporic = 'preventivo';
						}
						else {
							$tiporic = 'tirichiamiamonoi';
						}
						echo '
							<form style="margin-left:5px; display:block;float:left" name="cambiaincaricoap_'.$preventivo['id_thread'].'" id="cambiaincaricoap_'.$preventivo['id_thread'].'" action="" method="post" />
							<a id="'.$preventivo['id_customer_thread'].'"> </a>
							<input type="hidden" name="id_customer" value="'.$preventivo['id_customer'].'" />
							<input type="hidden" name="id_ticket_univoco" value="'.Customer::trovaSigla($preventivo['id_thread'],$tiporic).'" />
							<input type="hidden" name="back" value="1" />
							<input type="hidden" name="id_thread" value="'.$preventivo['id_thread'].'" />
							<input type="hidden" name="precedente_incarico" value="'.$preventivo['id_employee'].'" />
							<input type="hidden" name="tipo" value="'.$tiporic.'" />
							<select style="width:100px" name="cambiaincaricop" onchange="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { document.forms[\'cambiaincaricoap_'.$preventivo['id_thread'].'\'].submit(); } else { this.value=\''.$preventivo['id_employee'].'\'}">
							<option value="0" '.($preventivo['id_employee'] == '0' ? 'selected="selected"' : '').'>--Nessuno--</option>
							<option value="1" '.($preventivo['id_employee'] == '1' ? 'selected="selected"' : '').'>Ezio</option>
							<option value="2" '.($preventivo['id_employee'] == '2' ? 'selected="selected"' : '').'>Barbara</option>
							<option value="5" '.($preventivo['id_employee'] == '5' ? 'selected="selected"' : '').'>Paolo</option>
							<option value="4" '.($preventivo['id_employee'] == '4' ? 'selected="selected"' : '').'>Massimo</option>
							<option value="3" '.($preventivo['id_employee'] == '3' ? 'selected="selected"' : '').'>Riccardo</option>
							<option value="6" '.($preventivo['id_employee'] == '6' ? 'selected="selected"' : '').'>Federico</option>
							<option value="7" '.($preventivo['id_employee'] == '7' ? 'selected="selected"' : '').'>Matteo</option>
							</select>
							</form>'; 
					
					 echo '</a></td>';
						
						if($preventivo['tipo_richiesta'] == 7) {
							$ultimomess = Db::getInstance()->getValue("SELECT message FROM customer_message WHERE id_customer_thread = ".$preventivo['id_thread']." ORDER BY id_customer_message DESC");
							$nummess = Db::getInstance()->getValue("SELECT COUNT(id_customer_message) FROM customer_message WHERE id_customer_thread = ".$preventivo['id_thread']."");
							$dataultimomess = Db::getInstance()->getValue("SELECT date_add FROM customer_message WHERE id_customer_thread = ".$preventivo['id_thread']." ORDER BY id_customer_message DESC");

						}
						else {
							$ultimomess = Db::getInstance()->getValue("SELECT message FROM form_prevendita_message WHERE id_thread = ".$preventivo['id_thread']." ORDER BY id_message DESC");
							$nummess = Db::getInstance()->getValue("SELECT COUNT(id_message) FROM form_prevendita_message WHERE id_thread = ".$preventivo['id_thread']."");
							$dataultimomess = Db::getInstance()->getValue("SELECT date_add FROM form_prevendita_message WHERE id_thread = ".$preventivo['id_thread']." ORDER BY id_message DESC");
						}
						
						if($preventivo['tipo_richiesta'] == 7) {
							echo '<td style="text-align:center"><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewmessage&id_mex='.$preventivo['id_thread'].'&token='.$this->token.'&tab-container-1=7">'.$nummess.'</a></td>';
							echo '<td><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewmessage&id_mex='.$preventivo['id_thread'].'&token='.$this->token.'&tab-container-1=7">'.substr(strip_tags(html_entity_decode($ultimomess, ENT_NOQUOTES, 'UTF-8')), 0, 75).'...</a></td>
							<td><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewmessage&id_mex='.$preventivo['id_thread'].'&token='.$this->token.'&tab-container-1=7">'.Tools::displayDate($preventivo['date_add'], (int)($cookie->id_lang), false).'</a></td>
							<td><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewmessage&id_mex='.$preventivo['id_thread'].'&token='.$this->token.'&tab-container-1=7">'.Tools::displayDate($dataultimomess, (int)($cookie->id_lang), false).'</a></td>';
							echo '<td><a href="index.php?tab=AdminCustomerThreads&id_customer_thread='.$preventivo['id_thread'].'&del&typedel='.$preventivo['tipo_richiesta'].'&token='.Tools::getAdminToken('AdminCustomerThreads'.(int)Tab::getIdFromClassName('AdminCustomerThreads').(int)$cookie->id_employee).'&backtocustomers='.$customer->id.'&tab-container-1=7"><img src="../img/admin/delete.gif" alt="Cancella" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" /></a></td>';
							

						}
						
						else {
						
							echo '<td style="text-align:center"><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewpreventivo&id_thread='.$preventivo['id_thread'].'&token='.$this->token.'&tab-container-1=7">'.$nummess.'</a></td>';
							echo '<td><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewpreventivo&id_thread='.$preventivo['id_thread'].'&token='.$this->token.'&tab-container-1=7">'.substr(strip_tags(html_entity_decode($ultimomess, ENT_NOQUOTES, 'UTF-8')), 0, 75).'...</a></td>
							<td><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewpreventivo&id_thread='.$preventivo['id_thread'].'&token='.$this->token.'&tab-container-1=7">'.Tools::displayDate($preventivo['date_add'], (int)($cookie->id_lang), false).'</a></td>
							<td><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewpreventivo&id_thread='.$preventivo['id_thread'].'&token='.$this->token.'&tab-container-1=7">'.Tools::displayDate($dataultimomess, (int)($cookie->id_lang), false).'</a></td>';
							echo '<td><a href="index.php?tab=AdminCustomerThreads&id_customer_thread='.$preventivo['id_thread'].'&del&typedel=preventivo&token='.Tools::getAdminToken('AdminCustomerThreads'.(int)Tab::getIdFromClassName('AdminCustomerThreads').(int)$cookie->id_employee).'&backtocustomers='.$customer->id.'&tab-container-1=7"><img src="../img/admin/delete.gif" alt="Cancella" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" /></a></td>';
						
						}
						
						
					echo '</tr>';
				}
				echo '</table>';
				
				

				
				
				echo '<br />Clicca sul preventivo per aprirlo, leggere i messaggi e interagire con il cliente.';
			}
			else
				echo $customer->firstname.' '.$customer->lastname.' '.$this->l(' non ha alcun preventivo.');
		}
		
		
		else {
		
		
			if(isset($_GET['reinvia'])) {
			
			$values = Db::getInstance()->getRow("SELECT * FROM form_prevendita_message fm JOIN form_prevendita_thread ft ON ft.id_thread = fm.id_thread WHERE fm.id_message = '".$_GET['reinvia']."'");
			
				$anno = date('Y');
				$anno = substr($anno, 2,2);
				$sigla = "P";
				$idrichiesta = $sigla.$anno.$values['id_thread'];
				$params = array(
					'{reply}' => $gentilecliente.$values['message'],
					'{firma}' => $firma,
					'{id_richiesta}' => "Id richiesta: <strong>".$idrichiesta."</strong>",
					'{link}' => $link->getPageLink('modules/formprevendita/form.php', true).'?id_thread='.$values['id_thread'].'&token='.$values['token']);
				$attachments = array();
					
				$files = explode(";", $values['file_name']);	
					foreach($files as $file) {
						$parti = explode(":::", $file);
						$fileAttachment['content'] = file_get_contents(_PS_MODULE_DIR_.'../upload/'.$parti[0]);
						$fileAttachment['name'] = $parti[1];	
						$extensions = array('.txt' => 'text/plain', '.rtf' => 'application/rtf', '.doc' => 'application/msword', '.docx'=> 'application/msword',
						'.pdf' => 'application/pdf', '.zip' => 'multipart/x-zip', '.png' => 'image/png', '.jpeg' => 'image/jpeg', '.gif' => 'image/gif', '.jpg' => 'image/jpeg');

						$extension = '';
						foreach ($extensions AS $key => $val)
						if (substr($parti[1], -4) == $key OR substr($parti[1], -5) == $key)
						{
							$extension = $val;
							break;
						}
						$fileAttachment['mime'] = $extension;
						$attachments[] = $fileAttachment;			
					}	
					Customer::findEmailPersona($values['email'], Tools::getValue('id_customer'));
					if($is_supplier == 1) {
						if (Mail::Send(5, 'risposta_fornitore', Mail::l('Messaggio da Ezdirect', 5), 
							$params, $values['email'], NULL, 'info@ezdirect.it', 'Ezdirect', $attachments, NULL, 
								_PS_MAIL_DIR_, true))
						{
									
									//$ct->status = 'closed';
									//$ct->update();
									$mail_scrivente = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = ".$cookie->id_employee."");
									
				
									
									$log_mail=fopen("../import/log-mail.txt","a+");
									$riga_log = "PRV | Mail inviata a ".$values['email']." in data ".date("Y-m-d H:i:s")."\n";
									@fwrite($log_mail,$riga_log);
						}
					}
					else {
						if (Mail::Send((int)$cookie->id_lang, 'reply_msg', Mail::l('Messaggio da Ezdirect', (int)$cookie->id_lang), 
							$params, $values['email'], NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
							_PS_MAIL_DIR_, true))
						{
							
							//$ct->status = 'closed';
							//$ct->update();
								$mail_scrivente = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = ".$cookie->id_employee."");
								
								/*Mail::Send(5, 'simple_msg', Mail::l('Copia preventivo inviato al cliente', 5), 
									$params, $mail_scrivente, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
									_PS_MAIL_DIR_, true);*/
									
								$log_mail=fopen("../import/log-mail.txt","a+");
									$riga_log = "PRV | Mail inviata a ".$values['email']." in data ".date("Y-m-d H:i:s")."\n";
									@fwrite($log_mail,$riga_log);
						}
					}
			Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=7');
			
			}
		
		
			if (isset($_GET['filename'])) {
						$filename = $_GET['filename'];
						
						if(strpos($filename, ":::")) {
			
							$parti = explode(":::", $filename);
							$nomecodificato = $parti[0];
							$nomevero = $parti[1];
							
						}
			
						else {
							$nomecodificato = $filename;
							$nomevero = $filename;
			
						}
						
						if (file_exists(_PS_UPLOAD_DIR_.$nomecodificato)) {
							self::openUploadedFile();
						}
						else { }
					} 
					else { }
			
			if(isset($_GET['id_thread'])) {
			
			
				echo '<div id="tab-container-preventivi" style="margin-top:16px">
				
				
				
				<ul id="tab-container-preventivi-nav" style="list-style-type:none; margin-top:-5px; margin-left:-20px">
				<li><a href="#preventivi-1"><strong>Generale</strong></a></li>
				<li><a href="#preventivi-2"><strong>Cronologia</strong></a></li>
				</ul>';
				
				
				echo '
				
				<div class="yetii-preventivi" id="preventivi-1">
				<fieldset style="width:95%">';
				
				
				echo '
				<form id="modificapreventivo" action="'.$currentIndex.'&id_customer='.$_GET['id_customer'].'&submitPreventivo&viewcustomer&token='.$this->token.'&tab-container-1=7" method="post" autocomplete="off">';
				
				
				
				echo '<div class="anagrafica-cliente" style="width:800px; margin-bottom:10px">
				<strong>Oggetto</strong><br />
				
					<textarea name="preventivo_subject" id="preventivo_subjectContent" style="width:690px;height:40px;display:block;float:left">'.strip_tags(htmlspecialchars_decode($oggetto)).'</textarea>
					<br />
		
				<br /><br />
				</div>

				<div class="clear">
				<div class="anagrafica-cliente">
				ID '.($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo').'<br />
				<span class="tab-span">'.$idprevunivoco.'</span><br />
				</div>
				<div class="anagrafica-cliente">
				In carico a<br />
		<input type="hidden" name="id_preventivo_univoco" value="'.$idprevunivoco.'" />
		<select style="width:220px" name="assegnaimpiegatop">';
		
		echo '<option value="0"'.($cookie->id_employee == 0 ? 'selected="selected"' : '').' >Da assegnare</option>';
		
		foreach ($impiegati_ezp as $impez) {
		
			echo "<option value='".$impez['id_employee']."' ".($preventivo['id_employee'] == $impez['id_employee'] ? 'selected="selected"' : '')." >".$impez['firstname']."</option>";
		
		}

		echo '</select>
		
				</div>
		
			
				<div class="anagrafica-cliente">
				Data apertura<br />
				<span class="tab-span">'.Tools::displayDate($preventivo['date_add'], (int)($cookie->id_lang), true).'</span><br />
				</div>
				
				<div class="clear"></div>
				<div class="anagrafica-cliente">
				Status<br />
				'.$status_preventivo.'
		<input type="hidden" name="id_customer" value="'.Tools::getValue('id_customer').'" />
		<input type="hidden" name="id_thread" value="'.Tools::getValue('id_thread').'" />
		<select name="cambiastatusp">
		<option value="closed" '.($preventivo['status'] == 'closed' ? 'selected="selected"' : '').'>Chiuso</option>
		<option value="pending1" '.($preventivo['status'] == 'pending1' ? 'selected="selected"' : '').'>In lavorazione</option>
		<option value="open" '.($preventivo['status'] == 'open' ? 'selected="selected"' : '').'>Aperto</option>
		</select>
		
		
		<br />
				</div>
				<div class="anagrafica-cliente">
				Tipo richiesta<br />

				<span class="tab-span">'.$preventivo['tipo_richiesta'].'</span><br />
				</div>
					
				
				<div class="anagrafica-cliente">
				Data ultima com.<br />
				<span class="tab-span">'.Tools::displayDate($dataultimo, (int)($cookie->id_lang), true).'</span><br />
				</div>
				
				<div class="clear"></div>';
				
				if(is_numeric($preventivo['category'])) {
				
					$cp_interest = Db::getInstance()->getValue('SELECT name FROM product_lang WHERE id_lang = 5 AND id_product = '.$preventivo['category'].'');
				
				}
				
				else {
					
					$cp_interest = $preventivo['category'];
				
				}
				
				echo '<div class="anagrafica-cliente">
				Categoria / Prodotto di interesse<br />
				<span class="tab-span" style="width:360px">'.$cp_interest.'</span><br />
				</div>
				
				<div class="clear"></div>';

		
		echo '<br />
		<strong>Nota interna visibile solo agli impiegati</strong><br />
					
						<textarea name="preventivo_note" id="preventivonoteContent" style="width:80%;height:100px">'.Tools::htmlentitiesUTF8($preventivo['note']).'</textarea><br /><br />';
				
	
				echo '<input type="submit" value="'.$this->l('   Save   ').'" name="submitPreventivo" class="button" />';
				
		echo '</form>
		</fieldset><br /><br /></div>';
		
		
		}
	
		echo '<div class="yetii-preventivi" id="preventivi-2">';
				
			
			$messaggip = Db::getInstance()->ExecuteS("SELECT * FROM form_prevendita_message fm WHERE fm.id_thread = ".$_GET['id_thread']."");
			
			foreach($messaggip as $messaggiop) {
			
			
				
			
			
				echo "<table style='width:100%; 
				border:1px solid #dfd5c3; 
				";
				if ($messaggiop['id_employee'] == 0) {
					echo 'background-color: #ffffff;';
				} else {
		
					echo 'background-color: #ffecf2;';
				}
				$employeez = new Employee($messaggiop['id_employee']);
				
				
				if(is_numeric($preventivo['category'])) {
				
					$categoriaint =  Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_product = '$preventivo[category]' AND id_lang = 5");
				
				} else {
					$categoriaint = $preventivo['category'];
				
				}
				
				echo "margin-bottom:15px'>";
				echo '<tr>
					<td style="colspan:2">
						<table>
				
						<td style="font-size:14px; width:250px">Da: <strong>';if ($messaggiop['id_employee'] == 0) { echo $preventivo['firstname']." ".$preventivo['lastname']; } else { echo $employeez->firstname." ".$employeez->lastname; } echo '</strong></td>
						
						<td style="width:170px"><strong>Data: </strong>'.$messaggiop['date_add'].'</td>
						<td style="width:180px"><strong>Allegato: </strong>';
						if(!empty($messaggiop['file_name'])) {
						
							$allegatip = explode(";",$messaggiop['file_name']);
							$nallp = 1;
							foreach($allegatip as $allegatop) {
								
								if(strpos($allegatop, ":::")) {
					
									$parti = explode(":::", $allegatop);
									$nomeverop = $parti[1];
									
								}
					
								else {
									$nomeverop = $allegatop;
					
								}
								
								if($allegatop == "") { } else {
									if($nallp == 1) { } else { echo " - "; }
									echo '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewpreventivo&id_thread='.$preventivo['id_thread'].'&token='.$this->token.'&tab-container-1=7&filename='.$allegatop.'"><span style="color:red">'.$nomeverop.'</span></a>';
									$nallp++;
								}
							}
						
						}
						
						else { echo 'Nessun allegato'; }
						
						
						echo '</td>';
						
						$cc = Db::getInstance()->getValue("SELECT cc FROM ticket_cc WHERE type = 'p' AND msg = ".$messaggiop['id_message']."");
						if(!empty($cc)) {
						
							echo '<td>';
						
							$ccs = explode(";",$cc);
							$cc_str = "<strong>CC:</strong> ";
							
							foreach ($ccs as $conoscenza) {
							
								$imp = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".$conoscenza."");
								$cc_str .= $imp." ";
							}
							
							echo $cc_str;
							echo "</td>";
						
						}
						else {
						
							echo '<td><strong>CC:</strong> Nessuno</td>';
						
						}
						
						echo '</tr></table>
					</td>
					</tr>
					<tr>
					<td style="colspan:2">
						<table>
						<tr>
						<td style="width:100px"><strong>Messaggio</strong></td>
						<td>'.htmlspecialchars_decode($messaggiop['message']).($messaggiop['id_employee'] == 0 ? '' : '<form method="post">
						<input type="hidden" name="modifica_messaggio_preventivo" value="'.htmlentities($messaggiop['message'], ENT_QUOTES | ENT_SUBSTITUTE, "UTF-8").'" />
						<input type="submit" name="modifica_messaggio_submit" value="Modifica" class="button" />
						</form>').'<br />
						'.($messaggiop['id_employee'] == 0 ? '' : '<a class="button_invio" style="cursor:pointer" onclick="javascript: var sure=window.confirm(\'Sei sicuro di reinviare questo messaggio?\'); if (sure) { submitFormOkay = false; document.location = \''.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewpreventivo&id_thread='.$preventivo['id_thread'].'&token='.$this->token.'&reinvia='.$messaggiop['id_message'].'\' } else { }"">Reinvia</a>').'
						
						</td>
						</tr>
						</table>
					</td>';
					echo '</tr>';
					
						if($messaggiop['email'] == '') { } else {
					
					echo '<tr><td style="colspan:2">
						<table>
						<tr>
						<td style="width:100px"><strong>Inviato a</strong></td>
						<td>'.htmlspecialchars_decode($messaggiop['email']).'</td>
						</tr>
						</table>
					</td></tr>';
					
					}
					
					
					echo '</table><strong>Prodotto / categoria di interesse</strong>: '.$cp_interest.'<br />';
			}
			
			
		
		if (Tools::isSubmit('submitPreventivoReply'))
			{
				$tipo_richiesta_p = Db::getInstance()->getValue("SELECT tipo_richiesta FROM form_prevendita_thread WHERE id_thread = ".$preventivo['id_thread']."");
				$files=array();
					$fdata=$_FILES['joinFile'];
					if(is_array($fdata['name'])){
						for($i=0;$i<count($fdata['name']);++$i){
							$files[]=array(
								'name'    =>$fdata['name'][$i],
								'tmp_name'=>$fdata['tmp_name'][$i],
								'type' => $fdata['type'][$i],
								'size' => $fdata['size'][$i],
								'error' => $fdata['error'][$i],
							 
							);
						}
					}
					else $files[]=$fdata;
										
					$attachments = array();
					
					
					foreach($files as $file) {
					
						if (isset($files) AND !empty($file['name']) AND $file['error'] != 0)
							$this->_errors[] = Tools::displayError('An error occurred with the file upload.');
							
							
							if (!empty($file['name']))
								{
									$extension = array('.txt', '.rtf', '.doc', '.docx', '.pdf', '.zip', '.png', '.jpeg', '.gif', '.jpg');
									$filename = md5(uniqid().substr($file['name'], -5));
									$fileAttachment['content'] = file_get_contents($file['tmp_name']);
									$fileAttachment['name'] = $file['name'];
									$fileAttachment['mime'] = $file['type'];
								}
								
								if (isset($filename) AND rename($file['tmp_name'], _PS_MODULE_DIR_.'../upload/'.$filename)) {
									$filename = $filename.":::".$file['name'];
									$file_name .= $filename.";";
							}
							
							$attachments[] = $fileAttachment;
							
					}
					
				if(Tools::getValue('messaggiop') == '') {
			
					Db::getInstance()->executeS("UPDATE form_prevendita_thread SET status = '".Tools::getValue('status')."', id_employee = '".Tools::getValue('in_carico_a_p')."' WHERE id_thread = ".$preventivo['id_thread']."");
					
					$this->assegna_incarico(Tools::getValue('in_carico_a_p'), $preventivo['id_thread'], 'preventivo');

					Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&error=1pv&token='.$this->token.'&tab-container-1=7');
							
			
				}
					
				
				if(Tools::getValue('nuovopreventivo') == 1) {
				
					$ultimothread = Db::getInstance()->getValue("SELECT id_thread FROM form_prevendita_thread ORDER BY id_thread DESC");
					$thread = $ultimothread+1;
					
					$addresscli = Db::getInstance()->getValue("SELECT id_address FROM address WHERE fatturazione = 1 AND active = 1 AND deleted = 0 AND id_customer = ".$customer->id."");
					
					$addresscliente = new Address($addresscli);
					$token = Tools::passwdGen(12);
					
					$cstmr = Db::getInstance()->getRow("SELECT company, firstname, lastname, is_company FROM customer WHERE id_customer = ".(int)($customer->id)."");
						
						
					if($cstmr['is_company'] == 1) { $cstmr_r = $cstmr['company']; } else { $cstmr_r = $cstmr['firstname']." ".$cstmr['lastname']; }
					
					$subject = "*** RICHIESTA COMMERCIALE da ".$cstmr_r." - Tipo richiesta: Preventivo ***";
					
					Db::getInstance()->ExecuteS("INSERT INTO form_prevendita_thread (id_thread, tipo_richiesta, id_customer, id_employee, subject, tax_code, vat_number, firstname, lastname, company, address1, postcode, state, country, email, phone, token, status, category, date_add, date_upd, note) VALUES ('$thread', 'preventivo', '".$customer->id."', '".Tools::getValue('in_carico_a_p')."', '".addslashes($subject)."', '".addslashes($customer->tax_code)."', '".addslashes($customer->vat_number)."', '".addslashes($customer->firstname)."', '".addslashes($customer->lastname)."', '".addslashes($customer->company)."', '".addslashes($addresscliente->address1)."', '".addslashes($addresscliente->postcode)."', '".addslashes($addresscliente->id_state)."', '".addslashes($addresscliente->id_country)."', '".addslashes($customer->email)."', '".addslashes($addresscliente->phone)."', '$token', '".addslashes(Tools::getValue('status'))."', '', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."', '".Tools::getValue('note')."')");   
					
					Db::getInstance()->ExecuteS("INSERT INTO form_prevendita_message (id_message, id_thread, id_customer, id_employee, file_name, message, date_add, date_upd) VALUES (NULL, '$thread', ".$customer->id.", '".$cookie->id_employee."', '".addslashes($file_name)."', '".addslashes(Tools::getValue('messaggiop'))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
					
					$mail_verso_cui_inviare = $customer->email;
					
					$sigla = 'P';
					$idmessaggio = mysql_insert_id();

				}
				
				else {
					
					$token = $preventivo['token'];
					$thread = $preventivo['id_thread'];
				
					Db::getInstance()->ExecuteS("UPDATE form_prevendita_thread SET  status = '".addslashes(Tools::getValue('status'))."', date_upd = '".date("Y-m-d H:i:s")."', id_employee = '".Tools::getValue('in_carico_a_p')."' WHERE id_thread = '".$preventivo['id_thread']."'");  
				
					Db::getInstance()->ExecuteS("INSERT INTO form_prevendita_message (id_message, id_thread, id_customer, id_employee, file_name, message, date_add, date_upd) VALUES (NULL, '".$preventivo['id_thread']."', ".$customer->id.", '".$cookie->id_employee."', '".addslashes($file_name)."', '".addslashes(Tools::getValue('messaggiop'))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
					
					$mail_verso_cui_inviare = $preventivo['email'];
					
					$idmessaggio = mysql_insert_id();
				}
				
				
				Db::getInstance()->ExecuteS("UPDATE form_prevendita_message SET email = '".Tools::getValue('customer_email_msg_p')."' WHERE id_message = ".$idmessaggio."");
					
			
				if($preventivo['tipo_richiesta'] == 'preventivo') {
					$sigla = 'P';
				}
				else if($preventivo['tipo_richiesta'] == 'tirichiamiamonoi') {
					$sigla = 'R';
				}
				
				$anno = date('Y');
						$anno = substr($anno, 2,2);
						
				$numerounivoco = $sigla.$anno.$preventivo['id_thread'].$idmessaggio;
					
				$primoticket = Db::getInstance()->getValue("SELECT id_message FROM form_prevendita_message WHERE id_thread = $preventivo[id_thread] ORDER BY id_message ASC");
						
				if(!$primoticket) {
							
					$primoticket = $idmessaggio;
						
				}
						
				else {
						
				}	
				$primoticket = $sigla.$anno.$preventivo['id_thread'].$primoticket;
				
				$file_csv=fopen("../cms/quotazioni/servizio-clienti.csv","a+");
				
				$data_quotazione = date("d/m/Y H:i:s");
				
				$categoria = $preventivo['category'];
				
				if(is_numeric($categoria)) {
				
					$intcat = "Prodotto di interesse";
					$categoria =  Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_product = '$categoria' AND id_lang = 5");
		
				
				}
				
				else {
				
					$intcat = "Categoria di interesse";
					$categoria = $preventivo['category'];
				
				
				}
				$in_carico_a =  Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".Tools::getValue('in_carico_a_p')."");
				if(empty($in_carico_a)) { $in_carico_a = "Nessuno"; }
				
				$messaggioclientepercsv = Tools::getValue('messaggiop');
				$messaggioclientepercsv = strip_tags($messaggioclientepercsv);
				$messaggioclientepercsv = str_replace(";", ",", $messaggioclientepercsv);
					
				$messaggioclientepercsv = str_replace(chr(10), " ", $messaggioclientepercsv); //remove carriage returns
				$messaggioclientepercsv = str_replace(chr(13), " ", $messaggioclientepercsv); //remove carriage returns	
				
				$employeeincaricato = new Employee($cookie->id_employee);
				$employeeincaricato2 = new Employee($preventivo['id_employee']);
				if(Tools::getValue('nuovopreventivo') == 1) {
			
					$idrichiesta = $sigla.$anno.$thread;
				}
				
				else {
				
					$idrichiesta = $sigla.$anno.$preventivo['id_thread'];
				}
				$riga_richiesta = $categoria.";".$data_quotazione.";;;$preventivo[vat_number];$preventivo[tax_code];".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;;;;;;;".$messaggioclientepercsv.";$idmessaggio;$preventivo[id_thread];$numerounivoco;$primoticket;prevendita;R;$idrichiesta;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
				
				if($tipo_richiesta_p == 'preventivo') {
					@fwrite($file_csv,$riga_richiesta);
				}
				else {
				}
						
				@fclose("../cms/quotazioni/servizio-clienti.csv");	
				
				
					$params = array(
					'{reply}' => $gentilecliente.(Tools::getValue('messaggiop')),
					'{firma}' => $firma,
					'{id_richiesta}' => "Id richiesta: <strong>".$idrichiesta."</strong>",
					'{link}' => $link->getPageLink('modules/formprevendita/form.php', true).'?id_thread='.$thread.'&token='.$token);
					
					
					$params2 = array(
					'{reply}' => $employeeincaricato->firstname." ".$employeeincaricato->lastname." ha aperto un ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." per conto di un cliente. L'id del preventivo &egrave;: ".$idrichiesta."");
					
					$params3 = array(
					'{reply}' => $employeeincaricato->firstname." ".$employeeincaricato->lastname." ti ha assegnato un ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." su Ezdirect. L'id del ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." &egrave;: ".$idrichiesta."");
					
					$mails_preventivi = explode(";", Tools::getValue('customer_email_msg_p'));
							
					foreach ($mails_preventivi as $mail) {
						$mail = trim($mail);
						Customer::findEmailPersona($mail, Tools::getValue('id_customer'));
						if (Mail::Send((int)$cookie->id_lang, 'reply_msg', Mail::l('Messaggio da Ezdirect', (int)$cookie->id_lang), 
							$params, $mail, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
							_PS_MAIL_DIR_, true))
						{
							
							//$ct->status = 'closed';
							//$ct->update();
								$mail_scrivente = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = ".$cookie->id_employee."");
								
								/*Mail::Send(5, 'simple_msg', Mail::l('Copia preventivo inviato al cliente', 5), 
									$params, $mail_scrivente, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
									_PS_MAIL_DIR_, true);*/
									
								$log_mail=fopen("../import/log-mail.txt","a+");
									$riga_log = "PRV | Mail inviata a ".$mail." in data ".date("Y-m-d H:i:s")."\n";
									@fwrite($log_mail,$riga_log);
						}
					}
				
					
					
					
					$cc = "";
					
					foreach($_POST['conoscenzap'] as $conoscenza) {
					
						$cc .= $conoscenza.";";
					
						echo $conoscenza."<br />";
						
						$mailconoscenza = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = $conoscenza");
						
						$tokenimp = Tools::getAdminToken("AdminCustomers"."2".$conoscenza);
								
						$linkimp = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewpreventivo&id_thread=".$thread."&token=".$tokenimp.'&tab-container-1=7';
							
						$params4 = array(
						'{reply}' => "<strong>".$employeemess->firstname."</strong> ha inviato un ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." a un cliente e ti ha messo in copia conoscenza.
						<br /><br />
						L'ID del ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." &egrave;: <strong>".$idrichiesta."</strong><br /><br />
						Il ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." &egrave; in carico a: <strong>".$in_carico_a."</strong><br /><br />
						<a href='".$linkimp."'>Clicca qui per aprire il ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')."</a>. ",
						'{firma}' => $firma,
						'{id_richiesta}' => "Id richiesta: <strong>".$idrichiesta."</strong>",
						'{link}' => $link->getPageLink('modules/formprevendita/form.php', true).'?id_thread='.$thread.'&token='.$token);
					
						
								
						Mail::Send((int)$cookie->id_lang, 'senzagrafica', Mail::l(($tipo_richiesta_p == 'tirichiamiamonoi' ? 'Ti richiamiamo noi' : 'Preventivo').' utente in copia conoscenza', (int)$cookie->id_lang), 
						$params4, $mailconoscenza, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
						_PS_MAIL_DIR_, true);
					
							
					}
					
					if(!empty($_POST['conoscenzap'])) {
							
								Db::getInstance()->executeS("INSERT INTO ticket_cc (msg, type, cc) VALUES (".$idmessaggio.", 'p', '".$cc."')"); 
							
							
					}
					
					
				
					/*Mail::Send((int)$cookie->id_lang, 'action', Mail::l('Aperto un preventivo per conto del cliente', (int)$cookie->id_lang), 
								$params2, "ezio.delgiudice@ezdirect.it", NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
								_PS_MAIL_DIR_, true);*/
					
					if(Tools::getValue('in_carico_a_p') != 0) {

						if (Tools::getValue('in_carico_a_p') == $preventivo['id_employee']) {
									
						}
						
						else if (Tools::getValue('in_carico_a_p') == $cookie->id_employee) {
									
						}
						
						else {
							$mail_incarico_p = Db::getInstance()->getValue('SELECT email FROM employee WHERE id_employee = '.Tools::getValue('in_carico_a_p').'');
							$tokenimp = Tools::getAdminToken("AdminCustomers"."2".Tools::getValue('in_carico_a_p'));
								
							$linkimp = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewpreventivo&id_thread=".$thread."&token=".$tokenimp.'&tab-container-1=7';
							
							$params3 = array(
							'{reply}' => $employeeincaricato->firstname." ".$employeeincaricato->lastname." ti ha assegnato un ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." su Ezdirect. L'id del ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." &egrave;: ".$idrichiesta."<br /><br />
							<a href='".$linkimp."'>Clicca qui per rispondere</a>.
							");
							
							
							Mail::Send(5, 'action', Mail::l(''.($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo').' cliente assegnato a te su Ezdirect', 5), 
							$params3, $mail_incarico_p, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
							_PS_MAIL_DIR_, true);
						}
					
					
					Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=7');
					
					}
					else {
					
					Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&viewpreventivo&id_thread='.$thread.'&preventivoreply&conf=4&token='.$this->token.'&tab-container-1=7');
					}
			}
		
		
			else {
			
				if (isset($_GET['preventivoreply'])) {
			
				echo 'Risposta inviata con successo.<br /> <a class="button" href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewpreventivo&id_thread='.$preventivo['id_thread'].'&token='.$this->token.'&tab-container-1=7"><strong>Clicca qui se vuoi rispondere nuovamente.</strong></a><br /><br />';
				
				}
				
				else if (isset($_GET['preventivoreply2'])) {
			
				echo 'Preventivo aperto con successo. <br /><br />';
				
				}
			
				else if (!isset($_GET['id_mex']) && (!isset($_GET['preventivoreply']) || isset($_GET['aprinuovopreventivo']))) {
				$iso = Language::getIsoById((int)($cookie->id_lang));
		$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
		$ad = dirname($_SERVER["PHP_SELF"]);
		
		
			if(isset($_GET['aprinuovopreventivo'])) {
			
				echo '<h2>Invia preventivo al cliente</h2>';
			
			
			}
		
			else {
			
				echo '<h2 id="rispondi-cliente-preventivo">Rispondi al cliente</h2>';
				if(isset($_POST['modifica_messaggio_submit'])) {
				echo '<script type="text/javascript">
					 window.location = "#rispondi-cliente-preventivo";
				</script>';
				}
				
			}
		
		
		
				echo '
			<script type="text/javascript">
			
			noConfirm = false;

			</script>	
			
			<script type="text/javascript">
			var iso = \''.$isoTinyMCE.'\' ;
			var pathCSS = \''._THEME_CSS_DIR_.'\' ;
			var ad = \''.$ad.'\' ;
			</script>
			
			
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce3/tiny_mce.js"></script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>';
			
			echo '<script type="text/javascript">
				$(document).ready(function () {
				
dataChanged = 0;     // global variable flags unsaved changes      

function bindForChange(){    
     $(\'input,checkbox,text,textarea,radio,select\').bind(\'change\',function(event) { dataChanged = 1});
	 $(\'#messaggiop\').bind(\'input propertychange\', function() { dataChanged = 1 });
     $(\':reset,:submit\').bind(\'click\',function(event) { dataChanged = 0 });
}


function askConfirm(){  
    if (dataChanged){ 
        return "You have some unsaved changes.  Press OK to continue without saving." 
    }
}

window.onbeforeunload = askConfirm;
window.onload = bindForChange;
});
			</script>';
			
			
			
			
			echo '<script type="text/javascript">
					toggleVirtualProduct(getE(\'is_virtual_good\'));
					unitPriceWithTax(\'unit\');
			</script>';
			$employeemessp = new Employee($cookie->id_employee);
				echo'	<form name="submit_preventivo" id="submit_preventivo" action="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewpreventivo&id_thread='.$preventivo['id_thread'].'&token='.$this->token.'&tab-container-1=7" method="post" class="std" enctype="multipart/form-data">
				<fieldset>';
				
				if(isset($_GET['aprinuovopreventivo'])) {
			
				echo '<input type="hidden" name="nuovopreventivo" value="1" />';
			
				}
				
				else {
				
					echo '			<script type="text/javascript">
					
						$(document).ready(function () {
							
							$("#submit_preventivo").submit(function () {
								d = document.getElementById("status").value;
								
								if(d != "'.$preventivo['status'].'") {
										}
								
								else {
									var salvamodifiche = window.confirm("STATUS: OK per procedere, ANNULLA per cambiare."); 
							
										if (salvamodifiche) {
		$(\'#waiting1\').waiting({ 
		elements: 10, 
		auto: true 
		});
		var overlay = jQuery(\'<div id="overlay"></div>\');
		var attendere = jQuery(\'<div id="attendere">ATTENDERE PREGO</div>\');
		overlay.appendTo(document.body);
		attendere.appendTo(document.body);
		
											$("#submitMessage").trigger("click");
										} else {
											 return false;
												
											//niente
										}
								}
							});
							
							
						});
			
					</script>';	
					
				}
				
				
				$customer_phone = Db::getInstance()->getRow("SELECT	phone, phone_mobile FROM address WHERE id_customer = ".$customer->id." AND deleted = 0 AND fatturazione = 1 AND active = 1");
							
							if($customer_phone['phone'] == '') { $telefono_cliente = $customer_phone['phone_mobile']; } else { $telefono_cliente = $customer_phone['phone']; }
							
					echo '<table>
					<tr><td>Email</td><td>';
							
							
							if($customer->is_company == 1) { echo '<div style="float:left">';
							
							$persone = Db::getInstance()->executeS("SELECT firstname, lastname, email FROM persone WHERE id_customer = ".Tools::getValue('id_customer')." AND email != ''");
							
							echo '
							<select style="width:160px" name="seleziona-persona" onchange="javascript:document.getElementById(\'customer_email_msg_p\').value = (this.value);" >';
							echo "<option value=''>-- Seleziona persona --</option>";
							
							foreach ($persone as $persona) {
							
								echo "<option value='".$persona['email']."'>".$persona['firstname']." ".$persona['lastname']."</option>";
							
							}
							
							echo "</select></div>";
					
					
							} else { }
					
					
					
					
					
					
					
					
					echo '<div style="float:left; '.($customer->is_company == 1? 'margin-left:20px' : '').'"><input type="text" size="40" name="customer_email_msg_p" id="customer_email_msg_p" value="'.(!$preventivo['email'] ? $customer->email : $preventivo['email']).'" /><br />
							<span style="font-size:11px">In caso di più indirizzi mail, separare con punto e virgola (;)</span></div><div style="float:left; margin-left:20px"> Tel. cliente: <input type="text" readonly="readonly" value="'.(!$preventivo['phone'] ? $telefono_cliente : $preventivo['phone']).'" /></div></td></tr>
					
					';
				
						
					echo '
						
						<tr><td>Stato preventivo</td>
						<td>
						<div style="float:left">
						<select name="status" id="status">
						<option value="open" '.($preventivo['status'] == 'open' ? 'selected="selected"' : '').'>Aperto   </option>
						<option value="pending1" '.($preventivo['status'] == 'pending1' ? 'selected="selected"' : '').'>In lavorazione   </option>
						<option value="closed" '.($preventivo['status'] == 'closed' ? 'selected="selected"' : '').'>Chiuso   </option>
						
						</select>
						</div><div style="float:left; margin-left:20px">
						<script type="text/javascript">
								function changeInCaricoAP()
								{';
								
								
								foreach ($impiegati_ezp as $impezp) {
									echo '
									
									firstVar = document.getElementById("in_carico_a_p").value;
									
									if (document.getElementById("in_carico_a_p").value == "'.$impezp['id_employee'].'") {
									
									
									document.getElementById(\'conoscenza_'.$impezp['id_employee'].'\').disabled = true;
									document.getElementById(\'conoscenza_'.$impezp['id_employee'].'\').checked = false;
									
									} else { document.getElementById(\'conoscenza_'.$impezp['id_employee'].'\').disabled = false; }
								';
								}
							echo '
								}
								</script>
							In carico a';
					echo '
				
				<select style="width:250px" name="in_carico_a_p" id="in_carico_a_p" onChange = "changeInCaricoAP();">';
				

				foreach ($impiegati_ezp as $impezp) {
				
					if(isset($_GET['aprinuovopreventivo'])) {
						echo "<option value='".$impezp['id_employee']."' ".($cookie->id_employee == $impezp['id_employee'] ? 'selected="selected"' : '')." >".$impezp['firstname']." ".$impezp['lastname']."</option>";
					}
					
					else {
					
						echo "<option value='".$impezp['id_employee']."' ".($preventivo['id_employee'] == $impezp['id_employee'] ? 'selected="selected"' : '')." >".$impezp['firstname']." ".$impezp['lastname']."</option>";
					}
				}

				echo '</select><br /><br /></td>
							</tr>
						<tr><td>Conoscenza</td>
							<td>
							';
							
							foreach ($impiegati_ezp as $impez) {
		
								echo "<input type='checkbox' name='conoscenzap[]' id='conoscenza_".$impez['id_employee']."'value='".$impez['id_employee']."' /> ".$impez['firstname']."  ";
							
							}
							echo '<br /></td>
							</tr>
							<tr><td><br /></td></tr>
						<tr><td>Allega file</td>
						<td>
						<input name="MAX_FILE_SIZE" value="20000000" type="hidden">
						<script type="text/javascript" src="jquery.MultiFile.js"></script>
						<input name="joinFile[]" class="multi" id="joinFile" type="file" onchange="handleFileSelect(this.files)">
							<output id="list-tkt">Anteprime: </output>
								<script type="text/javascript">
							function handleFileSelect(files) {
							
								// Loop through the FileList and render image files as thumbnails.
								 for (var i = 0; i < files.length; i++) {
									var f = files[i];
									var name = files[i].name;
          
									var reader = new FileReader();  
									reader.onload = function(e) {  
									  // Render thumbnail.
									  var span = document.createElement("span");
									  span.innerHTML = ["<a href=\'",e.target.result,"\' target=\'_blank\'>",name,"</a> - "].join("");
									  document.getElementById("list-tkt").insertBefore(span, null);
									};

								  // Read in the image file as a data URL.
								  reader.readAsDataURL(f);
								}
							  }
							 
							</script>
						</td>
						</tr>
						<tr><td>Messaggio</td>
						<td>
				
						<textarea style="width:760px" class="rte" id="messaggiop" name="messaggiop" rows="5" cols="40">
						'.(isset($_POST['modifica_messaggio_submit']) ? stripslashes($_POST['modifica_messaggio_preventivo']) : '').'
						</textarea>
						
						
						</td>
						</tr>';
						
						if(isset($_GET['aprinuovopreventivo'])) {
							
							echo '<tr>
							<td>Nota interna (solo per impiegati)</td>
							<td><textarea id="note" name="note" rows="5" cols="145"></textarea>
							</tr>';
							
						}
						// onclick="submitFormOkay = true; this.style.visibility=\'hidden\', loading.style.visibility=\'visible\'"
						echo '<tr><td></td>
						<td><input name="submitPreventivoReply"  id="submitPreventivoReply" value="Invia" class="button_invio" type="submit" ></td>
			
						</tr>
					</table>
			
				</fieldset>
			</form>';
			
			if(isset($_GET['id_thread'])) {
			echo "</div></div>";
			} else { }
		}
		
		}
		
			
		}
		
		
		
		echo '<br /><br /><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=7" class="button">Torna alla lista dei preventivi</a>';
		echo '</div>';
		}
		else { }
		
		// FINE PREVENTIVI!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
		
		
		
		
		
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 8) {
		
		
		
		

		echo '
		<div class="tab1" id="stats">';
		echo '<h2>'.$this->l('Dati statistici cliente').'</h2>
		<table cellspacing="0" cellpadding="0" class="table">
		<tr>
		<th>Dato</th>
		<th>Valore</th>
		</tr>
		';
		
		if($customer->is_company == 1) {
		
		echo "<tr><td><strong>Numero dipendenti</strong></td>
		<td>".$customer->employees_number."</td>
		</tr>";
		}
		else {
		}
		
		$interesse = $customer->getInteresse();
		
		foreach($interesse as $intx) {
		echo "<tr><td><strong>Interesse</strong></td><td>";
		echo Category::getCatNameById($intx['id_category_default'], 5);
		echo "</td></tr>";
		}
		
		$primoacquisto = $customer->ultimoAcquistoPrimoAcquisto("primo");
		
		foreach($primoacquisto as $primoprod) {
		echo "<tr><td><strong>Primo acquisto</strong></td><td>";
		echo $primoprod['product_name'].", ".$primoprod['product_quantity']." pz.";
		echo "</td></tr>";
		}
		
		$ultimoacquisto = $customer->ultimoAcquistoPrimoAcquisto("ultimo");
		
		foreach($ultimoacquisto as $ultimoprod) {
		echo "<tr><td><strong>Ultimo acquisto</strong></td><td>";
		echo $ultimoprod['product_name'].", ".$ultimoprod['product_quantity']." pz.";
		echo "</td></tr>";
		}	
				
		
		
		echo "</table><br /><br />";
		echo '<h2>Totale acquisti</h2>';
		
		$acquistiperanno = $customer->getOrdersByYear();
		
		if(count($acquistiperanno) == 0) {
			echo "Questo cliente non ha mai fatto acquisti su Ezdirect.";
		}
		else {
			echo '<table cellspacing="0" cellpadding="0" class="table" style="text-align:right">';
			echo '<tr><th>Anno</th><th>Totale pagato</th><th>Totale senza iva</th></tr>';
			foreach($acquistiperanno as $acquisti) {
				echo '<tr><td><strong>'.$acquisti['anno'].'</strong></td>
				<td>'.Tools::displayPrice($acquisti['totale'], new Currency($defaultCurrency)).'</td>
				<td>'.Tools::displayPrice($acquisti['totale_senza_iva'], new Currency($defaultCurrency)).'</td>
				</tr>';
			}
			foreach ($orders AS $order)
			if ($order['valid']) {
				$totalOKconiva += $order['total_paid_real'];
				$totalOKsenzaiva +=  $order['total_products'];
			}

			echo '<tr><td><strong>Totale</strong></td>
				<td>'.Tools::displayPrice($totalOKconiva, new Currency($defaultCurrency)).'</td>
				<td>'.Tools::displayPrice($totalOKsenzaiva, new Currency($defaultCurrency)).'</td>
				</tr>';
			
			echo '</table>';
		}
		
	
		
		echo "<br /><br />";
		
			
			echo '<h2>'.$this->l('Prodotti ordinati').' ('.sizeof($products).')</h2>
			<table cellspacing="0" cellpadding="0" class="table">
				<tr>
					<th>'.$this->l('Date').'
					<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=dateasc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/up.gif" alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a>
					
					<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=datedesc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/down.gif" alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>
					
					</th>
					<th>'.$this->l('Name').'
					<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=nameasc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/up.gif" alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a>

					<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=namedesc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/down.gif" alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>
					
					</th>
					<th>Marca
					
					<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=manufacturerasc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/up.gif" alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a>
					<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=manufacturerdesc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/down.gif" alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>
					</th>
					<th>Categoria
					<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=categoryasc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/up.gif" alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a>
					<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=categorydesc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/down.gif" alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>
					</th>
					<th>'.$this->l('Qt.').'
					<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=quantityasc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/up.gif" alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a>
					<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=quantitydesc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/down.gif" alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>
					</th>
					<th>Totale di sempre</th>
					<th>'.$this->l('Actions').'</th>
				</tr>';
			$tokenOrders = Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).(int)($cookie->id_employee));

			if(isset($_GET['sort']) && $_GET['sort'] == "datedesc") {
			$products = $customer->getBoughtProducts2("o.date_add DESC");
			}
			if(isset($_GET['sort']) && $_GET['sort'] == "dateasc") {
			$products = $customer->getBoughtProducts2("o.date_add ASC");
			}
			if(isset($_GET['sort']) && $_GET['sort'] == "nameasc") {
			$products = $customer->getBoughtProducts2("od.product_name ASC");
			}			
			if(isset($_GET['sort']) && $_GET['sort'] == "namedesc") {
			$products = $customer->getBoughtProducts2("od.product_name DESC");
			}	
			if(isset($_GET['sort']) && $_GET['sort'] == "manufacturerdesc") {
			$products = $customer->getBoughtProducts2("m.name DESC");
			}			
			if(isset($_GET['sort']) && $_GET['sort'] == "manufacturerasc") {
			$products = $customer->getBoughtProducts2("m.name ASC");
			}				
			if(isset($_GET['sort']) && $_GET['sort'] == "categoryasc") {
			$products = $customer->getBoughtProducts2("cl.name ASC");
			}			
			if(isset($_GET['sort']) && $_GET['sort'] == "categorydesc") {
			$products = $customer->getBoughtProducts2("cl.name DESC");
			}				
			if(isset($_GET['sort']) && $_GET['sort'] == "quantityasc") {
			$products = $customer->getBoughtProducts2("od.product_quantity ASC");
			}	
			if(isset($_GET['sort']) && $_GET['sort'] == "quantitydesc") {
			$products = $customer->getBoughtProducts2("od.product_quantity DESC");
			}
			
			foreach ($products AS $product) {
			
			echo '
				<tr '.($irow++ % 2 ? 'class="alt_row"' : '').' style="cursor: pointer" onclick="window.open(\'?tab=AdminOrders&id_order='.$product['id_order'].'&vieworder&token='.$tokenOrders.'\', \'blank\', \'height=600,width=1200,modal=yes,alwaysRaised=yes,scrollbars=yes\');">
					<td>'.Tools::displayDate($product['date_add'], (int)($cookie->id_lang), false).'</td>
					<td>'.$product['product_name'].'</td>
					<td>'.Manufacturer::getNameById($product['id_manufacturer']).'</td>
					<td>'.Category::getCatNameById($product['id_category_default'], 5).'</td>
					<td align="right">'.$product['product_quantity'].'</td>
					<td align="right">'.Customer::getTotalBoughtProducts($product['product_id'], $_GET['id_customer']).'</td>
					<td align="center"><img src="../img/admin/details.gif" /></td>
				</tr>';
				}
			echo '
			</table>';

		
		$interested = Db::getInstance()->ExecuteS('SELECT DISTINCT * FROM '._DB_PREFIX_.'cart_product cp INNER JOIN '._DB_PREFIX_.'cart c on c.id_cart = cp.id_cart WHERE c.id_customer = '.(int)$customer->id.' ');
		
		/* //per escludere prodotti ordinati
		$interested = Db::getInstance()->ExecuteS('SELECT DISTINCT id_product FROM '._DB_PREFIX_.'cart_product cp INNER JOIN '._DB_PREFIX_.'cart c on c.id_cart = cp.id_cart WHERE c.id_customer = '.(int)$customer->id.' AND cp.id_product NOT IN (
		SELECT product_id FROM '._DB_PREFIX_.'orders o inner join '._DB_PREFIX_.'order_detail od ON o.id_order = od.id_order WHERE o.valid = 1 AND o.id_customer = '.(int)$customer->id.')'); */
		
		
		if (count($interested))
		{
			echo '
			<br /><h2>'.$this->l('Prodotti nel carrello').' ('.count($interested).')</h2>
			<table cellspacing="0" cellpadding="0" class="table">';
			echo "<tr><th>Aggiunto il</th><th>Id prodotto</th><th>Nome prodotto</th><th></th></tr>";
			
			foreach ($interested as $p)
			{
				$product = new Product((int)$p['id_product'], false, $cookie->id_lang);
				
				if($product->id != 0) {
				
				echo '
				<tr '.($irow++ % 2 ? 'class="alt_row"' : '').' style="cursor: pointer" onclick="javascript:window.open(\''.$link->getProductLink((int)$product->id, $product->link_rewrite, Category::getLinkRewrite($product->id_category_default, (int)($cookie->id_lang))).'\', \'blank\', \'height=600,width=1200,modal=yes,alwaysRaised=yes,scrollbars=yes\');">
					<td>'.$p['date_add'].'</td>
					<td>'.(int)$product->id.'</td>
					<td>'.Tools::htmlentitiesUTF8($product->name).'</td>
					<td align="center"><img src="../img/admin/details.gif" /></td>
				</tr>';
				
				}
				
				else {
				
				$resnmex = mysql_query("SELECT product_id, product_name FROM order_detail WHERE product_id = $p[id_product]");
				$rownmex = mysql_fetch_array($resnmex, MYSQL_ASSOC);
				
				echo '
				<tr>
				<td>'.$p['date_add'].'</td>
					<td>'.$rownmex['product_id'].'</td>
					<td>'.Tools::htmlentitiesUTF8($rownmex['product_name']).' - <em>prodotto non pi&ugrave; esistente</em></td>
					<td align="center"></td>
				</tr>';
				
				
				
				}
				
				
				
			}
			echo '</table>';
		}
		

		/* Last connections */
		$connections = $customer->getLastConnections();
		
			echo '
		<h2>'.$this->l('Last connections').'</h2>';
		
	if (sizeof($connections))
		{
		echo '
			<table cellspacing="0" cellpadding="0" class="table">
				<tr>
					<th style="width: 200px">'.$this->l('Date').'</th>
					<th style="width: 100px">'.$this->l('Pages viewed').'</th>
					<th style="width: 100px">'.$this->l('Total time').'</th>
					<th style="width: 100px">'.$this->l('Origin').'</th>
					<th style="width: 100px">'.$this->l('IP Address').'</th>
				</tr>';
			foreach ($connections as $connection)
				echo '<tr>
						<td>'.Tools::displayDate($connection['date_add'], (int)($cookie->id_lang), true).'</td>
						<td>'.(int)($connection['pages']).'</td>
						<td>'.$connection['time'].'</td>
						<td>'.($connection['http_referer'] ? preg_replace('/^www./', '', parse_url($connection['http_referer'], PHP_URL_HOST)) : $this->l('Direct link')).'</td>
						<td>'.$connection['ipaddress'].'</td>
					</tr>';
			echo '</table>';
		}
		if (sizeof($referrers))
		{
			echo '<h2>'.$this->l('Referrers').'</h2>
			<table cellspacing="0" cellpadding="0" class="table">
				<tr>
					<th style="width: 200px">'.$this->l('Date').'</th>
					<th style="width: 200px">'.$this->l('Name').'</th>
				</tr>';
			foreach ($referrers as $referrer)
				echo '<tr>
						<td>'.Tools::displayDate($referrer['date_add'], (int)($cookie->id_lang), true).'</td>
						<td>'.$referrer['name'].'</td>
					</tr>';
			echo '</table>';
		}
		echo '</div>';
		
		} 

		// AZIONI!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 10) {
		
		$azioni = Db::getInstance()->ExecuteS("SELECT * FROM action_thread WHERE id_customer = ".$customer->id." ORDER BY id_action DESC");
		
		
		echo '<div class="tab1" id="azioni-customer">';
		
		$impiegati_eza = Db::getInstance()->ExecuteS("SELECT id_employee, firstname, lastname FROM employee");
		
		if(isset($_GET['id_action'])) {
		
				$azione = Db::getInstance()->getRow("SELECT * FROM action_thread at WHERE at.id_action = ".$_GET['id_action']."");
			$incaricato = new Employee($azione['action_to']);
			
				switch($azione['status']) {
						case 'open': $status_azione = "<img src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; break;
						case 'closed': $status_azione = "<img src='../img/admin/status_green.gif' alt='Chiuso' title='Chiuso' />"; break;
						case 'pending1': $status_azione = "<img src='../img/admin/status_orange.gif' alt='In lavorazione' title='In lavorazione' />"; break;
						default: $status_azione = "<img src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; break;
					}
			
		
				$oggetto = Db::getInstance()->getValue("SELECT subject FROM action_thread WHERE id_action = ".$azione['id_action']." ORDER BY date_add ASC");
			
				if($oggetto == '') {
					$oggetto = Db::getInstance()->getValue("SELECT action_message FROM action_message WHERE id_action = ".$azione['id_action']." ORDER BY date_add ASC");
				} else { }
				$dataultimo = Db::getInstance()->getValue("SELECT date_add FROM action_message WHERE id_action = ".$azione['id_action']." ORDER BY date_add DESC");
			
			if(isset($_GET['id_action'])) {
			
				echo '<h2 style="display:block; float:left; margin-right:10px; color:red">ATTIVIT&Agrave; INTERNA '.$azione['id_action'].'</h2>';
			}
		}
		
		echo '<a class="button" style="display:block;float:left;margin-right:10px" href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&aprinuovaazione&token='.$this->token.'&tab-container-1=10">Apri nuova azione interna</a>
		
		&nbsp;&nbsp;&nbsp;<a class="button" style="display:block;float:left" href="http://www.ezdirect.it" target="_blank">Collegati al sito</a>
		<div class="clear:both"></div>
		<br /><br />';
		
		/*
		if(isset($_GET['error']) && $_GET['error'] == '1ac') {
			
			echo '<div style="width:100%; height:30px; border:1px solid red; color:red"> ERRORE: il messaggio non deve essere vuoto.</div>';
		}
		*/
		
		if(isset($_POST['cambiastatusa_listing'])) {

				
			Db::getInstance()->ExecuteS("UPDATE action_thread SET status = '".Tools::getValue('cambiastatusa_listing')."' WHERE id_action = '".addslashes(Tools::getValue('id_action'))."'");
			
			Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&token='.$this->token.'&conf=4&tab-container-1=10');
			
		}
		
		if(isset($_POST['cambiaincaricoa'])) {

			
			Customer::cambiaIncaricoVeloce(Tools::getValue('tipo'), Tools::getValue('cambiaincaricoa'), Tools::getValue('id_thread'), Tools::getValue('precedente_incarico'), Tools::getValue('id_ticket_univoco'), Tools::getValue('id_customer'));
	
			
			Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&token='.$this->token.'&conf=4&tab-container-1=10');
			
		}
		
		
		if(isset($_POST['submitAzione'])) {

			$action_date_array = explode("-",Tools::getValue('action_date'));
			$action_date = $action_date_array[2]."-".$action_date_array[1]."-".$action_date_array[0]." ".Tools::getValue('action_hours').":".Tools::getValue('action_minutes').":00";
		
			
					if(strpos(Tools::getValue('action_date'),'0000') !== false) {
						$action_date = Db::getInstance()->getValue("SELECT date_action FROM action_thread WHERE id_action = ".Tools::getValue('id_action')."");
					}
			
		
			Db::getInstance()->ExecuteS("UPDATE action_thread SET subject = '".addslashes(Tools::getValue('azione_subject'))."', action_type = '".addslashes(Tools::getValue('action_type'))."', action_date = '".$action_date."', status = '".Tools::getValue('cambiastatusa')."' WHERE id_action = '".addslashes(Tools::getValue('id_action'))."'");
			
			$impiegato_attuale = Db::getInstance()->getValue("SELECT action_to FROM action_thread WHERE id_action = '".Tools::getValue('id_action')."'");
			
			if($impiegato_attuale != Tools::getValue('assegnaimpiegatoa')) {
					
				Db::getInstance()->ExecuteS("UPDATE action_thread SET action_to = '".Tools::getValue('assegnaimpiegatoa')."' WHERE id_action = '".Tools::getValue('id_action')."'");
								
				$mailimp = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".Tools::getValue('assegnaimpiegatoa')."'");
								
				$tokenimp = Tools::getAdminToken("AdminCustomers"."2".Tools::getValue('assegnaimpiegatoa'));
								
				$linkimp = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewaction&id_action=".Tools::getValue('id_action')."&token=".$tokenimp.'&tab-container-1=10';
	
				$msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato un'azione su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirla</a>.";
								
				if($azione['action_from'] != 0) {
									
					$mailimprev = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".$azione['action_to']."'");
					$nomeimprev = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = '".Tools::getValue('assegnaimpiegatoa')."'");
									
					$msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione di un'azione su Ezdirect e l'ha passata a ".$nomeimprev.".";
									
					$params = array(
					'{link}' => $linkimp,
					'{firma}' => '',
					'{msg}' => $msgimprev);
									
					Mail::Send(5, 'msg_base', Mail::l('Gestione azione revocata', 5), 
						$params, $mailimprev, NULL, NULL, NULL, NULL, NULL, 
					_PS_MAIL_DIR_, true);
								
				}
								
				else {
								
				}
								
				$params = array(
				'{link}' => $linkimp,
				'{firma}' => '',
				'{msg}' => $msgimp);
								
				if($_POST['assegnaimpiegatoa'] != 0 && $_POST['assegnaimpiegato'] != $cookie->id_employee) {
								
					Mail::Send(5, 'msg_base', Mail::l('Azione assegnata a te su Ezdirect', 5), 
						$params, $mailimp, NULL, NULL, NULL, NULL, NULL, 
						_PS_MAIL_DIR_, true);
												
				} else { }
			}

			else {
			}
			
			Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&viewaction&id_action='.Tools::getValue('id_action').'&token='.$this->token.'&conf=4&tab-container-1=10');
			
		}
				
		

		if(!isset($_GET['viewaction'])) {
		
		if (sizeof($azioni))
			{
				
				echo '
				<table cellspacing="0" cellpadding="0" class="table">
					<tr>
						<th class="center">Tipo richiesta</th>
						<th class="center">'.$this->l('ID azione').'</th>
						<th class="center" style="width:80px">'.$this->l('Status').'</th>
						<th style="text-align:left">In carico</th>
						<th style="text-align:center">Q.ta com.</th>
						<th class="center">Ultima com.</th>
						<th class="center">'.$this->l('Aperto il').'</th>
						<th class="center">Modificato il</th>
						<th class="center">Azioni</th>
					</tr>';
				foreach ($azioni AS $azione) {
			
					switch($azione['status']) {
						case 'open': $status_azione = "<img style='margin-top:5px; display:block; float:left' src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; break;
						case 'closed': $status_azione = "<img style='margin-top:5px; display:block; float:left' src='../img/admin/status_green.gif' alt='Chiuso' title='Chiuso' />"; break;
						case 'pending1': $status_azione = "<img style='margin-top:5px; display:block; float:left' src='../img/admin/status_orange.gif' alt='In lavorazione' title='In lavorazione' />"; break;
						default: $status_azione = "<img style='margin-top:5px; display:block; float:left' src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; break;
					}
				
					$incaricatoa = new Employee($azione['action_to']);
					
					echo '<tr id="'.$azione['id_action'].'" class="actions_ticket_tr" rel="todo">';
					
					echo '<td>'.$azione['action_type'].'</td>';
					echo'	<td>'.$azione['id_action'].'</td>
						<td>'.$status_azione.'
						
						&nbsp;&nbsp;&nbsp;<form style="margin-left:5px; display:block;float:left" name="cambiastatoa_'.$azione['id_action'].'" id="cambiastatoa_'.$azione['id_action'].'" method="post" />
						
						<input type="hidden" name="id_customer" value="'.$azione['id_customer'].'" />
						<input type="hidden" name="id_action" value="'.$azione['id_action'].'" />
						<input type="hidden" name="back" value="1" />
						<select name="cambiastatusa_listing" onchange="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { document.forms[\'cambiastatoa_'.$azione['id_action'].'\'].submit(); } else { this.value=\''.$azione['status'].'\'}">
						<option style="background-image:url(../img/admin/status_green.gif); background-repeat:no-repeat;
						background-position:bottom left;
						padding-left:30px;"  value="closed" '.($azione['status'] == 'closed' ? 'selected="selected"' : '').'>C</option>
						<option style="background-image:url(../img/admin/status_orange.gif); background-repeat:no-repeat;
						background-position:bottom left;
						padding-left:30px;"  value="pending1" '.($azione['status'] == 'pending1' ? 'selected="selected"' : '').'>L</option>
						<option style="background-image:url(../img/admin/status_red.gif); background-repeat:no-repeat;
						background-position:bottom left;
						padding-left:30px;"  value="open" '.($azione['status'] == 'open' ? 'selected="selected"' : '').'>A</option>
						</select>
						</form></td><td><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&id_action='.$azione['id_action'].'&token='.$this->token.'&tab-container-1=10">';
					
					echo '
							<form style="margin-left:5px; display:block;float:left" name="cambiaincaricoaa_'.$azione['id_action'].'" id="cambiaincaricoaa_'.$azione['id_action'].'" action="" method="post" />
							<a id="'.$azione['id_customer_thread'].'"> </a>
							<input type="hidden" name="id_customer" value="'.$azione['id_customer'].'" />
							<input type="hidden" name="id_ticket_univoco" value="'.Customer::trovaSigla($azione['id_action'],'to-do').'" />
							<input type="hidden" name="back" value="1" />
							<input type="hidden" name="id_thread" value="'.$azione['id_action'].'" />
							<input type="hidden" name="precedente_incarico" value="'.$azione['action_to'].'" />
							<input type="hidden" name="tipo" value="to-do" />
							<select style="width:100px" name="cambiaincaricoa" onchange="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { document.forms[\'cambiaincaricoaa_'.$azione['id_action'].'\'].submit(); } else { this.value=\''.$azione['action_to'].'\'}">
							<option value="0" '.($azione['action_to'] == '0' ? 'selected="selected"' : '').'>--Nessuno--</option>
							<option value="1" '.($azione['action_to'] == '1' ? 'selected="selected"' : '').'>Ezio</option>
							<option value="2" '.($azione['action_to'] == '2' ? 'selected="selected"' : '').'>Barbara</option>
							<option value="5" '.($azione['action_to'] == '5' ? 'selected="selected"' : '').'>Paolo</option>
							<option value="4" '.($azione['action_to'] == '4' ? 'selected="selected"' : '').'>Massimo</option>
							<option value="3" '.($azione['action_to'] == '3' ? 'selected="selected"' : '').'>Riccardo</option>
							<option value="6" '.($azione['action_to'] == '6' ? 'selected="selected"' : '').'>Federico</option>
							<option value="7" '.($azione['action_to'] == '7' ? 'selected="selected"' : '').'>Matteo</option>
							</select>
							</form>'; 
					
					echo '</a></td>';
						
						$ultimomessa = Db::getInstance()->getValue("SELECT action_message FROM action_message WHERE id_action = ".$azione['id_action']." ORDER BY id_action_message DESC");
						$nummessa = Db::getInstance()->getValue("SELECT COUNT(id_action_message) FROM action_message WHERE id_action = ".$azione['id_action']."");
						$dataultimomessa = Db::getInstance()->getValue("SELECT date_add FROM action_message WHERE id_action = ".$azione['id_action']." ORDER BY id_action_message DESC");
						
						echo '<td style="text-align:center"><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&id_action='.$azione['id_action'].'&token='.$this->token.'&tab-container-1=10">'.$nummessa.'</a></td>';
						echo '<td><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&id_action='.$azione['id_action'].'&token='.$this->token.'&tab-container-1=10">'.substr(strip_tags(html_entity_decode($ultimomessa, ENT_NOQUOTES, 'UTF-8')), 0, 75).'...</a></td>
						<td><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&id_action='.$azione['id_action'].'&token='.$this->token.'&tab-container-1=10">'.Tools::displayDate($dataultimomessa, (int)($cookie->id_lang), false).'</a></td>';
						echo '<td><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&id_action='.$azione['id_action'].'&token='.$this->token.'&tab-container-1=10">'.Tools::displayDate($azione['date_add'], (int)($cookie->id_lang), false).'</a></td>';
						echo '<td><a href="index.php?tab=AdminCustomerThreads&id_customer_thread='.$azione['id_action'].'&del&typedel=to-do&token='.Tools::getAdminToken('AdminCustomerThreads'.(int)Tab::getIdFromClassName('AdminCustomerThreads').(int)$cookie->id_employee).'&backtocustomers='.$customer->id.'&tab-container-1=10" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }"><img src="../img/admin/delete.gif" alt="Cancella" /></a></td>';
			
						
					echo '</tr>';
				}
				echo '</table>';
				
				

				
				
				echo '<br />Clicca sull\'azione per aprirla, leggere i messaggi e rispondere.';
			}
			else
				echo $customer->firstname.' '.$customer->lastname.' '.$this->l('non ha alcuna azione.');
		}
		
		
		else {
		
			if (isset($_GET['filename'])) {
						$filename = $_GET['filename'];
						
						if(strpos($filename, ":::")) {
			
							$parti = explode(":::", $filename);
							$nomecodificato = $parti[0];
							$nomevero = $parti[1];
							
						}
			
						else {
							$nomecodificato = $filename;
							$nomevero = $filename;
			
						}
						
						if (file_exists(_PS_UPLOAD_DIR_.$nomecodificato)) {
							self::openUploadedFile();
						}
						else { }
					} 
					else { }
			
	
			if(isset($_GET['id_action'])) {
			
			
				echo '<div id="tab-container-azioni" style="margin-top:16px">
				
				
				
				<ul id="tab-container-azioni-nav" style="list-style-type:none; margin-top:-5px; margin-left:-20px">
				<li><a href="#azioni-1"><strong>Generale</strong></a></li>
				<li><a href="#azioni-2"><strong>Cronologia</strong></a></li>
				</ul>';
			}
			
			if(isset($_GET['id_action'])) {
				echo '
				
				<div class="yetii-azioni" id="azioni-1">
				<fieldset style="width:95%; background-color:#ffe8d4">';
				
				
				
				echo '
				<form id="modificaazione" action="'.$currentIndex.'&id_customer='.$_GET['id_customer'].'&submitAzione&viewcustomer&token='.$this->token.'&tab-container-1=10" method="post" autocomplete="off">';
				
				
				
				
				
				echo '<div class="anagrafica-cliente" style="width:800px; margin-bottom:10px">
				<strong>Oggetto</strong><br />
				
					<textarea name="azione_subject" id="azione_subjectContent" style="width:690px;height:40px;display:block;float:left">'.strip_tags(htmlspecialchars_decode($oggetto)).'</textarea>
					
					
				<br />
				</div>

				
				<div class="clear"></div>
				<div class="anagrafica-cliente">
				ID Azione<br />
				<span class="tab-span">'.$azione['id_action'].'</span><br />
				</div>
				
				<div class="anagrafica-cliente">
				In carico a<br />';
				echo '<select  style="width:180px" name="assegnaimpiegatoa" >';
				
				echo '<option value="0"'.($cookie->id_employee == 0 ? 'selected="selected"' : '').' >Da assegnare</option>';
				
				foreach ($impiegati_eza as $impez) {
				
					echo "<option value='".$impez['id_employee']."' ".($azione['action_to'] == $impez['id_employee'] ? 'selected="selected"' : '')." >".$impez['firstname']." ".$impez['lastname']."</option>";
				
				}

				echo '</select>
				';
				echo '</span>
				</div>
				
				
				<div class="anagrafica-cliente">
				Data apertura<br />
				<span class="tab-span">'.Tools::displayDate($azione['date_add'], (int)($cookie->id_lang), true).'</span><br />
				</div>
				
				
				
				<div class="clear"></div>
				<div class="anagrafica-cliente">
				Status<br />
				'.$status_azione.' <input type="hidden" name="id_customer" value="'.Tools::getValue('id_customer').'" />
				<input type="hidden" name="id_action" value="'.Tools::getValue('id_action').'" />
				<select name="cambiastatusa">
				<option value="closed" '.($azione['status'] == 'closed' ? 'selected="selected"' : '').'>Chiuso</option>
				<option value="pending1" '.($azione['status'] == 'pending1' ? 'selected="selected"' : '').'>In lavorazione</option>
				<option value="open" '.($azione['status'] == 'open' ? 'selected="selected"' : '').'>Aperto</option>
				</select><br />
				</div>
				<div class="anagrafica-cliente">
				Tipo azione<br />
				
				<select name="action_type" style="width:180px" id="action_type">
						<option '.($azione['action_type'] == "Attivita" ? 'selected = "selected"' : '').' value="Attivita">Attivit&agrave;</option>
						<option '.($azione['action_type'] == "Telefonata" ? 'selected = "selected"' : '').' value="Telefonata">Telefonata</option>
						<option '.($azione['action_type'] == "Visita" ? 'selected = "selected"' : '').' value="Visita">Visita</option>
						<option '.($azione['action_type'] == "Caso" ? 'selected = "selected"' : '').' value="Caso">Caso</option>
						<option '.($azione['action_type'] == "Intervento" ? 'selected = "selected"' : '').' value="Intervento">Intervento</option>
						<option '.($azione['action_type'] == "Richiesta_RMA" ? 'selected = "selected"' : '').' value="Richiesta_RMA">Richiesta RMA</option>
						</select>
				
				
				
				</div>
				
				
				
				<div class="anagrafica-cliente">
				Data ultima com.<br />
				<span class="tab-span">'.Tools::displayDate($dataultimo, (int)($cookie->id_lang), true).'</span><br />
				</div>
				
				<div class="clear"></div>';
				
				includeDatepicker3(array('action_date'), true);
				
				echo '<div class="anagrafica-cliente">
				Data azione<br />
				<input type="text" id="action_date" style="width:100px" name="action_date" value="'.($azione['action_date'] == '0000-00-00 00:00:00' ? 'Nessuna data' : date("d-m-Y", strtotime($azione['action_date']))).'" />
				alle ore <input type="text" name="action_hours" maxlength="2" style="width:16px" value="'.date('H',strtotime($azione['action_date'])).'" />:
				<input type="text" name="action_minutes" maxlength="2" style="width:16px" value="'.date('i',strtotime($azione['action_date'])).'"  />
				
				<br /><br />
				</div>
				
				<div class="clear"></div>';
			
				
				echo '<input type="submit" value="'.$this->l('   Save   ').'" name="submitAzione" class="button" />';
				
		echo '</form>
					</fieldset><br /><br /></div>';
			
			}

		
			
			if(isset($_GET['id_action'])) {
				echo '<div class="yetii-azioni" id="azioni-2">';
			}
			
			$messaggia = Db::getInstance()->ExecuteS("SELECT * FROM action_message am WHERE am.id_action = ".$_GET['id_action']."");
			
			foreach($messaggia as $messaggioa) {
			
				$employee_from = new Employee($messaggioa['action_m_from']);
				$employee_to = new Employee($messaggioa['action_m_to']);
				
				echo "<table style='width:100%; 
				border:1px solid #dfd5c3; background-color: #ffecf2;'margin-bottom:15px'>";
				echo '<tr>
					<td style="colspan:2">
						<table>
				
						<td style="font-size:14px; width:250px">Da: <strong>'.$employee_from->firstname." ".$employee_from->lastname.'<strong></td>
						<td style="width:170px"><strong>Data: </strong>'.$messaggioa['date_add'].'</td>
						<td style="width:180px"><strong>Allegato: </strong>';
						if(!empty($messaggioa['file_name'])) {
						
							$allegatia = explode(";",$messaggioa['file_name']);
							$nalla = 1;
							foreach($allegatia as $allegatoa) {
								if(strpos($allegatoa, ":::")) {
					
									$parti = explode(":::", $allegatoa);
									$nomeveroa = $parti[1];
									
								}
					
								else {
									$nomeveroa = $allegatoa;
					
								}
								if($allegatoa == "") { } else {
									if($nalla == 1) { } else { echo " - "; }
									echo '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&id_action='.$azione['id_action'].'&token='.$this->token.'&tab-container-1=10&filename='.$allegatoa.'"><span style="color:red">'.$nomeveroa.'</span></a>';
									$nallp++;
								}
							}
						
						}
						
						else { echo 'Nessun allegato'; }
						
						
						echo '</td>';
						
						$cc = Db::getInstance()->getValue("SELECT cc FROM ticket_cc WHERE type = 'a' AND msg = ".$messaggioa['id_action_message']."");
						if(!empty($cc)) {
						
							echo '<td>';
						
							$ccs = explode(";",$cc);
							$cc_str = "<strong>CC:</strong> ";
							
							foreach ($ccs as $conoscenza) {
							
								$imp = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".$conoscenza."");
								$cc_str .= $imp." ";
							}
							
							echo $cc_str;
							echo "</td>";
						
						}
						else {
						
							echo '<td><strong>CC:</strong> Nessuno</td>';
						
						}
						
						
						echo '</tr></table>
						
					</td>
					</tr>
					<tr>
					<td style="colspan:2">
						<table>
						<tr>
						<td style="width:100px"><strong>Messaggio</strong></td>
						<td>'.htmlspecialchars_decode($messaggioa['action_message']).'
						<form method="post">
						<input type="hidden" name="modifica_messaggio_azione" value="'.htmlentities($messaggioa['action_message'], ENT_QUOTES | ENT_SUBSTITUTE, "UTF-8").'" />
						<input type="hidden" name="modifica_messaggio_azione_id" value="'.$messaggioa['id_action_message'].'" />
						<input type="submit" name="modifica_messaggio_submit" value="Modifica" class="button" />
						</form>
						</td>
						</tr>
						</table>
					</td>';
					echo '</tr>
					<tr><td style="colspan:2">
						<table>
						<tr>
						<td style="width:100px"><strong>Inviato a</strong></td>
						<td><strong>'.$employee_to->firstname." ".$employee_to->lastname.'<strong></td>
						</tr>
						</table>
					</td></tr>
					</table><br />';
			}
			
			
		
		if (Tools::isSubmit('submitActionReply'))
			{
			
				/*if(Tools::getValue('messaggioa') == '') {
			
			
					Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&error=1ac&token='.$this->token.'&tab-container-1=10');
							
			
				}*/
				
				$files=array();
					$fdata=$_FILES['joinFile'];
					if(is_array($fdata['name'])){
						for($i=0;$i<count($fdata['name']);++$i){
							$files[]=array(
								'name'    =>$fdata['name'][$i],
								'tmp_name'=>$fdata['tmp_name'][$i],
								'type' => $fdata['type'][$i],
								'size' => $fdata['size'][$i],
								'error' => $fdata['error'][$i],
							 
							);
						}
					}
					else $files[]=$fdata;
										
					$attachments = array();
					
					
					foreach($files as $file) {
					
						if (isset($files) AND !empty($file['name']) AND $file['error'] != 0)
							$this->_errors[] = Tools::displayError('An error occurred with the file upload.');
							
							if (!empty($file['name']))
								{
									$extension = array('.txt', '.rtf', '.doc', '.docx', '.pdf', '.zip', '.png', '.jpeg', '.gif', '.jpg');
									$filename = md5(uniqid().substr($file['name'], -5));
									$fileAttachment['content'] = file_get_contents($file['tmp_name']);
									$fileAttachment['name'] = $file['name'];
									$fileAttachment['mime'] = $file['type'];
								}
								
								if (isset($filename) AND rename($file['tmp_name'], _PS_MODULE_DIR_.'../upload/'.$filename)) {
									$filename = $filename.":::".$file['name'];
									$file_name .= $filename.";";
							}
							
							$attachments[] = $fileAttachment;
							
					}					
				
				if(Tools::getValue('nuovaazione') == 1) {
				
					$ultimothread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
					$thread = $ultimothread+1;
					
					$cstmr = Db::getInstance()->getRow("SELECT company, firstname, lastname, is_company FROM customer WHERE id_customer = ".(int)($customer->id)."");
						
					if($cstmr['is_company'] == 1) { $cstmr_r = $cstmr['company']; } else { $cstmr_r = $cstmr['firstname']." ".$cstmr['lastname']; }
						
					$subject = "*** AZIONE su cliente ".$cstmr_r." - Tipo azione: ".Tools::getValue('action_type')." ***";
					
					$action_date_array = explode("-",Tools::getValue('action_date'));
					
					$action_date = $action_date_array[2]."-".$action_date_array[1]."-".$action_date_array[0]." ".Tools::getValue('action_hours').":".Tools::getValue('action_minutes').":00";
					
					if(strpos(Tools::getValue('action_date'),'0000') !== false) {
						$action_date = date("Y-m-d H:i:s");
					}

					
					Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, action_date, status, date_add, date_upd) VALUES ('$thread', '".Tools::getValue('action_type')."', '".$customer->id."', '".addslashes($subject)."', '".$cookie->id_employee."', '".Tools::getValue('in_carico_a_a')."', '".$action_date."', '".addslashes(Tools::getValue('statusa'))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");   
					
					Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, action_message, file_name, date_add, date_upd) VALUES (NULL, '$thread', ".$customer->id.", '".$cookie->id_employee."', '".Tools::getValue('in_carico_a_a')."', '".addslashes(Tools::getValue('messaggioa'))."', '".addslashes($file_name)."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
					
					$idmessaggio = mysql_insert_id();
				}
				
				else {
					
					$thread = $azione['id_action'];
				
					Db::getInstance()->ExecuteS("UPDATE action_thread SET  status = '".addslashes(Tools::getValue('statusa'))."', date_upd = '".date("Y-m-d H:i:s")."', action_to = '".Tools::getValue('in_carico_a_a')."' WHERE id_action = '".$azione['id_action']."'");  
				
					if(isset($_POST['modifica_messaggio_azione_id'])) {
						Db::getInstance()->ExecuteS("UPDATE action_message SET action_message = '".addslashes(Tools::getValue('messaggioa'))."' WHERE id_action_message = ".$_POST['modifica_messaggio_azione_id'].""); 
						$idmessaggio = $_POST['modifica_messaggio_azione_id'];
					}
					else {
						Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, action_message, file_name, date_add, date_upd) VALUES (NULL, '$thread', ".$customer->id.", '".$cookie->id_employee."', '".Tools::getValue('in_carico_a_a')."', '".addslashes(Tools::getValue('messaggioa'))."', '".addslashes($file_name)."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')"); 
						$idmessaggio = mysql_insert_id();
					}
					
				}
				
					$file_csv=fopen("../cms/quotazioni/azioni.csv","a+");
					
					$in_carico_a =  Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".Tools::getValue('in_carico_a_a')."");
					
					$prima_azione = Db::getInstance()->getValue("SELECT id_action_message FROM action_message WHERE id_action = $thread ORDER BY id_action_message ASC");
						
					if(!$prima_azione) {
									
						$prima_azione = $idmessaggio;
								
					}
								
					else {
								
					}
					
					$messaggiopercsv = Tools::getValue('messaggioa');
					$messaggiopercsv = strip_tags($messaggiopercsv);
					$messaggiopercsv = str_replace(";", ",", $messaggiopercsv);
						
					$messaggiopercsv = str_replace(chr(10), " ", $messaggiopercsv); //remove carriage returns
					$messaggiopercsv = str_replace(chr(13), " ", $messaggiopercsv); //remove carriage returns	
					$ref = rand(0, 99999); $ref2 = rand(0, 99999);
					
					$riga_richiesta = "".date("d/m/Y H:i:s").";".date('Ymd')."-".$ref."-".$ref2.";".$in_carico_a.";".Tools::getValue('action_type').";".Tools::getValue('statusa').";".$messaggiopercsv.";".$thread.";".$idmessaggio.";".$prima_azione.";".$customer->id."\n";
						
								
					
					
					
					$subj_per_ctrl = Db::getInstance()->getValue("SELECT subject FROM action_thread WHERE id_action = ".$thread."");
					
					$pos = strpos($subj_per_ctrl,"ti richiamiamo noi");
					
					//if (strlen(stristr($subj_per_ctrl,"ti richiamiamo noi"))>0) {
					@fwrite($file_csv,$riga_richiesta);
					//} else {
						
					//}
					
					@fclose("../cms/quotazioni/azioni.csv","a+");	
				
					$mail_verso_cui_inviare = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = ".Tools::getValue('in_carico_a_a')."");
						
						
					$tokenimp = Tools::getAdminToken("AdminCustomers"."2".Tools::getValue('in_carico_a_a'));
					$linkimp = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewaction&id_action=".$thread."&token=".$tokenimp.'&tab-container-1=10';			
						
					if((Tools::getValue('messaggioa')) == '') {
						$testo_azione = "Il testo di questa azione &egrave; vuoto.";
					}
					else {
						$testo_azione = "Testo azione: ".(Tools::getValue('messaggioa'));
					}
					
					if($customer->is_company == 1) {
					
						$cliente = $customer->company;
					
					}
					else {
					
						$cliente = $customer->firstname." ".$customer->lastname;
					}
						
					$params = array(
					'{reply}' => "Ti &egrave; stata inviata una azione su Ezdirect.<br /><br />Azione aperta da: <strong>".$employeemess->firstname."</strong><br /><br />Cliente: <strong>".$cliente."</strong><br /><br />Azione in carico a: <strong>".$in_carico_a."</strong><br /><br />Tipo azione:<strong>".Tools::getValue('action_type')."</strong><br /><br />".$testo_azione."<br /><br />Per leggere questa azione o per rispondere, <a href='".$linkimp."'>clicca su questo link</a>.");
					
					/*
					if (Mail::Send((int)$cookie->id_lang, 'action', Mail::l('Aperta azione su Ezdirect', (int)$cookie->id_lang), 
						$params, $mail_verso_cui_inviare, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
						_PS_MAIL_DIR_, true))
					{
						//$ct->status = 'closed';
						//$ct->update();
					}
					*/
					$cc = "";
					
					foreach($_POST['conoscenza_a'] as $conoscenza) {
					
						$cc .= $conoscenza.";";
						$tokenimp = Tools::getAdminToken("AdminCustomers"."2".$conoscenza);
						$linkimp = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewaction&id_action=".$thread."&token=".$tokenimp.'&tab-container-1=10';			
					
						echo $conoscenza."<br />";
					
						if((Tools::getValue('messaggioa')) == '') {
							$testo_azione = "Il testo di questa azione &egrave; vuoto.";
						}
						else {
							$testo_azione = "Testo azione: ".(Tools::getValue('messaggioa'));
						}
									
						$params = array(
						'{reply}' => "Ti &egrave; stata inviata una azione in copia conoscenza su Ezdirect.<br /><br />
						Azione aperta da: <strong>".$employeemess->firstname."</strong><br /><br />
						Azione in carico a: <strong>".$in_carico_a."</strong><br /><br />
						Tipo azione: <strong>".Tools::getValue('action_type')."</strong><br /><br />".$testo_azione."<br /><br />Per aprire questa azione, <a href='".$linkimp."'>clicca su questo link</a>.");
					
						$mailconoscenza = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = $conoscenza");
								
						Mail::Send((int)$cookie->id_lang, 'senzagrafica', Mail::l('Azione in copia conoscenza', (int)$cookie->id_lang), 
						$params, $mailconoscenza, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
						_PS_MAIL_DIR_, true);
							
					}
					
					if(!empty($_POST['conoscenza_a'])) {
							
						Db::getInstance()->executeS("INSERT INTO ticket_cc (msg, type, cc) VALUES (".$idmessaggio.", 'a', '".$cc."')"); 
							
							
					}
					
					if(Tools::getValue('in_carico_a_a') != 0) {

					
						$mail_incarico_a = Db::getInstance()->getValue('SELECT email FROM employee WHERE id_employee = '.Tools::getValue('in_carico_a_a').'');
						
						if(Tools::getValue('in_carico_a_a') == $cookie->id_employee) {
						}
						else {
							Mail::Send(5, 'action', Mail::l('Azione assegnata a te su Ezdirect', 5), 
							$params, $mail_incarico_a, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
							_PS_MAIL_DIR_, true);
						}
					}
					
					Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=10');
					
		
			}
		
		
			else {
			
				if (isset($_GET['actionreply'])) {
			
				echo 'Risposta inviata con successo.<br /> <a class="button" href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&id_action='.$azione['id_action'].'&token='.$this->token.'&tab-container-1=10"><strong>Clicca qui se vuoi rispondere nuovamente.</strong></a><br /><br />';
				
				}
				
				else if (isset($_GET['actionreply2'])) {
			
				echo 'Azione aperta con successo. <br /><br />';
				
				}
			
				else if (!isset($_GET['actionreply']) || isset($_GET['aprinuovaazione'])) {
				$iso = Language::getIsoById((int)($cookie->id_lang));
				$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
				$ad = dirname($_SERVER["PHP_SELF"]);
		
		
			if(isset($_GET['aprinuovaazione'])) {
			
				echo '<h2>Apri nuova azione</h2>';
			
			
			}
		
			else {
			
				echo '<h2 id="rispondi-azione">Rispondi a questa azione</h2>';
				if(isset($_POST['modifica_messaggio_azione'])) {
					echo '<script type="text/javascript">
						 window.location = "#rispondi-azione";
					</script>';
				}
				
			}
		
		
		
				echo '
			<script type="text/javascript">
			var iso = \''.$isoTinyMCE.'\' ;
			var pathCSS = \''._THEME_CSS_DIR_.'\' ;
			var ad = \''.$ad.'\' ;
			</script>
			
			
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce3/tiny_mce.js"></script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>
			<script type="text/javascript">
				$(document).ready(function () {
				
dataChanged = 0;     // global variable flags unsaved changes      

function bindForChange(){    
     $(\'input,checkbox,text,textarea,radio,select\').bind(\'change\',function(event) { dataChanged = 1});
	 $(\'#messaggiop\').bind(\'input propertychange\', function() { dataChanged = 1 });
     $(\':reset,:submit\').bind(\'click\',function(event) { dataChanged = 0 });
}


function askConfirm(){  
    if (dataChanged){ 
        return "You have some unsaved changes.  Press OK to continue without saving." 
    }
}

window.onbeforeunload = askConfirm;
window.onload = bindForChange;
});
			</script>
			<script type="text/javascript">
					toggleVirtualProduct(getE(\'is_virtual_good\'));
					unitPriceWithTax(\'unit\');
			</script>';
			$employeemessa = new Employee($cookie->id_employee);
				echo'	<form name="submit_azione" id="submit_azione" action="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&id_action='.$azione['id_action'].'&token='.$this->token.'&tab-container-1=10" method="post" class="std" enctype="multipart/form-data">
				<fieldset style="background-color:#ffe8d4">';
		
				
				if(isset($_GET['aprinuovaazione'])) {
			
				echo '<input type="hidden" name="nuovaazione" value="1" />';
			
				}
				
				else {
				
					echo '			<script type="text/javascript">
					
						$(document).ready(function () {
							
							$("#submit_preventivo").submit(function () {
								d = document.getElementById("statusa").value;
								
								if(d != "'.$azione['status'].'") {
									
										}
								
								else {
									var salvamodifiche = window.confirm("STATUS: OK per procedere, ANNULLA per cambiare."); 
							
										if (salvamodifiche) {
											submitFormOkay = true;
											
		$(\'#waiting1\').waiting({ 
		elements: 10, 
		auto: true 
		});
		var overlay = jQuery(\'<div id="overlay"></div>\');
		var attendere = jQuery(\'<div id="attendere">ATTENDERE PREGO</div>\');
		overlay.appendTo(document.body);
		attendere.appendTo(document.body);
											$("#submitMessage").trigger("click");
										
										} else {
											 return false;
												
											//niente
										}
								}
							});
							
							
						});
			
					</script>';	
					
				}
				
				
				
					echo '<table>
					';
					
					
							
					
					if(isset($_GET['aprinuovaazione'])) {
					
						echo '<tr><td>Tipo azione</td>
						<td>
						<select name="action_type" id="action_type">
						<option value="Attivita">Attivit&agrave;</option>
						<option value="Telefonata">Telefonata</option>
						<option value="Visita">Visita</option>
						<option value="Caso">Caso</option>
						<option value="Intervento">Intervento</option>
						<option value="Richiesta_RMA">Richiesta RMA</option>
						</select>
						</td>
						</tr>';
						
					} else {
					
						echo '<input type="hidden" name="action_type" value="'.($azione['action_type']).'" />';
					
					}
					
				echo '<tr>
								<td>
								';
								
							$customer_phone = Db::getInstance()->getRow("SELECT	phone, phone_mobile FROM address WHERE id_customer = ".$customer->id." AND deleted = 0 AND fatturazione = 1 AND active = 1");
							
							if($customer_phone['phone'] == '') { $telefono_cliente = $customer_phone['phone_mobile']; } else { $telefono_cliente = $customer_phone['phone']; }
								
								echo '
								<input type="hidden" name="id_customer" value="'.$customer->id.'" />
								
								<input type="hidden" value="'.$thread['id_customer_thread'].'" name="id_customer_thread">
								</td>
							</tr>
					
							';
							
						
						echo '
						<tr><td>Stato azione</td>
						<td>
						<div style="float:left">
						<select name="statusa" id="statusa">
						<option value="open" '.($azione['status'] == 'open' ? 'selected="selected"' : '').'>Aperto   </option>
						<option value="pending1" '.($azione['status'] == 'pending1' ? 'selected="selected"' : '').'>In lavorazione   </option>
						<option value="closed" '.($azione['status'] == 'closed' ? 'selected="selected"' : '').'>Chiuso   </option>
						
						</select>
							</div><div style="float:left; margin-left:20px">
							In carico a';
					echo '
									
				<script type="text/javascript">
								function changeInCaricoAA()
								{';
								
								
								foreach ($impiegati_eza as $impez) {
									echo '
									
									firstVar = document.getElementById("in_carico_a_a").value;
									
									if (document.getElementById("in_carico_a_a").value == "'.$impez['id_employee'].'") {
									
									
									document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').disabled = true;
									document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').checked = false;
									
									} else { document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').disabled = false; }
								';
								}
							echo '
								}
								</script>
				<select style="width:250px" name="in_carico_a_a" id="in_carico_a_a" onChange = "changeInCaricoAA();">';

				
				foreach ($impiegati_eza as $impezp) {

				
					if(isset($_GET['aprinuovaazione'])) {
						echo "<option value='".$impezp['id_employee']."' ".($cookie->id_employee == $impezp['id_employee'] ? 'selected="selected"' : '')." >".$impezp['firstname']."</option>";
					}
					else {
						echo "<option value='".$impezp['id_employee']."' ".($azione['action_to'] == $impezp['id_employee'] ? 'selected="selected"' : '')." >".$impezp['firstname']."</option>";
					}
				}

				echo '</select></div></td>
							</tr>
							<tr><td>Conoscenza</td>
							<td>
							';
							
							foreach ($impiegati_eza as $impez) {
		
								echo "<input type='checkbox' name='conoscenza_a[]' id ='conoscenza_".$impez['id_employee']."' value='".$impez['id_employee']."' /> ".$impez['firstname']."  ";
							
							}
							echo '<br /></td>
							</tr>
						<tr><td><br /></td></tr>
						';
						if(isset($_GET['aprinuovaazione'])) {
							includeDatepicker3(array('action_date'), true);
							
							echo '<tr><td>Data azione</td>
							<td><input type="text" id="action_date" style="width:100px" name="action_date" value="'.($azione['action_date'] == '0000-00-00 00:00:00' ? 'Nessuna data' : date("d-m-Y")).'" />
				alle ore <input type="text" name="action_hours" maxlength="2" style="width:16px" value="'.date('H',strtotime($azione['action_date'])).'" />:
				<input type="text" name="action_minutes" maxlength="2" style="width:16px" value="'.date('i',strtotime($azione['action_date'])).'"  />
				</td>
							</tr>
							';
						} else { }
						
						echo '<tr><td><br /></td></tr>';
						echo '<tr><td>Allega file</td>
						<td>
						<input name="MAX_FILE_SIZE" value="20000000" type="hidden">
						<script type="text/javascript" src="jquery.MultiFile.js"></script>
						<input name="joinFile[]" class="multi" id="joinFile" type="file" onchange="handleFileSelect(this.files)">
							<output id="list-tkt">Anteprime: </output>
								<script type="text/javascript">
							function handleFileSelect(files) {
							
								// Loop through the FileList and render image files as thumbnails.
								 for (var i = 0; i < files.length; i++) {
									var f = files[i];
									var name = files[i].name;
          
									var reader = new FileReader();  
									reader.onload = function(e) {  
									  // Render thumbnail.
									  var span = document.createElement("span");
									  span.innerHTML = ["<a href=\'",e.target.result,"\' target=\'_blank\'>",name,"</a> - "].join("");
									  document.getElementById("list-tkt").insertBefore(span, null);
									};

								  // Read in the image file as a data URL.
								  reader.readAsDataURL(f);
								}
							  }
							 
							</script>
						</td>
						</tr>
						<tr><td>Messaggio</td>
						<td>
					
						<textarea style="width:760px" class="rte" id="messaggioa" name="messaggioa" rows="5" cols="40">
						'.(isset($_POST['modifica_messaggio_submit']) ? stripslashes($_POST['modifica_messaggio_azione']) : '').'
						</textarea>';
						
						if(isset($_POST['modifica_messaggio_submit'])) {
						
							echo "<input type='hidden' name='modifica_messaggio_azione_id' value='".$_POST['modifica_messaggio_azione_id']."' />";
						}
						
						//  onclick = "this.style.visibility=\'hidden\', loading.style.visibility=\'visible\'"
						echo '</td>
						</tr>
						<tr><td></td>
						<td><input name="submitActionReply" id="submitActionReply" value="Conferma azione" class="button_invio" type="submit"></td>
			
						</tr>
					</table>
				</fieldset>
			</form>
		';
		if(isset($_GET['id_action'])) {
		echo '</div>';	
		
		}
		
		
		}
		
		}
		
			
		}
		
		
		
		echo '<br /><br /><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=10" class="button">Torna alla lista delle azioni</a>';
		echo '</div>';
		}
		else { }
		// FINE AZIONI!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 11) {
		// PERSONEEEEEEEEEEEEEEEEEEEEEEEE
		echo '<div class="tab1" id="persone-customer">';
		
		echo '<h2>Persone</h2>';
		
		if(isset($_POST['cancella_persona'])) {
		
			Db::getInstance()->getValue("DELETE FROM persone WHERE id_persona = ".$_POST['id_persona']."");
		
		}
		
		if(isset($_POST['submitpersone'])) {
		
			foreach ($_POST['persona_nuovo'] as $id_persona=>$persona_id) {
		
				if($_POST['persona_nuovo'][$id_persona] == 0) {
					Db::getInstance()->executeS("UPDATE persone SET firstname = '".$_POST['persona_firstname'][$id_persona]."', lastname = '".$_POST['persona_lastname'][$id_persona]."', role = '".$_POST['persona_role'][$id_persona]."', phone = '".$_POST['persona_phone'][$id_persona]."', phone_2 = '".$_POST['persona_phone_2'][$id_persona]."', phone_mobile = '".$_POST['persona_phone_mobile'][$id_persona]."', email = '".$_POST['persona_email'][$id_persona]."' WHERE id_persona = ".$id_persona.""); 
				}
				
				else {
					
					Db::getInstance()->executeS("INSERT INTO persone
					(id_persona,
					firstname,
					lastname,
					role,
					email,
					email_aziendale,
					phone,
					phone_2,
					phone_mobile,
					id_customer,
					tax_code,
					vat_number)
					
					VALUES (
					NULL,
					'".addslashes($_POST['persona_firstname'][$id_persona])."',
					'".addslashes($_POST['persona_lastname'][$id_persona])."',
					'".addslashes($_POST['persona_role'][$id_persona])."',
					'".addslashes($_POST['persona_email'][$id_persona])."',
					'".addslashes(trim($customer->email))."',
					'".addslashes($_POST['persona_phone'][$id_persona])."',
					'".addslashes($_POST['persona_phone_2'][$id_persona])."',
					'".addslashes($_POST['persona_phone_mobile'][$id_persona])."',
					'".$customer->id."',
					'".addslashes(trim($customer->tax_code))."',
					'".addslashes(trim($customer->vat_number))."'
					)");
					
				}
			}
		}
		
		$conta_persone = Db::getInstance()->getValue("SELECT count(*) FROM persone WHERE vat_number = '".$customer->vat_number."'");
	
		if($conta_persone == 0) {
		
			echo "Non ci sono persone abbinate a questo cliente<br /><br />";
		
		}
		
		else {
		}
		
		echo '<form action="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&modificapersone&token='.$this->token.'&tab-container-1=11" method="post">';
		echo '<script type="text/javascript">
		function add_persone() {
			var possible = "0123456789";
			var randomid = "";
			for( var i=0; i < 11; i++ ) {
				randomid += possible.charAt(Math.floor(Math.random() * possible.length));
			}
					
			$("<tr id=\'tr_persona_"+randomid+"\'><td><input type=\'hidden\' name=\'persona_nuovo[]\' value=\'1\' /></td><td><input type=\'text\' size=\'15\' name=\'persona_firstname[]\' /></td><td><input type=\'text\' size=\'15\' name=\'persona_lastname[]\' /></td><td><input type=\'text\' size=\'20\' name=\'persona_role[]\' /></td><td><input type=\'text\' size=\'10\' name=\'persona_phone[]\' /></td><td><input type=\'text\' size=\'10\' name=\'persona_phone_2[]\' /></td><td><input type=\'text\' size=\'10\' name=\'persona_phone_mobile[]\' /></td><td><input type=\'text\' size=\'10\' name=\'persona_email[]\' /></td><td><a href=\'#\' onclick=\'togli_riga("+randomid+");\'><img src=\'../img/admin/delete.gif\' alt=\'Cancella\' title=\'Cancella\' style=\'cursor:pointer\' /></a></td></tr>").appendTo("#tablePersoneBody");

		}
			
		function togli_riga(id) {
			document.getElementById(\'tr_persona_\'+id).innerHTML = "";
		}
			
			
		function cancella_persona(id) {
			$.ajax({
				  url:"'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&cancellapersone&token='.$this->token.'",
				  type: "POST",
				  data: { 
				  cancella_persona: \'y\',
				  id_persona: id
				  },
				  success:function(){
						 
				  },
				  error: function(xhr,stato,errori){
					 alert("Errore nella cancellazione:"+xhr.status);
				  }
			});
		
		}
			
			
		</script>
		';
		
		
			
			
		echo '<table class="table" style="width:900px" id="tablePersone">';
		echo '<thead>';
		echo '<tr>';
		echo '<th style="width:50px">ID pers.</th><th style="width:180px">Nome</th><th style="width:180px">Cognome</th><th style="width:180px">Ruolo</th><th style="width:70px">Telefono 1</th><th style="width:70px">Telefono 2</th><th style="width:70px">Cellulare</th><th style="width:150px">Email</th><th></th><th></th>';
		echo '</tr>';
		echo '</thead>';
		echo '<tbody id="tablePersoneBody">';
		
		
		$persone = Db::getInstance()->executeS("SELECT * FROM persone WHERE id_customer = '".$_GET['id_customer']."'");
			
		foreach($persone as $persona) {
			
			echo '<tr id="tr_persona_'.$persona['id_persona'].'">';
			echo '<td><input type="hidden" name="persona_nuovo['.$persona['id_persona'].']" value="0" /> '.$persona['id_persona'].'</td>';
			echo '<td><input type="text" size="15" name="persona_firstname['.$persona['id_persona'].']" value="'.$persona['firstname'].'" /></td>';
			echo '<td><input type="text" size="15" name="persona_lastname['.$persona['id_persona'].']" value="'.$persona['lastname'].'" /></td>';
			echo '<td><input type="text" size="20" name="persona_role['.$persona['id_persona'].']" value="'.$persona['role'].'" /></td>';
			echo '<td><input type="text" size="10" name="persona_phone['.$persona['id_persona'].']" value="'.$persona['phone'].'" /></td>';
			echo '<td><input type="text" size="10" name="persona_phone_2['.$persona['id_persona'].']" value="'.$persona['phone_2'].'" /></td>';
			echo '<td><input type="text" size="10" name="persona_phone_mobile['.$persona['id_persona'].']" value="'.$persona['phone_mobile'].'" /></td>';
			echo '<td><input type="text" size="10" name="persona_email['.$persona['id_persona'].']" value="'.$persona['email'].'" /></td>';
			echo '<td>'.($persona['email'] != '' ? '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewmessage&aprinuovomessaggio&id_persona='.$persona['id_persona'].'&token='.$this->token.'&tab-container-1=7"><img src="../img/admin/email.gif" alt="Cancella" title="Cancella" style="cursor:pointer" /></a>' : '').'</td>';
			echo '<td><a href="#" onclick="javascript:var surec=window.confirm(\'Sei sicuro?\'); if (surec) { togli_riga('.$persona['id_persona'].'); cancella_persona('.$persona['id_persona'].'); } else { return false; }; "><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" style="cursor:pointer" /></a></td>';
			echo '</tr>';
				
			
		}

		echo '</tbody>';
		echo "</table>";
		echo '<br />
		<a href="#" onclick="javascript:add_persone()";><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" /> Clicca per aggiungere una persona</a> <br /><br />
			<input type="submit" value="Salva" name="submitpersone" class="button" style="cursor:pointer" />';
		echo "</form>";
		
		
		
		
		
		
		echo '</div>';
		} 
		
		
		
		
		
		
		
		else { }
		
		if(Tools::getIsset('cancellapersone') && Tools::getValue('cancella_persona') == 'y') {
			Db::getInstance()->executeS("DELETE FROM persone WHERE id_persona = ".Tools::getValue('id_persona')."");
		
		}
		// FINE PERSONEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 12) {
			// DOCUMENTI
			
			
			echo '<div class="tab1" id="documenti-customer">';
			
			
			
			
			if(Tools::getIsset('createdocsdir')) {
				$old_umask = umask(0);
				
				if($customer->is_company == 1) {
					$nuova_cartella_documenti = $this->sanitize($customer->company." ".$customer->id);
				}
				else {
					$nuova_cartella_documenti = $this->sanitize($customer->firstname." ".$customer->lastname." ".$customer->id);
				}
				
				Db::getInstance()->executeS("UPDATE customer SET cartella_documenti = '".$nuova_cartella_documenti."' WHERE id_customer = ".$customer->id."");
				mkdir('../documenti-clienti/'.$nuova_cartella_documenti,0777);
				
				umask($old_umask);
				echo '<div class="conf confirm">Cartella documenti creata con successo!</div>
				';
			}
			
			if(Tools::getIsset('delete_documento')) {
			
				foreach($_POST['selectdocfile'] as $selectedfile) {
				
					if(Tools::getIsset('current_dir')) {
						$entire_dir = Tools::getValue('current_dir');
					}
					else {
						$entire_dir = $directory_base;
					}
					
					if(is_file($entire_dir.'/'.$selectedfile)) {
					
						unlink($entire_dir.'/'.$selectedfile);
					}
					else if(is_dir($entire_dir.'/'.$selectedfile)) {
						$this->deleteDir($entire_dir.'/'.$selectedfile);
					}
				}
			
				echo '<div class="conf confirm">Documenti eliminati con successo</div>';
				
				Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=12&gotodir='.Tools::getValue('current_dir').'');
			
			}
			
			else if(Tools::getIsset('move_documento') || Tools::getIsset('copy_documento') ) {
			
				foreach($_POST['selectdocfile'] as $selectedfile) {
				
					if(Tools::getIsset('current_dir')) {
						$entire_dir = Tools::getValue('current_dir');
					}
					else {
						$entire_dir = $directory_base;
					}
					
					if(Tools::getIsset('move_documento')) {
					rename($entire_dir.'/'.$selectedfile, Tools::getValue('sposta_in')."/".$selectedfile);
					}
					else if(Tools::getIsset('copy_documento')) {
					copy($entire_dir.'/'.$selectedfile, Tools::getValue('sposta_in')."/".$selectedfile);
					}
				}
			
				echo '<div class="conf confirm">Documenti eliminati con successo</div>';
				
				Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=12&gotodir='.Tools::getValue('current_dir').'');
			
			}
			
			
			
			if(Tools::getIsset('formdocumentssubmitted')) {
			
				
				if(!empty($_FILES['aggiungiFile'])) {
				
					$docfiles=array();
					$docfdata=$_FILES['aggiungiFile'];
					if(is_array($docfdata['name'])){
						for($i=0;$i<count($docfdata['name']);++$i){
							$docfiles[]=array(
								'name'    =>$docfdata['name'][$i],
								'tmp_name'=>$docfdata['tmp_name'][$i],
								'type' => $docfdata['type'][$i],
								'size' => $docfdata['size'][$i],
								'error' => $docfdata['error'][$i],
							 
							);
						}
					}
					else $docfiles[]=$docfdata;
										
					$attachments = array();
					
					
					foreach($docfiles as $docfile) {
					
						if (isset($docfiles) AND !empty($docfile['name']) AND $docfile['error'] != 0)
							$this->_errors[] = Tools::displayError('An error occurred with the file upload.');
							
							if (!empty($docfile['name'])) {
								rename($docfile['tmp_name'], Tools::getValue('cartella').'/'.$docfile['name']); 
									
							}
													
					}			
					
					echo '<div class="conf confirm">File aggiunti con successo</div>';
				}
				else {
					echo 'Nessun file selezionato';
				}
				
				echo '';
				
			}
			
			echo '<div style="float:left"><h2>Documenti</h2></div>';
			
			$cartella_documenti = Db::getInstance()->getValue("SELECT cartella_documenti FROM customer WHERE id_customer = ".Tools::getValue('id_customer')."");
			
			
			if($cartella_documenti != "") {
			
				$filelist = array();
				$dirlist = array();
				
				$directory_base = '../documenti-clienti/'.$cartella_documenti;
				
				
				
				if(Tools::getIsset('gotodir')) {
					$entire_dir = Tools::getValue('gotodir');
				}
				else {
					$entire_dir = $directory_base;
				}
				
				if(substr($entire_dir,0,12) != '../documenti') {
					echo "ERRORE"; die();
				
				}
				
				if ($handle = opendir($entire_dir)) {
				
					
					while ($dir = readdir($handle)) {
					
						if(is_dir($entire_dir.'/'.$dir) && $dir != "." && $dir != "..") { $dirlist[]=$dir; }
						if(is_file($entire_dir.'/'.$dir)) { $filelist[]=$dir; }
						
					}
					
					if(Tools::getIsset('formcartellasubmitted')) {
			
						foreach($dirlist as $entry) {
							if($entry == Tools::getValue('nome_cartella')) {
								$this->_errors[] = Tools::displayError('Esiste gia una cartella con questo nome.');
								break;
							}
						}
						
						$old_umask = umask(0);
						mkdir(Tools::getValue('cartella')."/".Tools::getValue('nome_cartella'),0777);
						umask($old_umask);
						Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=12&gotodir='.Tools::getValue('cartella').'');
						
						
					}
					
					$cartelle_count = count($dirlist);
					$file_count = count($filelist);
					
					$count_docs = $cartelle_count + $file_count;
					
					echo '<link rel="stylesheet" type="text/css" href="explorer.css" />';
					
					if($count_docs == 0) {
						echo "<div style='float:left; margin-left:20px; margin-top:5px;'>Non sono presenti file n&egrave; sottocartelle in questa cartella.</div><div style='clear:both'></div>";
					} else {
						echo "<div style='float:left; margin-left:20px; margin-top:5px;'>Sono presenti ".$file_count." file e ".$cartelle_count." sottocartelle in questa cartella.</div><div style='clear:both'></div>";
					}
					//echo "Ti trovi in: ".$entire_dir;
					echo '<form name="submit_delete_move_documento" id="submit_delete_documento" action="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=12&deleteselection" method="post" class="std" enctype="multipart/form-data">
						<input type="hidden" name="current_dir" value="'.(Tools::getIsset('gotodir') ? Tools::getValue('gotodir') : $directory_base).'" />
						';
					echo "
					<table class='table' id='explorer'>
					<tr><th style='width:400px'>File</th><th style='width:150px'>Dimensioni</th><th style='width:150px'>Formato</th><th style='width:150px'>Ultima modifica</th><th style='width:50px'>Seleziona</th></tr>";
					
					if($count_docs == 0 && (!Tools::getIsset('gotodir') || Tools::getValue('gotodir') == $directory_base)) {
						echo "<tr><td></td><td></td><td></td><td></td><td></td></td></tr></table>";
					} else {
					
					}
						
					if(Tools::getIsset('gotodir') && Tools::getValue('gotodir') != $directory_base) {
						echo '
							<tr>
							<td class="icon parent" style="padding-left:23px; height:15px;" nowrap="nowrap">
							<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=12&gotodir='.(Tools::getIsset('gotodir') ? dirname(Tools::getValue('gotodir')) : $directory_base).'">'.'Livello superiore'."</a>
							</td>
							<td>
								
							</td>
							<td>
							</td>
							<td>
							</td>
							<td style='text-align:center'>
							</td>
							</tr>
							";	
					}
						
					foreach($dirlist as $entry) {
						$pathinfo = pathinfo($entire_dir.'/'.$entry);
						if($entry == '.' || $entry == '..') { }
						else {
							echo '
							<tr>
							<td class="icon folder" style="padding-left:23px; height:15px;" nowrap="nowrap">
							<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=12&gotodir='.(Tools::getIsset('gotodir') ? Tools::getValue('gotodir')."/".$entry : $directory_base."/".$entry).'">'.$entry."</a>
							</td>
							<td>
							
							</td>
							<td>
							&laquo;CARTELLA&raquo;
							</td>
							<td>
							".date ("d/m/Y H:i:s",filemtime($entire_dir.'/'.$entry))."
							</td>
							<td style='text-align:center'>
							<input type='checkbox' name='selectdocfile[]' value='".$entry."' />
							</td>
							</tr>
							";
						}						
					}
					
					foreach($filelist as $entry) {
						echo is_file($entry);
						if ($entry != "." && $entry != "..") {
							$pathinfo = pathinfo('../documenti-clienti/'.$cartella_documenti.'/'.$entry);
							echo '
							<tr>
							<td class="icon '.substr($pathinfo['extension'],0).'" style="padding-left:23px;" nowrap="nowrap">
							<a target="_blank" href="http://ezdocs:WRY753MNR43ASD147g!@www.ezdirect.it/documenti-clienti/'.$entire_dir.'/'.$entry.'">'.$entry."</a>
							</td>
							<td>
							".$this->FileSizeConvert(filesize($entire_dir.'/'.$entry))."
							</td>
							<td>
							".$pathinfo['extension']."
							</td>
							<td>
							".date ("d/m/Y H:i:s",filemtime($entire_dir.'/'.$entry))."
							</td>
							<td style='text-align:center'>
							<input type='checkbox' name='selectdocfile[]' value='".$entry."' />
							</td>
							</tr>
							";
						}
						
					}
					$directories = $this->ListIn($directory_base);
						
					echo "</table><br />";
					
					if($count_docs == 0) {
						
					} else {
					
					
					
						echo "
						<input type='submit' class='button' name='delete_documento' value='Elimina selezione' onclick=\"javascript: var surec=window.confirm('Sei sicuro? I file, le cartelle e il contenuto delle cartelle che hai selezionato, saranno cancellati in modo permanente!!!'); if (surec) { return true; } else { return false; }\" />
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						Sposta o copia documenti selezionati in: <select name='sposta_in'>"; 
						if(!Tools::getIsset('gotodir')) {
						}
						else {
							echo "<option value='$directory_base'>Cartella base del cliente</option>";
						}
						foreach($directories as $dir) {
							if($directory_base."/".$dir == Tools::getValue('gotodir')) {
							}
							else {
								echo "<option value='$directory_base/$dir'>$dir</option>";
							}
						}
							
						echo "</select>
						<input type='submit' class='button' value='Sposta' name='move_documento' />
						<input type='submit' class='button' value='Copia' name='copy_documento' />";
					}
					
					echo "
					</form>";
						
						
						
						
					
					closedir($handle);
				}

		
				echo '
				<hr />
				<div style="width:300px; display:block; float:left">
				<form name="submit_documento" id="submit_documento" action="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=12&formdocumentssubmitted&gotodir='.(Tools::getIsset('gotodir') ? Tools::getValue('gotodir') : $directory_base).'" method="post" class="std" enctype="multipart/form-data">
				<strong>Inserisci nuovo documento</strong><br />
				<div style="display:block;float:left">
				<input name="MAX_FILE_SIZE" value="200000000" type="hidden">
				<input name="cartella" value="'.$entire_dir.'" type="hidden">
							<input name="aggiungiFile[]" id="aggiungiFile" type="file" onchange="handleFileSelect(this.files)" multiple>
							<br />
							<!-- <output id="list-tkt">Anteprime: </output> -->
								<script type="text/javascript">
							function handleFileSelect(files) {
							
								// Loop through the FileList and render image files as thumbnails.
								 for (var i = 0; i < files.length; i++) {
									var f = files[i];
									var name = files[i].name;
          
									var reader = new FileReader();  
									reader.onload = function(e) {  
									  // Render thumbnail.
									  var span = document.createElement("span");
									  span.innerHTML = ["<a href=\'",e.target.result,"\' target=\'_blank\'>",name,"</a> - "].join("");
									  document.getElementById("list-tkt").insertBefore(span, null);
									};

								  // Read in the image file as a data URL.
								  reader.readAsDataURL(f);
								}
							  }
							 
							</script>
							</div>
				<input type="submit" value="OK" style="display:block;float:left" class="button" />
				
				<div style="clear:both"></div>
				</form>
				</div>	
				<div style="width:400px; display:block; float:left">
				<form name="submit_cartella" id="submit_cartella" action="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=12&formcartellasubmitted&gotodir='.(Tools::getIsset('gotodir') ? Tools::getValue('gotodir') : $directory_base).'" method="post" class="std" enctype="multipart/form-data">
				<strong>Crea nuova sottocartella</strong><br />
				<input name="cartella" value="'.$entire_dir.'" type="hidden">
				Inserire nome: <input name="nome_cartella" value="" type="text">
				
				<input type="submit" value="OK"  class="button" />
				</form>
				</div>	
				<div style="clear:both"></div>
							';
			}
			else {
				echo 'Non esiste una cartella documenti associata a questa anagrafica.
				<br /><br />
				<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&createdocsdir&token='.$this->token.'&tab-container-1=12" class="button">Clicca qui se vuoi crearla.</a><br /><br />';
				
				
			}
		
		
		} else { }
		
		//FINE DOCUMENTI
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 13) {
			// CONTRATTI
			echo '<div class="tab1" id="contratti-customer">';
			echo '<div style="float:left"><h2>Contratti assistenza</h2></div>';
			
			if(Tools::getIsset('nuovocontratto') > 0 || Tools::getIsset('edit_contratto') > 0 ) {
			
				if(Tools::getValue('submitted') == 'y') {
					if(Tools::getIsset('blocco_amministrativo')) {
						$blocco_amministrativo = 1;
					}
					else {
						$blocco_amministrativo = 0;
					}
					$data_registrazione = date("Y-m-d H:i:s", strtotime(Tools::getValue('data_registrazione')));
					$data_inizio = date("Y-m-d H:i:s", strtotime(Tools::getValue('data_inizio')));
					$data_fine = date("Y-m-d H:i:s", strtotime(Tools::getValue('data_fine')));
					
					if(Tools::getIsset('edit_contratto')) {
						
						Db::getInstance()->executeS("UPDATE contratto_assistenza SET status = '".Tools::getValue('status')."', tipo = '".Tools::getValue('tipo')."', data_registrazione = '".$data_registrazione."', data_inizio = '".$data_inizio."', data_fine = '".$data_fine."', indirizzo = '".Tools::getValue('indirizzo_contratto')."', prezzo = '".str_replace(",",".",Tools::getValue('prezzo_contratto'))."',  periodicita = '".Tools::getValue('periodicita')."',  pagamento = '".Tools::getValue('pagamento')."', blocco_amministrativo = '".$blocco_amministrativo."', rif_fattura = '".Tools::getValue('rif_fattura')."', rif_noleggio = '".Tools::getValue('rif_noleggio')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_contratto = '".Tools::getValue('id_contratto')."'");
					
						Db::getInstance()->executeS("DELETE FROM contratto_assistenza_prodotti WHERE id_contratto = '".Tools::getValue('edit_contratto')."'");
					
					} else {
						$codice_spring = Db::getInstance()->getValue("SELECT codice_spring FROM customer WHERE id_customer = ".Tools::getValue('id_customer')."");
				
						Db::getInstance()->executeS("INSERT INTO contratto_assistenza (id_contratto, id_customer, codice_spring, status, tipo, descrizione, data_registrazione, data_inizio, data_fine, indirizzo, prezzo, periodicita, pagamento, blocco_amministrativo, rif_fattura, rif_noleggio, date_add, date_upd) VALUES ('".Tools::getValue('id_contratto')."', '".Tools::getValue('id_customer')."', '".$codice_spring."', '".Tools::getValue('status')."', '".Tools::getValue('tipo')."', '', '".$data_registrazione."', '".$data_inizio."', '".$data_fine."', '".Tools::getValue('indirizzo_contratto')."', '".str_replace(",",".",Tools::getValue('prezzo_contratto'))."', '".Tools::getValue('periodicita')."', '".Tools::getValue('pagamento')."', '".$blocco_amministrativo."', '".Tools::getValue('rif_fattura')."', '".Tools::getValue('rif_noleggio')."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
					
					}
					foreach($_POST['prodotto'] as $id_product_g=>$id_product) {
					
						
						$codice_prodotto = Db::getInstance()->getValue("SELECT reference FROM product WHERE id_product = ".$id_product_g."");
						$descrizione_prodotto = Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_lang = ".$cookie->id_lang." AND id_product = ".$id_product_g."");
						
						Db::getInstance()->executeS("INSERT INTO contratto_assistenza_prodotti (id_contratto, id_product, codice_prodotto, descrizione_prodotto, quantita, prezzo, seriale, note, indirizzo, date_add, date_upd) VALUES ('".Tools::getValue('id_contratto')."', ".$id_product_g.", '".$codice_prodotto."', '".$descrizione_prodotto."', '".$_POST['quantita'][$id_product_g]."', '".str_replace(",",".",$_POST['prezzo_prodotto'][$id_product_g])."', '".$_POST['seriale_prodotto'][$id_product_g]."', '', '".$_POST['indirizzo_prodotto'][$id_product_g]."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
						
						
					}
					
					Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=13');
				
				}
				else {
				
					echo '<div style="clear:both"></div>';
					if(Tools::getIsset('edit_contratto')) {
						echo '<strong>Modifica contratto numero '.Tools::getValue('edit_contratto').'</strong>';
					}
					else {
						echo '<strong>Genera nuovo contratto</strong>';
					}
					if(Tools::getIsset('edit_contratto')) {
					
						echo "<form action='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&edit_contratto=".Tools::getValue('edit_contratto')."&submitted=y&token=".$this->token."&tab-container-1=13' method='post' onsubmit='return checkFormContratto();'>";
						
						$contratto = Db::getInstance()->getRow("SELECT * FROM contratto_assistenza WHERE id_contratto = ".Tools::getValue('edit_contratto')."");
						
					}
					else {
						echo "<form action='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&nuovocontratto&submitted=y&token=".$this->token."&tab-container-1=13' method='post' onsubmit='return checkFormContratto();'>";
						
						$id_contratto = Db::getInstance()->getValue("SELECT id_contratto FROM contratto_assistenza ORDER BY id_contratto DESC");
						$id_contratto++;
					}
								echo '
							
					<div class="anagrafica-cliente" style="width:100px">'.$this->l('N. contratto').' 
				
						<input type="text" size="33" name="id_contratto" id="id_contratto" value="'.(Tools::getIsset('edit_contratto') ? $contratto['id_contratto'] : $id_contratto).'" readonly="readonly" style="width:100px; background-color:#f0ebd6" /> 
					</div>';
					echo '
					<div class="anagrafica-cliente" style="width:100px">Rif. fattura
				
						<input type="text" size="33" name="rif_fattura" value="'.(Tools::getIsset('edit_contratto') ? $contratto['rif_fattura'] : '').'" style="width:100px" /> 
					</div>';
					echo '
					<div class="anagrafica-cliente" style="width:100px">Rif. noleggio 
				
						<input type="text" size="33" name="rif_noleggio" value="'.(Tools::getIsset('edit_contratto') ? $contratto['rif_noleggio'] : '').'" style="width:100px" /> 
					</div>';
						echo '
					<div class="anagrafica-cliente" style="width:100px">Tipo 
				
						<select name="tipo" style="width:100px">
							<option value="1" '.(Tools::getIsset('edit_contratto') ? ($contratto['tipo'] == 1 ? 'selected="selected"' : '') : '').'>Base</option>
							<option value="2" '.(Tools::getIsset('edit_contratto') ? ($contratto['tipo'] == 2 ? 'selected="selected"' : '') : '').'>Top</option>
							<option value="3" '.(Tools::getIsset('edit_contratto') ? ($contratto['tipo'] == 3 ? 'selected="selected"' : '') : '').'>Gold</option>
						</select>
					</div>';
						echo '
					<div class="anagrafica-cliente" style="width:100px">Stato
				
						<select name="status" style="width:100px">
							<option value="0" '.(Tools::getIsset('edit_contratto') ? ($contratto['status'] == 0 ? 'selected="selected"' : '') : '').'>Attivo</option>
							<option value="1" '.(Tools::getIsset('edit_contratto') ? ($contratto['status'] == 1 ? 'selected="selected"' : '') : '').'>Sospeso</option>
							<option value="2" '.(Tools::getIsset('edit_contratto') ? ($contratto['status'] == 2 ? 'selected="selected"' : '') : '').'>Cancellato</option>
						</select>
					</div>';
					echo '<div style="clear:both"></div><br />';
					
					
						echo '
					<div class="anagrafica-cliente" style="width:100px">Importo (in &euro;)
				
						<input type="text" size="33" id="prezzo_contratto" name="prezzo_contratto" value="'.(Tools::getIsset('edit_contratto') ? str_replace(".",",",$contratto['prezzo']) : '').'" style="width:100px" onkeyup="calcolaRataContratto()"; />
					</div>';
					echo '
					<div class="anagrafica-cliente" style="width:100px"  onchange="calcolaRataContratto()";>Periodicit&agrave; (in mesi)
				
						<select name="periodicita" id="periodicita"  style="width:100px">
							<option value="1" '.(Tools::getIsset('edit_contratto') ? ($contratto['periodicita'] == 1 ? 'selected="selected"' : '') : '').'>1</option>
							<option value="2" '.(Tools::getIsset('edit_contratto') ? ($contratto['periodicita'] == 2 ? 'selected="selected"' : '') : '').'>2</option>
							<option value="3" '.(Tools::getIsset('edit_contratto') ? ($contratto['periodicita'] == 3 ? 'selected="selected"' : '') : '').'>3</option>
							<option value="4" '.(Tools::getIsset('edit_contratto') ? ($contratto['periodicita'] == 4 ? 'selected="selected"' : '') : '').'>4</option>
							<option value="6" '.(Tools::getIsset('edit_contratto') ? ($contratto['periodicita'] == 6 ? 'selected="selected"' : '') : '').'>6</option>
						</select>
					</div>';
					
						echo '
					<div class="anagrafica-cliente" style="width:100px">Calcolo rata
						<span class="tab-span" id="rata_contratto" style="width:100px"><script type="text/javascript">
							function calcolaRataContratto() {
							
								var prezzo_contratto = parseFloat(document.getElementById("prezzo_contratto").value.replace(",", "."));
								var periodicita = parseFloat(document.getElementById("periodicita").value.replace(",", "."));
								var rata = prezzo_contratto/periodicita;
							
								rata = rata.toFixed(2);
								rata = rata.replace(".", ",");
								document.getElementById("rata_contratto").innerHTML = rata;
							}
						</script>
						'.(Tools::getIsset('edit_contratto') ? str_replace(".",",",$contratto['prezzo']/$contratto['periodicita']) : '').'
						</span>
					</div>';
					
					echo '
					<div class="anagrafica-cliente" style="width:220px">Tipo pagamento
				
						<select name="pagamento" id="pagamento"  style="width:220px">';
						echo "
							<option value='' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == '' ? 'selected="selected"' : '') : '').">Nessun metodo </option>
							<option value='Bonifico Bancario' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'Bonifico Bancario' ? 'selected="selected"' : '') : '').">Bonifico</option>
							<option value='GestPay' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'GestPay' ? 'selected="selected"' : '') : '').">Carta di credito (Gestpay)</option>
							<option value='PayPal' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'PayPal' ? 'selected="selected"' : '') : '').">Paypal</option>
							<option value='Contrassegno' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'Contrassegno' ? 'selected="selected"' : '') : '').">Contrassegno</option>
							<option value='Bonifico 30 gg. fine mese' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'Bonifico 30 gg. fine mese' ? 'selected="selected"' : '') : '').">Bonifico 30 gg. fine mese</option>
							<option value='Bonifico 60 gg. fine mese' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'Bonifico 60 gg. fine mese' ? 'selected="selected"' : '') : '').">Bonifico 60 gg. fine mese</option>
							<option value='Bonifico 90 gg. fine mese' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'Bonifico 90 gg. fine mese' ? 'selected="selected"' : '') : '').">Bonifico 90 gg. fine mese</option>
							<option value='R.B. 60 GG. D.F. F.M.' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'R.B. 60 GG. D.F. F.M.' ? 'selected="selected"' : '') : '').">R.B. 60 GG. D.F. F.M.</option>
							";
							
							
							
							
							echo '
						</select>
					</div>';
					
					echo '
					<div class="anagrafica-cliente" style="width:200px; color:red">Blocco amministrativo
				
						<input type="checkbox" name="blocco_amministrativo" '.(Tools::getIsset('edit_contratto') ? ($contratto['blocco_amministrativo'] == 1 ? 'checked="checked"' : '') : '').'" />
					</div>';
					
					echo '<div style="clear:both"></div><br />';
					
					includeDatepicker3(array('data_registrazione', 'data_inizio', 'data_fine'), true);
					echo '
					<div class="anagrafica-cliente" style="width:100px">Data registrazione
				
						<input type="text" size="33" id="data_registrazione" name="data_registrazione" value="'.(Tools::getIsset('edit_contratto') ? date("d-m-Y", strtotime($contratto['data_registrazione'])) : date('d-m-Y')).'" style="width:100px" />
					</div>';
						echo '
					<div class="anagrafica-cliente" style="width:100px">Data inizio 
				
						<input type="text" size="33" id="data_inizio" name="data_inizio" value="'.(Tools::getIsset('edit_contratto') ? date("d-m-Y", strtotime($contratto['data_inizio'])) : date('d-m-Y')).'" style="width:100px" />
					</div>';
						echo '
					<div class="anagrafica-cliente" style="width:100px">Data fine
				
						<input type="text" size="33" id="data_fine" name="data_fine" value="'.(Tools::getIsset('edit_contratto') ? date("d-m-Y", strtotime($contratto['data_fine'])) : '').'" style="width:100px" />
					</div>';
					
					$indirizzi = Db::getInstance()->executeS("SELECT * FROM address WHERE id_customer = ".$customer->id." AND deleted = 0 AND active = 1 ORDER BY id_address ASC");
					
						echo '
					<div class="anagrafica-cliente" style="width:300px">Sede
				
						<select name="indirizzo_contratto" style="width:400px">';
						foreach ($indirizzi as $del_a) {
							echo '<option value="'.$del_a['id_address'].'" '.(Tools::getIsset('edit_contratto') ? ($contratto['indirizzo'] == $del_a['id_address'] ? 'selected="selected"' : '') : '').'>'.$del_a['address1'].' - '.$del_a['city'].'</option>';
						}
					echo '</select>
					</div>';
				
					echo '<div style="clear:both"></div>';
					
					
					echo '<br />';
					
					echo '			Seleziona il prodotto da aggiungere: <input size="123" type="text" value="" id="product_autocomplete_input" /><br /><br />
				
					LISTINO INTERNO (codici non visibili per i clienti dal sito web): <input size="123" type="text" value="" id="product_autocomplete_input_interno" /><br /><br />
					<script type="text/javascript">
							var formProduct = "";
							var products = new Array();
						</script>
						<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
						<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete.js"></script>
						<script type="text/javascript">
						function addProductAssistenza(event, data, formatted)
						{
							document.getElementById("product_autocomplete_input").value = ""; 
							var productId = data[1];
							var productRef = data[4];
							var productName = data[5];
							
							
							$("<tr><td>"+productRef+"</td><td>"+productName+"</td><td><input type=\'text\' name=\'quantita["+productId+"]\' size=\'3\' /></td><td><input type=\'text\' name=\'prezzo_prodotto["+productId+"]\' size=\'7\' /></td><td><select name=\'indirizzo_prodotto["+productId+"]\' style=\'width:200px\'>'; foreach ($indirizzi as $del_a) { echo '<option value=\''.$del_a['id_address'].'\'>'.$del_a['address1'].' - '.$del_a['city'].'</option>'; } echo '</select></td><td><input type=\'hidden\' name=\'prodotto_id["+productId+"]\' value=\'"+productId+"\' /><input type=\'checkbox\' name=\'prodotto["+productId+"]\' checked=\'checked\' /></td></tr>").appendTo("#tableProductsBody");
							
							productId = productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "");
				
							var excludeIds = document.getElementById("excludeIds").value;
							excludeIds = excludeIds+productId+",";
							document.getElementById("excludeIds").value = excludeIds;
							$("#product_autocomplete_input").val("");
							
							
							$("#product_autocomplete_input").setOptions({
								extraParams: {excludeIds : document.getElementById("excludeIds").value}
							});	
						}

						
					</script>
					
					<script type="text/javascript">
						urlToCall = null;
						
						function getExcludeIds() {
							var ids = "";
							ids += $(\'#excludeIds\').val().replace(/\\-/g,\',\').replace(/\\,$/,\'\');
							return ids;
						}
						
						/* function autocomplete */
						$(function() {
							$(\'#product_autocomplete_input\')
								.autocomplete(\'ajax_products_list2.php?id_cart='.$_GET['id_cart'].'\', {
									minChars: 3,
									autoFill: false,
									max:50,
									matchContains: true,
									matchSubset: 0,
									mustMatch:true,
									scroll:true,
									cacheLength:1,
									formatItem: function(item) {
								
												var color = "";
												if(item[24] == 1) {
													var color = "; color:red";
												}
												if(item[26] == 0) {
													return \'<tr><th style="width:150px; text-align:left">Codice</th><th style="width:550px">Desc. prodotto</th><th style="width:60px; text-align:right">Prezzo</th><th style="width:50px; text-align:right">Qt. mag.</th><th style="width:50px; text-align:right">Qt. tot</th></tr><tr><td style="width:150px; text-align:left">\'+item[4]+\'</td><td style="width:550px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
													
													;
												}
												else {
													return \'<tr><td style="width:150px; text-align:left">\'+item[4]+\'</td><td style="width:550px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
													
													;
													}
												}
									
								}).result(addProductAssistenza);
								
								$("#product_autocomplete_input").setOptions({
									extraParams: {excludeIds : getExcludeIds()}
								});		
							
						});
					
						$("#product_autocomplete_input").css(\'width\',\'850px\');
						$("#product_autocomplete_input").keypress(function(event){
									
									  var keycode = (event.keyCode ? event.keyCode : event.which);
									  if (keycode == "13") {
										event.preventDefault();
										event.stopPropagation();    
									  }
									});
									
							$(function() {
							$(\'#product_autocomplete_input_interno\')
								.autocomplete(\'ajax_products_list2_interno.php?id_cart='.$_GET['id_cart'].'\', {
									minChars: 3,
									autoFill: false,
									max:50,
									matchContains: true,
									matchSubset: 0,
									mustMatch:true,
									scroll:true,
									cacheLength:1,
									formatItem: function(item) {
								
												var color = "";
												if(item[24] == 1) {
													var color = "; color:red";
												}
												if(item[26] == 0) {
													return \'<tr><th style="width:150px; text-align:left">Codice</th><th style="width:550px">Desc. prodotto</th><th style="width:60px; text-align:right">Prezzo</th><th style="width:50px; text-align:right">Qt. mag.</th><th style="width:50px; text-align:right">Qt. tot</th></tr><tr><td style="width:150px; text-align:left">\'+item[4]+\'</td><td style="width:550px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
													
													;
												}
												else {
													return \'<tr><td style="width:150px; text-align:left">\'+item[4]+\'</td><td style="width:550px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
													
													;
													}
												}
									
								}).result(addProductAssistenza);
								
								$("#product_autocomplete_input_interno").setOptions({
									extraParams: {excludeIds : getExcludeIds()}
								});		
							
						});
					
						$("#product_autocomplete_input_interno").css(\'width\',\'850px\');
						$("#product_autocomplete_input_interno").keypress(function(event){
									
									  var keycode = (event.keyCode ? event.keyCode : event.which);
									  if (keycode == "13") {
										event.preventDefault();
										event.stopPropagation();    
									  }
									});
									
						
					</script>';
					
						echo "<strong>Prodotti</strong> (spunta per includere il prodotto nel contratto di assistenza)<br />";
					
					if(Tools::getIsset('edit_contratto')) {
						$prodotti = Db::getInstance()->executeS("SELECT count(cap.id_product) AS conto, cap.id_product as product_id, cap.quantita, cap.prezzo, cap.indirizzo, cap.seriale, p.reference, pl.name FROM contratto_assistenza_prodotti cap JOIN contratto_assistenza ca ON ca.id_contratto = cap.id_contratto JOIN product p ON cap.id_product = p.id_product JOIN product_lang pl ON cap.id_product = pl.id_product WHERE cap.id_contratto = '".Tools::getValue('edit_contratto')."' AND pl.id_lang = 5 GROUP BY cap.id_product");
					}
					else {
					
						$prodotti = Db::getInstance()->executeS("SELECT count(od.product_id) AS conto, od.product_id, p.reference, pl.name, f.qt_ord FROM order_detail od JOIN orders o ON od.id_order = o.id_order JOIN product p ON od.product_id = p.id_product JOIN product_lang pl ON od.product_id = pl.id_product  JOIN fattura f ON p.reference = f.cod_articolo WHERE o.id_customer = ".$customer->id." AND p.reference IN (SELECT cod_articolo FROM fattura WHERE id_customer = ".$customer->id.")  AND pl.id_lang = 5 GROUP BY od.product_id");
					
					}
					echo "<table class='table'><thead><tr>
						<th>Codice prodotto</th>
						<th>Descrizione</th>
						<th></th>
						<th>Qta</th>
						<th>Seriale</th>
						<th>Indirizzo prodotto</th>
						
						</tr></thead><tbody id='tableProductsBody'>";
						
					if(sizeof($prodotti) > 0) {
					
						
						foreach($prodotti as $prodotto) {
						
							echo "<tr>";
							echo "<td>".$prodotto['reference']."</td>";
							echo "<td>".$prodotto['name']."</td>";
							echo "<td>
				
					<input type='checkbox' name='prodotto[".$prodotto['product_id']."]' ".(Tools::getIsset('edit_contratto') ? "checked='checked'" : '')." /></td>";
							echo "<td><input type='text' size='3' name='quantita[".$prodotto['product_id']."]' value='".(Tools::getIsset('edit_contratto') ? $prodotto['quantita'] : $prodotto['qt_ord'])."' /></td>";
							echo "<td><input type='text' size='7' name='seriale_prodotto[".$prodotto['product_id']."]' value='".(Tools::getIsset('edit_contratto') ? $prodotto['seriale'] : '')."' /></td>";
							echo '<td><select name="indirizzo_prodotto['.$prodotto['product_id'].']" style="width:200px">';
						foreach ($indirizzi as $del_a) {
							echo '<option value="'.$del_a['id_address'].'" '.(Tools::getIsset('edit_contratto') ? ($prodotto['indirizzo'] == $del_a['id_address'] ? 'selected="selected"' : '') : '').'>'.$del_a['address1'].' - '.$del_a['city'].'</option>';
						}
					echo '</select></td>';
					
							echo "</tr>";
						
						
						}
						
					}
					echo "</tbody></table>";
					echo '<script type="text/javascript">
					function checkFormContratto() {
						var check=document.getElementById("id_contratto").value; 
						if(check=="") {
							alert("Il campo N. contratto è obbligatorio per generare il contratto");
							return false;
						}
						else {
							return true;
						}
					}
					</script>';
					
					if(Tools::getIsset('edit_contratto')) {
						echo "<br /><input type='submit' value='Conferma modifiche' class='button' />";
					}
					else {
						echo "<br /><input type='submit' value='Conferma nuovo contratto di assistenza' class='button' />";
					}
					
					echo "</form>";
				}
			}
			
			else if(Tools::getIsset('id_contratto') && Tools::getIsset('id_contratto') > 0) {
				echo "<a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&nuovocontratto&token=".$this->token."&tab-container-1=13' class='button' style='display:block;float:left; margin-left:10px'>Genera nuovo contratto</a>";
				echo '<div style="clear:both"></div>';
				$contratto = Db::getInstance()->getRow("SELECT * FROM contratto_assistenza WHERE id_customer = ".$customer->id." AND id_contratto = ".Tools::getValue('id_contratto')."");
				$prodotti = Db::getInstance()->executeS("SELECT * FROM contratto_assistenza_prodotti WHERE id_contratto = ".Tools::getValue('id_contratto')."");
				switch($contratto['tipo']) { case 1: $tipo_contratto = 'Base'; break; case 2: $tipo_contratto = 'Top'; break; case 3: $tipo_contratto = 'Gold'; break; default: $tipo_contratto = 'Altro'; }
				switch($contratto['status']) { case 0: $status_contratto = 'Attivo'; break; case 1: $status_contratto = 'Sospeso'; break; case 2: $status_contratto = 'Cancellato'; break; default: $status_contratto = 'Altro'; }
			
							echo '
						
				<div class="anagrafica-cliente" style="width:100px">'.$this->l('N. contratto').' 
			
					<span class="tab-span" style="width:100px">'.$contratto['id_contratto'].'</span>
				</div>';
				echo '
				<div class="anagrafica-cliente" style="width:100px">Rif. fattura
			
					<span class="tab-span" style="width:100px">'.$contratto['rif_fattura'].'</span>
				</div>';
				echo '
				<div class="anagrafica-cliente" style="width:100px">Rif. noleggio 
			
					<span class="tab-span" style="width:100px">'.$contratto['rif_noleggio'].'</span>
				</div>';
					echo '
				<div class="anagrafica-cliente" style="width:100px">Tipo 
			
					<span class="tab-span" style="width:100px">'.$tipo_contratto.'</span>
				</div>';
					echo '
				<div class="anagrafica-cliente" style="width:100px">Stato
			
					<span class="tab-span" style="width:100px">'.$status_contratto.'</span>
				</div>';
				echo '<div style="clear:both"></div><br />';
				
					echo '
				<div class="anagrafica-cliente" style="width:100px">Importo
			
					<span class="tab-span" style="width:100px">'.Tools::displayPrice($contratto['prezzo'], new Currency($defaultCurrency)).'</span>
				</div>';
				echo '
				<div class="anagrafica-cliente" style="width:100px">Periodicit&agrave;
			
					<span class="tab-span" style="width:100px">'.$contratto['periodicita'].' mesi</span>
				</div>';
				
				echo '
				<div class="anagrafica-cliente" style="width:100px">Rata
			
					<span class="tab-span" style="width:100px">'.Tools::displayPrice($contratto['prezzo']/$contratto['periodicita'], new Currency($defaultCurrency)).' </span>
				</div>';
				
				echo '
				<div class="anagrafica-cliente" style="width:220px">Pagamento
			
					<span class="tab-span" style="width:220px">'.$contratto['pagamento'].'</span>
				</div>';
				
				if($contratto['blocco_amministrativo'] == 1) {
				echo '
				<div class="anagrafica-cliente" style="width:200px; color:red">Blocco amministrativo
			
					<span class="tab-span" style="width:200px; color:red">PRESENTE</span>
				</div>';
				}
				echo '<div style="clear:both"></div><br />';
				if(is_numeric($contratto['indirizzo'])) {
						
					$indirizzo_row = Db::getInstance()->getRow("SELECT * FROM address WHERE id_address = ".$contratto['indirizzo']."");
					$indirizzo = $indirizzo_row['address1']." - ".$indirizzo_row['postcode']." ".$indirizzo_row['city']." (".Db::getInstance()->getValue("SELECT iso_code FROM state WHERE id_state = ".$indirizzo_row['id_state']).")";
				}
				else {
					$indirizzo = $contratto['indirizzo'];
				}
				
						
				echo '
				<div class="anagrafica-cliente" style="width:100px">Data registrazione
			
					<span class="tab-span" style="width:100px">'.Tools::displayDate($contratto['data_registrazione'], $cookie->id_lang).'</span>
				</div>';
					echo '
				<div class="anagrafica-cliente" style="width:100px">Data inizio 
			
					<span class="tab-span" style="width:100px">'.Tools::displayDate($contratto['data_inizio'], $cookie->id_lang).'</span>
				</div>';
					echo '
				<div class="anagrafica-cliente" style="width:100px">Data fine
			
					<span class="tab-span" style="width:100px">'.Tools::displayDate($contratto['data_fine'], $cookie->id_lang).'</span>
				</div>';
				echo '
				<div class="anagrafica-cliente" style="width:350px">Sede
			
					<span class="tab-span" style="width:350px">'.$indirizzo.'</span>
				</div>';
			
				echo '<div style="clear:both"></div>';
				
				echo '<br /><strong>Prodotti</strong><br />';
				
				echo '<table class="table"><thead><tr>';
				echo '<th>Codice</th><th>Descrizione</th><th>Qta</th><th>Prezzo</th><th>Indirizzo</th>';
				echo '</tr></thead><tbody>';
				
				foreach($prodotti as $prodotto) {
					
					echo "<tr>";
					echo "<td>".$prodotto['codice_prodotto']."</td>";
					echo "<td>".$prodotto['descrizione_prodotto']."</td>";	
					echo "<td style='text-align:right'>".$prodotto['quantita']."</td>";
					echo "<td style='text-align:right'>".Tools::displayPrice($prodotto['prezzo'], new Currency($defaultCurrency))."</td>";
					if($prodotto['indirizzo'] == '') {
					
						$indirizzo1 = explode("Canone Assistenza",$prodotto['note']);
						$indirizzo2 = explode(" : ",$indirizzo1[0]);
						$indirizzo = $indirizzo2[1];
					}
					else {
						if(is_numeric($contratto['indirizzo'])) {
						
							$indirizzo_row = Db::getInstance()->getRow("SELECT * FROM address WHERE id_address = ".$prodotto['indirizzo']."");
							$indirizzo = $indirizzo_row['address1']." - ".$indirizzo_row['postcode']." ".$indirizzo_row['city']." (".Db::getInstance()->getValue("SELECT iso_code FROM state WHERE id_state = ".$indirizzo_row['id_state']).")";
						}
						else {
							$indirizzo = $prodotto['indirizzo'];
						}
						
					}
					echo "<td>".$indirizzo."</td>";
					echo "</tr>";
				}
				echo "</tbody></table><br />
				<a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&edit_contratto=".$contratto['id_contratto']."&token=".$this->token."&tab-container-1=13' class='button'>Modifica questo contratto</a>
				";
			}
			else {

				$count_contratti = Db::getInstance()->getValue("SELECT count(id_contratto) FROM contratto_assistenza WHERE id_customer = ".$customer->id."");			
				if($count_contratti == 0) {
					echo "<div style='float:left; margin-left:20px; margin-top:5px;'>Non sono presenti contratti per questa anagrafica.</div>
					
					<a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&nuovocontratto&token=".$this->token."&tab-container-1=13' class='button' style='display:block;float:left; margin-left:10px'>Genera nuovo contratto</a>
					<div style='clear:both'></div>";
					
					
				} else {
					echo "<div style='float:left; margin-left:20px; margin-top:5px;'>Sono presenti ".$count_contratti." contratti per questa anagrafica.</div>
					<a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&nuovocontratto&token=".$this->token."&tab-container-1=13' class='button' style='display:block;float:left; margin-left:10px'>Genera nuovo contratto</a>
					<div style='clear:both'></div>";
					echo "<table class='table'>";
					echo "<thead>";
					echo "<tr>";
					echo "<th>N. contratto</th>";
					echo "<th>Tipo</th>";
					echo "<th>Stato</th>";
					echo "<th>Inizia il</th>";
					echo "<th>Scade il</th>";
					echo "<th>Importo</th>";
					echo "<th>Sede</th>";
					echo "<th>Azioni</th>";
					echo "</tr>";
					echo "</thead>";
					echo "<tbody>";
					$contratti = Db::getInstance()->executeS("SELECT * FROM contratto_assistenza WHERE id_customer = ".$customer->id."");
					foreach($contratti as $contratto) {
					
						switch($contratto['tipo']) { case 1: $tipo_contratto = 'Base'; break; case 2: $tipo_contratto = 'Top'; break; case 3: $tipo_contratto = 'Gold'; break; default: $tipo_contratto = 'Altro'; }
						switch($contratto['status']) { case 0: $status_contratto = 'Attivo'; break; case 1: $status_contratto = 'Sospeso'; break; case 2: $status_contratto = 'Cancellato'; break; default: $status_contratto = 'Altro'; }
						
						if(is_numeric($contratto['indirizzo'])) {
						
							$indirizzo_row = Db::getInstance()->getRow("SELECT * FROM address WHERE id_address = ".$contratto['indirizzo']."");
							$indirizzo = $indirizzo_row['address1']." - ".$indirizzo_row['postcode']." ".$indirizzo_row['city']." (".Db::getInstance()->getValue("SELECT iso_code FROM state WHERE id_state = ".$indirizzo_row['id_state']).")";
						}
						else {
							$indirizzo = $contratto['indirizzo'];
						}
						
						echo "<tr>";
						echo "<td>".$contratto['id_contratto']."</td>";
						echo "<td>".$tipo_contratto."</td>";
						echo "<td>".$status_contratto."</td>";
						echo "<td>".Tools::displayDate($contratto['data_inizio'],$cookie->id_lang,false)."</td>";
						echo "<td>".Tools::displayDate($contratto['data_fine'],$cookie->id_lang,false)."</td>";
						echo "<td style='text-align:right'>".Tools::displayPrice($contratto['prezzo'], new Currency($defaultCurrency))."</td>";
						echo "<td>".$indirizzo."</td>";
						echo "<td><a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&id_contratto=".$contratto['id_contratto']."&token=".$this->token."&tab-container-1=13'><img src='../img/admin/details.gif' alt='Dettagli' title='Dettagli' /></a>
						<a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&edit_contratto=".$contratto['id_contratto']."&token=".$this->token."&tab-container-1=13'><img src='../img/admin/edit.gif' alt='Modifica' title='Modifica' /></a>
						";
						echo "</tr>";
					}
					echo "</tbody></table>";
				}
			}
		
		} 
		//FINE CONTRATTI
		else { }
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 14) {
			// BDL
			echo '<div style="float:left"><h2>BDL (Buoni Di Lavoro)</h2></div>';
			
			if(isset($_GET['getPDFBDL'])) {
			
				require_once('../classes/html2pdf/html2pdf.class.php');
				
				$content = $this->generatePDFBDL(Tools::getValue('id_bdl'), $_GET['getPDFBDL']);
				ob_clean();
				$html2pdf = new HTML2PDF('P','A4','it');
				$html2pdf->WriteHTML($content);
				
				$html2pdf->Output(Tools::getValue('id_bdl').'-buono-di-lavoro.pdf', 'D'); 
			
			}
			
			if(Tools::getIsset('delete_bdl') > 0) {
				
				Db::getInstance()->executeS("DELETE FROM bdl WHERE id_bdl = ".Tools::getValue('delete_bdl')."");
				Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=14');
			}
			
			if(Tools::getIsset('nuovobdl') > 0 || Tools::getIsset('edit_bdl') > 0 ) {
				if(Tools::getValue('submitted') == 'y') {
					
					$data_richiesta = date("Y-m-d H:i:s", strtotime(Tools::getValue('data_richiesta')." ".Tools::getValue('ora_richiesta')));
					$data_effettuato = date("Y-m-d H:i:s", strtotime(Tools::getValue('data_effettuato')." ".Tools::getValue('ora_effettuato')));
					if(Tools::getIsset('edit_bdl')) {
						
						Db::getInstance()->executeS("UPDATE bdl SET id_address = '".Tools::getValue('indirizzo_bdl')."', phone = '".Tools::getValue('bdl_phone')."', data_richiesta = '".$data_richiesta."', richiesto_da = '".Tools::getValue('richiesto_da')."', data_effettuato = '".$data_effettuato."', effettuato_da = '".Tools::getValue('effettuato_da')."', impianto_ubicazione = '".addslashes(Tools::getValue('impianto_ubicazione'))."', contratto_assistenza = '".Tools::getValue('contratto_assistenza')."', manutenzione_ordinaria = '".(Tools::getIsset('manutenzione_ordinaria') ? 1 : 0)."', manutenzione_straordinaria = '".(Tools::getIsset('manutenzione_straordinaria') ? 1 : 0)."', motivo_chiamata = '".addslashes(Tools::getValue('motivo_chiamata'))."', guasto = '".addslashes(Tools::getValue('guasto'))."', causa_guasto = '".addslashes(Tools::getValue('causa_guasto'))."', intervento_svolto = '".addslashes(Tools::getValue('intervento_svolto'))."', note = '".addslashes(Tools::getValue('note_bdl'))."', date_upd = '".date('Y-m-d H:i:s')."' WHERE id_bdl = '".Tools::getValue('id_bdl')."'");
						
					
					} else {
						$codice_spring = Db::getInstance()->getValue("SELECT codice_spring FROM customer WHERE id_customer = ".Tools::getValue('id_customer')."");
				
						Db::getInstance()->executeS("INSERT INTO bdl (id_bdl, id_customer, id_address, phone, data_richiesta, richiesto_da, data_effettuato, effettuato_da, impianto_ubicazione, contratto_assistenza, manutenzione_ordinaria, manutenzione_straordinaria, motivo_chiamata, guasto, causa_guasto, intervento_svolto, prodotti, dettaglio_costi, note, date_add, date_upd) VALUES ('".Tools::getValue('id_bdl')."', '".Tools::getValue('id_customer')."', '".Tools::getValue('indirizzo_bdl')."', '".Tools::getValue('bdl_phone')."', '".$data_richiesta."', '".Tools::getValue('richiesto_da')."', '".$data_effettuato."', '".Tools::getValue('effettuato_da')."', '".addslashes(Tools::getValue('impianto_ubicazione'))."', '".Tools::getValue('contratto_assistenza')."', '".(Tools::getIsset('manutenzione_ordinaria') ? 1 : 0)."', '".(Tools::getIsset('manutenzione_straordinaria') ? 1 : 0)."', '".addslashes(Tools::getValue('motivo_chiamata'))."', '".addslashes(Tools::getValue('guasto'))."', '".addslashes(Tools::getValue('causa_guasto'))."', '".addslashes(Tools::getValue('intervento_svolto'))."', '', '', '".addslashes(Tools::getValue('note_bdl'))."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
					
					}
					
					$prodotti_bdl = array();
					
					foreach($_POST['prodotto'] as $id_product_g=>$id_product) {
					
						$prodotto = array();
						$prodotto['id'] = $id_product_g;
						$prodotto['qta'] = $_POST['quantita'][$id_product_g];
						$prodotto['seriale'] = $_POST['seriale_prodotto'][$id_product_g];
						$prodotto['garanzia'] = (isset($_POST['garanzia'][$id_product_g]) ? 1 : 0);
						$prodotto['estensione'] = (isset($_POST['estensione'][$id_product_g]) ? 1 : 0);
						$prodotto['assistenza'] = (isset($_POST['assistenza'][$id_product_g]) ? 1 : 0);
						$prodotto['contratto'] = (isset($_POST['contratto'][$id_product_g]) ? 1 : 0);
						$prodotti_bdl[] = $prodotto;
					}
					
					$prodotti_bdl = serialize($prodotti_bdl);
					
					Db::getInstance()->executeS("UPDATE bdl SET prodotti = '".$prodotti_bdl."' WHERE id_bdl = ".Tools::getValue('id_bdl')."");
					
					
					$dettaglio_bdl = array();
					
					
					$dettaglio_bdl['chilometri_percorsi_qta'] = str_replace(",",".",$_POST['chilometri_percorsi_qta']);
					$dettaglio_bdl['chilometri_percorsi_unitario'] = str_replace(",",".",$_POST['chilometri_percorsi_unitario']);
					$dettaglio_bdl['diritto_chiamata_qta'] = str_replace(",",".",$_POST['diritto_chiamata_qta']);
					$dettaglio_bdl['diritto_chiamata_unitario'] = str_replace(",",".",$_POST['diritto_chiamata_unitario']);
					$dettaglio_bdl['costo_manodopera_qta'] = str_replace(",",".",$_POST['costo_manodopera_qta']);
					$dettaglio_bdl['costo_manodopera_unitario'] = str_replace(",",".",$_POST['costo_manodopera_unitario']);
					
					$dettaglio_bdl['num_varie'] = $_POST['num_varie'];
					
					$num_varie = $_POST['num_varie']; 
					for($i = 0; $i<=$_POST['num_varie']; $i++) {
						echo $_POST['ricambi_e_varie_'.$i.'_qta']."AAA";
						$dettaglio_bdl['ricambi_e_varie_'.$i.'_qta'] = str_replace(",",".",$_POST['ricambi_e_varie_'.$i.'_qta']);
						$dettaglio_bdl['ricambi_e_varie_'.$i.'_unitario'] = str_replace(",",".",$_POST['ricambi_e_varie_'.$i.'_unitario']);
						$dettaglio_bdl['ricambi_e_varie_'.$i.'_descrizione'] = str_replace(",",".",$_POST['ricambi_e_varie_'.$i.'_descrizione']);
					}
					
					
					
					
					
					$dettaglio_bdl = serialize($dettaglio_bdl);
					
					Db::getInstance()->executeS("UPDATE bdl SET dettaglio_costi = '".$dettaglio_bdl."' WHERE id_bdl = ".Tools::getValue('id_bdl')."");

					Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=14');
					
					
					
				}
				else {
					echo '<div style="clear:both"></div>';
					if(Tools::getIsset('edit_bdl')) {
						echo '<strong>Modifica BDL numero '.Tools::getValue('edit_bdl').'</strong> <a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&id_bdl='.Tools::getValue('edit_bdl').'&getPDFBDL=tecnotel&token='.$this->token.'&tab-container-1=14" class="button">Genera PDF TecnoTel</a> <a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&id_bdl='.Tools::getValue('edit_bdl').'&getPDFBDL=ezdirect&token='.$this->token.'&tab-container-1=14" class="button">Genera PDF Ezdirect</a>';
					}
					else {
						echo '<strong>Genera nuovo BDL</strong>';
					}
					
					
					echo '&nbsp;&nbsp;&nbsp;<strong>Codice Spring cliente</strong>: '.(Db::getInstance()->getValue('select codice_spring from customer where id_customer = '.Tools::getValue('id_customer').'') == '' ? '<em>Non presente</em>' : Db::getInstance()->getValue('select codice_spring from customer where id_customer = '.Tools::getValue('id_customer').'')).'';
					
					echo '<br /><br />';
					
					if(Tools::getIsset('edit_bdl')) {
					
						echo "<form action='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&edit_bdl=".Tools::getValue('edit_bdl')."&submitted=y&token=".$this->token."&tab-container-1=14' method='post' onsubmit='return checkFormBDL();'>";
						
						$bdl = Db::getInstance()->getRow("SELECT * FROM bdl WHERE id_bdl = ".Tools::getValue('edit_bdl')."");
						
					}
					else {
						echo "<form action='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&nuovobdl&submitted=y&token=".$this->token."&tab-container-1=14' method='post' onsubmit='return checkFormBDL();'>";
						
						$id_bdl = Db::getInstance()->getValue("SELECT id_bdl FROM bdl ORDER BY id_bdl DESC");
						$id_bdl++;
					}
								echo '
							
					<div class="anagrafica-cliente" style="width:100px">'.$this->l('N. BDL').' 
				
						<input type="text" size="33" name="id_bdl" id="id_bdl" value="'.(Tools::getIsset('edit_bdl') ? $bdl['id_bdl'] : $id_bdl).'" readonly="readonly" style="width:100px; background-color:#f0ebd6" /> 
					</div>';
					$indirizzi_bdl = Db::getInstance()->executeS('SELECT * FROM address a JOIN state s ON a.id_state = s.id_state WHERE a.active = 1 AND a.deleted = 0 AND a.id_customer='.$customer->id.''); 
					
					echo '
					<div class="anagrafica-cliente" style="width:490px">
						Indirizzo<br />
						<select name="indirizzo_bdl" style="width:490px">';
						foreach ($indirizzi_bdl as $indirizzo_bdl) {
						
						echo '<option value="'.$indirizzo_bdl['id_address'].'" '.($indirizzo_bdl['id_address'] == $bdl['id_address'] ? 'selected="selected"' : '').'>'.$indirizzo_bdl['address1'].' - '.$indirizzo_bdl['postcode'].' '.$indirizzo_bdl['city'].' ('.$indirizzo_bdl['iso_code'].')</option>';
						
						}
						echo '</select>
					</div>';
					
					echo '
							
					<div class="anagrafica-cliente" style="width:150px">'.$this->l('Tel.').' 
				
						<input type="text" size="33" name="bdl_phone" id="bdl_phone" value="'.(Tools::getIsset('edit_bdl') ? $bdl['phone'] : '').'" style="width:150px;" /> 
					</div>';
					
					
					echo '<div style="clear:both"></div>';
					echo '<br />';
					
					includeDatepicker3(array('data_richiesta', 'data_effettuato'), true);
					echo '
					<div class="anagrafica-cliente" style="width:100px">Richiesto il giorno
				
						<input type="text" size="33" id="data_richiesta" name="data_richiesta" value="'.(Tools::getIsset('edit_bdl') ? date("d-m-Y", strtotime($bdl['data_richiesta'])) : date('d-m-Y')).'" style="width:100px" />
					</div>';
					echo '
					<div class="anagrafica-cliente" style="width:50px">Alle ore
				
						<input type="text" size="33" id="ora_richiesta" name="ora_richiesta" value="'.(Tools::getIsset('edit_bdl') ? date("H:i", strtotime($bdl['data_richiesta'])) : '00:00').'" style="width:50px" />
					</div>';
					echo '
					<div class="anagrafica-cliente" style="width:150px">Da<br />
						<select name="richiesto_da" style="width:150px">';
						foreach ($persone as $persona) {
						
							echo '<option value="'.$persona['id_persona'].'" '.($persona['id_persona'] == $bdl['richiesto_da'] ? 'selected="selected"' : '').'>'.$persona['firstname']." ".$persona['lastname'].'</option>';
						
						}
						echo '</select>
					</div>';
					
						echo '
					<div class="anagrafica-cliente" style="width:100px">Effettuato il giorno
				
						<input type="text" size="33" id="data_effettuato" name="data_effettuato" value="'.(Tools::getIsset('edit_bdl') ? date("d-m-Y", strtotime($bdl['data_effettuato'])) : '').'" style="width:100px" />
					</div>';
						echo '
					<div class="anagrafica-cliente" style="width:50px">Alle ore
				
						<input type="text" size="33" id="ora_effettuato" name="ora_effettuato" value="'.(Tools::getIsset('edit_bdl') ? date("H:i", strtotime($bdl['data_effettuato'])) : '00:00').'" style="width:50px" />
					</div>';
					echo '
					<div class="anagrafica-cliente" style="width:150px">Da<br />
						<select name="effettuato_da" style="width:150px">';
						$employees = Employee::getEmployees();
						foreach ($employees as $employee) {
						
							echo '<option value="'.$employee['id_employee'].'" '.($employee['id_employee'] == $bdl['effettuato_da'] ? 'selected="selected"' : '').'>'.$employee['name'].'</option>';
						
						}
						echo '</select>
					</div>';
					
					echo '<div style="clear:both"></div>';
					
					
					echo '<br />';
					
					echo 'Impianto-Ubicazione: <input style="width:850px" type="text" value="'.(Tools::getIsset('edit_bdl') ? $bdl['impianto_ubicazione'] : '').'" id="impianto_ubicazione" name="impianto_ubicazione" /><br /><br />';
					
					echo '
					<div class="anagrafica-cliente" style="width:150px">Contratto assistenza<br />
						<select name="contratto_assistenza" style="width:150px">
						<option value="1" '.($bdl['contratto_assistenza'] == 1 ? 'selected="selected"' : '').'>S&igrave;</option>
						<option value="0" '.($bdl['contratto_assistenza'] == 0 ? 'selected="selected"' : '').'>No</option>
						</select>
					</div>';
					
					echo '<div class="anagrafica-cliente" style="width:150px">
					Manutenzione ordinaria<br />
					<input type="checkbox" name="manutenzione_ordinaria" '.($bdl['manutenzione_ordinaria'] == 1 ? 'checked="checked"' : '').' /><br />
					</div>';
					
					echo '<div class="anagrafica-cliente" style="width:150px">
					Manutenzione straordinaria<br />
					<input type="checkbox" name="manutenzione_straordinaria" '.($bdl['manutenzione_straordinaria'] == 1 ? 'checked="checked"' : '').' /><br />
					</div>';
					
					echo '<div style="clear:both"></div>';
					
					
					echo '<br />';
					
					echo 'Motivo chiamata: <input style="width:850px" type="text" value="'.(Tools::getIsset('edit_bdl') ? $bdl['motivo_chiamata'] : '').'" id="motivo_chiamata" name="motivo_chiamata" /><br /><br />';
					echo 'Guasto riscontrato dal tecnico: <input style="width:850px" type="text" value="'.(Tools::getIsset('edit_bdl') ? $bdl['guasto'] : '').'" id="guasto" name="guasto" /><br /><br />';
					echo 'Causa guasto: <input style="width:850px" type="text" value="'.(Tools::getIsset('edit_bdl') ? $bdl['causa_guasto'] : '').'" id="causa_guasto" name="causa_guasto" /><br /><br />';
					echo 'Descrizione intervento svolto: <textarea id="intervento_svolto" name="intervento_svolto" style="width:850px; height:50px">'.(Tools::getIsset('edit_bdl') ? $bdl['intervento_svolto'] : '').'"</textarea><br /><br />';

					echo '			Seleziona il prodotto da aggiungere: <input size="123" type="text" value="" id="product_autocomplete_input" /><br /><br />
				
					LISTINO INTERNO (codici non visibili per i clienti dal sito web): <input size="123" type="text" value="" id="product_autocomplete_input_interno" /><br /><br />
					<script type="text/javascript">
							var formProduct = "";
							var products = new Array();
						</script>
						<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
						<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete.js"></script>
						<script type="text/javascript">
						function addProductAssistenza(event, data, formatted)
						{
							document.getElementById("product_autocomplete_input").value = ""; 
							var productId = data[1];
							var productRef = data[4];
							var productName = data[5];
							
							
							$("<tr><td>"+productRef+"</td><td>"+productName+"</td><td><input type=\'text\' name=\'quantita["+productId+"]\' size=\'3\' /></td><td><input type=\'text\' name=\'seriale_prodotto["+productId+"]\' size=\'7\' /></td><td><input type=\'checkbox\' name=\'garanzia["+productId+"]\' /></td><td><input type=\'checkbox\' name=\'estensione["+productId+"]\' /></td><td><input type=\'checkbox\' name=\'assistenza["+productId+"]\'  /></td><td><input type=\'checkbox\' name=\'contratto["+productId+"]\' /></td></td><td><input type=\'hidden\' name=\'prodotto_id["+productId+"]\' value=\'"+productId+"\' /><input type=\'checkbox\' name=\'prodotto["+productId+"]\' checked=\'checked\' /></td></tr>").appendTo("#tableProductsBody");
							
							productId = productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "");
				
							var excludeIds = document.getElementById("excludeIds").value;
							excludeIds = excludeIds+productId+",";
							document.getElementById("excludeIds").value = excludeIds;
							$("#product_autocomplete_input").val("");
							
							
							$("#product_autocomplete_input").setOptions({
								extraParams: {excludeIds : document.getElementById("excludeIds").value}
							});	
						}

						
					</script>
					
					<script type="text/javascript">
						urlToCall = null;
						
						function getExcludeIds() {
							var ids = "";
							ids += $(\'#excludeIds\').val().replace(/\\-/g,\',\').replace(/\\,$/,\'\');
							return ids;
						}
						
						/* function autocomplete */
						$(function() {
							$(\'#product_autocomplete_input\')
								.autocomplete(\'ajax_products_list2.php?id_cart='.$_GET['id_cart'].'\', {
									minChars: 3,
									autoFill: false,
									max:50,
									matchContains: true,
									matchSubset: 0,
									mustMatch:true,
									scroll:true,
									cacheLength:1,
									formatItem: function(item) {
								
												var color = "";
												if(item[24] == 1) {
													var color = "; color:red";
												}
												if(item[26] == 0) {
													return \'<tr><th style="width:150px; text-align:left">Codice</th><th style="width:550px">Desc. prodotto</th><th style="width:60px; text-align:right">Prezzo</th><th style="width:50px; text-align:right">Qt. mag.</th><th style="width:50px; text-align:right">Qt. tot</th></tr><tr><td style="width:150px; text-align:left">\'+item[4]+\'</td><td style="width:550px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
													
													;
												}
												else {
													return \'<tr><td style="width:150px; text-align:left">\'+item[4]+\'</td><td style="width:550px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
													
													;
													}
												}
									
								}).result(addProductAssistenza);
								
								$("#product_autocomplete_input").setOptions({
									extraParams: {excludeIds : getExcludeIds()}
								});		
							
						});
					
						$("#product_autocomplete_input").css(\'width\',\'850px\');
						$("#product_autocomplete_input").keypress(function(event){
									
									  var keycode = (event.keyCode ? event.keyCode : event.which);
									  if (keycode == "13") {
										event.preventDefault();
										event.stopPropagation();    
									  }
									});
									
							$(function() {
							$(\'#product_autocomplete_input_interno\')
								.autocomplete(\'ajax_products_list2_interno.php?id_cart='.$_GET['id_cart'].'\', {
									minChars: 3,
									autoFill: false,
									max:50,
									matchContains: true,
									matchSubset: 0,
									mustMatch:true,
									scroll:true,
									cacheLength:1,
									formatItem: function(item) {
								
												var color = "";
												if(item[24] == 1) {
													var color = "; color:red";
												}
												if(item[26] == 0) {
													return \'<tr><th style="width:150px; text-align:left">Codice</th><th style="width:550px">Desc. prodotto</th><th style="width:60px; text-align:right">Prezzo</th><th style="width:50px; text-align:right">Qt. mag.</th><th style="width:50px; text-align:right">Qt. tot</th></tr><tr><td style="width:150px; text-align:left">\'+item[4]+\'</td><td style="width:550px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
													
													;
												}
												else {
													return \'<tr><td style="width:150px; text-align:left">\'+item[4]+\'</td><td style="width:550px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
													
													;
													}
												}
									
								}).result(addProductAssistenza);
								
								$("#product_autocomplete_input_interno").setOptions({
									extraParams: {excludeIds : getExcludeIds()}
								});		
							
						});
					
						$("#product_autocomplete_input_interno").css(\'width\',\'850px\');
						$("#product_autocomplete_input_interno").keypress(function(event){
									
									  var keycode = (event.keyCode ? event.keyCode : event.which);
									  if (keycode == "13") {
										event.preventDefault();
										event.stopPropagation();    
									  }
									});
									
						
					</script>';
					
					echo "<strong>Prodotti</strong> (spunta INCLUDI per includere il prodotto nel BDL)<br />";
					
					if(Tools::getIsset('edit_bdl')) {
						$prodotti_bdl = Db::getInstance()->getValue("SELECT prodotti FROM bdl WHERE id_bdl = '".Tools::getValue('edit_bdl')."'");
						
						$prodotti_bdl = unserialize($prodotti_bdl);
						
						$prodotto = array();
						$prodotti = array();
						foreach($prodotti_bdl as $prodotto_bdl) {
							$prodotto['reference'] = Db::getInstance()->getValue('SELECT reference FROM product WHERE id_product = '.$prodotto_bdl['id'].'');
							$prodotto['name'] = Db::getInstance()->getValue('SELECT name FROM product_lang WHERE id_lang = 5 AND id_product = '.$prodotto_bdl['id'].'');
							$prodotto['product_id'] = $prodotto_bdl['id'];
							$prodotto['qta'] = $prodotto_bdl['qta'];
							$prodotto['seriale'] = $prodotto_bdl['seriale'];
							$prodotto['garanzia'] = $prodotto_bdl['garanzia'];
							$prodotto['estensione'] = $prodotto_bdl['estensione'];
							$prodotto['assistenza'] = $prodotto_bdl['assistenza'];
							$prodotto['contratto'] = $prodotto_bdl['contratto'];
							
							$prodotti[] = $prodotto;
						}
					}
					else {
					
						$prodotti = Db::getInstance()->executeS("SELECT count(od.product_id) AS conto, od.product_id, p.reference, pl.name, f.qt_ord FROM order_detail od JOIN orders o ON od.id_order = o.id_order JOIN product p ON od.product_id = p.id_product JOIN product_lang pl ON od.product_id = pl.id_product  JOIN fattura f ON p.reference = f.cod_articolo WHERE o.id_customer = ".$customer->id." AND p.reference IN (SELECT cod_articolo FROM fattura WHERE id_customer = ".$customer->id.")  AND pl.id_lang = 5 GROUP BY od.product_id");
					
					}
					echo "<table class='table'><thead><tr>
						<th style='width:100px'>Codice prodotto</th>
						<th style='width:285px'>Descrizione</th>
						<th style='width:30px'>Qta</th>
						<th style='width:80px'>Seriale</th>
						<th style='width:60px'>Garanzia</th>
						<th style='width:60px'>Estensione</th>
						<th style='width:60px'>Assistenza</th>
						<th style='width:60px'>Contratto</th>
						<th style='width:30px'>Includi</th>
						
						</tr></thead><tbody id='tableProductsBody'>";
						
					if(sizeof($prodotti) > 0) {
					
						
						foreach($prodotti as $prodotto) {
						
							echo "<tr>";
							echo "<td>".$prodotto['reference']."</td>";
							echo "<td>".$prodotto['name']."</td>";
							
							echo "<td><input type='text' size='3' name='quantita[".$prodotto['product_id']."]' value='".(Tools::getIsset('edit_bdl') ? $prodotto['qta'] : $prodotto['qt_ord'])."' /></td>";
							echo "<td><input type='text' size='7' name='seriale_prodotto[".$prodotto['product_id']."]' value='".(Tools::getIsset('edit_bdl') ? $prodotto['seriale'] : '')."' /></td>";
							
								echo "<td><input type='checkbox' name='garanzia[".$prodotto['product_id']."]' ".(Tools::getIsset('edit_bdl') && $prodotto['garanzia'] == 1 ? "checked='checked'" : '')." /></td>";
								
								echo "<td><input type='checkbox' name='estensione[".$prodotto['product_id']."]' ".(Tools::getIsset('edit_bdl') && $prodotto['estensione'] == 1 ? "checked='checked'" : '')." /></td>";
								
								echo "<td><input type='checkbox' name='assistenza[".$prodotto['product_id']."]' ".(Tools::getIsset('edit_bdl') && $prodotto['assistenza'] == 1 ? "checked='checked'" : '')." /></td>";
								
								echo "<td><input type='checkbox' name='contratto[".$prodotto['product_id']."]' ".(Tools::getIsset('edit_bdl') && $prodotto['contratto'] == 1 ? "checked='checked'" : '')." /></td>";
							
					echo "<td><input type='checkbox' name='prodotto[".$prodotto['product_id']."]' ".(Tools::getIsset('edit_bdl') ? "checked='checked'" : '')." /></td>";
					
							echo "</tr>";
						
						
						}
						
					}
					echo "</tbody></table>";
					echo '<script type="text/javascript">
					function checkFormBDL() {
						var check=document.getElementById("id_bdl").value; 
						if(check=="") {
							alert("Il campo N. BDL è obbligatorio per generare il contratto");
							return false;
						}
						else {
							surec=window.confirm(\'Sei sicuro?\'); 
							if (surec) { 
								return true; 
							} 
							else { 
								return false; 
							}
						}
					}
					</script>';
					echo "<br />";
					echo "<strong>Dettaglio economico dell'intervento</strong><br />";
					echo '<script type="text/javascript">
					function moltiplicaCosto(campo) {
					
						var numqta = document.getElementById(campo+\'_qta\').value;
						var qta = parseFloat(numqta.replace(/\s/g, "").replace(",", "."));
						var numunitario = document.getElementById(campo+\'_unitario\').value;
						var unitario = parseFloat(numunitario.replace(/\s/g, "").replace(",", "."));
						costo = (qta*unitario).toFixed(2).replace(\'.\',\',\');
						document.getElementById(campo).value = costo;
					}
					
					
					function CalcTotBDL () {
				
					var num_diritto_chiamata = document.getElementById(\'diritto_chiamata\').value;
					var diritto_chiamata = parseFloat(num_diritto_chiamata.replace(/\s/g, "").replace(",", "."));
					if(isNaN(diritto_chiamata)) { diritto_chiamata = 0; }
					
					var num_chilometri_percorsi = document.getElementById(\'chilometri_percorsi\').value;
					var chilometri_percorsi = parseFloat(num_chilometri_percorsi.replace(/\s/g, "").replace(",", "."));
					if(isNaN(chilometri_percorsi)) { chilometri_percorsi = 0; }
					
					var num_costo_manodopera = document.getElementById(\'costo_manodopera\').value;
					var costo_manodopera = parseFloat(num_costo_manodopera.replace(/\s/g, "").replace(",", "."));
					if(isNaN(costo_manodopera)) { costo_manodopera = 0; }
					
					var num_varie = document.getElementById(\'num_varie\').value;
					
					var tot_varie = 0;
					for(var i=0; i<=num_varie; i++) {
						
						var num_ricambi_e_varie = document.getElementById(\'ricambi_e_varie_\'+i).value;
						var costo_ricambi_e_varie = parseFloat(num_ricambi_e_varie.replace(/\s/g, "").replace(",", "."));
						if(isNaN(costo_ricambi_e_varie)) { costo_ricambi_e_varie = 0; }
						tot_varie = tot_varie+costo_ricambi_e_varie;
					
					}
					
					var totale_BDL = costo_manodopera + diritto_chiamata + chilometri_percorsi + tot_varie;
					
					
					document.getElementById(\'totale_BDL\').innerHTML = totale_BDL.toFixed(2).replace(\'.\',\',\');
					
				}
				</script>';
				echo '
				<script language="JavaScript" type="text/javascript">
<!--
var num=1;
function accoda(){
	if(document.createElement && document.getElementById && document.getElementsByTagName) {
		// crea elementi
		var num_varie = document.getElementById("num_varie").value;
		num_varie++;
		document.getElementById("num_varie").value = num_varie;
		var oTr=document.createElement("TR");
		
		oTr.innerHTML = \'<td>Ricambi e varie</td><td><input type="text" size="30" name="ricambi_e_varie_\'+num_varie+\'_descrizione" id="ricambi_e_varie_\'+num_varie+\'_descrizione" /></td><td><input type="text" name="ricambi_e_varie_\'+num_varie+\'_qta" id="ricambi_e_varie_\'+num_varie+\'_qta" value="" onkeyup="moltiplicaCosto(&quot;ricambi_e_varie_\'+num_varie+\'&quot;); CalcTotBDL();"  style="width:60px; text-align:right" /></td><td><input type="text" name="ricambi_e_varie_\'+num_varie+\'_unitario" id="ricambi_e_varie_\'+num_varie+\'_unitario" value="" onkeyup="moltiplicaCosto(&quot;ricambi_e_varie_\'+num_varie+\'&quot;); CalcTotBDL();" style="width:60px; text-align:right" /></td><td><input type="text" name="ricambi_e_varie_\'+num_varie+\'" readonly="readonly" id="ricambi_e_varie_\'+num_varie+\'" value="" style="width:60px; text-align:right" /> &euro; <a style="cursor:pointer" onclick="rimuovi();"><img src="../img/admin/disabled.gif" alt="Aggiungi riga" title="Aggiungi riga" /></a></td>\';
		
		document.getElementById("dettaglio_costi_bdl").appendChild(oTr);

	}
	
			
}

function rimuovi(){
	if(document.removeChild && document.getElementById && document.getElementsByTagName) {
		var num_varie = document.getElementById("num_varie").value;
		// se non e\' la prima riga
		if(num_varie>0){
		
			// riferimento al tbody
			var tb=document.getElementById("dettaglio_costi_bdl");
			// riferimento all\' ultimo TR
			var lastTr=tb.getElementsByTagName("TR")[(tb.getElementsByTagName("TR").length)-1]
			// rimuovi
			tb.removeChild(lastTr);
			// decrementa variabile globale
			num_varie--;
			document.getElementById("num_varie").value = num_varie;
			CalcTotBDL();
		
		}
	}
}
//-->
</script>';
				
				if(Tools::getIsset('edit_bdl')) {
					$dettaglio_costi = Db::getInstance()->getValue("SELECT dettaglio_costi FROM bdl WHERE id_bdl = '".Tools::getValue('edit_bdl')."'");
					$dettaglio_costi = unserialize($dettaglio_costi);
				}
				
				echo '<table style="font-size:12px" class="table">
				<thead>
				<tr><th style="width:250px">Dettaglio</th><th>Descrizione</th><th>Qta</th><th>Unitario</th><th>Importo</th>
				</thead>
				<tbody id="dettaglio_costi_bdl">
				<tr><td>Diritto di chiamata</td><td></td><td>
				<input type="text" name="diritto_chiamata_qta" id="diritto_chiamata_qta" value="'.(Tools::getIsset('edit_bdl') ? number_format($dettaglio_costi['diritto_chiamata_qta'],2,",","") : '').'" onkeyup="moltiplicaCosto(\'diritto_chiamata\'); CalcTotBDL();"  style="width:60px; text-align:right" /></td>
				<td><input type="text" name="diritto_chiamata_unitario" id="diritto_chiamata_unitario" value="'.(Tools::getIsset('edit_bdl') ? number_format($dettaglio_costi['diritto_chiamata_unitario'],2,",","") : '').'" onkeyup="moltiplicaCosto(\'diritto_chiamata\'); CalcTotBDL();"  style="width:60px; text-align:right" /></td>
				<td><input type="text" name="diritto_chiamata" readonly="readonly" id="diritto_chiamata" value="'.(Tools::getIsset('edit_bdl') ? number_format($dettaglio_costi['diritto_chiamata_unitario']*$dettaglio_costi['diritto_chiamata_qta'],2,",","") : '').'" style="width:60px; text-align:right" /> &euro;</td></tr>
				
				<tr><td>Chilometri percorsi</td><td></td><td><input type="text" name="chilometri_percorsi_qta" id="chilometri_percorsi_qta" value="'.(Tools::getIsset('edit_bdl') ? number_format($dettaglio_costi['chilometri_percorsi_qta'],2,",","") : '').'" onkeyup="moltiplicaCosto(\'chilometri_percorsi\'); CalcTotBDL();"  style="width:60px; text-align:right" /></td>
				<td><input type="text" name="chilometri_percorsi_unitario" id="chilometri_percorsi_unitario" value="'.(Tools::getIsset('edit_bdl') ? number_format($dettaglio_costi['chilometri_percorsi_unitario'],2,",","") : '').'" onkeyup="moltiplicaCosto(\'chilometri_percorsi\'); CalcTotBDL();" style="width:60px; text-align:right" /></td>
				<td><input type="text" name="chilometri_percorsi" readonly="readonly" id="chilometri_percorsi" value="'.(Tools::getIsset('edit_bdl') ? number_format($dettaglio_costi['chilometri_percorsi_unitario']*$dettaglio_costi['chilometri_percorsi_qta'],2,",","") : '').'" style="width:60px; text-align:right" /> &euro;</td></tr>
				
				<tr><td>Costo manodopera</td><td></td><td><input type="text" name="costo_manodopera_qta" id="costo_manodopera_qta" value="'.(Tools::getIsset('edit_bdl') ? number_format($dettaglio_costi['costo_manodopera_qta'],2,",","") : '').'" onkeyup="moltiplicaCosto(\'costo_manodopera\'); CalcTotBDL();"  style="width:60px; text-align:right" /></td>
				<td><input type="text" name="costo_manodopera_unitario" id="costo_manodopera_unitario" value="'.(Tools::getIsset('edit_bdl') ? number_format($dettaglio_costi['costo_manodopera_unitario'],2,",","") : '').'" onkeyup="moltiplicaCosto(\'costo_manodopera\'); CalcTotBDL();" style="width:60px; text-align:right" /></td>
				<td><input type="text" name="costo_manodopera" readonly="readonly" id="costo_manodopera" value="'.(Tools::getIsset('edit_bdl') ? number_format($dettaglio_costi['costo_manodopera_unitario']*$dettaglio_costi['costo_manodopera_qta'],2,",","") : '').'" style="width:60px; text-align:right" /> &euro;</td></tr>';
				
				
				$num_varie = $dettaglio_costi['num_varie'];
				
				for($i=0; $i<=$num_varie; $i++) {
				
					echo '<tr><td>Ricambi e varie</td><td><input type="text" size="30" name="ricambi_e_varie_'.$i.'_descrizione" id="ricambi_e_varie_'.$i.'_descrizione" value="'.(Tools::getIsset('edit_bdl') ? $dettaglio_costi['ricambi_e_varie_'.$i.'_descrizione'] : '').'" /></td><td><input type="text" name="ricambi_e_varie_'.$i.'_qta" id="ricambi_e_varie_'.$i.'_qta" value="'.(Tools::getIsset('edit_bdl') ? number_format($dettaglio_costi['ricambi_e_varie_'.$i.'_qta'],2,",","") : '').'" onkeyup="moltiplicaCosto(\'ricambi_e_varie_'.$i.'\'); CalcTotBDL();"  style="width:60px; text-align:right" /></td>
					<td><input type="text" name="ricambi_e_varie_'.$i.'_unitario" id="ricambi_e_varie_'.$i.'_unitario" value="'.(Tools::getIsset('edit_bdl') ? number_format($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario'],2,",","") : '').'" onkeyup="moltiplicaCosto(\'ricambi_e_varie_'.$i.'\'); CalcTotBDL();" style="width:60px; text-align:right" /></td>
					<td><input type="text" name="ricambi_e_varie_'.$i.'" readonly="readonly" id="ricambi_e_varie_'.$i.'" value="'.(Tools::getIsset('edit_bdl') ? number_format($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario']*$dettaglio_costi['ricambi_e_varie_'.$i.'_qta'],2,",","") : '').'" style="width:60px; text-align:right" /> &euro; '.($i == 0 ? '<a style="cursor:pointer" onclick="accoda();"><img src="../img/admin/add.gif" alt="Aggiungi riga" title="Aggiungi riga" /></a>' : '<a style="cursor:pointer" onclick="rimuovi();"><img src="../img/admin/disabled.gif" alt="Rimuovi riga" title="Rimuovi riga" /></a>').'</td></tr>';
				
				}
				
				$totale_bdl = 0;
				
				if(Tools::getIsset('edit_bdl')) {
					$totale_bdl += $dettaglio_costi['diritto_chiamata_unitario']*$dettaglio_costi['diritto_chiamata_qta']+$dettaglio_costi['chilometri_percorsi_unitario']*$dettaglio_costi['chilometri_percorsi_qta']+$dettaglio_costi['costo_manodopera_unitario']*$dettaglio_costi['costo_manodopera_qta'];
					
					for($i=0; $i<=$num_varie; $i++) {
					
						$totale_bdl += $dettaglio_costi['ricambi_e_varie_'.$i.'_unitario']*$dettaglio_costi['ricambi_e_varie_'.$i.'_qta'];
					}
					
				}
				
				echo '</tbody><tfoot>
				<tr><td>Totale Importo intervento IVA esclusa S.E: &amp; O.</td><td></td><td></td><td></td><td><span class="tab-span" id="totale_BDL" style="width:63px; margin-right:3px; text-align:right; float:left">'.(Tools::getIsset('edit_bdl') ? number_format(($totale_bdl),2,",","") : '').'</span> &euro;</tr></tfoot>
				</table><br />';
					echo '<input type="hidden" name="num_varie" id="num_varie" value="'.(Tools::getIsset('edit_bdl') ? $dettaglio_costi['num_varie'] : '0').'" />';
					echo 'Note:<br /> <input style="width:850px" type="text" value="'.(Tools::getIsset('edit_bdl') ? $bdl['note'] : '').'" id="note_bdl" name="note_bdl" /><br />';
					
					
					if(Tools::getIsset('edit_bdl')) {
						echo "<br /><input type='submit' value='Conferma modifiche' class='button' />";
					}
					else {
						echo "<br /><input type='submit' value='Conferma nuovo buono di lavoro' class='button' />";
					}
					
					echo "</form><br /><br />";
				
				
				
				}
			}
			
			else if(Tools::getIsset('id_bdl') && Tools::getIsset('id_bdl') > 0) {
			
			}
			else {

				$count_bdl = Db::getInstance()->getValue("SELECT count(id_bdl) FROM bdl WHERE id_customer = ".$customer->id."");			
				if($count_bdl == 0) {
					echo "<div style='float:left; margin-left:20px; margin-top:5px;'>Non sono presenti buoni di lavoro per questa anagrafica.</div>
					
					<a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&nuovobdl&token=".$this->token."&tab-container-1=14' class='button' style='display:block;float:left; margin-left:10px'>Genera nuovo BDL</a>
					<div style='clear:both'></div>";
					
					
				} else {
					echo "<div style='float:left; margin-left:20px; margin-top:5px;'>Sono presenti ".$count_bdl." buoni di lavoro per questa anagrafica.</div>
					<a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&nuovobdl&token=".$this->token."&tab-container-1=14' class='button' style='display:block;float:left; margin-left:10px'>Genera nuovo BDL</a>
					<div style='clear:both'></div>";
					echo "<table class='table'>";
					echo "<thead>";
					echo "<tr>";
					echo "<th>N. BDL</th>";
					echo "<th>Richiesto il</th>";
					echo "<th>Azioni</th>";
					echo "</tr>";
					echo "</thead>";
					echo "<tbody>";
					$bdl = Db::getInstance()->executeS("SELECT * FROM bdl WHERE id_customer = ".$customer->id."");
					foreach($bdl as $bdl) {
					
					
						
						echo "<tr>";
						echo "<td>".$bdl['id_bdl']."</td>";
						echo "<td>".Tools::displayDate($bdl['data_richiesta'],$cookie->id_lang,false)."</td>";
						
						echo "<td><!-- <a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&id_bdl=".$bdl['id_bdl']."&token=".$this->token."&tab-container-1=14'><img src='../img/admin/details.gif' alt='Dettagli' title='Dettagli' /></a> -->
						<a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&edit_bdl=".$bdl['id_bdl']."&token=".$this->token."&tab-container-1=14'><img src='../img/admin/edit.gif' alt='Modifica' title='Modifica' /></a>
						
						<a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&delete_bdl=".$bdl['id_bdl']."&token=".$this->token."&tab-container-1=14' onclick=\"javascript: var surec=window.confirm('Sei sicuro?'); if (surec) { return true; } else { return false; }\"><img src='../img/admin/delete.gif' alt='Cancella' title='Cancella' /></a>
						";
						echo "</tr>";
					}
					echo "</tbody></table><br /><br />";
				}
			}
			
		}
		// FINE BDL
		
		
		$tokenTickets = Tools::getAdminToken('AdminCustomerThreads'.(int)(Tab::getIdFromClassName('AdminCustomerThreads')).(int)($cookie->id_employee));
			echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."&type=2'><img src='../img/ticket-a-120.jpg' alt='Ticket amministrativi' title='Ticket amministrativi' /></a>";
		echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."&type=8'><img src='../img/ticket-d-120.jpg' alt='Ticket ordini' title='Ticket ordini' /></a>";
		echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."&type=4'><img src='../img/ticket-t-120.jpg' alt='Ticket assistenza' title='Ticket assistenza' /></a>";
		echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."&type=preventivo'><img src='../img/ticket-p-120.jpg' alt='Ticket commerciale' title='Ticket commerciale' /></a>";
		echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."&type=3'><img src='../img/ticket-v-120.jpg' alt='Ticket rivenditori' title='Ticket rivenditori' /></a>";
		echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."'><img src='../img/ticket-tutti-120.jpg' alt='Tutti i ticket' title='Tutti i ticket' /></a>";
		echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."&data=oggi'><img src='../img/attivita-oggi-120.gif' alt='Attivita di oggi' title='Attivita di oggi' /></a>";
		
		
		echo '<br /><br /><a href="'.$currentIndex.'&token='.$this->token.'"><img src="../img/admin/arrow2.gif" /> '.$this->l('Back to customer list').'</a><br />
		</div>
		<script type="text/javascript">
var tabber1 = new Yetii({
id: "tab-container-1"
});
</script>

<script type="text/javascript">
var tabber2 = new Yetii({
id: "tab-container-azioni",
tabclass: "yetii-azioni",
'.(isset($_POST['modifica_messaggio_azione']) ? 'active: 2,' : '').'
});
</script>

<script type="text/javascript">
var tabber3 = new Yetii({
id: "tab-container-ticket",
tabclass: "yetii-ticket",
'.(isset($_POST['modifica_messaggio_ticket']) ? 'active: 2,' : '').'
});
</script>

<script type="text/javascript">
var tabber4 = new Yetii({
id: "tab-container-preventivi",
tabclass: "yetii-preventivi",
'.(isset($_POST['modifica_messaggio_preventivo']) ? 'active: 2,' : '').'
});
</script>

<script type="text/javascript">
var tabber5 = new Yetii({
id: "tab-container-mex",
tabclass: "yetii-mex",
'.(isset($_POST['modifica_messaggio_messaggio']) ? 'active: 2,' : '').'
});
</script>

';
	}

	public function displayForm($isMainTab = true)
	{
		global $currentIndex;
		parent::displayForm();

		if (!($obj = $this->loadObject(true)))
			return;

		$birthday = explode('-', $this->getFieldValue($obj, 'birthday'));
		$customer_groups = Tools::getValue('groupBox', $obj->getGroups());
		$groups = Group::getGroups($this->_defaultFormLanguage, true);

		
		if(!isset($_GET['modificheabilitate'])) {
		echo "
		<script type='text/javascript'>
		$(document).ready(function(){
			$('#modificaanagrafica :input').attr('readonly', 'readonly');
			
			$('#modificaanagrafica :checkbox, #modificaanagrafica :radio').click(function(e) {
				e.preventDefault();
			});
			
			$('#modificaanagrafica select').attr('disabled', 'disabled'); 
		});
		// end
		</script>";
		echo '<form id="modificaanagrafica" action="index.php?tab=AdminCustomers&submitAddcustomerAndStay&id_customer='.$obj->id.'&viewcustomer&modificheabilitate&token='.$this->token.'&tab-container-1=1" method="post" autocomplete="off">';
		}
		else {
		echo '
		<form id="modificaanagrafica" action="index.php?tab=AdminCustomers&submitAddcustomerAndStay&viewcustomer&token='.$this->token.'&tab-container-1=1" method="post" autocomplete="off">';
		}
		
		
		echo ($obj->id ? '<input type="hidden" name="id_'.$this->table.'" value="'.$obj->id.'" />' : '').'
			<fieldset><legend><img src="../img/admin/tab-customers.gif" />'.$this->l('Customer').'</legend>';
	
			//	if ($this->getFieldValue($obj, 'is_company') == 1) {
				
				echo '<div class="anagrafica-cliente">
				'.$this->l('Azienda:').'<br />
			
					<input type="text" size="33" name="company" value="'.htmlentities($this->getFieldValue($obj, 'company'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>
				';

				
				echo '<div class="anagrafica-cliente">'.$this->l('First name:').' <sup>*</sup><br />
				
					<input type="text" size="33" name="firstname" value="'.htmlentities($this->getFieldValue($obj, 'firstname'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>
				
		<div class="anagrafica-cliente">'.$this->l('Last name:').' <sup>*</sup><br />
				
					<input type="text" size="33" name="lastname" value="'.htmlentities($this->getFieldValue($obj, 'lastname'), ENT_COMPAT, 'UTF-8').'" /> 
					<!-- <span class="hint" name="help_box">'.$this->l('Invalid characters:').' 0-9!<>,;?=+()@#"?{}_$%:<span class="hint-pointer">&nbsp;</span></span> --><br /><br /></div>
				';
				
				echo "<div class='clear'></div>";
				
				
				echo '<div class="anagrafica-cliente">'.$this->l('Partita IVA:').' 
			
					<input type="text" size="33" name="vat_number" value="'.htmlentities($this->getFieldValue($obj, 'vat_number'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup></div>
					
		
								<div class="anagrafica-cliente">'.$this->l('Codice fiscale:').' 
			
					<input type="text" size="33" name="tax_code" value="'.htmlentities($this->getFieldValue($obj, 'tax_code'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup></div>
					
					<div class="anagrafica-cliente">'.$this->l('Codice spring:').' 
			
					<input type="text" size="33" name="codice_spring" value="'.htmlentities($this->getFieldValue($obj, 'codice_spring'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup></div>
					
			';
				echo "<div class='clear'></div><br />";
				
				
								echo '
				<div class="anagrafica-cliente">'.$this->l('E-mail address:').' 
			
					<input type="text" size="33" name="email" value="'.htmlentities($this->getFieldValue($obj, 'email'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup>
				</div>';
				
					// if the customer is guest, he hasn't any password
				if ($obj->id && !$obj->is_guest || Tools::isSubmit('add').$this->table)
				{
					echo '
					<div class="anagrafica-cliente">'.$this->l('Nuova pw forzata manualmente:').' 
						<input type="password" size="33" name="passwd" value="" /> '.(!$obj->id ? '<sup>*</sup>' : '').'
						
					</div>';
				}
				
				echo "<div class='clear'></div><br />";
				
					$res = mysql_query("SELECT id_address, phone, fax, phone_mobile FROM address JOIN customer ON customer.id_customer = address.id_customer WHERE address.deleted != 1 AND address.id_customer = $obj->id AND address.fatturazione = 1");
			$row = mysql_fetch_array($res, MYSQL_ASSOC);
			$telefono = $row['phone'];
			$fax = $row['fax'];
			$cellulare = $row['phone_mobile'];
			$idindirizzo = $row['id_address'];
			
			echo '
			<div class="anagrafica-cliente"><input type="hidden" name="idindirizzo" value="'.$idindirizzo.'" />
			'.$this->l('Telefono:').' 
			
			<input type="text" size="33" name="telefono" value="'.htmlentities($telefono, ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup>
			</div>
			
			<div class="anagrafica-cliente">
			'.$this->l('Fax:').' <br />
			
			<input type="text" size="33" name="fax" value="'.htmlentities($fax, ENT_COMPAT, 'UTF-8').'" />
			</div>
			
			<div class="anagrafica-cliente">
			'.$this->l('Cellulare:').' 
			
			<input type="text" size="33" name="cellulare" value="'.htmlentities($cellulare, ENT_COMPAT, 'UTF-8').'" />
			</div>
			';
			
				echo "<div class='clear'></div><br />";
			
			
				
				
				
				echo '<div class="anagrafica-cliente2" style="width:100px">Tipo cliente<br />'; 
				
				echo '<input type="radio" name="is_company" value="1" '; if ($this->getFieldValue($obj, 'is_company') == 1) { echo 'checked="checked"'; } else { echo ''; } echo' /><strong>AZIENDA</strong><br />
		<input type="radio" name="is_company" value="0"'; if ($this->getFieldValue($obj, 'is_company') == 0) { echo 'checked="checked"'; } else { echo ''; } echo' /></td><td style="width:60px"><strong>PRIVATO</strong>
			';
			
		
				echo '</div>';
					
			
				echo '<div class="anagrafica-cliente2" style="width:100px">Fornitore<br />'; 
				
				echo '<input type="radio" name="supplier" id="active_on" value="1" '.($this->getFieldValue($obj, 'supplier') ? 'checked="checked" ' : '').'/>
					<label class="t" for="supplier_on"><img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="supplier" id="active_off" value="0" '.(!$this->getFieldValue($obj, 'supplier') ? 'checked="checked" ' : '').'/>
					<label class="t" for="supplier_off"><img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label>';
		
				echo '</div>';
			
				
				
				
				echo '<div class="anagrafica-cliente">'.$this->l('Profilo:').' <br />
				
					<select name="id_default_group" onchange="checkDefaultGroup(this.value);">';
				foreach ($groups as $group)
					echo '<option value="'.(int)($group['id_group']).'"'.($group['id_group'] == $obj->id_default_group ? ' selected="selected"' : '').'>'.htmlentities($group['name'], ENT_NOQUOTES, 'utf-8').'</option>';
				echo '
					</select>
					
				</div>
				
				<div class="anagrafica-cliente" style="width:250px; "> <!-- 
				'.$this->l('Altri profili associati:').' 
				';
				
				/*
					if (sizeof($groups))
					{
						echo '
					<script type="text/javascript" src="mootools.js"></script>
					<script type="text/javascript" src="MultiSelect.js"></script>
					<script type="text/javascript">
					window.addEvent("domready", function() {
					var myMultiSelect = new MultiSelect(".MultiSelect");
					});
					</script>

					<div class="MultiSelect">';
						$irow = 0;
						foreach ($groups as $group)
						{
							echo '
						
								<input type="checkbox" class="delgruppo" name="groupBox[]" id="groupBox_'.$group['id_group'].'" value="'.$group['id_group'].'" '.(in_array($group['id_group'], $customer_groups) ? 'checked="checked" ' : '').' style="width:20px" /> <label for="'.$group['name'].'">'.$group['id_group'].' - '.$group['name'].'</label>
							';
						}
						echo '
					</div>
					
					';
					} else
						echo '<p>'.$this->l('No group created').'</p>';*/
				echo ' -->
				</div>';
				
				echo "<div class='clear'></div><br />";
			
			//	}
				
				echo '<div class="anagrafica-cliente">'.$this->l('Birthday:').'<br /> ';
				$sl_year = ($this->getFieldValue($obj, 'birthday')) ? $birthday[0] : 0;
				$years = Tools::dateYears();
				$sl_month = ($this->getFieldValue($obj, 'birthday')) ? $birthday[1] : 0;
				$months = Tools::dateMonths();
				$sl_day = ($this->getFieldValue($obj, 'birthday')) ? $birthday[2] : 0;
				$days = Tools::dateDays();
				$tab_months = array(
					$this->l('January'),
					$this->l('February'),
					$this->l('March'),
					$this->l('April'),
					$this->l('May'),
					$this->l('June'),
					$this->l('July'),
					$this->l('August'),
					$this->l('September'),
					$this->l('October'),
					$this->l('November'),
					$this->l('December'));
				echo '
				
					<select name="days">
						<option value="">-</option>';
						foreach ($days as $v)
							echo '<option value="'.$v.'" '.($sl_day == $v ? 'selected="selected"' : '').'>'.$v.'</option>';
					echo '
					</select>
					<select name="months">
						<option value="">-</option>';
						foreach ($months as $k => $v)
							echo '<option value="'.$k.'" '.($sl_month == $k ? 'selected="selected"' : '').'>'.$this->l($v).'</option>';
					echo '</select>
					<select name="years">
						<option value="">-</option>';
						foreach ($years as $v)
							echo '<option value="'.$v.'" '.($sl_year == $v ? 'selected="selected"' : '').'>'.$v.'</option>';
					echo '</select>
				</div>';
				
	
				if ($this->getFieldValue($obj, 'is_company') == 1) {
					echo	'<div class="anagrafica-cliente">'.$this->l('Numero impiegati:').' <br />
			
				
					<select name="employees_number">
						<option value="Da 1 a 10"'; if ($this->getFieldValue($obj, 'employees_number') == "Da 1 a 10") { echo 'selected="selected"'; } echo '>Da 1 a 10</option>
						<option value="Da 11 a 50"'; if ($this->getFieldValue($obj, 'employees_number') == "Da 11 a 50") { echo 'selected="selected"'; } echo '>Da 11 a 50</option>
						<option value="50 piu"'; if ($this->getFieldValue($obj, 'employees_number') == "50 piu") { echo 'selected="selected"'; } echo '>Più di 50</option>
						</select>
		
					</div>
				
					';
				}
				
				else {
				
				}
				
					echo	'<div class="anagrafica-cliente2">'.$this->l('Esente IVA?').' <br />
			
				
					<input type="radio" name="tax_exempt" id="active_on" value="1" '.($this->getFieldValue($obj, 'tax_exempt') ? 'checked="checked" ' : '').'/>
					<label class="t" for="tax_exempt_on"><img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="tax_exempt" id="active_off" value="0" '.(!$this->getFieldValue($obj, 'tax_exempt') ? 'checked="checked" ' : '').'/>
					<label class="t" for="tax_exempt_off"><img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label>
					
				</div>';
				
				echo	'<div class="anagrafica-cliente2">'.$this->l('Disabilita notifiche via mail?').' <br />
			
				
					<input type="radio" name="order_notifications" id="active_on" value="1" '.($this->getFieldValue($obj, 'order_notifications') ? 'checked="checked" ' : '').'/>
					<label class="t" for="order_notifications_on"><img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="order_notifications" id="active_off" value="0" '.(!$this->getFieldValue($obj, 'order_notifications') ? 'checked="checked" ' : '').'/>
					<label class="t" for="order_notifications_off"><img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label>
					
				</div>
				
					';
				
					echo "<div class='clear'></div><br />";
				
				
				echo '<div class="anagrafica-cliente2">'.$this->l('Gender:').'<br />
					<input type="radio" size="33" name="id_gender" id="gender_1" value="1" '.($this->getFieldValue($obj, 'id_gender') == 1 ? 'checked="checked" ' : '').'/>
					<label class="t" for="gender_1"> '.$this->l('Male').'</label>
					<input type="radio" size="33" name="id_gender" id="gender_2" value="2" '.($this->getFieldValue($obj, 'id_gender') == 2 ? 'checked="checked" ' : '').'/>
					<label class="t" for="gender_2"> '.$this->l('Female').'</label>
					<input type="radio" size="33" name="id_gender" id="gender_3" value="9" '.(($this->getFieldValue($obj, 'id_gender') == 9 OR !$this->getFieldValue($obj, 'id_gender')) ? 'checked="checked" ' : '').'/>
					<label class="t" for="gender_3"> '.$this->l('Unknown').'</label>
				</div>';
				
				
				echo '<div class="anagrafica-cliente2">'.$this->l('Status:').' <br />
					<input type="radio" name="active" id="active_on" value="1" '.($this->getFieldValue($obj, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_on"><img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="active" id="active_off" value="0" '.(!$this->getFieldValue($obj, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_off"><img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label>
					<p>'.$this->l('Allow or disallow this customer to log in').'</p>
				</div>
				<div class="anagrafica-cliente2">'.$this->l('Newsletter:').' <br />
					<input type="radio" name="newsletter" id="newsletter_on" value="1" '.($this->getFieldValue($obj, 'newsletter') ? 'checked="checked" ' : '').'/>
					<label class="t" for="newsletter_on"><img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="newsletter" id="newsletter_off" value="0" '.(!$this->getFieldValue($obj, 'newsletter') ? 'checked="checked" ' : '').'/>
					<label class="t" for="newsletter_off"><img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label>
					<p>'.$this->l('Customer will receive your newsletter via e-mail').'</p>
				</div>
				<div class="anagrafica-cliente2">'.$this->l('Opt-in:').'<br /> 
					<input type="radio" name="optin" id="optin_on" value="1" '.($this->getFieldValue($obj, 'optin') ? 'checked="checked" ' : '').'/>
					<label class="t" for="optin_on"><img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="optin" id="optin_off" value="0" '.(!$this->getFieldValue($obj, 'optin') ? 'checked="checked" ' : '').'/>
					<label class="t" for="optin_off"><img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label>
					<p>'.$this->l('Customer will receive your ads via e-mail').'</p>
				</div>
				<div class="clear"></div><br />
					<div class="anagrafica-cliente2" style="width:390px"><strong>'.$this->l('Nota privata:').'</strong>
				'.$this->l('This note will be displayed to all the employees but not to the customer.').'
				<textarea name="note" id="note" style="width:100%;height:100px">'.htmlentities($this->getFieldValue($obj, 'note'), ENT_COMPAT, 'UTF-8').'</textarea><br />
				</div>
				<div class="anagrafica-cliente2" style="width:390px"><strong>'.$this->l('Keyword').'</strong>
				<textarea name="keyword" id="keyword" style="width:100%;height:100px">'.htmlentities($this->getFieldValue($obj, 'keyword'), ENT_COMPAT, 'UTF-8').'</textarea><br />
				</div><div class="clear"></div><br />
				';
				
				
				if(!isset($_GET['modificheabilitate'])) {
				echo '<input type="submit" value="'.$this->l('Abilita modifiche').'" name="abilitamodifiche" class="button" />';
				}
				else {
				echo '<input type="submit" value="'.$this->l('   Save   ').'" name="submitAdd'.$this->table.'" class="button" />';
				}
				
				
				echo '
				<div class="small"><sup>*</sup> '.$this->l('Required field').'</div>
			
		</form>';
	}

	public function getList($id_lang, $orderBy = NULL, $orderWay = NULL, $start = 0, $limit = NULL)
	{
		global $cookie;
		return parent::getList((int)($cookie->id_lang), !Tools::getValue($this->table.'Orderby') ? 'date_add' : NULL, !Tools::getValue($this->table.'Orderway') ? 'DESC' : NULL);
	}
	
	public function display()
	{
		global $cookie;
		
			if (isset($_GET['view'.$this->table]))
			$this->viewcustomer();
		else
		{
			$this->getList((int)($cookie->id_lang), !Tools::getValue($this->table.'Orderby') ? 'date_add' : NULL, !Tools::getValue($this->table.'Orderway') ? 'DESC' : NULL);
			$currency = new Currency((int)(Configuration::get('PS_CURRENCY_DEFAULT')));
			$this->displayListHeader();
			$this->displayListContent();
		//	$this->displayListFooter();
			AdminCategories::displayListPagination($this->token);
		}
	}
	
	public function assegna_incarico($in_carico, $idct, $tipo) {
		global $cookie;
		$employeeincaricato = new Employee($cookie->id_employee);
		if($tipo != 'preventivo') { 
			
			$ct = new CustomerThread($idct);
			$tickettipo = $ct->id_contact;
			
			if ($tickettipo == 4){
				$sigla = "T";
			}
			else if ($tickettipo == 2){
				$sigla = "A";
			} 
			else if ($tickettipo == 3){
				$sigla = "V";
			} 
			else if ($tickettipo == 6){
				$sigla = "R";
			}
			else if ($tickettipo == 7){
				$sigla = "M";
			}
			else if ($tickettipo == 8){
				$sigla = "D";
			} 
			else if ($tickettipo == 9){
				$sigla = "S";
			} 
			$anno = $ct->date_add;
							
			$anno = substr($anno,2,2);
							
			$id_thread_ticket = $ct->id;
							
			$id_ticket = $sigla.$anno.$id_thread_ticket;
			
			if($in_carico != 0) {
								
				if ($in_carico == $ct->id_employee) {
										
				}
									
				else if ($in_carico == $cookie->id_employee) {
										
				}
										
				else {
										
					$mail_incarico = Db::getInstance()->getValue('SELECT email FROM employee WHERE id_employee = '.$in_carico.'');
					$tokenimp = Tools::getAdminToken("AdminCustomers"."2".$in_carico);
					$linkimp = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$ct->id_customer."&viewcustomer&viewticket&id_customer_thread=".$ct->id."&token=".$tokenimp.'&tab-container-1=6';	
					
					if($tickettipo = 7) {
						$linkimp = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$ct->id_customer."&viewcustomer&viewmessage&id_mex=".$ct->id."&token=".$tokenimp.'&tab-container-1=7';	
						$params3 = array(
						'{reply}' => $employeeincaricato->firstname." ".$employeeincaricato->lastname." ti ha assegnato un messaggio su Ezdirect. L'id del messaggio &egrave;: <strong>".$id_ticket."</strong><br /><br />
						<a href='".$linkimp."'>Per rispondere al messaggio, clicca qua</a>.										
						");
						Mail::Send(5, 'action', Mail::l('Messaggio cliente assegnato a te su Ezdirect', 5), 
						$params3, $mail_incarico, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', '', NULL, 
						_PS_MAIL_DIR_, true);
					}
					else {				
						$params3 = array(
						'{reply}' => $employeeincaricato->firstname." ".$employeeincaricato->lastname." ti ha assegnato un ticket su Ezdirect. L'id del ticket &egrave;: <strong>".$id_ticket."</strong><br /><br />
						<a href='".$linkimp."'>Per rispondere al ticket, clicca qua</a>.										
						");
						Mail::Send(5, 'action', Mail::l('Ticket cliente assegnato a te su Ezdirect', 5), 
						$params3, $mail_incarico, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', '', NULL, 
						_PS_MAIL_DIR_, true);
					}								
					
											
				}
			}
			else {
			
			}
		}
		else {
			$row = Db::getInstance()->getRow("SELECT * FROM form_prevendita_thread WHERE id_thread = ".$idct."");
			$tipo_richiesta_p = Db::getInstance()->getValue("SELECT tipo_richiesta FROM form_prevendita_thread WHERE id_thread = ".$idct."");
			if($tipo_richiesta_p == 'tirichiamiamonoi') {
				$sigla = "R";
			}
			else {
				$sigla = "P";
			}
			$anno = $row['date_add'];
			$anno = substr($anno,2,2);
			$id_thread_ticket = $idct;
			$id_ticket = $sigla.$anno.$id_thread_ticket;	
			if($in_carico != 0) {
								
				if ($in_carico == $row['id_employee']) {
										
				}
									
				else if ($in_carico == $row['id_employee']) {
										
				}
										
				else {
										
					$mail_incarico = Db::getInstance()->getValue('SELECT email FROM employee WHERE id_employee = '.$in_carico.'');
					$tokenimp = Tools::getAdminToken("AdminCustomers"."2".$in_carico);
					$linkimp = "http://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$row['id_customer']."&viewcustomer&viewticket&id_customer_thread=".$idct."&token=".$tokenimp.'&tab-container-1=7';	
				
					$params3 = array(
					'{reply}' => $employeeincaricato->firstname." ".$employeeincaricato->lastname." ti ha assegnato un ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." su Ezdirect. L'id del ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." &egrave;: <strong>".$id_ticket."</strong><br /><br />
					<a href='".$linkimp."'>Per rispondere al ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo').", clicca qua</a>.										
					");
					Mail::Send(5, 'action', Mail::l(''.($tipo_richiesta_p == 'tirichiamiamonoi' ? 'Ti richiamiamo noi' : 'Preventivo').' cliente assegnato a te su Ezdirect', 5), 
					$params3, $mail_incarico, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', '', NULL, 
					_PS_MAIL_DIR_, true);
							
				}
			}
			else {
			
			}			
		}
	
	}

	public function beforeDelete($object)
	{
		return $object->isUsed();
	}
	
		private function openUploadedFile()
	{
		$filename = $_GET['filename'];
		
		if(strpos($filename, ":::")) {
		
			$parti = explode(":::", $filename);
			$nomecodificato = $parti[0];
			$nomevero = $parti[1];
		
		}
		
		else {
		
			$nomecodificato = $filename;
			$nomevero = $filename;
		
		}
		
		$extensions = array('.txt' => 'text/plain', '.rtf' => 'application/rtf', '.doc' => 'application/msword', '.docx'=> 'application/msword',
		'.pdf' => 'application/pdf', '.zip' => 'multipart/x-zip', '.png' => 'image/png', '.jpeg' => 'image/jpeg', '.gif' => 'image/gif', '.jpg' => 'image/jpeg');

		$extension = '';
		foreach ($extensions AS $key => $val)
			if (substr($nomevero, -4) == $key OR substr($nomevero, -5) == $key)
			{
				$extension = $val;
				break;
			}
		echo $nomecodificato."<br />";
		echo $nomevero;
		ob_end_clean();
		header('Content-Type: '.$extension);
		header('Content-Disposition:attachment;filename="'.$nomevero.'"');
		readfile(_PS_UPLOAD_DIR_.$nomecodificato);
		die;
	}
	
			
			
			
	public function displayListContent($token = NULL)
	{
		/* Display results in a table
		 *
		 * align  : determine value alignment
		 * prefix : displayed before value
		 * suffix : displayed after value
		 * image  : object image
		 * icon   : icon determined by values
		 * active : allow to toggle status
		 */

		global $currentIndex, $cookie;
		$currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));

		$id_category = 1; // default categ

		$irow = 0;
		if ($this->_list AND isset($this->fieldsDisplay['position']))
		{
			$positions = array_map(create_function('$elem', 'return (int)($elem[\'position\']);'), $this->_list);
			sort($positions);
		}
		if ($this->_list)
		{
			$isCms = false;
			if (preg_match('/cms/Ui', $this->identifier))
				$isCms = true;
			$keyToGet = 'id_'.($isCms ? 'cms_' : '').'category'.(in_array($this->identifier, array('id_category', 'id_cms_category')) ? '_parent' : '');
			foreach ($this->_list AS $tr)
			{
				$id = $tr[$this->identifier];
				echo '<tr'.(array_key_exists($this->identifier,$this->identifiersDnd) ? ' id="tr_'.(($id_category = (int)(Tools::getValue('id_'.($isCms ? 'cms_' : '').'category', '1'))) ? $id_category : '').'_'.$id.'_'.$tr['position'].'"' : '').($irow++ % 2 ? ' class="alt_row"' : '').' '.((isset($tr['color']) AND $this->colorOnBackground) ? 'style="background-color: '.$tr['color'].'"' : '').'>
							<td class="center">';
				if ($this->delete AND (!isset($this->_listSkipDelete) OR !in_array($id, $this->_listSkipDelete)))
					echo '<input type="checkbox" name="'.$this->table.'Box[]" value="'.$id.'" class="noborder" />';
				echo '</td>';
				foreach ($this->fieldsDisplay AS $key => $params)
				{
					$active_ctrl = Db::getInstance()->getValue("SELECT active FROM customer WHERE id_customer = $id");
					$tmp = explode('!', $key);
					$key = isset($tmp[1]) ? $tmp[1] : $tmp[0];
					echo '
					<td '.(isset($params['position']) ? ' id="td_'.(isset($id_category) AND $id_category ? $id_category : 0).'_'.$id.'"' : '').' class="'.((!isset($this->noLink) OR !$this->noLink) ? 'pointer' : '').((isset($params['position']) AND $this->_orderBy == 'position')? ' dragHandle' : ''). (isset($params['align']) ? ' '.$params['align'] : '').'" ';
					if (!isset($params['position']) AND (!isset($this->noLink) OR !$this->noLink))
						echo ' onclick="document.location = \''.$currentIndex.'&'.$this->identifier.'='.$id.($this->view? '&view' : '&update').$this->table.'&token='.($token!=NULL ? $token : $this->token).'\'">'.($active_ctrl == 0 ? "<font style='text-decoration:line-through'>" : '').''.(isset($params['prefix']) ? $params['prefix'] : '');
					else
						echo '>';
					if (isset($params['supplier']) AND isset($tr[$key]))
						echo '<img src="../img/admin/'.($tr[$key] ? 'enabled.gif' : 'disabled.gif').'"
						alt="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" title="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" />';
					elseif (isset($params['active']) AND isset($tr[$key]))
						$this->_displayEnableLink($token, $id, $tr[$key], $params['active'], Tools::getValue('id_category'), Tools::getValue('id_product'));
					elseif (isset($params['activeVisu']) AND isset($tr[$key]))
						echo '<img src="../img/admin/'.($tr[$key] ? 'enabled.gif' : 'disabled.gif').'"
						alt="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" title="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" />';
					elseif (isset($params['position']))
					{
						if ($this->_orderBy == 'position' AND $this->_orderWay != 'DESC')
						{
							echo '<a'.(!($tr[$key] != $positions[sizeof($positions) - 1]) ? ' style="display: none;"' : '').' href="'.$currentIndex.
									'&'.$keyToGet.'='.(int)($id_category).'&'.$this->identifiersDnd[$this->identifier].'='.$id.'
									&way=1&position='.(int)($tr['position'] + 1).'&token='.($token!=NULL ? $token : $this->token).'">
									<img src="../img/admin/'.($this->_orderWay == 'ASC' ? 'down' : 'up').'.gif"
									alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>';

							echo '<a'.(!($tr[$key] != $positions[0]) ? ' style="display: none;"' : '').' href="'.$currentIndex.
									'&'.$keyToGet.'='.(int)($id_category).'&'.$this->identifiersDnd[$this->identifier].'='.$id.'
									&way=0&position='.(int)($tr['position'] - 1).'&token='.($token!=NULL ? $token : $this->token).'">
									<img src="../img/admin/'.($this->_orderWay == 'ASC' ? 'up' : 'down').'.gif"
									alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a>';						}
						else
							echo (int)($tr[$key] + 1);
					}
					elseif (isset($params['image']))
					{
						// item_id is the product id in a product image context, else it is the image id.
						$item_id = isset($params['image_id']) ? $tr[$params['image_id']] : $id;
						// If it's a product image
						if (isset($tr['id_image']))
						{
							$image = new Image((int)$tr['id_image']);
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$image->getExistingImgPath().'.'.$this->imageType;
						}else
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$item_id.(isset($tr['id_image']) ? '-'.(int)($tr['id_image']) : '').'.'.$this->imageType;

						echo cacheImage($path_to_image, $this->table.'_mini_'.$item_id.'.'.$this->imageType, 45, $this->imageType);
					}
					elseif (isset($params['icon']) AND (isset($params['icon'][$tr[$key]]) OR isset($params['icon']['default'])))
						echo '<img src="../img/admin/'.(isset($params['icon'][$tr[$key]]) ? $params['icon'][$tr[$key]] : $params['icon']['default'].'" alt="'.$tr[$key]).'" title="'.$tr[$key].'" />';
					elseif (isset($params['price']))
						echo Tools::displayPrice($tr[$key], (isset($params['currency']) ? Currency::getCurrencyInstance((int)($tr['id_currency'])) : $currency), false);
					elseif (isset($params['float']))
						echo rtrim(rtrim($tr[$key], '0'), '.');
					elseif (isset($params['type']) AND $params['type'] == 'date')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang);
					elseif (isset($params['type']) AND $params['type'] == 'datetime')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang, true);
					elseif (isset($tr[$key]))
					{
						$echo = ($key == 'price' ? round($tr[$key], 2) : isset($params['maxlength']) ? Tools::substr($tr[$key], 0, $params['maxlength']).'...' : $tr[$key]);
						echo isset($params['callback']) ? call_user_func_array(array($this->className, $params['callback']), array($echo, $tr)) : $echo;
					}
					else
						echo '--';

					echo (isset($params['suffix']) ? $params['suffix'] : '').($active_ctrl == 0 ? "<font style='text-decoration:line-through'>" : '').
					'</td>';
				}

				if ($this->edit OR $this->delete OR ($this->view AND $this->view !== 'noActionColumn'))
				{
					echo '<td class="center" style="white-space: nowrap;">';
					if ($this->view)
						$this->_displayViewLink($token, $id);
					if ($this->edit)
						$this->_displayEditLink($token, $id);
					if ($this->delete AND (!isset($this->_listSkipDelete) OR !in_array($id, $this->_listSkipDelete)))
						$this->_displayDeleteLink($token, $id);
					if ($this->duplicate)
						$this->_displayDuplicate($token, $id);
					echo '</td>';
				}
				echo '</tr>';
			}
		}
	}
	
	public function FileSizeConvert($bytes)
			{
				$bytes = floatval($bytes);
					$arBytes = array(
						0 => array(
							"UNIT" => "TB",
							"VALUE" => pow(1024, 4)
						),
						1 => array(
							"UNIT" => "GB",
							"VALUE" => pow(1024, 3)
						),
						2 => array(
							"UNIT" => "MB",
							"VALUE" => pow(1024, 2)
						),
						3 => array(
							"UNIT" => "KB",
							"VALUE" => 1024
						),
						4 => array(
							"UNIT" => "B",
							"VALUE" => 1
						),
					);

				foreach($arBytes as $arItem)
				{
					if($bytes >= $arItem["VALUE"])
					{
						$result = $bytes / $arItem["VALUE"];
						$result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
						break;
					}
				}
				return $result;
			}
	

	public static function deleteDir($dirPath) {
		if (! is_dir($dirPath)) {
			throw new InvalidArgumentException("$dirPath must be a directory");
		}
		if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
			$dirPath .= '/';
		}
		$files = glob($dirPath . '*', GLOB_MARK);
		foreach ($files as $file) {
			if (is_dir($file)) {
				self::deleteDir($file);
			} else {
				unlink($file);
			}
		}
		rmdir($dirPath);
	}
	
	public static function ListIn($dir, $prefix = '') {
		$dir = rtrim($dir, '\\/');
		$result = array();

		$h = opendir($dir);
		while (($f = readdir($h)) !== false) {
		  if ($f !== '.' and $f !== '..') {
			if (is_dir("$dir/$f")) {
			  $result[] = $prefix.$f;
			  $result = array_merge($result, self::ListIn("$dir/$f", "$prefix$f/"));
			} else {
			//  $result[] = $prefix.$f;
			}
		  }
		}
		closedir($h);

	  return $result;
	}
	
	public static function sanitize($string, $force_lowercase = true, $anal = false) {
    $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
                   "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
                   "â€”", "â€“", ",", "<", ".", ">", "/", "?");
    $clean = trim(str_replace($strip, "", strip_tags($string)));
    $clean = preg_replace('/\s+/', "-", $clean);
    $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
    return ($force_lowercase) ?
        (function_exists('mb_strtolower')) ?
            mb_strtolower($clean, 'UTF-8') :
            strtolower($clean) :
        $clean;
}

	public static function generatePDFBDL($id_bdl, $tipo = 'ezdirect') {
	
		$bdl = Db::getInstance()->getRow('SELECT * FROM bdl WHERE id_bdl = '.$id_bdl.'');
		$customer = Db::getInstance()->getRow('SELECT * FROM customer WHERE id_customer = '.$bdl['id_customer'].'');
		if($customer['is_company'] == 1) {
		
			$cliente = $customer['company'];
		}
		else {
			$cliente = $customer['firstname']." ".$customer['lastname'];
		}
		$indirizzo = Db::getInstance()->getRow('SELECT * FROM address a JOIN state s ON a.id_state = s.id_state WHERE a.id_address='.$bdl['id_address'].''); 
		
		$richiesto_da = Db::getInstance()->getRow('SELECT * FROM persone WHERE id_persona = '.$bdl['richiesto_da'].'');
		$effettuato_da = Db::getInstance()->getRow('SELECT * FROM employee WHERE id_employee = '.$bdl['effettuato_da'].'');
		
		$content = '<page><div id="contenitore" style="width:770px; height:1070px; font-family: Arial, Verdana, Sans-Serif; display:block; position:relative; margin:0 auto;">
	<div style="position:absolute; top:0px; left:0px">
		'.($tipo == 'tecnotel' ? '<img src="http://www.ezdirect.it/img/Tecnotel-centralini.gif" alt="Tecnotel" />' : '<img src="http://www.ezdirect.it/img/logo.jpg" alt="Ezdirect" />').'
	</div>
	
	<div style="position:absolute; top:10px; text-align:right; right:25px; height:45x; font-size:12px;">
		Via Nerino Garbuio snc - 54038 Montignoso (MS) - Tel. 0585 821163 - Fax 0585 821286<br />
		'.($tipo == 'tecnotel' ? 'P.IVA 00601870454 - Email info@tecnotel.net - Sito web www.tecnotel.net' : 'P.IVA - CF 01164670455 - REA 118272 - Email info@ezdirect.it - Sito web www.ezdirect.it').'
	</div>
	
	<div style="position:absolute; top:65px; text-align:right; right:25px; height:30px; font-size:15px">
	
	</div>
	
	<div style="clear:both">&nbsp;</div>
	
	<div style="position:relative; margin-top:125px; padding:5px; height:710px">
	
		<style type="text/css">
			.dettagli_tabella td {
				font-size:13px;
				height:20px;
				vertical-align:middle;
				border:1px solid black;
				padding:0px;
				padding-top:1px;
				padding-bottom:1px;
			}	
		</style>
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">
			<tr>
				
				<td style="width:355px; border:0px; border-right:1px solid black"></td>
				<td style="width:65px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Data</td>
				<td style="width:100px; border-bottom:0px">&nbsp;'.date('d/m/Y').'</td>
				<td style="width:108px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Buono di Lavoro N°</td> 
				<td style="width:100px; border-bottom:0px">&nbsp;'.$bdl['id_bdl'].'</td>
			</tr>
		</table>
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">	
			<tr>
				<td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Utente</td>
				<td style="width:303px; border-bottom:0px">&nbsp;'.$cliente.'</td>
				<td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Codice Spring</td>
				<td style="width:126px; border-bottom:0px">'.(Db::getInstance()->getValue('select codice_spring from customer where id_customer = '.Tools::getValue('id_customer').'') == '' ? '<em>Non presente</em>' : Db::getInstance()->getValue('select codice_spring from customer where id_customer = '.Tools::getValue('id_customer').'')).'</td>
			</tr>
		</table>
				
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
			<tr>
				
				<td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Indirizzo</td>
				<td style="width:585px; border-bottom:0px">&nbsp;'.$indirizzo['address1'].'</td>
						
			</tr>
		</table>
		
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
			<tr>
				<td style="width:150px; background-color:#f3f3f3; font-size:11px">&nbsp;CAP - Citt&agrave;</td>
				<td style="width:303px">&nbsp;'.$indirizzo['postcode'].' - '.$indirizzo['city'].'</td>
				<td style="width:35px; background-color:#f3f3f3; font-size:11px">&nbsp;Prov.</td>
				<td style="width:50px">&nbsp;'.$indirizzo['iso_code'].'</td>
				<td style="width:35px; background-color:#f3f3f3; font-size:11px">&nbsp;Tel.</td>
				<td style="width:150px;">&nbsp;'.$indirizzo['phone'].'</td>
			</tr>
		</table>
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
			<tr>
				<td style="width:150px; background-color:#f3f3f3; border-top:0px; font-size:11px">&nbsp;Richiesto il giorno</td>
				<td style="width:140px; border-top:0px">&nbsp;'.date("d-m-Y", strtotime($bdl['data_richiesta'])).'</td>
				<td style="width:50px; background-color:#f3f3f3; border-top:0px; font-size:11px">&nbsp;alle ore</td>
				<td style="width:107px; border-top:0px">&nbsp;'.date("H:i", strtotime($bdl['data_richiesta'])).'</td>
				<td style="width:35px; background-color:#f3f3f3; border-top:0px; font-size:11px">&nbsp;da</td>
				<td style="width:241px; border-top:0px">&nbsp;'.$richiesto_da['firstname']." ".$richiesto_da['lastname'].'</td>
			</tr>	
			<tr>
				<td style="width:150px; background-color:#f3f3f3; border-top:0px; font-size:11px">&nbsp;Effettuato il giorno</td>
				<td style="width:140px; border-top:0px">&nbsp;'.date("d-m-Y", strtotime($bdl['data_effettuato'])).'</td>
				<td style="width:50px; background-color:#f3f3f3; border-top:0px; font-size:11px">&nbsp;alle ore</td>
				<td style="width:107px; border-top:0px">&nbsp;'.date("H:i", strtotime($bdl['data_effettuato'])).'</td>
				<td style="width:35px; background-color:#f3f3f3; border-top:0px; font-size:11px">&nbsp;da</td>
				<td style="width:241px; border-top:0px">&nbsp;'.$effettuato_da['firstname']." ".$effettuato_da['lastname'].'</td>
			</tr>
		</table>
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
			<tr>
				
				<td style="width:150px; background-color:#f3f3f3; border-top:0px; border-bottom:0px; font-size:11px">&nbsp;Impianto-Ubicazione</td>
				<td style="width:585px; border-top:0px; border-bottom:0px">&nbsp;'.$bdl['impianto_ubicazione'].'</td>
						
			</tr>
		</table>
		
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">			
			<tr>
				<td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Contratto di assistenza</td>
				<td style="width:20px; border-bottom:0px; position:relative;">'.($bdl['contratto_assistenza'] == 1 ? '<div style="position:absolute; font-size:18px; left:42px">X</div>' : '').'SI</td>
				<td style="width:20px; border-bottom:0px; position:relative;">'.($bdl['contratto_assistenza'] == 0 ? '<div style="position:absolute; font-size:18px; left:20px">X</div>' : '').'NO</td>
				<td style="width:245px; background-color:#f3f3f3; border-bottom:0px; text-align:right; font-size:11px">Manutenzione ordinaria&nbsp;</td>
				<td style="width:20px; text-align:center; border-bottom:0px">'.($bdl['manutenzione_ordinaria'] == 1 ? 'X' : '').'</td>
				<td style="width:245px; background-color:#f3f3f3; border-bottom:0px; text-align:right; font-size:11px">&nbsp;Manutenzione straordinaria&nbsp;</td>
				<td style="width:20px; text-align:center; border-bottom:0px">'.($bdl['manutenzione_straordinaria'] == 1 ? 'X' : '').'</td>
			</tr>
		</table>
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
			<tr>
				
				<td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Motivo chiamata</td>
				<td style="width:575px; padding-left:5px; border-bottom:0px">'.$bdl['motivo_chiamata'].'</td>
						
			</tr>
		</table>
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
			<tr>
				
				<td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Guasto riscontrato dal tecnico</td>
				<td style="width:575px; padding-left:5px; border-bottom:0px">'.$bdl['guasto'].'</td>
						
			</tr>
		</table>
		
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
			<tr>
				
				<td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Causa guasto</td>
				<td style="width:575px; padding-left:5px; border-bottom:0px">'.$bdl['causa_guasto'].'</td>
						
			</tr>
		</table>
		
		
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
			<tr>
				
				<td style="width:738px; background-color:#f3f3f3; border-bottom:1px solid black; text-align:center; font-size:11px">&nbsp;Intervento svolto</td>
						
			</tr>
		</table>
		
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
			<tr>
				
				<td style="width:728px; padding-left:5px; border-top:0px; border-bottom:1px solid black;">'.$bdl['intervento_svolto'].'</td>
						
			</tr>
		</table>
		';
		
		$dettaglio_costi = Db::getInstance()->getValue("SELECT dettaglio_costi FROM bdl WHERE id_bdl = ".$id_bdl."");
					
					$dettaglio_costi = unserialize($dettaglio_costi);
			
					
					
					$content .= '
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
		
			<tr style="text-align:center">
				<td colspan="2" style="width:497px; background-color:#f3f3f3; border-top:0px; font-size:11px">Dettaglio Economico dell\'intervento</td>
				<td style="width:50px; border-top:0px; background-color:#f3f3f3; font-size:11px ">&nbsp;Q.t&agrave;</td>
				<td style="width:90px; border-top:0px; background-color:#f3f3f3; font-size:11px">&nbsp;Unitario</td>
				<td style="width:90px; border-top:0px; background-color:#f3f3f3; font-size:11px">&nbsp;Importo</td>
			</tr>
			<tr>
				<td colspan="2" style="width:497px; background-color:#f3f3f3; font-size:11px">&nbsp;Diritto fisso di chiamata ordinario</td>
				<td style="width:50px; text-align:right">&nbsp;'.number_format($dettaglio_costi['diritto_chiamata_qta'],2,",","").'&nbsp;</td>
				<td style="width:90px; text-align:right">&nbsp;'.number_format($dettaglio_costi['diritto_chiamata_unitario'],2,",","").'&nbsp;</td>
				<td style="width:90px; text-align:right">&nbsp;'.number_format($dettaglio_costi['diritto_chiamata_qta']*$dettaglio_costi['diritto_chiamata_unitario'],2,",","").'&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" style="width:497px; background-color:#f3f3f3; font-size:11px">&nbsp;Chilometri percorsi</td>
				<td style="width:50px; text-align:right">&nbsp;'.number_format($dettaglio_costi['chilometri_percorsi_qta'],2,",","").'&nbsp;</td>
				<td style="width:90px; text-align:right">&nbsp;'.number_format($dettaglio_costi['chilometri_percorsi_unitario'],2,",","").'&nbsp;</td>
				<td style="width:90px; text-align:right">&nbsp;'.number_format($dettaglio_costi['chilometri_percorsi_qta']*$dettaglio_costi['chilometri_percorsi_unitario'],2,",","").'&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" style="width:497px; background-color:#f3f3f3; font-size:11px">&nbsp;Costo manodopera</td>
				<td style="width:50px; text-align:right">&nbsp;'.number_format($dettaglio_costi['costo_manodopera_qta'],2,",","").'&nbsp;</td>
				<td style="width:90px; text-align:right">&nbsp;'.number_format($dettaglio_costi['costo_manodopera_unitario'],2,",","").'&nbsp;</td>
				<td style="width:90px; text-align:right">&nbsp;'.number_format($dettaglio_costi['costo_manodopera_qta']*$dettaglio_costi['costo_manodopera_unitario'],2,",","").'&nbsp;</td>
			</tr>';
			$tot_varie = 0;
			
			$tot_varie = $dettaglio_costi['diritto_chiamata_qta']*$dettaglio_costi['diritto_chiamata_unitario']+$dettaglio_costi['chilometri_percorsi_qta']*$dettaglio_costi['chilometri_percorsi_unitario']+$dettaglio_costi['costo_manodopera_qta']*$dettaglio_costi['costo_manodopera_unitario'];
			
			$num_varie = $dettaglio_costi['num_varie'];
				
				for($i=0; $i<=$num_varie; $i++) {
			
			
			$content .= '<tr>
				<td style="width:146px; background-color:#f3f3f3; font-size:11px">&nbsp;Ricambi e Varie</td>
				<td style="width:350px">&nbsp;'.$dettaglio_costi['ricambi_e_varie_'.$i.'_descrizione'].'</td>
				<td style="width:50px; text-align:right">&nbsp;'.number_format($dettaglio_costi['ricambi_e_varie_'.$i.'_qta'],2,",","").'&nbsp;</td>
				<td style="width:90px; text-align:right">&nbsp;'.number_format($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario'],2,",","").'&nbsp;</td>
				<td style="width:90px; text-align:right">&nbsp;'.number_format($dettaglio_costi['ricambi_e_varie_'.$i.'_qta']*$dettaglio_costi['ricambi_e_varie_'.$i.'_unitario'],2,",","").'&nbsp;</td>
			</tr>';
			$tot_varie += $dettaglio_costi['ricambi_e_varie_'.$i.'_qta']*$dettaglio_costi['ricambi_e_varie_'.$i.'_unitario'];
			
			}
			
			$content .= '
			<tr>
				<td colspan="2" style="width:497px;">&nbsp;</td>
				<td style="width:50px;">&nbsp;</td>
				<td style="width:90px">&nbsp;</td>
				<td style="width:90px">&nbsp;</td>
						
			</tr>
		</table>
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">				
			<tr>
				<td style="width:595px; background-color:#f3f3f3; text-align:right; border-top:0px; font-size:11px">IMPORTO INTERVENTO IVA esclusa S.E. &amp; O.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; EURO&nbsp;</td>
				<td style="width:140px; border-top:0px; text-align:right">&nbsp;'.number_format($tot_varie,2,",","").'&nbsp;</td>
			</tr>
		</table>
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">			
			<tr>
				<td style="width:150px; border-top:0px; background-color:#f3f3f3; font-size:11px">&nbsp;Note</td>
				<td style="width:585px; border-top:0px">&nbsp;'.$bdl['note'].'</td>
			</tr>
		</table>
	</div>
	<p style="font-size:11px; padding:5px">
	Il documento sar&agrave; reso perfetto in sede '.($tipo == 'tecnotel' ? 'TecnoTel' : 'Ezdirect').' qualora debbano essere consuntivati alcuni costi come riparazione in laboratorio interno o esterno etc. Altres&igrave; in caso di installazione o fornitura di merce non preventivata e/o non confermata attraverso conferma d\'ordine. Una copia completamente compilata sar&agrave; allegata alla fattura commerciale. Il cliente accetta e sottoscrive la buona riuscita dell\'intervento tecnico e conferma, dopo verifica effettuata con il tecnico '.($tipo == 'tecnotel' ? 'TecnoTel' : 'Ezdirect').', la risoluzione dei problemi, per i quali l\'intervento stesso &egrave; stato richiesto.
	
	</p>
	
	<div style="padding:5px; float:left">
		<table style="display:block;margin:0 auto; width:700px; text-align:center">
		<tr>&nbsp;<td style="width:350px; font-size:13px">&nbsp;<strong>'.($tipo == 'tecnotel' ? 'TecnoTel' : 'Ezdirect').'</strong>&nbsp;</td>&nbsp;<td style="width:350px; font-size:13px">&nbsp;<strong>L\'Utente (timbro e firma)</strong>&nbsp;</td>&nbsp;</tr>&nbsp;</table>
		<br /><br /><br /><br />
		
		<table style="display:block;margin:0 auto; width:735px; font-size:11px; border-collapse:collapse" class="dettagli_tabella" >
			<tr>
				<td style="width:115px; padding-left:5px; background-color:#f3f3f3; font-size:11px">&nbsp;Fatturare a</td>
				<td style="width:230px; padding-left:5px;">&nbsp;</td>
				<td style="width:115px; padding-left:5px; background-color:#f3f3f3; font-size:11px">&nbsp;Approvato da</td>
				<td style="width:230px; padding-left:5px;">&nbsp;</td>
			</tr>
			
		</table>
	
	</div>

</div></page>';

	return $content;
		
	
	
	}


}


