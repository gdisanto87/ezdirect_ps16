<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

class ImportaCambiPrezzi extends AdminPreferences
{


	public function display()
	{
		echo "<h1>Esportatore universale per comparaprezzi</h1>";

		if(Tools::getIsset('invia_cambio_prezzi')) {
			require_once 'excel_reader2.php';
			$path = $_SERVER["DOCUMENT_ROOT"];

			$tmp = $_FILES['filexls']['tmp_name']; //cartella temporanea 
			$filexls = $_FILES['filexls']['name'];
			$dir = "$path"."/ezadmin/importazione-catalogo/catalogo_xls/"; //percorso della cartella
			$dirfile = $dir.$filexls;
			move_uploaded_file($tmp, $dirfile);
			$data = new Spreadsheet_Excel_Reader("importazione-catalogo/catalogo_xls/$filexls");
			
			foreach($sheets as $key => $value)
			{
			}
	
		}
		else
		{
			echo "<form name='uploadfile' method='post' enctype='multipart/form-data'>
			
			Carica il file da importare: <input type='file' name='filexls' id='filexls' style='width:100px; '/>
			
			<br /><br />
			<input type='submit' name='invia_cambio_prezzi' value='Importa il file' />
			</form>
			</td>
			</tr>
			</table>
		}	

	}

}


