<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14632 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminOrdersNonProcessati extends AdminTab
{
	public function __construct()
	{
		include("AdminOrders.php");
		global $cookie;

	 	$this->table = 'order';
	 	$this->className = 'Order';
	 	$this->view = true;
		$this->delete = false;
		$this->colorOnBackground = false;
	 	$this->_select = '
			a.id_order AS id_pdf,
			(CASE c.is_company
			WHEN 0
			THEN CONCAT(c.firstname," ",c.lastname)
			WHEN 1
			THEN c.company
			ELSE
			CONCAT(c.firstname," ",c.lastname)
			END
			)as customer,
			(CASE ca.id_employee WHEN 0
			THEN "--"
			ELSE
			a.id_cart
			END) AS `preventivo`,
			c.`company` AS `company`,
			c.`id_default_group` AS `group`,
			osl.`name` AS `osname`,
			os.`color`,
			IF((SELECT COUNT(so.id_order) FROM `'._DB_PREFIX_.'orders` so WHERE so.id_customer = a.id_customer) > 1, 0, 1) as new,
			(SELECT COUNT(od.`id_order`) FROM `'._DB_PREFIX_.'order_detail` od WHERE od.`id_order` = a.`id_order` GROUP BY `id_order`) AS product_number';
	 	$this->_join = 'LEFT JOIN `'._DB_PREFIX_.'customer` c ON (c.`id_customer` = a.`id_customer`)
	 	LEFT JOIN `'._DB_PREFIX_.'order_history` oh ON (oh.`id_order` = a.`id_order`)
		LEFT JOIN `'._DB_PREFIX_.'cart` ca ON (ca.`id_cart` = a.`id_cart`)
		LEFT JOIN `'._DB_PREFIX_.'order_state` os ON (os.`id_order_state` = oh.`id_order_state`)
		LEFT JOIN `'._DB_PREFIX_.'order_state_lang` osl ON (os.`id_order_state` = osl.`id_order_state` AND osl.`id_lang` = '.(int)($cookie->id_lang).')';
		
		
		if(isset($_POST['cercaordineprodotto'])) {
		
			$this->_where = 'AND a.id_order IN (SELECT id_order FROM order_detail WHERE product_id = "'.$_POST['cercaordineprodotto'].'") AND oh.`id_order_history` = (SELECT MAX(`id_order_history`) FROM `'._DB_PREFIX_.'order_history` moh WHERE moh.`id_order` = a.`id_order` AND moh.id_order_state != 4 AND moh.id_order_state != 5 AND moh.id_order_state != 6 AND moh.id_order_state != 35 AND moh.id_order_state != 7 AND moh.id_order_state != 8 AND moh.id_order_state != 14 AND moh.id_order_state != 16 AND moh.id_order_state != 17 AND moh.id_order_state != 20 GROUP BY moh.`id_order`)';

		}
		
		else {
		
			$this->_where = 'AND oh.`id_order_history` = (SELECT MAX(`id_order_history`) FROM `'._DB_PREFIX_.'order_history` moh WHERE moh.`id_order` = a.`id_order` AND moh.id_order NOT IN (SELECT id_order FROM order_history moh WHERE moh.id_order_state = 4 OR moh.id_order_state = 5 OR moh.id_order_state = 6 OR moh.id_order_state = 7 OR moh.id_order_state = 8 OR moh.id_order_state = 14 OR moh.id_order_state = 16 OR moh.id_order_state = 17 OR moh.id_order_state = 20  OR moh.id_order_state = 35) GROUP BY moh.`id_order`)';
			
		}
		
		
		$statesArray = array();
		$states = OrderState::getOrderStates((int)($cookie->id_lang));

		foreach ($states AS $state)
			$statesArray[$state['id_order_state']] = $state['name'];
 		$array_orders = array(
		'id_order' => array('title' => AdminOrders::lx('ID'), 'align' => 'center', 'width' => 40, 'widthColumn' => 50),
		'preventivo' => array('title' => AdminOrders::lx('N. PREV.'), 'align' => 'center', 'width' => 40, 'tmpTableFilter' => true, 'widthColumn' => 50),
		'new' => array('title' => AdminOrders::lx('New'), 'width' => 25, 'align' => 'center', 'type' => 'bool', 'filter_key' => 'new', 'tmpTableFilter' => true, 'icon' => array(0 => 'blank.gif', 1 => 'news-new.gif'), 'orderby' => false, 'widthColumn' => 35),
		'customer' => array('title' => AdminOrders::lx('Customer'), 'widthColumn' => 120, 'width' => 120, 'filter_key' => 'customer', 'tmpTableFilter' => true),
		'group' => array('title' => AdminOrders::lx('Group'), 'widthColumn' => 10, 'width' => 10, 'filter_key' => 'group', 'tmpTableFilter' => true),
		'total_products' => array('title' => AdminOrders::lx('T. netto'), 'width' => 40, 'align' => 'right', 'prefix' => '<b>', 'suffix' => '</b>', 'price' => true, 'currency' => true, 'widthColumn' => 50),
		
		'total_paid' => array('title' => AdminOrders::lx('Total'), 'width' => 40, 'align' => 'right', 'prefix' => '<b>', 'suffix' => '</b>', 'price' => true, 'currency' => true, 'widthColumn' => 50),
		'payment' => array('title' => AdminOrders::lx('Payment'), 'width' => 100, 'widthColumn' => 110),
		'osname' => array('title' => AdminOrders::lx('Status'), 'widthColumn' => 120, 'type' => 'select', 'select' => $statesArray, 'filter_key' => 'os!id_order_state', 'filter_type' => 'int', 'width' => 120),
		'date_add' => array('title' => AdminOrders::lx('Date'), 'width' => 35, 'align' => 'right', 'type' => 'datetime', 'filter_key' => 'a!date_add', 'widthColumn' => 55));
		
		if($cookie->id_employee != 7)
			$array_orders['id_pdf'] = array('title' => AdminOrders::lx('PDF'), 'callback' => 'printPDFIcons', 'orderby' => false, 'search' => false, 'widthColumn' => 30);
		else
			$array_orders['spring'] = array('title' => AdminOrders::lx('Spring'), 'align' => 'center', 'orderby' => false, 'search' => false, 'widthColumn' => 30);
		
		$this->fieldsDisplay = $array_orders;
		
		parent::__construct();
		
		
		
	}
	
	public function postProcess()
	{
		global $cookie;
		
		echo '<a href="index.php?tab=AdminOrders&token='.Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee).'">Clicca qui per tornare alla lista di tutti gli ordini</a><br />';
		
		$tokenOrders =  Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).(int)($cookie->id_employee));
		$tokenCustomers =  Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee));
		
		if(Tools::getIsset('id_order') && Tools::getValue('id_order') != 0)
		{
			$id_customer = Db::getInstance()->getValue('SELECT id_customer FROM orders WHERE id_order = '.Tools::getValue('id_order'));
			Tools::redirectAdmin('index.php?tab=AdminCustomers&id_customer='.$id_customer.'&viewcustomer&id_order='.Tools::getValue('id_order').'&vieworder&token='.$tokenCustomers.'&tab-container-1=4');
		}
	}
	
	public function displayListContent($token = NULL)
	{
		/* Display results in a table
		 *
		 * align  : determine value alignment
		 * prefix : displayed before value
		 * suffix : displayed after value
		 * image  : object image
		 * icon   : icon determined by values
		 * active : allow to toggle status
		 */

		global $currentIndex, $cookie;
		$currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));

		$id_category = 1; // default categ
		
		echo '
		<script type="text/javascript">
		function updSpringOrder(id)
			{
				var updSpringOrder="no";
				if(document.getElementById("spring_"+id+"").checked == true)
					updSpringOrder="yes";
				else
					updSpringOrder="no";
				
				$.ajax({
				  url:"ajax.php?updSpringOrder="+updSpringOrder,
				  type: "POST",
				  data: { id_order: id
				  },
				  success:function(r){
					// alert(r);
				  },
				  error: function(xhr,stato,errori){
					 alert("Errore durante l\'operazione:"+xhr.status);
				  }
				});
				
				
			}
			
		$(".class-check").live("click", function(){
			var id = parseInt($(this).val(), 10);
			if($(this).is(":checked")) {
				$.ajax({
					type: "POST",
					url: "ajax_products_list2.php",
					data: "ordini_non_processati=yes&aggiungi=yes&id=" + id,
					success: function() {
					}
				});
			} else {
				$.ajax({
					type: "POST",
					url: "ajax_products_list2.php",
					data: "ordini_non_processati=yes&aggiungi=no&id=" + id,
					success: function() {
						
					}
				});
			}
		});
		</script>
';
		$irow = 0;
		if ($this->_list AND isset($this->fieldsDisplay['position']))
		{
			$positions = array_map(create_function('$elem', 'return (int)($elem[\'position\']);'), $this->_list);
			sort($positions);
		}
		if ($this->_list)
		{
			$isCms = false;
			if (preg_match('/cms/Ui', $this->identifier))
				$isCms = true;
			$keyToGet = 'id_'.($isCms ? 'cms_' : '').'category'.(in_array($this->identifier, array('id_category', 'id_cms_category')) ? '_parent' : '');
			$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
			
			foreach ($this->_list AS $tr)
			{
				$id = $tr[$this->identifier];
				$amazon_exists = Db::getInstance()->getValue("SELECT count(id_order) FROM amazon_orders WHERE id_order = '".$id."'");
				$eprice_exists = Db::getInstance()->getValue("SELECT count(id_order) FROM eprice_orders WHERE id_order = '".$id."'");
				if($amazon_exists > 0)
					$tr['color'] = '#000000';
				else if($eprice_exists > 0)
					$tr['color'] = '#ffff00';
					
				echo '<tr'.(array_key_exists($this->identifier,$this->identifiersDnd) ? ' id="tr_'.(($id_category = (int)(Tools::getValue('id_'.($isCms ? 'cms_' : '').'category', '1'))) ? $id_category : '').'_'.$id.'_'.$tr['position'].'"' : '').($irow++ % 2 ? ' class="alt_row"' : '').' '.((isset($tr['color']) ) ? 'style="background-color: '.$tr['color'].'"' : '').'>
							<td class="center">';
				if ($this->delete AND (!isset($this->_listSkipDelete) OR !in_array($id, $this->_listSkipDelete)))
					echo '<input type="checkbox" name="'.$this->table.'Box[]" value="'.$id.'" class="noborder" />';
				echo '</td>';
				foreach ($this->fieldsDisplay AS $key => $params)
				{
					$tmp = explode('!', $key);
					$key = isset($tmp[1]) ? $tmp[1] : $tmp[0];
					echo '
					<td '.($tr['color'] == '#000000' ? 'style="color:#ffffff"' : '').' '.(isset($params['position']) ? ' id="td_'.(isset($id_category) AND $id_category ? $id_category : 0).'_'.$id.'"' : '').' class="'.((!isset($this->noLink) OR !$this->noLink) ? 'pointer' : '').((isset($params['position']) AND $this->_orderBy == 'position')? ' dragHandle' : ''). (isset($params['align']) ? ' '.$params['align'] : '').'" ';
					if (!isset($params['position'])  AND $key != 'spring' AND (!isset($this->noLink) OR !$this->noLink))
						echo ' onclick="document.location = \'index.php?tab=AdminCustomers&id_customer='.$tr['id_customer'].'&viewcustomer&'.$this->identifier.'='.$id.($this->view? '&view' : '&update').$this->table.'&tab-container-1=4&token='.$tokenCustomers.'\'">'.(isset($params['prefix']) ? $params['prefix'] : '');
					else
						echo '>';
					if (isset($params['active']) AND isset($tr[$key]))
						$this->_displayEnableLink($token, $id, $tr[$key], $params['active'], Tools::getValue('id_category'), Tools::getValue('id_product'));
					elseif (isset($params['activeVisu']) AND isset($tr[$key]))
						echo '<img src="../img/admin/'.($tr[$key] ? 'enabled.gif' : 'disabled.gif').'"
						alt="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" title="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" />';
					elseif (isset($params['position']))
					{
						if ($this->_orderBy == 'position' AND $this->_orderWay != 'DESC')
						{
							echo '<a'.(!($tr[$key] != $positions[sizeof($positions) - 1]) ? ' style="display: none;"' : '').' href="'.$currentIndex.
									'&'.$keyToGet.'='.(int)($id_category).'&'.$this->identifiersDnd[$this->identifier].'='.$id.'
									&way=1&position='.(int)($tr['position'] + 1).'&token='.($token!=NULL ? $token : $this->token).'">
									<img src="../img/admin/'.($this->_orderWay == 'ASC' ? 'down' : 'up').'.gif"
									alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>';

							echo '<a'.(!($tr[$key] != $positions[0]) ? ' style="display: none;"' : '').' href="'.$currentIndex.
									'&'.$keyToGet.'='.(int)($id_category).'&'.$this->identifiersDnd[$this->identifier].'='.$id.'
									&way=0&position='.(int)($tr['position'] - 1).'&token='.($token!=NULL ? $token : $this->token).'">
									<img src="../img/admin/'.($this->_orderWay == 'ASC' ? 'up' : 'down').'.gif"
									alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a>';						}
						else
							echo (int)($tr[$key] + 1);
					}
					elseif (isset($params['image']))
					{
						// item_id is the product id in a product image context, else it is the image id.
						$item_id = isset($params['image_id']) ? $tr[$params['image_id']] : $id;
						// If it's a product image
						if (isset($tr['id_image']))
						{
							$image = new Image((int)$tr['id_image']);
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$image->getExistingImgPath().'.'.$this->imageType;
						}else
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$item_id.(isset($tr['id_image']) ? '-'.(int)($tr['id_image']) : '').'.'.$this->imageType;

						echo cacheImage($path_to_image, $this->table.'_mini_'.$item_id.'.'.$this->imageType, 45, $this->imageType);
					}
					elseif (isset($params['icon']) AND (isset($params['icon'][$tr[$key]]) OR isset($params['icon']['default'])))
						echo '<img src="../img/admin/'.(isset($params['icon'][$tr[$key]]) ? $params['icon'][$tr[$key]] : $params['icon']['default'].'" alt="'.$tr[$key]).'" title="'.$tr[$key].'" />';
					elseif (isset($params['price']))
						echo Tools::displayPrice($tr[$key], (isset($params['currency']) ? Currency::getCurrencyInstance((int)($tr['id_currency'])) : $currency), false);
					elseif (isset($params['float']))
						echo rtrim(rtrim($tr[$key], '0'), '.');
					elseif (isset($params['type']) AND $params['type'] == 'date')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang);
					elseif (isset($params['type']) AND $params['type'] == 'datetime')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang, true);
					elseif (isset($tr[$key]))
					{
						if($key == 'spring')
							echo '<input type="checkbox" name="spring_'.$id.'" id="spring_'.$id.'" '.($tr[$key] == 1 ? 'checked="checked"' : '').' onchange="updSpringOrder('.$id.'); return false;" />';
						else
						{	
							$echo = ($key == 'price' ? round($tr[$key], 2) : isset($params['maxlength']) ? Tools::substr($tr[$key], 0, $params['maxlength']).'...' : $tr[$key]);
							echo isset($params['callback']) ? call_user_func_array(array($this->className, $params['callback']), array($echo, $tr)) : $echo;
						}
					}
					else
						echo '--';

					echo (isset($params['suffix']) ? $params['suffix'] : '').
					'</td>';
				}

				if ($this->edit OR $this->delete OR ($this->view AND $this->view !== 'noActionColumn'))
				{
					echo '<td class="center" style="white-space: nowrap;">';
					if ($this->view)
						$this->_displayViewLink($token, $id);
					if ($this->edit)
						$this->_displayEditLink($token, $id);
					if ($this->delete AND (!isset($this->_listSkipDelete) OR !in_array($id, $this->_listSkipDelete)))
						$this->_displayDeleteLink($token, $id);
					if ($this->duplicate)
						$this->_displayDuplicate($token, $id);
					echo '</td>';
				}
				echo '</tr>';
			}
		}
	}
	
}

