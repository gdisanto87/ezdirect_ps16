<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14846 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminInformation extends AdminTab
{
	private function _getTestResultHtml()
	{
		$html = '';
		// Functions list to test with 'test_system'
		$funcs = array('fopen', 'fclose', 'fread', 'fwrite', 'rename', 'file_exists', 'unlink', 'rmdir', 'mkdir', 'getcwd', 'chdir', 'chmod');
		
		// Test list to execute (function/args)
		$tests = array(
			'phpversion' => false,
			'upload' => false,
			'system' => $funcs,
			'gd' => false,
			'mysql_support' => false,
			'config_dir' => PS_ADMIN_DIR.'/../config/',
			'tools_dir' => PS_ADMIN_DIR.'/../tools/smarty/compile',
			'cache_dir' => PS_ADMIN_DIR.'/../tools/smarty/cache/',
			'sitemap' => PS_ADMIN_DIR.'/../sitemap.xml',
			'img_dir' => PS_ADMIN_DIR.'/../img/',
			'mails_dir' => PS_ADMIN_DIR.'/../mails/',
			'module_dir' => PS_ADMIN_DIR.'/../modules/',
			'theme_lang_dir' => PS_ADMIN_DIR.'/../themes/'._THEME_NAME_.'/lang/',
			'translations_dir' => PS_ADMIN_DIR.'/../translations/',
			'customizable_products_dir' => PS_ADMIN_DIR.'/../upload/',
			'virtual_products_dir' => PS_ADMIN_DIR.'/../download/'
		);

		$tests_op = array(
			'fopen' => false,
			'register_globals' => false,
			'gz' => false
		);

		$testsErrors = array(
			'phpversion' => $this->l('Update your PHP version'),
			'upload' => $this->l('Configure your server to allow the upload file'),
			'system' => $this->l('Configure your server to allow the creation of directories and write to files'),
			'gd' => $this->l('Enable the GD library on your server'),
			'mysql_support' => $this->l('Enable the MySQL support on your server'),
			'config_dir' => $this->l('Set write permissions for config folder'),
			'tools_dir' => $this->l('Set write permissions for tools folder'),
			'cache_dir' => $this->l('Set write permissions for cache folder'),
			'sitemap' => $this->l('Set write permissions for sitemap.xml file'),
			'img_dir' => $this->l('Set write permissions for img folder and subfolders/recursively'),
			'mails_dir' => $this->l('Set write permissions for mails folder and subfolders/recursively'),
			'module_dir' => $this->l('Set write permissions for modules folder and subfolders/recursively'),
			'theme_lang_dir' => $this->l('Set write permissions for themes/')._THEME_NAME_.$this->l('/lang/ folder and subfolders/recursively'),
			'translations_dir' => $this->l('Set write permissions for translations folder and subfolders/recursively'),
			'customizable_products_dir' => $this->l('Set write permissions for upload folder and subfolders/recursively'),
			'virtual_products_dir' => $this->l('Set write permissions for download folder and subfolders/recursively'),
			'fopen' => $this->l('Enable fopen on your server'),
			'register_globals' => $this->l('Set PHP register global option to off'),
			'gz' => $this->l('Enable GZIP compression on your server')
		);

		$paramsRequiredResults = self::check($tests);
		$paramsOptionalResults = self::check($tests_op);

		$html .= '
			<p>
				<b>'.$this->l('Required parameters').':</b>';
		if (!in_array('fail', $paramsRequiredResults))
				$html .= ' <span style="color:green;font-weight:bold;">OK</span>
			</p>
			';
		else
		{
			$html .= ' <span style="color:red">'.$this->l('Please consult the following error(s)').'</span>
			</p>
			<ul>
			';
			foreach ($paramsRequiredResults AS $key => $value)
				if ($value == 'fail')
					$html .= '<li>'.$testsErrors[$key].'</li>';
			$html .= '</ul>';
		}
		
		$html .= '
			<p>
				<b>'.$this->l('Optional parameters').':</b>';
		if (!in_array('fail', $paramsOptionalResults))
				$html .= ' <span style="color:green;font-weight:bold;">OK</span>
			</p>
			';
		else
		{
			$html .= ' <span style="color:red">'.$this->l('Please consult the following error(s)').'</span>
			</p>
			<ul>
			';
			foreach ($paramsOptionalResults AS $key => $value)
				if ($value == 'fail')
					$html .= '<li>'.$testsErrors[$key].'</li>';
			$html .= '</ul>';
		}
		
		return $html;
	}
	
	public function postProcess()
	{
		/* PrestaShop demo mode */
		if (_PS_MODE_DEMO_)
		{
			$this->_errors[] = Tools::displayError('This functionnality has been disabled.');
			return;
		}
		/* PrestaShop demo mode*/
		parent::postProcess();
	}
	
	public function display()
	{
		global $cookie;
		
		
		if($cookie->id_employee == 6 || $cookie->id_employee == 22)
		{
		
			if(Tools::getIsset('conferma-prezzi'))
			{
				echo "CIAO";
				foreach($_POST['id_product'] as $id_product=>$id_product_g)
				{
					if(isset($_POST['aggiorna'][$id_product]))
					{
						$id_da_inserire = $_POST['id_product'][$id_product];
						Db::getInstance()->executeS("DELETE FROM specific_price WHERE id_product = ".$id_da_inserire." AND specific_price_name = 'sc_qta_1'");
						Db::getInstance()->executeS("DELETE FROM specific_price WHERE id_product = ".$id_da_inserire." AND specific_price_name = 'sc_qta_2'");
						Db::getInstance()->executeS("DELETE FROM specific_price WHERE id_product = ".$id_da_inserire." AND specific_price_name = 'sc_qta_3'");
						Db::getInstance()->executeS("DELETE FROM specific_price WHERE id_product = ".$id_da_inserire." AND specific_price_name = 'sc_riv_1'");
						
						$prezzoweb = floatval(str_replace(',', '.', $_POST['price'][$id_da_inserire]));
						$listino = floatval(str_replace(',', '.', $_POST['listino'][$id_da_inserire]));
						$sc_acq_1 = floatval(str_replace(',', '.', $_POST['sconto_acquisto_1'][$id_da_inserire]));
						$sc_acq_2 = floatval(str_replace(',', '.', $_POST['sconto_acquisto_2'][$id_da_inserire]));
						$sc_acq_3 = floatval(str_replace(',', '.', $_POST['sconto_acquisto_3'][$id_da_inserire]));
						$netto = floatval(str_replace(',', '.', $_POST['wholesale_price'][$id_da_inserire]));
						$sc_clienti = floatval(str_replace(',', '.', $_POST['scontolistinovendita'][$id_da_inserire]));
						$sc_riv_1 = floatval(str_replace(',', '.', $_POST['sconto_rivenditore_1'][$id_da_inserire]));
						$sc_riv_2 = floatval(str_replace(',', '.', $_POST['sconto_rivenditore_2'][$id_da_inserire]));
	
						$product_inserction = ("UPDATE product 
						SET 
						id_tax_rules_group = 1,
						price = '".$prezzoweb."',
						listino = '".$listino."',
						wholesale_price = '".$netto."',
						sconto_acquisto_1 = '".$sc_acq_1."',
						sconto_acquisto_2 = '".$sc_acq_2."',
						sconto_acquisto_3 = '".$sc_acq_3."',
						scontolistinovendita = '".$sc_clienti."'
						WHERE id_product = '".addslashes($id_da_inserire)."'

						");
						
						Db::getInstance()->executeS($product_insertion);
						
						

					}
				}
			}
			
			
			echo '	<br /><br />	<fieldset id="cambioprezzi">
				<legend>Cambioprezzi</legend>
			</fieldset>';
			
			echo '<form action="?tab=AdminInformation&token='.$this->token.'&step=cambioprezzi" id="cambioprezzi" method="post">';
			$prodotti = Db::getInstance()->executeS('SELECT p.*, pl.name FROM product p JOIN product_lang pl 
			ON pl.id_product = p.id_product WHERE id_category_default = 156');
			
			$tax_rules_groups = TaxRulesGroup::getTaxRulesGroups(true);
					$taxesRatesByGroup = TaxRulesGroup::getAssociatedTaxRatesByIdCountry(Country::getDefaultCountryId());
					$ecotaxTaxRate = Tax::getProductEcotaxRate();
					echo '<script type="text/javascript">';
					echo 'noTax = '.(Tax::excludeTaxeOption() ? 'true' : 'false'), ";\n";
					echo 'taxesArray = new Array ();'."\n";
					echo 'taxesArray[0] = 0', ";\n";

					foreach ($tax_rules_groups AS $tax_rules_group)
					{
						$tax_rate = (array_key_exists($tax_rules_group['id_tax_rules_group'], $taxesRatesByGroup) ?  $taxesRatesByGroup[$tax_rules_group['id_tax_rules_group']] : 0);
						echo 'taxesArray['.$tax_rules_group['id_tax_rules_group'].']='.$tax_rate."\n";
					}
					echo '
						ecotaxTaxRate = '.($ecotaxTaxRate / 100).';
					</script>';
			echo '
			<select style="display:none" onChange="javascript:calcPriceTI(); unitPriceWithTax(\'unit\');" name="id_tax_rules_group" id="id_tax_rules_group" '.(Tax::excludeTaxeOption() ? 'disabled="disabled"' : '' ).'>
						';

						foreach ($tax_rules_groups AS $tax_rules_group)
							echo '<option value="'.$tax_rules_group['id_tax_rules_group'].'" '.(($this->getFieldValue($obj, 'id_tax_rules_group') == $tax_rules_group['id_tax_rules_group']) ? ' selected="selected"' : '').'>'.Tools::htmlentitiesUTF8($tax_rules_group['name']).'</option>';
				//echo ' <option value="0">'.$this->l('No Tax').'</option>';
				echo '</select>
				<link type="text/css" rel="stylesheet" href="fixedHeaderTable.css" />	
			<style type="text/css">
			
			#cambioprezzi input {
				width:50px;
				text-align:right;
			}
			
			.thclass div {
				width:85px !important;
			}
			
			section.positioned {
			  position: absolute;
			  top:100px;
			  left:100px;
			  width:3000px;
			  box-shadow: 0 0 15px #333;
			}
			.container {
			  overflow-y: auto;
			  height: 400px;
			}

			</style>
		
			<script type="text/javascript" src="jquery.fixedheadertable.min.js"></script>
			<script type="text/javascript">
			$(document).ready(function() {
				
				$("#strumento_prezzi").fixedHeaderTable({ footer: true, cloneHeadToFoot: false, fixedColumn: true, fixedColumns: 3 });
			
			});
			
			</script>
			
			<script type="text/javascript" src="../js/price.js"></script>
			
			
			
			
			';
			echo '<div class="table_container" style="width:928px; height:500px; overflow:auto; position:relative;"><table class="fancyTable" id="strumento_prezzi" 
			style="position:relative;">
			<thead>
			<tr>
			
			<th><div style="width:100px; display:block">Cod. EZ</div></th>
			<th><div style="width:100px; display:block">Cod. SKU</div></th>
			<th><div style="width:250px; display:block">Desc</div></th>
			<th><div style="width:20px; display:block">Agg.?</div></th>
			<th class="thclass"><div>Listino</div></th>
			<th class="thclass"><div>Sc. acq 1</div></th>
			<th class="thclass"><div>Sc. acq 2</div></th>
			<th class="thclass"><div>Sc. acq 3</div></th>
			<th class="thclass"><div>Netto acq.</div></th>
			<th class="thclass"><div>Sc. cli</div></th>
			<th class="thclass"><div>Prz. web</div></th>
			<th class="thclass"><div>Margine %</div></th>
			<th class="thclass"><div>Sc. qta 1</div></th>
			<th class="thclass"><div>Qta 1</div></th>
			<th class="thclass"><div>Prz qta 1</div></th>
			<th class="thclass"><div>Marg qt 1</div></th>
			<th class="thclass"><div>Sc. qta 2</div></th>
			<th class="thclass"><div>Qta 2</div></th>
			<th class="thclass"><div>Prz qta 2</div></th>
			<th class="thclass"><div>Marg qt 2</div></th>
			<th class="thclass"><div>Sc. qta 3</div></th>
			<th class="thclass"><div>Qta 3</div></th>
			<th class="thclass"><div>Prz qta 3</div></th>
			<th class="thclass"><div>Marg qt 3</div></th>
			<th class="thclass"><div>Sc. riv 1</div></th>
			<th class="thclass"><div>Prz riv 1</div></th>
			<th class="thclass"><div>Mrg riv 1</div></th>
			<th class="thclass"><div>Sc. riv 2</div></th>
			<th class="thclass"><div>Prz riv 2</div></th>
			<th class="thclass"><div>Mrg riv 2</div></th>
			<th class="thclass"><div>Sc. distr.</div></th>
			<th class="thclass"><div>Prz distr.</div></th>
			<th class="thclass"><div>Mrg distr.</div></th>
			</tr></thead>';
			
			foreach($prodotti as $product)
			{
				$sc_cli = $product['scontolistinovendita'];
				if($sc_cli <= 0)
					$sc_cli = 100-(($product['price']*100)/$product['listino']);
				
				$sc_qta_1 = Db::getInstance()->getValue("SELECT reduction FROM specific_price WHERE id_product = ".$product['id_product']." AND specific_price_name = 'sc_qta_1'");
				if($sc_qta_1 > 0) {		
					$qta_1 = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE id_product = ".$product['id_product']." AND specific_price_name = 'sc_qta_1'");
					$prz_sc_qta_1 = $product['price']-($product['price']*$sc_qta_1);
					$marg_qta_1 = ($product['price']-($product['price']*$sc_qta_1)-$product['wholesale_price'])/($product['price']-($product['price']*$sc_qta_1))*100;
				}
				else {
					$qta_1 = 0;
					$prz_sc_qta_1 = 0;
					$marg_qta_1 = 0;
				}
				
				$sc_qta_2 = Db::getInstance()->getValue("SELECT reduction FROM specific_price WHERE id_product = ".$product['id_product']." AND specific_price_name = 'sc_qta_2'");
				if($sc_qta_2 > 0) {		
					$qta_2 = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE id_product = ".$product['id_product']." AND specific_price_name = 'sc_qta_2'");
					$prz_sc_qta_2 = $product['price']-($product['price']*$sc_qta_2);
					$marg_qta_2 = ($product['price']-($product['price']*$sc_qta_2)-$product['wholesale_price'])/($product['price']-($product['price']*$sc_qta_2))*100;
				}
				else {
					$qta_2 = 0;
					$prz_sc_qta_2 = 0;
					$marg_qta_2 = 0;
				}
				
				$sc_qta_3 = Db::getInstance()->getValue("SELECT reduction FROM specific_price WHERE id_product = ".$product['id_product']." AND specific_price_name = 'sc_qta_3'");
				if($sc_qta_3 > 0) {		
					$qta_3 = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE id_product = ".$product['id_product']." AND specific_price_name = 'sc_qta_3'");
					$prz_sc_qta_3 = $product['price']-($product['price']*$sc_qta_3);
					$marg_qta_3 = ($product['price']-($product['price']*$sc_qta_3)-$product['wholesale_price'])/($product['price']-($product['price']*$sc_qta_3))*100;
				}
				else {
					$qta_3 = 0;
					$prz_sc_qta_3 = 0;
					$marg_qta_3 = 0;
				}
				
				$sc_riv_1 = Db::getInstance()->getValue("SELECT reduction FROM specific_price WHERE id_product = ".$product['id_product']." AND specific_price_name = 'sc_riv_1'");
				if($sc_riv_1 > 0) {		
					$prz_sc_riv_1 = $product['listino']-($product['listino']*$sc_riv_1);
					$marg_riv_1 = ($product['listino']-($product['listino']*$sc_riv_1)-$product['wholesale_price'])/($product['price']-($product['price']*$sc_riv_1))*100;
				}
				else {
					$prz_sc_riv_1 = 0;
					$marg_riv_1 = 0;
				}
				
				$sc_riv_2 = Db::getInstance()->getValue("SELECT reduction FROM specific_price WHERE id_product = ".$product['id_product']." AND specific_price_name = 'sc_riv_2'");
				if($sc_riv_2 > 0) {	
					$prz_sc_riv_2 = $product['listino']-($product['listino']*$sc_riv_2);
					$marg_riv_2 = ($product['listino']-($product['listino']*$sc_riv_2)-$product['wholesale_price'])/($product['price']-($product['price']*$sc_riv_2))*100;
				}
				else {
					$prz_sc_riv_2 = 0;
					$marg_riv_2 = 0;
				}
				
				$sc_riv_3 = Db::getInstance()->getValue("SELECT reduction FROM specific_price WHERE id_product = ".$product['id_product']." AND specific_price_name = 'sc_riv_3'");
				if($sc_riv_3 > 0) {	
					$prz_sc_distr = $product['listino']-($product['listino']*$sc_riv_3);
					$marg_riv_3 = ($product['listino']-($product['listino']*$sc_riv_3)-$product['wholesale_price'])/($product['price']-($product['price']*$sc_riv_3))*100;
				}
				else {
					$prz_sc_riv_3 = 0;
					$marg_riv_3 = 0;
				}
				
				$marg = (($product['price']-$product['wholesale_price'])*100)/$product['price'];
			
				echo '
			<tr>
			<td style="width:100px">'.$product['reference'].'</td>
			<td style="width:100px">'.$product['supplier_reference'].'</td>
			<td style="width:250px"><a href="?tab=AdminCatalog&id_product='.$product['id_product'].'&updateproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.$product['name'].'</a></td>
			
			<td style="width:20px"><input type="checkbox" name="aggiorna['.$product['id_product'].']" value="1" />
			<input type="hidden" name="id_product['.$product['id_product'].']" value="'.$product['id_product'].'" />
			</td>
			
			<td><input type="text" id="listino" name="listino" onchange="this.value = this.value.replace(/,/g, \'.\');" value="'.number_format($product['listino'],2,",","").'" onkeyup="
							calcPrezzoSconto(\'scontolistinovendita\', \'priceTE\', \'listino\');
							calcScontoAcquisto1(); calcScontoAcquisto2(); calcScontoAcquisto3(); 
							calcMarginalita(); calcSconto(\'listino\', \'priceTE\', \'scontolistinovendita\');
							calcMarginalitaSconto(\'sconto_rivenditore_1\', \'margin_riv_1\', \'listino\'); calcPrezzoSconto(\'sconto_rivenditore_1\', \'prezzo_riv_1\', \'listino\');
							calcMarginalitaSconto(\'sconto_rivenditore_2\', \'margin_riv_2\', \'listino\'); calcPrezzoSconto(\'sconto_rivenditore_2\', \'prezzo_riv_2\', \'listino\');
							calcMarginalitaSconto(\'sconto_rivenditore_3\', \'margin_riv_3\', \'listino\'); calcPrezzoSconto(\'sconto_rivenditore_3\', \'prezzo_riv_3\', \'listino\');" /></td>
							
			<td><input type="text" id="sconto_acquisto_1" name="sconto_acquisto_1" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcScontoAcquisto1(); calcMarginalita(); 
						
							calcMarginalitaSconto(\'sconto_rivenditore_1\', \'margin_riv_1\', \'listino\'); 
							calcMarginalitaSconto(\'sconto_rivenditore_2\', \'margin_riv_2\', \'listino\');
							calcMarginalitaSconto(\'sconto_rivenditore_3\', \'margin_riv_3\', \'listino\');
							calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); 
							calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); 
							calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\')							
							" value="'.number_format($product['sconto_acquisto_1'],2,",","").'" />%</td>
			
			<td><input id="sconto_acquisto_2" name="sconto_acquisto_2"  onchange="this.value = this.value.replace(/,/g, \'.\');" type="text" value="'.number_format($product['sconto_acquisto_2'],2,",","").'" onkeyup="if (isArrowKey(event)) return; calcScontoAcquisto2(); calcMarginalita();
	calcMarginalitaSconto(\'sconto_rivenditore_1\', \'margin_riv_1\', \'listino\'); calcPrezzoSconto(\'sconto_rivenditore_1\', \'prezzo_riv_1\', \'listino\');
							calcMarginalitaSconto(\'sconto_rivenditore_2\', \'margin_riv_2\', \'listino\'); 
							calcMarginalitaSconto(\'sconto_rivenditore_3\', \'margin_riv_3\', \'listino\'); 
							calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); 
							calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); 
							calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\')
							" />%</td>
			<td><input id="sconto_acquisto_3" name="sconto_acquisto_3" type="text" value="'.number_format($product['sconto_acquisto_3'],2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcScontoAcquisto3(); calcMarginalita();	calcMarginalitaSconto(\'sconto_rivenditore_1\', \'margin_riv_1\', \'listino\');
							calcMarginalitaSconto(\'sconto_rivenditore_2\', \'margin_riv_2\', \'listino\'); 
							calcMarginalitaSconto(\'sconto_rivenditore_3\', \'margin_riv_3\', \'listino\'); 
							calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); 
							calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); 
							calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\')

							" />%</td>
			<td><input id="wholesale_price" name="wholesale_price"  type="text" value="'.number_format($product['wholesale_price'],2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');
							calcMarginalita();
							calcSconto(\'listino\', \'wholesale_price\', \'sconto_acquisto_1\');
							document.getElementById(\'sconto_acquisto_2\').value = 0;
							document.getElementById(\'sconto_acquisto_3\').value = 0;
							calcMarginalitaSconto(\'sconto_rivenditore_1\', \'margin_riv_1\', \'listino\');
							calcMarginalitaSconto(\'sconto_rivenditore_2\', \'margin_riv_2\', \'listino\'); 
							calcMarginalitaSconto(\'sconto_rivenditore_3\', \'margin_riv_3\', \'listino\'); 
							calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); 
							calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); 
							calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\')
							
							"  /></td>
			
			<td><input id="scontolistinovendita" name="scontolistinovendita" type="text" value="'.number_format($sc_cli,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return;  calcPrezzoSconto(\'scontolistinovendita\', \'priceTE\', \'listino\');
							calcMarginalita(); 
									calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_1\', \'prezzo_qta_1\', \'priceTE\');
							calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_2\', \'prezzo_qta_2\', \'priceTE\');
							calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_3\', \'prezzo_qta_3\', \'priceTE\');
							
							" />%</td>
			<td><input id="priceTE" name="price" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcMarginalita(); calcSconto(\'listino\', \'priceTE\', \'scontolistinovendita\'); calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_1\', \'prezzo_qta_1\', \'priceTE\');
							calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_2\', \'prezzo_qta_2\', \'priceTE\');
							calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_3\', \'prezzo_qta_3\', \'priceTE\');
							
							" type="text" value="'.number_format($product['price'],2,",","").'" /></td>
			<td><input id="marginalita" name="marginalita" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcPriceTI();" readonly="readonly" type="text" value="'.number_format($marg,2,",","").'" />%</td>
			<td><input id="sconto_quantita_1" name="sconto_quantita_1" type="text" value="'.number_format($sc_qta_1*100,2,",","").'" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto(\'sconto_quantita_1\', \'margin_qta_1\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_1\', \'prezzo_qta_1\', \'priceTE\');" />%</td>
			
			<td><input id="quantita_1" name="quantita_1" type="text" value="'.$qta_1.'" /></td>
			
			<td><input id="prezzo_qta_1" name="prezzo_qta_1" type="text" value="'.number_format($prz_sc_qta_1,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'priceTE\', \'prezzo_qta_1\', \'sconto_quantita_1\');" /></td>
			
			<td><input type="text" id="margin_qta_1"  value="'.number_format($marg_qta_1,2,",","").'" name="margin_qta_1" type="text" onchange="this.value = this.value.replace(/,/g, \'.\');" readonly="readonly" />%</td>
			
			<td><input id="sconto_quantita_2" name="sconto_quantita_2" type="text" value="'.number_format($sc_qta_2*100,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto(\'sconto_quantita_2\', \'margin_qta_2\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_2\', \'prezzo_qta_2\', \'priceTE\');" />%</td>
			
			<td><input id="quantita_2" name="quantita_2" type="text" value="'.$qta_2.'" /></td>
			
			<td><input type="text" id="prezzo_qta_2" name="prezzo_qta_2"  value="'.number_format($prz_sc_qta_2,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'priceTE\', \'prezzo_qta_2\', \'sconto_quantita_2\');" /></td>
			<td><input type="text" id="margin_qta_2" value="'.number_format($marg_qta_2,2,",","").'" name="margin_qta_2" type="text" onchange="this.value = this.value.replace(/,/g, \'.\');" readonly="readonly" />%</td>
			
			<td><input id="sconto_quantita_3" name="sconto_quantita_3" type="text" value="'.number_format($sc_qta_3*100,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto(\'sconto_quantita_3\', \'margin_qta_3\', \'priceTE\'); calcPrezzoSconto(\'sconto_quantita_3\', \'prezzo_qta_3\', \'priceTE\');" />%</td>
			
			<td><input id="quantita_3" name="quantita_3" type="text" value="'.$qta_3.'" /></td>
			
			<td><input id="prezzo_qta_3" name="prezzo_qta_3"  type="text" value="'.number_format($prz_sc_qta_3,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'priceTE\', \'prezzo_qta_3\', \'sconto_quantita_3\');" /></td>
			<td><input id="margin_qta_3" name="margin_qta_3" readonly="readonly" type="text" value="'.number_format($marg_qta_3,2,",","").'" />%</td>
			
			<td><input id="sconto_rivenditore_1" name="sconto_rivenditore_1" type="text" value="'.number_format($sc_riv_1*100,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto(\'sconto_rivenditore_1\', \'margin_riv_1\', \'listino\'); calcPrezzoSconto(\'sconto_rivenditore_1\', \'prezzo_riv_1\', \'listino\');" />%</td>
			
			<td><input id="prezzo_riv_1" name="prezzo_riv_1"  type="text" value="'.number_format($prz_sc_riv_1,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'listino\', \'prezzo_riv_1\', \'sconto_rivenditore_1\');" /></td>
			
			<td><input id="margin_riv_1" name="margin_riv_1" type="text" value="'.number_format($marg_riv_1,2,",","").'"  onchange="this.value = this.value.replace(/,/g, \'.\');" readonly="readonly" />%</td>
			
			<td><input id="sconto_rivenditore_2" name="sconto_rivenditore_2" type="text" value="'.number_format($sc_riv_2*100,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto(\'sconto_rivenditore_2\', \'margin_riv_2\', \'listino\'); calcPrezzoSconto(\'sconto_rivenditore_2\', \'prezzo_riv_2\', \'listino\');" />%</td>
			<td><input id="prezzo_riv_2"  name="prezzo_riv_2" type="text" value="'.number_format($prz_sc_riv_2,2,",","").'"  onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="calcSconto(\'listino\', \'prezzo_riv_2\', \'sconto_rivenditore_2\');" /></td>
			<td><input type="text" id="margin_riv_2" name="margin_riv_2" type="text" onchange="this.value = this.value.replace(/,/g, \'.\');" value="'.number_format($marg_riv_2,2,",","").'" readonly="readonly" />%</td>
			
			<td><input id="sconto_rivenditore_3" name="sconto_rivenditore_3" type="text" value="'.number_format($sc_riv_3*100,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');" onkeyup="if (isArrowKey(event)) return; calcMarginalitaSconto(\'sconto_rivenditore_3\', \'margin_riv_3\', \'listino\'); calcPrezzoSconto(\'sconto_rivenditore_3\', \'prezzo_riv_3\', \'listino\');" />%</td>
			<td><input id="prezzo_riv_3" name="prezzo_riv_3" type="text" value="'.number_format($prz_sc_riv_3,2,",","").'" onchange="this.value = this.value.replace(/,/g, \'.\');"  onkeyup="calcSconto(\'listino\', \'prezzo_riv_3\', \'sconto_rivenditore_3\');" /></td>
			<td><input type="text" id="margin_riv_3" name="margin_riv_3" value="'.number_format($marg_riv_3,2,",","").'"  onchange="this.value = this.value.replace(/,/g, \'.\');" readonly="readonly" />%</td>
			</tr>';
		
		
		
		
			}
			
			echo '</table></div>';
			echo '<input type="submit" name="conferma-prezzi" value="Conferma modifiche" style="width:160px; text-align:center; cursor:pointer" class="button" />';
			echo '</form>';
			
			
			
		}
		
		
		
		
	}
	
	static private function check($tests)
	{
		$res = array();
		foreach ($tests AS $key => $test)
			$res[$key] = self::run($key, $test);
		return $res;
	}
	
	static private function run($ptr, $arg = 0)
	{
		if (call_user_func(array('self', 'test_'.$ptr), $arg))
			return ('ok');
		return ('fail');
	}
	
	// Misc functions	
	static private function test_phpversion()
	{
		return PHP_VERSION_ID >= 50000; /* PHP version > 5.0 */
	}
	
	static private function test_mysql_support()
	{
		return function_exists('mysql_connect');
	}

	static private function test_upload()
	{
		return  ini_get('file_uploads');
	}

	static private function test_fopen()
	{
		return ini_get('allow_url_fopen');
	}

	static private function test_system($funcs)
	{
		foreach ($funcs AS $func)
			if (!function_exists($func))
				return false;
		return true;
	}

	static private function test_gd()
	{
		return function_exists('imagecreatetruecolor');
	}
	
	static private function test_register_globals()
	{
		return !ini_get('register_globals');
	}
	
	static private function test_gz()
	{
		if (function_exists('gzencode'))
			return !(@gzencode('dd') === false); 
		return false;
	}
	
	// is_writable dirs	
	static private function test_dir($dir, $recursive = false)
	{
		if (!is_writable($dir) OR !$dh = opendir($dir))
			return false;
		if ($recursive)
		{
			while (($file = readdir($dh)) !== false)
				if (is_dir($dir.$file) AND $file != '.' AND $file != '..')
					if (!self::test_dir($dir.$file, true))
						return false;
		}
		closedir($dh);
		return true;
	}
	
	// is_writable files	
	static private function test_file($file)
	{
		return (file_exists($file) AND is_writable($file));
	}
	
	static private function test_config_dir($dir)
	{
		return self::test_dir($dir);
	}
	
	static private function test_sitemap($dir)
	{
		return self::test_file($dir);
	}
	
	static private function test_root_dir($dir)
	{
		return self::test_dir($dir);
	}

	static private function test_admin_dir($dir)
	{
		return self::test_dir($dir);
	}
	
	static private function test_img_dir($dir)
	{
		return self::test_dir($dir, true);
	}
	
	static private function test_module_dir($dir)
	{
		return self::test_dir($dir, true);
	}
	
	static private function test_tools_dir($dir)
	{
		return self::test_dir($dir);
	}
	
	static function test_cache_dir($dir)
	{
		return self::test_dir($dir);
	}
	
	static private function test_download_dir($dir)
	{
		return self::test_dir($dir);
	}
	
	static private function test_mails_dir($dir)
	{
		return self::test_dir($dir, true);
	}
	
	static private function test_translations_dir($dir)
	{
		return self::test_dir($dir, true);
	}
	
	static private function test_theme_lang_dir($dir)
	{
		return self::test_dir($dir, true);
	}

	static private function test_customizable_products_dir($dir)
	{
		return self::test_dir($dir);
	}
	
	static private function test_virtual_products_dir($dir)
	{
		return self::test_dir($dir);
	}
}
