<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14632 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class RicercaPrezzi extends AdminTab
{
	public function getLinks($link, $search)
    {
        /*** return array ***/
        $ret = array();

        /*** a new dom object ***/
        $dom = new domDocument;

        /*** get the HTML (suppress errors) ***/
        @$dom->loadHTML($link);

        /*** remove silly white space ***/
        $dom->preserveWhiteSpace = false;

        /*** get the links from the HTML ***/
        $links = $dom->getElementsByTagName('a');
		
		$search_words = explode(" ",$search);
		$search_pattern = "";
		
		foreach ($search_words as $word)
		{
			$word = "(?=.*\b".$word."\b)";
			$search_pattern .= $word;
		}		
    
        /*** loop over the links ***/
        foreach ($links as $tag)
        {
			if(preg_match("/\b".$search_pattern."\b/i", $tag->c14n())) {
				$ret[$tag->getAttribute('href')] = $tag->childNodes->item(0)->nodeValue;
			}
        }

        return $ret;
    }
	
	
	public function getData($html, $price_id)
    {
        /*** a new dom object ***/
        $dom = new domDocument;

        /*** get the HTML (suppress errors) ***/
        @$dom->loadHTML($html);

        /*** get the links from the HTML ***/
        $tag = $dom->getElementById($price_id);
		$h1Nodes = $dom->getElementsByTagName("h1");
		foreach ($h1Nodes as $node) {
			$h1 = $node->c14n();
		}
		
		if(!$tag) 
		{
			$finder = new DomXPath($dom);
			$classname=$price_id;
			$nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), '$classname')]");
			foreach ($nodes as $node) 
			{
				$tag = $node;
				$price = $tag->c14n();
			}
			
			
		}
		else
		{
			$price = $tag->c14n();
		}
		
		$array_return = array();
		$array_return['price'] = $price;
		$array_return['title'] = strip_tags($h1);
		
        return $array_return;
    }
	
	
	public function display() 
	{
		echo "<style type='text/css'>
		#progress-bar {
		width: 200px;
		padding: 2px;
		border: 2px solid #aaa;
		background: #fff;
		}

		#progress {
			background: #000;
			color: #fff;
			overflow: hidden;
			white-space: nowrap;
			padding: 5px 0;
			text-indent: 5px;
			width: 0%;
		}
		</style>";
		
		echo "
		
	
		<script type='text/javascript'>
		function updateProgress(percentage) {
			progressElement.style.width = percentage + '%';
			progressElement.innerHTML = percentage + '%';
		}
		</script>";

		echo "<h1>Ricerca prezzi dei prodotti sui siti della concorrenza</h1>";
		
		echo "<p>Attraverso questo programma puoi cercare prezzi dei prodotti sui siti della concorrenza. Inserisci nella cella sottostante un prodotto (puoi inserire il codice, oppure un nome, o una descrizione). Il programma cercher&agrave; nei motori di ricerca dei siti pre-caricati, controller&agrave; per ogni sito i risultati pi&ugrave; pertinenti per la tua ricerca, e mostrer&agrave; i prezzi dei prodotti. <br /><br />
		<strong>ATTENZIONE</strong>: Dal momento che il programma deve collegarsi a diversi siti, possono essere necessari alcuni secondi prima di vedere i risultati (circa UN MINUTO). I tempi di caricamento della pagina dipendono anche dalla velocit&agrave; di risposta dei server dei siti concorrenti e dalla velocit&agrave; della connessione su cui si sta lavorando.</p>
		
		<form name='ricerca_prezzi' method='post' action='index.php?tab=RicercaPrezzi&token=".$this->token."&s'>
		<input name='ricerca' type='text' style='width:350px' /> <input type='submit' value='Cerca' class='button' />
		
		</form>";
		
		if(Tools::getIsset('ricerca'))
		{	
		
			/*echo '<div id="progress-bar">
				<div id="progress">0%</div>
			</div>';
			*/
			
			echo '<div id="ricerca-in-corso" style="width:96%; margin-top:5px; margin-bottom:5px; text-align:left;padding:8px;border:1px solid #e62026;background-color:#ffffff;">RICERCA PREZZI IN CORSO... ATTENDERE... (<strong>PER RAGIONI DI SICUREZZA</strong>, il link ai siti della concorrenza non sono cliccabili).</div>';
			
			ob_flush();
			flush();
			function progress($resource,$download_size, $downloaded, $upload_size, $uploaded)
			{
				echo "<script type='text/javascript'>var progressElement = document.getElementById('progress')</script>";
				if($download_size > 0)
				{
					 echo '<script type="text/javascript">updateProgress('.number_format(($downloaded / $download_size  * 100),2,".","").");</script>";
				}	
			}	
			
			$array_websites = array();
			
			
			$array_websites[] = array(
				"sito" => "Voipshop",
				"base" => "http://www.voipshop.it",
				"url" => "http://www.voipshop.it/cerca",
				"post_data" => 'search_query',
				"id_prezzo" => "our_price_display",
				"method" => "post"
			);

			
			$array_websites[] = array(
				"sito" => "Onedirect",
				"base" => "http://www.onedirect.it",
				"url" => "http://www.onedirect.it/search?query=",
				"post_data" => 'query',
				"id_prezzo" => "v2_productRegularPrice",
				"method" => "get"
			);
			
			
			
			$array_websites[] = array(
				"sito" => "Eurylink",
				"base" => "http://shop.eurylink.com",
				"url" => "http://shop.eurylink.com/telefonia-ip/telefoni-per-conferenza/search.html",
				"post_data" => 'keyword',
				"id_prezzo" => "PricesalesPrice",
				"method" => "post"
			);
			
			
			
			$array_websites[] = array(
				"sito" => "TeknoTLC",
				"base" => "http://www.teknotlc.it",
				"url" => "http://www.teknotlc.it/advanced_search_result.php?keywords=",
				"post_data" => 'keywords',
				"id_prezzo" => "pageHeading",
				"method" => "get"
			);
			
			
			
			$array_websites[] = array(
				"sito" => "VoipMarket",
				"base" => "http://www.voipmarket.it",
				"url" => "http://www.voipmarket.it/cerca",
				"post_data" => 'search_query',
				"id_prezzo" => "our_price_display",
				"method" => "post"
			);
			
		
			$array_websites[] = array(
				"sito" => "100 ASA",
				"base" => "http://www.100asa.it",
				"url" => "http://www.100asa.it/advanced_search_result?main_page=advanced_search_result&search_in_description=1&keyword=",
				"post_data" => 'keyword',
				"id_prezzo" => "productPrices",
				"method" => "get"
			);
			
			
			
			$array_websites[] = array(
				"sito" => "XVoip",
				"base" => "https://www.xvoip.it",
				"url" => "https://www.xvoip.it/cerca",
				"post_data" => 'search_query',
				"id_prezzo" => "our_price_display",
				"method" => "post"
			);
			
			
			
			$array_websites[] = array(
				"sito" => "Misco",
				"base" => "http://www.misco.it",
				"url" => "http://www.misco.it/Search/Keyword",
				"post_data" => 'searchTerm',
				"id_prezzo" => "size20 b",
				"method" => "post"
			);
			
			
			/*
			$array_websites[] = array(
				"sito" => "Amazon",
				"base" => "http://www.amazon.it",
				"url" => "http://www.amazon.it/s/ref=nb_sb_noss",
				"post_data" => 'field-keywords',
				"id_prezzo" => "a-color-price",
				"method" => "post"
			);
			*/
			
			/*
			$array_websites[] = array(
				"sito" => "ePrice",
				"base" => "http://www.eprice.it",
				"url" => "http://www.eprice.it/search/qs=",
				"post_data" => 'inputSearch',
				"id_prezzo" => "priceContainingRegion",
				"method" => "get"
			);
			*/
		
		
			$array_websites[] = array(
				"sito" => "Monclick",
				"base" => "http://www.monclick.it",
				"url" => "www.monclick.it/risultati_ricerca?fh_view_size=10&fh_sort_by=-_match_rate,-orderby_piupopolari&fh_location=//root/it_IT/\$s=",
				"post_data" => 'testo',
				"id_prezzo" => "blu13",
				"method" => "get"
			);
			
			
			echo '<table class="table">';
			echo '<tr><th style="width:250px">Descr. prodotto su sito concorrente</th><th style="width:100px">Sito</th><th style="width:100px">Prezzo I.E. </th><th style="width:100px">Prezzo Riportato</th><th style="width:300px">Link</th></tr>';

			foreach($array_websites as $sito)
			{
				ob_flush();
				flush();
				$search = Tools::getValue('ricerca');
				
				if($sito['method'] == 'post')
					$curl_connection = curl_init($sito['url']);
				else if($sito['method'] == 'get')
				{
					$search_clean = str_replace(" ","+",$search);
					if($sito['sito'] == 'Monclick')
						$curl_connection = curl_init($sito['url'].$search_clean.'&testo='.$search_clean);
					else if($sito['sito'] == 'ePrice')
						$curl_connection = curl_init($sito['url'].$search_clean);
					else
						$curl_connection = curl_init($sito['url'].$search_clean);
						
				}	
				
				curl_setopt($curl_connection, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
				curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl_connection, CURLOPT_PROGRESSFUNCTION, 'progress');
				curl_setopt($curl_connection, CURLOPT_NOPROGRESS, false);
				curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, 1);
				
				$post_data[$sito['post_data']] = $search;
				foreach ( $post_data as $key => $value)
				{
					$post_items[] = $key . '=' . $value;
				}
				$post_string = implode ('&', $post_items);

				curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $post_string);
			
				$result = curl_exec($curl_connection);
				
				curl_close($curl_connection);

				$links = array();
				/*
				if(preg_match_all('/<a\s+href=["\']([^"\']+)["\']/i', $result, $links, PREG_PATTERN_ORDER))
					$all_hrefs = array_unique($links[1]);
					
				foreach($all_hrefs as $link)
					echo $link."<br />";
				*/
				 /*** get the links ***/
					$urls = $this->getLinks($result, $search);

					/*** check for results ***/
					if(sizeof($urls) > 0)
					{
						$count_urls = 0;
						foreach($urls as $key=>$value)
						{
							if(substr($key,0,7) != 'http://' && substr($key,0,8) != 'https://')
								$key = $sito['base'].$key;
								
							if( substr($key, -3) == 'jpg')
							{
							}
							else
							{
								ob_flush();
								flush();
								$curl_connection = curl_init($key);
								curl_setopt($curl_connection, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
								curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
								curl_setopt($curl_connection, CURLOPT_PROGRESSFUNCTION, 'progress');
								curl_setopt($curl_connection, CURLOPT_NOPROGRESS, false);
								curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
								curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, 1);
								$result = curl_exec($curl_connection);
								curl_close($curl_connection);
								$dati = $this->getData($result, $sito['id_prezzo']);
								if($dati['price'] == '')
								{
								}
								else
								{
								
									$prezzo_finito = '';
									$prezzo_riportato = strip_tags(mb_convert_encoding($dati['price'], "HTML-ENTITIES", "UTF-8"));
									switch($sito['sito'])
									{
										case 'Voipshop': $prezzo_finito = number_format((number_format(str_replace(",",".",str_replace(' €','',$prezzo_riportato)),2,".","")/(1.22)),2,",",""); break;
										case 'Onedirect': $prezzo_finito = str_replace(substr($prezzo_riportato,strpos($prezzo_riportato,'i')),'',$prezzo_riportato); $prezzo_finito = str_replace('€','',$prezzo_finito); $prezzo_finito = str_replace('&euro;','',$prezzo_finito); break;
										case 'Eurylink': $prezzo_finito = number_format((number_format(str_replace(' &euro;','',$prezzo_riportato),2,".","")/(1.22)),2,",",""); break;
										case 'TeknoTLC': $prezzo_finito = number_format((number_format(str_replace(",",".",str_replace('&#128;','',$prezzo_riportato)),2,".","")/(1.22)),2,",",""); break;
										case 'VoipMarket': $prezzo_finito = str_replace('&euro;','',$prezzo_riportato); break;
										case '100 ASA': $prezzo_finito = str_replace('Prezzo: &euro;','',$prezzo_riportato); break;
										case 'XVoip': $prezzo_finito = str_replace(' + IVA','',str_replace('â‚¬','',$prezzo_riportato)); break;
										case 'Misco': $prezzo_finito = str_replace('&euro; ','',$prezzo_riportato); break;
										case 'Monclick': $prezzo_finito = str_replace('&euro;','',str_replace('IVA esclusa:','',$prezzo_riportato)); break;
										default: $prezzo_finito = '*'; break;
									
									}
								
								
									echo '<tr>
									<td>'. $dati['title'] . '</td>
									<td><strong>'.$sito['sito'].'</strong></td>
									<td style="text-align:right">'.$prezzo_finito.' &euro;</td>
									<td>'.$prezzo_riportato.'</td>
									<td>'.$key . '</td><td>';
									echo "</td></tr>";
									$count_urls++;
								}
							
							}
						}
						if($count_urls == 0)
						{
							echo "<tr><td><strong>Nessun risultato trovato su ".$sito['sito']."</strong>.</td><td></td><td></td><td></td></tr>";
						}
					}
					else
					{
						echo "<tr><td><strong>Nessun risultato trovato su ".$sito['sito']."</strong>.</td><td></td><td></td><td></td></tr>";
					}
			}

			echo '</table>';	
			echo '<br /><strong>PER RAGIONI DI SICUREZZA</strong>, il link ai siti della concorrenza non sono cliccabili.';
			echo '<script type="text/javascript">
				$(document).ready(function(){
					alert("RICERCA COMPLETATA");
					$("#ricerca-in-corso").hide();
					
				});
			</script>';
			ob_flush();
			flush();
		}
	}
}

