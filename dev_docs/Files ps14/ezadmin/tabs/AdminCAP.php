<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14703 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminCAP extends AdminTab
{
	public function display()
	{	
		
		echo '<h1>Programma aggiornamento CAP</h1>';
		
		if(!Tools::getIsset('cercacap'))
		{
			echo'
		
			<form method="post">
			Cerca il comune a cui vuoi modificare il CAP. Puoi inserire il nome del comune, una parte del nome, oppure il cap.
			
			<br /><br />
			
			<input type="text" name="cap" style="width:200px" /><br /><br />
			
			<input type="submit" name="cercacap" style="width:200px" class="button" value="Cerca CAP" /><br /><br />
			
			</form>
			';
		}
		
		else
		{
			
			$caps = Db::getInstance()->executeS('SELECT * FROM cap WHERE comune LIKE "%'.Tools::getValue('cap').'%" OR cap LIKE "%'.Tools::getValue('cap').'%"');
			
			echo 'Ecco la lista dei comuni che puoi modificare. Clicca sull\'icona in ogni singola riga per salvare. <br /><br />';
			
			echo "<script type='text/javascript'>function salvaCAP(comune, cap, cod_istat)
				{
					$.ajax({
					  url:'ajax.php?salvaCAP=y',
					  type: 'POST',
					  data: { comune: comune, cap: cap, cod_istat: cod_istat
					  },
					  success:function(r){
						$('#span_'+cod_istat).html('<img src=\"..\/img\/admin\/ok.gif\" alt=\"OK\" title=\"OK\"  \/>').fadeIn(100);
					  },
					 
					});
					
					
				}
				</script>";
				
				echo '<script type="text/javascript">
		function add_cap() {
			var possible = "0123456789";
			var randomid = "";
			for( var i=0; i < 11; i++ ) {
				randomid += possible.charAt(Math.floor(Math.random() * possible.length));
			}
					
			$("<div id=\'tr_cap_"+randomid+"\'>Comune: <input type=\'hidden\' name=\'comune_nuovo[]\' value=\'1\' /></td><td><input type=\'text\' style=\'width:200px\' name=\'comune\' id=\'comune_"+randomid+"\' />&nbsp;&nbsp;&nbsp; CAP: <input type=\'text\' name=\'cap\'  id=\'cap_"+randomid+"\' style=\'width:200px\' /> &nbsp;&nbsp;&nbsp; Codice Istat (obbligatorio): <input type=\'text\' name=\'cod_istat\'  id=\'cod_istat_"+randomid+"\' style=\'width:200px\' /> <img src=\'../img/admin/save.gif\' alt=\'Salva\' title=\'Salva\' onclick=\'newCap(document.getElementById(\"comune_"+randomid+"\").value, document.getElementById(\"cap_"+randomid+"\").value, document.getElementById(\"cod_istat_"+randomid+"\").value)\' style=\'cursor:pointer\' /> <a href=\'#\' onclick=\'togli_riga("+randomid+");\'><img src=\'../img/admin/delete.gif\' alt=\'Cancella\' title=\'Cancella\' style=\'cursor:pointer\' /></a>&nbsp;&nbsp;&nbsp;<span id=\'span_"+randomid+"\'></span><br /><br /></div>").appendTo("#caps");

		}
			
		function togli_riga(id) {
			document.getElementById(\'tr_cap_\'+id).innerHTML = "";
		}</script>';
				
				echo "<div id='caps'>";
			foreach($caps as $cap)
			{
				echo 'Comune: <input type="text" style="width:200px" name="comune" value="'.$cap['comune'].'" id="comune_'.$cap['cod_istat'].'" />
				&nbsp;&nbsp;&nbsp;
				CAP: <input type="text" style="width:200px" name="cap" value="'.$cap['cap'].'" id="cap_'.$cap['cod_istat'].'" />
				&nbsp;&nbsp;&nbsp;
				<img src="../img/admin/save.gif" alt="Salva" title="Salva" style="cursor:pointer" onclick="salvaCAP(document.getElementById(\'comune_'.$cap['cod_istat'].'\').value, document.getElementById(\'cap_'.$cap['cod_istat'].'\').value, \''.$cap['cod_istat'].'\')">
				&nbsp;&nbsp;&nbsp;
				<span id="span_'.$cap['cod_istat'].'"></span><br /><br />';
			}
			echo '</div>';
			echo '<!-- <a href="#" onclick="javascript:add_cap()";><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" /> Clicca per aggiungere una localit&agrave;</a> <br /><br /> -->';
			
			echo '<a href="index.php?tab=AdminCAP&token='.$this->token.'">Clicca qui per una nuova ricerca</a>';
			
		}
		
	}	
}

