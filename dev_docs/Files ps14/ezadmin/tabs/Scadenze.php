<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

class Scadenze extends AdminTab
{

	public function display()
	{
		global $cookie;
		global $currentIndex;
		
		if($cookie->id_employee == 1 || $cookie->id_employee == 2 || $cookie->id_employee == 6 || $cookie->id_employee == 22)
		{	
			if(Tools::getIsset('cancella_persona')) {
		
				Db::getInstance()->getValue("DELETE FROM scadenze WHERE id_scadenza = ".Tools::getValue('id_persona')."");
			
			}
			
			if(Tools::getIsset('submitscadenze')) {
			
				foreach ($_POST['scadenza_nuovo'] as $id_scadenza=>$scadenza_id) {
					
					$data_scadenza = explode('-',$_POST['data_scadenza'][$id_scadenza]);
					
					$data_scadenza_int = $data_scadenza[2].'-'.$data_scadenza[1].'-'.$data_scadenza[0].' '.$_POST['ora_scadenza'][$id_scadenza].':'.$_POST['minuti_scadenza'][$id_scadenza].':00';
					
					if($_POST['scadenza_nuovo'][$id_scadenza] == 0) 
						Db::getInstance()->executeS("UPDATE scadenze SET scadenza = '".$_POST['scadenza'][$id_scadenza]."', data_scadenza = '".$data_scadenza_int."',  tipo = '".$_POST['tipo'][$id_scadenza]."',  decorrenza = '".date('Y-m-d',strtotime($_POST['decorrenza'][$id_scadenza]))."', referente = '".$_POST['referente'][$id_scadenza]."', importo = '".$_POST['importo'][$id_scadenza]."', esito = '".$_POST['esito'][$id_scadenza]."', note = '".$_POST['note'][$id_scadenza]."', date_upd = '".date('Y-m-d H:i:s')."' WHERE id_scadenza = ".$id_scadenza.""); 
					else {
						
						Db::getInstance()->executeS("INSERT INTO scadenze
						(id_scadenza,
						scadenza,
						data_scadenza,
						tipo,
						decorrenza,
						referente,
						importo,
						esito,
						note,
						date_add,
						date_upd)
						
						VALUES (
						NULL,
						'".($_POST['scadenza'][$id_scadenza])."',
						'".$data_scadenza_int."',
						'".($_POST['tipo'][$id_scadenza])."',
						'".date('Y-m-d',strtotime($_POST['decorrenza'][$id_scadenza]))."',
						'".($_POST['referente'][$id_scadenza])."',
						'".($_POST['importo'][$id_scadenza])."',
						'".($_POST['esito'][$id_scadenza])."',
						'".($_POST['note'][$id_scadenza])."',
						'".date('Y-m-d H:i:s')."',
						'".date('Y-m-d H:i:s')."'
						)");
						
					}
				}
			}
	
	
	
			echo '<h1>Scadenzario</h1>';
			
			$conta_scadenze = Db::getInstance()->getValue("SELECT count(*) FROM scadenze");
	
			if($conta_scadenze == 0) 
				echo "Non ci sono scadenze<br /><br />";
	
			
			includeDatepicker3(array('data_scadenza'), true);
				
			echo '<form action="'.$currentIndex.'&token='.$this->token.'" method="post">';
			echo '
			<script type="text/javascript">
				$(document).ready(function() {
				
					$(".textarea_note").each(function () {
						this.style.height = ((this.scrollHeight)-4)+"px";
					});
					
					$(".textarea_note").on("change keyup keydown paste cut", "textarea", function () {
						$(this).height(0).height(this.scrollHeight);
					}).find("textarea").change();
					
					autosize($(".textarea_note"));
										
				});
				
			</script>
			<script type="text/javascript">
			function add_scadenze() {
				var possible = "0123456789";
				var randomid = "";
				for( var i=0; i < 11; i++ ) {
					randomid += possible.charAt(Math.floor(Math.random() * possible.length));
				}
				
				$("<tr id=\'tr_scadenza_"+randomid+"\'><td><input type=\'hidden\' name=\'scadenza_nuovo[]\' value=\'1\' /><textarea class=\'textarea_note\' style=\'width:160px; height:16px\' name=\'scadenza[]\'></textarea></td><td><input type=\'text\' style=\'width:65px\' id=\'data_scadenza_"+randomid+"\' name=\'data_scadenza[]\' /></td><td><select name=\'tipo[]\'><option value=\'\'>-- Scegli --</option><option value=\'Pagamento\'>Pagamento</option><option value=\'Dichiarazione\'>Dichiarazione</option><option value=\'Altro\'>Altro</option></select></td><td><input type=\'text\' style=\'width:65px\' id=\'decorrenza_scadenza_"+randomid+"\' name=\'decorrenza[]\' value=\'\' /></td><td><input type=\'text\'  style=\'width:80px\' name=\'referente[]\' value=\'\' style=\'width:200px\' /><td><input type=\'text\' style=\'width:60px\' name=\'importo[]\' value=\'\' /></td><td><select name=\'esito[]\'><option value=\'\'>-- Scegli --</option><option value=\'Pagato\'>Pagato</option><option value=\'Non pagato\'>Non pagato</option><option value=\'Altro\'>Altro</option></select></td><td colspan=\'5\'><textarea class=\'textarea_note\' style=\'width:160px; height:16px\' name=\'note[]\'></textarea></td><td><a href=\#\' onclick=\'togli_riga("+randomid+");\'><img src=\'../img/admin/delete.gif\' alt=\'Cancella\' title=\'Cancella\' style=\'cursor:pointer\' /></a></td></tr>").appendTo("#tableScadenzeBody");
					
				
				$("#data_scadenza_"+randomid).datepicker({
						prevText:"",
						nextText:"",
						dateFormat:"dd-mm-yy"});
				
				$("#decorrenza_scadenza_"+randomid).datepicker({
						prevText:"",
						nextText:"",
						dateFormat:"dd-mm-yy"});
						
				$(".textarea_note").each(function () {
						this.style.height = ((this.scrollHeight)-4)+"px";
					});
					
					$(".textarea_note").on("change keyup keydown paste cut", "textarea", function () {
						$(this).height(0).height(this.scrollHeight);
					}).find("textarea").change();
					
					autosize($(".textarea_note"));		
			}
				
			function togli_riga(id) {
				document.getElementById(\'tr_scadenza_\'+id).innerHTML = "";
				document.getElementsByClassName(\'sub_\'+id).innerHTML = "";
				document.getElementsByClassName(\'sub2_\'+id).innerHTML = "";
			}
			
			function cancella_scadenza(id) {
				$.ajax({
					  url:"'.$currentIndex.'&cancellascadenze&token='.$this->token.'",
					  type: "POST",
					  data: { 
					  cancella_scadenza: \'y\',
					  id_scadenza: id
					  },
					  success:function(){
							 
					  },
					  error: function(xhr,stato,errori){
						 alert("Errore nella cancellazione:"+xhr.status);
					  }
				});
			
			}
			
			function aggiungi_calendario(id) {
				$.ajax({
					  url:"'.$currentIndex.'&aggiungicalendario&token='.$this->token.'",
					  type: "POST",
					  data: { 
					  id: id
					  },
					  success:function(){
						alert("OK");	 
					  },
					  error: function(xhr,stato,errori){
						
					  }
				});
				
			}	
				
			</script>
			
			';
			
			
			echo '<table class="table" style="width:900px" id="tableScadenze">';
			echo '<thead>';
			echo '<tr>';
			echo '
			<th>Descr. scadenza<br /> <a href="index.php?tab=Scadenze&orderby=scadenza&orderway=asc&token='.$this->token.'"><img src="../img/admin/up.gif" alt="Su" title="Su" /></a>   
			<a href="index.php?tab=Scadenze&orderby=scadenza&orderway=desc&token='.$this->token.'"><img src="../img/admin/down.gif" alt="Giu" title="Giu" /></a></th>
			<th>Data scadenza<br /><a href="index.php?tab=Scadenze&orderby=data_scadenza&orderway=asc&token='.$this->token.'"><img src="../img/admin/up.gif" alt="Su" title="Su" /></a>   
			<a href="index.php?tab=Scadenze&orderby=data_scadenza&orderway=desc&token='.$this->token.'"><img src="../img/admin/down.gif" alt="Giu" title="Giu" /></a></th>
			<th>Tipo<br /><a href="index.php?tab=Scadenze&orderby=tipo&orderway=asc&token='.$this->token.'"><img src="../img/admin/up.gif" alt="Su" title="Su" /></a>   
			<a href="index.php?tab=Scadenze&orderby=tipo&orderway=desc&token='.$this->token.'"><img src="../img/admin/down.gif" alt="Giu" title="Giu" /></a></th>
			<th>Decorrenza<br /><a href="index.php?tab=Scadenze&orderby=decorrenza&orderway=asc&token='.$this->token.'"><img src="../img/admin/up.gif" alt="Su" title="Su" /></a>   
			<a href="index.php?tab=Scadenze&orderby=decorrenza&orderway=desc&token='.$this->token.'"><img src="../img/admin/down.gif" alt="Giu" title="Giu" /></a></th>
			<th>Referente<br /><a href="index.php?tab=Scadenze&orderby=referente&orderway=asc&token='.$this->token.'"><img src="../img/admin/up.gif" alt="Su" title="Su" /></a>   
			<a href="index.php?tab=Scadenze&orderby=referente&orderway=desc&token='.$this->token.'"><img src="../img/admin/down.gif" alt="Giu" title="Giu" /></a></th>
			<th>Importo<br /><a href="index.php?tab=Scadenze&orderby=importo&orderway=asc&token='.$this->token.'"><img src="../img/admin/up.gif" alt="Su" title="Su" /></a>   
			<a href="index.php?tab=Scadenze&orderby=importo&orderway=desc&token='.$this->token.'"><img src="../img/admin/down.gif" alt="Giu" title="Giu" /></a></th>
			<th>Esito<br /><a href="index.php?tab=Scadenze&orderby=esito&orderway=asc&token='.$this->token.'"><img src="../img/admin/up.gif" alt="Su" title="Su" /></a>   
			<a href="index.php?tab=Scadenze&orderby=esito&orderway=desc&token='.$this->token.'"><img src="../img/admin/down.gif" alt="Giu" title="Giu" /></a></th>
			<th>Note</th><th></th>';
			echo '</tr>';
			echo '</thead>';
			echo '<tbody id="tableScadenzeBody">';
			
			
			$scadenze = Db::getInstance()->executeS("SELECT * FROM scadenze ORDER BY ".(Tools::getIsset('orderby') ? Tools::getValue('orderby') : 'data_scadenza')." ".(Tools::getIsset('orderway') ? Tools::getValue('orderway') : 'ASC')."");
				
			foreach($scadenze as $scadenza) {
				
				includeDatepicker3(array('data_scadenza_'.$scadenza['id_scadenza'], 'decorrenza_scadenza_'.$scadenza['id_scadenza']), true);
				
				$orario_scadenza = explode(" ",$scadenza['data_scadenza']);
				$orario_scadenza_2 = explode(":",$orario_scadenza[1]);
				$ora_scadenza = $orario_scadenza_2[0];
				$minuti_scadenza = $orario_scadenza_2[1];
				
				echo '<tr id="tr_scadenza_'.$scadenza['id_scadenza'].'">';
				echo '<td id="td_espandi_'.$scadenza['id_scadenza'].'">
				<script type="text/javascript" src="autosize.js"></script>
				<script type="text/javascript">
					$(document).ready(function(){
						$("#espandi_'.$scadenza['id_scadenza'].', #td_espandi_'.$scadenza['id_scadenza'].'").click(function(){
							$("tr.sub_'.$scadenza['id_scadenza'].'").toggle();
							$("tr.sub2_'.$scadenza['id_scadenza'].'").toggle();
							
							if($("tr.sub_'.$scadenza['id_scadenza'].':visible").length) {
								$(this).attr("src", "../img/admin/forbbiden.gif");	
							}
							else {
								$(this).attr("src", "../img/admin/add.gif");
								$("tr.sub_'.$scadenza['id_scadenza'].'").hide();
								$("tr.sub2_'.$scadenza['id_scadenza'].'").hide();
							}
							return false;
						});
						
					});
				</script>
				<input type="hidden" name="scadenza_nuovo['.$scadenza['id_scadenza'].']" value="0" /><textarea class="textarea_note" name="scadenza['.$scadenza['id_scadenza'].']"  style="width:160px; height:16px">'.$scadenza['scadenza'].'</textarea></td>';
				echo '<td><input type="text" style="width:65px" id="data_scadenza_'.$scadenza['id_scadenza'].'" name="data_scadenza['.$scadenza['id_scadenza'].']" value="'.($scadenza['data_scadenza'] != '0000-00-00 00:00:00' ? date("d-m-Y", strtotime($scadenza['data_scadenza'])) : date('d-m-Y')).'" />
				';
				echo '
				</td>';
				echo '<td><select name="tipo['.$scadenza['id_scadenza'].']"><option value="">-- Scegli --</option>
				<option value="Pagamento" '.($scadenza['tipo'] == 'Pagamento' ? 'selected="selected"' : '').'>Pagamento</option>
				<option value="Dichiarazione" '.($scadenza['tipo'] == 'Dichiarazione' ? 'selected="selected"' : '').'>Dichiarazione</option>
				<option value="Altro" '.($scadenza['tipo'] == 'Altro' ? 'selected="selected"' : '').'>Altro</option>
				</select></td><td><input type="text" style="width:65px" id="decorrenza_scadenza_'.$scadenza['id_scadenza'].'" name="decorrenza['.$scadenza['id_scadenza'].']" value="'.($scadenza['decorrenza'] != '0000-00-00 00:00:00' ? date("d-m-Y", strtotime($scadenza['decorrenza'])) : date('d-m-Y')).'" /></td>
				<td>
				<input type="text" name="referente['.$scadenza['id_scadenza'].']" style="width:80px" value="'.$scadenza['referente'].'" />
				<td>
				<input type="text" name="importo['.$scadenza['id_scadenza'].']" style="width:60px" value="'.$scadenza['importo'].'" />
				</td><td><select name="esito['.$scadenza['id_scadenza'].']"><option value="">-- Scegli --</option>
				<option value="Pagato" '.($scadenza['esito'] == 'Pagato' ? 'selected="selected"' : '').'>Pagato</option>
				<option value="Non pagato" '.($scadenza['esito'] == 'Non pagato' ? 'selected="selected"' : '').'>Non pagato</option>
				<option value="Altro" '.($scadenza['esito'] == 'Altro' ? 'selected="selected"' : '').'>Altro</option>
				</select>
				
				<td>
				<textarea class="textarea_note"   style="width:160px; height:16px" name="note['.$scadenza['id_scadenza'].']">'.$scadenza['note'].'</textarea>
				</td><td>
				<a href="#" onclick="javascript:var surec=window.confirm(\'Sei sicuro?\'); if (surec) { togli_riga('.$scadenza['id_scadenza'].'); cancella_scadenza('.$scadenza['id_scadenza'].'); } else { return false; }; "><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" style="cursor:pointer" /></a>
				</td></tr>
				';
					
				
			}

			echo '</tbody>';
			echo "</table>";
			echo '<br />
			
				<a href="#" class="button" style="display:block; height:18px; padding-top:5px; width:250px; float:left; margin-right:15px" onclick="javascript:add_scadenze()";><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" /> Clicca per aggiungere una scadenza</a>
		
			
			<button type="submit" class="button" name="submitscadenze">
				<img src="../img/admin/save.gif" alt="Salva" title="Salva" />&nbsp;&nbsp;&nbsp;'.$this->l('   Save   ').'
			</button>';
			echo "</form>";
			
			
			if(Tools::getIsset('cancellascadenze') && Tools::getValue('cancella_scadenza') == 'y') 
				Db::getInstance()->executeS("DELETE FROM scadenze WHERE id_scadenza = ".Tools::getValue('id_scadenza')."");
			
			if(Tools::getIsset('aggiungicalendario'))
				includeDatepicker3(array('data_scadenza_'.$_POST['id']), true);
		
			
		}
		else
			echo 'Non hai i permessi per vedere questa pagina';
	}

}


