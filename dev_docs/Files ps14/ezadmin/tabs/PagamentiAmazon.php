<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');
global $cookie;

class PagamentiAmazon extends AdminTab
{
		
	public function bindDatepickerZ($id, $time)
	{
		if ($time)
			echo '
			var dateObj = new Date();
			var hours = dateObj.getHours();
			var mins = dateObj.getMinutes();
			var secs = dateObj.getSeconds();
			if (hours < 10) { hours = "0" + hours; }
			if (mins < 10) { mins = "0" + mins; }
			if (secs < 10) { secs = "0" + secs; }
			var time = " "+hours+":"+mins+":"+secs;';

		echo '
		$(function() {
			$("#'.Tools::htmlentitiesUTF8($id).'").datepicker({
				prevText:"",
				nextText:"",
				dateFormat:"dd-mm-yy"});
		});';
	}

	// id can be a identifier or an array of identifiers
	public function includeDatepickerZ($id, $time = false)
	{
		global $cookie;
		echo '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery-ui-1.8.10.custom.min.js"></script>';
		$iso = Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'lang WHERE `id_lang` = '.(int)$cookie->id_lang);
		if ($iso != 'en')
			echo '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/datepicker/ui/i18n/ui.datepicker-'.Tools::htmlentitiesUTF8($iso).'.js"></script>';
		echo '<script type="text/javascript">';
			if (is_array($id))
				foreach ($id as $id2)
					$this->bindDatepickerZ($id2, $time);
			else
				$this->bindDatepickerZ($id, $time);
		echo '</script>';
	}
	
	public function display()
	{
		echo '<h1>Pagamenti Amazon</h1>';
		
		if(Tools::getIsset('tipo'))
		{
		
			if(Tools::getValue('tipo') == 'riepiloghi')
			{
				if(Tools::getIsset('invia_form'))
					include('../docs/disponibilita/MWSFinancesService/Samples/ListFinancialEventGroupsSample.php');
				
				if(Tools::getIsset('invia_form2'))
					include('../docs/disponibilita/MWSFinancesService/Samples/ListFinancialEventGroupsSample2.php');
			}
			else
				include('../docs/disponibilita/MWSFinancesService/Samples/ListFinancialEventsSample.php');
		}
		else
		{
			include_once('functions.php');
			
			$this->includeDatepickerZ(array('data_creazione_da', 'data_creazione_a'), true);
				
			echo '<form method="post">
			Seleziona pagamenti creati dal <input type="text" id="data_creazione_da" name="data_creazione_da"  /> al <input type="text" id="data_creazione_a" name="data_creazione_a"  />
			<span id="per_questi_mktp">Per questi marketplace:</span><select id="marketplace" name="marketplace"><option value="tutti">Tutti</option><option value="it">IT</option><option value="fr">FR</option><option value="es">ES</option><option value="de">DE</option><option value="uk">UK</option></select>
			<br /><br />
			<input type="radio" value="riepiloghi" name="tipo"  onclick="$(\'#per_questi_mktp\').hide(); $(\'#marketplace\').hide()" /> Riepiloghi complessivi periodi
			<input type="radio" value="singoli" name="tipo" onclick="$(\'#per_questi_mktp\').show(); $(\'#marketplace\').show()" /> Singoli ordini
			
			<br /><br />
			<em>NB: in modalit&agrave; singoli ordini, Amazon impone dei limiti al numero di ordini visualizzabili. Potrebbe essere necessario fare pi&ugrave; ricerche.</em>
			<br /><br />
			
			<input type="submit" name="invia_form" value="Cerca" class="button" />
			
			
			
			<br /><br />
			<input type="submit" name="invia_form2" value="Attenzione: CERCA QUI per pagamenti prima del 10 aprile 2016 (vecchio account)" class="button" />
			
			</form>';
		}	
		
	}
}

