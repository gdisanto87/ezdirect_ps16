<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

class ControlloPagamenti extends AdminTab
{

	public function display()
	{
		global $cookie;
		global $currentIndex;
		
		echo '<h1>Controllo pagamenti differiti</h1>';
		
		echo '<script type="text/javascript">
			function updControlloPagamento(tipo,id)
			{
				if(tipo == "altro")
				{
					var altro = document.getElementById("altro_"+id+"").value;
					$.ajax({
					  url:"ajax.php?updControlloPagamento=yes",
					  type: "POST",
					  data: { id_order: id, tipo: tipo, altro: altro
					  },
					  success:function(r){
						// alert(r);
					  },
					  error: function(xhr,stato,errori){
						 alert("Errore durante l\'operazione:"+xhr.status);
					  }
					});
				}
				else
				{
					var attivo="no";
					if(document.getElementById(tipo+"_"+id+"").checked == true)
						attivo="yes";
					else
						attivo="no";
					
					$.ajax({
					  url:"ajax.php?updControlloPagamento="+attivo,
					  type: "POST",
					  data: { id_order: id, tipo: tipo
					  },
					  success:function(r){
						// alert(r);
					  },
					  error: function(xhr,stato,errori){
						 alert("Errore durante l\'operazione:"+xhr.status);
					  }
					});
				}
				
			}</script>
		';	
		$clienti = Db::getInstance()->executeS('SELECT o.id_order as id_ordine, f.id_fattura, o.*, c.company, c.firstname, c.lastname, c.id_customer, c.is_company, oc.* FROM orders o JOIN customer c ON o.id_customer = c.id_customer LEFT JOIN (SELECT rif_ordine, id_fattura FROM fattura GROUP BY rif_ordine) f ON o.id_order = f.rif_ordine LEFT JOIN orders_controllo_differiti oc ON o.id_order = oc.id_order WHERE o.payment = "Bonifico 30 gg. fine mese" OR o.payment = "Bonifico 60 gg. fine mese" or o.payment = "Bonifico 90 gg. fine mese" OR o.payment = "Bonifico 30 gg. 15 mese successivo"  or o.payment = "R.B. 30 GG. D.F. F.M." or o.payment = "R.B. 60 GG. D.F. F.M." or o.payment = "R.B. 90 GG. D.F. F.M." or o.payment = "R.B. 30 GG. 5 mese successivo" or o.payment = "R.B. 30 GG. 10 mese successivo" or o.payment = "R.B. 60 GG. 5 mese successivo" or o.payment = "R.B. 60 GG. 10 mese successivo" GROUP BY c.id_customer ORDER BY o.date_add DESC');
		
		$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
		
			echo '<script type="text/javascript" src="../js/jquery/jquery.tablesorter.min.js"></script> 
			<script type="text/javascript" src="../js/jquery/jquery.tablesorter.widgets.js"></script> 
			<script type="text/javascript" src="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.js"></script> 
			<link rel="stylesheet" type="text/css" href="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.css" />
		
			<script type="text/javascript">
			var myTextExtraction = function(node) 
				{ 
					// extract data from markup and return it 
					return node.textContent.replace( /<.*?>/g, "" ); 
				}
				
			$(document).ready(function() 
				{ 
				
			
				$.tablesorter.addParser({ 
					// set a unique id 
					id: "thousands",
					is: function(s) { 
						// return false so this parser is not auto detected 
						return false; 
					}, 
					format: function(s) {
						// format your data for normalization 
						return s.replace(/[\s.$£€]/g,"").replace(/,/g,".");
					}, 
					// set type, either numeric or text 
					type: "numeric" 
				}); 


					$("#clienti_controllo_pagamento").tablesorter({ 
    dateFormat : "ddmmyyyy", textExtraction: { 0: function(node, table, cellIndex){ return $(node).find("strong").text(); } }, headers: {
					1: {sorter: "shortDate", dateFormat: "ddmmyy"}, 4: {sorter:"thousands" }, 6: {sorter: false}, 7: {sorter: false}, 8: {sorter: false}, 9: {sorter: false}
        }, cssChildRow: "invisible-table-row", widthFixed: true, widgets: ["zebra"]}).tablesorterPager(
						{
							
							container: $("#pager"),
							output: "Da {startRow} a {endRow} ({totalRows})",
							 updateArrows: true,
							 page:0,
							 size:30,
							 fixedHeight:true,
							 removeRows:false,
							 cssNext: ".next",
							// previous page arrow
							cssPrev: ".prev",
							// go to first page arrow
							cssFirst: ".first",
							// go to last page arrow
							cssLast: ".last",
							// select dropdown to allow choosing a page
							cssGoto: ".gotoPage",
							// location of where the "output" is displayed
							cssPageDisplay: ".pagedisplay",
							// dropdown that sets the "size" option
							cssPageSize: ".pagesize",
							// class added to arrows when at the extremes 
							// (i.e. prev/first arrows are "disabled" when on the first page)
							// Note there is no period "." in front of this class name
							cssDisabled: "disabled"
						})					;
					
				} 
			);
			</script>
			
			';
		echo "<table class='table paginated' id='clienti_controllo_pagamento' style='width:100%'><thead><tr><th>Id ordine</th><th>Data ord.</th><th>Cliente</th><th class='sorter-false'>Pagamento</th><th class='sorter-false'>Importo</th><th class='sorter-false'>Fatt.</th><th>Pagato</th><th data-sorter='false' class='sorter-false'>Insoluto</th><th class='sorter-false'>Pagamento in corso</th><th>Altro</th></tr></thead><tbody>";
		foreach($clienti as $row)
		{
			
			
			echo '
			<tr>
			<td>'.$row['id_ordine'].'</td>
			<td>'.date('d/m/Y',strtotime($row['date_add'])).'</td>
			<td><a href="index.php?tab=AdminCustomers&id_customer='.$row['id_customer'].'&viewcustomer&token='.$tokenCustomers.'">'.($row['is_company'] == 1 ? $row['company'] : $row['firstname'].' '.$row['lastname']).'</a></td>
			<td>'.$row['payment'].'</td>
			<td style="text-align:right">'.number_format($row['total_products'],2,",",".").'</td>
			<td style="text-align:right">'.$row['id_fattura'].'</td>
			<td style="text-align:right"><input type="checkbox" onclick="updControlloPagamento(\'pagato\','.$row['id_ordine'].')" name="pagato_'.$row['id_ordine'].'" id="pagato_'.$row['id_ordine'].'" '.($row['pagato'] == 1 ? 'checked="checked"' : '').' /></td>
			<td style="text-align:right"><input type="checkbox" onclick="updControlloPagamento(\'insoluto\','.$row['id_ordine'].')" name="insoluto_'.$row['id_ordine'].'" id="insoluto_'.$row['id_ordine'].'" '.($row['insoluto'] == 1 ? 'checked="checked"' : '').' /></td>
			<td style="text-align:right"><input type="checkbox" onclick="updControlloPagamento(\'pagamento_in_corso\','.$row['id_ordine'].')" name="pagamento_in_corso_'.$row['id_ordine'].'" id="pagamento_in_corso_'.$row['id_ordine'].'" '.($row['pagamento_in_corso'] == 1 ? 'checked="checked"' : '').' /></td>
			<td style="text-align:right"><input type="text" onkeyup="updControlloPagamento(\'altro\','.$row['id_ordine'].')" name="altro_'.$row['id_ordine'].'" id="altro_'.$row['id_ordine'].'" value="'.$row['altro'].'" /></td>
			</tr>
			';
			
		}
		echo '</tbody></table><div class="pager" id="pager">
						<span class="first" style="cursor:pointer"><img src="../img/admin/list-prev2.gif" title="Prima" alt="Prima" /></span>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="prev" style="cursor:pointer"><img src="../img/admin/list-prev.gif" title="Precedente" alt="Precedente" /></span>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="pagedisplay"></span>
						<span class="next" style="cursor:pointer"><img src="../img/admin/list-next.gif" title="Successiva" alt="Successiva" /></span>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="last" style="cursor:pointer"><img src="../img/admin/list-next2.gif" title="Ultima" alt="Ultima" /></span>
						<select class="pagesize">
						  <option value="10">10</option>
						  <option value="20">20</option>
						  <option value="30">30</option>
						  <option value="40">40</option>
						  <option value="all">Tutto</option>
						</select>
						 <select class="gotoPage" title="Select page number"></select>
					</div>';
	}

}


