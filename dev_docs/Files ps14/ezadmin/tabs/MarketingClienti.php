<?php

	include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

	class MarketingClienti extends AdminPreferences {
	
	public function bindDatepickerZ($id, $time)
	{
		if ($time)
			echo '
			var dateObj = new Date();
			var hours = dateObj.getHours();
			var mins = dateObj.getMinutes();
			var secs = dateObj.getSeconds();
			if (hours < 10) { hours = "0" + hours; }
			if (mins < 10) { mins = "0" + mins; }
			if (secs < 10) { secs = "0" + secs; }
			var time = " "+hours+":"+mins+":"+secs;';

		echo '
		$(function() {
			$("#'.Tools::htmlentitiesUTF8($id).'").datepicker({
				prevText:"",
				nextText:"",
				dateFormat:"dd-mm-yy"});
		});';
	}

	// id can be a identifier or an array of identifiers
	public function includeDatepickerZ($id, $time = false)
	{
		global $cookie;
		echo '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery-ui-1.8.10.custom.min.js"></script>';
		$iso = Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'lang WHERE `id_lang` = '.(int)$cookie->id_lang);
		if ($iso != 'en')
			echo '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/datepicker/ui/i18n/ui.datepicker-'.Tools::htmlentitiesUTF8($iso).'.js"></script>';
		echo '<script type="text/javascript">';
			if (is_array($id))
				foreach ($id as $id2)
					$this->bindDatepickerZ($id2, $time);
			else
				$this->bindDatepickerZ($id, $time);
		echo '</script>';
	}

	public	function datediff($tipo, $partenza, $fine)
    {
        switch ($tipo)
        {
            case "A" : $tipo = 365;
            break;
            case "M" : $tipo = (365 / 12);
            break;
            case "S" : $tipo = (365 / 52);
            break;
            case "G" : $tipo = 1;
            break;
        }
		$arrpulitopartenza = explode(" ", $partenza);
        $arr_partenza = explode("-", $arrpulitopartenza[0]);
        $partenza_gg = $arr_partenza[2];
        $partenza_mm = $arr_partenza[1];
        $partenza_aa = $arr_partenza[0];
		$arrpulitofine = explode(" ", $fine);
        $arr_fine = explode("-", $arrpulitofine[0]);
        $fine_gg = $arr_fine[2];
        $fine_mm = $arr_fine[1];
        $fine_aa = $arr_fine[0];
        $date_diff = mktime(12, 0, 0, $fine_mm, $fine_gg, $fine_aa) - mktime(12, 0, 0, $partenza_mm, $partenza_gg, $partenza_aa);
        $date_diff  = floor(($date_diff / 60 / 60 / 24) / $tipo);
        return $date_diff;
    }
	
		public function display()
		{
			

			if (get_magic_quotes_gpc()) {
				$process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
				while (list($key, $val) = each($process)) {
					foreach ($val as $k => $v) {
						unset($process[$key][$k]);
						if (is_array($v)) {
							$process[$key][stripslashes($k)] = $v;
							$process[] = &$process[$key][stripslashes($k)];
						} else {
							$process[$key][stripslashes($k)] = stripslashes($v);
						}
					}
				}
				unset($process);
			}


			global $cookie;
			echo "<h1>Strumento marketing per clienti</h1>";

		
			if(isset($_POST['cercaiclienti'])) { 
			
				$prodotti = "";
				$categorie_di_default = "";
				$macrocategorie = "";
				$macrocategorie_di_default = "";
				$categorie = "";
				$marchi = "";

				echo "<strong>Riepilogo - clienti che hanno acquistato...</strong><br /><br />";
				
				if(isset($_POST['per_prodotto'])) {
					echo "<strong>Questi prodotti precisi:</strong><br /> ";
					
					
					foreach ($_POST['per_prodotto'] as $prodotto) {

						$ref_prod = Db::getInstance()->getValue("SELECT reference FROM product WHERE id_product = ".$prodotto."");
						$nomeprodotto = Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_lang = 5 AND id_product = ".$prodotto."");
						
						$prodottiinclusi.= $nomeprodotto."; ";
						
						$strisciaprodotti.= "- ".$nomeprodotto."<br />";
	
						$prodotti.= "p.id_product = '$prodotto' OR ";
					
					}
				}
				
				else if(isset($_POST['prodottiinclusi']) && !empty($_POST['prodottiinclusi'])) {
				
					echo "<strong>Questi prodotti precisi:</strong><br /> ";
					$strisciaprodotti = $_POST['strisciaprodotti'];
					$prodottiinclusi = $_POST['prodottiinclusi'];
				
				
				}
				
				echo $strisciaprodotti;
				
				
				echo "<br />";
				
		
				
				if(isset($_POST['per_categoria'])) {
				
					echo "<strong>Prodotti in queste sottocategorie:</strong><br /> ";
			
			
					foreach ($_POST['per_categoria'] as $categoria) {
	
						$nomecat = Db::getInstance()->getValue("SELECT name FROM category_lang WHERE id_lang = 5 AND id_category = ".$categoria."");
						$strisciacat .= "- ".$nomecat."<br />";
						
					
						$categorieincluse.= $nomecat."; ";
						
						$categorie_di_default.= "p.id_category_default = $categoria OR ";
						$categorie.= "cp.id_category = $categoria OR ";
					}	
					
				}
				
				
				else if(isset($_POST['categorieincluse']) && !empty($_POST['categorieincluse'])) {
				
					echo "<strong>Prodotti in queste sottocategorie:</strong><br /> ";
					$strisciacat = $_POST['strisciacat'];
					$categorieincluse = $_POST['categorieincluse'];
				
				
				}
				
				echo $strisciacat;
				
				if(isset($_POST['per_macrocategoria'])) {
				
					echo "<strong>Prodotti in queste macrocategorie:</strong><br /> ";
			
			
					foreach ($_POST['per_macrocategoria'] as $macrocategoria) {
	
						$nomemacrocat = Db::getInstance()->getValue("SELECT name FROM category_lang WHERE id_lang = 5 AND id_category = ".$macrocategoria."");
						$strisciamacrocat .= "- ".$nomemacrocat."<br />";
						
						$children = Db::getInstance()->executeS("SELECT id_category FROM
						(SELECT t4.id_category
						FROM category AS t1
						LEFT JOIN category AS t2 ON t1.id_category = t2.id_parent
						LEFT JOIN category AS t3 ON t2.id_category = t3.id_parent
						LEFT JOIN category AS t4 ON t3.id_category = t3.id_parent
						WHERE t2.id_parent =$macrocategoria

						UNION 

						SELECT t3.id_category
						FROM category AS t1
						LEFT JOIN category AS t2 ON t1.id_category = t2.id_parent
						LEFT JOIN category AS t3 ON t2.id_category = t3.id_parent
						WHERE t2.id_parent =$macrocategoria

						UNION 

						SELECT t2.id_category
						FROM category AS t1
						LEFT JOIN category AS t2 ON t1.id_category = t2.id_parent
						WHERE t2.id_parent =$macrocategoria

						UNION 

						SELECT id_category FROM category WHERE id_category = $macrocategoria) ccc WHERE id_category IS NOT NULL ORDER BY id_category ASC

						");

						foreach ($children as $child) {
	
							$macrocategorie_di_default.= "p.id_category_default = ".$child['id_category']." OR ";
							$macrocategorie.= "cp.id_category = ".$child['id_category']." OR ";
						}
						
					
						$macrocategorieincluse.= $nomemacrocat."; ";
						
						
				
					}	
					
				}
				
				
				else if(isset($_POST['macrocategorieincluse']) && !empty($_POST['macrocategorieincluse'])) {
				
					echo "<strong>Prodotti in queste macrocategorie:</strong><br /> ";
					$strisciamacrocat = $_POST['strisciamacrocat'];
					$macrocategorieincluse = $_POST['macrocategorieincluse'];
				
				
				}
				
				echo $strisciamacrocat;
				
				if(isset($_POST['per_marchio'])) {
				
					echo "<strong>Prodotti in questi marchi:</strong><br /> ";
			
			
					foreach ($_POST['per_marchio'] as $marchio) {
	
						$nomemarchio = Db::getInstance()->getValue("SELECT name FROM manufacturer WHERE id_manufacturer = ".$marchio."");
						$strisciamarchi .= "- ".$nomemarchio."<br />";
						
					
						$marchiinclusi.= $nomemarchio."; ";
						$marchi.= "p.id_manufacturer = $marchio OR ";
						$marchi2.= "od.manufacturer_id = $marchio OR ";
					}	
					
				}
				
				else if(isset($_POST['marchiinclusi']) && !empty($_POST['marchiinclusi'])) {
				
					echo "<strong>Prodotti in questi marchi:</strong><br /> ";
					$strisciamarchi = $_POST['strisciamarchi'];
					$marchiinclusi = $_POST['marchiinclusi'];
				
				
				}
				
				echo $strisciamarchi;
				
						if(isset($_POST['escludi-privati'])) {
				
					$escludi_privati = "AND c.is_company = 1";
					$striscia_escludi_privati = "<strong>Privati esclusi</strong>";
				}
				else {
					$striscia_escludi_privati = "<strong>Tutti i clienti</strong>";
				}
				
				if(isset($_POST['striscia_escludi_privati'])) {
				
					$striscia_escludi_privati = $_POST['striscia_escludi_privati'];
				}
			
				echo $striscia_escludi_privati."<br />";
				
				if(!empty($_POST['arco_dal'])) {
					
					$data_dal_a = explode("-", $_POST['arco_dal']);
					$data_dal = $data_dal_a[2]."-".$data_dal_a[1]."-".$data_dal_a[0]; 
					if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'ordinato') {
						$arco_dal = 'AND o.date_add > "'.$data_dal.' 00:00:00"';
					}
					else if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'preventivi') {
						$arco_dal = 'AND ca.date_add > "'.$data_dal.' 00:00:00"';
					}
					else  {
						$arco_dal = 'AND ca.date_add > "'.$data_dal.' 00:00:00" OR o.date_add > "'.$data_dal.' 00:00:00"';
					}
					$striscia_arco_dal = "Da: <strong>$_POST[arco_dal]</strong> ";
					
				}
				else {
					$striscia_arco_dal = "Da: <strong>inizio della storia del sito</strong> ";
				
				}
				
				if(isset($_POST['striscia_arco_dal'])) {
				
					$striscia_arco_dal = $_POST['striscia_arco_dal'];
				}
			
				echo $striscia_arco_dal;
				
				if(!empty($_POST['arco_al'])) {
				
					$data_al_a = explode("-", $_POST['arco_al']);
					$data_al = $data_al_a[2]."-".$data_al_a[1]."-".$data_al_a[0]; 
					
					if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'ordinato') {
						$arco_al = 'AND o.date_add < "'.$data_al.' 00:00:00"';
					}
					else if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'preventivi') {
						$arco_al = 'AND ca.date_add < "'.$data_al.' 00:00:00"';
					}
					else  {
						$arco_dal = 'AND ca.date_add < "'.$data_dal.' 00:00:00" OR o.date_add < "'.$data_dal.' 00:00:00"';
					}
					$striscia_arco_al = "a: <strong>$_POST[arco_al]</strong> ";
				}
				else {
					$striscia_arco_al = "a: <strong>oggi</strong> ";
				
				}
				
				if(isset($_POST['striscia_arco_al'])) {
				
					$striscia_arco_al = $_POST['striscia_arco_al'];
				}
			
				echo $striscia_arco_al."<br />";
				
				$anno_corrente = "2014";
				switch($_POST['cercaiclienti']) {
				
					case 'idcustomerasc': $orderby = "ORDER BY cz.id_customer ASC"; break;
					case 'idcustomerdesc': $orderby = "ORDER BY cz.id_customer DESC"; break;
					case 'lastnameasc': $orderby = "ORDER BY cz.lastname ASC"; break;
					case 'lastnamedesc': $orderby = "ORDER BY cz.lastname DESC"; break;
					case 'companyasc': $orderby = "ORDER BY cz.company ASC"; break;
					case 'companydesc': $orderby = "ORDER BY cz.company DESC"; break;
					case 'clienteasc': $orderby = "ORDER BY cz.cliente ASC"; break;
					case 'clientedesc': $orderby = "ORDER BY cz.cliente DESC"; break;
					case 'iscompanyasc': $orderby = "ORDER BY cz.is_company ASC"; break;
					case 'iscompanydesc': $orderby = "ORDER BY cz.is_company DESC"; break;
					case 'groupasc': $orderby = "ORDER BY cz.id_default_group ASC"; break;
					case 'groupdesc': $orderby = "ORDER BY cz.id_default_group DESC"; break;
					case 'stateasc': $orderby = "ORDER BY cz.iso_code ASC"; break;
					case 'statedesc': $orderby = "ORDER BY cz.iso_code DESC"; break;
					case 'totaleacqasc': $orderby = "ORDER BY cz.totale_acquisti ASC"; break;
					case 'totaleacqdesc': $orderby = "ORDER BY cz.totale_acquisti DESC"; break;
					case 'totaleacqannoasc': $orderby = "ORDER BY cz.ultimo_anno ASC"; break;
					case 'totaleacqannodesc': $orderby = "ORDER BY cz.ultimo_anno DESC"; break;
					
					default: $orderby = "ORDER BY cz.id_default_group, cz.id_customer ASC"; break;
					
				
				}
				
				/*$where = "
					
					AND ((
					$prodotti
					ft.cod_articolo =99999999999999999
					) 
					OR 
					(
					$categorie_di_default
					p.id_category_default = 9999999999999999
					)
					OR 
					(
					$categorie
					cp.id_category = 9999999999999999999999
					)
					OR 
					(
					$marchi
					p.id_manufacturer = 9999999999999999999999
					)
					OR 
					(
					$marchi2
					od.manufacturer_id = 9999999999999999999999
					)
					OR
					(
					$macrocategorie
					cp.id_category = 9999999999999999999999
					)
					OR 
					(
					$macrocategorie_di_default
					p.id_category_default = 9999999999999999
					)
					)
					";*/
					
					
					$where = "
					
						
						";
						
						if(isset($_POST['per_prodotto'])) {
							$where.= "AND 
							(
							$prodotti
							p.id_product = 9999999999999999999999
							)";
						}
						if(isset($_POST['per_categoria'])) {
						
							$where .= "AND 
							(
							$categorie
							cp.id_category = 9999999999999999999999
							)";
						
						}
						
						if(isset($_POST['per_marchio'])) {
						
							$where .= "
							AND 
							(
							$marchi
							p.id_manufacturer = 9999999999999999999999
							)";
						}
						
						if(isset($_POST['per_macrocategoria'])) {
						
							$where .= "
							AND
							(
							$macrocategorie
							cp.id_category = 9999999999999999999999
							)";
						
						}
						
						/*
						OR 
						(
						$macrocategorie_di_default
						p.id_category_default = 9999999999999999
						)
						*/
						
					/*$where.= ")
					)
					";*/
				
				if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'ordinato' || isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'entrambi' ) {
					$from = 'FROM customer c
						JOIN orders o ON c.id_customer = o.id_customer
						JOIN order_detail od ON o.id_order = od.id_order
						JOIN address a ON c.id_customer = a.id_customer
						JOIN state s ON a.id_state = s.id_state
						JOIN product p ON od.product_reference = p.reference
						JOIN category_product cp ON p.id_product = cp.id_product
						
						
						';
				}
				else if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'preventivi') {
					$from = 'FROM customer c
						JOIN cart ca ON ca.id_customer = c.id_customer
						LEFT JOIN orders o ON c.id_customer = o.id_customer
						LEFT JOIN order_detail od ON o.id_order = od.id_order
						JOIN cart_product cap ON ca.id_cart = cap.id_cart
						JOIN address a ON c.id_customer = a.id_customer
						JOIN state s ON a.id_state = s.id_state
						JOIN product p ON cap.id_product = p.id_product
						JOIN category_product cp ON p.id_product = cp.id_product
					';
				
				}
				
				else {
						
				
				}
				
				
				if(isset($_POST['querysql'])) {

					$sql = $_POST['querysql'];
					
				}
			
				else {
				
					if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'ordinato') {
						
					
						$sql = "SELECT * FROM (SELECT c.id_customer, c.firstname, c.lastname, s.iso_code, c.email, c.id_default_group, c.is_company, c.company, a.phone, a.phone_mobile, a.fax, f.totale_acquisti, fa.ultimo_anno, (CASE WHEN c.is_company = 0 THEN CONCAT(c.lastname,\" \",c.firstname) WHEN c.is_company = 1 THEN (CASE WHEN c.company IS NULL OR c.company = \"\" THEN CONCAT(c.lastname,\" \",c.firstname) ELSE c.company END) END) cliente, od.product_price, od.reduction_percent, od.product_quantity,  o.date_add
						$from
						
						LEFT JOIN (SELECT SUM(fprv.totale_fattura) AS totale_acquisti, fprv.id_customer FROM (SELECT totale_fattura, id_customer FROM fattura GROUP BY id_fattura) fprv GROUP BY fprv.id_customer) f ON f.id_customer = c.id_customer
						LEFT JOIN (SELECT SUM(totale_fattura) AS ultimo_anno, id_customer FROM (SELECT totale_fattura, id_customer FROM fattura WHERE data_fattura > '".$anno_corrente."-01-01 00:00:00' AND data_fattura < '".$anno_corrente."-12-31 00:00:00' GROUP BY id_fattura) faprv GROUP BY faprv.id_customer) fa ON fa.id_customer = c.id_customer
						
						WHERE a.fatturazione =1
						AND (o.valid = 1 OR (o.valid = 0 AND o.module = \"bankwire\")
						AND o.id_order NOT IN (SELECT id_order FROM order_history WHERE id_order_state = 6  OR id_order_state = 35))
						AND a.deleted =0
						AND a.active =1
						$escludi_privati
						$arco_dal
						$arco_al
						$where 
						
						
						GROUP BY od.id_order_detail
						
						) cz GROUP BY cz.id_customer
						";
					}
					
					else if(isset($_POST['su_tipo']) && $_POST['su_tipo'] == 'entrambi') {
						$sql = "SELECT * FROM (SELECT * FROM (SELECT c.id_customer, c.firstname, c.lastname, s.iso_code, c.email, c.id_default_group, c.is_company, c.company, a.phone, a.phone_mobile, a.fax, f.totale_acquisti, fa.ultimo_anno, (CASE WHEN c.is_company = 0 THEN CONCAT(c.lastname,\" \",c.firstname) WHEN c.is_company = 1 THEN (CASE WHEN c.company IS NULL OR c.company = \"\" THEN CONCAT(c.lastname,\" \",c.firstname) ELSE c.company END) END) cliente, od.product_price, od.reduction_percent, od.product_quantity,  o.date_add
						FROM customer c
						JOIN orders o ON c.id_customer = o.id_customer
						JOIN order_detail od ON o.id_order = od.id_order
						JOIN address a ON c.id_customer = a.id_customer
						JOIN state s ON a.id_state = s.id_state
						JOIN product p ON od.product_reference = p.reference
						JOIN category_product cp ON p.id_product = cp.id_product
						
						LEFT JOIN (SELECT SUM(fprv.totale_fattura) AS totale_acquisti, fprv.id_customer FROM (SELECT totale_fattura, id_customer FROM fattura GROUP BY id_fattura) fprv GROUP BY fprv.id_customer) f ON f.id_customer = c.id_customer
						LEFT JOIN (SELECT SUM(totale_fattura) AS ultimo_anno, id_customer FROM (SELECT totale_fattura, id_customer FROM fattura WHERE data_fattura > '".$anno_corrente."-01-01 00:00:00' AND data_fattura < '".$anno_corrente."-12-31 00:00:00' GROUP BY id_fattura) faprv GROUP BY faprv.id_customer) fa ON fa.id_customer = c.id_customer
						
						WHERE a.fatturazione =1
						AND (o.valid = 1 OR (o.valid = 0 AND o.module = \"bankwire\")
						AND o.id_order NOT IN (SELECT id_order FROM order_history WHERE id_order_state = 6  OR id_order_state = 35))
						AND a.deleted =0
						AND a.active =1
						$escludi_privati
						$arco_dal
						$arco_al
						$where 
						
						
						GROUP BY od.id_order_detail
						
						) cx GROUP BY cx.id_customer
						
						UNION
						
						SELECT * FROM (SELECT c.id_customer, c.firstname, c.lastname, s.iso_code, c.email, c.id_default_group, c.is_company, c.company, a.phone, a.phone_mobile, a.fax, f.totale_acquisti, fa.ultimo_anno, (CASE WHEN c.is_company = 0 THEN CONCAT(c.lastname,\" \",c.firstname) WHEN c.is_company = 1 THEN (CASE WHEN c.company IS NULL OR c.company = \"\" THEN CONCAT(c.lastname,\" \",c.firstname) ELSE c.company END) END) cliente, od.product_price, od.reduction_percent, od.product_quantity, o.date_add
						FROM customer c
						JOIN cart ca ON ca.id_customer = c.id_customer
						LEFT JOIN orders o ON c.id_customer = o.id_customer
						LEFT JOIN order_detail od ON o.id_order = od.id_order
						JOIN cart_product cap ON ca.id_cart = cap.id_cart
						JOIN address a ON c.id_customer = a.id_customer
						JOIN state s ON a.id_state = s.id_state
						JOIN product p ON cap.id_product = p.id_product
						JOIN category_product cp ON p.id_product = cp.id_product
						
						LEFT JOIN (SELECT SUM(fprv.totale_fattura) AS totale_acquisti, fprv.id_customer FROM (SELECT totale_fattura, id_customer FROM fattura GROUP BY id_fattura) fprv GROUP BY fprv.id_customer) f ON f.id_customer = c.id_customer
						LEFT JOIN (SELECT SUM(totale_fattura) AS ultimo_anno, id_customer FROM (SELECT totale_fattura, id_customer FROM fattura WHERE data_fattura > '".$anno_corrente."-01-01 00:00:00' AND data_fattura < '".$anno_corrente."-12-31 00:00:00' GROUP BY id_fattura) faprv GROUP BY faprv.id_customer) fa ON fa.id_customer = c.id_customer
						
						WHERE a.fatturazione =1
						AND ca.preventivo = 1
						AND a.deleted =0
						AND a.active =1
						$escludi_privati
						$arco_dal
						$arco_al
						$where 
						
						
						GROUP BY c.id_customer
						
						) cy GROUP BY cy.id_customer) cz GROUP BY cz.id_customer
						
						";
					}
					
					else {
						$sql = "SELECT * FROM (SELECT c.id_customer, c.firstname, c.lastname, s.iso_code, c.email, c.id_default_group, c.is_company, c.company, a.phone, a.phone_mobile, a.fax, f.totale_acquisti, fa.ultimo_anno, (CASE WHEN c.is_company = 0 THEN CONCAT(c.lastname,\" \",c.firstname) WHEN c.is_company = 1 THEN (CASE WHEN c.company IS NULL OR c.company = \"\" THEN CONCAT(c.lastname,\" \",c.firstname) ELSE c.company END) END) cliente, o.id_order, od.product_price, od.reduction_percent, od.product_quantity, cap.price, cap.quantity,  ca.date_add
						$from
						
						LEFT JOIN (SELECT SUM(fprv.totale_fattura) AS totale_acquisti, fprv.id_customer FROM (SELECT totale_fattura, id_customer FROM fattura GROUP BY id_fattura) fprv GROUP BY fprv.id_customer) f ON f.id_customer = c.id_customer
						LEFT JOIN (SELECT SUM(totale_fattura) AS ultimo_anno, id_customer FROM (SELECT totale_fattura, id_customer FROM fattura WHERE data_fattura > '".$anno_corrente."-01-01 00:00:00' AND data_fattura < '".$anno_corrente."-12-31 00:00:00' GROUP BY id_fattura) faprv GROUP BY faprv.id_customer) fa ON fa.id_customer = c.id_customer
						WHERE a.fatturazione =1
						AND ca.preventivo = 1
						AND a.deleted =0
						AND a.active =1
						$escludi_privati
						$arco_dal
						$arco_al
						$where 
						
						
						GROUP BY c.id_customer
						
						) cz GROUP BY cz.id_customer
						";
					}
					
				}
		
				$numeroclienti = Db::getInstance()->NumRows($sql);
		
				echo "<br />";
				
				
				echo "<form method='post' action=''>";
				echo "<strong>Esportazione Dati</strong> - opzioni<br />";
				echo "<strong>IMPORTANTE: non spuntare NESSUNA CASELLA se vuoi esportare la lista completa che trovi qua sotto.</strong><br /><br />";
				echo "<input type='checkbox' name='esportaaziende' /> Esporta aziende<br />";
				echo "<input type='checkbox' name='esportaprivati' /> Esporta privati<br />---<br />";
				echo "<input type='checkbox' name='esportaclienti' /> Esporta clienti<br />";
				echo "<input type='checkbox' name='esportarivenditori' /> Esporta rivenditori<br /><br />";
				
				echo "<input type='submit' value='Esporta i dati in formato Excel' name='esportaexcel' class='button' />
				<input type='submit' value='Vedi solo le mail in formato TXT per invio' name='esportamailtxt' class='button' />
				<input type='hidden' name='querysql' value='".htmlentities((isset($_POST['querysql']) ? $_POST['querysql'] : $sql),ENT_QUOTES)."' />	
				<input type='hidden' name='query_from' value='".htmlentities((isset($_POST['query_from']) ? $_POST['query_from'] : $from),ENT_QUOTES)."' />	
				<input type='hidden' name='query_where' value='".htmlentities((isset($_POST['query_where']) ? $_POST['query_where'] : $where),ENT_QUOTES)."' />						
				<input type='hidden' name='prodottiinclusi' value='".$prodottiinclusi."' />		
				<input type='hidden' name='strisciaprodotti' value='".$strisciaprodotti."' />		
				<input type='hidden' name='categorieincluse' value='".$categorieincluse."' />	
				<input type='hidden' name='macrocategorieincluse' value='".$macrocategorieincluse."' />						
				<input type='hidden' name='strisciacat' value='".$strisciacat."' />	
				<input type='hidden' name='marchiinclusi' value='".$marchiinclusi."' />		
				<input type='hidden' name='strisciamarchi' value='".$strisciamarchi."' />	
				<input type='hidden' name='striscia_escludi_privati' value='".$striscia_escludi_privati."' />				
				<input type='hidden' name='striscia_arco_dal' value='".$striscia_arco_dal."' />	
				<input type='hidden' name='striscia_arco_al' value='".$striscia_arco_al."' />	
				";
				echo "<br /><br />";
				
				$sql.= $orderby;
			
				$resultsclienti = Db::getInstance()->ExecuteS(stripslashes($sql));
				
				$num = 0;
				$tot_acquisti = 0;
				
				
				echo "<input type='hidden' name='num' value='".$num."' />		
				<input type='hidden' name='tot_acquisti' value='".$tot_acquisti."' />";	
				echo "<table class='table'><tr><td>Totale clienti trovati</td><td><strong>$num</strong></td></tr>";
				echo "<tr><td>Totale acquisti</td><td><strong>".Tools::displayPrice($tot_acquisti,1)."</strong></td></tr></table><br />";
				echo "<table class='table'><thead class='persist-header'>";
				echo "<tr>";
				echo "<th style='width:30px'>N.</th>";
				echo "<th style='width:45px'>Id cliente 
				<br />
				<input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='idcustomerasc' /></a>   
				<input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='idcustomerdesc' /></a>   
				</th>";
				echo "<th style='width:150px'>Cliente
				<br />
				<input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='clienteasc' /></a>   
				<input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='clientedesc' /></a> 
				</th>";
				echo "<th style='width:50px'>Tipo
				<br />
				<input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='iscompanyasc' /></a>   
				<input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='iscompanydesc' /></a> 
				</th>";
				echo "<th style='width:50px'>Gruppo
				<br />
				<input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='groupasc' /></a>   
				<input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='groupdesc' /></a> 
				</th>";
			
				echo "<th style='width:30px'>Provincia
				<br />
				<input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='stateasc' /></a>   
				<input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='statedesc' /></a> 
				</th>";
				
				
				echo "<th style='width:80px'>Totale acq. in euro
				<br />
				<input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='totaleacqasc' /></a>   
				<input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='totaleacqdesc' /></a> 
				</th>
				";
				
				echo "<th style='width:80px'>Totale acq. ".$anno_corrente."
				<br />
				<input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='totaleacqannoasc' /></a>   
				<input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='totaleacqannodesc' /></a> 
				</th>
				";
				
				echo "<th style='width:80px'>Data primo acquisto 
				
				</th>";
				
				echo "<th style='width:80px'>Data ultimo acquisto</th>
				
				";
				
				echo "<th>Mesi inattivit&agrave;</th>";
				
				echo "<th>Vai</th>";
				echo "</tr></thead><tbody>";
				
				$i = 1;
				
				foreach ($resultsclienti as $row) {
					
					
					if($row['is_company'] == 1) {
						$tipo = 'A';
					}
					else {
						$tipo = 'P';
					}
			
					
					$data_primo_acquisto = Db::getInstance()->getValue("SELECT o.date_add ".html_entity_decode((isset($_POST['query_from']) ? $_POST['query_from'] : $from))." WHERE o.id_customer = ".$row['id_customer']." ".html_entity_decode((isset($_POST['query_where']) ? $_POST['query_where'] : $where))." ORDER BY o.date_add ASC");
					
					$data_ultimo_acquisto = Db::getInstance()->getValue("SELECT o.date_add ".html_entity_decode((isset($_POST['query_from']) ? $_POST['query_from'] : $from))." WHERE o.id_customer = ".$row['id_customer']." ".html_entity_decode((isset($_POST['query_where']) ? $_POST['query_where'] : $where))." ORDER BY o.date_add DESC");
					
					$mesi_inattivita = $this->datediff("M", $data_ultimo_acquisto, date("Y-m-d H:i:s"));
					
					$gruppo = Db::getInstance()->getValue("SELECT name FROM group_lang WHERE id_lang = 5 AND id_group = ".$row['id_default_group']."");
					$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee));
					
					
					
					
					echo "<tr>";
					echo "<td style='width:30px'>".$i."</td>";
					echo "<td>".$row['id_customer']."</td>";
					echo "<td>".$row['cliente']."</td>";
					echo "<td>".$tipo."</td>";
					echo "<td>".$gruppo."</td>";
					echo "<td style='width:30px'>".$row['iso_code']."</td>";
					
					echo "<td style='text-align:right'>".Tools::displayPrice($row['totale_acquisti'],1)."</td>";
					
					echo "<td style='text-align:right'>".Tools::displayPrice($row['ultimo_anno'],1)."</td>";
					
					echo "<td>".Tools::displayDate($data_primo_acquisto,5)."</td>";
					echo "<td>".Tools::displayDate($data_ultimo_acquisto,5)."</td>";
					
					echo "<td>".$mesi_inattivita."</td>";
					
					echo "<td><a href='index.php?tab=AdminCustomers&id_customer=".$row['id_customer']."&viewcustomer&token=".$tokenCustomers."&tab-container-1=1' target='_blank'><img src='../img/admin/details.gif' alt='Apri cliente' title='Apri cliente' /></a></td>";
					
					
					
					echo "</tr>";
					
					$i++;
				
				}
				
				echo "</tbody></table>";
				echo "</form>";
			}
			
			else if(isset($_POST['esportamailtxt'])) { 
			
				$resultsclienti = Db::getInstance()->ExecuteS(html_entity_decode($_POST['querysql']));
				
				foreach ($resultsclienti as $row) {
					
				
					$mails.= $row['email']."; ";
				}
		
				echo $mails;
				
				/*$filename = "mail-clienti.txt";
				
				$newf = fopen($filename, 'w');
				fwrite($newf, $mails);
				fclose($newf);
				header("Content-Description: File Transfer");
				header("Content-Length: ". filesize("$filename").";");
				header("Content-Disposition: attachment; filename=$filename");
				header("Content-Type: application/octet-stream; "); 
				header("Content-Transfer-Encoding: binary");
				readfile($filename);*/

			}
			
			else if(isset($_POST['esportaexcel'])) { 
				ini_set("memory_limit","892M");
				set_time_limit(3600);
				require_once 'esportazione-catalogo/Classes/PHPExcel.php';
				$objPHPExcel = new PHPExcel();
				
				
				$resultsclienti = Db::getInstance()->ExecuteS(stripslashes($_POST['querysql']));
				
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'CLIENTI CHE HANNO ACQUISTATO')
					->setCellValue('A2', 'Prodotti: '.$_POST['prodottiinclusi'].'')
					->setCellValue('A3', 'Categorie: '.$_POST['macrocategorieincluse'].' '.$_POST['categorieincluse'].'')
					->setCellValue('A4', 'Marchi: '.$_POST['marchiinclusi'].'')
					->setCellValue('A5', 'Data esportazione: '.date('d/m/Y H:i:s').'')
					->setCellValue('H1', 'Numero clienti trovati: '.$_POST['num'].'')
					->setCellValue('H2', 'Totale acquisti: '.str_replace(".",",",$_POST['tot_acquisti']).' euro')
					->setCellValue('A6', 'N.')
					->setCellValue('B6', 'Id cliente')
					->setCellValue('C6', 'Nome')
					->setCellValue('D6', 'Cognome')
					->setCellValue('E6', 'Azienda')
					->setCellValue('F6', 'Tipo')
					->setCellValue('G6', 'Gruppo')
					->setCellValue('H6', 'Email')
					->setCellValue('I6', 'Telefono')
					->setCellValue('J6', 'Cellulare')
					->setCellValue('K6', 'Fax')
					->setCellValue('L6', 'Provincia')
					->setCellValue('M6', 'Tot. acq. in euro')
					->setCellValue('N6', 'Tot. acq. '.date("Y").'')
					->setCellValue('O6', 'Data primo acquisto')
					->setCellValue('P6', 'Data ultimo acquisto')
					->setCellValue('Q6', 'Mesi inattivita')
					;
				$k = 7;
				$i = 1;
				
				$objPHPExcel->getActiveSheet()->getStyle("I")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("J")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("K")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				
				
				foreach ($resultsclienti as $row) {
					
					
					if($row['is_company'] == 1) {
						$tipo = 'Azienda';
					}
					else {
						$tipo = 'Privato';
					}
					
					
					$data_primo_acquisto = Db::getInstance()->getValue("SELECT o.date_add ".html_entity_decode((isset($_POST['query_from']) ? $_POST['query_from'] : $from))." WHERE o.id_customer = ".$row['id_customer']." ".html_entity_decode((isset($_POST['query_where']) ? $_POST['query_where'] : $where))." ORDER BY o.date_add ASC");
					
					
					
					$data_ultimo_acquisto = Db::getInstance()->getValue("SELECT o.date_add ".html_entity_decode((isset($_POST['query_from']) ? $_POST['query_from'] : $from))." WHERE o.id_customer = ".$row['id_customer']." ".html_entity_decode((isset($_POST['query_where']) ? $_POST['query_where'] : $where))." ORDER BY o.date_add DESC");
					
					$mesi_inattivita = $this->datediff("M", $data_ultimo_acquisto, date("Y-m-d H:i:s"));
					
					if(isset($_POST['esportaaziende']) && $row['is_company'] == 0) {
					}
					else if(isset($_POST['esportaprivati']) && $row['is_company'] == 1) {
					}
					else if(isset($_POST['esportaclienti']) && $row['id_default_group'] == 3) {
					}
					else if(isset($_POST['esportarivenditori']) && $row['id_default_group'] == 1) {
					}
					else {
					
						//$gruppo = Db::getInstance()->getValue("SELECT name FROM group_lang WHERE id_lang = 5 AND id_group = ".$row['id_default_group']."");
						
						//$ultimo_anno = Db::getInstance()->getValue('SELECT SUM(totale_fattura) AS totale_acquisti FROM (SELECT * FROM fattura WHERE id_customer = '.$row['id_customer'].' AND (data_fattura > "'.$anno_corrente.'-01-01 00:00:00" AND data_fattura < "'.$anno_corrente.'-12-31 00:00:00") GROUP BY id_fattura) f');
					
						//$acquisti_totali = Db::getInstance()->getValue('SELECT SUM(totale_fattura) AS totale_acquisti FROM (SELECT * FROM fattura WHERE id_customer = '.$row['id_customer'].'  GROUP BY id_fattura) f');
						
						
						$tot_anno[$key] = $ultimo_anno;
						
						$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue("A$k", $i)
							->setCellValue("B$k", $row['id_customer'])
							->setCellValue("C$k", $row['firstname'])
							->setCellValue("D$k", $row['lastname'])
							->setCellValue("E$k", $row['company'])
							->setCellValue("F$k", $tipo)
							->setCellValue("G$k", $row['id_default_group'])
							->setCellValue("H$k", $row['email'])
							->setCellValue("I$k", $row['phone'])
							->setCellValue("J$k", $row['phome_mobile'])
							->setCellValue("K$k", $row['fax'])
							->setCellValue("L$k", $row['iso_code'])
							->setCellValue("M$k", str_replace(".",",",round($row['totale_acquisti'],2)))
							->setCellValue("N$k", str_replace(".",",",round($row['ultimo_anno'],2)))
							->setCellValue("O$k", Tools::displayDate($data_primo_acquisto,5))
							->setCellValue("P$k", Tools::displayDate($data_ultimo_acquisto,5))
							->setCellValue("Q$k", $mesi_inattivita)
						;
					
						$objPHPExcel->getActiveSheet()->getCell("I$k")->setValueExplicit($row['phone'], PHPExcel_Cell_DataType::TYPE_STRING);
						$objPHPExcel->getActiveSheet()->getCell("J$k")->setValueExplicit($row['phone_mobile'], PHPExcel_Cell_DataType::TYPE_STRING);
						$objPHPExcel->getActiveSheet()->getCell("K$k")->setValueExplicit($row['fax'], PHPExcel_Cell_DataType::TYPE_STRING);
						$objPHPExcel->getActiveSheet()->getStyle("M$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
						$objPHPExcel->getActiveSheet()->getStyle("N$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
						$i++;
						$k++;
					}
				}
				
			
				$objPHPExcel->getActiveSheet()->setTitle('Clienti');

				$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	

				$objPHPExcel->getActiveSheet()->getStyle('A6:Q6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
					
				$objPHPExcel->getActiveSheet()->getStyle('A6:Q6')->getFill()->getStartColor()->setRGB('FFFF00');	

				$objPHPExcel->getActiveSheet()->getStyle("A6:Q$k")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('A6:Q6')->getFont()->setBold(true);

				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
				
				
				// Set active sheet index to the first sheet, so Excel opens this as the first sheet
				$objPHPExcel->setActiveSheetIndex(0);

				$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
				$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
				PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

				$data = date("Ymd");

				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->setPreCalculateFormulas(false);
				$objWriter->save(str_replace(".php", ".xls", "esportazione-catalogo/catalogo_xls/prodotti-clienti-$data.php"));

				$this->_html .= "<div style='visibility:hidden'><br /><br />FILE PRONTO PER IL DOWNLOAD: <a id='scaricaexcelstatistiche' href='http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-clienti-$data.xls'>CLICCA QUI PER SCARICARE</a>!</div>";

				header("Location: http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-clienti-$data.xls");
					
				
				
			
			}
			
			else {
			
			
				echo '<script type="text/javascript" src="../js/select2.js"></script>
				<script type="text/javascript">
				$(document).ready(function() { $("#per_prodotto").select2(); });
				
				$(document).ready(function() { $("#per_categoria").select2(); });
				
				$(document).ready(function() { $("#per_macrocategoria").select2(); });
				
				$(document).ready(function() { $("#per_marchio").select2(); });
				</script>
				
				
				<form name="cercaclienti" method="post" action="">
				
				Cerca clienti che hanno acquistato un preciso prodotto:<br />
				<select multiple id="per_prodotto" name="per_prodotto[]" style="width:600px">
				<option name="0" value="0">--- Scegli un prodotto ---</option>
				';
				
				
				$results = Db::getInstance()->ExecuteS("SELECT product.id_product, product.reference, product.price, product_lang.name FROM product JOIN product_lang ON product.id_product = product_lang.id_product WHERE product_lang.id_lang = 5 ORDER BY name");
				
				foreach ($results as $row) {
				
					echo "<option name='$row[id_product]' value='$row[id_product]'"; 
					echo ">$row[id_product] - $row[name] ($row[reference]) - ".Tools::displayPrice($row['price'], 1, false)."</option>";
				
				}
					
				echo "</select><br /><br />";
				
				echo 'Cerca clienti che hanno acquistato un prodotto in una macrocategoria:<br />
				<select multiple id="per_macrocategoria" name="per_macrocategoria[]" style="width:600px">
				<option name="0" value="0">--- Scegli una macrocategoria ---</option>
				';
				
				
				$resultscat = Db::getInstance()->ExecuteS("SELECT category.id_category, category_lang.name FROM category JOIN category_lang ON category.id_category = category_lang.id_category WHERE category_lang.id_lang = 5 AND category.id_parent = 1 ORDER BY name");
				
				foreach ($resultscat as $rowcat) {
				
					echo "<option name='$rowcat[id_category]' value='$rowcat[id_category]'"; 
					echo ">$rowcat[id_category] - $rowcat[name]</option>";
				
				}
					
				echo "</select><br /><br />";
				
				echo 'Cerca clienti che hanno acquistato un prodotto in una sottocategoria:<br />
				<select multiple id="per_categoria" name="per_categoria[]" style="width:600px">
				<option name="0" value="0">--- Scegli una categoria ---</option>
				';
				
				
				$resultscat = Db::getInstance()->ExecuteS("SELECT category.id_category, category_lang.name FROM category JOIN category_lang ON category.id_category = category_lang.id_category WHERE category_lang.id_lang = 5 AND category.id_parent != 1 ORDER BY name");
				
				foreach ($resultscat as $rowcat) {
				
					echo "<option name='$rowcat[id_category]' value='$rowcat[id_category]'"; 
					echo ">$rowcat[id_category] - $rowcat[name]</option>";
				
				}
					
				echo "</select><br /><br />";
				
				
				
				echo 'Cerca clienti che hanno acquistato un prodotto di un marchio:<br />
				<select multiple id="per_marchio" name="per_marchio[]" style="width:600px">
				<option name="0" value="0">--- Scegli un marchio ---</option>
				';
				
				
				$resultsman = Db::getInstance()->ExecuteS("SELECT id_manufacturer, name FROM manufacturer ORDER BY name");
				
				foreach ($resultsman as $rowman) {
				
					echo "<option name='$rowman[id_manufacturer]' value='$rowman[id_manufacturer]'"; 
					echo ">$rowman[id_manufacturer] - $rowman[name]</option>";
				
				}
					
				echo "</select><br /><br />";
				
				echo "<input type='checkbox' value='0' name='escludi-privati' /> Escludi privati<br /><br />";
				
				echo "Seleziona un arco temporale:<br /><br />";
				
				include_once('functions.php');
			
				$this->includeDatepickerZ(array('arco_dal', 'arco_al'), true);
				
				echo 'Dal <input type="text" id="arco_dal" name="arco_dal"  /> al <input type="text" id="arco_al" name="arco_al"  /><br /><br />';
				
				echo '
				<input type="radio" name="su_tipo" value="ordinato"> Sull\'ordinato<br />
				<input type="radio" name="su_tipo" value="preventivi"> Sui preventivi<br />
				<!-- <input type="radio" name="su_tipo" value="entrambi"> Su entrambi<br /> -->
				';
				
				echo "<input type='submit' name='cercaiclienti' value='Cerca clienti' class='button' />
				</form>";
			
			}
			
			
			
		}

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	