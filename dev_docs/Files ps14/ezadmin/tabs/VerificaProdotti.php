<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14632 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class VerificaProdotti extends AdminTab
{
		public function display()
		{	
			global $cookie, $currentIndex;
			echo "<h1>Approvvigionamento</h1>";
			
			
		
			$tokenOrders = Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee);
			$tokenProducts = Tools::getAdminToken('AdminProducts'.(int)Tab::getIdFromClassName('AdminProducts').(int)$cookie->id_employee);
			$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
			
			if(isset($_GET['per_ordine'])) {
				$o = "&per_ordine=s";
			}
			else {
				$o = "";
			}
			
			echo '<!-- <strong><a href="'.$currentIndex.'&token='.($token!=NULL ? $token : $this->token).'">CLICCA QUI PER RAGGRUPPAMENTO SU BASE PRODOTTO</a></strong>';
			echo '<br /><br />';
			echo '<strong><a href="'.$currentIndex.'&token='.($token!=NULL ? $token : $this->token).'&per_ordine=s">CLICCA QUI PER RAGGRUPPAMENTO SU BASE ORDINE</a></strong><br /><br /> -->';
			
			if(Tools::getIsset('cercagliordini'))
			{
				$cookie->sql_prodotti_verifica = '';
				$cookie->_per_prodotto_ap = "";
				$cookie->_per_costruttore_ap = "";
				$cookie->_per_categoria_ap = "";
				$cookie->_per_fornitore_ap = "";
				$cookie->_solo_sm_ap = "";
				$cookie->_solo_do_ap = "";
				$cookie->_tutti_p = "";
			}
			if(Tools::getIsset('per_prodottox')) {
				
				$per_prodotto = serialize($_POST['per_prodottox']);
				
				foreach ($_POST['per_prodottox'] as $prodotto) {

					$nomeprodotto = Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_lang = 5 AND id_product = ".$prodotto."");
							
					$prodottiinclusi.= $nomeprodotto."; ";
							
					$strisciaprodotti.= "- ".$nomeprodotto."<br />";
		
					$prodotti.= "p.id_product = $prodotto OR ";
						
				}
						
					$cookie->sql_prodotti_verifica .= "AND ($prodotti p.id_product =99999999999999999) ";
					$cookie->_per_prodotto_ap = $per_prodotto;
					
			}
			
			if(Tools::getIsset('per_costruttorex')) {
				
				$per_costruttore = serialize($_POST['per_costruttorex']);
				
				foreach ($_POST['per_costruttorex'] as $costruttore) {
					
					$nomecostruttore = Db::getInstance()->getValue("SELECT name FROM manufacturer WHERE id_manufacturer = ".$costruttore."");
		
					$costruttori.= "p.id_manufacturer = $costruttore OR ";
						
				}
						
					$cookie->sql_prodotti_verifica .= "AND ($costruttori p.id_manufacturer =99999999999999999) ";
					$cookie->_per_costruttore_ap = $per_costruttore;
			}
			
			if(Tools::getIsset('per_categoriax')) {
						
				foreach ($_POST['per_categoriax'] as $categoria) {
					$per_categoria = serialize($_POST['per_categoriax']);
					$nomecategoria = Db::getInstance()->getValue("SELECT name FROM category_lang WHERE id_lang = 5 AND id_category =  ".$categoria."");
		
					$categorie.= "p.id_category_default = $categoria OR ";
						
				}
						
					$cookie->sql_prodotti_verifica .= "AND ($categorie p.id_category_default =99999999999999999) ";
					$cookie->_per_categoria_ap = $per_categoria;
			}
			
			if(Tools::getIsset('per_fornitorex')) {
				
				$per_fornitore = serialize($_POST['per_fornitorex']);
				
				foreach ($_POST['per_fornitorex'] as $fornitore) {
					
					$nomefornitore = Db::getInstance()->getValue("SELECT name FROM supplier WHERE id_supplier = ".$supplier."");
		
					$fornitori.= "p.id_supplier = $fornitore OR ";
						
				}
						
					$cookie->sql_prodotti_verifica .= "AND ($fornitori p.id_supplier =99999999999999999) ";
					$cookie->_per_fornitore_ap = $per_fornitore;
			}
			
			if(!isset($cookie->sql_prodotti_verifica)) {
				$cookie->sql_prodotti_verifica = "";
				$cookie->_per_prodotto_ap = "";
				$cookie->_per_costruttore_ap = "";
				$cookie->_per_categoria_ap = "";
			}
			
			if(isset($_POST['solo_sm']))
			{
				$cookie->sql_prodotti_verifica .= "AND (p.scorta_minima > 0)";
				$cookie->_solo_sm_ap = 'y';
			}
			
			if(isset($_POST['solo_do']))
			{
				$cookie->_solo_do_ap = 'y';
			}
			
			if(isset($_POST['tutti_p']))
			{
				$cookie->_tutti_p = 'y';
			}
			
			if(isset($_POST['resetta'])) {
				$cookie->sql_prodotti_verifica = "";
				$cookie->_per_prodotto_ap = "";
				$cookie->_per_costruttore_ap = "";
				$cookie->_per_fornitore_ap = "";
				$cookie->_per_categoria_ap = "";
				$cookie->_solo_sm_ap = "";
				$cookie->_solo_do_ap = "";
				$cookie->_tutti_p = "";
			}
				
			
			
			
			echo '<script type="text/javascript" src="../js/select2.js"></script>
			<script type="text/javascript">
			$(document).ready(function() { $("#per_prodottox").select2(); });
			$(document).ready(function() { $("#per_costruttorex").select2(); });
			$(document).ready(function() { $("#per_categoriax").select2(); });
			$(document).ready(function() { $("#per_fornitorex").select2(); });
			</script>
			<form name="cercaordini" method="post" action="index.php?tab=VerificaProdotti&token='.$this->token.$o.'">
			<input type="checkbox" name="tutti_p" id="tutti_p" '.($cookie->_tutti_p == 'y' ? 'checked="checked"' : '').' onclick="document.getElementById(\'solo_sm\').checked = false;"/>Visualizza tutti i prodotti<br /><br />
			
			<input type="checkbox" name="solo_sm" id="solo_sm" '.($cookie->_solo_sm_ap == 'y' ? 'checked="checked"' : '').' onclick="document.getElementById(\'tutti_p\').checked = false;" />Visualizza solo prodotti che hanno scorta minima<br /><br />
			<input type="checkbox" name="solo_do" id="solo_do" '.($cookie->_solo_do_ap == 'y' ? 'checked="checked"' : '').' />Visualizza solo prodotti da ordinare al fornitore<br /><br />
			
			<table>
			<tr>
			<td>
			
			Cerca in base al prodotto:
			</td>
			<td>
			<select multiple id="per_prodottox" name="per_prodottox[]" style="width:500px">
			<option name="0" value="0">--- Scegli un prodotto ---</option>
			';
				
				
			$results = Db::getInstance()->ExecuteS("SELECT product.id_product, product.reference, product_lang.name FROM product JOIN product_lang ON product.id_product = product_lang.id_product WHERE product_lang.id_lang = 5 ORDER BY name");
			
			
			$per_prodotto_ap = unserialize($cookie->_per_prodotto_ap);
			
			foreach ($results as $row) {
				
				echo "<option name='$row[id_product]' value='$row[id_product]' ".(in_array($row['id_product'], $per_prodotto_ap) ? "selected='selected'" : '').""; 
				echo ">$row[id_product] - $row[name] ($row[reference])</option>";
				
			}
					
			echo "</select></td><td>";
			
			echo "&nbsp;</td></tr>";
				
			echo '<tr><td>
				
			Cerca in base al costruttore:</td>
			<td>
			<select multiple id="per_costruttorex" name="per_costruttorex[]" style="width:500px">
			<option name="0" value="0">--- Scegli un costruttore ---</option>
			';
				
				
			$results = Db::getInstance()->ExecuteS("SELECT * FROM manufacturer ORDER BY name");
			
			$per_costruttore_ap = unserialize($cookie->_per_costruttore_ap);			
			foreach ($results as $row) {
				
				echo "<option name='$row[id_manufacturer]' value='$row[id_manufacturer]'".(in_array($row['id_manufacturer'], $per_costruttore_ap) ? "selected='selected'" : '').""; 
				echo ">$row[id_manufacturer] - $row[name]</option>";
			
			}
					
			echo "</select></td><td>";
			
			echo "</td></tr>";
			
			echo '<tr><td>
				
			Cerca in base alla categoria:</td>
			<td>
			<select multiple id="per_categoriax" name="per_categoriax[]" style="width:500px">
			<option name="0" value="0">--- Scegli una categoria ---</option>
			';
				
				
			$results = Db::getInstance()->ExecuteS("SELECT c.id_category, cl.name FROM category c JOIN category_lang cl ON c.id_category = cl.id_category WHERE cl.id_lang = 5 ORDER BY cl.name");
				
			$per_categoria_ap = unserialize($cookie->_per_categoria_ap);			
			foreach ($results as $row) {
				
				echo "<option name='$row[id_category]' value='$row[id_category]' ".(in_array($row['id_category'], $per_categoria_ap) ? "selected='selected'" : '').""; 
				echo ">$row[id_category] - $row[name]</option>";
				
			}
					
			echo "</select></td><td>";
			
			
			echo "&nbsp;</td></tr>";
			
			echo '<tr><td>
				
			Cerca in base al fornitore:</td>
			<td>
			<select multiple id="per_fornitorex" name="per_fornitorex[]" style="width:500px">
			<option name="0" value="0">--- Scegli un fornitore ---</option>
			';
				
				
			$results = Db::getInstance()->ExecuteS("SELECT * FROM supplier ORDER BY name");
			
			$per_fornitore_ap = unserialize($cookie->_per_fornitore_ap);			
			foreach ($results as $row) {
				
				echo "<option name='$row[id_supplier]' value='$row[id_supplier]'".(in_array($row['id_supplier'], $per_fornitore_ap) ? "selected='selected'" : '').""; 
				echo ">$row[id_supplier] - $row[name]</option>";
			
			}
					
			echo "</select></td><td>";
			
			echo "</td></tr>";
			
			echo "</table>
			&nbsp;<input type='submit' name='cercagliordini' value='Cerca' class='button' />&nbsp;&nbsp;&nbsp;
			<input type='submit' name='resetta' value='Resetta' class='button' />
			</form><br /><br />";
			
			echo '<strong><a href="'.$_SERVER["HTTP_NAME"].$_SERVER["REQUEST_URI"].'&astec=s">Clicca qui per tutti gli ordini con assistenza tecnica</a></strong><br /><br />';
			
			
			echo "
			<style type='text/css'>
			#tableVerificaProdotti {
			}
			
			#tableVerificaProdotti tbody {
				
			}

			#tableVerificaProdotti td {
				color:#000;
			}
			
			
			</style>
			
			<script type='text/javascript'>
				$(document).ready(function() {
					function moveScroll(){
						var scroll = $(window).scrollTop();
						var anchor_top = $('#tableVerificaProdotti').offset().top;
						var anchor_bottom = $('#bottom_anchor').offset().top;
						if (scroll>anchor_top && scroll<anchor_bottom) {
						clone_table = $('#clone');
						if(clone_table.length == 0){
							clone_table = $('#tableVerificaProdotti').clone();
							clone_table.attr('id', 'clone');
							clone_table.css({position:'fixed',
									 'pointer-events': 'none',
									 top:0});
							clone_table.width($('#tableVerificaProdotti').width());
							$('#table-container').append(clone_table);
							$('#clone').css({visibility:'hidden'});
							$('#clone thead').css({'visibility':'visible','pointer-events':'auto'});
						}
						} else {
						$('#clone').remove();
						}
					}
					$(window).scroll(moveScroll); 
				});
			</script>
			<div id='table-container'>
			
			<table class='table' id='tableVerificaProdotti'>
			<thead>
			<tr>";
			if(isset($_GET['per_ordine'])) {
			
				echo "<th style='width:50px'>ID <br />ordine<br /><a href='index.php?tab=VerificaProdotti&orderby=o.id_order&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
				<a href='index.php?tab=VerificaProdotti&orderby=o.id_order&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a>
				</th>";
			}
			else {
				
				
			}
	
			echo "
	
			<th style='width:140px'>Codice<br /><br /> <a href='index.php?tab=VerificaProdotti&orderby=p.reference&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=VerificaProdotti&orderby=p.reference&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
			
			<th style='width:140px'>Prodotto<br /><br /> <a href='index.php?tab=VerificaProdotti&orderby=nome_prodotto&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=VerificaProdotti&orderby=nome_prodotto&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
			
			<th style='width:100px'>Costruttore<br /><br /> <a href='index.php?tab=VerificaProdotti&orderby=costruttore&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=VerificaProdotti&orderby=costruttore&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
			
			<th style='width:100px'>Fornitore<br /><br /> <a href='index.php?tab=VerificaProdotti&orderby=fornitore&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=VerificaProdotti&orderby=fornitore&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
			
			
			
			if(Tools::getIsset('per_ordine')) {
				echo "<th style='width:50px'>Imp.<br /> per ord. <br /><a href='index.php?tab=VerificaProdotti&orderby=ordinati&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=VerificaProdotti&orderby=ordinati&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
			
			}
			
			
			echo "
			
			<th style='width:50px'>Mag.<br /> EZ  <br /><a href='index.php?tab=VerificaProdotti&orderby=magazzino&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=VerificaProdotti&orderby=magazzino&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
			
			<th style='width:50px'>Impe-<br />gnati <br />";
			
			
			if(Tools::getIsset('per_ordine')) {
				echo "<a href='index.php?tab=VerificaProdotti&orderby=impegnati&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
				<a href='index.php?tab=VerificaProdotti&orderby=impegnati&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
				
			}
			else {
				echo "<a href='index.php?tab=VerificaProdotti&orderby=ordinati&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
				<a href='index.php?tab=VerificaProdotti&orderby=ordinati&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
			
			}
			echo "
			
			<th style='width:50px'>Scorta minima <a href='index.php?tab=VerificaProdotti&orderby=scorta_minima&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=VerificaProdotti&orderby=scorta_minima&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
		
			<th style='width:50px'>Da ordinare<br /> <a href='index.php?tab=VerificaProdotti&orderby=da_ordinare&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=VerificaProdotti&orderby=da_ordinare&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
			
			
			<th style='width:50px'>Allnet <br /> <br /><a href='index.php?tab=VerificaProdotti&orderby=allnet&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=VerificaProdotti&orderby=allnet&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
			
			<th style='width:50px'>Esprinet <br /> <br /><a href='index.php?tab=VerificaProdotti&orderby=esprinet&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=VerificaProdotti&orderby=esprinet&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
			
			<th style='width:50px'>ITANCIA <br /> <br /><a href='index.php?tab=VerificaProdotti&orderby=itancia&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=VerificaProdotti&orderby=itancia&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
			
			<th style='width:50px'>Mag. <br />Tot. <br /><a href='index.php?tab=VerificaProdotti&orderby=totale&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=VerificaProdotti&orderby=totale&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
			
			<th style='width:50px'>Ordinato <br /> <br /><a href='index.php?tab=VerificaProdotti&orderby=qt_ordinato&orderway=asc&token=".$this->token.$o."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=VerificaProdotti&orderby=qt_ordinato&orderway=desc&token=".$this->token.$o."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>
			";
			if(isset($_GET['per_ordine'])) {
			}
			else {
				echo "<th style='width:100px'>Ordini associati</th>";
			}
			echo "</tr></thead><tbody>";
			if(isset($_GET['per_ordine'])) {
				//SQL per raggruppamento ordine
				$queryorders = 'SELECT od.id_order_detail, o.id_order, p.id_product, p.reference, p.stock_quantity as magazzino, pl.name AS nome_prodotto, p.supplier_quantity as allnet, p.esprinet_quantity as esprinet, p.itancia_quantity as itancia, p.ordinato_quantity as qt_ordinato, p.quantity as totale, m.name as costruttore, s.name AS fornitore, od.product_quantity AS ordinati, ord.impegnati, ord.impegnati - p.stock_quantity AS da_ordinare FROM product p 
				
				JOIN
				
				(SELECT product_id, sum( product_quantity - (REPLACE(cons,"0_","")) ) as impegnati
					FROM order_detail od JOIN orders o ON od.id_order = o.id_order JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE moh.id_order_state != 18 AND  moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state)
					
					
					
					'.(isset($_GET['astec']) ? 'JOIN (SELECT id_order	FROM order_detail	WHERE product_reference LIKE \'%astec%\'	OR product_reference LIKE \'%inst%\' 		GROUP BY id_order)ox ON ox.id_order = o.id_order' : '').'
					
					WHERE os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16 AND os.id_order_state !=20  AND os.id_order_state !=26  AND os.id_order_state !=29 AND os.id_order_state != 35 AND os.id_order_state !=31 AND os.id_order_state !=32 GROUP BY product_id) ord ON p.id_product = ord.product_id
					
				JOIN product_lang pl ON p.id_product = pl.id_product JOIN manufacturer m ON p.id_manufacturer = m.id_manufacturer LEFT JOIN supplier s ON p.id_supplier = s.id_supplier JOIN order_detail od ON od.product_id = p.id_product JOIN orders o ON o.id_order = od.id_order JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 35 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state !=20  AND os.id_order_state !=26  AND os.id_order_state !=29  AND os.id_order_state !=31 AND os.id_order_state !=32 AND pl.id_lang = 5 '.$cookie->sql_prodotti_verifica.' ORDER BY '.(isset($_GET['orderby']) ? $_GET['orderby'] : 'o.id_order').' '.(isset($_GET['orderway']) ? $_GET['orderway'] : 'ASC').'
				 
				';
				
				$orders = Db::getInstance()->executeS($queryorders);
				
			
			}
			else {
				$queryorders = 'SELECT p.id_product, p.reference, p.stock_quantity as magazzino, sku_amazon, fnsku, asin, scorta_minima_amazon, impegnato_amazon, p.scorta_minima, pl.name AS nome_prodotto, p.ordinato_quantity as qt_ordinato, p.supplier_quantity as allnet, p.esprinet_quantity as esprinet, p.itancia_quantity as itancia, p.quantity as totale, pl.name, (CASE WHEN ord.ordinati > 0 THEN ord.ordinati ELSE 0 END) ordinati, ord.ordinati - p.stock_quantity - p.ordinato_quantity AS da_ordinare, m.name AS costruttore, s.name AS fornitore FROM
				
				product p LEFT JOIN

				
				(SELECT o.id_cart, ca.riferimento, product_id, sum( product_quantity - (REPLACE(cons,\'0_\',\'\')) ) as ordinati
					FROM order_detail od JOIN orders o ON od.id_order = o.id_order '.(isset($_GET['astec']) ? '
					JOIN (SELECT id_order	FROM order_detail	WHERE product_reference LIKE \'%astec%\'	OR product_reference LIKE \'%inst%\' 		GROUP BY id_order)ox ON ox.id_order = o.id_order' : '').' JOIN cart ca ON o.id_cart = ca.id_cart JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE moh.id_order_state != 18 AND moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state !=20 AND os.id_order_state !=26  AND os.id_order_state !=29  AND os.id_order_state !=30  AND os.id_order_state !=31  AND os.id_order_state !=32 AND os.id_order_state != 35 AND ca.riferimento NOT LIKE \'%BDL%\' GROUP BY product_id) ord ON p.id_product = ord.product_id

				JOIN product_lang pl ON p.id_product = pl.id_product
				JOIN manufacturer m ON p.id_manufacturer = m.id_manufacturer 
				LEFT JOIN supplier s ON p.id_supplier = s.id_supplier
				
				WHERE pl.id_lang = 5 
				AND p.fuori_produzione = 0
				AND (
				
				'.($cookie->sql_prodotti_verifica == '' ? 'p.scorta_minima > 0 OR (pl.id_lang = 5' : '(pl.id_lang = 5').' 
				
				
				'.$cookie->sql_prodotti_verifica.'
				
				'.($cookie->_solo_sm_ap == 'y' ? '' : ($cookie->_tutti_p == 'y' ? '' : 'AND ((p.scorta_minima > 0 AND ord.ordinati > 0) 
				OR  (p.active = 1 AND p.ordinato_quantity > 0) 
				 OR ((p.scorta_minima_amazon - p.impegnato_amazon) > 0)
				OR (p.scorta_minima > 0 AND ord.ordinati = 0) OR (p.scorta_minima = 0 AND ord.ordinati > 0) OR (p.scorta_minima > 0 AND ord.ordinati IS NULL) )')).'
				
		
				))
				
				GROUP BY p.id_product 
				 
				ORDER BY '.(isset($_GET['orderby']) ? $_GET['orderby'] : 'p.reference').' '.(isset($_GET['orderway']) ? $_GET['orderway'] : 'ASC').'
				 
				';
				
				$orders = Db::getInstance()->executeS($queryorders);
			}
			
			
			$tokenCatalog = Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee));
			foreach ($orders as $order) {
				
				//controllo per consegne parziali
				
				$parziali = Db::getInstance()->executeS('SELECT o.id_order, product_quantity FROM order_detail od JOIN orders o ON o.id_order = od.id_order JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE moh.id_order_state != 18 AND moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE os.id_order_state = 15 AND od.product_id = '.$order['id_product'].'');
				
				foreach($parziali as $parziale)
				{
					$order['ordinati'] -= ($parziale['product_quantity'] - $parziale['ordinati']);
					
					/*$array_parziali1 = Db::getInstance()->executeS('SELECT cod_articolo FROM fattura WHERE rif_vs_ordine = '.$parziale['id_order']);
					$array_parziali = array();
					foreach($array_parziali1 as $ar)
						$array_parziali[] = trim($ar['cod_articolo']);
						
					if(in_array($order['reference'], $array_parziali))
					{
						// $order['ordinati'] -= Db::getInstance()->getValue('SELECT SUM(qt_ord) FROM fattura WHERE cod_articolo = "'.trim($order['reference']).'" AND rif_vs_ordine = '.$parziale['id_order'].' GROUP BY cod_articolo');

					}*/
				}	
				
				if(isset($_GET['per_ordine'])) {
				}
				else {
					
					$ordini_associati = Db::getInstance()->executeS("SELECT o.id_order AS ordine, o.id_customer FROM order_detail od JOIN orders o ON od.id_order = o.id_order JOIN order_history oh ON ( oh.id_order_history = (SELECT MAX( id_order_history ) FROM order_history moh WHERE moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order ) ) JOIN order_state os ON ( os.id_order_state = oh.id_order_state ) WHERE os.id_order_state !=4 AND os.id_order_state !=5 AND os.id_order_state !=6 AND os.id_order_state !=7 AND os.id_order_state !=8 AND os.id_order_state !=14 AND os.id_order_state !=16  AND os.id_order_state !=20 AND os.id_order_state !=26  AND os.id_order_state !=29 AND os.id_order_state !=31 AND os.id_order_state !=32 AND od.product_id = ".$order['id_product']."");
					
					foreach($ordini_associati as $ord)
					{
						
						/*$qt_fatt = Db::getInstance()->getValue('SELECT qt_ord FROM fattura WHERE rif_vs_ordine = "'.$ord['ordine'].'" AND TRIM(cod_articolo) = "'.trim($order['reference']).'"');
						if($qt_fatt > 0)
							$order['ordinati'] = $order['ordinati'] - $qt_fatt;
						if($order['ordinati'] < 0)
							$order['ordinati'] = 0;
						*/
					}	

				}
				//if($order['scorta_minima'] > 0 && (($order['magazzino']-$order['ordinati']+$order['qt_ordinato']) < $order['scorta_minima']))
				//{
					if(isset($_GET['per_ordine'])) 
						$order['da_ordinare'] = $order['scorta_minima'] - ($order['magazzino'] - ($order['impegnati']) + $order['qt_ordinato']);
					else
						$order['da_ordinare'] = $order['scorta_minima'] - ($order['magazzino'] - ($order['ordinati']) + $order['qt_ordinato']);
				//}
				
				
				
			
		
					
				//  || $order['da_ordinare'] > 0 && $order['qt_ordinato'] <= $order['da_ordinare']
				echo "<tr style=' ".($cookie->_solo_do_ap == 'y' && $order['da_ordinare'] <= 0 ? ';display:none' : '')."'>";
				if(isset($_GET['per_ordine'])) {
					echo "<td style='width:50px'>".$order['id_order']."</td>";
				}
				else {
				}
		
				echo "<td style='width:140px'>".'<span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($order['id_product']).'"><a href="index.php?tab=AdminCatalog&id_product='.$order['id_product'].'&updateproduct&token='.$tokenCatalog.'">'.$order['reference']."</a></span></td><td style='width:140px'>".$order['nome_prodotto']."</td><td style='width:100px'>".$order['costruttore']."</td><td style='width:100px'>".$order['fornitore']."</td>";
				
				
				echo "<td style='text-align:right'>".$order['magazzino']." </td>";
				
				if(isset($_GET['per_ordine'])) {
					echo "<td style='text-align:right'>".$order['ordinati']."</td>";
				}
				else {
				
				}
				echo "<td style='text-align:right'>".(Tools::getIsset('per_ordine') ? $order['impegnati'] : $order['ordinati'])."</td>";
				
				echo "<td style='width:80px; text-align:right; ".($order['scorta_minima'] > 0 && (($order['magazzino']-$order['ordinati']+$order['qt_ordinato']) < $order['scorta_minima']) ? " border:1px solid red'><img style='float:left' src='../img/admin/error2.png' alt='Sotto scorta' title='Sotto scorta'>" : 
				($order['scorta_minima'] > 0 && ($order['qt_ordinato'] > 0 && ($order['magazzino']-$order['ordinati']+$order['qt_ordinato']) > $order['scorta_minima']) ? "border:1px solid green '><img style='float:left' src='../img/admin/delivery.gif' width='24' height='24' alt='In arrivo' title='In arrivo'>" :	"'> " ))."  ".$order['scorta_minima']."</td>";
				
				echo "<td style='text-align:right; ".($order['da_ordinare'] <= 0 ? "" : "background-color:#fff0c7")."'>".($order['da_ordinare'] <= 0 && $order['qt_ordinato'] >= $order['da_ordinare'] ? 0 : $order['da_ordinare'])."</td>";
				
				
				
				echo "<td style='text-align:right'>".$order['allnet']."</td><td style='text-align:right'>".$order['esprinet']."</td><td style='text-align:right'>".$order['itancia']."</td><td style='text-align:right'>".$order['totale']."</td><td style='text-align:right'>".$order['qt_ordinato']."</td>";
		
				if(isset($_GET['per_ordine'])) {
				}
				else {
					echo "<td>";
					
					$lunghezza_array = count($ordini_associati);
					$i= 0;
			
					foreach($ordini_associati as $ordine_associato) {
						$i++;
						$fatt = Db::getInstance()->getValue('SELECT rif_ordine FROM fattura WHERE rif_ordine = "'.$ordine_associato['ordine'].'"');
						if($fatt > 0) { }
						else {
							echo '<a href="?tab=AdminCustomers&id_customer='.$ordine_associato['id_customer'].'&viewcustomer&id_order='.$ordine_associato['ordine'].'&vieworder&token='.$tokenCustomers.'&tab-container-1=4" target="_blank">'.$ordine_associato['ordine'].'</a>'.($i == $lunghezza_array ? "" : " - ").'';
						}
					}
					echo "</td>";
				}
				echo "</tr>";
	
			}
		
			echo "</tbody></table>
			<div id='bottom_anchor'>
			</div>";
		
			echo "<br /><form method='post' action=''>";
				
				
			echo "
			<input type='hidden' name='querysql' value=\"".$queryorders."\" />		
			<input type='submit' value='Esporta i dati in formato Excel' name='esportaexcel' class='button'  onclick='window.onbeforeunload = null' />
			<input type='submit' value='Esporta file per ordini fornitore' name='esportaexcel_ord' class='button' onclick='window.onbeforeunload = null' />
			</form>
			";
			echo "<br /><br />";
		
			if(isset($_POST['esportaexcel']) || isset($_POST['esportaexcel_ord'])) { 
				ini_set("memory_limit","892M");
				set_time_limit(3600);
				require_once 'esportazione-catalogo/Classes/PHPExcel.php';
				$objPHPExcel = new PHPExcel();
				
				$results = Db::getInstance()->ExecuteS($_POST['querysql']);
				
				if(isset($_POST['esportaexcel'])) {
					
					if(isset($_GET['per_ordine'])) {
					
						$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A1', 'Id ordine')
						->setCellValue('B1', 'Codice')
						->setCellValue('C1', 'Nome prodotto')
						->setCellValue('D1', 'Costruttore')
						->setCellValue('E1', 'Fornitore')
						->setCellValue('F1', 'Impegnati')
						->setCellValue('G1', 'Ord. a for.')
						->setCellValue('H1', 'Mag. EZ')
						->setCellValue('I1', 'Mag. Allnet')
						->setCellValue('J1', 'Mag. Esprinet')
						->setCellValue('K1', 'Mag. Itancia')
						->setCellValue('L1', 'Mag. tot')
						->setCellValue('M1', 'Qt. ord.')
						;
						$objPHPExcel->getActiveSheet()->getStyle("B")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
					} else {
						
							
						$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A1', 'Codice')
						->setCellValue('B1', 'Nome prodotto')
						->setCellValue('C1', 'Costruttore')
						->setCellValue('D1', 'Fornitore')
						->setCellValue('E1', 'Mag. EZ')
						->setCellValue('F1', 'Impegnati')
						->setCellValue('G1', 'Netto disp.')
						->setCellValue('H1', 'Fabbisogno')
						->setCellValue('I1', 'Extra ord')
						->setCellValue('J1', 'Scorta min.')
						->setCellValue('K1', 'Mag. Allnet')
						->setCellValue('L1', 'Mag. Esprinet')
						->setCellValue('M1', 'Mag. Itancia')
						->setCellValue('N1', 'Mag. tot')
						->setCellValue('O1', 'Qt. ord')
						;
						
					
					}
					$objPHPExcel->getActiveSheet()->getStyle("A")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
					$i = 2;
					foreach ($results as $row) {
					
						if(isset($_GET['per_ordine'])) {
							$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue("A$i", $row['id_order'])
							->setCellValue("B$i", $row['reference'])
							->setCellValue("C$i", $row['nome_prodotto'])
							->setCellValue("D$i", $row['costruttore'])
							->setCellValue("E$i", $row['fornitore'])
							->setCellValue("F$i", $row['ordinati'])
							->setCellValue("G$i", "-")
							->setCellValue("H$i", $row['magazzino'])
							->setCellValue("I$i", $row['allnet'])
							->setCellValue("J$i", $row['esprinet'])
							->setCellValue("K$i", $row['itancia'])
							->setCellValue("L$i", $row['totale'])
							->setCellValue("M$i", $row['qt_ordinato'])
							;
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(70);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
						}
						else {
							$ordini_associati = Db::getInstance()->executeS("SELECT o.id_order AS ordine FROM order_detail od JOIN orders o ON od.id_order = o.id_order JOIN order_history oh ON ( oh.id_order_history = (SELECT MAX( id_order_history ) FROM order_history moh WHERE moh.id_order_state != 18 AND moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order ) ) JOIN order_state os ON ( os.id_order_state = oh.id_order_state ) WHERE os.id_order_state !=4 AND os.id_order_state !=5 AND os.id_order_state !=6 AND os.id_order_state !=7 AND os.id_order_state !=8 AND os.id_order_state !=14  AND os.id_order_state !=20 AND os.id_order_state !=16 AND os.id_order_state !=26  AND os.id_order_state !=29 AND os.id_order_state !=31  AND os.id_order_state !=32 AND od.product_id = ".$row['id_product']."");
							$lunghezza_array = count($ordini_associati);
							$j= 0;
							$ordini_associati_string = "";
							foreach($ordini_associati as $ordine_associato) {
								$j++;
								$ordini_associati_string.= $ordine_associato['ordine'].($j == $lunghezza_array ? "" : " - ").'';
					
							}
							
							foreach($ordini_associati as $ord)
							{
								
								/*$qt_fatt = Db::getInstance()->getValue('SELECT qt_ord FROM fattura WHERE rif_vs_ordine = "'.$ord['ordine'].'" AND TRIM(cod_articolo) = "'.trim($row['reference']).'"');
								if($qt_fatt > 0)
									$row['ordinati'] = $row['ordinati'] - $qt_fatt;
								if($row['ordinati'] < 0)
									$row['ordinati'] = 0;
								*/
							}	
									
							$row['da_ordinare'] = 0-($row['magazzino'] - ($row['ordinati']) + $row['qt_ordinato']);
							
							if($row['scorta_minima'] > 0 && (($row['magazzino']-$row['ordinati']+$row['qt_ordinato']) < $row['scorta_minima']))
								$row['extra_ord'] = $row['scorta_minima'] - ($row['magazzino'] - ($row['ordinati']) + $row['qt_ordinato']);
							else
								$row['extra_ord'] = 0;
							
							$sc_att = $row['magazzino']-$row['ordinati']+$row['qt_ordinato'];
							
							
							if($row['da_ordinare'] > 0 || $row['extra_ord'] > 0)
							{	
								$objPHPExcel->getActiveSheet()->getStyle("A$i:O$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
								$objPHPExcel->getActiveSheet()->getStyle("A$i:O$i")->getFill()->getStartColor()->setRGB('fff0c7');	
							}
							
							if($row['scorta_minima'] > 0)
							{	
								if($sc_att < $row['scorta_minima'])
								{
									$objPHPExcel->getActiveSheet()->getStyle("J$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
									$objPHPExcel->getActiveSheet()->getStyle("J$i")->getFill()->getStartColor()->setRGB('ffb4b4');	
									
								}
								else
								{
									if($row['qt_ordinato'] > 0 && $sc_att > $row['scorta_minima'])
									{
										$objPHPExcel->getActiveSheet()->getStyle("J$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
										$objPHPExcel->getActiveSheet()->getStyle("J$i")->getFill()->getStartColor()->setRGB('daffc8');	
									}
								}
							}
							
							$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue("A$i", $row['reference'])
							->setCellValue("B$i", $row['nome_prodotto'])
							->setCellValue("C$i", $row['costruttore'])
							->setCellValue("D$i", $row['fornitore'])
							->setCellValue("E$i", $row['magazzino'])
							->setCellValue("F$i", $row['ordinati'])
							->setCellValue("G$i", ($row['magazzino'] - $row['impegnati']))
							->setCellValue("H$i", ($row['da_ordinare'] > 0 ? $row['da_ordinare'] : 0))
							->setCellValue("I$i", $row['extra_ord'])
							->setCellValue("J$i", $row['scorta_minima'])
							->setCellValue("K$i", $row['allnet'])
							->setCellValue("L$i", $row['esprinet'])
							->setCellValue("M$i", $row['itancia'])
							->setCellValue("N$i", $row['totale'])
							->setCellValue("O$i", $row['qt_ordinato'])
							;
							
							$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
							$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(70);
							$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
							$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
							$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
							$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
							
						}
						$i++;
					}
					$objPHPExcel->getActiveSheet()->setTitle('Verifica prodotti');

					$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	

					$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFill()->getStartColor()->setRGB('FFFF00');	

					$objPHPExcel->getActiveSheet()->getStyle("A1:O$i")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					
					$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true);
					
					$objPHPExcel->getActiveSheet()->getStyle("E2:E$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("F2:F$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("G2:G$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("H2:H$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("I2:I$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("J2:J$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("K2:K$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("L2:L$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("M2:L$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("N2:L$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("O2:L$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
					$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
					PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

					$data = date("Ymd");

					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
					$objWriter->setPreCalculateFormulas(false);
					$objWriter->save(str_replace(".php", ".xls", "esportazione-catalogo/catalogo_xls/verifica-prodotti-$data.php"));

					$this->_html .= "<div style='visibility:hidden'><br /><br />FILE PRONTO PER IL DOWNLOAD: <a id='scaricaexcelstatistiche' href='http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/verifica-prodotti-$data.xls' onclick='window.onbeforeunload = null'>CLICCA QUI PER SCARICARE</a>!</div>";

					header("Location: http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/verifica-prodotti-$data.xls");
				}
				else
				{
					
					$queryorders = "SELECT p.id_product, p.reference, p.stock_quantity as magazzino, p.scorta_minima, pl.name AS nome_prodotto, p.ordinato_quantity as qt_ordinato, p.supplier_quantity as allnet, p.esprinet_quantity as esprinet, p.itancia_quantity as itancia, p.quantity as totale, pl.name, ord.ordinati, ord.ordinati - p.stock_quantity - p.ordinato_quantity AS da_ordinare, m.name AS costruttore, s.name AS fornitore FROM product p LEFT JOIN (SELECT product_id, sum( product_quantity - (REPLACE(cons,'0_','')) ) as ordinati FROM order_detail od JOIN orders o ON od.id_order = o.id_order JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE moh.id_order_state != 18 AND moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7  AND os.id_order_state != 35 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16 AND os.id_order_state !=20 AND os.id_order_state !=26  AND os.id_order_state !=29 AND os.id_order_state !=31  AND os.id_order_state !=32 GROUP BY product_id) ord ON p.id_product = ord.product_id JOIN product_lang pl ON p.id_product = pl.id_product JOIN manufacturer m ON p.id_manufacturer = m.id_manufacturer LEFT JOIN supplier s ON p.id_supplier = s.id_supplier WHERE pl.id_lang = 5 AND ( p.scorta_minima > 0 OR (pl.id_lang = 5 AND ((p.scorta_minima > 0 AND ord.ordinati > 0) OR (p.scorta_minima > 0 AND ord.ordinati = 0) OR (p.scorta_minima = 0 AND ord.ordinati > 0) OR (p.scorta_minima > 0 AND ord.ordinati IS NULL) ) )) GROUP BY p.id_product ORDER BY costruttore asc, reference asc";

					/* $queryorders = 'SELECT p.id_product, p.reference, p.supplier_reference, p.stock_quantity as magazzino, p.scorta_minima, pl.name AS nome_prodotto, p.ordinato_quantity as qt_ordinato, p.supplier_quantity as allnet, p.esprinet_quantity as esprinet, p.itancia_quantity as itancia, p.quantity as totale, pl.name, ord.ordinati, ord.ordinati - p.stock_quantity - p.ordinato_quantity AS da_ordinare, p.wholesale_price AS acquisto, m.name AS costruttore, s.name AS fornitore FROM product p LEFT JOIN (SELECT product_id, sum( product_quantity - (REPLACE(cons,"0_","")) ) as ordinati FROM order_detail od JOIN orders o ON od.id_order = o.id_order JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16 AND os.id_order_state !=20 AND os.id_order_state !=26 GROUP BY product_id) ord ON p.id_product = ord.product_id JOIN product_lang pl ON p.id_product = pl.id_product JOIN manufacturer m ON p.id_manufacturer = m.id_manufacturer LEFT JOIN supplier s ON p.id_supplier = s.id_supplier WHERE p.fuori_produzione = 0 AND pl.id_lang = 5 AND ( (pl.id_lang = 5 AND (p.scorta_minima > 0) )) GROUP BY p.id_product ORDER BY costruttore ASC, p.reference ASC'; */
		
					$results = Db::getInstance()->executeS($queryorders);
						
					$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'NOTE: il numero di prodotti da ordinare è nella cella F. Il numero si ottiene con questa formula: impegnato (ovvero prodotti ordinati dal cliente) - magazzino EZ + scorta minima. La cella F è verde se siamo sotto scorta ma i prodotti stanno arrivando dal fornitore.');
					
					$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A2', 'ATTENZIONE: l\'impegnato calcola anche prodotti in ordini non ancora pagati dal cliente (es. con bonifico). Il prezzo d\'acquisto è in blu se c\'è un prezzo d\'acquisto speciale.');
					
					$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A4', 'Codice Spring')
					->setCellValue('B4', 'Codice SKU')
					->setCellValue('C4', 'Nome prodotto')
					->setCellValue('D4', 'Costruttore')
					->setCellValue('E4', 'Fornitore')
					->setCellValue('F4', 'Qt. da ordinare')
					->setCellValue('G4', 'Scorta minima')
					->setCellValue('H4', 'Mag. EZ')
					->setCellValue('I4', 'Impegnato')
					->setCellValue('J4', 'Ordinato A.F.')
					->setCellValue('K4', 'Prezzo acquisto')
					;
						
					$objPHPExcel->getActiveSheet()->getStyle("A")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
					$i = 5;
					foreach ($results as $row) {
						
						$parziali = Db::getInstance()->executeS('SELECT o.id_order, product_quantity FROM order_detail od JOIN orders o ON o.id_order = od.id_order JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE  moh.id_order_state != 18 AND moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE os.id_order_state = 15 AND od.product_id = '.$row['id_product'].'');
				
						foreach($parziali as $parziale)
						{
							/*$array_parziali1 = Db::getInstance()->executeS('SELECT cod_articolo FROM fattura WHERE rif_vs_ordine = '.$parziale['id_order']);
							$array_parziali = array();
							foreach($array_parziali1 as $ar)
								$array_parziali[] = trim($ar['cod_articolo']);
								*/
							if(in_array($row['reference'], $array_parziali))
							{
// 								$row['ordinati'] -= Db::getInstance()->getValue('SELECT SUM(qt_ord) FROM fattura WHERE cod_articolo = "'.trim($row['reference']).'" AND rif_vs_ordine = '.$parziale['id_order'].' GROUP BY cod_articolo');

							}
						}	
				
						$prezzo_acquisto = $row['acquisto'];
						
						//if($row['scorta_minima'] > 0 && (($row['magazzino']-$row['ordinati']+$row['qt_ordinato']) < $row['scorta_minima']))
						$row['da_ordinare'] = $row['scorta_minima'] - ($row['magazzino'] - ($row['ordinati']) + $row['qt_ordinato']);
							
						if($row['da_ordinare'] > 0)
						{	
							$acquisto_speciale = Db::getInstance()->getValue("SELECT wholesale_price FROM specific_price_wholesale spw WHERE id_product = ".$row['id_product']." AND spw.to != '0000-00-00 00:00:00' AND spw.to > '".date("Y-m-d H:i:s")."' ");
							
							if($acquisto_speciale > 0)
							{
								$prezzo_acquisto = $acquisto_speciale;
								$objPHPExcel->getActiveSheet()->getStyle('K'.$i)->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_BLUE ) );
							}
							
						
							$sc_att = $row['magazzino']-$row['ordinati']+$row['qt_ordinato'];
							
							
							if($row['scorta_minima'] > 0)
							{	
								if($sc_att < $row['scorta_minima'])
								{
									$objPHPExcel->getActiveSheet()->getStyle("F$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
									$objPHPExcel->getActiveSheet()->getStyle("F$i")->getFill()->getStartColor()->setRGB('ffb4b4');	
									
								}
								else
								{
									if($row['qt_ordinato'] > 0 && $sc_att > $row['scorta_minima'])
									{
										$objPHPExcel->getActiveSheet()->getStyle("F$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
										$objPHPExcel->getActiveSheet()->getStyle("F$i")->getFill()->getStartColor()->setRGB('daffc8');	
									}
								}
							}
					
						
							$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue("A$i", $row['reference'])
							->setCellValue("B$i", $row['supplier_reference'])
							->setCellValue("C$i", $row['nome_prodotto'])
							->setCellValue("D$i", $row['costruttore'])
							->setCellValue("E$i", $row['fornitore'])
							->setCellValue("F$i", ($row['da_ordinare'] > 0 ? $row['da_ordinare'] : 0))
							->setCellValue("G$i", $row['scorta_minima'])
							->setCellValue("H$i", $row['magazzino'])
							->setCellValue("I$i", $row['ordinati'])
							->setCellValue("J$i", $row['qt_ordinato'])
							->setCellValue("K$i", $prezzo_acquisto)
							;
							$i++;
							
						}
						

					}
					

					$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
					$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
					$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
					$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
					$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
					$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
					$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
					$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
					$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
					
					$objPHPExcel->getActiveSheet()->setTitle('Prodotti da ordinare');

					$objPHPExcel->getActiveSheet()->getStyle('A4:K4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	

					$objPHPExcel->getActiveSheet()->getStyle('A4:K4')->getFill()->getStartColor()->setRGB('FFFF00');	

					$objPHPExcel->getActiveSheet()->getStyle("A4:K$i")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					
					$objPHPExcel->getActiveSheet()->getStyle('A4:K4')->getFont()->setBold(true);
					
					$objPHPExcel->getActiveSheet()->getStyle("A5:A$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					
					$objPHPExcel->getActiveSheet()->getStyle("F5:F$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);	
					$objPHPExcel->getActiveSheet()->getStyle("G5:G$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
					
					$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
					PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

					$data = date("Ymd");

					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
					$objWriter->setPreCalculateFormulas(false);
					$objWriter->save(str_replace(".php", ".xls", "esportazione-catalogo/catalogo_xls/ordini-fornitore-$data.php"));

					$this->_html .= "<div style='visibility:hidden'><br /><br />FILE PRONTO PER IL DOWNLOAD: <a id='scaricaexcelstatistiche' href='http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/ordini-fornitore-$data.xls' onclick='window.onbeforeunload = null'>CLICCA QUI PER SCARICARE</a>!</div>";

					header("Location: http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/ordini-fornitore-$data.xls");
					
					
				}	
			}
		}
		
		
}

