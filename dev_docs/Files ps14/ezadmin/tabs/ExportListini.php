<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');
ini_set('display_errors', 'off');

class ExportListini extends AdminPreferences
{


	public function display()
	{
		if(isset($_POST['vai']) && $_POST['vai'] == 'Esporta listini XLS') {
			
			echo "OK";
			
		
			include("esportazione-catalogo/esportazione-listini.php");
			
			echo "<br /><br /><a href='index.php?tab=ExportListini&token=".$this->token."'>Torna indietro</a>";
		

		 
		 } 
		 
		 else {


		echo "




		 <br />
		 <script type='text/javascript'>

		function message (t1, t2) {
			
			strong = document.createElement ('STRONG')
			strong.appendChild (document.createTextNode (t1))
			br = document.createElement ('BR')
			strong.appendChild (document.createTextNode (t2))
			document.getElementsByTagName ('BODY')[0].appendChild (strong)
		}

		</script>
				
			
		<div style='border:1px solid black; float:left; margin-left:15px; width:600px; padding:5px'>
		<strong>Esporta listini</strong><br /><br />
		    Scegli i carrelli
			<table>
			<form name='esportazione-listini' method='post'>
				<tr>
			<td><div style='height:600px; overflow:auto'>"; echo '
				
				
				';
				
				
				$resultsman = Db::getInstance()->ExecuteS("SELECT * FROM cart WHERE id_customer = 44431 AND name != '' ORDER BY name");
				
				/*echo "<input type='checkbox' name='per-costruttore[]' value='tutti' /"; 
					echo "> <strong><u>Seleziona tutti</u></strong> <br /> <br />";*/
				foreach ($resultsman as $rowman) {
				
					echo "<input type='checkbox' name='carrello[]' value='$rowman[id_cart]' /"; 
					echo "> $rowman[name] <br /> ";
				
				}
					
					
				echo "</div>
			</td>
			</tr>
			
			
			<tr>
			<td><br /><input type='checkbox' name='rivenditori' /> Spunta per esportare il listino per rivenditori 1<br />
			<br /><input type='checkbox' name='rivenditori2' /> Spunta per esportare il listino per rivenditori 2<br />
			<br />
			
			</td><td></td>
			</tr>
			
					<tr>
			<td><br /><input type='submit' name='vai' value='Esporta listini XLS' /></td><td></td>
			</form>
			
			</tr>
			</table>
			</div>	
			
			<div style='clear:both'></div>
			
			
			<br /><br /><br /><br />";
			
			
			
			//echo "WORK IN PROGRESS in arrivo i file Excel 2.0... ";
		}
			
	}
	

}


