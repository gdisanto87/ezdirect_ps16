<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license	http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AnagraficheDuplicate extends AdminTab
{
	public function display()
	{
		global $cookie;
		
		
		$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
		
		echo '
		<script type="text/javascript" src="yetii.js"></script>
		<div id="tab-container-cart" style="background-color:#fffff !important">
		<ul id="tab-container-cart-nav" class="tab-containers-nav">
		<li><a href="#per-pi"><strong>Partite IVA doppie</strong></a></li>
		<li><a href="#per-cf"><strong>Codici fiscali doppi</strong></a></li>
		<li><a href="#unisci-anagrafica"><strong>Deduplica anagrafica</strong></a></li>
		</ul>
		';

		echo '
		<div class="yetii-cart" id="per-pi"><h2>Partite IVA doppie</h2>';
		
		$doppi = Db::getInstance()->executeS("SELECT vat_number, COUNT(vat_number) c FROM customer WHERE is_company = 1 AND vat_number != '' AND vat_number != '00000000000' AND vat_number != '1' AND vat_number != '-' GROUP BY vat_number HAVING c > 1");

		foreach($doppi as $doppio) {


			$clienti_doppi = Db::getInstance()->executeS("SELECT DISTINCT c.id_customer, c.firstname, c.email, c.lastname, c.company, c.vat_number, tax_code FROM customer c WHERE vat_number = '".$doppio['vat_number']."' ORDER BY id_customer DESC");
			$i = 0;
			$connessione_first = "";
			$connessione = "";
			$numero_risultati = count($clienti_doppi);

			echo '<table class="table" style="width:100%"><tr><th colspan="5">Partita IVA: '.$doppio['vat_number'].' </th><th>Ricorrenze: '.$numero_risultati.'</th></tr><tr><th>ID CRM</th><th>P.Iva</th><th>Email</th><th>Azienda</th><th>Nome</th><th>Cognome</th></tr>';
			foreach($clienti_doppi as $doppio2) {
			
				
				echo '<tr><td><a href="index.php?tab=AdminCustomers&id_customer='.$doppio2['id_customer'].'&viewcustomer&tab-container-1=1&token='.$tokenCustomers.'">'.$doppio2['id_customer']."</a></td><td>".$doppio2['vat_number']."</td><td>".$doppio2['email']."</td><td>".$doppio2['company'].'</td><td>'.$doppio2['firstname']."</td><td>".$doppio2['lastname']."</td></tr>";
				
				
			}	
			echo "</table><br /><br />";
	

		}

		echo '</div>
		<div class="yetii-cart" id="per-cf"><h2>Codici fiscali doppi</h2>';
		
		$doppi_cf = Db::getInstance()->executeS("SELECT tax_code, COUNT(tax_code) c FROM customer WHERE is_company = 0 AND tax_code != '' AND tax_code != '0' AND tax_code != '0000000000000000' AND tax_code != '1' AND tax_code != '-' GROUP BY tax_code HAVING c > 1");

		foreach($doppi_cf as $doppio_cf) {


			$clienti_doppi_cf = Db::getInstance()->executeS("SELECT DISTINCT c.id_customer, c.firstname, c.email, c.lastname, c.company, c.tax_code, tax_code FROM customer c WHERE tax_code = '".$doppio_cf['tax_code']."' ORDER BY id_customer DESC");
			$i_cf = 0;
			$connessione_first_cf = "";
			$connessione_cf = "";
			$numero_risultati_cf = count($clienti_doppi_cf);

			echo '<table class="table" style="width:100%"><tr><th colspan="4">Codice fiscale: '.$doppio_cf['tax_code'].' </th><th>Ricorrenze: '.$numero_risultati_cf.'</th></tr><tr><th>ID CRM</th><th>Codice fiscale</th><th>Email</th><th>Nome</th><th>Cognome</th></tr>';
			
			foreach($clienti_doppi_cf as $doppio2_cf) {
				
				echo '<tr><td><a href="index.php?tab=AdminCustomers&id_customer='.$doppio2_cf['id_customer'].'&viewcustomer&tab-container-1=1&token='.$tokenCustomers.'">'.$doppio2_cf['id_customer']."</a></td><td>".$doppio2_cf['tax_code']."</td><td>".$doppio2_cf['email']."</td><td>".$doppio2_cf['firstname']."</td><td>".$doppio2_cf['lastname']."</td></tr>";
				
				
			}	
			echo "</table><br /><br />";

		}
		
		echo '</div>';
		echo '<div class="yetii-cart" id="unisci-anagrafica"><h2>Deduplica anagrafica</h2>
		
		<p>Con questo form puoi deduplicare un\'anagrafica. Tutti i dati (ordini, carrelli, fatture, indirizzi, todo, ticket, azioni cliente, contratti, bdl, persone) saranno spostati dall\'anagrafica di <strong>partenza</strong> verso l\'anagrafica di <strong>arrivo</strong>. Spuntando il flag &egrave; possibile anche cancellare l\'anagrafica di partenza. Nelle caselle &egrave; necessario inserire l\'ID CRM delle anagrafiche. Il programma, una volta dato avvio, chieder&agrave; la conferma. <strong>ATTENZIONE</strong>: l\'operazione &egrave; irreversibile.</p>';
	
		if(Tools::getIsset('deduplica_anag_conf') && is_numeric(Tools::getValue('partenza')) && is_numeric(Tools::getValue('arrivo')))
		{
			Db::getInstance()->executeS('update orders set id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza').'');
			Db::getInstance()->executeS('update cart set id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza').'  ');
			Db::getInstance()->executeS('update carrelli_creati set id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza').'  ');
			Db::getInstance()->executeS('update fattura set id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza').'  ');
			Db::getInstance()->executeS('update address set id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza').'  and active = 1 and fatturazione = 0 and deleted = 0');
			Db::getInstance()->executeS('update action_thread set id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza').'  ');
			Db::getInstance()->executeS('update customer_thread set id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza').'  ');
			Db::getInstance()->executeS('update form_prevendita_thread set id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza').'  ');
			Db::getInstance()->executeS('update contratto_assistenza set id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza').'  ');
			Db::getInstance()->executeS('update bdl set id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza').'  ');
			Db::getInstance()->executeS('update guest set id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza').'  ');
			Db::getInstance()->executeS('update persone set id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza').'  ');
			
			$stringa_1 = 'Dati ID '.Tools::getValue('partenza').' (ordini, carrelli, fatture, indirizzi, todo, ticket, azioni cliente, contratti, bdl, persone) spostati su ID '.Tools::getValue('arrivo').'';
			
			if(Tools::getIsset('cancella_partenza'))
			{
				$customer = new Customer(Tools::getValue('partenza'));
				$customer->delete();
				$stringa_2 = 'Anagrafica ID '.Tools::getValue('partenza').' cancellata';
			}
			else
				$stringa_2 = '';
			
			echo '
			<div class="conf">
				<img src="../img/admin/ok2.png" alt=""> Operazione eseguita con successo<br />
				'.$stringa_1.'<br />
				'.$stringa_2.'
			</div>';
			
		}
		else if(Tools::getIsset('deduplica_anag_conf') && (!is_numeric(Tools::getValue('partenza')) || !is_numeric(Tools::getValue('arrivo'))))
		{
			echo '<div class="error">
						<img src="../img/admin/error2.png">
						Errore: i campi ID devono essere numerici
					</div>';
		}

		echo '
		<form method="post" name="form_deduplica" id="form_deduplica">
		<table>
		<tr><td>ID CRM anagrafica di <strong>partenza</strong></td><td><input type="text" size="20" name="partenza" onkeyup="searchCustomerById(this.value, \'an_partenza\')" /></td><td><strong><span id="an_partenza"></span></strong></td></tr>
		<tr><td>ID CRM anagrafica di <strong>arrivo</strong></td><td><input type="text" size="20" name="arrivo" onkeyup="searchCustomerById(this.value, \'an_arrivo\')" /></td><td><strong><span id="an_arrivo"></span></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NB: tutti i dati saranno spostati su questa anagrafica</td></tr>
		<tr><td>Cancellare anagrafica di partenza?</td><td style="text-align:center"><input type="checkbox" name="cancella_partenza" /></td><td>NB: l\'operazione &egrave; irreversibile</td></tr>
		<tr><td>
		<input type="hidden" id="deduplica_anag_conf" name="deduplica_anag_conf"  />
		<input type="button" id="deduplica_anag" value="Avvio" style="width:200px" class="button" name="deduplica_anag" 
		/></td><td></td><td></td></tr>
		</table>
		<script type="text/javascript">
			function searchCustomerById(id, type)
			{
				$.ajax({
				  url:"ajax.php?searchCustomerById=y",
				  type: "POST",
				  data: { id_customer: id
				  },
				  success:function(r){
						$("#"+type).html(r);
				  },
				  error: function(xhr,stato,errori){
					 alert("Errore:"+xhr.status);
				  }
				});
				
				
			}
			</script>
		
		</script>
		</form>
		<script type="text/javascript" src="https://www.ezdirect.it/js/sweetalert2.js"></script>
		<link rel="stylesheet" href="https://www.ezdirect.it/css/sweetalert2.css" />
		<script type="text/javascript">
		
		$(document).ready(function(){
			 $("#deduplica_anag").on("click",function(e){
				var valid = false;
				swal({
				  title: \'Sei sicuro/a?\',
				  text: \'Questa operazione è irreversibile\',
				  type: \'warning\',
				  showCancelButton: true,
				  confirmButtonColor: \'#6A9944\',
				  cancelButtonColor: \'#d33\',
				  confirmButtonText: \'S&igrave;, voglio continuare!\'
				}).then(function () {
					$("#form_deduplica").submit();
					console.log("OK");
				});
				
			});
		});
		</script>
		</div>';
		
		echo '
		<script type="text/javascript">
		var tabber2 = new Yetii({
		id: "tab-container-cart",
		tabclass: "yetii-cart",
		persist: true
		});
		</script>';
	}
}

