<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14703 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
include_once(PS_ADMIN_DIR.'/tabs/AdminProfiles.php');

class AdminBundles extends AdminTab
{
	

	
	public function display()
	{
		global $cookie, $currentIndex;
		
		$this->_languages = Db::getInstance()->executeS('SELECT * FROM lang');
		$this->_defaultFormLanguage = 5;
	
		if (Tools::isSubmit('submitnewbundle')) {
			
			$bundle_products = "";
			$bundle_prices = "";
			
			foreach ($_POST['nuovo_prodotto'] as $id_product_g=>$id_product) {
			
				$bundle_products.= trim($_POST['nuovo_prodotto'][$id_product_g]).";";
				$bundle_prices.= trim($_POST['nuovo_prodotto'][$id_product_g]).";".$_POST['nuovo_quantity'][trim($id_product_g)].";".$_POST['nuovo_sconto'][trim($id_product_g)].";".$_POST['nuovo_scontoriv'][trim($id_product_g)].":";
			
			}
			
			$amazon_array = array();
			if(isset($_POST['bundle_amazon_it'])) 
				$amazon_array[] = 5;
			if(isset($_POST['bundle_amazon_fr'])) 
				$amazon_array[] = 2;	
			if(isset($_POST['bundle_amazon_es'])) 
				$amazon_array[] = 3;	
			if(isset($_POST['bundle_amazon_uk'])) 
				$amazon_array[] = 1;	
			if(isset($_POST['bundle_amazon_de'])) 
				$amazon_array[] = 4;
			if(isset($_POST['bundle_amazon_nl'])) 
				$amazon_array[] = 6;

			$amazon_string = implode(";",$amazon_array);
		
		
			Db::getInstance()->executeS("INSERT INTO bundle (id_bundle, bundle_name, bundle_ref, ean13, asin, father, id_category_default, bundle_products, bundle_prices, amazon, estensione_garanzia, supporto_da_remoto, telegestione, active, date_add, date_upd) VALUES ('NULL', '".$_POST['bundle_name_5']."', '".$_POST['bundle_ref']."', '".$_POST['bundle_ean13']."', '".$_POST['bundle_asin']."', '".$_GET['id_product']."', '".$_POST['id_category_default']."', '".$bundle_products."', '".$bundle_prices."', '".$amazon_string."' ".$_POST['estensione_garanzia'].", ".$_POST['supporto_da_remoto'].", ".$_POST['telegestione'].", ".$_POST['active'].", '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
			
			
			$new_bundle_id = mysql_insert_id();
			
			foreach($this->_languages as $language)
			{
				Db::getInstance()->executeS('REPLACE INTO bundle_lang (id_bundle, id_lang, name, description_amazon) VALUES ('.$new_bundle_id.', '.$language['id_lang'].', "'.$_POST['bundle_name_'.$language['id_lang']].'", "'.$_POST['bundle_description_amazon_'.$language['id_lang']].'")');
				
			}
			
			foreach($_POST['categoryBox'] as $catbox) 
			{
				if(isset($catbox) && $catbox != $_POST['id_category_default'])
					Db::getInstance()->executeS("INSERT INTO category_bundle (id_category, id_bundle) VALUES (".$catbox.", ".$new_bundle_id.")");
			}
			
			Bundle::salvaCategoria($new_bundle_id);
			
					if(isset($_POST['trovaprezzi_add'])) {
			Db::getInstance()->executeS("UPDATE bundle SET trovaprezzi = 1 WHERE id_bundle = $new_bundle_id");
			}
				else {
			Db::getInstance()->executeS("UPDATE bundle SET trovaprezzi = 0 WHERE id_bundle = $new_bundle_id");
			}
			
					if(isset($_POST['google_shopping_add'])) {
			Db::getInstance()->executeS("UPDATE bundle SET google_shopping = 1 WHERE id_bundle = $new_bundle_id");
			}
				else {
			Db::getInstance()->executeS("UPDATE bundle SET google_shopping = 0 WHERE id_bundle = $new_bundle_id");
			}
			
					if(isset($_POST['tutti_add'])) {
			Db::getInstance()->executeS("UPDATE bundle SET comparaprezzi_check = 1 WHERE id_bundle = $new_bundle_id");
			}
				else {
			Db::getInstance()->executeS("UPDATE bundle SET comparaprezzi_check = 0 WHERE id_bundle = $new_bundle_id");
			}
			
			if(isset($_POST['trovaprezzi_add']) && isset($_POST['google_shopping_add'])) {
			Db::getInstance()->executeS("UPDATE bundle SET comparaprezzi_check = 1 WHERE id_bundle = $new_bundle_id");
			}
			else {
			Db::getInstance()->executeS("UPDATE bundle SET comparaprezzi_check = 0 WHERE id_bundle = $new_bundle_id");
			}
			$this->addBundleImage($new_bundle_id, $_POST['id_image_bundle']);
			
			Tools::redirectAdmin($currentIndex.'&id_product='.$_GET['id_product'].'&updateproduct&conf=4&tabs=2&token='.($token ? $token : $this->token));
						
		}
		
		elseif (isset($_GET['deletebundle'])) {
			
			Db::getInstance()->executeS("DELETE FROM bundle WHERE id_bundle = $_GET[id_bundle]");
			
			Tools::redirectAdmin($currentIndex.'&id_product='.$_GET['id_product'].'&updateproduct&conf=4&tabs=2&token='.($token ? $token : $this->token));
						
		}
		
		elseif (Tools::isSubmit('submitupdatedbundle')) {
			
			$bundle_products = "";
			$bundle_prices = "";
			
			foreach ($_POST['nuovo_prodotto'] as $id_product_g=>$id_product) {
			
				$bundle_products.= trim($_POST['nuovo_prodotto'][$id_product_g]).";";
				$bundle_prices.= trim($_POST['nuovo_prodotto'][$id_product_g]).";".$_POST['nuovo_quantity'][trim($id_product_g)].";".$_POST['nuovo_sconto'][trim($id_product_g)].";".$_POST['nuovo_scontoriv'][trim($id_product_g)].":";
			
			}
			
			$amazon_array = array();
			if(isset($_POST['bundle_amazon_it'])) 
				$amazon_array[] = 5;
			if(isset($_POST['bundle_amazon_fr'])) 
				$amazon_array[] = 2;	
			if(isset($_POST['bundle_amazon_es'])) 
				$amazon_array[] = 3;	
			if(isset($_POST['bundle_amazon_uk'])) 
				$amazon_array[] = 1;	
			if(isset($_POST['bundle_amazon_de'])) 
				$amazon_array[] = 4;
			if(isset($_POST['bundle_amazon_nl'])) 
				$amazon_array[] = 6;
			
			$amazon_string = implode(";",$amazon_array);
			
			Db::getInstance()->executeS("UPDATE bundle SET date_upd = '".date("Y-m-d H:i:s")."', bundle_name = '".$_POST['bundle_name_5']."',  bundle_ref = '".$_POST['bundle_ref']."', ean13 = '".$_POST['bundle_ean13']."', amazon = '".$amazon_string."', asin = '".$_POST['bundle_asin']."', id_category_default = '".$_POST['id_category_default']."', bundle_products = '".$bundle_products."', bundle_prices = '".$bundle_prices."', estensione_garanzia = '".$_POST['estensione_garanzia']."', supporto_da_remoto = '".$_POST['supporto_da_remoto']."', telegestione = '".$_POST['telegestione']."' WHERE id_bundle = '".$_POST['id_bundle']."'");
			
			foreach($this->_languages as $language)
			{
				Db::getInstance()->executeS('REPLACE INTO bundle_lang (id_bundle, id_lang, name, description_amazon) VALUES ('.$_POST['id_bundle'].', '.$language['id_lang'].', "'.$_POST['bundle_name_'.$language['id_lang']].'", "'.$_POST['bundle_description_amazon_'.$language['id_lang']].'")');
				
			}
			
			Db::getInstance()->executeS("DELETE FROM category_bundle WHERE id_bundle = ".$_POST['id_bundle']."");
			
			foreach($_POST['categoryBox'] as $catbox) 
			{
				if(isset($catbox) && $_POST['id_category_default'])
					Db::getInstance()->executeS("INSERT INTO category_bundle (id_category, id_bundle) VALUES (".$catbox.", ".$_POST['id_bundle'].")");
			}
			
			Bundle::salvaCategoria($_POST['id_bundle']);
			
			if($_POST['active'] == 1) {
				
				Db::getInstance()->executeS("UPDATE bundle SET active = ".$_POST['active']." WHERE id_bundle = '".$_POST['id_bundle']."'");
				
				$child_bundles = Db::getInstance()->executeS("SELECT * FROM bundle WHERE id_bundle = '".$_POST['id_bundle']."'");
				foreach($child_bundles as $child) {
								
					$products_bundle = explode(";",$child['bundle_products']);
								
					foreach ($products_bundle as $product_bundle) {	
						if($product_bundle == "") {
						}
						else {
							
							$child_p = new Product($product_bundle);
							if($child_p->price == 0) {
									
								Db::getInstance()->executeS("UPDATE bundle SET active = 0 WHERE id_bundle = ".$child['id_bundle']);
							}
							else {
							}
						}
					}
					
				}
			
			}
			
			else {
			
				Db::getInstance()->executeS("UPDATE bundle SET active = ".$_POST['active']." WHERE id_bundle = '".$_POST['id_bundle']."'");
			
			}
			
			
					if(isset($_POST['trovaprezzi_add'])) {
			Db::getInstance()->executeS("UPDATE bundle SET trovaprezzi = 1 WHERE id_bundle = ".$_POST['id_bundle']."");
			}
				else {
			Db::getInstance()->executeS("UPDATE bundle SET trovaprezzi = 0 WHERE id_bundle = ".$_POST['id_bundle']."");
			}
			
			
					if(isset($_POST['google_shopping_add'])) {
			Db::getInstance()->executeS("UPDATE bundle SET google_shopping = 1 WHERE id_bundle = ".$_POST['id_bundle']."");
			}
				else {
			Db::getInstance()->executeS("UPDATE bundle SET google_shopping = 0 WHERE id_bundle = ".$_POST['id_bundle']."");
			}
			
					if(isset($_POST['tutti_add'])) {
			Db::getInstance()->executeS("UPDATE bundle SET comparaprezzi_check = 1 WHERE id_bundle = ".$_POST['id_bundle']."");
			}
				else {
			Db::getInstance()->executeS("UPDATE bundle SET comparaprezzi_check = 0 WHERE id_bundle = ".$_POST['id_bundle']."");
			}
			
			if(isset($_POST['trovaprezzi_add']) && isset($_POST['google_shopping_add'])) {
			Db::getInstance()->executeS("UPDATE bundle SET comparaprezzi_check = 1 WHERE id_bundle = ".$_POST['id_bundle']."");
			}
			else {
			Db::getInstance()->executeS("UPDATE bundle SET comparaprezzi_check = 0 WHERE id_bundle = ".$_POST['id_bundle']."");
			}
			$this->addBundleImage($_POST['id_bundle'], $_POST['id_image_bundle']);
			
			Tools::redirectAdmin($currentIndex.'&id_product='.$_GET['id_product'].'&updateproduct&conf=4&tabs=2&token='.($token ? $token : $this->token));
						
		}
		

		else {
	

			echo '
			<div class="tab-page" id="step_bundles" style="padding:7px"><form method="post" enctype="multipart/form-data" action="">';
				
			
			
			if(isset($_GET['newbundle'])) {
				
				global $cookie;
		$iso = Language::getIsoById((int)($cookie->id_lang));
		$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
		$ad = dirname($_SERVER["PHP_SELF"]);
		echo '
			<script type="text/javascript">
			var iso = \''.$isoTinyMCE.'\' ;
			var pathCSS = \''._THEME_CSS_DIR_.'\' ;
			var ad = \''.$ad.'\' ;
			</script>
			
			
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce3/tiny_mce.js"></script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>
			';
			
			/*
			<script src="../js/tinymce4.4.3/tinymce.min.js"></script>
             <script>tinymce.init({ selector:"textarea.editable", 
			  language_url : "../js/tiny_mce/auz/it.js",
			 
			 browser_spellcheck: true,
			 language: "it",
			 color_picker_callback: function(callback, value) {
    callback("#FF00FF");
  },
  insert_toolbar: "quickimage quicktable",
   min_width: 700,
   plugins: "link image code responsivefilemanager ",
    external_filemanager_path:"respfilemanager/",
   filemanager_title:"Responsive Filemanager" ,
			 menubar: "file edit view format table tools",
			 external_plugins: { "filemanager" : "plugins/responsivefilemanager/plugin.min.js"},
        content_css : "tiny-mce-editor.css",
			 resize: "both",
			 toolbar: "undo redo code | styleselect | bold italic | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | bullist numlist | link responsivefilemanager"
			 });</script>*/
			 
			 echo '
			<script type="text/javascript" src="../js/JavaScriptSpellCheck/include.js" ></script>

<script type="text/javascript">$Spelling.UserInterfaceTranslation = "it"; $Spelling.DefaultDictionary = "Italiano";</script>

';
			
			
				if(isset($_GET['updatebundle'])) {
					
					$bundle = Db::getInstance()->getRow("SELECT * FROM bundle WHERE id_bundle = ".$_GET['id_bundle']."");
					echo "<input type='hidden' name='id_bundle' value='".$bundle['id_bundle']."' size='60' />";	
				}
				echo "<h3>Crea nuovo kit</h3>";
				
				echo "
				Codice kit: <input type='text' name='bundle_ref' value='".(isset($_GET['updatebundle']) ? $bundle['bundle_ref'] : '' )."' size='30' /><br /><br />";
				echo "EAN kit: <input type='text' name='bundle_ean13' value='".(isset($_GET['updatebundle']) ? $bundle['ean13'] : '' )."' size='30' /><br /><br />";
				echo "ASIN kit: <input type='text' name='bundle_asin' value='".(isset($_GET['updatebundle']) ? $bundle['ean13'] : '' )."' size='30' /><br /><br />";
				echo '
				<strong>Status</strong>: <input type="radio" name="active" id="active_on" value="1" '.($bundle['active'] == 1 ? 'checked="checked" ' : (!$bundle ? 'checked="checked"' : '')).'/>
					<label class="t" for="active_on"><img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="active" id="active_off" value="0" '.($bundle['active'] == 0 ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_off"><img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label><br /><br />';
					
				echo '<div class="translatable">';
				foreach ($this->_languages as $language)
				{
					$bundle_name = Db::getInstance()->getValue('SELECT name FROM bundle_lang WHERE id_bundle = '.$_GET['id_bundle'].' AND id_lang = '.$language['id_lang']);
					echo '<div name="bundle_name_div_'.$language['id_lang'].'" id="bundle_name_div_'.$language['id_lang'].'" class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'block').'; float: left;">';
					echo "
					Nome kit: <input type='text' name='bundle_name_".$language['id_lang']."' value='".(isset($_GET['updatebundle']) ? ($bundle_name != '' ? $bundle_name : $bundle['bundle_name']) : '' )."' size='60' /> <img src='../img/l/".$language['id_lang'].".jpg' /><br />";
					echo '</div><br /><br />';
				}
				
				// echo '<div style="display:none">'.$this->displayFlags($this->_languages, $this->_defaultFormLanguage, 'bundle_name_div', 'bundle_name_div').'</div>';
				
				echo '</div>';
				
				echo '<div class="translatable">';
				foreach ($this->_languages as $language)
				{
					$bundle_description_amazon = Db::getInstance()->getValue('SELECT description_amazon FROM bundle_lang WHERE id_bundle = '.$_GET['id_bundle'].' AND id_lang = '.$language['id_lang']);
					echo '<div name="bundle_description_amazon_div_'.$language['id_lang'].'" id="bundle_description_amazon_div_'.$language['id_lang'].'" class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'block').'; float: left;">';
					echo "
					Descrizione kit per Amazon <img src='../img/l/".$language['id_lang'].".jpg' />: <textarea class='rte' name='bundle_description_amazon_".$language['id_lang']."'>".(isset($_GET['updatebundle']) ? ($bundle_description_amazon != '' ? $bundle_description_amazon : $bundle['bundle_name']) : '' )."</textarea><br />";
					echo '</div><br /><br />';
				}
				echo '<div style="clear:both"></div>';
				// echo '<div style="display:none">'.$this->displayFlags($this->_languages, $this->_defaultFormLanguage, 'bundle_name_div', 'bundle_name_div').'</div>';
				
				echo '</div>';
				
				echo '
					<br /><br />
				<script type="text/javascript" src="../js/jquery/jquery.tablednd_0_5.js"></script>
						<script type="text/javascript">
						$(document).ready(function() {
							$("#tableProducts").tableDnD();
						});
						</script>
						<script type="text/javascript">
						function calcolaImporto(id_product) {
							
							var totale = 0;
							var totaleriv = 0;
							var quantity = parseFloat(document.getElementById(\'product_quantity[\'+id_product+\']\').value);
							var numprice = document.getElementById(\'product_price[\'+id_product+\']\').value;
							
							var numpriceriv = document.getElementById(\'product_priceriv[\'+id_product+\']\').value;
							var numunitario = document.getElementById(\'unitario[\'+id_product+\']\').value;
							var numacquisto = document.getElementById(\'wholesale_price[\'+id_product+\']\').value;
							var unitario = parseFloat(numunitario.replace(/\s/g, "").replace(",", "."));
							var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
							
							var priceriv = parseFloat(numpriceriv.replace(/\s/g, "").replace(",", "."));
							var acquisto = parseFloat(numacquisto.replace(/\s/g, "").replace(",", "."));
							
							var numscontocli = document.getElementById(\'product_sconto[\'+id_product+\']\').value;
							var numscontoriv = document.getElementById(\'product_scontoriv[\'+id_product+\']\').value;
							
							var scontocli = parseFloat(numscontocli.replace(/\s/g, "").replace(",", "."));
							var scontoriv = parseFloat(numscontoriv.replace(/\s/g, "").replace(",", "."));
							
							var sc_qta_1 = parseFloat(document.getElementById(\'sc_qta_1[\'+id_product+\']\').value);
							var sc_qta_2 = parseFloat(document.getElementById(\'sc_qta_2[\'+id_product+\']\').value);
							var sc_qta_3 = parseFloat(document.getElementById(\'sc_qta_3[\'+id_product+\']\').value);
							var sc_riv_1 = parseFloat(document.getElementById(\'sc_riv_1[\'+id_product+\']\').value);
							var sc_riv_2 = parseFloat(document.getElementById(\'sc_riv_2[\'+id_product+\']\').value);
											
							var sc_qta_1_q = parseFloat(document.getElementById(\'sc_qta_1_q[\'+id_product+\']\').value);
							var sc_qta_2_q = parseFloat(document.getElementById(\'sc_qta_2_q[\'+id_product+\']\').value);
							var sc_qta_3_q = parseFloat(document.getElementById(\'sc_qta_3_q[\'+id_product+\']\').value);
							var sc_riv_1_q = parseFloat(document.getElementById(\'sc_riv_1_q[\'+id_product+\']\').value);
							var sc_riv_2_q = parseFloat(document.getElementById(\'sc_riv_2_q[\'+id_product+\']\').value);
					
						
								var prezzounitarionew = unitario;
								
								var prezziconfronto = new Array(sc_riv_1, sc_riv_2, sc_qta_1, sc_qta_2, sc_qta_3);
								for (var i = 0; i < prezziconfronto.length; ++i) {
									if(prezziconfronto[i] !=0 && prezziconfronto[i] < prezzounitarionew) { 
										prezzounitarionew = prezziconfronto[i];
									
									} else { }
								}
														
								
								
								var prezzofinitoriv = prezzounitarionew - ((prezzounitarionew*scontoriv)/100);
								var importoriv = parseFloat(prezzofinitoriv.toFixed(2)*quantity);
								document.getElementById(\'product_priceriv[\'+id_product+\']\').value=prezzofinitoriv;
								document.getElementById(\'prz_vis_riv[\'+id_product+\']\').innerHTML=prezzounitarionew.toFixed(2).replace(".",",");
								document.getElementById(\'prz_nel_bundle_riv[\'+id_product+\']\').innerHTML=prezzofinitoriv.toFixed(2).replace(".",",");
							
							if(quantity < sc_qta_1_q && sc_qta_1_q != 0) {
								
								var prezzofinitocli = unitario - ((unitario*scontocli)/100);
								document.getElementById(\'prz_vis_cli[\'+id_product+\']\').innerHTML=unitario.toFixed(2).replace(".",",");	
										
							}
							
							else if((quantity >= sc_qta_1_q && quantity < sc_qta_2_q) || (quantity >= sc_qta_1_q && isNaN(parseFloat(sc_qta_2_q)))) {
						
								var prezzofinitocli = sc_qta_1 - ((sc_qta_1*scontocli)/100);
								document.getElementById(\'prz_vis_cli[\'+id_product+\']\').innerHTML=sc_qta_1.toFixed(2).replace(".",",");	
							}
							
							else if((quantity >= sc_qta_2_q && quantity < sc_qta_3_q) || (quantity >= sc_qta_2_q && isNaN(parseFloat(sc_qta_3_q == 0)))) {
							
								var prezzofinitocli = sc_qta_2 - ((sc_qta_2*scontocli)/100);
								document.getElementById(\'prz_vis_cli[\'+id_product+\']\').innerHTML=sc_qta_2.toFixed(2).replace(".",",");	
								
							}
							
							else if(quantity >= sc_qta_3_q && sc_qta_2_q != 0 && sc_qta_3_q != 0 && sc_qta_1_q != 0) {

								var prezzofinitocli = sc_qta_3 - ((sc_qta_3*scontocli)/100);
								document.getElementById(\'prz_vis_cli[\'+id_product+\']\').innerHTML=sc_qta_3.toFixed(2).replace(".",",");
								
							} else {
								var prezzofinitocli = unitario - ((unitario*scontocli)/100);
								document.getElementById(\'prz_vis_cli[\'+id_product+\']\').innerHTML=unitario.toFixed(2).replace(".",",");	
							}
							
							document.getElementById(\'product_price[\'+id_product+\']\').value=prezzofinitocli;	
							var importo = parseFloat(prezzofinitocli.toFixed(2)*quantity);
							
							document.getElementById(\'prz_nel_bundle_cli[\'+id_product+\']\').innerHTML=prezzofinitocli.toFixed(2).replace(".",",");
							
							document.getElementById(\'impImporto[\'+id_product+\']\').value = importo;
							document.getElementById(\'impImportoRiv[\'+id_product+\']\').value = importoriv;
							
							var marginalita = (((prezzofinitocli - acquisto)*100) / prezzofinitocli);
							marginalita = marginalita.toFixed(2);
							
							var marginalitariv = (((prezzofinitoriv - acquisto)*100) / prezzofinitoriv);
							marginalitariv = marginalitariv.toFixed(2);
						
							document.getElementById(\'spanmarginalita[\'+id_product+\']\').innerHTML=marginalita.replace(".",",")+"%";
							document.getElementById(\'spanmarginalitariv[\'+id_product+\']\').innerHTML=marginalitariv.replace(".",",")+"%";
													
							
						
							var arrayImporti = document.getElementsByClassName("importo");
							for (var i = 0; i < arrayImporti.length; ++i) {
								var item = parseFloat(arrayImporti[i].value);
								totale += item;
							}
							
							var arrayImportiriv = document.getElementsByClassName("importoriv");
							for (var i = 0; i < arrayImportiriv.length; ++i) {
								var item = parseFloat(arrayImportiriv[i].value);  
								totaleriv += item;
							}

							var totaleacquisti = 0;
						
							var arrayAcquisti = document.getElementsByClassName("prezzoacquisto");
							for (var i = 0; i < arrayAcquisti.length; ++i) {
								arrayAcquisti[i].value = arrayAcquisti[i].value.replace(",",".");
								var item = parseFloat(arrayAcquisti[i].value); 					
								totaleacquisti += item;
							}
				
							var marginalitatotale = (((totale - totaleacquisti)*100) / totale);
							
							marginalitatotale = marginalitatotale.toFixed(2);
			
							document.getElementById(\'marginalitatotale\').innerHTML=marginalitatotale.replace(".",",") + "%";
							
							document.getElementById(\'totaleprodotti\').value = totale;
							totale = totale.toFixed(2);
							document.getElementById(\'totaleprodottiriv\').value = totaleriv;
							totaleriv = totaleriv.toFixed(2);
							
							document.getElementById(\'spantotaleprodottiriv\').innerHTML=totaleriv.replace(".",",");
							document.getElementById(\'spantotaleprodotti\').innerHTML=totale.replace(".",",");

							
									
							
							
					}
					
					function togliImporto(id_product) {
					
						var daTogliere = parseFloat(document.getElementById(\'product_price[\'+id_product+\']\').value.replace(",", "."));
						var daTogliereriv = parseFloat(document.getElementById(\'product_priceriv[\'+id_product+\']\').value.replace(",", "."));
						
						var totale = parseFloat(document.getElementById(\'totaleprodotti\').value);
						
						var totaleriv = parseFloat(document.getElementById(\'totaleprodottiriv\').value);
						
						totale = totale-daTogliere;
						
						totale = totale.toFixed(2);
						
						totaleriv = totaleriv-daTogliereriv;
						
						totaleriv = totaleriv.toFixed(2);
						
						document.getElementById(\'totaleprodotti\').value=totale;
							
						document.getElementById(\'spantotaleprodotti\').innerHTML=totale.replace(".",",");
						
						document.getElementById(\'totaleprodottiriv\').value=totaleriv;
							
						document.getElementById(\'spantotaleprodottiriv\').innerHTML=totaleriv.replace(".",",");
						
					}
					
					function addProduct_TR(event, data, formatted)
						{
							
							var productId = data[1];
							var productPrice = data[2];
							var productScQta = data[3];
							var productRef = data[4];
							var productName = data[5];
							var productQuantity = data[16];
							var tdImporto = data[17];
							var przUnitario = data[18];
							var deleteProd = data[19];
							var marg = data[20];
							var scontoExtra = data[21];
							var sc_qta_1 = data[6];
							var sc_qta_2 = data[7];
							var sc_qta_3 = data[8];
							var sc_riv_1 = data[9];
							var sc_riv_2 = data[10];
							var sc_qta_1_q = data[11];	
							var sc_qta_2_q = data[12];	
							var sc_qta_3_q = data[13];	
							var sc_riv_1_q = data[14];	
							var sc_riv_2_q = data[15];	
							var priceriv = data[22];
							var baseprice = data[23];
							var basepriceriv = data[24];
							var margriv = data[25];
							var varspec = data[26];
							var textVarSpec = "";
							
							if(varspec == 0) {
							}
							else {
								textVarSpec = "style=\'color:red\'";
							}
							
							$("<tr id=\'tr_"+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"\'><td> " + productId + " </td><td> " + productRef + " </td><td>"+ productName +"</td><td>" + productQuantity + " " + przUnitario + " " + sc_qta_1 +" " + sc_qta_2 +" " + sc_qta_3 +" " + sc_riv_1 +" " + sc_riv_2 +" " + sc_qta_1_q +" " + sc_qta_2_q +" " + sc_qta_3_q +" " + sc_riv_1_q +" " + sc_riv_2_q +"</td><td style=\'text-align:right\'><table style=\'width:100%\'><tr><td>Prz. base</td><td><span " + textVarSpec + " id=\'prz_vis_cli["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\'>" + baseprice + "</span><span " + textVarSpec + ">&euro;</span></td></tr><tr><td>Sconto %</td><td>" + productPrice + "<input type=\'text\' style=\'text-align:right\' size=\'4\' id=\'product_sconto["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' name=\'nuovo_sconto["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' size=\'4\' value=\'0,00\' onkeyup=\'javascript: calcolaImporto("+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+");\' /></td></tr><tr><td>Prz. nel kit</td><td><span id=\'prz_nel_bundle_cli["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\'>" + baseprice + "</span> &euro;</td></tr><td>Marg.</td><td>" + marg + "</td></tr></table></td><td style=\'text-align:right\'><table style=\'width:100%\'><tr><td>Prz. base</td><td><span id=\'prz_vis_riv["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\'>" + basepriceriv + "</span> &euro;</td></tr><tr><td>Sconto %</td><td>" + priceriv + "<input type=\'text\' style=\'text-align:right\' size=\'4\' id=\'product_scontoriv["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' name=\'nuovo_scontoriv["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' value=\'0,00\' onkeyup=\'javascript: calcolaImporto("+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+");\' /></td></tr><tr><td>Prz. nel kit</td><td><span id=\'prz_nel_bundle_riv["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\'>" + basepriceriv + "</span> &euro;</td></tr><td>Marg.</td><td>" + margriv + "</td></tr></table></td><td style=\'text-align:center\'>" + deleteProd + "<input name=\'nuovo_prodotto["+ productId +"]\' type=\'hidden\' value=\'" + productId + "\' /></td></tr>").appendTo("#tableProducts");
							
							productId = productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "");
					
							calcolaImporto(productId);
							
							$("#tableProducts").tableDnD();
							
							$("#span-reference-"+productId+"").tooltip({ showURL: false  });
							
				
						}
						
						function delProduct30(id)
						{
							document.getElementById(\'tr_\'+id).innerHTML = "";
							
							
						}
					</script>	';
				
				$this->maxImageSize = (Configuration::get('PS_LIMIT_UPLOAD_IMAGE_VALUE') * 1000000);
				
				if(isset($_GET['updatebundle'])) {
				
					$id_image_bundle = Db::getInstance()->getValue("SELECT id_image FROM bundle_image WHERE id_bundle = ".$bundle['id_bundle']."");
					$image_legend = Db::getInstance()->getValue("SELECT legend FROM bundle_image_lang WHERE id_lang = 5 AND id_image = ".$id_image_bundle."");
					echo "<input type='hidden' name='id_image_bundle' value='".$id_image_bundle."' />";
				
				}
				else { 
					echo "<input type='hidden' name='id_image_bundle' value='0' />";
				
				}
				
				echo '<hr style="width: 100%;" /><br />
				<table cellpadding="5" style="width:100%">
				<tr>
				<td>
					<table cellpadding="5" style="width:100%">
						<tr>
							<td class="col-left">'.$this->l('Immagine per bundle:').'</td>
							<td style="padding-bottom:5px;">
								<input type="file" id="image_bundle" name="image_bundle" />
								<p>
									'.$this->l('Formato:').' JPG, GIF, PNG. '.$this->l('Dimensioni:').' '.($this->maxImageSize / 1000).''.$this->l('Kb max.').'<br />
								
								</p>
							</td>
						</tr>
						<tr>
						<td>Descrizione immagine</td>
						<td><input type="text" name="image_legend" size="50" value="'.(isset($_GET['updatebundle']) ? htmlentities($image_legend) : '' ).'" /></td>
					</table>
				</td>
				<td>';
				if(isset($_GET['updatebundle']) && $id_image_bundle > 0) {
					echo '<table cellpadding="5" style="width:100%">
						<tr>
							<td class="col-left" style="vertical-align:middle">'.$this->l('Immagine attuale:').'<br /><span style="font-size:10px">Per sostituire l\'immagine basta caricarne una nuova</td>
							<td style="padding-bottom:5px;" style="vertical-align:middle">
								<img src="'._PS_IMG_.'b/'.$id_image_bundle."-".$bundle['id_bundle'].'-medium.jpg" alt="" title="" />
							</td>
						</tr>
						
					</table>';
				
				}
				else {
				}
				echo '</td>
				</tr>
				</table>
				<hr style="width: 100%;" /><br />			
				Seleziona il prodotto da aggiungere: <input size="123" type="text" value="" id="product_autocomplete_input_bundle" /><br /><br />
					<script type="text/javascript">
							var formProduct = "";
							var products = new Array();
						</script>
						<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
						<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete.js"></script>
						
						
						<script type="text/javascript">
							urlToCall = null;
							
							/* function autocomplete */
							$(function() {
								$(\'#product_autocomplete_input_bundle\')
									.autocomplete(\'ajax_products_list4.php?\', {
										minChars: 1,
										autoFill: true,
										max:20,
										matchContains: true,
										mustMatch:true,
										scroll:false,
										cacheLength:0,
										formatItem: function(item) {
														return item[1]+\' - \'+item[0];
													}	
										
									}).result(addProduct_TR);
						
							});
							
							$("#product_autocomplete_input_bundle").keypress(function(event){
							
										  var keycode = (event.keyCode ? event.keyCode : event.which);
										  if (keycode == "13") {
											event.preventDefault();
											event.stopPropagation();    
										  }
										});
						</script>
						
				<table width="100%" class="table"  id="tableProducts">
					<thead><tr>
					
						<th style="width:45px">ID</th>
						<th style="width:130px">Codice articolo</th>
						<th style="width:370px">Nome prodotto</th>
						<th style="width:30px; text-align:right">Qt.</th>
						<th style="width:195px; text-align:right">Prezzo CLI</th>
						<th style="width:195px; text-align:right">Prezzo RIV</th>
						<th style="width:55px">Canc.</th>
					
					</tr></thead><tbody id="tableProductsBody">';
					
					if(isset($_GET['updatebundle'])) {

						$bundle_prices = $bundle['bundle_prices'];
						
						$bundle_products = explode(":", $bundle_prices);
						$totale = 0;
						$totaleriv = 0;
						
						foreach($bundle_products as $bundle_product) {
						
							if($bundle_product == "") { } else {
								$bundle_prod = explode(";", $bundle_product);
								$prezzo_prd_cli = Product::trovaMigliorPrezzo($bundle_prod[0],1,$bundle_prod[1]);
								$prezzo_base_cli = number_format($prezzo_prd_cli, 2,",","");
								$prezzo_prd_cli = $prezzo_prd_cli - (($prezzo_prd_cli * (str_replace(",",".",$bundle_prod[2])))/100);
								$prezzo_prd_cli = number_format($prezzo_prd_cli, 2,",","");
								
								$prezzo_prd_riv = Product::trovaMigliorPrezzo($bundle_prod[0],3,$bundle_prod[1]);
								$prezzo_base_riv = number_format($prezzo_prd_riv, 2,",","");
								$prezzo_prd_riv = $prezzo_prd_riv - (($prezzo_prd_riv * (str_replace(",",".",$bundle_prod[3])))/100);
								$prezzo_prd_riv = number_format($prezzo_prd_riv, 2,",","");
								$tokenCatalog = Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee));
								echo '
								<tr id="tr_'.$bundle_prod[0].'">
									<td>'.$bundle_prod[0].'</td>
									<td><a href="index.php?tab=AdminCatalog&id_product='.$bundle_prod[0].'&updateproduct&token='.$tokenCatalog.'" target="_blank" style="cursor:pointer"><span class="span-reference" title="'.Product::showProductTooltip($bundle_prod[0]).'">'.Db::getInstance()->getValue("SELECT reference FROM product WHERE id_product = ".$bundle_prod[0]."").'</span></a></td>
									
									<td>'.Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_lang = 5 AND id_product = ".$bundle_prod[0]."").'</td>
									
									<td><input style="text-align:right" name="nuovo_quantity['.$bundle_prod[0].']]" id="product_quantity['.$bundle_prod[0].']" type="text" size="3" value="'.$bundle_prod[1].'" onkeyup="if (isArrowKey(event)) return; calcolaImporto('.$bundle_prod[0].');" /></td>';
									$sc_acq_1_b = Db::getInstance()->getValue("SELECT sconto_acquisto_1 FROM product WHERE id_product = ".$bundle_prod[0]."");
									
									$sc_acq_2_b = Db::getInstance()->getValue("SELECT sconto_acquisto_2 FROM product WHERE id_product = ".$bundle_prod[0]."");
									$sc_acq_3_b = Db::getInstance()->getValue("SELECT sconto_acquisto_3 FROM product WHERE id_product = ".$bundle_prod[0]."");
									$listino_b = Db::getInstance()->getValue("SELECT listino FROM product WHERE id_product = ".$bundle_prod[0]."");
									$wholesale_b = Db::getInstance()->getValue("SELECT wholesale_price FROM product WHERE id_product = ".$bundle_prod[0]."");
									$wholesale_price = ($wholesale_b > 0 ? $wholesale_b : ($listino_b*(100-$sc_acq_1_b)/100)*((100-$sc_acq_2_b)/100)*((100-$sc_acq_3_b)/100));
									echo '<input type="hidden" name="wholesale_price['.$bundle_prod[0].']" id="wholesale_price['.$bundle_prod[0].']" value="'.number_format(round($wholesale_price,2), 2, ',', '').'" />';
										
									$sc_qta_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_1' AND id_product = '".$bundle_prod[0]."'");
									$sc_qta_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_2' AND id_product = '".$bundle_prod[0]."'");
									$sc_qta_3_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_3' AND id_product = '".$bundle_prod[0]."'");	
									$sc_riv_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_1' AND id_product = '".$bundle_prod[0]."'");	
									$sc_riv_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_2' AND id_product = '".$bundle_prod[0]."'");
											
									$sc_qta_1 = $sc_qta_1_r['price'] - ($sc_qta_1_r['price'] * $sc_qta_1_r['reduction']);
									$sc_qta_2 = $sc_qta_2_r['price'] - ($sc_qta_2_r['price'] * $sc_qta_2_r['reduction']);
									$sc_qta_3 = $sc_qta_3_r['price'] - ($sc_qta_3_r['price'] * $sc_qta_3_r['reduction']);
									$sc_riv_1 = $sc_riv_1_r['price'] - ($sc_riv_1_r['price'] * $sc_riv_1_r['reduction']);
									$sc_riv_2 = $sc_riv_2_r['price'] - ($sc_riv_2_r['price'] * $sc_riv_2_r['reduction']);
											
									$speciale = Product::trovaPrezzoSpeciale($bundle_prod[0], 1, 0);
									$unitario = Db::getInstance()->getValue("SELECT price FROM product WHERE id_product = '".$bundle_prod[0]."'");
									
									if($speciale < $unitario && $speciale != 0) {
										$unitario = $speciale;
									}		
									
									$sc_qta_1_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_1' AND id_product = '".$bundle_prod[0]."'");
									$sc_qta_2_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_2' AND id_product = '".$bundle_prod[0]."'");
									$sc_qta_3_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_3' AND id_product = '".$bundle_prod[0]."'");
											
									$sc_riv_1_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_1' AND id_product = '".$bundle_prod[0]."'");
									$sc_riv_2_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_2' AND id_product = '".$bundle_prod[0]."'");	
										
									echo '<input type="hidden" id="unitario['.$bundle_prod[0].']" value="'.$unitario.'" />';
									echo '<input type="hidden" id="sc_qta_1['.$bundle_prod[0].']" value="'.$sc_qta_1.'" />';
									echo '<input type="hidden" id="sc_qta_2['.$bundle_prod[0].']" value="'.$sc_qta_2.'" />';
									echo '<input type="hidden" id="sc_qta_3['.$bundle_prod[0].']" value="'.$sc_qta_3.'" />';
									echo '<input type="hidden" id="sc_riv_1['.$bundle_prod[0].']" value="'.$sc_riv_1.'" />';
									echo '<input type="hidden" id="sc_riv_2['.$bundle_prod[0].']" value="'.$sc_riv_2.'" />';
					
									echo '<input type="hidden" id="sc_qta_1_q['.$bundle_prod[0].']" value="'.$sc_qta_1_q.'" />';
									echo '<input type="hidden" id="sc_qta_2_q['.$bundle_prod[0].']" value="'.$sc_qta_2_q.'" />';
									echo '<input type="hidden" id="sc_qta_3_q['.$bundle_prod[0].']" value="'.$sc_qta_3_q.'" />';
									echo '<input type="hidden" id="sc_riv_1_q['.$bundle_prod[0].']" value="'.$sc_riv_1_q.'" />';
									echo '<input type="hidden" id="sc_riv_2_q['.$bundle_prod[0].']" value="'.$sc_riv_2_q.'" />';	
										
									$totaleacquisti += $wholesale_price*1;
									$prezzo_partenza = (float)str_replace(",",".",$prezzo_prd_cli);
									$priceclib = (float)str_replace(",",".",$prezzo_prd_cli)*$bundle_prod[1];
									$prezzoriv = (float)str_replace(",",".",$prezzo_prd_riv)*$bundle_prod[1];
											
									$marginalita = ((($prezzo_partenza - $wholesale_price)*100)/$prezzo_partenza);
									$marginalitariv = ((($prezzoriv - $wholesale_price)*100)/$prezzo_partenza);
											
									
									echo '<td style="text-align:right">';
									echo '<table style="width:100%">
									<tr>
									<td>Prz. base </td>
									<td>
									<span '.(Product::checkPrezzoSpeciale($bundle_prod[0],1,1) == 'yes' ? 'style="color:red"' : '').' id="prz_vis_cli['.$bundle_prod[0].']">'.$prezzo_base_cli.'</span><span '.(Product::checkPrezzoSpeciale($bundle_prod[0],1,1) == 'yes' ? 'style="color:red"' : '').'> &euro;</span> </td>
									</tr>
									
									<tr>
									<td>Sconto %</td>
									<td>
									<input type="hidden" id="product_price['.$bundle_prod[0].']" name="nuovo_price['.$bundle_prod[0].']" value="'.$prezzo_prd_cli.'" />
									
									<input type="text" style="text-align:right" id="product_sconto['.$bundle_prod[0].']" size="4" name="nuovo_sconto['.$bundle_prod[0].']" value="'.$bundle_prod[2].'" onkeyup="javascript: calcolaImporto('.$bundle_prod[0].');"  />
									</td></tr>
									
									<tr>
									<td>Prz. nel bundle</td>
									<td>
									<span id="prz_nel_bundle_cli['.$bundle_prod[0].']">'.$prezzo_prd_cli.'</span> &euro; </td>
									</tr>
									<tr>
									<td>Marg.</td>
									<td>
									<span id="spanmarginalita['.$bundle_prod[0].']">'.number_format($marginalita, 2, ',', '').'%</span>
									<input id="impImporto['.$bundle_prod[0].']" class="importo" type="hidden" value="'.$priceclib.'" />
									</td>
									</tr>
									</table>
									
									</td>
									
									<td style="text-align:right">
									<table style="width:100%">
									<tr>
									<td>Prz. base</td>
									<td>
									<span id="prz_vis_riv['.$bundle_prod[0].']">'.$prezzo_base_riv.'</span> &euro; </td>
									</tr>
									
									<tr>
									<td>Sconto %</td>
									<td>
									<input type="hidden" id="product_priceriv['.$bundle_prod[0].']" name="nuovo_priceriv['.$bundle_prod[0].']" value="'.$prezzo_prd_riv.'" />
									
									<input type="text" style="text-align:right" size="4" id="product_scontoriv['.$bundle_prod[0].']" name="nuovo_scontoriv['.$bundle_prod[0].']" value="'.$bundle_prod[3].'" onkeyup="javascript: calcolaImporto('.$bundle_prod[0].');"  />
									
									</td></tr>
									<tr>
									<td>Prz. nel kit</td>
									<td>
									<span id="prz_nel_bundle_riv['.$bundle_prod[0].']">'.$prezzo_prd_riv.'</span> &euro; </td>
									</tr>
									<tr>
									<td>Marg.</td>
									<td><span id="spanmarginalitariv['.$bundle_prod[0].']">'.number_format($marginalitariv, 2, ',', '').'%</span>
									<input id="impImportoRiv['.$bundle_prod[0].']" class="importoriv" type="hidden" value="'.$prezzoriv.'" />
									</td>
									</tr>
									
									</table>
									
									</td>
									
											
									<td style="text-align:center">
									<input name="nuovo_prodotto['.$bundle_prod[0].']" type="hidden" value="'.$bundle_prod[0].'" />
									'.($bundle_prod[0] == $bundle['father'] ? '<strong>PADRE</strong>' : '<a style="cursor:pointer" onclick="togliImporto('.$bundle_prod[0].'); delProduct30('.$bundle_prod[0].')"><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a>').'
									</td>
								</tr>';
								$totale += $priceclib;
								$totaleriv += $prezzoriv;
							}
						}
					}
					
					else {
					
						
					}
				
				echo '</tbody>';
				echo '<tfoot><tr><td></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td style="text-align:right">';
			
				$marginalitatotale = ((($totale - $totaleacquisti)*100) / $totale);
				echo "<input type='hidden' value='".$totale."' name='totaleprodotti' id='totaleprodotti' />
				<input type='hidden' value='".$totaleriv."' name='totaleprodottiriv' id='totaleprodottiriv' />
				TOT. kit CLI: <span id='spantotaleprodotti'>".number_format($totale,2, ',', '')."</span> &euro;<br />
				<span style='visibility:hidden' id='marginalitatotale'>".number_format($marginalitatotale,2, ',', '')."%</span>
				</td><td style='text-align:right'>
				TOT. kit RIV: <span id='spantotaleprodottiriv'>".number_format($totaleriv,2, ',', '')."</span>  &euro;<br />
				
				<span style='visibility:hidden' id='marginalitatotaleriv'>".number_format($marginalitatotaleriv,2, ',', '')."%</span>
				</td><td style='text-align:right'>
				
				</td></tr></tfoot>";
				
				echo '</table><br />
				';
				$rowcompara = Db::getInstance()->getRow("SELECT trovaprezzi, google_shopping, comparaprezzi_check FROM bundle WHERE id_bundle = $_GET[id_bundle]");
					echo '
				<strong>Estensione garanzia</strong><br />';
				echo '<script type="text/javascript" src="../js/select2.js"></script>
					<script type="text/javascript">
					$(document).ready(function() { $("#estensione_garanzia").select2(); });
					</script>';
				echo '<select id="estensione_garanzia" name="estensione_garanzia" style="width:600px">
					<option name="0" value="0">--- Scegli un prodotto ---</option>
					';
					
					
					$results = Db::getInstance()->ExecuteS("SELECT DISTINCT p.id_product, p.reference, p.price, pl.name FROM product p LEFT JOIN product_lang pl ON p.id_product = pl.id_product WHERE (pl.name LIKE '%Estensione%') AND pl.id_lang = 5 GROUP BY p.id_product");
					
					foreach ($results as $row) {
					
						echo "<option name='$row[reference]' ".($bundle['estensione_garanzia'] == $row['id_product'] ? "selected='selected'" : "")." value='$row[id_product]'"; 
						echo ">$row[name] ($row[reference])  - ".Tools::displayPrice($row['price'], 1, false)."</option>";
					
					}
						
					echo "</select>&nbsp;&nbsp;&nbsp;<br /><br />
					";
					
							echo '
				<strong>Supporto da remoto</strong><br />';
				echo '
					<script type="text/javascript">
					$(document).ready(function() { $("#supporto_da_remoto").select2(); });
					</script>';
			echo '<select id="supporto_da_remoto" name="supporto_da_remoto" style="width:600px">
					<option name="0" value="0">--- Scegli un prodotto ---</option>
					';
					
					
					$results = Db::getInstance()->ExecuteS("SELECT DISTINCT p.id_product, p.reference, p.price, pl.name FROM product p LEFT JOIN product_lang pl ON p.id_product = pl.id_product WHERE (pl.name LIKE '%assistenza%') AND pl.id_lang = 5 GROUP BY p.id_product");
					
					foreach ($results as $row) {
					
						echo "<option name='$row[reference]' ".($bundle['supporto_da_remoto'] == $row['id_product'] ? "selected='selected'" : "")." value='$row[id_product]'"; 
						echo ">$row[name] ($row[reference])  - ".Tools::displayPrice($row['price'], 1, false)."</option>";
					
					}
						
					echo "</select>&nbsp;&nbsp;&nbsp;<br /><br />
					";
					
					
					echo '
				<strong>Telegestione</strong><br />';
				echo '
					<script type="text/javascript">
					$(document).ready(function() { $("#telegestione").select2(); });
					</script>';
			echo '<select id="telegestione" name="telegestione" style="width:600px">
					<option name="0" value="0">--- Scegli un prodotto ---</option>
					';
					
					
					$results = Db::getInstance()->ExecuteS("SELECT DISTINCT p.id_product, p.reference, p.price, pl.name FROM product p LEFT JOIN product_lang pl ON p.id_product = pl.id_product WHERE (pl.name LIKE '%teleassist%') AND pl.id_lang = 5 GROUP BY p.id_product");
					
					foreach ($results as $row) {
					
						echo "<option name='$row[reference]' ".($bundle['telegestione'] == $row['id_product'] ? "selected='selected'" : "")." value='$row[id_product]'"; 
						echo ">$row[name] ($row[reference])  - ".Tools::displayPrice($row['price'], 1, false)."</option>";
					
					}
						
					echo "</select>&nbsp;&nbsp;&nbsp;<br /><br />
					";
					
					
					echo "<strong>Categoria/e del kit</strong>";
				$default_category = $bundle['id_category_default'];
				$selected_categories = array();
				
				$bundle_categories = Db::getInstance()->executeS("SELECT id_category FROM category_bundle WHERE id_bundle = ".$bundle['id_bundle']."");
				foreach($bundle_categories as $bdlcat)
					$selected_categories[] = $bdlcat['id_category'];
				
				$selected_categories[] = $default_category;
						
						$selectedCat = Category::getCategoryInformations($selected_categories, 5);
						echo '
						<script type="text/javascript">
							post_selected_cat = \''.implode(',', array_keys($selectedCat)).'\';
						</script>';
						

						echo '<select id="id_category_default" name="id_category_default">';
						foreach($selectedCat AS $cat)
							echo '<option value="'.$cat['id_category'].'" '.($bundle['id_category_default'] == $cat['id_category'] ? 'selected' : '').'>'.$cat['name'].'</option>';
						echo '</select>
						</td>
					</tr>
					<tr id="tr_categories">
						<td colspan="2">
						';
					// Translations are not automatic for the moment ;)
					$trads = array(
						 'Home' => $this->l('Home'),
						 'selected' => $this->l('selected'),
						 'Collapse All' => $this->l('Collapse All'),
						 'Expand All' => $this->l('Expand All'),
						 'Check All' => $this->l('Check All'),
						 'Uncheck All'  => $this->l('Uncheck All')
					);
					echo Helper::renderAdminCategorieTree($trads, $selectedCat).'
						</td>
					</tr>	';
	
				echo "
				<strong>Seleziona i comparaprezzi su cui esportare il kit:</strong>
				<script type='text/javascript'>
				function checkUncheckSome(controller,theElements) {
					
					 var formElements = theElements.split(',');
					 var theController = document.getElementById(controller);
					 for(var z=0; z<formElements.length;z++){
					  theItem = document.getElementById(formElements[z]);
					  if(theItem.type && theItem.type=='checkbox'){

							theItem.checked=theController.checked;

					  } else {
						  theInputs = theItem.getElementsByTagName('input');
					  for(var y=0; y<theInputs.length; y++){
					  if(theInputs[y].type == 'checkbox' && theInputs[y].id != theController.id){
						 theInputs[y].checked = theController.checked;
						}
					  }
					  }
					}
				}
				</script>";
			
				
				
				echo "<fieldset id='comparaprezzifields' style='border:0px'> ";
				if(!isset($_GET['updatebundle'])) {
					echo "
					
					<input type='checkbox' name='trovaprezzi_add' /> Trovaprezzi<br />
					<input type='checkbox' name='google_shopping_add' /> Google Shopping<br /><br />
					
					<input type='checkbox' name='bundle_amazon_it' />Amazon.it
					<input type='checkbox' name='bundle_amazon_fr' />Amazon.fr
					<input type='checkbox' name='bundle_amazon_es' />Amazon.es
					<input type='checkbox' name='bundle_amazon_uk' />Amazon.co.uk
					<input type='checkbox' name='bundle_amazon_de' />Amazon.de
					<input type='checkbox' name='bundle_amazon_nl' />Amazon.nl
<br />
					<br />
					<input type='checkbox' onclick=\"checkUncheckSome('tutti_add','comparaprezzifields')\" id='tutti_add' name='tutti_add' /> Tutti<br /></fieldset>
					
					";

				} else {

		
					$amazon_check = explode(";",Db::getInstance()->getValue('SELECT amazon FROM bundle WHERE id_bundle = '.$_GET['id_bundle']));
					
					echo "
					<input type='checkbox' name='trovaprezzi_add'"; if($rowcompara['trovaprezzi'] == 1) { echo "checked='checked'"; } else if($rowcompara['trovaprezzi'] == 0)  { }
					else { echo "checked='checked'"; } echo " /> Trovaprezzi<br />
					<input type='checkbox' name='google_shopping_add'"; if($rowcompara['google_shopping'] == 1) { echo "checked='checked'"; } else if($rowcompara['google_shopping'] == 0)  { }
					else { echo "checked='checked'"; } echo " /> Google Shopping<br /><br />
					
					<input type='checkbox' name='bundle_amazon_it' checked='checked'  ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('bundle_amazon_it') ? "checked='checked'" : '') : (in_array("5",$amazon_check) ? "checked='checked'" : ''))." />Amazon.it
					<input type='checkbox' name='bundle_amazon_fr' checked='checked' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('bundle_amazon_fr') ? "checked='checked'" : '') : (in_array("2",$amazon_check) ? "checked='checked'" : ''))." />Amazon.fr
					<input type='checkbox' name='bundle_amazon_es' checked='checked' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('bundle_amazon_es') ? "checked='checked'" : '') : (in_array("3",$amazon_check) ? "checked='checked'" : '')) : '')." />Amazon.es
					<input type='checkbox' name='bundle_amazon_uk' checked='checked' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('bundle_amazon_uk') ? "checked='checked'" : '') : (in_array("1",$amazon_check) ? "checked='checked'" : ''))." />Amazon.co.uk
					<input type='checkbox' name='bundle_amazon_de' checked='checked' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('bundle_amazon_de') ? "checked='checked'" : '') : (in_array("4",$amazon_check) ? "checked='checked'" : ''))." />Amazon.de<br /><br />
					<input type='checkbox' name='bundle_amazon_nl' checked='checked' ".(isset($_POST['tutti_i_comparaprezzi']) ? (Tools::getIsset('bundle_amazon_nl') ? "checked='checked'" : '') : (in_array("4",$amazon_check) ? "checked='checked'" : ''))." />Amazon.nl<br /><br />

					<input type='checkbox' onclick=\"checkUncheckSome('tutti_add','comparaprezzifields')\" id='tutti_add' name='tutti_add' /> Tutti<br /></fieldset>
					



					<br /><br />

					";

				

				}
				
				
				echo "</fieldset><br />
				<input type='hidden' name='id_product' value='".$obj->id_product."' />
				<input type='hidden' name='id_tabs' value='3' />
				<p style='text-align:center'>";
				
				if(isset($_GET['updatebundle'])) {
					echo "<input type='submit' class='button' name='submitupdatedbundle' value='Salva kit' />&nbsp;&nbsp;&nbsp;";
				} else {
					echo "<input type='submit' class='button' name='submitnewbundle' value='Salva kit' />&nbsp;&nbsp;&nbsp;";
				}
				
				echo '<a href="'.$currentIndex.'&id_product='.$obj->id.'&updateproduct&token='.Tools::getAdminToken('AdminBundles'.(int)(Tab::getIdFromClassName('AdminBundles')).(int)($cookie->id_employee)).'&submitAddAndStay=on&tabs=2" class="button">Torna alla lista dei kit</a>
				</form>
				</p>';
			} 
			
			else {
			
			
				echo "
			
				<h1>Kit</h1>";
				
				
				echo "Cerca kit in base al codice prodotto: <br />
			<form action='' name='cercareordineprodotto' method='post'>";
			echo '<script type="text/javascript" src="../js/select2.js"></script>
					<script type="text/javascript">
					$(document).ready(function() { $("#cercabundleprodotto").select2(); });
					</script>';
			echo '<select id="cercabundleprodotto" name="cercabundleprodotto" style="width:600px">
					<option name="0" value="0">--- Scegli un prodotto ---</option>
					';
					
					
					$results = Db::getInstance()->ExecuteS("SELECT DISTINCT p.id_product, p.reference, pl.name FROM product p LEFT JOIN product_lang pl ON p.id_product = pl.id_product WHERE pl.id_lang = 5");
					
					foreach ($results as $row) {
					
						echo "<option name='$row[reference]' value='$row[id_product]'"; 
						echo ">$row[name] ($row[reference])</option>";
					
					}
						
					echo "</select>&nbsp;&nbsp;&nbsp;";
			
			echo "<input type='submit' value='Cerca' class='button'/>    &nbsp;&nbsp;&nbsp;<input type='submit' name='azzeraricerca' value='Azzera ricerca' class='button' /></form>
			<br />";
				
				
				if(isset($_POST['cercabundleprodotto'])) {
				echo $_POST['cercabundleprodotto']; 
					$bundles = Db::getInstance()->executeS("SELECT * FROM bundle WHERE father = '".$_POST['cercabundleprodotto']."' OR bundle_products LIKE '%".$_POST['cercabundleprodotto']."%'");
				}
				else if(isset($_POST['azzeraricerca'])) {
					$bundles = Db::getInstance()->executeS("SELECT * FROM bundle");
				}
				else {
					$bundles = Db::getInstance()->executeS("SELECT * FROM bundle");
				}
				$bundles_current = array();
				
				foreach($bundles as $bundle) {
				
					$bundle_products = explode(";", $bundle['bundle_products']);
					
					if(in_array($obj->id, $bundle_products)) {
					
						$bundles_current[] = $bundle;
					
					}
				}
				
				if(!empty($bundles_current)) {
				
					echo '
					
					
					<table width="100%" class="table" id="tableBundles">
					<thead>
					<tr>
						<th style="width:45px">ID</th>
						
						<th style="width:45px">Immagine</th>
						<th style="width:90px">Codice</th>
						<th style="width:150px">Descrizione</th>
						<th style="width:450px">Prodotti</th>
						<th style="width:75px; text-align:right">Prezzo CLI</th>
						<th style="width:75px; text-align:right">Prezzo RIV</th>
						<th style="width:35px; text-align:right">On/Off</th>
						<th style="width:55px">Azioni</th>
					</tr>
					</thead>
					<tbody id="tableBundlesBody">
					';	
					
					foreach($bundles_current as $bundle) {
						$prezzi_bundle = array();
						$prezzi_bundle_riv = array();
						
						echo "<tr><td><input type='hidden' class='id_bundle_td' value='".$bundle['id_bundle']."' />".$bundle['id_bundle']."</td>";
						
						$id_image_bundle = Db::getInstance()->getValue("SELECT id_image FROM bundle_image WHERE id_bundle = ".$bundle['id_bundle']."");
						
						echo "<td>";
						if($id_image_bundle > 0) {
						
						echo '<img src="'._PS_IMG_.'b/'.$id_image_bundle."-".$bundle['id_bundle'].'-small.jpg" alt="" title="" />';
						
						}
						
						else {
						
						echo '<p style="text-align:center">-</p>';
						
						}
						
						echo "</td>";
						
						echo "<td>".$bundle['bundle_ref']."</td>";
						echo "<td>".$bundle['bundle_name']."</td>";
						$bundle_products_row = explode(":", $bundle['bundle_prices']);
						
						echo "<td style='vertical-align:center'>";
						$i_n = 0;
						foreach($bundle_products_row as $bundle_row) {
							
							$idprz = explode(";", $bundle_row);
						
							
							$nomeprod = Db::getInstance()->getRow("SELECT i.id_image, pl.id_product, pl.name, p.reference FROM product_lang pl LEFT JOIN image i ON pl.id_product = i.id_product JOIN product p ON pl.id_product = p.id_product WHERE pl.id_product = ".$idprz[0]." AND pl.id_lang = 5 AND i.cover = 1");
							
							if(empty($nomeprod)) {
							}
							else {
								echo ($i_n == 0 ? '' : "<div style='float:left; position:relative; text-align:center'><br /> + </div>");
								echo "<div style='float:left; position:relative; text-align:center; margin-right:5px; margin-left:5px;'>";
								echo "<div style='position:relative;display:block;margin:0 auto; width:50px'><div style='position:absolute; background-color:white;border:1px solid black; color:black; width:25px; left:5px'><strong>".$idprz[1]."x</strong></div>";
								echo "<img style='border:1px solid black;' src='http://www.ezdirect.it/img/p/".$nomeprod['id_product']."-".$nomeprod['id_image']."-80x80.jpg' alt='".$nomeprod['name']."' title='".$nomeprod['name']."' width='50' height='50' /></div>";
								echo "<br />".$nomeprod['reference']."</div>";
							}
							

							
							$i_n++;
							
							$prezzo_prd_cli = Product::trovaMigliorPrezzo($idprz[0],1,$idprz[1]);
							$prezzo_prd_cli = round($prezzo_prd_cli - (($prezzo_prd_cli * (str_replace(",",".",$idprz[2])))/100),2);
							
							
							$prezzo_prd_riv = Product::trovaMigliorPrezzo($idprz[0],3,$idprz[1]);
							$prezzo_prd_riv = round($prezzo_prd_riv - (($prezzo_prd_riv * (str_replace(",",".",$idprz[3])))/100),2);
							
							
							$prezzi_bundle[] = (float)($prezzo_prd_cli)*$idprz[1];
							$prezzi_bundle_riv[] = (float)($prezzo_prd_riv)*$idprz[1];
						}
						
						
						echo "</td>";
						
						echo "<td style='text-align:right'>";
						$prezzo_tot = 0;
							foreach($prezzi_bundle as $prezzo_bundle) {
								
								$prz_u = (float)(str_replace(",",".",$prezzo_bundle))." - ";
								$prezzo_tot += $prz_u;
							}
							
						echo number_format($prezzo_tot,2,",","")." &euro;";	
						echo "</td>";
						echo "<td style='text-align:right'>";
						$prezzo_tot_riv = 0;
							foreach($prezzi_bundle_riv as $prezzo_bundle_riv) {
								
								$prz_u_riv = (float)(str_replace(",",".",$prezzo_bundle_riv))." - ";
								$prezzo_tot_riv += $prz_u_riv;
							}
							
						echo number_format($prezzo_tot_riv,2,",","")." &euro;";	
						echo "</td>";
						echo "<td>".($bundle['active'] == 1 ? '<img src="../img/admin/enabled.gif" />' : '<img src="../img/admin/forbbiden.gif" />')."</td>";
						echo "<td>";
						echo '<a style="cursor:pointer" href="'.$currentIndex.'&id_product='.$bundle['father'].'&updateproduct&newbundle&id_bundle='.$bundle['id_bundle'].'&updatebundle&token='.Tools::getAdminToken('AdminBundles'.(int)(Tab::getIdFromClassName('AdminBundles')).(int)($cookie->id_employee)).'&tabs=2"><img src="../img/admin/edit.gif" /></a>
						
						<a style="cursor:pointer" href="'.$currentIndex.'&id_product='.$obj->id.'&updateproduct&id_bundle='.$bundle['id_bundle'].'&deletebundle&token='.Tools::getAdminToken('AdminBundles'.(int)(Tab::getIdFromClassName('AdminBundles')).(int)($cookie->id_employee)).'&tabs=2"  onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }"><img src="../img/admin/delete.gif" /></a>';
						echo "</td>";
						echo "</tr>";
					}
					
					echo "</tbody></table>";
					
			
				}
				
				else {
					echo 'Nessun kit ancora creato per questo prodotto.<br /><br />';

				}
				
			
			}
			
			
			
			echo '</div>';
		}
		
	}
	
		/**
	 * Add or update a bundle image
	 *
	 * @param object $product Product object to add image
	 */
	public function addBundleImage($id_bundle, $id_image)
	{
		/* Updating an existing product image */
		if ($id_image > 0)
		{
			Db::getInstance()->executeS("UPDATE bundle_image_lang SET legend = '".htmlentities($_POST['image_legend'])."' WHERE id_image = ".$id_image."");
				
			$this->copyFromPost($image, 'image');
			if (sizeof($this->_errors)) {
					$this->_errors[] = Tools::displayError('An error occurred while updating image.');
			}
			else if (isset($_FILES['image_bundle']['tmp_name']) AND $_FILES['image_bundle']['tmp_name'] != NULL) {
					$imagesTypes = ImageType::getImagesTypes('products');
						foreach ($imagesTypes AS $k => $imageType) {
							@unlink(_PS_IMG_DIR_.'b/'.$id_image.'-'.$id_bundle.'-'.stripslashes($imageType['name']).'.'.'jpg', $imageType['width'], $imageType['height'], 'jpg');
						
						}
					$this->copyBundleImage($id_bundle, $id_image);
			}
		}

		/* Adding a new bundle image */
		else if (isset($_FILES['image_bundle']['name']) && $_FILES['image_bundle']['name'] != NULL )
		{
			
			$id_image = Db::getInstance()->getValue("SELECT id_image FROM bundle_image ORDER BY id_image DESC");
			$id_image = $id_image+1;
			Db::getInstance()->executeS("INSERT INTO bundle_image (id_image, id_bundle) VALUES (".$id_image.", ".$id_bundle.")");
			Db::getInstance()->executeS("INSERT INTO bundle_image_lang (id_image, id_lang, legend) VALUES (".$id_image.", 5, '".htmlentities($_POST['image_legend'])."')");
			
			if ($error = checkImageUploadError($_FILES['image_bundle'])) {
				$this->_errors[] = $error;
			}
			if (!sizeof($this->_errors) AND isset($_FILES['image_bundle']['tmp_name']) AND $_FILES['image_bundle']['tmp_name'] != NULL)
			{
			
				if (!sizeof($this->_errors))
				{
					
					$this->copyBundleImage($id_bundle, $id_image);
					
				}
				
			}
		}
		/*if (isset($image) AND Validate::isLoadedObject($image) AND !file_exists(_PS_PROD_IMG_DIR_.$image->getExistingImgPath().'.'.$image->image_format))
			$image->delete();
		if (sizeof($this->_errors))
			return false;
		@unlink(_PS_TMP_IMG_DIR_.'/product_'.$product->id.'.jpg');
		@unlink(_PS_TMP_IMG_DIR_.'/product_mini_'.$product->id.'.jpg');
		return ((isset($id_image) AND is_int($id_image) AND $id_image) ? $id_image : true);*/
	}
	
	public function copyBundleImage($id_bundle, $id_image, $method = 'auto')
	{
		if (!isset($_FILES['image_bundle']['tmp_name']))
			return false;
		if ($error = checkImage($_FILES['image_bundle'], $this->maxImageSize))
			$this->_errors[] = $error;
		else
		{
			$path = _PS_IMG_DIR_.'b/'.$id_image."-".$id_bundle;
			
			if (!$new_path = $path)
				$this->_errors[] = Tools::displayError('An error occurred during new folder creation');
			if (!$tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS') OR !move_uploaded_file($_FILES['image_bundle']['tmp_name'], $tmpName))
				$this->_errors[] = Tools::displayError('An error occurred during the image upload');
			elseif (!imageResize($tmpName, $new_path.'.'.'jpg'))
				$this->_errors[] = Tools::displayError('An error occurred while copying image.');
			elseif ($method == 'auto')
			{
				$imagesTypes = ImageType::getImagesTypes('products');
				foreach ($imagesTypes AS $k => $imageType)
					if (!imageResize($tmpName, $new_path.'-'.stripslashes($imageType['name']).'.'.'jpg', $imageType['width'], $imageType['height'], 'jpg'))
						$this->_errors[] = Tools::displayError('An error occurred while copying image:').' '.stripslashes($imageType['name']);
			}

			@unlink($tmpName);
			Module::hookExec('watermark', array('id_image' => $id_image, 'id_product' => $id_product));
		}
	}
	
}

