<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14703 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminOreFerie extends AdminTab
{
	
	
		public function display()
		{	
		
			echo '<h1>Situazione ferie impiegati</h1>';
			/*
			echo '
			
			<form method="post">
			<select name="month-ore" style="height:25px">
			<option value="1" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '1' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 1 ? 'selected="selected"' : '') ).'>Gennaio</option>
			<option value="2" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '2' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 2 ? 'selected="selected"' : '') ).'>Febbraio</option>
			<option value="3" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '3' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 3 ? 'selected="selected"' : '') ).'>Marzo</option>
			<option value="4" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '4' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 4 ? 'selected="selected"' : '') ).'>Aprile</option>
			<option value="5" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '5' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 5 ? 'selected="selected"' : '') ).'>Maggio</option>
			<option value="6" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '6' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 6 ? 'selected="selected"' : '') ).'>Giugno</option>
			<option value="7" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '7' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 7 ? 'selected="selected"' : '') ).'>Luglio</option>
			<option value="8" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '8' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 8 ? 'selected="selected"' : '') ).'>Agosto</option>
			<option value="9" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '9' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 9 ? 'selected="selected"' : '') ).'>Settembre</option>
			<option value="10" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '10' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 10 ? 'selected="selected"' : '') ).'>Ottobre</option>
			<option value="11" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '11' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 11 ? 'selected="selected"' : '') ).'>Novembre</option>
			<option value="12" '.(Tools::getIsset('month-ore') && Tools::getValue('month-ore') == '12' ? 'selected="selected"' : (!Tools::getIsset('month-ore') && date('n') == 12 ? 'selected="selected"' : '') ).'>Dicembre</option>
			
			
			</select>
			&nbsp;&nbsp;&nbsp;
			<select name="year-ore" style="height:25px">
			<option value="2014" '.(Tools::getIsset('year-ore') && Tools::getValue('year-ore') == '2014' ? 'selected="selected"' : '').'>2014</option>
			<option value="2015"  '.(Tools::getIsset('year-ore') && Tools::getValue('year-ore') == '2015' ? 'selected="selected"' : '').' '.(!Tools::getIsset('year-ore') ? 'selected="selected"' : '').'>2015</option>
			</select>
			&nbsp;&nbsp;&nbsp;
			<input type="submit" value="Seleziona" name="select-month-year" class="button" style="margin-top:-3px; cursor:pointer" />
			
			<br /><br />

			

			';*/
			
			
		/*	if(Tools::getIsset('year-ore'))
				$year = Tools::getValue('year-ore');
			else
				$year = date("Y");
				
			if(Tools::getIsset('month-ore'))
				$month = Tools::getValue('month-ore');
			else
				$month = date("n");
				
			
			switch($month)
			{
				case 1: $month_name = 'Gennaio'; break; 
				case 2: $month_name = 'Febbraio'; break; 
				case 3: $month_name = 'Marzo'; break; 
				case 4: $month_name = 'Aprile'; break; 
				case 5: $month_name = 'Maggio'; break; 
				case 6: $month_name = 'Giugno'; break; 
				case 7: $month_name = 'Luglio'; break; 
				case 8: $month_name = 'Agosto'; break; 
				case 9: $month_name = 'Settembre'; break; 
				case 10: $month_name = 'Ottobre'; break; 
				case 11: $month_name = 'Novembre'; break; 
				case 12: $month_name = 'Dicembre'; break; 
				default: ''; break; 
			
			
			}
			
			$days_number = cal_days_in_month(CAL_GREGORIAN, $month, $year);
			$days_sequence = '';
			$days_sequence_empty = ',';
			
			
			
			for($i = 1; $i<=$days_number; $i++)
			{
				$days_sequence .= $i.",";
				$days_sequence_empty .= ",";
			}	
			$days_sequence = substr($days_sequence, 0, -1);
			*/
			
			/*
			echo '<strong>Totale fino al mese corrente (31-'.date('m').'-'.date('Y').')</strong>';
			
			$ore_totali = array();
			$ferie_godute = array();
			$ferie_residue = array();
			$permessi_goduti = array();
			$permessi_residui = array();
			$certificati = array();
			
			$permessi_residui[2] = 60;
			$permessi_residui[7] = 55;
			$permessi_residui[4] = 26;
			$permessi_residui[3] = 29;
			$permessi_residui[5] = 25;
			
			$ferie_residue[2] = 27;
			$ferie_residue[7] = 19;
			$ferie_residue[4] = 23;
			$ferie_residue[3] = 20;
			$ferie_residue[5] = 23;
			
			
			echo '<table class="table">';
			echo '<tr><th>Dipendente</th><!-- <th>Ore totali</th> --><th>Ferie godute</th><th>Permessi goduti</th><th>Ferie residue</th><th>Permessi residui</th><th>Certificati</th></tr>';
		
			
			foreach ($employees as $employee) {
			
			
				if($employee['id_employee'] == 6)
				{
				}
				else
				{
					$rowHeaders .= '"ID'.$employee['id_employee'].'",';
						
					$months = Db::getInstance()->executeS('SELECT valori_ore FROM ore WHERE id_employee = '.$employee['id_employee'].' AND year = '.$year.' AND month <= '.date('m').'');
					foreach($months as $month)
					{
						$valori_ore = $month['valori_ore'];
						$valori_ore_array = explode("*", $valori_ore);
						
						foreach($valori_ore_array as $v)
						{
							if(is_numeric($v))
							{
								$ore_totali[$employee['id_employee']] += $v;
								if($v < 8 && $v != 0) 
								{
									if($v == 4)
									{
										$ferie_godute[$employee['id_employee']] += 0.5;
										$ferie_residue[$employee['id_employee']] -= 0.5;
									}
									else
									{
										$permesso = 8-($v);
										$permessi_goduti[$employee['id_employee']] += $permesso;
										$permessi_residui[$employee['id_employee']] -= $permesso;
									}
									
								}
								
							}
							else
							{
								if($v == 'F')
								{
									$ferie_godute[$employee['id_employee']] += 1;
									$ferie_residue[$employee['id_employee']] -= 1;
								
								}
								else if($v == 'C')
								{
									$certificati[$employee['id_employee']] += 1;
								
								}
								else {
								}
							
							
							
							}
						
						}
						
						if( is_numeric( $ferie_godute[$employee['id_employee']] ) && floor( $ferie_godute[$employee['id_employee']] ) != $ferie_godute[$employee['id_employee']])
						{
							$ferie_godute[$employee['id_employee']] -= 0.5;
							$ferie_residue[$employee['id_employee']] += 0.5;
							$permessi_goduti[$employee['id_employee']] += 4;
							$permessi_residui[$employee['id_employee']] -= 4;
						}	
					}
					
					
					echo '<tr>';
					echo '<td>'.$employee['lastname'].' '.$employee['firstname'].'</td>';
					//echo '<td style="text-align:right">'.$ore_totali[$employee['id_employee']].'</td>';
					echo '<td style="text-align:right">'.$ferie_godute[$employee['id_employee']].'</td>';
					echo '<td style="text-align:right">'.$permessi_goduti[$employee['id_employee']].'</td>';
					echo '<td style="text-align:right">'.$ferie_residue[$employee['id_employee']].'</td>';
					echo '<td style="text-align:right">'.$permessi_residui[$employee['id_employee']].'</td>';
					echo '<td style="text-align:right">'.$certificati[$employee['id_employee']].'</td>';
					echo '</tr>';
				}
				
			}
				echo '</table><br /><br />';
			*/
			
			
			
			$year = 2019;
			
			$employees = Db::getInstance()->ExecuteS('
			SELECT `id_employee`, CONCAT(`firstname`, \' \', `lastname`) AS "name", lastname, firstname
			FROM `'._DB_PREFIX_.'employee`
			WHERE id_employee != 6 AND id_employee != 11 AND id_employee != 1  AND id_employee != 16 AND id_employee != 18 AND id_employee != 20 AND id_employee != 3 AND id_employee != 21 AND id_employee != 23 AND id_employee != 25
			ORDER BY (CASE id_employee WHEN 1 THEN 0 WHEN 2 THEN 1 WHEN 7 THEN 2 WHEN 4 THEN 3 WHEN 3 THEN 4 WHEN 5 THEN 6 WHEN 13 THEN 7 WHEN 12 THEN 8 WHEN 14 THEN 9 WHEN 17 THEN 10 WHEN 18 THEN 11 WHEN 19 THEN 12 WHEN 22 THEN 13 WHEN 24 THEN 14 END)');
			
			$ore_totali = array();
			$ferie_godute = array();
			$ferie_pianificate = array();
			$ferie_residue = array();
			$permessi_goduti = array();
			$permessi_pianificati = array();
			$permessi_residui = array();
			$certificati = array();
			
			/*$permessi_residui[2] = 80;
			$permessi_residui[7] = 61;
			$permessi_residui[4] = 34;
			$permessi_residui[3] = 24;
			$permessi_residui[5] = 27;
			$permessi_residui[13] = 19;
			$permessi_residui[12] = 20;
			
			$ferie_residue[2] = 27;
			$ferie_residue[7] = 23;
			$ferie_residue[4] = 27;
			$ferie_residue[3] = 20;
			$ferie_residue[5] = 27;
			$ferie_residue[13] = 26;
			$ferie_residue[12] = 28;
			*/
			/*
			$permessi_residui[2] = 64;
			$permessi_residui[7] = 32;
			$permessi_residui[4] = 9;
			$permessi_residui[3] = 17;
			$permessi_residui[5] = 4;
			$permessi_residui[13] = 7;
			$permessi_residui[12] = 41;
			$permessi_residui[14] = 0;
			
			$ferie_residue[2] = 10;
			$ferie_residue[7] = 7;
			$ferie_residue[4] = 8;
			$ferie_residue[3] = 5;
			$ferie_residue[5] = 10;
			$ferie_residue[13] = 3;
			$ferie_residue[12] = 6;
			$ferie_residue[14] = 5;
			*/
			
			
			//situazione al 30 aprile 2019
			$permessi_residui[2] = 176;
			$permessi_residui[7] = 185;
			$permessi_residui[4] = 154;
			$permessi_residui[5] = 101;
			$permessi_residui[13] = 44;
			$permessi_residui[12] = 90;
			$permessi_residui[14] = 43;
			$permessi_residui[17] = 28;
			$permessi_residui[19] = 24;
			
			$ferie_residue[2] = 34;
			$ferie_residue[7] = 22;
			$ferie_residue[4] = 30;
			$ferie_residue[5] = 31;
			$ferie_residue[13] = 22;
			$ferie_residue[12] = 27;
			$ferie_residue[14] = 23;
			$ferie_residue[17] = 23;
			$ferie_residue[19] = 19;
			
			
			$limit[2] = 8; $limit[7] = 8; $limit[4] = 8; $limit[3] = 8; $limit[5] = 8; $limit[13] = 5; $limit[12] = 8;  $limit[14] = 8; $limit[17] = 8; $limit[19] = 8; 
			
			echo '<strong>Totale anno corrente</strong>';
			
			echo '<table class="table">';
			echo '<tr><th>Dipendente</th><!-- <th>Ore totali</th> --><th>Ferie godute<br />(giorni)</th><th>Ferie pianificate<br />(giorni)</th><th>Ferie residue<br />(giorni) *</th><th>Permessi goduti<br />(ore)</th><th>Permessi pianificati<br />(ore)</th><th>Permessi residui<br />(ore)</th><th>Certificati</th></tr>';
			
			foreach ($employees as $employee) {
				
				if($year < 2019 || ($year == 2019 && $month < 4))
					$limit[19] = 6;
				else
					$limit[19] = 8;
				
				if($employee['id_employee'] == 6 || ($month > 9 && $year >= 2018 && ($employee['id_employee'] == 3 || $employee['id_employee'] == 18)))
				{
				}
				else
				{
					$rowHeaders .= '"ID'.$employee['id_employee'].'",';
						
					$months = Db::getInstance()->executeS('SELECT month, valori_ore FROM ore WHERE id_employee = '.$employee['id_employee'].' AND month > 4 AND year = '.$year.'');
					foreach($months as $month)
					{
						$valori_ore = $month['valori_ore'];
						$valori_ore_array = explode("*", $valori_ore);
						
						
						
						
						$iv = 1;
						
						foreach($valori_ore_array as $v)
						{
							
							if(is_numeric($v))
							{
								
								$ore_totali[$employee['id_employee']] += $v;
								if($v < $limit[$employee['id_employee']] && $v != 0) 
								{
									/*if($v == 4 && $v != $limit[$employee['id_employee']])
									{
										if(strtotime($year.'-'. sprintf("%02d", $month['month']).'-'. sprintf("%02d", $iv).' 23:59:59') < strtotime(date('Y-m-d H:i:s')))
											$ferie_godute[$employee['id_employee']] += 0.5;
										else
											$ferie_pianificate[$employee['id_employee']] += 0.5;
											
										$ferie_residue[$employee['id_employee']] -= 0.5;
									}
									else
									{*/
										$permesso = $limit[$employee['id_employee']]-($v);
										
										if(strtotime($year.'-'. sprintf("%02d", $month['month']).'-'. sprintf("%02d", $iv).' 23:59:59') < strtotime(date('Y-m-d H:i:s')))
											$permessi_goduti[$employee['id_employee']] += $permesso;
										else
											$permessi_pianificati[$employee['id_employee']] += $permesso;
										
										$permessi_residui[$employee['id_employee']] -= $permesso;
									//}
									
									
									
								}
								
							}
							else
							{
								if($v == 'F')
								{
									
									if(strtotime($year.'-'. sprintf("%02d", $month['month']).'-'. sprintf("%02d", $iv).' 23:59:59') < strtotime(date('Y-m-d H:i:s')))
										$ferie_godute[$employee['id_employee']] += 1;
									else
										$ferie_pianificate[$employee['id_employee']] += 1;
									
									$ferie_residue[$employee['id_employee']] -= 1;
								
								}
								else if($v == 'C')
								{
									$certificati[$employee['id_employee']] += 1;
								
								}
								else {
								}
							
							
							
							}
							$iv++;
							
						
						}
						
						if( is_numeric( $ferie_godute[$employee['id_employee']] ) && floor( $ferie_godute[$employee['id_employee']] ) != $ferie_godute[$employee['id_employee']])
						{
							$ferie_godute[$employee['id_employee']] -= 0.5;
							$ferie_residue[$employee['id_employee']] += 0.5;
							//$permessi_goduti[$employee['id_employee']] += 4;
							//$permessi_residui[$employee['id_employee']] -= 4;
						}
						
						if( is_numeric( $ferie_pianificate[$employee['id_employee']] ) && floor( $ferie_pianificate[$employee['id_employee']] ) != $ferie_pianificate[$employee['id_employee']])
						{
							$ferie_pianificate[$employee['id_employee']] -= 0.5;
							$ferie_residue[$employee['id_employee']] += 0.5;
							//$permessi_pianificati[$employee['id_employee']] += 4;
							//$permessi_residui[$employee['id_employee']] -= 4;
						}
						
					}
					
					if($ferie_godute[$employee['id_employee']] == '') 
						$ferie_godute[$employee['id_employee']] = 0;
					if($ferie_pianificate[$employee['id_employee']] == '') 
						$ferie_pianificate[$employee['id_employee']] = 0;
					if($ferie_residue[$employee['id_employee']] == '') 
						$ferie_residue[$employee['id_employee']] = 0;
						
					if($permessi_goduti[$employee['id_employee']] == '') 
						$permessi_goduti[$employee['id_employee']] = 0;
					if($permessi_pianificati[$employee['id_employee']] == '') 
						$permessi_pianificati[$employee['id_employee']] = 0;
					if($permessi_residui[$employee['id_employee']] == '') 
						$permessi_residui[$employee['id_employee']] = 0;
						
					
					echo '<tr>';
					echo '<td>'.$employee['lastname'].' '.$employee['firstname'].'</td>';
					echo '<!-- <td style="text-align:right">'.$ore_totali[$employee['id_employee']].'</td> -->';
					echo '<td style="text-align:right">'.$ferie_godute[$employee['id_employee']].'</td>';
					echo '<td style="text-align:right">'.$ferie_pianificate[$employee['id_employee']].'</td>';
					echo '<td style="text-align:right">'.$ferie_residue[$employee['id_employee']].'</td>';
					echo '<td style="text-align:right">'.$permessi_goduti[$employee['id_employee']].'</td>';
					echo '<td style="text-align:right">'.$permessi_pianificati[$employee['id_employee']].'</td>';
					
					echo '<td style="text-align:right">'.$permessi_residui[$employee['id_employee']].'</td>';
					echo '<td style="text-align:right">'.$certificati[$employee['id_employee']].'</td>';
					echo '</tr>';
				}
				
			}
			
		
			
			
			
			echo '</table>';
				
			echo '<br /><strong>* da fare entro il 31/12/2019</strong>';
		}
	
	
	
}


/*
			$year = 2015;
			
			$employees = Db::getInstance()->ExecuteS('
			SELECT `id_employee`, CONCAT(`firstname`, \' \', `lastname`) AS "name", lastname, firstname
			FROM `'._DB_PREFIX_.'employee`
			WHERE id_employee != 6 AND id_employee != 11
			ORDER BY (CASE id_employee WHEN 1 THEN 0 WHEN 2 THEN 1 WHEN 7 THEN 2 WHEN 4 THEN 3 WHEN 3 THEN 4 WHEN 5 THEN 6 WHEN 13 THEN 7 WHEN 12 THEN 8 END)');
			
			$ore_totali = array();
			$ferie_godute = array();
			$ferie_pianificate = array();
			$ferie_residue = array();
			$permessi_goduti = array();
			$permessi_pianificati = array();
			$permessi_residui = array();
			$certificati = array();
			
			$permessi_residui[2] = 60;
			$permessi_residui[7] = 55;
			$permessi_residui[4] = 26;
			$permessi_residui[3] = 29;
			$permessi_residui[5] = 25;
			
			$ferie_residue[2] = 27;
			$ferie_residue[7] = 19;
			$ferie_residue[4] = 23;
			$ferie_residue[3] = 20;
			$ferie_residue[5] = 23;
			*/
