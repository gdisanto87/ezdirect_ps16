<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14632 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminFatture extends AdminTab
{
	public function __construct()
	{
		$this->className = 'Fattura';
		$this->table = 'fattura';
		$this->_select = '
			(CASE c.is_company
			WHEN 0
			THEN CONCAT(c.firstname," ",c.lastname)
			WHEN 1
			THEN c.company
			ELSE
			CONCAT(c.firstname," ",c.lastname)
			END
			)as customer,
			c.`company` AS `company`,
			c.`id_default_group` AS `group`';
		
			$this->_join = 'LEFT JOIN `'._DB_PREFIX_.'customer` c ON (c.`id_customer` = a.`id_customer`)';
		$this->_group = 'GROUP BY id_storico';
		
		
	 		
		$groupArray = array('Tutto tranne estero' => 'Tutto tranne estero', 'Fattura cliente (accompagnatoria)' => 'Fattura cliente (accompagnatoria)', 'Fattura cliente (immediata)' => 'Fattura cliente (immediata)', 'Fattura cliente (differita)' => 'Fattura cliente (differita)', 'Fattura cli. estero accompagnatoria' => 'Fattura cli. estero accompagnatoria', 'Nota credito (immediata)' => 'Nota credito (immediata)');
		
		$this->delete = true;
		$this->colorOnBackground = true;
	 	
 		$this->fieldsDisplay = array(
		'id_fattura' => array('title' => $this->l('ID Fattura'),  'align' => 'center', 'width' => 40, 'widthColumn' => 70),
		'id_storico' => array('title' => $this->l('ID Storico'), 'align' => 'center', 'width' => 40, 'widthColumn' => 130),
		'tipo_fattura' => array('title' => $this->l('Tipo'),'type' => 'select', 'select' => $groupArray, 'align' => 'center', 'width' => 140, 'widthColumn' => 70, 'tmpTableFilter' => true, 'filter_key' => 'tipo_fattura' ),
		'id_customer' => array('title' => $this->l('ID cliente'), 'align' => 'center', 'width' => 40, 'widthColumn' => 70),
		'customer' => array('title' => $this->l('Customer'), 'widthColumn' => 340, 'width' => 320, 'filter_key' => 'customer', 'tmpTableFilter' => true),
		'group' => array('title' => $this->l('Group'), 'widthColumn' => 20, 'width' => 10, 'filter_key' => 'group', 'tmpTableFilter' => true),
		'data_fattura' => array('title' => $this->l('Data fattura'), 'type' => 'date', 'align' => 'center', 'width' => 80, 'widthColumn' => 50)
		
		);
		
		parent::__construct();
		
		
	}
	
	public function postProcess()
	{
		global $currentIndex, $cookie;
		$id_fattura = Tools::getValue('id_fattura');
		
		if (isset($_GET['updatefattura'])) {
		
			$id_customer = Tools::getValue('id_customer');
			$id_fattura = Tools::getValue('id_fattura');
			$datiFattura = Db::getInstance()->getRow("SELECT * FROM (SELECT * FROM ".(Tools::getIsset('tipo_fattura') && Tools::getValue('tipo_fattura') == 'nota' ? 'note_di_credito' : 'fattura')." WHERE id_fattura = '".$id_fattura."' AND iva_percent >= 0 AND id_customer = ".$id_customer." ORDER BY num_riga) t GROUP BY ".$id_fattura."");
		
			$numero_righe = Db::getInstance()->getValue("SELECT count(num_riga) AS tot FROM ".(Tools::getIsset('tipo_fattura') && Tools::getValue('tipo_fattura') == 'nota' ? 'note_di_credito' : 'fattura')." WHERE id_fattura = '".$id_fattura."' AND iva_percent >= 0 AND id_customer = ".$id_customer." ORDER BY num_riga");
		
			$datiCliente = Db::getInstance()->getRow("SELECT * FROM customer WHERE id_customer = ".$datiFattura['id_customer']."");
			$customer = new Customer($id_customer);
			$customerStats = $customer->getStats();
			$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
			
	
			echo '
			<script type="text/javascript" src="yetii.js"></script>
			<div id="tab-container-cart" style="background-color:#fffff !important">
			<ul id="tab-container-cart-nav" class="tab-containers-nav">
			<li><a href="#cart-1"><strong>Dati</strong></a></li>
			<li><a href="#cart-4"><strong>Storico</strong></a></li>
			</ul>
			<div class="yetii-cart" id="cart-1">
			';
			
			echo '<h2 style="float:left; margin-right:30px">
				
				'.(Validate::isLoadedObject($customer) ? ($customer->is_company == 1 ? $customer->company : $customer->firstname.' '.$customer->lastname).' - ' : '').''.(Tools::getIsset('tipo_fattura') && Tools::getValue('tipo_fattura') == 'nota' ? 'Nota di credito' : 'Fattura').' ID '.$id_fattura.' - '.date("d/m/Y", strtotime($datiFattura['data_fattura'])).' - '.($datiFattura['tipo'] == 'tecnotel' ? 'Tecnotel' : 'Ezdirect').'
			
			</h2>
			<script type="text/javascript" src="https://www.ezdirect.it/js/sweetalert2.js"></script>
			<link rel="stylesheet" href="https://www.ezdirect.it/css/sweetalert2.css" />
			
			
			
			<script type="text/javascript">
				$(document).ready(function(){
					 $("#invia_fattura_anagrafica").on("click",function(e){
						var valid = false;
						swal({
						  title: \'Invia fattura al cliente\',
						  text: \'\',
						  type: \'info\',
						 html:
						\'Il cliente riceverà la fattura via mail con questo testo (se vuoi puoi aggiungere delle note nella cella): Gentile cliente, cliccando qui [link] puoi scaricare la tua fattura n. '.$id_fattura.' in formato PDF.<br /><br />Email: <input id="swal-input1" class="swal2-input" value="'.$customer->email.'">\' +
						\'Note: <input id="swal-input2" class="swal2-input">\',
						  showCancelButton: true,
						  showLoaderOnConfirm: true,
						  confirmButtonColor: \'#6A9944\',
						  cancelButtonColor: \'#d33\',
						  confirmButtonText: \'Invia\',
							showLoaderOnConfirm: true,
						}).then(function (result) {
							$.ajax({
							  url:"ajax.php?id_customer='.$customer->id.'&viewcustomer&id_fattura='.$id_fattura.'&tab-container-1=5&viewfattura&tipo='.$datiFattura['tipo'].'&updatefattura&token='.$tokenCustomers.'&inviafatturamail&mail="+$(\'#swal-input1\').val()+"&note="+$(\'#swal-input2\').val(),
							  type: "GET",
							  success:function(){
								swal({
								type: \'success\',
								title: \'Fattura inviata con successo. Un record è stato salvato nello storico\'		
								});
								location.reload(); 								
							  },
							  error: function(xhr,stato,errori){
								 alert("Errore durante l\'operazione:"+xhr.status);
							  }
							});
													
						});
						
					});
				});
			</script>
			<div style="width:700px; float:left">
			<a href="index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&tab-container-1=1&token='.$tokenCustomers.'"><img src="../img/admin/employee.gif" alt="Apri scheda anagrafica" /> Apri scheda anagrafica</a> - 
			<a href="index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&viewmessage&aprinuovomessaggio&tab-container-1=7&token='.$tokenCustomers.'"><img src="../img/admin/email.gif" alt="Invia un messaggio al cliente" /> Invia un messaggio al cliente</a> -  <a href="../fattura.php?id_fattura='.$id_fattura.(Tools::getIsset('tipo_fattura') && Tools::getValue('tipo_fattura') == 'nota' ? '&tipo_fattura=nota' : '').'&id_customer='.$id_customer.'"  onclick="window.onbeforeunload = null"><img src="../img/admin/charged_ok.gif" alt="Scarica PDF" />Scarica PDF</a> - 
			<a href="javascript:void(0)" id="invia_fattura_anagrafica"> <img src="../img/admin/contact.gif" alt="Invia fattura al cliente" /> Invia fattura al cliente tramite email</a>
			</div>
			<div class="clear">&nbsp;</div>';
			
			$acquisti_totali = Db::getInstance()->getValue('SELECT SUM(imponibile) AS totale_acquisti FROM (SELECT * FROM '.(Tools::getIsset('tipo_fattura') && Tools::getValue('tipo_fattura') == 'nota' ? 'note_di_credito' : 'fattura').' WHERE id_customer = '.$customer->id.' GROUP BY id_fattura) f');
			
			if($customer->is_company == 0) {
		
				echo '<fieldset style="width: 90%">
				<legend><img src="../img/admin/tab-customers.gif" /> Informazioni sul cliente</legend>
				<table>
				<tr style="width:800px">
				<td style="width:100px"><em>Tipo cliente</em></td>
				<td style="width:300px"><em>Nome e cognome</em></td>
				<td style="width:200px"><em>Data creazione account</em></td>
				<td style="width:100px"><em>Ordini totali</em></td>
				<td style="width:100px"><em>Acquisti totali</em></td>
				</tr>
				<td><strong>PRIVATO</strong></td>
				<td>
					
				<span style="font-weight: bold; font-size: 14px;"><a href="?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'"> '.$customer->firstname.' '.$customer->lastname.'</a></span> (#'.$customer->id.')</td>';
				
			}
				
			else {		
		
				echo '<br /><fieldset style="width: 90%">
				<table>
					
				<legend><img src="../img/admin/tab-customers.gif" /> Informazioni sul cliente</legend>
				<table>
				<tr style="width:800px">
				<td style="width:100px"><em>Tipo cliente</em></td>
				<td style="width:300px"><em>Ragione sociale</em></td>
				<td style="width:200px"><em>Data creazione account</em></td>
				<td style="width:100px"><em>Ordini totali</em></td>
				<td style="width:100px"><em>Acquisti totali</em></td>
				</tr>
				<td><strong>AZIENDA</strong></td>
				<td><span style="font-weight: bold; font-size: 14px;"><a href="?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'"> '.$customer->company.'</a></span> (#'.$customer->id.')</td>';
				
				
						
			}
			echo '<td>'.Tools::displayDate($customer->date_add, (int)($cookie->id_lang), true).'</td>
			<td><b>'.$customerStats['nb_orders'].'</b></td>
			<td><b>'.Tools::displayPrice(Tools::ps_round(Tools::convertPrice($acquisti_totali, 1), 2), 1, false).'</b></td>';			
					
			echo '</tr></table></fieldset><div class="clear">&nbsp;</div>';
			
			$datafattura = date("d/m/Y", strtotime($datiFattura['data_fattura']));
			$numero_documento = explode('-', $datiFattura['id_fattura']);
			
			echo '<fieldset style="width: 90%">
			<legend><img src="../img/admin/tab-customers.gif" />Dati '.(Tools::getIsset('tipo_fattura') && Tools::getValue('tipo_fattura') == 'nota' ? 'nota' : 'fattura').'</legend>
			<table>
			<tr style="width:800px">
			<td style="width:100px"><em>ID '.(Tools::getIsset('tipo_fattura') && Tools::getValue('tipo_fattura') == 'nota' ? 'nota' : 'fattura').'</em></td>
			<td style="width:200px"><em>Codice cliente</em></td>
			<td style="width:200px"><em>Partita IVA - Codice fiscale</em></td>
			<td style="width:200px"><em>Numero documento</em></td>
			<td style="width:100px"><em>Data documento</em></td>
			</tr>
			<tr>
			<td>'.$id_fattura.'</td>
			<td>'.$datiFattura['codice_cliente'].'</td>
			<td>'.$customer->vat_number.' '.$customer->tax_code.'</td>
			<td>'.$numero_documento[0].'</td>
			<td>'.$datafattura.'</td>
			</tr>
			</table>
			<table>
			<tr style="width:800px">
			<td style="width:300px"><em>Modalit&agrave; di pagamento</em></td>
			<td style="width:200px"><em>Valuta</em></td>
			<td style="width:200px"><em>Rif. ordine sito</em></td>
			</tr>
			<tr>
			<td>'.$datiFattura['pagamento'].'</td>
			<td>EURO</td>
			<td>'.$datiFattura['rif_vs_ordine'].'</td>
			</tr>
			</table>
			</fieldset>
			
			';			
			
			
			
			
			$fatturazione = $datiFattura['dati_fatturazione'];
			$spedizione = $datiFattura['dati_spedizione'];
			$dati_fatturazione = explode('--', $fatturazione);
			$dati_spedizione = explode('--', $spedizione);
			echo '<div class="clear">&nbsp;</div>
			<div style="float: left">
			<fieldset style="width: 380px;">
			<legend><img src="../img/admin/invoice.gif" alt="Indirizzo di fatturazione" />Indirizzo di fatturazione</legend>
			<table>
			<tr><td style="width:100px">Spett.le</td><td><strong>'.$dati_fatturazione[0].'</strong></td></tr>
			<tr><td style="width:100px">Indirizzo</td><td>'.$dati_fatturazione[1].'</td></tr>
			<tr><td style="width:100px">CAP</td><td>'.$dati_fatturazione[2].'</td></tr>
			<tr><td style="width:100px">Citt&agrave;</td><td>'.$dati_fatturazione[3].'</td></tr>
			<tr><td style="width:100px">Provincia</td><td>'.$dati_fatturazione[4].'</td></tr>
			</table>
			</fieldset>
			</div>
			<div style="float: left; margin-left: 40px">
	
			<fieldset style="width: 380px;">
			<legend><img src="../img/admin/delivery.gif" alt="Destinazione" />Destinazione</legend>
			
			<table>
			<tr><td style="width:100px">Spett.le</td><td><strong>'.$dati_spedizione[0].'</strong></td></tr>
			<tr><td style="width:100px">Indirizzo</td><td>'.$dati_spedizione[1].'</td></tr>
			<tr><td style="width:100px">CAP</td><td>'.$dati_spedizione[2].'</td></tr>
			<tr><td style="width:100px">Citt&agrave;</td><td>'.$dati_spedizione[3].'</td></tr>
			<tr><td style="width:100px">Provincia</td><td>'.$dati_spedizione[4].'</td></tr>
			</table>
			</fieldset>
						
			</div>
			<div class="clear">&nbsp;</div>';
		
			$prodottiFattura = Db::getInstance()->executeS("SELECT * FROM ".(Tools::getIsset('tipo_fattura') && Tools::getValue('tipo_fattura') == 'nota' ? 'note_di_credito' : 'fattura')." WHERE id_fattura = '".$id_fattura."' AND id_customer = ".$id_customer." ORDER BY num_riga");
			$riferimento_ordine = explode(' ', $datiFattura['rif_ordine']);
			$num_prod = 0;
			
			echo '<fieldset style="width: 90%; ">
				<legend><img src="../img/admin/cart.gif" alt="Prodotti" />Prodotti</legend>
				<div style="float:left;">
					<table cellspacing="0" cellpadding="0" class="table" id="orderProducts">
						<tr>
							<th style="width:100px">Cod. articolo</th>
							<th style="width: 226px; text-align: left">Desc. articolo</th>
							<th style="width: 40px; text-align: left">UM</th>
							<th style="width: 58px; text-align: right">Qta</th>
							<th style="width: 58px; text-align: right">Rif. ord</th>
							<th style="width: 58px; text-align: right">Prezzo</th>
							<th style="width: 58px; text-align: right">Sconti</th>
							<th style="width: 58px; text-align: right">Importo</th>
							<th style="width: 42px; text-align: right">C.I.</th>
							</tr>';
							
							$tokenCatalog = Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee));
							
			foreach($prodottiFattura as $rigaFattura) {
			
				$num_prod++;
				
				if($num_prod > 9999999) {
				}
				else {
				
					if(empty($rigaFattura['desc_articolo']))  {
					
					}
					
					
					else {
					
						$id_prodotto = Db::getInstance()->getValue('SELECT id_product FROM product WHERE reference LIKE "%'.$rigaFattura['cod_articolo'].'%"');
						echo '
							<tr style="font-size:10px;">
							<td style="width:100px"><span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($id_prodotto).'"> <a href="index.php?tab=AdminCatalog&id_product='.$id_prodotto.'&updateproduct&token='.$tokenCatalog.'">'.$rigaFattura['cod_articolo'].'</a></span></td>
							<td style="width:226px">'.$rigaFattura['desc_articolo'].'</td>
							<td style="width:40px;">'.($rigaFattura['qt_sped'] == 0 ? '' : 'N.').'</td>
							<td style="width:58px; text-align:right">'.($rigaFattura['qt_sped'] == 0 ? '' : $rigaFattura['qt_sped']).'</td>
							<td style="width:58px; text-align:right">'.$riferimento_ordine[0].'</td>
							<td style="width:58px; text-align:right">'.($rigaFattura['qt_sped'] == 0 ? '' : number_format($rigaFattura['prezzo'],2,',','')).'</td>
							<td style="width:58px; text-align:right">'.($rigaFattura['sconto1'] != 0 ? number_format($rigaFattura['sconto1'],2,',','') : '').'</td>
							<td style="width:58px; text-align:right">'.($rigaFattura['qt_sped'] == 0 ? '' : number_format($rigaFattura['importo_netto'],2,',','')).'</td>
							<td style="width:42px; text-align:right">'.($rigaFattura['qt_sped'] == 0 ? '' : (int)$rigaFattura['iva_percent']).'</td>
							</tr>
						';
					}
				}
			}

			echo '</table></div></fieldset><div class="clear">&nbsp;</div>';

			echo '<fieldset style="width: 90%">
			<legend><img src="../img/admin/tab-customers.gif" />Totali</legend>
			<table>
			<tr style="width:800px">
			<td style="width:160px; text-align:right"><em>Tot. merce</em></td>
			<td style="width:160px; text-align:right"><em>C.IVA</em></td>
			<td style="width:160px; text-align:right"><em>Imponibile</em></td>
			<td style="width:160px; text-align:right"><em>IVA</em></td>
			<td style="width:160px; text-align:right"><em>Totale documento</em></td>
			</tr>
			<tr>
			<td style="width:160px; text-align:right">'.number_format($datiFattura['imponibile'],2,',','').' &euro;</td>
			<td style="width:160px; text-align:right">'.number_format($datiFattura['iva_percent'],0,',','').'%</td>
			<td style="width:160px; text-align:right">'.number_format($datiFattura['imponibile'],2,',','').' &euro;</td>
			<td style="width:160px; text-align:right">'.number_format($datiFattura['iva'],2,',','').' &euro;</td>
			<td style="width:160px; text-align:right">'.number_format($datiFattura['totale_fattura'],2,',','').' &euro;</td>
			</tr>
			</table>
			</fieldset><div class="clear">&nbsp;</div>
			
			';		
			
			
			echo '<fieldset style="width: 90%">
			<legend><img src="../img/admin/tab-customers.gif" />Trasporto</legend>
			<table>
			<tr style="width:800px">
			<td style="width:200px"><em>Causale trasporto</em></td>
			<td style="width:200px"><em>Trasporto a cura</em></td>
			<td style="width:400px"><em>Vettore</em></td>
			</tr>
			<tr>
			<td>'.$datiFattura['causale_trasporto'].'</td>
			<td>Vettore</td>
			<td>'.$datiFattura['dati_vettore'].'</td>
			</tr>
			</table>
			<table>
			<tr style="width:800px">
			<td style="width:200px"><em>Porto</em></td>
			<td style="width:200px"><em>N. Colli</em></td>
			<td style="width:200px"><em>Peso</em></td>
			<td style="width:200px"><em>Data e ora trasporto</em></td>
			</tr>
			<tr>
			<td>'.$datiFattura['desc_porto'].'</td>
			<td>'.$datiFattura['num_colli'].'</td>
			<td>'.number_format($datiFattura['peso'],2,',','').'</td>
			<td>'.$datafattura.'</td>
			</tr>
			</table>
			</fieldset>
			</div>
			<div id="cart-4" class="yetii-cart" style="display:block">';
			
			$id_storico = Db::getInstance()->getValue('SELECT id_riga FROM '.(Tools::getIsset('tipo_fattura') && Tools::getValue('tipo_fattura') == 'nota' ? 'note_di_credito' : 'fattura').' WHERE id_fattura = "'.Tools::getValue('id_fattura').'" ORDER BY id_riga ASC');
			
			
			$storico_fatt = Db::getInstance()->executeS('SELECT * FROM storico_attivita WHERE id_attivita = '.$id_storico.' AND tipo_attivita = "F" ORDER BY data_attivita DESC, desc_attivita DESC');
				
				if(sizeof($storico_fatt) > 0)
				{
					echo '<table class="table">';
					echo '<thead><tr><th>Persona</th><th>Azione</th><th>Data</th></tr></thead><tbody>';
					
					foreach($storico_fatt as $storico)
					{
						if(is_numeric(substr($storico['desc_attivita'], -1)))
						{	
							$employee = filter_var($storico['desc_attivita'], FILTER_SANITIZE_NUMBER_INT);
							$name_employee = Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM employee WHERE id_employee = '.$employee);
							$desc_attivita = 'Ha messo l\'attivit&agrave; in carico a '.$name_employee;
						}
						else
							$desc_attivita = $storico['desc_attivita'];
						
						echo '<tr><td '.($storico['id_employee'] == 0 ? 'style="color:red; font-weight:bold"' : '').'>'.($storico['id_employee'] == 0 ? 'Cliente' : Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM employee WHERE id_employee = '.$storico['id_employee'])).'</td><td '.($storico['id_employee'] == 0 ? 'style="color:red; font-weight:bold"' : '').'>'.$desc_attivita.'</td><td '.($storico['id_employee'] == 0 ? 'style="color:red; font-weight:bold"' : '').'>'.Tools::displayDate($storico['data_attivita'],$cookie->id_lang, true).'</td></tr>'; 
						
					}	
					
					echo '</tbody></table><br /><br />';
				}
				
				
				
				echo '</div>'; // chiudo cart 4
				
				echo '
				<script type="text/javascript">
				var tabber2 = new Yetii({
				id: "tab-container-cart",
				tabclass: "yetii-cart",
				});
				</script>';
		
				//Tools::redirectAdmin('../fattura.php?id_fattura='.$id_fattura.'&id_customer='.$id_customer.'');
		} else if (isset($_GET['cancella'.$this->table])) {
		
		
			Db::getInstance()->executeS("DELETE FROM ".(Tools::getIsset('tipo_fattura') && Tools::getValue('tipo_fattura') == 'nota' ? 'note_di_credito' : 'fattura')." WHERE id_fattura = '".Tools::getValue('id_fattura')."'");
			Tools::redirectAdmin($currentIndex.'&conf=1&token='.$tokenfatture);
		}
		parent::postProcess();
		
	}

	public function display()
	{
		global $cookie;
		
		if (!Tools::isSubmit('update'.$this->table) && !Tools::isSubmit('cancella'.$this->table)) {
			$this->getList((int)($cookie->id_lang), !Tools::getValue($this->table.'Orderby') ? 'data_fattura DESC, id_fattura ' : NULL, !Tools::getValue($this->table.'Orderway') ? 'DESC' : NULL);
			$currency = new Currency((int)(Configuration::get('PS_CURRENCY_DEFAULT')));
			$this->displayListHeader();
			$this->displayListContent();
			$this->displayListPagination($this->token);
		}
	}

	protected function _displayDeleteLink($token = NULL, $id)
	{
		global $currentIndex;

		$_cacheLang['Delete'] = $this->l('Delete');
		$_cacheLang['DeleteItem'] = $this->l('Delete item #', __CLASS__, TRUE, FALSE);

		echo '
			<a href="'.$currentIndex.'&'.$this->identifier.'='.$id.'&cancella'.$this->table.'&token='.($token!=NULL ? $token : $this->token).'" onclick="return confirm(\''.$_cacheLang['DeleteItem'].$id.' ?'.
					(!is_null($this->specificConfirmDelete) ? '\r'.$this->specificConfirmDelete : '').'\');">
			<img src="../img/admin/delete.gif" alt="'.$_cacheLang['Delete'].'" title="'.$_cacheLang['Delete'].'" /></a>';
	}
	
	public function displayListContent($token = NULL)
	{
		/* Display results in a table
		 *
		 * align  : determine value alignment
		 * prefix : displayed before value
		 * suffix : displayed after value
		 * image  : object image
		 * icon   : icon determined by values
		 * active : allow to toggle status
		 */

		global $currentIndex, $cookie;
		$currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));

		$id_category = 1; // default categ

		$irow = 0;
		if ($this->_list AND isset($this->fieldsDisplay['position']))
		{
			$positions = array_map(create_function('$elem', 'return (int)($elem[\'position\']);'), $this->_list);
			sort($positions);
		}
		if ($this->_list)
		{
			$isCms = false;
			if (preg_match('/cms/Ui', $this->identifier))
				$isCms = true;
			$keyToGet = 'id_'.($isCms ? 'cms_' : '').'category'.(in_array($this->identifier, array('id_category', 'id_cms_category')) ? '_parent' : '');
			$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
			
			foreach ($this->_list AS $tr)
			{
				$id = $tr[$this->identifier];
				echo '<tr'.(array_key_exists($this->identifier,$this->identifiersDnd) ? ' id="tr_'.(($id_category = (int)(Tools::getValue('id_'.($isCms ? 'cms_' : '').'category', '1'))) ? $id_category : '').'_'.$id.'_'.$tr['position'].'"' : '').($irow++ % 2 ? ' class="alt_row"' : '').' '.((isset($tr['color']) AND $this->colorOnBackground) ? 'style="background-color: '.$tr['color'].'"' : '').'>
							<td class="center">';
				if ($this->delete AND (!isset($this->_listSkipDelete) OR !in_array($id, $this->_listSkipDelete)))
					echo '<input type="checkbox" name="'.$this->table.'Box[]" value="'.$id.'" class="noborder" />';
				echo '</td>';
				foreach ($this->fieldsDisplay AS $key => $params)
				{
					$tmp = explode('!', $key);
					$key = isset($tmp[1]) ? $tmp[1] : $tmp[0];
					echo '
					<td '.(isset($params['position']) ? ' id="td_'.(isset($id_category) AND $id_category ? $id_category : 0).'_'.$id.'"' : '').' class="'.((!isset($this->noLink) OR !$this->noLink) ? 'pointer' : '').((isset($params['position']) AND $this->_orderBy == 'position')? ' dragHandle' : ''). (isset($params['align']) ? ' '.$params['align'] : '').'" ';
					if (!isset($params['position']) AND (!isset($this->noLink) OR !$this->noLink))
					echo ' onclick="document.location = \'index.php?tab=AdminCustomers&id_customer='.$tr['id_customer'].'&viewcustomer&'.$this->identifier.'='.$id.($this->view? '&view' : '&view').$this->table.'&updatefattura&tipo_fattura='.($tr['tipo_fattura'] == 'Nota credito (immediata)' ? 'nota' : 'fattura').'&tipo='.$tr['tipo'].'&tab-container-1=5&token='.$tokenCustomers.'\'">'.(isset($params['prefix']) ? $params['prefix'] : '');
					else
						echo '>';
					if (isset($params['active']) AND isset($tr[$key]))
						$this->_displayEnableLink($token, $id, $tr[$key], $params['active'], Tools::getValue('id_category'), Tools::getValue('id_product'));
					elseif (isset($params['activeVisu']) AND isset($tr[$key]))
						echo '<img src="../img/admin/'.($tr[$key] ? 'enabled.gif' : 'disabled.gif').'"
						alt="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" title="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" />';
					elseif (isset($params['position']))
					{
						if ($this->_orderBy == 'position' AND $this->_orderWay != 'DESC')
						{
							echo '<a'.(!($tr[$key] != $positions[sizeof($positions) - 1]) ? ' style="display: none;"' : '').' href="'.$currentIndex.
									'&'.$keyToGet.'='.(int)($id_category).'&'.$this->identifiersDnd[$this->identifier].'='.$id.'
									&way=1&position='.(int)($tr['position'] + 1).'&token='.($token!=NULL ? $token : $this->token).'">
									<img src="../img/admin/'.($this->_orderWay == 'ASC' ? 'down' : 'up').'.gif"
									alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>';

							echo '<a'.(!($tr[$key] != $positions[0]) ? ' style="display: none;"' : '').' href="'.$currentIndex.
									'&'.$keyToGet.'='.(int)($id_category).'&'.$this->identifiersDnd[$this->identifier].'='.$id.'
									&way=0&position='.(int)($tr['position'] - 1).'&token='.($token!=NULL ? $token : $this->token).'">
									<img src="../img/admin/'.($this->_orderWay == 'ASC' ? 'up' : 'down').'.gif"
									alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a>';						}
						else
							echo (int)($tr[$key] + 1);
					}
					elseif (isset($params['image']))
					{
						// item_id is the product id in a product image context, else it is the image id.
						$item_id = isset($params['image_id']) ? $tr[$params['image_id']] : $id;
						// If it's a product image
						if (isset($tr['id_image']))
						{
							$image = new Image((int)$tr['id_image']);
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$image->getExistingImgPath().'.'.$this->imageType;
						}else
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$item_id.(isset($tr['id_image']) ? '-'.(int)($tr['id_image']) : '').'.'.$this->imageType;

						echo cacheImage($path_to_image, $this->table.'_mini_'.$item_id.'.'.$this->imageType, 45, $this->imageType);
					}
					elseif (isset($params['icon']) AND (isset($params['icon'][$tr[$key]]) OR isset($params['icon']['default'])))
						echo '<img src="../img/admin/'.(isset($params['icon'][$tr[$key]]) ? $params['icon'][$tr[$key]] : $params['icon']['default'].'" alt="'.$tr[$key]).'" title="'.$tr[$key].'" />';
					elseif (isset($params['price']))
						echo Tools::displayPrice($tr[$key], (isset($params['currency']) ? Currency::getCurrencyInstance((int)($tr['id_currency'])) : $currency), false);
					elseif (isset($params['float']))
						echo rtrim(rtrim($tr[$key], '0'), '.');
					elseif (isset($params['type']) AND $params['type'] == 'date')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang);
					elseif (isset($params['type']) AND $params['type'] == 'datetime')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang, true);
					elseif (isset($tr[$key]))
					{
						$echo = ($key == 'price' ? round($tr[$key], 2) : isset($params['maxlength']) ? Tools::substr($tr[$key], 0, $params['maxlength']).'...' : $tr[$key]);
						echo isset($params['callback']) ? call_user_func_array(array($this->className, $params['callback']), array($echo, $tr)) : $echo;
					}
					else
						echo '--';

					echo (isset($params['suffix']) ? $params['suffix'] : '').
					'</td>';
				}

				if ($this->edit OR $this->delete OR ($this->view AND $this->view !== 'noActionColumn'))
				{
					echo '<td class="center" style="white-space: nowrap;">';
					if ($this->view)
						$this->_displayViewLink($token, $id);
					if ($this->edit)
						$this->_displayEditLink($token, $id);
					if ($this->delete AND (!isset($this->_listSkipDelete) OR !in_array($id, $this->_listSkipDelete)))
						$this->_displayDeleteLink($token, $id);
					if ($this->duplicate)
						$this->_displayDuplicate($token, $id);
					echo '</td>';
				}
				echo '</tr>';
			}
		}
	}

	public function getList($id_lang, $orderBy = NULL, $orderWay = NULL, $start = 0, $limit = NULL)
	{
		global $cookie;

		/* Manage default params values */
		if (empty($limit))
			$limit = ((!isset($cookie->{$this->table.'_pagination'})) ? $this->_pagination[1] : $limit = $cookie->{$this->table.'_pagination'});

		if (!Validate::isTableOrIdentifier($this->table))
			die (Tools::displayError('Table name is invalid:').' "'.$this->table.'"');

		if (empty($orderBy))
			$orderBy = $cookie->__get($this->table.'Orderby') ? $cookie->__get($this->table.'Orderby') : $this->_defaultOrderBy;
		if (empty($orderWay))
			$orderWay = $cookie->__get($this->table.'Orderway') ? $cookie->__get($this->table.'Orderway') : 'ASC';

		$limit = (int)(Tools::getValue('pagination', $limit));
		$cookie->{$this->table.'_pagination'} = $limit;

		/* Check params validity */
		/*if (!Validate::isOrderBy($orderBy) OR !Validate::isOrderWay($orderWay)
			OR !is_numeric($start) OR !is_numeric($limit)
			OR !Validate::isUnsignedId($id_lang))
			die(Tools::displayError('get list params is not valid'));
		*/
		/* Determine offset from current page */
		if ((isset($_POST['submitFilter'.$this->table]) OR
		isset($_POST['submitFilter'.$this->table.'_x']) OR
		isset($_POST['submitFilter'.$this->table.'_y'])) AND
		!empty($_POST['submitFilter'.$this->table]) AND
		is_numeric($_POST['submitFilter'.$this->table]))
			$start = (int)($_POST['submitFilter'.$this->table] - 1) * $limit;

		/* Cache */
		$this->_lang = (int)($id_lang);
		$this->_orderBy = $orderBy;
		$this->_orderWay = Tools::strtoupper($orderWay);

		/* SQL table : orders, but class name is Order */
		$sqlTable = $this->table == 'order' ? 'orders' : $this->table;

		/* Query in order to get results with all fields */
		$sql = 'SELECT SQL_CACHE SQL_CALC_FOUND_ROWS
			'.($this->_tmpTableFilter ? ' * FROM (SELECT ' : '').'
			'.($this->lang ? 'b.*, ' : '').'a.*'.(isset($this->_select) ? ', '.$this->_select.' ' : '').'
			FROM `'._DB_PREFIX_.$sqlTable.'` a
			'.($this->lang ? 'LEFT JOIN `'._DB_PREFIX_.$this->table.'_lang` b ON (b.`'.$this->identifier.'` = `'.$this->identifier.'` AND b.`id_lang` = '.(int)($id_lang).')' : '').'
			'.(isset($this->_join) ? $this->_join.' ' : '').'
			WHERE 1 '.(isset($this->_where) ? $this->_where.' ' : '').($this->deleted ? 'AND `deleted` = 0 ' : '').(isset($this->_filter) ? $this->_filter : '').'
			'.(isset($this->_group) ? $this->_group.' ' : '').'
			 UNION SELECT n.*, (CASE c.is_company WHEN 0 THEN CONCAT(c.firstname," ",c.lastname) WHEN 1 THEN c.company ELSE CONCAT(c.firstname," ",c.lastname) END )as customer, c.`company` AS `company`, c.`id_default_group` AS `group` FROM `note_di_credito` n LEFT JOIN `customer` c ON (c.`id_customer` = n.`id_customer`) WHERE 1 '.$this->_where.' '.$this->_filter.' GROUP BY id_storico
			
			'.((isset($this->_filterHaving) || isset($this->_having)) ? 'HAVING ' : '').(isset($this->_filterHaving) ? ltrim($this->_filterHaving, ' AND ') : '').(isset($this->_having) ? $this->_having.' ' : '').'
			ORDER BY '.(($orderBy == $this->identifier) ? '' : '').' '.pSQL($orderBy).' '.pSQL($orderWay).
			($this->_tmpTableFilter ? ') tmpTable WHERE 1'.$this->_tmpTableFilter.' ORDER BY '.(($orderBy == $this->identifier) ? '' : '').' '.pSQL($orderBy).' '.pSQL($orderWay).' ' : '').' 
			LIMIT '.(int)($start).','.(int)($limit);
			
			$sql = str_replace("`tipo_fattura` = 'Tutto tranne estero' ", " (`tipo_fattura` = 'Fattura cliente (accompagnatoria)' OR `tipo_fattura` = 'Fattura cliente (differita)' OR `tipo_fattura` = 'Fattura cliente (immediata)' OR `tipo_fattura` = 'Nota credito (immediata)') ", $sql);
			$sql = str_replace("id_fattura desc", "id_storico DESC", $sql);
			$sql = str_replace("id_fattura DESC", "id_storico DESC", $sql);
			$sql = str_replace("id_fattura ASC", "id_storico ASC", $sql);
			$sql = str_replace("id_fattura asc", "id_storico ASC", $sql);
			
		$this->_list = Db::getInstance()->ExecuteS($sql);
		$this->_listTotal = Db::getInstance()->getValue('SELECT FOUND_ROWS() AS `'._DB_PREFIX_.$this->table.'`');

	}
	
}


