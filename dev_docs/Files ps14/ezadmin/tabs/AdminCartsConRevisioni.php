<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/../classes/AdminTab.php');

class AdminCarts extends AdminTab
{

	public function bindDatepickerZ($id, $time)
	{
		if ($time)
			echo '
			var dateObj = new Date();
			var hours = dateObj.getHours();
			var mins = dateObj.getMinutes();
			var secs = dateObj.getSeconds();
			if (hours < 10) { hours = "0" + hours; }
			if (mins < 10) { mins = "0" + mins; }
			if (secs < 10) { secs = "0" + secs; }
			var time = " "+hours+":"+mins+":"+secs;';

		echo '
		$(function() {
			$("#'.Tools::htmlentitiesUTF8($id).'").datepicker({
				prevText:"",
				nextText:"",
				dateFormat:"dd-mm-yy"});
		});';
	}

	// id can be a identifier or an array of identifiers
	public function includeDatepickerZ($id, $time = false)
	{
		global $cookie;
		echo '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery-ui-1.8.10.custom.min.js"></script>';
		$iso = Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'lang WHERE `id_lang` = '.(int)$cookie->id_lang);
		if ($iso != 'en')
			echo '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/datepicker/ui/i18n/ui.datepicker-'.Tools::htmlentitiesUTF8($iso).'.js"></script>';
		echo '<script type="text/javascript">';
			if (is_array($id))
				foreach ($id as $id2)
					AdminCarts::bindDatepickerZ($id2, $time);
			else
				AdminCarts::bindDatepickerZ($id, $time);
		echo '</script>';
	}
	
	public function __construct()
	{
		global $cookie;
	 	$this->table = 'cart';
	 	$this->className = 'Cart';
		$this->lang = false;
	 	$this->edit = false;
	 	$this->view = true;
	 	$this->delete = true;

		$this->_select = '(CASE c.is_company WHEN 0 THEN CONCAT(LEFT(c.`firstname`, 1), \'. \', c.`lastname`)
		WHEN 1 THEN c.company
		ELSE CONCAT(LEFT(c.`firstname`, 1), \'. \', c.`lastname`)
		END
		)  `customer`, 
		(SELECT firstname FROM employee WHERE id_employee = a.created_by) `created`, 
		(SELECT firstname FROM employee WHERE id_employee = a.in_carico_a) `in_carico`, 
		(CASE a.preventivo WHEN 0 THEN 0 ELSE (CASE WHEN o.id_order IS NULL THEN 0 ELSE 1 END) END) `preventivo_convertito`,
		a.id_cart total, ca.name carrier, o.id_order, 
		(CASE WHEN o.id_order IS NULL THEN 0 ELSE 1 END) `convertito`,
		(CASE a.preventivo WHEN 0 THEN "--" ELSE a.visualizzato END) `letto`
		';
		
		$this->_join = 'LEFT JOIN '._DB_PREFIX_.'customer c ON (c.id_customer = a.id_customer)
		LEFT JOIN '._DB_PREFIX_.'currency cu ON (cu.id_currency = a.id_currency)
		LEFT JOIN '._DB_PREFIX_.'carrier ca ON (ca.id_carrier = a.id_carrier)
		LEFT JOIN '._DB_PREFIX_.'orders o ON (o.id_cart = a.id_cart) ';
		
		if(isset($_POST['cercacarrelloprodotto'])) {
			
			$cookie->_wherecart = 'AND a.id_cart IN (SELECT id_cart FROM cart_product WHERE id_product = "'.$_POST['cercacarrelloprodotto'].'") AND a.id_customer != 0';
			$cookie->_cercacarrelloprodotto = $_POST['cercacarrelloprodotto'];
		}
		
		if(isset($cookie->_wherecart)) {
		
			$this->_where = $cookie->_wherecart;
		
		}
		
		if(isset($_POST['submitResetcart'])) {
		
			$cookie->_wherecart = "";
			$cookie->_cercacarrelloprodotto = "";
			$this->_where = $cookie->_wherecart;
			unset($cookie->_wherecart);
		}
		
		$the_employees = Db::getInstance()->ExecuteS('
		SELECT `id_employee`, firstname AS "name"
		FROM `'._DB_PREFIX_.'employee`
		WHERE `active` = 1
		ORDER BY `email`');

		$contactEmployees = array();
		foreach($the_employees as $employee) 
			$contactEmployees[$employee['name']] = $employee['name'];

		$imagesArray = array(
			'--' => 'lineette.gif',
			'1' => 'enabled.gif',
			'0' => 'red_no.gif'
		);
		
 		$this->fieldsDisplay = array(
			'id_cart' => array('title' => AdminCarts::lx('ID'), 'align' => 'center', 'width' => 25, 'widthColumn' => 30),
			//'preventivo_convertito' => array('title' => AdminCarts::lx('Conv.?'), 'width' => 25, 'active' => 'status', 'type' => 'bool', 'orderby' => false, 'tmpTableFilter' => true),
			'preventivo' => array('title' => AdminCarts::lx('Prev.?'), 'width' => 25, 'align' => 'center', 'icon' => $imagesArray, 'type' => 'bool', 'orderby' => false, 'tmpTableFilter' => true, 'widthColumn' => 30),
			'convertito' => array('title' => AdminCarts::lx('Conv.?'), 'width' => 25, 'align' => 'center', 'icon' => $imagesArray, 'type' => 'bool', 'orderby' => false, 'tmpTableFilter' => true, 'widthColumn' => 30),
			'letto' => array('title' => AdminCarts::lx('Letto?'), 'width' => 25, 'align' => 'center', 'icon' => $imagesArray, 'type' => 'bool', 'orderby' => false, 'tmpTableFilter' => true, 'widthColumn' => 30),
			'id_order' => array('title' => AdminCarts::lx('N. ORD.'), 'align' => 'center', 'font-weight' => 'bold', 'width' => 25, 'widthColumn' => 30),
			'customer' => array('title' => AdminCarts::lx('Customer'), 'width' => 65, 'filter_key' => 'customer', 'tmpTableFilter' => true, 'widthColumn' => 150),
			'name' => array('title' => AdminCarts::lx('Oggetto'), 'align' => 'left', 'width' => 30, 'filter_key' => 'name', 'search' => true, 'tmpTableFilter' => true, 'widthColumn' => 90),
			'total' => array('title' => AdminCarts::lx('Total'), 'callback' => 'getOrderTotalUsingTaxCalculationMethod', 'orderby' => false, 'search' => true, 'width' => 60, 'align' => 'right', 'prefix' => '<b>', 'suffix' => '</b>', 'currency' => true, 'widthColumn' => 70),
			'created' => array('title' => AdminCarts::lx('Creato da'), 'width' => 55, 'type' => 'select', 'select' => $contactEmployees, 
			'filter_key' => 'created', 'search' => true, 'tmpTableFilter' => true, 'widthColumn' => 65),
			'in_carico' => array('title' => AdminCarts::lx('In carico a'), 'width' => 55, 'type' => 'select', 'select' => $contactEmployees, 'filter_key' => 'in_carico', 'search' => true, 'tmpTableFilter' => true, 'widthColumn' => 65),
			
			
			'date_add' => array('title' => AdminCarts::lx('Date'), 'width' => 60, 'align' => 'right', 'type' => 'date', 'filter_key' => 'a!date_add', 'widthColumn' => 70));

		parent::__construct();
		
		if(isset($_GET['deletecart'])) {
			Db::getInstance()->executeS("DELETE FROM noleggio WHERE id_cart = ".$_GET['id_cart']."");
		
		}
		
		if(isset($_GET['convertcart'])) {
		
			include("../modules/pss_clearcarts/pss_clearcarts.php");
			include("../modules/pss_clearcarts/AdminPssClearCarts.php");
			$apcc = new AdminPssClearCarts();
			$apcc->orderThisCart(Tools::getValue('id_cart'));
			$order = Db::getInstance()->getValue('SELECT id_order FROM orders WHERE id_cart = '.Tools::getValue('id_cart').'');
			$tokenOrders = Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee);
			Tools::redirectAdmin('?tab=AdminOrders&id_order='.$order.'&vieworder&token='.$tokenOrders);
			
		}
		
		if(isset($_GET['deleteccart'])) {
			Db::getInstance()->executeS("DELETE FROM cart WHERE id_cart = ".$_GET['id_cart']."");
			Db::getInstance()->executeS("DELETE FROM noleggio WHERE id_cart = ".$_GET['id_cart']."");
			Db::getInstance()->executeS("DELETE FROM cart_product WHERE id_cart = ".$_GET['id_cart']."");
			
		
			global $cookie;
			
			Tools::redirectAdmin("index.php?tab=AdminCustomers&id_customer=".$_GET['id_customer']."&viewcustomer&token=".Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee))."&tab-container-1=4");
			
		}
	}

	public function viewDetails()
	{
		global $currentIndex, $cookie;
		
		if(isset($_GET['getPDF'])) {
		
			require_once('../classes/html2pdf/html2pdf.class.php');
			
			$id_cst = Db::getInstance()->getValue("SELECT id_customer FROM cart WHERE id_cart = ".$_GET['id_cart']."");
			if(Tools::getIsset('revisione'))
				$content = Cart::getCartPDF($_GET['id_cart'], $id_cst, 'y', Tools::getValue('revisione'));
			else
				$content = Cart::getCartPDF($_GET['id_cart'], $id_cst, 'y');
			ob_clean();
			
			$html2pdf = new HTML2PDF('P','A4','it');
			$html2pdf->WriteHTML($content);
			if(Tools::getValue('originale') == 'y') {
				$html2pdf->Output($id_cst.'-offerta-'.$_GET['id_cart'].'-originale.pdf', 'D'); 
			}
			else {
				if(Tools::getIsset('revisione'))
					$html2pdf->Output($id_cst.'-offerta-'.$_GET['id_cart'].'-revisione-'.$_GET['revisione'].'.pdf', 'D'); 
				else
					$html2pdf->Output($id_cst.'-offerta-'.$_GET['id_cart'].'.pdf', 'D'); 
			}
		}
		
		if (isset($_GET['filename'])) {
			$filename = $_GET['filename'];
						
			if(strpos($filename, ":::")) {
			
				$parti = explode(":::", $filename);
				$nomecodificato = $parti[0];
				$nomevero = $parti[1];
							
			}
			
			else {
				$nomecodificato = $filename;
				$nomevero = $filename;
			
			}
						
			if (file_exists(_PS_UPLOAD_DIR_.$nomecodificato)) {
				AdminCarts::openUploadedFile();
			}
			else { }
		} 
		else { }
		
		
		if(isset($_GET['createnew'])) {
			
			$getLastCart = Db::getInstance()->getValue("SELECT id_cart FROM cart ORDER BY id_cart DESC");
			$newCart = $getLastCart+1;
			$address_fatturazione = Db::getInstance()->getValue("SELECT id_address FROM address WHERE customer = ".$_GET['id_customer']." AND fatturazione = 1 AND deleted = 0 AND active = 1");
				
			Db::getInstance()->executeS("INSERT INTO cart (id_cart, id_carrier, id_lang, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, date_add, date_upd, created_by, id_employee, preventivo, provvisorio) VALUES (
			'".$newCart."', 0, 5, '".$address_fatturazione."', '".$address_fatturazione."', 1, '".$_GET['id_customer']."', 0, '', 0,0,'', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."', ".$cookie->id_employee.", ".$cookie->id_employee.", ".(Tools::getIsset('preventivo') ? (Tools::getValue('preventivo') == 2 ? 0 : Tools::getValue('preventivo')) : Tools::getValue('preventivo')).", 1)");
				
			Db::getInstance()->executeS("INSERT INTO carrelli_creati (id_cart, id_carrier, id_lang, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, date_add, date_upd, created_by, id_employee, preventivo, provvisorio) VALUES (
			'".$newCart."', 0, 5, '".$address_fatturazione."', '".$address_fatturazione."', 1, '".$_GET['id_customer']."', 0, '', 0,0,'', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."', ".$cookie->id_employee.", ".$cookie->id_employee.", ".(Tools::getIsset('preventivo') ? (Tools::getValue('preventivo') == 2 ? 0 : Tools::getValue('preventivo')) : Tools::getValue('preventivo')).", 1)");
			
		
			if(Tools::getIsset('rif_prev')) 
			{
				Db::getInstance()->executeS("UPDATE form_prevendita_thread SET status='pending1' WHERE id_thread = ".Tools::getValue('rif_prev')."");
				Db::getInstance()->executeS("UPDATE cart SET esigenze = '".addslashes(Db::getInstance()->getValue('SELECT message FROM form_prevendita_message WHERE id_thread='.Tools::getValue('rif_prev').' ORDER BY id_message ASC'))."' WHERE id_cart = '".$newCart."'");
				
				Db::getInstance()->executeS("UPDATE carrelli_creati SET esigenze = '".Db::getInstance()->getValue('SELECT message FROM form_prevendita_message WHERE id_thread='.Tools::getValue('rif_prev').' ORDER BY id_message DESC')."' WHERE id_cart = '".$newCart."'");
				
			}
			
			if(Tools::getIsset('riferimento') && Tools::getValue('riferimento') != '')
			{
				Db::getInstance()->executeS("UPDATE cart SET riferimento = ".Tools::getValue('riferimento')." WHERE id_cart = '".$newCart."'");
				
				Db::getInstance()->executeS("UPDATE carrelli_creati SET riferimento = ".Tools::getValue('riferimento')." WHERE id_cart = '".$newCart."'");
			
			}
			
			if(Tools::getIsset('riferimento') && Tools::getValue('riferimento') == '')
			{
				$cstp = new Customer($_GET['id_customer']);
				$persona = Db::getInstance()->getValue('SELECT id_persona FROM persone WHERE firstname = "'.$cstp->firstname.'" AND lastname = "'.$cstp->lastname.'"');
			
				Db::getInstance()->executeS("UPDATE cart SET riferimento = ".$persona." WHERE id_cart = '".$newCart."'");
				
				Db::getInstance()->executeS("UPDATE carrelli_creati SET riferimento = ".$persona." WHERE id_cart = '".$newCart."'");
			
			}
			
			if(!Tools::getIsset('riferimento'))
			{
				$cstp = new Customer($_GET['id_customer']);
				$persona = Db::getInstance()->getValue('SELECT id_persona FROM persone WHERE firstname = "'.$cstp->firstname.'" AND lastname = "'.$cstp->lastname.'"');
			
				Db::getInstance()->executeS("UPDATE cart SET riferimento = ".$persona." WHERE id_cart = '".$newCart."'");
				
				Db::getInstance()->executeS("UPDATE carrelli_creati SET riferimento = ".$persona." WHERE id_cart = '".$newCart."'");
			}
			
			Tools::redirectAdmin($currentIndex.'&id_customer='.$_GET['id_customer'].'&viewcustomer&id_cart='.$newCart.'&conf=4&tab-container-1=4&preventivo='.Tools::getValue('preventivo').(Tools::getIsset('rif_prev') ? '&rif_prev='.Tools::getValue('rif_prev') : '').'&viewcart'.'&token='.$this->token);
				
				
			
		}
		
		

		/*if (!($cart = AdminCarts::loadObject(true)))
			return;*/
		$cart = new Cart(Tools::getValue('id_cart'));		
			
		$customer = new Customer($cart->id_customer);
		$customerStats = $customer->getStats();
		$products = $cart->getProducts();
		$customizedDatas = Product::getAllCustomizedDatas((int)($cart->id));
		Product::addCustomizationPrice($products, $customizedDatas);
		$summary = $cart->getSummaryDetails();
		$discounts = $cart->getDiscounts();

		$currency = new Currency($cart->id_currency);
		$currentLanguage = new Language((int)($cookie->id_lang));
		$id_order = (int)(Order::getOrderByCartId($cart->id));
		$order = new Order($id_order);
		
		if(Tools::getIsset('vedirevisione'))
		{
			mysql_select_db(_DB_REV_NAME_);
			$id_order = "REVISIONE";
			$order = new Order(0);
			$order->id = "REVISIONE";
			$tot_revisioni = Db::getInstance()->executeS('SELECT id_revisione FROM cart WHERE id_cart = '.Tools::getValue('id_cart').' ORDER BY id_revisione ASC');
			$ord_rev = 0;
			foreach($tot_revisioni as $tot_rev)
			{
				if(Tools::getValue('vedirevisione') == $tot_rev['id_revisione'])
					$ord_revisione = $ord_rev;
					
				$ord_rev++;
			}
			mysql_select_db(_DB_NAME_);
		}
		
		$creato_da = Db::getInstance()->getValue("SELECT created_by FROM cart WHERE id_cart = ".$cart->id."");
		$creato_da_nome = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".$creato_da."");
		// display cart header
		echo '<h2><a href="?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'">'.(($customer->id) ? ($customer->is_company == 1 ? $customer->company : $customer->firstname.' '.$customer->lastname) : AdminCarts::lx('Guest')).'</a> - '.AdminCarts::lx('Carrello ').$cart->id.(Tools::getIsset('vedirevisione') ? '-'.$ord_revisione : '').' '.($id_order > 0 ? ' - '.(Tools::getValue('tab') == 'AdminCustomers' ? '<a href="?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&id_order='.(int)($order->id).'&vieworder&token='.$this->token.'&tab-container-1=4">' : '<a href="?tab=AdminOrders&id_order='.(int)($order->id).'&vieworder&token='.Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).(int)($cookie->id_employee)).'">').' '.AdminCarts::lx('Ordine n.').sprintf('%06d', $order->id).' convertito il '.Tools::displayDate($order->date_add, (int)$cookie->id_lang, true).'</a>' : '').'</h2><br />';

		if(Tools::getIsset('vedirevisione'))
		{
			mysql_select_db(_DB_REV_NAME_);
		}
		
			$validita = Db::getInstance()->getValue("SELECT validita FROM cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
			$payment = Db::getInstance()->getValue("SELECT payment FROM cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
			$name = Db::getInstance()->getValue("SELECT name FROM cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
			$in_carico_a = Db::getInstance()->getValue("SELECT in_carico_a FROM cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
			$riferimento = Db::getInstance()->getValue("SELECT riferimento FROM cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
			$consegna = Db::getInstance()->getValue("SELECT consegna FROM cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
			
			if(Tools::getIsset('vedirevisione'))
				$note = Db::getInstance()->getValue("SELECT note FROM cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
			else
				$note = Db::getInstance()->getValue("SELECT note FROM carrelli_creati WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
				
				
			$esigenze = Db::getInstance()->getValue("SELECT esigenze FROM cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
			$risorse = Db::getInstance()->getValue("SELECT risorse FROM cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
			$note_private = Db::getInstance()->getValue("SELECT note_private FROM cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
			$attachment = Db::getInstance()->getValue("SELECT attachment FROM cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
			$id_persona = Db::getInstance()->getValue("SELECT id_persona FROM cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
			$impiegato = Db::getInstance()->getValue("SELECT id_employee FROM cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
			$preventivo = Db::getInstance()->getValue("SELECT preventivo FROM cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
			$visualizzato = Db::getInstance()->getValue("SELECT visualizzato FROM cart WHERE id_cart = ".$_GET['id_cart']." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
			
		if(Tools::getIsset('vedirevisione'))
		{
			mysql_select_db(_DB_NAME_);
		}
		
			if($impiegato != 0) { $nome_impiegato = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".$impiegato.""); } else { $nome_impiegato = 'Cliente'; }
		
		echo '<fieldset style="background-color:#ffffff; margin-top:-30px; border:0px">';
		echo '
			<script type="text/javascript">
			$(document).ready(function() {
					$("#products").bind("keypress", function(e) {
						if (e.keyCode == 13) {
							return false;
						}
					});
				});
			</script>';
			
			echo '
            <script type="text/javascript">  
            var iso = \''.$isoTinyMCE.'\' ;  
            var pathCSS = \''._THEME_CSS_DIR_.'\' ;  
            var ad = \''.$ad.'\' ;  
            </script>  
            <script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce3/tiny_mce.js"></script>  
            <script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>';  
        /* End TinyMCE */ 
		
		echo '<table><tr>';
			echo '<td style="width:70px">Carrello n.</td><td><input type="text" id="id_cart" style="width:150px; margin-right:20px" readonly="readonly" name="id" value="'.$cart->id.'" /></td>';
			echo '<td style="width:70px">Creato il</td><td><input type="text" id="creato_il" style="width:130px; margin-right:20px"  readonly="readonly" name="creato_il" value="'.date("d/m/Y H:i:s", strtotime($cart->date_add)).'" /></td>';
			echo '<td style="width:70px">Creato da</td><td><input type="text"  readonly="readonly" id="creato_da "style="width:130px"  name="creato_da" value="'.($creato_da == 0 ? 'Cliente' : ''.$creato_da_nome).'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
			echo '<td style="width:120px">
				<div style="float:left"><form action="ajax.php" method="post" onsubmit="saveCreaRevisione(); return false;" id="crea_revisione">';
				
				if(Tools::getIsset('vedirevisione'))
				{
				}
				else {
				
					echo '<input type="submit" id="submitCreaRevisione" class="button" onclick="var surec = window.confirm(\'Sei sicuro/a?\'); if (surec) {  } else { return false; }"  value="'.$this->l('Crea revisione').'"  />';
				}
				echo '
				<div id="note_feedback"></div></form>
				<script type="text/javascript">
					function saveCreaRevisione()
					{	
						$("#risposta_feedback").html("<img src=\"../img/loader.gif\" />").show();
						
						$.post("ajax.php", {submitCreaRevisione:1,id_employee:'.$cookie->id_employee.',id_cart:'.Tools::getValue('id_cart').'}, function (res) {
							$("#risposta_feedback").html("").hide();
							
							var r = res.split(";"); 
							
							if (r[0] == "ok")
							{
								$("#risposta_feedback").html("<img src=\"../img/admin/enabled.gif\" />").fadeIn(400);
							}
							else if (r[0] == "error:validation")
								$("#risposta_feedback").html("<b style=\"color:red\">'.addslashes($this->l('Errore')).'</b>").fadeIn(400);
							else if (r[0] == "error:update")
								$("#risposta_feedback").html("<b style=\"color:red\">'.addslashes($this->l('Errore')).'</b>").fadeIn(400);
							$("#risposta_feedback").fadeOut(3000);
							
							if(r[1] == 1)
							{
								$("#select-revisioni").html("<select name=\'revisioni\' id=\'revisioni\' style=\'width:165px\'><option value=\'\'>-- Seleziona --</option>"+r[3]+"</select>").fadeIn(400);
							
							}
							else
							{
								$("#revisioni").append(r[3]);
							
							}
						});
					}
				</script></div>
				<div id="risposta_feedback" style="float:left"></div>
			</td><td></td>';
			echo '</tr>';
		echo "<tr>";
			echo '<td>Cliente</td><td><input type="text" id="cliente" style="width:150px" readonly="readonly" name="cliente" value="'.(($customer->id) ? ($customer->is_company == 1 ? $customer->company: $customer->firstname.' '.$customer->lastname) : AdminCarts::lx('Guest')).'" /></td>';
			echo '<td>Ultima mod.</td><td><input type="text" id="ultima_mod" style="width:130px"  readonly="readonly" name="ultima_mod" value="'.date("d/m/Y H:i:s", strtotime($cart->date_upd)).'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
			echo '<td>Mod. da</td><td><input type="text"  readonly="readonly" id="modificato_da" style="width:130px"  name="modificato_da" value="'.$nome_impiegato.'" /></td>';
			
			
			echo '<td style="width:70px" id="select-revisioni">';
			mysql_select_db(_DB_REV_NAME_);
			
			$count_revisioni = Db::getInstance()->getValue('SELECT count(id_revisione) FROM cart WHERE id_cart = '.Tools::getValue('id_cart').' GROUP BY id_cart');
			if($count_revisioni > 0)
			{
				echo '<select name="revisioni" id="revisioni" style="position:relative; width:75px; height:25px; display:block; float:left">';
				echo '<option value="">Seleziona</option>';
				$revisioni = Db::getInstance()->executeS('SELECT * FROM cart WHERE id_cart = '.Tools::getValue('id_cart').' ORDER BY id_revisione ASC');
				$tokenCarts = Tools::getAdminToken('AdminCarts'.(int)(Tab::getIdFromClassName('AdminCarts')).(int)($cookie->id_employee));
				$rv_tot = 0;
				foreach($revisioni as $revisione)
				{
					mysql_select_db(_DB_NAME_);
					$impiegato = Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$revisione['id_employee'].'');
					mysql_select_db(_DB_REV_NAME_);
					 
					
					echo '<option onclick="// window.open(\'index.php?tab=AdminCarts&id_cart='.$cart->id.'&viewcart&revisione='.$revisione['id_revisione'].'&getPDF=y&token='.$tokenCarts.'\')" value="'.$revisione['id_revisione'].'" '.(Tools::getIsset('vedirevisione') ? (Tools::getValue('vedirevisione') == $revisione['id_revisione'] ? 'selected="selected"' : '') : '').'>'.$revisione['id_cart'].'-'.$rv_tot.' - '.Tools::displayDate($revisione['date_add'], $cookie->id_lang, true).' ('.$impiegato.')</option>';
					$rv_tot++;
				}
				echo '</select>';
				
				echo '<script type="text/javascript">
				$(document).ready(function() {
					$("#revisioni").val("'.Tools::getValue('vedirevisione').'");
				});
				</script>
				';
				
				echo '<a href="#" class="button" style="display:block; float:left" onclick="var id_rev = document.getElementById(\'revisioni\').value; window.open(\'index.php?tab=AdminCarts&id_cart='.$cart->id.'&viewcart&vedirevisione=\'+id_rev+\''.(Tools::getIsset('id_customer') ? '&id_customer='.Tools::getValue('id_customer').'&viewcustomer' : '').'&token='.$tokenCarts.'\')">Carica</a>
				
				
				';
			
			}
			else 
			{
			
				echo 'Non ci sono revisioni';
			
			}
			echo '</td><td></td>';
			
			mysql_select_db(_DB_NAME_);
			
			
			
			echo '</tr>';
			
			
		echo '</table>';
		/* Display customer information */

		/*
		echo '<fieldset style="margin-top:-30px; background-color:#ffffff; border:0px">';
		//	<legend><img src="../img/admin/tab-customers.gif" /> '.AdminCarts::lx('Customer information').'</legend>
			echo '<span style="font-weight: bold; font-size: 14px;">';
			if ($customer->id) {
				echo '
			<a href="?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'"> '.($customer->is_company == 1 ? $customer->company: $customer->firstname.' '.$customer->lastname).'</a></span> ('.AdminCarts::lx('#').$customer->id.') - 
			<strong>Gruppo</strong>: '.($customer->id_default_group == 3 ? 'Rivenditori' : 'Clienti web').' - <strong>Email</strong>: <a href="mailto:'.$customer->email.'">'.$customer->email.'</a> - ';
			
			$customer_phone = Db::getInstance()->getRow("SELECT	phone, phone_mobile FROM address WHERE id_customer = ".$customer->id." AND deleted = 0 AND fatturazione = 1 AND active = 1");
			
			if($customer_phone['phone'] == '') { $telefono_cliente = $customer_phone['phone_mobile']; } else { $telefono_cliente = $customer_phone['phone']; }
			
			echo '<strong>Telefono</strong>: '.$telefono_cliente.'';
			
		
			} else {
				echo AdminCarts::lx('Guest not registered').'</span>';
			}
		echo '</fieldset>';*/
	

		/* Display order information */
	
		
			
		if ($order->getTaxCalculationMethod() == PS_TAX_EXC)
		{
		    $total_products = $summary['total_products'];
		    $total_discount = $summary['total_discounts_tax_exc'];
		    $total_wrapping = $summary['total_wrapping_tax_exc'];
		    $total_price = $summary['total_price_without_tax'];
   		    $total_shipping = $summary['total_shipping_tax_exc'];
		} else {
		    $total_products = $summary['total_products_wt'];
		    $total_discount = $summary['total_discounts'];
		    $total_wrapping = $summary['total_wrapping'];
		    $total_price = $summary['total_price'];
  		    $total_shipping = $summary['total_shipping'];
		}
		
		
		
		
		if ($order->id) {
			$tokenCarts = Tools::getAdminToken('AdminCarts'.(int)(Tab::getIdFromClassName('AdminCarts')).(int)($cookie->id_employee));
			
			
			/*echo '
				<span style="font-weight: bold; font-size: 14px;">';
				if ($order->id)
					echo '
				<a href="?tab=AdminOrders&id_order='.(int)($order->id).'&vieworder&token='.Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).(int)($cookie->id_employee)).'"> '.AdminCarts::lx('Order #').sprintf('%06d', $order->id).'</a></span>
				
				'.AdminCarts::lx('Made on:').' '.Tools::displayDate($order->date_add, (int)$cookie->id_lang, true).'';
				else
					echo AdminCarts::lx('No order created from this cart').'</span>';
				$originale = Db::getInstance()->getValue("SELECT count(*) FROM carrelli_creati WHERE id_cart = ".$_GET['id_cart']."");
				
				echo '</div><br /><br />';
			*/	
			
			
			
			
		}
		
		else {
			
			
			include_once('functions.php');
			AdminCarts::includeDatepickerZ(array('validita'), true);
			
			$tokenCarts = Tools::getAdminToken('AdminCarts'.(int)(Tab::getIdFromClassName('AdminCarts')).(int)($cookie->id_employee));
			$date_validita = date('d/m/Y', strtotime($Date. ' + 15 days'));
			
			
			echo '
			<form name="products" id="products" method="post" action="'.$currentIndex.'&id_cart='.$_GET['id_cart'].'&id_customer='.$customer->id.'&viewcustomer&viewcart&cartupdated&tab-container-1=4&token='.$this->token.'#modifica-carrello" enctype="multipart/form-data">
			';
			//	<legend><img src="../img/admin/cart.gif" /> '.AdminCarts::lx('Informazioni sul carrello').'</legend>';
		
		
			echo "<table><tr>";
			echo '<td style="width:70px">Oggetto</td><td><input type="text" id="name" style="width:150px; margin-right:20px" name="name" value="'.$name.'" /></td>';
			echo '<td style="width:70px">Validit&agrave;</td><td><input type="text" id="validita" style="width:130px; margin-right:20px"  name="validita" value="'.((isset($validita) && $validita != '0000-00-00 00:00:00' && $validita != '1942-01-01') ? date("d/m/Y", strtotime($validita)) : $date_validita).'" /></td>';
			echo '<td style="width:70px">Consegna</td><td><input type="text" id="consegna"style="width:130px"  name="consegna" value="'.(!empty($consegna) ? $consegna : "2-5 gg.").'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
			
			echo '<td style="width:70px">'.($preventivo == 0 ? '' : '<a href="index.php?tab=AdminCarts&id_cart='.$cart->id.'&viewcart&getPDF=y&token='.$tokenCarts.'"><strong>PDF ultima off. salvata</strong></a>').'</td></tr>';

			echo '
			<td style="width:70px">File </td><td><input name="MAX_FILE_SIZE" value="20000000" type="hidden"><input name="joinFile[]" style="width:150px" class="multi" id="joinFile" type="file" onchange="handleFileSelect(this.files)"></td>
			';
			
			echo '<td style="width:70px">In carico a</td>';
			
			echo '<td>
			<input type="hidden" name="preventivo" value="'.(Tools::getIsset('preventivo') ? (Tools::getValue('preventivo') == 2 ? 0 : Tools::getValue('preventivo')) : Db::getInstance()->getValue('SELECT preventivo FROM cart WHERE id_cart = '.$cart->id.'')).'" />
			
			
			<input type="hidden" name="rif_prev" value="'.(Tools::getIsset('rif_prev') ? Tools::getValue('rif_prev') : Db::getInstance()->getValue('SELECT rif_prev FROM cart WHERE id_cart = '.$cart->id.'')).'" />
			
			<select id="in_carico_a" style="width:140px" name="in_carico_a" >';
			
			$impiegati_ez = Db::getInstance()->ExecuteS("SELECT id_employee, firstname, lastname FROM employee");

				foreach ($impiegati_ez as $impez) {
				
					if($impez['id_employee'] != 3 && $impez['id_employee'] != 4 && $impez['id_employee'] != 5)
						echo "<option value='".$impez['id_employee']."' ".($in_carico_a == 0 ? ($impez['id_employee'] == $cookie->id_employee ? 'selected="selected"' : '') : ($in_carico_a == $impez['id_employee'] ? 'selected="selected"' : '') )." >".$impez['firstname']." ".$impez['lastname']."</option>";
				
				}

				echo '</select></td>
				';
				
				if($customer->is_company == 1) {
				
			echo '<td style="width:70px">Riferimento</td>';
			
			echo '<td><select id="riferimento" style="width:140px" name="riferimento" >';
			echo "<option value='0'>--</option>";
			$persone_rif = Db::getInstance()->ExecuteS("SELECT * FROM persone WHERE id_customer = ".$customer->id);

				foreach ($persone_rif as $persona_rif) {
				
						echo "<option value='".$persona_rif['id_persona']."' ".($riferimento == $persona_rif['id_persona'] ? 'selected="selected"' : '')." >".$persona_rif['firstname']." ".$persona_rif['lastname']." - ".$persona_rif['phone']."</option>";
				
				}

				echo '</select></td>';
				
				
				}
				else 
				{
					echo '<td></td><td></td>';
				}
			
			echo '<td>';
			
				$template_carrelli = Db::getInstance()->executeS('SELECT id_cart, name FROM cart WHERE id_customer = 44431');
				
				echo '<script type="text/javascript">
				function loadTemplate(id)
				{
					
					$.ajax({
					  url:"ajax.php?load_template=y",
					  type: "POST",
					  data: { id_template: id
					  },
					  success:function(resp){
						
						var valori_template = resp.split("|||");
						
						$("#name").val(valori_template[0]);
						
						$(tinymce.editors[\'note\'].getBody()).html(valori_template[1]);
					  },
					  error: function(xhr,stato,errori){
						 alert("Errore ");
					  }
					});
					
					
				}
				</script>';

				echo '<select style="width:155px" name="template_carrello" id="template_carrello" onchange="var surec = window.confirm(\'Se selezioni un template, le note attuali saranno sovrascritte. Sei sicuro/a?\'); if (surec) { loadTemplate(this.value); return true; } else { return false; }"><option value="">-- Template --</option>';
				foreach($template_carrelli as $template) 
				{
					echo '<option value="'.$template['id_cart'].'" '.($name == $template['name'] ? 'selected="selected"' : '').'>'.$template['name'].'</option>';
					
				}
				echo '</select>';
			
			
			
			
			echo '</td>';


			
				echo '</tr></table>
				';
				
			$fatturazione = Db::getInstance()->getValue("SELECT id_address_invoice FROM cart WHERE id_cart = ".$_GET['id_cart']."");
			$consegna = Db::getInstance()->getValue("SELECT id_address_delivery FROM cart WHERE id_cart = ".$_GET['id_cart']."");
			
			$indirizzi_di_fatturazione = Db::getInstance()->executeS("SELECT * FROM address WHERE id_customer = ".$customer->id." AND fatturazione = 1 AND deleted = 0 AND active = 1 ORDER BY id_address ASC");
			
			$indirizzi_di_consegna = Db::getInstance()->executeS("SELECT * FROM address WHERE id_customer = ".$customer->id." AND deleted = 0 AND active = 1 ORDER BY id_address ASC");
			
			echo '<output id="list-tkt"></output>
								<script type="text/javascript">
							function handleFileSelect(files) {
							
								// Loop through the FileList and render image files as thumbnails.
								 for (var i = 0; i < files.length; i++) {
									var f = files[i];
									var name = files[i].name;
          
									var reader = new FileReader();  
									reader.onload = function(e) {  
									  // Render thumbnail.
									  var span = document.createElement("span");
									  span.innerHTML = ["<a href=\'",e.target.result,"\' target=\'_blank\'>",name,"</a> - "].join("");
									  document.getElementById("list-tkt").insertBefore(span, null);
									};

								  // Read in the image file as a data URL.
								  reader.readAsDataURL(f);
								}
							  }
							 
							</script>';
			
			echo '<br />
			
			<table style="margin-bottom:-20px">
			<tr>
			<td><strong>Fatturazione: </strong></td><td> <select name="id_address_invoice" style="width:351px">';
			foreach ($indirizzi_di_fatturazione as $inv_a) {
			echo '<option value="'.$inv_a['id_address'].'" '.($fatturazione == $inv_a['id_address'] ? 'selected="selected"' : '').'>'.$inv_a['address1'].' - '.$inv_a['city'].'</option>';
			}
			$tokenAddresses = Tools::getAdminToken('AdminAddresses'.(int)(Tab::getIdFromClassName('AdminAddresses')).(int)($cookie->id_employee));
			
			echo '</select> <a style="cursor:pointer" onclick="window.open(\'?tab=AdminAddresses&addaddress&fatturazione=1&idclientee='.$customer->id.'&back='.urlencode($_SERVER['REQUEST_URI']).'&token='.$tokenAddresses.'\', \'blank\', \'height=600,width=1200,modal=yes,alwaysRaised=yes,scrollbars=yes\');" ><img src="../img/admin/add.gif" alt="" title="" /></a></td>
			<td><strong>Consegna: </strong></td><td> <select name="id_address_delivery" id="id_address_delivery" onchange="calcolaSpedizionePerIndirizzo();" style="width:352px">';
			foreach ($indirizzi_di_consegna as $del_a) {
			echo '<option value="'.$del_a['id_address'].'" '.($consegna == $del_a['id_address'] ? 'selected="selected"' : '').'>'.$del_a['address1'].' - '.$del_a['city'].'</option>';
			}
			echo '</select><a style="cursor:pointer" onclick="window.open(\'?tab=AdminAddresses&addaddress&idclientee='.$customer->id.'&back='.urlencode($_SERVER['REQUEST_URI']).'&token='.$tokenAddresses.'\', \'blank\', \'height=600,width=1200,modal=yes,alwaysRaised=yes,scrollbars=yes\');"><img src="../img/admin/add.gif" alt="" title="" /></a>
			</td></tr></table>
			';

			
			
		}
		// List of products
		/*echo '
		<br style="clear:both;" />
				';
				*/
				
		if ($order->id) {
						
				echo '<table><tr>';
			echo '<td style="width:70px">Oggetto</td><td><input type="text" id="name" style="width:150px" readonly="readonly" name="name" value="'.$name.'" /></td>';

			
			echo '<td style="width:70px">Validit&agrave;</td><td><input type="text" id="validita" style="width:130px"  readonlye="readonly" name="validita" value="'.((isset($validita) && $validita != '0000-00-00 00:00:00' && $validita != '1942-01-01') ? date("d/m/Y", strtotime($validita)) : $date_validita).'" /></td>';
			echo '<td style="width:70px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Consegna</td><td>&nbsp;&nbsp;&nbsp;<input type="text"  readonly="readonly" id="consegna"style="width:130px"  name="consegna" value="'.(!empty($consegna) ? $consegna : "2-5 gg.").'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
			echo '<td style="width:100px"><a href="index.php?tab=AdminCarts&id_cart='.$cart->id.'&viewcart&getPDF=y'.(Tools::getIsset('vedirevisione') ? '&revisione='.Tools::getValue('vedirevisione') : '').'&token='.$tokenCarts.'"><strong>PDF offerta</strong></a></td><td></td></tr>';
			
			if($originale > 0) {
					echo '<td style="width:100px"><a href="index.php?tab=AdminCarts&id_cart='.$cart->id.'&viewcart&getPDF=y&originale=y'.(Tools::getIsset('vedirevisione') ? '&revisione='.Tools::getValue('vedirevisione') : '').'&token='.$tokenCarts.'"><strong>PDF offerta</strong></a></td></tr>';
				} else {  echo '<td></td></tr>'; }

			echo '
			<td> </td><td><input name="MAX_FILE_SIZE" value="20000000" type="hidden"><input name="joinFile[]" style="visibility:hidden; width:150px" class="multi" id="joinFile" type="file">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			';
			
			echo '<td>In carico a</td>';
			
			echo '<td><input type="text" readonly="readonly" value="'.($in_carico_a == 0 ? 'Nessuno' : Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$in_carico_a.'')).'" style="width:130px" name="in_carico_a" >';
			
			

				echo '</td></tr></table>
				';
			
			$fatturazione = Db::getInstance()->getValue("SELECT id_address_invoice FROM cart WHERE id_cart = ".$_GET['id_cart']."");
			$consegna = Db::getInstance()->getValue("SELECT id_address_delivery FROM cart WHERE id_cart = ".$_GET['id_cart']."");
			
			$inv_a = Db::getInstance()->getRow("SELECT * FROM address WHERE id_address = ".$fatturazione."");
			
			$del_a = Db::getInstance()->getRow("SELECT * FROM address WHERE id_address = ".$consegna."");
			
			echo '<br />
			<table style="margin-bottom:-20px">
			<tr>
			<td><strong>Fatturazione: </strong></td><td> <input type="text" readonly="readonly" value="'.$inv_a['address1'].' - '.$inv_a['city'].'" name="id_address_invoice" style="width:360px"></td>
			<td><strong>Consegna: </strong></td><td> <input type="text" readonly="readonly" value="'.$del_a['address1'].' - '.$del_a['city'].'" name="id_address_delivery" style="width:351px">';
			
			echo '
			</td></tr></table>
			';
			
				echo '<br /><br />';
				
			  
			  
			  
		}
		
		else {
		
		
			echo '
			
		<br />
		';
			
				$ctrl_creato_nuovo = Db::getInstance()->getValue("SELECT count(id_cart) FROM carrelli_creati WHERE id_cart = ".$_GET['id_cart']."");
				if($ctrl_creato_nuovo == 0) {
					
					/*$query="select c.* from ". _DB_PREFIX_."cart c where c.id_cart=".$_GET['id_cart'];

					$res=mysql_query($query) or die;
					if (mysql_num_rows($res)>0) {
						$cart=mysql_fetch_array($res);
					
					Db::getInstance()->executeS("INSERT INTO carrelli_creati (id_cart, id_carrier, id_lang, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, date_add, date_upd, id_employee) VALUES (
					'".$cart['id_cart']."', '".$cart['id_carrier']."', '".$cart['id_lang']."', '".$cart['id_address_delivery']."', '".$cart['id_address_invoice']."', '".$cart['id_currency']."', '".$cart['id_customer']."', '".$cart['id_guest']."', '".$cart['secure_key']."', '".$cart['recyclable']."','".$cart['gift']."','".$cart['gift_message']."','".$cart['date_add']."', '".$cart['date_upd']."', ".$cookie->id_employee.")");
					}
					
					$query2="select * from ". _DB_PREFIX_."cart_product cp where cp.id_cart=".$_GET['id_cart'];
					$res2=mysql_query($query2) or die;
					if (mysql_num_rows($res2)>0) {
						while($cp=mysql_fetch_array($res2, MYSQL_ASSOC)) {
						
							Db::getInstance()->executeS("INSERT INTO carrelli_creati_prodotti (id_cart, id_product, id_product_attribute, quantity, price, free, date_add) VALUES (
							'".$cp['id_cart']."', '".$cp['id_product']."', '".$cp['id_product_attribute']."', '".$cp['quantity']."', '".$cp['price']."', '".$cp['free']."', '".$cp['date_add']."')");
							
						}
					}*/
					
					
				}
				else {
				
				
				}
			
			
		
			?>
			
			<?php
			
			
		}
			echo '
			<script type="text/javascript" src="../js/jquery/jquery.tablednd_0_5.js"></script>
				<script type="text/javascript">
				$(document).ready(function() {
						$("#tableProducts").tableDnD({
						onDrop: function(table, row) {
						},
						dragHandle: ".dragHandle"
					});
				
				
					$("#tableProducts").hover(function() {
						$(this.cells[8]).addClass(\'showDragHandle\');
					}, function() {
						  $(this.cells[8]).removeClass(\'showDragHandle\');
					});
				});
				</script>
				
			<script type="text/javascript">
			$(document).ready(function() {
			
				$(".wholesale_price_class").each(function() {
				
							var productId = $(this).attr("id").substring($(this).attr("id").indexOf("[") + 1,$(this).attr("id").indexOf("]",$(this).attr("id").indexOf("[") + 1));
							
							var acq = document.getElementById("wholesale_price["+productId+"]"); var last = acq.value;
								
								var ctrl_m_c = 0;
								
								//if(ctrl_m_c == 0) {
								//	$(document.getElementById("wholesale_price["+productId+"]")).one(\'click\', function() {
								//		alert(\'Attenzione: stai cambiando il prezzo di acquisto\');
								//		ctrl_m_c = 1;
								//	});
								//}
								
								
								//if(ctrl_m_c == 0) {
									//$(document.getElementById("wholesale_price["+productId+"]")).one(\'keydown\', function(event) {
									
										//var code = (event.keyCode ? event.keyCode : event.which);
										
										//if(code == 9 || code == 37 || code == 38 || code == 39 || code == 40) {
											
										//}
										//else {
											//alert(\'Attenzione: stai cambiando il prezzo di acquisto\');
											//ctrl_m_c = 1;
										//}
									//});
								//}
								
								
								// Setup the event
								$(document.getElementById("wholesale_price["+productId+"]")).focusout(function() {
									ctrl_m_c = 0;
									if (last != $(this).val()) {
										
										var surec = window.confirm(\'Stai cambiando un prezzo di acquisto. Sei sicuro?\'); 
										if (surec) { return true; } else { document.getElementById("wholesale_price["+productId+"]").value = last; }
									}
								});
							});
						
						
					});
					
					
			</script>
			
			<script type="text/javascript">
			function ristabilisciPrezzo(id_product) {
			
				var numprice = document.getElementById(\'product_price[\'+id_product+\']\').value;
				document.getElementById(\'sconto_extra[\'+id_product+\']\').value = 0;
				var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
				var unitario = parseFloat(document.getElementById(\'unitario[\'+id_product+\']\').value);
				unitario = unitario.toFixed(2);
				unitario = unitario.toString();				
				unitario = unitario.replace(".",",");
				document.getElementById(\'product_price[\'+id_product+\']\').value=unitario;
				price=unitario;
			
			}
			
			
			
			function calcolaPrezzoScontoExtra(id_product, tipo) {
			
				var numunitario = document.getElementById(\'unitario[\'+id_product+\']\').value;
			
				var unitario = parseFloat(numunitario.replace(/\s/g, "").replace(",", "."));
				
				if (unitario == 0) {
				unitario = 0.001;
				}
				else {
				}
				
				var quantity = parseFloat(document.getElementById(\'product_quantity[\'+id_product+\']\').value);
				var numsconto_extra = document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".");
				
				var numprice = document.getElementById(\'product_price[\'+id_product+\']\').value;	
				var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
				var sconto_extra = parseFloat(numsconto_extra);
				
				var sc_qta_1 = parseFloat(document.getElementById(\'sc_qta_1[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
				var sc_qta_2 = parseFloat(document.getElementById(\'sc_qta_2[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
				var sc_qta_3 = parseFloat(document.getElementById(\'sc_qta_3[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
				var sc_riv_1 = parseFloat(document.getElementById(\'sc_riv_1[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
				var sc_riv_2 = parseFloat(document.getElementById(\'sc_riv_2[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
				var sc_riv_3 = parseFloat(document.getElementById(\'sc_riv_3[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
				var sc_qta_1_q = parseFloat(document.getElementById(\'sc_qta_1_q[\'+id_product+\']\').value);
				var sc_qta_2_q = parseFloat(document.getElementById(\'sc_qta_2_q[\'+id_product+\']\').value);
				var sc_qta_3_q = parseFloat(document.getElementById(\'sc_qta_3_q[\'+id_product+\']\').value);
				var sc_riv_1_q = parseFloat(document.getElementById(\'sc_riv_1_q[\'+id_product+\']\').value);
				var sc_riv_2_q = parseFloat(document.getElementById(\'sc_riv_2_q[\'+id_product+\']\').value);	
				var sc_riv_3_q = parseFloat(document.getElementById(\'sc_riv_3_q[\'+id_product+\']\').value);	
				
				if (document.getElementById(\'usa_sconti_quantita[\'+id_product+\']\').checked == true) {
				';	
					if ($customer->id_default_group == 3) {
					
						echo '
						if(tipo == "sconto") {
							var finito = sc_riv_1 - ((sconto_extra*sc_riv_1)/100);
						}
						else if (tipo == "inverso") {
							var finito = ((sc_riv_1 - price)/sc_riv_1)*100;
						}
						
						
						';
						
					}
					
					else if ($customer->id_default_group == 12) {
					
						echo '
						if(tipo == "sconto") {
							var finito = sc_riv_3 - ((sconto_extra*sc_riv_3)/100);
						}
						else if (tipo == "inverso") {
							var finito = ((sc_riv_3 - price)/sc_riv_3)*100;
						}
						
						
						';
						
					}
						
					else {
						
					echo '
					if(quantity < sc_qta_1_q && sc_qta_1_q != 0) {
						if(tipo == "sconto") {
							var finito = unitario - ((sconto_extra*unitario)/100);
						}
						else if (tipo == "inverso") {
							var finito = ((unitario - price)/unitario)*100;
						}
					}
						
					else if((quantity >= sc_qta_1_q && quantity < sc_qta_2_q) || (quantity >= sc_qta_1_q && isNaN(parseFloat(sc_qta_2_q)))) {
						if(tipo == "sconto") {
							var finito = sc_qta_1 - ((sconto_extra*sc_qta_1)/100);	
						}
						else if (tipo == "inverso") {
							var finito = ((sc_qta_1 - price)/sc_qta_1)*100;
						}
					}
						
					else if((quantity >= sc_qta_2_q && quantity < sc_qta_3_q) || (quantity >= sc_qta_2_q && isNaN(parseFloat(sc_qta_3_q == 0)))) {
						if(tipo == "sconto") {
							var finito = sc_qta_2 - ((sconto_extra*sc_qta_2)/100);
						}
						else if (tipo == "inverso") {
							var finito = ((sc_qta_2 - price)/sc_qta_2)*100;
						}
					}
						
					else if(quantity >= sc_qta_3_q && sc_qta_2_q != 0 && sc_qta_3_q != 0 && sc_qta_1_q != 0) {
						if(tipo == "sconto") {
							var finito = sc_qta_3 - ((sconto_extra*sc_qta_3)/100);					
						}
						else if (tipo == "inverso") {
							var finito = ((sc_qta_3 - price)/sc_qta_3)*100;
						}
						
					}	';
						
					} echo '				
				}
				else {
					if (tipo == "sconto") {
						var finito = unitario - ((sconto_extra*unitario)/100);
					}
					else if (tipo == "inverso") {
						var finito = ((unitario - price)/unitario)*100;
					}
				}
				
		
				finito = finito.toFixed(2);
				finito = finito.toString();				
				finito = finito.replace(".",",");
				
				if (tipo == "sconto") {
					document.getElementById(\'product_price[\'+id_product+\']\').value=finito;
				}
				else if (tipo == "inverso") {
					document.getElementById(\'sconto_extra[\'+id_product+\']\').value=finito;
				}
			}
			
			function calcolaImportoConSpedizione(id_metodo) {
	
				if(document.getElementById(\'trasporto_modificato\').value == "y") {
					var metodo_price = parseFloat(document.getElementById(\'transport\').value.replace(/\s/g, "").replace(",", "."));
				}
				else {
					var metodo_price = parseFloat(document.getElementById(\'metodo[\'+id_metodo+\']\').getAttribute("rel"));
				}
				var totale = 0;
				var totale_con_iva = 0;
				var arrayImporti = document.getElementsByClassName("importo");
					for (var i = 0; i < arrayImporti.length; ++i) {
						var item = parseFloat(arrayImporti[i].value);  
						totale += item;
					}
					
				totale = totale+metodo_price;
				var totalep = totale;
				
				
				
				totale = totale.toFixed(2);
				
					
				document.getElementById(\'totaleprodotti\').value=totale;
						
				document.getElementById(\'spantotaleprodotti\').innerHTML=totale.replace(".",",");
				
				
				/*
				$.ajax({
					url:"ajax_products_list2.php?calcola_totale_con_iva=y",
					type: "POST",
					data: { address: document.getElementById("id_address_delivery").value,
					customer : '.$customer->id.',
					totalep : totalep,
					 },
					success:function(resp){
						totale_con_iva = parseFloat(resp);
						totale_con_iva = totale_con_iva.toFixed(2);
						document.getElementById(\'spantotaleprodottiiva\').innerHTML=totale_con_iva.replace(".",",");
					},
					error: function(xhr,stato,errori){
						alert("Errore nella cancellazione:"+xhr.status);
					}
				});
				*/
				calcola_noleggio();
			
			}
			
			

			function calcolaImporto(id_product) {
					var numprice = document.getElementById(\'product_price[\'+id_product+\']\').value;
					var numunitario = document.getElementById(\'unitario[\'+id_product+\']\').value;
					var numacquisto = document.getElementById(\'wholesale_price[\'+id_product+\']\').value;
					
					var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
								
					var quantity = parseFloat(document.getElementById(\'product_quantity[\'+id_product+\']\').value);

					var unitario = parseFloat(numunitario.replace(/\s/g, "").replace(",", "."));
					var acquisto = parseFloat(numacquisto.replace(/\s/g, "").replace(",", "."));
					
					var totaleacquisto = acquisto * quantity;
					
					document.getElementById(\'totaleacquisto[\'+id_product+\']\').value = totaleacquisto;

					var totale = parseFloat(document.getElementById(\'totaleprodotti\').value);
							
					var sc_qta_1 = parseFloat(document.getElementById(\'sc_qta_1[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
					var sc_qta_2 = parseFloat(document.getElementById(\'sc_qta_2[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
					var sc_qta_3 = parseFloat(document.getElementById(\'sc_qta_3[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
					var sc_riv_1 = parseFloat(document.getElementById(\'sc_riv_1[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
					var sc_riv_2 = parseFloat(document.getElementById(\'sc_riv_2[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));
					var sc_riv_3 = parseFloat(document.getElementById(\'sc_riv_3[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", "."));				
					var sc_qta_1_q = parseFloat(document.getElementById(\'sc_qta_1_q[\'+id_product+\']\').value);
					var sc_qta_2_q = parseFloat(document.getElementById(\'sc_qta_2_q[\'+id_product+\']\').value);
					var sc_qta_3_q = parseFloat(document.getElementById(\'sc_qta_3_q[\'+id_product+\']\').value);
					var sc_riv_1_q = parseFloat(document.getElementById(\'sc_riv_1_q[\'+id_product+\']\').value);
					var sc_riv_2_q = parseFloat(document.getElementById(\'sc_riv_2_q[\'+id_product+\']\').value);
					var sc_riv_3_q = parseFloat(document.getElementById(\'sc_riv_3_q[\'+id_product+\']\').value);
					
					
					var importo = parseFloat(price*quantity);
					
					if (document.getElementById(\'usa_sconti_quantita[\'+id_product+\']\').checked == true) {
					
					
						';
						if ($customer->id_default_group == 3 || $customer->id_default_group == 12) {
						
						echo '
							if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".") > 0) {
							} else {
								var prezzounitarionew = unitario;
								var prezziconfronto = new Array(sc_riv_1, '.($customer->id_default_group == 12 ? 'sc_riv_3, ': '').'sc_qta_1, sc_qta_2, sc_qta_3);
								for (var i = 0; i < prezziconfronto.length; ++i) {
									if(prezziconfronto[i] !=0 && prezziconfronto[i] < prezzounitarionew) { 
										prezzounitarionew = prezziconfronto[i];
										
									} else { }
								}	
								var importo = parseFloat(prezzounitarionew*quantity);
								document.getElementById(\'product_price[\'+id_product+\']\').value=prezzounitarionew.toFixed(2).replace(".",",");					
							}
						';
						
						}
						
						else {
						
						echo '
						if(quantity < sc_qta_1_q && sc_qta_1_q != 0) {
							
							if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".") > 0) {
							} else {
								document.getElementById(\'product_price[\'+id_product+\']\').value=unitario.toFixed(2).replace(".",",");	
								var importo = parseFloat(price*quantity);
							}		
						}
						
						else if((quantity >= sc_qta_1_q && quantity < sc_qta_2_q) || (quantity >= sc_qta_1_q && isNaN(parseFloat(sc_qta_2_q)))) {
							if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".") > 0) {
							} else {
								var importo = parseFloat(sc_qta_1*quantity);
								document.getElementById(\'product_price[\'+id_product+\']\').value=sc_qta_1.toFixed(2).replace(".",",");	
							}
						}
						
						else if((quantity >= sc_qta_2_q && quantity < sc_qta_3_q) || (quantity >= sc_qta_2_q && isNaN(parseFloat(sc_qta_3_q == 0)))) {
							if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".") > 0) {
							} else {
								var importo = parseFloat(sc_qta_2*quantity);	
								document.getElementById(\'product_price[\'+id_product+\']\').value=sc_qta_2.toFixed(2).replace(".",",");	
							}
						}
						
						else if(quantity >= sc_qta_3_q && sc_qta_2_q != 0 && sc_qta_3_q != 0 && sc_qta_1_q != 0) {
							if(document.getElementById(\'sconto_extra[\'+id_product+\']\').value.replace(/\s/g, "").replace(",", ".") > 0) {
							} else {
								var importo = parseFloat(sc_qta_3*quantity);	
								document.getElementById(\'product_price[\'+id_product+\']\').value=sc_qta_3.toFixed(2).replace(".",",");
							}
						}';
						
						} echo '
					}
					
					else if (document.getElementById(\'usa_sconti_quantita[\'+id_product+\']\').checked == false) {
						
						var importo = parseFloat(price*quantity);
					
					}
					document.getElementById(\'product_price[\'+id_product+\']\').value.replace(".",",");
					
					totale = 0;
					
					
					
					document.getElementById(\'impImporto[\'+id_product+\']\').value = importo;
					
					var arrayImporti = document.getElementsByClassName("importo");
					for (var i = 0; i < arrayImporti.length; ++i) {
						var item = parseFloat(arrayImporti[i].value);  
						totale += item;
					}
					

					var totaleacquisti = 0;
					
					var arrayAcquisti = document.getElementsByClassName("prezzoacquisto");
					for (var i = 0; i < arrayAcquisti.length; ++i) {
						arrayAcquisti[i].value = arrayAcquisti[i].value.replace(",",".");
						var item = parseFloat(arrayAcquisti[i].value); 					
						totaleacquisti += item;
					}
					
					
								
					var marginalitatotale = (((totale - totaleacquisti)*100) / totale);
		
					marginalitatotale = marginalitatotale.toFixed(2);
		
					document.getElementById(\'marginalitatotale\').innerHTML=marginalitatotale.replace(".",",") + "%";
					
					var guadagnototale = totale - totaleacquisti;
					
					guadagnototale = guadagnototale.toFixed(2);
					
					document.getElementById(\'guadagnototale\').innerHTML=guadagnototale.replace(".",",");
					
					
					
					importo = importo.toFixed(2);
					importo = importo.replace(".",",");
					
					
					document.getElementById(\'valoreImporto[\'+id_product+\']\').innerHTML=importo;
					
					
					/*
					for (var i = 0; i < document.forms.products.elements.metodo.length; i++) {
						var button = document.forms.products.elements.metodo[i];
						
						if (button.checked) {
							
							totale += parseFloat(button.getAttribute("rel"));
						}
						else {
						
						}
					}*/';
					
							if($customer->id_default_group == 1) {
				
					echo '	
						if(totale < 399) {
							var supplemento_contrassegno = 3.5;
						}
						else {
							var supplemento_contrassegno = 0;
							
						}';
					}
						
					else if($customer->id_default_group  == 3) {
							
						echo 'if(totale < 516) {
							var supplemento_contrassegno = 5;
						}
						else {
							var supplemento_contrassegno = ((totale/100)*1.5);
						
						}';
							
					}
						
					else {
						echo 'if(totale < 516) {
							var supplemento_contrassegno = 5;
						}
						else {
							var supplemento_contrassegno = ((totale/100)*1.5);
							
						}';
					
					}
					
					echo '
					
					supplemento_contrassegno = supplemento_contrassegno.toFixed(2);
									
					document.getElementById(\'supplemento_contrassegno\').innerHTML=supplemento_contrassegno.replace(".",",");	

					calcolaSpedizionePerIndirizzo();				
					
					totale = totale.toFixed(2);
				
					document.getElementById(\'totaleprodotti\').value=totale;
					
					document.getElementById(\'spantotaleprodotti\').innerHTML=totale.replace(".",",");
					
					numprice = document.getElementById(\'product_price[\'+id_product+\']\').value;
					
					price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
					
					var marginalita = (((price - acquisto)*100) / price);
					
					marginalita = marginalita.toFixed(2);
				
					document.getElementById(\'spanmarginalita[\'+id_product+\']\').innerHTML=marginalita.replace(".",",")+"%";
					
					calcola_noleggio();
					
					
					
			}
			
			function togliImporto(id_product) {
			
				var daTogliere = parseFloat(document.getElementById(\'impImporto[\'+id_product+\']\').value.replace(",", "."));
				
				
				var totale = parseFloat(document.getElementById(\'totaleprodotti\').value.replace(",", "."));
				
				';
				
							if($customer->id_default_group == 1) {
				
					echo '	
						if(totale < 399) {
							var supplemento_contrassegno = 3.5;
						}
						else {
							var supplemento_contrassegno = 0;
							
						}';
					}
						
					else if($customer->id_default_group  == 3) {
							
						echo 'if(totale < 516) {
							var supplemento_contrassegno = 5;
						}
						else {
							var supplemento_contrassegno = ((totale/100)*1.5);
						
						}';
							
					}
						
					else {
						echo 'if(totale < 516) {
							var supplemento_contrassegno = 5;
						}
						else {
							var supplemento_contrassegno = ((totale/100)*1.5);
							
						}';
					
					}
					
					echo '
					
					var numacquisto = document.getElementById(\'wholesale_price[\'+id_product+\']\').value;
			
					var quantity = parseFloat(document.getElementById(\'product_quantity[\'+id_product+\']\').value);

					var acquisto = parseFloat(numacquisto.replace(/\s/g, "").replace(",", "."));
					
					supplemento_contrassegno = supplemento_contrassegno.toFixed(2);
									
					document.getElementById(\'supplemento_contrassegno\').innerHTML=supplemento_contrassegno.replace(".",",");
					
					var totaleacquisti = 0;
					
					var arrayAcquisti = document.getElementsByClassName("prezzoacquisto");
					for (var i = 0; i < arrayAcquisti.length; ++i) {
						arrayAcquisti[i].value = arrayAcquisti[i].value.replace(",",".");
						var item = parseFloat(arrayAcquisti[i].value); 					
						totaleacquisti += item;
					}
					
					var togliAcquisto = (acquisto*quantity);
					
					totaleacquisti = totaleacquisti-togliAcquisto;
					
					totale = totale-daTogliere;
								
					
					var marginalitatotale = (((totale - totaleacquisti)*100) / totale);
		
					marginalitatotale = marginalitatotale.toFixed(2);
		
					document.getElementById(\'marginalitatotale\').innerHTML=marginalitatotale.replace(".",",") + "%";
					
					var guadagnototale = totale - totaleacquisti;
					
					guadagnototale = guadagnototale.toFixed(2);
					

					
					document.getElementById(\'guadagnototale\').innerHTML=guadagnototale.replace(".",",");
				
					totale = totale.toFixed(2);
				
					document.getElementById(\'totaleprodotti\').value=totale;
					
					document.getElementById(\'spantotaleprodotti\').innerHTML=totale.replace(".",",");
				
					calcola_noleggio();
				
				
			}
			function deleteRow(rowid)  
			{   
				var row = document.getElementById(rowid);
				row.parentNode.removeChild(row);
			}
			
			function delProduct30(id)
				{
					deleteRow(\'tr_\'+id);
					$.ajax({
					  url:"ajax_products_list2.php?cancellaprodotto=y",
					  type: "POST",
					  data: { id_product: id,
					  id_cart: '.$_GET['id_cart'].'
					  },
					  success:function(){
						 	var excludeIds = document.getElementById("excludeIds").value;
							
							var da_cancellare = new RegExp(id, "g");
							
							excludeIds = excludeIds.replace(da_cancellare,""); 
							document.getElementById("excludeIds").value = excludeIds;
							$("#product_autocomplete_input").val("");
							$("#product_autocomplete_input").setOptions({
								extraParams: {excludeIds : document.getElementById("excludeIds").value}
							});	
							calcolaSpedizionePerIndirizzo();	
							$("#product_autocomplete_input_interno").setOptions({
								extraParams: {excludeIds : document.getElementById("excludeIds").value}
							});	
					  },
					  error: function(xhr,stato,errori){
						 alert("Errore nella cancellazione:"+xhr.status);
					  }
					});
					
					
				}
				
				
				
			function calcola_noleggio() {
				
					var numtotale = document.getElementById("spantotaleprodotti").innerHTML;

					var totale = parseFloat(numtotale.replace(/\s/g, "").replace(",", "."));
				
					if(totale <= 5000) { document.getElementById("spese_contratto_noleggio").value = 50 }
					if(totale >= 5001 && totale <= 10000) { document.getElementById("spese_contratto_noleggio").value = 75 }
					if(totale >= 10001 && totale <= 50000) { document.getElementById("spese_contratto_noleggio").value = 100 }
					if(totale >= 50001 && totale <= 100000) { document.getElementById("spese_contratto_noleggio").value = 150 }
					if(totale >= 100001) { document.getElementById("spese_contratto_noleggio").value = 200 }
					
				
					
					var mesi_noleggio = document.getElementById("mesi_noleggio").value;
					var parametro = 0;
					
					if(mesi_noleggio == 0) {
						parametro = 0;
						document.getElementById("spese_contratto_noleggio").value = 0;
					}
					
					else if(mesi_noleggio == 18) {
					
						if(totale <= 5000) { parametro = 6.306; }
						if(totale >= 5001 && totale <= 10000) { parametro = 6.243; }
						if(totale >= 10001 && totale <= 25000) { parametro = 6.214; }
						if(totale >= 25001 && totale <= 50000) { parametro = 6.185; }
						if(totale >= 50001 && totale <= 100000) { parametro = 6.178; }
						if(totale >= 100001) { parametro = 6.170; }
	
					}
					
					else if(mesi_noleggio == 24) {
					
						if(totale <= 5000) { parametro = 4.857; }
						if(totale >= 5001 && totale <= 10000) { parametro = 4.793; }
						if(totale >= 10001 && totale <= 25000) { parametro = 4.764; }
						if(totale >= 25001 && totale <= 50000) { parametro = 4.735; }
						if(totale >= 50001 && totale <= 100000) { parametro = 4.728; }
						if(totale >= 100001) { parametro = 4.721; }
	
					}
					
					else if(mesi_noleggio == 36) {
					
						if(totale <= 5000) { parametro = 3.411; }
						if(totale >= 5001 && totale <= 10000) { parametro = 3.346; }
						if(totale >= 10001 && totale <= 25000) { parametro = 3.317; }
						if(totale >= 25001 && totale <= 50000) { parametro = 3.288; }
						if(totale >= 50001 && totale <= 100000) { parametro = 3.280; }
						if(totale >= 100001) { parametro = 3.273; }
	
					}
					
					else if(mesi_noleggio == 48) {
					
						if(totale <= 5000) { parametro = 2.747; }
						if(totale >= 5001 && totale <= 10000) { parametro = 2.681; }
						if(totale >= 10001 && totale <= 25000) { parametro = 2.650; }
						if(totale >= 25001 && totale <= 50000) { parametro = 2.620; }
						if(totale >= 50001 && totale <= 100000) { parametro = 2.613; }
						if(totale >= 100001) { parametro = 2.605; }
	
					}
					
					else if(mesi_noleggio == 60) {
					
						if(totale <= 5000) { parametro = 2.320; }
						if(totale >= 5001 && totale <= 10000) { parametro = 2.252; }
						if(totale >= 10001 && totale <= 25000) { parametro = 2.221; }
						if(totale >= 25001 && totale <= 50000) { parametro = 2.190; }
						if(totale >= 50001 && totale <= 100000) { parametro = 2.182; }
						if(totale >= 100001) { parametro = 2.175; }
	
					}
					document.getElementById("parametro_noleggio").value = parametro;
					
					var rata_mensile = totale*(parametro/100);
					
					rata_mensile = rata_mensile.toFixed(2);
					
					document.getElementById("importo_rata_mensile_noleggio").value = rata_mensile.replace(".",",");
					
					if(mesi_noleggio == 0) {
					
						document.getElementById("importo_rata_mensile_noleggio").value = 0;
						document.getElementById("parametro_noleggio").value = 0;
					
					}
					
				}
			
			
			function calcolaSpedizionePerIndirizzo() {
				var totale = 0;
				var totale_con_iva = 0;
				var arrayImporti = document.getElementsByClassName("importo");
					for (var i = 0; i < arrayImporti.length; ++i) {
						var item = parseFloat(arrayImporti[i].value);  
						totale += item;
					}
				var address_delivery = document.getElementById("id_address_delivery").value;
					var carrier_cart = $("input:radio[name=metodo]:checked").val();
					var trasporto_modificato = document.getElementById("valore_trasporto_modificato").value;
					trasporto_modificato = trasporto_modificato.replace(",",".");
					if(trasporto_modificato == "") { trasporto_modificato = 0; }
					
					';
				
							if($customer->id_default_group == 1) {
				
					echo '	
						if(totale < 399) {
							var supplemento_contrassegno = 3.5;
						}
						else {
							var supplemento_contrassegno = 0;
							
						}';
					}
						
					else if($customer->id_default_group  == 3) {
							
						echo 'if(totale < 516) {
							var supplemento_contrassegno = 5;
						}
						else {
							var supplemento_contrassegno = ((totale/100)*1.5);
						
						}';
							
					}
						
					else {
						echo 'if(totale < 516) {
							var supplemento_contrassegno = 5;
						}
						else {
							var supplemento_contrassegno = ((totale/100)*1.5);
							
						}';
					
					}
					
					echo '
					supplemento_contrassegno = supplemento_contrassegno.toFixed(2);
									
					document.getElementById(\'supplemento_contrassegno\').innerHTML=supplemento_contrassegno.replace(".",",");	
					
					$.ajax({
						type: "POST",
						async: true,
						url: "calcola_costo_spedizione.php",
						data: "id_cart='.$cart->id.'&totale="+totale+"&address_delivery="+address_delivery+"&carrier_cart="+carrier_cart+"&trasporto_modificato="+trasporto_modificato,
						success: function(resp)
						{
							
							var values = resp.split("|");
							var carri = parseFloat(values[0]);
							var trasp = parseFloat(values[1]);
							trasp = parseFloat(trasp);
							totale = parseFloat(totale);
							totale = totale+trasp;
							
							
							totalep = totale;
							totale = totale.toFixed(2);
								
				
							trasp = trasp.toFixed(2);
							trasp = trasp.replace(".",",");
							document.getElementById(\'metodo_costo[\'+carrier_cart+\']\').innerHTML=trasp;
							document.getElementById(\'transport\').value=trasp;
							document.getElementById(\'importo_trasporto\').innerHTML=trasp;
							document.getElementById(\'totaleprodotti\').value=totale;
							document.getElementById(\'spantotaleprodotti\').innerHTML=totale.replace(".",",");		
							
							$.ajax({
								url:"ajax_products_list2.php?calcola_totale_con_iva=y",
								type: "POST",
								data: { address: address_delivery,
								customer : '.$customer->id.',
								totalep : totalep
								 },
								success:function(resp){
									totale_con_iva = resp;
									totale_con_iva = parseFloat(totale_con_iva);
									totale_con_iva = totale_con_iva.toFixed(2);
									document.getElementById(\'spantotaleprodottiiva\').innerHTML=totale_con_iva.replace(".",",");		
								},
								error: function(xhr,stato,errori){
									alert("Errore nella cancellazione:"+xhr.status);
								}
								
								
							});
							
						},
						error: function(XMLHttpRequest, textStatus, errorThrown)
						{
							alert("ERROR"+XMLHttpRequest+" "+textStatus+" "+errorThrown);
						
						}
								
					});	

					
									
					
							
				
			}
			
			function vediTrasportoGratuito() {
			
				if(document.getElementById("transport").value == 0) {
					$("input:radio[class=gratis]").attr("checked", true);
				}
			
				else {
					$("input:radio[class=a_pagamento]").attr("checked", true);
				}
				
			
			}
				
			</script>

';
$tokenProducts = $tokenCarts = Tools::getAdminToken('AdminCatalogExFeatures'.(int)(Tab::getIdFromClassName('AdminCatalogExFeatures')).(int)($cookie->id_employee));
$excludeIds = "";
	$array_prodotti_da_escludere = Db::getInstance()->executeS("select id_product FROM cart_product WHERE id_cart=".$_GET['id_cart']."");
			foreach($array_prodotti_da_escludere as $escl) {
			
				$excludeIds .= $escl['id_product'].",";
			
			}
echo '<input id="excludeIds" type="hidden" value="'.$excludeIds.'" />';			
			
 echo '
 	<script type="text/javascript" src="../themes/ezdirect/js/jquery.tooltip.js"></script>
			<style type="text/css">
			#tooltip {
				position: absolute;
				z-index: 3000;
				width:350px;
				border: 1px solid #111;
				background-color: #eee;
				padding: 5px;
				text-align:left;
			}
			#tooltip h3, #tooltip div { text-align:left; }
			</style>
			
			<script type="text/javascript">
				$(function() {
					$(".span-reference").tooltip({ showURL: false  });
				});
			</script>	
 
 
			<script type="text/javascript">
			var prodotti_nel_carrello = ['.$excludeIds.'0];
 
				function addProduct_TR(event, data, formatted)
				{
					if(data[6] == "*BUNDLE*") {
						var prodotti_bundle = data[7].split(";");
						for(i=0;i<((prodotti_bundle.length))-1;i++)
						{
							$.ajax({
							type: "GET",
							data: "id_cart='.$_GET['id_cart'].'&id_bundle="+data[1]+"&product_in_bundle="+prodotti_bundle[i],
							async: false,
							url: "ajax_products_list2.php",
							success: function(resp)
								{
									addProduct_TR("",resp.split("|"),"");
								},
								error: function(XMLHttpRequest, textStatus, errorThrown)
								{
									tooltip_content = "";
									alert("ERROR" + textStatus + " " + XMLHttpRequest.responseText);					
								}
														
							});
						}
						return false;
					
					}
							
					document.getElementById("product_autocomplete_input").value = ""; 
					var productId = data[1];
					var productPrice = data[2];
					var productScQta = data[3];
					var productRef = data[4];
					var productName = data[5];
					var productQuantity = data[16];
					var tdImporto = data[17];
					var przUnitario = data[18];
					var deleteProd = data[19];
					var marg = data[20];
					var scontoExtra = data[21];
					var sc_qta_1 = data[6];
					var sc_qta_2 = data[7];
					var sc_qta_3 = data[8];
					var sc_riv_1 = data[9];
					var sc_riv_2 = data[10];
					var sc_riv_3 = data[26];
					var sc_qta_1_q = data[11];	
					var sc_qta_2_q = data[12];	
					var sc_qta_3_q = data[13];	
					var sc_riv_1_q = data[14];	
					var sc_riv_2_q = data[15];	
					var sc_riv_3_q = data[27];	
					var acquisto = data[30];	
					
					prodotti_nel_carrello.push(parseInt(productId));
					
					var varspec = data[24];
					var textVarSpec = "";
					
					var excludeIds = document.getElementById("excludeIds").value;
					excludeIds = excludeIds+productId+",";
					document.getElementById("excludeIds").value = excludeIds;
					$("#product_autocomplete_input").val("");
					
					
					$("#product_autocomplete_input").setOptions({
						extraParams: {excludeIds : document.getElementById("excludeIds").value}
					});	
					
					$("#product_autocomplete_input_interno").setOptions({
						extraParams: {excludeIds : document.getElementById("excludeIds").value}
					});
							
					if(varspec == 0) {
					}
					else {
						
						textVarSpec = "style=\'border:1px solid red\'"
					}
					
					$("<tr " + textVarSpec + " id=\'tr_"+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"\'><td><a href=\'index.php?tab=AdminCatalogExFeatures&id_product="+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"&updateproduct&token='.$tokenProducts.'\' target=\'_blank\'>" + productRef + "</a></td><td><input name=\'nuovo_name["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' size=\'53\' type=\'text\' value=\'"+ productName +"\' /></td><td> " + productQuantity + "</td><td>" + productPrice + "</td><td>" + scontoExtra + "</td><td>" + acquisto + "</td>" + marg + "<td style=\'text-align:center\'> " + productScQta + "</td>" + tdImporto + "<td class=\'pointer dragHandle center\' style=\'background:url(../img/admin/up-and-down.gif) no-repeat center;\'><input type=\'hidden\' name=\'sort_order["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' /></td><td style=\'text-align:center\'>" + deleteProd + "<input name=\'nuovo_prodotto["+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"]\' type=\'hidden\' value=\'"+productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "")+"\' />" + sc_qta_1 + " " + sc_qta_2 + " " + sc_qta_3 + " " + sc_riv_1 + " " + sc_riv_2 + " " + sc_riv_3 + " " + sc_qta_1_q + " " + sc_qta_2_q + " " + sc_qta_3_q + " " + sc_riv_1_q + " " + sc_riv_2_q + " " + sc_riv_3_q + " " + przUnitario + "</td></tr>").appendTo("#tableProductsBody");
					
					
					'; 
					//CONTROLLO PER COUPON CHEAPNET
								$conto_iniziale = Db::getInstance()->getValue("SELECT COUNT(usato) FROM cheapnetcoupons WHERE usato = 0");
								if($conto_iniziale == 0) {
								}
								else {
									$prodotti_con_coupon = Db::getInstance()->getValue("SELECT value FROM configuration WHERE name = 'PS_CHEAPNET_COUPONS'");
					
									$prodotti = explode("-", $prodotti_con_coupon);

									$prodotti_da_6 = str_replace(";",",",$prodotti[0]);
									$prodotti_da_30 = str_replace(";",",",$prodotti[1]);
									
									$conto_iniziale_6 = Db::getInstance()->getValue("SELECT COUNT(usato) FROM cheapnetcoupons WHERE tipo = 6 AND usato = 0");
									if($conto_iniziale_6 == 0) {
									}
									else {
										echo 'var prodotti_da_6 = ['.$prodotti_da_6.'0]; ';
									}
									
									$conto_iniziale_30 = Db::getInstance()->getValue("SELECT COUNT(usato) FROM cheapnetcoupons WHERE tipo = 30 AND usato = 0");
									if($conto_iniziale_30 == 0) {
										echo 'var prodotti_da_30 = [0]; ';
									}
									else {
										echo 'var prodotti_da_30 = ['.$prodotti_da_30.'0]; ';
									}
								}
								// FINE CONTROLLO PER COUPON CHEAPNET
					
					 echo '
						
						/*
						if($.inArray(parseInt(productId),prodotti_da_6) > -1){
							if($.inArray(31110,prodotti_nel_carrello) == -1){
								$(\'<tr style="cursor: move;" id="tr_31110">    <td>CHEAP-300-O</td>  <td><input size="55" name="nuovo_name[31110]" value="OMAGGIO Traffico telefonico VoIP 300 minuti" type="text"></td>  <td><input style="text-align:right" name="nuovo_quantity[31110]" id="product_quantity[31110]" size="1" value="1" onkeyup="if (isArrowKey(event)) return; calcolaImporto(31110);" type="text"></td>  <td><input size="7" style="text-align:right" onkeyup="calcolaImporto(31110);" name="nuovo_price[31110]" id="product_price[31110]" value="0,00" type="text"></td>  <td></td>  <td style="text-align:right"></td>  <td style="text-align:center"><input id="usa_sconti_quantita[31110]" name="usa_sconti_quantita[31110]" value="0" type="hidden"> <input id="impImporto[31110]" class="importo" value="0" type="hidden"></td>  <td style="text-align:right" id="valoreImporto[31110]">0,00</td><td class="pointer dragHandle center" style="background:url(../img/admin/up-and-down.gif) no-repeat center;"><input name="sort_order[31110]" type="hidden"></td>  <td style="text-align:center"><a style="cursor:pointer" onclick="togliImporto(31110); delProduct30(31110);"><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a></td><input name="nuovo_prodotto[31110]" value="31110" type="hidden"><input name="product_id[31110]" value="31110" type="hidden"><input id="wholesale_price[31110]" value="0.000000" type="hidden"><input id="unitario[31110]" value="0.000000" type="hidden"><input id="sc_qta_1[31110]" value="0" type="hidden"><input id="sc_qta_2[31110]" value="0" type="hidden"><input id="sc_qta_3[31110]" value="0" type="hidden"><input id="sc_riv_1[31110]" value="0" type="hidden"><input id="sc_riv_2[31110]" value="0" type="hidden"><input id="sc_qta_1_q[31110]" value="" type="hidden"><input id="sc_qta_2_q[31110]" value="" type="hidden"><input id="sc_qta_3_q[31110]" value="" type="hidden"><input id="sc_riv_1_q[31110]" value="" type="hidden"><input id="sc_riv_2_q[31110]" value="" type="hidden"><input id="oldQuantity[31110]" value="1" type="hidden"><input id="oldPrice[31110]" value="0.000000" type="hidden"><td>31110</td></tr>\').appendTo("#tableProductsBody");
								prodotti_nel_carrello.push(31110);
							} else { }
						} 
						else if($.inArray(parseInt(productId),prodotti_da_30) > -1){
							if($.inArray(31109,prodotti_nel_carrello) == -1){
								$(\'<tr style="cursor: move;" id="tr_31109">   <td>CHEAP-1500-O</td>  <td><input size="55" name="nuovo_name[31109]" value="OMAGGIO Traffico telefonico VoIP 1500 minuti" type="text"></td>  <td><input style="text-align:right" name="nuovo_quantity[31109]" id="product_quantity[31109]" size="1" value="1" onkeyup="if (isArrowKey(event)) return; calcolaImporto(31109);" type="text"></td>  <td><input size="7" style="text-align:right" onkeyup="calcolaImporto(31109);" name="nuovo_price[31109]" id="product_price[31109]" value="0,00" type="text"></td>  <td></td>  <td style="text-align:right"></td>  <td style="text-align:center"><input id="usa_sconti_quantita[31109]" name="usa_sconti_quantita[31109]" value="0" type="hidden"> <input id="impImporto[31109]" class="importo" value="0" type="hidden"></td>  <td style="text-align:right" id="valoreImporto[31109]">0,00</td><td class="pointer dragHandle center" style="background:url(../img/admin/up-and-down.gif) no-repeat center;"><input name="sort_order[31109]" type="hidden"></td>  <td style="text-align:center"><a style="cursor:pointer" onclick="togliImporto(31109); delProduct30(31109);"><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a></td><input name="nuovo_prodotto[31109]" value="31109" type="hidden"><input name="product_id[31109]" value="31109" type="hidden"><input id="wholesale_price[31109]" value="0.000000" type="hidden"><input id="unitario[31109]" value="0.000000" type="hidden"><input id="sc_qta_1[31109]" value="0" type="hidden"><input id="sc_qta_2[31109]" value="0" type="hidden"><input id="sc_qta_3[31109]" value="0" type="hidden"><input id="sc_riv_1[31109]" value="0" type="hidden"><input id="sc_riv_2[31109]" value="0" type="hidden"><input id="sc_qta_1_q[31109]" value="" type="hidden"><input id="sc_qta_2_q[31109]" value="" type="hidden"><input id="sc_qta_3_q[31109]" value="" type="hidden"><input id="sc_riv_1_q[31109]" value="" type="hidden"><input id="sc_riv_2_q[31109]" value="" type="hidden"><input id="oldQuantity[31109]" value="1" type="hidden"><input id="oldPrice[31109]" value="0.000000" type="hidden"> <td>31109</td></tr>\').appendTo("#tableProductsBody");
								prodotti_nel_carrello.push(31109);
							}
						}
						else {
						}
						*/
						
						var string_prodotti_nel_carrello = "";
						for (var i = 0; i < prodotti_nel_carrello.length; ++i) {
							var item = parseFloat(prodotti_nel_carrello[i]);  
							string_prodotti_nel_carrello += item+\',\';
						}
						string_prodotti_nel_carrello += "0";
						/*
						document.getElementById("excludeIds").value = string_prodotti_nel_carrello;
						
						$("#product_autocomplete_input").val("");
							$("#product_autocomplete_input").setOptions({
								extraParams: {excludeIds : document.getElementById("excludeIds").value}
							});	
						
						$("#product_autocomplete_input_interno").val("");
							$("#product_autocomplete_input_interno").setOptions({
								extraParams: {excludeIds : document.getElementById("excludeIds").value}
							});	
						*/
					$(".wholesale_price_class").unbind();
					
					$(".wholesale_price_class").each(function() {
				
					
							var productId = $(this).attr("id").substring($(this).attr("id").indexOf("[") + 1,$(this).attr("id").indexOf("]",$(this).attr("id").indexOf("[") + 1));
							
							var acq = document.getElementById("wholesale_price["+productId+"]"); var last = acq.value;
								
								var ctrl_m_c = 0;
								';
								/*
								if(ctrl_m_c == 0) {
									$(document.getElementById("wholesale_price["+productId+"]")).one(\'click\', function() {
										alert(\'Attenzione: stai cambiando il prezzo di acquisto\');
										ctrl_m_c = 1;
									});
								}
								
								if(ctrl_m_c == 0) {
									$(document.getElementById("wholesale_price["+productId+"]")).one(\'keydown\', function(event) {
									
										var code = (event.keyCode ? event.keyCode : event.which);
										
										if(code == 9 || code == 37 || code == 38 || code == 39 || code == 40) {
											
										}
										else {
											alert(\'Attenzione: stai cambiando il prezzo di acquisto\');
											ctrl_m_c = 1;
										}
									});
								}
								*/
								
								echo '
								// Setup the event
								$(document.getElementById("wholesale_price["+productId+"]")).focusout(function() {
									ctrl_m_c = 0;
									if (last != $(this).val()) {
										
										var surec = window.confirm(\'Stai cambiando un prezzo di acquisto. Sei sicuro?\'); 
										if (surec) { return true; } else { document.getElementById("wholesale_price["+productId+"]").value = last; }
									}
								});
							});
					
					
					
					
					$("#tableProducts").tableDnD({
						onDrop: function(table, row) {
						},
						dragHandle: ".dragHandle"
					});
									
					$("#tableProducts").hover(function() {
						  $(this.cells[8]).addClass(\'showDragHandle\');
					}, function() {
						  $(this.cells[8]).removeClass(\'showDragHandle\');
					});
					
					
					
			
					
					productId = productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "");
					
					$("#span-reference-"+productId+"").tooltip({ showURL: false  });
					
					
				
					
					
					var postdata =  $("#products").serialize().replace(/%5B/g, "[").replace(/%5D/g, "]");
							
					$.ajax({
					   type: "POST",
					   url: \''.$currentIndex.'&id_cart='.$_GET['id_cart'].'&id_customer='.$customer->id.'&viewcustomer&ajaxsave&viewcart&cartupdated&tab-container-1=4&token='.$this->token.'#modifica-carrello\',
					   data: postdata+"&Apply=Conferma", 
					   success: function(data)
					   {
						  calcolaSpedizionePerIndirizzo();	
					   }
					 });
					 
					 calcolaImporto(productId);
					 
				}

				
				
			
				
			</script>
			
			
			
			
			
			
			
			';
			
							
			if($attachment != '') {
				echo 'Allegati: ';
				if(!empty($attachment)) {
							
					$allegati = explode(";",$attachment);
					$nall = 1;
					foreach($allegati as $allegato) {
						
						if(strpos($allegato, ":::")) {
						
							$parti = explode(":::", $allegato);
							$nomevero = $parti[1];
										
						}
						
						else {
							$nomevero = $allegato;
						
						}
									
						if($allegato == "") { } else {
							if($nall == 1) { } else { echo " - "; }
							echo '<a href="index.php?tab=AdminCarts&id_cart='.$cart->id.'&viewcart&token='.$tokenCarts.'&filename='.$allegato.'"><span style="color:red">'.$nomevero.'</span></a>';
							$nall++;
						}
					}
							
				}
							
							else { echo 'Nessun allegato'; }
			}
			if($attachment != '') { echo '<br />';
			
			echo '<input name="cancella_tutti_allegati" type="checkbox" /> Cancella tutti gli allegati';
			
			echo '<br />';
			} else { }
			
			if($order->id) { } else {
			echo '<input name="id_cart" type="hidden" value="'.$_GET['id_cart'].'" />
			Seleziona il prodotto da aggiungere: <input size="123" type="text" value="" id="product_autocomplete_input" /><br /><br />
			
			PRODOTTI DELLA CATEGORIA "LISTINO INTERNO": <input size="123" type="text" value="" id="product_autocomplete_input_interno" /><br /><br />
			<script type="text/javascript">
					var formProduct = "";
					var products = new Array();
				</script>
				<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete.js"></script>
				
				
				<script type="text/javascript">
					urlToCall = null;
					
					function getExcludeIds() {
						var ids = "";
						ids += $(\'#excludeIds\').val().replace(/\\-/g,\',\').replace(/\\,$/,\'\');
						return ids;
					}
					
					/* function autocomplete */
					$(function() {
						$(\'#product_autocomplete_input\')
							.autocomplete(\'ajax_products_list2.php?id_cart='.$_GET['id_cart'].'\', {
								minChars: 2,
								autoFill: false,
								max:50,
								matchContains: true,
								matchSubset: 0,
								mustMatch:true,
								scroll:true,
								cacheLength:1,
								formatItem: function(item) {
							
											var color = "";
											if(item[24] == 1) {
												var color = "; color:red";
											}
											if(item[28] == 0) {
												return \'</table><div style="position:absolute; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:150px;float:left;margin-left:10px;">Codice</div></th><th style="width:570px;"><div style="width:550px; float:left;">Desc. prodotto<br /><br /></div></th><th style="width:60px; text-align:right"><div style="width:60px;float:left;">Prezzo</div></th><th style="width:50px; text-align:right"><div style="width:60px;float:left;">Qt. mag.</div></th><th style="width:50px; text-align:right"><div style="width:75px;float:left;">Qt. tot</div></th></tr></div><table><tr><td style="width:145px; text-align:left"><br /><br />\'+item[4]+\'</td><td style="width:555px\'+color+\'"><br /><br />\'+item[5]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[23]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[25]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[22]+\'</td></tr></table>\'
												
												;
											}
											else {
												return \'<tr><td style="width:150px; text-align:left">\'+item[4]+\'</td><td style="width:550px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
												
												;
												}
											}
								
							}).result(addProduct_TR);
							
							$("#product_autocomplete_input").setOptions({
								extraParams: {excludeIds : getExcludeIds()}
							});		
						
					});
				
					$("#product_autocomplete_input").css(\'width\',\'905px\');
					$("#product_autocomplete_input").keypress(function(event){
								
								  var keycode = (event.keyCode ? event.keyCode : event.which);
								  if (keycode == "13") {
									event.preventDefault();
									event.stopPropagation();    
								  }
								});
								
						$(function() {
						$(\'#product_autocomplete_input_interno\')
							.autocomplete(\'ajax_products_list2_interno.php?id_cart='.$_GET['id_cart'].'\', {
								minChars: 2,
								autoFill: false,
								max:50,
								matchContains: true,
								matchSubset: 0,
								mustMatch:true,
								scroll:true,
								cacheLength:1,
								formatItem: function(item) {
							
											var color = "";
											if(item[24] == 1) {
												var color = "; color:red";
											}
											if(item[28] == 0) {
												return \'</table><div style="position:absolute; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:150px;float:left;margin-left:10px;">Codice</div></th><th style="width:570px;"><div style="width:550px; float:left;">Desc. prodotto<br /><br /></div></th><th style="width:60px; text-align:right"><div style="width:60px;float:left;">Prezzo</div></th><th style="width:50px; text-align:right"><div style="width:60px;float:left;">Qt. mag.</div></th><th style="width:50px; text-align:right"><div style="width:75px;float:left;">Qt. tot</div></th></tr></div><table><tr><td style="width:145px; text-align:left"><br /><br />\'+item[4]+\'</td><td style="width:555px\'+color+\'"><br /><br />\'+item[5]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[23]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[25]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[22]+\'</td></tr></table>\'
												
												;
											}
											else {
												return \'<tr><td style="width:150px; text-align:left">\'+item[4]+\'</td><td style="width:550px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
												
												;
												}
											}
								
							}).result(addProduct_TR);
							
							$("#product_autocomplete_input_interno").setOptions({
								extraParams: {excludeIds : getExcludeIds()}
							});		
						
					});
				
					$("#product_autocomplete_input_interno").css(\'width\',\'905px\');
					$("#product_autocomplete_input_interno").keypress(function(event){
								
								  var keycode = (event.keyCode ? event.keyCode : event.which);
								  if (keycode == "13") {
									event.preventDefault();
									event.stopPropagation();    
								  }
								});
								
					
				</script>
				
				
				KIT <input size="123" type="text" value="" id="product_autocomplete_input_kit" /><br /><br /> 
			<script type="text/javascript">
					var formProduct = "";
					var products = new Array();
				</script>
				<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete.js"></script>
				
				
				<script type="text/javascript">
					urlToCall = null;
					
					/* function autocomplete */
					$(function() {
						$(\'#product_autocomplete_input_kit\')
							.autocomplete(\'ajax_products_list2.php?bundles=y&id_cart='.$_GET['id_cart'].'\', {
								minChars: 2,
								autoFill: false,
								max:50,
								matchContains: true,
								matchSubset: 0,
								mustMatch:true,
								scroll:true,
								cacheLength:1,
								formatItem: function(item) {
							
											var color = "";
											if(item[24] == 1) {
												var color = "; color:red";
											}
											if(item[28] == 0) {
												return \'</table><div style="position:absolute; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:150px;float:left;margin-left:10px;">Codice</div></th><th style="width:570px;"><div style="width:550px; float:left;">Desc. kit<br /><br /></div></th><th style="width:60px; text-align:right"><div style="width:60px;float:left;">Prezzo</div></th><th style="width:50px; text-align:right"><div style="width:60px;float:left;">Qt. mag.</div></th><th style="width:50px; text-align:right"><div style="width:75px;float:left;">Qt. tot</div></th></tr></div><table><tr><td style="width:145px; text-align:left"><br /><br />\'+item[4]+\'</td><td style="width:555px\'+color+\'"><br /><br />\'+item[5]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[23]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[25]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[22]+\'</td></tr></table>\'
												
												;
											}
											else {
												return \'<tr><td style="width:150px; text-align:left">\'+item[4]+\'</td><td style="width:550px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
												
												;
												}
											}
								
							}).result(addProduct_TR);
						
						
					});
				
					$("#product_autocomplete_input_kit").css(\'width\',\'905px\');
					$("#product_autocomplete_input_kit").keypress(function(event){
								
								  var keycode = (event.keyCode ? event.keyCode : event.which);
								  if (keycode == "13") {
									event.preventDefault();
									event.stopPropagation();    
								  }
								});
								
					
				</script>';
			
			}
				
				echo '
			<table width="100%" class="table" id="tableProducts">
			<thead>
			  <tr>
				
				<th style="width:130px">Codice + Dati</th>
				<th style="width:310px">Nome prodotto</th>
				<th style="width:45px">Qta</th>
				<th style="width:75px">Unitario</th>
				
				
				<th style="width:45px; text-align:center">Sconto<br />extra</th>
				<th style="width:75px">Acquisto</th>
				<th style="width:55px">Marg</th>
				<th style="width:55px; text-align:center">'.($customer->id_default_group == 3 || $customer->id_default_group == 12 ? 'Sc. riv?' : 'Sc. qta?').'</th>
				<th style="width:85px">Importo</th>
				<th style="width:10px"><img src="../img/admin/up-and-down.gif" alt="Ordina" title="Ordina" /></th>
				<th style="width:10px"><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></th>
				<!-- <th style="width:45px">ID</th> -->
			  </tr>
			  </thead>
			  <tbody id="tableProductsBody">';
			  
		if(Tools::getIsset('vedirevisione'))
		{
			mysql_select_db(_DB_REV_NAME_);
			$query="select cp.*
			
			from ". _DB_PREFIX_."cart_product cp 
			
			WHERE id_revisione = ".Tools::getValue('vedirevisione')." AND id_cart=".$_GET['id_cart']." ORDER BY cp.sort_order ASC";
			
			$res1=mysql_query($query) or die(mysql_error());
			
		}
		else 
		{
			 
			$query="select cp.*,t.rate as tax_rate,p.quantity as stock,p.id_tax_rules_group,p.price as product_price,p.listino as listino,p.wholesale_price as acquisto,p.sconto_acquisto_1 as sc_acq_1, p.sconto_acquisto_2 as sc_acq_2, p.sconto_acquisto_3 as sc_acq_3, p.quantity as qt_tot, p.stock_quantity as magazzino, p.supplier_quantity as allnet, p.eds_quantity as eds, p.ordinato_quantity as qt_ordinato, p.impegnato_quantity as qt_impegnato, p.arrivo_quantity as qt_arrivo, p.arrivo_eds_quantity as qt_arrivo_eds, p.reference as product_reference,pl.name as product_name from ". _DB_PREFIX_."cart_product cp left join ". _DB_PREFIX_."product p on  cp.id_product=p.id_product left join ". _DB_PREFIX_."product_lang pl  on  cp.id_product=pl.id_product 
			LEFT JOIN `"._DB_PREFIX_."tax_rule` tr ON (p.`id_tax_rules_group` = tr.`id_tax_rules_group`
			AND tr.`id_country` = ".(int)Country::getDefaultCountryId()."
			AND tr.`id_state` = 0)
			LEFT JOIN `"._DB_PREFIX_."tax` t ON (t.`id_tax` = tr.`id_tax`)
			
			WHERE pl.id_lang = 5 
			AND id_cart=".$_GET['id_cart']." ORDER BY cp.sort_order ASC";
			$res1=mysql_query($query);
		}
			 
			 
			 if(Tools::getIsset('vedirevisione'))
		{
			mysql_select_db(_DB_NAME_);
		}
		
			if (mysql_num_rows($res1)>0) {
			
				
			
			$totale = 0;
				while ($products=mysql_fetch_array($res1)) {
					if(Tools::getIsset('vedirevisione'))
					{
						$products['tax_rate'] = Db::getInstance()->getValue('SELECT rate AS tax_rate FROM tax WHERE id_tax = 1');
						$products['stock'] = Db::getInstance()->getValue('SELECT quantity FROM product WHERE id_product = '.$products['id_product']);
						$products['id_tax_rules_group'] = Db::getInstance()->getValue('SELECT id_tax_rules_group FROM product WHERE id_product = '.$products['id_product']);
						$products['product_price'] = Db::getInstance()->getValue('SELECT price FROM product WHERE id_product = '.$products['id_product']);
						$products['listino'] = Db::getInstance()->getValue('SELECT listino FROM product WHERE id_product = '.$products['id_product']);
						$products['acquisto'] = Db::getInstance()->getValue('SELECT wholesale_price FROM product WHERE id_product = '.$products['id_product']);
						$products['sc_acq_1'] = Db::getInstance()->getValue('SELECT sconto_acquisto_1 FROM product WHERE id_product = '.$products['id_product']);
						$products['sc_acq_2'] = Db::getInstance()->getValue('SELECT sconto_acquisto_2 FROM product WHERE id_product = '.$products['id_product']);
						$products['sc_acq_3'] = Db::getInstance()->getValue('SELECT sconto_acquisto_3 FROM product WHERE id_product = '.$products['id_product']);
						$products['qt_tot'] = Db::getInstance()->getValue('SELECT quantity FROM product WHERE id_product = '.$products['id_product']);
						$products['magazzino'] = Db::getInstance()->getValue('SELECT stock_quantity FROM product WHERE id_product = '.$products['id_product']);
						$products['allnet'] = Db::getInstance()->getValue('SELECT supplier_quantity FROM product WHERE id_product = '.$products['id_product']);
						$products['eds'] = Db::getInstance()->getValue('SELECT eds_quantity FROM product WHERE id_product = '.$products['id_product']);
						$products['qt_ordinato'] = Db::getInstance()->getValue('SELECT ordinato_quantity FROM product WHERE id_product = '.$products['id_product']);
						$products['qt_impegnato'] = Db::getInstance()->getValue('SELECT impegnato_quantity FROM product WHERE id_product = '.$products['id_product']);
						$products['qt_arrivo'] = Db::getInstance()->getValue('SELECT arrivo_quantity FROM product WHERE id_product = '.$products['id_product']);
						$products['qt_arrivo_eds'] = Db::getInstance()->getValue('SELECT arrivo_eds_quantity FROM product WHERE id_product = '.$products['id_product']);
						$products['product_reference'] = Db::getInstance()->getValue('SELECT reference FROM product WHERE id_product = '.$products['id_product']);
						$products['product_name'] = Db::getInstance()->getValue('SELECT name FROM product_lang WHERE id_lang = 5 AND id_product = '.$products['id_product']);
						
					}	
				
					$tax_rate=$products['tax_rate'];
						$sc_qta_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_1' AND id_product = '".$products['id_product']."'");
						$sc_qta_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_2' AND id_product = '".$products['id_product']."'");
						$sc_qta_3_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_3' AND id_product = '".$products['id_product']."'");	
						$sc_riv_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_1' AND id_product = '".$products['id_product']."'");	
						$sc_riv_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_2' AND id_product = '".$products['id_product']."'");
						$sc_riv_3_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_3' AND id_product = '".$products['id_product']."'");
						
						$sc_qta_1 = $sc_qta_1_r['price'] - ($sc_qta_1_r['price'] * $sc_qta_1_r['reduction']);
						$sc_qta_2 = $sc_qta_2_r['price'] - ($sc_qta_2_r['price'] * $sc_qta_2_r['reduction']);
						$sc_qta_3 = $sc_qta_3_r['price'] - ($sc_qta_3_r['price'] * $sc_qta_3_r['reduction']);
						$sc_riv_1 = $sc_riv_1_r['price'] - ($sc_riv_1_r['price'] * $sc_riv_1_r['reduction']);
						$sc_riv_2 = $sc_riv_2_r['price'] - ($sc_riv_2_r['price'] * $sc_riv_2_r['reduction']);
						$sc_riv_3 = $sc_riv_3_r['price'] - ($sc_riv_3_r['price'] * $sc_riv_3_r['reduction']);
						
						$unitario = Db::getInstance()->getValue("SELECT price FROM product WHERE id_product = '".$products['id_product']."'");
						
						$sc_qta_1_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_1' AND id_product = '".$products['id_product']."'");
						$sc_qta_2_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_2' AND id_product = '".$products['id_product']."'");
						$sc_qta_3_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_3' AND id_product = '".$products['id_product']."'");
						
						$sc_riv_1_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_1' AND id_product = '".$products['id_product']."'");
						$sc_riv_2_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_2' AND id_product = '".$products['id_product']."'");
						$sc_riv_3_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_3' AND id_product = '".$products['id_product']."'"); 
						
						$speciale = Product::trovaPrezzoSpeciale($products['id_product'], 1, 0);
			
						if($speciale < $unitario && $speciale != 0) {
					
							$unitario = $speciale;
			
						}
						else {
							
						}
						

						if($customer->id_default_group == 3) {
							$ctrl_sc_qt = Db::getInstance()->getValue("SELECT count(id_product) FROM specific_price WHERE id_product = ".$products['id_product']." AND (specific_price_name = 'sc_riv_1' OR specific_price_name = 'sc_riv_2')");
							
							if($ctrl_sc_qt > 0) {
								$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['id_product'].']" name="usa_sconti_quantita['.$products['id_product'].']" type="checkbox" '.($products['sc_qta'] == 0 ? '' : 'checked="checked"').' onclick="ristabilisciPrezzo('.$products['id_product'].'); calcolaImporto('.$products['id_product'].'); " />';
							}
							else {
								$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['id_product'].']" name="usa_sconti_quantita['.$products['id_product'].']" type="hidden" value="0" />';
							}
						}
						
						else if($customer->id_default_group == 3) {
							$ctrl_sc_qt = Db::getInstance()->getValue("SELECT count(id_product) FROM specific_price WHERE id_product = ".$products['id_product']." AND (specific_price_name = 'sc_riv_1' OR specific_price_name = 'sc_riv_2' OR specific_price_name = 'sc_riv_3')");
							
							if($ctrl_sc_qt > 0) {
								$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['id_product'].']" name="usa_sconti_quantita['.$products['id_product'].']" type="checkbox" '.($products['sc_qta'] == 0 ? '' : 'checked="checked"').' onclick="ristabilisciPrezzo('.$products['id_product'].'); calcolaImporto('.$products['id_product'].'); " />';
							}
							else {
								$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['id_product'].']" name="usa_sconti_quantita['.$products['id_product'].']" type="hidden" value="0" />';
							}
						}
						
						else {
							$ctrl_sc_qt = Db::getInstance()->getValue("SELECT count(id_product) FROM specific_price WHERE id_product = ".$products['id_product']." AND (specific_price_name = 'sc_qta_1' OR specific_price_name = 'sc_qta_2' OR specific_price_name = 'sc_qta_3')");
							
							if($ctrl_sc_qt > 0) {
								$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['id_product'].']" name="usa_sconti_quantita['.$products['id_product'].']" type="checkbox" '.($products['sc_qta'] == 0 ? '' : 'checked="checked"').' onclick="ristabilisciPrezzo('.$products['id_product'].'); calcolaImporto('.$products['id_product'].'); " />';
							}
							else {
								$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$products['id_product'].']" name="usa_sconti_quantita['.$products['id_product'].']" type="hidden" value="0" />';
							}
						}
					  
					//if($cart->date_upd > '2014-10-25') {
					//	$products['acquisto'] = $products['prezzo_acquisto'];
					//}
					//else {
					//	if($products['prezzo_acquisto'] > 0) 
							$products_acquisto = $products['prezzo_acquisto'];
					//}
						
						
					  echo '<tr id="tr_'.$products['id_product'].'">';
					
					  echo '  <td> <script type="text/javascript">calcolaImporto('.$products['id_product'].');</script><span style="cursor:pointer" class="span-reference" title="
					  '.($cookie->id_employee == 1 || $cookie->id_employee == 2 || $cookie->id_employee == 6 || $cookie->id_employee == 7 ? '<strong>Prezzo netto acquisto</strong>: '.Tools::displayPrice(($products['acquisto'] > 0 ? $products['acquisto'] : ($products['listino']*(100-$products['sc_acq_1'])/100)*((100-$products['sc_acq_2'])/100)*((100-$products['sc_acq_3'])/100))).'<br />' : '').'
					  <strong>Prezzo listino</strong>: '.Tools::displayPrice($products['listino']).'<br />
					  <hr />
					  <strong>Qta disponibile ALLNET</strong>: '.$products['allnet'].' pz.
					  <br />
					  <strong>Qta disponibile EDS</strong>: '.$products['eds'].' pz.<br />
					  <strong>Qta disponibile MAGAZZINO</strong>: '.$products['magazzino'].' pz.<br />
					  <strong>Qta IMPEGNATO</strong>: '.$products['qt_impegnato'].' pz. <br />
					  <strong>Qta disponibile TOTALE</strong>: '.$products['qt_tot'].' pz. <br />
					  <strong>Qta disponibile NETTA</strong>: '.($products['qt_tot']-$products['qt_impegnato']).' pz. <br />
					  <strong>Qta ORDINATO</strong>: '.$products['qt_ordinato'].' pz. <br />
					  <strong>Qta IN ARRIVO ALLNET</strong>: '.$products['qt_arrivo'].' pz. 
					  <strong>Qta IN ARRIVO EDS</strong>: '.$products['qt_arrivo_eds'].' pz. 
					  "><a href="index.php?tab=AdminCatalogExFeatures&id_product='.$products['id_product'].'&updateproduct&token='.$tokenProducts.'" target="_blank">'.$products['product_reference'].'</a></span></td>';
					  echo '  <td><input size="53" name="product_name['.$products['id_product'].']" '.($order->id? 'readonly="readonly"' : '').' type="text" value="'.(empty($products['name']) ? $products['product_name'] : $products['name']).'" /></td>';
					  
					    echo '  <td><input style="text-align:right" name="product_quantity['.$products['id_product'].']" id="product_quantity['.$products['id_product'].']" type="text" '.($order->id? 'readonly="readonly"' : '').' size="1" value="'.$products['quantity'].'" onkeyup="if (isArrowKey(event)) return; calcolaImporto('.$products['id_product'].');" /></td>';
						
					  echo '  <td><input size="7" style="text-align:right" onkeyup="javascript:document.getElementById(\'usa_sconti_quantita['.$products['id_product'].']\').checked = false; calcolaPrezzoScontoExtra('.$products['id_product'].', \'inverso\'); calcolaImporto('.$products['id_product'].');" name="product_price['.$products['id_product'].']" id="product_price['.trim($products['id_product']).']" type="text" '.($order->id? 'readonly="readonly"' : '').' value="'.
					  ($products['price'] == 0 && $products['free'] == 0 ? number_format(($customer->id_default_group == 3 ? Product::trovaMigliorPrezzo($products['id_product'],3,0) : ($customer->id_default_group == 12 ? Product::trovaMigliorPrezzo($products['id_product'],12,0) : Product::trovaMigliorPrezzo($products['id_product'],1,$products['quantity']))), 2, ',', '') : number_format($products['price'], 2, ',', ''))
					 
					  .'" /></td>';
					  /*echo '  <td>'.$products['tax_rate'].'%</td>';
					  echo '  <td>'.number_format($products['product_price']*(1+$products['tax_rate']/100),3, ',', '').'</td>';  */
					
					  
					 
					   echo '  <td style="text-align:right">';
					  
					   
					   echo '<input style="text-align:right" name="sconto_extra['.$products['id_product'].']" id="sconto_extra['.$products['id_product'].']" type="text" '.($order->id? 'readonly="readonly"' : '').' size="2" value="'.number_format($products['sconto_extra'],2,",","").'" onkeyup="if (isArrowKey(event)) return; calcolaPrezzoScontoExtra('.$products['id_product'].', \'sconto\'); calcolaImporto('.$products['id_product'].');" /></td>';
					   
					   $wholesale_price = ($products_acquisto > 0 ? $products_acquisto : ($products['no_acq'] == 1 ? $products_acquisto : ($products['listino']*(100-$products['sc_acq_1'])/100)*((100-$products['sc_acq_2'])/100)*((100-$products['sc_acq_3'])/100)));
					   
					   echo '<td>
					   <input type="text" size="7" class="wholesale_price_class" style="text-align:right" onkeyup="javascript:document.getElementById(\'usa_sconti_quantita['.$products['id_product'].']\').checked = false; calcolaPrezzoScontoExtra('.$products['id_product'].', \'inverso\'); calcolaImporto('.$products['id_product'].');"  name="wholesale_price['.$products['id_product'].']" id="wholesale_price['.$products['id_product'].']" value="'.number_format(round($wholesale_price,2), 2, ',', '').'" />
					  
					   </td>';
					   
					    echo '  <td style="text-align:right">
						<input type="hidden" name="sconto_acquisto_1['.$products['id_product'].']" id="sconto_acquisto_1['.$products['id_product'].']" value="'.$products['sc_acq_1'].'" />
						<input type="hidden" name="sconto_acquisto_2['.$products['id_product'].']" id="sconto_acquisto_2['.$products['id_product'].']" value="'.$products['sc_acq_2'].'" />
						<input type="hidden" name="sconto_acquisto_3['.$products['id_product'].']" id="sconto_acquisto_3['.$products['id_product'].']" value="'.$products['sc_acq_3'].'" />
						';
						
						echo '
						
						<input type="hidden" class="prezzoacquisto" name="totaleacquisto['.$products['id_product'].']" id="totaleacquisto['.$products['id_product'].']" value="'.number_format(round($wholesale_price*$products['quantity'],2), 2, ',', '').'" />
						';
						
						$totaleacquisti += $wholesale_price*$products['quantity'];
						
						$prezzo_partenza = ($products['price'] == 0 && $products['free'] == 0 ? ($customer->id_default_group == 3 ? Product::trovaMigliorPrezzo($products['id_product'],3,0) : ($customer->id_default_group == 12 ? Product::trovaMigliorPrezzo($products['id_product'],12,0) : Product::trovaMigliorPrezzo($products['id_product'],1,$products['quantity']))) : $products['price']);
						
						$marginalita = ((($prezzo_partenza - $wholesale_price)*100)/$prezzo_partenza);
						
						echo '<span id="spanmarginalita['.$products['id_product'].']">'.number_format($marginalita, 2, ',', '').'%</span></td>';
					  echo '  <td style="text-align:center">'.$usa_sconti_quantita.' <input id="impImporto['.$products['id_product'].']" class="importo" type="hidden" value="'.($products['price'] == 0 && $products['free'] == 0 ? Product::trovaMigliorPrezzo($products['id_product'],1,$products['quantity']) : $products['price'])*$products['quantity'].'" /></td>';
					  echo '  <td style="text-align:right" id="valoreImporto['.$products['id_product'].']">
					 
					  '.number_format(($products['price'] == 0 && $products['free'] == 0 ? ($customer->id_default_group == 3 ? Product::trovaMigliorPrezzo($products['id_product'],3,0) : ($customer->id_default_group == 12 ? Product::trovaMigliorPrezzo($products['id_product'],12,0) : Product::trovaMigliorPrezzo($products['id_product'],1,$products['quantity']))) : $products['price'])*$products['quantity'],2, ',', '').'
					  
					 
					  
					  </td>';  
					  
					   $totale += ($products['price'] == 0 && $products['free'] == 0 ? ($customer->id_default_group == 3 ? Product::trovaMigliorPrezzo($products['id_product'],3,0) : ($customer->id_default_group == 12 ? Product::trovaMigliorPrezzo($products['id_product'],12,0) : Product::trovaMigliorPrezzo($products['id_product'],1,$products['quantity']))) : $products['price'])*$products['quantity'];
					  /*echo '  <td>'.number_format($products['product_price']*$products['quantity']*(1+$products['tax_rate']/100),2, ',', '').'</td>';  */
					  
					  echo '<td class="pointer dragHandle center" style="background:url(../img/admin/up-and-down.gif) no-repeat center;">
					  <input type="hidden" name="sort_order['.$products['id_product'].']" /></td>';
					  
					  echo '  <td style="text-align:center">
					  <a style="cursor:pointer" onclick="delProduct30('.$products['id_product'].'); togliImporto('.$products['id_product'].'); calcolaSpedizionePerIndirizzo(); "><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a>
					  </td>';
					  echo '  <input name="product_id['.$products['id_product'].']" type="hidden" value="'.$products['id_product'].'" class="product_ids" />';
					  
						
						echo '<input type="hidden" id="unitario['.$products['id_product'].']" value="'.$unitario.'" />';
						echo '<input type="hidden" id="sc_qta_1['.$products['id_product'].']" value="'.$sc_qta_1.'" />';
						echo '<input type="hidden" id="sc_qta_2['.$products['id_product'].']" value="'.$sc_qta_2.'" />';
						echo '<input type="hidden" id="sc_qta_3['.$products['id_product'].']" value="'.$sc_qta_3.'" />';
						echo '<input type="hidden" id="sc_riv_1['.$products['id_product'].']" value="'.$sc_riv_1.'" />';
						echo '<input type="hidden" id="sc_riv_2['.$products['id_product'].']" value="'.$sc_riv_2.'" />';
						echo '<input type="hidden" id="sc_riv_3['.$products['id_product'].']" value="'.$sc_riv_3.'" />';
		
						echo '<input type="hidden" id="sc_qta_1_q['.$products['id_product'].']" value="'.$sc_qta_1_q.'" />';
						echo '<input type="hidden" id="sc_qta_2_q['.$products['id_product'].']" value="'.$sc_qta_2_q.'" />';
						echo '<input type="hidden" id="sc_qta_3_q['.$products['id_product'].']" value="'.$sc_qta_3_q.'" />';
						echo '<input type="hidden" id="sc_riv_1_q['.$products['id_product'].']" value="'.$sc_riv_1_q.'" />';
						echo '<input type="hidden" id="sc_riv_2_q['.$products['id_product'].']" value="'.$sc_riv_2_q.'" />';
						echo '<input type="hidden" id="sc_riv_3_q['.$products['id_product'].']" value="'.$sc_riv_3_q.'" />';
						
					    echo '<input type="hidden" id="oldQuantity['.$products['id_product'].']" value="'.$products['quantity'].'" />';
						echo '<input type="hidden" id="oldPrice['.trim($products['id_product']).']" value="'.
					  ($products['price'] == 0 && $products['free'] == 0 ? ($customer->id_default_group == 3 ? $sc_riv_1 : ($customer->id_default_group == 12 ? $sc_riv_1 : $products['product_price'])) : $products['price']).'" />';
					    //echo '  <td>'.$products['id_product'].'</td>';
				   echo '</tr> ';
				}
			}
		
			$cart_ctrl = new Cart($_GET['id_cart']);
			$carrier_cart = Db::getInstance()->getValue("SELECT id_carrier FROM cart WHERE id_cart = ".$_GET['id_cart']."");
			$costo_trasporto_modificato_r = Db::getInstance()->getValue("SELECT transport FROM cart WHERE id_cart = ".$_GET['id_cart']."");
			$costo_trasporto_modificato_r = split(":",$costo_trasporto_modificato_r);
			$costo_trasporto_modificato = $costo_trasporto_modificato_r[1];
			if($carrier_cart != 0) {
				if($costo_trasporto_modificato != "" && $costo_trasporto_modificato > 0) {
					$costo_spedizione = $costo_trasporto_modificato;
				}
				else {				
					$costo_spedizione = $cart_ctrl->getOrderShippingCost($carrier_cart, false);
				}
			}
			else {
				$costo_spedizione = 0;
			}
			
			$marginalitatotale = ((($totale - $totaleacquisti)*100) / $totale);
			$guadagno = $totale - $totaleacquisti;
			$totale_senza_spedizione = $totale;
			
			
	/* Add TinyMCE */  
		        global $cookie;  
        $iso = Language::getIsoById((int)($cookie->id_lang));  
        $isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');  
        $ad = dirname($_SERVER["PHP_SELF"]);
		
		$provincia_cliente = Db::getInstance()->getValue("SELECT id_state FROM address WHERE id_customer = ".$customer->id." AND active = 1 AND deleted = 0 AND fatturazione = 1");
			
		$nazione_cliente = Db::getInstance()->getValue("SELECT id_country FROM address WHERE id_customer = ".$customer->id." AND active = 1 AND deleted = 0 AND fatturazione = 1");
			
		if($provincia_cliente == 0) {
			$zona_cliente = Db::getInstance()->getValue("SELECT id_zone FROM country WHERE id_country = ".$nazione_cliente."");
		}
		else {
			$zona_cliente = Db::getInstance()->getValue("SELECT id_zone FROM state WHERE id_state = ".$nazione_cliente."");
		}
			
		$gruppi_per_spedizione = array();
		$gruppi_per_spedizione[] = $customer->id_default_group;
		
		$metodi_spedizione = AdminCarts::getCarriersForEditOrder($zona_cliente, $gruppi_per_spedizione, $_GET['id_cart']);
		
		foreach($metodi_spedizione as $metodo) {
			if($metodo['name'] != 'Ritiro in sede Ezdirect' && $metodo['name'] != 'Trasporto gratuito') {
				$default_carrier = $metodo['id_carrier'];
			}
			else {
			}
		}

		$spedizione_default = Db::getInstance()->getValue('SELECT d.`price` FROM `delivery` d LEFT JOIN `range_price` r ON d.`id_range_price` = r.`id_range_price` WHERE d.`id_zone` = '.$zona_cliente.' AND 1 >= r.`delimiter1` AND 156 < r.`delimiter2` AND d.`id_carrier` = '.$default_carrier.' ORDER BY r.`delimiter1` ASC');

/*
		if($totale_senza_spedizione == 0) {	
			$costo_spedizione = $spedizione_default;
		}
	*/	
		$totale += $costo_spedizione; 
		
		$tax_regime = Db::getInstance()->getValue("SELECT tax_regime FROM customer WHERE id_customer = ".$customer->id."");
		
		if(!$tax_regime)
			$tax_regime = 0;
		
		if($tax_regime == 0) {
			$id_tax = 1;
		}
		else {
			$id_tax = $tax_regime;
		}
		$tax_rate = Db::getInstance()->getValue("SELECT rate FROM tax WHERE id_tax = ".$id_tax."");
			
		if($tax_regime == 1) {
			$tax_rate = 0;
		}
		
		$id_country = Db::getInstance()->getValue("SELECT id_country FROM address WHERE id_address = ".$cart->id_address_delivery."");
		
		if($id_country == 10) {
			
			
			$iva = ($totale * $tax_rate)/100;
			
		}
			
		else {
			
			$iva = 0;
			
		}
		
		if(!$id_country)
			$iva = ($totale * $tax_rate)/100;
			
		if($tax_regime == 1) {

			$iva = 0;
				
		}
		
        echo '  
		</tbody>
		<tfoot>
		<tr id="trasp_carrello">
		<td>TRASP</td><td>Trasporti (non modificare per calcolare in automatico)</td><td style="text-align:right">1</td>
		<input type="hidden" id="trasporto_modificato" value="'.($costo_trasporto_modificato == '' || $costo_trasporto_modificato == 0 ? 'n' : 'y').'" />
		<td style="text-align:right" id="costo_trasporto"><input type="text" '.($order->id? 'readonly="readonly"' : '').' value="'.($costo_trasporto_modificato == '' || $costo_trasporto_modificato == 0 ? number_format($costo_spedizione,2,',','') : str_replace(".",",",$costo_trasporto_modificato)).'" size="7" onkeyup="document.getElementById(\'valore_trasporto_modificato\').value = this.value; vediTrasportoGratuito(); document.getElementById(\'trasporto_modificato\').value = \'y\'; document.getElementById(\'importo_trasporto\').innerHTML=this.value; calcolaImportoConSpedizione('.$carrier_cart.');" name="transport" id="transport" style="text-align:right" /><input type="hidden" id="valore_trasporto_modificato" value="'.($costo_trasporto_modificato == '' || $costo_trasporto_modificato == 0 ? '' : $costo_trasporto_modificato).'" /></td><td style="text-align:right"></td><td style="text-align:right"></td><td></td><td></td><td style="text-align:right" id="importo_trasporto">'.($costo_trasporto_modificato == '' || $costo_trasporto_modificato == 0 ? number_format($costo_spedizione,2,',','') : str_replace(".",",",$costo_trasporto_modificato)).'</td><td></td><td></td></tr>
		
		<tr><td style="width:130px"></td><td style="width:310px">Guadagno totale in euro: <span id="guadagnototale">'.number_format($guadagno,2, ',', '').'</span> &euro;</td><td style="width:45px"></td><td style="width:75px"></td><td style="width:45px"></td><td style="width:75px"></td><td style="width:55px"><span id="marginalitatotale">'.number_format($marginalitatotale,2, ',', '').'%</span></td><td style="width:55px"><strong>Imponibile</strong></td><td style="width:85px; text-align:right"><span id="spantotaleprodotti"> '.number_format($totale,2, ',', '').'</span></td><td style="width:10px"></td>
		<td style="width:10px"></td><!-- <td style="width:45px"></td> --></tr>
		
		<tr><td style="width:130px"></td><td style="width:310px"></td><td style="width:45px"></td><td style="width:75px"></td><td style="width:45px"></td><td style="width:75px"></td><td style="width:55px"></td><td style="width:55px"><strong>IVA incl.</strong></td><td style="width:85px; text-align:right"><span id="spantotaleprodottiiva"> '.number_format($totale+$iva,2, ',', '').'</span></td><td style="width:10px"></td>
		<td style="width:10px"></td><!-- <td style="width:45px"></td> --></tr>
		
		</tfoot>
		</table><table class="table">
		</table>';
		
		
		
			
		
			
			
			
			
			
			if($payment != "" && $payment != "Bonifico" && $payment != "nessuno" && $payment != "Bonifico Bancario" && $payment != "Bonifico anticipato" && $payment != "Carta" && $payment != "GestPay" && $payment != "Paypal" && $payment != "PayPal" && $payment != "Contrassegno" && $payment != "Bonifico 30 gg. fine mese" && $payment != "Bonifico 60 gg. fine mese" && $payment != "Bonifico 90 gg. fine mese") {
				$check_payment = 1;
			} else {
				$check_payment = 0;
			}
			
			
			if($payment == "") {
				$payment = Db::getInstance()->getValue("SELECT pagamento FROM customer_amministrazione WHERE id_customer = ".$customer->id."");
			}
			
			if($payment == "") {
				$payment = 'Bonifico anticipato';
			}
			
			echo "<br /><strong>Metodo di pagamento</strong>: ";
				
				if($order->id) { 
				
					echo "<input type='text' readonly='readonly' style='width:250px' value='".str_replace("'",htmlentities("'",ENT_QUOTES),$payment)."' />&nbsp;&nbsp;&nbsp;";
				} 
				
				else {
					echo "<select name='payment' id='payment'>
					<option value='nessuno' ".($payment == "nessuno" ? "selected='selected'" : "").">Nessun metodo - lascia scelta al cliente</option>
					<option value='Bonifico' ".($payment == "Bonifico" || $payment == "Bonifico anticipato" || $payment == "Bonifico bancario" || $payment == "Paypal" || $payment == "PayPal" || $payment == "Carta" || $payment == "GestPay" || $payment == "Bonifico Bancario" ? "selected='selected'" : "").">Bonifico anticipato</option>
					<option value='Carta'>Carta di credito (Gestpay)</option>
					<option value='Paypal'>Paypal</option>
					<option value='Contrassegno' ".($payment == "Contrassegno" ? "selected='selected'" : "").">Contrassegno</option>
					<option value='Bonifico 30 gg. fine mese' ".($payment == "Bonifico 30 gg. fine mese" ? "selected='selected'" : "").">Bonifico 30 gg. fine mese</option>
					<option value='Bonifico 60 gg. fine mese' ".($payment == "Bonifico 60 gg. fine mese" ? "selected='selected'" : "").">Bonifico 60 gg. fine mese</option>
					<option value='Bonifico 90 gg. fine mese' ".($payment == "Bonifico 90 gg. fine mese" ? "selected='selected'" : "").">Bonifico 90 gg. fine mese</option>
					<option value='R.B. 30 GG. D.F. F.M.' ".($payment == "R.B. 30 GG. D.F. F.M." ? "selected='selected'" : "").">R.B. 30 GG. D.F. F.M.</option>
					<option value='R.B. 60 GG. D.F. F.M.' ".($payment == "R.B. 60 GG. D.F. F.M." ? "selected='selected'" : "").">R.B. 60 GG. D.F. F.M.</option>
					<option value='R.B. 90 GG. D.F. F.M.' ".($payment == "R.B. 90 GG. D.F. F.M." ? "selected='selected'" : "").">R.B. 90 GG. D.F. F.M.</option>
					<option value='Altro' ".($check_payment == 1 ? "selected='selected'": "").">Altro</option>
					</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					echo "Altro metodo: <input type='text' name='altropagamento' id='altropagamento' value='".($check_payment == 1 ? str_replace("'",htmlentities("'",ENT_QUOTES),$payment) : "")."' /><br />";
					
					if($customer->id_default_group == 1) {
						
						if($totale < 399) {
							$supplemento_contrassegno = 3.5;
						}
						else {
							$supplemento_contrassegno = 0;
							
						}
					}
						
					else if($customer->id_default_group  == 3) {
							
						if($totale < 516) {
							$supplemento_contrassegno = 5;
						}
						else {
							$supplemento_contrassegno = (($totale/100)*1.5);
						
						}
							
					}
						
					else {
						if($totale < 516) {
							$supplemento_contrassegno = 5;
						}
						else {
							$supplemento_contrassegno = (($totale/100)*1.5);
							
						}
					
					}

					echo "Il pagamento in contrassegno prevede un supplemento di euro <span id='supplemento_contrassegno'>".number_format($supplemento_contrassegno,2,",","")."</span> iva esclusa
								
					<br /><br />
					";
				}
				
				if($order->id) { 
					$mtds = Db::getInstance()->getValue("SELECT name FROM carrier WHERE id_carrier = ".$carrier_cart."");
					echo "<strong>Metodo spedizione</strong>: <input type='text' readonly='readonly' style='width:250px' value='".$mtds."' /><br />";
				} 
				
				else {
				
				
					echo "<strong>Seleziona metodo di spedizione</strong> (se l'importo prevede consegna gratuita, il costo sar&agrave; ricalcolato automaticamente dopo aver fatto clic sul tasto Conferma):
					
					
					";
					
					
					$i_metodi_spedizione = 0;
					foreach ($metodi_spedizione as $metodo) {
						$i_metodi_spedizione++;
						if ($costo_trasporto_modificato == '' || $costo_trasporto_modificato == 0) {
							$metodo['price_tax_exc'] = $metodo['price_tax_exc'];
						}
						else {
							if ($carrier_cart == $metodo['id_carrier']) {
								$metodo['price_tax_exc'] = $costo_trasporto_modificato;
							}
							else {
								$metodo['price_tax_exc'] = $metodo['price_tax_exc'];
							}
						}

						echo "<input type='radio' id='metodo[".$metodo['id_carrier']."]' ".(strpos($metodo['name'],'grat') !== false ? "class='gratis'"  : (strpos($metodo['name'],'itiro') !== false ? "class='ritiro'" : "class='a_pagamento'"))." name='metodo' rel='".$metodo['price_tax_exc']."' value='".$metodo['id_carrier']."' ".($carrier_cart == $metodo['id_carrier'] ? "checked='checked'" : ($carrier_cart == '' || $carrier_cart == 0 ? ($metodo['name'] != 'Ritiro in sede Ezdirect' && $metodo['name'] != 'Trasporto gratuito' ? "checked='checked'" : "") : ""))." onclick='document.getElementById(\"valore_trasporto_modificato\").value = 0; calcolaSpedizionePerIndirizzo(); document.getElementById(\"transport\").value = \"".number_format($metodo['price_tax_exc'],2,",","")."\"; document.getElementById(\"importo_trasporto\").innerHTML = \"".number_format($metodo['price_tax_exc'],2,",","")."\"; calcolaImportoConSpedizione(".$metodo['id_carrier']."); ' />".$metodo['name']." (<span id='metodo_costo[".$metodo['id_carrier']."]'>".number_format($metodo['price_tax_exc'],2,",","")."</span> &euro;)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					
					}
				}
				
			
			if(Tools::getValue('preventivo') == 222) 
			{
			}
			
			else {
				echo '
				<style type="text/css">
				.mceEditor td.mceIframeContainer iframe {
					min-height: 30px !important;
				}
				</style>
				<p>
				<table style="margin-left:-3px"><tr><td><strong>Esigenze del cliente</strong>: 
				
				'.($order->id? '<div style="width:445px; background-color:#f0ebd6; border:1px solid #cccccc; overflow-y:auto; height:150px">'.$esigenze.'</div>' : '<textarea name="esigenze" id="esigenze" style="width:445px; " class="rte" rows="3">'.$esigenze.'</textarea>').'</td><td style="padding-left:20px">
				<strong>Risorse</strong>: '.($order->id? '<div style="width:445px; background-color:#f0ebd6; border:1px solid #cccccc; overflow-y:auto; height:150px">'.$risorse.'</div>' : '<textarea name="risorse" id="risorse" style="width:445px" class="rte">'.$risorse.'</textarea>').'</td></tr></table><br />
				
				
				<strong>Note</strong>:<br /> 
				'.($order->id ? '' : '<input size="123" type="text" value="" id="product_autocomplete_input_tkt" /><br /><br />').'
				
				<script type="text/javascript">
				
					function addProductLink_TR(event, data, formatted)
					{
						document.getElementById("product_autocomplete_input_tkt").value = ""; 
						var link_rewrite = data[31];
						var productName = data[5];
						
						var $body = $(tinymce.editors[\'note\'].getBody());
						$body.find("p:last").append(\'<br />\' + productName + \'<br /><a href="\' + link_rewrite + \'">\' + link_rewrite + \'<\/a><br /><br />\');
						
					}	
					
					$(function() {
						$(\'#product_autocomplete_input_tkt\')
							.autocomplete(\'ajax_products_list2.php?products_and_bundles=y&id_customer='.Tools::getValue('id_customer').'\', {
								minChars: 2,
								autoFill: false,
								max:50,
								matchContains: true,
								matchSubset: 0,
								mustMatch:true,
								scroll:true,
								cacheLength:1,
								formatItem: function(item) {
							
											var color = "";
											if(item[24] == 1) {
												var color = "; color:red";
											}
											if(item[28] == 0) {
												return \'</table><div style="position:absolute; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:150px;float:left;margin-left:10px;">Codice</div></th><th style="width:570px;"><div style="width:350px; float:left;">Desc. prodotto<br /><br /></div></th><th style="width:60px; text-align:right"><div style="width:60px;float:left;">Prezzo</div></th><th style="width:50px; text-align:right"><div style="width:60px;float:left;">Qt. mag.</div></th><th style="width:50px; text-align:right"><div style="width:75px;float:left;">Qt. tot</div></th></tr></div><table><tr><td style="width:145px; text-align:left"><br /><br />\'+item[4]+\'</td><td style="width:555px\'+color+\'"><br /><br />\'+item[5]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[23]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[25]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[22]+\'</td></tr></table>\'
												
												;
											}
											else {
												return \'<tr><td style="width:150px; text-align:left">\'+item[4]+\'</td><td style="width:350px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
												
												;
												}
											}
								
							}).result(addProductLink_TR);
						
					});
				
					$("#product_autocomplete_input_tkt").css(\'width\',\'705px\');
					$("#product_autocomplete_input_tkt").keypress(function(event){
								
								  var keycode = (event.keyCode ? event.keyCode : event.which);
								  if (keycode == "13") {
									event.preventDefault();
									event.stopPropagation();    
								  }
								});
				</script>
				
				
				'.($order->id? '<div style="width:910px; background-color:#f0ebd6; border:1px solid #cccccc; overflow-y:auto; height:150px">'.$note.'</div>' : '<textarea name="note" id="note" style="width:910px" class="rte">'.$note.'</textarea>').'<br />
				
		
				';
				
				
				$numero_notifiche =  Db::getInstance()->getValue("SELECT numero_notifiche FROM cart WHERE id_cart = ".$_GET['id_cart']."");
				$data_ultima_notifica =  Db::getInstance()->getValue("SELECT data_ultima_notifica FROM cart WHERE id_cart = ".$_GET['id_cart']."");
				$provvisorio =  Db::getInstance()->getValue("SELECT provvisorio FROM cart WHERE id_cart = ".$_GET['id_cart']."");
				
				$impiegati_ez = Db::getInstance()->executeS("SELECT * FROM employee");
				$notifiche = Db::getInstance()->getValue("SELECT notifiche FROM cart WHERE id_cart = ".$_GET['id_cart']."");
				$notifiche = unserialize($notifiche);
				
				
				echo '<span class="button" onclick="$(\'#mostra_nota_privata\').slideToggle(); " style="cursor: pointer; display:block; color:red; font-size:14px; font-weight:bold; width:902px; border:1px solid #cccccc; text-align:center; background-color:#f0f0ee; text-decoration:none"><img src="../img/admin/arrow.gif" alt="'.$this->l('Dettaglio note private').'" title="'.$this->l('Dettaglio note private').'" style="float:left; margin-right:5px;"/>'.$this->l('Clicca qui per inserire note private/riservate - Non visibili all\'utente.').'</span>';
				
				echo '<p><div id="mostra_nota_privata" style="'.(strip_tags(str_replace(" ","",$note_private)) == '' ? 'display:none;' : '').' margin-top:-15px">'.($order->id? '<div style="width:910px; background-color:#f0ebd6; border:1px solid #cccccc; overflow-y:auto; height:150px">'.$note_private.'</div>' : '<textarea name="note_private" id="note_private"  style="width:910px; color:red"  class="rte">'.$note_private.'</textarea>').'<br />
				<script type="text/javascript">
				
				
				</script>
				</div>';
				
				$noleggio = Db::getInstance()->getValue("SELECT id_cart FROM noleggio WHERE id_cart = ".$_GET['id_cart']."");
				if($noleggio > 0) {
					$datiNoleggio = Db::getInstance()->getRow("SELECT * FROM noleggio WHERE id_cart = ".$_GET['id_cart']."");
				}
				
				
				//echo '<div '.($customer->is_company == 0 ? '<!-- style="display:none" -->' : '').'>';
				
				echo '<div>
				<div id="dettaglio_noleggio">
				<table style="margin-left:-3px">
				<tr>
				
				<td>';
				
				if($order->id) {
					echo '<input type="text" readonly="readonly"  name="mesi_noleggio" id="mesi_noleggio" value="'.$datiNoleggio['mesi'].'" />';
				}
				else {
				
					/*echo '<input type="checkbox" name="attiva_noleggio" '.($noleggio > 0 ? 'checked="checked"' : '').'> <strong>ATTIVA NOLEGGIO</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';*/
					
					echo '<strong>Mesi noleggio</strong> (imposta su 0 per disattivare noleggio)<select name="mesi_noleggio" id="mesi_noleggio" onchange="calcola_noleggio()";>
					<option value="0">0</option>
					<option value="18" '.($noleggio > 0 ? ($datiNoleggio['mesi'] == 18 ? 'selected="selected"' : '') : '').'>18</option>
					<option value="24" '.($noleggio > 0 ? ($datiNoleggio['mesi'] == 24 ? 'selected="selected"' : '') : '').'>24</option>
					<option value="36" '.($noleggio > 0 ? ($datiNoleggio['mesi'] == 36 ? 'selected="selected"' : '') : '').'>36</option>
					<option value="48" '.($noleggio > 0 ? ($datiNoleggio['mesi'] == 48 ? 'selected="selected"' : '') : '').'>48</option>
					<option value="60" '.($noleggio > 0 ? ($datiNoleggio['mesi'] == 60 ? 'selected="selected"' : '') : '').'>60</option>
					
					</select>&nbsp;&nbsp;&nbsp;';
				}
				
				echo '
				</td>
				<td><strong>Spese contratto</strong></td>
				<td><input readonly="readonly" type="text" name="spese_contratto_noleggio" style="text-align:right; background-color:#f0ebd6" id="spese_contratto_noleggio" size="5"  '.($noleggio > 0 ? 'value="'.number_format($datiNoleggio['spese_contratto'],2,",","").'"' : '').' /> &euro; &nbsp;&nbsp;&nbsp;</td>
				
				<td><strong>Importo rata mensile</strong></td>
				
				<td><input readonly="readonly" type="text" name="importo_rata_mensile_noleggio" style="text-align:right; background-color:#f0ebd6" id="importo_rata_mensile_noleggio" size="5" '.($noleggio > 0 ? 'value="'.number_format($totale*$datiNoleggio['parametri']/100,2,",","").'"' : '').' /> &euro; &nbsp;&nbsp;&nbsp;
				<input type="hidden" id="parametro_noleggio" name="parametro_noleggio" />
				
				</td>
				
				
				</tr>
				</table>
				';
				
				
				echo '
				</div></div>
				<br />
				
				';
				
				
				if($order->id) {
				}
				
				else {

					if($cookie->id_employee != 1 && $cookie->id_employee != 2 && $cookie->id_employee != 6 && $cookie->id_employee != 7) 
					{
						echo '<input name="provvisorio" id="provvisorio" type="checkbox" checked="checked" style="display:none;" />';
					}
					else
					{
						echo '
						<input name="provvisorio" id="provvisorio" type="checkbox" '.($provvisorio == 1 ? 'checked="checked"' : '').' />Carrello provvisorio? Se spuntato, non verr&agrave; mostrato al cliente<br /> <br />';
					}
				
					echo '
					<input name="'.($cookie->id_employee != 1 &&  $cookie->id_employee != 2 &&  $cookie->id_employee != 6 && $cookie->id_employee != 7 ? "prepara_mail" : "notifica_mail").'" id="'.($cookie->id_employee != 1 &&  $cookie->id_employee != 2 &&  $cookie->id_employee != 6 && $cookie->id_employee != 7 ? "prepara_mail" : "notifica_mail").'" '.($cookie->id_employee != 1 &&  $cookie->id_employee != 2 &&  $cookie->id_employee != 6 && $cookie->id_employee != 7 ? 'checked="checked" type="hidden" value="1"' : 'type="checkbox"').' />'.($cookie->id_employee != 1 &&  $cookie->id_employee != 2 &&  $cookie->id_employee != 6 && $cookie->id_employee != 7 ? "Prepara le mail a cui inviare la notifica" : "Spunta questo flag se vuoi inviare una notifica via mail al cliente").' ';
					echo ($numero_notifiche == 0 ? '' : '<strong style="color:red">Questo carrello &egrave; gi&agrave; stato notificato al cliente. L\'ultima volta in data '.Tools::displayDate($data_ultima_notifica, (int)($cookie->id_lang)).'</strong><br />').'
					<br /><br />';
						
						
					if ($cookie->id_employee != 1 &&  $cookie->id_employee != 2 &&  $cookie->id_employee != 6 && $cookie->id_employee != 7) {
					
						if(isset($_GET['notifica_inviata']) && $numero_notifiche > 0) {
							echo "<script type='text/javascript'>alert('Notifica correttamente inviata');</script>";
						} 
						else if(isset($_GET['notifica_inviata']) && $numero_notifiche == 0) {
							echo "<script type='text/javascript'>alert('Notifica NON INVIATA. Assicurarsi di aver scelto bene l\'indirizzo mail.');</script>";
						}
						else {
						
						}
					
					}
					$ultima_mail_notifica = "";
						
					if($numero_notifiche > 0) {
							
						//echo '<span onclick="$(\'#dettaglio_notifiche\').slideToggle(); " style="cursor: pointer; color:blue; text-decoration:underline"><img src="../img/admin/arrow.gif" alt="'.$this->l('Dettaglio notifiche').'" title="'.$this->l('Dettaglio notifiche').'" style="float:left; margin-right:5px;"/>'.$this->l('Clicca qui per aprire il dettaglio notifiche').'</span>
						echo '<div id="dettaglio_notifiche">
						<table class="table">
						<tr>
						<th style="width:250px">Inviata a</th><th style="width:250px">In data</th>						</tr>
						';
						foreach($notifiche as $notifica) {
								
							echo "<tr><td>".$notifica['email']."</td><td>".Tools::displayDate($notifica['data'], (int)$cookie->id_lang, true)."</td></tr>";
								$ultima_mail_notifica = $notifica['email'];
						}
							
							
						echo'
						</table>
						</div>
						';
					}
					else 
					{ 
						
					}
					echo '<br /><table class="table">
					<tr>
					<th style="width:250px">Telefono cliente</th><th style="width:250px">Cellulare cliente</th>';
							
					echo '</th></tr>';
					$customer_phone = Db::getInstance()->getRow("SELECT	phone, phone_mobile FROM address WHERE id_customer = ".$customer->id." AND deleted = 0 AND fatturazione = 1 AND active = 1");
						
					echo '<tr><td>'.(!$customer_phone['phone'] ? 'Non presente' : $customer_phone['phone']).'</td><td>'.(!$customer_phone['phone_mobile'] ? 'Non presente' : $customer_phone['phone_mobile']).'</td></tr>';
					echo '</table><br />';
					if ($cookie->id_employee != 1 &&  $cookie->id_employee != 2 &&  $cookie->id_employee != 6 && $cookie->id_employee != 7) {
						echo 'Seleziona le mail da preparare per la notifica: ';
					}
					else {
						echo 'Seleziona la persona a cui vuoi inviare la notifica: ';
						
					}
					$persone = Db::getInstance()->executeS("SELECT id_persona, firstname, lastname, email FROM persone WHERE id_customer = ".$customer->id." AND email != ''");
						
					$email_preparate = Db::getInstance()->getValue("SELECT email_preparate FROM cart WHERE id_cart = ".Tools::getValue('id_cart')."");
					$email_preparate = unserialize($email_preparate);
							
					$indirizzo_email_persona_a = Db::getInstance()->getValue("SELECT email FROM persone WHERE id_persona = ".$id_persona."");
							
					echo '<script type="text/javascript" src="../js/select2.js"></script>
					<script type="text/javascript">
					$(document).ready(function() { $("#seleziona-persona").select2(); });
					</script>';

					echo '
					<select multiple id="seleziona-persona" name="seleziona-persona[]" style="width:500px">';
					if($customer->is_company == 1) {
						echo "<option value=''>-- Seleziona persona --</option>";
								
						if($numero_notifiche > 0) {
								
							echo "<option ".($numero_notifiche > 0 ? ($ultima_mail_notifica == $customer->email ? "selected='selected'" : "") : "")." value='".$customer->email."'>".$customer->email." (Mail dell'account)</option>";
								
						}
								
						else  {
							if(!empty($email_preparate)) {
								$id_persona_base = Db::getInstance()->getValue('SELECT id_persona FROM persone WHERE email = "'.$customer->email.'"');
								echo "<option ".(in_array($id_persona_base, $email_preparate) ? "selected='selected'" : "")." value='".$customer->email."'>".$customer->email." (Mail dell'account)</option>";
							}
							else {
								echo "<option selected='selected' value='".$customer->email."'>".$customer->email." (Mail dell'account)</option>";
							}
						}
								
						$i_persona = 0;
						$mail_persone = array();
								
						foreach ($persone as $persona) {
										
							if(trim($persona['email']) == trim($customer->email))
							{
							}
									
							else 
							{
									
								if(in_array($persona['email'], $mail_persone)) {
													
								}
								else {
										
									if($numero_notifiche > 0) {
										
										echo "<option ".($numero_notifiche > 0 ? ($ultima_mail_notifica == $persona['email'] ? "selected='selected'" : "") : ($i_persona == 0 ? "selected='selected'" : ""))." value='".$persona['email']."'>".$persona['firstname']." ".$persona['lastname']." - ".$persona['email']."</option>";
									}
											
									else {
										if(!empty($email_preparate)) {
											echo "<option ".(in_array($persona['id_persona'], $email_preparate) ? "selected='selected'" : "")." value='".$persona['email']."'>".$persona['firstname']." ".$persona['lastname']." - ".$persona['email']."</option>";
										}
										else {
											echo "<option value='".$persona['email']."'>".$persona['firstname']." ".$persona['lastname']." - ".$persona['email']."</option>";
										}
															
									}
										
											
								}
								$mail_persone[] = $persona['email'];
								$i_persona++;
							}
						}
									
						
					}
					else {
							
						if($numero_notifiche > 0) {
							$stop_mail_cliente = 0;
							$mail_persone = array();
							foreach($notifiche as $notifica) {
								if($notifica['email'] == $customer->email) {
									if($stop_mail_cliente == 0) {
										echo "<option value='".$customer->email."' ".($ultima_mail_notifica == $notifica['email'] ? "selected='selected'" : "").">".$customer->firstname." ".$customer->lastname." - ".$customer->email."</option>";
										$stop_mail_cliente = 1;
									}
								}
								else {
									if(in_array($notifica['email'], $mail_persone)) {
													
									}
									else {
										echo "<option value='".$notifica['email']."' ".($ultima_mail_notifica == $notifica['email'] ? "selected='selected'" : "").">".$notifica['email']."</option>";
										$mail_persone[] = $notifica['email'];
									}
								}
											
							}
						}
						else {
							echo "<option value='".$customer->email."' selected='selected'>".$customer->firstname." ".$customer->lastname." - ".$customer->email."</option>";
						}
								
					}						
					echo "</select><br />";

					echo 'Se le persone non sono nell\'elenco, inserisci in questo campo gli indirizzi email separati da punto e virgola: <input type="text" size="40" name="customer_email_msg_cart" id="customer_email_msg_cart" value="" />';
						
					if ($cookie->id_employee != 1 &&  $cookie->id_employee != 2 &&  $cookie->id_employee != 6 && $cookie->id_employee != 7) {
						
					}
					else {
						echo "<br /><br />";
						echo '
						Copia conoscenza: '; 
								
						foreach ($impiegati_ez as $impez) {
							
							echo "<input type='checkbox' name='conoscenza[]' id='conoscenza_".$impez['id_employee']."'value='".$impez['id_employee']."' /> ".$impez['firstname']."  ";
												
						}
							
					}
							
					echo "<br /><br />";
						
					echo '<script type="text/javascript">
						function check_notifica_provvisorio() {
							 if (document.getElementById("provvisorio").checked && document.getElementById("notifica_mail").checked) {
								alert("ATTENZIONE! Per notificare questo carrello "+String.fromCharCode(0xE8)+" necessario togliere il flag del carrello provvisorio. Il carrello sar"+String.fromCharCode(0xE0)+" adesso salvato, ma non verr"+String.fromCharCode(0xE0)+" inviata alcuna notifica.");
								
						 }
						
							
					}
						
						
					</script>
					';
				
				
					if($visualizzato == 1) {
						echo "<strong>Il cliente ha visualizzato questa offerta sul sito</strong>";
					}
					else {
						echo "<strong>Il cliente non ha ancora visualizzato questa offerta sul sito</strong>";
					}
				}	
				echo "<br /><br />";
				if(Tools::getValue('preventivo') == 2) 
				{
					echo  '<input name="Apply" type="submit" class="button_invio" value="Conferma ordine" onclick="check_notifica_provvisorio();" /><br />
					  <input name="totaleprodotti" id="totaleprodotti" type="hidden" value="'.$totale.'" />
					  <input name="id_order" type="hidden" value="'. $_GET['id_order'] .'" />
					  <input name="tax_rate" type="hidden" value="'. $tax_rate .'" />
					  <input name="id_lang" type="hidden" value="'. $id_lang .'" />
					';
					
					
				
				}
				if(Tools::getIsset('vedirevisione'))
				{
				}
				else {
				
					echo  '<input name="Apply" type="submit" class="button_invio" value="'.(Tools::getValue('preventivo') == 2 ? 'Conferma solo carrello'  : 'Conferma').'" onclick="check_notifica_provvisorio();" /><br />
					  <input name="totaleprodotti" id="totaleprodotti" type="hidden" value="'.$totale.'" />
					  <input name="id_order" type="hidden" value="'. $_GET['id_order'] .'" />
					  <input name="tax_rate" type="hidden" value="'. $tax_rate .'" />
					  <input name="id_lang" type="hidden" value="'. $id_lang .'" />
					';
				}
			}
				echo '</form></fieldset>
				';
				
				
				echo '	<script type="text/javascript">

				$(\'.button_invio\').live(\'click\', function(e) {
					$(\'#waiting1\').waiting({ 
					elements: 10, 
					auto: true 
					});
					var overlay = jQuery(\'<div id="overlay"></div>\');
					var attendere = jQuery(\'<div id="attendere">ATTENDERE PREGO</div>\');
					overlay.appendTo(document.body);
					attendere.appendTo(document.body);
				});
					
			</script>';
	
		
		
			if(isset($_GET['cartupdated'])) {

			
			$id_cart_g = htmlspecialchars($_GET['id_cart']);
			$action_g = ""; 
			$id_product_g = htmlspecialchars($_POST['id_product']);
			$id_lang_g = 5;


			if ($id_order_g) 
			$_POST['id_order']=$id_order_g;
			

			if ($_POST['id_cart']) {
				
				/*
					$query="update  ". _DB_PREFIX_."orders set ";
					$query.=" total_discounts=".$this->price($_POST['total_discounts']);
					$query.=" ,total_shipping=".$this->price($_POST['total_shipping']);
					$query.=" where id_order=".$_POST['id_order'];
					$query.=" limit 1";
					//echo $query;
					mysql_query($query) or die(mysql_error());
				*/
				

				if ($_POST['Apply']) {
				
					Db::getInstance()->executes("DELETE FROM cart_product WHERE bundle = 88888877 AND id_cart = ".$_POST['id_cart']."");
					Db::getInstance()->executes("DELETE FROM carrelli_creati_prodotti WHERE bundle = 88888877 AND id_cart = ".$_POST['id_cart']."");
					
					$creato_da = Db::getInstance()->getValue("SELECT created_by FROM cart WHERE id_cart = ".$_POST['id_cart']."");
					$impiegato_ultima_modifica = Db::getInstance()->getValue("SELECT id_employee FROM cart WHERE id_cart = ".$_POST['id_cart']."");
			
					if($creato_da == 0 && $impiegato_ultima_modifica == 0) { 
					
						
						
						$da_copiare = Db::getInstance()->getRow('SELECT * FROM cart WHERE id_cart = '.$_POST['id_cart'].'');
		
						Db::getInstance()->executeS('INSERT INTO carrelli_creati (id_cart, id_carrier, id_lang, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, date_add, date_upd, rif_prev, name, riferimento, risorse, esigenze, note, note_private, preventivo, validita, consegna, in_carico_a, payment, attachment, created_by, id_employee, numero_notifiche) VALUES ('.$_POST['id_cart'].', "'.$da_copiare['id_carrier'].'", "'.$da_copiare['id_lang'].'","'.addslashes($da_copiare['id_address_delivery']).'","'.addslashes($da_copiare['id_address_invoice']).'",1,'.$da_copiare['id_customer'].',0,"",0,0,"","'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'", "'.addslashes($da_copiare['rif_prev']).'", "'.addslashes($da_copiare['name']).'", "'.addslashes($da_copiare['riferimento']).'", "'.addslashes($da_copiare['risorse']).'","'.addslashes($da_copiare['esigenze']).'","'.$da_copiare['note'].'","'.$da_copiare['note_private'].'","'.addslashes($da_copiare['preventivo']).'","'.$da_copiare['validita'].'","'.addslashes($da_copiare['consegna']).'","'.addslashes($da_copiare['in_carico_a']).'","'.$da_copiare['payment'].'","'.$da_copiare['attachment'].'",'.$cookie->id_employee.','.$cookie->id_employee.',0)');  

						$prodotti_da_copiare = Db::getInstance()->executeS('SELECT * FROM cart_product WHERE id_cart = '.trim($_POST['id_cart']).'');
		
						foreach($prodotti_da_copiare as $prodotto_da_copiare) {
			
			
							Db::getInstance()->executeS('INSERT INTO carrelli_creati_prodotti (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, sconto_extra, prezzo_acquisto, no_acq, sort_order, bundle, date_add) VALUES ('.$_POST['id_cart'].', "'.$prodotto_da_copiare['id_product'].'", "'.$prodotto_da_copiare['id_product_attribute'].'", "'.$prodotto_da_copiare['quantity'].'", "'.$prodotto_da_copiare['price'].'","'.$prodotto_da_copiare['free'].'","'.addslashes($prodotto_da_copiare['name']).'","'.$prodotto_da_copiare['sc_qta'].'","'.$prodotto_da_copiare['sconto_extra'].'","'.$prodotto_da_copiare['wholesale_price'].'","'.$prodotto_da_copiare['no_acq'].'","'.$prodotto_da_copiare['sort_order'].'","'.$prodotto_da_copiare['bundle'].'","'.date('Y-m-d H:i:s').'")');   
			
			
			
						}
					
					}
					
					//delete product
					if ($_POST['product_delete']) {
						foreach ($_POST['product_delete'] as $id_product=>$value) {
							mysql_query("delete from ". _DB_PREFIX_."cart_product where id_product=".$id_product);
							mysql_query("delete from ". _DB_PREFIX_."carrelli_creati_prodotti where id_product=".$id_product);
						}
					}
					
					
					
					if($_POST['altropagamento'] != '') {
						$pagamento = $_POST['altropagamento'];
					}
					else {
						$pagamento = $_POST['payment'];
					}
					
					$validita_corretto = str_replace("/","-",$_POST['validita']);
					
					Db::getInstance()->executeS("update  ". _DB_PREFIX_."cart set date_upd = '".date("Y-m-d H:i:s")."', id_employee = ".$cookie->id_employee.", esigenze='".$_POST['esigenze']."',risorse='".$_POST['risorse']."',note='".preg_replace('/(<[^>]+) style=".*?"/i', '',$_POST['note'])."', note_private='".$_POST['note_private']."', preventivo='".$_POST['preventivo']."', id_address_invoice='".$_POST['id_address_invoice']."', id_address_delivery='".$_POST['id_address_delivery']."', id_carrier='".$_POST['metodo']."', rif_prev='".$_POST['rif_prev']."', name='".$_POST['name']."', riferimento='".$_POST['riferimento']."', validita='".date("Y-m-d H:i:s", strtotime($validita_corretto))."',in_carico_a='".$_POST['in_carico_a']."',".'payment="'.$pagamento.'"'."".", consegna='".$_POST['consegna']."' where id_cart = ".$_POST['id_cart']);
					
					Db::getInstance()->executeS("update  ". _DB_PREFIX_."carrelli_creati set date_upd = '".date("Y-m-d H:i:s")."', id_employee = ".$cookie->id_employee.", esigenze='".$_POST['esigenze']."', risorse='".$_POST['risorse']."', note='".preg_replace('/(<[^>]+) style=".*?"/i', '', $_POST['note'])."', note_private='".$_POST['note_private']."', preventivo='".$_POST['preventivo']."', id_address_invoice='".$_POST['id_address_invoice']."', id_address_delivery='".$_POST['id_address_delivery']."', id_carrier='".$_POST['metodo']."', rif_prev='".$_POST['rif_prev']."', name='".$_POST['name']."', riferimento='".$_POST['riferimento']."', validita='".date("Y-m-d H:i:s", strtotime($validita_corretto))."',in_carico_a='".$_POST['in_carico_a']."',".'payment="'.$pagamento.'"'."".", consegna='".$_POST['consegna']."' where id_cart = ".$_POST['id_cart']);
					
					
					
					if($_POST['mesi_noleggio'] > 0) {
					
						$noleggio = Db::getInstance()->getValue("SELECT id_cart FROM noleggio WHERE id_cart = ".$_POST['id_cart']."");
						if($noleggio > 0) {
							Db::getInstance()->executeS("UPDATE noleggio SET total = ".$_POST['totaleprodotti'].", mesi = ".$_POST['mesi_noleggio'].", spese_contratto = ".$_POST['spese_contratto_noleggio'].", parametri = '".$_POST['parametro_noleggio']."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_cart = '".$_POST['id_cart']."'");
							
						}
						else {
							Db::getInstance()->executeS("INSERT INTO noleggio (id_noleggio, id_cart, total, mesi, spese_contratto, parametri, date_add, date_upd) VALUES (NULL, ".$_POST['id_cart'].", ".$_POST['totaleprodotti'].", ".$_POST['mesi_noleggio'].", ".$_POST['spese_contratto_noleggio'].", '".$_POST['parametro_noleggio']."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
						}
					
					
					
					}
					else {
						Db::getInstance()->executeS("DELETE FROM noleggio WHERE id_cart = '".$_POST['id_cart']."'");
					
					}
			
					if(Tools::getIsset('provvisorio')) {
						mysql_query("update  ". _DB_PREFIX_."cart set provvisorio = 1 where id_cart = ".$_POST['id_cart']);
						mysql_query("update  ". _DB_PREFIX_."carrelli_creati set provvisorio = 1 where id_cart = ".$_POST['id_cart']);
					}
					else {
						mysql_query("update  ". _DB_PREFIX_."cart set provvisorio = 0 where id_cart = ".$_POST['id_cart']);
						mysql_query("update  ". _DB_PREFIX_."carrelli_creati set provvisorio = 0 where id_cart = ".$_POST['id_cart']);
					}
					
					
					if($cookie->id_employee != 1 &&  $cookie->id_employee != 2 &&  $cookie->id_employee != 6 && $cookie->id_employee != 7) {
						mysql_query("update  ". _DB_PREFIX_."cart set provvisorio = 1 where id_cart = ".$_POST['id_cart']);
						mysql_query("update  ". _DB_PREFIX_."carrelli_creati set provvisorio = 1 where id_cart = ".$_POST['id_cart']);
					}
					
					if(str_replace(",",".",$_POST['transport']) == $costo_spedizione) {
						
						
					}
					else {
						Db::getInstance()->executeS("update  ". _DB_PREFIX_."cart set transport = '".$_POST['metodo'].":".str_replace(",",".",$_POST['transport'])."' WHERE id_cart = ".$_POST['id_cart']);
						Db::getInstance()->executeS("update  ". _DB_PREFIX_."carrelli_creati set transport = '".$_POST['metodo'].":".str_replace(",",".",$_POST['transport'])."' WHERE id_cart = ".$_POST['id_cart']);
					}
					
					
					foreach ($_POST['nuovo_prodotto'] as $id_product_g=>$id_product) {
						
						if(isset($_POST['product_delete'][$id_product_g])) {
						}
						else {
							
						
							$price = str_replace(',', '.', $_POST['nuovo_price'][trim($id_product_g)]);
							$sconto_extra = str_replace(',', '.', $_POST['sconto_extra'][trim($id_product_g)]);	
							$acquisto = str_replace(',', '.', $_POST['wholesale_price'][trim($id_product_g)]);	
			

						
							$is_there_product = Db::getInstance()->getValue("SELECT count(id_product) FROM cart_product WHERE id_product = ".$_POST['nuovo_prodotto'][$id_product_g]." AND id_cart = ".$_POST['id_cart']."");
								
							if($is_there_product > 0) {
	
								$query="update ". _DB_PREFIX_."cart_product set ";
								$query.="id_product_attribute = '0', quantity = '".$_POST['nuovo_quantity'][trim($id_product_g)]."', price = ";
								$query.=$price.",".(isset($_GET['ajaxsave']) ? '' : ' bundle=0, ')."free = '".($_POST['nuovo_price'][$id_product_g] == 0 ? '1' : '0')."',name = '".$_POST['nuovo_name'][$id_product_g]."', sc_qta = '".($_POST['usa_sconti_quantita'][$id_product_g] == 0 ? '1' : '0')."', prezzo_acquisto = '".$acquisto."', sconto_extra = '".$sconto_extra."' WHERE id_cart = ".$_POST['id_cart']." AND id_product = ".$_POST['nuovo_prodotto'][$id_product_g]."";
									
								$query2="update ". _DB_PREFIX_."carrelli_creati_prodotti set ";
								$query2.="id_product_attribute = '0', quantity = '".$_POST['nuovo_quantity'][trim($id_product_g)]."', price = ";
								$query2.=$price.",".(isset($_GET['ajaxsave']) ? '' : ' bundle=0, ')." free = '".($_POST['nuovo_price'][$id_product_g] == 0 ? '1' : '0')."',name = '".$_POST['nuovo_name'][$id_product_g]."', sc_qta = '".($_POST['usa_sconti_quantita'][$id_product_g] == 0 ? '1' : '0')."', prezzo_acquisto = '".$acquisto."', sconto_extra = '".$sconto_extra."' WHERE id_cart = ".$_POST['id_cart']." AND id_product = ".$_POST['nuovo_prodotto'][$id_product_g]."";
								
							}
								
							else {
								
								$query="insert into ". _DB_PREFIX_."cart_product (id_cart,id_product,id_product_attribute,quantity,price,free,bundle,name,sc_qta,sconto_extra,prezzo_acquisto,date_add) values  ";
								$query.="(".$_POST['id_cart'].",".$_POST['nuovo_prodotto'][$id_product_g].",'0','".$_POST['nuovo_quantity'][trim($id_product_g)]."',";
								$query.=$price.",'".($_POST['nuovo_price'][$id_product_g] == 0 ? '1' : '0')."','".(isset($_GET['ajaxsave']) ? '999999' : '0')."','".$_POST['nuovo_name'][$id_product_g]."','".($_POST['usa_sconti_quantita'][$id_product_g] == 0 ? '1' : '0')."','".$sconto_extra."','".$acquisto."','".date("Y-m-d H:i:s")."')";
									
								$query2="insert into ". _DB_PREFIX_."carrelli_creati_prodotti (id_cart,id_product,id_product_attribute,quantity,price,free,bundle,name,sc_qta,sconto_extra,prezzo_acquisto,date_add) values  ";
								$query2.="(".$_POST['id_cart'].",".$_POST['nuovo_prodotto'][$id_product_g].",'0','".$_POST['nuovo_quantity'][trim($id_product_g)]."',";
								$query2.=$price.",'".($_POST['price'][$id_product_g] == 0 ? '1' : '0')."','".(isset($_GET['ajaxsave']) ? '999999' : '0')."','".$_POST['nuovo_name'][$id_product_g]."','".($_POST['usa_sconti_quantita'][$id_product_g] == 0 ? '1' : '0')."','".$sconto_extra."','".$acquisto."','".date("Y-m-d H:i:s")."')";
							
							}
								
							echo $query;
							mysql_query($query) or die(mysql_error());
							mysql_query($query2) or die(mysql_error());
							
							
							
						}
					}
					
					foreach ($_POST['product_name'] as $id_product=>$product_name) {
						$name = Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_lang = 5 AND id_product = ".$id_product."");
							
						if($name != $_POST['product_name'][$id_product]) {
							
								mysql_query("update  ". _DB_PREFIX_."cart_product set name='".addslashes($_POST['product_name'][$id_product])."' where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
								mysql_query("update  ". _DB_PREFIX_."carrelli_creati_prodotti set name='".addslashes($_POST['product_name'][$id_product])."' where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							} else { }
							
					}
					
					
					
					
					
					if ($_POST['product_price']) {
					
						foreach ($_POST['product_price'] as $id_product=>$price_product) {
						
							if(!empty($_POST['usa_sconti_quantita'][$id_product])) {
							
								$price_product = str_replace(',', '.', $_POST['product_price'][$id_product]);
								
								mysql_query("update  ". _DB_PREFIX_."cart_product set sc_qta=1 where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							}
							
							else {
								$price_product = str_replace(',', '.', $_POST['product_price'][$id_product]);
								mysql_query("update  ". _DB_PREFIX_."cart_product set sc_qta=0 where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							}
							
							
							mysql_query("update  ". _DB_PREFIX_."cart_product set ".(isset($_GET['ajaxsave']) ? '' : 'bundle=0,')." price=".$price_product." where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							mysql_query("update  ". _DB_PREFIX_."carrelli_creati_prodotti set ".(isset($_GET['ajaxsave']) ? '' : 'bundle=0,')."  price=".$price_product." where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							
							
							if($price_product == 0) {
							
								mysql_query("update  ". _DB_PREFIX_."cart_product set free=1 where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
								mysql_query("update  ". _DB_PREFIX_."carrelli_creati_prodotti set free=1 where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
								
							}
							
							
							
							
							
							mysql_query("update  ". _DB_PREFIX_."cart_product set quantity=".$_POST['product_quantity'][$id_product]." where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							mysql_query("update  ". _DB_PREFIX_."carrelli_creati_prodotti set quantity=".$_POST['product_quantity'][$id_product]." where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							
							$sconto_extra = str_replace(',', '.', $_POST['sconto_extra'][$id_product]);
							
							mysql_query("update  ". _DB_PREFIX_."cart_product set sconto_extra=".$sconto_extra." where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							mysql_query("update  ". _DB_PREFIX_."carrelli_creati_prodotti set sconto_extra=".$sconto_extra." where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							
							$acquisto = str_replace(',', '.', $_POST['wholesale_price'][$id_product]);
							
							mysql_query("update  ". _DB_PREFIX_."cart_product set prezzo_acquisto=".$acquisto.", no_acq = ".($acquisto == 0 ? 1 : 0)." where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
							mysql_query("update  ". _DB_PREFIX_."carrelli_creati_prodotti set prezzo_acquisto=".$acquisto.", no_acq = ".($acquisto == 0 ? 1 : 0)." where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product);
						}
						

						/*$total_products=$total_products*(1+$_POST['tax_rate']/100);
						mysql_query("update  ". _DB_PREFIX_."orders set total_products=".$total_products." where id_order=".$_POST['id_order']);
						$this->update_total($_POST['id_order']);*/

					}
					
					$i_ord = 0;
					foreach ($_POST['sort_order'] as $id_product_g=>$id_product) {
					
					
						mysql_query("update  ". _DB_PREFIX_."cart_product set sort_order=$i_ord where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product_g)."<br />";
						mysql_query("update  ". _DB_PREFIX_."carrelli_creati_prodotti set sort_order=$i_ord where id_cart = '".$_POST['id_cart']."' AND id_product=".$id_product_g);
						$i_ord++;
						
					
					}
					
					$files=$_FILES["joinFile"];
		
					$attachment = Db::getInstance()->getValue("SELECT attachment FROM cart WHERE id_cart = ".$_GET['id_cart']."");
					$attachments = array();
					$file_name = $attachment;
					
					if(isset($_POST['cancella_tutti_allegati'])) {
					
						$file_name = "";
					
					}
					
					$files=array();
					$fdata=$_FILES['joinFile'];
					if(is_array($fdata['name'])){
						for($i=0;$i<count($fdata['name']);++$i){
							$files[]=array(
								'name'    =>$fdata['name'][$i],
								'tmp_name'=>$fdata['tmp_name'][$i],
								'type' => $fdata['type'][$i],
								'size' => $fdata['size'][$i],
								'error' => $fdata['error'][$i],
							 
							);
						}
					}
					else $files[]=$fdata;
										
				
					require_once('../classes/html2pdf/html2pdf.class.php');
						if($attachment != '') {
						if(!empty($attachment)) {
									
							$allegati = explode(";",$attachment);
							foreach($allegati as $allegato) {
								
								if(strpos($allegato, ":::")) {
								
									$parti = explode(":::", $allegato);
									$nomevero = $parti[1];
									$nomecodificato = $parti[0];
												
								}
								
								else {
									$nomevero = $allegato;
								
								}
											
								if($allegato == "") { } else {
								
									$fileAttachment2['content'] = file_get_contents(_PS_UPLOAD_DIR_.$nomecodificato);
									$fileAttachment2['name'] = $nomevero;
									$fileAttachment2['mime'] = 'application/x-download';
									$attachments[] = $fileAttachment2;
									
								}
							}
							
									
						}
									
					}
						$id_cst = Db::getInstance()->getValue("SELECT id_customer FROM cart WHERE id_cart = ".$_POST['id_cart']."");
						$content = Cart::getCartPDF($_POST['id_cart'], $id_cst, 'y');
						
						
						//$html2pdf = new HTML2PDF('P','A4','it');
						//$html2pdf->WriteHTML($content);
						
						//$pdfdoc = $html2pdf->Output('', true);

						//$fileAttachment1['content'] = $pdfdoc;
						//$fileAttachment1['name'] = $id_cst.'-offerta-'.$_POST['id_cart'].'.pdf';
						//$fileAttachment1['mime'] = 'application/x-download';

									
						//$attachments[] = $fileAttachment1;
					
					foreach($files as $file) {
					
						if (isset($files) AND !empty($file['name']) AND $file['error'] != 0)
							$this->_errors[] = Tools::displayError('An error occurred with the file upload.');
		
							if (!empty($file['name']))
								{
									$extension = array('.txt', '.rtf', '.doc', '.docx', '.pdf', '.zip', '.png', '.jpeg', '.gif', '.jpg');
									$filename = md5(uniqid().substr($file['name'], -5));
									$fileAttachment['content'] = file_get_contents($file['tmp_name']);
									$fileAttachment['name'] = $file['name'];
									$fileAttachment['mime'] = $file['type'];
									
								}
								
								if (isset($filename) AND rename($file['tmp_name'], _PS_MODULE_DIR_.'../upload/'.$filename)) {
									$filename = $filename.":::".$file['name'];
									$file_name .= $filename.";";
									
							}
							
							
							$attachments[] = $fileAttachment;
							
					}
					
					
					
					
					mysql_query("update  ". _DB_PREFIX_."cart set attachment='".$file_name."' where id_cart = ".$_POST['id_cart']);
					
					mysql_query("update  ". _DB_PREFIX_."carrelli_creati set attachment='".$file_name."' where id_cart = ".$_POST['id_cart']);
					
					
					
					if(isset($_POST['notifica_mail']) && ($cookie->id_employee == 1 ||  $cookie->id_employee == 2 ||  $cookie->id_employee == 6 || $cookie->id_employee == 7)) {
						
						
								
						Db::getInstance()->executeS("UPDATE form_prevendita_thread SET status='closed' WHERE id_thread = ".Tools::getValue('rif_prev')."");
								
						$customer_login = Db::getInstance()->getValue("SELECT count(id_guest) FROM guest WHERE id_customer = ".$customer->id."");
						
						$notifiche = Db::getInstance()->getValue("SELECT notifiche FROM cart WHERE id_cart = ".$_POST['id_cart']."");
						$notifiche = unserialize($notifiche);
						
						$numero_notifiche =  Db::getInstance()->getValue("SELECT numero_notifiche FROM cart WHERE id_cart = ".$_POST['id_cart']."");
						
						$numero_notifiche++;
						
						
						
						$mail_verso_cui_inviare = Tools::getValue('customer_email_msg_cart');
						
						switch($cookie->id_employee) {
							case 1: $interno = "(201)";
							case 2: $interno = "(202)";
							case 3: $interno = "(204)";
							case 4: $interno = "(205)";
							case 5: $interno = "(203)";
							case 6: $interno = "";
							case 7: $interno = "(207)";
							default: $interno = "";
						}

						$employeemess = new Employee($cookie->id_employee);
						// INIZIO TICKET
						$gentilecliente = "Gentile cliente, <br />";
						
						$firma = '<br />
							Cordiali saluti / Best regards<br /><br />
							'.$employeemess->firstname." ".$employeemess->lastname.'<br /><br />
							Ezdirect srl <a href="http://www.ezdirect.it">www.ezdirect.it</a><br /><br />

							Tel +39 0585821163 '.$interno.'<br />
							Fax +39 0585821286<br />
						
							<br />
							Prima di stampare pensa all\'ambiente - Think about the environment before printing
							<p style="font-size:10px">
							Ai sensi del Decreto Legislativo n. 196/2003, si precisa che le informazioni contenute in questo messaggio e negli eventuali allegati sono riservate e per uso esclusivo del destinatario. Persone diverse dallo stesso non possono copiare o distribuire il messaggio a terzi. Chiunque riceva questo messaggio per errore, &egrave; pregato di distruggerlo e di informare immediatamente info@ezdirect.it<br /><br />
							
							This message is for the designated recipient only and may contain privileged or confidential information. If you have received it in error, please notify the sender immediately and delete the original. Any other use of the email by you is
							prohibited.
							
							</p>';
							
						

						$params = array('{msg}' => "Gentile cliente, <br /><br />
						Gentile cliente, grazie ancora per averci richiesto un preventivo. Lo abbiamo preparato e puoi visualizzarlo, quando vuoi, via web.<br /><br />
						Per visualizzarlo (o modificarlo e stamparlo in PDF) devi effettuare l'accesso al tuo account (per la login utilizza l'indirizzo email che hai fornito in fase di richiesta/registrazione. Se non hai inserito una password in fase di richiesta/registrazione, utilizza ".($customer->is_company == 1 ? "la tua partita iva" : "il tuo codice fiscale")."). 
						<br /><br />
						In questo modo puoi controllare e stampare la tua offerta richiedere ulteriori informazioni o procedere all'acquisto.
						<br /><br />
						Il nostro staff &egrave; a tua completa disposizione qualora tu rilevassi difficolt&agrave; per accedere al tuo account e per ricevere ulteriore supporto tecnico commerciale.
						<br /><br />
						Se hai bisogno di supporto chiamaci al numero 0585821163. Grazie per la tua attenzione!".$firma);
						
						//Una volta effettuato l'accesso, seleziona <a href='http://www.ezdirect.it/modules/mieofferte/offerte.php'>\"Le mie offerte\"</a>. Troverai uno o pi&ugrave; carrelli con il contenuto dettagliato.
						
						$s_mails = explode(";", Tools::getValue('customer_email_msg_cart'));
						foreach ($s_mails as $s_mail) {
							if($s_mail == '') {
							}
							else {
								Customer::findEmailPersona(trim($s_mail), $customer->id);
								
								$id_persona = Db::getInstance()->getValue("SELECT id_persona FROM persone WHERE email = '".trim($s_mail)."'");
						
								
								if(Mail::Send(5, 'msg_base', Mail::l('La tua quotazione su Ezdirect', 5), $params, trim($s_mail), NULL, 'nonrispondere@ezdirect.it', 'Ezdirect',$attachments, NULL, _PS_MAIL_DIR_, true)) {
									Db::getInstance()->executeS("UPDATE cart SET numero_notifiche = ".$numero_notifiche.", id_persona = ".$id_persona.", data_ultima_notifica = '".date("Y-m-d H:i:s")."' WHERE id_cart = ".$_POST['id_cart']."");
									Db::getInstance()->executeS("UPDATE carrelli_creati SET numero_notifiche = ".$numero_notifiche.", id_persona = ".$id_persona.", data_ultima_notifica = '".date("Y-m-d H:i:s")."' WHERE id_cart = ".$_POST['id_cart']."");
								}
								
								$notifica = array();
								$notifica['email'] = $s_mail;
								$notifica['data'] = date("Y-m-d H:i:s");
								
								$notifiche[] = $notifica;

								$log_mail=fopen("../import/log-mail.txt","a+");
								$riga_log = "CAR | Mail inviata a ".trim($s_mail)." in data ".date("Y-m-d H:i:s")."\n";
								@fwrite($log_mail,$riga_log);									
							}
						}
						
						foreach ($_POST['seleziona-persona'] as $persona) {
			
							echo $persona."<br />";
							$notifica = array();
							$notifica['email'] = $persona;
							$notifica['data'] = date("Y-m-d H:i:s");
							
							$notifiche[] = $notifica;
							
							
							
							if (Mail::Send(5, 'msg_base', Mail::l('La tua quotazione su Ezdirect', 5), $params, trim($persona), NULL, 'nonrispondere@ezdirect.it', 'Ezdirect',$attachments, NULL, _PS_MAIL_DIR_, true)) {
								Db::getInstance()->executeS("UPDATE cart SET numero_notifiche = ".$numero_notifiche.", id_persona = ".$id_persona.", data_ultima_notifica = '".date("Y-m-d H:i:s")."' WHERE id_cart = ".$_POST['id_cart']."");
									Db::getInstance()->executeS("UPDATE carrelli_creati SET numero_notifiche = ".$numero_notifiche.", id_persona = ".$id_persona.", data_ultima_notifica = '".date("Y-m-d H:i:s")."' WHERE id_cart = ".$_POST['id_cart']."");
								}
							$log_mail=fopen("../import/log-mail.txt","a+");
							$riga_log = "CAR | Mail inviata a ".$persona." in data ".date("Y-m-d H:i:s")."\n";
							@fwrite($log_mail,$riga_log);							
							
						}
						
						
						
						foreach($_POST['conoscenza'] as $conoscenza) {
						
							$employeemess = new Employee($cookie->id_employee);
							$mailconoscenza = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = $conoscenza");
							$tokenimp = Tools::getAdminToken("AdminCarts".(int)(Tab::getIdFromClassName('AdminCarts')).$conoscenza);
								
							$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCarts&id_cart=".Tools::getValue('id_cart')."&viewcart&token=".$tokenimp.'';
							
							$params4 = array(
							'{reply}' => "<strong>".$employeemess->firstname."</strong> ti ha inviato una notifica carrello in copia conoscenza.
							<br /><br />
							L'ID del carrello &egrave;: <strong>".Tools::getValue('id_cart')."</strong><br /><br />
							
							<a href='".$linkimp."'>Clicca qui per aprire il carrello</a>. ",
							'{firma}' => "",
							'{id_richiesta}' => "",
							'{link}' => "");
						
							Mail::Send((int)$cookie->id_lang, 'senzagrafica', 'Notifica carrello in copia conoscenza', 
							$params4, $mailconoscenza, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
							_PS_MAIL_DIR_, true);
							
							$notifica = array();
							$notifica['email'] = $mailconoscenza;
							$notifica['data'] = date("Y-m-d H:i:s");
							
							$notifiche[] = $notifica;
						
						}
						
						$notifiche = serialize($notifiche);
						
						$notifiche =  Db::getInstance()->executeS("UPDATE cart SET notifiche = '".$notifiche."' WHERE id_cart = ".$_POST['id_cart']."");
						$notifiche =  Db::getInstance()->executeS("UPDATE carrelli_creati SET notifiche = '".$notifiche."' WHERE id_cart = ".$_POST['id_cart']."");					
					
					}
					else 
					{
						$notifiche = array();
						$s_mails = explode(";", Tools::getValue('customer_email_msg_cart'));
						foreach ($s_mails as $s_mail) {
							if($s_mail == '') {
							}
							else {
								Customer::findEmailPersona(trim($s_mail), $customer->id);
								
								$id_persona = Db::getInstance()->getValue("SELECT id_persona FROM persone WHERE email = '".trim($s_mail)."'");
						
								
								$notifiche[] = $id_persona;
															
							}
						}
						
						foreach ($_POST['seleziona-persona'] as $persona) {
			
							$id_persona = Db::getInstance()->getValue("SELECT id_persona FROM persone WHERE email = '".trim($persona)."' AND id_customer = '".$customer->id."'");
							$notifiche[] = $id_persona;
							
						}
						
						$notifiche = serialize($notifiche);
						
						Db::getInstance()->executeS("UPDATE cart SET email_preparate = '".$notifiche."' WHERE id_cart = ".$_POST['id_cart']."");
						
						Db::getInstance()->executeS("UPDATE carrelli_creati SET email_preparate = '".$notifiche."' WHERE id_cart = ".$_POST['id_cart']."");
					
					}
					
					
					if(isset($_POST['prepara_mail']) && ($cookie->id_employee != 1 &&  $cookie->id_employee != 2 &&  $cookie->id_employee != 6 && $cookie->id_employee != 7)) 
					{
						$notifiche = array();
						$s_mails = explode(";", Tools::getValue('customer_email_msg_cart'));
						foreach ($s_mails as $s_mail) {
							if($s_mail == '') {
							}
							else {
								Customer::findEmailPersona(trim($s_mail), $customer->id);
								
								$id_persona = Db::getInstance()->getValue("SELECT id_persona FROM persone WHERE email = '".trim($s_mail)."'");
						
								
								$notifiche[] = $id_persona;
															
							}
						}
						
						foreach ($_POST['seleziona-persona'] as $persona) {
			
							$id_persona = Db::getInstance()->getValue("SELECT id_persona FROM persone WHERE email = '".trim($persona)."' AND id_customer = '".$customer->id."'");
							$notifiche[] = $id_persona;
							
						}
						
						$notifiche = serialize($notifiche);
						
						Db::getInstance()->executeS("UPDATE cart SET email_preparate = '".$notifiche."' WHERE id_cart = ".$_POST['id_cart']."");
						
						Db::getInstance()->executeS("UPDATE carrelli_creati SET email_preparate = '".$notifiche."' WHERE id_cart = ".$_POST['id_cart']."");
					
					}
					
					
					if($_POST['Apply'] == 'Conferma ordine') 
					{
						Tools::redirectAdmin('index.php?tab=AdminCarts&id_customer='.$customer->id.'&id_cart='.$_GET['id_cart'].'&convertcart&token='.$tokenCarts.'');
					
					}
					
				}


			
			} 
			// echo "alert('Are you sure you want to give us the deed to your house?')"
		
		
			Tools::redirectAdmin($currentIndex.'&id_cart='.$_POST['id_cart'].'&id_customer='.$customer->id.'&viewcustomer&conf=4&tab-container-1=4&preventivo='.Tools::getValue('preventivo').'&viewcart'.'&token='.$this->token.(isset($_POST['notifica_mail']) ? '&notifica_inviata=y' : '') );
		
		
		
		}
		
	
		
		
	
		if(!isset($_POST['copia_carrello'])) {
			if(Tools::getIsset('vedirevisione'))
			{
			}
			else {
				echo '
			
				<p style="margin-top:-44px; padding-left:150px">
				<form action="#copia-carrello" method="post">
				<input type="hidden" name="id_cart" value="'.$_GET['id_cart'].'" />
				<input style="display:block; '.(Tools::getValue('preventivo') == 2 ? 'margin-left:300px' : 'margin-left:150px').'; z-index:999999999;" name="copia_carrello" type="submit" class="button_invio" value="Copia questo carrello su un altro cliente" />
				</form>
				</p>';
			}
		}
		
			if($provvisorio == 0 && Tools::getValue('preventivo') != 2 && !$order->id) 
		{
			echo '<div style="position:relative; margin-top:-26px;float:left; margin-left:450px;">';
			echo '<a class="button"  style=" margin-left:0px"  href="index.php?tab=AdminCarts&id_customer='.$customer->id.'&id_cart='.$_GET['id_cart'].'&convertcart&token='.$tokenCarts.'" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }">Converti in ordine</a>
			</div>
			';
			
		}
		
			if($order->id) 
		{
			if(Tools::getIsset('vedirevisione'))
			{
			}
			else {
			
				echo '<div style="position:relative; margin-top:-26px;float:left; margin-left:450px;">';
				
				if(Tools::getValue('tab') == 'AdminCustomers')
					echo '<a class="button" href="?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&id_order='.(int)($order->id).'&vieworder&token='.$this->token.'&tab-container-1=4">Vai all\'ordine</a>';
				else
					echo '<a class="button" href="?tab=AdminOrders&id_order='.(int)($order->id).'&vieworder&token='.Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).(int)($cookie->id_employee)).'">Vai all\'ordine</a>';
					
				echo '</div>
				';
			}
		}
		
		if(isset($_POST['copia_carrello'])) {
		
			echo "<p id='copia-carrello'>";
			
			
			echo '<form action="#copia-carrello" method="post">
		<input type="hidden" name="id_cart" value="'.$_POST['id_cart'].'" />
		<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete.js"></script>
		Seleziona il cliente su cui copiare il carrello scegliendolo da questo modulo:<br /><br />
		<input size="123" type="text" value="" id="customer_autocomplete_input" /><br /><br />
			<script type="text/javascript">
					var formProduct = "";
					var products = new Array();
				</script>
				
				
				<script type="text/javascript">
				
				function addCustomer_TR(event, data, formatted)
				{
					
					var id_customer = data[0];
					var company = data[1];
					var firstname = data[2];
					var lastname = data[3];
					var vat_number = data[4];
					var tax_code = data[5];
					
					document.getElementById("customer_autocomplete_input").value = id_customer+" - "+company+" - "+firstname+" "+lastname+" - "+vat_number+" - "+tax_code; 
					document.getElementById("copy_to").value = id_customer;
				
				}
				
					/* function autocomplete */
					$(function() {
						$(\'#customer_autocomplete_input\')
							.autocomplete(\'ajax_customer_list.php\', {
								minChars: 1,
								max:50,
								scroll:true,
								formatItem: function(item, position) {
												return \'<tr><td style="width:50px; text-align:right">\'+item[0]+\'</td><td style="width:200px; text-align:left">\'+item[1]+\'</td><td style="width:150px;text-align:left">\'+item[2]+\'</td><td style="width:150px;text-align:left ">\'+item[3]+\'</td><td style="width:125px;text-align:left">\'+item[4]+\'</td><td style="width:125px;text-align:left">\'+item[5]+\'</td></tr>\'
												
												;
											}	
								
							}).result(addCustomer_TR);
							
							
					});
				
					$("#customer_autocomplete_input").css(\'width\',\'850px\');
					$("#customer_autocomplete_input").keypress(function(event){
								
								  var keycode = (event.keyCode ? event.keyCode : event.which);
								  if (keycode == "13") {
									event.preventDefault();
									event.stopPropagation();    
								  }
								});
								
					
				</script>
		<input name="copy_to" id="copy_to" value="" type="hidden" />
		<input name="copia_carrello_conferma" type="submit" class="button_invio"  onclick="javascript: var surec=window.confirm(\'Sei sicuro di voler copiare il carrello?\'); if (surec) { return true; } else { return false; }" value="Copia carrello su cliente selezionato" />
		</form>';
			
			
			
			
			
			
			echo "<p>";
		
		
		}
		
		
		if(Tools::getIsset('copia_carrello_conferma')) {
			
			$copy_to = trim(Tools::getValue('copy_to'));
			
			if(Tools::getValue('id_cart') <= 0 || !is_numeric(Tools::getValue('id_cart'))) 
			{
				$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
				
				Tools::redirectAdmin("index.php?tab=AdminCustomers&viewcart&editcart&createnew&provvisorio=1&id_customer=".$copy_to."&viewcustomer&token=".$tokenCustomers."&tab-container-1=4&preventivo=1#modifica-carrello");
			}
			
			
			$nuovo_carrello = Db::getInstance()->getValue("SELECT id_cart FROM cart ORDER BY id_cart DESC") or die (mysql_error());
			
			$nuovo_carrello = $nuovo_carrello+1;
			
			$da_copiare = Db::getInstance()->getRow('SELECT * FROM cart WHERE id_cart = '.Tools::getValue('id_cart').'');
			$da_copiare_2 = Db::getInstance()->getRow('SELECT * FROM carrelli_creati WHERE id_cart = '.Tools::getValue('id_cart').'');
			
			if(Tools::getIsset('preventivo'))
				$da_copiare['preventivo'] = Tools::getValue('preventivo');
				
		
			$cstp = new Customer($copy_to);
			$personac = Db::getInstance()->getValue('SELECT id_persona FROM persone WHERE firstname = "'.$cstp->firstname.'" AND lastname = "'.$cstp->lastname.'"');
			
			Db::getInstance()->executeS('INSERT INTO cart (id_cart, id_carrier, id_lang, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, date_add, date_upd, rif_prev, name, riferimento, esigenze, risorse, note, note_private, preventivo, validita, consegna, in_carico_a, payment, attachment, created_by, id_employee, numero_notifiche, provvisorio) VALUES ('.$nuovo_carrello.', "'.$da_copiare['id_carrier'].'", "'.$da_copiare['id_lang'].'","'.addslashes($da_copiare['id_address_delivery']).'","'.addslashes($da_copiare['id_address_invoice']).'",1,'.$copy_to.',0,"",0,0,"","'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'","'.addslashes($da_copiare['rif_prev']).'","'.addslashes($da_copiare['name']).'","'.$personac.'","'.addslashes(($da_copiare['esigenze'] != '' ? $da_copiare['esigenze'] : $da_copiare_2['esigenze'])).'", "'.addslashes(($da_copiare['risorse'] != '' ? $da_copiare['risorse'] : $da_copiare_2['risorse'])).'", "'.($da_copiare['note'] != '' ? addslashes($da_copiare['note']) : addslashes($da_copiare_2['note'])).'","'.addslashes(($da_copiare['note_private'] != '' ? $da_copiare['note_private'] : $da_copiare_2['note_private'])).'","'.addslashes($da_copiare['preventivo']).'","'.$da_copiare['validita'].'","'.addslashes($da_copiare['consegna']).'","'.addslashes($da_copiare['in_carico_a']).'","'.$da_copiare['payment'].'","'.$da_copiare['attachment'].'",'.$cookie->id_employee.','.$cookie->id_employee.',0,1)');

		
			Db::getInstance()->executeS('INSERT INTO carrelli_creati (id_cart, id_carrier, id_lang, id_address_delivery, id_address_invoice, id_currency, id_customer, id_guest, secure_key, recyclable, gift, gift_message, date_add, date_upd, rif_prev, name, riferimento, esigenze, risorse, note, note_private, preventivo, validita, consegna, in_carico_a, payment, attachment, created_by, id_employee, numero_notifiche, provvisorio) VALUES ('.$nuovo_carrello.', "'.$da_copiare['id_carrier'].'", "'.$da_copiare['id_lang'].'","'.addslashes($da_copiare['id_address_delivery']).'","'.addslashes($da_copiare['id_address_invoice']).'",1,'.$copy_to.',0,"",0,0,"","'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'","'.addslashes($da_copiare['rif_prev']).'","'.addslashes($da_copiare['name']).'","'.$personac.'","'.addslashes(($da_copiare['esigenze'] != '' ? $da_copiare['esigenze'] : $da_copiare_2['esigenze'])).'", "'.addslashes(($da_copiare['risorse'] != '' ? $da_copiare['risorse'] : $da_copiare_2['risorse'])).'", "'.($da_copiare['note'] != '' ? addslashes($da_copiare['note']) : addslashes($da_copiare_2['note'])).'","'.addslashes(($da_copiare['note_private'] != '' ? $da_copiare['note_private'] : $da_copiare_2['note_private'])).'","'.addslashes($da_copiare['preventivo']).'","'.$da_copiare['validita'].'","'.addslashes($da_copiare['consegna']).'","'.addslashes($da_copiare['in_carico_a']).'","'.$da_copiare['payment'].'","'.$da_copiare['attachment'].'",'.$cookie->id_employee.','.$cookie->id_employee.',0,1)');
			
			if(Tools::getIsset('rif_prev')) 
			{
				Db::getInstance()->executeS("UPDATE form_prevendita_thread SET status='pending1' WHERE id_thread = ".Tools::getValue('rif_prev')."");
				Db::getInstance()->executeS("UPDATE cart SET rif_prev = ".Tools::getValue('rif_prev').",  esigenze = '".addslashes(Db::getInstance()->getValue('SELECT message FROM form_prevendita_message WHERE id_thread='.Tools::getValue('rif_prev').' ORDER BY id_message ASC'))."' WHERE id_cart = '".$nuovo_carrello."'");
				
				Db::getInstance()->executeS("UPDATE carrelli_creati SET rif_prev = ".Tools::getValue('rif_prev').", esigenze = '".Db::getInstance()->getValue('SELECT message FROM form_prevendita_message WHERE id_thread='.Tools::getValue('rif_prev').' ORDER BY id_message DESC')."' WHERE id_cart = '".$nuovo_carrello."'");
				
			}

			$prodotti_da_copiare = Db::getInstance()->executeS('SELECT * FROM cart_product WHERE id_cart = '.trim(Tools::getValue('id_cart')).'');
		
			foreach($prodotti_da_copiare as $prodotto_da_copiare) {
			
				Db::getInstance()->executeS('INSERT INTO cart_product (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, prezzo_acquisto, no_acq, sconto_extra, sort_order, bundle, date_add) VALUES ('.$nuovo_carrello.', "'.$prodotto_da_copiare['id_product'].'", "'.$prodotto_da_copiare['id_product_attribute'].'", "'.$prodotto_da_copiare['quantity'].'", "'.$prodotto_da_copiare['price'].'","'.$prodotto_da_copiare['free'].'","'.addslashes($prodotto_da_copiare['name']).'","'.$prodotto_da_copiare['sc_qta'].'","'.$prodotto_da_copiare['wholesale_price'].'","'.$prodotto_da_copiare['no_acq'].'","'.$prodotto_da_copiare['sconto_extra'].'","'.$prodotto_da_copiare['sort_order'].'","'.$prodotto_da_copiare['bundle'].'","'.date('Y-m-d H:i:s').'")');   
			
				Db::getInstance()->executeS('INSERT INTO carrelli_creati_prodotti (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, prezzo_acquisto, no_acq, sconto_extra, sort_order, bundle, date_add) VALUES ('.$nuovo_carrello.', "'.$prodotto_da_copiare['id_product'].'", "'.$prodotto_da_copiare['id_product_attribute'].'", "'.$prodotto_da_copiare['quantity'].'", "'.$prodotto_da_copiare['price'].'","'.$prodotto_da_copiare['free'].'","'.addslashes($prodotto_da_copiare['name']).'","'.$prodotto_da_copiare['sc_qta'].'","'.$prodotto_da_copiare['wholesale_price'].'","'.$prodotto_da_copiare['no_acq'].'","'.$prodotto_da_copiare['sconto_extra'].'","'.$prodotto_da_copiare['sort_order'].'","'.$prodotto_da_copiare['bundle'].'","'.date('Y-m-d H:i:s').'")');   
			
			
			
			}
			
			Tools::redirectAdmin($currentIndex.'&id_cart='.$nuovo_carrello.'&id_customer='.$copy_to.'&viewcustomer&conf=4&tab-container-1=4&preventivo='.Tools::getValue('preventivo').'&viewcart'.'&token='.$this->token.(Tools::getIsset('notifica_mail') ? '&notifica_inviata=y' : '') );
		
		}
		
		
		
		
		
		
		
		
		
	}

	private function displayCustomizedDatas(&$customizedDatas, &$product, &$currency, &$image, $tokenCatalog, &$stock)
	{
		if (!($order = AdminCarts::loadObject(true)))
			return;

		if (is_array($customizedDatas) AND isset($customizedDatas[(int)($product['id_product'])][(int)($product['id_product_attribute'])]))
		{
			if ($image = new Image($image['id_image']))
				echo '
					<tr>
						<td align="center">'.(isset($image->id_image) ? cacheImage(_PS_IMG_DIR_.'p/'.$image->getExistingImgPath().'.jpg',
						'product_mini_'.(int)($product['id_product']).(isset($product['id_product_attribute']) ? '_'.(int)($product['id_product_attribute']) : '').'.jpg', 45, 'jpg') : '--').'</td>
						<td><a href="index.php?tab=AdminCatalog&id_product='.$product['id_product'].'&updateproduct&token='.$tokenCatalog.'">
							<span class="productName">'.$product['name'].'</span>'.(isset($product['attributes']) ? '<br />'.$product['attributes'] : '').'<br />
							'.($product['reference'] ? AdminCarts::lx('Ref:').' '.$product['reference'] : '')
							.(($product['reference'] AND $product['supplier_reference']) ? ' / '.$product['supplier_reference'] : '')
							.'</a></td>
						<td align="center">'.Tools::displayPrice($product['price_wt'], $currency, false).'</td>
						<td align="center" class="productQuantity">'.$product['customizationQuantityTotal'].'</td>
						<td align="center" class="productQuantity">'.(int)($stock['quantity']).'</td>
						<td align="right">'.Tools::displayPrice($product['total_customization_wt'], $currency, false).'</td>
					</tr>';
			foreach ($customizedDatas[(int)($product['id_product'])][(int)($product['id_product_attribute'])] AS $customization)
			{
				echo '
				<tr>
					<td colspan="2">';
				foreach ($customization['datas'] AS $type => $datas)
					if ($type == _CUSTOMIZE_FILE_)
					{
						$i = 0;
						echo '<ul style="margin: 0; padding: 0; list-style-type: none;">';
						foreach ($datas AS $data)
							echo '<li style="display: inline; margin: 2px;">
									<a href="displayImage.php?img='.$data['value'].'&name='.(int)($order->id).'-file'.++$i.'" target="_blank"><img src="'._THEME_PROD_PIC_DIR_.$data['value'].'_small" alt="" /></a>
								</li>';
						echo '</ul>';
					}
					elseif ($type == _CUSTOMIZE_TEXTFIELD_)
					{
						$i = 0;
						echo '<ul style="margin-bottom: 4px; padding: 0; list-style-type: none;">';
						foreach ($datas AS $data)
							echo '<li>'.($data['name'] ? $data['name'] : AdminCarts::lx('Text #').++$i).AdminCarts::lx(':').' <b>'.$data['value'].'</b></li>';
						echo '</ul>';
					}
				echo '</td>
					<td align="center"></td>
					<td align="center" class="productQuantity">'.$customization['quantity'].'</td>
					<td align="center" class="productQuantity"></td>
					<td align="center"></td>
				</tr>';
			}
		}
	}

	public function display()
	{
		global $cookie;

		if (isset($_GET['view'.$this->table]))
			$this->viewDetails();
		else
		{
			
			echo "Cerca carrello in base al codice prodotto: 
			<form action='' name='cercarecarrelloprodotto' method='post'>";
			echo '<script type="text/javascript" src="../js/select2.js"></script>
					<script type="text/javascript">
					$(document).ready(function() { $("#cercacarrelloprodotto").select2(); });
					</script>';
			echo '<select id="cercacarrelloprodotto" name="cercacarrelloprodotto" style="width:600px">
					<option name="0" value="0">--- Scegli un prodotto ---</option>
					';
					
					
					$results = Db::getInstance()->ExecuteS("SELECT DISTINCT p.id_product, p.reference, p.price, pl.name FROM product p LEFT JOIN product_lang pl ON p.id_product = pl.id_product WHERE pl.id_lang = 5");
					
					foreach ($results as $row) {
					
						echo "<option name='$row[reference]' value='$row[id_product]'"; 
						echo ">$row[name] ($row[reference]) - ".Tools::displayPrice($row['price'], 1, false)."</option>";
					
					}
						
					echo "</select>&nbsp;&nbsp;&nbsp;";
			
			echo "<input type='submit' value='Cerca' class='button' /></form>
			<br />";
			
			if(isset($cookie->_wherecart)) {
				$prodottocercato = Db::getInstance()->getRow('SELECT name, reference FROM product p JOIN product_lang pl ON p.id_product = pl.id_product WHERE pl.id_lang = '.$cookie->id_lang.' AND p.id_product = '.$cookie->_cercacarrelloprodotto);
				echo "<strong>Stai vedendo la lista dei carrelli che contengono: ".$prodottocercato['name']." (".$prodottocercato['reference'].")</strong>";
				echo "<br />";
			
			}
		
			$this->getList((int)($cookie->id_lang), !Tools::getValue($this->table.'Orderby') ? 'date_add' : NULL, !Tools::getValue($this->table.'Orderway') ? 'DESC' : NULL);
			$this->displayList();
		}
	}
	
	protected function _displayDeleteLink($token = NULL, $id)
	{
		global $currentIndex;
		
		foreach ($this->_list as $cart)
			if ($id == $cart['id_cart'])
				if ($cart['id_order'])
					return;
		
		$_cacheLang['Delete'] = AdminCarts::lx('Delete', __CLASS__, true, false);
		$_cacheLang['DeleteItem'] = AdminCarts::lx('Delete item #', __CLASS__, true, false).$id.' ?)';
		
		echo '
			<a href="'.$currentIndex.'&'.$this->identifier.'='.$id.'&delete'.$this->table.'&token='.($token != null ? $token : $this->token).'" onclick="return confirm(\''.$_cacheLang['DeleteItem'].'\');">
			<img src="../img/admin/delete.gif" alt="'.$_cacheLang['Delete'].'" title="'.$_cacheLang['Delete'].'" /></a>
		';
	}
	
	protected function lx($string, $class = 'AdminCarts', $addslashes = FALSE, $htmlentities = TRUE)
	{
		// if the class is extended by a module, use modules/[module_name]/xx.php lang file
		$currentClass = get_class($this);
		if (Module::getModuleNameFromClass($currentClass))
		{
			$string = str_replace('\'', '\\\'', $string);
			return Module::findTranslation(Module::$classInModule[$currentClass], $string, $currentClass);
		}
		global $_LANGADM;

		if ($class == __CLASS__)
				$class = 'AdminCarts';

		$key = md5(str_replace('\'', '\\\'', $string));
		$str = (key_exists(get_class($this).$key, $_LANGADM)) ? $_LANGADM[get_class($this).$key] : ((key_exists($class.$key, $_LANGADM)) ? $_LANGADM[$class.$key] : $string);
		$str = $htmlentities ? htmlentities($str, ENT_QUOTES, 'utf-8') : $str;
		return str_replace('"', '&quot;', ($addslashes ? addslashes($str) : stripslashes($str)));
	}
	
	protected function getCarriersForEditOrder($id_zone, $groups = null, $id_cart)
	{
		$cart = new Cart($id_cart);
		global $cookie; 
		
		if (is_array($groups) && !empty($groups))
			$result = Carrier::getCarriers((int)$cookie->id_lang, false, false, (int)$id_zone, $groups, Carrier::PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE);
		else
			$result = Carrier::getCarriers((int)$cookie->id_lang, false, false, (int)$id_zone, array(1), Carrier::PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE);
		$resultsArray = array();

		foreach ($result as $k => $row)
		{
			$carrier = new Carrier((int)$row['id_carrier']);
			$shippingMethod = $carrier->getShippingMethod();
			if ($shippingMethod != Carrier::SHIPPING_METHOD_FREE)
			{
				if($row['active'] == 0 && $row['name'] != 'Trasporto gratuito') {
					unset($result[$k]);
					continue;
				
				}
				if($id_zone == 0) {
					$id_zone = 9;
				}
				// Get only carriers that are compliant with shipping method
				if (($shippingMethod == Carrier::SHIPPING_METHOD_WEIGHT && $carrier->getMaxDeliveryPriceByWeight($id_zone) === false)
					|| ($shippingMethod == Carrier::SHIPPING_METHOD_PRICE && $carrier->getMaxDeliveryPriceByPrice($id_zone) === false))
				{
					unset($result[$k]);
					continue;
				}

				// If out-of-range behavior carrier is set on "Desactivate carrier"
				if ($row['range_behavior'])
				{
					// Get id zone
					if (!$id_zone)
						$id_zone = Country::getIdZone(Country::getDefaultCountryId());

					// Get only carriers that have a range compatible with cart
					if (($shippingMethod == Carrier::SHIPPING_METHOD_WEIGHT && (!Carrier::checkDeliveryPriceByWeight($row['id_carrier'], $cart->getTotalWeight(), $id_zone)))
						|| ($shippingMethod == Carrier::SHIPPING_METHOD_PRICE
							&& (!Carrier::checkDeliveryPriceByPrice($row['id_carrier'], $cart->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING), $id_zone, $cart->id_currency))))
					{
						unset($result[$k]);
						continue;
					}
				}
			}
			
			$row['name'] = (strval($row['name']) != '0' ? $row['name'] : Configuration::get('PS_SHOP_NAME'));
			$row['price'] = ($shippingMethod == Carrier::SHIPPING_METHOD_FREE ? 0 : $cart->getOrderShippingCost((int)$row['id_carrier']));
			$row['price_tax_exc'] = ($shippingMethod == Carrier::SHIPPING_METHOD_FREE ? 0 : $cart->getOrderShippingCost((int)$row['id_carrier'], false));
			$row['img'] = file_exists(_PS_SHIP_IMG_DIR_.(int)($row['id_carrier']).'.jpg') ? _THEME_SHIP_DIR_.(int)($row['id_carrier']).'.jpg' : '';

			// If price is false, then the carrier is unavailable (carrier module)
			if ($row['price'] === false)
			{
				unset($result[$k]);
				continue;
			}

			$resultsArray[] = $row;
		}
		return $resultsArray;
	}
	
	public function openUploadedFile()
	{
		$filename = $_GET['filename'];
		
		if(strpos($filename, ":::")) {
		
			$parti = explode(":::", $filename);
			$nomecodificato = $parti[0];
			$nomevero = $parti[1];
		
		}
		
		else {
		
			$nomecodificato = $filename;
			$nomevero = $filename;
		
		}
		
		$extensions = array('.txt' => 'text/plain', '.rtf' => 'application/rtf', '.doc' => 'application/msword', '.docx'=> 'application/msword',
		'.pdf' => 'application/pdf', '.zip' => 'multipart/x-zip', '.png' => 'image/png', '.jpeg' => 'image/jpeg', '.gif' => 'image/gif', '.jpg' => 'image/jpeg');

		$extension = '';
		foreach ($extensions AS $key => $val)
			if (substr($nomevero, -4) == $key OR substr($nomevero, -5) == $key)
			{
				$extension = $val;
				break;
			}
		echo $nomecodificato."<br />";
		echo $nomevero;
		ob_end_clean();
		header('Content-Type: '.$extension);
		header('Content-Disposition:attachment;filename="'.$nomevero.'"');
		readfile(_PS_UPLOAD_DIR_.$nomecodificato);
		die;
	}
	
}

