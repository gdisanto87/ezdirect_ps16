<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14516 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminAgenti extends AdminTab
{
	
	
	public function __construct()
	{
		
		global $cookie;
		if($cookie->id_employee != 6 && $cookie->id_employee != 1 && $cookie->id_employee != 22)
			die('Non hai i permessi per vedere questa pagina');
		
	 	$this->table = 'agente';
	 	$this->className = 'Agente';
	 	$this->lang = false;
	 	$this->edit = false;
	 	$this->view = true;
	 	$this->delete = false;
		$this->deleted = false;
		$this->_defaultOrderBy = 'company';

		
 		$this->fieldsDisplay = array(
		'id_agente' => array('title' => $this->l('ID'), 'align' => 'center', 'font-size' =>'12px', 'width' => 40, 'widthColumn' => 40),
		'company' => array('title' => $this->l('Rag. Soc.'), 'font-size' =>'14px',  'width' => 100, 'widthColumn' => 100),
		'firstname' => array('title' => $this->l('Nome'), 'font-size' =>'14px',  'width' => 70, 'widthColumn' => 70),
		'lastname' => array('title' => $this->l('Cognome'), 'font-size' =>'14px',  'width' => 70, 'widthColumn' => 70),
		'email' => array('title' => $this->l('E-mail'), 'width' => 40, 'maxlength' => 40, 'widthColumn' => 70),
		'city' => array('title' => $this->l('Città'), 'font-size' =>'14px',  'width' => 100, 'widthColumn' => 100),
		'vat_number' => array('title' => $this->l('P.IVA'), 'font-size' =>'14px',  'width' => 100, 'widthColumn' => 100)
		);

		

		parent::__construct();
	}

	public function viewagente()
	{
		global $cookie, $currentIndex;
		if (!($agente = $this->loadObject()))
			die('errore');
		
		echo '
		<script type="text/javascript" src="yetii.js"></script>
		<br />
		<div id="tab-container-1">
		
		<ul id="tab-container-2-nav" style="margin-top:-27px">
			
			<li '.($_GET['tab-container-1'] == 1 || !isset($_GET['tab-container-1']) ? 'class="activeli" ' : '').' style=""><a href="'.$currentIndex.'&id_agente='.$agente->id.'&viewagente&token='.$this->token.'&tab-container-1=1" '.($cookie->id_employee == 1 && (Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 4) ? '' : '').'><strong>Anagrafica</strong></a></li>
		</ul>
			
			
			
		<script type="text/javascript">
		var tabber1 = new Yetii({
		id: "tab-container-1"
		});
		</script><br /><br /><br />';
		$this->displayForm();
		
		echo '<br /><br /></div>';
	}
	
	public function displayForm($isMainTab = true)
	{
		global $cookie, $currentIndex;
		
		
		$obj = $this->loadObject();
		
	
		if(!isset($_GET['modificheabilitate'])) {
		echo "
		<script type='text/javascript'>
		$(document).ready(function(){
			$('#modificaanagrafica :input').attr('readonly', 'readonly');
			
			$('#modificaanagrafica :checkbox, #modificaanagrafica :radio').click(function(e) {
				e.preventDefault();
			});
			
			$('#modificaanagrafica select').attr('disabled', 'disabled'); 
		});
		// end
		</script>";
		
		echo '<fieldset><legend><img src="../img/admin/tab-customers.gif" />Anagrafica '.$this->l('Agente').'</legend>';
		echo '<form id="modificaanagrafica" action="index.php?tab=AdminAgenti&submitAddagenteAndStay&id_agente='.$obj->id.'&viewagente&modificheabilitate&token='.$this->token.'&tab-container-1=1" method="post" autocomplete="off">';
		}
		else {
		echo '
		<form id="modificaanagrafica" action="index.php?tab=AdminAgenti&submitAddagenteAndStay&viewagente&token='.$this->token.'&tab-container-1=1" method="post" autocomplete="off">';
		}
		
		
		echo ($obj->id ? '<input type="hidden" name="id_'.$this->table.'" value="'.$obj->id.'" />' : '').'
			';
	
		//	if ($this->getFieldValue($obj, 'is_company') == 1) {
				
		echo '<div class="anagrafica-cliente">
		'.$this->l('Ragione sociale:').'<br />
			
			<input type="text" size="33" name="company" value="'.htmlentities($this->getFieldValue($obj, 'company'), ENT_COMPAT, 'UTF-8').'" /> 
			</div>
		';

				
		echo '<div class="anagrafica-cliente">'.$this->l('First name:').' <sup>*</sup><br />
		
			<input type="text" size="33" name="firstname" value="'.htmlentities($this->getFieldValue($obj, 'firstname'), ENT_COMPAT, 'UTF-8').'" /> 
			</div>
				
		<div class="anagrafica-cliente">'.$this->l('Last name:').' <sup>*</sup><br />
				
			<input type="text" size="33" name="lastname" value="'.htmlentities($this->getFieldValue($obj, 'lastname'), ENT_COMPAT, 'UTF-8').'" /> 
			<!-- <span class="hint" name="help_box">'.$this->l('Invalid characters:').' 0-9!<>,;?=+()@#"?{}_$%:<span class="hint-pointer">&nbsp;</span></span> --><br /><br /></div>
		';
		
		echo "<div class='clear'></div>";
				
				
		echo '<div class="anagrafica-cliente">'.$this->l('Partita IVA:').' 
	
		<input type="text" size="33" name="vat_number" value="'.htmlentities($this->getFieldValue($obj, 'vat_number'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup></div>
		

					<div class="anagrafica-cliente">'.$this->l('Codice fiscale:').' 

		<input type="text" size="33" name="tax_code" value="'.htmlentities($this->getFieldValue($obj, 'tax_code'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup></div>
					
					<div class="anagrafica-cliente"></div>
					
		';
		echo "<div class='clear'></div><br />";
				
				
		echo '
		<div class="anagrafica-cliente">'.$this->l('E-mail address:').' 

		<input type="text" size="33" name="email" value="'.htmlentities($this->getFieldValue($obj, 'email'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup>
		<a href="mailto:'.htmlentities($this->getFieldValue($obj, 'email'), ENT_COMPAT, 'UTF-8').'"><div style="display:block; margin-top:-10px; float:right"><img src="../img/admin/outlook.gif" alt="Invia email con Outlook" title="Invia email con Outlook"  /></a><br /><a href="'.$currentIndex.'&id_customer='.$obj->id.'&viewcustomer&viewmessage&aprinuovomessaggio&token='.$this->token.'&tab-container-1=7"><img src="../img/admin/email.gif" alt="Apri nuovo messaggio" title="Apri nuovo messaggio" style="cursor:pointer"  /></a></div>
						</div>';
						
				
		echo '
		<div class="anagrafica-cliente">'.$this->l('PEC:').'<br /> 
		<input type="text" size="33" name="pec" value="'.htmlentities($this->getFieldValue($obj, 'pec'), ENT_COMPAT, 'UTF-8').'" /> 
		<a href="mailto:'.htmlentities($this->getFieldValue($obj, 'pec'), ENT_COMPAT, 'UTF-8').'">

		</div>';
				
				
		echo '
				
		<div class="clear"></div><br />';
				
		if(!isset($_GET['modificheabilitate'])) {
			echo '<button type="submit" class="button" name="abilitamodifiche">
			<img src="../img/admin/edit.gif" alt="Modifica" title="Modifica" />&nbsp;&nbsp;&nbsp;'.$this->l('Abilita modifiche').'
			</button>';
		}
		else {
			/*echo '<button type="submit" class="button" name="submitAdd'.$this->table.'">
			<img src="../img/admin/save.gif" alt="Salva" title="Salva" />&nbsp;&nbsp;&nbsp;'.$this->l('   Save   ').'
			</button>';
			*/
		}
				
			
		
				
		if(!isset($_GET['modificheabilitate'])) {
		
		echo '<button type="submit" class="button" name="submitAdd'.$this->table.'Note">
			<img src="../img/admin/save.gif" alt="Salva" title="Salva" />&nbsp;&nbsp;&nbsp;'.$this->l('Salva').'
			</button>';
		}
		else
		{
			echo '<button type="submit" class="button" name="submitAdd'.$this->table.'">
			<img src="../img/admin/save.gif" alt="Salva" title="Salva" />&nbsp;&nbsp;&nbsp;'.$this->l('Salva').'
			</button>';
			
		}
		
		echo '
		
			
		</form></fieldset>';
		
		
	}
}
