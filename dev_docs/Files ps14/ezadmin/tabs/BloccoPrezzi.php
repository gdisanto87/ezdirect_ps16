<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14632 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class BloccoPrezzi extends AdminTab
{
		public function display()
		{	
			global $cookie, $currentIndex;
			echo "<h1>Tutti i prezzi bloccati</h1>";
			
			echo "Questo strumento ti consente di verificare quali sono i prodotti con i prezzi bloccati (ovvero che sono esclusi dalle procedure di aggiornamento automatico). Spunta il flag e clicca su conferma per rimuovere i prodotti dalla lista.";
			
			if(isset($_POST['conferma_tutti']))
			{
				foreach($_POST['blocco_prezzi'] as $id_product => $blocco)
					Db::getInstance()->executeS('UPDATE product SET blocco_prezzi = 0 WHERE id_product = '.$id_product);
				
			}
			
			$prodotti = Db::getInstance()->executeS("SELECT p.id_product, p.blocco_prezzi, p.stock_quantity, p.reference, p.supplier_reference, p.listino, p.price, p.wholesale_price, pl.name nome_prodotto, m.name costruttore FROM product p JOIN product_lang pl ON p.id_product = pl.id_product JOIN manufacturer m ON p.id_manufacturer = m.id_manufacturer WHERE pl.id_lang = 5 AND p.blocco_prezzi = 1 ORDER BY ".(Tools::getIsset('orderby') ? Tools::getValue('orderby') : 'costruttore')." ".(Tools::getIsset('orderway') ? Tools::getValue('orderway') : 'ASC')."");
			
			include_once('functions.php');
			
			echo '
			<style type="text/css">
				tr.tr-red-border td {
				border:1px solid red;
				}
			</style>
			
			<script type="text/javascript">
				function activateEditForm(special_id, special_price, sp_from, sp_to) {
					document.getElementById("apri_form").style.display = "block";
					
					document.getElementById("new_special_price_"+special_id).style.display = "none";
					document.getElementById("new_special_price_2_"+special_id).style.display = "block";
					
					document.getElementById("sp_from_"+special_id).style.display = "none";
					document.getElementById("sp_from_2_"+special_id).style.display = "block";
					
					document.getElementById("sp_to_"+special_id).style.display = "none";
					document.getElementById("sp_to_2_"+special_id).style.display = "block";
					
					document.getElementById("azioni_"+special_id).style.display = "none";
					document.getElementById("azioni_2_"+special_id).style.display = "block";
					
					document.getElementById("chiudi_form").style.display = "block";
				}
				</script>';
			echo '<div id="apri_form"><form style="display:none" name="all_specials" action="index.php?tab=BloccoPrezzi&edit_price_conf&token='.$this->token.(isset($_GET['orderby']) ? '&orderby='.$_GET['orderby'] : '').(isset($_GET['order_by']) ? '&order_by='.$_GET['order_by'] : '').(isset($_GET['orderway']) ? '&orderway='.$_GET['orderway'] : '').(isset($_GET['order_way']) ? '&order_way='.$_GET['order_way'] : '').'" method="post"></div>';
			echo "<script type='text/javascript'>
			$(document).ready(function() {
				function moveScroll(){
					var scroll = $(window).scrollTop();
					var anchor_top = $('#tablePrezzi').offset().top;
					var anchor_bottom = $('#bottom_anchor').offset().top;
					if (scroll>anchor_top && scroll<anchor_bottom) {
					clone_table = $('#clone');
					if(clone_table.length == 0){
						clone_table = $('#tablePrezzi').clone();
						clone_table.attr('id', 'clone');
						clone_table.css({position:'fixed',
								 'pointer-events': 'none',
								 top:0});
						clone_table.width($('#tablePrezzi').width());
						$('#table-container').append(clone_table);
						$('#clone').css({visibility:'hidden'});
						$('#clone thead').css({'visibility':'visible','pointer-events':'auto'});
					}
					} else {
					$('#clone').remove();
					}
				}
				$(window).scroll(moveScroll); 
			});
		</script>
		<div id='table-container'>
		<table class='table' id='tablePrezzi'>";
			echo "<thead>";
			echo "<tr>";
			echo "<th style='width:150px'>Cod. prodotto <a href='index.php?tab=BloccoPrezzi&orderby=p.reference&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=BloccoPrezzi&orderby=p.reference&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
			echo "<th style='width:290px; max-width:290px'>Desc. prodotto <a href='index.php?tab=BloccoPrezzi&orderby=nome_prodotto&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=BloccoPrezzi&orderby=nome_prodotto&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
			echo "<th style='width:140px'>Costruttore <a href='index.php?tab=BloccoPrezzi&orderby=costruttore&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=BloccoPrezzi&orderby=costruttore&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
			echo "<th style='width:80px'>Mag. EZ<a href='index.php?tab=BloccoPrezzi&orderby=p.stock_quantity&orderway=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=BloccoPrezzi&orderby=p.stock_quantity&orderway=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
			
			echo "<th style='width:80px'>Listino <a href='index.php?tab=BloccoPrezzi&order_by=p.listino&order_way=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=BloccoPrezzi&order_by=p.listino&order_way=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
			
			
			echo "<th style='width:80px'>Acquisto <a href='index.php?tab=BloccoPrezzi&order_by=p.wholesale_price&order_way=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=BloccoPrezzi&order_by=p.wholesale_price&order_way=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
			echo "<th style='width:80px'>Prezzo web <a href='index.php?tab=BloccoPrezzi&order_by=p.price&order_way=asc&token=".$this->token."'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
			<a href='index.php?tab=BloccoPrezzi&order_by=p.price&order_way=desc&token=".$this->token."'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a></th>";
			echo "
			<th style='width:75px'>Azioni</th>";
			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
			
			
			$tokenProducts = Tools::getAdminToken('AdminCatalogExFeatures'.(int)Tab::getIdFromClassName('AdminCatalogExFeatures').(int)$cookie->id_employee);
			
			foreach ($prodotti as $prodotto) {
			
				echo "<tr>";
				echo "<td>".''."<span style='cursor:pointer' class='span-reference' title=\"".Product::showProductTooltip($prodotto['id_product'])."\"><a href='index.php?tab=AdminCatalogExFeatures&id_product=".$prodotto['id_product']."&updateproduct&token=".$tokenProducts."&tabs=2' target='_blank'>".$prodotto['reference']."</a></span></td>";
				echo "<td id='nome_prodotto_".$prodotto['id_specific_price']."' style='width:290px; max-width:290px'>".$prodotto['nome_prodotto']."</td>";
				echo "<td>".$prodotto['costruttore']."</td>";
				echo "<td style='text-align:right'>".$prodotto['stock_quantity']."</td>";
				echo "<td style='text-align:right'>".Tools::displayPrice($prodotto['listino'],1)."</td>";
				echo "<td style='text-align:right'>".Tools::displayPrice($prodotto['wholesale_price'],1)."</td>";
				echo "<td style='text-align:right'>".Tools::displayPrice($prodotto['price'],1)."</td>";
				echo "<td style='text-align:right'>
				<input type='checkbox' name='blocco_prezzi[".$prodotto['id_product']."]' />
				</td>";
				echo "</tr>";
			}
			echo "</tbody>";
			echo '</table></div><div id="bottom_anchor">
			</div><!-- <div id="chiudi_form" style="display:none"> --> 
			
			<input onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" type="submit" class="button" id="conferma_tutti" name="conferma_tutti" value="Conferma le modifiche" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			

			</form>';
			
					
			
		}
		
		
}

