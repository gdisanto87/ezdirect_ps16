<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/../classes/AdminTab.php');

class AdminAttachments extends AdminTab
{
	
	private $_productAttachements = array();
	
	public function __construct()
	{
		global $cookie;
		
	 	$this->table = 'attachment';
	 	$this->className = 'Attachment';
	 	$this->lang = true;
	 	$this->edit = true;
	 	$this->delete = true;
		
		$this->fieldsDisplay = array(
		'id_attachment' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 25),
		'name' => array('title' => $this->l('Name')),
		'description' => array('title' => $this->l('Descrizione')),
		'file' => array('title' => $this->l('File')));
	
		parent::__construct();
	}

	public function postProcess()
	{
		/* PrestaShop demo mode */
		if (_PS_MODE_DEMO_)
		{
			$this->_errors[] = Tools::displayError('This functionnality has been disabled.');
			return;
		}
		/* PrestaShop demo mode*/
		if (Tools::isSubmit('submitAdd'.$this->table))
		{
			if ($id = (int)(Tools::getValue('id_attachment')) AND $a = new Attachment($id))
			{
				$_POST['file'] = $a->file;
				$_POST['mime'] = $a->mime;
			}
			
			Db::getInstance()->executes("DELETE FROM product_attachment WHERE id_attachment = ".$id."");
			
			foreach($_POST['prodotti'] AS $prodotto) {
			
				
				Db::getInstance()->executeS("INSERT INTO product_attachment (id_attachment, id_product) VALUES ('".$id."', '".$prodotto."')");
			
			}
			
			
			
			
			if (!sizeof($this->_errors))
			{
				if (isset($_FILES['file']) AND is_uploaded_file($_FILES['file']['tmp_name']))
				{
					if ($_FILES['file']['size'] > (Configuration::get('PS_ATTACHMENT_MAXIMUM_SIZE') * 1024 * 1024))
						$this->_errors[] = $this->l('File too large, maximum size allowed:').' '.(Configuration::get('PS_ATTACHMENT_MAXIMUM_SIZE') * 1024).' '.$this->l('kb').'. '.$this->l('File size you\'re trying to upload is:').number_format(($_FILES['file']['size']/1024), 2, '.', '').$this->l('kb');
					else
					{
						do $uniqid = sha1(microtime());	while (file_exists(_PS_DOWNLOAD_DIR_.$uniqid));
						if (!copy($_FILES['file']['tmp_name'], _PS_DOWNLOAD_DIR_.$uniqid))
							$this->_errors[] = $this->l('File copy failed');
						$_POST['file_name'] = $_FILES['file']['name'];
						@unlink($_FILES['file']['tmp_name']);
						$_POST['file'] = $uniqid;
						$_POST['mime'] = $_FILES['file']['type'];
					}
				}
				elseif (array_key_exists('file', $_FILES) && (int)$_FILES['file']['error'] === 1) 
				{
					$max_upload = (int)(ini_get('upload_max_filesize'));
					$max_post = (int)(ini_get('post_max_size'));
					$upload_mb = min($max_upload, $max_post);
					$this->_errors[] = $this->l('the File').' <b>'.$_FILES['file']['name'].'</b> '.$this->l('exceeds the size allowed by the server. This limit is set to').' <b>'.$upload_mb.$this->l('Mb').'</b>';
				}
				elseif (!empty($_FILES['file']['tmp_name']))
					$this->_errors[] = $this->l('No file or your file isn\'t uploadable, check your server configuration about the upload maximum size.');
			}
			$this->validateRules();
		}
		return parent::postProcess();
	}
	
	public function displayForm($isMainTab = true)
	{
		global $currentIndex, $cookie;
		parent::displayForm();
		
	
		
		if (!($obj = $this->loadObject(true)))
			return;
		
			$products = Db::getInstance()->executeS("SELECT pl.name, pl.id_product FROM product_lang pl JOIN product_attachment pa ON pl.id_product = pa.id_product WHERE pl.id_lang = 5 AND pa.id_attachment = '".$obj->id."'");
	
		echo '
		<form action="'.$currentIndex.'&submitAdd'.$this->table.'=1&token='.$this->token.'" method="post" enctype="multipart/form-data">
		'.($obj->id ? '<input type="hidden" name="id_'.$this->table.'" value="'.$obj->id.'" />' : '').'
			<fieldset><legend><img src="../img/t/AdminAttachments.gif" />'.$this->l('Attachment').'</legend>
				<label>'.$this->l('Filename:').' </label>
				<div class="margin-form">';
		foreach ($this->_languages as $language)
			echo '	<div id="cname_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input size="130" type="text" name="name_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'name', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" /><sup> *</sup>
					</div>';							
		$this->displayFlags($this->_languages, $this->_defaultFormLanguage, 'cname¤cdescription', 'cname');
		echo '	</div>
				<div class="clear">&nbsp;</div>
				<label>'.$this->l('Description:').' </label>
				<div class="margin-form">';
		foreach ($this->_languages as $language)
			echo '	<div id="cdescription_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<textarea name="description_'.$language['id_lang'].'" rows="5" cols="70">'.htmlentities($this->getFieldValue($obj, 'description', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'</textarea>
					</div>';							
		$this->displayFlags($this->_languages, $this->_defaultFormLanguage, 'cname¤cdescription', 'cdescription');
		
		echo '	</div>
				<div class="clear">&nbsp;</div>
				<label>'.$this->l('File').'</label>
				<div class="margin-form">
					<p><input type="file" name="file" /></p>
					<p><!-- '.$this->l('Upload file from your computer').' --></p>
					'.($obj->file_name != '' ? '<p style="font-size:12px">File attuale: <a onclick="window.onbeforeunload = null" href="http://www.ezdirect.it/attachment.php?id_attachment='.$obj->id.'">'.$obj->file_name.'</a></p>
					
					<p style="font-size:12px">Link fisso: <a onclick="window.onbeforeunload = null" href="http://www.ezdirect.it/attachment.php?id_attachment='.$obj->id.'">http://www.ezdirect.it/attachment.php?id_attachment='.$obj->id.'</a> (se si cambia il nome del file, questo link non cambia)</p>
					
					<p style="font-size:12px">Link con nome: <a onclick="window.onbeforeunload = null" href="http://www.ezdirect.it/file/'.$obj->id.'/'.$obj->file_name.'">http://www.ezdirect.it/file/'.$obj->id.'/'.$obj->file_name.'</a> (se si cambia il nome del file, questo link cambia)</p>
					' : '').'
				</div>
				
				
				<div class="clear">&nbsp;</div>
				<label>'.$this->l('Prodotti').'</label>
				<div class="margin-form">
				<div id="divProducts">
				';
				$prodotti = array();
				foreach ($products as $product) {
				$prodotti[] = $product['id_product'];
				}
				
				
				$results = Db::getInstance()->ExecuteS("SELECT product.id_product, product.reference, product_lang.name FROM product JOIN product_lang ON product.id_product = product_lang.id_product WHERE product_lang.id_lang = 5 ORDER BY name");
				
				foreach ($results as $row) {
				
					if(in_array($row['id_product'], $prodotti)) {
					echo '<div style="font-size:13px; color:#000000" id="productId'.$row['id_product'].'">';
					echo (!empty($row['reference']) ? ' '.$row['reference'].' - ' : '').htmlentities($row['name'], ENT_COMPAT, 'UTF-8').' - '.$row['id_product'].'
					<span onclick="delProduct('.$row['id_product'].');" style="cursor: pointer;"><img src="../img/admin/delete.gif" class="middle" alt="" /></span>
					<input type="hidden" name="prodotti[]" id="inputProduct" value="'.$row['id_product'].'" />
					</div>';
					} 
					else { }
					
				
				
				}
				
				
				
				echo '
				</div>
				<script type="text/javascript">
					var formProduct = "";
					var products = new Array();
				</script>
	<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
							<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete.js"></script>
				<div id="ajax_choose_product" style="padding:6px; padding-top:2px; width:600px;">
				
					<input type="text" value="" style="width:600px"  id="product_autocomplete_input" />
						
				</div>
			
				<script type="text/javascript">
					function addProduct(event, data, formatted)
				{
					if (data == null)
						return false;
					var productId = data[1];
					var productName = data[0];

					var $divAccessories = $("#divProducts");
					
					var $inputAccessories = $("#inputProducts");
					var $nameAccessories = $("#nameProducts");
					
					/* delete product from select + add product line to the div, input_name, input_ids elements */
					$divAccessories.html($divAccessories.html() + productName + \' <span onclick="delProduct(\' + productId + \');" style="cursor: pointer;"><img src="../img/admin/delete.gif" class="middle" alt="" /></span><input type="hidden" name="prodotti[]" id="inputProduct" value="\'+productId+\'" /><br />\');
					$nameAccessories.val($nameAccessories.val() + productName + \'¤\');
					$inputAccessories.val($inputAccessories.val() + productId + \'-\');
					$("#product_autocomplete_input").val("");
					
					$("body").trigger("click");			
					$("#product_autocomplete_input").trigger("click");
					$("#product_autocomplete_input").trigger("click");
				}

				
				function delProduct(id)
				{
					var row = document.getElementById("productId"+id);
					row.parentNode.removeChild(row);
					
				}
				
					urlToCall = null;
					/* function autocomplete */
					$(function() {
						$(\'#product_autocomplete_input\')
							.autocomplete(\'ajax_products_list.php\', {
								minChars: 1,
								autoFill: true,
								max:20,
								matchContains: true,
								mustMatch:true,
								scroll:false,
								cacheLength:0,
								
							}).result(addProduct);
						
					});
					
					$("#product_autocomplete_input").keypress(function(event){
									
								  var keycode = (event.keyCode ? event.keyCode : event.which);
								  if (keycode == "13") {
									event.preventDefault();
									event.stopPropagation();    
								  }
								});
								
				</script>';
				
			
				
				
				echo'	
				
				</div>
				<div class="clear">&nbsp;</div>
				<div class="margin-form">
					<input type="submit" value="'.$this->l('   Salva   ').'" name="submitAdd'.$this->table.'" class="button" />
				</div>
				<div class="small"><sup>*</sup> '.$this->l('Required field').'</div>
			</fieldset>
		</form>';
	}
	
	public function getList($id_lang, $orderBy = NULL, $orderWay = NULL, $start = 0, $limit = NULL)
	{
		parent::getList((int)$id_lang, $orderBy, $orderWay, $start, $limit);
		if (sizeof($this->_list))
			$this->_productAttachements = Attachment::getProductAttached((int)$id_lang, $this->_list);	
	}
	
	protected function _displayDeleteLink($token = NULL, $id)
	{
	    global $currentIndex;
		
		$_cacheLang['Delete'] = $this->l('Delete');
		$_cacheLang['DeleteItem'] = $this->l('Delete item #', __CLASS__, TRUE, FALSE);
		
		if (isset($this->_productAttachements[$id]))
		{
			$productList = '';
			foreach($this->_productAttachements[$id] as $product)
				$productList .= $product.', ';
		}
		echo '
			<script>
				function confirmProductAttached(productList)
				{
					if (confirm(\''.$_cacheLang['DeleteItem'].$id.'\'))
						return confirm(\''.$this->l('This attachment is used by the following products:').'\r\n\' + productList);
					return false;
				}
			</script>
			<a href="'.$currentIndex.'&'.$this->identifier.'='.$id.'&delete'.$this->table.'&token='.($token!=NULL ? $token : $this->token).'"
			onclick="'.(isset($this->_productAttachements[$id]) ? 'return confirmProductAttached(\''.$productList.'\')' : 'return confirm(\''.$_cacheLang['DeleteItem'].$id.' ?'.(!is_null($this->specificConfirmDelete) ? '\r'.rtrim($this->specificConfirmDelete, ', ') : '').'\')' ).'">
			<img src="../img/admin/delete.gif" alt="'.$_cacheLang['Delete'].'" title="'.$_cacheLang['Delete'].'" /></a>';
	}
}
