<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

abstract class AdminStatsTab extends AdminPreferences
{
	public function __construct()
	{
 		$this->_fieldsSettings = array(
			'PS_STATS_RENDER' => array('title' => $this->l('Graph engine'), 'validation' => 'isGenericName'),
			'PS_STATS_GRID_RENDER' => array('title' => $this->l('Grid engine'), 'validation' => 'isGenericName')
		);
		parent::__construct();
	}
	
	public function postProcess()
	{
		global $cookie, $currentIndex;
		$submitted = false;
		
		if (Tools::isSubmit('submitDatePicker'))
		{
			$submitted = true;
			if (!Validate::isDate($from = date("Y-m-d",strtotime(Tools::getValue('datepickerFrom')))) OR !Validate::isDate($to = date("Y-m-d",strtotime(Tools::getValue('datepickerTo')))))
				$this->_errors[] = Tools::displayError('Date specified is invalid');
		}
		if (Tools::isSubmit('submitDateDay'))
		{
			$submitted = true;
			$from = date('Y-m-d');
			$to = date('Y-m-d');
		}
		if (Tools::isSubmit('submitDateDayPrev'))
		{
			$submitted = true;
			$yesterday = time() - 60*60*24;
			$from = date('Y-m-d', $yesterday);
			$to = date('Y-m-d', $yesterday);
		}
		if (Tools::isSubmit('submitDateMonth'))
		{
			$submitted = true;
			$from = date('Y-m-01');
			$to = date('Y-m-t');
		}
		if (Tools::isSubmit('submitDateMonthPrev'))
		{
			$submitted = true;
			$m = (date('m') == 1 ? 12 : date('m') - 1);
			$y = ($m == 12 ? date('Y') - 1 : date('Y'));
			$from = $y.'-'.$m.'-01';
			$to = $y.'-'.$m.date('-t', mktime(12, 0, 0, $m, 15, $y));
		}
		if (Tools::isSubmit('submitDateYear'))
		{
			$submitted = true;
			$from = date('Y-01-01');
			$to = date('Y-12-31');
		}
		if (Tools::isSubmit('submitDateYearPrev'))
		{
			$submitted = true;
			$from = (date('Y') - 1).date('-01-01');
			$to = (date('Y') - 1).date('-12-31');
		}
		
		if (Tools::isSubmit('submitDatePickerCompare'))
		{
				
			$submittedCompare = true;
			if (!Validate::isDate($fromCompare = date("Y-m-d",strtotime(Tools::getValue('datepickerFromCompare')))) OR !Validate::isDate($toCompare = date("Y-m-d",strtotime(Tools::getValue('datepickerToCompare')))))
				$this->_errors[] = Tools::displayError('Date specified is invalid');
		}
		
		if (Tools::isSubmit('submitDateCompareSame'))
		{
			$submittedCompare = true;
			$fromCompare = date("Y-m-d",strtotime(Tools::getValue('datepickerFrom').' -1 year'));
			$toCompare = date("Y-m-d",strtotime(Tools::getValue('datepickerTo').' -1 year'));
			$cookie->stats_excel_from_compare = date("d-m-Y",strtotime(Tools::getValue('datepickerFrom').' -1 year'));
			$cookie->stats_excel_to_compare = date("d-m-Y",strtotime(Tools::getValue('datepickerTo').' -1 year'));
			
			$employee->stats_date_from_compare = date("d-m-Y",strtotime(Tools::getValue('datepickerFrom').' -1 year'));
			
			$employee->stats_date_to_compare = date("d-m-Y",strtotime(Tools::getValue('datepickerTo').' -1 year'));
		}
		
		if ($submittedCompare == true AND isset($fromCompare) AND isset($toCompare) AND !sizeof($this->_errors))
		{
			$employee = new Employee($cookie->id_employee);
			$from = date("Y-m-d",strtotime(Tools::getValue('datepickerFrom')));
			$to = date("Y-m-d",strtotime(Tools::getValue('datepickerTo')));
			$cookie->stats_excel_from = $from;
			$cookie->stats_excel_to = $to;
			$employee->stats_date_from = $from;
			$employee->stats_date_to = $to;
			
			$cookie->stats_excel_from_compare = $fromCompare;
			$cookie->stats_excel_to_compare = $toCompare;
			
			$employee->stats_date_from_compare = $fromCompare;
			
			$employee->stats_date_to_compare = $toCompare;
			$employee->update();
			//$submitted = true;
		}
		
		if ($submitted == true AND isset($from) AND isset($to) AND !sizeof($this->_errors))
		{
			
			$cookie->stats_excel_from = $from;
			$cookie->stats_excel_to = $to;
			$employee = new Employee($cookie->id_employee);
			$employee->stats_date_from = $from;
			$employee->stats_date_to = $to;
			$employee->stats_date_from_compare = '';
			$employee->stats_date_to_compare = '';
			$employee->update();
			$submitted = true;
			//Tools::redirectAdmin($_SERVER['REQUEST_URI']);
		}
		else if(!$cookie->stats_excel_from)
		{
			$employee = new Employee($cookie->id_employee);
			$employee->stats_date_from = date('Y-m-01');
			$employee->stats_date_to = date('Y-m-t');
			$employee->update();
		}
		if (Tools::getValue('submitSettings'))
		{
		 	if ($this->tabAccess['edit'] === '1')
			{
				$currentIndex .= '&module='.Tools::getValue('module');
				$this->_postConfig($this->_fieldsSettings);
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
		}
		
		if (sizeof($this->_errors))
			AdminTab::displayErrors();
	}
	
	protected function displayEngines()
	{
		global $currentIndex, $cookie;
		
		$graphEngine = Configuration::get('PS_STATS_RENDER');
		$gridEngine = Configuration::get('PS_STATS_GRID_RENDER');
		$arrayGraphEngines = ModuleGraphEngine::getGraphEngines();
		$arrayGridEngines = ModuleGridEngine::getGridEngines();
		/*
		echo '
		<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post">
			<fieldset style="width: 200px;"><legend><img src="../img/admin/tab-preferences.gif" />'.$this->l('Settings', 'AdminStatsTab').'</legend>';
		echo '<p><strong>'.$this->l('Graph engine', 'AdminStatsTab').' </strong><br />';
		if (sizeof($arrayGraphEngines))
		{
			echo '<select name="PS_STATS_RENDER">';
			foreach ($arrayGraphEngines as $k => $value)
				echo '<option value="'.$k.'"'.($k == $graphEngine ? ' selected="selected"' : '').'>'.$value[0].'</option>';
			echo '</select><p>';
		}
		else
			echo $this->l('No graph engine module installed', 'AdminStatsTab');
		echo '<p><strong>'.$this->l('Grid engine', 'AdminStatsTab').' </strong><br />';
		if (sizeof($arrayGridEngines))
		{
			echo '<select name="PS_STATS_GRID_RENDER">';
			foreach ($arrayGridEngines as $k => $value)
				echo '<option value="'.$k.'"'.($k == $gridEngine ? ' selected="selected"' : '').'>'.$value[0].'</option>';
			echo '</select></p>';
		}
		else
			echo $this->l('No grid engine module installed', 'AdminStatsTab');
		echo '<p><input type="submit" value="'.$this->l('   Save   ', 'AdminStatsTab').'" name="submitSettings" class="button" /></p>
			</fieldset>
		</form><div class="clear space">&nbsp;</div>';*/
	}
	
	protected function getDate()
	{
		global $cookie;
		$year = isset($cookie->stats_year) ? $cookie->stats_year : date('Y');
		$month = isset($cookie->stats_month) ? sprintf('%02d', $cookie->stats_month) : '%';
		$day = isset($cookie->stats_day) ? sprintf('%02d', $cookie->stats_day) : '%';
		return $year.'-'.$month.'-'.$day;
	}
	
	public function displayCalendar()
	{
		echo '<div id="calendar">
		'.self::displayCalendarStatic(array(
			'Calendar' => $this->l('Calendar', 'AdminStatsTab'), 'Day' => $this->l('Day', 'AdminStatsTab'), 
			'Month' => $this->l('Month', 'AdminStatsTab'), 'Year' => $this->l('Year', 'AdminStatsTab'),
			'From' => $this->l('From:', 'AdminStatsTab'), 'To' => $this->l('To:', 'AdminStatsTab'), 'Save' => $this->l('Save', 'AdminStatsTab')
		)).'
		<script type="text/javascript">
			// When the user scrolls the page, execute myFunction
			window.onscroll = function() {myFunction()};

			// Get the header
			var header = document.getElementById("calendar");

			// Get the offset position of the navbar
			var sticky = header.offsetTop;

			// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
			function myFunction() {
			  if (window.pageYOffset > sticky) {
				header.classList.add("sticky-stats");
			  } else {
				header.classList.remove("sticky-stats");
			  }
			} 
		</script>
		
		<div class="clear space">&nbsp;</div></div>';
	}
	
	public static function displayCalendarStatic($translations)
	{
	
		global $cookie;
		$employee = new Employee($cookie->id_employee);
		
		if (!isset($cookie->stats_granularity))
			$cookie->stats_granularity = 10;
		if (Tools::isSubmit('submitIdZone'))
			$cookie->stats_id_zone = (int)Tools::getValue('stats_id_zone');
		if (Tools::isSubmit('submitGranularity'))
			$cookie->stats_granularity = Tools::getValue('stats_granularity');
		

		includeDatepicker3(array('datepickerFrom', 'datepickerTo', 'datepickerFromCompare', 'datepickerToCompare'));
		return '
		<fieldset style=" font-size:13px;"><legend><img src="../img/admin/date.png" /> '.$translations['Calendar'].'</legend>
			<div>
				<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post">
				
					<input type="hidden" name="submitGranularity" value="1" />
					<!-- Modalit&agrave;: <select name="stats_granularity" onchange=" // this.form.submit();" style="width:100px">
						<option value="10">Giorno</option>
						<option value="7" '.($cookie->stats_granularity == '7' ? 'selected="selected"' : '').'>Mese</option> -->
						<!-- <option value="4" '.($cookie->stats_granularity == '4' ? 'selected="selected"' : '').'>Anno</option> -->
					<!-- </select> -->
					
					<strong>Modalit&agrave:</strong> 
					<input type="radio" name="stats_granularity" value="10" '.($cookie->stats_granularity == '10' ? 'checked="checked"' : '').' /> Giorno
					<input type="radio" name="stats_granularity" value="7" '.($cookie->stats_granularity == '7' ? 'checked="checked"' : '').' /> Mese
				<br /><br />
					<div style="float:left; width:390px">
						<input type="submit" name="submitDateDay" class="button" value="'.$translations['Day'].'">
						<input type="submit" name="submitDateMonth" class="button" value="'.$translations['Month'].'">
						<input type="submit" name="submitDateYear" class="button" value="'.$translations['Year'].'">
						<input type="submit" name="submitDateDayPrev" class="button" value="'.$translations['Day'].'-1">
						<input type="submit" name="submitDateMonthPrev" class="button" value="'.$translations['Month'].'-1">
						<input type="submit" name="submitDateYearPrev" class="button" value="'.$translations['Year'].'-1">
					</div>
					<div style="float:left">
						<span style="height: 15px;  width: 15px;  background-color: #ed7e17;  border-radius: 50%;  display: inline-block;"></span>'.(isset($translations['From']) ? $translations['From'] : 'From:').' <input type="text" style="width:90px" name="datepickerFrom" id="datepickerFrom" value="'.date("d-m-Y",strtotime($employee->stats_date_from)).'">
						'.(isset($translations['To']) ? $translations['To'] : 'To:').' <input type="text" style="width:90px" name="datepickerTo" id="datepickerTo" value="'.date("d-m-Y",strtotime($employee->stats_date_to)).'">
						<input style="width:120px" type="submit" name="submitDatePicker" class="button" value="'.(isset($translations['Save']) ? $translations['Save'] : '   Save   ').'" />
					</div><div style="clear:both"></div>
					<br />
					<div style="float:left; width:390px">
						<strong>Confronta con periodo:</strong>
						<input type="submit" style="width:200px; margin-left:20px" name="submitDateCompareSame" class="button" value="Stesso periodo anno precedente" />
					</div>
					<div style="float:left">
						<span style="height: 15px;  width: 15px;  background-color: #058dc7;  border-radius: 50%;  display: inline-block;"></span>'.(isset($translations['From']) ? $translations['From'] : 'From:').' <input type="text" style="width:90px" name="datepickerFromCompare" id="datepickerFromCompare" value="'.($employee->stats_date_from_compare != '' ? date("d-m-Y",strtotime($employee->stats_date_from_compare)) : date("01-m-Y")).'">
						'.(isset($translations['To']) ? $translations['To'] : 'To:').' <input type="text" style="width:90px" name="datepickerToCompare" id="datepickerToCompare" value="'.($employee->stats_date_to_compare != '' ? date("d-m-Y",strtotime($employee->stats_date_to_compare)) : date("t-m-Y")).'">
						<input type="submit" style="width:120px" name="submitDatePickerCompare" class="button" value="'.(isset($translations['Confronta']) ? $translations['Confronta'] : '   Confronta   ').'" />
					</div>
					
					
					<div style="clear:both"></div>
				</form>
				
				
			</div>
			<br />
			<form id="granularity" action="#granularity" method="post" >
				
				
				
			</form>
		</fieldset>';
	}
	
	public function displaySearch()
	{
		return;
		echo '
		<fieldset style="margin-top:20px; width: 200px;"><legend><img src="../img/admin/binoculars.png" /> '.$this->l('Search', 'AdminStatsTab').'</legend>
			<input type="text" /> <input type="button" class="button" value="'.$this->l('Go', 'AdminStatsTab').'" />
		</fieldset>';
	}
	
	private function getModules()
	{
		return Db::getInstance()->ExecuteS('
		SELECT h.`name` AS hook, m.`name`
		FROM `'._DB_PREFIX_.'module` m
		LEFT JOIN `'._DB_PREFIX_.'hook_module` hm ON hm.`id_module` = m.`id_module`
		LEFT JOIN `'._DB_PREFIX_.'hook` h ON hm.`id_hook` = h.`id_hook`
		WHERE h.`name` LIKE \'AdminStatsModules\'
		AND m.`active` = 1
		ORDER BY hm.`position`');
	}
	
	public function displayMenu()
	{
		global $currentIndex, $cookie;
		$modules = $this->getModules();

		echo '<fieldset style="width: 250px; margin-top: 15px;"><legend><img src="../img/admin/navigation.png" /> '.$this->l('Navigation', 'AdminStatsTab').'</legend>';
		if (sizeof($modules))
		{
			foreach ($modules AS $module)
				if ($moduleInstance = Module::getInstanceByName($module['name']))
					echo '<li style="list-style-type:none; margin-bottom:1%;"><img style="margin-right:2%" src="../modules/'.$module['name'].'/logo.gif" /><a href="'.$currentIndex.'&token='.Tools::getValue('token').'&module='.$module['name'].'">'.$moduleInstance->displayName.'</a></li>';
		}
		else
			echo $this->l('No module installed', 'AdminStatsTab');
		echo '</fieldset><div class="clear space">&nbsp;</div>';
	}
	
	public function display()
	{
		
		echo '<div>';
		$this->displayCalendar();
		
		if (!($moduleName = Tools::getValue('module')) AND $moduleInstance = Module::getInstanceByName('statsforecast') AND $moduleInstance->active)
			$moduleName = 'statsforecast';
		if ($moduleName)
		{
			// Needed for the graphics display when this is the default module
			$_GET['module'] = $moduleName;
			if (!isset($moduleInstance))
				$moduleInstance = Module::getInstanceByName($moduleName);
			if ($moduleInstance AND $moduleInstance->active)
				echo Module::hookExec('AdminStatsModules', NULL, $moduleInstance->id);
			else
				echo $this->l('Module not found', 'AdminStatsTab');
		}
		else
			echo '<h3 class="space">'.$this->l('Please select a module in the left column.').'</h3>';
		echo '</div><div class="clear"></div>';
		
		$this->displayEngines();
		$this->displayMenu();
		$this->displaySearch();
	}
}


