<?php

	include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

	class AdminFornitori extends AdminPreferences {
	
	
		public function display()
		{
			global $currentIndex;
			
			if(Tools::getIsset('other_suppliers_2')) 
			{
				
				Db::getInstance()->executeS("UPDATE manufacturer SET supplier = '".Tools::getValue('id_supplier')."' WHERE id_manufacturer = ".Tools::getValue('costruttore-di-riferimento'));
				
				Db::getInstance()->executeS("UPDATE product SET id_supplier = '".Tools::getValue('id_supplier')."' WHERE id_manufacturer = ".Tools::getValue('costruttore-di-riferimento'));
				
				$suppliers = array();
				foreach ($_POST['other_suppliers'] as $supplier) {
					$suppliers[] = $supplier;

				}
				$suppliers = serialize($suppliers);
				
				Db::getInstance()->executeS("UPDATE manufacturer SET other_suppliers = '".$suppliers."' WHERE id_manufacturer = ".Tools::getValue('costruttore-di-riferimento'));
				
				Db::getInstance()->executeS("UPDATE product SET other_suppliers = '".$suppliers."' WHERE id_manufacturer = ".Tools::getValue('costruttore-di-riferimento'));
				
				Tools::redirectAdmin($currentIndex.'&conf=4&tabs=2&costruttore='.Tools::getValue('costruttore-di-riferimento').'&token='.($token ? $token : $this->token));
				
			
			
			}
			/*$products = Db::getInstance()->executeS('SELECT id_supplier s, id_manufacturer m FROM product GROUP BY id_manufacturer');
			foreach($products as $p)
				Db::getInstance()->executeS('UPDATE manufacturer SET supplier = '.$p['s'].' WHERE id_manufacturer = '.$p['m'].'');
				*/
			global $cookie;
			echo "<h1>Fornitori</h1>";
			
			if(!Tools::getIsset('costruttore-di-riferimento'))
				echo "Seleziona il costruttore:  <br /><br /> ";
			echo "<form name='selezione-costruttore' method='post'>";
			
			if(!Tools::getIsset('costruttore-di-riferimento'))
			{
				echo "<select name='costruttore-di-riferimento' style='height:25px' >";
				
				$costruttori = Db::getInstance()->executeS('SELECT * FROM manufacturer ORDER BY name ASC');
				
				foreach ($costruttori as $costruttore) 
				{
				
					echo "<option value='".$costruttore['id_manufacturer']."' "; 
					
					if(isset($_POST['costruttore-di-riferimento']) && $_POST['costruttore-di-riferimento'] == $costruttore['id_manufacturer']) { echo "selected='selected' "; } else { } 
					if(isset($_GET['costruttore']) && $_GET['costruttore'] == $costruttore['id_manufacturer']) { echo "selected='selected' "; } else { } 
					echo ">".$costruttore['name']."</option>";
				
				}
				
				echo "</select>";
			}
			else
			{
				echo "<input name='costruttore-di-riferimento' value='".Tools::getValue('costruttore-di-riferimento')."' type='hidden' />";
				echo "Seleziona i fornitori di: <strong>".Db::getInstance()->getValue('SELECT name FROM manufacturer WHERE id_manufacturer = '.Tools::getValue('costruttore-di-riferimento'))."</strong>";
			
			}
			
			if(Tools::getIsset('costruttore-di-riferimento'))
			{
				$resultssup = Db::getInstance()->ExecuteS("SELECT id_supplier, name FROM supplier ORDER BY name");
				
				$id_supplier = Db::getInstance()->getValue('SELECT supplier FROM manufacturer WHERE id_manufacturer = '.Tools::getValue('costruttore-di-riferimento'));
							
				echo '<br /><br /><table class="table">';
				echo '<tr>
						<td style="vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;">'.$this->l('Fornitore principale:').'</td>
						<td style="padding-bottom:5px;">
							<select name="id_supplier" id="id_supplier"><option name="0" value="0">-- Scegli (opzionale) --</option>';
							
							foreach ($resultssup as $rowsup) {
							
								echo "<option name='$rowsup[id_supplier]' ".($rowsup['id_supplier'] == $id_supplier ? "selected='selected'" : '')." value='$rowsup[id_supplier]'"; 
								echo ">$rowsup[name]</option>";
							
							}
							
						echo '
							</select>&nbsp;&nbsp;&nbsp;<a href="?tab=AdminSuppliers&addsupplier&token='.Tools::getAdminToken('AdminSuppliers'.(int)(Tab::getIdFromClassName('AdminSuppliers')).(int)($cookie->id_employee)).'" target="_blank"><img src="../img/admin/add.gif" alt="'.$this->l('Crea nuovo fornitore').'" title="'.$this->l('Crea nuovo fornitore').'" /> <b>'.$this->l('Crea nuovo fornitore').'</b></a>
						</td>
					</tr>';
					
					echo '<tr>
						<td style="vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;">'.$this->l('Altri fornitori:').'</td>
						<td style="padding-bottom:5px;">
						<script type="text/javascript">
						function add_riga() {
				
							$("<div style=\'position:relative; width:145px; float:left\'><select name=\'other_suppliers[]\' style=\'width:145px\'><option name=\'0\' value=\'0\'>-- Scegli (opzionale) --</option>';
							
							foreach ($resultssup as $rowsup) {
				
								echo "<option name='$rowsup[id_supplier]' value='$rowsup[id_supplier]'"; 
								echo ">$rowsup[name]</option>";
							
							}
							
							echo '</select></div>").appendTo("#selezione-fornitori");

						}
						
						function rimuovi_riga() {
							$("#selezione-fornitori").children().last().remove();
						}
			
						</script>
						<div id="selezione-fornitori" style="position:relative; width:300px">';
						
						$other_suppliers = Db::getInstance()->getValue('SELECT other_suppliers FROM manufacturer WHERE id_manufacturer = '. Tools::getValue('costruttore-di-riferimento').'');
					
						$other_suppliers = unserialize($other_suppliers);
						
						foreach ($other_suppliers as $supplier)
						{
						
							echo '
							<div style="position:relative; width:145px; float:left"><select name="other_suppliers[]" style="width:145px">
							<option name="0" value="0">-- Scegli (opzionale) --</option>
							';
							
							
							
							foreach ($resultssup as $rowsup) {
							
								echo "<option name='$rowsup[id_supplier]' ".($rowsup['id_supplier'] == $supplier ? "selected='selected'" : '')." value='$rowsup[id_supplier]'"; 
								echo ">$rowsup[name]</option>";
							
							}
				
					
							echo '</select>';
							echo '</div>';
				
						}
				echo '</div><div style="clear:both"></div>
				
				
				<a href="javascript:void(0)" onclick="javascript:add_riga(); return false;"><img src="../img/admin/add.gif" alt="'.$this->l('Add').'" title="'.$this->l('Add').'" /> <b>'.$this->l('Aggiungi forn.').'</b></a><br />
				<a href="javascript:void(0)" onclick="javascript:rimuovi_riga(); return false;"><img src="../img/admin/forbbiden.gif" alt="'.$this->l('Remove').'" title="'.$this->l('Remove').'" /> <b>'.$this->l('Rimuovi forn.').'</b></a>
				<input type="hidden" name="other_suppliers_2" id="other_suppliers_2" /><!-- serve per ordinamento --><br /><br />
				
						</td>
					</tr></table>';
			
			
			
			
			}
			
			if(Tools::getIsset('costruttore-di-riferimento'))
				echo '<br />';
				
			
			echo "<input type='submit' name='seleziona-costruttore' value='Vai' class='button' style='margin-top:-4px; margin-left:5px; cursor:pointer' />";
			
			if(Tools::getIsset('costruttore-di-riferimento'))
				echo '&nbsp;&nbsp;&nbsp;<a href="'.$currentIndex.'&token='.($token ? $token : $this->token).'" class="button" style="height:23px">Torna alla selezione del costruttore</a>';
				
			echo "</form>";
		
			
		}

	}
	
	