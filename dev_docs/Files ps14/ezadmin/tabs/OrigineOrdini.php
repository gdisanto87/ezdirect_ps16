<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14632 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class OrigineOrdini extends AdminTab
{
		
	public function display()
	{	
		global $cookie, $currentIndex;
		
		//echo "WORK IN PROGRESS..."; die();
		
		if(Tools::getIsset('pagination') && !Tools::getIsset('submitResetOrigineOrdini')) 
			$cookie->{'origineordini_pagination'} = Tools::getValue('pagination');
			
		if(Tools::getIsset('id_order')) 
			$cookie->{'id_order'} = Tools::getValue('id_order');
			
		if(Tools::getIsset('data_da')) 
			$cookie->{'data_da'} = Tools::getValue('data_da');
			
		if(Tools::getIsset('data_a')) 
			$cookie->{'data_a'} = Tools::getValue('data_a');
			
		if(Tools::getIsset('origine')) 
			$cookie->{'origine'} = Tools::getValue('origine');	
		
		if(Tools::getIsset('origine_primo')) 
			$cookie->{'origine_primo'} = Tools::getValue('origine_primo');
			
			
		if(Tools::getIsset('submitResetOrigineOrdini')) 
		{
			$cookie->{'origineordini_pagination'} = 50;
			$cookie->{'id_order'} = '';
			$cookie->{'data_da'} = '';
			$cookie->{'data_a'} = '';
			$cookie->{'origine'} = '';
			$cookie->{'origine_primo'} = '';
		}
			
		if(isset($cookie->{'origineordini_pagination'})) 
			$pagination_limit = $cookie->{'origineordini_pagination'};
		else
			$pagination_limit = 50;

		if(Tools::getIsset('submitFilterOrigineOrdini')) 
			$start = (Tools::getValue('submitFilterOrigineOrdini')*$pagination_limit)-$pagination_limit;
		else
			$start = 0;
			
		$where = '';

		if(isset($cookie->{'id_order'})) 
		{
			if(empty($cookie->{'id_order'}))
				$where .= 'o.id_order > 0 AND ';
			else
				$where .= 'o.id_order = '.$cookie->{'id_order'}.' AND ';
		}

		if(isset($cookie->{'data_da'})) 
		{
			if(empty($cookie->{'data_da'}))
				$where .= 'o.date_add > "0000-00-00 00:00:00" AND ';
			else {
				$array_data_da = explode("-",$cookie->{'data_da'});
				$where .= 'o.date_add >= "'.$array_data_da[2]."-".$array_data_da[1]."-".$array_data_da[0].' 00:00:00" AND ';
			}
		}	

		if(isset($cookie->{'data_a'})) 
		{
			if(empty($cookie->{'data_a'}))
				$where .= 'o.date_add > "0000-00-00 00:00:00" AND ';
			else {
				$array_data_a = explode("-",$cookie->{'data_a'});
				$where .= 'o.date_add <= "'.$array_data_a[2]."-".$array_data_a[1]."-".$array_data_a[0].' 23:59:59" AND ';
			}
		}			
	
		if(isset($cookie->{'origine'})) 
		{
			
				
			switch($cookie->{'origine'}) 
			{

				case 'Diretto': if($origine_ordine == 'Diretto') { $where.= 'id_order = '.$order['id_order'].' OR '; } break;
				case 'CPC': if($origine_ordine == 'Google CPC') { $where.= 'id_order = '.$order['id_order'].' OR '; } break;
				case 'Trovaprezzi': 
				
						break;
				case 'Pangora': if(preg_match('/pangora/', $origine_ordine)) { $where.= 'id_order = '.$order['id_order'].' OR '; } break;
				case 'Kelkoo': if(preg_match('/kelkoo/', $origine_ordine)) { $where.= 'id_order = '.$order['id_order'].' OR '; } break;
				case 'organico': if($origine_ordine == 'google.it') { $where.= 'id_order = '.$order['id_order'].' OR '; } break;
				case 'Altro': $where_origine = 'WHERE cos.http_referer != "" OR cos.http_referer NOT LIKE "%google%" OR cos.http_referer NOT LIKE "%trovaprezzi%" OR cos.http_referer NOT LIKE "%pangora%" OR cos.http_referer NOT LIKE "%kelkoo%") OR '; $where .= $origine_join.$where_origine; break;
				case 'Sconosciuto': if($origine_ordine == '*Sconosciuto*') { $where.= 'id_order = '.$order['id_order'].' OR '; } break;
				case '' : $where.= '';
					
					
				
			}
		}
	
		/*if(isset($cookie->{'origine'})) 
		{
			
				
			switch($cookie->{'origine'}) 
			{

				case 'Diretto': if($origine_ordine == 'Diretto') { $where.= 'id_order = '.$order['id_order'].' OR '; } break;
				case 'CPC': if($origine_ordine == 'Google CPC') { $where.= 'id_order = '.$order['id_order'].' OR '; } break;
				case 'Trovaprezzi': 
					$where .= "("; $i = 0;
					for($j=$start; $j<$pagination_limit; $j++) {
						$ord = Db::getInstance()->executeS('SELECT o.id_order AS ordine FROM orders o ORDER BY o.id_order DESC LIMIT '.$i.', 1');
						foreach($ord as $o) {
						
							$source = $this->getThisOrderSources($o['ordine']);
							if(preg_match('/trovaprezzi/', $source['http_referer'])) {
								$where.= 'o.id_order = '.$o['ordine'].' OR ';
							}
							else {
								$j--;
							}
						}
						$i++;
					}
					$where .= "o.id_order = 0) AND";
						break;
				case 'Pangora': if(preg_match('/pangora/', $origine_ordine)) { $where.= 'id_order = '.$order['id_order'].' OR '; } break;
				case 'Kelkoo': if(preg_match('/kelkoo/', $origine_ordine)) { $where.= 'id_order = '.$order['id_order'].' OR '; } break;
				case 'organico': if($origine_ordine == 'google.it') { $where.= 'id_order = '.$order['id_order'].' OR '; } break;
				case 'Altro': $where_origine = 'WHERE cos.http_referer != "" OR cos.http_referer NOT LIKE "%google%" OR cos.http_referer NOT LIKE "%trovaprezzi%" OR cos.http_referer NOT LIKE "%pangora%" OR cos.http_referer NOT LIKE "%kelkoo%") OR '; $where .= $origine_join.$where_origine; break;
				case 'Sconosciuto': if($origine_ordine == '*Sconosciuto*') { $where.= 'id_order = '.$order['id_order'].' OR '; } break;
				case '' : $where.= '';
					
					
				
			}
		}*/
		
		$query = 'SELECT DISTINCT * FROM orders o WHERE '.$where.' o.id_customer > 0 GROUP BY o.id_order ORDER BY o.id_order DESC LIMIT '.$start.', '.$pagination_limit.'';
		$orders = Db::getInstance()->executeS($query);
		
		$tokenOrders = Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).(int)($cookie->id_employee));
		$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee));
		
		
		
		
		echo "<form method='post' action='".$currentIndex."&token=".$this->token."'>
		";
		
		echo "<input type='hidden' name='querysql' value='".(isset($_POST['querysql']) ? $_POST['querysql'] : $query)."' />	";
		$this->displayListPagination();
		
		$this->includeDatepickerZ(array('data_da', 'data_a'), true);
		
		echo "<table class='table'><thead>
			<tr><th>ID ordine</th><th>Data ordine</th><th>Origine questo ordine</th><th>Origine primo ordine</th><th>Vai a ordine</th></tr>
			</thead>
			<tbody>
			<tr><td><input type='text' name='id_order' id='id_order' class='input' value='".(isset($cookie->{'id_order'}) ? $cookie->{'id_order'} : '')."' /></td>
			<td>Dal <input type='text' name='data_da' id='data_da' value='".(isset($cookie->{'data_da'}) ? $cookie->{'data_da'} : '')."' /><br />
			al <input type='text' name='data_a' id='data_a' value='".(isset($cookie->{'data_a'}) ? $cookie->{'data_a'} : '')."' /></td>
			<td>
			";
			
			/*
			<select name='origine' id='origine'>
			
			<option value='' ".(isset($cookie->{'origine'}) && $cookie->{'origine'} == '' ? "selected='selected'" : '').">-- Scegli --</option>
			<option value='Diretto' ".(isset($cookie->{'origine'}) && $cookie->{'origine'} == 'Diretto' ? "selected='selected'" : '').">Diretto</option>
			<option value='CPC' ".(isset($cookie->{'origine'}) && $cookie->{'origine'} == 'CPC' ? "selected='selected'" : '').">Google CPC</option>
			<option value='organico' ".(isset($cookie->{'origine'}) && $cookie->{'origine'} == 'organico' ? "selected='selected'" : '').">Google organico</option>
			<option value='Trovaprezzi' ".(isset($cookie->{'origine'}) && $cookie->{'origine'} == 'Trovaprezzi' ? "selected='selected'" : '').">Trovaprezzi</option>
			<option value='Kelkoo' ".(isset($cookie->{'origine'}) && $cookie->{'origine'} == 'Kelkoo' ? "selected='selected'" : '').">Kelkoo</option>
			<option value='Pangora' ".(isset($cookie->{'origine'}) && $cookie->{'origine'} == 'Pangora' ? "selected='selected'" : '').">Pangora</option>
			<option value='Altro' ".(isset($cookie->{'origine'}) && $cookie->{'origine'} == 'Altro' ? "selected='selected'" : '').">Altro</option>
			<option value='Sconosciuto' ".(isset($cookie->{'origine'}) && $cookie->{'origine'} == 'Sconosciuto' ? "selected='selected'" : '').">*Sconosciuto*</option>
			</select>*/ 
			echo "
			</td>
			<td>"; /*<select name='origine_primo' id='origine_primo'>
			<option value='' ".(isset($cookie->{'origine_primo'}) && $cookie->{'origine_primo'} == '' ? "selected='selected'" : '').">-- Scegli --</option>
			<option value='Diretto' ".(isset($cookie->{'origine_primo'}) && $cookie->{'origine_primo'} == 'Diretto' ? "selected='selected'" : '').">Diretto</option>
			<option value='CPC' ".(isset($cookie->{'origine_primo'}) && $cookie->{'origine_primo'} == 'CPC' ? "selected='selected'" : '').">Google CPC</option>
			<option value='organico' ".(isset($cookie->{'origine_primo'}) && $cookie->{'origine_primo'} == 'organico' ? "selected='selected'" : '').">Google organico</option>
			<option value='Trovaprezzi' ".(isset($cookie->{'origine_primo'}) && $cookie->{'origine_primo'} == 'Trovaprezzi' ? "selected='selected'" : '').">Trovaprezzi</option>
			<option value='Kelkoo' ".(isset($cookie->{'origine_primo'}) && $cookie->{'origine_primo'} == 'Kelkoo' ? "selected='selected'" : '').">Kelkoo</option>
			<option value='Pangora' ".(isset($cookie->{'origine_primo'}) && $cookie->{'origine_primo'} == 'Pangora' ? "selected='selected'" : '').">Pangora</option>
			<option value='Altro' ".(isset($cookie->{'origine_primo'}) && $cookie->{'origine_primo'} == 'Altro' ? "selected='selected'" : '').">Altro</option>
			<option value='Sconosciuto' ".(isset($cookie->{'origine_primo'}) && $cookie->{'origine_primo'} == 'Sconosciuto' ? "selected='selected'" : '').">*Sconosciuto*</option>
			</select>*/ echo "
			</td>
			<td></td>
			</tr>
			
			</form>";
			
			
		foreach ($orders as $order) {
		
			$source = $this->getThisOrderSources($order['id_order']);
			if(empty($source))
				$origine_ordine = '*Sconosciuto*';
			else 
			{
				$origine_ordine = preg_replace('/^www./', '', parse_url($source['http_referer'], PHP_URL_HOST));
				if($origine_ordine == '')
					$origine_ordine = 'Diretto';
				
				if(preg_match('/\&/', $source['keywords']) && preg_match('/google/', $source['http_referer'])) 
					$origine_ordine = 'Google CPC';
				//provare con pregmatch ampersand in keyword per distinguere adwords da organico... 
				
				if(preg_match('/googleads/', $source['http_referer'])) 
					$origine_ordine = 'Google CPC';
			}
			
			$primo_ordine = $this->getFirstOrderSources($order['id_order']);
			
			
			if(empty($primo_ordine))
				$origine_primo_ordine = '*Sconosciuto*';
			else 
			{
				$origine_primo_ordine = preg_replace('/^www./', '', parse_url($primo_ordine['http_referer'], PHP_URL_HOST));
				if($origine_primo_ordine == '')
					$origine_primo_ordine = 'Diretto';
					
				if(preg_match('/\&/', $primo_ordine['keywords']) && preg_match('/google/', $primo_ordine['http_referer'])) 
					$origine_primo_ordine = 'Google CPC';
				//provare con pregmatch ampersand in keyword per distinguere adwords da organico... 
				
				if(preg_match('/googleads/', $primo_ordine['http_referer'])) 
					$origine_primo_ordine = 'Google CPC';	
			}
			
			
			
			echo "<tr><td>".$order['id_order']."</td>
			<td>".Tools::displayDate($order['date_add'], $cookie->id_lang)."</td>
			<td>".$origine_ordine."</td>
			<td>".$origine_primo_ordine."</td>
			<td><a href='index.php?tab=AdminCustomers&id_customer=".$order['id_customer']."&viewcustomer&vieworder&id_order=".$order['id_order']."&token=".$tokenCustomers."&tab-container-1=4'><img src='../img/admin/details.gif' alt='Vedi' title='Vedi' /></a></td></tr>";
		
		
		}
		
		
		
		echo "</tbody></table>";
		
		
		
		if(isset($_POST['esportaexcel'])) { 
		
			ini_set("memory_limit","892M");
			set_time_limit(3600);
			require_once 'esportazione-catalogo/Classes/PHPExcel.php';
			$objPHPExcel = new PHPExcel();
			
			$results = Db::getInstance()->ExecuteS(stripslashes($_POST['querysql']));
			
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'ORIGINE ORDINI')
				->setCellValue('A6', 'N.')
				->setCellValue('B6', 'Id ordine')
				->setCellValue('C6', 'Data ordine')
				->setCellValue('D6', 'Origine questo ordine')
				->setCellValue('E6', 'Origine primo ordine')
				;
			$k = 7;
			$i = 1;
			
			$objPHPExcel->getActiveSheet()->getStyle("A")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
		
		
			foreach ($results as $row) {
			
				$source = $this->getThisOrderSources($row['id_order']);
				if(empty($source))
					$origine_ordine = '*Sconosciuto*';
				else 
				{
					$origine_ordine = preg_replace('/^www./', '', parse_url($source['http_referer'], PHP_URL_HOST));
					if($origine_ordine == '')
						$origine_ordine = 'Diretto';
					
					if(preg_match('/\&/', $source['keywords']) && preg_match('/google/', $source['http_referer'])) 
						$origine_ordine = 'Google CPC';
					//provare con pregmatch ampersand in keyword per distinguere adwords da organico... 
					
					if(preg_match('/googleads/', $source['http_referer'])) 
						$origine_ordine = 'Google CPC';
				}
				
				$primo_ordine = $this->getFirstOrderSources($row['id_order']);
				
				
				if(empty($primo_ordine))
					$origine_primo_ordine = '*Sconosciuto*';
				else 
				{
					$origine_primo_ordine = preg_replace('/^www./', '', parse_url($primo_ordine['http_referer'], PHP_URL_HOST));
					if($origine_primo_ordine == '')
						$origine_primo_ordine = 'Diretto';
						
					if(preg_match('/\&/', $primo_ordine['keywords']) && preg_match('/google/', $primo_ordine['http_referer'])) 
						$origine_primo_ordine = 'Google CPC';
					//provare con pregmatch ampersand in keyword per distinguere adwords da organico... 
					
					if(preg_match('/googleads/', $primo_ordine['http_referer'])) 
						$origine_primo_ordine = 'Google CPC';	
				}
		
				$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue("A$k", $i)
						->setCellValue("B$k", $row['id_order'])
						->setCellValue("C$k", Tools::displayDate($row['date_add'], $cookie->id_lang))
						->setCellValue("D$k", $origine_ordine)
						->setCellValue("E$k", $origine_primo_ordine)
				;
				
				$i++;
				$k++;
				
			}
			
			$objPHPExcel->getActiveSheet()->setTitle('Origine ordini');

			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	

			$objPHPExcel->getActiveSheet()->getStyle('A6:E6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
				
			$objPHPExcel->getActiveSheet()->getStyle('A6:E6')->getFill()->getStartColor()->setRGB('FFFF00');	

			$objPHPExcel->getActiveSheet()->getStyle("A6:E$k")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A6:E6')->getFont()->setBold(true);

			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
			
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);

			$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
			$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
			PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

			$data = date("Ymd");

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->setPreCalculateFormulas(false);
			$objWriter->save(str_replace(".php", ".xls", "esportazione-catalogo/catalogo_xls/origine-ordini-$data.php"));

			$this->_html .= "<div style='visibility:hidden'><br /><br />FILE PRONTO PER IL DOWNLOAD: <a id='scaricaexcelstatistiche' href='http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/origine-ordini-$data.xls'>CLICCA QUI PER SCARICARE</a>!</div>";

			header("Location: http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/origine-ordini-$data.xls");
		
		
		}
	}
		
	public static function getThisOrderSources($id_order)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
		SELECT cos.http_referer, cos.request_uri, cos.keywords, cos.date_add
		FROM '._DB_PREFIX_.'orders o
		INNER JOIN '._DB_PREFIX_.'guest g ON g.id_customer = o.id_customer
		INNER JOIN '._DB_PREFIX_.'connections co  ON co.id_guest = g.id_guest
		INNER JOIN '._DB_PREFIX_.'connections_source cos ON cos.id_connections = co.id_connections
		WHERE id_order = '.(int)($id_order).'
		AND cos.date_add >= DATE_SUB(o.date_add, INTERVAL 1 WEEK)
		ORDER BY cos.date_add ASC');
	}
	
	public static function getFirstOrderSources($id_order)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
		SELECT cos.http_referer, cos.request_uri, cos.keywords, cos.date_add
		FROM '._DB_PREFIX_.'orders o
		INNER JOIN '._DB_PREFIX_.'guest g ON g.id_customer = o.id_customer
		INNER JOIN '._DB_PREFIX_.'connections co  ON co.id_guest = g.id_guest
		INNER JOIN '._DB_PREFIX_.'connections_source cos ON cos.id_connections = co.id_connections
		WHERE id_order = '.(int)($id_order).'
		ORDER BY cos.date_add ASC');
	}
	
	public function displayListPagination($token = NULL)
	{
		global $currentIndex, $cookie;
		$isCms = false;
		if (preg_match('/cms/Ui', $this->identifier))
			$isCms = true;
		$id_cat = Tools::getValue('id_'.($isCms ? 'cms_' : '').'category');

		if (!isset($token) OR empty($token))
			$token = $this->token;

		$this->_listTotal = Db::getInstance()->getValue("SELECT count(id_order) FROM orders ORDER BY id_order DESC");
		
		/* Determine total page number */
		$totalPages = ceil($this->_listTotal / (isset($cookie->{'origineordini_pagination'}) ? $cookie->{'origineordini_pagination'} : 50));
		if (!$totalPages) $totalPages = 1;
		

		echo '<a name="'.$this->table.'">&nbsp;</a>';
		echo '
		<table>
			<tr>
				<td style="vertical-align: bottom;">
					<span style="float: left;">';

		/* Determine current page number */
		$page = (int)(Tools::getValue('submitFilterOrigineOrdini'));
		if (!$page) $page = 1;
		if ($page > 1)
			echo '
						<input type="image" src="../img/admin/list-prev2.gif" onclick="getE(\'submitFilterOrigineOrdini\').value=1"/>
						&nbsp; <input type="image" src="../img/admin/list-prev.gif" onclick="getE(\'submitFilterOrigineOrdini\').value='.($page - 1).'"/> ';
		echo $this->l('Page').' <b>'.$page.'</b> / '.$totalPages;
		if ($page < $totalPages)
			echo '
						<input type="image" src="../img/admin/list-next.gif" onclick="getE(\'submitFilterOrigineOrdini\').value='.($page + 1).'"/>
						 &nbsp;<input type="image" src="../img/admin/list-next2.gif" onclick="getE(\'submitFilterOrigineOrdini\').value='.$totalPages.'"/>';
		echo '			| '.$this->l('Display').'
						<select name="pagination">';
		/* Choose number of results per page */
		$selectedPagination = Tools::getValue('pagination', (isset($cookie->{$this->table.'_pagination'}) ? $cookie->{$this->table.'_pagination'} : NULL));
		foreach ($this->_pagination AS $value)
			echo '<option value="'.(int)($value).'"'.($selectedPagination == $value ? ' selected="selected"' : (($selectedPagination == NULL && $value == $this->_pagination[1]) ? ' selected="selected2"' : '')).'>'.(int)($value).'</option>';
		echo '
						</select>
						/ '.(int)($this->_listTotal).' '.$this->l('result(s)').'
					</span>
					<span style="float: right;">
						<input type="hidden" name="submitFilterOrigineOrdini" id="submitFilterOrigineOrdini" value="'.$page.'" />
						<input type="submit" name="submitResetOrigineOrdini" value="'.$this->l('Reset').'" class="button" />
						<input type="submit" id="submitFilterButton_OrigineOrdini" name="submitFilter" value="'.$this->l('Filter').'" class="button" />
						<input type="submit" value="Esporta i dati in formato Excel" name="esportaexcel" class="button" />						
						
					</span>
					<span class="clear"></span>
				</td>
			</tr></table>';
		
	
	}
	
	public function bindDatepickerZ($id, $time)
	{
		if ($time)
			echo '
			var dateObj = new Date();
			var hours = dateObj.getHours();
			var mins = dateObj.getMinutes();
			var secs = dateObj.getSeconds();
			if (hours < 10) { hours = "0" + hours; }
			if (mins < 10) { mins = "0" + mins; }
			if (secs < 10) { secs = "0" + secs; }
			var time = " "+hours+":"+mins+":"+secs;';

		echo '
		$(function() {
			$("#'.Tools::htmlentitiesUTF8($id).'").datepicker({
				prevText:"",
				nextText:"",
				dateFormat:"dd-mm-yy"});
		});';
	}

	// id can be a identifier or an array of identifiers
	public function includeDatepickerZ($id, $time = false)
	{
		global $cookie;
		echo '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery-ui-1.8.10.custom.min.js"></script>';
		$iso = Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'lang WHERE `id_lang` = '.(int)$cookie->id_lang);
		if ($iso != 'en')
			echo '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/datepicker/ui/i18n/ui.datepicker-'.Tools::htmlentitiesUTF8($iso).'.js"></script>';
		echo '<script type="text/javascript">';
			if (is_array($id))
				foreach ($id as $id2)
					$this->bindDatepickerZ($id2, $time);
			else
				$this->bindDatepickerZ($id, $time);
		echo '</script>';
	}
		
		
}

