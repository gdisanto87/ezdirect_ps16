<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

class CaricoMagazzino extends AdminPreferences
{


	public function display()
	{
		$modalita_esolver = Db::getInstance()->getValue('SELECT value FROM configuration WHERE name = "MODALITA_ESOLVER"');

		if($modalita_esolver == 1)
			die('Questa procedura non funziona in modalit&agrave; eSolver');

		if(isset($_POST['vai']) == 'Importa quantita') 
		{
			 if($_FILES['file_ordini_qta']['size'] != 0) 
			 {
				$path = $_SERVER["DOCUMENT_ROOT"];

				$tmp = $_FILES['file_ordini_qta']['tmp_name']; //cartella temporanea 
				$file_ordini_qta = $_FILES['file_ordini_qta']['name'];
				$dir = "$path"."/ezadmin/importazione-catalogo/catalogo_xls/"; //percorso della cartella
				$dirfile = $dir.$file_ordini_qta;
				move_uploaded_file($tmp, $dirfile);
			 }
			include("importazione-catalogo/import-quantita.php");
		}
		else 
		{

			$gia_importato = 0;
			
			if(is_file("importazione-catalogo/catalogo_xls/movimenti_magazzino-".date('d-m-Y').".xls"))
				$gia_importato = 1;

			echo "




			 <br />
			 <script type='text/javascript'>

				function message (t1, t2) {
					
					strong = document.createElement ('STRONG')
					strong.appendChild (document.createTextNode (t1))
					br = document.createElement ('BR')
					strong.appendChild (document.createTextNode (t2))
					document.getElementsByTagName ('BODY')[0].appendChild (strong)
				}

			</script>
			
				<h2>Movimenti magazzino</h2>
				<br /><br />
				Attraverso questo form puoi aggiornare i movimenti di magazzino. ATTENZIONE: questo file si pu&ograve; caricare soltanto una volta al giorno. Ulteriori caricamenti vengono bloccati. In caso di errori rivolgersi al tecnico.<br /><br />
				<form name='uploadfile' method='post' enctype='multipart/form-data'>
				
				".($gia_importato == 0 ? "<input type='file' name='file_ordini_qta' id='file_ordini_qta' style='width:400px; '/>
				
			<table>

				<tr>
				<td></td><td>
				<br /><br />
				<input type='submit' name='vai' value='Invia' class='button' onclick='var surec=window.confirm(\"Sei sicuro?\"); if (surec) { 
				
				var fu1 = document.getElementById(\"file_ordini_qta\").files[0].name; var ftype = fu1.split(\"-\"); ftype = ftype[0]; 
				if(fu1 != \"movimenti_magazzino-".date('d-m-Y').".xls\") { alert(\"Nome del file non corretto. Deve essere movimenti_magazzino-DATA DI OGGI\"); return false; }  else { }
				
				} else { return false; }' />" : "<strong>ATTENZIONE: il file di oggi &egrave; gi&agrave; stato importato. Per caricarne un altro, attendere domani</strong>")."
				</form>
				</td>
				</tr>
				</table>";
					
		
				
		}
	

	}

}
