<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/../classes/AdminTab.php');

class AdminCMS extends AdminTab
{	
	private $_category;

	public function __construct()
	{
	 	$this->table = 'cms';
	 	$this->className = 'CMS';
	 	$this->lang = true;
	 	$this->edit = true;
	 	$this->view = true;
	 	$this->delete = true;
		
		$this->fieldsDisplay = array(
			'id_cms' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 25),
			'link_rewrite' => array('title' => $this->l('URL'), 'width' => 200),
			'meta_title' => array('title' => $this->l('Title'), 'width' => 300),
			'position' => array('title' => $this->l('Position'), 'width' => 40,'filter_key' => 'position', 'align' => 'center', 'position' => 'position'),
			'active' => array('title' => $this->l('Enabled'), 'width' => 25, 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false)
			);
			
		$this->_category = AdminCMSContent::getCurrentCMSCategory();
		$this->_join = '
		LEFT JOIN `'._DB_PREFIX_.'cms_category` c ON (c.`id_cms_category` = a.`id_cms_category`)';
		$this->_select = 'a.position ';
		$this->_filter = 'AND c.id_cms_category = '.(int)($this->_category->id);
		
		parent::__construct();
	}
	
	private function _displayDraftWarning($active)
	{
		return 
		'<div class="warn draft" style="'.($active ? 'display:none' : '').'">
			<p>
			<span style="float: left">
			<img src="../img/admin/warn2.png" />
			'.$this->l('Your CMS page will be saved as a draft').'
			</span>
			<input type="button" class="button" style="float: right;" value="'.$this->l('Save and preview').'" onclick="submitAddcmsAndPreview();">
			<input type="hidden" name="previewSubmitAddcmsAndPreview" id="previewSubmitAddcmsAndPreview" />
			<br class="clear" />
			</p>
		</div>';
	}
	
	public function displayForm($isMainTab = true)
	{
		global $currentIndex, $cookie;
		parent::displayForm();
		
		$obj = $this->loadObject(true);
		$iso = Language::getIsoById((int)($cookie->id_lang));
		$divLangName = 'meta_title¤meta_description¤meta_keywords¤ccontent¤link_rewrite';

		echo '
		<form action="'.$currentIndex.'&submitAdd'.$this->table.'=1&token='.Tools::getAdminTokenLite('AdminCMSContent').'" method="post" name="cms" id="cms">
		<script type="text/javascript">
		function maxlengthinputf(area,max,id_campo){
			var conta = area.value.length;
			
			if(id_campo!=null){
			
				document.getElementById(id_campo).innerHTML=conta;
				
			}
			if(conta < 0){

				area.value = area.value.substring(0,max);
				if(id_campo!=null){
					document.getElementById(id_campo).innerHTML = \'0\';
				}
			}
		}
		</script>

			'.($obj->id ? '<input type="hidden" name="id_'.$this->table.'" value="'.$obj->id.'" />' : '').'
			'.$this->_displayDraftWarning($obj->active).'
			<fieldset><legend><img src="../img/admin/cms.gif" />'.$this->l('CMS page').'</legend>';
			
		// META TITLE
		echo '<label>'.$this->l('CMS Category:').' </label>
				<div class="margin-form">
					<select name="id_cms_category">';
		$categories = CMSCategory::getCategories((int)($cookie->id_lang), false);
		CMSCategory::recurseCMSCategory($categories, $categories[0][1], 1, $this->getFieldValue($obj, 'id_cms_category'));
		echo '
					</select>
				</div>
				<label>'.$this->l('Meta title').' </label>
				<div class="margin-form">';
		foreach ($this->_languages as $language)
			echo '	<div id="meta_title_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input size="110" type="text" onkeyup="copyMeta2friendlyURL(); attivaSEO(document.getElementById(\'content_5\').value); maxlengthinputf(this,80,\'conta_title_'.$language['id_lang'].'\')" id="name_'.$language['id_lang'].'" name="meta_title_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'meta_title', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" /><sup> *</sup>
						<span id="conta_title_'.$language['id_lang'].'">'.strlen($this->getFieldValue($obj, 'meta_title', $language['id_lang'])).'</span> (Ottimale: tra i 50 e i 70)
					</div>';
		
		$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'meta_title');
		echo '	</div><div class="clear space">&nbsp;</div>';
		
		// META DESCRIPTION
		echo '	<label>'.$this->l('Meta description').' </label>
				<div class="margin-form">';
		foreach ($this->_languages as $language)
			echo '	<div id="meta_description_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input size="110" type="text" name="meta_description_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'meta_description', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" onkeyup=" maxlengthinputf(this,350,\'conta_meta_desc_'.$language['id_lang'].'\'); attivaSEO(document.getElementById(\'content_5\').value);" />
						<span id="conta_meta_desc_'.$language['id_lang'].'">'.strlen($this->getFieldValue($obj, 'meta_description', $language['id_lang'])).'</span> (Ottimale: tra i 110 e i 300)
						
					</div>';
		$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'meta_description');
		echo '	</div><div class="clear space">&nbsp;</div>';
		
		// META KEYWORDS
		echo '	<label>'.$this->l('Meta keywords').' </label>
				<div class="margin-form">';
		foreach ($this->_languages as $language)
			echo '	<div id="meta_keywords_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input size="110" onkeyup="maxlengthinputf(this,250,\'conta_keywords_'.$language['id_lang'].'\'); attivaSEO(document.getElementById(\'content_5\').value);" type="text" name="meta_keywords_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'meta_keywords', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" />
						<span id="conta_keywords_'.$language['id_lang'].'">'.strlen($this->getFieldValue($obj, 'meta_keywords', $language['id_lang'])).'</span> (Ottimale: 5-10 parole, 200 c.)
					</div>';
		$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'meta_keywords');
		echo '	</div><div class="clear space">&nbsp;</div>';
		
		// LINK REWRITE
		echo '	<label>'.$this->l('Friendly URL').' </label>
				<div class="margin-form">';
		foreach ($this->_languages as $language)
			echo '	<div id="link_rewrite_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input size="110" type="text" id="input_link_rewrite_'.$language['id_lang'].'" name="link_rewrite_'.$language['id_lang'].'" onkeyup="this.value = str2url(this.value); updateFriendlyURL();" value="'.htmlentities($this->getFieldValue($obj, 'link_rewrite', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" /><sup> *</sup>
					</div>';
		$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'link_rewrite');
		echo '	</div><div class="clear space">&nbsp;</div>';
		
		// CONTENT
		echo '	<label>'.$this->l('Page content').' </label>
				<div class="margin-form">';
		foreach ($this->_languages as $language)
			echo '	<div id="ccontent_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').';float: left;">
						<textarea onkeypress="changes=true; alert(\'OK\'); attivaSEO(document.getElementById(\'content_5\').value);"  class="editable rte" style="width:755px; height:500px"  id="content_'.$language['id_lang'].'" name="content_'.$language['id_lang'].'">'.htmlentities(stripslashes($this->getFieldValue($obj, 'content', $language['id_lang'])), ENT_COMPAT, 'UTF-8').'</textarea>
					</div>';
		$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'ccontent');
		echo '	</div>';
		
		// SEO
		
		echo '<label>SEO (solo per italiano)
					<br />
					Parola chiave:
					<input id="parola_chiave" name="parola_chiave" type="text" onkeyup="attivaSEO(document.getElementById(\'content_5\').value)" value="'.Db::getInstance()->getValue('SELECT seo_keyword FROM cms_lang WHERE id_lang = 5 AND id_cms = '.Tools::getValue('id_cms')).'" /> - <a href="https://www.ezdirect.it/cms.php?id_cms='.Tools::getValue('id_cms').'" target="_blank">Preview pagina</a><br /><br />
				
					</label><div class="margin-form space">';
					
					echo '';
					echo "
					<script type='text/javascript'>
					$(document).ready(function(){
						$('#toggleseo').click(function() {
							$('#seoby').slideToggle('fast', function() {
								if ($('#seoby').is(':hidden')) {
									var hidden_seo = 1;
								} else {
									var hidden_seo = 0;
								}
								
								$.ajax({
									type: 'POST',
									async: true,
									url: 'ajax.php?nascondiMostraSeo&type=2',
									data: {hidden_seo: hidden_seo, nascondiMostraSeo: 'y'},
									success: function(data) {
									},
									error: function(xhr,stato,errori){
									}
								});
							});
						});
					});
					</script>";
					
					$hidden_seo = (Db::getInstance()->getValue('SELECT value FROM fixed_ck WHERE id_employee = '.$cookie->id_employee.' AND name="hidden_seo_2"'));
					
					echo '
						<a href="javascript:void(0)" id="toggleseo">Clicca per aprire/chiudere SEO</a><br /><br />
					<div id="seoby" style="'.($hidden_seo == 0 ? "display:block" : 'display:none').'">
					</div>
					<script type="text/javascript">
						
						function attivaSEO(testo) 
						{
							var keyword = document.getElementById("parola_chiave").value;
							var title = document.getElementById("meta_title_5").value;
							var meta_description = document.getElementById("meta_description_5").value;
							var url = document.getElementById("link_rewrite_5").value;
							var urlified_keyword = str2url(document.getElementById("parola_chiave").value, "UTF-8");
							
							$.ajax({
							  url:"ajax.php?SEOby=y",
							  type: "POST",
							  data: { text: testo,
							  keyword: keyword,
							  title: title,
							  meta_description: meta_description,
							  url: url,
							  urlified_keyword: urlified_keyword,
							  tipo: "product"
							  },
							  success:function(r){
									console.log("SEO SUCCESS");
									$("#seoby").html(r);
							  },
							  error: function(xhr,stato,errori){
								 console.log("Errore nella cancellazione:"+xhr.status);
							  }
							});
						}
						
						var temp = document.getElementById("content_5").value;
						
						attivaSEO(temp);
						
					 </script>
					 
		 </div>';
		
		// ACTIVE
		
		echo '<div class="clear space">&nbsp;</div>
				<label>'.$this->l('Enable:').' </label>
				<div class="margin-form">
					<input type="radio" name="active" id="active_on" onclick="toggleDraftWarning(false);" value="1" '.($this->getFieldValue($obj, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_on"> <img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="active" id="active_off" onclick="toggleDraftWarning(true);" value="0" '.(!$this->getFieldValue($obj, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_off"> <img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label>
				</div>';
		// SUBMIT
		echo '	<div class="margin-form space">
					<input type="submit" value="'.$this->l('   Save   ').'" name="submitAdd'.$this->table.'" class="button" />
				</div>
				<div class="small"><sup>*</sup> '.$this->l('Required field').'</div>
			</fieldset><br />
			'.$this->_displayDraftWarning($obj->active).'
		</form>';
		// TinyMCE
		global $cookie;
		$iso = Language::getIsoById((int)($cookie->id_lang));
		$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
		$ad = dirname($_SERVER["PHP_SELF"]);
		echo '
			
			<script src="https://www.ezdirect.it/js/tinymce4.9.8/tinymce.min.js"></script>

 <script>
      tinymce.init({
        selector: "textarea", 
		resize: "both",
		images_upload_url: "postAcceptor.php",
		image_title: true, 
  images_upload_base_path: "/img/cms/",
  images_upload_credentials: true,
  plugins: "print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template code table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists textcolor wordcount spellchecker ",
  toolbar1: "undo redo styleselect | fontselect fontsizeselect formatselect | bold italic forecolor alignleft aligncenter alignright justify bulletlist numberlist link image spellchecker | numlist bullist checklist",
  link_context_toolbar: true, 
  link_class_list: [
   {title: "--", value: ""},
    {title: "Link per aprire form contattaci", value: "apri-contattaci"},
  ],
  
  browser_spellcheck : true,
  gecko_spellcheck: true,
  contextmenu: false,
  style_formats: [
    { title: \'Bold text\', inline: \'strong\' },
    { title: \'Red text\', inline: \'span\', styles: { color: \'#ff0000\' } },
    { title: \'Red header\', block: \'h1\', styles: { color: \'#ff0000\' } },
    { title: \'Badge\', inline: \'span\', styles: { display: \'inline-block\', border: \'1px solid #2276d2\', \'border-radius\': \'5px\', padding: \'2px 5px\', margin: \'0 2px\', color: \'#2276d2\' } },
    { title: \'Paragraph\', selector: \'p\', classes: \'tablerow1\' },
	 { title: \'test\', selector: \'p\', classes: \'row section-header wow fadeInUp\' }
  ],
  formats: {
    customformat: { inline: \'span\', styles: { color: \'#00ff00\', fontSize: \'20px\' }, attributes: { title: \'My custom format\' }, classes: \'example1\' },
  },
 
   images_upload_url: "postAcceptor.php",
 
  images_upload_handler: function (blobInfo, success, failure) {
    var xhr, formData;

    xhr = new XMLHttpRequest();
    xhr.withCredentials = false;
	  xhr.open("POST", "postAcceptor.php");
    xhr.onload = function() {
      var json;

      if (xhr.status != 200) {
        failure("HTTP Error: " + xhr.status);
        return;
      }

      json = JSON.parse(xhr.responseText);

      if (!json || typeof json.location != "string") {
        failure("Invalid JSON: " + xhr.responseText);
        return;
      }

      success(json.location);
    };

    formData = new FormData();
    formData.append("file", blobInfo.blob(), blobInfo.filename());

    xhr.send(formData);
  }
  
      });
	  
	 

    </script>';
	}
	
	public function display($token = NULL)
	{
		global $currentIndex, $cookie;
		
		if (($id_cms_category = (int)Tools::getValue('id_cms_category')))
			$currentIndex .= '&id_cms_category='.$id_cms_category;
		$this->getList((int)($cookie->id_lang), !$cookie->__get($this->table.'Orderby') ? 'position' : NULL, !$cookie->__get($this->table.'Orderway') ? 'ASC' : NULL);
		//$this->getList((int)($cookie->id_lang));
		if (!$id_cms_category)
			$id_cms_category = 1;
		echo '<h3>'.(!$this->_listTotal ? ($this->l('No pages found')) : ($this->_listTotal.' '.($this->_listTotal > 1 ? $this->l('pages') : $this->l('page')))).' '.
		$this->l('in category').' "'.stripslashes(CMSCategory::hideCMSCategoryPosition($this->_category->getName())).'"</h3>';
		echo '<a href="'.$currentIndex.'&id_cms_category='.$id_cms_category.'&add'.$this->table.'&token='.Tools::getAdminTokenLite('AdminCMSContent').'"><img src="../img/admin/add.gif" border="0" /> '.$this->l('Add a new page').'</a>
		<div style="margin:10px;">';
		$this->displayList($token);
		echo '</div>';
	}
	
	public function displayList($token = NULL)
	{
		global $currentIndex;
		
		/* Display list header (filtering, pagination and column names) */
		$this->displayListHeader($token);
		if (!sizeof($this->_list))
			echo '<tr><td class="center" colspan="'.(sizeof($this->fieldsDisplay) + 2).'">'.$this->l('No items found').'</td></tr>';

		/* Show the content of the table */
		$this->displayListContent($token);

		/* Close list table and submit button */
		$this->displayListFooter($token);
	}

	function postProcess()
	{
		global $cookie, $link, $currentIndex;
		
		if (Tools::isSubmit('viewcms') AND ($id_cms = (int)(Tools::getValue('id_cms'))) AND $cms = new CMS($id_cms, (int)($cookie->id_lang)) AND Validate::isLoadedObject($cms))
		{
			$redir = $link->getCMSLink($cms);
			if (!$cms->active)
			{
				$admin_dir = dirname($_SERVER['PHP_SELF']);
				$admin_dir = substr($admin_dir, strrpos($admin_dir,'/') + 1);
				$redir .= '?adtoken='.Tools::encrypt('PreviewCMS'.$cms->id).'&ad='.$admin_dir;
			}
			Tools::redirectAdmin($redir);
		}
		elseif (Tools::isSubmit('deletecms'))
		{
			if (Tools::getValue('id_cms') == Configuration::get('PS_CONDITIONS_CMS_ID'))
			{
				Configuration::updateValue('PS_CONDITIONS', 0);
				Configuration::updateValue('PS_CONDITIONS_CMS_ID', 0);
			}
			$cms = new CMS((int)(Tools::getValue('id_cms')));
			$cms->cleanPositions($cms->id_cms_category);
			if (!$cms->delete())
				$this->_errors[] = Tools::displayError('An error occurred while deleting object.').' <b>'.$this->table.' ('.mysql_error().')</b>';
			else
				Tools::redirectAdmin($currentIndex.'&id_cms_category='.$cms->id_cms_category.'&conf=1&token='.Tools::getAdminTokenLite('AdminCMSContent'));
		}/* Delete multiple objects */
		elseif (Tools::getValue('submitDel'.$this->table))
		{
			if ($this->tabAccess['delete'] === '1')
			{
				if (isset($_POST[$this->table.'Box']))
				{
					$cms = new CMS();
					$result = true;
					$result = $cms->deleteSelection(Tools::getValue($this->table.'Box'));
					if ($result)
					{
						$cms->cleanPositions((int)(Tools::getValue('id_cms_category')));
						Tools::redirectAdmin($currentIndex.'&conf=2&token='.Tools::getAdminTokenLite('AdminCMSContent').'&id_category='.(int)(Tools::getValue('id_cms_category')));
					}
					$this->_errors[] = Tools::displayError('An error occurred while deleting selection.');

				}
				else
					$this->_errors[] = Tools::displayError('You must select at least one element to delete.');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to delete here.');
		}
		elseif (Tools::isSubmit('submitAddcms') OR Tools::isSubmit('submitAddcmsAndPreview'))
		{
			parent::validateRules();

			if (!sizeof($this->_errors))
			{
				Db::getInstance()->executes("UPDATE cms_lang SET seo_keyword = '".$_POST['parola_chiave']."' WHERE id_lang = 5 AND id_cms = ".$object->id."");
				
				
				
				if (!$id_cms = (int)(Tools::getValue('id_cms')))
				{
					$cms = new CMS();
					$this->copyFromPost($cms, 'cms');
					if (!$cms->add())
						$this->_errors[] = Tools::displayError('An error occurred while creating object.').' <b>'.$this->table.' ('.mysql_error().')</b>';
					elseif (Tools::isSubmit('submitAddcmsAndPreview'))
					{
						Db::getInstance()->executes("UPDATE cms_lang SET seo_keyword = '".$_POST['parola_chiave']."' WHERE id_lang = 5 AND id_cms = ".$cms->id."");
						
						$preview_url = $link->getCMSLink($cms, $this->getFieldValue($object, 'link_rewrite', $this->_defaultFormLanguage), (int)($cookie->id_lang));
						if (!$cms->active)
						{
							$admin_dir = dirname($_SERVER['PHP_SELF']);
							$admin_dir = substr($admin_dir, strrpos($admin_dir,'/') + 1);
							$token = Tools::encrypt('PreviewCMS'.$cms->id);
	
							$preview_url .= $object->active ? '' : '&adtoken='.$token.'&ad='.$admin_dir;
						}
						Tools::redirectAdmin($preview_url);
					}
					else
						Tools::redirectAdmin($currentIndex.'&id_cms_category='.$cms->id_cms_category.'&conf=3&token='.Tools::getAdminTokenLite('AdminCMSContent'));
				}
				else
				{
					$cms = new CMS($id_cms);
					
					Db::getInstance()->executes("UPDATE cms_lang SET seo_keyword = '".$_POST['parola_chiave']."' WHERE id_lang = 5 AND id_cms = ".$cms->id."");
					
					$this->copyFromPost($cms, 'cms');
					if (!$cms->update())
						$this->_errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.' ('.mysql_error().')</b>';
					elseif (Tools::isSubmit('submitAddcmsAndPreview'))
					{
						$preview_url = $link->getCMSLink($cms, $this->getFieldValue($object, 'link_rewrite', $this->_defaultFormLanguage), (int)($cookie->id_lang));
						if (!$cms->active)
						{
							$admin_dir = dirname($_SERVER['PHP_SELF']);
							$admin_dir = substr($admin_dir, strrpos($admin_dir,'/') + 1);
							$token = Tools::encrypt('PreviewCMS'.$cms->id);
	
							$preview_url .= $object->active ? '' : '&adtoken='.$token.'&ad='.$admin_dir;
						}
						Tools::redirectAdmin($preview_url);
					}
					else
						Tools::redirectAdmin($currentIndex.'&id_cms_category='.$cms->id_cms_category.'&conf=4&token='.Tools::getAdminTokenLite('AdminCMSContent'));
				}
			}
		}
		elseif (Tools::getValue('position'))
		{
			if ($this->tabAccess['edit'] !== '1')
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
			elseif (!Validate::isLoadedObject($object = $this->loadObject()))
				$this->_errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			elseif (!$object->updatePosition((int)(Tools::getValue('way')), (int)(Tools::getValue('position'))))
				$this->_errors[] = Tools::displayError('Failed to update the position.');
			else
				Tools::redirectAdmin($currentIndex.'&'.$this->table.'Orderby=position&'.$this->table.'Orderway=asc&conf=4'.(($id_category = (int)(Tools::getValue('id_cms_category'))) ? ('&id_cms_category='.$id_category) : '').'&token='.Tools::getAdminTokenLite('AdminCMSContent'));
		}
		/* Change object statuts (active, inactive) */
		elseif (Tools::isSubmit('status') AND Tools::isSubmit($this->identifier))
		{
			if ($this->tabAccess['edit'] === '1')
			{
				if (Validate::isLoadedObject($object = $this->loadObject()))
				{
					if ($object->toggleStatus())
						Tools::redirectAdmin($currentIndex.'&conf=5'.((int)Tools::getValue('id_cms_category') ? '&id_cms_category='.(int)Tools::getValue('id_cms_category') : '').'&token='.Tools::getValue('token'));
					else
						$this->_errors[] = Tools::displayError('An error occurred while updating status.');
				}
				else
					$this->_errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
		}
		else
			parent::postProcess(true);
	}
}


