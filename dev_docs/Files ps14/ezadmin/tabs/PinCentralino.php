<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

class PinCentralino extends AdminTab
{

	public function display()
	{
		global $cookie;

		echo '<h1>Pin centralino</h1>';
		
		echo 'Inserisci il file nel form per ottenere un file di testo coi campi dei PIN compilati:
		<form method="post" action="ajax.php" enctype="multipart/form-data">
		<input type="file" name="file-di-testo" style="display:block; float:left"  />
		<input type="submit" name="carica-pin-centralino" class="button" value="Carica" style="display:block; float:left; height:28px; margin-left:10px" />
		<div style="clear:both"></div>
		</form>';
		
	}
}


