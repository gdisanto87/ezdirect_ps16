<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

class ExportComp extends AdminPreferences
{


	public function display()
	{
		echo "<h1>Esportatore universale per comparaprezzi</h1>";


		
	function createLink ($id, $cat_rewrite, $rewrite) {

		$link = '';
		$link .= (_PS_BASE_URL_.__PS_BASE_URI__);
				
		if ($cat_rewrite && $cat_rewrite != 'home') {
			$link .= $cat_rewrite.'/';
		}
		else  {
			$link .= '';
		}
		$link .= $id.'-'.$rewrite;

		$link .= '.html';
		
		return $link;
	
	}
		
		
		
		
		
function check($id) {
if($id == 1) {
echo "checked='checked'";
}
else {
}
}


$tax_rate = Db::getInstance()->getValue("SELECT rate FROM tax WHERE id_tax = 1");


if(isset($_POST['submit']) && $_POST['submit'] == 'Tutti (no Amazon)') {


if(!is_dir("../listini")) {
mkdir("../listini");
}
else {
}
if(is_file("../listini/trovaprezzi.txt")) {
unlink("../listini/trovaprezzi.txt");
} else { }
$file_txt = "trovaprezzi.txt";
$file_txt=fopen("../listini/$file_txt","a+");
$riga_prodotti = "";

		$query = "SELECT product_lang.name AS nome, product.active AS attivo, product.price AS prezzo, imaget.id_image AS immagine, product.id_product AS id, category_lang.name AS categoria, product.quantity, product_lang.description_short AS descrizione, product.reference AS codice, manufacturer.name AS produttore, product.supplier_reference AS sku, product.ean13 AS ean FROM product JOIN product_lang ON product.id_product = product_lang.id_product JOIN category_lang ON product.id_category_default = category_lang.id_category JOIN manufacturer ON product.id_manufacturer = manufacturer.id_manufacturer JOIN (SELECT * FROM image WHERE cover = 1) imaget ON product.id_product = imaget.id_product WHERE product.active = 1  AND product.fuori_produzione = 0 AND product.trovaprezzi = 1 AND product.id_category_default != 248 AND product.id_category_default != 119 AND product.id_category_default != 249 AND product.id_category_default != 256 AND product_lang.id_lang = 5 AND category_lang.id_lang = 5 GROUP BY product.id_product ORDER BY product_lang.name ASC";
			
		$rows = Db::getInstance()->executeS($query);
		foreach($rows as $row) {
			Db::getInstance()->executeS("UPDATE product SET comparaprezzi_check = 1, trovaprezzi = 1 WHERE id_product = $row[id]");

			$nome = $row['nome'];
			$ean13 = $row['ean13'];
			$link = "http://www.ezdirect.it/product.php?id_product=".$row['id'];
			//$migliorprezzo = Product::trovaMigliorPrezzo($row['id'], 1, 99);	
			$speciale = Product::trovaPrezzoSpeciale($row['id'], 1);
			//$row['prezzo'] = $migliorprezzo;
			if($speciale != 0) {
				if($row['prezzo'] > $speciale) {
				
					$row['prezzo'] = $speciale;
				}
			}		
			
			$prezzo = round(($row['prezzo'])+(($row['prezzo']*22)/100),2);
			$categoria = $row['categoria'];
			$marca = $row['produttore'];
			
			if($marca == '3CX')
				$categoria = 'Telefonia IP';
		
			$immagine = "http://www.ezdirect.it/img/p/".$row['id']."-".$row['immagine']."-medium.jpg";
			
			if($row['quantity'] > 0)
				$disponibilita = "disponibile";
			else
				$disponibilita = "non disponibile";
		
			$descrizione = strip_tags($row['descrizione']);
			if($prezzo < 499.00) {
				$spedizione = 8.90+((8.90*22)/100);
			}
			else {
				$spedizione = 8.90+((8.90*22)/100);
			}
			$codice = $row['codice'];
			$codice_produttore = $row['sku'];
			$ean = $row['ean'];
			if ($row['prezzo'] > 0) {
			
			}
			else {
				$row['attivo'] = 0;
			}
			if($row['attivo'] == 0) {
			}
			else {	
				$riga_prodotti .= "$nome|$marca|$descrizione|$prezzo|$codice|$link|$disponibilita|$categoria|$immagine|$spedizione|$codice_produttore|$ean<endrecord>";
			}
		}

  	
	$bundles = Db::getInstance()->executeS("SELECT * FROM bundle WHERE active = 1 AND  trovaprezzi = 1");
	foreach($bundles as $bundle) {
		$id_bundle = $bundle['id_bundle'];
		$id_categoria = Bundle::getBundleCategory($id_bundle, 5);
		$categoria = Db::getInstance()->getValue("SELECT name FROM category_lang WHERE id_lang = 5 AND id_category = ".$id_categoria."");
		$id_marca = Bundle::getBundleManufacturer($id_bundle, 5);
		$marca = Db::getInstance()->getValue("SELECT name FROM manufacturer WHERE id_manufacturer = ".$id_marca."");
		$codice = $bundle['bundle_ref'];
		$ean13 = $bundle['ean13'];
		$ean = "";
		$sku = "";
		$codice_produttore = "";
		if(!empty($bundle['bundle_name'])) {
			$nome = $bundle['bundle_name'];
		}
		else {
			$nome = Bundle::getBundleNames($id_bundle, 5);
		}
		$descrizione = Bundle::getBundleNames($id_bundle, 5);
		$prezzi = Product::getBundlesPrices($id_bundle);
		$prezzo = round(($prezzi[0])+(($prezzi[0]*22)/100),2);
		if($marca == '3CX')
			$categoria = 'Telefonia IP';
		$link = Link::getBundleLink($id_bundle);
		$disponibilita = "disponibile";
		if($prezzo < 499.00) {
			$spedizione = 8.90+((8.90*22)/100);
		}
		else {
			$spedizione = 8.90+((8.90*22)/100);
		}
		$id_immagine = Db::getInstance()->getValue("SELECT id_image FROM bundle_image WHERE id_bundle = ".$id_bundle."");
		if(!$id_immagine) {
			$immagine = "";
		}
		else {
			$immagine = "http://www.ezdirect.it/img/b/".$id_immagine."-".$id_bundle."-medium.jpg";
		}
		$riga_prodotti .= "$nome|$marca|$descrizione|$prezzo|$codice|$link|$disponibilita|$categoria|$immagine|$spedizione|$codice_produttore|$ean<endrecord>";
	}	
    
@fwrite($file_txt,$riga_prodotti);

echo "Listino prodotti per Trovaprezzi esportato con successo! 
<br /><a href='../listini/trovaprezzi.txt' target='_blank'>Salva o apri il listino esportato</a><br />
<a href='index.php?tab=ExportComp&token=".$this->token."'>Torna indietro</a>
";
////////////////////////////////////////////////////////////////////////////////////////////////////////////

if(!is_dir("../listini")) {
mkdir("../listini");
}
else {
}
if(is_file("../listini/googleshopping.txt")) {
unlink("../listini/googleshopping.txt");
} else { }
$file_txt = "googleshopping.txt";
$file_txt=fopen("../listini/$file_txt","a+");
$riga_prodotti = "";

		$riga_prodotti = "ID\tCondizione\tDisponibilit&agrave;\tLink\tTitolo\tCategoria\tMarca\tDescrizione\tPrezzo\tCategoria prodotto Google\tGTIN\tlink_immagine\tQuantit&agrave;\tMpn\tSpedizione\n";

		$query = "SELECT product_lang.name AS nome, product.active AS attivo, product_lang.link_rewrite AS rewrite, category_lang.link_rewrite AS cat_rewrite, imaget.id_image AS immagine, product.quantity AS quantita, product.condition AS condizione, product.price AS prezzo, product.id_product AS id, category_lang.name AS categoria, product_lang.description_short AS descrizione, product_lang.meta_description AS meta_description, product_lang.description AS descrizione_lunga, product.reference AS codice, manufacturer.name AS produttore, product.supplier_reference AS sku, product.ean13 AS ean FROM product JOIN product_lang ON product.id_product = product_lang.id_product JOIN category_lang ON product.id_category_default = category_lang.id_category JOIN manufacturer ON product.id_manufacturer = manufacturer.id_manufacturer JOIN (SELECT * FROM image WHERE cover = 1) imaget ON product.id_product = imaget.id_product WHERE product.active = 1 AND product.fuori_produzione = 0 AND product.google_shopping = 1 AND product_lang.id_lang = 5 AND product.id_category_default != 248 AND product.id_category_default != 119 AND product.id_category_default != 249 AND product.id_category_default != 256 AND category_lang.id_lang = 5 GROUP BY product.id_product ORDER BY product_lang.name ASC";
			
		$rows = Db::getInstance()->executeS($query);
		foreach($rows as $row) {

			Db::getInstance()->executeS("UPDATE product SET comparaprezzi_check = 1, google_shopping = 1 WHERE id_product = $row[id]");

		
			$id = $row['id'];
			$nome = $row['nome'];
			$categoria = $row['categoria'];
			$cat_rewrite = $row['cat_rewrite'];
			
			$link = createLink($id, $cat_rewrite, $row['rewrite']);
			
				
			$migliorprezzo = Product::trovaMigliorPrezzo($row['id'], 1, 99);	
				
			//$row['prezzo'] = $migliorprezzo;
			$speciale = Product::trovaPrezzoSpeciale($row['id'], 1);
		
			if($speciale != 0) {
				if($row['prezzo'] > $speciale) {
				
					$row['prezzo'] = $speciale;
				}
			}
			$prezzo = round(($row['prezzo'])+(($row['prezzo']*22)/100),2);
			$marca = $row['produttore'];
			$immagine = "http://www.ezdirect.it/img/p/".$row['id']."-".$row['immagine']."-medium.jpg";
			$disponibilita = "disponibile";
			$descrizione = strip_tags($row['descrizione']);
			
			if($descrizione == '') {
				$descrizione = strip_tags($row['meta_description']);
			}
			
			if($marca == '') {
				$marca = 'Produttore non disponibile';
			}
						/*
			$descrizione_lunga = strip_tags(preg_replace('@<h1[^>]*?>.*?<\/h1>@si', '', $row['descrizione_lunga']));
			$descrizione = $descrizione." ".$descrizione_lunga;*/
			$descrizione = str_replace(array("\r\n", "\r", "\n", "\t"), ' ', $descrizione);
		
			if($prezzo < 499.00) {
				$spedizione = 8.90+((8.90*22)/100);
			}
			else {
				$spedizione = 8.90+((8.90*22)/100);
			}
			
			$freeshipping = Db::getInstance()->getValue('SELECT value FROM configuration WHERE id_configuration = 240');
			$freeshipping = unserialize($freeshipping);
			
			if(in_array($id, $freeshipping)) {
				$spedizione = 0;
			}
			
			$spedizione = "IT:::".$spedizione." EUR";
			
			$codice = $row['codice'];
				
			if($codice == '') {
				$codice = 'ND';
			}
		
			$codice_produttore = $row['sku'];
			$ean = $row['ean'];
			$condizione = "nuovo";
			$categoriagoogle = "Dispositivi elettronici > Comunicazioni";
			$quantita = $row['quantita'];
			
			$accessorio = Db::getInstance()->getValue('SELECT id_product FROM feature_product WHERE id_product = '.$row['id'].' AND (id_feature_value = 8737 OR id_feature_value = 12989 OR id_feature_value = 6995 OR id_feature_value = 2 OR id_feature_value = 5766 OR id_feature_value = 5006 OR id_feature_value = 5756 OR id_feature_value = 5090 OR id_feature_value = 5055 OR id_feature_value = 5482 OR id_feature_value = 6459 OR id_feature_value = 11221 OR id_feature_value = 6204 OR id_feature_value = 6229 OR id_feature_value = 11281 OR id_feature_value = 11278 OR id_feature_value = 11280 OR id_feature_value = 11279 OR id_feature_value = 8286 OR id_feature_value = 12850 OR id_feature_value = 9899 OR id_feature_value = 11152)');
		
			if($accessorio > 0)
			{
				
			}
		
			if ($row['prezzo'] > 0) {
			
			}
			else {
				$row['attivo'] = 0;
			}
			if($row['attivo'] == 0 || $accessorio > 0) {
			}
			else {	

				$riga_prodotti .= "$id\t$condizione\t$disponibilita\t$link\t$nome\t$categoria\t$marca\t$descrizione\t$prezzo\t$categoriagoogle\t$ean\t$immagine\t$quantita\t$codice\t$spedizione\n";

			}
		}

   	
	$bundles = Db::getInstance()->executeS("SELECT * FROM bundle WHERE active = 1 AND  google_shopping = 1");
	foreach($bundles as $bundle) {
		$id_bundle = $bundle['id_bundle'];
		$id = $id_bundle."b";
		$id_categoria = Bundle::getBundleCategory($id_bundle, 5);
		$categoria = Db::getInstance()->getValue("SELECT name FROM category_lang WHERE id_lang = 5 AND id_category = ".$id_categoria."");
		$id_marca = Bundle::getBundleManufacturer($id_bundle, 5);
		$marca = Db::getInstance()->getValue("SELECT name FROM manufacturer WHERE id_manufacturer = ".$id_marca."");
		$codice = $bundle['bundle_ref'];
		if($codice == "") {
			$codice = "ND";
		}
		$ean = "";
		$sku = "";
		$codice_produttore = "";
		if(!empty($bundle['bundle_name'])) {
			$nome = $bundle['bundle_name'];
		}
		else {
			$nome = Bundle::getBundleNames($id_bundle, 5);
		}
		$descrizione = Bundle::getBundleNames($id_bundle, 5);
		$prezzi = Product::getBundlesPrices($id_bundle);
		$prezzo = round(($prezzi[1])+(($prezzi[1]*22)/100),2);
		
		$link = Link::getBundleLink($id_bundle);
		$disponibilita = "disponibile";
		if($prezzo < 499.00) {
			$spedizione = 8.90+((8.90*22)/100);
		}
		else {
			$spedizione = 8.90+((8.90*22)/100);
		}
		
			
		$spedizione = "IT:::".$spedizione." EUR";
		
		$id_immagine = Db::getInstance()->getValue("SELECT id_image FROM bundle_image WHERE id_bundle = ".$id_bundle."");
		if(!$id_immagine) {
			$immagine = "";
		}
		else {
			$immagine = "http://www.ezdirect.it/img/b/".$id_immagine."-".$id_bundle."-medium.jpg";
		}
		
		$r_immagine = Db::getInstance()->getValue('SELECT id_image FROM image WHERE id_product = '.$bundle['father'].' AND cover = 1');
		$immagine = "http://www.ezdirect.it/img/p/".$bundle['father']."-".$r_immagine."-medium.jpg";
		
		$condizione = "nuovo";
		$categoriagoogle = "Dispositivi elettronici > Comunicazioni";
		$quantita = 999;
		$riga_prodotti .= "$id\t$condizione\t$disponibilita\t$link\t$nome\t$categoria\t$marca\t$descrizione\t$prezzo\t$categoriagoogle\t$ean\t$immagine\t$quantita\t$codice\t$spedizione\n";
	}	 
@fwrite($file_txt,$riga_prodotti);

echo "Listino prodotti per Google Shopping esportato con successo! 
<br /><a href='../listini/googleshopping.txt' target='_blank'>Salva o apri il listino esportato</a><br />
<a href='index.php?tab=ExportComp&token=".$this->token."'>Torna indietro</a>
";

}

//////////////////////////////////////////////////////////////////////////////////////


else if(isset($_POST['submit']) && $_POST['submit'] == 'Trovaprezzi') {

if(!is_dir("../listini")) {
mkdir("../listini");
}
else {
}
if(is_file("../listini/trovaprezzi.txt")) {
unlink("../listini/trovaprezzi.txt");
} else { }
$file_txt = "trovaprezzi.txt";
$file_txt=fopen("../listini/$file_txt","a+");
$riga_prodotti = "";

Db::getInstance()->executeS("UPDATE product SET comparaprezzi_check = 0, trovaprezzi = 0");
Db::getInstance()->executeS("UPDATE category SET comparaprezzi_check = 0, trovaprezzi = 0");

if(isset($_POST['categoria'])) {
 $categorie = $_POST['categoria'];
    $numero_categorie = count($categorie);
	
		

    for ($i=0; $i<$numero_categorie; $i++) {
	


Db::getInstance()->executeS("UPDATE category SET comparaprezzi_check = 1, trovaprezzi = 1 WHERE id_category = $categorie[$i]");

    
	
}
}
	if(isset($_POST['prodotto'])) {
		$prodotti = $_POST['prodotto'];
		$numero_prodotti = count($prodotti);

		foreach($_POST['prodotto'] as $prodotto) {
	
			$query = "SELECT product_lang.name AS nome, product.active AS attivo, product.price AS prezzo, imaget.id_image AS immagine, product.id_product AS id, category_lang.name AS categoria, product_lang.description_short AS descrizione, product.reference AS codice, manufacturer.name AS produttore, product.supplier_reference AS sku, product.ean13 AS ean FROM product JOIN product_lang ON product.id_product = product_lang.id_product JOIN category_lang ON product.id_category_default = category_lang.id_category JOIN manufacturer ON product.id_manufacturer = manufacturer.id_manufacturer JOIN (SELECT * FROM image WHERE cover = 1) imaget ON product.id_product = imaget.id_product WHERE product.id_product = $prodotto AND product.id_category_default != 248 AND product.id_category_default != 119 AND product.id_category_default != 249 AND product.id_category_default != 256 AND product_lang.id_lang = 5 AND category_lang.id_lang = 5 GROUP BY product.id_product ORDER BY product_lang.name ASC";
			
			$rows = Db::getInstance()->executeS($query);
	foreach($rows as $row) {
				Db::getInstance()->executeS("UPDATE product SET comparaprezzi_check = 1, trovaprezzi = 1 WHERE id_product = $row[id]");

				$nome = $row['nome'];
				$link = "http://www.ezdirect.it/product.php?id_product=".$row['id'];
				//$migliorprezzo = Product::trovaMigliorPrezzo($row['id'], 1, 99);	
				//$row['prezzo'] = $migliorprezzo;
				$speciale = Product::trovaPrezzoSpeciale($row['id'], 1);
		
				if($speciale != 0) {
					if($row['prezzo'] > $speciale) {
					
						$row['prezzo'] = $speciale;
					}
				}		
				//$prezzo = round(($migliorprezzo)+(($migliorprezzo*22)/100),2);
				$prezzo = round(($row['prezzo'])+(($row['prezzo']*22)/100),2);
				$categoria = $row['categoria'];
				$marca = $row['produttore'];
				$immagine = "http://www.ezdirect.it/img/p/".$row['id']."-".$row['immagine']."-medium.jpg";
				$disponibilita = "disponibile";
				$descrizione = strip_tags($row['descrizione']);
				if($prezzo < 499.00) {
					$spedizione = 8.90+((8.90*22)/100);
				}
				else {
					$spedizione = 8.90+((8.90*22)/100);
				}
				$codice = $row['codice'];
				$codice_produttore = $row['sku'];
				$ean = $row['ean'];
				if ($row['prezzo'] > 0) {
					
				}
				else {
					$row['attivo'] = 0;
				}
				if($row['attivo'] == 0) {
				}
				else {	
					$riga_prodotti .= "$nome|$marca|$descrizione|$prezzo|$codice|$link|$disponibilita|$categoria|$immagine|$spedizione|$codice_produttore<endrecord>";
				}
			}
		}
	
    }
    foreach($_POST['bundle'] as $id_bundle) {
		
		$bundle = Db::getInstance()->getRow("SELECT * FROM bundle WHERE active = 1 AND  id_bundle = ".$id_bundle."");
		$id_categoria = Bundle::getBundleCategory($id_bundle, 5);
		$categoria = Db::getInstance()->getValue("SELECT name FROM category_lang WHERE id_lang = 5 AND id_category = ".$id_categoria."");
		$id_marca = Bundle::getBundleManufacturer($id_bundle, 5);
		$marca = Db::getInstance()->getValue("SELECT name FROM manufacturer WHERE id_manufacturer = ".$id_marca."");
		$codice = $bundle['bundle_ref'];
		$ean = "";
		$sku = "";
		$codice_produttore = "";
		if(!empty($bundle['bundle_name'])) {
			$nome = $bundle['bundle_name'];
		}
		else {
			$nome = Bundle::getBundleNames($id_bundle, 5);
		}
		$descrizione = Bundle::getBundleNames($id_bundle, 5);
		$prezzi = Product::getBundlesPrices($id_bundle);
		$prezzo = round(($prezzi[0])+(($prezzi[0]*22)/100),2);
		
		$link = Link::getBundleLink($id_bundle);
		$disponibilita = "disponibile";
		if($prezzo < 499.00) {
			$spedizione = 8.90+((8.90*22)/100);
		}
		else {
			$spedizione = 8.90+((8.90*22)/100);
		}
		$id_immagine = Db::getInstance()->getValue("SELECT id_image FROM bundle_image WHERE id_bundle = ".$id_bundle."");
		if(!$id_immagine) {
			$immagine = "";
		}
		else {
			$immagine = "http://www.ezdirect.it/img/b/".$id_immagine."-".$id_bundle."-medium.jpg";
		}
	
		$riga_prodotti .= "$nome|$marca|$descrizione|$prezzo|$codice|$link|$disponibilita|$categoria|$immagine|$spedizione|$codice_produttore<endrecord>";
	}    
@fwrite($file_txt,$riga_prodotti);

echo "Listino prodotti per Trovaprezzi esportato con successo! 
<br /><a href='../listini/trovaprezzi.txt' target='_blank'>Salva o apri il listino esportato</a><br />
<a href='index.php?tab=ExportComp&token=".$this->token."'>Torna indietro</a>
";

}

else if(isset($_POST['submit']) && $_POST['submit'] == 'Google Shopping') {

if(!is_dir("../listini")) {
mkdir("../listini");
}
else {
}
if(is_file("../listini/googleshopping.txt")) {
unlink("../listini/googleshopping.txt");
} else { }
$file_txt = "googleshopping.txt";
$file_txt=fopen("../listini/$file_txt","a+");
$riga_prodotti = "";

Db::getInstance()->executeS("UPDATE product SET comparaprezzi_check = 0, google_shopping = 0");
	Db::getInstance()->executeS("UPDATE category SET comparaprezzi_check = 0, google_shopping = 0");

if(isset($_POST['categoria'])) {
 $categorie = $_POST['categoria'];
    $numero_categorie = count($categorie);

	
	
    for ($i=0; $i<$numero_categorie; $i++) {
	
	

Db::getInstance()->executeS("UPDATE category SET comparaprezzi_check = 1, google_shopping = 1 WHERE id_category = $categorie[$i]");

    
	
}
}
	if(isset($_POST['prodotto'])) {
		$prodotti = $_POST['prodotto'];
		$numero_prodotti = count($prodotti);
		
		$riga_prodotti = "ID\tCondizione\tDisponibilit&agrave;\tLink\tTitolo\tCategoria\tMarca\tDescrizione\tPrezzo\tCategoria prodotto Google\tGTIN\tlink_immagine\tQuantit&agrave;\tMpn\tSpedizione\n";

		foreach($_POST['prodotto'] as $prodotto) {

	
			$query = "SELECT product_lang.name AS nome, product.active AS attivo, product_lang.link_rewrite AS rewrite, category_lang.link_rewrite AS cat_rewrite, imaget.id_image AS immagine, product.quantity AS quantita, product.condition AS condizione, product.price AS prezzo, product.id_product AS id, category_lang.name AS categoria, product_lang.description_short AS descrizione, product_lang.meta_description AS meta_description, product_lang.description AS descrizione_lunga, product.reference AS codice, manufacturer.name AS produttore, product.supplier_reference AS sku, product.ean13 AS ean FROM product 
			JOIN product_lang ON product.id_product = product_lang.id_product 
			JOIN category_lang ON product.id_category_default = category_lang.id_category 
			JOIN manufacturer ON product.id_manufacturer = manufacturer.id_manufacturer 
			JOIN (SELECT * FROM image WHERE cover = 1) imaget ON product.id_product = imaget.id_product WHERE product.id_product = $prodotto AND product_lang.id_lang = 5 AND product.id_category_default != 248 AND product.id_category_default != 119 AND product.id_category_default != 249 AND product.id_category_default != 256 AND category_lang.id_lang = 5 GROUP BY product.id_product ORDER BY product_lang.name ASC";
			
			$rows = Db::getInstance()->executeS($query);
	foreach($rows as $row) {

				Db::getInstance()->executeS("UPDATE product SET comparaprezzi_check = 1, google_shopping = 1 WHERE id_product = $row[id]");

	
				$id = $row['id'];
				$nome = $row['nome'];

					
				$migliorprezzo = Product::trovaMigliorPrezzo($row['id'], 1, 99);	
				//$row['prezzo'] = $migliorprezzo;
				$speciale = Product::trovaPrezzoSpeciale($row['id'], 1);
				
				if($speciale != 0) {
					if($row['prezzo'] > $speciale) {
					
						$row['prezzo'] = $speciale;
					}
				}
				$prezzo = round(($row['prezzo'])+(($row['prezzo']*22)/100),2);
				$categoria = $row['categoria'];
				$cat_rewrite = $row['cat_rewrite'];
				
				$link = createLink($id, $cat_rewrite, $row['rewrite']);
				
				$marca = $row['produttore'];
				$immagine = "http://www.ezdirect.it/img/p/".$row['id']."-".$row['immagine']."-medium.jpg";
				$disponibilita = "disponibile";
				$descrizione = strip_tags($row['descrizione']);
				/*
				$descrizione_lunga = strip_tags(preg_replace('@<h1[^>]*?>.*?<\/h1>@si', '', $row['descrizione_lunga']));
				$descrizione = $descrizione." ".$descrizione_lunga;*/
	

				if($descrizione == '') {
					$descrizione = strip_tags($row['meta_description']);
				}
				
				$descrizione = str_replace(array("\r\n", "\r", "\n", "\t"), ' ', $descrizione);
				
				if($marca == '') {
					$marca = 'Produttore non disponibile';
				}

				if($prezzo < 499.00) {
					$spedizione = 8.90+((8.90*22)/100);
				}
				else {
					$spedizione = 8.90+((8.90*22)/100);
				}
				
				$freeshipping = Db::getInstance()->getValue('SELECT value FROM configuration WHERE id_configuration = 240');
				$freeshipping = unserialize($freeshipping);
				
				if(in_array($id, $freeshipping)) {
					$spedizione = 8.90+((8.90*22)/100);
				}
				
				$spedizione = "IT:::".$spedizione." EUR";
			
				$codice = $row['codice'];
				if($codice == '') {
					$codice = 'ND';
				}
				
				$codice_produttore = $row['sku'];
				$ean = $row['ean'];
				$condizione = "nuovo";
				$categoriagoogle = "Dispositivi elettronici > Comunicazioni";
				$quantita = $row['quantita'];
				
				$accessorio = Db::getInstance()->getValue('SELECT id_product FROM feature_product WHERE id_product = '.$row['id'].' AND (id_feature_value = 8737 OR id_feature_value = 12989 OR id_feature_value = 6995 OR id_feature_value = 2 OR id_feature_value = 5766 OR id_feature_value = 5006 OR id_feature_value = 5756 OR id_feature_value = 5090 OR id_feature_value = 5055 OR id_feature_value = 5482 OR id_feature_value = 6459 OR id_feature_value = 11221 OR id_feature_value = 6204 OR id_feature_value = 6229 OR id_feature_value = 11281 OR id_feature_value = 11278 OR id_feature_value = 11280 OR id_feature_value = 11279 OR id_feature_value = 8286 OR id_feature_value = 12850 OR id_feature_value = 9899 OR id_feature_value = 11152)');
		
				if($accessorio > 0)
				{
					
				}
		
				if ($row['prezzo'] > 0) {
			
				}
				else {
					$row['attivo'] = 0;
				}
				if($row['attivo'] == 0 || $accessorio > 0) {
				}
				else {	
				$riga_prodotti .= "$id\t$condizione\t$disponibilita\t$link\t$nome\t$categoria\t$marca\t$descrizione\t$prezzo\t$categoriagoogle\t$immagine\t$quantita\t$codice\t$spedizione\n";

				}
			}
		}
	
    }

    foreach($_POST['bundle'] as $id_bundle) {
		
		$bundle = Db::getInstance()->getRow("SELECT * FROM bundle WHERE active = 1 AND  id_bundle = ".$id_bundle."");
		$id = $id_bundle."b";
		$id_categoria = Bundle::getBundleCategory($id_bundle, 5);
		$categoria = Db::getInstance()->getValue("SELECT name FROM category_lang WHERE id_lang = 5 AND id_category = ".$id_categoria."");
		$id_marca = Bundle::getBundleManufacturer($id_bundle, 5);
		$marca = Db::getInstance()->getValue("SELECT name FROM manufacturer WHERE id_manufacturer = ".$id_marca."");
		$codice = $bundle['bundle_ref'];
		if($codice == "") {
			$codice = "ND";
		}
		$ean = "";
		$sku = "";
		$codice_produttore = "";
		if(!empty($bundle['bundle_name'])) {
			$nome = $bundle['bundle_name'];
		}
		else {
			$nome = Bundle::getBundleNames($id_bundle, 5);
		}
		$descrizione = Bundle::getBundleNames($id_bundle, 5);
		$prezzi = Product::getBundlesPrices($id_bundle);
		$prezzo = round(($prezzi[1])+(($prezzi[1]*22)/100),2);
		
		$link = Link::getBundleLink($id_bundle);
		$disponibilita = "disponibile";
		if($prezzo < 499.00) {
			$spedizione = 8.90+((8.90*22)/100);
		}
		else {
			$spedizione = 8.90+((8.90*22)/100);
		}
			
		$spedizione = "IT:::".$spedizione." EUR";
		
		$id_immagine = Db::getInstance()->getValue("SELECT id_image FROM bundle_image WHERE id_bundle = ".$id_bundle."");
		if(!$id_immagine) {
			$immagine = "";
		}
		else {
			$immagine = "http://www.ezdirect.it/img/b/".$id_immagine."-".$id_bundle."-medium.jpg";
		}
		
		$r_immagine = Db::getInstance()->getValue('SELECT id_image FROM image WHERE id_product = '.$bundle['father'].' AND cover = 1');
		$immagine = "http://www.ezdirect.it/img/p/".$bundle['father']."-".$r_immagine."-medium.jpg";
		
		$condizione = "nuovo";
		$categoriagoogle = "Dispositivi elettronici > Comunicazioni";
		$quantita = 999;
		$riga_prodotti .= "$id\t$condizione\t$disponibilita\t$link\t$nome\t$categoria\t$marca\t$descrizione\t$prezzo\t$categoriagoogle\t$immagine\t$quantita\t$codice\t$spedizione\n";
	}
	
@fwrite($file_txt,$riga_prodotti);

echo "Listino prodotti per Google Shopping esportato con successo! 
<br /><a href='../listini/googleshopping.txt' target='_blank'>Salva o apri il listino esportato</a><br />
<a href='index.php?tab=ExportComp&token=".$this->token."'>Torna indietro</a>
";

}

else if(isset($_POST['submit']) && $_POST['submit'] == 'Amazon') {

Db::getInstance()->executeS("UPDATE product SET comparaprezzi_check = 0, amazon = 0");
Db::getInstance()->executeS("UPDATE category SET comparaprezzi_check = 0, amazon = 0");
	
if(isset($_POST['categoria'])) {
 $categorie = $_POST['categoria'];
    $numero_categorie = count($categorie);

    for ($i=0; $i<$numero_categorie; $i++) {

Db::getInstance()->executeS("UPDATE category SET comparaprezzi_check = 1, amazon = 1 WHERE id_category = $categorie[$i]");




}
}

	if(isset($_POST['prodotto'])) {
		$prodotti = $_POST['prodotto'];
		$numero_prodotti = count($prodotti);

		foreach($_POST['prodotto'] as $prodotto) {

		
			$query = "SELECT product_lang.name AS nome, product.active AS attivo, product.price AS prezzo, imaget.id_image AS immagine, product.id_product AS id, category_lang.name AS categoria, product_lang.description_short AS descrizione, product.reference AS codice, manufacturer.name AS produttore, product.supplier_reference AS sku, product.ean13 AS ean FROM product JOIN product_lang ON product.id_product = product_lang.id_product JOIN category_lang ON product.id_category_default = category_lang.id_category JOIN manufacturer ON product.id_manufacturer = manufacturer.id_manufacturer  JOIN (SELECT * FROM image WHERE cover = 1) imaget ON product.id_product = imaget.id_product  WHERE product.id_product = $prodotto AND product.id_category_default != 248 AND product.id_category_default != 119 AND product.id_category_default != 249 AND product.id_category_default != 256 AND product_lang.id_lang = 5 AND category_lang.id_lang = 5 GROUP BY product.id_product ORDER BY product_lang.name ASC";
			
			$rows = Db::getInstance()->executeS($query);
	foreach($rows as $row) {
				Db::getInstance()->executeS("UPDATE product SET comparaprezzi_check = 1, amazon = 1 WHERE id_product = $row[id]");
			}
		}
echo "Lista prodotti da esportare su Amazon aggiornata con successo. Il feed sar&agrave; aggiornato con il prossimo aggiornamento automatico. 
<br />
<a href='index.php?tab=ExportComp&token=".$this->token."'>Torna indietro</a>
";

}
}


else {
echo "
<form name='esportatore_selezione' method='post'>
Seleziona il comparatore su cui vuoi lavorare:<br /><br />
<input type='submit' value='Trovaprezzi' name='selezione' />
<input type='submit' value='Google Shopping' name='selezione' />
<input type='submit' value='Amazon' name='selezione' />
<br /><br />
<input type='submit' value='Tutti (no Amazon)' name='selezione' /> 
</form>";
}


if(isset($_POST['selezione'])) {

echo "
<script type='text/javascript'>
function select(a) {
    var theForm = document.esportatore;
    for (i=0; i<theForm.elements.length; i++) {
        if (theForm.elements[i].name=='prodotto[]')
            theForm.elements[i].checked = a;
    }
}

function select_cat(a) {
    var theForm = document.esportatore;
    for (i=0; i<theForm.elements.length; i++) {
        if (theForm.elements[i].name=='categoria[]')
            theForm.elements[i].checked = a;
    }
}

function select_bundle(a) {
    var theForm = document.esportatore;
    for (i=0; i<theForm.elements.length; i++) {
        if (theForm.elements[i].name=='bundle[]')
            theForm.elements[i].checked = a;
    }
}
</script>
<script type='text/javascript'>
$(document).ready(function(){
    $(':checkbox').change(function(){
        if($('.cat-class').attr('checked'))
        {
          
        }
        else
        {
           
        }
    });
});
</script>
<br /><br />
<form name='esportatore' method='post'>";

if($_POST['selezione'] == 'Tutti (no Amazon)') {
}
else {
echo "
<div class='prodotti' style='float:left;
margin-left:15px;
border:1px solid black;
margin-bottom:10px;
height:500px;
width:350px;
overflow-y:scroll;'>
<strong>Singoli prodotti</strong><br /><br />";

$query = "SELECT * FROM product JOIN product_lang ON product.id_product = product_lang.id_product WHERE product_lang.id_lang = 5 AND product.active = 1  AND product_lang.name != '' AND product.fuori_produzione = 0 AND product.id_category_default != 248 AND product.id_category_default != 119 AND product.id_category_default != 249 AND product.id_category_default != 256 ORDER BY product_lang.name ASC";
$rows = Db::getInstance()->executeS($query);
	foreach($rows as $row) {

if(isset($_POST['selezione']) && $_POST['selezione'] == 'Trovaprezzi') {
$compscelto = 'trovaprezzi';
$input = '<input type="submit" value="Trovaprezzi" name="submit" />';
}
else if(isset($_POST['selezione']) && $_POST['selezione'] == 'Google Shopping') {
$compscelto = 'google_shopping';
$input = '<input type="submit" value="Google Shopping" name="submit" />';
}
else if(isset($_POST['selezione']) && $_POST['selezione'] == 'Amazon') {
$compscelto = 'amazon';
$input = '<input type="submit" value="Amazon" name="submit" />';
}
else if(isset($_POST['selezione']) && $_POST['selezione'] == 'Tutti (no Amazon)') {
$compscelto = 'comparaprezzi_check';
$input = '<input type="submit" value="Tutti (no Amazon)" name="submit" />';
}


 

echo "<input type='checkbox' name='prodotto[]'"; check($row[$compscelto]); echo "value='".$row['id_product']."' />".$row['name']."<br />";

}

echo "</div>
<div class='prodotti' style='float:left;
margin-left:15px;
border:1px solid black;
margin-bottom:10px;
height:500px;
width:350px;
overflow-y:scroll;'>
<strong>Categorie</strong><br /><br />";

$query = "SELECT * FROM category JOIN category_lang ON category.id_category = category_lang.id_category WHERE category_lang.id_lang = 5 ORDER BY category_lang.name ASC";
$rows = Db::getInstance()->executeS($query);
	foreach($rows as $row) {


echo "<input type='checkbox' class='cat-class' name='categoria[]' "; check($row[$compscelto]); echo "value='".$row['id_category']."' />".$row['name']."<br />";


}


echo '</div>
<div style="clear:both"></div>
<a href="javascript:select(1)">Seleziona tutti i prodotti</a> |
<a href="javascript:select(0)">Deseleziona tutti i prodotti</a> |
<a href="javascript:select_cat(1)">Seleziona tutte le categorie</a> |
<a href="javascript:select_cat(0)">Deseleziona tutte le categorie</a>
<br /><br />';

if($compscelto == 'amazon')
{
}
else
{

	echo "<div class='bundle' style='float:left;
	margin-left:15px;
	border:1px solid black;
	margin-bottom:10px;
	height:250px;
	width:350px;
	overflow-y:scroll;'>
	<strong>Bundle</strong><br /><br />";


	$query = "SELECT * FROM bundle WHERE active = 1 ORDER BY bundle_name ASC";
	$rows = Db::getInstance()->executeS($query);
	foreach($rows as $row) {



	$is_image = Bundle::checkIfBundleHasImage($row['id_bundle']);

	echo "<input type='checkbox' name='bundle[]'"; check($row[$compscelto]); echo "value='".$row['id_bundle']."' />".($is_image == 0 ? "<strong>" : "")."".(empty($row['bundle_name']) ? Bundle::getBundleNames($row['id_bundle'], 5) : $row['bundle_name'])."".($is_image == 0 ? "</strong>" : "")."<br />";

	}

	echo "</div>";
	
echo	"<div style='clear:both'>";
echo '<a href="javascript:select_bundle(1)">Seleziona tutti i bundle</a> | <a href="javascript:select_bundle(0)">Deseleziona tutti i bundle</a><br />';
}
}
echo	"<div style='clear:both'>";

if(isset($_POST['selezione']) && $_POST['selezione'] == 'Trovaprezzi') {
$compscelto = 'trovaprezzi';
$input = '<input type="submit" value="Trovaprezzi" name="submit" />';
}
else if(isset($_POST['selezione']) && $_POST['selezione'] == 'Google Shopping') {
$compscelto = 'google_shopping';
$input = '<input type="submit" value="Google Shopping" name="submit" />';
}
else if(isset($_POST['selezione']) && $_POST['selezione'] == 'Amazon') {
$compscelto = 'amazon';
$input = '<input type="submit" value="Amazon" name="submit" />';
}
else if(isset($_POST['selezione']) && $_POST['selezione'] == 'Tutti (no Amazon)') {
$compscelto = 'comparaprezzi_check';
$input = '<input type="submit" value="Tutti (no Amazon)" name="submit" />';
}


echo '<br /><br />Esporta per: ';
echo $input;

echo '<!-- <input type="submit" value="Shopmania" name="submit" /> -->








</form>';

			}
			
	}
	

}


