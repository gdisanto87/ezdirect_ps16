<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14632 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminContratti extends AdminTab
{
		public function __construct()
	{
		
		global $cookie;
			
	 	$this->table = 'contratto_assistenza';
	 	$this->lang = false;
	 	$this->edit = false;
		$this->delete = false;
	 	$this->view = true;
		$this->requiredDatabase = true;

		$this->_select = '
		(CASE c.is_company
		WHEN 1
		THEN c.company
		ELSE
		CONCAT(c.firstname," ",c.lastname)
		END
		) cliente,
		
		(CASE WHEN a.data_fine < DATE(NOW()) THEN 3
		ELSE a.status
		END) status_enhanced,
		
		a.id_contratto as id_contratto_assistenza,
		
		(CASE a.tipo
		WHEN 1
		THEN "Base"
		WHEN 2
		THEN "Top"
		WHEN 3
		THEN "Gold"
		WHEN 4
		THEN "Teleassistenza"
		WHEN 5
		THEN "Noleggio"
		END)
		tipo_contratto,
		
		(CASE c.is_company
		WHEN 1
		THEN c.vat_number
		WHEN 0
		THEN c.tax_code
		ELSE c.tax_code
		END
		) piva_cf
		';
		
		
		$this->_join = 'JOIN customer c ON a.id_customer = c.id_customer';
		
		
		$statusArray = array(
			'0' => $this->l('Attivo'),
			'1' => $this->l('Sospeso'),
			'2' => $this->l('Cancellato'),
			'4' => $this->l('Scaduto')
		);
		
		$imagesArray = array(
			'4' => 'status_red.gif',
			'3' => 'status_red.gif',
			'2' => 'status_red.gif',
			'0' => 'status_green.gif',
			'1' => 'status_orange.gif'
		);
		
		$tipiArray = array(
			1 => 'Base',
			2 => 'Top',
			3 => 'Gold',
			4 => 'Teleassistenza',
			5 => 'Noleggio'
		);
		
 		$this->fieldsDisplay = array(
		'id_contratto' => array('title' => $this->l('ID'), 'align' => 'center', 'font-size' =>'12px', 'width' => 25, 'widthColumn' => 25,  'tmpTableFilter' => true),
		
		'cliente' => array('title' => $this->l('Cliente'), 'font-size' =>'14px',  'width' => 200,  'widthColumn' => 200, 'tmpTableFilter' => true,  'tmpTableFilter' => true),
		'piva_cf' => array('title' => $this->l('PI/CF'), 'font-size' =>'14px',  'width' => 70, 'widthColumn' => 90,  'tmpTableFilter' => true,  'tmpTableFilter' => true),
		'rif_fattura'  => array('title' => $this->l('Rif. Fattura'), 'font-size' =>'14px',  'width' => 70, 'widthColumn' => 90,  'tmpTableFilter' => true,  'tmpTableFilter' => true),
		
		'status_enhanced' => array('title' => $this->l('Stato'), 'width' =>25,  'widthColumn' => 25, 'type' => 'select', 'select' => $statusArray, 'icon' => $imagesArray, 'align' => 'center', 'filter_key' => 'status', 'tmpTableFilter' => true, 'filter_type' => 'string'),
		
		'tipo_contratto' => array('title' => $this->l('Tipo'), 'width' =>25,  'widthColumn' => 25, 'type' => 'select', 'select' => $tipiArray, 'align' => 'center', 'filter_key' => 'tipo', 'tmpTableFilter' => true),
		
		'prezzo' => array('title' => $this->l('Prezzo'), 'width' => 80, 'align' => 'right', 'prefix' => '<b>', 'suffix' => '</b>', 'price' => true, 'widthColumn' => 80,  'tmpTableFilter' => true),
		
		
		
		'data_inizio' => array('title' => $this->l('Data inizio'), 'width' => 30,  'widthColumn' => 30, 'type' => 'date',  'tmpTableFilter' => true),
		
		'data_fine' => array('title' => $this->l('Scadenza'), 'width' => 30,  'widthColumn' => 30, 'type' => 'date',  'tmpTableFilter' => true),

		//'connect' => array('title' => $this->l('Connection'), 'width' => 60, 'type' => 'datetime', 'search' => false)
		);
		

		

		parent::__construct();
		
		/*
		if (isset($_GET['delete'.$this->table]) ) {
		
			Db::getInstance()->executeS('DELETE FROM bdl WHERE id_bdl = '.Tools::getValue('id_bdl'));
		
			Tools::redirectAdmin("index.php?tab=AdminBDL&conf=4&token=".$this->token);
		}
		*/
	
	}

	
	public function display()
	{	
			global $cookie, $currentIndex;
			$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
			
			echo "<h1>Contratti assistenza e noleggi</h1>";
			
		echo "<div id='mostra_legenda' style='display:block; float:left; margin-left:15px; margin-top:-5px'>
			<table>

			
			<tr><td><strong>Status</strong></td><td>  </td><td>  ROSSO: cancellato / disdetto / scaduto.<br />GIALLO: sospeso.<br />VERDE: attivo.</td></tr></table></div><div style='clear:both'></div>";
			
			
			if (isset($_GET['view'.$this->table])) {
				
				$id_customer = Db::getInstance()->getValue('SELECT id_customer FROM contratto_assistenza WHERE id_contratto = '.Tools::getValue('id_contratto_assistenza'));
				$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
				
				Tools::redirectAdmin("index.php?tab=AdminCustomers&id_customer=".$id_customer."&viewcustomer&id_contratto=".Tools::getValue('id_contratto_assistenza')."&token=".$tokenCustomers."&tab-container-1=13");
				
			}
			
			
			
		else
		{
			$this->getList((int)($cookie->id_lang), !Tools::getValue($this->table.'Orderby') ? 'data_fine' : NULL, !Tools::getValue($this->table.'Orderway') ? 'DESC' : NULL);
			$currency = new Currency((int)(Configuration::get('PS_CURRENCY_DEFAULT')));
			$this->displayListHeader();
			$this->displayListContent();
		//	$this->displayListFooter();
			$this->displayListPagination($this->token);
		}
		
		
			
	}
		
}

