<?php

	include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

	class StatsTarget extends AdminPreferences {
	
	
		public function display()
		{
			global $cookie;
			global $currentIndex;
		
			echo "<h1>Target</h1>";
			
			echo "<style type='text/css'>
			#target-table tr td {
			text-align:right
			}
			</style>";
			echo "Seleziona l'anno di riferimento: <br /><br />";
			echo "<div style='float:left'>";
			echo "<form name='selezione-anno' method='post'>";
			
			echo "<input type='hidden' name='costruttore-di-riferimento' value='".(isset($_POST['costruttore-di-riferimento']) ? $_POST['costruttore-di-riferimento'] : $costruttore)."'>";
			
			echo "<select name='anno-di-riferimento' style='height:25px'>";
			
			$anno_attuale = date("Y");
			
			for($i='2020'; $i>2013; $i--) {
			
				echo "<option value='$i' "; 
				if(isset($_POST['anno-di-riferimento']) && $_POST['anno-di-riferimento'] == $i) { echo "selected='selected' "; }  else if (!isset($_POST['anno-di-riferimento']) && $i == $anno_attuale) { echo "selected='selected'"; } else { } 
				echo ">$i</option>";
			
			}
			
			echo "</select>";
			echo "<input type='submit' name='seleziona-anno' value='Seleziona' class='button' style='margin-top:-4px; margin-left:5px; cursor:pointer' />";
			echo "</form>";
			echo "</div>";
			
			if(isset($_POST['anno-di-riferimento'])) {
			
				$anno_di_riferimento = $_POST['anno-di-riferimento'];
			
			}
			
			else {
			
				$anno_di_riferimento = date("Y");
			
			}

			 
			 $query_totale = 'SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate)+IFNULL((SELECT SUM(importo) FROM bdl WHERE rif_ordine = 0 AND importo > 0 AND invio_contabilita > 0 AND year(`date_add`) = '.($anno_di_riferimento).'';

			$query_totale_2 = ' ),0) as total
			FROM `'._DB_PREFIX_.'orders` o
			LEFT JOIN order_history oh ON (oh.id_order_state = 6 or oh.id_order_state = 35) AND oh.id_order = o.id_order
			LEFT JOIN `'._DB_PREFIX_.'order_detail` od ON o.id_order = od.id_order
			WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire" OR o.module = "other_payment"))) AND oh.id_order_state IS NULL AND year( o.date_add ) = '.($anno_di_riferimento) .'';
			 
	
			$query_margine = 'SELECT SUM(((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) - ((CASE WHEN (od.wholesale_price = 0 AND od.no_acq = 0) THEN p.wholesale_price ELSE od.wholesale_price END)*od.product_quantity))+IFNULL(((SELECT SUM(importo) FROM bdl WHERE rif_ordine = 0 AND importo > 0 AND invio_contabilita > 0 AND year(`date_add`) = '.($anno_di_riferimento);
			
			$query_margine_2 = ' )*0.30),0) as margine,
			SUM((CASE WHEN (od.wholesale_price = 0 AND od.no_acq = 0) THEN p.wholesale_price ELSE od.wholesale_price END)*od.product_quantity) as tot_acquisti
			FROM `'._DB_PREFIX_.'orders` o
			LEFT JOIN `'._DB_PREFIX_.'order_detail` od ON o.id_order = od.id_order
			LEFT JOIN order_history oh ON (oh.id_order_state = 6 or oh.id_order_state = 35) AND oh.id_order = o.id_order
			LEFT JOIN `'._DB_PREFIX_.'product` p ON p.id_product = od.product_id
			LEFT JOIN `'._DB_PREFIX_.'customer` c ON o.id_customer = c.id_customer 
			'.((int)$cookie->stats_id_zone ? $join : '').'
			WHERE (o.valid = 1 OR (o.valid = 0 AND (o.module = "bankwire" OR o.module = "other_payment"))) AND oh.id_order_state IS NULL AND year( o.date_add ) = '.($anno_di_riferimento) .'';
			 
			$where_totale = "";
			$where_margine = "";
			
			echo "<div style='float:left; margin-left:20px'>";
			echo '<form name="obiettivi-annuali" method="post" action="ajax.php" id="obiettivi-annuali">';
			echo '<script type="text/javascript">
			
			function aggiornaTarget(tipo,anno,riga) 
			{
				venduto = document.getElementById(tipo+"-"+riga+"-"+anno+"-vend").innerHTML;
				venduto = parseFloat(venduto.replace(/\s/g, "").replace(/\./g,"").replace(",", "."));
				
				target = document.getElementById(tipo+"-"+riga+"-"+anno+"-target").value;
				target = parseFloat(target.replace(/\s/g, "").replace(/\./g,"").replace(",", "."));
				
				var qtot = 
				parseFloat((document.getElementById(tipo+"-q1-"+anno+"-target").value).replace(/\s/g, "").replace(/\./g,"").replace(",", ".")) + 
				parseFloat((document.getElementById(tipo+"-q2-"+anno+"-target").value).replace(/\s/g, "").replace(/\./g,"").replace(",", ".")) + 
				parseFloat((document.getElementById(tipo+"-q3-"+anno+"-target").value).replace(/\s/g, "").replace(/\./g,"").replace(",", ".")) + 
				parseFloat((document.getElementById(tipo+"-q4-"+anno+"-target").value).replace(/\s/g, "").replace(/\./g,"").replace(",", ".")) 
				;
				
				var qtotfloat = qtot;
				
				qtot = qtot.toFixed(2);
				qtot = qtot.toString();				
				qtot = qtot.replace(".",",");
				
				document.getElementById(tipo+"-totale-"+anno+"-target").value = qtot;
				
				risultato = venduto*100/target;
				risultato = risultato.toFixed(2);
				risultato = risultato.toString();				
				risultato = risultato.replace(".",",");
				
				var vendutotot = document.getElementById(tipo+"-tot-"+anno+"-vend").innerHTML;
				
				vendutotot = vendutotot.replace(/<\/?[^>]+(>|$)/g, "");
				
				vendutotot = parseFloat(vendutotot.replace(/\s/g, "").replace(/\./g,"").replace(",", "."));
				
				risultatotot = vendutotot*100/qtotfloat;
				risultatotot = risultatotot.toFixed(2);
				risultatotot = risultatotot.toString();				
				risultatotot = risultatotot.replace(".",",");
				
				document.getElementById(tipo+"-"+riga+"-"+anno+"-perc").innerHTML = risultato;
			
				document.getElementById(tipo+"-tot-"+anno+"-perc").innerHTML = risultatotot;
			}
			</script>
			';
			
			
			//anno intero
			$query_totale_d = Db::getInstance()->getRow($query_totale.$query_totale_2.$where_totale);
			$query_margine_d = Db::getInstance()->getRow($query_margine.$query_margine_2.$where_totale);
			
			echo '<table id="target-table" class="table">
			<tr><th>Valore</th><th>Target</th><th>Venduto</th><th>% raggiunto</th><th>Margine Lordo</th></tr>';
			
			$obiettivi_anno_q = Db::getInstance()->getRow('SELECT * FROM trgt WHERE anno = "'.$anno_di_riferimento.'" AND descrizione = "Anno"');
			
			$obiettivi_anno = unserialize($obiettivi_anno_q['configurazione']);
	
			echo '<tr><td style="text-align:left">Anno</td>
			<td><input onkeyup="aggiornaTarget(\'anno\',\''.$anno_di_riferimento.'\',\'anno\')" type="text" style="width:80px; text-align:right" name="totale" id="anno-anno-'.$anno_di_riferimento.'-target" value="'.number_format($obiettivi_anno['totale'],2,",",".").'" /></td><td id="anno-anno-'.$anno_di_riferimento.'-vend">'.number_format($query_totale_d['total'],2,",",".").'</td>
			<td id="anno-anno-'.$anno_di_riferimento.'-perc">'.number_format(((($query_totale_d['total']*100)/$obiettivi_anno['totale'])),2,",",".").'%</td>
			<td id="anno-anno-'.$anno_di_riferimento.'-ml">'.number_format($query_margine_d['margine'],2,",",".").'</td></tr>';
			
			$tot_t = number_format($query_totale_d['total'],2,",",".");
			$totale_anno = $query_totale_d['total'];
			$tot_d = number_format(((($query_totale_d['total']*100)/$obiettivi_anno['totale'])),2,",",".");
			$tot_m = number_format($query_margine_d['margine'],2,",",".");
			
			//q1
			$where_totale = ' AND ( month(o.date_add) = 1 OR  month(o.date_add) = 2 OR month(o.date_add) = 3)';
			$where_margine = ' AND ( month(o.date_add) = 1 OR  month(o.date_add) = 2 OR month(o.date_add) = 3)';
			$where_bdl = ' AND ( month(date_add) = 1 OR  month(date_add) = 2 OR month(date_add) = 3)';
			$query_totale_d = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
			$query_margine_d = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
			
			echo '<tr><td style="text-align:left">Q1</td>
			<td><input onkeyup="aggiornaTarget(\'anno\',\''.$anno_di_riferimento.'\',\'q1\')" type="text" style="width:80px; text-align:right" name="q1" id="anno-q1-'.$anno_di_riferimento.'-target" value="'.number_format($obiettivi_anno['q1'],2,",",".").'" /></td><td id="anno-q1-'.$anno_di_riferimento.'-vend">'.number_format($query_totale_d['total'],2,",",".").'</td>
			<td id="anno-q1-'.$anno_di_riferimento.'-perc">'.number_format(((($query_totale_d['total']*100)/$obiettivi_anno['q1'])),2,",",".").'%</td>
			<td id="anno-q1-'.$anno_di_riferimento.'-ml">'.number_format($query_margine_d['margine'],2,",",".").'</td></tr>';
			
			//q2
			$where_totale = ' AND ( month(o.date_add) = 4 OR  month(o.date_add) = 5 OR month(o.date_add) = 6)';
			$where_margine = ' AND ( month(o.date_add) = 4 OR  month(o.date_add) = 5 OR month(o.date_add) = 6)';
			$where_bdl = ' AND ( month(date_add) = 4 OR  month(date_add) = 5 OR month(date_add) = 6)';
			$query_totale_d = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
			$query_margine_d = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
			
			echo '<tr><td style="text-align:left">Q2</td>
			<td><input onkeyup="aggiornaTarget(\'anno\',\''.$anno_di_riferimento.'\',\'q2\')" type="text" style="width:80px; text-align:right" name="q2"  id="anno-q2-'.$anno_di_riferimento.'-target" value="'.number_format($obiettivi_anno['q2'],2,",",".").'" /></td><td id="anno-q2-'.$anno_di_riferimento.'-vend">'.number_format($query_totale_d['total'],2,",",".").'</td>
			<td id="anno-q2-'.$anno_di_riferimento.'-perc">'.number_format(((($query_totale_d['total']*100)/$obiettivi_anno['q2'])),2,",",".").'%</td>
			<td id="anno-q2-'.$anno_di_riferimento.'-ml">'.number_format($query_margine_d['margine'],2,",",".").'</td></tr>';
			
			//q3
			$where_totale = ' AND ( month(o.date_add) = 7 OR  month(o.date_add) = 8 OR month(o.date_add) = 9)';
			$where_margine = ' AND ( month(o.date_add) = 7 OR  month(o.date_add) = 8 OR month(o.date_add) = 9)';
			$where_bdl = ' AND ( month(date_add) = 7 OR  month(date_add) = 8 OR month(date_add) = 9)';
			$query_totale_d = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
			$query_margine_d = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
			
			echo '<tr><td style="text-align:left">Q3</td>
			<td><input onkeyup="aggiornaTarget(\'anno\',\''.$anno_di_riferimento.'\',\'q3\')" type="text" style="width:80px; text-align:right" name="q3"  id="anno-q3-'.$anno_di_riferimento.'-target" value="'.number_format($obiettivi_anno['q3'],2,",",".").'" /></td><td id="anno-q3-'.$anno_di_riferimento.'-vend">'.number_format($query_totale_d['total'],2,",",".").'</td>
			<td id="anno-q3-'.$anno_di_riferimento.'-perc">'.number_format(((($query_totale_d['total']*100)/$obiettivi_anno['q3'])),2,",",".").'%</td>
			<td id="anno-q3-'.$anno_di_riferimento.'-ml">'.number_format($query_margine_d['margine'],2,",",".").'</td></tr>';
			
			//q4
			$where_totale = ' AND ( month(o.date_add) = 10 OR  month(o.date_add) = 11 OR month(o.date_add) = 12)';
			$where_margine = ' AND ( month(o.date_add) = 10 OR  month(o.date_add) = 11 OR month(o.date_add) = 12)';
			$where_bdl = ' AND ( month(date_add) = 10 OR  month(date_add) = 11 OR month(date_add) = 12)';
			$query_totale_d = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
			$query_margine_d = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
			
			echo '<tr><td style="text-align:left">Q4</td>
			<td><input onkeyup="aggiornaTarget(\'anno\',\''.$anno_di_riferimento.'\',\'q4\')" type="text" style="width:80px; text-align:right" name="q4"  id="anno-q4-'.$anno_di_riferimento.'-target" value="'.number_format($obiettivi_anno['q4'],2,",",".").'" /></td><td id="anno-q4-'.$anno_di_riferimento.'-vend">'.number_format($query_totale_d['total'],2,",",".").'</td>
			<td id="anno-q4-'.$anno_di_riferimento.'-perc">'.number_format(((($query_totale_d['total']*100)/$obiettivi_anno['q4'])),2,",",".").'%</td>
			<td id="anno-q4-'.$anno_di_riferimento.'-ml">'.number_format($query_margine_d['margine'],2,",",".").'</td></tr>';	
			
			// qtot
			echo '<tr><td style="text-align:left"><strong>Q.TOT. 1+2+3+4</strong></td>
			<td><strong><input type="text" style="width:80px; text-align:right" id="anno-totale-'.$anno_di_riferimento.'-target" readonly="readonly" value="'.number_format($obiettivi_anno['q1']+$obiettivi_anno['q2']+$obiettivi_anno['q3']+$obiettivi_anno['q4'],2,",",".").'" /></strong></td><td id="anno-tot-'.$anno_di_riferimento.'-vend"><strong>'.$tot_t.'</strong></td>
			<td id="anno-tot-'.$anno_di_riferimento.'-perc"><strong>'.number_format(((($totale_anno*100)/($obiettivi_anno['q1']+$obiettivi_anno['q2']+$obiettivi_anno['q3']+$obiettivi_anno['q4']))),2,",",".").'%</strong></td>
			<td id="anno-tot-'.$anno_di_riferimento.'-ml"><strong>'.$tot_m.'</strong></td></tr>';
			
			echo '</table>';
			echo '<br />';
			echo "<input type='hidden' name='descrizione' id='anno-descrizione' value='Anno' />";
			echo "<input type='hidden' name='anno' id='anno-anno' value='".$anno_di_riferimento."' />";
			echo "<input type='submit' class='button' style='cursor:pointer' value='Salva' name='salva' id='submitTrgt' onclick='saveTrgt(\"anno\"); return false;' />";
			echo "<input type='submit' class='button' style='cursor:pointer; margin-left:15px' value='Esporta in Excel' id='esporta-target-excel' name='esporta-target-excel' />";
				echo "<div id='save_anno_anno'></div>";
			echo "</form>";
			echo "</div>";
			echo "<div style='clear:both'></div>";
			echo "<br /><br />";
			
			echo "Seleziona un costruttore da compilare:  <br />";
			echo "<div style='float:left'>";
			echo "<form name='selezione-costruttore' method='post'>";
			echo "<input type='hidden' name='seleziona-anno' value='y' />";
			echo "<input type='hidden' name='anno-di-riferimento' value='".$anno_di_riferimento."' />";
			echo "<select name='costruttore-di-riferimento' style='height:25px'>";
			
			$costruttori = Db::getInstance()->executeS('SELECT * FROM manufacturer ORDER BY name ASC');
			
			foreach ($costruttori as $costruttore) 
			{
				$gia = Db::getInstance()->getValue('SELECT id FROM trgt WHERE descrizione = "m-'.$costruttore['id_manufacturer'].'-'.$anno_di_riferimento.'"');
				
				if($gia > 0)
				{ }
				else
				{	
					echo "<option value='".$costruttore['id_manufacturer']."' "; 
					if(isset($_POST['costruttore-di-riferimento']) && $_POST['costruttore-di-riferimento'] == $costruttore['id_manufacturer']) { echo "selected='selected' "; } else { } 
					echo ">".$costruttore['name']."</option>";
				}
			}
			
			echo "</select>";
			echo "<input type='submit' name='seleziona-costruttore' value='Seleziona' class='button' style='margin-top:-4px; margin-left:5px; cursor:pointer' />";
			echo "</form>";
			echo "</div>";
			
			if(isset($_POST['costruttore-di-riferimento'])) {
			
				$costruttore = $_POST['costruttore-di-riferimento'];
			
			}
			
			else 
			{
				$costruttore = '';
			}
			
			
			$where_totale = "";
			$where_margine = "";
			
			
			
			if(isset($_POST['costruttore-di-riferimento']) && !empty($_POST['costruttore-di-riferimento'])) {
				//anno intero
				echo "<div style='float:left; margin-left:20px'>";
				echo '<form name="obiettivi-annuali" method="post" action="ajax.php" onSubmit="saveTrgt(\'m-'.$costruttore.'\'); return false;" id="obiettivi-annuali">';
				
				if($costruttore == 105)
					$where_bdl_c = '';
				else
					$where_bdl_c = ' AND id_bdl = "XXX"';
			
				$where_totale = ' AND od.manufacturer_id = '.$costruttore.' ';
				$where_margine = ' AND od.manufacturer_id = '.$costruttore.' ';
			
				$query_totale_d = Db::getInstance()->getRow($query_totale.$where_bdl_c.$query_totale_2.$where_totale);
				$query_margine_d = Db::getInstance()->getRow($query_margine.$where_bdl_c.$query_margine_2.$where_totale);
				
				echo '<table id="target-table" class="table">
				<tr><th>Valore</th><th>Target</th><th>Venduto</th><th>% raggiunto</th><th>Margine Lordo</th></tr>';
				
				$obiettivi_anno_q = Db::getInstance()->getRow('SELECT * FROM trgt WHERE anno = "'.$anno_di_riferimento.'" AND descrizione = "m-'.$costruttore.'-'.$anno_di_riferimento.'"');
				
			
				$obiettivi_anno = unserialize($obiettivi_anno_q['configurazione']);
		
				echo '<tr><td style="text-align:left">Anno</td>
				<td><input onkeyup="aggiornaTarget(\'m-'.$costruttore.'\',\''.$anno_di_riferimento.'\',\'anno\')" type="text" style="width:80px; text-align:right" name="totale" id="m-'.$costruttore.'-anno-'.$anno_di_riferimento.'-target" value="'.number_format($obiettivi_anno['totale'],2,",",".").'" /></td><td id="m-'.$costruttore.'-anno-'.$anno_di_riferimento.'-vend">'.number_format($query_totale_d['total'],2,",",".").'</td>
				<td id="m-'.$costruttore.'-anno-'.$anno_di_riferimento.'-perc">'.number_format(((($query_totale_d['total']*100)/$obiettivi_anno['totale'])),2,",",".").'%</td>
				<td id="m-'.$costruttore.'-anno-'.$anno_di_riferimento.'-ml">'.number_format($query_margine_d['margine'],2,",",".").'</td></tr>';
				
				$tot_t = number_format($query_totale_d['total'],2,",",".");
				$totale_anno = $query_totale_d['total'];
				$tot_d = number_format(((($query_totale_d['total']*100)/$obiettivi_anno['totale'])),2,",",".");
				$tot_m = number_format($query_margine_d['margine'],2,",",".");
				
				//q1
				$where_totale = ' AND od.manufacturer_id = '.$costruttore.' AND ( month(o.date_add) = 1 OR  month(o.date_add) = 2 OR month(o.date_add) = 3)';
				$where_margine = ' AND od.manufacturer_id = '.$costruttore.' AND ( month(o.date_add) = 1 OR  month(o.date_add) = 2 OR month(o.date_add) = 3)';
				$where_bdl = $where_bdl_c.' AND ( month(date_add) = 1 OR  month(date_add) = 2 OR month(date_add) = 3)';
				$query_totale_d = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
				$query_margine_d = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
				
				echo '<tr><td style="text-align:left">Q1</td>
				<td><input onkeyup="aggiornaTarget(\'m-'.$costruttore.'\',\''.$anno_di_riferimento.'\',\'q1\')" type="text" style="width:80px; text-align:right" name="q1" id="m-'.$costruttore.'-q1-'.$anno_di_riferimento.'-target" value="'.number_format($obiettivi_anno['q1'],2,",",".").'" /></td><td id="m-'.$costruttore.'-q1-'.$anno_di_riferimento.'-vend">'.number_format($query_totale_d['total'],2,",",".").'</td>
				<td id="m-'.$costruttore.'-q1-'.$anno_di_riferimento.'-perc">'.number_format(((($query_totale_d['total']*100)/$obiettivi_anno['q1'])),2,",",".").'%</td>
				<td id="m-'.$costruttore.'-q1-'.$anno_di_riferimento.'-ml">'.number_format($query_margine_d['margine'],2,",",".").'</td></tr>';
				
				//q2
				$where_totale = ' AND od.manufacturer_id = '.$costruttore.' AND ( month(o.date_add) = 4 OR  month(o.date_add) = 5 OR month(o.date_add) = 6)';
				$where_margine = ' AND od.manufacturer_id = '.$costruttore.' AND ( month(o.date_add) = 4 OR  month(o.date_add) = 5 OR month(o.date_add) = 6)';
				$where_bdl = $where_bdl_c.' AND ( month(date_add) = 4 OR  month(date_add) = 5 OR month(date_add) = 6)';
				$query_totale_d = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
				$query_margine_d = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
				
				echo '<tr><td style="text-align:left">Q2</td>
				<td><input onkeyup="aggiornaTarget(\'m-'.$costruttore.'\',\''.$anno_di_riferimento.'\',\'q2\')" type="text" style="width:80px; text-align:right" name="q2"  id="m-'.$costruttore.'-q2-'.$anno_di_riferimento.'-target" value="'.number_format($obiettivi_anno['q2'],2,",",".").'" /></td><td id="m-'.$costruttore.'-q2-'.$anno_di_riferimento.'-vend">'.number_format($query_totale_d['total'],2,",",".").'</td>
				<td id="m-'.$costruttore.'-q2-'.$anno_di_riferimento.'-perc">'.number_format(((($query_totale_d['total']*100)/$obiettivi_anno['q2'])),2,",",".").'%</td>
				<td id="m-'.$costruttore.'-q2-'.$anno_di_riferimento.'-ml">'.number_format($query_margine_d['margine'],2,",",".").'</td></tr>';
				
				//q3
				$where_totale = ' AND od.manufacturer_id = '.$costruttore.' AND ( month(o.date_add) = 7 OR  month(o.date_add) = 8 OR month(o.date_add) = 9)';
				$where_margine = ' AND od.manufacturer_id = '.$costruttore.' AND ( month(o.date_add) = 7 OR  month(o.date_add) = 8 OR month(o.date_add) = 9)';
				$where_bdl = $where_bdl_c.' AND ( month(date_add) = 7 OR  month(date_add) = 8 OR month(date_add) = 9)';
				$query_totale_d = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
				$query_margine_d = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
				
				echo '<tr><td style="text-align:left">Q3</td>
				<td><input onkeyup="aggiornaTarget(\'m-'.$costruttore.'\',\''.$anno_di_riferimento.'\',\'q3\')" type="text" style="width:80px; text-align:right" name="q3"  id="m-'.$costruttore.'-q3-'.$anno_di_riferimento.'-target" value="'.number_format($obiettivi_anno['q3'],2,",",".").'" /></td><td id="m-'.$costruttore.'-q3-'.$anno_di_riferimento.'-vend">'.number_format($query_totale_d['total'],2,",",".").'</td>
				<td id="m-'.$costruttore.'-q3-'.$anno_di_riferimento.'-perc">'.number_format(((($query_totale_d['total']*100)/$obiettivi_anno['q3'])),2,",",".").'%</td>
				<td id="m-'.$costruttore.'-q3-'.$anno_di_riferimento.'-ml">'.number_format($query_margine_d['margine'],2,",",".").'</td></tr>';
				
				//q4
				$where_totale = ' AND od.manufacturer_id = '.$costruttore.' AND ( month(o.date_add) = 10 OR  month(o.date_add) = 11 OR month(o.date_add) = 12)';
				$where_margine = ' AND od.manufacturer_id = '.$costruttore.' AND ( month(o.date_add) = 10 OR  month(o.date_add) = 11 OR month(o.date_add) = 12)';
				$where_bdl = $where_bdl_c.' AND ( month(date_add) = 10 OR  month(date_add) = 11 OR month(date_add) = 12)';
				$query_totale_d = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
				$query_margine_d = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
				
				echo '<tr><td style="text-align:left">Q4</td>
				<td><input onkeyup="aggiornaTarget(\'m-'.$costruttore.'\',\''.$anno_di_riferimento.'\',\'q4\')" type="text" style="width:80px; text-align:right" name="q4"  id="m-'.$costruttore.'-q4-'.$anno_di_riferimento.'-target" value="'.number_format($obiettivi_anno['q4'],2,",",".").'" /></td><td id="m-'.$costruttore.'-q4-'.$anno_di_riferimento.'-vend">'.number_format($query_totale_d['total'],2,",",".").'</td>
				<td id="m-'.$costruttore.'-q4-'.$anno_di_riferimento.'-perc">'.number_format(((($query_totale_d['total']*100)/$obiettivi_anno['q4'])),2,",",".").'%</td>
				<td id="m-'.$costruttore.'-q4-'.$anno_di_riferimento.'-ml">'.number_format($query_margine_d['margine'],2,",",".").'</td></tr>';
				
				// qtot
				echo '<tr><td style="text-align:left"><strong>Q.TOT. 1+2+3+4</strong></td>
				<td><strong><input type="text" style="width:80px; text-align:right"  id="m-'.$costruttore.'-totale-'.$anno_di_riferimento.'-target" value="'.number_format($obiettivi_anno['q4'],2,",",".").'" readonly="readonly" value="'.number_format($obiettivi_anno['q1']+$obiettivi_anno['q2']+$obiettivi_anno['q3']+$obiettivi_anno['q4'],2,",",".").'" /></strong></td><td id="m-'.$costruttore.'-tot-'.$anno_di_riferimento.'-vend"><strong>'.$tot_t.'</strong></td>
				<td id="m-'.$costruttore.'-tot-'.$anno_di_riferimento.'-perc"><strong>'.number_format(((($totale_anno*100)/($obiettivi_anno['q1']+$obiettivi_anno['q2']+$obiettivi_anno['q3']+$obiettivi_anno['q4']))),2,",",".").'%/<strong></td>
				<td id="m-'.$costruttore.'-tot-'.$anno_di_riferimento.'-perc"><strong>'.$tot_m.'</strong></td></tr>';
				
				
				echo '</table>';
				echo '<br />';
				echo "<input type='hidden' name='descrizione' id='m-".$costruttore."-descrizione' value='m-".$costruttore."-".$anno_di_riferimento."' />";
				echo "<input type='hidden' name='anno' id='m-".$costruttore."-anno' value='".$anno_di_riferimento."' />";
				echo "<input type='submit' class='button' style='cursor:pointer' value='Salva' name='salva' />";
				echo "<div id='save_anno_m-".$costruttore."'></div>";
				echo "</form>";
				echo "</div>";
			}
			else {
			}
			
		
			
			echo "<div style='clear:both'></div>";
			
			
			echo '<br /><br />
			<script type="text/javascript">
				function saveTrgt(tipo)
				{	
					$("#save_anno_"+tipo).html("<img src=\"../img/loader.gif\" />").show();
					var descrizione = $("#"+tipo+"-descrizione").val();
					var anno = $("#"+tipo+"-anno").val();
					var totale = $("#"+tipo+"-anno-'.$anno_di_riferimento.'-target").val().replace(/\s/g, "").replace(/\./g,"").replace(",", ".");
					var q1 = $("#"+tipo+"-q1-'.$anno_di_riferimento.'-target").val().replace(/\s/g, "").replace(/\./g,"").replace(",", ".");
					var q2 = $("#"+tipo+"-q2-'.$anno_di_riferimento.'-target").val().replace(/\s/g, "").replace(/\./g,"").replace(",", ".");
					var q3 = $("#"+tipo+"-q3-'.$anno_di_riferimento.'-target").val().replace(/\s/g, "").replace(/\./g,"").replace(",", ".");
					var q4 = $("#"+tipo+"-q4-'.$anno_di_riferimento.'-target").val().replace(/\s/g, "").replace(/\./g,"").replace(",", ".");
					$.ajax({
						type: "POST", 
						url:"ajax.php", 
						async:false, 
						data: {submitTrgt:1,anno:anno,descrizione:descrizione,totale:totale,q1:q1,q2:q2,q3:q3,q4:q4}, 
						success:function (r) {
						
						if (r == "ok")
						{
							$("#save_anno_"+tipo).html("<b style=\"color:green\">'.addslashes($this->l('Configurazione salvata')).'</b>").fadeIn(400);
							/* window.location.href = "http://www.ezdirect.it/'.$currentIndex.'&token='.$this->token.'";	*/
						}
						else if (r == "error:validation")
							$("#save_anno_"+tipo).html("<b style=\"color:red\">'.addslashes($this->l('Errore: configurazione non valida')).'</b>").fadeIn(400);
						else if (r == "error:update")
							$("#save_anno_"+tipo).html("<b style=\"color:red\">'.addslashes($this->l('Errore: impossibile salvare configurazione')).'</b>").fadeIn(400);
						$("#save_anno_"+tipo).fadeOut(3000);
						}
					});
					/* window.location.href = "http://www.ezdirect.it/'.$currentIndex.'&token='.$this->token.'"; */	
					
				}
				
				function delTrgt(tipo)
				{	

					$.post("ajax.php", {deleteTrgt:1,descrizione:tipo}, function (r) {
						
						if (r == "ok")
						{
							document.getElementById(tipo).innerHTML = "";
						}
						else
						{
							alert("ERROR");
						}
					});
					
					
				}
			</script>';
			
			// TUTTI I COSTRUTTORI DI QUEST'ANNO
			
			//if(!isset($_POST['costruttore-di-riferimento']) || empty($_POST['costruttore-di-riferimento'])) {
			
				$costruttori_anno = Db::getInstance()->executeS('SELECT * FROM trgt WHERE anno = "'.$anno_di_riferimento.'" AND descrizione != "Anno"');
				
				foreach($costruttori_anno as $c) {
				
					
					$costruttore_array = $c['descrizione'];
					$costruttore_array = explode("-",$costruttore_array);
					$costruttore = $costruttore_array[1];
					
					if($costruttore == 105)
						$where_bdl_c = '';
					else
						$where_bdl_c = ' AND id_bdl = "XXX"';
					
					
					$where_totale = ' AND od.manufacturer_id = '.$costruttore.' ';
					$where_margine = ' AND od.manufacturer_id = '.$costruttore.' ';
					$query_totale_d = Db::getInstance()->getRow($query_totale.$where_bdl_c.$query_totale_2.$where_totale);
					$query_margine_d = Db::getInstance()->getRow($query_margine.$where_bdl_c.$query_margine_2.$where_totale);
					
					$obiettivi_anno_q = Db::getInstance()->getRow('SELECT * FROM trgt WHERE anno = "'.$anno_di_riferimento.'" AND descrizione = "m-'.$costruttore.'-'.$anno_di_riferimento.'"');
					
					$obiettivi_anno = unserialize($obiettivi_anno_q['configurazione']);
					
					if($obiettivi_anno['totale'] == 0 && $obiettivi_anno['q1'] == 0 && $obiettivi_anno['q2'] == 0 && $obiettivi_anno['q3'] == 0 && $obiettivi_anno['q4'] == 0)
					{
					
					}
					
					else
					{

						echo "<div style='float:left; margin-left:20px' id='".$obiettivi_anno_q['descrizione']."'>";
						
						
						echo '<div style="display:block; width:163px; height:30px; padding-top:15px; padding-left:5px; border-top:1px solid #dfd5c3; border-left:1px solid #dfd5c3; border-right:1px solid #dfd5c3; background-color: #f4e8cd; font-weight:bold">'.Db::getInstance()->getValue('SELECT name FROM manufacturer WHERE id_manufacturer = '.$costruttore).' <img src="http://www.ezdirect.it/img/m/'.$costruttore.'-small.jpg" alt="" title="" style="display:block; float:right; height:45px; margin-top:-15px" /></div>';
						
						echo '<form name="obiettivi-annuali" method="post" action="ajax.php" onSubmit="saveTrgt(\'m-'.$costruttore.'\'); return false;" id="obiettivi-annuali">';
					
						echo '<table id="target-table" class="table">
						<tr><th style="width:40px">Valore</th><th style="width:100px">Target</th><th style="width:60px">Venduto</th><th style="width:70px">% raggiunto</th><th style="width:90px">Margine Lordo</th></tr>';
						
						
				
						echo '<tr><td style="text-align:left;">Anno</td>
						<td><input onkeyup="aggiornaTarget(\'m-'.$costruttore.'\',\''.$anno_di_riferimento.'\',\'anno\')" type="text" style="width:80px; text-align:right" name="totale" id="m-'.$costruttore.'-anno-'.$anno_di_riferimento.'-target" value="'.number_format($obiettivi_anno['totale'],2,",",".").'" /></td><td id="m-'.$costruttore.'-anno-'.$anno_di_riferimento.'-vend">'.number_format($query_totale_d['total'],2,",",".").'</td>
						<td id="m-'.$costruttore.'-anno-'.$anno_di_riferimento.'-perc">'.number_format(((($query_totale_d['total']*100)/$obiettivi_anno['totale'])),2,",",".").'%</td>
						<td id="m-'.$costruttore.'-anno-'.$anno_di_riferimento.'-ml">'.number_format($query_margine_d['margine'],2,",",".").'</td></tr>';
						
						
						$tot_t = number_format($query_totale_d['total'],2,",",".");
						$totale_anno = $query_totale_d['total'];
						$tot_d = number_format(((($query_totale_d['total']*100)/$obiettivi_anno['totale'])),2,",",".");
						$tot_m = number_format($query_margine_d['margine'],2,",",".");
						
						//q1
						$where_totale = ' AND od.manufacturer_id = '.$costruttore.' AND ( month(o.date_add) = 1 OR  month(o.date_add) = 2 OR month(o.date_add) = 3)';
						$where_margine = ' AND od.manufacturer_id = '.$costruttore.' AND ( month(o.date_add) = 1 OR  month(o.date_add) = 2 OR month(o.date_add) = 3)';
						$where_bdl = $where_bdl_c.' AND ( month(date_add) = 1 OR  month(date_add) = 2 OR month(date_add) = 3)';
						$query_totale_d = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
						$query_margine_d = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
						
						echo '<tr><td style="text-align:left">Q1</td>
						<td><input onkeyup="aggiornaTarget(\'m-'.$costruttore.'\',\''.$anno_di_riferimento.'\',\'q1\')" type="text" style="width:80px; text-align:right" name="q1" id="m-'.$costruttore.'-q1-'.$anno_di_riferimento.'-target" value="'.number_format($obiettivi_anno['q1'],2,",",".").'" /></td><td id="m-'.$costruttore.'-q1-'.$anno_di_riferimento.'-vend">'.number_format($query_totale_d['total'],2,",",".").'</td>
						<td id="m-'.$costruttore.'-q1-'.$anno_di_riferimento.'-perc">'.number_format(((($query_totale_d['total']*100)/$obiettivi_anno['q1'])),2,",",".").'%</td>
						<td id="m-'.$costruttore.'-q1-'.$anno_di_riferimento.'-ml">'.number_format($query_margine_d['margine'],2,",",".").'</td></tr>';
						
						
						//q2
						$where_totale = ' AND od.manufacturer_id = '.$costruttore.' AND ( month(o.date_add) = 4 OR  month(o.date_add) = 5 OR month(o.date_add) = 6)';
						$where_margine = ' AND od.manufacturer_id = '.$costruttore.' AND ( month(o.date_add) = 4 OR  month(o.date_add) = 5 OR month(o.date_add) = 6)';
						$where_bdl = $where_bdl_c.' AND ( month(date_add) = 4 OR  month(date_add) = 5 OR month(date_add) = 6)';
						$query_totale_d = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
						$query_margine_d = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
						
						echo '<tr><td style="text-align:left">Q2</td>
						<td><input onkeyup="aggiornaTarget(\'m-'.$costruttore.'\',\''.$anno_di_riferimento.'\',\'q2\')" type="text" style="width:80px; text-align:right" name="q2"  id="m-'.$costruttore.'-q2-'.$anno_di_riferimento.'-target" value="'.number_format($obiettivi_anno['q2'],2,",",".").'" /></td><td id="m-'.$costruttore.'-q2-'.$anno_di_riferimento.'-vend">'.number_format($query_totale_d['total'],2,",",".").'</td>
						<td id="m-'.$costruttore.'-q2-'.$anno_di_riferimento.'-perc">'.number_format(((($query_totale_d['total']*100)/$obiettivi_anno['q2'])),2,",",".").'%</td>
						<td id="m-'.$costruttore.'-q2-'.$anno_di_riferimento.'-ml">'.number_format($query_margine_d['margine'],2,",",".").'</td></tr>';
						
						//q3
						$where_totale = ' AND od.manufacturer_id = '.$costruttore.' AND ( month(o.date_add) = 7 OR  month(o.date_add) = 8 OR month(o.date_add) = 9)';
						$where_margine = ' AND od.manufacturer_id = '.$costruttore.' AND ( month(o.date_add) = 7 OR  month(o.date_add) = 8 OR month(o.date_add) = 9)';
						$where_bdl = $where_bdl_c.' AND ( month(date_add) = 7 OR  month(date_add) = 8 OR month(date_add) = 9)';
						$query_totale_d = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
						$query_margine_d = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
						
						echo '<tr><td style="text-align:left">Q3</td>
						<td><input onkeyup="aggiornaTarget(\'m-'.$costruttore.'\',\''.$anno_di_riferimento.'\',\'q3\')" type="text" style="width:80px; text-align:right" name="q3"  id="m-'.$costruttore.'-q3-'.$anno_di_riferimento.'-target" value="'.number_format($obiettivi_anno['q3'],2,",",".").'" /></td><td id="m-'.$costruttore.'-q3-'.$anno_di_riferimento.'-vend">'.number_format($query_totale_d['total'],2,",",".").'</td>
						<td id="m-'.$costruttore.'-q3-'.$anno_di_riferimento.'-perc">'.number_format(((($query_totale_d['total']*100)/$obiettivi_anno['q3'])),2,",",".").'%</td>
						<td id="m-'.$costruttore.'-q3-'.$anno_di_riferimento.'-ml">'.number_format($query_margine_d['margine'],2,",",".").'</td></tr>';
						
						//q4
						$where_totale = ' AND od.manufacturer_id = '.$costruttore.' AND ( month(o.date_add) = 10 OR  month(o.date_add) = 11 OR month(o.date_add) = 12)';
						$where_margine = ' AND od.manufacturer_id = '.$costruttore.' AND ( month(o.date_add) = 10 OR  month(o.date_add) = 11 OR month(o.date_add) = 12)';
						$where_bdl = $where_bdl_c.' AND ( month(date_add) = 10 OR  month(date_add) = 11 OR month(date_add) = 12)';
						$query_totale_d = Db::getInstance()->getRow($query_totale.$where_bdl.$query_totale_2.$where_totale);
						$query_margine_d = Db::getInstance()->getRow($query_margine.$where_bdl.$query_margine_2.$where_totale);
						
						echo '<tr><td style="text-align:left">Q4</td>
						<td><input onkeyup="aggiornaTarget(\'m-'.$costruttore.'\',\''.$anno_di_riferimento.'\',\'q4\')" type="text" style="width:80px; text-align:right" name="q4"  id="m-'.$costruttore.'-q4-'.$anno_di_riferimento.'-target" value="'.number_format($obiettivi_anno['q4'],2,",",".").'" /></td><td id="m-'.$costruttore.'-q4-'.$anno_di_riferimento.'-vend">'.number_format($query_totale_d['total'],2,",",".").'</td>
						<td id="m-'.$costruttore.'-q4-'.$anno_di_riferimento.'-perc">'.number_format(((($query_totale_d['total']*100)/$obiettivi_anno['q4'])),2,",",".").'%</td>
						<td id="m-'.$costruttore.'-q4-'.$anno_di_riferimento.'-ml">'.number_format($query_margine_d['margine'],2,",",".").'</td></tr>';
						
						// qtot
						echo '<tr><td style="text-align:left"><strong>Q.TOT. 1+2+3+4</strong></td>
						<td><strong><input type="text" style="width:80px; text-align:right" id="m-'.$costruttore.'-totale-'.$anno_di_riferimento.'-target" readonly="readonly" value="'.number_format($obiettivi_anno['q1']+$obiettivi_anno['q2']+$obiettivi_anno['q3']+$obiettivi_anno['q4'],2,",",".").'" /></strong></td><td id="m-'.$costruttore.'-tot-'.$anno_di_riferimento.'-vend"><strong>'.$tot_t.'</strong></td>
						<td id="m-'.$costruttore.'-tot-'.$anno_di_riferimento.'-perc"><strong>'.number_format(((($totale_anno*100)/($obiettivi_anno['q1']+$obiettivi_anno['q2']+$obiettivi_anno['q3']+$obiettivi_anno['q4']))),2,",",".").'%</strong></td>
						<td id="m-'.$costruttore.'-tot-'.$anno_di_riferimento.'-perc"><strong>'.$tot_m.'</strong></td></tr>';
						
						echo '</table>';
						echo '';
						echo "<input type='hidden' name='descrizione' id='m-".$costruttore."-descrizione' value='m-".$costruttore."-".$anno_di_riferimento."' />";
						echo "<input type='hidden' name='anno' id='m-".$costruttore."-anno' value='".$anno_di_riferimento."' />";
						echo "<input type='submit' class='button' style='cursor:pointer' value='Salva' name='salva' />&nbsp;&nbsp;&nbsp;";
						echo "<input type='button' class='button' style='cursor:pointer' value='Elimina' name='Elimina' onclick='javascript: var surec=window.confirm(\"Sei sicuro?\"); if (surec) { delTrgt(\"m-".$costruttore."-".$anno_di_riferimento."\"); } else { return false; } ' />";
					
						echo "<div id='save_anno_m-".$costruttore."'></div>";
						echo "</form><br />";
						echo "</div>";
					}
				}
			//}
			
			
			// FINE TUTTI I COSTRUTTORI DI QUEST'ANNO
			
			echo '<div style="clear:both"></div><br /><br />';
			
		}

	}
	
	