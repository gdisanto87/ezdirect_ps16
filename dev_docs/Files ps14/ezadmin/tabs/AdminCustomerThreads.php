<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14703 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminCustomerThreads extends AdminTab
{
	
	
	public function __construct()
	{
	 	global $cookie;
		
		if (!isset($cookie->{'submitFilter'.$this->table}))
		{
			$cookie->customer_threadFilter_id_employee = $cookie->id_employee;
			$cookie->customer_threadFilter_stato = 'pen';
			$cookie->submitFiltercustomer_thread = true;
		}
		
	 	$this->table = 'customer_thread';
	 	$this->lang = false;
	 	$this->className = 'CustomerThread';
	 	$this->edit = false; 
	 	$this->view = noActionColumn; 
	 	$this->delete = true;
		
 		$this->_select = 'CONCAT(
		CASE a.id_contact
		WHEN 2
		THEN "A"
		WHEN 4
		THEN "T"
		WHEN 3
		THEN "V"
		WHEN 6
		THEN "R"
		WHEN 7
		THEN "M"
		WHEN 8
		THEN "D"
		WHEN 9
		THEN "S"
		END , SUBSTRING( a.date_add, 3, 2 ) , a.id_customer_thread ) AS ticket,
		
		(CASE c.id_default_group
		WHEN 1
		THEN "Clienti web"
		WHEN 3
		THEN "Rivenditori"
		WHEN 15
		THEN "Rich. Riv."
		WHEN 4
		THEN "Call center"
		WHEN 5
		THEN "PA"
		WHEN 8
		THEN "Clienti top"
		WHEN 11
		THEN "Fornitori"
		WHEN 12
		THEN "Distributori"
		WHEN 16
		THEN "C. Amazon"
		WHEN 17
		THEN "C. ePrice"
		ELSE "Altro"
		END
		) AS gruppo_clienti,
	c.company,
	
	(CASE c.is_company
	WHEN 1 
	THEN c.vat_number
	ELSE c.tax_code
	END) AS cfpi,
	cm.id_employee first_open,
	c.vat_number,
	c.tax_code,
	(CASE c.is_company
	WHEN 0
	THEN CONCAT(c.firstname," ",c.lastname)
	WHEN 1
	THEN c.company
	ELSE
	CONCAT(c.firstname," ",c.lastname)
	END
	)as customer, cl.id_contact as contact_type, 
	(CASE
	WHEN cl.name LIKE "%contabil%"
	THEN "Contabilita"
	WHEN cl.name LIKE "%ordini%"
	THEN "Ordini"
	WHEN cl.name = "Assistenza tecnica postvendita"
	THEN "Assistenza"
	WHEN cl.name = "Rivenditori"
	THEN "Rivenditori"
	WHEN cl.name = "Messaggi"
	THEN "Messaggi"
	WHEN cl.name = "RMA"
	THEN "RMA"
	ELSE "Altro"
	END	
	) 
	AS contact, 
	a.status AS stato,
	(CASE WHEN a.priorita = "high" THEN "ALTA" WHEN a.priorita = "medium" THEN "Media" WHEN a.priorita = "low" THEN "Bassa" ELSE "--" END) AS priority,
	a.date_upd AS data_act, (
			SELECT IFNULL(CONCAT(e.firstname," ",LEFT(e.lastname, 1),"."), "--")
			FROM `'._DB_PREFIX_.'customer_thread` ct 
			JOIN customer c ON c.id_customer = ct.id_customer
			
			INNER JOIN '._DB_PREFIX_.'employee e ON e.id_employee = ct.id_employee
			
			WHERE ct.id_employee > 0 AND ct.`id_customer_thread` = a.`id_customer_thread` ORDER BY ct.date_add DESC LIMIT 1) as employee';
			
		
		
		$this->_group.= "";
		
		/*if(!empty($_POST['cercaticket']) || !$cookie->submitFiltercustomer_thread) {
			$cookie->customer_threadFilter_id_employee = '';
			$cookie->customer_threadFilter_stato = '';
			$cookie->submitFiltercustomer_thread = true;
		}
		else {

		}*/
	ini_set("memory_limit","892M");
		if(isset($_GET['type'])) {
				if(is_numeric($_GET['type'])) {
					$this->_group.= 'AND a.id_contact = '.$_GET['type'].' ';
				}
				else {
					$this->_group.= 'AND a.id_contact = 3523453453 ';
				}
			}
			else {
			
		}
		
		$this->_where = ' AND (a.id_contact != 7)  ';
		
		if(isset($_GET['date']) && $_GET['date'] == 'oggi') {
		
			$this->_where = ' AND a.date_add BETWEEN "'.date("Y-m-d").' 00:00:00" AND "'.date("Y-m-d").' 23:59:59" ';
		
		}
		
		
	
		
		$this->_group .= 'GROUP BY a.id_customer_thread';
		
			$this->_group .= " UNION ALL

			SELECT ct.id_customer_thread,
			'5' AS id_lang,
			ct.id_contact,
			ct.id_customer,
			ct. id_employee,
			ct.status AS status,
			ct.date_add AS date_add,
			ct.date_upd AS date_upd,
			(CONCAT(
			'M', SUBSTRING( ct.date_add, 3, 2 ) , ct.id_customer_thread ) 
			
			)AS ticket,
			(CASE c.id_default_group
		WHEN 1
		THEN 'Clienti web'
		WHEN 3
		THEN 'Rivenditori'
		WHEN 15
		THEN 'Rich. Riv.'
		WHEN 4
		THEN 'Call center'
		WHEN 5
		THEN 'PA'
		WHEN 8
		THEN 'Clienti top'
		WHEN 11
		THEN 'Fornitori'
		WHEN 12
		THEN 'Distributori'
		WHEN 16
		THEN 'C. Amazon'
		WHEN 17
		THEN 'C. ePrice'
		ELSE 'Altro'
		END
		) AS gruppo_clienti,
		c.company AS company,
		(CASE c.is_company
	WHEN 1 
	THEN c.vat_number
	ELSE c.tax_code
	END) AS cfpi,
	cm.id_employee first_open,
		c.vat_number AS vat_number,
			c.tax_code AS tax_code,
			(CASE c.is_company
	WHEN 0
	THEN CONCAT(c.firstname,' ',c.lastname)
	WHEN 1
	THEN c.company
	ELSE
	CONCAT(c.firstname,' ',c.lastname)
	END
	) as customer, 
			'' AS contact_type,
			
			'Messaggi' AS contact,
			ct.status AS stato,
			'--' AS priority,
			ct.date_upd AS data_act, (
				SELECT IFNULL(CONCAT(e.firstname,' ',LEFT(e.lastname, 1),'.'), '--')
			FROM `"._DB_PREFIX_."customer_thread` cz 
			INNER JOIN "._DB_PREFIX_."employee e ON e.id_employee = cz.id_employee
			WHERE cz.id_employee > 0 AND cz.`id_customer_thread` = ct.`id_customer_thread` ORDER BY cz.date_add DESC LIMIT 1) as employee
			
			

			FROM customer_thread ct
			JOIN `customer` c ON c.`id_customer` = ct.`id_customer` 
			JOIN ".(!empty($_POST['cercaticket']) ? '(SELECT * FROM ' : '')." customer_message ".(!empty($_POST['cercaticket']) ? 'WHERE message LIKE "%'.$_POST['cercaticket'].'%")' : '')." cm ON cm.id_customer_thread = ct.id_customer_thread 
			WHERE ct.id_contact = 7 
			
			".(!empty($_POST['cercaticket']) ? '' : 'AND cm.id_employee = 0')." ".' '.($this->_tmpTableFilter && !Tools::getIsset('cercaticket') ? '' : 'AND (ct.status = "open" OR ct.status = "pending1" OR ct.status = "pending2" '.(Tools::getIsset('cercaticket') ? 'OR ct.status = "closed"' : '').') '.(Tools::getIsset('cercaticket') ? '' : '')).' GROUP BY ct.id_customer_thread';
		
		
			$this->_group .= " UNION ALL

			SELECT ft.id_thread AS id_customer_thread,
			'5' AS id_lang,
			ft.tipo_richiesta AS id_contact,
			ft.id_customer AS id_customer,
			ft.id_employee AS id_employee,
			ft.status AS status,
			ft.date_add AS date_add,
			ft.date_upd AS date_upd,
			(CASE ft.tipo_richiesta WHEN 'preventivo' THEN
			CONCAT(
			'P', SUBSTRING( ft.date_add, 3, 2 ) , ft.id_thread ) 
			WHEN 'tirichiamiamonoi' THEN
			CONCAT(
			'R', SUBSTRING( ft.date_add, 3, 2 ) , ft.id_thread ) 
			ELSE '--'
			END
			)AS ticket,
			(CASE c.id_default_group
		WHEN 1
		THEN 'Clienti web'
		WHEN 3
		THEN 'Rivenditori'
		WHEN 15
		THEN 'Rich. Riv.'
		WHEN 4
		THEN 'Call center'
		WHEN 5
		THEN 'PA'
		WHEN 8
		THEN 'Clienti top'
		WHEN 11
		THEN 'Fornitori'
		WHEN 12
		THEN 'Distributori'
		WHEN 16
		THEN 'C. Amazon'
		WHEN 17
		THEN 'C. ePrice'
		ELSE 'Altro'
		END
		) AS gruppo_clienti,
		c.company AS company,
		(CASE c.is_company
	WHEN 1 
	THEN ft.vat_number
	ELSE ft.tax_code
	END) AS cfpi,
	fm2.id_employee first_open,
		ft.vat_number AS vat_number,
			ft.tax_code AS tax_code,
			(CASE c.is_company
	WHEN 0
	THEN CONCAT(c.firstname,' ',c.lastname)
	WHEN 1
	THEN c.company
	ELSE
	CONCAT(c.firstname,' ',c.lastname)
	END
	) as customer, 
			'' AS contact_type,
			
			(CASE ft.tipo_richiesta WHEN 'preventivo' THEN
			'Commerciale'
			WHEN 'tirichiamiamonoi' THEN
			'Richiamiamo'
			ELSE '--'
			END
			) AS contact,
			ft.status AS stato,
			'--' AS priority,
			ft.date_upd AS data_act,
			( SELECT IFNULL(CONCAT(e.firstname,' ',LEFT(e.lastname, 1),'.'), '--') FROM form_prevendita_thread ft2 JOIN employee e ON e.id_employee = ft2.id_employee WHERE ft2.id_employee > 0 AND ft2.`id_thread` = ft.`id_thread` ORDER BY ft2.date_add DESC LIMIT 1) as employee
			
		

			FROM form_prevendita_thread ft
			JOIN `customer` c ON c.`id_customer` = ft.`id_customer` 
			JOIN (SELECT id_thread, id_employee FROM form_prevendita_message ".(!empty($_POST['cercaticket']) ? 'WHERE message LIKE "%'.$_POST['cercaticket'].'%"' : '').") fm2 ON ft.id_thread = fm2.id_thread
			
			".(!empty($_POST['cercaidticket']) ? 'JOIN (SELECT * FROM form_prevendita_thread WHERE id_thread LIKE "%'.substr($_POST['cercaidticket'],3,strlen($_POST['cercaidticket'])).'%") cft ON ft.id_thread = cft.id_thread' : '')."
			
				
			WHERE 1 ".' '.($this->_tmpTableFilter && !Tools::getIsset('cercaticket') ? '' : 'AND (ft.status = "open" OR ft.status = "pending1" OR ft.status = "pending2" '.($this->_tmpTableFilter && !Tools::getIsset('cercaticket') ? '' : 'OR ft.status = "closed"').') '.($this->_tmpTableFilter && !Tools::getIsset('cercaticket') ? 'AND ft.id_employee = '.$cookie->id_employee : '')).' ';
		
		
			if(isset($_GET['type'])) {
				if($_GET['type'] == 'preventivo') {
					$this->_group.= 'AND ft.tipo_richiesta = "'.$_GET['type'].'" ';
				}
				else {
					$this->_group.= 'AND ft.tipo_richiesta = "sdxcvxcvxcv" ';
				}
			}
			else {
						
			}
			
				
			if(isset($_GET['date']) && $_GET['date'] == 'oggi') {
		
				$this->_group .= 'AND ft.date_add BETWEEN "'.date("Y-m-d").' 00:00:00" AND "'.date("Y-m-d").' 23:59:59" ';
		
			}

			$this->_group .= " GROUP BY ft.id_thread ";
		
		// uniamo azioni
		
		$this->_group .= " UNION ALL

			SELECT ac.id_action AS id_customer_thread,
			'5' AS id_lang,
			ac.action_type AS id_contact,
			ac.id_customer AS id_customer,
			ac.action_to AS id_employee,
			ac.status AS status,
			ac.date_add AS date_add,
			ac.date_upd AS date_upd,
			CONCAT('TODO', ac.id_action) AS ticket,
			(CASE c.id_default_group
		WHEN 1
		THEN 'Clienti web'
		WHEN 3
		THEN 'Rivenditori'
		WHEN 15
		THEN 'Rich. Riv.'
		WHEN 4
		THEN 'Call center'
		WHEN 5
		THEN 'PA'
		WHEN 8
		THEN 'Clienti top'
		WHEN 11
		THEN 'Fornitori'
		WHEN 12
		THEN 'Distributori'
		WHEN 16
		THEN 'C. Amazon'
		WHEN 17
		THEN 'C. ePrice'
		ELSE 'Altro'
		END
		) AS gruppo_clienti,
		c.company AS company,
		
		(CASE c.is_company
	WHEN 1 
	THEN c.vat_number
	ELSE c.tax_code
	END) AS cfpi,
	ac.action_from first_open,
		c.vat_number AS vat_number,
			c.tax_code AS tax_code,
			(CASE c.is_company
	WHEN 0
	THEN CONCAT(c.firstname,' ',c.lastname)
	WHEN 1
	THEN c.company
	ELSE
	CONCAT(c.firstname,' ',c.lastname)
	END
	) as customer, 
			'' AS contact_type,
			ac.action_type AS contact,
			ac.status AS stato,
			'--' AS priority,
			(CASE ac.action_date
			WHEN '0000-00-00 00:00:00' THEN ac.date_upd 
			ELSE ac.action_date
			END) AS data_act,
			( SELECT IFNULL(CONCAT(e.firstname,' ',LEFT(e.lastname, 1),'.'), '--') FROM action_thread ac2 JOIN employee e ON e.id_employee = ac2.action_to WHERE ac2.action_to > 0 AND ac2.`id_action` = ac.`id_action` ORDER BY ac2.date_add DESC LIMIT 1) as employee
			
			

			FROM action_thread ac
			JOIN `customer` c ON c.`id_customer` = ac.`id_customer` 
			".(!empty($_POST['cercaidticket']) ? 'JOIN (SELECT * FROM action_thread WHERE id_action LIKE "%'.substr($_POST['cercaidticket'],4,strlen($_POST['cercaidticket'])).'%") cac ON ac.id_action = cac.id_action' : '').
			
			(!empty($_POST['cercaticket']) ? 'JOIN (SELECT * FROM action_message WHERE action_message LIKE "%'.$_POST['cercaticket'].'%") am ON ac.id_action = am.id_action' : '')."
			WHERE 1 ".' '.($this->_tmpTableFilter && !Tools::getIsset('cercaticket') ? '' : 'AND (ac.status = "open" OR ac.status = "pending1" OR ac.status = "pending2" '.($this->_tmpTableFilter && !Tools::getIsset('cercaticket')  ? '' : 'OR ac.status = "closed"').') '.($this->_tmpTableFilter && !Tools::getIsset('cercaticket') ? 'AND ac.action_to = '.$cookie->id_employee : '')).' ';
			

			
			if(isset($_GET['type'])) {
				if(!is_numeric($_GET['type']) && $_GET['type'] != 'preventivo') {
					$this->_group.= 'AND ac.action_type = "'.$_GET['type'].'" ';
				}
				else {
					$this->_group.= 'AND ac.action_type = "sdxcvxcvxcv" ';
				}
			}
			else {
						
			}
			
			if(isset($_GET['date']) && $_GET['date'] == 'oggi') {
		
				$this->_group .= 'AND (ac.date_add BETWEEN "'.date("Y-m-d").' 00:00:00" AND "'.date("Y-m-d").' 23:59:59" OR ac.action_date BETWEEN "'.date("Y-m-d").' 00:00:00" AND "'.date("Y-m-d").' 23:59:59")';
		
			}

			$this->_group .= " GROUP BY ac.id_action ";
		
		// fine unisco azioni
		
		// uniamo azioni impiegati
		
		$this->_group .= " UNION ALL

			SELECT ae.id_action AS id_customer_thread,
			'5' AS id_lang,
			ae.action_type AS id_contact,
			'Staff' AS id_customer,
			ae.action_to AS id_employee,
			ae.status AS status,
			ae.date_add AS date_add,
			ae.date_upd AS date_upd,
			CONCAT('STAFF', ae.id_action) AS ticket,
			'Att. interna' AS gruppo_clienti,
			'Staff' AS company,
			'' AS cfpi,
			ae.action_from first_open,
			'' AS vat_number,
			'' AS tax_code,
			( SELECT IFNULL(CONCAT(e.firstname,' ',LEFT(e.lastname, 1),'.'), '--') FROM action_thread_employee ae2 JOIN employee e ON e.id_employee = ae2.action_to WHERE ae2.action_to > 0 AND ae2.`id_action` = ae.`id_action` ORDER BY ae2.date_add DESC LIMIT 1) as customer, 
			'' AS contact_type,
			ae.action_type AS contact,
			ae.status AS stato,
			'--' AS priority,
			(CASE ae.action_date
			WHEN '0000-00-00 00:00:00' THEN ae.date_upd 
			ELSE ae.action_date
			END) AS data_act,
			( SELECT IFNULL(CONCAT(e.firstname,' ',LEFT(e.lastname, 1),'.'), '--') FROM action_thread_employee ae2 JOIN employee e ON e.id_employee = ae2.action_to WHERE ae2.action_to > 0 AND ae2.`id_action` = ae.`id_action` ORDER BY ae2.date_add DESC LIMIT 1) as employee
			
			

			FROM action_thread_employee ae
			".'JOIN (SELECT * FROM action_message_employee '.(!empty($_POST['cercaticket']) ? 'WHERE action_message_employee LIKE "%'.$_POST['cercaticket'].'%"' : '').') am ON ae.id_action = am.id_action'."
			
			".(!empty($_POST['cercaidticket']) ? 'JOIN (SELECT * FROM action_message_employee WHERE id_action LIKE "%'.substr($_POST['cercaidticket'],5,strlen($_POST['cercaidticket'])).'%") cae ON ae.id_action = cae.id_action' : '')."
			
			WHERE 1 ".' '.($this->_tmpTableFilter && !Tools::getIsset('cercaticket') && !Tools::getIsset('cercaidticket') ? '' : 'AND (ae.status = "open" OR ae.status = "pending1" OR ae.status = "pending2" '.($this->_tmpTableFilter && !Tools::getIsset('cercaticket') && !Tools::getIsset('cercaidticket')  ? '' : 'OR ae.status = "closed"').') '.($this->_tmpTableFilter && !Tools::getIsset('cercaticket')  && !Tools::getIsset('cercaidticket') ? 'AND ae.action_to = '.$cookie->id_employee : '')).' ';
			

			
			if(isset($_GET['type'])) {
				if(!is_numeric($_GET['type']) && $_GET['type'] != 'preventivo') {
					$this->_group.= 'AND ae.action_type = "'.$_GET['type'].'" ';
				}
				else {
					$this->_group.= 'AND ae.action_type = "sdxcvxcvxcv" ';
				}
			}
			else {
						
			}
			
			if(isset($_GET['date']) && $_GET['date'] == 'oggi') {
		
				$this->_group .= 'AND (ae.date_add BETWEEN "'.date("Y-m-d").' 00:00:00" AND "'.date("Y-m-d").' 23:59:59" OR ae.action_date BETWEEN "'.date("Y-m-d").' 00:00:00" AND "'.date("Y-m-d").' 23:59:59")';
		
			}

			$this->_group .= " GROUP BY ae.id_action ";
			
		// fine unisco azioni impiegati
		$this->_join = '
		LEFT JOIN `'._DB_PREFIX_.'customer` c ON c.`id_customer` = a.`id_customer`
		LEFT JOIN (SELECT id_employee, id_customer_thread FROM customer_message ORDER BY date_add ASC) cm on a.id_customer_thread = cm.id_customer_thread
		'.(!empty($_POST['cercaticket']) ? 'JOIN (SELECT * FROM customer_message WHERE message LIKE "%'.$_POST['cercaticket'].'%") cct ON a.id_customer_thread = cct.id_customer_thread' : '').'
		
		'.(!empty($_POST['cercaidticket']) ? 'JOIN (SELECT * FROM customer_thread WHERE id_customer_thread LIKE "%'.substr($_POST['cercaidticket'],3,strlen($_POST['cercaidticket'])).'%") cct ON a.id_customer_thread = cct.id_customer_thread' : '').'
		LEFT JOIN `'._DB_PREFIX_.'contact_lang` cl ON (cl.`id_contact` = a.`id_contact` AND cl.`id_lang` = '.(int)$cookie->id_lang.') ';
		
		$the_employees = Db::getInstance()->ExecuteS('
		SELECT `id_employee`, CONCAT(firstname," ",LEFT(lastname, 1),".") AS "name"
		FROM `'._DB_PREFIX_.'employee`
		WHERE `active` = 1
		ORDER BY `email`');
		
		if($cookie->profile == 7){
			$the_employees = Db::getInstance()->ExecuteS('
				SELECT `id_employee`, CONCAT(firstname," ",LEFT(lastname, 1),".") AS "name"
				FROM `'._DB_PREFIX_.'employee`
				WHERE `id_employee` = '.$cookie->id_employee.'
			');
		}
		
		$contactEmployees = array();
		foreach($the_employees as $employee) 
			$contactEmployees[$employee['id_employee']] = $employee['name'];
			
		$contactEmployeesFirstOpen = array();
		foreach($the_employees as $employee) 
			$contactEmployeesFirstOpen[$employee['id_employee']] = $employee['name'];
		
		$contactEmployeesFirstOpen[0] = 'Cliente';
		
		$contactArray = array();
		$contacts = Contact::getContacts($cookie->id_lang);
		foreach ($contacts AS $contact)
			$contactArray[$contact['id_contact']] = $contact['name'];
			
		$languageArray = array();
		$languages = Language::getLanguages();
		foreach ($languages AS $language)
			$languageArray[$language['id_lang']] = $language['name'];
			
		$statusArray = array(
			'open' => $this->l('Open'),
			'closed' => $this->l('Closed'),
			'pending1' => $this->l('In lavorazione'),
			'pen' => $this->l('Aperto + In lavorazione')
		);
		
		$imagesArray = array(
			'open' => 'status_red.gif',
			'closed' => 'status_green.gif',
			'pending1' => 'status_orange.gif'
		);
		
		$contactArray['Telefonata'] = 'Telefonata';
		$contactArray['Visita'] = 'Visita';
		$contactArray['Attivita'] = 'Attivita';
		$contactArray['Caso'] = 'Caso';
		$contactArray['Intervento'] = 'Intervento';
		$contactArray['preventivo'] = 'Commerciale';
		$contactArray['Richiesta_RMA'] = 'Richiesta RMA';
		$contactArray['tirichiamiamonoi'] = 'Ti richiamiamo noi';
		$contactArray['TODO Amazon'] = 'TODO Amazon';
		$contactArray['TODO Social'] = 'TODO Social';
		$contactArray['TODO Sendinblue'] = 'TODO Sendinblue';
		
		// IN CARICO A: nome invece di numero, mettere "employee"
	
		$this->fieldsDisplay = array(
			'ticket' => array('title' => $this->l('ID'), 'filter' => true, 'search' => true, 'width' => 25, 'tmpTableFilter' => true, 'filter_type' => 'string'),
				'stato' => array('title' => $this->l('Status'), 'width' =>120, 'type' => 'select', 'select' => $statusArray, 'icon' => $imagesArray, 'align' => 'center', 'filter_key' => 'stato', 'tmpTableFilter' => true, 'filter_type' => 'string'),
				
			'id_employee' => array('title' => $this->l('In carico a'), 'width' => 75, 'type' => 'select', 'select' => $contactEmployees, 
			'filter_key' => 'id_employee', 'tmpTableFilter' => true),
			
			'first_open' => array('title' => $this->l('Aperto da'), 'width' => 75,  'type' => 'select', 'select' => $contactEmployeesFirstOpen, 'filter_key' => 'first_open', 'tmpTableFilter' => true),
			
			'contact' => array('title' => $this->l('Azioni'), 'width' => 35, 'type' => 'select', 'select' => $contactArray, 'filter_key' => 'id_contact',  'tmpTableFilter' => true),
			'customer' => array('title' => $this->l('Customer'), 'width' => 120, 'filter_key' => 'customer', 'tmpTableFilter' => true),
			'gruppo_clienti' => array('title' => $this->l('Gruppo'), 'width' => 75, 'filter' => false, 'tmpTableFilter' => true),
			//'cfpi' => array('title' => $this->l('CF / PI'), 'width' => 55, 'filter' => false, 'tmpTableFilter' => true),
			'priority' => array('title' => $this->l('Urgenza'), 'width' => 75, 'filter' => false, 'tmpTableFilter' => true),
			//	'id_order' => array('title' => $this->l('ID ordine'), 'width' => 30, 'filter' => false),
			'data_act' => array('title' => $this->l('Data'), 'width' => 75, 'type' => 'date', 'tmpTableFilter' => true),
			
		
			
		);
		parent::__construct();
	}
	
	public function postProcess()
	{
		global $currentIndex, $cookie, $link;
		
		if (Tools::isSubmit('submitFilter'.$this->table) OR $cookie->{'submitFilter'.$this->table} !== false)
		{
			$_POST = array_merge($cookie->getFamily($this->table.'Filter_'), (isset($_POST) ? $_POST : array()));
			foreach ($_POST AS $key => $value)
			{
				/* Extracting filters from $_POST on key filter_ */
				if ($value != NULL AND !strncmp($key, $this->table.'Filter_', 7 + Tools::strlen($this->table)))
				{
					$key = Tools::substr($key, 7 + Tools::strlen($this->table));
					/* Table alias could be specified using a ! eg. alias!field */
					$tmpTab = explode('!', $key);
					$filter = count($tmpTab) > 1 ? $tmpTab[1] : $tmpTab[0];
					if ($field = $this->filterToField($key, $filter))
					{
						$type = (array_key_exists('filter_type', $field) ? $field['filter_type'] : (array_key_exists('type', $field) ? $field['type'] : false));
						if (($type == 'date' OR $type == 'datetime') AND is_string($value))
							$value = unserialize($value);
						$key = isset($tmpTab[1]) ? $tmpTab[0].'.`'.bqSQL($tmpTab[1]).'`' : '`'.bqSQL($tmpTab[0]).'`';
						if (array_key_exists('tmpTableFilter', $field))
							$sqlFilter = & $this->_tmpTableFilter;
						elseif (array_key_exists('havingFilter', $field))
							$sqlFilter = & $this->_filterHaving;
						else
							$sqlFilter = & $this->_filter;

						/* Only for date filtering (from, to) */
						if (is_array($value))
						{
							/*if (isset($value[0]) AND !empty($value[0]))
							{
								if (!Validate::isDate($value[0]))
									$this->_errors[] = Tools::displayError('\'from:\' date format is invalid (YYYY-MM-DD)');
								else
									$sqlFilter .= ' AND (date_add >= \''.pSQL(Tools::dateFrom($value[0])).'\'';
							}

							if (isset($value[1]) AND !empty($value[1]))
							{
								if (!Validate::isDate($value[1]))
									$this->_errors[] = Tools::displayError('\'to:\' date format is invalid (YYYY-MM-DD)');
								else
									$sqlFilter .= ' AND date_add <= \''.pSQL(Tools::dateTo($value[1])).'\'';
							}*/
						}
						else
						{
							$sqlFilter .= ' AND ';
							if ($type == 'int' OR $type == 'bool')
								$sqlFilter .= (($key == $this->identifier OR $key == '`'.$this->identifier.'`' OR $key == '`active`') ? 'a.' : '').pSQL($key).' = '.(int)($value).' ';
							elseif ($type == 'decimal')
								$sqlFilter .= (($key == $this->identifier OR $key == '`'.$this->identifier.'`') ? 'a.' : '').pSQL($key).' = '.(float)($value).' ';
							elseif ($type == 'select')
								$sqlFilter .= (($key == $this->identifier OR $key == '`'.$this->identifier.'`') ? 'a.' : '').pSQL($key).' = \''.pSQL($value).'\' ';
							else
								$sqlFilter .= (($key == $this->identifier OR $key == '`'.$this->identifier.'`') ? 'a.' : '').pSQL($key).' LIKE \'%'.pSQL($value).'%\' ';
						}
					}
				}
			}
		}
		
		
		if (isset($_POST['submitResetX'.$this->table]))
		{
			$filters = $cookie->getFamily($this->table.'Filter_');
			foreach ($filters AS $cookieKey => $filter)
				if (strncmp($cookieKey, $this->table.'Filter_', 7 + Tools::strlen($this->table)) == 0)
					{
						$key = substr($cookieKey, 7 + Tools::strlen($this->table));
						/* Table alias could be specified using a ! eg. alias!field */
						$tmpTab = explode('!', $key);
						$key = (count($tmpTab) > 1 ? $tmpTab[1] : $tmpTab[0]);
						if (array_key_exists($key, $this->fieldsDisplay))
							unset($cookie->$cookieKey);
					}
			if (isset($cookie->{'submitFilter'.$this->table}))
				unset($cookie->{'submitFilter'.$this->table});
			if (isset($cookie->{$this->table.'Orderby'}))
				unset($cookie->{$this->table.'Orderby'});
			if (isset($cookie->{$this->table.'Orderway'}))
				unset($cookie->{$this->table.'Orderway'});
			unset($_POST);
			
			$cookie->customer_threadFilter_id_employee = $cookie->id_employee;
			$cookie->customer_threadFilter_stato = 'pen';
			$cookie->customer_threadFilter_ticket = '';
			$cookie->customer_threadFilter_contact = '';
			$cookie->customer_threadFilter_customer = '';
			$cookie->customer_threadFilter_gruppo_clienti = '';
			$cookie->customer_threadFilter_cfpi = '';
			$cookie->customer_threadFilter_data_act = '';
			$cookie->submitFiltercustomer_thread = true;
			
			Tools::redirectAdmin($currentIndex.'&token='.$this->token);
		}
		
		if(isset($_GET['del']) && isset($_GET['typedel'])) {

			if(is_numeric($_GET['typedel']) || $_GET['typedel'] == 'messaggio' || $_GET['typedel'] == 'ticket') {
			
				Db::getInstance()->executeS("DELETE customer_thread, customer_message FROM customer_thread JOIN customer_message ON customer_message.id_customer_thread = customer_thread.id_customer_thread WHERE customer_thread.id_customer_thread = ".$_GET['id_customer_thread']."");
				
				Db::getInstance()->executeS("DELETE FROM customer_thread WHERE customer_thread.id_customer_thread = ".$_GET['id_customer_thread']."");
				
				if(Tools::getValue('typedel') == 9) {

					Db::getInstance()->executeS("DELETE FROM rma WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
				}
			
			}

			else if($_GET['typedel'] == 'preventivo' || $_GET['typedel'] == 'tirichiamiamonoi') {
			
				Db::getInstance()->executeS("DELETE form_prevendita_thread, form_prevendita_message  FROM form_prevendita_thread JOIN form_prevendita_message ON form_prevendita_message.id_thread = form_prevendita_thread.id_thread WHERE form_prevendita_thread.id_thread = ".$_GET['id_customer_thread']."");
				
				Db::getInstance()->executeS("DELETE FROM form_prevendita_thread WHERE form_prevendita_thread.id_thread = ".$_GET['id_customer_thread']."");
				
				
			}
			
			else if(is_numeric($_GET['typedel']) || $_GET['typedel'] == 'to-do-employee') {
			
				Db::getInstance()->executeS("DELETE action_thread_employee, action_message_employee  FROM action_thread_employee JOIN action_message_employee ON action_message_employee.id_action = action_thread_employee.id_action WHERE action_thread_employee.id_action = ".$_GET['id_employee_thread']."");
				
				Db::getInstance()->executeS("DELETE FROM action_thread WHERE action_thread.id_action = ".$_GET['id_customer_thread']."");
				
				Db::getInstance()->execute("DELETE FROM ticket_promemoria WHERE tipo = 'ae' AND msg = ".$_GET['id_employee_thread']."");
				Tools::redirectAdmin($currentIndex.'&conf=1&id_employee='.Tools::getValue('id_employee').'&updateemployee&tab-container-1=3&token='.$this->token);
				
			}
			
			else {
			
				Db::getInstance()->executeS("DELETE action_thread, action_message  FROM action_thread JOIN action_message ON action_message.id_action = action_thread.id_action WHERE action_thread.id_action = ".$_GET['id_customer_thread']."");
				Db::getInstance()->executeS("DELETE FROM action_thread WHERE action_thread.id_action = ".$_GET['id_customer_thread']."");
				Db::getInstance()->execute("DELETE FROM ticket_promemoria WHERE tipo = 'a' AND msg = ".$_GET['id_customer_thread']."");
			
			}
			
			if(Tools::getIsset('backtocustomers')) {
				Tools::redirectAdmin('index.php?tab=AdminCustomers&id_customer='.Tools::getValue('backtocustomers').'&viewcustomer&conf=1&token='.Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee).'&tab-container-1='.Tools::getValue('tab-container-1'));
			}
				
			else {
				Tools::redirectAdmin($currentIndex.'&conf=1&token='.$this->token);
			}
		}
		
		if(isset($_POST['multidelete'])) {
			
			foreach($_POST['customer_threadBox'] as $box) {
			
				$arrbox = explode(":", $box);
		
				if(is_numeric($arrbox[1])) {
				
					Db::getInstance()->executeS("DELETE customer_thread, customer_message FROM customer_thread JOIN customer_message ON customer_message.id_customer_thread = customer_thread.id_customer_thread WHERE customer_thread.id_customer_thread = ".$arrbox[0]."");
				
				}

				else if($arrbox[1] == 'preventivo' || $arrbox[1] == 'tirichiamiamonoi' ) {
				
					Db::getInstance()->executeS("DELETE form_prevendita_thread, form_prevendita_message  FROM form_prevendita_thread JOIN form_prevendita_message ON form_prevendita_message.id_thread = form_prevendita_thread.id_thread WHERE form_prevendita_thread.id_thread = ".$arrbox[0]."");
								
				}
				
				else {
				
					Db::getInstance()->executeS("DELETE action_thread, action_message  FROM action_thread JOIN action_message ON action_message.id_action = action_thread.id_action WHERE action_thread.id_action = ".$arrbox[0]."");
				
				}	
				
			}
			
			Tools::redirectAdmin($currentIndex.'&conf=1&token='.$this->token);
		}
		
		if(isset($_POST['cambiastatus'])) {

			if(Tools::getValue('tipo') == 'ticket') {
				
				Db::getInstance()->ExecuteS("UPDATE customer_thread SET status = '".Tools::getValue('cambiastatus')."' WHERE id_customer_thread = '".Tools::getValue('id_thread')."'");
			
			}
			
			
			else if(Tools::getValue('tipo') == 'preventivo' || Tools::getValue('tipo') == 'tirichiamiamonoi' ) {
			
				Db::getInstance()->ExecuteS("UPDATE form_prevendita_thread SET status = '".Tools::getValue('cambiastatus')."' WHERE id_thread = '".Tools::getValue('id_thread')."'");
				
				
			
			}
			
			else if(Tools::getValue('tipo') == 'to-do') {
			
				Db::getInstance()->ExecuteS("UPDATE action_thread SET status = '".Tools::getValue('cambiastatus')."' WHERE id_action = '".Tools::getValue('id_thread')."'");
				
				$subject = Db::getInstance()->getValue('SELECT subject FROM action_thread WHERE id_action = '.Tools::getValue('id_thread'));
				$current_action_status = Db::getInstance()->getValue('SELECT status FROM action_thread WHERE id_action = '.Tools::getValue('id_thread'));
			
				if(substr($subject,0,20) == '*** VERIFICA TECNICA')
				{	
					if(Tools::getValue('cambiastatus') == 'closed'  && $current_action_status != 'closed')
					{
						Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.substr($subject, -6,5).', 25, "'.date('Y-m-d H:i:s').'")');
						
					}
					else if((Tools::getValue('cambiastatus') == 'open' || Tools::getValue('cambiastatus') == 'pending1')   && $current_action_status == 'closed')
					{
						Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.substr($subject, -6,5).', 24, "'.date('Y-m-d H:i:s').'")');
						
					}
					
				}
				
				Customer::chiudiOrdineDaAzione(Tools::getValue('id_thread'));
			
			}
			
			else if(Tools::getValue('tipo') == 'to-do-employee') {
			
				Db::getInstance()->ExecuteS("UPDATE action_thread_employee SET status = '".Tools::getValue('cambiastatus')."' WHERE id_action = '".Tools::getValue('id_thread')."'");
				
			}
			
			if (Tools::getIsset($this->table.'Orderby') && Tools::getValue($this->table.'Orderby') != '' ) {
		
				Tools::redirectAdmin($currentIndex.'&customer_threadOrderby='.Tools::getValue('customer_threadOrderby').'&customer_threadOrderway='.Tools::getValue('customer_threadOrderway').(Tools::getValue('type') > 0 ? '&type='.Tools::getValue('type') : '').(Tools::getValue('date') != '' ? '&date='.Tools::getValue('date') : '').'&token='.$this->token.'&conf=4');
	
			}
			
			else {
			
				Tools::redirectAdmin($currentIndex.'&token='.$this->token.'&conf=4');
			
			}

		}
		
		if(isset($_POST['cambiaincarico'])) {
			
			Customer::cambiaIncaricoVeloce(Tools::getValue('tipo'), Tools::getValue('cambiaincarico'), Tools::getValue('id_thread'), Tools::getValue('precedente_incarico'), Tools::getValue('id_ticket_univoco'), Tools::getValue('id_customer'));
			
			if (Tools::getIsset($this->table.'Orderby') && Tools::getValue($this->table.'Orderby') != '' ) {
		
				Tools::redirectAdmin($currentIndex.'&customer_threadOrderby='.Tools::getValue('customer_threadOrderby').'&customer_threadOrderway='.Tools::getValue('customer_threadOrderway').(Tools::getValue('type') > 0 ? '&type='.Tools::getValue('type') : '').(Tools::getValue('date') != '' ? '&date='.Tools::getValue('date') : '').'&token='.$this->token.'&conf=4');
	
			}
			
			else {
			
				Tools::redirectAdmin($currentIndex.'&token='.$this->token.'&conf=4');
			
			}

		}
		
		

		
		if ($id_customer_thread = (int)Tools::getValue('id_customer_thread'))
		{
			if (($id_contact = (int)Tools::getValue('id_contact')))
				Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'customer_thread SET id_contact = '.(int)$id_contact.' WHERE id_customer_thread = '.(int)$id_customer_thread);
			if ($id_status = (int)Tools::getValue('setstatus'))
			{
				$statusArray = array(1 => 'open', 2 => 'closed', 3 => 'pending1', 4 => 'pending2');
				Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'customer_thread SET status = "'.$statusArray[$id_status].'" WHERE id_customer_thread = '.(int)$id_customer_thread.' LIMIT 1');
			}
			if (isset($_POST['id_employee_forward']))
			{
				// Todo: need to avoid doubles 
				$messages = Db::getInstance()->ExecuteS('
				SELECT ct.*, cm.*, cl.name subject, CONCAT(e.firstname, \' \', e.lastname) employee_name, CONCAT(c.firstname, \' \', c.lastname) customer_name, c.firstname
				FROM '._DB_PREFIX_.'customer_thread ct
				LEFT JOIN '._DB_PREFIX_.'customer_message cm ON (ct.id_customer_thread = cm.id_customer_thread)
				LEFT JOIN '._DB_PREFIX_.'contact_lang cl ON (cl.id_contact = ct.id_contact AND cl.id_lang = '.(int)$cookie->id_lang.')
				LEFT OUTER JOIN '._DB_PREFIX_.'employee e ON e.id_employee = cm.id_employee
				LEFT OUTER JOIN '._DB_PREFIX_.'customer c ON (c.email = ct.email)
				WHERE ct.id_customer_thread = '.(int)Tools::getValue('id_customer_thread').'
				ORDER BY cm.date_add DESC');
				$output = '';
				foreach ($messages AS $message)
					$output .= $this->displayMsg($message, true, (int)Tools::getValue('id_employee_forward'));
				
				$cm = new CustomerMessage();
				$cm->id_employee = (int)$cookie->id_employee;
				$cm->id_customer_thread = (int)Tools::getValue('id_customer_thread');
				$cm->ip_address = ip2long($_SERVER['REMOTE_ADDR']);
				$currentEmployee = new Employee($cookie->id_employee);
				if (($id_employee = (int)Tools::getValue('id_employee_forward')) AND ($employee = new Employee($id_employee)) AND Validate::isLoadedObject($employee))
				{
					$params = array(
					'{id_richiesta}' => "Id ticket: <strong>".$id_customer_thread."</strong>",
					'{messages}' => $output,
					'{employee}' => $currentEmployee->firstname.' '.$currentEmployee->lastname,
					'{comment}' => stripslashes($_POST['message_forward']));
					if (Mail::Send((int)($cookie->id_lang), 'forward_msg', Mail::l('Fwd: Customer message', (int)($cookie->id_lang)), $params,
						$employee->email, $employee->firstname.' '.$employee->lastname,
						$currentEmployee->email, $currentEmployee->firstname.' '.$currentEmployee->lastname,
						NULL, NULL, _PS_MAIL_DIR_, true))
					{
						$cm->message = $this->l('Message forwarded to').' '.$employee->firstname.' '.$employee->lastname."\n".$this->l('Comment:').' '.$_POST['message_forward'];
						$cm->add();
					}
				}
				elseif (($email = Tools::getValue('email')) AND Validate::isEmail($email))
				{
					$params = array(
					'{id_richiesta}' => "Id ticket: <strong>".$id_customer_thread."</strong>",
					'{messages}' => $output,
					'{employee}' => $currentEmployee->firstname.' '.$currentEmployee->lastname,
					'{comment}' => stripslashes($_POST['message_forward']));
					if (Mail::Send((int)($cookie->id_lang), 'forward_msg', Mail::l('Fwd: Customer message', (int)($cookie->id_lang)), $params,
						$email, NULL,
						$currentEmployee->email, $currentEmployee->firstname.' '.$currentEmployee->lastname,
						NULL, NULL, _PS_MAIL_DIR_, true))
					{
						$cm->message = $this->l('Message forwarded to').' '.$email."\n".$this->l('Comment:').' '.$_POST['message_forward'];
						$cm->add();
					}
				}
				else
					echo '<div class="alert error">'.Tools::displayError('Email invalid.').'</div>';
			}
			if (Tools::isSubmit('submitReply'))
			{
				$ct = new CustomerThread($id_customer_thread);
				$cm = new CustomerMessage();
				$cm->id_employee = (int)$cookie->id_employee;
				$cm->id_customer_thread = $ct->id;
				$cm->message = Tools::htmlentitiesutf8(nl2br2(Tools::getValue('reply_message')));
				$cm->ip_address = ip2long($_SERVER['REMOTE_ADDR']);
				if (isset($_FILES) AND !empty($_FILES['joinFile']['name']) AND $_FILES['joinFile']['error'] != 0)
					$this->_errors[] = Tools::displayError('An error occurred with the file upload.');
				elseif ($cm->add())
				{
					$fileAttachment = NULL;
					if (!empty($_FILES['joinFile']['name']))
					{
						$fileAttachment['content'] = file_get_contents($_FILES['joinFile']['tmp_name']);
						$fileAttachment['name'] = $_FILES['joinFile']['name'];
						$fileAttachment['mime'] = $_FILES['joinFile']['type'];
					}
					$params = array(
					'{id_richiesta}' => "Id ticket: <strong>".$ct->id."</strong>",
					'{reply}' => nl2br2(Tools::getValue('reply_message')),
					'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token);
					if (Mail::Send((int)$ct->id_lang, 'reply_msg', Mail::l('Messaggio da Ezdirect', (int)$ct->id_lang), 
						$params, Tools::getValue('msg_email'), NULL, NULL, NULL, $fileAttachment, NULL, 
						_PS_MAIL_DIR_, true))
					{
						$ct->status = 'closed';
						$ct->update();
					}
					Tools::redirectAdmin($currentIndex.'&id_customer_thread='.(int)$id_customer_thread.'&viewcustomer_thread&token='.Tools::getValue('token'));
				}
				else
					$this->_errors[] = Tools::displayError('An error occurred, your message was not sent. Please contact your system administrator.');
			}
		}

		return AdminCustomerThreads::postProcessPCT();
	}
	
	public function display()
	{
		global $cookie;

		if (isset($_GET['filename']) AND file_exists(_PS_UPLOAD_DIR_.$_GET['filename']))
			self::openUploadedFile();
		elseif (isset($_GET['view'.$this->table]))
			$this->viewcustomer_thread();
		else
		{
			$this->getList((int)$cookie->id_lang, !Tools::getValue($this->table.'Orderby') ? 'date_add' : NULL, !Tools::getValue($this->table.'Orderway') ? 'DESC' : NULL);
			$this->displayListModificata();
		}
	}

	
	private function openUploadedFile()
	{
		$filename = $_GET['filename'];
		
		$extensions = array('.txt' => 'text/plain', '.rtf' => 'application/rtf', '.doc' => 'application/msword', '.docx'=> 'application/msword',
		'.pdf' => 'application/pdf', '.zip' => 'multipart/x-zip', '.png' => 'image/png', '.jpeg' => 'image/jpeg', '.gif' => 'image/gif', '.jpg' => 'image/jpeg');

		$extension = '';
		foreach ($extensions AS $key => $val)
			if (substr($filename, -4) == $key OR substr($filename, -5) == $key)
			{
				$extension = $val;
				break;
			}

		ob_end_clean();
		header('Content-Type: '.$extension);
		header('Content-Disposition:attachment;filename="'.$filename.'"');
		readfile(_PS_UPLOAD_DIR_.$filename);
		die;
	}
	private function displayMsg($message, $email = false, $id_employee = null)
	{
		global $cookie, $currentIndex;

		$customersToken = Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee));
		$contacts = Contact::getContacts($cookie->id_lang);
		
		if (!$email)
		{
			if (!empty($message['id_product']) AND empty($message['id_employee']))
				$id_order_product = Db::getInstance()->getValue('
				SELECT o.id_order
				FROM '._DB_PREFIX_.'orders o
				LEFT JOIN '._DB_PREFIX_.'order_detail od ON o.id_order = od.id_order
				WHERE o.id_customer = '.(int)$message['id_customer'].'
				AND od.product_id = '.(int)$message['id_product'].'
				ORDER BY o.date_add DESC');
				
				$prod_name = Db::getInstance()->getValue('
				SELECT pl.name
				FROM '._DB_PREFIX_.'orders o
				LEFT JOIN '._DB_PREFIX_.'order_detail od ON o.id_order = od.id_order
				JOIN product_lang pl ON od.product_id = pl.id_product
				WHERE o.id_customer = '.(int)$message['id_customer'].'
				AND pl.id_lang = 5
				AND od.product_id = '.(int)$message['id_product'].'
				ORDER BY o.date_add DESC');
			
			$output = '
			<fieldset style="'.(!empty($message['id_employee']) ? 'background: rgb(255,236,242);' : '').'width:600px;margin-top:10px">
				<legend '.(empty($message['id_employee']) ? '' : 'style="background:rgb(255,210,225)"').'>'.(
					!empty($message['employee_name'])
					? '<img src="../img/t/AdminCustomers.gif" alt="'.Configuration::get('PS_SHOP_NAME').'" /> '.Configuration::get('PS_SHOP_NAME').' - '.$message['employee_name']
					: '<img src="'.__PS_BASE_URI__.'img/admin/tab-customers.gif" alt="'.Configuration::get('PS_SHOP_NAME').'" /> '.(
						!empty($message['id_customer'])
						? '<a href="index.php?tab=AdminCustomers&id_customer='.(int)($message['id_customer']).'&viewcustomer&token='.$customersToken.'" title="'.$this->l('View customer').'">'.$message['customer_name'].'</a>'
						: $message['email']
					)
				).'</legend>
				<div style="font-size:11px">'.(
				
						(!empty($message['id_customer']) AND empty($message['id_employee']))
						? '<b>'.$this->l('Customer ID:').'</b> <a href="index.php?tab=AdminCustomers&id_customer='.(int)($message['id_customer']).'&viewcustomer&token='.$customersToken.'" title="'.$this->l('View customer').'">'.(int)($message['id_customer']).' <img src="../img/admin/search.gif" alt="'.$this->l('view').'" /></a><br />'
						: ''
					).(
				
						(!empty($message['phone']))
						? '<b>'.$this->l('Recapito telefonico:').'</b> '.($message['phone']).' <br />'
						: '<b>'.$this->l('Recapito telefonico:').'</b> <em>Non inserito</em> <br />'
					).'
					<b>'.$this->l('Sent on:').'</b> '.date("d/m/Y, H:i:s", strtotime($message['date_add'])).'<br />'.(
						empty($message['id_employee'])
						? '<b>'.$this->l('Browser:').'</b> '.strip_tags($message['user_agent']).'<br />'
						: ''
					).(
						(!empty($message['file_name']) AND file_exists(_PS_UPLOAD_DIR_.$message['file_name']))
						? '<b>'.$this->l('File attachment').'</b> <a href="index.php?tab=AdminCustomerThreads&id_customer_thread='.$message['id_customer_thread'].'&viewcustomer_thread&token='.Tools::getAdminToken('AdminCustomerThreads'.(int)(Tab::getIdFromClassName('AdminCustomerThreads')).(int)($cookie->id_employee)).'&filename='.$message['file_name'].'" title="'.$this->l('View file').'"><img src="../img/admin/search.gif" alt="'.$this->l('view').'" /></a><br />'
						: ''
					).(
						(!empty($message['id_order']) AND empty($message['id_employee']))
						? '<b>'.$this->l('Order #').'</b> <a href="index.php?tab=AdminCustomers&id_customer='.$message['id_customer'].'&viewcustomer&id_order='.(int)($message['id_order']).'&vieworder&token='.Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee)).'&tab-container-1=4" title="'.$this->l('View order').'">'.(int)($message['id_order']).' <img src="../img/admin/search.gif" alt="'.$this->l('view').'" /></a><br />'
						: ''
					).(
						(!empty($message['id_product']) AND empty($message['id_employee']))
						? '<b>'.$this->l('Prodotto:').'</b> '.$prod_name.' <br />'
						: ''
					).'<br />
					<form action="'.Tools::htmlentitiesutf8($_SERVER['REQUEST_URI']).'" method="post">
						<b>'.$this->l('Subject:').'</b>
						<input type="hidden" name="id_customer_message" value="'.$message['id_customer_message'].'" />
						<select name="id_contact" onchange="this.form.submit();">';
			foreach ($contacts as $contact)
				$output .= '<option value="'.(int)$contact['id_contact'].'" '.($contact['id_contact'] == $message['id_contact'] ? 'selected="selected"' : '').'>'.Tools::htmlentitiesutf8($contact['name']).'</option>';
			$output .= '</select>
					</form>';
		}
		else
		{
			$output = '<div style="font-size:11px">
			'.($id_employee ? '<a href="'.Tools::getHttpHost(true).$currentIndex.'&token='.Tools::getAdminToken('AdminCustomerThreads'.(int)(Tab::getIdFromClassName('AdminCustomerThreads')).(int)($id_employee)).'&id_customer_thread='.(int)$message['id_customer_thread'].'&viewcustomer_thread">'.$this->l('View this thread').'</a><br />' : '').'
			<b>'.$this->l('Sent by:').'</b> '.(!empty($message['customer_name']) ? $message['customer_name'].' ('.$message['email'].')' : $message['email'])
			.((!empty($message['id_customer']) AND empty($message['employee_name'])) ? '<br /><b>'.$this->l('Customer ID:').'</b> '.(int)($message['id_customer']).'<br />' : '')
			.((!empty($message['id_order']) AND empty($message['employee_name'])) ? '<br /><b>'.$this->l('Order #').':</b> '.(int)($message['id_order']).'<br />' : '')
			.((!empty($message['id_product']) AND empty($message['employee_name'])) ? '<br /><b>'.$this->l('Product #').':</b> '.(int)($message['id_product']).'<br />' : '')
			.'<br /><b>'.$this->l('Subject:').'</b> '.$message['subject'];
		}
		
		$message['message'] = preg_replace('/(https?:\/\/[a-z0-9#%&_=\(\)\.\? \+\-@\/]{6,1000})([\s\n<])/Uui', '<a href="\1">\1</a>\2', html_entity_decode($message['message'], ENT_NOQUOTES, 'UTF-8'));
		$output .= '<br /><br />
			<b>'.$this->l('Thread ID:').'</b> '.(int)$message['id_customer_thread'].'<br />
			<b>'.$this->l('Message ID:').'</b> '.(int)$message['id_customer_message'].'<br />
			<b>'.$this->l('Message:').'</b><br />
			'.$message['message'].'
		</div>';
		
		if (!$email)
		{
			$iso = Language::getIsoById((int)($cookie->id_lang));
		$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
		$ad = dirname($_SERVER["PHP_SELF"]);
				$output .= '
			<script type="text/javascript">
			var iso = \''.$isoTinyMCE.'\' ;
			var pathCSS = \''._THEME_CSS_DIR_.'\' ;
			var ad = \''.$ad.'\' ;
			</script>
			
			
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce/tiny_mce.js"></script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>
			<script type="text/javascript">
					toggleVirtualProduct(getE(\'is_virtual_good\'));
					unitPriceWithTax(\'unit\');
			</script>';
			if (empty($message['employee_name']))
				$output .= '
				<p style="text-align:right">
					<button style="font-family: Verdana; font-size: 11px; font-weight:bold; height: 65px; width: 120px;" onclick="$(\'#reply_to_'.(int)($message['id_customer_message']).'\').show(500); $(this).hide();">
						<img src="'.__PS_BASE_URI__.'img/admin/contact.gif" alt="" style="margin-bottom: 5px;" /><br />'.$this->l('Reply to this message').'
					</button>
				</p>
				<div id="reply_to_'.(int)($message['id_customer_message']).'" style="display: none; margin-top: 20px;"">
					<form action="'.Tools::htmlentitiesutf8($_SERVER['REQUEST_URI']).'" method="post" enctype="multipart/form-data">
						<p>'.$this->l('Please type your reply below:').'</p>
						<textarea style="width: 450px; height: 175px;" class="rte" name="reply_message">'.str_replace('\r\n', "\n", Configuration::get('PS_CUSTOMER_SERVICE_SIGNATURE', $message['id_lang'])).'</textarea>
						<div style="width: 450px; text-align: right; font-style: italic; font-size: 9px; margin-top: 2px;">
							'.$this->l('Your reply will be sent to:').' '.$message['email'].'
						</div>
						<div style="width: 450px; margin-top: 0px;">
							<input type="file" name="joinFile"/>
						<div>
						<div style="width: 450px; text-align: center;">
							<input type="submit" class="button" name="submitReply" value="'.$this->l('Send my reply').'" style="margin-top:20px;" />
							<input type="hidden" name="id_customer_thread" value="'.(int)($message['id_customer_thread']).'" />
							<input type="hidden" name="msg_email" value="'.$message['email'].'" />
						</div>					
					</form>
				</div>';
			$output .= '
			</fieldset>';
		}
		
		return $output;
	}
	
	public function viewcustomer_thread()
	{
		global $cookie, $currentIndex;	
		
		if (!($thread = $this->loadObject()))
			return;
		$cookie->{'customer_threadFilter_cl!id_contact'} = $thread->id_contact;
		
		$employees = Db::getInstance()->ExecuteS('
		SELECT e.id_employee, e.firstname, e.lastname FROM '._DB_PREFIX_.'employee e
		WHERE e.active = 1 ORDER BY e.lastname ASC');

		echo '
		<h2>'.$this->l('Messages').'</h2>
		<form action="'.Tools::htmlentitiesUTF8($_SERVER['REQUEST_URI']).'" method="post" enctype="multipart/form-data">
			<p>
				<img src="../img/admin/msg-forward.png" alt="" style="vertical-align: middle;" /> '.$this->l('Forward this discussion to an employee:').' 
				<select name="id_employee_forward" style="vertical-align: middle;" onchange="
					if ($(this).val() >= 0)
						$(\'#message_forward\').show(400);
					else
						$(\'#message_forward\').hide(200);
					if ($(this).val() == 0)
						$(\'#message_forward_email\').show(200);
					else
						$(\'#message_forward_email\').hide(200);
				">
					<option value="-1">'.$this->l('-- Choose --').'</option>
					<option value="0">'.$this->l('Someone else').'</option>';
		foreach ($employees AS $employee)
			echo '	<option value="'.(int)($employee['id_employee']).'">'.substr($employee['firstname'], 0, 1).'. '.$employee['lastname'].'</option>';
		echo '	</select>
				<div id="message_forward_email" style="display:none">
					<b>'.$this->l('E-mail').'</b> <input type="text" name="email" />
				</div>
				<div id="message_forward" style="display:none;margin-bottom:10px">
					<textarea name="message_forward" style="width: 500px; height: 80px; margin-top: 15px;" onclick="if ($(this).val() == \''.addslashes($this->l('You can add a comment here.')).'\') { $(this).val(\'\'); }">'.$this->l('You can add a comment here.').'</textarea><br />
					<input type="Submit" name="submitForward" class="button" value="'.$this->l('Forward this discussion').'" style="margin-top: 10px;" />
				</div>
			</p>
		</form>
		<div class="clear">&nbsp;</div>';
		
		$messages = Db::getInstance()->ExecuteS('
		SELECT ct.*, cm.*, cl.name subject, CONCAT(e.firstname, \' \', e.lastname) employee_name, CONCAT(c.firstname, \' \', c.lastname) customer_name, c.firstname
		FROM '._DB_PREFIX_.'customer_thread ct
		LEFT JOIN '._DB_PREFIX_.'customer_message cm ON (ct.id_customer_thread = cm.id_customer_thread)
		LEFT JOIN '._DB_PREFIX_.'contact_lang cl ON (cl.id_contact = ct.id_contact AND cl.id_lang = '.(int)$cookie->id_lang.')
		LEFT JOIN '._DB_PREFIX_.'employee e ON e.id_employee = cm.id_employee
		LEFT JOIN '._DB_PREFIX_.'customer c ON (IFNULL(ct.id_customer, ct.email) = IFNULL(c.id_customer, c.email))
		WHERE ct.id_customer_thread = '.(int)Tools::getValue('id_customer_thread').'
		ORDER BY cm.date_add DESC');
	
		echo '<div style="float:right">';

		$nextThread = Db::getInstance()->getValue('
		SELECT id_customer_thread FROM '._DB_PREFIX_.'customer_thread ct
		WHERE ct.status = "open" AND ct.date_add = (
			SELECT date_add FROM '._DB_PREFIX_.'customer_message
			WHERE (id_employee IS NULL OR id_employee = 0) AND id_customer_thread = '.(int)$thread->id.'
			ORDER BY date_add DESC LIMIT 1
		)
		'.($cookie->{'customer_threadFilter_cl!id_contact'} ? 'AND ct.id_contact = '.(int)$cookie->{'customer_threadFilter_cl!id_contact'} : '').'
		'.($cookie->{'customer_threadFilter_l!id_lang'} ? 'AND ct.id_lang = '.(int)$cookie->{'customer_threadFilter_l!id_lang'} : '').
		' ORDER BY ct.date_add ASC');
				
		if ($nextThread)
			echo $this->displayButton('
			<a href="'.$currentIndex.'&id_customer_thread='.(int)$nextThread.'&viewcustomer_thread&token='.$this->token.'">
				<img src="../img/admin/next-msg.png" title="'.$this->l('Go to the oldest next unanswered message').'" style="margin-bottom: 10px;" />
				<br />'.$this->l('Answer to the next unanswered message in this category').' &gt;
			</a>');
		else
			echo $this->displayButton('
			<img src="../img/admin/msg-ok.png" title="'.$this->l('Go to the oldest next unanswered message').'" style="margin-bottom: 10px;" />
			<br />'.$this->l('The other messages in this category have been answered'));

		if ($thread->status != "closed")
			echo $this->displayButton('
			<a href="'.$currentIndex.'&viewcustomer_thread&setstatus=2&id_customer_thread='.(int)Tools::getValue('id_customer_thread').'&viewmsg&token='.$this->token.'">
				<img src="../img/admin/msg-ok.png" style="margin-bottom:10px" />
				<br />'.$this->l('Set this message as handled').'
			</a>');
			
		if ($thread->status != "pending1")
			echo $this->displayButton('
			<a href="'.$currentIndex.'&viewcustomer_thread&setstatus=3&id_customer_thread='.(int)Tools::getValue('id_customer_thread').'&viewmsg&token='.$this->token.'">
				<img src="../img/admin/msg-pending.png" style="margin-bottom:10px" />
				<br />'.$this->l('Declare this message').'<br />'.$this->l('as "pending 1"').'<br />'.$this->l('(will be answered later)').'
			</a>');
		else
			echo $this->displayButton('
			<a href="'.$currentIndex.'&viewcustomer_thread&setstatus=1&id_customer_thread='.(int)Tools::getValue('id_customer_thread').'&viewmsg&token='.$this->token.'">
				<img src="../img/admin/msg-is-pending.png" style="margin-bottom:10px" />
				<br />'.$this->l('Click here to disable pending status').'
			</a>');
			
		if ($thread->status != "pending2")
			echo $this->displayButton('
			<a href="'.$currentIndex.'&viewcustomer_thread&setstatus=4&id_customer_thread='.(int)Tools::getValue('id_customer_thread').'&viewmsg&token='.$this->token.'">
				<img src="../img/admin/msg-pending.png" style="margin-bottom:10px" />
				<br />'.$this->l('Declare this message').'<br />'.$this->l('as "pending 2"').'<br />'.$this->l('(will be answered later)').'
			</a>');
		else
			echo $this->displayButton('
			<a href="'.$currentIndex.'&viewcustomer_thread&setstatus=1&id_customer_thread='.(int)Tools::getValue('id_customer_thread').'&viewmsg&token='.$this->token.'">
				<img src="../img/admin/msg-is-pending.png" style="margin-bottom:10px" />
				<br />'.$this->l('Click here to disable pending status').'
			</a>');
			
		echo '</div>';
		
		if ($thread->id_customer)
		{
			$customer = new Customer($thread->id_customer);
			$products = $customer->getBoughtProducts();
			$orders = Order::getCustomerOrders($customer->id);
			
			echo '<div style="float:left;width:600px">';
			if ($orders AND sizeof($orders))
			{
				$totalOK = 0;
				$ordersOK = array();
				$tokenOrders = Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).(int)($cookie->id_employee));
				foreach ($orders as $order)
					if ($order['valid'])
					{
						$ordersOK[] = $order;
						$totalOK += $order['total_paid_real'];
					}
				if ($countOK = sizeof($ordersOK))
				{
					echo '<div style="float:left;margin-right:20px;">
					<h2>'.$this->l('Orders').'</h2>
					<table cellspacing="0" cellpadding="0" class="table float">
						<tr>
							<th class="center">'.$this->l('ID').'</th>
							<th class="center">'.$this->l('Date').'</th>
							<th class="center">'.$this->l('Products').'</th>
							<th class="center">'.$this->l('Total paid').'</th>
							<th class="center">'.$this->l('Payment').'</th>
							<th class="center">'.$this->l('State').'</th>
							<th class="center">'.$this->l('Actions').'</th>
						</tr>';
						$irow = 0;
					foreach ($ordersOK AS $order)
						echo '<tr '.($irow++ % 2 ? 'class="alt_row"' : '').' style="cursor: pointer" onclick="document.location = \'?tab=AdminOrders&id_order='.$order['id_order'].'&vieworder&token='.$tokenOrders.'\'">
						<td class="center">'.$order['id_order'].'</td>
							<td>'.date("d/m/Y, H:i:s", strtotime($order['date_add'])).'</td>
							<td align="right">'.$order['nb_products'].'</td>
							<td align="right">'.Tools::displayPrice($order['total_paid_real'], new Currency((int)($order['id_currency']))).'</td>
							<td>'.$order['payment'].'</td>
							<td>'.$order['order_state'].'</td>
							<td align="center"><a href="?tab=AdminOrders&id_order='.$order['id_order'].'&vieworder&token='.$tokenOrders.'"><img src="../img/admin/details.gif" /></a></td>
						</tr>';
					echo '</table>
					<h3 style="color:green;font-weight:700;margin-top:10px">'.$this->l('Validated Orders:').' '.$countOK.' '.$this->l('for').' '.Tools::displayPrice($totalOK, new Currency(Configuration::get('PS_CURRENCY_DEFAULT'))).'</h3>
					</div>';
				}
			}
			if ($products AND sizeof($products))
			{
				echo '<div style="float:left;margin-right:20px">
				<h2>'.$this->l('Products').'</h2>
				<table cellspacing="0" cellpadding="0" class="table">
					<tr>
						<th class="center">'.$this->l('Date').'</th>
						<th class="center">'.$this->l('ID').'</th>
						<th class="center">'.$this->l('Name').'</th>
						<th class="center">'.$this->l('Quantity').'</th>
						<th class="center">'.$this->l('Actions').'</th>
					</tr>';
				$irow = 0;
				$tokenOrders = Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).(int)($cookie->id_employee));
				foreach ($products AS $product)
					echo '
					<tr '.($irow++ % 2 ? 'class="alt_row"' : '').' style="cursor: pointer" onclick="document.location = \'?tab=AdminOrders&id_order='.$product['id_order'].'&vieworder&token='.$tokenOrders.'\'">
						<td>'.date("d/m/Y, H:i:s", strtotime($product['date_add'])).'</td>
						<td>'.$product['product_id'].'</td>
						<td>'.$product['product_name'].'</td>
						<td align="right">'.$product['product_quantity'].'</td>
						<td align="center"><a href="?tab=AdminOrders&id_order='.$product['id_order'].'&vieworder&token='.$tokenOrders.'"><img src="../img/admin/details.gif" /></a></td>
					</tr>';
				echo '</table></div>';
			}
			echo '</div>';
		}
		
		echo '<div style="float:left;margin-top:10px">';
		foreach ($messages AS $message)
			echo $this->displayMsg($message);
		echo '</div><div class="clear">&nbsp;</div>';
	}
	
	private function displayButton($content)
	{
		return '
		<div style="margin-bottom:10px;border:1px solid #005500;width:200px;height:130px;padding:10px;background:#EFE">
			<p style="text-align:center;font-size:15px;font-weight:bold">
				'.$content.'
			</p>
		</div>';
	}
	
		
	public function displayListModificata()
	{
		global $currentIndex, $cookie;

		$this->displayTop();

		if ($this->edit AND (!isset($this->noAdd) OR !$this->noAdd))
			echo '<br /><a href="'.$currentIndex.'&add'.$this->table.'&token='.$this->token.'"><img src="../img/admin/add.gif" border="0" /> '.$this->l('Add new').'</a><br /><br />';
		/* Append when we get a syntax error in SQL query */
		if ($this->_list === false)
		{
			$this->displayWarning($this->l('Bad SQL query'));
		}
		$cookie->{$this->table.'_pagination'} = 100;
		/* Display list header (filtering, pagination and column names) */
		$this->displayListHeaderModificata();
		if (!sizeof($this->_list))
			echo '<tr><td class="center" colspan="'.(sizeof($this->fieldsDisplay) + 2).'">'.$this->l('No items found').'</td></tr>';

		/* Show the content of the table */
		$this->displayListContentModificata();

		/* Close list table and submit button */
		
		$this->displayListPagination(Tools::getValue('token'));
		$this->displayListFooter();
		echo '<div class="tab1" id="opz">';

			echo 'az';
		
		
		echo '</div>';
		echo '<div class="tab1" id="azioni">';

			if($cookie->profile == 7)
				echo 'Non hai i permessi per visualizzare questa sezione';
			else
			{
				echo '
				<link rel="stylesheet" href="jquery.treeview.css" type="text/css" />

				<script type="text/javascript" src="jquery.treeview.js"></script>

				<script type="text/javascript">

				// build treeview after web page has been loaded

				$(document).ready(function(){

				$(".tree-menu").treeview();
				
				$(".tasti-apertura").mouseover(function() {
					$(this).children(".menu-apertura").show();
				}).mouseout(function() {
					$(this).children(".menu-apertura").hide();
				});
				
				$(".tasti-apertura-in").mouseover(function() {
					$(this).children(".menu-apertura-in").show();
				}).mouseout(function() {
					$(this).children(".menu-apertura-in").hide();
				});


				});

				</script>
				';
			
				$c_emp = new Employee($cookie->id_employee);
				
				echo '
				<div class="tasti-apertura"><a class="button" style="display:block" href="#"><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" />&nbsp;&nbsp;&nbsp;Aggiungi nuova azione staff</a>
				<div class="menu-apertura">
					<ul class="dropdown">
						<li class="tasti-apertura-in"><a href="'.$currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&viewaction&aprinuovaazione&token='.$this->token.'&tab-container-1=3"><img src="../img/admin/return.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;To-do</a>
							<div class="menu-apertura-in" style="position:absolute; left:100px">
								<ul class="dropdown">
									<li><a style="color:#000" href="'.$currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&viewaction&aprinuovaazione&token='.$this->token.'&tipo-todo=Attivita&tab-container-1=3"><img src="../img/admin/icons/attivita.gif" alt="Attivita" title="Attivita" />&nbsp;&nbsp;&nbsp;Attivit&agrave;</a></li>
									
									<li><a href="'.$currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&viewaction&aprinuovaazione&token='.$this->token.'&tipo-todo=Telefonata&tab-container-1=3"><img src="../img/admin/icons/telefonata.gif" alt="Telefonata" title="Telefonata" />&nbsp;&nbsp;&nbsp;Telefonata</a></li>
									
									<li><a href="'.$currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&viewaction&aprinuovaazione&token='.$this->token.'&tipo-todo=Visita&tab-container-1=3"><img src="../img/admin/icons/visita.gif" alt="Visita" title="Visita" />&nbsp;&nbsp;&nbsp;Visita</a></li>
									
									<li><a href="'.$currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&viewaction&aprinuovaazione&token='.$this->token.'&tipo-todo=Caso&tab-container-1=3"><img src="../img/admin/icons/caso.gif" alt="Caso" title="Caso" />&nbsp;&nbsp;&nbsp;Caso</a></li>
									
									<li><a href="'.$currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&viewaction&aprinuovaazione&token='.$this->token.'&tipo-todo=Intervento&tab-container-1=3"><img src="../img/admin/icons/intervento.gif" alt="Intervento" title="Intervento" />&nbsp;&nbsp;&nbsp;Intervento</a></li>
									
									<li><a href="'.$currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&viewaction&aprinuovaazione&token='.$this->token.'&tipo-todo=Richiesta_RMA&tab-container-1=3"><img src="../img/admin/icons/richiesta_rma.gif" alt="Richiesta RMA" title="Richiesta RMA" />&nbsp;&nbsp;&nbsp;Richiesta RMA</a></li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
				</div>';
				echo '<div class="tab1" id="azioni-customer" style="clear:both">';
				echo '<br /><h2>TO DO</h2>';
				$impiegati_eza = Db::getInstance()->ExecuteS("SELECT id_employee, firstname, lastname FROM employee WHERE active = 1 ORDER BY firstname ASC, lastname ASC");
				
				if(isset($_GET['id_action'])) {
				
						$azione = Db::getInstance()->getRow("SELECT * FROM action_thread_employee at WHERE at.id_action = ".$_GET['id_action']." ");
						
						
						if(!$azione && Tools::getValue('id_action') != '') {
							echo "<span style='color:red'>ERRORE - Azione non trovata</span><br /><br /><br />"; 
						}
						
					$incaricato = new Employee($azione['action_to']);
					
						switch($azione['status']) {
								case 'open': $status_azione = "<img src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; $st_azione = 'Aperto'; break;
								case 'closed': $status_azione = "<img src='../img/admin/status_green.gif' alt='Chiuso' title='Chiuso' />"; $st_azione = 'Chiuso'; break;
								case 'pending1': $status_azione = "<img src='../img/admin/status_orange.gif' alt='In lavorazione' title='In lavorazione' />"; $st_azione = 'In lavorazione'; break;
								default: $status_azione = "<img src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; $st_azione = 'Aperto'; break;
							}
					
				
						$oggetto = Db::getInstance()->getValue("SELECT subject FROM action_thread_employee WHERE id_action = ".$azione['id_action']." ORDER BY date_add ASC");
					
						if($oggetto == '') {
							$oggetto = Db::getInstance()->getValue("SELECT action_message_employee FROM action_message_employee FORCE INDEX (PRIMARY) WHERE id_action = ".$azione['id_action']." ORDER BY date_add ASC");
						} else { }
						$dataultimo = Db::getInstance()->getValue("SELECT date_add FROM action_message_employee WHERE id_action = ".$azione['id_action']." ORDER BY date_add DESC");
					
					if(isset($_GET['id_action'])) {
					
						echo '<h2 style="float:left; margin-right:10px; color:red">ATTIVIT&Agrave; INTERNA STAFF '.$azione['id_action'].' - In carico a: '.$incaricato->firstname.' - Status: '.$st_azione.'</h2><br />';
					}
				}
				
				
					echo '
				<div class="clear:both"></div>
				';
				
				/*
				if(isset($_GET['error']) && $_GET['error'] == '1ac') {
					
					echo '<div style="width:100%; height:30px; border:1px solid red; color:red"> ERRORE: il messaggio non deve essere vuoto.</div>';
				}
				*/
				
				if(isset($_POST['cambiastatusae_listing'])) {

					
					Db::getInstance()->ExecuteS("UPDATE action_thread_employee SET status = '".Tools::getValue('cambiastatusae_listing')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_action = '".addslashes(Tools::getValue('id_action'))."'");
					
					
					switch(Tools::getValue('cambiastatusae_listing'))
					{
						case 'open': $switch_status = 3; break;
						case 'pending1': $switch_status = 4; break;
						case 'closed': $switch_status = 5; break;
						default: $switch_status = 10; break;
					}	
					
					Customer::Storico(Tools::getValue('id_action'), 'AE', $cookie->id_employee, $switch_status);

					Tools::redirectAdmin($currentIndex.'&id_employee='.$cookie->id_employee.'&updateemployee&token='.$this->token.'&conf=4&tab-container-1=3');
					
				}
				
				if(isset($_POST['cambiaincaricoae'])) {

					Customer::cambiaIncaricoVeloce(Tools::getValue('tipo'), Tools::getValue('cambiaincaricoae'), Tools::getValue('id_thread'), Tools::getValue('precedente_incarico'), Tools::getValue('id_ticket_univoco'), Tools::getValue('id_employee'));
			
					Tools::redirectAdmin($currentIndex.'&id_employee='.$cookie->id_employee.'&updateemployee&token='.$this->token.'&conf=4&tab-container-1=3');
					
				}
				
				
				if(isset($_POST['submitAzione'])) {
				
					$action_date_array = explode("-",Tools::getValue('action_date'));
					$action_date = $action_date_array[2]."-".$action_date_array[1]."-".$action_date_array[0]." ".Tools::getValue('action_hours').":".Tools::getValue('action_minutes').":00";
				
					
					if(strpos(Tools::getValue('action_date'),'0000') !== false) {
						$action_date = Db::getInstance()->getValue("SELECT date_action FROM action_thread_employee WHERE id_action = ".Tools::getValue('id_action')."");
					}
					
					$precedente_status = Db::getInstance()->getValue("SELECT status FROM action_thread_employee WHERE id_action = ".Tools::getValue('id_action')."");
					if($precedente_status != Tools::getValue('cambiastatusae'))
					{	
						switch(Tools::getValue('cambiastatusae'))
						{
							case 'open': $switch_status = 3; break;
							case 'pending1': $switch_status = 4; break;
							case 'closed': $switch_status = 5; break;
							default: $switch_status = 10; break;
						}	
						
						Customer::Storico(Tools::getValue('id_action'), 'AE', $cookie->id_employee, $switch_status);
			
					}

					
					Db::getInstance()->ExecuteS("UPDATE action_thread_employee SET subject = '".addslashes(Tools::getValue('azione_subject'))."', action_type = '".addslashes(Tools::getValue('action_type'))."', action_date = '".$action_date."', status = '".Tools::getValue('cambiastatusae')."', note = '".addslashes(Tools::getValue('action_note'))."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_action = '".addslashes(Tools::getValue('id_action'))."'");
					
					foreach ($_POST['note_nuova'] as $id_nota=>$nota_id) {
						
						echo $_POST['note_nota'][$id_nota].'<br />';
					
						if($_POST['note_nuova'][$id_nota] == 0) {
							
							$testo_nota = Db::getInstance()->getValue('SELECT note FROM note_attivita WHERE id_note = '.$id_nota.'');
							if($testo_nota != $_POST['note_nota'][$id_nota])
								Db::getInstance()->executeS("UPDATE note_attivita SET note = '".$_POST['note_nota'][$id_nota]."', date_upd = '".date("Y-m-d H:i:s")."', id_employee = '".$cookie->id_employee."' WHERE id_note = ".$id_nota.""); 
						}
						
						else {
							Db::getInstance()->executeS("INSERT INTO note_attivita
							(id_note,
							id_attivita,
							tipo_attivita,
							id_employee,
							note,
							date_add,
							date_upd)
							
							VALUES (
							NULL,
							'".Tools::getValue('id_action')."',
							'AE',
							'".$cookie->id_employee."',
							'".$_POST['note_nota'][$id_nota]."',
							'".date("Y-m-d H:i:s")."',
							'".date("Y-m-d H:i:s")."'
							)");
							
							
						}
					}
					
					foreach ($_POST['promemoria_nuovo'] as $id_promemoria=>$promemoria_id) {
								Db::getInstance()->execute('INSERT INTO ticket_promemoria (msg, tipo_att, ora, conto, tipo, status) VALUES ('.Tools::getValue('id_action').', "ae", "'.$action_date.'" - INTERVAL '.$_POST['action_minuti'][$id_promemoria].' '.$_POST['action_tipo_promemoria'][$id_promemoria] .', "'.$_POST['action_minuti'][$id_promemoria].'", "'.$_POST['action_tipo_promemoria'][$id_promemoria].'", "open")');
							}
							
				
					$impiegato_attuale = Db::getInstance()->getValue("SELECT action_to FROM action_thread_employee WHERE id_action = '".Tools::getValue('id_action')."'");
					
					if($impiegato_attuale != Tools::getValue('assegnaimpiegatoa')) {
							
						Customer::Storico(Tools::getValue('id_action'), 'AE', $cookie->id_employee, 2, Tools::getValue('assegnaimpiegatoa'));
						
						Db::getInstance()->ExecuteS("UPDATE action_thread_employee SET action_to = '".Tools::getValue('assegnaimpiegatoa')."' WHERE id_action = '".Tools::getValue('id_action')."'");
										
						$mailimp = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".Tools::getValue('assegnaimpiegatoa')."'");
										
						$tokenimp = Tools::getAdminToken("AdminCustomerThreads"."107".Tools::getValue('assegnaimpiegatoa'));
										
						$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomerThreads&id_employee=".Tools::getValue('id_employee')."&updateemployee&viewaction&id_action=".Tools::getValue('id_action')."&token=".$tokenimp.'&tab-container-1=3';
						$employeemess = new Employee($cookie->id_employee);
						
						$msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato l'azione staff numero ".Tools::getValue('id_action')." su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirla</a>.";
										
						if($impiegato_attuale != 0) {
											
							$mailimprev = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".$impiegato_attuale."'");
							$nomeimprev = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = '".$impiegato_attuale."'");
											
							$msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione dell'azione staff numero ".Tools::getValue('id_action')." su Ezdirect e l'ha passata a ".Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.Tools::getValue('assegnaimpiegatoa').'');
											
							$params = array(
							'{link}' => $linkimp,
							'{firma}' => '',
							'{msg}' => $msgimprev);
							
							if($impiegato_attuale != $cookie->id_employee)
							{			
								Mail::Send(5, 'msg_base', Mail::l('Gestione azione revocata', 5), 
									$params, $mailimprev, NULL, NULL, NULL, NULL, NULL, 
								_PS_MAIL_DIR_, true);
							}		
						}
										
						else {
										
						}
										
						$params = array(
						'{link}' => $linkimp,
						'{firma}' => '',
						'{msg}' => $msgimp);
										
						if($_POST['assegnaimpiegatoa'] != 0 && $_POST['assegnaimpiegatoa'] != $cookie->id_employee) {
										
							Mail::Send(5, 'msg_base', Mail::l('Azione assegnata a te su Ezdirect', 5), 
								$params, $mailimp, NULL, NULL, NULL, NULL, NULL, 
								_PS_MAIL_DIR_, true);
														
						} else { }
					}

					else {
					}
					
					//Tools::redirectAdmin($currentIndex.'&id_employee='.Tools::getValue('id_employee').'&updateemployee&viewaction&id_action='.Tools::getValue('id_action').'&token='.$this->token.'&conf=4&tab-container-1=3');
					
				}
						
				

				if(!isset($_GET['viewaction'])) {
				
					echo  Customer::BuildListing(Tools::getValue('id_employee'), 'to-do-employee');
					
				}
				
				
				else {
					
					/*$fta = Db::getInstance()->getValue('SELECT id_attivita FROM storico_attivita WHERE tipo_num = 1 AND id_attivita = '.Tools::getValue('id_action'));
					if((!$fta || $fta <= 0) && $_GET['id_action'] > 12210)*/
					if(!Tools::getIsset('conf') && !Tools::getIsset('filename'))
						Customer::Storico(Tools::getValue('id_action'), 'AE', $cookie->id_employee, 14);
				
					if (isset($_GET['filename'])) {
								$filename = $_GET['filename'];
								
								if(strpos($filename, ":::")) {
					
									$parti = explode(":::", $filename);
									$nomecodificato = $parti[0];
									$nomevero = $parti[1];
									
								}
					
								else {
									$nomecodificato = $filename;
									$nomevero = $filename;
					
								}
								
								if (file_exists(_PS_UPLOAD_DIR_.$nomecodificato)) {
									self::openUploadedFile();
								}
								else { }
							} 
							else { }
					
			
					if(isset($_GET['id_action'])) {
					
					
						echo '<div id="tab-container-azioni" class="tab-containers">
						
						
						
						<ul id="tab-container-azioni-nav" class="tab-containers-nav">
						<li><a href="#azioni-1"><strong>Generale</strong></a></li>
						<li><a href="#azioni-2"><strong>Cronologia</strong></a></li>
						<li><a href="#azioni-3"><strong>Gerarchia</strong></a></li>
						<li><a href="#azioni-4"><strong>Storico</strong></a></li>
						</ul>';
					}
					
					if(isset($_GET['id_action'])) {
						echo '
						
						<div class="yetii-azioni" id="azioni-1">
						<fieldset style="width:95%; background-color:#ffe8d4">';
						
						
						
						echo '
						<form id="modificaazione" action="'.$currentIndex.'&id_employee='.$c_emp->id.'&submitAzione&updateemployee&token='.$this->token.'&tab-container-1=3" method="post" autocomplete="off">';
						
						
						
						
						
						echo '<div class="anagrafica-cliente" style="width:800px; margin-bottom:10px">
						<strong>Oggetto</strong><br />
						
							<textarea name="azione_subject" id="azione_subjectContent" style="width:690px;height:40px;display:block;float:left" '.(substr($oggetto,0,20) == '*** VERIFICA TECNICA' ? 'readonly="readonly"' : '').'>'.strip_tags(htmlspecialchars_decode($oggetto)).'</textarea>
							
							
						<br />
						</div>

						
						<div class="clear"></div>
						<div class="anagrafica-cliente" style="width:103px">
						ID Azione<br />
						<span class="tab-span" style="width:103px">'.$azione['id_action'].'</span><br />
						</div>';
						
						$aperto_da = Db::getInstance()->getValue('SELECT action_m_from FROM action_message_employee WHERE id_action = '.$azione['id_action'].' ORDER BY id_action_message_employee ASC');
						$aperto_da = Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$aperto_da);
						echo '
						
						<div class="anagrafica-cliente" style="width:97px">
						Aperto da<br />
						<span class="tab-span" style="width:97px">'.$aperto_da.'</span><br />
						</div>
						
						<div class="anagrafica-cliente">
						In carico a<br />';
						echo '<select  style="width:180px" name="assegnaimpiegatoa" >';
						
						echo '<option value="0"'.($cookie->id_employee == 0 ? 'selected="selected"' : '').' >Da assegnare</option>';
						
						foreach ($impiegati_eza as $impez) {
						
							echo "<option value='".$impez['id_employee']."' ".($azione['action_to'] == $impez['id_employee'] ? 'selected="selected"' : '')." >".$impez['firstname']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>";
						
						}

						echo '</select>
						';
						echo '</span>
						</div>
						
						
						<div class="anagrafica-cliente">
						Data apertura<br />
						<span class="tab-span">'.Tools::displayDate($azione['date_add'], (int)($cookie->id_lang), true).'</span><br />
						</div>
						
						
						
						<div class="clear"></div>
						<div class="anagrafica-cliente">
						Status<br />
						'.$status_azione.' <input type="hidden" name="id_employee" value="'.Tools::getValue('id_employee').'" />
						<input type="hidden" name="id_action" value="'.Tools::getValue('id_action').'" />
						<select name="cambiastatusae">
						<option value="closed" '.($azione['status'] == 'closed' ? 'selected="selected"' : '').'>Chiuso</option>
						<option value="pending1" '.($azione['status'] == 'pending1' ? 'selected="selected"' : '').'>In lavorazione</option>
						<option value="open" '.($azione['status'] == 'open' ? 'selected="selected"' : '').'>Aperto</option>
						</select><br />
						</div>
						<div class="anagrafica-cliente">
						Tipo azione<br />
						
						<select name="action_type" style="width:180px" id="action_type">
								<option '.($azione['action_type'] == "Attivita" ? 'selected = "selected"' : '').' value="Attivita">Attivit&agrave;</option>
								<option '.($azione['action_type'] == "Telefonata" ? 'selected = "selected"' : '').' value="Telefonata">Telefonata</option>
								<option '.($azione['action_type'] == "Visita" ? 'selected = "selected"' : '').' value="Visita">Visita</option>
								<option '.($azione['action_type'] == "Caso" ? 'selected = "selected"' : '').' value="Caso">Caso</option>
								<option '.($azione['action_type'] == "Intervento" ? 'selected = "selected"' : '').' value="Intervento">Intervento</option>
								<option '.($azione['action_type'] == "Richiesta_RMA" ? 'selected = "selected"' : '').' value="Richiesta_RMA">Richiesta RMA</option>
								</select>
						
						
						
						</div>
						
						
						
						<div class="anagrafica-cliente">
						Data ultima com.<br />
						<span class="tab-span">'.Tools::displayDate($dataultimo, (int)($cookie->id_lang), true).'</span><br />
						</div>
						
						<div class="clear"></div>';
						
						if($azione['riferimento'] != '')
						{
							echo '
							<div class="clear"></div>
							<div class="anagrafica-cliente" style="width:690px">
							Azione padre<br />
							<span class="tab-span" style="width:690px">'.Customer::trovaSiglaLinkPerAlbero(substr($azione['riferimento'],1), substr($azione['riferimento'],0,1), '','y:A:'.$azione['id_action'].':'.$azione['status']).'</span><br />
							</div><div class="clear"></div>
							';
							
						}
						
						includeDatepicker3(array('action_date'), true);
						
						echo '<div class="anagrafica-cliente" style="width:500px">
						Data azione<br />
						<input type="text" id="action_date" style="width:100px" name="action_date" value="'.($azione['action_date'] == '0000-00-00 00:00:00' ? 'Nessuna data' : date("d-m-Y", strtotime($azione['action_date']))).'" />
						alle ore <input type="text" name="action_hours" maxlength="2" style="width:16px" value="'.date('H',strtotime($azione['action_date'])).'" />:
						<input type="text" name="action_minutes" maxlength="2" style="width:16px" value="'.date('i',strtotime($azione['action_date'])).'"  />
						
						<br /><br />Promemoria <div style="font-size:12px">
						';
						$promemoria_az = Db::getInstance()->executeS('SELECT * FROM ticket_promemoria WHERE tipo_att = "a" AND msg = '.$azione['id_action'].'');
						$i_p_a = 1;
						echo '<table>';
						foreach($promemoria_az as $p_a)
						{	
							switch($p_a['tipo']) {
								case 'DAY': $tipo_pm = 'giorni'; break;
								case 'HOUR': $tipo_pm = 'ore'; break;
								case 'MINUTE': $tipo_pm = 'minuti'; break;
								default: $tipo_pm = ''; break;
							}	
							echo '<tr id="pm_'.$i_p_a.'"><td>'.$i_p_a.') '.$p_a['conto'].' '.$tipo_pm.' prima (data: '.Tools::displayDate($p_a['ora'], $cookie->id_lang, true).')</td><td><a href="javascript:void(0)" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { deletePromemoria('.$p_a['msg'].', \''.$p_a['tipo_att'].'\', \''.$p_a['ora'].'\', '.$i_p_a.'); } else { return false; }"><img src="../img/admin/delete.gif" alt="Elimina" title="Elimina" /></a></td></tr>';
							$i_p_a++;
						}
						echo '</table>';
						echo '
						<script type="text/javascript">
						function deletePromemoria(id, tipo_att, ora, conteggio)
						{
							$.ajax({
							  url:"ajax.php?deletePromemoria=y",
							  type: "POST",
							  data: { msg: id, tipo_att: tipo_att, ora: ora
							  },
							  success:function(){
									$("#pm_"+conteggio).hide();
									alert("Promemoria cancellato con successo");
							  },
							  error: function(xhr,stato,errori){
								 alert("Errore nella cancellazione:"+xhr.status);
							  }
							});
							
							
						}
						</script>
						<script type="text/javascript">
						function add_promemoria() {
							var possible = "0123456789";
							var randomid = "";
							for( var i=0; i < 11; i++ ) {
								randomid += possible.charAt(Math.floor(Math.random() * possible.length));
							}
									
							$("<tr id=\'tr_persona_"+randomid+"\'><td><input type=\'hidden\' name=\'promemoria_nuovo[]\' value=\'1\' />Inviami promemoria: <input type=\'text\' style=\'width:30px\' name=\'action_minuti[]\' value=\'10\' /></td><td><select style=\'position:relative; height:21px;\' name=\'action_tipo_promemoria[]\'><option value=\'MINUTE\'>Minuti</option><option value=\'HOUR\'>Ore</option><option value=\'DAY\'>Giorni</option></select> prima <a href=\'javascript:void(0)\' onclick=\'togli_riga("+randomid+");\'><img src=\'../img/admin/delete.gif\' alt=\'Cancella\' title=\'Cancella\' style=\'cursor:pointer\' /></a></td></tr>").appendTo("#promemoriaTableBody");

						}
							
						function togli_riga(id) {
							document.getElementById(\'tr_persona_\'+id).innerHTML = "";
						}
						</script>
						
						
						 <table id="promemoriaTable"><tbody id="promemoriaTableBody"></tbody></table><br />
						<a href="javascript:void(0)" onclick="javascript:add_promemoria()";><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" /> Clicca per aggiungere un promemoria <img src="../img/admin/time.gif" style="width:20px" alt="Promemoria" title="Promemoria" /></a> <br /></td></tr>
						</div><br />
						
						</div>
						
						<div class="clear"></div>';
						
						$azione_note = Db::getInstance()->executeS('SELECT * FROM note_attivita WHERE tipo_attivita = "AE" AND id_attivita = '.$azione['id_action'].'');
						
						echo '<strong>Note interne visibili solo agli impiegati</strong><br />';
						
						echo '<table><thead>'.(count($azione_note) > 0 ? '<tr><th>Nota</th><th>Creata da</th><th>Ultima modifica</th><th></th></tr>' : '' ).'</thead><tbody id="tableNoteBody">
						';
						
						foreach($azione_note as $azione_nota)
						{
							echo '<tr id="tr_note_'.$azione_nota['id_note'].'">';
							echo '
							<td>
							<textarea class="textarea_note" name="note_nota['.$azione_nota['id_note'].']" id="note_nota['.$azione_nota['id_note'].']" style="width:540px; height:16px" onkeyup="auto_grow(this)">'.htmlentities($azione_nota['note'], ENT_COMPAT, 'UTF-8').'</textarea><input type="hidden" name="note_nuova['.$azione_nota['id_note'].']" id="note_nuova['.$azione_nota['id_note'].']" value="0" /></td>
							<td><input type="text" readonly="readonly" name="note_id_employee['.$azione_nota['id_note'].']" value="'.Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM employee WHERE id_employee = '.$azione_nota['id_employee']).'" /></td>
							<td><input type="text" readonly="readonly" name="note_date_add['.$azione_nota['id_note'].']" value="'.Tools::displayDate($azione_nota['date_upd'], 5).'" /></td>';
							echo '<td><a href="javascript:void(0)" onclick="javascript:var surec=window.confirm(\'Sei sicuro?\'); if (surec) { togli_nota('.$azione_nota['id_note'].'); cancella_nota_attivita('.$azione_nota['id_note'].'); } else { return false; }; "><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" style="cursor:pointer" /></a></td>';
							echo'
							</tr>
							';
						}
						
						echo '</tbody></table><br />';
						
						echo '
						<script type="text/javascript">
					function add_nota_privata() {
						var possible = "0123456789";
						var randomid = "";
						for( var i=0; i < 11; i++ ) {
							randomid += possible.charAt(Math.floor(Math.random() * possible.length));
						}
								
						$("<tr id=\'tr_note_"+randomid+"\'><td><textarea class=\'textarea_note\' name=\'note_nota[]\' onkeyup=\'auto_grow(this)\' id=\'note_nota[]\' style=\'width:540px;height:16px\'></textarea><input type=\'hidden\' name=\'note_nuova[]\' value=\'1\' /></td><td><input type=\'text\' readonly=\'readonly\' name=\'note_id_employee[]\' value=\''.Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM employee WHERE id_employee = '.$cookie->id_employee).'\' /></td><td><input type=\'text\' readonly=\'readonly\' name=\'note_date_add[]\' value=\''.date('d/m/Y').'\' /></td><td><a href=\'javascript:void(0)\' onclick=\'togli_nota("+randomid+");\'><img src=\'../img/admin/delete.gif\' alt=\'Cancella\' title=\'Cancella\' style=\'cursor:pointer\' /></a></td></tr>").appendTo("#tableNoteBody");
						
						$(".textarea_note").each(function () {
							this.style.height = ((this.scrollHeight)-4)+"px";
						});
						
						$(".textarea_note").on("change keyup keydown paste cut", "textarea", function () {
							$(this).height(0).height(this.scrollHeight);
						}).find("textarea").change();
						
						autosize($(".textarea_note"));

					}
						
					function togli_nota(id) {
						document.getElementById(\'tr_note_\'+id).innerHTML = "";
					}
						
						
					function cancella_nota(id) {
						$.ajax({
							  url:"ajax.php?cancellanota_customer=y",
							  type: "POST",
							  data: { 
							  cancella_nota: \'y\',
							  id_note: id
							  },
							  success:function(){
								alert("Nota cancellata con successo"); 
							  },
							  error: function(xhr,stato,errori){
								 alert("Errore nella cancellazione:"+xhr.status);
							  }
						});
					
					}
					
					function cancella_nota_attivita(id) {
						$.ajax({
							  url:"ajax.php?cancellanota_attivita=y",
							  type: "POST",
							  data: { 
							  cancella_nota: \'y\',
							  id_note: id
							  },
							  success:function(){
								alert("Nota cancellata con successo"); 
							  },
							  error: function(xhr,stato,errori){
								 alert("Errore nella cancellazione:"+xhr.status);
							  }
						});
					
					}
					
					function auto_grow(element) {
					}

					
					
					</script>
						<a href="javascript:void(0)" onclick="javascript:add_nota_privata()";><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" /> Clicca per aggiungere una nota privata</a> <br /><br />';
						
						if($azione['date_add'] < '2017-12-08')
						{
						echo '
							<textarea name="action_note" id="actionnoteContent" style="width:83%;height:100px">'.Tools::htmlentitiesUTF8($azione['note']).'</textarea><br />
							';
						}
						echo '<br /><div class="clear"></div>';
						
						
					
						echo '<button type="submit" class="button" name="submitAzione">
						<img src="../img/admin/save.gif" alt="Salva" title="Salva" />&nbsp;&nbsp;&nbsp;'.$this->l('   Save   ').'
						</button>
						
						<button class="button" onclick="javascript:var surec=window.confirm(\'Sei sicuro?\'); if (surec) { $(\'#coll_destinazione\').val($(\'#collega_todo\').val()); document.forms[\'collega_attivita\'].submit(); return false; } else { return false; }; " style="margin-left:10px; margin-right:0px">
						
						<img src="../img/admin/link.gif" alt="Collega" title="Collega" />&nbsp;&nbsp;&nbsp;'.$this->l('Collega a:').'
						</button>
						<select name="collega_todo" id="collega_todo" style="margin-left:-4px; height:27px;width:150px" >
							<option value="">-- Seleziona --</option>
							'.Customer::BuildSelectForLinking(Tools::getValue('id_employee'), $azione['id_action'], 'A').'
						</select>
						';
							
						
						echo '</form>
					
						<form style="margin-left:5px; display:block;float:left" name="collega_attivita" id="collega_attivita" action="'.htmlentities($_SERVER['REQUEST_URI']).'&conf=4" method="post" />
						<input type="hidden" name="submitCollegaAttivita" value="" />
						<input type="hidden" name="coll_destinazione" id="coll_destinazione" value="" />
						<input type="hidden" name="coll_partenza" id="coll_partenza" value="A'.$azione['id_action'].'" />
						</form>
						</fieldset><br /><br /></div>';
					
					}

				
					
					if(isset($_GET['id_action'])) {
						echo '<div class="yetii-azioni" id="azioni-2">';
					}
					
					$messaggia = Db::getInstance()->ExecuteS("SELECT * FROM action_message_employee am WHERE am.id_action = ".$_GET['id_action']."");
					
					foreach($messaggia as $messaggioa) {
					
						$employee_from = new Employee($messaggioa['action_m_from']);
						$employee_to = new Employee($messaggioa['action_m_to']);
						
						echo "<table style='width:100%; 
						border:1px solid #dfd5c3; background-color: #ffecf2;'margin-bottom:15px'>";
						echo '<tr>
							<td style="colspan:2">
								<table>
						
								<td style="font-size:14px; width:250px">Da: <strong>'.$employee_from->firstname." ".substr($employee_from->lastname,0,1).".".'<strong></td>
								<td style="width:170px"><strong>Data: </strong>'.date("d-m-Y H:i:s",strtotime($messaggioa['date_add'])).'</td>
								<td style="width:180px"><strong>Allegato: </strong>';
								if(!empty($messaggioa['file_name'])) {
								
									$allegatia = explode(";",$messaggioa['file_name']);
									$nalla = 1;
									foreach($allegatia as $allegatoa) {
										if(strpos($allegatoa, ":::")) {
							
											$parti = explode(":::", $allegatoa);
											$nomeveroa = $parti[1];
											
										}
							
										else {
											$nomeveroa = $allegatoa;
							
										}
										if($allegatoa == "") { } else {
											if($nalla == 1) { } else { echo " - "; }
											echo '<a href="'.$currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&viewaction&id_action='.$azione['id_action'].'&token='.$this->token.'&tab-container-1=3&filename='.$allegatoa.'"><span style="color:red">'.$nomeveroa.'</span></a>';
											$nallp++;
										}
									}
								
								}
								
								else { echo 'Nessun allegato'; }
								
								
								echo '</td>';
								
								$cc = Db::getInstance()->getValue("SELECT cc FROM ticket_cc WHERE type = 'ae' AND msg = ".$messaggioa['id_action_message_employee']."");
								if(!empty($cc)) {
								
									echo '<td>';
								
									$ccs = explode(";",$cc);
									$cc_str = "<strong>CC:</strong> ";
									
									foreach ($ccs as $conoscenza) {
									
										$imp = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".$conoscenza."");
										$cc_str .= $imp." ";
									}
									
									echo $cc_str;
									echo "</td>";
								
								}
								else {
								
									echo '<td><strong>CC:</strong> Nessuno</td>';
								
								}
								
								
								echo '</tr></table>
								
							</td>
							</tr>
							<tr>
							<td style="colspan:2">
								<table>
								<tr>
								<td style="width:100px"><strong>Messaggio</strong></td>
								<td>'.htmlspecialchars_decode($messaggioa['action_message_employee']).'
								<form method="post">
								<input type="hidden" name="modifica_messaggio_azione" value="'.htmlentities($messaggioa['action_message_employee'], ENT_QUOTES | ENT_SUBSTITUTE, "UTF-8").'" />
								<input type="hidden" name="modifica_messaggio_azione_id" value="'.$messaggioa['id_action_message_employee'].'" />
								<button type="submit" class="button" name="modifica_messaggio_submit" value="Modifica">
								<img src="../img/admin/edit.gif" alt="Modifica" title="Modifica" />&nbsp;&nbsp;&nbsp;Modifica
								</button>
								
								<button type="submit" class="button" name="cancella_messaggio_submit" value="Cancella" onclick="javascript:var surec=window.confirm(\'Sei sicuro/a?\'); if (surec) { return true; } else { return false; }; ">
								<img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" />&nbsp;&nbsp;&nbsp;Cancella
								</button>
								</form>
								</td>
								</tr>
								</table>
							</td>';
							echo '</tr>
							<tr><td style="colspan:2">
								<table>
								<tr>
								<td><strong>Inviato a: </strong>'.$employee_to->firstname.'</td>
								
								
								<td style="padding-left:20px"><strong>In carico a</strong></td>
								<td>'.($messaggioa['in_carico'] > 0 ? Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$messaggioa['in_carico']) : '--').'</td>
								</tr>
								</table>
							
							</td></tr>
							</table><br />';
					}
					
					
				
				if (Tools::isSubmit('submitActionReply'))
				{
					
					/*if(Tools::getValue('messaggioa') == '') {
				
				
						Tools::redirectAdmin($currentIndex.'&id_employee='.(int)$customer->id.'&updateemployee&error=1ac&token='.$this->token.'&tab-container-1=3');
								
				
					}*/
					
					$files=array();
						$fdata=$_FILES['joinFile'];
						if(is_array($fdata['name'])){
							for($i=0;$i<count($fdata['name']);++$i){
								$files[]=array(
									'name'    =>$fdata['name'][$i],
									'tmp_name'=>$fdata['tmp_name'][$i],
									'type' => $fdata['type'][$i],
									'size' => $fdata['size'][$i],
									'error' => $fdata['error'][$i],
									
								);
							}
						}
						else $files[]=$fdata;
											
						$attachments = array();
						
						
						foreach($files as $file) {
						
							if (isset($files) AND !empty($file['name']) AND $file['error'] != 0)
								$this->_errors[] = Tools::displayError('An error occurred with the file upload.');
								
								if (!empty($file['name']))
									{
										$extension = array('.txt', '.rtf', '.doc', '.docx', '.pdf', '.zip', '.png', '.jpeg', '.gif', '.jpg');
										$filename = md5(uniqid().substr($file['name'], -5));
										$fileAttachment['content'] = file_get_contents($file['tmp_name']);
										$fileAttachment['name'] = $file['name'];
										$fileAttachment['mime'] = $file['type'];
									}
									
									if (isset($filename) AND rename($file['tmp_name'], _PS_MODULE_DIR_.'../upload/'.$filename)) {
										$filename = $filename.":::".$file['name'];
										$file_name .= $filename.";";
								}
								
								$attachments[] = $fileAttachment;
								
						}					
					
					if(Tools::getValue('nuovaazione') == 1) {
						
						$ultimothread = Db::getInstance()->getValue("SELECT id_action FROM action_thread_employee ORDER BY id_action DESC");
						$thread = $ultimothread+1;
							
						$subject = "*** AZIONE STAFF - Tipo azione: ".Tools::getValue('action_type')." ***";
						
						$action_date_array = explode("-",Tools::getValue('action_date'));
						
						$action_date = $action_date_array[2]."-".$action_date_array[1]."-".$action_date_array[0]." ".Tools::getValue('action_hours').":".Tools::getValue('action_minutes').":00";
						
						foreach ($_POST['promemoria_nuovo'] as $id_promemoria=>$promemoria_id) {
							Db::getInstance()->execute('INSERT INTO ticket_promemoria (msg, tipo_att, ora, conto, tipo, status) VALUES ('.$thread.', "a", "'.$action_date.'" - INTERVAL '.$_POST['action_minuti'][$id_promemoria].' '.$_POST['action_tipo_promemoria'][$id_promemoria] .', "'.$_POST['action_minuti'][$id_promemoria].'", "'.$_POST['action_tipo_promemoria'][$id_promemoria].'", "open")');
						}
						
						if(strpos(Tools::getValue('action_date'),'0000') !== false) {
							$action_date = date("Y-m-d H:i:s");
						}

						Customer::Storico($thread, 'AE', $cookie->id_employee, 13);
						
						Db::getInstance()->ExecuteS("INSERT INTO action_thread_employee (id_action, action_type, id_employee, subject, action_from, action_to, action_date, status, riferimento, date_add, date_upd, note) VALUES ('$thread', '".Tools::getValue('action_type')."', '".Tools::getValue('id_employee')."', '".addslashes($subject)."', '".$cookie->id_employee."', '".Tools::getValue('in_carico_a_a')."', '".$action_date."', '".addslashes(Tools::getValue('statusa'))."', '".Tools::getValue('riferimento')."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."', '".addslashes(Tools::getValue('action_note'))."')");   
						
						
						Db::getInstance()->ExecuteS("INSERT INTO action_message_employee (id_action_message_employee, id_action, id_employee, action_m_from, action_m_to, in_carico, action_message_employee, file_name, date_add, date_upd) VALUES (NULL, '$thread', '".Tools::getValue('id_employee')."', '".$cookie->id_employee."', '".Tools::getValue('in_carico_a_a')."', '".Tools::getValue('in_carico_a_a')."', '".addslashes(Tools::getValue('messaggioa'))."', '".addslashes($file_name)."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
						
						
						$idmessaggio = mysql_insert_id();
						
						
					}
					
					else {
						$azione = Db::getInstance()->getRow("SELECT * FROM action_thread_employee at WHERE at.id_action = ".$_GET['id_action']."");
						$thread = $azione['id_action'];
						
						$precedente_status = Db::getInstance()->getValue("SELECT status FROM action_thread_employee WHERE id_action = ".$azione['id_action']."");
						if($precedente_status != Tools::getValue('statusa'))
						{	
							switch(Tools::getValue('statusa'))
							{
								case 'open': $switch_status = 3; break;
								case 'pending1': $switch_status = 4; break;
								case 'closed': $switch_status = 5; break;
								default: $switch_status = 10; break;
							}	
							
							Customer::Storico($azione['id_action'], 'AE', $cookie->id_employee, $switch_status);
				
						}
						
						$precedente_incarico = Db::getInstance()->getValue("SELECT action_to FROM action_thread_employee WHERE id_action = ".$azione['id_action']."");
						
						if($precedente_incarico != Tools::getValue('in_carico_a_a'))
							Customer::Storico($azione['id_action'], 'A', $cookie->id_employee, 2, Tools::getValue('in_carico_a_a'));
					
						Db::getInstance()->ExecuteS("UPDATE action_thread_employee SET  status = '".addslashes(Tools::getValue('statusa'))."', date_upd = '".date("Y-m-d H:i:s")."', action_to = '".Tools::getValue('in_carico_a_a')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_action = '".$azione['id_action']."'");  
					
					
						if(isset($_POST['modifica_messaggio_azione_id'])) {
							Customer::Storico($azione['id_action'], 'AE', $cookie->id_employee, 16);
							Db::getInstance()->ExecuteS("UPDATE action_message_employee SET action_message_employee = '".addslashes(Tools::getValue('messaggioa'))."' WHERE id_action_message_employee = ".$_POST['modifica_messaggio_azione_id'].""); 
							$idmessaggio = $_POST['modifica_messaggio_azione_id'];
						}
						else {
							
							Customer::Storico($azione['id_action'], 'AE', $cookie->id_employee, 15);
							Db::getInstance()->ExecuteS("INSERT INTO action_message_employee (id_action_message_employee, id_action, id_employee, action_m_from, action_m_to, in_carico, action_message_employee, file_name, date_add, date_upd) VALUES (NULL, '$thread', ".Tools::getValue('id_employee').", '".$cookie->id_employee."', '".Tools::getValue('in_carico_a_a')."', '".Tools::getValue('in_carico_a_a')."', '".addslashes(Tools::getValue('messaggioa'))."', '".addslashes($file_name)."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')"); 
							$idmessaggio = mysql_insert_id();
							$subject = Db::getInstance()->getValue('SELECT subject FROM action_thread_employee WHERE id_action = '.$thread);
						
							
						}
						
					}
					
					
					
						$file_csv=fopen("../cms/quotazioni/azioni.csv","a+");
						
						$in_carico_a =  Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".Tools::getValue('in_carico_a_a')."");
						
						$prima_azione = Db::getInstance()->getValue("SELECT id_action_message_employee FROM action_message_employee WHERE id_action = $thread ORDER BY id_action_message_employee ASC");
							
						if(!$prima_azione) {
										
							$prima_azione = $idmessaggio;
									
						}
									
						else {
									
						}
						
						$messaggiopercsv = Tools::getValue('messaggioa');
						$messaggiopercsv = strip_tags($messaggiopercsv);
						$messaggiopercsv = str_replace(";", ",", $messaggiopercsv);
							
						$messaggiopercsv = str_replace(chr(10), " ", $messaggiopercsv); //remove carriage returns
						$messaggiopercsv = str_replace(chr(13), " ", $messaggiopercsv); //remove carriage returns	
						$ref = rand(0, 99999); $ref2 = rand(0, 99999);
						
						$riga_richiesta = "".date("d/m/Y H:i:s").";".date('Ymd')."-".$ref."-".$ref2.";".$in_carico_a.";".Tools::getValue('action_type').";".Tools::getValue('statusa').";".$messaggiopercsv.";".$thread.";".$idmessaggio.";".$prima_azione.";".Tools::getValue('id_employee')."\n";
							
									
						
						
						
						$subj_per_ctrl = Db::getInstance()->getValue("SELECT subject FROM action_thread_employee WHERE id_action = ".$thread."");
						
						$pos = strpos($subj_per_ctrl,"ti richiamiamo noi");
						
						//if (strlen(stristr($subj_per_ctrl,"ti richiamiamo noi"))>0) {
						@fwrite($file_csv,$riga_richiesta);
						//} else {
							
						//}
						
						@fclose("../cms/quotazioni/azioni.csv","a+");	
					
						$mail_verso_cui_inviare = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = ".Tools::getValue('in_carico_a_a')."");
							
							
						$tokenimp = Tools::getAdminToken("AdminCustomerThreads"."107".Tools::getValue('in_carico_a_a'));
						$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomerThreads&id_employee=".Tools::getValue('id_employee')."&updateemployee&viewaction&id_action=".$thread."&token=".$tokenimp.'&tab-container-1=3';			
							
						if((Tools::getValue('messaggioa')) == '') {
							$testo_azione = "Il testo di questa azione &egrave; vuoto.";
						}
						else {
							$testo_azione = "Testo azione: ".(Tools::getValue('messaggioa'));
						}
						
						if($customer->is_company == 1) {
						
							$cliente = $customer->company;
						
						}
						else {
						
							$cliente = $customer->firstname." ".$customer->lastname;
						}
							
						
						
						/*
						if (Mail::Send((int)$cookie->id_lang, 'action', Mail::l('Aperta azione su Ezdirect', (int)$cookie->id_lang), 
							$params, $mail_verso_cui_inviare, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
							_PS_MAIL_DIR_, true))
						{
							//$ct->status = 'closed';
							//$ct->update();
						}
						*/
						$cc = "";
						$cc_str = "";
						foreach($_POST['conoscenza_a'] as $conoscenza) {
							
							$cc_str = $cc_str.Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$conoscenza).' ';
						}
						
						foreach($_POST['conoscenza_a'] as $conoscenza) {
							
							
						
							$cc .= $conoscenza.";";
							$tokenimp = Tools::getAdminToken("AdminCustomerThreads"."107".$conoscenza);
							$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomerThreads&id_employee=".Tools::getValue('id_employee')."&updateemployee&viewaction&id_action=".$thread."&token=".$tokenimp.'&tab-container-1=3';			
						
							echo $conoscenza."<br />";
						
							if((Tools::getValue('messaggioa')) == '') {
								$testo_azione = "Il testo di questa azione &egrave; vuoto.";
							}
							else {
								$testo_azione = "Testo azione: ".(Tools::getValue('messaggioa'));
							}
										
							$params = array(
							'{reply}' => "Ti &egrave; stata inviata una azione staff in copia conoscenza su Ezdirect.<br /><br />
							Azione aperta da: <strong>".$employeemess->firstname."</strong><br /><br />
							Azione in carico a: <strong>".$in_carico_a."</strong><br /><br />
							Tipo azione: <strong>".Tools::getValue('action_type')."</strong><br /><br />".$testo_azione."<br /><br />
							".($cc_str != '' ? "Il messaggio &egrave; stato inviato in conoscenza a: ".$cc_str."<br /><br />" : "")."
							Per aprire questa azione, <a href='".$linkimp."'>clicca su questo link</a>.");
						
							$mailconoscenza = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = $conoscenza");
									
							Mail::Send((int)$cookie->id_lang, 'senzagrafica', Mail::l('Azione in copia conoscenza', (int)$cookie->id_lang), 
							$params, $mailconoscenza, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
							_PS_MAIL_DIR_, true);
								
						}
						
						$params = array(
						'{reply}' => "Ti &egrave; stata inviata una azione staff su Ezdirect.<br /><br />Azione aperta da: <strong>".$employeemess->firstname."</strong><br /><br />Azione in carico a: <strong>".$in_carico_a."</strong><br /><br />Tipo azione:<strong>".Tools::getValue('action_type')."</strong><br /><br />".$testo_azione."<br /><br />".($cc_str != '' ? "Il messaggio &egrave; stato inviato in conoscenza a: ".$cc_str."<br /><br />" : "")."Per leggere questa azione o per rispondere, <a href='".$linkimp."'>clicca su questo link</a>.");
						
						if(!empty($_POST['conoscenza_a'])) {
								
							Db::getInstance()->executeS("INSERT INTO ticket_cc (msg, type, cc) VALUES (".$idmessaggio.", 'ae', '".$cc."')"); 
								
								
						}
						
						if(Tools::getValue('in_carico_a_a') != 0) {

						
							$mail_incarico_a = Db::getInstance()->getValue('SELECT email FROM employee WHERE id_employee = '.Tools::getValue('in_carico_a_a').'');
							
							if(Tools::getValue('in_carico_a_a') == $cookie->id_employee) {
							}
							else {
								Mail::Send(5, 'action', Mail::l('Azione assegnata a te su Ezdirect', 5), 
								$params, $mail_incarico_a, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
								_PS_MAIL_DIR_, true);
							}
						}
						
						Tools::redirectAdmin($currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&id_employee='.Tools::getValue('id_employee').'&viewaction&id_action='.$thread.'&conf=4&token='.$this->token.'&tab-container-1=3');
						
						
						
			
				}
				else 
				{
					
						if (isset($_GET['actionreply'])) {
					
						echo 'Risposta inviata con successo.<br /> <a class="button" href="'.$currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&viewaction&id_action='.$azione['id_action'].'&token='.$this->token.'&tab-container-1=3"><strong>Clicca qui se vuoi rispondere nuovamente.</strong></a><br /><br />';
						
						}
						
						else if (isset($_GET['actionreply2'])) {
					
						echo 'Azione aperta con successo. <br /><br />';
						
						}
					
						else if (!isset($_GET['actionreply']) || isset($_GET['aprinuovaazione'])) {
						$iso = Language::getIsoById((int)($cookie->id_lang));
						$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
						$ad = dirname($_SERVER["PHP_SELF"]);
				
				
					if(isset($_GET['aprinuovaazione'])) {
					
						echo '<h2>Apri nuova azione</h2>';
					
					
					}
				
					else {
					
						echo '<h2 id="rispondi-azione">Rispondi a questa azione</h2>';
						if(isset($_POST['modifica_messaggio_azione'])) {
							
							if(Tools::isSubmit('cancella_messaggio_submit'))
							{
								$id_action = Db::getInstance()->getValue('SELECT id_action FROM action_message_employee WHERE id_action_message_employee = '.Tools::getValue('modifica_messaggio_azione_id'));
								Db::getInstance()->getInstance()->execute('DELETE FROM action_message_employee WHERE id_action_message_employee = '.Tools::getValue('modifica_messaggio_azione_id'));
								
								Tools::redirectAdmin($currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&conf=4&token='.$this->token.'&viewaction&id_action='.$id_action.'&tab-container-1=3');
							}
							else
							{
								echo '<script type="text/javascript">
									 window.location = "#rispondi-azione";
								</script>';
							}
						}
						
					}
				
				
				
						echo '
					<script type="text/javascript">
					var iso = \''.$isoTinyMCE.'\' ;
					var pathCSS = \''._THEME_CSS_DIR_.'\' ;
					var ad = \''.$ad.'\' ;
					</script>
					
					
					<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce3/tiny_mce.js"></script>
					<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>
					<script type="text/javascript">
						$(document).ready(function () {
						
					dataChanged = 0;     // global variable flags unsaved changes      

					function bindForChange(){    
						$(\'input,checkbox,text,textarea,radio,select\').bind(\'change\',function(event) { dataChanged = 1});
						$(\'#messaggiop\').bind(\'input propertychange\', function() { dataChanged = 1 });
						$(\':reset,:submit\').bind(\'click\',function(event) { dataChanged = 0 });
					}


					function askConfirm(){  
						if (dataChanged){ 
							return "You have some unsaved changes.  Press OK to continue without saving." 
						}
					}

					window.onbeforeunload = askConfirm;
					window.onload = bindForChange;
					});
					</script>
					<script type="text/javascript">
							toggleVirtualProduct(getE(\'is_virtual_good\'));
							unitPriceWithTax(\'unit\');
					</script>';
					$employeemessa = new Employee($cookie->id_employee);
						echo'	<form name="submit_azione" id="submit_azione" action="'.$currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&viewaction'.(Tools::getIsset('id_action') ? '&id_action='.Tools::getValue('id_action') : '').'&token='.$this->token.'&tab-container-1=3" method="post" class="std" enctype="multipart/form-data">
						<fieldset style="background-color:#ffe8d4">';
				
						
						if(isset($_GET['aprinuovaazione'])) {
					
						echo '<input type="hidden" name="nuovaazione" value="1" /><input type="hidden" name="riferimento" id="riferimento" value="'.Tools::getValue('riferimento').'" />';
					
						}
						
						else {
						
							echo '			<script type="text/javascript">
							
								$(document).ready(function () {
									
									$("#submit_preventivo").submit(function () {
										d = document.getElementById("statusa").value;
										
										if(d != "'.$azione['status'].'") {
											
												}
										
										else {
											var salvamodifiche = window.confirm("STATUS: OK per procedere, ANNULLA per cambiare."); 
									
												if (salvamodifiche) {
													submitFormOkay = true;
													
													$(\'#waiting1\').waiting({ 
													elements: 10, 
													auto: true 
													});
													var overlay = jQuery(\'<div id="overlay"></div>\');
													var attendere = jQuery(\'<div id="attendere">ATTENDERE PREGO</div>\');
													overlay.appendTo(document.body);
													attendere.appendTo(document.body);
													$("#submitMessage").trigger("click");
												
												} else {
													 return false;
														
													//niente
												}
										}
									});
									
									
								});
					
							</script>';	
							
						}
						
						
						
							echo '<table>
							';
							
							
									
							
							if(isset($_GET['aprinuovaazione'])) {
							
								echo '<tr><td>Tipo azione</td>
								<td>
								<select name="action_type" id="action_type">
								<option value="Attivita" '.(Tools::getIsset('tipo-todo') && Tools::getValue('tipo-todo') == 'Attivita' ? 'selected="selected"' : '' ).'>Attivit&agrave;</option>
								<option value="Telefonata" '.(Tools::getIsset('tipo-todo') && Tools::getValue('tipo-todo') == 'Telefonata' ? 'selected="selected"' : '' ).'>Telefonata</option>
								<option value="Visita" '.(Tools::getIsset('tipo-todo') && Tools::getValue('tipo-todo') == 'Visita' ? 'selected="selected"' : '' ).'>Visita</option>
								<option value="Caso" '.(Tools::getIsset('tipo-todo') && Tools::getValue('tipo-todo') == 'Caso' ? 'selected="selected"' : '' ).'>Caso</option>
								<option value="Intervento" '.(Tools::getIsset('tipo-todo') && Tools::getValue('tipo-todo') == 'Intervento' ? 'selected="selected"' : '' ).'>Intervento</option>
								<option value="TODO Amazon" '.(Tools::getIsset('tipo-todo') && Tools::getValue('tipo-todo') == 'TODO Amazon' ? 'selected="selected"' : '' ).'>TODO Amazon</option>
								<option value="TODO Social" '.(Tools::getIsset('tipo-todo') && Tools::getValue('tipo-todo') == 'TODO Social' ? 'selected="selected"' : '' ).'>TODO Social</option>
								<option value="TODO Sendinblue" '.(Tools::getIsset('tipo-todo') && Tools::getValue('tipo-todo') == 'TODO Sendinblue' ? 'selected="selected"' : '' ).'>TODO Sendinblue</option>
								</select>
								</td>
								</tr>';
								
							} else {
							
								echo '<input type="hidden" name="action_type" value="'.($azione['action_type']).'" />';
							
							}
							
						echo '<tr>
										<td>
										';
										
										
										echo '
										<input type="hidden" name="id_employee" value="'.$c_emp->id.'" />
										
										<input type="hidden" value="'.$thread['id_employee_thread'].'" name="id_employee_thread">
										</td>
									</tr>
							
									';
									
								
								echo '
								<tr><td>Stato azione</td>
								<td>
								<div style="float:left">
								<select name="statusa" id="statusa">
								<option value="open" '.($azione['status'] == 'open' ? 'selected="selected"' : '').'>Aperto   </option>
								<option value="pending1" '.($azione['status'] == 'pending1' ? 'selected="selected"' : '').'>In lavorazione   </option>
								<option value="closed" '.($azione['status'] == 'closed' ? 'selected="selected"' : '').'>Chiuso   </option>
								
								</select>
									</div><div style="float:left; margin-left:20px">
									In carico a';
							echo '
											
						<script type="text/javascript">
										function changeInCaricoAA()
										{';
										
										
										foreach ($impiegati_eza as $impez) {
											echo '
											
											firstVar = document.getElementById("in_carico_a_a").value;
											
											if (document.getElementById("in_carico_a_a").value == "'.$impez['id_employee'].'") {
											
											
											document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').disabled = true;
											document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').checked = false;
											
											} else { document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').disabled = false; }
										';
										}
									echo '
										}
										</script>
						<select style="width:250px" name="in_carico_a_a" id="in_carico_a_a" onChange = "changeInCaricoAA();">';

						
						foreach ($impiegati_eza as $impezp) {

						
							if(isset($_GET['aprinuovaazione'])) {
								echo "<option value='".$impezp['id_employee']."' ".(Tools::getValue('id_employee') == $impezp['id_employee'] ? 'selected="selected"' : '')." >".$impezp['firstname']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>";
							}
							else {
								echo "<option value='".$impezp['id_employee']."' ".($azione['action_to'] == $impezp['id_employee'] ? 'selected="selected"' : '')." >".$impezp['firstname']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>";
							}
						}

						echo '</select></div></td>
									</tr>
									<tr><td>Conoscenza</td>
									<td>
									';
									$impez_count = 0;
									foreach ($impiegati_eza as $impez) {
										$impez_count++;
										if($impez_count == 10)
											echo '<br />';
										
										echo "<input type='checkbox' name='conoscenza_a[]' id ='conoscenza_".$impez['id_employee']."' value='".$impez['id_employee']."' /> ".$impez['firstname']."&nbsp;&nbsp;&nbsp;&nbsp;";
									
									}
									echo '<br /></td>
									</tr>
								<tr><td><br /></td></tr>
								';
								if(isset($_GET['aprinuovaazione'])) {
									includeDatepicker3(array('action_date'), true);
									
									echo '<tr><td>Data azione</td>
									<td><input type="text" id="action_date" style="width:100px" name="action_date" value="'.($azione['action_date'] == '0000-00-00 00:00:00' ? 'Nessuna data' : date("d-m-Y")).'" />
						alle ore <input type="text" name="action_hours" maxlength="2" style="width:16px" value="'.date('H').'" />:
						<input type="text" name="action_minutes" maxlength="2" style="width:16px" value="'.date('i').'"  />
						</td>
									</tr>
									<tr><td><br /></td></tr>
									<tr><td>Promemoria</td><td>
									
									<script type="text/javascript">
									function add_promemoria() {
										var possible = "0123456789";
										var randomid = "";
										for( var i=0; i < 11; i++ ) {
											randomid += possible.charAt(Math.floor(Math.random() * possible.length));
										}
												
										$("<tr id=\'tr_persona_"+randomid+"\'><td><input type=\'hidden\' name=\'promemoria_nuovo[]\' value=\'1\' />Inviami promemoria: <input type=\'text\' style=\'width:30px\' name=\'action_minuti[]\' value=\'10\' /></td><td><select style=\'position:relative; height:21px;\' name=\'action_tipo_promemoria[]\'><option value=\'MINUTE\'>Minuti</option><option value=\'HOUR\'>Ore</option><option value=\'DAY\'>Giorni</option></select> prima <a href=\'javascript:void(0)\' onclick=\'togli_riga("+randomid+");\'><img src=\'../img/admin/delete.gif\' alt=\'Cancella\' title=\'Cancella\' style=\'cursor:pointer\' /></a></td></tr>").appendTo("#promemoriaTableBody");

									}
										
									function togli_riga(id) {
										document.getElementById(\'tr_persona_\'+id).innerHTML = "";
									}
									</script>
									
									
									 <table id="promemoriaTable"><tbody id="promemoriaTableBody"></tbody></table><br />
									<a href="javascript:void(0)" onclick="javascript:add_promemoria()";><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" /> Clicca per aggiungere un promemoria</a> <br /></td></tr>
									';
								} else { }
								
								echo '<tr><td><br /></td></tr>';
								echo '<tr><td><script type="text/javascript" src="../js/select2.js"></script>
							<script type="text/javascript">
							$(document).ready(function() { $("#utils_autocomplete_input_action").select2(); $("#messaggi_precompilati").select2(); });
						
						function addUtil_TR(event, data, formatted)
							{
								var $body = $(tinymce.activeEditor.getBody());
								$body.prepend(\'<p>\');
								$body.append(\'</p>\');
								if(document.getElementById("utils_autocomplete_input_action").value != "-- Seleziona un link utile --")
								{
									$body.find("p:last").append(\'<a href="\' + document.getElementById("utils_autocomplete_input_action").value + \'">\' + document.getElementById("utils_autocomplete_input_action").value + \'<\/a><br /><br />\');
								}
								
							}	</script>
						Link utili</td><td><select name="utils_autocomplete_input_action" id="utils_autocomplete_input_action" style="width:730px" onchange="addUtil_TR();">
						'.$utils_options.'</select></td></tr>';
								echo '<tr><td>Messaggi precompilati</td><td><select name="messaggi_precompilati" id="messaggi_precompilati" style="width:730px" onchange="inserisciPrecompilato(this.value);">
						';
						include('precompilati.html');
						echo '
						</select>
						<script type="text/javascript">
						function inserisciPrecompilato(msg)
						{
							var $body = $(tinymce.activeEditor.getBody());
							$body.html(\'\');
							$body.prepend(\'<p>\');
							$body.append(\'</p>\');
							if(msg != "-- Seleziona un messaggio --") {
								$body.find("p:last").append(msg);
							}
						}
						</script>
						</td></tr>
						';
						
						
								echo '<tr><td>Allega file</td>
								<td>
								<input name="MAX_FILE_SIZE" value="20000000" type="hidden">
								<script type="text/javascript" src="jquery.MultiFile.js"></script>
								<input name="joinFile[]" class="multi" id="joinFile" type="file" onchange="handleFileSelect(this.files)">
									<output id="list-tkt">Anteprime: </output>
										<script type="text/javascript">
									function handleFileSelect(files) {
									
										// Loop through the FileList and render image files as thumbnails.
										 for (var i = 0; i < files.length; i++) {
											var f = files[i];
											var name = files[i].name;
				  
											var reader = new FileReader();  
											reader.onload = function(e) {  
											  // Render thumbnail.
											  var span = document.createElement("span");
											  span.innerHTML = ["<a href=\'",e.target.result,"\' target=\'_blank\'>",name,"</a> - "].join("");
											  document.getElementById("list-tkt").insertBefore(span, null);
											};

										  // Read in the image file as a data URL.
										  reader.readAsDataURL(f);
										}
									  }
									 
									</script>
								</td>
								</tr>
								<tr><td>Messaggio</td>
								<td>
							
								<textarea style="width:760px" class="rte" id="messaggioa" name="messaggioa" rows="5" cols="40">
								'.(isset($_POST['modifica_messaggio_submit']) ? stripslashes($_POST['modifica_messaggio_azione']) : '').'
								</textarea>';
								
								if(isset($_POST['modifica_messaggio_submit'])) {
								
									echo "<input type='hidden' name='modifica_messaggio_azione_id' value='".$_POST['modifica_messaggio_azione_id']."' />";
								}
								
								//  onclick = "this.style.visibility=\'hidden\', loading.style.visibility=\'visible\'"
								echo '</td>
								</tr>
								<!-- <tr>
									<td>Nota staff</td>
									<td><textarea id="action_note" name="action_note" rows="5" cols="145"></textarea>
									</tr>-->
								<tr><td></td>
								<td>
									<button type="submit" class="button"  name="submitActionReply" id="submitActionReply" value="Modifica">
									<img src="../img/admin/enabled-2.gif" alt="Conferma" title="Conferma" />&nbsp;&nbsp;&nbsp;Conferma azione
									</button>
								</td>
								</tr> 
							</table>
						</fieldset>
					</form>
					';
					if(isset($_GET['id_action'])) {
					echo '</div>';	//chiudo azioni-2
				
					}
				
					
					if(isset($_GET['id_action'])) {
						echo '<div class="yetii-azioni" id="azioni-3">';
					
					$first = Customer::HierarchyFirst($azione['id_action'], 'AE');
					
					echo '<div class="tree">';
					echo '<ul class="tree-menu"><li>'.Customer::trovaSiglaLinkPerAlbero(substr($first,1), substr($first,0,1)).'';
					echo str_replace('<ul></ul>','',Customer::HierarchyFindChildren($first, $children, 'AE'.$azione['id_action']));
					echo '</li></ul>';
					echo '</div>';
					echo '<div style="clear:both"></div><br /><br />';
					
					
						echo '</div>'; // chiudo azioni-3
					}	
					
					
					echo '<div class="yetii-azioni" id="azioni-4">';
				
					$storico_ticket = Db::getInstance()->executeS('SELECT * FROM storico_attivita WHERE id_attivita = '.$azione['id_action'].' AND tipo_attivita = "AE" ORDER BY data_attivita DESC, desc_attivita DESC');
					
					if(sizeof($storico_ticket) > 0)
					{
						echo '<table class="table">';
						echo '<thead><tr><th>Persona</th><th>Azione</th><th>Data</th></tr></thead><tbody>';
						
						foreach($storico_ticket as $storico)
						{
							if(is_numeric(substr($storico['desc_attivita'], -1)))
							{	
								$employee = filter_var($storico['desc_attivita'], FILTER_SANITIZE_NUMBER_INT);
								$name_employee = Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM employee WHERE id_employee = '.$employee);
								$desc_attivita = 'Ha messo l\'attivit&agrave; in carico a '.$name_employee;
							}
							else
								$desc_attivita = $storico['desc_attivita'];
							
							echo '<tr><td>'.($storico['id_employee'] == 0 ? 'Cliente' : Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM employee WHERE id_employee = '.$storico['id_employee'])).'</td><td>'.$desc_attivita.'</td><td>'.Tools::displayDate($storico['data_attivita'],$cookie->id_lang, true).'</td></tr>'; 
							
						}	
						
						echo '</tbody></table><br /><br />';
					}
						echo '</div>'; // chiudo azioni-4
					
				
				}
				
				}
				
					
				}
				
				
				
				echo '<br /><br /><a href="'.$currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&token='.$this->token.'&tab-container-1=3" class="button"><img src="../img/admin/arrow-left.png" alt="Back" title="Back" />&nbsp;&nbsp;&nbsp;Torna alla lista delle azioni</a>';
				echo '</div>';
			}	
		
		
		echo '</div>';
		
		echo '<script type="text/javascript">
		var tabber2 = new Yetii({
		id: "tab-container-azioni",
		tabclass: "yetii-azioni",
		'.(isset($_POST['modifica_messaggio_azione']) ? 'active: 2,' : '').'
		});
		</script>';
		// fine azioni
		
		echo '
		</div> <!-- fine tab container 1 -->
		<script type="text/javascript">
		var tabber1 = new Yetii({
		id: "tab-container-1",
		tabclass: "tab1",
		});
		</script>';
		echo '<br /><br />';
		
		echo '<br /><a href="'.$currentIndex.'&token='.Tools::getValue('token').'">Lista completa ticket/azioni/messaggi</a>';
		
		$params = array(
			$this->l('Total threads') => $all = Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'customer_thread'),
			$this->l('Threads pending') => $pending = Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'customer_thread WHERE status LIKE "%pending%"'),
			$this->l('Total customer messages') => Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'customer_message WHERE id_employee = 0'),
			$this->l('Total employee messages') => Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'customer_message WHERE id_employee != 0'),
			$this->l('Threads unread') => $unread = Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'customer_thread WHERE status = "open"'),
			$this->l('Threads closed') => $all - ($unread + $pending));

		/*echo '<div style="padding 0px;border:1px solid #CFCFCF;width:280px;">
				<h3 class="button" style="margin:0;line-height:23px;height:23px;border:0;padding:0 5px;">'.$this->l('Customer service').' : '.$this->l('Statistics').'</h3>
				<table cellspacing="1" class="table" style="border-collapse:separate;width:280px;border:0">';
		$count = 0;
		foreach ($params as $key => $val)
			echo '<tr '.(++$count % 2 == 0 ? 'class="alt_row"' : '').'><td>'.$key.'</td><td>'.$val.'</td></tr>';
		echo '	</table>
			</div>';
		*/
	}
	
	
	public function displayListContentModificata($token = NULL)
	{
		/* Display results in a table
		 *
		 * align  : determine value alignment
		 * prefix : displayed before value
		 * suffix : displayed after value
		 * image  : object image
		 * icon   : icon determined by values
		 * active : allow to toggle status
		 */

		global $currentIndex, $cookie;
		$currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));

		$id_category = 1; // default categ
		echo "</form>";
		$irow = 0;
		if ($this->_list AND isset($this->fieldsDisplay['position']))
		{
			$positions = array_map(create_function('$elem', 'return (int)($elem[\'position\']);'), $this->_list);
			sort($positions);
		}
		if ($this->_list)
		{
			$isCms = false;
			if (preg_match('/cms/Ui', $this->identifier))
				$isCms = true;
			$keyToGet = 'id_'.($isCms ? 'cms_' : '').'category'.(in_array($this->identifier, array('id_category', 'id_cms_category')) ? '_parent' : '');
			foreach ($this->_list AS $tr)
			{
				$id = $tr[$this->identifier];
				$contact_type = $tr['id_contact'];
				echo '<tr id="'.$tr['id_customer_thread'].'" rel="'.($tr['id_customer'] == 'Staff' ? 'AE' : $tr['id_contact']).'" '.($irow++ % 2 ? ' class="alt_row"' : 'class="odd_row"').' '.((isset($tr['color']) AND $this->colorOnBackground) ? 'style="background-color: '.$tr['color'].'"' : '').'>
							<td class="center">';
				if ($this->delete AND (!isset($this->_listSkipDelete) OR !in_array($id, $this->_listSkipDelete)))
					echo '<input type="checkbox" name="'.$this->table.'Box[]" value="'.$id.':'.$contact_type.'" class="noborder" />';
				echo '</td>';
				foreach ($this->fieldsDisplay AS $key => $params)
				{
					$tmp = explode('!', $key);
					$key = isset($tmp[1]) ? $tmp[1] : $tmp[0];
					echo '
					<td '.(isset($params['position']) ? ' id="td_'.(isset($id_category) AND $id_category ? $id_category : 0).'_'.$id.'"' : '').' class="'.((!isset($this->noLink) OR !$this->noLink) ? 'pointer' : '').((isset($params['position']) AND $this->_orderBy == 'position')? ' dragHandle' : ''). (isset($params['align']) ? ' '.$params['align'] : '').'" ';
					
					
					// MODIFICARE QUI
					$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee));
					$idcustct = Db::getInstance()->getValue("SELECT id_customer FROM customer_thread ct WHERE ct.id_customer_thread = ".$id."");
					
					if($params['title'] == 'Status' || $params['title'] == 'In carico a') {
						echo '>';
					}
					else {
					
					
						if($tr['id_customer'] == 0 && $tr['id_customer'] != 'Staff') {
						
							echo ' onclick="document.location = \''.$currentIndex.'&'.$this->identifier.'='.$id.($this->view? '&view' : '&update').$this->table.'&token='.($token!=NULL ? $token : $this->token).'\'">'.(isset($params['prefix']) ? $params['prefix'] : '');
						}
						
						else {
							
							if($tr['id_contact'] == 'preventivo') {
								if (!isset($params['position']) AND (!isset($this->noLink) OR !$this->noLink)) {
									echo ' onclick="document.location = \'index.php?tab=AdminCustomers&id_customer='.$tr['id_customer'].'&viewcustomer&viewpreventivo&id_thread='.$id.'&token='.$tokenCustomers.'&tab-container-1=7\'">';
								}
								else {
									echo '>';
								}
							}
							
							else if($tr['id_contact'] == 'tirichiamiamonoi') {
								if (!isset($params['position']) AND (!isset($this->noLink) OR !$this->noLink)) {
									echo ' onclick="document.location = \'index.php?tab=AdminCustomers&id_customer='.$tr['id_customer'].'&viewcustomer&viewpreventivo&id_thread='.$id.'&token='.$tokenCustomers.'&tab-container-1=7\'">';
								}
								else {
									echo '>';
								}
							}
							
						else if($tr['id_contact'] == 'Attivita' || $tr['id_contact'] == 'Telefonata' || $tr['id_contact'] == 'Visita' || $tr['id_contact'] == 'Intervento' || $tr['id_contact'] == 'Caso' || $tr['id_contact'] == 'Richiesta_RMA' || $tr['id_contact'] == 'TODO Amazon' || $tr['id_contact'] == 'TODO Social' || $tr['id_contact'] == 'TODO Sendinblue') {
								
								if($tr['id_customer'] == 'Staff')
								{
									if (!isset($params['position']) AND (!isset($this->noLink) OR !$this->noLink)) {
										echo ' onclick="document.location = \'index.php?tab=AdminCustomerThreads&id_employee='.$tr['id_employee'].'&updateemployee&viewaction&id_action='.$id.'&token='.$this->token.'&tab-container-1=3\'">';
									}
									else {
										echo '>';
									}
								
								}
								else
								{
									if (!isset($params['position']) AND (!isset($this->noLink) OR !$this->noLink)) {
										echo ' onclick="document.location = \'index.php?tab=AdminCustomers&id_customer='.$tr['id_customer'].'&viewcustomer&viewaction&id_action='.$id.'&token='.$tokenCustomers.'&tab-container-1=10\'">';
									}
									else {
										echo '>';
									}
								}	
						}
							
							else {
								if (!isset($params['position']) AND (!isset($this->noLink) OR !$this->noLink)) {
									echo ' onclick="document.location = \'index.php?tab=AdminCustomers&id_customer='.$tr['id_customer'].'&'.($tr['id_contact'] == 7 ? 'viewcustomer&viewmessage&id_mex='.$id.'' : 'viewcustomer&viewticket&id_customer_thread='.$id).'&token='.$tokenCustomers.($tr['id_contact'] == 7 ? '&tab-container-1=7' : '&tab-container-1=6').'\'">';
								}
								else {
									echo '>';
								}
							}
						}
					}
					
					if (isset($params['active']) AND isset($tr[$key]))
						$this->_displayEnableLink($token, $id, $tr[$key], $params['active'], Tools::getValue('id_category'), Tools::getValue('id_product'));
					else if ($params['title'] == 'Aperto da') {
						switch($tr[$key]) {
							case 0: echo 'Cliente'; break;
							default: echo Db::getInstance()->getValue('SELECT CONCAT (firstname," ",LEFT(lastname, 1),".") FROM employee WHERE id_employee = '.$tr[$key]); break;
						}
					}
					else if ($params['title'] == 'Azioni') {
						switch($tr[$key]) {
						
							case 'Assistenza tecnica postvendita': echo '<img src="http://www.ezdirect.it/img/admin/icons/assistenza.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Altro': echo '<img src="http://www.ezdirect.it/img/admin/icons/assistenza.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Assistenza': echo '<img src="http://www.ezdirect.it/img/admin/icons/assistenza.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Contabilita': echo '<img src="http://www.ezdirect.it/img/admin/icons/contabilita.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Messaggi': echo '<img src="http://www.ezdirect.it/img/admin/icons/messaggio.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Ordini': echo '<img src="http://www.ezdirect.it/img/admin/icons/ordini.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Rivenditori': echo '<img src="http://www.ezdirect.it/img/admin/icons/rivenditori.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'RMA': echo '<img src="http://www.ezdirect.it/img/admin/icons/rma.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Telefonata': echo '<img src="http://www.ezdirect.it/img/admin/icons/telefonata.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Attivita': echo '<img src="http://www.ezdirect.it/img/admin/icons/attivita.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Caso': echo '<img src="http://www.ezdirect.it/img/admin/icons/caso.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Intervento': echo '<img src="http://www.ezdirect.it/img/admin/icons/intervento.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'preventivo': echo '<img src="http://www.ezdirect.it/img/admin/icons/commerciale.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Commerciale': echo '<img src="http://www.ezdirect.it/img/admin/icons/commerciale.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Visita': echo '<img src="http://www.ezdirect.it/img/admin/icons/visita.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Richiesta_RMA': echo '<img src="http://www.ezdirect.it/img/admin/icons/richiesta_rma.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'TODO Amazon': echo '<img src="https://www.ezdirect.it/img/tmp/order_state_mini_22.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'TODO Social': echo '<img src="http://www.ezdirect.it/img/admin/icons/face-todo.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'TODO Sendinblue': echo '<img src="http://www.ezdirect.it/img/admin/icons/Sendinblue-logo.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'tirichiamiamonoi': echo '<img src="http://www.ezdirect.it/img/admin/icons/tirichiamiamonoi.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							case 'Richiamiamo': echo '<img src="http://www.ezdirect.it/img/admin/icons/tirichiamiamonoi.gif" alt="'.$tr[$key].'" title="'.$tr[$key].'" />'; break;
							default: echo $tr[$key]; break;
						
						
						}
					}
					elseif (isset($params['activeVisu']) AND isset($tr[$key]))
						echo '<img src="../img/admin/'.($tr[$key] ? 'enabled.gif' : 'disabled.gif').'"
						alt="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" title="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" />';
					elseif (isset($params['position']))
					{
						if ($this->_orderBy == 'position' AND $this->_orderWay != 'DESC')
						{
							echo '<a'.(!($tr[$key] != $positions[sizeof($positions) - 1]) ? ' style="display: none;"' : '').' href="'.$currentIndex.
									'&'.$keyToGet.'='.(int)($id_category).'&'.$this->identifiersDnd[$this->identifier].'='.$id.'
									&way=1&position='.(int)($tr['position'] + 1).'&token='.($token!=NULL ? $token : $this->token).'">
									<img src="../img/admin/'.($this->_orderWay == 'ASC' ? 'down' : 'up').'.gif"
									alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>';

							echo '<a'.(!($tr[$key] != $positions[0]) ? ' style="display: none;"' : '').' href="'.$currentIndex.
									'&'.$keyToGet.'='.(int)($id_category).'&'.$this->identifiersDnd[$this->identifier].'='.$id.'
									&way=0&position='.(int)($tr['position'] - 1).'&token='.($token!=NULL ? $token : $this->token).'">
									<img src="../img/admin/'.($this->_orderWay == 'ASC' ? 'up' : 'down').'.gif"
									alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a>';						}
						else
							echo (int)($tr[$key] + 1);
					}
					elseif (isset($params['image']))
					{
						// item_id is the product id in a product image context, else it is the image id.
						$item_id = isset($params['image_id']) ? $tr[$params['image_id']] : $id;
						// If it's a product image
						if (isset($tr['id_image']))
						{
							$image = new Image((int)$tr['id_image']);
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$image->getExistingImgPath().'.'.$this->imageType;
						}else
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$item_id.(isset($tr['id_image']) ? '-'.(int)($tr['id_image']) : '').'.'.$this->imageType;

						echo cacheImage($path_to_image, $this->table.'_mini_'.$item_id.'.'.$this->imageType, 45, $this->imageType);
					}
					elseif (isset($params['icon']) AND (isset($params['icon'][$tr[$key]]) OR isset($params['icon']['default'])))
						echo '<img id="icon_'.$tr['id_customer_thread'].'" style="margin-top:5px; display:block; float:left" src="../img/admin/'.(isset($params['icon'][$tr[$key]]) ? $params['icon'][$tr[$key]] : $params['icon']['default'].'" alt="'.$tr[$key]).'" title="'.$tr[$key].'" />';
					elseif (isset($params['price']))
						echo Tools::displayPrice($tr[$key], (isset($params['currency']) ? Currency::getCurrencyInstance((int)($tr['id_currency'])) : $currency), false);
					elseif (isset($params['float']))
						echo rtrim(rtrim($tr[$key], '0'), '.');
					elseif (isset($params['type']) AND $params['type'] == 'date')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang);
					elseif (isset($params['type']) AND $params['type'] == 'datetime')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang, true);
					elseif (isset($tr[$key]))
					{
						if($params['title'] == 'In carico a') {
						}
						else {
							$echo = ($key == 'price' ? round($tr[$key], 2) : isset($params['maxlength']) ? Tools::substr($tr[$key], 0, $params['maxlength']).'...' : $tr[$key]);
							echo isset($params['callback']) ? call_user_func_array(array($this->className, $params['callback']), array($echo, $tr)) : $echo;
						}
					}
					else
						echo '--';

					echo (isset($params['suffix']) ? $params['suffix'] : '');
					
					
					
					if ($params['title'] == 'Status') {
					
						echo '
						<form style="margin-left:5px; display:block;float:left" name="cambiastato_'.$tr['id_customer_thread'].'" id="cambiastato_'.$tr['id_customer_thread'].'" action="" method="post" />
						<a id="'.$tr['id_customer_thread'].'"> </a>
						<input type="hidden" name="id_customer" value="'.$tr['id_customer'].'" />
						<input type="hidden" name="type" value="'.($tr['id_customer'] == 'Staff' ? 'to-do-employee' : Tools::getValue('type')).'" />
						<input type="hidden" name="customer_threadOrderby" value="'.Tools::getValue('customer_threadOrderby').'" />
						<input type="hidden" name="customer_threadOrderway" value="'.Tools::getValue('customer_threadOrderway').'" />
						<input type="hidden" name="id_thread" value="'.$tr['id_customer_thread'].'" />
						<input type="hidden" name="tipo" value="'.(substr($tr['ticket'],0,1) == 'P' ? 'preventivo' : (substr($tr['ticket'],0,1) == 'R' ? 'tirichiamiamonoi' : (substr($tr['ticket'],0,4) == 'TODO' ? 'to-do' : 'ticket'))).'" />
						<select name="cambiastatus" onchange="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { changeStatusFromListing(\''.$tr['id_customer_thread'].'\', \''.(substr($tr['ticket'],0,1) == 'P' ? 'preventivo' : (substr($tr['ticket'],0,1) == 'R' ? 'tirichiamiamonoi' : (substr($tr['ticket'],0,4) == 'TODO' ? 'to-do' : (substr($tr['ticket'],0,5) == 'STAFF' ? 'to-do-employee' : 'ticket')))).'\', this.value); } else { this.value=\''.$tr[$key].'\'}">
						<option style="background-image:url(../img/admin/status_green.gif); background-repeat:no-repeat;
						background-position:bottom left;
						padding-left:30px;"  value="closed" '.($tr[$key] == 'closed' ? 'selected="selected"' : '').'>C</option>
						<option style="background-image:url(../img/admin/status_orange.gif); background-repeat:no-repeat;
						background-position:bottom left;
						padding-left:30px;"  value="pending1" '.($tr[$key] == 'pending1' ? 'selected="selected"' : '').'>L</option>
						<option style="background-image:url(../img/admin/status_red.gif); background-repeat:no-repeat;
						background-position:bottom left;
						padding-left:30px;"  value="open" '.($tr[$key] == 'open' ? 'selected="selected"' : '').'>A</option>
						</select>
						</form>
						';
					
					}
					
					if ($params['title'] == 'In carico a') {
						$tiporic = (substr($tr['ticket'],0,1) == 'P' ? 'preventivo' : (substr($tr['ticket'],0,1) == 'R' ? 'tirichiamiamonoi' : ($tr['ticket'] == 'TO DO' ? 'to-do' : 'ticket')));
						echo '
						<form style="margin-left:5px; display:block;float:left" onsubmit="alert(\'Incarico cambiato\'); return false;" name="cambiaincaricoa_'.$tr['id_customer_thread'].'" id="cambiaincaricoa_'.$tr['id_customer_thread'].'" action="" method="post" />
						<a id="'.$tr['id_customer_thread'].'"> </a>
						<input type="hidden" name="id_customer" value="'.$tr['id_customer'].'" />
						<input type="hidden" name="id_ticket_univoco" value="'.Customer::trovaSigla($tr['id_customer_thread'],$tiporic).'" />
						<input type="hidden" name="type" value="'.($tr['id_customer'] == 'Staff' ? 'to-do-employee' : Tools::getValue('type')).'" />
						<input type="hidden" name="customer_threadOrderby" value="'.Tools::getValue('customer_threadOrderby').'" />
						<input type="hidden" name="customer_threadOrderway" value="'.Tools::getValue('customer_threadOrderway').'" />
						<input type="hidden" name="id_thread" value="'.$tr['id_customer_thread'].'" />
						<input type="hidden" name="precedente_incarico" value="'.$tr[$key].'" />
						<input type="hidden" name="tipo" value="'.(substr($tr['ticket'],0,1) == 'P' ? 'preventivo' : (substr($tr['ticket'],0,1) == 'R' ? 'tirichiamiamonoi' : (substr($tr['ticket'],0,4) == 'TODO' ? 'to-do' : 'ticket'))).'" />
						<select style="width:100px" name="cambiaincarico" onchange="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { changeInCaricoFromListing(\''.$tr['id_customer_thread'].'\', \''.(substr($tr['ticket'],0,1) == 'P' ? 'preventivo' : (substr($tr['ticket'],0,1) == 'R' ? 'tirichiamiamonoi' : (substr($tr['ticket'],0,4) == 'TODO' ? 'to-do' : (substr($tr['ticket'],0,5) == 'STAFF' ? 'to-do-employee' : 'ticket')))).'\', this.value); } else { this.value=\''.$tr[$key].'\'}">
						<option value="0" '.($tr[$key] == '0' ? 'selected="selected"' : '').'>--Nessuno--</option>
						
						<option value="2" '.($tr[$key] == '2' ? 'selected="selected"' : '').'>Barbara G.</option>
						
						<option value="22" '.($tr[$key] == '22' ? 'selected="selected"' : '').'>Carolina C.</option>
						<option value="26" '.($tr[$key] == '26' ? 'selected="selected"' : '').'>Dorinel T.</option>
						
						<option value="1" '.($tr[$key] == '1' ? 'selected="selected"' : '').'>Ezio D.</option>
						<option value="6" '.($tr[$key] == '6' ? 'selected="selected"' : '').'>Federico G.</option>
						
						<option value="24" '.($tr[$key] == '24' ? 'selected="selected"' : '').'>Grazia B.</option>
						
						<option value="19" '.($tr[$key] == '19' ? 'selected="selected"' : '').'>Leila S.</option>
						<option value="17" '.($tr[$key] == '17' ? 'selected="selected"' : '').'>Leonardo B.</option>
						<option value="12" '.($tr[$key] == '12' ? 'selected="selected"' : '').'>Lorenzo M.</option>
						
						<option value="4" '.($tr[$key] == '4' ? 'selected="selected"' : '').'>Massimo B.</option>
						<option value="7" '.($tr[$key] == '7' ? 'selected="selected"' : '').'>Matteo D.</option>
						<option value="5" '.($tr[$key] == '5' ? 'selected="selected"' : '').'>Paolo D.</option>
						
						<option value="25" '.($tr[$key] == '25' ? 'selected="selected"' : '').'>Sara M.</option>
						
						<option value="14" '.($tr[$key] == '14' ? 'selected="selected"' : '').'>Valentina V.</option>					
						
						
						</select>
						</form>';
					
					}
					
					
					
					
					echo '</td>';
				}

				if ($this->edit OR $this->delete OR ($this->view AND $this->view !== 'noActionColumn'))
				{
					echo '<td class="center" style="white-space: nowrap;">';
					
					if ($this->edit)
						$this->_displayEditLink($token, $id);
					if ($this->delete AND (!isset($this->_listSkipDelete) OR !in_array($id, $this->_listSkipDelete)))
						$this->_displayDeleteLink($token, $id, $contact_type);
					if ($this->duplicate)
						$this->_displayDuplicate($token, $id);
					echo '</td>';
				}
				echo '</tr>';
			}
		}
	
	}
	
	public function displayListHeaderModificata($token = NULL)
	{
		global $currentIndex, $cookie;
		$contacts = Db::getInstance()->ExecuteS('
			SELECT cl.*, COUNT(*) as total, (
				SELECT id_customer_thread
				FROM '._DB_PREFIX_.'customer_thread ct2
				WHERE status = "open" AND ct.id_contact = ct2.id_contact
				ORDER BY date_add ASC
				LIMIT 1			
			) as id_customer_thread
			FROM '._DB_PREFIX_.'customer_thread ct
			LEFT JOIN '._DB_PREFIX_.'contact_lang cl ON (cl.id_contact = ct.id_contact AND cl.id_lang = '.$cookie->id_lang.')
			WHERE ct.status = "open"
			GROUP BY ct.id_contact HAVING COUNT(*) > 0');
		$categories = Db::getInstance()->ExecuteS('
			SELECT cl.*
			FROM '._DB_PREFIX_.'contact ct
			LEFT JOIN '._DB_PREFIX_.'contact_lang cl ON (cl.id_contact = ct.id_contact AND cl.id_lang = '.$cookie->id_lang.')
			WHERE ct.customer_service = 1');
		$dim = count($categories);

		echo '<div style="float:left;border:0;width:1030px;">';
		/*foreach ($categories as $key => $val)
		{
			$totalThread = 0;
			$id_customer_thread = 0;
			foreach ($contacts as $tmp => $tmp2)
				if ($val['id_contact'] == $tmp2['id_contact'])
				{
					$totalThread = $tmp2['total'];
					$id_customer_thread = $tmp2['id_customer_thread'];
					break; 
				}
			echo '<div style="background-color:#EFEFEF;float:left;margin:0 10px 10px 0;width:'.($dim > 6 ? '200' : '300').'px;border:1px solid #CFCFCF" >
					<h3 style="overflow:hidden;line-height:25px;color:#812143;height:25px;margin:0;">&nbsp;'.$val['name'].'</h3>'.
					($dim > 6 ? '' : '<p style="overflow:hidden;line-height:15px;height:45px;margin:0;padding:0 5px;">'.$val['description'].'</p>').
					($totalThread == 0 ? '<h3 style="padding:0 5px;margin:0;height:23px;line-height:23px;background-color:#DEDEDE">'.$this->l('No new message').'</h3>' 
					: '<a href="'.$currentIndex.'&token='.Tools::getValue('token').'&type='.$val['id_contact'].'" style="padding:0 5px;display:block;height:23px;line-height:23px;border:0;" class="button">Vedi tutti</a>').'
				</div>';
		}
		
		
		echo '<div style="background-color:#EFEFEF;float:left;margin:0 10px 10px 0;width:'.($dim > 6 ? '200' : '300').'px;border:1px solid #CFCFCF" >
					<h3 style="overflow:hidden;line-height:25px;color:#812143;height:25px;margin:0;">&nbsp;Prevendita/preventivi</h3>'.
					($dim > 6 ? '' : '<p style="overflow:hidden;line-height:15px;height:45px;margin:0;padding:0 5px;">Messaggi prevendita/preventivi</p>').
					($totalThread == 0 ? '<h3 style="padding:0 5px;margin:0;height:23px;line-height:23px;background-color:#DEDEDE">'.$this->l('No new message').'</h3>' 
					: '<a href="'.$currentIndex.'&token='.Tools::getValue('token').'&type=preventivo" style="padding:0 5px;display:block;height:23px;line-height:23px;border:0;" class="button">Vedi tutti</a>').'
				</div>';
		*/	$tokenTickets = Tools::getAdminToken('AdminCustomerThreads'.(int)(Tab::getIdFromClassName('AdminCustomerThreads')).(int)($cookie->id_employee));
		
		$hidden_planner = (Db::getInstance()->getValue('SELECT value FROM fixed_ck WHERE id_employee = '.$cookie->id_employee.' AND name="hidden_planner"'));
		
		
		echo "<script type='text/javascript'>
		function changeStatusFromListing(id_thread, type, status)
		{
			
			$.ajax({
				type: 'POST',
				async: true,
				url: 'ajax.php?changeStatusFromListing=y',
				data: {id_thread: id_thread, type: type, status: status},
				success: function(data) {
					document.getElementById('icon_'+id_thread).src='../img/admin/status_'+data+'.gif';
					alert('Status cambiato con successo');
				},
				error: function(xhr,stato,errori){
					alert('errore');
				}
			});
			
			
		}
		
		function changeInCaricoFromListing(id_thread, type, in_carico)
		{
			
			$.ajax({
				type: 'POST',
				async: true,
				url: 'ajax.php?changeInCaricoFromListing=y',
				data: {id_thread: id_thread, type: type, in_carico: in_carico},
				success: function(data) {
					alert('Incarico cambiato con successo');
				},
				error: function(xhr,stato,errori){
					alert('errore');
				}
			});
			
			
		}
		
		$(document).ready(function(){
			$('#toggleplanner').click(function() {
				$('#planner').slideToggle('fast', function() {
					if ($('#planner').is(':hidden')) {
						var hidden_planner = 1;
					} else {
						var hidden_planner = 0;
					}
					
					$.ajax({
						type: 'POST',
						async: true,
						url: 'ajax.php?nascondiMostraPlanner',
						data: {hidden_planner: hidden_planner, nascondiMostraPlanner: 'y'},
						success: function(data) {
						},
						error: function(xhr,stato,errori){
							alert('errore');
						}
					});
				});
			});
		});
</script>";

echo '
		<script type="text/javascript" src="yetii.js"></script>
		';

echo '<div id="tab-container-1" style="margin-top:0px">
			<ul id="tab-container-2-nav" style="margin-top:0px; margin-left:18px">
			<li '.($_GET['tab-container-1'] == 1 || !isset($_GET['tab-container-1']) ? 'class="activeli" ' : '').' style="margin-left:-35px"><a href="'.$currentIndex.'&id_employee='.Tools::getValue('id_employee').'&updateemployee&token='.$this->token.'&tab-container-1=1"><strong>Planner e azioni clienti</strong></a></li>
			<li '.($_GET['tab-container-1'] == 3 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_employee='.$cookie->id_employee.'&updateemployee&token='.$this->token.'&tab-container-1=3"><strong>Azioni staff</strong></a></li>
			
			<li '.($_GET['tab-container-1'] == 2 ? 'class="activeli" ' : '').' style="display:none"><a href="'.$currentIndex.'&id_employee='.Tools::getValue('id_employee').'&updateemployee&token='.$this->token.'&tab-container-1=2"><strong>#</strong></a></li>
			</ul>';
			
			echo '<ul id="tab-container-1-nav" style="list-style-type:none; display:none">
			<li style="margin-left:-35px"><a href="'.$currentIndex.'&id_employee='.Tools::getValue('id_employee').'&viewemployee&token='.$this->token.'#attivita"><strong>Attivit&agrave;</strong></a></li>
			<li><a href="'.$currentIndex.'&id_employee='.Tools::getValue('id_employee').'&viewemployee&token='.$this->token.'#opzioni"><strong>Opzioni</strong></a></li></ul>';

			echo '<br /><br /><div class="tab1" id="attivita">';
			
			echo '
			<div class="tasti-apertura"><a class="button" style="display:block" href="#"><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" />&nbsp;&nbsp;&nbsp;Aggiungi nuova azione staff</a>
				<div class="menu-apertura">
					<ul class="dropdown">
						<li class="tasti-apertura-in"><a href="'.$currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&viewaction&aprinuovaazione&token='.$this->token.'&tab-container-1=3"><img src="../img/admin/return.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;To-do</a>
							<div class="menu-apertura-in" style="position:absolute; left:100px">
								<ul class="dropdown">
									<li><a style="color:#000" href="'.$currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&viewaction&aprinuovaazione&token='.$this->token.'&tipo-todo=Attivita&tab-container-1=3"><img src="../img/admin/icons/attivita.gif" alt="Attivita" title="Attivita" />&nbsp;&nbsp;&nbsp;Attivit&agrave;</a></li>
									
									<li><a href="'.$currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&viewaction&aprinuovaazione&token='.$this->token.'&tipo-todo=Telefonata&tab-container-1=3"><img src="../img/admin/icons/telefonata.gif" alt="Telefonata" title="Telefonata" />&nbsp;&nbsp;&nbsp;Telefonata</a></li>
									
									<li><a href="'.$currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&viewaction&aprinuovaazione&token='.$this->token.'&tipo-todo=Visita&tab-container-1=3"><img src="../img/admin/icons/visita.gif" alt="Visita" title="Visita" />&nbsp;&nbsp;&nbsp;Visita</a></li>
									
									<li><a href="'.$currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&viewaction&aprinuovaazione&token='.$this->token.'&tipo-todo=Caso&tab-container-1=3"><img src="../img/admin/icons/caso.gif" alt="Caso" title="Caso" />&nbsp;&nbsp;&nbsp;Caso</a></li>
									
									<li><a href="'.$currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&viewaction&aprinuovaazione&token='.$this->token.'&tipo-todo=Intervento&tab-container-1=3"><img src="../img/admin/icons/intervento.gif" alt="Intervento" title="Intervento" />&nbsp;&nbsp;&nbsp;Intervento</a></li>
									
									<li><a href="'.$currentIndex.'&id_employee='.$c_emp->id.'&updateemployee&viewaction&aprinuovaazione&token='.$this->token.'&tipo-todo=Richiesta_RMA&tab-container-1=3"><img src="../img/admin/icons/richiesta_rma.gif" alt="Richiesta RMA" title="Richiesta RMA" />&nbsp;&nbsp;&nbsp;Richiesta RMA</a></li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
			</div>';
			
echo "<p style='text-align:center; margin-top:-10px'><a href='#' id='toggleplanner'>Clicca per nascondere/mostrare il planner</a>
";
if($cookie->id_employee == 3 || $cookie->id_employee == 4 || $cookie->id_employee == 5 || $cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 12 || $cookie->id_employee == 17 || $cookie->id_employee == 26) 
echo "
<script type='text/javascript'>
function refreshPlanner(newsrc) {
    var ifr = document.getElementById('planner');
    ifr.src = newsrc;
}
</script>

| Vedi planner di: 
<button class='button' onclick='refreshPlanner(\"https://www.ezdirect.it/ezadmin/planner/wdCalendar/sample.php?id_employee=4\")'>Massimo</button>  - 
<button class='button' onclick='refreshPlanner(\"https://www.ezdirect.it/ezadmin/planner/wdCalendar/sample.php?id_employee=5\")'>Paolo</button> - 
<button class='button' onclick='refreshPlanner(\"https://www.ezdirect.it/ezadmin/planner/wdCalendar/sample.php?id_employee=12\")'>Lorenzo</button> -
<button class='button' onclick='refreshPlanner(\"https://www.ezdirect.it/ezadmin/planner/wdCalendar/sample.php?id_employee=17\")'>Leonardo</button> -
<button class='button' onclick='refreshPlanner(\"https://www.ezdirect.it/ezadmin/planner/wdCalendar/sample.php?id_employee=26\")'>Dorinel</button>

";

//if($cookie->id_employee == 1) {
//}
//else {

		echo "</p><iframe id='planner' src='https://www.ezdirect.it/ezadmin/planner/wdCalendar/sample.php?id_employee=".$cookie->id_employee."' style='border:0px; width:100%; height:620px; overflow:hide; ".($hidden_planner == 0 ? "display:block" : 'display:none')."'>
			<base target='_parent' />
			</iframe>";
	//}		
			
		
		
		
		echo '<p class="clear">&nbsp;</p>';
		
		echo "
		<style type='text/css'>
	#hint{
		cursor:pointer;
	}
	.tooltip{
		margin:8px;
		text-align:left;
		padding:8px;
		border:1px solid blue;
		background-color:#ffffff;
		position: absolute;
		z-index: 2;
	}
</style>";
echo '
<script type="text/javascript">
	$(document).ready(function() {
		$("#filtro_ticket").keyup(function(){
			$(this).val($(this).val().toUpperCase());
		});
	});		
			</script>';
echo "<script type='text/javascript'>
 
$(document).ready(function() {
	var changeTooltipPosition = function(event) {
	  var tooltipX = event.pageX - 8;
	  var tooltipY = event.pageY + 8;
	  $('div.tooltip').css({top: tooltipY, left: tooltipX});
	};
	var tooltip_content = '';
	
 
	
	var showTooltip = function(event) {
	  $('div.tooltip').remove();
	  $.ajax({
	type: 'POST',
	data: 'id='+$(this).attr('id')+'&rel='+$(this).attr('rel'),
	async: false,
	url: 'customer_message_preview.php',
	success: function(resp)
		{
			tooltip_content = resp;						
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			tooltip_content = '';
			alert('ERROR');					
		}
								
	});
	 
	  $(\"<div class='tooltip'>\"+tooltip_content+\"</div>\")
            .appendTo('body');
	  changeTooltipPosition(event);
	};
	
	var hideTooltip = function() {
	   $('div.tooltip').remove();
	};
 
	$('tr.alt_row, tr.odd_row').bind({
	   mousemove : changeTooltipPosition,
	   mouseenter : showTooltip,
	   mouseleave: hideTooltip
	});
});
 
</script>
		<table><tr><td>
		<form method='post'><table style='width:350px'><tr><td style='width:150px'>
		Cerca testo nel ticket</td><td> <input type='text' name='cercaticket' size='20' /> 
		<input type='hidden' value='data_act' name='customer_threadOrderby' />
		<input type='hidden' value='DESC' name='customer_threadOrderway' />
		<input type='submit' value='Cerca' name='vaicercaticket' class='button' />	</td></tr></table>
		</form>
		</td>
		<td>
		<form method='post'><table style='width:400px'><tr><td style='width:200px'>
		Cerca ticket per ID senza filtri <img class='img_control' src='https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif' alt='Cerca un ticket in base al suo ID (es. P166955). Funziona anche con parti di codice. La ricerca non considera filtri impostati in precedenza.' title='Cerca un ticket in base al suo ID (es. P166955). Funziona solo se il codice viene inserito per intero. La ricerca non considera filtri impostati in precedenza.' /></td><td> <input type='text' name='cercaidticket' size='20' /> 
		<input type='hidden' value='data_act' name='customer_threadOrderby' />
		<input type='hidden' value='DESC' name='customer_threadOrderway' />
		<input type='submit' value='Cerca' name='vaicercaticket' class='button' />	</td></tr></table>
		</form></td></tr></table><br />
		
		<!-- <form method='post'><table style='width:750px'><tr><td style='width:200px'>
		Cerca attivit&agrave; aperta da:</td><td> <select name='aperto_da'>
		
		<option value='0' >Cliente</option>
		<option value='2'>Barbara G.</option>
		
		<option value='22'>Carolina C.</option>
		<option value='26'>Dorinel T.</option>
		
		<option value='1'>Ezio D.</option>
		<option value='6'>Federico G.</option>
		
		<option value='24'>Grazia B.</option>
		
		<option value='19'>Leila S.</option>
		<option value='17'>Leonardo B.</option>
		<option value='12'>Lorenzo M.</option>

		<option value='4'>Massimo B.</option>
		<option value='7'>Matteo D.</option>
		<option value='5'>Paolo D.</option>
		
		<option value='25'>Sara M.</option>
		
		<option value='14'>Valentina V.</option>							
		</select>		
		<input type='hidden' value='data_act' name='customer_threadOrderby' />
		<input type='hidden' value='DESC' name='customer_threadOrderway' />
		<input type='submit' value='Cerca' name='vaicercaticket' class='button' />	</td></tr></table>
		</form><br /> -->
		
		";
		
		$isCms = false;
		if (preg_match('/cms/Ui', $this->identifier))
			$isCms = true;
		$id_cat = Tools::getValue('id_'.($isCms ? 'cms_' : '').'category');

		if (!isset($token) OR empty($token))
			$token = $this->token;

		/* Determine total page number */
		$totalPages = ceil($this->_listTotal / Tools::getValue('pagination', (isset($cookie->{$this->table.'_pagination'}) ? $cookie->{$this->table.'_pagination'} : $this->_pagination[0])));
		if (!$totalPages) $totalPages = 1;

		echo '<a name="'.$this->table.'">&nbsp;</a>';
		echo '<form method="post" action="'.$currentIndex;
		if (Tools::getIsset($this->identifier))
			echo '&'.$this->identifier.'='.(int)(Tools::getValue($this->identifier));
		echo '&token='.$token.(isset($_GET['type']) ? '&type='.Tools::getValue('type').'' : '');
		
		if (Tools::getIsset($this->table.'Orderby') && Tools::getValue($this->table.'Orderby') != '' )
			echo '&'.$this->table.'Orderby='.urlencode($this->_orderBy).'&'.$this->table.'Orderway='.urlencode(strtolower($this->_orderWay));
		echo '" class="form">
		<input type="hidden" id="submitFilter'.$this->table.'" name="submitFilter'.$this->table.'" value="0">
		<table>
			<tr>
				<td style="vertical-align: bottom;">
					';

		/* Determine current page number */
		$page = (int)(Tools::getValue('submitFilter'.$this->table));
		if (!$page) $page = 1;
		if ($page > 1)
			echo '
						<input type="image" src="../img/admin/list-prev2.gif" onclick="getE(\'submitFilter'.$this->table.'\').value=1"/>
						&nbsp; <input type="image" src="../img/admin/list-prev.gif" onclick="getE(\'submitFilter'.$this->table.'\').value='.($page - 1).'"/> ';
		echo $this->l('Page').' <b>'.$page.'</b> / '.$totalPages;
		if ($page < $totalPages)
			echo '
						<input type="image" src="../img/admin/list-next.gif" onclick="getE(\'submitFilter'.$this->table.'\').value='.($page + 1).'"/>
						 &nbsp;<input type="image" src="../img/admin/list-next2.gif" onclick="getE(\'submitFilter'.$this->table.'\').value='.$totalPages.'"/>';
		echo '			| '.$this->l('Display').'
						<select name="pagination">';
		/* Choose number of results per page */
		$selectedPagination = Tools::getValue('pagination', (isset($cookie->{$this->table.'_pagination'}) ? $cookie->{$this->table.'_pagination'} : NULL));
		foreach ($this->_pagination AS $value)
			echo '<option value="'.(int)($value).'"'.($selectedPagination == $value ? ' selected="selected"' : (($selectedPagination == NULL && $value == $this->_pagination[2]) ? ' selected="selected2"' : '')).'>'.(int)($value).'</option>';
		echo '
						</select>
						/ '.(int)($this->_listTotal).' '.$this->l('result(s)').'
					</span>
					<span style="float: right;">
						<input type="submit" name="submitResetX'.$this->table.'" value="'.$this->l('Reset').'" class="button" />
						<input type="submit" id="submitFilterButton_'.$this->table.'" name="submitFilter" value="'.$this->l('Filter').'" class="button" />
					</span>
					<span class="clear"></span>
				</td>
			</tr>
			<tr>
				<td>';
		
		/* Display column names and arrows for ordering (ASC, DESC) */
		if (array_key_exists($this->identifier,$this->identifiersDnd) AND $this->_orderBy == 'position')
		{
			echo '
			
			<script type="text/javascript" src="../js/jquery/jquery.tablednd_0_5.js"></script>
			<script type="text/javascript">
				var token = \''.($token!=NULL ? $token : $this->token).'\';
				var come_from = \''.$this->table.'\';
				var alternate = \''.($this->_orderWay == 'DESC' ? '1' : '0' ).'\';
			</script>
			<script type="text/javascript" src="../js/admin-dnd.js"></script>
			';
		}
		
		echo "<script type='text/javascript'>
				$(document).ready(function() {
					function moveScroll(){
						var scroll = $(window).scrollTop();
						var anchor_top = $('#".(((int)(Tools::getValue($this->identifiersDnd[$this->identifier], 1))) ? substr($this->identifier,3,strlen($this->identifier)) : 'table-list')."').offset().top;
						var anchor_bottom = $('#bottom_anchor').offset().top;
						if (scroll>anchor_top && scroll<anchor_bottom) {
						clone_table = $('#clone');
						if(clone_table.length == 0){
							clone_table = $('#".(((int)(Tools::getValue($this->identifiersDnd[$this->identifier], 1))) ? substr($this->identifier,3,strlen($this->identifier)) : 'table-list')."').clone();
							clone_table.attr('id', 'clone');
							clone_table.css({position:'fixed',
									 'pointer-events': 'none',
									 top:0});
							clone_table.width($('#".(((int)(Tools::getValue($this->identifiersDnd[$this->identifier], 1))) ? substr($this->identifier,3,strlen($this->identifier)) : 'table-list')."').width());
							$('#".(((int)(Tools::getValue($this->identifiersDnd[$this->identifier], 1))) ? substr($this->identifier,3,strlen($this->identifier)) : 'table-list')."-table-container').append(clone_table);
							$('#clone').css({visibility:'hidden'});
							$('#clone thead').css({'visibility':'visible','pointer-events':'auto'});
						}
						} else {
						$('#clone').remove();
						}
					}
					$(window).scroll(moveScroll); 
				});
			</script>";
		echo "<div id='".(((int)(Tools::getValue($this->identifiersDnd[$this->identifier], 1))) ? substr($this->identifier,3,strlen($this->identifier)) : 'table-list')."-table-container'>".'<table'.(array_key_exists($this->identifier,$this->identifiersDnd) ? ' id="'.(((int)(Tools::getValue($this->identifiersDnd[$this->identifier], 1))) ? substr($this->identifier,3,strlen($this->identifier)) : '').'"' : ' id="table-list"' ).' class="table2 table'.((array_key_exists($this->identifier,$this->identifiersDnd) AND ($this->_orderBy != 'position 'AND $this->_orderWay != 'DESC')) ? ' tableDnD'  : '' ).'" cellpadding="0" cellspacing="0">
			<thead class="persist-header-2">
				<tr class="nodrag nodrop">
					<th>';
		if ($this->delete)
			echo '		<input type="checkbox" name="checkme" class="noborder" onclick="checkDelBoxes(this.form, \''.$this->table.'Box[]\', this.checked)" />';
		echo '		</th>';
		foreach ($this->fieldsDisplay AS $key => $params)
		{
			echo '	<th '.(isset($params['widthColumn']) ? 'style="width: '.$params['widthColumn'].'px"' : '').'>'.$params['title'];
			if (!isset($params['orderby']) OR $params['orderby'])
			{
				if(!Tools::getIsset('cercaticket') && !Tools::getIsset('cercaidticket'))
				{	
					// Cleaning links
					if (Tools::getValue($this->table.'Orderby') && Tools::getValue($this->table.'Orderway'))
						$currentIndex = preg_replace('/&'.$this->table.'Orderby=([a-z _]*)&'.$this->table.'Orderway=([a-z]*)/i', '', $currentIndex);
					echo '	<br />
							<a href="'.$currentIndex.'&'.$this->identifier.'='.(int)$id_cat.'&'.$this->table.'Orderby='.urlencode($key).'&'.$this->table.'Orderway=desc&submitFiltercustomer_thread=true&token='.$token.(isset($_GET['type']) ? '&type='.Tools::getValue('type').'' : '').'"><img border="0" src="../img/admin/down'.((isset($this->_orderBy) && ($key == $this->_orderBy) && ($this->_orderWay == 'DESC')) ? '_d' : '').'.gif" /></a>
							<a href="'.$currentIndex.'&'.$this->identifier.'='.(int)$id_cat.'&'.$this->table.'Orderby='.urlencode($key).'&'.$this->table.'Orderway=asc&submitFiltercustomer_thread=true&token='.$token.(isset($_GET['type']) ? '&type='.Tools::getValue('type').'' : '').'"><img border="0" src="../img/admin/up'.((isset($this->_orderBy) && ($key == $this->_orderBy) && ($this->_orderWay == 'ASC')) ? '_d' : '').'.gif" /></a>';
				}
			}
			echo '	</th>';
		}

		/* Check if object can be modified, deleted or detailed */
		if ($this->edit OR $this->delete OR ($this->view AND $this->view !== 'noActionColumn'))
			echo '	<th style="width: 52px">'.$this->l('Actions').'</th>';
		echo '	</tr>';
		
		if(!Tools::getIsset('cercaticket') && !Tools::getIsset('cercaidticket'))
		{
					
		echo '
				<tr class="nodrag nodrop" style="height: 35px; background-color:#ffffff">
					<td class="center">';
		if ($this->delete)
			echo '		--';
		echo '		</td>';

		/* Javascript hack in order to catch ENTER keypress event */
		$keyPress = 'onkeypress="formSubmit(event, \'submitFilterButton_'.$this->table.'\');"';

		/* Filters (input, select, date or bool) */
		foreach ($this->fieldsDisplay AS $key => $params)
		{
			$width = (isset($params['width']) ? ' style="width: '.(int)($params['width']).'px;"' : '');
			echo '<td'.(isset($params['align']) ? ' class="'.$params['align'].'"' : '').'>';
			if (!isset($params['type']))
				$params['type'] = 'text';

			$value = Tools::getValue($this->table.'Filter_'.(array_key_exists('filter_key', $params) ? $params['filter_key'] : $key));
			if (isset($params['search']) AND !$params['search'])
			{
				echo '--</td>';
				continue;
			}
			switch ($params['type'])
			{
				case 'bool':
					echo '
					<select name="'.$this->table.'Filter_'.$key.'">
						<option value="">--</option>
						<option value="1"'.($value == 1 ? ' selected="selected"' : '').'>'.$this->l('Yes').'</option>
						<option value="0"'.(($value == 0 AND $value != '') ? ' selected="selected"' : '').'>'.$this->l('No').'</option>
					</select>';
					break;

				case 'date':
				case 'datetime':
					if (is_string($value))
						$value = unserialize($value);
					if (!Validate::isCleanHtml($value[0]) OR !Validate::isCleanHtml($value[1]))
						$value = '';
					$name = $this->table.'Filter_'.(isset($params['filter_key']) ? $params['filter_key'] : $key);
					$nameId = str_replace('!', '__', $name);
					includeDatepicker(array($nameId.'_0', $nameId.'_1'));
					echo $this->l('From').' <input type="text" id="'.$nameId.'_0" name="'.$name.'[0]" value="'.(isset($value[0]) ? $value[0] : '').'"'.$width.' '.$keyPress.' /><br />
					'.$this->l('To').' <input type="text" id="'.$nameId.'_1" name="'.$name.'[1]" value="'.(isset($value[1]) ? $value[1] : '').'"'.$width.' '.$keyPress.' />';
					break;

				case 'select':

					if (isset($params['filter_key']))
					{
						echo '<select onchange="$(\'#submitFilter'.$this->table.'\').focus();$(\'#submitFilter'.$this->table.'\').click();" name="'.$this->table.'Filter_'.$params['filter_key'].'" '.(isset($params['width']) ? 'style="width: '.$params['width'].'px"' : '').'>
								<option value=""'.(($value == 0 AND $value != '') ? ' selected="selected"' : '').'>--</option>';
						if (isset($params['select']) AND is_array($params['select']))
							foreach ($params['select'] AS $optionValue => $optionDisplay)
							{
								echo '<option value="'.$optionValue.'"'.((isset($_POST[$this->table.'Filter_'.$params['filter_key']]) AND Tools::getValue($this->table.'Filter_'.$params['filter_key']) == $optionValue AND Tools::getValue($this->table.'Filter_'.$params['filter_key']) != '') ? ' selected="selected"' : '').'>'.$optionDisplay.'</option>';
								}
						echo '</select>';
						break;
					}

				case 'text':
				default:
					if (!Validate::isCleanHtml($value))
							$value = '';
					echo '<input type="text" '.($key == 'ticket' ? 'id="filtro_ticket"' : '').' name="'.$this->table.'Filter_'.(isset($params['filter_key']) ? $params['filter_key'] : $key).'" value="'.htmlentities($value, ENT_COMPAT, 'UTF-8').'"'.$width.' '.$keyPress.' />';
			}
			echo '</td>';
		}

		if ($this->edit OR $this->delete OR ($this->view AND $this->view !== 'noActionColumn'))
			echo '<td class="center">--</td>';

		echo '</tr>';
		}
		
		echo '
			</thead>';
	}
	
	public function getList($id_lang, $orderBy = NULL, $orderWay = NULL, $start = 0, $limit = NULL)
	{
		global $cookie;

		/* Manage default params values */
		if (empty($limit))
			$limit = ((!isset($cookie->{$this->table.'_pagination'})) ? $this->_pagination[1] : $limit = $cookie->{$this->table.'_pagination'});

		if (!Validate::isTableOrIdentifier($this->table))
			die (Tools::displayError('Table name is invalid:').' "'.$this->table.'"');

		if (empty($orderBy))
			$orderBy = $cookie->__get($this->table.'Orderby') ? $cookie->__get($this->table.'Orderby') : $this->_defaultOrderBy;
		if (empty($orderWay))
			$orderWay = $cookie->__get($this->table.'Orderway') ? $cookie->__get($this->table.'Orderway') : 'ASC';

		$limit = (int)(Tools::getValue('pagination', $limit));
		$cookie->{$this->table.'_pagination'} = $limit;

		/* Check params validity */
		if (!Validate::isOrderBy($orderBy) OR !Validate::isOrderWay($orderWay)
			OR !is_numeric($start) OR !is_numeric($limit)
			OR !Validate::isUnsignedId($id_lang))
			die(Tools::displayError('get list params is not valid'));

		/* Determine offset from current page */
		if ((isset($_POST['submitFilter'.$this->table]) OR
		isset($_POST['submitFilter'.$this->table.'_x']) OR
		isset($_POST['submitFilter'.$this->table.'_y'])) AND
		!empty($_POST['submitFilter'.$this->table]) AND
		is_numeric($_POST['submitFilter'.$this->table]))
			$start = (int)($_POST['submitFilter'.$this->table] - 1) * $limit;

		/* Cache */
		$this->_lang = (int)($id_lang);
		$this->_orderBy = $orderBy;
		$this->_orderWay = Tools::strtoupper($orderWay);

		/* SQL table : orders, but class name is Order */
		$sqlTable = $this->table == 'order' ? 'orders' : $this->table;

		/* Query in order to get results with all fields */
		$sql = 'SELECT SQL_CACHE 
			'.($this->_tmpTableFilter || !isset($_GET['customer_threadOrderby']) ? ' * FROM (SELECT ' : '').'
			'.($this->lang ? 'b.*, ' : '').'a.id_customer_thread, a.id_lang, a.id_contact, a.id_customer, a.id_employee, a.status, a.date_add, a.date_upd'.(isset($this->_select) ? ', '.$this->_select.' ' : '').'
			FROM `'._DB_PREFIX_.$sqlTable.'` a
			'.($this->lang ? 'LEFT JOIN `'._DB_PREFIX_.$this->table.'_lang` b ON (b.`'.$this->identifier.'` = a.`'.$this->identifier.'` AND b.`id_lang` = '.(int)($id_lang).')' : '').'
			'.(isset($this->_join) ? $this->_join.' ' : '').'
			'.($this->_tmpTableFilter && !Tools::getIsset('cercaticket')  && !Tools::getIsset('cercaidticket') ? '' : 'AND (a.status = "open" OR a.status = "pending1" OR a.status = "pending2" '.(!Tools::getIsset('cercaticket')  && !Tools::getIsset('cercaidticket') ? 'OR a.status = "closed"' : '').') '.(!Tools::getIsset('cercaticket')  && !Tools::getIsset('cercaidticket')? '' : 'AND a.id_employee = '.$cookie->id_employee)).'
			WHERE 1 
			
			'.(isset($this->_where) ? $this->_where.' ' : '').($this->deleted ? 'AND a.`deleted` = 0 ' : '').(isset($this->_filter) ? $this->_filter : '').'
			'.(isset($this->_group) ? $this->_group.' ' : '').'
			'.((isset($this->_filterHaving) || isset($this->_having)) ? 'HAVING ' : '').(isset($this->_filterHaving) ? ltrim($this->_filterHaving, ' AND ') : '').(isset($this->_having) ? $this->_having.' ' : '').'
			ORDER BY '.(($orderBy == $this->identifier) ? 'a.' : '').'`'.pSQL($orderBy).'` '.pSQL($orderWay).
			($this->_tmpTableFilter  || !isset($_GET['customer_threadOrderby']) ? ') tmpTable WHERE 1 
			'.(!empty($_POST['cercaidticket']) ? 'AND ticket LIKE "%'.($_POST['cercaidticket']).'%"' : '').'
			'.($this->_tmpTableFilter && !Tools::getIsset('cercaticket') && !Tools::getIsset('cercaidticket') ? stripslashes($this->_tmpTableFilter) : '').' ORDER BY '.(Tools::getIsset($this->table.'Orderby') ? Tools::getValue($this->table.'Orderby').' '.Tools::getValue($this->table.'Orderway') : '(CASE WHEN stato = "open" THEN 3 WHEN stato = "pending1" THEN 2 WHEN stato = "pending2" THEN 1 WHEN stato = "closed" THEN 0 END) DESC, '.(!Tools::getIsset('cercaticket')  && !Tools::getIsset('cercaidticket') ? '' : '(CASE WHEN id_employee = '.$cookie->id_employee.' THEN 0 ELSE 1 END),').' data_act ASC') : '');
			
		//$this->_list = Db::getInstance()->ExecuteS($sql.' LIMIT '.(int)($start).','.(int)($limit));
		
		if(Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 3)
		{
			
		}
		else
		{	
			$lista = Db::getInstance()->ExecuteS($sql);
		
			$this->_listTotal = sizeof($lista);
		
			$this->_list = array_slice($lista,$start,$limit);
		}
		//$this->_listTotal = sizeof(Db::getInstance()->ExecuteS($sql));
		
	}
	
	protected function _displayDeleteLink($token = NULL, $id, $contact_type)
	{
		global $currentIndex;

		$_cacheLang['Delete'] = $this->l('Delete');
		$_cacheLang['DeleteItem'] = $this->l('Delete item #', __CLASS__, TRUE, FALSE);
		
		echo '
			<a href="'.$currentIndex.'&'.$this->identifier.'='.$id.'&del&typedel='.$contact_type.'&token='.($token!=NULL ? $token : $this->token).'" onclick="return confirm(\''.$_cacheLang['DeleteItem'].$id.' ?'.
					(!is_null($this->specificConfirmDelete) ? '\r'.$this->specificConfirmDelete : '').'\');">
			<img src="../img/admin/delete.gif" alt="'.$_cacheLang['Delete'].'" title="'.$_cacheLang['Delete'].'" /></a>';
	}
	
		/**
	 * Close list table and submit button
	 */
	public function displayListFooter($token = NULL)
	{
		echo '</table><div id="bottom_anchor">';
		if ($this->delete)
		//	echo '<p><input type="submit" class="button" name="multidelete" value="'.$this->l('Delete selection').'" onclick="return confirm(\''.$this->l('Delete selected items?', __CLASS__, TRUE, FALSE).'\');" /></p>';
		echo '
				</td>
			</tr>
		</table>
		<input type="hidden" name="token" value="'.($token ? $token : $this->token).'" />
		</form><script type="text/javascript" src="'._PS_JS_DIR_.'jquery/maintainscroll.jquery.js"></script>
';
		if (isset($this->_includeTab) AND sizeof($this->_includeTab))
			echo '<br /><br />';
			
			echo '	';
		echo '</div></div>';
	}
	
	
	public function postProcessPCT()
	{
		global $currentIndex, $cookie;
		if (!isset($this->table))
			return false;

		// set token
		$token = Tools::getValue('token') ? Tools::getValue('token') : $this->token;

		// Sub included tab postProcessing
		$this->includeSubTab('postProcess', array('status', 'submitAdd1', 'submitDel', 'delete', 'submitFilter', 'submitReset'));

		/* Delete object image */
		if (isset($_GET['deleteImage']))
		{
			if (Validate::isLoadedObject($object = $this->loadObject()))
				if (($object->deleteImage()))
					Tools::redirectAdmin($currentIndex.'&add'.$this->table.'&'.$this->identifier.'='.Tools::getValue($this->identifier).'&conf=7&token='.$token);
			$this->_errors[] = Tools::displayError('An error occurred during image deletion (cannot load object).');
		}

		/* Delete object */
		elseif (isset($_GET['delete'.$this->table]))
		{
			if ($this->tabAccess['delete'] === '1')
			{
				if (Validate::isLoadedObject($object = $this->loadObject()) AND isset($this->fieldImageSettings))
				{
					// check if request at least one object with noZeroObject
					if (isset($object->noZeroObject) AND sizeof(call_user_func(array($this->className, $object->noZeroObject))) <= 1)
						$this->_errors[] = Tools::displayError('You need at least one object.').' <b>'.$this->table.'</b><br />'.Tools::displayError('You cannot delete all of the items.');
					else
					{
						if ($this->deleted)
						{
							$object->deleteImage();
							$object->deleted = 1;
							if ($object->update())
								Tools::redirectAdmin($currentIndex.'&conf=1&token='.$token);
						}
						elseif ($object->delete())
							Tools::redirectAdmin($currentIndex.'&conf=1&token='.$token);
						$this->_errors[] = Tools::displayError('An error occurred during deletion.');
					}
				}
				else
					$this->_errors[] = Tools::displayError('An error occurred while deleting object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to delete here.');
		}

		/* Change object statuts (active, inactive) */
		elseif ((isset($_GET['status'.$this->table]) OR isset($_GET['status'])) AND Tools::getValue($this->identifier))
		{
			if ($this->tabAccess['edit'] === '1')
			{
				if (Validate::isLoadedObject($object = $this->loadObject()))
				{
					if ($object->toggleStatus())
						Tools::redirectAdmin($currentIndex.'&conf=5'.((($id_category = (int)(Tools::getValue('id_category'))) AND Tools::getValue('id_product')) ? '&id_category='.$id_category : '').'&token='.$token);
					else
						$this->_errors[] = Tools::displayError('An error occurred while updating status.');
				}
				else
					$this->_errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
		}
		/* Move an object */
		elseif (isset($_GET['position']))
		{
			if ($this->tabAccess['edit'] !== '1')
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
			elseif (!Validate::isLoadedObject($object = $this->loadObject()))
				$this->_errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			elseif (!$object->updatePosition((int)(Tools::getValue('way')), (int)(Tools::getValue('position'))))
				$this->_errors[] = Tools::displayError('Failed to update the position.');
			else
				Tools::redirectAdmin($currentIndex.'&'.$this->table.'Orderby=position&'.$this->table.'Orderway=asc&conf=5'.(($id_identifier = (int)(Tools::getValue($this->identifier))) ? ('&'.$this->identifier.'='.$id_identifier) : '').'&token='.$token);
		}
		/* Delete multiple objects */
		elseif (Tools::getValue('submitDel'.$this->table))
		{
			if ($this->tabAccess['delete'] === '1')
			{
				if (isset($_POST[$this->table.'Box']))
				{
					$object = new $this->className();
					if (isset($object->noZeroObject) AND
						// Check if all object will be deleted
						(sizeof(call_user_func(array($this->className, $object->noZeroObject))) <= 1 OR sizeof($_POST[$this->table.'Box']) == sizeof(call_user_func(array($this->className, $object->noZeroObject)))))
						$this->_errors[] = Tools::displayError('You need at least one object.').' <b>'.$this->table.'</b><br />'.Tools::displayError('You cannot delete all of the items.');
					else
					{
						$result = true;
						if ($this->deleted)
						{
							foreach(Tools::getValue($this->table.'Box') as $id)
							{
								$toDelete = new $this->className($id);
								$toDelete->deleted = 1;
								$result = $result AND $toDelete->update();
							}
						}
						else
							$result = $object->deleteSelection(Tools::getValue($this->table.'Box'));

						if ($result)
							Tools::redirectAdmin($currentIndex.'&conf=2&token='.$token);
						$this->_errors[] = Tools::displayError('An error occurred while deleting selection.');
					}
				}
				else
					$this->_errors[] = Tools::displayError('You must select at least one element to delete.');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to delete here.');
		}

		/* Create or update an object */
		elseif (Tools::getValue('submitAdd'.$this->table))
		{
			/* Checking fields validity */
			$this->validateRules();
			if (!sizeof($this->_errors))
			{
				$id = (int)(Tools::getValue($this->identifier));

				/* Object update */
				if (isset($id) AND !empty($id))
				{
					if ($this->tabAccess['edit'] === '1' OR ($this->table == 'employee' AND $cookie->id_employee == Tools::getValue('id_employee') AND Tools::isSubmit('updateemployee')))
					{
						$object = new $this->className($id);
						if (Validate::isLoadedObject($object))
						{
							/* Specific to objects which must not be deleted */
							if ($this->deleted AND $this->beforeDelete($object))
							{
								// Create new one with old objet values
								$objectNew = new $this->className($object->id);
								$objectNew->id = NULL;
								$objectNew->date_add = '';
								$objectNew->date_upd = '';

								// Update old object to deleted
								$object->deleted = 1;
								$object->update();

								// Update new object with post values
								$this->copyFromPost($objectNew, $this->table);
								$result = $objectNew->add();
								if (Validate::isLoadedObject($objectNew))
									$this->afterDelete($objectNew, $object->id);
							}
							else
							{
								$this->copyFromPost($object, $this->table);
								$result = $object->update();
								$this->afterUpdate($object);
							}
							if (!$result)
								$this->_errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.'</b> ('.Db::getInstance()->getMsgError().')';
							elseif ($this->postImage($object->id) AND !sizeof($this->_errors))
							{
								$parent_id = (int)(Tools::getValue('id_parent', 1));
								// Specific back redirect
								if ($back = Tools::getValue('back'))
									Tools::redirectAdmin(urldecode($back).'&conf=4');
								// Specific scene feature
								if (Tools::getValue('stay_here') == 'on' || Tools::getValue('stay_here') == 'true' || Tools::getValue('stay_here') == '1')
									Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$object->id.'&conf=4&updatescene&token='.$token);
								// Save and stay on same form
								if (Tools::isSubmit('submitAdd'.$this->table.'AndStay'))
									Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$object->id.'&conf=4&update'.$this->table.'&token='.$token);
								// Save and back to parent
								if (Tools::isSubmit('submitAdd'.$this->table.'AndBackToParent'))
									Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$parent_id.'&conf=4&token='.$token);
								// Default behavior (save and back)
								Tools::redirectAdmin($currentIndex.($parent_id ? '&'.$this->identifier.'='.$object->id : '').'&conf=4&token='.$token);
							}
						}
						else
							$this->_errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
					}
					else
						$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
				}

				/* Object creation */
				else
				{
					if ($this->tabAccess['add'] === '1')
					{
						$object = new $this->className();
						$this->copyFromPost($object, $this->table);
						if (!$object->add())
							$this->_errors[] = Tools::displayError('An error occurred while creating object.').' <b>'.$this->table.' ('.mysql_error().')</b>';
						elseif (($_POST[$this->identifier] = $object->id /* voluntary */) AND $this->postImage($object->id) AND !sizeof($this->_errors) AND $this->_redirect)
						{
							$parent_id = (int)(Tools::getValue('id_parent', 1));
							$this->afterAdd($object);
							// Save and stay on same form
							if (Tools::isSubmit('submitAdd'.$this->table.'AndStay'))
								Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$object->id.'&conf=3&update'.$this->table.'&token='.$token);
							// Save and back to parent
							if (Tools::isSubmit('submitAdd'.$this->table.'AndBackToParent'))
								Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$parent_id.'&conf=3&token='.$token);
							// Default behavior (save and back)
							Tools::redirectAdmin($currentIndex.($parent_id ? '&'.$this->identifier.'='.$object->id : '').'&conf=3&token='.$token);
						}
					}
					else
						$this->_errors[] = Tools::displayError('You do not have permission to add here.');
				}
			}
			$this->_errors = array_unique($this->_errors);
		}

		/* Cancel all filters for this tab */
		elseif (isset($_POST['submitReset'.$this->table]))
		{
			$filters = $cookie->getFamily($this->table.'Filter_');
			foreach ($filters AS $cookieKey => $filter)
				if (strncmp($cookieKey, $this->table.'Filter_', 7 + Tools::strlen($this->table)) == 0)
					{
						$key = substr($cookieKey, 7 + Tools::strlen($this->table));
						/* Table alias could be specified using a ! eg. alias!field */
						$tmpTab = explode('!', $key);
						$key = (count($tmpTab) > 1 ? $tmpTab[1] : $tmpTab[0]);
						if (array_key_exists($key, $this->fieldsDisplay))
							unset($cookie->$cookieKey);
					}
			if (isset($cookie->{'submitFilter'.$this->table}))
				unset($cookie->{'submitFilter'.$this->table});
			if (isset($cookie->{$this->table.'Orderby'}))
				unset($cookie->{$this->table.'Orderby'});
			if (isset($cookie->{$this->table.'Orderway'}))
				unset($cookie->{$this->table.'Orderway'});
			unset($_POST);
		}

		/* Submit options list */
		elseif (Tools::getValue('submitOptions'.$this->table))
		{
			$this->updateOptions($token);
		}

		/* Manage list filtering */
		elseif (Tools::isSubmit('submitFilter'.$this->table) OR $cookie->{'submitFilter'.$this->table} !== false)
		{
			$_POST = array_merge($cookie->getFamily($this->table.'Filter_'), (isset($_POST) ? $_POST : array()));
			foreach ($_POST AS $key => $value)
			{
				/* Extracting filters from $_POST on key filter_ */
				if ($value != NULL AND !strncmp($key, $this->table.'Filter_', 7 + Tools::strlen($this->table)))
				{
					$key = Tools::substr($key, 7 + Tools::strlen($this->table));
					/* Table alias could be specified using a ! eg. alias!field */
					$tmpTab = explode('!', $key);
					$filter = count($tmpTab) > 1 ? $tmpTab[1] : $tmpTab[0];
					if ($field = $this->filterToField($key, $filter))
					{
						$type = (array_key_exists('filter_type', $field) ? $field['filter_type'] : (array_key_exists('type', $field) ? $field['type'] : false));
						if (($type == 'date' OR $type == 'datetime') AND is_string($value))
							$value = unserialize($value);
						$key = isset($tmpTab[1]) ? $tmpTab[0].'.`'.bqSQL($tmpTab[1]).'`' : '`'.bqSQL($tmpTab[0]).'`';
						if (array_key_exists('tmpTableFilter', $field))
							$sqlFilter = & $this->_tmpTableFilter;
						elseif (array_key_exists('havingFilter', $field))
							$sqlFilter = & $this->_filterHaving;
						else
							$sqlFilter = & $this->_filter;

						/* Only for date filtering (from, to) */
						if (is_array($value))
						{
							if (isset($value[0]) AND !empty($value[0]))
							{
								if (!Validate::isDate($value[0]))
									$this->_errors[] = Tools::displayError('\'from:\' date format is invalid (YYYY-MM-DD)');
								else
									$sqlFilter .= ' AND (( '.$key.' >= \''.pSQL(Tools::dateFrom($value[0])).'\'';
							}

							if (isset($value[1]) AND !empty($value[1]))
							{
								if (!Validate::isDate($value[1]))
									$this->_errors[] = Tools::displayError('\'to:\' date format is invalid (YYYY-MM-DD)');
								else
									$sqlFilter .= ' AND '.$key.' <= \''.pSQL(Tools::dateTo($value[1])).'\') OR ( date_add >= \''.pSQL(Tools::dateFrom($value[0])).'\' AND date_add <= \''.pSQL(Tools::dateTo($value[1])).'\'))';
							}
						}
						else
						{
							$sqlFilter .= ' AND ';
							if ($type == 'int' OR $type == 'bool')
								$sqlFilter .= (($key == $this->identifier OR $key == '`'.$this->identifier.'`' OR $key == '`active`') ? 'a.' : '').pSQL($key).' = '.(int)($value).' ';
							elseif ($type == 'decimal')
								$sqlFilter .= (($key == $this->identifier OR $key == '`'.$this->identifier.'`') ? 'a.' : '').pSQL($key).' = '.(float)($value).' ';
							elseif ($type == 'select')
								$sqlFilter .= (($key == $this->identifier OR $key == '`'.$this->identifier.'`') ? 'a.' : '').pSQL($key).' = \''.pSQL($value).'\' ';
							else
								$sqlFilter .= (($key == $this->identifier OR $key == '`'.$this->identifier.'`') ? 'a.' : '').pSQL($key).' LIKE \'%'.pSQL($value).'%\' ';
						}
					}
				}
			}
		}
		elseif (Tools::isSubmit('submitFields') AND $this->requiredDatabase AND $this->tabAccess['add'] === '1' AND $this->tabAccess['delete'] === '1')
		{
			if (!is_array($fields = Tools::getValue('fieldsBox')))
				$fields = array();

			$object = new $this->className();
			if (!$object->addFieldsRequiredDatabase($fields))
				$this->_errors[] = Tools::displayError('Error in updating required fields');
			else
				Tools::redirectAdmin($currentIndex.'&conf=4&token='.$token);
		}
	}
	
	
}

