<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

class LicenzeEzdial extends AdminTab
{

	public $connection;
	
	function getValue($query)
	{
		$result = mysqli_query($this->connection, $query);
		$results = mysqli_fetch_array($result);
		return $results[0];
	}

	function getRow($query)
	{
		$result = mysqli_query($this->connection, $query);
		$results = mysqli_fetch_array($result);
		return $results;
	}

	function executeS($query)
	{
	
		$result = mysqli_query($this->connection, $query);
		
		$array_results = array();
		while($row = mysqli_fetch_array($result))
		{
			$array_results[] = $row;
		
		}
		return $array_results;
	}

	public function display()
	{
		global $cookie, $currentIndex;

		echo '<h1>Licenze ezDial</h1>';
		
		if(Tools::getIsset('function'))
		{
			if(Tools::getValue('function') == 'cerca')
			{
				echo '<h2>Cerca una licenza ezDial</h2>';
				
				if(Tools::getIsset('submit_cerca'))
				{
					$connection = mysqli_connect('localhost','ezdirectezdial','zRv16^p1qwudsuh5(a') or die('connection error');
					mysqli_select_db($connection,'ezdir_ezdial');
					
					$macs = $this->executeS('SELECT * FROM ezdial_mac_address WHERE mac_address LIKE "%'.Tools::getValue('cerca_licenza').'%"');
					
					$lics = $this->executeS('SELECT * FROM ezdial_licenze WHERE licenza LIKE "%'.Tools::getValue('cerca_licenza').'%"');
					
					
					if(count($macs) > 0 || count($lics) > 0)
					{
						echo '<table class="table"><thead><tr><th>Licenza</th><th>MAC Address</th></tr></thead>';
						foreach($macs as $mac)
						{
							$licenza_assegnata = $this->getValue('SELECT id_licenza FROM ezdial_mac_licenze WHERE id_mac_address = '.$mac['id_mac_address']);
							if($licenza_assegnata > 0)
								$lic_mac = $this->getValue('SELECT licenza FROM ezdial_licenze WHERE id_licenza = '.$licenza_assegnata);
							else
								$lic_mac = '<em>Nessuna licenza assegnata</em>';
							
							echo '<tr><td>'.$lic_mac.'</td><td>'.$mac['mac_address'].'</td></tr>';
						}
						
						foreach($lics as $lic)
						{
							$mac_assegnato = $this->getValue('SELECT id_mac_address FROM ezdial_mac_licenze WHERE id_licenza = '.$lic['id_licenza']);
							if($mac_assegnato > 0)
								$mac_lic = $this->getValue('SELECT mac_address FROM ezdial_mac_address WHERE id_mac_address = '.$mac_assegnato);
							else
								$mac_lic = '<em>Non assegnata</em>';
							
							echo '<tr><td>'.$lic['licenza'].'</td><td>'.$mac_lic.'</td></tr>';
						}
						
						echo '</table><br /><br />';
						$connection = mysqli_connect(_DB_SERVER_,_DB_USER_,_DB_PASSWD_);
						mysqli_select_db($connection,_DB_NAME_);
					}
					else
						echo '<strong style="color:red; font-size:16px">La ricerca per "'.Tools::getValue('cerca_licenza').'" non ha prodotto risultati</strong><br /><br />';
					
				}
				
				echo '<form method="post" action="'.$currentIndex.'&token='.$this->token.'&function=cerca">
				
				
				Cerca per codice licenza o per Mac Address nel campo sotto:<br /><br />
				
				<input name="cerca_licenza" type="text" id="cerca_licenza" onfocus="if(this.value==\'Inserisci licenza o Mac Address...\'){this.value=\'\';};return false;" onblur="if(this.value==\'\'){this.value=\'Inserisci licenza o Mac Address...\';};return false;" name="search_query" value="Inserisci licenza o Mac Address..." autocomplete="off" style="width:350px">
				<input class="button" name="submit_cerca" value="Cerca" type="submit" />
				</form>
				';
				
				
				
			}	

			else if(Tools::getValue('function') == 'crea')
			{
				echo '<h2>Crea licenze ezDial</h2>';
				
				if(Tools::getIsset('submit_crea'))
				{
					$this->connection = mysqli_connect('localhost','ezdirectezdial','zRv16^p1qwudsuh5(a');
					mysqli_select_db($this->connection,'ezdir_ezdial');
					
					
					echo '<strong>Ecco i codici delle licenze appena create</strong>: <br /><br />';
					for($i = 0;$i< Tools::getValue('crea_licenza'); $i++)
					{
						$lii = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(16/strlen($x)) )),1,16);
						$this->executeS('INSERT INTO ezdial_licenze VALUES (NULL, "'.$lii.'")');
						echo $lii.'<br />';
					}	
					echo '<br /><br />';
				}
				
				echo '<form method="post" action="'.$currentIndex.'&token='.$this->token.'&function=crea">
				
				
				Da questo form puoi creare nuove licenze ezDial. Inserisci il numero di licenze che vuoi creare e verranno prodotte automaticamente licenze di 16 caratteri.<br /><br />
				
				<input name="crea_licenza" type="text" id="crea_licenza" onfocus="if(this.value==\'Inserisci il numero di licenze che vuoi creare...\'){this.value=\'\';};return false;" onblur="if(this.value==\'\'){this.value=\'Inserisci il numero di licenze che vuoi creare...\';};return false;" name="search_query" value="Inserisci il numero di licenze che vuoi creare..." autocomplete="off" style="width:350px">
				<input class="button" name="submit_crea" value="Crea" type="submit" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" />
				</form>
				';
				
				
				
			}		
			
			else if(Tools::getValue('function') == 'associa')
			{
				echo '<h2>Associa licenze ezDial</h2>';
				
				if(Tools::getIsset('submit_associa'))
				{
					$connection = mysqli_connect('localhost','ezdirectezdial','zRv16^p1qwudsuh5(a') or die('connection error');
					mysqli_select_db($connection,'ezdir_ezdial');
				
					$lics = $this->executeS('SELECT * FROM ezdial_licenze WHERE licenza LIKE "%'.Tools::getValue('cerca_licenza').'%"');
					
					
					if(count($lics) > 0)
					{
						echo '<form method="post" action="'.$currentIndex.'&token='.$this->token.'&function=associa">';
						echo '<table class="table"><thead><tr><th>Licenza</th><th>MAC Address</th></tr></thead>';
						
						foreach($lics as $lic)
						{
							$mac_assegnato = $this->getValue('SELECT id_mac_address FROM ezdial_mac_licenze WHERE id_licenza = '.$lic['id_licenza']);
							if($mac_assegnato > 0)
								$mac_lic = $this->getValue('SELECT mac_address FROM ezdial_mac_address WHERE id_mac_address = '.$mac_assegnato);
							else
								$mac_lic = '<input name="inserisci_mac['.$lic['licenza'].']" type="text" id="inserisci_mac['.$lic['licenza'].']" onfocus="if(this.value==\'Inserisci MAC address...\'){this.value=\'\';};return false;" onblur="if(this.value==\'\'){this.value=\'Inserisci MAC address...\';};return false;" name="search_query" value="Inserisci MAC address..." autocomplete="off" style="width:350px">';
							
							echo '<tr><td>'.$lic['licenza'].'</td><td>'.$mac_lic.'</td></tr>';
						}
						
						echo '</table><br /><input class="button" name="submit_associa_mac" value="Salva associazioni" type="submit" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" /></form><br /><br />';
						$connection = mysqli_connect(_DB_SERVER_,_DB_USER_,_DB_PASSWD_);
						mysqli_select_db($connection,_DB_NAME_);
					}
					else
						echo '<strong style="color:red; font-size:16px">La ricerca per "'.Tools::getValue('cerca_licenza').'" non ha prodotto risultati</strong><br /><br />';
					
				}
				
				else if(Tools::getIsset('submit_associa_mac'))
				{
					$connection = mysql_connect('localhost','ezdirectezdial','zRv16^p1qwudsuh5(a') or die('connection error');
					mysql_select_db('ezdir_ezdial',$connection);
					
					foreach($_POST['inserisci_mac'] as $key => $value)
					{
						if($value == '' || $value == 'Inserisci MAC address...')
						{
							
						}
						else
						{
							$id_licenza = $this->getValue('SELECT id_licenza FROM ezdial_licenze WHERE licenza = "'.$key.'"');
							$mac = $this->getValue('SELECT id_mac_address FROM ezdial_mac_address WHERE mac_address = "'.$value.'"');
							if($mac <= 0)
							{
								$this->executeS('INSERT INTO ezdial_mac_address VALUES (NULL, "'.$value.'")');
								$id_mac = mysql_insert_id();
							}
							else
								$id_mac = $mac;
							
							$this->executeS('INSERT INTO ezdial_mac_licenze VALUES ("'.$id_mac.'", "'.$id_licenza.'")');
						}
						
					}
					echo '<strong>Associazioni create con successo</strong><br /><br />';
					
					$connection = mysql_connect(_DB_SERVER_,_DB_USER_,_DB_PASSWD_) or die('connection error');
					mysql_select_db(_DB_NAME_,$connection);
					
				}
				
				echo '<form method="post" action="'.$currentIndex.'&token='.$this->token.'&function=associa">
				
				
				Cerca per codice licenza nel campo sotto:<br /><br />
				
				<input name="cerca_licenza" type="text" id="cerca_licenza" onfocus="if(this.value==\'Inserisci licenza...\'){this.value=\'\';};return false;" onblur="if(this.value==\'\'){this.value=\'Inserisci licenza...\';};return false;" name="search_query" value="Inserisci licenza..." autocomplete="off" style="width:350px">
				<input class="button" name="submit_associa" value="Cerca" type="submit" />
				</form>
				';
				
				
				
			}
			
			else if(Tools::getValue('function') == 'dissocia')
			{
				echo '<h2>Svincola licenze ezDial</h2>';
				
				if(Tools::getIsset('submit_dissocia'))
				{
					
					$connection = mysql_connect('localhost','ezdirectezdial','zRv16^p1qwudsuh5(a') or die('connection error');
					mysql_select_db('ezdir_ezdial',$connection);
					
					$macs = $this->executeS('SELECT * FROM ezdial_mac_address WHERE mac_address LIKE "%'.Tools::getValue('cerca_licenza').'%"');
					
					$lics = $this->executeS('SELECT * FROM ezdial_licenze WHERE licenza LIKE "%'.Tools::getValue('cerca_licenza').'%"');
					
					
					if(count($macs) > 0 || count($lics) > 0)
					{
						echo '<form method="post" action="'.$currentIndex.'&token='.$this->token.'&function=dissocia">';
						echo '<table class="table"><thead><tr><th>Licenza</th><th>MAC Address</th><th>Svincola</th></tr></thead>';
						foreach($macs as $mac)
						{
							$licenza_assegnata = $this->getValue('SELECT id_licenza FROM ezdial_mac_licenze WHERE id_mac_address = '.$mac['id_mac_address']);
							if($licenza_assegnata > 0)
							{
								$lic_mac = $this->getValue('SELECT licenza FROM ezdial_licenze WHERE id_licenza = '.$licenza_assegnata);
								$macli = $mac['mac_address'].'</td><td><input type="checkbox" name="svincola['.$licenza_assegnata.'_'.$mac['id_mac_address'].']" /></td>';
							}	
							else
							{
								$lic_mac = '<em>Nessuna licenza assegnata</em></td><td>';
								$macli = $mac['mac_address'].'</td><td></td>';
							}
							echo '<tr><td>'.$lic_mac.'</td><td>'.$macli;
						}
						
						foreach($lics as $lic)
						{
							$mac_assegnato = $this->getValue('SELECT id_mac_address FROM ezdial_mac_licenze WHERE id_licenza = '.$lic['id_licenza']);
							if($mac_assegnato > 0)
							{
								$mac_lic = $this->getValue('SELECT mac_address FROM ezdial_mac_address WHERE id_mac_address = '.$mac_assegnato);
								$mac_lic .= '</td><td><input type="checkbox" name="svincola['.$lic['id_licenza'].'_'.$mac_assegnato.']" />';
							}	
							else
								$mac_lic = '<em>Non assegnata</em></td><td>';
							
							echo '<tr><td>'.$lic['licenza'].'</td><td>'.$mac_lic.'</td></tr>';
						}
						
						echo '</table><br /><input class="button" name="submit_dissocia_mac" value="Svincola licenze flaggate" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" type="submit" /></form><br /><br />';
						$connection = mysql_connect(_DB_SERVER_,_DB_USER_,_DB_PASSWD_) or die('connection error');
						mysql_select_db(_DB_NAME_,$connection);
					}
					else
						echo '<strong style="color:red; font-size:16px">La ricerca per "'.Tools::getValue('cerca_licenza').'" non ha prodotto risultati</strong><br /><br />';
					
				
					
				}
				
				else if(Tools::getIsset('submit_dissocia_mac'))
				{
					$connection = mysql_connect('localhost','ezdirectezdial','zRv16^p1qwudsuh5(a') or die('connection error');
					mysql_select_db('ezdir_ezdial',$connection);
					
					foreach($_POST['svincola'] as $key => $value)
					{
						$ids = explode('_',$key);
						$this->executeS('DELETE FROM ezdial_mac_licenze WHERE id_licenza = '.$ids[0].' AND id_mac_address = '.$ids[1]);
						$this->executeS('DELETE FROM ezdial_mac_address WHERE id_mac_address = '.$ids[1]);
						
					}
					echo '<strong>Licenze svincolate con successo</strong><br /><br />';
					
					$connection = mysql_connect(_DB_SERVER_,_DB_USER_,_DB_PASSWD_) or die('connection error');
					mysql_select_db(_DB_NAME_,$connection);
					
				}
				
				echo '<form method="post" action="'.$currentIndex.'&token='.$this->token.'&function=dissocia">
				
				
				Cerca per codice licenza o per mac address nel campo sotto:<br /><br />
				
				<input name="cerca_licenza" type="text" id="cerca_licenza" onfocus="if(this.value==\'Inserisci licenza o Mac Address...\'){this.value=\'\';};return false;" onblur="if(this.value==\'\'){this.value=\'Inserisci licenza o Mac Address...\';};return false;" name="search_query" value="Inserisci licenza o Mac Address..." autocomplete="off" style="width:350px">
				<input class="button" name="submit_dissocia" value="Cerca" type="submit" />
				</form>
				';
				
				
				
			}
		}
		else
		{
			echo 'Funzioni per licenze ezDial <br /><br />
					
			<a class="button" style="display:block; text-align:center; width:450px" href="'.$currentIndex.'&token='.$this->token.'&function=crea">Crea nuove licenze ezDial</a><br /><br />';
				
			echo '<a class="button" style="display:block; text-align:center; width:450px" href="'.$currentIndex.'&token='.$this->token.'&function=dissocia">Svincola licenze gi&agrave; assegnate</a><br /><br />';
					
			echo '<a class="button" style="display:block; text-align:center; width:450px" href="'.$currentIndex.'&token='.$this->token.'&function=associa">Associa licenze non assegnate</a><br /><br />';
			
			echo '<a class="button" style="display:block; text-align:center; width:450px" href="'.$currentIndex.'&token='.$this->token.'&function=cerca">Cerca una licenza</a><br /><br />';
		}
	}
}


