<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14516 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
ini_set('memory_limit', '10240M');
set_time_limit(900);


class AdminCustomers extends AdminTab
{
	public function __construct()
	{
		global $cookie;
	 	$this->table = 'customer';
	 	$this->className = 'Customer';
	 	$this->lang = false;
	 	$this->edit = false;
	 	$this->view = true;
	 	$this->delete = true;
		$this->deleted = true;
		$this->requiredDatabase = true;
		$this->addressType = 'customer';
		$this->_defaultOrderBy = 'company';
		$countries = Country::getCountries((int)($cookie->id_lang));
		foreach ($countries AS $country)
			$this->countriesArray[$country['id_country']] = $country['name'];

		$this->_select = '
		CONCAT(a.firstname," ",a.lastname) customer,
		gl.name AS group_name,
		(CASE a.is_company
		WHEN 1
		THEN (
			a.company
		)
		ELSE CONCAT(a.firstname," ",a.lastname)
		END
		) cliente,
		
		(CASE a.is_company
		WHEN 1
		THEN (
			CASE a.vat_number
			WHEN "" THEN a.tax_code
			ELSE a.vat_number
			END
		)
		WHEN 0
		THEN a.tax_code
		ELSE a.tax_code
		END
		) piva_cf,
		
		(SELECT iso_code FROM state WHERE id_state = (SELECT id_state FROM address WHERE id_customer = a.id_customer AND fatturazione = 1 AND active = 1 AND deleted = 0 LIMIT 1)) provincia,
		
		(CASE (SELECT id_country FROM address WHERE id_customer = a.id_customer AND fatturazione = 1 AND active = 1 AND deleted = 0 LIMIT 1)
		WHEN 10 THEN (CASE
		(SELECT iso_code FROM state WHERE id_state = (SELECT id_state FROM address WHERE id_customer = a.id_customer AND fatturazione = 1 AND active = 1 AND deleted = 0 LIMIT 1)) 
		WHEN "CH" THEN "ABR" WHEN "AQ" THEN "ABR" WHEN "PE" THEN "ABR" WHEN "TE" THEN "ABR"
		WHEN "MT" THEN "BAS" WHEN "PZ" THEN "BAS"
		WHEN "CZ" THEN "CAL" WHEN "CS" THEN "CAL" WHEN "KR" THEN "CAL" WHEN "RC" THEN "CAL" WHEN "VV" THEN "CAL"
		WHEN "AV" THEN "CAM" WHEN "BN" THEN "CAM" WHEN "CE" THEN "CAM" WHEN "NA" THEN "CAM" WHEN "SA" THEN "CAM"
		WHEN "BO" THEN "EMR" WHEN "FE" THEN "EMR" WHEN "FC" THEN "EMR" WHEN "MO" THEN "EMR" WHEN "PR" THEN "EMR" WHEN "PC" THEN "EMR" WHEN "RA" THEN "EMR" WHEN "RE" THEN "EMR" WHEN "RN" THEN "EMR"
		WHEN "GO" THEN "FVG" WHEN "PN" THEN "FVG" WHEN "TS" THEN "FVG" WHEN "UD" THEN "FVG"
		WHEN "FR" THEN "LAZ" WHEN "LT" THEN "LAZ" WHEN "RI" THEN "LAZ" WHEN "RM" THEN "LAZ" WHEN "VT" THEN "LAZ"
		WHEN "GE" THEN "LIG" WHEN "IM" THEN "LIG" WHEN "SP" THEN "LIG" WHEN "SV" THEN "LIG"
		WHEN "BG" THEN "LOM" WHEN "BS" THEN "LOM" WHEN "CO" THEN "LOM" WHEN "CR" THEN "LOM" WHEN "LC" THEN "LOM" WHEN "LO" THEN "LOM" WHEN "MN" THEN "LOM" WHEN "MI" THEN "LOM" WHEN "MB" THEN "LOM" WHEN "PV" THEN "LOM" WHEN "SO" THEN "LOM" WHEN "VA" THEN "LOM"
		WHEN "AN" THEN "MAR" WHEN "AP" THEN "MAR" WHEN "FM" THEN "MAR" WHEN "MC" THEN "MAR" WHEN "PU" THEN "MAR"
		WHEN "CB" THEN "MOL" WHEN "IS" THEN "MOL"
		WHEN "AL" THEN "PIE" WHEN "AT" THEN "PIE" WHEN "BI" THEN "PIE" WHEN "CN" THEN "PIE" WHEN "NO" THEN "PIE" WHEN "TO" THEN "PIE" WHEN "VB" THEN "PIE" WHEN "VC" THEN "PIE"
		WHEN "BA" THEN "PGL" WHEN "BR" THEN "PGL" WHEN "FG" THEN "PGL" WHEN "LC" THEN "PGL" WHEN "TA" THEN "PGL" WHEN "BT" THEN "PGL"
		WHEN "CA" THEN "SAR" WHEN "CI" THEN "SAR" WHEN "VS" THEN "SAR" WHEN "NU" THEN "SAR" WHEN "OG" THEN "SAR" WHEN "OT" THEN "SAR" WHEN "OR" THEN "SAR" WHEN "SS" THEN "SAR"
		WHEN "AG" THEN "SIC" WHEN "CL" THEN "SIC" WHEN "CT" THEN "SIC" WHEN "EN" THEN "SIC" WHEN "ME" THEN "SIC" WHEN "PA" THEN "SIC" WHEN "RG" THEN "SIC" WHEN "SR" THEN "SIC" WHEN "TP" THEN "SIC"
		WHEN "AR" THEN "TOS" WHEN "FI" THEN "TOS" WHEN "GR" THEN "TOS" WHEN "LI" THEN "TOS" WHEN "LU" THEN "TOS" WHEN "MS" THEN "TOS" WHEN "PI" THEN "TOS" WHEN "PT" THEN "TOS" WHEN "PO" THEN "TOS" WHEN "SI" THEN "TOS"
		WHEN "BZ" THEN "TAA" WHEN "TN" THEN "TAA"
		WHEN "PG" THEN "UMB" WHEN "TR" THEN "UMB"
		WHEN "AO" THEN "VAO"
		WHEN "BL" THEN "VEN" WHEN "PD" THEN "VEN" WHEN "RO" THEN "VEN" WHEN "TV" THEN "VEN" WHEN "VE" THEN "VEN" WHEN "VR" THEN "VEN" WHEN "VI" THEN "VEN"
		ELSE "--"
		END) 
		ELSE "EEE" END) regione,

		
		(YEAR(CURRENT_DATE)-YEAR(`birthday`)) - (RIGHT(CURRENT_DATE, 5) < RIGHT(`birthday`, 5)) as age';
		
		$this->_join = ''; 
		
		
		
		if($cookie->profile == 7)
		{
			$this->_join .= 'LEFT JOIN `'._DB_PREFIX_.'customer_amministrazione` ca ON (ca.`id_customer` = a.`id_customer`)';
			$this->_where = ' AND ca.agente = '.$cookie->id_employee;
			$this->_group = 'GROUP BY a.id_customer';
			
		}
		$genders = array(1 => $this->l('M'), 2 => $this->l('F'), 9 => $this->l('?'));
		
		if(Tools::getIsset('filter'))
		{
			$cookie->customerFilter_id_default_group = Tools::getValue('filter');
			$cookie->submitFiltercustomer = true;
		}
		
		$this->_join .= ' LEFT JOIN (select * from group_lang where id_lang = 5) gl ON a.id_default_group = gl.id_group';
		
		$regioniArray = array("ABR", "BAS", "CAL", "CAM", "EMR", "FVG", "LAZ", "LIG", "LOM", "MAR", "MOL", "PIE", "PGL", "SAR", "SIC", "TOS", "TAA", "UMB", "VEN", "EEE");
		
		$groupArray = array(1 => 'Business', 2 => 'Privati', 3 => 'Rivenditori 1', 4 => 'Call center', 5 => 'PA', 8 => 'Top Business', 11 => 'Fornitori', 12 => 'Distributori', 15 => 'Rich. Rivenditore', 16 => 'Amazon', 17 => 'ePrice', 18 => 'Hotel', 19 => 'Top Hotel');
		
 		$this->fieldsDisplay = array(
		'id_customer' => array('title' => $this->l('ID'), 'align' => 'center', 'font-size' =>'12px', 'width' => 40, 'widthColumn' => 40),
		'company' => array('title' => $this->l('Company'), 'font-size' =>'14px',  'width' => 40, 'widthColumn' => 170),

		'customer' => array('title' => $this->l('Persona'), 'width' => 40, 'filter_key' => 'customer', 'tmpTableFilter' => true, 'widthColumn' => 70),
		'group_name' => array('title' => $this->l('Group'), 'width' => 30, 'widthColumn' => 45,  'type' => 'select', 'select' => $groupArray, 'filter_key' => 'id_default_group', 'tmpTableFilter' => true),
		'ex_rivenditore' => array('title' => $this->l('Ex. Riv.'), 'width' => 30, 'supplier' => 'status', 'type' => 'bool', 'widthColumn' => 30),
		'supplier' => array('title' => $this->l('Forn.'), 'width' => 25, 'align' => 'center', 'supplier' => 'status', 'type' => 'bool', 'orderby' => false, 'widthColumn' => 30),
		'provincia' => array('title' => $this->l('Prov.'), 'width' => 30, 'filter_key' => 'provincia', 'tmpTableFilter' => true, 'widthColumn' => 35),
		'regione' => array('title' => $this->l('Reg.'), 'width' => 30, 'filter_key' => 'regione', 'tmpTableFilter' => true, 'widthColumn' => 35),
		'piva_cf' => array('title' => $this->l('P.I. / CF'), 'width' => 40, 'filter_key' => 'piva_cf', 'tmpTableFilter' => true, 'widthColumn' => 120),
		'email' => array('title' => $this->l('E-mail'), 'width' => 40, 'maxlength' => 8, 'widthColumn' => 70),
		
		'active' => array('title' => $this->l('Attivo'), 'width' => 25, 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false, 'widthColumn' => 30),
		
		'date_add' => array('title' => $this->l('Reg.'), 'width' => 30, 'type' => 'date', 'align' => 'right', 'widthColumn' => 40),
		//'connect' => array('title' => $this->l('Connection'), 'width' => 60, 'type' => 'datetime', 'search' => false)
		);

		$this->optionTitle = $this->l('Customers options');
		$this->_fieldsOptions = array(
			'PS_PASSWD_TIME_FRONT' => array('title' => $this->l('Regenerate password:'), 'desc' => $this->l('Security minimum time to wait to regenerate the password'),'validation' => 'isUnsignedInt', 'cast' => 'intval', 'size' => 5, 'type' => 'text', 'suffix' => ' '.$this->l('minutes'))
		);

		parent::__construct();
	}

	public function postProcess()
	{
		global $currentIndex, $cookie;
		
		if(Tools::getIsset('getPDF')) {
		
			require_once('../classes/html2pdf/html2pdf.class.php');
			
			$id = Tools::getValue('id');
			$tipo = Tools::getValue('tipo');
			
			
			$content = Customer::exportConversation($id, $tipo);
			
			ob_clean(); ob_end_clean(); 
			$html2pdf = new HTML2PDF('P','A4','it');
			$html2pdf->setTestTdInOnePage(false);
			$html2pdf->WriteHTML($content);
			
			$html2pdf->Output($tipo.'-'.$id.'.pdf', 'D'); 
			
		}
		
		if($_GET['id_customer'] == 44431 && ($cookie->id_employee != 1 && $cookie->id_employee != 6))
			die("Non hai l'autorizzazione per lavorare su questa anagrafica");
		
		
		if(isset($_GET['updatecustomer']) && isset($_GET['conf']) && $_GET['conf'] == 4) {
			Tools::redirectAdmin($currentIndex.'&id_customer='.$_GET['id_customer'].'&viewcustomer&token='.$this->token.'&tab-container-1=1');
		}
		
		if(Tools::getIsset('submitAddcustomerNote') || Tools::getIsset('submitAddcustomerNote2')) {

			Db::getInstance()->executeS("UPDATE customer SET keyword = '".Tools::getValue('keyword')."' WHERE id_customer = ".$_POST['id_customer']."");
			
			foreach ($_POST['note_nuova'] as $id_nota=>$nota_id) {
			
				
				if($_POST['note_nuova'][$id_nota] == 0) {
					
					$testo_nota = Db::getInstance()->getValue('SELECT note FROM customer_note WHERE id_note = '.$id_nota.'');
					if($testo_nota != $_POST['note_nota'][$id_nota])
						Db::getInstance()->executeS("UPDATE customer_note SET note = '".addslashes($_POST['note_nota'][$id_nota])."', date_upd = '".date("Y-m-d H:i:s")."', id_employee = '".$cookie->id_employee."' WHERE id_note = ".$id_nota.""); 
				}
				
				else {
					if($_POST['note_nota'][$id_nota] != '')
					{
						Db::getInstance()->executeS("INSERT INTO customer_note
						(id_note,
						id_customer,
						id_employee,
						note,
						date_add,
						date_upd)
						
						VALUES (
						NULL,
						'".$_POST['id_customer']."',
						'".$cookie->id_employee."',
						'".addslashes($_POST['note_nota'][$id_nota])."',
						'".date("Y-m-d H:i:s")."',
						'".date("Y-m-d H:i:s")."'
						)");
					}
					
				}
			}
			if(Tools::getIsset('submitAddcustomerNote'))
				Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.Tools::getValue('id_customer').'&viewcustomer&conf=4&token='.$this->token);
		}	
		
		if(Tools::getIsset('submitAddcustomer')) {
			
			$company = Db::getInstance()->getValue('SELECT company FROM customer WHERE id_customer = '.$_POST['id_customer']);
			$gruppo_c = Db::getInstance()->getValue('SELECT id_default_group FROM customer WHERE id_customer = '.$_POST['id_customer']);
			//echo $company."*".$_POST['company'];die();
			
			if($company != $_POST['company'] && $gruppo_c != 5)
				Db::getInstance()->executeS("UPDATE address SET company = '".addslashes($_POST['company'])."' WHERE fatturazione = 1 AND active = 1 AND deleted = 0 AND id_customer = ".$_POST['id_customer']);
			
			
			Db::getInstance()->executeS("UPDATE customer SET tax_regime = ".$_POST['tax_regime']." WHERE id_customer = ".$_POST['id_customer']."");
			
			Db::getInstance()->executeS("UPDATE customer SET pec = '".$_POST['pec']."' WHERE id_customer = ".$_POST['id_customer']."");
			
			$ultimo_contatto_ar = explode(' ',$_POST['ultimo_contatto']);
			$ultimo_contatto_ar2 = explode('-',$ultimo_contatto_ar[0]);
			$ultimo_contatto_i = $ultimo_contatto_ar2[2].'-'.$ultimo_contatto_ar2[1].'-'.$ultimo_contatto_ar2[0];
			
			
			Db::getInstance()->executeS("UPDATE customer SET codice_univoco = '".$_POST['codice_univoco']."', ipa = '".$_POST['ipa']."', settore = '".$_POST['settore']."', canale = '".$_POST['canale']."', categoria = '".addslashes($_POST['categoria'])."', ultimo_contatto = '".$ultimo_contatto_i."'   WHERE id_customer = ".$_POST['id_customer']."");
			
			if($_POST['supplier'] == 1) {
				Db::getInstance()->executeS("UPDATE customer SET supplier = 1 WHERE id_customer = ".$_POST['id_customer']."");
			}
			
			else {
				Db::getInstance()->executeS("UPDATE customer SET supplier = 0 WHERE id_customer = ".$_POST['id_customer']."");
			}
			
			
		
		}

		if (Tools::isSubmit('submitDel'.$this->table) OR Tools::isSubmit('delete'.$this->table))
		{
			$deleteForm = '
			<form action="'.htmlentities($_SERVER['REQUEST_URI']).'" method="post">
				<fieldset><legend>'.$this->l('How do you want to delete your customer(s)?').'</legend>
					<!-- '.$this->l('You have two ways to delete a customer, please choose what you want to do.').' -->
					<p>
						<input type="radio" name="deleteMode" value="real" id="deleteMode_real" />
						<label for="deleteMode_real" style="float:none">'.$this->l('I want to delete my customer(s) for real, all data will be removed from the database. A customer with the same e-mail address will be able to register again.').'</label>
					</p>
					<!-- <p>
						<input type="radio" name="deleteMode" value="deleted" id="deleteMode_deleted" />
						<label for="deleteMode_deleted" style="float:none">'.$this->l('I don\'t want my customer(s) to register again. The customer(s) will be removed from this list but all data will be kept in the database.').'</label>
					</p> -->';
			foreach ($_POST as $key => $value)
				if (is_array($value))
					foreach ($value as $val)
						$deleteForm .= '<input type="hidden" name="'.htmlentities($key).'[]" value="'.htmlentities($val).'" />';
				else
					$deleteForm .= '<input type="hidden" name="'.htmlentities($key).'" value="'.htmlentities($value).'" />';
			$deleteForm .= '	<br /><input type="submit" class="button" value="'.$this->l('   Delete   ').'" />
				</fieldset>
			</form>
			<div class="clear">&nbsp;</div>';
		}

		if (Tools::getIsset('submitAdd'.$this->table))
		{
			
		 	$groupList = Tools::getValue('groupBox');

		 	/* Checking fields validity */
			$this->validateRules();
			if (!sizeof($this->_errors))
			{
				$id = (int)(Tools::getValue('id_'.$this->table));
				if (isset($id) AND !empty($id))
				{
					if ($this->tabAccess['edit'] !== '1')
						$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
					else
					{
						$object = new $this->className($id);
						if (Validate::isLoadedObject($object))
						{
							
							//AGGIORNO IL CODICE SPRING
							$codice_spring = Tools::getValue('codice_spring');
							
							Db::getInstance()->execute("UPDATE customer SET codice_spring = '$codice_spring' WHERE id_customer = $id");
							
							//AGGIORNO IL CODICE ESOLVER
							$codice_esolver = Tools::getValue('codice_esolver');
							
							Db::getInstance()->execute("UPDATE customer SET codice_esolver = '$codice_esolver' WHERE id_customer = $id");
							
							//AGGIORNO I NUMERI DI TELEFONO
							$telefono = Tools::getValue('telefono');
							$cellulare = Tools::getValue('cellulare');
							$fax = Tools::getValue('fax');
							$idindirizzo = Tools::getValue('idindirizzo');
			
								Db::getInstance()->execute("UPDATE customer SET id_default_group = ".Tools::getValue('id_default_group')." WHERE id_customer = $id");
			
								Db::getInstance()->execute("UPDATE address SET phone = '$telefono' WHERE address.deleted != 1 AND address.id_address = $idindirizzo AND address.fatturazione = 1");
							
								Db::getInstance()->execute("UPDATE customer SET telefono_principale = '$telefono' WHERE id_customer = $id");
					
								Db::getInstance()->execute("UPDATE address SET phone_mobile = '$cellulare' WHERE address.deleted != 1 AND address.id_address = $idindirizzo AND address.fatturazione = 1");
								
								Db::getInstance()->execute("UPDATE customer SET cellulare_principale = '$cellulare' WHERE id_customer = $id");
					
								Db::getInstance()->execute("UPDATE address SET fax = '$fax' WHERE address.deleted != 1 AND address.id_address = $idindirizzo AND address.fatturazione = 1");
						
									//MANDO LA MAIL PER ABILITAZIONE RIVENDITORI
									
									
									$cliente_id_default_group = Db::getInstance()->getValue("SELECT id_default_group FROM customer WHERE id_customer = $_GET[id_customer]");
									
									$headers_nostri  = 'MIME-Version: 1.0' . "\n";
									$headers_nostri .= 'Content-Type: text/html' ."\n";
									$headers_nostri .= 'From: Ezdirect <info@ezdirect.it>' . "\n";
									$message = "<html>
									<head>
									<title>Abilitazione rivenditore</title>
									</head>
									<body>
									<table style='font-family: Verdana,sans-serif; font-size: 11px; color: #374953; width: 550px;'>
									<tr>
									<td align='left'><a title='Ezdirect' href='http://www.ezdirect.it'><img style='border: none;' src='https://www.ezdirect.it/img/logo.jpg' alt='Ezdirect' /></a></td>
									</tr>
									<tr>
									<td style='background-color: #db6909; color: #fff; font-size: 12px; font-weight: bold; padding: 0.5em 1em;' align='left'>Sei stato abilitato come rivenditore</td>
									</tr>
									<td>
									Grazie per la registrazione. Abbiamo abilitato il tuo account ai prezzi riservati.
<br /><br />
<strong>Avvisi:</strong>
Ti comunichiamo che il minimo ordinabile &egrave; di 150,00 euro iva esclusa
Alcuni prodotti possono essere visualizzati a prezzo netto, (senza sconti, per quantit&agrave; richiedi quotazione)
 <br /><br />
<strong>Per comunicare con Ezdirect e richiedere assistenza:</strong><br /><br />

<strong>Per richiedere supporto nella configurazione di sistemi e/o sui prodotti utilizza i moduli predisposti o chiama 0585821163</strong><br />
Per richiedere assistenza tecnica <strong>postvendita</strong><br /> <a href='http://www.ezdirect.it/contattaci?step=tecnica' target='_blank'>clicca su questo link</a>
 <br /><br />
Per richiedere assistenza amministrativa e contabilit&agrave; <a href='http://www.ezdirect.it/contattaci?step=contabilita' target='_blank'>clicca su questo link</a><br />
Per richiedere informazioni sullo stato degli ordini <a href='http://www.ezdirect.it/contattaci?step=ordini' target='_blank'>clicca su questo link</a><br /><br />
 
Grazie per la collaborazione
 <br /><br />
Staff Ezdirect
									</table>
									</body>
									</html>";									
									
								if($object->id_default_group == 1 && Tools::getValue('id_default_group') == 3) {
								Db::getInstance()->executeS("UPDATE customer SET ex_rivenditore = 0 WHERE id_customer = ".Tools::getValue('id_customer')."");
								mail($object->email, "Ezdirect - Abilitazione rivenditore", $message, $headers_nostri);
								}		
								
								if($object->id_default_group == 5)
								{
									$pagamento_default = Db::getInstance()->getValue('SELECT pagamento FROM customer_amministrazione WHERE id_customer = '.$object->id);
									
									if($pagamento_default == '' || $pagamento_default == 'Bonifico bancario' || $pagamento_default == 'Bonifico Bancario')
									{
										Db::getInstance()->executeS('UPDATE customer_amministrazione SET pagamento = "Bonifico 30 gg. fine mese" WHERE id_customer = '.$object->id);
										
									}
									if($pagamento_default == '')
									{
										Db::getInstance()->executeS('INSERT INTO customer_amministrazione (id_customer, pagamento) VALUES ('.$object->id.', "Bonifico 30 gg. fine mese")');
									}	
																
								}
							$customer_email = strval(Tools::getValue('email'));

							// check if e-mail already used
							if ($customer_email != $object->email)
							{
								$customer = new Customer();
								$customer->getByEmail($customer_email);
								if ($customer->id)
									$this->_errors[] = Tools::displayError('An account already exists for this e-mail address:').' '.$customer_email;
							}
							
							Db::getInstance()->executeS("DELETE FROM customer_group WHERE id_customer = ".Tools::getValue('id_customer')."");
							Db::getInstance()->executeS("INSERT INTO customer_group (id_customer, id_group) VALUES (".Tools::getValue('id_customer').", ".Tools::getValue('id_default_group').")");
							
							
							
							/*

							if (!is_array($groupList) OR sizeof($groupList) == 0)
								$this->_errors[] = Tools::displayError('Customer must be in at least one group.');
							else
								if (!in_array(Tools::getValue('id_default_group'), $groupList))
									$this->_errors[] = Tools::displayError('Default customer group must be selected in group box.');
									
												

							// Updating customer's group
							if (!sizeof($this->_errors))
							{
								$object->cleanGroups();
								if (is_array($groupList) AND sizeof($groupList) > 0)
									$object->addGroups($groupList);
							}
							*/
						}
						else
							$this->_errors[] = Tools::displayError('An error occurred while loading object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
					}
				
				}
				else
				{
					if ($this->tabAccess['add'] === '1')
					{
						$object = new $this->className();
						$this->copyFromPost($object, $this->table);
						if (!$object->add())
							$this->_errors[] = Tools::displayError('An error occurred while creating object.').' <b>'.$this->table.' ('.mysql_error().')</b>';
						elseif (($_POST[$this->identifier] = $object->id /* voluntary */) AND $this->postImage($object->id) AND !sizeof($this->_errors) AND $this->_redirect)
						{
							// Add Associated groups
							$group_list = Tools::getValue('groupBox');
							if (is_array($group_list) && sizeof($group_list) > 0)
								$object->addGroups($group_list, true);
							$parent_id = (int)(Tools::getValue('id_parent', 1));
							// Save and stay on same form
							if (Tools::isSubmit('submitAdd'.$this->table.'AndStay'))
								Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$object->id.'&conf=3&update'.$this->table.'&token='.$this->token);
								
							// Save and back to parent
							if (Tools::isSubmit('submitAdd'.$this->table.'AndBackToParent'))
								Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$parent_id.'&conf=3&token='.$this->token);
							// Default behavior (save and back)
							Tools::redirectAdmin($currentIndex.($parent_id ? '&'.$this->identifier.'='.$object->id : '').'&conf=3&token='.$this->token);
						}
					}
					else
						$this->_errors[] = Tools::displayError('You do not have permission to add here.');
				}
			}
		}
		elseif (Tools::isSubmit('delete'.$this->table) AND $this->tabAccess['delete'] === '1')
		{
			switch (Tools::getValue('deleteMode'))
			{
				case 'real':
					$this->deleted = false;
					Db::getInstance()->executeS("DELETE FROM persone WHERE id_customer = ".Tools::getValue('id_customer')."");
					Discount::deleteByIdCustomer((int)(Tools::getValue('id_customer')));
					break;
				case 'deleted':
					$this->deleted = true;
					break;
				default:
					echo $deleteForm;
					if (isset($_POST['delete'.$this->table]))
						unset($_POST['delete'.$this->table]);
					if (isset($_GET['delete'.$this->table]))
						unset($_GET['delete'.$this->table]);
					break;
			}
		}
		elseif (Tools::isSubmit('submitDel'.$this->table) AND $this->tabAccess['delete'] === '1')
		{
			switch (Tools::getValue('deleteMode'))
			{
				case 'real':
					$this->deleted = false;
					foreach (Tools::getValue('customerBox') as $id_customer)
						Discount::deleteByIdCustomer((int)($id_customer));
					break;
				case 'deleted':
					$this->deleted = true;
					break;
				default:
					echo $deleteForm;
					if (isset($_POST['submitDel'.$this->table]))
						unset($_POST['submitDel'.$this->table]);
					if (isset($_GET['submitDel'.$this->table]))
						unset($_GET['submitDel'.$this->table]);
					break;
			}
		}
		elseif (Tools::isSubmit('submitGuestToCustomer') AND Tools::getValue('id_customer'))
		{
			if ($this->tabAccess['edit'] === '1')
			{
				$customer = new Customer((int)Tools::getValue('id_customer'));
				if (!Validate::isLoadedObject($customer))
					$this->_errors[] = Tools::displayError('This customer does not exist.');
				if (Customer::customerExists($customer->email))
					$this->_errors[] = Tools::displayError('This customer already exist as non-guest.');
				elseif ($customer->transformToCustomer(Tools::getValue('id_lang', Configuration::get('PS_LANG_DEFAULT'))))
					Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$customer->id.'&conf=3&token='.$this->token);
				else
					$this->_errors[] = Tools::displayError('An error occurred while updating customer.');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
		}elseif (Tools::isSubmit('changeNewsletterVal') AND Tools::getValue('id_customer'))
		{
			$id_customer = (int)Tools::getValue('id_customer');
			$customer = new Customer($id_customer);
			if (!Validate::isLoadedObject($customer))
				$this->_errors[] = Tools::displayError('An error occurred while updating customer.');
			$update = Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'customer` SET newsletter = '.($customer->newsletter ? 0 : 1).' WHERE `id_customer` = '.(int)($customer->id));
			if (!$update)
				$this->_errors[] = Tools::displayError('An error occurred while updating customer.');
			Tools::redirectAdmin($currentIndex.'&token='.$this->token);

		}elseif (Tools::isSubmit('changeOptinVal') AND Tools::getValue('id_customer'))
		{
			$id_customer = (int)Tools::getValue('id_customer');
			$customer = new Customer($id_customer);
			if (!Validate::isLoadedObject($customer))
				$this->_errors[] = Tools::displayError('An error occurred while updating customer.');
			$update = Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'customer` SET optin = '.($customer->optin ? 0 : 1).' WHERE `id_customer` = '.(int)($customer->id));
			if (!$update)
				$this->_errors[] = Tools::displayError('An error occurred while updating customer.');
			Tools::redirectAdmin($currentIndex.'&token='.$this->token);
		}
		
	
					
		return parent::postProcess();
	}

	public function viewcustomer()
	{
		$db1 = Db::getInstance();
		$link_glob = $db1->connect(_DB_SERVER_, _DB_USER_, _DB_PASSWD_);
		
		ini_set('meemory_limit', '96M');
		ini_set('post_max_size', '64M');
		ini_set('upload_max_filesize', '64M');
		global $currentIndex, $cookie, $link;
		$tokenAddresses = Tools::getAdminToken('AdminAddresses'.(int)(Tab::getIdFromClassName('AdminAddresses')).(int)($cookie->id_employee));
		$irow = 0;
		$configurations = Configuration::getMultiple(array('PS_LANG_DEFAULT', 'PS_CURRENCY_DEFAULT'));
		$defaultLanguage = (int)($configurations['PS_LANG_DEFAULT']);
		$defaultCurrency = (int)($configurations['PS_CURRENCY_DEFAULT']);
		if (!($customer = $this->loadObject()))
			return;
		$customerStats = $customer->getStats();
		$addresses_fatt = $customer->getAddressesFatturazione($defaultLanguage);
		$addresses_cons = $customer->getAddressesConsegna($defaultLanguage);
		$products = $customer->getBoughtProducts();
		$discounts = Discount::getCustomerDiscounts($defaultLanguage, (int)$customer->id, false, false);
		$orders = Order::getCustomerOrders((int)$customer->id, true);
		$carts = Cart::getCustomerCarts((int)$customer->id);
		$groups = $customer->getGroups();
		$messages = CustomerThread::getCustomerMessages((int)$customer->id);
		$referrers = Referrer::getReferrers((int)$customer->id);
		
		//link utili
		$array_utils = array();
		$array_utils[] = '<option name="" value="-- Seleziona un link utile --" selected="selected">-- Seleziona un link utile --</option>';
		$array_utils[] = '<option name="Preventivi" value="https://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini">Preventivi</option>';
		$array_utils[] = '<option name="Ti richiamiamo noi" value="https://www.ezdirect.it/ti-richiamiamo-noi">Ti richiamiamo noi</option>';
		$array_utils[] = '<option name="Il tuo account" value="https://www.ezdirect.it/autenticazione">Il tuo account</option>';
		$array_utils[] = '<option name="Blog" value="http://www.ezdirect.it/blog/">Blog</option>';
		$array_utils[] = '<option name="Offerte speciali" value="http://www.ezdirect.it/offerte-speciali">Offerte speciali</option>';
		$array_utils[] = '<option name="Assistenza tecnica postvendita" value="http://www.ezdirect.it/contattaci?step=tecnica">Assistenza tecnica postvendita</option>';
		$array_utils[] = '<option name="Assistenza ordini" value="http://www.ezdirect.it/contattaci?step=assistenza-ordini">Assistenza ordini</option>';
		$array_utils[] = '<option name="Assistenza contabilita" value="http://www.ezdirect.it/contattaci?step=contabilita">Assistenza contabilità</option>';
		$array_utils[] = '<option name="Rivenditori" value="https://www.ezdirect.it/autenticazione?cliente=rivenditore">Rivenditori</option>';
		$array_utils[] = '<option name="Condizioni di vendita" value="http://www.ezdirect.it/guide/3-termini-e-condizioni-acquisto-on-line-ezdirect">Condizioni di vendita</option>';
		$array_utils[] = '<option name="Cashback Jabra" value="http://www.ezdirect.it/blog/2017/01/acqistare-al-miglior-prezzo-jabra-rimborso-su-prodotto-comprato/">Cashback Jabra</option>';


		$cmss = Db::getInstance()->executeS('SELECT * FROM cms_lang WHERE id_lang = 5');
		foreach ($cmss as $cms)
		{
			$array_utils[] = '<option name="'.$cms['meta_title'].'" value="http://www.ezdirect.it/guide/'.$cms['id_cms'].'-'.$cms['link_rewrite'].'.php">'.$cms['meta_title'].'</option>';
		}
		
		sort($array_utils, SORT_STRING);
		
		$utils_options = '';
		
		foreach($array_utils as $util)
			$utils_options .= $util;
	
		//
		
		if(Tools::getIsset('cancellanota') && Tools::getValue('cancella_nota') == 'y') {
			Db::getInstance()->executeS("DELETE FROM customer_note WHERE id_note = ".Tools::getValue('id_note')."");
		}
		
		if ($totalCustomer = Db::getInstance()->getValue('SELECT SUM(total_paid_real) FROM '._DB_PREFIX_.'orders WHERE id_customer = '.$customer->id.' AND valid = 1'))
		{
			Db::getInstance()->getValue('SELECT SQL_CALC_FOUND_ROWS COUNT(*) FROM '._DB_PREFIX_.'orders o WHERE o.id_order NOT IN (SELECT id_order FROM order_history WHERE id_order_state = 6  OR id_order_state = 35) AND (o.valid = 1 OR (o.valid = 0 AND o.module = "bankwire") GROUP BY id_customer HAVING SUM(total_paid_real) > '.$totalCustomer);
			$countBetterCustomers = (int)Db::getInstance()->getValue('SELECT FOUND_ROWS()') + 1;
		}
		else
			$countBetterCustomers = '-';
			
			if($customer->company != '') { $azienda = $customer->company." -"; } else { $azienda = ""; }
			if($customer->vat_number != '') { $piva = "- <strong>Partita IVA</strong>:". $customer->vat_number.""; } else { $piva = ""; }
			
			
			
			$nome = $customer->firstname;
			$cognome = $customer->lastname;
			$cf = $customer->tax_code;
			$is_supplier = Db::getInstance()->getValue("SELECT supplier FROM customer WHERE id_customer = ".Tools::getValue('id_customer')."");
			
			$preventivi = Db::getInstance()->ExecuteS("SELECT * FROM form_prevendita_thread WHERE id_customer = ".$customer->id." ORDER BY id_thread DESC");
			$ticket = Db::getInstance()->ExecuteS("SELECT * FROM customer_thread WHERE id_contact != 7 AND id_customer = ".$customer->id." ORDER BY id_customer_thread DESC");
			$messaggi = Db::getInstance()->ExecuteS("SELECT * FROM customer_thread WHERE id_contact = 7 AND id_customer = ".$customer->id." ORDER BY id_customer_thread DESC");
			$azioni = Db::getInstance()->ExecuteS("SELECT * FROM action_thread WHERE id_customer = ".$customer->id." ORDER BY id_action DESC");
			$fatture = Db::getInstance()->ExecuteS("SELECT id_riga FROM fattura WHERE id_customer = ".$customer->id." GROUP BY id_fattura");
			$persone = Db::getInstance()->ExecuteS("SELECT * FROM persone WHERE id_customer = ".$customer->id." GROUP BY id_persona");
			$contratti = Db::getInstance()->ExecuteS("SELECT * FROM contratto_assistenza WHERE id_customer = ".$customer->id." GROUP BY id_contratto");
			$contratti_attivi = Db::getInstance()->getValue("SELECT count(*) FROM contratto_assistenza WHERE id_customer = ".$customer->id." AND status = 0");
			
			$bdl_aperti = Db::getInstance()->ExecuteS("SELECT * FROM bdl a WHERE id_customer = ".$customer->id." AND a.id_bdl > 384 AND nessun_addebito = 0 AND ( (a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 0) OR (a.rif_ordine = 0 AND a.invio_controllo = 0) ) GROUP BY a.id_bdl ORDER BY a.id_bdl");
			
			$bdl = Db::getInstance()->ExecuteS("SELECT * FROM bdl WHERE id_customer = ".$customer->id." GROUP BY id_bdl");
			
			$azioni_cliente = array_merge($preventivi, $messaggi);
			
			$preventiviaperti = Db::getInstance()->ExecuteS("SELECT * FROM form_prevendita_thread WHERE id_customer = ".$customer->id." AND status != 'closed' ORDER BY id_thread DESC");
			$messaggiaperti = Db::getInstance()->ExecuteS("SELECT ct.id_customer_thread FROM customer_message cm JOIN (SELECT * FROM customer_thread WHERE id_customer = ".$customer->id.") ct ON cm.id_customer_thread = ct.id_customer_thread WHERE ct.id_contact = 7 AND cm.id_employee = 0 AND (status = 'open' OR status = 'pending1') GROUP BY ct.id_customer_thread");
			
			$ticketaperti = Db::getInstance()->ExecuteS("SELECT * FROM customer_thread WHERE id_contact != 7 AND id_customer = ".$customer->id." AND status != 'closed' ORDER BY id_customer_thread DESC");
			$azioniaperti = Db::getInstance()->ExecuteS("SELECT * FROM action_thread WHERE id_customer = ".$customer->id." AND status != 'closed' ORDER BY id_action DESC");
			
			$ordini_totali = Db::getInstance()->getValue('SELECT SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) tot FROM order_detail JOIN orders ON orders.id_order = order_detail.id_order LEFT JOIN order_history oh ON oh.id_order_state =6 AND oh.id_order = orders.id_order WHERE oh.id_order_state IS NULL  AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment"))) AND orders.id_customer = '.$customer->id.'');
			
			$ordini_ultimi_12_mesi = Db::getInstance()->getValue('SELECT SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) tot FROM order_detail JOIN orders ON orders.id_order = order_detail.id_order LEFT JOIN order_history oh ON oh.id_order_state =6 AND oh.id_order = orders.id_order WHERE oh.id_order_state IS NULL  AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment"))) AND orders.id_customer = '.$customer->id.' AND orders.date_add BETWEEN date_sub(curdate(),interval 365 day) AND  date_sub(curdate(),interval 0 day)');
			
			$ordini_ultimi_6_mesi = Db::getInstance()->getValue('SELECT SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) tot FROM order_detail JOIN orders ON orders.id_order = order_detail.id_order LEFT JOIN order_history oh ON oh.id_order_state =6 AND oh.id_order = orders.id_order WHERE oh.id_order_state IS NULL  AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment"))) AND orders.id_customer = '.$customer->id.' AND orders.date_add BETWEEN date_sub(curdate(),interval 180 day) AND  date_sub(curdate(),interval 0 day)');
			
			
			$data_ultimo_acquisto = Db::getInstance()->getValue("SELECT data_fattura FROM fattura WHERE id_customer = ".$customer->id." ORDER BY data_fattura DESC");
			
			$agente_c = Db::getInstance()->getValue('SELECT agente FROM customer_amministrazione WHERE id_customer = '.$customer->id);
			
			if($cookie->profile == 7)
			{
				if($agente_c != $cookie->id_employee)
					die('Non hai i permessi per visualizzare questo cliente');
			}
			
			if(!$agente_c || $agente_c == 0) 
			{
				$agente_c = Db::getInstance()->getValue('SELECT id_employee FROM (SELECT * FROM form_prevendita_thread WHERE id_customer = '.$customer->id.') au ORDER BY id_thread DESC');
				if(!$agente_c || $agente_c == 0) 
				{	
					if($customer->id_default_group == 3 || $customer->id_default_group == 5 || $customer->id_default_group == 15 || $customer->id_default_group == 8 || $customer->id_default_group == 19)
					{
						$agente_c = 1;
					}
					else
						$agente_c = Db::getInstance()->getValue('SELECT id_employee FROM (SELECT * FROM cart WHERE id_customer = '.$customer->id.') au ORDER BY id_cart DESC');
				}	
			}
				
			
			$tecnico_c = Db::getInstance()->getValue('SELECT tecnico FROM customer_amministrazione WHERE id_customer = '.$customer->id);
			
			if(!$tecnico_c || $tecnico_c == 0) 
			{
				$tecnico_c = Db::getInstance()->getValue('SELECT id_employee FROM (SELECT * FROM customer_thread WHERE id_customer = '.$customer->id.') au ORDER BY id_customer_thread DESC');
				
				if(!$tecnico_c || $tecnico_c == 0) 
					$tecnico_c = Db::getInstance()->getValue('SELECT action_to FROM (SELECT * FROM action_thread WHERE id_customer = '.$customer->id.') au ORDER BY id_action DESC');
			}
			
			$referente_c = Db::getInstance()->getValue('SELECT referente FROM customer WHERE id_customer = '.$customer->id);
			
			if(!$referente_c || $referente_c == 0) 
			{
				$referente_c = Db::getInstance()->getValue('SELECT id_persona FROM persone WHERE id_customer = '.$customer->id.' AND firstname = "'.$customer->firstname.'" AND lastname = "'.$customer->lastname.'"');
				Db::getInstance()->executeS('UPDATE customer SET referente = '.$referente_c.' WHERE id_customer = '.$customer->id.'');
			}
			
			$azioni_cliente_aperte = array_merge($preventiviaperti, $messaggiaperti);
			$codice_spring = Db::getInstance()->getValue("SELECT codice_spring FROM customer WHERE id_customer = ".Tools::getValue('id_customer')."");
			
			$filelist_count = array();
			$cartella_documenti = Db::getInstance()->getValue("SELECT cartella_documenti FROM customer WHERE id_customer = ".Tools::getValue('id_customer')."");

			if(empty($cartella_documenti)) {
				$dirs_count = 0;
				$docs_count = 0;
			}
			else {
				$directory_base = '../documenti-clienti/'.$cartella_documenti;
				
				$entire_dir = $directory_base;
				
				if(substr($entire_dir,0,12) != '../documenti') {
					echo "ERRORE"; die();
					
				}
					
				if ($handle = opendir($entire_dir)) {
					
					while ($dir = readdir($handle)) {
						
						if(is_dir($entire_dir.'/'.$dir) && $dir != "." && $dir != "..") { $dirlist_count[]=$dir; }
						if(is_file($entire_dir.'/'.$dir)) { $filelist_count[]=$dir; }
						
					}
				}
				
				$dirs_count = count($dirlist_count);
				$docs_count = count($filelist_count);
			}
			echo "<style type='text/css'>
			#hint{
				cursor:pointer;
			}
			div.tooltip{
				margin:8px;
				text-align:left;
				padding:8px;
				border:1px solid blue;
				background-color:#ffffff;
				position: absolute;
				z-index: 2;
			}
			</style>";
			
			echo '<script type="text/javascript" src="../js/jquery/jquery.tablesorter.min.js"></script> 
			<script type="text/javascript" src="../js/jquery/jquery.tablesorter.widgets.js"></script> 
			<script type="text/javascript" src="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.js"></script> 
			<link rel="stylesheet" type="text/css" href="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.css" />';

			echo "<script type='text/javascript'>
 
$(document).ready(function() {
	var changeTooltipPosition = function(event) {
	  var tooltipX = event.pageX - 8;
	  var tooltipY = event.pageY + 8;
	  $('div.tooltip').css({top: tooltipY, left: tooltipX});
	};
	var tooltip_content = '';

	var showTooltip = function(event) {
	  $('div.tooltip').remove();
	  $.ajax({
	type: 'POST',
	data: 'id='+$(this).attr('id')+'&rel='+$(this).attr('rel'),
	async: false,
	url: 'customer_message_preview.php',
	success: function(resp)
		{
			tooltip_content = resp;						
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			tooltip_content = '';
			alert('ERROR');					
		}
								
	});
	
	
	 
	  $(\"<div class='tooltip'>\"+tooltip_content+\"</div>\")
            .appendTo('body');
	  changeTooltipPosition(event);
	};
	
	var hideTooltip = function() {
	   $('div.tooltip').remove();
	};
 
	$('.actions_ticket_tr').bind({
	   mousemove : changeTooltipPosition,
	   mouseenter : showTooltip,
	   mouseleave: hideTooltip
	});



	
	var showTooltip2 = function(event) {
	  $('div.tooltip').remove();
	 
	  $(\"<div class='tooltip'>ATTENZIONE: se vuoi utilizzare un carrello compilato, sceglilo dal men&ugrave; a tendina di fianco</div>\")
            .appendTo('body');
	  changeTooltipPosition(event);
	};
	
	$('.nuovo_preventivo').bind({
	   mousemove : changeTooltipPosition,
	   mouseenter : showTooltip2,
	   mouseleave: hideTooltip
	});
	
	
	
	
	
});

	function closethis(tipo,riferimento,link)
	{
		$.ajax({
		  url:'ajax.php?closethis=y',
		  type: 'POST',
		  data: { tipo: tipo, id: riferimento
		  },
		  success:function(resp){
			if(resp == 'ok')
			{	
				alert('Azione chiusa con successo');
				window.location.href = link;
			}
			else
				alert('Errore durante la chiusura dell\'azione');
		  },
		  error: function(xhr,stato,errori){
			 alert('ERRORE: '+xhr.status);
		  }
		});

		
	}	
 
</script>
 
</script>";
			$ex_rivenditore = Db::getInstance()->getValue("SELECT ex_rivenditore FROM customer WHERE id_customer = ".$customer->id."");
			$codice_esolver = Db::getInstance()->getValue("SELECT codice_esolver FROM customer WHERE id_customer = ".$customer->id."");
			
			$telefonate_inviate = Db::getInstance()->executeS('SELECT * FROM telefonate WHERE id_customer = '.$customer->id.' AND calltype = "Outbound" ORDER BY datetime DESC');
			$telefonate_ricevute = Db::getInstance()->executeS('SELECT * FROM telefonate WHERE id_customer = '.$customer->id.' AND calltype = "Inbound" ORDER BY datetime DESC');
			
			echo '<script type="text/javascript" src="autosize.js"></script>
				<script type="text/javascript">
					function add_nota_privata() {
						var possible = "0123456789";
						var randomid = "";
						for( var i=0; i < 11; i++ ) {
							randomid += possible.charAt(Math.floor(Math.random() * possible.length));
						}
								
						$("<tr id=\'tr_note_"+randomid+"\'><td><textarea class=\'textarea_note\' name=\'note_nota[]\' onkeyup=\'auto_grow(this)\' id=\'note_nota[]\' style=\'width:540px;height:16px\'></textarea><input type=\'hidden\' name=\'note_nuova[]\' value=\'1\' /></td><td><input type=\'text\' readonly=\'readonly\' name=\'note_id_employee[]\' value=\''.Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM employee WHERE id_employee = '.$cookie->id_employee).'\' /></td><td><input type=\'text\' readonly=\'readonly\' name=\'note_date_add[]\' value=\''.date('d/m/Y').'\' /></td><td><a href=\'javascript:void(0)\' onclick=\'togli_nota("+randomid+");\'><img src=\'../img/admin/delete.gif\' alt=\'Cancella\' title=\'Cancella\' style=\'cursor:pointer\' /></a></td></tr>").appendTo("#tableNoteBody");
						
						$(".textarea_note").each(function () {
							this.style.height = ((this.scrollHeight)-4)+"px";
						});
						
						$(".textarea_note").on("change keyup keydown paste cut", "textarea", function () {
							$(this).height(0).height(this.scrollHeight);
						}).find("textarea").change();
						
						autosize($(".textarea_note"));

					}
						
					function togli_nota(id) {
						document.getElementById(\'tr_note_\'+id).innerHTML = "";
					}
						
						
					function cancella_nota(id) {
						$.ajax({
							  url:"ajax.php?cancellanota_customer=y",
							  type: "POST",
							  data: { 
							  cancella_nota: \'y\',
							  id_note: id
							  },
							  success:function(){
								alert("Nota cancellata con successo"); 
							  },
							  error: function(xhr,stato,errori){
								 alert("Errore nella cancellazione:"+xhr.status);
							  }
						});
					
					}
					
					function cancella_nota_attivita(id) {
						$.ajax({
							  url:"ajax.php?cancellanota_attivita=y",
							  type: "POST",
							  data: { 
							  cancella_nota: \'y\',
							  id_note: id
							  },
							  success:function(){
								alert("Nota cancellata con successo"); 
							  },
							  error: function(xhr,stato,errori){
								 alert("Errore nella cancellazione:"+xhr.status);
							  }
						});
					
					}
					
					function auto_grow(element) {
					}

					
					
					</script>';
			
			echo '
			<script type="text/javascript" src="yetii.js"></script>
			<br />
			<div id="tab-container-1">
			
			<ul id="tab-container-2-nav" '.($cookie->profile == 7 ? 'style="margin-top:-26px"' : '').'>
			
			
			'.($cookie->profile == 7 ? '' : '<li '.($_GET['tab-container-1'] == 6 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=6" '.($cookie->id_employee == 1 && (Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 4) ? '' : '').'><strong>Ticket ('.(sizeof($ticketaperti) > 0 ? '<span style="color:red">'.sizeof($ticketaperti).'</span>' : sizeof($ticketaperti)).' / '.sizeof($ticket).')</strong></a></li>
			<li '.($_GET['tab-container-1'] == 7 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=7" '.($cookie->id_employee == 1 && (Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 4) ? '' : '').'><strong>Azioni cliente ('.(sizeof($azioni_cliente_aperte) > 0 ? '<span style="color:red">'.sizeof($azioni_cliente_aperte).'</span>' : sizeof($azioni_cliente_aperte)).' / '.sizeof($azioni_cliente).')</strong></a></li>
			
			
			<li '.($_GET['tab-container-1'] == 10 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=10" '.($cookie->id_employee == 1 && (Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 4) ? '' : '').'><strong>TO DO ('.(sizeof($azioniaperti) > 0 ? '<span style="color:red">'.sizeof($azioniaperti).'</span>' : sizeof($azioniaperti)).' / '.sizeof($azioni).')</strong></a></li>
			
			<li '.($_GET['tab-container-1'] == 8 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=8" '.($cookie->id_employee == 1 && (Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 4) ? '' : '').'><strong>Stat</strong></a></li>
			
			<li '.($_GET['tab-container-1'] == 13 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=13" '.($cookie->id_employee == 1 && (Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 4) ? '' : '').'><strong>Contratti ('.($contratti_attivi > 0 ? '<span style="color:red">'.$contratti_attivi.'</span>' : '0').' / '.sizeof($contratti).')</strong></a></li>
			
			<li '.($_GET['tab-container-1'] == 14 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=14" '.($cookie->id_employee == 1 && (Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 4) ? '' : '').'><strong>BDL ('.(sizeof($bdl_aperti) > 0 ? '<span style="color:red">'.sizeof($bdl_aperti).'</span>' : '0').' / '.sizeof($bdl).')</strong></a></li>
			
			<li '.($_GET['tab-container-1'] == 15 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=15" '.($cookie->id_employee == 1 && (Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 4) ? '' : '').'><strong>Mail</strong></a></li>
			
			<li '.($_GET['tab-container-1'] == 16 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=16" '.($cookie->id_employee == 1 && (Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 4) ? '' : '').'><strong>Telefonate</strong> ('.count($telefonate_ricevute).'-'.count($telefonate_inviate).')</a></li>
			
			<!-- break -->').'
			
			<li '.($_GET['tab-container-1'] == 1 || !isset($_GET['tab-container-1']) ? 'class="activeli" ' : '').' style="'.($cookie->profile == 7 ? 'flex-basis:3%' : '').'"><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=1" '.($cookie->id_employee == 1 && (Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 4) ? '' : '').'><strong>Anagrafica</strong></a></li>
			<li '.($_GET['tab-container-1'] == 2 ? 'class="activeli" ' : '').' '.($cookie->profile == 7 ? 'style="flex-basis:3%"' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=2" '.($cookie->id_employee == 1 && (Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 4) ? '' : '').'><strong>Indirizzi</strong></a></li>
			<li '.($_GET['tab-container-1'] == 11 ? 'class="activeli" ' : '').' '.($customer->is_company == 1 ? '' : 'style="display:none"').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=11" '.($cookie->id_employee == 1 && (Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 4) ? '' : '').'><strong>Persone ('.sizeof($persone).')</strong></a></li>
			<li '.($_GET['tab-container-1'] == 3 ? 'class="activeli" ' : '').' '.($cookie->profile == 7 ? 'style="flex-basis:3%"' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=3" '.($cookie->id_employee == 1 && (Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 4) ? '' : '').'><strong>Amministrazione</strong></a></li>
			<li '.($_GET['tab-container-1'] == 4 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4" '.($cookie->id_employee == 1 && (Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 4) ? '' : '').'><strong>Ordini	('.sizeof($orders).') Carrelli ('.sizeof($carts).')</strong></a></li>
			'.($cookie->profile == 7 ? 
			
			'<li '.($_GET['tab-container-1'] == 6 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=6" '.($cookie->id_employee == 1 && (Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 4) ? '' : '').'><strong>Ticket ('.(sizeof($ticketaperti) > 0 ? '<span style="color:red">'.sizeof($ticketaperti).'</span>' : sizeof($ticketaperti)).' / '.sizeof($ticket).')</strong></a></li>
			<li '.($_GET['tab-container-1'] == 7 ? 'class="activeli" ' : '').' style="flex-basis:12%"><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=7" '.($cookie->id_employee == 1 && (Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 4) ? '' : '').'><strong>Azioni cliente ('.(sizeof($azioni_cliente_aperte) > 0 ? '<span style="color:red">'.sizeof($azioni_cliente_aperte).'</span>' : sizeof($azioni_cliente_aperte)).' / '.sizeof($azioni_cliente).')</strong></a></li><li '.($_GET['tab-container-1'] == 10 ? 'class="activeli" ' : '').' style="flex-basis:12%"><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=10" '.($cookie->id_employee == 1 && (Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 4) ? '' : '').'><strong>TO DO ('.(sizeof($azioniaperti) > 0 ? '<span style="color:red">'.sizeof($azioniaperti).'</span>' : sizeof($azioniaperti)).' / '.sizeof($azioni).')</strong></a></li>' 
			
			: 
			
			'<li '.($_GET['tab-container-1'] == 5 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=5" '.($cookie->id_employee == 1 && (Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 4) ? '' : '').'><strong>Fatture ('.sizeof($fatture).')</strong></a></li>
			<li '.($_GET['tab-container-1'] == 17 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=17"><strong>Ezcloud</strong></a></li>
			<li '.($_GET['tab-container-1'] == 12 ? 'class="activeli" ' : '').'><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=12" '.($cookie->id_employee == 1 && (Tools::getIsset('tab-container-1') && Tools::getValue('tab-container-1') == 4) ? '' : '').'><strong>Documenti ('.$dirs_count."-".$docs_count.')</strong></a></li>').'
			
			
			</ul>
			
			
			
			<ul id="tab-container-1-nav" style="list-style-type:none; display:none">
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#anagrafica"><strong>Anagrafica</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#addresses"><strong>Indirizzi</strong></a></li>
			'.($customer->is_company == 1 ? '<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#persone-customer"><strong>Persone</strong></a></li>' : '').'
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#amministrazione"><strong>Amministrazione</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#orders-customer"><strong>Ordini</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#fatture-customer"><strong>Fatture</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#ezcloud-customer"><strong>Ezcloud</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#messages-customer"><strong>Ticket</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#preventivi-customer"><strong>Preventivi</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#stats"><strong>Stats</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#azioni-customer"><strong>Azioni</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#documenti-customer"><strong>Docs</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#contratti-customer"><strong>Contratti</strong></a></li>
			<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#bdl-customer"><strong>BDL</strong></a></li>
			
			</ul>
			<script type="text/javascript" src="https://www.ezdirect.it/js/sweetalert2.js"></script>
				<link rel="stylesheet" href="https://www.ezdirect.it/css/sweetalert2.css" />
			<script type="text/javascript">
			

				$("body").on("click", function (event) {
					 if(event.target.tagName.toUpperCase() == "BODY"  || event.target.id == "auto_marca"   || event.target.id == "auto_serie" || event.target.id == "auto_categoria" || event.target.id == "auto_fornitore" || event.target.id == "veditutti" || event.target.id == "prodotti_online"  || event.target.id == "prodotti_offline" || event.target.id == "prodotti_old"  || event.target.id == "prodotti_disponibili" || event.target.id == "product_autocomplete_input")
					 {
						event.stopPropagation();
						
					 }
					 else
					 {
						$(\'#prodlist\').hide();
						$(\'div.autocomplete_list\').hide();
					 }
				});	
			</script>';
			
			$provincia = Db::getInstance()->getValue('SELECT id_state FROM address WHERE fatturazione = 1 AND deleted = 0 AND active = 1 AND id_customer = '.$customer->id);	
			$nazione = Db::getInstance()->getValue('SELECT id_country FROM address WHERE fatturazione = 1 AND deleted = 0 AND active = 1 AND id_customer = '.$customer->id);	
			
			$acquisti_ultimo_anno = Db::getInstance()->getValue('SELECT count(id_order) FROM orders WHERE id_customer = '.$customer->id.' AND date_add BETWEEN date_sub(curdate(),interval 365 day) AND date_sub(curdate(),interval -1 day)');
			
			$preventivi_ultimo_anno = Db::getInstance()->getValue('SELECT count(id_cart) FROM cart WHERE id_customer = '.$customer->id.' AND date_add BETWEEN date_sub(curdate(),interval 365 day) AND date_sub(curdate(),interval -1 day)');
			
			$ric_prev_ultimo_anno = Db::getInstance()->getValue('SELECT count(id_thread) FROM form_prevendita_thread WHERE id_customer = '.$customer->id.' AND date_add BETWEEN date_sub(curdate(),interval 365 day) AND date_sub(curdate(),interval -1 day)');
			
			$todo_ultimo_anno = Db::getInstance()->getValue('SELECT count(id_action) FROM action_thread WHERE id_customer = '.$customer->id.' AND date_add BETWEEN date_sub(curdate(),interval 365 day) AND date_sub(curdate(),interval -1 day)');
			
			$ticket_ultimo_anno = Db::getInstance()->getValue('SELECT count(id_customer_thread) FROM customer_thread WHERE id_customer = '.$customer->id.' AND date_add BETWEEN date_sub(curdate(),interval 365 day) AND date_sub(curdate(),interval -1 day)');
			
			$blacklist = Db::getInstance()->getValue('SELECT blacklist FROM customer_amministrazione WHERE id_customer = '.Tools::getValue('id_customer'));
			
			if($blacklist == 1)
			{
				echo '<script type="text/javascript">
					$(document).ready(function(){
						 
							swal({
							  type: "error",
							  title: "ATTENZIONE",
							  text: "Questo cliente è in blacklist!",
							  footer: ""
							})
					});
				</script>';
			}
			$profilo = '';
			
			if($acquisti_ultimo_anno > 3)
				$profilo = 'Fedele';
			else if ($acquisti_ultimo_anno > 0 && $acquisti_ultimo_anno < 3)
				$profilo = 'Attivo';
			else
			{
				if($preventivi_ultimo_anno > 0)
					$profilo = 'Prospect';
				else
				{
					if($ric_prev_ultimo_anno > 0 || $todo_ultimo_anno > 0 || $ticket_ultimo_anno > 0)
						$profilo = 'Lead';
					else
						$profilo = 'Inattivo';
				}
			}
			
			$paese_cliente = Db::getInstance()->getValue('SELECT iso_code FROM country WHERE id_country = '.Db::getInstance()->getValue('SELECT id_country FROM address WHERE fatturazione = 1 AND deleted = 0 AND active = 1 AND id_customer = '.$customer->id));
			
			$fido = Db::getInstance()->getValue('SELECT fido FROM customer_amministrazione WHERE id_customer = '.$customer->id);
			$data_fido = Db::getInstance()->getValue('SELECT data_fido FROM customer_amministrazione WHERE id_customer = '.$customer->id);
			
			$fido = str_replace('.','',$fido);
			$fido = (float)(str_replace(",",".",$fido));
			
			$somma_fido = Db::getInstance()->getValue('SELECT SUM(total_paid_real-acconto) FROM orders WHERE id_customer = '.$customer->id.' AND (payment = "Bonifico 30 gg. fine mese" OR payment = "Bonifico 60 gg. fine mese" OR payment = "Bonifico 90 gg. fine mese" OR payment = "Bonifico 30 gg. 15 mese successivo"  OR payment = "R.B. 30 GG. D.F. F.M." OR payment = "R.B. 60 GG. D.F. F.M." OR payment = "R.B. 90 GG. D.F. F.M." OR payment = "R.B. 30 GG. 5 mese successivo" OR payment = "R.B. 30 GG. 10 mese successivo" OR payment = "R.B. 60 GG. 5 mese successivo" OR payment = "R.B. 60 GG. 5 mese successivo" OR payment LIKE "%saldo%") AND id_order NOT IN (SELECT id_order FROM order_history WHERE id_order_state = 18 OR id_order_state = 32) AND date_add > "'.$data_fido.'"');
			
			
			$somma_fido_32 = Db::getInstance()->getValue('SELECT SUM(total_paid_real-acconto) FROM orders o JOIN (SELECT id_order_state, id_order FROM order_history WHERE id_order_state = 32) oh ON o.id_order = oh.id_order WHERE id_customer = '.$customer->id.' AND (payment = "Bonifico 30 gg. fine mese" OR payment = "Bonifico 60 gg. fine mese" OR payment = "Bonifico 90 gg. fine mese" OR payment = "Bonifico 30 gg. 15 mese successivo" OR payment = "R.B. 30 GG. D.F. F.M." OR payment = "R.B. 60 GG. D.F. F.M." OR payment = "R.B. 90 GG. D.F. F.M." OR payment = "R.B. 30 GG. 5 mese successivo" OR payment = "R.B. 30 GG. 10 mese successivo" OR payment = "R.B. 60 GG. 5 mese successivo" OR payment = "R.B. 60 GG. 10 mese successivo" OR payment LIKE "%saldo%") AND id_order NOT IN (SELECT id_order FROM order_history WHERE id_order_state = 18 OR id_order_state = 32)  AND date_add > "'.$data_fido.'"');
			$crediti_fido = $fido - $somma_fido + $somma_fido_32;
			
			if($blacklist == 1)
				$crediti_fido = 0;
			
			switch($provincia)
			{
				case 124: case 126: case 129: case 133: case 135: case 136: case 137: case 138: case 139: case 149: case 151: case 153: case 156: case 159: case 161: case 162: case 164: case 167: case 170: case 172: case 175: case 180: case 181: case 182: case 184: case 189: case 191: case 192: case 196: case 199: case 203: case 205: case 207: case 209: case 212: case 215: case 219: case 221: case 222: case 223: case 224: case 225: case 226: case 227: case 228: case 229: case 231: $zona_cliente = 'Nord'; break;

				case 125: case 127: case 128: case 155: case 157: case 160: case 163: case 168: case 171: case 173: case 174: case 176: case 193: case 194: case 197: case 198: case 201: case 206: case 208: case 213: case 218: case 232: $zona_cliente = 'Centro'; break;

				case 123: case 130: case 131: case 132: case 134: case 140: case 141: case 142: case 143: case 144: case 145: case 146: case 147: case 148: case 150: case 152: case 154: case 158: case 165: case 166: case 169: case 177: case 178: case 179: case 183: case 185: case 186: case 187: case 188: case 190: case 195: case 200: case 202: case 204: case 210: case 211: case 214: case 216: case 217: case 220: case 230: $zona_cliente = 'Sud'; break;

				default: $zona_cliente = 'Estero / '.$paese_cliente; break;
			}
			
			echo '
			<div id="dati-cliente">
				<table>
				<tr><td colspan="2" style="width:280px"><strong>Cliente<br /></strong> '.($customer->is_company == 1 ? $customer->company : $customer->firstname.' '.$customer->lastname).' </td>
				<td style="width:200px"><table><tr><td style="width:70px; text-align:left"><strong>Spring</strong></td><td style="width:50px; text-align:left"><strong>/ CRM</strong></td><td style="width:60px; text-align:left"><strong>/ eSolv.</strong></td></tr><tr><td>'.($codice_spring == '' ? '--' : $codice_spring).'</td><td> / '.$customer->id.'</td><td>/ '.($codice_esolver == '0' ? '--' : $codice_esolver).'</tr></table></td>
				<td style="width:140px"><strong>Profilo</strong><br /><em>'.Db::getInstance()->getValue('SELECT name FROM group_lang WHERE id_lang = 5 AND id_group = '.$customer->id_default_group).'</em> '.($ex_rivenditore == 1 ? ' - <strong style="color:red">(ex riv.)</strong>' : '').'<br /> '.$profilo.' <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="ATTIVO: Almeno un acquisto nell\'anno<br />FEDELE: almeno tre acquisti nell\'anno<br />PROSPECT: non ha ancora acquistato quest\'anno, ma ha un preventivo o un carrello<br />LEAD: non ha acquistato né chiesto preventivi quest\'anno, ma ci ha contattato o ci sono attivit&agrave; in corso<br />INATTIVO: non acquista da almeno un anno" title="ATTIVO: Almeno un acquisto nell\'anno<br />FEDELE: almeno tre acquisti nell\'anno<br />PROSPECT: non ha ancora acquistato quest\'anno, ma ha un preventivo o un carrello<br />LEAD: non ha acquistato né chiesto preventivi quest\'anno, ma ci ha contattato o ci sono attivit&agrave; in corso<br />INATTIVO: non acquista da almeno un anno" /></td>
				<td style="width:100px"><strong>Tipo</strong><br />'.($customer->is_company == 1? 'Azienda' : 'Privato').'</td>
				<td style="width:100px"><strong>Status</strong><br />'.($customer->active == 1 ? '<span style="color:#008316">Attivo</span>' : '<span style="color:red">Inattivo</span>').'</td>
				<td><strong>Registrazione</strong><br />'.Tools::displayDate($customer->date_add, $cookie->id_lang, false).'</td>
				</tr>
				
				<tr id="dati-cliente-riga-due"><td>'.($fido != '' && $fido > 0 ? '<table><tr><td style="padding-right:15px"><strong>Fido residuo</strong></td><td>' : '').'<strong>Referente</strong> '.($fido != '' && $fido > 0 ? '</td></tr>' : '<br />');
				
				if ($customer->is_company == 0) 
				{
					echo $nome.' '.$cognome;
				}
				else 
				{
					
					echo '
					'.($fido != '' && $fido > 0 ? '<tr><td><span style="color:'.($crediti_fido < 0 ? 'red' : 'green').'">'.number_format($crediti_fido,2,",",".").'</span> <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Finora il cliente ha eseguito ordini con pagamento differito per '.number_format($somma_fido,2,",",".").' euro. Rimangono quindi '.($crediti_fido < 0 ? 0 : number_format($crediti_fido,2,",",".")).' euro disponibili per ulteriori ordini." title="Il cliente ha un fido di '.number_format($fido,2,",",".").' euro. Finora il cliente ha eseguito ordini con pagamento differito per '.number_format($somma_fido,2,",",".").' euro. Rimangono quindi '.($crediti_fido < 0 ? 0 : number_format($crediti_fido,2,",",".")).' euro disponibili per ulteriori ordini." /></td><td>' : '').'<select name="referenteCC" style="max-width:230px" onchange="aggiornaDatiCliente(\'referente\', this.value);" >';
					echo "<option value=''>-- Seleziona --</option>";
					
					$persone = Db::getInstance()->executeS("SELECT id_persona, firstname, lastname, email FROM persone WHERE id_customer = ".Tools::getValue('id_customer')."");

					foreach ($persone as $persona) 
					{
							
						echo "<option value='".$persona['id_persona']."' ".($persona['id_persona'] == $referente_c ? 'selected="selected"' : "").">".$persona['firstname']." ".$persona['lastname']."</option>";
							
					}
							
					echo "</select>".($fido != '' && $fido > 0 ? '</td></tr></table>' : '');
					
				}	
				
				$prov_text = Db::getInstance()->getValue('SELECT iso_code FROM state WHERE id_state = '.$provincia);
				if($prov_text == '')
					$prov_text = 'N.d.';
				
				$naz_text = Db::getInstance()->getValue('SELECT iso_code FROM country WHERE id_country = '.$nazione);
				
				echo '</td>
				<td><strong>Zona&nbsp;&nbsp;&nbsp;<br /></strong>'.($prov_text == 'N.d.' ? $naz_text : $naz_text.'-'.$prov_text).'</td>
				<td><table cellpadding="0" cellspacing="0"><tr style="padding-top:0px;"><td style="padding-top:0px;"><strong>Ord. 6 mesi</strong></td><td style="padding-top:0px; padding-left:20px"><strong>12 mesi</strong></td></tr><tr><td style="padding-top:0px;">'.Tools::displayPrice($ordini_ultimi_6_mesi,1).'</td><td style="padding-top:0px; padding-left:20px">'.Tools::displayPrice($ordini_ultimi_12_mesi,1).'</td></tr></table></td>
				<td><strong>Ordinato tot.</strong><br />'.Tools::displayPrice($ordini_totali,1).'</td>
				
				<td><strong>Ultimo acq.</strong><br />'.(!$data_ultimo_acquisto ? '-' : Tools::displayDate($data_ultimo_acquisto, $cookie->id_lang, false)).'</td>
				
				<td><strong>Agente</strong><br />
				
				<select name="agenteC" '.($cookie->profile == 7 ? 'disabled="disabled"' : '').' onchange="aggiornaDatiCliente(\'agente\', this.value)">
				<option value="0" >-- Seleziona --</option>
				';
					
					$impiegati = Db::getInstance()->executeS('SELECT id_employee, CONCAT(firstname," ",LEFT(lastname, 1),".") AS name FROM employee ORDER BY firstname ASC, lastname ASC');
					
				foreach ($impiegati as $impiegato)
					echo '<option value="'.(int)($impiegato['id_employee']).'" '.($impiegato['id_employee'] == $agente_c ? ' selected="selected"' : '').'>'.htmlentities($impiegato['name'], ENT_NOQUOTES, 'utf-8').'</option>';
				echo '
					</select>
				
				
				</td><td><strong>Tecnico</strong><br />
				
				<select name="tecnicoCC" onchange="aggiornaDatiCliente(\'tecnico\', this.value)">
				<option value="0" >-- Seleziona --</option>
				';
				
					$tecnici = Db::getInstance()->executeS('SELECT id_employee, CONCAT(firstname," ",LEFT(lastname, 1),".") AS name FROM employee WHERE id_employee = 3 OR id_employee = 4 OR id_employee = 5 OR id_employee = 12 OR id_employee = 17 ORDER BY firstname ASC, lastname ASC');
		
					foreach ($tecnici as $impiegato)
					echo '<option value="'.(int)($impiegato['id_employee']).'" '.($impiegato['id_employee'] == $tecnico_c ? ' selected="selected"' : '').'>'.htmlentities($impiegato['name'], ENT_NOQUOTES, 'utf-8').'</option>';
				echo '
					</select>

				</td>
				
				</tr>';
				
				// RIGA 3
				
				$r3_visite = Db::getInstance()->getValue('
					SELECT COUNT(conn.id_connections) as Visite
					FROM connections conn 
					LEFT JOIN guest g ON conn.id_guest = g.id_guest 
					LEFT JOIN customer c ON g.id_customer = c.id_customer 
					WHERE g.id_customer = '.Tools::getValue('id_customer').'
				');
				
				$r3_pagine = Db::getInstance()->getValue('
					SELECT
						COUNT(a.id_connections) as Pagine
					FROM (
						SELECT DISTINCT conn.id_connections, cp.id_page 
						FROM connections conn 
						LEFT JOIN guest g ON conn.id_guest = g.id_guest 
						LEFT JOIN customer c ON g.id_customer = c.id_customer 
						LEFT JOIN connections_page cp ON cp.id_connections = conn.id_connections 
						WHERE g.id_customer = '.Tools::getValue('id_customer').'
					) a
				');
				
				$r3_prodotti = Db::getInstance()->executeS('
					SELECT pl.name AS Nome, CONCAT(pr.reference," (", IF(LEFT(c.date_add,10) = CURRENT_DATE(), "Oggi", IF(YEAR(c.date_add) = YEAR(CURRENT_DATE()), DATE_FORMAT(LEFT(c.date_add,10), "%d/%m"), DATE_FORMAT(LEFT(c.date_add,10), "%d/%m/%Y"))),")") AS Codice
					FROM connections c 
					LEFT JOIN guest g ON c.id_guest = g.id_guest 
					LEFT JOIN customer cu ON g.id_customer = cu.id_customer 
					LEFT JOIN connections_page cp ON cp.id_connections = c.id_connections 
					LEFT JOIN page p ON p.id_page = cp.id_page 
					LEFT JOIN page_type pt ON pt.id_page_type = p.id_page_type
					LEFT JOIN product pr ON pr.id_product = p.id_object
					LEFT JOIN product_lang pl ON pr.id_product = pl.id_product
					WHERE g.id_customer = '.Tools::getValue('id_customer').'
						AND pt.name = "product.php"
					ORDER BY c.date_add DESC
					LIMIT 1
				');
				
				// TUTTO IL RESTO FUORI DAL FOREACH PERCHE' LA QUERY RESTITUISCE UNA SOLA ROW
				foreach($r3_prodotti as $r_prodotto){
					$r3_prodotti_c = $r_prodotto['Codice'];
					$r3_prodotti_n = $r_prodotto['Nome'];
				}
				
				$r3_ch_ricevuta = Db::getInstance()->executeS('SELECT LEFT(datetime,16) as data, extfield1, extfield2 FROM telefonate WHERE id_customer = '.Tools::getValue('id_customer').' AND calltype = "Inbound" ORDER BY datetime DESC LIMIT 1');
				
				if(count($r3_ch_ricevuta)>0){
					foreach($r3_ch_ricevuta as $ricevuta){
						$ric_date = $ricevuta['data'];
						$ric_da = $ricevuta['extfield2'];
					}
				}
				
				$ric_date1 = new DateTime($ric_date);
				
				if($ric_date1->format('Y-m-d') == date("Y-m-d"))
					$ric_date = 'Oggi '.$ric_date1->format('H:i');
				else if($ric_date1->format('Y') == date("Y"))
					$ric_date = $ric_date1->format('d/m');
				else
					$ric_date = $ric_date1->format('d/m/Y');
				
				$r3_ch_inviata = Db::getInstance()->executeS('SELECT LEFT(datetime,16) as data, extfield1, extfield2 FROM telefonate WHERE id_customer = '.Tools::getValue('id_customer').' AND calltype = "Outbound" ORDER BY datetime DESC LIMIT 1');
				
				if(count($r3_ch_inviata)>0){
					foreach($r3_ch_inviata as $inviata){
						$inv_date = $inviata['data'];
						$inv_da = $inviata['extfield1'];
					}
				}
				
				$inv_date1 = new DateTime($inv_date);
				
				if($inv_date1->format('Y-m-d') == date("Y-m-d"))
					$inv_date = 'Oggi '.$inv_date1->format('H:i');
				else if($inv_date1->format('Y') == date("Y"))
					$inv_date = $inv_date1->format('d/m');
				else
					$inv_date = $inv_date1->format('d/m/Y');
				
				$r3_azione = Db::getInstance()->executeS('
					SELECT a.tipo, a.tecnico, a.data 
					FROM (
						(SELECT "TO DO" as tipo, act.id_action as id, e.firstname as tecnico, act.date_upd as data FROM action_thread act join employee e on act.action_to = e.id_employee WHERE act.id_customer = '.Tools::getValue('id_customer').' and (e.id_employee = 4 OR e.id_employee = 5 OR e.id_employee = 12 OR e.id_employee = 17) ORDER BY act.date_upd DESC)
						UNION
						(SELECT "Preventivo" as tipo, ft.id_thread, e.firstname, ft.date_upd FROM form_prevendita_thread ft join employee e on ft.id_employee = e.id_employee WHERE ft.id_customer = '.Tools::getValue('id_customer').' and (e.id_employee = 4 OR e.id_employee = 5 OR e.id_employee = 12 OR e.id_employee = 17) order by ft.date_upd desc)
						UNION
						(SELECT "Ticket" as tipo, cm.id_customer_thread, e.firstname, cm.date_add FROM `customer_thread` ct inner join customer_message cm on ct.id_customer_thread = cm.id_customer_thread inner join employee e on cm.id_employee = e.id_employee where cm.id_employee!=0 and ct.id_customer = '.Tools::getValue('id_customer').' and (e.id_employee = 4 OR e.id_employee = 5 OR e.id_employee = 12 OR e.id_employee = 17) group by cm.id_customer_thread order by cm.date_add desc)
						UNION
						(SELECT "Carrello creato" as tipo, c.id_cart, e.firstname, c.date_add from cart c join employee e on c.created_by = e.id_employee WHERE c.id_customer = '.Tools::getValue('id_customer').' and (e.id_employee = 4 OR e.id_employee = 5 OR e.id_employee = 12 OR e.id_employee = 17) ORDER BY c.date_add desc)
						UNION
						(SELECT "Carrello aggiornato" as tipo, c.id_cart, e.firstname, c.date_upd from cart c join employee e on c.id_employee = e.id_employee WHERE c.created_by = 0 and c.id_customer = '.Tools::getValue('id_customer').' and (e.id_employee = 4 OR e.id_employee = 5 OR e.id_employee = 12 OR e.id_employee = 17) ORDER BY c.date_upd desc)
						) a
					ORDER BY data desc
					LIMIT 1
				');
				
				// TUTTO IL RESTO FUORI DAL FOREACH PERCHE' LA QUERY RESTITUISCE UNA SOLA ROW
				foreach($r3_azione as $ultima_azione){
					$r3_azione_tipo = $ultima_azione['tipo'];
					$r3_tecnico = $ultima_azione['tecnico'];
					$r3_data_azione = $ultima_azione['data'];
				}
				
				$azione_date1 = new DateTime($r3_data_azione);
				// MODIFICATO - PROVARE
				if($azione_date1->format('Y-m-d') == date("Y-m-d"))
					$r3_data_azione = 'Oggi '.$azione_date1->format('H:i');
				else if($azione_date1->format('Y') == date("Y"))
					$r3_data_azione = $azione_date1->format('d/m');
				else
					$r3_data_azione = $azione_date1->format('d/m/Y');
				
				echo '
				<tr id="dati-cliente-riga-tre" style="height:46px;">
					<td><strong>Visite</strong><br />'.$r3_visite.'</td>
					<td><strong>Pagine viste</strong><br />'.$r3_pagine.'</td>
					<td title="'.$r3_prodotti_n.'"><strong>Ultimo prod. visto</strong><br />'.(count($r3_prodotti)>0 ?$r3_prodotti_c : "Nessuno").'</td>
					<td><strong>Ultima ch. ric.</strong><br />'.(count($r3_ch_ricevuta)>0 ? $ric_da." (".$ric_date.")" : "-").'</td>
					<td><strong>Ultima ch. inv.</strong><br />'.(count($r3_ch_inviata)>0 ? $inv_da." (".$inv_date.")" : "-").'</td>
					<td><strong>Ultima azione</strong><br />'.(count($r3_azione)>0 ? $r3_tecnico." (".$r3_data_azione.")" : "-").'</td>
				</tr>
				
				</table>
				<hr />
			</div>';
			$customer_note = Db::getInstance()->getValue('SELECT count(*) FROM customer_note WHERE id_customer = '.Tools::getValue('id_customer').'');
			$num_prv = Db::getInstance()->getValue('SELECT count(*) FROM form_prevendita_thread WHERE status = "open" AND id_customer = '.Tools::getValue('id_customer'));
			$num_tkt = Db::getInstance()->getValue('SELECT count(*) FROM customer_thread ct	LEFT JOIN customer_message cm ON ct.id_customer_thread = cm.id_customer_thread
			WHERE (status = "open") AND (id_contact != 7 OR (cm.id_employee = 0 AND ct.id_contact = 7)	) AND id_customer = '.Tools::getValue('id_customer'));
			$num_act = Db::getInstance()->getValue('SELECT count(*) FROM action_thread WHERE status = "open" AND id_customer = '.Tools::getValue('id_customer'));
			
		
			if($num_prv+$num_tkt+$num_act > 0 || $customer_note > 0 || $blacklist == 1)
				echo '<div class="error">Attenzione: 
				'.($num_tkt > 0 != '' ? '<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&tab-container-1=6&token='.$this->token.'" target="_blank">ticket in sospeso</a> - ' : '').' 
				'.($num_prv > 0 != '' ? '<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&tab-container-1=7&token='.$this->token.'" target="_blank">azioni cliente in sospeso</a> - ' : '').' 
				'.($num_act > 0 != '' ? '<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&tab-container-1=10&token='.$this->token.'" target="_blank">TO DO in sospeso</a> - ' : '').' 
				'.($customer_note > 0 ? 'note private in anagrafica - ' : '').' 
				'.($blacklist == 1 ? 'CLIENTE IN BLACKLIST - ' : '').' 
				</div>';
			
		//if(Tools::getIsset('tab-container-1') && (Tools::getValue('tab-container-1') == 6 || Tools::getValue('tab-container-1') == 7 || Tools::getValue('tab-container-1') == 10  || Tools::getValue('tab-container-1') == 14)) {
		if(Tools::getIsset('tab-container-1')) {
			if(Tools::getIsset('submitCollegaAttivita'))
			{
				$id_thread = substr(Tools::getValue('coll_partenza'),1);
				switch(substr(Tools::getValue('coll_partenza'), 0,1))
				{
					case 'T': Db::getInstance()->execute('UPDATE customer_thread SET riferimento = "'.Tools::getValue('coll_destinazione').'" WHERE id_customer_thread = '.$id_thread.''); break;
					case 'P': Db::getInstance()->execute('UPDATE form_prevendita_thread SET riferimento = "'.Tools::getValue('coll_destinazione').'" WHERE id_thread = '.$id_thread.''); break;
					case 'A': Db::getInstance()->execute('UPDATE action_thread SET riferimento = "'.Tools::getValue('coll_destinazione').'" WHERE id_action = '.$id_thread.''); break;
					case 'L': Db::getInstance()->execute('UPDATE bdl SET riferimento = "'.Tools::getValue('coll_destinazione').'" WHERE id_bdl = '.$id_thread.''); break;
					
					default: ''; break;
				}
				
				Customer::Storico($id_thread, substr(Tools::getValue('coll_partenza'),0,1), $cookie->id_employee, 'Ha collegato ad attivit&agrave; n. '.Customer::trovaSigla(substr(Tools::getValue('coll_destinazione'),1), substr(Tools::getValue('coll_destinazione'),0,1)));
			}
		
			echo '
			<link rel="stylesheet" href="jquery.treeview.css" type="text/css" />


			<script type="text/javascript" src="jquery.treeview.js"></script>

			<script type="text/javascript">

			// build treeview after web page has been loaded

			$(document).ready(function(){

			$(".tree-menu").treeview();
			
			$(".tasti-apertura").mouseover(function() {
				$(this).children(".menu-apertura").show();
			}).mouseout(function() {
				$(this).children(".menu-apertura").hide();
			});
			
			$(".tasti-apertura-in").mouseover(function() {
				$(this).children(".menu-apertura-in").show();
			}).mouseout(function() {
				$(this).children(".menu-apertura-in").hide();
			});


			});

			</script>
	';
	
			echo '
			<div class="tasti-apertura"><a class="button" style="display:block" href="#"><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" />&nbsp;&nbsp;&nbsp;Aggiungi nuova azione padre</a>
				<div class="menu-apertura">
					<ul class="dropdown">
						<li class="tasti-apertura-in"><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewticket&aprinuovoticket&token='.$this->token.'&tab-container-1=6"><img src="../img/admin/AdminTools.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;Ticket</a>
							<div class="menu-apertura-in" style="position:absolute; left:100px">
								<ul class="dropdown">
									<li><a style="color:#000" href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewticket&aprinuovoticket&token='.$this->token.'&tipo-ticket=2&tab-container-1=6"><img src="../img/admin/icons/contabilita.gif" alt="Contabilita" title="Contabilita" />&nbsp;&nbsp;&nbsp;Contabilit&agrave;</a></li>
									
									<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewticket&aprinuovoticket&token='.$this->token.'&tipo-ticket=8&tab-container-1=6"><img src="../img/admin/icons/ordini.gif" alt="Ordini" title="Ordini" />&nbsp;&nbsp;&nbsp;Ordini</a></li>
									
									<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewticket&aprinuovoticket&token='.$this->token.'&tipo-ticket=4&tab-container-1=6"><img src="../img/admin/icons/assistenza.gif" alt="Assistenza tecnica" title="Assistenza tecnica" />&nbsp;&nbsp;&nbsp;Assistenza tecnica</a></li>
									
									<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewticket&aprinuovoticket&token='.$this->token.'&tipo-ticket=9&tab-container-1=6"><img src="../img/admin/icons/rma.gif" alt="RMA" title="RMA" />&nbsp;&nbsp;&nbsp;RMA</a></li>
									
									<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewticket&aprinuovoticket&token='.$this->token.'&tipo-ticket=3&tab-container-1=6"><img src="../img/admin/icons/rivenditori.gif" alt="Rivenditori" title="Rivenditori" />&nbsp;&nbsp;&nbsp;Rivenditori</a></li>
									
								</ul>
							</div>
						</li>
						<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewmessage&aprinuovomessaggio&token='.$this->token.'&tab-container-1=7"><img src="../img/admin/email.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;Messaggio</a></li>
						<li class="tasti-apertura-in"><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&aprinuovaazione&token='.$this->token.'&tab-container-1=10"><img src="../img/admin/return.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;To-do</a>
							<div class="menu-apertura-in" style="position:absolute; left:100px">
								<ul class="dropdown">
									<li><a style="color:#000" href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&aprinuovaazione&token='.$this->token.'&tipo-todo=Attivita&tab-container-1=10"><img src="../img/admin/icons/attivita.gif" alt="Attivita" title="Attivita" />&nbsp;&nbsp;&nbsp;Attivit&agrave;</a></li>
									
									<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&aprinuovaazione&token='.$this->token.'&tipo-todo=Telefonata&tab-container-1=10"><img src="../img/admin/icons/telefonata.gif" alt="Telefonata" title="Telefonata" />&nbsp;&nbsp;&nbsp;Telefonata</a></li>
									
									<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&aprinuovaazione&token='.$this->token.'&tipo-todo=Visita&tab-container-1=10"><img src="../img/admin/icons/visita.gif" alt="Visita" title="Visita" />&nbsp;&nbsp;&nbsp;Visita</a></li>
									
									<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&aprinuovaazione&token='.$this->token.'&tipo-todo=Caso&tab-container-1=10"><img src="../img/admin/icons/caso.gif" alt="Caso" title="Caso" />&nbsp;&nbsp;&nbsp;Caso</a></li>
									
									<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&aprinuovaazione&token='.$this->token.'&tipo-todo=Intervento&tab-container-1=10"><img src="../img/admin/icons/intervento.gif" alt="Intervento" title="Intervento" />&nbsp;&nbsp;&nbsp;Intervento</a></li>
									
									<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&aprinuovaazione&token='.$this->token.'&tipo-todo=Richiesta_RMA&tab-container-1=10"><img src="../img/admin/icons/richiesta_rma.gif" alt="Richiesta RMA" title="Richiesta RMA" />&nbsp;&nbsp;&nbsp;Richiesta RMA</a></li>
								</ul>
							</div>
						</li>
						<li class="tasti-apertura-in"><a href="index.php?tab=AdminCustomers&viewcart&editcart&createnew&provvisorio=1&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4&preventivo=1#modifica-carrello" onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }" id="link-preventivo"><img src="../img/admin/charged_ok.gif" alt="Nuovo preventivo" title="Nuovo preventivo" /> &nbsp;&nbsp;&nbsp;Preventivo</a>';
							
							$orders_telegest = Db::getInstance()->getValue('SELECT count(id_order) FROM orders WHERE id_customer = '.$customer->id);
							if($orders_telegest > 0)
							{
								$orders_telegest_a = Db::getInstance()->executeS('SELECT DISTINCT id_order FROM orders WHERE id_customer = '.$customer->id);
								$otz = 0;
								foreach($orders_telegest_a as $ot)
								{
									$orders_t_string .= ($otz % 9 == 0 ? '</tr><tr>' : '').'<td><input type="checkbox" class="orders_telegest_c" name="orders_telegest[]" value="'.$ot['id_order'].'" /> '.$ot['id_order'].'&nbsp;&nbsp;&nbsp;&nbsp;</td>';
									$otz++;
								}	
								echo '
									<div class="hider" id="hider_telegest_form" style="display:none"></div>
									<div class="popup_box" id="popup_telegest_form" style="display:none; width:50%; text-align:center; z-index:999999999"><form method="post" action="index.php?tab=AdminCustomers&viewcart&editcart&provvisorio=1&id_customer='.$customer->id.'&amp;copy_to='.$customer->id.'&copia_carrello_conferma=y&id_cart=84366&viewcustomer&token='.$this->token.'&tab-container-1=4&preventivo=1&rif_prev='.$rif.'#modifica-carrello"> Seleziona gli ordini su cui applicare la telegestione: <table><tr>'.$orders_t_string.'</tr></table><br />
									<input type="checkbox" onchange="$(\'.orders_telegest_c\').not(this).prop(\'checked\', this.checked);" />Seleziona tutti<br /><br />
									<input type="submit" class="button" value="Conferma" />
									
									<button type="button" class="button"  onclick="$(\'#hider_telegest_form\').hide(); $(\'#popup_telegest_form\').hide();">Chiudi</button>
									
									</form>
									</div>
									<div class="menu-apertura-in" style="position:absolute; left:100px">
										<ul class="dropdown">
											<li><a style="color:#000" onclick="$(\'#hider_telegest_form\').show(); $(\'#popup_telegest_form\').show(); "><img src="../img/admin/icons/attivita.gif" alt="Telegestione annuale" title="Telegestione annuale" />&nbsp;&nbsp;&nbsp;Telegestione annuale</a></li>
										</ul>
									</div>
								';
							}
							echo '</li>
						
						<li><a href="index.php?tab=AdminCustomers&viewcart&editcart&createnew&provvisorio=1&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4&preventivo=2#modifica-carrello" onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }" id="link-ordine-manuale"><img src="../img/admin/cart.gif" alt="Nuovo ordine manuale" title="Nuovo ordine manuale" /> &nbsp;&nbsp;&nbsp;Ordine manuale</a></li>
						
						<li><a href="index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&nuovobdl&token='.$this->token.'&tab-container-1=14"><img src="../img/admin/prefs.gif" alt="Buono di lavoro" title="Buono di lavoro" />&nbsp;&nbsp;&nbsp;Buono di lavoro (BDL)</a></li>
					</ul>
				</div>
			</div>';
			
			if(Tools::getIsset('id_customer_thread') || Tools::getIsset('id_thread') || Tools::getIsset('id_mex') || Tools::getIsset('id_action') || Tools::getIsset('id_order') || Tools::getIsset('id_cart')  || Tools::getIsset('edit_bdl'))
			{
				if(Tools::getIsset('id_customer_thread'))
					$rif = 'T'.Tools::getValue('id_customer_thread');
				else if(Tools::getIsset('id_thread'))
					$rif = 'P'.Tools::getValue('id_thread');
				else if(Tools::getIsset('id_mex'))
					$rif = 'T'.Tools::getValue('id_mex');
				else if(Tools::getIsset('id_action'))
					$rif = 'A'.Tools::getValue('id_action');
				else if(Tools::getIsset('id_order'))
					$rif = 'O'.Tools::getValue('id_order');
				else if(Tools::getIsset('id_cart'))
					$rif = 'C'.Tools::getValue('id_cart');
				else if(Tools::getIsset('edit_bdl'))
					$rif = 'L'.Tools::getValue('edit_bdl');
				
				echo '
				<div class="tasti-apertura">
					<a style="display:block" class="button" href="#"><img src="../img/admin/tab-groups.gif" alt="Aggiungi figlia" title="Aggiungi figlia" />&nbsp;&nbsp;&nbsp;Aggiungi azione figlia</a>
					<div class="menu-apertura">
						<ul class="dropdown">
							<li class="tasti-apertura-in"><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewticket&aprinuovoticket&riferimento='.$rif.'&token='.$this->token.'&tab-container-1=6"><img src="../img/admin/AdminTools.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;Ticket</a>
								<div class="menu-apertura-in" style="position:absolute; left:100px">
									<ul class="dropdown">
										<li><a style="color:#000" href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewticket&aprinuovoticket&riferimento='.$rif.'&token='.$this->token.'&tipo-ticket=2&tab-container-1=6"><img src="../img/admin/icons/contabilita.gif" alt="Contabilita" title="Contabilita" />&nbsp;&nbsp;&nbsp;Contabilit&agrave;</a></li>
										
										<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewticket&aprinuovoticket&riferimento='.$rif.'&token='.$this->token.'&tipo-ticket=8&tab-container-1=6"><img src="../img/admin/icons/ordini.gif" alt="Ordini" title="Ordini" />&nbsp;&nbsp;&nbsp;Ordini</a></li>
										
										<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewticket&aprinuovoticket&riferimento='.$rif.'&token='.$this->token.'&tipo-ticket=4&tab-container-1=6"><img src="../img/admin/icons/assistenza.gif" alt="Assistenza tecnica" title="Assistenza tecnica" />&nbsp;&nbsp;&nbsp;Assistenza tecnica</a></li>
										
										<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewticket&aprinuovoticket&riferimento='.$rif.'&token='.$this->token.'&tipo-ticket=9&tab-container-1=6"><img src="../img/admin/icons/rma.gif" alt="RMA" title="RMA" />&nbsp;&nbsp;&nbsp;RMA</a></li>
										
										<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewticket&aprinuovoticket&riferimento='.$rif.'&token='.$this->token.'&tipo-ticket=3&tab-container-1=6"><img src="../img/admin/icons/rivenditori.gif" alt="Rivenditori" title="Rivenditori" />&nbsp;&nbsp;&nbsp;Rivenditori</a></li>
										
									</ul>
								</div>
							</li>
							<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewmessage&aprinuovomessaggio&riferimento='.$rif.'&token='.$this->token.'&tab-container-1=7"><img src="../img/admin/email.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;Messaggio</a></li>
							<li class="tasti-apertura-in"><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$this->token.'&tab-container-1=10"><img src="../img/admin/return.gif" alt="Ticket" title="Ticket" />&nbsp;&nbsp;&nbsp;To-do</a>
								<div class="menu-apertura-in" style="position:absolute; left:100px">
									<ul class="dropdown">
										<li><a style="color:#000" href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$this->token.'&tipo-todo=Attivita&tab-container-1=10"><img src="../img/admin/icons/attivita.gif" alt="Attivita" title="Attivita" />&nbsp;&nbsp;&nbsp;Attivit&agrave;</a></li>
										
										<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$this->token.'&tipo-todo=Telefonata&tab-container-1=10"><img src="../img/admin/icons/telefonata.gif" alt="Telefonata" title="Telefonata" />&nbsp;&nbsp;&nbsp;Telefonata</a></li>
										
										<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$this->token.'&tipo-todo=Visita&tab-container-1=10"><img src="../img/admin/icons/visita.gif" alt="Visita" title="Visita" />&nbsp;&nbsp;&nbsp;Visita</a></li>
										
										<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$this->token.'&tipo-todo=Caso&tab-container-1=10"><img src="../img/admin/icons/caso.gif" alt="Caso" title="Caso" />&nbsp;&nbsp;&nbsp;Caso</a></li>
										
										<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$this->token.'&tipo-todo=Intervento&tab-container-1=10"><img src="../img/admin/icons/intervento.gif" alt="Intervento" title="Intervento" />&nbsp;&nbsp;&nbsp;Intervento</a></li>
										
										<li><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&aprinuovaazione&riferimento='.$rif.'&token='.$this->token.'&tipo-todo=Richiesta_RMA&tab-container-1=10"><img src="../img/admin/icons/richiesta_rma.gif" alt="Richiesta RMA" title="Richiesta RMA" />&nbsp;&nbsp;&nbsp;Richiesta RMA</a></li>
									</ul>
								</div>
							</li>
							<li class="tasti-apertura-in"><a href="index.php?tab=AdminCustomers&viewcart&editcart&createnew&provvisorio=1&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4&preventivo=1&rif_prev='.$rif.'#modifica-carrello" onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }" id="link-preventivo"><img src="../img/admin/charged_ok.gif" alt="Nuovo preventivo" title="Nuovo preventivo" /> Preventivo</a>';
							
							$orders_telegest = Db::getInstance()->getValue('SELECT count(id_order) FROM orders WHERE id_customer = '.$customer->id);
							if($orders_telegest > 0)
							{
								$orders_telegest_a = Db::getInstance()->executeS('SELECT DISTINCT id_order FROM orders WHERE id_customer = '.$customer->id);
								foreach($orders_telegest_a as $ot)
									$orders_t_stringb .= '<input type="checkbox" name="orders_telegest[]" value="'.$ot['id_order'].'" /> '.$ot['id_order'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
									
								echo '
									<div class="hider" id="hider_telegest_form2" style="display:none"></div>
									<div class="popup_box" id="popup_telegest_form2" style="display:none; width:50%; text-align:center; z-index:999999999"><form method="post" action="index.php?tab=AdminCustomers&viewcart&editcart&provvisorio=1&id_customer='.$customer->id.'&amp;copy_to='.$customer->id.'&copia_carrello_conferma=y&id_cart=84366&viewcustomer&token='.$this->token.'&tab-container-1=4&preventivo=1&rif_prev='.$rif.'#modifica-carrello"> Seleziona gli ordini su cui applicare la telegestione: '.$orders_t_stringb.'<br /><br />
									
									<input type="submit" class="button" value="Conferma" />
									
									<button type="button" class="button"  onclick="$(\'#hider_telegest_form2\').hide(); $(\'#popup_telegest_form2\').hide();">Chiudi</button>
									
									</form>
									</div>
									<div class="menu-apertura-in" style="position:absolute; left:100px">
										<ul class="dropdown">
											<li><a style="color:#000" onclick="$(\'#hider_telegest_form2\').show(); $(\'#popup_telegest_form2\').show(); "><img src="../img/admin/icons/attivita.gif" alt="Telegestione annuale" title="Telegestione annuale" />&nbsp;&nbsp;&nbsp;Telegestione annuale</a></li>
										</ul>
									</div>
								';
							}
							echo '
							</li> 
							
							<li><a href="index.php?tab=AdminCustomers&viewcart&editcart&createnew&provvisorio=1&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4&preventivo=2&rif_prev='.$rif.'#modifica-carrello" onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }" id="link-ordine-manuale"><img src="../img/admin/cart.gif" alt="Nuovo ordine manuale" title="Nuovo ordine manuale" /> Ordine manuale</a></li> 
							
							<li><a href="index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&nuovobdl&token='.$this->token.'&riferimento='.$rif.'&tab-container-1=14"><img src="../img/admin/prefs.gif" alt="Buono di lavoro" title="Buono di lavoro" />&nbsp;&nbsp;&nbsp;Buono di lavoro (BDL)</a></li>
							
						</ul>
					</div>
				</div>';
			}
			
			echo '
			&nbsp;&nbsp;&nbsp;<a id="invio_mail_outlook_t" href="#" onclick="window.location = \'mailto:\'+document.getElementById(\'selezione_outlook_t\').value" style="display:block;float:left;margin-right:0px" class="button" ><img src="../img/admin/outlook.gif" alt="Outlook" title="Outlook" />&nbsp;&nbsp;&nbsp;Invia mail con Outlook a: </a>
				<select name="selezione_outlook_t" id="selezione_outlook_t" onchange="window.location = \'mailto:\'+this.value" style="display:block;float:left;margin-right:10px; height:24px;width:150px" >
				<option value="">-- Seleziona mail --</option>
				';
				$le_mail = Db::getInstance()->executeS("SELECT id_persona, firstname, lastname, email FROM persone WHERE id_customer = ".Tools::getValue('id_customer')." AND email != ''");

				foreach ($le_mail as $la_mail) {
								
					echo "<option value='".$la_mail['email']."'>".$la_mail['email']."</option>";
							
				}
				
				echo '</select>	
			
			&nbsp;&nbsp;&nbsp;<a style="display:block;float:left;margin-right:10px" class="button" href="http://www.ezdirect.it" target="_blank"><img src="../img/admin/world.gif" alt="Collegati" title="Collegati" />&nbsp;&nbsp;&nbsp;Collegati al sito</a>
			<br /><br />
			
			';
			
			echo $customer->displayFormPreventivo($customer, $preventivo);
		}
			echo "
			<script type='text/javascript'>
			
				function aggiornaDatiCliente(tipo, valore)
				{
					$.ajax({
					  url:'ajax.php?aggiorna_dati_cliente=y',
					  type: 'POST',
					  data: { tipo: tipo, valore: valore, cliente: ".$customer->id."
					  },
					  success:function(resp){
						alert(resp + ' modificato');
					  },
					 
					});
					
					
				}

function openKCFinder(textarea) {
   window.KCFinder = {
        callBackMultiple: function(files) {
            window.KCFinder = null;
            textarea.value = '';
			var list = document.getElementById('list-ticket').innerHTML;	
			list = '';
            for (var i = 0; i < files.length; i++) {
                textarea.value += files[i] + \":::\";
				list += '<a href=\"https://ezdocs:WRY753MNR43ASD147g!@https://www.ezdirect.it'+files[i]+'\" target=\"_blank\">https://www.ezdirect.it'+files[i] + \"</a><br />\";
			}
			document.getElementById('list-ticket').innerHTML = list;
        }
    };
    window.open('kcfinder/browse.php?lang=it&type=files&dir=files', 'kcfinder_multiple',
        'status=no, toolbar=no, location=no, location=no, menubar=no, directories=no, ' +
        'resizable=1, scrollbars=no, width=800, height=600'
    );
}

</script>";
			
			
			if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 1 || !isset($_GET['tab-container-1'])) {
				$sql_ultime_cose = ("SELECT * FROM (SELECT id_thread as id, tipo_richiesta as tipo, tipo_richiesta as tipo_attivita, id_customer as cliente, id_employee as in_carico, status, (CASE WHEN((status = 'open' OR status = 'pending1') AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM form_prevendita_thread WHERE id_customer = ".$_GET['id_customer']." UNION 
			
				SELECT * FROM (SELECT id_customer_thread as id, ct.id_contact as tipo, cl.name as tipo_attivita, id_customer as cliente, id_employee as in_carico, status, (CASE WHEN((status = 'open' OR status = 'pending1') AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM customer_thread ct JOIN (SELECT * FROM contact_lang WHERE id_lang = 5) cl WHERE id_customer = ".$_GET['id_customer']." GROUP BY ct.id_customer_thread) ccc UNION 
				
				SELECT id_order as id, 'Ordine' as tipo, 'Ordine' as tipo_attivita, id_customer as cliente, '' as in_carico, (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) AS status, (CASE WHEN(((SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 4 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 5 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 14 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 15 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 16 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 20) AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM orders WHERE id_customer = ".$_GET['id_customer']." UNION 
				
				SELECT id_action as id, action_type as tipo, action_type as tipo_attivita, id_customer as cliente, action_to as in_carico, status, (CASE WHEN((status = 'open' OR status = 'pending1') AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM action_thread WHERE id_customer = ".$_GET['id_customer']." UNION 
				
				SELECT id_cart as id, 'Carrello' as tipo, 'Carrello' as tipo_attivita, id_customer as cliente, in_carico_a as in_carico, visualizzato AS status, '' as scaduto, date_add, date_upd FROM cart WHERE id_cart NOT IN (SELECT id_cart FROM orders) AND name LIKE '%amazon%' AND id_customer = ".$_GET['id_customer']." UNION
				
				SELECT id_cart as id, 'Carrello' as tipo, 'Carrello' as tipo_attivita, id_customer as cliente, in_carico_a as in_carico, visualizzato AS status, '' as scaduto, date_add, date_upd FROM carrelli_creati WHERE id_cart NOT IN (SELECT id_cart FROM orders) AND id_customer = ".$_GET['id_customer']." ORDER BY date_add DESC) x ORDER BY x.date_upd DESC ".(Tools::getIsset('ultime') && Tools::getValue('ultime') == 'vedi_tutte' ? '' : 'LIMIT 10')."");
			}
			else
			{
				$sql_ultime_cose = ("SELECT * FROM (
				
				SELECT id_order as id, 'Ordine' as tipo, 'Ordine' as tipo_attivita, id_customer as cliente, '' as in_carico, (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) AS status, (CASE WHEN(((SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 4 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 5 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 14 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 15 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 16 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 20) AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM orders WHERE id_customer = ".$_GET['id_customer']." UNION 
				
				
				SELECT id_cart as id, 'Carrello' as tipo, 'Carrello' as tipo_attivita, id_customer as cliente, in_carico_a as in_carico, visualizzato AS status, '' as scaduto, date_add, date_upd FROM carrelli_creati WHERE id_customer = ".$_GET['id_customer']." AND id_cart NOT IN (SELECT id_cart FROM orders) ORDER BY date_add DESC) x ORDER BY x.date_upd DESC ".(Tools::getIsset('ultime') && Tools::getValue('ultime') == 'vedi_tutte' ? '' : 'LIMIT 10')."");
				
			}
			
			if (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== false)
			{
				flush();
			}
			
			echo '<div class="tab1" id="anagrafica">';
			
			/*echo '<h2><a>'.$this->l('Attività recenti del cliente').' </a></h2>';*/
			
			
			
			
			//if(Tools::getIsset('id_customer') != 66355)
			$ultime_cose = Db::getInstance()->executeS($sql_ultime_cose);
			
			
			//SELECT id_cart as id, 'Carrello' as tipo, id_customer as cliente, in_carico_a as in_carico, '' AS status, '' as scaduto, date_add, date_upd FROM cart WHERE id_cart NOT IN (SELECT id_cart FROM orders WHERE id_customer = ".$_GET['id_customer'].") AND id_customer = ".$_GET['id_customer']." ORDER BY date_add DESC");
			
			
			if(isset($_GET['orderby']))
			{
				$pre_sql_ultime_cose = 'SELECT * FROM (';
				$sql_ultime_cose = $pre_sql_ultime_cose.$sql_ultime_cose;
				
				$sql_ultime_cose .= ') y ORDER BY y.'.Tools::getValue('orderby').' '.Tools::getValue('orderway');
				
				$ultime_cose = Db::getInstance()->executeS($sql_ultime_cose);
				
			}
			
			if(sizeof($ultime_cose) > 0)
			{	
		echo '
			<script type="text/javascript">
			var myTextExtraction = function(node) 
				{ 
					// extract data from markup and return it 
					return node.textContent.replace( /<.*?>/g, "" ); 
				}
				
			$(document).ready(function() 
				{ 
				
			
				$.tablesorter.addParser({ 
					// set a unique id 
					id: "thousands",
					is: function(s) { 
						// return false so this parser is not auto detected 
						return false; 
					}, 
					format: function(s) {
						// format your data for normalization 
						return s.replace(/[\s.$£€]/g,"").replace(/,/g,".");
					}, 
					// set type, either numeric or text 
					type: "numeric" 
				}); 


					$("#table-ultime-cose").tablesorter({ textExtraction: { 0: function(node, table, cellIndex){ return $(node).find("strong").text(); } }, headers: {
					0: {sorter: false}, 9: {sorter: "shortDate", dateFormat: "ddmmyy"},  10: {sorter: "shortDate", dateFormat: "ddmmyy"}, 8: {//zero-based column index
                sorter:"thousands"
            }
        }, cssChildRow: "invisible-table-row", widthFixed: true, widgets: ["zebra"]});
					
				} 
			);
			</script>';
			echo "<div style='".(count($ultime_cose) > 3 ? 'height:200px; overflow-y:auto' : '')."' id='ultime-container'><table id='table-ultime-cose' class='table tablesorter'>
			<thead><tr><th style='width:100px; max-width:100px'>Tipo 
			<a href='index.php?tab=AdminCustomers&viewcustomer&id_customer=".$_GET['id_customer']."&orderby=tipo_attivita&orderway=asc&token=".$this->token."&tab-container-1=1'><img src='../img/admin/up.gif' alt='Su' title='Su' /></a>   
				<a href='index.php?tab=AdminCustomers&viewcustomer&id_customer=".$_GET['id_customer']."&orderby=tipo_attivita&orderway=desc&token=".$this->token."&tab-container-1=1'><img src='../img/admin/down.gif' alt='Giu' title='Giu' /></a>
			</th><th style='width:70px; max-width:70px'>ID</th><th style='width:70px; max-width:70px'>Creato da</th><th  style='width:70px; max-width:70px'>In carico a</th><th style='width:30px; max-width:30px'>Conv.</th><th style='max-width:150px'>Status</th><th style='max-width:100px'>Oggetto</th><th style='width:70px; max-width:70px'>Totale</th><th  data-sorter='shortDate' data-date-format='ddmmyyyy'>Data apertura</th><th data-sorter='shortDate' data-date-format='ddmmyyyy'>Ultima modifica</th></tr></thead><tbody>";
			
			foreach($ultime_cose as $ultima_cosa) {
				if ($ultima_cosa['tipo'] == 7){
					$ctrl_mex = Db::getInstance()->getValue("SELECT COUNT(id_customer_message) FROM customer_thread ct JOIN customer_message cm ON ct.id_customer_thread = cm.id_customer_thread WHERE cm.id_employee = 0 AND ct.id_customer_thread = ".$ultima_cosa['id']."");	
						
					if($ctrl_mex >= 1) {
						
					} else {
						$ultima_cosa['status'] = ''; $ultima_cosa['scaduto'] = '';
					}

				}
					
				if($i<5) {
					$includilo = 'yes';
				}
				else {
					if($ultima_cosa['scaduto'] == 'SCADUTO') {
						$includilo = 'yes';
					}
					else {
						$includilo = 'no';
					}
				
				}
				$conv = "--";
				if($includilo == 'yes') {
					$in_carico = $ultima_cosa['in_carico'];
					
					if(is_numeric($in_carico)) {
						$newinc = new Employee($in_carico);
						$in_carico = $newinc->firstname." ".substr($newinc->lastname, 0,1).".";
					}
					
					$tipo = $ultima_cosa['tipo'];
					if ($tipo == 4){
						$sigla = "T"; $tipo = "Ticket assistenza"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
					}
					else if ($tipo == 2){
						$sigla = "A"; $tipo = "Ticket contabilita"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
					} 
					else if ($tipo == 3){
						$sigla = "V"; $tipo = "Ticket rivenditori"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
					} 
					else if ($tipo == 7){
						$sigla = "M"; $tipo = "Messaggio"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewmessage&id_mex='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=7">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
						
						$ctrl_mex = Db::getInstance()->getValue("SELECT COUNT(id_customer_message) FROM customer_thread ct JOIN customer_message cm ON ct.id_customer_thread = cm.id_customer_thread WHERE cm.id_employee = 0 AND ct.id_customer_thread = ".$ultima_cosa['id']."");	
						
						if($ctrl_mex >= 1) {
							$status = ''; $scaduto = '';
						}
					}
					else if ($tipo == 8){
						$sigla = "D"; $tipo = "Ticket ordini"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
					} 
					else if ($tipo == 9){
						$sigla = "S"; $tipo = "RMA"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=6">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
					} 
					else if ($tipo == 'tirichiamiamonoi') {	
						$sigla = "R"; $tipo = "Ti richiamiamo noi"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewpreventivo&id_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=7">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM form_prevendita_message WHERE id_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
					}
					else if ($tipo == 'preventivo') {	
						$sigla = "P"; $tipo = "Preventivo (ric.)"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewpreventivo&id_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=7">';
						$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM form_prevendita_message WHERE id_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
					}
					else {
						if($tipo == 'Carrello') {
							$ultima_mod_imp = Db::getInstance()->getValue('SELECT date_upd FROM carrelli_creati WHERE id_cart = '.$ultima_cosa['id']);
							/*if($ultima_mod_imp == '' || !$ultima_mod_imp)
								$ultima_mod_imp = $ultima_cosa['date_upd'];*/
							
							$ultima_cosa['date_upd'] = $ultima_mod_imp;
			
							$revisioni = Db::getInstance()->getValue("SELECT revisioni FROM cart WHERE id_cart = ".$ultima_cosa['id']."");
							$ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcart&id_cart='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=4">'; $creato_da = Db::getInstance()->getValue("SELECT created_by FROM cart WHERE id_cart = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
							$in_carico = $ultima_cosa['in_carico'];
							
							
							$n_ord_c = Db::getInstance()->getValue("SELECT id_order FROM orders WHERE id_cart = ".$ultima_cosa['id']."");
							if($n_ord_c > 0) 
							{
								$conv = "<img src='https://www.ezdirect.it/img/admin/enabled.gif' alt='Si' title='Si' />";
							}
							else
							{
								$conv = "<img src='https://www.ezdirect.it/img/admin/red_no.gif' alt='No' title='No' />";
								
							}
					
							if(is_numeric($in_carico)) {
								$newinc = new Employee($in_carico);
								$in_carico = $newinc->firstname." ".substr($newinc->lastname, 0,1).".";
							}
						}
						else if ($tipo == 'Ordine') {
							
							$ord_fatt = Db::getInstance()->getRow('SELECT SQL_CACHE id_fattura, tipo FROM fattura FORCE INDEX (PRIMARY) WHERE rif_ordine LIKE "'.$ultima_cosa['id'].'"');
		
							if($ord_fatt['id_fattura'] > 0)
								$link_fatt = '<a href="https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&id_fattura='.$ord_fatt['id_fattura'].'&tab-container-1=5&viewfattura&tipo='.$ord_fatt['tipo'].'&updatefattura" target="_blank"><img src="../img/admin/enabled.gif" /> Fatt. </a>';
							else
								$link_fatt = '';
							
							$ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&vieworder&id_order='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=4">'; 
							
							$creato_da = Db::getInstance()->getValue("SELECT created_by FROM cart WHERE id_cart = (SELECT id_cart FROM orders WHERE id_order = ".$ultima_cosa['id'].") ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
							
						}
						else if ($tipo != 'Ordine' && $tipo != 'Carrello' && tipo != '') {
							$ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewaction&id_action='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=10">'; $creato_da = Db::getInstance()->getValue("SELECT action_from FROM action_thread WHERE id_action = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
						}
						else {
							$ahrefcosa = '<a href="#">';
						}
						
						$sigla = "";
					}
					
					$anno = substr($ultima_cosa['date_upd'],2,2);
					
					if($sigla != '') {
						$id_cosa = $sigla.$anno.$ultima_cosa['id'];
					}
					else {
						$id_cosa = $ultima_cosa['id'];
					}
					
					$status = $ultima_cosa['status']; 
					if($status == 'closed') {
						$status = '<img src="../img/admin/status_green.gif" alt="Chiuso" /> Chiuso';
					}
					else if($status == 'pending1' || $status == 'pending2') {
						$status = '<img src="../img/admin/status_orange.gif" alt="In lavorazione" />In lavorazione';
					}
					else if($status == 'open') {
						$status = '<img src="../img/admin/status_red.gif" alt="Aperto" />Aperto';
					}
					else if(is_numeric($status) && $tipo != 'Carrello') {
						
						if($status == 25 || $status == 24 || $status == 27 || $status == 28)
						{
							$status_lav = Db::getInstance()->getValue("SELECT name FROM order_state_lang WHERE id_lang = 5 AND id_order_state = ".$status."");
							
							$status = Db::getInstance()->getValue("SELECT name FROM order_state_lang WHERE id_lang = 5 AND id_order_state = ".Db::getInstance()->getValue('SELECT id_order_state FROM order_history WHERE id_order_history = '.Db::getInstance()->getValue('SELECT MAX(id_order_history) FROM order_history WHERE id_order = '.$ultima_cosa['id'].' AND id_order_state != 24 AND id_order_state != 25 AND id_order_state != 27 AND id_order_state != 28')));
							
							$status .= ' ('.$status_lav.')';
						}
						else
							$status = Db::getInstance()->getValue("SELECT name FROM order_state_lang WHERE id_lang = 5 AND id_order_state = ".$status."");
						
					}
					else if(is_numeric($status) && $tipo == 'Carrello') {
						$numero_notifiche =  Db::getInstance()->getValue("SELECT numero_notifiche FROM cart WHERE id_cart = ".$ultima_cosa['id']);
						$status_s = ($status == 1 ? '<img src="../img/admin/enabled.gif" alt="Letto" title="Letto" />Letto' : '<img src="../img/admin/red_no.gif" alt="Non letto" title="Non letto" />Non letto');
						
						if($numero_notifiche > 0)
							$status_s .= '&nbsp;&nbsp;&nbsp;<img src="../img/admin/email-sent.gif" alt="Carrello notificato al cliente" title="Carrello notificato al cliente" />';
						
						$status = $status_s;
						$preventivo = Db::getInstance()->getValue("SELECT preventivo FROM cart WHERE id_cart = ".$ultima_cosa['id']."");
						if($preventivo == 0)
							$status = '--';
						else
							$tipo = 'C.Preventivo';
					}
					
					if($tipo == 'Carrello' || $tipo == 'Preventivo' || $tipo == 'C.Preventivo') {
						$oggetto_uc = Db::getInstance()->getValue("SELECT name FROM cart WHERE id_cart = ".$ultima_cosa['id']."");
						$cartID = new Cart((int)($ultima_cosa['id']));
						$importo_uc = Tools::displayPrice($cartID->getOrderTotal(false),1);
						$is_order = Db::getInstance()->getValue("SELECT id_cart FROM orders WHERE id_cart = ".$ultima_cosa['id']."");
						if($is_order > 0) 
						{
							$rowOrder =  Db::getInstance()->getRow('SELECT * FROM orders WHERE id_cart = '.$ultima_cosa['id']);
							$importo_uc = Tools::displayPrice(($rowOrder['total_products']+($rowOrder['total_shipping']/(($rowOrder['carrier_tax_rate']/100)+1))), 1);
						}
						else
						{
							$importo_uc = Tools::displayPrice($cartID->getOrderTotal(false),1);
						}
						$id_cosa = $id_cosa."-".$revisioni;
					}
					else if($tipo == 'Ordine') {
						$rowOrder =  Db::getInstance()->getRow('SELECT * FROM orders WHERE id_order = '.$ultima_cosa['id']);
						$oggetto_uc = "";

						if($cookie->id_employee == 2)
							$importo_uc = Tools::displayPrice($rowOrder['total_paid_real']);
						else
							$importo_uc = Tools::displayPrice(($rowOrder['total_products']+($rowOrder['total_shipping']/(($rowOrder['carrier_tax_rate']/100)+1))), 1);
					}
					else {
						$oggetto_uc = "";
						$importo_uc = "";
					}
					
					
					echo "<tr id='cart_riga_".$id_cosa."' class='riga'>";
					if($tipo == 'Carrello' || $tipo == 'C.Preventivo')
					{
					echo '<script type="text/javascript">
						$(document).ready(function(){
							$("#espandi_cart_'.$id_cosa.', #td_cart_espandi_'.$id_cosa.'").click(function(){
								
								if($(this).attr("src") == ("../img/admin/add.gif")) {
									$(this).attr("src", "../img/admin/forbbiden.gif");
									$.ajax({
									  url:"ajax.php?getCartDisplay=y",
									  type: "POST",
									  data: {
									  id_cart: '.substr($id_cosa, 0, strpos($id_cosa, '-')).'
									  },
									  success:function(r){
										'.(count($ultime_cose) > 3 ? '$("#ultime-container").height($("#ultime-container").height()+250);' : '').'
										$("#cart_riga_show_'.$id_cosa.'_td").html(r);
										$("#cart_riga_show_'.$id_cosa.'").show();
										/*$(".riga").show();*/
										/*$("#orderTempDisplay").html("");*/
										/*$("#cartTempDisplay").html(r);*/
										/*$("#cart_riga_'.$id_cosa.'").toggle();*/
									  },
									  error: function(xhr,stato,errori){
										 alert("Errore durante l\'operazione:"+xhr.status);
									  }
									});
								}
								else {
									$(this).attr("src", "../img/admin/add.gif");
									$("#cart_riga_show_'.$id_cosa.'").hide();
									'.(count($ultime_cose) > 3 ? '$("#ultime-container").height(200);' : '').'
									/*$("#cartTempDisplay").html("");*/
								}
								return false;
							});
							
						});
						
						
					</script>';
					}
					else if($tipo == 'Ordine')
					{
						echo '<script type="text/javascript">
						$(document).ready(function(){
							$("#espandi_cart_'.$id_cosa.', #td_cart_espandi_'.$id_cosa.'").click(function(){
								
								if($(this).attr("src") == ("../img/admin/add.gif")) {
									$(this).attr("src", "../img/admin/forbbiden.gif");
									$.ajax({
									  url:"ajax.php?getOrderDisplay=y&ultime=y",
									  type: "POST",
									  data: {
									  id_order: '.$id_cosa.'
									  },
									  success:function(r){
										'.(count($ultime_cose) > 3 ? '$("#ultime-container").height($("#ultime-container").height()+250);' : '').'
										$("#cart_riga_show_'.$id_cosa.'_td").html(r);
										$("#cart_riga_show_'.$id_cosa.'").show();
										/*$(".riga").show();*/
										/*$("#cartTempDisplay").html("");*/
										/*$("#orderTempDisplay").html(r);*/
										/*$("#cart_riga_'.$id_cosa.'").toggle();*/
									  },
									  error: function(xhr,stato,errori){
										 alert("Errore durante l\'operazione:"+xhr.status);
									  }
									});
								}
								else {
									$(this).attr("src", "../img/admin/add.gif");
									'.(count($ultime_cose) > 3 ? '$("#ultime-container").height(200);' : '').'
									$("#cart_riga_show_'.$id_cosa.'").hide();
									/*$("#orderTempDisplay").html("");*/
								}
								return false;
							});
							
						});
						
						
					</script>';
						
					}
					echo "<td>".($tipo == 'Carrello' || $tipo == 'C.Preventivo' || $tipo == 'Ordine' ? '<img src="../img/admin/add.gif" alt="Espandi" style="cursor:pointer" title="Espandi" id="espandi_cart_'.$id_cosa.'" /> ' : '').$ahrefcosa.$tipo."</a> ".($tipo == 'Ordine' ? $link_fatt : '')."</td>";
					echo "<td>".$ahrefcosa.$id_cosa."</a></td>";
					echo "<td>".$ahrefcosa.$creato_da."</a></td>";
					echo "<td>".$ahrefcosa.$in_carico."</a></td>";
					echo "<td>".$conv."</td>";
					echo "<td>".$ahrefcosa.$status."</a></td>";
					echo "<td style='max-width:100px'>".$ahrefcosa.(strlen($oggetto_uc) > 13 ? substr($oggetto_uc,0,13).'...' : $oggetto_uc)."</a></td>";
					echo "<td style='text-align:right'>".$ahrefcosa.$importo_uc."</a></td>";
					echo "<td>".$ahrefcosa.Tools::DisplayDate($ultima_cosa['date_add'],$cookie->id_lang,false)."</a></td>";
					echo "<td>".$ahrefcosa.Tools::DisplayDate($ultima_cosa['date_upd'],$cookie->id_lang,false)."</a></td>";
					echo "</tr>";
					if($tipo == 'Carrello' || $tipo == 'C.Preventivo' || $tipo == 'Ordine')
					{
						echo '
						<tr id="cart_riga_show_'.$id_cosa.'" class="invisible-table-row subs sub_'.$id_cosa.'" style="display:none"><td id="cart_riga_show_'.$id_cosa.'_td" colspan="11"></td></tr>
						';
					}
				}
			}
		
			//echo "</tbody></table></div><br /><div id='cartTempDisplay'></div><div id='orderTempDisplay'></div><p style='text-align:center'>".(Tools::getIsset('ultime') && Tools::getValue('ultime') == "vedi_tutte" ? "<a class='button' href='https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$customer->id."&viewcustomer&token=".$this->token."&tab-container-1=1'>Vedi solo ultime 15</a>" : "<a class='button' href='https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$customer->id."&viewcustomer&token=".$this->token."&tab-container-1=1&ultime=vedi_tutte'>Vedi tutte</a>")."</p><hr />";
			
			
			echo "</tbody></table></div><br /><div id='cartTempDisplay'></div><div id='orderTempDisplay'></div><p style='text-align:center'>".(count($ultime_cose) > 7 ? "<button class='button' id='vedi_tutte' style='display:none'>Vedi tutti</button>" : '')."</p><hr />";
			
			echo '<script type="text/javascript">
						$(document).ready(function(){
							$("#vedi_tutte").click(function(){
								
								if($(this).attr("src") == ("../img/admin/add.gif")) {
									$(this).attr("src", "../img/admin/forbbiden.gif");
									$.ajax({
									  url:"ajax.php?vedi_tutte_ultime_cose=y",
									  type: "POST",
									  data: {
										tabcontainer1: '.(Tools::getIsset('tab-container-1') ? Tools::getValue('tab-container-1') : '1').',
										ultime: "vedi_tutte", token: "'.$this->token.'",
									  id_customer: '.Tools::getValue('id_customer').'
									  },
									  success:function(r){
										$("#table-ultime-cose").html(r);
										/*$(".riga").show();*/
										/*$("#cartTempDisplay").html("");*/
										/*$("#orderTempDisplay").html(r);*/
										/*$("#cart_riga_'.$id_cosa.'").toggle();*/
									  },
									  error: function(xhr,stato,errori){
										 alert("Errore durante l\'operazione:"+xhr.status);
									  }
									});
								}
								else {
									$(this).attr("src", "../img/admin/add.gif");
									'.(count($ultime_cose) > 3 ? '$("#ultime-container").height(200);' : '').'
									$("#cart_riga_show_'.$id_cosa.'").hide();
									/*$("#orderTempDisplay").html("");*/
								}
								return false;
							});
							
						});
						
						
					</script>';
			}
			/*echo '<h2><a>'.$this->l('Anagrafica').' </a></h2>';*/
			
			if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 1 || !isset($_GET['tab-container-1'])) {
			$this->displayForm();
	
		
		echo '<br />
			
		
		</fieldset>
		';
		
		
		echo '</div>
		';
			}
		else { }
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 2) {
		
			if (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== false)
			{
				flush();
			}
		
			if(!isset($_GET['newaddress'])) {

				echo '<div class="tab1" id="addresses"><h2><a>'.$this->l('Indirizzo di fatturazione').' </a></h2>';
				if (sizeof($addresses_fatt))
				{
					echo '
					<table cellspacing="0" cellpadding="0" class="table" style="font-size:12px">
						<tr>
							<th style="width:100px; font-size:13px">'.$this->l('Company').'</th>
							<th style="width:150px; font-size:13px">'.$this->l('Name').'</th>
							<th style="width:150px; font-size:13px">'.$this->l('Address').'</th>
							<th style="width:50px; font-size:13px">'.$this->l('CAP').'</th>
							<th style="width:100px; font-size:13px">'.$this->l('Citta').'</th>
							<th style="width:40px; font-size:13px">'.$this->l('Prov.').'</th>
							<th style="width:100px; font-size:13px">'.$this->l('Nazione').'</th>
							<th style="width:100px; font-size:13px">'.$this->l('Phone number(s)').'</th>
							<th style="width:50px; font-size:13px">'.$this->l('Tipo').'</th>
							<th style="width:50px; font-size:13px">'.$this->l('Actions').'</th>
						</tr>';
					
					foreach ($addresses_fatt AS $address) {
					
					if($address['fatturazione'] == 1) { $tipoind = "<strong>Fatturazione</strong>"; } else { $tipoind = "Consegna"; }
						echo '
						<tr '.($irow++ % 2 ? 'class="alt_row"' : '').'>
							<td style="font-size:13px"><b>'.($address['company'] ? $address['company'] : '--').'</b></td>
							<td style="font-size:13px">'.$address['firstname'].' '.$address['lastname'].'</td>
							<td style="font-size:13px">'.$address['address1'].($address['address2'] ? ' '.$address['address2'] : '').'</td>
							<td style="font-size:13px"> '.$address['postcode'].'</td>
							<td style="font-size:13px">'.$address['city'].'</td>
							<td style="font-size:13px">'.Db::getInstance()->getValue('SELECT iso_code FROM state WHERE id_state = '.$address['id_state']).'</td>
							<td style="font-size:13px">'.$address['country'].'</td>
							<td style="font-size:13px">'.($address['phone'] ? ($address['phone'].($address['phone_mobile'] ? '<br />'.$address['phone_mobile'] : '')) : ($address['phone_mobile'] ? $address['phone_mobile'] : '--')).'</td>
							<td>'.$tipoind.'</td>
							<td align="center">
								'.(($cookie->profile == 7 && $agente_c == $cookie->id_employee) || $cookie->profile != 7 ? '<a style="cursor:pointer" href="index.php?tab=AdminCustomers&viewcart&id_customer='.$customer->id.'&viewcustomer&id_address='.$address['id_address'].'&token='.$this->token.'&tab-container-1=2&newaddress&addaddress&fatturazione=1&idclientee='.$customer->id.'" ><img src="../img/admin/edit.gif" /></a>
								'.($cookie->profile != 8 ? '<a href="?tab=AdminAddresses&id_address='.$address['id_address'].'&deleteaddress&cancellind=1&back='.urlencode($_SERVER['REQUEST_URI']).'&token='.$tokenAddresses.'"><img src="../img/admin/delete.gif" /></a>' : '') : '--').'
							</td>
						</tr>';
						}
					echo '
					</table>';
					echo ($customer->id_default_group == 5 || sizeof($addresses_fatt) == 0 ? '<br />'.(($cookie->profile == 7 && $agente_c == $cookie->id_employee) || $cookie->profile != 7 ? '<a style="cursor:pointer" href="index.php?tab=AdminCustomers&viewcart&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=2&newaddress&addaddress&fatturazione=1&idclientee='.$customer->id.'" class="button"><img src="../img/admin/return.gif" alt="Fatturazione" title="Fatturazione" />&nbsp;&nbsp;&nbsp;Aggiungi un indirizzo di fatturazione</a>' : '') : '');
					
				}
				else {
					echo $customer->firstname.' '.$customer->lastname.' '.$this->l('has not registered any addresses yet').'.<br />';
					echo '<br /><a style="cursor:pointer" href="index.php?tab=AdminAddresses&newaddress&addaddress&fatturazione=1&idclientee='.$customer->id.'&token='.$tokenAddresses.'" class="button"><img src="../img/admin/return.gif" alt="Fatturazione" title="Fatturazione" />&nbsp;&nbsp;&nbsp;Aggiungi un indirizzo di fatturazione</a>';
				}	
				echo '<br /><br /><br />';	
					
					
					echo '
				<h2><a name="addresses">'.$this->l('Indirizzi di consegna').' ('.sizeof($addresses_cons).')</a></h2>';
				if (sizeof($addresses_cons))
				{
					echo '
					<table cellspacing="0" cellpadding="0" class="table" style="font-size:12px">
						<tr>
							<th style="width:100px; font-size:13px">'.$this->l('Company').'</th>
							<th style="width:150px; font-size:13px">'.$this->l('Name').'</th>
							<th style="width:150px; font-size:13px">'.$this->l('Address').'</th>
							<th style="width:50px; font-size:13px">'.$this->l('CAP').'</th>
							<th style="width:100px; font-size:13px">'.$this->l('Citta').'</th>
							<th style="width:40px; font-size:13px">'.$this->l('Prov.').'</th>
							<th style="width:100px; font-size:13px">'.$this->l('Nazione').'</th>
							<th style="width:100px; font-size:13px">'.$this->l('Phone number(s)').'</th>
							<th style="width:50px; font-size:13px">'.$this->l('Tipo').'</th>
							<th style="width:50px; font-size:13px">'.$this->l('Actions').'</th>
						</tr>';
					$tokenAddresses = Tools::getAdminToken('AdminAddresses'.(int)(Tab::getIdFromClassName('AdminAddresses')).(int)($cookie->id_employee));
					foreach ($addresses_cons AS $address) {
					
					if($address['fatturazione'] == 1) { $tipoind = "<strong>Fatturazione</strong>"; } else { $tipoind = "Consegna"; }
						echo '
						<tr '.($irow++ % 2 ? 'class="alt_row"' : '').'>
							<td style="font-size:13px"><b>'.($address['company'] ? $address['company'] : '--').'</b></td>
							<td style="font-size:13px">'.$address['firstname'].' '.$address['lastname'].'</td>
							<td style="font-size:13px">'.$address['address1'].($address['address2'] ? ' '.$address['address2'] : '').'</td>
							<td style="font-size:13px"> '.$address['postcode'].'</td>
							<td style="font-size:13px">'.$address['city'].'</td>
							<td style="font-size:13px">'.Db::getInstance()->getValue('SELECT iso_code FROM state WHERE id_state = '.$address['id_state']).'</td>
							<td style="font-size:13px">'.$address['country'].'</td>
							<td style="font-size:13px">'.($address['phone'] ? ($address['phone'].($address['phone_mobile'] ? '<br />'.$address['phone_mobile'] : '')) : ($address['phone_mobile'] ? $address['phone_mobile'] : '--')).'</td>
							<td>'.$tipoind.'</td>
							<td align="center">
								
								'.(($cookie->profile == 7 && $agente_c == $cookie->id_employee) || $cookie->profile != 7 ? '<a style="cursor:pointer" href="index.php?tab=AdminCustomers&viewcart&id_customer='.$customer->id.'&viewcustomer&id_address='.$address['id_address'].'&token='.$this->token.'&tab-container-1=2&newaddress&addaddress&consegna=1&idclientee='.$customer->id.'" ><img src="../img/admin/edit.gif" /></a>
								
								'.($cookie->profile != 8 ? '<a href="?tab=AdminAddresses&id_address='.$address['id_address'].'&deleteaddress&cancellind=1&back='.urlencode($_SERVER['REQUEST_URI']).'&token='.$tokenAddresses.'"><img src="../img/admin/delete.gif" /></a>' : '') : '--').'
							</td>
						</tr>';
						}
					echo '
					</table>';
				
				}
				else {
					echo $customer->firstname.' '.$customer->lastname.' '.$this->l('has not registered any addresses yet').'.<br />';
				}	
				/*echo '<br /><a style="cursor:pointer" onclick="window.open(\'?tab=AdminAddresses&addaddress&idclientee='.$customer->id.'&back='.urlencode($_SERVER['REQUEST_URI']).'&token='.$tokenAddresses.'\', \'blank\', \'height=600,width=1200,modal=yes,alwaysRaised=yes,scrollbars=yes\');" class="button">Aggiungi un indirizzo di consegna</a><br /><br /><br />';*/
				
				echo '<br />'.(($cookie->profile == 7 && $agente_c == $cookie->id_employee) || $cookie->profile != 7 ? '<a style="cursor:pointer" href="index.php?tab=AdminAddresses&newaddress&addaddress&consegna=1&idclientee='.$customer->id.'&token='.$tokenAddresses.'" class="button"><img src="../img/admin/delivery.gif" alt="Consegna" title="Consegna" />&nbsp;&nbsp;&nbsp;Aggiungi un indirizzo di consegna</a>' : '').'<br /><br /><br />';

			

				echo '</div>
				<div class="clear"></div>';
			}
			else if(isset($_GET['newaddress'])) {
				include("AdminAddresses.php");
				
				$adminAddresses = new AdminAddresses();
			
				$adminAddresses->displayForm();
				
				//echo '<br /><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4" class="button"><img src="../img/admin/arrow-left.png" alt="Back" title="Back" />&nbsp;&nbsp;&nbsp;Torna alla lista degli ordini</a>';
			
			
			}
		}
		else {
		}
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 3) {
		echo '<div class="tab1" id="amministrazione"><h2><a>'.$this->l('Amministrazione').' </a></h2>';
		
		// INIZIO AMMINISTRAZIONE //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		

		
		if(isset($_GET['submitAmministrazione'])) {
		
			$count_amm = Db::getInstance()->getValue("SELECT count(id_customer) total FROM customer_amministrazione WHERE id_customer = ".Tools::getValue('id_customer')."");
			
			$dati_fido = Db::getInstance()->getRow("SELECT fido, data_fido FROM customer_amministrazione WHERE id_customer = ".Tools::getValue('id_customer')."");
		
			
			$fido_a = (float)(str_replace(",",".",$dati_fido['fido']));
			$fido_i = (float)(str_replace(",",".",(str_replace('.','',$_POST['fido']))));
				
			if($fido_a != $fido_i && $fido_i > 0)
			{	
				$data_fido_new = date('Y-m-d H:i:s');
			}
			
			if(Tools::getValue('data_fido') != '')
				$data_fido_new = date('Y-m-d H:i:s',strtotime(Tools::getValue('data_fido')));
			
			if($count_amm == 0) {
				Db::getInstance()->executeS("INSERT INTO customer_amministrazione (id_customer, tipo_soggetto, codice_fornitore, pagamento, pagamento_2, fido, assicurato, data_fido, no_addebito_commissioni, zona, trasporto_a_mezzo, consegna, vettore, id_corriere, trasporto_gratis_da, fermo_deposito, trasporto_assicurato, note_consegna, iban, swift, paypal, iva_agevolata, esportatore_abituale, sconto_extra, per_ordini_da, fino_a, data_rinnovo, data_revoca, motivo_revoca, rebate, fatturato_annuo, blacklist, agente, tecnico, installatore) VALUES ('".Tools::getValue('id_customer')."', '".Tools::getValue('tipo_soggetto')."', '".Tools::getValue('codice_fornitore')."', '".Tools::getValue('pagamento')."', '".Tools::getValue('pagamento_2')."', '".(str_replace('.','',Tools::getValue('fido')))."', '".Tools::getValue('assicurato')."', '".$data_fido_new."', '".(Tools::getIsset('no_addebito_commissioni') ? 1 : 0)."', '".Tools::getValue('zona')."', '".Tools::getValue('trasporto_a_mezzo')."', '".Tools::getValue('consegna')."', '".Tools::getValue('vettore')."', '".Tools::getValue('id_corriere')."', '".Tools::getValue('trasporto_gratis_da')."', '".Tools::getValue('fermo_deposito')."', '".Tools::getValue('trasporto_assicurato')."', '".Tools::getValue('note_consegna')."', '".Tools::getValue('iban')."', '".Tools::getValue('swift')."', '".Tools::getValue('paypal')."', '".Tools::getValue('iva_agevolata')."', '".Tools::getValue('esportatore_abituale')."', '".Tools::getValue('sconto_extra')."', '".Tools::getValue('per_ordini_da')."', '".Tools::getValue('fino_a')."',  '".date('Y-m-d H:i:s',strtotime(Tools::getValue('data_rinnovo')))."', '".date('Y-m-d H:i:s',strtotime(Tools::getValue('data_revoca')))."', '".addslashes(Tools::getValue('motivo_revoca'))."', '".Tools::getValue('rebate')."', '".Tools::getValue('fatturato_annuo')."', '".Tools::getValue('blacklist')."', '".Tools::getValue('agente')."',  '".Tools::getValue('tecnico')."', '".Tools::getValue('installatore')."')");
			}
			else {
				Db::getInstance()->executeS("UPDATE customer_amministrazione SET tipo_soggetto = '$_POST[tipo_soggetto]', codice_fornitore = '$_POST[codice_fornitore]', pagamento = '$_POST[pagamento]', pagamento_2 = '$_POST[pagamento_2]', fido = '".(str_replace('.','',$_POST['fido']))."', assicurato = '$_POST[assicurato]', data_fido = '$data_fido_new', no_addebito_commissioni = ".(Tools::getIsset('no_addebito_commissioni') ? 1 : 0).", zona = '$_POST[zona]', trasporto_a_mezzo = '$_POST[trasporto_a_mezzo]', consegna = '$_POST[consegna]', vettore = '$_POST[vettore]', id_corriere = '$_POST[id_corriere]', trasporto_gratis_da = '$_POST[trasporto_gratis_da]', fermo_deposito = '$_POST[fermo_deposito]', trasporto_assicurato = '$_POST[trasporto_assicurato]', note_consegna = '$_POST[note_consegna]', iban = '$_POST[iban]', swift = '$_POST[swift]', paypal = '$_POST[paypal]', iva_agevolata = '$_POST[iva_agevolata]', esportatore_abituale = '$_POST[esportare_abituale]', sconto_extra = '$_POST[sconto_extra]', per_ordini_da = '$_POST[per_ordini_da]', fino_a = '$_POST[fino_a]', rebate = '$_POST[rebate]', fatturato_annuo = '$_POST[fatturato_annuo]', data_rinnovo = '".date('Y-m-d H:i:s',strtotime(Tools::getValue('data_rinnovo')))."', data_revoca = '".date('Y-m-d H:i:s',strtotime(Tools::getValue('data_revoca')))."', motivo_revoca = '$_POST[motivo_revoca]',  blacklist = '$_POST[blacklist]', agente = '$_POST[agente]', tecnico = '$_POST[tecnico]', installatore = '$_POST[installatore]' WHERE id_customer = '$_POST[id_customer]'");
			}
			
			$blacklist = Db::getInstance()->getValue('SELECT blacklist FROM customer_amministrazione WHERE id_customer = '.Tools::getValue('id_customer'));
			
			if($blacklist == 1)
			{
				echo '<script type="text/javascript">
					$(document).ready(function(){
						 
							swal({
							  type: "error",
							  title: "ATTENZIONE",
							  text: "Questo cliente è in blacklist!",
							  footer: ""
							})
					});
				</script>';
			}
			
		}
		
		$rowamm = Db::getInstance()->getRow("SELECT * FROM customer_amministrazione WHERE id_customer = $_GET[id_customer]");
		echo '<fieldset><legend><img src="../img/admin/tab-customers.gif" />'.$this->l('Amministrazione').'</legend>';
		if(!isset($_GET['modificheabilitate_amm'])) {
		echo "
		<script type='text/javascript'>
		$(document).ready(function(){
			$('#modificaamministrazione :input').attr('readonly', 'readonly');
			
			$('#modificaamministrazione :checkbox, #modificaamministrazione :radio').click(function(e) {
				e.preventDefault();
			});
			
			$('#modificaamministrazione select').attr('disabled', 'disabled');
		});
		// end
		</script>
		";
		echo '<form id="modificaamministrazione" action="'.$currentIndex.'&id_customer='.$_GET['id_customer'].'&viewcustomer&modificheabilitate_amm&token='.$this->token.'&tab-container-1=3" method="post" autocomplete="off">';
		}
		else {
		echo '
		<form id="modificaamministrazione" action="'.$currentIndex.'&id_customer='.$_GET['id_customer'].'&submitAmministrazione&viewcustomer&token='.$this->token.'&tab-container-1=3" method="post" autocomplete="off">';
		}
		
		
	echo '
			';
	
			//	if ($this->getFieldValue($obj, 'is_company') == 1) {
				
				$acquisti_totali = Db::getInstance()->getValue('SELECT SUM(imponibile) AS totale_acquisti FROM (SELECT * FROM fattura WHERE id_customer = '.$customer->id.' GROUP BY id_fattura) f');
			
				$acquisti_ultimo_anno = Db::getInstance()->getValue('SELECT SUM(imponibile) AS ultimo_anno FROM (SELECT * FROM fattura WHERE id_customer = '.$customer->id.' AND data_fattura > "'.date("Y").'-01-01 00:00:00" AND data_fattura < "'.date("Y").'-12-31 00:00:00" GROUP BY id_fattura) f');

				echo '<div class="anagrafica-cliente3" style="font-size:14px">
				'.$this->l('Fatturato annuo').'<br />
			
					<strong style="font-size:20px">'.Tools::displayPrice($acquisti_ultimo_anno,1).'</strong>
					</div>
				';
				
				echo '<div class="anagrafica-cliente3" style="font-size:14px">
				'.$this->l('Fatturato totale').'<br />
			
					<strong style="font-size:20px">'.Tools::displayPrice($acquisti_totali,1).'</strong>
					</div>
				';
				
				
				// FIDO RIVENDITORE
				
				if($customer->id_default_group == 3 || $customer->id_default_group == 15 || $customer->id_default_group == 22){
				
					/*$acquisti_tre_mesi = Db::getInstance()->getValue('SELECT SUM(imponibile) AS tre_mesi FROM (SELECT * FROM fattura WHERE id_customer = '.$customer->id.' AND data_fattura > (CURRENT_DATE() - INTERVAL 3 MONTH) AND data_fattura < CURRENT_DATE() GROUP BY id_fattura) f');*/
					
					$acquisti_tre_mesi = Db::getInstance()->getValue('SELECT SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) AS netto FROM order_detail JOIN orders ON orders.id_order = order_detail.id_order LEFT JOIN order_history oh ON oh.id_order_state =6 AND oh.id_order = orders.id_order WHERE oh.id_order_state IS NULL  AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment"))) AND orders.id_customer = '.$customer->id.' AND orders.date_add > (CURRENT_DATE() - INTERVAL 3 MONTH) AND orders.date_add < CURRENT_DATE()');
					
					$acquisti_mille = Db::getInstance()->ExecuteS('SELECT SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) AS netto FROM order_detail JOIN orders ON orders.id_order = order_detail.id_order LEFT JOIN order_history oh ON oh.id_order_state =6 AND oh.id_order = orders.id_order WHERE oh.id_order_state IS NULL  AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment"))) AND orders.id_customer = '.$customer->id.' AND orders.date_add > (CURRENT_DATE() - INTERVAL 3 MONTH) AND orders.date_add < CURRENT_DATE() GROUP BY orders.id_order');
					
					$ordine_mille = 'No';
					
					foreach($acquisti_mille as $mille){
						if($mille['netto'] >= 1000){
							$ordine_mille = 'Sì';
							//$ordine_mille_netto = $mille['netto'];
							break;
						}
						//else
							//$ordine_mille = 'No';
					}
					
					if($acquisti_tre_mesi >= 1500 || $ordine_mille == 'Sì')
						$fido_ok = 1; // per ora inutilizzata
					else
						$fido_ok = 0;
					
					echo '<div style="font-size: 16px; font-weight: bold; text-align: center; left: -8%; position: relative;">Requisiti per richiesta fido: ';
					if($fido_ok) echo '<span style="color: green;">OK</span>';
					else echo '<span style="color: red;">NO</span>';
					echo '<p style="font-size: 12px;font-weight: normal;">almeno 1.500€ di ordini o 1 ordine da 1.000€ netti negli ultimi 3 mesi</p></div>';
					
					echo '<div class="anagrafica-cliente3" style="font-size:14px; padding:2px; text-align:center;';
					if($acquisti_tre_mesi >= 1500) echo 'border:2px solid green;">';
					else echo 'border:2px solid red;">';
					echo' '.$this->l('Netto ultimi 3 mesi').'<br />
				
						<strong style="font-size:20px">'.Tools::displayPrice($acquisti_tre_mesi,1).'</strong>
						</div>
					';
					
					echo '<div class="anagrafica-cliente3" style="font-size:14px; padding:2px; text-align:center;';
					if($ordine_mille == 'Sì') echo 'border:2px solid green;">';
					else echo 'border:2px solid red;">';
					echo' '.$this->l('Ordine > 1000 Euro?').'<br />
				
						<strong style="font-size:20px">'.$ordine_mille./*' ('.Tools::displayPrice($ordine_mille_netto,1).')'.*/'</strong>
						</div>
					';
				
				}
				
				// FINE FIDO RIVENDITORE
				
				
				echo '<div class="clear"></div>';
				
				echo '<div class="anagrafica-cliente3">
				'.$this->l('Codice web:').'<br />
			
					<input type="text" size="25" name="id_customer" value="'.htmlentities(Tools::getValore($rowamm, 'id_customer'), ENT_COMPAT, 'UTF-8').'" readonly="readonly" /> 
					</div>
				';

				
			echo '<div class="anagrafica-cliente3">
				'.$this->l('Tipo soggetto:').'<br />
			<select name="tipo_soggetto">';
			echo '<option value="3"'.(Tools::getValore($rowamm, 'tipo_soggetto') == 3 ? ' selected="selected"' : '').'>Persona fisica</option>';
			echo '<option value="1"'.(Tools::getValore($rowamm, 'tipo_soggetto') == 1 ? ' selected="selected"' : '').'>Societ&agrave; di capitali</option>';
			echo '<option value="2"'.(Tools::getValore($rowamm, 'tipo_soggetto') == 2 ? ' selected="selected"' : '').'>Societ&agrave; di persone</option>';
			echo '<option value="4"'.(Tools::getValore($rowamm, 'tipo_soggetto') == 4 ? ' selected="selected"' : '').'>Altro</option>';
			echo '
					</select>
					</div>
				';
				
				echo '<div class="anagrafica-cliente3">
				'.$this->l('Codice fornitore:').'<br />
			
					<input type="text" size="25" name="codice_fornitore" value="'.htmlentities(Tools::getValore($rowamm, 'codice_fornitore'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>
				';
				
				
				
				
				
				
				
								echo '<div class="anagrafica-cliente3">
				'.$this->l('Pagamento:').'<br />
			<select name="pagamento" style="width:145px">';
			echo '<option value="Bonifico Bancario"'.(Tools::getValore($rowamm, 'pagamento') == 'Bonifico Bancario' || Tools::getValore($rowamm, 'pagamento') == 'Bonifico anticipato' ? ' selected="selected"' : '').'>Bonifico anticipato</option>';
			echo '<option value="GestPay"'.(Tools::getValore($rowamm, 'pagamento') == 'GestPay' ? ' selected="selected"' : '').'>Carta di credito (gestpay)</option>';
			echo "<option value='Carta Amazon' ".(Tools::getValore($rowamm, 'pagamento') == "Carta Amazon" || Tools::getValore($rowamm, 'pagamento') == "Amazon" ? "selected='selected'" : "").">Carta Amazon</option>";
			echo "<option value='Carta ePrice' ".(Tools::getValore($rowamm, 'pagamento') == "Carta ePrice" || Tools::getValore($rowamm, 'pagamento') == "ePrice" ? "selected='selected'" : "").">Carta ePrice</option>"; 
			echo '<option value="PayPal"'.(Tools::getValore($rowamm, 'pagamento') == 'PayPal' ? ' selected="selected"' : '').'>PayPal</option>';
			echo '<option value="Contrassegno"'.(Tools::getValore($rowamm, 'pagamento') == 'Contrassegno' ? ' selected="selected"' : '').'>Contrassegno</option>';
			echo "<option value='Bonifico 30 gg. fine mese' ".(Tools::getValore($rowamm, 'pagamento') == 'Bonifico 30 gg. fine mese' ? "selected='selected'" : "").">Bonifico 30 gg. fine mese</option>
			<option value='Bonifico 60 gg. fine mese' ".(Tools::getValore($rowamm, 'pagamento') == 'Bonifico 60 gg. fine mese' ? "selected='selected'" : "").">Bonifico 60 gg. fine mese</option>
			<option value='Bonifico 90 gg. fine mese' ".(Tools::getValore($rowamm, 'pagamento') == 'Bonifico 90 gg. fine mese' ? "selected='selected'" : "").">Bonifico 90 gg. fine mese</option>
			<option value='Bonifico 30 gg. 15 mese successivo' ".(Tools::getValore($rowamm, 'pagamento') == 'Bonifico 30 gg. 15 mese successivo' ? "selected='selected'" : "").">Bonifico 30 gg. 15 mese successivo</option>
			<option value='R.B. 30 GG. D.F. F.M.' ".(Tools::getValore($rowamm, 'pagamento') == 'R.B. 30 GG. D.F. F.M.' ? "selected='selected'" : "").">R.B. 30 GG. D.F. F.M.</option>
			<option value='R.B. 60 GG. D.F. F.M.' ".(Tools::getValore($rowamm, 'pagamento') == 'R.B. 60 GG. D.F. F.M.' ? "selected='selected'" : "").">R.B. 60 GG. D.F. F.M.</option>
			<option value='R.B. 90 GG. D.F. F.M.' ".(Tools::getValore($rowamm, 'pagamento') == 'R.B. 90 GG. D.F. F.M.' ? "selected='selected'" : "").">R.B. 90 GG. D.F. F.M.</option>
			<option value='R.B. 30 GG. 5 mese successivo' ".(Tools::getValore($rowamm, 'pagamento') == 'R.B. 30 GG. 5 mese successivo' ? "selected='selected'" : "").">R.B. 30 GG. 5 mese successivo</option>
			<option value='R.B. 30 GG. 10 mese successivo' ".(Tools::getValore($rowamm, 'pagamento') == 'R.B. 30 GG. 10 mese successivo' ? "selected='selected'" : "").">R.B. 30 GG. 10 mese successivo</option>
			<option value='R.B. 60 GG. 5 mese successivo' ".(Tools::getValore($rowamm, 'pagamento') == 'R.B. 60 GG. 5 mese successivo' ? "selected='selected'" : "").">R.B. 60 GG. 5 mese successivo</option>
			<option value='R.B. 60 GG. 10 mese successivo' ".(Tools::getValore($rowamm, 'pagamento') == 'R.B. 60 GG. 10 mese successivo' ? "selected='selected'" : "").">R.B. 60 GG. 10 mese successivo</option>
			<option value='Renting' ".(Tools::getValore($rowamm, 'pagamento') == 'Renting' ? "selected='selected'" : "").">Renting</option>
		";
			echo '
					</select>
					</div>
				';

				$data_revoca = Db::getInstance()->getValue('SELECT data_revoca FROM customer_amministrazione WHERE id_customer = '.$customer->id);
				$data_rinnovo = Db::getInstance()->getValue('SELECT data_rinnovo FROM customer_amministrazione WHERE id_customer = '.$customer->id);
				$data_fido = Db::getInstance()->getValue('SELECT data_fido FROM customer_amministrazione WHERE id_customer = '.$customer->id);
				
				includeDatepicker3(array('data_revoca', 'data_fido', 'data_rinnovo'), true);
				
				echo "<div class='clear'></div>";
				
							echo '<div class="anagrafica-cliente3">
				'.$this->l('Data affidamento:').'<br />
			
					<input type="text" size="25" name="data_fido" id="data_fido" value="'.(((isset($data_fido) && $data_fido != '0000-00-00 00:00:00' && $data_fido != '1970-01-01 01:00:00' && $data_fido != ''&& $data_fido != '0000-00-00' && $data_fido != '1970-01-01' && $data_fido != '1942-01-01')) ? date("d-m-Y", strtotime($data_fido)) : '').'" /> 
					</div>
				';		

							echo '<div class="anagrafica-cliente3" style="width:105px">
				'.$this->l('Fido:').'<br />
			
					<input type="text" size="10" name="fido" value="'.number_format($rowamm['fido'],2,",",".").'" /> &euro;
					<input type="hidden" name="zona" value="" />
					</div>
				';				
				
				$fido = Db::getInstance()->getValue('SELECT fido FROM customer_amministrazione WHERE id_customer = '.$customer->id);
			$data_fido = Db::getInstance()->getValue('SELECT data_fido FROM customer_amministrazione WHERE id_customer = '.$customer->id);
			$fido = str_replace(".","",$fido);
			$fido = (float)(str_replace(",",".",$fido));
			
			$somma_fido = Db::getInstance()->getValue('SELECT SUM(total_paid_real-acconto) FROM orders WHERE id_customer = '.$customer->id.' AND (payment = "Bonifico 30 gg. fine mese" OR payment = "Bonifico 60 gg. fine mese" OR payment = "Bonifico 90 gg. fine mese" OR payment = "R.B. 30 GG. D.F. F.M." OR payment = "R.B. 60 GG. D.F. F.M." OR payment = "R.B. 90 GG. D.F. F.M." OR payment = "R.B. 30 GG. 5 mese successivo" OR payment = "R.B. 30 GG. 10 mese successivo" OR payment = "R.B. 60 GG. 5 mese successivo" OR payment = "R.B. 60 GG. 10 mese successivo"  OR payment LIKE "%saldo%")  AND id_order NOT IN (SELECT id_order FROM order_history WHERE id_order_state = 18 OR id_order_state = 32)  AND date_add > "'.$data_fido.'"');
			
			
			$somma_fido_32 = Db::getInstance()->getValue('SELECT SUM(total_paid_real-acconto) FROM orders o JOIN (SELECT id_order_state, id_order FROM order_history WHERE id_order_state = 32) oh ON o.id_order = oh.id_order WHERE id_customer = '.$customer->id.' AND (payment = "Bonifico 30 gg. fine mese" OR payment = "Bonifico 60 gg. fine mese" OR payment = "Bonifico 90 gg. fine mese" OR payment = "Bonifico 30 gg. 15 mese successivo" OR payment = "R.B. 30 GG. D.F. F.M." OR payment = "R.B. 60 GG. D.F. F.M." OR payment = "R.B. 90 GG. D.F. F.M." OR payment = "R.B. 30 GG. 5 mese successivo" OR payment = "R.B. 30 GG. 10 mese successivo" OR payment = "R.B. 60 GG. 5 mese successivo" OR payment = "R.B. 60 GG. 10 mese successivo"  OR payment LIKE "%saldo%")  AND id_order NOT IN (SELECT id_order FROM order_history WHERE id_order_state = 18 OR id_order_state = 32)  AND date_add > "'.$data_fido.'"');
			
			$crediti_fido = $fido - $somma_fido + $somma_fido_32;
			
			if($blacklist == 1)
				$crediti_fido = 0;
			
			if($fido == 0)
				$crediti_fido = '';
				echo '<div class="anagrafica-cliente3" style="width:105px">
				'.$this->l('Residuo:').'<br />
			
					<input type="text" size="10" name="residuo" readonly="readonly" value="'.number_format($crediti_fido,2,",",".").'" /> &euro;
					</div>
				';			
				
				echo '<div class="anagrafica-cliente3" style="width:135px; position:relative">
				'.$this->l('Assicurazione credito:').'<br />
			
					<select style="position:absolute; top:13px;  width:130px" onchange="this.nextElementSibling.value=this.value">
					<option value=""></option>
					<option value="Si">Si</option>
					<option value="No">No</option>
					<option value="Non affidabile">Non affidabile</option>
					<option value="Revocato">Revocato</option>
				</select>
				<input type="text" name="assicurato" id="assicurato" value="'.Tools::getValore($rowamm, 'assicurato').'" style="position:absolute; top:14px; left:2px; border:0px; width:102px; height:14px" /><img class="img_control" style="position:absolute; left:130px" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" title="Si = tutto il fido è assicurato con Coface<br />No o vuoto = il fido non è assicurato<br />(N) = il fido è assicurato per N euro<br />Non affidabile: richiesta non accettata da Coface" />
				
				 
					
					</div>
				';			
				
				
				
				echo '<div class="anagrafica-cliente3" style="width:85px; margin-left:20px">
				'.$this->l('Data rinnovo:').'<br />
			
					<input type="text" size="10" name="data_rinnovo" id="data_rinnovo" value="'.(((isset($data_rinnovo) && $data_rinnovo != '0000-00-00 00:00:00' && $data_rinnovo != '1970-01-01 01:00:00' && $data_rinnovo != '' && $data_rinnovo != '0000-00-00' && $data_rinnovo != '1970-01-01' && $data_rinnovo != '1942-01-01')) ? date("d-m-Y", strtotime($data_rinnovo)) : '').'" /> 
					</div>
				';	

				echo '<div class="anagrafica-cliente3" style="width:85px; margin-left:20px">
				'.$this->l('Data revoca:').'<br />
			
					<input type="text" size="10" name="data_revoca" id="data_revoca" value="'.(((isset($data_revoca) && $data_revoca != '0000-00-00 00:00:00' && $data_revoca != '1970-01-01 01:00:00' && $data_revoca != '' && $data_revoca != '0000-00-00' && $data_revoca != '1970-01-01' && $data_revoca != '1942-01-01')) ? date("d-m-Y", strtotime($data_revoca)) : '').'" /> 
					</div>
				';					
				
				echo '<div class="anagrafica-cliente3">
				'.$this->l('Motivo revoca / Non affidato:').'<br />
			
					<input type="text" size="15" name="motivo_revoca" value="'.htmlentities(Tools::getValore($rowamm, 'motivo_revoca'), ENT_COMPAT, 'UTF-8').'" /> 
					</div><div style="clear:both"></div> 
				';	
				
				
				
				echo '<div class="anagrafica-cliente3" style="width:250px; clear:both">
				'.$this->l('Non addebitare commissioni su pag. diff.:').'<br />
			
					<input type="checkbox" name="no_addebito_commissioni" '.($rowamm['no_addebito_commissioni'] == 1 ? 'checked="checked"' : '').' />
					</div>
				';			
				
											/*echo '<div class="anagrafica-cliente3">
				'.$this->l('Zona:').'<br />
			
					<select name="zona" onchange="checkDefaultGroup(this.value);">';
					
					$zone = State::getStatesByIdCountry(10);
					
				foreach ($zone as $zona)
					echo '<option value="'.(int)($zona['id_state']).'"'.($zona['id_state'] == $rowamm['zona'] ? ' selected="selected"' : '').'>'.htmlentities($zona['name'], ENT_NOQUOTES, 'utf-8').'</option>';
				echo '
					</select>
					
					</div>
				';*/
				
				
				
				echo "<div class='clear'></div>";
				
				
								echo '<div class="anagrafica-cliente3">
				'.$this->l('Trasporto a mezzo:').'<br />
			<select name="trasporto_a_mezzo">';
			echo '<option value="3"'.(Tools::getValore($rowamm, 'trasporto_a_mezzo') == '3' ? ' selected="selected"' : '').'>Vettore</option>';
			echo '<option value="1"'.(Tools::getValore($rowamm, 'trasporto_a_mezzo') == '1' ? ' selected="selected"' : '').'>Mittente</option>';
			echo '<option value="2"'.(Tools::getValore($rowamm, 'trasporto_a_mezzo') == '2' ? ' selected="selected"' : '').'>Destinatario</option>';
			echo '<option value="4"'.(Tools::getValore($rowamm, 'trasporto_a_mezzo') == '4' ? ' selected="selected"' : '').'>Ritiro in sede</option>';
			echo '
					</select>
					</div>
				';
				
				
				
								echo '<div class="anagrafica-cliente3">
				'.$this->l('Consegna (porto):').'<br />
			<select name="consegna">';
			echo '<option value="3"'.(Tools::getValore($rowamm, 'consegna') == 3 ? ' selected="selected"' : '').'>Franco Add. Fatt.</option>';
			echo '<option value="1"'.(Tools::getValore($rowamm, 'consegna') == 1 ? ' selected="selected"' : '').'>Franco</option>';
			echo '<option value="2"'.(Tools::getValore($rowamm, 'consegna') == 2 ? ' selected="selected"' : '').'>Assegnato</option>';
			
			echo '
					</select>
					</div>
				';
											echo '<div class="anagrafica-cliente3">
				'.$this->l('Vettore:').'<br />
			<select name="vettore">';
			echo '<option value="1043"'.(Tools::getValore($rowamm, 'vettore') == 1043 ? ' selected="selected"' : '').'>GLS National Express s.r.l.</option>';
			echo '<option value="1062"'.(Tools::getValore($rowamm, 'vettore') == 1062 ? ' selected="selected"' : '').'>UPS Italia s.r.l.</option>';
			echo '<option value="1245"'.(Tools::getValore($rowamm, 'vettore') == 1245 ? ' selected="selected"' : '').'>TNT Global Express spa</option>';
			echo '<option value="1269"'.(Tools::getValore($rowamm, 'vettore') == 1269 ? ' selected="selected"' : '').'>SDA Trasporti</option>';
			echo '<option value="1271"'.(Tools::getValore($rowamm, 'vettore') == 1271 ? ' selected="selected"' : '').'>DHL Trasporti</option>';
			echo '<option value="1752"'.(Tools::getValore($rowamm, 'vettore') == 1752 ? ' selected="selected"' : '').'>Ascoli Trasporti</option>';
			echo '<option value="1753"'.(Tools::getValore($rowamm, 'vettore') == 1753 ? ' selected="selected"' : '').'>Poste Trasporti</option>';
			echo '
					</select>
					</div>
				';
					
					echo '<div class="anagrafica-cliente3">
				'.$this->l('Id corriere cliente:').'<br />
			
					<input type="text" size="25" name="id_corriere" value="'.htmlentities(Tools::getValore($rowamm, 'id_corriere'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>
				';			
				
				
				echo "<div class='clear'></div>";
				
			echo '<div class="anagrafica-cliente3">
				'.$this->l('Trasporto gratis da:').'<br />
			
					<input type="text" size="25" name="trasporto_gratis_da" value="'.Tools::getValore($rowamm, 'trasporto_gratis_da').'" /> &euro;
					</div>
				';			
			
				echo '<div class="anagrafica-cliente3">
				'.$this->l('Fermo deposito:').'<br />
			
					<input type="text" size="25" name="fermo_deposito" value="'.htmlentities(Tools::getValore($rowamm, 'fermo_deposito'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>
				';	

		echo '<div class="anagrafica-cliente3">Trasporto assicurato<br />'; 
				
				echo '<input type="radio" name="trasporto_assicurato" value="1" '; if (Tools::getValore($rowamm, 'trasporto_assicurato') == 1) { echo 'checked="checked"'; } else { echo ''; } echo' /><strong>S&igrave;</strong><br />
		<input type="radio" name="trasporto_assicurato" value="0"'; if (Tools::getValore($rowamm, 'trasporto_assicurato') == 0) { echo 'checked="checked"'; } else { echo ''; } echo' /><strong>No</strong>
			';
			echo '</div>';
			
						echo '<div class="anagrafica-cliente3">
				'.$this->l('Note consegna:').'<br />
			
					<input type="text" size="25" name="note_consegna" value="'.htmlentities(Tools::getValore($rowamm, 'note_consegna'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>
				';	
			

				echo "<div class='clear'></div><hr />";
			
			
				echo '<div class="anagrafica-cliente3">
				'.$this->l('IBAN:').'<br />
			
					<input type="text" size="25" name="iban" value="'.htmlentities(Tools::getValore($rowamm, 'iban'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>
				';	
					
				echo '<div class="anagrafica-cliente3">
				'.$this->l('Swift:').'<br />
			
					<input type="text" size="25" name="swift" value="'.htmlentities(Tools::getValore($rowamm, 'swift'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>
				';	
			
				
				
				
				echo '<div class="anagrafica-cliente3">
				'.$this->l('Paypal:').'<br />
			
					<input type="text" size="25" name="paypal" value="'.htmlentities(Tools::getValore($rowamm, 'paypal'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>
				';	
				
				echo "<div class='clear'></div>";
			

								echo '<div class="anagrafica-cliente3">
				'.$this->l('IVA agevolata:').'<br />
			
					<input type="text" size="25" name="iva_agevolata" value="'.htmlentities(Tools::getValore($rowamm, 'iva_agevolata'), ENT_COMPAT, 'UTF-8').'" /> %
					</div>
				';	
			echo '<div class="anagrafica-cliente3">Esportatore abituale<br />'; 
				
				echo '<input type="radio" name="esportatore_abituale" value="1" '; if (Tools::getValore($rowamm, 'esportatore_abituale') == 1) { echo 'checked="checked"'; } else { echo ''; } echo' /><strong>S&igrave;</strong><br />
		<input type="radio" name="esportatore_abituale" value="0"'; if (Tools::getValore($rowamm, 'esportatore_abituale') == 0) { echo 'checked="checked"'; } else { echo ''; } echo' /><strong>No</strong>
			';
			echo '</div>';
			
			
					echo "<div class='clear'></div>";

								echo '<div class="anagrafica-cliente3">
				'.$this->l('Sconto extra:').'<br />
			
					<input type="text" size="25" name="sconto_extra" value="'.Tools::getValore($rowamm, 'sconto_extra').'" /> %
					</div>		';			
									echo '<div class="anagrafica-cliente3">
				'.$this->l('Per ordini da:').'<br />
			
					<input type="text" size="25" name="per_ordini_da" value="'.Tools::getValore($rowamm, 'per_ordini_da').'" /> &euro;
					</div>		';	
								echo '<div class="anagrafica-cliente3">
				'.$this->l('Fino a (data):').'<br />
			
					<input type="text" size="25" name="fino_a" value="'.htmlentities(Tools::getValore($rowamm, 'fino_a'), ENT_COMPAT, 'UTF-8').'" />
					</div>		';		
					
					echo "<div class='clear'></div>";
	
									echo '<div class="anagrafica-cliente3">
				'.$this->l('Rebate:').'<br />
			
					<input type="text" size="25" name="rebate" value="'.Tools::getValore($rowamm, 'rebate').'" /> %
					</div>		';		
					
													echo '<div class="anagrafica-cliente3">
				'.$this->l('Fatturato annuo:').'<br />
			
					<input type="text" size="25" name="fatturato_annuo" value="'.Tools::getValore($rowamm, 'fatturato_annuo').'" /> &euro;
					</div>		';		
					
					echo '<div class="anagrafica-cliente3">Blacklist<br />'; 
				
				echo '<input type="radio" name="blacklist" value="1" '; if (Tools::getValore($rowamm, 'blacklist') == 1) { echo 'checked="checked"'; } else { echo ''; } echo' /><strong>S&igrave;</strong><br />
		<input type="radio" name="blacklist" value="0"'; if (Tools::getValore($rowamm, 'blacklist') == 0) { echo 'checked="checked"'; } else { echo ''; } echo' /><strong>No</strong>
			';
			echo '</div>';
					echo "<div class='clear'></div>";
				
				
						echo '<div class="anagrafica-cliente3">
				'.$this->l('Agente:').'<br />
			
					<select name="agente">';
					
					$agenti = Db::getInstance()->executeS('SELECT * FROM employee ORDER BY firstname ASC, lastname ASC');
					
				foreach ($agenti as $agente)
					echo '<option value="'.(int)($agente['id_employee']).'"'.($agente['id_employee'] == Tools::getValore($rowamm, 'agente') ? ' selected="selected"' : '').'>'.htmlentities($agente['firstname']." ".substr($agente['lastname'],0,1).".", ENT_NOQUOTES, 'utf-8').'</option>';
				echo '
					</select>
					
					</div>
				';
				
				echo '<div class="anagrafica-cliente3">
				'.$this->l('Tecnico:').'<br />
			
					<select name="tecnico">';
					
					$tecnici = Db::getInstance()->executeS('SELECT * FROM employee WHERE id_employee = 1 OR id_employee = 3 OR id_employee = 4 OR id_employee = 5 ORDER BY firstname ASC, lastname ASC');
					
				foreach ($tecnici as $tecnico)
					echo '<option value="'.(int)($tecnico['id_employee']).'"'.($tecnico['id_employee'] == Tools::getValore($rowamm, 'tecnico') ? ' selected="selected"' : '').'>'.htmlentities($tecnico['firstname']." ".substr($tecnico['lastname'],0,1).".", ENT_NOQUOTES, 'utf-8').'</option>';
				echo '
					</select>
					
					</div>
				';
				
													echo '<div class="anagrafica-cliente3">
				'.$this->l('Installatore:').'<br />
			
					<input type="text" size="25" name="installatore" value="'.htmlentities(Tools::getValore($rowamm, 'installatore'), ENT_QUOTES, 'utf-8').'" />
					</div>		';		
				
				
				
				
				
				echo "<div class='clear'></div>";
				
				echo '<div class="margin-form">';
				
				if($cookie->profile == 7)
				{
					
				}
				else
				{
					if(!isset($_GET['modificheabilitate_amm'])) {
						echo '<button type="submit" class="button" name="abilitamodifiche">
						<img src="../img/admin/edit.gif" alt="Modifica" title="Modifica" />&nbsp;&nbsp;&nbsp;'.$this->l('Abilita modifiche').'
						</button>
						
						
						';
					}
					else {
						echo '<button type="submit" class="button" name="submitAmministrazione">
						<img src="../img/admin/save.gif" alt="Salva" title="Salva" />&nbsp;&nbsp;&nbsp;'.$this->l('   Save   ').'
						</button>';
					}
				}
				
				echo '</div>
				<div class="small"><sup>*</sup> '.$this->l('Required field').'</div>
			
		</form></fieldset>';
		
		// FINE AMMINISTRAZIONE ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		
		echo '</div>';
		} else { }
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 4) {
			if (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== false && !isset($_GET['viewcart']) && !Tools::getIsset('submitSuperUser'))
			{
				//flush();
			}
			
		echo '<div class="tab1" id="orders-customer">';
		
		if(!isset($_GET['vieworder']) && !isset($_GET['viewcart'])) {
		
		echo '<h2><a>'.$this->l('Orders').' ('.sizeof($orders).')</a></h2>';
			if ($orders AND sizeof($orders))
			{
				$totalOK = 0;
				$totalNettoOK = 0;
				$ordersOK = array();
				$ordersKO = array();
				$tokenOrders = Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$cookie->id_employee);
				foreach ($orders AS $order)
				if ($order['valid'] || (($order['module'] == 'bankwire' || $order['module'] == 'cashondeliverywithfee'  || $order['module'] == 'other_payment' && $order['id_order_state'] != 6)))
				{
					$ordersOK[] = $order;
					$totalNettoOK += ($order['total_paid_real']-$order['total_tax']);
					$totalOK += $order['total_paid_real'];
				}
				else
					$ordersKO[] = $order;
				$orderHead = '
				<table cellspacing="0" cellpadding="0" class="table float paginated">
					<tr><thead>
						<th class="center" style="width:80px">'.$this->l('N. Ordine').'</th>
						<th class="center" style="width:70px">'.$this->l('Date').'</th>
						<th class="center" style="width:200px">'.$this->l('Stato').'</th>
						<th class="center" style="width:205px" colspan="2">'.$this->l('Stato tecnico').'</th>
						<th class="center" style="width:75px">'.$this->l('Carrello').'</th>
						
						<th class="center" style="width:105px" colspan="2">'.$this->l('Payment').'</th>
						<th style="text-align:right; width:50px">'.$this->l('Tracking').'</th>
						<th style="text-align:right; width:85px">'.$this->l('Importo').'</th>
						<!-- <th class="center" style="width:110px">'.$this->l('Actions').'</th> -->
					</tr></thead><tbody>';
					$orderFoot = '</tbody></table>';
					$tokenCatalog = Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee));
					
					if ($countOK = sizeof($ordersOK))
					{
						
						echo '<strong style="display:block; margin-top:20px; color:green;font-weight:700">'.$this->l('Valid orders:').' '.$countOK.' '.$this->l('for').' '.Tools::displayPrice($totalNettoOK, new Currency($defaultCurrency)).'</strong>'.$orderHead;
						foreach ($ordersOK AS $order) {
							$ord = new Order($order['id_order']);
							$currency = new Currency($order['id_currency']);
							echo '<tr class="riga" '.($irow++ % 2 ? 'class="alt_row"' : '').' id="riga_'.$order['id_order'].'" style="cursor: pointer" onclick="document.location = \'?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&id_order='.$order['id_order'].'&vieworder&token='.$this->token.'&tab-container-1=4\';">
							
						
							<td id="td_espandi_'.$order['id_order'].'"><img src="../img/admin/add.gif" alt="Espandi" style="cursor:pointer" title="Espandi" id="espandi_'.$order['id_order'].'" /> '.$order['id_order'].'
							
							<script type="text/javascript">
								$(document).ready(function(){
									$("#espandi_'.$order['id_order'].', #td_espandi_'.$order['id_order'].'").click(function(){
										
										if($(this).attr("src") == ("../img/admin/add.gif")) {
											$(this).attr("src", "../img/admin/forbbiden.gif");
											$.ajax({
											  url:"ajax.php?getOrderDisplay=y",
											  type: "POST",
											  data: {
											  id_order: '.$order['id_order'].'
											  },
											  success:function(r){
												/* $(".riga").show(); */
												$("#order_riga_show_'.$order['id_order'].'_td").html(r);
												$("#order_riga_show_'.$order['id_order'].'").show();
												/*$("#orderTempDisplay").html(r);*/
												/* $("#riga_'.$order['id_order'].'").toggle(); */
											  },
											  error: function(xhr,stato,errori){
												 alert("Errore durante l\'operazione:"+xhr.status);
											  }
											});
										}
										else {
											$(this).attr("src", "../img/admin/add.gif");
											/*$("#orderTempDisplay").html("");*/
											$("#order_riga_show_'.$order['id_order'].'").hide();
										}
										return false;
									});
									
								});
							</script>
							
							</td>
								<td>'.Tools::displayDate($order['date_add'], (int)($cookie->id_lang), false).'</td>
								<td >'.Order::getTechState($order['id_order'], date('Y-m-d H:i:s'), 'pay').'</td>
								<td colspan="2">'.Order::getTechState($order['id_order'],  date('Y-m-d H:i:s'), 'tech').'</td>
								<td>'.$order['id_cart'].'</td>
								
								<td colspan="2">'.$order['payment'].'</td>
								<td>'.$order['shipping_number'].'</td>
								<td align="right">'.($cookie->id_employee == 2 ? $order['total_paid'] : number_format(($order['total_products']+($order['total_shipping']/(($order['carrier_tax_rate']/100)+1))), 2,",","")).'</td>
								<!-- <td align="center"><a href="?tab=AdminOrders&id_order='.$order['id_order'].'&vieworder&token='.$tokenOrders.'"><img src="../img/admin/details.gif" /></a></td> -->
							</tr>
							<tr id="order_riga_show_'.$order['id_order'].'" style="display:none"><td id="order_riga_show_'.$order['id_order'].'_td" colspan="12"></td></tr>
							';
							
							
						}
						echo $orderFoot.'';
						
						
					}
					
					echo '<div class="clear"></div>
						<div id="orderTempDisplay"></div>';
						
					if ($countKO = sizeof($ordersKO))
					{
						echo '
						
						<strong style="display:block; margin-top:20px; color:red;font-weight:700">'.$this->l('Invalid orders:').' '.$countKO.'</strong>'.$orderHead;
						foreach ($ordersKO AS $order)
							echo '
							<tr class="riga" '.($irow++ % 2 ? 'class="alt_row"' : '').' style="cursor: pointer"
							onclick="document.location = \'?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&id_order='.$order['id_order'].'&vieworder&token='.$this->token.'&tab-container-1=4\';">
								<td class="center">'.$order['id_order'].'</td>
								<td>'.Tools::displayDate($order['date_add'], (int)($cookie->id_lang), false).'</td>
								<td colspan="2">'.$order['order_state'].'</td>
								<td>--</td>
								<td align="right">'.$order['id_cart'].'</td>
								<td colspan="2">'.$order['payment'].'</td>
								<td>'.$order['shipping_number'].'</td>
								<td align="right">'.number_format($order['total_products'], 2,",","").'</td>
								
								
								<!-- <td align="center"><a href="?tab=AdminCustomers&id_customer='.$order['id_customer'].'&viewcustomer&id_order='.$order['id_order'].'&vieworder&token='.$this->token.'&tab-container-1=4"><img src="../img/admin/details.gif" /></a></td> -->
							</tr>';
						echo $orderFoot.'';
					}
			}
			else
				echo $customer->firstname.' '.$customer->lastname.' '.$this->l('has not placed any orders yet');

				echo '<div class="clear"></div>';

				echo '<br /><h2><a>'.$this->l('Carts').' ('.sizeof($carts).')</a></h2>';
				$tokenCarts = Tools::getAdminToken('AdminCarts'.(int)(Tab::getIdFromClassName('AdminCarts')).(int)($cookie->id_employee));
			
				if ($carts AND sizeof($carts))
				{
					echo '
					<table cellspacing="0" cellpadding="0" class="table paginated">
						<thead>
						<tr>
							<th class="center" style="width:80px">'.$this->l('ID').'</th>
							<th class="center" style="width:80px">'.$this->l('Date').'</th>
							<th class="center" style="width:50px">'.$this->l('Prev.?').'</th>
							<th class="center" style="width:50px">'.$this->l('Letto?').'</th>
							<th class="center" style="width:50px">'.$this->l('Conv.?').'</th>
							<th class="center" style="width:50px">'.$this->l('N.Ordine').'</th>
							<th class="center" style="width:150px; text-align:right">'.$this->l('Total').'</th>
							<th class="center" style="width:140px">'.$this->l('Oggetto').'</th>
							<th class="center" style="width:50px">'.$this->l('Creato da').'</th>
							<th class="center" style="width:50px">'.$this->l('In carico a').'</th>
							<th class="center" style="width:110px">'.$this->l('Actions').'</th>
						</tr></thead><tbody>';
					
					foreach ($carts AS $cart)
					{
						$cartI = new Cart((int)($cart['id_cart']));
						//$summaryD = $cartI->getSummaryDetails();
						$currency = new Currency((int)($cart['id_currency']));
						$carrier = new Carrier((int)($cart['id_carrier']));
						$is_preventivo = Db::getInstance()->getValue("SELECT preventivo FROM cart WHERE id_cart = ".$cart['id_cart']."");
						$n_ordine = Db::getInstance()->getValue("SELECT id_order FROM orders WHERE id_cart = ".$cart['id_cart']."");
						$visualizzato = Db::getInstance()->getValue("SELECT visualizzato FROM cart WHERE id_cart = ".$cart['id_cart']."");
						$provvisorio = Db::getInstance()->getValue("SELECT provvisorio FROM cart WHERE id_cart = ".$cart['id_cart']."");
						$visualizzato_i = $visualizzato;
						
						
						$total_price_wt = Db::getInstance()->getValue("SELECT total_products FROM cart WHERE id_cart = ".$cart['id_cart']."");
					
						if($is_preventivo == 1) 
						{
							$preventivo = "<img src='https://www.ezdirect.it/img/admin/enabled.gif' alt='Si' title='Si' />";
						}
						else
						{
							$preventivo = "<img src='https://www.ezdirect.it/img/admin/red_no.gif' alt='No' title='No' />";
							
						}
						
						if($n_ordine > 0) 
						{
							$convertito = "<img src='https://www.ezdirect.it/img/admin/enabled.gif' alt='Si' title='Si' />";
						}
						else
						{
							$convertito = "<img src='https://www.ezdirect.it/img/admin/red_no.gif' alt='No' title='No' />";
							$n_ordine = "--";
						}
						
						$nome_carrello = Db::getInstance()->getValue("SELECT name FROM cart WHERE id_cart = ".$cart['id_cart']."");
						$creato_da = Db::getInstance()->getValue("SELECT created_by FROM cart WHERE id_cart = ".$cart['id_cart']."");
						$creato_da_nome = Db::getInstance()->getValue("SELECT CONCAT(firstname,' ',LEFT(lastname, 1),'.')  FROM employee WHERE id_employee = ".$creato_da."");
						$in_carico_a = Db::getInstance()->getValue("SELECT in_carico_a FROM cart WHERE id_cart = ".$cart['id_cart']."");
						$in_carico_a_nome = Db::getInstance()->getValue("SELECT CONCAT(firstname,' ',LEFT(lastname, 1),'.') FROM employee WHERE id_employee = ".$in_carico_a."");
						if($creato_da == 0) 
							$creato_da_nome = 'Cliente';
						if($is_preventivo == 0) 
						{
							//$creato_da_nome = "--";
							$visualizzato = "--";
						}
						if($in_carico_a == 0) 
						{
							$in_carico_a_nome = "--";
						}
						
						if($is_preventivo != 0 && $visualizzato > 0) 
						{
							$visualizzato = "<img src='https://www.ezdirect.it/img/admin/enabled.gif' alt='Si' title='Si' />";
						}
						else if($is_preventivo != 0 && $visualizzato == 0) 
						{
							$visualizzato = "<img src='https://www.ezdirect.it/img/admin/red_no.gif' alt='No' title='No' />";
							
						}
						else 
						{
							$visualizzato = "<img src='https://www.ezdirect.it/img/admin/lineette.gif' alt='--' title='--' />";
						}
						
						
						switch($visualizzato_i)
						{
							case '999': $style = ';padding:3px; background-color:#000000; color:#ffffff'; break;
							case '998': $style = ';padding:3px; background-color:#ffff00; color:#000000'; break;
							default: $style = ''; break;
						}
						echo '
						<tr id="cart_riga_'.$cart['id_cart'].'" class="riga" '.($irow++ % 2 ? 'class="alt_row"' : '').' style="cursor:pointer '.$style.'">
							<td id="td_cart_espandi_'.$cart['id_cart'].'"'.($visualizzato_i == 999 ? 'style="color:#ffffff"' : '').' class="center" onclick="document.location = \'?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&id_cart='.$cart['id_cart'].'&viewcart&token='.$this->token.'&tab-container-1=4\';"><img src="../img/admin/add.gif" alt="Espandi" style="cursor:pointer" title="Espandi" id="espandi_cart_'.$cart['id_cart'].'" /> 
							
							<script type="text/javascript">
								$(document).ready(function(){
									$("#espandi_cart_'.$cart['id_cart'].', #td_cart_espandi_'.$cart['id_cart'].'").click(function(){
										
										if($(this).attr("src") == ("../img/admin/add.gif")) {
											$(this).attr("src", "../img/admin/forbbiden.gif");
											$.ajax({
											  url:"ajax.php?getCartDisplay=y",
											  type: "POST",
											  data: {
											  id_cart: '.$cart['id_cart'].'
											  },
											  success:function(r){
												/*$(".riga").show();*/
												
												$("#cart_riga_show_'.$cart['id_cart'].'_td").html(r);
												$("#cart_riga_show_'.$cart['id_cart'].'").show();
												
												/*$("#cartTempDisplay").html(r);*/
												/*$("#cart_riga_'.$cart['id_cart'].'").toggle();*/
											  },
											  error: function(xhr,stato,errori){
												 alert("Errore durante l\'operazione:"+xhr.status);
											  }
											});
										}
										else {
											$(this).attr("src", "../img/admin/add.gif");
											/*$("#cartTempDisplay").html("");*/
											$("#cart_riga_show_'.$cart['id_cart'].'").hide();
										}
										return false;
									});
									
								});
								
								
							</script>
							
							'.$cart['id_cart'].'-'.$cart['revisioni'].'</td>
							<td '.($visualizzato_i == 999 ? 'style="color:#ffffff"' : '').' onclick="document.location = \'?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&id_cart='.$cart['id_cart'].'&viewcart&token='.$this->token.'&tab-container-1=4\';">'.Tools::displayDate($cart['date_add'], (int)($cookie->id_lang), false).'</td>
							<td '.($visualizzato_i == 999 ? 'style="color:#ffffff"' : '').'>'.$preventivo.'</td>
							
							<td '.($visualizzato_i == 999 ? 'style="color:#ffffff"' : '').'>'.$visualizzato.'</td>
							<td '.($visualizzato_i == 999 ? 'style="color:#ffffff"' : '').'>'.$convertito.'</td>
							<td '.($visualizzato_i == 999 ? 'style="color:#ffffff"' : '').'>'.$n_ordine.'</td>
							
							<td '.($visualizzato_i == 999 ? 'style="color:#ffffff"' : '').' align="right" onclick="document.location = \'?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&id_cart='.$cart['id_cart'].'&viewcart&token='.$this->token.'&tab-container-1=4\';">'.number_format($total_price_wt,2,",","").'</td>
							<td '.($visualizzato_i == 999 ? 'style="color:#ffffff"' : '').' onclick="document.location = \'?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&id_cart='.$cart['id_cart'].'&viewcart&token='.$this->token.'&tab-container-1=4\';">'.$nome_carrello.'</td>
							<td '.($visualizzato_i == 999 ? 'style="color:#ffffff"' : '').' onclick="document.location = \'?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&id_cart='.$cart['id_cart'].'&viewcart&token='.$this->token.'&tab-container-1=4\';">'.$creato_da_nome.'</td>
							<td '.($visualizzato_i == 999 ? 'style="color:#ffffff"' : '').' onclick="document.location = \'?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&id_cart='.$cart['id_cart'].'&viewcart&token='.$this->token.'&tab-container-1=4\';">'.$in_carico_a_nome.'</td>
							
							
							<td '.($visualizzato_i == 999 ? 'style="color:#ffffff"' : '').' align="center">
							
							';
							
							if($n_ordine > 0 || $provvisorio == 1) {
							
							}
							
							else 
							{
								echo '<a class="button"  href="index.php?tab=AdminCarts&id_customer='.$customer->id.'&id_cart='.$cart['id_cart'].'&convertcart&token='.$tokenCarts.'" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }">Converti</a>';
							}
							
							echo '
							
							'.($cookie->profile != 8 ? '<a href="index.php?tab=AdminCarts&id_customer='.$customer->id.'&id_cart='.$cart['id_cart'].'&deleteccart&token='.$tokenCarts.'" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }"><img src="../img/admin/delete.gif" /></a>' : '').'
							
							</td>
						</tr>
						<tr id="cart_riga_show_'.$cart['id_cart'].'" style="display:none"><td  id="cart_riga_show_'.$cart['id_cart'].'_td" colspan="12"></td></tr>
						';
						
						
						
					}
					echo '
					</tbody></table><br />
					<div id="cartTempDisplay"></div><div class="clear"></div>
					';
				}
				else
					echo $this->l('No cart available').'.';
				echo '';
				
			if($cookie->id_employee != 1  && $cookie->id_employee != 2 && $cookie->id_employee != 6 && $cookie->id_employee != 7) {
			}
			else {
			/*echo '

			
				<a href="index.php?tab=AdminCustomers&viewcart&editcart&createnew&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4#modifica-carrello" class="button" onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }">Crea un nuovo carrello</a>';*/
			}
			
			echo '<form name="new_preventivo_form" style="display:block; float:left" action="index.php?tab=AdminCustomers&viewcart&preventivo=1&editcart&copy_to='.$customer->id.'&provvisorio=1&id_customer='.$customer->id.'&viewcustomer&copia_carrello_conferma=y&token='.$this->token.'&tab-container-1=4&preventivo=1&rif_prev='.(Tools::getIsset('id_thread') ? Tools::getValue('id_thread') : ( Tools::getValue('id_customer_thread') ? Tools::getValue('id_customer_thread').'T' : Tools::getValue('id_action').'A' )).'&riferimento='.Tools::getValue('seleziona_personap').'&md#modifica-carrello" method="post">';
			
			$template_carrelli = Db::getInstance()->executeS('SELECT id_cart, name FROM cart WHERE id_customer = 44431 ORDER BY name ASC');
			echo '<input type="hidden" name="copy_to" value="'.$customer->id.'" />';
			echo '<input type="hidden" name="preventivo" value="1" />';
			echo '<input type="hidden" name="copia_carrello_conferma" value="y" />';
			echo '<input type="hidden" name="rif_prev" value="'.Tools::getValue('id_thread').'" />';
			echo '<select style="display:block; float:left" name="id_cart" id="template_carrello" onchange="$(\'#link-preventivo\').attr(\'href\', \'index.php?tab=AdminCustomers&viewcart&preventivo=1&editcart&amp;copy_to='.$customer->id.'&provvisorio=1&id_customer='.$customer->id.'&viewcustomer&copia_carrello_conferma=y&id_cart=\'+this.value+\'&token='.$this->token.'&tab-container-1=4&preventivo=1&rif_prev='.(Tools::getIsset('id_thread') ? Tools::getValue('id_thread') : ( Tools::getValue('id_customer_thread') ? Tools::getValue('id_customer_thread').'T' : Tools::getValue('id_action').'A' )).'&riferimento='.Tools::getValue('seleziona_personap').'&md#modifica-carrello\');";><option value="">-- Prima selezionare template --</option>';
			foreach($template_carrelli as $template) 
			{
				echo '<option value="'.$template['id_cart'].'">'.$template['name'].'</option>';
				
			}
			echo '</select>';
		
			/*echo '
		&nbsp;&nbsp;&nbsp;<input type="submit" class="button" style="display:block;float:left;margin-right:10px"  onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }" value="NUOVO PREVENTIVO" />';
		*/
			
			
			echo '
			
			<a href="index.php?tab=AdminCustomers&viewcart&editcart&createnew&provvisorio=1&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4&preventivo=1&rif_prev='.(Tools::getIsset('id_thread') ? Tools::getValue('id_thread') : ( Tools::getValue('id_customer_thread') ? Tools::getValue('id_customer_thread').'T' : Tools::getValue('id_action').'A' )).'#modifica-carrello" style=" width:150px" class="button"  onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }" id="link-preventivo"><span class="nuovo_preventivo"><img src="../img/admin/charged_ok.gif" alt="Nuovo preventivo" title="Nuovo preventivo" /> NUOVO PREVENTIVO</span></a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
			';
			
			echo '</form>';
			
			/*if($cookie->id_employee != 1  && $cookie->id_employee != 2 && $cookie->id_employee != 6 && $cookie->id_employee != 7) {
			}
			else {
			*/
				echo '
				
				<a href="index.php?tab=AdminCustomers&viewcart&editcart&createnew&provvisorio=1&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4&preventivo=2#modifica-carrello" class="button" onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }"> <img src="https://www.ezdirect.it/img/admin/icons/ordini.gif" alt="Nuovo ordine manuale" title="Nuovo ordine manuale" /> NUOVO ORDINE MANUALE</a>
				';
			//}
			
			if($cookie->id_employee == 1 || $cookie->id_employee == 6 || $cookie->id_employee == 22  || $cookie->id_employee == 7)
				echo "<br /><br />* Per modificare i template, recarsi nell'anagrafica CARRELLI TIPO.";
			
			if($cookie->profile == 7)
			{
				
			}
			else
			{	
				echo '
						<br /><br /><h2>'.$this->l('Discounts').' ('.sizeof($discounts).')</h2>';
				if (sizeof($discounts))
				{
					echo '
					<table cellspacing="0" cellpadding="0" class="table">
						<tr>
							<th>'.$this->l('ID').'</th>
							<th>'.$this->l('Code').'</th>
							<th>'.$this->l('Type').'</th>
							<th>'.$this->l('Value').'</th>
							<th>'.$this->l('Qty available').'</th>
							<th>'.$this->l('Status').'</th>
							<th>'.$this->l('Actions').'</th>
						</tr>';
					$tokenDiscounts = Tools::getAdminToken('AdminDiscounts'.(int)(Tab::getIdFromClassName('AdminDiscounts')).(int)($cookie->id_employee));
					foreach ($discounts AS $discount)
					{
						echo '
						<tr '.($irow++ % 2 ? 'class="alt_row"' : '').'>
							<td align="center">'.$discount['id_discount'].'</td>
							<td>'.$discount['name'].'</td>
							<td>'.$discount['type'].'</td>
							<td align="right">'.$discount['value'].'</td>
							<td align="center">'.$discount['quantity_for_user'].'</td>
							<td align="center"><img src="../img/admin/'.($discount['active'] ? 'enabled.gif' : 'disabled.gif').'" alt="'.$this->l('Status').'" title="'.$this->l('Status').'" /></td>
							<td align="center">
								<a style="cursor:pointer" onclick="window.open(\'?tab=AdminDiscounts&id_discount='.$discount['id_discount'].'&adddiscount&token='.$tokenDiscounts.'\', \'blank\', \'height=600,width=1200,modal=yes,alwaysRaised=yes,scrollbars=yes\');"><img src="../img/admin/edit.gif" /></a>
								<a style="cursor:pointer" onclick="window.open(\'?tab=AdminDiscounts&id_discount='.$discount['id_discount'].'&deletediscount&token='.$tokenDiscounts.'\', \'blank\', \'height=600,width=1200,modal=yes,alwaysRaised=yes,scrollbars=yes\');"><img src="../img/admin/delete.gif" /></a>
							</td>
						</tr>';
					}
					echo '
					</table>';
					
					
				

				}
				else
					echo $customer->firstname.' '.$customer->lastname.' '.$this->l('has no discount vouchers').'.';
					
				
				// display hook specified to this page : AdminCustomers
				/*if (($hook = Module::hookExec('adminCustomers', array('id_customer' => $customer->id))) !== false)
					echo '<br /><br />'.$hook.'';
				echo '<br /><br />
				<iframe style="display:none; margin-left:-10px" src="" id="ordini-frame" width="920" height="800"></iframe>'
				
				;*/
			}	
		}
		
		else if(isset($_GET['vieworder'])) {
		
			include("AdminOrders.php");
			
			AdminOrders::viewDetails();
			
			echo '<br /><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4" class="button"><img src="../img/admin/arrow-left.png" alt="Back" title="Back" />&nbsp;&nbsp;&nbsp;Torna alla lista degli ordini</a>';
		
		}
		
		else if(isset($_GET['viewcart'])) {
		
			include("AdminCarts.php");
			
			$adminCarts = new AdminCarts();
			$adminCarts->viewDetails();
			
			//echo '<br /><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4" class="button">Torna alla lista degli ordini</a>';
		
		}
		
		
		//FINE ORDINI
		echo '</div>';
		}
		else { }
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 5) {
			
		// INIZIO FATTURE
		
		if($cookie->profile == 7)
			echo 'Non hai i permessi per visualizzare questa sezione';
		else
		{	
			$fatture = Db::getInstance()->executeS("SELECT * FROM fattura WHERE id_customer = ".Tools::getValue('id_customer')." GROUP BY id_fattura ORDER BY data_fattura DESC");
			
			echo '
			<div class="tab1" id="fatture-customer"><h2><a>'.$this->l('Fatture').' ('.sizeof($fatture).')</a></h2>';
	
		
			
			if(!isset($_GET['viewfattura'])) {
			
			echo '
				<table cellspacing="0" cellpadding="0" class="table paginated">
					<thead>
						<tr>
							<th style="width:110px">'.$this->l('ID Fattura').'</th>
							<th style="width:230px">'.$this->l('ID Storico').'</th>
							<th style="width:90px">'.$this->l('Rif. ordine Sito').'</th>
							<th style="width:90px">'.$this->l('Rif. ordine eSolver').'</th>
							<th style="width:150px">'.$this->l('Data fattura').'</th>
							<th style="width:150px">'.$this->l('Pagamento').'</th>
							<th style="width:120px">'.$this->l('Imponibile').'</th>
							<th style="width:120px">'.$this->l('Totale fattura').'</th>
							<th>Invia per mail a cliente</th>
						</tr>
					</thead>
					<tbody>';
			
			foreach($fatture as $fattura) {
			
				$datafattura = date("d/m/Y", strtotime($fattura['data_fattura']));
				echo '<tr class="riga" style="cursor:pointer">
				<td onclick="document.location = \'index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&id_fattura='.$fattura['id_fattura'].'&tab-container-1=5&viewfattura&tipo='.$fattura['tipo'].'&updatefattura\';">
				<script type="text/javascript">
					$(document).ready(function(){
						$("#espandif_'.$fattura['id_fattura'].', #td_espandif_'.$fattura['id_fattura'].'").click(function(){
							$("tr.subf_'.$fattura['id_fattura'].'").toggle();
							if($("tr.subf_'.$fattura['id_fattura'].':visible").length) {
								$(this).attr("src", "../img/admin/forbbiden.gif");	
							}
							else {
								$(this).attr("src", "../img/admin/add.gif");
								$("tr.subf_'.$fattura['id_fattura'].'").hide();
							}
							return false;
						});
						
					});
				</script>
				<img src="../img/admin/add.gif" alt="Espandi" style="cursor:pointer" title="Espandi" id="espandif_'.$fattura['id_fattura'].'" />'.$fattura['id_fattura'].'</td>
				<td onclick="document.location = \'index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&id_fattura='.$fattura['id_fattura'].'&tab-container-1=5&viewfattura&tipo='.$fattura['tipo'].'&updatefattura\';">'.$fattura['id_storico'].'</td>
				<td onclick="document.location = \'index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&id_fattura='.$fattura['id_fattura'].'&tab-container-1=5&viewfattura&tipo='.$fattura['tipo'].'&updatefattura\';">'.$fattura['rif_vs_ordine'].'</td>
				<td onclick="document.location = \'index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&id_fattura='.$fattura['id_fattura'].'&tab-container-1=5&viewfattura&tipo='.$fattura['tipo'].'&updatefattura\';">'.$fattura['rif_ordine'].'</td>
				<td onclick="document.location = \'index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&id_fattura='.$fattura['id_fattura'].'&tab-container-1=5&viewfattura&tipo='.$fattura['tipo'].'&updatefattura\';">'.$datafattura.'</td>
				<td onclick="document.location = \'index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&id_fattura='.$fattura['id_fattura'].'&tab-container-1=5&viewfattura&tipo='.$fattura['tipo'].'&updatefattura\';">'.$fattura['pagamento'].'</td>
				<td onclick="document.location = \'index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&id_fattura='.$fattura['id_fattura'].'&tab-container-1=5&viewfattura&tipo='.$fattura['tipo'].'&updatefattura\';" style="text-align:right">'.Tools::displayPrice($fattura['imponibile'], $currency).'</td>
				<td onclick="document.location = \'index.php?tab=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&id_fattura='.$fattura['id_fattura'].'&tab-container-1=5&viewfattura&tipo='.$fattura['tipo'].'&updatefattura\';" style="text-align:right">'.Tools::displayPrice($fattura['totale_fattura'], $currency).'</td>
				<td>
				
				
				
				
				<script type="text/javascript">
					$(document).ready(function(){
						 $("#invia_fattura_anagrafica_'.$fattura['id_storico'].'").on("click",function(e){
							var valid = false;
							swal({
							  title: \'Invia fattura al cliente\',
							  text: \'\',
							  type: \'info\',
							 html:
							\'Il cliente riceverà la fattura via mail con questo testo (se vuoi puoi aggiungere delle note nella cella): Gentile cliente, cliccando qui [link] puoi scaricare la tua fattura n. '.$fattura['id_fattura'].' in formato PDF.<br /><br />Email: <input id="swal-input1" class="swal2-input" value="'.$customer->email.'">\' +
							\'Note: <input id="swal-input2" class="swal2-input">\',
							  showCancelButton: true,
							  showLoaderOnConfirm: true,
							  confirmButtonColor: \'#6A9944\',
							  cancelButtonColor: \'#d33\',
							  confirmButtonText: \'Invia\',
								showLoaderOnConfirm: true,
							}).then(function (result) {
								$.ajax({
								  url:"ajax.php?id_customer='.$customer->id.'&viewcustomer&id_fattura='.$fattura['id_fattura'].'&tab-container-1=5&viewfattura&tipo='.$datiFattura['tipo'].'&updatefattura&token='.$this->token.'&inviafatturamail&mail="+$(\'#swal-input1\').val()+"&note="+$(\'#swal-input2\').val(),
								  type: "GET",
								  success:function(){
									swal({
									type: \'success\',
									title: \'Fattura inviata con successo. Un record è stato salvato nello storico\'		
									});
									location.reload(); 								
								  },
								  error: function(xhr,stato,errori){
									 alert("Errore durante l\'operazione:"+xhr.status);
								  }
								});
														
							});
							
						});
					});
				</script>
				<a href="javascript:void(0)" id="invia_fattura_anagrafica_'.$fattura['id_storico'].'"><img src="../img/admin/contact.gif" alt="Invia per mail al cliente" /></a></td>
				</tr>
				';
				
				echo '<tr class="subf_'.$fattura['id_fattura'].'" style="display:none">
				<th>Codice</th>
				<th>Nome prodotto</th>
				<th style="text-align: right">Qta</th>
				<th style="text-align: right">Unitario</th>
				<th colspan="2" style="text-align: right">Totale</th>
				
				</tr>';
				$products = Db::getInstance()->executeS('SELECT cod_articolo, desc_articolo, qt_ord, qt_sped, prezzo, importo_netto FROM fattura WHERE id_fattura = "'.$fattura['id_fattura'].'" AND id_customer = '.$fattura['id_customer']);
				$tokenCatalog = Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee));
					
					
				foreach ($products as $k => $product)
				{
					$id_prodotto = Db::getInstance()->getValue('SELECT id_product FROM product WHERE reference = "'.$product['cod_articolo'].'"');
					
					echo '<tr class="subf_'.$fattura['id_fattura'].'" style="display:none">
						<td align="left">
						'.($id_prodotto > 0 ?  '<span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($id_prodotto).'"><a href="index.php?tab=AdminCatalog&id_product='.$id_prodotto.'&updateproduct&token='.$tokenCatalog.'">'.$product['cod_articolo'].'</a></span>' : $product['cod_articolo']).'</td>
						<td><span class="productName">'.$product['desc_articolo'].'</span></td>
						<td align="right" class="productQuantity">'.(int)$product['qt_sped'].'</td>
						<td align="right">'.Tools::displayPrice($product['prezzo'], $currency, false).'</td>
						
						<td colspan="2" align="right">'.Tools::displayPrice($product['importo_netto'], $currency, false).'</td>
						
						</tr>';
					
				}
			}
			
			
			echo '</tbody></table>';
			
				
				
			} else {
				
				
				include("AdminFatture.php");
				
				AdminFatture::postProcess();
				
				//echo '<br /><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4" class="button"><img src="../img/admin/arrow-left.png" alt="Back" title="Back" />&nbsp;&nbsp;&nbsp;Torna alla lista degli ordini</a>';
		
			}
		
		
		
		
		
		
		}
		echo '</div>';
		}
		else { }
		//FINE FATTURE
		
		
		$employeemess = new Employee($cookie->id_employee);
		// INIZIO TICKET
		$gentilecliente = "Gentile cliente, <br />";
		
		$firma = '<br />
							Cordiali saluti / Best regards<br /><br />
							'.($cookie->id_employee != 19 ? $employeemess->firstname." ".$employeemess->lastname : 'Staff Ezdirect').'<br /><br />
							Ezdirect srl <a href="http://www.ezdirect.it">www.ezdirect.it</a><br /><br />

							Tel +39 0585821163 '.$interno.'<br />
							Fax +39 0585821286
							</p>';
							
		$threads = Db::getInstance()->ExecuteS("SELECT * FROM customer_thread WHERE id_contact != 7 AND id_customer = ".$customer->id." ORDER BY id_customer_thread DESC");
		
		if(isset($_POST['cambiastatust_listing'])) {

				
			Db::getInstance()->ExecuteS("UPDATE customer_thread SET status = '".Tools::getValue('cambiastatust_listing')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = '".Tools::getValue('id_customer_thread')."'");
			
			switch(Tools::getValue('cambiastatust_listing'))
			{
				case 'open': $switch_status = 3; break;
				case 'pending1': $switch_status = 4; break;
				case 'closed': $switch_status = 5; break;
				default: $switch_status = 10; break;
			}	
			
			Customer::Storico(Tools::getValue('id_customer_thread'), 'T', $cookie->id_employee, $switch_status);
			
			$type = Db::getInstance()->getValue("SELECT id_contact FROM customer_thread WHERE id_customer_thread = '".Tools::getValue('id_customer_thread')."'");
		
			Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&token='.$this->token.'&conf=4&tab-container-1='.($type == 7 ? '7': '6'));
			
		}
		
		if(isset($_POST['cambiaincaricot'])) {

			
			Customer::cambiaIncaricoVeloce(Tools::getValue('tipo'), Tools::getValue('cambiaincaricot'), Tools::getValue('id_thread'), Tools::getValue('precedente_incarico'), Tools::getValue('id_ticket_univoco'), Tools::getValue('id_customer'));
	
			$type = Db::getInstance()->getValue("SELECT id_contact FROM customer_thread WHERE id_customer_thread = '".Tools::getValue('id_thread')."'");
			
			Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&token='.$this->token.'&conf=4&tab-container-1='.($type == 7 ? '7': '6'));
			
		}
		
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 6) {
			
		if($cookie->profile == 7 && $agente_c != $cookie->id_employee)
			echo 'Non hai i permessi per visualizzare questa sezione';
		else
		{
			
			echo '<div class="tab1" id="messages-customer">';
			echo '<h2>Ticket</h2>';
			if(isset($_GET['id_customer_thread'])) {
			
				$thread = Db::getInstance()->getRow("SELECT * FROM customer_thread ct WHERE ct.id_customer_thread = ".$_GET['id_customer_thread']." AND ct.id_customer = ".$customer->id."");
				
					if(!$thread && Tools::getValue('id_customer_thread') != '') {
						echo "<span style='color:red'>ERRORE - Ticket non trovato</span><br /><br /><br />"; die();
					}
		
					$incmess = new Employee($thread['id_employee']);
					
					$tiporichiesta = Db::getInstance()->getValue("SELECT name FROM contact_lang WHERE id_lang = 5 and id_contact = ".$thread['id_contact']."");
					
					$annoticket = substr($thread['date_add'],2,2);
						if($thread['id_contact'] == 2) { $siglat = 'A'; } else if($thread['id_contact'] == 4) { $siglat = 'T'; } else if($thread['id_contact'] == 3) { $siglat = 'V'; } else if($thread['id_contact'] == 6) { $siglat = 'R'; } else if($thread['id_contact'] == 8) { $siglat = 'D'; } else if($thread['id_contact'] == 9) { $siglat = 'S'; }
						$idticketunivoco = $siglat.$annoticket.$thread['id_customer_thread'];
					
					
							switch($thread['status']) {
								case 'open': $status_thread = "<img src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; $st_thread = 'Aperto'; break;
								case 'closed': $status_thread = "<img src='../img/admin/status_green.gif' alt='Chiuso' title='Chiuso' />";  $st_thread = 'Chiuso'; break;
								case 'pending1': $status_thread = "<img src='../img/admin/status_orange.gif' alt='In lavorazione' title='In lavorazione' />";  $st_thread = 'In lavorazione'; break;
								default: $status_thread = "<img src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />";  $st_thread = 'Aperto'; break;
							}
					
					$dataultimo = Db::getInstance()->getValue("SELECT date_add FROM customer_message WHERE id_customer_thread = ".$thread['id_customer_thread']." ORDER BY date_add DESC");
					
					$oggetto = Db::getInstance()->getValue("SELECT subject FROM customer_thread WHERE id_customer_thread = ".$thread['id_customer_thread']." ORDER BY date_add ASC");
					
					if($oggetto == '') {
						$oggetto = Db::getInstance()->getValue("SELECT message FROM customer_message WHERE id_customer_thread = ".$thread['id_customer_thread']." ORDER BY date_add ASC");
					} 
					else {
					
					}
					
				echo "<h2 style='float:left; margin-right:10px'>".($thread['id_contact'] == 9 ? 'RMA n.' : 'Ticket n.')." ".$idticketunivoco." - In carico a: ".$incmess->firstname." - Status: ".$st_thread."</h2>";
			
			}
		
			echo '<div style="clear:both"></div>';
			
			
			$impiegati_ez = Db::getInstance()->ExecuteS("SELECT id_employee, firstname, lastname FROM employee WHERE active = 1 ORDER BY firstname ASC, lastname ASC");
			
			$employeemess = new Employee($cookie->id_employee);
			
			
			switch($cookie->id_employee) {
			case 1: $interno = "(201)";
			case 2: $interno = "(202)";
			case 3: $interno = "(204)";
			case 4: $interno = "(205)";
			case 5: $interno = "(203)";
			case 6: $interno = "";
			case 7: $interno = "(207)";
			default: $interno = "";
			}

			
			if(isset($_GET['error']) && $_GET['error'] == '1tk') {
				
				echo '<div style="width:100%; height:30px; border:1px solid red; color:red">Il messaggio era vuoto e non &egrave; stato inviato, ma sono stati aggiornati i campi status e in carico a.</div>';
			}
			
			
			if(isset($_POST['submitTicket'])) {
		
				$impiegato_attuale = Db::getInstance()->getValue("SELECT id_employee FROM customer_thread WHERE id_customer_thread = '".Tools::getValue('id_customer_thread')."'");
				
				$precedente_status = Db::getInstance()->getValue("SELECT status FROM customer_thread WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
					
				$guasto_tkt = Tools::getValue('guasto_tkt');
				if($guasto_tkt == 'Altro')
					$guasto_tkt .= Tools::getValue('guasto_altro_tkt');
					
				$causa_guasto_tkt = Tools::getValue('causa_guasto_tkt');
				if($causa_guasto_tkt == 'Altro')
					$causa_guasto_tkt .= Tools::getValue('causa_guasto_altro_tkt');
				
				$descrizione_tkt = Tools::getValue('descrizione_tkt');
				if($descrizione_tkt == 'Altro')
					$descrizione_tkt .= Tools::getValue('descrizione_altro_tkt');
							
				Db::getInstance()->ExecuteS("UPDATE customer_thread SET subject = '".addslashes(Tools::getValue('ticket_subject'))."', descrizione = '".addslashes($descrizione_tkt)."', motivo_chiamata = '".Tools::getValue('motivo_chiamata_tkt')."', guasto = '".addslashes($guasto_tkt)."', causa_guasto = '".addslashes($causa_guasto_tkt)."', note = '".addslashes(Tools::getValue('ticket_note'))."', status = '".Tools::getValue('cambiastatus')."', priorita = '".Tools::getValue('cambiapriorita')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = '".Tools::getValue('id_customer_thread')."'");
				
				Db::getInstance()->executeS('UPDATE customer_thread SET id_product = '.Tools::getValue('id_product').', id_order = "'.Tools::getValue('id_order').'" WHERE id_customer_thread = '.Tools::getValue('id_customer_thread'));
				switch(Tools::getValue('cambiastatus'))
				{
					case 'open': $switch_status = 3; break;
					case 'pending1': $switch_status = 4; break;
					case 'closed': $switch_status = 5; break;
					default: $switch_status = 10; break;
				}	
				
				foreach ($_POST['note_nuova'] as $id_nota=>$nota_id) {
					
					echo $_POST['note_nota'][$id_nota].'<br />';
				
					if($_POST['note_nuova'][$id_nota] == 0) {
						
						$testo_nota = Db::getInstance()->getValue('SELECT note FROM note_attivita WHERE id_note = '.$id_nota.'');
						if($testo_nota != $_POST['note_nota'][$id_nota])
							Db::getInstance()->executeS("UPDATE note_attivita SET note = '".addslashes($_POST['note_nota'][$id_nota])."', date_upd = '".date("Y-m-d H:i:s")."', id_employee = '".$cookie->id_employee."' WHERE id_note = ".$id_nota.""); 
					}
					
					else {
						if(addslashes($_POST['note_nota'][$id_nota]) != '')
						{
							Db::getInstance()->executeS("INSERT INTO note_attivita
							(id_note,
							id_attivita,
							tipo_attivita,
							id_employee,
							note,
							date_add,
							date_upd)
							
							VALUES (
							NULL,
							'".Tools::getValue('id_customer_thread')."',
							'T',
							'".$cookie->id_employee."',
							'".addslashes($_POST['note_nota'][$id_nota])."',
							'".date("Y-m-d H:i:s")."',
							'".date("Y-m-d H:i:s")."'
							)");
						}
						
					}
				}
				
				
				if($precedente_status != Tools::getValue('cambiastatus'))
					Customer::Storico(Tools::getValue('id_customer_thread'), 'T', $cookie->id_employee, $switch_status);
				
				
				if(Tools::getValue('id_contact') == 9) {
				
					Customer::Storico(Tools::getValue('id_customer_thread'), 'T', $cookie->id_employee, 'Ha aggiornato le informazioni dell\'RMA');
					
					$rma_id = Db::getInstance()->getValue("SELECT codice_rma FROM rma WHERE id_customer_thread = '".Tools::getValue('id_customer_thread')."'");
					
					$chars_per_codice = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
					for ($i = 0, $codice_rma = ''; $i < 5; $i++) {
						$codice_rma .= Tools::substr($chars_per_codice, mt_rand(0, Tools::strlen($chars_per_codice) - 1), 1);
					}
							
					if($rma_id == '')
						Db::getInstance()->executeS("INSERT INTO rma (id_customer_thread, codice_rma) VALUES (".Tools::getValue('id_customer_thread').", '".$codice_rma."')");
					
					
					$test_now = Db::getInstance()->getValue('SELECT test FROM rma WHERE id_customer_thread = '.Tools::getValue('id_customer_thread'));
					
					Db::getInstance()->ExecuteS("UPDATE rma SET id_address = '".addslashes(Tools::getValue('indirizzo_rma'))."', rma_type = '".addslashes(Tools::getValue('rma_type'))."', rma_shipping = '".addslashes(Tools::getValue('rma_shipping'))."', 
					arrivoezcli = '".(Tools::getIsset('arrivoezcli') ? 1 : 0)."',
					arrivocli =  '".date('Y-m-d H:i:s', strtotime(Tools::getValue('arrivocli')))."',
					a_carico = '".Tools::getValue('a_carico')."',
					corriere = '".Tools::getValue('corriere')."',
					richiesto_rma = '".(Tools::getIsset('richiesto_rma') ? 1 : 0)."',
					data_richiesta = '".date('Y-m-d H:i:s', strtotime(Tools::getValue('data_richiesta')))."',
					codice_rma_fornitore = '".Tools::getValue('codice_rma_fornitore')."',
					il_fornitore = '".Tools::getValue('il_fornitore')."',
					arrivoezfornitore = '".date('Y-m-d H:i:s', strtotime(Tools::getValue('arrivoezfornitore')))."',
					sostituitoriparato = '".Tools::getValue('sostituitoriparato')."',
					
					test = '".Tools::getValue('test')."',
					test_in_carico_a = '".Tools::getValue('test_in_carico_a')."',
					problematica_test = '".Tools::getValue('problematica_test')."',
					urgente = '".(Tools::getIsset('urgente') ? 1 : 0)."',
					
					qta = '".addslashes(Tools::getValue('qta'))."', seriale = '".addslashes(Tools::getValue('seriale'))."', in_garanzia = '".(Tools::getIsset('in_garanzia') ? 1 : 0)."', attivita_svolta = '".addslashes(Tools::getValue('attivita_svolta'))."', altra_attivita = '".addslashes(Tools::getValue('altra_attivita'))."', costo_materiale = '".str_replace(",", ".", addslashes(Tools::getValue('costo_materiale')))."', costo_manodopera = '".str_replace(",", ".", addslashes(Tools::getValue('costo_manodopera')))."', trasporti_qta = '".addslashes(Tools::getValue('trasporti_qta'))."', manodopera_qta = '".str_replace(",", ".", addslashes(Tools::getValue('manodopera_qta')))."', totale_trasporti = '".str_replace(",", ".", addslashes(Tools::getValue('totale_trasporti')))."' WHERE id_customer_thread = '".Tools::getValue('id_customer_thread')."'");
					
					
					if(Tools::getValue('test') == 1 && $test_now == 0)
					{
						$prodrichiesta = Db::getInstance()->getRow("SELECT reference, name FROM product JOIN product_lang ON product.id_product = product_lang.id_product WHERE id_lang = 5 and product.id_product = ".Tools::getValue('id_product').""); 
						
						$action_threadz = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
						$action_threadz++;
						$action_subjectz = "*** RMA N. ".Customer::trovaSigla(Tools::getValue('id_customer_thread'),'ticket')." - TESTARE PRODOTTO *** ";
						$id_employee = Tools::getValue('test_in_carico_a');
						
						$idcust = Db::getInstance()->getValue('SELECT id_customer FROM customer_thread WHERE id_customer_thread = '.Tools::getValue('id_customer_thread'));
						
						Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd, promemoria) VALUES ('$action_threadz', 'Attivita', '".$idcust."', '".addslashes($action_subjectz)."', 0, ".$id_employee.", 'open', 'T".Tools::getValue('id_customer_thread')."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."',1)");   

						Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_threadz', ".$idcust.", 99, ".$id_employee.", ".$id_employee.", '', 'RMA N. ".Customer::trovaSigla(Tools::getValue('id_customer_thread'),'ticket')." - TESTARE PRODOTTO ".$prodrichiesta['reference']." (".$prodrichiesta['name']."). Problematica: ".addslashes(Tools::getValue('problematica_test'))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
						
					}
					
					
				}
						
				if($impiegato_attuale != Tools::getValue('assegnaimpiegato')) {
					
					Customer::Storico(Tools::getValue('id_customer_thread'), 'T', $cookie->id_employee, 2, Tools::getValue('assegnaimpiegato'));
					
					Db::getInstance()->ExecuteS("UPDATE customer_thread SET id_employee = '".Tools::getValue('assegnaimpiegato')."' WHERE id_customer_thread = '".Tools::getValue('id_customer_thread')."'");
					
					$mailimp = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".Tools::getValue('assegnaimpiegato')."'");
							
					$tokenimp = Tools::getAdminToken("AdminCustomers"."2".Tools::getValue('assegnaimpiegato'));
							
					$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewticket&id_customer_thread=".Tools::getValue('id_customer_thread')."&token=".$tokenimp.'&tab-container-1=6';
							
					$msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato il ticket numero ".Tools::getValue('id_ticket_univoco')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirlo</a>.";
							
					if($impiegato_attuale != 0) {
								
						$mailimprev = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".$impiegato_attuale."'");
						$nomeimprev = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = '".$impiegato_attuale."'");
								
						$msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione del ticket numero ".Tools::getValue('id_ticket_univoco')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect e l'ha passata a ".Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.Tools::getValue('assegnaimpiegato').'');
								
						$params = array(
						'{msg}' => $msgimprev);
						
						if($impiegato_attuale != $cookie->id_employee)
						{			
							Mail::Send(5, 'msg_base', Mail::l('Gestione ticket revocata', 5), 
							$params, $mailimprev, NULL, NULL, NULL, NULL, NULL, 
							_PS_MAIL_DIR_, true);
						}	
					}
							
					else {
							
					}
							
							
					$params = array(
					'{msg}' => $msgimp);
							
					if($_POST['assegnaimpiegato'] != 0 && $_POST['assegnaimpiegato'] != $cookie->id_employee) {
							
						Mail::Send(5, 'msg_base', Mail::l('Ticket assegnato a te su Ezdirect', 5), 
							$params, $mailimp, NULL, NULL, NULL, NULL, NULL, 
							_PS_MAIL_DIR_, true);
										
					} else { }
					
				}
				
				$id_contact = Db::getInstance()->getValue('SELECT id_contact FROM customer_thread WHERE id_customer_thread = '.Tools::getValue('id_customer_thread'));
				if($id_contact != Tools::getValue('id_contact')) 
					Customer::convertiTicket(Tools::getValue('id_customer_thread'), $id_contact, Tools::getValue('id_contact'));
						
				Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&viewticket&id_customer_thread='.Tools::getValue('id_customer_thread').'&token='.$this->token.'&conf=4&tab-container-1=6');
			}
				

			if(isset($_GET['viewticket'])) {
				
				/*$ftt = Db::getInstance()->getValue('SELECT id_attivita FROM storico_attivita WHERE tipo_num = 1 AND id_attivita = '.Tools::getValue('id_customer_thread'));
				if((!$ftt || $ftt <= 0) && $_GET['id_customer_thread'] > 18794)*/
				if(!Tools::getIsset('conf') && !Tools::getIsset('filename'))
					Customer::Storico(Tools::getValue('id_customer_thread'), 'T', $cookie->id_employee, 14);
				
				$bdl_ticket = Db::getInstance()->getValue('SELECT id_bdl FROM bdl WHERE riferimento = "T'.Tools::getValue('id_customer_thread').'"');
			
				if(isset($_GET['id_customer_thread'])) {			
					echo '<div id="tab-container-ticket" class="tab-containers">
					<ul id="tab-container-ticket-nav" class="tab-containers-nav">
					<li><a href="#ticket-1"><strong>Generale</strong></a></li>
					<li><a href="#ticket-2"><strong>Cronologia</strong></a></li>
					<li><a href="#ticket-3"><strong>Gerarchia</strong></a></li>
					<li><a href="#ticket-4"><strong>Storico</strong></a></li>
					'.($bdl_ticket > 0 || Tools::getIsset('nuovobdl') ? '<li><a href="#ticket-5"><strong>BDL</strong></a></li>' : '').'
					</ul>';
				
			
					if(!isset($_GET['aprinuovoticket'])) {
				
						if (isset($_GET['filename'])) {
							$filename = $_GET['filename'];
							
							if(strpos($filename, ":::")) {
				
								$parti = explode(":::", $filename);
								$nomecodificato = $parti[0];
								$nomevero = $parti[1];
								
							}
				
							else {
								$nomecodificato = $filename;
								$nomevero = $filename;
				
							}
							
							if (file_exists(_PS_UPLOAD_DIR_.$nomecodificato)) {
								self::openUploadedFile();
							}
							else { }
						} 
						else { }
					
					echo '
					
					<div class="yetii-ticket" id="ticket-1">
					<fieldset style="width:95%">';
					
					
					echo '
					<form id="modificaticket" action="'.$currentIndex.'&id_customer='.$_GET['id_customer'].'&submitTicket&viewcustomer&token='.$this->token.'&tab-container-1=6" method="post" autocomplete="off">';
					
					
					
					echo '
					
					<div class="anagrafica-cliente" style="width:800px; margin-bottom:10px">
					<strong>Oggetto</strong><br />
					
						<textarea name="ticket_subject" id="ticket_subjectContent" style="width:690px;height:40px;display:block;float:left">'.strip_tags(htmlspecialchars_decode($oggetto)).'</textarea>
						<br /><br />
					</div>

					
					<div class="clear"></div>
				
					
					<div class="anagrafica-cliente" style="width:97px">
					ID Ticket<br />
					<span class="tab-span" style="width:97px">'.$idticketunivoco.'</span><br />
					</div>
					';
					
					$aperto_da = Db::getInstance()->getValue('SELECT id_employee FROM customer_message WHERE id_customer_thread = '.Tools::getValue('id_customer_thread').' ORDER BY id_customer_message ASC');
					
					if($aperto_da != 0)
						$aperto_da = Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$aperto_da);
					else
						$aperto_da = 'Cliente';
					
					echo '
					
					<div class="anagrafica-cliente" style="width:97px">
					Aperto da<br />
					<span class="tab-span" style="width:97px">'.$aperto_da.'</span><br />
					</div>
					
					<div class="anagrafica-cliente" style="width:250px">
					In carico a<br />';
						echo '
					<input type="hidden" name="id_ticket_univoco" value="'.$idticketunivoco.'" />
					<select style="width:250px" name="assegnaimpiegato">';
					
					echo '<option value="0"'.($thread['id_employee'] == 0 ? 'selected="selected"' : '').' >Da assegnare</option>';
					
					foreach ($impiegati_ez as $impez) {
					
						echo "<option value='".$impez['id_employee']."' ".($thread['id_employee'] == $impez['id_employee'] ? 'selected="selected"' : '')." >".$impez['firstname']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>";
					
					}

					echo '</select>
			
					</div>
					
						<div class="anagrafica-cliente">
					Data apertura<br />
					<span class="tab-span">'.Tools::displayDate($thread['date_add'], (int)($cookie->id_lang), true).'</span><br />
					</div>
				
					<div class="clear"></div>
					
					
					
					<div class="clear"></div>
					<div class="anagrafica-cliente" style="width:97px">
					Status<br />
					'.$status_thread.' <input type="hidden" name="id_customer" value="'.Tools::getValue('id_customer').'" />
					<input type="hidden" name="id_customer_thread" value="'.Tools::getValue('id_customer_thread').'" />

					<select name="cambiastatus" style="width:80px">
					<option value="closed" '.($thread['status'] == 'closed' ? 'selected="selected"' : '').'>Chiuso</option>
					<option value="pending1" '.($thread['status'] == 'pending1' ? 'selected="selected"' : '').'>In lavorazione</option>
					<option value="open" '.($thread['status'] == 'open' ? 'selected="selected"' : '').'>Aperto</option>
					</select><br />
					</div>
					
					<div class="anagrafica-cliente" style="width:97px">
					Urgenza<br />

					<select name="cambiapriorita" style="width:102px">
					<option value="" '.($thread['priorita'] == '' ? 'selected="selected"' : '').'>--</option>
					<option value="low" '.($thread['priorita'] == 'low' ? 'selected="selected"' : '').'>Bassa</option>
					<option value="medium" '.($thread['priorita'] == 'medium' ? 'selected="selected"' : '').'>Media</option>
					<option value="high" '.($thread['priorita'] == 'high' ? 'selected="selected"' : '').'>Alta</option>
					</select><br />
					</div>
					
					<div class="anagrafica-cliente" style="width:250px">
					Tipo ticket<br />
					';
					
					if($thread['id_contact'] == 9) {
						echo '<span class="tab-span" style="width:240px">RMA</span><br />
						<input type="hidden" name="id_contact" id="id_contact" value="9">'
						;
					}
					
					else {
					
					echo '
					<select style="width:250px" name="id_contact" id="id_contact">
											<option '.($thread['id_contact'] == 4? 'selected="selected"' : '').' value="4">Assistenza tecnica</option>
											<option '.($thread['id_contact'] == 2? 'selected="selected"' : '').' value="2">Assistenza amministrativa - contabilit&agrave;</option>
											<option '.($thread['id_contact'] == 8? 'selected="selected"' : '').' value="8">Assistenza amministrativa - ordini</option>
											<option '.($thread['id_contact'] == 3? 'selected="selected"' : '').' value="3">Rivenditori</option>
											'.($thread['id_contact'] == 9 ? '<option '.($thread['id_contact'] == 9? 'selected="selected"' : '').' value="9">RMA</option>' : '').'
											<option value="preventivo">Richiesta di preventivo</option>
											<option value="tirichiamiamonoi">Ti richiamiamo noi</option>
											<option value="7">Messaggio</option>
											</select>
							';				
					}
					echo '</div>
					
					
						<div class="anagrafica-cliente">
					Data ultima com.<br />
					<span class="tab-span">'.Tools::displayDate($dataultimo, (int)($cookie->id_lang), true).'</span><br />
					</div>';
				
					
					$invoices = Db::getInstance()->executeS('SELECT * FROM fattura WHERE id_customer = '.$customer->id.' GROUP BY id_fattura ORDER BY data_fattura');
					
					if($thread['id_contact'] == 9) {
						$dati_rma = Db::getInstance()->getRow('SELECT * FROM rma WHERE id_customer_thread = '.$thread['id_customer_thread'].'');
						
						$dati_fattura = Db::getInstance()->getRow('SELECT * FROM fattura WHERE id_riga = '.$thread['id_order'].'');
						echo '<div class="anagrafica-cliente" style="width:210px">
						Numero fattura<br />
						<select style="width:210px" name="id_order"><option name="0">--</option>';
						
						foreach($invoices as $inv1)
						{
							echo '<option name="'.$inv1['id_riga'].'" value="'.$inv1['id_riga'].'" '.($inv1['id_riga'] == $thread['id_order'] ? 'selected="selected"' : '').'>'.$inv1['id_fattura'] .' del '. date('d/m/Y',strtotime($inv1['data_fattura'])).'</option>';
						}
						echo '</select><br /></div>';
						echo '<div class="anagrafica-cliente" style="width:100px">
						Codice RMA<br />
						<span class="tab-span" style="width:100px">'.($dati_rma['codice_rma']).'</span><br />
						</div>';
						
						echo '<div class="anagrafica-cliente" style="width:130px">
						Codice seriale prodotto<br />
						<input type="text" name="seriale" value="'.($dati_rma['seriale']).'"  style="width:120px" /><br />
						</div>';
						
						echo '<div class="anagrafica-cliente" style="width:70px">
						Qta prodotto<br />
						<input type="text" name="qta" value="'.($dati_rma['qta']).'" style="width:70px" /><br />
						</div>';
						
						echo '<div class="anagrafica-cliente" style="width:70px">
						In garanzia?<br />
						<input type="checkbox" name="in_garanzia" '.($dati_rma['in_garanzia'] == 1 ? 'checked="checked"' : '').' /><br />
						</div>';
					
					}
					else {
					
					}
					
					if($thread['riferimento'] != '')
					{
						echo '
						<div class="clear"></div>
						<div class="anagrafica-cliente" style="width:720px">
						Azione padre<br />
						<span class="tab-span" style="width:720px">'.Customer::trovaSiglaLinkPerAlbero(substr($thread['riferimento'],1,''), substr($thread['riferimento'],0,1), '','y:T:'.$thread['id_customer_thread'].':'.$thread['status']).'</span><br />
						</div>
						';
						
					}
					
					echo '
					<div class="clear"></div>
					<div class="anagrafica-cliente" style="width:720px">
					Prodotto di riferimento<br />
					<select name="id_product" style="width:720px"><option value=""> -- </option>'; 
					
					$getoOrders = Db::getInstance()->ExecuteS('
						SELECT id_order
						FROM '._DB_PREFIX_.'orders
						WHERE id_customer = '.(int)$customer->id.' ORDER BY date_add');
						
					foreach ($getoOrders as $rowo)
					{
						$ordero = new Order($rowo['id_order']);
						$tmp = $ordero->getProducts();
						foreach ($tmp as $key => $val)
						{
							$prodrichiesta = Db::getInstance()->getRow("SELECT reference, name FROM product JOIN product_lang ON product.id_product = product_lang.id_product WHERE id_lang = 5 and product.id_product = ".$val['product_id'].""); 
							echo '<option name="'.$val['product_id'].'" value="'.$val['product_id'].'" '.($thread['id_product'] == $val['product_id'] ? 'selected="selected"' : '').'>'.$prodrichiesta['reference']." - ".$prodrichiesta['name'].'</option>';
						}
					}
					echo '</select>';
			
					if($thread['id_product'] != 0) 
					{ 
						$prodrichiesta = Db::getInstance()->getRow("SELECT reference, name FROM product JOIN product_lang ON product.id_product = product_lang.id_product WHERE id_lang = 5 and product.id_product = ".$thread['id_product'].""); 
						echo $prodrichiesta['reference']." - ".$prodrichiesta['name']; 
						
						
					} 
					else if ($thread['id_contact'] == 9 && $thread['id_product'] == 0) {
						echo $dati_rma['rma_product'];
					}
					else { 
						echo '<em>Nessuno</em>';
					} 
					echo '</span><br />
					</div>

					
					<div class="clear"></div>';
					
					if($thread['id_contact'] == 4 ) {
						
						echo 'Motivo chiamata: <input style="width:831px" type="text" value="'.(($thread['id_customer_thread'] > 0) ? $thread['motivo_chiamata'] : '').'" id="motivo_chiamata_tkt" name="motivo_chiamata_tkt" /><br /><br />';
						
						
						echo '<div style="display:block; float:left; width:170px">Descrizione:</div> <select style="width:150px" type="text" value="'.(($thread['id_customer_thread'] > 0) ? $thread['descrizione'] : '').'" id="descrizione_tkt" name="descrizione_tkt" onchange="if(this.value != \'Altro\') { document.getElementById(\'descrizione_altro_tkt\').value = \'\' }">
						
						
						<option value="Ripristino funzionalita" '.(($thread['id_customer_thread'] > 0) ? ($thread['descrizione'] == 'Ripristino funzionalita' ? 'selected = "selected"' : '') : '').'>Ripristino funzionalit&agrave;</option>
						<option value="Upgrade firmware" '.(($thread['id_customer_thread'] > 0) ? ($thread['descrizione'] == 'Upgrade firmware' ? 'selected = "selected"' : '') : '').'>Upgrade firmware</option>
						<option value="Configurazione servizi" '.(($thread['id_customer_thread'] > 0) ? ($thread['descrizione'] == 'Configurazione servizi' ? 'selected = "selected"' : '') : '').'>Configurazione servizi</option>
						<option value="Configurazione nuovo hardware" '.(($thread['id_customer_thread'] > 0) ? ($thread['descrizione'] == 'Configurazione nuovo hardware' ? 'selected = "selected"' : '') : '').'>Configurazione nuovo hardware</option>
						<option value="Altro" '.(($thread['id_customer_thread'] > 0) ? (substr($thread['descrizione'],0,5) == 'Altro' ? 'selected = "selected"' : '') : '').'>Altro</option>
						
						</select> Se altro, specificare: <input style="width:383px" type="text" id="descrizione_altro_tkt" name="descrizione_altro_tkt" value="'.(($thread['id_customer_thread'] > 0) ? (substr($thread['descrizione'],0,5) == 'Altro' ? (substr($thread['descrizione'],0,5) == 'Altro' ? substr($thread['descrizione'],5,strlen($thread['descrizione'])) : $thread['descrizione']) : '') : '').'" onkeyup="document.getElementById(\'descrizione_tkt\').value = \'Altro\';" /><br /><br />';
					
						echo '<div style="display:block; float:left; width:170px">Guasto riscontrato dal tecnico:</div> <select style="width:150px" type="text" value="'.(($thread['id_customer_thread'] > 0) ? $thread['guasto'] : '').'" id="guasto_tkt" name="guasto_tkt" onchange="if(this.value != \'Altro\') { document.getElementById(\'guasto_altro_tkt\').value = \'\' }">
						
						<option value="Nessuna" '.(($thread['id_customer_thread'] > 0) ? ($thread['guasto'] == 'Nessuna' || $thread['guasto'] == '' ? 'selected = "selected"' : '') : '').'>Nessuna</option>
						<option value="Impianto fermo" '.(($thread['id_customer_thread'] > 0) ? ($thread['guasto'] == 'Impianto fermo' ? 'selected = "selected"' : '') : '').'>Impianto fermo</option>
						<option value="Problemi su linee telefoniche" '.(($thread['id_customer_thread'] > 0) ? ($thread['guasto'] == 'Problemi su linee telefoniche' ? 'selected = "selected"' : '') : '').'>Problemi su linee telefoniche</option>
						<option value="Problemi di fonia" '.(($thread['id_customer_thread'] > 0) ? ($thread['guasto'] == 'Problemi di fonia' ? 'selected = "selected"' : '') : '').'>Problemi di fonia</option>
						<option value="Altro" '.(($thread['id_customer_thread'] > 0) ? (substr($thread['guasto'],0,5) == 'Altro'  ? 'selected = "selected"' : '') : '').'>Altro</option>
						
						</select> Se altro, specificare: <input style="width:383px" type="text" id="guasto_altro_tkt" name="guasto_altro_tkt" value="'.(($thread['id_customer_thread'] > 0) ? (substr($thread['guasto'],0,5) == 'Altro' ? (substr($thread['guasto'],0,5) == 'Altro' ? substr($thread['guasto'],5,strlen($thread['guasto'])) : $thread['guasto']) : '') : '').'" onkeyup="document.getElementById(\'guasto_tkt\').value = \'Altro\';" /><br /><br />';
					
						echo '<div style="display:block; float:left; width:170px">Causa guasto:</div> <select style="width:150px" type="text" value="'.(($thread['id_customer_thread'] > 0) ? $thread['causa_guasto'] : '').'" id="causa_guasto_tkt" name="causa_guasto_tkt" onchange="if(this.value != \'Altro\') { document.getElementById(\'causa_guasto_altro_tkt\').value = \'\' }">
						
								<option value="Nessuno" '.(($thread['id_customer_thread'] > 0) ? ($thread['causa_guasto'] == 'Nessuno' || $thread['causa_guasto'] == '' ? 'selected = "selected"' : '') : '').'>Nessuno</option>
						<option value="Sovratensione" '.(($thread['id_customer_thread'] > 0) ? ($thread['causa_guasto'] == 'Sovratensione' ? 'selected = "selected"' : '') : '').'>Sovratensione</option>
						<option value="Dolo/Incuria/Accidentale" '.(($thread['id_customer_thread'] > 0) ? ($thread['causa_guasto'] == 'Dolo/Incuria/Accidentale' ? 'selected = "selected"' : '') : '').'>Dolo/Incuria/Accidentale</option>
						<option value="Ossido/Umidita" '.(($thread['id_customer_thread'] > 0) ? ($thread['causa_guasto'] == 'Ossido/Umidita' ? 'selected = "selected"' : '') : '').'>Ossido/Umidita</option>
						<option value="Usura" '.(($thread['id_customer_thread'] > 0) ? ($thread['causa_guasto'] == 'Usura' ? 'selected = "selected"' : '') : '').'>Usura</option>
						<option value="Non definita" '.(($thread['id_customer_thread'] > 0) ? ($thread['causa_guasto'] == 'Non definita' ? 'selected = "selected"' : '') : '').'>Non definita</option>
						<option value="Altro" '.(($thread['id_customer_thread'] > 0) ? (substr($thread['causa_guasto'],0,5) == 'Altro'  ? 'selected = "selected"' : '') : '').'>Altro</option>
						
						
						</select> Se altro, specificare: <input style="width:383px" type="text" id="causa_guasto_altro_tkt" name="causa_guasto_altro_tkt" value="'.(($thread['id_customer_thread'] > 0) ? (substr($thread['causa_guasto'],0,5) == 'Altro' ? (substr($thread['causa_guasto'],0,5) == 'Altro' ? substr($thread['causa_guasto'],5,strlen($thread['causa_guasto'])) : $thread['causa_guasto']) : '') : '').'" onkeyup="document.getElementById(\'causa_guasto_tkt\').value = \'Altro\';" /><br /><br />';
						
					}
			
					if($thread['id_contact'] == 9) {
					
					
					
					echo '<div class="anagrafica-cliente" style="width:100px">
					Tipo RMA<br />
					<select name="rma_type" style="width:110px">
					<option name="Reso" '.($dati_rma['rma_type'] == 'Reso' ? 'selected="selected"' : '').'>Reso</option>
					<option name="Riparazione"  '.($dati_rma['rma_type'] == 'Riparazione' ? 'selected="selected"' : '').'>Riparazione</option>
					</select><br />
					</div>';
					
					includeDatepicker3(array('arrivocli', 'data_richiesta', 'arrivoezfornitore'), true);
					
					echo '<div class="anagrafica-cliente" style="width:100px">
					Spedizione<br />
					<select name="rma_shipping" style="width:110px">
					<option name="Corriere Ezdirect" '.($dati_rma['rma_shipping'] == 'Corriere Ezdirect' ? 'selected="selected"' : '').'>Corriere Ezdirect</option>
					<option name="Corriere Cliente"  '.($dati_rma['rma_shipping'] == 'Corriere Cliente' ? 'selected="selected"' : '').'>Corriere Cliente</option>
					</select><br />
					</div>';
					$indirizzi_rma = Db::getInstance()->executeS('SELECT * FROM address a JOIN state s ON a.id_state = s.id_state WHERE a.active = 1 AND a.deleted = 0 AND a.id_customer='.$customer->id.''); 
					
					echo '<div class="anagrafica-cliente" style="width:480px">
					Indirizzo spedizione reso<br />
					<select name="indirizzo_rma" style="width:490px">';
					foreach ($indirizzi_rma as $indirizzo_rma) {
					
					echo '<option value="'.$indirizzo_rma['id_address'].'" '.($indirizzo_rma['id_address'] == $dati_rma['id_address'] ? 'selected="selected"' : '').'>'.$indirizzo_rma['address1'].' - '.$indirizzo_rma['postcode'].' '.$indirizzo_rma['city'].' ('.$indirizzo_rma['iso_code'].')</option>';
					
					}
					echo '</select>
					
					<br /><br />
					</div>';
					
					
					echo '<div class="clear"></div>';
					
					echo '<div class="anagrafica-cliente" style="width:100px">
					Arrivato c/o EZ<br />
					<input type="checkbox" style="width:10px" name="arrivoezcli" '.($dati_rma['arrivoezcli'] == 1 ? 'checked="checked"' : '').' /> 
					</div>';
					
						echo '<div class="anagrafica-cliente" style="width:100px">
					Data arrivo cliente<br />
					<input type="text" name="arrivocli" id="arrivocli" style="width:100px" value="'.($dati_rma['arrivocli'] == '0000-00-00 00:00:00' || $dati_rma['arrivocli'] == '1970-01-01 01:00:00' ? '' : date('d-m-Y', strtotime($dati_rma['arrivocli']))).'" />
					</div>';
					
						echo '<div class="anagrafica-cliente" style="width:100px">
					A carico di<br />
				
					<input type="radio" style="width:10px" value="0" name="a_carico" '.($dati_rma['a_carico'] == 0 ? 'checked="checked"' : '').' /> Cliente
					&nbsp;&nbsp;&nbsp;
					<input type="radio" style="width:10px" value="1" name="a_carico" '.($dati_rma['a_carico'] == 1 ? 'checked="checked"' : '').' /> EZ
					</div>';
					
					
						echo '<div class="anagrafica-cliente" style="width:130px">
					Corriere<br />
					<select name="corriere" id="corriere" style="width:130px">
					<option '.($dati_rma['corriere'] == 0 || $dati_rma['corriere'] == '' ? 'selected="selected"' : '').' value="0">GLS</option>
					<option '.($dati_rma['corriere'] == 1? 'selected="selected"' : '').' value="1">Bartolini</option>
					<option '.($dati_rma['corriere'] == 2? 'selected="selected"' : '').' value="2">TNT</option>
					<option '.($dati_rma['corriere'] == 3? 'selected="selected"' : '').' value="3">Poste</option>
					<option '.($dati_rma['corriere'] == 5? 'selected="selected"' : '').' value="5">DHL</option>
					<option '.($dati_rma['corriere'] == 6? 'selected="selected"' : '').' value="6">UPS</option>
					<option '.($dati_rma['corriere'] == 7? 'selected="selected"' : '').' value="7">SDA</option>
					<option '.($dati_rma['corriere'] == 4? 'selected="selected"' : '').' value="4">Altro</option>
					</select><br /><br />
					</div>';
					
					echo '<div class="clear"></div>';
					
					echo '<div class="anagrafica-cliente" style="width:100px">
					Testato<br />
				
					<input type="radio" style="width:10px" value="0" name="test" '.($dati_rma['test'] == 0 ? 'checked="checked"' : '').' /> S&igrave;&nbsp;&nbsp;&nbsp;
					
					<input type="radio" style="width:10px" value="1" name="test" '.($dati_rma['test'] == 1 ? 'checked="checked"' : '').' /> No
					</div>';
						
					echo '<div class="anagrafica-cliente" style="width:100px">
					Test in carico a<br />
					<select name="test_in_carico_a" style="width:110px">
					';
					
					$impiegati_test = Db::getInstance()->executeS('SELECT * FROM employee WHERE active = 1 AND id_profile = 4 OR id_profile = 6');
					
					echo '<option value="0"'.($dati_rma['test_in_carico_a'] == 0 ? 'selected="selected"' : '').' >Da assegnare</option>';
					
					foreach ($impiegati_test as $impezt) {
					
						echo "<option value='".$impezt['id_employee']."' ".($dati_rma['test_in_carico_a'] == $impezt['id_employee'] ? 'selected="selected"' : '')." >".$impezt['firstname']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>";
					
					}
					
					echo '
					</select><br />
					</div>';
					
					echo '
					<div class="anagrafica-cliente" style="width:480px">
					Problematica da testare
					<input type="text" name="problematica_test" style="width:480px" value="'.$dati_rma['problematica_test'].'" />
					</div>';
					
					echo '<div class="anagrafica-cliente" style="width:auto; margin-left:10px">
					Urgente<br />
					<input type="checkbox" style="width:10px" name="urgente" '.($dati_rma['urgente'] == 1 ? 'checked="checked"' : '').' /> 
					</div>';
					
					echo '<div class="clear"></div>';
					
					echo '<div class="anagrafica-cliente" style="width:100px">
					Richiesto forn.<br />
					<input type="checkbox" style="width:10px" name="richiesto_rma" '.($dati_rma['richiesto_rma'] == 1 ? 'checked="checked"' : '').' /> 
					</div>';
					
					
						echo '<div class="anagrafica-cliente" style="width:100px">
					Data richiesta<br />
					<input type="text" name="data_richiesta" id="data_richiesta" style="width:100px" value="'.($dati_rma['data_richiesta'] == '0000-00-00 00:00:00' || $dati_rma['data_richiesta'] == '1970-01-01 01:00:00' ? '' : date('d-m-Y', strtotime($dati_rma['data_richiesta']))).'" />
					</div>';
					
						echo '<div class="anagrafica-cliente" style="width:100px">
					Codice RMA Forn.<br />
					<input type="text" name="codice_rma_fornitore" style="width:100px" value="'.$dati_rma['codice_rma_fornitore'].'" />
					</div>';
					
					echo '<div class="anagrafica-cliente" style="width:100px">
					Fornitore<br />
					<div style="position:relative">
					<select style="position:absolute; top:1px; height:22px; width:105px; left:0px" onchange="this.nextElementSibling.value=this.value">
						<option id="rif_ordine_prima_opzione" value=""></option>
						<option value="Allnet">Allnet</option>
						<option value="EdsLan">EdsLan</option>
						<option value="Itancia">Itancia</option>
						<option value="Cobra">Cobra</option>
						<option value="Comtel">Comtel</option>
						<option value="Jabra">Jabra</option>
						</select>
						<input name="il_fornitore" id="il_fornitore" style="position:absolute; top:2px; left:1px; border:0px; width:77px" type="text" value="'.$dati_rma['il_fornitore'].'" /></div>
					</div>';
					
					
						echo '<div class="anagrafica-cliente" style="width:130px">
					Arrivato c/o EZ dal forn.<br />
					<input type="text" name="arrivoezfornitore" id="arrivoezfornitore" style="width:130px" value="'.($dati_rma['arrivoezfornitore'] == '0000-00-00 00:00:00' || $dati_rma['arrivoezfornitore'] == '1970-01-01 01:00:00' ? '' : date('d-m-Y', strtotime($dati_rma['arrivoezfornitore']))).'" />
					<br /><br />
					</div>';
					
						echo '<div class="anagrafica-cliente">
					Sostituito / Riparato<br />
					<input type="radio" style="width:10px" value="0" name="sostituitoriparato" '.($dati_rma['sostituitoriparato'] == 0 ? 'checked="checked"' : '').' /> Sostituito
					&nbsp;&nbsp;&nbsp;
					<input type="radio" style="width:10px" value="1" name="sostituitoriparato" '.($dati_rma['sostituitoriparato'] == 1 ? 'checked="checked"' : '').' /> Riparato
					</div>';
					
					
					
					echo '<div class="clear"></div>';
					
					
					echo '<div class="anagrafica-cliente">
					Attivit&agrave; svolta<br />
					<select name="attivita_svolta" id="attivita_svolta" style="width:230px">
											<option '.($dati_rma['attivita_svolta'] == 0 || $dati_rma['attivita_svolta'] == '' ? 'selected="selected"' : '').' value="0">Nessuna</option>
											<option '.($dati_rma['attivita_svolta'] == 1? 'selected="selected"' : '').' value="1">Sostituito con nuovo</option>
											<option '.($dati_rma['attivita_svolta'] == 2? 'selected="selected"' : '').' value="2">Riparato</option>
											<option '.($dati_rma['attivita_svolta'] == 3? 'selected="selected"' : '').' value="3">Inviato al centro assistenza</option>
											<option '.($dati_rma['attivita_svolta'] == 4? 'selected="selected"' : '').' value="4">Altro</option>
											</select>
					</div>
					<div class="anagrafica-cliente" style="width:480px">
					Se in "Attivit&agrave; svolta" hai selezionato "altro", specificalo qua:
					<input type="text" name="altra_attivita" style="width:480px" value="'.$dati_rma['altra_attivita'].'" />
					</div>
					<div class="clear"></div>
					
					';
					
					
					echo '<div class="anagrafica-cliente" style="width:380px;"><br />
					Dettaglio costi
					<script type="text/javascript">
					function CalcTotRMA () {
					
						var num_costo_materiale = document.getElementById(\'costo_materiale\').value;
						var costo_materiale = parseFloat(num_costo_materiale.replace(/\s/g, "").replace(",", "."));
						if(isNaN(costo_materiale)) { costo_materiale = 0; }
						
						var num_costo_manodopera = document.getElementById(\'costo_manodopera\').value;
						var costo_manodopera = parseFloat(num_costo_manodopera.replace(/\s/g, "").replace(",", "."));
						if(isNaN(costo_manodopera)) { costo_manodopera = 0; }
						
						var num_totale_trasporti = document.getElementById(\'totale_trasporti\').value;
						var totale_trasporti = parseFloat(num_totale_trasporti.replace(/\s/g, "").replace(",", "."));
						if(isNaN(totale_trasporti)) { totale_trasporti = 0; }
						
						
						var totale_RMA = costo_manodopera + costo_materiale + totale_trasporti;
						
						
						document.getElementById(\'totale_RMA\').innerHTML = totale_RMA.toFixed(2).replace(\'.\',\',\');
						
					}
					</script>
					<table style="font-size:12px">
					<tr><td style="width:150px">Costo materiale</td><td></td><td><input type="text" onkeyup="CalcTotRMA();" name="costo_materiale" id="costo_materiale" style="width:60px; text-align:right" value="'.str_replace(".",",",$dati_rma['costo_materiale']).'" /> &euro;</td></tr>
					<tr><td>Costo manodopera</td><td><select name="manodopera_qta" id="manodopera_qta" style="width:60px" onchange="document.getElementById(\'costo_manodopera\').value = document.getElementById(\'manodopera_qta\').value*40; CalcTotRMA();">
					<option value="0" '.($dati_rma['manodopera_qta'] == 0 ? 'selected="selected"' : '').'>Scegli</option>
					<option value="0.5" '.($dati_rma['manodopera_qta'] == 0.5 ? 'selected="selected"' : '').'>0,5</option>
					<option value="1" '.($dati_rma['manodopera_qta'] == 1 ? 'selected="selected"' : '').'>1</option>
					<option value="1.5" '.($dati_rma['manodopera_qta'] == 1.5 ? 'selected="selected"' : '').'>1,5</option>
					<option value="2" '.($dati_rma['manodopera_qta'] == 2 ? 'selected="selected"' : '').'>2</option>
					<option value="2.5" '.($dati_rma['manodopera_qta'] == 2.5 ? 'selected="selected"' : '').'>2,5</option>
					<option value="3" '.($dati_rma['manodopera_qta'] == 3 ? 'selected="selected"' : '').'>3</option>
					<option value="3.5" '.($dati_rma['manodopera_qta'] == 3.5 ? 'selected="selected"' : '').'>3,5</option>
					<option value="4" '.($dati_rma['manodopera_qta'] == 4 ? 'selected="selected"' : '').'>4</option>
					<option value="4.5" '.($dati_rma['manodopera_qta'] == 4.5 ? 'selected="selected"' : '').'>4,5</option>
					<option value="5" '.($dati_rma['manodopera_qta'] == 5 ? 'selected="selected"' : '').'>5</option>
					<option value="5.5" '.($dati_rma['manodopera_qta'] == 5.5 ? 'selected="selected"' : '').'>5,5</option>
					<option value="6" '.($dati_rma['manodopera_qta'] == 6 ? 'selected="selected"' : '').'>6</option>
					<option value="6.5" '.($dati_rma['manodopera_qta'] == 6.5 ? 'selected="selected"' : '').'>6,5</option>
					<option value="7" '.($dati_rma['manodopera_qta'] == 7 ? 'selected="selected"' : '').'>7</option>
					</select> ore
					</td><td><input type="text" name="costo_manodopera" id="costo_manodopera" value="'.str_replace(".",",",$dati_rma['costo_manodopera']).'" style="width:60px; text-align:right" onkeyup="CalcTotRMA();" /> &euro;</td></tr>
					<tr><td>Trasporti qta</td><td><input type="text" name="trasporti_qta" id="trasporti_qta" value="'.str_replace(".",",",$dati_rma['trasporti_qta']).'" onkeyup="document.getElementById(\'totale_trasporti\').value = (document.getElementById(\'trasporti_qta\').value*9.90).toFixed(2).replace(\'.\',\',\'); CalcTotRMA();"  style="width:60px; text-align:right" /></td><td><input type="text" name="totale_trasporti" id="totale_trasporti" value="'.str_replace(".",",",$dati_rma['totale_trasporti']).'" style="width:60px; text-align:right" onkeyup="CalcTotRMA();" /> &euro;</td></tr>
					<tr><td>Totale</td><td></td><td><span class="tab-span" id="totale_RMA" style="width:63px; margin-right:3px; text-align:right; float:left">'.number_format(($dati_rma['costo_materiale']+$dati_rma['costo_manodopera']+$dati_rma['totale_trasporti']),2,",","").'</span> &euro;</tr>
					</table>
					</div>';
					
					echo '<div class="anagrafica-cliente" style="width:325px"><br />Template RMA
					<span class="tab-span" style="width:325px"><a href="http://www.ezdirect.it/rma/'.$dati_rma['codice_rma'].'.html" target="_blank">http://www.ezdirect.it/rma/'.$dati_rma['codice_rma'].'.html</a></span></div>';
					
					echo '<div class="clear"></div>';
					
					}
					
					
			
					$ticket_note = Db::getInstance()->executeS('SELECT * FROM note_attivita WHERE tipo_attivita = "T" AND id_attivita = '.$thread['id_customer_thread'].'');
					
					echo '<strong>Note interne visibili solo agli impiegati</strong><br />';
					
					echo '<table><thead>'.(count($ticket_note) > 0 ? '<tr><th>Nota</th><th>Creata da</th><th>Ultima modifica</th><th></th></tr>' : '' ).'</thead><tbody id="tableNoteBody">
					';
					
					foreach($ticket_note as $ticket_nota)
					{
						echo '<tr id="tr_note_'.$ticket_nota['id_note'].'">';
						echo '
						<td>
						<textarea class="textarea_note" name="note_nota['.$ticket_nota['id_note'].']" id="note_nota['.$ticket_nota['id_note'].']" style="width:540px; height:16px" onkeyup="auto_grow(this)">'.htmlentities($ticket_nota['note'], ENT_COMPAT, 'UTF-8').'</textarea><input type="hidden" name="note_nuova['.$ticket_nota['id_note'].']" id="note_nuova['.$ticket_nota['id_note'].']" value="0" /></td>
						<td><input type="text" readonly="readonly" name="note_id_employee['.$ticket_nota['id_note'].']" value="'.Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM employee WHERE id_employee = '.$ticket_nota['id_employee']).'" /></td>
						<td><input type="text" readonly="readonly" name="note_date_add['.$ticket_nota['id_note'].']" value="'.Tools::displayDate($ticket_nota['date_upd'], 5).'" /></td>';
						echo '<td><a href="javascript:void(0)" onclick="javascript:var surec=window.confirm(\'Sei sicuro?\'); if (surec) { togli_nota('.$ticket_nota['id_note'].'); cancella_nota_attivita('.$ticket_nota['id_note'].'); } else { return false; }; "><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" style="cursor:pointer" /></a></td>';
						echo'
						</tr>
						';
					}
					
					echo '</tbody></table><br />';
					
					echo '
					<script type="text/javascript">add_nota_privata();</script>
					<a href="javascript:void(0)" onclick="javascript:add_nota_privata()";><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" /> Clicca per aggiungere una nota privata</a> <br /><br />';
					
					if($thread['date_add'] < '2017-12-08 00:00:00')
					{
						echo '
						<textarea name="ticket_note" id="ticketnoteContent" style="width:80%;height:100px">'.Tools::htmlentitiesUTF8($thread['note']).'</textarea><br />
						';
					}
					
					echo '<br />';
					
					echo '<button type="submit" class="button" name="submitTicket">
							<img src="../img/admin/save.gif" alt="Salva" title="Salva" />&nbsp;&nbsp;&nbsp;'.$this->l('   Save   ').'
							</button>				
							<button class="button" onclick="javascript:var surec=window.confirm(\'Sei sicuro?\'); if (surec) { $(\'#coll_destinazione\').val($(\'#collega_ticket\').val()); document.forms[\'collega_attivita\'].submit(); return false; } else { return false; }; " style="margin-left:10px; margin-right:0px">
					
					<img src="../img/admin/link.gif" alt="Collega" title="Collega" />&nbsp;&nbsp;&nbsp;'.$this->l('Collega a:').'
					</button>
					<select name="collega_ticket" id="collega_ticket" style="margin-left:-4px; height:27px;width:150px" >
						<option value="">-- Seleziona --</option>
						'.Customer::BuildSelectForLinking($customer->id, $thread['id_customer_thread'], 'T').'
					</select>
					';
						
					if($cookie->id_employee == 6 || $cookie->id_employee == 22)
					{
						echo '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&getPDF&id='.$thread['id_customer_thread'].'&tipo=T&token='.$this->token.'&filename='.$allegato.'">Esporta in PDF</a>';
						
					}
					
					if($bdl_ticket == 0) {
						echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&nuovobdl&riferimento=T".Tools::getValue('id_customer_thread')."&token=".$this->token."&viewticket&id_customer_thread=".Tools::getValue('id_customer_thread')."&tab-container-1=6&tab-container-ticket=5#ticket-5' class='button' ><img src='../img/admin/prefs.gif' alt='Nuovo BDL' title='Nuovo BDL' />&nbsp;&nbsp;&nbsp;Genera nuovo BDL</a>";
					}
					
					echo '</form>
				
					<form style="margin-left:5px; display:block;float:left" name="collega_attivita" id="collega_attivita" action="'.htmlentities($_SERVER['REQUEST_URI']).'&conf=4" method="post" />
					<input type="hidden" name="submitCollegaAttivita" value="" />
					<input type="hidden" name="coll_destinazione" id="coll_destinazione" value="" />
					<input type="hidden" name="coll_partenza" id="coll_partenza" value="T'.$thread['id_customer_thread'].'" />
					</form>';
					
			echo '</form>
			</fieldset><br /><br /></div>';
				}
				
				if(isset($_GET['id_customer_thread'])) {
					echo '<div class="yetii-ticket" id="ticket-2">';
				}	
					$messaggit = Db::getInstance()->ExecuteS("SELECT * FROM customer_message cm WHERE cm.id_customer_thread = ".$_GET['id_customer_thread']." ORDER BY date_add ASC");
					$msg_id = 0;
					foreach($messaggit as $messaggiot) {
					
						echo "<table style='width:100%; 
						border:1px solid #dfd5c3; 
						";
						if ($messaggiot['id_employee'] == 0) {
							echo 'background-color: #ffffff;';
						} else {
				
							echo 'background-color: #ffecf2;';
						}
						$employeezt = new Employee($messaggiot['id_employee']);
						echo "margin-bottom:15px'>";
						echo '<tr>
						<td style="colspan:2">
							<table>
					
							<td style="font-size:14px; width:230px">Da: <strong>';
							if ($messaggiot['id_employee'] == 0) {
								if($messaggiot['email'] == "" || is_numeric($messaggiot['email'])) {
									echo $customer->firstname." ".$customer->lastname; 
								}
								else {
									echo "Ticket aperto dallo staff";
								}
							} 
							else 
							{ 
								echo $employeezt->firstname." ".substr($employeezt->lastname,0,1).".";
								if($msg_id == 0)
										echo ' - ticket aperto dallo staff';
							} 
							echo '<strong></td>
							
							<td style="width:170px"><strong>Data: </strong>'.date("d-m-Y H:i:s",strtotime($messaggiot['date_add'])).'</td>
							<td style="width:180px"><strong>Allegati: </strong>';
							if(!empty($messaggiot['file_name'])) {
							
								$allegati = explode(";",$messaggiot['file_name']);
								$nall = 1;
								foreach($allegati as $allegato) {
									
									
									if(strpos($allegato, ":::")) {
						
										$parti = explode(":::", $allegato);
										$nomevero = $parti[1];
										
									}
						
									else {
										$nomevero = $allegato;
						
									}
						
									if($allegato == "") { } else {
										if($nall == 1) { } else { echo " - "; }
										
										/*if($customer->id == 2) {
											echo '<a href="https://ezdocs:WRY753MNR43ASD147g!@'.preg_replace('#^https?://#', '', rtrim($nomevero,'/')).'"><span style="color:red">'.substr( $nomevero, strrpos( $nomevero, '/' )+1 ).'</span></a>';
											$nall++;
										}
										else {*/
											echo '<a href="ajax.php?openAllegato=y&id_customer='.$customer->id.'&viewcustomer&viewticket&id_customer_thread='.$thread['id_customer_thread'].'&tab-container-1=6&token='.$this->token.'&filename='.$allegato.'"><span style="color:red">'.$nomevero.'</span></a>';
											$nall++;
										//}
									}
								}
							}
							
							else { echo 'Nessun allegato'; }
							
							
							echo '</td>';
							
							
							
							echo '<td></td></tr></table>
						</td>
						</tr>
						<tr>
						<td style="colspan:2">
							<table>
							<tr>
							<td style="width:100px"><strong>Messaggio</strong></td>
							<td>'.htmlspecialchars_decode(preg_replace('/(<[^>]+) style=".*?"/i', '$1', preg_replace('/(<[^>]+) class=".*?"/i', '$1', $messaggiot['message']))).($messaggiot['id_employee'] == 0 ? '' : '
							<form method="post">
							<input type="hidden" name="modifica_messaggio_ticket" value="'.htmlentities($messaggiot['message'], ENT_QUOTES | ENT_SUBSTITUTE, "UTF-8").'" />
							
							<button type="submit" class="button" name="modifica_messaggio_submit" value="Modifica">
							<img src="../img/admin/edit.gif" alt="Modifica" title="Modifica" />&nbsp;&nbsp;&nbsp;Modifica
							</button>
							
							</form>').'
							</td>
							</tr>
							</table>
						</td>';
						echo '</tr>
						';
						
						if($messaggiot['note_private'] != '')
						{
							echo '<tr>
							<td style="colspan:2">
								<table>
								<tr>
								<td style="width:100px"><strong style="color:red">Nota privata</strong></td>
								<td style="color:red">'.htmlspecialchars_decode(preg_replace('/(<[^>]+) style=".*?"/i', '$1', preg_replace('/(<[^>]+) class=".*?"/i', '$1', $messaggiot['note_private']))).'
								</td>
								</tr>
								</table>
							</td>';
							echo '</tr>
							';
						}
						
						if($messaggiot['email'] == '') { 
							if($messaggiot['in_carico'] > 0) 
							{
								echo '<tr><td style="colspan:2">
								<table>
								<tr>
								
								<td style="width:100px"><strong>In carico a</strong></td>
								<td>'.Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$messaggiot['in_carico']).'</td>
								';
							}	
						} else {
						
							echo '<tr><td style="colspan:2">
							<table>
							<tr>
							<td><strong>Inviato a: </strong>'.(is_numeric($messaggiot['email']) ? Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$messaggiot['email']) : htmlspecialchars_decode($messaggiot['email'])).'</td>
							
							'.($messaggiot['in_carico'] > 0 ? '<td style="padding-left:20px"><strong>In carico a</strong>:
							'.($messaggiot['in_carico'] > 0 ? Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$messaggiot['in_carico'])  : '--').'</td>' : '').'
							
							';
						
						} 
						
						$cc = Db::getInstance()->getValue("SELECT cc FROM ticket_cc WHERE type = 't' AND msg = ".$messaggiot['id_customer_message']."");
							if(!empty($cc)) {
							
								echo '<td style="padding-left:20px">';
							
								$ccs = explode(";",$cc);
								$cc_str = "<strong>CC:</strong> ";
								
								foreach ($ccs as $conoscenza) {
								
									$imp = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".$conoscenza."");
									$cc_str .= $imp." ";
								}
								
								echo $cc_str;
								echo "</td>";
							
							}
							else {
							
								echo '<td style="padding-left:20px"><strong>CC:</strong> Nessuno</td>';
							
							}
						
						echo '</tr>
							</table></tr></table>';
						
						if($thread['id_contact'] == 9) {
						}
						else {
							echo '<strong>Ordine di riferimento</strong>: '.($thread['id_order'] != 0 ? $thread['id_order'] : '<em>Nessuno</em>').' - <strong>Prodotto di riferimento</strong>: '; if($thread['id_product'] != 0) { $prodrichiesta = Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_lang = 5 and id_product = ".$thread['id_product'].""); echo $prodrichiesta; } else { 	echo '<em>Nessuno</em>'; }
						}					
						/*
						if($thread['id_contact'] == 9) {
						
						echo '<br /><strong>Seriale</strong>: '.$dati_rma['seriale'].' - <strong>Codice RMA</strong>: '.$dati_rma['codice_rma'].' - <strong>Tipo RMA</strong>: '.$dati_rma['rma_type'].' - <strong>Spedizione</strong>: '.$dati_rma['rma_shipping'].' - <strong>Indirizzo</strong>: '.$indirizzo_rma['address1'].' - '.$indirizzo_rma['postcode'].' '.$indirizzo_rma['city'].' ('.$indirizzo_rma['iso_code'].')<br /><strong>Template RMA</strong>: <a href="http://www.ezdirect.it/rma/'.$dati_rma['codice_rma'].'.html" target="_blank">http://www.ezdirect.it/rma/'.$dati_rma['codice_rma'].'.html</a>';  
						
						}					
						*/
						
						echo '
						
						<br /><br />';
						$msg_id++;
					}
				
				}
				
				else {
				
				}
				
				if(isset($_GET['aprinuovoticket'])) {
				
					echo '<h2>Apri ticket per conto del cliente</h2>';
					echo '<strong style="color:red">ATTENZIONE!!!!!!</strong>: quando si apre un ticket per il cliente, il testo del ticket viene notificato anche al cliente tramite mail.<br /><br />';
				
				}
			
				else {
				
					echo '<h2 id="rispondi-cliente-ticket">Rispondi al cliente</h2>';
					if(isset($_POST['modifica_messaggio_submit'])) {
						echo '<script type="text/javascript">
							 window.location = "#rispondi-cliente-ticket";
						</script>';
					}
				}

				$iso = Language::getIsoById((int)($cookie->id_lang));
				$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
				$ad = dirname($_SERVER["PHP_SELF"]);
					echo '
				<script type="text/javascript">
				var iso = \''.$isoTinyMCE.'\' ;
				var pathCSS = \''._THEME_CSS_DIR_.'\' ;
				var ad = \''.$ad.'\' ;
				</script>
				
				
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce3/tiny_mce.js"></script>
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>
				<script type="text/javascript">
					$(document).ready(function () {
					
	dataChanged = 0;     // global variable flags unsaved changes      

	function bindForChange(){    
		 $(\'input,checkbox,text,textarea,radio,select\').bind(\'change\',function(event) { dataChanged = 1});
		 $(\'#messaggiom\').bind(\'input propertychange\', function() { dataChanged = 1 });
		 $(\':reset,:submit\').bind(\'click\',function(event) { dataChanged = 0 });
	}


	function askConfirm(){  
		if (dataChanged){ 
			return "You have some unsaved changes.  Press OK to continue without saving." 
		}
	}

	window.onbeforeunload = askConfirm;
	window.onload = bindForChange;
	});
				</script>
				<!-- <script type="text/javascript">
						 toggleVirtualProduct(getE(\'is_virtual_good\'));
						unitPriceWithTax(\'unit\'); 
				</script> -->';
				$employeemess = new Employee($cookie->id_employee);
				
				echo'	<form id="submit_ticket" name="submit_ticket" action="'.$currentIndex.'&id_customer='.$obj->id.'&viewcustomer&token='.$this->token.'&tab-container-1=6'.(Tools::getIsset('riferimento') ? '&riferimentoo='.Tools::getValue('riferimento') : '').'" method="post" class="std" enctype="multipart/form-data">';
				
				

				echo'	<fieldset>
							<table>';
							if(isset($_GET['aprinuovoticket'])) {
								
								echo '
								
								<input type="hidden" name="nuovoticket" value="1" />
								<input type="hidden" name="riferimento" id="riferimento" value="'.Tools::getValue('riferimento').'" /><input type="hidden" name="riferimentoo" id="riferimentoo" value="'.Tools::getValue('riferimento').'" />
								<tr><td>Tipo ticket</td>
											<td>
											<select name="id_contact" id="id_contact">
											<option value="4" '.(Tools::getIsset('tipo-ticket') && Tools::getValue('tipo-ticket') == '4' ? 'selected="selected"' : '' ).'>Assistenza tecnica</option>
											<option value="2" '.(Tools::getIsset('tipo-ticket') && Tools::getValue('tipo-ticket') == '2' ? 'selected="selected"' : '' ).'>Assistenza amministrativa - contabilit&agrave;</option>
											<option value="8" '.(Tools::getIsset('tipo-ticket') && Tools::getValue('tipo-ticket') == '8' ? 'selected="selected"' : '' ).'>Assistenza amministrativa - ordini</option>
											<option value="9" '.(Tools::getIsset('tipo-ticket') && Tools::getValue('tipo-ticket') == '9' ? 'selected="selected"' : '' ).'>RMA</option>
											<option value="3" '.(Tools::getIsset('tipo-ticket') && Tools::getValue('tipo-ticket') == '3' ? 'selected="selected"' : '' ).'>Rivenditori</option>
											</select>
											</td>
											</tr>

								';
					
				
							}
			
							else {
									echo '<script type="text/javascript">
				
									$(document).ready(function () {
										
										$("#submit_ticket").submit(function () {
											d = document.getElementById("status").value;
											
											if(d != "'.$thread['status'].'") {
												
											}
											
											else {
												var salvamodifiche = window.confirm("STATUS: OK per procedere, ANNULLA per cambiare."); 
										
													if (salvamodifiche) {
			$(\'#waiting1\').waiting({ 
			elements: 10, 
			auto: true 
			});
			var overlay = jQuery(\'<div id="overlay"></div>\');
			var attendere = jQuery(\'<div id="attendere">ATTENDERE PREGO</div>\');
			overlay.appendTo(document.body);
			attendere.appendTo(document.body);
														$("#submitMessage").trigger("click");
													
													} else {
														 return false;
															
														//niente
													}
											}
										});
										
										
									});
						
								</script>';
								echo '<input type="hidden" value="1" name="isAnswer" />';
								echo '<input type="hidden" value="'.$thread['id_contact'].'" name="id_contact">';
							}	
								echo '<tr>
									<td>
									';
									
								$customer_phone = Db::getInstance()->getRow("SELECT	phone, phone_mobile FROM address WHERE id_customer = ".$customer->id." AND deleted = 0 AND fatturazione = 1 AND active = 1");
								
								if($customer_phone['phone'] == '') { $telefono_cliente = $customer_phone['phone_mobile']; } else { $telefono_cliente = $customer_phone['phone']; }
									
									echo '
									<input type="hidden" name="id_customer" value="'.$customer->id.'" />
									
									<input type="hidden" value="'.$thread['id_customer_thread'].'" name="id_customer_thread">
									</td>
								</tr>
						
								<tr><td>Email<br /></td><td>';
								
								
								if($customer->is_company == 1) { echo '<div style="float:left">';
								
								$persone = Db::getInstance()->executeS("SELECT firstname, lastname, email FROM persone WHERE id_customer = ".Tools::getValue('id_customer')." AND email != ''");
								
								echo '
								<select style="width:160px" name="seleziona-personat" onchange="javascript:document.getElementById(\'customer_email_msg\').value = (this.value);" >';
								echo "<option value=''>-- Seleziona persona --</option>";
								
								$persona_thread = Db::getInstance()->getValue('SELECT id_persona FROM persona WHERE email = "'.$thread['email'].'" AND id_customer = '.$customer->id.'');
								
								foreach ($persone as $persona) {
								
									echo "<option value='".$persona['email']."' ".($persona['id_persona'] == $persona_thread ? 'selected="selected"' : "").">".$persona['firstname']." ".$persona['lastname']."</option>";
								
								}
								
								echo "</select></div>";
								} else { }
								
									echo '<div style="float:left; '.($customer->is_company == 1? 'margin-left:20px' : '').'">
								<input type="text" size="40" name="customer_email_msg" id="customer_email_msg" value="'.(!$thread['email'] ? $customer->email : $thread['email']).'" /><br />
								<span style="font-size:11px">In caso di più indirizzi mail, separare con punto e virgola (;)</span></div>';
								
				
								
								echo '<div style="float:left; margin-left:20px"> Tel. Cliente: <span class="tab-span" style="width:200px; padding-left:16px; padding-right:16px; display:inline"> <a href="callto:'.$telefono_cliente.'">'.$telefono_cliente.'</a></span></div></td></tr>
								';
								
								
									
								echo '
								<tr><td>Stato ticket</td>
								<td>
								<div style="float:left">
								<select name="status" id="status">
								<option value="open" '.($thread['status'] == 'open' ? 'selected="selected"' : '').'>Aperto   </option>
								<option value="pending1" '.($thread['status'] == 'pending1' ? 'selected="selected"' : '').'>In lavorazione   </option>
								<option value="closed" '.($thread['status'] == 'closed' ? 'selected="selected"' : '').'>Chiuso   </option>
								
								</select>
								</div><div style="float:left; margin-left:20px">
								Urgenza
								<select name="priorita" id="priorita">
								<option value="" '.($thread['priorita'] == '' ? 'selected="selected"' : '').'>--</option>
								<option value="low" '.($thread['priorita'] == 'low' ? 'selected="selected"' : '').'>Bassa</option>
								<option value="medium" '.($thread['priorita'] == 'medium' ? 'selected="selected"' : '').'>Media</option>
								<option value="high" '.($thread['priorita'] == 'high' ? 'selected="selected"' : '').'>Alta</option>
								</select>
								
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								In carico a';
						echo '
					<script type="text/javascript">
									function changeInCaricoA()
									{';
									
									
									foreach ($impiegati_ez as $impez) {
										echo '
										
										firstVar = document.getElementById("in_carico_a").value;
										
										if (document.getElementById("in_carico_a").value == "'.$impez['id_employee'].'") {
										
										
										document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').disabled = true;
										document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').checked = false;
										
										} else { document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').disabled = false; }
									';
									}
								echo '
									}
									</script>';
									
									echo '
					<select style="width:250px" name="in_carico_a" id="in_carico_a" onChange = "changeInCaricoA();">';
		
					foreach ($impiegati_ez as $impez) {
					
						if(isset($_GET['aprinuovoticket'])) {
							echo "<option value='".$impez['id_employee']."' ".($cookie->id_employee == $impez['id_employee'] ? "selected='selected'" : '')." >".$impez['firstname']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>";
						}
						
						else {
						
							echo "<option value='".$impez['id_employee']."' ".($thread['id_employee'] == 0 ? ($cookie->id_employee == $impez['id_employee'] ? "selected='selected'" : '') : ($thread['id_employee'] == $impez['id_employee'] ? "selected='selected'" : ''))." >".$impez['firstname']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>";
						}
					}

					echo '</select><br /><br /></td>
								</tr>
								 <tr><td>Conoscenza</td>
								<td>
								';
								$impez_count = 0;
								
								foreach ($impiegati_ez as $impez) {
									$impez_count++;
									if($impez_count == 10)
										echo '<br />';
									
									echo "<input type='checkbox' name='conoscenza[]' id='conoscenza_".$impez['id_employee']."' value='".$impez['id_employee']."' /> ".$impez['firstname']." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
								
								}
								
								
								session_start();
								echo '</td>
								</tr>
								<tr><td><br /></td></tr>';
								
								if(isset($_GET['aprinuovoticket'])) {
									echo '<tr><td>Apri un todo a:</td><td><select name="apri-todo"><option value="">Nessuno</option>';
									foreach ($impiegati_ez as $impez) {
									
										echo "<option value='".$impez['id_employee']."'> ".$impez['firstname']."</option>";
								
									}
						
									echo '</td></tr>';
								}
								echo '
								<tr><td>Allega file</td>
								<td>
								<input name="MAX_FILE_SIZE" value="20000000" type="hidden">
								<script type="text/javascript" src="jquery.MultiFile.js"></script>';
								if($customer->id == 0) {
									
									$_SESSION['KCFINDER'] = array();
									$_SESSION['KCFINDER']['disabled'] = false;
									$_SESSION['KCFINDER']['uploadURL'] = '../../documenti-clienti/';
									$_SESSION['fold_type'] = "federico-giannini-2"; 
									echo '
									<textarea name="joinFile" class="multi" id="joinFile" rows="1" style="display:block; float:left; width:450px; height:18px;" onclick="openKCFinder(this)" id="joinFile_ticket"></textarea>
									
									<a href="javascript:openKCFinder(document.getElementById(\'joinFile_ticket\'))" class="button" style="text-decoration:none; float:left">Sfoglia...</a>
									
									<div style="clear:both"></div>
									</td></tr>
								<tr><td colspan="2">
									Anteprime: <span id="list-ticket"></span>
									';
									
								}
								
								else {
								
									echo '<input name="MAX_FILE_SIZE" value="20000000" type="hidden">
								<script type="text/javascript" src="jquery.MultiFile.js"></script>
								<input name="joinFile[]" class="multi" id="joinFile" type="file" onchange="handleFileSelect(this.files)">
								<output id="list-tkt">Anteprime: </output>
									<script type="text/javascript">
								function handleFileSelect(files) {
								
									// Loop through the FileList and render image files as thumbnails.
									 for (var i = 0; i < files.length; i++) {
										var f = files[i];
										var name = files[i].name;
			  
										var reader = new FileReader();  
										reader.onload = function(e) {  
										  // Render thumbnail.
										  var span = document.createElement("span");
										  span.innerHTML = ["<a href=\'",e.target.result,"\' target=\'_blank\'>",name,"</a> - "].join("");
										  document.getElementById("list-tkt").insertBefore(span, null);
										};

									  // Read in the image file as a data URL.
									  reader.readAsDataURL(f);
									}
								  }
								 
								</script>';
								
								}
								
								echo '</td>
								</tr>';
								
								echo '
								<tr><td>Link prodotto</td><td>
								<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
								<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete3.js"></script>
								<div id="ajax_choose_product" style="padding:6px; padding-top:2px; width:800px;">
									
									<input type="checkbox" checked="checked" id="prodotti_online" onclick="if(document.getElementById(\'product_autocomplete_input_tkt\').value != \'\') { clearAutoComplete(); }" />Online &nbsp;&nbsp;
				<input type="checkbox" id="prodotti_offline" onclick="if(document.getElementById(\'product_autocomplete_input_tkt\').value != \'\') { clearAutoComplete(); }" />Offline &nbsp;
	<input type="checkbox" id="prodotti_old" onclick="if(document.getElementById(\'product_autocomplete_input_tkt\').value != \'\') { clearAutoComplete(); }" />Old &nbsp;&nbsp;
				<input type="checkbox" id="prodotti_disponibili" onclick="if(document.getElementById(\'product_autocomplete_input_tkt\').value != \'\') { clearAutoComplete(); }" />Solo disp. &nbsp;&nbsp;&nbsp;
				
				<script type="text/javascript" src="../js/select2.js"></script>
						<script type="text/javascript">
						$(document).ready(function() { $("#auto_marca").select2(); $("#auto_serie").select2(); $("#auto_fornitore").select2();  $("#auto_categoria").select2(); });
						</script>
						
				<select id="auto_marca" name="auto_marca" onchange="repopulateSeries(this.value); clearAutoComplete();" style="width:100px">
				<option value="0">Marca...</option>
				';
				$marche = Db::getInstance()->executeS('SELECT * FROM manufacturer ORDER BY name ASC');
				foreach($marche as $marca)
					echo '<option value="'.$marca['id_manufacturer'].'">'.$marca['name'].'</option>';
				echo '
				</select>
				
						
				<select id="auto_serie" name="auto_serie" onchange="clearAutoComplete();" style="width:100px">
				<option value="0">Serie...</option>
				<option value="0">Scegli prima un costruttore</option>
				';
				echo '
				</select>
				
				<select id="auto_categoria" name="auto_categoria" onchange="clearAutoComplete();" style="width:100px">
				<option value="0">Categoria...</option>
				';
				$categorie = Db::getInstance()->executeS('SELECT * FROM category c JOIN category_lang cl ON c.id_category = cl.id_category WHERE cl.id_lang = 5 AND c.id_parent = 1 ORDER BY cl.name ASC');
				foreach($categorie as $categoria)
					echo '<option value="'.$categoria['id_category'].'">'.$categoria['name'].'</option>';
				echo '
				</select>
				
				<select id="auto_fornitore" name="auto_fornitore" onchange="clearAutoComplete(); " style="width:100px">
				<option value="0">Fornitore...</option>
				';
				
				$fornitori = Db::getInstance()->executeS('SELECT * FROM supplier WHERE id_supplier = 8 OR id_supplier = 11 OR id_supplier = 42 ORDER BY name ASC');
				foreach($fornitori as $fornitore)
					echo '<option value="'.$fornitore['id_supplier'].'">'.$fornitore['name'].'</option>';
				echo '
				</select>
				
				<input id="reset" type="button" value="Reset" class="button" onclick="document.getElementById(\'prodotti_online\').checked = true; document.getElementById(\'prodotti_offline\').checked = true; document.getElementById(\'prodotti_old\').checked = true; document.getElementById(\'prodotti_disponibili\').checked = false; document.getElementById(\'auto_categoria\').options[0].selected = true; $(\'#auto_categoria\').select2();  document.getElementById(\'auto_marca\').options[0].selected = true; $(\'#auto_marca\').select2();  
				document.getElementById(\'auto_serie\').options[0].selected = true; $(\'#auto_serie\').select2();  
				document.getElementById(\'auto_fornitore\').options[0].selected = true; $(\'#auto_fornitore\').select2(); $(\'#prodlist\').hide(); $(\'div.autocomplete_list\').hide();" />
				
				<br />
				
								
								<input size="123" type="text" value="" id="product_autocomplete_input_tkt" /><input id="veditutti2" type="button" value="Cerca" class="button" onclick=\'$("#veditutti").trigger("click");\' /><input id="veditutti" type="button" value="Cerca" class="button" style="display:none" /> <script type="text/javascript">
								
						var formProduct = "";
						var products = new Array();
					</script>
					<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
					
					<script type="text/javascript">
								
									
									urlToCall = null;
									/* function autocomplete */
									
									function getOnline()
						{
							if(document.getElementById("prodotti_online").checked == true)
								return 1;
							else
								return 0;
						}
						
						function getOffline()
						{
							if(document.getElementById("prodotti_offline").checked == true)
								return 1;
							else
								return 0;
						}

	function getOld()
						{
							if(document.getElementById("prodotti_old").checked == true)
								return 1;
							else
								return 0;
						}
						
						function getDisponibili()
						{
							if(document.getElementById("prodotti_disponibili").checked == true)
								return 1;
							else
								return 0;
						}
						
						function getCopiaDa()
						{
								return 0;
						}
						
						function getSerie()
						{
							if(document.getElementById("auto_serie").value > 0)
								return document.getElementById("auto_serie").value;
							else
								return 0;
						}
						
						function getMarca()
						{
							if(document.getElementById("auto_marca").value > 0)
								return document.getElementById("auto_marca").value;
							else
								return 0;
						}
						
						function getFornitore()
						{
							if(document.getElementById("auto_fornitore").value > 0)
								return document.getElementById("auto_fornitore").value;
							else
								return 0;
						}
						
						function getCategoria()
						{
							if(document.getElementById("auto_categoria").value > 0)
								return document.getElementById("auto_categoria").value;
							else
								return 0;
						}
						
						function repopulateSeries(id_manufacturer)
						{
							$("#auto_serie").empty();
							$.ajax({
							  url:"ajax.php?repopulate_series=y",
							  type: "POST",
							  data: { id_manufacturer: id_manufacturer
							  },
							  success:function(resp){  
								var newOptions = $.parseJSON(resp);
								
								 $("#auto_serie").append($("<option></option>")
									 .attr("value", "0").text("Serie..."));
								
								$.each(newOptions, function(key,value) {
								  $("#auto_serie").append($("<option></option>")
									 .attr("value", value).text(key));
								});
								
								$("#auto_serie").select2({
									placeholder: "Serie..."
								});
							  },
							  error: function(xhr,stato,errori){
								 alert("Errore: impossibile trovare serie ");
							  }
							});
								
							
						}	
									
								function clearAutoComplete()
						{
							$("#veditutti").trigger("click");
							
						}		
									
					</script>
					<script type="text/javascript">
					
						function addProduct_TR(event, data, formatted)
						{
							document.getElementById("product_autocomplete_input_tkt").value = ""; 
							var link_rewrite = data[31];
							var productName = data[5];
							var productRef = data[36];
							
							tinyMCE.get(\'message\').focus();
							var $body = $(tinymce.activeEditor.getBody());
							$body.prepend(\'<p>\');
							$body.append(\'</p>\');
							$body.find("p:last").append(productName + \' (\'+ productRef +\') <br /><a href="\' + link_rewrite + \'">\' + link_rewrite + \'<\/a><br /><br />\');
							$("body").trigger("click");
							$("#product_autocomplete_input_tkt").trigger("click");
							$("#product_autocomplete_input_tkt").trigger("click");
						}	
						
						
	 
						urlToCall = null;
						
						function getExcludeIds() {
							var ids = "";
							ids += $(\'#excludeIds\').val().replace(/\\-/g,\',\').replace(/\\,$/,\'\');
							return ids;
						}
						
						$(function() {
							$(\'#product_autocomplete_input_tkt\')
								.autocomplete(\'ajax_products_list2.php?products_and_bundles=y&id_customer='.Tools::getValue('id_customer').'\', {
									minChars: 0,
									autoFill: false,
									max:500,
									matchContains: true,
									matchSubset: 0,
									mustMatch:true,
									scroll:true,
									cacheLength:1,
									formatItem: function(item) {
								
												var color = "";
												if(item[24] == 1) {
													var color = "; color:red";
												}
												if(item[28] == 0) {
													return \'</table><div style="position:absolute; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:150px;float:left;margin-left:10px;">Codice</div></th><th style="width:570px;"><div style="width:350px; float:left;">Desc. prodotto<br /><br /></div></th><th style="width:60px; text-align:right"><div style="width:60px;float:left;">Prezzo</div></th><th style="width:50px; text-align:right"><div style="width:60px;float:left;">Qt. mag.</div></th><th style="width:50px; text-align:right"><div style="width:75px;float:left;">Qt. tot</div></th></tr></div><table><tr><td style="width:145px; text-align:left"><br /><br /><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:555px\'+color+\'"><br /><br />\'+item[5]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[23]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[25]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[22]+\'</td></tr></table>\'
													
													;
												}
												else {
													return \'<tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:350px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
													
													;
													}
												}
									
								}).result(addProduct_TR);
							
						});
					
						$("#product_autocomplete_input_tkt").css(\'width\',\'673px\');
						$("#product_autocomplete_input_tkt").keypress(function(event){
									
									  var keycode = (event.keyCode ? event.keyCode : event.which);
									  if (keycode == "13") {
										event.preventDefault();
										event.stopPropagation();    
									  }
									});
									
						$("#product_autocomplete_input_tkt").focus();
					
						$("#product_autocomplete_input_tkt").css(\'width\',\'673px\');
						$("#product_autocomplete_input_tkt").keypress(function(event){
									
									  var keycode = (event.keyCode ? event.keyCode : event.which);
									  if (keycode == "13") {
										  $("#veditutti").trigger("click");
										event.preventDefault();
										event.stopPropagation();    
									  }
									});
						
						
								</script>
								
								<script type="text/javascript">
					
					$("#veditutti2").click ( function (zEvent) {
						
						/*	$("body").trigger("click");
							$("body").trigger("click");
							
							
							
							$("#product_autocomplete_input_tkt").focus();
						//	$("#product_autocomplete_input_tkt").val("");
						//	$("#product_autocomplete_input_tkt").val(" ");
						*/
							var press = jQuery.Event("keypress");
							press.which = 13;
							if(document.getElementById(\'product_autocomplete_input_tkt\').value == "")
							{
								$("#product_autocomplete_input_tkt").val(" ");
							}
							$("#product_autocomplete_input_tkt").trigger(press);
						
							$("#product_autocomplete_input_tkt").trigger("click");
							$("#product_autocomplete_input_tkt").trigger("click");
						//	$("#product_autocomplete_input_tkt").val("");
							
					} );


					</script></td></tr>';
					
					
					
					echo '
					<tr><td><script type="text/javascript">
					
						function addUtil_TR(event, data, formatted)
						{
							tinyMCE.get(\'message\').focus();
							var $body = $(tinymce.activeEditor.getBody());
							$body.prepend(\'<p>\');
							$body.append(\'</p>\');
							if(document.getElementById("utils_autocomplete_input_tkt").value != "-- Seleziona un link utile --")
							{
								$body.find("p:last").append(\'<a href="\' + document.getElementById("utils_autocomplete_input_tkt").value + \'">\' + document.getElementById("utils_autocomplete_input_tkt").value + \'<\/a><br /><br />\');
							}
						}	
						
						
					</script>
					<script type="text/javascript" src="../js/select2.js"></script>
						<script type="text/javascript">
						$(document).ready(function() { $("#utils_autocomplete_input_tkt").select2(); $("#messaggi_precompilati").select2();});
					</script>
					
					Link utili</td><td><select name="utils_autocomplete_input_tkt"  style="width:730px" onchange="addUtil_TR();" id="utils_autocomplete_input_tkt">
					'.$utils_options.'</select>
					</td></tr>';
					
					echo '<tr><td>Messaggi precompilati</td><td><select name="messaggi_precompilati" id="messaggi_precompilati" style="width:730px" onchange="inserisciPrecompilato(this.value);">
					<option value="-- Seleziona un messaggio --">-- Seleziona un messaggio --</option>
					';
					$precompilati = Db::getInstance()->executeS('SELECT * FROM precompilato WHERE active = 1');
						echo '<option value="-- Seleziona un messaggio --">-- Seleziona un messaggio --</option>';
						foreach($precompilati as $precompilato)
						{
							echo '<option value="'.htmlentities($precompilato['testo'], ENT_QUOTES).'">'.htmlentities($precompilato['oggetto'], ENT_QUOTES).'</option>';
						}
					echo '
					</select>
					<script type="text/javascript">
					function inserisciPrecompilato(msg)
					{
						tinyMCE.get(\'message\').focus();
						var $body = $(tinymce.activeEditor.getBody());
						$body.html(\'\');
						$body.prepend(\'<p>\');
						$body.append(\'</p>\');
						if(msg != "-- Seleziona un messaggio --") {
							$body.find("p:last").append(msg);
						}
					}
					</script>
					</td></tr>
					';
					
					echo'			<tr><td>Messaggio</td>
								<td> 
								<br />
								
								<textarea id="message" class="rte" name="message" rows="15" cols="20" style="width:760px;height:220px">
								'.(isset($_POST['modifica_messaggio_submit']) ? stripslashes($_POST['modifica_messaggio_ticket']) : '').'
								
								
								
								</textarea>
								
								
								
								
								</td>
								</tr>';
								
								
								echo'			<tr><td><strong style="color:red">Note private visibili solo da staff</strong></td>
								<td> 
								<br />';
								
								echo '<span class="button" onclick="$(\'#mostra_nota_privata\').slideToggle(); " style="cursor: pointer; display:block; color:red; font-size:14px; font-weight:bold; width:752px; border:1px solid #cccccc; text-align:center; background-color:#f0f0ee; text-decoration:none"><img src="../img/admin/arrow.gif" alt="Dettaglio note private" title="Dettaglio note private" style="float:left; margin-right:5px;"/>Clicca qui per inserire note private/riservate - Non visibili all\'utente.</span>';
				
				
				
								echo '<div id="mostra_nota_privata" style="display:none; margin-top:0px">';
								echo '<textarea id="note_private" class="rte" name="note_private" rows="15" cols="20" style="width:760px;height:130px">
							
								
								
								
								</textarea></div>
								
								
								
								
								</td>
								</tr>';
								
								/*if(isset($_GET['aprinuovoticket'])) {
								
								echo '<tr>
								<td>Nota interna (solo per impiegati)</td>
								<td><textarea id="note" name="note" rows="5" cols="145"></textarea>
								</tr>';
								
								}*/
								$riferimento = Tools::getValue('riferimento') ? Tools::getValue('riferimento') : Db::getInstance()->getValue('SELECT riferimento FROM customer_thread WHERE id_customer_thread = '.$thread['id_customer_thread']);
								$statusPadre = Customer::statusPadre($riferimento);
								echo '<tr><td colspan="2">'.($riferimento != '' && $statusPadre != 'closed' && $statusPadre != '' ? '<div style="border:3px solid red; padding:5px;"><input type="checkbox" name="chiudi_padre" />&nbsp;&nbsp;&nbsp;<strong style="color:red">ATTENZIONE!</strong>  L\'azione padre risulta ancora aperta. Spunta questa casella se vuoi chiuderla.</div>' : '').'</td></tr>';
								
								echo '<tr><td></td>
								<td>
									<button type="submit" class="button"  name="submitMessage" id="submitMessage" value="Modifica">
									<img src="../img/admin/email.gif" alt="Invia" title="Invia" />&nbsp;&nbsp;&nbsp;Invia
									</button>	
								</td>
								</tr>
							</table>
						</fieldset>
					</form>';			

				
						
						if(isset($_GET['id_customer_thread'])) {
					echo '</div>';	 // chiudo ticket-2
			
					}
					
					if(isset($_GET['id_customer_thread'])) {
					echo '<div class="yetii-ticket" id="ticket-3">';
				
				$first = Customer::HierarchyFirst($thread['id_customer_thread'], 'T');
				
				echo '<div class="tree">';
				echo '<ul class="tree-menu"><li>'.Customer::trovaSiglaLinkPerAlbero(substr($first,1), substr($first,0,1),'').'';
				echo str_replace('<ul></ul>','',Customer::HierarchyFindChildren($first, $children, 'T'.$thread['id_customer_thread']));
				echo '</li></ul>';
				echo '</div>';
				echo '<div style="clear:both"></div><br /><br />';
				
					echo '</div>'; // chiudo ticket-3
				}	
				
				if(isset($_GET['id_customer_thread'])) {
				echo '<div class="yetii-ticket" id="ticket-4">';
			
				$storico_ticket = Db::getInstance()->executeS('SELECT * FROM storico_attivita WHERE id_attivita = '.$thread['id_customer_thread'].' AND tipo_attivita = "T" ORDER BY data_attivita DESC, desc_attivita DESC');
				
				if(sizeof($storico_ticket) > 0)
				{
					echo '<table class="table">';
					echo '<thead><tr><th>Persona</th><th>Azione</th><th>Data</th></tr></thead><tbody>';
					
					foreach($storico_ticket as $storico)
					{
						if(is_numeric(substr($storico['desc_attivita'], -1)))
						{	
							$employee = filter_var($storico['desc_attivita'], FILTER_SANITIZE_NUMBER_INT);
							$name_employee = Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM employee WHERE id_employee = '.$employee);
							$desc_attivita = 'Ha messo l\'attivit&agrave; in carico a '.$name_employee;
						}
						else
							$desc_attivita = $storico['desc_attivita'];
						
						echo '<tr><td>'.($storico['id_employee'] == 0 ? 'Cliente' : Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM employee WHERE id_employee = '.$storico['id_employee'])).'</td><td>'.$desc_attivita.'</td><td>'.Tools::displayDate($storico['data_attivita'],$cookie->id_lang, true).'</td></tr>'; 
						
					}	
					
					echo '</tbody></table><br /><br />';
				}
					echo '</div>'; // chiudo ticket-4
				}
				
				if($bdl_ticket > 0  || Tools::getIsset('nuovobdl'))
				{
					echo '<div class="yetii-ticket" id="ticket-4">';
						Customer::BDLform($customer, $bdl_ticket);
					echo '</div>'; // chiudo ticket-5
				}
				
				if(isset($_GET['id_customer_thread'])) {
					echo '</div>'; // chiudo contenitore ticket
				}	
				
				
				echo '<br /><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=6" class="button"><img src="../img/admin/arrow-left.png" alt="Back" title="Back" />&nbsp;&nbsp;&nbsp;Torna alla lista dei ticket</a>';
			
			}
			
			else {
			
				echo Customer::BuildListing($customer->id, 'ticket');
			
				$id_orders = array();
				$id_products = array();
				
				$rowqordmxs = Db::getInstance()->executeS("SELECT id_order FROM orders WHERE id_customer = ".$customer->id."");
				foreach($rowqordmxs as $rowqordmx) {
					$id_orders[] = $rowqordmx['id_order'];
				}

				$rowqprodmxs = Db::getInstance()->executeS("SELECT DISTINCT product_id FROM order_detail JOIN orders ON orders.id_order = order_detail.id_order WHERE id_customer = ".$customer->id."");
				foreach($rowqprodmxs as $rowqprodmx) {
					$id_products[] = $rowqprodmx['product_id'];
				}	
			
				//echo '<br /><h2>Apri un nuovo ticket</h2>';
				

			}
			
			echo '</div>';
		
			}
		// fine tab container 6
		
		
		}
		else { }
		
		if (Tools::isSubmit('submitMessage')) {
			
			
			if(Tools::getValue('isMail') != 1) {
			
				if(Tools::getValue('message') == '') {
				
					$precedente_incarico = Db::getInstance()->getValue("SELECT id_employee FROM customer_thread WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
					$precedente_status = Db::getInstance()->getValue("SELECT status FROM customer_thread WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
			
					Customer::cambiaIncaricoVeloce('ticket', Tools::getValue('in_carico_a'), Tools::getValue('id_customer_thread'), $precedente_incarico, Customer::trovaSigla(Tools::getValue('id_customer_thread'),'ticket'), Tools::getValue('id_customer'));
					
					if($precedente_status != Tools::getValue('status'))
					{	
						switch(Tools::getValue('status'))
						{
							case 'open': $switch_status = 3; break;
							case 'pending1': $switch_status = 4; break;
							case 'closed': $switch_status = 5; break;
							default: $switch_status = 10; break;
						}	
						
						Customer::Storico(Tools::getValue('id_customer_thread'), 'T', $cookie->id_employee, $switch_status);

						Db::getInstance()->executeS("UPDATE customer_thread SET priorita = '".Tools::getValue('priorita')."', status = '".Tools::getValue('status')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
			
					}
					Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&error=1tk&token='.$this->token.'&tab-container-1=6');
								
				
				}
			}
			
			else {
				
				if(Tools::getValue('messaggio') == '') {
				
					$precedente_incarico = Db::getInstance()->getValue("SELECT id_employee FROM customer_thread WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
					$precedente_status = Db::getInstance()->getValue("SELECT status FROM customer_thread WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
			
					Customer::cambiaIncaricoVeloce('ticket', Tools::getValue('in_carico_a_m'), Tools::getValue('id_customer_thread'), $precedente_incarico, Customer::trovaSigla(Tools::getValue('id_customer_thread'),'ticket'), Tools::getValue('id_customer'));
					
					if($precedente_status != Tools::getValue('statusm'))
					{	
						switch(Tools::getValue('statusm'))
						{
							case 'open': $switch_status = 3; break;
							case 'pending1': $switch_status = 4; break;
							case 'closed': $switch_status = 5; break;
							default: $switch_status = 10; break;
						}	
						
						Customer::Storico(Tools::getValue('id_customer_thread'), 'T', $cookie->id_employee, $switch_status);

						Db::getInstance()->executeS("UPDATE customer_thread SET status = '".Tools::getValue('statusm')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
			
					}

					Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&error=1ms&token='.$this->token.'&tab-container-1=7');
								
				
				}
			
			}
	
				if(Tools::getValue('isAnswer') == 1) {
					
						$precedente_incarico = Db::getInstance()->getValue("SELECT id_employee FROM customer_thread WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
						$precedente_status = Db::getInstance()->getValue("SELECT status FROM customer_thread WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
					
						$primaqueryct = ("SELECT id_customer_thread, token FROM "._DB_PREFIX_."customer_thread WHERE id_customer = $_POST[id_customer] AND id_customer_thread = $_POST[id_customer_thread]");
		
						$primarowct = Db::getInstance()->getRow($primaqueryct);
						$id_customer_thread = $primarowct['id_customer_thread'];
						echo $id_customer_thread;
						$newtoken = $primarowct['token'];
						$subj = Mail::l('An answer to your message is available', (int)$ct->id_lang);
						
						if(Tools::getValue('isMail') != 1) {
							
							Customer::cambiaIncaricoVeloce('ticket', Tools::getValue('in_carico_a'), Tools::getValue('id_customer_thread'), $precedente_incarico, Customer::trovaSigla(Tools::getValue('id_customer_thread'),'ticket'), Tools::getValue('id_customer'));
							
							$queryinserimentomsg = ("UPDATE "._DB_PREFIX_."customer_thread SET id_employee = '".Tools::getValue('in_carico_a')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = ".$id_customer_thread."");
							Db::getInstance()->executeS($queryinserimentomsg);
							
						
						}
						
						else {
						
							$precedente_status = Db::getInstance()->getValue("SELECT status FROM customer_thread WHERE id_customer_thread = ".$id_customer_thread."");
							if($precedente_status != Tools::getValue('statusm'))
							{	
								switch(Tools::getValue('statusm'))
								{
									case 'open': $switch_status = 3; break;
									case 'pending1': $switch_status = 4; break;
									case 'closed': $switch_status = 5; break;
									default: $switch_status = 10; break;
								}	
								
								Customer::Storico($id_customer_thread, 'T', $cookie->id_employee, $switch_status);

							}
							
							$queryinserimentomsg = ("UPDATE "._DB_PREFIX_."customer_thread SET status = '".Tools::getValue('statusm')."', date_upd = '".date("Y-m-d H:i:s")."', id_employee = '".Tools::getValue('in_carico_a_m')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = ".$id_customer_thread."");
							Db::getInstance()->executeS($queryinserimentomsg);
						}
					}
					
					else {
						
						$queryct = ("SELECT id_customer_thread FROM "._DB_PREFIX_."customer_thread ORDER BY id_customer_thread DESC");
						$rowct = Db::getInstance()->getRow($queryct);
						$id_customer_thread = $rowct['id_customer_thread']+1;
						$newtoken = Tools::passwdGen(12);
						$subj = "Messaggio da parte di Ezdirect";
						
						$cstmr = Db::getInstance()->getRow("SELECT company, firstname, lastname, is_company FROM customer WHERE id_customer = ".(int)($customer->id)."");
						
						switch(Tools::getValue('id_contact')) {
						case 2: $titolomessaggio = "Ticket per amministrazione - contabilita"; break;
						case 3: $titolomessaggio = "Ticket rivenditori"; break;
						case 4: $titolomessaggio = "Ticket per assistenza"; break;
						case 5: $titolomessaggio = "Ticket servizio clienti e ufficio vendite"; break;
						case 6: $titolomessaggio = "Ticket ti richiamiamo noi"; break;
						case 7: $titolomessaggio = "Messaggio semplice"; break;
						case 8: $titolomessaggio = "Ticket per amministrazione - ordini"; break;
						default: $titolomessaggio = "Messaggio da modulo di contatto"; break;
						
						}
						
						if($cstmr['is_company'] == 1) { $cstmr_r = $cstmr['company']; } else { $cstmr_r = $cstmr['firstname']." ".$cstmr['lastname']; }
						
						$subject = "*** RICHIESTA TICKET da ".$cstmr_r." - Tipo: ".$titolomessaggio." ***";
						
						if(Tools::getValue('isMail') == 1) {
						
						$subject = "*** MESSAGGIO da ".$cstmr_r." ***";
						
						$queryinserimentomsg = ("INSERT INTO "._DB_PREFIX_."customer_thread (id_customer_thread, id_lang, id_contact, id_customer, id_employee, subject, id_order, id_product, status, email, riferimento, token, date_add, date_upd, note) VALUES ('".$id_customer_thread."', '".$cookie->id_lang."',  '7', '".Tools::getValue('id_customer')."', '".$cookie->id_employee."', '".addslashes($subject)."', '$_POST[id_order]', '$_POST[id_product]', 'open', '$_POST[customer_email_msg]', '".Tools::getValue('riferimento')."', '".$newtoken."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."', '".addslashes(Tools::getValue('note'))."')");
						
						}
						else {
							
							$queryinserimentomsg = ("INSERT INTO "._DB_PREFIX_."customer_thread (id_customer_thread, id_lang, id_contact, id_customer, id_employee, subject, id_order, id_product, status, email, riferimento, token, date_add, date_upd, note) VALUES ('".$id_customer_thread."', '".$cookie->id_lang."',  '".Tools::getValue('id_contact')."', '".Tools::getValue('id_customer')."', '".Tools::getValue('in_carico_a')."', '".addslashes($subject)."', '$_POST[id_order]', '$_POST[id_product]', 'open', '$_POST[customer_email_msg]', '".Tools::getValue('riferimento')."', '".$newtoken."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."', '".addslashes(Tools::getValue('note'))."')");
							
							if(Tools::getValue('id_contact') == 9)
							{
								$chars_per_codice = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
								for ($i = 0, $codice_rma = ''; $i < 5; $i++) {
									$codice_rma .= Tools::substr($chars_per_codice, mt_rand(0, Tools::strlen($chars_per_codice) - 1), 1);
								}
								
								$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
								$action_thread++;
								$action_subject = "*** RMA SU ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - Tipo richiesta: RMA (Aperto da ".Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$cookie->id_employee).") *** ";
								
								/*Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd) VALUES ('$action_thread', 'Richiesta_RMA', '".$customer->id."', '".addslashes($action_subject)."', 0, 4, 'open', 'T".$id_customer_thread."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");   
*/
								$siglarma = 'S'.substr(date('Y'), 2,2).$id_customer_thread;
								/*
								Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', ".$customer->id.", 0, 4, 4, '', '<strong>*** RMA***</strong><br /><br /><strong>Codice RMA</strong>: ".$codice_rma."<br /><strong>Codice Ticket</strong>: ".$siglarma."<br /><strong>Template RMA</strong>: <a href=\"http://www.ezdirect.it/rma/".$codice_rma.".html\" target=\"_blank\">http://www.ezdirect.it/rma/".$codice_rma.".html</a><br /><strong>Messaggio del cliente</strong>: ".addslashes(Tools::getValue('message'))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
								*/
								$id_address_rma = Db::getInstance()->getValue('SELECT id_address FROM address WHERE id_customer = '.$customer->id.' AND fatturazione = 1 AND deleted = 0 AND active = 1');
								
								Db::getInstance()->executeS("INSERT INTO rma (id_customer_thread, codice_rma, rma_product, seriale, qta, rma_shipping, rma_type, id_address) VALUES (".$id_customer_thread.", '".$codice_rma."', '', '', '', '', '', ".$id_address_rma.")");
							}
							
						}
						echo $queryinserimentomsg;
						
						Db::getInstance()->executeS($queryinserimentomsg);
					}
					

					$ct = new CustomerThread($id_customer_thread);
					$cm = new CustomerMessage();
					
					if(Tools::getValue('apri-todo') != '')
					{
						
						$action_threadz = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
						$action_threadz++;
						$action_subjectz = "*** PROMEMORIA TICKET ".Customer::trovaSigla($id_customer_thread,'ticket')." *** ";
						$id_employee = Tools::getValue('apri-todo');
						
						Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd, promemoria) VALUES ('$action_threadz', 'Attivita', '".$customer->id."', '".addslashes($action_subjectz)."', 0, ".$id_employee.", 'open', 'T".$id_customer_thread."', '".date("Y-m-d H:i:s", strtotime('+1 hours'))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."',1)");   

						Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, telefono_cliente, date_add, date_upd) VALUES (NULL, '$action_threadz', ".$customer->id.", 99, ".$id_employee.", ".$id_employee.", '', '<strong>*** PROMEMORIA TICKET  ".Customer::trovaSigla($id_customer_thread,'ticket')." ***</strong><br /><br />".Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$cookie->id_employe)." ti ha aperto questo todo per ricordarti di gestire il ticket ".Customer::trovaSigla($id_customer_thread,'ticket').". <a href=\"https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$customer->id."&viewcustomer&viewticket&id_customer_thread=".$id_customer_thread."&token=".Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').$id_employee)."&tab-container-1=6\" target=\"_blank\">clicca qui per aprirlo</a>', '".Tools::getValue('telefono_cliente')."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
						
						Db::getInstance()->execute('INSERT INTO ticket_promemoria (msg, tipo_att, ora, conto, tipo, status) VALUES ('.$action_threadz.', "a", "'.date("Y-m-d H:i:s", strtotime('+1 hours')).'" - INTERVAL 30 MINUTE, "30", "MINUTE", "open")');
					}
					
					$getstatus = Tools::getValue('status');
					
					Customer::chiudiPadre(Db::getInstance()->getValue('SELECT riferimento FROM customer_thread WHERE id_customer_thread = '.$id_customer_thread));
					
					if(Tools::getIsset('riferimento'))
					{
						Db::getInstance()->ExecuteS("UPDATE customer_thread SET riferimento = '".Tools::getValue('riferimento')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = ".$id_customer_thread."");
					}	
					if(Tools::getIsset('riferimentoo') && Tools::getValue('riferimentoo') != '')
					{
						if(substr(Tools::getValue('riferimentoo'),0,1) == 'C')
						{
							$note_da_copiare = Db::getInstance()->executeS('SELECT * FROM cart_note WHERE id_cart = "'.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo'))).'"');
						}
						else if(substr(Tools::getValue('riferimentoo'),0,1) == 'O')
						{
							$note_da_copiare = Db::getInstance()->executeS('SELECT * FROM cart_note WHERE id_cart = "'.Db::getInstance()->getValue('SELECT id_cart FROM orders WHERE id_order = '.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo')))).'"');
						}
						else
						{ 
							$note_da_copiare = Db::getInstance()->executeS('SELECT * FROM note_attivita WHERE id_attivita = "'.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo'))).'" AND tipo_attivita = "'.substr(Tools::getValue('riferimentoo'),0,1).'"');
						}
						foreach($note_da_copiare as $ndc)
						{
							Db::getInstance()->executeS('INSERT INTO note_attivita (id_note, id_attivita, tipo_attivita, id_employee, note, date_add, date_upd) VALUES (NULL, '.$id_customer_thread.', "T", "'.$ndc['id_employee'].'","'.$ndc['note'].'", "'.$ndc['date_add'].'","'.$ndc['date_upd'].'")');
						}
					}	
					
					if(!empty($getstatus)) {
						
						$precedente_status = Db::getInstance()->getValue("SELECT status FROM customer_thread WHERE id_customer_thread = ".$id_customer_thread."");
						if($precedente_status != Tools::getValue('status'))
						{	
							switch(Tools::getValue('status'))
							{
								case 'open': $switch_status = 3; break;
								case 'pending1': $switch_status = 4; break;
								case 'closed': $switch_status = 5; break;
								default: $switch_status = 10; break;
							}	
							
							Customer::Storico($id_customer_thread, 'T', $cookie->id_employee, $switch_status);
				
						}
					
					Db::getInstance()->ExecuteS("UPDATE customer_thread SET status = '".Tools::getValue('status')."', priorita = '".Tools::getValue('priorita')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = ".$id_customer_thread."");
					
					//$ct->status = Tools::getValue('status');
					}
					if(Tools::getValue('isMail') == 1) {
						$cm->id_employee = (int)$cookie->id_employee;
					}
					else {
						if(Tools::getValue('isAnswer') == 1) {
						
						$cm->id_employee = (int)$cookie->id_employee;
						}
						
						else {
						$cm->id_employee = (int)$cookie->id_employee;
						}
					}
					$cm->id_customer_thread = $ct->id;
					if(Tools::getValue('isMail') == 1) {
						$cm->message = Tools::htmlentitiesutf8(Tools::getValue('messaggio'));
					}
					else {
						$cm->message = Tools::htmlentitiesutf8(Tools::getValue('message'));
					}
					$cm->ip_address = ip2long($_SERVER['REMOTE_ADDR']);
		
					
					if($customer->id == 0) {
						/*$attachments = array();
						
						$files = explode(":::",$_POST['joinFile']);
						foreach ($files as $file) 
						{
							if($file == "") {
							}
							else {
							
								$extension = array('.txt', '.rtf', '.doc', '.docx', '.pdf', '.zip', '.png', '.jpeg', '.gif', '.jpg');
								$filename = md5(uniqid().substr($file, -5));
								$fileAttachment['content'] = file_get_contents('/var/www/vhosts/ezdirect.it/httpdocs'.$file);
								$fileAttachment['name'] = substr( $file, strrpos( $file, '/' )+1 );
								$fileAttachment['mime'] = Tools::getFileMimeType('/var/www/vhosts/ezdirect.it/httpdocs'.$file);
								
								if (isset($filename)) {
									$filename = $filename.":::".'http://www.ezdirect.it'.$file;
									$file_name .= $filename.";";
								}
								
								if(Tools::getIsset('invio_con_bdl_pdf'))
								{
									require_once('../classes/html2pdf/html2pdf.class.php');
									$content = $this->generatePDFBDL(Tools::getValue('invio_con_bdl_pdf'), 'ezdirect');
									
									$html2pdf = new HTML2PDF('P','A4','it');
									$html2pdf->WriteHTML($content);
									
									$pdfdoc = $html2pdf->Output('', true);

									$fileAttachment['content'] = $pdfdoc;
									$fileAttachment['name'] = Tools::getValue('invio_con_bdl_pdf').'-buono-di-lavoro.pdf';
									$fileAttachment['mime'] = 'application/x-download';

									$attachments[] = $fileAttachment;
								}
								
								$attachments[] = $fileAttachment;
							}
						}*/
					}
					
					else {
						$files=$_FILES["joinFile"];

						$files=array();
						$fdata=$_FILES['joinFile'];
						if(is_array($fdata['name'])){
							$countf_name = count($fdata['name']);
							for($i=0;$i<$countf_name;++$i){
								$files[]=array(
									'name'    =>$fdata['name'][$i],
									'tmp_name'=>$fdata['tmp_name'][$i],
									'type' => $fdata['type'][$i],
									'size' => $fdata['size'][$i],
									'error' => $fdata['error'][$i],
								);
							}
						}
						else 
							$files[]=$fdata;
											
						$attachments = array();
						
						if(Tools::getIsset('invio_con_bdl_pdf')) {
							require_once('../classes/html2pdf/html2pdf.class.php');
							$content = $this->generatePDFBDL(Tools::getValue('invio_con_bdl_pdf'), 'ezdirect');
							
							$html2pdf = new HTML2PDF('P','A4','it');
							$html2pdf->WriteHTML($content);
							
							$pdfdoc = $html2pdf->Output('', true);

							$fileAttachment1['content'] = $pdfdoc;
							$fileAttachment1['name'] = Tools::getValue('invio_con_bdl_pdf').'-buono-di-lavoro.pdf';
							$fileAttachment1['mime'] = 'application/x-download';
							
							$filename = "BDL".Tools::getValue('invio_con_bdl_pdf').":::".Tools::getValue('invio_con_bdl_pdf').'-buono-di-lavoro.pdf';
							$file_name .= $filename.";";
						
							$attachments[] = $fileAttachment1;
						}
						
						foreach($files as $file) {
						
						
							if (isset($files) AND !empty($file['name']) AND $file['error'] != 0)
								$this->_errors[] = Tools::displayError('An error occurred with the file upload.');
								
								if (!empty($file['name']))
									{
										$extension = array('.txt', '.rtf', '.doc', '.docx', '.pdf', '.zip', '.png', '.jpeg', '.gif', '.jpg');
										$filename = md5(uniqid().substr($file['name'], -5));
										$fileAttachment['content'] = file_get_contents($file['tmp_name']);
										$fileAttachment['name'] = $file['name'];
										$fileAttachment['mime'] = $file['type'];
									}
									
									
									
									if (isset($filename) AND rename($file['tmp_name'], _PS_MODULE_DIR_.'../upload/'.$filename)) {
										$filename = $filename.":::".$file['name'];
										$file_name .= $filename.";";
										
								}
								
								$attachments[] = $fileAttachment;
								
						}
					}
					
					$cm->file_name = $file_name;	
					
					
					if ($cm->add())
					{
						
						$idmessaggio_ticket = mysqli_insert_id($link_glob);
						
						if(Tools::getValue('isMail') == 1) {
							Db::getInstance()->ExecuteS("UPDATE customer_message SET note_private = '".addslashes(Tools::getValue('note_private'))."', in_carico = '".Tools::getValue('in_carico_a_m')."', email = '".Tools::getValue('customer_email_msg')."' WHERE id_customer_message = ".$idmessaggio_ticket."");
						}
						else
						{
							Db::getInstance()->ExecuteS("UPDATE customer_message SET note_private = '".addslashes(Tools::getValue('note_private'))."', in_carico = '".Tools::getValue('in_carico_a')."', email = '".Tools::getValue('customer_email_msg')."' WHERE id_customer_message = ".$idmessaggio_ticket."");
						}
				
						$tickettipo = $ct->id_contact;

						if(Tools::getValue('isMail') == 1) {
							$sigla = "M";
						}
						else {
							if ($tickettipo == 4){
								$sigla = "T";
							}
							else if ($tickettipo == 2){
								$sigla = "A";
							} 
							else if ($tickettipo == 3){
								$sigla = "V";
							} 
							else if ($tickettipo == 6){
								$sigla = "R";
							}
							else if ($tickettipo == 8){
								$sigla = "D";
							} 
							else if ($tickettipo == 9){
								$sigla = "S";
							} 
						}
						
						$anno = $ct->date_add;
						
						$anno = substr($anno,2,2);
						
						$id_thread_ticket = $ct->id;
						
						$id_ticket = $sigla.$anno.$id_thread_ticket;
						$numerounivoco_ticket = $sigla.$anno.$id_thread_ticket.$idmessaggio_ticket;
						
						$primo_ticket = Db::getInstance()->getValue("SELECT id_customer_message FROM customer_message WHERE id_customer_thread = $id_thread_ticket ORDER BY id_customer_message ASC");
						
						if(!$primo_ticket) {
									
							$primo_ticket = $idmessaggio_ticket;
								
						}
								
						else {
								
						}
						
						$cfpicr = Db::getInstance()->getValue("SELECT id_customer FROM customer WHERE id_customer = $ct->id_customer");
						$addretc = Db::getInstance()->getValue("SELECT id_address FROM address WHERE fatturazione = 1 AND active = 1 AND deleted = 0 AND id_customer = $ct->id_customer");
						
						$in_carico_a =  Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".Tools::getValue('in_carico_a')."");
						
						$clixcsv = new Customer($cfpicr);
						$addxcsv = new Address($addretc);
						
						$provinciaxcsv = Db::getInstance()->getRow("SELECT iso_code FROM state WHERE id_state = $addxcsv->id_state");
						
						$primo_ticket = $sigla.$anno.$primo_ticket;
						$messaggioclientepercsv = Tools::getValue('message');
						$messaggioclientepercsv = strip_tags($messaggioclientepercsv);
						$messaggioclientepercsv = str_replace(";", ",", $messaggioclientepercsv);
						
						$messaggioclientepercsv = str_replace(chr(10), " ", $messaggioclientepercsv); //remove carriage returns
						$messaggioclientepercsv = str_replace(chr(13), " ", $messaggioclientepercsv); //remove carriage returns	
						
						$messaggioclientepercsv2 = explode("Cordiali saluti / Best regards", $messaggioclientepercsv);
						
						$messaggioclientepercsv = $messaggioclientepercsv2[0];
						
						$employeeincaricato = new Employee($cookie->id_employee);
						$employeeincaricato2 = new Employee(Tools::getValue('in_carico_a'));
						
						if(Tools::getValue('isMail') == 1) {
							$ref = rand(0, 99999); $ref2 = rand(0, 99999);
							
								$file_csv=fopen("../cms/quotazioni/messaggi.csv","a+");
								if(Tools::getValue('isAnswer') == 1) {
									$riga_richiesta = "".date("d/m/Y H:i:s")." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;;;;;;;".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;Accettata;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;R;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								else {
									$riga_richiesta = "".date("d/m/Y H:i:s")." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$clixcsv->firstname.";".$clixcsv->lastname.";".$addxcsv->address1.";".$addxcsv->postcode.";".$addxcsv->city.";".$provincia.";".$addxcsv->phone.";".$addxcsv->fax.";".$clixcsv->email.";".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;Accettata;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;D;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								@fwrite($file_csv,$riga_richiesta);
								@fclose($file_csv);	
								
						} else {
							if ($tickettipo == 4){
								$ref = rand(0, 99999); $ref2 = rand(0, 99999);
						
								$file_csv=fopen("../cms/assistenza_no_contratto_csv/csv-assistenza-output.csv","a+");
								
								if(Tools::getValue('isAnswer') == 1) {
									$riga_richiesta = "".date('H:i:s d F Y')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;;;;;;;;;;;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;R;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								else {
									$riga_richiesta = "".date('H:i:s d F Y')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;".$clixcsv->firstname.";".$clixcsv->lastname.";;".$clixcsv->vat_number.";".$clixcsv->tax_code.";".$addxcsv->address1.";".$addxcsv->city.";".$addxcsv->postcode.";".$provincia.";".$clixcsv->email.";".$addxcsv->phone.";".$addxcsv->fax.";".$addxcsv->phone_mobile.";;;;;;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;D;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								
								
								@fwrite($file_csv,$riga_richiesta);
								@fclose($file_csv);	
							}
							else if ($tickettipo == 2){
								$ref = rand(0, 99999); $ref2 = rand(0, 99999);
							
								$file_csv=fopen("../cms/amministrativa_ordini_csv/csv-EZamm-contabilita-output.csv","a+");
								if(Tools::getValue('isAnswer') == 1) {
									$riga_richiesta = "".date('H:i:s d L a')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;;;;;;;".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;Accettata;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;R;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								else {
									$riga_richiesta = "".date('H:i:s d L a')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$clixcsv->firstname.";".$clixcsv->lastname.";".$addxcsv->address1.";".$addxcsv->postcode.";".$addxcsv->city.";".$provincia.";".$addxcsv->phone.";".$addxcsv->fax.";".$clixcsv->email.";".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;Accettata;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;D;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								@fwrite($file_csv,$riga_richiesta);
								@fclose($file_csv);	
							} 
							else if ($tickettipo == 8){
								$ref = rand(0, 99999); $ref2 = rand(0, 99999);
							
								$file_csv=fopen("../cms/amministrativa_ordini_csv/csv-EZamm-ordini-output.csv","a+");
								if(Tools::getValue('isAnswer') == 1) {
									$riga_richiesta = "".date('H:i:s d L a')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;;;;;;;".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;Accettata;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;R;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								else {
									$riga_richiesta = "".date('H:i:s d L a')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$clixcsv->firstname.";".$clixcsv->lastname.";".$addxcsv->address1.";".$addxcsv->postcode.";".$addxcsv->city.";".$provincia.";".$addxcsv->phone.";".$addxcsv->fax.";".$clixcsv->email.";".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;Accettata;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;D;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								@fwrite($file_csv,$riga_richiesta);
								@fclose($file_csv);	
							} 
							else if ($tickettipo == 3){
								$ref = rand(0, 99999); $ref2 = rand(0, 99999);
		
								$data_quotazione = date("d/m/Y H:i:s");

								$file_csv=fopen("../cms/quotazioni/rivenditori.csv","a+");
								if(Tools::getValue('isAnswer') == 1) {
									$riga_richiesta = ";".$data_quotazione.";".date('Ymd')."-".$ref."-".$ref2.";;".$clixcsv->vat_number.";".$clixcsv->tax_code.";".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;;0;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;R;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								else {
									$riga_richiesta = ";".$data_quotazione.";".date('Ymd')."-".$ref."-".$ref2.";;".$clixcsv->vat_number.";".$clixcsv->tax_code.";".$clixcsv->firstname.";".$clixcsv->lastname.";;;0;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;D;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}
								@fwrite($file_csv,$riga_richiesta);
								@fclose($file_csv);	
							} 
							else if ($tickettipo == 6){
								$ref = rand(0, 99999); $ref2 = rand(0, 99999);
								$file_csv=fopen("../cms/callmeback/csv-EZcall-output.csv","a+");

								if(Tools::getValue('isAnswer') == 1) {
									$riga_richiesta = "".date('H:i:s d F Y')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;;;;;;;;;".$cfpicr['vat_number'].";".$cfpicr['tax_code'].";".$messaggioclientepercsv.";;Accettata;$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;R;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
								}

								@fwrite($file_csv,$riga_richiesta);
								@fclose($file_csv);	
							}
						
						}
						
						if(Tools::getValue('isMail') == 1) {
							
							if(Tools::getValue('isAnswer') == 1) {
								$params = array(
								'{reply}' => (Tools::getIsset('mail_solo_testo') ? strip_tags($gentilecliente.Tools::getValue('messaggio')) : $gentilecliente.Tools::getValue('messaggio')),
'{msg}' => (Tools::getIsset('mail_solo_testo') ? strip_tags($gentilecliente.Tools::getValue('messaggio')) : $gentilecliente.Tools::getValue('messaggio')),
								'{firma}' => $firma,
								'{id_richiesta}' => "Id richiesta: <strong>".$id_ticket."</strong>",
								'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token);
							}
							
							else {
								$params = array(
									'{reply}' => (Tools::getIsset('mail_solo_testo') ? strip_tags($gentilecliente.Tools::getValue('messaggio')) : $gentilecliente.Tools::getValue('messaggio')),
									'{msg}' => (Tools::getIsset('mail_solo_testo') ? strip_tags($gentilecliente.Tools::getValue('messaggio')) : $gentilecliente.Tools::getValue('messaggio')),
									'{firma}' => $firma,
									'{id_richiesta}' => "Id richiesta: <strong>".$id_ticket."</strong>",
									'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token
								);
							}
						}
						else {
							if(Tools::getValue('isAnswer') == 1) {
								$params = array(
									'{reply}' => $gentilecliente.(Tools::getValue('message')),
									'{firma}' => $firma,
									'{id_richiesta}' => "Id ticket: <strong>".$id_ticket."</strong>",
									'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token
								);
							}
							else {
								Customer::Storico($id_thread_ticket, 'T', $cookie->id_employee, 11);
								
								$params = array(
									'{reply}' => $gentilecliente."&egrave; stato aperto un ticket dal nostro operatore ".$employeeincaricato->firstname." ".$employeeincaricato->lastname.". Il testo del ticket:<br /><br />".(Tools::getValue('message')),
									'{firma}' => $firma,
									'{id_richiesta}' => "Id ticket: <strong>".$id_ticket."</strong>",
									'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token
								);

								$tokenimp = Tools::getAdminToken("AdminCustomers"."2".$employeeincaricato->id);
								$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewticket&id_customer_thread=".$ct->id."&token=".$tokenimp.'&tab-container-1=6';	
								if($mailcontatto == 'assistenza@ezdirect.it') {
									$linkpaolo = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewticket&id_customer_thread=".$ct->id."&token=".Tools::getAdminToken("AdminCustomers"."2"."5").'&tab-container-1=6';	
									$linkmassimo = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewticket&id_customer_thread=".$ct->id."&token=".Tools::getAdminToken("AdminCustomers"."2"."4").'&tab-container-1=6';		
									$linklorenzo = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewticket&id_customer_thread=".$ct->id."&token=".Tools::getAdminToken("AdminCustomers"."2"."12").'&tab-container-1=6';	
									$linkleonardo = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewticket&id_customer_thread=".$ct->id."&token=".Tools::getAdminToken("AdminCustomers"."2"."17").'&tab-container-1=6';
									$stringrisposta = 'Ticket assistenza<br />
									<a href="'.$linkpaolo.'">Paolo - Per rispondere clicca qui</a><br />
									<a href="'.$linkmassimo.'">Massimo - Per rispondere clicca qui</a><br />
									<a href="'.$linklorenzo.'">Lorenzo - Per rispondere clicca qui</a><br />
									<a href="'.$linkleonardo.'">Leonardo - Per rispondere clicca qui</a><br />
									';
								}
								
								$params2 = array(
								'{reply}' => "<strong>".$employeeincaricato->firstname." ".$employeeincaricato->lastname."</strong> ha aperto un ticket per conto di un cliente.<br /><br />
								Il ticket &egrave; in carico a: <strong>".$in_carico_a."</strong><br /><br />
								L'id del ticket &egrave;: <strong>".$id_ticket."</strong><br /><br />
								".($mailcontatto == 'assistenza@ezdirect.it' ? $stringrisposta : "<a href='".$linkimp."'>Per rispondere clicca qui</a>.")." Di seguito il testo del ticket:<br /><br />".(Tools::getValue('message'))."
								");
								
								
								$mailcontatto = Db::getInstance()->getValue('
									SELECT email 
									FROM contact 
									WHERE id_contact = '.$ct->id_contact.'
								');
								
								if($is_supplier == 1) {
									Mail::Send(5, 'risposta_fornitore', Mail::l('Aperto ticket su Ezdirect', 5), $params2, $mailcontatto, NULL, 'info@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true); 
								}
								else {
									Mail::Send(5, 'action', Mail::l('Aperto ticket per conto del cliente', 5), 
									$params2, $mailcontatto, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
									_PS_MAIL_DIR_, true);
								}
							}	
							$this->assegna_incarico(Tools::getValue('in_carico_a'), $ct->id, 'ticket');
						}
						
						$in_carico_a =  Db::getInstance()->getValue("
							SELECT firstname 
							FROM employee 
							WHERE id_employee = ".Tools::getValue('in_carico_a')."
						");

						if(empty($in_carico_a)) 
							$in_carico_a = "Nessuno";
				
						if(Tools::getValue('isMail') == 1) {
				
							$mails_message = explode(";", Tools::getValue('customer_email_msg'));
						
							foreach ($mails_message as $mail) {
								$mail = trim($mail);
								Customer::findEmailPersona($mail, Tools::getValue('id_customer'));

								if($is_supplier == 1) {
									if (Mail::Send(5, 'risposta_fornitore', Mail::l('Messaggio da Ezdirect', 5), 
										$params, $mail, NULL, 'info@ezdirect.it', 'Ezdirect', $attachments, NULL, 
										_PS_MAIL_DIR_, true)) {
										
										//$ct->status = 'closed';
										//$ct->update();
										/*$mail_scrivente = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = ".$cookie->id_employee."");
										
										Mail::Send(5, 'msg_base', Mail::l('Copia messaggio inviato al cliente', 5), 
										$params, $mail_scrivente, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
										_PS_MAIL_DIR_, true);*/
										Customer::Storico($id_thread_ticket, 'T', $cookie->id_employee, 7);
										$log_mail=fopen("../import/log-mail.txt","a+");
										$riga_log = "MSG | Mail inviata a ".$mail." in data ".date("Y-m-d H:i:s")."\n";
										@fwrite($log_mail,$riga_log);
										@fclose($log_mail);
									}
								}
								else {

									if (Mail::Send(5, 'simple_msg', Mail::l('Messaggio da Ezdirect', 5), 
										$params, $mail, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
										_PS_MAIL_DIR_, true))
									{
										Customer::Storico($id_thread_ticket, 'T', $cookie->id_employee, 7);
										//$ct->status = 'closed';
										//$ct->update();
										/*$mail_scrivente = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = ".$cookie->id_employee."");
										
										Mail::Send(5, 'simple_msg', Mail::l('Copia messaggio inviato al cliente', 5), 
										$params, $mail_scrivente, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
										_PS_MAIL_DIR_, true);*/
										
										$log_mail=fopen("../import/log-mail.txt","a+");
										$riga_log = "MSG | Mail inviata a ".$mail." in data ".date("Y-m-d H:i:s")."\n";
										@fwrite($log_mail,$riga_log);
										@fclose($log_mail);
									}
								}
							}
							
							
							$cc = "";
							$cc_str = "";
							foreach($_POST['conoscenza'] as $conoscenza) {
								
								$cc_str = $cc_str.Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$conoscenza).' ';
							}
								
							foreach($_POST['conoscenza'] as $conoscenza) {

								$tokenimp = Tools::getAdminToken("AdminCustomers"."2".$conoscenza);
								$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewmessage&id_mex=".$ct->id."&token=".$tokenimp.'&tab-container-1=7';	
								
								$params4 = array(
								'{reply}' => "<strong>".$employeemess->firstname."</strong> ha inviato una comunicazione al cliente e ti ha messo in copia conoscenza. <br /><br />
								Id messaggio: <strong>".$id_ticket."</strong><br /><br />
								Cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id."<br /><br />
								Il messaggio &egrave; in carico a: <strong>".$in_carico_a."</strong><br /><br />	
								".($cc_str != '' ? "Il messaggio &egrave; stato inviato in conoscenza a: ".$cc_str."<br /><br />" : "")."
								<a href='".$linkimp."'>Per aprire il messaggio fai clic qua</a>.",
							
								'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token);
								
								$cc .= $conoscenza.";";
						
								$mailconoscenza = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = $conoscenza");
								
								Mail::Send(5, 'senzagrafica', Mail::l('Messaggio cliente in copia conoscenza', 5), 
								$params4, $mailconoscenza, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
								_PS_MAIL_DIR_, true);
							
							
							}
							
							if(!empty($_POST['conoscenza'])) {
							
								Db::getInstance()->executeS("INSERT INTO ticket_cc (msg, type, cc) VALUES (".$idmessaggio_ticket.", 't', '".$cc."')"); 
							
							
							}
							
							
							
						}
						
						else {
							$mails_ticket = explode(";", Tools::getValue('customer_email_msg'));
							
							foreach ($mails_ticket as $mail) {
								$mail = trim($mail);
								Customer::findEmailPersona($mail, Tools::getValue('id_customer'));
								if($is_supplier == 1) {
									if (Mail::Send(5, 'risposta_fornitore', Mail::l('Messaggio da Ezdirect', 5), 
										$params, $mail, NULL, 'info@ezdirect.it', 'Ezdirect', $attachments, NULL, 
										_PS_MAIL_DIR_, true))
									{
										
										//$ct->status = 'closed';
										//$ct->update();
										/*$mail_scrivente = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = ".$cookie->id_employee."");
										
										Mail::Send(5, 'msg_base', Mail::l('Copia messaggio inviato al cliente', 5), 
										$params, $mail_scrivente, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
										_PS_MAIL_DIR_, true);*/
										Customer::Storico($id_thread_ticket, 'T', $cookie->id_employee, 7);
										$log_mail=fopen("../import/log-mail.txt","a+");
										$riga_log = "MSG | Mail inviata a ".$mail." in data ".date("Y-m-d H:i:s")."\n";
										@fwrite($log_mail,$riga_log);
										@fclose($log_mail);
									}
								}	
								else {
								
									if (Mail::Send((int)$ct->id_lang, 'reply_msg', Mail::l('Messaggio da Ezdirect', (int)$ct->id_lang), 
										$params, $mail, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
										_PS_MAIL_DIR_, true))
									{
										
										//$ct->status = 'closed';
										//$ct->update();
										/*
										$mail_scrivente = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = ".$cookie->id_employee."");
									
										Mail::Send(5, 'simple_msg', Mail::l('Copia ticket inviato al cliente', 5), 
										$params, $mail_scrivente, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
										_PS_MAIL_DIR_, true);
										*/
										Customer::Storico($id_thread_ticket, 'T', $cookie->id_employee, 7);
										$log_mail=fopen("../import/log-mail.txt","a+");
										$riga_log = "TKT | Mail inviata a ".$mail." in data ".date("Y-m-d H:i:s")."\n";
										@fwrite($log_mail,$riga_log);
										@fclose($log_mail);
									}
								}
							}
							
							
							$cc = "";
							
							$cc_str = "";
							foreach($_POST['conoscenza'] as $conoscenza) {
								
								$cc_str = $cc_str.Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$conoscenza).' ';
							}
							
							foreach($_POST['conoscenza'] as $conoscenza) {
								
								$tokenimp = Tools::getAdminToken("AdminCustomers"."2".$conoscenza);
								$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewticket&id_customer_thread=".$ct->id."&token=".$tokenimp.'&tab-container-1=6';	
								
								$params4 = array(
								'{reply}' => "<strong>".$employeemess->firstname."</strong> ha inviato una comunicazione di un ticket al cliente e ti ha messo in copia conoscenza. <br /><br />
								Id ticket: <strong>".$id_ticket."</strong><br /><br />
								Cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id."<br /><br />
								Il ticket &egrave; in carico a: <strong>".$in_carico_a."</strong><br /><br />	
								Il messaggio &egrave; stato inviato in conoscenza a: ".$cc_str."<br /><br />								
								<a href='".$linkimp."'>Per aprire il ticket fai clic qua</a>.",
							
								'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token);
								
								$cc .= $conoscenza.";";
								$mailconoscenza = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = $conoscenza");
								
								Mail::Send((int)$ct->id_lang, 'senzagrafica', Mail::l('Ticket utente in copia conoscenza', (int)$ct->id_lang), 
								$params4, $mailconoscenza, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
								_PS_MAIL_DIR_, true);
							
							
							}
							if(!empty($_POST['conoscenza'])) {
							
								Db::getInstance()->executeS("INSERT INTO ticket_cc (msg, type, cc) VALUES (".$idmessaggio_ticket.", 't', '".$cc."')"); 
							
							
							}
						}

						if(Tools::getValue('isMail') == 1) {
							Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&token='.$this->token.'&conf=4&tab-container-1=7');
						}
						else {
							Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&viewticket&id_customer_thread='.$ct->id.'&messagesent&token='.$this->token.'&conf=4&tab-container-1=6');
						}
					}
					else
						$this->_errors[] = Tools::displayError('An error occurred, your message was not sent. Please contact your system administrator.');
				
			}
			
			
				else {
				
					if (isset($_GET['messagesent'])) {
				
					/*echo 'Ticket aperto con successo!<br /> <a class="button" href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=6"><strong>Clicca qui per aprire un nuovo ticket</strong></a>';
					*/
					}
				
					else {

					
			}
		}
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 7) {
		
		
		// INIZIO PREVENTIVIIIIII!!!!!!
		if($cookie->profile == 7 && $agente_c != $cookie->id_employee)
			echo 'Non hai i permessi per visualizzare questa sezione';
		else
		{
			echo '<div class="tab1" id="preventivi-customer">';
			echo '<h2>Azioni cliente</h2>';
			$impiegati_ezp = Db::getInstance()->ExecuteS("SELECT id_employee, firstname, lastname FROM employee WHERE active = 1 ORDER BY firstname ASC, lastname ASC");
			
			if(isset($_GET['id_thread'])) {
					
				$thread = Db::getInstance()->getRow("SELECT * FROM form_prevendita_thread ct WHERE ct.id_thread = ".$_GET['id_thread']." AND ct.id_customer = ".$customer->id."");
				
				if(!$thread && Tools::getValue('id_thread') != '') {
					echo "<span style='color:red'>ERRORE - Azione non trovata</span><br /><br /><br />"; die();
				}
				
				$tipo_richiesta_p = Db::getInstance()->getValue("SELECT tipo_richiesta FROM form_prevendita_thread ft WHERE ft.id_thread = ".$_GET['id_thread']."");
				
				$preventivo = Db::getInstance()->getRow("SELECT * FROM form_prevendita_thread ft WHERE ft.id_thread = ".$_GET['id_thread']."");
				$incaricato = new Employee($preventivo['id_employee']);
				$dataultimo = Db::getInstance()->getValue("SELECT date_add FROM form_prevendita_message WHERE id_thread = ".$_GET['id_thread']." ORDER BY date_add DESC");
				
				$oggetto = Db::getInstance()->getValue("SELECT subject FROM form_prevendita_thread WHERE id_thread = ".$_GET['id_thread']."");
					
				if($oggetto == '') {
					$oggetto = Db::getInstance()->getValue("SELECT message FROM form_prevendita_message WHERE id_thread = ".$_GET['id_thread']." ORDER BY date_add ASC");
				
				} 
				else {
					
				}
				
						
						
				switch($preventivo['status']) {
					case 'open': $status_preventivo = "<img src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; $st_prev = 'Aperto'; break;
					case 'closed': $status_preventivo = "<img src='../img/admin/status_green.gif' alt='Chiuso' title='Chiuso' />";  $st_prev = 'Chiuso'; break;
					case 'pending1': $status_preventivo = "<img src='../img/admin/status_orange.gif' alt='In lavorazione' title='In lavorazione' />"; $st_prev = 'In lavorazione';  break;
					default: $status_preventivo = "<img src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />";  $st_prev = 'Aperto';  break;
				}
				
				$annoprev = substr($preventivo['date_add'],2,2);
				if($preventivo['tipo_richiesta'] == 'preventivo') { $sigla = 'P'; } else if($preventivo['tipo_richiesta'] == 'tirichiamiamonoi') { $sigla = 'R'; }
				$idprevunivoco = $sigla.$annoprev.$preventivo['id_thread'];
				
			
				echo '
				<br /><h2 style="display:block;float:left;margin-right:10px" >'.($tipo_richiesta_p == 'tirichiamiamonoi' ? 'Ti richiamiamo noi n.' : 'Preventivo n.').' '.$idprevunivoco.'  - In carico a: '.$incaricato->firstname.' - Status: '.$st_prev.'</h2><div style="clear:both"></div>';
			}
				
			
			
				
				/*$ftp = Db::getInstance()->getValue('SELECT id_attivita FROM storico_attivita WHERE tipo_num = 1 AND id_attivita = '.Tools::getValue('id_thread'));
				if((!$ftp || $ftp <= 0) && $_GET['id_customer_thread'] > 5430)*/
			if(Tools::getIsset('viewpreventivo')) 
			{
				if(!Tools::getIsset('conf') && !Tools::getIsset('filename'))
					Customer::Storico(Tools::getValue('id_thread'), 'P', $cookie->id_employee, 14);
			}	
			
			if(isset($_GET['error']) && $_GET['error'] == '1pv') {
				
				echo '<div style="width:100%; height:30px; border:1px solid red; color:red"> Il messaggio era vuoto e non &egrave; stato inviato, ma sono stati aggiornati i campi status e in carico a.</div>';
			}
			
			if(isset($_POST['cambiastatusp_listing'])) {

				if(Tools::getValue('tipo_richiesta') == 7) {
					Db::getInstance()->ExecuteS("UPDATE customer_thread SET status = '".Tools::getValue('cambiastatusp_listing')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = '".Tools::getValue('id_thread')."'");
					
					switch(Tools::getValue('cambiastatusp_listing'))
					{
						case 'open': $switch_status = 3; break;
						case 'pending1': $switch_status = 4; break;
						case 'closed': $switch_status = 5; break;
						default: $switch_status = 10; break;
					}	
					
					Customer::Storico(Tools::getValue('id_thread'), 'P', $cookie->id_employee, $switch_status);
					
				}
				else {
					switch(Tools::getValue('cambiastatusp_listing'))
					{
						case 'open': $switch_status = 3; break;
						case 'pending1': $switch_status = 4; break;
						case 'closed': $switch_status = 5; break;
						default: $switch_status = 10; break;
					}	
					
					Customer::Storico(Tools::getValue('id_thread'), 'P', $cookie->id_employee, $switch_status);
					
					Db::getInstance()->ExecuteS("UPDATE form_prevendita_thread SET status = '".Tools::getValue('cambiastatusp_listing')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_thread = '".Tools::getValue('id_thread')."'");
				}
				Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&token='.$this->token.'&conf=4&tab-container-1=7');
				
			}
			
			if(isset($_POST['cambiaincaricop'])) {

				
				Customer::cambiaIncaricoVeloce(Tools::getValue('tipo'), Tools::getValue('cambiaincaricop'), Tools::getValue('id_thread'), Tools::getValue('precedente_incarico'), Tools::getValue('id_ticket_univoco'), Tools::getValue('id_customer'));
		
				
				Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&token='.$this->token.'&conf=4&tab-container-1=7');
				
			}
			
			if(isset($_GET['id_mex'])) {		
				$mex = Db::getInstance()->getRow("SELECT * FROM customer_thread ct WHERE ct.id_customer_thread = ".$_GET['id_mex']."");
					
				$id_incmex = Db::getInstance()->getValue("SELECT id_employee FROM customer_message WHERE id_customer_thread = ".$mex['id_customer_thread']."");
					
				$incmex = new Employee($id_incmex);
				
				$annomex = substr($mex['date_add'],2,2);
				$siglam = "M";
				$idmexunivoco = $siglam.$annomex.$mex['id_customer_thread'];
					
				switch($mex['status']) {
					case 'open': $status_mex = "<img src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; $st_mex = 'Aperto'; break;
					case 'closed': $status_mex = "<img src='../img/admin/status_green.gif' alt='Chiuso' title='Chiuso' />"; $st_mex = 'Chiuso'; break;
					case 'pending1': $status_mex = "<img src='../img/admin/status_orange.gif' alt='In lavorazione' title='In lavorazione' />"; $st_mex = 'In lavorazione'; break;
					default: $status_mex = "<img src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; $st_mex = 'Aperto'; break;
				}		

				$ctrl_mex = Db::getInstance()->getValue("SELECT COUNT(id_customer_message) FROM customer_thread ct JOIN customer_message cm ON ct.id_customer_thread = cm.id_customer_thread WHERE cm.id_employee = 0 AND ct.id_customer_thread = ".$mex['id_customer_thread']."");	
			
				echo '<h2 style="display:block;float:left;margin-right:10px" >Messaggio n. '.$idmexunivoco.' '.($ctrl_mex >= 1? ' - In carico a: '.$incmex->firstname.' - Status: '.$st_mex.'' : '').'</h2><div style="clear:both"></div>';
				
			}	
			
			
			if(isset($_GET['error']) && $_GET['error'] == '1ms') {
				
				echo '<div style="width:100%; height:30px; border:1px solid red; color:red">Il messaggio era vuoto e non &egrave; stato inviato, ma sono stati aggiornati i campi status e in carico a.</div>';
			}
			
			
			if(isset($_POST['submitMex'])) {

				$precedente_incarico = Db::getInstance()->getValue("SELECT id_employee FROM customer_thread WHERE id_customer_thread = ".Tools::getValue('id_mex')."");
				
				$precedente_status = Db::getInstance()->getValue("SELECT status FROM customer_thread WHERE id_customer_thread = ".Tools::getValue('id_mex')."");
				
				Db::getInstance()->ExecuteS("UPDATE customer_thread SET subject = '".addslashes(Tools::getValue('mex_subject'))."', status = '".addslashes(Tools::getValue('cambiastatusm'))."', note = '".addslashes(Tools::getValue('mex_note'))."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = '".Tools::getValue('id_mex')."'");
				
				switch(Tools::getValue('cambiastatusm'))
				{
					case 'open': $switch_status = 3; break;
					case 'pending1': $switch_status = 4; break;
					case 'closed': $switch_status = 5; break;
					default: $switch_status = 10; break;
				}	
				
				if($precedente_status != Tools::getValue('cambiastatusm'))
					Customer::Storico(Tools::getValue('id_mex'), 'T', $cookie->id_employee, $switch_status);
				
				$impiegato_attuale = Db::getInstance()->getValue("SELECT id_employee FROM customer_thread WHERE id_customer_thread = '".Tools::getValue('id_mex')."'");
				
				foreach ($_POST['note_nuova'] as $id_nota=>$nota_id) {
					
					echo $_POST['note_nota'][$id_nota].'<br />';
				
					if($_POST['note_nuova'][$id_nota] == 0) {
						
						$testo_nota = Db::getInstance()->getValue('SELECT note FROM note_attivita WHERE id_note = '.$id_nota.'');
						if($testo_nota != $_POST['note_nota'][$id_nota])
							Db::getInstance()->executeS("UPDATE note_attivita SET note = '".addslashes($_POST['note_nota'][$id_nota])."', date_upd = '".date("Y-m-d H:i:s")."', id_employee = '".$cookie->id_employee."' WHERE id_note = ".$id_nota.""); 
					}
					
					else {
						if(addslashes($_POST['note_nota'][$id_nota]) != '')
						{
							Db::getInstance()->executeS("INSERT INTO note_attivita
							(id_note,
							id_attivita,
							tipo_attivita,
							id_employee,
							note,
							date_add,
							date_upd)
							
							VALUES (
							NULL,
							'".Tools::getValue('id_mex')."',
							'T',
							'".$cookie->id_employee."',
							'".addslashes($_POST['note_nota'][$id_nota])."',
							'".date("Y-m-d H:i:s")."',
							'".date("Y-m-d H:i:s")."'
							)");
						
						}
					}
				}
				
				if($impiegato_attuale != Tools::getValue('assegnaimpiegatom')) {
				
					Customer::Storico(Tools::getValue('id_mex'), 'T', $cookie->id_employee, 2, Tools::getValue('assegnaimpiegatom'));
					
					Db::getInstance()->ExecuteS("UPDATE customer_thread SET id_employee = '".Tools::getValue('assegnaimpiegatom')."' WHERE id_customer_thread = '".Tools::getValue('id_mex')."'");
					
					$mailimp = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".Tools::getValue('assegnaimpiegatom')."'");
									
					$tokenimp = Tools::getAdminToken("AdminCustomers"."2".Tools::getValue('assegnaimpiegatom'));
									
					$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewmessage&id_mex=".Tools::getValue('id_mex')."&token=".$tokenimp.'&tab-container-1=7';

					$msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato il messaggio numero ".Tools::getValue('id_messaggio_univoco')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirlo</a>.";
									
					if($impiegato_attuale != 0) {
										
						$mailimprev = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".$impiegato_attuale."'");
						$nomeimprev = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = '".Tools::getValue('assegnaimpiegatom')."'");
										
						$msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione del messaggio numero ".Tools::getValue('id_messaggio_univoco')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect e l'ha passata a ".Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.Tools::getValue('assegnaimpiegatom').'');
										
						$params = array(
						'{link}' => $linkimp,
						'{firma}' => '',
						'{msg}' => $msgimprev);
						
						if($impiegato_attuale != $cookie->id_employee)
						{						
							Mail::Send(5, 'msg_base', Mail::l('Gestione messaggio revocata', 5), 
								$params, $mailimprev, NULL, NULL, NULL, NULL, NULL, 
							_PS_MAIL_DIR_, true);
						}			
					}
									
					else {
									
					}
									
					$params = array(
					'{link}' => $linkimp,
					'{firma}' => '',
					'{msg}' => $msgimp);
									
					if($_POST['assegnaimpiegatom'] != 0 && $_POST['assegnaimpiegatom'] != $cookie->id_employee) {
									
						Mail::Send(5, 'msg_base', Mail::l('Messaggio assegnato a te su Ezdirect', 5), 
							$params, $mailimp, NULL, NULL, NULL, NULL, NULL, 
							_PS_MAIL_DIR_, true);
													
					} else { }
				}

				else {
				}
				
				$id_contact = Db::getInstance()->getValue('SELECT id_contact FROM customer_thread WHERE id_customer_thread = '.Tools::getValue('id_mex'));
				
				if($id_contact != Tools::getValue('id_contact')) 
					Customer::convertiTicket(Tools::getValue('id_mex'), $id_contact, Tools::getValue('id_contact'));
				
				Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&viewmessage&id_mex='.Tools::getValue('id_mex').'&token='.$this->token.'&conf=4&tab-container-1=7');
			}
			
			//INVIO E VISUALIZZAZIONE MESSAGGIO
			if(isset($_GET['viewmessage'])) {
				
				/*$ftt = Db::getInstance()->getValue('SELECT id_attivita FROM storico_attivita WHERE tipo_num = 1 AND id_attivita = '.Tools::getValue('id_mex'));
				if((!$ftt || $ftt <= 0) && $_GET['id_mex'] > 18794)*/
				if(!Tools::getIsset('conf') && !Tools::getIsset('filename'))
					Customer::Storico(Tools::getValue('id_mex'), 'T', $cookie->id_employee, 14);
			
				if(!isset($_GET['aprinuovomessaggio'])) {
			
					if (isset($_GET['filename'])) {
						$filename = $_GET['filename'];
						
						if(strpos($filename, ":::")) {
			
							$parti = explode(":::", $filename);
							$nomecodificato = $parti[0];
							$nomevero = $parti[1];
							
						}
			
						else {
							$nomecodificato = $filename;
							$nomevero = $filename;
			
						}
						
						if (file_exists(_PS_UPLOAD_DIR_.$nomecodificato)) {
							self::openUploadedFile();
						}
						else { }
					} 
					else { }
			
					if(isset($_GET['id_mex'])) {	

						if($oggetto == '') {
							$oggetto = Db::getInstance()->getValue("SELECT message FROM customer_message WHERE id_customer_thread = ".$_GET['id_mex']." ORDER BY date_add ASC");
							
						} 
						else {
								
						}
						echo '<div id="tab-container-mex" class="tab-containers">
						
						
						
						<ul id="tab-container-mex-nav" class="tab-containers-nav">
						<li><a href="#mex-1"><strong>Generale</strong></a></li>
						<li><a href="#mex-2"><strong>Cronologia</strong></a></li>
						<li><a href="#mex-3"><strong>Gerarchia</strong></a></li>
						<li><a href="#mex-4"><strong>Storico</strong></a></li>
						</ul>';
				
						echo '
						
						<div class="yetii-mex" id="mex-1">
						<fieldset style="width:95%">';
						
						
						echo '
						<form id="modificamex" action="'.$currentIndex.'&id_customer='.$_GET['id_customer'].'&submitMex&viewcustomer&token='.$this->token.'&tab-container-1=7'.(Tools::getIsset('riferimento') ? '&riferimentoo='.Tools::getValue('riferimento') : '').'" method="post" autocomplete="off">';
						
						
						
						echo '<div class="anagrafica-cliente" style="width:800px; margin-bottom:10px">
						<strong>Oggetto</strong><br />
							
							<input type="hidden" value="'.Tools::getValue('id_mex').'" name="id_mex" />
							<textarea name="mex_subject" id="mex_subjectContent" style="width:690px;height:40px;display:block;float:left">'.strip_tags(htmlspecialchars_decode($oggetto)).'</textarea>
							<br />
				
						<br /><br />
						</div>

						<div class="clear">
						<div class="anagrafica-cliente" style="width:103px">
						ID Messaggio<br />
						<span class="tab-span" style="width:103px">'.$idmexunivoco.'</span><br />
						</div>';
						
						$aperto_da = Db::getInstance()->getValue('SELECT id_employee FROM customer_message WHERE id_customer_thread = '.Tools::getValue('id_mex').' ORDER BY id_customer_message ASC');
					
						if($aperto_da != 0)
							$aperto_da = Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$aperto_da);
						else
							$aperto_da = 'Cliente';
						
						echo '
					
					<div class="anagrafica-cliente" style="width:97px">
					Aperto da<br />
					<span class="tab-span" style="width:97px">'.$aperto_da.'</span><br />
					</div>
					
						<div class="anagrafica-cliente" style="width:250px">
						<input type="hidden" name="id_messaggio_univoco" value="'.$idmexunivoco.'" />
						In carico a<br />';
							echo '
						
						<select style="width:250px" name="assegnaimpiegatom">';
						$impiegati_ez = Db::getInstance()->ExecuteS("SELECT id_employee, firstname, lastname FROM employee WHERE active = 1 ORDER BY firstname ASC, lastname ASC");
				
						echo '<option value="0"'.($mex['id_employee'] == 0 ? 'selected="selected"' : '').' >Da assegnare</option>';
						
						foreach ($impiegati_ez as $impez) {
						
							echo "<option value='".$impez['id_employee']."' ".($mex['id_employee'] == $impez['id_employee'] ? 'selected="selected"' : '')." >".$impez['firstname']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>";
						
						}

						echo '</select>
				
						</div>
						
						
						
						<div class="anagrafica-cliente">
						Data apertura<br />
						<span class="tab-span">'.Tools::displayDate($mex['date_add'], (int)($cookie->id_lang), true).'</span><br />
						</div>
						
							
							<div class="clear"></div>';
							
						switch($mex['status']) 
						{
							case 'open': $status_mex = "<img style='display:block; margin-top:5px; float:left' src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; 
							break;
							case 'closed': $status_mex = "<img style='display:block; margin-top:5px; float:left' src='../img/admin/status_green.gif' alt='Chiuso' title='Chiuso' />"; 
							break;
							case 'pending1': $status_mex = "<img style='display:block; margin-top:5px; float:left' src='../img/admin/status_orange.gif' alt='In lavorazione' title='In lavorazione' />"; 
							break;
							default: $status_mex = "<img style='display:block; margin-top:5px; float:left' src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; break;
						}
							
						echo '<div class="anagrafica-cliente">';
						$dataultimo = Db::getInstance()->getValue("SELECT date_add FROM customer_message WHERE id_customer_thread = ".$_GET['id_mex']." ORDER BY date_add DESC");
						if($ctrl_mex >= 1) {
						echo 'Status<br />
						'.$status_mex.' <input type="hidden" name="id_customer" value="'.Tools::getValue('id_customer').'" />
						
						
						<select name="cambiastatusm">
						<option value="closed" '.($mex['status'] == 'closed' ? 'selected="selected"' : '').'>Chiuso</option>
						<option value="pending1" '.($mex['status'] == 'pending1' ? 'selected="selected"' : '').'>In lavorazione</option>
						<option value="open" '.($mex['status'] == 'open' ? 'selected="selected"' : '').'>Aperto</option>
						</select><br />'; } else { echo "<br />"; }
						echo '</div>
						
						<div class="anagrafica-cliente" style="width:250px">
						Tipo attivit&agrave;<br />
						<select style="width:250px" name="id_contact" id="id_contact">
												<option value="4">Assistenza tecnica</option>
												<option value="2">Assistenza amministrativa - contabilit&agrave;</option>
												<option value="8">Assistenza amministrativa - ordini</option>
												<option value="3">Rivenditori</option>
												<option value="preventivo">Richiesta di preventivo</option>
												<option value="tirichiamiamonoi">Ti richiamiamo noi</option>
												<option value="7" selected="selected">Messaggio</option>
												</select>
						
						</div>
						
						<div class="anagrafica-cliente">
						Data ultima com.<br />
						<span class="tab-span">'.Tools::displayDate($dataultimo, (int)($cookie->id_lang), true).'</span><br />
						</div>
						
						<div class="clear"></div>';
						
						if($mex['riferimento'] != '')
						{
							echo '
							<div class="clear"></div>
							<div class="anagrafica-cliente" style="width:720px">
							Azione padre<br />
							<span class="tab-span" style="width:720px">'.Customer::trovaSiglaLinkPerAlbero(substr($mex['riferimento'],1), substr($mex['riferimento'],0,1), '','y:T:'.$mex['id_customer_thread'].':'.$mex['status']).'</span><br />
							</div><div class="clear"></div>
							';
							
						}

						$messaggio_note = Db::getInstance()->executeS('SELECT * FROM note_attivita WHERE tipo_attivita = "T" AND id_attivita = '.$mex['id_customer_thread'].'');
					
					echo '<strong>Note interne visibili solo agli impiegati</strong><br />';
					
					echo '<table><thead>'.(count($messaggio_note) > 0 ? '<tr><th>Nota</th><th>Creata da</th><th>Ultima modifica</th><th></th></tr>' : '' ).'</thead><tbody id="tableNoteBody">
					';
					
					foreach($messaggio_note as $messaggio_nota)
					{
						echo '<tr id="tr_note_'.$messaggio_nota['id_note'].'">';
						echo '
						<td>
						<textarea class="textarea_note" name="note_nota['.$messaggio_nota['id_note'].']" id="note_nota['.$messaggio_nota['id_note'].']" style="width:540px; height:16px" onkeyup="auto_grow(this)">'.htmlentities($messaggio_nota['note'], ENT_COMPAT, 'UTF-8').'</textarea><input type="hidden" name="note_nuova['.$messaggio_nota['id_note'].']" id="note_nuova['.$messaggio_nota['id_note'].']" value="0" /></td>
						<td><input type="text" readonly="readonly" name="note_id_employee['.$messaggio_nota['id_note'].']" value="'.Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM employee WHERE id_employee = '.$messaggio_nota['id_employee']).'" /></td>
						<td><input type="text" readonly="readonly" name="note_date_add['.$messaggio_nota['id_note'].']" value="'.Tools::displayDate($messaggio_nota['date_upd'], 5).'" /></td>';
						echo '<td><a href="javascript:void(0)" onclick="javascript:var surec=window.confirm(\'Sei sicuro?\'); if (surec) { togli_nota('.$messaggio_nota['id_note'].'); cancella_nota_attivita('.$messaggio_nota['id_note'].'); } else { return false; }; "><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" style="cursor:pointer" /></a></td>';
						echo'
						</tr>
						';
					}
					
					echo '</tbody></table><br />';
					
					echo '
					<script type="text/javascript">add_nota_privata();</script>
					<a href="javascript:void(0)" onclick="javascript:add_nota_privata()";><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" /> Clicca per aggiungere una nota privata</a> <br /><br />';
					
						if($mex['date_add'] < '2017-12-08 00:00:00')
						{
							echo '<br />
						
									
										<textarea name="mex_note" id="mexnoteContent" style="width:80%;height:100px">'.Tools::htmlentitiesUTF8($mex['note']).'</textarea><br /><br />';
						}
								
								echo '<button type="submit" class="button" name="submitMex">
								<img src="../img/admin/save.gif" alt="Salva" title="Salva" />&nbsp;&nbsp;&nbsp;'.$this->l('   Save   ').'
								</button>
								<button class="button" onclick="javascript:var surec=window.confirm(\'Sei sicuro?\'); if (surec) { $(\'#coll_destinazione\').val($(\'#collega_msg\').val()); document.forms[\'collega_attivita\'].submit(); return false; } else { return false; }; " style="margin-left:10px; margin-right:0px">
						
						<img src="../img/admin/link.gif" alt="Collega" title="Collega" />&nbsp;&nbsp;&nbsp;'.$this->l('Collega a:').'
						</button>
						<select name="collega_msg" id="collega_msg" style="margin-left:-4px; height:27px;width:150px" >
							<option value="">-- Seleziona --</option>
							'.Customer::BuildSelectForLinking($customer->id, $mex['id_customer_thread'], 'T').'
						</select>
						';
							
						
						echo '</form>
					
						<form style="margin-left:5px; display:block;float:left" name="collega_attivita" id="collega_attivita" action="'.htmlentities($_SERVER['REQUEST_URI']).'&conf=4" method="post" />
						<input type="hidden" name="submitCollegaAttivita" value="" />
						<input type="hidden" name="coll_destinazione" id="coll_destinazione" value="" />
						<input type="hidden" name="coll_partenza" id="coll_partenza" value="T'.$mex['id_customer_thread'].'" />
						</form>';
								
								
						echo '</form>
						
						
						</fieldset><br /><br /></div>';
				
			
					}
		
				echo '<div class="yetii-mex" id="mex-2">';
					
		
					$messaggixt = Db::getInstance()->ExecuteS("SELECT * FROM customer_message cm WHERE cm.id_customer_thread = ".$_GET['id_mex']."");
					
					foreach($messaggixt as $messaggioxt) {
					
						echo "<table style='width:100%; 
						border:1px solid #dfd5c3; 
						";
						if ($messaggioxt['id_employee'] == 0) {
							echo 'background-color: #ffffff;';
						} else {
				
							echo 'background-color: #ffecf2;';
						}
						$employeezxt = new Employee($messaggioxt['id_employee']);
						echo "margin-bottom:15px'>";
						echo '<tr>
						<td style="colspan:2">
							<table>
					
							<td style="font-size:14px; width:250px">Da: <strong>';if ($messaggioxt['id_employee'] == 0) { echo $customer->firstname." ".$customer->lastname; } else { echo $employeezxt->firstname." ".substr($employeezxt->lastname,0,1)."."; } echo '<strong></td>
							
							<td style="width:170px"><strong>Data: </strong>'.date("d-m-Y H:i:s",strtotime($messaggioxt['date_add'])).'</td>
							<td style="width:180px"><strong>Allegato: </strong>';
							if(!empty($messaggioxt['file_name'])) {
							
								$allegatim = explode(";",$messaggioxt['file_name']);
								$nallm = 1;
								foreach($allegatim as $allegatom) {
									if(strpos($allegatom, ":::")) {
						
										$parti = explode(":::", $allegatom);
										$nomeverom = $parti[1];
										
									}
						
									else {
										$nomeverom = $allegatom;
						
									}
									if($allegatom == "") { } else {
										if($nallm == 1) { } else { echo " - "; }
										echo '<a href="'.( substr($allegatom,0,3) == 'BDL' ? 'ajax.php?openAllegato=y&id_customer='.$customer->id.'&viewcustomer&id_bdl='.str_replace('BDL','',$parti[0]).'&getPDFBDL=ezdirect&token='.$this->token.'&tab-container-1=14' : 'ajax.php?openAllegato=y&id_customer='.$customer->id.'&viewcustomer&viewmessage&id_mex='.$mex['id_customer_thread'].'&token='.$this->token.'&tab-container-1=7&filename='.$allegatom).'"><span style="color:red">'.$nomeverom.'</span></a>';
										$nallm++;
									}
								}
							
							}
							
							else { echo 'Nessun allegato'; }
							
							
							echo '</td>';
							
							
							
							echo '<td></td></tr></table>
						</td>
						</tr>
						<tr>
						<td style="colspan:2">
							<table>
							<tr>
							<td style="width:100px"><strong>Messaggio</strong></td>
							<td>'.htmlspecialchars_decode($messaggioxt['message']).($messaggioxt['id_employee'] == 0 ? '' : '<form method="post">
							<input type="hidden" name="modifica_messaggio_messaggio" value="'.htmlentities($messaggioxt['message'], ENT_QUOTES | ENT_SUBSTITUTE, "UTF-8").'" />
							<button type="submit" class="button" name="modifica_messaggio_submit" value="Modifica">
							<img src="../img/admin/edit.gif" alt="Modifica" title="Modifica" />&nbsp;&nbsp;&nbsp;Modifica
							</button>
							</form>').'</td>
							</tr>
							</table>
						</td>';
						echo '</tr>';
						
						if($messaggioxt['note_private'] != '')
						{
							echo '<tr>
							<td style="colspan:2">
								<table>
								<tr>
								<td style="width:100px"><strong style="color:red">Nota privata</strong></td>
								<td style="color:red">'.htmlspecialchars_decode(preg_replace('/(<[^>]+) style=".*?"/i', '$1', preg_replace('/(<[^>]+) class=".*?"/i', '$1', $messaggioxt['note_private']))).'
								</td>
								</tr>
								</table>
							</td>';
							echo '</tr>
							';
						}
							if($messaggioxt['email'] == '') { 
							
								echo '<tr><td style="colspan:2">
								<table>
								<tr>
								
								<td style="width:100px"><strong>In carico a</strong></td>
								<td>'.Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$messaggioxt['in_carico']).'</td>
								';
							
							
							
							} else {
						
						echo '<tr><td style="colspan:2">
							<table>
							<tr>
							<td><strong>Inviato a: </strong>'.(is_numeric($messaggioxt['email']) ? Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$messaggioxt['email']) : htmlspecialchars_decode($messaggioxt['email'])).'</td>
							
							'.($messaggioxt['in_carico'] > 0 ? '<td style="padding-left:20px"><strong>In carico a</strong>:
							'.($messaggioxt['in_carico'] > 0 ? Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$messaggioxt['in_carico'])  : '--').'</td>' : '').'
							';
						
						}
						
						$cc = Db::getInstance()->getValue("SELECT cc FROM ticket_cc WHERE type = 't' AND msg = ".$messaggioxt['id_customer_message']."");
							if(!empty($cc)) {
							
								echo '<td style="padding-left:20px">';
							
								$ccs = explode(";",$cc);
								$cc_str = "<strong>CC:</strong> ";
								
								foreach ($ccs as $conoscenza) {
								
									$imp = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".$conoscenza."");
									$cc_str .= $imp." ";
								}
								
								echo $cc_str;
								echo "</td>";
							
							}
							else {
							
								echo '<td style="padding-left:20px"><strong>CC:</strong> Nessuno</td>';
							
							}
						
						echo '</tr>
							</table>
						</td></tr></table>';
					}
				
				}
				
				else {
				
				}
				
				if(isset($_GET['aprinuovomessaggio'])) {
				
					echo '<h2>Invia messaggio al cliente</h2>';
				
				
				}
				
				
			
				else {
				
					echo '<h2 id="rispondi-cliente-messaggio">Rispondi al cliente</h2>';
					if(isset($_POST['modifica_messaggio_submit'])) {
					echo '<script type="text/javascript">
						 window.location = "#rispondi-cliente-messaggio";
					</script>';
					}
				}
			
						$iso = Language::getIsoById((int)($cookie->id_lang));
				$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
				$ad = dirname($_SERVER["PHP_SELF"]);
					echo '
				<script type="text/javascript">
				var iso = \''.$isoTinyMCE.'\' ;
				var pathCSS = \''._THEME_CSS_DIR_.'\' ;
				var ad = \''.$ad.'\' ;
				</script>
				
				
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce3/tiny_mce.js"></script>
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>
				<script type="text/javascript">
					$(document).ready(function () {
					
	dataChanged = 0;     // global variable flags unsaved changes      

	function bindForChange(){    
		 $(\'input,checkbox,text,textarea,radio,select\').bind(\'change\',function(event) { dataChanged = 1});
		 $(\'#messaggio\').bind(\'input propertychange\', function() { dataChanged = 1 });
		 $(\':reset,:submit\').bind(\'click\',function(event) { dataChanged = 0 });
	}


	function askConfirm(){  
		if (dataChanged){ 
			return "You have some unsaved changes.  Press OK to continue without saving." 
		}
	}

	window.onbeforeunload = askConfirm;
	window.onload = bindForChange;
	});
				</script>
				<!-- <script type="text/javascript">
						toggleVirtualProduct(getE(\'is_virtual_good\'));
						unitPriceWithTax(\'unit\');
				</script> -->';
				
						echo'	<form action="'.$currentIndex.'&id_customer='.$obj->id.'&viewcustomer&token='.$this->token.'&tab-container-1=7" method="post" class="std" enctype="multipart/form-data">
						<fieldset style="width:80%">
						<input type="hidden" name="isMail" value="1">
							<table>';
							
							$customer_phone = Db::getInstance()->getRow("SELECT	phone, phone_mobile FROM address WHERE id_customer = ".$customer->id." AND deleted = 0 AND fatturazione = 1 AND active = 1");
								
								if($customer_phone['phone'] == '') { $telefono_cliente = $customer_phone['phone_mobile']; } else { $telefono_cliente = $customer_phone['phone']; }
							
							if(isset($_GET['aprinuovomessaggio'])) {
				
								echo '<input type="hidden" name="nuovomessaggio" value="1" />
								<input type="hidden" name="riferimento" id="riferimento" value="'.Tools::getValue('riferimento').'" /><input type="hidden" name="riferimentoo" id="riferimentoo" value="'.Tools::getValue('riferimento').'" />

								';
					
				
							}
			
							else {
								echo '<input type="hidden" value="1" name="isAnswer" />';
							}	
							echo '	<tr>
									<td style="width:170px">
									
									<input type="hidden" name="id_customer" value="'.$customer->id.'" />
									</td>
								</tr>
								
								<tr><td>Email</td><td>';
								
								
								if($customer->is_company == 1) { echo '<div style="float:left">';
								
								$persone = Db::getInstance()->executeS("SELECT id_persona, firstname, lastname, email FROM persone WHERE id_customer = ".Tools::getValue('id_customer')." AND email != ''");
								
								echo '
								<select name="seleziona-persona" onchange="javascript:document.getElementById(\'customer_email_msg_m\').value = (this.value);">';
								echo "<option value=''>-- Seleziona persona --</option>";
								
								foreach ($persone as $persona) {
								
									echo "<option ".($persona['id_persona'] == $_GET['id_persona'] ? "selected='selected'" : "")."  value='".$persona['email']."'>".$persona['firstname']." ".$persona['lastname']."</option>";
								
								}
								
								echo "</select></div>";
								
								} else { }
								
								
								if(isset($_GET['id_persona'])) {
									$mail_persona = Db::getInstance()->getValue("SELECT email FROM persone WHERE id_persona = ".$_GET['id_persona']);
								}
								else {
									$mail_persona = $customer->email;
								}
								
								
								echo '<div style="float:left; '.($customer->is_company == 1? 'margin-left:20px' : '').'"><input type="text" size="40" name="customer_email_msg" id="customer_email_msg_m" value="'.(!$mex['email'] ? $mail_persona : $mex['email']).'" /><br />
								<span style="font-size:11px">In caso di più indirizzi mail, separare con punto e virgola (;)</span></div><div style="float:left; margin-left:20px"> Tel. cliente: <span class="tab-span" style="width:200px; padding-left:16px; padding-right:16px; display:inline"><a href="callto:'.$telefono_cliente.'">'.$telefono_cliente.'</a></span></div></td></tr>
						
							
								<input type="hidden" value="'.$mex['id_customer_thread'].'" name="id_customer_thread">';
								
							if(isset($_GET['aprinuovomessaggio'])) {
							
							}
							
							else {
								
							echo ' 
								<tr><td>Stato messaggio</td>
								<td>
								<div style="float:left">
								<script type="text/javascript">
									function changeInCaricoAM()
									{';
									
									
									foreach ($impiegati_ez as $impez) {
										echo '
										
										firstVar = document.getElementById("in_carico_a_m").value;
										
										if (document.getElementById("in_carico_a_m").value == "'.$impez['id_employee'].'") {
										
										
										document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').disabled = true;
										document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').checked = false;
										
										} else { document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').disabled = false; }
									';
									}
								echo '
									}
									</script>
								<select name="statusm" id="statusm">
								<option value="open" '.($mex['status'] == 'open' ? 'selected="selected"' : '').'>Aperto   </option>
								<option value="pending1" '.($mex['status'] == 'pending1' ? 'selected="selected"' : '').'>In lavorazione   </option>
								<option value="closed" '.($mex['status'] == 'closed' ? 'selected="selected"' : '').'>Chiuso   </option>
								
								</select>
								</div><div style="float:left; margin-left:20px">
								In carico a';
						echo '
					
					<select style="width:250px" id="in_carico_a_m" name="in_carico_a_m" onChange="changeInCaricoAM()">';
					foreach ($impiegati_ez as $impez) {
					
						if(isset($_GET['aprinuovomessaggio'])) {
							echo "<option value='".$impez['id_employee']."' ".($cookie->id_employee == $impez['id_employee'] ? 'selected="selected"' : '')." >".$impez['firstname']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>";
						}
						else {
							echo "<option value='".$impez['id_employee']."' ".($mex['id_employee'] == $impez['id_employee'] ? 'selected="selected"' : '')." >".$impez['firstname']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>";
						}
					}

					echo '</select><br /><br /></td>
								</tr>';
								
								}
					$impiegati_ez = Db::getInstance()->ExecuteS("SELECT id_employee, firstname, lastname FROM employee WHERE active = 1 ORDER BY firstname ASC, lastname ASC");			
						echo '<tr><td>Conoscenza</td>
								<td>
								';
								
								$impez_count = 0;
								foreach ($impiegati_ez as $impez) {
									$impez_count++;
									if($impez_count == 10)
										echo '<br />';
									echo "<input type='checkbox' id='conoscenza_".$impez['id_employee']."' name='conoscenza[]' value='".$impez['id_employee']."' /> ".$impez['firstname']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
								
								}
								echo '</td>
								<tr><td><br /></td></tr>
								<tr><td>Allega file</td>
								<td>
								<input name="MAX_FILE_SIZE" value="20000000" type="hidden">
								<script type="text/javascript" src="jquery.MultiFile.js"></script>
								<input name="joinFile[]" class="multi" id="joinFile" type="file" onchange="handleFileSelect(this.files)">
								
								<output id="list-tkt">Anteprime: </output>
								'.(Tools::getIsset('allega_bdl') ? '<input type="hidden" name="invio_con_bdl_pdf" value="'.Tools::getValue('allega_bdl').'" /><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&id_bdl='.Tools::getValue('allega_bdl').'&getPDFBDL=ezdirect&token='.$this->token.'&tab-container-1=14">'.Tools::getValue('allega_bdl').'-buono-di-lavoro.pdf</a>' : '').'
									<script type="text/javascript">
								function handleFileSelect(files) {
								
									// Loop through the FileList and render image files as thumbnails.
									 for (var i = 0; i < files.length; i++) {
										var f = files[i];
										var name = files[i].name;
			  
										var reader = new FileReader();  
										reader.onload = function(e) {  
										  // Render thumbnail.
										  var span = document.createElement("span");
										  span.innerHTML = ["<a href=\'",e.target.result,"\' target=\'_blank\'>",name,"</a> - "].join("");
										  document.getElementById("list-tkt").insertBefore(span, null);
										};

									  // Read in the image file as a data URL.
									  reader.readAsDataURL(f);
									}
								  }
								 
								</script>
								
								</td>
								</tr>
								
								<tr><td>Link prodotto</td><td><link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
								<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete3.js"></script>
								<div id="ajax_choose_product" style="padding:6px; padding-top:2px; width:800px;">
									
									<input type="checkbox" checked="checked" id="prodotti_online" onclick="if(document.getElementById(\'product_autocomplete_input_messaggio\').value != \'\') { clearAutoComplete(); }" />Online &nbsp;&nbsp;
				<input type="checkbox" id="prodotti_offline" onclick="if(document.getElementById(\'product_autocomplete_input_messaggio\').value != \'\') { clearAutoComplete(); }" />Offline &nbsp;
	<input type="checkbox" id="prodotti_old" onclick="if(document.getElementById(\'product_autocomplete_input_messaggio\').value != \'\') { clearAutoComplete(); }" />Old &nbsp;&nbsp;
				<input type="checkbox" id="prodotti_disponibili" onclick="if(document.getElementById(\'product_autocomplete_input_messaggio\').value != \'\') { clearAutoComplete(); }" />Solo disp. &nbsp;&nbsp;&nbsp;
				
				<script type="text/javascript" src="../js/select2.js"></script>
						<script type="text/javascript">
						$(document).ready(function() { $("#auto_marca").select2(); $("#auto_serie").select2(); $("#auto_fornitore").select2();  $("#auto_categoria").select2(); });
						</script>
						
				<select id="auto_marca" name="auto_marca" onchange="repopulateSeries(this.value); clearAutoComplete();" style="width:100px">
				<option value="0">Marca...</option>
				';
				$marche = Db::getInstance()->executeS('SELECT * FROM manufacturer ORDER BY name ASC');
				foreach($marche as $marca)
					echo '<option value="'.$marca['id_manufacturer'].'">'.$marca['name'].'</option>';
				echo '
				</select>
				
						
				<select id="auto_serie" name="auto_serie" onchange="clearAutoComplete();" style="width:100px">
				<option value="0">Serie...</option>
				<option value="0">Scegli prima un costruttore</option>
				';
				echo '
				</select>
				
				<select id="auto_categoria" name="auto_categoria" onchange="clearAutoComplete();" style="width:100px">
				<option value="0">Categoria...</option>
				';
				$categorie = Db::getInstance()->executeS('SELECT * FROM category c JOIN category_lang cl ON c.id_category = cl.id_category WHERE cl.id_lang = 5 AND c.id_parent = 1 ORDER BY cl.name ASC');
				foreach($categorie as $categoria)
					echo '<option value="'.$categoria['id_category'].'">'.$categoria['name'].'</option>';
				echo '
				</select>
				
				<select id="auto_fornitore" name="auto_fornitore" onchange="clearAutoComplete(); " style="width:100px">
				<option value="0">Fornitore...</option>
				';
				
				$fornitori = Db::getInstance()->executeS('SELECT * FROM supplier WHERE id_supplier = 8 OR id_supplier = 11 OR id_supplier = 42 ORDER BY name ASC');
				foreach($fornitori as $fornitore)
					echo '<option value="'.$fornitore['id_supplier'].'">'.$fornitore['name'].'</option>';
				echo '
				</select>
				
				<input id="reset" type="button" value="Reset" class="button" onclick="document.getElementById(\'prodotti_online\').checked = true; document.getElementById(\'prodotti_offline\').checked = true; document.getElementById(\'prodotti_old\').checked = true; document.getElementById(\'prodotti_disponibili\').checked = false; document.getElementById(\'auto_categoria\').options[0].selected = true; $(\'#auto_categoria\').select2();  document.getElementById(\'auto_marca\').options[0].selected = true; $(\'#auto_marca\').select2();  
				document.getElementById(\'auto_serie\').options[0].selected = true; $(\'#auto_serie\').select2();  
				document.getElementById(\'auto_fornitore\').options[0].selected = true; $(\'#auto_fornitore\').select2(); $(\'#prodlist\').hide(); $(\'div.autocomplete_list\').hide();" />
				
				<br />
				 <input size="123" type="text" value="" id="product_autocomplete_input_messaggio" /><input id="veditutti2" type="button" value="Cerca" class="button" onclick=\'$("#veditutti").trigger("click");\' /><input id="veditutti" type="button" value="Cerca" class="button" style="display:none" /><script type="text/javascript">
						var formProduct = "";
						var products = new Array();
					</script>
					<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
					<script type="text/javascript">
								
									
									urlToCall = null;
									/* function autocomplete */
									
									function getOnline()
						{
							if(document.getElementById("prodotti_online").checked == true)
								return 1;
							else
								return 0;
						}
						
						function getOffline()
						{
							if(document.getElementById("prodotti_offline").checked == true)
								return 1;
							else
								return 0;
						}

	function getOld()
						{
							if(document.getElementById("prodotti_old").checked == true)
								return 1;
							else
								return 0;
						}
						
						function getDisponibili()
						{
							if(document.getElementById("prodotti_disponibili").checked == true)
								return 1;
							else
								return 0;
						}
						
						function getCopiaDa()
						{
								return 0;
						}
						
						function getSerie()
						{
							if(document.getElementById("auto_serie").value > 0)
								return document.getElementById("auto_serie").value;
							else
								return 0;
						}
						
						function getMarca()
						{
							if(document.getElementById("auto_marca").value > 0)
								return document.getElementById("auto_marca").value;
							else
								return 0;
						}
						
						function getFornitore()
						{
							if(document.getElementById("auto_fornitore").value > 0)
								return document.getElementById("auto_fornitore").value;
							else
								return 0;
						}
						
						function getCategoria()
						{
							if(document.getElementById("auto_categoria").value > 0)
								return document.getElementById("auto_categoria").value;
							else
								return 0;
						}
						
						function repopulateSeries(id_manufacturer)
						{
							$("#auto_serie").empty();
							$.ajax({
							  url:"ajax.php?repopulate_series=y",
							  type: "POST",
							  data: { id_manufacturer: id_manufacturer
							  },
							  success:function(resp){  
								var newOptions = $.parseJSON(resp);
								
								 $("#auto_serie").append($("<option></option>")
									 .attr("value", "0").text("Serie..."));
								
								$.each(newOptions, function(key,value) {
								  $("#auto_serie").append($("<option></option>")
									 .attr("value", value).text(key));
								});
								
								$("#auto_serie").select2({
									placeholder: "Serie..."
								});
							  },
							  error: function(xhr,stato,errori){
								 alert("Errore: impossibile trovare serie ");
							  }
							});
								
							
						}	
									
								function clearAutoComplete()
						{
							$("#veditutti").trigger("click");
							
						}		</script>
					
					<script type="text/javascript">
					
						function addProduct_TR(event, data, formatted)
						{
							document.getElementById("product_autocomplete_input_messaggio").value = ""; 
							var link_rewrite = data[31];	
							var productName = data[5];
							var productRef = data[36];
							
							tinyMCE.get(\'messaggio\').focus();
							var $body = $(tinymce.activeEditor.getBody());
							$body.prepend(\'<p>\');
							$body.append(\'</p>\');
							$body.find("p:last").append(productName + \' (\'+ productRef +\') <br /><a href="\' + link_rewrite + \'">\' + link_rewrite + \'<\/a><br /><br />\');
							$("body").trigger("click");
							$("#product_autocomplete_input_messaggio").trigger("click");
							$("#product_autocomplete_input_messaggio").trigger("click");
							
						}	
						
						
	 
						urlToCall = null;
						
						function getExcludeIds() {
							var ids = "";
							ids += $(\'#excludeIds\').val().replace(/\\-/g,\',\').replace(/\\,$/,\'\');
							return ids;
						}
						
						$(function() {
							$(\'#product_autocomplete_input_messaggio\')
								.autocomplete(\'ajax_products_list2.php?products_and_bundles=y&id_customer='.Tools::getValue('id_customer').'\', {
									minChars: 0,
									autoFill: false,
									max:500,
									matchContains: true,
									matchSubset: 0,
									mustMatch:true,
									scroll:true,
									cacheLength:1,
									formatItem: function(item) {
								
												var color = "";
												if(item[24] == 1) {
													var color = "; color:red";
												}
												if(item[28] == 0) {
													return \'</table><div style="position:absolute; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:150px;float:left;margin-left:10px;">Codice</div></th><th style="width:570px;"><div style="width:350px; float:left;">Desc. prodotto<br /><br /></div></th><th style="width:60px; text-align:right"><div style="width:60px;float:left;">Prezzo</div></th><th style="width:50px; text-align:right"><div style="width:60px;float:left;">Qt. mag.</div></th><th style="width:50px; text-align:right"><div style="width:75px;float:left;">Qt. tot</div></th></tr></div><table><tr><td style="width:145px; text-align:left"><br /><br /><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:555px\'+color+\'"><br /><br />\'+item[5]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[23]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[25]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[22]+\'</td></tr></table>\'
													
													;
												}
												else {
													return \'<tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:350px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
													
													;
													}
												}
									
								}).result(addProduct_TR);
							
						});
					
						
						$("#product_autocomplete_input_messaggio").focus();
					
						$("#product_autocomplete_input_messaggio").css(\'width\',\'673px\');
						$("#product_autocomplete_input_messaggio").keypress(function(event){
									
									  var keycode = (event.keyCode ? event.keyCode : event.which);
									  if (keycode == "13") {
										  $("#veditutti").trigger("click");
										event.preventDefault();
										event.stopPropagation();    
									  }
									});
						
						
								</script>
								
								<script type="text/javascript">
					
					$("#veditutti2").click ( function (zEvent) {
						
						/*	$("body").trigger("click");
							$("body").trigger("click");
							
							
							
							$("#product_autocomplete_input_messaggio").focus();
						//	$("#product_autocomplete_input_messaggio").val("");
						//	$("#product_autocomplete_input_messaggio").val(" ");
						*/
							var press = jQuery.Event("keypress");
							press.which = 13;
							if(document.getElementById(\'product_autocomplete_input_messaggio\').value == "")
							{
								$("#product_autocomplete_input_messaggio").val(" ");
							}
							$("#product_autocomplete_input_messaggio").trigger(press);
						
							$("#product_autocomplete_input_messaggio").trigger("click");
							$("#product_autocomplete_input_messaggio").trigger("click");
						//	$("#product_autocomplete_input_messaggio").val("");
							
					} );


					</script>
									
						</td></tr>';
					
					
					
					echo '
					<tr><td><script type="text/javascript" src="../js/select2.js"></script>
						<script type="text/javascript">
						$(document).ready(function() { $("#utils_autocomplete_input_messaggio").select2(); $("#messaggi_precompilati").select2(); });
					
					function addUtil_TR(event, data, formatted)
						{
							tinyMCE.get(\'messaggio\').focus();
							var $body = $(tinymce.activeEditor.getBody());
							$body.prepend(\'<p>\');
							$body.append(\'</p>\');
							if(document.getElementById("utils_autocomplete_input_messaggio").value != "-- Seleziona un link utile --")
							{
								$body.find("p:last").append(\'<a href="\' + document.getElementById("utils_autocomplete_input_messaggio").value + \'">\' + document.getElementById("utils_autocomplete_input_messaggio").value + \'<\/a><br /><br />\');
							}
						}	</script>
					Link utili</td><td><select name="utils_autocomplete_input_messaggio" id="utils_autocomplete_input_messaggio" style="width:730px" onchange="addUtil_TR();">
					'.$utils_options.'</select></td></tr>';
					
					echo '<tr><td>Messaggi precompilati</td><td><select name="messaggi_precompilati" id="messaggi_precompilati" style="width:730px" onchange="inserisciPrecompilato(this.value);">
					';
					$precompilati = Db::getInstance()->executeS('SELECT * FROM precompilato WHERE active = 1');
					echo '<option value="-- Seleziona un messaggio --">-- Seleziona un messaggio --</option>';
					foreach($precompilati as $precompilato)
					{
						echo '<option value="'.htmlentities($precompilato['testo'], ENT_QUOTES).'">'.htmlentities($precompilato['oggetto'], ENT_QUOTES).'</option>';
					}
					echo '
					</select>
					<script type="text/javascript">
					function inserisciPrecompilato(msg)
					{
						tinyMCE.get(\'messaggio\').focus();
						var $body = $(tinymce.activeEditor.getBody());
						$body.html(\'\');
						$body.prepend(\'<p>\');
						$body.append(\'</p>\');
						if(msg != "-- Seleziona un messaggio --") {
							$body.find("p:last").append(msg);
						}
					}
					</script>
					</td></tr>
					';
					
					echo '
								<tr><td>Messaggio</td>
								<td>
								
								<textarea id="messaggio" class="rte" name="messaggio" rows="15" cols="20" style="width:760px;height:220px">'.(isset($_POST['modifica_messaggio_submit']) ? stripslashes($_POST['modifica_messaggio_messaggio']) : '').'</textarea>
								
								</td>
								</tr>';
								
								echo'			<tr><td><strong style="color:red">Note private visibili solo da staff</strong></td>
								<td> 
								<br />';
								echo '<span class="button" onclick="$(\'#mostra_nota_privata\').slideToggle(); " style="cursor: pointer; display:block; color:red; font-size:14px; font-weight:bold; width:752px; border:1px solid #cccccc; text-align:center; background-color:#f0f0ee; text-decoration:none"><img src="../img/admin/arrow.gif" alt="Dettaglio note private" title="Dettaglio note private" style="float:left; margin-right:5px;"/>Clicca qui per inserire note private/riservate - Non visibili all\'utente.</span>';
				
				
				
								echo '<div id="mostra_nota_privata" style="display:none; margin-top:0px">';
								echo '
								
								<textarea id="note_private" class="rte" name="note_private" rows="15" cols="20" style="width:760px;height:130px">
							
								
								
								
								</textarea></div>
								
								
								
								
								</td>
								</tr>';
								
								/*if(isset($_GET['aprinuovomessaggio'])) {
								
								echo '<tr>
								<td>Nota interna (solo per impiegati)</td>
								<td><textarea id="note" name="note" rows="5" cols="145"></textarea>
								</tr>';
								
								}*/
								
								$riferimento = Tools::getValue('riferimento') ? Tools::getValue('riferimento') : Db::getInstance()->getValue('SELECT riferimento FROM customer_thread WHERE id_customer_thread = '.$mex['id_customer_thread']);
								$statusPadre = Customer::statusPadre($riferimento);
								echo '<tr><td colspan="2">'.($riferimento != '' && $statusPadre != 'closed' && $statusPadre != '' ? '<div style="border:3px solid red; padding:5px;"><input type="checkbox" name="chiudi_padre" />&nbsp;&nbsp;&nbsp;<strong style="color:red">ATTENZIONE!</strong>  L\'azione padre risulta ancora aperta. Spunta questa casella se vuoi chiuderla.</div>' : '').'</td></tr>';
								
								echo '<tr><td></td>
								<td>
								<input name="mail_solo_testo" type="checkbox" />Invia come mail di solo testo (senza HTML)<br /><br />
								
									<button type="submit" class="button"  name="submitMessage" id="submitMessage" value="Modifica">
									<img src="../img/admin/email.gif" alt="Invia" title="Invia" />&nbsp;&nbsp;&nbsp;Invia
									</button>
									</td>
								</tr>
							</table>
						</fieldset>
					</form>';
					
					if(isset($_GET['id_mex'])) {
				echo "</div>"; // chiudo mex-2
				} else { }
					
						if(isset($_GET['id_mex'])) {
					echo '<div class="yetii-mex" id="mex-3">';
				
				
				$first = Customer::HierarchyFirst(Tools::getValue('id_mex'), 'T');
				
				echo '<div class="tree">';
				echo '<ul class="tree-menu"><li>'.Customer::trovaSiglaLinkPerAlbero(substr($first,1), substr($first,0,1),'').'';
				echo str_replace('<ul></ul>','',Customer::HierarchyFindChildren($first, $children, 'T'.Tools::getValue('id_mex')));
				echo '</li></ul>';
				echo '</div>';
				echo '<div style="clear:both"></div><br /><br />';
				
					echo '</div>'; // chiudo mex-3
				}	
				
				echo '<div class="yetii-mex" id="mex-4">';
			
				$storico_ticket = Db::getInstance()->executeS('SELECT * FROM storico_attivita WHERE id_attivita = '.$_GET['id_mex'].' AND tipo_attivita = "T" ORDER BY data_attivita DESC, desc_attivita DESC');
				
				if(sizeof($storico_ticket) > 0)
				{
					echo '<table class="table">';
					echo '<thead><tr><th>aaaPersona</th><th>Azione</th><th>Data</th></tr></thead><tbody>';
					
					foreach($storico_ticket as $storico)
					{
						if(is_numeric(substr($storico['desc_attivita'], -1)))
						{	
							$employee = filter_var($storico['desc_attivita'], FILTER_SANITIZE_NUMBER_INT);
							$name_employee = Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM employee WHERE id_employee = '.$employee);
							$desc_attivita = 'Ha messo l\'attivit&agrave; in carico a '.$name_employee;
						}
						else
							$desc_attivita = $storico['desc_attivita'];
						
						echo '<tr><td>'.($storico['id_employee'] == 0 ? 'Cliente' : Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM employee WHERE id_employee = '.$storico['id_employee'])).'</td><td>'.$desc_attivita.'</td><td>'.Tools::displayDate($storico['data_attivita'],$cookie->id_lang, true).'</td></tr>'; 
						
					}	
					
					echo '</tbody></table><br /><br />';
				}
					echo '</div>'; // chiudo mex-4
				
				
				
					if(isset($_GET['id_mex'])) {
				echo "</div>"; // chiudo tutti i msg
				} else { }
					
					
			} // FINE INVIO E VISUALIZZAZIONE MESSAGGIO
			
			if(isset($_POST['submitPreventivo'])) {

				$precedente_status = Db::getInstance()->getValue("SELECT status FROM form_prevendita_thread WHERE id_thread = ".Tools::getValue('id_thread')."");
				$precedente_incarico = Db::getInstance()->getValue("SELECT id_employee FROM form_prevendita_thread WHERE id_thread = ".Tools::getValue('id_thread')."");
				
				switch(Tools::getValue('cambiastatusp'))
				{
					case 'open': $switch_status = 3; break;
					case 'pending1': $switch_status = 4; break;
					case 'closed': $switch_status = 5; break;
					default: $switch_status = 10; break;
				}	
				
				if($precedente_status != Tools::getValue('cambiastatusp'))
					Customer::Storico(Tools::getValue('id_thread'), 'P', $cookie->id_employee, $switch_status);
				
				$tipo_richiesta_p = Db::getInstance()->getValue("SELECT tipo_richiesta FROM form_prevendita_thread WHERE id_thread = '".Tools::getValue('id_thread')."'");
				Db::getInstance()->ExecuteS("UPDATE form_prevendita_thread SET subject = '".addslashes(Tools::getValue('preventivo_subject'))."', note = '".addslashes(Tools::getValue('preventivo_note'))."', status = '".Tools::getValue('cambiastatusp')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_thread = '".Tools::getValue('id_thread')."'");
				
				foreach ($_POST['note_nuova'] as $id_nota=>$nota_id) {
					
					echo $_POST['note_nota'][$id_nota].'<br />';
				
					if($_POST['note_nuova'][$id_nota] == 0) {
						
						$testo_nota = Db::getInstance()->getValue('SELECT note FROM note_attivita WHERE id_note = '.$id_nota.'');
						if($testo_nota != $_POST['note_nota'][$id_nota])
							Db::getInstance()->executeS("UPDATE note_attivita SET note = '".addslashes($_POST['note_nota'][$id_nota])."', date_upd = '".date("Y-m-d H:i:s")."', id_employee = '".$cookie->id_employee."' WHERE id_note = ".$id_nota.""); 
					}
					
					else {
						if(addslashes($_POST['note_nota'][$id_nota]) != '')
						{
							Db::getInstance()->executeS("INSERT INTO note_attivita
							(id_note,
							id_attivita,
							tipo_attivita,
							id_employee,
							note,
							date_add,
							date_upd)
							
							VALUES (
							NULL,
							'".Tools::getValue('id_thread')."',
							'P',
							'".$cookie->id_employee."',
							'".addslashes($_POST['note_nota'][$id_nota])."',
							'".date("Y-m-d H:i:s")."',
							'".date("Y-m-d H:i:s")."'
							)");
						
						}
					}
				}

				$impiegato_attuale = Db::getInstance()->getValue("SELECT id_employee FROM form_prevendita_thread WHERE id_thread = '".Tools::getValue('id_thread')."'");
				
				if($impiegato_attuale != Tools::getValue('assegnaimpiegatop')) {
		
					Customer::Storico(Tools::getValue('id_thread'), 'P', $cookie->id_employee, 2, Tools::getValue('assegnaimpiegatop'));
					
					Db::getInstance()->ExecuteS("UPDATE form_prevendita_thread SET id_employee = '".Tools::getValue('assegnaimpiegatop')."' WHERE id_thread = '".Tools::getValue('id_thread')."'");
							
					$mailimp = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".Tools::getValue('assegnaimpiegatop')."'");
							
					$tokenimp = Tools::getAdminToken("AdminCustomers"."2".Tools::getValue('assegnaimpiegatop'));
							
					$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewpreventivo&id_thread=".Tools::getValue('id_thread')."&token=".$tokenimp.'&tab-container-1=7';

					$msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato il ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." numero ".Tools::getValue('id_preventivo_univoco')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirlo</a>.";
							
					if($impiegato_attuale != 0) {
								
						$mailimprev = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".$impiegato_attuale."'");
						$nomeimprev = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = '".$impiegato_attuale."'");
								
						$msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione del ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." numero ".Tools::getValue('id_preventivo_univoco')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect e l'ha passata ".Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.Tools::getValue('assegnaimpiegatop').'');
								
						$params = array(
						'{msg}' => $msgimprev);
						
						if($impiegato_attuale != $cookie->id_employee)
						{						
							Mail::Send(5, 'msg_base', Mail::l('Gestione '.($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo').' revocata', 5), 
								$params, $mailimprev, NULL, NULL, NULL, NULL, NULL, 
								_PS_MAIL_DIR_, true);
						}
					}
							
					else {
							
					}
							
					$params = array(
					'{msg}' => $msgimp);
							
					//if($_POST['assegnaimpiegatop'] != 0 && $_POST['assegnaimpiegatop'] != $cookie->id_employee) {
							
						Mail::Send(5, 'msg_base', Mail::l(''.($tipo_richiesta_p == 'tirichiamiamonoi' ? 'Ti richiamiamo noi' : 'Preventivo').' assegnato a te su Ezdirect', 5), 
											$params, $mailimp, NULL, NULL, NULL, NULL, NULL, 
											_PS_MAIL_DIR_, true);
											
					//} else { }
							
					
				}
				
				else {
				
				}
				
				$id_contact = Db::getInstance()->getValue('SELECT tipo_richiesta FROM form_prevendita_thread WHERE id_thread = '.Tools::getValue('id_thread'));
				
				if($id_contact != Tools::getValue('id_contact')) 
					Customer::convertiTicket(Tools::getValue('id_thread'), $id_contact, Tools::getValue('id_contact'));
					
				Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&viewpreventivo&id_thread='.Tools::getValue('id_thread').'&token='.$this->token.'&conf=4&tab-container-1=7');
			}
					
			
			if(!isset($_GET['viewpreventivo']) && !isset($_GET['id_mex'])) {
				echo Customer::BuildListing($customer->id, 'preventivo');
			
				
			}
			
			
			else {
			
			
				if(isset($_GET['reinvia'])) {
					
					Customer::Storico(Tools::getValue('id_thread'), 'P', $cookie->id_employee, 12);
				
					$values = Db::getInstance()->getRow("SELECT * FROM form_prevendita_message fm JOIN form_prevendita_thread ft ON ft.id_thread = fm.id_thread WHERE fm.id_message = '".$_GET['reinvia']."'");
				
					$anno = date('Y');
					$anno = substr($anno, 2,2);
					$sigla = "P";
					$idrichiesta = $sigla.$anno.$values['id_thread'];
					$params = array(
						'{reply}' => $gentilecliente.$values['message'],
						'{firma}' => $firma,
						'{id_richiesta}' => "Id richiesta: <strong>".$idrichiesta."</strong>",
						'{link}' => $link->getPageLink('modules/formprevendita/form.php', true).'?id_thread='.$values['id_thread'].'&token='.$values['token']);
					$attachments = array();
						
					$files = explode(";", $values['file_name']);	
						foreach($files as $file) {
							$parti = explode(":::", $file);
							$fileAttachment['content'] = file_get_contents(_PS_MODULE_DIR_.'../upload/'.$parti[0]);
							$fileAttachment['name'] = $parti[1];	
							$extensions = array('.txt' => 'text/plain', '.rtf' => 'application/rtf', '.doc' => 'application/msword', '.docx'=> 'application/msword',
							'.pdf' => 'application/pdf', '.zip' => 'multipart/x-zip', '.png' => 'image/png', '.jpeg' => 'image/jpeg', '.gif' => 'image/gif', '.jpg' => 'image/jpeg');

							$extension = '';
							foreach ($extensions AS $key => $val)
							if (substr($parti[1], -4) == $key OR substr($parti[1], -5) == $key)
							{
								$extension = $val;
								break;
							}
							$fileAttachment['mime'] = $extension;
							$attachments[] = $fileAttachment;			
						}	
						Customer::findEmailPersona($values['email'], Tools::getValue('id_customer'));
						if($is_supplier == 1) {
							if (Mail::Send(5, 'risposta_fornitore', Mail::l('Messaggio da Ezdirect', 5), 
								$params, $values['email'], NULL, 'info@ezdirect.it', 'Ezdirect', $attachments, NULL, 
									_PS_MAIL_DIR_, true)){

								Customer::Storico($values['id_thread'], 'P', $cookie->id_employee, 7);
								
								//$ct->status = 'closed';
								//$ct->update();
								$mail_scrivente = Db::getInstance()->getValue("
									SELECT email 
									FROM employee 
									WHERE id_employee = ".$cookie->id_employee."
								");
								
								$log_mail=fopen("../import/log-mail.txt","a+");
								$riga_log = "PRV | Mail inviata a ".$values['email']." in data ".date("Y-m-d H:i:s")."\n";
								@fwrite($log_mail,$riga_log);
								@fclose($log_mail);
							}
						}
						else {
							if (Mail::Send((int)$cookie->id_lang, 'reply_msg', Mail::l('Messaggio da Ezdirect', (int)$cookie->id_lang), 
								$params, $values['email'], NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
								_PS_MAIL_DIR_, true)) {

								Customer::Storico($values['id_thread'], 'P', $cookie->id_employee, 7);
								//$ct->status = 'closed';
								//$ct->update();
								$mail_scrivente = Db::getInstance()->getValue("
									SELECT email 
									FROM employee 
									WHERE id_employee = ".$cookie->id_employee."
								");
								
								/*Mail::Send(5, 'simple_msg', Mail::l('Copia preventivo inviato al cliente', 5), 
									$params, $mail_scrivente, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
									_PS_MAIL_DIR_, true);*/
									
								$log_mail=fopen("../import/log-mail.txt","a+");
								$riga_log = "PRV | Mail inviata a ".$values['email']." in data ".date("Y-m-d H:i:s")."\n";
								@fwrite($log_mail,$riga_log);
								@fclose($log_mail);
							}
						}
				Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=7');
				}
			
				if (isset($_GET['filename'])) {
					$filename = $_GET['filename'];
					
					if(strpos($filename, ":::")) {
						$parti = explode(":::", $filename);
						$nomecodificato = $parti[0];
						$nomevero = $parti[1];
					}
					else {
						$nomecodificato = $filename;
						$nomevero = $filename;
					}
					
					if (file_exists(_PS_UPLOAD_DIR_.$nomecodificato)) {
						self::openUploadedFile();
					}
				}
				
				if(isset($_GET['id_thread'])) {
				
				
					echo '<div id="tab-container-preventivi" class="tab-containers">
					
					
					
					<ul id="tab-container-preventivi-nav" class="tab-containers-nav">
					<li><a href="#preventivi-1"><strong>Generale</strong></a></li>
					<li><a href="#preventivi-2"><strong>Cronologia</strong></a></li>
					<li><a href="#preventivi-3"><strong>Gerarchia</strong></a></li>
					<li><a href="#preventivi-4"><strong>Storico</strong></a></li>
					</ul>';
					
					
					echo '
					
					<div class="yetii-preventivi" id="preventivi-1">
					<fieldset style="width:95%">';
					
					
					echo '
					<form id="modificapreventivo" action="'.$currentIndex.'&id_customer='.$_GET['id_customer'].'&submitPreventivo&viewcustomer&token='.$this->token.'&tab-container-1=7'.(Tools::getIsset('riferimento') ? '&riferimentoo='.Tools::getValue('riferimento') : '').'" method="post" autocomplete="off">';
					
					
					
					echo '<div class="anagrafica-cliente" style="width:800px; margin-bottom:10px">
					<strong>Oggetto</strong><br />
					
						<textarea name="preventivo_subject" id="preventivo_subjectContent" style="width:690px;height:40px;display:block;float:left">'.strip_tags(htmlspecialchars_decode($oggetto)).'</textarea>
						<br />
			
					<br /><br />
					</div>

					<div class="clear">
					<div class="anagrafica-cliente" style="width:103px">
					ID '.($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo').'<br />
					<span class="tab-span" style="width:103px">'.$idprevunivoco.'</span><br />
					</div>';
					
					
						$aperto_da = Db::getInstance()->getValue('SELECT id_employee FROM form_prevendita_message WHERE id_thread = '.Tools::getValue('id_thread').' ORDER BY id_message ASC');
					
					if($aperto_da != 0)
						$aperto_da = Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$aperto_da);
					else
						$aperto_da = 'Cliente';
					
					echo '
					
					<div class="anagrafica-cliente" style="width:97px">
					Aperto da<br />
					<span class="tab-span" style="width:97px">'.$aperto_da.'</span><br />
					</div>
					
					<div class="anagrafica-cliente">
					In carico a<br />
			<input type="hidden" name="id_preventivo_univoco" value="'.$idprevunivoco.'" />
			<select style="width:220px" name="assegnaimpiegatop">';
			
			echo '<option value="0"'.($cookie->id_employee == 0 ? 'selected="selected"' : '').' >Da assegnare</option>';
			
			foreach ($impiegati_ezp as $impez) {
			
				echo "<option value='".$impez['id_employee']."' ".($preventivo['id_employee'] == $impez['id_employee'] ? 'selected="selected"' : '')." >".$impez['firstname']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>";
			
			}

			echo '</select>
			
					</div>
			
				
					<div class="anagrafica-cliente">
					Data apertura<br />
					<span class="tab-span">'.Tools::displayDate($preventivo['date_add'], (int)($cookie->id_lang), true).'</span><br />
					</div>
					
					<div class="clear"></div>
					<div class="anagrafica-cliente">
					Status<br />
					'.$status_preventivo.'
			<input type="hidden" name="id_customer" value="'.Tools::getValue('id_customer').'" />
			<input type="hidden" name="id_thread" value="'.Tools::getValue('id_thread').'" />
			<select name="cambiastatusp">
			<option value="closed" '.($preventivo['status'] == 'closed' ? 'selected="selected"' : '').'>Chiuso</option>
			<option value="pending1" '.($preventivo['status'] == 'pending1' ? 'selected="selected"' : '').'>In lavorazione</option>
			<option value="open" '.($preventivo['status'] == 'open' ? 'selected="selected"' : '').'>Aperto</option>
			</select>
			
			
			<br />
					</div>
					<div class="anagrafica-cliente">
					Tipo richiesta<br />

					<select style="width:220px" name="id_contact" id="id_contact">
											<option value="4">Assistenza tecnica</option>
											<option value="2">Assistenza amministrativa - contabilit&agrave;</option>
											<option value="8">Assistenza amministrativa - ordini</option>
											<option value="3">Rivenditori</option>
											<option '.($preventivo['tipo_richiesta'] == 'preventivo' ? 'selected="selected"' : '').' value="preventivo">Richiesta di preventivo</option>
											<option '.($preventivo['tipo_richiesta'] == 'tirichiamiamonoi' ? 'selected="selected"' : '').' value="tirichiamiamonoi">Ti richiamiamo noi</option>
											<option value="7">Messaggio</option>
											</select>
					</div>
						
					
					<div class="anagrafica-cliente">
					Data ultima com.<br />
					<span class="tab-span">'.Tools::displayDate($dataultimo, (int)($cookie->id_lang), true).'</span><br />
					</div>
					
					<div class="clear"></div>';
					
					if(is_numeric($preventivo['category'])) {
					
						$cp_interest = Db::getInstance()->getValue('SELECT name FROM product_lang WHERE id_lang = 5 AND id_product = '.$preventivo['category'].'');
					
					}
					
					else {
						
						$cp_interest = $preventivo['category'];
					
					}
					
					echo '<div class="anagrafica-cliente">
					Categoria / Prodotto di interesse<br />
					<span class="tab-span" style="width:360px">'.$cp_interest.'</span><br />
					</div>
					
					<div class="clear"></div>';
					
					if($preventivo['riferimento'] != '')
					{
						echo '
						<div class="clear"></div>
						<div class="anagrafica-cliente" style="width:720px">
						Azione padre<br />
						<span class="tab-span" style="width:720px">'.Customer::trovaSiglaLinkPerAlbero(substr($preventivo['riferimento'],1), substr($preventivo['riferimento'],0,1), '','y:P:'.$preventivo['id_thread'].':'.$preventivo['status']).'</span><br />
						</div><div class="clear"></div>
						';
						
					}

					$preventivo_note = Db::getInstance()->executeS('SELECT * FROM note_attivita WHERE tipo_attivita = "P" AND id_attivita = '.$preventivo['id_thread'].'');
					
					echo '<strong>Note interne visibili solo agli impiegati</strong><br />';
					
					echo '<table><thead>'.(count($preventivo_note) > 0 ? '<tr><th>Nota</th><th>Creata da</th><th>Ultima modifica</th><th></th></tr>' : '' ).'</thead><tbody id="tableNoteBody">
					';
					
					foreach($preventivo_note as $preventivo_nota)
					{
						echo '<tr id="tr_note_'.$preventivo_nota['id_note'].'">';
						echo '
						<td>
						<textarea class="textarea_note" name="note_nota['.$preventivo_nota['id_note'].']" id="note_nota['.$preventivo_nota['id_note'].']" style="width:540px; height:16px" onkeyup="auto_grow(this)">'.htmlentities($preventivo_nota['note'], ENT_COMPAT, 'UTF-8').'</textarea><input type="hidden" name="note_nuova['.$preventivo_nota['id_note'].']" id="note_nuova['.$preventivo_nota['id_note'].']" value="0" /></td>
						<td><input type="text" readonly="readonly" name="note_id_employee['.$preventivo_nota['id_note'].']" value="'.Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM employee WHERE id_employee = '.$preventivo_nota['id_employee']).'" /></td>
						<td><input type="text" readonly="readonly" name="note_date_add['.$preventivo_nota['id_note'].']" value="'.Tools::displayDate($preventivo_nota['date_upd'], 5).'" /></td>';
						echo '<td><a href="javascript:void(0)" onclick="javascript:var surec=window.confirm(\'Sei sicuro?\'); if (surec) { togli_nota('.$preventivo_nota['id_note'].'); cancella_nota_attivita('.$preventivo_nota['id_note'].'); } else { return false; }; "><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" style="cursor:pointer" /></a></td>';
						echo'
						</tr>
						';
					}
					
					echo '</tbody></table><br />';
					
					echo '
					<script type="text/javascript">add_nota_privata();</script>
					<a href="javascript:void(0)" onclick="javascript:add_nota_privata()";><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" /> Clicca per aggiungere una nota privata</a> <br /><br />';
			
					if($preventivo['date_add'] < '2017-12-08')
					{	
						echo '
						
							<textarea name="preventivo_note" id="preventivonoteContent" style="width:80%;height:100px">'.Tools::htmlentitiesUTF8($preventivo['note']).'</textarea><br /><br />';
					}
					
					echo '<button type="submit" class="button" name="submitPreventivo">
					<img src="../img/admin/save.gif" alt="Salva" title="Salva" />&nbsp;&nbsp;&nbsp;'.$this->l('   Save   ').'
					</button>
					<button class="button" onclick="javascript:var surec=window.confirm(\'Sei sicuro?\'); if (surec) { $(\'#coll_destinazione\').val($(\'#collega_prv\').val()); document.forms[\'collega_attivita\'].submit(); return false; } else { return false; }; " style="margin-left:10px; margin-right:0px">
					
					<img src="../img/admin/link.gif" alt="Collega" title="Collega" />&nbsp;&nbsp;&nbsp;'.$this->l('Collega a:').'
					</button>
					<select name="collega_prv" id="collega_prv" style="margin-left:-4px; height:27px;width:150px" >
						<option value="">-- Seleziona --</option>
						'.Customer::BuildSelectForLinking($customer->id, $preventivo['id_thread'], 'P').'
					</select>
					';
						
					
					echo '</form>
				
					<form style="margin-left:5px; display:block;float:left" name="collega_attivita" id="collega_attivita" action="'.htmlentities($_SERVER['REQUEST_URI']).'&conf=4" method="post" />
					<input type="hidden" name="submitCollegaAttivita" value="" />
					<input type="hidden" name="coll_destinazione" id="coll_destinazione" value="" />
					<input type="hidden" name="coll_partenza" id="coll_partenza" value="P'.$preventivo['id_thread'].'" />
					</form>';
					
					
			echo '</form>
			</fieldset><br /><br /></div>';
			
			
			}
		
			echo '<div class="yetii-preventivi" id="preventivi-2">';
					
				
				$messaggip = Db::getInstance()->ExecuteS("SELECT * FROM form_prevendita_message fm WHERE fm.id_thread = ".$_GET['id_thread']."");
				
				foreach($messaggip as $messaggiop) {
				
					echo "<table style='width:100%; 
					border:1px solid #dfd5c3; 
					";
					if ($messaggiop['id_employee'] == 0) {
						echo 'background-color: #ffffff;';
					} else {
			
						echo 'background-color: #ffecf2;';
					}
					$employeez = new Employee($messaggiop['id_employee']);
					
					
					if(is_numeric($preventivo['category'])) {
					
						$categoriaint =  Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_product = '$preventivo[category]' AND id_lang = 5");
					
					} else {
						$categoriaint = $preventivo['category'];
					
					}
					
					echo "margin-bottom:15px'>";
					echo '<tr>
						<td style="colspan:2">
							<table>
					
							<td style="font-size:14px; width:250px">Da: <strong>';if ($messaggiop['id_employee'] == 0) { echo $preventivo['firstname']." ".$preventivo['lastname']; } else { echo $employeez->firstname." ".substr($employeez->lastname,0,1)."."; } echo '</strong></td>
							
							<td style="width:170px"><strong>Data: </strong>'.date("d-m-Y H:i:s",strtotime($messaggiop['date_add'])).'</td>
							<td style="width:180px"><strong>Allegato: </strong>';
							if(!empty($messaggiop['file_name'])) {
							
								$allegatip = explode(";",$messaggiop['file_name']);
								$nallp = 1;
								foreach($allegatip as $allegatop) {
									
									if(strpos($allegatop, ":::")) {
						
										$parti = explode(":::", $allegatop);
										$nomeverop = $parti[1];
										
									}
						
									else {
										$nomeverop = $allegatop;
						
									}
									
									if($allegatop == "") { } else {
										if($nallp == 1) { } else { echo " - "; }
										echo '<a href="ajax.php?openAllegato=y&id_customer='.$customer->id.'&viewcustomer&viewpreventivo&id_thread='.$preventivo['id_thread'].'&token='.$this->token.'&tab-container-1=7&filename='.$allegatop.'"><span style="color:red">'.$nomeverop.'</span></a>';
										$nallp++;
									}
								}
							
							}
							
							else { echo 'Nessun allegato'; }
							
							
							echo '</td>';
							
							
							
							echo '<td></td></tr></table>
						</td>
						</tr>
						<tr>
						<td style="colspan:2">
							<table>
							<tr>
							<td style="width:100px"><strong>Messaggio</strong></td>
							<td>'.htmlspecialchars_decode($messaggiop['message']).($messaggiop['id_employee'] == 0 ? '' : '<form method="post">
							<input type="hidden" name="modifica_messaggio_preventivo" value="'.htmlentities($messaggiop['message'], ENT_QUOTES | ENT_SUBSTITUTE, "UTF-8").'" />
							<button type="submit" class="button" name="modifica_messaggio_submit" value="Modifica">
							<img src="../img/admin/edit.gif" alt="Modifica" title="Modifica" />&nbsp;&nbsp;&nbsp;Modifica
							</button>
							</form>').'<br />
							'.($messaggiop['id_employee'] == 0 ? '' : '<a class="button_invio" style="cursor:pointer" onclick="javascript: var sure=window.confirm(\'Sei sicuro di reinviare questo messaggio?\'); if (sure) { submitFormOkay = false; document.location = \''.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewpreventivo&id_thread='.$preventivo['id_thread'].'&token='.$this->token.'&tab-container-1=7&reinvia='.$messaggiop['id_message'].'\' } else { }""><img src="../img/admin/email.gif" alt="Reinvia" title="Reinvia" />&nbsp;&nbsp;&nbsp;Reinvia</a>').'
							
							</td>
							</tr>
							</table>
						</td>';
						echo '</tr>';
						
						if($messaggiop['note_private'] != '')
						{
							echo '<tr>
							<td style="colspan:2">
								<table>
								<tr>
								<td style="width:100px"><strong style="color:red">Nota privata</strong></td>
								<td style="color:red">'.htmlspecialchars_decode(preg_replace('/(<[^>]+) style=".*?"/i', '$1', preg_replace('/(<[^>]+) class=".*?"/i', '$1', $messaggiop['note_private']))).'
								</td>
								</tr>
								</table>
							</td>';
							echo '</tr>
							';
						}
						
							if($messaggiop['email'] == '') { 
							
								echo '<tr><td style="colspan:2">
								<table>
								<tr>
								
								<td style="width:100px"><strong>In carico a</strong></td>
								<td>'.Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$messaggiop['in_carico']).'</td>
								';
							
							
							} else {
						
						echo '<tr><td style="colspan:2">
							<table>
							<tr>
							<td><strong>Inviato a: </strong>'.(is_numeric($messaggiop['email']) ? Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$messaggiop['email']) : htmlspecialchars_decode($messaggiop['email'])).'</td>
							
							'.($messaggiop['in_carico'] > 0 ? '<td style="padding-left:20px"><strong>In carico a</strong>:
							'.($messaggiop['in_carico'] > 0 ? Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$messaggiop['in_carico'])  : '--').'</td>' : '').'
							';
						
						}
						
						$cc = Db::getInstance()->getValue("SELECT cc FROM ticket_cc WHERE type = 'p' AND msg = ".$messaggiop['id_message']."");
							if(!empty($cc)) {
							
								echo '<td style="padding-left:20px">';
							
								$ccs = explode(";",$cc);
								$cc_str = "<strong>CC:</strong> ";
								
								foreach ($ccs as $conoscenza) {
								
									$imp = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".$conoscenza."");
									$cc_str .= $imp." ";
								}
								
								echo $cc_str;
								echo "</td>";
							
							}
							else {
							
								echo '<td style="padding-left:20px"><strong>CC:</strong> Nessuno</td>';
							
							}
						
						
						echo '</tr>';
						
						if($cookie->id_employee == 6 || $cookie->id_employee == 22 || $cookie->id_employee == 1)
						{
							$inoltra_count = 0;
							$inoltra_string = '';
								foreach ($impiegati_ezp as $impez) {
			
									$inoltra_count++;
									
									
									$inoltra_string .= "<input type='checkbox' class='inoltrap' name='inoltrap[]' id='inoltra_".$impez['id_employee']."' value='".$impez['id_employee']."' /> ".$impez['firstname']." &nbsp;&nbsp;&nbsp;&nbsp;";
									if($inoltra_count == 8)
										$inoltra_string .= '<br />';
								}
							echo '<tr><td><strong>Inoltra CC a:</strong></td><td>'.$inoltra_string.' <br /><span id="inoltrap_response"></span></td><td><button class="button" onclick="javascript:var surec=window.confirm(\'Sei sicuro?\'); if (surec) { var checkedVals = $(\'.inoltrap:checkbox:checked\').map(function() { return this.value; }).get(); inoltraP(checkedVals.join(\',\')); } " style="margin-left:10px; margin-right:0px">
							
							
						
						<img src="../img/admin/email.gif" alt="Inoltra" title="Inoltra" />&nbsp;&nbsp;&nbsp;'.$this->l('Inoltra').'
						</button><script type="text/javascript">
								function inoltraP(checkedVals)
								{	
									$("#inoltrap_response").html("<img src=\"../img/loader.gif\" />").show();

									$.post("ajax.php", {inoltra:1,azione_da:'.$cookie->id_employee.',id_customer:'.(int)$customer->id.',utenti:checkedVals,tipo:"'.($preventivo['tipo_richiesta'] == 'tirichiamiamonoi' ? 'tirichiamiamonoi' : 'preventivo').'",id_messaggio:"'.$messaggiop['id_message'].'",id_thread:"'.$preventivo['id_thread'].'"}, function (r) {
										
										if (r == "ok")
										{
											$("#inoltrap_response").html("<img src=\"..\/img\/admin\/ok.gif\" alt=\"OK\" title=\"OK\"  \/>").fadeIn(400);
											location.reload(); 
										}
										else if (r == "error:validation")
											$("#inoltrap_response").html("<b style=\"color:red\">'.addslashes($this->l('Errore')).'</b>").fadeIn(400);
										else if (r == "error:update")
											$("#inoltrap_response").html("<b style=\"color:red\">'.addslashes($this->l('Errore')).'</b>").fadeIn(400);
										$("#inoltrap_response").fadeOut(5000);
									});
								}
							</script></td></tr>';
						}
						echo '
								</table>
								</td></tr></table><strong>Prodotto / categoria di interesse</strong>: '.$cp_interest.'<br />';
				}
				
				
			
			if (Tools::isSubmit('submitPreventivoReply')) {
					
					$tipo_richiesta_p = Db::getInstance()->getValue("
						SELECT tipo_richiesta 
						FROM form_prevendita_thread 
						WHERE id_thread = ".$preventivo['id_thread']."
					");

					$files=array();
					$fdata=$_FILES['joinFile'];
					if(is_array($fdata['name'])){
						$count_name = count($fdata['name']);
						for($i=0;$i<$count_name;++$i){
							$files[]=array(
								'name'    =>$fdata['name'][$i],
								'tmp_name'=>$fdata['tmp_name'][$i],
								'type' => $fdata['type'][$i],
								'size' => $fdata['size'][$i],
								'error' => $fdata['error'][$i],
							 
							);
						}
					}
					else $files[]=$fdata;
										
					$attachments = array();
					
					
					foreach($files as $file) {
					
						if (isset($files) AND !empty($file['name']) AND $file['error'] != 0)
							$this->_errors[] = Tools::displayError('An error occurred with the file upload.');
							
							
							if (!empty($file['name']))
								{
									$extension = array('.txt', '.rtf', '.doc', '.docx', '.pdf', '.zip', '.png', '.jpeg', '.gif', '.jpg');
									$filename = md5(uniqid().substr($file['name'], -5));
									$fileAttachment['content'] = file_get_contents($file['tmp_name']);
									$fileAttachment['name'] = $file['name'];
									$fileAttachment['mime'] = $file['type'];
								}
								
								if (isset($filename) AND rename($file['tmp_name'], _PS_MODULE_DIR_.'../upload/'.$filename)) {
									$filename = $filename.":::".$file['name'];
									$file_name .= $filename.";";
							}
							
							$attachments[] = $fileAttachment;
							
					}
						
					if(Tools::getValue('messaggiop') == '') {
						
						$precedente_status = Db::getInstance()->getValue("SELECT status FROM form_prevendita_thread WHERE id_customer_thread = ".$preventivo['id_thread']."");
						if($precedente_status != Tools::getValue('status'))
						{	
							switch(Tools::getValue('status'))
							{
								case 'open': $switch_status = 3; break;
								case 'pending1': $switch_status = 4; break;
								case 'closed': $switch_status = 5; break;
								default: $switch_status = 10; break;
							}	
							
							Customer::Storico($preventivo['id_thread'], 'P', $cookie->id_employee, $switch_status);
				
						}
						
						$precedente_incarico = Db::getInstance()->getValue("SELECT id_employee FROM form_prevendita_thread WHERE id_customer_thread = ".$preventivo['id_thread']."");
						
						if($precedente_incarico != 0) {
								
							$mailimprev = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".$precedente_incarico."'");
							$nomeimprev = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = '".$precedente_incarico."'");
							
							$employeemess = new Employee($cookie->id_employee);
									
							$msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione del ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." numero ".Tools::getValue('id_preventivo_univoco')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect e l'ha passata ".Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.Tools::getValue('assegnaimpiegatop').'');
									
							$params = array(
							'{msg}' => $msgimprev);
							
							if($precedente_incarico != $cookie->id_employee)
							{						
								Mail::Send(5, 'msg_base', Mail::l('Gestione '.($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo').' revocata', 5), 
									$params, $mailimprev, NULL, NULL, NULL, NULL, NULL, 
									_PS_MAIL_DIR_, true);
							}
						}
								
						else {
								
						}
						
						
						if($precedente_incarico != Tools::getValue('in_carico_a_p'))
							Customer::Storico($preventivo['id_thread'], 'P', $cookie->id_employee, 2, Tools::getValue('in_carico_a_p'));
				
						Db::getInstance()->executeS("UPDATE form_prevendita_thread SET status = '".Tools::getValue('status')."', id_employee = '".Tools::getValue('in_carico_a_p')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_thread = ".$preventivo['id_thread']."");
						
						$this->assegna_incarico(Tools::getValue('in_carico_a_p'), $preventivo['id_thread'], 'preventivo');

						Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&error=1pv&token='.$this->token.'&tab-container-1=7');
								
				
					}
						
					
					if(Tools::getValue('nuovopreventivo') == 1) {
					
						$ultimothread = Db::getInstance()->getValue("SELECT id_thread FROM form_prevendita_thread ORDER BY id_thread DESC");
						$thread = $ultimothread+1;
						
						$addresscli = Db::getInstance()->getValue("SELECT id_address FROM address WHERE fatturazione = 1 AND active = 1 AND deleted = 0 AND id_customer = ".$customer->id."");
						
						$addresscliente = new Address($addresscli);
						$token = Tools::passwdGen(12);
						
						$cstmr = Db::getInstance()->getRow("SELECT company, firstname, lastname, is_company FROM customer WHERE id_customer = ".(int)($customer->id)."");
							
							
						if($cstmr['is_company'] == 1) { $cstmr_r = $cstmr['company']; } else { $cstmr_r = $cstmr['firstname']." ".$cstmr['lastname']; }
						
						$subject = "*** RICHIESTA COMMERCIALE da ".$cstmr_r." - Tipo richiesta: Preventivo ***";
						
						
						Db::getInstance()->ExecuteS("INSERT INTO form_prevendita_thread (id_thread, tipo_richiesta, id_customer, id_employee, subject, tax_code, vat_number, firstname, lastname, company, address1, postcode, state, country, email, phone, token, status, category, date_add, date_upd, note) VALUES ('$thread', 'preventivo', '".$customer->id."', '".Tools::getValue('in_carico_a_p')."', '".addslashes($subject)."', '".addslashes($customer->tax_code)."', '".addslashes($customer->vat_number)."', '".addslashes($customer->firstname)."', '".addslashes($customer->lastname)."', '".addslashes($customer->company)."', '".addslashes($addresscliente->address1)."', '".addslashes($addresscliente->postcode)."', '".addslashes($addresscliente->id_state)."', '".addslashes($addresscliente->id_country)."', '".addslashes($customer->email)."', '".addslashes($addresscliente->phone)."', '$token', '".addslashes(Tools::getValue('status'))."', '', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."', '".Tools::getValue('note')."')");   
						
						Db::getInstance()->ExecuteS("INSERT INTO form_prevendita_message (id_message, id_thread, id_customer, id_employee, in_carico, file_name, message, note_private, date_add, date_upd) VALUES (NULL, '$thread', ".$customer->id.", '".$cookie->id_employee."', '".Tools::getValue('in_carico_a_p')."', '".addslashes($file_name)."', '".addslashes(Tools::getValue('messaggiop'))."', '".addslashes(Tools::getValue('note_private'))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
						
						Customer::Storico($thread, 'P', $cookie->id_employee, 13);
						
						$mail_verso_cui_inviare = $customer->email;
						
						$sigla = 'P';
						$idmessaggio = mysqli_insert_id($link_glob);

					}
					
					else {
						
						$token = $preventivo['token'];
						$thread = $preventivo['id_thread'];
						
						$precedente_status = Db::getInstance()->getValue("SELECT status FROM form_prevendita_thread WHERE id_customer_thread = ".$preventivo['id_thread']."");
						if($precedente_status != Tools::getValue('status'))
						{	
							switch(Tools::getValue('status'))
							{
								case 'open': $switch_status = 3; break;
								case 'pending1': $switch_status = 4; break;
								case 'closed': $switch_status = 5; break;
								default: $switch_status = 10; break;
							}	
							
							Customer::Storico($preventivo['id_thread'], 'P', $cookie->id_employee, $switch_status);
				
						}
						
						$precedente_incarico = Db::getInstance()->getValue("SELECT id_employee FROM form_prevendita_thread WHERE id_customer_thread = ".$preventivo['id_thread']."");
						
						if($precedente_incarico != Tools::getValue('in_carico_a_p'))
							Customer::Storico($preventivo['id_thread'], 'P', $cookie->id_employee, 2, Tools::getValue('in_carico_a_p'));
					
						Db::getInstance()->ExecuteS("UPDATE form_prevendita_thread SET  status = '".addslashes(Tools::getValue('status'))."', date_upd = '".date("Y-m-d H:i:s")."', id_employee = '".Tools::getValue('in_carico_a_p')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_thread = '".$preventivo['id_thread']."'");  
					
						Db::getInstance()->ExecuteS("INSERT INTO form_prevendita_message (id_message, id_thread, id_customer, id_employee, in_carico, file_name, message, note_private, date_add, date_upd) VALUES (NULL, '".$preventivo['id_thread']."', ".$customer->id.", '".$cookie->id_employee."', '".Tools::getValue('in_carico_a_p')."','".addslashes($file_name)."', '".addslashes(Tools::getValue('messaggiop'))."', '".addslashes(Tools::getValue('note_private'))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
						
						$mail_verso_cui_inviare = $preventivo['email'];
						
						$idmessaggio = mysqli_insert_id($link_glob);
					}
					
					Customer::chiudiPadre(Db::getInstance()->getValue('SELECT riferimento FROM form_prevendita_thread WHERE id_thread = '.$preventivo['id_thread']));
					
					if(Tools::getIsset('riferimentoo') && Tools::getValue('riferimentoo') != '')
					{	
						if(substr(Tools::getValue('riferimentoo'),0,1) == 'C')
						{
							$note_da_copiare = Db::getInstance()->executeS('SELECT * FROM cart_note WHERE id_cart = "'.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo'))).'"');
						}
						else if(substr(Tools::getValue('riferimentoo'),0,1) == 'O')
						{
							$note_da_copiare = Db::getInstance()->executeS('SELECT * FROM cart_note WHERE id_cart = "'.Db::getInstance()->getValue('SELECT id_cart FROM orders WHERE id_order = '.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo')))).'"');
						}
						else
						{ 
							$note_da_copiare = Db::getInstance()->executeS('SELECT * FROM note_attivita WHERE id_attivita = "'.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo'))).'" AND tipo_attivita = "'.substr(Tools::getValue('riferimentoo'),0,1).'"');
						}
						foreach($note_da_copiare as $ndc)
						{
							Db::getInstance()->executeS('INSERT INTO note_attivita (id_note, id_attivita, tipo_attivita, id_employee, note, date_add, date_upd) VALUES (NULL, '.$preventivo['id_thread'].', "P", "'.$ndc['id_employee'].'","'.$ndc['note'].'", "'.$ndc['date_add'].'","'.$ndc['date_upd'].'")');
						}
					}	
					
					
					Db::getInstance()->ExecuteS("UPDATE form_prevendita_message SET email = '".Tools::getValue('customer_email_msg_p')."' WHERE id_message = ".$idmessaggio."");
						
				
					if($preventivo['tipo_richiesta'] == 'preventivo') {
						$sigla = 'P';
					}
					else if($preventivo['tipo_richiesta'] == 'tirichiamiamonoi') {
						$sigla = 'R';
					}
					
					$anno = date('Y');
							$anno = substr($anno, 2,2);
							
					$numerounivoco = $sigla.$anno.$preventivo['id_thread'].$idmessaggio;
						
					$primoticket = Db::getInstance()->getValue("SELECT id_message FROM form_prevendita_message WHERE id_thread = $preventivo[id_thread] ORDER BY id_message ASC");
							
					if(!$primoticket) {
								
						$primoticket = $idmessaggio;
							
					}
							
					else {
							
					}	
					$primoticket = $sigla.$anno.$preventivo['id_thread'].$primoticket;
					
					$file_csv=fopen("../cms/quotazioni/servizio-clienti.csv","a+");
					
					$data_quotazione = date("d/m/Y H:i:s");
					
					$categoria = $preventivo['category'];
					
					if(is_numeric($categoria)) {
					
						$intcat = "Prodotto di interesse";
						$categoria =  Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_product = '$categoria' AND id_lang = 5");
					}
					else {
						$intcat = "Categoria di interesse";
						$categoria = $preventivo['category'];
					}

					$in_carico_a =  Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".Tools::getValue('in_carico_a_p')."");
					if(empty($in_carico_a)) { $in_carico_a = "Nessuno"; }
					
					$messaggioclientepercsv = Tools::getValue('messaggiop');
					$messaggioclientepercsv = strip_tags($messaggioclientepercsv);
					$messaggioclientepercsv = str_replace(";", ",", $messaggioclientepercsv);
						
					$messaggioclientepercsv = str_replace(chr(10), " ", $messaggioclientepercsv); //remove carriage returns
					$messaggioclientepercsv = str_replace(chr(13), " ", $messaggioclientepercsv); //remove carriage returns	
					
					$employeeincaricato = new Employee($cookie->id_employee);
					$employeeincaricato2 = new Employee($preventivo['id_employee']);
					if(Tools::getValue('nuovopreventivo') == 1) {
						$idrichiesta = $sigla.$anno.$thread;
					}
					else {
						$idrichiesta = $sigla.$anno.$preventivo['id_thread'];
					}

					$riga_richiesta = $categoria.";".$data_quotazione.";;;$preventivo[vat_number];$preventivo[tax_code];".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;;;;;;;".$messaggioclientepercsv.";$idmessaggio;$preventivo[id_thread];$numerounivoco;$primoticket;prevendita;R;$idrichiesta;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
					
					if($tipo_richiesta_p == 'preventivo') {
						@fwrite($file_csv,$riga_richiesta);
					}
							
					@fclose($file_csv);	
					
					$params = array(
					'{reply}' => $gentilecliente.(Tools::getValue('messaggiop')),
					'{firma}' => $firma,
					'{id_richiesta}' => "Id richiesta: <strong>".$idrichiesta."</strong>",
					'{link}' => $link->getPageLink('modules/formprevendita/form.php', true).'?id_thread='.$thread.'&token='.$token);
					
					
					$params2 = array(
					'{reply}' => $employeeincaricato->firstname." ".$employeeincaricato->lastname." ha aperto un ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." per conto di un cliente. L'id del preventivo &egrave;: ".$idrichiesta."");
					
					$params3 = array(
					'{reply}' => $employeeincaricato->firstname." ".$employeeincaricato->lastname." ti ha assegnato un ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." su Ezdirect. L'id del ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." &egrave;: ".$idrichiesta." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.")");
					
					$mails_preventivi = explode(";", Tools::getValue('customer_email_msg_p'));
							
					foreach ($mails_preventivi as $mail) {
						$mail = trim($mail);
						Customer::findEmailPersona($mail, Tools::getValue('id_customer'));
						if (Mail::Send((int)$cookie->id_lang, 'reply_msg', Mail::l('Messaggio da Ezdirect', (int)$cookie->id_lang), 
							$params, $mail, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
							_PS_MAIL_DIR_, true)) {

							Customer::Storico($preventivo['id_thread'], 'P', $cookie->id_employee, 7);
							//$ct->status = 'closed';
							//$ct->update();
							$mail_scrivente = Db::getInstance()->getValue("
								SELECT email 
								FROM employee 
								WHERE id_employee = ".$cookie->id_employee."
							");
							
							/*Mail::Send(5, 'simple_msg', Mail::l('Copia preventivo inviato al cliente', 5), 
								$params, $mail_scrivente, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
								_PS_MAIL_DIR_, true);*/
							Customer::Storico(Tools::getValue('id_thread'), 'P', $cookie->id_employee, 7);
							
							$log_mail=fopen("../import/log-mail.txt","a+");
							$riga_log = "PRV | Mail inviata a ".$mail." in data ".date("Y-m-d H:i:s")."\n";
							@fwrite($log_mail,$riga_log);
							@fclose($log_mail);
						}
					}
					
					$cc = "";
					$cc_str = "";
					foreach($_POST['conoscenzap'] as $conoscenza) {
						$cc_str = $cc_str.Db::getInstance()->getValue('
							SELECT firstname 
							FROM employee 
							WHERE id_employee = '.$conoscenza).' 
						';
					}
						
					foreach($_POST['conoscenzap'] as $conoscenza) {
					
						$cc .= $conoscenza.";";
						echo $conoscenza."<br />";
						
						$mailconoscenza = Db::getInstance()->getValue('
							SELECT email 
							FROM employee 
							WHERE id_employee = $conoscenza
						');
						
						$tokenimp = Tools::getAdminToken("AdminCustomers"."2".$conoscenza);
								
						$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewpreventivo&id_thread=".$thread."&token=".$tokenimp.'&tab-container-1=7';
							
						$params4 = array(
						'{reply}' => "<strong>".$employeemess->firstname."</strong> ha inviato un ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." a un cliente e ti ha messo in copia conoscenza.
						<br /><br />
						L'ID del ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." &egrave;: <strong>".$idrichiesta."</strong><br /><br />
						Il ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." &egrave; in carico a: <strong>".$in_carico_a."</strong><br /><br />
						Il messaggio &egrave; stato inviato in conoscenza a: ".$cc_str."<br /><br />
						<a href='".$linkimp."'>Clicca qui per aprire il ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')."</a>. ",
						'{firma}' => $firma,
						'{id_richiesta}' => "Id richiesta: <strong>".$idrichiesta."</strong>",
						'{link}' => $link->getPageLink('modules/formprevendita/form.php', true).'?id_thread='.$thread.'&token='.$token);
								
						Mail::Send((int)$cookie->id_lang, 'senzagrafica', Mail::l(($tipo_richiesta_p == 'tirichiamiamonoi' ? 'Ti richiamiamo noi' : 'Preventivo').' utente in copia conoscenza', (int)$cookie->id_lang), 
						$params4, $mailconoscenza, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
						_PS_MAIL_DIR_, true);
					}
					
					if(!empty($_POST['conoscenzap']))
						Db::getInstance()->executeS("
							INSERT INTO ticket_cc (msg, type, cc) 
							VALUES (".$idmessaggio.", 'p', '".$cc."')
						");
				
					/*Mail::Send((int)$cookie->id_lang, 'action', Mail::l('Aperto un preventivo per conto del cliente', (int)$cookie->id_lang), 
								$params2, "ezio.delgiudice@ezdirect.it", NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
								_PS_MAIL_DIR_, true);*/
					
					if(Tools::getValue('in_carico_a_p') != 0) {

						if (Tools::getValue('in_carico_a_p') == $preventivo['id_employee']) {
									
						}
						
						else if (Tools::getValue('in_carico_a_p') == $cookie->id_employee) {
									
						}
						
						else {
							$mail_incarico_p = Db::getInstance()->getValue('
								SELECT email 
								FROM employee 
								WHERE id_employee = '.Tools::getValue('in_carico_a_p').'
							');

							$tokenimp = Tools::getAdminToken("AdminCustomers"."2".Tools::getValue('in_carico_a_p'));
							$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewpreventivo&id_thread=".$thread."&token=".$tokenimp.'&tab-container-1=7';
							
							$params3 = array(
							'{reply}' => $employeeincaricato->firstname." ".$employeeincaricato->lastname." ti ha assegnato un ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." su Ezdirect. L'id del ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." &egrave;: ".$idrichiesta." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.")<br /><br />
							Il messaggio &egrave; stato inviato in conoscenza a: ".$cc_str."<br /><br />
							<a href='".$linkimp."'>Clicca qui per rispondere</a>.
							");
							
							Mail::Send(5, 'action', Mail::l(''.($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo').' cliente assegnato a te su Ezdirect', 5), 
							$params3, $mail_incarico_p, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
							_PS_MAIL_DIR_, true);
						}
					
						Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=7');
					}
					else {
						Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&viewpreventivo&id_thread='.$thread.'&preventivoreply&conf=4&token='.$this->token.'&tab-container-1=7');
					}
				}
				else {
				
					if (isset($_GET['preventivoreply'])) {
						echo 'Risposta inviata con successo.<br /> <a class="button" href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewpreventivo&id_thread='.$preventivo['id_thread'].'&token='.$this->token.'&tab-container-1=7"><strong>Clicca qui se vuoi rispondere nuovamente.</strong></a><br /><br />';
					}
					else if (isset($_GET['preventivoreply2'])) {
						echo 'Preventivo aperto con successo. <br /><br />';
					}
					else if (!isset($_GET['id_mex']) && (!isset($_GET['preventivoreply']) || isset($_GET['aprinuovopreventivo']))) {
						$iso = Language::getIsoById((int)($cookie->id_lang));
						$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
						$ad = dirname($_SERVER["PHP_SELF"]);
			
			
						if(isset($_GET['aprinuovopreventivo'])) {
							echo '<h2>Invia preventivo al cliente</h2>';
						}
						else {
							echo '<h2 id="rispondi-cliente-preventivo">Rispondi al cliente</h2>';
							if(isset($_POST['modifica_messaggio_submit'])) {
							echo '<script type="text/javascript">
								 window.location = "#rispondi-cliente-preventivo";
							</script>';
							}
						}
			
					echo '
				<script type="text/javascript">
				
				noConfirm = false;

				</script>	
				
				<script type="text/javascript">
				var iso = \''.$isoTinyMCE.'\' ;
				var pathCSS = \''._THEME_CSS_DIR_.'\' ;
				var ad = \''.$ad.'\' ;
				</script>
				
				
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce3/tiny_mce.js"></script>
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>';
				
				echo '<script type="text/javascript">
					$(document).ready(function () {
					
	dataChanged = 0;     // global variable flags unsaved changes      

	function bindForChange(){    
		 $(\'input,checkbox,text,textarea,radio,select\').bind(\'change\',function(event) { dataChanged = 1});
		 $(\'#messaggiop\').bind(\'input propertychange\', function() { dataChanged = 1 });
		 $(\':reset,:submit\').bind(\'click\',function(event) { dataChanged = 0 });
	}


	function askConfirm(){  
		if (dataChanged){ 
			return "You have some unsaved changes.  Press OK to continue without saving." 
		}
	}

	window.onbeforeunload = askConfirm;
	window.onload = bindForChange;
	});
				</script>';
				
				
				
				
				echo '<!-- <script type="text/javascript">
						toggleVirtualProduct(getE(\'is_virtual_good\'));
						unitPriceWithTax(\'unit\'); 
				</script> -->';
				$employeemessp = new Employee($cookie->id_employee);
					echo'	<form name="submit_preventivo" id="submit_preventivo" action="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewpreventivo&id_thread='.$preventivo['id_thread'].'&token='.$this->token.'&tab-container-1=7'.(Tools::getIsset('riferimento') ? '&riferimentoo='.Tools::getValue('riferimento') : '').'" method="post" class="std" enctype="multipart/form-data">
					<fieldset>';
					
					if(isset($_GET['aprinuovopreventivo'])) {
				
					echo '<input type="hidden" name="nuovopreventivo" value="1" />';
				
					}
					
					else {
					
						echo '			<script type="text/javascript">
						
							$(document).ready(function () {
								
								$("#submit_preventivo").submit(function () {
									d = document.getElementById("status").value;
									
									if(d != "'.$preventivo['status'].'") {
											}
									
									else {
										var salvamodifiche = window.confirm("STATUS: OK per procedere, ANNULLA per cambiare."); 
								
											if (salvamodifiche) {
			$(\'#waiting1\').waiting({ 
			elements: 10, 
			auto: true 
			});
			var overlay = jQuery(\'<div id="overlay"></div>\');
			var attendere = jQuery(\'<div id="attendere">ATTENDERE PREGO</div>\');
			overlay.appendTo(document.body);
			attendere.appendTo(document.body);
			
												$("#submitMessage").trigger("click");
											} else {
												 return false;
													
												//niente
											}
									}
								});
								
								
							});
				
						</script>';	
						
					}
					
					
					$customer_phone = Db::getInstance()->getRow("SELECT	phone, phone_mobile FROM address WHERE id_customer = ".$customer->id." AND deleted = 0 AND fatturazione = 1 AND active = 1");
								
								if($customer_phone['phone'] == '') { $telefono_cliente = $customer_phone['phone_mobile']; } else { $telefono_cliente = $customer_phone['phone']; }
								
						echo '<table>
						<tr><td>Email</td><td>';
								
								
								if($customer->is_company == 1) { echo '<div style="float:left">';
								
								$persone = Db::getInstance()->executeS("SELECT id_persona, firstname, lastname, email FROM persone WHERE id_customer = ".Tools::getValue('id_customer')." AND email != ''");
								
								$persona_preventivo = Db::getInstance()->getValue('SELECT id_persona FROM persone WHERE firstname = "'.$preventivo['firstname'].'" AND lastname = "'.$preventivo['lastname'].'" AND email = "'.$preventivo['email'].'"');
								
								echo '
								<select style="width:160px" name="seleziona-personap" onchange="javascript:document.getElementById(\'customer_email_msg_p\').value = (this.value);" >';
								echo "<option value=''>-- Seleziona persona --</option>";
								
								
								
								foreach ($persone as $persona) {
								
									echo "<option ".($persona['id_persona'] == $persona_preventivo ? 'selected="selected"' : "")."  value='".$persona['email']."'>".$persona['firstname']." ".$persona['lastname']."</option>";
								
								}
								
								echo "</select></div>";
						
						
								} else { }
						
						
						
						
						
						
						
						
						echo '<div style="float:left; '.($customer->is_company == 1? 'margin-left:20px' : '').'"><input type="text" size="40" name="customer_email_msg_p" id="customer_email_msg_p" value="'.(!$preventivo['email'] ? $customer->email : $preventivo['email']).'" /><br />
								<span style="font-size:11px">In caso di più indirizzi mail, separare con punto e virgola (;)</span></div><div style="float:left; margin-left:20px"> Tel. cliente: <span class="tab-span" style="width:200px; padding-left:16px; padding-right:16px; display:inline"><a href="callto:'.(!$preventivo['phone'] ? $telefono_cliente : $preventivo['phone']).'">'.(!$preventivo['phone'] ? $telefono_cliente : $preventivo['phone']).'</a></span> </div></td></tr>
						
						';
					
							
						echo '
							
							<tr><td>Stato preventivo</td>
							<td>
							<div style="float:left">
							<select name="status" id="status">
							<option value="open" '.($preventivo['status'] == 'open' ? 'selected="selected"' : '').'>Aperto   </option>
							<option value="pending1" '.($preventivo['status'] == 'pending1' ? 'selected="selected"' : '').'>In lavorazione   </option>
							<option value="closed" '.($preventivo['status'] == 'closed' ? 'selected="selected"' : '').'>Chiuso   </option>
							
							</select>
							</div><div style="float:left; margin-left:20px">
							<script type="text/javascript">
									function changeInCaricoAP()
									{';
									
									
									foreach ($impiegati_ezp as $impezp) {
										echo '
										
										firstVar = document.getElementById("in_carico_a_p").value;
										
										if (document.getElementById("in_carico_a_p").value == "'.$impezp['id_employee'].'") {
										
										
										document.getElementById(\'conoscenza_'.$impezp['id_employee'].'\').disabled = true;
										document.getElementById(\'conoscenza_'.$impezp['id_employee'].'\').checked = false;
										
										} else { document.getElementById(\'conoscenza_'.$impezp['id_employee'].'\').disabled = false; }
									';
									}
								echo '
									}
									</script>
								In carico a';
						echo '
					
					<select style="width:250px" name="in_carico_a_p" id="in_carico_a_p" onChange = "changeInCaricoAP();">';
					

					foreach ($impiegati_ezp as $impezp) {
					
						if(isset($_GET['aprinuovopreventivo'])) {
							echo "<option value='".$impezp['id_employee']."' ".($cookie->id_employee == $impezp['id_employee'] ? 'selected="selected"' : '')." >".$impezp['firstname']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>";
						}
						
						else {
						
							echo "<option value='".$impezp['id_employee']."' ".($preventivo['id_employee'] == $impezp['id_employee'] ? 'selected="selected"' : '')." >".$impezp['firstname']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>";
						}
					}

					echo '</select><br /><br /></td>
								</tr>
							<tr><td>Conoscenza</td>
								<td>
								';
								$impez_count = 0;
								foreach ($impiegati_ezp as $impez) {
			
									$impez_count++;
									if($impez_count == 10)
										echo '<br />';
									
									echo "<input type='checkbox' name='conoscenzap[]' id='conoscenza_".$impez['id_employee']."'value='".$impez['id_employee']."' /> ".$impez['firstname']." &nbsp;&nbsp;&nbsp;&nbsp;";
								
								}
								echo '<br /></td>
								</tr>
								<tr><td><br /></td></tr>
							<tr><td>Allega file</td>
							<td>
							<input name="MAX_FILE_SIZE" value="20000000" type="hidden">
							<script type="text/javascript" src="jquery.MultiFile.js"></script>
							<input name="joinFile[]" class="multi" id="joinFile" type="file" onchange="handleFileSelect(this.files)">
								<output id="list-tkt">Anteprime: </output>
									<script type="text/javascript">
								function handleFileSelect(files) {
								
									// Loop through the FileList and render image files as thumbnails.
									 for (var i = 0; i < files.length; i++) {
										var f = files[i];
										var name = files[i].name;
			  
										var reader = new FileReader();  
										reader.onload = function(e) {  
										  // Render thumbnail.
										  var span = document.createElement("span");
										  span.innerHTML = ["<a href=\'",e.target.result,"\' target=\'_blank\'>",name,"</a> - "].join("");
										  document.getElementById("list-tkt").insertBefore(span, null);
										};

									  // Read in the image file as a data URL.
									  reader.readAsDataURL(f);
									}
								  }
								 
								</script>
							</td>
							</tr>';
							
							echo '
								<tr><td>Link prodotto</td><td><link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
								<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete3.js"></script>
								<div id="ajax_choose_product" style="padding:6px; padding-top:2px; width:800px;">
									
									<input type="checkbox" checked="checked" id="prodotti_online" onclick="if(document.getElementById(\'product_autocomplete_input_messaggio\').value != \'\') { clearAutoComplete(); }" />Online &nbsp;&nbsp;
				<input type="checkbox" id="prodotti_offline" onclick="if(document.getElementById(\'product_autocomplete_input_preventivo\').value != \'\') { clearAutoComplete(); }" />Offline &nbsp;
	<input type="checkbox" id="prodotti_old" onclick="if(document.getElementById(\'product_autocomplete_input_preventivo\').value != \'\') { clearAutoComplete(); }" />Old &nbsp;&nbsp;
				<input type="checkbox" id="prodotti_disponibili" onclick="if(document.getElementById(\'product_autocomplete_input_preventivo\').value != \'\') { clearAutoComplete(); }" />Solo disp. &nbsp;&nbsp;&nbsp;
				
				<script type="text/javascript" src="../js/select2.js"></script>
						<script type="text/javascript">
						$(document).ready(function() { $("#auto_marca").select2(); $("#auto_serie").select2(); $("#auto_fornitore").select2();  $("#auto_categoria").select2(); });
						</script>
						
				<select id="auto_marca" name="auto_marca" onchange="repopulateSeries(this.value); clearAutoComplete();" style="width:100px">
				<option value="0">Marca...</option>
				';
				$marche = Db::getInstance()->executeS('SELECT * FROM manufacturer ORDER BY name ASC');
				foreach($marche as $marca)
					echo '<option value="'.$marca['id_manufacturer'].'">'.$marca['name'].'</option>';
				echo '
				</select>
				
						
				<select id="auto_serie" name="auto_serie" onchange="clearAutoComplete();" style="width:100px">
				<option value="0">Serie...</option>
				<option value="0">Scegli prima un costruttore</option>
				';
				echo '
				</select>
				
				<select id="auto_categoria" name="auto_categoria" onchange="clearAutoComplete();" style="width:100px">
				<option value="0">Categoria...</option>
				';
				$categorie = Db::getInstance()->executeS('SELECT * FROM category c JOIN category_lang cl ON c.id_category = cl.id_category WHERE cl.id_lang = 5 AND c.id_parent = 1 ORDER BY cl.name ASC');
				foreach($categorie as $categoria)
					echo '<option value="'.$categoria['id_category'].'">'.$categoria['name'].'</option>';
				echo '
				</select>
				
				<select id="auto_fornitore" name="auto_fornitore" onchange="clearAutoComplete(); " style="width:100px">
				<option value="0">Fornitore...</option>
				';
				
				$fornitori = Db::getInstance()->executeS('SELECT * FROM supplier WHERE id_supplier = 8 OR id_supplier = 11 OR id_supplier = 42 ORDER BY name ASC');
				foreach($fornitori as $fornitore)
					echo '<option value="'.$fornitore['id_supplier'].'">'.$fornitore['name'].'</option>';
				echo '
				</select>
				
				<input id="reset" type="button" value="Reset" class="button" onclick="document.getElementById(\'prodotti_online\').checked = true; document.getElementById(\'prodotti_offline\').checked = true; document.getElementById(\'prodotti_old\').checked = true; document.getElementById(\'prodotti_disponibili\').checked = false; document.getElementById(\'auto_categoria\').options[0].selected = true; $(\'#auto_categoria\').select2();  document.getElementById(\'auto_marca\').options[0].selected = true; $(\'#auto_marca\').select2();  
				document.getElementById(\'auto_serie\').options[0].selected = true; $(\'#auto_serie\').select2();  
				document.getElementById(\'auto_fornitore\').options[0].selected = true; $(\'#auto_fornitore\').select2(); $(\'#prodlist\').hide(); $(\'div.autocomplete_list\').hide();" />
				
				<br />
				 <input size="123" type="text" value="" id="product_autocomplete_input_preventivo" /><input id="veditutti2" type="button" value="Cerca" class="button" onclick=\'$("#veditutti").trigger("click");\' /><input id="veditutti" type="button" value="Cerca" class="button" style="display:none" /><script type="text/javascript">
						var formProduct = "";
						var products = new Array();
					</script>
					<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
					
					
					<script type="text/javascript">
					
						function addProduct_TR(event, data, formatted)
						{
							document.getElementById("product_autocomplete_input_preventivo").value = ""; 
							var link_rewrite = data[31];	
							var productName = data[5];
							var productRef = data[36];
							
							tinyMCE.get(\'messaggiop\').focus();
							var $body = $(tinymce.activeEditor.getBody());
							$body.prepend(\'<p>\');
							$body.append(\'</p>\');
							$body.find("p:last").append(productName + \' (\'+ productRef +\') <br /><a href="\' + link_rewrite + \'">\' + link_rewrite + \'<\/a><br /><br />\');
							$("body").trigger("click");
							$("#product_autocomplete_input_preventivo").trigger("click");
							$("#product_autocomplete_input_preventivo").trigger("click");
							
						}	
						
						
	 
						urlToCall = null;
						
						function getExcludeIds() {
							var ids = "";
							ids += $(\'#excludeIds\').val().replace(/\\-/g,\',\').replace(/\\,$/,\'\');
							return ids;
						}
						</script>
						<script type="text/javascript">
								
									
									urlToCall = null;
									/* function autocomplete */
									
									function getOnline()
						{
							if(document.getElementById("prodotti_online").checked == true)
								return 1;
							else
								return 0;
						}
						
						function getOffline()
						{
							if(document.getElementById("prodotti_offline").checked == true)
								return 1;
							else
								return 0;
						}

	function getOld()
						{
							if(document.getElementById("prodotti_old").checked == true)
								return 1;
							else
								return 0;
						}
						
						function getDisponibili()
						{
							if(document.getElementById("prodotti_disponibili").checked == true)
								return 1;
							else
								return 0;
						}
						
						function getCopiaDa()
						{
								return 0;
						}
						
						function getSerie()
						{
							if(document.getElementById("auto_serie").value > 0)
								return document.getElementById("auto_serie").value;
							else
								return 0;
						}
						
						function getMarca()
						{
							if(document.getElementById("auto_marca").value > 0)
								return document.getElementById("auto_marca").value;
							else
								return 0;
						}
						
						function getFornitore()
						{
							if(document.getElementById("auto_fornitore").value > 0)
								return document.getElementById("auto_fornitore").value;
							else
								return 0;
						}
						
						function getCategoria()
						{
							if(document.getElementById("auto_categoria").value > 0)
								return document.getElementById("auto_categoria").value;
							else
								return 0;
						}
						
						function repopulateSeries(id_manufacturer)
						{
							$("#auto_serie").empty();
							$.ajax({
							  url:"ajax.php?repopulate_series=y",
							  type: "POST",
							  data: { id_manufacturer: id_manufacturer
							  },
							  success:function(resp){  
								var newOptions = $.parseJSON(resp);
								
								 $("#auto_serie").append($("<option></option>")
									 .attr("value", "0").text("Serie..."));
								
								$.each(newOptions, function(key,value) {
								  $("#auto_serie").append($("<option></option>")
									 .attr("value", value).text(key));
								});
								
								$("#auto_serie").select2({
									placeholder: "Serie..."
								});
							  },
							  error: function(xhr,stato,errori){
								 alert("Errore: impossibile trovare serie ");
							  }
							});
								
							
						}	
									
								function clearAutoComplete()
						{
							$("#veditutti").trigger("click");
							
						}		
									
									$(function() {
							$(\'#product_autocomplete_input_preventivo\')
								.autocomplete(\'ajax_products_list2.php?products_and_bundles=y&id_customer='.Tools::getValue('id_customer').'\', {
									minChars: 0,
									autoFill: false,
									max:500,
									matchContains: true,
									matchSubset: 0,
									mustMatch:true,
									scroll:true,
									cacheLength:1,
									formatItem: function(item) {
								
												var color = "";
												if(item[24] == 1) {
													var color = "; color:red";
												}
												if(item[28] == 0) {
													return \'</table><div style="position:absolute; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:150px;float:left;margin-left:10px;">Codice</div></th><th style="width:570px;"><div style="width:350px; float:left;">Desc. prodotto<br /><br /></div></th><th style="width:60px; text-align:right"><div style="width:60px;float:left;">Prezzo</div></th><th style="width:50px; text-align:right"><div style="width:60px;float:left;">Qt. mag.</div></th><th style="width:50px; text-align:right"><div style="width:75px;float:left;">Qt. tot</div></th></tr></div><br />\'
													
													;
												}
												else {
													return \'<tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:350px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
													
													;
													}
												}
									
								}).result(addProduct_TR);
							
						});
					
						$("#product_autocomplete_input_preventivo").css(\'width\',\'673px\');
						$("#product_autocomplete_input_preventivo").keypress(function(event){
									
									  var keycode = (event.keyCode ? event.keyCode : event.which);
									  if (keycode == "13") {
										event.preventDefault();
										event.stopPropagation();    
									  }
									});
									
						
					</script><script type="text/javascript">
					
					$("#veditutti2").click ( function (zEvent) {
						
						/*	$("body").trigger("click");
							$("body").trigger("click");
							
							
							
							$("#product_autocomplete_input_preventivo").focus();
						//	$("#product_autocomplete_input_preventivo").val("");
						//	$("#product_autocomplete_input_preventivo").val(" ");
						*/
							var press = jQuery.Event("keypress");
							press.which = 13;
							if(document.getElementById(\'product_autocomplete_input_preventivo\').value == "")
							{
								$("#product_autocomplete_input_preventivo").val(" ");
							}
							$("#product_autocomplete_input_preventivo").trigger(press);
						
							$("#product_autocomplete_input_preventivo").trigger("click");
							$("#product_autocomplete_input_preventivo").trigger("click");
						//	$("#product_autocomplete_input_preventivo").val("");
							
					} );


					</script></td></tr>';
					
					echo '
					<tr><td><script type="text/javascript" src="../js/select2.js"></script>
						<script type="text/javascript">
						$(document).ready(function() { $("#utils_autocomplete_input_preventivo").select2(); $("#messaggi_precompilati").select2(); });
					
					function addUtil_TR(event, data, formatted)
						{
							tinyMCE.get(\'messaggiop\').focus();
							var $body = $(tinymce.activeEditor.getBody());
							$body.prepend(\'<p>\');
							$body.append(\'</p>\');
							if(document.getElementById("utils_autocomplete_input_preventivo").value != "-- Seleziona un link utile --")
							{
								$body.find("p:last").append(\'<a href="\' + document.getElementById("utils_autocomplete_input_preventivo").value + \'">\' + document.getElementById("utils_autocomplete_input_preventivo").value + \'<\/a><br /><br />\');
							}
							
						}	</script>
					Link utili</td><td><select name="utils_autocomplete_input_preventivo" id="utils_autocomplete_input_preventivo" style="width:730px" onchange="addUtil_TR();">
					'.$utils_options.'</select></td></tr>';
					
				echo '<tr><td>Messaggi precompilati</td><td><select name="messaggi_precompilati" id="messaggi_precompilati" style="width:730px" onchange="inserisciPrecompilato(this.value);">
					';
					$precompilati = Db::getInstance()->executeS('SELECT * FROM precompilato WHERE active = 1');
					echo '<option value="-- Seleziona un messaggio --">-- Seleziona un messaggio --</option>';
					foreach($precompilati as $precompilato)
					{
						echo '<option value="'.htmlentities($precompilato['testo'], ENT_QUOTES).'">'.htmlentities($precompilato['oggetto'], ENT_QUOTES).'</option>';
					}
					echo '
					</select>
					<script type="text/javascript">
					function inserisciPrecompilato(msg)
					{
						tinyMCE.get(\'messaggiop\').focus();
						var $body = $(tinymce.activeEditor.getBody());
						$body.html(\'\');
						$body.prepend(\'<p>\');
						$body.append(\'</p>\');
						if(msg != "-- Seleziona un messaggio --") {
							$body.find("p:last").append(msg);
						}
					}
					</script>
					</td></tr>
					';

					echo '<tr><td>Messaggio</td>
							<td>
					
							<textarea style="width:760px" class="rte" id="messaggiop" name="messaggiop" rows="5" cols="40">
							'.(isset($_POST['modifica_messaggio_submit']) ? stripslashes($_POST['modifica_messaggio_preventivo']) : '').'
							</textarea>
							
							
							</td>
							</tr>';
							
							echo'			<tr><td><strong style="color:red">Note private visibili solo da staff</strong></td>
								<td> 
								<br />';
								echo '<span class="button" onclick="$(\'#mostra_nota_privata\').slideToggle(); " style="cursor: pointer; display:block; color:red; font-size:14px; font-weight:bold; width:752px; border:1px solid #cccccc; text-align:center; background-color:#f0f0ee; text-decoration:none"><img src="../img/admin/arrow.gif" alt="Dettaglio note private" title="Dettaglio note private" style="float:left; margin-right:5px;"/>Clicca qui per inserire note private/riservate - Non visibili all\'utente.</span>';
				
				
				
								echo '<div id="mostra_nota_privata" style="display:none; margin-top:0px">';
								echo '
								
								<textarea id="note_private" class="rte" name="note_private" rows="15" cols="20" style="width:760px;height:130px">
							
								
								
								
								</textarea></div>
								
								
								
								
								</td>
								</tr>';
								
							/*
							if(isset($_GET['aprinuovopreventivo'])) {
								
								echo '<tr>
								<td>Nota interna (solo per impiegati)</td>
								<td><textarea id="note" name="note" rows="5" cols="145"></textarea>
								</tr>';
								
							}*/
							// onclick="submitFormOkay = true; this.style.visibility=\'hidden\', loading.style.visibility=\'visible\'"
							
							$riferimento = Tools::getValue('riferimento') ? Tools::getValue('riferimento') : Db::getInstance()->getValue('SELECT riferimento FROM form_prevendita_thread WHERE id_thread = '.$thread['id_thread']);
							$statusPadre = Customer::statusPadre($riferimento);
							echo '<tr><td colspan="2">'.($riferimento != '' && $statusPadre != 'closed' && $statusPadre != '' ? '<div style="border:3px solid red; padding:5px;"><input type="checkbox" name="chiudi_padre" />&nbsp;&nbsp;&nbsp;<strong style="color:red">ATTENZIONE!</strong>  L\'azione padre risulta ancora aperta. Spunta questa casella se vuoi chiuderla.</div>' : '').'</td></tr>';
							
								
							echo '<tr><td></td>
							<td>
								<button type="submit" class="button"  name="submitPreventivoReply" id="submitPreventivoReply" value="Modifica">
								<img src="../img/admin/email.gif" alt="Invia" title="Invia" />&nbsp;&nbsp;&nbsp;Invia
								</button>
							
							</td>
				
							</tr>
						</table>
				
					</fieldset>
				</form>';
				
				if(isset($_GET['id_thread'])) {
				echo "</div>";
				} else { }
				
				if(isset($_GET['id_thread'])) {
					echo '<div class="yetii-preventivi" id="preventivi-3">';
				
				$first = Customer::HierarchyFirst($preventivo['id_thread'], 'P');
				
				echo '<div class="tree">';
				echo '<ul class="tree-menu"><li>'.Customer::trovaSiglaLinkPerAlbero(substr($first,1), substr($first,0,1),'').'';
				echo str_replace('<ul></ul>','',Customer::HierarchyFindChildren($first, $children, 'P'.$preventivo['id_thread']));
				echo '</li></ul>';
				echo '</div>';
				echo '<div style="clear:both"></div><br /><br />';
				
					echo '</div>'; // chiudo preventivi-3
				}	
				
				echo '<div class="yetii-preventivi" id="preventivi-4">';
			
				$storico_ticket = Db::getInstance()->executeS('SELECT * FROM storico_attivita WHERE id_attivita = '.$thread['id_thread'].' AND tipo_attivita = "P" ORDER BY data_attivita DESC, desc_attivita DESC');
				
				if(sizeof($storico_ticket) > 0)
				{
					echo '<table class="table">';
					echo '<thead><tr><th>Persona</th><th>Azione</th><th>Data</th></tr></thead><tbody>';
					
					foreach($storico_ticket as $storico)
					{
						if(is_numeric(substr($storico['desc_attivita'], -1)))
						{	
							$employee = filter_var($storico['desc_attivita'], FILTER_SANITIZE_NUMBER_INT);
							$name_employee = Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM employee WHERE id_employee = '.$employee);
							$desc_attivita = 'Ha messo l\'attivit&agrave; in carico a '.$name_employee;
						}
						else
							$desc_attivita = $storico['desc_attivita'];
						
						echo '<tr><td>'.($storico['id_employee'] == 0 ? 'Cliente' : Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM employee WHERE id_employee = '.$storico['id_employee'])).'</td><td>'.$desc_attivita.'</td><td>'.Tools::displayDate($storico['data_attivita'],$cookie->id_lang, true).'</td></tr>'; 
						
					}	
					
					echo '</tbody></table><br /><br />';
				}
					echo '</div>'; // chiudo preventivi-4
			
				
				if(isset($_GET['id_thread'])) {
				echo "</div>";
				} else { }
				
				
			}
			
			}
			
				
			}
			
			
			
			echo '<br /><br /><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=7" class="button"><img src="../img/admin/arrow-left.png" alt="Back" title="Back" />&nbsp;&nbsp;&nbsp;Torna alla lista delle azioni cliente</a>';
			echo '</div>';
			}
		}
		else { }
		
		// FINE PREVENTIVI!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
		// INIZIO STATISTICHE
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 8) {
			if($cookie->profile == 7)
				echo 'Non hai i permessi per visualizzare questa sezione';
			else
			{
				echo '
				<div class="tab1" id="stats">';
				echo '<h2>'.$this->l('Dati statistici cliente').'</h2>
				<table cellspacing="0" cellpadding="0" class="table">
				<tr>
				<th>Dato</th>
				<th>Valore</th>
				</tr>
				';
				
				if($customer->is_company == 1) {
				
				echo "<tr><td><strong>Numero dipendenti</strong></td>
				<td>".$customer->employees_number."</td>
				</tr>";
				}
				else {
				}
				
				$interesse = $customer->getInteresse();
				
				foreach($interesse as $intx) {
				echo "<tr><td><strong>Interesse</strong></td><td>";
				echo Category::getCatNameById($intx['id_category_default'], 5);
				echo "</td></tr>";
				}
				
				$primoacquisto = $customer->ultimoAcquistoPrimoAcquisto("primo");
				
				foreach($primoacquisto as $primoprod) {
				echo "<tr><td><strong>Primo acquisto</strong></td><td>";
				echo $primoprod['product_name'].", ".$primoprod['product_quantity']." pz.";
				echo "</td></tr>";
				}
				
				$ultimoacquisto = $customer->ultimoAcquistoPrimoAcquisto("ultimo");
				
				foreach($ultimoacquisto as $ultimoprod) {
				echo "<tr><td><strong>Ultimo acquisto</strong></td><td>";
				echo $ultimoprod['product_name'].", ".$ultimoprod['product_quantity']." pz.";
				echo "</td></tr>";
				}	
						
				
				
				echo "</table><br /><br />";
				echo '<h2>Totale acquisti</h2>';
				
				$acquistiperanno = $customer->getOrdersByYear();
				
				if(count($acquistiperanno) == 0) {
					echo "Questo cliente non ha mai fatto acquisti su Ezdirect.";
				}
				else {
					echo '<table cellspacing="0" cellpadding="0" class="table" style="text-align:right">';
					echo '<tr><th>Anno</th><th>Totale pagato</th><th>Totale senza iva</th></tr>';
					foreach($acquistiperanno as $acquisti) {
						echo '<tr><td><strong>'.$acquisti['anno'].'</strong></td>
						<td>'.Tools::displayPrice($acquisti['totale'], new Currency($defaultCurrency)).'</td>
						<td>'.Tools::displayPrice($acquisti['totale_senza_iva'], new Currency($defaultCurrency)).'</td>
						</tr>';
					}
					foreach ($orders AS $order)
					if ($order['valid']) {
						$totalOKconiva += $order['total_paid_real'];
						$totalOKsenzaiva +=  $order['total_products'];
					}

					echo '<tr><td><strong>Totale</strong></td>
						<td>'.Tools::displayPrice($totalOKconiva, new Currency($defaultCurrency)).'</td>
						<td>'.Tools::displayPrice($totalOKsenzaiva, new Currency($defaultCurrency)).'</td>
						</tr>';
					
					echo '</table>';
				}
				
			
				
				echo "<br /><br />";
				
					
					echo '<h2>'.$this->l('Prodotti ordinati').' ('.sizeof($products).')</h2>
					<table cellspacing="0" cellpadding="0" class="table">
						<tr>
							<th>'.$this->l('Date').'
							<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=dateasc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/up.gif" alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a>
							
							<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=datedesc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/down.gif" alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>
							
							</th>
							<th>'.$this->l('Name').'
							<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=nameasc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/up.gif" alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a>

							<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=namedesc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/down.gif" alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>
							
							</th>
							<th>Marca
							
							<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=manufacturerasc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/up.gif" alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a>
							<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=manufacturerdesc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/down.gif" alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>
							</th>
							<th>Categoria
							<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=categoryasc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/up.gif" alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a>
							<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=categorydesc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/down.gif" alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>
							</th>
							<th>'.$this->l('Qt.').'
							<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=quantityasc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/up.gif" alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a>
							<a href="index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&sort=quantitydesc&token='.Tools::getValue('token').'&tab-container-1=8"><img src="../img/admin/down.gif" alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>
							</th>
							<th>Totale di sempre</th>
							<th>'.$this->l('Actions').'</th>
						</tr>';
					$tokenOrders = Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).(int)($cookie->id_employee));

					if(isset($_GET['sort']) && $_GET['sort'] == "datedesc") {
					$products = $customer->getBoughtProducts2("o.date_add DESC");
					}
					if(isset($_GET['sort']) && $_GET['sort'] == "dateasc") {
					$products = $customer->getBoughtProducts2("o.date_add ASC");
					}
					if(isset($_GET['sort']) && $_GET['sort'] == "nameasc") {
					$products = $customer->getBoughtProducts2("od.product_name ASC");
					}			
					if(isset($_GET['sort']) && $_GET['sort'] == "namedesc") {
					$products = $customer->getBoughtProducts2("od.product_name DESC");
					}	
					if(isset($_GET['sort']) && $_GET['sort'] == "manufacturerdesc") {
					$products = $customer->getBoughtProducts2("m.name DESC");
					}			
					if(isset($_GET['sort']) && $_GET['sort'] == "manufacturerasc") {
					$products = $customer->getBoughtProducts2("m.name ASC");
					}				
					if(isset($_GET['sort']) && $_GET['sort'] == "categoryasc") {
					$products = $customer->getBoughtProducts2("cl.name ASC");
					}			
					if(isset($_GET['sort']) && $_GET['sort'] == "categorydesc") {
					$products = $customer->getBoughtProducts2("cl.name DESC");
					}				
					if(isset($_GET['sort']) && $_GET['sort'] == "quantityasc") {
					$products = $customer->getBoughtProducts2("od.product_quantity ASC");
					}	
					if(isset($_GET['sort']) && $_GET['sort'] == "quantitydesc") {
					$products = $customer->getBoughtProducts2("od.product_quantity DESC");
					}
					
					foreach ($products AS $product) {
					
					echo '
						<tr '.($irow++ % 2 ? 'class="alt_row"' : '').' style="cursor: pointer" onclick="window.open(\'?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&id_order='.$product['id_order'].'&vieworder&token='.$this->token.'&tab-container-1=4\', \'blank\', \'height=600,width=1200,modal=yes,alwaysRaised=yes,scrollbars=yes\');">
							<td>'.Tools::displayDate($product['date_add'], (int)($cookie->id_lang), false).'</td>
							<td>'.$product['product_name'].'</td>
							<td>'.Manufacturer::getNameById($product['id_manufacturer']).'</td>
							<td>'.Category::getCatNameById($product['id_category_default'], 5).'</td>
							<td align="right">'.$product['product_quantity'].'</td>
							<td align="right">'.Customer::getTotalBoughtProducts($product['product_id'], $_GET['id_customer']).'</td>
							<td align="center"><img src="../img/admin/details.gif" /></td>
						</tr>';
						}
					echo '
					</table>';

				
				$interested = Db::getInstance()->ExecuteS('SELECT DISTINCT * FROM '._DB_PREFIX_.'cart_product cp INNER JOIN '._DB_PREFIX_.'cart c on c.id_cart = cp.id_cart WHERE c.id_customer = '.(int)$customer->id.' ');
				
				/* //per escludere prodotti ordinati
				$interested = Db::getInstance()->ExecuteS('SELECT DISTINCT id_product FROM '._DB_PREFIX_.'cart_product cp INNER JOIN '._DB_PREFIX_.'cart c on c.id_cart = cp.id_cart WHERE c.id_customer = '.(int)$customer->id.' AND cp.id_product NOT IN (
				SELECT product_id FROM '._DB_PREFIX_.'orders o inner join '._DB_PREFIX_.'order_detail od ON o.id_order = od.id_order WHERE o.valid = 1 AND o.id_customer = '.(int)$customer->id.')'); */
				
				
				if (count($interested))
				{
					echo '
					<br /><h2>'.$this->l('Prodotti nel carrello').' ('.count($interested).')</h2>
					<table cellspacing="0" cellpadding="0" class="table">';
					echo "<tr><th>Aggiunto il</th><th>Id prodotto</th><th>Nome prodotto</th><th></th></tr>";
					
					foreach ($interested as $p)
					{
						$product = new Product((int)$p['id_product'], false, $cookie->id_lang);
						
						if($product->id != 0) {
						
						echo '
						<tr '.($irow++ % 2 ? 'class="alt_row"' : '').' style="cursor: pointer" onclick="javascript:window.open(\''.$link->getProductLink((int)$product->id, $product->link_rewrite, Category::getLinkRewrite($product->id_category_default, (int)($cookie->id_lang))).'\', \'blank\', \'height=600,width=1200,modal=yes,alwaysRaised=yes,scrollbars=yes\');">
							<td>'.date("d-m-Y",strtotime($p['date_add'])).'</td>
							<td>'.(int)$product->id.'</td>
							<td>'.Tools::htmlentitiesUTF8($product->name).'</td>
							<td align="center"><img src="../img/admin/details.gif" /></td>
						</tr>';
						
						}
						
						else {
						
						$resnmex = ("SELECT product_id, product_name FROM order_detail WHERE product_id = $p[id_product]");
						$rownmex = Db::getInstance()->getRow($resnmex);
						
						echo '
						<tr>
						<td>'.$p['date_add'].'</td>
							<td>'.$rownmex['product_id'].'</td>
							<td>'.Tools::htmlentitiesUTF8($rownmex['product_name']).' - <em>prodotto non pi&ugrave; esistente</em></td>
							<td align="center"></td>
						</tr>';
						
						
						
						}
						
						
						
					}
					echo '</table>';
				}
				

				/* Last connections */
				$connections = $customer->getLastConnections();
				
					echo '
				<h2>'.$this->l('Last connections').'</h2>';
				
			if (sizeof($connections))
				{
				echo '
					<table cellspacing="0" cellpadding="0" class="table">
						<tr>
							<th style="width: 200px">'.$this->l('Date').'</th>
							<th style="width: 100px">'.$this->l('Pages viewed').'</th>
							<th style="width: 100px">'.$this->l('Total time').'</th>
							<th style="width: 100px">'.$this->l('Origin').'</th>
							<th style="width: 100px">'.$this->l('IP Address').'</th>
						</tr>';
					foreach ($connections as $connection)
						echo '<tr>
								<td>'.Tools::displayDate($connection['date_add'], (int)($cookie->id_lang), true).'</td>
								<td>'.(int)($connection['pages']).'</td>
								<td>'.$connection['time'].'</td>
								<td>'.($connection['http_referer'] ? preg_replace('/^www./', '', parse_url($connection['http_referer'], PHP_URL_HOST)) : $this->l('Direct link')).'</td>
								<td>'.$connection['ipaddress'].'</td>
							</tr>';
					echo '</table>';
				}
				if (sizeof($referrers))
				{
					echo '<h2>'.$this->l('Referrers').'</h2>
					<table cellspacing="0" cellpadding="0" class="table">
						<tr>
							<th style="width: 200px">'.$this->l('Date').'</th>
							<th style="width: 200px">'.$this->l('Name').'</th>
						</tr>';
					foreach ($referrers as $referrer)
						echo '<tr>
								<td>'.Tools::displayDate($referrer['date_add'], (int)($cookie->id_lang), true).'</td>
								<td>'.$referrer['name'].'</td>
							</tr>';
					echo '</table>';
				}
				echo '</div>';
			}	
		} 
		// FINE STATISTICHE

		// INIZIO AZIONI!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 10) {
			
			if($cookie->profile == 7 && $agente_c != $cookie->id_employee)
				echo 'Non hai i permessi per visualizzare questa sezione';
			else
			{
				echo '<div class="tab1" id="azioni-customer">';
				echo '<h2>TO DO</h2>';
				$impiegati_eza = Db::getInstance()->ExecuteS("SELECT id_employee, firstname, lastname FROM employee WHERE active = 1 ORDER BY firstname ASC, lastname ASC");
				
				if(isset($_GET['id_action'])) {
				
						$azione = Db::getInstance()->getRow("SELECT * FROM action_thread at WHERE at.id_action = ".$_GET['id_action']." AND at.id_customer = ".$customer->id."");
						//$azione = Db::getInstance()->getRow("SELECT * FROM action_thread at WHERE at.id_action = ".$_GET['id_action']." AND at.id_customer = ".$customer->id." AND at.date_add BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL 2 MONTH) AND CURRENT_DATE()");
						
						if(!$azione && Tools::getValue('id_action') != '') {
							echo "<span style='color:red'>ERRORE - Azione non trovata</span><br /><br /><br />"; die();
						}
						
						$incaricato = new Employee($azione['action_to']);
					
						switch($azione['status']) {
							case 'open': $status_azione = "<img src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; $st_azione = 'Aperto'; break;
							case 'closed': $status_azione = "<img src='../img/admin/status_green.gif' alt='Chiuso' title='Chiuso' />"; $st_azione = 'Chiuso'; break;
							case 'pending1': $status_azione = "<img src='../img/admin/status_orange.gif' alt='In lavorazione' title='In lavorazione' />"; $st_azione = 'In lavorazione'; break;
							default: $status_azione = "<img src='../img/admin/status_red.gif' alt='Aperto' title='Aperto' />"; $st_azione = 'Aperto'; break;
						}
				
						$oggetto = Db::getInstance()->getValue("SELECT subject FROM action_thread WHERE id_action = ".$azione['id_action']." ORDER BY date_add ASC");
					
						if($oggetto == '') {
							$oggetto = Db::getInstance()->getValue("SELECT action_message FROM action_message FORCE INDEX (PRIMARY) WHERE id_action = ".$azione['id_action']." ORDER BY date_add ASC");
						}

						$dataultimo = Db::getInstance()->getValue("SELECT date_add FROM action_message WHERE id_action = ".$azione['id_action']." ORDER BY date_add DESC");
					
					if(isset($_GET['id_action'])) {
					
						echo '<h2 style="float:left; margin-right:10px; color:red">ATTIVIT&Agrave; INTERNA '.$azione['id_action'].' - In carico a: '.$incaricato->firstname.' - Status: '.$st_azione.'</h2><br />';
					}
				}
				
				
					echo '
				<div class="clear:both"></div>
				';
				
				/*
				if(isset($_GET['error']) && $_GET['error'] == '1ac') {
					
					echo '<div style="width:100%; height:30px; border:1px solid red; color:red"> ERRORE: il messaggio non deve essere vuoto.</div>';
				}
				*/
				
				if(isset($_POST['cambiastatusa_listing'])) {

						
					
					
					$subject = Db::getInstance()->getValue('SELECT subject FROM action_thread WHERE id_action = '.Tools::getValue('id_action'));
					$current_action_status = Db::getInstance()->getValue('SELECT status FROM action_thread WHERE id_action = '.Tools::getValue('id_action'));
		
					if(substr($subject,0,20) == '*** VERIFICA TECNICA')
					{	
						if(Tools::getValue('cambiastatusa_listing') == 'closed'  && $current_action_status != 'closed')
						{
							Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.substr($subject, -6,5).', 25, "'.date('Y-m-d H:i:s').'")');
							Product::RecuperaCSV(substr($subject, -6,5));
						}
						else if((Tools::getValue('cambiastatusa_listing') == 'open' || Tools::getValue('cambiastatusa_listing') == 'pending1')  && $current_action_status != 'closed')
						{
							Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.substr($subject, -6,5).', 24, "'.date('Y-m-d H:i:s').'")');
							Product::RecuperaCSV(substr($subject, -6,5));
						}
						
					}
					
					else if(substr($subject,0,10) == '*** RMA N.')
					{	
						if(Tools::getValue('cambiastatusa_listing') == 'closed'  && $current_action_status != 'closed')
						{
							Db::getInstance()->executeS('UPDATE rma SET test = 0 WHERE id_customer_thread = '.substr(Db::getInstance()->getValue('SELECT riferimento FROM action_thread WHERE id_action = '.Tools::getValue('id_action')),1));
						}
					}
					
					Db::getInstance()->ExecuteS("UPDATE action_thread SET status = '".Tools::getValue('cambiastatusa_listing')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_action = '".addslashes(Tools::getValue('id_action'))."'");
					
					
					switch(Tools::getValue('cambiastatusa_listing'))
					{
						case 'open': $switch_status = 3; break;
						case 'pending1': $switch_status = 4; break;
						case 'closed': $switch_status = 5; break;
						default: $switch_status = 10; break;
					}	
					
					Customer::Storico(Tools::getValue('id_action'), 'A', $cookie->id_employee, $switch_status);
					
					Customer::chiudiOrdineDaAzione(Tools::getValue('id_action'));
					
					Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&token='.$this->token.'&conf=4&tab-container-1=10');
					
				}
				
				if(isset($_POST['cambiaincaricoa'])) {

					Customer::cambiaIncaricoVeloce(Tools::getValue('tipo'), Tools::getValue('cambiaincaricoa'), Tools::getValue('id_thread'), Tools::getValue('precedente_incarico'), Tools::getValue('id_ticket_univoco'), Tools::getValue('id_customer'));
			
					Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&token='.$this->token.'&conf=4&tab-container-1=10');
					
				}
				
				
				if(isset($_POST['submitAzione'])) {

					$action_date_array = explode("-",Tools::getValue('action_date'));
					$action_date = $action_date_array[2]."-".$action_date_array[1]."-".$action_date_array[0]." ".Tools::getValue('action_hours').":".Tools::getValue('action_minutes').":00";
				
					
					if(strpos(Tools::getValue('action_date'),'0000') !== false) {
						$action_date = Db::getInstance()->getValue("SELECT date_action FROM action_thread WHERE id_action = ".Tools::getValue('id_action')."");
					}
					
					$precedente_status = Db::getInstance()->getValue("SELECT status FROM action_thread WHERE id_action = ".Tools::getValue('id_action')."");
					if($precedente_status != Tools::getValue('cambiastatusa'))
					{	
						switch(Tools::getValue('cambiastatusa'))
						{
							case 'open': $switch_status = 3; break;
							case 'pending1': $switch_status = 4; break;
							case 'closed': $switch_status = 5; break;
							default: $switch_status = 10; break;
						}	
						
						Customer::Storico(Tools::getValue('id_action'), 'A', $cookie->id_employee, $switch_status);
			
					}

					
					
					
					$subject = Db::getInstance()->getValue('SELECT subject FROM action_thread WHERE id_action = '.Tools::getValue('id_action'));
					
					if(substr($subject,0,20) == '*** VERIFICA TECNICA')
					{	
						
						$id_cart = Db::getInstance()->getValue('SELECT id_cart FROM orders WHERE id_order = '.substr($subject, -6,5));
						$note_private = Db::getInstance()->getValue("SELECT note_private FROM cart WHERE id_cart = ".$id_cart."");
						$note_private .= addslashes(Tools::getValue('messaggioa'));
						
						if((Tools::getValue('cambiastatusa') == 'closed' || Tools::getValue('statusa') == 'closed' )  && $current_action_status != 'closed')
						{
							Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.substr($subject, -6,5).', 25, "'.date('Y-m-d H:i:s').'")');
							Product::RecuperaCSV(substr($subject, -6,5));
							
						}
						else if((Tools::getValue('cambiastatusa') == 'open' || Tools::getValue('statusa') == 'open' || Tools::getValue('cambiastatusa') == 'pending1' || Tools::getValue('statusa') == 'pending1') && $current_action_status == 'closed')
						{
							Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.substr($subject, -6,5).', 24, "'.date('Y-m-d H:i:s').'")');
							Product::RecuperaCSV(substr($subject, -6,5));
							
						}
						
						Db::getInstance()->executeS('UPDATE cart SET note_private = "'.addslashes($note_private).'" WHERE id_cart = '.$id_cart);
						Db::getInstance()->executeS('UPDATE carrelli_creati SET note_private = "'.addslashes($note_private).'" WHERE id_cart = '.$id_cart);
					}
					else if(substr($subject,0,10) == '*** RMA N.')
					{	
						
						if(Tools::getValue('cambiastatusa') == 'closed'  && $current_action_status != 'closed')
						{
							Db::getInstance()->executeS('UPDATE rma SET test = 0 WHERE id_customer_thread = '.substr(Db::getInstance()->getValue('SELECT riferimento FROM action_thread WHERE id_action = '.Tools::getValue('id_action')),1));
						}
					}
								
					$current_action_status = Db::getInstance()->getValue('SELECT status FROM action_thread WHERE id_action = '.Tools::getValue('id_action'));
					
					if(substr($subject,0,20) == '*** VERIFICA TECNICA')
					{	
						if(Tools::getValue('cambiastatusa') == 'closed' && $current_action_status != 'closed')
						{
							Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.substr($subject, -6,5).', 25, "'.date('Y-m-d H:i:s').'")');
							Product::RecuperaCSV(substr($subject, -6,5));
						}
						else if((Tools::getValue('cambiastatusa') == 'open' || Tools::getValue('cambiastatusa') == 'pending1') && $current_action_status == 'closed')
						{
							Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.substr($subject, -6,5).', 24, "'.date('Y-m-d H:i:s').'")');
							Product::RecuperaCSV(substr($subject, -6,5));
						}
						
					}
					else if(substr($subject,0,10) == '*** RMA N.')
					{	
						if(Tools::getValue('cambiastatusa_listing') == 'closed'  && $current_action_status != 'closed')
						{
							Db::getInstance()->executeS('UPDATE rma SET test = 0 WHERE id_customer_thread = '.substr(Db::getInstance()->getValue('SELECT riferimento FROM action_thread WHERE id_action = '.Tools::getValue('id_action')),1));
						}
					}
					Db::getInstance()->ExecuteS("UPDATE action_thread SET subject = '".addslashes(Tools::getValue('azione_subject'))."', action_type = '".addslashes(Tools::getValue('action_type'))."', action_date = '".$action_date."', status = '".Tools::getValue('cambiastatusa')."', note = '".addslashes(Tools::getValue('action_note'))."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_action = '".addslashes(Tools::getValue('id_action'))."'");
					
					foreach ($_POST['note_nuova'] as $id_nota=>$nota_id) {
						
						echo $_POST['note_nota'][$id_nota].'<br />';
					
						if($_POST['note_nuova'][$id_nota] == 0) {
							
							$testo_nota = Db::getInstance()->getValue('SELECT note FROM note_attivita WHERE id_note = '.$id_nota.'');
							if($testo_nota != $_POST['note_nota'][$id_nota])
								Db::getInstance()->executeS("UPDATE note_attivita SET note = '".addslashes($_POST['note_nota'][$id_nota])."', date_upd = '".date("Y-m-d H:i:s")."', id_employee = '".$cookie->id_employee."' WHERE id_note = ".$id_nota.""); 
						}
						
						else {
							if(addslashes($_POST['note_nota'][$id_nota]) != '')
							{
								Db::getInstance()->executeS("INSERT INTO note_attivita
								(id_note,
								id_attivita,
								tipo_attivita,
								id_employee,
								note,
								date_add,
								date_upd)
								
								VALUES (
								NULL,
								'".Tools::getValue('id_action')."',
								'A',
								'".$cookie->id_employee."',
								'".addslashes($_POST['note_nota'][$id_nota])."',
								'".date("Y-m-d H:i:s")."',
								'".date("Y-m-d H:i:s")."'
								)");
							}
							
						}
					}
					
					foreach ($_POST['promemoria_nuovo'] as $id_promemoria=>$promemoria_id) {
								Db::getInstance()->execute('INSERT INTO ticket_promemoria (msg, tipo_att, ora, conto, tipo, status) VALUES ('.Tools::getValue('id_action').', "a", "'.$action_date.'" - INTERVAL '.$_POST['action_minuti'][$id_promemoria].' '.$_POST['action_tipo_promemoria'][$id_promemoria] .', "'.$_POST['action_minuti'][$id_promemoria].'", "'.$_POST['action_tipo_promemoria'][$id_promemoria].'", "open")');
							}
							
					Customer::chiudiOrdineDaAzione(Tools::getValue('id_action'));
					
					$impiegato_attuale = Db::getInstance()->getValue("SELECT action_to FROM action_thread WHERE id_action = '".Tools::getValue('id_action')."'");
					
					if($impiegato_attuale != Tools::getValue('assegnaimpiegatoa')) {
							
						Customer::Storico(Tools::getValue('id_action'), 'A', $cookie->id_employee, 2, Tools::getValue('assegnaimpiegatoa'));
						
						Db::getInstance()->ExecuteS("UPDATE action_thread SET action_to = '".Tools::getValue('assegnaimpiegatoa')."' WHERE id_action = '".Tools::getValue('id_action')."'");
										
						$mailimp = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".Tools::getValue('assegnaimpiegatoa')."'");
										
						$tokenimp = Tools::getAdminToken("AdminCustomers"."2".Tools::getValue('assegnaimpiegatoa'));
										
						$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewaction&id_action=".Tools::getValue('id_action')."&token=".$tokenimp.'&tab-container-1=10';
			
						$msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato l'azione numero ".Tools::getValue('id_action')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirla</a>.";
										
						if($impiegato_attuale != 0) {
											
							$mailimprev = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = '".$impiegato_attuale."'");
							$nomeimprev = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = '".$impiegato_attuale."'");
											
							$msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione dell'azione numero ".Tools::getValue('id_action')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect e l'ha passata a ".Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.Tools::getValue('assegnaimpiegatoa').'');
											
							$params = array(
							'{link}' => $linkimp,
							'{firma}' => '',
							'{msg}' => $msgimprev);
							
							if($impiegato_attuale != $cookie->id_employee)
							{			
								Mail::Send(5, 'msg_base', Mail::l('Gestione azione revocata', 5), 
									$params, $mailimprev, NULL, NULL, NULL, NULL, NULL, 
								_PS_MAIL_DIR_, true);
							}		
						}
										
						else {
										
						}
										
						$params = array(
						'{link}' => $linkimp,
						'{firma}' => '',
						'{msg}' => $msgimp);
										
						if($_POST['assegnaimpiegatoa'] != 0 && $_POST['assegnaimpiegatoa'] != $cookie->id_employee) {
										
							Mail::Send(5, 'msg_base', Mail::l('Azione assegnata a te su Ezdirect', 5), 
								$params, $mailimp, NULL, NULL, NULL, NULL, NULL, 
								_PS_MAIL_DIR_, true);
														
						} else { }
					}

					else {
					}
					
					Tools::redirectAdmin($currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&viewaction&id_action='.Tools::getValue('id_action').'&token='.$this->token.'&conf=4&tab-container-1=10');
					
				}
						
				

				if(!isset($_GET['viewaction'])) {
				
					echo Customer::BuildListing($customer->id, 'to-do');
					
				}
				
				
				else {
					
					/*$fta = Db::getInstance()->getValue('SELECT id_attivita FROM storico_attivita WHERE tipo_num = 1 AND id_attivita = '.Tools::getValue('id_action'));
					if((!$fta || $fta <= 0) && $_GET['id_action'] > 12210)*/
					if(!Tools::getIsset('conf') && !Tools::getIsset('filename'))
						Customer::Storico(Tools::getValue('id_action'), 'A', $cookie->id_employee, 14);
				
					if (isset($_GET['filename'])) {
								$filename = $_GET['filename'];
								
								if(strpos($filename, ":::")) {
					
									$parti = explode(":::", $filename);
									$nomecodificato = $parti[0];
									$nomevero = $parti[1];
									
								}
					
								else {
									$nomecodificato = $filename;
									$nomevero = $filename;
					
								}
								
								if (file_exists(_PS_UPLOAD_DIR_.$nomecodificato)) {
									self::openUploadedFile();
								}
								else { }
							} 
							else { }
					
			
					if(isset($_GET['id_action'])) {
					
					
						echo '<div id="tab-container-azioni" class="tab-containers">
						
						
						
						<ul id="tab-container-azioni-nav" class="tab-containers-nav">
						<li><a href="#azioni-1"><strong>Generale</strong></a></li>
						<li><a href="#azioni-2"><strong>Cronologia</strong></a></li>
						<li><a href="#azioni-3"><strong>Gerarchia</strong></a></li>
						<li><a href="#azioni-4"><strong>Storico</strong></a></li>
						</ul>';
					}
					
					if(isset($_GET['id_action'])) {
						echo '
						
						<div class="yetii-azioni" id="azioni-1">
						<fieldset style="width:95%; background-color:#ffe8d4">';
						
						
						
						echo '
						<form id="modificaazione" action="'.$currentIndex.'&id_customer='.$_GET['id_customer'].'&submitAzione&viewcustomer&token='.$this->token.'&tab-container-1=10'.(Tools::getIsset('riferimento') ? '&riferimentoo='.Tools::getValue('riferimento') : '').'" method="post" autocomplete="off">';
						
						
						
						
						
						echo '<div class="anagrafica-cliente" style="width:800px; margin-bottom:10px">
						<strong>Oggetto</strong><br />
						
							<textarea name="azione_subject" id="azione_subjectContent" style="width:690px;height:40px;display:block;float:left" '.(substr($oggetto,0,20) == '*** VERIFICA TECNICA' ? 'readonly="readonly"' : '').'>'.strip_tags(htmlspecialchars_decode($oggetto)).'</textarea>
							
							
						<br />
						</div>

						
						<div class="clear"></div>
						<div class="anagrafica-cliente" style="width:103px">
						ID Azione<br />
						<span class="tab-span" style="width:103px">'.$azione['id_action'].'</span><br />
						</div>';
						
						$aperto_da = Db::getInstance()->getValue('SELECT action_m_from FROM action_message WHERE id_action = '.$azione['id_action'].' ORDER BY id_action_message ASC');
						$aperto_da = Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$aperto_da);
						echo '
						
						<div class="anagrafica-cliente" style="width:97px">
						Aperto da<br />
						<span class="tab-span" style="width:97px">'.$aperto_da.'</span><br />
						</div>
						
						<div class="anagrafica-cliente">
						In carico a<br />';
						echo '<select  style="width:180px" name="assegnaimpiegatoa" >';
						
						echo '<option value="0"'.($cookie->id_employee == 0 ? 'selected="selected"' : '').' >Da assegnare</option>';
						
						foreach ($impiegati_eza as $impez) {
						
							echo "<option value='".$impez['id_employee']."' ".($azione['action_to'] == $impez['id_employee'] ? 'selected="selected"' : '')." >".$impez['firstname']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>";
						
						}

						echo '</select>
						';
						echo '</span>
						</div>
						
						
						<div class="anagrafica-cliente">
						Data apertura<br />
						<span class="tab-span">'.Tools::displayDate($azione['date_add'], (int)($cookie->id_lang), true).'</span><br />
						</div>
						
						
						
						<div class="clear"></div>
						<div class="anagrafica-cliente">
						Status<br />
						'.$status_azione.' <input type="hidden" name="id_customer" value="'.Tools::getValue('id_customer').'" />
						<input type="hidden" name="id_action" value="'.Tools::getValue('id_action').'" />
						<select name="cambiastatusa">
						<option value="closed" '.($azione['status'] == 'closed' ? 'selected="selected"' : '').'>Chiuso</option>
						<option value="pending1" '.($azione['status'] == 'pending1' ? 'selected="selected"' : '').'>In lavorazione</option>
						<option value="open" '.($azione['status'] == 'open' ? 'selected="selected"' : '').'>Aperto</option>
						</select><br />
						</div>
						<div class="anagrafica-cliente">
						Tipo azione<br />
						
						<select name="action_type" style="width:180px" id="action_type">
								<option '.($azione['action_type'] == "Attivita" ? 'selected = "selected"' : '').' value="Attivita">Attivit&agrave;</option>
								<option '.($azione['action_type'] == "Telefonata" ? 'selected = "selected"' : '').' value="Telefonata">Telefonata</option>
								<option '.($azione['action_type'] == "Visita" ? 'selected = "selected"' : '').' value="Visita">Visita</option>
								<option '.($azione['action_type'] == "Caso" ? 'selected = "selected"' : '').' value="Caso">Caso</option>
								<option '.($azione['action_type'] == "Intervento" ? 'selected = "selected"' : '').' value="Intervento">Intervento</option>
								<option '.($azione['action_type'] == "Richiesta_RMA" ? 'selected = "selected"' : '').' value="Richiesta_RMA">Richiesta RMA</option>
								<option '.($azione['action_type'] == "TODO Amazon" ? 'selected = "selected"' : '').' value="TODO Amazon">TODO Amazon</option>
								</select>
						
						
						
						</div>
						
						
						
						<div class="anagrafica-cliente">
						Data ultima com.<br />
						<span class="tab-span">'.Tools::displayDate($dataultimo, (int)($cookie->id_lang), true).'</span><br />
						</div>
						
						<div class="clear"></div>';
						
						if($azione['riferimento'] != '')
						{
							echo '
							<div class="clear"></div>
							<div class="anagrafica-cliente" style="width:690px">
							Azione padre<br />
							<span class="tab-span" style="width:690px">'.Customer::trovaSiglaLinkPerAlbero(substr($azione['riferimento'],1), substr($azione['riferimento'],0,1), '','y:A:'.$azione['id_action'].':'.$azione['status']).'</span><br />
							</div><div class="clear"></div>
							';
							
						}
						
						includeDatepicker3(array('action_date'), true);
						
						echo '<div class="anagrafica-cliente" style="width:500px">
						Data azione<br />
						<input type="text" id="action_date" style="width:100px" name="action_date" value="'.($azione['action_date'] == '0000-00-00 00:00:00' ? 'Nessuna data' : date("d-m-Y", strtotime($azione['action_date']))).'" />
						alle ore <input type="text" name="action_hours" maxlength="2" style="width:16px" value="'.date('H',strtotime($azione['action_date'])).'" />:
						<input type="text" name="action_minutes" maxlength="2" style="width:16px" value="'.date('i',strtotime($azione['action_date'])).'"  />
						
						<br /><br />Promemoria <div style="font-size:12px">
						';
						$promemoria_az = Db::getInstance()->executeS('SELECT * FROM ticket_promemoria WHERE tipo_att = "a" AND msg = '.$azione['id_action'].'');
						$i_p_a = 1;
						echo '<table>';
						foreach($promemoria_az as $p_a)
						{	
							switch($p_a['tipo']) {
								case 'DAY': $tipo_pm = 'giorni'; break;
								case 'HOUR': $tipo_pm = 'ore'; break;
								case 'MINUTE': $tipo_pm = 'minuti'; break;
								default: $tipo_pm = ''; break;
							}	
							echo '<tr id="pm_'.$i_p_a.'"><td>'.$i_p_a.') '.$p_a['conto'].' '.$tipo_pm.' prima (data: '.Tools::displayDate($p_a['ora'], $cookie->id_lang, true).')</td><td><a href="javascript:void(0)" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { deletePromemoria('.$p_a['msg'].', \''.$p_a['tipo_att'].'\', \''.$p_a['ora'].'\', '.$i_p_a.'); } else { return false; }"><img src="../img/admin/delete.gif" alt="Elimina" title="Elimina" /></a></td></tr>';
							$i_p_a++;
						}
						echo '</table>';
						echo '
						<script type="text/javascript">
						function deletePromemoria(id, tipo_att, ora, conteggio)
						{
							$.ajax({
							  url:"ajax.php?deletePromemoria=y",
							  type: "POST",
							  data: { msg: id, tipo_att: tipo_att, ora: ora
							  },
							  success:function(){
									$("#pm_"+conteggio).hide();
									alert("Promemoria cancellato con successo");
							  },
							  error: function(xhr,stato,errori){
								 alert("Errore nella cancellazione:"+xhr.status);
							  }
							});
							
							
						}
						</script>
						<script type="text/javascript">
						function add_promemoria() {
							var possible = "0123456789";
							var randomid = "";
							for( var i=0; i < 11; i++ ) {
								randomid += possible.charAt(Math.floor(Math.random() * possible.length));
							}
									
							$("<tr id=\'tr_persona_"+randomid+"\'><td><input type=\'hidden\' name=\'promemoria_nuovo[]\' value=\'1\' />Inviami promemoria: <input type=\'text\' style=\'width:30px\' name=\'action_minuti[]\' value=\'10\' /></td><td><select style=\'position:relative; height:21px;\' name=\'action_tipo_promemoria[]\'><option value=\'MINUTE\'>Minuti</option><option value=\'HOUR\'>Ore</option><option value=\'DAY\'>Giorni</option></select> prima <a href=\'javascript:void(0)\' onclick=\'togli_riga("+randomid+");\'><img src=\'../img/admin/delete.gif\' alt=\'Cancella\' title=\'Cancella\' style=\'cursor:pointer\' /></a></td></tr>").appendTo("#promemoriaTableBody");

						}
							
						function togli_riga(id) {
							document.getElementById(\'tr_persona_\'+id).innerHTML = "";
						}
						</script>
						
						
						 <table id="promemoriaTable"><tbody id="promemoriaTableBody"></tbody></table><br />
						<a href="javascript:void(0)" onclick="javascript:add_promemoria()";><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" /> Clicca per aggiungere un promemoria <img src="../img/admin/time.gif" style="width:20px" alt="Promemoria" title="Promemoria" /></a> <br /></td></tr>
						</div><br />
						
						</div>
						
						<div class="clear"></div>';
						
						$azione_note = Db::getInstance()->executeS('SELECT * FROM note_attivita WHERE tipo_attivita = "A" AND id_attivita = '.$azione['id_action'].'');
						
						echo '<strong>Note interne visibili solo agli impiegati</strong><br />';
						
						echo '<table><thead>'.(count($azione_note) > 0 ? '<tr><th>Nota</th><th>Creata da</th><th>Ultima modifica</th><th></th></tr>' : '' ).'</thead><tbody id="tableNoteBody">
						';
						
						foreach($azione_note as $azione_nota)
						{
							echo '<tr id="tr_note_'.$azione_nota['id_note'].'">';
							echo '
							<td>
							<textarea class="textarea_note" name="note_nota['.$azione_nota['id_note'].']" id="note_nota['.$azione_nota['id_note'].']" style="width:540px; height:16px" onkeyup="auto_grow(this)">'.htmlentities($azione_nota['note'], ENT_COMPAT, 'UTF-8').'</textarea><input type="hidden" name="note_nuova['.$azione_nota['id_note'].']" id="note_nuova['.$azione_nota['id_note'].']" value="0" /></td>
							<td><input type="text" readonly="readonly" name="note_id_employee['.$azione_nota['id_note'].']" value="'.Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM employee WHERE id_employee = '.$azione_nota['id_employee']).'" /></td>
							<td><input type="text" readonly="readonly" name="note_date_add['.$azione_nota['id_note'].']" value="'.Tools::displayDate($azione_nota['date_upd'], 5).'" /></td>';
							echo '<td><a href="javascript:void(0)" onclick="javascript:var surec=window.confirm(\'Sei sicuro?\'); if (surec) { togli_nota('.$azione_nota['id_note'].'); cancella_nota_attivita('.$azione_nota['id_note'].'); } else { return false; }; "><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" style="cursor:pointer" /></a></td>';
							echo'
							</tr>
							';
						}
						
						echo '</tbody></table><br />';
						
						echo '
						<script type="text/javascript">add_nota_privata();</script>
						<a href="javascript:void(0)" onclick="javascript:add_nota_privata()";><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" /> Clicca per aggiungere una nota privata</a> <br /><br />';
						
						if($azione['date_add'] < '2017-12-08')
						{
						echo '
							<textarea name="action_note" id="actionnoteContent" style="width:83%;height:100px">'.Tools::htmlentitiesUTF8($azione['note']).'</textarea><br />
							';
						}
						echo '<br /><div class="clear"></div>';
						
						
					
						echo '<button type="submit" class="button" name="submitAzione">
						<img src="../img/admin/save.gif" alt="Salva" title="Salva" />&nbsp;&nbsp;&nbsp;'.$this->l('   Save   ').'
						</button>
						
						<button class="button" onclick="javascript:var surec=window.confirm(\'Sei sicuro?\'); if (surec) { $(\'#coll_destinazione\').val($(\'#collega_todo\').val()); document.forms[\'collega_attivita\'].submit(); return false; } else { return false; }; " style="margin-left:10px; margin-right:0px">
						
						<img src="../img/admin/link.gif" alt="Collega" title="Collega" />&nbsp;&nbsp;&nbsp;'.$this->l('Collega a:').'
						</button>
						<select name="collega_todo" id="collega_todo" style="margin-left:-4px; height:27px;width:150px" >
							<option value="">-- Seleziona --</option>
							'.Customer::BuildSelectForLinking($customer->id, $azione['id_action'], 'A').'
						</select>
						';
							
						
						echo '</form>
					
						<form style="margin-left:5px; display:block;float:left" name="collega_attivita" id="collega_attivita" action="'.htmlentities($_SERVER['REQUEST_URI']).'&conf=4" method="post" />
						<input type="hidden" name="submitCollegaAttivita" value="" />
						<input type="hidden" name="coll_destinazione" id="coll_destinazione" value="" />
						<input type="hidden" name="coll_partenza" id="coll_partenza" value="A'.$azione['id_action'].'" />
						</form>
						</fieldset><br /><br /></div>';
					
					}

				
					
					if(isset($_GET['id_action'])) {
						echo '<div class="yetii-azioni" id="azioni-2">';
					}
					
					$messaggia = Db::getInstance()->ExecuteS("SELECT * FROM action_message am WHERE am.id_action = ".$_GET['id_action']."");
					
					foreach($messaggia as $messaggioa) {
					
						$employee_from = new Employee($messaggioa['action_m_from']);
						$employee_to = new Employee($messaggioa['action_m_to']);
						
						echo "<table style='width:100%; 
						border:1px solid #dfd5c3; background-color: #ffecf2;'margin-bottom:15px'>";
						echo '<tr>
							<td style="colspan:2">
								<table>
						
								<td style="font-size:14px; width:250px">Da: <strong>'.$employee_from->firstname." ".substr($employee_from->lastname,0,1).".".'<strong></td>
								<td style="width:170px"><strong>Data: </strong>'.date("d-m-Y H:i:s",strtotime($messaggioa['date_add'])).'</td>
								<td style="width:180px"><strong>Allegato: </strong>';
								if(!empty($messaggioa['file_name'])) {
								
									$allegatia = explode(";",$messaggioa['file_name']);
									$nalla = 1;
									foreach($allegatia as $allegatoa) {
										if(strpos($allegatoa, ":::")) {
							
											$parti = explode(":::", $allegatoa);
											$nomeveroa = $parti[1];
											
										}
							
										else {
											$nomeveroa = $allegatoa;
							
										}
										if($allegatoa == "") { } else {
											if($nalla == 1) { } else { echo " - "; }
											echo '<a href="ajax.php?openAllegato=y&id_customer='.$customer->id.'&viewcustomer&viewaction&id_action='.$azione['id_action'].'&token='.$this->token.'&tab-container-1=10&filename='.$allegatoa.'"><span style="color:red">'.$nomeveroa.'</span></a>';
											$nallp++;
										}
									}
								
								}
								
								else { echo 'Nessun allegato'; }
								
								
								echo '</td>';
								
								
								
								
								echo '</tr></table>
								
							</td>
							</tr>
							<tr>
							<td style="colspan:2">
								<table>
								<tr>
								<td style="width:100px"><strong>Messaggio</strong></td>
								<td>'.htmlspecialchars_decode($messaggioa['action_message']).'
								<form method="post">
								<input type="hidden" name="modifica_messaggio_azione" value="'.htmlentities($messaggioa['action_message'], ENT_QUOTES | ENT_SUBSTITUTE, "UTF-8").'" />
								<input type="hidden" name="modifica_messaggio_azione_id" value="'.$messaggioa['id_action_message'].'" />
								<button type="submit" class="button" name="modifica_messaggio_submit" value="Modifica">
								<img src="../img/admin/edit.gif" alt="Modifica" title="Modifica" />&nbsp;&nbsp;&nbsp;Modifica
								</button>
								
								'.($cookie->profile != 8 ? '<button type="submit" class="button" name="cancella_messaggio_submit" value="Cancella" onclick="javascript:var surec=window.confirm(\'Sei sicuro/a?\'); if (surec) { return true; } else { return false; }; ">
								<img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" />&nbsp;&nbsp;&nbsp;Cancella
								</button>' : '').'
								</form>
								</td>
								</tr>
								</table>
							</td>';
							echo '</tr>
							<tr><td style="colspan:2">
								<table>
								<tr>
								<td><strong>Inviato a: </strong>'.$employee_to->firstname.'</td>
								
								
								<td style="padding-left:20px"><strong>In carico a</strong></td>
								<td>'.($messaggioa['in_carico'] > 0 ? Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$messaggioa['in_carico']) : '--').'</td>
								
								'.($messaggioa['telefono_cliente'] != '' ? '<td style="padding-left:20px"><strong>Telefono cliente</td><td><a href="callto:'.$messaggioa['telefono_cliente'].'">'.$messaggioa['telefono_cliente'].'</a></td>' : '').'
								';
							$cc = Db::getInstance()->getValue("SELECT cc FROM ticket_cc WHERE type = 'a' AND msg = ".$messaggioa['id_action_message']."");
								if(!empty($cc)) {
								
									echo '<td style="padding-left:20px">';
								
									$ccs = explode(";",$cc);
									$cc_str = "<strong>CC:</strong> ";
									
									foreach ($ccs as $conoscenza) {
									
										$imp = Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".$conoscenza."");
										$cc_str .= $imp." ";
									}
									
									echo $cc_str;
									echo "</td>";
								
								}
								else {
								
									echo '<td style="padding-left:20px"><strong>CC:</strong> Nessuno</td>';
								
								}
							
							echo '</tr>
								</table>
							
							</td></tr>
							</table><br />';
					}
					
					
				
				if (Tools::isSubmit('submitActionReply'))
					{
					
						/*if(Tools::getValue('messaggioa') == '') {
					
					
							Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&error=1ac&token='.$this->token.'&tab-container-1=10');
									
					
						}*/
						
						$files=array();
							$fdata=$_FILES['joinFile'];
							if(is_array($fdata['name'])){
								$count_name2 = count($fdata['name']);
								for($i=0;$i<$count_name2;++$i){
									$files[]=array(
										'name'    =>$fdata['name'][$i],
										'tmp_name'=>$fdata['tmp_name'][$i],
										'type' => $fdata['type'][$i],
										'size' => $fdata['size'][$i],
										'error' => $fdata['error'][$i],
									 
									);
								}
							}
							else $files[]=$fdata;
												
							$attachments = array();
							
							
							foreach($files as $file) {
							
								if (isset($files) AND !empty($file['name']) AND $file['error'] != 0)
									$this->_errors[] = Tools::displayError('An error occurred with the file upload.');
									
									if (!empty($file['name']))
										{
											$extension = array('.txt', '.rtf', '.doc', '.docx', '.pdf', '.zip', '.png', '.jpeg', '.gif', '.jpg');
											$filename = md5(uniqid().substr($file['name'], -5));
											$fileAttachment['content'] = file_get_contents($file['tmp_name']);
											$fileAttachment['name'] = $file['name'];
											$fileAttachment['mime'] = $file['type'];
										}
										
										if (isset($filename) AND rename($file['tmp_name'], _PS_MODULE_DIR_.'../upload/'.$filename)) {
											$filename = $filename.":::".$file['name'];
											$file_name .= $filename.";";
									}
									
									$attachments[] = $fileAttachment;
									
							}					
						
						
							
						if(Tools::getValue('nuovaazione') == 1) {
							
							
							$ultimothread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
							$thread = $ultimothread+1;
							
							$cstmr = Db::getInstance()->getRow("SELECT company, firstname, lastname, is_company FROM customer WHERE id_customer = ".(int)($customer->id)."");
								
							if($cstmr['is_company'] == 1) { $cstmr_r = $cstmr['company']; } else { $cstmr_r = $cstmr['firstname']." ".$cstmr['lastname']; }
								
							$subject = "*** AZIONE su cliente ".$cstmr_r." - Tipo azione: ".Tools::getValue('action_type')." ***";
							
							$action_date_array = explode("-",Tools::getValue('action_date'));
							
							$action_date = $action_date_array[2]."-".$action_date_array[1]."-".$action_date_array[0]." ".Tools::getValue('action_hours').":".Tools::getValue('action_minutes').":00";
							
							foreach ($_POST['promemoria_nuovo'] as $id_promemoria=>$promemoria_id) {
								Db::getInstance()->execute('INSERT INTO ticket_promemoria (msg, tipo_att, ora, conto, tipo, status) VALUES ('.$thread.', "a", "'.$action_date.'" - INTERVAL '.$_POST['action_minuti'][$id_promemoria].' '.$_POST['action_tipo_promemoria'][$id_promemoria] .', "'.$_POST['action_minuti'][$id_promemoria].'", "'.$_POST['action_tipo_promemoria'][$id_promemoria].'", "open")');
							}
							
							if(strpos(Tools::getValue('action_date'),'0000') !== false) {
								$action_date = date("Y-m-d H:i:s");
							}

							Customer::Storico($thread, 'A', $cookie->id_employee, 13);
							
							Db::getInstance()->ExecuteS("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, action_date, status, riferimento, date_add, date_upd, note) VALUES ('$thread', '".Tools::getValue('action_type')."', '".$customer->id."', '".addslashes($subject)."', '".$cookie->id_employee."', '".Tools::getValue('in_carico_a_a')."', '".$action_date."', '".addslashes(Tools::getValue('statusa'))."', '".Tools::getValue('riferimento')."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."', '".addslashes(Tools::getValue('action_note'))."')");   
							
							
							Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, action_message, file_name, telefono_cliente, date_add, date_upd) VALUES (NULL, '$thread', ".$customer->id.", '".$cookie->id_employee."', '".Tools::getValue('in_carico_a_a')."', '".Tools::getValue('in_carico_a_a')."', '".addslashes(Tools::getValue('messaggioa'))."', '".addslashes($file_name)."', '".Tools::getValue('telefono_cliente')."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
							
							
							if(Tools::getIsset('riferimentoo') && Tools::getValue('riferimentoo') != '')
							{	
								if(substr(Tools::getValue('riferimentoo'),0,1) == 'C')
								{
									$note_da_copiare = Db::getInstance()->executeS('SELECT * FROM cart_note WHERE id_cart = "'.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo'))).'"');
								}
								else if(substr(Tools::getValue('riferimentoo'),0,1) == 'O')
								{
									$note_da_copiare = Db::getInstance()->executeS('SELECT * FROM cart_note WHERE id_cart = "'.Db::getInstance()->getValue('SELECT id_cart FROM orders WHERE id_order = '.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo')))).'"');
								}
								else
								{ 
									$note_da_copiare = Db::getInstance()->executeS('SELECT * FROM note_attivita WHERE id_attivita = "'.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo'))).'" AND tipo_attivita = "'.substr(Tools::getValue('riferimentoo'),0,1).'"');
								}
								foreach($note_da_copiare as $ndc)
								{	
									Db::getInstance()->executeS('INSERT INTO note_attivita (id_note, id_attivita, tipo_attivita, id_employee, note, date_add, date_upd) VALUES (NULL, '.$thread.', "A", "'.$ndc['id_employee'].'","'.$ndc['note'].'", "'.$ndc['date_add'].'","'.$ndc['date_upd'].'")');
									
								}
							}	
						
							
							
							$idmessaggio = mysqli_insert_id($link_glob);
							
							
						}
						
						else {
							
							$thread = $azione['id_action'];
							$current_action_status = Db::getInstance()->getValue('SELECT status FROM action_thread WHERE id_action = '.$thread);
							$precedente_status = Db::getInstance()->getValue("SELECT status FROM action_thread WHERE id_action = ".$azione['id_action']."");
							if($precedente_status != Tools::getValue('statusa'))
							{	
								switch(Tools::getValue('statusa'))
								{
									case 'open': $switch_status = 3; break;
									case 'pending1': $switch_status = 4; break;
									case 'closed': $switch_status = 5; break;
									default: $switch_status = 10; break;
								}	
								
								Customer::Storico($azione['id_action'], 'A', $cookie->id_employee, $switch_status);
					
							}
							
							$precedente_incarico = Db::getInstance()->getValue("SELECT action_to FROM action_thread WHERE id_action = ".$azione['id_action']."");
							
							if($precedente_incarico != Tools::getValue('in_carico_a_a'))
								Customer::Storico($azione['id_action'], 'A', $cookie->id_employee, 2, Tools::getValue('in_carico_a_a'));
						
							Db::getInstance()->ExecuteS("UPDATE action_thread SET  status = '".addslashes(Tools::getValue('statusa'))."', date_upd = '".date("Y-m-d H:i:s")."', action_to = '".Tools::getValue('in_carico_a_a')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_action = '".$azione['id_action']."'");  
						
							Customer::chiudiOrdineDaAzione($azione['id_action']);
							
							if(isset($_POST['modifica_messaggio_azione_id'])) {
								Customer::Storico($azione['id_action'], 'A', $cookie->id_employee, 16);
								Db::getInstance()->ExecuteS("UPDATE action_message SET action_message = '".addslashes(Tools::getValue('messaggioa'))."' WHERE id_action_message = ".$_POST['modifica_messaggio_azione_id'].""); 
								$idmessaggio = $_POST['modifica_messaggio_azione_id'];
							}
							else {
								Customer::Storico($azione['id_action'], 'A', $cookie->id_employee, 15);
								Db::getInstance()->ExecuteS("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, action_message, file_name, telefono_cliente, date_add, date_upd) VALUES (NULL, '$thread', ".$customer->id.", '".$cookie->id_employee."', '".Tools::getValue('in_carico_a_a')."', '".Tools::getValue('in_carico_a_a')."', '".addslashes(Tools::getValue('messaggioa'))."', '".addslashes($file_name)."', '".Tools::getValue('telefono_cliente')."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')"); 
								$idmessaggio = mysqli_insert_id($link_glob);
								$subject = Db::getInstance()->getValue('SELECT subject FROM action_thread WHERE id_action = '.$thread);
								
								
								if(substr($subject,0,20) == '*** VERIFICA TECNICA')
								{	
									$id_cart = Db::getInstance()->getValue('SELECT id_cart FROM orders WHERE id_order = '.substr($subject, -6,5));
									$note_private = Db::getInstance()->getValue("SELECT note_private FROM cart WHERE id_cart = ".$id_cart."");
									$note_private .= addslashes(Tools::getValue('messaggioa'));
									
									if((Tools::getValue('cambiastatusa') == 'closed' || Tools::getValue('statusa') == 'closed' )  && $current_action_status != 'closed')
									{
										Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.substr($subject, -6,5).', 25, "'.date('Y-m-d H:i:s').'")');
										Product::RecuperaCSV(substr($subject, -6,5));
										
									}
									else if((Tools::getValue('cambiastatusa') == 'open' || Tools::getValue('statusa') == 'open' || Tools::getValue('cambiastatusa') == 'pending1' || Tools::getValue('statusa') == 'pending1') && $current_action_status == 'closed')
									{
										Db::getInstance()->execute('INSERT INTO order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$cookie->id_employee.', '.substr($subject, -6,5).', 24, "'.date('Y-m-d H:i:s').'")');
										Product::RecuperaCSV(substr($subject, -6,5));
										
									}
									
									Db::getInstance()->executeS('UPDATE cart SET note_private = "'.addslashes($note_private).'" WHERE id_cart = '.$id_cart);
									Db::getInstance()->executeS('UPDATE carrelli_creati SET note_private = "'.addslashes($note_private).'" WHERE id_cart = '.$id_cart);
								}
								else if(substr($subject,0,10) == '*** RMA N.')
								{	
									
									if(Tools::getValue('cambiastatusa') == 'closed'  && $current_action_status != 'closed')
									{
										Db::getInstance()->executeS('UPDATE rma SET test = 0 WHERE id_customer_thread = '.substr(Db::getInstance()->getValue('SELECT riferimento FROM action_thread WHERE id_action = '.Tools::getValue('id_action')),1));
									}
								}
							}
							
						}
						
						
						Customer::chiudiPadre(Db::getInstance()->getValue('SELECT riferimento FROM action_thread WHERE id_action = '.$thread ));
						
							$file_csv=fopen("../cms/quotazioni/azioni.csv","a+");
							
							$in_carico_a =  Db::getInstance()->getValue("SELECT firstname FROM employee WHERE id_employee = ".Tools::getValue('in_carico_a_a')."");
							
							$prima_azione = Db::getInstance()->getValue("SELECT id_action_message FROM action_message WHERE id_action = $thread ORDER BY id_action_message ASC");
								
							if(!$prima_azione)
								$prima_azione = $idmessaggio;
							
							$messaggiopercsv = Tools::getValue('messaggioa');
							$messaggiopercsv = strip_tags($messaggiopercsv);
							$messaggiopercsv = str_replace(";", ",", $messaggiopercsv);
								
							$messaggiopercsv = str_replace(chr(10), " ", $messaggiopercsv); //remove carriage returns
							$messaggiopercsv = str_replace(chr(13), " ", $messaggiopercsv); //remove carriage returns	
							$ref = rand(0, 99999); $ref2 = rand(0, 99999);
							
							$riga_richiesta = "".date("d/m/Y H:i:s").";".date('Ymd')."-".$ref."-".$ref2.";".$in_carico_a.";".Tools::getValue('action_type').";".Tools::getValue('statusa').";".$messaggiopercsv.";".$thread.";".$idmessaggio.";".$prima_azione.";".$customer->id."\n";
								
										
							
							
							
							$subj_per_ctrl = Db::getInstance()->getValue("SELECT subject FROM action_thread WHERE id_action = ".$thread."");
							
							$pos = strpos($subj_per_ctrl,"ti richiamiamo noi");
							
							//if (strlen(stristr($subj_per_ctrl,"ti richiamiamo noi"))>0)
								@fwrite($file_csv,$riga_richiesta);
							
							@fclose($file_csv);	
						
							$mail_verso_cui_inviare = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = ".Tools::getValue('in_carico_a_a')."");
								
								
							$tokenimp = Tools::getAdminToken("AdminCustomers"."2".Tools::getValue('in_carico_a_a'));
							$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewaction&id_action=".$thread."&token=".$tokenimp.'&tab-container-1=10';			
								
							if((Tools::getValue('messaggioa')) == '') {
								$testo_azione = "Il testo di questa azione &egrave; vuoto.";
							}
							else {
								$testo_azione = "Testo azione: ".(Tools::getValue('messaggioa'));
							}
							
							if($customer->is_company == 1) {
							
								$cliente = $customer->company;
							
							}
							else {
							
								$cliente = $customer->firstname." ".$customer->lastname;
							}
								
							
							
							/*
							if (Mail::Send((int)$cookie->id_lang, 'action', Mail::l('Aperta azione su Ezdirect', (int)$cookie->id_lang), 
								$params, $mail_verso_cui_inviare, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
								_PS_MAIL_DIR_, true))
							{
								//$ct->status = 'closed';
								//$ct->update();
							}
							*/
							$cc = "";
							$cc_str = "";
							foreach($_POST['conoscenza_a'] as $conoscenza) {
								
								$cc_str = $cc_str.Db::getInstance()->getValue('SELECT firstname FROM employee WHERE id_employee = '.$conoscenza).' ';
							}
							
							foreach($_POST['conoscenza_a'] as $conoscenza) {
								
								
							
								$cc .= $conoscenza.";";
								$tokenimp = Tools::getAdminToken("AdminCustomers"."2".$conoscenza);
								$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&viewaction&id_action=".$thread."&token=".$tokenimp.'&tab-container-1=10';			
							
								echo $conoscenza."<br />";
							
								if((Tools::getValue('messaggioa')) == '') {
									$testo_azione = "Il testo di questa azione &egrave; vuoto.";
								}
								else {
									$testo_azione = "Testo azione: ".(Tools::getValue('messaggioa'));
								}
											
								$params = array(
								'{reply}' => "Ti &egrave; stata inviata una azione in copia conoscenza su Ezdirect.<br /><br />
								Azione aperta da: <strong>".$employeemess->firstname."</strong><br /><br />
								Azione in carico a: <strong>".$in_carico_a."</strong><br /><br />
								Tipo azione: <strong>".Tools::getValue('action_type')."</strong><br /><br />".$testo_azione."<br /><br />
								".($cc_str != '' ? "Il messaggio &egrave; stato inviato in conoscenza a: ".$cc_str."<br /><br />" : "")."
								Per aprire questa azione, <a href='".$linkimp."'>clicca su questo link</a>.");
							
								$mailconoscenza = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = $conoscenza");
										
								Mail::Send((int)$cookie->id_lang, 'senzagrafica', Mail::l('Azione in copia conoscenza', (int)$cookie->id_lang), 
								$params, $mailconoscenza, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
								_PS_MAIL_DIR_, true);
									
							}
							
							$params = array(
							'{reply}' => "Ti &egrave; stata inviata una azione su Ezdirect.<br /><br />Azione aperta da: <strong>".$employeemess->firstname."</strong><br /><br />Cliente: <strong>".$cliente."</strong><br /><br />Azione in carico a: <strong>".$in_carico_a."</strong><br /><br />Tipo azione:<strong>".Tools::getValue('action_type')."</strong><br /><br />".$testo_azione."<br /><br />".($cc_str != '' ? "Il messaggio &egrave; stato inviato in conoscenza a: ".$cc_str."<br /><br />" : "")."Per leggere questa azione o per rispondere, <a href='".$linkimp."'>clicca su questo link</a>.");
							
							if(!empty($_POST['conoscenza_a'])) {
									
								Db::getInstance()->executeS("INSERT INTO ticket_cc (msg, type, cc) VALUES (".$idmessaggio.", 'a', '".$cc."')"); 
									
									
							}
							
							if(Tools::getValue('in_carico_a_a') != 0) {

							
								$mail_incarico_a = Db::getInstance()->getValue('SELECT email FROM employee WHERE id_employee = '.Tools::getValue('in_carico_a_a').'');
								
								if(Tools::getValue('in_carico_a_a') == $cookie->id_employee) {
								}
								else {
									Mail::Send(5, 'action', Mail::l('Azione assegnata a te su Ezdirect', 5), 
									$params, $mail_incarico_a, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
									_PS_MAIL_DIR_, true);
								}
							}
							
							Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=10');
							
				
					}
				
				
					else {
					
						if (isset($_GET['actionreply'])) {
					
						echo 'Risposta inviata con successo.<br /> <a class="button" href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&id_action='.$azione['id_action'].'&token='.$this->token.'&tab-container-1=10"><strong>Clicca qui se vuoi rispondere nuovamente.</strong></a><br /><br />';
						
						}
						
						else if (isset($_GET['actionreply2'])) {
					
						echo 'Azione aperta con successo. <br /><br />';
						
						}
					
						else if (!isset($_GET['actionreply']) || isset($_GET['aprinuovaazione'])) {
						$iso = Language::getIsoById((int)($cookie->id_lang));
						$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
						$ad = dirname($_SERVER["PHP_SELF"]);
				
				
					if(isset($_GET['aprinuovaazione'])) {
					
						echo '<h2>Apri nuova azione</h2>';
					
					
					}
				
					else {
					
						echo '<h2 id="rispondi-azione">Rispondi a questa azione</h2>';
						if(isset($_POST['modifica_messaggio_azione'])) {
							
							if(Tools::isSubmit('cancella_messaggio_submit'))
							{
								$id_action = Db::getInstance()->getValue('SELECT id_action FROM action_message WHERE id_action_message = '.Tools::getValue('modifica_messaggio_azione_id'));
								Db::getInstance()->getInstance()->execute('DELETE FROM action_message WHERE id_action_message = '.Tools::getValue('modifica_messaggio_azione_id'));
								
								Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&viewaction&id_action='.$id_action.'&tab-container-1=10');
							}
							else
							{
								echo '<script type="text/javascript">
									 window.location = "#rispondi-azione";
								</script>';
							}
						}
						
					}
				
				
				
						echo '
					<script type="text/javascript">
					var iso = \''.$isoTinyMCE.'\' ;
					var pathCSS = \''._THEME_CSS_DIR_.'\' ;
					var ad = \''.$ad.'\' ;
					</script>
					
					
					<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce3/tiny_mce.js"></script>
					<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>
					<script type="text/javascript">
						$(document).ready(function () {
						
		dataChanged = 0;     // global variable flags unsaved changes      

		function bindForChange(){    
			 $(\'input,checkbox,text,textarea,radio,select\').bind(\'change\',function(event) { dataChanged = 1});
			 $(\'#messaggiop\').bind(\'input propertychange\', function() { dataChanged = 1 });
			 $(\':reset,:submit\').bind(\'click\',function(event) { dataChanged = 0 });
		}


		function askConfirm(){  
			if (dataChanged){ 
				return "You have some unsaved changes.  Press OK to continue without saving." 
			}
		}

		window.onbeforeunload = askConfirm;
		window.onload = bindForChange;
		});
					</script>
					<!-- <script type="text/javascript">
							 toggleVirtualProduct(getE(\'is_virtual_good\'));
							unitPriceWithTax(\'unit\'); 
					</script> -->';
					$employeemessa = new Employee($cookie->id_employee);
						echo'	<form name="submit_azione" id="submit_azione" action="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&id_action='.$azione['id_action'].'&token='.$this->token.'&tab-container-1=10'.(Tools::getIsset('riferimento') ? '&riferimentoo='.Tools::getValue('riferimento') : '').'" method="post" class="std" enctype="multipart/form-data">
						<fieldset style="background-color:#ffe8d4">';
				
						
						if(isset($_GET['aprinuovaazione'])) {
					
						echo '<input type="hidden" name="nuovaazione" value="1" /><input type="hidden" name="riferimento" id="riferimento" value="'.Tools::getValue('riferimento').'" /><input type="hidden" name="riferimentoo" id="riferimentoo" value="'.Tools::getValue('riferimento').'" />';
					
						}
						
						else {
						
							echo '			<script type="text/javascript">
							
								$(document).ready(function () {
									
									$("#submit_preventivo").submit(function () {
										d = document.getElementById("statusa").value;
										
										if(d != "'.$azione['status'].'") {
											
												}
										
										else {
											var salvamodifiche = window.confirm("STATUS: OK per procedere, ANNULLA per cambiare."); 
									
												if (salvamodifiche) {
													submitFormOkay = true;
													
				$(\'#waiting1\').waiting({ 
				elements: 10, 
				auto: true 
				});
				var overlay = jQuery(\'<div id="overlay"></div>\');
				var attendere = jQuery(\'<div id="attendere">ATTENDERE PREGO</div>\');
				overlay.appendTo(document.body);
				attendere.appendTo(document.body);
													$("#submitMessage").trigger("click");
												
												} else {
													 return false;
														
													//niente
												}
										}
									});
									
									
								});
					
							</script>';	
							
						}
						
						
						
							echo '<table>
							';
							
							
									
							
							if(isset($_GET['aprinuovaazione'])) {
							
								echo '<tr><td>Tipo azione</td>
								<td>
								<select name="action_type" id="action_type">
								<option value="Attivita" '.(Tools::getIsset('tipo-todo') && Tools::getValue('tipo-todo') == 'Attivita' ? 'selected="selected"' : '' ).'>Attivit&agrave;</option>
								<option value="Telefonata" '.(Tools::getIsset('tipo-todo') && Tools::getValue('tipo-todo') == 'Telefonata' ? 'selected="selected"' : '' ).'>Telefonata</option>
								<option value="Visita" '.(Tools::getIsset('tipo-todo') && Tools::getValue('tipo-todo') == 'Visita' ? 'selected="selected"' : '' ).'>Visita</option>
								<option value="Caso" '.(Tools::getIsset('tipo-todo') && Tools::getValue('tipo-todo') == 'Caso' ? 'selected="selected"' : '' ).'>Caso</option>
								<option value="Intervento" '.(Tools::getIsset('tipo-todo') && Tools::getValue('tipo-todo') == 'Intervento' ? 'selected="selected"' : '' ).'>Intervento</option>
								<option value="Richiesta_RMA" '.(Tools::getIsset('tipo-todo') && Tools::getValue('tipo-todo') == 'Richiesta_RMA' ? 'selected="selected"' : '' ).'>Richiesta RMA</option>
								<option value="TODO Amazon" '.(Tools::getIsset('tipo-todo') && Tools::getValue('tipo-todo') == 'TODO Amazon' ? 'selected="selected"' : '' ).'>TODO Amazon</option>
								</select>
								</td>
								</tr>';
								
							} else {
							
								echo '<input type="hidden" name="action_type" value="'.($azione['action_type']).'" />';
							
							}
							
						echo '<tr>
										<td>
										';
										
									$customer_phone = Db::getInstance()->getRow("SELECT	phone, phone_mobile FROM address WHERE id_customer = ".$customer->id." AND deleted = 0 AND fatturazione = 1 AND active = 1");
									
									if($customer_phone['phone'] == '') { $telefono_cliente = $customer_phone['phone_mobile']; } else { $telefono_cliente = $customer_phone['phone']; }
										
										echo '
										<input type="hidden" name="id_customer" value="'.$customer->id.'" />
										
										<input type="hidden" value="'.$thread['id_customer_thread'].'" name="id_customer_thread">
										</td>
									</tr>
							
									';
									
								
								echo '
								<tr><td>Stato azione</td>
								<td>
								<div style="float:left">
								<select name="statusa" id="statusa">
								<option value="open" '.($azione['status'] == 'open' ? 'selected="selected"' : '').'>Aperto   </option>
								<option value="pending1" '.($azione['status'] == 'pending1' ? 'selected="selected"' : '').'>In lavorazione   </option>
								<option value="closed" '.($azione['status'] == 'closed' ? 'selected="selected"' : '').'>Chiuso   </option>
								
								</select>
									</div><div style="float:left; margin-left:20px">
									In carico a';
							echo '
											
						<script type="text/javascript">
										function changeInCaricoAA()
										{';
										
										
										foreach ($impiegati_eza as $impez) {
											echo '
											
											firstVar = document.getElementById("in_carico_a_a").value;
											
											if (document.getElementById("in_carico_a_a").value == "'.$impez['id_employee'].'") {
											
											
											document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').disabled = true;
											document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').checked = false;
											
											} else { document.getElementById(\'conoscenza_'.$impez['id_employee'].'\').disabled = false; }
										';
										}
									echo '
										}
										</script>
						<select style="width:250px" name="in_carico_a_a" id="in_carico_a_a" onChange = "changeInCaricoAA();">';

						
						foreach ($impiegati_eza as $impezp) {

						
							if(isset($_GET['aprinuovaazione'])) {
								echo "<option value='".$impezp['id_employee']."' ".($cookie->id_employee == $impezp['id_employee'] ? 'selected="selected"' : '')." >".$impezp['firstname']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>";
							}
							else {
								echo "<option value='".$impezp['id_employee']."' ".($azione['action_to'] == $impezp['id_employee'] ? 'selected="selected"' : '')." >".$impezp['firstname']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>";
							}
						}

						echo '</select></div>
						';
						$telefoni_cliente = Db::getInstance()->executeS('SELECT phone AS tel FROM address WHERE id_customer = '.$customer->id.' AND deleted = 0 AND active = 1
						UNION
						SELECT phone_mobile AS tel FROM address WHERE id_customer = '.$customer->id.' AND deleted = 0 AND active = 1
						UNION
						SELECT phone AS tel FROM persone WHERE id_customer = '.$customer->id.'
						UNION
						SELECT phone_2 AS tel FROM persone WHERE id_customer = '.$customer->id.'
						UNION
						SELECT phone_mobile AS tel FROM persone WHERE id_customer = '.$customer->id.'
						');
						$telefoni_options = '';
						
						$attuale_tel = Db::getInstance()->getValue('SELECT telefono_cliente FROM action_message WHERE id_action = '.Tools::getValue('id_action').' ORDER BY id_action_message DESC');
						$i_tels = 0;
						foreach($telefoni_cliente as $tel)
						{
							if($i_tels == 0 && $tel['tel'] != '')
								$primo_tel = $tel['tel'];
							
							if($tel['tel'] != '')
							{	
								$telefoni_options .= '<option value="'.$tel['tel'].'" '.($tel['tel'] == $attuale_tel ? ' selected="selected"' : '').'>'.$tel['tel'].'</option>';
								$i_tels++;
							}	
						}
						if($attuale_tel == '')
							$attuale_tel = $primo_tel;
						
						
						echo '<div style="float:left; margin-left:20px"> Tel. Cliente: <select onclick="document.getElementById(\'tel_action\').href=\'callto:\'+document.getElementById(\'telefono_cliente\').value" id="telefono_cliente" name="telefono_cliente">'.$telefoni_options.'</select> <a id="tel_action" href="callto:'.$attuale_tel.'">Chiama</a></div></td></tr>
								';
						
						echo '
						
									</tr>
									<tr><td>Conoscenza</td>
									<td>
									';
									$impez_count = 0;
									foreach ($impiegati_eza as $impez) {
										$impez_count++;
										if($impez_count == 10)
											echo '<br />';
										
										echo "<input type='checkbox' name='conoscenza_a[]' id ='conoscenza_".$impez['id_employee']."' value='".$impez['id_employee']."' /> ".$impez['firstname']."&nbsp;&nbsp;&nbsp;&nbsp;";
									
									}
									echo '<br /></td>
									</tr>
								<tr><td><br /></td></tr>
								';
								if(isset($_GET['aprinuovaazione'])) {
									includeDatepicker3(array('action_date'), true);
									
									echo '<tr><td>Data azione</td>
									<td><input type="text" id="action_date" style="width:100px" name="action_date" value="'.($azione['action_date'] == '0000-00-00 00:00:00' ? 'Nessuna data' : date("d-m-Y")).'" />
						alle ore <input type="text" name="action_hours" maxlength="2" style="width:16px" value="'.date('H').'" />:
						<input type="text" name="action_minutes" maxlength="2" style="width:16px" value="'.date('i').'"  />
						</td>
									</tr>
									<tr><td><br /></td></tr>
									<tr><td>Promemoria</td><td>
									
									<script type="text/javascript">
									function add_promemoria() {
										var possible = "0123456789";
										var randomid = "";
										for( var i=0; i < 11; i++ ) {
											randomid += possible.charAt(Math.floor(Math.random() * possible.length));
										}
												
										$("<tr id=\'tr_persona_"+randomid+"\'><td><input type=\'hidden\' name=\'promemoria_nuovo[]\' value=\'1\' />Inviami promemoria: <input type=\'text\' style=\'width:30px\' name=\'action_minuti[]\' value=\'10\' /></td><td><select style=\'position:relative; height:21px;\' name=\'action_tipo_promemoria[]\'><option value=\'MINUTE\'>Minuti</option><option value=\'HOUR\'>Ore</option><option value=\'DAY\'>Giorni</option></select> prima <a href=\'javascript:void(0)\' onclick=\'togli_riga("+randomid+");\'><img src=\'../img/admin/delete.gif\' alt=\'Cancella\' title=\'Cancella\' style=\'cursor:pointer\' /></a></td></tr>").appendTo("#promemoriaTableBody");

									}
										
									function togli_riga(id) {
										document.getElementById(\'tr_persona_\'+id).innerHTML = "";
									}
									</script>
									
									
									 <table id="promemoriaTable"><tbody id="promemoriaTableBody"></tbody></table><br />
									<a href="javascript:void(0)" onclick="javascript:add_promemoria()";><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" /> Clicca per aggiungere un promemoria</a> <br /></td></tr>
									';
								} else { }
								
								echo '<tr><td><br /></td></tr>';
								
								echo '<tr><td>Link prodotto</td><td><link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
								<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete3.js"></script>
								<div id="ajax_choose_product" style="padding:6px; padding-top:2px; width:800px;">
									
									<input type="checkbox" checked="checked" id="prodotti_online" onclick="if(document.getElementById(\'product_autocomplete_input_action\').value != \'\') { clearAutoComplete(); }" />Online &nbsp;&nbsp;
				<input type="checkbox" id="prodotti_offline" onclick="if(document.getElementById(\'product_autocomplete_input_action\').value != \'\') { clearAutoComplete(); }" />Offline &nbsp;
	<input type="checkbox" id="prodotti_old" onclick="if(document.getElementById(\'product_autocomplete_input_action\').value != \'\') { clearAutoComplete(); }" />Old &nbsp;&nbsp;
				<input type="checkbox" id="prodotti_disponibili" onclick="if(document.getElementById(\'product_autocomplete_input_action\').value != \'\') { clearAutoComplete(); }" />Solo disp. &nbsp;&nbsp;&nbsp;
				
				<script type="text/javascript" src="../js/select2.js"></script>
						<script type="text/javascript">
						$(document).ready(function() { $("#auto_marca").select2(); $("#auto_serie").select2(); $("#auto_fornitore").select2();  $("#auto_categoria").select2(); });
						</script>
						
				<select id="auto_marca" name="auto_marca" onchange="repopulateSeries(this.value); clearAutoComplete();" style="width:100px">
				<option value="0">Marca...</option>
				';
				$marche = Db::getInstance()->executeS('SELECT * FROM manufacturer ORDER BY name ASC');
				foreach($marche as $marca)
					echo '<option value="'.$marca['id_manufacturer'].'">'.$marca['name'].'</option>';
				echo '
				</select>
				
						
				<select id="auto_serie" name="auto_serie" onchange="clearAutoComplete();" style="width:100px">
				<option value="0">Serie...</option>
				<option value="0">Scegli prima un costruttore</option>
				';
				echo '
				</select>
				
				<select id="auto_categoria" name="auto_categoria" onchange="clearAutoComplete();" style="width:100px">
				<option value="0">Categoria...</option>
				';
				$categorie = Db::getInstance()->executeS('SELECT * FROM category c JOIN category_lang cl ON c.id_category = cl.id_category WHERE cl.id_lang = 5 AND c.id_parent = 1 ORDER BY cl.name ASC');
				foreach($categorie as $categoria)
					echo '<option value="'.$categoria['id_category'].'">'.$categoria['name'].'</option>';
				echo '
				</select>
				
				<select id="auto_fornitore" name="auto_fornitore" onchange="clearAutoComplete(); " style="width:100px">
				<option value="0">Fornitore...</option>
				';
				
				$fornitori = Db::getInstance()->executeS('SELECT * FROM supplier WHERE id_supplier = 8 OR id_supplier = 11 OR id_supplier = 42 ORDER BY name ASC');
				foreach($fornitori as $fornitore)
					echo '<option value="'.$fornitore['id_supplier'].'">'.$fornitore['name'].'</option>';
				echo '
				</select>
				
				<input id="reset" type="button" value="Reset" class="button" onclick="document.getElementById(\'prodotti_online\').checked = true; document.getElementById(\'prodotti_offline\').checked = true; document.getElementById(\'prodotti_old\').checked = true; document.getElementById(\'prodotti_disponibili\').checked = false; document.getElementById(\'auto_categoria\').options[0].selected = true; $(\'#auto_categoria\').select2();  document.getElementById(\'auto_marca\').options[0].selected = true; $(\'#auto_marca\').select2();  
				document.getElementById(\'auto_serie\').options[0].selected = true; $(\'#auto_serie\').select2();  
				document.getElementById(\'auto_fornitore\').options[0].selected = true; $(\'#auto_fornitore\').select2(); $(\'#prodlist\').hide(); $(\'div.autocomplete_list\').hide();" />
				
				<br />
				<input size="123" type="text" value="" id="product_autocomplete_input_action" /><input id="veditutti2" type="button" value="Cerca" class="button" onclick=\'$("#veditutti").trigger("click");\' /><input id="veditutti" type="button" value="Cerca" class="button" style="display:none" /> <script type="text/javascript">
						var formProduct = "";
						var products = new Array();
					</script>
					<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
					<script type="text/javascript">
								
									
									urlToCall = null;
									/* function autocomplete */
									
									function getOnline()
						{
							if(document.getElementById("prodotti_online").checked == true)
								return 1;
							else
								return 0;
						}
						
						function getOffline()
						{
							if(document.getElementById("prodotti_offline").checked == true)
								return 1;
							else
								return 0;
						}

	function getOld()
						{
							if(document.getElementById("prodotti_old").checked == true)
								return 1;
							else
								return 0;
						}
						
						function getDisponibili()
						{
							if(document.getElementById("prodotti_disponibili").checked == true)
								return 1;
							else
								return 0;
						}
						
						function getCopiaDa()
						{
								return 0;
						}
						
						function getSerie()
						{
							if(document.getElementById("auto_serie").value > 0)
								return document.getElementById("auto_serie").value;
							else
								return 0;
						}
						
						function getMarca()
						{
							if(document.getElementById("auto_marca").value > 0)
								return document.getElementById("auto_marca").value;
							else
								return 0;
						}
						
						function getFornitore()
						{
							if(document.getElementById("auto_fornitore").value > 0)
								return document.getElementById("auto_fornitore").value;
							else
								return 0;
						}
						
						function getCategoria()
						{
							if(document.getElementById("auto_categoria").value > 0)
								return document.getElementById("auto_categoria").value;
							else
								return 0;
						}
						
						function repopulateSeries(id_manufacturer)
						{
							$("#auto_serie").empty();
							$.ajax({
							  url:"ajax.php?repopulate_series=y",
							  type: "POST",
							  data: { id_manufacturer: id_manufacturer
							  },
							  success:function(resp){  
								var newOptions = $.parseJSON(resp);
								
								 $("#auto_serie").append($("<option></option>")
									 .attr("value", "0").text("Serie..."));
								
								$.each(newOptions, function(key,value) {
								  $("#auto_serie").append($("<option></option>")
									 .attr("value", value).text(key));
								});
								
								$("#auto_serie").select2({
									placeholder: "Serie..."
								});
							  },
							  error: function(xhr,stato,errori){
								 alert("Errore: impossibile trovare serie ");
							  }
							});
								
							
						}	
									
								function clearAutoComplete()
						{
							$("#veditutti").trigger("click");
							
						}		</script>
					
					<script type="text/javascript">
					
						function addProduct_TR(event, data, formatted)
						{
							document.getElementById("product_autocomplete_input_action").value = ""; 
							var link_rewrite = data[31];	
							var productName = data[5];
							var productRef = data[36];
							
							tinyMCE.get(\'messaggioa\').focus();
							var $body = $(tinymce.activeEditor.getBody());
							$body.prepend(\'<p>\');
							$body.append(\'</p>\');
							$body.find("p:last").append(productName + \' (\'+ productRef +\') <br /><a href="\' + link_rewrite + \'">\' + link_rewrite + \'<\/a><br /><br />\');
							$("body").trigger("click");
							$("#product_autocomplete_input_action").trigger("click");
							$("#product_autocomplete_input_action").trigger("click");
							
						}	
						
						
	 
						urlToCall = null;
						
						function getExcludeIds() {
							var ids = "";
							ids += $(\'#excludeIds\').val().replace(/\\-/g,\',\').replace(/\\,$/,\'\');
							return ids;
						}
						
						$(function() {
							$(\'#product_autocomplete_input_action\')
								.autocomplete(\'ajax_products_list2.php?products_and_bundles=y&id_customer='.Tools::getValue('id_customer').'\', {
									minChars: 0,
									autoFill: false,
									max:500,
									matchContains: true,
									matchSubset: 0,
									mustMatch:true,
									scroll:true,
									cacheLength:1,
									formatItem: function(item) {
								
												var color = "";
												if(item[24] == 1) {
													var color = "; color:red";
												}
												if(item[28] == 0) {
													return \'</table><div style="position:absolute; top:0px; border-bottom:1px solid black; left:0px; height:25px; background-color:#ffffff; font-weight:bold; color:black !important"><tr style="position:fixed;float:left"><th style="width:150px; text-align:left"><div style="width:150px;float:left;margin-left:10px;">Codice</div></th><th style="width:570px;"><div style="width:350px; float:left;">Desc. prodotto<br /><br /></div></th><th style="width:60px; text-align:right"><div style="width:60px;float:left;">Prezzo</div></th><th style="width:50px; text-align:right"><div style="width:60px;float:left;">Qt. mag.</div></th><th style="width:50px; text-align:right"><div style="width:75px;float:left;">Qt. tot</div></th></tr></div><table><tr><td style="width:145px; text-align:left"><br /><br /><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:555px\'+color+\'"><br /><br />\'+item[5]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[23]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[25]+\'</td><td style="width:50px; text-align:right"><br /><br />\'+item[22]+\'</td></tr></table>\'
													
													;
												}
												else {
													return \'<tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:350px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
													
													;
													}
												}
									
								}).result(addProduct_TR);
							
						});
					
						
						$("#product_autocomplete_input_action").focus();
					
						$("#product_autocomplete_input_action").css(\'width\',\'673px\');
						$("#product_autocomplete_input_action").keypress(function(event){
									
									  var keycode = (event.keyCode ? event.keyCode : event.which);
									  if (keycode == "13") {
										  $("#veditutti").trigger("click");
										event.preventDefault();
										event.stopPropagation();    
									  }
									});
						
						
								</script>
								
								<script type="text/javascript">
					
					$("#veditutti2").click ( function (zEvent) {
						
						/*	$("body").trigger("click");
							$("body").trigger("click");
							
							
							
							$("#product_autocomplete_input_action").focus();
						//	$("#product_autocomplete_input_action").val("");
						//	$("#product_autocomplete_input_action").val(" ");
						*/
							var press = jQuery.Event("keypress");
							press.which = 13;
							if(document.getElementById(\'product_autocomplete_input_action\').value == "")
							{
								$("#product_autocomplete_input_action").val(" ");
							}
							$("#product_autocomplete_input_action").trigger(press);
						
							$("#product_autocomplete_input_action").trigger("click");
							$("#product_autocomplete_input_action").trigger("click");
						//	$("#product_autocomplete_input_action").val("");
							
					} );


					</script>
									
						</td></tr>';
								
								
								echo '<tr><td><script type="text/javascript" src="../js/select2.js"></script>
							<script type="text/javascript">
							$(document).ready(function() { $("#utils_autocomplete_input_action").select2(); $("#messaggi_precompilati").select2(); });
						
						function addUtil_TR(event, data, formatted)
							{
								tinyMCE.get(\'messaggioa\').focus();
								var $body = $(tinymce.activeEditor.getBody());
								$body.prepend(\'<p>\');
								$body.append(\'</p>\');
								if(document.getElementById("utils_autocomplete_input_action").value != "-- Seleziona un link utile --")
								{
									$body.find("p:last").append(\'<a href="\' + document.getElementById("utils_autocomplete_input_action").value + \'">\' + document.getElementById("utils_autocomplete_input_action").value + \'<\/a><br /><br />\');
								}
								
							}	</script>
						Link utili</td><td><select name="utils_autocomplete_input_action" id="utils_autocomplete_input_action" style="width:730px" onchange="addUtil_TR();">
						'.$utils_options.'</select></td></tr>';
								echo '<tr><td>Messaggi precompilati</td><td><select name="messaggi_precompilati" id="messaggi_precompilati" style="width:730px" onchange="inserisciPrecompilato(this.value);">
						';
						$precompilati = Db::getInstance()->executeS('SELECT * FROM precompilato WHERE active = 1');
						echo '<option value="-- Seleziona un messaggio --">-- Seleziona un messaggio --</option>';
						foreach($precompilati as $precompilato)
						{
							echo '<option value="'.htmlentities($precompilato['testo'], ENT_QUOTES).'">'.htmlentities($precompilato['oggetto'], ENT_QUOTES).'</option>';
						}
						echo '
						</select>
						<script type="text/javascript">
						function inserisciPrecompilato(msg)
						{
							tinyMCE.get(\'messaggioa\').focus();
							var $body = $(tinymce.activeEditor.getBody());
							$body.html(\'\');
							$body.prepend(\'<p>\');
							$body.append(\'</p>\');
							if(msg != "-- Seleziona un messaggio --") {
								$body.find("p:last").append(msg);
							}
						}
						</script>
						</td></tr>
						';
						
						
								echo '<tr><td>Allega file</td>
								<td>
								<input name="MAX_FILE_SIZE" value="20000000" type="hidden">
								<script type="text/javascript" src="jquery.MultiFile.js"></script>
								<input name="joinFile[]" class="multi" id="joinFile" type="file" onchange="handleFileSelect(this.files)">
									<output id="list-tkt">Anteprime: </output>
										<script type="text/javascript">
									function handleFileSelect(files) {
									
										// Loop through the FileList and render image files as thumbnails.
										 for (var i = 0; i < files.length; i++) {
											var f = files[i];
											var name = files[i].name;
				  
											var reader = new FileReader();  
											reader.onload = function(e) {  
											  // Render thumbnail.
											  var span = document.createElement("span");
											  span.innerHTML = ["<a href=\'",e.target.result,"\' target=\'_blank\'>",name,"</a> - "].join("");
											  document.getElementById("list-tkt").insertBefore(span, null);
											};

										  // Read in the image file as a data URL.
										  reader.readAsDataURL(f);
										}
									  }
									 
									</script>
								</td>
								</tr>
								<tr><td>Messaggio</td>
								<td>
							
								<textarea style="width:760px" class="rte" id="messaggioa" name="messaggioa" rows="5" cols="40">
								'.(isset($_POST['modifica_messaggio_submit']) ? stripslashes($_POST['modifica_messaggio_azione']) : '').'
								</textarea>';
								
								if(isset($_POST['modifica_messaggio_submit'])) {
								
									echo "<input type='hidden' name='modifica_messaggio_azione_id' value='".$_POST['modifica_messaggio_azione_id']."' />";
								}
								
								//  onclick = "this.style.visibility=\'hidden\', loading.style.visibility=\'visible\'"
								echo '</td>
								</tr>
								<!-- <tr>
									<td>Nota impiegato</td>
									<td><textarea id="action_note" name="action_note" rows="5" cols="145"></textarea>
									</tr>-->';
									
									$riferimento = Tools::getValue('riferimento') ? Tools::getValue('riferimento') : Db::getInstance()->getValue('SELECT riferimento FROM action_thread WHERE id_action = '.$azione['id_action']);
							$statusPadre = Customer::statusPadre($riferimento);
							echo '<tr><td colspan="2">'.($riferimento != '' && $statusPadre != 'closed' && $statusPadre != '' ? '<div style="border:3px solid red; padding:5px;"><input type="checkbox" name="chiudi_padre" />&nbsp;&nbsp;&nbsp;<strong style="color:red">ATTENZIONE!</strong>  L\'azione padre risulta ancora aperta. Spunta questa casella se vuoi chiuderla.</div>' : '').'</td></tr>';
							
									
									echo '
								<tr><td></td>
								<td>
									<button type="submit" class="button"  name="submitActionReply" id="submitActionReply" value="Modifica">
									<img src="../img/admin/enabled-2.gif" alt="Conferma" title="Conferma" />&nbsp;&nbsp;&nbsp;Conferma azione
									</button>
								</td>
								</tr> 
							</table>
						</fieldset>
					</form>
				';
				if(isset($_GET['id_action'])) {
				echo '</div>';	//chiudo azioni-2
				
				}
				
					
					if(isset($_GET['id_action'])) {
						echo '<div class="yetii-azioni" id="azioni-3">';
					
					$first = Customer::HierarchyFirst($azione['id_action'], 'A');
					
					echo '<div class="tree">';
					echo '<ul class="tree-menu"><li>'.Customer::trovaSiglaLinkPerAlbero(substr($first,1), substr($first,0,1),'').'';
					echo str_replace('<ul></ul>','',Customer::HierarchyFindChildren($first, $children, 'A'.$azione['id_action']));
					echo '</li></ul>';
					echo '</div>';
					echo '<div style="clear:both"></div><br /><br />';
					
					
						echo '</div>'; // chiudo azioni-3
					}	
					
					
					echo '<div class="yetii-azioni" id="azioni-4">';
				
					$storico_ticket = Db::getInstance()->executeS('SELECT * FROM storico_attivita WHERE id_attivita = '.$azione['id_action'].' AND tipo_attivita = "A" ORDER BY data_attivita DESC, desc_attivita DESC');
					
					if(sizeof($storico_ticket) > 0)
					{
						echo '<table class="table">';
						echo '<thead><tr><th>Persona</th><th>Azione</th><th>Data</th></tr></thead><tbody>';
						
						foreach($storico_ticket as $storico)
						{
							if(is_numeric(substr($storico['desc_attivita'], -1)))
							{	
								$employee = filter_var($storico['desc_attivita'], FILTER_SANITIZE_NUMBER_INT);
								$name_employee = Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM employee WHERE id_employee = '.$employee);
								$desc_attivita = 'Ha messo l\'attivit&agrave; in carico a '.$name_employee;
							}
							else
								$desc_attivita = $storico['desc_attivita'];
							
							echo '<tr><td>'.($storico['id_employee'] == 0 ? 'Cliente' : Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM employee WHERE id_employee = '.$storico['id_employee'])).'</td><td>'.$desc_attivita.'</td><td>'.Tools::displayDate($storico['data_attivita'],$cookie->id_lang, true).'</td></tr>'; 
							
						}	
						
						echo '</tbody></table><br /><br />';
					}
						echo '</div>'; // chiudo azioni-4
					
				
				}
				
				}
				
					
				}
				
				
				
				echo '<br /><br /><a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=10" class="button"><img src="../img/admin/arrow-left.png" alt="Back" title="Back" />&nbsp;&nbsp;&nbsp;Torna alla lista delle azioni</a>';
				echo '</div>';
			}	
		}
		else { }
		// FINE AZIONI!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 11) {
		// INIZIO PERSONEEEEEEEEEEEEEEEEEEEEEEEE
		echo '<div class="tab1" id="persone-customer">';
		
		echo '<h2>Persone</h2>';
		
		if(isset($_POST['cancella_persona'])) {
		
			Db::getInstance()->getValue("DELETE FROM persone WHERE id_persona = ".$_POST['id_persona']."");
		
		}
		
		if(isset($_POST['submitpersone'])) {
		
			foreach ($_POST['persona_nuovo'] as $id_persona=>$persona_id) {
		
				if($_POST['persona_nuovo'][$id_persona] == 0) {
					Db::getInstance()->executeS("UPDATE persone SET firstname = '".$_POST['persona_firstname'][$id_persona]."', lastname = '".$_POST['persona_lastname'][$id_persona]."', role = '".$_POST['persona_role'][$id_persona]."', phone = '".$_POST['persona_phone'][$id_persona]."', phone_2 = '".$_POST['persona_phone_2'][$id_persona]."', phone_mobile = '".$_POST['persona_phone_mobile'][$id_persona]."', email = '".$_POST['persona_email'][$id_persona]."' WHERE id_persona = ".$id_persona.""); 
				}
				
				else {
					
					Db::getInstance()->executeS("INSERT INTO persone
					(id_persona,
					firstname,
					lastname,
					role,
					email,
					email_aziendale,
					phone,
					phone_2,
					phone_mobile,
					id_customer,
					tax_code,
					vat_number)
					
					VALUES (
					NULL,
					'".addslashes($_POST['persona_firstname'][$id_persona])."',
					'".addslashes($_POST['persona_lastname'][$id_persona])."',
					'".addslashes($_POST['persona_role'][$id_persona])."',
					'".addslashes($_POST['persona_email'][$id_persona])."',
					'".addslashes(trim($customer->email))."',
					'".addslashes($_POST['persona_phone'][$id_persona])."',
					'".addslashes($_POST['persona_phone_2'][$id_persona])."',
					'".addslashes($_POST['persona_phone_mobile'][$id_persona])."',
					'".$customer->id."',
					'".addslashes(trim($customer->tax_code))."',
					'".addslashes(trim($customer->vat_number))."'
					)");
					
				}
			}
		}
		
		$conta_persone = Db::getInstance()->getValue("
			SELECT count(*) 
			FROM persone 
			WHERE vat_number = '".$customer->vat_number."'
		");
	
		if($conta_persone == 0) {
			echo "Non ci sono persone abbinate a questo cliente<br /><br />";
		}
		
		echo '<form action="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&modificapersone&token='.$this->token.'&tab-container-1=11" method="post">';
		echo '<script type="text/javascript">
		function add_persone() {
			var possible = "0123456789";
			var randomid = "";
			for( var i=0; i < 11; i++ ) {
				randomid += possible.charAt(Math.floor(Math.random() * possible.length));
			}
					
			$("<tr id=\'tr_persona_"+randomid+"\'><td><input type=\'hidden\' name=\'persona_nuovo[]\' value=\'1\' /></td><td><input type=\'text\' size=\'15\' name=\'persona_firstname[]\' /></td><td><input type=\'text\' size=\'15\' name=\'persona_lastname[]\' /></td><td><input type=\'text\' size=\'20\' name=\'persona_role[]\' /></td><td><input type=\'text\' size=\'10\' name=\'persona_phone[]\' /></td><td><input type=\'text\' size=\'10\' name=\'persona_phone_2[]\' /></td><td><input type=\'text\' size=\'10\' name=\'persona_phone_mobile[]\' /></td><td><input type=\'text\' size=\'10\' name=\'persona_email[]\' /></td><td><a href=\'#\' onclick=\'togli_riga("+randomid+");\'><img src=\'../img/admin/delete.gif\' alt=\'Cancella\' title=\'Cancella\' style=\'cursor:pointer\' /></a></td></tr>").appendTo("#tablePersoneBody");

		}
			
		function togli_riga(id) {
			document.getElementById(\'tr_persona_\'+id).innerHTML = "";
		}
			
			
		function cancella_persona(id) {
			$.ajax({
				  url:"'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&cancellapersone&token='.$this->token.'",
				  type: "POST",
				  data: { 
				  cancella_persona: \'y\',
				  id_persona: id
				  },
				  success:function(){
						 
				  },
				  error: function(xhr,stato,errori){
					 alert("Errore nella cancellazione:"+xhr.status);
				  }
			});
		
		}
			
			
		</script>
		';
		
		
		if(!(($cookie->profile == 7 && $agente_c == $cookie->id_employee) || $cookie->profile != 7)){

		echo "
			<script type='text/javascript'>
			$(document).ready(function(){
				$('#tablePersoneBody :input').attr('readonly', 'readonly');
				
			});
			// end
			</script>
			";
		}
		echo '<table class="table" style="width:900px" id="tablePersone">';
		echo '<thead>';
		echo '<tr>';
		echo '<th style="width:50px">ID pers.</th><th style="width:110px">Nome</th><th style="width:110px">Cognome</th><th style="width:110px">Ruolo</th><th style="width:150px">Telefono 1</th><th style="width:150px">Telefono 2</th><th style="width:150px">Cellulare</th><th style="width:120px">Email</th><th></th><th></th>';
		echo '</tr>';
		echo '</thead>';
		echo '<tbody id="tablePersoneBody">';
		
		
		$persone = Db::getInstance()->executeS("SELECT * FROM persone WHERE id_customer = '".$_GET['id_customer']."'");
			
		foreach($persone as $persona) {
			
			$no_phone = 0;
			$no_email = 0;
			
			$customer_phone = Db::getInstance()->getRow("SELECT	phone, phone_mobile FROM address WHERE id_customer = ".$customer->id." AND deleted = 0 AND fatturazione = 1 AND active = 1");
							
			if($customer_phone['phone'] == '') { $telefono_cliente = $customer_phone['phone_mobile']; } else { $telefono_cliente = $customer_phone['phone']; }
							
			if(empty($persona['phone']) && empty($persona['phone_2']) && empty($persona['phone_mobile']))
			{
				$no_phone = 1; 
				$persona['phone'] = $telefono_cliente;
			}	
			if(empty($persona['email'])) {
				$no_email = 1; 
				$persona['email'] = $customer->email;
			}
			
			echo '<tr id="tr_persona_'.$persona['id_persona'].'">';
			echo '<td><input type="hidden" name="persona_nuovo['.$persona['id_persona'].']" value="0" /> '.$persona['id_persona'].'</td>';
			echo '<td><input type="text" size="15" title="'.$persona['firstname'].'" name="persona_firstname['.$persona['id_persona'].']" value="'.$persona['firstname'].'" /></td>';
			echo '<td><input type="text" size="15" title="'.$persona['lastname'].'" name="persona_lastname['.$persona['id_persona'].']" value="'.$persona['lastname'].'" /></td>';
			echo '<td><input type="text" size="10" title="'.$persona['role'].'" name="persona_role['.$persona['id_persona'].']" value="'.$persona['role'].'" /></td>';
			echo '<td><input type="text" onkeyup="this.value = this.value.replace(/\s/g,\'\');" title="'.$persona['phone'].'" size="10" '.($no_phone == 1 ? 'style="font-style:italic"' : '').' name="persona_phone['.$persona['id_persona'].']" value="'.$persona['phone'].'" />  '.($persona['phone'] != '' ? '<a onclick="window.onbeforeunload = null" href="callto:'.$persona['phone'].'"><img src="../img/admin/phone.gif" alt="Chiama" title="Chiama" /></a>' : '').'</td>';
			echo '<td><input type="text" onkeyup="this.value = this.value.replace(/\s/g,\'\');" title="'.$persona['phone_2'].'" size="10" name="persona_phone_2['.$persona['id_persona'].']" value="'.$persona['phone_2'].'" /> '.($persona['phone_2'] != '' ? '<a onclick="window.onbeforeunload = null" href="callto:'.$persona['phone_2'].'"><img src="../img/admin/phone.gif" alt="Chiama" title="Chiama" /></a>' : '').'</td>';
			echo '<td><input type="text" onkeyup="this.value = this.value.replace(/\s/g,\'\');" title="'.$persona['phone_mobile'].'" size="10" name="persona_phone_mobile['.$persona['id_persona'].']" value="'.$persona['phone_mobile'].'" /> '.($persona['phone_mobile'] != '' ? ' <a onclick="window.onbeforeunload = null" href="callto:'.$persona['phone_mobile'].'"><img src="../img/admin/phone.gif" alt="Chiama" title="Chiama" /></a>' : '').'</td>';
			echo '<td><input type="text"  title="'.$persona['email'].'" size="10" '.($no_email == 1 ? 'style="font-style:italic"' : '').' name="persona_email['.$persona['id_persona'].']" value="'.$persona['email'].'" /></td>';
			echo '<td>'.($persona['email'] != '' ? '<a id="invio_mail_outlook_t" href="mailto:'.$persona['email'].'"><img src="../img/admin/outlook.gif" alt="Invia mail con Outlook" title="Invia mail con Outlook a:" style="cursor:pointer" /></a>'.(($cookie->profile == 7 && $agente_c == $cookie->id_employee) || $cookie->profile != 7 ? '&nbsp;&nbsp;&nbsp;<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewmessage&aprinuovomessaggio&id_persona='.$persona['id_persona'].'&token='.$this->token.'&tab-container-1=7"><img src="../img/admin/email.gif" alt="Apri nuovo messaggio" title="Apri nuovo messaggio" style="cursor:pointer" /></a>' : '') : '').'</td>';
			echo '<td>'.(($cookie->profile == 7 && $agente_c == $cookie->id_employee) || $cookie->profile != 7 ? ''.($cookie->profile != 8 ? '<a href="#" onclick="javascript:var surec=window.confirm(\'Sei sicuro?\'); if (surec) { togli_riga('.$persona['id_persona'].'); cancella_persona('.$persona['id_persona'].'); } else { return false; }; "><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" style="cursor:pointer" /></a>' : '') : '--').'</td>';
			echo '</tr>';
				
			
		}

		echo '</tbody>';
		echo "</table>";
		echo '<br />
		'.(($cookie->profile == 7 && $agente_c == $cookie->id_employee) || $cookie->profile != 7 ? '<a href="#" onclick="javascript:add_persone()";><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" /> Clicca per aggiungere una persona</a> <br /><br />
		<button type="submit" class="button" name="submitpersone">
			<img src="../img/admin/save.gif" alt="Salva" title="Salva" />&nbsp;&nbsp;&nbsp;'.$this->l('   Save   ').'
		</button>' : '');
		echo "</form>";
		
		
		
		
		
		
		echo '</div>';
		} 
		
		if(Tools::getIsset('cancellapersone') && Tools::getValue('cancella_persona') == 'y') {
			Db::getInstance()->executeS("DELETE FROM persone WHERE id_persona = ".Tools::getValue('id_persona')."");
		
		}
		// FINE PERSONEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 12) {
			// INIZIO DOCUMENTI
			
			if($cookie->profile == 7)
				echo 'Non hai i permessi per visualizzare questa sezione';
			else
			{
				echo '<div class="tab1" id="documenti-customer">';
				
				
				
				
				if(Tools::getIsset('createdocsdir')) {
					$old_umask = umask(0);
					
					if($customer->is_company == 1) {
						$nuova_cartella_documenti = $this->sanitize($customer->company." ".$customer->id);
					}
					else {
						$nuova_cartella_documenti = $this->sanitize($customer->firstname." ".$customer->lastname." ".$customer->id);
					}
					
					Db::getInstance()->executeS("UPDATE customer SET cartella_documenti = '".$nuova_cartella_documenti."' WHERE id_customer = ".$customer->id."");
					mkdir('../documenti-clienti/'.$nuova_cartella_documenti,0777);
					
					umask($old_umask);
					echo '<div class="conf confirm">Cartella documenti creata con successo!</div>
					';
				}
				
				if(Tools::getIsset('delete_documento')) {
				
					foreach($_POST['selectdocfile'] as $selectedfile) {
					
						if(Tools::getIsset('current_dir')) {
							$entire_dir = Tools::getValue('current_dir');
						}
						else {
							$entire_dir = $directory_base;
						}
						
						if(is_file($entire_dir.'/'.$selectedfile)) {
						
							unlink($entire_dir.'/'.$selectedfile);
						}
						else if(is_dir($entire_dir.'/'.$selectedfile)) {
							$this->deleteDir($entire_dir.'/'.$selectedfile);
						}
					}
				
					echo '<div class="conf confirm">Documenti eliminati con successo</div>';
					
					Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=12&gotodir='.Tools::getValue('current_dir').'');
				
				}
				
				else if(Tools::getIsset('move_documento') || Tools::getIsset('copy_documento') ) {
				
					foreach($_POST['selectdocfile'] as $selectedfile) {
					
						if(Tools::getIsset('current_dir')) {
							$entire_dir = Tools::getValue('current_dir');
						}
						else {
							$entire_dir = $directory_base;
						}
						
						if(Tools::getIsset('move_documento')) {
						rename($entire_dir.'/'.$selectedfile, Tools::getValue('sposta_in')."/".$selectedfile);
						}
						else if(Tools::getIsset('copy_documento')) {
						copy($entire_dir.'/'.$selectedfile, Tools::getValue('sposta_in')."/".$selectedfile);
						}
					}
				
					echo '<div class="conf confirm">Documenti eliminati con successo</div>';
					
					Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=12&gotodir='.Tools::getValue('current_dir').'');
				
				}
				
				
				
				if(Tools::getIsset('formdocumentssubmitted')) {
				
					
					if(!empty($_FILES['aggiungiFile'])) {
					
						$docfiles=array();
						$docfdata=$_FILES['aggiungiFile'];
						if(is_array($docfdata['name'])){
							$count_docfname = count($docfdata['name']);
							for($i=0;$i<$count_docfname;++$i){
								$docfiles[]=array(
									'name'    =>$docfdata['name'][$i],
									'tmp_name'=>$docfdata['tmp_name'][$i],
									'type' => $docfdata['type'][$i],
									'size' => $docfdata['size'][$i],
									'error' => $docfdata['error'][$i],
								 
								);
							}
						}
						else 
							$docfiles[]=$docfdata;
											
						$attachments = array();
						
						foreach($docfiles as $docfile) {
						
							if (isset($docfiles) AND !empty($docfile['name']) AND $docfile['error'] != 0)
								$this->_errors[] = Tools::displayError('An error occurred with the file upload.');
								
								if (!empty($docfile['name'])) {
									rename($docfile['tmp_name'], Tools::getValue('cartella').'/'.$docfile['name']); 
										
								}
														
						}			
						
						echo '<div class="conf confirm">File aggiunti con successo</div>';
					}
					else {
						echo 'Nessun file selezionato';
					}
					
					echo '';
					
				}
				
				echo '<div style="float:left"><h2>Documenti</h2></div>';
				
				$cartella_documenti = Db::getInstance()->getValue("SELECT cartella_documenti FROM customer WHERE id_customer = ".Tools::getValue('id_customer')."");
				
				
				if($cartella_documenti != "") {
				
					$filelist = array();
					$dirlist = array();
					
					$directory_base = '../documenti-clienti/'.$cartella_documenti;
					
					
					
					if(Tools::getIsset('gotodir')) {
						$entire_dir = Tools::getValue('gotodir');
					}
					else {
						$entire_dir = $directory_base;
					}
					
					if(substr($entire_dir,0,12) != '../documenti') {
						echo "ERRORE"; die();
					
					}
					
					if ($handle = opendir($entire_dir)) {
					
						
						while ($dir = readdir($handle)) {
						
							if(is_dir($entire_dir.'/'.$dir) && $dir != "." && $dir != "..") { $dirlist[]=$dir; }
							if(is_file($entire_dir.'/'.$dir)) { $filelist[]=$dir; }
							
						}
						
						if(Tools::getIsset('formcartellasubmitted')) {
				
							foreach($dirlist as $entry) {
								if($entry == Tools::getValue('nome_cartella')) {
									$this->_errors[] = Tools::displayError('Esiste gia una cartella con questo nome.');
									break;
								}
							}
							
							$old_umask = umask(0);
							mkdir(Tools::getValue('cartella')."/".Tools::getValue('nome_cartella'),0777);
							umask($old_umask);
							Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=12&gotodir='.Tools::getValue('cartella').'');
							
							
						}
						
						$cartelle_count = count($dirlist);
						$file_count = count($filelist);
						
						$count_docs = $cartelle_count + $file_count;
						
						echo '<link rel="stylesheet" type="text/css" href="explorer.css" />';
						
						if($count_docs == 0) {
							echo "<div style='float:left; margin-left:20px; margin-top:5px;'>Non sono presenti file n&egrave; sottocartelle in questa cartella.</div><div style='clear:both'></div>";
						} else {
							echo "<div style='float:left; margin-left:20px; margin-top:5px;'>Sono presenti ".$file_count." file e ".$cartelle_count." sottocartelle in questa cartella.</div><div style='clear:both'></div>";
						}
						//echo "Ti trovi in: ".$entire_dir;
						echo '<form name="submit_delete_move_documento" id="submit_delete_documento" action="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=12&deleteselection" method="post" class="std" enctype="multipart/form-data">
							<input type="hidden" name="current_dir" value="'.(Tools::getIsset('gotodir') ? Tools::getValue('gotodir') : $directory_base).'" />
							';
						echo "
						<table class='table' id='explorer'>
						<tr><th style='width:400px'>File</th><th style='width:150px'>Dimensioni</th><th style='width:150px'>Formato</th><th style='width:150px'>Ultima modifica</th><th style='width:50px'>Seleziona</th></tr>";
						
						if($count_docs == 0 && (!Tools::getIsset('gotodir') || Tools::getValue('gotodir') == $directory_base)) {
							echo "<tr><td></td><td></td><td></td><td></td><td></td></td></tr></table>";
						} else {
						
						}
							
						if(Tools::getIsset('gotodir') && Tools::getValue('gotodir') != $directory_base) {
							echo '
								<tr>
								<td class="icon parent" style="padding-left:23px; height:15px;" nowrap="nowrap">
								<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=12&gotodir='.(Tools::getIsset('gotodir') ? dirname(Tools::getValue('gotodir')) : $directory_base).'">'.'Livello superiore'."</a>
								</td>
								<td>
									
								</td>
								<td>
								</td>
								<td>
								</td>
								<td style='text-align:center'>
								</td>
								</tr>
								";	
						}
							
						foreach($dirlist as $entry) {
							$pathinfo = pathinfo($entire_dir.'/'.$entry);
							if($entry == '.' || $entry == '..') { }
							else {
								echo '
								<tr>
								<td class="icon folder" style="padding-left:23px; height:15px;" nowrap="nowrap">
								<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=12&gotodir='.(Tools::getIsset('gotodir') ? Tools::getValue('gotodir')."/".$entry : $directory_base."/".$entry).'">'.$entry."</a>
								</td>
								<td>
								
								</td>
								<td>
								&laquo;CARTELLA&raquo;
								</td>
								<td>
								".date ("d/m/Y H:i:s",filemtime($entire_dir.'/'.$entry))."
								</td>
								<td style='text-align:center'>
								<input type='checkbox' name='selectdocfile[]' value='".$entry."' />
								</td>
								</tr>
								";
							}						
						}
						
						foreach($filelist as $entry) {
							echo is_file($entry);
							if ($entry != "." && $entry != "..") {
								
								chmod('/var/www/vhosts/ezdirect.it/httpdocs/documenti-clienti/'.$entire_dir.'/'.$entry, 0644);
								
								$pathinfo = pathinfo('../documenti-clienti/'.$cartella_documenti.'/'.$entry);
								echo '
								<tr>
								<td class="icon '.substr($pathinfo['extension'],0).'" style="padding-left:23px;" nowrap="nowrap">
								<a target="_blank" href="https://ezdocs:WRY753MNR43ASD147g!@www.ezdirect.it/documenti-clienti/'.$entire_dir.'/'.$entry.'">'.$entry."</a>
								</td>
								<td>
								".$this->FileSizeConvert(filesize($entire_dir.'/'.$entry))."
								</td>
								<td>
								".$pathinfo['extension']."
								</td>
								<td>
								".date ("d/m/Y H:i:s",filemtime($entire_dir.'/'.$entry))."
								</td>
								<td style='text-align:center'>
								<input type='checkbox' name='selectdocfile[]' value='".$entry."' />
								</td>
								</tr>
								";
							}
							
						}
						$directories = $this->ListIn($directory_base);
							
						echo "</table><br />";
						
						if($count_docs == 0) {
							
						} else {
						
						
						
							echo "
							<button type='submit' class='button' name='delete_documento' value='Elimina selezione' onclick=\"javascript: var surec=window.confirm('Sei sicuro? I file, le cartelle e il contenuto delle cartelle che hai selezionato, saranno cancellati in modo permanente!!!'); if (surec) { return true; } else { return false; }\">
							<img src='../img/admin/delete.gif' alt='Elimina selezione' title='Elimina selezione' />&nbsp;&nbsp;&nbsp;Elimina selezione
							</button>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							Sposta o copia documenti selezionati in: <select name='sposta_in'>"; 
							if(!Tools::getIsset('gotodir')) {
							}
							else {
								echo "<option value='$directory_base'>Cartella base del cliente</option>";
							}
							foreach($directories as $dir) {
								if($directory_base."/".$dir == Tools::getValue('gotodir')) {
								}
								else {
									echo "<option value='$directory_base/$dir'>$dir</option>";
								}
							}
								
							echo "</select>
							<button type='submit' class='button' name='move_documento'>
								<img src='../img/admin/move.gif' alt='Sposta' title='Sposta' />Sposta
							</button>
							
							<button type='submit' class='button' name='copy_documento'>
								<img src='../img/admin/copy_files.gif' alt='Copia' title='Copia' />Copia
							</button>
							
							
							";
						}
						
						echo "
						</form>";
							
							
							
							
						
						closedir($handle);
					}

			
					echo '
					<hr />
					<div style="width:400px; display:block; float:left">
					<form name="submit_documento" id="submit_documento" action="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=12&formdocumentssubmitted&gotodir='.(Tools::getIsset('gotodir') ? Tools::getValue('gotodir') : $directory_base).'" method="post" class="std" enctype="multipart/form-data">
					<strong>Inserisci nuovo documento</strong><br />
					<div style="display:block;float:left">
					<input name="MAX_FILE_SIZE" value="200000000" type="hidden">
					<input name="cartella" value="'.$entire_dir.'" type="hidden">
								<input name="aggiungiFile[]" style="width:300px" id="aggiungiFile" type="file" onchange="handleFileSelect(this.files)" multiple>
								<br />
								<!-- <output id="list-tkt">Anteprime: </output> -->
									<script type="text/javascript">
								function handleFileSelect(files) {
								
									// Loop through the FileList and render image files as thumbnails.
									 for (var i = 0; i < files.length; i++) {
										var f = files[i];
										var name = files[i].name;
			  
										var reader = new FileReader();  
										reader.onload = function(e) {  
										  // Render thumbnail.
										  var span = document.createElement("span");
										  span.innerHTML = ["<a href=\'",e.target.result,"\' target=\'_blank\'>",name,"</a> - "].join("");
										  document.getElementById("list-tkt").insertBefore(span, null);
										};

									  // Read in the image file as a data URL.
									  reader.readAsDataURL(f);
									}
								  }
								 
								</script>
								</div>
					<input type="submit" value="OK" style="display:block;float:left" class="button" />
					
					<div style="clear:both"></div>
					</form>
					</div>	
					<div style="width:400px; display:block; float:left">
					<form name="submit_cartella" id="submit_cartella" action="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=12&formcartellasubmitted&gotodir='.(Tools::getIsset('gotodir') ? Tools::getValue('gotodir') : $directory_base).'" method="post" class="std" enctype="multipart/form-data">
					<strong>Crea nuova sottocartella</strong><br />
					<input name="cartella" value="'.$entire_dir.'" type="hidden">
					Inserire nome: <input name="nome_cartella" value="" type="text">
					
					<input type="submit" value="OK"  class="button" />
					</form>
					</div>	
					<div style="clear:both"></div>
								';
				}
				else {
					echo 'Non esiste una cartella documenti associata a questa anagrafica.
					<br /><br />
					<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&createdocsdir&token='.$this->token.'&tab-container-1=12" class="button">Clicca qui se vuoi crearla.</a><br /><br />';
					
					
				}
		
			}
		} 
		else { }
		
		//FINE DOCUMENTI
		
		// INIZIO CONTRATTI
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 13) {
			
			if($cookie->profile == 7)
				echo 'Non hai i permessi per visualizzare questa sezione';
			else
			{
				echo '<div class="tab1" id="contratti-customer">';
				echo '<div style="float:left"><h2>Contratti assistenza</h2></div>';
				
				if(Tools::getIsset('nuovocontratto') > 0 || Tools::getIsset('edit_contratto') > 0 ) {
				
					if(Tools::getValue('submitted') == 'y') {
						if(Tools::getIsset('blocco_amministrativo')) {
							$blocco_amministrativo = 1;
						}
						else {
							$blocco_amministrativo = 0;
						}
						$data_registrazione = date("Y-m-d H:i:s", strtotime(Tools::getValue('data_registrazione')));
						$data_inizio = date("Y-m-d H:i:s", strtotime(Tools::getValue('data_inizio')));
						$data_fine = date("Y-m-d H:i:s", strtotime(Tools::getValue('data_fine')));
						
						if(Tools::getIsset('edit_contratto')) {
							
							$dati_contratto = Db::getInstance()->getRow('SELECT * FROM contratto_assistenza WHERE id_contratto = '.Tools::getValue('edit_contratto'));
							
							switch($storico_contratto['tipo']) {
								case 1: $tipo_s = 'Base'; break;
								case 2: $tipo_s = 'Top'; break;
								case 3: $tipo_s = 'Gold'; break;
								case 4: $tipo_s = 'Teleassistenza'; break;
								case 5: $tipo_s = 'Noleggio'; break;
								default: $tipo_s = 'Base'; break;
							}
							
							switch($storico_contratto['status']) {
								case 0: $status_s = 'Attivo'; break;
								case 1: $status_s = 'Sospeso'; break;
								case 2: $status_s = 'Cancellato'; break;
								case 3: $status_s = 'Disdetto'; break;
								case 4: $status_s = 'Scaduto'; break;
								default: $status_s = 'Attivo'; break;
							}
							
							$storico_contratto = '
							<table class=\'table\'><tr><th>Rif. fattura</th><th>Rif. ordine</th><th>Rif. noleggio</th><th>Tipo</th><th>Status</th><th>Prezzo</th><th>Pagamento</th><th>CIG</th><th>CUP</th><th>IPA</th><th>Data reg.</th><th>Inizio</th><th>Fine</th></tr><tr><td>'.$dati_contratto['rif_fattura'].'</td><td>'.$dati_contratto['rif_ordine'].'</td><td>'.$dati_contratto['rif_noleggio'].'</td><td>'.$tipo_s.'</td><td>'.$status_s.'</td><td>'.$dati_contratto['prezzo'].'</td><td>'.$dati_contratto['pagamento'].'</td><td>'.$dati_contratto['cig'].'</td><td>'.$dati_contratto['cup'].'</td><td>'.$dati_contratto['univoco'].'</td><td>'.date('d/m/Y',strtotime($dati_contratto['data_registrazione'])).'</td><td>'.date('d/m/Y',strtotime($dati_contratto['data_inizio'])).'</td><td>'.date('d/m/Y',strtotime($dati_contratto['data_fine'])).'</td></tr></table>';
							
							Customer::Storico(Tools::getValue('id_contratto'), 'CO', $cookie->id_employee, $storico_contratto);
							
							Db::getInstance()->executeS("UPDATE contratto_assistenza SET status = '".Tools::getValue('status')."', tipo = '".Tools::getValue('tipo')."', data_registrazione = '".$data_registrazione."', data_inizio = '".$data_inizio."', data_fine = '".$data_fine."', indirizzo = '".Tools::getValue('indirizzo_contratto')."',  cig = '".Tools::getValue('cig')."',  cup = '".Tools::getValue('cup')."', univoco = '".Tools::getValue('univoco')."', prezzo = '".str_replace(",",".",Tools::getValue('totale_importo'))."',  periodicita = '".Tools::getValue('periodicita')."', competenza_dal = '".Tools::getValue('competenza_dal')."', competenza_al = '".Tools::getValue('competenza_al')."', cadenza = '".Tools::getValue('cadenza')."', pagamento = '".Tools::getValue('pagamento')."', blocco_amministrativo = '".$blocco_amministrativo."', rif_fattura = '".Tools::getValue('rif_fattura')."', pagato = '".(Tools::getIsset('pagato') ? 1 : 0)."', fatturato = '".(Tools::getIsset('fatturato') ? 1 : 0)."', rif_noleggio = '".Tools::getValue('rif_noleggio')."', rif_ordine = '".Tools::getValue('rif_ordine')."', note_private = '".addslashes(Tools::getValue('note_private'))."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_contratto = '".Tools::getValue('id_contratto')."'");
						
							Db::getInstance()->executeS("DELETE FROM contratto_assistenza_prodotti WHERE id_contratto = '".Tools::getValue('edit_contratto')."'");
							
						} else {
							$codice_spring = Db::getInstance()->getValue("SELECT codice_spring FROM customer WHERE id_customer = ".Tools::getValue('id_customer')."");
					
							
							Db::getInstance()->executeS("INSERT INTO contratto_assistenza (id_contratto, id_customer, codice_spring, status, tipo, descrizione, data_registrazione, data_inizio, data_fine, cig, cup, univoco, indirizzo, prezzo, periodicita, competenza_dal, competenza_al, cadenza, pagamento, blocco_amministrativo, rif_fattura, rif_noleggio, rif_ordine, note_private, date_add, date_upd) VALUES ('".Tools::getValue('id_contratto')."', '".Tools::getValue('id_customer')."', '".$codice_spring."', '".Tools::getValue('status')."', '".Tools::getValue('tipo')."', '', '".$data_registrazione."', '".$data_inizio."', '".$data_fine."', '".Tools::getValue('cig')."', '".Tools::getValue('cup')."', '".Tools::getValue('univoco')."', '".Tools::getValue('indirizzo_contratto')."', '".str_replace(",",".",Tools::getValue('totale_importo'))."', '".Tools::getValue('periodicita')."', '".$data_inizio."', '".$data_fine."', '".Tools::getValue('cadenza')."', '".Tools::getValue('pagamento')."', '".$blocco_amministrativo."', '".Tools::getValue('rif_fattura')."', '".Tools::getValue('rif_noleggio')."', '".Tools::getValue('rif_ordine')."', '".addslashes(Tools::getValue('note_private'))."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
						
						}
						
						Db::getInstance()->executeS("DELETE FROM contratto_assistenza_prodotti WHERE id_contratto = '".Tools::getValue('id_contratto')."'");
						
						foreach($_POST['prodotto'] as $id_product_g=>$id_product) {
						
							
							$codice_prodotto = Db::getInstance()->getValue("SELECT reference FROM product WHERE id_product = ".$id_product_g."");
							$descrizione_prodotto = Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_lang = ".$cookie->id_lang." AND id_product = ".$id_product_g."");
							
							
							
							Db::getInstance()->executeS("INSERT INTO contratto_assistenza_prodotti (id_contratto, id_product, codice_prodotto, descrizione_prodotto, quantita, prezzo, seriale, note, indirizzo, date_add, date_upd) VALUES ('".Tools::getValue('id_contratto')."', ".$id_product_g.", '".$codice_prodotto."', '".$_POST['prodotto_descrizione'][$id_product_g]."', '".$_POST['quantita'][$id_product_g]."', '".str_replace(",",".",$_POST['prezzo_prodotto'][$id_product_g])."', '".$_POST['seriale_prodotto'][$id_product_g]."', '', '".$_POST['indirizzo_prodotto'][$id_product_g]."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
							
							
						}
						
						
						
						if((Tools::getIsset('invio_contabilita') && Tools::getValue('invio_contabilita') == 1 && !Tools::getIsset('fatturato')) || Tools::getValue('rinnova_contratto_ok') == 1)
						{
							if(Tools::getValue('rinnova_contratto_ok') == 1)
								Db::getInstance()->executeS('UPDATE contratto_assistenza SET status = 0 WHERE id_contratto = '.Tools::getValue('edit_contratto'));
							
							$contratto = Db::getInstance()->getRow("SELECT c.id_customer, c.firstname, c.lastname, c.company, c.is_company, c.email, co.data_fine, co.id_contratto, co.status, co.tipo, co.descrizione, co.prezzo FROM customer c
							JOIN contratto_assistenza co ON co.id_customer = c.id_customer
							WHERE 
							co.id_contratto = '".Tools::getValue('edit_contratto')."'");

							$indirizzo = Db::getInstance()->getValue('SELECT id_address FROM address WHERE id_customer = '.$contratto['id_customer'].' AND active = 1 AND deleted = 0 AND fatturazione = 1');
							
							if(Tools::getValue('rinnova_contratto_ok') == 1)
							{	
								Db::getInstance()->executeS('INSERT INTO cart (id_cart, id_lang, id_currency, id_customer, id_address_invoice, id_address_delivery, name, cig, cup, ipa, validita, date_add, date_upd, created_by,  id_employee, in_carico_a, preventivo, note) VALUES (NULL, 5, 1, "'.$contratto['id_customer'].'", "'.$indirizzo.'", "'.$indirizzo.'", "Rinnovo contratto", "'.$_POST['cig'].'", "'.$_POST['cup'].'", "'.$_POST['univoco'].'", "'.$contratto['data_fine'].'", "'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'", 1,1,1,1, "'.$nota_ta.'")');

								$id_cart = mysqli_insert_id($link_glob);
								
								$prodotti_per_carrello = Db::getInstance()->executeS('SELECT id_product, prezzo, quantita FROM contratto_assistenza_prodotti WHERE id_contratto = '.$contratto['id_contratto'].'');
								
								$prezzo_prod = $contratto['prezzo'];
								$prezzo_acquisto = (($prezzo_prod/100) * 60);
								
								Db::getInstance()->executeS('INSERT INTO cart_product (id_cart, id_product, quantity, price, free, prezzo_acquisto, date_add) VALUES ('.$id_cart.', 336198, "1", "'.$prezzo_prod.'", "0", "'.$prezzo_acquisto.'",  "'.date('Y-m-d H:i:s').'")') ;
								
								$id_cart_ta = $id_cart;
								
								include("../modules/pss_clearcarts/pss_clearcarts.php");
								include("../modules/pss_clearcarts/AdminPssClearCarts.php");
								$apcc = new AdminPssClearCarts();
								
								$apcc->orderThisCart($id_cart_ta);
								$order = Db::getInstance()->getValue('SELECT id_order FROM orders WHERE id_cart = '.$id_cart_ta.'');
							}
							else
							{
								if(Tools::getIsset('nuovocontratto'))
								{
									$contratto['id_customer'] = Tools::getValue('id_customer');
									$contratto['id_contratto'] = Tools::getValue('id_contratto');
								}	
								Db::getInstance()->executeS('INSERT INTO cart (id_cart, id_lang, id_currency, id_customer, id_address_invoice, id_address_delivery, name, cig, cup, ipa, validita, date_add, date_upd, created_by,  id_employee, in_carico_a, preventivo, note) VALUES (NULL, 5, 1, "'.$contratto['id_customer'].'", "'.$indirizzo.'", "'.$indirizzo.'", "Contratto N. '.$contratto['id_contratto'].'", "'.$_POST['cig'].'", "'.$_POST['cup'].'", "'.$_POST['univoco'].'", "'.$contratto['data_fine'].'", "'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'", 1,1,1,1, "'.$nota_ta.'")');

								$id_cart = mysqli_insert_id($link_glob);
								
								$prodotti_per_carrello = Db::getInstance()->executeS('SELECT id_product, prezzo, quantita FROM contratto_assistenza_prodotti WHERE id_contratto = '.$contratto['id_contratto'].'');
								
								//$prezzo_prod = $contratto['prezzo'];
								//$prezzo_acquisto = (($prezzo_prod/100) * 60);
								
								foreach($prodotti_per_carrello as $p)
								{
									Db::getInstance()->executeS('INSERT INTO cart_product (id_cart, id_product, quantity, price, free, prezzo_acquisto, date_add) VALUES ('.$id_cart.', '.$p['id_product'].', "'.$p['quantita'].'", "'.$p['prezzo'].'", "0", "0",  "'.date('Y-m-d H:i:s').'")') ;
								}
								$id_cart_ta = $id_cart;
								
								include("../modules/pss_clearcarts/pss_clearcarts.php");
								include("../modules/pss_clearcarts/AdminPssClearCarts.php");
								/*$apcc = new AdminPssClearCarts();
								
								$apcc->orderThisCart($id_cart_ta);
								$order = Db::getInstance()->getValue('SELECT id_order FROM orders WHERE id_cart = '.$id_cart_ta.'');*/
							}
							$subject = "Nuovo contratto da controllare per fatturazione";
			
							$message = "<html>
							<head>
								<title>Nuovo contratto da controllare per fatturazione</title>
							</head>
							<body>
							Controllare <a href='http://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$customer->id."&viewcustomer&edit_contratto=".Tools::getValue('id_contratto')."&tab-container-1=13&token=".Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers')."2")."'>contratto n. ".Tools::getValue('id_contratto')."</a> (ordine n. ".$order.") per fatturazione (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname.' '.$customer->lastname).")
							</body>
							</html>";
									
							$headers  = 'MIME-Version: 1.0' . "\n";
							$headers .= 'Content-Type: text/html' ."\n";
							$headers .= 'From: "Ezdirect" <info@ezdirect.it>' . "\n";
							mail("federico.giannini@mail.com",$subject,$message,$headers );
							
							Db::getInstance()->executeS('UPDATE contratto_assistenza SET invio_contabilita = 1 WHERE id_contratto = '.Tools::getValue('id_contratto'));
						}
						
						Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=13');
					
					}
					else {
						
						echo '<div style="clear:both"></div>';
						
						echo '<div id="tab-container-contratti" class="tab-containers">
						<ul id="tab-container-contratti-nav" class="tab-containers-nav">
						<li><a href="#dati_contratto">Contratto</a></li>
						<li><a href="#storico_contratto">Storico</a></li>
						</ul>

						<div class="tab-contratto" id="dati_contratto">
						';
					
						if(Tools::getIsset('edit_contratto')) {
							echo '<strong>Modifica contratto numero '.Tools::getValue('edit_contratto').'</strong>';
						}
						else {
							echo '<strong>Genera nuovo contratto</strong>';
						}
						if(Tools::getIsset('edit_contratto')) {
						
							echo "<form action='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&edit_contratto=".Tools::getValue('edit_contratto')."&submitted=y&token=".$this->token."&tab-container-1=13' method='post' onsubmit='return checkFormContratto();'>";
							
							$contratto = Db::getInstance()->getRow("SELECT * FROM contratto_assistenza WHERE id_contratto = ".Tools::getValue('edit_contratto')."");
							
						}
						else {
							echo "<form action='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&nuovocontratto&submitted=y&token=".$this->token."&tab-container-1=13' method='post' onsubmit='return checkFormContratto();'>";
							
							$id_contratto = Db::getInstance()->getValue("SELECT id_contratto FROM contratto_assistenza ORDER BY id_contratto DESC");
							$id_contratto++;
						}
									echo '
								
						<div class="anagrafica-cliente" style="width:100px">'.$this->l('N. contratto').' 
					
							<input type="text" size="33" name="id_contratto" id="id_contratto" value="'.(Tools::getIsset('edit_contratto') ? $contratto['id_contratto'] : $id_contratto).'" readonly="readonly" style="width:100px; background-color:#f0ebd6" /> 
						</div>';
						echo '
						<div class="anagrafica-cliente" style="width:100px">Rif. fattura
					
							<input type="text" size="33" name="rif_fattura" id="rif_fattura" value="'.(Tools::getIsset('edit_contratto') ? $contratto['rif_fattura'] : '').'" style="width:100px" /> 
						</div>';
						echo '
						<div class="anagrafica-cliente" style="width:100px">Rif. ordine
					
							<input type="text" size="33" name="rif_ordine" id="rif_ordine" value="'.(Tools::getIsset('edit_contratto') ? $contratto['rif_ordine'] : '').'" style="width:100px" /> 
						</div>';
						
						echo '
						<div class="anagrafica-cliente" style="width:100px">Rif. noleggio 
					
							<input type="text" size="33" name="rif_noleggio" id="rif_noleggio" value="'.(Tools::getIsset('edit_contratto') ? $contratto['rif_noleggio'] : '').'" style="width:100px" /> 
						</div>';
						
							echo '
						<div class="anagrafica-cliente" style="width:100px">Tipo 
					
							<select name="tipo" style="width:100px">
								<option value="1" '.(Tools::getIsset('edit_contratto') ? ($contratto['tipo'] == 1 ? 'selected="selected"' : '') : '').'>Base</option>
								<option value="2" '.(Tools::getIsset('edit_contratto') ? ($contratto['tipo'] == 2 ? 'selected="selected"' : '') : '').'>Top</option>
								<option value="3" '.(Tools::getIsset('edit_contratto') ? ($contratto['tipo'] == 3 ? 'selected="selected"' : '') : '').'>Gold</option>
								<option value="4" '.(Tools::getIsset('edit_contratto') ? ($contratto['tipo'] == 4 ? 'selected="selected"' : '') : '').'>Teleassistenza</option>
								<option value="5" '.(Tools::getIsset('edit_contratto') ? ($contratto['tipo'] == 5 ? 'selected="selected"' : '') : '').'>Noleggio</option>
							</select>
						</div>';
							echo '
						<div class="anagrafica-cliente" style="width:100px">Stato
					
							<select name="status" style="width:100px">
								<option value="0" '.(Tools::getIsset('edit_contratto') ? ($contratto['status'] == 0 ? 'selected="selected"' : '') : '').'>Attivo</option>
								<option value="1" '.(Tools::getIsset('edit_contratto') ? ($contratto['status'] == 1 ? 'selected="selected"' : '') : '').'>Sospeso</option>
								<option value="2" '.(Tools::getIsset('edit_contratto') ? ($contratto['status'] == 2 ? 'selected="selected"' : '') : '').'>Cancellato</option>
								<option value="3" '.(Tools::getIsset('edit_contratto') ? ($contratto['status'] == 3 ? 'selected="selected"' : '') : '').'>Disdetto da cliente</option>
								<option value="4" '.(Tools::getIsset('edit_contratto') ? ($contratto['status'] == 4 ? 'selected="selected"' : '') : '').'>Scaduto</option>
							</select>
						</div>';
						
						if($contratto['status'] == 4)
						{
							echo '
							<script type="text/javascript">
							function rinnova_contratto()
							{
								document.getElementById("rif_fattura").value = "";
								document.getElementById("rif_ordine").value = "";
								document.getElementById("rif_noleggio").value = "";
								document.getElementById("cig").value = "";
								document.getElementById("cup").value = "";
								document.getElementById("univoco").value = "";
								document.getElementById("data_inizio").value = "";
								document.getElementById("data_fine").value = "";
								document.getElementById("cadenza").value = "";
								document.getElementById("competenza_dal").value = "";
								document.getElementById("competenza_al").value = "";
								document.getElementById("rinnova_contratto_ok").value = 1;
								document.getElementById("annulla_rinnova_contratto_button").style.display = "block";
								document.getElementById("rinnova_contratto_button").style.display = "none";
								document.getElementById("invii_controlli").style.display = "none";
							}
							
							function annulla_rinnova_contratto() {
								
								window.location.href = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&edit_contratto='.Tools::getValue('edit_contratto').'&token='.Tools::getValue('token').'&tab-container-1=13";
							}
							</script>
							
							<div class="anagrafica-cliente" style="width:100px; margin-top:5px">
							<div id="rinnova_contratto_button">
							<button type="button" class="button" onclick="rinnova_contratto()">
							<img src="../img/admin/enabled-2.gif" alt="Rinnova" title="Rinnova" />&nbsp;&nbsp;&nbsp;Rinnova
							</button></div></div>
							
							<div id="annulla_rinnova_contratto_button" style="display:none">
							<button type="button" class="button" onclick="annulla_rinnova_contratto()">
							<img src="../img/admin/forbbiden.gif" alt="Annulla Rinnova" title="Annulla Rinnova" />&nbsp;&nbsp;&nbsp;Annulla
							</button></div>
							';
							
						}
						
						echo '<div style="clear:both"></div><br />';
						
						
							/*echo '
						<div class="anagrafica-cliente" style="width:100px">Importo (in &euro;)
					
							<input type="text" size="33" id="prezzo_contratto" name="prezzo_contratto" value="'.(Tools::getIsset('edit_contratto') ? str_replace(".",",",$contratto['prezzo']) : '').'" style="width:100px" onkeyup="calcolaRataContratto()"; />
						</div>';*/
						/*echo '
						<div class="anagrafica-cliente" style="width:100px"  onchange="calcolaRataContratto()";>Periodicit&agrave; (in mesi)
					
							<select name="periodicita" id="periodicita"  style="width:100px">
								<option value="1" '.(Tools::getIsset('edit_contratto') ? ($contratto['periodicita'] == 1 ? 'selected="selected"' : '') : '').'>1</option>
								<option value="2" '.(Tools::getIsset('edit_contratto') ? ($contratto['periodicita'] == 2 ? 'selected="selected"' : '') : '').'>2</option>
								<option value="3" '.(Tools::getIsset('edit_contratto') ? ($contratto['periodicita'] == 3 ? 'selected="selected"' : '') : '').'>3</option>
								<option value="4" '.(Tools::getIsset('edit_contratto') ? ($contratto['periodicita'] == 4 ? 'selected="selected"' : '') : '').'>4</option>
								<option value="6" '.(Tools::getIsset('edit_contratto') ? ($contratto['periodicita'] == 6 ? 'selected="selected"' : '') : '').'>6</option>
								<option value="12" '.(Tools::getIsset('edit_contratto') ? ($contratto['periodicita'] == 12 ? 'selected="selected"' : '') : '').'>12</option>
								<option value="24" '.(Tools::getIsset('edit_contratto') ? ($contratto['periodicita'] == 24 ? 'selected="selected"' : '') : '').'>24</option>
								<option value="36" '.(Tools::getIsset('edit_contratto') ? ($contratto['periodicita'] == 36 ? 'selected="selected"' : '') : '').'>36</option>
								<option value="48" '.(Tools::getIsset('edit_contratto') ? ($contratto['periodicita'] == 48 ? 'selected="selected"' : '') : '').'>48</option>
							</select>
						</div>';*/
						
						includeDatepicker3(array('data_registrazione', 'data_inizio', 'data_fine', 'competenza_dal', 'competenza_al'), true);
						
						echo '
						<div class="anagrafica-cliente" style="width:100px">Cadenza
					
							<select name="cadenza" id="cadenza"  style="width:100px">
								
								<option value="12" '.(Tools::getIsset('edit_contratto') ? ($contratto['cadenza'] == 12 ? 'selected="selected"' : '') : '').'>Mensile</option>
								<option value="1" '.(Tools::getIsset('edit_contratto') ? ($contratto['cadenza'] == 1 ? 'selected="selected"' : '') : '').'>Annuale</option>
								<option value="6" '.(Tools::getIsset('edit_contratto') ? ($contratto['cadenza'] == 6 ? 'selected="selected"' : '') : '').'>Bimestrale</option>
								<option value="4" '.(Tools::getIsset('edit_contratto') ? ($contratto['cadenza'] == 4 ? 'selected="selected"' : '') : '').'>Trimestrale</option>
								<option value="2" '.(Tools::getIsset('edit_contratto') ? ($contratto['cadenza'] == 2 ? 'selected="selected"' : '') : '').'>Semestrale</option>
							</select>
						</div>';
						
						echo '
						<div class="anagrafica-cliente" style="display:none; width:100px">Competenza dal
					
							<input type="text" size="33" id="competenza_dal" name="competenza_dal" value="'.(Tools::getIsset('edit_contratto') ? date("d-m-Y", strtotime($contratto['competenza_dal'])) : date('d-m-Y')).'" style="width:100px" />
						</div>';
							echo '
						<div class="anagrafica-cliente" style="display:none; width:100px">al
					
							<input type="text" size="33" id="competenza_al" name="competenza_al" value="'.(Tools::getIsset('edit_contratto') ? date("d-m-Y", strtotime($contratto['competenza_al'])) : '').'" style="width:100px" />
						</div>';
						
							echo '
						<div class="anagrafica-cliente" style="width:100px">Calcolo rata
							<span class="tab-span" id="rata_contratto" style="width:100px"><script type="text/javascript">
								function calcolaRataContratto() {
								
									var prezzo_contratto = parseFloat(document.getElementById("totale_contratto").innerHTML.replace(",", "."));
									var periodicita = parseFloat(document.getElementById("periodicita").value.replace(",", "."));
									var rata = prezzo_contratto/periodicita;
								
									rata = rata.toFixed(2);
									rata = rata.replace(".", ",");
									document.getElementById("rata_contratto").innerHTML = rata;
								}
							</script>
							'.(Tools::getIsset('edit_contratto') ? str_replace(".",",",$contratto['prezzo']/$contratto['periodicita']) : '').'
							</span>
						</div>';
						
						echo '
						<div class="anagrafica-cliente" style="width:220px">Tipo pagamento
					
							<select name="pagamento" id="pagamento"  style="width:220px">';
							echo "
								<option value='' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == '' ? 'selected="selected"' : '') : '').">Nessun metodo </option>
								<option value='Bonifico Bancario' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'Bonifico Bancario' ? 'selected="selected"' : '') : '').">Bonifico</option>
								<option value='GestPay' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'GestPay' ? 'selected="selected"' : '') : '').">Carta di credito (Gestpay)</option>
								<option value='PayPal' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'PayPal' ? 'selected="selected"' : '') : '').">PayPal</option>
								<option value='Contrassegno' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'Contrassegno' ? 'selected="selected"' : '') : '').">Contrassegno</option>
								<option value='Bonifico 30 gg. fine mese' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'Bonifico 30 gg. fine mese' ? 'selected="selected"' : '') : '').">Bonifico 30 gg. fine mese</option>
								<option value='Bonifico 60 gg. fine mese' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'Bonifico 60 gg. fine mese' ? 'selected="selected"' : '') : '').">Bonifico 60 gg. fine mese</option>
								<option value='Bonifico 90 gg. fine mese' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'Bonifico 90 gg. fine mese' ? 'selected="selected"' : '') : '').">Bonifico 90 gg. fine mese</option>
								<option value='Bonifico 30 gg. 15 mese successivo' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'Bonifico 30 gg. 15 mese successivo' ? 'selected="selected"' : '') : '').">Bonifico 30 gg. 15 mese successivo</option>
								<option value='R.B. 30 GG. 5 mese successivo' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'R.B. 30 GG. 5 mese successivo' ? 'selected="selected"' : '') : '').">R.B. 30 GG. 5 mese successivo</option>
								<option value='R.B. 30 GG. 10 mese successivo' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'R.B. 30 GG. 10 mese successivo' ? 'selected="selected"' : '') : '').">R.B. 30 GG. 10 mese successivo</option>
								
								<option value='R.B. 60 GG. 5 mese successivo' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'R.B. 60 GG. 5 mese successivo' ? 'selected="selected"' : '') : '').">R.B. 60 GG. 5 mese successivo</option>
								<option value='R.B. 60 GG. 10 mese successivo' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'R.B. 60 GG. 10 mese successivo' ? 'selected="selected"' : '') : '').">R.B. 60 GG. 10 mese successivo</option>
								
								
								<option value='R.B. 60 GG. D.F. F.M.' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'R.B. 60 GG. D.F. F.M.' ? 'selected="selected"' : '') : '').">R.B. 60 GG. D.F. F.M.</option>
								<option value='Renting' ".(Tools::getIsset('edit_contratto') ? ($contratto['pagamento'] == 'Renting' ? 'selected="selected"' : '') : '').">Renting</option>
								";
								
								
								
								
								echo '
							</select>
						</div>';
						
						echo '
						<div class="anagrafica-cliente" style="width:200px; color:red">Blocco amministrativo
					
							<input type="checkbox" name="blocco_amministrativo" '.(Tools::getIsset('edit_contratto') ? ($contratto['blocco_amministrativo'] == 1 ? 'checked="checked"' : '') : '').'" />
						</div>';
						
						echo '<div style="clear:both"></div><br />';
					
					echo '
					<div class="anagrafica-cliente" style="width:150px">CIG
				
						<input type="text" style="width:150px" id="cig" value="'.$contratto['cig'].'" name="cig" />
					</div>';
						echo '
					<div class="anagrafica-cliente" style="width:150px">CUP
				
						<input type="text" style="width:150px" id="cup" value="'.$contratto['cup'].'" name="cup" />
					</div>';
						echo '
					<div class="anagrafica-cliente" style="width:250px">Cod. univoco ufficio x fatturazione elettronica
				
						<input type="text" style="width:150px" id="univoco" value="'.$contratto['univoco'].'" name="univoco" />
					</div>';
						
						echo '<div style="clear:both"></div><br />';
						
						
						echo '
						<div class="anagrafica-cliente" style="width:100px">Data registrazione
					
							<input type="text" size="33" id="data_registrazione" name="data_registrazione" value="'.(Tools::getIsset('edit_contratto') ? date("d-m-Y", strtotime($contratto['data_registrazione'])) : date('d-m-Y')).'" style="width:100px" />
						</div>';
							echo '
						<div class="anagrafica-cliente" style="width:100px">Data inizio 
					
							<input type="text" size="33" id="data_inizio" name="data_inizio" value="'.(Tools::getIsset('edit_contratto') ? date("d-m-Y", strtotime($contratto['data_inizio'])) : date('d-m-Y')).'" style="width:100px" />
						</div>';
							echo '
						<div class="anagrafica-cliente" style="width:100px">Data fine
					
							<input type="text" size="33" id="data_fine" name="data_fine" value="'.(Tools::getIsset('edit_contratto') ? date("d-m-Y", strtotime($contratto['data_fine'])) : '').'" style="width:100px" />
						</div>';
						
						$indirizzi = Db::getInstance()->executeS("SELECT * FROM address WHERE id_customer = ".$customer->id." AND deleted = 0 AND active = 1 ORDER BY id_address ASC");
						
							echo '
						<div class="anagrafica-cliente" style="width:300px">Sede
					
							<select name="indirizzo_contratto" style="width:400px">';
							foreach ($indirizzi as $del_a) {
								echo '<option value="'.$del_a['id_address'].'" '.(Tools::getIsset('edit_contratto') ? ($contratto['indirizzo'] == $del_a['id_address'] ? 'selected="selected"' : '') : '').'>'.$del_a['address1'].' - '.$del_a['city'].'</option>';
							}
						echo '</select>
						</div>';
					
						echo '<div style="clear:both"></div>';
						
						
						echo '<br />';
						
						echo '			Seleziona il prodotto da aggiungere: <link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
								<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery.autocomplete3.js"></script>
								<div id="ajax_choose_product" style="padding:6px; padding-top:2px; width:800px;">
									
									<input type="checkbox" checked="checked" id="prodotti_online" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Online &nbsp;&nbsp;
				<input type="checkbox" id="prodotti_offline" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Offline &nbsp;
	<input type="checkbox" id="prodotti_old" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Old &nbsp;&nbsp;
				<input type="checkbox" id="prodotti_disponibili" onclick="if(document.getElementById(\'product_autocomplete_input\').value != \'\') { clearAutoComplete(); }" />Solo disp. &nbsp;&nbsp;&nbsp;
				
				<script type="text/javascript" src="../js/select2.js"></script>
						<script type="text/javascript">
						$(document).ready(function() { $("#auto_marca").select2(); $("#auto_serie").select2(); $("#auto_fornitore").select2();  $("#auto_categoria").select2(); });
						</script>
						
				<select id="auto_marca" name="auto_marca" onchange="repopulateSeries(this.value); clearAutoComplete();" style="width:100px">
				<option value="0">Marca...</option>
				';
				$marche = Db::getInstance()->executeS('SELECT * FROM manufacturer ORDER BY name ASC');
				foreach($marche as $marca)
					echo '<option value="'.$marca['id_manufacturer'].'">'.$marca['name'].'</option>';
				echo '
				</select>
				
						
				<select id="auto_serie" name="auto_serie" onchange="clearAutoComplete();" style="width:100px">
				<option value="0">Serie...</option>
				<option value="0">Scegli prima un costruttore</option>
				';
				echo '
				</select>
				
				<select id="auto_categoria" name="auto_categoria" onchange="clearAutoComplete();" style="width:100px">
				<option value="0">Categoria...</option>
				';
				$categorie = Db::getInstance()->executeS('SELECT * FROM category c JOIN category_lang cl ON c.id_category = cl.id_category WHERE cl.id_lang = 5 AND c.id_parent = 1 ORDER BY cl.name ASC');
				foreach($categorie as $categoria)
					echo '<option value="'.$categoria['id_category'].'">'.$categoria['name'].'</option>';
				echo '
				</select>
				
				<select id="auto_fornitore" name="auto_fornitore" onchange="clearAutoComplete(); " style="width:100px">
				<option value="0">Fornitore...</option>
				';
				
				$fornitori = Db::getInstance()->executeS('SELECT * FROM supplier WHERE id_supplier = 8 OR id_supplier = 11 OR id_supplier = 42 ORDER BY name ASC');
				foreach($fornitori as $fornitore)
					echo '<option value="'.$fornitore['id_supplier'].'">'.$fornitore['name'].'</option>';
				echo '
				</select>
				
				<input id="reset" type="button" value="Reset" class="button" onclick="document.getElementById(\'prodotti_online\').checked = true; document.getElementById(\'prodotti_offline\').checked = true; document.getElementById(\'prodotti_old\').checked = true; document.getElementById(\'prodotti_disponibili\').checked = false; document.getElementById(\'auto_categoria\').options[0].selected = true; $(\'#auto_categoria\').select2();  document.getElementById(\'auto_marca\').options[0].selected = true; $(\'#auto_marca\').select2();  
				document.getElementById(\'auto_serie\').options[0].selected = true; $(\'#auto_serie\').select2();  
				document.getElementById(\'auto_fornitore\').options[0].selected = true; $(\'#auto_fornitore\').select2(); $(\'#prodlist\').hide(); $(\'div.autocomplete_list\').hide();" />
				
				<br />
				 <input size="123" type="text" value="" id="product_autocomplete_input" /><input id="veditutti2" type="button" value="Cerca" class="button" onclick=\'$("#veditutti").trigger("click");\' /><input id="veditutti" type="button" value="Cerca" class="button" style="display:none" /><br /><br />
					
						<input id="excludeIds" type="hidden" value="" />
						
						<script type="text/javascript">
								var formProduct = "";
								var products = new Array();
							</script>
							<link rel="stylesheet" type="text/css" href="'.__PS_BASE_URI__.'css/jquery.autocomplete.css" />
							<script type="text/javascript">
							function addProductAssistenza(event, data, formatted)
							{
								var productId = data[1];
								var productRef = data[4];
								var productName = data[5];
								var productPrice = data[23];
								productPrice = productPrice.replace(/\s\&euro\;/, "").replace(/\s\s*$/, "");
								
								var productPriceP = (parseFloat(productPrice.replace(/\s/g, "").replace(",", ".")));
								
								productId = productId.replace(/^\s\s*/, "").replace(/\s\s*$/, "");
								
								$("body").trigger("click");			
								
								$("#product_autocomplete_input").trigger("click");
								$("#product_autocomplete_input_interno").trigger("click");
								$("#product_autocomplete_input").trigger("click");
								
								$("#product_autocomplete_input_interno").trigger("click");
								
								$("<tr id=\'tr_"+productId+"\'><td>"+productRef+"</td><td><input type=\'text\' name=\'prodotto_descrizione["+productId+"]\' style=\'width:250px\' value=\'"+productName+"\' /></td><td><input type=\'text\' id=\'quantita["+productId+"]\' name=\'quantita["+productId+"]\'  onkeyup=\'CalcTotContratto();\' size=\'3\' value=\'1\' /></td><td><input type=\'text\' value=\'"+productPrice+"\' name=\'prezzo_prodotto["+productId+"]\' onkeyup=\'CalcTotContratto();\' class=\'importo\' style=\'text-align:right\' id=\'prezzo_prodotto["+productId+"]\' onkeyup=\'CalcTotContratto();\' size=\'7\' /></td><td><input type=\'text\' name=\'seriale_prodotto["+productId+"]\' size=\'7\' /></td><td><select name=\'indirizzo_prodotto["+productId+"]\' style=\'width:200px\'>'; foreach ($indirizzi as $del_a) { echo '<option value=\''.$del_a['id_address'].'\'>'.$del_a['address1'].' - '.$del_a['city'].'</option>'; } echo '</select><input type=\'hidden\' id=\'prodotto_id["+productId+"]\' name=\'prodotto_id["+productId+"]\' value=\'"+productId+"\' /><input type=\'hidden\' name=\'prodotto["+productId+"]\' checked=\'checked\' /></td><td><a style=\'cursor:pointer\' onclick=\'CalcTotContratto(); delProduct30Contratto("+productId+")\'><img src=\'../img/admin/delete.gif\' alt=\'Cancella\' title=\'Cancella\' /></a></td></tr>").appendTo("#tableProductsBody");
								
								CalcTotContratto();
								
								
					
								var excludeIds = document.getElementById("excludeIds").value;
								excludeIds = excludeIds+productId+",";
								document.getElementById("excludeIds").value = excludeIds;
								$("#product_autocomplete_input").val("");
								
								
								$("#product_autocomplete_input").setOptions({
									extraParams: {excludeIds : document.getElementById("excludeIds").value}
								});	
								
								$("#product_autocomplete_input_interno").val("");
								
								
								$("#product_autocomplete_input_interno").setOptions({
									extraParams: {excludeIds : document.getElementById("excludeIds").value}
								});	
								
								
							}

							function importoContratto(id_product)
							{
								/*
								var quantity = document.getElementById(\'quantita[\'+id_product+\']\').value;
								var numPrice = document.getElementById(\'prezzo_prodotto[\'+id_product+\']\').value;
								var productPriceP = parseFloat(numPrice.replace(/\s/g, "").replace(",", "."));
								var numimporto = document.getElementById(\'prezzo_contratto\').value;
								var importo = parseFloat(numimporto.replace(/\s/g, "").replace(",", "."));
								
								importo = importo + (productPriceP*quantity);
								
								importo = importo.toFixed(2);
								importo = importo.replace(".",",");
						
								document.getElementById(\'prezzo_contratto\').value=importo;
								*/
							}	
							
							function togliImportoContratto(id_product)
							{
								/*
								var quantity = document.getElementById(\'quantita[\'+id_product+\']\').value;
								var numPrice = document.getElementById(\'prezzo_prodotto[\'+id_product+\']\').value;
								var productPriceP = parseFloat(numPrice.replace(/\s/g, "").replace(",", "."));
								var numimporto = document.getElementById(\'prezzo_contratto\').value;
								var importo = parseFloat(numimporto.replace(/\s/g, "").replace(",", "."));
								
								importo = importo - (productPriceP*quantity);
								
								importo = importo.toFixed(2);
								importo = importo.replace(".",",");
						
								document.getElementById(\'prezzo_contratto\').value=importo;
								*/
							}
							
							function deleteRowContratto(rowid)  
							{   
								var row = document.getElementById(rowid);
								row.parentNode.removeChild(row);
							}
				
							function delProduct30Contratto(id)
								{
									deleteRowContratto(\'tr_\'+id);
									
								}

							urlToCall = null;
							
							function getExcludeIds() {
								var ids = "";
								ids += $(\'#excludeIds\').val().replace(/\\-/g,\',\').replace(/\\,$/,\'\');
								return ids;
							}
							</script>
							
							<script type="text/javascript">
								
								function getAccessorieIds()
									{
										var ids = '. $obj->id.'+\',\';
										ids += $(\'#inputAccessories\').val().replace(/\\-/g,\',\').replace(/\\,$/,\'\');
										ids = ids.replace(/\,$/,\'\');

										return ids;
									}
									
									urlToCall = null;
									/* function autocomplete */
									
									function getOnline()
						{
							if(document.getElementById("prodotti_online").checked == true)
								return 1;
							else
								return 0;
						}
						
						function getOffline()
						{
							if(document.getElementById("prodotti_offline").checked == true)
								return 1;
							else
								return 0;
						}

	function getOld()
						{
							if(document.getElementById("prodotti_old").checked == true)
								return 1;
							else
								return 0;
						}
						
						function getDisponibili()
						{
							if(document.getElementById("prodotti_disponibili").checked == true)
								return 1;
							else
								return 0;
						}
						
						function getCopiaDa()
						{
								return 0;
						}
						
						function getSerie()
						{
							if(document.getElementById("auto_serie").value > 0)
								return document.getElementById("auto_serie").value;
							else
								return 0;
						}
						
						function getMarca()
						{
							if(document.getElementById("auto_marca").value > 0)
								return document.getElementById("auto_marca").value;
							else
								return 0;
						}
						
						function getFornitore()
						{
							if(document.getElementById("auto_fornitore").value > 0)
								return document.getElementById("auto_fornitore").value;
							else
								return 0;
						}
						
						function getCategoria()
						{
							if(document.getElementById("auto_categoria").value > 0)
								return document.getElementById("auto_categoria").value;
							else
								return 0;
						}
						
						function repopulateSeries(id_manufacturer)
						{
							$("#auto_serie").empty();
							$.ajax({
							  url:"ajax.php?repopulate_series=y",
							  type: "POST",
							  data: { id_manufacturer: id_manufacturer
							  },
							  success:function(resp){  
								var newOptions = $.parseJSON(resp);
								
								 $("#auto_serie").append($("<option></option>")
									 .attr("value", "0").text("Serie..."));
								
								$.each(newOptions, function(key,value) {
								  $("#auto_serie").append($("<option></option>")
									 .attr("value", value).text(key));
								});
								
								$("#auto_serie").select2({
									placeholder: "Serie..."
								});
							  },
							  error: function(xhr,stato,errori){
								 alert("Errore: impossibile trovare serie ");
							  }
							});
								
							
						}	
									
								function clearAutoComplete()
						{
							$("#veditutti").trigger("click");
							
						}		
									
									$(function() {
								$(\'#product_autocomplete_input\')
									.autocomplete(\'ajax_products_list2.php?id_cart='.$_GET['id_cart'].'\', {
										minChars: 0,
										autoFill: false,
										max:500,
										matchContains: true,
										matchSubset: 0,
										mustMatch:true,
										scroll:true,
										cacheLength:1,
										formatItem: function(item) {
								
									var color = "";
									if(item[24] == 1) {
										var color = "; color:red";
									}
									if(item[28] == 0) {
										return \'<tr><th style="width:150px; text-align:left">Codice</th><th style="width:550px">Desc. prodotto</th><th style="width:60px; text-align:right">Prezzo</th><th style="width:50px; text-align:right">Qt. mag.</th><th style="width:50px; text-align:right">Qt. tot</th></tr><tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:550px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
										
										;
									}
									else {
										return \'<tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:550px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
										
										;
										}
									}
										
									}).result(addProductAssistenza);
								
							});
						
							$("#product_autocomplete_input").css(\'width\',\'850px\');
							$("#product_autocomplete_input").keypress(function(event){
										
										  var keycode = (event.keyCode ? event.keyCode : event.which);
										  if (keycode == "13") {
											event.preventDefault();
											event.stopPropagation();    
										  }
										});
										
								$(function() {
								$(\'#product_autocomplete_input_interno\')
									.autocomplete(\'ajax_products_list2_interno.php?id_cart='.$_GET['id_cart'].'\', {
										minChars: 3,
										autoFill: false,
										max:500,
										matchContains: true,
										matchSubset: 0,
										mustMatch:true,
										scroll:true,
										cacheLength:1,
										formatItem: function(item) {
									
													var color = "";
													if(item[24] == 1) {
														var color = "; color:red";
													}
													if(item[28] == 0) {
														return \'<tr><th style="width:150px; text-align:left">Codice</th><th style="width:550px">Desc. prodotto</th><th style="width:60px; text-align:right">Prezzo</th><th style="width:50px; text-align:right">Qt. mag.</th><th style="width:50px; text-align:right">Qt. tot</th></tr><tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:550px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
														
														;
													}
													else {
														return \'<tr><td style="width:150px; text-align:left"><img style="width:35px; height:35px" src="\'+item[35]+\'" />&nbsp;&nbsp;\'+item[4]+\'</td><td style="width:550px\'+color+\'">\'+item[5]+\'</td><td style="width:60px; text-align:right">\'+item[23]+\'</td><td style="width:50px; text-align:right">\'+item[25]+\'</td><td style="width:50px; text-align:right">\'+item[22]+\'</td></tr>\'
														
														;
														}
													}
										
									}).result(addProductAssistenza);
								
							});
						
							$("#product_autocomplete_input_interno").css(\'width\',\'850px\');
							$("#product_autocomplete_input_interno").keypress(function(event){
										
										  var keycode = (event.keyCode ? event.keyCode : event.which);
										  if (keycode == "13") {
											event.preventDefault();
											event.stopPropagation();    
										  }
										});
										
							
						</script><script type="text/javascript">
					
					$("#veditutti2").click ( function (zEvent) {
						
						/*	$("body").trigger("click");
							$("body").trigger("click");
							
							
							
							$("#product_autocomplete_input").focus();
						//	$("#product_autocomplete_input").val("");
						//	$("#product_autocomplete_input").val(" ");
						*/
							var press = jQuery.Event("keypress");
							press.which = 13;
							if(document.getElementById(\'product_autocomplete_input\').value == "")
							{
								$("#product_autocomplete_input").val(" ");
							}
							$("#product_autocomplete_input").trigger(press);
						
							$("#product_autocomplete_input").trigger("click");
							$("#product_autocomplete_input").trigger("click");
						//	$("#product_autocomplete_input").val("");
							
					} );


						function CalcTotContratto() {
				
							var tot_varie = 0;
							
							var arrayImporti = document.getElementsByClassName("importo");
							for (var i = 0; i < arrayImporti.length; ++i) {
								var item = parseFloat(arrayImporti[i].value.replace(\',\',\'.\'));  
								tot_varie += item;
							}
						
							
							var totale_contratto = tot_varie;
							
							document.getElementById(\'totale_importo\').value = totale_contratto.toFixed(2);
							document.getElementById(\'totale_contratto\').innerHTML = totale_contratto.toFixed(2).replace(\'.\',\',\');
							calcolaRataContratto();
							
						}
					</script>';
						
							echo "<strong>Prodotti</strong> (spunta per includere il prodotto nel contratto di assistenza)<br />";
						
						if(Tools::getIsset('edit_contratto')) {
							$prodotti = Db::getInstance()->executeS("SELECT count(cap.id_product) AS conto, cap.id_product as product_id, cap.quantita, cap.prezzo, cap.indirizzo, cap.seriale, p.reference, cap.descrizione_prodotto name FROM contratto_assistenza_prodotti cap JOIN contratto_assistenza ca ON ca.id_contratto = cap.id_contratto JOIN product p ON cap.id_product = p.id_product JOIN product_lang pl ON cap.id_product = pl.id_product WHERE cap.id_contratto = '".Tools::getValue('edit_contratto')."' AND pl.id_lang = 5 GROUP BY cap.id_product");
						}
						else {
						
							$prodotti = Db::getInstance()->executeS("SELECT count(od.product_id) AS conto, od.product_id, p.reference, pl.name, f.qt_ord FROM order_detail od JOIN orders o ON od.id_order = o.id_order JOIN product p ON od.product_id = p.id_product JOIN product_lang pl ON od.product_id = pl.id_product  JOIN fattura f ON p.reference = f.cod_articolo WHERE o.id_customer = ".$customer->id." AND p.reference IN (SELECT cod_articolo FROM fattura WHERE id_customer = ".$customer->id.")  AND pl.id_lang = 5 GROUP BY od.product_id");
						
						}
						echo "<table class='table' style='width:100%'><thead>
						<tr>
							<th>Codice prodotto</th>
							<th>Descrizione</th>
							<th>Qta</th>
							<th>Prezzo</th>
							<th>Seriale</th>
							<th>Indirizzo prodotto</th>
							<th></th>
							</tr></thead><tbody id='tableProductsBody'>";
							
						if(sizeof($prodotti) > 0) {
						
							
							foreach($prodotti as $prodotto) {
							
								echo "<tr id='tr_".$prodotto['product_id']."'>";
								echo "<td><span style='cursor:pointer' class='span-reference' title=\"".Product::showProductTooltip($prodotto['product_id'])."\">".$prodotto['reference']."</span></td>";
								echo "<td><input type='text' style='width:250px' name='prodotto_descrizione[".$prodotto['product_id']."]' value='".$prodotto['name']."' /></td>";
								echo "<td><input type='hidden' name='prodotto[".$prodotto['product_id']."]' ".(Tools::getIsset('edit_contratto') ? "checked='checked'" : '')." />";
								echo "<input type='text' size='3' id='quantita[".$prodotto['product_id']."]' name='quantita[".$prodotto['product_id']."]' onkeyup='CalcTotContratto();' value='".(Tools::getIsset('edit_contratto') ? $prodotto['quantita'] : $prodotto['qt_ord'])."' /></td>";
								echo "<td><input type='text' size='7' class='importo' style='text-align:right' name='prezzo_prodotto[".$prodotto['product_id']."]' id='prezzo_prodotto[".$prodotto['product_id']."]' onkeyup='CalcTotContratto();' value='".(Tools::getIsset('edit_contratto') ? number_format($prodotto['prezzo'],2,',','') : '')."' /></td>";
								echo "<td><input type='text' size='7' name='seriale_prodotto[".$prodotto['product_id']."]' value='".(Tools::getIsset('edit_contratto') ? $prodotto['seriale'] : '')."' /></td>";
								echo '<td><select name="indirizzo_prodotto['.$prodotto['product_id'].']" style="width:200px">';
							foreach ($indirizzi as $del_a) {
								echo '<option value="'.$del_a['id_address'].'" '.(Tools::getIsset('edit_contratto') ? ($prodotto['indirizzo'] == $del_a['id_address'] ? 'selected="selected"' : '') : '').'>'.$del_a['address1'].' - '.$del_a['city'].'</option>';
							}
						echo '</select></td><td>'.($cookie->profile != 8 ? '<a style="cursor:pointer" onclick="togliImportoContratto('.$prodotto['product_id'].'); CalcTotContratto(); delProduct30Contratto('.$prodotto['product_id'].')"><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a>' : '').'</td>';
						
								echo "</tr>";
							
							
							}
							
						}
						echo "</tbody><tfoot>
				<tr><td colspan='2'><span style='font-size:11px'>Tot. Importo Contratto IVA escl.</span></td><td></td><td><span class='tab-span' id='totale_contratto' style='width:56px; margin-right:3px; text-align:right; float:left'>".(Tools::getIsset('edit_contratto') ? number_format(($contratto['prezzo']),2,',','') : '')."</span></td><td><input type='hidden' name='totale_importo' id='totale_importo' value='".$contratto['prezzo']."' /></td><td></td><td></td></tr></tfoot></table>";
						
						echo '<br />
					';
					
					echo '<div class="clear"></div>';
					
					echo '<strong>Nota interna visibile solo agli impiegati</strong><br />
						<textarea name="note_private" id="ContrattoNoteContent" style="width:95%;height:100px">'.Tools::htmlentitiesUTF8($contratto['note_private']).'</textarea><br />
						';
		
					echo '<br /><div class="clear"></div>';
						
						echo '<div id="dettagli_controlli"><div style="float:left; margin-left:0px" id="dettagli_pagamento_contratto"><table class="table"><tr><th>Pagato</th><th>Da pagare</th><th>Fatturato</th><th>Da fatturare</th></tr>
						
						<tr>
						<td style="text-align:center"><input type="checkbox" onclick="if(this.checked) { document.getElementById(\'da_pagare\').checked = false; } else if(this.checked == false) {  document.getElementById(\'da_pagare\').checked = true; }" id="pagato" name="pagato" '.($contratto['pagato'] == 1 ? 'checked="checked"' : '').' value="1" /></td>
						<td style="text-align:center"><input type="checkbox" onclick="if(this.checked) { document.getElementById(\'pagato\').checked = false; } else if(this.checked == false) {  document.getElementById(\'pagato\').checked = true; }"id="da_pagare" name="da_pagare" '.($contratto['pagato'] == 0 ? 'checked="checked"' : '').' value="0" /></td>
						<td style="text-align:center"><input type="checkbox" id="fatturato" name="fatturato" '.($contratto['fatturato'] == 1 ? 'checked="checked"' : '').' onclick="if(this.checked) { document.getElementById(\'da_fatturare\').checked = false; } else if(this.checked == false) {  document.getElementById(\'da_fatturare\').checked = true; }" value="1" /></td>
						<td style="text-align:center"><input type="checkbox" id="da_fatturare" name="da_fatturare" '.($contratto['fatturato'] == 0 ? 'checked="checked"' : '').' onclick=" if(this.checked) { document.getElementById(\'fatturato\').checked = false; } else if(this.checked == false) {  document.getElementById(\'fatturato\').checked = true; } " value="0" /></td>
						</tr></table></div>';
						
						echo '<div id="invii_controlli" style="float:left; margin-left:70px '.($contratto['invio_contabilita'] == 2 ? '; display:none' : '').'">
						<table class="table"><tr><th>Conferma per contabilità</th></tr>
						
						<tr>
					
						'.($cookie->id_employee != 1 && $cookie->id_employee != 6 ? ($contratto['invio_contabilita'] == 1  || $contratto['invio_contabilita'] == 2 ? '<input type="hidden" name="invio_contabilita" value="2" />' : '') : '<td style="text-align:center"> '.($contratto['invio_contabilita'] == 1  || $contratto['invio_contabilita'] == 2 ? '<input type="hidden" name="invio_contabilita" value="2" />Gi&agrave; inviato' :  '<input type="checkbox" value="1" id="invio_contabilita" name="invio_contabilita" />').'</td>').'
						</tr></table>
						
						</div></div>';
						
						echo '<div style="clear:both"></div>';
						
						echo '<script type="text/javascript">
						function checkFormContratto() {
							var check=document.getElementById("id_contratto").value; 
							if(check=="") {
								alert("Il campo N. contratto è obbligatorio per generare il contratto");
								return false;
							}
							else {
								return true;
							}
						}
						</script>';
						
						echo '<input type="hidden" name="rinnova_contratto_ok" id="rinnova_contratto_ok" value="0" />';
						
						if(Tools::getIsset('edit_contratto')) {
							echo '<br /><button type="submit" class="button" id="conferma_modifiche_contratto">
							<img src="../img/admin/enabled-2.gif" alt="Conferma" title="Conferma" />&nbsp;&nbsp;&nbsp;Conferma modifiche
							</button>';
						}
						else {
							echo '<br /><button type="submit" class="button">
							<img src="../img/admin/enabled-2.gif" alt="Conferma" title="Conferma" />&nbsp;&nbsp;&nbsp;Conferma nuovo contratto di assistenza
							</button>';
						}
						
						echo "</form><br /><br />";
						
						echo '</div>';
					
					echo '<div id="storico_contratto" class="tab-contratto">
					
					';
					$storico_contratto = Db::getInstance()->executeS('SELECT * FROM storico_attivita WHERE id_attivita = '.$contratto['id_contratto'].' AND tipo_attivita = "CO"');
					
					if(count($storico_contratto) == 0)
						echo 'Nessuna informazione';
					else
					{
						echo '<table class="table">
						<tr><th>Data</th><th>Dati storici</th></tr>';
						foreach($storico_contratto as $storico)
							echo '<tr><td>'.Tools::displayDate($storico['data_attivita'],5).'</td><td>'.$storico['desc_attivita'].'</td></tr>';
						
						echo '</table>';
					}	
					
					echo'
					</div>
					
					</div>';
					echo '<script type="text/javascript">
						var tabber_contratti = new Yetii({
						id: "tab-container-contratti",
						tabclass: "tab-contratto"
						});
						</script>
						';
					}
				}
				
				else if(Tools::getIsset('id_contratto') && Tools::getIsset('id_contratto') > 0) {
					echo '<div style="clear:both"></div>';
					echo '<div id="tab-container-contratti" class="tab-containers">
					<ul id="tab-container-contratti-nav" class="tab-containers-nav">
					<li><a href="#dati_contratto">Contratto</a></li>
					<li><a href="#storico_contratto">Storico</a></li>
					</ul>

					<div class="tab-contratto" id="dati_contratto">
					';
					
					// echo "<a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&nuovocontratto&token=".$this->token."&tab-container-1=13' class='button' style='display:block;float:left; margin-left:10px'><img src='../img/admin/cms.gif' alt='Nuovo contratto' title='Nuovo contratto' />&nbsp;&nbsp;&nbsp;Genera nuovo contratto</a>";
					
					$contratto = Db::getInstance()->getRow("SELECT * FROM contratto_assistenza WHERE id_customer = ".$customer->id." AND id_contratto = ".Tools::getValue('id_contratto')."");
					$prodotti = Db::getInstance()->executeS("SELECT * FROM contratto_assistenza_prodotti WHERE id_contratto = ".Tools::getValue('id_contratto')."");
					switch($contratto['tipo']) { case 1: $tipo_contratto = 'Base'; break; case 2: $tipo_contratto = 'Top'; break; case 3: $tipo_contratto = 'Gold'; break; default: $tipo_contratto = 'Altro'; }
					switch($contratto['status']) { case 0: $status_contratto = 'Attivo'; break; case 1: $status_contratto = 'Sospeso'; break; case 2: $status_contratto = 'Cancellato'; break;  case 3: $status_contratto = 'Disdetto da cliente'; break; case 4: $status_contratto = 'Scaduto'; break; default: $status_contratto = 'Altro'; }
				
								echo '
							
					<div class="anagrafica-cliente" style="width:100px">'.$this->l('N. contratto').' 
				
						<span class="tab-span" style="width:100px">'.$contratto['id_contratto'].'</span>
					</div>';
					echo '
					<div class="anagrafica-cliente" style="width:100px">Rif. fattura
				
						<span class="tab-span" style="width:100px">'.$contratto['rif_fattura'].'</span>
					</div>';
					echo '
					<div class="anagrafica-cliente" style="width:100px">Rif. noleggio 
				
						<span class="tab-span" style="width:100px">'.$contratto['rif_noleggio'].'</span>
					</div>';
					
					echo '
					<div class="anagrafica-cliente" style="width:100px">Rif. ordine
				
						<span class="tab-span" style="width:100px">'.$contratto['rif_ordine'].'</span>
					</div>';
					
						echo '
					<div class="anagrafica-cliente" style="width:100px">Tipo 
				
						<span class="tab-span" style="width:100px">'.$tipo_contratto.'</span>
					</div>';
						echo '
					<div class="anagrafica-cliente" style="width:100px">Stato
				
						<span class="tab-span" style="width:100px">'.$status_contratto.'</span>
					</div>';
					echo '<div style="clear:both"></div><br />';
					
						echo '
					<div class="anagrafica-cliente" style="width:100px">Importo
				
						<span class="tab-span" style="width:100px">'.Tools::displayPrice($contratto['prezzo'], new Currency($defaultCurrency)).'</span>
					</div>';
					echo '
					<div class="anagrafica-cliente" style="width:100px">Cadenza
				
						<span class="tab-span" style="width:100px">'.$contratto['cadenza'].' mesi</span>
					</div>';
					
						echo '
						<div class="anagrafica-cliente" style="width:100px">Competenza dal
					
							<input type="text" size="33" id="competenza_dal" name="competenza_dal" value="'.(Tools::getIsset('edit_contratto') ? date("d-m-Y", strtotime($contratto['competenza_dal'])) : date('d-m-Y')).'" style="width:100px" />
						</div>';
							echo '
						<div class="anagrafica-cliente" style="width:100px">al
					
							<input type="text" size="33" id="competenza_al" name="competenza_al" value="'.(Tools::getIsset('edit_contratto') ? date("d-m-Y", strtotime($contratto['competenza_al'])) : '').'" style="width:100px" />
						</div>';
						
					echo '
					<div class="anagrafica-cliente" style="width:100px">Rata
				
						<span class="tab-span" style="width:100px">'.Tools::displayPrice($contratto['prezzo']/$contratto['periodicita'], new Currency($defaultCurrency)).' </span>
					</div>';
					
					echo '
					<div class="anagrafica-cliente" style="width:220px">Pagamento
				
						<span class="tab-span" style="width:220px">'.$contratto['pagamento'].'</span>
					</div>';
					
					if($contratto['blocco_amministrativo'] == 1) {
					echo '
					<div class="anagrafica-cliente" style="width:200px; color:red">Blocco amministrativo
				
						<span class="tab-span" style="width:200px; color:red">PRESENTE</span>
					</div>';
					}
					
					echo '<div style="clear:both"></div><br />';
					
					echo '
					<div class="anagrafica-cliente" style="width:150px">CIG
				
						<span class="tab-span" style="width:150px">'.$contratto['cig'].'</span>
					</div>';
						echo '
					<div class="anagrafica-cliente" style="width:150px">CUP
				
						<span class="tab-span" style="width:150px">'.$contratto['cup'].'</span>
					</div>';
						echo '
					<div class="anagrafica-cliente" style="width:150px">Cod. univoco ufficio x fatturazione elettronica
				
						<span class="tab-span" style="width:150px">'.$contratto['univoco'].'</span>
					</div>';
					
					
					
					echo '<div style="clear:both"></div><br />';
					if(is_numeric($contratto['indirizzo'])) {
							
						$indirizzo_row = Db::getInstance()->getRow("SELECT * FROM address WHERE id_address = ".$contratto['indirizzo']."");
						$indirizzo = $indirizzo_row['address1']." - ".$indirizzo_row['postcode']." ".$indirizzo_row['city']." (".Db::getInstance()->getValue("SELECT iso_code FROM state WHERE id_state = ".$indirizzo_row['id_state']).")";
					}
					else {
						$indirizzo = $contratto['indirizzo'];
					}
					
							
					echo '
					<div class="anagrafica-cliente" style="width:100px">Data registrazione
				
						<span class="tab-span" style="width:100px">'.Tools::displayDate($contratto['data_registrazione'], $cookie->id_lang).'</span>
					</div>';
						echo '
					<div class="anagrafica-cliente" style="width:100px">Data inizio 
				
						<span class="tab-span" style="width:100px">'.Tools::displayDate($contratto['data_inizio'], $cookie->id_lang).'</span>
					</div>';
						echo '
					<div class="anagrafica-cliente" style="width:100px">Data fine
				
						<span class="tab-span" style="width:100px">'.Tools::displayDate($contratto['data_fine'], $cookie->id_lang).'</span>
					</div>';
					echo '
					<div class="anagrafica-cliente" style="width:350px">Sede
				
						<span class="tab-span" style="width:350px">'.$indirizzo.'</span>
					</div>';
				
					echo '<div style="clear:both"></div>';
					
					echo '<br /><strong>Prodotti</strong><br />';
					
					echo '<table class="table"><thead><tr>';
					echo '<th>Codice</th><th>Descrizione</th><th>Qta</th><th>Prezzo</th><th>Seriale</th><th>Indirizzo</th>';
					echo '</tr></thead><tbody>';
					
					foreach($prodotti as $prodotto) {
						
						echo "<tr>";
						echo "<td>".$prodotto['codice_prodotto']."</td>";
						echo "<td>".$prodotto['descrizione_prodotto']."</td>";	
						echo "<td style='text-align:right'>".$prodotto['quantita']."</td>";
						echo "<td style='text-align:right'>".Tools::displayPrice($prodotto['prezzo'], new Currency($defaultCurrency))."</td>";
						echo "<td style=''>".$prodotto['seriale']."</td>";
						if($prodotto['indirizzo'] == '') {
						
							$indirizzo1 = explode("Canone Assistenza",$prodotto['note']);
							$indirizzo2 = explode(" : ",$indirizzo1[0]);
							$indirizzo = $indirizzo2[1];
						}
						else {
							if(is_numeric($contratto['indirizzo'])) {
							
								$indirizzo_row = Db::getInstance()->getRow("SELECT * FROM address WHERE id_address = ".$prodotto['indirizzo']."");
								$indirizzo = $indirizzo_row['address1']." - ".$indirizzo_row['postcode']." ".$indirizzo_row['city']." (".Db::getInstance()->getValue("SELECT iso_code FROM state WHERE id_state = ".$indirizzo_row['id_state']).")";
							}
							else {
								$indirizzo = $prodotto['indirizzo'];
							}
							
						}
						echo "<td>".$indirizzo."</td>";
						echo "</tr>";
					}
					echo "</tbody></table><br />
					";
					
					echo '<div class="clear"></div>';
						
						echo '<strong>Note private</strong><br />
						<textarea readonly="readonly" style="width:83%;">'.Tools::htmlentitiesUTF8($contratto['note_private']).'</textarea><br />
						';
						echo "<br /><br />
					<a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&edit_contratto=".$contratto['id_contratto']."&token=".$this->token."&tab-container-1=13' class='button'>Modifica questo contratto</a><br /><br />
					";
					echo '</div>';
					
					echo '<div id="storico_contratto" class="tab-contratto">
					
					';
					$storico_contratto = Db::getInstance()->executeS('SELECT * FROM storico_attivita WHERE id_attivita = '.$contratto['id_contratto'].' AND tipo_attivita = "CO"');
					
					if(count($storico_contratto) == 0)
						echo 'Nessuna informazione';
					else
					{
						echo '<table class="table">
						<tr><th>Data</th><th>Dati storici</th></tr>';
						foreach($storico_contratto as $storico)
							echo '<tr><td>'.Tools::displayDate($storico['data_attivita'],5).'</td><td>'.$storico['desc_attivita'].'</td></tr>';
						
						echo '</table>';
					}	
					
					echo'
					</div>
					
					</div>';
					echo '<script type="text/javascript">
						var tabber_contratti = new Yetii({
						id: "tab-container-contratti",
						tabclass: "tab-contratto"
						});
						</script>
						';
				}
				else {

					$count_contratti = Db::getInstance()->getValue("SELECT count(id_contratto) FROM contratto_assistenza WHERE id_customer = ".$customer->id."");			
					if($count_contratti == 0) {
						echo "<div style='float:left; margin-left:20px; margin-top:5px;'>Non sono presenti contratti per questa anagrafica.</div>
						
						<a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&nuovocontratto&token=".$this->token."&tab-container-1=13' class='button' style='display:block;float:left; margin-left:10px'><img src='../img/admin/cms.gif' alt='Nuovo contratto' title='Nuovo contratto' />&nbsp;&nbsp;&nbsp;Genera nuovo contratto</a>
						<div style='clear:both'></div>";
						
						
					} else {
						echo "<div style='float:left; margin-left:20px; margin-top:5px;'>Sono presenti ".$count_contratti." contratti per questa anagrafica.</div>
						<a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&nuovocontratto&token=".$this->token."&tab-container-1=13' class='button' style='display:block;float:left; margin-left:10px'><img src='../img/admin/cms.gif' alt='Nuovo contratto' title='Nuovo contratto' />&nbsp;&nbsp;&nbsp;Genera nuovo contratto</a>
						<div style='clear:both'></div>";
						echo "<table class='table'>";
						echo "<thead>";
						echo "<tr>";
						echo "<th>N. contratto</th>";
						echo "<th>Tipo</th>";
						echo "<th>Stato</th>";
						echo "<th>Inizia il</th>";
						echo "<th>Scade il</th>";
						echo "<th>Importo</th>";
						echo "<th>Sede</th>";
						echo "<th>Azioni</th>";
						echo "</tr>";
						echo "</thead>";
						echo "<tbody>";
						$contratti = Db::getInstance()->executeS("SELECT * FROM contratto_assistenza WHERE id_customer = ".$customer->id."");
						foreach($contratti as $contratto) {
						
							switch($contratto['tipo']) { case 1: $tipo_contratto = 'Base'; break; case 2: $tipo_contratto = 'Top'; break; case 3: $tipo_contratto = 'Gold'; break; default: $tipo_contratto = 'Altro'; }
							switch($contratto['status']) { case 0: $status_contratto = 'Attivo'; break; case 1: $status_contratto = 'Sospeso'; break; case 2: $status_contratto = 'Cancellato'; break; case 3: $status_contratto = 'Disdetto da cliente'; break; case 4: $status_contratto = 'Scaduto'; break; default: $status_contratto = 'Altro'; }
							
							if(is_numeric($contratto['indirizzo'])) {
							
								$indirizzo_row = Db::getInstance()->getRow("SELECT * FROM address WHERE id_address = ".$contratto['indirizzo']."");
								$indirizzo = $indirizzo_row['address1']." - ".$indirizzo_row['postcode']." ".$indirizzo_row['city']." (".Db::getInstance()->getValue("SELECT iso_code FROM state WHERE id_state = ".$indirizzo_row['id_state']).")";
							}
							else {
								$indirizzo = $contratto['indirizzo'];
							}
							
							echo "<tr>";
							echo "<td>".$contratto['id_contratto']."</td>";
							echo "<td>".$tipo_contratto."</td>";
							echo "<td>".$status_contratto."</td>";
							echo "<td>".Tools::displayDate($contratto['data_inizio'],$cookie->id_lang,false)."</td>";
							echo "<td>".Tools::displayDate($contratto['data_fine'],$cookie->id_lang,false)."</td>";
							echo "<td style='text-align:right'>".Tools::displayPrice($contratto['prezzo'], new Currency($defaultCurrency))."</td>";
							echo "<td>".$indirizzo."</td>";
							echo "<td><a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&id_contratto=".$contratto['id_contratto']."&token=".$this->token."&tab-container-1=13'><img src='../img/admin/details.gif' alt='Dettagli' title='Dettagli' /></a>
							 <a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&edit_contratto=".$contratto['id_contratto']."&token=".$this->token."&tab-container-1=13'><img src='../img/admin/edit.gif' alt='Modifica' title='Modifica' /></a>
							";
							echo "</tr>";
						}
						echo "</tbody></table>";
						
						
					}
				}
			}
		} 
		//FINE CONTRATTI
		else { }
		
		
		//INIZIO BDL
		if(isset($_GET['tab-container-1']) && ($_GET['tab-container-1'] == 6 || $_GET['tab-container-1'] == 14)) {
			// BDL
			if($cookie->profile == 7)
				echo 'Non hai i permessi per visualizzare questa sezione';
			else
			{
				
				if(isset($_GET['getPDFBDL'])) {
				
					require_once('../classes/html2pdf/html2pdf.class.php');
					
					if(Tools::getIsset('id_bdl') && Tools::getValue('id_bdl') == 'vuoto')
						$content = $this->generatePDFBDL(Tools::getValue('id_bdl'), $_GET['getPDFBDL'], 'y');
					else
						$content = $this->generatePDFBDL(Tools::getValue('id_bdl'), $_GET['getPDFBDL']);
					ob_clean();
					$html2pdf = new HTML2PDF('P','A4','it');
					$html2pdf->WriteHTML($content);
					
					ob_start();
					ob_end_clean();

					$html2pdf->Output(Tools::getValue('id_bdl').'-buono-di-lavoro.pdf', 'D'); 
				
				}
				
				if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 14) 
					echo '<div style="float:left"><h2>BDL (Buoni Di Lavoro)</h2></div>';
				
				
				if(Tools::getIsset('delete_bdl') > 0) {
					
					Db::getInstance()->executeS("DELETE FROM bdl WHERE id_bdl = ".Tools::getValue('delete_bdl')."");
					Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=14');
				}
				
				if(Tools::getIsset('nuovobdl') > 0 || Tools::getIsset('edit_bdl') > 0 ) {
					if(Tools::getValue('submitted') == 'y') {
						
						$ora_richiesta_array = explode(":",Tools::getValue('ora_richiesta'));
						$ora_richiesta =  sprintf("%02d", $ora_richiesta_array[0]).":".(!$ora_richiesta_array[1] ? '00' : $ora_richiesta_array[1]);
						$ora_effettuato_array = explode(":",Tools::getValue('ora_effettuato'));
						$ora_effettuato =  sprintf("%02d", $ora_effettuato_array[0]).":".(!$ora_effettuato_array[1] ? '00' : $ora_effettuato_array[1]);
						
						$data_richiesta = date("Y-m-d H:i:s", strtotime(Tools::getValue('data_richiesta')." ".$ora_richiesta));
						$data_effettuato = date("Y-m-d H:i:s", strtotime(Tools::getValue('data_effettuato')." ".$ora_effettuato));
						if(Tools::getIsset('edit_bdl')) {
						
							$phone = Tools::getValue('bdl_phone');
							if($phone == 'Altro')
								$phone .= Tools::getValue('bdl_phone_altro');	
						
							$guasto = Tools::getValue('guasto');
							if($guasto == 'Altro')
								$guasto .= Tools::getValue('guasto_altro');
								
							$causa_guasto = Tools::getValue('causa_guasto');
							if($causa_guasto == 'Altro')
								$causa_guasto .= Tools::getValue('causa_guasto_altro');
							
							$descrizione_bdl = Tools::getValue('descrizione');
							if($descrizione_bdl == 'Altro')
								$descrizione_bdl .= Tools::getValue('descrizione_altro');
								
							$impianto_ubicazione = Tools::getValue('impianto_ubicazione');
							if(!is_numeric($impianto_ubicazione))
								$impianto_ubicazione = Tools::getValue('impianto_ubicazione_manuale');
							
							Db::getInstance()->executeS("UPDATE bdl SET id_address = '".Tools::getValue('indirizzo_bdl')."', phone = '".$phone."', data_richiesta = '".$data_richiesta."', richiesto_da = '".Tools::getValue('richiesto_da')."', data_effettuato = '".$data_effettuato."', effettuato_da = '".Tools::getValue('effettuato_da')."', impianto_ubicazione = '".$impianto_ubicazione."', contratto_assistenza = '".Tools::getValue('contratto_assistenza')."', manutenzione_ordinaria = '".(Tools::getIsset('manutenzione_ordinaria') ? 1 : 0)."', manutenzione_straordinaria = '".(Tools::getIsset('manutenzione_straordinaria') ? 1 : 0)."', motivo_chiamata = '".addslashes(Tools::getValue('motivo_chiamata'))."', guasto = '".addslashes($guasto)."', causa_guasto = '".addslashes($causa_guasto)."', descrizione = '".addslashes($descrizione_bdl)."', invio_controllo = '".(Tools::getIsset('invio_controllo') ? 1 : 0)."', invio_contabilita = '".(Tools::getIsset('invio_contabilita') ? 1 : 0)."', intervento_svolto = '".addslashes(Tools::getValue('intervento_svolto'))."', note = '".addslashes(Tools::getValue('note_bdl'))."', note_private = '".addslashes(Tools::getValue('note_private_bdl'))."', rif_ordine = '".addslashes(Tools::getValue('rif_ordine'))."', date_upd = '".date('Y-m-d H:i:s')."', pagato = '".(Tools::getIsset('pagato') ? 1 : 0)."', fatturato = '".(Tools::getIsset('fatturato') ? 1 : 0)."', nessun_addebito = '".(Tools::getIsset('nessun_addebito') ? 1 : 0)."' WHERE id_bdl = '".Tools::getValue('id_bdl')."'");
							
							foreach ($_POST['note_nuova'] as $id_nota=>$nota_id) {
					
								echo $_POST['note_nota'][$id_nota].'<br />';
							
								if($_POST['note_nuova'][$id_nota] == 0) {
									
									$testo_nota = Db::getInstance()->getValue('SELECT note FROM note_attivita WHERE id_note = '.$id_nota.'');
									if($testo_nota != $_POST['note_nota'][$id_nota])
										Db::getInstance()->executeS("UPDATE note_attivita SET note = '".addslashes($_POST['note_nota'][$id_nota])."', date_upd = '".date("Y-m-d H:i:s")."', id_employee = '".$cookie->id_employee."' WHERE id_note = ".$id_nota.""); 
								}
								
								else {
									if(addslashes($_POST['note_nota'][$id_nota]) != '')
									{
										Db::getInstance()->executeS("INSERT INTO note_attivita
										(id_note,
										id_attivita,
										tipo_attivita,
										id_employee,
										note,
										date_add,
										date_upd)
										
										VALUES (
										NULL,
										'".Tools::getValue('id_bdl')."',
										'B',
										'".$cookie->id_employee."',
										'".addslashes($_POST['note_nota'][$id_nota])."',
										'".date("Y-m-d H:i:s")."',
										'".date("Y-m-d H:i:s")."'
										)");
										
									}
								}
							}
							
						
						} else {
							$ora_richiesta_array = explode(":",Tools::getValue('ora_richiesta'));
							$ora_richiesta = sprintf("%02d", $ora_richiesta_array[0]).":".(!$ora_richiesta_array[1] ? '00' : $ora_richiesta_array[1]);
							$ora_effettuato_array = explode(":",Tools::getValue('ora_effettuato'));
							$ora_effettuato =  sprintf("%02d", $ora_effettuato_array[0]).":".(!$ora_effettuato_array[1] ? '00' : $ora_effettuato_array[1]);
							
							$data_richiesta = date("Y-m-d H:i:s", strtotime(Tools::getValue('data_richiesta')." ".$ora_richiesta));
							$data_effettuato = date("Y-m-d H:i:s", strtotime(Tools::getValue('data_effettuato')." ".$ora_effettuato));
						
							$codice_spring = Db::getInstance()->getValue("SELECT codice_spring FROM customer WHERE id_customer = ".Tools::getValue('id_customer')."");
							
							$guasto = Tools::getValue('guasto');
							if($guasto == 'Altro')
								$guasto .= Tools::getValue('guasto_altro');
								
							$phone = Tools::getValue('bdl_phone');
							if($phone == 'Altro')
								$phone .= Tools::getValue('bdl_phone_altro');	
					
							$causa_guasto = Tools::getValue('causa_guasto');
							if($causa_guasto == 'Altro')
								$causa_guasto .= Tools::getValue('causa_guasto_altro');
							
							$descrizione_bdl = Tools::getValue('descrizione');
							if($descrizione_bdl == 'Altro')
								$descrizione_bdl .= Tools::getValue('descrizione_altro');
								
							$impianto_ubicazione = Tools::getValue('impianto_ubicazione');
							if(!is_numeric($impianto_ubicazione))
								$impianto_ubicazione = Tools::getValue('impianto_ubicazione_manuale');
								
							Db::getInstance()->executeS("INSERT INTO bdl (id_bdl, id_customer, id_address, phone, data_richiesta, richiesto_da, data_effettuato, effettuato_da, impianto_ubicazione, contratto_assistenza, manutenzione_ordinaria, manutenzione_straordinaria, motivo_chiamata, guasto, causa_guasto, descrizione, intervento_svolto, prodotti, dettaglio_costi, note, note_private, rif_ordine, pagato, fatturato, nessun_addebito, invio_controllo, invio_contabilita, date_add, date_upd ".(Tools::getIsset('riferimento') ? ', riferimento' : '').") VALUES (NULL, '".Tools::getValue('id_customer')."', '".Tools::getValue('indirizzo_bdl')."', '".$phone."', '".$data_richiesta."', '".Tools::getValue('richiesto_da')."', '".$data_effettuato."', '".Tools::getValue('effettuato_da')."', '".addslashes($impianto_ubicazione)."', '".Tools::getValue('contratto_assistenza')."', '".(Tools::getIsset('manutenzione_ordinaria') ? 1 : 0)."', '".(Tools::getIsset('manutenzione_straordinaria') ? 1 : 0)."', '".addslashes(Tools::getValue('motivo_chiamata'))."', '".addslashes($guasto)."', '".addslashes($causa_guasto)."',  '".addslashes($descrizione_bdl)."', '".addslashes(Tools::getValue('intervento_svolto'))."', '', '', '".addslashes(Tools::getValue('note_bdl'))."',  '".addslashes(Tools::getValue('note_private_bdl'))."', '".addslashes(Tools::getValue('rif_ordine'))."', '".(Tools::getIsset('pagato') ? 1 : 0)."', '".(Tools::getIsset('fatturato') ? 1 : 0)."', '".(Tools::getIsset('nessun_addebito') ? 1 : 0)."', '".(Tools::getIsset('invio_controllo') ? 1 : 0)."', '".(Tools::getIsset('invio_contabilita') ? 1 : 0)."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."' ".(Tools::getIsset('riferimento') ? ", '".Tools::getValue('riferimento')."'" : '').")");
							
						
						}
						
						$string_bdl_order = '';
						
						if(Tools::getIsset('nuovobdl'))
							$id_bdl = mysqli_insert_id($link_glob);
						else
							$id_bdl = Tools::getValue('id_bdl');
						
							$identificatore_univoco_cliente = $customer->id;
							$partita_iva_cliente = $customer->vat_number;
							$codice_fiscale =  $customer->tax_code;
							
						
						$id_bdl_cart = Db::getInstance()->getValue('SELECT id_cart FROM cart ORDER BY id_cart DESC');
						$id_bdl_cart++;
						
						$dettaglio_bdl['chilometri_percorsi_tipo'] = str_replace(",",".",$_POST['chilometri_percorsi_tipo']);
						$dettaglio_bdl['chilometri_percorsi_qta'] = str_replace(",",".",$_POST['chilometri_percorsi_qta']);
						$dettaglio_bdl['chilometri_percorsi_sconto'] = str_replace(",",".",$_POST['chilometri_percorsi_sconto']);
						$dettaglio_bdl['chilometri_percorsi_unitario'] = str_replace(",",".",$_POST['chilometri_percorsi_unitario']);
						
						if($dettaglio_bdl['chilometri_percorsi_unitario'] > 0)
						{
							$product_bdl_data = Db::getInstance()->getRow('SELECT * FROM product p JOIN product_lang pl ON p.id_product = pl.id_product WHERE id_lang = 5 AND p.id_product = (SELECT id_product FROM product WHERE reference = "'.$dettaglio_bdl['chilometri_percorsi_tipo'].'")');
							
							$string_bdl_order .= 'INSERT INTO cart_product (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, sconto_extra, prezzo_acquisto, no_acq, id_specific_price, sort_order, bundle, date_add) VALUES ('.$id_bdl_cart.', '.$product_bdl_data['id_product'].', 0,'.$dettaglio_bdl['chilometri_percorsi_qta'].','.($dettaglio_bdl['chilometri_percorsi_unitario']-(($dettaglio_bdl['chilometri_percorsi_unitario']*$dettaglio_bdl['chilometri_percorsi_sconto'])/100)).',0,"'.$product_bdl_data['name'].'",0,'.$dettaglio_bdl['chilometri_percorsi_sconto'].','.$product_bdl_data['wholesale_price'].',0,0,0,0,"'.date('Y-m-d H:i:s').'");|; ';
							
							
						}
						$dettaglio_bdl['diritto_chiamata_tipo'] = str_replace(",",".",$_POST['diritto_chiamata_tipo']);
						$dettaglio_bdl['diritto_chiamata_qta'] = str_replace(",",".",$_POST['diritto_chiamata_qta']);
						$dettaglio_bdl['diritto_chiamata_sconto'] = str_replace(",",".",$_POST['diritto_chiamata_sconto']);
						$dettaglio_bdl['diritto_chiamata_unitario'] = str_replace(",",".",$_POST['diritto_chiamata_unitario']);
						
						if($dettaglio_bdl['diritto_chiamata_unitario'] > 0)
						{
							$product_bdl_data = Db::getInstance()->getRow('SELECT * FROM product p JOIN product_lang pl ON p.id_product = pl.id_product WHERE id_lang = 5 AND p.id_product = (SELECT id_product FROM product WHERE reference = "'.$dettaglio_bdl['diritto_chiamata_tipo'].'")');
							
							$string_bdl_order .= 'INSERT INTO cart_product (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, sconto_extra, prezzo_acquisto, no_acq, id_specific_price, sort_order, bundle, date_add) VALUES ('.$id_bdl_cart.', '.$product_bdl_data['id_product'].', 0,'.$dettaglio_bdl['diritto_chiamata_qta'].','.($dettaglio_bdl['diritto_chiamata_unitario']-(($dettaglio_bdl['diritto_chiamata_unitario']*$dettaglio_bdl['diritto_chiamata_sconto'])/100)).',0,"'.$product_bdl_data['name'].'",0,'.$dettaglio_bdl['diritto_chiamata_sconto'].','.$product_bdl_data['wholesale_price'].',0,0,0,0,"'.date('Y-m-d H:i:s').'");|; ';
						}
						
						$dettaglio_bdl['costo_manodopera_tipo'] = str_replace(",",".",$_POST['costo_manodopera_tipo']);
						$dettaglio_bdl['costo_manodopera_qta'] = str_replace(",",".",$_POST['costo_manodopera_qta']);
						$dettaglio_bdl['costo_manodopera_sconto'] = str_replace(",",".",$_POST['costo_manodopera_sconto']);
						$dettaglio_bdl['costo_manodopera_unitario'] = str_replace(",",".",$_POST['costo_manodopera_unitario']);
						
						if($dettaglio_bdl['costo_manodopera_unitario'])
						{
							$product_bdl_data = Db::getInstance()->getRow('SELECT * FROM product p JOIN product_lang pl ON p.id_product = pl.id_product WHERE id_lang = 5 AND p.id_product = (SELECT id_product FROM product WHERE reference = "'.$dettaglio_bdl['costo_manodopera_tipo'].'")');
							
							$string_bdl_order .= 'INSERT INTO cart_product (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, sconto_extra, prezzo_acquisto, no_acq, id_specific_price, sort_order, bundle, date_add) VALUES ('.$id_bdl_cart.', '.$product_bdl_data['id_product'].', 0,'.$dettaglio_bdl['costo_manodopera_qta'].','.($dettaglio_bdl['costo_manodopera_unitario']-(($dettaglio_bdl['costo_manodopera_unitario']*$dettaglio_bdl['costo_manodopera_sconto'])/100)).',0,"'.$product_bdl_data['name'].'",0,'.$dettaglio_bdl['costo_manodopera_sconto'].','.$product_bdl_data['wholesale_price'].',0,0,0,0,"'.date('Y-m-d H:i:s').'");|; ';
						}
						
						$dettaglio_bdl['num_varie'] = $_POST['num_varie'];
						
						$num_varie = $_POST['num_varie']; 
						
						$totale_bdl_u = 0;
						$totale_bdl_u += (($dettaglio_bdl['diritto_chiamata_unitario']-(($dettaglio_bdl['diritto_chiamata_unitario']*$dettaglio_bdl['diritto_chiamata_sconto'])/100))*$dettaglio_bdl['diritto_chiamata_qta'])+(($dettaglio_bdl['chilometri_percorsi_unitario']-(($dettaglio_bdl['chilometri_percorsi_unitario']*$dettaglio_bdl['chilometri_percorsi_sconto'])/100))*$dettaglio_bdl['chilometri_percorsi_qta'])+(($dettaglio_bdl['costo_manodopera_unitario']-(($dettaglio_bdl['costo_manodopera_unitario']*$dettaglio_bdl['costo_manodopera_sconto'])/100))*$dettaglio_bdl['costo_manodopera_qta']);
						
						
						for($i = 0; $i<=$_POST['num_varie']; $i++) {
							
							$dettaglio_bdl['ricambi_e_varie_'.$i.'_qta'] = str_replace(",",".",$_POST['ricambi_e_varie_'.$i.'_qta']);
							$dettaglio_bdl['ricambi_e_varie_'.$i.'_unitario'] = str_replace(",",".",$_POST['ricambi_e_varie_'.$i.'_unitario']);
							$dettaglio_bdl['ricambi_e_varie_'.$i.'_sconto'] = str_replace(",",".",$_POST['ricambi_e_varie_'.$i.'_sconto']);
							$dettaglio_bdl['ricambi_e_varie_'.$i.'_descrizione'] = str_replace(",",".",$_POST['ricambi_e_varie_'.$i.'_descrizione']);
							$dettaglio_bdl['ricambi_e_varie_'.$i.'_tipo'] = $_POST['ricambi_e_varie_'.$i.'_tipo'];
							$totale_bdl_u += ($dettaglio_bdl['ricambi_e_varie_'.$i.'_unitario']-(($dettaglio_bdl['ricambi_e_varie_'.$i.'_unitario']*$dettaglio_bdl['ricambi_e_varie_'.$i.'_sconto'])/100))*$dettaglio_bdl['ricambi_e_varie_'.$i.'_qta'];
							
							$prezzo_ricambi_e_varie = str_replace(".",",",$prezzo_ricambi_e_varie);
							
							$product_bdl_data = Db::getInstance()->getRow('SELECT * FROM product p JOIN product_lang pl ON p.id_product = pl.id_product WHERE id_lang = 5 AND p.id_product = (SELECT id_product FROM product WHERE reference = "'.$dettaglio_bdl['ricambi_e_varie_'.$i.'_tipo'].'")');
						
							if($product_bdl_data['id_product'] > 0)
							{
								$string_bdl_order .= 'INSERT INTO cart_product (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, sconto_extra, prezzo_acquisto, no_acq, id_specific_price, sort_order, bundle, date_add) VALUES ('.$id_bdl_cart.', '.$product_bdl_data['id_product'].', 0,'.$dettaglio_bdl['ricambi_e_varie_'.$i.'_qta'].','.($dettaglio_bdl['ricambi_e_varie_'.$i.'_unitario']-(($dettaglio_bdl['ricambi_e_varie_'.$i.'_unitario']*$dettaglio_bdl['ricambi_e_varie_'.$i.'_sconto'])/100)).',0,"'.$product_bdl_data['name'].'",0,'.$dettaglio_bdl['ricambi_e_varie_'.$i.'_sconto'].','.$product_bdl_data['wholesale_price'].',0,0,0,0,"'.date('Y-m-d H:i:s').'");|; ';
							}
						
						}
						
						
						
						
						if(Tools::getValue('rif_ordine') > 0)
						{
							$dettaglio_bdl = '';
							Db::getInstance()->executeS('UPDATE bdl SET importo = 0 WHERE id_bdl = '.$id_bdl.'');
						}
						else
						{
							$dettaglio_bdl = serialize($dettaglio_bdl);
							Db::getInstance()->executeS('UPDATE bdl SET importo = '.$totale_bdl_u.' WHERE id_bdl = '.$id_bdl.'');
						}
						Db::getInstance()->executeS("UPDATE bdl SET dettaglio_costi = '".$dettaglio_bdl."' WHERE id_bdl = ".$id_bdl."");
						
						
						
						if(Tools::getIsset('invio_controllo') && Tools::getValue('invio_controllo') == 1)
						{
						}
						
						if(Tools::getValue('rif_ordine') > 0)
						{
							Db::getInstance()->executeS('UPDATE bdl SET invio_controllo = 2, invio_contabilita = 2 WHERE id_bdl = '.$id_bdl);
						
						}
						
						if(Tools::getValue('contratto_assistenza') == 1 && Tools::getIsset('manutenzione_ordinaria'))
						{
							Db::getInstance()->executeS('UPDATE bdl SET pagato = 2, fatturato = 2, invio_controllo = 2, invio_contabilita = 2 WHERE id_bdl = '.$id_bdl);
						
						}
						
						if(Tools::getIsset('invio_contabilita') && !Tools::getIsset('nessun_addebito') && Tools::getValue('invio_contabilita') == 1 && (Tools::getValue('rif_ordine') == 0 || empty($_POST['rif_ordine'])) && $totale_bdl_u != 0 && !Tools::getIsset('fatturato'))
						{
						
						
							$note_bdl = 'BDL '.$id_bdl.' del '.date("d/m/Y", strtotime(Db::getInstance()->getValue('SELECT data_effettuato FROM bdl WHERE id_bdl = '.$id_bdl)));
							
							$bdl_dati = Db::getInstance()->getRow('SELECT * FROM bdl WHERE id_bdl = '.$id_bdl);
							
							$note_bdl .= '<br /><br /><strong>Ticket:</strong> '.Customer::trovaSigla(substr($bdl_dati['riferimento'],1), substr($bdl_dati['riferimento'],0,1));
							$note_bdl .= '<br /><br /><strong>Descrizione intervento:</strong> '.$bdl_dati['descrizione'].'';
							$note_bdl .= '<br /><br /><strong>Motivo chiamata:</strong> '.$bdl_dati['motivo_chiamata'].'';
							$note_bdl .= '<br /><br /><strong>Guasto:</strong> '.$bdl_dati['guasto'].'';
							$note_bdl .= '<br /><br /><strong>Causa guasto riscontrata:</strong> '.$bdl_dati['causa_guasto'].'';
							
							// creo carrello e trasformo in ordine
							
							$indirizzo = Db::getInstance()->getValue('SELECT id_address FROM address WHERE id_customer = '.$customer->id.' AND deleted = 0 AND active = 1 AND fatturazione = 1');
							
							$bdl_order_products = explode(";|;",$string_bdl_order);
						
							$payment_default = Db::getInstance()->getValue('SELECT pagamento FROM customer_amministrazione WHERE id_customer = '.$id_customer);
							
							if($payment_default == '')
								$payment_default = 'Bonifico';
						
							$transport_bdl = Db::getInstance()->getValue('SELECT id_carrier FROM carrier WHERE name LIKE "%grat%" AND deleted = 0');
							
							Db::getInstance()->executeS('INSERT INTO cart (id_cart, riferimento, id_carrier, id_lang, id_currency, id_customer, id_address_invoice, id_address_delivery, name, cig, cup, ipa, validita, date_add, date_upd, created_by,  id_employee, in_carico_a, preventivo, transport, payment, note, rif_ordine) VALUES ('.$id_bdl_cart.', "BDL'.$id_bdl.'", '.$transport_bdl.', 5, 1, "'.$customer->id.'", "'.$indirizzo.'", "'.$indirizzo.'", "BDL n. '.$id_bdl.'", "", "", "", "", "'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'", '.$cookie->id_employee.','.$cookie->id_employee.','.$cookie->id_employee.',1, "'.$transport_bdl.':0", "'.$payment_default.'", "'.$note_bdl.'", "BDL'.$id_bdl.'")');
							
							foreach($bdl_order_products as $b)
							{
								Db::getInstance()->executeS($b);
							}
							include("../modules/pss_clearcarts/pss_clearcarts.php");
							include("../modules/pss_clearcarts/AdminPssClearCarts.php");
							$apcc = new AdminPssClearCarts();
							
							$apcc->orderThisCart($id_bdl_cart);
							//$order = Db::getInstance()->getValue('SELECT id_order FROM orders WHERE id_cart = '.$id_cart_ta.'');
							
							
							// fine creazione carrello e trasformazione in ordine
						
							$subject = "Nuovo BDL da fatturare";
			
							$message = "<html>
							<head>
								<title>Nuovo BDL da fatturare</title>
							</head>
							<body>
							".$this->generatePDFBDL($id_bdl)."
							</body>
							</html>";
									
							$headers  = 'MIME-Version: 1.0' . "\n";
							$headers .= 'Content-Type: text/html' ."\n";
							$headers .= 'From: "Ezdirect" <info@ezdirect.it>' . "\n";
							mail("barbara.giorgini@ezdirect.it",$subject,$message,$headers );
						
						
						}
						
						if(Tools::isSubmit('bdlCarrello'))
						{
							
							
							
							echo "1**"; die();
							
						}
						
						if(Tools::isSubmit('bdlOrdine'))
						{
							
							
							
							echo "2**"; die();
							
						}

						/*if(Tools::getIsset('backz') && Tools::getValue('backz') == 'lista')
						{
							$tokenBDL = Tools::getAdminToken('AdminBDL'.(int)Tab::getIdFromClassName('AdminBDL').(int)$cookie->id_employee);
					
							Tools::redirectAdmin("index.php?tab=AdminBDL&conf=4&token=".$tokenBDL);
						}*/
						
						if(Tools::getIsset('viewticket'))
							Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&viewticket&id_customer_thread='.Tools::getValue('id_customer_thread').'&token='.$this->token.'&tab-container-1=6&conf=4&tab-container-ticket=5#ticket-5');
						else
							Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&edit_bdl='.$id_bdl.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=14');
						
					}
					else {
						if(isset($_GET['tab-container-1']) && ($_GET['tab-container-1'] == 14))
						{
							echo '<div style="clear:both"></div>';
							Customer::BDLform($customer);
						}	
					}
				}
				
				else if(Tools::getIsset('id_bdl') && Tools::getIsset('id_bdl') > 0) {
				
				}
				else {
					if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 14) {
						$count_bdl = Db::getInstance()->getValue("SELECT count(id_bdl) FROM bdl WHERE id_customer = ".$customer->id."");			
						if($count_bdl == 0) {
							echo "<div style='float:left; margin-left:20px; margin-top:5px;'>Non sono presenti buoni di lavoro per questa anagrafica.</div>
							
							<a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&nuovobdl&token=".$this->token."&tab-container-1=14' class='button' style='display:block;float:left; margin-left:10px'><img src='../img/admin/prefs.gif' alt='Nuovo BDL' title='Nuovo BDL' />&nbsp;&nbsp;&nbsp;Genera nuovo BDL</a>
							<div style='clear:both'></div>";
							
							
						} else {
							echo "<div style='float:left; margin-left:20px; margin-top:5px;'>Sono presenti ".$count_bdl." buoni di lavoro per questa anagrafica.</div>
							<a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&nuovobdl&token=".$this->token."&tab-container-1=14' class='button' style='display:block;float:left; margin-left:10px'><img src='../img/admin/prefs.gif' alt='Nuovo BDL' title='Nuovo BDL' />&nbsp;&nbsp;&nbsp;Genera nuovo BDL</a>
							<div style='clear:both'></div>";
							echo "<table class='table'>";
							echo "<thead>";
							echo "<tr>";
							echo "<th>N. BDL</th>";
							echo "<th>Data BDL</th>";
							echo "<th>Motivo</th>";
							echo "<th>Importo</th>";
							echo "<th>Status</th>";
							echo "<th>Fatto da</th>";
							echo "<th>Azioni</th>";
							echo "</tr>";
							echo "</thead>";
							echo "<tbody>";
							$bdl = Db::getInstance()->executeS("SELECT * FROM bdl WHERE id_customer = ".$customer->id." ORDER BY id_bdl DESC");
							foreach($bdl as $bdl) {
							
								$status_bdl = Db::getInstance()->getValue('SELECT (
								CASE WHEN a.rif_ordine > 0
								THEN "closed"
								WHEN a.nessun_addebito = 1
								THEN "closed"
								WHEN a.id_bdl < 384
								THEN "closed"
								WHEN a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 0
								THEN "pending1"
								WHEN a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 1
								THEN "closed"
								WHEN a.invio_controllo = 2 AND a.invio_contabilita = 2
								THEN "closed"
								WHEN a.rif_ordine = 0 AND a.invio_controllo = 0
								THEN "open"
								ELSE "open"
								END
								) stato FROM bdl a WHERE id_bdl = '.$bdl['id_bdl']);
								
								switch($status_bdl)
								{
									case 'open': $stbdl = '<img src="../img/admin/status_red.gif" alt="A" title="A" /> A'; break;
									case 'closed': $stbdl = '<img src="../img/admin/status_green.gif" alt="C" title="C" /> C'; break;
									case 'pending1': $stbdl = '<img src="../img/admin/status_orange.gif" alt="L" title="L" /> L'; break;
									default: $stbdl = ''; break;
								}
								
								echo "<tr>";
								echo "<td>".$bdl['id_bdl']."</td>";
								echo "<td>".Tools::displayDate($bdl['data_richiesta'],$cookie->id_lang,false)."</td>";
								echo "<td style='width:500px'>".$bdl['motivo_chiamata']."</td>";
								echo "<td style='text-align:right'>".Tools::displayPrice($bdl['importo'])."</td>";
								echo "<td>".$stbdl."</td>";
								echo "<td>".Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".")  FROM employee WHERE id_employee = '.$bdl['effettuato_da'])."</td>";
								echo "<td><!-- <a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&id_bdl=".$bdl['id_bdl']."&token=".$this->token."&tab-container-1=14'><img src='../img/admin/details.gif' alt='Dettagli' title='Dettagli' /></a> -->
								<a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&edit_bdl=".$bdl['id_bdl']."&token=".$this->token."&tab-container-1=14'><img src='../img/admin/edit.gif' alt='Modifica' title='Modifica' /></a>
								
								".($cookie->profile != 8 ? "<a href='".$currentIndex."&id_customer=".$customer->id."&viewcustomer&delete_bdl=".$bdl['id_bdl']."&token=".$this->token."&tab-container-1=14' onclick=\"javascript: var surec=window.confirm('Sei sicuro?'); if (surec) { return true; } else { return false; }\"><img src='../img/admin/delete.gif' alt='Cancella' title='Cancella' /></a>" : '')."
								";
								echo "</tr>";
							}
							echo "</tbody></table><br /><br />";
						}
					}
				}
			}
			
		}
		// FINE BDL
		
		//INIZIO MAIL
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 15) {
			// MAIL
			if($cookie->profile == 7)
				echo 'Non hai i permessi per visualizzare questa sezione';
			else
			{
				echo '<div class="tab1" id="mail-customer">';
				echo '<div style="float:left"><h2>eMail</h2></div>';
				echo '<div style="clear:both"></div>';
				
				echo '
				
				<script type="text/javascript" src="../js/jquery/jquery.tablesorter.min.js"></script> 
				<script type="text/javascript" src="../js/jquery/jquery.tablesorter.widgets.js"></script> 
				<script type="text/javascript" src="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.js"></script> 
				<link rel="stylesheet" type="text/css" href="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.css" /> 
				<script type="text/javascript">
				$(document).ready(function() 
					{ 
						$("#mail_table_received").tablesorter({ widthFixed: true, widgets: ["zebra"]}).tablesorterPager(
						{
							
							container: $("#pager_ricevute"),
							output: "Da {startRow} a {endRow} ({totalRows})",
							 updateArrows: true,
							 page:0,
							 size:10,
							 fixedHeight:true,
							 removeRows:false,
							 cssNext: ".next",
							// previous page arrow
							cssPrev: ".prev",
							// go to first page arrow
							cssFirst: ".first",
							// go to last page arrow
							cssLast: ".last",
							// select dropdown to allow choosing a page
							cssGoto: ".gotoPage",
							// location of where the "output" is displayed
							cssPageDisplay: ".pagedisplay",
							// dropdown that sets the "size" option
							cssPageSize: ".pagesize",
							// class added to arrows when at the extremes 
							// (i.e. prev/first arrows are "disabled" when on the first page)
							// Note there is no period "." in front of this class name
							cssDisabled: "disabled"
						}
						); 
						
						$("#mail_table_sent").tablesorter({ widthFixed: true, widgets: ["zebra"]}).tablesorterPager(
						{
							
							container: $("#pager_inviate"),
							output: "Da {startRow} a {endRow} ({totalRows})",
							 updateArrows: true,
							 page:0,
							 size:10,
							 fixedHeight:true,
							 removeRows:false,
							 cssNext: ".next",
							// previous page arrow
							cssPrev: ".prev",
							// go to first page arrow
							cssFirst: ".first",
							// go to last page arrow
							cssLast: ".last",
							dateFormat : "ddmmyyyy", // set the default date format
							// select dropdown to allow choosing a page
							cssGoto: ".gotoPage",
							// location of where the "output" is displayed
							cssPageDisplay: ".pagedisplay",
							// dropdown that sets the "size" option
							cssPageSize: ".pagesize",
							// class added to arrows when at the extremes 
							// (i.e. prev/first arrows are "disabled" when on the first page)
							// Note there is no period "." in front of this class name
							cssDisabled: "disabled",
							headers: {
							  3: { sorter: "shortDate", dateFormat: "ddmmyy" } //, dateFormat will parsed as the default above
							
							}
						}					
						
						); 
					} 
				);
				</script>
				
				<script type="text/javascript">';
				echo 'function openMail(id)
				{
					$.ajax({
					  url:"ajax.php?readMail=y",
					  type: "POST",
					  data: {
					  id: id,
					  database: "ez_ml",
					  database_user: "ezdml",
					  database_pass: "as5HJ786A/&4g1x",
					  database_server: "localhost"
					  },
					  success:function(r){
						$("#mail_info").html(r);
						$("html, body").animate({ scrollTop: $("#mail_info").offset().top }, 100);
					  },
					  error: function(xhr,stato,errori){
						 alert("Errore durante l\'operazione:"+xhr.status);
					  }
					});
				}';
				echo '</script>';
			
				
				$connection = mysqli_connect("localhost","ezdml", "as5HJ786A/&4g1x");
				mysqli_select_db($connection,"ez_ml");
			
				
				$result_mails = mysqli_query($connection, 'SELECT * FROM mail WHERE type = "received" AND id_customer = '.$customer->id.' ORDER BY data DESC');
		
				$mails = array();
				while($row_mails = mysqli_fetch_array($result_mails))
				{
					$mails[] = $row_mails;
				
				}
				
				$result_mails_inviate = mysqli_query($connection, 'SELECT * FROM mail WHERE type = "sent" AND id_customer = '.$customer->id.' ORDER BY data DESC');
		
				$mails_inviate = array();
				while($row_mails_inviate = mysqli_fetch_array($result_mails_inviate))
				{
					$mails_inviate[] = $row_mails_inviate;
				
				}
				
				echo '<div id="tab-container-mail" class="tab-containers">
				<ul id="tab-container-mail-nav" class="tab-containers-nav">
				<li><a href="#tab_ricevute">Ricevute ('.count($mails).')</a></li>
				<li><a href="#tab_inviate">Inviate ('.count($mails_inviate).')</a></li>
				</ul>

				<div class="yetii-mail" id="tab_ricevute">
				';
		
				if(count($mails) > 0)
				{
					echo '<table id="mail_table_received" class="table tablesorter mail_table" style="width:100%">';
					echo '<thead><tr><th>Da </th><th>A </th><th>Oggetto</th><th data-sorter="shortDate" data-date-format="ddmmyyyy">Data</th><th><img src="../img/admin/attachment.gif" alt="Allegati" title="Allegati" /></th></tr></thead><tbody>';
					foreach($mails as $mail)
					{
						echo '<tr class="riga" style="cursor:pointer" onclick="openMail('.$mail['id'].')">';
						echo '<td>'.$mail['da_intestazione'].'</td>';
						echo '<td>'.$mail['a_intestazione'].'</td>';
						echo '<td>'.$mail['oggetto'].'</td>';
						echo '<td>'.date('d/m/Y H:i:s', strtotime($mail['data'])).'</td>';
						echo '<td>'.($mail['numero_allegati'] > 0 ? $mail['numero_allegati'] : '').'</td>';
						echo '</tr>';
						
					}
					echo '</tbody>
					</table>
					<div class="pager" id="pager_ricevute">
						<span class="first" style="cursor:pointer"><img src="../img/admin/list-prev2.gif" title="Prima" alt="Prima" /></span>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="prev" style="cursor:pointer"><img src="../img/admin/list-prev.gif" title="Precedente" alt="Precedente" /></span>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="pagedisplay"></span>
						<span class="next" style="cursor:pointer"><img src="../img/admin/list-next.gif" title="Successiva" alt="Successiva" /></span>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="last" style="cursor:pointer"><img src="../img/admin/list-next2.gif" title="Ultima" alt="Ultima" /></span>
						<select class="pagesize">
						  <option value="10">10</option>
						  <option value="20">20</option>
						  <option value="30">30</option>
						  <option value="40">40</option>
						  <option value="all">Tutto</option>
						</select>
						 <select class="gotoPage" title="Select page number"></select>
					</div>
					';
							
				}
				else
					echo 'Nessuna mail ricevuta';
				$connection = mysqli_connect(_DB_SERVER_,_DB_USER_, _DB_PASSWD_);
				mysqli_select_db($connection,_DB_NAME_);
			
				echo '
				</div>
				<div class="yetii-mail" id="tab_inviate">';
				
				if(count($mails_inviate) > 0)
				{
					echo '<table id="mail_table_sent" class="table tablesorter mail_table" style="width:100%">';
					echo '<thead><tr><th>Da </th><th>A </th><th>Oggetto</th><th  data-sorter="shortDate" data-date-format="ddmmyyyy">Data</th><th><img src="../img/admin/attachment.gif" alt="Allegati" title="Allegati" /></th></tr></thead><tbody>';
					foreach($mails_inviate as $maili)
					{
						echo '<tr class="riga" style="cursor:pointer" onclick="openMail('.$maili['id'].')">';
						echo '<td>'.$maili['da_intestazione'].'</td>';
						echo '<td>'.$maili['a_intestazione'].'</td>';
						echo '<td>'.$maili['oggetto'].'</td>';
						echo '<td>'.date('d/m/Y H:i:s', strtotime($maili['data'])).'</td>';
						echo '<td>'.($maili['numero_allegati'] > 0 ? $maili['numero_allegati'] : '').'</td>';
						echo '</tr>';
						
					}
					echo '</tbody>
					</table>
					<div class="pager" id="pager_inviate">
						<span class="first" style="cursor:pointer"><img src="../img/admin/list-prev2.gif" title="Prima" alt="Prima" /></span>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="prev" style="cursor:pointer"><img src="../img/admin/list-prev.gif" title="Precedente" alt="Precedente" /></span>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="pagedisplay"></span>
						<span class="next" style="cursor:pointer"><img src="../img/admin/list-next.gif" title="Successiva" alt="Successiva" /></span>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="last" style="cursor:pointer"><img src="../img/admin/list-next2.gif" title="Ultima" alt="Ultima" /></span>
						<select class="pagesize">
						  <option value="10">10</option>
						  <option value="20">20</option>
						  <option value="30">30</option>
						  <option value="40">40</option>
						  <option value="all">Tutto</option>
						</select>
						 <select class="gotoPage" title="Select page number"></select>
					</div>
					';
							
				}
				else
					echo 'Nessuna mail inviata';
				
				echo '
				</div><br />
				</div>
				
				<section><div id="mail_info"></div></section><script type="text/javascript" src="../js/jquery/scoper.min.js"></script> ';
				
				echo '<script type="text/javascript">
						var tabber_mail = new Yetii({
						id: "tab-container-mail",
						tabclass: "yetii-mail",
					});
				</script>
				';
				echo '</div>'; // fine mail-customer
			}
		}
			
		// FINE MAIL	
		
		
		// INIZIO TELEFONATE
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 16) {
			if($cookie->profile == 7)
				echo 'Non hai i permessi per visualizzare questa sezione';
			else
			{
			
				echo '<div class="tab1" id="telefonate-customer">';
				echo '<div style="float:left"><h2>Telefonate</h2></div>';
				echo '<div style="clear:both"></div>';
				
				echo '
				
				
				<script type="text/javascript">
				$(document).ready(function() 
					{ 
						$("#telefonate_table_received").tablesorter({ widthFixed: true, widgets: ["zebra"]}).tablesorterPager(
						{
							
							container: $("#pager_telefonate_ricevute"),
							output: "Da {startRow} a {endRow} ({totalRows})",
							 updateArrows: true,
							 page:0,
							 size:10,
							 fixedHeight:true,
							 removeRows:false,
							 cssNext: ".next",
							// previous page arrow
							cssPrev: ".prev",
							// go to first page arrow
							cssFirst: ".first",
							// go to last page arrow
							cssLast: ".last",
							// select dropdown to allow choosing a page
							cssGoto: ".gotoPage",
							// location of where the "output" is displayed
							cssPageDisplay: ".pagedisplay",
							// dropdown that sets the "size" option
							cssPageSize: ".pagesize",
							// class added to arrows when at the extremes 
							// (i.e. prev/first arrows are "disabled" when on the first page)
							// Note there is no period "." in front of this class name
							cssDisabled: "disabled",
							headers: {
							  2: { sorter: "shortDate", dateFormat: "ddmmyy" } //, dateFormat will parsed as the default above
							
							}
						}
						); 
						
						$("#telefonate_table_sent").tablesorter({ widthFixed: true, widgets: ["zebra"]}).tablesorterPager(
						{
							
							container: $("#pager_telefonate_inviate"),
							output: "Da {startRow} a {endRow} ({totalRows})",
							 updateArrows: true,
							 page:0,
							 size:10,
							 fixedHeight:true,
							 removeRows:false,
							 cssNext: ".next",
							// previous page arrow
							cssPrev: ".prev",
							// go to first page arrow
							cssFirst: ".first",
							// go to last page arrow
							cssLast: ".last",
							// select dropdown to allow choosing a page
							cssGoto: ".gotoPage",
							// location of where the "output" is displayed
							cssPageDisplay: ".pagedisplay",
							// dropdown that sets the "size" option
							cssPageSize: ".pagesize",
							// class added to arrows when at the extremes 
							// (i.e. prev/first arrows are "disabled" when on the first page)
							// Note there is no period "." in front of this class name
							cssDisabled: "disabled"
						}					
						
						); 
					} 
				);
				</script>
				';
				
				$telefonate = Db::getInstance()->executeS('SELECT * FROM telefonate WHERE id_customer = '.$customer->id.' AND calltype = "Inbound" ORDER BY datetime DESC');			
				
				$telefonate_inviate = Db::getInstance()->executeS('SELECT * FROM telefonate WHERE id_customer = '.$customer->id.' AND calltype = "Outbound" ORDER BY datetime DESC');
				
				echo '<div id="tab-container-telefonate" class="tab-containers">
				<ul id="tab-container-telefonate-nav" class="tab-containers-nav">
				<li><a href="#tab_ricevute">Ricevute ('.count($telefonate).')</a></li>
				<li><a href="#tab_inviate">Inviate ('.count($telefonate_inviate).')</a></li>
				<li><a href="#tab_stats">Statistiche</a></li>
				</ul>

				<div class="yetii-telefonate" id="tab_ricevute">
				';
		
				if(count($telefonate) > 0)
				{
					echo '<table id="telefonate_table_received" class="table tablesorter mail_table" style="width:100%">';
					echo '<thead><tr><th>Da </th><th>A </th><th class="sorter-shortDate dateFormat-ddmmyyyy">Data</th><th>Durata</th><th>Stato</th></tr></thead><tbody>';
					foreach($telefonate as $telefonata)
					{
						echo '<tr class="riga" style="cursor:pointer">';
						echo '<td>'.$telefonata['src'].'</td>';
						echo '<td>'.$telefonata['dst'].'</td>';
						echo '<td>'.date('d/m/Y H:i:s', strtotime($telefonata['datetime'])).'</td>';
						
						echo '<td style="text-align:right">'.gmdate("H:i:s", $telefonata['billable']).'</td>';
						echo '<td>'.$telefonata['disposition'].'</td>';
						echo '</tr>';
						
					}
					echo '</tbody>
					</table>
					<div class="pager" id="pager_telefonate_ricevute">
						<span class="first" style="cursor:pointer"><img src="../img/admin/list-prev2.gif" title="Prima" alt="Prima" /></span>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="prev" style="cursor:pointer"><img src="../img/admin/list-prev.gif" title="Precedente" alt="Precedente" /></span>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="pagedisplay"></span>
						<span class="next" style="cursor:pointer"><img src="../img/admin/list-next.gif" title="Successiva" alt="Successiva" /></span>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="last" style="cursor:pointer"><img src="../img/admin/list-next2.gif" title="Ultima" alt="Ultima" /></span>
						<select class="pagesize">
						  <option value="10">10</option>
						  <option value="20">20</option>
						  <option value="30">30</option>
						  <option value="40">40</option>
						  <option value="all">Tutto</option>
						</select>
						 <select class="gotoPage" title="Select page number"></select>
					</div>
					';
							
				}
				else
					echo 'Nessuna telefonata ricevuta';
			
				echo '
				</div>
				<div class="yetii-telefonate" id="tab_inviate">';
				
				if(count($telefonate_inviate) > 0)
				{
					echo '<table id="telefonate_table_sent" class="table tablesorter mail_table" style="width:100%">';
					echo '<thead><tr><th>Da </th><th>A </th><th class="sorter-shortDate dateFormat-ddmmyyyy">Data</th><th>Durata</th><th>Stato</th></tr></thead><tbody>';
					foreach($telefonate_inviate as $telefonata)
					{
						echo '<tr class="riga" style="cursor:pointer">';
						echo '<td>'.$telefonata['src'].'</td>';
						echo '<td>'.$telefonata['dst'].'</td>';
						echo '<td>'.date('d/m/Y H:i:s', strtotime($telefonata['datetime'])).'</td>';
						
						echo '<td style="text-align:right">'.gmdate("H:i:s", $telefonata['billable']).'</td>';
						echo '<td>'.$telefonata['disposition'].'</td>';
						echo '</tr>';
						
					}
					echo '</tbody>
					</table>
					<div class="pager" id="pager_telefonate_inviate">
						<span class="first" style="cursor:pointer"><img src="../img/admin/list-prev2.gif" title="Prima" alt="Prima" /></span>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="prev" style="cursor:pointer"><img src="../img/admin/list-prev.gif" title="Precedente" alt="Precedente" /></span>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="pagedisplay"></span>
						<span class="next" style="cursor:pointer"><img src="../img/admin/list-next.gif" title="Successiva" alt="Successiva" /></span>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="last" style="cursor:pointer"><img src="../img/admin/list-next2.gif" title="Ultima" alt="Ultima" /></span>
						<select class="pagesize">
						  <option value="10">10</option>
						  <option value="20">20</option>
						  <option value="30">30</option>
						  <option value="40">40</option>
						  <option value="all">Tutto</option>
						</select>
						 <select class="gotoPage" title="Select page number"></select>
					</div>
					';
							
				}
				else
					echo 'Nessuna telefonata inviata';
				
				echo '
				</div><br />
				
				 ';
				 
				 echo '<div class="yetii-telefonate" id="tab_stats">';
				
				$stats_mese_inviate = Db::getInstance()->executeS('SELECT SUM(durata_ricevute) ricevute, SUM(durata_inviate) inviate, SUM(tel_ricevute) totale_ricevute, SUM(tel_inviate) totale_inviate, datetime FROM (SELECT datetime, id_customer, calltype, IF(calltype LIKE "%inbound%", billable,NULL) AS durata_ricevute, IF(calltype LIKE "%outbound%", billable,NULL) AS durata_inviate,
				IF(calltype LIKE "%inbound%", 1,NULL) AS tel_ricevute, IF(calltype LIKE "%outbound%", 1,NULL) AS tel_inviate,			COUNT(id_telefonata) conto FROM telefonate  WHERE id_customer = '.$customer->id.' GROUP BY id_telefonata) x GROUP BY year(datetime), month(datetime)');
				
				if(count($stats_mese_inviate) > 0)
				{
					$totale_ricevute = 0;
					$totale_inviate = 0;
					$durata_ricevute = 0;
					$durata_inviate = 0;
					$totale_ricevute_reparto = 0;
					$totale_inviate_reparto = 0;
					$durata_ricevute_reparto = 0;
					$durata_inviate_reparto = 0;
					
					echo '
					
					
					<strong>Totali per mese</strong><br /><br />
					<table class="table"><tr><th>Mese</th><th>Totale ricevute</th><th>Durata ricevute</th><th>Totale inviate</th><th>Durata inviate</th></tr>';
					
					foreach($stats_mese_inviate as $telefonate_mese)
					{
						echo '<tr><td>'.date('M Y', strtotime($telefonate_mese['datetime'])).'</td>
						<td style="text-align:right">'.($telefonate_mese['totale_ricevute'] != '' ? $telefonate_mese['totale_ricevute'] : 0).'</td>
						<td style="text-align:right">'.gmdate("H:i:s", $telefonate_mese['ricevute']).'</td>
						<td style="text-align:right">'.($telefonate_mese['totale_inviate'] != '' ? $telefonate_mese['totale_inviate'] : 0).'</td>
						<td style="text-align:right">'.gmdate("H:i:s", $telefonate_mese['inviate']).'</td>
						</tr>';
						
						$totale_ricevute += $telefonate_mese['totale_ricevute'];
						$totale_inviate +=  $telefonate_mese['totale_inviate'];
						$durata_ricevute += $telefonate_mese['ricevute'];
						$durata_inviate +=  $telefonate_mese['inviate'];
						
					}
					
					echo '<tr><th>Tot.</th><th>'.$totale_ricevute.'</th><th>'.gmdate("H:i:s", $durata_ricevute).'</th><th>'.$totale_inviate.'</th><th>'.gmdate("H:i:s", $durata_inviate).'</th></tr></table><br /><br />';
					
					$stats_mese_reparto = Db::getInstance()->executeS('SELECT reparto, SUM(durata_ricevute) ricevute, SUM(durata_inviate) inviate, SUM(tel_ricevute) totale_ricevute, SUM(tel_inviate) totale_inviate, datetime FROM (SELECT datetime, id_customer, calltype, IF(calltype LIKE "%inbound%", billable,NULL) AS durata_ricevute, IF(calltype LIKE "%outbound%", billable,NULL) AS durata_inviate,
					IF(calltype LIKE "%inbound%", 1,NULL) AS tel_ricevute, IF(calltype LIKE "%outbound%", 1,NULL) AS tel_inviate,
					(CASE WHEN calltype LIKE "%inbound%" THEN dst ELSE src END) reparto,
					COUNT(id_telefonata) conto FROM telefonate  WHERE id_customer = '.$customer->id.' GROUP BY id_telefonata) x GROUP BY reparto');
					
					echo '
					
					<strong>Totali per reparto</strong> (da sempre)<br /><br />
					<table class="table"><tr><th>Reparto</th><th>Totale ricevute</th><th>Durata ricevute</th><th>Totale inviate</th><th>Durata inviate</th></tr>';
					
					foreach($stats_mese_reparto as $telefonate_reparto)
					{
						echo '<tr><td>'.date('M Y', strtotime($telefonate_reparto['datetime'])).'</td>
						<td style="text-align:right">'.($telefonate_reparto['totale_ricevute'] != '' ? $telefonate_reparto['totale_ricevute'] : 0).'</td>
						<td style="text-align:right">'.gmdate("H:i:s", $telefonate_reparto['ricevute']).'</td>
						<td style="text-align:right">'.($telefonate_reparto['totale_inviate'] != '' ? $telefonate_reparto['totale_inviate'] : 0).'</td>
						<td style="text-align:right">'.gmdate("H:i:s", $telefonate_reparto['inviate']).'</td>
						</tr>';
						
						$totale_ricevute_reparto += $telefonate_reparto['totale_ricevute'];
						$totale_inviate_reparto +=  $telefonate_reparto['totale_inviate'];
						$durata_ricevute_reparto += $telefonate_reparto['ricevute'];
						$durata_inviate_reparto +=  $telefonate_reparto['inviate'];
						
					}
					
					echo '<tr><th>Tot.</th><th>'.$totale_ricevute_reparto.'</th><th>'.gmdate("H:i:s", $durata_ricevute_reparto).'</th><th>'.$totale_inviate_reparto.'</th><th>'.gmdate("H:i:s", $durata_inviate_reparto).'</th></tr></table><br /><br />';
					
				}
				
				echo '<div style="clear:both"></div><br />
				</div>
				</div>
				 ';
				
				echo '<script type="text/javascript">
						var tabber_telefonate = new Yetii({
						id: "tab-container-telefonate",
						tabclass: "yetii-telefonate",
					});
				</script>
				';
				echo '</div>'; // fine telefonate-customer
		
			}
		
		
		}
		
		// FINE TELEFONATE
		
		// INIZIO EZCLOUD
		
		if(isset($_GET['tab-container-1']) && $_GET['tab-container-1'] == 17) {
			if($cookie->profile == 7)
				echo 'Non hai i permessi per visualizzare questa sezione';
			else
			{
				echo '<h2>Ezcloud</h2>';
				
				if(Tools::getIsset('crea_ezcloud'))
				{
					/*Db::getInstance()->executeS('INSERT INTO ezcloud (id_ezcloud, id_customer, active, date_add, date_upd) VALUES (NULL, '.$customer->id.', 1, "'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'")');*/
					
					Tools::redirectAdmin($currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=17');
				}
				
				$ezcloud_attivi = Db::getInstance()->getValue('SELECT count(id_ezcloud) FROM ezcloud WHERE id_customer = '.$customer->id);
				
				
				if($ezcloud_attivi == 0)
					echo 'Questo cliente non ha ancora alcun centralino virtuale Ezcloud attivo. <!-- Vuoi attivare il primo?<br /><br /><form method="post" action="'.$currentIndex."&id_customer=".$customer->id."&viewcustomer&nuovoezcloud&token=".$this->token.'&tab-container-1=17"><input type="submit" class="button" name="crea_ezcloud" value="S&igrave;, voglio attivare un centralino Ezcloud su questo cliente" /></form> -->';
				else
				{
					if(Tools::getIsset('viewezcloud'))
					{
						$id_ezcloud = Tools::getValue('id_ezcloud');
						
						$valutazione = Db::getInstance()->getRow('SELECT * FROM ezcloud_valutazione WHERE id_ezcloud = '.$id_ezcloud.' AND id_customer = '.$customer->id);
						
						echo '<strong>Ezcloud n. '.$id_ezcloud.'</strong><br /><br />
						Valutazione cliente<br /><br />
						Qualit&agrave; del servizio: '.$valutazione['servizio'].'<br />
						Funzionalit&agrave; del centralino: '.$valutazione['funzionalita'].'<br />
						Qualit&agrave; del supporto: '.$valutazione['supporto'].'<br />';
						echo '<br /><br />.....................';
					}
					else
					{						
						echo '<strong>Centralini virtuali Ezcloud attivi</strong><br /><br />
						<table class="table"><thead><tr><th>Id</th><th>Status</th><th>Attivato il</th><th>Qt. linee</th><th>Qt. interni</th><th>Costo singolo interno</th><th></th></tr></thead><tbody>';
						
						$ezcloud_centralini = Db::getInstance()->executeS('SELECT * FROM ezcloud WHERE id_customer = '.$customer->id);
						
						foreach($ezcloud_centralini as $ezcloud_centralino)
						{
							echo '<tr><td>'.$ezcloud_centralino['id_ezcloud'].'</td><td>'.($ezcloud_centralino['active'] == 1 ? 'Attivo' : 'Non attivo').'</td><td>'.Tools::displayDate($ezcloud_centralino['date_add'],5).'</td><td style="text-align:right">'.$ezcloud_centralino['linee'].'</td><td style="text-align:right">'.$ezcloud_centralino['interni'].'</td><td style="text-align:right">'.number_format($ezcloud_centralino['prezzo'],2,",","").'</td><td><a href="'.$currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&viewezcloud&id_ezcloud='.$ezcloud_centralino['id_ezcloud'].'&token='.$this->token.'&tab-container-1=17">Vedi</a></td></tr>';
						}
						echo '</tbody></table>';
					}	
				}
				
			}
			
		}

		// FINE EZCLOUD 
		
		$tokenTickets = Tools::getAdminToken('AdminCustomerThreads'.(int)(Tab::getIdFromClassName('AdminCustomerThreads')).(int)($cookie->id_employee));
		/*	echo "<br /><br /><a href='?tab=AdminCustomerThreads&token=".$tokenTickets."&type=2'><img src='../img/ticket-a-120.jpg' alt='Ticket amministrativi' title='Ticket amministrativi' /></a>";
		echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."&type=8'><img src='../img/ticket-d-120.jpg' alt='Ticket ordini' title='Ticket ordini' /></a>";
		echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."&type=4'><img src='../img/ticket-t-120.jpg' alt='Ticket assistenza' title='Ticket assistenza' /></a>";
		echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."&type=preventivo'><img src='../img/ticket-p-120.jpg' alt='Ticket commerciale' title='Ticket commerciale' /></a>";
		echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."&type=3'><img src='../img/ticket-v-120.jpg' alt='Ticket rivenditori' title='Ticket rivenditori' /></a>";
		echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."'><img src='../img/ticket-tutti-120.jpg' alt='Tutti i ticket' title='Tutti i ticket' /></a>";
		echo "<a href='?tab=AdminCustomerThreads&token=".$tokenTickets."&data=oggi'><img src='../img/attivita-oggi-120.gif' alt='Attivita di oggi' title='Attivita di oggi' /></a>";
		*/
		
		if (($hook = Module::hookExec('adminCustomers', array('id_customer' => $customer->id))) !== false)
					echo '<div style="clear:both"></div><div style="margin-top:30px"><br /><br />'.$hook.'';
				echo '<br /><br />
				<iframe style="display:none; margin-left:-10px" src="" id="ordini-frame" width="920" height="800"></iframe></div>'
				
				;
		
		echo '<br /><br /><a href="'.$currentIndex.'&token='.$this->token.'"><img src="../img/admin/arrow2.gif" /> '.$this->l('Back to customer list').'</a><br />
		</div>
		<script type="text/javascript">
var tabber1 = new Yetii({
id: "tab-container-1"
});
</script>

<script type="text/javascript">
var tabber2 = new Yetii({
id: "tab-container-azioni",
tabclass: "yetii-azioni",
'.(isset($_POST['modifica_messaggio_azione']) ? 'active: 2,' : '').'
});
</script>

<script type="text/javascript">
var tabber3 = new Yetii({
id: "tab-container-ticket",
tabclass: "yetii-ticket",
'.(isset($_POST['modifica_messaggio_ticket']) ? 'active: 2,' : '').'
});
</script>

<script type="text/javascript">
var tabber4 = new Yetii({
id: "tab-container-preventivi",
tabclass: "yetii-preventivi",
'.(isset($_POST['modifica_messaggio_preventivo']) ? 'active: 2,' : '').'
});
</script>

<script type="text/javascript">
var tabber5 = new Yetii({
id: "tab-container-mex",
tabclass: "yetii-mex",
'.(isset($_POST['modifica_messaggio_messaggio']) ? 'active: 2,' : '').'
});
</script>

';
	}

	public function displayForm($isMainTab = true)
	{
		global $cookie, $currentIndex;
		parent::displayForm();

		if (!($obj = $this->loadObject(true)))
			return;

		$birthday = explode('-', $this->getFieldValue($obj, 'birthday'));
		$customer_groups = Tools::getValue('groupBox', $obj->getGroups());
		$groups = Group::getGroups($this->_defaultFormLanguage, true);
		
		if($cookie->profile == 7)
			$customer_note = Db::getInstance()->executeS('SELECT * FROM customer_note WHERE id_customer = '.Tools::getValue('id_customer').' AND id_employee = '.$cookie->id_employee);
		else
			$customer_note = Db::getInstance()->executeS('SELECT * FROM customer_note WHERE id_customer = '.Tools::getValue('id_customer').'');
		
		if(!isset($_GET['modificheabilitate'])) {
		echo "
		<script type='text/javascript'>
		$(document).ready(function(){
			$('#modificaanagrafica :input').attr('readonly', 'readonly');
			
			$('#modificaanagrafica :checkbox, #modificaanagrafica :radio').click(function(e) {
				e.preventDefault();
			});
			
			$('#modificaanagrafica select').attr('disabled', 'disabled'); 
		});
		// end
		</script>";
		
		echo '<fieldset><legend><img src="../img/admin/tab-customers.gif" />Anagrafica '.$this->l('Customer').'</legend>';
		echo '<form id="modificaanagrafica" action="index.php?tab=AdminCustomers&submitAddcustomerAndStay&id_customer='.$obj->id.'&viewcustomer&modificheabilitate&token='.$this->token.'&tab-container-1=1" method="post" autocomplete="off">';
		}
		else {
		echo '
		<form id="modificaanagrafica" action="index.php?tab=AdminCustomers&submitAddcustomerAndStay&viewcustomer&token='.$this->token.'&tab-container-1=1" method="post" autocomplete="off">';
		}
		
		
		echo ($obj->id ? '<input type="hidden" name="id_'.$this->table.'" value="'.$obj->id.'" />' : '').'
			';
	
			//	if ($this->getFieldValue($obj, 'is_company') == 1) {
				
				echo '<div class="anagrafica-cliente">
				'.$this->l('Azienda:').'<br />
			
					<input type="text" size="33" name="company" value="'.htmlentities($this->getFieldValue($obj, 'company'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>
				';

				
				echo '<div class="anagrafica-cliente">'.$this->l('First name:').' <sup>*</sup><br />
				
					<input type="text" size="33" name="firstname" value="'.htmlentities($this->getFieldValue($obj, 'firstname'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>
				
		<div class="anagrafica-cliente">'.$this->l('Last name:').' <sup>*</sup><br />
				
					<input type="text" size="33" name="lastname" value="'.htmlentities($this->getFieldValue($obj, 'lastname'), ENT_COMPAT, 'UTF-8').'" /> 
					<!-- <span class="hint" name="help_box">'.$this->l('Invalid characters:').' 0-9!<>,;?=+()@#"?{}_$%:<span class="hint-pointer">&nbsp;</span></span> --><br /><br /></div>
					
					<div class="anagrafica-cliente">'.$this->l('Codice eSolver:').' 
			
					<input type="text" size="33" name="codice_esolver" value="'.htmlentities($this->getFieldValue($obj, 'codice_esolver'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup></div>
					
				';
				
				echo "<div class='clear'></div>";
				
				
				echo '<div class="anagrafica-cliente">'.$this->l('Partita IVA:').' 
			
					<input type="text" size="33" name="vat_number" value="'.htmlentities($this->getFieldValue($obj, 'vat_number'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup></div>
					
		
								<div class="anagrafica-cliente">'.$this->l('Codice fiscale:').' 
			
					<input type="text" size="33" name="tax_code" value="'.htmlentities($this->getFieldValue($obj, 'tax_code'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup></div>
					
					
					
			';
			
			echo '
				<div class="anagrafica-cliente">'.$this->l('E-mail address:').' 
			
					<input type="text" size="33" name="email" value="'.htmlentities($this->getFieldValue($obj, 'email'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup>
<a href="mailto:'.htmlentities($this->getFieldValue($obj, 'email'), ENT_COMPAT, 'UTF-8').'"><div style="display:block; margin-top:-10px; float:right"><img src="../img/admin/outlook.gif" alt="Invia email con Outlook" title="Invia email con Outlook"  /></a><br /><a href="'.$currentIndex.'&id_customer='.$obj->id.'&viewcustomer&viewmessage&aprinuovomessaggio&token='.$this->token.'&tab-container-1=7"><img src="../img/admin/email.gif" alt="Apri nuovo messaggio" title="Apri nuovo messaggio" style="cursor:pointer"  /></a></div>
				</div>';
				
				$pec = Db::getInstance()->getValue('SELECT pec FROM customer WHERE id_customer = '.$obj->id);
					echo '
				<div class="anagrafica-cliente">'.$this->l('PEC:').'<br /> 
					<input type="text" size="33" name="pec" value="'.$pec.'" />
			

				</div>';
				
				echo "<div class='clear'></div><br />";
				
				
			$row = Db::getInstance()->getRow("SELECT id_address, phone, fax, phone_mobile FROM address JOIN customer ON customer.id_customer = address.id_customer WHERE address.deleted != 1 AND address.id_customer = $obj->id AND address.fatturazione = 1");
			$telefono = $row['phone'];
			$fax = $row['fax'];
			$cellulare = $row['phone_mobile'];
			$idindirizzo = $row['id_address'];
			
			if(empty($telefono))
				$telefono = Db::getInstance()->getValue('SELECT telefono_principale FROM customer WHERE id_customer = '.$obj->id);
			
			if(empty($cellulare))
				$cellulare = Db::getInstance()->getValue('SELECT cellulare_principale FROM customer WHERE id_customer = '.$obj->id);
			
			
			echo '
			<div class="anagrafica-cliente"><input type="hidden" name="idindirizzo" value="'.$idindirizzo.'" />
			'.$this->l('Telefono:').' 
			
			'.(Tools::getIsset('modificheabilitate') ? '<input type="text" onkeyup="this.value = this.value.replace(/\s/g,\'\');" size="33" name="telefono" value="'.htmlentities($telefono, ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup>' : '<span class="tab-span readonly" style="border:1px solid #E0D0B1; background-color: #f0ebd6; width:182px"><a onclick="window.onbeforeunload = null" href="callto:'.$telefono.'">'.$telefono.'</a></span>').'
			</div>
			
			<div class="anagrafica-cliente">
			'.$this->l('Fax:').' <br />
			
			'.(Tools::getIsset('modificheabilitate')  ? '<input type="text" size="33" name="fax" value="'.htmlentities($fax, ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup>' : '<span class="tab-span readonly" style="border:1px solid #E0D0B1; background-color: #f0ebd6; width:182px"><a onclick="window.onbeforeunload = null" href="callto:'.$fax.'">'.$fax.'</a></span>').'
			
			</div>
			
			<div class="anagrafica-cliente">
			'.$this->l('Cellulare:').' 
			
			'.(Tools::getIsset('modificheabilitate') ? '<input type="text" size="33" name="cellulare" value="'.htmlentities($cellulare, ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup>' : '<span class="tab-span readonly" style="border:1px solid #E0D0B1; background-color: #f0ebd6; width:182px"><a onclick="window.onbeforeunload = null" href="callto:'.$cellulare.'">'.$cellulare.'</a></span>').'
			</div>
			';				
				
				
					// if the customer is guest, he hasn't any password
				if ($obj->id && !$obj->is_guest || Tools::isSubmit('add').$this->table)
				{
					echo '
					<div class="anagrafica-cliente">'.$this->l('Nuova pw forzata manualmente:').' 
						<input type="text" size="33" name="passwd" value="" /> '.(!$obj->id ? '<sup>*</sup>' : '').'
						
					</div>';
				}
				
				
			
				echo "<div class='clear'></div><br />";
				
				
				
				$codice_univoco = Db::getInstance()->getValue('SELECT codice_univoco FROM customer WHERE id_customer = '.$obj->id);
				echo '
				
				<div class="anagrafica-cliente">'.$this->l('Codice destinatario SDI').' <br />
					<input type="text" name="codice_univoco" id="codice_univoco" value="'.$codice_univoco.'" />
					<p>'.$this->l('Per Aziende').'</p>
				</div>
				';
				
				
				$ipa = Db::getInstance()->getValue('SELECT ipa FROM customer WHERE id_customer = '.$obj->id);
				echo '
				
				<div class="anagrafica-cliente">'.$this->l('IPA:').' <br />
					<input type="text" name="ipa" id="ipa" value="'.$ipa.'" />
					<p>'.$this->l('Codice Fatt. Elettr. per la P.A.').'</p>
				</div>
				';
				
				
				echo	'<div class="anagrafica-cliente">'.$this->l('Regime Fiscale').' <br />
					<select name="tax_regime" style="width:194px">
					<option value="0" '.($this->getFieldValue($obj, 'tax_regime') == 0 ? 'selected="selected" ' : '').'>Base (Italia: IVA al 22%)</option>
					<option value="2" '.($this->getFieldValue($obj, 'tax_regime') == 2 ? 'selected="selected" ' : '').'>IVA al 10%</option>
					<option value="3" '.($this->getFieldValue($obj, 'tax_regime') == 3 ? 'selected="selected" ' : '').'>IVA al 4%</option>
					<option value="1" '.($this->getFieldValue($obj, 'tax_regime') == 1 ? 'selected="selected" ' : '').'>Esente</option>
					<option value="4" '.($this->getFieldValue($obj, 'tax_regime') == 4 ? 'selected="selected" ' : '').'>Split payment con IVA al 22%</option>
					</select>					
				</div>';
			
			
				echo "<div class='clear'></div><br />";
				
				$settore = Db::getInstance()->getValue('SELECT settore FROM customer WHERE id_customer = '.$obj->id);
				echo	'<div class="anagrafica-cliente">'.$this->l('Settore merceologico del cliente').' <br />
					<select name="settore" style="width:194px">
					<option value="" '.($settore == '' ? 'selected="selected" ' : '').'>-- Seleziona --</option>
					<option value="Call Center" '.($settore == 'Call Center' ? 'selected="selected" ' : '').'>Call Center</option>
					<option value="Commercio" '.($settore == 'Commercio' ? 'selected="selected" ' : '').'>Commercio</option>
					<option value="Hotel" '.($settore == 'Hotel' ? 'selected="selected" ' : '').'>Hotel</option>
					<option value="Industria" '.($settore == 'Industria' ? 'selected="selected" ' : '').'>Industria</option>
					<option value="Meccanica" '.($settore == 'Meccanica' ? 'selected="selected" ' : '').'>Meccanica</option>
					<option value="Ristorazione" '.($settore == 'Ristorazione' ? 'selected="selected" ' : '').'>Ristorazione</option>
					<option value="Servizi" '.($settore == 'Servizi' ? 'selected="selected" ' : '').'>Servizi</option>
					<option value="Telecomunicazioni" '.($settore == 'Telecomunicazioni' ? 'selected="selected" ' : '').'>Telecomunicazioni</option>
					<option value="Turismo" '.($settore == 'Turismo' ? 'selected="selected" ' : '').'>Turismo</option>
					<option value="Altro" '.($settore == 'Altro' ? 'selected="selected" ' : '').'>Altro</option>
					
					
					</select>					
				</div>';
			
				
				
				$categoria = Db::getInstance()->getValue('SELECT name FROM category_lang WHERE id_category = (SELECT category_id from order_detail od join orders o on o.id_order = od.id_order where id_customer = '.$obj->id.' group by category_id order by count(category_id) desc LIMIT 1)');
				echo '
				
				<div class="anagrafica-cliente">'.$this->l('Categoria più acquistata:').' <br />
					<input type="text" name="categoria" id="categoria" value="'.$categoria.'" readonly="readonly" />
				</div>
				';
				
				
				$canale = Db::getInstance()->getValue('SELECT canale FROM customer WHERE id_customer = '.$obj->id);
				
				if($canale == '')
				{
					$origine = Db::getInstance()->getValue('SELECT cs.http_referer FROM connections_source cs JOIN connections c ON cs.id_connections = c.id_connections JOIN guest g ON c.id_guest = g.id_guest WHERE g.id_customer = '.$obj->id.' ORDER BY c.id_connections ASC');
					
					if($origine != '')
						$canale = 'Link su altro sito';
					else
						$canale = '';
					
					if(strpos($origine, 'google.') !== false)
						$canale = 'Google';
					else if (strpos($origine, 'bing.') !== false || strpos($origine, 'yahoo') !== false || strpos($origine, 'duckduck') !== false)
						$canale = 'Altro motore di ricerca';
					else if(strpos($origine, 'trovaprezzi') !== false)
						$canale = 'Trovaprezzi';
					else if(strpos($origine, 'facebook') !== false)
						$canale = 'Facebook';
					else if(strpos($origine, 'linkedin') !== false)
						$canale = 'Linkedin';
					else if(strpos($origine, 'googleads') !== false || strpos($origine, 'adsense') !== false)
						$canale = 'Paid';
					
					if(strpos($obj->email, 'amazon') !== false)
						$canale = 'Amazon';
					else if(strpos($obj->email, 'eprice') !== false)
						$canale = 'ePrice';
				}
				
				echo '
				
				<div class="anagrafica-cliente">'.$this->l('Canale:').' <br />
					<select name="canale" style="width:194px">
					<option value="" '.($canale == '' ? 'selected="selected" ' : '').'>-- Seleziona --</option>
					<option value="Google" '.($canale == 'Google' ? 'selected="selected" ' : '').'>Google</option>
					<option value="Amazon" '.($canale == 'Amazon' ? 'selected="selected" ' : '').'>Amazon</option>
					
					<option value="DEM" '.($canale == 'DEM' ? 'selected="selected" ' : '').'>DEM</option>
					<option value="ePrice" '.($canale == 'ePrice' ? 'selected="selected" ' : '').'>ePrice</option>
					<option value="Facebook" '.($canale == 'Facebook' ? 'selected="selected" ' : '').'>Facebook</option>
					<option value="Inserzione su giornale" '.($canale == 'Inserzione su giornale' ? 'selected="selected" ' : '').'>Inserzione su giornale</option>
					<option value="Linkedin" '.($canale == 'Linkedin' ? 'selected="selected" ' : '').'>Linkedin</option>
					<option value="Link diretto" '.($canale == 'Link diretto' ? 'selected="selected" ' : '').'>Link diretto</option>
					<option value="Link su altro sito" '.($canale == 'Link su altro sito' ? 'selected="selected" ' : '').'>Link su altro sito</option>
					<option value="Altro motore di ricerca" '.($canale == 'Altro motore di ricerca' ? 'selected="selected" ' : '').'>Altro motore di ricerca (Bing, Yahoo, Duckduckgo ecc.)</option>
					<option value="Paid" '.($canale == 'Paid' ? 'selected="selected" ' : '').'>Paid (Adwords, Inserzioni Facebook ecc.)</option>
					<option value="Passaparola" '.($canale == 'Passaparola' ? 'selected="selected" ' : '').'>Passaparola</option>
					
					<option value="Trovaprezzi" '.($canale == 'Trovaprezzi' ? 'selected="selected" ' : '').'>Trovaprezzi</option>
					
					
					
					
					</select>	
				</div>
				';
				includeDatepicker3(array('ultimo_contatto'), true);
				
				$ultimo_contatto = Db::getInstance()->getValue('select * from (SELECT date_add from customer_thread where id_customer = '.$obj->id.' union select date_add from action_thread where id_customer = '.$obj->id.' union select date_add from form_prevendita_thread where id_customer = '.$obj->id.' union select date_add from cart where id_customer = '.$obj->id.' union select datetime from telefonate where id_customer = '.$obj->id.' union select date_add from orders where id_customer = '.$obj->id.') x order by date_add desc ');
				echo '
				
				<div class="anagrafica-cliente">'.$this->l('Ultimo contatto:').' <br />
					<input type="text" name="ultimo_contatto" id="ultimo_contatto" readonly="readonly" value="'.date('d-m-Y H:i:s', strtotime($ultimo_contatto)).'" />
				</div>
				';
				
			
				echo "<div class='clear'></div><br />";
				
				
				
				echo '<div class="anagrafica-cliente2" style="width:100px">Tipo cliente<br />'; 
				
				echo '<input type="radio" name="is_company" value="1" '; if ($this->getFieldValue($obj, 'is_company') == 1) { echo 'checked="checked"'; } else { echo ''; } echo' /><strong>AZIENDA</strong><br />
		<input type="radio" name="is_company" value="0"'; if ($this->getFieldValue($obj, 'is_company') == 0) { echo 'checked="checked"'; } else { echo ''; } echo' /></td><td style="width:60px"><strong>PRIVATO</strong>
			';
			
		
				echo '</div>';
					
			
				echo '<div class="anagrafica-cliente2" style="width:100px">Fornitore<br />'; 
				
				echo '<input type="radio" name="supplier" id="active_on" value="1" '.($this->getFieldValue($obj, 'supplier') ? 'checked="checked" ' : '').'/>
					<label class="t" for="supplier_on"><img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="supplier" id="active_off" value="0" '.(!$this->getFieldValue($obj, 'supplier') ? 'checked="checked" ' : '').'/>
					<label class="t" for="supplier_off"><img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label>';
		
				echo '</div>';
			
				
				
				
				echo '<div class="anagrafica-cliente">'.$this->l('Profilo:').' <br />
				
					<select name="id_default_group" onchange="checkDefaultGroup(this.value);" style="width:194px">';
				foreach ($groups as $group)
					echo '<option value="'.(int)($group['id_group']).'"'.($group['id_group'] == ($this->getFieldValue($obj, 'id_default_group')) ? ' selected="selected"' : '').'>'.htmlentities($group['name'], ENT_NOQUOTES, 'utf-8').'</option>';
				echo '
					</select>
					
				</div>
				
				<div class="anagrafica-cliente2">'.$this->l('Cliente Risk').'<br /> 
					<input type="radio" name="risk" id="risk_on" value="1" '.($this->getFieldValue($obj, 'risk') ? 'checked="checked" ' : '').'/>
					<label class="t" for="risk_on"><img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="risk" id="risk_off" value="0" '.(!$this->getFieldValue($obj, 'risk') ? 'checked="checked" ' : '').'/>
					<label class="t" for="risk_off"><img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label>

				</div>
				
				<div class="anagrafica-cliente" style="width:250px; "> <!-- 
				'.$this->l('Altri profili associati:').' 
				';
				
				/*
					if (sizeof($groups))
					{
						echo '
					<script type="text/javascript" src="mootools.js"></script>
					<script type="text/javascript" src="MultiSelect.js"></script>
					<script type="text/javascript">
					window.addEvent("domready", function() {
					var myMultiSelect = new MultiSelect(".MultiSelect");
					});
					</script>

					<div class="MultiSelect">';
						$irow = 0;
						foreach ($groups as $group)
						{
							echo '
						
								<input type="checkbox" class="delgruppo" name="groupBox[]" id="groupBox_'.$group['id_group'].'" value="'.$group['id_group'].'" '.(in_array($group['id_group'], $customer_groups) ? 'checked="checked" ' : '').' style="width:20px" /> <label for="'.$group['name'].'">'.$group['id_group'].' - '.$group['name'].'</label>
							';
						}
						echo '
					</div>
					
					';
					} else
						echo '<p>'.$this->l('No group created').'</p>';*/
				echo ' -->
				</div>';
				
				echo "<div class='clear'></div><br />";
			
			//	}
				
				echo '<div class="anagrafica-cliente">'.$this->l('Birthday:').'<br /> ';
				$sl_year = ($this->getFieldValue($obj, 'birthday')) ? $birthday[0] : 0;
				$years = Tools::dateYears();
				$sl_month = ($this->getFieldValue($obj, 'birthday')) ? $birthday[1] : 0;
				$months = Tools::dateMonths();
				$sl_day = ($this->getFieldValue($obj, 'birthday')) ? $birthday[2] : 0;
				$days = Tools::dateDays();
				$tab_months = array(
					$this->l('January'),
					$this->l('February'),
					$this->l('March'),
					$this->l('April'),
					$this->l('May'),
					$this->l('June'),
					$this->l('July'),
					$this->l('August'),
					$this->l('September'),
					$this->l('October'),
					$this->l('November'),
					$this->l('December'));
				echo '
				
					<select name="days">
						<option value="">-</option>';
						foreach ($days as $v)
							echo '<option value="'.$v.'" '.($sl_day == $v ? 'selected="selected"' : '').'>'.$v.'</option>';
					echo '
					</select>
					<select name="months">
						<option value="">-</option>';
						foreach ($months as $k => $v)
							echo '<option value="'.$k.'" '.($sl_month == $k ? 'selected="selected"' : '').'>'.$this->l($v).'</option>';
					echo '</select>
					<select name="years">
						<option value="">-</option>';
						foreach ($years as $v)
							echo '<option value="'.$v.'" '.($sl_year == $v ? 'selected="selected"' : '').'>'.$v.'</option>';
					echo '</select>
				</div>';
				
	
				if ($this->getFieldValue($obj, 'is_company') == 1) {
					echo	'<div class="anagrafica-cliente">'.$this->l('Numero impiegati:').' <br />
			
				
					<select name="employees_number" style="width:194px">
						<option value="Da 1 a 10"'; if ($this->getFieldValue($obj, 'employees_number') == "Da 1 a 10") { echo 'selected="selected"'; } echo '>Da 1 a 10</option>
						<option value="Da 11 a 50"'; if ($this->getFieldValue($obj, 'employees_number') == "Da 11 a 50") { echo 'selected="selected"'; } echo '>Da 11 a 50</option>
						<option value="50 piu"'; if ($this->getFieldValue($obj, 'employees_number') == "50 piu") { echo 'selected="selected"'; } echo '>Più di 50</option>
						</select>
		
					</div>
				
					';
				}
				
				else {
				
				}
				
					
					
				
				echo	'<div class="anagrafica-cliente2" style="">'.$this->l('Notifiche attività via mail').' <br />
			
				
					<input type="radio" name="order_notifications" id="active_off" value="0" '.(!$this->getFieldValue($obj, 'order_notifications') ? 'checked="checked" ' : '').'/>
					<label class="t" for="order_notifications_on"><img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="order_notifications" id="active_on" value="1" '.($this->getFieldValue($obj, 'order_notifications') ? 'checked="checked" ' : '').'/>
					<label class="t" for="order_notifications_off"><img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label>
					
				</div>
				
					';
				
					echo "<div class='clear'></div><br />";
				
				
				echo '<div class="anagrafica-cliente2">'.$this->l('Gender:').'<br />
					<input type="radio" size="33" name="id_gender" id="gender_1" value="1" '.($this->getFieldValue($obj, 'id_gender') == 1 ? 'checked="checked" ' : '').'/>
					<label class="t" for="gender_1"> '.$this->l('Male').'</label>
					<input type="radio" size="33" name="id_gender" id="gender_2" value="2" '.($this->getFieldValue($obj, 'id_gender') == 2 ? 'checked="checked" ' : '').'/>
					<label class="t" for="gender_2"> '.$this->l('Female').'</label>
					<input type="radio" size="33" name="id_gender" id="gender_3" value="9" '.(($this->getFieldValue($obj, 'id_gender') == 9 OR !$this->getFieldValue($obj, 'id_gender')) ? 'checked="checked" ' : '').'/>
					<label class="t" for="gender_3"> '.$this->l('Unknown').'</label>
				</div>';
				
				
				echo '<div class="anagrafica-cliente2">'.$this->l('Status:').' <br />
					<input type="radio" name="active" id="active_on" value="1" '.($this->getFieldValue($obj, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_on"><img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="active" id="active_off" value="0" '.(!$this->getFieldValue($obj, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_off"><img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label>
					<p>'.$this->l('Allow or disallow this customer to log in').'</p>
				</div>
				<div class="anagrafica-cliente2">'.$this->l('Newsletter:').' <br />
					<input type="radio" name="newsletter" id="newsletter_on" value="1" '.($this->getFieldValue($obj, 'newsletter') ? 'checked="checked" ' : '').'/>
					<label class="t" for="newsletter_on"><img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="newsletter" id="newsletter_off" value="0" '.(!$this->getFieldValue($obj, 'newsletter') ? 'checked="checked" ' : '').'/>
					<label class="t" for="newsletter_off"><img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label>
					<p>'.$this->l('Consenso ricezione newsletter').'</p>
				</div>';
				echo "<div class='clear'></div><br />";
				if($cookie->profile == 7)
				{
					
				}
				else
				{
					if(!isset($_GET['modificheabilitate'])) {
						echo '<button type="submit" class="button" name="abilitamodifiche">
						<img src="../img/admin/edit.gif" alt="Modifica" title="Modifica" />&nbsp;&nbsp;&nbsp;'.$this->l('Abilita modifiche').'
						</button> '.($cookie->profile != 8 ? '<a class="button" href="/ezadmin/index.php?tab=AdminCustomers&amp;id_customer='.$customer->id.'&amp;deletecustomer&amp;token=d45d14295479a74d3ac97885512f1f4d" onclick="return confirm(\'Vuoi davvero eliminare questo cliente?\');"><span style="display:table-cell"><img src="../img/admin/delete.gif" alt="Elimina" title="Elimina" style="margin-top:-5px"></span> Elimina cliente</a>' : '');
					}
					else {
						/*echo '<button type="submit" class="button" name="submitAdd'.$this->table.'">
						<img src="../img/admin/save.gif" alt="Salva" title="Salva" />&nbsp;&nbsp;&nbsp;'.$this->l('   Save   ').'
						</button>';
						*/
					}
				}
				if(!isset($_GET['modificheabilitate'])) {
					echo '
					</form>
					<br /><br />
					<form action="index.php?tab=AdminCustomers&submitAddcustomerNoteAndStay&viewcustomer&token='.$this->token.'&tab-container-1=1" method="post" autocomplete="off">
					<input name="id_customer" type="hidden" value="'.Tools::getValue('id_customer').'" />
				';
				}
				else
				{
					echo '<input name="submitAddcustomerNote2" type="hidden" value="y" />';
				}
				
				
				/*
				echo '<div id="mostra_nota_privata" >
					<div class="anagrafica-cliente2" style="width:390px"><strong>'.$this->l('Nota privata:').'</strong>
				'.$this->l('This note will be displayed to all the employees but not to the customer.').'
				<textarea name="note" id="note" style="width:100%;height:100px">'.htmlentities($this->getFieldValue($obj, 'note'), ENT_COMPAT, 'UTF-8').'</textarea><br />
				</div>';
				*/
				
				
				echo '<strong>Note private</strong><br />';
				
				
				
				echo '
				
					
					<table><thead>'.(count($customer_note) > 0 ? '<tr><th>Nota</th><th>Creata da</th><th>Ultima modifica</th><th></th></tr>' : '' ).'</thead><tbody id="tableNoteBody">
				';
				
				
				foreach($customer_note as $customer_nota)
				{
					echo '<tr id="tr_note_'.$customer_nota['id_note'].'">';
					echo '
					<td>
					<textarea class="textarea_note" name="note_nota['.$customer_nota['id_note'].']" id="note_nota['.$customer_nota['id_note'].']" style="width:550px; height:16px" onkeyup="auto_grow(this)">'.htmlentities($customer_nota['note'], ENT_COMPAT, 'UTF-8').'</textarea><input type="hidden" name="note_nuova['.$customer_nota['id_note'].']" id="note_nuova['.$customer_nota['id_note'].']" value="0" /></td>
					<td><input type="text" readonly="readonly" name="note_id_employee['.$customer_nota['id_note'].']" value="'.Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM employee WHERE id_employee = '.$customer_nota['id_employee']).'" /></td>
					<td><input type="text" readonly="readonly" name="note_date_add['.$customer_nota['id_note'].']" value="'.Tools::displayDate($customer_nota['date_upd'], 5).'" /></td>';
					echo '<td><a href="javascript:void(0)" onclick="javascript:var surec=window.confirm(\'Sei sicuro?\'); if (surec) { togli_nota('.$customer_nota['id_note'].'); cancella_nota('.$customer_nota['id_note'].'); } else { return false; }; "><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" style="cursor:pointer" /></a></td>';
					echo'
					</tr>
					';
				}
				
				echo '</tbody></table><br />';
				
				echo '
				<script type="text/javascript">add_nota_privata();</script>
				<a href="javascript:void(0)" onclick="javascript:add_nota_privata()";><img src="../img/admin/add.gif" alt="Aggiungi" title="Aggiungi" /> Clicca per aggiungere una nota privata</a> <br /><br />';
				
				
				echo '
				<strong>'.$this->l('Keyword').'</strong>
				<textarea name="keyword" id="keyword" style="width:820px;height:16px">'.htmlentities($this->getFieldValue($obj, 'keyword'), ENT_COMPAT, 'UTF-8').'</textarea><br />
				
				<script type="text/javascript">
				$(document).ready(function() {
					
						$(".textarea_note").each(function () {
							this.style.height = ((this.scrollHeight)-4)+"px";
						});
						
						$(".textarea_note").on("change keyup keydown paste cut", "textarea", function () {
							$(this).height(0).height(this.scrollHeight);
						}).find("textarea").change();
						
						autosize($(".textarea_note"));
											
					});
					
				</script>
				
				<br />
				';
				
				if(!isset($_GET['modificheabilitate'])) {
				
				echo '<button type="submit" class="button" name="submitAdd'.$this->table.'Note">
					<img src="../img/admin/save.gif" alt="Salva" title="Salva" />&nbsp;&nbsp;&nbsp;'.$this->l('Salva Note e Keyword').'
					</button>';
				}
				else
				{
					echo '<button type="submit" class="button" name="submitAdd'.$this->table.'">
					<img src="../img/admin/save.gif" alt="Salva" title="Salva" />&nbsp;&nbsp;&nbsp;'.$this->l('   Save   ').'
					</button>';
					
				}
				
				echo '
				<div class="small"><sup>*</sup> '.$this->l('Required field').'</div><br /><br />';
				
				$anagrafica_creata_da = Db::getInstance()->getValue('SELECT created_by FROM customer WHERE id_customer = '.$obj->id);
				
				switch($anagrafica_creata_da)
				{
					case '999': $creata_da = 'N.D.'; break;
					case '0': $creata_da = 'Cliente'; break;
					default: $creata_da = Db::getInstance()->getValue('SELECT concat(firstname," ",lastname) FROM employee WHERE id_employee = '.$anagrafica_creata_da); break;
				}
				
				echo '
				
				<div class="small">Anagrafica creata il '.date('d/m/Y H:i:s', strtotime($obj->date_add)).' da: '.$creata_da.'</div>
		</form>';
		
		
	}

	public function getList($id_lang, $orderBy = NULL, $orderWay = NULL, $start = 0, $limit = NULL)
	{
		global $cookie;
		return parent::getList((int)($cookie->id_lang), !Tools::getValue($this->table.'Orderby') ? 'date_add' : NULL, !Tools::getValue($this->table.'Orderway') ? 'DESC' : NULL);
	}
	
	public function display()
	{
		global $cookie;
		
			if (isset($_GET['view'.$this->table]))
			$this->viewcustomer();
		else
		{
			$this->getList((int)($cookie->id_lang), !Tools::getValue($this->table.'Orderby') ? 'date_add' : NULL, !Tools::getValue($this->table.'Orderway') ? 'DESC' : NULL);
			$currency = new Currency((int)(Configuration::get('PS_CURRENCY_DEFAULT')));
			$this->displayListHeader();
			$this->displayListContent();
			$this->displayListPagination($this->token);
			$this->displayListFooter();
			
		}
	}
	
	public function assegna_incarico($in_carico, $idct, $tipo) {
		global $cookie;
		$employeeincaricato = new Employee($cookie->id_employee);
		
		if($tipo != 'preventivo' && $tipo != 'tirichiamiamonoi') { 
			
			$ct = new CustomerThread($idct);
			
			$customerz = new Customer($ct->id_customer);
			
			$tickettipo = $ct->id_contact;
			
			if ($tickettipo == 4){
				$sigla = "T";
			}
			else if ($tickettipo == 2){
				$sigla = "A";
			} 
			else if ($tickettipo == 3){
				$sigla = "V";
			} 
			else if ($tickettipo == 6){
				$sigla = "R";
			}
			else if ($tickettipo == 7){
				$sigla = "M";
			}
			else if ($tickettipo == 8){
				$sigla = "D";
			} 
			else if ($tickettipo == 9){
				$sigla = "S";
			} 
			$anno = $ct->date_add;
							
			$anno = substr($anno,2,2);
							
			$id_thread_ticket = $ct->id;
							
			$id_ticket = $sigla.$anno.$id_thread_ticket;
			
			if($in_carico != 0) {
								
				if ($in_carico == $ct->id_employee) {
										
				}
									
				else if ($in_carico == $cookie->id_employee) {
										
				}
										
				else {
									
					$mail_incarico = Db::getInstance()->getValue('SELECT email FROM employee WHERE id_employee = '.$in_carico.'');
					$tokenimp = Tools::getAdminToken("AdminCustomers"."2".$in_carico);
					$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$ct->id_customer."&viewcustomer&viewticket&id_customer_thread=".$ct->id."&token=".$tokenimp.'&tab-container-1=6';	
					
					if($tickettipo == 7) {
						$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$ct->id_customer."&viewcustomer&viewmessage&id_mex=".$ct->id."&token=".$tokenimp.'&tab-container-1=7';	
						$params3 = array(
						'{reply}' => $employeeincaricato->firstname." ".$employeeincaricato->lastname." ti ha assegnato un messaggio su Ezdirect. L'id del messaggio &egrave;: <strong>".$id_ticket."</strong> (cliente: ".($customerz->is_company == 1 ? $customerz->company : $customerz->firstname." ".$customerz->lastname)." - ID ".$customerz->id.")<br /><br />
						<a href='".$linkimp."'>Per rispondere al messaggio, clicca qua</a>.										
						");
						Mail::Send(5, 'action', Mail::l('Messaggio cliente assegnato a te su Ezdirect', 5), 
						$params3, $mail_incarico, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', '', NULL, 
						_PS_MAIL_DIR_, true);
					}
					else {				
						$params3 = array(
						'{reply}' => $employeeincaricato->firstname." ".$employeeincaricato->lastname." ti ha assegnato un ticket su Ezdirect. L'id del ticket &egrave;: <strong>".$id_ticket."</strong> (cliente: ".($customerz->is_company == 1 ? $customerz->company : $customerz->firstname." ".$customerz->lastname)." - ID ".$customerz->id.")<br /><br />
						<a href='".$linkimp."'>Per rispondere al ticket, clicca qua</a>.										
						");
						Mail::Send(5, 'action', Mail::l('Ticket cliente assegnato a te su Ezdirect', 5), 
						$params3, $mail_incarico, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', '', NULL, 
						_PS_MAIL_DIR_, true);
					}								
					
											
				}
			}
			else {
			
			}
		}
		else {
			$rowz = Db::getInstance()->getRow("SELECT * FROM form_prevendita_thread WHERE id_thread = ".$idct."");
			

			$customerz = new Customer($rowz['id_customer']);

			$tipo_richiesta_p = Db::getInstance()->getValue("SELECT tipo_richiesta FROM form_prevendita_thread WHERE id_thread = ".$idct."");
			if($tipo_richiesta_p == 'tirichiamiamonoi') {
				$sigla = "R";
			}
			else {
				$sigla = "P";
			}
			$anno = $rowz['date_add'];
			$anno = substr($anno,2,2);
			$id_thread_ticket = $idct;
			$id_ticket = $sigla.$anno.$id_thread_ticket;	
			if($in_carico != 0) {
								
				if ($in_carico == $cookie->id_employee) {
										
				}
								
				else {
										
					$mail_incarico = Db::getInstance()->getValue('SELECT email FROM employee WHERE id_employee = '.$in_carico.'');
					$tokenimp = Tools::getAdminToken("AdminCustomers"."2".$in_carico);
					$linkimp = "https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$rowz['id_customer']."&viewcustomer&viewticket&id_customer_thread=".$idct."&token=".$tokenimp.'&tab-container-1=7';	
				
					$params3 = array(
					'{reply}' => $employeeincaricato->firstname." ".$employeeincaricato->lastname." ti ha assegnato un ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." su Ezdirect. L'id del ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." &egrave;: <strong>".$id_ticket."</strong> (cliente: ".($customerz->is_company == 1 ? $customerz->company : $customerz->firstname." ".$customerz->lastname)." - ID ".$customerz->id.")<br /><br />
					
					<a href='".$linkimp."'>Per rispondere al ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo').", clicca qua</a>.										
					");
					Mail::Send(5, 'action', Mail::l(''.($tipo_richiesta_p == 'tirichiamiamonoi' ? 'Ti richiamiamo noi' : 'Preventivo').' cliente assegnato a te su Ezdirect', 5), 
					$params3, $mail_incarico, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', '', NULL, 
					_PS_MAIL_DIR_, true);
							
				}
			}
			else {
			
			}			
		}
	
	}

	public function beforeDelete($object)
	{
		return $object->isUsed();
	}
	
		private function openUploadedFile()
	{
		$filename = $_GET['filename'];
		
		if(strpos($filename, ":::")) {
		
			$parti = explode(":::", $filename);
			$nomecodificato = $parti[0];
			$nomevero = $parti[1];
		
		}
		
		else {
		
			$nomecodificato = $filename;
			$nomevero = $filename;
		
		}
		
		$extensions = array('.txt' => 'text/plain', '.rtf' => 'application/rtf', '.doc' => 'application/msword', '.docx'=> 'application/msword',
		'.pdf' => 'application/pdf', '.zip' => 'multipart/x-zip', '.png' => 'image/png', '.jpeg' => 'image/jpeg', '.gif' => 'image/gif', '.jpg' => 'image/jpeg');

		$extension = '';
		foreach ($extensions AS $key => $val)
			if (substr($nomevero, -4) == $key OR substr($nomevero, -5) == $key)
			{
				$extension = $val;
				break;
			}
		echo $nomecodificato."<br />";
		echo $nomevero;
		ob_end_clean();
		header('Content-Type: '.$extension);
		header('Content-Disposition:attachment;filename="'.$nomevero.'"');
		readfile(_PS_UPLOAD_DIR_.$nomecodificato);
		die;
	}
	
			
			
			
	public function displayListContent($token = NULL)
	{
		/* Display results in a table
		 *
		 * align  : determine value alignment
		 * prefix : displayed before value
		 * suffix : displayed after value
		 * image  : object image
		 * icon   : icon determined by values
		 * active : allow to toggle status
		 */

		global $currentIndex, $cookie;
		$currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));

		$id_category = 1; // default categ

		$irow = 0;
		if ($this->_list AND isset($this->fieldsDisplay['position']))
		{
			$positions = array_map(create_function('$elem', 'return (int)($elem[\'position\']);'), $this->_list);
			sort($positions);
		}
		if ($this->_list)
		{
			$isCms = false;
			if (preg_match('/cms/Ui', $this->identifier))
				$isCms = true;
			$keyToGet = 'id_'.($isCms ? 'cms_' : '').'category'.(in_array($this->identifier, array('id_category', 'id_cms_category')) ? '_parent' : '');
			foreach ($this->_list AS $tr)
			{
				$id = $tr[$this->identifier];
				echo '<tr'.(array_key_exists($this->identifier,$this->identifiersDnd) ? ' id="tr_'.(($id_category = (int)(Tools::getValue('id_'.($isCms ? 'cms_' : '').'category', '1'))) ? $id_category : '').'_'.$id.'_'.$tr['position'].'"' : '').($irow++ % 2 ? ' class="alt_row"' : '').' '.((isset($tr['color']) AND $this->colorOnBackground) ? 'style="background-color: '.$tr['color'].'"' : '').'>
							<td '.(isset($params['widthColumn']) ? 'style="width: '.$params['widthColumn'].'px; max-width:  '.$params['widthColumn'].'px"' :  'style="width: '.$params['width'].'px; max-width:  '.$params['width'].'px"').' class="center">';
				if ($this->delete AND (!isset($this->_listSkipDelete) OR !in_array($id, $this->_listSkipDelete)))
					echo '<input type="checkbox" name="'.$this->table.'Box[]" value="'.$id.'" class="noborder" />';
				echo '</td>';
				foreach ($this->fieldsDisplay AS $key => $params)
				{
					$active_ctrl = Db::getInstance()->getValue("SELECT active FROM customer WHERE id_customer = $id");
					$tmp = explode('!', $key);
					$key = isset($tmp[1]) ? $tmp[1] : $tmp[0];
					echo '
					<td '.(isset($params['widthColumn']) ? 'style="width: '.$params['widthColumn'].'px; max-width:  '.$params['widthColumn'].'px"' :  'style="width: '.$params['width'].'px; max-width:  '.$params['width'].'px"').' '.(isset($params['position']) ? ' id="td_'.(isset($id_category) AND $id_category ? $id_category : 0).'_'.$id.'"' : '').' class="'.((!isset($this->noLink) OR !$this->noLink) ? 'pointer' : '').((isset($params['position']) AND $this->_orderBy == 'position')? ' dragHandle' : ''). (isset($params['align']) ? ' '.$params['align'] : '').'" ';
					if (!isset($params['position']) AND (!isset($this->noLink) OR !$this->noLink))
						echo ' onclick="document.location = \''.$currentIndex.'&'.$this->identifier.'='.$id.($this->view? '&view' : '&update').$this->table.'&token='.($token!=NULL ? $token : $this->token).'\'">'.($active_ctrl == 0 ? "<font style='text-decoration:line-through'>" : '').''.(isset($params['prefix']) ? $params['prefix'] : '');
					else
						echo '>';
					if (isset($params['supplier']) AND isset($tr[$key]))
						echo ($tr[$key] ? 'S&igrave;' : '--');
					elseif (isset($params['active']) AND isset($tr[$key]))
						$this->_displayEnableLink($token, $id, $tr[$key], $params['active'], Tools::getValue('id_category'), Tools::getValue('id_product'));
					elseif (isset($params['activeVisu']) AND isset($tr[$key]))
						echo '<img src="../img/admin/'.($tr[$key] ? 'enabled.gif' : 'disabled.gif').'"
						alt="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" title="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" />';
					elseif (isset($params['position']))
					{
						if ($this->_orderBy == 'position' AND $this->_orderWay != 'DESC')
						{
							echo '<a'.(!($tr[$key] != $positions[sizeof($positions) - 1]) ? ' style="display: none;"' : '').' href="'.$currentIndex.
									'&'.$keyToGet.'='.(int)($id_category).'&'.$this->identifiersDnd[$this->identifier].'='.$id.'
									&way=1&position='.(int)($tr['position'] + 1).'&token='.($token!=NULL ? $token : $this->token).'">
									<img src="../img/admin/'.($this->_orderWay == 'ASC' ? 'down' : 'up').'.gif"
									alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>';

							echo '<a'.(!($tr[$key] != $positions[0]) ? ' style="display: none;"' : '').' href="'.$currentIndex.
									'&'.$keyToGet.'='.(int)($id_category).'&'.$this->identifiersDnd[$this->identifier].'='.$id.'
									&way=0&position='.(int)($tr['position'] - 1).'&token='.($token!=NULL ? $token : $this->token).'">
									<img src="../img/admin/'.($this->_orderWay == 'ASC' ? 'up' : 'down').'.gif"
									alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a>';						}
						else
							echo (int)($tr[$key] + 1);
					}
					elseif (isset($params['image']))
					{
						// item_id is the product id in a product image context, else it is the image id.
						$item_id = isset($params['image_id']) ? $tr[$params['image_id']] : $id;
						// If it's a product image
						if (isset($tr['id_image']))
						{
							$image = new Image((int)$tr['id_image']);
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$image->getExistingImgPath().'.'.$this->imageType;
						}else
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$item_id.(isset($tr['id_image']) ? '-'.(int)($tr['id_image']) : '').'.'.$this->imageType;

						echo cacheImage($path_to_image, $this->table.'_mini_'.$item_id.'.'.$this->imageType, 45, $this->imageType);
					}
					elseif (isset($params['icon']) AND (isset($params['icon'][$tr[$key]]) OR isset($params['icon']['default'])))
						echo '<img src="../img/admin/'.(isset($params['icon'][$tr[$key]]) ? $params['icon'][$tr[$key]] : $params['icon']['default'].'" alt="'.$tr[$key]).'" title="'.$tr[$key].'" />';
					elseif (isset($params['price']))
						echo Tools::displayPrice($tr[$key], (isset($params['currency']) ? Currency::getCurrencyInstance((int)($tr['id_currency'])) : $currency), false);
					elseif (isset($params['float']))
						echo rtrim(rtrim($tr[$key], '0'), '.');
					elseif (isset($params['type']) AND $params['type'] == 'date')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang);
					elseif (isset($params['type']) AND $params['type'] == 'datetime')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang, true);
					elseif (isset($tr[$key]))
					{
						$echo = ($key == 'price' ? round($tr[$key], 2) : isset($params['maxlength']) ? Tools::substr($tr[$key], 0, $params['maxlength']).'...' : $tr[$key]);
						echo isset($params['callback']) ? call_user_func_array(array($this->className, $params['callback']), array($echo, $tr)) : $echo;
					}
					else
						echo '--';

					echo (isset($params['suffix']) ? $params['suffix'] : '').($active_ctrl == 0 ? "<font style='text-decoration:line-through'>" : '').
					'</td>';
				}

				if ($this->edit OR $this->delete OR ($this->view AND $this->view !== 'noActionColumn'))
				{
					echo '<td class="center" style="white-space: nowrap;">';
					if ($this->view)
						$this->_displayViewLink($token, $id);
					if ($this->edit)
						$this->_displayEditLink($token, $id);
					if ($this->delete AND (!isset($this->_listSkipDelete) OR !in_array($id, $this->_listSkipDelete)))
						$this->_displayDeleteLink($token, $id);
					if ($this->duplicate)
						$this->_displayDuplicate($token, $id);
					echo '</td>';
				}
				echo '</tr>';
			}
		}
	}
	
	public function FileSizeConvert($bytes)
			{
				$bytes = floatval($bytes);
					$arBytes = array(
						0 => array(
							"UNIT" => "TB",
							"VALUE" => pow(1024, 4)
						),
						1 => array(
							"UNIT" => "GB",
							"VALUE" => pow(1024, 3)
						),
						2 => array(
							"UNIT" => "MB",
							"VALUE" => pow(1024, 2)
						),
						3 => array(
							"UNIT" => "KB",
							"VALUE" => 1024
						),
						4 => array(
							"UNIT" => "B",
							"VALUE" => 1
						),
					);

				foreach($arBytes as $arItem)
				{
					if($bytes >= $arItem["VALUE"])
					{
						$result = $bytes / $arItem["VALUE"];
						$result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
						break;
					}
				}
				return $result;
			}
	

	public static function deleteDir($dirPath) {
		if (! is_dir($dirPath)) {
			throw new InvalidArgumentException("$dirPath must be a directory");
		}
		if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
			$dirPath .= '/';
		}
		$files = glob($dirPath . '*', GLOB_MARK);
		foreach ($files as $file) {
			if (is_dir($file)) {
				self::deleteDir($file);
			} else {
				unlink($file);
			}
		}
		rmdir($dirPath);
	}
	
	public static function ListIn($dir, $prefix = '') {
		$dir = rtrim($dir, '\\/');
		$result = array();

		$h = opendir($dir);
		while (($f = readdir($h)) !== false) {
		  if ($f !== '.' and $f !== '..') {
			if (is_dir("$dir/$f")) {
			  $result[] = $prefix.$f;
			  $result = array_merge($result, self::ListIn("$dir/$f", "$prefix$f/"));
			} else {
			//  $result[] = $prefix.$f;
			}
		  }
		}
		closedir($h);

	  return $result;
	}
	
	public static function sanitize($string, $force_lowercase = true, $anal = false) {
		$strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
					   "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
					   "â€”", "â€“", ",", "<", ".", ">", "/", "?");
		$clean = trim(str_replace($strip, "", strip_tags($string)));
		$clean = preg_replace('/\s+/', "-", $clean);
		$clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
		return ($force_lowercase) ?
			(function_exists('mb_strtolower')) ?
				mb_strtolower($clean, 'UTF-8') :
				strtolower($clean) :
			$clean;
	}

	public static function generatePDFBDL($id_bdl, $tipo_az = 'ezdirect', $vergine = 'n') {
	
		if($vergine == 'n')
		{
			$bdl = Db::getInstance()->getRow('SELECT * FROM bdl WHERE id_bdl = '.$id_bdl.'');
			$customer = Db::getInstance()->getRow('SELECT * FROM customer WHERE id_customer = '.$bdl['id_customer'].'');
			if($customer['is_company'] == 1) {
			
				$cliente = $customer['company'];
			}
			else {
				$cliente = $customer['firstname']." ".$customer['lastname'];
			}
			$indirizzo = Db::getInstance()->getRow('SELECT * FROM address a JOIN state s ON a.id_state = s.id_state WHERE a.id_address='.$bdl['id_address'].''); 
			
			$impianto_ubicazione = $bdl['impianto_ubicazione'];
			if(is_numeric($impianto_ubicazione)) 
			{
				$indirizzo_impianto = Db::getInstance()->getRow('SELECT * FROM address a JOIN state s ON a.id_state = s.id_state WHERE a.id_address='.$impianto_ubicazione.''); 
				$impianto_ubicazione = $indirizzo_impianto['address1']." - ".$indirizzo_impianto['postcode']." ".$indirizzo_impianto['city']." (".$indirizzo_impianto['iso_code'].")";
			}
			
			$richiesto_da = Db::getInstance()->getRow('SELECT * FROM persone WHERE id_persona = '.$bdl['richiesto_da'].'');
			$effettuato_da = Db::getInstance()->getRow('SELECT * FROM employee WHERE id_employee = '.$bdl['effettuato_da'].'');
		}
		else
		{
			$bdl = ''; $customer = ''; $cliente = ''; $indirizzo = ''; $impianto_ubicazione = ''; $indirizzo_impianto = ''; $richiesto_da = ''; $effettuato_da = '';
		}
		
		
		$content = '<page><div id="contenitore" style="width:770px; height:1070px; font-family: Arial, Verdana, Sans-Serif; display:block; position:relative; margin:0 auto;">
	<div style="position:absolute; top:0px; left:0px">
		'.($tipo_az == 'tecnotel' ? '<img src="http://www.ezdirect.it/img/Tecnotel-centralini.gif" alt="Tecnotel" />' : '<img src="http://www.ezdirect.it/img/logo.jpg" alt="Ezdirect" />').'
	</div>
	
	<div style="position:absolute; top:10px; text-align:right; right:25px; height:45x; font-size:12px;">
		Via Nerino Garbuio snc - 54038 Montignoso (MS) - Tel. 0585 821163 - Fax 0585 821286<br />
		'.($tipo_az == 'tecnotel' ? 'P.IVA 00601870454 - Email info@tecnotel.net - Sito web www.tecnotel.net' : 'P.IVA - CF 01164670455 - REA 118272 - Email info@ezdirect.it - Sito web www.ezdirect.it').'
	</div>
	
	<div style="position:absolute; top:65px; text-align:right; right:25px; height:30px; font-size:15px">
	
	</div>
	
	<div style="clear:both">&nbsp;</div>
	
	<div style="position:relative; margin-top:65px; padding:5px; height:710px">
	
		<style type="text/css">
			.dettagli_tabella td {
				font-size:13px;
				height:20px;
				vertical-align:middle;
				border:1px solid black;
				padding:0px;
				padding-top:1px;
				padding-bottom:1px;
			}	
		</style>
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">
			<tr>
				
				<td style="width:355px; border:0px; border-right:1px solid black"></td>
				<td style="width:65px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Data</td>
				<td style="width:100px; border-bottom:0px">&nbsp;'.($vergine == 'y' ? '' : date("d-m-Y", strtotime($bdl['data_effettuato']))).'</td>
				<td style="width:108px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Buono di Lavoro N°</td> 
				<td style="width:100px; border-bottom:0px">&nbsp;'.$bdl['id_bdl'].'</td>
			</tr>
		</table>
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">	
			<tr>
				<td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Utente</td>
				<td style="width:303px; border-bottom:0px">&nbsp;'.$cliente.'</td>
				<td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Codice Spring</td>
				<td style="width:126px; border-bottom:0px">'.($vergine == 'y' ? '' : (Db::getInstance()->getValue('select codice_spring from customer where id_customer = '.Tools::getValue('id_customer').'') == '' ? '<em>Non presente</em>' : Db::getInstance()->getValue('select codice_spring from customer where id_customer = '.Tools::getValue('id_customer').''))).'</td>
			</tr>
		</table>
				
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
			<tr>
				
				<td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Indirizzo</td>
				<td style="width:585px; border-bottom:0px">&nbsp;'.$indirizzo['address1'].'</td>
						
			</tr>
		</table>
		
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
			<tr>
				<td style="width:150px; background-color:#f3f3f3; font-size:11px">&nbsp;CAP - Citt&agrave;</td>
				<td style="width:303px">&nbsp;'.($vergine == 'y' ? '' : $indirizzo['postcode'].' - '.$indirizzo['city']).'</td>
				<td style="width:35px; background-color:#f3f3f3; font-size:11px">&nbsp;Prov.</td>
				<td style="width:50px">&nbsp;'.$indirizzo['iso_code'].'</td>
				<td style="width:35px; background-color:#f3f3f3; font-size:11px">&nbsp;Tel.</td>
				<td style="width:150px;">&nbsp;'.(substr($bdl['phone'],0,5) == 'Altro' ? (substr($bdl['phone'],5,strlen($bdl['phone'])) ) : $bdl['phone']).'</td>
			</tr>
		</table>
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
			<tr>
				<td style="width:150px; background-color:#f3f3f3; border-top:0px; font-size:11px">&nbsp;Richiesto il giorno</td>
				<td style="width:140px; border-top:0px">&nbsp;'.($vergine == 'y' ? '' : date("d-m-Y", strtotime($bdl['data_richiesta']))).'</td>
				<td style="width:50px; background-color:#f3f3f3; border-top:0px; font-size:11px">&nbsp;alle ore</td>
				<td style="width:107px; border-top:0px">&nbsp;'.($vergine == 'y' ? '' : date("H:i", strtotime($bdl['data_richiesta']))).'</td>
				<td style="width:35px; background-color:#f3f3f3; border-top:0px; font-size:11px">&nbsp;da</td>
				<td style="width:241px; border-top:0px">&nbsp;'.$richiesto_da['firstname']." ".$richiesto_da['lastname'].'</td>
			</tr>	
			<tr>
				<td style="width:150px; background-color:#f3f3f3; border-top:0px; font-size:11px">&nbsp;Effettuato il giorno</td>
				<td style="width:140px; border-top:0px">&nbsp;'.($vergine == 'y' ? '' : date("d-m-Y", strtotime($bdl['data_effettuato']))).'</td>
				<td style="width:50px; background-color:#f3f3f3; border-top:0px; font-size:11px">&nbsp;alle ore</td>
				<td style="width:107px; border-top:0px">&nbsp;'.($vergine == 'y' ? '' : date("H:i", strtotime($bdl['data_effettuato']))).'</td>
				<td style="width:35px; background-color:#f3f3f3; border-top:0px; font-size:11px">&nbsp;da</td>
				<td style="width:241px; border-top:0px">&nbsp;'.$effettuato_da['firstname']." ".$effettuato_da['lastname'].'</td>
			</tr>
		</table>
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
			<tr>
				
				<td style="width:150px; background-color:#f3f3f3; border-top:0px; border-bottom:0px; font-size:11px">&nbsp;Impianto-Ubicazione</td>
				<td style="width:585px; border-top:0px; border-bottom:0px">&nbsp;'.$impianto_ubicazione.'</td>
						
			</tr>
		</table>
		
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">			
			<tr>
				<td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Contratto di assistenza</td>
				<td style="width:20px; border-bottom:0px; position:relative;">'.($bdl['contratto_assistenza'] == 1 ? '<div style="position:absolute; font-size:18px; left:42px">X</div>' : '').'SI</td>
				<td style="width:20px; border-bottom:0px; position:relative;">'.($vergine == 'y' ? '' : ($bdl['contratto_assistenza'] == 0 ? '<div style="position:absolute; font-size:18px; left:20px">X</div>' : '')).'NO</td>
				<td style="width:245px; background-color:#f3f3f3; border-bottom:0px; text-align:right; font-size:11px">Manutenzione ordinaria&nbsp;</td>
				<td style="width:20px; text-align:center; border-bottom:0px">'.($bdl['manutenzione_ordinaria'] == 1 ? 'X' : '').'</td>
				<td style="width:245px; background-color:#f3f3f3; border-bottom:0px; text-align:right; font-size:11px">&nbsp;Manutenzione straordinaria&nbsp;</td>
				<td style="width:20px; text-align:center; border-bottom:0px">'.($bdl['manutenzione_straordinaria'] == 1 ? 'X' : '').'</td>
			</tr>
		</table>
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
			<tr>
				
				<td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Motivo chiamata</td>
				<td style="width:575px; padding-left:5px; border-bottom:0px">'.$bdl['motivo_chiamata'].'</td>
						
			</tr>
		</table>
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
			<tr>
				
				<td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Descrizione</td>
				<td style="width:575px; padding-left:5px; border-bottom:0px">'.(substr($bdl['descrizione'],0,5) == 'Altro' || ($bdl['id_bdl'] < 392 && $bdl['descrizione'] != 'Nessuno') ? (substr($bdl['descrizione'],0,5) == 'Altro' ? substr($bdl['descrizione'],5,strlen($bdl['descrizione'])) : $bdl['descrizione']) : $bdl['descrizione']).'</td>
						
			</tr>
		</table>
		
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
			<tr>
				
				<td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Guasto riscontrato dal tecnico</td>
				<td style="width:575px; padding-left:5px; border-bottom:0px">'.(substr($bdl['guasto'],0,5) == 'Altro' || ($bdl['id_bdl'] < 392 && $bdl['guasto'] != 'Nessuno') ? (substr($bdl['guasto'],0,5) == 'Altro' ? substr($bdl['guasto'],5,strlen($bdl['guasto'])) : $bdl['guasto']) : $bdl['guasto']).'</td>
						
			</tr>
		</table>
		
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
			<tr>
				
				<td style="width:150px; background-color:#f3f3f3; border-bottom:0px; font-size:11px">&nbsp;Causa guasto</td>
				<td style="width:575px; padding-left:5px; border-bottom:0px">'.(substr($bdl['causa_guasto'],0,5) == 'Altro' || ($bdl['id_bdl'] < 392 && $bdl['causa_guasto'] != 'Nessuno') ? (substr($bdl['causa_guasto'],0,5) == 'Altro' ? substr($bdl['causa_guasto'],5,strlen($bdl['causa_guasto'])) : $bdl['causa_guasto']) : $bdl['causa_guasto']).'</td>
						
			</tr>
		</table>
		
		
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
			<tr>
				
				<td style="width:738px; background-color:#f3f3f3; border-bottom:1px solid black; text-align:center; font-size:11px">&nbsp;Intervento svolto</td>
						
			</tr>
		</table>
		
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
			<tr>
				
				<td style="width:728px; padding-left:5px; border-top:0px; border-bottom:1px solid black;">'.$bdl['intervento_svolto'].'</td>
						
			</tr>
			'.(strlen($bdl['intervento_svolto']) > 50 ? '' : '
			<tr>
				
				<td style="width:728px; padding-left:5px; border-top:0px; border-bottom:1px solid black;"></td>
						
			</tr>
			<tr>
				
				<td style="width:728px; padding-left:5px; border-top:0px; border-bottom:1px solid black;"></td>
						
			</tr>
			<tr>
				
				<td style="width:728px; padding-left:5px; border-top:0px; border-bottom:1px solid black;"></td>
						
			</tr>').'
		</table>
		';
		
		$dettaglio_costi = Db::getInstance()->getValue("SELECT dettaglio_costi FROM bdl WHERE id_bdl = ".$id_bdl."");
					
					$dettaglio_costi = unserialize($dettaglio_costi);
			
					if($bdl['rif_ordine'] <= 0)
					{
					
					$content .= '
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">		
		
			<tr style="text-align:center">
				<td colspan="2" style="width:494px; background-color:#f3f3f3; border-top:0px; font-size:11px">Dettaglio Economico dell\'intervento</td>
				<td style="width:50px; border-top:0px; background-color:#f3f3f3; font-size:11px ">&nbsp;Q.t&agrave;</td>
				<td style="width:60px; border-top:0px; background-color:#f3f3f3; font-size:11px">&nbsp;Unitario</td>
				<td style="width:60px; border-top:0px; background-color:#f3f3f3; font-size:11px">&nbsp;Sconto %</td>
				<td style="width:60px; border-top:0px; background-color:#f3f3f3; font-size:11px">&nbsp;Importo</td>
				
			</tr>';
			
		//	if($dettaglio_costi['diritto_chiamata_qta']*$dettaglio_costi['diritto_chiamata_unitario'] > 0)
		//	{
				switch($dettaglio_costi['diritto_chiamata_tipo'])
				{
					case 'EZDIRFIX': $tipo = 'Fisso intervento &lt; 20 km'; break;
					case 'EZDIRFIXF': $tipo = 'Fisso intervento &lt; 20 km festivo'; break;
					default: $tipo = ''; break;
				}
				$content .= '<tr>
					<td style="width:96px; background-color:#f3f3f3; font-size:11px">&nbsp;Diritto fisso di chiamata</td>
					<td style="width:397px">&nbsp;'.$tipo.'</td>
					<td style="width:50px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($dettaglio_costi['diritto_chiamata_qta'],2,",","")).'&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($dettaglio_costi['diritto_chiamata_unitario'],2,",","")).'&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($dettaglio_costi['diritto_chiamata_sconto'],2,",","")).'&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format(($dettaglio_costi['diritto_chiamata_unitario']-(($dettaglio_costi['diritto_chiamata_unitario']*$dettaglio_costi['diritto_chiamata_sconto'])/100))*($dettaglio_costi['diritto_chiamata_qta']),2,",","")).'&nbsp;</td>
				</tr>';
		//	}
		//	if($dettaglio_costi['chilometri_percorsi_qta']*$dettaglio_costi['chilometri_percorsi_unitario'] > 0)
		//	{
				switch($dettaglio_costi['chilometri_percorsi_tipo'])
				{
					case 'EZKMR': $tipo = 'Rimborso km oltre diritto fisso'; break;
					case 'EZZONAFIX': $tipo = 'Chiamata fissa con partner di zona'; break;
					default: $tipo = ''; break;
				}
				$content .= '
				<tr>
					<td style="width:96px; background-color:#f3f3f3; font-size:11px">&nbsp;Chilometri percorsi</td>
					<td style="width:397px">&nbsp;'.$tipo.'</td>
					<td style="width:50px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($dettaglio_costi['chilometri_percorsi_qta'],2,",","")).'&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($dettaglio_costi['chilometri_percorsi_unitario'],2,",","")).'&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($dettaglio_costi['chilometri_percorsi_sconto'],2,",","")).'&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format(($dettaglio_costi['chilometri_percorsi_unitario']-(($dettaglio_costi['chilometri_percorsi_unitario']*$dettaglio_costi['chilometri_percorsi_sconto'])/100))*($dettaglio_costi['chilometri_percorsi_qta']),2,",","")).'&nbsp;</td>
				</tr>';
		//	}
			
		//	if($dettaglio_costi['costo_manodopera_qta']*$dettaglio_costi['costo_manodopera_unitario'] > 0)
		//	{
				switch($dettaglio_costi['costo_manodopera_tipo'])
				{
					case 'EZTECORD': $tipo = 'Tariffa oraria ordinaria'; break;
					case 'EZTEC30': $tipo = 'Tariffa 30 minuti ordinaria'; break;
					case 'EZTECXTRA': $tipo = 'Tariffa oraria straordinaria'; break;
					case 'EZTECFEST': $tipo = 'Tariffa oraria festivo'; break;
					default: $tipo = ''; break;
				}
				
				$content .= '
				<tr>
					<td style="width:96px; background-color:#f3f3f3; font-size:11px">&nbsp;Costo manodopera</td>
					<td style="width:397px">&nbsp;'.$tipo.'</td>
					<td style="width:50px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($dettaglio_costi['costo_manodopera_qta'],2,",","")).'&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($dettaglio_costi['costo_manodopera_unitario'],2,",","")).'&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($dettaglio_costi['costo_manodopera_sconto'],2,",","")).'&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format(($dettaglio_costi['costo_manodopera_unitario']-(($dettaglio_costi['costo_manodopera_unitario']*$dettaglio_costi['costo_manodopera_sconto'])/100))*($dettaglio_costi['costo_manodopera_qta']),2,",","")).'&nbsp;</td>
				</tr>';
		//	}
			
			$tot_varie = 0;
			
			$tot_varie = (($dettaglio_costi['diritto_chiamata_unitario']-(($dettaglio_costi['diritto_chiamata_unitario']*$dettaglio_costi['diritto_chiamata_sconto'])/100))*($dettaglio_costi['diritto_chiamata_qta']))+(($dettaglio_costi['chilometri_percorsi_unitario']-(($dettaglio_costi['chilometri_percorsi_unitario']*$dettaglio_costi['chilometri_percorsi_sconto'])/100))*($dettaglio_costi['chilometri_percorsi_qta']))+(($dettaglio_costi['costo_manodopera_unitario']-(($dettaglio_costi['costo_manodopera_unitario']*$dettaglio_costi['costo_manodopera_sconto'])/100))*($dettaglio_costi['costo_manodopera_qta']));
			
			$num_varie = $dettaglio_costi['num_varie'];
				
				for($i=0; $i<=$num_varie; $i++) {
			
			if(empty($dettaglio_costi['ricambi_e_varie_'.$i.'_descrizione']) && $dettaglio_costi['ricambi_e_varie_'.$i.'_unitario'] == 0) 
			{
			}
			else
			{
				$content .= '<tr>
					<td style="width:96px; background-color:#f3f3f3; font-size:11px">&nbsp;Ricambi e Varie</td>
					<td style="width:397px">&nbsp;'.$dettaglio_costi['ricambi_e_varie_'.$i.'_descrizione'].'</td>
					<td style="width:50px; text-align:right">&nbsp;'.number_format($dettaglio_costi['ricambi_e_varie_'.$i.'_qta'],2,",","").'&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;'.number_format($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario'],2,",","").'&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;'.number_format($dettaglio_costi['ricambi_e_varie_'.$i.'_sconto'],2,",","").'&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;'.number_format(($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario']-(($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario']*$dettaglio_costi['ricambi_e_varie_'.$i.'_sconto'])/100))*($dettaglio_costi['ricambi_e_varie_'.$i.'_qta']),2,",","").'&nbsp;</td>
				</tr>';
			}
			
				
			$tot_varie += ($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario']-(($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario']*$dettaglio_costi['ricambi_e_varie_'.$i.'_sconto'])/100))*($dettaglio_costi['ricambi_e_varie_'.$i.'_qta']);
			
			}
			
			$content .= '<tr>
					<td style="width:96px; background-color:#f3f3f3; font-size:11px">&nbsp;Ricambi e Varie</td>
					<td style="width:397px">&nbsp;</td>
					<td style="width:50px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
				</tr>';$content .= '<tr>
					<td style="width:96px; background-color:#f3f3f3; font-size:11px">&nbsp;Ricambi e Varie</td>
					<td style="width:397px">&nbsp;</td>
					<td style="width:50px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
				</tr>';$content .= '<tr>
					<td style="width:96px; background-color:#f3f3f3; font-size:11px">&nbsp;Ricambi e Varie</td>
					<td style="width:397px">&nbsp;</td>
					<td style="width:50px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
				</tr>';
			
			if($vergine == 'y')
			{
				$content .= '<tr>
					<td style="width:96px; background-color:#f3f3f3; font-size:11px">&nbsp;Ricambi e Varie</td>
					<td style="width:397px">&nbsp;</td>
					<td style="width:50px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
					<td style="width:60px; text-align:right">&nbsp;</td>
				</tr>';
			}
			
			
			/*$content .= '
			<tr>
				<td colspan="2" style="width:494px;">&nbsp;</td>
				<td style="width:50px;">&nbsp;</td>
				<td style="width:60px">&nbsp;</td>
				<td style="width:60px">&nbsp;</td>
				<td style="width:60px">&nbsp;</td>		
			</tr>';*/
			
			$content .='
		</table>';
		
		
		$content .= '
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">				
			<tr>
				<td style="width:496px; background-color:#f3f3f3; text-align:right; border-top:0px; font-size:11px">IMPORTO INTERVENTO IVA esclusa S.E. &amp; O.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; EURO&nbsp;</td>
				<td style="width:239px; border-top:0px; text-align:right">&nbsp;'.($vergine == 'y' ? '' : number_format($tot_varie,2,",","")).'&nbsp;</td>
			</tr>
		</table>';
		
		}
		
		$content .= '
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">			
			<tr>
				<td style="width:150px; border-top:0px; background-color:#f3f3f3; font-size:11px">&nbsp;Note</td>
				<td style="width:585px; border-top:0px">&nbsp;'.$bdl['note'].'</td>
			</tr>
			<tr>
				<td style="width:150px; border-top:0px; background-color:#f3f3f3; font-size:11px">&nbsp;Rif. ordine</td>
				<td style="width:585px; border-top:0px">&nbsp;'.($bdl['rif_ordine'] == 0 ? '--' : $bdl['rif_ordine']).'</td>
			</tr>
		</table>
		<table style="width:735px; border-collapse:collapse" class="dettagli_tabella" cellspacing="0" cellpadding="0">			
			<tr>
				<td style="width:20px; text-align:center; border-top:0px">'.($bdl['pagato'] == 1 ? 'X' : '').'</td>
				<td style="width:160px; border-top:0px; font-size:11px">&nbsp;Pagato&nbsp;</td>
				<td style="width:20px; text-align:center; border-top:0px">'.($vergine == 'y' ? '' : ($bdl['pagato'] == 0 ? 'X' : '')).'</td>
				<td style="width:160px; border-top:0px; font-size:11px">&nbsp;Da pagare&nbsp;</td>
				<td style="width:20px; text-align:center; border-top:0px">'.($bdl['fatturato'] == 1 ? 'X' : '').'</td>
				<td style="width:160px; border-top:0px; font-size:11px">&nbsp;Fatturato&nbsp;</td>
				<td style="width:20px; text-align:center; border-top:0px">'.($vergine == 'y' ? '' : ($bdl['fatturato'] == 0 ? 'X' : '')).'</td>
				<td style="width:157px; border-top:0px; font-size:11px">&nbsp;Da fatturare&nbsp;</td>
			</tr>
		</table>
		
	</div>
	<p style="font-size:11px; padding:5px">
	Il documento sar&agrave; reso perfetto in sede '.($tipo_az == 'tecnotel' ? 'TecnoTel' : 'Ezdirect').' qualora debbano essere consuntivati alcuni costi come riparazione in laboratorio interno o esterno etc. Altres&igrave; in caso di installazione o fornitura di merce non preventivata e/o non confermata attraverso conferma d\'ordine. Una copia completamente compilata sar&agrave; allegata alla fattura commerciale. Il cliente accetta e sottoscrive la buona riuscita dell\'intervento tecnico e conferma, dopo verifica effettuata con il tecnico '.($tipo_az == 'tecnotel' ? 'TecnoTel' : 'Ezdirect').', la risoluzione dei problemi, per i quali l\'intervento stesso &egrave; stato richiesto.
	
	</p>
	
	<div style="padding:5px; float:left">
		<table style="display:block;margin:0 auto; width:700px; text-align:center">
		<tr>&nbsp;<td style="width:350px; font-size:13px">&nbsp;<strong>'.($tipo_az == 'tecnotel' ? 'TecnoTel' : 'Ezdirect').'</strong>&nbsp;</td>&nbsp;<td style="width:350px; font-size:13px">&nbsp;<strong>L\'Utente (timbro e firma)</strong>&nbsp;</td>&nbsp;</tr>&nbsp;</table>
		<br /><br /><br /><br />
		
		<table style="display:block;margin:0 auto; width:735px; font-size:11px; border-collapse:collapse" class="dettagli_tabella" >
			<tr>
				<td style="width:115px; padding-left:5px; background-color:#f3f3f3; font-size:11px">&nbsp;Fatturare a</td>
				<td style="width:230px; padding-left:5px;">&nbsp;</td>
				<td style="width:115px; padding-left:5px; background-color:#f3f3f3; font-size:11px">&nbsp;Approvato da</td>
				<td style="width:230px; padding-left:5px;">&nbsp;</td>
			</tr>
			
		</table>
	
	</div>

</div></page>';

	return $content;
		
	
	
	}


}
