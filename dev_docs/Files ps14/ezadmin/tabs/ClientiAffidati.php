<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once(PS_ADMIN_DIR.'/tabs/AdminPreferences.php');

class ClientiAffidati extends AdminTab
{

	public function display()
	{
		global $cookie;
		global $currentIndex;
		
		echo '<h1>Clienti affidati</h1>';
		
		$clienti = Db::getInstance()->executeS('SELECT * FROM customer c JOIN customer_amministrazione ca ON c.id_customer = ca.id_customer WHERE ca.fido != "" GROUP BY c.id_customer');
		
		$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
		
			echo '<script type="text/javascript" src="../js/jquery/jquery.tablesorter.min.js"></script> 
			<script type="text/javascript" src="../js/jquery/jquery.tablesorter.widgets.js"></script> 
			<script type="text/javascript" src="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.js"></script> 
			<link rel="stylesheet" type="text/css" href="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.css" />
		
			<script type="text/javascript">
			var myTextExtraction = function(node) 
				{ 
					// extract data from markup and return it 
					return node.textContent.replace( /<.*?>/g, "" ); 
				}
				
			$(document).ready(function() 
				{ 
				
			
				$.tablesorter.addParser({ 
					// set a unique id 
					id: "thousands",
					is: function(s) { 
						// return false so this parser is not auto detected 
						return false; 
					}, 
					format: function(s) {
						// format your data for normalization 
						return s.replace(/[\s.$£€]/g,"").replace(/,/g,".");
					}, 
					// set type, either numeric or text 
					type: "numeric" 
				}); 


					$("#clienti_affidati").tablesorter({ textExtraction: { 0: function(node, table, cellIndex){ return $(node).find("strong").text(); } }, headers: {
					6: {sorter: "shortDate", dateFormat: "ddmmyy"}, 3: {sorter:"thousands" }, 4: {sorter:"thousands" }, 5: {sorter:"thousands" }
        }, cssChildRow: "invisible-table-row", widthFixed: true, widgets: ["zebra"]});
					
				} 
			);
			</script>';
		
		echo "<table class='table' id='clienti_affidati' style='width:100%'><thead><tr><th>Id CRM</th><th>eSolver</th><th>Cliente</th><th>Fido</th><th>Speso</th><th>Residuo</th><th>Data registrazione fido</th></tr></thead>";
		foreach($clienti as $row)
		{
			$fido = (float)(str_replace(",",".",$row['fido']));
			
			$somma_fido = Db::getInstance()->getValue('SELECT SUM(total_paid_real) FROM orders WHERE id_customer = '.$row['id_customer'].' AND (payment = "Bonifico 30 gg. fine mese" OR payment = "Bonifico 60 gg. fine mese" OR payment = "Bonifico 90 gg. fine mese" OR payment = "Bonifico 30 gg. 15 mese successivo"  OR payment = "R.B. 30 GG. D.F. F.M." OR payment = "R.B. 60 GG. D.F. F.M." OR payment = "R.B. 90 GG. D.F. F.M."  OR payment = "R.B. 30 GG. 5 mese successivo" OR payment = "R.B. 30 GG. 10 mese successivo" OR payment = "R.B. 60 GG. 5 mese successivo" OR payment = "R.B. 60 GG. 10 mese successivo"  OR payment LIKE "%saldo%")  AND id_order NOT IN (SELECT id_order FROM order_history WHERE id_order_state = 18 OR id_order_state = 32) AND date_add > "'.$row['data_fido'].'"');
			
			$somma_fido_32 = Db::getInstance()->getValue('SELECT SUM(total_paid_real) FROM orders o JOIN (SELECT id_order_state, id_order FROM order_history WHERE id_order_state = 32) oh ON o.id_order = oh.id_order WHERE id_customer = '.$row['id_customer'].' AND (payment = "Bonifico 30 gg. fine mese" OR payment = "Bonifico 60 gg. fine mese" OR payment = "Bonifico 90 gg. fine mese" OR payment = "Bonifico 30 gg. 15 mese successivo" OR payment = "R.B. 30 GG. D.F. F.M." OR payment = "R.B. 60 GG. D.F. F.M." OR payment = "R.B. 90 GG. D.F. F.M."  OR payment = "R.B. 30 GG. 5 mese successivo" OR payment = "R.B. 30 GG. 10 mese successivo" OR payment = "R.B. 60 GG. 5 mese successivo" OR payment = "R.B. 60 GG. 10 mese successivo" OR payment LIKE "%saldo%")  AND id_order NOT IN (SELECT id_order FROM order_history WHERE id_order_state = 18 OR id_order_state = 32) AND date_add > "'.$row['data_fido'].'"');
			
			
			
			$crediti_fido = $fido - $somma_fido + $somma_fido_32;
			
			echo '
			<tr>
			<td>'.$row['id_customer'].'</td>
			<td>'.$row['codice_esolver'].'</td>
			<td><a href="index.php?tab=AdminCustomers&id_customer='.$row['id_customer'].'&viewcustomer&token='.$tokenCustomers.'">'.($row['is_company'] == 1 ? $row['company'] : $row['firstname'].' '.$row['lastname']).'</a></td>
			<td style="text-align:right">'.number_format($fido,2,",",".").'</td>
			<td style="text-align:right">'.number_format($somma_fido + $somma_fido_32,2,",",".").'</td>
			<td style="text-align:right">'.number_format($crediti_fido,2,",",".").'</td>
			<td style="text-align:right">'.date('d/m/Y',strtotime($row['data_fido'])).'</td>
			</tr>
			';
			
		}
		echo '</table>';
	}

}


