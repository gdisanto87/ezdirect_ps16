<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14632 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AmazonProducts extends AdminTab
{
	public function display()
	{	
		global $cookie, $currentIndex;
		echo "<h1>Prodotti esposti su Amazon</h1>";
echo 'In questa tabella si possono vedere i prodotti attivi su Amazon. Sono presenti tutti i prodotti che hanno il flag sul nostro portale per l\'uscita su Amazon (almeno uno). <br />
<strong>LEGENDA</strong><br />
Flag verde = il prodotto &egrave; attivo sul marketplace relativo alla colonna (IT, FR ecc.) in cui si trova. <br />
Flag grigio = il prodotto ha il flag per essere pubblicato su Amazon ma non &egrave; attivo nel marketplace relativo alla colonna.<br />
Numero vicino al flag = quantit&agrave; pubblicata sul marketplace.<br />
La colonna "Tipo QT" indica che tipo di disponibilit&agrave; pubblichiamo su Amazon. Se "EZ", su Amazon pubblichiamo solo disponibilit&agrave; del magazzino interno. Se "EZ+F", pubblichiamo disponibilit&agrave; magazzino interno e magazzino fornitore principale.<br />
Un prodotto pu&ograve; avere i flag ma non essere attivo sul marketplace per vari motivi (non disponibile in magazzino, descrizione troppo lunga per Amazon, EAN assente, rifiuto di Amazon per altri motivi).<br /><br />
<strong>Scorrendo il mouse sul logo prime, si pu&ograve; vedere il prezzo che il prodotto ha su Prime</strong>.<br /><br />
<span style="color:red">ATTENZIONE: i prodotti si aggiornano due volte al giorno (alle 07:05 e alle 23:05). Se si vede solo una colonna con i flag verdi (o due, o comunque non tutte) significa che la procedura è in corso di aggiornamento, quindi attendere qualche minuto e poi aggiornare con F5. La procedura pu&ograve; richiedere anche 20-30 minuti per completarsi causa tempi di Amazon. <!--  &Egrave; possibile farla partire a mano <a href="ezdirect.it/docs/disponibilita/MarketplaceWebService/Samples/AmazonFeed.php" target="_blank">cliccando qui</a> (previo inserimento username e password). --></span>

<br /><br />';
		
		
		if(Tools::getIsset('products'))
		{
			$prodotti = "";
			$categorie_di_default = "";
			$macrocategorie = "";
			$macrocategorie_di_default = "";
			$categorie = "";
			$marchi = "";

			echo "<strong>Hai cercato...</strong><br />";
				
			if(isset($_POST['per_prodotto'])) {
				echo "<strong>Questi prodotti precisi:</strong><br /> ";
				
				foreach ($_POST['per_prodotto'] as $prodotto) {

					$id_product = $prodotto;
					$nomeprodotto = Db::getInstance()->getValue("SELECT name FROM product_lang WHERE id_lang = 5 AND id_product = ".$id_product."");
					
					$prodottiinclusi.= $nomeprodotto."; ";
					$strisciaprodotti.= "- ".$nomeprodotto."<br />";

					$prodotti.= "p.id_product = '$prodotto' OR ";
				}
			}
			else if(isset($_POST['prodottiinclusi']) && !empty($_POST['prodottiinclusi'])) {
				echo "<strong>Questi prodotti precisi:</strong><br /> ";
				$strisciaprodotti = $_POST['strisciaprodotti'];
				$prodottiinclusi = $_POST['prodottiinclusi'];
			}
				
			echo $strisciaprodotti;
			echo "<br />";
			
			if(isset($_POST['per_categoria'])) {
			
				echo "<strong>Prodotti in queste sottocategorie:</strong><br /> ";
		
		
				foreach ($_POST['per_categoria'] as $categoria) {

					$nomecat = Db::getInstance()->getValue("SELECT name FROM category_lang WHERE id_lang = 5 AND id_category = ".$categoria."");
					$strisciacat .= "- ".$nomecat."<br />";
					
				
					$categorieincluse.= $nomecat."; ";
					
					$categorie_di_default.= "p.id_category_default = $categoria OR ";
					$categorie.= "cp.id_category = $categoria OR ";
					
				}	
			}
			else if(isset($_POST['categorieincluse']) && !empty($_POST['categorieincluse'])) {
			
				echo "<strong>Prodotti in queste sottocategorie:</strong><br /> ";
				$strisciacat = $_POST['strisciacat'];
				$categorieincluse = $_POST['categorieincluse'];
			
			
			}
			
			echo $strisciacat;
			
			if(isset($_POST['per_macrocategoria'])) {
			
				echo "<strong>Prodotti in queste macrocategorie:</strong><br /> ";
			
			
				foreach ($_POST['per_macrocategoria'] as $macrocategoria) {

					$nomemacrocat = Db::getInstance()->getValue("SELECT name FROM category_lang WHERE id_lang = 5 AND id_category = ".$macrocategoria."");
					$strisciamacrocat .= "- ".$nomemacrocat."<br />";
					
					$children = Db::getInstance()->executeS("SELECT id_category FROM
					(SELECT t4.id_category
					FROM category AS t1
					LEFT JOIN category AS t2 ON t1.id_category = t2.id_parent
					LEFT JOIN category AS t3 ON t2.id_category = t3.id_parent
					LEFT JOIN category AS t4 ON t3.id_category = t3.id_parent
					WHERE t2.id_parent =$macrocategoria

					UNION 

					SELECT t3.id_category
					FROM category AS t1
					LEFT JOIN category AS t2 ON t1.id_category = t2.id_parent
					LEFT JOIN category AS t3 ON t2.id_category = t3.id_parent
					WHERE t2.id_parent =$macrocategoria

					UNION 

					SELECT t2.id_category
					FROM category AS t1
					LEFT JOIN category AS t2 ON t1.id_category = t2.id_parent
					WHERE t2.id_parent =$macrocategoria

					UNION 

					SELECT id_category FROM category WHERE id_category = $macrocategoria) ccc WHERE id_category IS NOT NULL ORDER BY id_category ASC

					");

					foreach ($children as $child) {

						$macrocategorie_di_default.= "p.id_category_default = ".$child['id_category']." OR ";
						$macrocategorie.= "cp.id_category = ".$child['id_category']." OR ";
					}
					
					
					$macrocategorieincluse.= $nomemacrocat."; ";
					
					
			
				}	
				
			}
			else if(isset($_POST['macrocategorieincluse']) && !empty($_POST['macrocategorieincluse'])) {
			
				echo "<strong>Prodotti in queste macrocategorie:</strong><br /> ";
				$strisciamacrocat = $_POST['strisciamacrocat'];
				$macrocategorieincluse = $_POST['macrocategorieincluse'];
			
			
			}
			
			echo $strisciamacrocat;
				
			if(isset($_POST['per_marchio'])) {
			
				echo "<strong>Prodotti in questi marchi:</strong><br /> ";
		
		
				foreach ($_POST['per_marchio'] as $marchio) {

					$nomemarchio = Db::getInstance()->getValue("SELECT name FROM manufacturer WHERE id_manufacturer = ".$marchio."");
					$strisciamarchi .= "- ".$nomemarchio."<br />";
					
				
					$marchiinclusi.= $nomemarchio."; ";
					$marchi.= "p.id_manufacturer = $marchio OR ";
				}	
				
			}
			else if(isset($_POST['marchiinclusi']) && !empty($_POST['marchiinclusi'])) {
			
				echo "<strong>Prodotti in questi marchi:</strong><br /> ";
				$strisciamarchi = $_POST['strisciamarchi'];
				$marchiinclusi = $_POST['marchiinclusi'];
			
			
			}
				
			echo $strisciamarchi;

			// inutilizzato
			$anno_corrente = "2017";
			
			$where = "
				
			";
			
			if(isset($_POST['per_prodotto'])) {
				$where.= "AND 
				(
				$prodotti
				p.id_product = '9999999999999999999999'
				)";
			}

			if(isset($_POST['per_categoria'])) {
			
				$where .= "AND 
				(
				$categorie
				cp.id_category = 9999999999999999999999
				)";
			
			}

			if(isset($_POST['per_marchio'])) {
			
				$where .= "
				AND 
				(
				$marchi
				p.id_manufacturer = 9999999999999999999999
				)";
			}

			if(isset($_POST['per_macrocategoria'])) {
			
				$where .= "
				AND
				(
				$macrocategorie
				cp.id_category = 9999999999999999999999
				)";
			
			}
						
			$sql = 'SELECT p.id_product, reference, supplier_reference, ean13, m.name as manufacturer, asin, pl.name, amazon, id_supplier, quantity quantita, stock_quantity, supplier_quantity allnet_quantity, amazon_quantity, scorta_minima_amazon, itancia_quantity, esprinet_quantity, attiva_quantity, techdata_quantity, intracom_quantity, it, fr, es, uk, de, p.price as prezzo, p.sku_amazon, p.no_amazon, ap.price as amazon_price, ap.last_price, ap.qt_last, ap.price_prime, p.id_manufacturer, supplier_quantity, p.amazon_it FROM product p JOIN manufacturer m ON p.id_manufacturer = m.id_manufacturer LEFT JOIN amazon_products ap ON p.id_product = ap.id_product JOIN category_product cp ON p.id_product = cp.id_product  JOIN product_lang pl ON p.id_product = pl.id_product WHERE (p.amazon != "" AND p.amazon != "0") 
			
			'.$where.' 
			AND pl.id_lang = 5 GROUP BY p.id_product ORDER BY '.(isset($_GET['orderby']) ? $_GET['orderby'] : 'p.reference').' '.(isset($_GET['orderway']) ? $_GET['orderway'] : 'ASC').' ';
			
			echo '<a href="https://www.ezdirect.it/ezadmin/index.php?tab=AmazonProducts&token='.$this->token.'">Clicca qui per eseguire una nuova ricerca</a><br /><br />';
			
			$prodotti = Db::getInstance()->executeS($sql);
			
			set_time_limit(3600);
			require_once 'esportazione-catalogo/Classes/PHPExcel.php';
			$objPHPExcel = new PHPExcel();
			
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'Codice')
				->setCellValue('B1', 'Codice SKU')
				->setCellValue('C1', 'EAN')
				->setCellValue('D1', 'ASIN')
				->setCellValue('E1', 'Prodotto')
				->setCellValue('F1', 'Costruttore')
				->setCellValue('G1', 'Mag. EZ')
				->setCellValue('H1', 'Mag. Forn.')
				->setCellValue('I1', 'Mag. Amz')
				->setCellValue('J1', 'Scorta Minima Amz')
				->setCellValue('K1', 'Tipo qt.')
				->setCellValue('L1', 'Prezzo EZ')
				->setCellValue('M1', 'Prezzo AMZ ')
				->setCellValue('N1', 'IT')
				->setCellValue('O1', 'FR')
				->setCellValue('P1', 'UK')
				->setCellValue('Q1', 'DE')
				->setCellValue('R1', 'ES')
				;
				$k = 2;
			
				echo '<script type="text/javascript" src="../js/jquery/jquery.tablesorter.min.js"></script> 
				<script type="text/javascript" src="../js/jquery/jquery.tablesorter.widgets.js"></script> 
				<script type="text/javascript" src="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.js"></script> 
				<link rel="stylesheet" type="text/css" href="tabs/tablesorter/addons/pager/jquery.tablesorter.pager.css" />';
				
				echo '
				<script type="text/javascript">
				var myTextExtraction = function(node) 
					{ 
						// extract data from markup and return it 
						return node.textContent.replace( /<.*?>/g, "" ); 
					}
					
				$(document).ready(function() 
					{ 
					
				
					$.tablesorter.addParser({ 
						// set a unique id 
						id: "thousands",
						is: function(s) { 
							// return false so this parser is not auto detected 
							return false; 
						}, 
						format: function(s) {
							// format your data for normalization 
							return s.replace(/[\s.$£€]/g,"").replace(/,/g,".");
						}, 
						// set type, either numeric or text 
						type: "numeric" 
					}); 


						$("#tableVerificaProdotti").tablesorter({  headers: {
						
			}, widthFixed: true, widgets: ["zebra"]});
						
					} 
				);
				</script>';
				
			echo "
			<style type='text/css'>
			#tableVerificaProdotti {
			}
			
			#tableVerificaProdotti tbody {
				
			}

			#tableVerificaProdotti td {
				color:#000;
			}
			
			
			</style>
			
			<script type='text/javascript'>
				$(document).ready(function() {
					function moveScroll(){
						var scroll = $(window).scrollTop();
						var anchor_top = $('#tableVerificaProdotti').offset().top;
						var anchor_bottom = $('#bottom_anchor').offset().top;
						if (scroll>anchor_top && scroll<anchor_bottom) {
						clone_table = $('#clone');
						if(clone_table.length == 0){
							clone_table = $('#tableVerificaProdotti').clone();
							clone_table.attr('id', 'clone');
							clone_table.css({position:'fixed',
									 'pointer-events': 'none',
									 top:0});
							clone_table.width($('#tableVerificaProdotti').width());
							$('#table-container').append(clone_table);
							$('#clone').css({visibility:'hidden'});
							$('#clone thead').css({'visibility':'visible','pointer-events':'auto'});
						}
						} else {
						$('#clone').remove();
						}
					}
					$(window).scroll(moveScroll); 
				});
			</script>
			<div id='table-container'>
			
			<table class='table' id='tableVerificaProdotti'>
			<thead>
			<tr>";
			
			echo "

			<th style='width:90px'>Codice<a href='index.php?tab=AmazonProducts&orderby=p.reference&orderway=asc&token=".$this->token."'></th>
			<th style='width:90px'>Codice SKU</th>
			
			<th style='width:90px'>EAN</th>
			
			<th style='width:90px'>ASIN</th>
			
			
			<th style='width:160px'>Prodotto </th>
			<th style='width:80px'>Costr. </th>
			<th style='width:30px'>Mag. EZ</th>
			
			<th style='width:30px'>Mag. Forn.</th>
			
			<th style='width:30px'>Tipo QT</th>
			<th style='width:30px'>Prezzo IT</th>
			<th style='width:30px'>Mag. Amz.</th>
			<th style='width:30px'>Sco. Min. Amz</th>
			<th style='width:50px'>IT</th>
			
			<th style='width:50px'>FR</th>
			
			<th style='width:50px'>UK</th>
			
			<th style='width:50px'>DE</th>
			
			<th style='width:50px'>ES</th>
			";
			
			echo "</tr></thead><tbody>";
			
			$tokenCatalog = Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee));
			$tipo_qt = '';
			
			
			foreach($prodotti as $p)
			{
				//$prezzo_standard = Product::trovaMigliorPrezzo($p['id_product'], 1, 99);
				$prezzo_standard = $p['prezzo'];
				
				$tax_rate = Db::getInstance()->getValue('SELECT rate FROM tax WHERE id_tax = 1');
				
				$id_speciale = Product::trovaIdPrezzoSpeciale($p['id_product']);
			
				if($id_speciale > 0)
				{
					$speciale = Product::trovaPrezzoSpeciale($p['id_product'], 1);
					
					//$speciale = $speciale + (($speciale*22)/100);
					//$speciale = round($speciale, 2);
					$prezzo_standard = $speciale;
				
					if($speciale != 0) {
						if($p['prezzo'] < $speciale) {
							
							$speciale = '';
							$data_speciale = '';
							$data_inizio_speciale = '';
							$sale = '';
						}
						else
						{
							
							$data_speciale = Db::getInstance()->getValue('SELECT sp.to FROM specific_price sp WHERE sp.id_specific_price = '.$id_speciale);
							$data_speciale = substr($data_speciale, 0, 10);
							$data_inizio_speciale = date("Y-m-d");
							/*$sale = '<Sale>
							<StartDate>'.$data_inizio_speciale.'T00:00:00Z</StartDate>
							<EndDate>'.$data_speciale.'T23:59:59Z</EndDate>
							<SalePrice currency="EUR">'.$speciale.'</SalePrice>
							</Sale>';*/
						
						}
					}
					else
					{
						$speciale = '';
						$data_speciale = '';
						$data_inizio_speciale = '';
						$sale = '';
					
					}
					
				}
				else
				{
					$speciale = '';
					$data_speciale = '';
					$data_inizio_speciale = '';
					$sale = '';
				}
				
				$prezzo_standard = $prezzo_standard - ($prezzo_standard * ($p['amazon_it'] / 100));
				
				
				$prezzo = $prezzo_standard + (($prezzo_standard*$tax_rate)/100);
			
				$prezzo = round($prezzo, 2);
				$p['amazon_price'] = round($p['amazon_price'], 2);
				
				if(($p['id_manufacturer'] == 117 || $p['id_manufacturer'] == 37 || $p['id_manufacturer'] == 43 || $p['id_manufacturer'] == 25 || $p['id_manufacturer'] == 108 || $p['id_manufacturer'] == 133 || $p['id_manufacturer'] == 184 || $p['id_manufacturer'] == 116 || $p['id_manufacturer'] == 182 || $p['id_manufacturer'] == 27 || $p['id_manufacturer'] == 120 || $p['id_manufacturer'] == 130 || $p['id_manufacturer'] == 158))
				{	
					$supplier =  Db::getInstance()->getValue('SELECT id_supplier FROM product WHERE id_product = '.$p['id_product']);
			
					switch($supplier)
					{
						case 11: $main_supplier = 'supplier_quantity '; break;
						case 8: $main_supplier = 'esprinet_quantity '; break;
						case 42: $main_supplier = 'itancia_quantity '; break;
						case 76: $main_supplier = 'asit_quantity '; break;
						case 1450: $main_supplier = 'esprinet_quantity '; break;
						default: $main_supplier = 'quantity'; break;
					}	
				
					if($supplier == 11 || $supplier == 8 || $supplier == 42  || $supplier == 76)
					{
						$quantita = Db::getInstance()->getValue('SELECT '.$main_supplier.' AS quantita FROM product WHERE id_product = '.$p['id_product']);
						$quantita += $p['stock_quantity'];
					}
					else
					{
						$quantita = Db::getInstance()->getValue('SELECT quantity AS quantita FROM product WHERE id_product = '.$p['id_product']);
					}
					if($p['id_manufacturer'] == 182)
						$quantita = Db::getInstance()->getValue('SELECT quantity FROM product WHERE id_product = '.$p['id_product']);

					$tipo_qt = 'EZ+F';
				}
				else
				{
					$quantita = $p['stock_quantity'];
					$tipo_qt = 'EZ';
				}
				
				if($p['id_manufacturer'] == 17)
					$quantita = Db::getInstance()->getValue('SELECT stock_quantity AS main_quantity FROM product WHERE id_product = '.$p['id_product']);	
					
				if($quantita < 0)
					$quantita = 0;
		
			
				$qta_ord_clienti = Db::getInstance()->getValue('SELECT sum(product_quantity - (REPLACE(cons,"0_",""))) FROM order_detail od JOIN orders o ON o.id_order = od.id_order
						JOIN cart c ON c.id_cart = o.id_cart
						JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE moh.id_order_state != 18 AND moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE c.name NOT LIKE "%prime%" AND  od.product_id = '.$p['id_product'].' AND os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state != 20 AND os.id_order_state != 26 AND os.id_order_state != 31 AND os.id_order_state != 32 AND os.id_order_state != 35 GROUP BY od.product_id');
						
				
				
				
				$quantita -= $qta_ord_clienti;
			
				if($quantita != $p['it'])
						$red = 'y';
					else
						$red = 'n';
					
				if($p['reference'] != '')
				{
					echo '<tr style=" '.($red == 'y' && ($p['ean13'] != '' && $p['asin'] != '') ? 'outline:1px solid red' : '').'">
					<td style="font-size:11px"><span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($p['id_product']).'"><a href="index.php?tab=AdminCatalog&id_product='.$p['id_product'].'&updateproduct&token='.$tokenCatalog.'">'.$p['reference'].'</a></span></td>
					
					<td style="font-size:11px">'.$p['supplier_reference'].'</td>
					
					<td style="font-size:11px; ">'.$p['ean13'].' '.($p['ean13'] == '' ? '<img src="../img/admin/module_notinstall.png" class="img_control" alt="No" title="Non presente" />' : '').'</td>
					<td style="font-size:11px; ">'.$p['asin'].' '.($p['asin'] == '' ? '<img src="../img/admin/module_notinstall.png" alt="No" class="img_control"  title="Non presente" />' : '').'</td>
					
					<td>'.$p['name'].'</td>
					<td style="text-align:right">'.$p['manufacturer'].' '.($p['sku_amazon'] != '' ? '<img src="../img/admin/prime.png"  alt="Prime" title="'.number_format($p['price_prime'],2,",",".").'" />' : '').'</td>
					<td style="text-align:right">'.$p['stock_quantity'].'</td>
					
					
					';
					
					switch($p['id_supplier'])
					{
						case 11: $main_supplier = 'supplier_quantity'; break;
						case 8: $main_supplier = 'esprinet_quantity'; break;
						case 42: $main_supplier = 'itancia_quantity'; break;
						case 76: $main_supplier = 'asit_quantity'; break;
						case 1450: $main_supplier = 'attiva_quantity'; break;
						default: $main_supplier = 'stock_quantity'; break;
					}	
					
					$quantita_fornitore = Db::getInstance()->getValue('SELECT '.$main_supplier.' AS main_quantity FROM product WHERE id_product = '.$p['id_product']); 
					
					if($quantita_fornitore < 0)
						$quantita_fornitore = 0;
					
					if($main_supplier == 'stock_quantity')
						$quantita_fornitore = 0;
					
					
					
					if($p['no_amazon'] != '')
						$mancata_pubblicazione = $p['no_amazon'];
					else
						$mancata_pubblicazione = 'Motivo mancata pubblicazione sconosciuto';
					
						
					echo '
					
					<td style="text-align:right;">'.($quantita_fornitore).'</td>
				
					<td style="text-align:left">'.$tipo_qt.'</td>
					<td style="font-size:11px; '.($p['ean13'] != '' && $p['asin'] != '' ? (($prezzo-$p['last_price']) > (0.02) || ($p['last_price'] - $prezzo) > (0.02)  ? 'border:1px solid red' : '').'">EZ: '.number_format($prezzo,2,",","").'<br />AMZ: '.number_format($p['last_price'],2,",","") : '">').'</td>
					<td style="text-align:right">'.$p['amazon_quantity'].'</td>
					<td style="text-align:right">'.$p['scorta_minima_amazon'].'</td>
					<td style="text-align:center">'.($p['it'] > 0 ? '<img src="../img/admin/module_install.png" alt="Presente su Amazon" /><span style="font-size:9px">'.$p['it'].' ('.$p['qt_last'].')</span>' : ((strpos($p['amazon'], '5') !== false) ? '<img src="../img/admin/module_disabled.png" class="img_control" title="Flaggato ma non presente su Amazon. '.$mancata_pubblicazione.'" alt="Flaggato ma non presente su Amazon" /><span style="font-size:9px">'.$p['it'].'</span>' : '')).'</td>
					<td style="text-align:center">'.($p['fr'] > 0 ? '<img src="../img/admin/module_install.png" alt="Presente su Amazon" />' : ((strpos($p['amazon'], '2') !== false) ? '<img src="../img/admin/module_disabled.png" class="img_control" title="Flaggato ma non presente su Amazon. '.$mancata_pubblicazione.'" alt="Flaggato ma non presente su Amazon" />' : '')).'</td>
					<td style="text-align:center">'.($p['uk'] > 0 ? '<img src="../img/admin/module_install.png" alt="Presente su Amazon" /><span style="font-size:9px">'.$p['uk'].'</span>' : ((strpos($p['amazon'], '1') !== false) ? '<img src="../img/admin/module_disabled.png" alt="Flaggato ma non presente su Amazon" class="img_control" title="Flaggato ma non presente su Amazon. '.$mancata_pubblicazione.'" /><span style="font-size:9px">'.$p['uk'].'</span>' : '')).'</td>
					<td style="text-align:center">'.($p['es'] > 0 ? '<img src="../img/admin/module_install.png" alt="Presente su Amazon" class="img_control" title="Flaggato ma non presente su Amazon. '.$mancata_pubblicazione.'" /><span style="font-size:9px">'.$p['es'].'</span>' : ((strpos($p['amazon'], '3') !== false) ? '<img src="../img/admin/module_disabled.png" class="img_control" title="Flaggato ma non presente su Amazon. '.$mancata_pubblicazione.'" alt="Flaggato ma non presente su Amazon" /><span style="font-size:9px">'.$p['es'].'</span>' : '')).'</td>
					<td style="text-align:center">'.($p['de'] > 0 ? '<img src="../img/admin/module_install.png" class="img_control" title="Flaggato ma non presente su Amazon. '.$mancata_pubblicazione.'" alt="Presente su Amazon" /><span style="font-size:9px">'.$p['de'].'</span>' : ((strpos($p['amazon'], '4') !== false) ? '<img src="../img/admin/module_disabled.png" class="img_control" title="Flaggato ma non presente su Amazon. '.$mancata_pubblicazione.'" alt="Flaggato ma non presente su Amazon" /><span style="font-size:9px">'.$p['de'].'</span>' : '')).'</td></tr>';
					
						$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue("A$k", $p['reference'])
							->setCellValue("B$k", $p['supplier_reference'])
							->setCellValue("C$k", $p['ean13'])
							->setCellValue("D$k", $p['asin'])
							->setCellValue("E$k", $p['name'])
							->setCellValue("F$k", $p['manufacturer'])
							->setCellValue("G$k", $p['stock_quantity'])
							->setCellValue("H$k", $quantita_fornitore)
							->setCellValue("I$k", $tipo_qt)
							->setCellValue("J$k", $p['amazon_quantity'])
							->setCellValue("K$k", $p['scorta_minima_amazon'])
							->setCellValue("L$k", ($p['ean13'] != '' && $p['asin'] != '' ?number_format($prezzo,2,",","") : ''))
							->setCellValue("M$k", ($p['ean13'] != '' && $p['asin'] != '' ?number_format($p['amazon_price'],2,",","") : ''))
							->setCellValue("N$k", $p['it'])
							->setCellValue("O$k", $p['fr'])
							->setCellValue("P$k", $p['uk'])
							->setCellValue("Q$k", $p['de'])
							->setCellValue("R$k", $p['es'])
								;
		
				
						$k++;
					
				}
			}
			
			
				echo "</tbody></table></div><div id='bottom_anchor'>
				</div>";
				
			
					$objPHPExcel->getActiveSheet()->setTitle('Prodotti su Amazon');

					$objPHPExcel->getActiveSheet()->getStyle('A1:R1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
						
					$objPHPExcel->getActiveSheet()->getStyle('A1:R1')->getFill()->getStartColor()->setRGB('FFFF00');	

					$objPHPExcel->getActiveSheet()->getStyle("A2:R$k")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle('A1:R1')->getFont()->setBold(true);

					$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
					$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
					$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
					$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
					$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);
					$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(15);
					
					$objPHPExcel->getActiveSheet()->getStyle("G2:G$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("H2:H$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("I2:I$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

					$objPHPExcel->getActiveSheet()->getStyle("J2:J$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("K2:K$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("L2:L$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("M2:M$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("N2:N$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("O2:O$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("P2:P$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("Q2:Q$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyle("R2:R$k")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

					// Set active sheet index to the first sheet, so Excel opens this as the first sheet
					$objPHPExcel->setActiveSheetIndex(0);

					$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
					$cacheSettings = array('memoryCacheSize ' => '8000MB');
					PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
					
					$data = date("Ymd");
					/*
					$gdImage = imagecreatefromjpeg('https://www.ezdirect.it/img/logo.jpg');
					// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
					$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
					$objDrawing->setName('Grafico');$objDrawing->setDescription('Grafico');
					$objDrawing->setImageResource($gdImage);
					$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
					$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
					$objDrawing->setHeight(150);
					$objDrawing->setCoordinates('I1');
					$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
					*/
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
					$objWriter->setIncludeCharts(TRUE);
					$objWriter->setPreCalculateFormulas(false);
					$objWriter->save(str_replace(".php", ".xls", "esportazione-catalogo/catalogo_xls/PRODOTTI-AMAZON.php"));

					$this->_html .= "<div style='visibility:hidden'><br /><br />FILE PRONTO PER IL DOWNLOAD: <a id='scaricaexcelstatistiche' href='http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-amazon.xls'>CLICCA QUI PER SCARICARE</a>!</div>";

					//header("Location: http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-amazon.xls");
				
				
			echo '<br /><a class="button" name="esportazionedati" onclick="window.onbeforeunload = null" href="http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/PRODOTTI-AMAZON.xls">CLICCA QUI PER ESPORTARE I DATI IN FORMATO EXCEL</a>
			<br />';
		}
		
		else
		{
			echo '<strong>Seleziona i prodotti con i filtri sotto</strong> (lasciali vuoti se vuoi la lista completa di tutti i prodotti):<br />';
			
			echo '<script type="text/javascript" src="../js/select2.js"></script>
				<script type="text/javascript">
				$(document).ready(function() { $("#per_prodotto").select2(); });
				
				$(document).ready(function() { $("#per_categoria").select2(); });
				
				$(document).ready(function() { $("#per_macrocategoria").select2(); });
				
				$(document).ready(function() { $("#per_marchio").select2(); });
				</script>
				
				
				<form name="cercaclienti" method="post" action="">
				
				Per codice prodotto:<br />
				<select multiple id="per_prodotto" name="per_prodotto[]" style="width:600px">
				<option name="0" value="0">--- Scegli un prodotto ---</option>
				';
				
				
				$results = Db::getInstance()->ExecuteS('SELECT reference, p.id_product, name FROM product p JOIN product_lang pl ON p.id_product = pl.id_product WHERE active = 1 AND (p.amazon != "" AND p.amazon != "0") AND pl.id_lang = 5 GROUP BY p.id_product ORDER BY reference ASC');
				
				foreach ($results as $row) {
				
					echo "<option name='$row[id_product]' value='$row[id_product]'"; 
					echo ">$row[reference] - $row[name]</option>";
				
				}
					
				echo "</select><br /><br />";
				
				echo 'Per macrocategoria:<br />
				<select multiple id="per_macrocategoria" name="per_macrocategoria[]" style="width:600px">
				<option name="0" value="0">--- Scegli una macrocategoria ---</option>
				';
				
				
				$resultscat = Db::getInstance()->ExecuteS("SELECT category.id_category, category_lang.name FROM category JOIN category_lang ON category.id_category = category_lang.id_category WHERE category_lang.id_lang = 5 AND category.id_parent = 1 ORDER BY name");
				
				foreach ($resultscat as $rowcat) {
				
					echo "<option name='$rowcat[id_category]' value='$rowcat[id_category]'"; 
					echo ">$rowcat[id_category] - $rowcat[name]</option>";
				
				}
					
				echo "</select><br /><br />";
				
				echo 'Per sottocategoria:<br />
				<select multiple id="per_categoria" name="per_categoria[]" style="width:600px">
				<option name="0" value="0">--- Scegli una categoria ---</option>
				';
				
				
				$resultscat = Db::getInstance()->ExecuteS("SELECT category.id_category, category_lang.name FROM category JOIN category_lang ON category.id_category = category_lang.id_category WHERE category_lang.id_lang = 5 AND category.id_parent != 1 ORDER BY name");
				
				foreach ($resultscat as $rowcat) {
				
					echo "<option name='$rowcat[id_category]' value='$rowcat[id_category]'"; 
					echo ">$rowcat[id_category] - $rowcat[name]</option>";
				
				}
					
				echo "</select><br /><br />";
				
				
				
				echo 'Per marchio:<br />
				<select multiple id="per_marchio" name="per_marchio[]" style="width:600px">
				<option name="0" value="0">--- Scegli un marchio ---</option>
				';
				
				
				$resultsman = Db::getInstance()->ExecuteS("SELECT id_manufacturer, name FROM manufacturer ORDER BY name");
				
				foreach ($resultsman as $rowman) {
				
					echo "<option name='$rowman[id_manufacturer]' value='$rowman[id_manufacturer]'"; 
					echo ">$rowman[id_manufacturer] - $rowman[name]</option>";
				
				}
					
				echo "</select><br /><br />";
			
				echo "<input type='submit' name='products' value='Cerca prodotti' class='button' />
				</form>";
			
		}
	}
		
		
}

