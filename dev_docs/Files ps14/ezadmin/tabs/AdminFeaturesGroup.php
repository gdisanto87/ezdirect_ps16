<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminFeaturesGroup extends AdminTab
{
	public function __construct()
	{
	 	$this->table = 'feature_group';
	 	$this->className = 'FeatureGroup';
	 	$this->lang = true;
	 	$this->edit = true;
	 	$this->delete = true;

		parent::__construct();
	}

	/**
	 * Display form
	 *
	 * @global string $currentIndex Current URL in order to keep current Tab
	 */
	public function displayForm($val)
	{
		global $currentIndex;
		parent::displayForm();

		if(!isset($val)) {
		
		echo '
		<h2>'.$this->l('Add a new feature group').'</h2>
		<form action="'.$currentIndex.'&token='.Tools::getValue('token').'" method="post">
	
		'.($obj->id ? '<input type="hidden" name="id_'.$this->table.'" value="'.$obj->id.'" />' : '').'
			<fieldset class="width2">
				<legend><img src="../img/t/AdminFeatures.gif" />'.$this->l('Add a new feature group').'</legend>
				<label>'.$this->l('Name:').'</label>
				<div class="margin-form">';
		foreach ($this->_languages AS $language)
			echo '
					<div id="name_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input size="33" type="text" name="name_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'name', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" /><sup> *</sup>
						<span class="hint" name="help_box">'.$this->l('Invalid characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
					</div>
				<script type="text/javascript">
					var flag_fields = \'name\';
				</script>';
		$this->displayFlags($this->_languages, $this->_defaultFormLanguage, 'flag_fields', 'name', false, true);
		echo '
					<div class="clear"></div>
				</div>
				'.Module::hookExec('featureGroupForm', array('id_feature' => $obj->id)).'
				<div class="margin-form">
					<input type="submit" value="'.$this->l('   Save   ').'" name="submitAddgroup" class="button" />
				</div>
				<div class="small"><sup>*</sup> '.$this->l('Required field').'</div>
			</fieldset>
		</form>';
		}
		
		else {
				
		echo '
		<h2>'.$this->l('Add a new feature group').'</h2>
		<form action="'.$currentIndex.'&token='.Tools::getValue('token').'" method="post">
	
		'.($obj->id ? '<input type="hidden" name="id_'.$this->table.'" value="'.$obj->id.'" />' : '').'
		<input type="hidden" name="groupid" value="'.$val.'" />
			<fieldset class="width2">
				<legend><img src="../img/t/AdminFeatures.gif" />'.$this->l('Add a new feature group').'</legend>
				<label>'.$this->l('Name:').'</label>
				<div class="margin-form">';
		foreach ($this->_languages AS $language)
		$rowsgr = Db::getInstance()->getRow("SELECT DISTINCT * FROM feature_group JOIN feature_group_lang ON feature_group.id_feature_group = feature_group_lang.id_feature_group WHERE feature_group.id_feature_group = $val AND feature_group_lang.id_feature_group_lang = ".$language['id_lang']."");
		
		foreach($rowsgr as $rowgr) {
		
			echo '
					<div id="name_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input size="33" type="text" name="name_'.$language['id_lang'].'" value="'.htmlentities($rowgr['name'], ENT_COMPAT, 'UTF-8').'" /><sup> *</sup>
						<span class="hint" name="help_box">'.$this->l('Invalid characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
					</div>
				<script type="text/javascript">
					var flag_fields = \'name\';
				</script>';
		$this->displayFlags($this->_languages, $this->_defaultFormLanguage, 'flag_fields', 'name', false, true);
		echo '
					<div class="clear"></div>
				</div>
				'.Module::hookExec('featureGroupForm', array('id_feature' => $obj->id)).'
				<div class="margin-form">
					<input type="submit" value="'.$this->l('   Save   ').'" name="submitAddgroup" class="button" />
				</div>
				<div class="small"><sup>*</sup> '.$this->l('Required field').'</div>
			</fieldset>
		</form>';
		
		}
		
		
		
		
		}
	}

	/**
	 * Manage page processing
	 *
	 * @global string $currentIndex Current URL in order to keep current Tab
	 */
	public function postProcess($token = NULL)
	{
		global $currentIndex;
		
		Module::hookExec('postProcessFeatureGroup',
		array('errors' => &$this->_errors)); // send _errors as reference to allow postProcessFeatureValue to stop saving process

		if (Tools::getValue('submitDel'.$this->table))
		{
			if ($this->tabAccess['delete'] === '1')
			{
				if (isset($_POST[$this->table.$_POST['groupid'].'Box']))
				{
					$object = new $this->className();
					if ($object->deleteSelection($_POST[$this->table.$_POST['groupid'].'Box']))
						
					$this->_errors[] = Tools::displayError('An error occurred while deleting selection.');
				}
				else
					$this->_errors[] = Tools::displayError('You must select at least one element to delete.');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to delete here.');
		}
		else
			parent::postProcess();
	}
}