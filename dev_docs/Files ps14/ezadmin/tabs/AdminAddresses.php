<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14920 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
include_once(PS_ADMIN_DIR.'/../classes/AdminTab.php');
if (Configuration::get('VATNUMBER_MANAGEMENT') AND file_exists(_PS_MODULE_DIR_.'vatnumber/vatnumber.php'))
	include_once(_PS_MODULE_DIR_.'vatnumber/vatnumber.php');

class AdminAddresses extends AdminTab
{
	/** @var array countries list */
	protected $countriesArray = array();

	public function __construct()
	{
	 	global $cookie;

	 	$this->table = 'address';
	 	$this->className = 'Address';
	 	$this->lang = false;
	 	$this->edit = true;
	 	$this->delete = true;
		$this->requiredDatabase = true;
		$this->addressType = 'customer';
		
		$this->_select = '
		CONCAT(cx.firstname," ",cx.lastname) customer,
		
		(CASE cx.is_company
		WHEN 1
		THEN (
			CASE cx.vat_number
			WHEN "" THEN cx.tax_code
			ELSE cx.vat_number
			END
		)
		WHEN 0
		THEN cx.tax_code
		ELSE cx.tax_code
		END
		) piva_cf,
		
		(SELECT iso_code FROM state WHERE id_state = (SELECT id_state FROM address WHERE id_customer = a.id_customer AND fatturazione = 1 AND active = 1 AND deleted = 0 LIMIT 1)) provincia,
		
		(CASE (SELECT id_country FROM address WHERE id_customer = a.id_customer AND fatturazione = 1 AND active = 1 AND deleted = 0 LIMIT 1)
		WHEN 10 THEN (CASE
		(SELECT iso_code FROM state WHERE id_state = (SELECT id_state FROM address WHERE id_customer = a.id_customer AND fatturazione = 1 AND active = 1 AND deleted = 0 LIMIT 1)) 
		WHEN "CH" THEN "ABR" WHEN "AQ" THEN "ABR" WHEN "PE" THEN "ABR" WHEN "TE" THEN "ABR"
		WHEN "MT" THEN "BAS" WHEN "PZ" THEN "BAS"
		WHEN "CZ" THEN "CAL" WHEN "CS" THEN "CAL" WHEN "KR" THEN "CAL" WHEN "RC" THEN "CAL" WHEN "VV" THEN "CAL"
		WHEN "AV" THEN "CAM" WHEN "BN" THEN "CAM" WHEN "CE" THEN "CAM" WHEN "NA" THEN "CAM" WHEN "SA" THEN "CAM"
		WHEN "BO" THEN "EMR" WHEN "FE" THEN "EMR" WHEN "FC" THEN "EMR" WHEN "MO" THEN "EMR" WHEN "PR" THEN "EMR" WHEN "PC" THEN "EMR" WHEN "RA" THEN "EMR" WHEN "RE" THEN "EMR" WHEN "RN" THEN "EMR"
		WHEN "GO" THEN "FVG" WHEN "PN" THEN "FVG" WHEN "TS" THEN "FVG" WHEN "UD" THEN "FVG"
		WHEN "FR" THEN "LAZ" WHEN "LT" THEN "LAZ" WHEN "RI" THEN "LAZ" WHEN "RM" THEN "LAZ" WHEN "VT" THEN "LAZ"
		WHEN "GE" THEN "LIG" WHEN "IM" THEN "LIG" WHEN "SP" THEN "LIG" WHEN "SV" THEN "LIG"
		WHEN "BG" THEN "LOM" WHEN "BS" THEN "LOM" WHEN "CO" THEN "LOM" WHEN "CR" THEN "LOM" WHEN "LC" THEN "LOM" WHEN "LO" THEN "LOM" WHEN "MN" THEN "LOM" WHEN "MI" THEN "LOM" WHEN "MB" THEN "LOM" WHEN "PV" THEN "LOM" WHEN "SO" THEN "LOM" WHEN "VA" THEN "LOM"
		WHEN "AN" THEN "MAR" WHEN "AP" THEN "MAR" WHEN "FM" THEN "MAR" WHEN "MC" THEN "MAR" WHEN "PU" THEN "MAR"
		WHEN "CB" THEN "MOL" WHEN "IS" THEN "MOL"
		WHEN "AL" THEN "PIE" WHEN "AT" THEN "PIE" WHEN "BI" THEN "PIE" WHEN "CN" THEN "PIE" WHEN "NO" THEN "PIE" WHEN "TO" THEN "PIE" WHEN "VB" THEN "PIE" WHEN "VC" THEN "PIE"
		WHEN "BA" THEN "PGL" WHEN "BR" THEN "PGL" WHEN "FG" THEN "PGL" WHEN "LC" THEN "PGL" WHEN "TA" THEN "PGL" WHEN "BT" THEN "PGL"
		WHEN "CA" THEN "SAR" WHEN "CI" THEN "SAR" WHEN "VS" THEN "SAR" WHEN "NU" THEN "SAR" WHEN "OG" THEN "SAR" WHEN "OT" THEN "SAR" WHEN "OR" THEN "SAR" WHEN "SS" THEN "SAR"
		WHEN "AG" THEN "SIC" WHEN "CL" THEN "SIC" WHEN "CT" THEN "SIC" WHEN "EN" THEN "SIC" WHEN "ME" THEN "SIC" WHEN "PA" THEN "SIC" WHEN "RG" THEN "SIC" WHEN "SR" THEN "SIC" WHEN "TP" THEN "SIC"
		WHEN "AR" THEN "TOS" WHEN "FI" THEN "TOS" WHEN "GR" THEN "TOS" WHEN "LI" THEN "TOS" WHEN "LU" THEN "TOS" WHEN "MS" THEN "TOS" WHEN "PI" THEN "TOS" WHEN "PT" THEN "TOS" WHEN "PO" THEN "TOS" WHEN "SI" THEN "TOS"
		WHEN "BZ" THEN "TAA" WHEN "TN" THEN "TAA"
		WHEN "PG" THEN "UMB" WHEN "TR" THEN "UMB"
		WHEN "AO" THEN "VAO"
		WHEN "BL" THEN "VEN" WHEN "PD" THEN "VEN" WHEN "RO" THEN "VEN" WHEN "TV" THEN "VEN" WHEN "VE" THEN "VEN" WHEN "VR" THEN "VEN" WHEN "VI" THEN "VEN"
		ELSE "--"
		END) 
		ELSE "EEE" END) regione';
		
		
		if (!Tools::getValue('realedit'))
			$this->deleted = true;
		//$this->_select = 'cl.`name` as country, cx.`tax_code` as txcode, cx.`company` as xcompany, cx.`vat_number` as xvatnumber';
		$this->_join = '
		LEFT JOIN (SELECT id_customer, firstname, lastname, is_company, tax_code, vat_number FROM '._DB_PREFIX_.'customer) cx ON (cx.`id_customer` = a.`id_customer`)
		LEFT JOIN `'._DB_PREFIX_.'country_lang` cl ON
		(cl.`id_country` = a.`id_country` AND cl.`id_lang` = '.(int)($cookie->id_lang).')';

		$countries = Country::getCountries((int)($cookie->id_lang));
		foreach ($countries AS $country)
			$this->countriesArray[$country['id_country']] = $country['name'];

		$this->fieldsDisplay = array(
		'id_address' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 25),
		
		'company' => array('title' => $this->l('Company'), 'font-size' =>'14px',  'width' => 50, 'widthColumn' => 100),

		'customer' => array('title' => $this->l('Persona'), 'width' => 40, 'filter_key' => 'customer', 'tmpTableFilter' => true, 'widthColumn' => 70),
		
		'address1' => array('title' => $this->l('Address'), 'width' => 200),
		'postcode' => array('title' => $this->l('Postcode/ Zip Code'), 'align' => 'right', 'width' => 50),
		'city' => array('title' => $this->l('City'), 'width' => 150),
		'provincia' => array('title' => $this->l('Prov.'), 'width' => 30, 'filter_key' => 'provincia', 'tmpTableFilter' => true, 'widthColumn' => 35),
		'regione' => array('title' => $this->l('Reg.'), 'width' => 30, 'filter_key' => 'regione', 'tmpTableFilter' => true, 'widthColumn' => 35),
		'country' => array('title' => $this->l('Country'), 'width' => 100, 'type' => 'select', 'select' => $this->countriesArray, 'filter_key' => 'cl!id_country'));

		parent::__construct();
		
		if(isset($_GET['back']) && !isset($_GET['addaddress'])) {
		header('Location: '.urldecode($_GET['back']));
		}
	}

	public function postProcess()
	{
		global $cookie;
		
		if (Tools::getIsset('submitAddaddress'))
		{
			// Transform e-mail in id_customer for parent processing
			if ($this->addressType == 'customer')
			{
				/*if (Validate::isEmail(Tools::getValue('email')))
				{
					$customer = new Customer();
					$customer->getByEmail(Tools::getValue('email'), null, true);
					if (Validate::isLoadedObject($customer)) 
						$_POST['id_customer'] = $customer->id;
					else 
						$this->_errors[] = Tools::displayError('This e-mail address is not registered.');
				}*/
				if ($id_customer = Tools::getValue('id_customer'))
				{
					$customer = new Customer((int)$id_customer);
					if (Validate::isLoadedObject($customer))
						$_POST['id_customer'] = $customer->id;
					else
						$this->_errors[] = Tools::displayError('Unknown customer');
				}
				else
					$this->_errors[] = Tools::displayError('Unknown customer');
				if (Country::isNeedDniByCountryId(Tools::getValue('id_country')) AND !Tools::getValue('dni'))
					$this->_errors[] = Tools::displayError('Identification number is incorrect or has already been used.');
			}
			
			

			// Check manufacturer selected
			if ($this->addressType == 'manufacturer')
			{
				$manufacturer = new Manufacturer((int)(Tools::getValue('id_manufacturer')));
				if (!Validate::isLoadedObject($manufacturer))
					$this->_errors[] = Tools::displayError('Manufacturer selected is not valid.');
			}

			/* If the selected country does not contain states */
			$id_state = (int)(Tools::getValue('id_state'));
			if ($id_country = Tools::getValue('id_country') AND $country = new Country((int)($id_country)) AND !(int)($country->contains_states) AND $id_state)
				$this->_errors[] = Tools::displayError('You have selected a state for a country that does not contain states.');

			/* If the selected country contains states, then a state have to be selected */
			if ((int)($country->contains_states) AND !$id_state)
				$this->_errors[] = Tools::displayError('An address located in a country containing states must have a state selected.');

			/* Check zip code */
			if ($country->need_zip_code)
			{
				$zip_code_format = $country->zip_code_format;
				if (($postcode = Tools::getValue('postcode')) AND $zip_code_format)
				{
					$zip_regexp = '/^'.$zip_code_format.'$/ui';
					$zip_regexp = str_replace(' ', '( |)', $zip_regexp);
					$zip_regexp = str_replace('-', '(-|)', $zip_regexp);
					$zip_regexp = str_replace('N', '[0-9]', $zip_regexp);
					$zip_regexp = str_replace('L', '[a-zA-Z]', $zip_regexp);
					$zip_regexp = str_replace('C', $country->iso_code, $zip_regexp);
					if (!preg_match($zip_regexp, $postcode))
						$this->_errors[] = Tools::displayError('Your zip/postal code is incorrect.').'<br />'.Tools::displayError('Must be typed as follows:').' '.str_replace('C', $country->iso_code, str_replace('N', '0', str_replace('L', 'A', $zip_code_format)));
				}
				elseif ($zip_code_format)
					$this->_errors[] = Tools::displayError('Postcode required.');
				elseif ($postcode AND !preg_match('/^[0-9a-zA-Z -]{4,9}$/ui', $postcode))
					$this->_errors[] = Tools::displayError('Your zip/postal code is incorrect.');
			}


			/* If this address come from order's edition and is the same as the other one (invoice or delivery one)
			** we delete its id_address to force the creation of a new one */
			if (Tools::getIsset('id_order'))
			{
				
				$_POST['back'] = ('index.php?tab=AdminAddresses&id_address='.Tools::getValue('id_address').'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&token='.$this->token.'').'&id_order='.Tools::getValue('id_order');
				$this->_redirect = false;
				if (isset($_POST['address_type']))
					$_POST['id_address'] = '';
			}
			
			$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee));
		
			
		}
		
		
		if (!sizeof($this->_errors))
			parent::postProcess();
	
		/* Reassignation of the order's new (invoice or delivery) address */
		$address_type = ((int)(Tools::getValue('address_type')) == 2 ? 'invoice' : ((int)(Tools::getValue('address_type')) == 1 ? 'delivery' : ''));
		if ($id_order = (int)(Tools::getValue('id_order')) AND !sizeof($this->_errors) )
		{
			$order_delivery = Db::getInstance()->getValue('SELECT id_address_delivery FROM orders WHERE `id_order` = '.$id_order);
			$order_invoice =  Db::getInstance()->getValue('SELECT id_address_invoice FROM orders WHERE `id_order` = '.$id_order);
			
			$new_address = Db::getInstance()->getValue('SELECT id_address FROM address WHERE `id_customer` = '.Tools::getValue('id_customer').' ORDER BY id_address DESC'); 
			
			if(Tools::getValue('id_address') == $order_delivery)
			{
				Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'orders SET `id_address_delivery` = '.$new_address.' WHERE `id_order` = '.$id_order);
			}
			if(Tools::getValue('id_address') == $order_invoice)
			{
				Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'orders SET `id_address_invoice` = '.$new_address.' WHERE `id_order` = '.$id_order);
			}
			
			//Product::RecuperaCSV($id_order);
			
			global $cookie;
				//if(Tools::getIsset('fromTabCustomer')) 
					Tools::redirectAdmin('index.php?tab=AdminCustomers&id_customer='.Tools::getValue('id_customer').'&viewcustomer&token='.Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee).'&conf=4&tab-container-1=2');
				//else 
				//	Tools::redirectAdmin(Tools::getValue('back').'&conf=4');
			
		}
		
	}

	public function getList($id_lang, $orderBy = NULL, $orderWay = NULL, $start = 0, $limit = NULL)
	{
		global $cookie;
	 	parent::getList($id_lang, $orderBy, $orderWay, $start, $limit);
		
		if(Tools::getIsset('id_address')) {
		
			if(!Tools::getIsset('id_customer'))
				$id_customer = Db::getInstance()->getValue('SELECT id_customer FROM address WHERE id_address = '.Tools::getValue('id_address').'');
			else
				$id_customer = Tools::getValue('id_customer');
			
			$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($cookie->id_employee));
			
			Tools::redirectAdmin('index.php?tab=AdminCustomers&id_customer='.$id_customer.'&viewcustomer&token='.$tokenCustomers.'&conf=4&tab-container-1=2');

		}
		
		global $cookie;

	 	/* Manage default params values */
	 	if (empty($limit))
			$limit = ((!isset($cookie->{$this->table.'_pagination'})) ? $this->_pagination[0] : $limit = $cookie->{$this->table.'_pagination'});

	 	if (!Validate::isTableOrIdentifier($this->table))
	 		die('filter is corrupted');
	 	if (empty($orderBy))
			$orderBy = Tools::getValue($this->table.'Orderby', 'id_'.$this->table);
	 	if (empty($orderWay))
			$orderWay = Tools::getValue($this->table.'Orderway', 'ASC');
		$limit = (int)(Tools::getValue('pagination', $limit));
		$cookie->{$this->table.'_pagination'} = $limit;

		/* Check params validity */
		if (!Validate::isOrderBy($orderBy) OR !Validate::isOrderWay($orderWay)
			OR !is_numeric($start) OR !is_numeric($limit)
			OR !Validate::isUnsignedId($id_lang))
			die(Tools::displayError('get list params is not valid'));

		/* Determine offset from current page */
		if ((isset($_POST['submitFilter'.$this->table]) OR
		isset($_POST['submitFilter'.$this->table.'_x']) OR
		isset($_POST['submitFilter'.$this->table.'_y'])) AND
		!empty($_POST['submitFilter'.$this->table]) AND
		is_numeric($_POST['submitFilter'.$this->table]))
			$start = (int)($_POST['submitFilter'.$this->table] - 1) * $limit;

		/* Cache */
		$this->_lang = (int)($id_lang);
		$this->_orderBy = $orderBy;
		$this->_orderWay = Tools::strtoupper($orderWay);

		/* SQL table : orders, but class name is Order */
		$sqlTable = $this->table == 'order' ? 'orders' : $this->table;

		/* Query in order to get results number */
		$queryTotal = Db::getInstance()->getRow('
		SELECT COUNT(a.`id_'.$this->table.'`) AS total
		FROM `'._DB_PREFIX_.$sqlTable.'` a
		'.($this->lang ? 'LEFT JOIN `'._DB_PREFIX_.$this->table.'_lang` b ON (b.`id_'.$this->table.'` = a.`id_'.$this->table.'` AND b.`id_lang` = '.(int)($id_lang).')' : '').'
		'.(isset($this->_join) ? $this->_join.' ' : '').'
		WHERE 1 '.(isset($this->_where) ? $this->_where.' ' : '').(($this->deleted OR $this->table == 'currency') ? 'AND a.`deleted` = 0 ' : '').$this->_filter.'
		'.(isset($this->_group) ? $this->_group.' ' : '').'
		'.(isset($this->addressType) ? 'AND a.id_'.strval($this->addressType).' != 0' : ''));
		$this->_listTotal = (int)($queryTotal['total']);

		/* Query in order to get results with all fields */
		$this->_list = Db::getInstance()->ExecuteS('
		SELECT a.*'.($this->lang ? ', b.*' : '').(isset($this->_select) ? ', '.$this->_select.' ' : '').'
		FROM `'._DB_PREFIX_.$sqlTable.'` a
		'.($this->lang ? 'LEFT JOIN `'._DB_PREFIX_.$this->table.'_lang` b ON (b.`id_'.$this->table.'` = a.`id_'.$this->table.'` AND b.`id_lang` = '.(int)($id_lang).')' : '').'
		'.(isset($this->_join) ? $this->_join.' ' : '').'
		WHERE 1 '.(isset($this->_where) ? $this->_where.' ' : '').(($this->deleted OR $this->table == 'currency') ? 'AND a.`deleted` = 0 ' : '').$this->_filter.'
		'.(isset($this->_group) ? $this->_group.' ' : '').'
		'.(isset($this->addressType) ? 'AND a.id_'.strval($this->addressType).' != 0' : '').'
		ORDER BY '.(($orderBy == 'id_address') ? 'a.' : '').'`'.bqSQL($orderBy).'` '.bqSQL($orderWay).'
		LIMIT '.(int)($start).','.(int)($limit));
	}

	public function displayForm($isMainTab = true)
	{
		global $currentIndex, $cookie;
		parent::displayForm();
		$obj = new Address(Tools::getValue('id_address'));

		$tokenAddresses = Tools::getAdminToken('AdminAddresses'.(int)(Tab::getIdFromClassName('AdminAddresses')).(int)($cookie->id_employee));
				
		echo '
		<form action="index.php?tab=AdminAddresses&id_address='.$obj->id.'&fromTabCustomer&id_customer='.Tools::getValue('id_customer').'&submitAddaddress=1&token='.$tokenAddresses.'&idclientee='.Tools::getValue('id_customer').'" method="post">
		'.((int)($obj->id) ? '<input type="hidden" name="id_address" value="'.(int)($obj->id).'" />' : '').'
		'.(($id_order = (int)(Tools::getValue('id_order'))) ? '<input type="hidden" name="id_order" value="'.(int)($id_order).'" />' : '').'
		'.(($address_type = (int)(Tools::getValue('address_type'))) ? '<input type="hidden" name="address_type" value="'.(int)($address_type).'" />' : '').'
		'.(Tools::getValue('realedit') ? '<input type="hidden" name="realedit" value="1" />' : '').'
		
			<fieldset>
				<legend><img src="../img/admin/contact.gif" alt="" />'.$this->l('Indirizzo').'</legend>';
				
		if($this->table == 'customer') 
			echo '<input type="hidden" name="fromTabCustomer" value="1" />';
		
		
		switch ($this->addressType)
		{
			case 'manufacturer':
				echo '<label>'.$this->l('Choose manufacturer').'</label>
				<div class="margin-form">';
				$manufacturers = Manufacturer::getManufacturers();
				echo '<select name="id_manufacturer">';
				if (!sizeof($manufacturers))
					echo '<option value="0">'.$this->l('No manufacturer available').'&nbsp</option>';
				foreach ($manufacturers as $manufacturer)
					echo '<option value="'.(int)($manufacturer['id_manufacturer']).'"'.($this->getFieldValue($obj, 'id_manufacturer') == $manufacturer['id_manufacturer'] ? ' selected="selected"' : '').'>'.$manufacturer['name'].'&nbsp</option>';
				echo	'</select>';
				echo '</div>';
				echo '<input type="hidden" name="alias" value="manufacturer">';
				break;
			case 'customer':
			default:
			
			if(isset($_GET['idclientee'])) {
				$customer = new Customer($_GET['idclientee']);
			}
			else {
				$customer = new Customer($obj->id_customer);
			}
			
			if(isset($_GET['fatturazione'])) {
				$fatturazione = 1;
			}
			else {
				$fatturazione = ($this->getFieldValue($obj, 'fatturazione'));
			}
			
			
			echo '<input type="hidden" name="id_customer" value="'.$customer->id.'" />
			<style type="text/css">
			div.margin-form
			{ margin-top:-15px;
			}
			</style>
			<input type="hidden" name="fatturazione" value="'.$fatturazione.'" />
			<input type="hidden" name="email" value="'.$customer->email.'" />';
			
			$row = Db::getInstance()->getRow("SELECT is_company FROM customer WHERE id_customer = $obj->id_customer");
			$is_company = $row['is_company'];
			
			echo '<label>'.$this->l('Tipo').'</label>
						<div class="margin-form" style="font-size:15px; color:#000">
						'.($fatturazione == 1 ? 'Fatturazione' : 'Consegna').'
						</div>';
				
				

								echo '<label>'.$this->l('Company').'</label>
						<div class="margin-form">
						<input type="text" size="33" name="company" value="'.htmlentities($this->getFieldValue($obj, 'company'), ENT_COMPAT, 'UTF-8').'" />
						<span >'.$this->l('Invalid characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
						</div>';
				
				
				
				
				break;
		}

		$addresses_fields = AdminAddresses::processAddressFormat();
		$addresses_fields = $addresses_fields["dlv_all_fields"];	// we use  delivery address



		foreach($addresses_fields as $addr_field_item)
		{
			if ($addr_field_item == 'company')
			{
				if ($this->addressType != 'manufacturer')
				{
				

					if ((Configuration::get('VATNUMBER_MANAGEMENT') AND file_exists(_PS_MODULE_DIR_.'vatnumber/vatnumber.php')) && VatNumber::isApplicable(Configuration::get('PS_COUNTRY_DEFAULT')))
						echo '<div id="vat_area" style="display: visible">';
					elseif (Configuration::get('VATNUMBER_MANAGEMENT'))
						echo '<div id="vat_area" style="display: hidden">';
					else
						echo'<div style="display: none;">';

					echo '
						</div>';
				}
			}
			
		

			
			
			
			
			elseif ($addr_field_item == 'lastname')
			{
				echo '
					<label>'.$this->l('Cognome').'</label>
					<div class="margin-form">
					<input type="text" size="33" name="lastname" value="'.htmlentities($this->getFieldValue($obj, 'lastname'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup>
					<span >'.$this->l('Invalid characters:').' 0-9!<>,;?=+()@#"�{}_$%:<span class="hint-pointer">&nbsp;</span></span>
					</div>';
			}
			elseif ($addr_field_item == 'firstname')
			{

				echo '
					<label>'.$this->l('Nome').'</label>
					<div class="margin-form">
					<input type="text" size="33" name="firstname" value="'.htmlentities($this->getFieldValue($obj, 'firstname'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup>
					<span >'.$this->l('Invalid characters:').' 0-9!<>,;?=+()@#"�{}_$%:<span class="hint-pointer">&nbsp;</span></span>
					</div>';
			}
			
			
			elseif ($addr_field_item == 'country' || $addr_field_item == 'Country:name')
			{

				
				echo '<script type="text/javascript" src="https://www.ezdirect.it/themes/ezdirect-new/js/tools.js"></script>
					<label>'.$this->l('Nazione').'</label>
					<div class="margin-form">
					<select name="id_country" id="id_country" onchange="countryChange();" />';
				$selectedCountry = $this->getFieldValue($obj, 'id_country');
				foreach ($this->countriesArray AS $id_country => $name)
					echo '		<option value="'.$id_country.'"'.((!$selectedCountry AND Configuration::get('PS_COUNTRY_DEFAULT') == $id_country) ? ' selected="selected"' : ($selectedCountry == $id_country ? ' selected="selected"' : '')).'>'.$name.'</option>';
				echo '		</select> <sup>*</sup>
					</div>';


				$id_country_ajax = (int)$this->getFieldValue($obj, 'id_country');
$selectedState = $this->getFieldValue($obj, 'id_state');

				echo '
				<script type="text/javascript">
							$(document).ready(function(){
								countryChange();
							});	
							</script>
					<script type="text/javascript">
					$(document).ready(function(){
							ajaxStates ();
							$(\'#id_country\').change(function() {
								ajaxStates ();
								});
							function ajaxStates ()
							{
								$.ajax({
									url: "ajax.php",
									cache: false,
									data: "ajaxStates=1&id_country="+$(\'#id_country\').val()+"&id_state="+$(\'#id_state\').val(),
									success: function(html)
										{
											if (html == \'false\')
											{
												$("#contains_states").fadeOut();
												$(\'#id_state option[value=0]\').attr("selected", "selected");
}
											else
											{
												$("#id_state").html(html);
												$("#contains_states").fadeIn();
												$(\'#id_state option[value='.$selectedState.']\').attr("selected", "selected");
											}
										}
									}); ';
		if (file_exists(_PS_MODULE_DIR_.'vatnumber/ajax.php'))
			echo '	$.ajax({
					type: "GET",
					url: "'._MODULE_DIR_.'vatnumber/ajax.php?id_country="+$(\'#id_country\').val(),
					success: function(isApplicable)
						{
							if (isApplicable == 1)
								$(\'#vat_area\').show();
							else
								$(\'#vat_area\').hide();
						}
					});';
		echo '	}; }); </script>';
		}
		
			elseif ($addr_field_item == 'postcode')
			{

				echo '
					<div class="ee_address"><label>'.$this->l('CAP').'</label>
					<div class="margin-form">
					<input type="text" size="33" name="postcode" id="postcode" value="'.htmlentities($this->getFieldValue($obj, 'postcode'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup>
					</div></div>';
					
					
					echo '<div class="it_address"><label>'.$this->l('CAP').'</label>
					<div class="margin-form">
					<input type="text" size="33" name="postcode" id="cap" value="'.htmlentities($this->getFieldValue($obj, 'postcode'), ENT_COMPAT, 'UTF-8').'" onkeyup="cercaCitta();" /> <sup>*</sup> <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" alt="Inserisci il CAP, se trovato nel sistema aggiunge in automatico città e provincia nei campi successivi" title="Inserisci il CAP, se trovato nel sistema aggiunge in automatico città e provincia nei campi successivi" />
					</div></div>';
					
					echo '
					<label>'.$this->l('Località / Frazione').'</label>
					<div class="margin-form">
					<input type="text" size="33" name="suburb" value="'.htmlentities($this->getFieldValue($obj, 'suburb'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>';
					
					
					
					
			}
			elseif ($addr_field_item == 'city')
			{

				echo '
					<div class="ee_address"><label>'.$this->l('Città').'</label>
					<div class="margin-form">
					<input type="text" size="33" name="city" id="city" value="'.htmlentities($this->getFieldValue($obj, 'city'), ENT_COMPAT, 'UTF-8').'"  /> <sup>*</sup>
					</div></div>';
					
						echo '
					<div class="it_address"><label>'.$this->l('Città').'</label>
					<div class="margin-form">
					<select name="city" id="citta" onclick="cercaProvincia();">
										<option name="">-- Prima selezionare CAP --</option>
										'.(isset($_POST['city']) ? '<option name="'.$_POST['city'].'" selected="selected">'.$_POST['city'].'</option>' : '').'
										
										'.($this->getFieldValue($obj, 'city') != '' ? '<option name="'.$this->getFieldValue($obj, 'city').'" selected="selected">'.$this->getFieldValue($obj, 'city').'</option>' : '').'
									</select>
									<sup>*</sup>
					</div></div>';
					
					echo '
				<div class="ee_address"><label>'.$this->l('Provincia').'</label>
				<div class="margin-form">
				<select name="id_state" id="id_state">
				</select>
				<sup>*</sup></div></div>';
				
				echo '
				<div class="it_address"><label>'.$this->l('Provincia').'</label>
				<div class="margin-form">
					<input id="provincia" name="provincia_mark" readonly="readonly" type="text" value="'.(isset($_POST['provincia_mark']) ? $_POST['provincia_mark'] : ($this->getFieldValue($obj, 'id_state') != '' ? Db::getInstance()->getValue('SELECT name FROM state WHERE id_state = '.$this->getFieldValue($obj, 'id_state')) : '')).'" />
				<input id="provincia_hidden" name="id_state" type="hidden" value="'.(isset($_POST['id_state']) ? $_POST['id_state'] : ($this->getFieldValue($obj, 'id_state') != '' ? $this->getFieldValue($obj, 'id_state') : '')).'" />
				</select>
				<sup>*</sup></div></div>';
			}
			
		elseif ($addr_field_item == 'address1')
			{
				echo '
					<label>'.$this->l('C/O').'</label>
					<div class="margin-form">
					<input type="text" size="33" name="c_o" value="'.htmlentities($this->getFieldValue($obj, 'c_o'), ENT_COMPAT, 'UTF-8').'" /> 
					</div>';

				echo '
					<label>'.$this->l('Address').'</label>
					<div class="margin-form">
					<script type="text/javascript">
									function suggerisciIndirizzo(indirizzo, cap, citta, campo) {
										$.ajax({
											  url:"https://www.ezdirect.it/cap.php?suggerisciIndirizzo=y",
											  type: "POST",
											  data: { 
											  indirizzo: indirizzo,
											  cap: cap,
											  citta: citta,
											  campo: campo
											  },
											  success:function(r){
												console.log(r);
												$("#address1_consigli").html(r)
											  },
											  error: function(xhr,stato,errori){
												
											  }
										});
									
									}
									</script>
									
					<input onkeyup="suggerisciIndirizzo(this.value, document.getElementById(\'cap\').value, document.getElementById(\'citta\').value, \'address1\')" type="text" size="33" id="address1" name="address1" value="'.htmlentities($this->getFieldValue($obj, 'address1'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup>
					
					<div id="address1_consigli">
			
									</div>
									
					</div>';
			}
	} // End foreach
			echo '
				<label>'.$this->l('Tel. Fisso').'</label>
				<div class="margin-form">
					<input type="text" size="33" name="phone" onkeyup="this.value = this.value.replace(/\s/g,\'\');" value="'.htmlentities($this->getFieldValue($obj, 'phone'), ENT_COMPAT, 'UTF-8').'" />
				</div><sup>*</sup>';

			echo '
				<label>'.$this->l('Tel. Cellulare').'</label>
				<div class="margin-form">
					<input type="text" size="33" onkeyup="this.value = this.value.replace(/\s/g,\'\');" name="phone_mobile" value="'.htmlentities($this->getFieldValue($obj, 'phone_mobile'), ENT_COMPAT, 'UTF-8').'" />
				</div>';

		echo '
				<label>'.$this->l('Fax').'</label>
				<div class="margin-form">
					<input type="text" size="33" onkeyup="this.value = this.value.replace(/\s/g,\'\');" name="fax" value="'.htmlentities($this->getFieldValue($obj, 'fax'), ENT_COMPAT, 'UTF-8').'" />
				</div>';
				
				
			echo '
				<label>'.$this->l('Altro / Note').'</label>
				<div class="margin-form">
					<textarea name="other" cols="36" rows="4">'.htmlentities($this->getFieldValue($obj, 'other'), ENT_COMPAT, 'UTF-8').'</textarea>
					<span >'.$this->l('Forbidden characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
				</div>';
				
						
				echo '<label>'.$this->l('Nome di questo indirizzo').'</label>
				<div class="margin-form">
					<input type="text" size="33" name="alias" value="'.htmlentities($this->getFieldValue($obj, 'alias'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup>
					<span >'.$this->l('Invalid characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
				</div>';

			echo '
				<div class="margin-form">
				<br />
				<input type="hidden" name="stay_here" value="on" />
				'.(Tools::getIsset('id_order') ? '<input type="hidden" name="id_order" value="'.Tools::getValue('id_order').'" />' : '').'
					<input value="Salva" type="submit" class="button" name="submitAddaddress" />
					<!-- <img src="../img/admin/save.gif" alt="Salva" title="Salva" />&nbsp;&nbsp;&nbsp;'.$this->l('   Save   ').'
					</button> -->
				</div>
				<div class="small"><sup>*</sup> '.$this->l('Required field').'</div>
			</fieldset>';
		echo '
		</form>';
	}

	protected function processAddressFormat()
	{
		$tmp_addr = new Address((int)Tools::getValue("id_address"));

		$selectedCountry = ($tmp_addr && $tmp_addr->id_country) ? $tmp_addr->id_country :
				(int)(Configuration::get('PS_COUNTRY_DEFAULT'));

		$inv_adr_fields = AddressFormat::getOrderedAddressFields($selectedCountry, false, true);
		$dlv_adr_fields = AddressFormat::getOrderedAddressFields($selectedCountry, false, true);

		$inv_all_fields = array();
		$dlv_all_fields = array();

		$out = array();

		foreach (array('inv','dlv') as $adr_type)
		{
			foreach (${$adr_type.'_adr_fields'} as $fields_line)
				foreach(explode(' ',$fields_line) as $field_item)
					${$adr_type.'_all_fields'}[] = trim($field_item);


			$out[$adr_type.'_adr_fields'] =  ${$adr_type.'_adr_fields'};
			$out[$adr_type.'_all_fields'] =  ${$adr_type.'_all_fields'};
		}

		return $out;
	}
	
	public function displayListContent($token = NULL)
	{
		/* Display results in a table
		 *
		 * align  : determine value alignment
		 * prefix : displayed before value
		 * suffix : displayed after value
		 * image  : object image
		 * icon   : icon determined by values
		 * active : allow to toggle status
		 */

		global $currentIndex, $cookie;
		$currency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));

		
		
		$id_category = 1; // default categ

		$irow = 0;
		if ($this->_list AND isset($this->fieldsDisplay['position']))
		{
			$positions = array_map(create_function('$elem', 'return (int)($elem[\'position\']);'), $this->_list);
			sort($positions);
		}
		if ($this->_list)
		{
			$isCms = false;
			
			$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$cookie->id_employee);
			
			if (preg_match('/cms/Ui', $this->identifier))
				$isCms = true;
			$keyToGet = 'id_'.($isCms ? 'cms_' : '').'category'.(in_array($this->identifier, array('id_category', 'id_cms_category')) ? '_parent' : '');
			foreach ($this->_list AS $tr)
			{
				$id = $tr[$this->identifier];
				echo '<tr'.(array_key_exists($this->identifier,$this->identifiersDnd) ? ' id="tr_'.(($id_category = (int)(Tools::getValue('id_'.($isCms ? 'cms_' : '').'category', '1'))) ? $id_category : '').'_'.$id.'_'.$tr['position'].'"' : '').($irow++ % 2 ? ' class="alt_row"' : '').' '.((isset($tr['color']) AND $this->colorOnBackground) ? 'style="background-color: '.$tr['color'].'"' : '').'>
							<td style="width:30px" class="center">';
				if ($this->delete AND (!isset($this->_listSkipDelete) OR !in_array($id, $this->_listSkipDelete)))
					echo '<input type="checkbox" name="'.$this->table.'Box[]" value="'.$id.'" class="noborder" />';
				echo '</td>';
				foreach ($this->fieldsDisplay AS $key => $params)
				{
					$tmp = explode('!', $key);
					$key = isset($tmp[1]) ? $tmp[1] : $tmp[0];
					echo '
					<td '.(isset($params['widthColumn']) ? 'style="width: '.$params['widthColumn'].'px; "' :  'style="width: '.$params['width'].'px;"').' '.(isset($params['position']) ? ' id="td_'.(isset($id_category) AND $id_category ? $id_category : 0).'_'.$id.'"' : '').' class="'.((!isset($this->noLink) OR !$this->noLink) ? 'pointer' : '').((isset($params['position']) AND $this->_orderBy == 'position')? ' dragHandle' : ''). (isset($params['align']) ? ' '.$params['align'] : '').'" ';
					if (!isset($params['position']) AND (!isset($this->noLink) OR !$this->noLink))
					{
						$fatturazione = Db::getInstance()->getValue('SELECT fatturazione FROM address WHERE id_address = '.$id);
						if($fatturazione = 1)
							$fatt_cons = 'fatturazione=1';
						else
							$fatt_cons = 'consegna=1';
						
						echo ' onclick="document.location = \'index.php?tab=AdminCustomers&id_customer='.$tr['id_customer'].'&viewcustomer&'.$this->identifier.'='.$id.($this->view? '&view' : '&update').$this->table.'&tab-container-1=2&newaddress&addaddress&'.$fatt_cons.'&token='.$tokenCustomers.'\'">'.(isset($params['prefix']) ? $params['prefix'] : '');
					}
					else
						echo '>';
					if(in_array($this->identifier, array('id_category')) && $params['title'] == 'Nome') 
					{
						if(Category::checkIfHasChildren($id)) {
							echo "<strong>";
						}
						else 
						{
						}
					}
					if (isset($params['active']) AND isset($tr[$key]))
						$this->_displayEnableLink($token, $id, $tr[$key], $params['active'], Tools::getValue('id_category'), Tools::getValue('id_product'));
					elseif (isset($params['activeVisu']) AND isset($tr[$key]))
						echo '<img src="../img/admin/'.($tr[$key] ? 'enabled.gif' : 'disabled.gif').'"
						alt="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" title="'.($tr[$key] ? $this->l('Enabled') : $this->l('Disabled')).'" />';
					elseif (isset($params['position']))
					{
						if ($this->_orderBy == 'position' AND $this->_orderWay != 'DESC')
						{
							/*echo '<a'.(!($tr[$key] != $positions[sizeof($positions) - 1]) ? ' style="display: none;"' : '').' href="'.$currentIndex.
									'&'.$keyToGet.'='.(int)($id_category).'&'.$this->identifiersDnd[$this->identifier].'='.$id.'
									&way=1&position='.(int)($tr['position'] + 1).'&token='.($token!=NULL ? $token : $this->token).'">
									<img src="../img/admin/'.($this->_orderWay == 'ASC' ? 'down' : 'up').'.gif"
									alt="'.$this->l('Down').'" title="'.$this->l('Down').'" /></a>';

							echo '<a'.(!($tr[$key] != $positions[0]) ? ' style="display: none;"' : '').' href="'.$currentIndex.
									'&'.$keyToGet.'='.(int)($id_category).'&'.$this->identifiersDnd[$this->identifier].'='.$id.'
									&way=0&position='.(int)($tr['position'] - 1).'&token='.($token!=NULL ? $token : $this->token).'">
									<img src="../img/admin/'.($this->_orderWay == 'ASC' ? 'up' : 'down').'.gif"
									alt="'.$this->l('Up').'" title="'.$this->l('Up').'" /></a>';	*/				

							echo	'<img src="../img/admin/up-and-down.gif"
									alt="'.$this->l('Up').'" title="'.$this->l('Up').'" />';
						}
						else
							echo (int)($tr[$key] + 1);
					}
					elseif (isset($params['image']))
					{
						// item_id is the product id in a product image context, else it is the image id.
						$item_id = isset($params['image_id']) ? $tr[$params['image_id']] : $id;
						// If it's a product image
						if (isset($tr['id_image']))
						{
							$image = new Image((int)$tr['id_image']);
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$image->getExistingImgPath().'.'.$this->imageType;
						}else
							$path_to_image = _PS_IMG_DIR_.$params['image'].'/'.$item_id.(isset($tr['id_image']) ? '-'.(int)($tr['id_image']) : '').'.'.$this->imageType;

						echo cacheImage($path_to_image, $this->table.'_mini_'.$item_id.'.'.$this->imageType, 45, $this->imageType);
					}
					elseif (isset($params['icon']) AND (isset($params['icon'][$tr[$key]]) OR isset($params['icon']['default'])))
						echo '<img src="../img/admin/'.(isset($params['icon'][$tr[$key]]) ? $params['icon'][$tr[$key]] : $params['icon']['default'].'" alt="'.$tr[$key]).'" title="'.$tr[$key].'" />';
					elseif (isset($params['price']))
						echo Tools::displayPrice($tr[$key], (isset($params['currency']) ? Currency::getCurrencyInstance((int)($tr['id_currency'])) : $currency), false);
					elseif (isset($params['float']))
						echo rtrim(rtrim($tr[$key], '0'), '.');
					elseif (isset($params['type']) AND $params['type'] == 'date')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang);
					elseif (isset($params['type']) AND $params['type'] == 'datetime')
						echo Tools::displayDate($tr[$key], (int)$cookie->id_lang, true);
					elseif (isset($tr[$key]))
					{
						$echo = ($key == 'price' ? round($tr[$key], 2) : isset($params['maxlength']) ? Tools::substr(Tools::htmlentitiesUTF8($tr[$key]), 0, $params['maxlength']).'...' : Tools::htmlentitiesUTF8($tr[$key]));
						echo isset($params['callback']) ? call_user_func_array(array($this->className, $params['callback']), array($echo, $tr)) : $echo;
					}
					else
						echo '--';

					echo (isset($params['suffix']) ? $params['suffix'] : '').
					'</td>';
				}

				if ($this->edit OR $this->delete OR ($this->view AND $this->view !== 'noActionColumn'))
				{
					echo '<td class="center" style="white-space: nowrap;">';
					if ($this->view)
						$this->_displayViewLink($token, $id);
					if ($this->edit)
						$this->_displayEditLink($token, $id);
					if ($this->delete AND (!isset($this->_listSkipDelete) OR !in_array($id, $this->_listSkipDelete)))
						$this->_displayDeleteLink($token, $id);
					if ($this->duplicate)
						$this->_displayDuplicate($token, $id);
					echo '</td>';
				}
				echo '</tr>';
			}
		}
	}
}


