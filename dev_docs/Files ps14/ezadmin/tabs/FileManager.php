<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14632 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class FileManager extends AdminTab
{
		public function display()
		{	
			global $cookie, $currentIndex;
			
			if(Tools::getIsset('delete_documento')) {
			
				foreach($_POST['selectdocfile'] as $selectedfile) {
				
					if(Tools::getIsset('current_dir')) {
						$entire_dir = Tools::getValue('current_dir');
					}
					else {
						$entire_dir = $directory_base;
					}
					
					if(is_file($entire_dir.'/'.$selectedfile)) {
					
						unlink($entire_dir.'/'.$selectedfile);
					}
					else if(is_dir($entire_dir.'/'.$selectedfile)) {
						$this->deleteDir($entire_dir.'/'.$selectedfile);
					}
				}
			
				echo '<div class="conf confirm">Documenti eliminati con successo</div>';
				
				Tools::redirectAdmin($currentIndex.'&token='.$this->token.'&gotodir='.Tools::getValue('current_dir').'');
			
			}
			
			else if(Tools::getIsset('move_documento') || Tools::getIsset('copy_documento') ) {
			
				foreach($_POST['selectdocfile'] as $selectedfile) {
				
					if(Tools::getIsset('current_dir')) {
						$entire_dir = Tools::getValue('current_dir');
					}
					else {
						$entire_dir = $directory_base;
					}
					
					if(Tools::getIsset('move_documento')) {
					rename($entire_dir.'/'.$selectedfile, Tools::getValue('sposta_in')."/".$selectedfile);
					}
					else if(Tools::getIsset('copy_documento')) {
					copy($entire_dir.'/'.$selectedfile, Tools::getValue('sposta_in')."/".$selectedfile);
					}
				}
			
				echo '<div class="conf confirm">Documenti eliminati con successo</div>';
				
				Tools::redirectAdmin($currentIndex.'&token='.$this->token.'&gotodir='.Tools::getValue('current_dir').'');
			
			}
			
			
			
			if(Tools::getIsset('formdocumentssubmitted')) {
			
				
				if(!empty($_FILES['aggiungiFile'])) {
				
					$docfiles=array();
					$docfdata=$_FILES['aggiungiFile'];
					if(is_array($docfdata['name'])){
						for($i=0;$i<count($docfdata['name']);++$i){
							$docfiles[]=array(
								'name'    =>$docfdata['name'][$i],
								'tmp_name'=>$docfdata['tmp_name'][$i],
								'type' => $docfdata['type'][$i],
								'size' => $docfdata['size'][$i],
								'error' => $docfdata['error'][$i],
							 
							);
						}
					}
					else $docfiles[]=$docfdata;
										
					$attachments = array();
					
					
					foreach($docfiles as $docfile) {
					
						if (isset($docfiles) AND !empty($docfile['name']) AND $docfile['error'] != 0)
							$this->_errors[] = Tools::displayError('An error occurred with the file upload.');
							
							if (!empty($docfile['name'])) {
								rename($docfile['tmp_name'], Tools::getValue('cartella').'/'.$docfile['name']); 
									
							}
													
					}			
					
					echo '<div class="conf confirm">File aggiunti con successo</div>';
				}
				else {
					echo 'Nessun file selezionato';
				}
				
				echo '';
				
			}
			
			echo '<div style="float:left"><h2>Documenti</h2></div>';
			
			
			$filelist = array();
			$dirlist = array();
				
			$directory_base = '../documenti-costruttori';
				
				
				
			if(Tools::getIsset('gotodir')) {
				$entire_dir = Tools::getValue('gotodir');
			}
			else {
				$entire_dir = $directory_base;
			}
			
				
			if(substr($entire_dir,0,12) != '../documenti') {
				echo "ERRORE"; die();
				
			}
				
				
			if ($handle = opendir($entire_dir)) {
				
					
				while ($dir = readdir($handle)) {
					
					if(is_dir($entire_dir.'/'.$dir) && $dir != "." && $dir != "..") { $dirlist[]=$dir; }
					if(is_file($entire_dir.'/'.$dir)) { $filelist[]=$dir; }
						
				}
					
				if(Tools::getIsset('formcartellasubmitted')) {
			
					foreach($dirlist as $entry) {
						if($entry == Tools::getValue('nome_cartella')) {
							$this->_errors[] = Tools::displayError('Esiste gia una cartella con questo nome.');
							break;
						}
					}
						
					$old_umask = umask(0);
					mkdir(Tools::getValue('cartella')."/".Tools::getValue('nome_cartella'),0777);
					umask($old_umask);
					Tools::redirectAdmin($currentIndex.'&token='.$this->token.'&gotodir='.Tools::getValue('cartella').'');
						
						
				}
					
				$cartelle_count = count($dirlist);
				$file_count = count($filelist);
					
				$count_docs = $cartelle_count + $file_count;
					
				echo '<link rel="stylesheet" type="text/css" href="explorer.css" />';
					
				if($count_docs == 0) {
					echo "<div style='float:left; margin-left:20px; margin-top:5px;'>Non sono presenti file n&egrave; sottocartelle in questa cartella.</div><div style='clear:both'></div>";
				} else {
					echo "<div style='float:left; margin-left:20px; margin-top:5px;'>Sono presenti ".$file_count." file e ".$cartelle_count." sottocartelle in questa cartella.</div><div style='clear:both'></div>";
				}
				echo '<form name="submit_delete_move_documento" id="submit_delete_documento" action="'.$currentIndex.'&deleteselection&token='.$this->token.'" method="post" class="std" enctype="multipart/form-data">
					<input type="hidden" name="current_dir" value="'.(Tools::getIsset('gotodir') ? Tools::getValue('gotodir') : $directory_base).'" />
					';
				echo "
				<table class='table' id='explorer'>
				<tr><th style='width:400px'>File</th><th style='width:150px'>Dimensioni</th><th style='width:150px'>Formato</th><th style='width:150px'>Ultima modifica</th><th style='width:50px'>Seleziona</th></tr>";
				
				if($count_docs == 0 && (!Tools::getIsset('gotodir') || Tools::getValue('gotodir') == $directory_base)) {
					echo "<tr><td></td><td></td><td></td><td></td><td></td></td></tr></table>";
				} else {
					
				}
						
				if(Tools::getIsset('gotodir') && Tools::getValue('gotodir') != $directory_base) {
					echo '
						<tr>
						<td class="icon parent" style="padding-left:23px; height:15px;" nowrap="nowrap">
						<a href="'.$currentIndex.'&token='.$this->token.'&gotodir='.(Tools::getIsset('gotodir') ? dirname(Tools::getValue('gotodir')) : $directory_base).'">'.'Livello superiore'."</a>
						</td>
						<td>
							
						</td>
						<td>
						</td>
						<td>
						</td>
						<td style='text-align:center'>
						</td>
						</tr>
						";	
				}
						
				foreach($dirlist as $entry) {
					$pathinfo = pathinfo($entire_dir.'/'.$entry);
					if($entry == '.' || $entry == '..') { }
					else {
						echo '
						<tr>
						<td class="icon folder" style="padding-left:23px; height:15px;" nowrap="nowrap">
						<a href="'.$currentIndex.'&token='.$this->token.'&gotodir='.(Tools::getIsset('gotodir') ? Tools::getValue('gotodir')."/".$entry : $directory_base."/".$entry).'">'.$entry."</a>
						</td>
						<td>
							
						</td>
						<td>
						&laquo;CARTELLA&raquo;
						</td>
						<td>
						".date ("d/m/Y H:i:s",filemtime($entire_dir.'/'.$entry))."
						</td>
						<td style='text-align:center'>
						<input type='checkbox' name='selectdocfile[]' value='".$entry."' />
						</td>
						</tr>
						";
					}						
				}
					
				foreach($filelist as $entry) {
					echo is_file($entry);
					if ($entry != "." && $entry != "..") {
						$pathinfo = pathinfo('../documenti-costruttori/'.(Tools::getIsset('gotodir') ? Tools::getValue('gotodir') : '').'/'.$entry);
						echo '
						<tr>
						<td class="icon '.substr($pathinfo['extension'],0).'" style="padding-left:23px;" nowrap="nowrap">
						<a target="_blank" href="http://ezdocs:WRY753MNR43ASD147g!@www.ezdirect.it/documenti-costruttori/'.(Tools::getIsset('gotodir') ? Tools::getValue('gotodir') : '').'/'.$entry.'">'.$entry."</a>
						</td>
						<td>
						".$this->FileSizeConvert(filesize($entire_dir.'/'.$entry))."
						</td>
						<td>
						".$pathinfo['extension']."
						</td>
						<td>
						".date ("d/m/Y H:i:s",filemtime($entire_dir.'/'.$entry))."
						</td>
						<td style='text-align:center'>
						<input type='checkbox' name='selectdocfile[]' value='".$entry."' />
						</td>
						</tr>
						";
					}
					
				}
				$directories = $this->ListIn($directory_base);
						
				echo "</table><br />
				
				";
				
				if($count_docs == 0) {
					
				} else {
					
					
					echo "
						
					<input type='submit' class='button' name='delete_documento' value='Elimina selezione' onclick=\"javascript: var surec=window.confirm('Sei sicuro? I file, le cartelle e il contenuto delle cartelle che hai selezionato, saranno cancellati in modo permanente!!!'); if (surec) { return true; } else { return false; }\" />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Sposta o copia documenti selezionati in: <select name='sposta_in'>"; 
					if(!Tools::getIsset('gotodir')) {
					}
					else {
						echo "<option value='$directory_base'>Cartella base costruttori</option>";
					}
					foreach($directories as $dir) {
						if($directory_base."/".$dir == Tools::getValue('gotodir')) {
						}
						else {
							echo "<option value='$directory_base/$dir'>$dir</option>";
						}
					}
								
					echo "</select>
					<input type='submit' class='button' value='Sposta' name='move_documento' />
					<input type='submit' class='button' value='Copia' name='copy_documento' />";
				}
				
				echo "</form>";
						
						
						
						
					
				closedir($handle);
			}

			if($entire_dir == '../documenti') {
			}
			else {
				echo '
				<hr />
				<div style="width:300px; display:block; float:left">
				<form name="submit_documento" id="submit_documento" action="'.$currentIndex.'&token='.$this->token.'&formdocumentssubmitted&gotodir='.(Tools::getIsset('gotodir') ? Tools::getValue('gotodir') : $directory_base).'" method="post" class="std" enctype="multipart/form-data">
				<strong>Inserisci nuovo documento</strong><br />
				<div style="display:block;float:left">
				<input name="MAX_FILE_SIZE" value="20000000" type="hidden">
				<input name="cartella" value="'.$entire_dir.'" type="hidden">
							<input name="aggiungiFile[]" id="aggiungiFile" type="file" onchange="handleFileSelect(this.files)" multiple>
							<br />
							<!-- <output id="list-tkt">Anteprime: </output> -->
								<script type="text/javascript">
							function handleFileSelect(files) {
							
								// Loop through the FileList and render image files as thumbnails.
								 for (var i = 0; i < files.length; i++) {
									var f = files[i];
									var name = files[i].name;
			 
									var reader = new FileReader();  
									reader.onload = function(e) {  
									  // Render thumbnail.
									  var span = document.createElement("span");
									  span.innerHTML = ["<a href=\'",e.target.result,"\' target=\'_blank\'>",name,"</a> - "].join("");
									  document.getElementById("list-tkt").insertBefore(span, null);
									};
									  // Read in the image file as a data URL.
								  reader.readAsDataURL(f);
								}
							  }
							 
							</script>
							</div>
				<input type="submit" value="OK" style="display:block;float:left" class="button" />
					
				<div style="clear:both"></div>
				</form>
				</div>	
				<div style="width:400px; display:block; float:left">
				<form name="submit_cartella" id="submit_cartella" action="'.$currentIndex.'&token='.$this->token.'&formcartellasubmitted&gotodir='.(Tools::getIsset('gotodir') ? Tools::getValue('gotodir') : $directory_base).'" method="post" class="std" enctype="multipart/form-data">
				<strong>Crea nuova sottocartella</strong><br />
				<input name="cartella" value="'.$entire_dir.'" type="hidden">
				Inserire nome: <input name="nome_cartella" value="" type="text">
					
				<input type="submit" value="OK"  class="button" />
				</form>
				</div>	
				<div style="clear:both"></div>
							';
				}
		
			
	}
	
	public static function deleteDir($dirPath) {
		if (! is_dir($dirPath)) {
			throw new InvalidArgumentException("$dirPath must be a directory");
		}
		if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
			$dirPath .= '/';
		}
		$files = glob($dirPath . '*', GLOB_MARK);
		foreach ($files as $file) {
			if (is_dir($file)) {
				self::deleteDir($file);
			} else {
				unlink($file);
			}
		}
		rmdir($dirPath);
	}
	
	public static function ListIn($dir, $prefix = '') {
		$dir = rtrim($dir, '\\/');
		$result = array();

		$h = opendir($dir);
		while (($f = readdir($h)) !== false) {
		  if ($f !== '.' and $f !== '..') {
			if (is_dir("$dir/$f")) {
			  $result[] = $prefix.$f;
			  $result = array_merge($result, self::ListIn("$dir/$f", "$prefix$f/"));
			} else {
			//  $result[] = $prefix.$f;
			}
		  }
		}
		closedir($h);

	  return $result;
	}
	
	public function FileSizeConvert($bytes)
			{
				$bytes = floatval($bytes);
					$arBytes = array(
						0 => array(
							"UNIT" => "TB",
							"VALUE" => pow(1024, 4)
						),
						1 => array(
							"UNIT" => "GB",
							"VALUE" => pow(1024, 3)
						),
						2 => array(
							"UNIT" => "MB",
							"VALUE" => pow(1024, 2)
						),
						3 => array(
							"UNIT" => "KB",
							"VALUE" => 1024
						),
						4 => array(
							"UNIT" => "B",
							"VALUE" => 1
						),
					);

				foreach($arBytes as $arItem)
				{
					if($bytes >= $arItem["VALUE"])
					{
						$result = $bytes / $arItem["VALUE"];
						$result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
						break;
					}
				}
				return $result;
			}
		
		
}

