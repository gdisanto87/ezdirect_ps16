<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14632 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class ToolGestioneLink extends AdminTab
{
	
	public function display() 
	{
		global $currentIndex;
		
		if(Tools::getIsset('inserimento-regola'))
		{
			Db::getInstance()->executeS('INSERT INTO gestione_link (id_regola, keyword, url, date_add, date_upd) VALUES (NULL, "'.addslashes(Tools::getValue('keyword')).'",  "'.addslashes(Tools::getValue('url')).'", "'.date("Y-m-d H:i:s").'",  "'.date("Y-m-d H:i:s").'")');
			
			Tools::redirectAdmin($currentIndex.'&token='.($token!=NULL ? $token : $this->token).'&conf=4');
			
		}
		
		if(Tools::getIsset('modifica-regola'))
		{
			Db::getInstance()->executeS('UPDATE gestione_link SET keyword = "'.addslashes(Tools::getValue('keyword')).'", url = "'.addslashes(Tools::getValue('url')).'", date_upd = "'.date("Y-m-d H:i:s").'" WHERE id_regola = "'.Tools::getValue('id_regola').'"');
			
			Tools::redirectAdmin($currentIndex.'&token='.($token!=NULL ? $token : $this->token).'&conf=4');
			
		}
		
		if(Tools::getIsset('deleteregola'))
		{
			Db::getInstance()->executeS('DELETE FROM gestione_link WHERE id_regola = "'.Tools::getValue('deleteregola').'"');
			
			Tools::redirectAdmin($currentIndex.'&token='.($token!=NULL ? $token : $this->token).'&conf=4');
			
		}
		
		if(Tools::getIsset('active'))
		{
			if(Tools::getValue('active') == 1)
				Db::getInstance()->executeS('UPDATE gestione_link SET active = 1 WHERE id_regola = "'.Tools::getValue('id_regola').'"');
			else
				Db::getInstance()->executeS('UPDATE gestione_link SET active = 0 WHERE id_regola = "'.Tools::getValue('id_regola').'"');
			
			Tools::redirectAdmin($currentIndex.'&token='.($token!=NULL ? $token : $this->token).'&conf=4');
			
		}
		
		
		echo '<h1>Tool per gestione link</h1>';
		
		
		echo '<h2 style="display:block; float:left">Lista regole inserite</h2> <a id="regola_link" href="#regola_form" class="button" style="display:block; float:left; margin-left:15px">+ Inserisci nuova regola</a>';
		
		echo '
		<script type="text/javascript" src="../js/jquery/jquery.fancybox-1.3.4.js"></script>
		<link rel="stylesheet" href="../css/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
		<script type="text/javascript">
		
			$(document).ready(function() {
				
				$("a#regola_link").fancybox();
				
			});
		
		
		</script>';
		echo '<div style="clear:both"></div>';		
		$regole = Db::getInstance()->executeS('SELECT * FROM gestione_link');
		
		$cms_numbers = Db::getInstance()->getValue('SELECT count(id_cms) FROM cms_lang WHERE id_lang = 5');
		$every_cms = Db::getInstance()->executeS('SELECT * FROM cms_lang WHERE id_lang = 5');
		$array_cms = array();
		
		$category_numbers = Db::getInstance()->getValue('SELECT count(id_category) FROM category_lang WHERE id_lang = 5');
		$every_category = Db::getInstance()->executeS('SELECT * FROM category_lang WHERE id_lang = 5');
		$array_category = array();
		
		$product_numbers = Db::getInstance()->getValue('SELECT count(id_product) FROM product_lang WHERE id_lang = 5');
		$every_product = Db::getInstance()->executeS('SELECT * FROM product_lang WHERE id_lang = 5');
		$array_product = array();
		
		
		echo '<table class="table">';
		$i = 1;
		foreach($regole as $regola)
		{
			
			echo '<tr>';
			echo '<td style="width:800px"><br />
			<div><strong>Regola '.$i.'</strong></div><div style="display:block;float:right; margin-top:-10px"> Data creazione: '.Tools::displayDate($regola['date_add'],5).'</div>
			<div style="clear:both"></div>
			<div style="width:200px; display:block;float:left;" >Anchor: '.$regola['keyword'].'</div><div style="display:block;float:left;margin-left:30px">URL: <a href="'.$regola['url'].'" target="_blank">'.$regola['url'].'</a></div><div style="clear:both"></div>
			<br />
			';
			$count_cms = 0;
			$pattern = '#(?!<.*?)(?!<a)(\\b'.$regola['keyword'].'\\b)(?!<\/a>)(?![^<>]*?>)#'; 
			$pattern2 = '/<a\s[^>]*href=([\"\']??)([^\" >]*?)\\1[^>]*>'.$regola['keyword'].'<\/a>/siU'; 
			
			//<a id="regola_link" href="#regola_form" 
			
			foreach($every_cms as $cms)
			{
				if(preg_match($pattern, $cms['content']) || preg_match($pattern2, $cms['content']))
					$count_cms++;
			
			}
			
			$count_categories = 0;
			foreach($every_category as $category)
			{
				if(preg_match($pattern, $category['description']) || preg_match($pattern2, $category['description']))
					$count_categories++;
			
			}
			
			$count_products = 0;
			foreach($every_product as $product)
			{
				if(preg_match($pattern, $product['description']) || preg_match($pattern2, $product['description']))
					$count_products++;
			
			}
			
			echo '<script type="text/javascript">
		
			$(document).ready(function() {
				
				$("a#regola_'.$regola['id_regola'].'_prodotti_link").fancybox();
				$("a#regola_'.$regola['id_regola'].'_guide_link").fancybox();
				$("a#regola_'.$regola['id_regola'].'_categorie_link").fancybox();
				$("a#regola_'.$regola['id_regola'].'_form_edit_link").fancybox();
			});
		
		
			</script>';
			
			echo '<div style="width:130px; display:block;float:left;" >Guide: <a id="regola_'.$regola['id_regola'].'_guide_link" href="#regola_'.$regola['id_regola'].'_guide">'.$count_cms.'</a> / '.$cms_numbers.'</div>';
			echo '<div style="width:150px; display:block;float:left;" >Categorie: <a id="regola_'.$regola['id_regola'].'_categorie_link" href="#regola_'.$regola['id_regola'].'_categorie">'.$count_categories.'</a> / '.$category_numbers.'</div>';
			echo '<div style="width:130px; display:block;float:left;" >Prodotti: <a id="regola_'.$regola['id_regola'].'_prodotti_link" href="#regola_'.$regola['id_regola'].'_prodotti">'.$count_products.'</a> / '.$product_numbers.'</div>';
			
			echo '<div style="display:block; float:right"><a id="regola_'.$regola['id_regola'].'_form_edit_link" href="#regola_'.$regola['id_regola'].'_form_edit"><img src="../img/admin/edit.gif" alt="Modifica" title="Modifica" /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.($regola['active'] == 1 ? '<a href="'.$currentIndex.'&active=0&id_regola='.$regola['id_regola'].'&token='.$this->token.'" onclick="var surec = window.confirm(\'Sei sicuro/a?\'); if (surec) {  } else { return false; }"><img src="../img/admin/enabled.gif" alt="Attiva" title="Attiva" /></a>' : '<a href="'.$currentIndex.'&active=1&id_regola='.$regola['id_regola'].'&token='.$this->token.'" onclick="var surec = window.confirm(\'Sei sicuro/a?\'); if (surec) {  } else { return false; }"><img src="../img/admin/disabled.gif" alt="Disattiva" title="Disattiva" /></a>').'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="'.$currentIndex.'&deleteregola='.$regola['id_regola'].'&token='.$this->token.'" onclick="var surec = window.confirm(\'Sei sicuro/a?\'); if (surec) {  } else { return false; }" ><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a>';
			
			echo '<div style="clear:both"></div>';
			
			
			echo '</td></tr>';
			$i++;
		}
		
		echo '</table>';
		
		
		
	
		echo "<br /><br /><br /><br />";
		
		
		
		//FORM REGOLA
		
		echo '<div style="display:none">
			<form id="regola_form" method="post" action="">
			<table><tr>
					<td><label for="keyword">Keyword: </label></td>
					<td><input type="text" id="keyword" name="keyword" size="60" /></td>
				</tr>
				<tr>
					<td><label for="url">URL: </label></td>
					<td><input type="text" id="url" name="url" size="60" value="http://" /></td>
				</tr>
				<tr>
				<td colspan="2">	<input type="submit" class="button" value="Inserisci" name="inserimento-regola" /></td>
				</tr>
				</table>
			</form>
		</div>';
		
		
		
		foreach($regole as $regola)
		{
			//FORM REGOLA EDIT
		
			echo '<div style="display:none">
				<form id="regola_'.$regola['id_regola'].'_form_edit" method="post" action="">
				<input type="hidden" name="id_regola" value="'.$regola['id_regola'].'" />
				<table><tr>
						<td><label for="keyword">Keyword: </label></td>
						<td><input type="text" id="keyword" name="keyword" size="60" value="'.$regola['keyword'].'" /></td>
					</tr>
					<tr>
						<td><label for="url">URL: </label></td>
						<td><input type="text" id="url" name="url" size="60" value="'.$regola['url'].'" /></td>
					</tr>
					<tr>
					<td colspan="2">	<input type="submit" class="button" value="Modifica" name="modifica-regola" /></td>
					</tr>
					</table>
				</form>
			</div>';
		
		
			// GUIDE
			echo '<div style="display:none">';
			echo '<div style="text-align:left; width:500px; height:300px; overflow-y:auto" id="regola_'.$regola['id_regola'].'_guide">';
			$pattern = '#(?!<.*?)(?!<a)(\\b'.$regola['keyword'].'\\b)(?!<\/a>)(?![^<>]*?>)#'; 
			$pattern2 = '/<a\s[^>]*href=([\"\']??)([^\" >]*?)\\1[^>]*>'.$regola['keyword'].'<\/a>/siU'; 
			$i_guide = 1;
			foreach($every_cms as $cms)
			{
				if(preg_match($pattern, $cms['content']))
				{
					echo '<img src="../img/admin/enabled.gif" alt="Attiva" title="Attiva" />&nbsp;&nbsp;&nbsp;'.$i_guide.' <a href="http://www.ezdirect.it/cms.php?id_cms='.$cms['id_cms'].'" target="_blank">'.Db::getInstance()->getValue('SELECT meta_title FROM cms_lang WHERE id_lang = 5 AND id_cms = '.$cms['id_cms'])."</a><br />";
					$i_guide++;
				}
				else if(preg_match($pattern2, $cms['content']))
				{
					echo '<img src="../img/admin/disabled.gif" alt="Attiva" title="Attiva" />&nbsp;&nbsp;&nbsp;'.$i_guide.'<a href="http://www.ezdirect.it/cms.php?id_cms='.$cms['id_cms'].'" target="_blank">'.Db::getInstance()->getValue('SELECT meta_title FROM cms_lang WHERE id_lang = 5 AND id_cms = '.$cms['id_cms'])."</a><br />";
					$i_guide++;
				}
				
			}
			
			echo '</div>';
			echo '</div>';
			// FINE GUIDE
			
			// CATEGORIE
			echo '<div style="display:none">';
			echo '<div style="text-align:left; width:500px; height:300px; overflow-y:auto" id="regola_'.$regola['id_regola'].'_categorie">';
			$pattern = '#(?!<.*?)(?!<a)(\\b'.$regola['keyword'].'\\b)(?!<\/a>)(?![^<>]*?>)#'; 
			$pattern2 = '/<a\s[^>]*href=([\"\']??)([^\" >]*?)\\1[^>]*>'.$regola['keyword'].'<\/a>/siU'; 
			$i_categorie = 1;
			foreach($every_category as $category)
			{
				if(preg_match($pattern, $category['description']))
				{
					echo '<img src="../img/admin/enabled.gif" alt="Attiva" title="Attiva" />&nbsp;&nbsp;&nbsp;'.$i_categorie.' <a href="http://www.ezdirect.it/category.php?id_category='.$category['id_category'].'" target="_blank">'.Db::getInstance()->getValue('SELECT name FROM category_lang WHERE id_lang = 5 AND id_category = '.$category['id_category'])."</a><br />";
					$i_categorie++;
				}
				else if(preg_match($pattern2, $category['description']))
				{
					echo '<img src="../img/admin/disabled.gif" alt="Attiva" title="Attiva" />&nbsp;&nbsp;&nbsp;'.$i_categorie.'<a href="http://www.ezdirect.it/category.php?id_category='.$category['id_category'].'" target="_blank">'.Db::getInstance()->getValue('SELECT name FROM category_lang WHERE id_lang = 5 AND id_category = '.$category['id_category'])."</a><br />";
					$i_categorie++;
				}
				
			}
			
			echo '</div>';
			echo '</div>';
			// FINE CATEGORIE
			
			// PRODOTTI
			echo '<div style="display:none">';
			echo '<div style="text-align:left; width:500px; height:300px; overflow-y:auto" id="regola_'.$regola['id_regola'].'_prodotti">';
			$pattern = '#(?!<.*?)(?!<a)(\\b'.$regola['keyword'].'\\b)(?!<\/a>)(?![^<>]*?>)#'; 
			$pattern2 = '/<a\s[^>]*href=([\"\']??)([^\" >]*?)\\1[^>]*>'.$regola['keyword'].'<\/a>/siU'; 
			$i_prodotti = 1;
			foreach($every_product as $product)
			{
				if(preg_match($pattern, $product['description']))
				{
					echo '<img src="../img/admin/enabled.gif" alt="Attiva" title="Attiva" />&nbsp;&nbsp;&nbsp;'.$i_prodotti.' <a href="http://www.ezdirect.it/product.php?id_product='.$product['id_product'].'" target="_blank">'.Db::getInstance()->getValue('SELECT name FROM product_lang WHERE id_lang = 5 AND id_product = '.$product['id_product'])."</a><br />";
					$i_prodotti++;
				}
				else if(preg_match($pattern2, $product['description']))
				{
					echo '<img src="../img/admin/disabled.gif" alt="Attiva" title="Attiva" />&nbsp;&nbsp;&nbsp;'.$i_prodotti.'<a href="http://www.ezdirect.it/product.php?id_product='.$product['id_product'].'" target="_blank">'.Db::getInstance()->getValue('SELECT name FROM product_lang WHERE id_lang = 5 AND id_product = '.$product['id_product'])."</a><br />";
					$i_prodotti++;
				}
				
			}
			
			echo '</div>';
			echo '</div>';
			// FINE PRODOTTI
			
			
		}
	
	
	}
}

