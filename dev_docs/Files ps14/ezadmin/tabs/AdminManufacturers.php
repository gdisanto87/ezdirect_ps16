<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14731 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminManufacturers extends AdminTab
{
	protected $maxImageSize = 200000;

	/** @var array countries list */
	private $countriesArray = array();

	public function __construct()
	{
		global $cookie;

		$this->table = 'manufacturer';
		$this->className = 'Manufacturer';
		$this->lang = false;
		$this->edit = true;
	 	$this->delete = true;

		// Sub tab addresses
		$countries = Country::getCountries((int)($cookie->id_lang));
		foreach ($countries AS $country)
			$this->countriesArray[$country['id_country']] = $country['name'];
		
		$this->fieldsDisplayAddresses = array(
		'id_address' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 25),
		'm!manufacturer_name' => array('title' => $this->l('Manufacturer'), 'width' => 100),
		'firstname' => array('title' => $this->l('First name'), 'width' => 80),
		'lastname' => array('title' => $this->l('Last name'), 'width' => 100, 'filter_key' => 'a!lastname'),
		'postcode' => array('title' => $this->l('Postcode/ Zip Code'), 'align' => 'right', 'width' => 50),
		'city' => array('title' => $this->l('City'), 'width' => 150),
		'country' => array('title' => $this->l('Country'), 'width' => 100, 'type' => 'select', 'select' => $this->countriesArray, 'filter_key' => 'cl!id_country'));
		$this->_includeTabTitle = array($this->l('Manufacturers addresses'));
		$this->_joinAddresses = 'LEFT JOIN `'._DB_PREFIX_.'country_lang` cl ON 
		(cl.`id_country` = a.`id_country` AND cl.`id_lang` = '.(int)($cookie->id_lang).') ';
	 	$this->_joinAddresses .= 'LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (a.`id_manufacturer` = m.`id_manufacturer`)';
		$this->_selectAddresses = 'cl.`name` as country, m.`name` AS manufacturer_name';
		$this->_includeTab = array('Addresses' => array('addressType' => 'manufacturer', 'fieldsDisplay' => $this->fieldsDisplayAddresses, '_join' => $this->_joinAddresses, '_select' => $this->_selectAddresses));
		$this->view = true;
		$this->_select = 'COUNT(`id_product`) AS `products`, (SELECT COUNT(ad.`id_manufacturer`) as `addresses` FROM `'._DB_PREFIX_.'address` ad WHERE ad.`id_manufacturer` = a.`id_manufacturer` AND ad.`deleted` = 0 GROUP BY ad.`id_manufacturer`) as `addresses`';
		$this->_join = 'LEFT JOIN `'._DB_PREFIX_.'product` p ON (a.`id_manufacturer` = p.`id_manufacturer`)';
		$this->_joinCount = false;
		$this->_group = 'GROUP BY a.`id_manufacturer`';

		$this->fieldImageSettings = array('name' => 'logo', 'dir' => 'm');

		$this->fieldsDisplay = array(
			'id_manufacturer' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 25),
			'name' => array('title' => $this->l('Name'), 'width' => 200),
			'logo' => array('title' => $this->l('Logo'), 'align' => 'center', 'image' => 'm', 'orderby' => false, 'search' => false),
			'addresses' => array('title' => $this->l('Addresses'), 'align' => 'right', 'tmpTableFilter' => true, 'width' => 20),
			'products' => array('title' => $this->l('Products'), 'align' => 'right', 'tmpTableFilter' => true, 'width' => 20),
			'active' => array('title' => $this->l('Enabled'), 'width' => 25, 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false)
		);

		parent::__construct();
		
		if (Tools::isSubmit('submitAddmanufacturer'))
		{
			if(!Tools::getValue('id_manufacturer'))
			{
				mail('federico.giannini@mail.com', 'Nuovo costruttore su Ezdirect', 'Aggiunto nuovo costruttore su Ezdirect');
			}
			
			Db::getInstance()->executeS("UPDATE manufacturer SET supplier = '".Tools::getValue('supplier')."' WHERE id_manufacturer = ".Tools::getValue('id_manufacturer'));
			
			Db::getInstance()->executeS("UPDATE product SET id_supplier = '".Tools::getValue('supplier')."' WHERE id_manufacturer = ".Tools::getValue('id_manufacturer'));
			
			Db::getInstance()->executeS("UPDATE product SET margin_login_for_offer = '".Tools::getValue('margin_login_for_offer')."' WHERE id_manufacturer = ".Tools::getValue('id_manufacturer'));
			
			if(Tools::getIsset('abilita_banda'))
			{
				Db::getInstance()->executeS("UPDATE manufacturer SET abilita_banda = 1 WHERE id_manufacturer = ".Tools::getValue('id_manufacturer'));
				
				Db::getInstance()->executeS("UPDATE specific_price SET abilita_banda = 1 WHERE specific_price_name = '' AND id_product IN (SELECT id_product FROM product WHERE id_manufacturer = ".Tools::getValue('id_manufacturer').')');
			}
			else
			{
				Db::getInstance()->executeS("UPDATE manufacturer SET abilita_banda = 0 WHERE id_manufacturer = ".Tools::getValue('id_manufacturer'));
				
				Db::getInstance()->executeS("UPDATE specific_price SET abilita_banda = 0 WHERE specific_price_name = '' AND id_product IN (SELECT id_product FROM product WHERE id_manufacturer = ".Tools::getValue('id_manufacturer').')');
			}
			Db::getInstance()->executeS("UPDATE product SET amazon_fr = '".str_replace(',','.',Tools::getValue('amazon_fr'))."', amazon_it = '".str_replace(',','.',Tools::getValue('amazon_it'))."', amazon_de = '".str_replace(',','.',Tools::getValue('amazon_de'))."', amazon_es = '".str_replace(',','.',Tools::getValue('amazon_es'))."', amazon_nl = '".str_replace(',','.',Tools::getValue('amazon_nl'))."',  amazon_uk = '".str_replace(',','.',Tools::getValue('amazon_uk'))."' WHERE id_manufacturer = ".Tools::getValue('id_manufacturer'));
			
			Db::getInstance()->executes("UPDATE manufacturer_lang SET seo_keyword = '".$_POST['parola_chiave']."' WHERE id_lang = 5 AND id_manufacturer = ".Tools::getValue('id_manufacturer')."");
				
			$suppliers = array();
			foreach ($_POST['other_suppliers'] as $supplier) {
				$suppliers[] = $supplier;

			}
			$suppliers = serialize($suppliers);
				
			Db::getInstance()->executeS("UPDATE manufacturer SET other_suppliers = '".$suppliers."' WHERE id_manufacturer = ".Tools::getValue('id_manufacturer'));
			Db::getInstance()->executeS("UPDATE product SET other_suppliers = '".$suppliers."' WHERE id_manufacturer = ".Tools::getValue('id_manufacturer'));
		
							
		}	

		if (Tools::isSubmit('deletemanufacturer'))
		{		
			mail('federico.giannini@mail.com', 'Cancellato costruttore su Ezdirect', 'Cancellato costruttore n. '.Tools::getValue('id_manufacturer').' ('.Db::getInstance()->getValue('SELECT name FROM manufacturer WHERE id_manufacturer = '.Tools::getValue('id_manufacturer')).') su Ezdirect');
		}
	}

	public function afterImageUpload()
	{
		/* Generate image with differents size */
		if (($id_manufacturer = (int)(Tools::getValue('id_manufacturer'))) AND isset($_FILES) AND count($_FILES) AND file_exists(_PS_MANU_IMG_DIR_.$id_manufacturer.'.jpg'))
		{
			$imagesTypes = ImageType::getImagesTypes('manufacturers');
			foreach ($imagesTypes AS $k => $imageType)
				imageResize(_PS_MANU_IMG_DIR_.$id_manufacturer.'.jpg', _PS_MANU_IMG_DIR_.$id_manufacturer.'-'.stripslashes($imageType['name']).'.jpg', (int)($imageType['width']), (int)($imageType['height']));
		}
	}

	public function displayForm($isMainTab = true)
	{
		global $currentIndex, $cookie;
		parent::displayForm();
		
		if (!($manufacturer = $this->loadObject(true)))
			return;
		$langtags = 'cdesc2¤cdesc¤mmeta_title¤mmeta_keywords¤mmeta_description';

		echo '
		<form action="'.$currentIndex.'&submitAdd'.$this->table.'=1&token='.$this->token.'" method="post" enctype="multipart/form-data">
		'.($manufacturer->id ? '<input type="hidden" name="id_'.$this->table.'" value="'.$manufacturer->id.'" />' : '').'
			<fieldset style="width: 905px;">
				<legend><img src="../img/admin/manufacturers.gif" />'.$this->l('Manufacturers').'</legend>
				<label>'.$this->l('Name').'</label>
				<div class="margin-form">
					<input type="text" size="40" name="name" value="'.htmlentities(Tools::getValue('name', $manufacturer->name), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup>
					<span class="hint" name="help_box">'.$this->l('Invalid characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
				</div>
				<label>'.$this->l('Sito web').'</label>
				<div class="margin-form">
					<input type="text" size="40" name="website" value="'.htmlentities(Tools::getValue('website', $manufacturer->website), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup>
					<span class="hint" name="help_box">'.$this->l('Invalid characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
				</div>
				';
				
				echo '
				<label>'.$this->l('Meta title').'</label>
				<div class="margin-form">';
		foreach ($this->_languages as $language)
			echo '
					<div id="mmeta_title_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input type="text" onkeyup="attivaSEO(document.getElementById(\'description_5\').value);" name="meta_title_'.$language['id_lang'].'" style="width:700px" id="meta_title_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($manufacturer, 'meta_title', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" />
						<span class="hint" name="help_box">'.$this->l('Forbidden characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
					</div>';
		$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $langtags, 'mmeta_title');
		echo '		<div class="clear"></div>
				</div>
				<label>'.$this->l('Meta description').'</label>
				<div class="margin-form">';
		foreach ($this->_languages as $language)
			echo '<div id="mmeta_description_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input type="text"  onkeyup="attivaSEO(document.getElementById(\'description_5\').value);" name="meta_description_'.$language['id_lang'].'" style="width:700px" id="meta_description_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($manufacturer, 'meta_description', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" />
						<span class="hint" name="help_box">'.$this->l('Forbidden characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
				</div>';
		$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $langtags, 'mmeta_description');
		echo '		<div class="clear"></div>
				</div>
				<label>'.$this->l('Meta keywords').'</label>
				<div class="margin-form">';
		foreach ($this->_languages as $language)
			echo '
					<div id="mmeta_keywords_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input type="text" name="meta_keywords_'.$language['id_lang'].'" style="width:700px" id="meta_keywords_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($manufacturer, 'meta_keywords', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" />
						<span class="hint" name="help_box">'.$this->l('Forbidden characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
					</div>';
		$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $langtags, 'mmeta_keywords');
		echo '		<div class="clear"></div>
				</div>';

		echo '<br class="clear" /><label>'.$this->l('Short description').'</label>
				<div class="margin-form">';
		foreach ($this->_languages as $language)
			echo '
							<div id="cdesc2_'.$language['id_lang'].'" style="float: left;'.($language['id_lang'] != $this->_defaultFormLanguage ? 'display:none;' : '').'">
								<textarea class="rte" cols="48" rows="5" id="short_description_'.$language['id_lang'].'" name="short_description_'.$language['id_lang'].'">'.htmlentities(stripslashes($this->getFieldValue($manufacturer, 'short_description', $language['id_lang'])), ENT_COMPAT, 'UTF-8').'</textarea>
							</div>';
		$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $langtags, 'cdesc2');
		echo '</div>';
				
		echo '<br class="clear" /><br /><br /><label>'.$this->l('Description').'</label>
				<div class="margin-form">';
		foreach ($this->_languages as $language)
			echo '
							<div id="cdesc_'.$language['id_lang'].'" style="float: left;'.($language['id_lang'] != $this->_defaultFormLanguage ? 'display:none;' : '').'">
								<textarea class="rte" cols="48" rows="10" id="description_'.$language['id_lang'].'" name="description_'.$language['id_lang'].'">'.htmlentities(stripslashes($this->getFieldValue($manufacturer, 'description', $language['id_lang'])), ENT_COMPAT, 'UTF-8').'</textarea>
							</div>';
		$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $langtags, 'cdesc');
		echo '</div>';
		
		// TinyMCE
		global $cookie;
		$iso = Language::getIsoById((int)($cookie->id_lang));
		$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
		$ad = dirname($_SERVER["PHP_SELF"]);
		echo '
			<script type="text/javascript">	
			var iso = \''.$isoTinyMCE.'\' ;
			var pathCSS = \''._THEME_CSS_DIR_.'\' ;
			var ad = \''.$ad.'\' ;
			</script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce3/tiny_mce.js"></script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc-4.js"></script>';
			
			echo '<label>SEO (solo per italiano)
					<br />
					Parola chiave:
					<input id="parola_chiave" name="parola_chiave" type="text" onkeyup="attivaSEO(document.getElementById(\'description_5\').value)" value="'.Db::getInstance()->getValue('SELECT seo_keyword FROM manufacturer_lang WHERE id_lang = 5 AND id_manufacturer = '.Tools::getValue('id_manufacturer')).'" /><br /><br />
				
					</label>';
					
					echo '<div style="padding: 0 0 1em 210px;">';
					echo "
					<script type='text/javascript'>
					$(document).ready(function(){
						$('#toggleseo').click(function() {
							$('#seoby').slideToggle('fast', function() {
								if ($('#seoby').is(':hidden')) {
									var hidden_seo = 1;
								} else {
									var hidden_seo = 0;
								}
								
								$.ajax({
									type: 'POST',
									async: true,
									url: 'ajax.php?nascondiMostraSeo',
									data: {hidden_seo: hidden_seo, nascondiMostraSeo: 'y'},
									success: function(data) {
									},
									error: function(xhr,stato,errori){
									}
								});
							});
						});
					});
					</script>";
					
					$hidden_seo = (Db::getInstance()->getValue('SELECT value FROM fixed_ck WHERE id_employee = '.$cookie->id_employee.' AND name="hidden_seo"'));
					
					echo '
						<a href="javascript:void(0)" id="toggleseo">Clicca per aprire/chiudere SEO</a><br /><br />
					<div id="seoby" style="'.($hidden_seo == 0 ? "display:block" : 'display:none').'">
					</div>
					<script type="text/javascript">
						
						function attivaSEO(testo) 
						{
							var keyword = document.getElementById("parola_chiave").value;
							var title = document.getElementById("meta_title_5").value;
							var meta_description = document.getElementById("meta_description_5").value;
							var url = "";
							var urlified_keyword = str2url(document.getElementById("parola_chiave").value, "UTF-8");
							
							$.ajax({
							  url:"ajax.php?SEOby=y",
							  type: "POST",
							  data: { text: testo,
							  keyword: keyword,
							  title: title,
							  meta_description: meta_description,
							  url: url,
							  urlified_keyword: urlified_keyword,
							  tipo: "produttore"
							  },
							  success:function(r){
									console.log("SEO SUCCESS");
									$("#seoby").html(r);
							  },
							  error: function(xhr,stato,errori){
								 console.log("Errore nella cancellazione:"+xhr.status);
							  }
							});
						}
						
						var temp = document.getElementById("description_5").value;
						
						attivaSEO(temp);
						
					 </script>
					 
					 </div>';
			
			
			
			
			
			$abilita_banda = Db::getInstance()->getValue('SELECT abilita_banda FROM manufacturer WHERE id_manufacturer = '.$manufacturer->id);
			
		echo '<br style="clear:both;" /><br/><br/><label>'.$this->l('Logo').'</label>
				<div class="margin-form">';
					$this->displayImage($manufacturer->id, _PS_MANU_IMG_DIR_.$manufacturer->id.'.jpg', 350, NULL, NULL, true);
		echo '	<br /><input type="file" name="logo" />
					<p>'.$this->l('Upload manufacturer logo from your computer').'</p>
				</div>';
				
				
				echo '
				<label>'.$this->l('Enable:').' </label>
				<div class="margin-form">
					<input type="radio" name="active" id="active_on" value="1" '.($this->getFieldValue($manufacturer, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_on"> <img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="active" id="active_off" value="0" '.(!$this->getFieldValue($manufacturer, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_off"> <img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label>
				</div>
				
				<label>'.$this->l('Abilita banda offerta speciale').' </label>
				<div class="margin-form">
					<input type="checkbox" name="abilita_banda" '.($abilita_banda == 1 ? 'checked="checked"' : '').'  /> <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" title="Abilita una banda con scritto \'Offerta speciale\' in listing e scheda prodotto in tutti i prezzi speciali di questo produttore" />
				</div>
				
				
				<label>'.$this->l('Margine minimo CLIENTI').' </label>
				<div class="margin-form">
					<input type="text" name="margin_min_cli" style="width:30px" id="margin_min_cli" value="'.str_replace('.',',',$this->getFieldValue($manufacturer, 'margin_min_cli')).'" />
				</div>
				
				<label>'.$this->l('Margine minimo RIVENDITORI').' </label>
				<div class="margin-form">
					<input type="text" name="margin_min_riv" style="width:30px" id="margin_min_riv" value="'.str_replace('.',',',$this->getFieldValue($manufacturer, 'margin_min_riv')).'" />
				</div>
				
				<label>'.$this->l('Margine LOGIN FOR OFFER').' </label>
				<div class="margin-form">
					<input type="text" name="margin_login_for_offer" style="width:30px" id="margin_login_for_offer" value="'.str_replace('.',',',$this->getFieldValue($manufacturer, 'margin_login_for_offer')).'" />
				</div>
				
					<label>'.$this->l('Sconto Amazon FR').' </label>
				<div class="margin-form">
					<input type="text" name="amazon_fr" style="width:30px" id="amazon_fr" value="'.str_replace('.',',',$this->getFieldValue($manufacturer, 'amazon_fr')).'" />
				</div>
				
				
				
				<label>'.$this->l('Sconto Amazon DE').' </label>
				<div class="margin-form">
					<input type="text" name="amazon_de" style="width:30px" id="amazon_de" value="'.str_replace('.',',',$this->getFieldValue($manufacturer, 'amazon_de')).'" />
				</div>
				
				<label>'.$this->l('Sconto Amazon NL').' </label>
				<div class="margin-form">
					<input type="text" name="amazon_nl" style="width:30px" id="amazon_nl" value="'.str_replace('.',',',$this->getFieldValue($manufacturer, 'amazon_nl')).'" />
				</div>

<label>'.$this->l('Sconto Amazon ES').' </label>
				<div class="margin-form">
					<input type="text" name="amazon_es" style="width:30px" id="amazon_es" value="'.str_replace('.',',',$this->getFieldValue($manufacturer, 'amazon_es')).'" />
				</div>

<label>'.$this->l('Sconto Amazon IT').' </label>
				<div class="margin-form">
					<input type="text" name="amazon_it" style="width:30px" id="amazon_it" value="'.str_replace('.',',',$this->getFieldValue($manufacturer, 'amazon_it')).'" />
				</div>
				
				<label>'.$this->l('Sconto Amazon UK').' </label>
				<div class="margin-form">
					<input type="text" name="amazon_uk" style="width:30px" id="amazon_uk" value="'.str_replace('.',',',$this->getFieldValue($manufacturer, 'amazon_uk')).'" />
				</div>
				
				
				';
				
				$resultssup = Db::getInstance()->ExecuteS("SELECT id_supplier, name FROM supplier ORDER BY name");
				
				$id_supplier = Db::getInstance()->getValue('SELECT supplier FROM manufacturer WHERE id_manufacturer = '.$manufacturer->id);
							
				echo '<label>'.$this->l('Fornitore principale:').'</label>
						<div class="margin-form">
							<select name="supplier" id="id_supplier"><option name="0" value="0">-- Scegli (opzionale) --</option>';
							
							foreach ($resultssup as $rowsup) {
							
								echo "<option name='$rowsup[id_supplier]' ".($rowsup['id_supplier'] == $id_supplier ? 'selected="selected"' : '')." value='$rowsup[id_supplier]'"; 
								echo ">$rowsup[name]</option>";
							
							}
							
							echo '
							</select>&nbsp;&nbsp;&nbsp;<a href="?tab=AdminSuppliers&addsupplier&token='.Tools::getAdminToken('AdminSuppliers'.(int)(Tab::getIdFromClassName('AdminSuppliers')).(int)($cookie->id_employee)).'" target="_blank"><img src="../img/admin/add.gif" alt="'.$this->l('Crea nuovo fornitore').'" title="'.$this->l('Crea nuovo fornitore').'" /> <b>'.$this->l('Crea nuovo fornitore').'</b></a>
						</div>
					';
					
					echo '<label>'.$this->l('Altri fornitori:').'</label>
						<div class="margin-form">
						<script type="text/javascript">
						function add_riga() {
				
							$("<div style=\'position:relative; width:145px; float:left\'><select name=\'other_suppliers[]\' style=\'width:145px\'><option name=\'0\' value=\'0\'>-- Scegli (opzionale) --</option>';
							
							foreach ($resultssup as $rowsup) {
				
								echo "<option name='$rowsup[id_supplier]' value='$rowsup[id_supplier]'"; 
								echo ">$rowsup[name]</option>";
							
							}
							
							echo '</select></div>").appendTo("#selezione-fornitori");

						}
						
						function rimuovi_riga() {
							$("#selezione-fornitori").children().last().remove();
						}
			
						</script>
						<div id="selezione-fornitori" style="position:relative; width:600px">';
						
						$other_suppliers = Db::getInstance()->getValue('SELECT other_suppliers FROM manufacturer WHERE id_manufacturer = '. $manufacturer->id.'');
					
						$other_suppliers = unserialize($other_suppliers);
						
						foreach ($other_suppliers as $supplier)
						{
						
							echo '
							<div style="position:relative; width:145px; float:left"><select name="other_suppliers[]" style="width:145px">
							<option name="0" value="0">-- Scegli (opzionale) --</option>
							';
							
							
							
							foreach ($resultssup as $rowsup) {
							
								echo "<option name='$rowsup[id_supplier]' ".($rowsup['id_supplier'] == $supplier ? "selected='selected'" : '')." value='$rowsup[id_supplier]'"; 
								echo ">$rowsup[name]</option>";
							
							}
				
					
							echo '</select>';
							echo '</div>';
				
						}
				echo '</div><div style="clear:both"></div>
				
				
				<a href="javascript:void(0)" onclick="javascript:add_riga(); return false;"><img src="../img/admin/add.gif" alt="'.$this->l('Add').'" title="'.$this->l('Add').'" /> <b>'.$this->l('Aggiungi forn.').'</b></a><br />
				<a href="javascript:void(0)" onclick="javascript:rimuovi_riga(); return false;"><img src="../img/admin/forbbiden.gif" alt="'.$this->l('Remove').'" title="'.$this->l('Remove').'" /> <b>'.$this->l('Rimuovi forn.').'</b></a>
				<input type="hidden" name="other_suppliers_2" id="other_suppliers_2" /><!-- serve per ordinamento --><br /><br />
				
				</div>';
				
				echo '
				<div class="margin-form">
					<input type="submit" value="'.$this->l('   Save   ').'" name="submitAdd'.$this->table.'" class="button" />
				</div>
				<div class="small"><sup>*</sup> '.$this->l('Required field').'</div>
			</fieldset>
		</form>';
	}

	public function viewmanufacturer()
	{
		global $cookie;
		if (!($manufacturer = $this->loadObject()))
			return;
		echo '<h2>'.$manufacturer->name.'</h2>';

		$products = $manufacturer->getProductsLite((int)($cookie->id_lang));
		$addresses = $manufacturer->getAddresses((int)($cookie->id_lang));
		
		echo '<h3>'.$this->l('Total addresses:').' '.sizeof($addresses).'</h3>';
		echo '<hr />';
		foreach ($addresses AS $addresse)
			echo '
				<h3></h3>
				<table border="0" cellpadding="0" cellspacing="0" class="table" style="width: 600px;">
					<tr>
						<th><b>'.$addresse['firstname'].' '.$addresse['lastname'].'</b></th>
					</tr>
					<tr>
						<td>
							<div style="padding:5px; float:left; width:350px;">
								'.$addresse['address1'].'<br />
								'.($addresse['address2'] ? $addresse['address2'].'<br />' : '').'
								'.$addresse['postcode'].' '.$addresse['city'].'<br />
								'.($addresse['state'] ? $addresse['state'].'<br />' : '').'
								<b>'.$addresse['country'].'</b><br />
								</div>
							<div style="padding:5px; float:left;">
								'.($addresse['phone'] ? $addresse['phone'].'<br />' : '').'
								'.($addresse['phone_mobile'] ? $addresse['phone_mobile'].'<br />' : '').'
							</div>
							'.($addresse['other'] ? '<div style="padding:5px; clear:both;"><br /><i>'.$addresse['other'].'</i></div>' : '').'
						</td>
					</tr>
				</table>';
		if (!sizeof($addresses))
			echo 'No address for this manufacturer.';
		echo '<br /><br />';
		echo '<h3>'.$this->l('Total products:').' '.sizeof($products).'</h3>';
		foreach ($products AS $product)
		{
			$product = new Product($product['id_product'], false, (int)($cookie->id_lang));
			echo '<hr />';
			if (!$product->hasAttributes())
			{
				echo '
				<div style="float:right;">
					<a href="?tab=AdminCatalog&id_product='.$product->id.'&updateproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'" class="button">'.$this->l('Edit').'</a>
					<a href="?tab=AdminCatalog&id_product='.$product->id.'&deleteproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'" class="button" onclick="return confirm(\''.$this->l('Delete item #', __CLASS__, TRUE).$product->id.' ?\');">'.$this->l('Delete').'</a>
				</div>
				<table border="0" cellpadding="0" cellspacing="0" class="table width3">
					<tr>
						<th>'.$product->name.' <img src="../img/admin/'.($product->active ? 'enabled' : 'disabled').'.gif" /></th>
						'.(!empty($product->reference) ? '<th width="150">'.$this->l('Ref:').' '.$product->reference.'</th>' : '').'
						'.(!empty($product->ean13) ? '<th width="120">'.$this->l('EAN13:').' '.$product->ean13.'</th>' : '').'
						'.(!empty($product->upc) ? '<th width="120">'.$this->l('UPC:').' '.$product->upc.'</th>' : '').'
						'.(Configuration::get('PS_STOCK_MANAGEMENT') ? '<th class="right" width="50">'.$this->l('Qty:').' '.$product->quantity.'</th>' : '').'
					</tr>
				</table>';
			}
			else
			{
				echo '
				<div style="float:right;">
					<a href="?tab=AdminCatalog&id_product='.$product->id.'&updateproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'" class="button">'.$this->l('Edit').'</a>
					<a href="?tab=AdminCatalog&id_product='.$product->id.'&deleteproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'" class="button" onclick="return confirm(\''.$this->l('Delete item #', __CLASS__, TRUE).$product->id.' ?\');">'.$this->l('Delete').'</a>
				</div>
				<h3><a href="?tab=AdminCatalog&id_product='.$product->id.'&updateproduct&token='.Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee)).'">'.$product->name.'</a> <img src="../img/admin/'.($product->active ? 'enabled' : 'disabled').'.gif" /></h3>
				<table border="0" cellpadding="0" cellspacing="0" class="table" style="width:600px">
					<tr>
	                    <th>'.$this->l('Attribute name').'</th>
	                    <th width="80">'.$this->l('Reference').'</th>
	                    <th width="80">'.$this->l('EAN13').'</th>
						<th width="80">'.$this->l('UPC').'</th>
	                   '.(Configuration::get('PS_STOCK_MANAGEMENT') ? '<th class="right" width="40">'.$this->l('Quantity').'</th>' : '').'
                	</tr>';
			     	/* Build attributes combinaisons */
				$combinaisons = $product->getAttributeCombinaisons((int)($cookie->id_lang));
				foreach ($combinaisons AS $k => $combinaison)
				{
					$combArray[$combinaison['id_product_attribute']]['reference'] = $combinaison['reference'];
					$combArray[$combinaison['id_product_attribute']]['ean13'] = $combinaison['ean13'];
					$combArray[$combinaison['id_product_attribute']]['upc'] = $combinaison['upc'];
					$combArray[$combinaison['id_product_attribute']]['quantity'] = $combinaison['quantity'];
					$combArray[$combinaison['id_product_attribute']]['attributes'][] = array($combinaison['group_name'], $combinaison['attribute_name'], $combinaison['id_attribute']);
				}
				$irow = 0;
				foreach ($combArray AS $id_product_attribute => $product_attribute)
				{
					$list = '';
					foreach ($product_attribute['attributes'] AS $attribute)
						$list .= $attribute[0].' - '.$attribute[1].', ';
					$list = rtrim($list, ', ');
					echo '
					<tr'.($irow++ % 2 ? ' class="alt_row"' : '').' >
						<td>'.stripslashes($list).'</td>
						<td>'.(!empty($product_attribute['reference']) ? $product_attribute['reference'] : '-').'</td>
						<td>'.(!empty($product_attribute['ean13']) ? $product_attribute['ean13'] : '-').'</td>
						<td>'.(!empty($product_attribute['upc']) ? $product_attribute['upc'] : '-').'</td>
						'.(Configuration::get('PS_STOCK_MANAGEMENT') ? '<td class="right">'.$product_attribute['quantity'].'</td>' : '').'
					</tr>';
				}
				unset($combArray);
				echo '</table>';
			}
		}
	}
}
