<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14002 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/



define('_PS_ADMIN_DIR_', getcwd());
define('PS_ADMIN_DIR', _PS_ADMIN_DIR_); // Retro-compatibility

include(PS_ADMIN_DIR.'/../config/config.inc.php');
/* Getting cookie or logout */
require_once(dirname(__FILE__).'/init.php');

global $cookie;

if(isset($_POST['ordini_non_processati'])) {

	if($_POST['aggiungi'] == 'yes')
	{
		Db::getInstance()->executeS("INSERT INTO order_processati (id_order) VALUES (".$_POST['id'].")");

	}
	else if($_POST['aggiungi'] == 'no')
	{
		Db::getInstance()->executeS("DELETE FROM order_processati WHERE id_order = ".$_POST['id']."");
	}

}

if(isset($_POST['edit_bundle_sort_order'])) {

	Db::getInstance()->executeS("UPDATE bundle SET sort_order = ".$_POST['sort_order']." WHERE id_bundle = ".$_POST['id_bundle']."");
		
}

if(isset($_GET['calcola_totale_con_iva'])) {
	
		$tax_regime = Db::getInstance()->getValue("SELECT tax_regime FROM customer WHERE id_customer = ".$_POST['customer']."");
		$is_company = Db::getInstance()->getValue("SELECT is_company FROM customer WHERE id_customer = ".$_POST['customer'].""); 
		
		$totale = $_POST['totalep'];
		if($tax_regime == 0) {
			$id_tax = 1;
		}
		else {
			$id_tax = $tax_regime;
		}
		$tax_rate = Db::getInstance()->getValue("SELECT rate FROM tax WHERE id_tax = ".$id_tax."");
			
		if($tax_regime == 1) {
			$tax_rate = 0;
		}
		
		if($is_company == 0)
			$id_country = 10;
		
		$id_country = Db::getInstance()->getValue("SELECT id_country FROM address WHERE id_address = ".$_POST['address']."");
		
		$carrellon = new Cart($_POST['id_cart']);
		
		$current_products = str_replace("impImporto[", ";", $_POST['current_prices']);
		$current_products = str_replace("]","",$current_products);
		$current_products = ltrim($current_products, ";");
		
		$cproducts = explode(";",$current_products);
		
		$iva = 0;
		
		if($id_country == 10) {
			
			
			//$iva = $carrellon->getOrderTotal() - $carrellon->getOrderTotal(false);
			foreach($cproducts as $p)
			{
				$p1 = explode('_',$p);
				$rp = Db::getInstance()->getValue('SELECT id_tax_rules_group FROM product WHERE id_product = '.$p1[0]);
				if($rp == 10)
					$tax_r = 0;
				else
					$tax_r = Db::getInstance()->getValue("SELECT rate FROM tax WHERE id_tax = 1");
				
				$iva = $iva + (($p1[1] * $tax_r)/100);
				
			}
			
			
		}
			
		else {
			/*
			if($is_company == 1  || $is_company == 0 && $id_country == 19)
				$iva = 0;
			else
				$iva = $carrellon->getOrderTotal() - $carrellon->getOrderTotal(false);
			*/
			if($is_company == 1  || $is_company == 0 && $id_country == 19)
				$iva = 0;
			else
				$iva = ($importo * $tax_rate)/100;
		}
			
		if($tax_regime == 1) {

			$iva = 0;
				
		}
		
		$totale_con_iva = $totale + $iva;
		
		echo $totale_con_iva;
		
}

if(isset($_POST['edit_image_sort_order'])) {
	$image_sort = $_POST['sort_order']+1;
	Db::getInstance()->executeS("UPDATE image SET position = ".$image_sort." WHERE id_image = ".$_POST['id_image']."");
		
}

if(Tools::getValue('cancellaprodotto')) {

	/*Db::getInstance()->executes("UPDATE cart_product SET bundle = (CASE WHEN bundle = 999999 THEN 88888878 ELSE 88888877 END) WHERE id_product = ".$_POST['id_product']." AND id_cart = ".$_POST['id_cart']."");
	Db::getInstance()->executes("UPDATE carrelli_creati_prodotti SET bundle  = (CASE WHEN bundle = 999999 THEN 88888878 ELSE 88888877 END) WHERE id_product = ".$_POST['id_product']." AND id_cart = ".$_POST['id_cart']."");
	*/
}

if(Tools::getValue('cancellaprodotto_daordine')) {

	//Db::getInstance()->executes("DELETE FROM order_detail WHERE product_id = ".$_POST['id_product']." AND id_order = ".$_POST['id_order']."");

}

else if(Tools::getValue('aggiungiescludi')) {

	$excludeIds.= $_POST['id_product'].",";

}

else {


	
	$query = Tools::getValue('q', false);
	
	if(!$_GET['product_in_bundle']) {
	if (!$query OR $query == '' OR strlen($query) < 1)
		die();
	}
	/*
	 * In the SQL request the "q" param is used entirely to match result in database.
	 * In this way if string:"(ref : #ref_pattern#)" is displayed on the return list,
	 * they are no return values just because string:"(ref : #ref_pattern#)"
	 * is not write in the name field of the product.
	 * So the ref pattern will be cut for the search request.
	 */
	if ($pos = strpos($query, ' (ref:'))
		$query = substr($query, 0, $pos);

	$query = trim($query);
	$excludeIds = Tools::getValue('excludeIds');
	if ($excludeIds && $excludeIds != 'NaN')
		$excludeIds = implode(',', array_map('intval', explode(',', $excludeIds)));
	else
		$excludeIds = '';

	// Excluding downloadable products from packs because download from pack is not supported
	$excludeVirtuals = (bool)Tools::getValue('excludeVirtuals', false);

	if(Tools::getValue('products_and_bundles')) 
	{
		$customer = new Customer(Tools::getValue('id_customer'));
		$items = Db::getInstance()->ExecuteS('
		SELECT * FROM ((SELECT p.`id_product`, `reference`, p.active, p.id_manufacturer, p.price, pl.name, p.listino as listino, p.quantity as qt_tot, p.stock_quantity as spring, p.wholesale_price as acquisto, p.stock_quantity as magazzino, p.supplier_quantity as allnet, p.esprinet_quantity as esprinet, p.intracom_quantity as intracom, p.asit_quantity as asit, p.itancia_quantity as itancia, p.ordinato_quantity as qt_ordinato, p.impegnato_quantity as qt_impegnato, p.arrivo_quantity as qt_arrivo, p.arrivo_esprinet_quantity as qt_arrivo_esprinet, p.sconto_acquisto_1 as sc_acq_1, p.sconto_acquisto_2 as sc_acq_2, p.sconto_acquisto_3 as sc_acq_3
		FROM `'._DB_PREFIX_.'product` p
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.id_product = p.id_product)
		LEFT JOIN feature_product fp ON p.id_product = fp.id_product
		LEFT JOIN manufacturer m ON m.id_manufacturer = p.id_manufacturer
		LEFT JOIN category c ON p.id_category_default = c.id_category
		WHERE p.id_category_default != 246 
		AND
		(
		p.active = 3
		'.(Tools::getValue('online') == 1 ? ' or p.active = 1 ': '').' 
		
		'.(Tools::getValue('offline') == 1 ? 'or p.active = 0 ': '').' 

		'.(Tools::getValue('old') == 1 ? 'or p.active = 2 ': '').' 
		
		)
		'.(Tools::getValue('disponibili') == 1 ? 'AND p.quantity > 0 ': '').' 
		'.(Tools::getValue('marca') > 0 ? 'AND p.id_manufacturer = '.Tools::getValue('marca').' ': '').' 
		'.(Tools::getValue('serie') > 0 ? 'AND fp.id_feature_value = '.Tools::getValue('serie').' ': '').' 
		'.(Tools::getValue('fornitore') > 0 ? 'AND p.id_supplier = '.Tools::getValue('fornitore').' ': '').' 
		'.(Tools::getValue('categoria') > 0 ? 'AND (c.nleft >= '.$cleftright['nleft'].' AND c.nright <= '.$cleftright['nright'].')': '').' 
		
		AND p.id_category_default != 256 AND (pl.name LIKE \'%'.pSQL($query).'%\' OR p.id_product LIKE \'%'.pSQL($query).'%\' OR p.reference LIKE \'%'.pSQL($query).'%\' OR REPLACE(pl.name," ","") LIKE \'%'.pSQL($query).'%\' OR REPLACE(p.reference," ","") LIKE \'%'.pSQL($query).'%\' OR m.name LIKE \'%'.pSQL($query).'%\' OR pl.name LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\' OR p.reference LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\') AND pl.id_lang = 5'.
		(!empty($excludeIds) ? ' AND p.id_product NOT IN ('.$excludeIds.') ' : ' ').
		($excludeVirtuals ? 'AND p.id_product NOT IN (SELECT pd.id_product FROM `'._DB_PREFIX_.'product_download` pd WHERE (pd.id_product = p.id_product))' : '').'
		)
		
		UNION
		
		(SELECT CONCAT(b.id_bundle,"b") AS id_product, b.bundle_ref as reference, b.active, "" as id_manufacturer, '.($customer->id_default_group == 3 || $customer->id_default_group == 12 ? 'b.priceRIV' : 'b.priceCLI').' as price, b.bundle_name as name, "" as listino, "" as qt_tot, "" as spring, "" as acquisto, "" as magazzino, "" as allnet, "" as esprinet, "" as intracom, "" as asit, "" as itancia, "" as qt_ordinato, "" as qt_impegnato, "" as qt_arrivo, "" as qt_arrivo_esprinet, "" as sc_acq_1, "" as sc_acq_2, "" as sc_acq_3 FROM bundle b WHERE 
		
		(
		b.active = 3
		'.(Tools::getValue('online') == 1 ? ' or b.active = 1 ': '').' 
		
		'.(Tools::getValue('offline') == 1 ? 'or b.active = 0 ': '').' 

		'.(Tools::getValue('old') == 1 ? 'or b.active = 2 ': '').' 
		
		)
		'.(Tools::getValue('disponibili') == 1 ? 'AND b.father IN (SELECT id_product FROM product WHERE id quantity > 0) ': '').' 
		'.(Tools::getValue('marca') > 0 ? 'AND b.father IN (SELECT id_product FROM product WHERE id_manufacturer = '.Tools::getValue('marca').') ': '').' 
		'.(Tools::getValue('serie') > 0 ? 'AND b.father IN (SELECT id_product FROM product p JOIN feature_product fp ON p.id_product = fp.id_product WHERE fp.id_feature_value = '.Tools::getValue('serie').') ': '').' 
		'.(Tools::getValue('fornitore') > 0 ? 'AND b.father IN (SELECT id_product FROM product WHERE id_supplier = '.Tools::getValue('fornitore').')  ': ' ').' 
		'.(Tools::getValue('categoria') > 0 ? 'AND b.father IN (SELECT id_product FROM product p JOIN category c ON p.id_category_default = c.id_category WHERE (c.nleft >= '.$cleftright['nleft'].' AND c.nright <= '.$cleftright['nright'].')) ': '').' 
		AND 
		
		
		b.bundle_name LIKE \'%'.pSQL($query).'%\' OR b.bundle_ref LIKE \'%'.pSQL($query).'%\' OR REPLACE(b.bundle_name," ","") LIKE \'%'.pSQL($query).'%\' OR REPLACE(b.bundle_ref," ","") LIKE \'%'.pSQL($query).'%\'
		OR b.bundle_name LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\' OR b.bundle_ref LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\')) tt

		ORDER BY (CASE WHEN tt.name LIKE "'.$query.'%" THEN 0 ELSE 1 END), tt.reference ASC');
	}
	else if(Tools::getValue('products_base')) 
	{
		$items = Db::getInstance()->ExecuteS('
		SELECT p.`id_product`, `reference`, p.active, p.id_manufacturer, p.price, pl.name, p.listino as listino, p.quantity as qt_tot, p.stock_quantity as spring, p.wholesale_price as acquisto, p.stock_quantity as magazzino, p.supplier_quantity as allnet, p.esprinet_quantity as esprinet, p.intracom_quantity as intracom, p.asit_quantity as asit,  p.itancia_quantity as itancia, p.ordinato_quantity as qt_ordinato, p.impegnato_quantity as qt_impegnato, p.arrivo_quantity as qt_arrivo, p.arrivo_esprinet_quantity as qt_arrivo_esprinet, p.sconto_acquisto_1 as sc_acq_1, p.sconto_acquisto_2 as sc_acq_2, p.sconto_acquisto_3 as sc_acq_3
		FROM `'._DB_PREFIX_.'product` p
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.id_product = p.id_product)
		LEFT JOIN manufacturer m ON m.id_manufacturer = p.id_manufacturer
		LEFT JOIN category c ON p.id_category_default = c.id_category
		WHERE 
		pl.id_lang = 5 AND p.id_category_default and
		
		AND 
		(pl.name LIKE \'%'.pSQL($query).'%\'  OR m.name LIKE \'%'.pSQL($query).'%\' OR p.id_product LIKE \'%'.pSQL($query).'%\' OR p.reference LIKE \'%'.pSQL($query).'%\' OR REPLACE(pl.name," ","") LIKE \'%'.pSQL($query).'%\' OR REPLACE(p.reference," ","") LIKE \'%'.pSQL($query).'%\'
	OR pl.name LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\' OR p.reference LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\')  '.
		(!empty($excludeIds) ? ' AND p.id_product NOT IN ('.$excludeIds.') ' : ' ').
		($excludeVirtuals ? '
		AND p.id_product NOT IN (SELECT pd.id_product FROM `'._DB_PREFIX_.'product_download` pd WHERE (pd.id_product = p.id_product))' : '').' ORDER BY (CASE WHEN pl.name LIKE "'.$query.'%" THEN 0 ELSE 1 END), reference ASC LIMIT 150');
	}
	else 
	{
		if(Tools::getValue('categoria') > 0)
			$cleftright = Db::getInstance()->getRow('SELECT nleft, nright FROM category WHERE id_category = '.Tools::getValue('categoria').'');
		
		$items = Db::getInstance()->ExecuteS('
		SELECT p.`id_product`, `reference`, p.active, p.price, p.id_manufacturer, pl.name, p.listino as listino, p.quantity as qt_tot, p.stock_quantity as spring, p.wholesale_price as acquisto, p.stock_quantity as magazzino, p.supplier_quantity as allnet, p.intracom_quantity as intracom, p.asit_quantity as asit, p.esprinet_quantity as esprinet, p.itancia_quantity as itancia, p.ordinato_quantity as qt_ordinato, p.impegnato_quantity as qt_impegnato, p.arrivo_quantity as qt_arrivo, p.arrivo_esprinet_quantity as qt_arrivo_esprinet, p.sconto_acquisto_1 as sc_acq_1, p.sconto_acquisto_2 as sc_acq_2, p.sconto_acquisto_3 as sc_acq_3
		FROM `'._DB_PREFIX_.'product` p
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.id_product = p.id_product)
		LEFT JOIN feature_product fp ON p.id_product = fp.id_product
		LEFT JOIN manufacturer m ON m.id_manufacturer = p.id_manufacturer
		LEFT JOIN category c ON p.id_category_default = c.id_category
		WHERE 
		pl.id_lang = 5 AND p.id_category_default and
		(
		p.active = 3
		'.(Tools::getValue('online') == 1 ? ' or p.active = 1 ': '').' 
		
		'.(Tools::getValue('offline') == 1 ? 'or p.active = 0 ': '').' 

'.(Tools::getValue('old') == 1 ? 'or p.active = 2 ': '').' 
		
		)
		'.(Tools::getValue('disponibili') == 1 ? 'AND p.quantity > 0 ': '').' 
		'.(Tools::getValue('marca') > 0 ? 'AND p.id_manufacturer = '.Tools::getValue('marca').' ': '').' 
		'.(Tools::getValue('serie') > 0 ? 'AND fp.id_feature_value = '.Tools::getValue('serie').' ': '').' 
		'.(Tools::getValue('fornitore') > 0 ? 'AND p.id_supplier = '.Tools::getValue('fornitore').' ': '').' 
		'.(Tools::getValue('categoria') > 0 ? 'AND (c.nleft >= '.$cleftright['nleft'].' AND c.nright <= '.$cleftright['nright'].')': '').' 
		
		AND 
		(pl.name LIKE \'%'.pSQL($query).'%\'  OR m.name LIKE \'%'.pSQL($query).'%\' OR p.id_product LIKE \'%'.pSQL($query).'%\' OR p.reference LIKE \'%'.pSQL($query).'%\' OR REPLACE(pl.name," ","") LIKE \'%'.pSQL($query).'%\' OR REPLACE(p.reference," ","") LIKE \'%'.pSQL($query).'%\'
	OR pl.name LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\' OR p.reference LIKE \'%'.pSQL(str_replace(' ','',$query)).'%\') '.
		(!empty($excludeIds) && Tools::getValue('copia_accessori') != 1 ? ' AND p.id_product NOT IN ('.$excludeIds.') ' : ' ').
		($excludeVirtuals ? '
		AND p.id_product NOT IN (SELECT pd.id_product FROM `'._DB_PREFIX_.'product_download` pd WHERE (pd.id_product = p.id_product))' : '').' GROUP BY p.id_product ORDER BY (CASE WHEN pl.name LIKE "'.$query.'%" THEN 0 ELSE 1 END), reference ASC LIMIT 150');
	}
	
	$i = 0;
	
	if($_GET['bundles'] == 'y') {
	
		$items = Db::getInstance()->ExecuteS('SELECT * FROM bundle WHERE bundle_name LIKE \'%'.pSQL($query).'%\' OR bundle_ref LIKE \'%'.pSQL($query).'%\'');
	}
	
	
	
	if($_GET['product_in_bundle']) {
	
		$items = Db::getInstance()->ExecuteS('
	SELECT p.`id_product`, `reference`, p.active, p.price, pl.name, p.id_manufacturer, p.listino as listino, p.quantity as qt_tot, p.stock_quantity as spring, p.wholesale_price as acquisto, p.stock_quantity as magazzino, p.supplier_quantity as allnet, p.esprinet_quantity as esprinet, p.intracom_quantity as intracom, p.asit_quantity as asit, p.itancia_quantity as itancia, p.sconto_acquisto_1 as sc_acq_1, p.sconto_acquisto_2 as sc_acq_2, p.sconto_acquisto_3 as sc_acq_3
	FROM `'._DB_PREFIX_.'product` p
	LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.id_product = p.id_product)
	WHERE pl.id_lang = 5 AND p.id_product = '.$_GET['product_in_bundle'].' ORDER BY reference ASC');
	
	}

	if ($items)
		foreach ($items AS $item) {
		
		$cart = new Cart(Tools::getValue('id_cart'));		
		
		$customer = Db::getInstance()->getValue("SELECT id_customer FROM cart WHERE id_cart = ".Tools::getValue('id_cart')."");
		$group = Db::getInstance()->getValue("SELECT id_default_group FROM customer WHERE id_customer = ".$customer."");
		
		$prezzi_carrello = Db::getInstance()->getValue("SELECT prezzi_carrello FROM cart WHERE id_cart = ".Tools::getValue('id_cart')." ".(Tools::getIsset('vedirevisione') ? "AND id_revisione = ".Tools::getValue('vedirevisione') : '')."");
		
		if($prezzi_carrello == 3)
			$group = 3;
		

		if($group == 3) {
			$ctrl_sc_qt = Db::getInstance()->getValue("SELECT count(id_product) FROM specific_price WHERE id_product = ".$item['id_product']." AND (specific_price_name = 'sc_riv_1' OR specific_price_name = 'sc_riv_2')");
								
			if($ctrl_sc_qt > 0) {
				$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$item['id_product'].']" name="usa_sconti_quantita['.$item['id_product'].']" type="checkbox" checked="checked" onclick="ristabilisciPrezzo('.$item['id_product'].'); calcolaImporto('.$item['id_product'].'); '.($cookie->profile == 7 ? 'calcolaProvvigione('.$item['id_product'].');' : '').'" />';
			}
			else {
				$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$item['id_product'].']" name="usa_sconti_quantita['.$item['id_product'].']" type="hidden" value="0" />';
			}
		}

		else if($group == 12) {
			$ctrl_sc_qt = Db::getInstance()->getValue("SELECT count(id_product) FROM specific_price WHERE id_product = ".$item['id_product']." AND (specific_price_name = 'sc_riv_1' OR specific_price_name = 'sc_riv_2'  OR specific_price_name = 'sc_riv_3')");
								
			if($ctrl_sc_qt > 0) {
				$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$item['id_product'].']" name="usa_sconti_quantita['.$item['id_product'].']" type="checkbox" checked="checked" onclick="ristabilisciPrezzo('.$item['id_product'].'); calcolaImporto('.$item['id_product'].'); '.($cookie->profile == 7 ? 'calcolaProvvigione('.$item['id_product'].');' : '').'" />';
			}
			else {
				$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$item['id_product'].']" name="usa_sconti_quantita['.$item['id_product'].']" type="hidden" value="0" />';
			}
		}

		else if($group == 22) {
			$ctrl_sc_qt = Db::getInstance()->getValue("SELECT count(id_product) FROM specific_price WHERE id_product = ".$item['id_product']." AND (specific_price_name = 'sc_riv_1' OR specific_price_name = 'sc_riv_2')");
								
			if($ctrl_sc_qt > 0) {
				$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$item['id_product'].']" name="usa_sconti_quantita['.$item['id_product'].']" type="checkbox" checked="checked" onclick="ristabilisciPrezzo('.$item['id_product'].'); calcolaImporto('.$item['id_product'].'); '.($cookie->profile == 7 ? 'calcolaProvvigione('.$item['id_product'].');' : '').'" />';
			}
			else {
				$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$item['id_product'].']" name="usa_sconti_quantita['.$item['id_product'].']" type="hidden" value="0" />';
			}
		}			
							
		else {
			$ctrl_sc_qt = Db::getInstance()->getValue("SELECT count(id_product) FROM specific_price WHERE id_product = ".$item['id_product']." AND (specific_price_name = 'sc_qta_1' OR specific_price_name = 'sc_qta_2' OR specific_price_name = 'sc_qta_3')");
								
			if($ctrl_sc_qt > 0) {
				$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$item['id_product'].']" name="usa_sconti_quantita['.$item['id_product'].']" type="checkbox" checked="checked" onclick="ristabilisciPrezzo('.$item['id_product'].'); calcolaImporto('.$item['id_product'].'); '.($cookie->profile == 7 ? 'calcolaProvvigione('.$item['id_product'].');' : '').'" />';
			}
			else {
				$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$item['id_product'].']" name="usa_sconti_quantita['.$item['id_product'].']" type="hidden" value="0" />';
			}
		}
		
			if(strlen($item['name'] > 100)) {
				$name = substr($item['name'],0,100)."... ";
			}
			else {
				$name = $item['name'];
			}
			
			$login_for_offer = Db::getInstance()->getValue('SELECT login_for_offer FROM product WHERE id_product = '.$item['id_product']);
			$margin_login_for_offer = Db::getInstance()->getValue('SELECT margin_login_for_offer FROM product WHERE id_product = '.$item['id_product']);
			
			$wholesale_price = ($item['acquisto'] > 0 ? $item['acquisto'] : ($item['listino']*(100-$item['sc_acq_1'])/100)*((100-$item['sc_acq_2'])/100)*((100-$item['sc_acq_3'])/100));
			
			$prezzo_login_for_offer = (($wholesale_price * 100) / (100 - $margin_login_for_offer));	
			
			$sc_qta_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_1' AND id_product = '".$item['id_product']."'");
			$sc_qta_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_2' AND id_product = '".$item['id_product']."'");
			$sc_qta_3_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_qta_3' AND id_product = '".$item['id_product']."'");	
			$sc_riv_1_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_1' AND id_product = '".$item['id_product']."'");	
			$sc_riv_2_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_2' AND id_product = '".$item['id_product']."'");
			$sc_riv_3_r = Db::getInstance()->getRow("SELECT price, reduction FROM specific_price WHERE specific_price_name = 'sc_riv_3' AND id_product = '".$item['id_product']."'");
			
			$sc_qta_1 = $sc_qta_1_r['price'] - ($sc_qta_1_r['price'] * $sc_qta_1_r['reduction']);
			$sc_qta_2 = $sc_qta_2_r['price'] - ($sc_qta_2_r['price'] * $sc_qta_2_r['reduction']);
			$sc_qta_3 = $sc_qta_3_r['price'] - ($sc_qta_3_r['price'] * $sc_qta_3_r['reduction']);
			$sc_riv_1 = $sc_riv_1_r['price'] - ($sc_riv_1_r['price'] * $sc_riv_1_r['reduction']);
			$sc_riv_2 = $sc_riv_2_r['price'] - ($sc_riv_2_r['price'] * $sc_riv_2_r['reduction']);
			$sc_riv_3 = $sc_riv_3_r['price'] - ($sc_riv_3_r['price'] * $sc_riv_3_r['reduction']);
			
			$unitario = $item['price'];
			
			if($login_for_offer == 1)
				$unitario = $prezzo_login_for_offer;
			
			$sc_qta_1_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_1' AND id_product = '".$item['id_product']."'");
			$sc_qta_2_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_2' AND id_product = '".$item['id_product']."'");
			$sc_qta_3_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_qta_3' AND id_product = '".$item['id_product']."'");
			
			$sc_riv_1_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_1' AND id_product = '".$item['id_product']."'");
			$sc_riv_2_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_2' AND id_product = '".$item['id_product']."'");
			$sc_riv_3_q = Db::getInstance()->getValue("SELECT from_quantity FROM specific_price WHERE specific_price_name = 'sc_riv_3' AND id_product = '".$item['id_product']."'");
			
			if($group == 3 || ($group == 22 && $sc_riv_2 == 0)) {
				$prezzounitario = $unitario;
				$confronto_prezzi = array($sc_riv_1, $sc_qta_1, $sc_qta_2, $sc_qta_3);
				foreach($confronto_prezzi as $prz) {
					if($prz != 0 && $prz < $prezzounitario) {
						$prezzounitario = $prz;
						
						if($group == 22 && $sc_riv_2 == 0)
							$prezzounitario = $prezzounitario + ($prezzounitario / 100 * 3);
					}
				}
			}
			
			else if($group == 12) {
				$prezzounitario = $unitario;
				$confronto_prezzi = array($sc_riv_1, $sc_riv_3, $sc_qta_1, $sc_qta_2, $sc_qta_3);
				foreach($confronto_prezzi as $prz) {
					if($prz != 0 && $prz < $prezzounitario) {
						$prezzounitario = $prz;
					}
				}
			}
			else if($group == 22 && $sc_riv_2 != 0) {
				$prezzounitario = $unitario;
				$confronto_prezzi = array($sc_riv_2, $sc_qta_1, $sc_qta_2, $sc_qta_3);
				foreach($confronto_prezzi as $prz) {
					if($prz != 0 && $prz < $prezzounitario) {
						$prezzounitario = $prz;
					}
				}
			}
			
			else {
				$prezzounitario = $unitario;
			}
			
			$speciale = Product::trovaPrezzoSpeciale($item['id_product'], 1, 0);
			
			
			
			
			
			$special_wholesale = Db::getInstance()->getRow('SELECT * FROM specific_price_wholesale spw WHERE spw.to > NOW() AND (spw.pieces = "" OR spw.pieces > 0) AND spw.id_product = '.$item['id_product']);
			if(($special_wholesale['wholesale_price'] > 0 || ($special_wholesale['wholesale_price'] == 0 && $special_wholesale['reduction_1'] == 100)) && $wholesale_price > $special_wholesale['wholesale_price'])
				$wholesale_price = $special_wholesale['wholesale_price'];
								
			$prezzo_partenza = ($item['price'] == 0 && $item['free'] == 0 ? number_format(($customer->id_default_group == 3 ? $sc_riv_1 : ($customer->id_default_group == 12 ? $sc_riv_3 : $item['product_price'])), 2, ',', '') : number_format($item['price'], 2, ',', ''));
			
			if($group == 22)
			{
				if($sc_riv_2 == 0)
					$prezzo_partenza = $sc_riv_1 + ($sc_riv_1 / 100 * 3);
				else
					$prezzo_partenza = $sc_riv_2;
			}
			
			if($speciale < $prezzounitario && $speciale != 0) {
				$prezzounitario = $speciale;
				$unitario = $speciale;
				$id_spec = Product::trovaIdPrezzoSpeciale($item['id_product']);
				$wholesale_price_sp = Db::getInstance()->getValue('SELECT wholesale_price FROM specific_price WHERE id_specific_price = '.$id_spec);
				if($wholesale_price_sp > 0 && $wholesale_price_sp < $wholesale_price)
					$wholesale_price = $wholesale_price_sp;
				$varspec = 1;
				//$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$item['id_product'].']" name="usa_sconti_quantita['.$item['id_product'].']" type="hidden" value="0" />';
			}
			else {
				$varspec = 0;
			}
			
			if($prezzi_carrello == 15)
			{
				$prezzounitario = Product::trovaMigliorPrezzo($item['id_product'],3,999999);
				
				//per evitare che il prezzo per ric.riv. superi il prezzo dei clienti normali
				if($prezzounitario < Product::trovaMigliorPrezzo($item['id_product'],1,1))
				{
					$prezzounitario = $prezzounitario+(($prezzounitario/100)*3);
					
					$item['sconto_extra'] = (($unitario - $prezzounitario)*100)/$unitario;
				}
			}
			
			if($prezzi_carrello == 3)
			{
				$item['sconto_extra'] = (($unitario - $prezzounitario)*100)/$unitario;
				
			}
			
			$margine = $prezzo_partenza - $wholesale_price;
			$marginalita = ((($prezzo_partenza - $wholesale_price)*100)/$prezzo_partenza);
			
			if($marginalita > 20)
				$margine_provvigione = $margine / 100 * 30;
			else
				$margine_provvigione = $margine / 100 * 25;
			
			$provvigione = $margine_provvigione * 100 / $prezzo_partenza;
	
			$provvigione_personalizzata = Db::getInstance()->getValue('SELECT provvigione FROM product WHERE id_product = '.$item['id_product']);
			
			if($provvigione_personalizzata > 0)
				$provvigione = $provvigione_personalizzata;
			
			$provvigione = $prezzo_partenza * ($provvigione/100);
			
			
			$acquisto = '<input '.($cookie->profile == 7 ? 'type="hidden"' : 'type="text"').' size="7" style="text-align:right"  onkeyup="javascript:document.getElementById(\'usa_sconti_quantita['.$item['id_product'].']\').checked = false; calcolaPrezzoScontoExtra('.$item['id_product'].', \'inverso\'); calcolaImporto('.$item['id_product'].'); '.($cookie->profile == 7 ? 'calcolaProvvigione('.$item['id_product'].');' : '').'" class="wholesale_price_class" name="wholesale_price['.$item['id_product'].']" id="wholesale_price['.$item['id_product'].']" value="'.number_format(round($wholesale_price,2), 2, ',', '').'" />';
			
			if($cookie->profile == 7)
			{
				$acquisto .= '<input type="text" style="text-align:right" size="7" readonly="readonly" class="provvigione" name="provvigione['.$item['id_product'].']" id="provvigione['.$item['id_product'].']" value="'.number_format(round($provvigione,2),2,",","").'" />';
			}
			
			/* funzione controllo prezzo acquisto
			<script type="text/javascript">$(document).ready(function() { var acq = document.getElementById("wholesale_price['.$item['id_product'].']"); var last = acq.value; var ctrl_m_c_'.$item['id_product'].' = 0; if(ctrl_m_c_'.$item['id_product'].' == 0) { $(document.getElementById("wholesale_price['.$item['id_product'].']")).one("click", function() { alert("Attenzione: stai cambiando il prezzo di acquisto"); ctrl_m_c_'.$item['id_product'].' = 1; }); } if(ctrl_m_c_'.$item['id_product'].' == 0) { $(document.getElementById("wholesale_price['.$item['id_product'].']")).one("keydown", function(event) { var code = (event.keyCode ? event.keyCode : event.which); if(code == 9 || code == 37 || code == 38 || code == 39 || code == 40) { } else { alert("Attenzione: stai cambiando il prezzo di acquisto"); ctrl_m_c_'.$item['id_product'].' = 1; } }); } // Setup the event $(document.getElementById("wholesale_price['.$item['id_product'].']")).focusout(function() { ctrl_m_c_'.$item['id_product'].' = 0; if (last != $(this).val()) { var surec = window.confirm("Stai cambiando un prezzo d\'acquisto. Sei sicuro?"); if (surec) { return true; } else { document.getElementById("wholesale_price['.$item['id_product'].']").value = last; } } }); });</script>
			*/
			
			if($item['id_product'] == 367442 || $item['id_product'] == 367491 ||  $item['id_product'] == 387753 ||  $item['id_product'] == 387752)
				$section = 1;
			else
				$section = 0;
			
			$marg = '<td style="text-align:right"><input type="hidden" name="sconto_acquisto_1['.$item['id_product'].']" id="sconto_acquisto_1['.$item['id_product'].']" value="'.$item['sc_acq_1'].'" /><input type="hidden" name="sconto_acquisto_2['.$item['id_product'].']" id="sconto_acquisto_2['.$item['id_product'].']" value="'.$item['sc_acq_2'].'" /><input type="hidden" name="sconto_acquisto_3['.$item['id_product'].']" id="sconto_acquisto_3['.$item['id_product'].']" value="'.$item['sc_acq_3'].'" /><input type="hidden" class="prezzoacquisto '.($section == 1 ? 'prezzoacquisto_ezcloud' : 'prezzoacquisto_prodotti').'" name="totaleacquisto['.$item['id_product'].']" id="totaleacquisto['.$item['id_product'].']" value="'.number_format(round($wholesale_price,2), 2, ',', '').'" /><span id="spanmarginalita['.$item['id_product'].']" '.($cookie->profile == 7 ? 'style=\'display:none\'' : '').'>'.($cookie->profile == 7 ? '' : number_format($marginalita, 2, ',', '').'%').'</span></td>';
			
			
			
			
			//CONTROLLO PREZZI PRODOTTI AGGIUNTI CON BUNDLE
			if(isset($_GET['product_in_bundle'])) {
				
				$prices = Db::getInstance()->getValue("SELECT bundle_prices FROM bundle WHERE id_bundle = ".$_GET['id_bundle']."");
				$prices = rtrim($prices,';');
				$bundle_products = explode(":", $prices);
				
				
				
				for($ix = 0; $ix<(count($bundle_products));$ix++) {
					
					$bundle_prod = explode(";",$bundle_products[$ix]);
					$count_bundle_products = $bundle_prod[0];
					if($bundle_prod[0] == $_GET['product_in_bundle'] && $_GET['id_bundle'] > 0) {
					
						if($group == 3 || $group == 12) {
							$prezzo_prd_riv = Product::trovaMigliorPrezzo($bundle_prod[0],3,$bundle_prod[1]);
							if($group == 12)
								$prezzo_prd_riv = Product::trovaMigliorPrezzo($bundle_prod[0],12,$bundle_prod[1]);
								
							$prezzo_base_riv = number_format($prezzo_prd_riv, 2,".","");
							$prezzo_prd_riv = $prezzo_prd_riv - (($prezzo_prd_riv * (str_replace(",",".",$bundle_prod[3])))/100);
							$prezzo_prd_riv = number_format($prezzo_prd_riv, 2,".","");
							if($prezzo_prd_riv < $unitario) {
								$unitario = $prezzo_prd_riv;
								$prezzounitario = $prezzo_prd_riv;
								$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$item['id_product'].']" name="usa_sconti_quantita['.$item['id_product'].']" type="hidden" value="0" />';
							}
						}
						else {
							$prezzo_prd_cli = Product::trovaMigliorPrezzo($bundle_prod[0],1,$bundle_prod[1]);
							$prezzo_base_cli = number_format($prezzo_prd_cli, 2,".","");
							$prezzo_prd_cli = $prezzo_prd_cli - (($prezzo_prd_cli * (str_replace(",",".",$bundle_prod[2])))/100);
							$prezzo_prd_cli = number_format($prezzo_prd_cli, 2,".","");
							
							if($prezzo_prd_cli < $unitario) {
								$unitario = $prezzo_prd_cli;
								$prezzounitario = $prezzo_prd_cli;
								$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$item['id_product'].']" name="usa_sconti_quantita['.$item['id_product'].']" type="hidden" value="0" />';
							}
						}
					
						$qt1 = $bundle_prod[1];
				
						$imp1 = $prezzounitario*$bundle_prod[1];
					}
					else 
					{
						/*$quantity = "<input style='text-align:right' name='nuovo_quantity[".$item['id_product']."]' id='product_quantity[".$item['id_product']."]' size='1' type='text' value='".count($bundle_products)."' onkeyup='calcolaImporto(".$item['id_product'].");' />";
				
						$importo = "<td style='text-align:right' id='valoreImporto[".$item['id_product']."]'>".number_format((Product::trovaMigliorPrezzo($item['id_product'],$group,1)), 2, ',', '').	"</td>";
						*/
	
					}
					
					$quantity = "<input style='text-align:right' name='nuovo_quantity[".$item['id_product']."]' id='product_quantity[".$item['id_product']."]' size='3' type='text' value='".($qt1 > 0 ? $qt1 : 1)."' onkeyup='calcolaImporto(".$item['id_product']."); ".($cookie->profile == 7 ? 'calcolaProvvigione('.$item['id_product'].');' : '')."' />";
				
					$importo = "<td style='text-align:right' id='valoreImporto[".$item['id_product']."]'>".number_format(($imp1 > 0 ? $imp1 : Product::trovaMigliorPrezzo($item['id_product'],$group,1) ), 2, ',', '').	"</td>";
				}
				
				
			}
			//FINE CONTROLLO PREZZI PRODOTTI AGGIUNTI CON BUNDLE
			
			if(isset($_GET['id_cart'])) {
				$price = '<input size="7" style="text-align:right" onkeyup=" if(stopKeyUp == false) { document.getElementById(\'usa_sconti_quantita['.$item['id_product'].']\').checked = false; calcolaPrezzoScontoExtra('.$item['id_product'].', \'inverso\'); calcolaImporto('.$item['id_product'].'); '.($cookie->profile == 7 ? 'calcolaProvvigione('.$item['id_product'].');' : '').' }" name="nuovo_price['.$item['id_product'].']" id="product_price['.$item['id_product'].']" type="text" value="'.number_format($prezzounitario, 2, ',', '') .'" />';
			}
			else {
				$price = '<input size="7" style="text-align:right" onkeyup="if(stopKeyUp == false) {  calcolaImporto('.$item['id_product'].'); '.($cookie->profile == 7 ? 'calcolaProvvigione('.$item['id_product'].');' : '').' }" name="nuovo_price['.$item['id_product'].']" id="product_price['.$item['id_product'].']" type="text" value="'.number_format($prezzounitario, 2, ',', '') .'" />';
			}
			$unitario_base = $unitario;
			$unitario = '<input name="product_id['.$item['id_product'].']" id="product_id_'.$item['id_product'].'" value="'.$item['id_product'].'" class="product_ids" type="hidden" /><input type="hidden" id="unitario['.$item['id_product'].']" name="unitario['.$item['id_product'].']" value="'.$unitario.'" />';
			$sc_qta_1 = '<input type="hidden" id="sc_qta_1['.$item['id_product'].']" value="'.$sc_qta_1.'" />';
			$sc_qta_2 = '<input type="hidden" id="sc_qta_2['.$item['id_product'].']" value="'.$sc_qta_2.'" />';
			$sc_qta_3 = '<input type="hidden" id="sc_qta_3['.$item['id_product'].']" value="'.$sc_qta_3.'" />';
			$sc_riv_1 = '<input type="hidden" id="sc_riv_1['.$item['id_product'].']" value="'.$sc_riv_1.'" />';
			$sc_riv_2 = '<input type="hidden" id="sc_riv_2['.$item['id_product'].']" value="'.$sc_riv_2.'" />';
			$sc_riv_3 = '<input type="hidden" id="sc_riv_3['.$item['id_product'].']" value="'.$sc_riv_3.'" />';
			
			$sc_qta_1_q = '<input type="hidden" id="sc_qta_1_q['.$item['id_product'].']" value="'.$sc_qta_1_q.'" />';
			$sc_qta_2_q = '<input type="hidden" id="sc_qta_2_q['.$item['id_product'].']" value="'.$sc_qta_2_q.'" />';
			$sc_qta_3_q = '<input type="hidden" id="sc_qta_3_q['.$item['id_product'].']" value="'.$sc_qta_3_q.'" />';
			$sc_riv_1_q = '<input type="hidden" id="sc_riv_1_q['.$item['id_product'].']" value="'.$sc_riv_1_q.'" />';
			$sc_riv_2_q = '<input type="hidden" id="sc_riv_2_q['.$item['id_product'].']" value="'.$sc_riv_2_q.'" />';
			$sc_riv_3_q = '<input type="hidden" id="sc_riv_3_q['.$item['id_product'].']" value="'.$sc_riv_3_q.'" />';
			
			if(isset($_GET['product_in_bundle']) && ($_GET['product_in_bundle'] != '31109' && $_GET['product_in_bundle'] != '31110' )) {
				
			}
			else 
			{
				$quantity = "<input style='text-align:right' name='nuovo_quantity[".$item['id_product']."]' id='product_quantity[".$item['id_product']."]' size='3' type='text' value='1' onkeyup=\" if (document.getElementById('usa_sconti_quantita[".$item['id_product']."]').checked == true) { calcolaPrezzoScontoExtra(".$item['id_product'].", 'sconto'); } calcolaImporto(".$item['id_product']."); ".($cookie->profile == 7 ? 'calcolaProvvigione('.$item['id_product'].');' : '')."\" />";
				$importo = "<td style='text-align:right' id='valoreImporto[".$item['id_product']."]'>".number_format($prezzounitario, 2, ',', '')."</td>";
			}
			
			
		
			$delete = '<a style="cursor:pointer" onclick="delProduct30('.$item['id_product'].'); togliImporto('.$item['id_product'].'); "><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a>';
			$delete.="<input id='impImporto[".$item['id_product']."]' class='importo ".($section == 1 ? 'importo_ezcloud' : 'importo_prodotti')."' type='hidden' value='0' />";
			
			$sconto_extra = '<input class="sconto_extra" style="text-align:right '.($prezzounitario == 0 ? ';display:none' : '').'" name="sconto_extra['.$item['id_product'].']" id="sconto_extra['.$item['id_product'].']" type="text" size="2" value="'.number_format($item['sconto_extra'],2,",","").'" onkeyup="if (isArrowKey(event)) return; calcolaPrezzoScontoExtra('.$item['id_product'].', \'sconto\'); '.($cookie->profile == 7 ? 'calcolaProvvigione('.$item['id_product'].');' : '').' calcolaImporto('.$item['id_product'].');" />';
			
			$virtual = ProductDownload::getIdFromIdProduct($item['id_product']);
			if($virtual > 0) 
				$virtual = 1;
			else
				$virtual = 0;
			
			$tooltip = Product::showProductTooltip($item['id_product']);
			
			$reference = '<div id="div-element-'.$item['id_product'].'" style="background-color:#ffffff; border:1px solid black; position:fixed; top:0px; left:0px; z-index:999999; display:none">'.$tooltip.'</div><span style="" onmouseover="var Y = event.clientY; if(Y < 440) { $(\'#div-element-'.$item['id_product'].'\').css({position: \'fixed\', top: event.clientY + 80, left: event.clientX + 150}); } else { $(\'#div-element-'.$item['id_product'].'\').css({position: \'fixed\', top: event.clientY - 440, left: event.clientX + 150}); } $(\'#div-element-'.$item['id_product'].'\').show();" onmouseout="$(\'#div-element-'.$item['id_product'].'\').hide();" class="span-reference" id="span-reference-'.$item['id_product'].'">'.$item['reference'].'</span>';
			
			if(Tools::getValue('bundles')) 
			{
				$immagine = Db::getInstance()->getValue('SELECT id_image FROM bundle_image WHERE id_bundle = '.$item['id_bundle'].'');
				
				if($immagine > 0)
					$src_img = 'http://www.ezdirect.it/img/b/'.$immagine.'-'.$item['id_bundle'].'.jpg';
				else
					$src_img = 'http://www.ezdirect.it/img/m/'.$item['id_manufacturer'].'-small.jpg';
			}
			else if(Tools::getValue('products_and_bundles')) 
			{
				if(strpos($item['id_product'], 'b' !== false))
				{
					$immagine = Db::getInstance()->getValue('SELECT id_image FROM bundle_image WHERE id_bundle = '.$item['id_bundle'].'');
					
					if($immagine > 0)
						$src_img = 'http://www.ezdirect.it/img/b/'.$immagine.'-'.$item['id_bundle'].'.jpg';
					else
						$src_img = 'http://www.ezdirect.it/img/m/'.$item['id_manufacturer'].'-small.jpg';
				}
				else
				{
					$immagine = Db::getInstance()->getValue('SELECT id_image FROM image WHERE id_product = '.$item['id_product'].' AND cover = 1');
				
					if($immagine > 0)
						$src_img = 'http://www.ezdirect.it/img/p/'.$item['id_product'].'-'.$immagine.'-small.jpg';
					else
						$src_img = 'http://www.ezdirect.it/img/m/'.$item['id_manufacturer'].'-small.jpg';
					
					if(strpos($item['id_product'], 'b') !== false)
					{
						$immagine = Db::getInstance()->getValue('SELECT id_image FROM bundle_image WHERE id_bundle = '.str_replace("b","",$item['id_product']).'');		
						$src_img = 'http://www.ezdirect.it/img/b/'.$immagine.'-'.str_replace("b","",$item['id_product']).'.jpg';
					}	
					
				}
			}
			else
			{		
				
				$immagine = Db::getInstance()->getValue('SELECT id_image FROM image WHERE id_product = '.$item['id_product'].' AND cover = 1');
				
				if($immagine > 0)
					$src_img = 'http://www.ezdirect.it/img/p/'.$item['id_product'].'-'.$immagine.'-small.jpg';
				else
					$src_img = 'http://www.ezdirect.it/img/m/'.$item['id_manufacturer'].'-small.jpg';
				
				if(strpos($item['id_product'], 'b' !== false))
				{
					$immagine = Db::getInstance()->getValue('SELECT id_image FROM bundle_image WHERE id_bundle = '.str_replace("b","",$item['id_product']).'');		
					$src_img = 'http://www.ezdirect.it/img/b/'.$immagine.'-'.str_replace("b","",$item['id_product']).'.jpg';
				}	
			}
			

			global $link;
			$link_rewrite = $link->getProductLink($item['id_product'], Db::getInstance()->getValue('SELECT link_rewrite FROM product_lang WHERE id_lang = 5 AND id_product = '.$item['id_product']), Db::getInstance()->getValue('SELECT link_rewrite FROM category_lang WHERE id_lang = 5 AND id_category = '.Db::getInstance()->getValue('SELECT id_category_default FROM product WHERE id_product = '.$item['id_product'])), 5);
			
			
			if (strpos($item['id_product'],'b') !== false) {
				$link_rewrite = Link::getBundleLink(str_replace('b','',$item['id_product']));
			}
			
			if($_GET['bundles'] == 'y') 
			{
				$link_rewrite = '--';
				if($i == 0)
				{
						echo $item['id_bundle']." ".$item['bundle_name'].' '.$item['bundle_ref'].' | '.($item['id_bundle']).' | '.($group == 3 ? $item['priceRIV'] : $item['priceCLI']).' |  | '.$item['bundle_ref'].' |'.$item['bundle_name'].'|*BUNDLE*|'.$item['bundle_products'].'| | | | | | | | |--|--|--|--|--|--|--|'.number_format(($group == 3 ? $item['priceRIV'] : $item['priceCLI']), 2,",","")." &euro;".'|--|--|--|--|'.$i.'|--|--|'.$link_rewrite.'|--|--|--|'.$src_img."\n";
						
						$i++;
						
						echo $item['id_bundle']." ".$item['bundle_name'].' '.$item['bundle_ref'].' | '.($item['id_bundle']).' | '.($group == 3 ? $item['priceRIV'] : $item['priceCLI']).' |  | '.$item['bundle_ref'].' |'.$item['bundle_name'].'|*BUNDLE*|'.$item['bundle_products'].'| | | | | | | | |--|--|--|--|--|--|--|'.number_format(($group == 3 ? $item['priceRIV'] : $item['priceCLI']), 2,",","")." &euro;".'|--|--|--|--|'.$i.'|--|--|'.$link_rewrite.'|--|--|--|'.$src_img."\n";
				}
				else
				{
					echo $item['id_bundle']." ".$item['bundle_name'].' '.$item['bundle_ref'].' | '.($item['id_bundle']).' | '.($group == 3 ? $item['priceRIV'] : $item['priceCLI']).' |  | '.$item['bundle_ref'].' |'.$item['bundle_name'].'|*BUNDLE*|'.$item['bundle_products'].'| | | | | | | | |--|--|--|--|--|--|--|'.number_format(($group == 3 ? $item['priceRIV'] : $item['priceCLI']), 2,",","")." &euro;".'|--|--|--|--|'.$i.'|--|--|'.$link_rewrite.'|--|--|--|'.$src_img."\n";
				}	
			}
			else
			{
				//ultimo: 38
				if(Tools::getIsset('get_bdls'))
				{
					$prezzoriv = Product::trovaMigliorPrezzo($item['id_product'],3,1);
					$baseprice = number_format($item['price'], 2, ',', '');
					$basepriceriv = number_format($prezzoriv, 2, ',', '');
					$marginalitariv = ((($prezzoriv - $wholesale_price)*100)/$prezzoriv);
					
					$price = '<input name="nuovo_price['.$item['id_product'].']" id="product_price['.$item['id_product'].']" type="hidden" value="'.number_format($item['price'], 2, ',', '') .'" />';
					$unitario = '<input type="hidden" id="wholesale_price['.$item['id_product'].']" value="'.$wholesale_price.'" /><input type="hidden" id="unitario['.$item['id_product'].']" value="'.$unitario_base.'" />';
					
					
					$margriv = '<span id="spanmarginalitariv['.$item['id_product'].']" '.($cookie->profile == 7 ? 'style=\'display:none\'' : '').'>'.number_format($marginalitariv, 2, ',', '').'%</span><input id="impImportoRiv['.$item['id_product'].']" class="importoriv" type="hidden" value="0" /><input id="impImporto['.$item['id_product'].']" class="importo" type="hidden" value="0" /><input type="hidden" id="product_priceriv['.$item['id_product'].']" name="nuovo_priceriv['.$item['id_product'].']" value="'.$prezzoriv.'"><input type="hidden" id="product_price['.$item['id_product'].']" name="nuovo_price['.$item['id_product'].']" value="'.$item['price'].'">';
					
				
					$delete = '<a style="cursor:pointer" onclick="delProduct30('.$item['id_product'].'); togliImporto('.$item['id_product'].'); "><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a>';
				}
				
				if($prezzi_carrello == 15 || $prezzi_carrello == 3)
					$usa_sconti_quantita = '<input id="usa_sconti_quantita['.$item['id_product'].']" name="usa_sconti_quantita['.$item['id_product'].']" type="hidden" value="0" />';
				
				
				$current_category = Db::getInstance()->getValue('SELECT id_category_default FROM product WHERE id_product = '.$item['id_product']);
				$suggeriti .= '<div style="display:none"><div id="prodotti_suggeriti_'.$item['id_product'].'"><h2>Prodotti suggeriti</h2><table class="table"><tr><th></th><th>Codice</th><th>Desc. prodotto</th><th>Prezzo</th><th>EZ</th><th>Ord.</th><th></th></tr>';
				
				$products_suggeriti = Db::getInstance()->executeS('SELECT p.id_product, p.id_manufacturer, p.reference, p.supplier_reference, p.stock_quantity as magazzino, sku_amazon, fnsku, asin, scorta_minima_amazon, impegnato_amazon, p.scorta_minima, pl.name AS nome_prodotto, p.ordinato_quantity as qt_ordinato, p.supplier_quantity as allnet, p.esprinet_quantity as esprinet, p.itancia_quantity as itancia, p.quantity as totale, pl.name, m.name AS costruttore FROM product p JOIN product_lang pl ON p.id_product = pl.id_product JOIN manufacturer m ON p.id_manufacturer = m.id_manufacturer LEFT JOIN supplier s ON p.id_supplier = s.id_supplier WHERE pl.id_lang = 5 AND (stock_quantity + ordinato_quantity) > 0 AND p.id_category_default = "'.$current_category.'" AND p.id_product NOT IN (SELECT id_product FROM cart_product WHERE id_cart = "'.$_GET['id_cart'].'") AND p.active = 1 GROUP BY p.id_product ORDER BY reference asc LIMIT 25');			

								
				
				foreach($products_suggeriti as $suggerito)
				{
					$immagine_suggerito = Db::getInstance()->getValue('SELECT id_image FROM image WHERE id_product = '.$suggerito['id_product'].' AND cover = 1');
				
					if($prezzi_carrello_corrente == 3)
					{	
						$suggerito_prezzo = Product::trovaMigliorPrezzo($suggerito['id_product'],3,999999);
					}
					else if($prezzi_carrello_corrente == 15)
					{
						$suggerito_prezzo = Product::trovaMigliorPrezzo($suggerito['id_product'],3,999999);
						
						
						$suggerito_prezzo = $suggerito_prezzo+(($suggerito_prezzo/100)*3);
					}	
					else
						$suggerito_prezzo = Product::trovaMigliorPrezzo($suggerito['id_product'],$customer->id_default_group,999999);
					
					
					if($immagine_suggerito > 0)
						$src_img_suggerito = 'http://www.ezdirect.it/img/p/'.$suggerito['id_product'].'-'.$immagine_suggerito.'-small.jpg';
					else
						$src_img_suggerito = 'http://www.ezdirect.it/img/m/'.$suggerito['id_manufacturer'].'-small.jpg';	
					
					$suggeriti .= '<tr><td style="text-align:left"><img src="'.$src_img_suggerito.'" alt="" title="" /></td><td style="text-align:left"> <span style="cursor:pointer" class="span-reference" title=""><a href="index.php?tab=AdminCatalogExFeatures&id_product='.$suggerito['id_product'].'&updateproduct&token='.$tokenProducts.'" target="_blank">'.$suggerito['reference'].'</a></td><td style="text-align:left">'.$suggerito['nome_prodotto'].'</td><td style="text-align:right">'.number_format($suggerito_prezzo,2,",",".").'</td><td style="text-align:right">'.$suggerito['magazzino'].'</td><td style="text-align:right">'.$suggerito['qt_ordinato'].'</td><td><button class="button" onclick="aggiungiSuggerito('.$suggerito['id_product'].')">Aggiungi al carrello</button></td></tr>';
					
				}
				
				
				$suggeriti .= '</table></div></div><div style="display:none">';
				
				$link_suggeriti = '<script>$(document).ready(function() { $(".span-reference-2").tooltipster({interactive: true}); });</script><span style="cursor:pointer" class="span-reference-2" data-tooltip-content="#prodotti_suggeriti_'.$item['id_product'].'"><a href="javascript:void()"><img src="https://www.ezdirect.it/img/admin/tab-products.gif" alt="Prodotti suggeriti" title="Prodotti suggeriti" /></a>';
				
				$suggerimenti = $link_suggeriti.$suggeriti;
							
							

				if(Tools::getIsset('get_bdl'))
				{
					// PER BUNDLE
					
					$baseprice = number_format($item['price'], 2, ',', '');
					$basepriceriv = number_format($prezzoriv, 2, ',', '');
					$price = '<input name="nuovo_price['.$item['id_product'].']" id="product_price['.$item['id_product'].']" type="hidden" value="'.number_format($item['price'], 2, ',', '') .'" />';
					
					$unitario = '<input type="hidden" id="wholesale_price['.$item['id_product'].']" value="'.$wholesale_price.'" /><input type="hidden" id="unitario['.$item['id_product'].']" value="'.$unitario.'" />';
					$sc_qta_1 = '<input type="hidden" id="sc_qta_1['.$item['id_product'].']" value="'.$sc_qta_1.'" />';
					$sc_qta_2 = '<input type="hidden" id="sc_qta_2['.$item['id_product'].']" value="'.$sc_qta_2.'" />';
					$sc_qta_3 = '<input type="hidden" id="sc_qta_3['.$item['id_product'].']" value="'.$sc_qta_3.'" />';
					$sc_riv_1 = '<input type="hidden" id="sc_riv_1['.$item['id_product'].']" value="'.$sc_riv_1.'" />';
					$sc_riv_2 = '<input type="hidden" id="sc_riv_2['.$item['id_product'].']" value="'.$sc_riv_2.'" />';
					$sc_riv_3 = '<input type="hidden" id="sc_riv_3['.$item['id_product'].']" value="'.$sc_riv_3.'" />';
					
					$sc_qta_1_q = '<input type="hidden" id="sc_qta_1_q['.$item['id_product'].']" value="'.$sc_qta_1_q.'" />';
					$sc_qta_2_q = '<input type="hidden" id="sc_qta_2_q['.$item['id_product'].']" value="'.$sc_qta_2_q.'" />';
					$sc_qta_3_q = '<input type="hidden" id="sc_qta_3_q['.$item['id_product'].']" value="'.$sc_qta_3_q.'" />';
					$sc_riv_1_q = '<input type="hidden" id="sc_riv_1_q['.$item['id_product'].']" value="'.$sc_riv_1_q.'" />';
					$sc_riv_2_q = '<input type="hidden" id="sc_riv_2_q['.$item['id_product'].']" value="'.$sc_riv_2_q.'" />';
					$sc_riv_3_q = '<input type="hidden" id="sc_riv_3_q['.$item['id_product'].']" value="'.$sc_riv_3_q.'" />';
					
					$quantity = "<input style='text-align:right' name='nuovo_quantity[".$item['id_product']."]' id='product_quantity[".$item['id_product']."]' size='3' type='text' value='1' onkeyup='calcolaImporto(".$item['id_product']."); ".($cookie->profile == 7 ? 'calcolaProvvigione('.$item['id_product'].');' : '')."' />";
					
					$importo = "<td style='text-align:right' id='valoreImporto[".$item['id_product']."]'>".number_format($item['price'], 2, ',', '')."</td>";
					
					
					$marg = '<input type="hidden" name="sconto_acquisto_1['.$item['id_product'].']" id="sconto_acquisto_1['.$item['id_product'].']" value="'.$item['sc_acq_1'].'" /><input type="hidden" name="sconto_acquisto_2['.$item['id_product'].']" id="sconto_acquisto_2['.$item['id_product'].']" value="'.$item['sc_acq_2'].'" /><input type="hidden" name="sconto_acquisto_3['.$item['id_product'].']" id="sconto_acquisto_3['.$item['id_product'].']" value="'.$item['sc_acq_3'].'" /><input type="hidden" name="wholesale_price['.$item['id_product'].']" id="wholesale_price['.$item['id_product'].']" value="'.number_format(round($wholesale_price,2), 2, ',', '').'" /><input type="hidden" class="prezzoacquisto '.($section == 1 ? 'prezzoacquisto_ezcloud' : 'prezzoacquisto_prodotti').'" name="totaleacquisto['.$item['id_product'].']" id="totaleacquisto['.$item['id_product'].']" value="'.number_format(round($wholesale_price,2), 2, ',', '').'" /><span id="spanmarginalita['.$item['id_product'].']" '.($cookie->profile == 7 ? 'style=\'display:none\'' : '').'>'.($cookie->profile == 7 ? '' : number_format($marginalita, 2, ',', '').'%').'</span><input id="impImporto['.$item['id_product'].']" class="importo '.($section == 1 ? 'importo_ezcloud' : 'importo_prodotti').'" type="hidden" value="0" />';
					
					$margriv = '<span id="spanmarginalitariv['.$item['id_product'].']" '.($cookie->profile == 7 ? 'style=\'display:none\'' : '').'>'.number_format($marginalitariv, 2, ',', '').'%</span><input id="impImportoRiv['.$item['id_product'].']" class="importoriv" type="hidden" value="0" />';
					
				
					$delete = '<a style="cursor:pointer" onclick="delProduct30('.$item['id_product'].'); togliImporto('.$item['id_product'].'); "><img src="../img/admin/delete.gif" alt="Cancella" title="Cancella" /></a>';
					
					$sconto_extra = '<input style="text-align:right" name="sconto_extra['.$item['id_product'].']" id="sconto_extra['.$item['id_product'].']" type="text" size="2" value="'.number_format($item['sconto_extra'],2,",","").'" onkeyup="if (isArrowKey(event)) return; calcolaPrezzoScontoExtra('.$item['id_product'].', \'sconto\'); calcolaImporto('.$item['id_product'].'); '.($cookie->profile == 7 ? 'calcolaProvvigione('.$item['id_product'].');' : '').'" />';
					
					$tooltip = Product::showProductTooltip($item['id_product']);
					
					$reference = '<div id="div-element-'.$item['id_product'].'" style="background-color:#ffffff; border:1px solid black; position:fixed; top:0px; left:0px; z-index:999999; display:none">'.$tooltip.'</div><span style="" onmouseover="var Y = event.clientY; if(Y < 550) { $(\'#div-element-'.$item['id_product'].'\').css({position: \'fixed\', top: event.clientY - 200, left: event.clientX + 150}); } else { $(\'#div-element-'.$item['id_product'].'\').css({position: \'fixed\', top: event.clientY - 550, left: event.clientX + 150}); } $(\'#div-element-'.$item['id_product'].'\').show();" onmouseout="$(\'#div-element-'.$item['id_product'].'\').hide();" class="span-reference" id="span-reference-'.$item['id_product'].'">'.$item['reference'].'</span>';
			
					$priceriv = '<input name="nuovo_priceriv['.$item['id_product'].']" id="product_priceriv['.$item['id_product'].']" type="hidden" value="'.number_format($prezzoriv, 2, ',', '') .'" />';
					
					$reference = '<div id="div-element-'.$item['id_product'].'" style="background-color:#ffffff; border:1px solid black; position:fixed; top:0px; left:0px; z-index:999999; display:none">'.$tooltip.'</div><span style="" onmouseover="var Y = event.clientY; if(Y < 550) { $(\'#div-element-'.$item['id_product'].'\').css({position: \'fixed\', top: event.clientY - 200, left: event.clientX + 150}); } else { $(\'#div-element-'.$item['id_product'].'\').css({position: \'fixed\', top: event.clientY - 550, left: event.clientX + 150}); } $(\'#div-element-'.$item['id_product'].'\').show();" onmouseout="$(\'#div-element-'.$item['id_product'].'\').hide();" class="span-reference" id="span-reference-'.$item['id_product'].'">'.$item['reference'].'</span>';
				
					if($i == 0)
					{
						echo "<tr><td style='width:50px'>".($item['id_product'])."</td><td style='width:450px".($varspec == 1 ? '; color:red' : '')."'>".$name."</td><td style='width:100px;'>".$item['reference']."</td><td style='width:60px; text-align:right'>".number_format($prezzounitario, 2,",","")." &euro;</td><td style='width:30px; text-align:right'>".$item['qt_tot']."</td></tr>".' | '.($item['id_product']).' | '.$price.' | '.$usa_sconti_quantita.' | '.$reference.' |'.$item['name'].'|'.$sc_qta_1.'|'.$sc_qta_2.'|'.$sc_qta_3.'|'.$sc_riv_1.'|'.$sc_riv_2.'|'.$sc_qta_1_q.'|'.$sc_qta_2_q.'|'.$sc_qta_3_q.'|'.$sc_riv_1_q.'|'.$sc_riv_2_q.'|'.$quantity.'|'.$importo.'|'.$unitario.'|'.$delete.'|'.$marg.'|'.$sconto_extra.'|'.$priceriv.'|'.$baseprice.'|'.$basepriceriv.'|'.$margriv.'|'.$varspec.'|'.$sc_riv_3.'|'.$sc_riv_3_q.'|'.$suggerimenti.'|'."\n";
						$i++;
						echo "<tr><td style='width:50px'>".($item['id_product'])."</td><td style='width:450px".($varspec == 1 ? '; color:red' : '')."'>".$name."</td><td style='width:100px;'>".$item['reference']."</td><td style='width:60px; text-align:right'>".number_format($prezzounitario, 2,",","")." &euro;</td><td style='width:30px; text-align:right'>".$item['qt_tot']."</td></tr>".' | '.($item['id_product']).' | '.$price.' | '.$usa_sconti_quantita.' | '.$reference.' |'.$item['name'].'|'.$sc_qta_1.'|'.$sc_qta_2.'|'.$sc_qta_3.'|'.$sc_riv_1.'|'.$sc_riv_2.'|'.$sc_qta_1_q.'|'.$sc_qta_2_q.'|'.$sc_qta_3_q.'|'.$sc_riv_1_q.'|'.$sc_riv_2_q.'|'.$quantity.'|'.$importo.'|'.$unitario.'|'.$delete.'|'.$marg.'|'.$sconto_extra.'|'.$priceriv.'|'.$baseprice.'|'.$basepriceriv.'|'.$margriv.'|'.$varspec.'|'.$sc_riv_3.'|'.$sc_riv_3_q.'|'.$suggerimenti.'|'."\n";
					}
					else
					{
						echo "<tr><td style='width:50px'>".($item['id_product'])."</td><td style='width:450px".($varspec == 1 ? '; color:red' : '')."'>".$name."</td><td style='width:100px;'>".$item['reference']."</td><td style='width:60px; text-align:right'>".number_format($prezzounitario, 2,",","")." &euro;</td><td style='width:30px; text-align:right'>".$item['qt_tot']."</td></tr>".' | '.($item['id_product']).' | '.$price.' | '.$usa_sconti_quantita.' | '.$reference.' |'.$item['name'].'|'.$sc_qta_1.'|'.$sc_qta_2.'|'.$sc_qta_3.'|'.$sc_riv_1.'|'.$sc_riv_2.'|'.$sc_qta_1_q.'|'.$sc_qta_2_q.'|'.$sc_qta_3_q.'|'.$sc_riv_1_q.'|'.$sc_riv_2_q.'|'.$quantity.'|'.$importo.'|'.$unitario.'|'.$delete.'|'.$marg.'|'.$sconto_extra.'|'.$priceriv.'|'.$baseprice.'|'.$basepriceriv.'|'.$margriv.'|'.$varspec.'|'.$sc_riv_3.'|'.$sc_riv_3_q.'|'.$suggerimenti.'|'."\n";
						
					}	
				}	
			else
			{
				
				if(Tools::getValue('copia_accessori') == 1)
				{
					if(Tools::getIsset('stop_accessori'))
					{
						
					}
					else
					{
						$sc_riv_1 = 'COPIADA';
						$sc_riv_2 = '';
						$accessori = Db::getInstance()->executeS('SELECT * FROM accessory WHERE id_product_1 = '.$item['id_product']);
						foreach($accessori as $acc)
						{
							if($acc['id_product_2'] != 0)
								$sc_riv_2 .= $acc['id_product_2'].';';
						}
						$sc_riv_2 = substr($sc_riv_2, 0, -1);
					}	
				}
			
				if($item['id_product'] == 367442 || $item['id_product'] == 367491  || $item['id_product'] == 387753  || $item['id_product'] == 387752)
					$section = 1;
				else
					$section = 0;
				
				
				
				
							
	//per intestazione tabella quando restituisco risultati, duplico il primo risultato
				if($i == 0)
				{
					echo $item['id_product']." ".str_replace('|','',$name).' '.$item['reference'].' | '.($item['id_product']).' | '.$price.' | '.$usa_sconti_quantita.' | '.$reference.' |'.str_replace('|','',$item['name']).'|'.$sc_qta_1.'|'.$sc_qta_2.'|'.$sc_qta_3.'|'.$sc_riv_1.'|'.$sc_riv_2.'|'.$sc_qta_1_q.'|'.$sc_qta_2_q.'|'.$sc_qta_3_q.'|'.$sc_riv_1_q.'|'.$sc_riv_2_q.'|'.$quantity.'|'.$importo.'|'.$unitario.'|'.$delete.'|'.$marg.'|'.$sconto_extra.'|'.$item['qt_tot'].'|'.number_format($prezzounitario, 2,",","")." &euro;".'|'.$varspec.'|'.$item['spring'].'|'.$sc_riv_3.'|'.$sc_riv_3_q.'|'.$i.'|'.$virtual.'|'.$acquisto.'|'.$link_rewrite.'|'.$item['reference'].'|'.$item['esprinet'].'|'.$item['allnet'].'|'.$src_img.'|'.$item['reference'].'|'.$item['qt_impegnato'].'|'.$item['itancia'].'|'.$item['active'].'|'.$section.'|'.$priceriv.'|'.$baseprice.'|'.$basepriceriv.'|'.$margriv.'|'.$varspec.'|'.$sc_riv_3.'|'.$sc_riv_3_q.'|'.$suggerimenti.'|'.$item['intracom'].'|'.$item['asit']."\n";
					
					$i++;
					
					echo $item['id_product']." ".str_replace('|','',$name).' '.$item['reference'].' | '.($item['id_product']).' | '.$price.' | '.$usa_sconti_quantita.' | '.$reference.' |'.str_replace('|','',$item['name']).'|'.$sc_qta_1.'|'.$sc_qta_2.'|'.$sc_qta_3.'|'.$sc_riv_1.'|'.$sc_riv_2.'|'.$sc_qta_1_q.'|'.$sc_qta_2_q.'|'.$sc_qta_3_q.'|'.$sc_riv_1_q.'|'.$sc_riv_2_q.'|'.$quantity.'|'.$importo.'|'.$unitario.'|'.$delete.'|'.$marg.'|'.$sconto_extra.'|'.$item['qt_tot'].'|'.number_format($prezzounitario, 2,",","")." &euro;".'|'.$varspec.'|'.$item['spring'].'|'.$sc_riv_3.'|'.$sc_riv_3_q.'|'.$i.'|'.$virtual.'|'.$acquisto.'|'.$link_rewrite.'|'.$item['reference'].'|'.$item['esprinet'].'|'.$item['allnet'].'|'.$src_img.'|'.$item['reference'].'|'.$item['qt_impegnato'].'|'.$item['itancia'].'|'.$item['active'].'|'.$section.'|'.$priceriv.'|'.$baseprice.'|'.$basepriceriv.'|'.$margriv.'|'.$varspec.'|'.$sc_riv_3.'|'.$sc_riv_3_q.'|'.$suggerimenti.'|'.$item['intracom'].'|'.$item['asit']."\n";
					
				}
				else
				{
					echo $item['id_product']." ".str_replace('|','',$name).' '.$item['reference'].' | '.($item['id_product']).' | '.$price.' | '.$usa_sconti_quantita.' | '.$reference.' |'.str_replace('|','',$item['name']).'|'.$sc_qta_1.'|'.$sc_qta_2.'|'.$sc_qta_3.'|'.$sc_riv_1.'|'.$sc_riv_2.'|'.$sc_qta_1_q.'|'.$sc_qta_2_q.'|'.$sc_qta_3_q.'|'.$sc_riv_1_q.'|'.$sc_riv_2_q.'|'.$quantity.'|'.$importo.'|'.$unitario.'|'.$delete.'|'.$marg.'|'.$sconto_extra.'|'.$item['qt_tot'].'|'.number_format($prezzounitario, 2,",","")." &euro;".'|'.$varspec.'|'.$item['spring'].'|'.$sc_riv_3.'|'.$sc_riv_3_q.'|'.$i.'|'.$virtual.'|'.$acquisto.'|'.$link_rewrite.'|'.$item['reference'].'|'.$item['esprinet'].'|'.$item['allnet'].'|'.$src_img.'|'.$item['reference'].'|'.$item['qt_impegnato'].'|'.$item['itancia'].'|'.$item['active'].'|'.$section.'|'.$priceriv.'|'.$baseprice.'|'.$basepriceriv.'|'.$margriv.'|'.$varspec.'|'.$sc_riv_3.'|'.$sc_riv_3_q.'|'.$suggerimenti.'|'.$item['intracom'].'|'.$item['asit']."\n";
				}
	
}	
			
					
			}
			
			$i++;
		}
}