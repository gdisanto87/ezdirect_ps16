CHIESTO:

° Customers: 
- cos'è codice_spring?
- in override\classes\customer.php, cosa fa l'if a riga 91?
- i campi di customer_amministrazione inutilizzati (es. installatore) si possono eliminare dal db o nascondere dall'anagrafica?
- $zona_cliente e $paese_cliente inutilizzati, vanno tenuti?
- tutti i customer devono avere esattamente 1 record in customer_amministrazione?
- perchè agente ed installatore sono varchar in customer_amministrazione?
- fatturato annuo si ripete 2 volte nella scheda Amministrazione dell'anagrafica; prendere dal db o dalla var $acquisti_ultimo_anno?
- order_notifications ha icone / valori scambiati (off - 0 -> Enabled ; on - 1 -> Disabled)
- perchè in Persone la query $conta_persone ha la condizione WHERE vat_number = $customer->vat_number e invece $persone ha WHERE id_customer = customer->id ?
- stat: in $interested, if($product->id == 0) inizializzo $resnmex, cos'è e come fa a funzionare se id prodotto = 0?  
- nella classe, funzione getOrdersByYear() usa sum(total_paid) ma nel controller $totalOKconiva += $order['total_paid_real']; quale devo usare?
- Vedo un a.`fatturazione` != 1 ; ci possono essere valori diversi da 0 / 1 in fatturazione?
- ezcloud: il tasto aggiungi non deve mai essere visibile? Un cliente può avere più di un centralino ezcloud attivo?
- bdl: il pulsante delete è visibile solo se $cookie->profile != 8 ? Quindi Administrator2 non può eliminare bdl?
- perchè per contare il numero di persone, azioni, fatture ecc usi ExecuteS con group by id e poi sizeof() al posto di select count(*)?
- cos'è conf=4 negli url che usi con redirectAdmin?
- yetii: non lo sto usando perchè veniva sovrascritto (male) dal tpl e quindi ho rifatto il sistema di divisione in tab; dimmi se devo reinserirlo per qualche motivo e dove 
- gestione di tecnico e agente di default: come va fatta e quando?
- stat: getBoughtProducts2 sbagliato (mancano distinct e id_lang = x)
- c'è un motivo per fare if($entry == '.' || $entry == '..') { } else { // roba } invece di if(!($entry == '.' || $entry == '..')){ // roba } oppure if ($entry != "." && $entry != "..") { // roba } ?
- Funzione ultimoAcquistoPrimoAcquisto: prende un prodotto a caso del primo/ultimo ordine, che prodotto dovrei far vedere?
- Funzioni non utilizzate (possono essere eliminate?): getInternoTelefonata, iconaCRM, switchTicketType
- Funzione ControllaCF può essere definitivamente sostituita da controllaCF2?
- Funzione ControllaPIVA: variabile $errore_lunghezza non utilizzata, elimino?
- Funzione notMyVatNumberExists: usata in IdentityController 1.4 ma non capisco perchè non usi anzi vatNumberExists
- Funzione notMyTaxCodeExists e customerExistsNotMyMail: come notMyVatNumberExists
- dove fai delete from customer_amministrazione? 
- Quando i clienti vengono cancellati dal db e quando viene invece posto deleted = 1?

° Cloud:
- possiamo aggiungere un flag "stato" per i centralini cloud che indica se pagato o no (e, se è diverso da non pagato, scaduto)?

° BDL:
- nella lista, se fatturato / pagato = 0 e ci clicco, update a 1, ma se è già a 1 resta sempre a 1; corretto o se = 1 devo fare update a 0?

° Employees:
- vedi commenti ultime attività
- in AdminEmployeesController, if($group == 3) $tipo_cliente = 'Rivenditore'; non dovrebbe compendere tutti i tipi di rivenditori?
- function getLastAdmin: devo aggiungerla in override o era di PrestaShop?

    public static function getLastAdmin($active_only = false)
    {
        return Db::getInstance()->getValue('
        SELECT id_employee
        FROM `'._DB_PREFIX_.'employee`
        WHERE `id_profile` = '.(int)_PS_ADMIN_PROFILE_.'
        '.($active_only ? ' AND `active` = 1' : '').' ORDER BY id_employee DESC');
    }

------------------------------------------------------------------------------------------------------

DA CHIEDERE:

° Customers:
- Classe nuova customer riga 110: "True if carrier (forse customer??????????) has been deleted"
- modifica bdl / telefono: 
    $bdl['phone'] deve apparire nella lista ed essere quello selezionato? 
    Cosa vuol dire se $bdl['phone'] = "altro"? Ce ne sono pochi così nel db
    l'indirizzo di id $bdl['id_address'] non viene considerato?
    note private: vengono prese solo da note_attivita o anche dal campo del db bdl.note_private?
    non mi tornano le regole di manutenzione e invio controlli, mi puoi spiegare come dovrebbe funzionare?
- Cloud: come funzionano le tabelle ezcloud? Ci sono varie cose che non mi tornano 
    (rinnovi non presenti in cart_ezcloud? Vanno presi i cart.name rinnovo + tutta la roba che è in cart_ezcloud? 
    Come trovo l'ultimo rinnovo? tutti i clienti hanno un solo centralino cloud?)

° Carts:
- ho aggiunto il campo "convertito" (bool) nel db per evitare di fare sempre join con orders -> aggiornare i cart vecchi

° Products:
- Gestione filtri da correggere!
- tab Carico Magazzino: cos'è? cosa vuol dire modalità esolver? => ignorare per ora ma non eliminare
- Prodotti su amazon: $prodottiinclusi, $categorieincluse etc (anche quelli in POST) sono inutilizzati? Elimino?
- Perchè prezzo spec. vendita nel tooltip e nella lista "tutti i prezzi speciali" sono diversi? 
    price vs (price - reduction)

° Search:
- AdminSearchController: perchè manca il parametro $query in tutte le funzioni?

° Strumenti:
- ControlloImg: 
    - segnalare immagini prodotti caricati negli ultimi 2 anni che non sono conformi a qualità e peso decisi per la nuova grafica
    - cosa fare con le immagini vecchie e come rendere utile lo strumento Controllo Peso Immagini?
    - vogliamo img di buona qualità circa 600x600, usiamo le dim di default nuove?

------------------------------------------------------------------------------------------------------

Appunti per Luca (view customers):
- $customer->gender non esiste; le tabelle gender e gender_lang non c'erano nella 1.4; la varibile $gender contiene Sig. o Sig.ra; id_gender è l'int che devi usare per i controlli, nel db vecchio 1 = m, 2 = f, 9 = sconosciuto
- Aggiungere newsletter ($customer->newsletter) dopo status
- Controllare quale address prendere per il fax
- Allineare tutti i numeri / prezzi a destra e le icone al centro delle caselle
- Profile dev'essere il nome del gruppo $customer->id_default_group in ps_group_lang; devi usare l'array $groups ($groups[0]['name'] nel caso gruppo unico)
- $not_mio_agente comprende $is_agente (+ un'altra condizione), quindi le 2 variabili non si usano mai insieme
-------------------------------------------------------------------------------
- Credit request requirements - Net last 3 months - Order > 1000 Eur devono essere un "blocco" unico graficamente, essendo gli ultimi 2 condizioni per la validità del primo
- Manca da fare in AdminCustomers: controlli supplier ecc, permessi employee e agenti, js, link utili, pulsanti e ultime azioni/ordini/prev; controlla nuove modifiche nei file aggiornati in sito locale
- Eliminare tasto "collegati al sito" tra i pulsanti sotto summary / nelle tab
-------------------------------------------------------------------------------
- Ricordarsi l'update di is_company (se cambia id_default_group) e di ps_cart.convertito che nel db vecchio non esisteva
- Ho cambiato la validate di active nella classe customer
- Cambiato nome variabile in GET: tab-container-1 => tab_name; il valore da numero è diventato una stringa
- Controllare i tpl della 1.4 (backoffice)!!!!!!!!!!!!!!!!!
- Controllare che ci sia escape in tutte le query che ne hanno bisogno (usare funzione pSQL())

Privacy e newsletter:
Per essere GDPR Compliant, bisogna avere più colonne relative ai consensi privacy: Accettazione termini contrattuali, Consenso trattamento dati, Consenso al trattamento dati finalità Marketing e, se utilizzato in azienda, Consenso al trattamento dati finalità Marketing di soggetti terzi.
Generalmente il campo relativo a "newsletter" è coincidente a "Consenso al trattamento dati finalità marketing" perchè, nel momento della revoca da parte del cliente, non si possono più inviare email di quel tipo.
In ogni caso, c'è da aggiornare sul sito web l'informativa privacy al nuovo regolamento oltre che aggiornare l'informativa relativa all'utilizzo di cookie.

Modifiche grafica Ezio (valgono in generale):
- Non affiancare icona e scritta (es. "X No" => mettere "X" oppure "No")
- Compattare il più possibile (quindi + colonne e - righe)
- Dove possibile, risistemare le righe in modo che i flag abbiano X / V di fianco alla label e non sotto (solo dove si può fare per l'intera riga o dove la label non è troppo lunga)
- Risk V rossa e X verde
- Pulsanti tutti su 1 riga dove possibile, anche se nella 1.4 sono su 2
- Usiamo scala di grigi e colori rosso/verde/blu/grigio per flag (come descritto prima)
- Aumentare leggermente font delle label
- Tutti i titoli in grassetto