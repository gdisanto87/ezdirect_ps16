<?php

	include('/var/www/vhosts/ezdirect.it/httpdocs/config/config.inc.php'); 
	include('/var/www/vhosts/ezdirect.it/httpdocs/init.php');
	ini_set("memory_limit","892M");
	ini_set("display_errors","on");
	set_time_limit(3600);
	require_once 'esportazione-catalogo/Classes/PHPExcel.php';
	$objPHPExcel = new PHPExcel();

	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', 'eSolver')
	->setCellValue('B1', 'SKU')
	->setCellValue('C1', 'Nome prodotto')
	->setCellValue('D1', 'SKU Amazon')
	->setCellValue('E1', 'FNSKU')
	->setCellValue('F1', 'ASIN')
	->setCellValue('G1', 'Costruttore')
	->setCellValue('H1', 'Fornitore')
	->setCellValue('I1', 'Qt. da inviare')
	->setCellValue('J1', 'Mag. EZ')
	->setCellValue('K1', 'Da ordinare A.F.')
	->setCellValue('L1', 'Scorta minima')
	->setCellValue('M1', 'Rim. Amazon')
	->setCellValue('N1', 'Ordinato A.F.')
	->setCellValue('O1', 'Imp.EZ')
	->setCellValue('P1', 'Venduto Prime ultimi 30gg')
	->setCellValue('Q1', 'Invenduto 60gg')
	;
	
	$k = 2;
	
	// CORREGGERE: modificare id_lang
	$dataTable = Db::getInstance()->executeS('SELECT reference, supplier_reference, sku_amazon, fnsku, pl.name, asin, stock_quantity, ordinato_quantity, impegnato_quantity, impegnato_amazon, scorta_minima_amazon, m.name manufacturer, s.name supplier FROM '._DB_PREFIX_.'product p JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product = pl.id_product JOIN manufacturer m ON m.id_manufacturer = p.id_manufacturer JOIN '._DB_PREFIX_.'supplier s ON p.id_supplier = s.id_supplier WHERE pl.id_lang =5 AND ((sku_amazon != ""   AND amazon != "" AND (p.scorta_minima_amazon >= 0 OR p.impegnato_amazon >= 0)) OR (p.scorta_minima_amazon > 0 AND sku_amazon = "")) GROUP BY p.id_product ORDER BY (scorta_minima_amazon - impegnato_amazon) DESC ');
		
	foreach ($dataTable as $row) {
	
		$da_inviare_tot = $row['scorta_minima_amazon'] - $row['impegnato_amazon'];
		
		$da_inviare = $da_inviare_tot;
		
		if($da_inviare_tot < 0)
			$da_inviare = 0;
		
		$da_ordinare = $row['stock_quantity'] - $da_inviare_tot - $row['impegnato_quantity'] + $row['ordinato_quantity'];
	
		if($da_ordinare > 0)
			$da_ordinare = 0;
		
		if($da_ordinare < 0)
			$da_ordinare = 0-$da_ordinare;
		
		
		
		$venduto30 = Db::getInstance()->getValue('SELECT sum(product_quantity) from '._DB_PREFIX_.'orders o join '._DB_PREFIX_.'order_detail od on o.id_order = od.id_order join '._DB_PREFIX_.'cart c on o.id_cart = c.id_cart where od.product_reference = "'.$row['reference'].'" and c.name like "%prime%" and o.date_add BETWEEN NOW() - INTERVAL 30 DAY AND NOW()'); 
		
		$venduto60 = Db::getInstance()->getValue('SELECT sum(product_quantity) from '._DB_PREFIX_.'orders o join '._DB_PREFIX_.'order_detail od on o.id_order = od.id_order join '._DB_PREFIX_.'cart c on o.id_cart = c.id_cart where od.product_reference = "'.$row['reference'].'" and c.name like "%prime%" and o.date_add BETWEEN NOW() - INTERVAL 60 DAY AND NOW()'); 
		
		if($venduto60 == 0)
			$invenduto = 'Invenduto';
		else
			$invenduto = '';
		
		//if($da_inviare <> 0)
		//{
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue("A$k", $row['reference'])
				->setCellValue("B$k", $row['supplier_reference'])
				->setCellValue("C$k", $row['name'])
				->setCellValue("D$k", $row['sku_amazon'])
				->setCellValue("E$k", $row['fnsku'])
				->setCellValue("F$k", $row['asin'])
				->setCellValue("G$k", $row['manufacturer'])
				->setCellValue("H$k", $row['supplier'])
				->setCellValue("I$k", $da_inviare)
				->setCellValue("J$k", $row['stock_quantity'])
				->setCellValue("K$k", $da_ordinare)
				->setCellValue("L$k", $row['scorta_minima_amazon'])
				->setCellValue("M$k", $row['impegnato_amazon'])
				->setCellValue("N$k", $row['ordinato_quantity'])
				->setCellValue("O$k", $row['impegnato_quantity'])
				->setCellValue("P$k", $venduto30)
				->setCellValue("Q$k", $invenduto)
			;

		//}
		$k++;
		
	}

	$objPHPExcel->getActiveSheet()->setTitle('Prodotti da inviare ad Amazon');

	$objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);	
	$objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->getFill()->getStartColor()->setRGB('FFFF00');	

	$objPHPExcel->getActiveSheet()->getStyle("A1:Q$k")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
	$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->getFont()->setBold(true);

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
	
	$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
	$cacheSettings = array( ' memoryCacheSize ' => '8000MB');
	PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

	$data = date("Ymd");

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->setPreCalculateFormulas(false);
	$objWriter->setIncludeCharts(true);
	$objWriter->save(str_replace(".php", ".xls", "esportazione-catalogo/catalogo_xls/template-amazon-prime.php"));

	header("Location: http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/template-amazon-prime.xls");

?>
