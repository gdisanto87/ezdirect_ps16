<?php

/* Stampa a video tutti i prodotti con data di arrivo valorizzata */

include('/var/www/vhosts/ezdirect.it/httpdocs/config/config.inc.php'); 
include('/var/www/vhosts/ezdirect.it/httpdocs/init.php');

$arrivi = Db::getInstance()->executeS('
    SELECT id_product, reference, ordinato_quantity, data_arrivo_ordinato 
    FROM '._DB_PREFIX_.'product 
    WHERE data_arrivo_ordinato != "" AND data_arrivo_ordinato != "0" 
    ORDER BY reference ASC
');

foreach($arrivi as $a)
{
    $arrivo_ordinato = $a['data_arrivo_ordinato'];
    $arrivo_ordinato = unserialize($arrivo_ordinato);
    $date_arrivo_ordinato_riga = '';
        
    foreach($arrivo_ordinato as $ao)
        $date_arrivo_ordinato_riga .= '<tr><td style="text-align:right;">'.$ao['ordine'].'</td><td style="text-align:right;">'.round($ao['quantita'],0).'</td><td>'.Db::getInstance()->getValue('SELECT company FROM '._DB_PREFIX_.'customer WHERE codice_esolver = "'.$ao['fornitore'].'"').'</td><td>'.date('d/m/Y',strtotime($ao['data'])).'</td></tr>';

    if($a['ordinato_quantity'] < 0)
        $style = 'style="color:orange;"';
    else if($a['ordinato_quantity'] == 0)
        $style = 'style="color:red;"';
    else
        $style = '';


    $righe_prodotti .= '
        <tr>
            <td>'.$a['id_product'].'</td>
            <td>'.$a['reference'].'</td>
            <td '.$style.'>'.$a['ordinato_quantity'].'</td>
            <td>
                <table>
                    <thead>
                        <tr>
                            <th>ID Ord.</th>
                            <th>Qt.</th>
                            <th>Forn.</th>
                            <th>Data</th>
                        </tr>
                    </thead>
                    <tbody>
                        '.$date_arrivo_ordinato_riga.'
                    </tbody>
                </table>
            </td>
        </tr>
    ';
}

$totale = Db::getInstance()->getValue('
    SELECT COUNT(id_product) 
    FROM '._DB_PREFIX_.'product 
    WHERE data_arrivo_ordinato != "" AND data_arrivo_ordinato != "0" 
');

$totale_0 = Db::getInstance()->getValue('
    SELECT COUNT(id_product) 
    FROM '._DB_PREFIX_.'product 
    WHERE data_arrivo_ordinato != "" AND data_arrivo_ordinato != "0" AND ordinato_quantity = 0
');

$totale_n0 = Db::getInstance()->getValue('
    SELECT COUNT(id_product) 
    FROM '._DB_PREFIX_.'product 
    WHERE data_arrivo_ordinato != "" AND data_arrivo_ordinato != "0" AND ordinato_quantity != 0
');

$totali = '<strong>Totale prodotti:</strong> '.$totale.'<br /><strong>Totale con Ordinati uguale a 0:</strong> '.$totale_0.'<br /><strong>Totale con Ordinati diverso da 0:</strong> '.$totale_n0.'';

$table = '
    <table>
        <thead>
            <tr>
                <th>ID Prodotto</th>
                <th>Codice</th>
                <th>Ordinati</th>
                <th>Date qta in arrivo</th>
            </tr>
        </thead>
        <tbody>
            '.$righe_prodotti.'
        </tbody>
    </table>
';

echo $totali;
echo '<br /><br />';
echo $table;

?>