<?php

/* Procedura che elimina i filtri duplicati */

include('/var/www/vhosts/ezdirect.it/httpdocs/config/config.inc.php'); // correggere: cambiare percorso file per fare prove sul server attuale

if(!$this->context)
	$context = Context::getContext();
else
	$context = $this->context;

$id_lang = $context->language->id;

$features = Db::getInstance()->executeS('
	SELECT *
	FROM '._DB_PREFIX_.'feature_value_lang fvl
	JOIN '._DB_PREFIX_.'feature_value fv ON fv.id_feature_value = fvl.id_feature_value
	WHERE fvl.id_lang = '.$id_lang.'
	GROUP BY fvl.value, fv.id_feature
	HAVING count( fvl.value ) >1
	ORDER BY fv.id_feature
');

echo '<table><tr><th>Id fvl principale</th><th>Id fvl spec.</th><th>Specifica</th><th>Id feature</th><th>Prodotti</th></tr>';

foreach($features as $fvl)
{
	$doppi = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'feature_value_lang fvl JOIN '._DB_PREFIX_.'feature_value fv ON fv.id_feature_value = fvl.id_feature_value WHERE fvl.id_lang = '.$id_lang.' AND fvl.value = "'.$fvl['value'].'" AND fv.id_feature_value != '.$fvl['id_feature_value'].' AND fv.id_feature = '.$fvl['id_feature']);
	
	foreach($doppi as $doppio)
	{
		Db::getInstance()->executeS('UPDATE '._DB_PREFIX_.'feature_product SET id_feature_value = '.$fvl['id_feature_value'].' WHERE id_feature_value = '.$doppio['id_feature_value']);
		
		$prodotti = Db::getInstance()->getValue('SELECT count(id_product) FROM feature_product WHERE id_feature_value = '.$doppio['id_feature_value']);
		
		echo '<tr><td>'.$fvl['id_feature_value'].'</td><td>'.$doppio['id_feature_value'].'</td><td>'.$doppio['value'].'</td><td>'.$doppio['id_feature'].'</td><td>'.$prodotti.'</td>';
		
		Db::getInstance()->executeS('DELETE FROM '._DB_PREFIX_.'feature_value WHERE id_feature_value = '.$doppio['id_feature_value']);
		Db::getInstance()->executeS('DELETE FROM '._DB_PREFIX_.'feature_value_lang WHERE id_feature_value = '.$doppio['id_feature_value']);
	}
}

echo '</table>';

?>
