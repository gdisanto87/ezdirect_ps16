<?php
// correggere include (se non funziona)
include('/var/www/vhosts/ezdirect.it/httpdocs/config/config.inc.php'); 
ini_set('display_errors', 'on');

$prodotti_dollari = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'product WHERE acquisto_in_dollari = 1 AND blocco_prezzi = 0 ORDER BY reference ASC');

$ultima_conversione = Db::getInstance()->getValue('SELECT last_conversion_rate FROM '._DB_PREFIX_.'currency WHERE iso_code = "USD" AND active = 1 AND deleted = 0');
$conversione_attuale = Db::getInstance()->getValue('SELECT conversion_rate FROM '._DB_PREFIX_.'currency WHERE iso_code = "USD" AND active = 1 AND deleted = 0');

echo '<table class="table">';
foreach($prodotti_dollari as $prod)
{
	$vecchio_acquisto = $prod['wholesale_price'];
	$nuovo_acquisto = ($prod['wholesale_price']*$ultima_conversione)/$conversione_attuale;
	
	$nuovo_sconto_acquisto = (($prod['listino'] - $nuovo_acquisto)*100)/$prod['listino'];
	
	echo '<tr><td>'.$prod['reference'].'</td><td>'.number_format($vecchio_acquisto,2,",","").'</td><td>'.number_format($nuovo_acquisto,2,",","").'</td>
	
	<td>'.number_format($prod['sconto_acquisto_1'],2,",","").'</td>
	<td>'.number_format($nuovo_sconto_acquisto,2,",","").'</td>
	</tr>';
	
	$rebates = Db::getInstance()->getRow("SELECT * FROM product_esolver WHERE id_product = ".$prod['id_product']);
	
	$acq_aln_rb = $nuovo_acquisto * ((100 - $rebates['rebate_1'])/100) * ((100 - $rebates['rebate_2'])/100) * ((100 - $rebates['rebate_3'])/100);
	
	Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'product SET wholesale_price = '.$acq_aln_rb.', sconto_acquisto_1 = '.$nuovo_sconto_acquisto.', sconto_acquisto_2 = 0, sconto_acquisto_3 = 0 WHERE id_product = '.$prod['id_product']);
	
	Db::getInstance()->execute('UPDATE product_esolver SET wholesale_price = "'.$nuovo_acquisto.'" WHERE id_product = "'.$prod['id_product'].'"');
}	
echo '</table>';

Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'currency SET last_conversion_rate = conversion_rate WHERE iso_code = "USD" AND active = 1 AND deleted = 0');

?>
