<?php 
// correggere include (se non funziona) e id_lang
include('/var/www/vhosts/ezdirect.it/httpdocs/config/config.inc.php'); 
ini_set('display_errors', 'on');
ini_set("memory_limit","892000000M");
ini_set("max_execution_time", 88000);
ini_set("display_errors", "on");

$array_customers = array();

/* Ogni giorno devo importare / aggiornare i clienti che: 
	- si sono registrati
	- hanno fatto un ordine
	- hanno aggiornato i loro dati
	- si sono iscritti/disiscritti dalla newsletter
*/

$update_where = '
	(date_add BETWEEN SUBDATE(CURDATE(), 1) AND CURDATE()) 
	OR id_customer IN 
		(SELECT id_customer FROM '._DB_PREFIX_.'orders o JOIN '._DB_PREFIX_.'order_history oh ON o.id_order = oh.id_order WHERE oh.date_add BETWEEN SUBDATE(CURDATE(), 1) AND CURDATE())
	OR date_upd BETWEEN SUBDATE(CURDATE(), 1) AND CURDATE()
	OR newsletter_date_add BETWEEN SUBDATE(CURDATE(), 1) AND CURDATE()
';
// AND NOT EXISTS (SELECT * FROM customer WHERE email = c.email AND id_customer != c.id_customer)

// newsletter date add oggi -> fare update appena si iscrive per inviarlo subito a sendinblue

$customers = Db::getInstance()->executeS('
	SELECT * 
	FROM `'._DB_PREFIX_.'customer` 
	WHERE id_default_group != 11
		AND id_default_group != 12
		AND email != " "
		AND ('.$update_where.')
');

foreach($customers as $customer)
{
	$id_customer = $customer['id_customer'];
	$customer = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'customer WHERE id_customer = '.$id_customer);
	
	// Se azienda, nome azienda; altrimenti, nome e cognome
	$nomecliente = $customer['firstname'];
	$nomecliente .= ' '.$customer['lastname'];
	$company_or_name = (($customer['company'] != '') ? ($customer['active'] == 1 ? $customer['company'] : ($customer['active'] == 0 ? 'NON ATTIVO - '.$customer['company'] : 'CESSATA ATTIVITA\' - '.$customer['company'])) : ($customer['active'] == 1 ? $nomecliente : ($customer['active'] == 0 ? 'NON ATTIVO - '.$nomecliente : 'CESSATA ATTIVITA\' - '.$nomecliente)));
	
	if($company_or_name == " ")
		$company_or_name = 'No company';
	
	// Se non ci sono provincia o regione, prendo lo stato
	$stato_regione = Db::getInstance()->getValue('SELECT IF(a.id_state = 0, c.name, s.name) as nome FROM '._DB_PREFIX_.'address a LEFT JOIN '._DB_PREFIX_.'state s ON a.id_state = s.id_state LEFT JOIN '._DB_PREFIX_.'country_lang c ON a.id_country = c.id_country WHERE c.id_lang = 5 AND a.id_customer = '.$id_customer.' AND a.active = 1 AND a.deleted = 0 AND a.fatturazione = 1 ORDER BY a.id_address DESC');
	if($stato_regione == false)
		$stato_regione = 'No region';
	
	$city = Db::getInstance()->getValue('SELECT city FROM '._DB_PREFIX_.'address WHERE id_customer = '.$id_customer.' AND active = 1 AND deleted = 0 AND fatturazione = 1 ORDER BY id_address DESC');
	if($city == false || $city == '' || $city == ' ' || $city == '-- Prima selezionare CAP --')
		$city = 'No city';
	
	$orders = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'orders WHERE id_customer = '.$id_customer);

	$array_ordini = array();
	
	if($customer['id_customer'] > 0)
	{
		foreach($orders as $order)
		{
			$order_history_state = Db::getInstance()->getValue('SELECT MAX(id_order_history) FROM '._DB_PREFIX_.'order_history WHERE id_order = '.$order['id_order']);
			
			$status = Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'order_state_lang osl JOIN '._DB_PREFIX_.'order_state os ON osl.id_order_state = os.id_order_state JOIN '._DB_PREFIX_.'order_history oh ON os.id_order_state = oh.id_order_state WHERE id_lang = 5 AND id_order_history = '.$order_history_state);
			
			$order_details =  Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'order_detail WHERE id_order = '.$order['id_order']);
			$array_dettagli = array();
			foreach($order_details as $dettaglio)
			{
				$prezzo = $dettaglio['product_price'] - ($dettaglio['product_price'] * ($dettaglio['reduction_percent']/100));
				
				$prezzo += $prezzo * ($dettaglio['tax_rate'] / 100);
				
				if($dettaglio['product_reference'] != NULL)
					$reference = $dettaglio['product_reference'];
				else
					$reference = 'No reference';
				
				if($dettaglio['manufacturer_id'] != 0)
					$brand = Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'manufacturer WHERE id_manufacturer = '.$dettaglio['manufacturer_id']);
				else
					$brand = 'No brand';
				
				if($dettaglio['category_id'] != 0)
					$category = Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'category_lang WHERE id_lang = 5 AND id_category = '.$dettaglio['category_id']);
				else if($dettaglio['category_id'] == 0){ // se order_detail non ha la macrocategoria, la prendo da product 
					$cat_product = Db::getInstance()->ExecuteS('SELECT c.name FROM '._DB_PREFIX_.'category_lang c INNER JOIN '._DB_PREFIX_.'product p ON c.id_category = p.id_category_default WHERE c.id_lang = 5 AND p.id_product = '.$dettaglio['product_id']);
					if(count($cat_product)>0){
						foreach($cat_product as $cat_product_row){
							$category = $cat_product_row;
							break;
						}
					}
					else
						$category = 'No category';
				}
				
				$array_dettagli[] = array(
					  'reference' => $reference, // opzionale 1
					  'brand' => $brand, // opzionale 2
					  'category' => $category // opzionale 3
				);
			}
			
			$array_ordini[] = array(
				'id'=> $order['id_order'], // obbligatorio
				"createdAt"=> date('Y-m-d\Th:i:s\Z',strtotime($order['date_add'])), // obbligatorio
				"updatedAt"=> date('Y-m-d\Th:i:s\Z',strtotime($order['date_upd'])), // obbligatorio
				"amount"=> $order['total_paid_real'], // obbligatorio
				"status"=> $status, // obbligatorio
				"statusLabel"=> $status, // obbligatorio
				// metacampi opzionali - max 3 per il nostro piano
				"items"=> $array_dettagli 
			);	
		}
		
		$array_customers[] = array(
		    "id"=> $customer['id_customer'], // obbligatorio
			"email"=> $customer['email'], // obbligatorio
			"createdAt"=> date('Y-m-d\Th:i:s\Z',strtotime($customer['date_add'])), // obbligatorio
			"updatedAt"=> date('Y-m-d\Th:i:s\Z',strtotime($customer['date_upd'])), // obbligatorio
			// metacampi opzionali - max 5 per il nostro piano
			"company"=> $company_or_name, // opzionale 1
			"group" => $customer['id_default_group'], // opzionale 2
			"country_state" => $stato_regione, // opzionale 3
			"city" => $city, // opzionale 4
			"newsletter" => $customer['newsletter'], // opzionale 5
		  'orders' => $array_ordini
		);
	}
	
}

$data = array(
	  "overwriteDescriptors" => false,
	  'customers' => 
	  $array_customers
);

$payload = json_encode($data);
echo $payload;

$url = 'https://api.rfmcube.com/v1/import';
$ch = curl_init( $url );
# Setup request to send json via POST.
echo "<br>OK"; 
curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Accept: application/json', 'api-key:TFHlycgSUnO7w0VW2sPpwKAKBR83fFPn78fC4rlyqRD5l2rT0ntwWmTLISPx'));
# Return response instead of printing.
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
# Send request.
$result = curl_exec($ch);
curl_close($ch);
# Print response.
echo "<pre>$result</pre>";


// Stampa su file
/*$path = "rfmcube_json.json";
$fp = fopen($path, "w");
if(!$fp) 
	die ("Errore nell'apertura del file");
fwrite($fp, $payload);
fclose($fp);*/

?>