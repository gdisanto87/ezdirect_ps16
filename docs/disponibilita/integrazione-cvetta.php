<?php 

/*
 * -------------------------
 * INTEGRAZIONE INUTILIZZATA 
 * -------------------------
 */

// correggere include (se non funziona) e id_lang
include('/var/www/vhosts/ezdirect.it/httpdocs/config/config.inc.php'); 
include('/var/www/vhosts/ezdirect.it/httpdocs/init.php');
ini_set('display_errors', 'on');
ini_set("memory_limit","892M");
ini_set("max_execution_time", 88000);
ini_set("display_errors", "on");

$products = Db::getInstance()->executeS('
	SELECT p.id_product, p.reference, p.supplier_reference as codice, pl.name as nome, cl.name as categoria, m.name as produttore, p.price as prezzo, pl.link_rewrite, i.id_image
	FROM `'._DB_PREFIX_.'product` p
	LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON p.id_product = pl.id_product
	LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON p.id_category_default = cl.id_category
	LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON p.id_manufacturer = m.id_manufacturer
	LEFT JOIN (SELECT * FROM '._DB_PREFIX_.'image WHERE cover = 1) i ON i.id_product = p.id_product
	WHERE p.active = 1 
		AND p.price > 0
		AND p.fuori_produzione = 0
		AND pl.id_lang = 5
		AND cl.id_lang = 5
');

$path = "/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/feed_cvetta.xml";
$fp = fopen($path, "w");
if(!$fp) 
	die ("Errore nell'apertura del file");

$payload = '<?xml version="1.0" encoding="UTF-8"?>';
$payload .= '<add>';

$link_prodotto = new Link();

foreach($products as $product)
{
	$id_product = $product['id_product'];
	$sku = $product['codice'];
	$name = $product['reference'].' - '.$product['nome'];
	$image = 'https://www.ezdirect.it/img/p/'.$product['id_product'].'-'.$product['id_image'].'-large.jpg';
	$url_key = $link_prodotto->getProductLink($product['id_product'], $product['link_rewrite'], strtolower($product['categoria']));
	$categories = $product['categoria'];
	$manufacturer = $product['produttore'];
	$price = $product['prezzo'];

	$payload .= '<doc>';
	
	$payload .= '<field name="sku"><![CDATA['.$sku.']]></field>';
	$payload .= '<field name="name"><![CDATA['.$name.']]></field>';
	$payload .= '<field name="image"><![CDATA['.$image.']]></field>';
	$payload .= '<field name="url_key"><![CDATA['.$url_key.']]></field>';
	$payload .= '<field name="categories"><![CDATA['.$categories.']]></field>';
	$payload .= '<field name="manufacturer"><![CDATA['.$manufacturer.']]></field>';
	$payload .= '<field name="price"><![CDATA['.$price.']]></field>';
	
	$payload .= '</doc>';
}

$payload .= '</add>';

echo $payload;

fwrite($fp, $payload);
fclose($fp);

// connessione ftp
$ftp_username = 'ezdirect';
$ftp_password = '%Uq17126!ezdirecty14329?fK*';
$ftp_server = "cvetta.ittweb.net";
$ftp_conn = ftp_connect($ftp_server) or die("Errore di connessione al server $ftp_server");
$login = ftp_login($ftp_conn, $ftp_username, $ftp_password) or die("Dati errati");
ftp_set_option($ftp_conn, FTP_USEPASVADDRESS, false);
ftp_pasv($ftp_conn, true);

// upload 
if (ftp_put($ftp_conn, "ezdirect.xml", "feed_cvetta.xml", FTP_BINARY)){
	echo "File caricato.";
}
else{
	echo "Errore di caricamento";
}

// chiusura connessione ftp
ftp_close($ftp_conn);

?>