<?php 
// correggere include (se non funziona) e id_lang
include('/var/www/vhosts/ezdirect.it/httpdocs/config/config.inc.php');
ini_set('display_errors', 'on');
ini_set("memory_limit","892000000M");
ini_set("max_execution_time", 88000);
ini_set("display_errors", "on");

// Condizione di update customer
//$update_customer_where = 'date_upd LIKE "0000%"'; // invalid date
$update_customer_where = '(id_default_group = 3 OR id_default_group = 15 OR id_default_group = 22 OR id_default_group = 14) AND active = 1 AND id_customer NOT IN (SELECT id_customer FROM orders) AND newsletter = 0';

// Aggiorno i clienti uno alla volta
$customers = Db::getInstance()->executeS('
	SELECT * 
	FROM `'._DB_PREFIX_.'customer` 
	WHERE id_default_group != 11
		AND id_default_group != 12
		AND email != " "
		AND '.$update_customer_where.'
');

//$i=0;

foreach($customers as $customer)
{
	$id_customer = $customer['id_customer'];
	$customer = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'customer WHERE id_customer = '.$id_customer);
	
	// Se azienda, nome azienda; altrimenti, nome e cognome
	$nomecliente = $customer['firstname'];
	$nomecliente .= ' '.$customer['lastname'];
	$company_or_name = (($customer['company'] != '') ? ($customer['active'] == 1 ? $customer['company'] : ($customer['active'] == 0 ? 'NON ATTIVO - '.$customer['company'] : 'CESSATA ATTIVITA\' - '.$customer['company'])) : ($customer['active'] == 1 ? $nomecliente : ($customer['active'] == 0 ? 'NON ATTIVO - '.$nomecliente : 'CESSATA ATTIVITA\' - '.$nomecliente)));
	
	if($company_or_name == " ")
		$company_or_name = 'No company';
	
	// Se non ci sono provincia o regione, prendo lo stato
	$stato_regione_provincia = Db::getInstance()->getValue('SELECT IF(a.id_state = 0, c.name, s.name) as nome FROM '._DB_PREFIX_.'address a LEFT JOIN '._DB_PREFIX_.'state s ON a.id_state = s.id_state LEFT JOIN '._DB_PREFIX_.'country_lang c ON a.id_country = c.id_country WHERE c.id_lang = 5 AND a.id_customer = '.$id_customer.' AND a.active = 1 AND a.deleted = 0 AND a.fatturazione = 1 ORDER BY a.id_address DESC');
	if($stato_regione_provincia == false)
		$stato_regione_provincia = 'No region';
	
	$city = Db::getInstance()->getValue('SELECT city FROM '._DB_PREFIX_.'address WHERE id_customer = '.$id_customer.' AND active = 1 AND deleted = 0 AND fatturazione = 1 ORDER BY id_address DESC');
	if($city == false || $city == '' || $city == ' ' || $city == '-- Prima selezionare CAP --')
		$city = 'No city';
	
	$array_customer = array();
	
	$array_customer = array(
		"id"=> $customer['id_customer'], // obbligatorio
		"email"=> $customer['email'], // obbligatorio
		"createdAt"=> date('Y-m-d\Th:i:s\Z',strtotime($customer['date_add'])), // obbligatorio
		"updatedAt"=> date('Y-m-d\Th:i:s\Z',strtotime($customer['date_upd'])), // obbligatorio
		// metacampi opzionali - max 5 per il nostro piano
		"company"=> $company_or_name, // opzionale 1
		"group" => $customer['id_default_group'], // opzionale 2
		"country_state" => $stato_regione_provincia, // opzionale 3
		"city" => $city, // opzionale 4
		"newsletter" => $customer['newsletter'] // opzionale 5
	);
	
	$data = array(
	  "triggerIntegrations" => true,
	  'customer' => 
	  $array_customer
	);
	
	//$i++;
	
	unset($array_customer);

	$payload = json_encode($data);
	echo $payload.'<br>';

	$url = 'https://api.rfmcube.com/v1/merge_customer';
	$ch = curl_init( $url );
	# Setup request to send json via POST.
	echo "OK"; 
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
	curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Accept: application/json', 'api-key:TFHlycgSUnO7w0VW2sPpwKAKBR83fFPn78fC4rlyqRD5l2rT0ntwWmTLISPx'));
	# Return response instead of printing.
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	# Send request.
	$result = curl_exec($ch);
	curl_close($ch);
	# Print response.
	echo "<pre>$result</pre>";

	// RFMCube non accetta più di 60 request al minuto; per evitare che si sovrappongano, aspetto 1 secondo prima di inviare la prossima richiesta
	sleep(1);

}

//echo $i;

?>