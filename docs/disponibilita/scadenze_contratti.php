<?php 

/* Procedura eseguita 2 volte al giorno che invia una notifica al cliente e 
apre un todo a Valentina 30 giorni prima della data di scadenza di ogni contratto */

include('/var/www/vhosts/ezdirect.it/httpdocs/config/config.inc.php'); 

$connection = mysql_connect(_DB_SERVER_,_DB_USER_,_DB_PASSWD_);
mysql_select_db(_DB_NAME_,$connection);

ini_set("memory_limit","892M");
set_time_limit(3600);
	
$firma = '<br />
	Cordiali saluti / Best regards<br /><br />
	Ezdirect srl <a href="http://www.ezdirect.it">www.ezdirect.it</a><br /><br />

	Tel +39 0585821163<br />
	Fax +39 0585821286<br />

	<br />
	Prima di stampare pensa all\'ambiente - Think about the environment before printing
	<p style="font-size:10px">
	Ai sensi del Decreto Legislativo n. 196/2003, si precisa che le informazioni contenute in questo messaggio e negli eventuali allegati sono riservate e per uso esclusivo del destinatario. Persone diverse dallo stesso non possono copiare o distribuire il messaggio a terzi. Chiunque riceva questo messaggio per errore, &egrave; pregato di distruggerlo e di informare immediatamente info@ezdirect.it<br /><br />
	
	This message is for the designated recipient only and may contain privileged or confidential information. If you have received it in error, please notify the sender immediately and delete the original. Any other use of the email by you is
	prohibited.
	
	</p>
';
	
$oggetto_promemoria = "Contratto in scadenza";

$testa_promemoria = "
	<html>
	<head>
		<title>Contratto in scadenza</title>
	</head>
	<body>
	<table style='font-family: Verdana,sans-serif; font-size: 11px; color: #374953; width: 550px;'>
	<tr>
	<td align='left'><a title='Ezdirect' href='http://www.ezdirect.it'><img style='border: none;' src='http://www.ezdirect.it/img/logo.jpg' alt='Ezdirect' /></a></td>
	</tr>
	<tr>
	<td style='background-color: #db6909; color: #fff; font-size: 12px; font-weight: bold; padding: 0.5em 1em;' align='left'>Contratto in scadenza</td>
	</tr>
	</table>
";
 
$headers  = 'MIME-Version: 1.0' . "\n";
$headers .= 'Content-Type: text/html' ."\n";
$headers .= 'From: "Ezdirect" <no-reply@ezdirect.it>' . "\n";

// Status = 4 = Scaduto
Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'contratto_assistenza SET status = 4 WHERE data_fine <= CURDATE()');

// Per invio mail

// Procedura eseguita solo 30 giorni prima della scadenza
$contratti_in_scadenza = Db::getInstance()->executeS("
	SELECT c.id_customer, c.firstname, c.lastname, c.company, c.is_company, c.email, co.data_fine, co.id_contratto, co.status, co.tipo, co.descrizione, co.prezzo 
	FROM "._DB_PREFIX_."customer c
	JOIN "._DB_PREFIX_."contratto_assistenza co ON co.id_customer = c.id_customer
	WHERE co.status = 0 
		AND DATE(co.data_fine) = DATE(DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL 30 DAY))
");

echo '<h1>Carrelli creati per rinnovo il '.date('d/m/Y').'</h1>'; 

echo "<style type='text/css'>table td { border:1px solid black }</style><table><tr><th>Azienda</th><th>Nome</th><th>Cognome</th><th>Data scadenza</th><th>ID contratto</th></tr>";

foreach($contratti_in_scadenza as $contratto) 
{
	echo '<tr><td>'.$contratto['company'].'</td><td>'.$contratto['firstname'].'</td><td>'.$contratto['lastname'].'</td><td>'.$contratto['data_fine'].'</td><td>'.$contratto['id_contratto'].'</td></tr>';

	// Teleassistenza
	if($contratto['tipo'] == 4)
	{
		$id_cart_ta = Db::getInstance()->getValue('SELECT id_cart FROM '._DB_PREFIX_.'cart WHERE name = "Rinnovo assistenza '.$contratto['id_contratto'].' anno '.date('Y',strtotime($contratto['data_fine'])).'" AND id_customer = '.$contratto['id_customer']);
		
		if($id_cart_ta > 0) {
			// niente
		}	
		else
		{
			$indirizzo = Db::getInstance()->getValue('SELECT id_address FROM '._DB_PREFIX_.'address WHERE id_customer = '.$contratto['id_customer'].' AND active = 1 AND deleted = 0 AND fatturazione = 1');
			
			$notifiche = array();
			$notifica = array();
			$notifica['email'] = $contratto['email'];
			$notifica['data'] = date("Y-m-d H:i:s");
			$notifica['tipo'] = 'Automatica';
			
			$notifiche[] = $notifica;
			$notifiche = serialize($notifiche);
			
			Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'cart (id_cart, id_lang, id_currency, id_customer, id_address_invoice, id_address_delivery, name, validita, date_add, date_upd, created_by,  id_employee, in_carico_a, preventivo, note, numero_notifiche,notifiche,provvisorio) VALUES (NULL, 5, 1, "'.$contratto['id_customer'].'", "'.$indirizzo.'", "'.$indirizzo.'", "Rinnovo assistenza '.$contratto['id_contratto'].' anno '.date('Y').'", "'.$contratto['data_fine'].'", "'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'", 14,14,14,1, "'.$nota_ta.'",1,"'.addslashes($notifiche).'",1)');

			//$id_cart = mysqli_insert_id();
			$id_cart = Db::getInstance()->getValue('SELECT * FROM '._DB_PREFIX_.'cart ORDER BY id_cart DESC');
			
			$prodotti_per_carrello = Db::getInstance()->executeS('SELECT id_product, prezzo, quantita FROM '._DB_PREFIX_.'contratto_assistenza_prodotti WHERE id_contratto = '.$contratto['id_contratto'].'');
			
			$prezzo_prod = $contratto['prezzo'] + (($contratto['prezzo']/100) * 3);
			$prezzo_acquisto = (($prezzo_prod/100) * 60);
			
			Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'cart_product (id_cart, id_product, quantity, price, free, prezzo_acquisto, date_add) VALUES ("'.$id_cart.'", 335058, "1", "'.$prezzo_prod.'", "0", "'.$prezzo_acquisto.'",  "'.date('Y-m-d H:i:s').'")') ;
			
			$id_cart_ta = $id_cart;
		}
		
		$testo_promemoria = '
			<strong>Comunicazione importante</strong>.
			<br /><br />
			<strong>Gentile cliente, &egrave; in scadenza il servizio di teleassistenza (scadenza: '.Tools::displayDate($contratto['data_fine'],5).')</strong>.
			<br /><br />
			Questo messaggio, per comunicarti che il rinnovo, mantiene inalterate le caratteristiche di servizio di supporto tecnico e ti permette di avere <strong>priorit&agrave; nella gestione dei ticket</strong> postvendita, <strong>aggiornamenti</strong> firmware e supporto <strong>telefonico illimitato e via web</strong>.
			<br /><br />
			Suggeriamo il rinnovo immediato per non perdere la possibilit&agrave; di avere un team di tecnici a tua disposizione per qualunque evento o richiesta, ma anche per aggiornamenti dei prodotti.<br />
			Ti ricordiamo che nel contratto di teleassistenza sono inclusi anche interventi da remoto per supporto sulle funzioni e variazione di configurazione.<br />
			Tutto questo ti permette di avere dispositivi e sistemi sempre <strong>aggiornati, efficienti, la soluzione di problemi</strong> in brevissimo tempo, senza perdere <strong>produttivit&agrave;</strong> ed <strong>efficienza</strong> in azienda.
			<br /><br />
			<strong>Per rinnovare il servizio, fai login su Ezdirect.it e accedi al tuo carrello <a href="https://www.ezdirect.it/le-mie-offerte?id_cart='.$id_cart_ta.'&id_customer='.$contratto['id_customer'].'">cliccando qui</a> (oppure entra nell\'area "<a href="https://www.ezdirect.it/le-mie-offerte">Le mie offerte</a>"), quindi conferma ordine/carrello che abbiamo gi&agrave; predisposto per te</strong>.
			<br /><br />
			Qualora non fosse rinnovato il contratto di teleassistenza, potrai comunque richiedere assistenza al nostro staff tecnico.
			<br /><br />
			In caso di mancato rinnovo, per le future eventuali richieste di supporto, ti verr&agrave; richiesto di ordinare dal sito web un pacchetto di assistenza (a partire da 59,00 &euro; - al bisogno).  Dopo l\'ordine via web e verifica del pagamento, i nostri tecnici procederanno alla fornitura del servizio.
			<br /><br />
			Rimaniamo a tua completa disposizione per chiarimenti
		';
	}
	else
	{
		$id_cart_ta = Db::getInstance()->getValue('SELECT id_cart FROM '._DB_PREFIX_.'cart WHERE name = "Rinnovo contratto n. '.$contratto['id_contratto'].' anno '.date('Y').'" AND id_customer = '.$contratto['id_customer']);
		
		if($id_cart_ta > 0) {
			// niente
		}
		else
		{
			$indirizzo = Db::getInstance()->getValue('SELECT id_address FROM '._DB_PREFIX_.'address WHERE id_customer = '.$contratto['id_customer'].' AND active = 1 AND deleted = 0 AND fatturazione = 1');
			
			$notifiche = array();
			$notifica = array();
			$notifica['email'] = $contratto['email'];
			$notifica['data'] = date("Y-m-d H:i:s");
			$notifica['tipo'] = 'Automatica';
			
			$notifiche[] = $notifica;
			$notifiche = serialize($notifiche);
			
			Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'cart (id_cart, id_lang, id_currency, id_customer, id_address_invoice, id_address_delivery, name, validita, date_add, date_upd, created_by,  id_employee, in_carico_a, preventivo, note, numero_notifiche, notifiche,provvisorio) VALUES (NULL, 5, 1, "'.$contratto['id_customer'].'", "'.$indirizzo.'", "'.$indirizzo.'", "Rinnovo contratto n. '.$contratto['id_contratto'].' anno '.date('Y').'", "'.$contratto['data_fine'].'", "'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'", 14,14,14,1, "'.$nota_ta.'",1,"'.addslashes($notifiche).'",1)');

			//$id_cart = mysqli_insert_id();
			$id_cart = Db::getInstance()->getValue('SELECT * FROM '._DB_PREFIX_.'cart ORDER BY id_cart DESC');
			
			$prodotti_per_carrello = Db::getInstance()->executeS('SELECT id_product, prezzo, quantita FROM '._DB_PREFIX_.'contratto_assistenza_prodotti WHERE id_contratto = '.$contratto['id_contratto'].'');
			
			$prezzo_prod = $contratto['prezzo'];
			$prezzo_acquisto = (($prezzo_prod/100) * 60);
			
			Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'cart_product (id_cart, id_product, quantity, price, free, prezzo_acquisto, date_add) VALUES ('.$id_cart.', 336198, "1", "'.$prezzo_prod.'", "0", "'.$prezzo_acquisto.'",  "'.date('Y-m-d H:i:s').'")') ;
			
			$id_cart_ta = $id_cart;
		}

		$testo_promemoria = '
			Gentile cliente, ti informiamo che il tuo servizio di assistenza tecnica* sta per scadere (scadenza: '.Tools::displayDate($contratto['data_fine'],5).'). L\'importo per il rinnovo di questo importante servizio &egrave; di '.number_format($contratto['prezzo'],2,",","").' &euro; + IVA.<br />
			Se desideri disdire il servizio (evitando il rinnovo), comunicacelo utilizzando i moduli di contatto del nostro sito web (ti preghiamo di non inviare email) o chiamaci al numero 0585 821163.
			<br /><br /><strong>Per rinnovare il servizio, fai login su Ezdirect.it e accedi al tuo carrello <a href="https://www.ezdirect.it/le-mie-offerte?id_cart='.$id_cart_ta.'&id_customer='.$contratto['id_customer'].'">cliccando qui</a> (oppure entra nell\'area "<a href="https://www.ezdirect.it/le-mie-offerte">Le mie offerte</a>"), quindi conferma ordine/carrello che abbiamo gi&agrave; predisposto per te</strong>.
			<br /><br />* Il servizio include telegestione, supporto telefonico e via web, aggiornamenti firmware, precedenza sulla gestione dei ticket. Ti ricordiamo che, non rinnovando il servizio, potrai sempre usufruire dei servizi di assistenza, previa richiesta via ticket on line o chiamando il nostro servizio tecnico, il quale ti comunicher&agrave; ad ogni esigenza il pacchetto assistenza da acquistare, sulla base dell\'intervento da effettuare.<br />
		';
	}

	// $mail_promemoria = $testa_promemoria.$testo_promemoria.$firma;
	// mail($contratto['email'], $oggetto_promemoria, $mail_promemoria, $headers);

	// Apro un to-do a Valentina
	
	$action_subject = "*** PROMEMORIA AUTOMATICO - CONTRATTO ".$contratto['id_contratto']." (".date('Y').") IN SCADENZA SU ".($contratto['is_company'] == 1 ? $contratto['company'] : $contratto['firstname']." ".$contratto['lastname'])."**";

	$action = Db::getInstance()->getRow('
		SELECT at.id_action, at.action_from, at.action_to 
		FROM action_message am 
		JOIN action_thread at ON at.id_action = am.id_action 
		WHERE at.id_customer = '.$contratto['id_customer'].' 
			AND subject LIKE "%'.$action_subject.'%"
	');

	if($action['id_action'] > 0) {
		//Db::getInstance()->execute("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, ".$action['id_action'].", '".$contratto['id_customer']."', 99, '".$action['action_from']."', '".$action['action_to']."', '', 'Inviata notifica al cliente:<br /><br />".($testo_promemoria != '' ? addslashes($testo_promemoria) : addslashes($testo_promemoria))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
	}	
	else
	{
		$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
		$action_thread++;
	
		$id_employee = 14; // Valentina
		$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').'1');

		Db::getInstance()->execute("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd, promemoria) VALUES ('$action_thread', 'Attivita', '".$contratto['id_customer']."', '".$action_subject."', 0, ".$id_employee.", 'open', '', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."', 1)");   
		Db::getInstance()->execute("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', '".$contratto['id_customer']."', 99, '".$id_employee."', '".$id_employee."', '', '<strong>*** VERIFICA PREVENTIVO CONTRATTO IN SCADENZA ***</strong><br /><br />Il cliente ha in scadenza il <a href=\"https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$contratto['id_customer']."&viewcustomer&id_contratto=".$contratto['id_contratto']."&token=".$tokenCustomers."&tab-container-1=13\" target=\"_blank\">contratto n. ".$contratto['id_contratto']."</a>. Verificare il preventivo del contratto.', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
		// Con href 1.6 -> correggere: dovremmo fare un update degli href nel db oppure fare un redirect al link giusto nei controller
		// Db::getInstance()->execute("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', '".$contratto['id_customer']."', 99, '".$id_employee."', '".$id_employee."', '', '<strong>*** VERIFICA PREVENTIVO CONTRATTO IN SCADENZA ***</strong><br /><br />Il cliente ha in scadenza il <a href=\"https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".$contratto['id_customer']."&viewcustomer&tab_name=contracts&azione=contratto_view&id_contratto=".$contratto['id_contratto']."&token=".$tokenCustomers."\" target=\"_blank\">contratto n. ".$contratto['id_contratto']."</a>. Verificare il preventivo del contratto.', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
	}
}

// Per apertura TODO
/*
$contratti_in_scadenza2 = Db::getInstance()->executeS("
	SELECT c.id_customer, c.firstname, c.lastname, c.company, c.is_company, c.email, co.data_fine, co.id_contratto, co.status, co.tipo, co.descrizione 
	FROM "._DB_PREFIX_."customer c
	JOIN "._DB_PREFIX_."contratto_assistenza co ON co.id_customer = c.id_customer
	WHERE co.data_fine BETWEEN DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL 29 DAY) 
		AND DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL 30 DAY) OR (co.data_fine BETWEEN DATE_ADD(CURRENT_TIMESTAMP() , INTERVAL 14 DAY) 
		AND DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL 15 DAY))
");
*/

// SCADUTI, esclusi i noleggi (tipo = 5)
$contratti_scaduti = Db::getInstance()->executeS("
	SELECT c.id_customer, c.firstname, c.lastname, c.company, c.is_company, c.email, co.data_fine, co.id_contratto, co.status, co.tipo, co.descrizione, co.prezzo 
	FROM "._DB_PREFIX_."customer c
	JOIN "._DB_PREFIX_."contratto_assistenza co ON co.id_customer = c.id_customer
	WHERE co.status = 4 
		AND co.tipo !=5 
		AND ((co.data_fine BETWEEN DATE_ADD(CURRENT_TIMESTAMP() , INTERVAL -4 DAY) 
		AND DATE_ADD(CURRENT_TIMESTAMP() , INTERVAL -3 DAY))) 
");

echo "<style type='text/css'>table td { border:1px solid black }</style><table><tr><th>Azienda</th><th>Nome</th><th>Cognome</th><th>Data scadenza</th><th>ID contratto</th></tr>";

foreach($contratti_scaduti as $contratto) 
{
	// Teleassistenza
	if($contratto['tipo'] == 4)
	{
		$id_cart_ta = Db::getInstance()->getValue('SELECT id_cart FROM '._DB_PREFIX_.'cart WHERE name = "Rinnovo assistenza '.$contratto['id_contratto'].' anno '.date('Y').'" AND id_customer = '.$contratto['id_customer']);
		
		if($id_cart_ta > 0) {
			// niente
		}	
		else
		{
			$indirizzo = Db::getInstance()->getValue('SELECT id_address FROM '._DB_PREFIX_.'address WHERE id_customer = '.$contratto['id_customer'].' AND active = 1 AND deleted = 0 AND fatturazione = 1');
			
			$notifiche = array();
			$notifica = array();
			$notifica['email'] = $contratto['email'];
			$notifica['data'] = date("Y-m-d H:i:s");
			$notifica['tipo'] = 'Automatica';
			
			$notifiche[] = $notifica;
			$notifiche = serialize($notifiche);
			
			Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'cart (id_cart, id_lang, id_currency, id_customer, id_address_invoice, id_address_delivery, name, validita, date_add, date_upd, created_by,  id_employee, in_carico_a, preventivo, note, numero_notifiche,notifiche,provvisorio) VALUES (NULL, 5, 1, "'.$contratto['id_customer'].'", "'.$indirizzo.'", "'.$indirizzo.'", "Rinnovo assistenza '.$contratto['id_contratto'].' anno '.date('Y').'", "'.$contratto['data_fine'].'", "'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'", 14,14,14,1, "'.$nota_ta.'",1,"'.addslashes($notifiche).'",1)');

			$id_cart = mysqli_insert_id();
			
			$prodotti_per_carrello = Db::getInstance()->executeS('SELECT id_product, prezzo, quantita FROM '._DB_PREFIX_.'contratto_assistenza_prodotti WHERE id_contratto = '.$contratto['id_contratto'].'');
			
			$prezzo_prod = $contratto['prezzo'] + (($contratto['prezzo']/100) * 3);
			$prezzo_acquisto = (($prezzo_prod/100) * 60);
			
			Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'cart_product (id_cart, id_product, quantity, price, free, prezzo_acquisto, date_add) VALUES ('.$id_cart.', 335058, "1", "'.$prezzo_prod.'", "0", "'.$prezzo_acquisto.'",  "'.date('Y-m-d H:i:s').'")') ;
			
			$id_cart_ta = $id_cart;
			
			$action_subject = "*** PROMEMORIA AUTOMATICO - CONTRATTO ".$contratto['id_contratto']." (".date('Y').") IN SCADENZA SU ".($contratto['is_company'] == 1 ? $contratto2['company'] : $contratto['firstname']." ".$contratto['lastname'])."** ";
			
			$action = Db::getInstance()->getRow('
				SELECT at.id_action, at.action_from, at.action_to 
				FROM action_message am 
				JOIN action_thread at ON at.id_action = am.id_action 
				WHERE at.id_customer = '.$contratto['id_customer'].' 
					AND subject LIKE "%'.$action_subject.'%"
			');
	
			if($action['id_action'] > 0) {
				//Db::getInstance()->execute("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, ".$action['id_action'].", '".$contratto2['id_customer']."', 99, '".$action['action_from']."', '".$action['action_to']."', '', 'Inviata notifica al cliente:<br /><br />".($testo_promemoria != '' ? addslashes($testo_promemoria) : addslashes($testo_promemoria))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
			}	
			else
			{
				$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
				$action_thread++;
			
				$id_employee = 14; // Valentina
				$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').'1');

				Db::getInstance()->execute("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd, promemoria) VALUES ('$action_thread', 'Attivita', '".$contratto2['id_customer']."', 'Verifica preventivo contratto', 0, ".$id_employee.", 'open', '', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."', 1)");   
				Db::getInstance()->execute("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', '".$contratto2['id_customer']."', 99, '".$id_employee."', '".$id_employee."', '', '<strong>*** VERIFICA PREVENTIVO CONTRATTO IN SCADENZA ***</strong><br /><br />Il cliente ha in scadenza il <a href=\"https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$contratto2['id_customer']."&viewcustomer&id_contratto=".$contratto2['id_contratto']."&token=".$tokenCustomers."&tab-container-1=13\" target=\"_blank\">contratto n. ".$contratto2['id_contratto']."</a>. Verificare il preventivo del contratto.', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')"); 
				// Con href 1.6 -> correggere: dovremmo fare un update degli href nel db oppure fare un redirect al link giusto nei controller
				// Db::getInstance()->execute("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', '".$contratto2['id_customer']."', 99, '".$id_employee."', '".$id_employee."', '', '<strong>*** VERIFICA PREVENTIVO CONTRATTO IN SCADENZA ***</strong><br /><br />Il cliente ha in scadenza il <a href=\"https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".$contratto2['id_customer']."&viewcustomer&tab_name=contracts&azione=contratto_view&id_contratto=".$contratto2['id_contratto']."&token=".$tokenCustomers."\" target=\"_blank\">contratto n. ".$contratto2['id_contratto']."</a>. Verificare il preventivo del contratto.', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
			}
			
		}
	}	
	else
	{
		$id_cart_ta = Db::getInstance()->getValue('SELECT id_cart FROM '._DB_PREFIX_.'cart WHERE name = "Rinnovo contratto n. '.$contratto['id_contratto'].' anno '.date('Y').'" AND id_customer = '.$contratto['id_customer']);
		
		if($id_cart_ta > 0) {
			// niente
		}	
		else
		{
			$indirizzo = Db::getInstance()->getValue('SELECT id_address FROM '._DB_PREFIX_.'address WHERE id_customer = '.$contratto['id_customer'].' AND active = 1 AND deleted = 0 AND fatturazione = 1');
			
			$notifiche = array();
			$notifica = array();
			$notifica['email'] = $contratto['email'];
			$notifica['data'] = date("Y-m-d H:i:s");
			$notifica['tipo'] = 'Automatica';
			
			$notifiche[] = $notifica;
			$notifiche = serialize($notifiche);
			
			Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'cart (id_cart, id_lang, id_currency, id_customer, id_address_invoice, id_address_delivery, name, validita, date_add, date_upd, created_by,  id_employee, in_carico_a, preventivo, note, numero_notifiche, notifiche,provvisorio) VALUES (NULL, 5, 1, "'.$contratto['id_customer'].'", "'.$indirizzo.'", "'.$indirizzo.'", "Rinnovo contratto n. '.$contratto['id_contratto'].' anno '.date('Y').'", "'.$contratto['data_fine'].'", "'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'", 14,14,14,1, "'.$nota_ta.'",1,"'.addslashes($notifiche).'",1)');

			$id_cart = mysqli_insert_id();

			$prodotti_per_carrello = Db::getInstance()->executeS('SELECT id_product, prezzo, quantita FROM '._DB_PREFIX_.'contratto_assistenza_prodotti WHERE id_contratto = '.$contratto['id_contratto'].'');
			
			$prezzo_prod = $contratto['prezzo'];
			$prezzo_acquisto = (($prezzo_prod/100) * 60);
			
			Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'cart_product (id_cart, id_product, quantity, price, free, prezzo_acquisto, date_add) VALUES ('.$id_cart.', 336198, "1", "'.$prezzo_prod.'", "0", "'.$prezzo_acquisto.'",  "'.date('Y-m-d H:i:s').'")') ;
			
			$id_cart_ta = $id_cart;
			
			$action_subject = "*** PROMEMORIA AUTOMATICO - CONTRATTO ".$contratto['id_contratto']." (".date('Y').") IN SCADENZA SU ".($contratto['is_company'] == 1 ? $contratto['company'] : $contratto['firstname']." ".$contratto['lastname'])."** ";
			
			$action = Db::getInstance()->getRow('
				SELECT at.id_action, at.action_from, at.action_to 
				FROM action_message am 
				JOIN action_thread at ON at.id_action = am.id_action 
				WHERE at.id_customer = '.$contratto['id_customer'].' 
					AND subject LIKE "%'.$action_subject.'%"
			');
	
			if($action['id_action'] > 0) {
				//Db::getInstance()->execute("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, ".$action['id_action'].", '".$contratto2['id_customer']."', 99, '".$action['action_from']."', '".$action['action_to']."', '', 'Inviata notifica al cliente:<br /><br />".($testo_promemoria != '' ? addslashes($testo_promemoria) : addslashes($testo_promemoria))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
			}	
			else
			{
				$action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
				$action_thread++;
				
				$id_employee = 14; // Valentina
				$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').'1');

				Db::getInstance()->execute("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd, promemoria) VALUES ('$action_thread', 'Attivita', '".$contratto2['id_customer']."', 'Verifica preventivo contratto', 0, ".$id_employee.", 'open', '', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."', 1)");   
				Db::getInstance()->execute("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', '".$contratto2['id_customer']."', 99, '".$id_employee."', '".$id_employee."', '', '<strong>*** VERIFICA PREVENTIVO CONTRATTO IN SCADENZA ***</strong><br /><br />Il cliente ha in scadenza il <a href=\"https://www.ezdirect.it/ezadmin/index.php?tab=AdminCustomers&id_customer=".$contratto2['id_customer']."&viewcustomer&id_contratto=".$contratto2['id_contratto']."&token=".$tokenCustomers."&tab-container-1=13\" target=\"_blank\">contratto n. ".$contratto2['id_contratto']."</a>. Verificare il preventivo del contratto.', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");
				// Con href 1.6 -> correggere: dovremmo fare un update degli href nel db oppure fare un redirect al link giusto nei controller
				// Db::getInstance()->execute("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', '".$contratto2['id_customer']."', 99, '".$id_employee."', '".$id_employee."', '', '<strong>*** VERIFICA PREVENTIVO CONTRATTO IN SCADENZA ***</strong><br /><br />Il cliente ha in scadenza il <a href=\"https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".$contratto2['id_customer']."&viewcustomer&tab_name=contracts&azione=contratto_view&id_contratto=".$contratto2['id_contratto']."&token=".$tokenCustomers."\" target=\"_blank\">contratto n. ".$contratto2['id_contratto']."</a>. Verificare il preventivo del contratto.', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
			}
		}
	}
}

?>