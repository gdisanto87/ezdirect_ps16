	
	
	tinyMCE.init({
	
setup : function (theEditor) {
	
		
		 theEditor.onInit.add(function(ed) {
			 ed.getBody().style.fontSize = '12px';
			console.debug('Editor is done: ' + ed.id);
			if(ed.id == 'note_private') {
				var body = tinyMCE.get("note_private").getContent();
				var dom = tinymce.get("note_private").dom;
				var pElements = dom.select('p'); // Default scope is the editor document.
				dom.setStyle(pElements, 'color', 'red');
			
			}
			
		});
		
		theEditor.onChange.add(function (ed) {
				if(ed.id == 'description_5') {
					var testo = tinymce.get("description_5").getContent();
					attivaSEO(testo);
				}
            }
        );
		
		theEditor.onClick.add(function (ed) {
				if(ed.id == 'description_5') {
					var testo = tinymce.get("description_5").getContent();
					attivaSEO(testo);
				}
            }
        );
		
        theEditor.onKeyPress.add(function (ed) {
                previewContent();
				if(ed.id == 'description_5') {
					var testo = tinymce.get("description_5").getContent();
					attivaSEO(testo);
				}
            }
        );
    },

		mode : "textareas",
		theme : "advanced",
		editor_selector : "rte",
		editor_deselector : "noEditor",
		//plugins : "safari,pagebreak,style,table,advimage,advlink,inlinepopups,media,contextmenu,paste,fullscreen,xhtmlxtras,preview",
		// Theme options
		/*theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "styleprops,|,cite,abbr,acronym,del,ins,attribs,pagebreak",*/
		content_style: ".mce-content-body {font-size:15px;font-family:Arial,sans-serif;}",
		
		    theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px,20px",
    font_size_style_values: "12px,13px,14px,16px,18px,20px",
		  // plugins : "emotions,iespell,media,advhr,advimage,advlink,advlist,autolink,autosave,contextmenu,directionality,fullscreen,inlinepopups,insertdatetime,layer,legacyoutput,lists,media,nonbreaking,noneditable,pagebreak,paste,preview,print,save,searchreplace,spellchecker,style,tabfocus,table,visualchars,wordcount",
		  plugins : "iespell, autolink, scayt,spellchecker,safari,pagebreak,style,table,advimage,advlink,inlinepopups,media,paste,fullscreen,xhtmlxtras,preview",
		   language:"it",spellchecker_languages:"+Italian=it,English=en,Danish=da,Dutch=nl,Finnish=fi,French=fr,German=de,Polish=pl,Portuguese=pt,Spanish=es,Swedish=sv",theme_advanced_toolbar_location:"top",theme_advanced_toolbar_align:"left",theme_advanced_statusbar_location:"bottom",theme_advanced_resizing:true,theme_advanced_resize_horizontal:true,dialog_type:"modal",
		  
		   //theme_advanced_buttons1:"bold,italic,strikethrough,|,bullist,numlist,blockquote,|,justifyleft,justifycenter,justifyright,|,link,unlink,image,media|image,code,spellchecker,separator,downloadmanager",theme_advanced_buttons2:"formatselect,underline,justifyfull,forecolor,|,pastetext,pasteword,removeformat,|,charmap,|,outdent,indent,|,undo,redo",theme_advanced_buttons3:"",theme_advanced_buttons4:"",
		  
		
		
		
		
		
		
		theme_advanced_buttons1:"bold,italic,strikethrough,|,table,bullist,numlist,|,justifyleft,justifycenter,justifyright,|,link,unlink,image,|image,media,code,spellchecker,iespell,separator,downloadmanager,formatselect,fontselect,fontsizeselect,underline,justifyfull,forecolor",theme_advanced_buttons3:"",theme_advanced_buttons4:"",
  link_class_list: [
   {title: "--", value: ""},
    {title: "Link per aprire form contattaci", value: "apri-contattaci"},
  ],
		//content_css : pathCSS+"tinymce_link.css",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		browser_spellcheck : true,
		gecko_spellcheck: true,
		document_base_url : ad,
		/*width: "600",
		height: "400",
		height: "auto",*/
		font_size_style_values : "8pt, 10pt, 12pt, 14pt, 18pt, 24pt, 36pt",
		elements : "nourlconvert,ajaxfilemanager",
		file_browser_callback : "ajaxfilemanager",
		entity_encoding: "raw",
		convert_urls : false
		
	});
	
		
		
	function ajaxfilemanager(field_name, url, type, win) {
		var ajaxfilemanagerurl = "https://www.ezdirect.it/ezadmin/ajaxfilemanager/ajaxfilemanager.php";
		switch (type) {
			case "image":
				break;
			case "media":
				break;
			case "flash": 
				break;
			case "file":
				break;
			default:
				return false;
	}
    tinyMCE.activeEditor.windowManager.open({
        url: ajaxfilemanagerurl,
        width: 782,
        height: 440,
        inline : "yes",
        close_previous : "no"
    },{
        window : win,
        input : field_name
    });
}

function previewContent(){
     changes = true;
}

					
