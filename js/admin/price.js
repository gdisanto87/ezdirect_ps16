/*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

function getTax()
{
	if (noTax)
		return 0;

	var selectedTax = document.getElementById('id_tax_rules_group');
	var taxId = selectedTax.options[selectedTax.selectedIndex].value;
	return taxesArray[taxId].rates[0];
}

function getTaxes()
{
	if (noTax)
		taxesArray[taxId];

	var selectedTax = document.getElementById('id_tax_rules_group');
	var taxId = selectedTax.options[selectedTax.selectedIndex].value;
	return taxesArray[taxId];
}

function addTaxes(price)
{
	var taxes = getTaxes();
	var price_with_taxes = price;
	if (taxes.computation_method == 0) {
		for (i in taxes.rates) {
			price_with_taxes *= (1 + taxes.rates[i] / 100);
			break;
		}
	}
	else if (taxes.computation_method == 1) {
		var rate = 0;
		for (i in taxes.rates) {
			 rate += taxes.rates[i];
		}
		price_with_taxes *= (1 + rate / 100);
	}
	else if (taxes.computation_method == 2) {
		for (i in taxes.rates) {
			price_with_taxes *= (1 + taxes.rates[i] / 100);
		}
	}

	return price_with_taxes;
}

function removeTaxes(price)
{
	var taxes = getTaxes();
	var price_without_taxes = price;
	if (taxes.computation_method == 0) {
		for (i in taxes.rates) {
			price_without_taxes /= (1 + taxes.rates[i] / 100);
			break;
		}
	}
	else if (taxes.computation_method == 1) {
		var rate = 0;
		for (i in taxes.rates) {
			 rate += taxes.rates[i];
		}
		price_without_taxes /= (1 + rate / 100);
	}
	else if (taxes.computation_method == 2) {
		for (i in taxes.rates) {
			price_without_taxes /= (1 + taxes.rates[i] / 100);
		}
	}

	return price_without_taxes;
}

function getEcotaxTaxIncluded()
{
	return ps_round(ecotax_tax_excl * (1 + ecotaxTaxRate), 2);
}

function getEcotaxTaxExcluded()
{
	return ecotax_tax_excl;
}

function formatPrice(price)
{
	var fixedToSix = (Math.round(price * 1000000) / 1000000);
	return (Math.round(fixedToSix) == fixedToSix + 0.000001 ? fixedToSix + 0.000001 : fixedToSix);
}

function calcPrice()
{
	var priceType = $('#priceType').val();
	if (priceType == 'TE')
		calcPriceTI();
	else
		calcPriceTE();
}

function calcPriceTI()
{
	//var priceTE = parseFloat(document.getElementById('priceTEReal').value.replace(/,/g, '.'));
	var priceTE = parseFloat(document.getElementById('priceTE').value.replace(/,/g, '.'));
	var newPrice = addTaxes(priceTE);

	document.getElementById('priceTI').value = (isNaN(newPrice) == true || newPrice < 0) ? '' :
		ps_round(newPrice, priceDisplayPrecision);
	document.getElementById('finalPrice').innerHTML = (isNaN(newPrice) == true || newPrice < 0) ? '' :
		ps_round(newPrice, priceDisplayPrecision).toFixed(priceDisplayPrecision);
	document.getElementById('finalPriceWithoutTax').innerHTML = (isNaN(priceTE) == true || priceTE < 0) ? '' :
		(ps_round(priceTE, 6)).toFixed(6);
	calcReduction();

	if (isNaN(parseFloat($('#priceTI').val())))
	{
		$('#priceTI').val('');
		$('#finalPrice').html('');
	}
	else
	{
		$('#priceTI').val((parseFloat($('#priceTI').val()) + getEcotaxTaxIncluded()).toFixed(priceDisplayPrecision));
		$('#finalPrice').html(parseFloat($('#priceTI').val()).toFixed(priceDisplayPrecision));
	}
}

function calcPriceTE()
{
	ecotax_tax_excl =  $('#ecotax').val() / (1 + ecotaxTaxRate);
	var priceTI = parseFloat(document.getElementById('priceTI').value.replace(/,/g, '.'));
	var newPrice = removeTaxes(ps_round(priceTI - getEcotaxTaxIncluded(), priceDisplayPrecision));
	document.getElementById('priceTE').value = (isNaN(newPrice) == true || newPrice < 0) ? '' :
		ps_round(newPrice, 6).toFixed(6);
	document.getElementById('priceTEReal').value = (isNaN(newPrice) == true || newPrice < 0) ? 0 : ps_round(newPrice, 9);
	document.getElementById('finalPrice').innerHTML = (isNaN(newPrice) == true || newPrice < 0) ? '' :
		ps_round(priceTI, priceDisplayPrecision).toFixed(priceDisplayPrecision);
	document.getElementById('finalPriceWithoutTax').innerHTML = (isNaN(newPrice) == true || newPrice < 0) ? '' :
		(ps_round(newPrice, 6)).toFixed(6);
	calcReduction();
}

function calcImpactPriceTI()
{
	var priceTE = parseFloat(document.getElementById('attribute_priceTEReal').value.replace(/,/g, '.'));
	var newPrice = addTaxes(priceTE);
	$('#attribute_priceTI').val((isNaN(newPrice) == true || newPrice < 0) ? '' : ps_round(newPrice, priceDisplayPrecision).toFixed(priceDisplayPrecision));
	var total = ps_round((parseFloat($('#attribute_priceTI').val()) * parseInt($('#attribute_price_impact').val()) + parseFloat($('#finalPrice').html())), priceDisplayPrecision);
	if (isNaN(total) || total < 0)
		$('#attribute_new_total_price').html('0.00');
	else
		$('#attribute_new_total_price').html(total);
}

function calcImpactPriceTE()
{
	var priceTI = parseFloat(document.getElementById('attribute_priceTI').value.replace(/,/g, '.'));
	priceTI = (isNaN(priceTI)) ? 0 : ps_round(priceTI);
	var newPrice = removeTaxes(ps_round(priceTI, priceDisplayPrecision));
	$('#attribute_price').val((isNaN(newPrice) == true || newPrice < 0) ? '' : ps_round(newPrice, 6).toFixed(6));
	$('#attribute_priceTEReal').val((isNaN(newPrice) == true || newPrice < 0) ? 0 : ps_round(newPrice, 9));
	var total = ps_round((parseFloat($('#attribute_priceTI').val()) * parseInt($('#attribute_price_impact').val()) + parseFloat($('#finalPrice').html())), priceDisplayPrecision);
	if (isNaN(total) || total < 0)
		$('#attribute_new_total_price').html('0.00');
	else
		$('#attribute_new_total_price').html(total);
}

function calcReduction()
{
	if (parseFloat($('#reduction_price').val()) > 0)
		reductionPrice();
	else if (parseFloat($('#reduction_percent').val()) > 0)
		reductionPercent();
}

function reductionPrice()
{
	var price    = document.getElementById('priceTI');
	var priceWhithoutTaxes = document.getElementById('priceTE');
	var newprice = document.getElementById('finalPrice');
	var newpriceWithoutTax = document.getElementById('finalPriceWithoutTax');
	var curPrice = price.value;

	document.getElementById('reduction_percent').value = 0;
	if (isInReductionPeriod())
	{
		var rprice = document.getElementById('reduction_price');
		if (parseFloat(curPrice) <= parseFloat(rprice.value))
			rprice.value = curPrice;
		if (parseFloat(rprice.value) < 0 || isNaN(parseFloat(curPrice)))
			rprice.value = 0;
		curPrice = curPrice - rprice.value;
	}

	newprice.innerHTML = (ps_round(parseFloat(curPrice), priceDisplayPrecision) + getEcotaxTaxIncluded()).toFixed(priceDisplayPrecision);
	var rpriceWithoutTaxes = ps_round(removeTaxes(rprice.value), priceDisplayPrecision);
	newpriceWithoutTax.innerHTML = ps_round(priceWhithoutTaxes.value - rpriceWithoutTaxes, priceDisplayPrecision).toFixed(priceDisplayPrecision);
}

function reductionPercent()
{
	var price    = document.getElementById('priceTI');
	var newprice = document.getElementById('finalPrice');
	var newpriceWithoutTax = document.getElementById('finalPriceWithoutTax');
	var curPrice = price.value;

	document.getElementById('reduction_price').value = 0;
	if (isInReductionPeriod())
	{
		var newprice = document.getElementById('finalPrice');
		var rpercent = document.getElementById('reduction_percent');

		if (parseFloat(rpercent.value) >= 100)
			rpercent.value = 100;
		if (parseFloat(rpercent.value) < 0)
			rpercent.value = 0;
		curPrice = price.value * (1 - (rpercent.value / 100));
	}

	newprice.innerHTML = (ps_round(parseFloat(curPrice), priceDisplayPrecision) + getEcotaxTaxIncluded()).toFixed(priceDisplayPrecision);
	newpriceWithoutTax.innerHTML = ps_round(parseFloat(removeTaxes(ps_round(curPrice, priceDisplayPrecision))), priceDisplayPrecision).toFixed(priceDisplayPrecision);
}

function isInReductionPeriod()
{
	var start  = document.getElementById('reduction_from').value;
	var end    = document.getElementById('reduction_to').value;

	if (start == end && start != "" && start != "0000-00-00 00:00:00") return true;

	var sdate  = new Date(start.replace(/-/g,'/'));
	var edate  = new Date(end.replace(/-/g,'/'));
	var today  = new Date();

	return (sdate <= today && edate >= today);
}

function decimalTruncate(source, decimals)
{
	if (typeof(decimals) == 'undefined')
		decimals = 6;
	source = source.toString();
	var pos = source.indexOf('.');
	return parseFloat(source.substr(0, pos + decimals + 1));
}

function unitPriceWithTax(type)
{
	var priceWithTax = parseFloat(document.getElementById(type+'_price').value.replace(/,/g, '.'));
	var newPrice = addTaxes(priceWithTax);
	$('#'+type+'_price_with_tax').html((isNaN(newPrice) == true || newPrice < 0) ? '0.00' : ps_round(newPrice, priceDisplayPrecision).toFixed(priceDisplayPrecision));
}

function unitySecond()
{
	$('#unity_second').html($('#unity').val());
	if ($('#unity').get(0).value.length > 0)
	{
		$('#unity_third').html($('#unity').val());
		$('#tr_unit_impact').show();
	}
	else
		$('#tr_unit_impact').hide();
}

function changeCurrencySpecificPrice(index)
{
	var id_currency = $('#spm_currency_' + index).val();
	if (id_currency > 0)
		$('#sp_reduction_type option[value="amount"]').text($('#spm_currency_' + index + ' option[value= ' + id_currency + ']').text());
	else if (typeof currencyName !== 'undefined')
		$('#sp_reduction_type option[value="amount"]').text(currencyName);

	if (currencies[id_currency]["format"] == 2 || currencies[id_currency]["format"] == 4)
	{
		$('#spm_currency_sign_pre_' + index).html('');
		$('#spm_currency_sign_post_' + index).html(' ' + currencies[id_currency]["sign"]);
	}
	else if (currencies[id_currency]["format"] == 1 || currencies[id_currency]["format"] == 3)
	{
		$('#spm_currency_sign_post_' + index).html('');
		$('#spm_currency_sign_pre_' + index).html(currencies[id_currency]["sign"] + ' ');
	}
}


/* OVERRIDE */

function calcMarginalita()
{
	// Verificare se viene usata in altre pagine oltre alla scheda prodotto: ('prezzo_spec_acquisto') non esisteva
	var priceListino = parseFloat(document.getElementById('wholesale_price').value.replace(/,/g, '.'));
	var priceOk = priceListino;

	if(document.getElementById('prezzo_spec_acquisto') !== null){
		var priceSpeciale = parseFloat(document.getElementById('prezzo_spec_acquisto').value.replace(/,/g, '.'));
		
		if(!isNaN(priceSpeciale))
			priceOk = priceSpeciale;
	}
	
	var priceTE = parseFloat(document.getElementById('priceTE').value.replace(/,/g, '.'));
	
	if(priceOk == "" || isNaN(priceOk))
		priceOk = 0;
		
	if(priceTE == "" || isNaN(priceTE))
		priceTE = 0;
		
	var newPrice = (((priceTE - priceOk)*100) / priceTE);
	
	document.getElementById('marginalita').value = (isNaN(newPrice) == true || newPrice < 0) ? '' :
		ps_round(newPrice, 2);

	calcReduction();

	// Correggere: tentativo di mostrare 0 al posto di NaN nel calcolo del margine; non funziona
	//if(!isNaN(parseFloat(document.getElementById('marginalita').value)))
		document.getElementById('marginalita').value = parseFloat(document.getElementById('marginalita').value) + getEcotaxTaxIncluded();
	//else
		//document.getElementById('marginalita').value = parseFloat("0.00");
}

function calcMarginalitaSconto(sc_riv, margin_riv, list_type)
{
	// Verificare se viene usata in altre pagine oltre alla scheda prodotto: ('prezzo_spec_acquisto') non esisteva
	var priceListino = parseFloat(document.getElementById('wholesale_price').value.replace(/,/g, '.'));
	var priceOk = priceListino;

	if(document.getElementById('prezzo_spec_acquisto') !== null){
		var priceSpeciale = parseFloat(document.getElementById('prezzo_spec_acquisto').value.replace(/,/g, '.'));
		
		if(!isNaN(priceSpeciale))
			priceOk = priceSpeciale;
	}

	if(priceOk == "" || isNaN(priceOk))
		priceOk = 0;

	var sconto = parseFloat(document.getElementById(sc_riv).value.replace(/,/g, '.'));
	
	if(sconto == "" || !Number(sconto))
		sconto = 0;
		
	var priceTE = parseFloat(document.getElementById(list_type).value.replace(/,/g, '.'));
	
	if(priceTE == "" || !Number(priceTE))
		priceTE = 0;
		
	priceTE = priceTE - ((priceTE * sconto) / 100);

	if(priceTE < 0)
		var newPrice = 0;
	else
		var newPrice = (((priceTE - priceOk) * 100) / priceTE);

	if(newPrice < 0 || !Number(newPrice))
		newPrice = 0;
	
	document.getElementById(margin_riv).value = ps_round(newPrice, 2);

	calcReduction();
	document.getElementById(margin_riv).value = parseFloat(document.getElementById(margin_riv).value) + getEcotaxTaxIncluded();
}

// calcMarginalitaSconto calcolata sul prezzo di acquisto più basso (miglior prezzo)
function calcMarginalitaSconto2(sc_riv, margin_riv, list_type)
{
	var priceListino = parseFloat(document.getElementById('wholesale_price').value.replace(/,/g, '.'));
	//var migliorPrezzo = parseFloat(document.getElementById('miglior_prezzo_acquisto').value.replace(/,/g, '.'));

	// Modifica 19/07/2021: margine calcolato sul prezzo di acquiato speciale invece che sul miglior prezzo di acquisto
	if(document.getElementById('prezzo_spec_acquisto') !== null){
		var priceSpeciale = parseFloat(document.getElementById('prezzo_spec_acquisto').value.replace(/,/g, '.'));
		
		if(!isNaN(priceSpeciale))
			priceListino = priceSpeciale;
	}
	
	if(priceListino == "" || !Number(priceListino))
		priceListino = 0;
	
	/*if(migliorPrezzo != 0 && migliorPrezzo < priceListino)
		priceListino = migliorPrezzo;*/

	//console.log("priceListino = " + priceListino);

	var sconto = parseFloat(document.getElementById(sc_riv).value.replace(/,/g, '.'));
	
	if(sconto == "" || !Number(sconto))
		sconto = 0;
	
	//console.log("sconto = " + sconto);

	var priceTE = parseFloat(document.getElementById(list_type).value.replace(/,/g, '.'));

	//console.log("priceTE 1 = " + priceTE);
	
	if(priceTE == "" || !Number(priceTE))
		priceTE = 0;
		
	//console.log("priceTE 2 = " + priceTE);

	priceTE = priceTE - ((priceTE * sconto) / 100);

	//console.log("priceTE 3 = " + priceTE);

	if(priceTE < 0)
		var newPrice = 0;
	else
		var newPrice = (((priceTE - priceListino) * 100) / priceTE);

	//console.log("newPrice 1 = " + newPrice);

	if(newPrice < 0 || !Number(newPrice))
		newPrice = 0;

	//console.log("newPrice 2 = " + newPrice);
	
	document.getElementById(margin_riv).value = ps_round(newPrice, 2);

	//console.log("margin_riv = " + document.getElementById(margin_riv).value);

	calcReduction();
	document.getElementById(margin_riv).value = parseFloat(document.getElementById(margin_riv).value) + getEcotaxTaxIncluded();
}

function aggiornaScontisticaAmazon()
{
	var priceTE = parseFloat(document.getElementById('priceTE').value.replace(/,/g, '.'));
	
	var migliorPrezzo = parseFloat(document.getElementById('miglior_prezzo_appoggio').value.replace(/,/g, '.'));
	
	if(!migliorPrezzo || migliorPrezzo == 0)
		migliorPrezzo = priceTE;
	
	if(priceTE < migliorPrezzo)
		document.getElementById('miglior_prezzo').value = priceTE;
	else
		document.getElementById('miglior_prezzo').value = migliorPrezzo;
	
	//document.getElementById('miglior_prezzo').value = priceTE;
	
	calcPrezzoSconto('sconto_amazon_it', 'prezzo_amazon_it', 'miglior_prezzo');
	calcPrezzoSconto('sconto_amazon_fr', 'prezzo_amazon_fr', 'miglior_prezzo');
	calcPrezzoSconto('sconto_amazon_es', 'prezzo_amazon_es', 'miglior_prezzo');
	calcPrezzoSconto('sconto_amazon_de', 'prezzo_amazon_de', 'miglior_prezzo');
	calcPrezzoSconto('sconto_amazon_uk', 'prezzo_amazon_uk', 'miglior_prezzo');
	
	calcMarginalitaSconto('sconto_amazon_it', 'margin_amazon_it', 'miglior_prezzo');
	calcMarginalitaSconto('sconto_amazon_fr', 'margin_amazon_fr', 'miglior_prezzo');
	calcMarginalitaSconto('sconto_amazon_es', 'margin_amazon_es', 'miglior_prezzo');
	calcMarginalitaSconto('sconto_amazon_de', 'margin_amazon_de', 'miglior_prezzo');
	calcMarginalitaSconto('sconto_amazon_uk', 'margin_amazon_uk', 'miglior_prezzo');
}

function calcPrezzoSconto(sc_riv, prezzo_riv, list_type)
{
	var sconto = parseFloat(document.getElementById(sc_riv).value.replace(/,/g, '.'));
	
	if(sconto == "" || !Number(sconto))
		sconto = 0;
		
	var priceTE = parseFloat(document.getElementById(list_type).value.replace(/,/g, '.'));
	
	if(priceTE == "" || !Number(priceTE))
		priceTE = 0;
		
	var newPrice = priceTE = priceTE - ((priceTE * sconto) / 100);
	
	document.getElementById(prezzo_riv).value = (!Number(newPrice) == true ) ? 0 : ps_round(newPrice, 2);

	calcReduction();
	document.getElementById(prezzo_riv).value = parseFloat(document.getElementById(prezzo_riv).value) + getEcotaxTaxIncluded();
}

function calcPrezzoFromMargine(acquisto, margine, dest)
{
	var acquisto = parseFloat(document.getElementById(acquisto).value.replace(/,/g, '.'));
	var margine = parseFloat(document.getElementById(margine).value.replace(/,/g, '.'));
	
	var finale = (acquisto * 100) / (100 - margine);
	
	document.getElementById(dest).value = (!Number(finale) == true ) ? 0 : ps_round(finale, 2);
}

function calcPrezzoScontoHTML(sconto, prezzo, visualizzazione)
{
	var sconto = parseFloat(document.getElementById(sconto).value.replace(/,/g, '.'));
	
	if(sconto == "" || isNaN(sconto))
		sconto = 0;
		
	var prezzo = parseFloat(document.getElementById(prezzo).value.replace(/,/g, '.'));
	
	if(prezzo == "" || isNaN(prezzo))
		prezzo = 0;
		
	var nuovoPrezzo = prezzo - ((prezzo * sconto) / 100);

	document.getElementById(visualizzazione).innerHTML = nuovoPrezzo;
}

function calcSconto(partenza, fine, sconto)
{
	var partenza = parseFloat(document.getElementById(partenza).value.replace(/,/g, '.'));
	
	if(partenza == "" || !Number(partenza))
		partenza = 0;
		
	var fine = parseFloat(document.getElementById(fine).value.replace(/,/g, '.'));
	
	if(fine == "" || !Number(fine))
		fine = 0;
		
	var newPrice = ((partenza - fine) * 100) / partenza;
	
	document.getElementById(sconto).value = ((!Number(newPrice) == true ) ? 0 : ps_round(newPrice, 2));
	document.getElementById(sconto).innerHTML = parseFloat(document.getElementById(sconto).value);
}

// Calcola netto esolver = listino - sconti acquisto
function calcScontoAcquisto()
{
	var acq1 = parseFloat(document.getElementById('sconto_acquisto_1').value.replace(/,/g, '.'));
	var acq2 = parseFloat(document.getElementById('sconto_acquisto_2').value.replace(/,/g, '.'));
	var acq3 = parseFloat(document.getElementById('sconto_acquisto_3').value.replace(/,/g, '.'));
	
	if(acq1 == "" || isNaN(acq1))
		acq1 = 0;
		
	if(acq2 == "" || isNaN(acq2))
		acq2 = 0;
		
	if(acq3 == "" || isNaN(acq3))
		acq3 = 0;
		
	var listino = parseFloat(document.getElementById('listino').value.replace(/,/g, '.'));
	
	if(listino == "" || isNaN(listino))
		listino = 0;
		
	var newPrice = (listino * (100 - acq1) / 100) * ((100 - acq2)/100) * ((100 - acq3)/100);
	
	document.getElementById('wholesale_price_esolver').value = ((isNaN(newPrice) == true || newPrice < 0) ? '' : ps_round(newPrice, 2));
	document.getElementById('wholesale_price_esolver').innerHTML = parseFloat(document.getElementById('wholesale_price_esolver').value);
}

// Calcola acquisto netto = netto esolver - rebate
function calcRebate()
{
	var acq1 = parseFloat(document.getElementById('rebate_1').value.replace(/,/g, '.'));
	var acq2 = parseFloat(document.getElementById('rebate_2').value.replace(/,/g, '.'));
	var acq3 = parseFloat(document.getElementById('rebate_3').value.replace(/,/g, '.'));
	
	if(acq1 == "" || isNaN(acq1))
		acq1 = 0;
		
	if(acq2 == "" || isNaN(acq2))
		acq2 = 0;
		
	if(acq3 == "" || isNaN(acq3))
		acq3 = 0;
		
	var netto_esolver = parseFloat(document.getElementById('wholesale_price_esolver').value.replace(/,/g, '.'));
	
	if(netto_esolver == "" || isNaN(netto_esolver))
		netto_esolver = 0;
		
	var newPrice = (netto_esolver * (100 - acq1) / 100) * ((100 - acq2)/100) * ((100 - acq3)/100);
	
	document.getElementById('wholesale_price').value = ((isNaN(newPrice) == true || newPrice < 0) ? '' : ps_round(newPrice, 2));
	document.getElementById('wholesale_price').innerHTML = parseFloat(document.getElementById('wholesale_price').value);
}

// Correggere: eliminare o usare al posto di calcScontoAcquisto e calcRebate (ottimizzare aggiungendo il caso all per eseguire entrambe le operazioni)
function calcScontiAcquisto(acq1, acq2, acq3, listino, acquisto)
{
	var acq1 = parseFloat(document.getElementById(acq1).value.replace(/,/g, '.'));
	var acq2 = parseFloat(document.getElementById(acq2).value.replace(/,/g, '.'));
	var acq3 = parseFloat(document.getElementById(acq3).value.replace(/,/g, '.'));
	
	if(acq1 == "" || isNaN(acq1))
		acq1 = 0;
		
	if(acq2 == "" || isNaN(acq2))
		acq2 = 0;
		
	if(acq3 == "" || isNaN(acq3))
		acq3 = 0;
		
	var listino = parseFloat(document.getElementById(listino).value.replace(/,/g, '.'));
	
	if(listino == "" || isNaN(listino))
		listino = 0;
		
	var newPrice = (listino * (100 - acq1) / 100) * ((100 - acq2)/100) * ((100 - acq3)/100);
	
	document.getElementById(acquisto).value = (isNaN(newPrice) == true || newPrice < 0) ? '' :
		ps_round(newPrice, 2);

	document.getElementById(acquisto).innerHTML = parseFloat(document.getElementById(acquisto).value);
}

/*
function checkIfEmpty(y) 
{
    var x;
    x = y.value;
    if (x == "") {
        alert("Campo obbligatorio, inserisci un valore");
		window.setTimeout(function () { 
			y.focus(); 
		}, 0); 
    }
}
*/

function validate_form_product()
{
	if(document.getElementById('listino_esolver').value == '') {
		alert("Il campo listino eSolver è obbligatorio");
		document.getElementById('listino_esolver').scrollIntoView();
		return false;
	}
	else if(document.getElementById('sconto_acquisto_1').value == '') {
		alert("Il campo sconto acquisto 1 è obbligatorio");
		document.getElementById('sconto_acquisto_1').scrollIntoView();
		return false;
	}
	else if(document.getElementById('sconto_acquisto_2').value == '') {
		alert("Il campo sconto acquisto 2 è obbligatorio");
		document.getElementById('sconto_acquisto_2').scrollIntoView();
		return false;
	}
	else if(document.getElementById('sconto_acquisto_3').value == '') {
		alert("Il campo sconto acquisto 3 è obbligatorio");
		document.getElementById('sconto_acquisto_3').scrollIntoView();
		return false;
	}
	else if(document.getElementById('wholesale_price_esolver').value == '') {
		alert("Il campo acquisto netto eSolver è obbligatorio");
		document.getElementById('wholesale_price_esolver').scrollIntoView();
		return false;
	}
	else if(document.getElementById('scontolistinovendita').value == '') {
		alert("Il campo sconto clienti è obbligatorio");
		document.getElementById('scontolistinovendita').scrollIntoView();
		return false;
	}
	else if(document.getElementById('priceTE').value == '') {
		alert("Il campo prezzo di vendita è obbligatorio");
		document.getElementById('priceTE').scrollIntoView();
		return false;
	}
	else if(document.getElementById('rebate_1').value == '') {
		alert("Il campo rebate 1 è obbligatorio");
		document.getElementById('rebate_1').scrollIntoView();
		return false;
	}
	else if(document.getElementById('rebate_2').value == '') {
		alert("Il campo rebate 2 è obbligatorio");
		document.getElementById('rebate_2').scrollIntoView();
		return false;
	}
	else if(document.getElementById('rebate_3').value == '') {
		alert("Il campo rebate 3 è obbligatorio");
		document.getElementById('rebate_3').scrollIntoView();
		return false;
	}
	else if(document.getElementById('wholesale_price').value == '') {
		alert("Il campo acquisto netto è obbligatorio");
		document.getElementById('wholesale_price').scrollIntoView();
		return false;
	}
	else if(document.getElementById('sconto_quantita_1').value == '') {
		alert("Il campo sconto quantita 1 è obbligatorio");
		document.getElementById('sconto_quantita_1').scrollIntoView();
		return false;
	}
	else if(document.getElementById('prezzo_qta_1').value == '') {
		alert("Il campo prezzo quantita 1 è obbligatorio");
		document.getElementById('prezzo_qta_1').scrollIntoView();
		return false;
	}
	else if(document.getElementById('sconto_quantita_2').value == '') {
		alert("Il campo sconto quantita 2 è obbligatorio");
		document.getElementById('sconto_quantita_2').scrollIntoView();
		return false;
	}
	else if(document.getElementById('prezzo_qta_2').value == '') {
		alert("Il campo prezzo quantita 2 è obbligatorio");
		document.getElementById('prezzo_qta_2').scrollIntoView();
		return false;
	}
	else if(document.getElementById('sconto_quantita_3').value == '') {
		alert("Il campo sconto quantita 3 è obbligatorio");
		document.getElementById('sconto_quantita_3').scrollIntoView();
		return false;
	}
	else if(document.getElementById('prezzo_qta_3').value == '') {
		alert("Il campo prezzo quantita 3 è obbligatorio");
		document.getElementById('prezzo_qta_3').scrollIntoView();
		return false;
	}
	else if(document.getElementById('sconto_rivenditore_1').value == '') {
		alert("Il campo sconto rivenditore 1 è obbligatorio");
		document.getElementById('sconto_rivenditore_1').scrollIntoView();
		return false;
	}
	else if(document.getElementById('prezzo_riv_1').value == '') {
		alert("Il campo prezzo rivenditore 1 è obbligatorio");
		document.getElementById('prezzo_riv_1').scrollIntoView();
		return false;
	}
	else if(document.getElementById('sconto_rivenditore_2').value == '') {
		alert("Il campo sconto rivenditore 2 è obbligatorio");
		document.getElementById('sconto_rivenditore_2').scrollIntoView();
		return false;
	}
	else if(document.getElementById('prezzo_riv_2').value == '') {
		alert("Il campo prezzo rivenditore 2 è obbligatorio");
		document.getElementById('prezzo_riv_2').scrollIntoView();
		return false;
	}
	else if(document.getElementById('sconto_rivenditore_3').value == '') {
		alert("Il campo sconto distributore è obbligatorio");
		document.getElementById('sconto_rivenditore_3').scrollIntoView();
		return false;
	}
	else if(document.getElementById('prezzo_riv_3').value == '') {
		alert("Il campo prezzo distributore è obbligatorio");
		document.getElementById('prezzo_riv_3').scrollIntoView();
		return false;
	}
	else {
		return true;
	}
}

function calcolaScontoRivenditore2()
{
	var numprice_riv_1 = document.getElementById('prezzo_riv_1').value;
	var price_riv_1 = parseFloat(numprice_riv_1.replace(/\s/g, "").replace(",", "."));
	
	var listino = document.getElementById('listino_esolver').value;
	var price_listino = parseFloat(listino.replace(/\s/g, "").replace(",", "."));
	
	var prezzo_riv_2 = price_riv_1 + ((price_riv_1 / 100 ) * 3);
	
	if(prezzo_riv_2 > price_listino)
		prezzo_riv_2 = price_listino;
	
	document.getElementById('prezzo_riv_2').value = prezzo_riv_2;
	
	calcSconto('listino', 'prezzo_riv_2', 'sconto_rivenditore_2'); 
	
	var sconto_riv_2 = document.getElementById('sconto_rivenditore_2').value; // eliminare?
	var sco_riv_2 = parseFloat(sconto_riv_2.replace(/\s/g, "").replace(",", ".")); // eliminare?

	calcMarginalitaSconto('sconto_rivenditore_2', 'margin_riv_2', 'listino'); 
}

// Rivenditori + quantità, escluso Amazon
function calcMarginalitaScontoAll(all)
{
	if(all == 'all' || all == 'riv'){
		calcMarginalitaSconto('sconto_rivenditore_1', 'margin_riv_1', 'listino'); 
		calcMarginalitaSconto('sconto_rivenditore_2', 'margin_riv_2', 'listino'); 
		calcMarginalitaSconto('sconto_rivenditore_3', 'margin_riv_3', 'listino'); 
	}
	if(all == 'all' || all == 'qta'){
		calcMarginalitaSconto('sconto_quantita_1', 'margin_qta_1', 'priceTE'); 
		calcMarginalitaSconto('sconto_quantita_2', 'margin_qta_2', 'priceTE'); 
		calcMarginalitaSconto('sconto_quantita_3', 'margin_qta_3', 'priceTE');
	}
}

// Rivenditori + quantità, escluso Amazon
function calcPrezzoScontoAll(all)
{
	if(all == 'all' || all == 'riv'){
		calcPrezzoSconto('sconto_rivenditore_1', 'prezzo_riv_1', 'listino');
		calcPrezzoSconto('sconto_rivenditore_2', 'prezzo_riv_2', 'listino');
		calcPrezzoSconto('sconto_rivenditore_3', 'prezzo_riv_3', 'listino');
	}
	if(all == 'all' || all == 'qta'){
		calcPrezzoSconto('sconto_quantita_1', 'prezzo_qta_1', 'priceTE');
		calcPrezzoSconto('sconto_quantita_2', 'prezzo_qta_2', 'priceTE');
		calcPrezzoSconto('sconto_quantita_3', 'prezzo_qta_3', 'priceTE');
	}
}

// onkeyup Listino Vendita
function calcEsolverListino()
{
	calcPrezzoSconto('scontolistinovendita', 'priceTE', 'listino');
	calcScontoAcquisto(); // calcola netto esolver
	calcRebate(); // calcola acquisto netto

	calcMarginalita(); 
	calcSconto('listino', 'priceTE', 'scontolistinovendita');

	calcMarginalitaScontoAll('riv');
	aggiornaScontisticaAmazon();
	calcolaScontoRivenditore2();
	calcMarginalitaScontoAll('qta');

	calcPrezzoFromMargine('wholesale_price', 'margin_login_for_offer', 'prezzo_login_for_offer');
}

// onkeyup Sc.Acq.
function calcEsolverScAcq()
{
	if (isArrowKey(event)) return; 

	calcScontoAcquisto(); // calcola netto esolver
	calcRebate(); // calcola acquisto netto

	calcMarginalita();
	calcMarginalitaScontoAll('all');

	aggiornaScontisticaAmazon();						
	calcPrezzoFromMargine('wholesale_price', 'margin_login_for_offer', 'prezzo_login_for_offer');
}

// onchange Netto eSolver
function onChangeEsolverAcquisto()
{
	calcSconto('listino', 'wholesale_price_esolver', 'sconto_acquisto_1'); // calcola sc.acq. 1

	document.getElementById('sconto_acquisto_2').value = 0;
	document.getElementById('sconto_acquisto_3').value = 0;

	calcRebate(); // calcola acquisto netto

	calcMarginalita();
	calcMarginalitaScontoAll('all');

	aggiornaScontisticaAmazon();
	calcPrezzoFromMargine('wholesale_price', 'margin_login_for_offer', 'prezzo_login_for_offer');
}

function calcSalesListino()
{
	document.getElementById('listino_esolver').value = this.value.replace(/,/g, '.');

	calcPrezzoSconto('scontolistinovendita', 'priceTE', 'listino');

	calcScontoAcquisto(); 
	calcRebate();

	calcMarginalita(); 
	calcSconto('listino', 'priceTE', 'scontolistinovendita');

	calcMarginalitaScontoAll('all');
	calcPrezzoScontoAll('all');

	calcPrezzoFromMargine('wholesale_price', 'margin_login_for_offer', 'prezzo_login_for_offer');
}

function calcSalesScontoPrezzoV(tipo)
{
	if(tipo == 'sconto'){
		if(isArrowKey(event)) return; 
		calcPrezzoSconto('scontolistinovendita', 'priceTE', 'listino');
	}
	else if(tipo == 'prezzo'){
		calcSconto('listino', 'priceTE', 'scontolistinovendita');
	}

	calcMarginalita();

	calcMarginalitaScontoAll('qta');
	calcPrezzoScontoAll('qta');

	aggiornaScontisticaAmazon();
	calcPrezzoFromMargine('wholesale_price', 'margin_login_for_offer', 'prezzo_login_for_offer');
}

function calcSalesRebate(num)
{
	if(isArrowKey(event)) return; 

	calcRebate();
	calcMarginalita();

	calcMarginalitaScontoAll('all');

	if(num == '2'){
		calcPrezzoSconto('sconto_rivenditore_1', 'prezzo_riv_1', 'listino');
	}

	aggiornaScontisticaAmazon();	
	calcPrezzoFromMargine('wholesale_price', 'margin_login_for_offer', 'prezzo_login_for_offer');
}

function onChangeAcquisto()
{
	calcMarginalita();
	calcSconto('wholesale_price_esolver', 'wholesale_price', 'rebate_1');

	document.getElementById('rebate_2').value = 0;
	document.getElementById('rebate_3').value = 0;

	calcMarginalitaScontoAll('all');

	aggiornaScontisticaAmazon();
	calcPrezzoFromMargine('wholesale_price', 'margin_login_for_offer', 'prezzo_login_for_offer');
}

function calcScQta(num, tipo)
{
	if(tipo == 'sconto') {
		if(isArrowKey(event)) return;
		calcPrezzoSconto('sconto_quantita_' + num, 'prezzo_qta_' + num, 'priceTE');
	}
	else if(tipo == 'prezzo') {
		calcSconto('priceTE', 'prezzo_qta_' + num, 'sconto_quantita_' + num);
	}
	
	calcMarginalitaSconto('sconto_quantita_' + num, 'margin_qta_' + num, 'priceTE'); 
}

function calcScRiv(num, tipo)
{
	if(tipo == 'sconto') {
		if(isArrowKey(event)) return;
		calcPrezzoSconto('sconto_rivenditore_' + num, 'prezzo_riv_' + num, 'listino');
	}
	else if(tipo == 'prezzo') {
		calcSconto('listino', 'prezzo_riv_' + num, 'sconto_rivenditore_' + num);
	}

	if(num == '1'){
		calcolaScontoRivenditore2();
	}

	calcMarginalitaSconto('sconto_rivenditore_' + num, 'margin_riv_' + num, 'listino'); 
}

function calcAmazon(mkt, tipo)
{
	if(tipo == 'sconto') {
		if(isArrowKey(event)) return;
		calcPrezzoSconto('sconto_amazon_' + mkt, 'prezzo_amazon_' + mkt, 'miglior_prezzo'); 
	}
	else if(tipo == 'prezzo') {
		calcSconto('miglior_prezzo', 'prezzo_amazon_' + mkt, 'sconto_amazon_' + mkt); 
	}

	calcMarginalitaSconto2('sconto_amazon_'+ mkt, 'margin_amazon_' + mkt, 'miglior_prezzo'); 

	if(document.getElementById('margin_amazon_' + mkt).value < 12.50) { 
		document.getElementById('margine_amazon_' + mkt + '_alert').innerHTML = '<i class="icon-exclamation-sign" style="color:red" title="Margine ' + mkt.toUpperCase() + ' inferiore al 12,50%"></i>'; 
	} 
	else {
		document.getElementById('margine_amazon_' + mkt + '_alert').innerHTML = ''; 
	}
}

/* FINE OVERRIDE */
