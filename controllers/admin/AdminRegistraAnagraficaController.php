<?php

class AdminRegistraAnagraficaControllerCore extends AdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->lang = false;

        $this->context = Context::getContext();

        $this->display = 'view';
        
        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Registra Anagrafica'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitAnagrafica')) 
        {
            if (!isset($_POST['is_company']))
                $this->errors[] = Tools::displayError('Customer type is required');
            if (Tools::strlen(Tools::getValue('email')) > 0 AND Customer::customerExists(Tools::getValue('email')))
                $this->errors[] = Tools::displayError('This email is already registered');
            if (!($from = trim(Tools::getValue('email'))) OR !Validate::isEmail($from))
                $this->errors[] = Tools::displayError('Invalid e-mail address');
            /*if (Tools::strlen(Tools::getValue('firstname')) < 2)
                $this->errors[] = Tools::displayError('Firstname is required');
            if (Tools::strlen(Tools::getValue('lastname')) < 2)
                $this->errors[] = Tools::displayError('Lastname is required');*/
            if (Tools::getValue('is_company') == 1 AND Tools::strlen(Tools::getValue('company')) == 0)
                $this->errors[] = Tools::displayError('Company is required if this customer is a company');
            if (Tools::getValue('id_default_group') != 5 AND Tools::strlen(Tools::getValue('vat_number')) > 0 AND Customer::vatNumberExists(Tools::getValue('vat_number')))
                $this->errors[] = Tools::displayError('This vat number is already registered'); 
            /*if (Tools::getValue('is_company') == 1 AND Tools::strlen(Tools::getValue('vat_number')) == 0)
                $this->errors[] = Tools::displayError('VAT number is required if this customer is a company');*/
            if (Tools::getValue('id_country') == 10) 
            {
                $esito_indirizzo = Address::checkAddress($_POST['address1'], $_POST['postcode'], $_POST['city']);
                $esito_indirizzo = 'Destinazione corretta.'; // prova
        
                if($esito_indirizzo != 'Destinazione corretta.' && $_POST['id_country'] == 10)
                    $this->errors[] = Tools::displayError('L\'indirizzo di fatturazione non è stato identificato');
        
                if (Tools::strlen(Tools::getValue('vat_number')) > 0 AND Tools::strlen(Tools::getValue('vat_number')) != 11)
                    $this->errors[] = Tools::displayError('VAT Number is not valid');
                if (Tools::strlen(Tools::getValue('tax_code')) > 0 && Tools::strlen(Tools::getValue('tax_code')) != 16 && Tools::strlen(Tools::getValue('tax_code')) != 11)
                    $this->errors[] = Tools::displayError('Tax code is not valid');
            }
            /* else {
                if (Tools::strlen(Tools::getValue('vat_number')) == 0)
                    $this->errors[] = Tools::displayError('VAT Number is not valid');
                if (Tools::strlen(Tools::getValue('tax_code')) == 0)
                    $this->errors[] = Tools::displayError('Tax code is not valid');
            }*/

            /* 
            if (Tools::getValue('id_default_group') != 5 AND Tools::strlen(Tools::getValue('tax_code')) > 0 AND Customer::taxCodeExists(Tools::getValue('tax_code'))  AND Customer::CodiceSpringExists(Tools::getValue('codice_esolver')))
                $this->errors[] = Tools::displayError('This tax code is already registered');
            if (Tools::strlen(Tools::getValue('address1')) < 2)
                $this->errors[] = Tools::displayError('Address is required');
            if (Tools::getValue('id_country') == 10 && Tools::strlen(Tools::getValue('postcode')) == 0)
                $this->errors[] = Tools::displayError('Postcode is required');
            if (Tools::strlen(Tools::getValue('city')) < 2)
                $this->errors[] = Tools::displayError('City is required');
            if (Tools::strlen(Tools::getValue('id_country')) == 0)
                $this->errors[] = Tools::displayError('Country is required');	
            if (Country::containsStates(Tools::getValue('id_country')) AND !(int)(Tools::getValue('id_state')))
                $this->errors[] = Tools::displayError('This country requires a state selection.');
            if (Tools::strlen(Tools::getValue('phone')) < 2)
                $this->errors[] = Tools::displayError('Phone is required');	
            if (Tools::getValue('is_company') == 1 AND Tools::strlen(Tools::getValue('employees_number')) == 0)
                $this->errors[] = Tools::displayError('Employees number is required if customer is a company');
            */
            
            if(!sizeof($this->errors)) 
            {
                $numcustomer = Db::getInstance()->getValue("SELECT id_customer FROM "._DB_PREFIX_."customer ORDER BY id_customer DESC");
                $numaddress = Db::getInstance()->getValue("SELECT id_address FROM "._DB_PREFIX_."address ORDER BY id_address DESC"); 
                $numcustomer = $numcustomer + 1;
                $numaddress = $numaddress + 1;

                $is_company = Tools::getValue('is_company');
                
                if($is_company == 1) {
                    if(Tools::getValue('vat_number') != '')	
                        $password = Tools::encrypt(Tools::getValue('vat_number'));
                    else
                        $password = Tools::encrypt(Tools::getValue('tax_code'));
                }
                else {
                    $password = Tools::encrypt(Tools::getValue('tax_code'));
                }

                $date_add = date("Y-m-d H:i:s");
                $date_upd = date("Y-m-d H:i:s");
                
                if(Tools::getValue('is_company') == 1)
                    $impiegati = Tools::getValue('employees_number');
                else
                    $impiegati = '';
                
                if(Tools::getValue('id_default_group') == 5)
                    $disabilita_notifiche = 1;
                else
                    $disabilita_notifiche = 0;
                    
                Db::getInstance()->execute("INSERT INTO "._DB_PREFIX_."customer (id_customer, id_gender, id_default_group, is_company, firstname, lastname, codice_esolver, email, codice_univoco, pec, passwd, last_passwd_gen, birthday, tax_code, vat_number, company, employees_number, newsletter, ip_registration_newsletter, newsletter_date_add, optin, secure_key, note, active, is_guest, deleted, order_notifications, date_add, date_upd, created_by) VALUES ($numcustomer, 1, '".Tools::getValue('id_default_group')."', '".Tools::getValue('is_company')."', '".addslashes(Tools::getValue('firstname'))."', '".addslashes(Tools::getValue('lastname'))."', '".addslashes(Tools::getValue('codice_esolver'))."', '".addslashes(trim(Tools::getValue('email')))."', '".addslashes(Tools::getValue('codice_univoco'))."',  '".addslashes(Tools::getValue('pec'))."', '$password', '$date_add', '', '".addslashes(Tools::getValue('tax_code'))."', '".addslashes(Tools::getValue('vat_number'))."', '".addslashes(Tools::getValue('company'))."', '$impiegati', 1, '', '$date_add', 0, '', '', 1, 0, 0, '".$disabilita_notifiche."', '$date_add', '$date_upd', ".$this->context->employee->id.");");
                
                if(Tools::getValue('note_private') != '')
                {
                    Db::getInstance()->execute("
                        INSERT INTO customer_note (
                            id_note,
                            id_customer,
                            id_employee,
                            note,
                            date_add,
                            date_upd
                        )
                        VALUES (
                            NULL,
                            '".$numcustomer."',
                            '".$this->context->employee->id."',
                            '".addslashes(Tools::getValue('note_private'))."',
                            '".date("Y-m-d H:i:s")."',
                            '".date("Y-m-d H:i:s")."'
                        )");
                }	
                    
                Db::getInstance()->execute("INSERT INTO "._DB_PREFIX_."customer_group (id_customer, id_group) VALUES (".$numcustomer.", 1);");
                                    
                if(strpos(strtolower(Tools::getValue('company')), 's.p.a') !== false || strpos(strtolower(Tools::getValue('company')), ' spa') !== false || strpos(strtolower(Tools::getValue('company')), ' spa ') !== false)
                {
                    Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."customer_group WHERE id_customer = ".$numcustomer);
                    Db::getInstance()->execute("INSERT INTO "._DB_PREFIX_."customer_group (id_customer, id_group) VALUES (".$numcustomer.", 8)");
                    Db::getInstance()->execute("UPDATE "._DB_PREFIX_."customer SET id_default_group = 8 WHERE id_customer = ".$numcustomer);	
                }
                    
                Db::getInstance()->execute("INSERT INTO "._DB_PREFIX_."address (id_address, id_country, id_state, id_customer, id_manufacturer, id_supplier, alias, company, lastname, firstname, address1, address2, postcode, suburb, city, other, phone, phone_mobile, fax, vat_number, tax_code, company_tax_code, dni, date_add, date_upd, active, deleted, fatturazione) VALUES ($numaddress, '".Tools::getValue('id_country')."', '".Tools::getValue('id_state')."', $numcustomer, 0, 0, 'Il mio indirizzo', '".addslashes(Tools::getValue('company'))."', '".addslashes(Tools::getValue('lastname'))."', '".addslashes(Tools::getValue('firstname'))."', '".addslashes(Tools::getValue('address1'))."', '', '".addslashes(Tools::getValue('postcode'))."', '', '".addslashes(Tools::getValue('city'))."', '', '".addslashes(Tools::getValue('phone'))."', '', '', '', '', '', '', '$date_add', '$date_upd', 1,0,1);");
                
                if($this->context->employee->id_profile == 7)
                    Db::getInstance()->execute("INSERT INTO customer_amministrazione (id_customer, agente) VALUES ($numcustomer, '".$this->context->employee->id."')");
                
                if(Tools::getValue('id_default_group') == 5)
                {
                    $pagamento_default = Db::getInstance()->getValue('SELECT pagamento FROM customer_amministrazione WHERE id_customer = '.$numcustomer);
                    
                    if($pagamento_default == '' || $pagamento_default == 'Bonifico bancario' || $pagamento_default == 'Bonifico Bancario')
                        Db::getInstance()->execute('UPDATE customer_amministrazione SET pagamento = "Bonifico 30 gg. fine mese" WHERE id_customer = '.$numcustomer);

                    if($pagamento_default == '')
                        Db::getInstance()->execute('INSERT INTO customer_amministrazione (id_customer, pagamento) VALUES ('.$numcustomer.', "Bonifico 30 gg. fine mese")');
                }
                
                if($is_company == 1)
                    Db::getInstance()->execute("INSERT INTO persone (id_persona, firstname, lastname, role, email, email_aziendale, phone, phone_mobile, id_customer, tax_code, vat_number) VALUES ('NULL', '".addslashes(Tools::getValue('firstname'))."', '".addslashes(Tools::getValue('lastname'))."','".addslashes(Tools::getValue('ruolo'))."', '".addslashes(Tools::getValue('email'))."', '".addslashes(trim(Tools::getValue('email')))."', '".addslashes(Tools::getValue('phone'))."', '', $numcustomer, '".addslashes(Tools::getValue('tax_code'))."', '".addslashes(Tools::getValue('vat_number'))."')");
                
                /* SE NEWSLETTER = 1, INVIO UPDATE A RFMCUBE E SENDINBLUE -> TESTARE E ATTIVARE! */
                /*
                $update_customer_where = 'id_customer = '.$numcustomer.' ';

                $customers = Db::getInstance()->executeS('
                    SELECT * 
                    FROM `'._DB_PREFIX_.'customer` 
                    WHERE '.$update_customer_where.'
                ');

                foreach($customers as $customer)
                {
                    $id_customer = $customer['id_customer'];
                    $customer = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'customer WHERE id_customer = '.$id_customer);
                    
                    // Se azienda, nome azienda; altrimenti, nome e cognome
                    $nomecliente = $customer['firstname'];
                    $nomecliente .= ' '.$customer['lastname'];
                    $company_or_name = (($customer['company'] != '') ? ($customer['active'] == 1 ? $customer['company'] : ($customer['active'] == 0 ? 'NON ATTIVO - '.$customer['company'] : 'CESSATA ATTIVITA\' - '.$customer['company'])) : ($customer['active'] == 1 ? $nomecliente : ($customer['active'] == 0 ? 'NON ATTIVO - '.$nomecliente : 'CESSATA ATTIVITA\' - '.$nomecliente)));
                    
                    if($company_or_name == " ")
                        $company_or_name = 'No company';
                    
                    // Se non ci sono provincia o regione, prendo lo stato
                    $stato_regione_provincia = Db::getInstance()->getValue('SELECT IF(a.id_state = 0, c.name, s.name) as nome FROM '._DB_PREFIX_.'address a LEFT JOIN '._DB_PREFIX_.'state s ON a.id_state = s.id_state LEFT JOIN '._DB_PREFIX_.'country_lang c ON a.id_country = c.id_country WHERE c.id_lang = '.$this->context->language->id.' AND a.id_customer = '.$id_customer.' AND a.active = 1 AND a.deleted = 0 AND a.fatturazione = 1 ORDER BY a.id_address DESC');
                    if($stato_regione_provincia == false)
                        $stato_regione_provincia = 'No region';
                    
                    $city = Db::getInstance()->getValue('SELECT city FROM '._DB_PREFIX_.'address WHERE id_customer = '.$id_customer.' AND active = 1 AND deleted = 0 AND fatturazione = 1 ORDER BY id_address DESC');
                    if($city == false || $city == '' || $city == ' ' || $city == '-- Prima selezionare CAP --')
                        $city = 'No city';
                    
                    $array_customer = array();		
                    
                    $array_customer = array(
                        "id"=> $customer['id_customer'], // obbligatorio
                        "email"=> $customer['email'], // obbligatorio
                        "createdAt"=> date('Y-m-d\Th:i:s\Z',strtotime($customer['date_add'])), // obbligatorio
                        "updatedAt"=> date('Y-m-d\Th:i:s\Z',strtotime($customer['date_upd'])), // obbligatorio
                        // metacampi opzionali - max 5 per il nostro piano
                        "company"=> $company_or_name, // opzionale 1
                        "group" => $customer['id_default_group'], // opzionale 2
                        "country_state" => $stato_regione_provincia, // opzionale 3
                        "city" => $city, // opzionale 4
                        "newsletter" => $customer['newsletter'], // opzionale 5
                    );
                    
                    $data = array(
                        "triggerIntegrations" => true,
                        'customer' => 
                        $array_customer
                    );
                    
                    unset($array_customer);

                    $payload = json_encode($data);
                    
                    $url = 'https://api.rfmcube.com/v1/import';
                    $ch = curl_init( $url );
                    # Setup request to send json via POST.
                    echo "OK"; 
                    curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
                    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Accept: application/json', 'api-key:TFHlycgSUnO7w0VW2sPpwKAKBR83fFPn78fC4rlyqRD5l2rT0ntwWmTLISPx'));
                    # Return response instead of printing.
                    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                    # Send request.
                    $result = curl_exec($ch);
                    curl_close($ch);
                }
                */
                                
                Tools::redirectAdmin(self::$currentIndex.'&numcustomer='.$numcustomer.'&conf=4&token='.$this->token);	
            }
        }

        parent::postProcess();
    }

    public function renderView()
    {
        $content = $this->displayContent();

        $this->tpl_view_vars = array(
            'content' => $content,
        );

        return parent::renderView();
    }

    // Correggere: fare tpl; per ora viene usato il metodo 1.4
    public function displayContent()
    {
        $this->displayWarning('Correggere js (spostare nei file giusti). Mancano le classi (es. ee_address) e il modulo vatnumber; $selectedCountry non esiste, vedi "correggere" nel codice.');

        $content = '';

        if(Tools::getIsset('conf') && Tools::getValue('conf') == 4) {
            $tokenCustomers = Tools::getAdminTokenLite('AdminCustomers');
            $content .= "<br /><a href='index.php?controller=AdminCustomers&id_customer=".Tools::getValue('numcustomer')."&viewcustomer&token=".$tokenCustomers."'>Clicca qui per aprire la scheda del cliente appena registrato.</a><br />";
        }

        if(isset($_GET['conf'])) {
            $content .= "<a class='button' href='".self::$currentIndex.'&token='.$this->token."'>Clicca qui per creare una nuova anagrafica</a>";
        }
        else 
        {
            $groups = Group::getGroups($this->context->language->id, true);
			
            $content .= '<form method="post" class="std" enctype="multipart/form-data">
                    <fieldset>
                        <table>
                        <tr>
                            <td>
                                <label for="tipo"><strong>Tipo cliente*</strong></label>
                            </td>
                            <td>
                                <input type="radio" name="is_company" value="1" '.(Tools::getIsset('is_company') && Tools::getValue('is_company') == 1 ? 'checked="checked"' : '').' onclick="document.getElementById(\'id_default_group\').options[0].selected = \'selected\';" />Azienda <br />
                                <input type="radio" name="is_company" '.(Tools::getIsset('is_company') && Tools::getValue('is_company') == 0 ? 'checked="checked"' : '').' value="0" onclick="document.getElementById(\'id_default_group\').options[1].selected = \'selected\';" />Privato
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="tipo"><strong>Profilo*</strong></label>
                            </td>
                            <td>
                                <select name="id_default_group" id="id_default_group" onchange="checkDefaultGroup(this.value);">'; // correggere: la funzione checkDefaultGroup non esiste
                            foreach ($groups as $group)
                                $content .= '<option value="'.(int)($group['id_group']).'" '.(Tools::getIsset('id_default_group') && Tools::getValue('id_default_group') == $group['id_group'] ? 'selected="selected"' : '').'>'.htmlentities($group['name'], ENT_NOQUOTES, 'utf-8').'</option>';
                            $content .= '
                                </select>
                            </td>
                        </tr>
                        <tr>
                        <tr>
                            <td>
                                <label for="firstname"><strong>Nome</strong></label>
                            </td>
                            <td>
                                <input type="text" id="firstname" style="width:400px" name="firstname" value="'.(isset($_POST['firstname']) ? stripslashes($_POST['firstname']) : '').'" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="lastname"><strong>Cognome</strong></label></td><td>
                                <input type="text" id="lastname" style="width:400px" name="lastname" value="'.(isset($_POST['lastname']) ? stripslashes($_POST['lastname']) : '').'" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="lastname"><strong>Mansione in azienda</strong></label></td><td>
                                <input type="text" id="ruolo" style="width:400px" name="ruolo" value="'.(isset($_POST['ruolo']) ? stripslashes($_POST['ruolo']) : '').'" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="company"><strong>Azienda</strong></label></td><td>
                                <input type="text" id="company" style="width:400px" name="company" value="'.(isset($_POST['company']) ? stripslashes($_POST['company']) : '').'" /> (obbligatorio se azienda)
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="vat_number"><strong>Partita IVA</strong></label></td><td>
                                <input type="text" id="vat_number" style="width:400px" onchange="checkPI(this.value)" name="vat_number" value="'.(isset($_POST['vat_number']) ? stripslashes($_POST['vat_number']) : '').'" />
                                (obbligatorio se azienda)
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="tax_code"><strong>Codice Fiscale</strong></label></td><td>
                                <input type="text" id="tax_code" style="width:400px" onchange="checkCF(this.value)" name="tax_code" value="'.(isset($_POST['tax_code']) ? stripslashes($_POST['tax_code']) : '').'" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="codice_esolver"><strong>Codice eSolver</strong></label></td><td>
                                <input type="text" id="codice_esolver" style="width:400px" name="codice_esolver" value="'.(isset($_POST['codice_esolver']) ? stripslashes($_POST['codice_esolver']) : '').'" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="email"><strong>Email*</strong></label></td><td>
                                <input type="text" id="email" style="width:400px" name="email" value="'.(isset($_POST['email']) ? stripslashes($_POST['email']) : '').'" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="codice_univoco"><strong>Codice univoco SDI</strong></label></td><td>
                                <input type="text" id="codice_univoco" style="width:400px" name="codice_univoco" value="'.(isset($_POST['codice_univoco']) ? stripslashes($_POST['codice_univoco']) : '').'" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="pec"><strong>PEC</strong></label></td><td>
                                <input type="text" id="pec" style="width:400px" name="pec" value="'.(isset($_POST['pec']) ? stripslashes($_POST['pec']) : '').'" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="address1"><strong>Indirizzo</strong></label></td><td>
                                <input type="text" id="address1" style="width:400px" name="address1" value="'.(isset($_POST['address1']) ? stripslashes($_POST['address1']) : '').'" />
                            </td>
                        </tr>

                        <!-- PROVA: --> <script type="text/javascript" src="./themes/default/js/registra_anagrafica.js"></script>
                        <!-- CORREGGERE - decommentare: <script type="text/javascript" src="https://www.ezdirect.it/themes/ezdirect-new/js/tools.js"></script> -->

                        <tr>
                            <td>
                                <label for="country"><strong>Nazione</strong></label></td><td>
                                <select id="id_country" style="width:400px" name="id_country" onchange="countryChange()";>';
                                $countries = Db::getInstance()->executeS("SELECT c.id_country, c.contains_states, cl.name FROM "._DB_PREFIX_."country c JOIN "._DB_PREFIX_."country_lang cl ON c.id_country = cl.id_country WHERE cl.id_lang = ".$this->context->language->id." ORDER BY cl.name ASC");
                            foreach ($countries as $country) {
                                $content .= '<option '.(isset($_POST['country']) && $_POST['country'] == $country['id_country'] ? 'selected="selected"' : ($country['id_country'] == 10 ? 'selected="selected"' : '')).' value="'.$country['id_country'].'">'.$country['name'].'</option>';
                            }
                            $content .= '
                                </select>
                            </td>
                        </tr>

                        <script type="text/javascript">
                            $(document).ready(function(){
                                countryChange();
                            });	
                        </script>

                        <tr class="ee_address">
                            <td>
                                <label for="postcode"><strong>CAP</strong></label></td><td>
                                <input type="text" id="postcode" style="width:400px" name="postcode" value="'.(isset($_POST['postcode']) ? stripslashes($_POST['postcode']) : '').'" />
                            </td>
                        </tr>
                            <tr class="ee_address"><td>
                            <label for="city"><strong>Citt&agrave;</strong></label></td><td>
                                <input type="text" id="city" style="width:400px" name="city" value="'.(isset($_POST['city']) ? stripslashes($_POST['city']) : '').'" />
                            
                        </td></tr>
                        <tr class="ee_address">
                    
                            <div id="contains_states" '.(!Country::containsStates((int)$selectedCountry) ? 'style="display:none;"' : '').'>
        
                        <td>
                                    <label for="id_state"><strong>Provincia</strong></label></td>
                                    <td><select name="id_state" id="id_state">
                                        <option value="">-</option>
                                    </select>
                                    <sup></sup></td>
                            </div>
                                </tr>
                        
                            <tr class="it_address"><td>
                            
                            
                            <label for="postcode"><strong>CAP</strong></label></td><td>
                                <input type="text" id="cap" style="width:400px" onkeyup="cercaCitta();" name="postcode" value="'.(isset($_POST['postcode']) ? stripslashes($_POST['postcode']) : '').'" /> <img class="img_control" src="../img/admin/help.png" alt="Inserisci il CAP, se trovato nel sistema aggiunge in automatico città e provincia nei campi successivi" title="Inserisci il CAP, se trovato nel sistema aggiunge in automatico città e provincia nei campi successivi" /> <a href="index.php?controller=AdminCap&token='.Tools::getAdminToken('AdminCap'.(int)(Tab::getIdFromClassName('AdminCap')).($this->context->employee->id)).'" target="_blank">Clicca per aprire strumento aggiornamento CAP</a>
                            
                        </td></tr>
                            <tr class="it_address"><td>
                            <label for="city"><strong>Citt&agrave;</strong></label></td><td>
                                <select name="city" id="citta" onclick="cercaProvincia();">
                                    <option name="">-- Prima selezionare CAP --</option>
                                    '.(isset($_POST['city']) ? '<option name="'.$_POST['city'].'" selected="selected">'.$_POST['city'].'</option>' : '').'
                                </select>
                        </td></tr>
                        <tr class="it_address">
                    
                        <td>
                                    <label for="id_state"><strong>Provincia</strong></label></td>
                                    <td>
                                    
                                    <input id="provincia" name="provincia_mark" readonly="readonly" type="text" value="'.(isset($_POST['provincia_mark']) ? $_POST['provincia_mark'] : '').'" />
                                    <input id="provincia_hidden" name="id_state" type="hidden" value="'.(isset($_POST['id_state']) ? $_POST['id_state'] : '').'" />
                                    
                                    <sup></sup></td>
                            
                                </tr>
                            
                            <tr><td>
                            <label for="phone"><strong>Telefono</strong></label></td><td>
                                <input type="text" id="phone" style="width:400px" name="phone" value="'.(isset($_POST['phone']) ? $_POST['phone'] : '').'" />
                            
                        </td></tr>
                        
                        <tr>
                        <td>
                            <label for="employees_number"><strong>Numero dipendenti</strong></label>
                        </td><td>
                                <select name="employees_number">
                        <option value="Da 1 a 10">Da 1 a 10</option>
                        <option value="Da 11 a 50">Da 11 a 50</option>
                        <option value="50 piu">Pi&ugrave; di 50</option>
                        </select> (solo se azienda, se privato non viene registrato)
                        </td></tr>
                        <tr><td>
                            <label for="note_private"><strong>Note private</strong></label></td><td>
                                <input type="text" id="note_private" style="width:400px" name="note_private" value="'.(isset($_POST['note_private']) ? stripslashes($_POST['note_private']) : '').'" />
                            
                        </td></tr>
                        ';

                        // $id_country_ajax = (int)$this->getFieldValue($obj, 'id_country'); // correggere: eliminare?

                $content .= '
                    <script type="text/javascript">
                    
                    function checkPI(pi)
                    {
                        if(pi == "") {

                        }
                        else
                        {
                            $.ajax({
                                url: "ajax.php",
                                cache: false,
                                data: "checkPI=y&pi="+pi,
                                success: function(html)
                                {
                                    if (html == \'yes\') {
                                        alert("ATTENZIONE: esiste gia un cliente con questa partita IVA.");
                                    }
                                }
                            });	
                        }								
                        
                    }
                    
                    function checkCF(cf)
                    {
                        if(cf == "") {

                        }
                        else
                        {
                            $.ajax({
                                url: "ajax.php",
                                cache: false,
                                data: "checkCF=y&cf="+cf,
                                success: function(html)
                                {
                                    if (html == \'yes\') {
                                        alert("ATTENZIONE: esiste gia un cliente con questo codice fiscale.");
                                    }
                                }
                            });	
                        }								
                        
                    }
                    
                    $(document).ready(function(){
                            ajaxStates ();
                            $(\'#id_country\').change(function() {
                                ajaxStates ();
                            });
                            function ajaxStates()
                            {
                                $.ajax({
                                    url: "ajax.php",
                                    cache: false,
                                    data: "ajaxStates=1&id_country="+$(\'#id_country\').val()+"&id_state="+$(\'#id_state\').val(),
                                    success: function(html)
                                    {
                                        if (html == \'false\')
                                        {
                                            $("#contains_states").hide();
                                            $(\'#id_state option[value=0]\').attr("selected", "selected");
                                        }
                                        else
                                        {
                                            $("#id_state").html(html);
                                            $("#contains_states").fadeIn();
                                            $(\'#id_state option[value='.(int)$obj->id_state.']\').attr("selected", "selected");
                                        }
                                    }
                                }); '; // correggere: $obj non esiste?

                    if (file_exists(_PS_MODULE_DIR_.'vatnumber/ajax.php'))
                        $content .= '	$.ajax({
                                    type: "GET",
                                    url: "'._MODULE_DIR_.'vatnumber/ajax.php?id_country="+$(\'#id_country\').val(),
                                    success: function(isApplicable)
                                    {
                                        if (isApplicable == 1)
                                            $(\'#vat_area\').show();
                                        else
                                            $(\'#vat_area\').hide();
                                    }
                                });';
                    $content .= '	}; }); </script>';
                        
                        $content .= '<tr><td>
                                * campo obbligatorio<br /><br />
                                <input type="submit" name="submitAnagrafica" id="submitAnagrafica" value="Conferma" class="button" onclick="$(this).hide();" />
                            </td><td></td></tr>
                        </table>
                    </fieldset>
                </form>	
            ';
        }

        return $content;
    }
}