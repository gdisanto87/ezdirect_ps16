<?php

class AdminTESTControllerCore extends AdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'tab';
        $this->lang = false;

        $this->context = Context::getContext();

        $this->_select = 'tl.name';
		
        $this->_join = '
            LEFT JOIN `'._DB_PREFIX_.'tab_lang` tl ON (a.`id_tab` = tl.`id_tab`)
        ';

        // a.id_parent = 111 = AdminParentTest
        $this->_where = '
            AND a.id_parent = 111
            AND tl.id_lang = '.(int)$this->context->language->id
        ;

        $this->fields_list = array(
            'name' => array(
                'title' => $this->l('Vai a:'),  
                'align' => 'left',
                'search' => false,
                'orderby' => false,
                'remove_onclick' => true,
                'callback' => 'goToUrl',
            ),
        );

        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Strumenti'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }
    
    public function initToolbar()
    {
        parent::initToolbar();

        unset($this->toolbar_btn['new']); // Toglie il pulsante ADD NEW dalla toolbar
    }

    // Link al controller di nome "$value"
    public function goToUrl($value)
    {
        $className = Db::getInstance()->getValue('
            SELECT t.class_name
            FROM `'._DB_PREFIX_.'tab` t
                LEFT JOIN `'._DB_PREFIX_.'tab_lang` tl ON (t.`id_tab` = tl.`id_tab`)
            WHERE tl.name = "'.$value.'"
                AND tl.id_lang = '.(int)$this->context->language->id
        );
        $this->currentIndex = $className;
        $this->token = Tools::getAdminTokenLite($className);
        $href = '/ezadmin/index.php?controller='.$this->currentIndex.'&token='.$this->token;
        return '<a href="'.$href.'">'.$value.'</a>';
    }
}
