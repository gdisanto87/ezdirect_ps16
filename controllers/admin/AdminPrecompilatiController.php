<?php

class AdminPrecompilatiControllerCore extends AdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'precompilato';
        $this->className = 'Precompilato';
        $this->lang = false;

        $this->allow_export = true;

        $this->context = Context::getContext();
        $this->default_form_language = $this->context->language->id;

    	$this->fields_list = array(
			'id_precompilato' => array(
                'title' => $this->l('ID'), 
                'align' => 'text-center', 
            ),
			'oggetto' => array(
                'title' => $this->l('Oggetto'),
            ),
			'date_add' => array(
                'title' => $this->l('Data inserimento'),
                'type' => 'date',
            ),
			'date_upd' => array(
                'title' => $this->l('Data modifica'),
                'type' => 'date',
            ),
			'active' => array(
                'title' => $this->l('Attivato'),
                'align' => 'text-center', 
                'active' => 'status', 
                'type' => 'bool', 
                'orderby' => false,
            )
        );

        $this->_use_found_rows = true;
        
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        
        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Messaggi Precompilati'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }

    public function renderForm()
    {
        if (!($obj = $this->loadObject(true))) {
            return;
        }

        // CORREGGERE: in update devo fare: 
        // oggetto = '".addslashes($_POST['oggetto'])."', testo = '".addslashes($_POST['testo'])."'

        $this->fields_form = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->l('Precompilato'),
                'icon' => 'icon-pencil'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Subject'),
                    'name' => 'oggetto',
                    'required' => true,
                    'col' => '6',
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Text'),
                    'name' => 'testo',
                    'required' => false,
                    'col' => '6',
                    'autoload_rte' => true,
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Active'),
                    'name' => 'active',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                    'onchange' => 'displayDraftWarning(this.id)',
                ),
            ),
        );

        $this->fields_form['submit'] = array(
            'title' => $this->l('Save'),
        );

        // autoload rich text editor (tiny mce)
        $this->tpl_form_vars['tinymce'] = true;
        $iso = $this->context->language->iso_code;
        $this->tpl_form_vars['iso'] = file_exists(_PS_CORE_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en';
        $this->tpl_form_vars['path_css'] = _THEME_CSS_DIR_;
        $this->tpl_form_vars['ad'] = __PS_BASE_URI__.basename(_PS_ADMIN_DIR_);

        $this->addJS(array(
            _PS_JS_DIR_.'tiny_mce/tiny_mce.js',
            _PS_JS_DIR_.'admin/tinymce.inc.js',
            _PS_BO_DEFAULT_THEME_JS_DIR_.'override.js',
        ));

        return parent::renderForm();
    }
}